<?php
// Using:

/***********************************************
 *  20200428 Bill:  [2019-0924-1029-57289]
 *      - add logic to support bothLang = 1
 * 	20150202 Bill:
 * 		- default language selection same as current language
 * 	20100112 Marcus:
 * 		- fixed comment selection box
 * *********************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$CommentType = $_GET['CommentType'];
$fieldname = $_GET['fieldname'];

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include($PATH_WRT_ROOT."includes/eRCConfig.php");
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	if (!isset($fieldname)) {
		header("Location: /index.php");
		exit();
	}
	
	if ($lreportcard->hasAccessRight()) {

		$MODULE_OBJ['title'] = $eReportCard['SelectComment'];
		$linterface = new interface_html("popup.html");
		$linterface->LAYOUT_START();
		
		$IsAdditional = ($CommentType=='AdditionalComment')? 1 : 0;
		
		$CommentArr = $lreportcard->GET_COMMENT_BANK_SUGGEST($SubjectID, TRUE, $IsAdditional, $cat);

//		$CommentOptions = array();
//		$CommentEngOptions = array();
//		$tempCount1 = 0;
//		$tempCount2 = 0;
		
		foreach($CommentArr as $key => $thisComment)
		{
//			$CommentChiArr[] = "(".$thisComment["CommentCode"].") ".$thisComment["Comment"];
//			$CommentEngArr[] = "(".$thisComment["CommentCode"].") ".$thisComment["CommentEng"];
			$CommentChiArr[$key]['Display'] = $thisComment["Comment"];
			$CommentChiArr[$key]['Value'] = $thisComment["Comment"];
			$CommentChiArr[$key]['CommentCode'] = $thisComment["CommentCode"];
			$CommentEngArr[$key]['Display'] = "(".$thisComment["CommentCode"].") ".$thisComment["CommentEng"];
			$CommentEngArr[$key]['Value'] = $thisComment["CommentEng"];

			// [2019-0924-1029-57289]
			if($_REQUEST['bothLang'] == 1)
			{
			    $thisCommentBothLang = $thisComment["Comment"].'
'.$thisComment["CommentEng"];

                $CommentChiArr[$key]['Display'] = $thisCommentBothLang;
                $CommentChiArr[$key]['Value'] = $thisCommentBothLang;
                $CommentEngArr[$key]['Display'] = "(".$thisComment["CommentCode"].") ".$thisCommentBothLang;
                $CommentEngArr[$key]['Value'] = $thisCommentBothLang;
            }
		}
		
//		$CommentChiArr = Get_Array_By_Key($CommentArr,"Comment");
//		$CommentEngArr = Get_Array_By_Key($CommentArr,"CommentEng");
/*
		for($i=0; $i<sizeof($CommentArr); $i++) {
			if (trim($CommentArr[$i]["Comment"]) != "") {
				# count substr before changing to htmlspecialchars
				# otherwise, "yeah" 'yeah' yeah will become "yeah" 'yeah#0
				//$tempComment = htmlspecialchars(stripslashes($CommentArr[$i]["Comment"]), ENT_QUOTES);
				//$CommentOptions[$tempCount1][0] = $tempComment;
				//$CommentOptions[$tempCount1][1] = mb_substr($tempComment, 0, 30);
				
				if ($eRCCommentSetting['ShowCompleteSentence'])
				{
					$tempComment = trim($CommentArr[$i]["Comment"]);
					
					// Trim the line break of the comment in the "value" field
					$CommentOptions[$tempCount1][0] = str_replace(chr(13), ' ', $tempComment);
					$CommentOptions[$tempCount1][1] = $tempComment;
					$tempCount1++;
				}
				else
				{
					$tempCommentLong = trim($CommentArr[$i]["Comment"]);
					$tempCommentShort = trim(mb_substr($CommentArr[$i]["Comment"], 0, 30));
					
					// Trim the line break of the comment in the "value" field
					$tempCommentLong = str_replace(chr(13), ' ', $tempCommentLong);
					
					$CommentOptions[$tempCount1][0] = $tempCommentLong;
					$CommentOptions[$tempCount1][1] = $tempCommentShort;
					if (mb_strlen($CommentArr[$i]["Comment"]) > 30)
						$CommentOptions[$tempCount1][1] .="...";
					$tempCount1++;
				}
			}
			
			if (trim($CommentArr[$i]["CommentEng"]) != "") {
				//$tempCommentEng = htmlspecialchars(stripslashes($CommentArr[$i]["CommentEng"]), ENT_QUOTES);
				//$CommentEngOptions[$tempCount2][0] = $tempCommentEng;
				//$CommentEngOptions[$tempCount2][1] = mb_substr($tempCommentEng , 0, 30);
				
				if ($eRCCommentSetting['ShowCompleteSentence'])
				{
					$tempComment = trim($CommentArr[$i]["CommentEng"]);
					// Trim the line break of the comment in the "value" field
					$CommentEngOptions[$tempCount2][0] = str_replace(chr(13), ' ', $tempComment);
					$CommentEngOptions[$tempCount2][1] = $tempComment;
					$tempCount2++;
				}
				else
				{
					$tempCommentLong = trim($CommentArr[$i]["CommentEng"] );
					$tempCommentShort = trim(mb_substr($CommentArr[$i]["CommentEng"], 0, 30));
					
					// Trim the line break of the comment in the "value" field
					$tempCommentLong = str_replace(chr(13), ' ', $tempCommentLong);
					
					$CommentEngOptions[$tempCount2][0] = $tempCommentLong;
					$CommentEngOptions[$tempCount2][1] = $tempCommentShort;
					if (mb_strlen($CommentArr[$i]["CommentEng"]) > 30)
						$CommentEngOptions[$tempCount2][1] .="...";
					$tempCount2++;
				}
			}
		}
*/
		# prepare Options of selection box (multiline comment are separated into different options with same value)
		$idx=0;
			
		foreach	((array)$CommentEngArr as $thisComment)
		{
			$tmpOption = explode("\n",$thisComment['Display']);
			foreach((array)$tmpOption as $thisOptionPiece)
			{
				if ($eRCCommentSetting['ShowCompleteSentence'])
					$thisOptionPieceShort = trim($thisOptionPiece);
				else
					$thisOptionPieceShort = trim(mb_substr($thisOptionPiece, 0, 30)).(mb_strlen($thisOptionPiece)>30?'...':'');
				$tmpCommentEngOptions[] = array($idx,$thisOptionPieceShort);
			}
			$jsOption = str_replace(chr(13), ' ', $thisComment['Value']); // replace carriage return
			$jsOption = str_replace("\n","\\n",$jsOption); // replace new line
			$jsOption = str_replace('"','\\"',$jsOption); // addslashes to double quote
			$jsEngObjPieces[] = '"'.$jsOption.'"';
			$idx++; 
		}
		$idx=0;
		foreach	((array)$CommentChiArr as $thisComment)
		{
			$tmpOption = explode("\n",$thisComment['Display']);
			$tmpCommentCode = '('.$thisComment["CommentCode"].') ';
			foreach((array)$tmpOption as $thisOptionPiece)
			{
				if ($eRCCommentSetting['ShowCompleteSentence']) {
					$thisOptionPieceShort = trim($thisOptionPiece);
				}
				else {
					$thisOptionPieceShort = $tmpCommentCode.trim(mb_substr($thisOptionPiece, 0, 36)).(mb_strlen($thisOptionPiece)>30?'...':'');
				}
				$tmpCommentOptions[] = array($idx,$thisOptionPieceShort);
				
				$tmpCommentCode = '';	// second row of comment not show comment code
			}
			$jsOption = str_replace(chr(13), ' ', $thisComment['Value']); // replace carriage return
			$jsOption = str_replace("\n","\\n",$jsOption); // replace new line
			$jsOption = str_replace('"','\\"',$jsOption); // addslashes to double quote
			$jsObjPieces[] =  '"'.$jsOption.'"';
			$idx++;
		}
		$select_comment = $linterface->GET_SELECTION_BOX($tmpCommentOptions, 'name="Comment_b5[]" id="Comment_b5" size="21" multiple="multiple" onchange="" style="display:none"', '');
		$select_comment_eng = $linterface->GET_SELECTION_BOX($tmpCommentEngOptions, 'name="Comment_en[]" id="Comment_en" size="21" multiple="multiple" onchange="" style="display:block"', '');

		# build jsObj for return comment by value 
		$jsObjectStr = "new Array(".implode(",",(array)$jsObjPieces).")";
		$jsEngObjectStr = "new Array(".implode(",",(array)$jsEngObjPieces).")";
		
		// Comment category filter
		if($SubjectID){
			$commentCat = $lreportcard->GET_COMMENT_CAT($SubjectID);
			$array_cat_name = $commentCat;
			$array_cat_data = $commentCat;
			$select_cat = getSelectByValueDiffName($array_cat_data, $array_cat_name, "name='cat' onChange='jsCatFilter()'",$cat,1,0, $eReportCard['AllCategories']);
		}
		
		// Default language - teacher comment select [Case #F74774]
		if(!isset($Language)){
			$eng_Lang = $intranet_session_language=="en";
		} else {
			$eng_Lang = $Language=="eng";
		}
?>

<script language="javascript">
var chiComment = <?=$jsObjectStr?>;
var engComment = <?=$jsEngObjectStr?>;

function isExists(obj2,val){
	for(j=0;j<obj2.options.length;j++) {
		if(obj2.options[j].value==val) return true;
	}
	return false;
}
function AddOptions(){
	
	var jObj = returnActiveDropDownListObj();
	options = jObj.children("option:selected"); //get selected options
	var lang = $("[name=Language]:checked").val(); 
	if(lang=='eng')
		var comment = engComment;
	else
		var comment = chiComment;
     par = opener.window;
     parObj = opener.window.document.form1.elements['<?=$fieldname?>'];

	 var optionAdded = new Array();
	 
	 lastadded = '';
     jQuery.each(options,function(idx,optionObj){
     	if($(optionObj).val()!=lastadded) //if the comment have not been added in this round 
     	{
     		addtext = '';
//     		if (Trim(parObj.value) != '')
//     			addtext += "\n";
     		addtext += Trim(comment[$(optionObj).val()]); 
			par.checkOptionAdd(parObj, addtext, '<?=$CommentType?>');
			lastadded = $(optionObj).val();	
		}
     });
     parObj.focus();
     
    // options.remove();
}

function SelectAll() {
	
	var jObj = returnActiveDropDownListObj();
	jObj.children().attr("selected",true);
}

function jsChangeLang() {
	var lang_eng = document.getElementById("lang_eng");
	var lang_chi = document.getElementById("lang_chi");
	var CommentSelectCell = document.getElementById("CommentSelectCell");
	
	if(lang_eng.checked) 
	{
		$("#Comment_b5").hide();
		$("#Comment_en").show();
	}
	else
	{
		$("#Comment_b5").show();
		$("#Comment_en").hide();
	}
}


function returnActiveDropDownListObj(){
	var lang = $("[name=Language]:checked").val();
	if(lang=='eng')
		jObj = $("#Comment_en")
	else
		jObj = $("#Comment_b5")
	return jObj;
}

$().ready(function(){
	jsChangeLang();
	$("#Comment_en").change(SelectOptions);
	$("#Comment_b5").change(SelectOptions);
//	$("#Comment_en").bind('mousedown',SelectOptions);
//	$("#Comment_b5").bind('mousedown',SelectOptions);
});

function SelectOptions(evt)
{
	//loop each selected options to found if there are any other options with the same value with it
	//if so, also select those options (as they are the same multiple line comment )
	var jObj = returnActiveDropDownListObj();
	jQuery.each(jObj.children("option:selected"),function(idx,selectedOption){
		 thisVal = $(selectedOption).val(); 
		 jObj.children("[value="+thisVal+"]").attr("selected",true);
     })
}

function jsCatFilter(cat) {
	document.form1.submit();
}

</script>
<br />
<form name="form1" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class='tabletext'>
						<?=$eReportCard['Language']?>:
					</td>
					<td width="80%" class='tabletext'>
						<!--<input type="radio" name="Language" id="lang_eng" value="eng" onclick="jsChangeLang()" <?=(!isset($Language) || $Language=="eng") ? "checked" : ""?> />-->
						<input type="radio" name="Language" id="lang_eng" value="eng" onclick="jsChangeLang()" <?=$eng_Lang? "checked" : ""?>
						<label for="lang_eng"><?=$i_general_english?></label> 
						<!--<input type="radio" name="Language" id="lang_chi" value="chi" onclick="jsChangeLang()" <?=($Language=="chi") ? "checked" : ""?> />-->
						<input type="radio" name="Language" id="lang_chi" value="chi" onclick="jsChangeLang()" <?=!$eng_Lang? "checked" : ""?> />
						<label for="lang_chi"><?=$i_general_chinese?></label>
					</td>
				</tr>
				
				<?if($SubjectID){?>
				<tr>
					<td valign="top" nowrap="nowrap" class='tabletext'>
						<?=$eReportCard['Category']?>:
					</td>
					<td valign="top" nowrap="nowrap" class='tabletext'>
						<?=$select_cat?>
					</td>
				</tr>
				<?}?>
				<tr>
					<td valign="top" nowrap="nowrap" class='tabletext'>
						<?=$eReportCard['CommentList']?>:
					</td>
					<td width="80%">
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td id="CommentSelectCell"><?= $select_comment_eng ?><?= $select_comment?></td>
								<td valign="bottom">
									<table width="100%" border="0" cellspacing="0" cellpadding="6">
										<tr> 
											<td align="left"> 
												<?= $linterface->GET_SMALL_BTN($button_add, "button", "AddOptions()")?>
											</td>
										</tr>
										<tr> 
											<td align="left"> 
												<?= $linterface->GET_SMALL_BTN($button_select_all, "button", "SelectAll(); return false;")?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
                	<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
				<tr>
					<td align="center" colspan="2">
						<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="fieldname" value="<?php echo $fieldname; ?>" />
</form>
</br />
<?
		intranet_closedb();
        $linterface->LAYOUT_STOP();
    } else {
    ?>
You have no priviledge to access this page.
    <?
    }
} else {
?>
You have no priviledge to access this page.
<?
}
?>