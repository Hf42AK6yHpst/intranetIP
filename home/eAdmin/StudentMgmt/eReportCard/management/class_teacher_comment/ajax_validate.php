<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_comment.php");

$lreportcard = new libreportcardcustom();
$linterface = new interface_html();
$lreportcard_comment = new libreportcard_comment();

$lreportcard->hasAccessRight();

$Action = $_REQUEST['Action'];

if ($Action == 'Import_Class_Teacher_Comment') {
	include_once($PATH_WRT_ROOT."includes/libimporttext.php");
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
	
	$CommentMaxLength = $lreportcard->Get_Class_Teacher_Comment_MaxLength($ReportID);
	
	$ClassLevelID = $_REQUEST['ClassLevelID'];
	$ClassID = $_REQUEST['ClassID'];
	$ReportID = $_REQUEST['ReportID'];
	$TargetFilePath = trim(urldecode(stripslashes($_REQUEST['TargetFilePath'])));
	
	$lexport = new libexporttext();
	$limport = new libimporttext();
	
	### Delete the temp data first
	$SuccessArr['DeleteOldTempData'] = $lreportcard_comment->Delete_Import_Temp_Class_Teacher_Comment_Data($ReportID);
	
	### Include js libraries
	$jsInclude = '';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
	echo $jsInclude;
	
	### get accessible student array
	$ClassArr = $lreportcard->Get_User_Accessible_Class($ClassLevelID);
	if ($ClassID != '') {
		$ClassIDArr[0] = $ClassID;
	} else {
		$ClassIDArr = Get_Array_By_Key($ClassArr, 'ClassID');
	}
	
	$AccessibleStudentArray = array();
	foreach ($ClassIDArr as $thisClassID) {
		$AccessibleStudentArray = array_merge($AccessibleStudentArray, $lreportcard->GET_STUDENT_BY_CLASS($thisClassID));
	}
	$AccessibleStudentIDArray = Get_Array_By_Key($AccessibleStudentArray, 'UserID');
	
	### Get Csv data
	$DefaultCsvHeaderArr = $lreportcard_comment->Get_Class_Teacher_Comment_Csv_Header_Title();
	$ColumnPropertyArr = $lreportcard_comment->Get_Class_Teacher_Comment_Csv_Header_Property($forGetAllCsvContent=0);
	$CsvHeaderValidationArr = $lexport->GET_EXPORT_HEADER_COLUMN($DefaultCsvHeaderArr, $ColumnPropertyArr);
	$CsvHeaderDisplayArr = $lexport->GET_EXPORT_HEADER_COLUMN($DefaultCsvHeaderArr, $ColumnPropertyArr, $AddBracket=0);
	
	$ColumnPropertyAllInfoArr = $lreportcard_comment->Get_Class_Teacher_Comment_Csv_Header_Property($forGetAllCsvContent=1);
	$CsvDataArr = $limport->GET_IMPORT_TXT($TargetFilePath, "", $lineBreakReplacement='<!-br->', $CsvHeaderValidationArr, $ColumnPropertyAllInfoArr);
    $CsvHeaderArr = array_shift($CsvDataArr);	// Remove Chinese Title
	$numOfCsvData = count($CsvDataArr);
	
	### Get Student Info Asso Array
	$ReportInfoArr = $lreportcard->returnReportTemplateBasicInfo($ReportID);
	$ClassLevelID = $ReportInfoArr['ClassLevelID'];
	$StudentInfoArr = $lreportcard->GET_STUDENT_BY_CLASSLEVEL('', $ClassLevelID, $ClassID);
	
	# $ClassNameNumberAssoc[$ClassName][$ClassNumber] = $UserID
	$ClassNameNumberAssoArr = BuildMultiKeyAssoc($StudentInfoArr, array("ClassTitleEn", "ClassNumber"), "UserID", 1);
	# $UserLoginAssoc[$UserLogin] = $UserID
	$UserLoginAssoArr = BuildMultiKeyAssoc($StudentInfoArr, array("UserLogin"), "UserID", 1);
	# $WebSamsAssoc[$WebSAMSRegNo] = $UserID
	$WebSAMSRegNoAssoArr = BuildMultiKeyAssoc($StudentInfoArr, array("WebSAMSRegNo"), "UserID", 1);
	
	
	### Variable Initialization
	$NumOfSuccessRow = 0;
	$NumOfErrorRow = 0;
	$ErrorRowInfoArr = array();		//$ErrorRowInfoArr[$RowNumber][ErrorField] = array(ErrorMsg)
	$InsertValueArr = array();
	$RowNumber = 2;
	
	for ($i=0; $i<$numOfCsvData; $i++) {
		
		$thisColumnCount = 0;
		$thisWebSAMSRegNo = trim($CsvDataArr[$i][$thisColumnCount++]);
		$thisClassName = trim($CsvDataArr[$i][$thisColumnCount++]);
		$thisClassNumber = trim($CsvDataArr[$i][$thisColumnCount++]);
		$thisStudentName = trim($CsvDataArr[$i][$thisColumnCount++]);
		$thisCommentContent = trim($CsvDataArr[$i][$thisColumnCount++]);
		if ($eRCTemplateSetting['ClassTeacherComment_AdditionalComment'])
			$thisAdditionalComment = trim($CsvDataArr[$i][$thisColumnCount++]);
		if ($eRCTemplateSetting['ClassTeacherComment_Conduct'])
			$thisConduct = trim($CsvDataArr[$i][$thisColumnCount++]);
			
		### Check Student Identity
		if ($thisWebSAMSRegNo != '') {
			$thisStudentID = $WebSAMSRegNoAssoArr[$thisWebSAMSRegNo];
		} else {
			$thisStudentID = '';
		}
		if ($thisStudentID == '') {
			$thisStudentID = $ClassNameNumberAssoArr[$thisClassName][$thisClassNumber];
			if ($thisStudentID == '') {
				$ErrorRowInfoArr[$RowNumber]['WebSAMSRegNo'] = 1;
				$ErrorRowInfoArr[$RowNumber]['ClassName'] = 1;
				$ErrorRowInfoArr[$RowNumber]['ClassNumber'] = 1;
				(array)$ErrorRowInfoArr[$RowNumber]['ErrorMsgArr'][] = '- '.$Lang['General']['ImportWarningArr']['WrongWebSamsRegNo'];
			}
		}
		
		### Check Accessibility
		if ($thisStudentID !='' && !in_array($thisStudentID, $AccessibleStudentIDArray)) {
			$ErrorRowInfoArr[$RowNumber]['NotAccessible'] = 1;
			(array)$ErrorRowInfoArr[$RowNumber]['ErrorMsgArr'][] = '- '.intranet_htmlspecialchars($Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['NotAccessibleStudent']);
		}
		
		### Check Comment Max Length
		if ($CommentMaxLength != -1) {
			$CommentlineBreakCount = substr_count($thisCommentContent, '<!-br->');
			$CommentMaxLengthwithLineBreak = $CommentMaxLength + $CommentlineBreakCount*mb_strlen('<!-br->') - $CommentlineBreakCount;
			
			if(mb_strlen($thisCommentContent, "utf-8") > $CommentMaxLengthwithLineBreak) {
				$ErrorRowInfoArr[$RowNumber]['Comment'] = 1;
				(array)$ErrorRowInfoArr[$RowNumber]['ErrorMsgArr'][] = '- '.$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CommentExceedCharacterLimit'];
			}
			if ($eRCTemplateSetting['ClassTeacherComment_AdditionalComment']) {
				$AddiCommentlineBreakCount = substr_count($thisAdditionalComment, '<!-br->');
				$AddiCommentMaxLengthwithLineBreak = $CommentMaxLength + $AddiCommentlineBreakCount*mb_strlen('<!-br->') - $AddiCommentlineBreakCount;
			
				if(mb_strlen($thisAdditionalComment, "utf-8") > $AddiCommentMaxLengthwithLineBreak) {
					$ErrorRowInfoArr[$RowNumber]['AdditionalComment'] = 1;
					(array)$ErrorRowInfoArr[$RowNumber]['ErrorMsgArr'][] = '- '.$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CommentExceedCharacterLimit'];
				}
			}
		}
		
		### Validate Conduct
		if ($eRCTemplateSetting['ClassTeacherComment_Conduct']) {
			include_once($PATH_WRT_ROOT.'includes/libreportcard2008_extrainfo.php');
			$lreportcard_extrainfo = new libreportcard_extrainfo();
			
			$ConductInfo = $lreportcard_extrainfo->Get_Conduct();
			$ConductInfo = BuildMultiKeyAssoc($ConductInfo, "Conduct", "ConductID", 1);
			
			if(empty($thisConduct))
			{
				$StudentConductArr[$thisStudentID] = "";
			}
			else if($ConductID = $ConductInfo[$thisConduct]) // skip those not in conduct bank
			{
				$StudentConductArr[$thisStudentID] = $ConductID;
			}
			else
			{
				$ErrorRowInfoArr[$RowNumber]['Conduct'] = 1;
				(array)$ErrorRowInfoArr[$RowNumber]['ErrorMsgArr'][] = '- '.$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['InvalidConduct'];
			}
		}
		
		
		
		
		### Insert DB Statement
		$thisWebSAMSRegNo = $lreportcard -> Get_Safe_Sql_Query($thisWebSAMSRegNo);
		$thisStudentName = $lreportcard -> Get_Safe_Sql_Query($thisStudentName);
		$thisClassName = $lreportcard->Get_Safe_Sql_Query($thisClassName);
		$thisClassNumber = $lreportcard->Get_Safe_Sql_Query($thisClassNumber);
		$thisCommentContent = $lreportcard->Get_Safe_Sql_Query($thisCommentContent);
		$thisAdditionalComment = $lreportcard->Get_Safe_Sql_Query($thisAdditionalComment);
		$thisConduct = $lreportcard->Get_Safe_Sql_Query($thisConduct);
		
		
		$InsertValueArr[] = "('".$_SESSION['UserID']."', '".$RowNumber."', '".$thisClassName."', '".$thisClassNumber."', 
							'".$thisWebSAMSRegNo."', '".$thisStudentID."', '".$thisStudentName."', '".$thisCommentContent."', 
							'".$thisAdditionalComment."', '".$thisConduct."', '".$ReportID."', now())";
		
		### Count success / failed records	
		if (!isset($ErrorRowInfoArr[$RowNumber])) {
			$NumOfSuccessRow++;
		}
		else {
			$NumOfErrorRow++;
		}
				
		### Update Processing Display
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.document.getElementById("SuccessCountDiv").innerHTML = "'.$NumOfSuccessRow.'";';
			$thisJSUpdate .= 'window.parent.document.getElementById("FailCountDiv").innerHTML = "'.$NumOfErrorRow.'";';
			$thisJSUpdate .= 'window.parent.document.getElementById("BlockUI_Processed_Number_Span").innerHTML = "'.($RowNumber - 1).'";';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
		
		Flush_Screen_Display(1);
		
		$RowNumber++;
	}
	
	
	### Insert the temp records into the temp table
	if (count($InsertValueArr) > 0) {
		$SuccessArr['InsertTempRecords'] = $lreportcard_comment->Insert_Import_Temp_Class_Teacher_Comment_Data($InsertValueArr);
	}
	
	
	
	### Display Error Result Table if there are any
	$numOfErrorRow = count($ErrorRowInfoArr);
	if($numOfErrorRow > 0)
	{
		### Update Processing Display
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.document.getElementById("BlockUI_Progress_Span").innerHTML = "'.$Lang['SysMgr']['SubjectClassMapping']['GeneratingErrorsInfo'].'...";';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
		Flush_Screen_Display(1);
		
		### Get data of the error rows
		$ErrorRowArr = array_keys($ErrorRowInfoArr);
		$ErrorRowDataArr = $lreportcard_comment->Get_Temp_Class_Teacher_Comment_Data($ReportID, $ErrorRowArr);
		
		### Build Error Info Table
		$CsvHeaderDisplayArr = Get_Lang_Selection($CsvHeaderDisplayArr[1], $CsvHeaderDisplayArr[0]);
		$numOfCsvHeader = count($CsvHeaderDisplayArr);
		$x .= '<table class="common_table_list_v30 view_table_list_v30">';
			$x .= '<thead>';
				$x .= '<tr>';
					$x .= '<th width="10">'.$Lang['General']['ImportArr']['Row'].'</th>';
					for ($i=0; $i<$numOfCsvHeader; $i++)
					{
						$thisDisplay = $CsvHeaderDisplayArr[$i];					
						$x .= '<th>'.$thisDisplay.'</th>';
					}
					$x .= '<th>'.$Lang['General']['Remark'].'</th>';
				$x .= '</tr>';
			$x .= '</thead>';
			
			$x .= '<tbody>';
				for ($i=0; $i<$numOfErrorRow; $i++)
				{
					$_RowNumber = $ErrorRowDataArr[$i]['RowNumber'];
					$_ClassName = intranet_htmlspecialchars($ErrorRowDataArr[$i]['ClassName']);
					$_ClassNumber = intranet_htmlspecialchars($ErrorRowDataArr[$i]['ClassNumber']);
					$_WebSAMSRegNo = intranet_htmlspecialchars($ErrorRowDataArr[$i]['WebSAMSRegNo']);
					$_StudentName = intranet_htmlspecialchars($ErrorRowDataArr[$i]['StudentName']);
					$_CommentContent = intranet_htmlspecialchars($ErrorRowDataArr[$i]['CommentContent']);
					$_CommentContent = str_replace(intranet_htmlspecialchars('<!-br->'), '<br/>', $_CommentContent);
					$_AdditionalComment = intranet_htmlspecialchars($ErrorRowDataArr[$i]['AdditionalComment']);
					$_AdditionalComment = str_replace(intranet_htmlspecialchars('<!-br->'), '<br/>', $_AdditionalComment);
					$_Conduct = intranet_htmlspecialchars($ErrorRowDataArr[$i]['Conduct']);
					
					$_ColumnCount = 0;
					
					# Error Remarks
					$_ErrorInfoArr = $ErrorRowInfoArr[$_RowNumber];
					$_ErrorDisplayArr = $_ErrorInfoArr['ErrorMsgArr'];
					$_ErrorDisplayText = implode('<br />', (array)$_ErrorDisplayArr);
					
					$css_i = ($i % 2) ? "2" : "";
					$x .= '<tr style="vertical-align:top">';
						$x .= '<td>'.$_RowNumber.'</td>';
						
						// WebSAMSRegNo
						if ($_WebSAMSRegNo == '')
							$_WebSAMSRegNo = "&nbsp;";
						else 
							$_WebSAMSRegNo = ($_ErrorInfoArr['WebSAMSRegNo']==1)? '<font color="red">'.$_WebSAMSRegNo.'</font>' : $_WebSAMSRegNo;
						$x .= '<td>'.$_WebSAMSRegNo.'</td>';
						
						// Class Name
						if ($_ClassName == '')
							$_ClassName = "&nbsp;";
						else
							$_ClassName = ($_ErrorInfoArr['ClassName']==1)? '<font color="red">'.$_ClassName.'</font>' : $_ClassName;
						$x .= '<td>'.$_ClassName.'</td>';
						
						// Class Number
						if ($_ClassNumber == '')
							$_ClassNumber = "&nbsp;";
						else
							$_ClassNumber = ($_ErrorInfoArr['ClassNumber']==1)? '<font color="red">'.$_ClassNumber.'</font>' : $_ClassNumber;
						$x .= '<td>'.$_ClassNumber.'</td>';
						
						// Student Name
						if ($_StudentName == '')
							$_StudentName = "&nbsp;";
						$x .= '<td>'.$_StudentName.'</td>';
						
						// Comment Content
						if ($_CommentContent == '')
							$_CommentContent = "&nbsp;";
						else
							$_CommentContent = ($_ErrorInfoArr['Comment']==1)? '<font color="red">'.$_CommentContent.'</font>' : $_CommentContent;							
						$x .= '<td>'.$_CommentContent.'</td>';
							
						if ($eRCTemplateSetting['ClassTeacherComment_AdditionalComment']) {
							// Additional Comment
							if ($_AdditionalComment == '')
								$_AdditionalComment = "&nbsp;";
							else
								$_AdditionalComment = ($_ErrorInfoArr['AdditionalComment']==1)? '<font color="red">'.$_AdditionalComment.'</font>' : $_AdditionalComment;
							$x .= '<td>'.$_AdditionalComment.'</td>';
						}
						
						if ($eRCTemplateSetting['ClassTeacherComment_Conduct']) {
							// Conduct
							if ($_Conduct == '')
								$_Conduct = "&nbsp;";
							else
								$_Conduct = ($_ErrorInfoArr['Conduct']==1)? '<font color="red">'.$_Conduct.'</font>' : $_Conduct;
							$x .= '<td>'.$_Conduct.'</td>';
						}
						
						// Error Details
						if ($_ErrorDisplayText == '')
								$_ErrorDisplayText = "&nbsp;";
						$x .= '<td><font color="red">'.$_ErrorDisplayText.'</font></td>';
					$x .= '</tr>';
				}
			$x .= '</tbody>';
		$x .= '</table>';
	}
			
	
	
	$ErrorCountDisplay = ($NumOfErrorRow > 0) ? "<font color=\"red\">".$NumOfErrorRow."</font>" : $NumOfErrorRow;
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= 'window.parent.document.getElementById("ErrorTableDiv").innerHTML = \''.$x.'\';';
		$thisJSUpdate .= 'window.parent.document.getElementById("SuccessCountDiv").innerHTML = \''.$NumOfSuccessRow.'\';';
		$thisJSUpdate .= 'window.parent.document.getElementById("FailCountDiv").innerHTML = \''.$ErrorCountDisplay.'\';';
		
		if ($NumOfErrorRow == 0) {
			$thisJSUpdate .= '$("input#ContinueBtn", window.parent.document).removeClass("formbutton_disable").addClass("formbutton").attr("disabled", "");';
		}
		
		$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
	
}


intranet_closedb();

?>