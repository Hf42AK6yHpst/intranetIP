<?php
# Modifing by 

/*********************************************************
 * Modification Log
 * 
 * 20130711 Roy:
 * 		- Add $ClassID == '' case to import all accessible class data in form
 * 		- modified URL location: added case to go back to index.php
 * 
 ********************************************************/

$PageRight = "TEACHER";

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	}
	else {
		$lreportcard = new libreportcard();
	}
	
	if ($lreportcard->hasAccessRight())
	{
		$limport = new libimporttext();
		
		$format_array = array("WebSAMSRegNumber", "Class Name", "Class Number", "Student Name", "Comment Content");
		if ($eRCTemplateSetting['ClassTeacherComment_AdditionalComment']) {
			$format_array[] = "Additional Comment";
		}
		if ($eRCTemplateSetting['ClassTeacherComment_Conduct']) {
			$format_array[] = "Conduct";
		}
		
		$li = new libdb();
		
		$filepath = $userfile;
		$filename = $userfile_name;
		
		$CommentMaxLength = $lreportcard->Get_Class_Teacher_Comment_MaxLength($ReportID);
		
		if($eRCTemplateSetting['ClassTeacherComment_Conduct'])
		{
			include_once($PATH_WRT_ROOT.'includes/libreportcard2008_extrainfo.php');
			$lreportcard_extrainfo = new libreportcard_extrainfo();
			
			$ConductInfo = $lreportcard_extrainfo->Get_Conduct();
			$ConductInfo = BuildMultiKeyAssoc($ConductInfo, "Conduct", "ConductID", 1);
		}
		
		# 4 parameters need to be passed to other pages
		$parmeters = "ClassID=$ClassID&ReportID=$ReportID&ClassLevelID=$ClassLevelID";
		
		if($filepath=="none" || $filepath == "" || !isset($ReportID, $ClassID)){          # import failed
		    header("Location: import.php?$parmeters&Result=import_failed2");
		    exit();
		}
		else {
		    if($limport->CHECK_FILE_EXT($filename))
		    {
				# read file into array
				# return 0 if fail, return csv array if success
				$data = $limport->GET_IMPORT_TXT($filepath, $incluedEmptyRow=0, $lineBreakReplacement='<!--LineBreak-->');
				#$toprow = array_shift($data);                   # drop the title bar
				
				$limport->SET_CORE_HEADER($format_array);
				if (!$limport->VALIDATE_HEADER($data, true) || !$limport->VALIDATE_DATA($data)) {
					header("Location: import.php?$parmeters&Result=import_failed");
					exit();
				}
				
				# check user access right
				$ClassArr = $lreportcard->Get_User_Accessible_Class($ClassLevelID);
				if ($ClassID != '') {
					$ClassIDArr[0] = $ClassID;
				}
				else {
					$ClassIDArr = Get_Array_By_Key($ClassArr, 'ClassID');
				}
				
				$StudentArray = array();
				foreach ($ClassIDArr as $thisClassID) {
					$StudentArray = array_merge($StudentArray, $lreportcard->GET_STUDENT_BY_CLASS($thisClassID));
				}
//				$StudentArray = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $ClassID);
				
				$StudentSize = sizeof($StudentArray);
				if ($StudentSize == 0) {
					header("Location: import.php?$parmeters&Result=import_failed");
					exit();
				}
				
				$StudentIDList = "";
				for($i=0; $i<$StudentSize; $i++)
					$StudentIDArray[] = $StudentArray[$i]["UserID"];
				$StudentIDList = implode(",", $StudentIDArray);
				
				$table = $lreportcard->DBName.".RC_MARKSHEET_COMMENT";
				
				# get a list of exist comment record
				$sql = "SELECT StudentID FROM $table WHERE 
						SubjectID = '' AND 
						ReportID = '$ReportID' AND 
						StudentID IN ($StudentIDList)";
				$existStudents = $lreportcard->returnVector($sql);
				
				array_shift($data);
				
			    $values = "";
			    $delim = "";
				$successUpdatedCount = 0;
				
				$webSAMSMapping = BuildMultiKeyAssoc($StudentArray, "WebSAMSRegNo", "UserID", 1);
 				$classInfoMapping = BuildMultiKeyAssoc($StudentArray, array("ClassName", "ClassNumber"), "UserID", 1);
 				
 				for ($i=0; $i<sizeof($data); $i++)
 				{
					if (empty($data[$i]))
						continue;
					
					list($webSAMSRegNumber,$className,$classNum,$studentName,$comment,$additionalComment, $Conduct) = $data[$i];
					
					# check webSAMSRegNumber, or ClassName+ClassNumber if not exist
					if ($webSAMSRegNumber != '') {
						$studentID = $webSAMSMapping[$webSAMSRegNumber];
						if ($studentID == '') {
							$studentID = $classInfoMapping[$className][$classNum];
						}
					}
					else {
						// $webSAMSMapping[''] may not be empty => need special handling of no websams case
						$studentID = $classInfoMapping[$className][$classNum];
					}
					
					if ($studentID == '') {
						// no such student, log the webSAMSRegNumber
						$WrongWebSAMSRegNumberArr[] = $webSAMSRegNumber;
						continue;
					}		
					
					# if comment already exist, put it into array for UPDATE
					//$finalComment = $lreportcard->Get_Safe_Sql_Query(strip_tags(str_replace('<!--LineBreak-->', "\n", $comment)));
					//$finalAdditionalComment = $lreportcard->Get_Safe_Sql_Query(strip_tags(str_replace('<!--LineBreak-->', "\n", $additionalComment)));
					$finalComment = strip_tags(str_replace('<!--LineBreak-->', "\n", $comment));
					$finalAdditionalComment = strip_tags(str_replace('<!--LineBreak-->', "\n", $additionalComment));
					
					# sync both (edit and import)
					if ($CommentMaxLength != -1) {
						if(mb_strlen($finalComment, "utf-8") > $CommentMaxLength) {
							$finalComment = mb_substr($finalComment,0,$CommentMaxLength, "utf-8");
							$CutRow[] = ($i+1);
						}
						if(mb_strlen($finalAdditionalComment, "utf-8") > $CommentMaxLength) {
							$finalAdditionalComment = mb_substr($finalAdditionalComment,0,$CommentMaxLength, "utf-8");
							if (!in_array(($i+1), (array)$CutRow)) {
								$CutRow[] = ($i+1);
							}
						}
					}
					
					if($eRCTemplateSetting['ClassTeacherComment_Conduct'])
					{
						if(empty($Conduct))
						{
							$StudentConductArr[$studentID] = "";
						}
						else if($ConductID = $ConductInfo[$Conduct]) // skip those not in conduct bank
						{
							$StudentConductArr[$studentID] = $ConductID;
						}
						else
						{
							$WrongConductRow[] = ($i+1);
						}
					}
					
					if (in_array($studentID, $existStudents)) {
						$updateTeacherComment[$studentID]['Comment'] = $finalComment;
						$updateTeacherComment[$studentID]['AdditionalComment'] = $finalAdditionalComment;
					}	
					else {
						$insertTeacherComment[$studentID]['Comment'] = $finalComment;
						$insertTeacherComment[$studentID]['AdditionalComment'] = $finalAdditionalComment;
					}
			    }
			    
				$success = array();
				
				# INSERT new comment
				if (isset($insertTeacherComment)) {
					foreach($insertTeacherComment as $k => $v) {
						if (in_array($k, $StudentIDArray)) {
							$values[] = "('$k', '', '$ReportID', '".$lreportcard->Get_Safe_Sql_Query($v['Comment'])."', '".$lreportcard->Get_Safe_Sql_Query($v['AdditionalComment'])."', '".$lreportcard->uid."', NOW(), NOW())";
							$successUpdatedCount++;
						}
					}
					$values = implode(",", $values);
					$field = "StudentID, SubjectID, ReportID, Comment, AdditionalComment, TeacherID, DateInput, DateModified";
				    $sql = "INSERT INTO $table ($field) VALUES $values";
					$success[] = $li->db_db_query($sql);
				}	
				
				# UPDATE existing comment
				if (isset($updateTeacherComment)) {
					foreach($updateTeacherComment as $k => $v) {
						$sql = "UPDATE $table SET Comment = '".$lreportcard->Get_Safe_Sql_Query($v['Comment'])."', AdditionalComment = '".$lreportcard->Get_Safe_Sql_Query($v['AdditionalComment'])."', TeacherID = '".$lreportcard->uid."', DateModified = NOW() WHERE ";
						$sql .= "StudentID = '$k' AND SubjectID = '' AND ReportID = '$ReportID'";
						$success[] = $li->db_db_query($sql);
						$successUpdatedCount++;
					}
				}
				
				if($eRCTemplateSetting['ClassTeacherComment_Conduct'])
				{
					$success[] = $lreportcard_extrainfo->Insert_Update_Extra_Info("ConductID", $ReportID, $StudentConductArr);
					$WrongConductRow = implode(",",(array)$WrongConductRow); 		
				}
				
				$CutRow = implode(",",(array)$CutRow);
				
				if (in_array(false, $success)) {
					header("Location: import.php?$parmeters&Result=import_failed2");
					exit();
				}
		    }
		    else {
				header("Location: import.php?$parmeters&Result=import_failed");
				exit();
			}
		}
		intranet_closedb();
		
		if ($ClassID == "") {
			$url = "Location: index.php?$parmeters&SuccessCount=$successUpdatedCount&Result=num_records_updated";
		}
		else {
			$url = "Location: edit.php?$parmeters&SuccessCount=$successUpdatedCount&Result=num_records_updated";
		}
		if($CutRow)
			$url .= "&cut_row=$CutRow";
		if($WrongConductRow)
			$url .= "&WrongConductRow=$WrongConductRow";
		header($url);
	}
	else {
		echo "You have no priviledge to access this page.";
	}
}
else {
	echo "You have no priviledge to access this page.";
}
?>