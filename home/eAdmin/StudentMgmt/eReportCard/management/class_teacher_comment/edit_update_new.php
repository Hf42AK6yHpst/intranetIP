<?php
/* Using:
 *  
 * Modification Log:
 *  20190502 Bill:
 *  - prevent SQL Injection
 *  20190301 Bill:	[2018-1130-1440-05164]
 *  - Add Handling for Comment Selection  ($eRCTemplateSetting['Management']['ClassTeacherComment']['SelectExistingCommentOnly'])
 * 	20130703 Roy
 * 	- Add log for editing class teacher comments and conduct
 */

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";
 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();

$ReportID = IntegerSafe($_POST['ReportID']);
$ClassLevelID = IntegerSafe($_POST['ClassLevelID']);
$ClassID = IntegerSafe($_POST['ClassID']);
$CommentArr = $_POST['CommentArr'];
$AdditionalCommentArr = $_POST['AdditionalCommentArr'];

$logDataArr = array();
$logDataArr['ReportID'] = $ReportID;
$logDataArr['ClassLevelID'] = $ClassLevelID;
$logDataArr['ClassID'] = $ClassID;

### Student Info
$StudentInfoArr = $lreportcard->GET_STUDENT_BY_CLASS($ClassID, '', $isShowBothLangs=1, $withClassNumber=0, $isShowStyle=1);
$StudentIDArr = Get_Array_By_Key($StudentInfoArr, 'UserID');
$numOfStudent = count($StudentInfoArr);

### Get Comment Info
$CommentInfoArr = $lreportcard->GET_TEACHER_COMMENT($StudentIDArr, "", $ReportID);

$lreportcard->Start_Trans();
$SuccessArr = array();
for ($i=0; $i<$numOfStudent; $i++)
{
	$thisStudentID = $StudentInfoArr[$i]['UserID'];
	
	$DataArr = array();
	$DataArr['Comment'] = trim(stripslashes($CommentArr[$thisStudentID]));
	$DataArr['AdditionalComment'] = trim(stripslashes($AdditionalCommentArr[$thisStudentID]));
	
	// [2018-1130-1440-05164] Build Comment Content using CommentIDList
	if($eRCTemplateSetting['Management']['ClassTeacherComment']['SelectExistingCommentOnly'])
	{
	    $commentSeperator = '
';
	    
	    # CommentID List
	    $DataArr['CommentIDList'] = '';
	    $CommentIDList = IntegerSafe($_POST['CommentIDList'][$thisStudentID]);
	    if(!empty($CommentIDList)) {
	        $CommentIDList = array_values(array_filter(array_unique($CommentIDList)));
	        $DataArr['CommentIDList'] = implode(',', (array)$CommentIDList);
	    }
	    
	    # Comment
	    $DataArr['Comment'] = '';
	    $CommentTextDisplayArr = array();
	    foreach((array)$CommentIDList as $thisCommentID)
	    {
	        $thisCommentInfo = $lreportcard->GET_MARKSHEET_COMMENT_INFO_BY_ID($thisCommentID);
	        $thisCommentTextCh = $thisCommentInfo['Comment'];
	        $thisCommentTextEn = $thisCommentInfo['CommentEng'];
	        $CommentTextDisplayArr[] = $thisCommentTextCh.$commentSeperator.$thisCommentTextEn;
	    }
	    $DataArr['Comment'] = implode($commentSeperator, (array)$CommentTextDisplayArr);
	}
	
	$logDataArr['Comment_'.$thisStudentID] = $DataArr['Comment'];
	$logDataArr['AdditionalComment_'.$thisStudentID] = $DataArr['AdditionalComment'];
	
	if (isset($CommentInfoArr[$thisStudentID]['CommentID'])) {
		// update
		$thisCommentID = $CommentInfoArr[$thisStudentID]['CommentID'];
		$SuccessArr[$thisStudentID] = $lreportcard->Update_Teacher_Comment($thisCommentID, $DataArr);
	}
	else {
		// insert
		$DataArr['ReportID'] = $ReportID;
		$DataArr['StudentID'] = $thisStudentID;
		$DataArr['SubjectID'] = 0;
		$SuccessArr[$thisStudentID] = $lreportcard->Insert_Teacher_Comment($DataArr);
	}
}

if($eRCTemplateSetting['ClassTeacherComment_Conduct'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_extrainfo.php");
	$lreportcard_extrainfo = new libreportcard_extrainfo();
	
	$StudentConductIDArr = IntegerSafe($_POST['StudentConductIDArr']);
	$SuccessArr['Conduct'] = $lreportcard_extrainfo->Insert_Update_Extra_Info("ConductID", $ReportID, $StudentConductIDArr);
	
	for ($i=0; $i<$numOfStudent; $i++) {
		$thisStudentID = $StudentInfoArr[$i]['UserID'];
		$logDataArr['Conduct_'.$thisStudentID] = trim(stripslashes($StudentConductIDArr[$thisStudentID]));
	
	}
}

if (in_array(false, (array)$SuccessArr)) {
	$lreportcard->RollBack_Trans();
	$Result = 'update_failed';
}
else {
	$lreportcard->Commit_Trans();
	$Result = 'update';
}

### Log this action
$SuccessArr['logAction'] = $lreportcard->Insert_Year_Based_Log('Edit_Class_Teacher_Comment', $logDataArr);

intranet_closedb();

$parmeters = "ClassID=$ClassID&ReportID=$ReportID&ClassLevelID=$ClassLevelID";
header("Location:index.php?$parmeters&Result=$Result");
?>