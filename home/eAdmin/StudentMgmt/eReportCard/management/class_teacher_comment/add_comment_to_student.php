<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

if ($lreportcard->hasAccessRight()) 
{
	$ReportID = $_POST['ReportID'];
	$SelectedUserIDArr = $_POST['SelectedUserIDArr'];
	$CommentIDArr = unserialize(rawurldecode($CommentIDArr));
	
	$ReportInfoArr = $lreportcard->returnReportTemplateBasicInfo($ReportID);
	$YearID = $ReportInfoArr['ClassLevelID'];
	$SelectedUserIDArr = $lreportcard->Get_User_Array_From_Selection($SelectedUserIDArr, $YearID);
	
	$success = $lreportcard->Append_Class_Teacher_Comment($ReportID, $SelectedUserIDArr, $CommentIDArr);
	
	intranet_closedb();
	
	$ReturnMsgKey = ($success)? 'AddCommentSuccess' : 'AddCommentFailed';
	header("Location:index_comment.php?ReturnMsgKey=".$ReturnMsgKey."&Keyword=".$Keyword);
}

?>

