<?php
// Using:

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

$PageRight = "TEACHER";

if (!isset($_POST) && (!isset($comment) || !isset($commentID))) {
	header("Location:index.php");
}
if (!$plugin['ReportCard2008']) {
	intranet_closedb();
	header("Location:/index.php");
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
$lreportcard = new libreportcard();

if (!$lreportcard->hasAccessRight()) {
	intranet_closedb();
	header("Location:/index.php");
}

$table = $lreportcard->DBName.".RC_MARKSHEET_COMMENT";

$success = true;
if (sizeof($commentID) > 0)
{
	foreach ($commentID as $key => $value) {
		$value = addslashes($value);
		$sql  = "UPDATE $table SET ";
		$sql .= "Comment = '$value', TeacherID = '".$lreportcard->uid."', DateModified = NOW() ";
		$sql .= "WHERE CommentID = '$key'";
		$success = $lreportcard->db_db_query($sql);
	}
}

if (sizeof($comment) > 0)
{
	$field = "StudentID, SubjectID, ReportID, Comment, TeacherID, DateInput, DateModified";
	$sql = "INSERT INTO $table ($field) VALUES ";
	foreach ($comment as $key => $value) {
		$value = addslashes($value);
		$entries[] = "('$key', '', '$ReportID', '$value', '".$lreportcard->uid."', NOW(), NOW())";
	}
	$entries = implode(",", $entries);
	$sql .= $entries;
	$success = $lreportcard->db_db_query($sql);
}

intranet_closedb();

$Result = ($success) ? "update" : "update_failed";
$parmeters = "ClassID=$ClassID&ReportID=$ReportID&ClassLevelID=$ClassLevelID";
header("Location:index.php?$parmeters&Result=$Result");
?>

