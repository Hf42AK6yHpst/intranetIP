<?php
# Editing by

/****************************************************
 * Modification Log
 * 
 * 20171006 Bill [2017-0907-1704-15258]
 * 		- support new import mode - Append to original comment
 * 
 * 20130722 Roy
 * 		- New Import Page(Step 3), replace import.php
 * 
 *****************************************************/

@SET_TIME_LIMIT(21600);

$PageRight = "TEACHER";

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_comment.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

$lreportcard_comment = new libreportcard_comment();
$linterface = new interface_html();
$lreportcard_ui = new libreportcard_ui();
$libimport = new libimporttext();
$libfs = new libfilesystem();

$lreportcard_comment->hasAccessRight();

$ReportID = $_REQUEST['ReportID'];
$ClassLevelID = $_REQUEST['ClassLevelID'];
$ClassID = $_REQUEST['ClassID'];

$numOfCsvData = $_POST['numOfCsvData'];
$ReturnMsgKey = $_REQUEST['ReturnMsgKey'];

# Current Page
$CurrentPage = "Management_ClassTeacherComment";
$MODULE_OBJ = $lreportcard_comment->GET_MODULE_OBJ_ARR();

# Tag Information
if ($sys_custom['eRC']['Management']['ClassTeacherComment']['AddFromComment'])
{
	$TAGS_OBJ[] = array($eReportCard['ManagementArr']['ClassTeacherCommentArr']['Student'], "index.php", 1);
	$TAGS_OBJ[] = array($eReportCard['ManagementArr']['ClassTeacherCommentArr']['Comment'], "index_comment.php", 0);
}
else
{
	$TAGS_OBJ[] = array($eReportCard['Management_ClassTeacherComment'], "", 0);
}

$ReturnMsg = $Lang['General']['ReturnMessage'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

echo $lreportcard_ui->Get_Import_Class_Teacher_Comment_Step3_UI($ReportID, $ClassLevelID, $ClassID, $importMode);

### Thickbox loading message
$h_ProgressMsg = $linterface->Get_Import_Progress_Msg_Span(0, $numOfCsvData, $Lang['General']['ImportArr']['RecordsProcessed']);
?>

<script language="javascript">
$(document).ready( function() {
	Block_Document('<?=$h_ProgressMsg?>');
});

function js_Back_To_Import_Step1() {
	window.location = 'import_step1.php?ReportID=<?=$ReportID?>&ClassLevelID=<?=$ClassLevelID?>';
}

function js_Back() {
	<? if ($ClassID=='') { ?>
		window.location = 'index.php?ReportID=<?=$ReportID?>&ClassLevelID=<?=$ClassLevelID?>';
	<? }
	else { ?>
		window.location = 'edit.php?ReportID=<?=$ReportID?>&ClassLevelID=<?=$ClassLevelID?>&ClassID=<?=$ClassID?>';
	<? } ?>
}
	
function js_Go_Class_Teacher_Comment() {
	window.location = 'index.php?ReportID=<?=$ReportID?>&ClassLevelID=<?=$ClassLevelID?>';
}

function js_Go_Edit() {
	window.location = 'edit.php?ReportID=<?=$ReportID?>&ClassLevelID=<?=$ClassLevelID?>&ClassID=<?=$ClassID?>';
}
</script>

<br />
<br />

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>