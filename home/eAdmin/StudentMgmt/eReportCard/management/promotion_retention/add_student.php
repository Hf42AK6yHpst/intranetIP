<?php
//Editing by 
/*
 * 2013-04-30 Carlos: Created
 */
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_cust.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'] || !$eRCTemplateSetting['PromoteRetainQuit'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcard2008_cust();
} else {
	$lreportcard = new libreportcard();
}

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lreportcard_ui = new libreportcard_ui();
$linterface = new interface_html();
$fcm_ui = new form_class_manage_ui();

$CurrentPage = "Management_PromotionRetention";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();


$FormArr = $lreportcard->GET_ALL_FORMS(1);
if(count($FormArr) > 0) {	
	# Control default display table row when page loaded
	$ClassRowDisplay = "";
	$SubjectRowDisplay = "";
	$StudentRowDisplay = "";
	$ClassRankingRowDisplay = "";
	$ShowSubjectComponentRowDisplay = "";
						
	# Filters - By Form (ClassLevelID)
	$ClassLevelID = ($ClassLevelID == "") ? $FormArr[0][0] : $ClassLevelID;
	$ClassLevelName = $lreportcard->returnClassLevel($ClassLevelID);
	$FormSelection = $linterface->GET_SELECTION_BOX($FormArr, "name='ClassLevelID' class='' onchange='loadReportList(this.options[this.selectedIndex].value)'", "", $ClassLevelID);
	
	# Filters - By Class (ClassID)
	$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
	//$allClassesOption = array(0=> array("", $eReportCard['AllClasses']));
	//$ClassArr = array_merge($allClassesOption, $ClassArr);
	$ClassID = ($ClassID == "") ? $ClassArr[0][0] : $ClassID;
	$ClassSelection = $linterface->GET_SELECTION_BOX($ClassArr, 'name="ClassID" id="ClassID" class="" onchange="loadStudentList(this.options[this.selectedIndex].value);" ', "", $ClassID);
	
	# Student List (StudentID)
	$RawStudentArr = $lreportcard->GET_STUDENT_BY_CLASS($ClassID);
	$StudentArr = array();
	for($i=0;$i<count($RawStudentArr);$i++) {
		$StudentArr[] = array($RawStudentArr[$i]['UserID'],$RawStudentArr[$i]['StudentName']);
	}
	$StudentSelection = $linterface->GET_SELECTION_BOX($StudentArr, 'name="StudentID[]" id="StudentID" class="" multiple="multiple" size="10"', "");
	
	
	# Filters - By Report
	$ReportTypeSelection = '';
	$ReportTypeArr = array();
	$ReportTypeOption = array();
	$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID,'','F');
	$ReportTypeOptionCount = 0;
			
	if(count($ReportTypeArr) > 0){
		for($j=0 ; $j<count($ReportTypeArr) ; $j++){
			# for checking the existence of ReportID when changing ClassLevel
			// only display report which have been generated
			$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportTypeArr[$j]['ReportID']);
			if ($reportTemplateInfo["LastGenerated"] != "0000-00-00 00:00:00" && $reportTemplateInfo["LastGenerated"] != "") {
				$ReportIDList[] = $ReportTypeArr[$j]['ReportID'];
				$ReportTypeOption[$ReportTypeOptionCount][0] = $ReportTypeArr[$j]['ReportID'];
				$ReportTypeOption[$ReportTypeOptionCount][1] = $ReportTypeArr[$j]['SemesterTitle'];
				$ReportTypeOptionCount++;
			}
		}
	}
			
	$ReportTypeSelectDisabled = "";
	if (sizeof($ReportTypeOption) == 0) {
		$ReportTypeOption[0][0] = "-1";
		$ReportTypeOption[0][1] = $eReportCard['NoReportAvailable'];
		$ReportTypeSelectDisabled = "disabled='disabled'";
	}
			
	$ReportID = ($ReportID == "" || !in_array($ReportID,$ReportIDList)) ? $ReportTypeOption[0][0] : $ReportID;
	$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
	$ReportTypeSelection = $linterface->GET_SELECTION_BOX($ReportTypeOption, 'name="ReportID" id="ReportID" class="" '.$ReportTypeSelectDisabled.' ', '', $ReportID, '');
	
	$ReportIDOrder = "";
	if (sizeof($ReportIDList) > 0) {
		sort($ReportIDList);
		$ReportIDOrder = array_search($ReportID, $ReportIDList);
	}

	# Filters - By Form Subjects
	// A Flag, called $isFormSubject, is used to identify whether currect Form has that subject or not
	$FormSubjectArr = array();
	$SubjectOption = array();
//	$SubjectOption[0] = array("", '- '.$Lang['SysMgr']['SubjectClassMapping']['Select']['Subject'].' -');
//	$count = 1;
	$count = 0;
	$isFormSubject = 0;		// Default: Assume the selected subject does not belongs to that Form(ClassLevelID)
	$SubjectFormGradingArr = array();

	// Get Subjects By Form (ClassLevelID)
	$FormSubjectArr = $lreportcard->returnSubjectwOrder($ClassLevelID, 1, '', '', '', 0, $ReportID);		
	if(!empty($FormSubjectArr)){
		foreach($FormSubjectArr as $FormSubjectID => $Data){
			if(is_array($Data)){
				foreach($Data as $FormCmpSubjectID => $SubjectName){							
					$FormSubjectID = ($FormCmpSubjectID==0) ? $FormSubjectID : $FormCmpSubjectID;
					if($FormSubjectID == $SubjectID)
						$isFormSubject = 1;
					$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $FormSubjectID,0,0,$ReportID);
					// Prepare Subject Selection Box 
					if($FormCmpSubjectID==0 && $SubjectFormGradingArr["ScaleInput"]=="M" && $SubjectFormGradingArr["ScaleDisplay"]=="M"){
						$SubjectOption[$count][0] = $FormSubjectID;
						$SubjectOption[$count][1] = $SubjectName;
						$count++;
					}
				}
			}
		}
	}
			
	// Use for Selection Box
	// $SubjectListArr[][0] : SubjectID
	// $SubjectListArr[][1] : SubjectName
	$SubjectID = ($isFormSubject) ? $SubjectID : '';
	if($SubjectID == ""){
		$SubjectID = $SubjectOption[0][0];
	}
	$SelectedSubjectName = $lreportcard->GET_SUBJECT_NAME($SubjectID);
	$SubjectSelection = $linterface->GET_SELECTION_BOX($SubjectOption, 'name="SubjectID[]" id="SubjectID" class="" multiple="multiple" size="5" ', '', $SubjectID);
	
	// Alternate sets of subjects & classes, they will be used when User selected a different Class Level
	// They will store as JS array
	$OtherSubjectSelection = array();
	$OtherStudentSelection = array();
	
	for($i=0; $i<sizeof($FormArr); $i++)
	{
		$FormSubjectArrCT = $lreportcard->returnSubjectwOrder($FormArr[$i][0], 1, $UserID, '', '', 0, $ReportID);
		$ClassArrCT = $lreportcard->returnSubjectTeacherClass($UserID, $FormArr[$i][0]);
		
		
		if(!empty($TeacherClass)) {
			$searchResult = multiarray_search($TeacherClassAry , "ClassLevelID", $FormArr[$i][0]);
			if($searchResult != "-1") {	
				$searchResult2 = multiarray_search($ClassArrCT , "ClassName", $TeacherClassAry[$searchResult]['ClassName']);
				if($searchResult2 == "-1") {
					$thisAry = array(
						"0"=>$TeacherClassAry[$searchResult]['ClassID'],
						"ClassID"=>$TeacherClassAry[$searchResult]['ClassID'],
						"1"=>$TeacherClassAry[$searchResult]['ClassName'],
						"ClassName"=>$TeacherClassAry[$searchResult]['ClassName'],
						"2"=>$TeacherClassAry[$searchResult]['ClassLevelID'],
						"ClassLevelID"=>$TeacherClassAry[$searchResult]['ClassLevelID']);
					$ClassArrCT = array_merge($ClassArrCT, array($thisAry));
				}
				$FormSubjectArrCT = $lreportcard->returnSubjectwOrder($FormArr[$i][0], 1, '', '', '', 0, 0);
			}
		}
		
		$FormSubjectArr = array();
		$SubjectOption = array();
//		$SubjectOption[0] = array("", '- '.$Lang['SysMgr']['SubjectClassMapping']['Select']['Subject'].' -');
//		$count = 1;
		$count = 0;
		$SubjectFormGradingArr = array();
		
		$FormSubjectArr = $lreportcard->returnSubjectwOrder($FormArr[$i][0], 1, '', '', '', 0, 0);
						
		if(!empty($FormSubjectArr)){
			
			foreach($FormSubjectArr as $FormSubjectID => $Data){
				if(is_array($Data)){
					
					foreach($Data as $FormCmpSubjectID => $SubjectName){						
						$FormSubjectID = ($FormCmpSubjectID==0) ? $FormSubjectID : $FormCmpSubjectID;								
						$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($FormArr[$i][0], $FormSubjectID,0,0,0);
														
						// Prepare Subject Selection Box 
//								if($FormCmpSubjectID==0 && $SubjectFormGradingArr["ScaleInput"]=="M" && $SubjectFormGradingArr["ScaleDisplay"]=="M"){
						if($FormCmpSubjectID==0){
							$SubjectOption[$count][0] = $FormSubjectID;
							$SubjectOption[$count][1] = $SubjectName;
							$count++;
						}
					}
				}
			}
		}
		
		$SubjectID = $SubjectOption[0][0];				
		$SelectedSubjectName = $lreportcard->GET_SUBJECT_NAME($SubjectID);
		$OtherSubjectSelection[$FormArr[$i][0]] = $linterface->GET_SELECTION_BOX($SubjectOption, 'name="SubjectID[]" id="SubjectID" class="" multiple="multiple" size="5"', '', $SubjectID);	
						
		# Filters - By Class (ClassID)
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($FormArr[$i][0]);
		//$ClassArr = array_merge($allClassesOption, $ClassArr);
		$ClassID = $ClassArr[0][0];
		$OtherClassSelection[$FormArr[$i][0]] = $linterface->GET_SELECTION_BOX($ClassArr, 'name="ClassID" id="ClassID" class="" onchange="loadStudentList(this.options[this.selectedIndex].value);" ', "", $ClassID);
		
		# Student List (StudentID[])
		for($j=0;$j<count($ClassArr);$j++) {
			$RawStudentList = $lreportcard->GET_STUDENT_BY_CLASS($ClassArr[$j][0]);
			$StudentList = array();
			for($k=0;$k<count($RawStudentList);$k++) {
				$StudentList[] = array($RawStudentList[$k]['UserID'],$RawStudentList[$k]['StudentName']);
			}
			$OtherStudentSelection[$ClassArr[$j][0]] = $linterface->GET_SELECTION_BOX($StudentList, 'name="StudentID[]" id="StudentID" multiple="multiple" size="10" ',"");
		}
	}
			
	// Alternate sets of Reports, they will be used when User selected a different Class Level
	// They will store as JS array
	$OtherReportTypeSelection = array();
	
	// default ReportColumn dropdown list for "No Report Available"
	$ColumnOption[0][0] = "-1";
	$ColumnOption[0][1] = $eReportCard['NoTermAvailable'];
	$ColumnSelectDisabled = 'disabled="disabled"';
	$OtherReportColumnSelection["-1"] = $linterface->GET_SELECTION_BOX($ColumnOption, 'name="ReportColumnID" id="ReportColumnID" '.$ColumnSelectDisabled.' onchange=""', '');
	
	for($i=0; $i<sizeof($FormArr); $i++) {
		$ClassLevelName = $lreportcard->returnClassLevel($FormArr[$i][0]);
		$ReportTypeArr = array();
		$ReportTypeOption = array();
		$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($FormArr[$i][0],'','F');
		$ReportTypeOptionCount = 0;
		$ReportIDList = array();
		$ReportIDOrder = 0;
		
		if(count($ReportTypeArr) > 0){
			for($j=0; $j<sizeof($ReportTypeArr); $j++){
				$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportTypeArr[$j]['ReportID']);
				if ($reportTemplateInfo["LastGenerated"] != "0000-00-00 00:00:00" && $reportTemplateInfo["LastGenerated"] != "") {
					$ReportTypeOption[$ReportTypeOptionCount][0] = $ReportTypeArr[$j]['ReportID'];
					$ReportTypeOption[$ReportTypeOptionCount][1] = $ReportTypeArr[$j]['SemesterTitle'];
					$ReportTypeOptionCount++;
					
					$ReportIDList[] = $ReportTypeArr[$j]['ReportID'];
				}
			}
			
			sort($ReportIDList);
			
			for($j=0; $j<sizeof($ReportTypeArr); $j++){
				$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportTypeArr[$j]['ReportID']);
				$ReportIDOrder = array_search($ReportTypeArr[$j]['ReportID'], $ReportIDList);
				if ($reportTemplateInfo["LastGenerated"] != "0000-00-00 00:00:00" && $reportTemplateInfo["LastGenerated"] != "") {		
					// Alternate sets of ReportColumn (Assessment, Term or Whole year)
					$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportTypeArr[$j]['ReportID']);
					$ColumnOption = array();
					
					$ColumnOption[0] = array("", $eReportCard['Template']['OverallCombined']);
					$countColumn = 1;
					if (sizeof($ColumnTitleAry) > 0) {
						foreach ($ColumnTitleAry as $columnID => $columnTitle) {
							$ColumnOption[$countColumn][0] = $columnID;
							$ColumnOption[$countColumn][1] = $columnTitle;
							$countColumn++;
						}
					}
					
					$OtherReportColumnSelection[$ReportTypeArr[$j]['ReportID']] = $linterface->GET_SELECTION_BOX($ColumnOption, 'name="ReportColumnID" id="ReportColumnID" class="" ', '');
					
					$reportTypeArr[$ReportTypeArr[$j]['ReportID']] = $reportTemplateInfo['Semester'];
					$reportCalculationOrder = $lreportcard->returnReportTemplateCalculation($ReportTypeArr[$j]['ReportID']);
					$calculationOrderArr[$ReportTypeArr[$j]['ReportID']] = $reportCalculationOrder;
					
				}
			}
		}
		
		$ReportTypeSelectDisabled = "";
		if (sizeof($ReportTypeOption) == 0) {
			$ReportTypeOption[0][0] = "-1";
			$ReportTypeOption[0][1] = $eReportCard['NoReportAvailable'];
			$ReportTypeSelectDisabled = 'disabled="disabled"';
		}
		
		$OtherReportTypeSelection[$FormArr[$i][0]] = $linterface->GET_SELECTION_BOX($ReportTypeOption, 'name="ReportID" id="ReportID" '.$ReportTypeSelectDisabled.' ', '');
	}
}

$promotionStatusSelectionAry = array();
$promotionStatusSelectionAry[] = array('1',$eReportCard['PromoteRetainQuit']['PromotionWithFailedSubject']);
$promotionStatusSelectionAry[] = array('2',$eReportCard['PromoteRetainQuit']['GradeRetention']);
$promotionStatusSelectionAry[] = array('3',$eReportCard['PromoteRetainQuit']['QuitSchool']);
$PromotionStatusSelection = getSelectByArray($promotionStatusSelectionAry,' id="PromotionStatus" name="PromotionStatus" onchange="toggleSubjectList(this.value);" ', '', 0, 1);

$display_button  = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "checkForm(document.form1);", "submitBtn")."&nbsp";
//$display_button .= $linterface->GET_ACTION_BTN($Lang['Btn']['Reset'], "reset", "")."&nbsp";
$display_button .= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "window.location='manage.php?ClassID=$ClassID&SubjectID=$SubjectID'");


# tag information
$TAGS_OBJ[] = array($eReportCard['MakeupExam']['Generate'], $PATH_WRT_ROOT."home/admin/reportcard2008/management/promotion_retention/generate.php", 0);
$TAGS_OBJ[] = array($eReportCard['MakeupExam']['Manage'], $PATH_WRT_ROOT."home/admin/reportcard2008/management/promotion_retention/manage.php", 1);
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($eReportCard['PromoteRetainQuit']['PromotionAndRetention'],'index.php');
$PAGE_NAVIGATION[] = array($eReportCard['MakeupExam']['Manage'],'manage.php');
$PAGE_NAVIGATION[] = array($Lang['Btn']['Add']);

?>
<script type="text/JavaScript" language="JavaScript">
// Preloaded sets of Report list
var classLevelReportList = new Array();
var classLevelSubjectList = new Array();
var classLevelClassList = new Array();
var classLevelStudentList = new Array();

<?php
if(count($FormArr) > 0 /* && count($ReportTypeArr) > 0*/) {
	foreach((array)$OtherReportTypeSelection as $key => $value) {
		$value = addslashes(str_replace("\n", "", $value));
		$x .= "classLevelReportList[".$key."] = '$value';\n";
		
	}
	echo $x;
	
	foreach((array)$OtherSubjectSelection as $key => $value) {
		$value = addslashes(str_replace("\n", "", $value));
		$y .= "classLevelSubjectList[".$key."] = '$value';\n";
	}
	echo $y;
	
	foreach((array)$OtherClassSelection as $key => $value) {
		$value = addslashes(str_replace("\n", "", $value));
		$w .= "classLevelClassList[".$key."] = '$value';\n";
	}
	echo $w;
	
	foreach((array)$OtherStudentSelection as $key => $value) {
		$value = addslashes(str_replace("\n", "", $value));
		$z .= "classLevelStudentList[".$key."] = '$value';\n";
	}
	echo $z;	
}
?>

function loadReportList(classLevelID) {
	document.getElementById("ReportList").innerHTML = classLevelReportList[classLevelID];
	document.getElementById("SubjectList").innerHTML = classLevelSubjectList[classLevelID];
	document.getElementById("ClassList").innerHTML = classLevelClassList[classLevelID];
	var classIDSelect = document.getElementById("ClassID");
	var classIDSelected = '';
	if(classIDSelect) {
		classIDSelected = classIDSelect.options[classIDSelect.selectedIndex].value;
		document.getElementById("StudentList").innerHTML = classLevelStudentList[classIDSelected];
	}
}

function loadStudentList(classID)
{
	var classIDSelect = document.getElementById("ClassID");
	document.getElementById("StudentList").innerHTML = classLevelStudentList[classID];
}

function toggleSubjectList(selectedValue)
{
	var tdSubjectRow = document.getElementById('SubjectRow');
	
	if(selectedValue == '1' || selectedValue == '2') {
		tdSubjectRow.style.display = '';
	}else{
		tdSubjectRow.style.display = 'none';
	}
}

function checkForm(obj) {
	var promotionStatus = document.getElementById('PromotionStatus').value;
	var subjectIdSelect = document.getElementById('SubjectID');
	var reportIdSelect = document.getElementById('ReportID');
	var studentIdSelect = document.getElementById('StudentID');
	var numSubjectSelected = 0, numStudentSelected = 0;
	
	if(promotionStatus=='1' || promotionStatus=='2')
	{
		for(var i=0;i<subjectIdSelect.options.length;i++) {
			if(subjectIdSelect.options[i].selected){
				numSubjectSelected++;
			}
		}
		if(numSubjectSelected == 0) {
			alert('<?=$eReportCard['MakeupExam']['jsWarningArr']['PleaseSelectSubject']?>');
			return false;
		}
	}
	
	if(!(reportIdSelect.options[reportIdSelect.selectedIndex] && reportIdSelect.options[reportIdSelect.selectedIndex].value != '' && reportIdSelect.options[reportIdSelect.selectedIndex].value != '-1'))
	{
		alert('<?=$eReportCard['MakeupExam']['jsWarningArr']['PleaseSelectReport']?>');
		return false;
	}
	
	for(var i=0;i<studentIdSelect.options.length;i++) {
		if(studentIdSelect.options[i].selected){
			numStudentSelected++;
		}
	}
	if(numStudentSelected == 0){
		alert('<?=$eReportCard['MakeupExam']['jsWarningArr']['PleaseSelectStudents']?>');
		return false;
	}
	
	obj.submit();
	return true;
}
</script>
<br/>
<form name="form1" method="post" action="add_student_update.php" onsubmit="return false;">
<table width="98%" border="0" cellspacing="5" cellpadding="0">
	<tr>
		<td align="left" colspan="2">
			<?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?>
		</td>
	</tr>
</table>
<table id="html_body_frame" width="98%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table class="form_table_v30" width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td></td>
					<td align="right"><?=$linterface->GET_SYS_MSG($Result);?></td>
				</tr>
			<?php if(count($FormArr) == 0) { ?>
				<tr>
					<td align="center" colspan="2" class='tabletext'>
						<br /><?= $i_no_record_exists_msg ?><br />
					</td>
				</tr>
			<?php } else { ?>
				<tr>
					<td valign="top" nowrap="nowrap" class="field_title">
						<?= $eReportCard['Type'] ?>
					</td>
					<td width="75%" class="tabletext">
						<?= $PromotionStatusSelection ?>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="field_title">
						<?= $eReportCard['Form'] ?>
					</td>
					<td width="75%" class="tabletext">
						<?= $FormSelection ?>
					</td>
				</tr>
				<tr id="ClassRow" style="<?= $ClassRowDisplay ?>">
					<td valign="top" nowrap="nowrap" class="field_title">
						<?= $eReportCard['Class'] ?>
					</td>
					<td width="75%" class="tabletext">
						<span id="ClassList"><?= $ClassSelection ?></span>
					</td>
				</tr>
				<tr id="SubjectRow" style="<?= $SubjectRowDisplay ?>">
					<td valign="top" nowrap="nowrap" class="field_title">
						<?= $eReportCard['Subject'] ?>
					</td>
					<td width="75%" class="tabletext">
						<span id="SubjectList"><?= $SubjectSelection ?></span>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="field_title">
						<?= $eReportCard['ReportCard'] ?>
					</td>
					<td width="75%" class="tabletext">
						<span id="ReportList"><?= $ReportTypeSelection ?></span>
					</td>
				</tr>
				<tr id="ClassRow" style="<?= $StudentRowDisplay ?>">
					<td valign="top" nowrap="nowrap" class="field_title">
						<?= $eReportCard['Student'] ?>
					</td>
					<td width="75%" class="tabletext">
						<span id="StudentList"><?= $StudentSelection ?></span>
					</td>
				</tr>
			<?php } ?>
			</table>
		</td>
	</tr>
</table>
<table width="95%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td height="1" class="dotline"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td align="center" valign="bottom"><?=$display_button?></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<br>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>