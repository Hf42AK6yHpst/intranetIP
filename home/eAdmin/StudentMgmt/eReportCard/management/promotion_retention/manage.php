<?php
// Editing by 
/*
 * 	Date: 2017-06-21 (Bill)		[2017-0621-1726-12164]
 * 		- Added Absent Status
 * 	Date: 2017-05-19 (Bill)
 * 		- Allow View Group to access
 * 	Date: 2017-03-10 (Bill)		[2017-0109-1818-40164]
 * 		- Copy from EJ
 */

$PageRight = array("ADMIN", "VIEW_GROUP");

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# Access Checking
if (!$plugin['ReportCard2008'] || !$eRCTemplateSetting['ReportGeneration']['PromoteRetainQuit'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

# Cookies
$arrCookies = array();
$arrCookies[] = array("ck_reportcard_promotion_retention_page_size", "numPerPage");
$arrCookies[] = array("ck_reportcard_promotion_retention_page_number", "pageNo");
$arrCookies[] = array("ck_reportcard_promotion_retention_page_order", "order");
$arrCookies[] = array("ck_reportcard_promotion_retention_page_field", "field");	
$arrCookies[] = array("ck_reportcard_promotion_retention_page_keyword", "Keyword");
if($clearCoo == 1)
	clearCookies($arrCookies);
else
	updateGetCookies($arrCookies);

# Initial
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "")
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
}
else
{
	$lreportcard = new libreportcard();
}

# Access Checking
if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

# Initial
$linterface = new interface_html();
$lreportcard_ui = new libreportcard_ui();
$fcm_ui = new form_class_manage_ui();

# Toolbar
$toolbar = $linterface->Get_Content_Tool_v30("export", "javascript:jsExportStudent();");

# Button
$action_button_ary = array();
$action_button_ary[] = array("delete", "javascript:checkRemove2(document.form1, 'RecordID[]', 'jsRemoveStudent();');", "", "");
//$table_tool = $linterface->Get_DBTable_Action_Button_IP25($action_button_ary);

# Filter
$filterbar = "";

// Class Selection
$filterbar .= $fcm_ui->Get_Class_Selection($lreportcard->schoolYearID, "", "ClassID", $ClassID, "jsReloadTable();")."&nbsp;";

// Promotion Status Selection
$promotionStatusSelectionAry = array();
$promotionStatusSelectionAry[] = array("2", $eReportCard["PromotionStatus"][2]);
$promotionStatusSelectionAry[] = array("4", $eReportCard["PromotionStatus"][4]);
$promotionStatusSelectionAry[] = array("5", $eReportCard["PromotionStatus"][5]);
$promotionStatusSelectionAry[] = array("6", $eReportCard["PromotionStatus"][6]);
$promotionStatusSelectionAry[] = array("0", $eReportCard["PromotionStatusType"]["SubjectStatusAbsent"]);
$filterbar .= getSelectByArray($promotionStatusSelectionAry, " id='PromotionStatus' name='PromotionStatus' onchange='jsReloadTable();' ", "", 1, 0)."&nbsp;";

# Search Field
$searchbar = $linterface->Get_Search_Box_Div("Keyword", stripslashes($Keyword), "onkeyup='jsSearchRecord(event)';");

# Page
$CurrentPage = "Management_PromoteRetainQuit";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# Tag
//$TAGS_OBJ[] = array($eReportCard['MakeupExam']['Generate'], $PATH_WRT_ROOT."home/admin/reportcard2008/management/promotion_retention/generate.php?clearCoo=1", 0);
$TAGS_OBJ[] = array($eReportCard['PromoteRetainQuit']['PromotionAndRetention'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/management/promotion_retention/manage.php?clearCoo=1", 1);

$linterface->LAYOUT_START();
?>

<script type="text/javascript" language="Javascript">
function jsReloadTable()
{
	Block_Element('form1');
	var formData = $('#form1').serialize();
	$('#DivTable').load(
		'ajax_task.php?task=GET_PROMOTION_RETENTION_MANAGE_TABLE',
		formData,
		function(data){
			UnBlock_Element('form1');
			Scroll_To_Top();
			Set_DB_Table_Settings();
		}
	);
}

function jsSearchRecord(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	if (key == 13) {	// Press Enter
		jsReloadTable()
	}
	else {
		return false;
	}
}

function jsGoAddStudent()
{
	var ClassID = $('#ClassID').val();
	var PromotionStatus = $('#PromotionStatus').val();
	window.location.href='add_student.php?ClassID=' + ClassID + '&PromotionStatus=' + PromotionStatus;
}

function jsExportStudent()
{
	document.form1.action = "export.php";
	document.form1.submit();
	document.form1.action = "";
}

function jsRemoveStudent()
{
	Block_Element('form1');
	var RecordIDAry = [];
	var records = document.getElementsByName('RecordID[]');
	for(var i=0; i<records.length; i++) {
		if(records[i].checked) {
			RecordIDAry.push(records[i].value);
		}
	}
	
	$.post(
		'ajax_task.php',
		{
			task:'DELETE_PROMOTION_RETENTION_STUDENT_RECORD',
			'RecordID[]':RecordIDAry
		},
		function(ReturnMsg) {
			Get_Return_Message(ReturnMsg);
			Scroll_To_Top();
			jsReloadTable();
		}
	);
}

function Set_DB_Table_Settings()
{
	// Number per Page Selection
	if(document.form1.num_per_page) {
		document.form1.num_per_page.onchange = function(){ this.form.pageNo.value = 1; this.form.page_size_change.value = 1; this.form.numPerPage.value = this.options[this.selectedIndex].value; jsReloadTable(); };
	}
	
	// Page Number Selection
	var this_page_no = $('div.page_no select')[0];
	if(this_page_no) {
		this_page_no.onchange = function(){ this.form.pageNo.value = this.options[this.selectedIndex].value; jsReloadTable(); };
	}
	
	// Next Page Button
	var this_next = $('div.page_no a.next')[0];
	if(this_next && this_next.href) {
		this_next.href = "javascript:jsGoToPage(1); ";
	}
	
	// Previous Page Button
	var this_prev = $('div.page_no a.prev')[0];
	if(this_prev && this_prev.href) {
		this_prev.href = "javascript:jsGoToPage(2); ";
	}
}

function jsGoToPage(type)
{
	if(document.form1.pageNo)
	{
		if(type==1)
			document.form1.pageNo.value = parseInt(document.form1.pageNo.value) + 1;
		else if(type==2)
			document.form1.pageNo.value = parseInt(document.form1.pageNo.value) - 1;
		jsReloadTable();
	}
}

$(document).ready(function(){
	jsReloadTable();
});
</script>

<!-- Start Main Table //-->
<form name="form1" id="form1" method="POST" onsubmit="return false;">
<table width="100%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$filterbar?><td>
		<td align="right"><?= $linterface->GET_SYS_MSG($msg) ?></td>
	</tr>
</table>

<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td align="left">
			<table border="0" cellspacing="0" cellpadding="2" width="100%">
				<tr>
					<td>
						<div class="Conntent_tool">
							<?=$toolbar?>
	      				</div>
					</td>
				</tr>
			</table>
		</td>
		<td align="right"><?=$searchbar?></td>
	</tr>
</table>

<div style="width:100%">
	<?=$table_tool?>
	<div id="DivTable"></div>
</div>
</form>
<!-- End Main Table //-->
<br />

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>