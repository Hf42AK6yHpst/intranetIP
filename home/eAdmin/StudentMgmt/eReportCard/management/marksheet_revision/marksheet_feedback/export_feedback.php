<?php

#########################################
#
#   Date:   2019-05-02  Bill
#           prevent SQL Injection
#
#########################################

$PATH_WRT_ROOT = "../../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	$lreportcard = new libreportcard();
	if ($lreportcard->hasAccessRight())
	{
		if (!isset($_GET) || !isset($SubjectID, $ClassLevelID, $ReportID, $ClassID)) {
			header("Location: index.php");
			exit();
		}
		
		### Handle SQL Injection + XSS [START]
		$ClassLevelID = IntegerSafe($ClassLevelID);
		$ReportID = IntegerSafe($ReportID);
		$SubjectID = IntegerSafe($SubjectID);
		$ClassID = IntegerSafe($ClassID);
		$SubjectGroupID = IntegerSafe($SubjectGroupID);
		$ParentSubjectID = IntegerSafe($ParentSubjectID);
		### Handle SQL Injection + XSS [END]
		
		// Class
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		for($i=0 ; $i<count($ClassArr) ;$i++) {
			if($ClassArr[$i][0] == $ClassID)
				$ClassName = $ClassArr[$i][1];
		}
		
		// SubjectName
		$SubjectName = $lreportcard->GET_SUBJECT_NAME_LANG($SubjectID);
//		$SubjectName = str_replace("&nbsp;", "_", $SubjectName);
		
		// Student Info Array
		if ($SubjectGroupID != '')
		{
			$StudentArray = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupID, $ClassLevelID, '', 1);
			
			$obj_SubjectGroup = new subject_term_class($SubjectGroupID);
			$SubjectGroupName = $obj_SubjectGroup->Get_Class_Title();
			
			$Class_SG_Name = $SubjectGroupName;
		}
		else
		{
			$StudentArray =  $lreportcard->Get_Marksheet_Accessible_Student_List_By_Subject_And_Class($ReportID, $_SESSION['UserID'], $ClassID, $ParentSubjectID, $SubjectID, $showStyle=false);
		}
		$StudentSize = sizeof($StudentArray);
		$StudentIDArray = Get_array_By_Key($StudentArray,"UserID");
		
		// FeedBack Array
		$FeedBackArr = $lreportcard->Get_Marksheet_Feedback($ReportID, $SubjectID ,$StudentIDArray);
		
		# Create the data array for export
		$ExportArr = array();
		for($i=0; $i<$StudentSize; $i++) {
			$thisStudentID = $StudentArray[$i]["UserID"];
			
//			if(empty($FeedBackArr[$thisStudentID][$SubjectID])) continue;
			
			# Status
			if(!empty($targetStatus) && $FeedBackArr[$thisStudentID][$SubjectID]['RecordStatus']!=$targetStatus) continue;
			switch($FeedBackArr[$thisStudentID][$SubjectID]['RecordStatus'])
			{
				case 1: $thisStatus = $eReportCard['Closed']; break;
				case 2: $thisStatus = $eReportCard['InProgress']; break;
				default: $thisStatus = $Lang['General']['EmptySymbol']; 
			}

			# Comment
			if(empty($FeedBackArr[$thisStudentID][$SubjectID]))
			{
				$thisComment = '';
				$thisStatus = $Lang['General']['EmptySymbol']; 
			}
			else
			{
				$CommentArr = $lreportcard->Fetch_Feedback_Detail($FeedBackArr[$thisStudentID][$SubjectID]['Comment']);
				
				$thisComment = '';
				foreach((array)$CommentArr as $thisCommentArr )
				{
					$thisComment .= $thisCommentArr['Name']." ".$thisCommentArr['DateInput']."\n".$thisCommentArr['Comment']."\n";
				}
			}
			
			$ExportColArr = array();
			$ExportColArr[] = $SubjectName;
			if($Class_SG_Name) {
				$ExportColArr[] = $Class_SG_Name;
			}
			$ExportColArr[] = $StudentArray[$i]["ClassName"];
			$ExportColArr[] = $StudentArray[$i]["ClassNumber"];
			$ExportColArr[] = $StudentArray[$i]["StudentName"];
			$ExportColArr[] = $thisComment;
			$ExportColArr[] = $FeedBackArr[$thisStudentID][$SubjectID]['DateModified'];
			$ExportColArr[] = $thisStatus;
			$ExportArr[] = $ExportColArr;
		}
		
		$lexport = new libexporttext();
		
		$exportColumn = array();
		foreach(array("En","Ch") as $LangName)
		{
			$exportColumnRow = array();
			$exportColumnRow[] = $eReportCard['ExportHeader']['Subject'][$LangName];
			if($Class_SG_Name) {
				$exportColumnRow[] = $eReportCard['ExportHeader']['SubjectGroup'][$LangName];
			}
			$exportColumnRow[] = $eReportCard['ExportHeader']['ClassName'][$LangName];
			$exportColumnRow[] = $eReportCard['ExportHeader']['ClassNumber'][$LangName];
			$exportColumnRow[] = $eReportCard['ExportHeader']['StudentName'][$LangName];
			$exportColumnRow[] = $eReportCard['ExportHeader']['Feedback'][$LangName];
			$exportColumnRow[] = $eReportCard['ExportHeader']['LastModified'][$LangName];
			$exportColumnRow[] = $eReportCard['ExportHeader']['Status'][$LangName];	
			$exportColumn[] = $exportColumnRow;
		}
		$export_content = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportArr, $exportColumn,  "", "\r\n", "", 0, "11",1);
		
		intranet_closedb();
		
		// Output the file to user browser
		$filename = "export_feedback.csv";
//		$filename = str_replace(' ', '_', $filename);
		$lexport->EXPORT_FILE($filename, $export_content);
	}
	else
	{
		echo "You have no priviledge to access this page.";
	}
}
else
{
	echo "You have no priviledge to access this page.";
}
?>