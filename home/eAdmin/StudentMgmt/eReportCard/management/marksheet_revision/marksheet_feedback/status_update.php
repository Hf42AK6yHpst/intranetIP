<?
# modifying by : Bill

#########################################
#
#   Date:   2019-05-01  Bill
#           prevent SQL Injection
#
#########################################

$PATH_WRT_ROOT = "../../../../../../../";
//$PageRight = array("TEACHER", "STUDENT", "PARENT");

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$ReportID = IntegerSafe($ReportID);
$SubjectID = IntegerSafe($SubjectID);
foreach((array)$Status as $thisKey => $studentStatus) {
    $Status[$thisKey] = IntegerSafe($studentStatus);
}
### Handle SQL Injection + XSS [END]

$lreportcard = new libreportcard();
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
}
else
{
	$lreportcard = new libreportcard();
}

$Success = $lreportcard->Update_Marksheet_Feedback($ReportID, $SubjectID, $Status);

//$table = $lreportcard->DBName.".RC_MARKSHEET_FEEDBACK"; 
//
//foreach($Status as $StudentID => $RecordStatus)
//{
//	$sql = "	UPDATE 
//					$table
//				SET
//					RecordStatus = '$RecordStatus'
//				WHERE 
//					ReportID = '$ReportID'
//					AND SubjectID = '$SubjectID'
//					AND StudentID = '$StudentID'
//					";
//	$lreportcard->db_db_query($sql) or die($sql." ".mysql_error());
//					
//}	
//
//

$Msg = $Success?"UpdateSuccess":"UpdateUnsuccess";
header("location: index.php?SubjectID=".$SubjectID."&ReportID=".$ReportID."&StudentID=".$StudentID."&ClassLevelID=".$ClassLevelID."&SubjectGroupID=".$SubjectGroupID."&ClassID=".$ClassID."&Msg=".$Msg);
?>