<?php
// Modifing by 

/********************************************************
 * Modification log
 * 20190501 Bill
 *      - prevent SQL Injection + Cross-site Scripting
 * 20170519 Bill
 * 		- Allow View Group to access (view only)
 * 20170508 Bill [2017-0508-1128-10164]
 * 		- Display correct Year Report Column Score when $eRCTemplateSetting['Settings']['TemplateSettings_ConsolidatedReportWithTermReportAssessment'] = true
 * ******************************************************/

$PATH_WRT_ROOT = "../../../../../../../";
$PageRight = array("TEACHER");

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
//include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$ClassLevelID = IntegerSafe($ClassLevelID);
$ReportID = IntegerSafe($ReportID);
$SubjectID = IntegerSafe($SubjectID);
$SubjectGroupID = IntegerSafe($SubjectGroupID);
$ClassID = IntegerSafe($ClassID);
$ParentSubjectID = IntegerSafe($ParentSubjectID);
### Handle SQL Injection + XSS [END]

$lreportcard = new libreportcard();
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
}
else
{
	$lreportcard = new libreportcard();
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
$lreportcard = new libreportcard2008j();

//$CurrentPage = "Management_VerifyAssessmentResult";
$CurrentPage = $ck_ReportCard_UserType == "STUDENT"?"Management_VerifyAssessmentResult":"Management_MarkSheetRevision";

$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

if ($lreportcard->hasAccessRight())
{
	$linterface = new interface_html();
	
	# Gen Status Filter Bar
	$StatusAry[] = array("2",$eReportCard['InProgress']);
	$StatusAry[] = array("1",$eReportCard['Closed']);
	$StatusFilter = getSelectByArray($StatusAry," name='targetStatus' onchange='submit()'",$targetStatus,1,0,$eReportCard['AllStatus']);
	
	# Get Btns
	$ExportBtn = $linterface->Get_Content_Tool_v30("export", "javascript:js_Export();");
	
	// View Group - View Only
	if($ck_ReportCard_UserType == "VIEW_GROUP") {
		$ViewOnly = true;
	}
	if($ViewOnly && !$lreportcard->IS_ADMIN_USER($_SESSION['UserID'])) {
		$StatusMappingAry = BuildMultiKeyAssoc((array)$StatusAry, "0", "1");
	}
	
	# Get ColumnData 
	$ColumnArr = $lreportcard->returnReportTemplateColumnData($ReportID);
	$ColumnTitleArr = $lreportcard->returnReportColoumnTitle($ReportID);
	
	###### Get Report Basic Info ######
	$ReportInfoArr = $lreportcard->returnReportTemplateBasicInfo($ReportID);
	$SemID = $ReportInfoArr['Semester'];
	$ReportType = ($SemID=='F')? 'W' : 'T';
	
	### Gen Table
    
	###################################################################################################### 
	#	 Get MarkSheet Score
	#
		# Get MarkSheet Info
		// Tags of MarkSheet(0: Raw Mark, 1: Weighted Mark, 2: Converted Grade
		//$TagsType = (isset($TagsType) && $TagsType != "") ? $TagsType : 0;
        
		// Period (Term x, Whole Year, etc)
		$PeriodArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID, $ReportID);
		$PeriodName = (count($PeriodArr) > 0) ? $PeriodArr['SemesterTitle'] : '';
		
		# Load Settings - Calculation
		// Get the Calculation Method of Report Type - 1: Right-Down,  2: Down-Right
		$SettingCategory = 'Calculation';
		$categorySettings = $lreportcard->LOAD_SETTING($SettingCategory);
		$TermCalculationType = $categorySettings['OrderTerm'];
		$FullYearCalculationType = $categorySettings['OrderFullYear'];
		
		# Load Settings - Storage&Display
		$SettingStorDisp = 'Storage&Display';
		$storDispSettings = $lreportcard->LOAD_SETTING($SettingStorDisp);
		
		// Get Absent&Exempt Settings
		$SettingAbsentExempt = 'Absent&Exempt';
		$absentExemptSettings = $lreportcard->LOAD_SETTING($SettingAbsentExempt);
		
		if(count($PeriodArr) > 0 && $PeriodArr['Semester'] == "F") {
			$usePercentage = ($FullYearCalculationType == 1) ? 1 : 0;
		}
		else {
			$usePercentage = ($TermCalculationType == 1) ? 1 : 0;
		}
        
		# Get component subject(s) if any
		$isEdit = 1;					// default "1" as the marksheet is editable
		$CmpSubjectArr = array();
		$isCalculateByCmpSub = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
		$isCmpSubject = $lreportcard->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($SubjectID);
		if(!$isCmpSubject){
			$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
			
			$CmpSubjectIDArr = array();
			if(!empty($CmpSubjectArr)){	
				for($i=0 ; $i<count($CmpSubjectArr) ; $i++){
					$CmpSubjectIDArr[] = $CmpSubjectArr[$i]['SubjectID'];
					
					$CmpSubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $CmpSubjectArr[$i]['SubjectID'],0,0,$ReportID);
					if(count($CmpSubjectFormGradingArr) > 0 && $CmpSubjectFormGradingArr['ScaleInput'] == "M")
						$isCalculateByCmpSub = 1;
				}
			}
			// Check whether the marksheet is editable or not 
			$isEdit = $lreportcard->CHECK_MARKSHEET_STATUS($SubjectID, $ClassLevelID, $ReportID, $CmpSubjectIDArr);
		}
		
		// Initialization of Grading Scheme
		$SchemeID = '';
		$ScaleInput = "M";		// M: Mark, G: Grade
		$ScaleDisplay = "M";	// M: Mark, G: Grade
		$SchemeType = "H";		// H: Honor Based, PF: PassFail
		$SchemeInfo = array();
		$SchemeMainInfo = array();
		$SchemeMarkInfo = array();
		$SubjectFormGradingArr = array();
		$possibleSpeicalCaseSet1 = array();
		$possibleSpeicalCaseSet2 = array();
		
		$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0,0,$ReportID);
		# Check if no any scheme is assigned to this Form(ClassLevelID) of this subject, redirect
		if(count($SubjectFormGradingArr) > 0){
			if($SubjectFormGradingArr['SchemeID'] == "" || $SubjectFormGradingArr['SchemeID'] == null) {
				header("Location: ../marksheet_revision.php?ClassLevelID=$ClassLevelID&ReportID=$ReportID&SubjectID=$SubjectID&msg=1");
			}
		}
		
		# Get Grading Scheme Info of that subject
		if(count($SubjectFormGradingArr) > 0){
			$SchemeID = $SubjectFormGradingArr['SchemeID'];
			$ScaleInput = $SubjectFormGradingArr['ScaleInput'];
			$ScaleDisplay = $SubjectFormGradingArr['ScaleDisplay'];
			
			$SchemeInfo = $lreportcard->GET_GRADING_SCHEME_INFO($SchemeID);
			$SchemeMainInfo = $SchemeInfo[0];
			$SchemeMarkInfo = $SchemeInfo[1];
			$SchemeType = $SchemeMainInfo['SchemeType'];
			
		}	
        
		$possibleSpeicalCaseSet1 = $lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
		
		if($SchemeType == "H" && $ScaleInput == "G"){	// Prepare Selection Box using Grade as Input
			$InputTmpOption = array();
			$InputGradeOption = array();
			$SchemeGrade = $lreportcard->GET_GRADE_FROM_GRADING_SCHEME_RANGE($SchemeID);
			$InputGradeOption[0] = array('', '--');
			if(!empty($SchemeGrade)){
				foreach($SchemeGrade as $GradeRangeID => $grade) {
					$InputGradeOption[] = array($GradeRangeID, $grade);
				}
			}
			
			for($k=0 ; $k<count($possibleSpeicalCaseSet1) ; $k++){
				//$InputGradeOption[] = array($possibleSpeicalCaseSet1[$k], $lreportcard->GET_MARKSHEET_SPECIAL_CASE_SET1_STRING($possibleSpeicalCaseSet1[$k]));
				$InputGradeOption[] = array($possibleSpeicalCaseSet1[$k], $possibleSpeicalCaseSet1[$k]);
			}
		}
		else if($SchemeType == "PF"){			// Prepare Selection Box using Pass/Fail as Input
			$SchemePassFail = array();
			$SchemePassFail['P'] = $SchemeMainInfo['Pass'];
			$SchemePassFail['F'] = $SchemeMainInfo['Fail'];
		
			$InputPFOption = array();
			$InputPFOption[0] = array('', '--');
			$InputPFOption[1] = array('P', $SchemeMainInfo['Pass']);
			$InputPFOption[2] = array('F', $SchemeMainInfo['Fail']);
			
			$possibleSpeicalCaseSet2 = $lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2();
			for($k=0 ; $k<count($possibleSpeicalCaseSet2) ; $k++){
				//$InputPFOption[] = array($possibleSpeicalCaseSet2[$k], $lreportcard->GET_MARKSHEET_SPECIAL_CASE_SET2_STRING($possibleSpeicalCaseSet2[$k]));
				$InputPFOption[] = array($possibleSpeicalCaseSet2[$k], $possibleSpeicalCaseSet2[$k]);
			}
		}
			
		# Get Data Either Assessment Column(s) in Term Or Term Column(s) in Wholely Report	
		// Loading Report Template Data
		$basic_data = $lreportcard->returnReportTemplateBasicInfo($ReportID);			// Basic  Data
		$column_data = $lreportcard->returnReportTemplateColumnData($ReportID); 		// Column Data
		$ColumnSize = sizeof($column_data);
		$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
		$ReportTitle = $basic_data['ReportTitle'];
		$ReportType = $basic_data['Semester'];
		$isPercentage = $basic_data['PercentageOnColumnWeight'];
		
		# Get Existing Report Calculation Method
		$CalculationType = ($ReportType == "F") ? $FullYearCalculationType : $TermCalculationType;
		
		# Get Report Column Weight
		$WeightAry = array();
		if($CalculationType == 2){
			if(count($column_data) > 0){
		    	for($i=0 ; $i<sizeof($column_data) ; $i++){
					$OtherCondition = "SubjectID IS NULL AND ReportColumnID = ".$column_data[$i]['ReportColumnID'];
					$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
					
					$WeightAry[$weight_data[0]['ReportColumnID']][$SubjectID] = $weight_data[0]['Weight'];
				}
			}
		}
		else {
			$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID);  
			
			for($i=0 ; $i<sizeof($weight_data) ; $i++)
				$WeightAry[$weight_data[$i]['ReportColumnID']][$weight_data[$i]['SubjectID']] = $weight_data[$i]['Weight'];
		}		
		
		for($i=0 ; $i<sizeof($column_data) ; $i++){
			$OtherCondition = "SubjectID = '".$SubjectID."' AND ReportColumnID = ".$column_data[$i]['ReportColumnID'];
			$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
			
			$SubjectWeightAry[$weight_data[0]['ReportColumnID']] = $weight_data[0]['Weight'];
		}
		
		// Student Info Array
		if ($SubjectGroupID != '')
		{
			$StudentArr = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupID, $ClassLevelID, '', 1, 1);
			$showClassName = true;
			
			$obj_SubjectGroup = new subject_term_class($SubjectGroupID);
			$SubjectGroupName = $obj_SubjectGroup->Get_Class_Title();
			$TitleDisplay = "<td>".$Lang['SysMgr']['SubjectClassMapping']['SubjectGroup']." : <strong>".$SubjectGroupName."</strong></td>";
		}
		else
		{
			$StudentArr = $lreportcard->Get_Marksheet_Accessible_Student_List_By_Subject_And_Class($ReportID, $_SESSION['UserID'], $ClassID, $ParentSubjectID, $SubjectID);
//			if ($lreportcard->IS_ADMIN_USER($_SESSION['UserID']) == true || $lreportcard->Is_Class_Teacher($_SESSION['UserID'], $ClassID) == true)
//			{
//				# Admin / Class Teacher => Show all students
//				$TaughtStudentList = '';
//			}
//			else
//			{
//				# Subject Teacher => Show taught students only
//				$TargetSubjectID = ($ParentSubjectID != '')? $ParentSubjectID : $SubjectID;
//				$TaughtStudentArr = $lreportcard->Get_Student_List_Taught_In_This_Consolidated_Report($ReportID, $_SESSION['UserID'], $ClassID, $TargetSubjectID);
//				$TaughtStudentList = '';
//				if (count($TaughtStudentArr > 0))
//					$TaughtStudentList = implode(',', $TaughtStudentArr);
//			}
//			$StudentArr = $lreportcard->GET_STUDENT_BY_CLASS($ClassID, $TaughtStudentList, 1, 0, 1);
			
			$showClassName = false;
			$TitleDisplay = "<td>".$eReportCard['Class']." : <strong>".$ClassName."</strong></td>";
		}
		$StudentSize = sizeof($StudentArr);
		
		$StudentIDArr = array();
		if(count($StudentArr) > 0){
			for($i=0 ; $i<sizeof($StudentArr) ; $i++)
				$StudentIDArr[$i] = $StudentArr[$i]['UserID'];
		}
		
		# Get MarkSheet OVERALL Score Info for the cases when it is (Term Report OR Whole Year Report) AND 
		# when the scheme type is Honor-Based (H) AND its scaleinput is Grade(G) OR
		# when the scheme type is PassFail-Based (PF)
		$MarksheetOverallScoreInfoArr = array();
		if(($TagsType == 0 || $TagsType == 2) && (($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF") ){
			$MarksheetOverallScoreInfoArr = $lreportcard->GET_MARKSHEET_OVERALL_SCORE($StudentIDArr, $SubjectID, $ReportID);
		}
		
		##############################################################################################
		# General Approach dealing with Search Term Result From Whole Year Report not From Term Report
		# Can deal with Customization of SIS dealing with secondary template 
		$isAllWholeReport = 0;
		if($ReportType == "F"){
			$ReportTypeArr = array();
			$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);
			if(count($ReportTypeArr) > 0){
				$flag = 0;
				for($j=0 ; $j<count($ReportTypeArr) ; $j++){
					if($ReportTypeArr[$j]['ReportID'] != $ReportID){
						if($ReportTypeArr[$j]['Semester'] != "F")	$flag = 1;
					}
				}
				if($flag == 0) $isAllWholeReport = 1;
			}
		}
		
		# Get the Corresponding Whole Year ReportID from 
		# the ReportColumnID of the Current Whole Year Report 
		$WholeYearColumnHasTemplateArr = array();
		if($isAllWholeReport == 1){
			if($ReportType == "F" && $ColumnSize > 0){
				for($j=0 ; $j<$ColumnSize ; $j++){
					$ReportColumnID = $column_data[$j]['ReportColumnID'];
					# Return Whole Year Report Template ID - ReportID 
					$WholeYearColumnHasTemplateArr[$ReportColumnID] = $lreportcard->CHECK_WHOLE_YEAR_REPORT_TEMPLATE_FROM_COLUMN($ReportID, $column_data[$j]['SemesterNum'], $ClassLevelID);
					
					# Check Whether the Return WholeYear ReportID is not greater the Current ReportID
					# if User creates All Reports sequently
					if($WholeYearColumnHasTemplateArr[$ReportColumnID] != false){
						if($WholeYearColumnHasTemplateArr[$ReportColumnID] > $ReportID)
							$WholeYearColumnHasTemplateArr[$ReportColumnID] = false;
					}
				}
			}
		}
		####################################################################################################
			
		
		# Get MarkSheet Score Info / ParentSubMarksheetScoreInfoArr	
		$MarksheetScoreInfoArr = array();
		$ColumnHasTemplateAry = array();
		$WeightedMark = array();
		$hasDirectInputMark = 0;
		
		if(count($column_data) > 0){
		    for($i=0 ; $i<sizeof($column_data) ; $i++){
			    
			  if($isAllWholeReport == 1 && $WholeYearColumnHasTemplateArr[$column_data[$i]['ReportColumnID']] != false){
				#######################################################################################################################################
				# for the Whole Year Report which defined any Term Column in other Whole Year Report	added on 22 May by Jason
				// have defined term report template 
				$WholeYearReportID = $WholeYearColumnHasTemplateArr[$column_data[$i]['ReportColumnID']];
				$WholeYearReportColumnData = $lreportcard->returnReportTemplateColumnData($WholeYearReportID); 		// Column Data
				$WholeYearReportColumnWeightData = $lreportcard->returnReportTemplateSubjectWeightData($WholeYearReportID);  
				
				
				if(count($WholeYearReportColumnWeightData) > 0){
					for($j=0 ; $j<sizeof($WholeYearReportColumnWeightData) ; $j++){
						if($WholeYearReportColumnWeightData[$j]['SubjectID'] == $SubjectID && $WholeYearReportColumnWeightData[$j]['ReportColumnID'] != ""){
							$Weight[$WholeYearReportColumnWeightData[$j]['ReportColumnID']] = $WholeYearReportColumnWeightData[$j]['Weight'];
						}
					}
				}
				
				if(count($WholeYearReportColumnData) > 0){
					for($j=0 ; $j<sizeof($WholeYearReportColumnData) ; $j++){
						if($WholeYearReportColumnData[$j]['SemesterNum'] == $column_data[$i]['SemesterNum']){
							if($Weight[$WholeYearReportColumnData[$j]['ReportColumnID']] != 0){
								$MarksheetScoreInfoArr[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_MARKSHEET_SCORE($StudentIDArr, $SubjectID, $WholeYearReportColumnData[$j]['ReportColumnID']);
							} else {
								$MarksheetScoreInfoArr[$column_data[$i]['ReportColumnID']] = '--';
							}
						}
					}
				}
				#######################################################################################################################################
			  } else {
			    # for the general Term Report &
			    # for the Whole Year Report which does not define any Term Column in other Whole Year Report
			    /* 
				* Check for the Mark Column is allowed to fill in mark when any term does not have related report template 
				* by SemesterNum (ReportType) && ClassLevelID
				* An array is initialized as a control variable [$ColumnHasTemplateAry]
				* A variable is initialized as a control variable [$hasDirectInputMark], must be used with [$ReportType == "F"]
				* to control the Import function to be used or not
				*/
				if($ReportType == "F"){
					$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] = $lreportcard->CHECK_REPORT_TEMPLATE_FROM_COLUMN($column_data[$i]['SemesterNum'], $ClassLevelID);
					if(!$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']])
						$hasDirectInputMark = 1;
				} else {
					$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] = 1;
				}
			    
				
			    # Get Data from Subject without Component Subjects OR Subject having Component Subjects with ScaleInput is Not Marked
			    if( count($CmpSubjectArr) == 0 || 
			    	(count($CmpSubjectArr) > 0 && !$isCalculateByCmpSub) || 
			    	(count($CmpSubjectArr) >0 && $isCalculateByCmpSub && (($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF")) ){
					$MarksheetScoreInfoArr[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_MARKSHEET_SCORE($StudentIDArr, $SubjectID, $column_data[$i]['ReportColumnID']);
				
				} else {	
					# Conidtion: Subject contains Component Subjects & Mark is calculated by its components subjects
					// Get Parent Subject Marksheet Score by Marksheet Score of Component Subjects if any
					if($ReportType == "F" && !$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']]){
						
						// for whole year report & it does not have term report template defined 
						$ParentSubMarksheetScoreInfoArr[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_STUDENT_TERM_SUBJECT_MARK_WITHOUT_TEMPLATE($StudentIDArr, $ReportID, $column_data[$i]['ReportColumnID'], $SubjectID, $CmpSubjectIDArr);
					} else {						
						// for term report use only
						$ParentSubMarksheetScoreInfoArr[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_PARENT_SUBJECT_MARKSHEET_SCORE($StudentIDArr, $SubjectID, $CmpSubjectIDArr, $ReportID, $column_data[$i]['ReportColumnID'], "Term", 0, 0, $checkSpecialFullMark=false);
					}
				}

				# For Whole Year Report use only when Case i
				if($ReportType == "F"){
					# Case 1: Get the marksheet OVERALL score when the inputscale is Grade(G) OR PassFail(PF)
					if(($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF"){
						if($ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']]){
							$TermReportID = $ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']];
							$MarksheetOverallScoreInfoArr[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_MARKSHEET_OVERALL_SCORE($StudentIDArr, $SubjectID, $TermReportID);
						}
					}
					else if($SchemeType == "H" && $ScaleInput == "M"){
						# Case 2: Calculating The Weighted Mark of Parent Subjects if having Component Subjects
						if($ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']]){	
							// have defined term report template 
							$TermReportID = $ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']];
							// [2017-0508-1128-10164]
							$TermReportColumnID = $column_data[$i]['TermReportColumnID'];
							$excludedTermReportColumnWeight = $eRCTemplateSetting['Settings']['TemplateSettings_ConsolidatedReportWithTermReportAssessment_ExcludeWeightDisplay'];
							
							if(!$isCalculateByCmpSub){
								# Get Student Term Subject Mark of common subject without any component subjects
								$WeightedMark[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_STUDENT_TERM_SUBJECT_MARK($StudentIDArr, $TermReportID, $SubjectID, $CmpSubjectIDArr, 1, $ReportID, $TermReportColumnID, $excludedTermReportColumnWeight);
							}
							else {
								# Get Student Term Subject Mark of a subject by manipulating data of its component subjects
								$WeightedMark[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_STUDENT_TERM_SUBJECT_MARK($StudentIDArr, $TermReportID, $SubjectID, $CmpSubjectIDArr, $TermCalculationType, $ReportID, $TermReportColumnID, $excludedTermReportColumnWeight);
							}
						}
						else {
							# does not have term report Template for whole year report &
							# subject contains component subjects
							if($isCalculateByCmpSub){
								$ParentSubMarksheetScoreInfoArr[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_STUDENT_TERM_SUBJECT_MARK_WITHOUT_TEMPLATE($StudentIDArr, $ReportID, $column_data[$i]['ReportColumnID'], $SubjectID, $CmpSubjectIDArr);
								$hasDirectInputMark = 0;
							}
						}
					}
				}
		  	  } # End of if($isAllWholeReport == 0)
			}
		}
		# Get Overall Term Subject Mark of a subject having Component Subjects when 
		# Conition: TagsType is 1 - "Preview Mark" & it is Term Report 
		$OverallTermSubjectWeightedMark = array();
		if(($SchemeType != "PF" && $ScaleDisplay == "M" && $ScaleInput != "G") && $ReportType != "F" && !empty($CmpSubjectIDArr) ){
			$OverallTermSubjectWeightedMark = $lreportcard->GET_STUDENT_TERM_SUBJECT_MARK($StudentIDArr, $ReportID, $SubjectID, $CmpSubjectIDArr, $TermCalculationType);
		}

	    # Student Content of Marksheet Revision
	    # $OverallSubjectMark is used as a reference for user 
	    # $isShowOverallMark is used as a control to display term/overall subject mark
	    for($j=0; $j<count($StudentArr); $j++){
			list($StudentID, $WebSAMSRegNo, $StudentNo, $StudentName, $ClassName) = $StudentArr[$j];
			$tr_css = ($j % 2 == 1) ? "tablegreenrow2" : "tablegreenrow1";
			$td_css = ($j % 2 == 1) ? "attendanceouting" : "attendancepresent";
			
			if ($showClassName)
				$thisClassDisplay = $ClassName.'-'.$StudentNo;
			else
				$thisClassDisplay = $StudentNo;
		
			$display .= '<tr class="'.$tr_css.'">
		          <td align="left" nowrap="nowrap" class="'.$td_css.' tabletext">'.$thisClassDisplay.'</td>
	          	  <td nowrap="nowrap" class="'.$td_css.' tabletext">'.$StudentName.'<input type="hidden" name="StudentID[]" id="StudentID_'.$j.'" value="'.$StudentID.'"/></td>';
	        if(count($column_data) > 0){
		        $OverallSubjectMark = 0;
		        $isShowOverallMark = 1;
		        
		        # Check for Combination of Special Case of Input Mark / Term Mark
		        $SpecialCaseArr = $lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
		        foreach($SpecialCaseArr as $key){
		        	$SpecialCaseCountArr[$key] = array("Count"=>0, "Value"=>'', "isCount"=>'');
	        	}
	        	
	        	# Prepare an Array for converting mark to weighted mark 
				# AllColumnMarkArr is refer the all Column Marks of each student
				if($SchemeType != "PF" && $ScaleDisplay == "M" && $ScaleInput != "G"){
					$AllColumnMarkArr = array();
					for($r=0 ;$r<$ColumnSize ; $r++){
						if( $WeightAry[$column_data[$r]['ReportColumnID']][$SubjectID] != 0 && 
							$WeightAry[$column_data[$r]['ReportColumnID']][$SubjectID] != null && 
							$WeightAry[$column_data[$r]['ReportColumnID']][$SubjectID] != "" ){
								
							if($isAllWholeReport == 1 && $WholeYearColumnHasTemplateArr[$column_data[$i]['ReportColumnID']] != false){
								// new approach 
							} else {
								if($ReportType == "F" && $ColumnHasTemplateAry[$column_data[$r]['ReportColumnID']] && $ScaleInput == "M"){	// whole year with term template
									if(!empty($CmpSubjectArr) && $isCalculateByCmpSub){		// Subject with component Subjects
										$AllColumnMarkArr[$column_data[$r]['ReportColumnID']] = $WeightedMark[$column_data[$r]['ReportColumnID']][$StudentID];
									} else {	// subject without component subject
										$AllColumnMarkArr[$column_data[$r]['ReportColumnID']] = $WeightedMark[$column_data[$r]['ReportColumnID']][$StudentID];
									}
								} else {	// term report OR whole year without term template
									if(!empty($CmpSubjectArr) && $isCalculateByCmpSub){		// Subject with component Subjects
										$AllColumnMarkArr[$column_data[$r]['ReportColumnID']] = $ParentSubMarksheetScoreInfoArr[$column_data[$r]['ReportColumnID']][$StudentID];
									} else {	// subject without component subject
										if($MarksheetScoreInfoArr[$column_data[$r]['ReportColumnID']][$StudentID]['MarkType'] == "SC")
							    			$AllColumnMarkArr[$column_data[$r]['ReportColumnID']] = $MarksheetScoreInfoArr[$column_data[$r]['ReportColumnID']][$StudentID]['MarkNonNum'];
							    		else 
											$AllColumnMarkArr[$column_data[$r]['ReportColumnID']] = $MarksheetScoreInfoArr[$column_data[$r]['ReportColumnID']][$StudentID]['MarkRaw'];
									}
								}
							} # End if($isAllWholeReport)
				    		
				    		
				    	}
				    } # End Report Column Loop				

				}
				# Loop Report Column
			    for($i=0 ; $i<sizeof($column_data) ; $i++){
				    list($ReportColumnID, $ColumnTitle, $DefaultWeight, $SemesterNum) = $column_data[$i];
				    if($isAllWholeReport == 1 && $WholeYearColumnHasTemplateArr[$column_data[$i]['ReportColumnID']] != false){
				    	#############################################################################
					    # New Approach dealing with only having Whole Year Report
					    #############################################################################
						
						if($SchemeType != "PF" && $ScaleDisplay == "M" && $ScaleInput != "G"){
							if($WeightAry[$ReportColumnID][$SubjectID] != 0 && $WeightAry[$ReportColumnID][$SubjectID] != null && $WeightAry[$ReportColumnID][$SubjectID] != ""){
								$MarkType = (count($MarksheetScoreInfoArr) > 0) ? $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkType'] : '';
								if(count($MarksheetScoreInfoArr) > 0){
								  	if($MarkType == "SC"){
								  		$tmpRaw = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkNonNum'];
								  	} else {
								  		$tmpRaw = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkRaw'];
								  	}
										if($tmpRaw == "-" || $tmpRaw == "*" || $tmpRaw == "/" || $tmpRaw == "N.A."){
							    		$InputMethod = $tmpRaw;
						    		} else if($tmpRaw != "NOTCOMPLETED"){
							    		# for displaying mark with weight calculation
									    $tmpWeightedVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpRaw, $CalculationType);
						    			//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpWeightedVal, $storDispSettings["SubjectScore"]) : round($tmpWeightedVal, 0);
						    			$roundVal = $lreportcard->ROUND_MARK($tmpWeightedVal, "SubjectScore");
						    			$InputMethod = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $storDispSettings["SubjectScore"]);
						    			
						    			# for calculating real overall subject mark
						    			$tmpWeightedVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpRaw, $CalculationType, $AllColumnMarkArr);
						    			$OverallSubjectMark += $tmpWeightedVal;
					    			} else {
						    			$isShowOverallMark = 0;
					    			}
					    		}
							} else {
								$InputMethod = '--';
							}
						} else if($SchemeType != "PF" && $ScaleDisplay == "G"){
							if($WeightAry[$ReportColumnID][$SubjectID] != 0 && $WeightAry[$ReportColumnID][$SubjectID] != null && $WeightAry[$ReportColumnID][$SubjectID] != ""){
								$MarkType = (count($MarksheetScoreInfoArr) > 0) ? $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkType'] : '';
								if(count($MarksheetScoreInfoArr) > 0){
						    		$GradeVal = '';
						    		if($MarkType == "SC"){
						    			$displayVal = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkNonNum'];
						    		} else if($MarkType == "G"){
						    			 $displayVal = $SchemeGrade[$MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkNonNum']];	
						    		} else if($MarkType == "M"){
						    			$tmpRaw = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkRaw'];
						    			$displayVal = ($tmpRaw < 0 || $tmpRaw == "") ? "" : $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $tmpRaw);
						    		}						    	
								    $InputMethod = $displayVal;
							    }
							} else {
								$InputMethod = '--';
							}
							
						}
					} else {
						#############################################################################
						# Original Approach dealing with having Term Report & Whole Year Report
						############################################################################# ";
					    // Mark Type : M-Mark, G-Grade, PF-PassFail, SC-SpecialCase
						$MarkType = (count($MarksheetScoreInfoArr) > 0) ? $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkType'] : ''; 
				    	
				    	if($SchemeType != "PF" && $ScaleDisplay == "M" && $ScaleInput != "G"){	# View of Weighted Mark
				    		if($WeightAry[$ReportColumnID][$SubjectID] != 0 && $WeightAry[$ReportColumnID][$SubjectID] != null && $WeightAry[$ReportColumnID][$SubjectID] != ""){
							    if($ColumnHasTemplateAry[$ReportColumnID] && $ReportType == "F" && $ScaleInput == "M"){
								    if(!empty($CmpSubjectArr) && $isCalculateByCmpSub){	// case for generate the mark of parent subject from component subjects
										
								   		$tmpRaw = $WeightedMark[$ReportColumnID][$StudentID];
									    if($tmpRaw == "-" || $tmpRaw == "*" || $tmpRaw == "/" || $tmpRaw == "N.A."){
								    		$InputMethod = $tmpRaw;
							    		}
							    		else if($tmpRaw != "NOTCOMPLETED"){
							    			if($eRCTemplateSetting['Settings']['TemplateSettings_ConsolidatedReportWithTermReportAssessment_ExcludeWeightDisplay']) {
							    				$InputMethod = $tmpRaw;
							    			}
								    		else {
							    				# for displaying mark with weight calculation
											    $tmpWeightedVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpRaw, $CalculationType);
								    			//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpWeightedVal, $storDispSettings["SubjectScore"]) : round($tmpWeightedVal, 0);
								    			$roundVal = $lreportcard->ROUND_MARK($tmpWeightedVal, "SubjectScore");
								    			$InputMethod = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $storDispSettings["SubjectScore"]);;
								    		}
								    		
							    			# for calculating real overall subject mark
							    			$tmpWeightedVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpRaw, $CalculationType, $AllColumnMarkArr);
							    			//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpWeightedVal, $storDispSettings["SubjectScore"]) : round($tmpWeightedVal, 0);
							    			$OverallSubjectMark += $tmpWeightedVal;
						    			}
						    			else {
							    			$isShowOverallMark = 0;
						    			}
								    }
								    else {	# case for generate the mark of subject without any component subjects
									    if($SchemeType == "H"){
									    	
								    		$tmpVal = $WeightedMark[$ReportColumnID][$StudentID];
								    		if($tmpVal == "-" || $tmpVal == "*" || $tmpVal == "/" || $tmpVal == "N.A."){
									    		$InputMethod = $tmpVal;
								    		}
								    		else if($tmpVal != "NOTCOMPLETED" && $tmpVal != ""){
								    			if($eRCTemplateSetting['Settings']['TemplateSettings_ConsolidatedReportWithTermReportAssessment_ExcludeWeightDisplay']) {
								    				$InputMethod = $tmpVal;
								    			}
								    			else {
									    			# for displaying mark with weight calculation
									    			$tmpWeightedVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpVal, $CalculationType);
										    		//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpWeightedVal, $storDispSettings["SubjectScore"]) : round($tmpWeightedVal, 0);
										    		$roundVal = $lreportcard->ROUND_MARK($tmpWeightedVal, "SubjectScore");
										    		$InputMethod = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $storDispSettings["SubjectScore"]);
								    			}
								    			
									    		# for calculating real overall subject mark
									    		$tmpWeightedVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpVal, $CalculationType, $AllColumnMarkArr);
									    		//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpWeightedVal, $storDispSettings["SubjectScore"]) : round($tmpWeightedVal, 0);
									    		$OverallSubjectMark += $tmpWeightedVal;
							    			}
							    			else {
								    			$InputMethod = "";
								    			$isShowOverallMark = 0;
							    			}
							    		}
						    		}
							    }
							    else {
								    if(!empty($CmpSubjectArr) && $isCalculateByCmpSub){
								    	
									    $tmpRaw = $ParentSubMarksheetScoreInfoArr[$ReportColumnID][$StudentID];
									    
									    $SpecialCaseCountArr = $lreportcard->DIFFERENTIATE_SPECIAL_CASE($SpecialCaseCountArr, $tmpRaw, $absentExemptSettings);
									    
									    if($tmpRaw == "-" || $tmpRaw == "*" || $tmpRaw == "/" || $tmpRaw == "N.A."){
								    		$InputMethod = $tmpRaw;
							    		} else {
								    		# for displaying mark with weight calculation
							    			/* Replace by FL 20170217 */
								    		// $tmpVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpRaw, $CalculationType);
							    			$tmpVal = $tmpRaw;
							    			
								    		//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpVal, $storDispSettings["SubjectScore"]) : round($tmpVal, 0);
								    		$roundVal = $lreportcard->ROUND_MARK($tmpVal, "SubjectScore");
								    		$InputMethod = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $storDispSettings["SubjectScore"]);
								    		
								    		# for calculating real overall subject mark
								    		$tmpVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpRaw, $CalculationType, $AllColumnMarkArr);
								    		//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpVal, $storDispSettings["SubjectScore"]) : round($tmpVal, 0);
								    		$OverallSubjectMark += $roundVal;
							    		}
								    }
								    else {
							    		if(count($MarksheetScoreInfoArr) > 0){
								    		if($MarkType == "SC") {
								    			$tmpVal = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkNonNum'];
								    			
								    			$SpecialCaseCountArr = $lreportcard->DIFFERENTIATE_SPECIAL_CASE($SpecialCaseCountArr, $tmpVal, $absentExemptSettings);
								    			
								    			$InputMethod = $tmpVal;
							    			} 
							    			else {
								    			$tmpRaw = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkRaw'];
								    			
									    		if($tmpRaw != "" && $tmpRaw >= 0){
										    		# for displaying mark with weight calculation
										    		
										    		/* Replace by FL 20170217 */
										    		// $tmpVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpRaw, $CalculationType);
									    			$tmpVal = $tmpRaw;
										    		
										    		//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpVal, $storDispSettings["SubjectScore"]) : round($tmpVal, 0);
										    		$roundVal = $lreportcard->ROUND_MARK($tmpVal, "SubjectScore");
										    		$InputMethod = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $storDispSettings["SubjectScore"]);
										    		
										    		# for calculating real overall subject mark
										    		$tmpVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpRaw, $CalculationType, $AllColumnMarkArr);
										    		//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpVal, $storDispSettings["SubjectScore"]) : round($tmpVal, 0);
										    		$OverallSubjectMark += $tmpVal;
									    		}
									    		else {
										    		$InputMethod = "";
										    		$isShowOverallMark = 0;
									    		}
								    		}
							    		}
						    		}
					    		}
				    		}
				    		else {
							    $InputMethod = "--";
						    }
						    
				    	}
				    	else if($SchemeType != "PF" && $ScaleDisplay == "G"){	# View of Converted Grade
				    		if($WeightAry[$ReportColumnID][$SubjectID] != 0 && $WeightAry[$ReportColumnID][$SubjectID] != null && $WeightAry[$ReportColumnID][$SubjectID] != ""){
							    if($ColumnHasTemplateAry[$ReportColumnID] && $ReportType == "F"){	// according to student's term subject mark calculated by term report
							   		if(($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF"){
									    if(count($MarksheetOverallScoreInfoArr) > 0){
										    $tmpType = $MarksheetOverallScoreInfoArr[$ReportColumnID][$StudentID]['MarkType'];
										    $tmpNonNum = $MarksheetOverallScoreInfoArr[$ReportColumnID][$StudentID]['MarkNonNum'];
										    if($tmpType != "SC")
										    	$InputMethod = ($SchemeType == "PF") ? $SchemePassFail[$tmpNonNum] : $SchemeGrade[$tmpNonNum];
										    else 
										    	$InputMethod = $tmpNonNum;
									    }
								    }
								    else if($ScaleInput == "M"){
									    if($SchemeType == "H"){
								    		$tmpVal = $WeightedMark[$ReportColumnID][$StudentID];
								    		
								    		if($tmpVal == "+" || $tmpVal == "-" || $tmpVal == "abs"){
									    		$InputMethod = "abs";
								    		} else if($tmpVal == "/" || $tmpVal == "*" || $tmpVal == "N.A."){
									    		$InputMethod = $tmpVal;
								    		} else if($tmpVal != "NOTCOMPLETED" || $tmpVal != ""){
								    			$InputMethod = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $tmpVal);
							    			} else {
								    			$InputMethod = "";
							    			}
							    			
							    		}
						    		}
							    }
							    else {	# according to student's term subject mark provided by teacher without term report
							    	if(count($MarksheetScoreInfoArr) > 0){
							    		$GradeVal = '';
							    		
							    		if($ScaleInput == "G"){ 		// Input Scale is Grade
								    		$tmpVal = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkNonNum'];
									    	$GradeVal = ($MarkType == "SC") ? $tmpVal : $SchemeGrade[$tmpVal];
								    	}
								    	else if($ScaleInput == "M"){	// Input Scale is Mark
								    		$tmpRaw = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkRaw'];
									    	$tmpNonNum = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkNonNum'];
									    	if($MarkType == "SC")
									    	{
									    		$GradeVal = $tmpNonNum;
								    		}
									    	else 
									    	{
									    		$GradeVal = ($tmpRaw < 0 || $tmpRaw == "") ? "" : $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $tmpRaw);
									    		
									    	}
								    	}
								    	
									    $InputMethod = $GradeVal;
								    }
								    else
								    {
										if ($ScaleInput=="M" && $ScaleDisplay=="G")
										{
											$thisScore = $ParentSubMarksheetScoreInfoArr[$ReportColumnID][$StudentID];
											$GradeVal = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisScore, $ReportID, $SubjectID, $ClassLevelID, $ReportColumnID);
											
											if (is_numeric($GradeVal))
												$GradeVal = $lreportcard->ROUND_MARK($GradeVal, "SubjectScore");
										}   
										$InputMethod = $GradeVal;
								    }
							    }

						    }
						    else {
							    $InputMethod = "--";
						    }
						    
				    	}
		      		} 
		      		
		      		$InputMethod = ($InputMethod == '')? '&nbsp;' : $InputMethod;
				    $StudentMarkSheetScore[$ReportColumnID][$StudentID]["SubjectTotal"]= $InputMethod;
				    $postValue .= "<input type='hidden' name='Score[$StudentID][$SubjectID][]' value='$InputMethod'>";
		      		
		      		//$display .= '<td align="center" class="'.$td_css.' tabletext">'.$InputMethod.'</td>';
      			} # End of Report Column Loop
	      		# Generate Input Selection Box For ScaleInput is Grade(G) OR SchemeType is PassFail(PF)
				if($SchemeType != "PF" && $ScaleDisplay == "G"){
		      		$mark_name = "overall_mark_".$StudentID;
		      		$mark_id = "overall_mark_".$StudentID;
		      		$MarkType = (count($MarksheetOverallScoreInfoArr) > 0) ? $MarksheetOverallScoreInfoArr[$StudentID]['MarkType'] : "";
		      		$tmpValue = (count($MarksheetOverallScoreInfoArr) > 0) ? $MarksheetOverallScoreInfoArr[$StudentID]['MarkNonNum'] : "";
		      		
		      		if($SchemeType == "H" && $ScaleInput == "G"){
			      		$GradeVal = ($MarkType == "SC") ? $tmpValue : $SchemeGrade[$tmpValue];
		      		}
		      		$StudentMarkSheetScore[$ReportColumnID][$StudentID]["SubjectTotal"]= $GradeVal;
		      		$postValue .= "<input type='hidden' name='Score[$StudentID][$SubjectID][]' value='$GradeVal'>";
		      		
	      		}
	      		
				
	    	} //end if(count($column_data) > 0)
	        
		}

	#
	#	Get Marksheet Score End
	############################################################################################	
	
	####### Build Table
	//THEAD
	if($ViewOnly && !$lreportcard->IS_ADMIN_USER($_SESSION['UserID'])) {
		$ChangeAllToSelectBox = "&nbsp;";
	}
	else {
		$ChangeAllToSelectBox = getSelectByArray($StatusAry," onchange='SetAllStatus(this)' class='tabletexttoptext' ",$targetStatus,1,0, "---".$eReportCard['ChangeAllTo']."---");
	}
	
	$table .= '	<table width="100%" border="0" cellspacing="0" cellpadding="4">
					<tr class="tablegreentop">
						<td class="tabletopnolink">'.$eReportCard["StudentNo_short"].'</td>
						<td class="tabletopnolink">'.$eReportCard["Student"].'</td>';
	foreach($ColumnArr as $Column)
	{
		$table .= '			<td class="tabletopnolink">'.$ColumnTitleArr[$Column["ReportColumnID"]].'</td>';	
		$postValue .= '<input type="hidden" name="ColumnTitle[]" value="'.$ColumnTitleArr[$Column["ReportColumnID"]].'">';
	}
	$table .= '			<td align="center" class="tabletopnolink">'.$eReportCard["Feedback"].'</td>
						<td class="tabletopnolink">'.$eReportCard["LastModifiedDate"].'</td>
						<td align="center" class="tabletopnolink">'.$ChangeAllToSelectBox.'</td>
					</tr>
			';

	//TBODY			
	foreach ($StudentArr as $StudentInfo)
	{
		$thisStudentID = $StudentInfo["UserID"];
		$thisFeedbackInfo = $lreportcard->Get_Marksheet_Feedback_Info($ReportID, $SubjectID, $thisStudentID);
		$thisFeedbackInfo = $thisFeedbackInfo[0];
		$thisStatus = $thisFeedbackInfo["RecordStatus"];
		if(!empty($targetStatus) && $thisStatus!=$targetStatus) continue;
		if(empty($thisFeedbackInfo["LastModifiedBy"])&&empty($thisFeedbackInfo["DateModified"])) 
			$thisLastModifiedDate = "-";
		else
			$thisLastModifiedDate = $thisFeedbackInfo["LastModifiedBy"]."<br>".$thisFeedbackInfo["DateModified"];
		
		if($ViewOnly && !$lreportcard->IS_ADMIN_USER($_SESSION['UserID'])) {
			$thisStatusSelectBox = $StatusMappingAry[$thisStatus][1];
		}
		else {
			$thisStatusSelectBox = getSelectByArray($StatusAry," class='tabletexttoptext' name='Status[$thisStudentID]'",$thisStatus,1,0, "---");
		}

		$row_css = "attendancepresent";
		
		$table .= '	<tr class="tablegreenrow1" height="38">';
		$table .= '		<td class="'.$row_css.' tabletext">'.$StudentInfo['ClassNumber'].'</td>';
		$table .= '		<td class="'.$row_css.' tabletext">'.$StudentInfo['StudentName'].'</td>';
		foreach($ColumnArr as $Column)
		{
			//$thisColumnScoreInfo = $StudentMarkSheetScore[$Column['ReportColumnID']][$thisStudentID];
			//$thisColumnScore = $thisColumnScoreInfo['MarkRaw'] == -1?$thisColumnScoreInfo['MarkNonNum']:$thisColumnScoreInfo['MarkRaw'];
			//debug_pr($StudentMarkSheetScore);
			$thisReportColumnID = $Column['ReportColumnID'];
			$thisColumnWeight = $SubjectWeightAry[$thisReportColumnID];
			
			if ($thisColumnWeight == 0 && $ScaleInput=='M') {
				$thisColumnScore = $Lang['General']['EmptySymbol'];
			}
			else {
				$thisColumnScore = $StudentMarkSheetScore[$thisReportColumnID][$thisStudentID]["SubjectTotal"];
				$thisColumnScore = !empty($thisColumnScore)?$thisColumnScore:'-';
			}
			$table .= '		<td class="'.$row_css.' tabletext">'.$thisColumnScore.'</td>';
		}
		$table .= '		<td align="center" class="'.$row_css.' tabletext"><a href="javascript:ViewFeedback('.$thisStudentID.')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" alt="'.$button_view.' '.$eReportCard['Feedback'].'"></a></td>';
		$table .= '		<td class="'.$row_css.' tabletext">'.$thisLastModifiedDate.'</td>';
		$table .= '		<td align="center" class="'.$row_css.' tabletext">'.$thisStatusSelectBox.'</td>';
		$table .= '	</tr>';
		
		$ctr++;
	}
	
	//TFOOT
	$total = $eReportCard['TotalRecords'].": ";
	$total .= $ctr;	
	$table .= '	<tr class="tablegreenbottom">
				    <td colspan="'.(count((array)$ColumnArr) + 5).'">
				      <table width="100%" border="0" cellspacing="0" cellpadding="3">
				        <tr>
				    	  <td class="tabletext">'.$total.'</td>
				    	</tr>
				      </table>
				    </td>
				  </tr>';
	
	$table .= '</table>';
	####### Table End
	
	# Title 
	$PAGE_TITLE = $eReportCard['Management_MarksheetVerification'];
	     
	# tag information
	$TAGS_OBJ[] = array($PAGE_TITLE, "", 0);
	
	$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$Msg]);
	
	
	$redirectParm = "ReportID=$ReportID&ClassLevelID=$ClassLevelID";
	$obj_Subject = new subject($SubjectID);
	if ($obj_Subject->Is_Component_Subject())
	{
		$obj_SubjectComponent = new Subject_Component($SubjectID);
		$ParentSubjectInfoArr = $obj_SubjectComponent->Get_Parent_Subject();
		$ParentSubjectID = $ParentSubjectInfoArr['RecordID'];
		
		if ($SubjectGroupID != '')
			$redirectLocation = "../marksheet_revision2_subject_group.php?$redirectParm"."&SubjectID=$ParentSubjectID&SubjectGroupID=$SubjectGroupID";
		else
			$redirectLocation = "../marksheet_revision2.php?$redirectParm"."&SubjectID=$ParentSubjectID"."&ClassID=$ClassID";
	}
	else
	{
		if ($SubjectGroupID != '')
			$redirectLocation = "../marksheet_revision_subject_group.php?$redirectParm"."&SubjectID=$SubjectID&SubjectGroupID=$SubjectGroupID";
		else
			$redirectLocation = "../marksheet_revision.php?$redirectParm"."&SubjectID=$SubjectID";
	}
	
?>
<script>
function ViewFeedback(StudentID)
{
	var url = "../../marksheet_feedback/view_feedback.php?";
	url += "StudentID="+StudentID;
	
	document.form1.action= url;
	document.form1.submit();
	
}

function SetAllStatus(obj)
{
	$("select[name^='Status']").val(obj.value);
}

function SetAction(url)
{
	document.form1.action = url;
}

function js_Export()
{
	document.form1.action= './export_feedback.php';
	document.form1.submit();
	document.form1.action= '';
}
</script>

<br/>
<form name="form1" method="POST" action="">
	<div class="content_top_tool">
		<div class="Conntent_tool">
			<?=$ExportBtn?>
		</div>
	</div>
	<table width="98%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<?=$StatusFilter?>
				</table>
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<?=$table?>
					</td>
				</tr>
			</table><br></td>
		</tr>
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="1" height="1"></td></tr>
		<tr>
        	<td align="left" valign="bottom"><span class="tabletextremark"><?=$eReportCard['DeletedStudentLegend']?></span></td>
        </tr>
		<tr>
			<td align="center">
			<br />
			<? if($lreportcard->IS_ADMIN_USER($_SESSION['UserID']) || !$ViewOnly) { ?>
				<?=$linterface->GET_ACTION_BTN($button_save,"submit","SetAction('status_update.php')")?>
				<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="1" height="1">
			<? } ?>
				<?=$linterface->GET_ACTION_BTN($button_back, "button", "window.location.href='".$redirectLocation."'");?>
			</td>
		</tr>
	</table>
	<input type='hidden' name='ClassLevelID' value='<?=$ClassLevelID?>'>
	<input type='hidden' name='ReportID' value='<?=$ReportID?>'>
	<input type='hidden' name='SubjectID' value='<?=$SubjectID?>'>
	<input type='hidden' name='SubjectGroupID' value='<?=$SubjectGroupID?>'>
	<input type='hidden' name='ClassID' value='<?=$ClassID?>'>
	<input type='hidden' name='ParentSubjectID' value='<?=$ParentSubjectID?>'>
	<?=$postValue?>
</form>
	
<br />
<?
	$linterface->LAYOUT_STOP();
}
?>