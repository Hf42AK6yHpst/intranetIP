<?php

/**	Change Log
 * 2016-01-29	Kenneth 	[2016-0105-1050-50207]
 * 		- php is Created
 */

$PATH_WRT_ROOT = "../../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	$lreportcard = new libreportcard();
	
	if ($lreportcard->hasAccessRight()) {

		# Class
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		$ClassNameAssoc = BuildMultiKeyAssoc($ClassArr,"ClassID","Class");

		if ($SubjectGroupIDList != '' )  // Subject Group Parent Subject
		{
			foreach((array)$SubjectGroupIDList as $SubjectGroupID)
			{
				$StudentArray  = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupID, $ClassLevelID, '', 1);
				$obj_SubjectGroup = new subject_term_class($SubjectGroupID);
				$SubjectGroupName = $obj_SubjectGroup->Get_Class_Title();
				$thisSubjectID = $obj_SubjectGroup->SubjectID;
				$SubjectName = $lreportcard->GET_SUBJECT_NAME_LANG($thisSubjectID);
				$Class_SG_Title = "Subject Group";
				
				foreach($StudentArray as $StudentInfo)
				{
					$thisStudentID = $StudentInfo['UserID'];
					
					# Comment
					$returnArrComment = $lreportcard->GET_TEACHER_COMMENT($thisStudentID, $SubjectIDList, $ReportID);
					$thisComment = $returnArrComment[$thisStudentID][$thisSubjectID]['Comment'];
					
					$ExportColArr = array();
					$ExportColArr[] = $SubjectName;
					$ExportColArr[] = $SubjectGroupName;
					$ExportColArr[] = $StudentInfo["ClassName"];
					$ExportColArr[] = $StudentInfo["ClassNumber"];
					$ExportColArr[] = $StudentInfo["StudentName"];
					$ExportColArr[] = $thisComment;
					$ExportArr[] = $ExportColArr;
				}
			}
		}
//		else if( $SubjectGroupID != '' ) // Subject Group Component Subject
//		{		
//			$StudentArray  = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupID, $ClassLevelID, '', 1);
//			$obj_SubjectGroup = new subject_term_class($SubjectGroupID);
//			$SubjectGroupName = $obj_SubjectGroup->Get_Class_Title();
//			
//			foreach((array)$SubjectIDList as $thisSubjectID)
//			{
//				$SubjectName = $lreportcard->GET_SUBJECT_NAME_LANG($thisSubjectID);
//				
//				foreach($StudentArray as $StudentInfo)
//				{
//					$thisStudentID = $StudentInfo['UserID'];
////					if(empty($FeedBackArr[$thisStudentID][$thisSubjectID])) continue;
//					
//					# Status
//					switch($FeedBackArr[$thisStudentID][$thisSubjectID]['RecordStatus'])
//					{
//						case 1: $thisStatus = $eReportCard['Closed']; break;
//						case 2: $thisStatus = $eReportCard['InProgress']; break;
//						default: $thisStatus = $Lang['General']['EmptySymbol']; 
//					}
//		
//					# Comment
//					if(empty($FeedBackArr[$thisStudentID][$thisSubjectID]))
//					{
//						$thisComment = '';
//						$thisStatus = $Lang['General']['EmptySymbol']; 
//					}
//					else
//					{
//						$CommentArr = $lreportcard->Fetch_Feedback_Detail($FeedBackArr[$thisStudentID][$thisSubjectID]['Comment']);
//						$thisComment = '';
//						foreach((array)$CommentArr as $thisCommentArr )
//						{
//							$thisComment .= 	$thisCommentArr['Name']." ".$thisCommentArr['DateInput']."\n".$thisCommentArr['Comment']."\n";
//						}
//					}
//					$ExportColArr = array();
//					$ExportColArr[] = $SubjectName;
//					$ExportColArr[] = $SubjectGroupName;
//					$ExportColArr[] = $StudentInfo["ClassName"];
//					$ExportColArr[] = $StudentInfo["ClassNumber"];
//					$ExportColArr[] = $StudentInfo["StudentName"];
//					$ExportColArr[] = $thisComment;
//					$ExportColArr[] = $FeedBackArr[$thisStudentID][$thisSubjectID]['DateModified'];
//					$ExportColArr[] = $thisStatus;
//					$ExportArr[] = $ExportColArr;
//				}
//			}
//		}
		else if($ClassIDList != '' && $ParentSubjectID == '')
		{
			foreach((array)$ClassIDList as $ClassID)
			{
				foreach((array)$SubjectIDList as $thisSubjectID)
				{
					$StudentArray = $lreportcard->Get_Marksheet_Accessible_Student_List_By_Subject_And_Class($ReportID, $_SESSION['UserID'], $ClassID, $ParentSubjectID, $thisSubjectID, $showStyle=false);
					$StudentList = Get_Array_By_Key($StudentArr,"UserID");
					$SubjectName = $lreportcard->GET_SUBJECT_NAME_LANG($thisSubjectID);
					
					foreach($StudentArray as $StudentInfo)
					{
						$thisStudentID = $StudentInfo['UserID'];

						# Commnet
						$returnArrComment = $lreportcard->GET_TEACHER_COMMENT($thisStudentID, $SubjectIDList, $ReportID);
						$thisComment = $returnArrComment[$thisStudentID][$thisSubjectID]['Comment'];

						$ExportColArr = array();
						$ExportColArr[] = $SubjectName;
						$ExportColArr[] = $StudentInfo["ClassName"];
						$ExportColArr[] = $StudentInfo["ClassNumber"];
						$ExportColArr[] = $StudentInfo["StudentName"];
						$ExportColArr[] = $thisComment;
						$ExportArr[] = $ExportColArr;
					}
				}
			}
		}
		
		$lexport = new libexporttext();
		
		$exportColumn = array();
		
		foreach(array("En","Ch") as $LangName)
		{
			$exportColumnRow = array();
			$exportColumnRow[] = $eReportCard['ExportHeader']['Subject'][$LangName];
			if($SubjectGroupName)
				$exportColumnRow[] = $eReportCard['ExportHeader']['SubjectGroup'][$LangName];
			$exportColumnRow[] = $eReportCard['ExportHeader']['ClassName'][$LangName];
			$exportColumnRow[] = $eReportCard['ExportHeader']['ClassNumber'][$LangName];
			$exportColumnRow[] = $eReportCard['ExportHeader']['StudentName'][$LangName];
			$exportColumnRow[] = $eReportCard['ExportHeader']['TeacherComment'][$LangName];
			$exportColumn[] = $exportColumnRow;
		}
		
		$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn,  "", "\r\n", "", 0, "11",1);
		
		intranet_closedb();
		
		// Output the file to user browser
		$filename = "export_comment.csv";
//		$filename = str_replace(' ', '_', $filename);
		$lexport->EXPORT_FILE($filename, $export_content);
	} else {
		echo "You have no priviledge to access this page.";
	}
} else {
	echo "You have no priviledge to access this page.";
}

?>