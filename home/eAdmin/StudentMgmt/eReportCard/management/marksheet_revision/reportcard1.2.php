<?php
# Editing by 
@SET_TIME_LIMIT(1000);

$eRC_Addon_PATH_WRT_ROOT = "../../";
include_once($eRC_Addon_PATH_WRT_ROOT."includes/global.php");
include_once($eRC_Addon_PATH_WRT_ROOT."includes/libdb.php");
include_once($eRC_Addon_PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($eRC_Addon_PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($eRC_Addon_PATH_WRT_ROOT."includes/libaccount.php");
include_once("../check.php");

if ($_REQUEST['direct_run'] == 1)
	intranet_opendb();
	
$li = new libdb();
$lreportcard = new libreportcard();

if ($plugin['ReportCard2008'])
{

	# script : create file directory if folder does not exist
	$lo = new libfilesystem();
	$lo->folder_new($intranet_root."/file/reportcard2008/");
	$lo->folder_new($intranet_root."/file/reportcard2008/templates/");
	
	# create a text file storing current year used for database naming
	if (!file_exists($intranet_root."/file/reportcard2008/db_year.txt")) {
		$eRC_AcademicYear = $lreportcard->GET_ACADEMIC_YEAR();
		$eRC_AcademicYear = ($eRC_AcademicYear=='')? '2009' : $eRC_AcademicYear;
		$lo->file_write($eRC_AcademicYear, $intranet_root."/file/reportcard2008/db_year.txt");
		$lreportcard = new libreportcard();
	}
	?>
	<html>
	<head>
	<title>Script : Create eReportCard 1.2 Database & Tables</title>
	</head>
	<body>
	<h1>Script : Create eReportCard 1.2 Database & Tables</h1>
	<?php
	# The Suffix should be change according to  different academic year, 
	# e.g. DB_REPORT_CARD_2008, DB_REPORT_CARD_2009
	
	
	
	$reportcard_db = $lreportcard->DBName;
	
	# Create Database
	if ($li->db_create_db($reportcard_db)){
		print("<p>Database created successfully</p>\n");
	} else {
		print("<p>Error creating database: ".mysql_error()."</p>\n");
	}
	
	$sql = "SHOW DATABASES LIKE '%".$intranet_db."_DB_REPORT_CARD_%'";
	$DatabaseAry = $li->returnArray($sql);
	
	for($a=0; $a<sizeof($DatabaseAry);$a++)
	{
		$sql_table = array();
		$sql_alter = array();
		
		$reportcard_db = $DatabaseAry[$a][0];
		
		// ignore backup DB like "intranet_DB_REPORT_CARD_2007_UTF82"
		$tmpDBNameArr = explode('_', $reportcard_db);
		$numOfDBNamePart = count($tmpDBNameArr);
		$targetCheckingText = $tmpDBNameArr[$numOfDBNamePart - 1];
		if (strtolower(substr($targetCheckingText, 0, 4)) == 'utf8')
			continue;
		if (strpos($reportcard_db, '_RUBRICS_') !== false)
			continue;	
		
	
		print("<p>Creating tables for ".$DatabaseAry[$a][0]."...</p>\n");
		
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_CLASS_COMMENT_PROGRESS (
		  ClassCommentProgressID int(8) NOT NULL auto_increment,
		  ReportID int(11) default NULL,
		  ClassID int(11) default NULL,
		  TeacherID int(11) default NULL,
		  IsCompleted tinyint(4) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (ClassCommentProgressID),
		  KEY ReportID (ReportID),
		  KEY ClassID (ClassID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_COMMENT_BANK (
		  CommentID int(11) NOT NULL auto_increment,
		  CommentCode varchar(16) default NULL,
		  CommentCategory varchar(64) default NULL,
		  IsSubjectComment tinyint(4) default NULL,
		  CommentEng varchar(255) default NULL,
		  Comment varchar(255) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (CommentID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_GRADING_SCHEME (
		  SchemeID int(8) NOT NULL auto_increment,
		  SchemeTitle varchar(128) default NULL,
		  SchemeType char(2) default NULL,
		  FullMark float default NULL,
		  PassMark float default NULL,
		  TopPercentage tinyint(4) default NULL,
		  Pass varchar(16) default NULL,
		  Fail varchar(16) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (SchemeID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_GRADING_SCHEME_RANGE (
		  GradingSchemeRangeID int(8) NOT NULL auto_increment,
		  SchemeID int(8) default NULL,
		  Nature char(2) default NULL,
		  LowerLimit float default NULL,
		  UpperLimit float default NULL,
		  Grade varchar(4) default NULL,
		  GradePoint float default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (GradingSchemeRangeID),
		  KEY SchemeID (SchemeID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_MARKSHEET_COMMENT (
		  CommentID int(8) NOT NULL auto_increment,
		  StudentID int(11) default NULL,
		  SubjectID int(8) default NULL,
		  ReportID int(11) default NULL,
		  Comment text,
		  TeacherID int(11) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (CommentID),
		  KEY StudentID (StudentID),
		  KEY SubjectID (SubjectID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_MARKSHEET_FEEDBACK (
		  FeedbackID int(8) NOT NULL auto_increment,
		  SubjectID int(8) default NULL,
		  ReportID int(11) default NULL,
		  StudentID int(11) default NULL,
		  Comment text,
		  IsTeacherRead tinyint(4) default '0',
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (FeedbackID),
		  KEY SubjectID (SubjectID),
		  KEY ReportID (ReportID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_MARKSHEET_OVERALL_SCORE (
		  MarksheetOverallScoreID int(11) NOT NULL auto_increment,
		  StudentID int(11) default NULL,
		  SubjectID int(11) default NULL,
		  ReportID int(11) default NULL,
		  SchemeID int(11) default NULL,
		  MarkType char(2) default NULL,
		  MarkNonNum varchar(11) default NULL,
		  LastModifiedUserID int(11) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (MarksheetOverallScoreID),
		  UNIQUE KEY RC_MARKSHEET_OVERALL_SCORE_UK1 (ReportID,SubjectID,StudentID),
		  KEY StudentID (StudentID),
		  KEY SubjectID (SubjectID),
		  KEY SchemeID (SchemeID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_MARKSHEET_SCORE (
		  MarksheetScoreID int(11) NOT NULL auto_increment,
		  StudentID int(11) default NULL,
		  SubjectID int(11) default NULL,
		  ReportColumnID int(11) default NULL,
		  SchemeID int(11) default NULL,
		  MarkType char(2) default NULL,
		  MarkRaw float default '-1',
		  MarkNonNum varchar(11) default NULL,
		  LastModifiedUserID int(11) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (MarksheetScoreID),
		  UNIQUE KEY RC_MARKSHEET_SCORE_UK1 (ReportColumnID,SubjectID,StudentID),
		  KEY StudentID (StudentID),
		  KEY SubjectID (SubjectID),
		  KEY SchemeID (SchemeID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_MARKSHEET_SUBMISSION_PROGRESS (
		  MarksheetSubmissionProgressID int(11) NOT NULL auto_increment,
		  TeacherID int(11) default NULL,
		  SubjectID int(11) default NULL,
		  ClassID int(11) default NULL,
		  IsCompleted tinyint(4) default NULL,
		  ReportID int(11) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (MarksheetSubmissionProgressID),
		  KEY TeacherID (TeacherID),
		  KEY SubjectID (SubjectID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_MARKSHEET_VERIFICATION_PROGRESS (
		  VerificationProgressID int(8) NOT NULL auto_increment,
		  SubjectID int(8) default NULL,
		  ReportID int(11) default NULL,
		  StudentID int(11) default NULL,
		  ClassLevelID int(11) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (VerificationProgressID),
		  KEY SubjectID (SubjectID),
		  KEY ReportID (ReportID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_OTHER_STUDENT_INFO (
		  OtherStudentInfoID int(8) NOT NULL auto_increment,
		  StudentID int(11) default NULL,
		  ReportID int(11) default NULL,
		  CIPHours float default NULL,
		  Attendance tinyint(4) default NULL,
		  Conduct tinyint(4) default NULL,
		  CCA text,
		  NAPFA tinyint(4) default NULL,
		  TeacherID int(11) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (OtherStudentInfoID),
		  UNIQUE KEY RC_OTHER_STUDENT_INFO_UK1 (StudentID,ReportID),
		  KEY StudentID (StudentID),
		  KEY ReportID (ReportID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_REPORT_RESULT (
		  ResultID int(8) NOT NULL auto_increment,
		  StudentID int(11) default NULL,
		  ReportID int(8) default NULL,
		  ReportColumnID int(11) default NULL,
		  GrandTotal float default NULL,
		  GrandAverage float default NULL,
		  GPA float default NULL,
		  OrderMeritClass int(11) default NULL,
		  OrderMeritForm int(11) default NULL,
		  Promotion tinyint(4) default NULL,
		  Passed tinyint(4) default NULL,
		  NewClassName varchar(100) default NULL,
		  Semester char(2) default NULL,
		  AdjustedBy int(11) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (ResultID),
		  KEY StudentID (StudentID),
		  KEY ReportID (ReportID),
		  KEY ReportColumnID (ReportColumnID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_REPORT_RESULT_ARCHIVE (
		  ResultID int(8) NOT NULL auto_increment,
		  StudentID int(11) default NULL,
		  ReportID int(8) default NULL,
		  ReportColumnID int(11) default NULL,
		  GrandTotal float default NULL,
		  GrandAverage float default NULL,
		  GPA float default NULL,
		  OrderMeritClass int(11) default NULL,
		  OrderMeritForm int(11) default NULL,
		  Promotion tinyint(4) default NULL,
		  Passed tinyint(4) default NULL,
		  NewClassName varchar(100) default NULL,
		  Semester char(2) default NULL,
		  AdjustedBy int(11) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (ResultID),
		  KEY StudentID (StudentID),
		  KEY ReportID (ReportID),
		  KEY ReportColumnID (ReportColumnID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_REPORT_RESULT_FULLMARK (
		  ReportResultFullmarkID int(11) NOT NULL auto_increment,
		  StudentID int(11) default NULL,
		  ReportID int(11) default NULL,
		  ReportColumnID int(11) default NULL,
		  SubjectID int(11) default NULL,
		  FullMark float default NULL,
		  Semester char(2) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (ReportResultFullmarkID),
		  KEY StudentID (StudentID),
		  KEY SubjectID (SubjectID),
		  KEY ReportID (ReportID),
		  KEY ReportColumnID (ReportColumnID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_REPORT_RESULT_SCORE (
		  ReportResultScoreID int(11) NOT NULL auto_increment,
		  StudentID int(11) default NULL,
		  SubjectID int(11) default NULL,
		  ReportID int(11) default NULL,
		  ReportColumnID int(11) default NULL,
		  Mark float default NULL,
		  Grade varchar(8) default NULL,
		  IsOverall tinyint(4) default '0',
		  Semester char(2) default NULL,
		  OrderMeritClass int(11) default NULL,
		  OrderMeritForm int(11) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (ReportResultScoreID),
		  KEY StudentID (StudentID),
		  KEY SubjectID (SubjectID),
		  KEY ReportID (ReportID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_REPORT_RESULT_SCORE_ARCHIVE (
		  MarksheetConsolidatedID int(11) NOT NULL auto_increment,
		  StudentID int(11) default NULL,
		  SubjectID int(11) default NULL,
		  ReportID int(11) default NULL,
		  ReportColumnID int(11) default NULL,
		  MarkWeighted float default NULL,
		  Grade varchar(8) default NULL,
		  IsOverall tinyint(4) default '0',
		  Semester char(2) default NULL,
		  OrderMeritClass int(11) default NULL,
		  OrderMeritForm int(11) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (MarksheetConsolidatedID),
		  KEY StudentID (StudentID),
		  KEY SubjectID (SubjectID),
		  KEY ReportID (ReportID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_REPORT_TEMPLATE (
		  ReportID int(11) NOT NULL auto_increment,
		  ReportTitle varchar(255) default NULL,
		  Semester char(2) default NULL,
		  Description varchar(255) default NULL,
		  ClassLevelID int(11) default NULL,
		  HeaderHeight int(2) default NULL,
		  Footer varchar(255) default NULL,
		  LineHeight int(2) default NULL,
		  AllowClassTeacherComment tinyint(1) default NULL,
		  CommentLengthClassTeacher int(2) default NULL,
		  AllowSubjectTeacherComment tinyint(1) default NULL,
		  CommentLengthSubjectTeacher int(2) default NULL,
		  ShowOverallPositionClass tinyint(1) default NULL,
		  ShowOverallPositionForm tinyint(1) default NULL,
		  OverallPositionRangeClass varchar(10) default NULL,
		  OverallPositionRangeForm varchar(10) default NULL,
		  HideNotEnrolled tinyint(1) default NULL,
		  DisplaySettings longblob,
		  PercentageOnColumnWeight tinyint(1) default NULL,
		  MarksheetSubmissionStart datetime default NULL,
		  MarksheetSubmissionEnd datetime default NULL,
		  MarksheetVerificationStart datetime default NULL,
		  MarksheetVerificationEnd datetime default NULL,
		  ShowSubjectFullMark tinyint(1) default NULL,
		  ShowSubjectOverall tinyint(1) default NULL,
		  Issued date default NULL,
		  LastPrinted datetime default NULL,
		  LastAdjusted datetime default NULL,
		  LastGenerated datetime default NULL,
		  LastMarksheetInput datetime default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (ReportID),
		  KEY ClassLevelID (ClassLevelID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_REPORT_TEMPLATE_COLUMN (
		  ReportColumnID int(11) NOT NULL auto_increment,
		  ReportID int(8) default NULL,
		  ColumnTitle varchar(128) default NULL,
		  DefaultWeight float default NULL,
		  DisplayOrder int(2) default NULL,
		  ShowPosition tinyint(4) default NULL,
		  PositionRange varchar(10) default NULL,
		  SemesterNum int(2) default NULL,
		  IsDetails tinyint(1) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (ReportColumnID),
		  KEY ReportID (ReportID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_REPORT_TEMPLATE_SUBJECT_WEIGHT (
		  ReportColumnSubjectID int(11) NOT NULL auto_increment,
		  ReportID int(8) default NULL,
		  ReportColumnID int(11) default NULL,
		  SubjectID int(11) default NULL,
		  IsDisplay tinyint(4) default NULL,
		  Weight float default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (ReportColumnSubjectID),
		  KEY ReportID (ReportID),
		  KEY ReportColumnID (ReportColumnID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_SETTINGS (
		  SettingID int(8) NOT NULL auto_increment,
		  SettingCategory varchar(32) default NULL,
		  SettingKey varchar(32) default NULL,
		  SettingValue varchar(128) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (SettingID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_SUBJECT_FORM_GRADING (
		  SubjectFormGradingID int(11) NOT NULL auto_increment,
		  SubjectID int(11) default NULL,
		  ClassLevelID int(11) default NULL,
		  DisplayOrder int(2) default NULL,
		  SchemeID int(8) default NULL,
		  ScaleInput char(2) default NULL,
		  ScaleDisplay char(2) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (SubjectFormGradingID),
		  KEY SubjectID (SubjectID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_SUB_MARKSHEET_SCORE (
		  ColumnID int(11) default NULL,
		  StudentID int(11) default NULL,
		  MarkRaw float default NULL,
		  TeacherID int(11) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  UNIQUE KEY RC_SUB_MARKSHEET_STD (ColumnID,StudentID),
		  KEY ColumnID (ColumnID),
		  KEY StudentID (StudentID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_SUB_MARKSHEET_COLUMN (
		 ColumnID int(11) NOT NULL auto_increment,
		 SubjectID int(8),
		 ReportColumnID int(8),
		 ColumnTitle varchar(128),
		 Ratio float,
		 DateInput datetime,
		 DateModified datetime,
		 PRIMARY KEY (ColumnID),
		 INDEX ReportColumnID (ReportColumnID),
		 INDEX SubjectID (SubjectID),
		 INDEX ColumnTitle (ColumnTitle)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		
		##################################
		# Tables for St. Stephen - START (20080715/YatWoon)
		##################################
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.`RC_CURRICULUM_EXPECTATION` 
		(
		  `CurExpID` int(8) NOT NULL auto_increment,
		  `ClassLevelID` int(11) default NULL,
		  `SubjectID` int(11) default NULL,
		  `Content` text default NULL,
		  `DateInput` datetime default NULL,
		  `DateModified` datetime default NULL,
		  PRIMARY KEY  (`CurExpID`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.`RC_PERSONAL_CHARACTERISTICS` 
		(
		  `CharID` int(8) NOT NULL auto_increment,
		  `Title_EN` varchar (255) default NULL,  
		  `Title_CH` varchar (255) default NULL,  
		  `DateInput` datetime default NULL,
		  `DateModified` datetime default NULL,
		  PRIMARY KEY  (`CharID`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.`RC_PERSONAL_CHARACTERISTICS_DATA` 
		(
			`CharRecordID` int(8) NOT NULL auto_increment,
			`StudentID` int(11) default NULL,
			`SubjectID` int(8) default NULL,
			`ReportID` int(11) default NULL,
			`CharData` text,
			`TeacherID` int(11) default NULL,
			`DateInput` datetime default NULL,
			`DateModified` datetime default NULL,
			PRIMARY KEY  (`CharRecordID`),
			KEY `StudentID` (`StudentID`),
			KEY `SubjectID` (`SubjectID`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.`RC_LEVEL_SUBJECT_PERSONAL_CHARACTERISTICS` 
		(
		  `LSCharID` int(8) NOT NULL auto_increment,
		  `ClassLevelID` int(11) default NULL,
		  `SubjectID` int(11) default NULL,
		  `CharID` int(11) default NULL,
		  `DateModified` datetime default NULL,
		  PRIMARY KEY  (`LSCharID`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		##################################
		# Tables for St. Stephen - END
		##################################
		
		
		##################################
		# Added on 20080829, Table for UCC (KE)
		# Intented for storing "Effort", which is an additional info to subject
		##################################
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_EXTRA_SUBJECT_INFO (
		  ExtraInfoID int(8) NOT NULL auto_increment,
		  StudentID int(11) default NULL,
		  SubjectID int(8) default NULL,
		  ReportID int(11) default NULL,
		  Info varchar(255) default NULL,
		  TeacherID int(11) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY  (ExtraInfoID),
		  KEY StudentID (StudentID),
		  KEY SubjectID (SubjectID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		
		## Manual Adjustment
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_MANUAL_ADJUSTMENT (
			ManualAdjustmentID int(11) NOT NULL auto_increment,
			ReportID int(11),
			ReportColumnID int(11),
			StudentID int(8),
			SubjectID int(11),
			OtherInfoName varchar(255),
			AdjustedValue varchar(255),
			DateInput datetime,
			LastModified datetime,
			LastModifiedBy int(8),
			PRIMARY KEY (ManualAdjustmentID),
			INDEX ReportID (ReportID),
			INDEX ReportColumnID (ReportColumnID),
			INDEX StudentID (StudentID),
			INDEX SubjectID (SubjectID),
			UNIQUE ReportSubjectAdjustedValue (ReportID, ReportColumnID, StudentID, SubjectID, OtherInfoName)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		
		## Report Archive
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_REPORT_CARD_ARCHIVE (
			ReportCardArchiveID int(11) NOT NULL auto_increment,
			ReportID int(11) default NULL,
			Semester char(8) default NULL,
			YearID int(8) default NULL,
			YearClassID int(8) default NULL,
			StudentID int(8) default NULL,
			ReportCardHTML text default NULL,
			DateInput datetime default NULL,
			LastModifiedBy int(8) default NULL,
			PRIMARY KEY (ReportCardArchiveID),
			INDEX ReportID (ReportID),
			INDEX Semester (Semester),
			INDEX YearID (YearID),
			INDEX YearClassID (YearClassID),
			INDEX StudentID (StudentID),
			UNIQUE ArchiveReportCardKey (ReportID, StudentID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
		##################################
		# Added on 20091117, Table for HKUGA Primary School
		# Store Extended Learning Activity (ELA) Information
		##################################
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_EXTENDED_LEARNING_ACTIVITY (
		  ELAID int(11) NOT NULL auto_increment,
		  SubjectID int(11) default NULL,
		  IsModule tinyint(2) default 0,
		  Remark text default NULL,
		  LastModifiedBy int(11) default NULL,
		  DateInput datetime default NULL,
		  DateModified datetime default NULL,
		  PRIMARY KEY (ELAID),
		  UNIQUE KEY SubjectID (SubjectID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		##################################
		# Table for HKUGA Primary School - END
		##################################
		
		
		##################################
		# Added on 20091130, Table for Munsang College
		# Store Promotion Criteria Info
		##################################
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_PROMOTION_CRITERIA (
			PromotionCriteriaID int(11) NOT NULL auto_increment,
			ClassLevelID int(11) default NULL,
			PassGrandAverage tinyint(1) default 0,
			MaxFailNumOfSubject1 int(3) default NULL,
			SubjectList1 text default NULL,
			MaxFailNumOfSubject2 int(3) default NULL,
			SubjectList2 text default NULL,
			MaxFailNumOfSubject3 int(3) default NULL,
			SubjectList3 text default NULL,
			NonSubmissionAssignment tinyint(1) default 0,
			MaxAssignment int(3),
			DateInput datetime default NULL,
			DateModified datetime default NULL,
			LastModifiedBy int(11) default NULL,
			PRIMARY KEY (PromotionCriteriaID),
			KEY ClassLevelID (ClassLevelID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		##################################
		# Table for Munsang College - END
		##################################
		
		
		##################################
		# Added on 20091230, Table for Pooi Tun
		# Store Subject Ranking Display Settings
		##################################
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_POSITION_DISPLAY_SETTING (
			PositionDisplaySettingID int(11) NOT NULL auto_increment,
			ReportID int(11) default NULL,
			SubjectID int(11) default NULL,
			RangeType tinyint(3) default NULL,
			UpperLimit tinyint(3) default NULL,
			GreaterThanAverage tinyint(3) default NULL,
			RecordType varchar(255) default NULL,
			DateInput datetime default NULL,
			DateModified datetime default NULL,
			LastModifiedBy int(11) default NULL,
			PRIMARY KEY (PositionDisplaySettingID),
			UNIQUE KEY ReportSubjectPositionType(ReportID, SubjectID, RecordType)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		##################################
		# Table for Pooi Tun - END
		##################################
		
		
		##################################
		# Added on 20100513, Table for Holy Trinity
		# Store Student Academic Progress Info
		##################################
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_STUDENT_ACADEMIC_PROGRESS (
			AcademicProgressID int(11) NOT NULL auto_increment,
			ReportID int(11) default NULL,
			SubjectID int(11) default NULL,
			StudentID int(11) default NULL,
			FromReportID int(11) default NULL,
			ToReportID int(11) default NULL,
			FromScore int(11) default NULL,
			ToScore int(11) default NULL,
			ScoreDifference int(11) default NULL,
			OrderClassScoreDifference int(11) default NULL,
			OrderFormScoreDifference int(11) default NULL,
			FromRank int(11) default NULL,
			ToRank int(11) default NULL,
			RankDifference int(11) default NULL,
			OrderClassRankDifference int(11) default NULL,
			OrderFormRankDifference int(11) default NULL,
			DateModified datetime default NULL,
			LastModifiedBy int(11) default NULL,
			PRIMARY KEY (AcademicProgressID),
			UNIQUE KEY StudentSubjectProgress(ReportID, SubjectID, StudentID),
			KEY OrderClassScoreDifference (OrderClassScoreDifference),
			KEY OrderFormScoreDifference (OrderFormScoreDifference),
			KEY OrderClassRankDifference (OrderClassRankDifference),
			KEY OrderFormRankDifference (OrderFormRankDifference)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		##################################
		# Table for Holy Trinity - END
		##################################
		
		##################################
		# Added on 20100618, Table for Master Report
		##################################
		$sql_table[] = 
		"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_MASTER_REPORT_PRESET (
		     PresetID int(8) NOT NULL auto_increment,
		     PresetName varchar(100) default NULL,
		     PresetValue text,
		     DateInput datetime default NULL,
		     DateModified datetime default NULL,
		     PRIMARY KEY (PresetID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		##################################
		# Table for Master Report - END
		##################################
		
		# create new tables
		for($i=0; $i<sizeof($sql_table); $i++){
			$sql = $sql_table[$i];
			if($li->db_db_query($sql)){
				echo "<p>Created (or skip creating) a new table:<br />".$sql."</p>\n";
			} else {
				echo "<p>Failed to create table:<br />".$sql."<br />Error: ".mysql_error()."</p>\n";
			}
		}
		
		
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_COMMENT_BANK DROP COLUMN IsSubjectComment;";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_COMMENT_BANK ADD COLUMN SubjectID INT(11) AFTER CommentCategory;";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_COMMENT_BANK ADD INDEX SubjectID (SubjectID);";
		
		// 2008-07-02 Andy
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT_SCORE ADD RawMark FLOAT AFTER Grade;";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT_SCORE_ARCHIVE ADD RawMark FLOAT AFTER Grade;";
		
		// 2008-07-04 Yat Woon
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE ADD COLUMN ShowGrandTotal tinyint AFTER OverallPositionRangeForm;";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE ADD COLUMN ShowGrandAvg tinyint AFTER ShowGrandTotal;";
		
		// 2008-07-15 Andy
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT_SCORE_ARCHIVE DROP COLUMN MarkWeighted;";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT_SCORE_ARCHIVE ADD COLUMN Mark FLOAT DEFAULT NULL AFTER ReportColumnID;";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT_SCORE_ARCHIVE ADD COLUMN RawMark FLOAT DEFAULT NULL AFTER Grade;";
		
		// 2008-07-16 Andy
		/* disabled by Yuen on 2008-11-25
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_SUB_MARKSHEET_SCORE CHANGE ColumnID ReportColumnID INT(11) DEFAULT NULL;";
		*/
			
		// 2008-07-25 Andy
		$sql_alter[] = "ALTER TABLE $intranet_db.INTRANET_USER ADD HKID varchar(10) default NULL;";
		
		// 2008-07-29 Andy
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT ADD COLUMN ClassNoOfStudent INT(4) DEFAULT NULL AFTER OrderMeritClass";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT ADD COLUMN FormNoOfStudent INT(4) DEFAULT NULL AFTER OrderMeritForm";
		
		// 2008-08-11 Andy
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT_SCORE ADD COLUMN ClassNoOfStudent INT(4) DEFAULT NULL AFTER OrderMeritClass";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT_SCORE ADD COLUMN FormNoOfStudent INT(4) DEFAULT NULL AFTER OrderMeritForm";
		
		// 2008-11-25 Yuen
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_SUB_MARKSHEET_SCORE CHANGE ReportColumnID ColumnID INT(11) DEFAULT NULL;";
		
		// 2008-12-01 Ivan
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE ADD ShowNumOfStudentClass tinyint(1) DEFAULT NULL;";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE ADD ShowNumOfStudentForm tinyint(1) DEFAULT NULL;";
		
		// 2009-04-27 Ivan
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_SUB_MARKSHEET_COLUMN ADD Calculation char(255) DEFAULT NULL AFTER Ratio;";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_OTHER_STUDENT_INFO MODIFY Conduct varchar(32);";
		
		// 2009-05-20 Ivan
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE ADD LastDateToiPortfolio datetime DEFAULT NULL;";
		
		// 2009-07-23 Ivan
		$sql_alter[] = "ALTER Table $reportcard_db.RC_MARKSHEET_SUBMISSION_PROGRESS Add SubjectGroupID int(11) default NULL After ReportID;";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_MARKSHEET_SUBMISSION_PROGRESS ADD INDEX SubjectGroupID (SubjectGroupID);";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_MARKSHEET_SUBMISSION_PROGRESS ADD INDEX ReportID (ReportID);";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_MARKSHEET_SUBMISSION_PROGRESS ADD INDEX ClassID (ClassID);";
		
		// 2009-07-24 Ivan
		$sql_alter[] = "ALTER Table $reportcard_db.RC_COMMENT_BANK Modify Comment text Default NULL;";
		$sql_alter[] = "ALTER Table $reportcard_db.RC_COMMENT_BANK Modify CommentEng text Default NULL;";
		
		// 2009-08-11 Ivan (For Marksheet Verification)
		$sql_alter[] = "ALTER Table $reportcard_db.RC_MARKSHEET_FEEDBACK Add LastModifiedBy int(11) default NULL;";
		$sql_alter[] = "ALTER Table $reportcard_db.RC_MARKSHEET_FEEDBACK Add RecordStatus tinyint(1) default NULL;";
		
		// 2009-08-31 Marcus 
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_GRADING_SCHEME_RANGE ADD AdditionalCriteria float DEFAULT 0";
		
		// 2009-09-02 Marcus 
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_MARKSHEET_FEEDBACK MODIFY RecordStatus VARCHAR(25) DEFAULT NULL";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_MARKSHEET_FEEDBACK MODIFY Comment TEXT DEFAULT NULL";
		
		// 2009-09-14 Ivan 
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE MODIFY Semester char(8) DEFAULT NULL";
		
		// 2009-09-15 Ivan (For Archive Report Card)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE Add LastArchived datetime DEFAULT NULL";
		
		// 2009-10-30 Ivan (SubMS - transfer overall mark to marksheet)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_SUB_MARKSHEET_COLUMN ADD TransferToMS tinyint(2) DEFAULT 0 AFTER Calculation;";
		
		// 2009-11-17 Ivan (for HKUGA Primary School Grade - Very Long Grade Name)
		$sql_alter[] = "ALTER Table $reportcard_db.RC_GRADING_SCHEME_RANGE Modify Grade varchar(255) Default NULL;";
		$sql_alter[] = "ALTER Table $reportcard_db.RC_REPORT_RESULT_SCORE Modify Grade varchar(255) Default NULL;";

		// 2009-11-17 Ivan (for HKUGA Primary School Grade - Subjects in different Term has different Course Description)
		$sql_alter[] = "Alter Table $reportcard_db.RC_CURRICULUM_EXPECTATION Add COLUMN TermID INT(11) Default 0 AFTER SubjectID;";
		// 2009-11-17 Ivan (for HKUGA Primary School - Display Subjects in different languages)
		$sql_alter[] = "Alter Table $reportcard_db.RC_SUBJECT_FORM_GRADING Add COLUMN LangDisplay varchar(128) Default Null After ScaleDisplay;";
		
		// 2009-11-27 Ivan (for Kiangsu-Chekiang College - Stream Position)
		$sql_alter[] = "Alter Table $reportcard_db.RC_REPORT_RESULT Add COLUMN OrderMeritStream int(4) Default Null After FormNoOfStudent;";
		$sql_alter[] = "Alter Table $reportcard_db.RC_REPORT_RESULT Add COLUMN StreamNoOfStudent int(4) Default Null After OrderMeritStream;";
		$sql_alter[] = "Alter Table $reportcard_db.RC_REPORT_RESULT_SCORE Add COLUMN OrderMeritStream int(4) Default Null After FormNoOfStudent;";
		$sql_alter[] = "Alter Table $reportcard_db.RC_REPORT_RESULT_SCORE Add COLUMN StreamNoOfStudent int(4) Default Null After OrderMeritStream;";
		
		// 2009-11-27 Ivan (for UCCKE - Subject Group Position)
		$sql_alter[] = "Alter Table $reportcard_db.RC_REPORT_RESULT_SCORE Add COLUMN OrderMeritSubjectGroup int(4) Default Null After StreamNoOfStudent;";
		$sql_alter[] = "Alter Table $reportcard_db.RC_REPORT_RESULT_SCORE Add COLUMN SubjectGroupNoOfStudent int(4) Default Null After OrderMeritSubjectGroup;";
		
		// 2009-12-18 Marcus (for Display Subject Group Option)
		$sql_alter[] = "alter table $reportcard_db.RC_SUBJECT_FORM_GRADING add column DisplaySubjectGroup varchar(128) AFTER LangDisplay";
		
		// 2009-12-21 Ivan (for Multiple Term Report)
		$sql_alter[] = "alter table $reportcard_db.RC_REPORT_TEMPLATE add column isMainReport tinyint(2) Default 1";
		
		// 2009-12-22 Ivan (for Position Display - Display position only if the mark is greater than the Average for some marks)
		$sql_alter[] = "alter table $reportcard_db.RC_REPORT_TEMPLATE add column GreaterThanAverageClass tinyint(3) Default 0";
		$sql_alter[] = "alter table $reportcard_db.RC_REPORT_TEMPLATE add column GreaterThanAverageForm tinyint(3) Default 0";
		
		// 2010-01-08 Marcus (for Report Template Settings - to block teacher from modifying mark sheet again)
		$sql_alter[] = "alter table $reportcard_db.RC_REPORT_TEMPLATE_COLUMN add column BlockMarksheet varchar(11) Default NULL AFTER PositionRange ";
		
		// 2010-01-14 Ivan (for Report Template Settings Step 4 - Display Subject Component or not)
		$sql_alter[] = "alter table $reportcard_db.RC_REPORT_TEMPLATE add column ShowSubjectComponent tinyint(1) Default 1";
		
		// 2010-02-11 Marcus (for Carmel Alison to show Score in GrandMS and Rank Ordering)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT ADD COLUMN GrandSDScore FLOAT DEFAULT NULL AFTER AdjustedBy";
		
		// 2010-02-11 Marcus (for Carmel Alison to show Score in GrandMS and Rank Ordering)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT_SCORE ADD COLUMN SDScore FLOAT DEFAULT NULL AFTER SubjectGroupNoOfStudent";
		
		// 2010-02-19 Ivan (Add index to Grading Scheme Tables)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_SUBJECT_FORM_GRADING Add INDEX ClassLevelID (ClassLevelID)";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_SUBJECT_FORM_GRADING Add INDEX SchemeID (SchemeID)";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_GRADING_SCHEME_RANGE Add INDEX Nature (Nature)";
		
		// 2010-03-01 Ivan (Add Display Order in Personal Characteristics)
		$sql_alter[] = "Alter Table $reportcard_db.RC_PERSONAL_CHARACTERISTICS Add DisplayOrder int(3) Default Null After Title_CH";
		$sql_alter[] = "Alter Table $reportcard_db.RC_PERSONAL_CHARACTERISTICS Add INDEX DisplayOrder (DisplayOrder)";

		// 2010-03-02 Marcus (Add LastPromotionUpdate to store promotion generation date)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE ADD COLUMN LastPromotionUpdate DATETIME DEFAULT NULL";
		
		// 2010-03-31 Ivan (Add customized Term Start & End Date for Munsang College F5 & F7 - retrieve data from different modules)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE ADD COLUMN TermStartDate DATE DEFAULT NULL";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE ADD COLUMN TermEndDate DATE DEFAULT NULL";
		
		// 2010-04-07 Marcus (Add FullMark to RC_SUB_MARKSHEET_COLUMN for munsang )
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_SUB_MARKSHEET_COLUMN ADD COLUMN FullMark float Default NULL AFTER TransferToMS";
		
		// 2010-05-12 Ivan (Add LastGeneratedAcademicProgress to store academic progress generation date)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE ADD COLUMN LastGeneratedAcademicProgress DATETIME DEFAULT NULL";
		
		// 2010-05-13 Ivan (Add index)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT_SCORE Add INDEX ReportColumnID (ReportColumnID)";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT_SCORE Add INDEX Mark (Mark)";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT Add INDEX GrandTotal (GrandTotal)";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT Add INDEX GrandAverage (GrandAverage)";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT Add INDEX GPA (GPA)";
		
		// 2010-05-20 Ivan (Add RawSDScore)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT_SCORE ADD COLUMN RawSDScore float Default NULL AFTER SDScore";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT ADD COLUMN RawGrandSDScore float Default NULL AFTER GrandSDScore";

		// 2010-05-24 Marcus (Set Default Value of Promotion to 1)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT ALTER COLUMN Promotion SET DEFAULT 1";
		
		// 2010-05-25 Ivan (Change the Academic Progress Records as Float)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_STUDENT_ACADEMIC_PROGRESS CHANGE FromScore FromScore float DEFAULT NULL";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_STUDENT_ACADEMIC_PROGRESS CHANGE ToScore ToScore float DEFAULT NULL";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_STUDENT_ACADEMIC_PROGRESS CHANGE ScoreDifference ScoreDifference float DEFAULT NULL";
		
		// 2010-06-04 Ivan (Add Unique Key to prevent duplicated Subject-GradingScheme Relationship records)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_SUBJECT_FORM_GRADING Add Unique Index SubjectFormScheme (SubjectID, ClassLevelID, SchemeID)";
		
		// 2010-06-08 Ivan (Add AcademicProgressPrize field to record if the student get this prize)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_STUDENT_ACADEMIC_PROGRESS Add AcademicProgressPrize tinyint(1) default 0 After OrderFormRankDifference";
		
		// 2010-06-28 Ivan (Add Unique Key to prevent duplicated Subject Mark records)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_RESULT_SCORE Add Unique Index Report_Column_Subject_Student (ReportID , ReportColumnID , SubjectID, StudentID)";
		
		// 2010-07-06 Marcus (Add ShowPosition to control Subject Position Display)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_POSITION_DISPLAY_SETTING ADD COLUMN ShowPosition tinyint(1) DEFAULT 1 AFTER RecordType";
		
		// 2010-08-09 Marcus (change Footer data type from varchar(255) to text to store longer string)
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_REPORT_TEMPLATE MODIFY Footer TEXT";
		
		# alter tables
		for($i=0; $i<sizeof($sql_alter); $i++){
			$sql = $sql_alter[$i];
			if($li->db_db_query($sql)){
				echo "<p>Altered table: ".$sql."</p>\n";
			} else {
				echo "<p>Failed to alter table:<br />".$sql."<br />Error: ".mysql_error()."</p>\n";
			}
		}
		
		$sql_update_personal_char_display_order = "Update $reportcard_db.RC_PERSONAL_CHARACTERISTICS set DisplayOrder = CharID Where DisplayOrder is Null";
		$li->db_db_query($sql_update_personal_char_display_order);
		
		print("<p>=============================== Finish Creating tables for ".$DatabaseAry[$a][0]."... =============================== </p><br /><br />\n");
	}
	?>
	</body>
	</html>
	<? 
		if ($_REQUEST['direct_run'] == 1) 
			intranet_closedb(); 
	?>
<? } else { ?>
	<html>
	<body>
		The client has not purchased eRC.
	</body>
	</html>
<? } ?>