<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	/* Temp Library*/
	include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
	$lreportcard = new libreportcard2008j();
	//$lreportcard = new libreportcard();
	
	$CurrentPage = "Management_MarkSheetRevision";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    if ($lreportcard->hasAccessRight())
    {
        $linterface = new interface_html();
        
		############################################################################################################
		
		// Period (Term x, Whole Year, etc)
		$PeriodArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID, $ReportID);
		$PeriodName = (count($PeriodArr) > 0) ? $PeriodArr['SemesterTitle'] : '';
		
		# Load Settings - Calculation
		// Get the Calculation Method of Report Type - 1: Right-Down,  2: Down-Right
		$SettingCategory = 'Calculation';
		$categorySettings = $lreportcard->LOAD_SETTING($SettingCategory);
		$TermCalculationType = $categorySettings['OrderTerm'];
		$FullYearCalculationType = $categorySettings['OrderFullYear'];
		
		# Load Settings - Storage&Display
		$SettingStorDisp = 'Storage&Display';
		$storDispSettings = $lreportcard->LOAD_SETTING($SettingStorDisp);
		
		# Load Settings - Absent&Exempt
		$SettingAbsentExempt = 'Absent&Exempt';
		$absentExemptSettings = $lreportcard->LOAD_SETTING($SettingAbsentExempt);
		
		# Check for Weight or Percentage will be used 
		if(count($PeriodArr) > 0 && $PeriodArr['Semester'] == "F")
			$usePercentage = ($FullYearCalculationType == 1) ? 1 : 0;
		else 
			$usePercentage = ($TermCalculationType == 1) ? 1 : 0;
		
		# Get Class Info 
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		for($i=0 ; $i<count($ClassArr) ;$i++){
			if($ClassArr[$i][0] == $ClassID)
				$ClassName = $ClassArr[$i][1];
		}
		
		# Get Subject Info
		$SubjectName = $lreportcard->GET_SUBJECT_NAME($SubjectID);
		
		# Get Student Info
		$StudentArr = $lreportcard->GET_STUDENT_BY_CLASS($ClassID, '', 1);
		$StudentSize = sizeof($StudentArr);
		
		$StudentIDArr = array();
		if(count($StudentArr) > 0){
			for($i=0 ; $i<sizeof($StudentArr) ; $i++)
				$StudentIDArr[$i] = $StudentArr[$i]['UserID'];
		}
		
		# Get Component Subject(s) Info
		$isEdit = 1;					// default "1" as the marksheet is editable
		$CmpSubjectArr = array();
		$CmpSubjectIDArr = array();
		$isCalculateByCmpSub = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
		$isCmpSubject = $lreportcard->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($SubjectID);
		if(!$isCmpSubject){
			$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
			
			$CmpSubjectIDArr = array();
			if(!empty($CmpSubjectArr)){	
				for($i=0 ; $i<count($CmpSubjectArr) ; $i++){
					$CmpSubjectIDArr[] = $CmpSubjectArr[$i]['SubjectID'];
					
					$CmpSubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $CmpSubjectArr[$i]['SubjectID'],0,0,$ReportID);
					if(count($CmpSubjectFormGradingArr) > 0 && $CmpSubjectFormGradingArr['ScaleInput'] == "M")
						$isCalculateByCmpSub = 1;
				}
			}
			// Check whether the marksheet is editable or not 
			$isEdit = $lreportcard->CHECK_MARKSHEET_STATUS($SubjectID, $ClassLevelID, $ReportID, $CmpSubjectIDArr);
		}
		
		# Get Data Either Assessment Column(s) in Term Or Term Column(s) in Wholely Report	
		// Loading Report Template Data
		$basic_data = $lreportcard->returnReportTemplateBasicInfo($ReportID);			// Basic  Data
		$column_data = $lreportcard->returnReportTemplateColumnData($ReportID); 		// Column Data
		$ColumnSize = sizeof($column_data);
		$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
		$ReportTitle = $basic_data['ReportTitle'];
		$ReportType = $basic_data['Semester'];
		$isPercentage = $basic_data['PercentageOnColumnWeight'];
		
		# Get Existing Report Calculation Method
		$CalculationType = ($ReportType == "F") ? $FullYearCalculationType : $TermCalculationType;
		
		# Get Report Column Weight
		$WeightAry = array();
		if($CalculationType == 2){
			if(count($column_data) > 0){
		    	for($i=0 ; $i<sizeof($column_data) ; $i++){
					$OtherCondition = "SubjectID IS NULL AND ReportColumnID = ".$column_data[$i]['ReportColumnID'];
					$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
					$WeightAry[$weight_data[0]['ReportColumnID']][$SubjectID] = $weight_data[0]['Weight'];
				}
			}
		}
		else {
			$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID); 			
			for($i=0 ; $i<sizeof($weight_data) ; $i++)
				$WeightAry[$weight_data[$i]['ReportColumnID']][$weight_data[$i]['SubjectID']] = $weight_data[$i]['Weight'];
		}
		
		# Get MarkSheet Score Info / ParentSubMarksheetScoreInfoArr	
		$ColumnHasTemplateAry = array();
		$hasDirectInputMark = 0;
		
		if($ColumnSize > 0){
		    for($i=0 ; $i<$ColumnSize ; $i++){
			    /* 
				* Check for the Mark Column is allowed to fill in mark when any term does not have related report template 
				* by SemesterNum (ReportType) && ClassLevelID
				* An array is initialized as a control variable [$ColumnHasTemplateAry]
				* A variable is initialized as a control variable [$hasDirectInputMark], must be used with [$ReportType == "F"]
				* to control the Import function to be used or not
				*/
				if($ReportType == "F"){
					$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] = $lreportcard->CHECK_REPORT_TEMPLATE_FROM_COLUMN($column_data[$i]['SemesterNum'], $ClassLevelID);
					if(!$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']])
						$hasDirectInputMark = 1;
				}
				else {
					$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] = 1;
				}
			}
		}
		
		####################################################################################################
		# General Approach dealing with Search Term Result From Whole Year Report not From Term Report
		# Can deal with Customization of SIS dealing with secondary template 
		$isAllWholeReport = 0;
		if($ReportType == "F"){
			$ReportTypeArr = array();
			$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);
			if(count($ReportTypeArr) > 0){
				$flag = 0;
				for($j=0 ; $j<count($ReportTypeArr) ; $j++){
					if($ReportTypeArr[$j]['ReportID'] != $ReportID){
						if($ReportTypeArr[$j]['Semester'] != "F")	$flag = 1;
					}
				}
				if($flag == 0) $isAllWholeReport = 1;
			}
		}
		
		# Get the Corresponding Whole Year ReportID from 
		# the ReportColumnID of the Current Whole Year Report 
		$WholeYearColumnHasTemplateArr = array();
		if($isAllWholeReport == 1){
			if($ReportType == "F" && $ColumnSize > 0){
				for($j=0 ; $j<$ColumnSize ; $j++){
					$ReportColumnID = $column_data[$j]['ReportColumnID'];
					# Return Whole Year Report Template ID - ReportID 
					$WholeYearColumnHasTemplateArr[$ReportColumnID] = $lreportcard->CHECK_WHOLE_YEAR_REPORT_TEMPLATE_FROM_COLUMN($ReportID, $column_data[$j]['SemesterNum'], $ClassLevelID);
					
					# Check Whether the Return WholeYear ReportID is not greater the Current ReportID
					# if User creates All Reports sequently
					if($WholeYearColumnHasTemplateArr[$ReportColumnID] != false){
						if($WholeYearColumnHasTemplateArr[$ReportColumnID] > $ReportID)
							$WholeYearColumnHasTemplateArr[$ReportColumnID] = false;
					}
				}
			}
		}
		####################################################################################################
		
		# Get Special Case 
		$possibleSpeicalCaseSet1 = $lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
		$possibleSpeicalCaseSet2 = $lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2();
		
		# Main - Preparation
		$SchemeID = array();
		$ScaleInput = array();		// M: Mark, G: Grade
		$SchemeType = array();		// H: Honor Based, PF: PassFail
		$InputGradeOption = array();
		$InputPFOption = array();
		$NewReportColumn = array();
		$MarksheetScoreInfoArr = array();
		$SchemeFullMark = array();
		$ScaleInput = array();
		$SchemeType = array();
		$MarksheetOverallScoreInfoArr = array();
		$ProgressArr = array();
		$isSetSubjectOverall = array();
		
		if(count($CmpSubjectArr) > 0 && $ColumnSize > 0){
			for($i=0 ; $i<$ColumnSize ; $i++){
				$report_column_id = $column_data[$i]['ReportColumnID'];
				
			    for($j=0 ; $j<count($CmpSubjectArr) ; $j++){
				    $cmp_subject_id = $CmpSubjectArr[$j]['SubjectID'];
				    
				    if($isAllWholeReport == 1){
					    if($WholeYearColumnHasTemplateArr[$report_column_id] != false){
						    
					    } else {
							#######################################################################################################################################
							# for the Whole Year Report which defined any Term Column in other Whole Year Report	added on 22 May by Jason
							# the case for Whole Year Report Column Which cannot be found in Report Column of other Whole Year report
							# code enter here
							#######################################################################################################################################
							# Get Input Mark From Marksheet 
					    	$MarksheetScoreInfoArr[$report_column_id][$cmp_subject_id] = $lreportcard->GET_MARKSHEET_SCORE($StudentIDArr, $cmp_subject_id, $report_column_id);
							
							# Set Valid Report Columns into a new Column Array
					    	$column_data[$i]['CmpSubjectID'] = $cmp_subject_id;
					    	$NewReportColumn[] = $column_data[$i];
				    	}
					} else {
				    	if($ReportType != "F" || ($ReportType == "F" && !$ColumnHasTemplateAry[$report_column_id]) ){
					    	# Get Input Mark From Marksheet 
					    	$MarksheetScoreInfoArr[$report_column_id][$cmp_subject_id] = $lreportcard->GET_MARKSHEET_SCORE($StudentIDArr, $cmp_subject_id, $report_column_id);
					    	
					    	# Set Valid Report Columns into a new Column Array
					    	$column_data[$i]['CmpSubjectID'] = $cmp_subject_id;
					    	$NewReportColumn[] = $column_data[$i];
				    	}
			    	}
	    		}
			}
			
			for($j=0 ; $j<count($CmpSubjectArr) ; $j++){
				$cmp_subject_id = $CmpSubjectArr[$j]['SubjectID'];
				
				$isSetSubjectOverall[$CmpSubjectArr[$i]['SubjectID']] = 0;
				
				//Initialization of Grading Scheme
				$SchemeID[$cmp_subject_id] = '';
				$ScaleInput[$cmp_subject_id] = "M";		// M: Mark, G: Grade
				$SchemeType[$cmp_subject_id] = "H";		// H: Honor Based, PF: PassFail
				$SchemeInfo = array();
				$SubjectFormGradingArr = array();
				
				$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $cmp_subject_id,0,0,$ReportID);
				
				# Get Grading Scheme Info of that subject
				if(count($SubjectFormGradingArr) > 0){
					$SchemeID[$cmp_subject_id] = $SubjectFormGradingArr['SchemeID'];
					$ScaleInput[$cmp_subject_id] = $SubjectFormGradingArr['ScaleInput'];
					
					$SchemeMainInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID[$cmp_subject_id]);
					$SchemeType[$cmp_subject_id] = $SchemeMainInfo['SchemeType'];
					$SchemeFullMark[$cmp_subject_id] = $SchemeMainInfo['FullMark'];
				}
				
				
				if($SchemeType[$cmp_subject_id] == "H" && $ScaleInput[$cmp_subject_id] == "G"){	// Prepare Selection Box using Grade as Input
					$InputTmpOption = array();
					$InputGradeOption[$cmp_subject_id] = array();
					
					$SchemeGrade = $lreportcard->GET_GRADE_FROM_GRADING_SCHEME_RANGE($SchemeID[$cmp_subject_id]);
					$InputGradeOption[$cmp_subject_id][0] = array('', '--');
					if(!empty($SchemeGrade)){
						foreach($SchemeGrade as $GradeRangeID => $grade)
							$InputGradeOption[$cmp_subject_id][] = array($GradeRangeID, $grade);
					}
					
					for($k=0 ; $k<count($possibleSpeicalCaseSet1) ; $k++){
						$InputGradeOption[$cmp_subject_id][] = array($possibleSpeicalCaseSet1[$k], $possibleSpeicalCaseSet1[$k]);
					}
				}
				else if($SchemeType[$cmp_subject_id] == "PF"){			// Prepare Selection Box using Pass/Fail as Input
					$SchemePassFail = array();
					$SchemePassFail['P'] = $SchemeMainInfo['Pass'];
					$SchemePassFail['F'] = $SchemeMainInfo['Fail'];
				
					$InputPFOption[$cmp_subject_id] = array();
					$InputPFOption[$cmp_subject_id][0] = array('', '--');
					$InputPFOption[$cmp_subject_id][1] = array('P', $SchemeMainInfo['Pass']);
					$InputPFOption[$cmp_subject_id][2] = array('F', $SchemeMainInfo['Fail']);
					
					
					for($k=0 ; $k<count($possibleSpeicalCaseSet2) ; $k++){
						$InputPFOption[$cmp_subject_id][] = array($possibleSpeicalCaseSet2[$k], $possibleSpeicalCaseSet2[$k]);
					}
				}
				
				# Get Overall Subject Mark Or Overall Term Subject Mark 
				# for InputScale is Grade or Scheme Type is PassFail
				if($SchemeType[$cmp_subject_id] == "PF" || ($SchemeType[$cmp_subject_id] == "H" && $ScaleInput[$cmp_subject_id] == "G") ){
					$MarksheetOverallScoreInfoArr[$cmp_subject_id] = $lreportcard->GET_MARKSHEET_OVERALL_SCORE($StudentIDArr, $cmp_subject_id, $ReportID);
				}
				
				# Get Complete Status of Common Subject or Parent Subject
			    $ProgressArr[$cmp_subject_id] = $lreportcard->GET_MARKSHEET_SUBMISSION_PROGRESS($cmp_subject_id, $ReportID, $ClassID);
			}
		}
		
		
		
		# Main Content of Marksheet Reivision
		$input_mark_count = 0;
		$overall_cnt = 0;
		$display_overall = '';
		$display = '';
		$display .= '<table width="100%" cellpadding="4" cellspacing="0">';
		
		# Display - Header
		$display .= '<tr>';
		$display .= '<td colspan="2" class="tablegreentop tabletopnolink">&nbsp;</td>';
		if(count($NewReportColumn) > 0){
			# Report Column Header
		    for($i=0 ; $i<sizeof($NewReportColumn) ; $i++){
			    list($ReportColumnID, $ColumnTitle, $DefaultWeight, $SemesterNum) = $NewReportColumn[$i];
			    $CmpSubjectID = $NewReportColumn[$i]['CmpSubjectID'];
			    $ColumnTitle = ($ColumnTitle != "" && $ColumnTitle != null) ? $ColumnTitle : $ColumnTitleAry[$ReportColumnID];
			    
			    $col_colpsan = 1;
			    for($j=$i+1 ; $j<sizeof($NewReportColumn) ; $j++){
				    if($NewReportColumn[$j]['ReportColumnID'] == $ReportColumnID)
				    	$col_colpsan++;
				    else 
				    	break;
			    }
			    
			    $display .= '<td align="center" colspan="'.$col_colpsan.'" class="tablegreentop tabletopnolink">'.$ColumnTitle.'</td>';
			    
			    $i = $j - 1;
		    }
		    
		    # Overall Subject Mark Header
		    for($i=0 ; $i<count($CmpSubjectArr) ; $i++){
				if($SchemeType[$CmpSubjectArr[$i]['SubjectID']] == "PF" || ($SchemeType[$CmpSubjectArr[$i]['SubjectID']] == "H" && $ScaleInput[$CmpSubjectArr[$i]['SubjectID']] == "G") )
				    $overall_cnt++;
			}
		    
		    if($overall_cnt > 0){
		    	$display .= '<td align="center" colspan="'.$overall_cnt.'" class="tablegreentop tabletopnolink">';
		    	$display .= ($ReportType == "F") ? $eReportCard['OverallSubjectMark'] : $eReportCard['TermSubjectMark'];
		    	$display .= '</td>';
	    	}
    	}
		$display .= '</tr>';
		$display .= '<tr>
	          <td width="30" align="center" valign="top" class="tablegreentop tabletopnolink">'.$eReportCard['StudentNo_short'].'</td>
	          <td valign="top" class="tablegreentop tabletopnolink">'.$eReportCard['Student'].'</td>';
	    if(count($NewReportColumn) > 0){
		    
		    # Get Basic Column Title Info
		    for($i=0 ; $i<sizeof($NewReportColumn) ; $i++){
			    list($ReportColumnID, $ColumnTitle, $DefaultWeight, $SemesterNum) = $NewReportColumn[$i];
			    $CmpSubjectID = $NewReportColumn[$i]['CmpSubjectID'];
			    $ColumnTitle = ($ColumnTitle != "" && $ColumnTitle != null) ? $ColumnTitle : $ColumnTitleAry[$ReportColumnID];
			    
			    # Get Component Subject Name
			    $CmpSubjectNameEN = $lreportcard->GET_SUBJECT_NAME_LANG($NewReportColumn[$i]['CmpSubjectID'], "EN");
			    $CmpSubjectNameCH = $lreportcard->GET_SUBJECT_NAME_LANG($NewReportColumn[$i]['CmpSubjectID'], "CH");
			    $CmpSubjectName = $CmpSubjectNameEN.' - '.$CmpSubjectNameCH;
			    
			    # UI
	            $display .= '<td width="12%" align="center" class="tablegreentop tabletopnolink">'.$CmpSubjectName.'<br />';
	            $display .= '<input type="hidden" id="ColumnID_'.$i.'" name="ColumnID_'.$i.'" value="'.$ReportColumnID.'"/>';	
	            $display .= '<input type="hidden" id="SubjectID_Column_'.$i.'" name="SubjectID_Column_'.$i.'" value="'.$CmpSubjectID.'"/>';	
	          	$display .= '</td>';
	          	
	          	# Get Overall Subject Mark Title Info if any
	          	if($isSetSubjectOverall[$CmpSubjectID] == 0 && 
	          		($SchemeType[$CmpSubjectID] == "PF" || ($SchemeType[$CmpSubjectID] == "H" && $ScaleInput[$CmpSubjectID] == "G")) ){
		          	$display_overall .= '<td width="12%" align="center" class="tablegreentop tabletopnolink">'.$CmpSubjectName.'</td>';
		          	$isSetSubjectOverall[$CmpSubjectID] = 1;;
	          	} 
	          	
	          	# Count the number of Report Column which use Input Mark as Input Method
	          	if($SchemeType[$CmpSubjectID] == "H" && $ScaleInput[$CmpSubjectID] == "M"){
		          	$input_mark_count++;
	          	}
      		}
      		
      		$display .= $display_overall;
    	}
	    $display .= '</tr>';      
	          
	   	# Display - Student Marksheet
	    for($j=0; $j<$StudentSize ; $j++){
			list($StudentID, $WebSAMSRegNo, $StudentNo, $StudentName) = $StudentArr[$j];
			$tr_css = ($j % 2 == 1) ? "tablegreenrow2" : "tablegreenrow1";
			
			$display .= '<tr class="'.$tr_css.'">
		          <td align="center" nowrap="nowrap" class="tabletext">'.$StudentNo.'</td>
	          	  <td nowrap="nowrap" class="tabletext">'.$StudentName.'<input type="hidden" name="StudentID[]" id="StudentID_'.$j.'" value="'.$StudentID.'"/></td>';
	        if(count($NewReportColumn) > 0){
		        $inputmark_num = 0;
		        
		        for($i=0 ; $i<sizeof($NewReportColumn) ; $i++){
			        list($ReportColumnID, $ColumnTitle, $DefaultWeight, $SemesterNum) = $NewReportColumn[$i];
			        $CmpSubjectID = $NewReportColumn[$i]['CmpSubjectID'];
			        
			        # Format of Mark Name: mark[index of row][index of column]
				    $mark_name = "mark_".$CmpSubjectID."_".$ReportColumnID."_".$StudentID;
				    
			        if( $WeightAry[$ReportColumnID][$CmpSubjectID] != 0 && $WeightAry[$ReportColumnID][$CmpSubjectID] != null && $WeightAry[$ReportColumnID][$CmpSubjectID] != "" ){
				        
					    if(count($MarksheetScoreInfoArr) > 0){
						    // Mark Type : M-Mark, G-Grade, PF-PassFail, SC-SpecialCase
							$MarkType = $MarksheetScoreInfoArr[$ReportColumnID][$CmpSubjectID][$StudentID]['MarkType'];
						    
						    if($MarkType == "G" || $MarkType == "PF" || $MarkType == "SC"){
						    	$InputVal = $MarksheetScoreInfoArr[$ReportColumnID][$CmpSubjectID][$StudentID]['MarkNonNum'];
					    	} else if($MarkType == "M"){
								# For ScaleInput is Mark & SchemeType is Honor-Based
								# if MarkRaw is -1, the MarkRaw has not been assigned yet.
								# if MarkRaw >= 0, the MarkRaw is valid
						    	$InputVal = ($MarksheetScoreInfoArr[$ReportColumnID][$CmpSubjectID][$StudentID]['MarkRaw'] != -1) ? $MarksheetScoreInfoArr[$ReportColumnID][$CmpSubjectID][$StudentID]['MarkRaw'] : '';
					    	} else {
						    	$InputVal = '';
					    	}
					    } else {
						    $InputVal = '';
					    }	
				        
				        $otherPar = array('class'=>'textboxnum', 'onBlur'=>'if(!setStart) { this.value=startContent; setStart = 1}', 'onpaste'=>'pasteTable(\''.$j.'\', \''.$i.'\')', 'onchange'=>'jCHANGE_STATUS()');
				        
				        if($SchemeType[$CmpSubjectID] == "H" && $ScaleInput[$CmpSubjectID] == "G") {
				    		$InputMethod = $linterface->GET_SELECTION_BOX($InputGradeOption[$CmpSubjectID], 'name="'.$mark_name.'" onchange="jCHANGE_STATUS()" onfocusout="jCHANGE_STATUS()" class="tabletexttoptext"', '', $InputVal);
			    		} else if($SchemeType[$CmpSubjectID] == "PF") {
				    		$InputMethod = $linterface->GET_SELECTION_BOX($InputPFOption[$CmpSubjectID], 'name="'.$mark_name.'" onchange="jCHANGE_STATUS()" onfocusout="jCHANGE_STATUS()" class="tabletexttoptext"', '', $InputVal);
			    		} else {
				    		# Format of Mark ID  : mark_[Component Subject ID]_[ReportColumnID]_[StudentID]
				    		# [$i] of mark[$j][$i] == [$i] of ColumnID_$i for matching
				    		$mark_id = "mark[$j][$inputmark_num]";
				    		$InputMethod = $lreportcard->GET_INPUT_TEXT($mark_name, $mark_id, $InputVal, '', $otherPar);
				    		$inputmark_num++;
			    		}
			        } else {
					    $InputMethod = "--";
					}
					
					$display .= '<td align="center" class="tabletext">'.$InputMethod.'</td>';
		        }
		        
		        
				# Get Overall Subject Mark Or Overall Term Subject Mark 
				# for InputScale is Grade or Scheme Type is PassFail
				if($overall_cnt > 0){
					for($i=0 ; $i<count($CmpSubjectArr) ; $i++){
						$cmp_subject_id = $CmpSubjectArr[$i]['SubjectID'];
						if($SchemeType[$cmp_subject_id] == "PF"){
							$overall_value = (count($MarksheetOverallScoreInfoArr) > 0 && isset($MarksheetOverallScoreInfoArr[$cmp_subject_id]) ) ? $MarksheetOverallScoreInfoArr[$cmp_subject_id][$StudentID]['MarkNonNum'] : "";
							$mark_name = "overall_mark_".$cmp_subject_id."_".$StudentID;
							$display .= '<td align="center" class="tabletext">';
							$display .= $linterface->GET_SELECTION_BOX($InputPFOption[$cmp_subject_id], 'name="'.$mark_name.'" class="tabletexttoptext"', '', $overall_value);
							$display .= '</td>';
						}
						else if($SchemeType[$cmp_subject_id] == "H" && $ScaleInput[$cmp_subject_id] == "G"){
							$overall_value = (count($MarksheetOverallScoreInfoArr) > 0 && isset($MarksheetOverallScoreInfoArr[$cmp_subject_id]) ) ? $MarksheetOverallScoreInfoArr[$cmp_subject_id][$StudentID]['MarkNonNum'] : "";
							$mark_name = "overall_mark_".$cmp_subject_id."_".$StudentID;
							$display .= '<td align="center" class="tabletext">';
							$display .= $linterface->GET_SELECTION_BOX($InputGradeOption[$cmp_subject_id], 'name="'.$mark_name.'" class="tabletexttoptext"', '', $overall_value);
							$display .= '</td>';
						}
					}
				}
				
	         }
	         $display .= '</tr>';
        }
	    
	    $display .= '</table>';
	    
		# Button
	    $display_button = '';
	    if(count($NewReportColumn) > 0){
			$display_button .= '<input name="Save" type="button" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$button_save.'" onclick="jSUBMIT_FORM()">';
	      	$display_button .= '<input name="Reset" type="reset" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$button_reset.'">';
      	}
        $display_button .= '<input name="Cancel" type="button" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$button_cancel.'" onClick="jBACK()">';
	    
		############################################################################################################
        
		# tag information
		$TAGS_OBJ[] = array($eReportCard['Management_MarksheetRevision'], "", 0);
		
		$linterface->LAYOUT_START();
		
?>

<script language="javascript">

var rowx = 0, coly = 0;			//init
var xno = "<?=$input_mark_count?>";yno = "<?=$StudentSize?>";		// set table size

var startContent = "";
var setStart = 1;

function setxy(jParX, jParY){
	rowx = jParX;
	coly = jParY;
	document.getElementById('mark['+ jParX +']['+ jParY +']').blur();
}

function paste_clipboard() {

	var temp1 = null;
	var text1 = null;
	var s = "";

	// you must use a 'text' field
	// (a 'hidden' text field will not show anything)

	temp1 = document.getElementById('text1').createTextRange();
	temp1.execCommand( 'Paste' );

	s = document.getElementById('text1').value; // clipboard is pasted to a string here
	setStart = 0;

	document.getElementById('mark['+ (rowx) +']['+ (coly) +']').value = "";

	var row_array = s.split("\n");
	var row_num = 0;

	while (row_num < row_array.length)
	{
		var col_array = row_array[row_num].split("\t");
		var col_num = 0;
		var xPos=0, yPos=0;
		while (col_num < col_array.length)
		{
			if ((col_num == 0) && (row_num == 0)) startContent = col_array[0];
			if ((row_array[row_num] != "")&&(col_array[col_num] != ""))
			{
				var temp = document.getElementById('mark['+ (row_num + rowx) +']['+ (col_num + coly) +']');
				xPos = parseInt(row_num) + parseInt(rowx);
				yPos = parseInt(col_num) + parseInt(coly);
				if (document.getElementById('mark['+ xPos +']['+ yPos +']') != null)
				{
					document.getElementById('mark['+ xPos +']['+ yPos +']').value = col_array[col_num];
				}
			}
			col_num+=1;
		}
		row_num+=1;
	}
}

function pasteTable(jPari, jParj) {
	setxy(jPari, jParj);
	paste_clipboard();
}

/* JS Form */
<?php 
	$js_special_case = '';
	if(count($possibleSpeicalCaseSet1) > 0){
		$js_special_case .= 'var specialCaseArr = new Array(';
		for($i=0 ; $i<count($possibleSpeicalCaseSet1) ; $i++)
			$js_special_case .= ($i != 0 ) ? ', "'.$possibleSpeicalCaseSet1[$i].'"' : '"'.$possibleSpeicalCaseSet1[$i].'"' ;
		$js_special_case .= ');';
	}
	echo $js_special_case."\n";
?>

function jCHANGE_STATUS(){
	document.getElementById("isOutdated").value = 1;
}

function jCHECK_SPECIAL_CASE(scVal){
	if(specialCaseArr.length > 0){
		for(var i=0 ; i<specialCaseArr.length ; i++){
			if(scVal == specialCaseArr[i])
				return true;
		}
		return false;
	}
	else {
		return false;
	}
}

function jCHECK_INPUT_MARK(xcor, ycor){
	var jCmpSubjectID = document.getElementById("SubjectID_Column_"+ycor).value;
	var jFullMark = document.getElementById("FullMark_"+jCmpSubjectID).value;
	var jScaleInput = document.getElementById("ScaleInput_"+jCmpSubjectID).value;
	var jSchemeType = document.getElementById("SchemeType_"+jCmpSubjectID).value;
	
	if(jSchemeType == "H" && jScaleInput == "M"){
		var obj = document.getElementById("mark["+xcor+"]["+ycor+"]");
		if(obj && obj.value != ""){
			if(!jCHECK_SPECIAL_CASE(obj.value)){
				if(obj.value < 0){
					alert("<?=$eReportCard['jsInputMarkCannotBeNegative']?>");
					obj.focus();
					return false;
				}
				
				if(!check_numeric(obj, obj.value, "<?=$eReportCard['jsInputMarkInvalid']?>")){
					obj.focus();
					return false;
				}
				
				if(parseFloat(obj.value) > parseFloat(jFullMark)){
					alert("<?=$eReportCard['jsInputMarkCannotBeLargerThanFullMark']?>");
					obj.focus();
					return false;
				}
			}
		}
	}
	
	return true;
}

// xno: ColumnSize ; yno: StudentSize
function jCHECK_FORM(){

	// check for mark when mark is used for input value
	for(var i=0; i<yno ; i++){
		for(var j=0; j<xno ; j++){
			if(!jCHECK_INPUT_MARK(i, j))
				return false;
		}
	}
	
	var cmp_subject_id = document.getElementsByName("CmpSubjectID[]");
	if(cmp_subject_id.length > 0){
		for(var i=0 ; i<cmp_subject_id.length ; i++){			
			var isComplete = document.getElementById("isCmpSubjectComplete_"+cmp_subject_id[i].value).value;
			if(isComplete == 1){
				if(!confirm("<?=$eReportCard['jsConfirmCmpSubjectMarksheetToIncomplete']?>"))
					return false;
				else 
					break;
			}
		}
	}
	
	return true;
}

function jSUBMIT_FORM(){
	var obj = document.FormMain;
	
	if(!jCHECK_FORM(obj))
		return;
	
	obj.submit();
}

function jBACK(){
	var form_id = <?=(($ClassLevelID != "") ? $ClassLevelID : "''")?>;
	var report_id = <?=(($ReportID != "") ? $ReportID : "''")?>;
	var subject_id = <?=(($SubjectID != "") ? $SubjectID : "''")?>;
	var isProgress = document.getElementById("isProgress").value;
	var status = document.getElementById("isOutdated").value;
	
	if(status == 1){
		if(!confirm("Do you want to save before you leave this page?")){
			if(isProgress == 0){
				location.href = "marksheet_revision.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+subject_id;
			} else {
				location.href = "../progress/submission.php?ClassLevelID="+form_id+"&ReportID="+report_id;
			}
		} else {
			document.getElementById("IsSaveBefCancel").value = 1;
			jSUBMIT_FORM();
		}
	} else {
		if(isProgress == 0){
			location.href = "marksheet_revision.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+subject_id;
		} else {
			location.href = "../progress/submission.php?ClassLevelID="+form_id+"&ReportID="+report_id;
		}
	}
}
</script>

<br/>
<form name="FormMain" method="post" action="marksheet_cmp_edit_update.php">
<table width="98%" border="0" cellpadding="4" cellspacing="0">
  <tr>
    <td align="center">
	  <table width="100%" border="0" cellspacing="0" cellpadding="2">
	  	<tr>
	      <td>
	      	<table border="0" cellpadding="5" cellspacing="0" class="tabletext">
	          <tr>
	            <td><?=$eReportCard['Period']?> : <strong><?=$PeriodName?></strong></td>
	          	<td><?=$eReportCard['Class']?> : <strong><?=$ClassName?></strong></td>
	          	<td><?=$eReportCard['Subject']?> : <strong><?=$SubjectName?></strong></td>
	          </tr>
	      	</table>
	      	<table border="0" cellspacing="0" cellpadding="0" align="right">
              <tr>
                <td align="right" valign="bottom">&nbsp;<?=$linterface->GET_SYS_MSG($msg);?></td>
              </tr>
            </table>
          </td>
	  	</tr>
	  	<tr>
	  	  <td>
	  	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <tr>
	          	<td align="right" class="tabletextremark"><?=$eReportCard['MarkRemindSet1']?></td>
	          </tr>
	        </table>
	      </td>
	    </tr>
	  	<tr>
	      <td>
	      <!-- Start of Content -->
	      <?=$display?>
	      <!-- End of Content -->
	      </td>
	  	</tr>
	  </table>
	  <br>
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="1" class="dotline"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
        </tr>
      	<tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td align="center" valign="bottom"><?=$display_button?></td>
              </tr>
        	</table>
          </td>
      	</tr>
      </table>
	</td>
  </tr>
</table>

<input type="hidden" name="isProgress" id="isProgress" value="<?=$isProgress?>"/>
<input type="hidden" name="ClassLevelID" id="ClassLevelID" value="<?=$ClassLevelID?>"/>
<input type="hidden" name="ReportID" id="ReportID" value="<?=$ReportID?>"/>
<input type="hidden" name="ClassID" id="ClassID" value="<?=$ClassID?>"/>

<input type="hidden" name="isOutdated" id="isOutdated" value="0"/>
<input type="hidden" name="IsSaveBefCancel" id="IsSaveBefCancel" value="0"/>

<?php
echo '<input type="hidden" name="SubjectID" id="SubjectID" value="'.$SubjectID.'"/>'."\n";

if(count($column_data) > 0){
	for($i=0 ; $i<sizeof($column_data) ; $i++){
		if($ReportType != "F" || ($ReportType == "F" && !$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']]) ){
			echo '<input type="hidden" name="ReportColumnID[]" id="ReportColumnID_'.$i.'" value="'.$column_data[$i]['ReportColumnID'].'"/>'."\n";
			
			# Format of Columen Weight ID: ColumnWeight_[ColumnID]_[CmpSubjectID]
			for($j=0 ; $j<count($CmpSubjectIDArr) ; $j++){
				echo '<input type="hidden" id="ColumnWeight_'.$column_data[$i]['ReportColumnID'].'_'.$CmpSubjectIDArr[$j].'" name="ColumnWeight_'.$column_data[$i]['ReportColumnID'].'_'.$CmpSubjectIDArr[$j].'" value="'.$WeightAry[$ReportColumnID][$CmpSubjectIDArr[$j]].'"/>'."\n";  
			}
		}
	}
}
	            
if(count($CmpSubjectIDArr) > 0){
	for($i=0 ; $i<count($CmpSubjectIDArr) ; $i++){
		echo '<input type="hidden" name="CmpSubjectID[]" id="CmpSubjectID_'.$i.'" value="'.$CmpSubjectIDArr[$i].'"/>'."\n";
	}
	
	if(count($SchemeFullMark) > 0){
		for($i=0 ; $i<count($CmpSubjectIDArr) ; $i++){
			echo '<input type="hidden" name="FullMark_'.$CmpSubjectIDArr[$i].'" id="FullMark_'.$CmpSubjectIDArr[$i].'" value="'.$SchemeFullMark[$CmpSubjectIDArr[$i]].'"/>'."\n";
			echo '<input type="hidden" name="ScaleInput_'.$CmpSubjectIDArr[$i].'" id="ScaleInput_'.$CmpSubjectIDArr[$i].'" value="'.$ScaleInput[$CmpSubjectIDArr[$i]].'"/>'."\n";
			echo '<input type="hidden" name="SchemeType_'.$CmpSubjectIDArr[$i].'" id="SchemeType_'.$CmpSubjectIDArr[$i].'" value="'.$SchemeType[$CmpSubjectIDArr[$i]].'"/>'."\n";
		}
	}
	
	if(count($ProgressArr) > 0){
		for($i=0 ; $i<count($CmpSubjectIDArr) ; $i++){
			$isCmpSubjectComplete = (count($ProgressArr) > 0 && isset($ProgressArr[$CmpSubjectIDArr[$i]]) ) ? $ProgressArr[$CmpSubjectIDArr[$i]]['IsCompleted'] : 0;
			echo '<input type="hidden" name="isCmpSubjectComplete_'.$CmpSubjectIDArr[$i].'" id="isCmpSubjectComplete_'.$CmpSubjectIDArr[$i].'" value="'.$isCmpSubjectComplete.'"/>'."\n";
		}
	}
}
?>

<textarea style="height: 1px; width: 1px;" id="text1" name="text1"></textarea>

</form>


<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>