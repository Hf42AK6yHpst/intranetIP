<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
#################################################################################################

include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
$lreportcard = new libreportcard2008j();
//$lreportcard = new libreportcard();

$limport = new libimporttext();
$linterface = new interface_html();

# Preparation
$params = "ClassLevelID=$ClassLevelID&ReportID=$ReportID&ClassID=$ClassID&SubjectGroupID=$SubjectGroupID&isProgress=$isProgress&isProgressSG=$isProgressSG";


$ImportDataArr = unserialize(stripslashes($ImportData));

$lreportcard->Start_Trans();

$LastModifiedUserID = $UserID;
$DateName = "LastMarksheetInput";
$NumOfRec=count($ImportDataArr);
for($i=0;  $i<$NumOfRec; $i++ )
{
	# import column score
	foreach((array)$ImportDataArr[$i]["ColumnMark"] as $j => $ColumnMark)
	{
		list($StudentID,$SubjectID,$ReportColumnID,$SchemeID,$MarkType,$MarkRaw,$MarkNonNum,$IsEstimated) = $ColumnMark;

	    $table = $lreportcard->DBName.".RC_MARKSHEET_SCORE";
		# Remove all reocrds of student in the subject if exists
		$sql = "DELETE FROM 
					$table 
				WHERE 
					StudentID = '$StudentID' AND 
					SubjectID = '$SubjectID' AND 
					ReportColumnID = '$ReportColumnID'
				";
				
		$result['delete_'.$i.'_'.$j] = $lreportcard->db_db_query($sql);
		
		# Insert Raw Result
		$sql = "INSERT INTO $table
					(StudentID, SubjectID, ReportColumnID, SchemeID, 
					 MarkType, MarkRaw, MarkNonNum, LastModifiedUserID, 
					 DateInput, DateModified, IsEstimated) 
				VALUES
					('$StudentID', '$SubjectID', '$ReportColumnID', '$SchemeID', 
					 '$MarkType', '$MarkRaw', '$MarkNonNum', '$LastModifiedUserID', 
					 NOW(), NOW(), '$IsEstimated' )
				";
				
		$result['insert_'.$i.'_'.$j] = $lreportcard->db_db_query($sql);
		
		# Update the Time of LastMarksheetInput
		if($result['insert_'.$i.'_'.$j]){
			$lreportcard->UPDATE_REPORT_LAST_DATE($ReportID, $DateName);
		}
	}
	
	# import overall score
	if($ImportDataArr[$i]["OverallMark"])
	{
		list($StudentID,$SubjectID,$ReportID,$SchemeID,$MarkType,$MarkNonNum,$IsEstimated) = $ImportDataArr[$i]["OverallMark"];
	
		$table = $lreportcard->DBName.".RC_MARKSHEET_OVERALL_SCORE";
		# Remove all reocrds of student in the subject if exists
		$sql = "DELETE FROM 
					$table 
				WHERE 
					StudentID = '$StudentID' AND 
					SubjectID = '$SubjectID' AND 
					ReportID = '$ReportID'
				";
				
		$result['delete_overall_'.$i] = $lreportcard->db_db_query($sql);
		
		# Insert Raw Result
		$sql = "INSERT INTO $table
					(StudentID, SubjectID, ReportID, SchemeID, 
					 MarkType, MarkNonNum, LastModifiedUserID, 
					 DateInput, DateModified, IsEstimated) 
				VALUES
					('$StudentID', '$SubjectID', '$ReportID', '$SchemeID', 
					 '$MarkType', '$MarkNonNum', '$LastModifiedUserID', 
					 NOW(), NOW(), '$IsEstimated' )
				";
				
		$result['insert_overall_'.$i] = $lreportcard->db_db_query($sql);
		
		# Update the Time of LastMarksheetInput
		if($result['insert_overall_'.$i])
			$lreportcard->UPDATE_REPORT_LAST_DATE($ReportID, $DateName);
	}
}

if(!in_array(false, (array)$result)) {
	//$successUpdatedCount--;
	$lreportcard->Commit_Trans();
	$SuccessMsg = count($ImportDataArr)." ".$Lang['General']['ImportArr']['RecordsImportedSuccessfully'];
} else {
	$lreportcard->RollBack_Trans(); 
	$SuccessMsg = $eReportCard['FailToImportMarksheetScore'];
}

# tag information
$TAGS_OBJ[] = array($eReportCard['Management_MarksheetRevision'], "", 0);

# page navigation (leave the array empty if no need)
$PAGE_NAVIGATION[] = array($eReportCard['ImportMarks'], "");

if(trim($SubjectGroupID)=='' && trim($ClassID)=='')
{
	$BackUrl ="marksheet_revision.php";
	$params .= "&SubjectID=".((isset($ParentSubjectID) && $ParentSubjectID != "") ?"$ParentSubjectID":"$SubjectID");
}
else
{
	$BackUrl = "marksheet_edit.php";
	$params .= "&SubjectID=$SubjectID";
	$params .= (isset($ParentSubjectID) && $ParentSubjectID != "") ? "&ParentSubjectID=$ParentSubjectID" : "";
	
}
$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='$BackUrl?$params'","back");

$CurrentPage = "Management_MarkSheetRevision";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
							
?>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td colspan="2"><?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?><br><br></td>
	</tr>
	<tr>
		<td colspan="2"><?= $linterface->GET_IMPORT_STEPS(3) ?></td>
	</tr>
	<tr>
		<td class='tabletext' align='center'><br><?=$SuccessMsg?><br></td>
	</tr>
	<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<tr>
		<td colspan="2" align="center"><?=$BackBtn?></td>
	</tr>
</table>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>