<?php
/*
 * Change Log:
 * Date:	2017-01-05 Villa	add param for submark sheet layer
 */

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	}
	/* Temp Library*/
	include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
	$libreportcard = new libreportcard2008j();
	//$lreportcard = new libreportcard();
	
	$CurrentPage = "Sub-Marksheet Submission";
	$MODULE_OBJ = $libreportcard->GET_MODULE_OBJ_ARR();
	$MODULE_OBJ['title'] = $eReportCard['SubMarksheet'];
	
	$PAGE_NAVIGATION[] = array($button_edit);
	
	//$IsMSSubmissionPeriod = $libreportcard->CHECK_MARKSHEET_SUBMISSION_PERIOD();
	$IsMSSubmissionPeriod = true;
		
    if ($libreportcard->hasAccessRight())
    {
        $linterface = new interface_html("popup.html");
        $ColumnID = $AssessmentColumnID;
        $ColumnInfo = $libreportcard->GET_SUB_MARKSHEET_COLUMN_INFO($ColumnID);
        $ColumnTitle = $ColumnInfo[$ColumnID]["ColumnTitle"];
        $Ratio = $UsePercent==1?$ColumnInfo[$ColumnID]["Ratio"]*100:$ColumnInfo[$ColumnID]["Ratio"];
        $FullMark = $ColumnInfo[$ColumnID]["FullMark"];
        // Add $PassingMark [2014-1121-1759-15164]
        $PassingMark = $ColumnInfo[$ColumnID]["PassingMark"];
        $RatioRemarks = $UsePercent==1?$eReportCard['AssessmentRatioRemarks2']:$eReportCard['AssessmentRatioRemarks'];
        
        $ButtonRow = "<tr><td align='center' id='bottom'>";
		$ButtonRow .= $linterface->GET_ACTION_BTN($button_save, "button", "javascript:checkForm(document.form1)");
		$ButtonRow .= toolBarSpacer();
		$ButtonRow .= $linterface->GET_ACTION_BTN($button_reset, "reset");
		$ButtonRow .= toolBarSpacer();
		$ButtonRow .= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()");

		$ButtonRow .= "</td></tr>";
				
################################################################

$linterface->LAYOUT_START();
?>

<script language="javascript">

function checkForm(obj) {
	if(!check_text(obj.ColumnTitle, "<?= $i_alert_pleasefillin . $eReportCard['ColumnTitle'] ?>.")) return false;
	if(!check_text(obj.ColumnRatio, "<?= $i_alert_pleasefillin . $eReportCard['AssessmentRatio'] ?>.")) return false;
	
	if(obj.FullMark != null)
	{
		if(!check_text(obj.FullMark, "<?= $i_alert_pleasefillin . $eReportCard['SchemesFullMark'] ?>.")) return false;
		value = obj.FullMark.value;
		if (value == null || isNaN(value) || value < 0 || !IsNumeric(value))
		{
			alert("<?=  $eReportCard['jsWarnFullMarkNotANumber'] ?>.");
			return false;
		}
	}
	
	if(obj.PassingMark != null)
	{
		if(!check_text(obj.PassingMark, "<?= $i_alert_pleasefillin . $eReportCard['SchemesPassingMark'] ?>.")) return false;
		value = obj.PassingMark.value;
		fullvalue = obj.FullMark.value;
		if (value == null || isNaN(value) || value < 0 || !IsNumeric(value))
		{
			alert("<?=  $eReportCard['jsWarnPassingMarkNotANumber'] ?>.");
			return false;
		}
		if(Number(value) > Number(fullvalue))
		{
			alert("<?=  $eReportCard['jsWarnPassingMarkLargerThanFullMark'] ?>.");
			return false;
		}
	}
	
	obj.action = "sub_marksheet_new_update.php";
	obj.submit();
}

</script>

<form name="form1" action="" method="post">
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td id="top"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td>
		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td align="right" colspan="2"><?= $linterface->GET_SYS_MSG($Result)?></td></tr>
		<tr>
			<td colspan="2">
				<table width='100%' border='0' cellpadding="3" cellspacing="1">
					<tr>
						<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eReportCard['ColumnTitle']?></td>
						<td width="70%" class="tabletext"><input class="textboxtext" type="text" name="ColumnTitle" size="20" maxlength="128" value="<?=$ColumnTitle?>"></td>
					</tr>
					<?if($ReportCardCustomSchoolName=="munsang_college"){?>
					<tr>
						<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eReportCard['SchemesFullMark']?></td>
						<td width="70%" class="tabletext"><input class="textboxtext" type="text" name="FullMark" size="20" maxlength="8" value="<?=$FullMark?>"></td>
					</tr>
					<?}?>
					<?if($eRCTemplateSetting['SubMS']['DisplayFullMarks']){?>
					<tr>
						<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eReportCard['SchemesFullMark']?></td>
						<td width="70%" class="tabletext"><input class="textboxtext" type="text" name="FullMark" size="20" maxlength="8" value="<?=$FullMark?>"></td>
					</tr>
					<?}?>
					<?if($eRCTemplateSetting['SubMS']['DisplayPassingMarks']){?>
					<tr>
						<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eReportCard['SchemesPassingMark']?></td>
						<td width="70%" class="tabletext"><input class="textboxtext" type="text" name="PassingMark" size="20" maxlength="8" value="<?=$PassingMark?>"></td>
					</tr>
					<?}?>
					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eReportCard['AssessmentRatio']?></td>
						<td class="tabletext">
							<input class="textboxnum" type="text" name="ColumnRatio" value="<?=$Ratio?>">&nbsp;
							<? if(!$eRCTemplateSetting['SubMS']['SubMSOverallCalculation']){ ?>
								<span class='tabletextremark'>(<?=$RatioRemarks?>)</span>
							<? } ?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td class="dotline" colspan="2"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width=10 height=1></td></tr>
		<?=$x?>
		</table>
	</td>
</tr>
<tr>
	<td>
<center><?=$equation?></center>
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<?=$ButtonRow?>
	</table>
	</td>
</tr>
</table>

<input type="hidden" name="ColumnID" value="<?=$ColumnID?>" />
<input type="hidden" name="ReportID" value="<?=$ReportID?>" />
<input type="hidden" name="ReportColumnID" value="<?=$ReportColumnID?>" />
<input type="hidden" name="SubjectID" value="<?=$SubjectID?>" />
<input type="hidden" name="ClassID" value="<?=$ClassID?>" />
<input type="hidden" name="SubjectGroupID" value="<?=$SubjectGroupID?>" />
<input type="hidden" name="AssessmentColumnID" value="<?=$ColumnID?>" />
<input type="hidden" name="ColumnIDList" value="<?=$ColumnIDList?>" />
<input type="hidden" name="StudentIDList" value="<?=$StudentIDList?>" />
<input type="hidden" name="UsePercent" value="<?=$UsePercent?>" />
<input type="hidden" name="SubmitType" />

<input type="hidden" name="jsReportColumn" value="<?=$jsReportColumn?>" />
<input type="hidden" name="SubReportColumnID" value="<?=$SubReportColumnID?>" />
<input type="hidden" name="currentLevel" value="<?=$currentLevel?>" />

</form>
<?
		$linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>