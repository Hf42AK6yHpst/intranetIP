<?php
// Using: Bill
/********************************************************
 * Modification log
 * 20160129 Bill
 * 		- modified jBACK(), to fix js error 
 * 20151008	Bill	[2015-0520-1100-36066]
 * 		- Cust - Import Marksheet for All subjects
 * 		- Copy from marksheet_import.php
 * ******************************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
	$lreportcard = new libreportcard2008j();
	
	$CurrentPage = "Management_MarkSheetRevision";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    
    if ($lreportcard->hasAccessRight() && $eRCTemplateSetting['Marksheet']['ImportAllSubjectScore'])
    {
    	$linterface = new interface_html();
        
		############################################################################################################
		
		# Get MarkSheet Info
//		// Tags of MarkSheet(0: Raw Mark, 1: Weighted Mark, 2: Converted Grade
//		$TagsType = (isset($TagsType) && $TagsType == "") ? $TagsType : 0;
		
		// Period (Term x, Whole Year, etc)
		$PeriodArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID, $ReportID);
		$PeriodName = (count($PeriodArr) > 0) ? $PeriodArr['SemesterTitle'] : '';
				
//		// Loading Report Template Data
//		$basic_data = $lreportcard->returnReportTemplateBasicInfo($ReportID);			// Basic  Data
//		$column_data = $lreportcard->returnReportTemplateColumnData($ReportID); 		// Column Data
//		$ReportType = $basic_data['Semester'];
//		
//		/* 
//		* For Whole Year Report use only
//		* Check for the Mark Column is allowed to fill in mark when any term does not have related report template 
//		* by SemesterNum (ReportType) && ClassLevelID
//		* An array is initialized as a control variable [$ColumnHasTemplateAry]
//		* A variable is initialized as a control variable [$hasDirectInputMark], must be used with [$ReportType == "F"]
//		* to control the Import function to be used or not
//		*/
//		$ColumnHasTemplateAry = array();
//		$hasDirectInputMark = 0;
//		$ImportReportColumnMarkReminder = 0;
//		if(count($column_data) > 0){	
//			for($i=0 ; $i<sizeof($column_data) ; $i++){
//				if($ReportType == "F"){
//					$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] = $lreportcard->CHECK_REPORT_TEMPLATE_FROM_COLUMN($column_data[$i]['SemesterNum'], $ClassLevelID);
//					if(!$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']])
//						$hasDirectInputMark = 1;
//				}
//				else {
//					$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] = 1;
//				}
//					
//				//if($ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] && $ReportType == "F" && $ScaleInput == "M"){
//				if($ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] && $ReportType == "F"){
//					$ImportReportColumnMarkReminder = 1;
//				}
//			}
//		}
		
		# Button
	    $display_button = '';
	    $display_button .= $linterface->GET_ACTION_BTN($button_confirm, "button", "jSUBMIT_FORM()")."&nbsp;";
	    $display_button .= $linterface->GET_ACTION_BTN($button_cancel, "button", "jBACK()");
	    
        $param = "ClassLevelID=$ClassLevelID&ReportID=$ReportID&ClassID=$ClassID&SubjectGroupID=$SubjectGroupID&isProgress=$isProgress&isProgressSG=$isProgressSG";
        
//        if ($SubjectGroupID != '')
//		{
//			$obj_SubjectGroup = new subject_term_class($SubjectGroupID);
//			
//		     $SubjectGroupName .= '<tr>'."\n";
//	      	    $SubjectGroupName .= '<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">'.$Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'].'</td>'."\n";
//	      	    $SubjectGroupName .= '<td>'."\n";
//	      	    	$SubjectGroupName .= $obj_SubjectGroup->Get_Class_Title()."\n";
//	      	    $SubjectGroupName .= '</td>'."\n";
//	      	  $SubjectGroupName .= '</tr>'."\n";
//			
//			//$TitleDisplay = "<td>".$Lang['SysMgr']['SubjectClassMapping']['SubjectGroup']." : <strong>".$SubjectGroupName."</strong></td>";
//		}
        
		############################################################################################################
        
		# Tag
		$TAGS_OBJ[] = array($eReportCard['Management_MarksheetRevision'], "", 0);
		
		# Page Navigation (leave the array empty if no need)
		$PAGE_NAVIGATION[] = array($eReportCard['ImportMarks'], "");
		
		# Message
		$ReturnMsg = $Lang['General']['ReturnMessage'][$Result];
		$linterface->LAYOUT_START($ReturnMsg);
?>

<script language="javascript">
function jCHECK_FORM(){
	return true;
}

function jSUBMIT_FORM(){
	var obj = document.FormMain;
	
	if(!jCHECK_FORM(obj))
		return;
	
	obj.submit();
}

function jBACK(){
	var form_id = document.getElementById("ClassLevelID").value;
	var report_id = document.getElementById("ReportID").value;
	var subject_id = document.getElementById("SubjectID").value;
	var class_id = document.getElementById("ClassID").value;
	var subject_group_id = document.getElementById("SubjectGroupID").value;
	var is_progress = document.getElementById("isProgress").value;
	var is_progress_sg = document.getElementById("isProgressSG").value;
	
	var params = '';
	params += "ClassLevelID="+form_id+"&ReportID="+report_id+"&ClassID="+class_id+"&SubjectGroupID="+subject_group_id;
	params += "&SubjectID=" + subject_id + "&isProgess=" + is_progress + "&isProgressSG=" + is_progress_sg;
	<?php 
//	if(isset($ParentSubjectID)){
//		echo 'var parent_subject_id = '.(($ParentSubjectID != '') ? $ParentSubjectID : '""').';';
//		echo 'params += "&ParentSubjectID="+parent_subject_id;';
//	} 
	?>
	
//	if(class_id.Trim()=='' && subject_group_id.Trim()=='')
//		url = "marksheet_revision.php"; 
//	else
//		url = "marksheet_edit.php";

	location.href = "marksheet_revision_subject_group.php?"+params;
}

function jGenSample()
{
	self.location.href='generate_csv_template_all_subject.php?<?=$param?>';
}

//function aj_Get_Grading_Scheme(cur_id)
//{
//	var TaskStr = "View"
////	alert($("div#SchemePreviewDiv").html());
//	if(!LoadedSchemeData[cur_id])
//	{
//		$("div#SchemePreviewDiv").html('<table border="0" cellpadding="0" cellspacing="0" bgcolor="#feffed"><tr><td>loading...</td></tr></table>').show();
//		
//		$.post("../../settings/subjects_and_form/ajax_get_grading_scheme.php", { schemeID: cur_id, Task: TaskStr },
//			function(data){
//				$("div#SchemePreviewDiv").html(data);
//				LoadedSchemeData[cur_id] = data;
//			}
//		);
//	}
//	else
//	{
//		$("div#SchemePreviewDiv").html(LoadedSchemeData[cur_id]);
//		$("div#SchemePreviewDiv").show();
//	}
//		
//}
</script>

<br/>
<form name="FormMain" method="post" action="marksheet_import_all_subject_confirm.php" enctype="multipart/form-data">
<table width="98%" border="0" cellpadding="4" cellspacing="0">
  <tr>
    <td align="center">
	  <table width="100%" border="0" cellspacing="0" cellpadding="2">
	    <tr>
	      <td colspan="2"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	  	</tr>
	    <tr>
	      <td colspan="2"><?= $linterface->GET_IMPORT_STEPS(1) ?></td>
	  	</tr>
	  	<tr>
	      <td width="30px">&nbsp;</td>
	      <td>
	      	<!--
	      	<?php if($ImportReportColumnMarkReminder=1){ ?>
	      	<br/>
	      	<table align="center" width="80%" border="0" cellpadding="3" cellspacing="0" class="Warningtable">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="33%" valign="top">
                        <table width="100%" border="0" cellpadding="3" cellspacing="0">
                          <tr>
                            <td align="center" class="Warningtitletext"><br><?=$eReportCard['CSVMarksheetTemplateReminder']?><br><br></td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
	      	<?php } ?>
	      	-->
	      	<br/>
	      	<table width="100%" border="0" cellspacing="0" cellpadding="5" class="tabletext">
	      	  <tr>
	      	    <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eReportCard['ReportCard']?></td>
	      	    <td>
	      	    	<?=$PeriodName?>
	      	    </td>
	      	  </tr>
	      	  <!--
      	    	<?=$SubjectGroupName?>
	      	  <tr>
	      	    <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eReportCard['Subject']?></td>
	      	    <td>
	      	    	<?=$SubjectName?>
	      	    </td>
	      	  </tr>
	      	  <tr>
	      	    <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eReportCard['GradingScheme']?></td>
	      	    <td>
	      	    	<span id="GradingSchemeSpan"></span>
	      	    </td>
	      	  </tr>
	      	  -->
	      	  <tr>
	      	    <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eReportCard['File']?></td>
	      	    <td>
	      	    	<input type="file" name="userfile" size="50"><br />
	      	    </td>
	      	  </tr>
	      	  <tr>
				<td colspan="2">
					<a class="tablelink" href="javascript:jGenSample()"><img src="<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/icon_files/xls.gif" border="0" align="absmiddle" hspace="3"> <?=$eReportCard['DownloadCSVFile']?></a>
				</td>
			  </tr>
	      	</table>
	      </td>
	    </tr>
	  </table>
	  <br/>
	  <div class="edit_bottom_v30">
		<?=$display_button?>	  
	  </div> 
	  <!--<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="1" class="dotline"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
        </tr>
      	<tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td align="center" valign="bottom"><?=$display_button?></td>
              </tr>
        	</table>
          </td>
      	</tr>
      </table>-->
	</td>
  </tr>
</table>
<div id="SchemePreviewDiv" style="position:absolute; z-index:1; display:none; border-top:solid 1px; border-top-color:#999999; border-left:solid 1px; border-left-color:#999999; border-right:solid 2px; border-right-color:#646464; border-bottom:solid 2px; border-bottom-color:#646464;"></div>
<input type="hidden" name="isProgress" id="isProgress" value="<?=$isProgress?>"/>
<input type="hidden" name="isProgressSG" id="isProgressSG" value="<?=$isProgressSG?>"/>
<input type="hidden" name="ClassLevelID" id="ClassLevelID" value="<?=$ClassLevelID?>"/>
<input type="hidden" name="ReportID" id="ReportID" value="<?=$ReportID?>"/>
<input type="hidden" name="ClassID" id="ClassID" value="<?=$ClassID?>"/>
<input type="hidden" name="SubjectID" id="SubjectID" value="<?=$SubjectID?>"/>
<input type="hidden" name="SubjectGroupID" id="SubjectGroupID" value="<?=$SubjectGroupID?>"/>
<?=$hiddenSubjectID?>
<?php
if(isset($ParentSubjectID) && $ParentSubjectID != "")
	echo '<input type="hidden" name="ParentSubjectID" id="ParentSubjectID" value="'.$ParentSubjectID.'"/>'."\n";
?>

</form>

<?
        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>