<?php
// Editing: 
/*
 * Modification Log:
 * 20110829 (Ivan)
 * 	- added estimated mark logic
 */
 
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");

intranet_auth();
intranet_opendb();

#################################################################################################

$lreportcard = new libreportcard2008j();
//$lreportcard = new libreportcard();

# Initialization
$result = array();
$MarksheetScoreArr = array();

# Get Data
$StudentIDArr = $StudentID;
$LastModifiedUserID = $_SESSION['UserID'];
$ReportColumnIDArr = $ReportColumnID;

$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0,0,$ReportID);
$SchemeID = $SubjectFormGradingArr['SchemeID'];
$ScaleInput = $SubjectFormGradingArr['ScaleInput'];

$SchemeInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
$SchemeType = $SchemeInfo['SchemeType'];

$DateName = "LastMarksheetInput";
	
# Get Complete Status of Common Subject or Parent Subject
$ProgressArr = array();
$ProgressArr = $lreportcard->GET_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, $ClassID);
$isSubjectComplete = (count($ProgressArr) > 0) ? $ProgressArr['IsCompleted'] : 0;

# Get Report Template Details
$basic_data = $lreportcard->returnReportTemplateBasicInfo($ReportID);			// Basic  Data
$column_data = $lreportcard->returnReportTemplateColumnData($ReportID); 		// Column Data
$ReportType = $basic_data['Semester'];

# Check for Any Existing Template of Term Report Column in Whole Year Report
$ColumnHasTemplateAry = array();
if(count($column_data) > 0){
	for($i=0 ; $i<sizeof($column_data) ; $i++){
		if($ReportType == "F")
			$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] = $lreportcard->CHECK_REPORT_TEMPLATE_FROM_COLUMN($column_data[$i]['SemesterNum'], $ClassLevelID);
		else 
			$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] = 1;
	}
}

# Check whether it is Component Subject or Not
$isCmpSubject = $lreportcard->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($SubjectID);


# Mark Type : M-Mark, G-Grade, PF-PassFail, SC-SpecialCase
# Get Marksheet Column Input Mark Data
$count = 0;
if(count($ReportColumnIDArr) > 0){
	for($j=0 ; $j<count($ReportColumnIDArr) ; $j++){
		
		# Case 1: when it is term report
		# Case 2: when it is whole year report and the particular term report column has not defined term report template
		if($ReportType != "F" || ($ReportType == "F" && !$ColumnHasTemplateAry[$ReportColumnIDArr[$j]]) ){
		
			if(count($StudentIDArr) > 0){
				for($i=0 ; $i<count($StudentIDArr) ; $i++){
					$MarkNonNum = '';
					$MarkRaw = '-1';
					$RawData = ${"mark_".$ReportColumnIDArr[$j]."_".$StudentIDArr[$i]};
					
					$IsEstimated = ($lreportcard->Is_Estimated_Score($RawData))? 1 : 0;
					$RawData = $lreportcard->Convert_Marksheet_UIScore_To_DBRawData($RawData);
					
					$isSpecialCase = ($SchemeType == "PF") ? $lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2($RawData) : $lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1($RawData);
					
					
					// Set the MarkType 
					if($SchemeType == "H"){
						if($ScaleInput == "G"){
							$MarkType = "G";
							$MarkNonNum = $RawData;
						}
						else {
							$MarkType = "M";
							if($isSpecialCase)
								$MarkRaw = ($RawData == "+") ? 0 : -1;
							else 
								$MarkRaw = ($RawData != "" ) ? $lreportcard->ROUND_MARK($RawData, "SubjectScore") : -1;
						}
					}
					else if($SchemeType == "PF"){
						$MarkType = "PF";
						$MarkNonNum = $RawData;
					}
					else {
						$MarkType = "M";
						$MarkRaw = $RawData;
					}
					
					$MarksheetScoreArr[$count]["MarksheetScoreID"] = (isset(${"MarksheetScoreID_".$StudentIDArr[$i]})) ? ${"MarksheetScoreID_".$StudentIDArr[$i]} : "";
					$MarksheetScoreArr[$count]['StudentID'] = $StudentIDArr[$i];
					$MarksheetScoreArr[$count]['SubjectID'] = $SubjectID;
					$MarksheetScoreArr[$count]['ReportColumnID'] = $ReportColumnIDArr[$j];				
					$MarksheetScoreArr[$count]['SchemeID'] = $SchemeID;
					$MarksheetScoreArr[$count]['MarkType'] = ($isSpecialCase) ? "SC" : $MarkType;
					$MarksheetScoreArr[$count]['MarkRaw'] = trim($MarkRaw);
					$MarksheetScoreArr[$count]['MarkNonNum'] = ($isSpecialCase) ? trim($RawData) : trim($MarkNonNum);
					$MarksheetScoreArr[$count]["LastModifiedUserID"] = $LastModifiedUserID;
					$MarksheetScoreArr[$count]['IsEstimated'] = $IsEstimated;
					
					$count++;
				}
			}
		}
	}
}


if(($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF" || ($isAllColumnWeightZero == true && $SchemeType == "H" && $ScaleInput == "M")){
	if(count($StudentIDArr) > 0){
		for($i=0 ; $i<count($StudentIDArr) ; $i++){
			$MarkNonNum = '';
			$RawData = (isset(${"overall_mark_".$StudentIDArr[$i]})) ? ${"overall_mark_".$StudentIDArr[$i]} : "";
			
			$IsEstimated = ($lreportcard->Is_Estimated_Score($RawData))? 1 : 0;
			$RawData = $lreportcard->Convert_Marksheet_UIScore_To_DBRawData($RawData);
					
			$isSpecialCase = ($SchemeType == "PF") ? $lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2($RawData) : $lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1($RawData);
			//$MarkType = ($SchemeType == "PF") ? "PF" : "G";
			
			if ($SchemeType == 'PF') {
				$MarkType = 'PF';
			}
			else {
				$MarkType = $ScaleInput;
			}
			
			$MarksheetOverallScoreArr[$i]["MarksheetOverallScoreID"] = (isset(${"MarksheetOverallScoreID_".$StudentIDArr[$i]})) ? ${"MarksheetOverallScoreID_".$StudentIDArr[$i]} : "";
			$MarksheetOverallScoreArr[$i]['StudentID'] = $StudentIDArr[$i];
			$MarksheetOverallScoreArr[$i]['SubjectID'] = $SubjectID;
			$MarksheetOverallScoreArr[$i]['ReportID'] = $ReportID;				
			$MarksheetOverallScoreArr[$i]['SchemeID'] = $SchemeID;
			$MarksheetOverallScoreArr[$i]['MarkType'] = ($isSpecialCase) ? "SC" : $MarkType;
			$MarksheetOverallScoreArr[$i]['MarkNonNum'] = trim($RawData);
			$MarksheetOverallScoreArr[$i]["LastModifiedUserID"] = $LastModifiedUserID;
			$MarksheetOverallScoreArr[$i]["IsEstimated"] = $IsEstimated;
		}
	}
}

# Main
# INSERT - Insert New Record of Students corresponding to Certain Report Column to the MarkScore Table
if(count($ReportColumnIDArr) > 0){
	for($j=0 ; $j<count($ReportColumnIDArr) ; $j++){
		
		# Case 1: when it is term report
		# Case 2: when it is whole year report and the particular term report column has not defined term report template
		if($ReportType != "F" || ($ReportType == "F" && !$ColumnHasTemplateAry[$ReportColumnIDArr[$j]]) ){
			$result['insert_'.$j] = $lreportcard->INSERT_MARKSHEET_SCORE($StudentIDArr, $SubjectID, $ReportColumnIDArr[$j], $LastModifiedUserID);
		}
	}
}

# INSERT - Insert New Record of Students in Marksheet OVERALL Score for the cases 
# when the scheme type is Honor-Based (H) AND its scaleinput is Grade(G) OR
# when the scheme type is PassFail-Based (PF)
if(count($MarksheetOverallScoreArr) > 0){
	$result['insert_overall_mark'] = $lreportcard->INSERT_MARKSHEET_OVERALL_SCORE($StudentIDArr, $SubjectID, $ReportID, $LastModifiedUserID);
}

# UPDATE - Update Record of Marksheet Score
if(count($MarksheetScoreArr) > 0){
	$result['update_marksheet'] = $lreportcard->UPDATE_MARKSHEET_SCORE($MarksheetScoreArr);
	
	# Update the Time of LastMarksheetInput
	if($result['update_marksheet']){
		$result['udpate_report_lastdate'] = $lreportcard->UPDATE_REPORT_LAST_DATE($ReportID, $DateName);
		
		if($isSubjectComplete){
			//$result['udpate_submission_progress'] = $lreportcard->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, $ClassID, 0);
			if(isset($ParentSubjectID) && $ParentSubjectID != ""){
				$result['update_submission_progress'] = $lreportcard->UPDATE_RELATED_SUBJECT_SUBMISSION_PROGRESS($ReportID, $ClassLevelID, $ClassID, $SubjectID, 0, $ParentSubjectID, $SubjectGroupID);
			} else {
				$result['update_submission_progress'] = $lreportcard->UPDATE_RELATED_SUBJECT_SUBMISSION_PROGRESS($ReportID, $ClassLevelID, $ClassID, $SubjectID, 0, '', $SubjectGroupID);
			}
		}
	}
}

# UPDATE - Update Record of Marksheet OVERALL Score if any 
if(count($MarksheetOverallScoreArr) > 0){	
	$result['update_marksheet_overall'] = $lreportcard->UPDATE_MARKSHEET_OVERALL_SCORE($MarksheetOverallScoreArr);
	
	if($result['update_marksheet_overall']){
		$result['udpate_report_lastdate'] = $lreportcard->UPDATE_REPORT_LAST_DATE($ReportID, $DateName);
		
		if($isSubjectComplete){
			//$result['udpate_submission_progress'] = $lreportcard->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, $ClassID, 0, $SubjectGroupID);
			if(isset($ParentSubjectID) && $ParentSubjectID != ""){
				$result['update_submission_progress'] = $lreportcard->UPDATE_RELATED_SUBJECT_SUBMISSION_PROGRESS($ReportID, $ClassLevelID, $ClassID, $SubjectID, 0, $ParentSubjectID, $SubjectGroupID);
			} else {
				$result['update_submission_progress'] = $lreportcard->UPDATE_RELATED_SUBJECT_SUBMISSION_PROGRESS($ReportID, $ClassLevelID, $ClassID, $SubjectID, 0, '', $SubjectGroupID);
			}
		}
	}
}

#################################################################################################
if(!in_array(false, $result))
	$msg = "update";
else 
	$msg = "update_failed";


#################################################################################################
intranet_closedb();

#if($IsSaveBefCancel == 1){
	$params = "ClassLevelID=$ClassLevelID&ReportID=$ReportID&isProgress=$isProgress&isProgressSG=$isProgressSG";
	if(isset($ParentSubjectID) && $ParentSubjectID != ""){
		if ($isProgress)
			$redirectTo = "../progress/submission.php";
		else
		{
			if ($ReportType != 'F')
				$redirectTo = "marksheet_revision2_subject_group.php";
			else
				$redirectTo = "marksheet_revision2.php";
		}
		$params .= "&SubjectID=$ParentSubjectID";
		$params .= "&ClassID=$ClassID";
		$params .= "&SubjectGroupID=$SubjectGroupID";
		header("Location: $redirectTo?$params&msg=$msg");
	} else {
		if ($isProgress)
			$redirectTo = "../progress/submission.php";
		else
		{
			if ($ReportType != 'F')
				$redirectTo = "marksheet_revision_subject_group.php";
			else
				$redirectTo = "marksheet_revision.php";
		}
		$params .= "&SubjectID=$SubjectID";
		$params .= "&SubjectGroupID=$SubjectGroupID";
		header("Location: $redirectTo?$params&msg=$msg");
	}
/*
} else {
	$params = "ClassLevelID=$ClassLevelID&ReportID=$ReportID&SubjectID=$SubjectID";
	$params .= (isset($ParentSubjectID) && $ParentSubjectID != "") ? "&ParentSubjectID=$ParentSubjectID" : "";
	$params .= "&ClassID=$ClassID";	
	if($newTagsType != "") $params .= "&TagsType=".$newTagsType;
	header("Location: marksheet_edit.php?$params&msg=$msg");
}
*/


?>