<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();

/* Temp Library*/
include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
$lreportcard = new libreportcard2008j();
//$lreportcard = new libreportcard();
#########################################################################

# Initialization
$result = array();
$ClassUpdateInfo = array();
$FormSubjectArr = array();
$count = 0;

# Main
if($isAll){
	if(!isset($isCmpSubject) || (isset($isCmpSubject) && !$isCmpSubject) ){		// Case triggered by Subject without any Component Subject
		if($SubjectID == "")
			$FormSubjectArr = $lreportcard->returnSubjectwOrder($ClassLevelID, 1);
		
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
	
		if(count($ClassArr) > 0){
			for($i=0 ; $i<count($ClassArr) ;$i++){
				$ClassUpdateInfo[$count]['ClassID'] = $ClassArr[$i][0];
			
				if($SubjectID == "" && !empty($FormSubjectArr)){
					foreach($FormSubjectArr as $FormSubjectID => $Data){
						if(is_array($Data)){
							foreach($Data as $FormCmpSubjectID => $SubjectName){
								// Only Count Parent Subject now
								if($FormCmpSubjectID==0){
									$ClassUpdateInfo[$count]['SubjectID'] = $FormSubjectID;
									$count++;
								}
							}
						}
					}
				}
				else {
					$ClassUpdateInfo[$count]['SubjectID'] = $SubjectID;
					$count++;
				}
			}
		}
	}
	else {	// Case triggered by Subject having Component Subjects
		$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);

		for($i=0 ; $i<count($CmpSubjectArr) ; $i++){
			list($CmpSubjectID, $CmpSubjectName) = $CmpSubjectArr[$i];
			
			$ClassUpdateInfo[$count]['ClassID'] = $ClassID;
			$ClassUpdateInfo[$count]['SubjectID'] = $CmpSubjectID;
			$count++;
		}
	}
}
else {
	$ClassUpdateInfo[0]['ClassID'] = $ClassID;
	$ClassUpdateInfo[0]['SubjectID'] = $SubjectID;
}

//var_dump($ClassUpdateInfo);
// Set Update Value
if($ActionCode == "COMPLETE"){
	$IsCompleted = 1;
}
else if($ActionCode == "CANCEL"){
	$IsCompleted = 0;
}


# Sql
if(count($ClassUpdateInfo) > 0){
	for($i=0 ; $i<count($ClassUpdateInfo) ;$i++){
		// Get any existing record in Marksheet Submission Progress
		$SubProgressArr = $lreportcard->GET_MARKSHEET_SUBMISSION_PROGRESS($ClassUpdateInfo[$i]['SubjectID'], $ReportID, $ClassUpdateInfo[$i]['ClassID']);
		
		if(count($SubProgressArr) > 0){	// having existing records
			$result['update_complete_'.$i] = $lreportcard->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($ClassUpdateInfo[$i]['SubjectID'], $ReportID, $ClassUpdateInfo[$i]['ClassID'], $IsCompleted);
			$ClassUpdateInfo[$i]['isUpdate'] = ($result['update_complete_'.$i]) ? 1 : 0;
		}
		else {	// do not have existing records (first time to update marksheet submission progress of this term subject of a class
			$result['insert_complete_'.$i] = $lreportcard->INSERT_MARKSHEET_SUBMISSION_PROGRESS($ClassUpdateInfo[$i]['SubjectID'], $ReportID, $ClassUpdateInfo[$i]['ClassID'], $IsCompleted);
			$ClassUpdateInfo[$i]['isUpdate'] = ($result['insert_complete_'.$i]) ? 1 : 0;
		}
		
	}
			
}



$rs = '';

#################################################################################################

/*
*       XML content
*/
header('Content-Type: text/xml');
header("Cache-Control: no-cache, must-revalidate");
$rs .= '<?xml version="1.0" encoding="UTF-8" ?>';
$rs .= "<response>";
$rs .= "<update>";
$rs .= "<ActionCode>".$ActionCode."</ActionCode>";
$rs .= "<ReportID>".$ReportID."</ReportID>";
$rs .= "<ClassLevelID>".$ClassLevelID."</ClassLevelID>";
$rs .= "<isAll>".$isAll."</isAll>";

for($i=0 ; $i<count($ClassUpdateInfo) ; $i++){
	$rs .= "<ClassUpdateInfo>";
	$rs .= "<isUpdate>".$ClassUpdateInfo[$i]['isUpdate']."</isUpdate>";
	$rs .= "<SubjectID>".$ClassUpdateInfo[$i]['SubjectID']."</SubjectID>";
	$rs .= "<ClassID>".$ClassUpdateInfo[$i]['ClassID']."</ClassID>";
	$rs .= "</ClassUpdateInfo>";
}

$rs .= "</update>";
$rs .= "</response>";

echo $rs;
intranet_closedb();	


?>