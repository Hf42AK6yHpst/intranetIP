<?php
/*
 * Change Log:
 * 	Date:	2016-01-05 Villa	modified for submark sheet layer
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

#################################################################################################

include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
$lreportcard = new libreportcard2008j();
//$lreportcard = new libreportcard();

$limport = new libimporttext();

# Preparation
//$params = "ClassLevelID=$ClassLevelID&SubjectID=$SubjectID&ReportID=$ReportID&ClassID=$ClassID&ReportColumnID=$ReportColumnID&jsReportColumn=$jsReportColumn&SubjectGroupID=$SubjectGroupID";
$params = "SubjectID=$SubjectID&ClassID=$ClassID&SchoolCode=$SchoolCode&ReportColumnID=$ReportColumnID&ReportID=$ReportID&jsReportColumn=$jsReportColumn&SubjectGroupID=$SubjectGroupID&SubReportColumnID=$SubReportColumnID&currentLevel=$currentLevel";
$params .= (isset($ParentSubjectID) && $ParentSubjectID != "") ? "&ParentSubjectID=$ParentSubjectID" : "";

# Initialization 
$result = array();
$FormatWrong = 0;
$MissMatchStudentArr = array();
$DateName = "LastMarksheetInput";

# Get Data
// uploaded file information
$filepath = $userfile;
$filename = $userfile_name;

// Last Modified User
$LastModifiedUserID = $_SESSION['UserID'];

// Class
$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
for($i=0 ; $i<count($ClassArr) ;$i++){
	if($ClassArr[$i][0] == $ClassID)
		$ClassName = $ClassArr[$i][1];
}

# Header including [WebSAMSRegNumber] [ClassName] [ClassNumber] [ReportColumn1] ... [ReportColumn(j-1)]
# Verification by Matching the WebSAMSRegNumber of students
# Main
if($filepath=="none" || $filepath == "" || !is_uploaded_file($filepath) || !$limport->CHECK_FILE_EXT($filename)){
	// Check Valid File is uploaded
	header("Location: sub_marksheet_import.php?$params");
}
else {
	$lo = new libfilesystem();
	
	$data = $limport->GET_IMPORT_TXT($filepath);
	$HeaderRow = array_shift($data);		# drop the title bar
	
	// Subject Grading Scheme
	$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0,0,$ReportID);
	
	// Student Info Array
	/*
	$StudentArr = $lreportcard->GET_STUDENT_BY_CLASS($ClassID);
	$StudentSize = sizeof($StudentArr);
	*/
	if ($SubjectGroupID != '')
	{
		$StudentArr = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupID, $ClassLevelID, '', 1);
	}
	else
	{
		$StudentArr = $lreportcard->GET_STUDENT_BY_CLASS($ClassID);
	}
	$StudentSize = sizeof($StudentArr);
	
	$StudentWebSAMSRegNoArr = array();
	if(count($StudentArr) > 0){
		for($i=0 ; $i<sizeof($StudentArr) ; $i++)
			$StudentWebSAMSRegNoArr[] = $StudentArr[$i]['WebSAMSRegNo'];
	}
	
	# Get all Sub-MS columns of this report column of this subject
	$SubMSArr = $lreportcard->GET_ALL_SUB_MARKSHEET_COLUMN($ReportColumnID, $SubjectID, $currentLevel, $SubReportColumnID);
	$ColumnSize = sizeof($SubMSArr);
	
	# Generating Valid Header 
	$ColumnTitleArr = array("WebSAMSRegNumber", "ClassName", "ClassNumber", "StudentName");
	
	for($i=0; $i<$ColumnSize ; $i++){
		list($ColumnID, $ColumnTitle, $Ratio,$Calc,$Trans, $FullMark) = $SubMSArr[$i];
		$FullMarkArr[] = $FullMark;
		//$ColumnTitleArr[] = $ColumnTitle;
		$ColumnTitleArr[] = $ColumnTitle.(!empty($FullMark)?"($FullMark)":"");
// 		$WeightingArr[$ColumnID] = $Ratio;
	}
	
	# Verification of Header of imported Csv file
	for($j=0; $j<sizeof($ColumnTitleArr); $j++){
		if($HeaderRow[$j] != $ColumnTitleArr[$j]){
			$FormatWrong = 1;
			break;
		}
	}
	
	if ($FormatWrong == 1)
	{
		header("Location:sub_marksheet_import.php?$params&Result=update_failed");
	}
	else
	{
		$successUpdatedCount = 0;
				
		for($i=0; $i<sizeof($data) ; $i++){	
			$data_obj = $data[$i];
			$websamsregno = trim(array_shift($data_obj));	//	WebSAMSRegNumber from csv file
			$cname = trim(array_shift($data_obj));			// class name from csv file
			$cnum = trim(array_shift($data_obj));			// class number from csv file
			$studentname = trim(array_shift($data_obj));	// student name from csv file
			
			// check by WebSAMSRegNumber
			if(!in_array($websamsregno, $StudentWebSAMSRegNoArr)){
				//$MissMatchStudentArr[]['WebSAMSRegNo'] = $websamsregno;
				continue;
			}
			
			// check by classname
			/*
			if($cname != $ClassName){
				//$MissMatchStudentArr[]['ClassName'] = $cname;
				continue;
			}
			*/
			
			# get student's UserID
			$sql = '';
			$row = array();
			//$sql = "SELECT UserID, ClassName, ClassNumber FROM INTRANET_USER WHERE ClassName = '$cname' AND (ClassNumber = '$cnum' OR CONCAT('0',ClassNumber) = '$cnum' or ClassNumber = '0".$cnum."')";
			//$row = $lreportcard->returnArray($sql, 3);
			$row = $lreportcard->Get_Student_Info_By_ClassName_ClassNumber($cname, $cnum);
			list($StudentID, $ClassName, $ClassNumber) = $row[0];
			
			if(!empty($StudentID) && !empty($data_obj)){
				
				for($j=0; $j<$ColumnSize ; $j++){
					list($ColumnID, $ColumnTitle, $Ratio) = $SubMSArr[$j];
					$thisFullMark = $FullMarkArr[$j];
					$thisMark = $data_obj[$j];
					
					if(trim($thisMark)=='') continue;
					// Add full mark checking - $eRCTemplateSetting['SubMS']['SubMSOverallCalculation'] [2014-1121-1759-15164]
					if(($ReportCardCustomSchoolName=="munsang_college" || $eRCTemplateSetting['SubMS']['SubMSOverallCalculation']) && $thisMark > $thisFullMark) 
					{
						$result[$i."_".$j] = false;
						continue;
					}
					
					if (!is_numeric($thisMark))
						$thisMark = 0 - array_search($thisMark, $lreportcard->specialCasesSet1);
										
					# info for import
					$importInfo["ColumnID"] = $ColumnID;
					$importInfo["StudentID"] = $StudentID;
					$importInfo["MarkRaw"] = $thisMark;
					$importInfo["TeacherID"] = $UserID;
					$result[$i."_".$j] = $lreportcard->INSERT_SUB_MARKSHEET_SCORE($importInfo);
					if ($result[$i."_".$j]) $successUpdatedCount++;
					
// 					$StudentIDArr[] = $StudentID;
// 					$subMSMark = $WeightingArr[$ColumnID] * $thisMark;
// 					$overAllData[$StudentID] += $subMSMark;
				}
			}
		}
	}
}

// ### Insert the UpperLayer Record END ###
// foreach ($StudentIDArr as $_StudentID){
// 	$importInfo["ColumnID"] = $SubReportColumnID;
// 	$importInfo["StudentID"] = $_StudentID;
// 	$importInfo["MarkRaw"] = $overAllData[$_StudentID] ;
// 	$importInfo["TeacherID"] = $UserID;
// 	$lreportcard->INSERT_SUB_MARKSHEET_SCORE($importInfo);
// }
// ### Insert the UpperLayer Record END ###

if(!in_array(false, $result)) {
	//$successUpdatedCount--;
	$msg = "update";
	//$params .= "&SucessCount=$successUpdatedCount";
} else { 
	$msg = "update_failed";
}

intranet_closedb();

header("Location:sub_marksheet.php?$params&Result=$msg");
?>