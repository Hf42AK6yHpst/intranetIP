<?php
// Using : ivan

/***********************************************
 * 	20170519 Bill:
 * 		- allow View Group to access (view only)
 * 	20120109 Ivan:
 * 		- added $eRCTemplateSetting['SubjectTeacherComment']['PrintPage'] for print mode logic
 * 	20100809 Marcus:
 * 		- add flag $eRCTemplateSetting['UnlimitSubjectTeacherComment'] to unlimit teacher comment length 
 * 	20100112 Marcus:
 * 		- change url of select comment popup window to class_teacher_comment/choose_comment.php
 * *********************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$isPrintMode = (isset($_POST['isPrintMode']))? $_POST['isPrintMode'] : $_GET['isPrintMode'];

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	$CurrentPage = "Management_MarkSheetRevision";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    if ($lreportcard->hasAccessRight()) {
        $linterface = new interface_html();
        
		############################################################################################################
		
		if (!isset($ClassLevelID, $SubjectID, $ReportID)) {
			header("Location: marksheet_revision.php");
			exit();
		}
		
		// View Group - View Only
		if($ck_ReportCard_UserType == "VIEW_GROUP")
			$ViewOnly = true;
		
		//$TextareaMaxLength = ($lreportcard->textAreaMaxChar_SubjectTeacherComment=='')? $lreportcard->textAreaMaxChar : $lreportcard->textAreaMaxChar_SubjectTeacherComment;
		$TextareaMaxLength = $lreportcard->Get_Subject_Teacher_Comment_MaxLength($ReportID, $SubjectID);

		// Class
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		if (sizeof($ClassArr) > 0) {
			for($i=0 ; $i<count($ClassArr) ;$i++) {
				if($ClassArr[$i][0] == $ClassID)
					$ClassName = $ClassArr[$i][1];
			}
			// Subject
			$SubjectName = $lreportcard->GET_SUBJECT_NAME($SubjectID);
			
			// Period (Term or Whole Year)
			$ReportInfo = $lreportcard->GET_REPORT_TYPES($ClassLevelID, $ReportID);
			$PeriodName = $ReportInfo['SemesterTitle'];
			
			// Student Info Array
			if ($SubjectGroupID != '')
			{
				$StudentArray = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupID, $ClassLevelID, '', 1);
				$showClassName = true;
				
				$obj_SubjectGroup = new subject_term_class($SubjectGroupID);
				$SubjectGroupName = $obj_SubjectGroup->Get_Class_Title();
				//$TitleDisplay = "<td>".$Lang['SysMgr']['SubjectClassMapping']['SubjectGroup']." : <strong>".$SubjectGroupName."</strong></td>";
				
				$SubjectOrGroupTitle = $Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'];
				$SubjectOrGroupNameDisplay = $SubjectGroupName;
			}
			else
			{
				//2014-0224-1422-33184
				$StudentArray = $lreportcard->Get_Marksheet_Accessible_Student_List_By_Subject_And_Class($ReportID, $_SESSION['UserID'], $ClassID, $ParentSubjectID, $SubjectID);
			
				//$StudentArray = $lreportcard->GET_STUDENT_BY_CLASS($ClassID, '', 1);
				$showClassName = false;
				//$TitleDisplay = "<td>".$eReportCard['Class']." : <strong>".$ClassName."</strong></td>";
				$SubjectOrGroupTitle = $eReportCard['Class'];
				$SubjectOrGroupNameDisplay = $ClassName;
			}
			$StudentSize = sizeof($StudentArray);
			
			// Student ID list of students in a class
			for($i=0; $i<$StudentSize; $i++)
				$StudentIDArray[] = $StudentArray[$i][0];
			if(!empty($StudentIDArray))
				$StudentIDList = implode(",", $StudentIDArray);
			
			$teacherComment = $lreportcard->GET_TEACHER_COMMENT($StudentIDArray, $SubjectID, $ReportID);
		}
		
		### Get Subject Grading Scheme Info
		$GradingSchemeInfoArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, '', $withGrandResult=0, $returnAsso=1, $ReportID);
		$ScaleInput = $GradingSchemeInfoArr[$SubjectID]['ScaleInput'];
		
		if ($eRCTemplateSetting['Management']['SubjectTeacherComment']['ShowGeneratedMarkGrade']) {
			### student's Mark & Grade Array
			$MarksArr = $lreportcard->getMarks($ReportID);
		}
		
		if ($eRCTemplateSetting['Management']['SubjectTeacherComment']['ShowMarksheetMark']) {
			// Get SchemeRangeID and Grade Mapping
			$SchemeRangeIDGradeMappingArr = $lreportcard->GET_GRADE_FROM_GRADING_SCHEME_RANGE();
			
			// Get Report Column Info
			$ReportColumnInfoArr = $lreportcard->returnReportTemplateColumnData($ReportID);
			$numOfReportColumn = count($ReportColumnInfoArr);
			
			if ($ScaleInput == 'G') {
				// Add Overall Column for input grade Subject
				$ReportColumnInfoArr[$numOfReportColumn]['ReportColumnID'] = 0;
				$ReportColumnInfoArr[$numOfReportColumn]['ColumnTitle'] = $eReportCard['Overall'];
				$numOfReportColumn = count($ReportColumnInfoArr);
			}
			
			$MarksheetScoreInfoArr = array();
			for ($i=0; $i<$numOfReportColumn; $i++) {
				$thisReportColumnID = $ReportColumnInfoArr[$i]['ReportColumnID'];
				
				if ($thisReportColumnID == 0) {
					$MarksheetScoreInfoArr[$thisReportColumnID] = $lreportcard->GET_MARKSHEET_OVERALL_SCORE($StudentIDArray, $SubjectID, $ReportID, $SkipSubjectGroupCheckingForGetMarkSheet=1);
				}
				else {
					$MarksheetScoreInfoArr[$thisReportColumnID] = $lreportcard->GET_MARKSHEET_SCORE($StudentIDArray, $SubjectID, $thisReportColumnID, $ConvertToNaIfZeroWeight=0, $ColumnWeight='', $SkipSubjectGroupCheckingForGetMarkSheet=1);
				}
			}
		}
		
		
		
		$display = '<table class="yui-skin-sam" width="100%" cellpadding="5" cellspacing="0">';
		$display .= '<tr>
	          <td width="5%" align="center" class="tablegreentop tabletopnolink">'.$eReportCard['StudentNo_short'].'</td>
	          <td width="25%" class="tablegreentop tabletopnolink">'.$eReportCard['Student'].'</td>';
	    if($eRCTemplateSetting['Management']['SubjectTeacherComment']['ShowGeneratedMarkGrade'] )
		{
			//student's Mark & Grade Title			
			$display .= '<td width="5%" class="tablegreentop tabletopnolink">'.$eReportCard['Mark'].'</td>';
			$display .= '<td width="5%" class="tablegreentop tabletopnolink">'.$eReportCard['Grade'].'</td>';
		}
		if ($eRCTemplateSetting['Management']['SubjectTeacherComment']['ShowMarksheetMark']) {
			for ($i=0; $i<$numOfReportColumn; $i++) {
				$thisColumnTitle = $ReportColumnInfoArr[$i]['ColumnTitle'];
				$display .= '<td width="5%" class="tablegreentop tabletopnolink">'.$thisColumnTitle.'</td>';
			}
		}
		$display .= '<td class="tablegreentop tabletopnolink">'.$eReportCard['CommentContent'].'</td>';
	    $display .= '</tr>';
		
	    # Student Content of Marksheet Revision
		if (isset($StudentSize) && $StudentSize > 0) {
			for($j=0; $j<$StudentSize; $j++){
				list($StudentID, $WebSAMSRegNo, $StudentNo, $StudentName, $ClassName) = $StudentArray[$j];
				
				### show mark and grade
				if($eRCTemplateSetting['Management']['SubjectTeacherComment']['ShowGeneratedMarkGrade'] )
				{		
					$ReportColumnID=0;
					$thisMark = $MarksArr[$StudentID][$SubjectID][$ReportColumnID]['Mark'];
					$thisGrade = $MarksArr[$StudentID][$SubjectID][$ReportColumnID]['Grade'];				
														
					$thisScaleInput = $GradingSchemeInfoArr[$SubjectID]['ScaleInput'];
					$thisScaleDisplay = $GradingSchemeInfoArr[$SubjectID]['ScaleDisplay'];
					
					list($thisMark, $needStyle) = $lreportcard->checkSpCase($ReportID, $SubjectID, $thisMark, $thisGrade);
					
					if($needStyle) {
						$thisNatureDetermineScore = ($thisScaleDisplay=='G')? $thisGrade : $thisMark;
						$thisMarkDisplay = $lreportcard->Get_Score_Display_HTML($thisMark, $ReportID, $ClassLevelID, $SubjectID, $thisNatureDetermineScore);
						$thisGradeDisplay = $lreportcard->Get_Score_Display_HTML($thisGrade, $ReportID, $ClassLevelID, $SubjectID, $thisNatureDetermineScore);
					}
										
					$thisMark_html = '<td class="tabletext" valign="top">'.$thisMarkDisplay.'</td>';
					$thisGrade_html = '<td class="tabletext" valign="top">'.$thisGradeDisplay.'</td>';
				}
				
				if ($eRCTemplateSetting['Management']['SubjectTeacherComment']['ShowMarksheetMark']) {
					$thisColumnMark_html = '';
					for ($i=0; $i<$numOfReportColumn; $i++) {
						$thisReportColumnID = $ReportColumnInfoArr[$i]['ReportColumnID'];
						
						$thisMarkType = $MarksheetScoreInfoArr[$thisReportColumnID][$StudentID]['MarkType'];
						$thisMarkRaw = $MarksheetScoreInfoArr[$thisReportColumnID][$StudentID]['MarkRaw'];
						$thisMarkNonNum = $MarksheetScoreInfoArr[$thisReportColumnID][$StudentID]['MarkNonNum'];
						$thisIsEstimated = $MarksheetScoreInfoArr[$thisReportColumnID][$StudentID]['IsEstimated'];
						
						$thisDisplay = '';
						if ($lreportcard->Check_If_Grade_Is_SpecialCase($thisMarkNonNum)) {
							$thisDisplay = $thisMarkNonNum;
						}
						else {
							if ($thisMarkType == 'M' && $thisMarkRaw != '-1') {
								$thisDisplay = $thisMarkRaw;
							}
							else if ($thisMarkType == 'G') {
								$thisDisplay = $SchemeRangeIDGradeMappingArr[$thisMarkNonNum];
							}
						}
						
						if ($thisIsEstimated) {
							$thisDisplay = $lreportcard->Get_Estimated_Score_Display($thisDisplay);
						}
						
						$thisDisplay = ($thisDisplay=='')? $Lang['General']['EmptySymbol'] : $thisDisplay;
						$thisColumnMark_html .= '<td class="tabletext" valign="top">'.$thisDisplay.'</td>';
					}
				}
				

				$tr_css = ($j % 2 == 1) ? "tablegreenrow2" : "tablegreenrow1";
				
				if (isset($teacherComment[$StudentID])) {
					$textareaName = "commentID[".$teacherComment[$StudentID]["CommentID"]."]";
					$textareaID = "commentID".$teacherComment[$StudentID]["CommentID"];
					$textareaValue = stripslashes($teacherComment[$StudentID]["Comment"]);
				} else {
					$textareaName = "comment[$StudentID]";
					$textareaID = "comment".$StudentID;
					$textareaValue = "";
				}
				
				if ($showClassName)
					$thisClassDisplay = $ClassName.'-'.$StudentNo;
				else
					$thisClassDisplay = $StudentNo;
					
				$display .= '<tr class="'.$tr_css.'">
			          		 <td align="center" valign="top" class="tabletext">'.$thisClassDisplay.'</td>
		          	  		 <td class="tabletext" valign="top">'.$StudentName.'</td>' ;
		        $display .= $thisMark_html;
		        $display .= $thisGrade_html;
		        $display .= $thisColumnMark_html;
				$display .=  '<td class="tabletext" valign="top">';
				
				if (!$isPrintMode && !$ViewOnly) {
					$display .= '<div style="z-index:'.(9000-$j).'" id="myAutoComplete-'.$StudentID.'">';
					$display .= $button_find;
					$display .= ' <input class="textboxnum" type="text" name="code-'.$StudentID.'" id="code-'.$StudentID.'" value="" />';
					$display .= " $i_general_or ";
					$display .= $linterface->GET_SMALL_BTN($button_select, "button", "newWindow('../class_teacher_comment/choose_comment.php?SubjectID=$SubjectID&fieldname=$textareaName', 9)");
					$display .= '<div id="myContainer-'.$StudentID.'"></div></div>';
					$display .= '<br />';
					
					$script .= 'var myAutoComp'.$StudentID.' = new YAHOO.widget.AutoComplete("code-'.$StudentID.'", "myContainer-'.$StudentID.'", myDataSource);';
					$script .= 'myAutoComp'.$StudentID.'.animVert = false;';
					
					$script .= 'myAutoComp'.$StudentID.'.formatResult = function(oResultItem, sQuery) {';
					$script .= 'var sMarkup = oResultItem[0];';
					$script .= 'return (sMarkup);';
					$script .= '};';
					
					$script .= 'myAutoComp'.$StudentID.'.itemSelectEvent.subscribe(function(e, args) {';
					$script .= 'if (document.getElementById("'.$textareaID.'").value != "") document.getElementById("'.$textareaID.'").value += "";';
					$script .= 'var contentToFill = args[2][1];';
					$script .= 'if (Trim(document.getElementById("'.$textareaID.'").value) != \'\')
									document.getElementById("'.$textareaID.'").value += "\n";';
					$script .= 'document.getElementById("'.$textareaID.'").value += contentToFill.replace(/<br>/gi, "\n");';
					#$script .= 'document.getElementById("'.$textareaID.'").value += args[2][1];';
					$script .= 'document.getElementById("code-'.$StudentID.'").value = "";';
					if ($TextareaMaxLength != -1) {
						$script .= "limitText(document.getElementById(\"$textareaID\"),'".$TextareaMaxLength."');";
					}
					$script .= '});';
				}
				 
				if ($TextareaMaxLength != -1) {
					$limitTextAreaEvent = " onKeyDown='limitText(document.getElementById(\"$textareaID\"),".$TextareaMaxLength.", 0, 1, \"alertCommentMaximum()\");return noEsc();' ";
					$limitTextAreaEvent .= " onKeyUp='limitText(document.getElementById(\"$textareaID\"),".$TextareaMaxLength.", 0, 1, \"alertCommentMaximum()\");' ";
					$limitTextAreaEvent .= " onpaste='setTimeout(function() { limitText(document.getElementById(\"$textareaID\"), $TextareaMaxLength, 0, 1, \"alertCommentMaximum()\"); }, 10);' ";
					$limitTextAreaEvent .= " ondrop='setTimeout(function() { limitText(document.getElementById(\"$textareaID\"), $TextareaMaxLength, 0, 1, \"alertCommentMaximum()\"); }, 10);' ";
				}
				
				$isDisabled = "";
		        if($ViewOnly && !$lreportcard->IS_ADMIN_USER($_SESSION['UserID'])) {
		        	$isDisabled = " disabled  ";
		        }
				
				$display .= "<textarea class=\"textboxtext\" id=\"$textareaID\" name=\"$textareaName\" cols=\"70\" rows=\"2\" wrap=\"virtual\" onFocus=\"this.rows=5;\" $limitTextAreaEvent $isDisabled >$textareaValue</textarea>";
				$display .= '</td></tr>';
			} 	
		    $display .= '</table>';
			$display .= '<script type="text/javascript">';
			$display .= $script;
			$display .= '</script>';
			
			$redirectParm = "ReportID=$ReportID&ClassLevelID=$ClassLevelID";
			if ($redirectTo == "submission")
				$redirectLocation = "../progress/submission.php?$redirectParm";
			elseif ($redirectTo == "submissionSG")
				$redirectLocation = "../progress/submission_sg.php?$redirectParm";
				
			else
			{
				$obj_Subject = new subject($SubjectID);
				if ($obj_Subject->Is_Component_Subject())
				{
					$obj_SubjectComponent = new Subject_Component($SubjectID);
					$ParentSubjectInfoArr = $obj_SubjectComponent->Get_Parent_Subject();
					$ParentSubjectID = $ParentSubjectInfoArr['RecordID'];
					
					if ($SubjectGroupID != '')
						$redirectLocation = "marksheet_revision2_subject_group.php?$redirectParm"."&SubjectID=$ParentSubjectID&SubjectGroupID=$SubjectGroupID";
					else
						$redirectLocation = "marksheet_revision2.php?$redirectParm"."&SubjectID=$ParentSubjectID";
				}
				else
				{
					if ($SubjectGroupID != '')
						$redirectLocation = "marksheet_revision_subject_group.php?$redirectParm"."&SubjectID=$SubjectID&SubjectGroupID=$SubjectGroupID";
					else
						$redirectLocation = "marksheet_revision.php?$redirectParm"."&SubjectID=$SubjectID";
				}
			}
			
			if($lreportcard->IS_ADMIN_USER($_SESSION['UserID']) || !$ViewOnly) {
				$display_button .= $linterface->GET_ACTION_BTN($button_save, "button", "document.form1.submit()")."&nbsp";
		        $display_button .= $linterface->GET_ACTION_BTN($button_reset, "reset", "")."&nbsp";
			}
			$display_button .= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='$redirectLocation'");
		}
		else {
			$td_colspan = "3";
			$display .=	'<tr class="tablegreenrow1" height="40">
							<td class="tabletext" colspan="'.$td_colspan.'" align="center">'.$i_no_record_exists_msg.'</td>
						</tr>';
		}
		
		if ($Result == "num_records_updated") {
			$msg = "$SuccessCount $i_con_msg_num_records_updated";
			if ($cut_row) {
				$msg .= sprintf(' '.$eReportCard['CommentTooLongInFile'], $cut_row);
				
				$cutContentWarning = '';
				$cutContentWarning .= '<tr><td colspan="2">'.$linterface->GET_WARNING_TABLE($err_msg='', $DeleteItemName='', $DeleteButtonName='', $msg).'</td></tr>';
			}
			else {
				$color= 'green';
				$SysMsg = $linterface->GET_SYS_MSG("", "<font color='$color'>$msg</font>");
			}
			
		} else {
			$SysMsg = $linterface->GET_SYS_MSG($Result);
		}
		############################################################################################################
        
        $PAGE_NAVIGATION[] = array($eReportCard['Management_MarksheetRevision'], "javascript:window.location='$redirectLocation'");
		$PAGE_NAVIGATION[] = array($eReportCard['Subject']." ".$eReportCard['TeachersComment']);
		
		# tag information
		$TAGS_OBJ[] = array($eReportCard['Management_MarksheetRevision'], "", 0);
		$linterface->LAYOUT_START();
		
		$parameters = "ClassID=$ClassID&SubjectID=$SubjectID&ReportID=$ReportID&ClassLevelID=$ClassLevelID&SubjectGroupID=$SubjectGroupID&RedirectLocation=$RedirectLocation";
		
		if ($eRCTemplateSetting['SubjectTeacherComment']['PrintPage']) {
			$toolbarPrint = '<a href="javascript:void(0);" class="contenttool" onclick="js_Go_Print();"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_print.gif" width="18" height="18" border="0" align="absmiddle"> '.$Lang['Btn']['Print'].'</a>';
		}
?>

<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/yui/autocomplete_reportcard2008/autocomplete.css">
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/yui/autocomplete_reportcard2008/yahoo-dom-event.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/yui/autocomplete_reportcard2008/get-min.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/yui/autocomplete_reportcard2008/connection-min.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/yui/autocomplete_reportcard2008/animation-min.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/yui/autocomplete_reportcard2008/json-min.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/yui/autocomplete_reportcard2008/autocomplete-min.js"></script>

<script type="text/javascript">
var myArray = [<?= $lreportcard->GET_COMMENT_BANK_SUGGEST($SubjectID) ?>];
var myDataSource = new YAHOO.widget.DS_JSArray(myArray);
myDataSource.queryMatchContains = true;

function checkOptionAdd(parObj, addText) {
	//if (parObj.value != "")
	//	parObj.value += " ";
	
	if (Trim(parObj.value) != '') {
    	parObj.value += "\n";
    }
	parObj.value += addText;
	<? if ($TextareaMaxLength != -1) { ?>
		limitText(parObj,'<?=$TextareaMaxLength?>')
	<? } ?>
	parObj.focus();
}

function js_Go_Print() {
	var jsOriginalAction = $('form#form1').attr('action');
	$('form#form1').attr('action', '../../print_page.php').attr('target', '_blank').submit();
	
	// restore form attribute
	$('form#form1').attr('action', jsOriginalAction).attr('target', '_self');
}

function alertCommentMaximum() {
	alert('<?=$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CommentTooLong']?>');
}
</script>
<br/>
<form id="form1" name="form1" method="post" action="teacher_comment_update.php" onSubmit="return false">
<div class="content_top_tool">
	<table width="100%" border="0" cellspacing="5" cellpadding="0">
		<tr>
			<td>
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
					<? if($lreportcard->IS_ADMIN_USER($_SESSION['UserID']) || !$ViewOnly) { ?>
						<td>
							<a href="import_comment.php?<?=$parameters?>" class="contenttool">
								<img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/icon_import.gif" width="18" height="18" border="0" align="absmiddle"> <?=$button_import?>
							</a>
						</td>
					<? } ?>
						<td><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif"></td>
						<td><a href="export_comment.php?<?=$parameters?>" class="contenttool"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/icon_export.gif" width="18" height="18" border="0" align="absmiddle"> <?=$button_export?></a></td>
						<td><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif"></td>
						<td><?=$toolbarPrint?></td>
					</tr>
				</table>
			</td>
			<td align="right"><?= $SysMsg ?></td>
		</tr>
		<?=$cutContentWarning?>
	</table>
</div>
<br style="clear:both;" />

<?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?>
<br style="clear:both;" />
<br style="clear:both;" />

<!--PrintStart-->
<table class="form_table_v30">
	<tr>
		<td class="field_title"><?=$eReportCard['Settings_ReportCardTemplates']?></td>
		<td><?=$PeriodName?></td>
	</tr>
	<tr>
		<td class="field_title"><?=$SubjectOrGroupTitle?></td>
		<td><?=$SubjectOrGroupNameDisplay?></td>
	</tr>
	<tr>
		<td class="field_title"><?=$eReportCard['Subject']?></td>
		<td><?=$SubjectName?></td>
	</tr>
</table>
<br style="clear:both;" />

<?=$display?>
<!--PrintEnd-->

<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td height="1" class="dotline"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td align="center" valign="bottom"><?=$display_button?></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="ClassLevelID" id="ClassLevelID" value="<?=$ClassLevelID?>"/>
<input type="hidden" name="ReportID" id="ReportID" value="<?=$ReportID?>"/>
<input type="hidden" name="SubjectID" id="SubjectID" value="<?=$SubjectID?>"/>
<input type="hidden" name="ClassID" id="ClassID" value="<?=$ClassID?>"/>
<input type="hidden" name="Semester" id="Semester" value="<?=$ReportInfo['Semester']?>"/>
<input type="hidden" name="RedirectLocation" id="RedirectLocation" value="<?=intranet_htmlspecialchars($redirectLocation)?>"/>

</form>
<?
		intranet_closedb();
        $linterface->LAYOUT_STOP();
    } else {
    ?>
You have no priviledge to access this page.
    <?
    }
} else {
?>
You have no priviledge to access this page.
<?
}
?>