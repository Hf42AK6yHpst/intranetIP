<?php
/*
* Change Log:
* Date:	2017-01-05 Villa	change param for submark sheet layer
*/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");

intranet_auth();
intranet_opendb();

#################################################################################################

include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
$lreportcard = new libreportcard2008j();
//$lreportcard = new libreportcard();

$lexport = new libexporttext();

$ReportSetting = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$ClassLevelID = $ReportSetting['ClassLevelID'];

# Get Data
// Class
$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
for($i=0 ; $i<count($ClassArr) ;$i++){
	if($ClassArr[$i][0] == $ClassID)
		$ClassName = $ClassArr[$i][1];
}

// Period (Term x, Whole Year, etc)
$PeriodArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID, $ReportID);
$PeriodName = (count($PeriodArr) > 0) ? $PeriodArr['SemesterTitle'] : '';

// SubjectName
$SubjectName = $lreportcard->GET_SUBJECT_NAME($SubjectID);
$SubjectName = str_replace("&nbsp;", "_", $SubjectName);

// Class
$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
for($i=0 ; $i<count($ClassArr) ;$i++){
	if($ClassArr[$i][0] == $ClassID)
		$ClassName = $ClassArr[$i][1];
}

// Assessment Name
$AssessmentInfoArr = $lreportcard->returnReportTemplateColumnData($ReportID);
$AssessmentName = $AssessmentInfoArr[0]['ColumnTitle'];

# Get all Sub-MS columns of this report column of this subject
// $SubMSArr = $lreportcard->GET_ALL_SUB_MARKSHEET_COLUMN($ReportColumnID, $SubjectID);
$SubMSArr = $lreportcard->GET_ALL_SUB_MARKSHEET_COLUMN($ReportColumnID, $SubjectID, $currentLevel, $SubReportColumnID);
$ColumnSize = sizeof($SubMSArr);

# Student Info Array
if ($SubjectGroupID != '')
{
	$StudentArr = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupID, $ClassLevelID, '', 1);
	//$showClassName = true;
	$showClassName = false;
	
	$obj_SubjectGroup = new subject_term_class($SubjectGroupID);
	$SubjectGroupName = $obj_SubjectGroup->Get_Class_Title();
	
	//$Class_SG_Title = $Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'];
	$Class_SG_Title = "ClassName";
	$Class_SG_Name = $SubjectGroupName;
}
else
{
	$StudentArr = $lreportcard->GET_STUDENT_BY_CLASS($ClassID, '', 1);
	$showClassName = false;
	
	$Class_SG_Title = "ClassName";
	$Class_SG_Name = $ClassName;
}
$StudentSize = sizeof($StudentArr);

$StudentIDArr = array();
if(count($StudentArr) > 0){
	for($i=0 ; $i<sizeof($StudentArr) ; $i++)
		$StudentIDArr[$i] = $StudentArr[$i]['UserID'];
}

# Header 
$Content = "WebSAMSRegNumber,$Class_SG_Title,ClassNumber,StudentName";
$exportColumn[] = "WebSAMSRegNumber";
$exportColumn[] = $Class_SG_Title;
$exportColumn[] = "ClassNumber";
$exportColumn[] = "StudentName";

for($i=0; $i<$ColumnSize ; $i++){
	list($ColumnID, $ColumnTitle, $Ratio, $Calc, $Trans, $FullMark) = $SubMSArr[$i];
	
	//$ColumnTitle = ($ColumnTitle != "" && $ColumnTitle != null) ? convert2unicode($ColumnTitle, 1, 2) : $ColumnTitleAry[$ReportColumnID];
	$ColumnTitle = ($ColumnTitle != "" && $ColumnTitle != null) ? $ColumnTitle : $ColumnTitleAry[$ReportColumnID];
	$Content .= ",".$ColumnTitle;
	
	$exportColumn[] = $ColumnTitle.(!empty($FullMark)?"($FullMark)":"");
}

$Content .= "\n";

// content
$SubMarksheetResult = $lreportcard->GET_SUB_MARKSHEET_SCORE($ReportColumnID, $SubjectID);

for($j=0; $j<$StudentSize; $j++){
	list($StudentID, $WebSAMSRegNo, $StudentNo, $StudentName, $ClassName) = $StudentArr[$j];
	
	if ($showClassName)
		$thisClassDisplay = $ClassName.'-'.$StudentNo;
	else
		$thisClassDisplay = $StudentNo;
	
	$Content .= trim($WebSAMSRegNo).",".trim($ClassName).",".trim($thisClassDisplay).",".trim($StudentName);
	$row[] = trim($WebSAMSRegNo);
    $row[] = trim($ClassName);
	$row[] = trim($thisClassDisplay);
	$row[] = trim($StudentName);
	
	for($i=0; $i<$ColumnSize ; $i++){
		if ($forExport)
		{
			$thisDisplay = "";
		}
		else
		{
			list($ColumnID, $ColumnTitle, $Ratio) = $SubMSArr[$i];
			$thisMark = $SubMarksheetResult[$StudentID][$ColumnID];
			if ($thisMark >= 0)
			{
				$thisDisplay = $thisMark;
			}
			else
			{
				$thisDisplay = "";
			}
		}
		
		$row[] = trim($thisDisplay);
	}
	
	$rows[] = $row;
	unset($row);
    
}

// Output the file to user browser
$Prefix = "Sub_";
$set1 = array("&nbsp;", " ");
$set2 = array("-", ",");
$filename = $Prefix."Marksheet_".$SubjectName;
//$filename = $SubjectName.'_'.$Class_SG_Name.'_SubMarksheet';

$filename = str_replace($set2, "_", $filename);
$filename = str_replace($set1, "_", $filename);
$filename = iconv("UTF-8", "Big5", $filename);
$filename = $filename.".csv";

$export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn, "", "\r\n", "", 0, "11");
$lexport->EXPORT_FILE($filename, $export_content);
//output2browser($Content, $filename);

?>