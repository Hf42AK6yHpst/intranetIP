<?php
// Editing:
/*
 * Modification Log:
 * 20160616 Bill	[2016-0615-1638-35066] 
 * 	- trim() before header checking
 * 20160129 Bill
 * 	- Get correct weight when using Calculation Method 2
 * 20151117 Bill	[2015-1116-0916-31066]
 * 	- Use Userlogin instead of WebSAMSRegNo
 * 20151008	(Bill)	[2015-0520-1100-36066]
 * 	- Cust - Import Marksheet for All subjects
 * 	- Copy from marksheet_import_confirm.php
 */
 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

#################################################################################################

include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
$lreportcard = new libreportcard2008j();

$limport = new libimporttext();
$linterface = new interface_html();

# Preparation
$params = "ClassLevelID=$ClassLevelID&SubjectID=$SubjectID&ReportID=$ReportID&ClassID=$ClassID&SubjectGroupID=$SubjectGroupID&isProgress=$isProgress&isProgressSG=$isProgressSG";

# Initialization 
$result = array();
$MissMatchStudentArr = array();
$FormatWrong = 0;
$DateName = "LastMarksheetInput";

// Uploaded file information
$filepath = $userfile;
$filename = $userfile_name;

// Last Modified User
$LastModifiedUserID = $_SESSION['UserID'];

// Class
$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
for($i=0 ; $i<count($ClassArr) ;$i++){
	if($ClassArr[$i][0] == $ClassID)
		$ClassName = $ClassArr[$i][1];
}

// Check Valid File is uploaded
if($filepath=="none" || $filepath == "" || !is_uploaded_file($filepath) || !$limport->CHECK_FILE_EXT($filename)){
	header("Location: marksheet_import_all_subject.php?$params&Result=AddUnsuccess");
}
else {	
	//$lo = new libfilesystem();
	
	// Get CSV data
	$data = $limport->GET_IMPORT_TXT($filepath);
	$RecordNum = count($data) - 1;
	for($i=1; $i<count($data); $i++){
		$data[$i][] = $i;
	}
	
	// drop the title bar
	$HeaderRow = array_shift($data);
	
	// Build 3D data array
	$data = BuildMultiKeyAssoc((array)$data, array(0, 1, 2));
	
	# Loading Report Template Data
	$basic_data = $lreportcard->returnReportTemplateBasicInfo($ReportID);
	$ReportType = $basic_data['Semester'];

	# Load Settings - Calculation
	// Get the Calculation Method of Report Type - 1: Right-Down,  2: Down-Right
	$SettingCategory = 'Calculation';
	$categorySettings = $lreportcard->LOAD_SETTING($SettingCategory);
	$TermCalculationType = $categorySettings['OrderTerm'];
	$FullYearCalculationType = $categorySettings['OrderFullYear'];
	
	# Get Existing Report Calculation Method
	$CalculationType = ($ReportType == "F") ? $FullYearCalculationType : $TermCalculationType;
	
	# Get Column Data
	$column_data = $lreportcard->returnReportTemplateColumnData($ReportID);
	$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
	$ColumnSize = sizeof($column_data);
	
	// Subject Grading Scheme
	$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, "", 0, 0, $ReportID);
	$SubjectFormGradingArr = BuildMultiKeyAssoc((array)$SubjectFormGradingArr, array("SubjectID"));
	
	// Subject Code and ID Mapping
	$SubjectCodeIDMapping = $lreportcard->GET_SUBJECTS_CODEID_MAP(1, 1);
	$SubjectIDCodeMapping = array_flip($SubjectCodeIDMapping);
	
	// Subject Group Code and ID Mapping
	$scm = new subject_class_mapping();
	$SGCodeIDMapping = $scm->Get_Subject_Group_Info($lreportcard->GET_ACTIVE_YEAR_ID(), $ReportType);
	$SGCodeIDMapping = BuildMultiKeyAssoc((array)$SGCodeIDMapping, array("SubjectGroupCode"));
	$SGCodeAry = array_keys($SGCodeIDMapping);
	
	// Subject Group ID List
	$SubjectGroupIDAry = array();
	$SubjectCodeAry = array_keys((array)$data);
	foreach($SubjectCodeAry as $code){
		$SubjectGroupIDAry[] = $SGCodeIDMapping[$code]["SubjectGroupID"];
	}
	
	// Student Info Array
	$StudentArr = array();
	$StudentArr = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupIDAry, $ClassLevelID, '', 1);
	$StudentSize = sizeof($StudentArr);
	
	// Student Info Mapping
	$StudentUserLoginArr = array();
	if(count($StudentArr) > 0){
		//$StudentWebSAMSRegNoArr = BuildMultiKeyAssoc($StudentArr,"WebSAMSRegNo","UserID",1);
		$StudentClassInfoArr = BuildMultiKeyAssoc($StudentArr, array("ClassName","ClassNumber"),"UserID",1);
		$StudentUserLoginArr = BuildMultiKeyAssoc($StudentArr,"UserLogin","UserID",1);
		$StudentClassInfo = BuildMultiKeyAssoc($StudentArr,"UserID", array("ClassName","ClassNumber"));
	}
	
	# Get Report Column Weight
	$WeightAry = array();
	if($CalculationType == 2){
		if(count($column_data) > 0){
	    	for($i=0 ; $i<sizeof($column_data) ; $i++){
				//$OtherCondition = "SubjectID IS NULL AND ReportColumnID = ".$column_data[$i]['ReportColumnID'];
				$OtherCondition = " ReportColumnID = ".$column_data[$i]['ReportColumnID'];
				$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition);
				
				for($j=0; $j<count($weight_data); $j++){
					$weightDataContent = $weight_data[$j];
					$WeightAry[$weight_data[0]['ReportColumnID']][$weightDataContent['SubjectID']] = $weightDataContent['Weight'];
				}
			}
		}
	} else {
		$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID);  
		
		for($i=0 ; $i<sizeof($weight_data) ; $i++)
			$WeightAry[$weight_data[$i]['ReportColumnID']][$weight_data[$i]['SubjectID']] = $weight_data[$i]['Weight'];
	}
	
	# Generating Valid Header 
	$ColumnTitle = array();
	$ColumnTitle[] = $eReportCard['ImportHeader']['SubjectGroupCode']['En'];
	$ColumnTitle[] = $eReportCard['ImportHeader']['SubjectComponentCode']['En'];
	// [2015-1116-0916-31066] change to userlogin
	//$ColumnTitle[] = $eReportCard['ImportHeader']['WebSamsRegNo']['En'];
	$ColumnTitle[] = $eReportCard['ImportHeader']['UserLogin']['En'];

	// loop Report Column
	for($i=0; $i<$ColumnSize ; $i++){
		list($ReportColumnID, $ColumnTitleName, $DefaultWeight, $SemesterNum) = $column_data[$i];
		$BlockMarksheet = $column_data[$i]['BlockMarksheet'];
		
		$ColumnTitleName = ($ColumnTitleName != "" && $ColumnTitleName != null) ? $ColumnTitleName : $ColumnTitleAry[$ReportColumnID];
		$ColumnTitle[] = $ColumnTitleName;
	}
	
	# Verification of Header of Imported CSV file
	for($j=0; $j<sizeof($HeaderRow); $j++){
		// [2016-0615-1638-35066] trim() before header checking
		//if($HeaderRow[$j] != $ColumnTitle[$j]){
		if(trim($HeaderRow[$j]) != trim($ColumnTitle[$j])){
			$FormatWrong = 1;
			header("Location: marksheet_import_all_subject.php?Result=WrongCSVHeader&$params");
			exit;
		}
	}

	$rec_no = 0;
	foreach($data as $currentSubjectGroupCode => $sgAry)
	{
		$currentSubjectGroup = $SGCodeIDMapping[$currentSubjectGroupCode];
		$currentSubjectGroupID = $currentSubjectGroup["SubjectGroupID"];
		
		$parentSubjectID = $currentSubjectGroup["SubjectID"];
		$parentSubjectCode = $SubjectIDCodeMapping[$parentSubjectID];
		$isCmpSubject = $lreportcard->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($currentSubjectID);
			
		foreach($sgAry as $compSubjectCode => $studentAry)	
		{
			$currentSubjectID = $parentSubjectID;
			if($compSubjectCode!=""){
				$currentSubjectID = $SubjectCodeIDMapping[$parentSubjectCode.$compSubjectCode];
			}
			
			$columnValid = array();
			if($currentSubjectID){
				$isParentSubject = false;
				$isCalculateByCmpSub = false;
				
				// Scheme Range Info
				$SchemeGrade = array();
				$GradeInfo = array();
				$GradeOption = array();
				$PassFailInfo = array();
				$PassFailOption = array();
				$SchemeFullMark = '';
				
				$currentSubjectGrading = $SubjectFormGradingArr[$currentSubjectID];
				if(count($currentSubjectGrading) > 0){
					$SchemeID = $currentSubjectGrading['SchemeID'];
					$ScaleInput = $currentSubjectGrading['ScaleInput'];
					$ScaleDisplay = $currentSubjectGrading['ScaleDisplay'];
					
					$SchemeInfo = $lreportcard->GET_GRADING_SCHEME_INFO($SchemeID);
					$SchemeMainInfo = $SchemeInfo[0];
					$SchemeMarkInfo = $SchemeInfo[1];
					$SchemeType = $SchemeMainInfo['SchemeType'];
					$SchemeFullMark = $SchemeMainInfo['FullMark'];
					
					if($SchemeType == "H" && $ScaleInput == "G"){
						$SchemeGrade = $lreportcard->GET_GRADE_FROM_GRADING_SCHEME_RANGE($SchemeID);
						if(!empty($SchemeGrade)){
							foreach($SchemeGrade as $GradeRangeID => $grade){
								$GradeInfo[$GradeRangeID] = $grade;
								$GradeOption[] = $grade;
							}
						}
					}
					else if($SchemeType == "PF"){
						$PassFailInfo['P'] = $SchemeMainInfo['Pass'];
						$PassFailInfo['F'] = $SchemeMainInfo['Fail'];
						$PassFailOption = array(strtolower($SchemeMainInfo['Pass']), strtolower($SchemeMainInfo['Fail']));
					}
				}
				
				# Get component subject(s) if any
				$CmpSubjectArr = array();
				$isCalculateByCmpSub = 0;
				
				if(!$isCmpSubject){
					$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($currentSubjectID, $ClassLevelID);
					$isParentSubject = (count($CmpSubjectArr) > 0)? true : false;
					
					$CmpSubjectIDArr = array();
					if(!empty($CmpSubjectArr)){	
						for($i=0 ; $i<count($CmpSubjectArr) ; $i++){
							$CmpSubjectIDArr[] = $CmpSubjectArr[$i]['SubjectID'];
							
							$CmpSubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $CmpSubjectArr[$i]['SubjectID'], 0, 0, $ReportID);
							if(count($CmpSubjectFormGradingArr) > 0 && $CmpSubjectFormGradingArr['ScaleInput'] == "M")
								$isCalculateByCmpSub = true;
						}
					}
				}
				
				// special column checking for parent subject
				$isAllComponentZeroWeight = array();
				if ($isParentSubject) {
					for($i=0 ; $i<$ColumnSize ; $i++) {
						$_reportColumnId = $column_data[$i]['ReportColumnID'];
						
						$_isAllComponentZeroWeight = true;
						for($j=0 ; $j<count($CmpSubjectArr) ; $j++) {
							$__cmpSubjectId = $CmpSubjectArr[$i]['SubjectID'];
							
							$__weightAry = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, $other_condition='', $__cmpSubjectId, $_reportColumnId);
							$__subjectWeight = $__weightAry[0]['Weight'];
							
							if ($__subjectWeight > 0) {
								$_isAllComponentZeroWeight = false;
								break;
							}
						}
						
						$isAllComponentZeroWeight[$_reportColumnId] = $_isAllComponentZeroWeight;
					}
				}
			
				$ReportColumn = array();
				$ColumnHasTemplateAry = array();
				if($ColumnSize > 0){
					for($i=0; $i<$ColumnSize ; $i++){
						list($ReportColumnID, $ReportColumnTitle, $DefaultWeight, $SemesterNum) = $column_data[$i];
						$BlockMarksheet = $column_data[$i]['BlockMarksheet'];
						/* 
						* For Whole Year Report use only
						* Check for the MarkSheet Column is allowed to fill in mark when any term does not have related report template in Whole Year Report
						* by SemesterNum (ReportType) && ClassLevelID
						* An array is created as a control parameter [$ColumnHasTemplateAry]
						*/
						if($ReportType == "F")
							$ColumnHasTemplateAry[$ReportColumnID] = $lreportcard->CHECK_REPORT_TEMPLATE_FROM_COLUMN($SemesterNum, $ClassLevelID);
						else 
							$ColumnHasTemplateAry[$ReportColumnID] = 1;
							
						# Get ReportColumnTitle
						$ReportColumnTitle = ($ReportColumnTitle != "" && $ReportColumnTitle != null) ? $ReportColumnTitle : $ColumnTitleAry[$ReportColumnID];

						if($WeightAry[$ReportColumnID][$currentSubjectID] != 0 && $WeightAry[$ReportColumnID][$currentSubjectID] != null && $WeightAry[$ReportColumnID][$currentSubjectID] != ""){
							# Checking for the Readable Columns with (N/A) marked when the case is 
							# It is Whole Year Report AND its terms column has defined Term Report Template OR
							# It has component subjects and the parent subject mark is calculated by its component subjects
							if(($ColumnHasTemplateAry[$ReportColumnID] && $ReportType == "F") || 
							   (!empty($CmpSubjectArr) && $isCalculateByCmpSub && $ScaleInput == "M") 
							    || $lreportcard->Is_Marksheet_Blocked($BlockMarksheet)){
							    	if ($isAllComponentZeroWeight[$ReportColumnID])
							    		$columnValid[$ReportColumnID] = true;
							    	else
										$columnValid[$ReportColumnID] = false;
							}
							else {
								$columnValid[$ReportColumnID] = true;
							}
						}
						else {
							$columnValid[$ReportColumnID] = false;
						}
					}
		
					# Add a Overall Term Subject Mark OR Overall Subject Mark Header when the case is 
					# The ScaleInput is Grade(G) OR PassFail(PF)
//					$hasOverallSubjectMark = 0;
//					if(($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF"){
//						$hasOverallSubjectMark = 1;
//						if($ReportType != "F"){
//							$ColumnTitle[] = $eReportCard['TermSubjectMarkEn'];
//							$ReportColumn[] = $eReportCard['TermSubjectMarkEn'];
//						}
//						else {
//							$ColumnTitle[] = $eReportCard['OverallSubjectMarkEn'];
//							$ReportColumn[] = $eReportCard['OverallSubjectMarkEn'];
//						}
//					}
				}
			}
			
			# Retrieve Existing Data & Manipulate if Header of Format is correct
			if($FormatWrong!=1){
				// $websamsregno = Userlogin of student
				foreach($studentAry as $websamsregno => $studentInfo){
					$data_obj = $studentInfo;
					
					if(trim($currentSubjectGroupCode)=='')
					{
						$WarnMsg[$currentSubjectGroupCode][$compSubjectCode][$websamsregno][] = $Lang['eReportCard']['ImportWarningArr']['empty_sg_code'];
						$WarnCss[$currentSubjectGroupCode][$compSubjectCode][$websamsregno][0] = true;
					}
					else if(!in_array(trim($currentSubjectGroupCode), $SGCodeAry))
					{
						$WarnMsg[$currentSubjectGroupCode][$compSubjectCode][$websamsregno][] = $Lang['eReportCard']['ImportWarningArr']['sg_code_not_found'];
						$WarnCss[$currentSubjectGroupCode][$compSubjectCode][$websamsregno][0] = true;
					}
					else if($currentSubjectID=="")
					{
						$WarnMsg[$currentSubjectGroupCode][$compSubjectCode][$websamsregno][] = $Lang['eReportCard']['ImportWarningArr']['comp_code_not_found'];
						$WarnCss[$currentSubjectGroupCode][$compSubjectCode][$websamsregno][1] = true;
					}
					// [2015-1116-0916-31066] use UserLogin to check student exist or not
					//else if(!$StudentID = $StudentWebSAMSRegNoArr[$websamsregno])
					else if(!$StudentID = $StudentUserLoginArr[$websamsregno])
					{
						$WarnMsg[$currentSubjectGroupCode][$compSubjectCode][$websamsregno][] = $Lang['General']['ImportWarningArr']['WrongWebSamsRegNo'];
						$WarnCss[$currentSubjectGroupCode][$compSubjectCode][$websamsregno][2] = true;
					}
		
					if(!empty($StudentID) && !empty($data_obj)){
						for($j=0; $j<$ColumnSize; $j++){
							list($ReportColumnID, $ReportColumnTitle, $DefaultWeight, $SemesterNum) = $column_data[$j];
		
							# Check for (N/A) exist in Report Column Internally
						  	if($columnValid[$ReportColumnID]){
								$col_no = $j+3;
								
								$isValid = 1;
								$value = trim($data_obj[$col_no]);
								if($value == "")
								{
									$WarnCss[$currentSubjectGroupCode][$compSubjectCode][$websamsregno][$col_no] = true;
									$WarnMsg[$currentSubjectGroupCode][$compSubjectCode][$websamsregno][] = $eReportCard['ImportWarningArr']['EmptyMark'];
									$isValid = 0;
									continue;
								}
								
								$IsEstimated = ($lreportcard->Is_Estimated_Score($value))? 1 : 0;
								$value = $lreportcard->Remove_Estimate_Score_Symbol($value);
								
								$value = (strtolower($value) == "abs") ? strtolower($value) : $value;
								
								# Mark Type : M-Mark, G-Grade, PF-PassFail, SC-SpecialCase
								# Manipulate RawData From CSV file and generate new data Before Insert 
								$MarkNonNum = '';
								$MarkRaw = '-1';				// '-1' : no value provided
								$isSpecialCase = ($SchemeType == "PF") ? $lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2($value) : $lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1($value);
								
								if($SchemeType == "H"){
									if($ScaleInput == "G"){
										$MarkType = ($isSpecialCase) ? "SC" : "G";
										
										// Check Whether the Input Raw Grade is valid (defined in grading scheme) or not
										if($value != "" && (!$isSpecialCase && !in_array($value, $GradeOption)) ){
											
											$WarnCss[$currentSubjectGroupCode][$compSubjectCode][$websamsregno][$col_no] = true;
											$WarnMsg[$currentSubjectGroupCode][$compSubjectCode][$websamsregno][] = $eReportCard['ImportWarningArr']['InvilidMark'];
											$isValid = 0;
											continue;
										}
											
										if($isSpecialCase){
											$MarkNonNum = $value;
										}
										else {
											$grade = '';
											foreach($GradeInfo as $RangeID => $grade){
												if($value == $grade)
													$MarkNonNum = $RangeID;
											}
										}
									}
									else {
										$MarkType = ($isSpecialCase) ? "SC" : "M";
										if($isSpecialCase){
											$MarkRaw = ($value == "+") ? 0 : -1;
											$MarkNonNum = $value;
										}
										else {	// new
											if($value != "" ){
												if($SchemeFullMark != "" && $value > $SchemeFullMark){
													$WarnCss[$currentSubjectGroupCode][$compSubjectCode][$websamsregno][$col_no] = true;
													$WarnMsg[$currentSubjectGroupCode][$compSubjectCode][$websamsregno][] = $eReportCard['ImportWarningArr']['MarkGreaterThanFullMark'];
													$isValid = 0;
													continue;
												} else {
													$MarkRaw = $lreportcard->ROUND_MARK($value, "SubjectScore");
												}
										 	} else {
											 	$MarkRaw = -1;
										 	} 
										 	//$MarkRaw = ($value != "" ) ? $lreportcard->ROUND_MARK($value, "SubjectScore") : -1;
										}
									}
								}
								else if($SchemeType == "PF"){
									// Check Whether the Input Raw PassFail is valid (defined in grading scheme) or not
									if($value != "" && (!$isSpecialCase && !in_array(strtolower($value), $PassFailOption)) ){
										$isValid = 0;
										$WarnCss[$currentSubjectGroupCode][$compSubjectCode][$websamsregno][$col_no] = true;
										$WarnMsg[$currentSubjectGroupCode][$compSubjectCode][$websamsregno][] = $eReportCard['ImportWarningArr']['InvilidMark'];
										continue;
									}
									else
									{	
										foreach($PassFailInfo as $PFKey => $PFString){
											if(strtolower($value) == strtolower($PFString))
												$value = $PFKey;
										}
										
										$MarkType = ($isSpecialCase) ? "SC" : "PF";
										$MarkNonNum = $value;
									}
								}
								else {
									$MarkType = ($isSpecialCase) ? "SC" : "M";
									$MarkRaw = $value;
								}
							
								/*
							    * Allow to import value when 
							    * case 1: Report Type is Term Report AND Weight is not zero / "" / null OR
							    * case 2: It is Whole Year Report AND Any Term Column has not defined term template 
							    * case 3: A subject having component subjects AND its Input Scale is Grade(G) 
							    */
							    if(($ReportType != "F" && $WeightAry[$ReportColumnID][$currentSubjectID] != 0 && $WeightAry[$ReportColumnID][$currentSubjectID] != ""
							    	&& $WeightAry[$ReportColumnID][$currentSubjectID] != null) || 
							    	(!$ColumnHasTemplateAry[$ReportColumnID] && $ReportType == "F") || 
							    	(!empty($CmpSubjectArr) && $ScaleInput == "G") ){
									$ImportData[$rec_no][] = array($ReportID,$StudentID,$currentSubjectID,$ReportColumnID,$SchemeID,$MarkType,$MarkRaw,$MarkNonNum,($IsEstimated?1:0));
								}
					  	    }
						}
						
						# Update for Overall Term Subject Mark OR Overall SubjectMark
						# the case when the input scale is Grade(G) or the scheme type is PassFail(PF)
//						if($hasOverallSubjectMark){
//							$col_no++;
//							$value = $data_obj[$j];
//							$value = (strtolower($value) == "abs") ? strtolower($value) : $value;
//							
//							$IsEstimated = ($lreportcard->Is_Estimated_Score($value))? 1 : 0;
//							$value = $lreportcard->Remove_Estimate_Score_Symbol($value);
//							
//							# Mark Type : M-Mark, G-Grade, PF-PassFail, SC-SpecialCase
//							# Manipulate RawData From CSV file and generate new data Before Insert 
//							$MarkNonNum = '';
//							$isSpecialCase = ($SchemeType == "PF") ? $lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2($value) : $lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1($value);
//							
//							if($SchemeType == "H" && $ScaleInput == "G"){
//								$MarkType = ($isSpecialCase) ? "SC" : "G";
//								
//								// Check Whether the Input Raw Grade is valid (defined in grading scheme) or not
//								if($value != "" && (!$isSpecialCase && !in_array($value, $GradeOption)) ){
//									$isValid = 0;
//									$WarnCss[$currentSubjectGroupCode][$compSubjectCode][$websamsregno][$col_no] = true;
//									$WarnMsg[$$currentSubjectGroupCode][$compSubjectCode][$websamsregnoi][] = $eReportCard['ImportWarningArr']['InvilidMark'];
//									continue;
//								}
//									
//								if($isSpecialCase){
//									$MarkNonNum = $value;
//								}
//								else {
//									$grade = '';
//									foreach($GradeInfo as $RangeID => $grade){
//										if($value == $grade)
//											$MarkNonNum = $RangeID;
//									}
//								}
//							}
//							else if($SchemeType == "PF"){
//								// Check Whether the Input Raw PassFail is valid (defined in grading scheme) or not
//								if($value != "" && (!$isSpecialCase && !in_array(strtolower($value), $PassFailOption)) ){
//									$isValid = 0;
//									$WarnCss[$currentSubjectGroupCode][$compSubjectCode][$websamsregno][$col_no] = true;
//									$WarnMsg[$currentSubjectGroupCode][$compSubjectCode][$websamsregno][] = $eReportCard['ImportWarningArr']['InvilidMark'];
//									continue;
//								}
//									
//								foreach($PassFailInfo as $PFKey => $PFString){
//									if(strtolower($value) == strtolower($PFString))
//										$value = $PFKey;
//								}
//								
//								$MarkType = ($isSpecialCase) ? "SC" : "PF";
//								$MarkNonNum = $value;
//							}
//							
//							$ImportData[$rec_no]["OverallMark"] = array($StudentID,$SubjectID,$ReportID,$SchemeID,$MarkType,$MarkNonNum,$IsEstimated);
//				  	    
//				  	    }
					}
					if(!empty($ImportData[$rec_no]) && $WarnMsg[$currentSubjectGroupCode][$compSubjectCode][$websamsregno]==""){
						$rec_no++;
					}
					else{
						$ImportData[$rec_no] = array();
					}
				}
			}
		}
	}
}

$NoOfFail = count($WarnMsg);
$errctr = 0;
if(count($WarnMsg)>0)
{
	$errctr = 0;
	
	# build Table 
	$display .= '<table width="95%" border="0" cellpadding="5" cellspacing="0">'."\n";
		$display .= '<tr>'."\n";
			$display .= '<td class="tablebluetop tabletopnolink" width="1%">#</td>'."\n";
			for($i=0;$i<sizeof($HeaderRow);$i++)
			{
				$display .= '<td class="tablebluetop tabletopnolink">'.$HeaderRow[$i].'</td>'."\n";
			}
			$display .= '<td class="tablebluetop tabletopnolink" width="30%">'.$Lang['General']['Remark'].'</td>'."\n";
		$display .= '</tr>'."\n";
	
		foreach($WarnMsg as $currentSubjectGroupCode => $CompMsg)
		{
			foreach($CompMsg as $CompCode => $CodeMsg)
			{
				foreach($CodeMsg as $websamsregno => $Msg)
				{
					$rowcss = " class='".($errctr%2==0? "tablebluerow2":"tablebluerow1")."' ";
					$dataSize = sizeof($data[$currentSubjectGroupCode][$CompCode][$websamsregno]);
					
					$display .= '<tr '.$rowcss.'>'."\n";
						$display .= '<td class="tabletext">'.($data[$currentSubjectGroupCode][$CompCode][$websamsregno][$dataSize-1]).'</td>'."\n";
						for($j=0; $j<$dataSize-1; $j++)
						{
							$css = $WarnCss[$currentSubjectGroupCode][$CompCode][$websamsregno][$j]?"red":"";
							$value = $WarnCss[$currentSubjectGroupCode][$CompCode][$websamsregno][$j] && empty($data[$currentSubjectGroupCode][$CompCode][$websamsregno][$j])?"***":$data[$currentSubjectGroupCode][$CompCode][$websamsregno][$j];
							$display .= '<td class="tabletext '.$css.'">'.$value.'</td>'."\n";
						}
						$display .= '<td class="tabletext">'.implode("<br>",$Msg).'</td>'."\n";
					$display .= '</tr>'."\n";
					
					$errctr++;
				}
			}
		}
	$display .= '</table>'."\n";
}
else 
{
	$lreportcard->Copy_Import_All_Subject_Score_To_Temp_Table($ImportData);
}

$NoOfSuccess = $rec_no;
$NoOfFail = $errctr;

$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "javascript:history.back()","back");
$NextBtn = $linterface->GET_ACTION_BTN($button_import, "submit", "","submit2");

$Btn .= empty($WarnMsg)?$NextBtn."&nbsp;":"";
$Btn .= $BackBtn;
					
# tag information
$TAGS_OBJ[] = array($eReportCard['Management_MarksheetRevision'], "", 0);

# page navigation (leave the array empty if no need)
$PAGE_NAVIGATION[] = array($eReportCard['ImportMarks'], "");


$CurrentPage = "Management_MarkSheetRevision";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
							
?>
<form id="form1" name="form1" enctype="multipart/form-data" action="marksheet_import_all_subject_update.php" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?><br/><br/></td>
</tr>
<tr>
	<td><?= $linterface->GET_IMPORT_STEPS(2) ?></td>
</tr>
<tr>
	<td>
		<table width="30%">
			<tr>
				<td class='formfieldtitle'><?=$Lang['General']['SuccessfulRecord']?></td>
				<td class='tabletext'><?=$NoOfSuccess?></td>
			</tr>
			<tr>
				<td class='formfieldtitle'><?=$Lang['General']['FailureRecord']?></td>
				<td class='tabletext <?=$NoOfFail>0?"red":""?>'><?=$NoOfFail?></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<?=$display?>
	</td>
</tr>
<tr>
	<td><table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center"><?=$Btn?></td>
		</tr>
	</table>
	</td>
</tr>
</table>
<input type="hidden" name="isProgress" id="isProgress" value="<?=$isProgress?>"/>
<input type="hidden" name="isProgressSG" id="isProgressSG" value="<?=$isProgressSG?>"/>
<input type="hidden" name="ClassLevelID" value="<?=$ClassLevelID?>" />
<input type="hidden" name="ReportID" value="<?=$ReportID?>" />
<input type="hidden" name="SubjectID" value="<?=$SubjectID?>" />
<input type="hidden" name="ClassID" value="<?=$ClassID?>" />
<input type="hidden" name="SubjectGroupID" value="<?=$SubjectGroupID?>" />
<!--<input type="hidden" name="ParentSubjectID" value="<?=$ParentSubjectID?>" />-->

</form>
<br><br>
<?
$linterface->LAYOUT_STOP();

?>