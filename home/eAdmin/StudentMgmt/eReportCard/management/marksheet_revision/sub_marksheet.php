<?php
// using: 
/***************************************************
 * 	Modification log:
 * 20170105 Villa 
 * 		- add submark sheet layer -btn 
 * 20160112 Bill [2015-0924-1821-07164] [ip.2.5.7.1.1]:
 * 		- add checking for Wah Yan, Kowloon (F4-6)	($eRCTemplateSetting['Marksheet']['ManualInputGrade'])
 * 20150111 Bill [DM#2930] [ip.2.5.7.1.1]
 * 		- add checking if assessment and student number valid before showing Save button
 * 20151202 Ivan [X86217] [ip.2.5.7.1.1]
 * 		- added $eRCTemplateSetting['SubMS']['DefaultTransferToMarksheet'] logic
 * 20151202 Ivan [E89838] [ip.2.5.7.1.1]
 * 		- added "/" for sub-marksheet with full mark case
 * 20150710 Bill:
 *		- 2014-1121-1759-15164 - Remove ratio remarks 
 * 20150318 Bill:
 *		- 2014-1121-1759-15164 - Supppot Full Mark and Passing Mark for 救恩書院 
 * 20120403 Marcus:
 * 		- Allow teacher to view marks even after submission period
 * 		- 2012-0208-1800-43132 - 沙田循道衛理中學 - Edit mark after submit the marksheet 
 * *************************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	}
	/* Temp Library*/
	include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_schedule.php");
	$libreportcard = new libreportcard2008j();
	$lreportcard_schedule = new libreportcard_schedule();
	//$lreportcard = new libreportcard();
	
	$CurrentPage = "Sub-Marksheet Submission";
	$MODULE_OBJ = $libreportcard->GET_MODULE_OBJ_ARR();
	$MODULE_OBJ['title'] = $eReportCard['SubMarksheet'];
	
	$column_data = $libreportcard->returnReportTemplateColumnData($ReportID); 		// Column Data
	$ColumnSize = sizeof($column_data);
	$ColumnTitleAry = $libreportcard->returnReportColoumnTitle($ReportID);
	
	
	/* edit for Block Test */
	if (count($column_data) > 0) {
		foreach((array)$column_data as $thiscolumndata ) {
			$columnIDDataMap[$thiscolumndata['ReportColumnID']] = $thiscolumndata;
		}
	}
	
	# Get Report Column Weight and Form Number for Wah Yan, Kowloon
	if($eRCTemplateSetting['Marksheet']['ManualInputGrade']){
		# Get Form Number
		$CurrentSubMSReportSetting = $libreportcard->returnReportTemplateBasicInfo($ReportID);
		$CurrentClassLevelID = $CurrentSubMSReportSetting["ClassLevelID"];
		$CurrentFormNumber = $libreportcard->GET_FORM_NUMBER($CurrentClassLevelID);
		
		# Load Settings - Calculation
		$SettingCategory = 'Calculation';
		$categorySettings = $libreportcard->LOAD_SETTING($SettingCategory);
		$CalculationType = $categorySettings['OrderTerm'];
		
		# Get Report Column Weight
		$WeightAry = array();
		if($CalculationType == 2){
			if(count($column_data) > 0){
		    	for($i=0 ; $i<count($column_data) ; $i++){
					//$OtherCondition = "SubjectID IS NULL AND ReportColumnID = ".$column_data[$i]['ReportColumnID'];
					$OtherCondition = "SubjectID = '$SubjectID' AND ReportColumnID = ".$column_data[$i]['ReportColumnID'];
					$weight_data = $libreportcard->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
					
					$WeightAry[$weight_data[0]['ReportColumnID']][$SubjectID] = $weight_data[0]['Weight'];
				}
			}
		}
		else {
			$weight_data = $libreportcard->returnReportTemplateSubjectWeightData($ReportID);  
			
			for($i=0 ; $i<count($weight_data) ; $i++)
				$WeightAry[$weight_data[$i]['ReportColumnID']][$weight_data[$i]['SubjectID']] = $weight_data[$i]['Weight'];
		}
	}

	$TempIP = $SubjectID;
	if ($ParentSubjectID != "") $TempIP = $ParentSubjectID;
	//$IsMSSubmissionPeriod = $libreportcard->CHECK_MARKSHEET_SUBMISSION_PERIOD();
//	$IsMSSubmissionPeriod = true;
	
	# Get Status of Completed Action
	# $PeriodAction = "Submission" or "Verification"
	$PeriodAction = "Submission";
	$PeriodType = $libreportcard->COMPARE_REPORT_PERIOD($ReportID, $PeriodAction);
	$SpecialPeriodType = $lreportcard_schedule->compareSpecialMarksheetSubmissionPeriodForTeacher($ReportID, $_SESSION['UserID']);
	if ($SpecialPeriodType == 1) {
		$PeriodType = 1;
	}
	$IsMSSubmissionPeriod = false;
	
	if($PeriodType != 2) $IsMSSubmissionPeriod = true;
	
    if ($libreportcard->hasAccessRight())
    {
    	if(!isset($UsePercent))
    		$UsePercent = $eRCTemplateSetting['DefaultUsePercentageRatioInSubMS'];
    	
	    $useSpecialSymbolAsOverall = ($sys_custom['eRC']['SubMS']['UseSymbolAsOverallMark'])? 1 : 0;
	    
        $linterface = new interface_html("popup.html");
        
        # get permission
        $isAdmin = $libreportcard->IS_ADMIN_USER($UserID);
        
        # updated on 06 Mar 2017 by Frankie
        # disable edit if submission is end
        if ((($PeriodType != 1 && !$IsMSSubmissionPeriod) || $libreportcard->Is_Marksheet_Blocked($columnIDDataMap[$ReportColumnID]["BlockMarksheet"])) && !$isAdmin) {
        	$Disabled = true;
        }
        
        if ($top_menu_mode == 0)
			$fromEService = true;
		else
			$fromEService = false;

		# get class object
		$ClassObj = $libreportcard->GET_CLASSES_BY_FORM("", $ClassID);
		$ClassName = $ClassObj[0][1];
		
		# Get Subject Name
		$SubjectName = $libreportcard->GET_SUBJECT_NAME($SubjectID);

		$ReportColumnInfo = $libreportcard->returnReportTemplateColumnData($ReportID);
		for ($i=0; $i<sizeof($ReportColumnInfo); $i++)
		{
			if ($ReportColumnInfo[$i]['ReportColumnID']==$ReportColumnID)
			{
				$ColumnName = $ReportColumnInfo[$i]['ColumnTitle'];
				$ColumnWeight = $ReportColumnInfo[$i]['DefaultWeight']*100;
			}
		}
		$isSubLevel = ($currentLevel>0)? true:false;
		
		# Get ReportSubjectName
		/*
		$ReportSubject = $libreportcard->GET_REPORT_SUBJECT_ID($ReportID, $SubjectID);
		$ReportSubjectID = $ReportSubject[0];
		$ReportCell = $libreportcard->GET_REPORT_CELL_BY_SUBJECT_COLUMN($ReportSubjectID, $ReportColumnID);
		$Disabled = ($ReportCell[$ReportColumnID] == "N/A") ? 1 : 0;
		*/
		
		if(!empty($ReportColumnID) || $isSubLevel)
		{
// 			$param = "SubjectID=$SubjectID&ClassID=$ClassID&ReportColumnID=$ReportColumnID&TargetStudentIDList=$TargetStudentIDList&ReportID=$ReportID&jsReportColumn=$jsReportColumn&SubjectGroupID=$SubjectGroupID";
			$param = "SubjectID=$SubjectID&ClassID=$ClassID&SchoolCode=$SchoolCode&ReportColumnID=$ReportColumnID&ReportID=$ReportID&jsReportColumn=$jsReportColumn&SubjectGroupID=$SubjectGroupID&SubReportColumnID=$SubReportColumnID&currentLevel=$currentLevel";
			
			$ReportSetting = $libreportcard->returnReportTemplateBasicInfo($ReportID);
			$ClassLevelID = $ReportSetting['ClassLevelID'];
			
			# Get Subject Full Mark
			// [2014-1121-1759-15164] - get scale input only
			$FullMarkInputScale = $eRCTemplateSetting['SubMS']['SubMSOverallCalculation']? 1 : 0; 
			// $SubjectFullMarkArr = $libreportcard->returnSubjectFullMark($ClassLevelID, 1, array(), 0, $ReportID);
			$SubjectFullMarkArr = $libreportcard->returnSubjectFullMark($ClassLevelID, 1, array(), $FullMarkInputScale, $ReportID);
			$SubjectFullMark = $SubjectFullMarkArr[$SubjectID];
			
			if($eRCTemplateSetting['Marksheet']['SubMarksheetSubLevel'] && $isSubLevel && (($currentLevel + 1) < $eRCTemplateSetting['Marksheet']['SubMarksheetMaxSubLevel']))
			{
				$lowerLevelColumnAry = $libreportcard->GET_ALL_SUB_MARKSHEET_COLUMN($ReportColumnID, $SubjectID, ($currentLevel - 1));
				$lowerLevelColumnAry = BuildMultiKeyAssoc($lowerLevelColumnAry, array("ColumnID"));
				$thisColumn = $lowerLevelColumnAry[$SubReportColumnID];
				$ColumnName = $thisColumn['ColumnTitle'];
				$ColumnWeight = $thisColumn['Ratio']*100;
				$ColumnFullMark = $thisColumn['FullMark'];
			}
			
//			# Get Subject Passing Mark [2014-1121-1759-15164]
//			$SubjectPassingMarkArr = $libreportcard->returnSubjectPassingMark($ClassLevelID, 1, array(), 0, $ReportID);
//			$SubjectPassingMark = $SubjectPassingMarkArr[$SubjectID];

			# Get Column Array
// 			$ColumnArray = $libreportcard->GET_ALL_SUB_MARKSHEET_COLUMN($ReportColumnID, $SubjectID);
			$ColumnArray = $libreportcard->GET_ALL_SUB_MARKSHEET_COLUMN($ReportColumnID, $SubjectID, $currentLevel, $SubReportColumnID);
			$ColumnSize = sizeof($ColumnArray);
			
			## Calculation default selection
			if ($ColumnArray[0]['Calculation'] == "" || $ColumnArray[0]['Calculation'] == "F")
			{
				$CalculationMethod = "Formula";
				$formula_checked = "checked";
				$average_checked = "";
			}
			else
			{
				$CalculationMethod = "Average";
				$formula_checked = "";
				$average_checked = "checked";
			}
			
			## Transfer to Marksheet selection
//			if ($ColumnArray[0]['TransferToMS'] == 1)
//			{
//				$transferToMS_yes_checked = 'checked';
//				$transferToMS_no_checked = '';
//			}
//			else
//			{
//				$transferToMS_yes_checked = '';
//				$transferToMS_no_checked = 'checked';
//			}
			
			if ($eRCTemplateSetting['SubMS']['DefaultTransferToMarksheet']) {
				$transferToMS_yes_checked = 'checked';
				$transferToMS_no_checked = '';
			}
			else {
				if ($ColumnArray[0]['TransferToMS'] == 1)
				{
					$transferToMS_yes_checked = 'checked';
					$transferToMS_no_checked = '';
				}
				else
				{
					$transferToMS_yes_checked = '';
					$transferToMS_no_checked = 'checked';
				}	
			}
			
//			// Default set to True if not set Transfer overall mark to marksheet before
//			if ($eRCTemplateSetting['SubMS']['DefaultTransferToMarksheet'] && $ColumnArray[0]['TransferToMS']=="") {
//				$transferToMS_yes_checked = 'checked';
//				$transferToMS_no_checked = '';
//			}
//			else if ($ColumnArray[0]['TransferToMS'] == 1)
//			{
//				$transferToMS_yes_checked = 'checked';
//				$transferToMS_no_checked = '';
//			}
//			else {
//				$transferToMS_yes_checked = '';
//				$transferToMS_no_checked = 'checked';	
//			}
			
			# Get and Student Array
			//$StudentArray = $libreportcard->GET_STUDENT_BY_CLASS($ClassID, $TargetStudentIDList);
			//$StudentArray = $libreportcard->GET_STUDENT_BY_CLASS($ClassID, $TargetStudentIDList, 1);
			// Student Info Array
			if ($SubjectGroupID != '')
			{
				$StudentArray = $libreportcard->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupID, $ClassLevelID, '', 1, 1);
				$showClassName = true;
				
				$obj_SubjectGroup = new subject_term_class($SubjectGroupID);
				$SubjectGroupName = $obj_SubjectGroup->Get_Class_Title();
				$FirstRowTitle = $Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'];
				$FirstRowDisplay = $SubjectGroupName;
			}
			else
			{
//				if ($libreportcard->IS_ADMIN_USER($_SESSION['UserID']) == true || $libreportcard->Is_Class_Teacher($_SESSION['UserID'], $ClassID) == true)
//				{
//					# Admin / Class Teacher => Show all students
//					$TaughtStudentList = '';
//				}
//				else
//				{
//					# Subject Teacher => Show taught students only
//					$TargetSubjectID = ($ParentSubjectID != '')? $ParentSubjectID : $SubjectID;
//					$TaughtStudentArr = $libreportcard->Get_Student_List_Taught_In_This_Consolidated_Report($ReportID, $_SESSION['UserID'], $ClassID, $TargetSubjectID);
//					$TaughtStudentList = '';
//					if (count($TaughtStudentArr > 0))
//						$TaughtStudentList = implode(',', $TaughtStudentArr);
//				}
//				$StudentArray = $libreportcard->GET_STUDENT_BY_CLASS($ClassID, $TaughtStudentList, 1, 0, 1);

				//2014-0224-1422-33184
				$StudentArray = $libreportcard->Get_Marksheet_Accessible_Student_List_By_Subject_And_Class($ReportID, $_SESSION['UserID'], $ClassID, $ParentSubjectID, $SubjectID);

				$showClassName = false;
				
				$FirstRowTitle = $eReportCard['Class'];
				$FirstRowDisplay = $ClassName;
			}
			$StudentSize = sizeof($StudentArray);
			
			if(!empty($TargetStudentIDList)) {
				$StudentIDList = $TargetStudentIDList;
			}
			else {
				for($i=0; $i<$StudentSize; $i++)
				{
					$StudentIDArray[] = $StudentArray[$i][0];
				}
				if(!empty($StudentIDArray))
					$StudentIDList = implode(",", $StudentIDArray);
			}
		
			$MarksheetTable = "<table width='100%' border='0' cellspacing='0' cellpadding='5'>";
			$MarksheetTable .= "<tr>
								<td width='15%' class='tabletopnolink tablegreentop'>".$eReportCard['StudentNo_short']."</td>
								<td class='tabletopnolink tablegreentop'>".$i_identity_student."</td>";
			
			$ColumnWeightArr = array();
			$jsColumnWeightArr = array();
			$jsColumnFullMarkArr = array();
//			$jsColumnPassingMarkArr = array();
			for($i=0; $i<$ColumnSize; $i++)
			{
				// Add $PassingMark [2014-1121-1759-15164]
				list($ColumnID, $ColumnTitle, $Weight, $Calculation, $TransferToMS, $FullMark, $PassingMark) = $ColumnArray[$i];
				$ColumnWeightArr[$ColumnID] = $Weight;
				$jsColumnWeightArr[] = $Weight;
				$jsColumnFullMarkArr[] = $FullMark;
				// Add $jsColumnPassMarkArr [2014-1121-1759-15164]
//				$jsColumnPassingMarkArr[] = $PassingMark;
				
				if($ReportCardCustomSchoolName=="munsang_college" || $eRCTemplateSetting['SubMS']['SubMSOverallCalculation'])
					$formulaEquation .= ($formulaEquation!="" ? " + " : "") . "($ColumnTitle / $FullMark) x <span class='ratio' orivalue='$Weight'>$Weight</span><span class='PercentSign'></span>";
				else
					$formulaEquation .= ($formulaEquation!="" ? " + " : "") . "$ColumnTitle x <span class='ratio' orivalue='$Weight'>$Weight</span><span class='PercentSign'></span>";

				$averageEquation .= ($averageEquation!="" ? " + " : "(") . "$ColumnTitle";
					
				if($Weight!="") $ColumnTitle .= "<br />".$eReportCard['Ratio'].": <span class='ratio' orivalue='$Weight'>".$Weight."</span><span class='PercentSign'></span>";
				if(!empty($FullMark)) $ColumnTitle .= "<br />".$eReportCard['SchemesFullMark'].": ".$FullMark;
				// Add Passing Mark display [2014-1121-1759-15164]
				if(!empty($PassingMark)) $ColumnTitle .= "<br />".$eReportCard['SchemesPassingMark'].": ".$PassingMark;
				
				$MarksheetTable .= "<td width='12%' align='center' class='tabletopnolink tablegreentop'>";
				if ($IsMSSubmissionPeriod) {
					// if (!$Disabled && $isAdmin && !$fromEService) {
					if ( (!$Disabled && $isAdmin && !$fromEService) ||
							($eRCTemplateSetting['Marksheet']['SubMarksheetSkipAdminChecking'] && !$Disabled) ) {
						$MarksheetTable .= "<a class='tablelink' href=\"javascript:jDELETE_COLUMN('$ColumnID')\" >
											<img src=\"$image_path/$LAYOUT_SKIN/icon_delete.gif\" border=0 align='absmiddle' title='".$eReportCard['RemoveColumn']."'></a>&nbsp;<a class='tablelink' href=\"javascript:jEDIT_COLUMN($ColumnID)\" >
											<img src=\"$image_path/$LAYOUT_SKIN/icon_edit.gif\" border=0 align='absmiddle' title='".$eReportCard['EditColumn']."'></a><br />";
					}
				}
				$MarksheetTable .= $ColumnTitle;
				//2017-01-05 Villa SubmarkSheet layer
				if($eRCTemplateSetting['Marksheet']['SubMarksheetSubLevel'] && (($currentLevel + 1) < $eRCTemplateSetting['Marksheet']['SubMarksheetMaxSubLevel'])){
					$MarksheetTable .= '<br /> <input type="button" class="formsmallbutton" onClick="javascript:newWindow(\'sub_marksheet.php?SubjectID='.$SubjectID.'&ClassID='.$ClassID.'&SchoolCode='.$SchoolCode.'&ReportColumnID='.$ReportColumnID.'&ReportID='.$ReportID.'&jsReportColumn='.$i.'&SubjectGroupID='.$SubjectGroupID.'&SubReportColumnID='.$ColumnID.'&currentLevel='.($currentLevel + 1).'\', '.($currentLevel + 11).')" value="'.$eReportCard['SubMS'].'" onMouseOver="this.className=\'formsmallbuttonon\'" onMouseOut="this.className=\'formsmallbutton\'"/>';
				}
				$MarksheetTable .= "</td>";
			}
			$averageEquation .= ") / ".$ColumnSize;
			
			if($ColumnSize>0)
			{
				$MarksheetTable .= "<td width='12%' align='center' class='tabletopnolink tablegreentop' nowrap>".$eReportCard['Template']['OverallCombined']."<br />($ColumnName)</td>";
				if($ReportCardCustomSchoolName=="munsang_college" || $eRCTemplateSetting['SubMS']['SubMSOverallCalculation'])
					$formulaEquation = $ColumnName . " = (" . $formulaEquation.")/ ".array_sum($jsColumnWeightArr)." x $SubjectFullMark";
				else
					$formulaEquation = $ColumnName . " = " . $formulaEquation;
				$averageEquation = $ColumnName . " = " . $averageEquation;
				
				$equation = ($CalculationMethod == "Formula")? $formulaEquation : $averageEquation;
			}
			$MarksheetTable .= "</tr>";
			
			echo ConvertToJSArray($jsColumnWeightArr, "jWeightArr");
			echo ConvertToJSArray($jsColumnFullMarkArr, "jFullMarkArr");
			// Add JS array jPassMarkArr [2014-1121-1759-15164]
//			echo ConvertToJSArray($jsColumnPassingMarkArr, "jPassingMarkArr");
			echo ConvertToJSArray($StudentIDArray, "jStudentIDArr");
			
			##########################################################################################
			# Generate Sub-Marksheet Table

			$SubMarksheetResult = $libreportcard->GET_SUB_MARKSHEET_SCORE($ReportColumnID, $SubjectID);
			for($j=0; $j<$StudentSize; $j++)
			{
				list($StudentID, $RegNo, $ClassNo, $StudentName, $ClassName) = $StudentArray[$j];
				$td_style = ($j%2==1) ? "tablegreenrow2" : "tablegreenrow1";
				
				if ($showClassName)
					$thisClassDisplay = $ClassName.'-'.$ClassNo;
				else
					$thisClassDisplay = $ClassNo;
				
				$MarksheetTable .= "<tr class=\"dataRow\">";
				$MarksheetTable .= "<td class='tabletext $td_style' nowrap='nowrap'>".$thisClassDisplay."</td>";
				$MarksheetTable .= "<td class='tabletext $td_style' nowrap='nowrap'>".$StudentName."</td>";
				
				if($ColumnSize>0)
				{
					$MarkArray = array();
					$ColumnIDArray = array();
					$TotalPercent = 0;
					for($i=0; $i<$ColumnSize; $i++)
					{
						list($ColumnID, $ColumnTitle, $Weight) = $ColumnArray[$i];
						$Mark = "";
						
						$ColumnID = trim($ColumnID);
						if(!in_array($ColumnID, $ColumnIDArray))
								$ColumnIDArray[] = trim($ColumnID);

						$MarkLong = $SubMarksheetResult[$StudentID][$ColumnID];
						$Mark = ($MarkLong!="") ? round($MarkLong, 1) : $Mark;
						
						$LongFieldName = "mark_long_".$StudentID."_".$ColumnID;
						$FieldName = "mark_".$StudentID."_".$ColumnID;
						$FieldID = "mark[$j][$i]";
						
						/*
						if($Mark!="-2")
						{
							$TotalPercent = $TotalPercent + $Weight;
							if($Mark>=0)						
								$MarkArray[] = $MarkLong;
						}
						if($Mark<0) {
							$Mark = $libreportcard->SpecialMarkArray[trim(-$Mark)];
							$MarkLong = $Mark;
						}
						*/
						
						if($Mark < 0) {
							$Mark = $libreportcard->specialCasesSet1[abs($Mark)];
						}

						if ($Disabled) {
							$InputField = $Mark."<input type='hidden' name='$FieldName' id='$FieldID' value='".$Mark."'/>";
						} else {
							$InputField = "<input class='textboxnum' maxlength='8' type='text' name='$FieldName' id='$FieldID' value='".$Mark."' 
												onblur=\"if (!setStart) { this.value=startContent; setStart = 1}\" 
												onkeyup=\"isPasteContent(event, '$j', '$i'); updateOverall($j);\" 
												onkeydown=\"isPasteContent(event, '$j', '$i');\" 
												onPaste=\"pasteTable('$j', '$i');\" 
												/>";
						}						
						$InputField .= "<input type='hidden' name='{$LongFieldName}' value='".$MarkLong."' >";

						$MarksheetTable .= "<td align='center' class='tabletext $td_style'>".$InputField."</td>";
					}
					$ColumnIDList = implode(",", $ColumnIDArray);

					$OverallMarkLong = $SubMarksheetResult[$StudentID]["Overall"];
					# The overall mark will be calculated by the javascript function later
					//$OverallMark = ($OverallMarkLong!="") ? round($OverallMarkLong, 1) : $OverallMark;
					$OverallLongFieldName = "overall_long_".$StudentID;
					$OverallFieldName = "overall_".$StudentID;
					$OverallFieldID = "overall[$j]";
					$OverallHiddenFieldID = "overallHidden[$j]";
					
					$MarksheetTable .= "<td align='center' class='tabletext $td_style'>";
					/*
					if ($Disabled) {
						$MarksheetTable .= $OverallMark."<input type='hidden' name='$OverallFieldName' id='$OverallFieldID' value='".$OverallMark."' />";
					} else {
						$MarksheetTable .= "<input class='textboxnum' maxlength='8' type='text' name='$OverallFieldName' id='$OverallFieldID' value='".$OverallMark."' onPaste=\"pasteTable('$j', '$i');\" onblur=\"if (!setStart) { this.value=startContent; setStart = 1}\" />";
					}
					*/
					$MarksheetTable .= "<span id='$OverallFieldID'>$OverallMark</span>";
					$MarksheetTable .= "</td>";
					//$MarksheetTable .= "<input type='hidden' name='{$OverallLongFieldName}' value='".$OverallMarkLong."' >";
					$MarksheetTable .= "<input type='hidden' name='{$OverallFieldName}' id='$OverallHiddenFieldID' value='".$OverallMark."' >";

					$i++;
					/*
					$OverallWeightedMarkLong = $SubMarksheetResult[$StudentID]["WeightedOverall"];
					$OverallWeightedMark = ($OverallWeightedMarkLong!="")? round($OverallWeightedMarkLong, 1) : $OverallWeightedMark;

					$OverallWeightedLongFieldName = "overall_weighted_long_".$StudentID;
					$OverallWeightedFieldName = "overall_weighted_".$StudentID;
					$OverallWeightedFieldID = "mark[$j][$i]";
					$OverallWeightedMark = ($OverallWeightedMark<0) ? $libreportcard->SpecialMarkArray[trim($OverallWeightedMark)] : $OverallWeightedMark;
					$MarksheetTable .= "<td align='center' class='tabletext $td_style'>";
					if ($Disabled) {
						$MarksheetTable .= $OverallWeightedMark."<input type='hidden' name='$OverallWeightedFieldName' id='$OverallWeightedFieldID' value='".$OverallWeightedMark."' />";
					} else {
						$MarksheetTable .= "<input class='textboxnum' maxlength='8' type='text' name='$OverallWeightedFieldName' id='$OverallWeightedFieldID' value='".$OverallWeightedMark."' onPaste=\"pasteTable('$j', '$i');\" onblur=\"if (!setStart) { this.value=startContent; setStart = 1}\" />";
					}					
					$MarksheetTable .= "</td>";
					*/
					$MarksheetTable .= "<input type='hidden' name='{$OverallWeightedLongFieldName}' value='".$OverallWeightedMarkLong."' >";
				}
				$MarksheetTable .= "</tr>";
			}
			$MarksheetTable .= "</table>";

			if($eRCTemplateSetting['DisableMarksheetImport']!==true && ($IsMSSubmissionPeriod || $ck_ReportCard_UserType=="ADMIN"))
				$ImportButton = $linterface->GET_SMALL_BTN($button_import, "button", "javascript:self.location.href='sub_marksheet_import.php?$param'");
				
			$ExportButton = "&nbsp;".$linterface->GET_SMALL_BTN($button_export, 'button', "javascript:self.location.href='sub_marksheet_export.php?$param'");
			$MarkRemindRow = "<tr><td align='left' class='tabletextremark' colspan=2>".$eReportCard['MarkRemind']."</td></tr>";

			$colsize = ($ColumnSize>0) ? $ColumnSize+4 : 2;
			$x = $MarkRemindRow;
			$x .= "<tr><td class='tabletext' colspan='$colsize' align='right'><a class='tablelink' href='#bottom'>".$eReportCard['GoToBottom']."</a></td></tr>";
			$x .= "<tr><td colspan='1' align='left'>";
			if($ColumnSize>0)
			{
				if (!$Disabled) $x .= $ImportButton;
				$x .= $ExportButton;
				//if (!$Disabled) $x .= "&nbsp;".$linterface->GET_SMALL_BTN($eReportCard['CalculateOverallResult'], 'button', "javascript:jSUBMIT_FORM(0)");
			}
			else
				$x .= "&nbsp;";
			$x .= "</td>";
			$x .= "<td align='right' nowrap='nowrap'>";
			if (($IsMSSubmissionPeriod) && (!$Disabled) && ($isAdmin) && !($fromEService))
				$x .= "<a class='tablelink' href=\"javascript:jsAddAssessment()\" ><img src=\"$image_path/$LAYOUT_SKIN/icon_new.gif\" border=0 align='absmiddle' title='".$eReportCard['AddAssesment']."'>".$eReportCard['AddAssesment']."</a>";
			$x .= "</td></tr>";	
			$x .= "<tr id='AssessmentForm' style='display:none;'>";
				$x .= "<td valign='top' nowrap='nowrap'  class='formfieldtitle tabletext' style='text-align:right'>";
				$x .= $eReportCard['ColumnTitle'].": <input type='text' name='ColumnTitle' class='tabletext' value='' /> &nbsp;";
				if($ReportCardCustomSchoolName=="munsang_college")
					$x .= $eReportCard['SchemesFullMark'].": <input type='text' name='FullMark' class='tabletext' value='' size='8' /> ";
				// Add Full Mark and Passing Mark input [2014-1121-1759-15164]
				if($eRCTemplateSetting['SubMS']['DisplayFullMarks'])
					$x .= $eReportCard['SchemesFullMark'].": <input type='text' name='FullMark' class='tabletext' value='' size='8' /> ";
				if($eRCTemplateSetting['SubMS']['DisplayPassingMarks'])
					$x .= $eReportCard['SchemesPassingMark'].": <input type='text' name='PassingMark' class='tabletext' value='' size='8' /> ";	
				// Hide Remarks for Kau Yan College Cust [2014-1121-1759-15164]
//				$x .= $eReportCard['Ratio'].":<input type='text' name='ColumnRatio' class='tabletext' value='' size='8' /><span class='PercentSign'></span> <span class='tabletextremark'>(<span id='RatioRemark'>".$eReportCard['AssessmentRatioRemarks']."</span>)</span>&nbsp;".$linterface->GET_ACTION_BTN($button_submit, "button", "javascript:submitNewColumn(this.form)")."</td>";
				$x .= $eReportCard['Ratio'].":<input type='text' name='ColumnRatio' class='tabletext' value='' size='8' /><span class='PercentSign'></span> ";
				if(!$eRCTemplateSetting['SubMS']['SubMSOverallCalculation'])
					$x .= "<span class='tabletextremark'>(<span id='RatioRemark'>".$eReportCard['AssessmentRatioRemarks']."</span>)</span>&nbsp;";
				$x .= $linterface->GET_ACTION_BTN($button_submit, "button", "javascript:submitNewColumn(this.form)");
				$x .= "</td>";
			$x .= "</tr>\n";
			$x .= "<tr><td align='right' colspan='2' class='tabletextremark'>".$eReportCard['MarkRemindSet_SubMS']."</td></tr>";
			$x .= "<tr><td valign='top' nowrap='nowrap' colspan='2'>".$MarksheetTable."</td></tr>
					<tr><td align='right' colspan='2' nowrap='nowrap'><a class='tablelink' href='#top'>".$eReportCard['GoToTop']."</a></td></tr>";
			$x .= $MarkRemindRow;

			$ButtonRow = "<tr><td align='center' id='bottom'>";
			if ($Disabled) {
				$ButtonRow .= $linterface->GET_ACTION_BTN($button_close, "button", "javascript:windowClose()")."\n";
			} else {
				/* 
				 * [DM#2930] Logic to show Save button when:
				 * 	1. Within Submission Period OR is eReportCard admin
				 * 		AND
				 * 	2. Assessment Number > 0
				 * 		AND
				 * 	3. Student Number > 0
				 * 	
				 * 	Logic:	NOT( (NOT(A) AND NOT(B)) OR NOT(C) OR NOT(D) )
				 * 		  	(A OR B) AND C AND D
				 */
				if (!( (!$IsMSSubmissionPeriod && ($ck_ReportCard_UserType!="ADMIN")) || !($ColumnSize > 0) || !($StudentSize > 0) )) {
					$ButtonRow .= $linterface->GET_ACTION_BTN($button_save, "button", "javascript:jSUBMIT_FORM(1)")."\n";
					/*
					$ButtonRow .= "&nbsp;";		
					$ButtonRow .= $linterface->GET_ACTION_BTN($eReportCard['SaveAndUpdateMainMS'], "button", "javascript:jSUBMIT_FORM(2)");*/
					$ButtonRow .= "&nbsp;";
				}
				$ButtonRow .= ($HaveComponent) ? $linterface->GET_ACTION_BTN($button_back, "button", "javascript:self.location.href='marksheet_edit1.php?$param&ParentSubjectID=$ParentSubjectID'") : $linterface->GET_ACTION_BTN($button_close, "button", "javascript:windowClose()");
				$ButtonRow .= "\n";
			}
			$ButtonRow .= "</td></tr>";
		}

		$checked = $UsePercent==1?"checked":"";
		$UsePercentChkbox = '<input type="checkbox" id="UsePercent" name="UsePercent" onclick="ChangeRatioDisplay()" value=1 '.$checked.'>';

################################################################

$linterface->LAYOUT_START();
echo $linterface->Include_Excel_JS_CSS();
?>

<script language="javascript">
var rowx = 0, coly = 0;		//init
var xno = "<?=$ColumnSize?>"; yno = "<?=$StudentSize?>";		// set table size

var startContent = "";
var setStart = 1;

$(document).ready( function() {
	if('<?=$Result?>'=='update'&& '<?=$currentLevel?>'!=''){
		window.opener.location.reload();
	}
	jQuery.excel('dataRow');
});

function setxy(jParX, jParY){
	rowx = jParX;
	coly = jParY;
	document.getElementById('mark['+ jParX +']['+ jParY +']').blur();
}

function isPasteContent(e, j, i){
	var code = (document.all) ? e.keyCode : e.which;
	var ctrl = (document.all) ? e.ctrlKey : Event.CONTROL_MASK;
	
	// Mozilla: V-e.DOM_VK_V
	if (ctrl && code==86){ //CTRL+V
		// action
		pasteTable(j, i);
	}
	return;
}

function paste_clipboard() {

	var temp1 = null;
	var text1 = null;
	var s = "";

	// you must use a 'text' field
	// (a 'hidden' text field will not show anything)
	if(window.netscape){	// Mozilla.Fixfox 
		try {  
			netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");  
		} catch (e) { 
		 	alert('<?=$eReportCard["jsFirefoxNotAllowPaste"]?>'); 
		 	return false;   
		}  
	/*
		// create a clipboard object that refers to the system clipboard
		var transobj = Components.classes['@mozilla.org/widget/transferable;1'].createInstance(Components.interfaces.nsITransferable); 
		if (!transobj) return;      
		var transid = Components.interfaces.nsITransferable;  
		var trans = Components.classes["@mozilla.org/widget/transferable;1"].getService(transid);         
		
		var clipobj = Components.classes['@mozilla.org/widget/clipboard;1'].createInstance(Components.interfaces.nsIClipboard); 
		if (!clipobj)  return;  
		var clipid = Components.interfaces.nsIClipboard;
		var clip = Components.classes['@mozilla.org/widget/clipboard;1'].getService(clipid);
		
		clip.setData(trans, null, clipid.kGlobalClipboard);	// copy data to the system clipboard of the browser
	*/
		paste();	// copy content from system clipboard to a text area
	} else if(document.all){	// Internet Explorer
		temp1 = document.getElementById('text1').createTextRange();
		temp1.execCommand('Paste');
	}
	
	//temp1 = document.getElementById('text1').createTextRange();
	//temp1.execCommand( 'Paste' );

	s = document.getElementById('text1').value; // clipboard is pasted to a string here
	setStart = 0;

	document.getElementById('mark['+ (rowx) +']['+ (coly) +']').value = "";

	var row_array = s.split("\n");
	var row_num = 0;

	while (row_num < row_array.length)
	{
		var col_array = row_array[row_num].split("\t");
		var col_num = 0;
		var xPos=0, yPos=0;
		while (col_num < col_array.length)
		{
			if ((col_num == 0) && (row_num == 0)) startContent = col_array[0];
			if ((row_array[row_num] != "")&&(col_array[col_num] != ""))
			{
				var temp = document.getElementById('mark['+ (row_num + rowx) +']['+ (col_num + coly) +']');
				xPos = parseInt(row_num) + parseInt(rowx);
				yPos = parseInt(col_num) + parseInt(coly);
				if (document.getElementById('mark['+ xPos +']['+ yPos +']') != null)
				{
					document.getElementById('mark['+ xPos +']['+ yPos +']').value = Trim(col_array[col_num]);
				}
			}
			col_num+=1;
		}
		
		if (row_num < yno)
			updateOverall(row_num);
			
		row_num+=1;
	}
}

function pasteTable(jPari, jParj) {
	setxy(jPari, jParj);
	paste_clipboard();
}

// This function is used to paste the content to temporarily container for later use
function paste() {
	if (window.netscape) {
		try {  
			netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");  
		} catch (e) { 
		 	alert('<?=$eReportCard["jsFirefoxNotAllowPaste"]?>');   
		 	return false;    
		}  
		
		var clip = Components.classes['@mozilla.org/widget/clipboard;1'].createInstance(Components.interfaces.nsIClipboard); 
		if (!clip)  return;  
		var trans = Components.classes['@mozilla.org/widget/transferable;1'].createInstance(Components.interfaces.nsITransferable); 
		if (!trans) return;  
		trans.addDataFlavor('text/unicode');  
		
		
		clip.getData(trans,clip.kGlobalClipboard);
		var str = new Object();
		var strLength = new Object();
		trans.getTransferData("text/unicode",str,strLength);
		if(str){
			str = str.value.QueryInterface(Components.interfaces.nsISupportsString);
		}
		if(str){
			pastetxt = str.data.substring(0, strLength.value/2);
			//alert(pastetxt);
		}
		document.getElementById('text1').value = pastetxt;
	} else {
		document.getElementById('text1').value = window.clipboardData.getData('Text');
	}
}

function clear_clipboard(){
	if(window.clipboardData) {						// IE
		var content = window.clipboardData.getData("Text");  
		window.clipboardData.clearData(); 
		document.getElementById('text1').value = "";
	} else {										// FF
		//copy(1);
		document.getElementById('text1').value = "";
	}
	
}


function jDELETE_COLUMN(jColumnID)
{
	obj = document.form1;
	if(confirm("<?=$eReportCard['RemoveSubMarksheetColumn']?>"))
	{
		obj.AssessmentColumnID.value = jColumnID;
		obj.action = "sub_marksheet_remove.php";
		obj.submit();
	}
}

function jEDIT_COLUMN(jColumnID)
{
	obj = document.form1;
//	if(confirm("<?=$eReportCard['RemoveSubMarksheetColumn']?>"))
//	{
		obj.AssessmentColumnID.value = jColumnID;
		obj.action = "sub_marksheet_column_edit.php";
		obj.submit();
//	}
}

function jSUBMIT_FORM(jSubmitType)
{
	obj = document.form1;
	var jCheckResult = checkform(obj);
	if(jCheckResult==true)
	{
		obj.SubmitType.value = jSubmitType;
		obj.action = "sub_marksheet_update.php";
		obj.submit();
	}
}

function jRESET_FORM()
{
	obj = document.form1;
	obj.action = "<?=$PATH_WRT_ROOT?>home/admin/reportcard/marksheet_submission/sub_marksheet.php";
	obj.submit();
}

function checkform(obj)
{
		for(var x=0; x<xno; x++)
		{
			if(jFullMarkArr[x]!='' && jFullMarkArr[x] >0)
			{
				for(var y=0; y<yno; y++)
				{
					if(parseFloat($("#mark\\["+y+"\\]\\["+x+"\\]").val())>parseFloat(jFullMarkArr[x]))
					{
						alert("<?=$eReportCard['jsInputMarkInvalid']?>");
						return false;
					}
				}
			}
		}
	
	/*
	var jStudentList = obj.StudentIDList.value;
	var jStudentIDArray = jStudentList.split(",");

	var jColumnList = obj.ColumnIDList.value;
	var jColumnArray = jColumnList.split(",");
	var jValidResult = false;
	
	for(var i=0; i<jStudentIDArray.length; i++)
	{
		jSID = jStudentIDArray[i];
		for(var j=0; j<jColumnArray.length; j++)
		{
			jCID = jColumnArray[j];
			if (document.getElementById('mark_'+jSID+'_'+jCID) != null) {
				jValue = eval("obj.mark_"+jSID+"_"+jCID+".value");
				if(jValue!="" && jValue!="/" && jValue!="abs")
				{
					jValidResult = jIS_NUMERIC(jValue);
					if(jValidResult==false)
					{
						alert("<?=$eReportCard['jsInputMarkInvalid']?>");
						eval("obj.mark_"+jSID+"_"+jCID+".focus()");
						return false;
					}
				}	
			}		
		}
	}
	*/
	return true;
}

function jsAddAssessment() {
	displayTable("AssessmentForm");
	if (document.all["AssessmentForm"].style.display!="none")
	{
		document.form1.ColumnTitle.focus();
	}
}

function submitNewColumn(obj) {
	if(!check_text(obj.ColumnTitle, "<?= $i_alert_pleasefillin . $eReportCard['ColumnTitle'] ?>.")) return false;
	if(!check_text(obj.ColumnRatio, "<?= $i_alert_pleasefillin . $eReportCard['AssessmentRatio'] ?>.")) return false;
	value = obj.ColumnRatio.value;
	if (value == null || isNaN(value) || value < 0 || !IsNumeric(value))
	{
		alert("<?=  $eReportCard['jsWarnRatioNotANumber'] ?>.");
		return false;
	}
	//if(!check_percentage(obj.ColumnRatio, "<?=  $eReportCard['jsWarnRatioNotANumber'] ?>.")) return false;
	
	if(obj.FullMark != null)
	{
		if(!check_text(obj.FullMark, "<?= $i_alert_pleasefillin . $eReportCard['SchemesFullMark'] ?>.")) return false;
		value = obj.FullMark.value;
		if (value == null || isNaN(value) || value < 0 || !IsNumeric(value))
		{
			alert("<?=  $eReportCard['jsWarnFullMarkNotANumber'] ?>.");
			return false;
		}
	}
	
	if(obj.PassingMark != null)
	{
		if(!check_text(obj.PassingMark, "<?= $i_alert_pleasefillin . $eReportCard['SchemesPassingMark'] ?>.")) return false;
		value = obj.PassingMark.value;
		fullvalue = obj.FullMark.value;
		if (value == null || isNaN(value) || value < 0 || !IsNumeric(value))
		{
			alert("<?=  $eReportCard['jsWarnPassingMarkNotANumber'] ?>.");
			return false;
		}
		if(Number(value) > Number(fullvalue))
		{
			alert("<?=  $eReportCard['jsWarnPassingMarkLargerThanFullMark'] ?>.");
			return false;
		}
	}
	obj.action = "sub_marksheet_new_update.php";
	obj.submit();
}

function updateOverall(row){
	var i = 0;
	var overallMark = 0;
	var markCounter = 0;
	var specialMark = '';
	var totalWeight = 0;
	var fullmark = 1;
	
	var calculationObj = document.getElementById("subMS_Calculation_Formula");
	var calculationMethod = (calculationObj.checked)? "formula" : "average";
	if(<?=$SubjectFullMark? "true" : "false"?>)
		var SubjectFullMark = '<?=$SubjectFullMark?>';
//	if(<?=$SubjectPassingMark? "true" : "false"?>)
//		var SubjectPassingMark = '<?=$SubjectPassingMark?>';
	
	var excludeWeight = 0;
	for (i=0; i<xno; i++)
	{
		var thisID = "mark[" + row + "][" + i + "]";
		var thisMark = document.getElementById(thisID).value;
		thisMark = (isNaN(thisMark) || thisMark=='')? thisMark : parseFloat(thisMark);
		
		if (thisMark == '+')
			thisMark = "0";
		
		if (calculationMethod == "formula")
		{
			if(<?=$ReportCardCustomSchoolName=="munsang_college"?"true":"false"?> || <?= $eRCTemplateSetting['SubMS']['SubMSOverallCalculation']?"true":"false"?>)
			{
				// E89838 - added "/" handling
				if (thisMark == '/') {
					excludeWeight += parseFloat(jWeightArr[i]);
				}
				else {
					thisMark = (isNaN(thisMark))? 0 : thisMark;
					fullmark = jFullMarkArr[i];
					overallMark += (thisMark/fullmark) * jWeightArr[i];
					totalWeight += parseFloat(jWeightArr[i]);
				}
			}
			else if (<?=$useSpecialSymbolAsOverall?> && isNaN(thisMark) && thisMark!='')
			{
				specialMark = thisMark;
				break;
			}
			else
			{
				totalWeight += parseFloat(jWeightArr[i]);
				if (thisMark == '/') {
					excludeWeight += parseFloat(jWeightArr[i]);
				}
				else {
					thisMark = (isNaN(thisMark))? 0 : thisMark;
					overallMark += thisMark * jWeightArr[i];
				}
			}
		}
		else if (calculationMethod == "average")
		{
			if (thisMark=="0" || !isNaN(thisMark) && thisMark!="")
			{
				thisMark = parseFloat(thisMark);
				overallMark += thisMark;
				markCounter++;
			}
			else
			{
				if (<?=$useSpecialSymbolAsOverall?> && thisMark!="")
				{
					specialMark = thisMark;
					break;
				}
			}
		}
	}
	
	if (calculationMethod == "average")
	{
		overallMark = overallMark / markCounter;
	}
	else if(<?=$ReportCardCustomSchoolName=="munsang_college"?"true":"false"?> || <?= $eRCTemplateSetting['SubMS']['SubMSOverallCalculation']?"true":"false"?>)
	{
		overallMark = overallMark / totalWeight * SubjectFullMark;
	}
	else if (calculationMethod == "formula" && excludeWeight > 0) {
		overallMark = overallMark / (totalWeight - excludeWeight);
	}
	
	//convert to 2 decimal places
	if(<?= $eRCTemplateSetting['SubMS']['SubMSOverallCalculation']?"true":"false"?>){
		overallMark = Math.round(overallMark*100)/100;
	} else {
		overallMark = Math.round(overallMark*1000)/1000;
	}
	
	if (isNaN(overallMark)) overallMark = "---";
	
	if (specialMark != '') overallMark = specialMark;
	
	var overallMarkSpanID = "overall[" + row + "]";
	var overallMarkSpan = document.getElementById(overallMarkSpanID);
	overallMarkSpan.innerHTML = overallMark;
	
	var overallHiddenFieldID = "overallHidden[" + row + "]";
	var overallHiddenField = document.getElementById(overallHiddenFieldID);
	overallHiddenField.value = overallMark;
}

function update_calculation_method(calculationMethod)
{
	var i = 0;
	var j = 0;
	var thisRow = 0;
	var thisMarkID = 0;
	var thisMark = 0;
	var overallMark = 0;
	var markCounter = 0;
	
	var calculationObj = document.getElementById("subMS_Calculation_Formula");
	var calculationMethod = (calculationObj.checked)? "formula" : "average";
	
	// do not process if there are no assessments
	if (xno==0) return false;
	
	// loop each student
	for (i=0; i<yno; i++)
	{
		thisRow = i;
		updateOverall(thisRow);
	}
	
	var equationObj = document.getElementById("equation");
	if (calculationMethod == "formula")
		equationObj.innerHTML = "<?= $formulaEquation ?>";
	else
		equationObj.innerHTML = "<?= $averageEquation ?>";
		
		ChangeRatioDisplay()
}

function updateParentColumn()
{
	var overallMarksArr = new Array();
	for (i=0; i<yno; i++)
	{
		var thisID = "overall[" + i + "]";
		var thisMark = document.getElementById(thisID).innerHTML;
		
		overallMarksArr[i] = new Array(jStudentIDArr[i], thisMark); 
	}
	
	<?php 
		if($eRCTemplateSetting['Marksheet']['ManualInputGrade'] && $CurrentFormNumber>3 && empty($WeightAry[$ReportColumnID][$SubjectID])){
			?>
			// do nothing
	<?php
		} 
		else { ?>
				window.opener.updateColumnScore(overallMarksArr, yno, <?=$jsReportColumn?>);
	<?php } ?>
}

function windowClose()
{
	self.close();
	window.opener.alertMSChangedFromSubMS();
}

function ChangeRatioDisplay()
{
	var checked = $("#UsePercent").attr("checked");
	if(checked)
	{
		$("#RatioRemark").html("<?=$eReportCard['AssessmentRatioRemarks2']?>")
		$(".PercentSign").html("%")
		$(".ratio").each(function(){
			$(this).html($(this).attr("orivalue")*100);
		})
	}
	else
	{
		$("#RatioRemark").html("<?=$eReportCard['AssessmentRatioRemarks']?>")
		$(".PercentSign").html("")
		$(".ratio").each(function(){
			$(this).html($(this).attr("orivalue"));
		})
		
	}
}

$().ready(function(){
	if($("#UsePercent").attr("checked"))
		ChangeRatioDisplay()
});
</script>

<form name="form1" action="" method="post">
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td id="top"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td>
		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td align="right" colspan="2"><?= $linterface->GET_SYS_MSG($Result)?></td></tr>
		<tr>
			<td colspan="2">
				<table width='100%' border='0' cellpadding="3" cellspacing="1">
					<tr>
						<td valign="top" width="200px" nowrap="nowrap" class="formfieldtitle tabletext"><?=$FirstRowTitle?></td>
						<td class="tabletext" nowrap="nowrap"><?=$FirstRowDisplay?></td>
					</tr>
					<tr>
						<td valign="top" width="200px" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['Subject']  ?></td>
						<td class="tabletext" nowrap="nowrap"><?=$SubjectName?></td>
					</tr>
					<tr>
						<td valign="top" width="200px" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['ColumnTitle'] ?></td>
						<td class="tabletext" nowrap="nowrap"><?=$ColumnName?></td>
					</tr>
					<tr>
						<td valign="top" width="200px" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['AssessmentRatio'] ?></td>
						<td class="tabletext" nowrap="nowrap"><?=$ColumnWeight?>%</td>
					</tr>
					<tr>
						<td valign="top" width="200px" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['CalculationSettings'] ?></td>
						<td class="tabletext" nowrap="nowrap">
							<input type="radio" name="subMS_Calculation" id="subMS_Calculation_Formula" value="formula" <?=$formula_checked?> onClick="update_calculation_method('formula')">
								<label for="subMS_Calculation_Formula">
									<?= $eReportCard['Formula'] ?>
								</label>
							</input>
							&nbsp;&nbsp;
							<input type="radio" name="subMS_Calculation" id="subMS_Calculation_Average" value="average" <?=$average_checked?> onClick="update_calculation_method('average')">
								<label for="subMS_Calculation_Average">
									<?= $eReportCard['Template']['Average'] ?>
								</label>
							</input>
						</td>
					</tr>
				<?php if(!$currentLevel){?>
					<tr>
						<td valign="top" width="200px" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['TransferOverallMarkToMS'] ?></td>
						<td class="tabletext" nowrap="nowrap">
							<input type="radio" name="subMS_TransferToMS" id="subMS_TransferToMS_Yes" value="1" <?=$transferToMS_yes_checked?> >
								<label for="subMS_TransferToMS_Yes">
									<?= $i_general_yes ?>
								</label>
							</input>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="radio" name="subMS_TransferToMS" id="subMS_TransferToMS_No" value="0" <?=$transferToMS_no_checked?> >
								<label for="subMS_TransferToMS_No">
									<?= $i_general_no ?>
								</label>
							</input>
						</td>
					</tr>
				<?php }?>
					<tr>
						<td valign="top" width="200px" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eReportCard['Use']?>%</td>
						<td class="tabletext" nowrap="nowrap"><?=$UsePercentChkbox?></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td class="dotline" colspan="2"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width=10 height=1></td></tr>
		<?=$x?>
		</table>
	</td>
</tr>
<tr>
	<td>
	<center id="equation" class="tabletext"><?=$equation?></center>
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<tr>
	  <td align="left" valign="bottom"><span class="tabletextremark"><?=$eReportCard['DeletedStudentLegend']?></span></td>
	</tr>
	<?=$ButtonRow?>
	</table>
	</td>
</tr>
</table>
<textarea style="height:1px; width:1px; visibility:hidden;" id="text1" name="text1"></textarea>

<input type="hidden" name="ReportID" value="<?=$ReportID?>" />
<input type="hidden" name="ReportColumnID" value="<?=$ReportColumnID?>" />
<input type="hidden" name="SubjectID" value="<?=$SubjectID?>" />
<input type="hidden" name="ClassID" value="<?=$ClassID?>" />
<input type="hidden" name="SubjectGroupID" value="<?=$SubjectGroupID?>" />
<input type="hidden" name="AssessmentColumnID" value="" />
<input type="hidden" name="ColumnIDList" value="<?=$ColumnIDList?>" />
<input type="hidden" name="StudentIDList" value="<?=$StudentIDList?>" />
<input type="hidden" name="jsReportColumn" value="<?=$jsReportColumn?>" />
<input type="hidden" name="SubReportColumnID" value="<?=$SubReportColumnID?>" />
<input type="hidden" name="currentLevel" value="<?=$currentLevel?>" />
<input type="hidden" name="SubmitType" />

<p></p>
</form>

<script type="text/javascript">
	<? if ($formula_checked) { ?>
		update_calculation_method('formula');
	<? } else { ?>
		update_calculation_method('average');
	<? } ?>
</script>

<?	if ($Result=="update" && $transferToMS_yes_checked!='') { ?>
<script type="text/javascript">
	updateParentColumn();
</script>
<? } ?>

<?		//echo(StopTimer());
		$linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>