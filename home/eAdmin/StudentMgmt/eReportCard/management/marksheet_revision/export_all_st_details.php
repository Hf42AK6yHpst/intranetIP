<?php
# Using: 
/********************************************************
 * Modification log
 * 20201103 Bill    [2020-0915-1830-00164]
 *  - support term setting    ($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings'])
 * 20160427 Bill:	[2016-0330-1524-42164]
 *  - Change xls format
 * 20160204 Bill:	[2015-1209-1137-25164]
 * 	- Create file for marksheet input export
 * ******************************************************/
 
@ini_set('zend.ze1_compatibility_mode', '0');

@SET_TIME_LIMIT(250);
@ini_set(memory_limit, "250M");

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_subjectTopic.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
/** PHPExcel_IOFactory */
include_once($PATH_WRT_ROOT."includes/phpxls/Classes/PHPExcel/IOFactory.php");
include_once($PATH_WRT_ROOT."includes/phpxls/Classes/PHPExcel.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	$lreportcard = new libreportcard();

	if ($lreportcard->hasAccessRight()) {
		# redirect if not allow
		if (!isset($_POST) || !isset($ClassLevelID, $ReportID, $SubjectIDList, $SubjectGroupIDList) || 
					!$eRCTemplateSetting['Settings']['SubjectTopics'] || !$eRCTemplateSetting['Management']['ExportSubjectDetails'])
		{
			header("Location: /");
			exit();
		}
		
		// Initiate
		$xlsHeaderArr = array();
		$xlsContentArr = array();
		
		$libfs = new libfilesystem();
		$lreportcard_topic = new libreportcard_subjectTopic();
		
		# Get Report Info 
		$ReportSetting = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		$isMainReport = $ReportSetting['isMainReport'];
		$isProgressReport = !$isMainReport;
		$SemID = $ReportSetting['Semester'];
        $SemNum = $lreportcard->Get_Semester_Seq_Number($SemID);
        // [2020-0915-1830-00164]
        $targetTermSeq = 0;
        if ($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings'] && $SemID != "F") {
            $targetTermSeq = ($SemNum == 1 || $SemNum == 2) ? 1 : 2;
        }
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		$FormNumber = $lreportcard->GET_FORM_NUMBER($ClassLevelID);
		if (is_numeric($FormNumber)) {
			$FormNumber = intval($FormNumber);
			$isESReport = $FormNumber > 0 && $FormNumber < 6;
			$isMSReport = !$isESReport;
		}
		// KG Report
		else {
			$isKGReport = true;
		}
		
		# Get Report Column
		$column_data = $lreportcard->returnReportTemplateColumnData($ReportID);
		$STReportColumnID = $column_data[0]['ReportColumnID'];
		
		# Get Report Input Info
		$ReportInputComment = true;
		$ReportInputSubjectTopic = $isKGReport || ($isESReport && $isMainReport);
		$ReportInputSubjectScore = $isMSReport && $isMainReport;
		// for 146 testing
		//$ReportInputSubjectTopic = true;
		
		# Class
		//$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		//$ClassNameAssoc = BuildMultiKeyAssoc($ClassArr,array("ClassID"));
		
		# Get Subjects
		$SubjectIDArr = $_POST["SubjectIDList"];
		
		# Get Subject Component
		$FormSubjectArr = $lreportcard->returnSubjectwOrder($ClassLevelID, 1, "", "en", "Desc", 0, $ReportID);
		//$CmpSubjectAssoArr = $lreportcard->GET_COMPONENT_SUBJECT($SubjectIDArr, $ClassLevelID, $ParLang='', $ParDisplayType='Desc', $ReportID, $ReturnAsso=1);
		
		# Get Subject Group
		$SubjectGroupIDArr = $_POST["SubjectGroupIDList"];
		$scm = new subject_class_mapping();
		$SubjectGroupAry = $scm->Get_Subject_Group_List($SemID, "", false);
		$SubjectGroupMapping = BuildMultiKeyAssoc($SubjectGroupAry, array("SubjectID", "SubjectGroupID"));
		
		# Get Subject Group Student List
		$SubjectGroupStudentInfoArr = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupIDArr, $ClassLevelID, "", 1, $isShowStyle=0, 1);
		
		# Get Subject Topic
		if($ReportInputSubjectTopic)
		{
		    // [2020-0915-1830-00164]
            //$SubjectTopicArr = $lreportcard->CHECK_SUBJECT_WITH_SUBJECT_TOPICS($ClassLevelID);
            //$SubjectTopicInfoArr = $lreportcard_topic->getSubjectTopic($ClassLevelID);
			$SubjectTopicArr = $lreportcard->CHECK_SUBJECT_WITH_SUBJECT_TOPICS($ClassLevelID, '', $targetTermSeq);
			$SubjectTopicInfoArr = $lreportcard_topic->getSubjectTopic($ClassLevelID, '', '', $targetTermSeq);
			$SubjectTopicInfoArr = BuildMultiKeyAssoc($SubjectTopicInfoArr, array("SubjectID", "SubjectTopicID"));
		}

		# Get Subject Grading Scheme
		$SubjectGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, "", 0, 1, $ReportID);
		
		# Get Subject Comment
		$SubjectCommentArr = $lreportcard->returnSubjectTeacherCommentByBatch($ReportID, '', $SubjectIDArr);
		
		// loop Subject
		$numOfSubject = count($SubjectIDArr);
		for($i=0 ; $i<$numOfSubject ; $i++)
		{
			// Get Subject Info
			$thisSubjectID = $SubjectIDArr[$i];
			
			// Get Subject Components
			$thisCmpSubjectList = $FormSubjectArr[$thisSubjectID];
			$allSubjectIDList = array_keys((array)$thisCmpSubjectList);
			$allSubjectIDList = array_diff($allSubjectIDList, array(0));
			$allSubjectIDList[] = $thisSubjectID;
			
			// Get Subject Groups
			$thisSubjectGroupArr = $SubjectGroupMapping[$thisSubjectID];
			$thisSubjectGroupIDArr = array_keys((array)$thisSubjectGroupArr);
			
			// check if display parent subject input
			$displayParentScoreInput = true;
			if($ReportInputSubjectScore && count($thisCmpSubjectList) > 1){
				$thisParentSchemeInfo = $lreportcard->GET_GRADING_SCHEME_INFO($SubjectGradingArr[$thisSubjectID]["SchemeID"]);
				$thisParentSchemeType = $thisParentSchemeInfo[0]['SchemeType'];
				$thisParentSchemeInput = $SubjectGradingArr[$thisSubjectID]["ScaleInput"];
				
				// check if parent subject is calculated by componet subject
				$isCalculateByCmpSub = 0;
				for($j=0 ; $j<count($allSubjectIDList) ; $j++){
					$CmpSubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $allSubjectIDList[$j], 0, 0, $ReportID);
					if($allSubjectIDList[$j]!=$thisSubjectID && count($CmpSubjectFormGradingArr) > 0 && $CmpSubjectFormGradingArr['ScaleInput'] == "M")
						$isCalculateByCmpSub = 1;
				}
		
				if(!$isCalculateByCmpSub || ($isCalculateByCmpSub && (($thisParentSchemeType == "H" && $thisParentSchemeInput == "G") || $thisParentSchemeType == "PF"))){
					// do nothing
				}
				else{
					$displayParentScoreInput = false;
				}
			}

			// loop Subject Components for Grading Area
			$rowNum = 0;
			$thisLeftHeader = array();
			foreach ((array)$thisCmpSubjectList as $thisCmpSubjectID => $thisCmpSubjectName)
			{
				// Get Subject Component Info
				$isMainSubject = $thisCmpSubjectID==0;
				$thisCmpSubjectID = $isMainSubject? $thisSubjectID : $thisCmpSubjectID;
				$thisSubjectName = $isMainSubject? $thisCmpSubjectName : $thisCmpSubjectList[0]." - ".$thisCmpSubjectName;
				
				// Subject Topics
				if($ReportInputSubjectTopic && $SubjectTopicArr[$thisCmpSubjectID][1]>0)
				{
					// loop Subject Topics
					$thisSubjectTopicArr = $SubjectTopicInfoArr[$thisCmpSubjectID];
					foreach((array)$thisSubjectTopicArr as $thisSubjectTopicID => $thisSubjectTopicInfo){
						$thisLeftHeader[$rowNum][0] = $thisSubjectName." - ".$thisSubjectTopicInfo["NameEn"];
						$rowNum++;
					}
				}
				// Subject Score
				else if($ReportInputSubjectScore && ($isMainSubject && $displayParentScoreInput || !$isMainSubject))
				{
					$thisLeftHeader[$rowNum][0] = $thisSubjectName." - ".$column_data[0]['ColumnTitle'];
					$rowNum++;
				}
				
				// Comment
				if($ReportInputComment && $isMainSubject)
				{
					$thisLeftHeader[$rowNum][0] = $thisSubjectName." - Comment";
					$rowNum++;
				}
			}
			
			// loop Subject Groups
			$numOfSubjectGroup = count($thisSubjectGroupIDArr);
			for ($j=0; $j<$numOfSubjectGroup; $j++)
			{
				// Initiate
				$xlsSGContent = array();
				
				// Get Subject Group Info
				$thisSubjectGroupID = $thisSubjectGroupIDArr[$j];
				$thisSubjectGroupStudentList = $SubjectGroupStudentInfoArr[$thisSubjectGroupID];
				
				// skip if not in available subject group list
				if(!in_array($thisSubjectGroupID, (array)$SubjectGroupIDArr))
					continue;
			
				// Get Subject Score
				if($ReportInputSubjectScore && ($isMainSubject && $displayParentScoreInput || !$isMainSubject)){
					$thisSubjectGroupScore = $lreportcard->GET_MARKSHEET_INPUT_SCORE($ReportID, $STReportColumnID, Get_Array_By_Key((array)$thisSubjectGroupStudentList, "UserID"), $allSubjectIDList);
				}
				
				// Set xls Header
				$xlsHeaderArr[$thisSubjectGroupID] = array();
				$xlsHeaderArr[$thisSubjectGroupID][] = $thisSubjectGroupArr[$thisSubjectGroupID]["ClassTitleEN"];
				
				// Build xls Content
				$xlsSGContent = $thisLeftHeader;
				
				// loop Subject Group Students
				$numOfSubjectGroupStudent = count($thisSubjectGroupStudentList);
				for ($k=0; $k<$numOfSubjectGroupStudent; $k++) 
				{
					// Get Student Info
					$thisStudentInfo = $thisSubjectGroupStudentList[$k];
					$thisStudentID = $thisStudentInfo["UserID"];
					$thisStudentName = $thisStudentInfo["StudentName"];
					$thisStudentClass = $thisStudentInfo["ClassName"];
					$thisStudentClassNo = $thisStudentInfo["ClassNumber"];
					
					// build xls Header
					$xlsHeaderArr[$thisSubjectGroupID][] = $thisStudentName;
					
					// loop Subject Components
					$rowNum = 0;
					foreach ((array)$thisCmpSubjectList as $thisCmpSubjectID => $thisCmpSubjectName)
					{
						// Get Subject Component Info
						$isMainSubject = $thisCmpSubjectID==0;
						$thisCmpSubjectID = $isMainSubject? $thisSubjectID : $thisCmpSubjectID;
						
						// Subject Topics
						if($ReportInputSubjectTopic && $SubjectTopicArr[$thisCmpSubjectID][1]>0)
						{
							// Get Subject Topic Score
							$thisSubjectTopicArr = $SubjectTopicInfoArr[$thisCmpSubjectID];
							$thisSubjectTopicScore = $lreportcard_topic->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($thisStudentID, $thisCmpSubjectID, $thisSubjectTopicArr, $STReportColumnID, 0, '', 0, 1, $SubjectGradingArr[$thisCmpSubjectID]);
							
							// loop Subject Topics
							foreach((array)$thisSubjectTopicArr as $thisSubjectTopicID => $thisSubjectTopicInfo){
								$thisScore = $thisSubjectTopicScore[$thisSubjectTopicID];
								$xlsSGContent[$rowNum][] = $thisScore["MarkType"]=="M"? $thisScore["MarkRaw"] : $thisScore["DisplayContent"];
								$rowNum++;
							}
						}
						// Subject Score
						else if($ReportInputSubjectScore && ($isMainSubject && $displayParentScoreInput || !$isMainSubject))
						{
							$thisScore = $thisSubjectGroupScore[$thisStudentID][$thisCmpSubjectID][$STReportColumnID];
							if($thisScore["MarkType"]=="PF"){
								$thisPFGrading = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($thisScore["SchemeID"]);
								$xlsSGContent[$rowNum][] = $thisScore["MarkNonNum"]=="P"? $thisPFGrading["Pass"] : $SchemeInfo["Fail"];
							}
							else{
								$xlsSGContent[$rowNum][] = $thisScore["MarkType"]=="SC"? $thisScore["MarkNonNum"] : $thisScore["MarkRaw"];	
							}
							$rowNum++;
						}
						
						// Comment
						if($ReportInputComment && $isMainSubject){
							$xlsSGContent[$rowNum][] = $SubjectCommentArr[$thisCmpSubjectID][$thisStudentID]["Comment"];
							$rowNum++;
						}
					}
				}
				$xlsContentArr[$thisSubjectGroupID] = $xlsSGContent;
			}
		}
		
		# Build xls file content
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("eClass")
									 ->setLastModifiedBy("eClass")
									 ->setTitle("eReportCard Marksheet Input Content")
									 ->setSubject("eReportCard Marksheet")
									 ->setDescription("for eReportcard marksheet checking")
									 ->setKeywords("eReportCard Marksheet")
									 ->setCategory("eClass");
		
		// loop xls data array to write content to xls sheet
		$validSubjectGroupCount = count($xlsHeaderArr);
		if($validSubjectGroupCount>0){
			$xlsSheetCount = 0;
			$SubjectGroupMapping = BuildMultiKeyAssoc($SubjectGroupAry, array("SubjectGroupID"));
			$validSubjectGroupIDAry = array_keys((array)$xlsHeaderArr);
			
			// loop Subject Groups
			$numOfSubjectGroup = count($SubjectGroupIDArr);
			for ($sgCount=0; $sgCount<$numOfSubjectGroup; $sgCount++)
			{
				// Subject Group Info
				$currentSubjectGroupID = $SubjectGroupIDArr[$sgCount];
				
				// skip if subject group not valid
				if(!in_array($currentSubjectGroupID, (array)$validSubjectGroupIDAry)){
					continue;
				}
				
				// Create new sheet for more subject groups
				if($xlsSheetCount!=0){
					$objPHPExcel->createSheet();
				}
				// Set active sheet
				$objPHPExcel->setActiveSheetIndex($xlsSheetCount);
				$ActiveSheet = $objPHPExcel->getActiveSheet();
				// Set active sheet title
				$SheetTitle = $SubjectGroupMapping[$currentSubjectGroupID]["ClassTitleEN"];
				$SheetTitle = str_replace("/", " ", $SheetTitle);
				$SheetTitle = strlen($SheetTitle) > 31? substr($SheetTitle, 0, 31) : $SheetTitle;
				$ActiveSheet->setTitle($SheetTitle);
				
				// Write header
				$currentSheetHeader = $xlsHeaderArr[$currentSubjectGroupID];
				$numOfHeader = count($currentSheetHeader);
				for ($j=0; $j<$numOfHeader; $j++)
				{
					$columnPrefix = ($j < 26)? "" : chr(ord("A")+floor($j/26)-1);
					$ActiveSheet->setCellValue($columnPrefix.chr(ord("A")+($j%26))."1", $currentSheetHeader[$j]);
				}
				
				// Write content
				$currentSheetContent = $xlsContentArr[$currentSubjectGroupID];
				$numOfStudentContent = count($currentSheetContent);
				for($i=0; $i<$numOfStudentContent; $i++)
				{
					$currentStudentContent = $currentSheetContent[$i];
					$numOfStudentData = count($currentStudentContent);
					for($j=0; $j<$numOfStudentData; $j++)
					{
						$columnPrefix = ($j < 26)? "" : chr(ord("A")+floor($j/26)-1);
						
						// Text Format - General (Data Type: Numeric)
						if($j!=1 && trim($currentStudentContent[$j])!="" && is_numeric($currentStudentContent[$j])){
							$ActiveSheet->getCell($columnPrefix.chr(ord("A")+($j%26)).($i+2))->setValueExplicit($currentStudentContent[$j], PHPExcel_Cell_DataType::TYPE_NUMERIC);
						}
						// Text Format - General (Data Type: String) 
						else {
							$ActiveSheet->getCell($columnPrefix.chr(ord("A")+($j%26)).($i+2))->setValueExplicit($currentStudentContent[$j], PHPExcel_Cell_DataType::TYPE_STRING);
						}
					}
				}
				$xlsSheetCount++;
			}
			// Set first sheet active
			$objPHPExcel->setActiveSheetIndex(0);
		}
		
		# Export xls file
		// Prepare xls files folder
		$TempFolder = $intranet_root."/file/temp/erc_export";
		$TempUserFolder = $TempFolder.'/'.$_SESSION['UserID'];
		$TempXlsFolder = $TempUserFolder.'/marksheet_details';
		// Create folder if not exist
		if (!file_exists($TempXlsFolder)) {
			$libfs->folder_new($TempXlsFolder);
		}
		// Clear folder
		else{
			// Omas Clear Temp for last session
			$fileList = array_diff(scandir($TempXlsFolder), array('..','.'));
			if(count($fileList) > 0){
				foreach((array)$fileList as $filename){
					$libfs->file_remove($TempXlsFolder.'/'.$filename);
				}
			}
		}
		// xls file path
		$_xlsFileName = 'erc_marksheet_input_content.xls';
		$path2write = $TempXlsFolder.'/'.$_xlsFileName;
		
		// Save xls file
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save($path2write);
		
		// Output xls file
		output2browser(get_file_content($path2write), $_xlsFileName);
		
	} else {
		echo "You have no priviledge to access this page.";
	}
} else {
	echo "You have no priviledge to access this page.";
}
?>