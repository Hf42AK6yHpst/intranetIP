<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	$lreportcard = new libreportcard();
	
	if ($lreportcard->hasAccessRight()) {
		if (!isset($_GET) || !isset($SubjectID, $ClassLevelID, $ReportID, $ClassID)) {
			header("Location: index.php");
			exit();
		}
		
		// Class
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		for($i=0 ; $i<count($ClassArr) ;$i++) {
			if($ClassArr[$i][0] == $ClassID)
				$ClassName = $ClassArr[$i][1];
		}
		
		// SubjectName
		$SubjectName = $lreportcard->GET_SUBJECT_NAME($SubjectID);
		$SubjectName = str_replace("&nbsp;", "_", $SubjectName);
		
		// Student Info Array
		if ($SubjectGroupID != '')
		{
			$StudentArray = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupID, $ClassLevelID, '', 1);
			
			$obj_SubjectGroup = new subject_term_class($SubjectGroupID);
			$SubjectGroupName = $obj_SubjectGroup->Get_Class_Title();
			
			$Class_SG_Name = $SubjectGroupName;
		}
		else
		{
//			$TaughtStudentArr = $lreportcard->Get_Student_List_Taught_In_This_Consolidated_Report($ReportID, $_SESSION['UserID'], $ClassID);
//			$TaughtStudentList = '';
//			if (count($TaughtStudentArr > 0))
//				$TaughtStudentList = implode(',', $TaughtStudentArr);
//				
//			$StudentArray = $lreportcard->GET_STUDENT_BY_CLASS($ClassID, $TaughtStudentList);
			
			//2014-0224-1422-33184
			$StudentArray = $lreportcard->Get_Marksheet_Accessible_Student_List_By_Subject_And_Class($ReportID, $_SESSION['UserID'], $ClassID, $ParentSubjectID, $SubjectID, $showStyle=false);
			
			//$StudentArray = $lreportcard->GET_STUDENT_BY_CLASS($ClassID);
			$Class_SG_Name = $ClassName;
		}
		$StudentSize = sizeof($StudentArray);
		
		$StudentIDList = "";
		$StudentIDArray = array();
		for($i=0; $i<$StudentSize; $i++)
			$StudentIDArray[] = $StudentArray[$i]['UserID'];
		$StudentIDList = implode(",", $StudentIDArray);
		
		$table = $lreportcard->DBName.".RC_MARKSHEET_COMMENT";
		$NameField = getNameFieldByLang2("");
		$ClassNumField = getClassNumberField("");
		
		$SubjectCond = "SubjectID = '$SubjectID'";
		
		/*
		# Get Student Info
		$sql = "SELECT UserID, WebSAMSRegNo, ClassName, $ClassNumField AS ClassNumber, $NameField AS Name 
						FROM INTRANET_USER 
						WHERE UserID IN ($StudentIDList) 
						ORDER BY $ClassNumField
						";
		$StudentInfo = $lreportcard->returnArray($sql,4);
		*/
		
		//if ($StudentSize == 0) {
		//	header("Location: index.php");
		//	exit();
		//}
		
		if (isset($isTemplate) && $isTemplate = "1") {
			$filename = "comment_template_$Class_SG_Name.csv";
		} else {
			# only display the Comment if it match SubjectID & ReportID
			//$filename = "comment_export_$Class_SG_Name.csv";
			$filename = $SubjectName.'_'.$Class_SG_Name.'_SubjectComment.csv';
		}
		
		# Get Comment and create an associated array
		$sql = "SELECT StudentID, Comment FROM $table WHERE $SubjectCond AND ReportID='$ReportID' AND StudentID IN ($StudentIDList)";
		$CommentInfo = $lreportcard->returnArray($sql,2);
		$CommentInfoAsso = array();
		for($i=0; $i<sizeof($CommentInfo); $i++) {
			$CommentInfoAsso[$CommentInfo[$i]["StudentID"]] = stripslashes($CommentInfo[$i]["Comment"]);
		}
		
		# Create the data array for export
		$ExportArr = array();
		for($i=0; $i<$StudentSize; $i++) {
			$thisStudentID = $StudentArray[$i]["UserID"];
			
			$ExportArr[$i][0] = $StudentArray[$i]["WebSAMSRegNo"];
			$ExportArr[$i][1] = $StudentArray[$i]["ClassName"];
			$ExportArr[$i][2] = $StudentArray[$i]["ClassNumber"];
			$ExportArr[$i][3] = $StudentArray[$i]["StudentName"];
			
			if (isset($CommentInfoAsso[$thisStudentID]) && $isTemplate != "1")
				$ExportArr[$i][4] = $CommentInfoAsso[$thisStudentID];
			else
				$ExportArr[$i][4] = "";
		}
		
		$lexport = new libexporttext();
		
		$exportColumn = array("WebSAMSRegNumber", "Class Name", "Class Number", "Student Name", "Comment Content");
		
		$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn,  "", "\r\n", "", 0, "11",1);
		
		intranet_closedb();
		
		// Output the file to user browser
		$filename = str_replace(' ', '_', $filename);
//		$filename = $lreportcard->Get_File_Name_Encoding($filename);
		$lexport->EXPORT_FILE($filename, $export_content);
	} else {
		echo "You have no priviledge to access this page.";
	}
} else {
	echo "You have no priviledge to access this page.";
}
?>