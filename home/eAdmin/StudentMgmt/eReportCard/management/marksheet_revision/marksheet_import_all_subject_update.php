<?php
// Using By: 

/* Modification Log:
 * 20151012	(Bill)	[2015-0520-1100-36066]
 * 	- Cust - Import Marksheet for All subjects
 * 	- Copy from marksheet_import_update.php
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

#################################################################################################

include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
$lreportcard = new libreportcard2008j();

$limport = new libimporttext();
$linterface = new interface_html();

# Preparation
$params = "ClassLevelID=$ClassLevelID&ReportID=$ReportID&ClassID=$ClassID&SubjectGroupID=$SubjectGroupID&isProgress=$isProgress&isProgressSG=$isProgressSG";

# Get Import data from Temp table
$ImportDataArr = $lreportcard->Get_Import_All_Subject_Score_From_Temp_Table($ReportID);

$lreportcard->Start_Trans();

$LastModifiedUserID = $UserID;
$DateName = "LastMarksheetInput";

$NumOfRec=count($ImportDataArr);
for($i=0; $i<$NumOfRec; $i++)
{
	$currentRecord = $ImportDataArr[$i];
	
//	$RecordUserID = $currentRecord["UserID"];
//	$RecordReportID = $currentRecord["ReportID"];
//	$RecordRowNo = $currentRecord["RowNumber"];
	$RecordStudentID = $currentRecord["StudentID"];
	$RecordSubjectID = $currentRecord["SubjectID"];
	$RecordReportColumnID = $currentRecord["ReportColumnID"];
	$RecordSchemeID = $currentRecord["SchemeID"];
	$RecordMarkType = $currentRecord["MarkType"];
	$RecordMarkRaw = $currentRecord["MarkRaw"];
	$RecordMarkNonNum = $currentRecord["MarkNonNum"];
	$RecordIsEstimated = $currentRecord["IsEstimated"];
	
    $table = $lreportcard->DBName.".RC_MARKSHEET_SCORE";
    
	# Remove all reocrds of student in the subject if exists
	$sql = "DELETE FROM 
				$table 
			WHERE 
				StudentID = '$RecordStudentID' AND 
				SubjectID = '$RecordSubjectID' AND 
				ReportColumnID = '$RecordReportColumnID'
			";
	$result['delete_'.$i] = $lreportcard->db_db_query($sql);
	
	# Insert Raw Result
	$sql = "INSERT INTO $table
				(StudentID, SubjectID, ReportColumnID, SchemeID, 
				 MarkType, MarkRaw, MarkNonNum, LastModifiedUserID, 
				 DateInput, DateModified, IsEstimated) 
			VALUES
				('$RecordStudentID', '$RecordSubjectID', '$RecordReportColumnID', '$RecordSchemeID', 
				 '$RecordMarkType', '$RecordMarkRaw', '$RecordMarkNonNum', '$LastModifiedUserID', 
				 NOW(), NOW(), '$RecordIsEstimated' )
			";
	$result['insert_'.$i] = $lreportcard->db_db_query($sql);
	
	# Update the Time of LastMarksheetInput
	if($result['insert_'.$i]){
		$lreportcard->UPDATE_REPORT_LAST_DATE($ReportID, $DateName);
	}
}
	
//# import overall score
//if($ImportDataArr[$i]["OverallMark"])
//{
//	list($StudentID,$SubjectID,$ReportID,$SchemeID,$MarkType,$MarkNonNum,$IsEstimated) = $ImportDataArr[$i]["OverallMark"];
//
//	$table = $lreportcard->DBName.".RC_MARKSHEET_OVERALL_SCORE";
//	# Remove all reocrds of student in the subject if exists
//	$sql = "DELETE FROM 
//				$table 
//			WHERE 
//				StudentID = '$StudentID' AND 
//				SubjectID = '$SubjectID' AND 
//				ReportID = '$ReportID'
//			";
//			
//	$result['delete_overall_'.$i] = $lreportcard->db_db_query($sql);
//	
//	# Insert Raw Result
//	$sql = "INSERT INTO $table
//				(StudentID, SubjectID, ReportID, SchemeID, 
//				 MarkType, MarkNonNum, LastModifiedUserID, 
//				 DateInput, DateModified, IsEstimated) 
//			VALUES
//				('$StudentID', '$SubjectID', '$ReportID', '$SchemeID', 
//				 '$MarkType', '$MarkNonNum', '$LastModifiedUserID', 
//				 NOW(), NOW(), '$IsEstimated' )
//			";
//			
//	$result['insert_overall_'.$i] = $lreportcard->db_db_query($sql);
//	
//	# Update the Time of LastMarksheetInput
//	if($result['insert_overall_'.$i])
//		$lreportcard->UPDATE_REPORT_LAST_DATE($ReportID, $DateName);
//}

if(!in_array(false, $result)) {
	$lreportcard->Commit_Trans();
	
	$totalCount = $ImportDataArr[($NumOfRec-1)]["RowNumber"] + 1;
	$SuccessMsg = $totalCount." ".$Lang['General']['ImportArr']['RecordsImportedSuccessfully'];
} else {
	$lreportcard->RollBack_Trans(); 
	$SuccessMsg = $eReportCard['FailToImportMarksheetScore'];
}

# tag information
$TAGS_OBJ[] = array($eReportCard['Management_MarksheetRevision'], "", 0);

# page navigation (leave the array empty if no need)
$PAGE_NAVIGATION[] = array($eReportCard['ImportMarks'], "");

if(trim($SubjectGroupID)=='' && trim($ClassID)=='')
{
	$BackUrl ="marksheet_revision.php";
	$params .= "&SubjectID=".((isset($ParentSubjectID) && $ParentSubjectID != "") ?"$ParentSubjectID":"$SubjectID");
}
else
{
	$BackUrl = "marksheet_edit.php";
	$params .= "&SubjectID=$SubjectID";
	$params .= (isset($ParentSubjectID) && $ParentSubjectID != "") ? "&ParentSubjectID=$ParentSubjectID" : "";
	
}
$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='$BackUrl?$params'","back");

$CurrentPage = "Management_MarkSheetRevision";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
							
?>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td colspan="2"><?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?><br><br></td>
	</tr>
	<tr>
		<td colspan="2"><?= $linterface->GET_IMPORT_STEPS(3) ?></td>
	</tr>
	<tr>
		<td class='tabletext' align='center'><br><?=$SuccessMsg?><br></td>
	</tr>
	<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<tr>
		<td colspan="2" align="center"><?=$BackBtn?></td>
	</tr>
</table>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>