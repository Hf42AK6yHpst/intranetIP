<?php
// Editing: 
/*
 * Modification Log:
 * 20201103 (Bill)  [2020-0915-1830-00164]
 * - support term setting    ($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings'])
 * 20160427 (Bill)	[2016-0330-1524-42164]
 * - fill in NA if subject topic score empty
 */
 
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");

intranet_auth();
intranet_opendb();

#################################################################################################

$lreportcard = new libreportcard2008j();
//$lreportcard = new libreportcard();

# Initialization
$result = array();

# Get Data - $_POST 
$SubjectGroupIDArr = (isset($SubjectGroupIDList) && !empty($SubjectGroupIDList)) ? $SubjectGroupIDList : array();
$SubjectIDArr = (isset($SubjectIDList) && !empty($SubjectIDList)) ? $SubjectIDList : array();

/*
// [2016-0330-1524-42164] get subject topics
if($eRCTemplateSetting['Settings']['SubjectTopics']){
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_subjectTopic.php");
	$lreportcard_ST = new libreportcard_subjectTopic();
	$SubjectTopicList = $lreportcard_ST->getSubjectTopic($ClassLevelID);
	$SubjectTopicList = BuildMultiKeyAssoc((array)$SubjectTopicList, array("SubjectID", "SubjectTopicID"));
}
*/

# Get Data Either Assessment Column(s) in Term Or Term Column(s) in Wholely Report	
// Loading Report Template Data
$basic_data = $lreportcard->returnReportTemplateBasicInfo($ReportID);			// Basic  Data
$currentSem = $basic_data['Semester'];
$currentSemNum = $lreportcard->Get_Semester_Seq_Number($currentSem);
$ReportType = $currentSem == "F" ? "W" : "T";
$column_data = $lreportcard->returnReportTemplateColumnData($ReportID); 		// Column Data
$ColumnSize = sizeof($column_data);

// [2020-0915-1830-00164]
$targetTermSeq = 0;
if ($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings'] && $ReportType == "T") {
    $targetTermSeq = ($currentSemNum == 1 || $currentSemNum == 2) ? 1 : 2;
}

// [2016-0330-1524-42164] get subject topics
if($eRCTemplateSetting['Settings']['SubjectTopics'])
{
    include_once($PATH_WRT_ROOT."includes/libreportcard2008_subjectTopic.php");
    $lreportcard_ST = new libreportcard_subjectTopic();

    //$SubjectTopicList = $lreportcard_ST->getSubjectTopic($ClassLevelID);
    $SubjectTopicList = $lreportcard_ST->getSubjectTopic($ClassLevelID, '', '', $targetTermSeq);
    $SubjectTopicList = BuildMultiKeyAssoc((array)$SubjectTopicList, array("SubjectID", "SubjectTopicID"));
}

if(count($SubjectGroupIDArr) > 0 && count($SubjectIDArr) > 0){
	for($i=0 ; $i<count($SubjectGroupIDArr) ; $i++){
		$thisSubjectGroupID = $SubjectGroupIDArr[$i];
		# Get Student Info 
		$StudentArr = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($thisSubjectGroupID, $ClassLevelID);
		$StudentSize = sizeof($StudentArr);
		$StudentIDArr = array();
		
		if($StudentSize > 0){
			for($j=0 ; $j<$StudentSize ; $j++){
				$StudentIDArr[$j] = $StudentArr[$j]['UserID'];
			}
				
			# Main
			for($j=0 ; $j<count($SubjectIDArr) ; $j++){
				// check if it is a parent subject, if yes find info of its components subjects
				$CmpSubjectArr = array();
				$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($SubjectIDArr[$j], $ClassLevelID);
				$CmpSubjectIDArr = array();
				if(!empty($CmpSubjectArr)){
					for($p=0 ; $p<count($CmpSubjectArr) ; $p++){
						for($q=0; $q<$ColumnSize ; $q++) {
							$ReportColumnID = $column_data[$q]['ReportColumnID'];
							
							# Update - fill in NA in empty scores
							$result['fill_na_'.$i.'_'.$j.'_'.$p.'_'.$q] = $lreportcard->FILL_EMPTY_SCORE_WITH_NA($StudentIDArr, $CmpSubjectArr[$p]['SubjectID'], $ReportColumnID, 0, $ClassLevelID);
							
							# [2016-0330-1524-42164] Update - fill in NA in empty subject topic scores
							if($eRCTemplateSetting['Settings']['SubjectTopics'] && $currentSem != "F"){
								$currentSubjectTopics = $SubjectTopicList[$CmpSubjectArr[$p]['SubjectID']];
								if(is_array($currentSubjectTopics) && count($currentSubjectTopics)>0){
									$result['fill_topic_na_'.$i.'_'.$j.'_'.$p.'_'.$q] = $lreportcard->FILL_EMPTY_TOPIC_SCORE_WITH_NA($StudentIDArr, $CmpSubjectArr[$p]['SubjectID'], $ReportColumnID, $ClassLevelID, $currentSubjectTopics);
								}
							}
						}
						
						# Get Scheme Details & Subject Grading Details
						$SchemeType = '';
						$ScaleInput = '';
						$SubjectFormGradingArr = array();
						$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $CmpSubjectArr[$p]['SubjectID'],0,0,$ReportID);
						if(count($SubjectFormGradingArr) > 0){
							$SchemeID = $SubjectFormGradingArr['SchemeID'];
							$ScaleInput = $SubjectFormGradingArr['ScaleInput'];
							$SchemeMainInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
							$SchemeType = $SchemeMainInfo['SchemeType'];
						}
						
						# Update 
						if(($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF"){
							# Update - fill in NA in empty overall scores
							$result['fill_na_'.$i.'_'.$j.'_'.$p] = $lreportcard->FILL_EMPTY_SCORE_WITH_NA($StudentIDArr, $CmpSubjectArr[$p]['SubjectID'], $ReportID, 1, $ClassLevelID);
						}
						
						$result['update_cmp_subj_submission_progress_'.$i.'_'.$j.'_'.$p] = $lreportcard->UPDATE_RELATED_SUBJECT_SUBMISSION_PROGRESS($ReportID, $ClassLevelID, '', $CmpSubjectArr[$p]['SubjectID'], 1, $SubjectIDArr[$j], $thisSubjectGroupID);
					}
				}
				
				for($k=0 ; $k<$ColumnSize ; $k++){
					$ReportColumnID = $column_data[$k]['ReportColumnID'];
					
					# Update - fill in NA in empty scores
					$result['fill_na_'.$i.'_'.$j.'_'.$k] = $lreportcard->FILL_EMPTY_SCORE_WITH_NA($StudentIDArr, $SubjectIDArr[$j], $ReportColumnID, 0, $ClassLevelID);
							
					# [2016-0330-1524-42164] Update - fill in NA in empty subject topic scores
					if($eRCTemplateSetting['Settings']['SubjectTopics'] && $currentSem != "F"){
						$currentSubjectTopics = $SubjectTopicList[$SubjectIDArr[$j]];
						if(is_array($currentSubjectTopics) && count($currentSubjectTopics)>0){
							$result['fill_topic_na_'.$i.'_'.$j.'_'.$k] = $lreportcard->FILL_EMPTY_TOPIC_SCORE_WITH_NA($StudentIDArr, $SubjectIDArr[$j], $ReportColumnID, $ClassLevelID, $currentSubjectTopics);
						}
					}
				}
				
				# Get Scheme Details & Subject Grading Details
				$SchemeType = '';
				$ScaleInput = '';
				$SubjectFormGradingArr = array();
				$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectIDArr[$j],0,0,$ReportID);
				if(count($SubjectFormGradingArr) > 0){
					$SchemeID = $SubjectFormGradingArr['SchemeID'];
					$ScaleInput = $SubjectFormGradingArr['ScaleInput'];
					
					$SchemeMainInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
					$SchemeType = $SchemeMainInfo['SchemeType'];
				}
				
				#Update 
				if(($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF"){
					# Update - fill in NA in empty overall scores
					$result['fill_na_'.$i.'_'.$j] = $lreportcard->FILL_EMPTY_SCORE_WITH_NA($StudentIDArr, $SubjectIDArr[$j], $ReportID, 1, $ClassLevelID);
				}
				
				$result['update_subj_submission_progress_'.$i.'_'.$j] = $lreportcard->UPDATE_RELATED_SUBJECT_SUBMISSION_PROGRESS($ReportID, $ClassLevelID, '', $SubjectIDArr[$j], 1, '', $thisSubjectGroupID);
			}
		}
	}
}

# Update the LastMarksheetInput Time if any changes in This Report Template marksheet
if(in_array(true, $result)){
	$DateName = "LastMarksheetInput";
	$lreportcard->UPDATE_REPORT_LAST_DATE($ReportID, $DateName);
}


if(!in_array(false, $result))
	$msg = 0;
else 
	$msg = 1;

#################################################################################################
intranet_closedb();


if($isProgress){
	$url = "../progress/submission.php?ClassLevelID=$ClassLevelID&ReportID=$ReportID";
}
else {
	$params = "ClassLevelID=$ClassLevelID&ReportID=$ReportID&SubjectID=$SubjectID";
	if($MarksheetRevision == 2){	// Back to Marksheet Revision of Subject having Component subjects
		$params .= "&ClassID=$ClassID";
		$url = "marksheet_revision2_subject_group.php?$params";
	}
	else {	// Back to Marksheet Revision of Subject without Component subjects
		$url = "marksheet_revision_subject_group.php?$params";
	}
}

header("Location: $url");

?>