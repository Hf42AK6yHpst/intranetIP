<?php
/*
 *  change log:
 *  Date:	2017-01-24 Villa	Skip update level 0
 *  Date:	2016-01-05 Villa	update upper submark sheet layer 
 */

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
}

$libreportcard = new libreportcard();

if ($libreportcard->hasAccessRight())
{
// 	$param = "SubjectID=$SubjectID&ClassID=$ClassID&ReportColumnID=$ReportColumnID&ReportID=$ReportID&jsReportColumn=$jsReportColumn&SubjectGroupID=$SubjectGroupID&UsePercent=$UsePercent";
	$param = "SubjectID=$SubjectID&ClassID=$ClassID&SchoolCode=$SchoolCode&ReportColumnID=$ReportColumnID&ReportID=$ReportID&jsReportColumn=$jsReportColumn&SubjectGroupID=$SubjectGroupID&SubReportColumnID=$SubReportColumnID&currentLevel=$currentLevel&UsePercent=$UsePercent";
	
	# Get student with ClassName and ClassNumber
	if ($SubjectGroupID != '')
		$StudentClassArray = $libreportcard->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupID, $ClassLevelID, '', 1);
	else
		$StudentClassArray = $libreportcard->GET_STUDENT_BY_CLASS($ClassID, $StudentIDList);

	$ColumnArray = $libreportcard->GET_ALL_SUB_MARKSHEET_COLUMN($ReportColumnID, $SubjectID,$currentLevel,$SubReportColumnID);
	$ColumnSize = sizeof($ColumnArray);
	
	if(!empty($StudentClassArray) && !empty($ColumnArray))
	{
		$Year = $libreportcard->Year;
		$Semester = $libreportcard->Semester;

		# Remove Old record
		$sql = "DELETE FROM RC_SUB_MARKSHEET_SCORE WHERE ReportColumnID = '$ReportColumnID' AND SubjectID = '$SubjectID' AND StudentID IN ($StudentIDList)";
		$libreportcard->db_db_query($sql);


		for ($kk=0; $kk<sizeof($StudentClassArray); $kk++)
		{
			$StudentID = $StudentClassArray[$kk]["UserID"];

			$TotalMark = 0;
			$TotalWeight = 0;
			for($j=0; $j<$ColumnSize; $j++)
			{
				list($ColumnID, $ColumnTitle, $Weight) = $ColumnArray[$j];

				//$mark = ${"mark_".$StudentID."_".$ColumnID};
				$long_value = ${"mark_long_".$StudentID."_".$ColumnID};
				$short_value = ${"mark_".$StudentID."_".$ColumnID};
				$mark = ($long_value==$short_value || ($long_value>0 && $short_value>0 && round($long_value, 1)==$short_value)) ? $long_value : $short_value;
				$mark = trim($mark);
				/*
				if($mark=="abs")
					$mark = "-1";
				else if(strcmp($mark, "/")==0 || $mark==="")
					$mark = "-2";
				else
					$TotalMark = $TotalMark + ($mark*$Weight/100);
				
				if($mark!="-2")
					$TotalWeight = $TotalWeight + $Weight;
				*/
				
				if ($mark == '')
					$mark = 'NULL';
				else if (!is_numeric($mark))
					$mark = 0 - array_search($mark, $libreportcard->specialCasesSet1);
				
				# Insert result
				$columnInfo["ColumnID"] = $ColumnID;
				$columnInfo["StudentID"] = $StudentID;
				$columnInfo["MarkRaw"] = $mark;
				$columnInfo["TeacherID"] = $UserID;
				$success['Mark'][$ColumnID][$StudentID] = $libreportcard->INSERT_SUB_MARKSHEET_SCORE($columnInfo);
			}
			
			// update upper layer
			if($SubReportColumnID){
				$columnInfo["ColumnID"] = $SubReportColumnID;
				$columnInfo["StudentID"] = $StudentID;
				$columnInfo["MarkRaw"] = ${"overall_".$StudentID};
				$columnInfo["TeacherID"] = $UserID;
	// 			if(${"overall_".$StudentID}!='---'){
				$success['Mark'][$SubReportColumnID][$StudentID] = $libreportcard->INSERT_SUB_MARKSHEET_SCORE($columnInfo);
			}
// 			}

			/* Do not insert overall score
			# Insert overall result
			if($SubmitType==0) {
				$OverallResult = ($TotalWeight>0) ? ($TotalMark*100/$TotalWeight) : "-2";
			}
			else {
				$overall_long_value = ${"overall_long_".$StudentID};
				$overall_short_value = ${"overall_".$StudentID};
				$OverallResult = ($overall_long_value==$overall_short_value || ($overall_long_value>0 && $overall_short_value>0 && round($overall_long_value, 1)==$overall_short_value)) ? $overall_long_value : $overall_short_value;

				if($OverallResult=="abs")
					$OverallResult = "-1";
				else if(strcmp($OverallResult, "/")==0 || $OverallResult==="")
					$OverallResult = "-2";
			}
			
			$StudentIDArr = array($StudentID);
			$marksheetScore = $libreportcard->GET_MARKSHEET_SCORE($StudentIDArr, $SubjectID, $ReportColumnID);
			# data for update/insert
			$MarksheetScoreArr = array();
			$MarksheetScoreArr[0]['SchemeID'] = $marksheetScore[$StudentID]['SchemeID'];
			$MarksheetScoreArr[0]['MarkType'] = $marksheetScore[$StudentID]['MarkType'];
			$MarksheetScoreArr[0]['MarkRaw'] = $OverallResult;
			$MarksheetScoreArr[0]['LastModifiedUserID'] = $UserID;
			$MarksheetScoreArr[0]['MarksheetScoreID'] = $marksheetScore[$StudentID]['MarksheetScoreID'];
			$MarksheetScoreArr[0]['StudentID'] = $StudentID;
			$MarksheetScoreArr[0]['SubjectID'] = $SubjectID;
			$MarksheetScoreArr[0]['ReportColumnID'] = $ReportColumnID;
			
			if ($marksheetScore!=NULL)
			{
				# update
				$success = $libreportcard->UPDATE_MARKSHEET_SCORE($MarksheetScoreArr);
				
			}
			else
			{
				# insert	
				$success = $libreportcard->INSERT_MARKSHEET_SCORE($StudentIDArr, $SubjectID, $ReportColumnID, $UserID);
				$MarksheetScoreArr[0]['MarksheetScoreID'] = "";
				$success = $libreportcard->UPDATE_MARKSHEET_SCORE($MarksheetScoreArr);
			}
			*/
		}
	}
	
	## update calculation method
	$subMS_Calculation = $_POST['subMS_Calculation'];
	$Calculation = ($subMS_Calculation=="average")? "A" : "F";
	$TransferToMS = $subMS_TransferToMS;
	
	for($j=0; $j<$ColumnSize; $j++)
	{
		$ColumnID = $ColumnArray[$j]['ColumnID'];
		$success['Calculation'][$ColumnID] = $libreportcard->UPDATE_SUB_MARKSHEET_COLUMN_OVERALL_INFO($ColumnID, $Calculation, $TransferToMS);
	}
	
	intranet_closedb();
	
	$Result = (in_array("0", $success)) ? "update_failed" : "update";
	header("Location:sub_marksheet.php?$param&Result=$Result");
}
?>