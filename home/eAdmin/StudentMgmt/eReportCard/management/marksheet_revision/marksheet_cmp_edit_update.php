<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");

intranet_auth();
intranet_opendb();

#################################################################################################

$lreportcard = new libreportcard2008j();
//$lreportcard = new libreportcard();

include($PATH_WRT_ROOT."includes/eRCConfig.php");

# Get Data
$StudentIDArr = $StudentID;
$LastModifiedUserID = $_SESSION['UserID'];
$ReportColumnIDArr = $ReportColumnID;
$CmpSubjectIDArr = $CmpSubjectID;
$SubjectGroupID = $SubjectGroupID;

$DateName = "LastMarksheetInput";

# Initialization
$result = array();
$MarksheetScoreArr = array();
$MarksheetOverallScoreArr = array();

$isCalculateByCmpSub = 0;
$isCmpSubject = $lreportcard->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($SubjectID);
if(!$isCmpSubject){
	$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
	
	$CmpSubjectIDArr = array();
	if(!empty($CmpSubjectArr)){	
		for($i=0 ; $i<count($CmpSubjectArr) ; $i++){
			$CmpSubjectIDArr[] = $CmpSubjectArr[$i]['SubjectID'];
			
			$CmpSubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $CmpSubjectArr[$i]['SubjectID'],0,0,$ReportID);
			if(count($CmpSubjectFormGradingArr) > 0 && $CmpSubjectFormGradingArr['ScaleInput'] == "M")
				$isCalculateByCmpSub = 1;
				
		}
	}
}
$isAllCmpSubjectWeightZeroArr = $lreportcard->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);

# Get Data
if(count($CmpSubjectIDArr) > 0 && count($ReportColumnIDArr) > 0 && count($StudentIDArr) > 0){
	# Loop Component Subjects
	for($i=0 ; $i<count($CmpSubjectIDArr) ; $i++){
		$thisComponentSubjectID = $CmpSubjectIDArr[$i];
		
		# Get Subject Form Grading Info  
		$SubjectFormGradingArr = array();
		$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $thisComponentSubjectID,0,0,$ReportID);
		$SchemeID = $SubjectFormGradingArr['SchemeID'];
		$ScaleInput = $SubjectFormGradingArr['ScaleInput'];
		
		# Get Grading Scheme Info
		$SchemeInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
		$SchemeType = $SchemeInfo['SchemeType'];
		
		# Loop Report Column
		$count = 0;
		for($j=0 ; $j<count($ReportColumnIDArr) ; $j++){
			# Loop Student
			for($k=0 ; $k<count($StudentIDArr) ; $k++){
				$MarkNonNum = '';
				$MarkRaw = '-1';
				$RawData = ${"mark_".$thisComponentSubjectID."_".$ReportColumnIDArr[$j]."_".$StudentIDArr[$k]};
				
				$IsEstimated = ($lreportcard->Is_Estimated_Score($RawData))? 1 : 0;
				$RawData = $lreportcard->Convert_Marksheet_UIScore_To_DBRawData($RawData);
				
				$isSpecialCase = ($SchemeType == "PF") ? $lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2($RawData) : $lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1($RawData);
				
				
				// Set the MarkType 
				if($SchemeType == "H"){
					if($ScaleInput == "G"){
						$MarkType = "G";
						$MarkNonNum = $RawData;
					}
					else {
						$MarkType = "M";
						if($isSpecialCase)
							$MarkRaw = ($RawData == "+") ? 0 : -1;
						else 
							$MarkRaw = ($RawData != "" ) ? $lreportcard->ROUND_MARK($RawData, 'SubjectScore') : -1;
					}
				}
				else if($SchemeType == "PF"){
					$MarkType = "PF";
					$MarkNonNum = $RawData;
				}
				else {
					$MarkType = "M";
					$MarkRaw = $RawData;
				}
				
				$MarksheetScoreArr[$thisComponentSubjectID][$count]["MarksheetScoreID"] = (isset(${"MarksheetScoreID_".$StudentIDArr[$k]})) ? ${"MarksheetScoreID_".$StudentIDArr[$k]} : "";
				$MarksheetScoreArr[$thisComponentSubjectID][$count]['StudentID'] = $StudentIDArr[$k];
				$MarksheetScoreArr[$thisComponentSubjectID][$count]['SubjectID'] = $thisComponentSubjectID;
				$MarksheetScoreArr[$thisComponentSubjectID][$count]['ReportColumnID'] = $ReportColumnIDArr[$j];				
				$MarksheetScoreArr[$thisComponentSubjectID][$count]['SchemeID'] = $SchemeID;
				$MarksheetScoreArr[$thisComponentSubjectID][$count]['MarkType'] = ($isSpecialCase) ? "SC" : $MarkType;
				$MarksheetScoreArr[$thisComponentSubjectID][$count]['MarkRaw'] = trim($MarkRaw);
				$MarksheetScoreArr[$thisComponentSubjectID][$count]['MarkNonNum'] = ($isSpecialCase) ? trim($RawData) : trim($MarkNonNum);
				$MarksheetScoreArr[$thisComponentSubjectID][$count]["LastModifiedUserID"] = $LastModifiedUserID;
				$MarksheetScoreArr[$thisComponentSubjectID][$count]["IsEstimated"] = $IsEstimated;
				
				$count++;
			}
			
		}
		
		
		# Get Overall Subject Mark
		if(($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF"){
			for($k=0 ; $k<count($StudentIDArr) ; $k++){
				$MarkNonNum = '';
				$RawData = (isset(${"overall_mark_".$thisComponentSubjectID."_".$StudentIDArr[$k]})) ? ${"overall_mark_".$thisComponentSubjectID."_".$StudentIDArr[$k]} : "";
				
				$IsEstimated = ($lreportcard->Is_Estimated_Score($RawData))? 1 : 0;
				$RawData = $lreportcard->Convert_Marksheet_UIScore_To_DBRawData($RawData);
				
				$isSpecialCase = ($SchemeType == "PF") ? $lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2($RawData) : $lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1($RawData);
				$MarkType = ($SchemeType == "PF") ? "PF" : "G";
				
				$MarksheetOverallScoreArr[$thisComponentSubjectID][$k]["MarksheetOverallScoreID"] = (isset(${"MarksheetOverallScoreID_".$thisComponentSubjectID."_".$StudentIDArr[$k]})) ? ${"MarksheetOverallScoreID_".$thisComponentSubjectID."_".$StudentIDArr[$k]} : "";
				$MarksheetOverallScoreArr[$thisComponentSubjectID][$k]['StudentID'] = $StudentIDArr[$k];
				$MarksheetOverallScoreArr[$thisComponentSubjectID][$k]['SubjectID'] = $thisComponentSubjectID;
				$MarksheetOverallScoreArr[$thisComponentSubjectID][$k]['ReportID'] = $ReportID;				
				$MarksheetOverallScoreArr[$thisComponentSubjectID][$k]['SchemeID'] = $SchemeID;
				$MarksheetOverallScoreArr[$thisComponentSubjectID][$k]['MarkType'] = ($isSpecialCase) ? "SC" : $MarkType;
				$MarksheetOverallScoreArr[$thisComponentSubjectID][$k]['MarkNonNum'] = trim($RawData);
				$MarksheetOverallScoreArr[$thisComponentSubjectID][$k]["LastModifiedUserID"] = $LastModifiedUserID;
				$MarksheetOverallScoreArr[$thisComponentSubjectID][$k]["IsEstimated"] = $IsEstimated;
			}
		}
		
		
		# Get Complete Status of each Component Subject
		$isCmpSubjectComplete[$thisComponentSubjectID] = (isset(${"isCmpSubjectComplete_".$thisComponentSubjectID}) ) ? ${"isCmpSubjectComplete_".$thisComponentSubjectID} : 0;
	}
}

# Main
# INSERT - Insert New Record of Students corresponding to Certain Report Column to the MarkScore Table
if(count($CmpSubjectIDArr) > 0 && count($ReportColumnIDArr) > 0 && count($StudentIDArr) > 0){
	for($i=0 ; $i<count($CmpSubjectIDArr) ; $i++){
		$thisComponentSubjectID = $CmpSubjectIDArr[$i];
		
		# INSERT - Insert New Record of Students corresponding to Certain Report Column to the MarkScore Table
		# Case 1: when it is term report
		# Case 2: when it is whole year report and the particular term report column has not defined term report template
		for($j=0 ; $j<count($ReportColumnIDArr) ; $j++){
			$result['insert_'.$i.'_'.$j] = $lreportcard->INSERT_MARKSHEET_SCORE($StudentIDArr, $thisComponentSubjectID, $ReportColumnIDArr[$j], $LastModifiedUserID);
		}
		
		# INSERT - Insert New Record of Students in Marksheet OVERALL Score for the cases 
		# when the scheme type is Honor-Based (H) AND its scaleinput is Grade(G) OR
		# when the scheme type is PassFail-Based (PF)
		if(count($MarksheetOverallScoreArr) > 0 && isset($MarksheetOverallScoreArr[$thisComponentSubjectID]) ){
			$result['insert_overall_mark_'.$thisComponentSubjectID] = $lreportcard->INSERT_MARKSHEET_OVERALL_SCORE($StudentIDArr, $thisComponentSubjectID, $ReportID, $LastModifiedUserID);
		}
	}
}

# UPDATE - Score
if(count($MarksheetScoreArr) > 0){
	for($i=0 ; $i<count($CmpSubjectIDArr) ; $i++){
		$thisComponentSubjectID = $CmpSubjectIDArr[$i];
		
		if(isset($MarksheetScoreArr[$thisComponentSubjectID]) ){
			# Update Record of Marksheet Score
			$result['update_marksheet_'.$thisComponentSubjectID] = $lreportcard->UPDATE_MARKSHEET_SCORE($MarksheetScoreArr[$thisComponentSubjectID]);
		}
	
		if($result['update_marksheet_'.$thisComponentSubjectID]){
			# Update the Time of LastMarksheetInput
			$result['update_report_lastdate'] = $lreportcard->UPDATE_REPORT_LAST_DATE($ReportID, $DateName);
			
			# Update the Complete Status of Component Subject
			if($isCmpSubjectComplete[$thisComponentSubjectID]){
				$result['update_submission_progress_'.$thisComponentSubjectID] = $lreportcard->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($thisComponentSubjectID, $ReportID, $ClassID, 0, $SubjectGroupID);
			}
		}
	}
	
	# Added on 20080819 by Andy
	# Update the Complete Status of Parent Subject
	$result['update_subj_submission_progress'] = $lreportcard->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, $ClassID, 0, $SubjectGroupID);
}

# UPDATE - Overall Score
if(count($MarksheetOverallScoreArr) > 0){	
	for($i=0 ; $i<count($CmpSubjectIDArr) ; $i++){
		$thisComponentSubjectID = $CmpSubjectIDArr[$i];
		
		if(isset($MarksheetOverallScoreArr[$thisComponentSubjectID]) ){
			# Update Record of Marksheet OVERALL Score if any 
			$result['update_marksheet_overall_'.$thisComponentSubjectID] = $lreportcard->UPDATE_MARKSHEET_OVERALL_SCORE($MarksheetOverallScoreArr[$thisComponentSubjectID]);
		}
		
		if($result['update_marksheet_overall_'.$thisComponentSubjectID]){
			# Update the Time of LastMarksheetInput
			$result['update_report_lastdate'] = $lreportcard->UPDATE_REPORT_LAST_DATE($ReportID, $DateName);
			
			
			# Update the Complete Status of Component Subject
			if($isCmpSubjectComplete[$thisComponentSubjectID]){
				$result['update_submission_progress_'.$thisComponentSubjectID] = $lreportcard->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($thisComponentSubjectID, $ReportID, $ClassID, 0, $SubjectGroupID);
			}
		}
	}
	
	# Added on 20080819 by Andy
	# Update the Complete Status of Parent Subject
	$result['update_subj_submission_progress'] = $lreportcard->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, $ClassID, 0, $SubjectGroupID);
}


if ($isCalculateByCmpSub)
{
	# Get Subject Form Grading Info  
	$SubjectFormGradingArr = array();
	$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0,0,$ReportID);
	$SchemeID = $SubjectFormGradingArr['SchemeID'];
	$ScaleInput = $SubjectFormGradingArr['ScaleInput'];
	
	# Get Grading Scheme Info
	$SchemeInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
	$SchemeType = $SchemeInfo['SchemeType'];
	
	$count=0;
	for($j=0 ; $j<count($ReportColumnIDArr) ; $j++){
		$thisReportColumnID = $ReportColumnIDArr[$j];
		
		if ($isAllCmpSubjectWeightZeroArr[$thisReportColumnID] || $eRCTemplateSetting['ManualInputParentSubjectMark'])
		{
			for($k=0 ; $k<count($StudentIDArr) ; $k++){
				$thisStudentID = $StudentIDArr[$k];
				
				$MarkNonNum = '';
				$MarkRaw = '-1';
				$RawData = ${"sub_overall_mark_".$SubjectID."_".$ReportColumnIDArr[$j]."_".$StudentIDArr[$k]};
				
				$IsEstimated = ($lreportcard->Is_Estimated_Score($RawData))? 1 : 0;
				$RawData = $lreportcard->Convert_Marksheet_UIScore_To_DBRawData($RawData);
				
				$isSpecialCase = ($SchemeType == "PF") ? $lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2($RawData) : $lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1($RawData);
				
				// Set the MarkType 
				if($SchemeType == "H"){
					if($ScaleInput == "G"){
						$MarkType = "G";
						$MarkNonNum = $RawData;
					}
					else {
						$MarkType = "M";
						if($isSpecialCase)
							$MarkRaw = ($RawData == "+") ? 0 : -1;
						else 
							$MarkRaw = ($RawData != "" ) ? $lreportcard->ROUND_MARK($RawData, 'SubjectScore') : -1;
					}
				}
				else if($SchemeType == "PF"){
					$MarkType = "PF";
					$MarkNonNum = $RawData;
				}
				else {
					$MarkType = "M";
					$MarkRaw = $RawData;
				}
				
				$MarksheetSubOverallScoreArr[$SubjectID][$count]["MarksheetScoreID"] = (isset(${"MarksheetScoreID_".$StudentIDArr[$k]})) ? ${"MarksheetScoreID_".$StudentIDArr[$k]} : "";
				$MarksheetSubOverallScoreArr[$SubjectID][$count]['StudentID'] = $StudentIDArr[$k];
				$MarksheetSubOverallScoreArr[$SubjectID][$count]['SubjectID'] = $SubjectID;
				$MarksheetSubOverallScoreArr[$SubjectID][$count]['ReportColumnID'] = $ReportColumnIDArr[$j];				
				$MarksheetSubOverallScoreArr[$SubjectID][$count]['SchemeID'] = $SchemeID;
				$MarksheetSubOverallScoreArr[$SubjectID][$count]['MarkType'] = ($isSpecialCase) ? "SC" : $MarkType;
				$MarksheetSubOverallScoreArr[$SubjectID][$count]['MarkRaw'] = trim($MarkRaw);
				$MarksheetSubOverallScoreArr[$SubjectID][$count]['MarkNonNum'] = ($isSpecialCase) ? trim($RawData) : trim($MarkNonNum);
				$MarksheetSubOverallScoreArr[$SubjectID][$count]["LastModifiedUserID"] = $LastModifiedUserID;
				$MarksheetSubOverallScoreArr[$SubjectID][$count]["IsEstimated"] = $IsEstimated;
				
				$count++;
			}
		}
	}
		
	# INSERT - Insert New Record of Students corresponding to Certain Report Column to the MarkScore Table
	if(count($ReportColumnIDArr) > 0 && count($StudentIDArr) > 0){
		
			# INSERT - Insert New Record of Students corresponding to Certain Report Column to the MarkScore Table
			# Case 1: when it is term report
			# Case 2: when it is whole year report and the particular term report column has not defined term report template
			for($j=0 ; $j<count($ReportColumnIDArr) ; $j++){
				$result['insert_'.$i.'_'.$j] = $lreportcard->INSERT_MARKSHEET_SCORE($StudentIDArr, $SubjectID, $ReportColumnIDArr[$j], $LastModifiedUserID);
			}
	}
	
	# UPDATE - Score
	if(count($MarksheetSubOverallScoreArr) > 0){
		
		if(isset($MarksheetSubOverallScoreArr[$SubjectID]) ){
			# Update Record of Marksheet Score
			$result['update_marksheet_'.$SubjectID] = $lreportcard->UPDATE_MARKSHEET_SCORE($MarksheetSubOverallScoreArr[$SubjectID]);
		}
	
		if($result['update_marksheet_'.$SubjectID]){
			# Update the Time of LastMarksheetInput
			$result['update_report_lastdate'] = $lreportcard->UPDATE_REPORT_LAST_DATE($ReportID, $DateName);
			
			# Update the Complete Status of Component Subject
			if($isCmpSubjectComplete[$SubjectID]){
				$result['update_submission_progress_'.$SubjectID] = $lreportcard->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, $ClassID, 0, $SubjectGroupID);
			}
		}
		
		
		# Added on 20080819 by Andy
		# Update the Complete Status of Parent Subject
		$result['update_subj_submission_progress'] = $lreportcard->UPDATE_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, $ClassID, 0, $SubjectGroupID);
	}

}

#################################################################################################
if(!in_array(false, $result))
	$msg = "update";
else 
	$msg = "update_failed";


#################################################################################################
intranet_closedb();

$params = "ClassLevelID=$ClassLevelID&ReportID=$ReportID&SubjectID=$SubjectID&isProgress=$isProgress&isProgressSG=$isProgressSG";

if($IsSaveBefCancel == 1){
	header("Location: marksheet_revision.php?$params&msg=$msg");
} else {
	$params .= "&ClassID=$ClassID";
	header("Location: marksheet_cmp_edit.php?$params&msg=$msg");
}

?>