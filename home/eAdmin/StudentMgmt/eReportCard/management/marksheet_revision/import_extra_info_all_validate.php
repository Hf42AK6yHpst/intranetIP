<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

$lreportcard = new libreportcardcustom();
$linterface = new interface_html();
$limport = new libimporttext();

	
$CurrentPage = "Management_MarkSheetRevision";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	
$lreportcard->hasAccessRight();

$reportId = integerSafe(standardizeFormPostValue($_POST['ReportID']));
$classlevelId = integerSafe(standardizeFormPostValue($_POST['ClassLevelID']));
$subjectId = integerSafe(standardizeFormPostValue($_POST['SubjectID']));
$type = standardizeFormPostValue($_POST['type']);
$param = 'ReportID='.$reportId.'&ClassLevelID='.$classlevelId.'&SubjectID='.$subjectId.'&type='.$type;


$reportInfoAry = $lreportcard->returnReportTemplateBasicInfo($reportId);
$yearTermId = $reportInfoAry['Semester'];


### get header
if ($eRCTemplateSetting['Marksheet']['ManualInputGrade']) {
	$column_data = $lreportcard->returnReportTemplateColumnData($ReportID);
	$dataTitle = $column_data[0]['ColumnTitle'].' Grade';
}
else {
	$dataTitle = $eReportCard['ExtraInfoLabel'];
}
if ($type=="AdjustPosition") {
	$format_array = array("WebSAMSRegNumber", "Class Name", "Class Number", "Student Name", "Subject Group Code", "Subject Group Name", $eReportCard['AdjustPositionLabal']);
}
else {
	$format_array = array("WebSAMSRegNumber", "Class Name", "Class Number", "Student Name", "Subject Group Code", "Subject Group Name", $dataTitle);
}


### get csv data
$filepath = $_FILES['userfile']['tmp_name'];
$data = $limport->GET_IMPORT_TXT($filepath, $incluedEmptyRow=0, $lineBreakReplacement='<!--LineBreak-->');
$limport->SET_CORE_HEADER($format_array);
if (!$limport->VALIDATE_HEADER($data, true) || !$limport->VALIDATE_DATA($data)) {
	header("Location: import_extra_info_all.php?$param&returnMsgKey=WrongCSVHeader");
	exit();
}


# tag information
$TAGS_OBJ[] = array($eReportCard['Management_MarksheetRevision'], "", 0);
# page navigation (leave the array empty if no need)
if ($type=="AdjustPosition") {
	$PAGE_NAVIGATION[] = array($button_import." ".$eReportCard['AdjustPositionLabal']);
}
else {
	$PAGE_NAVIGATION[] = array($button_import." ".$dataTitle);
}
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);


### step display
$htmlAry['generalImportStepTbl'] = $linterface->GET_IMPORT_STEPS($CurrStep=2);


### get related subjects
if ($lreportcard->IS_ADMIN_USER_VIEW($_SESSION['UserID'])) {
	$checkPermission = false;
}
else {
	$checkPermission = true;
}
$subjectAry = $lreportcard->returnSubjectwOrder($classlevelId, $ParForSelection=0, $_SESSION['UserID'], $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $reportId, $checkPermission);
$subjectIdAry = array();
foreach ((array)$subjectAry as $_parentSubjectId => $_cmpSubjectAry) {
	$_cmpSubjectIdAry = array_keys($_cmpSubjectAry);
	$_numOfCmp = count($_cmpSubjectIdAry);
	for ($i=0; $i<$_numOfCmp; $i++) {
		$__cmpSubjectId = $_cmpSubjectIdAry[$i];
		
		if ($__cmpSubjectId == 0) {
			$subjectIdAry[] = $_parentSubjectId;
		}
		else {
			$subjectIdAry[] = $__cmpSubjectId;
		}
	}
}
if ($lreportcard->IS_ADMIN_USER_VIEW($_SESSION['UserID'])) {
	// do nth
}
else {
	$teachingSubjectGroupIdAry = Get_Array_By_Key($lreportcard->Get_Teacher_Related_Subject_Groups_By_Subjects($yearTermId, $classlevelId, $_SESSION['UserID'], $subjectIdAry), 'SubjectGroupID');
	$teachingClassIdAry = Get_Array_By_Key($lreportcard->GET_TEACHING_CLASS(), 'YearClassID');
}

### get related subject groups
$scm = new subject_class_mapping();
$subjectGroupAry = $scm->Get_Subject_Group_List($yearTermId, '', $returnAsso=false);
$subjectGroupIdAry = Get_Array_By_Key($subjectGroupAry, 'SubjectGroupID');
$subjectGroupAssoAry = BuildMultiKeyAssoc($subjectGroupAry, 'ClassCode');
unset($subjectGroupAry);


### get students
$studentAry = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($reportId, $classlevelId);
$studentWebSAMSRegNoAssoAry = BuildMultiKeyAssoc($studentAry, 'WebSAMSRegNo', 'UserID');
$studentClassAssoAry = BuildMultiKeyAssoc($studentAry, array('ClassName', 'ClassNumber'), 'UserID');
$studentIdAssoAry = BuildMultiKeyAssoc($studentAry, 'UserID');
unset($studentAry);

$studentAry = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($subjectGroupIdAry, $classlevelId);
$subjectGroupStudentAssoAry = BuildMultiKeyAssoc($studentAry, array('SubjectGroupID', 'UserID'), 'UserID');
unset($studentAry);


### get report info
$reportInfoAry = $lreportcard->returnReportTemplateBasicInfo($reportId);
$reportTitle = str_replace(":_:", "<br>", $reportInfoAry['ReportTitle']);
$yearTermId = $reportInfoAry['Semester'];



### delete old temp data
$table = $lreportcard->DBName.".RC_TEMP_IMPORT_EXTRA_INFO";
$sql = "Delete From $table Where UserID = '".$_SESSION['UserID']."' And ReportID = '".$reportId."'";
$successAry['deleteOldTempData'] = $lreportcard->db_db_query($sql);


### analyse data
array_shift($data);
$numOfData = count($data);
$errorMsgAssoAry = array();
$insertAry = array();
$subjectGroupCacheAry = array();
for ($i=0; $i<$numOfData; $i++) {
	$_rowNum = $i+2;
	$_webSAMSRegNo = trim($data[$i][0]);
	$_className = trim($data[$i][1]);
	$_classNumber = trim($data[$i][2]);
	$_studentName = trim($data[$i][3]);
	$_subjectGroupCode = trim($data[$i][4]);
	$_subjectGroupName = trim($data[$i][5]);
	$_info = trim($data[$i][6]);
	
	// initialize variables for each students
	$_studentId = '';
	$_subjectId = '';
	$_subjectGroupId = '';
	$_yearClassId = '';
	
	// check if student exist
	if ($_webSAMSRegNo != '' && isset($studentWebSAMSRegNoAssoAry[$_webSAMSRegNo])) {
		$_studentId = $studentWebSAMSRegNoAssoAry[$_webSAMSRegNo]['UserID'];
	}
	else if ($_className != '' && $_classNumber != '' && isset($studentClassAssoAry[$_className][$_classNumber])) {
		$_studentId = $studentClassAssoAry[$_className][$_classNumber]['UserID'];
	}
	if ($_studentId == '') {
		$errorMsgAssoAry[$_rowNum][] = 'studentNotFound';
	}
	
	
	// check if subject group exist
	if (isset($subjectGroupCacheAry[$_subjectGroupCode])) {
		$_subjectGroupId = $subjectGroupCacheAry[$_subjectGroupCode]['SubjectGroupID'];
		$_subjectId = $subjectGroupCacheAry[$_subjectGroupCode]['SubjectID'];
	}
	else {
		$_subjectGroupId = $subjectGroupAssoAry[$_subjectGroupCode]['SubjectGroupID'];
		$_subjectId = $subjectGroupAssoAry[$_subjectGroupCode]['SubjectID'];
		
		$subjectGroupCacheAry[$_subjectGroupCode]['SubjectGroupID'] = $_subjectGroupId;
		$subjectGroupCacheAry[$_subjectGroupCode]['SubjectID'] = $_subjectId;
	}
	
	if ($_subjectGroupId == '') {
		$errorMsgAssoAry[$_rowNum][] = 'subjectGroupNotFound';
	}
	
	// if student and subject group exist => further checking
	if ($_subjectGroupId > 0 && $_studentId > 0) {
		// check if student belongs to this subject group
		if (!isset($subjectGroupStudentAssoAry[$_subjectGroupId][$_studentId])) {
			$errorMsgAssoAry[$_rowNum][] = 'studentNotInSubjectGroup';
		}
		
		// check if teacher has access right to change this student data
		$_yearClassId = $studentIdAssoAry[$_studentId]['YearClassID'];
		if ($lreportcard->IS_ADMIN_USER_VIEW($_SESSION['UserID'])) {
			// do nth
		}
		else {
			if (!in_array($_subjectGroupId, $teachingSubjectGroupIdAry) && !in_array($_yearClassId, $teachingClassIdAry)) {
				$errorMsgAssoAry[$_rowNum][] = 'noRightToEditData';
			}
		}
	}
	
	$insertAry[] = " ('".$_SESSION['UserID']."', '".$_rowNum."', '".$reportId."', '".$lreportcard->Get_Safe_Sql_Query($_webSAMSRegNo)."', '".$lreportcard->Get_Safe_Sql_Query($_className)."', '".$lreportcard->Get_Safe_Sql_Query($_classNumber)."', '".$_studentId."',
						'".$_subjectId."', '".$lreportcard->Get_Safe_Sql_Query($_subjectGroupCode)."', '".$lreportcard->Get_Safe_Sql_Query($_subjectGroupName)."', '".$_subjectGroupId."', '".$lreportcard->Get_Safe_Sql_Query($_info)."', now()) ";
}

### simple statistics
$numOfErrorRow = count($errorMsgAssoAry);
$numOfSuccessRow = $numOfData - $numOfErrorRow;


### insert csv data to temp table
$numOfInsert = count($insertAry);
if ($numOfInsert > 0) {
	$insertChunkAry = array_chunk($insertAry, 1000);
	$numOfChunk = count($insertChunkAry);
	
	for ($i=0; $i<$numOfChunk; $i++) {
		$_insertAry = $insertChunkAry[$i];
		
		$sql = "Insert Into $table
					(UserID, RowNumber, ReportID, WebSAMSRegNo, ClassName, ClassNumber, StudentID, SubjectID, SubjectGroupCode, SubjectGroupName, SubjectGroupID, Info, DateInput)
				Values ".implode(', ', (array)$_insertAry);
		$successAry['insertData'][] = $lreportcard->db_db_query($sql);
	}
}


# report info display
if ($numOfErrorRow > 0) {
	$numOfErrorDisplay = '<span class="tabletextrequire">'.$numOfErrorRow.'</span>';
}
else {
	$numOfErrorDisplay = 0;
}
$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	// Date picker
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$eReportCard['Settings_ReportCardTemplates'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $reportTitle;
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['SuccessfulRecord'].'</td>'."\n";
		$x .= '<td>'.$numOfSuccessRow.'</td>'."\n";
	$x .= '</tr>'."\n";
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['FailureRecord'].'</td>'."\n";
		$x .= '<td>'.$numOfErrorDisplay.'</td>'."\n";
	$x .= '</tr>'."\n";
$x .= '</table>'."\r\n";
$htmlAry['reportInfoTbl'] = $x;


# error display
$x = '';
if ($numOfErrorRow > 0) {
	$x .= '<table class="common_table_list_v30 view_table_list_v30">'."\n";
		$x .= '<thead>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<th>'.$Lang['General']['ImportArr']['Row'].'</th>'."\n";
				$x .= '<th>'.$i_WebSAMS_Registration_No.'</th>'."\n";
				$x .= '<th>'.$Lang['General']['Class'].'</th>'."\n";
				$x .= '<th>'.$Lang['General']['ClassNumber'].'</th>'."\n";
				$x .= '<th>'.$Lang['AccountMgmt']['StudentName'].'</th>'."\n";
				$x .= '<th>'.$Lang['SysMgr']['SubjectClassMapping']['CSVField']['SubjectGroupCode'].'</th>'."\n";
				$x .= '<th>'.$Lang['SysMgr']['SubjectClassMapping']['CSVField']['SubjectGroupName'].'</th>'."\n";
				$x .= '<th>'.$dataTitle.'</th>'."\n";
				$x .= '<th>'.$Lang['General']['Remark'].'</th>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</thead>'."\n";
		$x .= '<tbody>'."\n";
			foreach ((array)$errorMsgAssoAry as $_rowNum => $_errorAry) {
				$aryIndex = $_rowNum - 2;
				$_webSAMSRegNo = trim($data[$aryIndex][0]);
				$_className = trim($data[$aryIndex][1]);
				$_classNumber = trim($data[$aryIndex][2]);
				$_studentName = trim($data[$aryIndex][3]);
				$_subjectGroupCode = trim($data[$aryIndex][4]);
				$_subjectGroupName = trim($data[$aryIndex][5]);
				$_info = trim($data[$aryIndex][6]);
				
				$_errorDisplayAry = array();
				
				if (in_array('studentNotFound', $_errorAry)) {
					if ($_webSAMSRegNo=='' && $_className=='' && $_classNumber=='') {
						$_webSAMSRegNo = '<span class="tabletextrequire">***</span>';
						$_className = '<span class="tabletextrequire">***</span>';
						$_classNumber = '<span class="tabletextrequire">***</span>';
					}
					else if ($_webSAMSRegNo != '') {
						$_webSAMSRegNo = '<span class="tabletextrequire">'.$_webSAMSRegNo.'</span>';
					}
					else {
						$_className = '<span class="tabletextrequire">'.$_className.'</span>';
						$_classNumber = '<span class="tabletextrequire">'.$_classNumber.'</span>';
					}
					$_studentName = '<span class="tabletextrequire">'.$_studentName.'</span>';
					
					$_errorDisplayAry[] = $eReportCard['StudentNotFound'];
				}
				
				if (in_array('subjectGroupNotFound', $_errorAry)) {
					if ($_subjectGroupCode == '') {
						$_subjectGroupCode = '***';
					}
					$_subjectGroupCode = '<span class="tabletextrequire">'.$_subjectGroupCode.'</span>';
					$_subjectGroupName = '<span class="tabletextrequire">'.$_subjectGroupName.'</span>';
					
					$_errorDisplayAry[] = $Lang['eReportCard']['ImportWarningArr']['sg_code_not_found'];
				}
				
				$_studentNotInSg = false;
				$_noRightToEditData = false;
				if (in_array('studentNotInSubjectGroup', $_errorAry)) {
					$_studentNotInSg = true;
				}
				if (in_array('noRightToEditData', $_errorAry)) {
					$_noRightToEditData = true;
				}
				if ($_studentNotInSg || $_noRightToEditData) {
					$_webSAMSRegNo = '<span class="tabletextrequire">'.$_webSAMSRegNo.'</span>';
					$_className = '<span class="tabletextrequire">'.$_className.'</span>';
					$_classNumber = '<span class="tabletextrequire">'.$_classNumber.'</span>';
					$_studentName = '<span class="tabletextrequire">'.$_studentName.'</span>';
					$_subjectGroupCode = '<span class="tabletextrequire">'.$_subjectGroupCode.'</span>';
					$_subjectGroupName = '<span class="tabletextrequire">'.$_subjectGroupName.'</span>';
					
					if ($_studentNotInSg) {
						$_errorDisplayAry[] = $Lang['eReportCard']['ImportWarningArr']['student_not_in_sg'];
					}
					if ($_noRightToEditData) {
						$_errorDisplayAry[] = $Lang['eReportCard']['ImportWarningArr']['not_teaching_student'];
					}
				}
				
				$_errorDisplay = '- '.implode('<br />- ', $_errorDisplayAry);
				
				$x .= '<tr>'."\n";
					$x .= '<td>'.$_rowNum.'</td>'."\n";
					$x .= '<td>'.$_webSAMSRegNo.'</td>'."\n";
					$x .= '<td>'.$_className.'</td>'."\n";
					$x .= '<td>'.$_classNumber.'</td>'."\n";
					$x .= '<td>'.$_studentName.'</td>'."\n";
					$x .= '<td>'.$_subjectGroupCode.'</td>'."\n";
					$x .= '<td>'.$_subjectGroupName.'</td>'."\n";
					$x .= '<td>'.$_info.'</td>'."\n";
					$x .= '<td>'.$_errorDisplay.'</td>'."\n";
				$x .= '</tr>'."\n";
			}
		$x .= '</tbody>'."\n";
	$x .= '</table>'."\n";
}
$htmlAry['errorTbl'] = $x;



### action buttons
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Import'], "button", "goSubmit()", 'submitBtn', $ParOtherAttribute="", ($numOfErrorRow>0)? true : false, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['backBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "goCancel()", 'cancelBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");

?>
<script type="text/JavaScript" language="JavaScript">
function trim(str) {
	return str.replace(/^\s+|\s+$/g,"");
}

function goSubmit() {
	$('form#form1').attr('action', 'import_extra_info_all_update.php').submit();
}

function goBack() {
	window.location = 'import_extra_info_all.php?<?=$param?>';
}
function goCancel() {
	window.location = 'marksheet_revision_subject_group.php?<?=$param?>';
}

</script>
<form id="form1" name="form1" method="POST">
	<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	<br />
	
	<?=$htmlAry['generalImportStepTbl']?>
	
	<div class="table_board">
		<?=$htmlAry['reportInfoTbl']?>
		<?=$htmlAry['errorTbl']?>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['backBtn']?>
			<?=$htmlAry['cancelBtn']?>
			<p class="spacer"></p>
		</div>
	</div>
	<input type="hidden" name="ClassLevelID" id="ClassLevelID" value="<?=$classlevelId?>"/>
	<input type="hidden" name="ReportID" id="ReportID" value="<?=$reportId?>"/>
	<input type="hidden" name="SubjectID" id="SubjectID" value="<?=$subjectId?>"/>
	<input type="hidden" name="type" id="type" value="<?=$type?>"/>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>