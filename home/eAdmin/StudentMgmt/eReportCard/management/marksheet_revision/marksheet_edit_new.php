<?php
$PATH_WRT_ROOT = "../../../../../../../";
$PageRight = "ADMIN";
 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

$lreportcard = new libreportcardcustom();
$linterface = new interface_html();
$lreportcard_ui = new libreportcard_ui();

$lreportcard->hasAccessRight();

$ReportID = $_GET['ReportID'];
$ClassLevelID = $_GET['ClassLevelID'];
$SubjectID = $_GET['SubjectID'];
$SubjectGroupID = $_GET['SubjectGroupID'];
$ReturnMsgKey = $_GET['ReturnMsgKey'];

$CurrentPage = "Reports_GenerateReport";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eReportCard['Reports_GenerateReport'], "", 0);


$linterface->LAYOUT_START($ReturnMsg);

# sub-tag information
echo $lreportcard_ui->Get_Management_Marksheet_Revision_Edit_UI($ReportID);

?>
<script language="javascript">
$(document).ready( function() {
	
});
</script>
<br />
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>