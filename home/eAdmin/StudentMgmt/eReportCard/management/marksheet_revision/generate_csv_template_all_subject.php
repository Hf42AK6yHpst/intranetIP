<?php
// Using: Bill
/********************************************************
 * Modification log
 * 20151117 Bill	[2015-1116-0916-31066]
 * 		- Use Userlogin instead of WebSAMSRegNo
 * 20151008	Bill	[2015-0520-1100-36066]
 * 		- Cust - Import Marksheet for All subjects
 * 		- Copy from generate_csv_template.php
 * ******************************************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

#################################################################################################

include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
$lreportcard = new libreportcard2008j();

$lexport = new libexporttext();

# Loading Report Template Data
$basic_data = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$column_data = $lreportcard->returnReportTemplateColumnData($ReportID);
$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
$ReportType = $basic_data['Semester'];
$ColumnSize = sizeof($column_data);

# Load Settings - Calculation
// Get the Calculation Method of Report Type - 1: Right-Down,  2: Down-Right
$SettingCategory = 'Calculation';
$categorySettings = $lreportcard->LOAD_SETTING($SettingCategory);
$TermCalculationType = $categorySettings['OrderTerm'];
$FullYearCalculationType = $categorySettings['OrderFullYear'];

// Get Existing Report Calculation Method
$CalculationType = ($ReportType == "F") ? $FullYearCalculationType : $TermCalculationType;

# Get Data
// Period (Term x, Whole Year, etc)
$PeriodArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID, $ReportID);
$PeriodName = (count($PeriodArr) > 0) ? $PeriodArr['SemesterTitle'] : '';

// Subject ID list in Report
$SubjectIDAry = $lreportcard->returnSubjectwOrder($ClassLevelID, 0, "", "", "Desc", 0, $ReportID);

// Subject Group Code and ID Mapping
$scm = new subject_class_mapping();
$SGCodeIDMapping = $scm->Get_Subject_Group_Info($lreportcard->GET_ACTIVE_YEAR_ID(), $ReportType);
$SGCodeIDMapping = BuildMultiKeyAssoc((array)$SGCodeIDMapping, array("SubjectID", "SubjectGroupID"));

# Get Report Column Weight
$WeightAry = array();
if($CalculationType == 2){
	if(count($column_data) > 0){
    	for($i=0 ; $i<sizeof($column_data) ; $i++){
			$OtherCondition = "SubjectID IS NULL AND ReportColumnID = ".$column_data[$i]['ReportColumnID'];
			$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
			
			$WeightAry[$weight_data[0]['ReportColumnID']][$SubjectID] = $weight_data[0]['Weight'];
		}
	}
}
else {
	$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID);  
	
	for($i=0 ; $i<sizeof($weight_data) ; $i++)
		$WeightAry[$weight_data[$i]['ReportColumnID']][$weight_data[$i]['SubjectID']] = $weight_data[$i]['Weight'];
}

# Subject Grading Scheme
$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, "", 0, 0, $ReportID);
$SubjectFormGradingArr = BuildMultiKeyAssoc((array)$SubjectFormGradingArr, array("SubjectID"));

//// Scheme Range Info
//$SchemeGrade = array();
//$GradeInfo = array();
//if(count($SubjectFormGradingArr) > 0){
//	$SchemeID = $SubjectFormGradingArr['SchemeID'];
//	$ScaleInput = $SubjectFormGradingArr['ScaleInput'];
//	$ScaleDisplay = $SubjectFormGradingArr['ScaleDisplay'];
//	
//	$SchemeInfo = $lreportcard->GET_GRADING_SCHEME_INFO($SchemeID);
//	$SchemeMainInfo = $SchemeInfo[0];
//	$SchemeMarkInfo = $SchemeInfo[1];
//	$SchemeType = $SchemeMainInfo['SchemeType'];
//}
//
//
//// special column checking for parent subject
//$isAllComponentZeroWeight = array();
//if ($isParentSubject) {
//	for($i=0 ; $i<$ColumnSize ; $i++) {
//		$_reportColumnId = $column_data[$i]['ReportColumnID'];
//		
//		$_isAllComponentZeroWeight = true;
//		for($j=0 ; $j<count($CmpSubjectArr) ; $j++) {
//			$__cmpSubjectId = $CmpSubjectArr[$i]['SubjectID'];
//			
//			$__weightAry = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, $other_condition='', $__cmpSubjectId, $_reportColumnId);
//			$__subjectWeight = $__weightAry[0]['Weight'];
//			
//			if ($__subjectWeight > 0) {
//				$_isAllComponentZeroWeight = false;
//				break;
//			}
//		}
//		
//		$isAllComponentZeroWeight[$_reportColumnId] = $_isAllComponentZeroWeight;
//	}
//}
//
//
//
//// Student Info Array
//$StudentArr = array();
//if ($SubjectGroupID != '') // get all student in subject group
//{
//	$StudentArr = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupID, $ClassLevelID, '', 1);
//	$showClassName = true;
//	
//	$obj_SubjectGroup = new subject_term_class($SubjectGroupID);
//	$SubjectGroupName = $obj_SubjectGroup->Get_Class_Title();
//	
//	$Class_SG_Title = $Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'];
//	$Class_SG_Name = $SubjectGroupName;
//}
//else if(trim($ClassID)!='') // consolidate get all student in class
//{
//	$StudentArr = $lreportcard->GET_STUDENT_BY_CLASS($ClassID);
//	$Class_SG_Title = "ClassName";
//	$Class_SG_Name = $ClassName;
//	
//	$showClassName = true;
//}
//else if($ReportType == "F") // consolidate get all student in classlevel
//{
//	$StudentArr = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID);
//	
//	$IsClassLevelStudentList=1;
//}
//else // get student in all subject group in classlevel
//{
//	if (!$special_feature['eRC_class_teacher_as_subject_teacher']) {
//		$lteaching = new libteaching();
//		$TeacherClassAry = $lteaching->returnTeacherClassWithLevel($_SESSION['UserID'], $lreportcard->schoolYearID);
//		$TeacherClass = $TeacherClassAry[0]['ClassName'];
//		$TeacherClassLevelID = $TeacherClassAry[0]['ClassLevelID'];
//		$TeacherClassID = $TeacherClassAry[0]['ClassID'];
//	}
//	
//	# Get all Subject Groups of the Subject
//	$TeacherID = '';
//	if ($ck_ReportCard_UserType=="TEACHER" && (empty($TeacherClass) || $TeacherClassLevelID!=$ClassLevelID)) {
//		# Subject Teacher => View teaching subject(s) only
//		$TeacherID = $_SESSION['UserID'];
//	}
//	
//	$MainSubjectID = $isCmpSubject?$lreportcard->GET_PARENT_SUBJECT_ID($SubjectID):$SubjectID;
//	
//	$obj_Subject = new Subject($MainSubjectID);
//	$AllSubjectGroupArr = $obj_Subject->Get_Subject_Group_List($ReportType, $ClassLevelID, '', $TeacherID, $returnAsso=0, $TeacherClassID, '', $InYearClassOnly=true);
//	$SubjectGroupIDArr = Get_Array_By_Key($AllSubjectGroupArr, 'SubjectGroupID');
//	
//	foreach((array)$SubjectGroupIDArr as $thisSubjectGroupID) {
//		$StudentArr = array_merge($StudentArr,(array)$lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($thisSubjectGroupID, $ClassLevelID, '', 1));
//	}
//}
//
////debug_pr($StudentArr);
//$StudentSize = sizeof($StudentArr);
//
//$StudentIDArr = array();
//if($StudentSize > 0){
//	for($i=0 ; $i<$StudentSize ; $i++)
//		$StudentIDArr[$i] = $StudentArr[$i]['UserID'];
//}
//
//$MarksheetScoreInfoArr = array();
//$ColumnHasTemplateAry = array();
//if($ColumnSize > 0){
//    for($i=0 ; $i<$ColumnSize ; $i++){
//		/* 
//		* For Whole Year Report use only
//		* Check for the Mark Column is allowed to fill in mark when any term does not have related report template 
//		* by SemesterNum (ReportType) && ClassLevelID
//		* An array is created as a control variable $ColumnHasTemplateAry
//		*/
//		if($ReportType == "F")
//			$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] = $lreportcard->CHECK_REPORT_TEMPLATE_FROM_COLUMN($column_data[$i]['SemesterNum'], $ClassLevelID);
//		else 
//			$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] = 1;
//			
//		if($ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] && $ReportType == "F"){
//			$TermReportID = $ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']];
//			$WeightedMark[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_STUDENT_TERM_SUBJECT_MARK($StudentIDArr, $TermReportID, $SubjectID);
//		}
//	}
//}

# Header
$exportColumn[0][] = $eReportCard['ImportHeader']['SubjectGroupCode']['En'];
$exportColumn[0][] = $eReportCard['ImportHeader']['SubjectComponentCode']['En'];
// [2015-1116-0916-31066] change to userlogin
//$exportColumn[0][] = $eReportCard['ImportHeader']['WebSamsRegNo']['En'];
$exportColumn[0][] = $eReportCard['ImportHeader']['UserLogin']['En'];

// loop Report Column
for($i=0; $i<$ColumnSize ; $i++){
	list($ReportColumnID, $ColumnTitle, $DefaultWeight, $SemesterNum) = $column_data[$i];
	$BlockMarksheet = $column_data[$i]['BlockMarksheet'];
	
	$ColumnTitle = ($ColumnTitle != "" && $ColumnTitle != null) ? $ColumnTitle : $ColumnTitleAry[$ReportColumnID];
	$exportColumn[0][] = $ColumnTitle;
}

# Content
if(count($SubjectIDAry)>0)
{
	// loop Subject
	foreach($SubjectIDAry as $CompSubjectID => $CompSubjectIDAry)
	{
		// Get related Subject Groups
		$relatedSubjectGroups = $SGCodeIDMapping[$CompSubjectID];
		if(count($relatedSubjectGroups)==0)
			continue;
		
		// loop Component Subject
		foreach($CompSubjectIDAry as $SubjectID => $CompSubject)
		{
			$isMainSubject = $SubjectID==0;
			$isParentSubject = false;
			$isCalculateByCmpSub = false;
			
			// Get component subject(s) if any
			$CmpSubjectArr = array();
			if($isMainSubject)
			{
				$subj_obj = "";
				$SubjectID = $CompSubjectID;	
				
				$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID, "", "Desc", $ReportID, 0);
				$isParentSubject = (count($CmpSubjectArr) > 0)? true : false;
			
				if(!empty($CmpSubjectArr)){
					for($i=0 ; $i<count($CmpSubjectArr) ; $i++){
						$CmpSubjectIDArr[] = $CmpSubjectArr[$i]['SubjectID'];
						
						$CmpSubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $CmpSubjectArr[$i]['SubjectID'],0,0,$ReportID);
						if(count($CmpSubjectFormGradingArr) > 0 && $CmpSubjectFormGradingArr['ScaleInput'] == "M")
							$isCalculateByCmpSub = true;
					}
					
					// Skip as no input needed for parent subject
					if($isCalculateByCmpSub)
						continue;
				}
			}
			else
			{
				$subj_obj = new subject($SubjectID);
			}
			
			// Scheme Range Info
			$SchemeGrade = array();
			$GradeInfo = array();
			$currentSubjectGrading = $SubjectFormGradingArr[$SubjectID];
			if(count($currentSubjectGrading) > 0){
				$SchemeID = $currentSubjectGrading['SchemeID'];
				$ScaleInput = $currentSubjectGrading['ScaleInput'];
				$ScaleDisplay = $currentSubjectGrading['ScaleDisplay'];
				
				$SchemeInfo = $lreportcard->GET_GRADING_SCHEME_INFO($SchemeID);
				$SchemeMainInfo = $SchemeInfo[0];
				$SchemeMarkInfo = $SchemeInfo[1];
				$SchemeType = $SchemeMainInfo['SchemeType'];
			}

			// Special column checking for Parent Subject
//			$isAllComponentZeroWeight = array();
//			if ($isParentSubject) {
//				for($i=0 ; $i<$ColumnSize ; $i++) {
//					$_reportColumnId = $column_data[$i]['ReportColumnID'];
//					
//					$_isAllComponentZeroWeight = true;
//					for($j=0 ; $j<count($CmpSubjectArr) ; $j++) {
//						$__cmpSubjectId = $CmpSubjectArr[$i]['SubjectID'];
//						
//						$__weightAry = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, $other_condition='', $__cmpSubjectId, $_reportColumnId);
//						$__subjectWeight = $__weightAry[0]['Weight'];
//						if ($__subjectWeight > 0) {
//							$_isAllComponentZeroWeight = false;
//							break;
//						}
//					}
//					
//					$isAllComponentZeroWeight[$_reportColumnId] = $_isAllComponentZeroWeight;
//				}
//			}
			
			foreach($relatedSubjectGroups as $SubjectGroupID => $SubjectGroup)
			{
				// Student Info Array
				$StudentArr = array();
				$StudentArr = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupID, $ClassLevelID, '', 1);
				
				// Student ID Array
				$StudentIDArr = array();
				$StudentSize = sizeof($StudentArr);
				if($StudentSize > 0){
					for($i=0 ; $i<$StudentSize ; $i++)
						$StudentIDArr[$i] = $StudentArr[$i]['UserID'];
				}
				
//				$MarksheetScoreInfoArr = array();
//				$ColumnHasTemplateAry = array();
//				if($ColumnSize > 0)
//				{
//				    for($i=0 ; $i<$ColumnSize ; $i++)
//				    {
//						/* 
//						* For Whole Year Report use only
//						* Check for the Mark Column is allowed to fill in mark when any term does not have related report template 
//						* by SemesterNum (ReportType) && ClassLevelID
//						* An array is created as a control variable $ColumnHasTemplateAry
//						*/
//						if($ReportType == "F")
//							$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] = $lreportcard->CHECK_REPORT_TEMPLATE_FROM_COLUMN($column_data[$i]['SemesterNum'], $ClassLevelID);
//						else 
//							$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] = 1;
//							
//						if($ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] && $ReportType == "F"){
//							$TermReportID = $ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']];
//							$WeightedMark[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_STUDENT_TERM_SUBJECT_MARK($StudentIDArr, $TermReportID, $SubjectID);
//						}
//					}
//				}
				
				for($j=0; $j<$StudentSize; $j++)
				{
					// [2015-1116-0916-31066] change to userlogin
					//list($StudentID, $WebSAMSRegNo) = $StudentArr[$j];
					$currentUserLogin = $StudentArr[$j]["UserLogin"];
					
				    $row[] = trim($relatedSubjectGroups[$SubjectGroupID]["SubjectGroupCode"]);
					$row[] = trim($subj_obj==""? "" : $subj_obj->Get_Subject_Code());
					$row[] = trim($currentUserLogin);
					if(count($column_data) > 0){
					    for($i=0 ; $i<sizeof($column_data) ; $i++)
				    		$row[] = $Value;
						$rows[] = $row;
						unset($row);
				    }
				}
			}
		}
	}
}

#################################################################################################

intranet_closedb();

// Output the file to user browser
$set1 = array("&nbsp;", " ");
$set2 = array("-", ",");

$filename = $Prefix."Marksheet_".$PeriodName;
$filename = str_replace($set2, "_", $filename);
$filename = str_replace($set1, "_", $filename);
$filename = $filename.".csv";

$export_content = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($rows, $exportColumn, "", "\r\n", "", 0, "11");
$lexport->EXPORT_FILE($filename, $export_content);

?>