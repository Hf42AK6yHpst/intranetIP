<?php
/*
 * Change Log:
 * Date:	2017-01-05 Villa	change param for submark sheet layer
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	}
	
	$li = new libreportcard();

// 	$param = "SubjectID=$SubjectID&ClassID=$ClassID&ReportColumnID=$ReportColumnID&ReportID=$ReportID&jsReportColumn=$jsReportColumn&SubjectGroupID=$SubjectGroupID&UsePercent=$UsePercent";
	$param = "SubjectID=$SubjectID&ClassID=$ClassID&SchoolCode=$SchoolCode&ReportColumnID=$ReportColumnID&ReportID=$ReportID&jsReportColumn=$jsReportColumn&SubjectGroupID=$SubjectGroupID&SubReportColumnID=$SubReportColumnID&currentLevel=$currentLevel&UsePercent=$UsePercent";
	
	$success = $li->DELETE_SUB_MARKSHEET_COLUMN("ColumnID", $AssessmentColumnID);

	$Result = $success ? "delete" : "delete_failed";

	intranet_closedb();
	header("Location:sub_marksheet.php?$param&Result=$Result");
}
?>