<?php
// Modifing by 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcardcustom();

$task = trim(stripslashes($_POST['task']));

if ($task == 'getEnrollmentPerformanceAverage') {
	$ReportID = trim(stripslashes($_POST['ReportID']));
	$StudentIDList = trim(stripslashes($_POST['StudentIDList']));
	
	$storageDisplaySettings = $lreportcard->LOAD_SETTING("Storage&Display");
	
	$reportInfoAry = $lreportcard->returnReportTemplateBasicInfo($ReportID);
	$termId = $reportInfoAry['Semester'];
	
	$studentIdAry = explode(',', $StudentIDList);
	$numOfStudent = count($studentIdAry);
	$resultAry = array();
	for ($i=0; $i<$numOfStudent; $i++) {
		$_studentId = $studentIdAry[$i];
		$_studentEnrolAry = $lreportcard->Get_eEnrolment_Data($_studentId, $termId);
		$_performanceAry = Get_Array_By_Key($_studentEnrolAry, 'Performance');
		
		$_averagePerformance = $lreportcard->getAverage($_performanceAry, $storageDisplaySettings['SubjectTotal']);
		$resultAry[] = $_studentId.'=='.$_averagePerformance;
	}
	
	echo implode('||', $resultAry);
}

intranet_closedb();
?>