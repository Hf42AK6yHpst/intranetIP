<?php
// Modifying by: Bill
/***************************************************
 * 	Modification log:
 * 20201103 (Bill)      [2020-0915-1830-00164]
 *      - support term setting    ($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings'])
 * 20170519 Bill:
 * 		- allow View Group to access (view only)
 * 20160426 Bill:		[2015-1008-1356-44164]
 * 		- display edit button for whole year report that display overall column due to all columns are zero weight
 * 20161113 Bill:
 * 		- BIBA Cust
 * 		- display "-" (Teacher Comment) for all BIBA Report
 * 20150507 Bill:
 * 		- BIBA Cust
 * 		- display "-" if component subject with subject topics
 * 20120403 Marcus:
 * 		- Allow teacher to view marks even after submission period
 * 		- 2012-0208-1800-43132 - 沙田循道衛理中學 - Edit mark after submit the marksheet 
 * *************************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	/* Temp Library*/
	include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
	$lreportcard = new libreportcard2008j();
	//$lreportcard = new libreportcard();
	
	$CurrentPage = "Management_MarkSheetRevision";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    if ($lreportcard->hasAccessRight())
    {
        $linterface = new interface_html();
        $PAGE_TITLE = $eReportCard['Management_MarksheetRevision'];
        
        if($ck_ReportCard_UserType=="TEACHER")
		{
			$PAGE_TITLE = $eReportCard['Management_MarksheetSubmission'];
		}
        
		############################################################################################################
		
		# Get component subject(s) if any
		$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
		
		if(empty($CmpSubjectArr)){	// If no any component subjects, redirect
			$param = "SubjectID=$SubjectID&ClassID=$ClassID&ClassLevelID=$ClassLevelID&ReportID=$ReportID";
			header("Location: marksheet_edit.php?$param");
		}
		else {
			# Get Data
			// Period (Term x, Whole Year, etc)
			$PeriodArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID, $ReportID);
			$PeriodName = (count($PeriodArr) > 0) ? $PeriodArr['SemesterTitle'] : '';
		
			// Class
			$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
			for($i=0 ; $i<count($ClassArr) ;$i++){
				if($ClassArr[$i][0] == $ClassID)
					$ClassName = $ClassArr[$i][1];
			}
			
			// Parent Subject
			$SubjectName = $lreportcard->GET_SUBJECT_NAME($SubjectID);
					
			// Component Subject Data
			$count = 1;
			$CmpSubjectDataArr = array();
			for($i=0 ; $i<count($CmpSubjectArr) ; $i++){
				$CmpSubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $CmpSubjectArr[$i]['SubjectID'],0,0,$ReportID);
				
				$CmpSubjectDataArr[$i] = $CmpSubjectArr[$i];
				$CmpSubjectDataArr[$i]['SchemeID'] = $CmpSubjectFormGradingArr['SchemeID'];
			}
			
			// BIBA Cust - get SubjectID with subject topics
            $targetTermSeq = 0;
			if($eRCTemplateSetting['Settings']['SubjectTopics'])
			{
                // [2016-0330-1524-42164]
				//$SubjectTopicArr = $lreportcard->CHECK_SUBJECT_WITH_SUBJECT_TOPICS($ClassLevelID);
                $SubjectTopicArr = $lreportcard->CHECK_SUBJECT_WITH_SUBJECT_TOPICS($ClassLevelID, '', $targetTermSeq);
			}
			
			// used to check whether there is any marksheet feedback which is not closed.
			$MarksheetFeedback = $lreportcard->Get_Marksheet_Feedback_Info($ReportID);
			$MarksheetFeedback = BuildMultiKeyAssoc($MarksheetFeedback, array("StudentID","SubjectID"),array("RecordStatus"),1);
			
			# Marcus
			$StudentArr = $lreportcard->Get_Marksheet_Accessible_Student_List_By_Subject_And_Class($ReportID, $_SESSION['UserID'], $ClassID, $SubjectID, $SubjectID);
			$StudentList = Get_Array_By_Key($StudentArr,"UserID");
			unset($StudentArr);
			
			# Other Preparation
			# Filters - By Type (Term1, Term2, Whole Year, etc)
			// Get Semester Type from the reportcard template corresponding to ClassLevelID
			$ReportTypeSelection = '';
			$ReportTypeArr = array();
			$ReportTypeOption = array();
			$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);
			
			$ReportType = '';			
			if(count($ReportTypeArr) > 0){
				for($j=0 ; $j<count($ReportTypeArr) ; $j++){
					if($ReportTypeArr[$j]['ReportID'] == $ReportID){
						$isFormReportType = 1;
						$ReportType = $ReportTypeArr[$j]['Semester'];
					}
					
					$ReportTypeOption[$j][0] = $ReportTypeArr[$j]['ReportID'];
					$ReportTypeOption[$j][1] = $ReportTypeArr[$j]['SemesterTitle'];
				}
				//$ReportTypeSelection .= $linterface->GET_SELECTION_BOX($ReportTypeOption, 'name="ReportType" id="ReportType" class="tabletexttoptext" onchange="jCHANGE_MARKSHEET_REVISION()"', '', $ReportID);
				
				$ReportType = ($ReportType != "") ? $ReportType : $ReportTypeArr[0]['Semester'];
			}
			$ReportID = ($isFormReportType) ? $ReportID : $ReportTypeOption[0][0];
			
			# Get Last Modified Info of This Report Template(Date-Time & Modified By who)
			$ReportColumnIDArr = array();
			$ReportColumnIDList = '';
			$column_data = $lreportcard->returnReportTemplateColumnData($ReportID); 	// Column Data
			if(count($column_data) > 0){
				for($i=0 ; $i<count($column_data) ; $i++)
					$ReportColumnIDArr[] = $column_data[$i]['ReportColumnID'];
				$ReportColumnIDList = implode(",", $ReportColumnIDArr);
			}
			for($i=0 ; $i<count($CmpSubjectArr) ; $i++){
				$LastModifiedArr[$CmpSubjectArr[$i]['SubjectID']] = $lreportcard->GET_CLASS_MARKSHEET_LAST_MODIFIED($CmpSubjectArr[$i]['SubjectID'], $ReportColumnIDList);
				$CommentLastModifiedArr[$CmpSubjectArr[$i]['SubjectID']] = $lreportcard->GET_CLASS_MARKSHEET_SUBJECT_COMMENT_LAST_MODIFIED($CmpSubjectArr[$i]['SubjectID'], $ReportColumnIDList);
				$LastModifiedOverallArr[$CmpSubjectArr[$i]['SubjectID']] = $lreportcard->GET_CLASS_MARKSHEET_OVERALL_LAST_MODIFIED($CmpSubjectArr[$i]['SubjectID'], $ReportID);
			}
			
			# [2015-1008-1356-44164] Load Settings - Calculation
			// Get the Calculation Method of Report Type - 1: Right-Down,  2: Down-Right
			$SettingCategory = 'Calculation';
			$categorySettings = $lreportcard->LOAD_SETTING($SettingCategory);
			$TermCalculationType = $categorySettings['OrderTerm'];
			$FullYearCalculationType = $categorySettings['OrderFullYear'];
			$CalculationType = ($ReportType == "F") ? $FullYearCalculationType : $TermCalculationType;	
			
			# Get Status of Completed Action
			# $PeriodAction = "Submission" or "Verification"
			$PeriodAction = "Submission";
			$PeriodType = $lreportcard->COMPARE_REPORT_PERIOD($ReportID, $PeriodAction);
			
			# Array of Selection Box of Complete Function 
			$CompleteArr = array();
			$CompleteArr[] = array(1, $eReportCard['Confirmed']);
			$CompleteArr[] = array(0, $eReportCard['NotCompleted']);
			
			$CompleteAllArr = array();
			$CompleteAllArr[] = array("", "---".$eReportCard['ChangeAllTo']."---");
			$CompleteAllArr[] = array("completeAll", $eReportCard['Confirmed']);
			$CompleteAllArr[] = array("notCompleteAll", $eReportCard['NotCompleted']);
			
			# Component Subject List
			$AllSubjectColumnWeightStatus = array();
			$display = '';
			$cnt = 0;
			if(count($CmpSubjectDataArr) > 0){
				for($j=0 ; $j<count($CmpSubjectDataArr) ; $j++){
					list($CmpSubjectID, $CmpSubjectName, $CmpSchemeID) = $CmpSubjectDataArr[$j];
					
					$row_css = ($cnt % 3 == 1) ? "attendanceouting" : "attendancepresent";
					
					# [2015-1008-1356-44164]
					# Get Grading Info
					$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $CmpSubjectID, 0, 0, $ReportID);
					$SchemeID = $SubjectFormGradingArr['SchemeID'];
					$ScaleInput = $SubjectFormGradingArr['ScaleInput'];
					$SchemeInfo = $lreportcard->GET_GRADING_SCHEME_INFO($SchemeID);
					$SchemeMainInfo = $SchemeInfo[0];
					$SchemeType = $SchemeMainInfo['SchemeType'];
					
					# Check if all Column Weight Zero
					$WeightAry = array();
					if($CalculationType == 2){
						if(count($column_data) > 0){
					    	for($k=0 ; $k<count($column_data) ; $k++){
								$OtherCondition = "SubjectID = '$CmpSubjectID' AND ReportColumnID = ".$column_data[$k]['ReportColumnID'];
								$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
								
								$WeightAry[$weight_data[$k]['ReportColumnID']][$CmpSubjectID] = $weight_data[0]['Weight'];
							}
						}
					}
					else {
						//$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID);
						$OtherCondition = "SubjectID = '$CmpSubjectID' AND ReportColumnID IS NOT NULL";
						$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition);
						
						for($k=0 ; $k<count($weight_data) ; $k++)
							$WeightAry[$weight_data[$k]['ReportColumnID']][$CmpSubjectID] = $weight_data[$k]['Weight'];
					}
					
					$isAllColumnWeightZero = true;
					foreach((array)$WeightAry as $thisReportColumnID => $thisArr) {
						if ($thisArr[$CmpSubjectID] != 0 && $thisReportColumnID != '') {
							$isAllColumnWeightZero = false;
						}
					}
					
					# Display Overall Column due to all columns are zero weight
					if ($isAllColumnWeightZero == true && $SchemeType == "H" && ($ScaleInput == "M" || $ScaleInput == "G")) {
						$displayOverallColumnDueToAllZeroWeight = true;
					}
					else {
						$displayOverallColumnDueToAllZeroWeight = false;
					}
					
					# Check whether all column weights are zero or not corresponding to each subject of a report
					$isAllColumnWeightZero = $lreportcard->IS_ALL_REPORT_COLUMN_WEIGHTS_ZERO($ReportID, $CmpSubjectID);
					$AllSubjectColumnWeightStatus[] = $isAllColumnWeightZero;
					
					# Check whether the marksheet is editable or not 
					$isEdit = $lreportcard->CHECK_MARKSHEET_STATUS($CmpSubjectID, $ClassLevelID, $ReportID);
					
					// BIBA Cust - check if component subject with subject topics
					$thisSubjectWithTopic = false;
					if($eRCTemplateSetting['Settings']['SubjectTopics'] && !$thisSubjectWithTopic && isset($SubjectTopicArr[$CmpSubjectID][1])){
						$thisSubjectWithTopic = true;
					}
					
					# Last Modified Info
//					$LastModifiedDate = '-';
//					$LastModifiedBy = '-';
//					if(!empty($LastModifiedArr)){
//						$LastModifiedDate = (isset($LastModifiedArr[$CmpSubjectID][$ClassID][0])) ? $LastModifiedArr[$CmpSubjectID][$ClassID][0] : $LastModifiedDate;
//						$LastModifiedBy = (isset($LastModifiedArr[$CmpSubjectID][$ClassID][1])) ? $LastModifiedArr[$CmpSubjectID][$ClassID][1] : $LastModifiedBy;
//					}
					
					$LastModifiedDate = '';
					$LastModifiedBy = '';
					if(!empty($LastModifiedArr) || !empty($LastModifiedOverallArr)){
						$LastAssessmentModifiedDate = $LastModifiedArr[$CmpSubjectID][$ClassID][0];
						$LastAssessmentModifiedBy = $LastModifiedArr[$CmpSubjectID][$ClassID][1];
						
						$LastOverallModifiedDate = $LastModifiedOverallArr[$CmpSubjectID][$ClassID][0];
						$LastOverallModifiedBy = $LastModifiedOverallArr[$CmpSubjectID][$ClassID][1];
						
						if ($LastAssessmentModifiedDate == '' && $LastOverallModifiedDate == '') {
							$LastModifiedDate = '-';
							$LastModifiedBy = '-';
						}
						else if ($LastAssessmentModifiedDate > $LastOverallModifiedDate) {
							$LastModifiedDate = $LastAssessmentModifiedDate;
							$LastModifiedBy = $LastAssessmentModifiedBy;
						}
						else {
							$LastModifiedDate = $LastOverallModifiedDate;
							$LastModifiedBy = $LastOverallModifiedBy;
						}
						
						$SubjectCommentLastModifiedDate = (isset($CommentLastModifiedArr[$CmpSubjectID][$ClassID][0])) ? $CommentLastModifiedArr[$CmpSubjectID][$ClassID][0] : $LastModifiedDate;
						$SubjectCommentLastModifiedBy = (isset($CommentLastModifiedArr[$CmpSubjectID][$ClassID][1])) ? $CommentLastModifiedArr[$CmpSubjectID][$ClassID][1] : $LastModifiedBy;
						if ($SubjectCommentLastModifiedDate != '' && $SubjectCommentLastModifiedDate > $LastModifiedDate) {
							$LastModifiedDate = $SubjectCommentLastModifiedDate;
							$LastModifiedBy = $SubjectCommentLastModifiedBy;
						}
					}
					
					// check whether there is any marksheet feedback which is not closed.
					$InProgressRemarks = '';
					foreach((array)$StudentList as $thisStudentID )
					{
						if($MarksheetFeedback[$thisStudentID][$CmpSubjectID]==2)
						{
							$InProgressRemarks = "<span class='tabletextrequire'>*</span>";
							break;
						}	
					}
					
					# State of IsCompleted 
					$ProgressArr = array();
					$ProgressArr = $lreportcard->GET_MARKSHEET_SUBMISSION_PROGRESS($CmpSubjectID, $ReportID, $ClassID);
					
					# Initialization - Display Icon
					$CmpSubjectIcon = "";
					$MarksheetIcon = "-";
					$TCommentIcon = "-";
					$FeedbackIcon = "-";
					
					if($isAllColumnWeightZero == 0){	// if Any Report Column Weight is not Zero
						if($ck_ReportCard_UserType == "TEACHER")
						{
							switch($PeriodType)
							{
								case -1:	 	# Period not set yet (NULL)
								case 0:			# before the Period
									break;
								case 1:			# within  the Period
									//$MarksheetIcon = ($CmpSubjectDataArr[$j]['SchemeID'] != null && $CmpSubjectDataArr[$j]['SchemeID'] != "") ? '<a href="javascript:jEDIT_MARKSHEET(\''.$ClassID.'\', \''.$CmpSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" alt="'.$button_edit.' '.$eReportCard['Marksheet'].'"></a>' : 'X';
									if($CmpSubjectDataArr[$j]['SchemeID'] != null && $CmpSubjectDataArr[$j]['SchemeID'] != "")
									{
								    	$MarksheetIcon = '<a href="javascript:jEDIT_MARKSHEET(\''.$ClassID.'\', \''.$CmpSubjectID.'\', 1)">';
								    	// [2015-1008-1356-44164]
								    	$MarksheetIcon .= ($ReportType != "F" || ($isEdit && $ReportType == "F") || ($ReportType == "F" && $displayOverallColumnDueToAllZeroWeight)) ? '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" alt="'.$button_edit.' '.$eReportCard['Marksheet'].'">' : '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" alt="'.$button_view.' '.$eReportCard['Marksheet'].'">';
								    	$MarksheetIcon .= '</a>';
							    	}
							    	else {
								    	$MarksheetIcon = 'X';
							    	}
	 								$TCommentIcon = '<a href="javascript:jEDIT_TEACHER_COMMENT(\''.$ClassID.'\', \''.$CmpSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" alt="'.$button_edit.' '.$eReportCard['TeacherComment'].'"></a>';
									$FeedbackIcon = '<a href="javascript:jVIEW_FEEDBACK(\''.$ClassID.'\', \''.$SubjectID.'\', \''.$CmpSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" alt="'.$button_view.' '.$eReportCard['Feedback'].'"></a>'.$InProgressRemarks;
									break;
								case 2:			# after the Period
// 									$MarksheetIcon = ($CmpSubjectDataArr[$j]['SchemeID'] != null && $CmpSubjectDataArr[$j]['SchemeID'] != "") ? '<a href="javascript:jEDIT_MARKSHEET(\''.$ClassID.'\', \''.$CmpSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" alt="'.$button_preview.' '.$eReportCard['Marksheet'].'"></a>' : 'X';
// 									$TCommentIcon = '<a href="javascript:jEDIT_TEACHER_COMMENT(\''.$ClassID.'\', \''.$CmpSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" alt="'.$button_preview.' '.$eReportCard['TeacherComment'].'"></a>';
// 							    	$FeedbackIcon = '<a href="javascript:jVIEW_FEEDBACK(\''.$ClassID.'\', \''.$SubjectID.'\', \''.$CmpSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" alt="'.$button_preview.' '.$eReportCard['Feedback'].'"></a>'.$InProgressRemarks;
							    	
// 							    	$MarksheetIcon = ($CmpSubjectDataArr[$j]['SchemeID'] != null && $CmpSubjectDataArr[$j]['SchemeID'] != "") ? '-' : 'X';
									if($CmpSubjectDataArr[$j]['SchemeID'] != null && $CmpSubjectDataArr[$j]['SchemeID'] != "")
									{
								    	$MarksheetIcon = '<a href="javascript:jEDIT_MARKSHEET(\''.$ClassID.'\', \''.$CmpSubjectID.'\', 1)">';
								    	// [2015-1008-1356-44164]
								    	$MarksheetIcon .= ($ReportType != "F" || ($isEdit && $ReportType == "F") || ($ReportType == "F" && $displayOverallColumnDueToAllZeroWeight)) ? '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" alt="'.$button_edit.' '.$eReportCard['Marksheet'].'">' : '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" alt="'.$button_view.' '.$eReportCard['Marksheet'].'">';
								    	$MarksheetIcon .= '</a>';
							    	}
							    	else {
								    	$MarksheetIcon = 'X';
							    	}
									$TCommentIcon = '-';
									break;
							}
						}
						else
						{
							//$MarksheetIcon = ($CmpSubjectDataArr[$j]['SchemeID'] != null && $CmpSubjectDataArr[$j]['SchemeID'] != "") ? '<a href="javascript:jEDIT_MARKSHEET(\''.$ClassID.'\', \''.$CmpSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" alt="'.$button_edit.' '.$eReportCard['Marksheet'].'"></a>' : 'X';
							if($CmpSubjectDataArr[$j]['SchemeID'] != null && $CmpSubjectDataArr[$j]['SchemeID'] != "")
							{
						    	$MarksheetIcon = '<a href="javascript:jEDIT_MARKSHEET(\''.$ClassID.'\', \''.$CmpSubjectID.'\', 1)">';
						    	
								if($ck_ReportCard_UserType == "VIEW_GROUP") {
						    		$MarksheetIcon .= '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" alt="'.$button_view.' '.$eReportCard['Marksheet'].'">';
								}
						    	else {
						    		$MarksheetIcon .= ($ReportType != "F" || ($isEdit && $ReportType == "F") || ($ReportType == "F" && $displayOverallColumnDueToAllZeroWeight)) ? '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" alt="'.$button_edit.' '.$eReportCard['Marksheet'].'">' : '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" alt="'.$button_view.' '.$eReportCard['Marksheet'].'">';
						    	}
						    	
						    	$MarksheetIcon .= '</a>';
					    	}
					    	else {
						    	$MarksheetIcon = 'X';
					    	}
						    
							if($ck_ReportCard_UserType == "VIEW_GROUP") {
								$TCommentIcon = '<a href="javascript:jEDIT_TEACHER_COMMENT(\''.$ClassID.'\', \''.$CmpSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" alt="'.$button_view.' '.$eReportCard['TeacherComment'].'"></a>';
							}
							else {
								$TCommentIcon = '<a href="javascript:jEDIT_TEACHER_COMMENT(\''.$ClassID.'\', \''.$CmpSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" alt="'.$button_edit.' '.$eReportCard['TeacherComment'].'"></a>';
							}
							
							$FeedbackIcon = '<a href="javascript:jVIEW_FEEDBACK(\''.$ClassID.'\', \''.$SubjectID.'\', \''.$CmpSubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" alt="'.$button_view.' '.$eReportCard['Feedback'].'"></a>'.$InProgressRemarks;
						}
					
						// BIBA Cust - for component subject with subject topic
						if ($thisSubjectWithTopic){
							if($CmpSubjectDataArr[$j]['SchemeID'] != null && $CmpSubjectDataArr[$j]['SchemeID'] != ""){
								$MarksheetIcon = "-";
							}
							$FeedbackIcon = "-";
						}
						// BIBA Report - display "-" for Teacher Comment
						if($eRCTemplateSetting['Settings']['AutoSelectReportType']){
							$TCommentIcon = "-";
						}
					}
					
					# Content
					$display .= '
						<tr class="tablegreenrow1">
				          <td class="'.$row_css.' tabletext">'.$CmpSubjectName.'</td>
				          <td align="center" class="'.$row_css.' tabletext">
				          '. $MarksheetIcon .'
				          </td>
				          <td align="center" class="'.$row_css.' tabletext">'. $TCommentIcon .'</td>
				          <td align="center" class="'.$row_css.' tabletext">'. $FeedbackIcon .'</td>
				          <td class="'.$row_css.' tabletext">'.$LastModifiedDate.'</td>
				          <td class="'.$row_css.' tabletext">'.$LastModifiedBy.'</td>';
				     
				     # Complete Status Function 
				     /*
				     $display .= '<td align="center" class="'.$row_css.' tabletext">';
				     if($isAllColumnWeightZero == 0){	// if Any Report Column Weight is not Zero  
					     if($ck_ReportCard_UserType == "TEACHER"){
						     if($PeriodType != 1){	// within submission period
						     	if($PeriodType <= 0){
							     	$display .= "-";
						     	} else {
							     	if(count($ProgressArr) > 0 && $ProgressArr['IsCompleted']){	// show imcompleted status
							     		$display .= $eReportCard['Confirmed'];
							     	} else {	// show completed status
							     		$display .= $eReportCard['NotCompleted'];
							     	}
						  		}   	
						     } else {
							     $display .= $linterface->GET_SELECTION_BOX($CompleteArr, "name='complete_".$CmpSubjectID."_".$ClassID."' id='complete_".$CmpSubjectID."_".$ClassID."' class='tabletexttoptext'", "", ((count($ProgressArr) > 0) ? $ProgressArr['IsCompleted'] : 0));
						     }
					     } else if($ck_ReportCard_UserType == "ADMIN"){
					     	$display .= $linterface->GET_SELECTION_BOX($CompleteArr, "name='complete_".$CmpSubjectID."_".$ClassID."' id='complete_".$CmpSubjectID."_".$ClassID."' class='tabletexttoptext'", "", ((count($ProgressArr) > 0) ? $ProgressArr['IsCompleted'] : 0));
				     	 }
			     	 } else {
				     	 $display .= '-';
			     	 }
				     $display .= '</td>';
				     */
				     $display .= '</tr>';
				      	
				    $cnt++;
				}
			}
			
			/*
			# Check Box Master of "Completed"
			if($ck_ReportCard_UserType == "TEACHER"){
				if($PeriodType != 1){	// within submission period
					$completeAll = '';
				}
				else {	// out of submission period
					$completeAll .= $linterface->GET_SELECTION_BOX($CompleteAllArr, 'name="completeAll" id="completeAll" class="tabletexttoptext" onchange="setCompleteAll(\''.$SubjectID.'\')"', "", "");
				}
			}
			else if($ck_ReportCard_UserType == "ADMIN"){
				$completeAll .= $linterface->GET_SELECTION_BOX($CompleteAllArr, 'name="completeAll" id="completeAll" class="tabletexttoptext" onchange="setCompleteAll(\''.$SubjectID.'\')"', "", "");
			}
			*/
			
			$total = $eReportCard['Total']." ".$cnt;
		}
		
		# Button
		$display_button = '';
		/*
		if($cnt > 0){
			if(in_array(0, $AllSubjectColumnWeightStatus)){
				$display_button .= '<input name="Save" type="button" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$button_save.'" onclick="jSUBMIT_FORM()">';
				$display_button .= '<input name="Reset" type="button" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$button_reset.'" onclick="jRESET_FORM()">';
			}
		}
		*/
		$display_button .= '<input name="Back" type="button" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$button_back.'" onClick="jBACK()">';
		
		$ExportBtn = $linterface->Get_Content_Tool_v30("export", "javascript:js_Export();",$eReportCard['ManagementArr']['MarksheetRevisionArr']['MarksheetFeedbackArr']['ExportFeedback']);
		
		
		############################################################################################################
        
		# tag information
		$TAGS_OBJ[] = array($PAGE_TITLE, "", 0);
		
		# Navigation
		$PAGE_NAVIGATION[] = array($eReportCard['Management_MarksheetRevision'], "javascript:jBACK()"); 
		$PAGE_NAVIGATION[] = array($eReportCard['CmpSubjectMarksheet'], ''); 
		$PageNavigation = $linterface->GET_NAVIGATION($PAGE_NAVIGATION);
		
		$linterface->LAYOUT_START();
		
?>

<script language="javascript">
function jGET_SELECTBOX_SELECTED_VALUE(objName){
	var obj = document.getElementById(objName);
	var index = obj.selectedIndex;
	var val = obj[index].value;
	return val;
}

function setCompleteAll(parSubjectID){
	var classIDArr = document.getElementsByName("ClassIDList[]");
	var subjectIDArr = (document.getElementsByName("SubjectIDList[]")) ? document.getElementsByName("SubjectIDList[]") : "";
	var indexToSelect = "";
	
	var parStatus = jGET_SELECTBOX_SELECTED_VALUE("completeAll");
	if (parStatus == "completeAll"){
		indexToSelect = 0;
	}
	else if (parStatus == "notCompleteAll"){
		indexToSelect = 1;
	}
	
	if(parStatus != ""){
		if(classIDArr.length > 0 && subjectIDArr.length > 0){
			for(var i=0 ; i<classIDArr.length ; i++){
				for(var j=0 ; j<subjectIDArr.length ; j++){
					if(document.getElementById("complete_"+subjectIDArr[j].value+"_"+classIDArr[i].value)){
						var obj = document.getElementById("complete_"+subjectIDArr[j].value+"_"+classIDArr[i].value);
						obj.selectedIndex = indexToSelect;
						obj.options[indexToSelect].selected = true;
					}
				}
			}
		}
	}
}

function jCHANGE_MARKSHEET_REVISION(){
	var form_id = <?=(($ClassLevelID != "") ? $ClassLevelID : "''")?>;
	var report_id = jGET_SELECTBOX_SELECTED_VALUE("ReportType");
	var subject_id = <?=(($SubjectID != "") ? $SubjectID : "''")?>;
	var class_id = <?=(($ClassID != "") ? $ClassID : "''")?>;
	
	if(subject_id != "" && class_id != "" && form_id != "" && report_id != ""){
		location.href = "?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+subject_id+"&ClassID="+class_id;
	}
}

function jEDIT_MARKSHEET(class_id, target_subject_id){
	var form_id = <?=(($ClassLevelID != "") ? $ClassLevelID : "''")?>;
	var report_id = <?=(($ReportID != "") ? $ReportID : "''")?>;
	var class_id = <?=(($ClassID != "") ? $ClassID : "''")?>;
	var parent_subject_id = <?=(($SubjectID != "") ? $SubjectID : "''")?>;
	
	report_id = (report_id == "") ? jGET_SELECTBOX_SELECTED_VALUE("ReportType") : report_id;
	
	if(target_subject_id != "" && class_id != "" && form_id != "" && report_id != ""){
		location.href = "marksheet_edit.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+target_subject_id+"&ClassID="+class_id+"&ParentSubjectID="+parent_subject_id;
	}
}

function jEDIT_TEACHER_COMMENT(class_id, target_subject_id){
	var form_id = <?=(($ClassLevelID != "") ? $ClassLevelID : "''")?>;
	var report_id = <?=(($ReportID != "") ? $ReportID : "''")?>;
	var class_id = <?=(($ClassID != "") ? $ClassID : "''")?>;
	
	report_id = (report_id == "") ? jGET_SELECTBOX_SELECTED_VALUE("ReportType") : report_id;
	
	if(target_subject_id != "" && class_id != "" && form_id != "" && report_id != ""){
		location.href = "teacher_comment.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+target_subject_id+"&ClassID="+class_id;
	}
}

function jBACK(){
	var form_id = <?=(($ClassLevelID != "") ? $ClassLevelID : "''")?>;
	var report_id = <?=(($ReportID != "") ? $ReportID : "''")?>;
	var subject_id = <?=(($SubjectID != "") ? $SubjectID : "''")?>;
	
	location.href = "marksheet_revision.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+subject_id;
}

/* Form */
function jRESET_FORM(){
	var obj = document.FormMain;
	obj.reset();
}

function jSUBMIT_FORM(){
	var obj = document.FormMain;
	obj.submit();
}
/* End Form */

function jCOMPLETE_MARKSHEET(class_id, target_subject_id, isAll){
	var jCheckBoxObj = (isAll == 1) ? document.getElementById("completeAll") : document.getElementById("complete_"+target_subject_id+"_"+class_id);
	var jCheck = jCheckBoxObj.checked;
	var action = (jCheckBoxObj.value == 0) ? "COMPLETE" : "CANCEL";
	var msg = (jCheckBoxObj.value == 0) ? "<?=$eReportCard['jsAlertToCompleteMarksheet']?>" : "<?=$eReportCard['jsAlertToIncompleteMarksheet']?>";
		
	if(confirm(msg))
		completeMarksheet(class_id, target_subject_id, isAll, action);
	else {
		jCheckBoxObj.checked = (jCheckBoxObj.value == 1) ? true : false;
	}
}

// Start of AJAX 
var callback = {
    success: function ( o )
    {
    	alert(o.responseText);
    	jUpdateCompleteCheckBox(o.responseXML);
    }
}
// Main
function completeMarksheet(class_id, target_subject_id, isAll, action)
{
	var classlevel_id = <?=(($ClassLevelID != "") ? $ClassLevelID : "''")?>;
	var report_id = <?=(($ReportID != "") ? $ReportID : "''")?>;
	var subject_id = target_subject_id;

	report_id = (report_id == "") ? jGET_SELECTBOX_SELECTED_VALUE("ReportType") : report_id;
	
	var params = '';
	params += "?ActionCode="+action+"&ClassLevelID="+classlevel_id+"&ReportID="+report_id+"&SubjectID="+subject_id;
	if(class_id != null)
		params += "&ClassID="+class_id;
	params += (isAll == 1) ? "&isAll=1" : "&isAll=0";
	params += "&isCmpSubject=1";
	
    var path = "marksheet_revision_aj_update.php" + params;
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
}

function jUpdateCompleteCheckBox(xmlObj) 
{
	var actionInfo = xmlObj.getElementsByTagName("ActionCode");
	var classlevelinfo = xmlObj.getElementsByTagName("ClassLevelID");
	var subjectInfo = xmlObj.getElementsByTagName("SubjectID");
	var reportInfo = xmlObj.getElementsByTagName("ReportID");
	var isAll = xmlObj.getElementsByTagName("isAll");
	
	var classInfo = xmlObj.getElementsByTagName("ClassID");
	var isUpdate = xmlObj.getElementsByTagName("isUpdate");
	
	var action_code = actionInfo[0].childNodes[0].nodeValue;
	var isAllFlag = isAll[0].childNodes[0].nodeValue;
	
	for (var i=0 ; i<classInfo.length ; i++){
		var subject_id = subjectInfo[i].childNodes[0].nodeValue;
		var class_id = classInfo[i].childNodes[0].nodeValue;
		var jCheckBoxObj = document.getElementById("complete_"+subject_id+"_"+class_id);
		
		if(isUpdate[i].childNodes[0].nodeValue == 1){
			if(action_code == "COMPLETE"){
				jCheckBoxObj.checked = true;
				jCheckBoxObj.value = 1;
			}
			else if(action_code == "CANCEL"){
				jCheckBoxObj.checked = false;
				jCheckBoxObj.value = 0;
			}
		}
	}
	
	if(isAllFlag == 1){
		var jAllObj = document.getElementById("completeAll");
		if(action_code == "COMPLETE"){
			jAllObj.checked = true;
			jAllObj.value = 1;
		}
		else if(action_code == "CANCEL"){
			jAllObj.checked = false;
			jAllObj.value = 0;
		}
	}			
}
// End of AJAX

function jVIEW_FEEDBACK(class_id, target_subject_id , target_cmp_subject_id){
	var form_id = <?=(($ClassLevelID != "") ? $ClassLevelID : "''")?>;
	var report_id = <?=(($ReportID != "") ? $ReportID : "''")?>;
	
	report_id = (report_id == "") ? jGET_SELECTBOX_SELECTED_VALUE("ReportType") : report_id;
	subject_id = target_subject_id ;
	cmp_subject_id = target_cmp_subject_id;
	
	location.href = "marksheet_feedback/index.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&ParentSubjectID="+subject_id+"&SubjectID="+cmp_subject_id+"&ClassID="+class_id;
}

function js_Export()
{
	var obj = document.FormMain;
	obj.action = './marksheet_feedback/export_form_feedback.php';
	obj.submit();
	obj.action = './marksheet_complete_confirm.php';
}
</script>

<br/>
<form name="FormMain" method="POST" action="marksheet_complete_confirm.php">
<table width="100%" border="0" cellpadding"0" cellspacing="0">
	<tr>
		<td class="navigation"><?=$PageNavigation?></td>
		<td align="right"><?=$ReportTypeSelection?></td>
	</tr>
</table>
<br style="clear:both;" />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
	  <table width="100%" border="0" cellspacing="0" cellpadding="2" class="tabletext">
	  	<tr>
			<td valign="top" width="30%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eReportCard['Settings_ReportCardTemplates']?></td>
			<td class="tabletext" nowrap="nowrap"><?=$PeriodName?></td>
		</tr>
		<tr>
			<td valign="top" width="30%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eReportCard['Class']?></td>
			<td class="tabletext" nowrap="nowrap"><?=$ClassName?></td>
		</tr>
		<tr>
			<td valign="top" width="30%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eReportCard['Subject']?></td>
			<td class="tabletext" nowrap="nowrap"><?=$SubjectName?></td>
		</tr>
  	  </table>
  	  <br style="clear:both;" />
  	</td>
  </tr>  
  <tr>
    <td>
    	<?php if(!$eRCTemplateSetting['Settings']['SubjectTopics']) { ?>
   		 <div class="content_top_tool">
			<div class="Conntent_tool">
				<?=$ExportBtn?>
			</div>
		</div>
		<?php } ?>
      <table width="100%" border="0" cellspacing="0" cellpadding="4">
        <tr class="tablegreentop">
          <!--<td class="tabletoplink"><?=$eReportCard['Class']?></td>-->
          <td class="tabletoplink"><?=$eReportCard['Subject']?></td>
          <td width="80" align="center" class="tabletoplink"><?=$eReportCard['Marksheet']?></td>
          <td width="80" align="center" class="tabletoplink"><?=$eReportCard['TeacherComment']?></td>
          <td width="80" align="center" class="tabletoplink"><?=$eReportCard['Feedback']?></td>
          <td class="tabletoplink"><?=$eReportCard['LastModifiedDate']?></td>
          <td class="tabletoplink"><?=$eReportCard['LastModifiedBy']?></td>
          <!--<td width="80" align="center" class="tabletoplink"><?=$eReportCard['Confirmed'].' '.$completeAll?></td>-->
        </tr>
        <?=$display?>
      </table>
    </td>
  </tr>
  <tr class="tablegreenbottom">
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr>
    	  <td class="tabletext"><?=$total?></td>
    	</tr>
      </table>
    </td>
  </tr>
  <tr>
  	<td><span class="tabletextrequire">*</span><span class="tabletextremark"><?=$eReportCard['ManagementArr']['MarksheetRevisionArr']['MarksheetFeedbackArr']['FeedbackInProgress']?></span></td>
  </tr>
  
  <?php 
  	if($ck_ReportCard_UserType == "ADMIN" || $ck_ReportCard_UserType == "VIEW_GROUP" || ($ck_ReportCard_UserType == "TEACHER" && $PeriodType == 1) ){// within submission period 
  ?>
  <tr>
    <td>
      <br>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
      	<tr>
          <td height="4" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
      	</tr>
      	<tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td align="center" valign="bottom"><?=$display_button?></td>
              </tr>
          	</table>
          </td>
      	</tr>
      </table>
    </td>
  </tr>
  <?php } ?>
</table>

<input type="hidden" name="isProgress" id="isProgress" value="0"/>
<input type="hidden" name="ClassLevelID" id="ClassLevelID" value="<?=$ClassLevelID?>"/>
<input type="hidden" name="ReportID" id="ReportID" value="<?=$ReportID?>"/>
<input type="hidden" name="SubjectID" id="SubjectID" value="<?=$SubjectID?>"/>
<input type="hidden" name="ClassIDList[]" id="ClassIDList" value="<?=$ClassID?>"/>
<input type="hidden" name="MarksheetRevision" id="MarksheetRevision" value="2"/>

<?php
if($SubjectID != ""){
	if(count($CmpSubjectDataArr) > 0){
		for($j=0 ; $j<count($CmpSubjectDataArr) ; $j++){
			echo "<input type=\"hidden\" name=\"SubjectIDList[]\" id=\"SubjectIDList\" value=\"".$CmpSubjectDataArr[$j]['SubjectID']."\"/>\n";
		}
	}
}
?>

</form>
<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>