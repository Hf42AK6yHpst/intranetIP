<?php
// Editing:
/*
 * Modification Log:
 * 20200728 (Bill)  [2020-0724-0936-58207]
 * 	- Display correct Year Report Column Score when $eRCTemplateSetting['Settings']['TemplateSettings_ConsolidatedReportWithTermReportAssessment'] = true
 * 20160311 (Bill)	[2016-0310-1616-26066]
 *  - ensure UserLogin can be exported - Consolidate report
 * 20110829 (Ivan)
 * 	- added estimated mark logic
 */
 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

#################################################################################################

include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
$lreportcard = new libreportcard2008j();
//$lreportcard = new libreportcard();

$lexport = new libexporttext();

# Get Data
// Tags Type - $TagsType

// Period (Term x, Whole Year, etc)
$PeriodArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID, $ReportID);
$PeriodName = (count($PeriodArr) > 0) ? $PeriodArr['SemesterTitle'] : '';

// Class
$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
for($i=0 ; $i<count($ClassArr) ;$i++){
	if($ClassArr[$i]['ClassID'] == $ClassID)
		$ClassName = $ClassArr[$i]['ClassName'];
}

// SubjectName
$SubjectName = $lreportcard->GET_SUBJECT_NAME($SubjectID);
$SubjectName = str_replace("&nbsp;", "_", $SubjectName);

# Load Settings - Calculation
// Get the Calculation Method of Report Type - 1: Right-Down,  2: Down-Right
$SettingCategory = 'Calculation';
$categorySettings = $lreportcard->LOAD_SETTING($SettingCategory);
$TermCalculationType = $categorySettings['OrderTerm'];
$FullYearCalculationType = $categorySettings['OrderFullYear'];

# Load Settings - Storage & Display
$SettingStorDisp = 'Storage&Display';
$storDispSettings = $lreportcard->LOAD_SETTING($SettingStorDisp);


# Get component subject(s) if any
$CmpSubjectArr = array();
$isCalculateByCmpSub = 0;
$isCmpSubject = $lreportcard->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($SubjectID);
if(!$isCmpSubject){
	$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
	
	$CmpSubjectIDArr = array();
	if(!empty($CmpSubjectArr)){	
		for($i=0 ; $i<count($CmpSubjectArr) ; $i++){
			$CmpSubjectIDArr[] = $CmpSubjectArr[$i]['SubjectID'];
			
			$CmpSubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $CmpSubjectArr[$i]['SubjectID'],0,0,$ReportID);
			if(count($CmpSubjectFormGradingArr) > 0 && $CmpSubjectFormGradingArr['ScaleInput'] == "M")
				$isCalculateByCmpSub = 1;
		}
	}
}


# Loading Report Template Data
$basic_data = $lreportcard->returnReportTemplateBasicInfo($ReportID);			// Basic  Data
$column_data = $lreportcard->returnReportTemplateColumnData($ReportID); 		// Column Data
$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
$ColumnSize = sizeof($column_data);
$ReportType = $basic_data['Semester'];

# Get Existing Report Calculation Method
$CalculationType = ($ReportType == "F") ? $FullYearCalculationType : $TermCalculationType;

# Get Report Column Weight
$WeightAry = array();
if($CalculationType == 2){
	if(count($column_data) > 0){
    	for($i=0 ; $i<sizeof($column_data) ; $i++){
    		//2012-0611-0920-45073
			//$OtherCondition = "SubjectID IS NULL AND ReportColumnID = ".$column_data[$i]['ReportColumnID'];
			$OtherCondition = "SubjectID = '$SubjectID' AND ReportColumnID = ".$column_data[$i]['ReportColumnID'];
			$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
			
			$WeightAry[$weight_data[0]['ReportColumnID']][$SubjectID] = $weight_data[0]['Weight'];
		}
	}
}
else {
	$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID);  
	
	for($i=0 ; $i<sizeof($weight_data) ; $i++)
		$WeightAry[$weight_data[$i]['ReportColumnID']][$weight_data[$i]['SubjectID']] = $weight_data[$i]['Weight'];
}


### Check if all Column Weight Zero
$isAllColumnWeightZero = true;
foreach((array)$WeightAry as $thisReportColumnID => $thisArr)
{
	if ($thisArr[$SubjectID] != 0 && $thisReportColumnID != '')
	{
		$isAllColumnWeightZero = false;
	}
}

# Student Info Array
if ($SubjectGroupID != '')
{
	$StudentArr = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupID, $ClassLevelID, '', 1);
	
	$obj_SubjectGroup = new subject_term_class($SubjectGroupID);
	$SubjectGroupName = $obj_SubjectGroup->Get_Class_Title();
	
	// Need the same format for export
	$showClassName = false;
	$Class_SG_Title = "ClassName";
	$Class_SG_Name = $SubjectGroupName;
}
else
{
	$TaughtStudentArr = $lreportcard->Get_Student_List_Taught_In_This_Consolidated_Report($ReportID, $_SESSION['UserID'], $ClassID);
	$TaughtStudentList = '';
	if (count($TaughtStudentArr > 0))
		$TaughtStudentList = implode(',', $TaughtStudentArr);
		
	$StudentArr = $lreportcard->GET_STUDENT_BY_CLASS($ClassID, $TaughtStudentList, 1);
	//$StudentArr = $lreportcard->GET_STUDENT_BY_CLASS($ClassID, '', 1);
	$showClassName = false;
	
	$Class_SG_Title = "ClassName";
	$Class_SG_Name = $ClassName;
}
$StudentSize = sizeof($StudentArr);

$StudentIDArr = array();
if(count($StudentArr) > 0){
	for($i=0 ; $i<sizeof($StudentArr) ; $i++)
		$StudentIDArr[$i] = $StudentArr[$i]['UserID'];
}

# Subject Grading Scheme
$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0,0,$ReportID);



# Scheme Range Info
$SchemeGrade = array();
$GradeInfo = array();
if(count($SubjectFormGradingArr) > 0){
	$SchemeID = $SubjectFormGradingArr['SchemeID'];
	$ScaleInput = $SubjectFormGradingArr['ScaleInput'];
	$ScaleDisplay = $SubjectFormGradingArr['ScaleDisplay'];
	
	$SchemeInfo = $lreportcard->GET_GRADING_SCHEME_INFO($SchemeID);
	$SchemeMainInfo = $SchemeInfo[0];
	$SchemeMarkInfo = $SchemeInfo[1];
	$SchemeType = $SchemeMainInfo['SchemeType'];
	
	
	if($SchemeType == "H" && $ScaleInput == "G"){
		$SchemeGrade = $lreportcard->GET_GRADE_FROM_GRADING_SCHEME_RANGE($SchemeID);
		if(!empty($SchemeGrade)){
			foreach($SchemeGrade as $GradeRangeID => $grade)
				$GradeInfo[$GradeRangeID] = $grade;
		}
	}
	else if($SchemeType == "PF"){
		$PassFailInfo['P'] = $SchemeMainInfo['Pass'];
		$PassFailInfo['F'] = $SchemeMainInfo['Fail'];
	}
	
}

# Get MarkSheet OVERALL Score Info for the cases when it is (Term Report OR Whole Year Report) AND 
# when the scheme type is Honor-Based (H) AND its scaleinput is Grade(G) OR
# when the scheme type is PassFail-Based (PF)
$MarksheetOverallScoreInfoArr = array();
if(($TagsType == 0 || $TagsType == 2) && (($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF" || ($isAllColumnWeightZero == true && $SchemeType == "H" && $ScaleInput == "M")) ){
	$MarksheetOverallScoreInfoArr = $lreportcard->GET_MARKSHEET_OVERALL_SCORE($StudentIDArr, $SubjectID, $ReportID);
}


####################################################################################################
# General Approach dealing with Search Term Result From Whole Year Report not From Term Report
# Can deal with Customization of SIS dealing with secondary template 
$isAllWholeReport = 0;
if($ReportType == "F"){
	$ReportTypeArr = array();
	$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);
	if(count($ReportTypeArr) > 0){
		$flag = 0;
		for($j=0 ; $j<count($ReportTypeArr) ; $j++){
			if($ReportTypeArr[$j]['ReportID'] != $ReportID){
				if($ReportTypeArr[$j]['Semester'] != "F")	$flag = 1;
			}
		}
		if($flag == 0) $isAllWholeReport = 1;
	}
}

# Get the Corresponding Whole Year ReportID from 
# the ReportColumnID of the Current Whole Year Report 
$WholeYearColumnHasTemplateArr = array();
if($isAllWholeReport == 1){
	if($ReportType == "F" && $ColumnSize > 0){
		for($j=0 ; $j<$ColumnSize ; $j++){
			$ReportColumnID = $column_data[$j]['ReportColumnID'];
			# Return Whole Year Report Template ID - ReportID 
			$WholeYearColumnHasTemplateArr[$ReportColumnID] = $lreportcard->CHECK_WHOLE_YEAR_REPORT_TEMPLATE_FROM_COLUMN($ReportID, $column_data[$j]['SemesterNum'], $ClassLevelID);
			
			# Check Whether the Return WholeYear ReportID is not greater the Current ReportID
			# if User creates All Reports sequently
			if($WholeYearColumnHasTemplateArr[$ReportColumnID] != false){
				if($WholeYearColumnHasTemplateArr[$ReportColumnID] > $ReportID)
					$WholeYearColumnHasTemplateArr[$ReportColumnID] = false;
			}
		}
	}
}
####################################################################################################
		
$MarksheetScoreInfoArr = array();
$ColumnHasTemplateAry = array();
$WeightedMark = array();

if($ColumnSize > 0){
    for($i=0 ; $i<$ColumnSize ; $i++){	
	    
	    if($isAllWholeReport == 1 && $WholeYearColumnHasTemplateArr[$column_data[$i]['ReportColumnID']] != false){
			#######################################################################################################################################
			# for the Whole Year Report which defined any Term Column in other Whole Year Report	added on 22 May by Jason
			// have defined term report template 
			$WholeYearReportID = $WholeYearColumnHasTemplateArr[$column_data[$i]['ReportColumnID']];
			$WholeYearReportColumnData = $lreportcard->returnReportTemplateColumnData($WholeYearReportID); 		// Column Data
			$WholeYearReportColumnWeightData = $lreportcard->returnReportTemplateSubjectWeightData($WholeYearReportID);  
			
			
			if(count($WholeYearReportColumnWeightData) > 0){
				for($j=0 ; $j<sizeof($WholeYearReportColumnWeightData) ; $j++){
					if($WholeYearReportColumnWeightData[$j]['SubjectID'] == $SubjectID && $WholeYearReportColumnWeightData[$j]['ReportColumnID'] != ""){
						$Weight[$WholeYearReportColumnWeightData[$j]['ReportColumnID']] = $WholeYearReportColumnWeightData[$j]['Weight'];
					}
				}
			}
			
			$WholeYearReportColumnExist[$column_data[$i]['ReportColumnID']] = false;
			if(count($WholeYearReportColumnData) > 0){
				for($j=0 ; $j<sizeof($WholeYearReportColumnData) ; $j++){
					if($WholeYearReportColumnData[$j]['SemesterNum'] == $column_data[$i]['SemesterNum']){
						$WholeYearReportColumnExist[$column_data[$i]['ReportColumnID']] = true;
						if($Weight[$WholeYearReportColumnData[$j]['ReportColumnID']] != 0){
							$MarksheetScoreInfoArr[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_MARKSHEET_SCORE($StudentIDArr, $SubjectID, $WholeYearReportColumnData[$j]['ReportColumnID']);
						} else {
							$MarksheetScoreInfoArr[$column_data[$i]['ReportColumnID']] = '--';
						}
					}
				}
			}
			#######################################################################################################################################
	  	} else {	
			/* 
			* For Whole Year Report use only
			* Check for the Mark Column is allowed to fill in mark when any term does not have related report template 
			* by SemesterNum (ReportType) && ClassLevelID
			* An array is created as a control variable $ColumnHasTemplateAry
			*/
			if($ReportType == "F"){
				$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] = $lreportcard->CHECK_REPORT_TEMPLATE_FROM_COLUMN($column_data[$i]['SemesterNum'], $ClassLevelID);
			}
			else {
				$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] = 1;
			}
			
			# Get Data from 
			# Subject without Component Subjects OR 
			# Subject having Component Subjects AND it is not Calculated By Component Subjects  OR 
			# Subject having Component Subjects with ScaleInput is Grade(G) even is Calculated By Component Subjects 
		    if(count($CmpSubjectArr) == 0 || 
		    	(count($CmpSubjectArr) > 0 && !$isCalculateByCmpSub) || 
				(count($CmpSubjectArr) > 0 && $isCalculateByCmpSub && (($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF")) ){
				$MarksheetScoreInfoArr[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_MARKSHEET_SCORE($StudentIDArr, $SubjectID, $column_data[$i]['ReportColumnID']);
			} 
			else {	
				# Conidtion: Subject contains Component Subjects & Mark is calculated by its components subjects
				# Get Parent Subject Marksheet Score by Marksheet Score of Component Subjects if any
				if($ReportType == "F" && !$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']]){
					// for whole year report use only
					$ParentSubMarksheetScoreInfoArr[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_STUDENT_TERM_SUBJECT_MARK_WITHOUT_TEMPLATE($StudentIDArr, $ReportID, $column_data[$i]['ReportColumnID'], $SubjectID, $CmpSubjectIDArr);
				}
				else {						
					// for term report use only
					$ParentSubMarksheetScoreInfoArr[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_PARENT_SUBJECT_MARKSHEET_SCORE($StudentIDArr, $SubjectID, $CmpSubjectIDArr, $ReportID, $column_data[$i]['ReportColumnID']);
				}
			}

			# For Whole Year Report use only when case i
			if($ReportType == "F"){
				# Case 1: Get the marksheet OVERALL score when the inputscale is Grade(G) OR PassFail(PF)
				if(($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF" || ($isAllColumnWeightZero == true && $SchemeType == "H" && $ScaleInput == "M")){
					if($ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']]){
						$TermReportID = $ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']];
						$MarksheetOverallScoreInfoArr[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_MARKSHEET_OVERALL_SCORE($StudentIDArr, $SubjectID, $TermReportID);
					}
				}
				else if($ScaleInput == "M"){
					# Case 2: Calculating The Weighted Mark of Parent Subjects if having Component Subjects
					if($ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']]){
						$TermReportID = $ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']];

                        // [2020-0724-0936-58207]
                        $TermReportColumnID = $column_data[$i]['TermReportColumnID'];
                        $excludedTermReportColumnWeight = $eRCTemplateSetting['Settings']['TemplateSettings_ConsolidatedReportWithTermReportAssessment_ExcludeWeightDisplay'];

						if(!$isCalculateByCmpSub){
							# Get Student Term Subject Mark of common subject without any component subjects
							//$WeightedMark[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_STUDENT_TERM_SUBJECT_MARK($StudentIDArr, $TermReportID, $SubjectID, $CmpSubjectIDArr, 1, $ReportID);
                            $WeightedMark[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_STUDENT_TERM_SUBJECT_MARK($StudentIDArr, $TermReportID, $SubjectID, $CmpSubjectIDArr, 1, $ReportID, $TermReportColumnID, $excludedTermReportColumnWeight);
						} else {
							# Get Student Term Subject Mark of a subject by manipulating data of its component subjects
							//$WeightedMark[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_STUDENT_TERM_SUBJECT_MARK($StudentIDArr, $TermReportID, $SubjectID, $CmpSubjectIDArr, $TermCalculationType, $ReportID);
                            $WeightedMark[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_STUDENT_TERM_SUBJECT_MARK($StudentIDArr, $TermReportID, $SubjectID, $CmpSubjectIDArr, $TermCalculationType, $ReportID, $TermReportColumnID, $excludedTermReportColumnWeight);
						}
					}
					else {		
						// does not have term report Template for whole year report
						if($isCalculateByCmpSub){
							$ParentSubMarksheetScoreInfoArr[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_STUDENT_TERM_SUBJECT_MARK_WITHOUT_TEMPLATE($StudentIDArr, $ReportID, $column_data[$i]['ReportColumnID'], $SubjectID, $CmpSubjectIDArr);
							$hasDirectInputMark = 0;
						}
					}
				}
			}
		} # End of if($isAllWholeReport == 0)
	}
}

# Get Real Overall Term Subject Mark of a subject having Component Subjects Using Right-Down / Down-Right Method when 
# Conition: TagsType is 1 - "Preview Mark" & it is Term Report 
$OverallTermSubjectWeightedMark = array();
if($TagsType == 1 && $ReportType != "F" && !empty($CmpSubjectIDArr) ){
	$OverallTermSubjectWeightedMark = $lreportcard->GET_STUDENT_TERM_SUBJECT_MARK($StudentIDArr, $ReportID, $SubjectID, $CmpSubjectIDArr, $TermCalculationType);
}


# Header 
$Content = "WebSAMSRegNumber,$Class_SG_Title,ClassNumber,StudentName";
//$exportColumn[] = "WebSAMSRegNumber";
//$exportColumn[] = $Class_SG_Title;
//$exportColumn[] = "ClassNumber";
//$exportColumn[] = "StudentName";
$exportColumn[0][] = $eReportCard['ImportHeader']['ClassName']['En'];
$exportColumn[0][] = $eReportCard['ImportHeader']['ClassNumber']['En'];
$exportColumn[0][] = $eReportCard['ImportHeader']['UserLogin']['En'];
$exportColumn[0][] = $eReportCard['ImportHeader']['WebSamsRegNo']['En'];
$exportColumn[0][] = $eReportCard['ImportHeader']['StudentName']['En'];
$exportColumn[1][] = $eReportCard['ImportHeader']['ClassName']['Ch'];
$exportColumn[1][] = $eReportCard['ImportHeader']['ClassNumber']['Ch'];
$exportColumn[1][] = $eReportCard['ImportHeader']['UserLogin']['Ch'];
$exportColumn[1][] = $eReportCard['ImportHeader']['WebSamsRegNo']['Ch'];
$exportColumn[1][] = $eReportCard['ImportHeader']['StudentName']['Ch'];


for($i=0; $i<$ColumnSize ; $i++){
	list($ReportColumnID, $ColumnTitle, $DefaultWeight, $SemesterNum) = $column_data[$i];
	$BlockMarksheet = $column_data[$i]['BlockMarksheet'];
	
	//$ColumnTitle = ($ColumnTitle != "" && $ColumnTitle != null) ? convert2unicode($ColumnTitle, 1, 2) : $ColumnTitleAry[$ReportColumnID];
	$ColumnTitle = ($ColumnTitle != "" && $ColumnTitle != null) ? $ColumnTitle : $ColumnTitleAry[$ReportColumnID];
	$Content .= ",".$ColumnTitle;
	
	if($WeightAry[$ReportColumnID][$SubjectID] != 0 && $WeightAry[$ReportColumnID][$SubjectID] != null && $WeightAry[$ReportColumnID][$SubjectID] != ""){
		
		# Case 1: when it is Whole Year Report AND its Report Column has defined Report Template
		# Case 2: when it has component subjects AND the mark is calculated by its component subjects
		if(($ColumnHasTemplateAry[$ReportColumnID] && $ReportType == "F") || 
		   (!empty($CmpSubjectArr) && $isCalculateByCmpSub && $ScaleInput == "M") || 
		   ($isAllWholeReport == 1 && $WholeYearReportColumnExist[$ReportColumnID]) || 
		   $lreportcard->Is_Marksheet_Blocked($BlockMarksheet)
		  ){
			$Content .= "(N/A)";
//			$exportColumn[] = $ColumnTitle."(N/A)";
			$exportColumn[0][] = $ColumnTitle."(N/A)";
			$exportColumn[1][] = "(".$ColumnTitle."(N/A)".")";
		}
		else {
//			$exportColumn[] = $ColumnTitle;
			$exportColumn[0][] = $ColumnTitle;
			$exportColumn[1][] = "(".$ColumnTitle.")";
		}
	}
	else {
		$Content .= "(N/A)";
//		$exportColumn[] = $ColumnTitle."(N/A)";
		$exportColumn[0][] = $ColumnTitle."(N/A)";
		$exportColumn[1][] = "(".$ColumnTitle."(N/A)".")";

	}
	//$NewColumnArray[] = $ColumnArray[$i];
}

# Add a Overall Term Subject Mark OR Overall Subject Mark Header when the case is 
# The ScaleInput is Grade(G) OR PassFail(PF)
if( (($TagsType == 0 || $TagsType == 2) && (($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF")) ||
	 $TagsType == 1  || ($isAllColumnWeightZero && $SchemeType == "H" && $ScaleInput == "M") ){
	 	
	 	
	if($ReportType != "F"){
		$Content .= ",".$eReportCard['TermSubjectMark'];
//		$exportColumn[] = $eReportCard['TermSubjectMark'];
		$exportColumn[0][] = $eReportCard['TermSubjectMark'];
		$exportColumn[1][] = "(".$eReportCard['TermSubjectMark'].")";

	}
	else {
		$Content .= ",".$eReportCard['OverallSubjectMark'];
//		$exportColumn[] = $eReportCard['OverallSubjectMark'];
		$exportColumn[0][] = $eReportCard['OverallSubjectMark'];
		$exportColumn[1][] = "(".$eReportCard['OverallSubjectMark'].")";

	}
}
$Content .= "\n";


// content
for($j=0; $j<$StudentSize; $j++){
	// Term Report
	if ($SubjectGroupID != '') {
		list($StudentID, $WebSAMSRegNo, $StudentNo, $StudentName, $ClassName, $UserLogin) = $StudentArr[$j];
	}
	// Consolidate Report
	// [2016-0310-1616-26066] fixed problem - export English Name instead of UserLogin
	else {
		list($StudentID, $WebSAMSRegNo, $StudentNo, $StudentName, $ClassName) = $StudentArr[$j];
		// get UserLogin directly
		$UserLogin = $StudentArr[$j]["UserLogin"];
	}
	
	//if ($showClassName)
	//	$thisClassDisplay = $ClassName.'-'.$StudentNo;
	//else
		$thisClassDisplay = $StudentNo;
	
	$Content .= trim($WebSAMSRegNo).",".trim($Class_SG_Name).",".trim($thisClassDisplay).",".trim($StudentName);
//	$row[] = trim($WebSAMSRegNo);
//	$row[] = trim($ClassName);
//	$row[] = trim($thisClassDisplay);
//	$row[] = trim($StudentName);
    $row[] = trim($ClassName);
	$row[] = trim($StudentNo);
	$row[] = trim($UserLogin);
	$row[] = trim($WebSAMSRegNo);
	$row[] = trim($StudentName);

	if(count($column_data) > 0){
		$OverallSubjectMark = 0;
		$isShowOverallMark = 1;
		$SpecialCaseCountArr = array("*"=>0, "/"=>0, "N.A."=>0);		// For Special Case Use only
		
		# Prepare an Array for converting mark to weighted mark 
		# AllColumnMarkArr is refer the all Column Marks of each student
		if($TagsType == 1){
			$AllColumnMarkArr = array();
			for($r=0 ;$r<$ColumnSize ; $r++){
				if( $WeightAry[$column_data[$r]['ReportColumnID']][$SubjectID] != 0 && 
					$WeightAry[$column_data[$r]['ReportColumnID']][$SubjectID] != null && 
					$WeightAry[$column_data[$r]['ReportColumnID']][$SubjectID] != "" ){
					if($ReportType == "F" && $ColumnHasTemplateAry[$column_data[$r]['ReportColumnID']] && $ScaleInput == "M"){	// whole year with term template
						if(!empty($CmpSubjectArr) && $isCalculateByCmpSub){		// Subject with component Subjects
							$AllColumnMarkArr[$column_data[$r]['ReportColumnID']] = $WeightedMark[$column_data[$r]['ReportColumnID']][$StudentID];
						} else {	// subject without component subject
							$AllColumnMarkArr[$column_data[$r]['ReportColumnID']] = $WeightedMark[$column_data[$r]['ReportColumnID']][$StudentID];
						}
					} else {	// term report OR whole year without term template
						if(!empty($CmpSubjectArr) && $isCalculateByCmpSub){		// Subject with component Subjects
							$AllColumnMarkArr[$column_data[$r]['ReportColumnID']] = $ParentSubMarksheetScoreInfoArr[$column_data[$r]['ReportColumnID']][$StudentID];
						} else {	// subject without component subject
							if($MarksheetScoreInfoArr[$column_data[$r]['ReportColumnID']][$StudentID]['MarkType'] == "SC")
				    			$AllColumnMarkArr[$column_data[$r]['ReportColumnID']] = $MarksheetScoreInfoArr[$column_data[$r]['ReportColumnID']][$StudentID]['MarkNonNum'];
				    		else 
								$AllColumnMarkArr[$column_data[$r]['ReportColumnID']] = $MarksheetScoreInfoArr[$column_data[$r]['ReportColumnID']][$StudentID]['MarkRaw'];
						}
					}
		    	}
		    }
		}
		
		# Loop Report Column
	    for($i=0 ; $i<sizeof($column_data) ; $i++){
		    list($ReportColumnID, $ColumnTitle, $DefaultWeight, $SemesterNum) = $column_data[$i];
		    
		    // Mark Type : M-Mark, G-Grade, PF-PassFail, SC-SpecialCase
			$MarkType = (count($MarksheetScoreInfoArr) > 0) ? $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkType'] : '';
			
			// Get Data From DB
			if(count($MarksheetScoreInfoArr) > 0){
			    if($MarkType == "G" || $MarkType == "PF" || $MarkType == "SC"){
			    	$tmpValue = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkNonNum'];
		    	} else if($MarkType == "M"){
			    	$tmpValue = ($MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkRaw'] != -1) ? $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkRaw'] : '';
		    	} else {
			    	$tmpValue = '';
		    	}
		    }
		    else {
			    $tmpValue = '';
		    }
		    
		    // Check whether the Data From DB is special case or not
		    $isSpecialCase = ($SchemeType == "PF") ? $lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2($tmpValue) : $lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1($tmpValue);
		    
		    // Generate Corresponding Scale Display 
		    if($TagsType == 0){		// Export From Input Mark's View : Input->Mark/Grade , Display->Mark/Grade
		    	if($WeightAry[$ReportColumnID][$SubjectID] != 0 && $WeightAry[$ReportColumnID][$SubjectID] != null && $WeightAry[$ReportColumnID][$SubjectID] != ""){
			    	if($ColumnHasTemplateAry[$ReportColumnID] && $ReportType == "F"){
				    	// according to student's term subject mark calculated by term report
				    	if(($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF"){
						    if(count($MarksheetOverallScoreInfoArr) > 0){
							    $tmpType = $MarksheetOverallScoreInfoArr[$ReportColumnID][$StudentID]['MarkType'];
							    $tmpNonNum = $MarksheetOverallScoreInfoArr[$ReportColumnID][$StudentID]['MarkNonNum'];
							    if($tmpType != "SC")
							    	$Value = ($SchemeType == "PF") ? $PassFailInfo[$tmpNonNum] : $GradeInfo[$tmpNonNum];
							    else 
							    	$Value = $tmpNonNum;
						    }
					    } else if($ScaleInput == "M"){
					    	if(!empty($CmpSubjectArr) && $isCalculateByCmpSub){	// case for generate the mark of parent subject from component subjects
							    $tmpVal = $WeightedMark[$ReportColumnID][$StudentID];
							    
							    if($tmpVal == "-" || $tmpVal == "*" || $tmpVal == "/" || $tmpVal == "N.A."){
								    $Value = $tmpVal;
							    } else if($tmpVal != "NOTCOMPLETED" && $tmpVal != ""){
					    			//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpVal, $storDispSettings["SubjectScore"]) : round($tmpVal, 0);
					    			//$Value = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $storDispSettings["SubjectScore"]);
					    			$roundVal = $lreportcard->ROUND_MARK($tmpVal, "SubjectScore");
							    	$Value = $lreportcard->ROUND_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $ReportType, "SubjectScore");
				    			} else {
					    			$Value = "";
				    			}
						    } else {	// case for generate the mark of subject without any component subjects
						    	if($SchemeType == "H"){
						    		$tmpVal = $WeightedMark[$ReportColumnID][$StudentID];
						    		
						    		if($tmpVal == "-" || $tmpVal == "*" || $tmpVal == "/" || $tmpVal == "N.A."){
									    $Value = $tmpVal;
								    } else if($tmpVal != "NOTCOMPLETED"){
						    			//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpVal, $storDispSettings["SubjectScore"]) : round($tmpVal, 0);
						    			//$Value = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $storDispSettings["SubjectScore"]);
						    			$roundVal = $lreportcard->ROUND_MARK($tmpVal, "SubjectScore");
							    		$Value = $lreportcard->ROUND_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $ReportType, "SubjectScore");
					    			} else {
						    			$Value = "";
					    			}
					    		}
				    		}
			    		}
			    	} else {
			    		// Case ii : ReportType is Whole Year and ReportColumn does not have its own Template => Can modify Mark/Grade
						// Case iii: ReportType is Term => Can modify Mark/Grade
			    		if(!empty($CmpSubjectArr) && $isCalculateByCmpSub && $ScaleInput == "M"){
						    $tmpVal = $ParentSubMarksheetScoreInfoArr[$ReportColumnID][$StudentID];
						    
						    if($ReportType == "F"){	// Case ii
						    	if($tmpVal == "-" || $tmpVal == "*" || $tmpVal == "/" || $tmpVal == "N.A."){
									$Value = $tmpVal;
								} else if($tmpVal != "NOTCOMPLETED" && $tmpVal != ""){
					    			//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpVal, $storDispSettings["SubjectScore"]) : round($tmpVal, 0);
					    			//$Value = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $storDispSettings["SubjectScore"]);
					    			$roundVal = $lreportcard->ROUND_MARK($tmpVal, "SubjectScore");
							    	$Value = $lreportcard->ROUND_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $ReportType, "SubjectScore");
							    	
				    			} else {
					    			$Value = "";
				    			}
			    			} else {	// Case iii 
			    				// $tmpVal = 0 case checking for not completed return value
			    				if($tmpVal == "-" || $tmpVal == "*" || $tmpVal == "/" || $tmpVal == "N.A."){
									$Value = $tmpVal;
								} else if($tmpVal != ""){
								    //$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpVal, $storDispSettings["SubjectScore"]) : round($tmpVal, 0);
								    //$Value = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $storDispSettings["SubjectScore"]);;
								    $roundVal = $lreportcard->ROUND_MARK($tmpVal, "SubjectScore");
							    	$Value = $lreportcard->ROUND_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $ReportType, "SubjectScore");
							    	
							    } else {
								    $Value = $tmpVal;
							    }
						    }
					    } else {
						    if($SchemeType == "H"){
							    if(!$isSpecialCase){
							    	$Value = ($ScaleInput == "G") ? $GradeInfo[$tmpValue] : $tmpValue;
						    	} else {
							    	$Value = $tmpValue;
						    	}
				    		} else if($SchemeType == "PF"){
						    	$Value = ($isSpecialCase) ? $tmpValue : $PassFailInfo[$tmpValue];
				    		}
				    		
				    		// Check if the mark is estimated or not
    						$Value = ($MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['IsEstimated'])? $lreportcard->Get_Estimated_Score_Display($Value) : $Value;
			    		}
		    		}
	    		} else {
		    		$Value = "--";
	    		}
    		}
    		else if($TagsType == 1){	// Export From Preview Mark's View : Input->Mark/Grade , Display->Weighted Mark calculated by Percentage or Weight
    			if($WeightAry[$ReportColumnID][$SubjectID] != 0 && $WeightAry[$ReportColumnID][$SubjectID] != null && $WeightAry[$ReportColumnID][$SubjectID] != ""){
	    			if($ColumnHasTemplateAry[$ReportColumnID] && $ReportType == "F" && $ScaleInput == "M"){
		    			// according to student's term subject mark calculated by term report
		    			if(!empty($CmpSubjectArr) && $isCalculateByCmpSub){	// case for generate the mark of parent subject from component subjects
						    $tmpRaw = $WeightedMark[$ReportColumnID][$StudentID];
						    
						    if($tmpRaw == "-" || $tmpRaw == "*" || $tmpRaw == "/" || $tmpRaw == "N.A."){
					    		$Value = $tmpRaw;
				    		} else if($tmpRaw != "NOTCOMPLETED"){
					    		# for displaying mark with weight calculation
					    		if ($eRCTemplateSetting['UseSumOfAssessmentToBeTheTermOverallMark'] == true)
					    			$tmpWeightedVal = $tmpRaw;
					    		else
							    	$tmpWeightedVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpRaw, $CalculationType);
							    	
				    			//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpWeightedVal, $storDispSettings["SubjectScore"]) : round($tmpWeightedVal, 0);
				    			$roundVal = $lreportcard->ROUND_MARK($tmpWeightedVal, "SubjectScore");
				    			$Value = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $storDispSettings["SubjectScore"]);
				    			
				    			# for calculating real overall subject mark
				    			if ($eRCTemplateSetting['UseSumOfAssessmentToBeTheTermOverallMark'] == true)
					    			$tmpWeightedVal = $tmpRaw;
					    		else
				    				$tmpWeightedVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpRaw, $CalculationType, $AllColumnMarkArr);
				    			//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpWeightedVal, $storDispSettings["SubjectScore"]) : round($tmpWeightedVal, 0);
				    			$OverallSubjectMark += $tmpWeightedVal;
			    			} else {
				    			$Value = "";
				    			$isShowOverallMark = 0;
			    			}
					    }
					    else {	// case for generate the mark of subject withour any component subjects		    			
			    			if($SchemeType == "H"){
				    			$tmpVal = $WeightedMark[$ReportColumnID][$StudentID];
				    			
						    	if($tmpVal == "-" || $tmpVal == "*" || $tmpVal == "/" || $tmpVal == "N.A."){
						    		$Value = $tmpVal;
					    		} else if($tmpVal != "NOTCOMPLETED" && $tmpVal != ""){
						    		# for displaying mark with weight calculation
						    		if ($eRCTemplateSetting['UseSumOfAssessmentToBeTheTermOverallMark'] == true)
						    			$tmpWeightedVal = $tmpRaw;
						    		else
							    		$tmpWeightedVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpVal, $CalculationType);
					    			//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpWeightedVal, $storDispSettings["SubjectScore"]) : round($tmpWeightedVal, 0);
					    			$roundVal = $lreportcard->ROUND_MARK($tmpWeightedVal, "SubjectScore");
								    $Value = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $storDispSettings["SubjectScore"]);
								    
								    # for calculating real overall subject mark
								    if ($eRCTemplateSetting['UseSumOfAssessmentToBeTheTermOverallMark'] == true)
						    			$tmpWeightedVal = $tmpRaw;
						    		else
							    		$tmpWeightedVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpVal, $CalculationType, $AllColumnMarkArr);
						    		//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpWeightedVal, $storDispSettings["SubjectScore"]) : round($tmpWeightedVal, 0);
								    $OverallSubjectMark += $tmpWeightedVal;
				    			} else {
					    			$Value = "";
					    			$isShowOverallMark = 0;
				    			}
				    		}
			    		}
	    			}
	    			else {	// according to student's term subject mark provided by teacher without term report
	    				if(!empty($CmpSubjectArr) && $isCalculateByCmpSub){
						    $tmpRaw = $ParentSubMarksheetScoreInfoArr[$ReportColumnID][$StudentID];
						  	
						    if($tmpRaw == "-" || $tmpRaw == "*" || $tmpRaw == "/" || $tmpRaw == "N.A."){
							    $Value = $tmpRaw;
						    } else if($tmpRaw != ""){
							    # for displaying mark with weight calculation
							    if ($eRCTemplateSetting['UseSumOfAssessmentToBeTheTermOverallMark'] == true)
					    			$tmpVal = $tmpRaw;
					    		else
					    			$tmpVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpRaw, $CalculationType);
					    		//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpVal, $storDispSettings["SubjectScore"]) : round($tmpVal, 0);
					    		$roundVal = $lreportcard->ROUND_MARK($tmpVal, "SubjectScore");
					    		$Value = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $storDispSettings["SubjectScore"]);
					    		
					    		# for calculating real overall subject mark
					    		if ($eRCTemplateSetting['UseSumOfAssessmentToBeTheTermOverallMark'] == true)
					    			$tmpVal = $tmpRaw;
					    		else
					    			$tmpVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpRaw, $CalculationType, $AllColumnMarkArr);
					    		//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpVal, $storDispSettings["SubjectScore"]) : round($tmpVal, 0);
					    		$OverallSubjectMark += $tmpVal;
				    		} else {
					    		$Value = $tmpRaw;
					    		$isShowOverallMark = 0;
				    		}
					    }
					    else {
				    		if($SchemeType == "H"){	
					    		if(!$isSpecialCase){
						    		if($ScaleInput == "M"){
					    				if($tmpValue != "" && $tmpValue >= 0){
						    				# for displaying mark with weight calculation
								    		$weightedMark = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpValue, $CalculationType);
								    		//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($weightedMark, $storDispSettings["SubjectScore"]) : round($weightedMark, 0);
								    		$roundVal = $lreportcard->ROUND_MARK($weightedMark, "SubjectScore");
								    		$Value = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $storDispSettings["SubjectScore"]);
								    		
								    		# for calculating real overall subject mark
								    		$weightedMark = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpValue, $CalculationType, $AllColumnMarkArr);
								    		//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($weightedMark, $storDispSettings["SubjectScore"]) : round($weightedMark, 0);
								    		$OverallSubjectMark += $weightedMark;
							    		}
							    		else {
								    		$Value = "";
								    		$isShowOverallMark = 0;
							    		}
					    			}
					    			else {
							    		$Value = "";
						    		}
				    			}
				    			else {
					    			if($tmpValue == "*" || $tmpValue == "/" || $tmpValue == "N.A."){
								    	$SpecialCaseCountArr[$tmpValue]++;
							    	}
						    		$Value = $tmpValue;
					    		}
				    		}
			    		}
		    		}
	    		}
	    		else {
	    			$Value = "--";
    			}
    		}
    		else if($TagsType == 2){	// Export From Preview Grade's View : Input->Mark/Grade , Display->Grade
    			if($WeightAry[$ReportColumnID][$SubjectID] != 0 && $WeightAry[$ReportColumnID][$SubjectID] != null && $WeightAry[$ReportColumnID][$SubjectID] != ""){
	    			if($ColumnHasTemplateAry[$ReportColumnID] && $ReportType == "F"){
		    			# according to student's term subject mark calculated by term report
		    			if(($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF"){
						    if(count($MarksheetOverallScoreInfoArr) > 0){
							    $tmpType = $MarksheetOverallScoreInfoArr[$ReportColumnID][$StudentID]['MarkType'];
							    $tmpNonNum = $MarksheetOverallScoreInfoArr[$ReportColumnID][$StudentID]['MarkNonNum'];
							    if($tmpType != "SC")
							    	$Value = ($SchemeType == "PF") ? $PassFailInfo[$tmpNonNum] : $GradeInfo[$tmpNonNum];
							    else 
							    	$Value = $tmpNonNum;
						    }
					    }
					    else if($SchemeType == "H" && $ScaleInput == "M"){
				    		$tmpVal = $WeightedMark[$ReportColumnID][$StudentID];
				    		
				    		if($tmpVal == "+" || $tmpVal == "-" || $tmpVal == "abs"){
					    		$Value = "abs";
				    		} else if($tmpVal == "/" || $tmpVal == "*" || $tmpVal == "N.A."){
					    		$Value = $tmpVal;
				    		} else if($tmpVal != "NOTCOMPLETED"){
				    			$Value = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $tmpVal);
			    			} else {
				    			$Value = "";
			    			}
			    		}
			    	}
			    	else {	// according to student's term subject mark provided by teacher without term report
			    		if($SchemeType == "H"){
				    		if(!$isSpecialCase){
					    		$Value = ($ScaleInput == "G") ? $GradeInfo[$tmpValue] : $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $tmpValue);
				    		}
				    		else {
					    		$Value = $tmpValue;
				    		}		    		
			    		}
		    		}
	    		}
	    		else {
		    		$Value = "--";
	    		}
    		}
    		
    		$Content .= ",".$Value;
		    $row[] = $Value;
	    }
	    
	    # Generate Input Selection Box For ScaleInput is Grade(G) OR SchemeType is PassFail(PF)
  		if($TagsType == 0){
	  		$tmpType = (count($MarksheetOverallScoreInfoArr) > 0) ? $MarksheetOverallScoreInfoArr[$StudentID]['MarkType'] : "";
      		$tmpValue = (count($MarksheetOverallScoreInfoArr) > 0) ? $MarksheetOverallScoreInfoArr[$StudentID]['MarkNonNum'] : "";
      		
      		$Value = '';
      		if ($isAllColumnWeightZero == true && $SchemeType == "H" && $ScaleInput == "M")
      		{
	      		$Value = $tmpValue;
      		}
      		else if($SchemeType == "H" && $ScaleInput == "G"){
	      		$Value = ($tmpType == "SC") ? $tmpValue : $GradeInfo[$tmpValue];
      		}
      		else if($SchemeType == "PF" ){
	      		$Value = ($tmpType == "SC") ? $tmpValue : $PassFailInfo[$tmpValue];
  			}
  			$row[] = $Value;
  		}
  		else if($TagsType == 2){
      		$tmpType = (count($MarksheetOverallScoreInfoArr) > 0) ? $MarksheetOverallScoreInfoArr[$StudentID]['MarkType'] : "";
      		$tmpValue = (count($MarksheetOverallScoreInfoArr) > 0) ? $MarksheetOverallScoreInfoArr[$StudentID]['MarkNonNum'] : "";
      		
      		$GradeVal = '';
      		if($SchemeType == "H" && $ScaleInput == "G"){
	      		$GradeVal = ($tmpType == "SC") ? $tmpValue : $SchemeGrade[$tmpValue];
	      		$row[] = $GradeVal;
      		}
  		}
  		else if($TagsType == 1){
		    # Re-Assign the Overall Term Subject Mark for Term Report AND Subject having component subjects
      		# in order to ensure the consistence of Term Subject mark with the Calculation Method in Settings
      		if($ReportType != "F" && !empty($CmpSubjectArr)){
	      		if ($OverallTermSubjectWeightedMark[$StudentID] != 0)
	      			$OverallSubjectMark = $OverallTermSubjectWeightedMark[$StudentID];
	      		$isShowOverallMark = ((string)$OverallSubjectMark != "NOTCOMPLETED")? 1 : 0;
      		}
		    
		    if($isShowOverallMark){
			    $setSpecialCase = 0;
	      		foreach($SpecialCaseCountArr as $case => $cnt){
		      		if($cnt == count($column_data)){
		      			$setSpecialCase = 1;
		      			$setSpeicalCaseValue = $case;
	      			}
	      		}
	      		
	      		if($setSpecialCase){
		      		$Content .= ",".$setSpeicalCaseValue;
		      		$row[] = $setSpeicalCaseValue;
	      		}
	      		else {
				    //$tmpOverall = ($storDispSettings["SubjectScore"] != "") ? round($OverallSubjectMark, $storDispSettings["SubjectTotal"]) : round($OverallSubjectMark, 0);
			    	//$Content .= ",".$lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($tmpOverall, $storDispSettings["SubjectTotal"]);
			    	//$row[] = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($tmpOverall, $storDispSettings["SubjectTotal"]);
			    	
			    	// Get the subject overall from MS if all column's weights are zero
      				if ($isAllColumnWeightZero == true && $SchemeType == "H" && $ScaleInput == "M")
      				{
	      				$thisMSOverallScoreArr = $lreportcard->GET_MARKSHEET_OVERALL_SCORE(array($StudentID), $SubjectID, $ReportID);
						$OverallSubjectMark = $thisMSOverallScoreArr[$StudentID]['MarkNonNum'];
					}
								
			    	$tmpOverall = $lreportcard->ROUND_MARK($OverallSubjectMark, "SubjectTotal");
			    	$Content .= ",".$lreportcard->ROUND_MARK_TO_RELATIVE_DECIMAL_PLACES($tmpOverall, $ReportType, "SubjectTotal");
		      		$row[] = $lreportcard->ROUND_MARK_TO_RELATIVE_DECIMAL_PLACES($tmpOverall, $ReportType, "SubjectTotal");
		    	}
	    	}
	    	else {
		    	$Content .= ",";
		    	$row[] = $eReportCard['NotCompleted'];
	    	}
	    }
	    $Content .= "\n";
		$rows[] = $row;
		unset($row);
    }
    
}


// Output the file to user browser
if($TagsType == 0)
	$Prefix = "Raw_";
else if($TagsType == 1)
	$Prefix = "Weighted_";
else if($TagsType == 2)
	$Prefix = "Grade_";
	
$set1 = array("&nbsp;", " ");
$set2 = array("-", ",");

//$filename = $Prefix."Marksheet_".$PeriodName."_".$SubjectName."_".$Class_SG_Name;
$filename = $SubjectName.'_'.$Class_SG_Name.'_'.$Prefix."Marksheet";
$filename = str_replace($set2, "_", $filename);
$filename = str_replace($set1, "_", $filename);
$filename = $filename.".csv";

//$export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn);
$export_content = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($rows, $exportColumn, "", "\r\n", "", 0, "11");

intranet_closedb();
$lexport->EXPORT_FILE($filename, $export_content);

//output2browser($Content, $filename);

?>