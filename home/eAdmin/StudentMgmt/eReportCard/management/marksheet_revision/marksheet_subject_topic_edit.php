<?php
# Edited by 
/*
 * 20201103 Bill:   [2020-0915-1830-00164]
 *      - support term setting    ($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings'])
 * 20160617 Bill:
 * 		- added remarks for students not complete fill in topic score
 * 20160427 Bill:	[2016-0330-1524-42164]
 * 		- added loading message when update subject topic score
 * 		- fixed: error when changing report column
 * 20151208 Bill:
 * 		- added drop down list to select available subject groups
 * 		- fixed confirm box message "marksheet is completed" problem
 * 20151203 Bill:	[2015-1111-1214-16164]
 * 		- modified layout for main subject
 * 20151201 Bill:
 * 		- allow users to update inputted scores when changing current student
 * 		- added input field nextStudentID for redirect in update.php
 * 20151124 Bill:	[2015-1111-1214-16164]
 * 		- for main subject, also display subject component score
 * 20150817 Bill:
 * 		- modified js function jCHECK_INPUT_MARK() for full mark checking
 * 20150508 Bill:
 * 		- create file for BIBA Cust (copy from marksheet_subject_topic_edit.php)
 */

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	}
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_schedule.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_subjectTopic.php");
	
	$lreportcard = new libreportcard2008j();
	$lreportcard_ui = new libreportcard_ui();
	$lreportcard_schedule = new libreportcard_schedule();
	$lreportcard_topic = new libreportcard_subjectTopic();
	
    if ($lreportcard->hasAccessRight())
    {
        $linterface = new interface_html();
        					
		# Report Template
		$ReportSetting = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		$SemID = $ReportSetting['Semester'];
        $SemNum = $lreportcard->Get_Semester_Seq_Number($SemID);
		$ReportTypeTW = $SemID == "F" ? "W" : "T";

		// [2020-0915-1830-00164]
        $targetTermSeq = 0;
		if ($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings'] && $ReportTypeTW == "T") {
            $targetTermSeq = ($SemNum == 1 || $SemNum == 2) ? 1 : 2;
        }
		
		// Update SubjectID and SubjectGroupID if select subject group from drop down list
		if($targetSubjectGroupID != ''){
			$SubjectGroupID = $targetSubjectGroupID;
			
			// get SubjectID
			include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
			$subjectClassObj = new subject_term_class($SubjectGroupID);
			$SubjectID = $subjectClassObj->SubjectID;
		}
		
		// Redirect to marksheet_revision.php if whole year report
		if ($ReportTypeTW == "W"){
			header("Location: marksheet_revision.php?ClassLevelID=$ClassLevelID&ReportID=$ReportID&SubjectID=$SubjectID");
			exit();
		}
		
		# Get MarkSheet Info
		// Tags of MarkSheet(0: Raw Mark, 1: Weighted Mark, 2: Converted Grade
		//$TagsType = (isset($TagsType) && $TagsType != "") ? $TagsType : 0;

		// Period (Term x, Whole Year, etc)
		$PeriodArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID, $ReportID);
		$PeriodName = (count($PeriodArr) > 0) ? $PeriodArr['SemesterTitle'] : '';
		
		# Load Settings - Calculation
		// Get the Calculation Method of Report Type - 1: Right-Down,  2: Down-Right
		$SettingCategory = 'Calculation';
		$categorySettings = $lreportcard->LOAD_SETTING($SettingCategory);
		$TermCalculationType = $categorySettings['OrderTerm'];
		//$FullYearCalculationType = $categorySettings['OrderFullYear'];
		
		# Load Settings - Storage & Display
		$SettingStorDisp = 'Storage&Display';
		$storDispSettings = $lreportcard->LOAD_SETTING($SettingStorDisp);
		
		# Load Settings - Absent & Exempt
		$SettingAbsentExempt = 'Absent&Exempt';
		$absentExemptSettings = $lreportcard->LOAD_SETTING($SettingAbsentExempt);
		
		# Get Info
		// Use Percentage
		$usePercentage = ($TermCalculationType == 1) ? 1 : 0;
			
		// Class
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		for($i=0 ; $i<count($ClassArr) ;$i++){
			if($ClassArr[$i][0] == $ClassID)
				$ClassName = $ClassArr[$i][1];
		}
		
		// Subject
		$SubjectName = $lreportcard->GET_SUBJECT_NAME($SubjectID);
		
		// Component subject(s)
		$isEdit = 1;						// default "1" as the marksheet is editable
		$CmpSubjectArr = array();
		$excludedCmpArr = array();
		$isCalculateByCmpSub = 0;			// set to "1" when one ScaleInput of component subjects is Mark(M)
		$isCmpSubject = $lreportcard->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($SubjectID);
		if(!$isCmpSubject){
			$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
//			
//			$CmpSubjectIDArr = array();
//			if(!empty($CmpSubjectArr)){
//				for($i=0 ; $i<count($CmpSubjectArr) ; $i++){
//					$CmpSubjectIDArr[] = $CmpSubjectArr[$i]['SubjectID'];
//					
//					$CmpSubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $CmpSubjectArr[$i]['SubjectID'],0,0,$ReportID);
//					if(count($CmpSubjectFormGradingArr) > 0 && $CmpSubjectFormGradingArr['ScaleInput'] == "M")
//						$isCalculateByCmpSub = 1;
//				}
//			}
//			// Check whether the marksheet is editable or not 
//			$isEdit = $lreportcard->CHECK_MARKSHEET_STATUS($SubjectID, $ClassLevelID, $ReportID, $CmpSubjectIDArr);
		}
		
		# Subject Groups
		$TargetTeacherID = $ck_ReportCard_UserType=="ADMIN"? "" : $UserID;
		$relatedSubjectAry = $lreportcard->returnSubjectwOrder($ClassLevelID, 1, $TargetTeacherID, '', 'Desc', 1, $ReportID, ($TargetTeacherID!=""));
		$relatedSubjectAry = array_keys((array)$relatedSubjectAry);
		$subjectGroupList = $lreportcard->Get_Teacher_Related_Subject_Groups_By_Subjects($SemID, $ClassLevelID, $TargetTeacherID, (array)$relatedSubjectAry);
		
		# Get Subject Topic
        // [2020-0915-1830-00164]
        //$SubjectTopics = $lreportcard_topic->getSubjectTopic($ClassLevelID, $SubjectID);
		$SubjectTopics = $lreportcard_topic->getSubjectTopic($ClassLevelID, $SubjectID, '', $targetTermSeq);
        $allSubjectTopic = array();
		$allSubjectTopic = $allSubjectTopic + $SubjectTopics;
		
		# Get Subject Topic (Component Subject)
		$cmpSubjectTopics = array();
		if(!empty($CmpSubjectArr) & count($CmpSubjectArr) > 0)
		{
			// loop components
			$cmpCount = count($CmpSubjectArr);
			for($csCount=0; $csCount<$cmpCount; $csCount++)
			{
				// Get subject topics of component
				$cmpSubjectID = $CmpSubjectArr[$csCount]['SubjectID'];

                // [2020-0915-1830-00164]
				//$cmpSubjectTopics[$cmpSubjectID] = $lreportcard_topic->getSubjectTopic($ClassLevelID, $cmpSubjectID);
                $cmpSubjectTopics[$cmpSubjectID] = $lreportcard_topic->getSubjectTopic($ClassLevelID, $cmpSubjectID, '', $targetTermSeq);
			}
		}
		
		# Get Status of Completed Action
		# $P"Submission" or "Verification"
		$PeriodAction = "Submission";
		$PeriodType = $lreportcard->COMPARE_REPORT_PERIOD($ReportID, $PeriodAction);
		if($PeriodType != 1 && $ck_ReportCard_UserType != "ADMIN") {
			$ViewOnly = true;
		}
		$SpecialPeriodType = $lreportcard_schedule->compareSpecialMarksheetSubmissionPeriodForTeacher($ReportID, $_SESSION['UserID']);
		if ($SpecialPeriodType == 1) {
			$ViewOnly = false;
		}
	    
		//Initialization of Grading Scheme
		$SchemeID = '';
		$ScaleInput = "M";		// M: Mark, G: Grade
		$ScaleDisplay = "M";	// M: Mark, G: Grade
		$SchemeType = "H";		// H: Honor Based, PF: PassFail
		$SchemeInfo = array();
		$SchemeMainInfo = array();
		$SchemeMarkInfo = array();
		$SubjectFormGradingArr = array();
		$possibleSpeicalCaseSet1 = array();
		$possibleSpeicalCaseSet2 = array();
		
		$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0,0,$ReportID);
		
		# Check if no any scheme is assigned to this Form(ClassLevelID) of this subject, redirect
		if(count($SubjectFormGradingArr) > 0){
			if($SubjectFormGradingArr['SchemeID'] == "" || $SubjectFormGradingArr['SchemeID'] == null){
				header("Location: marksheet_revision.php?ClassLevelID=$ClassLevelID&ReportID=$ReportID&SubjectID=$SubjectID&msg=1");
				exit();
			}
		}
		
		# Get Grading Scheme Info of that subject
		if(count($SubjectFormGradingArr) > 0){
			$SchemeID = $SubjectFormGradingArr['SchemeID'];
			$ScaleInput = $SubjectFormGradingArr['ScaleInput'];
			$ScaleDisplay = $SubjectFormGradingArr['ScaleDisplay'];
			
			$SchemeInfo = $lreportcard->GET_GRADING_SCHEME_INFO($SchemeID);
			$SchemeMainInfo = $SchemeInfo[0];
			$SchemeMarkInfo = $SchemeInfo[1];
			$SchemeType = $SchemeMainInfo['SchemeType'];
			
			$possibleSpeicalCaseSet1 = $lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
			
			if($SchemeType == "H" && $ScaleInput == "G"){	// Prepare Selection Box using Grade as Input
				$InputTmpOption = array();
				$InputGradeOption = array();
				$SchemeGrade = $lreportcard->GET_GRADE_FROM_GRADING_SCHEME_RANGE($SchemeID);
				$InputGradeOption[0] = array('', '--');
				if(!empty($SchemeGrade)){
					foreach($SchemeGrade as $GradeRangeID => $grade)
						$InputGradeOption[] = array($GradeRangeID, $grade);
				}
				
				for($k=0 ; $k<count($possibleSpeicalCaseSet1) ; $k++){
					//$InputGradeOption[] = array($possibleSpeicalCaseSet1[$k], $lreportcard->GET_MARKSHEET_SPECIAL_CASE_SET1_STRING($possibleSpeicalCaseSet1[$k]));
					$InputGradeOption[] = array($possibleSpeicalCaseSet1[$k], $possibleSpeicalCaseSet1[$k]);
				}
			}
			else if($SchemeType == "PF"){			// Prepare Selection Box using Pass/Fail as Input
				$SchemePassFail = array();
				$SchemePassFail['P'] = $SchemeMainInfo['Pass'];
				$SchemePassFail['F'] = $SchemeMainInfo['Fail'];
			
				$InputPFOption = array();
				$InputPFOption[0] = array('', '--');
				$InputPFOption[1] = array('P', $SchemeMainInfo['Pass']);
				$InputPFOption[2] = array('F', $SchemeMainInfo['Fail']);
				
				$possibleSpeicalCaseSet2 = $lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2();
				for($k=0 ; $k<count($possibleSpeicalCaseSet2) ; $k++){
					//$InputPFOption[] = array($possibleSpeicalCaseSet2[$k], $lreportcard->GET_MARKSHEET_SPECIAL_CASE_SET2_STRING($possibleSpeicalCaseSet2[$k]));
					$InputPFOption[] = array($possibleSpeicalCaseSet2[$k], $possibleSpeicalCaseSet2[$k]);
				}
			}
		}
		
		$InputGradeOptionAsso = array();
		$numOfOption = count($InputGradeOption);
		for ($i=0; $i<$numOfOption; $i++)
		{
			$thisIndex = $InputGradeOption[$i][0];
			$thisGrade = $InputGradeOption[$i][1];
			
			$InputGradeOptionAsso[$thisGrade] = $thisIndex;
		}
		
		// Mark Type Tags
		// [IMPORTANT] need to consider parent subject with components
		$params = "ClassLevelID=$ClassLevelID&ReportID=$ReportID&SubjectID=$SubjectID&ClassID=$ClassID&SubjectGroupID=$SubjectGroupID&isProgress=$isProgress&isProgressSG=$isProgressSG";
		$params .= (isset($ParentSubjectID) && $ParentSubjectID != "") ? "&ParentSubjectID=".$ParentSubjectID : "";
		$display_tagstype = '';
		$display_tagstype .= '
		  <div class="shadetabs">
            <ul>';
        
        //$display_tagstype .= '<li '.(($TagsType == 0) ? 'class="selected"' : '').'><a href="javascript:jCHECK_MARKSHEET_STATUS(0)">'.$eReportCard['InputMarks'].'</a></li>';
        $display_tagstype .= '<li class="selected"><a href="javascript:jCHECK_MARKSHEET_STATUS(0)">'.$eReportCard['InputMarks'].'</a></li>';
	    
	    //if ($eRCTemplateSetting['Marksheet']['ShowPreviewMarkTag']) {
	    //	$display_tagstype .= ($SchemeType != "PF" && $ScaleDisplay == "M" && $ScaleInput != "G") ? '<li '.(($TagsType == 1) ? 'class="selected"' : '').'><a href="javascript:jCHECK_MARKSHEET_STATUS(1)">'.$eReportCard['PreviewMarks'].'</a></li>' : '';
	    //}
	    //
	    //if ($eRCTemplateSetting['Marksheet']['ShowPreviewGradeTag']) {
	    //	$display_tagstype .= ($SchemeType != "PF" && $ScaleDisplay == "G") ? '<li '.(($TagsType == 2) ? 'class="selected"' : '').'><a href="javascript:jCHECK_MARKSHEET_STATUS(2)">'.$eReportCard['PreviewGrades'].'</a></li>' : '';
	    //}
	    
        $display_tagstype .= '
            </ul>
          </div>';
			
		# Get Data Either Assessment Column(s) in Term Or Term Column(s) in Wholely Report	
		// Loading Report Template Data
		$basic_data = $lreportcard->returnReportTemplateBasicInfo($ReportID);			// Basic  Data
		$column_data = $lreportcard->returnReportTemplateColumnData($ReportID); 		// Column Data
		
		$ReportTitle = $basic_data['ReportTitle'];
		$ReportType = $basic_data['Semester'];
		$isPercentage = $basic_data['PercentageOnColumnWeight'];
		
		# Report ColumnID
		foreach((array)$column_data as $thiscolumndata)
			$columnIDDataMap[$thiscolumndata['ReportColumnID']] = $thiscolumndata;
		if(!$SelectReportColumnID && count($column_data)>0)
			$SelectReportColumnID = $column_data[0]['ReportColumnID'];
		
		# Report Column drop donw list
		$contents = str_replace("<!--inputfield-->", $eReportCard['ReportColumn'], $Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['ChangeLostInputWarning']);
		$reportColumnOnchange = "if(document.getElementById('isOutdated').value!=1)
								{
									document.FormMain.action='marksheet_subject_topic_edit.php';
									document.FormMain.submit();
								} 
								else if (document.getElementById('isOutdated').value==1)
								{
									var formObj = document.getElementById('FormMain');
									var selectReportColumnID = jQuery(this).val();
									jQuery(this).val('$SelectReportColumnID');
									if(jCHECK_FORM(formObj) && confirm('".$contents."')){
										// short timeout
										setTimeout(function() {
											document.body.scrollTop = document.documentElement.scrollTop = 0;
										}, 15);
										document.getElementById('sendingDiv').style.display = '';
										jQuery(this).val(selectReportColumnID);
										document.FormMain.submit();
									}
								} 
								else {
									 jQuery(this).val('$SelectReportColumnID');
								}";
		$reportColumnOptions = "<select id=\"SelectReportColumnID\" name=\"SelectReportColumnID\" onchange=\"".$reportColumnOnchange."\">";
		foreach((array)$column_data as $thiscolumndata){
			$selectedOption = ($SelectReportColumnID==$thiscolumndata['ReportColumnID'])? " selected": "";
			$reportColumnOptions .= "<option value='".$thiscolumndata['ReportColumnID']."' $selectedOption>".$thiscolumndata['ColumnTitle']."</option>";
		}
		$reportColumnOptions .= "</select>";
		
		# Get Existing Report Calculation Method
		//$CalculationType = ($ReportType == "F") ? $FullYearCalculationType : $TermCalculationType;
		$CalculationType = $TermCalculationType;
		
		# Get Report Column Weight
		$WeightAry = array();
		if($CalculationType == 2){
			//$OtherCondition = "SubjectID = '$SubjectID' AND ReportColumnID = ".$column_data[$i]['ReportColumnID'];
			$OtherCondition = "SubjectID = '$SubjectID' AND ReportColumnID = '$SelectReportColumnID'";
			$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
			
			$WeightAry[$weight_data[0]['ReportColumnID']][$SubjectID] = $weight_data[0]['Weight'];
		} else {
			$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID);  
			
			for($i=0 ; $i<count($weight_data) ; $i++)
				$WeightAry[$weight_data[$i]['ReportColumnID']][$weight_data[$i]['SubjectID']] = $weight_data[$i]['Weight'];
		}
		
		### Check if all Column Weight Zero
		$isAllColumnWeightZero = true;
		foreach((array)$WeightAry as $thisReportColumnID => $thisArr)
		{
			if ($thisArr[$SubjectID] != 0 && $thisReportColumnID != '')
			{
				$isAllColumnWeightZero = false;
			}
		}
		
//		if ($isAllColumnWeightZero == true && $SchemeType == "H" && ($ScaleInput == "M" || $ScaleInput == "G"))
//			$displayOverallColumnDueToAllZeroWeight = true;
//		else
//			$displayOverallColumnDueToAllZeroWeight = false;
			
//		$IsCmpAllZeroWeightArr = $lreportcard->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
		
		// Student Info Array
		if ($SubjectGroupID != '')
		{
			$StudentArr = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupID, $ClassLevelID, '', 1, 1);
			$showClassName = true;
			
			$obj_SubjectGroup = new subject_term_class($SubjectGroupID, $GetTeacherList=true);
			$SubjectGroupName = $obj_SubjectGroup->Get_Class_Title();
			
			$SubjectOrGroupTitle = $Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'];
			$SubjectOrGroupNameDisplay = $SubjectGroupName;
			
			$TeacherName = implode('<br />', Get_Array_By_Key((array)$obj_SubjectGroup->ClassTeacherList, 'TeacherName'));
			$TeacherName = ($TeacherName=='')? $Lang['General']['EmptySymbol'] : $TeacherName;
			$SubjectTeacherTr = '';
			$SubjectTeacherTr .= '<tr>'."\r\n";
				$SubjectTeacherTr .= '<td class="field_title">'.$Lang['General']['Teacher'].'</td>'."\r\n";
				$SubjectTeacherTr .= '<td>'.$TeacherName.'</td>'."\r\n";
			$SubjectTeacherTr .= '</tr>'."\r\n";
		}
		else
		{
			//2014-0224-1422-33184
			$StudentArr = $lreportcard->Get_Marksheet_Accessible_Student_List_By_Subject_And_Class($ReportID, $_SESSION['UserID'], $ClassID, $ParentSubjectID, $SubjectID);
			
			$showClassName = false;
			
			$SubjectOrGroupTitle = $eReportCard['Class'];
			$SubjectOrGroupNameDisplay = $ClassName;
			
			$SkipSubjectGroupCheckingForGetMarkSheet = 1;
		}
		//$StudentSize = count($StudentArr);
		
		# Current Student
		if(!$targetStudentID && count($StudentArr)>0)
			$targetStudentID = $StudentArr[0]['UserID'];
			
		$StudentIDArr = array();
		if(count($StudentArr) > 0){
			for($i=0 ; $i<count($StudentArr) ; $i++){
				$StudentIDArr[$i] = $StudentArr[$i]['UserID'];
				if($targetStudentID == $StudentArr[$i]['UserID']){
					$targetStudentInfo = $StudentArr[$i];
					break;
				}
			}
		}
		
		# Student drop down list
		// allow users to update inputted scores when changing current student
		$contents = str_replace("<!--inputfield-->", $eReportCard['Student'], $Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['UpdateAfterChangeStudent']);
		$studentListOnchange = "if(document.getElementById('isOutdated').value!=1)
								{
									document.FormMain.action='marksheet_subject_topic_edit.php'; 
									document.FormMain.submit();
								} 
								else if (document.getElementById('isOutdated').value==1)
								{
									var formObj = document.getElementById('FormMain');
									var selectStudentID = jQuery(this).val();
									jQuery(this).val('$targetStudentID');
									if(jCHECK_FORM(formObj) && confirm('".$contents."')){
										// short timeout
										setTimeout(function() {
											document.body.scrollTop = document.documentElement.scrollTop = 0;
										}, 15);
										document.getElementById('sendingDiv').style.display = '';
										jQuery('input#nextStudentID').val(selectStudentID);
										document.FormMain.submit();
									}
								}
								else 
								{
									 jQuery(this).val('$targetStudentID');
								}";
		$targetStudentOptions = "<select id=\"targetStudentID\" name=\"targetStudentID\" onchange=\"".$studentListOnchange."\">";
		if(count($StudentArr) > 0)
		{
			list($ReportColumnID, $ColumnTitle, $DefaultWeight, $SemesterNum) = $columnIDDataMap[$SelectReportColumnID];
			$cmpCount = count($CmpSubjectArr);
			
			// loop students
			for($i=0 ; $i<count($StudentArr) ; $i++){
				$isEmpty = false;
				$curStudentID = $StudentArr[$i]['UserID'];
				
				// student with empty input checking
				// Main Subject
				if(count($SubjectTopics) > 0)
				{
					$emptyScoreCount = $lreportcard->IS_MARKSHEET_TOPIC_SCORE_EMPTY(array($curStudentID), $SubjectID, $ReportColumnID, count($SubjectTopics));
					if($emptyScoreCount == "all") {
						$isEmpty = true;
					}
				}
				
				// Component Subjects
				if(!$isEmpty && $cmpCount > 0)
				{
					// loop components
					for($csCount=0; $csCount<$cmpCount; $csCount++)
					{
						// Get subject topics of component subject
						$thisCmpSubjectID = $CmpSubjectArr[$csCount]['SubjectID'];
						$thisCmpSubjectTopicCount = count($cmpSubjectTopics[$thisCmpSubjectID]);
						
						// skip if no subject topic
						if($thisCmpSubjectTopicCount == 0){
							continue;
						}
						
						$emptyScoreCount = $lreportcard->IS_MARKSHEET_TOPIC_SCORE_EMPTY(array($curStudentID), $thisCmpSubjectID, $ReportColumnID, $thisCmpSubjectTopicCount);
						if($emptyScoreCount == "all") {
							$isEmpty = true;
							break;
						}
						
					}
				}
				
				// Option Settings
				$selectedOption = ($targetStudentID==$curStudentID)? " selected": "";
				$displayContent = $StudentArr[$i]['StudentName']." (".$StudentArr[$i]['ClassName']." - ".$StudentArr[$i]['ClassNumber'].")";
				if($selectStudentName=="" && ($targetStudentID==$curStudentID || $i==0 && $targetStudentID=="")){
					$selectStudentName = $displayContent;
				}
				$displayContent .= $isEmpty? " (".$eReportCard['NotCompleted']."*)" : "";
				
				$targetStudentOptions .= "<option value='".$curStudentID."' $selectedOption>".$displayContent."</option>";
			}
			
			// Set target Student if select subject group from drop down list, where student not in current subject group
			if($targetStudentID!="" && $selectStudentName==""){
				$targetStudentID = $StudentArr[0]["UserID"];
				$targetStudentInfo = $StudentArr[0];
			}
		}
		$targetStudentOptions .= "</select>";
		
		# Subject Group drop down list
		$contents = str_replace("<!--inputfield-->", $eReportCard['Student'], $Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['UpdateAfterChangeStudent']);
		$subjectGroupListOnchange = "if(document.getElementById('isOutdated').value!=1)
									{
										document.FormMain.action='marksheet_subject_topic_edit.php';
										document.FormMain.submit();
									} 
									else if (document.getElementById('isOutdated').value==1)
									{
										var formObj = document.getElementById('FormMain');
										var selectSubjectGroupID = jQuery(this).val();
										jQuery(this).val('$SubjectGroupID');
										if(jCHECK_FORM(formObj) && confirm('".$contents."')){
											// short timeout
											setTimeout(function() {
												document.body.scrollTop = document.documentElement.scrollTop = 0;
											}, 15);
											document.getElementById('sendingDiv').style.display = '';
											jQuery(this).val(selectSubjectGroupID);
											document.FormMain.submit();
										}
									}
									else 
									{
										 jQuery(this).val('$SubjectGroupID');
									}";
		$targetSubjectGroupOptions = "<select id=\"targetSubjectGroupID\" name=\"targetSubjectGroupID\" onchange=\"".$subjectGroupListOnchange."\">";
		if(count($subjectGroupList) > 0){
			for($i=0 ; $i<count($subjectGroupList) ; $i++){
				$selectedOption = ($SubjectGroupID==$subjectGroupList[$i]['SubjectGroupID'])? " selected": "";
				
				$contentSubjectName = Get_Lang_Selection($subjectGroupList[$i]['CH_DES'],$subjectGroupList[$i]['EN_DES']);
				$contentSubjectName = ($contentSubjectName=="")? Get_Lang_Selection($subjectGroupList[$i]['EN_DES'],$subjectGroupList[$i]['CH_DES']) : $contentSubjectName;
				$displayContent  = Get_Lang_Selection($subjectGroupList[$i]['ClassTitleB5'],$subjectGroupList[$i]['ClassTitleEN']);
				$displayContent .= $displayContent==""? Get_Lang_Selection($subjectGroupList[$i]['ClassTitleEN'],$subjectGroupList[$i]['ClassTitleB5']) : "";
				$displayContent .= " ( $contentSubjectName )";
				$targetSubjectGroupOptions .= "<option value='".$subjectGroupList[$i]['SubjectGroupID']."' $selectedOption>".$displayContent."</option>";
			}
		}
		$targetSubjectGroupOptions .= "</select>";
		if ($SubjectGroupID != '')
		{
			$SubjectOrGroupNameDisplay = $targetSubjectGroupOptions;
		}
		
		# Get MarkSheet OVERALL Score Info for the cases when it is (Term Report OR Whole Year Report) AND 
		# when the scheme type is Honor-Based (H) AND its scaleinput is Grade(G) OR
		# when the scheme type is PassFail-Based (PF)
//		$MarksheetOverallScoreInfoArr = array();
		//if(($TagsType == 0 || $TagsType == 2) && (($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF" || $displayOverallColumnDueToAllZeroWeight) ){
//		if(($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF" || $displayOverallColumnDueToAllZeroWeight){
//			$MarksheetOverallScoreInfoArr = $lreportcard->GET_MARKSHEET_OVERALL_SCORE($StudentIDArr, $SubjectID, $ReportID, $SkipSubjectGroupCheckingForGetMarkSheet);
//		}
		
		# Get MarkSheet Score Info / ParentSubMarksheetScoreInfoArr	
		$MarksheetScoreInfoArr = array();
		$WeightedMark = array();
		$hasDirectInputMark = 0;
			    
	    # for the general Term Report
	    /* 
		* Check for the Mark Column is allowed to fill in mark when any term does not have related report template 
		* by SemesterNum (ReportType) && ClassLevelID
		* An array is initialized as a control variable [$ColumnHasTemplateAry]
		* A variable is initialized as a control variable [$hasDirectInputMark], must be used with [$ReportType == "F"]
		* to control the Import function to be used or not
		*/
		$MarksheetScoreInfoArr[$SelectReportColumnID] = $lreportcard_topic->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($targetStudentID, $SubjectID, $SubjectTopics, $SelectReportColumnID, 0, '', $SkipSubjectGroupCheckingForGetMarkSheet);
//		$ColumnHasTemplateAry[$column_data[$i]['ReportColumnID']] = 1;
//			    
//	    # Get Data from Subject without Component Subjects OR Subject having Component Subjects with ScaleInput is Not Marked
//	    if(count($CmpSubjectArr) == 0 || (count($CmpSubjectArr) > 0 && !$isCalculateByCmpSub) || 
//	    	(count($CmpSubjectArr) >0 && $isCalculateByCmpSub && (($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF")) ){
//	    	//$MarksheetScoreInfoArr[$column_data[$i]['ReportColumnID']] = $lreportcard->GET_MARKSHEET_SCORE($StudentIDArr, $SubjectID, $column_data[$i]['ReportColumnID'],0,'', $SkipSubjectGroupCheckingForGetMarkSheet);
//	    	$MarksheetScoreInfoArr[$SelectReportColumnID] = $lreportcard->GET_MARKSHEET_SCORE($StudentIDArr, $SubjectID, $SelectReportColumnID, 0, '', $SkipSubjectGroupCheckingForGetMarkSheet);		
//		} 
//		else {
//			# Conidtion: Subject contains Component Subjects & Mark is calculated by its components subjects
//			// for term report use only
//			if ($IsCmpAllZeroWeightArr[$SelectReportColumnID] == 1) // all cmp zero weight => get the overall from the parent subject directly
//			{
//				$ParentSubMarksheetScoreInfoArr[$SelectReportColumnID] = $lreportcard->GET_PARENT_SUBJECT_MARKSHEET_SCORE($StudentIDArr, $SubjectID, $CmpSubjectIDArr, $ReportID, $SelectReportColumnID, "Term", 1, 0, $checkSpecialFullMark=false);
//			}
//			else {
//				$ParentSubMarksheetScoreInfoArr[$SelectReportColumnID] = $lreportcard->GET_PARENT_SUBJECT_MARKSHEET_SCORE($StudentIDArr, $SubjectID, $CmpSubjectIDArr, $ReportID, $SelectReportColumnID, "Term", 0, 0, $checkSpecialFullMark=false);
//			}
//		}
		
		/*
		* General Case for generating MarkSheet Column
		* Providing 2 Arrays : one is Column Data [$column_data] , the other is Student Data [$StudentArr]
		* Input Text Box ID : mark_[ReportColumnID]_[StudentID]
		*/
		# Main Content of Marksheet Reivision
		$display = '';
		$display .= '<table width="100%" cellpadding="4" cellspacing="0">';
		$display .= '<tr><td width="70%" align="left" class="tablegreentop tabletopnolink">'.$SubjectName.//$eReportCard['Settings_SubjectTopics'].
			  		'<td width="20px" align="left" class="tablegreentop tabletopnolink">'.$eReportCard['Student'].'</td>';
	    
	    list($ReportColumnID, $ColumnTitle, $DefaultWeight, $SemesterNum) = $columnIDDataMap[$SelectReportColumnID];
	    //list($ReportColumnID, $ColumnTitle, $DefaultWeight, $SemesterNum) = $column_data[$i];
	    //$ColumnTitle = ($ColumnTitle != "" && $ColumnTitle != null) ? convert2unicode($ColumnTitle, 1, 2) : $ColumnTitleAry[$ReportColumnID];
	    //$ColumnTitle = ($ColumnTitle != "" && $ColumnTitle != null) ? $ColumnTitle : $ColumnTitleAry[$ReportColumnID];
	    //$WeightOrPercent = ($usePercentage && $isPercentage) ? ($WeightAry[$ReportColumnID][$SubjectID] * 100)."%" : $WeightAry[$ReportColumnID][$SubjectID];
	    
	    //$display .= '<td align="center" class="tablegreentop tabletopnolink">'.$ColumnTitle.'<br />'.$WeightOrPercent;
	    $display .= '<td align="center" class="tablegreentop tabletopnolink">'.$targetStudentOptions;
//	    	$display .= '<div class="hideInPrint">';
//		    	$display .= $SubMarksheetBtn;
//	           	$display .= $CopyToSelection;
//            $display .= '</div>';

			$display .= '<input type="hidden" id="ColumnWeight_'.$ReportColumnID.'" name="ColumnWeight_'.$ReportColumnID.'" value="'.$WeightAry[$ReportColumnID][$SubjectID].'"/>';
            $display .= '<input type="hidden" id="ColumnID_'.$i.'" name="ColumnID_'.$i.'" value="'.$ReportColumnID.'"/>';	            
      	$display .= '</td>';
	          	
			
		# Add Empty Columns to make the mark input column more near to the student name
		$numOfEmptyColumn = 1;
		if ($numOfEmptyColumn > 0)
		{
  			for($i=0 ; $i<$numOfEmptyColumn ; $i++)
  			{
	  			$display .= '<td align="left" class="tablegreentop tabletopnolink" width="12%">&nbsp;</td>';
  			}
		}
		else
		{
  			$numOfEmptyColumn = 0;
		}

	    $display .= '</tr>';
	    
	    # Student Content of Marksheet Revision
	    # $OverallSubjectMark is used as a reference for user 
	    # $isShowOverallMark is used as a control to display term/overall subject mark
	    //$MarksheetOverallScoreInfoArr[0] = $lreportcard->GET_MARKSHEET_OVERALL_SCORE($StudentIDArr, $SubjectID, $ReportID, $SkipSubjectGroupCheckingForGetMarkSheet);
	    //$MarksheetOverallScoreInfoArr[0] = $lreportcard->GET_MARKSHEET_OVERALL_SCORE((array)$targetStudentID, $SubjectID, $ReportID, $SkipSubjectGroupCheckingForGetMarkSheet);
		list($StudentID, $WebSAMSRegNo, $StudentNo, $StudentName, $ClassName) = $targetStudentInfo;
	
		if(count($SubjectTopics) > 0){
			$targetLang = Get_Lang_Selection("NameCh", "NameEn");
			for($t_count=0; $t_count<count($SubjectTopics); $t_count++){
				$currentTopic = $SubjectTopics[$t_count];
				
				$tr_css = ($t_count % 2 == 1) ? "tablegreenrow2" : "tablegreenrow1";
				$td_css = ($t_count % 2 == 1) ? "attendanceouting" : "attendancepresent";
				
				$display .= '<tr class="'.$tr_css.' dataRow">
							<td width="70%" align="left" class="'.$td_css.' tabletext">'.$currentTopic[$targetLang].'
							<input type="hidden" name="SubjectTopics[]" id="SubjectTopic_'.$currentTopic['SubjectTopicID'].'" value="'.$currentTopic['SubjectTopicID'].'"/></td>';
				$display .= '<td align="left" nowrap="nowrap" class="'.$td_css.' tabletext">&nbsp;</td>';
				
		        # Check for Combination of Special Case of Input Mark / Term Mark
		        $SpecialCaseArr = $lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
		        foreach($SpecialCaseArr as $key){
		        	$SpecialCaseCountArr[$key] = array("Count"=>0, "Value"=>'', "isCount"=>'');
		    	}
		
			    //list($ReportColumnID, $ColumnTitle, $DefaultWeight, $SemesterNum) = $column_data[$i];
			    list($ReportColumnID, $ColumnTitle, $DefaultWeight, $SemesterNum) = $columnIDDataMap[$SelectReportColumnID];
				    
			    // Mark Type : M-Mark, G-Grade, PF-PassFail, SC-SpecialCase
				$MarkType = (count($MarksheetScoreInfoArr) > 0) ? $MarksheetScoreInfoArr[$ReportColumnID][$currentTopic['SubjectTopicID']]['MarkType'] : '';
				
			   // View of Raw Mark
		        # mark_id is used for paseting values to clipbroad from Excel
			    # mark_name is used for passing values to update
			    # [$i] of mark[$j][$i] == [$i] of ColumnID_$i for matching
			    $mark_name = "mark_".$currentTopic['SubjectTopicID'];
			    $mark_id = "mark[$t_count][0]";
			    # For Subject without component subject And ReportColumn does not have its Report Template to use only
			    if(count($MarksheetScoreInfoArr) > 0){
				    if($MarkType == "G" || $MarkType == "PF" || $MarkType == "SC"){
				    	$InputVal = $MarksheetScoreInfoArr[$ReportColumnID][$currentTopic['SubjectTopicID']]['MarkNonNum'];
			    	} else if($MarkType == "M"){
						# For ScaleInput is Mark & SchemeType is Honor-Based
						# if MarkRaw is -1, the MarkRaw has not been assigned yet.
						# if MarkRaw >= 0, the MarkRaw is valid
				    	$InputVal = ($MarksheetScoreInfoArr[$ReportColumnID][$currentTopic['SubjectTopicID']]['MarkRaw'] != -1) ? $MarksheetScoreInfoArr[$ReportColumnID][$currentTopic['SubjectTopicID']]['MarkRaw'] : '';
			    	} else {
				    	$InputVal = '';
			    	}
			    }
			    else {
				    $InputVal = '';
			    }
			    
			    # Set the InputMark/Grade to be an unchangable value if any column has defined template for whole year use & 
			    # Set Input Mark (Text) / Grade Method (Selection Box) / PassFail (Selection Box)
			    if($WeightAry[$ReportColumnID][$SubjectID] != 0 && $WeightAry[$ReportColumnID][$SubjectID] != null && $WeightAry[$ReportColumnID][$SubjectID] != ""){
				    # ReportType is Term => Can modify Mark/Grade
//				    if(!empty($CmpSubjectArr) && $isCalculateByCmpSub && $ScaleInput == "M"){
//					    $tmpVal = $ParentSubMarksheetScoreInfoArr[$ReportColumnID][$StudentID];
//					    
//						if($tmpVal == "-" || $tmpVal == "*" || $tmpVal == "/" || $tmpVal == "N.A."){
//						    $InputMethod = $tmpVal;
//							$InputMethod .= '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value="'.$tmpVal.'"/>&nbsp;';
//						} else if($tmpVal != ""){	//!strcmp($tmpVal, 0) || 
//							//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpVal, $storDispSettings["SubjectScore"]) : round($tmpVal, 0);
//				    		//$InputMethod = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $storDispSettings["SubjectScore"]);
//				    		$roundVal = $lreportcard->ROUND_MARK($tmpVal, "SubjectScore");
//				    		$InputMethod = $lreportcard->ROUND_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $ReportType, "SubjectScore");
//				    		$InputMethod .= '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value="'.$tmpVal.'"/>&nbsp;';
//				    		$OverallSubjectMark += $tmpVal;
//			    		} else {
//			    			$InputMethod = $tmpVal;
//				    		$InputMethod .= '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value="'.$tmpVal.'"/>&nbsp;';
//				    		$isShowOverallMark = 0;
//			    		}
//				    } else {
					    $otherPar = array(	'class'=>'textboxnum', 
					    					'onpaste'=>'isPasteContent(event,\''.$t_count.'\', \'0\')',
					    					'onkeyup'=>'isPasteContent(event, \''.$t_count.'\', \'0\')',
					    					'onkeydown'=>'isPasteContent(event, \''.$t_count.'\', \'0\')',
					    					'onchange'=>'jCHANGE_STATUS()'
					    				);
					    
					    $classname = '';
				        if($lreportcard->Is_Marksheet_Blocked($columnIDDataMap[$ReportColumnID]["BlockMarksheet"]))
				        {
				        	$disabled = " disabled ";
				        	$classname = " enableonsubmit ";
				        	$otherPar['disabled']='disabled';
				        	$otherPar['class']='textboxnum enableonsubmit';
				        }
				        else
				        {
				        	$disabled = '';
				        }
					    
					    $classname .= " Column_$ReportColumnID ";
					    $mark_tag = 'id="'.$mark_id.'" name="'.$mark_name.'" '.$disabled .' onchange="jCHANGE_STATUS()" onpaste="isPasteContent(event,'.$t_count.', 0)" onkeyup="isPasteContent(event, '.$t_count.', 0)" onkeydown="isPasteContent(event, '.$t_count.', 0)" onfocusout="jCHANGE_STATUS()" class="tabletexttoptext '.$classname.'"';
					    //$numOfInputBoxDisplayed++;
						if($SchemeType == "H" && $ScaleInput == "G") {
				    		$InputMethod = $linterface->GET_SELECTION_BOX($InputGradeOption, $mark_tag, '', $InputVal);
			    		} else if($SchemeType == "PF"){
				    		$InputMethod = $linterface->GET_SELECTION_BOX($InputPFOption, $mark_tag, '', $InputVal);
			    		} else {
			    			$thisInputValue = ($MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['IsEstimated'])? $lreportcard->Get_Estimated_Score_Display($InputVal) : $InputVal;
			    			$InputMethod = $lreportcard->GET_INPUT_TEXT($mark_name, $mark_id, $thisInputValue, '', $otherPar);
			    		}
//			    	}
			    }
			    else {
				    $InputMethod = "--";
			    }
			
				$_tdCss = ($t_count % 2 == 1) ? "attendanceouting" : "attendancepresent";//2014-0912-1716-13164 pt2
		        if($eRCTemplateSetting['Marksheet']['DiffCssStyleForAssessments']){ 
					$_tdCss = ($t_count % 2 == 0) ? $_tdCss."2" : $_tdCss;
				}
		  		$InputMethod = ($InputMethod == '')? '&nbsp;' : $InputMethod;
		  		$display .= '<td align="center" class="'.$_tdCss.' tabletext">'.$InputMethod.'</td>';
				
				# Add Empty Columns to make the mark input column more near to the student name
				for($i=0 ; $i<$numOfEmptyColumn ; $i++) {
					$display .= '<td align="center" class="'.$td_css.' tabletext">&nbsp;</td>';
				}
		        $display .= '</tr>';
			}
	    }
	    else{
	    	 // remove message display 
//	    	 $display .= '<tr class="tablegreenrow1 dataRow">
//							<td width="100%" align="left" nowrap="nowrap" class="attendancepresent tabletext" colspan="4">'.$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['NoExistingSubjectTopics'].'</td>
//						</tr>';
	    	 $display .= '<tr class="tablegreenrow1 dataRow">
							<td width="100%" align="left" nowrap="nowrap" class="attendancepresent tabletext" colspan="4">&nbsp;</td>
						</tr>';
	    }
	    $display .= '</table>';
	    
	    # Component Subject Input
	    $td_count = $t_count? $t_count : 0;
	    if(count($CmpSubjectArr)>0){
			for($cmpCount=0; $cmpCount<count($CmpSubjectArr); $cmpCount++){
			
				// Component SubjectID
				$cmpSubjectID = $CmpSubjectArr[$cmpCount]["SubjectID"];
				
				// Component Subject
				$cmpSubjectName = $lreportcard->GET_SUBJECT_NAME($cmpSubjectID);
				
				# Get Component Subject Topic
                // [2020-0915-1830-00164]
				//$cmpSubjectTopics = $lreportcard_topic->getSubjectTopic($ClassLevelID, $cmpSubjectID);
                $cmpSubjectTopics = $lreportcard_topic->getSubjectTopic($ClassLevelID, $cmpSubjectID, '', $targetTermSeq);
				if(count($cmpSubjectTopics) == 0){
					$excludedCmpArr[] = $cmpSubjectID;
					continue;
				}
			
				// Initialization of Grading Scheme
				$cmpSchemeID = '';
				$cmpScaleInput = "M";		// M: Mark, G: Grade
				$cmpScaleDisplay = "M";	// M: Mark, G: Grade
				$cmpSchemeType = "H";		// H: Honor Based, PF: PassFail
				$cmpSchemeInfo = array();
				$cmpSchemeMainInfo = array();
				$cmpSchemeMarkInfo = array();
				$cmpSubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $cmpSubjectID, 0, 0, $ReportID);
				
				# Check if no any scheme is assigned to this Form(ClassLevelID) of this subject, redirect
				if(count($cmpSubjectFormGradingArr) > 0){
					if($cmpSubjectFormGradingArr['SchemeID'] == "" || $cmpSubjectFormGradingArr['SchemeID'] == null){
						$excludedCmpArr[] = $cmpSubjectID;
						continue;
					}
				}
				
				$allSubjectTopic = array_merge($allSubjectTopic, $cmpSubjectTopics);
				
				# Get Grading Scheme Info of that subject
				if(count($cmpSubjectFormGradingArr) > 0){
					$cmpSchemeID = $cmpSubjectFormGradingArr['SchemeID'];
					$cmpScaleInput = $cmpSubjectFormGradingArr['ScaleInput'];
					$cmpScaleDisplay = $cmpSubjectFormGradingArr['ScaleDisplay'];
					
					$cmpSchemeInfo = $lreportcard->GET_GRADING_SCHEME_INFO($cmpSchemeID);
					$cmpSchemeMainInfo = $cmpSchemeInfo[0];
					$cmpSchemeMarkInfo = $cmpSchemeInfo[1];
					$cmpSchemeType = $cmpSchemeMainInfo['SchemeType'];
					
					if($cmpSchemeType == "H" && $cmpScaleInput == "G"){	// Prepare Selection Box using Grade as Input
						$cmpInputTmpOption = array();
						$cmpInputGradeOption = array();
						$cmpSchemeGrade = $lreportcard->GET_GRADE_FROM_GRADING_SCHEME_RANGE($cmpSchemeID);
						$cmpInputGradeOption[0] = array('', '--');
						if(!empty($cmpSchemeGrade)){
							foreach($cmpSchemeGrade as $cmpGradeRangeID => $cmpgrade)
								$cmpInputGradeOption[] = array($cmpGradeRangeID, $cmpgrade);
						}
						
						$possibleSpeicalCaseSet1 = $lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
						for($k=0 ; $k<count($possibleSpeicalCaseSet1) ; $k++){
							//$InputGradeOption[] = array($possibleSpeicalCaseSet1[$k], $lreportcard->GET_MARKSHEET_SPECIAL_CASE_SET1_STRING($possibleSpeicalCaseSet1[$k]));
							$cmpInputGradeOption[] = array($possibleSpeicalCaseSet1[$k], $possibleSpeicalCaseSet1[$k]);
						}
					}
					else if($cmpSchemeType == "PF"){			// Prepare Selection Box using Pass/Fail as Input
						$cmpSchemePassFail = array();
						$cmpSchemePassFail['P'] = $cmpSchemeMainInfo['Pass'];
						$cmpSchemePassFail['F'] = $cmpSchemeMainInfo['Fail'];
					
						$cmpInputPFOption = array();
						$cmpInputPFOption[0] = array('', '--');
						$cmpInputPFOption[1] = array('P', $cmpSchemeMainInfo['Pass']);
						$cmpInputPFOption[2] = array('F', $cmpSchemeMainInfo['Fail']);
				
						$possibleSpeicalCaseSet2 = $lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2();
						for($k=0 ; $k<count($possibleSpeicalCaseSet2) ; $k++){
							$cmpInputPFOption[] = array($possibleSpeicalCaseSet2[$k], $possibleSpeicalCaseSet2[$k]);
						}
					}
				}
			    
			   	list($ReportColumnID, $ColumnTitle, $DefaultWeight, $SemesterNum) = $columnIDDataMap[$SelectReportColumnID];
				$CmpSubjectArr[$cmpCount]["ScaleInput"] = $cmpScaleInput;
				$CmpSubjectArr[$cmpCount]["SchemeType"] = $cmpSchemeType;
				$CmpSubjectArr[$cmpCount]["FullMark"] = $cmpSchemeMainInfo['FullMark'];
			  	$CmpSubjectArr[$cmpCount]["Progress"] = $lreportcard->GET_MARKSHEET_SUBMISSION_PROGRESS($cmpSubjectID, $ReportID, $ClassID);
			  	
			  	$cmpMarksheetScoreInfoArr[$SelectReportColumnID] = $lreportcard_topic->GET_MARKSHEET_SUBJECT_TOPIC_SCORE($targetStudentID, $cmpSubjectID, $cmpSubjectTopics, $SelectReportColumnID, 0, '', $SkipSubjectGroupCheckingForGetMarkSheet);
				
				$display2 .= '
					<br>
					<br>
					<table width="100%" border="0" cellspacing="0" cellpadding="2">
						<!--
						<tr><td>
							<table class="form_table_v30">
									<tr>
										<td class="field_title">'.$eReportCard['Subject'].'</td>
										<td>'.$cmpSubjectName.'</td>
									</tr>
									<tr>
										<td class="field_title">'.$eReportCard['SchemesFullMark'].'</td>
										<td>'.((count($cmpSchemeMainInfo) > 0 && $cmpSchemeType != "PF") ? $cmpSchemeMainInfo['FullMark']: $Lang['General']['EmptySymbol']).'</td>
									</tr>
								</table>
								
								<br style="clear:both;" />
			      	
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td align="right" class="tabletextremark">'.(($cmpSchemeType == "PF") ? $eReportCard['MarkRemindSet2'] : ($eRCTemplateSetting['Marksheet']['EstimatedMark'])? $eReportCard['MarkRemindSet1_WithEstimate'] : $eReportCard['MarkRemindSet1']).'</td>
									</tr>
							</table>
						</td></tr>
						-->
						<tr><td>
							<table width="100%" cellpadding="4" cellspacing="0">
								<tr><td width="70%" align="left" class="tabletop tabletopnolink">'.$cmpSubjectName.//$eReportCard['Settings_SubjectTopics'].'
						  		'<td align="left" nowrap="nowrap" class="tabletop tabletopnolink">&nbsp;</td>
								<td align="center" nowrap="nowrap" class="tabletop tabletopnolink" >'.$selectStudentName.'</td>';
					
				# Add Empty Columns to make the mark input column more near to the student name
				$numOfEmptyColumn = 1;
				if ($numOfEmptyColumn > 0)
				{
		  			for($i=0 ; $i<$numOfEmptyColumn ; $i++)
		  			{
			  			//$display2 .= '<td align="left" class="tablegreentop tabletopnolink" width="12%">&nbsp;</td>';
			  			$display2 .= '<td align="left" class="tabletop tabletopnolink" width="12%">&nbsp;</td>';
		  			}
				}
				else
				{
		  			$numOfEmptyColumn = 0;
				}
		
			    $display2 .= '</tr>';
			
				if(count($cmpSubjectTopics) > 0){
					$targetLang = Get_Lang_Selection("NameCh", "NameEn");
					for($comp_count=0; $comp_count<count($cmpSubjectTopics); $comp_count++){
						$currentCmpTopic = $cmpSubjectTopics[$comp_count];
						
						//$tr_css = ($comp_count % 2 == 1) ? "tablegreenrow2" : "tablegreenrow1";
						//$td_css = ($comp_count % 2 == 1) ? "attendanceouting" : "attendancepresent";
						$tr_css = ($comp_count % 2 == 1) ? "tablerow2" : "tablerow1";
						$td_css = ($comp_count % 2 == 1) ? "row_off" : "row_on";
						
						$display2 .= '<tr class="'.$tr_css.' dataRow">
										<td width="70%" align="left" class="'.$td_css.' tabletext">'.$currentCmpTopic[$targetLang].'
										<input type="hidden" name="SubjectTopics[]" id="SubjectTopic_'.$currentCmpTopic['SubjectTopicID'].'" value="'.$currentCmpTopic['SubjectTopicID'].'"/>
										</td>
										<td align="left" nowrap="nowrap" class="'.$td_css.' tabletext">&nbsp;</td>';
						
				        # Check for Combination of Special Case of Input Mark / Term Mark
				        $SpecialCaseArr = $lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
				        foreach($SpecialCaseArr as $key){
				        	$SpecialCaseCountArr[$key] = array("Count"=>0, "Value"=>'', "isCount"=>'');
				    	}
				
					    //list($ReportColumnID, $ColumnTitle, $DefaultWeight, $SemesterNum) = $column_data[$i];
					    list($ReportColumnID, $ColumnTitle, $DefaultWeight, $SemesterNum) = $columnIDDataMap[$SelectReportColumnID];
						    
					    // Mark Type : M-Mark, G-Grade, PF-PassFail, SC-SpecialCase
						$cmpMarkType = (count($cmpMarksheetScoreInfoArr) > 0) ? $cmpMarksheetScoreInfoArr[$ReportColumnID][$currentCmpTopic['SubjectTopicID']]['MarkType'] : '';
						
					   // View of Raw Mark
				        # mark_id is used for paseting values to clipbroad from Excel
					    # mark_name is used for passing values to update
					    # [$i] of mark[$j][$i] == [$i] of ColumnID_$i for matching
					    $mark_name = "mark_".$currentCmpTopic['SubjectTopicID'];
					    $mark_id = "mark[$td_count][0]";
					    # For Subject without component subject And ReportColumn does not have its Report Template to use only
					    if(count($cmpMarksheetScoreInfoArr) > 0){
						    if($cmpMarkType == "G" || $cmpMarkType == "PF" || $cmpMarkType == "SC"){
						    	$InputVal = $cmpMarksheetScoreInfoArr[$ReportColumnID][$currentCmpTopic['SubjectTopicID']]['MarkNonNum'];
					    	} else if($cmpMarkType == "M"){
								# For ScaleInput is Mark & SchemeType is Honor-Based
								# if MarkRaw is -1, the MarkRaw has not been assigned yet.
								# if MarkRaw >= 0, the MarkRaw is valid
						    	$InputVal = ($cmpMarksheetScoreInfoArr[$ReportColumnID][$currentCmpTopic['SubjectTopicID']]['MarkRaw'] != -1) ? $cmpMarksheetScoreInfoArr[$ReportColumnID][$currentCmpTopic['SubjectTopicID']]['MarkRaw'] : '';
					    	} else {
						    	$InputVal = '';
					    	}
					    }
					    else {
						    $InputVal = '';
					    }
					    
					    # Set the InputMark/Grade to be an unchangable value if any column has defined template for whole year use & 
					    # Set Input Mark (Text) / Grade Method (Selection Box) / PassFail (Selection Box)
					    if($WeightAry[$ReportColumnID][$SubjectID] != 0 && $WeightAry[$ReportColumnID][$SubjectID] != null && $WeightAry[$ReportColumnID][$SubjectID] != ""){
						    # ReportType is Term => Can modify Mark/Grade
		//				    if(!empty($CmpSubjectArr) && $isCalculateByCmpSub && $ScaleInput == "M"){
		//					    $tmpVal = $ParentSubMarksheetScoreInfoArr[$ReportColumnID][$StudentID];
		//					    
		//						if($tmpVal == "-" || $tmpVal == "*" || $tmpVal == "/" || $tmpVal == "N.A."){
		//						    $InputMethod = $tmpVal;
		//							$InputMethod .= '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value="'.$tmpVal.'"/>&nbsp;';
		//						} else if($tmpVal != ""){	//!strcmp($tmpVal, 0) || 
		//							//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpVal, $storDispSettings["SubjectScore"]) : round($tmpVal, 0);
		//				    		//$InputMethod = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $storDispSettings["SubjectScore"]);
		//				    		$roundVal = $lreportcard->ROUND_MARK($tmpVal, "SubjectScore");
		//				    		$InputMethod = $lreportcard->ROUND_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $ReportType, "SubjectScore");
		//				    		$InputMethod .= '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value="'.$tmpVal.'"/>&nbsp;';
		//				    		$OverallSubjectMark += $tmpVal;
		//			    		} else {
		//			    			$InputMethod = $tmpVal;
		//				    		$InputMethod .= '<input type="hidden" id="'.$mark_id.'" name="'.$mark_name.'" value="'.$tmpVal.'"/>&nbsp;';
		//				    		$isShowOverallMark = 0;
		//			    		}
		//				    } else {
							    $otherPar = array(	'class'=>'textboxnum', 
							    					'onpaste'=>'isPasteContent(event,\''.$td_count.'\', \'0\')',
							    					'onkeyup'=>'isPasteContent(event, \''.$td_count.'\', \'0\')',
							    					'onkeydown'=>'isPasteContent(event, \''.$td_count.'\', \'0\')',
							    					'onchange'=>'jCHANGE_STATUS()'
							    				);
							    
							    $classname = '';
						        if($lreportcard->Is_Marksheet_Blocked($columnIDDataMap[$ReportColumnID]["BlockMarksheet"]))
						        {
						        	$disabled = " disabled ";
						        	$classname = " enableonsubmit ";
						        	$otherPar['disabled']='disabled';
						        	$otherPar['class']='textboxnum enableonsubmit';
						        }
						        else
						        {
						        	$disabled = '';
						        }
							    
							    $classname .= " Column_$ReportColumnID ";
							    $mark_tag = 'id="'.$mark_id.'" name="'.$mark_name.'" '.$disabled .' onchange="jCHANGE_STATUS()" onpaste="isPasteContent(event,'.$td_count.', 0)" onkeyup="isPasteContent(event, '.$td_count.', 0)" onkeydown="isPasteContent(event, '.$td_count.', 0)" onfocusout="jCHANGE_STATUS()" class="tabletexttoptext '.$classname.'"';
							    //$numOfInputBoxDisplayed++;
								if($cmpSchemeType == "H" && $cmpScaleInput == "G") {
						    		$InputMethod = $linterface->GET_SELECTION_BOX($cmpInputGradeOption, $mark_tag, '', $InputVal);
					    		} else if($SchemeType == "PF"){
						    		$InputMethod = $linterface->GET_SELECTION_BOX($cmpInputPFOption, $mark_tag, '', $InputVal);
					    		} else {
					    			$thisInputValue = ($cmpMarksheetScoreInfoArr[$ReportColumnID][$StudentID]['IsEstimated'])? $lreportcard->Get_Estimated_Score_Display($InputVal) : $InputVal;
					    			$InputMethod = $lreportcard->GET_INPUT_TEXT($mark_name, $mark_id, $thisInputValue, '', $otherPar);
					    		}
		//			    	}
					    }
					    else {
						    $InputMethod = "--";
					    }
					
						//$_tdCss = ($comp_count % 2 == 1) ? "attendanceouting" : "attendancepresent";//2014-0912-1716-13164 pt2
						$_tdCss = ($comp_count % 2 == 1) ? "row_off" : "row_on";
						
				        if($eRCTemplateSetting['Marksheet']['DiffCssStyleForAssessments']){ 
							$_tdCss = ($comp_count % 2 == 0) ? $_tdCss."2" : $_tdCss;
						}
				  		$InputMethod = ($InputMethod == '')? '&nbsp;' : $InputMethod;
				  		$display2 .= '<td align="center" class="'.$_tdCss.' tabletext">'.$InputMethod.'</td>';
						
						# Add Empty Columns to make the mark input column more near to the student name
						for($i=0 ; $i<$numOfEmptyColumn ; $i++) {
							$display2 .= '<td align="center" class="'.$td_css.' tabletext">&nbsp;</td>';
						}
				        $display2 .= '</tr>';
				        
				       	$td_count++;
					}
			    }
		    $display2 .= '</table>
					</td></tr>
				</table>';
		    }
	    }
	    
	    # Get Complete Status of Common Subject or Parent Subject
		$ProgressArr = array();
		// added SubjectGroupID, prevent always display confirm box even current subject group is incompleted
	    //$ProgressArr = $lreportcard->GET_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, $ClassID);
	    $ProgressArr = $lreportcard->GET_MARKSHEET_SUBMISSION_PROGRESS($SubjectID, $ReportID, $ClassID, $SubjectGroupID);
	    $isSubjectComplete = (count($ProgressArr) > 0) ? $ProgressArr['IsCompleted'] : 0;
	    
	    # Button
	    $display_button = '';
	    # Condition: TagsType Must be "0" - Input Raw Mark & case i
	  	# case 1: Whole Year Report & $hasDirectInputMark = 1 - allow direct input 
	  	# case 2: Term Report & subject without component subjects
	  	# case 3: Subject having Component Subjects & is Editable
//	  	if($TagsType == 0 && !$ViewOnly && (
		if(!$ViewOnly && (
	  		$ScaleInput == "G" || 
			//($ReportType == "F" && $hasDirectInputMark) || 
			($ReportType != "F" && empty($CmpSubjectArr)) || 
			(!empty($CmpSubjectArr) && $isEdit)) ){ 
				$display_button .= '<input name="Save" type="button" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$button_save.'" onclick="jSUBMIT_FORM()">&nbsp;';
				
//				if ($eRCTemplateSetting['SubjectWebSAMSCodeArr']['eEnrolSubject'] != '' && $ReportType != "F") {
//					$subjectMapping = $lreportcard->GET_SUBJECTS_CODEID_MAP();
//					$sujbectWebSAMSCode = $subjectMapping[$SubjectID];
//					
//					if ($sujbectWebSAMSCode == $eRCTemplateSetting['SubjectWebSAMSCodeArr']['eEnrolSubject']) {
//						$display_button .= '<input name="SynFromEnrol" type="button" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['SynchronizeFromEnrol'].'" onclick="clickedSynFromEnrolment();">&nbsp;';
//					}
//				}
				
				$display_button .= '<input name="ClearNA" type="button" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$eReportCard['ClearAllNA'].'"  onclick="jCLEAR_ALL_NA()">&nbsp;';
	          	$display_button .= '<input name="Reset" type="reset" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$button_reset.'">&nbsp;';
        }
        $display_button .= '<input name="Cancel" type="button" class="formbutton" onMouseOver="this.className=\'formbuttonon\'" onMouseOut="this.className=\'formbutton\'" value="'.$button_cancel.'" onClick="jBACK()">';
		
		if ($msg == "num_records_updated") {
			$SysMsg = $linterface->GET_SYS_MSG("", "<font color='green'>$SucessCount $i_con_msg_num_records_updated</font>");
		} else {
			$SysMsg = $linterface->GET_SYS_MSG($msg);
		}
		
//		if ($eRCTemplateSetting['Marksheet']['PrintPage']) {
//			$toolbarPrint = '<a href="javascript:void(0);" class="contenttool" onclick="js_Go_Print();"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_print.gif" width="18" height="18" border="0" align="absmiddle"> '.$Lang['Btn']['Print'].'</a>';
//		}
		
		############################################################################################################
        
		# Tag
		$CurrentPage = "Management_MarkSheetRevision";
		$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
		$TAGS_OBJ[] = array($eReportCard['Management_MarksheetRevision'], "", 0);
		
		# Navigation
		$PAGE_NAVIGATION[] = array($eReportCard['Management_MarksheetRevision'], "javascript:jBACK(1)");
		if ($ParentSubjectID != '')
			$PAGE_NAVIGATION[] = array($eReportCard['CmpSubjectMarksheet'], "javascript:jBACK()"); 
		$PAGE_NAVIGATION[] = array($eReportCard['Marksheet'], '');
		$PageNavigation = $linterface->GET_NAVIGATION($PAGE_NAVIGATION);
		
		$linterface->LAYOUT_START();
		echo $linterface->Include_Excel_JS_CSS();
		echo $linterface->Include_CopyPaste_JS_CSS();
		echo $lreportcard_ui->Include_eReportCard_Common_JS();
?>

<script language="javascript">
// set table size
var myno = "<?=count($SubjectTopics)?>";
var yno = "<?=count($allSubjectTopic)?>";

// mapping for subject code and subjectid
var SubjectCodeIDMapping = new Array();
<?php for($stcount=0; $stcount<count($allSubjectTopic); $stcount++){
	$subjectTopicData = $allSubjectTopic[$stcount];
	echo "SubjectCodeIDMapping[".$subjectTopicData["SubjectTopicID"]."] = ".$subjectTopicData["SubjectID"].";\n";
}
?>
var jsDefaultPasteMethod = "text";			// for copypaste.js

$(document).ready( function() {
	jQuery.excel('dataRow');
});

function jCLEAR_ALL_NA()
{
	$("input[value='N.A.'], input[value='N.A.#']").val('');
	$("option[value='N.A.']:selected").parent().val('');
}

/* JS Form */
<?php 
	$js_special_case = '';
	if(count($possibleSpeicalCaseSet1) > 0){
		$js_special_case .= 'var specialCaseArr = new Array(';
		for($i=0 ; $i<count($possibleSpeicalCaseSet1) ; $i++)
			$js_special_case .= ($i != 0 ) ? ', "'.$possibleSpeicalCaseSet1[$i].'"' : '"'.$possibleSpeicalCaseSet1[$i].'"' ;
		$js_special_case .= ');';
	}
	echo $js_special_case."\n";
?>

function jCHANGE_STATUS(){
	document.getElementById("isOutdated").value = 1;
}

function jCHECK_MARKSHEET_STATUS(newtagstype){
	var obj = document.getElementById('FormMain');
	var status = document.getElementById("isOutdated").value;
		
	if(status == 1){
		if(confirm("<?=$eReportCard['SubmitMarksheetConfirm']?>")){
			obj.submit();
		}
	} else {
		var classlevelid = document.getElementById("ClassLevelID").value;
		var reportid = document.getElementById("ReportID").value;
		var reportcolumnid = document.getElementById("SelectReportColumnID").value;
		var subjectid = document.getElementById("SubjectID").value;
		var classid = document.getElementById("ClassID").value;
		var subjectgroupid = document.getElementById("SubjectGroupID").value;
		var studentid = document.getElementById("targetStudentID").value;
		var params = "ClassLevelID="+classlevelid+"&ReportID="+reportid+"&SelectReportColumnID="+reportcolumnid+"&SubjectID="+subjectid+"&ClassID="+classid+"&SubjectGroupID="+subjectgroupid+"&targetStudentID="+studentid;
		
		<?php if($isCmpSubject) { ?>
//		var parent_subject_id = <?=((isset($ParentSubjectID) && $ParentSubjectID != "") ? $ParentSubjectID : "''")?>;
//		params += "&ParentSubjectID="+parent_subject_id;
		<?php } ?>
		
		location.href = "marksheet_subject_topic_edit.php?"+params;
	}
}

function jCHECK_SPECIAL_CASE(scVal){
	if(specialCaseArr.length > 0){
		for(var i=0 ; i<specialCaseArr.length ; i++){
			if(scVal == specialCaseArr[i])
				return true;
		}
		return false;
	}
	else {
		return false;
	}
}

/*
* Check whether the input mark is valid or not
*/
function jCHECK_INPUT_MARK(ycor){
	var jFullMark = document.getElementById("FullMark").value;
	var obj = document.getElementById("mark["+ycor+"][0]");
	
	if(ycor >= myno){
		var objName = obj.getAttribute("name");
		if(objName!=""){
			objName = objName.replace("mark_","");
			var compSubjectID = SubjectCodeIDMapping[objName];
			jFullMark = document.getElementById("FullMark_"+compSubjectID).value;
		}
	}
	
	var jsScore = Trim(js_Remove_Estimate_Mark_Symbol(obj.value));
	
	if(obj && jsScore != ""){
		if(!jCHECK_SPECIAL_CASE(jsScore)){
			if(jsScore < 0){
				alert("<?=$eReportCard['jsInputMarkCannotBeNegative']?>");
				obj.focus();
				return false;
			}
			
			if(!IsNumeric(jsScore)){
				alert("<?=$eReportCard['jsInputMarkInvalid']?>");
				obj.focus();
				return false;
			}
			
			if(parseFloat(jsScore) > parseFloat(jFullMark)){
				alert("<?=$eReportCard['jsInputMarkCannotBeLargerThanFullMark']?>");
				obj.focus();
				return false;
			}
		}
	}
	return true;
}

//function jCHECK_OVERALL_SUBJECT_WEIGHTED_MARK(xcor){
//	var TotalWeightedMark = 0;
//	var TotalColWeight = 0;
//	var TotalMark = 0;
//	var jFullMark = document.getElementById("FullMark").value;
//	
//	for(var j=0; j<xno ; j++){
//		var ycor = j;
//		if(document.getElementById("mark["+xcor+"]["+ycor+"]")){
//			var obj = document.getElementById("mark["+xcor+"]["+ycor+"]");
//			var columnID = document.getElementById("ColumnID_"+ycor).value;
//			var colWeight = document.getElementById("ColumnWeight_"+columnID).value;
//			TotalColWeight += parseFloat(colWeight);
//			
//			if(obj.value != ""){
//				if(jCHECK_SPECIAL_CASE(obj.value)) 
//					continue;
//				if(parseFloat(colWeight) > 0){
//					var tmp = parseFloat(obj.value) * parseFloat(colWeight);
//					TotalMark += parseFloat(tmp);
//				}
//			}
//		}
//	}
//	if(parseFloat(TotalColWeight) > 0){
//		TotalWeightedMark = parseFloat(TotalMark).toFixed(5) / parseFloat(TotalColWeight).toFixed(5);
//		TotalWeightedMark = TotalWeightedMark.toFixed(5);
//		
//		if(parseFloat(TotalWeightedMark) > parseInt(jFullMark)){
//			alert("<?=$eReportCard['jsCheckOverallSubjectWeightedMark']?>");
//			return false;
//		}
//	}
//	return true;
//}

<?php 
	if ($sys_custom['eRC']['Marksheet']['CopyLastColumnGradeToOverallColumn'] || getenv("SERVER_NAME") == "project6.broadlearning.com" || getenv("SERVER_NAME") == "sis-trial.broadlearning.com") { 
		if($SchemeType == "PF" || ($SchemeType == "H" && $ScaleInput == "G") ){
?>
// customized for SIS
//function jCOPY_TO_OVERALL_MARK(){
//	var lastReportColumnID = document.getElementById("LastReportColumnID").value;
//	var studentID = document.getElementsByName("StudentID[]");
//	var studentSize = studentID.length;
//
//	if(studentSize > 0){
//		for(var i=0 ; i<studentSize ; i++){
//			//var lastColMarkSelectedIndex = document.getElementById("mark_"+lastReportColumnID+"_"+studentID[i].value).selectedIndex;
//			//var overall = document.getElementById("overall_mark_"+studentID[i].value);
//			//overall.selectedIndex = lastColMarkSelectedIndex;
//			
//			var jsThisStudentID = studentID[i].value;
//			var targetValue = $('select[name=mark_' + lastReportColumnID + '_' + jsThisStudentID + ']').val();
//			
//			if (targetValue == null || targetValue == '')
//				targetValue = $('#tmp_for_copy_mark_' + lastReportColumnID + '_' + jsThisStudentID).val();
//				
//			//if (i==studentSize-1)
//			//	alert('targetValue = ' + targetValue);
//			
//			$('select[name=overall_mark_' + jsThisStudentID + ']').val(targetValue);
//			
//		}
//	}
}


// End of customized for SIS
<?php 
		}
	} 
?>


// yno: Topic Size
function jCHECK_FORM(){
	<?php if($SchemeType == "H" && $ScaleInput != "G"){?>
	// check for mark when mark is used for input value
	for(var i=0; i<yno ; i++){
		if(!jCHECK_INPUT_MARK(i))
			return false;
	}
	
//	// check for overall subject weighted mark which must be less than full mark
//	for(var i=0; i<yno ; i++){
//		if(!jCHECK_OVERALL_SUBJECT_WEIGHTED_MARK(i))
//			return false;
//	}
	<?php } ?>
	
	var isComplete = document.getElementById("isSubjectComplete").value;
	if(isComplete == 1){
		if(!confirm("<?=$eReportCard['jsConfirmMarksheetToIncomplete']?>"))
			return false;
	}
	
	return true;
}

function jSUBMIT_FORM(){
	var obj = document.getElementById('FormMain');
	
	if(!jCHECK_FORM(obj))
		return;
	
	$(".enableonsubmit").removeAttr("disabled");
	
	// short timeout
	setTimeout(function() {
		document.body.scrollTop = document.documentElement.scrollTop = 0;
	}, 15);
	document.getElementById('sendingDiv').style.display = '';
	obj.submit();
}

function jBACK(jsGoFirstPage){
	var form_id = <?=(($ClassLevelID != "") ? $ClassLevelID : "''")?>;
	var report_id = <?=(($ReportID != "") ? $ReportID : "''")?>;
	var subject_id = <?=(($SubjectID != "") ? $SubjectID : "''")?>;
	var isProgress = document.getElementById("isProgress").value;
	var isProgressSG = document.getElementById("isProgressSG").value;
	var status = document.getElementById("isOutdated").value;
	
	if(status == 1){
		if(<?=$ViewOnly?1:0?> || !confirm("<?=$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['SaveBeforeLeaveWarning']?>")){
			if(isProgress == 0 && isProgressSG == 0){
			<?php if($isCmpSubject) { ?>
				<? if ($SubjectGroupID != '') { ?>
					var subject_group_id = <?=(($SubjectGroupID != "") ? $SubjectGroupID : "''")?>;
					var parent_subject_id = <?=((isset($ParentSubjectID) && $ParentSubjectID != "") ? $ParentSubjectID : "''")?>;
					
					var jsPageURL;
					if (jsGoFirstPage==1)
						jsPageURL = 'marksheet_revision_subject_group.php';
					else
						jsPageURL = 'marksheet_revision2_subject_group.php';
						
					location.href = jsPageURL + "?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+parent_subject_id+"&SubjectGroupID="+subject_group_id;
				<? } else { ?>
					var class_id = <?=(($ClassID != "") ? $ClassID : "''")?>;
					var parent_subject_id = <?=((isset($ParentSubjectID) && $ParentSubjectID != "") ? $ParentSubjectID : "''")?>;
					location.href = "marksheet_revision2.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+parent_subject_id+"&ClassID="+class_id;
				<? } ?>
				
			<?php } else { ?>
				location.href = "marksheet_revision.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+subject_id;
			<?php } ?>
			}
			else {
				if (isProgressSG == 1) {
					location.href = "../progress/submission_sg.php";
				}
				else {
					location.href = "../progress/submission.php?ClassLevelID="+form_id+"&ReportID="+report_id;
				}
			}
		} else {
			document.getElementById("IsSaveBefCancel").value = 1;
			jSUBMIT_FORM();
		}
	} else {	
		if((isProgress == 0 || isProgress == '') && (isProgressSG == 0 || isProgressSG == '')){
		<?php if($isCmpSubject) { ?>
			<? if ($SubjectGroupID != '') { ?>
				var subject_group_id = <?=(($SubjectGroupID != "") ? $SubjectGroupID : "''")?>;
				var parent_subject_id = <?=((isset($ParentSubjectID) && $ParentSubjectID != "") ? $ParentSubjectID : "''")?>;
				
				var jsPageURL;
				if (jsGoFirstPage==1)
					jsPageURL = 'marksheet_revision_subject_group.php';
				else
					jsPageURL = 'marksheet_revision2_subject_group.php';
						
				location.href = jsPageURL + "?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+parent_subject_id+"&SubjectGroupID="+subject_group_id;	
			<? } else { ?>
				var class_id = <?=(($ClassID != "") ? $ClassID : "''")?>;
				var parent_subject_id = <?=((isset($ParentSubjectID) && $ParentSubjectID != "") ? $ParentSubjectID : "''")?>;
				location.href = "marksheet_revision2.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+parent_subject_id+"&ClassID="+class_id;	
			<? } ?>
			
		<?php } else { ?>
			location.href = "marksheet_revision.php?ClassLevelID="+form_id+"&ReportID="+report_id+"&SubjectID="+subject_id;
		<?php } ?>
		}
		else {
			if (isProgressSG == 1) {
				location.href = "../progress/submission_sg.php";
			}
			else {
				location.href = "../progress/submission.php?ClassLevelID="+form_id+"&ReportID="+report_id;
			}
		}
	}
}

//var isUpdatedFromSubMS = 0;
//function updateColumnScore(OverallScoreArr, ColumnSize, jsReportColumn)
//{
//	for (i=0; i<ColumnSize; i++)
//	{
//		var thisStudentID = OverallScoreArr[i][0];
//		var thisMark = OverallScoreArr[i][1];
//		thisMark = (thisMark=="---")? "" : thisMark;
//		
//		// format: mark_ReportColumnID_StudentID
//		// var thisName = "mark_" + ReportColumnID + "_" + thisStudentID;
//		var thisID = "mark[" + i + "][" + jsReportColumn + "]";
//		var thisTB = document.getElementById(thisID);
//		
//		thisTB.value = thisMark;
//	}
//	
//	isUpdatedFromSubMS = 1;
//}
//
//function js_ReloadMS(targetSubjectGroupID)
//{
//	var jsClassLevelID = <?=$ClassLevelID?>;
//	var jsReportID = <?=$ReportID?>;
//	var jsSubjectID = <?=$SubjectID?>;
//	
//	self.location = "marksheet_edit.php?ClassLevelID=" + jsClassLevelID + "&ReportID=" + jsReportID + "&SubjectID=" + jsSubjectID + "&SubjectGroupID=" + targetSubjectGroupID;
//}
//
//function js_Go_Print() {
//	var jsOriginalAction = $('form#FormMain').attr('action');
//	$('form#FormMain').attr('action', '../../print_page.php').attr('target', '_blank').submit();
//	
//	// restore form attribute
//	$('form#FormMain').attr('action', jsOriginalAction).attr('target', '_self');
//}
//
//function clickedSynFromEnrolment() {
//	if (confirm('<?=$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['SynchronizeFromEnrolWarning']?>')) {
//		Block_Document();
//	
//		var studentIdAry = new Array();
//		$('input[name="StudentID\\[\\]"]').each( function() {
//			studentIdAry[studentIdAry.length] = $(this).val();
//		});
//		var studentIdText = studentIdAry.join(',');
//		
//		$.post(
//			"ajax_task.php", 
//			{ 
//				task: 'getEnrollmentPerformanceAverage',
//				ReportID: '<?=$ReportID?>',
//				StudentIDList: studentIdText
//			},
//			function(ReturnData) {
//				var performanceAry = ReturnData.split('||');
//				var numOfStudent = performanceAry.length;
//				var i;
//				for (i=0; i<numOfStudent; i++) {
//					var studentDataAry = performanceAry[i].split('==');
//					var studentId = studentDataAry[0];  
//					var performance = studentDataAry[1];
//					
//					if (performance == '--') {
//						performance = '';
//					}
//					
//					$('input[name="overall_mark_'+ studentId +'"]').val(performance);
//				}
//				
//				jCHANGE_STATUS();
//				UnBlock_Document();
//			}
//		);
//	}
//}
</script>

<br/>
<form id="FormMain" name="FormMain" method="post" action="marksheet_subject_topic_edit_update.php">
	<table border="0" cellspacing="0" cellpadding="0" align="right">
		<tr>
			<td><?=$SysMsg?></td>
		</tr>
	</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td class="tab_underline"><?=$display_tagstype?></td>
		</tr>
	</table>
	<div class="content_top_tool">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<?php if(false) { ?>
				<table border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td>
		      		<?php 
		      		  # Condition: TagsType Must be "0" - Input Raw Mark & case i
		      		  # case 1: Whole Year Report & $hasDirectInputMark = 1 - allow direct input 
		      		  # case 2: Term Report & subject without component subjects
		      		  # case 3: Subject having Component Subjects & is Editable
		      		  if(false){
//		      		  if(!$ViewOnly && (
//		      		    $ScaleInput == "G" || 
//		      			($ReportType == "F" && $hasDirectInputMark) || 
//		      			($ReportType != "F" && empty($CmpSubjectArr)) || 
//		      			(!empty($CmpSubjectArr) && $isEdit)) && $eRCTemplateSetting['DisableMarksheetImport']!==true){ 
		          			
		        	?>
	      	  			<a href="marksheet_import.php<?="?$params&TagsType=$TagsType"?>" class="contenttool"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/icon_import.gif" width="18" height="18" border="0" align="absmiddle"> <?=$button_import?></a>
	      	  		<?php } ?>
	      	  		<a href="marksheet_export.php<?="?ClassLevelID=$ClassLevelID&SubjectID=$SubjectID&ReportID=$ReportID&ClassID=$ClassID&TagsType=$TagsType&SubjectGroupID=$SubjectGroupID"?>" class="contenttool"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/icon_export.gif" width="18" height="18" border="0" align="absmiddle"> <?=$button_export?></a>
	      	  		<?=$toolbarPrint?>
						</td>
					</tr>
				</table>
				<?php } ?>
			</td>
		</tr>
	</table>
</div>

<br style="clear:both;" />

<table width="100%" border="0" cellpadding"0" cellspacing="0">
	<tr><td class="navigation"><?=$PageNavigation?></td></tr>
</table>

<div id="sendingDiv" style="width:100%; text-align:center; display:none;"><?=$linterface->Get_Ajax_Loading_Image($noLang=1)?> <span style="color:red; font-weight:bold;"><?=$Lang['General']['Saving...']?>...</span></div>

<!--PrintStart-->
<table width="100%" border="0" cellpadding="4" cellspacing="0">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td>
						<table class="form_table_v30">
							<tr>
								<td class="field_title"><?=$eReportCard['Settings_ReportCardTemplates']?></td>
								<td><?=$PeriodName?></td>
							</tr>
							<tr>
								<td class="field_title"><?=$eReportCard['Subject']?></td>
								<td><?=$SubjectName?></td>
							</tr>
							<tr>
								<td class="field_title"><?=$SubjectOrGroupTitle?></td>
								<td><?=$SubjectOrGroupNameDisplay?></td>
							</tr>
							<tr>
								<td class="field_title"><?=$eReportCard['SchemesFullMark']?></td>
								<td><?=((count($SchemeMainInfo) > 0 && $SchemeType != "PF") ? $SchemeMainInfo['FullMark']: $Lang['General']['EmptySymbol'])?></td>
							</tr>
							<?=$SubjectTeacherTr?>
						</table>
						
						<br style="clear:both;" />
	      	
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="right" class="tabletextremark"><?=(($SchemeType == "PF") ? $eReportCard['MarkRemindSet2'] : ($eRCTemplateSetting['Marksheet']['EstimatedMark'])? $eReportCard['MarkRemindSet1_WithEstimate'] : $eReportCard['MarkRemindSet1'])?></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td><?=$reportColumnOptions?></td></tr>
				<tr>
					<td>
						<!-- Start of Content -->
				      <?=$display?>
				      <!-- End of Content -->
					</td>
				</tr>
			</table>
			<?=$display2?>
		</td>
	</tr>
</table>
<!--PrintEnd-->

<br>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td height="1" class="dotline"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td align="left" valign="bottom"><span class="tabletextremark"><?=$eReportCard['DeletedStudentLegend']?></span></td>
				</tr>
				<tr>
					<td align="center" valign="bottom"><?=$display_button?></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" name="isProgress" id="isProgress" value="<?=$isProgress?>"/>
<input type="hidden" name="isProgressSG" id="isProgressSG" value="<?=$isProgressSG?>"/>

<input type="hidden" name="ReportID" id="ReportID" value="<?=$ReportID?>"/>
<input type="hidden" name="ReportColumnID" id="ReportColumnID" value="<?=$ReportColumnID?>"/>
<input type="hidden" name="ClassLevelID" id="ClassLevelID" value="<?=$ClassLevelID?>"/>
<input type="hidden" name="ClassID" id="ClassID" value="<?=$ClassID?>"/>
<input type="hidden" name="SubjectID" id="SubjectID" value="<?=$SubjectID?>"/>
<input type="hidden" name="SubjectGroupID" id="SubjectGroupID" value="<?=$SubjectGroupID?>"/>
<input type="hidden" name="SchemeID" id="SchemeID" value="<?=$SchemeID?>"/>

<input type="hidden" name="isSubjectComplete" id="isSubjectComplete" value="<?=$isSubjectComplete?>"/>
<input type="hidden" name="isOutdated" id="isOutdated" value="0"/>
<input type="hidden" name="newTagsType" id="newTagsType" value=""/>
<input type="hidden" name="IsSaveBefCancel" id="IsSaveBefCancel" value="0"/>

<input type="hidden" name="nextStudentID" id="nextStudentID" value=""/>

<?php
if(isset($ParentSubjectID) && $ParentSubjectID != "")
	echo '<input type="hidden" name="ParentSubjectID" id="ParentSubjectID" value="'.$ParentSubjectID.'"/>'."\n";
	
if(!empty($CmpSubjectArr) && count($CmpSubjectArr)>0)
	echo '<input type="hidden" name="fromParentSubjectID" id="fromParentSubjectID" value="'.$SubjectID.'"/>'."\n";
?>

<input type="hidden" name="FullMark" id="FullMark" value="<?=((count($SchemeMainInfo) > 0) ? $SchemeMainInfo['FullMark'] : '""')?>"/>
<input type="hidden" name="isAllColumnWeightZero" id="isAllColumnWeightZero" value="<?=$isAllColumnWeightZero?>" />

<?php if(count($CmpSubjectArr) > 0){
	for($i=0 ; $i<count($CmpSubjectArr) ; $i++){
		$currentCmpSubjectID = $CmpSubjectArr[$i]["SubjectID"];
		if(in_array($currentCmpSubjectID, $excludedCmpArr)){
			continue;
		}
		
		echo '<input type="hidden" name="CmpSubjectID[]" id="CmpSubjectID_'.$i.'" value="'.$currentCmpSubjectID.'"/>'."\n";
		echo '<input type="hidden" name="FullMark_'.$currentCmpSubjectID.'" id="FullMark_'.$currentCmpSubjectID.'" value="'.$CmpSubjectArr[$i]["FullMark"].'"/>'."\n";
		//echo '<input type="hidden" name="ScaleInput_'.$currentCmpSubjectID.'" id="ScaleInput_'.$currentCmpSubjectID.'" value="'.$CmpSubjectArr[$i]["ScaleInput"].'"/>'."\n";
		//echo '<input type="hidden" name="SchemeType_'.$currentCmpSubjectID.'" id="SchemeType_'.$currentCmpSubjectID.'" value="'.$CmpSubjectArr[$i]["SchemeType"].'"/>'."\n";
		//echo '<input type="hidden" name="isCmpSubjectComplete_'.$currentCmpSubjectID.'" id="isCmpSubjectComplete_'.$currentCmpSubjectID.'" value="'.$CmpSubjectArr[$i]["Progress"].'"/>'."\n";
	}
}
?>

<textarea style="height: 1px; width: 1px; visibility:hidden;" id="text1" name="text1"></textarea>

</form>

<?
		$linterface->LAYOUT_STOP();
	}
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>