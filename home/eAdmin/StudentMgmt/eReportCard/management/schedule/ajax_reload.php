<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_schedule.php");

$linterface = new interface_html();
$lreportcard = new libreportcardcustom();
$lreportcard_ui = new libreportcard_ui();
$lreportcard_schedule = new libreportcard_schedule();
$lreportcard->hasAccessRight();


$task = $_POST['task'];
$reportId = $_POST['reportId'];


if ($task == 'reloadAvailableTeacherSelection') {
	$libuser = new libuser();
	
	$isInitialize = ($_POST['isInitialize']=='1')? true : false;
	$addTeacherIdList = $_POST['addTeacherIdList'];
	$addTeacherIdAry = explode(',', $addTeacherIdList);
	$removeTeacherIdList = $_POST['removeTeacherIdList'];
	$removeTeacherIdAry = explode(',', $removeTeacherIdList);
	$curAvailableTeacherIdList = $_POST['curAvailableTeacherIdList'];
	$curAvailableTeacherIdAry = explode(',', $curAvailableTeacherIdList);
	$curSelectedTeacherIdList = $_POST['curSelectedTeacherIdList'];
	$curSelectedTeacherIdAry = explode(',', $curSelectedTeacherIdList);
	
	if ($isInitialize) {
		$specificDataAry = $lreportcard_schedule->getSpecificMarksheetSubmissionPeriodData($reportId);
		$teacherIdAry = Get_Array_By_Key($specificDataAry, 'UserID');
		
		$teacherInfoAry = $libuser->returnUsersType2(USERTYPE_STAFF, $teaching=-1, $userIdAry='', $excludeUserIdAry=$teacherIdAry);
	}
	else {
		$curAvailableTeacherIdAry = array_diff($curAvailableTeacherIdAry, $addTeacherIdAry);
		$curSelectedTeacherIdAry = array_diff($curSelectedTeacherIdAry, $removeTeacherIdAry);
		$teacherInfoAry = $libuser->returnUsersType2(USERTYPE_STAFF, $teaching=-1, array_merge((array)$curAvailableTeacherIdAry, (array)$removeTeacherIdAry), array_merge((array)$curSelectedTeacherIdAry, (array)$addTeacherIdAry));
	}
	
	echo getSelectByArray($teacherInfoAry, ' id="specificMarksheetSubmissionAvailableTeacherSel" name="specificMarksheetSubmissionAvailableTeacherIdAry[]" multiple="multiple" size="10" style="width:250px;" ', $selected='', $all=0, $noFirst=1);
}
else if ($task == 'reloadSelectedTeacherSelection') {
	$libuser = new libuser();
	
	$isInitialize = ($_POST['isInitialize']=='1')? true : false;
	$addTeacherIdList = $_POST['addTeacherIdList'];
	$addTeacherIdAry = explode(',', $addTeacherIdList);
	$removeTeacherIdList = $_POST['removeTeacherIdList'];
	$removeTeacherIdAry = explode(',', $removeTeacherIdList);
	$curAvailableTeacherIdList = $_POST['curAvailableTeacherIdList'];
	$curAvailableTeacherIdAry = explode(',', $curAvailableTeacherIdList);
	$curSelectedTeacherIdList = $_POST['curSelectedTeacherIdList'];
	$curSelectedTeacherIdAry = explode(',', $curSelectedTeacherIdList);
	
	
	if ($isInitialize) {
		$specificDataAry = $lreportcard_schedule->getSpecificMarksheetSubmissionPeriodData($reportId);
		$teacherIdAry = Get_Array_By_Key($specificDataAry, 'UserID');
		$teacherInfoAry = $libuser->returnUsersType2(USERTYPE_STAFF, $teaching=-1, $teacherIdAry);
	}
	else {
		$curAvailableTeacherIdAry = array_diff($curAvailableTeacherIdAry, $addTeacherIdAry);
		$curSelectedTeacherIdAry = array_diff($curSelectedTeacherIdAry, $removeTeacherIdAry);
		$teacherInfoAry = $libuser->returnUsersType2(USERTYPE_STAFF, $teaching=-1, array_merge((array)$curSelectedTeacherIdAry, (array)$addTeacherIdAry), array_merge((array)$curAvailableTeacherIdAry, (array)$removeTeacherIdAry));
	}
	
	echo getSelectByArray($teacherInfoAry, ' id="specificMarksheetSubmissionSelectedTeacherSel" name="specificMarksheetSubmissionSelectedTeacherIdAry[]" multiple="multiple" size="10" style="width:250px;" ', $selected='', $all=0, $noFirst=1);
}
else if ($task == 'reloadSpecialSubmissionTbContent') {
	$reportId = IntegerSafe(standardizeFormPostValue($_POST['reportId']));
	$reportInfoAry = $lreportcard->returnReportTemplateBasicInfo($ReportID);
	$reportSemester = $reportInfoAry["Semester"];
	$reportType = ($reportInfoAry["Semester"]=='F')? 'W' : 'T';
	
	$htmlAry['addBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Add'], "button", "javascript:addSpecialSubmissionTeacherSubject(0);", 'addBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
	$htmlAry['closeBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Close'], "button", "js_Hide_ThickBox();", 'closeBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
	
	$x = '';
	$x .= '<div id="thickboxContainerDiv" class="edit_pop_board">'."\r\n";
		$x .= '<form id="thickboxForm" name="thickboxForm">'."\r\n";
			$x .= '<div id="thickboxContentDiv" class="edit_pop_board_write">'."\r\n";
				$x .= '<table class="form_table_v30">'."\r\n";
					// teacher
					$x .= '<tr>'."\r\n";
						$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['General']['Teacher'].'</td>'."\r\n";
						$x .= '<td>'."\r\n";
							$x .= $lreportcard_ui->Get_Report_Teacher_Selection($reportId, 'specialSubmissionTb_teacherIdSel', 'specialSubmissionTb_teacherId', 'changedSpecialSubmissionTeacherSel(this.value);');
						$x .= '</td>'."\r\n";
					$x .= '</tr>'."\r\n";
					// subject
					$x .= '<tr>'."\r\n";
						$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['SysMgr']['SubjectClassMapping']['Subject'].'</td>'."\r\n";
						$x .= '<td>'."\r\n";
							$x .= '<div id="specialSubmissionTb_subjectSelDiv"><!--load by ajax--></div>'."\r\n";
						$x .= '</td>'."\r\n";
					$x .= '</tr>'."\r\n";
					// subject group
					if ($eRCTemplateSetting['Management']['Schedule']['SpecificMarksheetSubmissionPeriod_subjectGroupBased'] && $reportType=='T') {
						$x .= '<tr>'."\r\n";
							$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'].'</td>'."\r\n";
							$x .= '<td>'."\r\n";
								$x .= '<div id="specialSubmissionTb_subjectGroupSelDiv"><!--load by ajax--></div>'."\r\n";
							$x .= '</td>'."\r\n";
						$x .= '</tr>'."\r\n";
					}
				$x .= '</table>'."\r\n";
			$x .= '</div>'."\r\n";
			$x .= '<div id="editBottomDiv" class="edit_bottom_v30">'."\r\n";
				$x .= '<p class="spacer"></p>'."\r\n";
				$x .= $htmlAry['addBtn'];
				$x .= $htmlAry['closeBtn'];
				$x .= '<p class="spacer"></p>'."\r\n";
			$x .= '</div>'."\r\n";
		$x .= '</form>'."\r\n";
	$x .= '</div>'."\r\n";
	
	echo $x;
}
else if ($task == 'reloadSubjectSelection') {
	$reportId = IntegerSafe(standardizeFormPostValue($_POST['reportId']));
	$teacherId = IntegerSafe(standardizeFormPostValue($_POST['teacherId']));
	
	$reportInfoAry = $lreportcard->returnReportTemplateBasicInfo($reportId);
	$classlevelId = $reportInfoAry['ClassLevelID']; 
	
	echo $lreportcard->Get_Subject_Selection($classlevelId, '', 'specialSubmissionTb_subjectId', 'changedSpecialSubmissionSubjectSel();', $isMultiple=0, $includeComponent=0, $InputMarkOnly=0, $includeGrandMarks=0, $otherTagInfo='', $reportId, $ParentSubjectAsOptGroup=0, $ExcludeWithoutSubjectGroup=0, $TeachingOnly=1, $ExcludeSubjectIDArr='', $ParDefault='', $IncludeConduct=false, $showAllSubject=false, $teacherId);
}
else if ($task == 'reloadSubjectGroupSelection') {
	$reportId = IntegerSafe(standardizeFormPostValue($_POST['reportId']));
	$teacherId = IntegerSafe(standardizeFormPostValue($_POST['teacherId']));
	$subjectId = IntegerSafe(standardizeFormPostValue($_POST['subjectId']));
	
	$reportInfoAry = $lreportcard->returnReportTemplateBasicInfo($reportId);
	$reportClassLevelId = $reportInfoAry["ClassLevelID"];
	$reportSemester = $reportInfoAry["Semester"];
	
	$optiontAry = array();
	$subjectGroupAry = $lreportcard->Get_Teacher_Related_Subject_Groups_By_Subjects($reportSemester, $reportClassLevelId, $teacherId, $subjectId);
	$numOfSubjectGroup = count($subjectGroupAry);
	$nameField = Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN');
	for ($i=0; $i<$numOfSubjectGroup; $i++) {
		$_subjectGroupId = $subjectGroupAry[$i]['SubjectGroupID'];
		$_subjectGroupName = $subjectGroupAry[$i][$nameField];
		
		$optiontAry[$_subjectGroupId] = $_subjectGroupName;
	}
	
	$selectAttr = ' id="specialSubmissionTb_subjectGroupIdSel" name="specialSubmissionTb_subjectGroupId" ';
	echo getSelectByAssoArray($optiontAry, $selectAttr, $selected="", $all=0, $noFirst=1);
}

intranet_closedb();
?>