<?php
// Using: 

/*****************************************************************
 * 
 * Date: 2019-05-01 Bill
 * Details: prevent SQL Injection + Cross-site Scripting
 * 
 * Date: 2017-03-22 Bill	[2017-0109-1818-40164]
 * Details: added Report Publishing Period ($eRCTemplateSetting['Management']['Schedule']['PublishReportPeriod'])
 * 
 * Date: 2016-11-18 Bill	[2015-1104-1130-08164]
 * Details: added Class Teacher's Comment Submission Period ($eRCTemplateSetting['Management']['Schedule']['ClassTeacherCommentSubmissionPeriod'])
 * 
 * Date: 2015-05-13 Bill
 * Details: fixed: remove duplicate report info div
 * 
 * Date: 2013-02-05 Rita
 * Details: add access setting checking
 * 			-$accessSettingData[EnableVerificationPeriod]==1
 * 
 *****************************************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_schedule.php");
include_once($PATH_WRT_ROOT.'includes/table/table_commonTable.php');

$linterface = new interface_html();
$lreportcard = new libreportcardcustom();
$lreportcard_ui = new libreportcard_ui();
$lreportcard_schedule = new libreportcard_schedule();
$lreportcard->hasAccessRight();

if(isset($_POST['ReportIDs']) && count($_POST['ReportIDs']) > 0) {
	$ReportIDs = $_POST['ReportIDs'];
	$ReportID = $ReportIDs[0];
	$isEditAll = true;
}
else {
	$ReportIDs[0] = $ReportID;
	$isEditAll = false;
}
$ReportIDs = IntegerSafe($ReportIDs);
$ReportID = IntegerSafe($ReportID);

$Result = $_GET['Result'];

// $ReportID = $_GET['ReportID'];
// $Result = $_GET['Result'];

$CurrentPage = "Management_Schedule";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($eReportCard['Management_Schedule']);
$PAGE_NAVIGATION[] = array($eReportCard['Management_Schedule'], 'javascript: goCancel();');
$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit']);

$linterface->LAYOUT_START();
echo $linterface->Include_Thickbox_JS_CSS();

if($isEditAll){
	$tableObj = new table_commonTable();
	$tableObj->setTableType('view');
	$tableObj->setApplyConvertSpecialChars(false);
	$tableObj->addHeaderTr(new tableTr());
	$tableObj->addHeaderCell(new tableTh('#'));
	$headerAry = array($Lang['SysMgr']['FormClassMapping']['Form'],$eReportCard['ReportTitle'],$eReportCard['ReportType']);
	for ($i=0; $i<count($headerAry); $i++) {
		$tableObj->addHeaderCell(new tableTh($headerAry[$i]));
	}
	$hiddenInput = '';
	
	### Process Report Data
	for($i=0;$i<count($ReportIDs);$i++){
		$_reportId = $ReportIDs[$i];
		$_reportInfoAry = $lreportcard->returnReportTemplateBasicInfo($_reportId);
		$_reportTitle = str_replace(':_:', '<br />', $_reportInfoAry["ReportTitle"]);
		$_reportType = $lreportcard_ui->Get_Report_Card_Type_Display($_reportId, 1);
		$_formName = $lreportcard_ui->returnClassLevel($_reportInfoAry['ClassLevelID']);
		$hiddenInput .= $linterface->GET_HIDDEN_INPUT('', 'ReportIDs[]', $_reportId)."\n";
		
		$tableObj->addBodyTr(new tableTr());
		$tableObj->addBodyCell(new tableTd($i+1));
		$tableObj->addBodyCell(new tableTd($_formName));
		$tableObj->addBodyCell(new tableTd($_reportTitle));
		$tableObj->addBodyCell(new tableTd($_reportType));
	}
}


# Load the highlight setting data
$accessSettingData = $lreportcard->LOAD_SETTING("AccessSettings");

### Process Report Data
$reportInfoAry = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$applySpecificMarksheetSubmission = $reportInfoAry['ApplySpecificMarksheetSubmission'];
$reportTitle = str_replace(':_:', '<br />', $reportInfoAry["ReportTitle"]);
$reportSemester = $reportInfoAry["Semester"];
$reportType = ($reportInfoAry["Semester"]=='F')? 'W' : 'T';


$reportDetail = $lreportcard->GET_REPORT_DATE_PERIOD($Condition="", $ReportID);
$subStart = ($reportDetail[0]["SubStart"]=="") ? "" : date("Y-m-d",$reportDetail[0]["SubStart"]);
$subStartTime = ($reportDetail[0]["SubStart"]=="") ? "00:00:00" : date("H:i:s",$reportDetail[0]["SubStart"]);
$subEnd = ($reportDetail[0]["SubEnd"]=="") ? "" : date("Y-m-d",$reportDetail[0]["SubEnd"]);
$subEndTime = ($reportDetail[0]["SubEnd"]=="") ? "23:59:59" : date("H:i:s",$reportDetail[0]["SubEnd"]);

$verStart = ($reportDetail[0]["VerStart"]=="") ? "" : date("Y-m-d",$reportDetail[0]["VerStart"]);
$verStartTime = ($reportDetail[0]["VerStart"]=="") ? "00:00:00" : date("H:i:s",$reportDetail[0]["VerStart"]);
$verEnd = ($reportDetail[0]["VerEnd"]=="") ? "" : date("Y-m-d",$reportDetail[0]["VerEnd"]);
$verEndTime = ($reportDetail[0]["VerEnd"]=="") ? "23:59:59" : date("H:i:s",$reportDetail[0]["VerEnd"]);

$PCStart = ($reportDetail[0]["PCStart"]=="") ? "" : date("Y-m-d",$reportDetail[0]["PCStart"]);
$PCStartTime = ($reportDetail[0]["PCStart"]=="") ? "00:00:00" : date("H:i:s",$reportDetail[0]["PCStart"]);
$PCEnd = ($reportDetail[0]["PCEnd"]=="") ? "" : date("Y-m-d",$reportDetail[0]["PCEnd"]);
$PCEndTime = ($reportDetail[0]["PCEnd"]=="") ? "23:59:59" : date("H:i:s",$reportDetail[0]["PCEnd"]);

$PublishStart = ($reportDetail[0]["PublishStart"]=="") ? "" : date("Y-m-d",$reportDetail[0]["PublishStart"]);
$PublishStartTime = ($reportDetail[0]["PublishStart"]=="") ? "00:00:00" : date("H:i:s",$reportDetail[0]["PublishStart"]);
$PublishEnd = ($reportDetail[0]["PublishEnd"]=="") ? "" : date("Y-m-d",$reportDetail[0]["PublishEnd"]);
$PublishEndTime = ($reportDetail[0]["PublishEnd"]=="") ? "23:59:59" : date("H:i:s",$reportDetail[0]["PublishEnd"]);

$CTCStart = ($reportDetail[0]["CTCStart"]=="") ? "" : date("Y-m-d",$reportDetail[0]["CTCStart"]);
$CTCStartTime = ($reportDetail[0]["CTCStart"]=="") ? "00:00:00" : date("H:i:s",$reportDetail[0]["CTCStart"]);
$CTCEnd = ($reportDetail[0]["CTCEnd"]=="") ? "" : date("Y-m-d",$reportDetail[0]["CTCEnd"]);
$CTCEndTime = ($reportDetail[0]["CTCEnd"]=="") ? "23:59:59" : date("H:i:s",$reportDetail[0]["CTCEnd"]);
		
$issue = ($reportDetail[0]["Issued"]=="") ? "" : date("Y-m-d",$reportDetail[0]["Issued"]);


$specificSubmissionPeriodAry = $lreportcard_schedule->getSpecificMarksheetSubmissionPeriodData($ReportID);
$specificSubStart = ($specificSubmissionPeriodAry[0]["SubStart"]=="") ? "" : date("Y-m-d",$specificSubmissionPeriodAry[0]["SubStart"]);
$specificSubStartTime = ($specificSubmissionPeriodAry[0]["SubStart"]=="") ? "00:00:00" : date("H:i:s",$specificSubmissionPeriodAry[0]["SubStart"]);
$specificSubEnd = ($specificSubmissionPeriodAry[0]["SubEnd"]=="") ? "" : date("Y-m-d",$specificSubmissionPeriodAry[0]["SubEnd"]);
$specificSubEndTime = ($specificSubmissionPeriodAry[0]["SubEnd"]=="") ? "23:59:59" : date("H:i:s",$specificSubmissionPeriodAry[0]["SubEnd"]);



### Construct UI elements 
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);


$x = '';
if($isEditAll){
	$x .= $tableObj->returnHtml(); 
	$x .= "\n<br />"; 
}
else{
	$x .= $lreportcard_ui->Get_Settings_ReportCardTemplate_ReportCardInfo_Table($ReportID);
}
// 2015-05-13: commented - prevent displaying incorrect records
//$x .= $lreportcard_ui->Get_Settings_ReportCardTemplate_ReportCardInfo_Table($ReportID);

$x .= '<table class="form_table_v30">'."\n";
	// Submission Period
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$eReportCard['SubmissionPeriod'].'</td>'."\n";
		$x .= '<td>'."\n";
 			$x .= $Lang['General']['From'];
 			$x .= ' ';
 			$x .= $linterface->GET_DATE_PICKER("subStart",$subStart).' '.$linterface->Get_Time_Selection("subStart", $subStartTime);
 			$x .= ' ';
 			$x .= $Lang['General']['To'];
 			$x .= ' ';
 			$x .= $linterface->GET_DATE_PICKER("subEnd",$subEnd).' '.$linterface->Get_Time_Selection("subEnd", $subEndTime);
 			$x .= $linterface->Get_Form_Warning_Msg('marksheetSubmission_inputDateWarnDiv', $Lang['General']['WarningArr']['InputDate'], 'warningMsgDiv', $display=false);
 			$x .= $linterface->Get_Form_Warning_Msg('marksheetSubmission_endDateEarlierWarnDiv', $Lang['General']['WarningArr']['EndDateCannotEarlierThanStartDate'], 'warningMsgDiv', $display=false);
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
	
	if ($isEditAll && count($_POST['ReportIDs'])>1 && $eRCTemplateSetting['Management']['Schedule']['SpecificMarksheetSubmissionPeriod_subjectBased']) {
		// do not allow subject based special submission settings if selected more than 1 report since too much checking => must set report by report
	}
	else if ($eRCTemplateSetting['Management']['Schedule']['SpecificMarksheetSubmissionPeriod']) {
		$yesClicked = ($applySpecificMarksheetSubmission)? true : false;
		$noClicked = ($applySpecificMarksheetSubmission)? false : true;
		
		$yesRadio = $linterface->Get_Radio_Button('ApplySpecificMarksheetSubmission_Yes', 'ApplySpecificMarksheetSubmission', 1, $yesClicked, $Class="", $Lang['General']['Yes'], 'clickedApplySpecificMarksheetSubmissionRadio();');
		$noRadio = $linterface->Get_Radio_Button('ApplySpecificMarksheetSubmission_No', 'ApplySpecificMarksheetSubmission', 0, $noClicked, $Class="", $Lang['General']['No'], 'clickedApplySpecificMarksheetSubmissionRadio();');
		
		
		$x .= '<tr>'."\n";
			$x .= '<td class="field_title">'.$eReportCard['ManagementArr']['SchdeuleArr']['ApplySpecificSubmissionPeriod'].'</td>'."\n";
			$x .= '<td>'."\n";
				$x .= '<div>'.$yesRadio.' '.$noRadio.'</div>'."\n";
				$x .= '<div id="specificMarcksheetSubmissionPeriodDiv">'."\n";
					$x .= '<br />'."\n";
					$x .= '<table class="inside_form_table">'."\n";
						$x .= '<tr>'."\n";
							$x .= '<td>'."\n";
					 			$x .= $Lang['General']['From'];
					 			$x .= ' ';
					 			$x .= $linterface->GET_DATE_PICKER("specificSubStart",$specificSubStart).' '.$linterface->Get_Time_Selection("specificSubStart", $specificSubStartTime);
					 			$x .= ' ';
					 			$x .= $Lang['General']['To'];
					 			$x .= ' ';
					 			$x .= $linterface->GET_DATE_PICKER("specificSubEnd",$specificSubEnd).' '.$linterface->Get_Time_Selection("specificSubEnd", $specificSubEndTime);
					 			$x .= $linterface->Get_Form_Warning_Msg('specificMarksheetSubmission_inputDateWarnDiv', $Lang['General']['WarningArr']['InputDate'], 'warningMsgDiv', $display=false);
					 			$x .= $linterface->Get_Form_Warning_Msg('specificMarksheetSubmission_endDateEarlierWarnDiv', $Lang['General']['WarningArr']['EndDateCannotEarlierThanStartDate'], 'warningMsgDiv', $display=false);
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						
						if ($eRCTemplateSetting['Management']['Schedule']['SpecificMarksheetSubmissionPeriod_subjectBased']) {
							// teacher with subject-based settings
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									
									$teacherIdAry = Get_Array_By_Key($specificSubmissionPeriodAry, 'UserID');
									$userObj = new libuser('', '', $teacherIdAry);
									
									$subjetcIdAry = Get_Array_By_Key($specificSubmissionPeriodAry, 'SubjectID');
									$numOfSettings = count($specificSubmissionPeriodAry);
									
									$nameField = Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN');
									$subjectGroupAssoAry = BuildMultiKeyAssoc($lreportcard->Get_Subject_Group_List_Of_Report($ReportID, '', $subjetcIdAry, $IncludeSubject=false, $returnFullList=true), 'SubjectGroupID', array($nameField), $SingleValue=1);
									
									if ($numOfSettings > 0) {
										for ($i=0; $i<$numOfSettings; $i++) {
											$_teacherId = $specificSubmissionPeriodAry[$i]['UserID'];
											$_subjectId = $specificSubmissionPeriodAry[$i]['SubjectID'];
											$_subjectGroupId = $specificSubmissionPeriodAry[$i]['SubjectGroupID'];
											
											$userObj->loadUserData($_teacherId);
											$_teacherName = Get_Lang_Selection($userObj->ChineseName, $userObj->EnglishName);
											
											$_subjectName = $lreportcard->GET_SUBJECT_NAME($_subjectId, $ShortName=0, $Bilingual=0);
											$_subjectGroupName = $subjectGroupAssoAry[$_subjectGroupId];
											
											if ($_subjectGroupId > 0) {
												$_displayName = $_subjectGroupName;
											}
											else {
												$_displayName = $_subjectName;
											}
											
											$x .= '<div id="specialSubmissionDiv_'.$_teacherId.'_'.$_subjectId.'_'.$_subjectGroupId.'" class="specialSubmissionTeacherSubjectDiv">';
												$x .= '<span>'.$_teacherName.': '.$_displayName.'</span>';
												$x .= ' ';
												$x .= '<span><a class="tablelink" href="javascript:void(0);" onclick="removeSpecialSubmissionTeacherSubject(\''.$_teacherId.'\', \''.$_subjectId.'\', \''.$_subjectGroupId.'\');">['.$Lang['Btn']['Delete'].']</a></span>';
												$x .= '<input type="hidden" id="specialSubmissionHidden_'.$_teacherId.'_'.$_subjectId.'_'.$_subjectGroupId.'" name="specialSubmissionAssoAry['.$_teacherId.'][]" value="'.$_subjectId.'_'.$_subjectGroupId.'">';
											$x .= '</div>';
										}
									}
									$x .= '<div id="specialSubmissionPeriodAddTeacherLinkDiv">'."\r\n";
										$x .= '<a id="specialSubmissionPeriodAddTeacherLink" class="tablelink" href="javascript:void(0);">+ '.$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['AddTeacher'].'</a>'."\r\n";
									$x .= '</div>'."\r\n";
									$x .= $linterface->Get_Form_Warning_Msg('specificMarksheetSubmission_selectTeacherWarnDiv', $eReportCard['ManagementArr']['SchdeuleArr']['WarningArr']['PleaseSelectTeacher'], 'warningMsgDiv', $display=false);
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
						}
						else {
							// basic => only teacher settings
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
										$x .= '<tr>'."\n";
											$x .= '<td width="50%" bgcolor="#EFFEE" class="steptitletext">'.$eReportCard['ManagementArr']['SchdeuleArr']['AvailableTeacher'].' </td>'."\n";
											$x .= '<td width="40"></td>'."\n";
											$x .= '<td width="50%" bgcolor="#EFFEE2" class="steptitletext">'.$eReportCard['ManagementArr']['SchdeuleArr']['SelectedTeacher'].' </td>'."\n";
										$x .= '</tr>'."\n";
										$x .= '<tr>'."\n";
											$x .= '<td bgcolor="#EEEEEE" align="center">'."\n";
												$x .= '<div id="specificMarksheetSubmissionAvailableTeacherSelectionDiv">';
												$x .= '</div>';
												$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].' </span>'."\n";
											$x .= '</td>'."\n";
											$x .= '<td>'."\n";
												$x .= $linterface->GET_SMALL_BTN('>>', 'button', 'addAllTeacher();', $ParName='AddAll', 'style="width:40px;"', '', $Lang['Btn']['AddAll']);
												$x .= '<br />'."\n";
												$x .= $linterface->GET_SMALL_BTN('>', 'button', 'addSelectedTeacher();', $ParName='Add', 'style="width:40px;"', '', $Lang['Btn']['AddSelected']);
												$x .= '<br /><br />'."\n";
												$x .= $linterface->GET_SMALL_BTN('<', 'button', 'removeSelectedTeacher();', $ParName='Remove', 'style="width:40px;"', '', $Lang['Btn']['RemoveSelected']);
												$x .= '<br />'."\n";
												$x .= $linterface->GET_SMALL_BTN('<<', 'button', 'removeAllTeacher();', $ParName='RemoveAll', 'style="width:40px;"', '', $Lang['Btn']['RemoveAll']);
											$x .= '</td>'."\n";
											$x .= '<td bgcolor="#EFFEE2">'."\n";
												$x .= '<div id="specificMarksheetSubmissionSelectedTeacherSelectionDiv">';
												$x .= '</div>';
												$x .= $linterface->Get_Form_Warning_Msg('specificMarksheetSubmission_selectTeacherWarnDiv', $eReportCard['ManagementArr']['SchdeuleArr']['WarningArr']['PleaseSelectTeacher'], 'warningMsgDiv', $display=false);
											$x .= '</td>'."\n";
										$x .= '</tr>'."\n";
									$x .= '</table>'."\n";
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
						}
					$x .= '</table>'."\n";
				$x .= '</div>'."\n";
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
	}
	
	// Verification Period
	if($accessSettingData[EnableVerificationPeriod]==1){
		$x .= '<tr>'."\n";
			$x .= '<td class="field_title">'.$eReportCard['VerificationPeriod'].'</td>'."\n";
			$x .= '<td>'."\n";
	 			$x .= $Lang['General']['From'];
	 			$x .= ' ';
	 			$x .= $linterface->GET_DATE_PICKER("verStart",$verStart,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1).' '.$linterface->Get_Time_Selection("verStart", $verStartTime);
	 			$x .= ' ';
	 			$x .= $Lang['General']['To'];
	 			$x .= ' ';
	 			$x .= $linterface->GET_DATE_PICKER("verEnd",$verEnd,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1).' '.$linterface->Get_Time_Selection("verEnd", $verEndTime);
	 			$x .= $linterface->Get_Form_Warning_Msg('verificationPeriod_endDateEarlierWarnDiv', $Lang['General']['WarningArr']['EndDateCannotEarlierThanStartDate'], 'warningMsgDiv', $display=false);
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
	}
	if ($eRCTemplateSetting['Management']['Schedule']['PublishReportPeriod']) {
		// Report Publishing Period
		$x .= '<tr>'."\n";
			$x .= '<td class="field_title">'.$eReportCard['PublishReportPeriod'].'</td>'."\n";
			$x .= '<td>'."\n";
	 			$x .= $Lang['General']['From'];
	 			$x .= ' ';
	 			$x .= $linterface->GET_DATE_PICKER("PublishStart", $PublishStart, $OtherMember="", $DateFormat="yy-mm-dd", $MaskFunction="", $ExtWarningLayerID="", $ExtWarningLayerContainer="", $OnDatePickSelectedFunction="", $ID="", $SkipIncludeJS=0, $CanEmptyField=1).' '.$linterface->Get_Time_Selection("PublishStart", $PublishStartTime);
	 			$x .= ' ';
	 			$x .= $Lang['General']['To'];
	 			$x .= ' ';
	 			$x .= $linterface->GET_DATE_PICKER("PublishEnd", $PublishEnd, $OtherMember="", $DateFormat="yy-mm-dd", $MaskFunction="", $ExtWarningLayerID="", $ExtWarningLayerContainer="", $OnDatePickSelectedFunction="", $ID="", $SkipIncludeJS=0, $CanEmptyField=1).' '.$linterface->Get_Time_Selection("PublishEnd", $PublishEndTime);
	 			$x .= $linterface->Get_Form_Warning_Msg('publishDate_endDateEarlierWarnDiv', $Lang['General']['WarningArr']['EndDateCannotEarlierThanStartDate'], 'warningMsgDiv', $display=false);
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
	}
	if ($eRCTemplateSetting['Management']['Schedule']['ClassTeacherCommentSubmissionPeriod']) {
		// Class Teacher Comment Submission Period
		$x .= '<tr>'."\n";
			$x .= '<td class="field_title">'.$eReportCard['ManagementArr']['ClassTeacherCommentArr']['ClassTeacherCommentSubmissionPeriod'].'</td>'."\n";
			$x .= '<td>'."\n";
	 			$x .= $Lang['General']['From'];
	 			$x .= ' ';
	 			$x .= $linterface->GET_DATE_PICKER("CTCStart", $CTCStart, $OtherMember="", $DateFormat="yy-mm-dd", $MaskFunction="", $ExtWarningLayerID="", $ExtWarningLayerContainer="", $OnDatePickSelectedFunction="", $ID="", $SkipIncludeJS=0, $CanEmptyField=1).' '.$linterface->Get_Time_Selection("CTCStart", $CTCStartTime);
	 			$x .= ' ';
	 			$x .= $Lang['General']['To'];
	 			$x .= ' ';
	 			$x .= $linterface->GET_DATE_PICKER("CTCEnd", $CTCEnd, $OtherMember="", $DateFormat="yy-mm-dd", $MaskFunction="", $ExtWarningLayerID="", $ExtWarningLayerContainer="", $OnDatePickSelectedFunction="", $ID="", $SkipIncludeJS=0, $CanEmptyField=1).' '.$linterface->Get_Time_Selection("CTCEnd", $CTCEndTime);
	 			//$x .= $linterface->Get_Form_Warning_Msg('ctcSubmissionDate_inputDateWarnDiv', $Lang['General']['WarningArr']['InputDate'], 'warningMsgDiv', $display=false);
	 			$x .= $linterface->Get_Form_Warning_Msg('ctcSubmissionDate_endDateEarlierWarnDiv', $Lang['General']['WarningArr']['EndDateCannotEarlierThanStartDate'], 'warningMsgDiv', $display=false);
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
	}
	if ($eRCTemplateSetting['PersonalCharacteristicSubmissionSchedule']) {
		// Personal Characteristics Submission Period
		$x .= '<tr>'."\n";
			$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$eReportCard['PCSubmissionPeriod'].'</td>'."\n";
			$x .= '<td>'."\n";
	 			$x .= $Lang['General']['From'];
	 			$x .= ' ';
	 			$x .= $linterface->GET_DATE_PICKER("PCStart",$PCStart).' '.$linterface->Get_Time_Selection("PCStart", $PCStartTime);
	 			$x .= ' ';
	 			$x .= $Lang['General']['To'];
	 			$x .= ' ';
	 			$x .= $linterface->GET_DATE_PICKER("PCEnd",$PCEnd).' '.$linterface->Get_Time_Selection("PCEnd", $PCEndTime);
	 			$x .= $linterface->Get_Form_Warning_Msg('pcSubmissionDate_inputDateWarnDiv', $Lang['General']['WarningArr']['InputDate'], 'warningMsgDiv', $display=false);
	 			$x .= $linterface->Get_Form_Warning_Msg('pcSubmissionDate_endDateEarlierWarnDiv', $Lang['General']['WarningArr']['EndDateCannotEarlierThanStartDate'], 'warningMsgDiv', $display=false);
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
	}
	
	// Issue Date
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$eReportCard['IssueDate'].'</td>'."\n";
		$x .= '<td>'."\n";
 			$x .= $linterface->GET_DATE_PICKER("issue",$issue);
 			$x .= $linterface->Get_Form_Warning_Msg('issueDate_inputDateWarnDiv', $Lang['General']['WarningArr']['InputDate'], 'warningMsgDiv', $display=false);
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
$x .= '</table>'."\n";
$htmlAry['editTable'] = $x;


$htmlAry['tableRemarks'] = $linterface->MandatoryField();


$htmlAry['submitBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "goSubmit();");
$htmlAry['cancelBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "goCancel();");
?>

<script language="javascript">
var ajaxLoadingImg = '<?=$linterface->Get_Ajax_Loading_Image($noLang=0, $custLang='')?>';

$(document).ready(function () {
	clickedApplySpecificMarksheetSubmissionRadio();
	reloadSpecialMarksheetSubmissionTeacherSelection(1);
	
	$('a#specialSubmissionPeriodAddTeacherLink').click( function() {
		load_dyn_size_thickbox_ip('<?=$eReportCard['ManagementArr']['SchdeuleArr']['SpecificSubmissionPeriod']?>', 'onloadSpecialSubmissionTb();', '', 300, 600);
	});
});

function onloadSpecialSubmissionTb() {
	$('div#TB_ajaxContent').html(ajaxLoadingImg).load(
		"ajax_reload.php", 
		{ 
			task: 'reloadSpecialSubmissionTbContent',
			reportId: '<?=$ReportID?>'
		},
		function(ReturnData) {
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
			
			changedSpecialSubmissionTeacherSel($('select#specialSubmissionTb_teacherIdSel').val());
		}
	);
}

function changedSpecialSubmissionTeacherSel(parTeacherId) {
	reloadSubjectSelection(parTeacherId);
}

function changedSpecialSubmissionSubjectSel() {
	var teacherId = $('select#specialSubmissionTb_teacherIdSel').val();
	var subjectId = $('select#specialSubmissionTb_subjectId').val();
	
	reloadSubjectGroupSelection(teacherId, subjectId);
}

function reloadSubjectSelection(parTeacherId) {
	$('div#specialSubmissionTb_subjectSelDiv').html(ajaxLoadingImg).load(
		"ajax_reload.php", 
		{ 
			task: 'reloadSubjectSelection',
			reportId: '<?=$ReportID?>',
			teacherId: parTeacherId
		},
		function(ReturnData) {
			<?php if ($eRCTemplateSetting['Management']['Schedule']['SpecificMarksheetSubmissionPeriod_subjectGroupBased'] && $reportType=='T') { ?>
				reloadSubjectGroupSelection(parTeacherId, $('select#specialSubmissionTb_subjectId').val());
			<?php } ?>
		}
	);
}

function reloadSubjectGroupSelection(parTeacherId, parSubjectId) {
	$('div#specialSubmissionTb_subjectGroupSelDiv').html(ajaxLoadingImg).load(
		"ajax_reload.php", 
		{ 
			task: 'reloadSubjectGroupSelection',
			reportId: '<?=$ReportID?>',
			teacherId: parTeacherId,
			subjectId: parSubjectId
		},
		function(ReturnData) {
			
		}
	);
}

function addSpecialSubmissionTeacherSubject() {
	var selectedTeacherId = $('select#specialSubmissionTb_teacherIdSel').val();
	var selectedTeacherName = $("select#specialSubmissionTb_teacherIdSel option:selected").text();
	var selectedSubjectId = $('select#specialSubmissionTb_subjectId').val();
	var selectedSubjectName = $("select#specialSubmissionTb_subjectId option:selected").text();
	var selectedSubjectGroupId = $('select#specialSubmissionTb_subjectGroupIdSel').val();
	var selectedSubjectGroupName = $("select#specialSubmissionTb_subjectGroupIdSel option:selected").text();
	
	selectedSubjectGroupId = (selectedSubjectGroupId == '')? 0 : selectedSubjectGroupId;
	
	var testDiv = '';
	testDiv += '<div id="specialSubmissionDiv_' + selectedTeacherId + '_' + selectedSubjectId + '_' + selectedSubjectGroupId + '" class="specialSubmissionTeacherSubjectDiv">';
		testDiv += '<span>' + selectedTeacherName + ': ' + (selectedSubjectGroupName==''? selectedSubjectName : selectedSubjectGroupName) + '</span>';
		testDiv += ' ';
		testDiv += '<span><a class="tablelink" href="javascript:void(0);" onclick="removeSpecialSubmissionTeacherSubject(\'' + selectedTeacherId + '\', \'' + selectedSubjectId + '\' , \'' + selectedSubjectGroupId + '\');">[<?=$Lang['Btn']['Delete']?>]</a></span>';
		testDiv += '<input type="hidden" id="specialSubmissionHidden_' + selectedTeacherId + '_' + selectedSubjectId + '_' + selectedSubjectGroupId + '" name="specialSubmissionAssoAry[' + selectedTeacherId + '][]" value="' + selectedSubjectId + '_' + selectedSubjectGroupId + '">';
	testDiv += '</div>';
	
	$( testDiv ).insertBefore( "div#specialSubmissionPeriodAddTeacherLinkDiv" );
	js_Hide_ThickBox();
}

function removeSpecialSubmissionTeacherSubject(parTeacherId, parSubjectId, parSubjectGroupId) {
	parSubjectGroupId = parSubjectGroupId || 0;
	
	var divId = 'specialSubmissionDiv_' + parTeacherId + '_' + parSubjectId + '_' + parSubjectGroupId;
	$('div#' + divId).remove();
}

function goSubmit() {
	var canSubmit = true;
	$('div.warningMsgDiv').hide();
	
	// marksheet submission start date
	if (Trim($('input#subStart').val())=='') {
		$('input#subStart').focus();
		$('div#marksheetSubmission_inputDateWarnDiv').show();
		canSubmit = false;
	}
	
	// marksheet submission end date
	if (Trim($('input#subEnd').val())=='') {
		$('input#subEnd').focus();
		$('div#marksheetSubmission_inputDateWarnDiv').show();
		canSubmit = false;
	}
	
	// marksheet submission end date must after start date
	if (!compareTimeByObjId('subStart', 'subStart_hour', 'subStart_min', 'subStart_sec', 'subEnd', 'subEnd_hour', 'subEnd_min', 'subEnd_sec')) {
		$('input#subStart').focus();
		$('div#marksheetSubmission_endDateEarlierWarnDiv').show();
		canSubmit = false;
	}
	
	if ($('input#ApplySpecificMarksheetSubmission_Yes').attr('checked')) {
		// specific marksheet submission start date
		if (Trim($('input#specificSubStart').val())=='') {
			$('input#specificSubStart').focus();
			$('div#specificMarksheetSubmission_inputDateWarnDiv').show();
			canSubmit = false;
		}
		
		// specific marksheet submission end date
		if (Trim($('input#specificSubEnd').val())=='') {
			$('input#specificSubEnd').focus();
			$('div#specificMarksheetSubmission_inputDateWarnDiv').show();
			canSubmit = false;
		}
		
		// specific marksheet submission end date must after start date
		if (!compareTimeByObjId('specificSubStart', 'specificSubStart_hour', 'specificSubStart_min', 'specificSubStart_sec', 'specificSubEnd', 'specificSubEnd_hour', 'specificSubEnd_min', 'specificSubEnd_sec')) {
			$('input#specificSubStart').focus();
			$('div#specificMarksheetSubmission_endDateEarlierWarnDiv').show();
			canSubmit = false;
		}
		
		<? if ($eRCTemplateSetting['Management']['Schedule']['SpecificMarksheetSubmissionPeriod_subjectBased']) { ?>
			if ($('div.specialSubmissionTeacherSubjectDiv').length == 0) {
				$('div#specificMarksheetSubmission_selectTeacherWarnDiv').show();
				canSubmit = false;
			}
		<? } else { ?>
			$('select#specificMarksheetSubmissionSelectedTeacherSel option').attr('selected', 'selected');
			
			// specific marksheet submission target teacher
			if ($('select#specificMarksheetSubmissionSelectedTeacherSel option:selected').length == 0) {
				$('select#specificMarksheetSubmissionAvailableTeacherSel').focus();
				$('div#specificMarksheetSubmission_selectTeacherWarnDiv').show();
				canSubmit = false;
			}
		<? }?>
	}
	
	
	// verification end date must after start date 
	if (Trim($('input#verStart').val())!='' && Trim($('input#verEnd').val())=='') {
		if (!compareTimeByObjId('verStart', 'verStart_hour', 'verStart_min', 'verStart_sec', 'verEnd', 'verEnd_hour', 'verEnd_min', 'verEnd_sec')) {
			$('input#verStart').focus();
			$('div#verificationPeriod_endDateEarlierWarnDiv').show();
			canSubmit = false;
		}
	}
	
	<? if ($eRCTemplateSetting['Management']['Schedule']['PublishReportPeriod']) { ?>
		// report publishing end date must after start date
		if (!compareTimeByObjId('PublishStart', 'PublishStart_hour', 'PublishStart_min', 'PublishStart_sec', 'PublishEnd', 'PublishEnd_hour', 'PublishEnd_min', 'PublishEnd_sec')) {
			$('input#PublishStart').focus();
			$('div#publishDate_endDateEarlierWarnDiv').show();
			canSubmit = false;
		}
	<? } ?>
	
	<? if ($eRCTemplateSetting['Management']['Schedule']['ClassTeacherCommentSubmissionPeriod']) { ?>
		/*
		// class teacher comment start date
		if (Trim($('input#CTCStart').val())=='') {
			$('input#CTCStart').focus();
			$('div#ctcSubmissionDate_inputDateWarnDiv').show();
			canSubmit = false;
		}
		
		// class teacher comment submission end date
		if (Trim($('input#CTCEnd').val())=='') {
			$('input#CTCEnd').focus();
			$('div#ctcSubmissionDate_inputDateWarnDiv').show();
			canSubmit = false;
		}
		*/
		// class teacher comment submission end date must after start date
		if (!compareTimeByObjId('CTCStart', 'CTCStart_hour', 'CTCStart_min', 'CTCStart_sec', 'CTCEnd', 'CTCEnd_hour', 'CTCEnd_min', 'CTCEnd_sec')) {
			$('input#CTCStart').focus();
			$('div#ctcSubmissionDate_endDateEarlierWarnDiv').show();
			canSubmit = false;
		}
	<? } ?>
	
	<? if ($eRCTemplateSetting['PersonalCharacteristicSubmissionSchedule']) { ?>
		// personal characteristics submission start date
		if (Trim($('input#PCStart').val())=='') {
			$('input#PCStart').focus();
			$('div#pcSubmissionDate_inputDateWarnDiv').show();
			canSubmit = false;
		}
		
		// personal characteristics submission end date
		if (Trim($('input#PCEnd').val())=='') {
			$('input#PCEnd').focus();
			$('div#pcSubmissionDate_inputDateWarnDiv').show();
			canSubmit = false;
		}
		
		// personal characteristics submission end date must after start date
		if (!compareTimeByObjId('PCStart', 'PCStart_hour', 'PCStart_min', 'PCStart_sec', 'PCEnd', 'PCEnd_hour', 'PCEnd_min', 'PCEnd_sec')) {
			$('input#PCStart').focus();
			$('div#pcSubmissionDate_endDateEarlierWarnDiv').show();
			canSubmit = false;
		}
	<? } ?>
	
	// issue date
	if (Trim($('input#issue').val())=='') {
		$('input#issue').focus();
		$('div#issueDate_inputDateWarnDiv').show();
		canSubmit = false;
	}
	
	
	if (canSubmit) {
		$('form#form1').attr('action', 'edit_update.php').submit();
	}
}

function goCancel() {
	window.location = 'schedule.php';
}

function clickedApplySpecificMarksheetSubmissionRadio() {
	if ($('input#ApplySpecificMarksheetSubmission_Yes').attr('checked')) {
		$('div#specificMarcksheetSubmissionPeriodDiv').show();
	}
	else {
		$('div#specificMarcksheetSubmissionPeriodDiv').hide();
	}
}

function reloadSpecialMarksheetSubmissionTeacherSelection(isInitialize, addTeacherIdList, removeTeacherIdList) {
	isInitialize = isInitialize || 0;
	addTeacherIdList = addTeacherIdList || '';
	removeTeacherIdList = removeTeacherIdList || '';
	
	$('div#specificMarksheetSubmissionAvailableTeacherSelectionDiv').load(
		"ajax_reload.php", 
		{ 
			task: 'reloadAvailableTeacherSelection',
			reportId: $('input#ReportID').val(),
			isInitialize: isInitialize,
			addTeacherIdList: addTeacherIdList,
			removeTeacherIdList: removeTeacherIdList,
			curAvailableTeacherIdList: getAvailableTeacherIdList(),
			curSelectedTeacherIdList: getSelectedTeacherIdList()
		},
		function(ReturnData) {
			
		}
	);
	
	$('div#specificMarksheetSubmissionSelectedTeacherSelectionDiv').load(
		"ajax_reload.php", 
		{ 
			task: 'reloadSelectedTeacherSelection',
			reportId: $('input#ReportID').val(),
			isInitialize: isInitialize,
			addTeacherIdList: addTeacherIdList,
			removeTeacherIdList: removeTeacherIdList,
			curAvailableTeacherIdList: getAvailableTeacherIdList(),
			curSelectedTeacherIdList: getSelectedTeacherIdList()
		},
		function(ReturnData) {
			
		}
	);
}

function addAllTeacher() {
	reloadSpecialMarksheetSubmissionTeacherSelection(0, getAvailableTeacherIdList(false));
}

function addSelectedTeacher() {
	reloadSpecialMarksheetSubmissionTeacherSelection(0, getAvailableTeacherIdList(true));
}

function removeSelectedTeacher() {
	reloadSpecialMarksheetSubmissionTeacherSelection(0, '', getSelectedTeacherIdList(true));
}

function removeAllTeacher() {
	reloadSpecialMarksheetSubmissionTeacherSelection(0, '', getSelectedTeacherIdList(false));
}

function getAvailableTeacherIdList(selectedOnly) {
	var selectedParam = '';
	if (selectedOnly) {
		selectedParam = ':selected';
	}
	
	var teacherIdAry = new Array();
	$('select#specificMarksheetSubmissionAvailableTeacherSel option' + selectedParam).each( function() {
		teacherIdAry[teacherIdAry.length] = $(this).val();
	})
	
	return teacherIdAry.join(',');
}

function getSelectedTeacherIdList(selectedOnly) {
	var selectedParam = '';
	if (selectedOnly) {
		selectedParam = ':selected';
	}
	
	var teacherIdAry = new Array();
	$('select#specificMarksheetSubmissionSelectedTeacherSel option' + selectedParam).each( function() {
		teacherIdAry[teacherIdAry.length] = $(this).val();
	})
	
	return teacherIdAry.join(',');
}
</script>

<br />
<form id="form1" name="form1" method="post">
	<?=$htmlAry['navigation']?>
	<br />
	
	<?=$htmlAry['editTable']?>
	<br />
	
	<?=$htmlAry['tableRemarks']?>
	
	<div class="edit_bottom_v30">
		<?=$htmlAry['submitBtn']?>
		<?=$htmlAry['cancelBtn']?>
	</div>
	<?=$hiddenInput?>
	<input type="hidden" id="ReportID" name="ReportID" value="<?=$ReportID?>" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>