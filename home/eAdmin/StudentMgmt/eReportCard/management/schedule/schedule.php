<?php
// Using: 

/********************************************************
 * Modification log
 * 20170322 Bill:	[2017-0109-1818-40164]
 * 		- add Report Publishing Period ($eRCTemplateSetting['Management']['Schedule']['PublishReportPeriod'])
 * 20161118 Bill:	[2015-1104-1130-08164]
 * 		- add Class Teacher's Comment Submission Period ($eRCTemplateSetting['Management']['Schedule']['ClassTeacherCommentSubmissionPeriod'])
 * 20130205 Rita:
 * 		- add verification period checking $accessSettingData[EnableVerificationPeriod]==1
 * 20120326 Marcus:
 * 		- Cater allow to set time for submission, verification
 * 		- 2012-0112-1622-04073 - 迦密愛禮信中學 - Schedule of Report Card Management
 * ******************************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();

$lreportcard_ui = new libreportcard_ui();
$linterface = new interface_html();

$CurrentPage = "Management_Schedule";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# Cookie for maintaining filter status after save / cancel
$arrCookies = array();
$arrCookies[] = array("ck_eRC_Management_Schedule_Record_Form_Selection", "ClassLevelID"); 
$arrCookies[] = array("ck_eRC_Management_Schedule_Record_Term_Selection", "semester"); 
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else 
	updateGetCookies($arrCookies);
	
############################################################################################################
# Load the highlight setting data
$accessSettingData = $lreportcard->LOAD_SETTING("AccessSettings");

# Get ClassLevelID (Form) of the reportcard template
$libYear = new Year();
$FormArr = $libYear->Get_All_Year_List();
$numOfForm = count($FormArr);
for ($i=0; $i<$numOfForm; $i++)
{
	$thisClassLevelID = $FormArr[$i]['YearID'];
	$thisLevelName = $FormArr[$i]['YearName'];
	$classLevelName[$thisClassLevelID] = $thisLevelName;
}

//$lib_form_class_manage_ui = new form_class_manage_ui();
if ($ClassLevelID == "") {
	$ClassLevelID = "-1";
}
$FormSelection = $lreportcard_ui->Get_Form_Selection('ClassLevelID', $ClassLevelID, 'document.form1.submit();', $noFirst=1, $isAll=1, $hasTemplateOnly=1, $checkPermission=0, $IsMultiple=0);
//$FormSelection = $lib_form_class_manage_ui->Get_Form_Selection('ClassLevelID', $ClassLevelID, 'document.form1.submit()', $noFirst=1, $isAll=1);

# Filters - By Type (Term1, Term2, Whole Year, etc)
// Get Semester Type from the reportcard template corresponding to ClassLevelID
$ReportTypeSelection = '';
$ReportTypeArr = array();
$ReportTypeOption = array();
$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);
if(count($ReportTypeArr) > 0){
	for($j=0 ; $j<count($ReportTypeArr) ; $j++){		
		$ReportTypeOption[$j][0] = $ReportTypeArr[$j]['ReportID'];
		$ReportTypeOption[$j][1] = $ReportTypeArr[$j]['ReportTitle'];
	}
}
$AllReportOption = array(
						0 => array(
						0 => "-1", 
						1 => $eReportCard['AllReportCards']
						)
						);
$ReportTypeOption = array_merge($AllReportOption, $ReportTypeOption);

$ReportTypeSelection = $linterface->GET_SELECTION_BOX($ReportTypeOption, 'name="ReportID" class="tabletexttoptext" onchange="document.form1.submit()"', '', $ReportID);

if ($ReportID == "") {
	$ReportID = "-1";
}

/*
$semesters = $lreportcard->GET_ALL_SEMESTERS("-1", true);
$all_semester_status = $eReportCard['AllSemesters'];
$semesters = array_merge(array("F"=>$eReportCard['WholeYear']), $semesters);
$select_semester = "<select onchange='this.form.submit()' name='semester'>";
$select_semester .= "<option value='-1'".($semester=="-1"?" selected":"").">$all_semester_status</option>";
foreach ($semesters as $key => $value) {
	$select_semester .= "<option value='$key'".("$semester"==="$key"?" selected":"").">$value</option>";
}
$select_semester .= "</select>";
*/

if ($semester == "") {
	$semester = "-1";
}
$activeAcademicYearID = $lreportcard->schoolYearID;
$select_semester = $lreportcard->Get_Term_Selection('semester', $activeAcademicYearID, $semester, 'this.form.submit()');


# construct the condition
if ($ClassLevelID != "-1" || $semester != "-1") {
	if ($ClassLevelID != "-1") {
		$cond .= "a.ClassLevelID = '$ClassLevelID'";
	}
	if ($ClassLevelID != "-1" && $semester != "-1") {
		$cond .= " AND ";
	}
	if ($semester != "-1") {
		$cond .= "a.Semester = '$semester'";
	}
}

$scheduleEntries = $lreportcard->GET_REPORT_DATE_PERIOD($cond);
$filterbar = $FormSelection.$select_semester;

$reportInfoAry = $lreportcard->Get_Report_List();
$reportInfoAssoAry = BuildMultiKeyAssoc($reportInfoAry, 'ReportID');

############################################################################################################

# tag information
$TAGS_OBJ[] = array($eReportCard['Management_Schedule'], "", 0);
$linterface->LAYOUT_START();

?>
<br/>
<script language="javascript">
<!--
//-->
</script>
<form name="form1" method="POST" action="schedule.php">
<table width="95%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?></td>
		<td align="right"><?= $linterface->GET_SYS_MSG($Result); ?></td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$filterbar?><?=$searchbar?></td>
		<td valign="bottom" align="right">
		</td>
	</tr>
</table>
<!--<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr class="tabletop">
		<td>&nbsp;</td>
		<td class="tabletopnolink term_break_link" align="left"><?=$eReportCard['ReportTitle']?></td>
		<td class="tabletopnolink term_break_link" colspan="2" align="center"><?=$eReportCard['Submission']?></td>
		<td class="tabletopnolink term_break_link" colspan="2" align="center"><?=$eReportCard['Verification']?></td>
		<td class="tabletopnolink" align="center"><?=$eReportCard['IssueDate']?></td>
	</tr>
	<tr class="tabletop">
		<td>&nbsp;</td>
		<td class="term_break_link">&nbsp;</td>
		<td class="tabletopnolink" align="center"><?=$eReportCard['Start']?></td>
		<td class="tabletopnolink term_break_link" align="center"><?=$eReportCard['End']?></td>
			<td class="tabletopnolink" align="center"><?=$eReportCard['Start']?></td>
			<td class="tabletopnolink term_break_link" align="center"><?=$eReportCard['End']?></td>
		<td>&nbsp;</td>
	</tr>-->
<?php
	 
# table tool
if (sizeof($scheduleEntries) > 0) {
	$edit_href = "javascript:checkEditMultiple(document.form1,'ReportIDs[]','edit.php')";
	$table_tool = '<div class="common_table_tool">'."\n";
	$table_tool .= '<a href='.$edit_href.' class="tool_edit">'.$Lang['Btn']['Edit'].'</a>'."\n";
	$table_tool .= '</div>'."\n";
} else {
	$table_tool = "";
}
	
	$x.= '<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">'."\n";
		$x.= '<tr>'."\n";
			$x.= '<td align="left"></td>'."\n";
			$x.= '<td valign="bottom" align="right">'.$table_tool.'</td>'."\n";
		$x.= '</tr>'."\n";
	$x.= '</table>'."\n";
	$x.= '<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">'."\n";
		$x.= '<tr class="tabletop">'."\n";
			$x.= '<td>&nbsp;</td>'."\n";
			$x.= '<td class="tabletopnolink term_break_link" align="left">'.$eReportCard['ReportTitle'].'</td>'."\n";
			$x.= '<td class="tabletopnolink term_break_link" colspan="2" align="center">'.$eReportCard['Submission'].'</td>'."\n";
			
			if($accessSettingData[EnableVerificationPeriod]==1)
				$x.= '<td class="tabletopnolink term_break_link" colspan="2" align="center">'.$eReportCard['Verification'].'</td>'."\n";
			if($eRCTemplateSetting['Management']['Schedule']['PublishReportPeriod'])
				$x.= '<td class="tabletopnolink term_break_link" colspan="2" align="center">'.$eReportCard['PublishReport'].'</td>'."\n";
			if($eRCTemplateSetting['Management']['Schedule']['ClassTeacherCommentSubmissionPeriod'])
				$x.= '<td class="tabletopnolink term_break_link" colspan="2" align="center">'.$eReportCard['Management_ClassTeacherComment'].'</td>'."\n";
			if($eRCTemplateSetting['PersonalCharacteristicSubmissionSchedule'])
				$x.= '<td class="tabletopnolink term_break_link" colspan="2" align="center">'.$eReportCard['PersonalCharacteristics'].'</td>'."\n";
			$x.= '<td class="tabletopnolink" align="center">'.$eReportCard['IssueDate'].'</td>'."\n";
			$x.= '<td class="tableTitle tabletoplink">';
			$checkAllOnClick = "(this.checked)?setChecked(1,this.form,'ReportIDs[]'):setChecked(0,this.form,'ReportIDs[]')";
			$x.= '<input type="checkbox" onClick='.$checkAllOnClick.'></td>';
		$x.= '</tr>'."\n";
		$x.= '<tr class="tabletop">'."\n";
			$x.= '<td>&nbsp;</td>'."\n";
			$x.= '<td class="term_break_link">&nbsp;</td>'."\n";
			$x.= '<td class="tabletopnolink" align="center">'.$eReportCard['Start'].'</td>'."\n";
			$x.= '<td class="tabletopnolink term_break_link" align="center">'.$eReportCard['End'].'</td>'."\n";
			if($accessSettingData[EnableVerificationPeriod]==1){	
				$x.= '<td class="tabletopnolink" align="center">'.$eReportCard['Start'].'</td>'."\n";
				$x.= '<td class="tabletopnolink term_break_link" align="center">'.$eReportCard['End'].'</td>'."\n";
			}
			if($eRCTemplateSetting['Management']['Schedule']['PublishReportPeriod']){
				$x.= '<td class="tabletopnolink" align="center">'.$eReportCard['Start'].'</td>'."\n";
				$x.= '<td class="tabletopnolink term_break_link" align="center">'.$eReportCard['End'].'</td>'."\n";
			}
			if($eRCTemplateSetting['Management']['Schedule']['ClassTeacherCommentSubmissionPeriod']){
				$x.= '<td class="tabletopnolink" align="center">'.$eReportCard['Start'].'</td>'."\n";
				$x.= '<td class="tabletopnolink term_break_link" align="center">'.$eReportCard['End'].'</td>'."\n";
			}
			if($eRCTemplateSetting['PersonalCharacteristicSubmissionSchedule'])
			{
				$x.= '<td class="tabletopnolink" align="center">'.$eReportCard['Start'].'</td>'."\n";
				$x.= '<td class="tabletopnolink term_break_link" align="center">'.$eReportCard['End'].'</td>'."\n";
			}
			$x.= '<td>&nbsp;</td>'."\n";
			$x.= '<td>&nbsp;</td>'."\n";
		$x.= '</tr>'."\n";


	$lastClassLevelID = "";
	$nowTs = time();
	$numOfScheduleEntries = count($scheduleEntries);
	if ($numOfScheduleEntries > 0) {
		for($i=0; $i<$numOfScheduleEntries; $i++) {
			$_reportId = $scheduleEntries[$i]['ReportID'];
			$_classLevelId = $scheduleEntries[$i]['ClassLevelID'];
			$_applySpecificMarksheetSubmission = $reportInfoAssoAry[$_reportId]['ApplySpecificMarksheetSubmission'];
						
			$_reportPrefix = '';
			if ($_applySpecificMarksheetSubmission) {
				$_reportPrefix = $linterface->RequiredSymbol();
			}
			
			if ($_classLevelId != $lastClassLevelID) {
				$x .= "<tr class='resubtabletop'><td class='tabletext' width='80'><strong>";
				$x .= $classLevelName[$_classLevelId];
				$x .= "</strong></td>";
				
					// submission & verification
					$x .= 	"<td class='term_break_link'>&nbsp;</td>
							<td>&nbsp;</td><td class='term_break_link'>&nbsp;</td>";
					
					if($accessSettingData[EnableVerificationPeriod]==1){	
						$x .= "<td>&nbsp;</td><td class='term_break_link'>&nbsp;</td>";
					}
					if($eRCTemplateSetting['Management']['Schedule']['PublishReportPeriod']){
						$x .= "<td>&nbsp;</td><td class='term_break_link'>&nbsp;</td>";
					}
					if($eRCTemplateSetting['Management']['Schedule']['ClassTeacherCommentSubmissionPeriod']){
						$x .= "<td>&nbsp;</td><td class='term_break_link'>&nbsp;</td>";
					}
					if($eRCTemplateSetting['PersonalCharacteristicSubmissionSchedule'])	{
						$x .= "<td>&nbsp;</td><td class='term_break_link'>&nbsp;</td>";
					}
				
					// issue date
					$x .= 	"<td>&nbsp;</td>";
					$x.= '<td>&nbsp;</td>'."\n";
					$x .= 	"</tr>";
			}
			
			
			$subStart = ($scheduleEntries[$i]["SubStart"]=='') ? "-" : date("Y-m-d H:i:s",$scheduleEntries[$i]["SubStart"]);
			$subEnd = ($scheduleEntries[$i]["SubEnd"]=='') ? "-" : date("Y-m-d H:i:s",$scheduleEntries[$i]["SubEnd"]);
			
			$comparePeriodSub = $lreportcard->COMPARE_REPORT_PERIOD($scheduleEntries[$i]["ReportID"], "Submission", $scheduleEntries[$i]["SubStart"], $scheduleEntries[$i]["SubEnd"]);
			$subTdClass = ($comparePeriodSub == 1) ? "retablechose" : "";
			
			$verStart = ($scheduleEntries[$i]["VerStart"]=="") ? "-" : date("Y-m-d H:i:s",$scheduleEntries[$i]["VerStart"]);
			$verEnd = ($scheduleEntries[$i]["VerEnd"]=="") ? "-" : date("Y-m-d H:i:s",$scheduleEntries[$i]["VerEnd"]);
			
			$comparePeriodVer = $lreportcard->COMPARE_REPORT_PERIOD($scheduleEntries[$i]["ReportID"], "Verification", $scheduleEntries[$i]["VerStart"], $scheduleEntries[$i]["VerEnd"]);
			$verTdClass = ($comparePeriodVer == 1) ? "retablechose" : "";
			
			$issue = ($scheduleEntries[$i]["Issued"]=="") ? "-" : date("Y-m-d",$scheduleEntries[$i]["Issued"]);

			$editHref = "edit.php?ReportID=".$scheduleEntries[$i]["ReportID"];
			
			$x .= 	"<tr><td align='right' class='tabletext'>
						<a href='$editHref'>
							<img width='20' height='20' border='0' alt='$button_edit' src='".$PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/icon_edit_b.gif' />
						</a>
					</td>
					<td class='tabletext term_break_link'>".$_reportPrefix."<a class='tablelink' href='$editHref'>".str_replace(":_:", "<br>", $scheduleEntries[$i]["ReportTitle"])."</a></td>
					<td class='tabletext $subTdClass' align='center'>$subStart</td>
					<td class='tabletext term_break_link $subTdClass' align='center'>$subEnd</td>";
			
			if($accessSettingData[EnableVerificationPeriod]==1)
			{
				$x .= " <td class='tabletext $verTdClass' align='center'>$verStart</td>
						<td class='tabletext term_break_link $verTdClass' align='center'>$verEnd</td>";
			}
			if($eRCTemplateSetting['Management']['Schedule']['PublishReportPeriod'])
			{
				$PublishStart = ($scheduleEntries[$i]["PublishStart"]=="") ? "-" : date("Y-m-d H:i:s", $scheduleEntries[$i]["PublishStart"]);
				$PublishEnd = ($scheduleEntries[$i]["PublishEnd"]=="") ? "-" : date("Y-m-d H:i:s", $scheduleEntries[$i]["PublishEnd"]);
				
				$comparePeriodPublish = $lreportcard->COMPARE_REPORT_PERIOD($scheduleEntries[$i]["ReportID"], "", $scheduleEntries[$i]["PublishStart"], $scheduleEntries[$i]["PublishEnd"]);
				$PublishTdClass = ($comparePeriodPublish == 1) ? "retablechose" : "";
				
				$x .= "	<td class='tabletext $PublishTdClass' align='center'>$PublishStart</td>
						<td class='tabletext term_break_link $PublishTdClass' align='center'>$PublishEnd</td>";
			}
			if($eRCTemplateSetting['Management']['Schedule']['ClassTeacherCommentSubmissionPeriod'])
			{
				$CTCStart = ($scheduleEntries[$i]["CTCStart"]=="") ? "-" : date("Y-m-d H:i:s",$scheduleEntries[$i]["CTCStart"]);
				$CTCEnd = ($scheduleEntries[$i]["CTCEnd"]=="") ? "-" : date("Y-m-d H:i:s",$scheduleEntries[$i]["CTCEnd"]);
				
				$comparePeriodCTC = $lreportcard->COMPARE_REPORT_PERIOD($scheduleEntries[$i]["ReportID"], "", $scheduleEntries[$i]["CTCStart"], $scheduleEntries[$i]["CTCEnd"]);
				$CTCTdClass = ($comparePeriodCTC == 1) ? "retablechose" : "";
				
				$x .= "	<td class='tabletext $CTCTdClass' align='center'>$CTCStart</td>
						<td class='tabletext term_break_link $CTCTdClass' align='center'>$CTCEnd</td>";
			}
			if($eRCTemplateSetting['PersonalCharacteristicSubmissionSchedule'])
			{
				$PCStart = ($scheduleEntries[$i]["PCStart"]=="") ? "-" : date("Y-m-d H:i:s",$scheduleEntries[$i]["PCStart"]);
				$PCEnd = ($scheduleEntries[$i]["PCEnd"]=="") ? "-" : date("Y-m-d H:i:s",$scheduleEntries[$i]["PCEnd"]);
				
				$comparePeriodPC = $lreportcard->COMPARE_REPORT_PERIOD($scheduleEntries[$i]["ReportID"], "", $scheduleEntries[$i]["PCStart"], $scheduleEntries[$i]["PCEnd"]);
				$PCTdClass = ($comparePeriodPC == 1) ? "retablechose" : "";
				
				$x .= " <td class='tabletext $PCTdClass' align='center'>$PCStart</td>
						<td class='tabletext term_break_link $PCTdClass' align='center'>$PCEnd</td>";
			}
			
			$x .= "<td class='tabletext' align='center'>$issue</td>";
			$x .= "<td class='tabletext' align='center' width='1'><input type='checkbox' name='ReportIDs[]' value='$_reportId' /></td>";			### NOT YET FINISH

			$x .= '</tr>';
			$lastClassLevelID = $_classLevelId;
		}
	} else {
		$x .= "<tr class='tablerow2'><td class='tabletext' height='40' align='center' colspan='10'>";
		$x .= $eReportCard['NoSchedule']."</td></tr>";
	}
	$x .= '</table>';
	echo $x;
?>

<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td colspan="7" height="1" class="dotline">
			<img width="10" height="1" src="<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/10x10.gif"/>
		</td>
	</tr>
	<tr><td>
		<br />
		<span class="tabletextremark"><?=$linterface->RequiredSymbol()?> <?=$eReportCard['ManagementArr']['SchdeuleArr']['ApplySpecificSubmissionPeriod']?></span>
	</td></tr>
</table>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
    
?>