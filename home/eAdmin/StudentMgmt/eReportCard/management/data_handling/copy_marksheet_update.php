<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");


$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();


$yearId = $_POST['YearID'];
$fromReportId = $_POST['FromReportID'];
$fromReportColumnId = $_POST['FromReportColumnID'];
$toReportId = $_POST['ToReportID'];
$toReportColumnId = $_POST['ToReportColumnID'];


$backParam = 'YearID='.$yearId.'&FromReportID='.$fromReportId.'&FromReportColumnID='.$FromReportColumnID.'&ToReportID='.$ToReportID.'&ToReportColumnID='.$ToReportColumnID;


// get all report columns of the report if selected "all report columns"
$fromReportColumnIdAry = array();
if ($fromReportColumnId == '') {
	$fromReportColumnIdAry = Get_Array_By_Key($lreportcard->returnReportTemplateColumnData($fromReportId), 'ReportColumnID');
	$fromReportColumnIdAry[] = -1;	// overall
}
else {
	$fromReportColumnIdAry[] = $fromReportColumnId;
}
$numOfFromReportColumn = count($fromReportColumnIdAry);

$toReportColumnIdAry = array();
if ($toReportColumnId == '') {
	$toReportColumnIdAry = Get_Array_By_Key($lreportcard->returnReportTemplateColumnData($toReportId), 'ReportColumnID');
	$toReportColumnIdAry[] = -1;	// overall
}
else {
	$toReportColumnIdAry[] = $toReportColumnId;
}
$numOfToReportColumn = count($toReportColumnIdAry);

if ($numOfFromReportColumn != $numOfToReportColumn) {
	header('Location: copy_marksheet.php?'.$backParam.'&returnMsgKey=ReportColumnNotMatched');
	die();
}

$success = $lreportcard->Copy_Marksheet($fromReportId, $fromReportColumnIdAry, $toReportId, $toReportColumnIdAry);
intranet_closedb();

$returnMsgKey = ($success)? 'CopySuccess' : 'CopyFailed';
header('Location: copy_marksheet.php?'.$backParam.'&returnMsgKey='.$returnMsgKey);
?>