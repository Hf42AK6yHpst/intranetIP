<?php
// Using: 

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcardcustom();
$lreportcard_ui = new libreportcard_ui();
$linterface = new interface_html();

if ($lreportcard->hasAccessRight()) {
	
	# Interface
	$linterface = new interface_html();
	$CurrentPage = "Management_DataHandling";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();		
	$TAGS_OBJ = $lreportcard_ui->Get_Mgmt_DataHandling_Tab_Array('transfertoWebSAMS');
	$linterface->LAYOUT_START();
		
	# Current Academic Year
	$AcademicYearID = $lreportcard->schoolYearID;
	$yearTermObj = new academic_year($AcademicYearID);
	$AcademicYear = $yearTermObj->Get_Academic_Year_Name();
		
	# ClassLevelID Selection Box
	$FormArr = $lreportcard->GET_ALL_FORMS(1);
	for($i=0; $i<sizeof($FormArr); $i++) {
		$classLevelName[$FormArr[$i]["ClassLevelID"]] = $FormArr[$i]["LevelName"];
	}
	if($ClassLevelID == ''){
		$ClassLevelID = $FormArr[0]['YearID'];
	}
	# Filters - By Form (ClassLevelID)
// 	$FormSelection = $linterface->GET_SELECTION_BOX($FormArr, 'name="ClassLevelID" ', "js_Reload_Selection(this.value)", $ClassLevelID);
	$FormSelection = $lreportcard_ui->Get_Form_Selection('ClassLevelID', $ClassLevelID, 'js_Reload_Selection(this.value)', $noFirst=1);
	$ReportSelection = $lreportcard->Get_Report_Selection($ClassLevelID, '', 'ReportID');
?>

<script language="javascript">
<!--
function js_Go_Export_WebSAMS(ReportID)
{
	window.location = 'export_to_websams_generate.php?ReportID=' + ReportID;
}
function js_Reload_Selection(jsYearID)
{
	$('#ReportSelectionDiv').hide();
	
	$('#ReportSelectionDiv').load(
		"ajax_reload_selection.php", 
		{ 
			RecordType: 'Report',
			YearID: jsYearID,
			ReportID: '',
			SelectionID: 'ReportID',
			ReportType: 'T',
			HideNonGenerated: '1',
			isMainReport: '1'
		},
		function(ReturnData)
		{
			$('#ReportSelectionDiv').show();
			js_Reload_Column();
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
}
function js_Reload_Column(){

	$('#ReportColumnSelectionDiv').hide();

	$('#ReportColumnSelectionDiv').load(
		"ajax_reload_selection.php", 
		{ 
			RecordType: 'ReportColumnSelection',
			ReportID: $('#ReportID').val(),
			SelectionID: 'ColumnID',
			IncludeOverallOption: '0',
			isAll: '0',
			NoFirst: '1'
		},
		function(ReturnData)
		{
			$('#ReportColumnSelectionDiv').show();
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
}
$(document).ready(function(){
	$('#ClassLevelID').change();
});
//-->
</script>
<br />						
										
<!-- Start Main Table //-->
<form name='form1' id='form1' method='POST' action='transfer_to_websams_generate.php'>
<table width="96%" border="0" cellspacing="0" cellpadding="5">
	<tr><td align="center" colspan="2">
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
    			<tr><td align="left" valign="top">
    					<table width="100%" border="0" cellspacing="0" cellpadding="0">
	          				<tr><td align="right">
	          						<table width="100%" border="0" cellspacing="0" cellpadding="0">
	            						<tr><td>
			                					<table class="form_table_v30">
													<tr>
														<td class="field_title"><?=$eReportCard['CurrentAcadermicYear']?></td>
														<td><?=$AcademicYear?></td>
													</tr>
												</table>
										</td></tr>
										<tr><td>
			                					<table class="form_table_v30">
													<tr>
														<td class="field_title"><?=$eReportCard['Form']?></td>
														<td><?=$FormSelection?></td>
													</tr>
												</table>
										</td></tr>
										<tr><td>
			                					<table class="form_table_v30">
													<tr>
														<td class="field_title"><?=$eReportCard['ReportCard']?></td>
														<td>
															<div id="ReportSelectionDiv">
																<?=$ReportSelection?>
															</div>
														</td>
													</tr>
												</table>
										</td></tr>
										<tr><td>
			                					<table class="form_table_v30">
													<tr>
														<td class="field_title"><?= $eReportCard['Term'] ?></td>
														<td>
															<div id="ReportColumnSelectionDiv">
															</div>
														</td>
													</tr>
												</table>
										</td></tr>
										<tr>
											<td colspan='2' class="dotline">
												<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
											</td>
										</tr>
										<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
										<tr'>
											<td colspan='2' align="center">
												<?= $linterface->GET_ACTION_BTN($button_export, "submit", "", "", "id='exportBtn'") ?>
											</td>
										</tr>
            						</table>
            				</td></tr>
        				</table>
          		</td></tr>
    		</table>
    </td></tr>
</table>

</form>
<!-- End Main Table //-->

<br />
<?
} else {
?>
	You have no priviledge to access this page.
<?
}
$linterface->LAYOUT_STOP();
intranet_closedb();
?>