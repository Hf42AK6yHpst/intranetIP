<?php
// Using: Bill

#### This page is for IP25 only
/*
 * Date:  20191101 Bill:  [2019-1030-0914-49066]
 *      - Set Up Main / Competition Subject from settings  ($eRCTemplateSetting['TransferToiPortfolio']['HandleCompetitionSubject'])
 * Date: 2019-03-28 (Bill)  [2019-0326-1045-57206]
 *      - not transfer raw marks to iPortfolio  ($eRCTemplateSetting['TransferToiPortfolio']['AlwaysNotTransferRawMark'])
 *      - always round marks when transfer to iPortfolio
 * Date: 2018-08-06 (Bill)  [2017-0901-1527-45265]
 *      - Updated Term Assessment handling for HKUGA College
 * Date: 2018-07-20 (Bill)
 *      - handle cannot transfer grand average grade    ($eRCTemplateSetting['HandleGradeForExcludeRankingOrderStudents'])
 * Date: 2018-06-19 (Bill)  [2017-0717-1415-46164]
 *      - set grade to 'D' when students passed mark-up exam
 *      - hide competition subject result - year report / input mark below display limit
 * Date: 2018-05-28 (Bill)
 *      - support transfer of conduct from Class Teacher Comment    ($eRCTemplateSetting['ClassTeacherComment_Conduct'])
 *      - support transfer of conduct (overall grade calculation)
 *      - support transfer of promotion status                      ($eRCTemplateSetting['TransferToiPortfolio']['TransferPromotionStatus'])
 * Date: 2018-04-23 (Bill)  [2018-0423-1435-32206]
 * 	    - Fixed: cannot delete old Academic Result for UCCKE - F.6 Term Report
 * Date: 2018-03-09 (Bill)  [2017-1016-1446-12235]
 * 	    - Set Mark = -1 when Mark IS NULL   (for SDAS Usage)
 *          > both Academic Result & Mock Exam
 *      - for TWGHs Mrs Wu York Yu Memorial College only
 *          > Convert "Business, Accounting & Financial Studies" (11N) to "BAFS ACCOUNTING" (12N)
 *      - Fixed: cannot transfer Mock Exam result to correct year in iPortfolio > when previous year transferred Mock Exam result
 * Date: 2018-02-20	(Bill)	[2017-0901-1522-34265]
 * 	    - Insert target Component Overall to Parent Term Assessment / Overall
 *     ($eRCTemplateSetting['Report']['HKUGACDataTransferHandling'] & $eRCTemplateSetting['Report']['DataTransferCompAssessmentAry'])
 * Date: 2017-12-15 (Ivan)
 * 	    - support $eRCTemplateSetting['TransferToiPortfolio']['TransferTermReportAsConsolidatedReportResultFormNumberAry'] for uccke
 * Date: 2017-07-04 (Bill)	[2017-0704-1206-51164]
 * 	    - Academic Result > Delete old Year Report records
 * Date: 2017-06-12 (Bill)	[2017-0228-0958-20066]
 * 	    - Academic Result > Delete selected Term Assessment only
 * Date: 2017-06-06 (Bill)
 * 	    - Use SDAS Method to calculate SD & Mean
 * Date: 2017-05-12	(Bill)	[2017-0228-0958-20066]
 * 	    - Support Multiple Report Column for Academic Result
 */

@SET_TIME_LIMIT(21600);
@ini_set(memory_limit, -1);

$PageRight = "ADMIN";

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/iPortfolioConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/liblog.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
}
else {
	$lreportcard = new libreportcard();
}

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

# Check for Combination of Special Case of Input Mark / Term Mark
$SpecialCaseArrH = $lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
$SpecialCaseArrPF = $lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2();

# Get ReportID
//$ReportID = $_GET['ReportID'];
//$ClassLevelID_filter = $_GET['ClassLevelID'];
//$Semester_filter = $_GET['Semester'];
$ReportID = integerSafe($_POST['ReportID']);
$dataTypeAry = $_POST['dataTypeAry'];
$resultColumnIDAry = $_POST['resultColumnID'];
$resultAssessmentAry = $_POST['Assessment'];
$mockExamReportColumnId = integerSafe($_POST['mockExamReportColumnId']);

# Get current acadermic year (input from admin console one)
//$acadermicYear = $lreportcard->schoolYear;
$academicYearID = $lreportcard->GET_ACTIVE_YEAR_ID();
$thisObjYear = new academic_year($academicYearID);
$acadermicYear = $thisObjYear->YearNameEN;

# Get Report Info
$ReportBasicInfoArr = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$reportTitle 	= str_replace(":_:", "<br>", $ReportBasicInfoArr[ReportTitle]);
$SemID 			= $ReportBasicInfoArr['Semester'];
$ReportType 	= $SemID == "F" ? "W" : "T";
$SemesterNum    = $lreportcard->Get_Semester_Seq_Number($SemID);
$ClassLevelID	= $ReportBasicInfoArr['ClassLevelID'];
$FormNumber		= $lreportcard->GET_FORM_NUMBER($ClassLevelID, $ByWebSAMSCode=1);
$isMainReport 	= $ReportBasicInfoArr['isMainReport'];
$CalSetting		= $lreportcard->LOAD_SETTING("Calculation");
$CalculationMethod = ($ReportType == 'T')? $CalSetting['OrderTerm'] : $CalSetting['OrderFullYear'];

# Get Classes from the report template
$classArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
for($i=0; $i<sizeof($classArr); $i++)
{
	// $studentArr[#ClassID][$i] + ["UserID"], ["WebSAMSRegNo"],...
	$studentArr[$classArr[$i]["ClassID"]] = $lreportcard->GET_STUDENT_BY_CLASS($classArr[$i]["ClassID"]);
}
$formStudentInfoAry = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID);
$numOfFormStudent = count($formStudentInfoAry);

if ($ReportType == "T")
{
	// modified by marcus 20091014 , also modified lib.php, form_class_manage.php to make it work
	$semName = $lreportcard->returnSemesters($SemID, "en");
	$YearTermID = $SemID;
	$YearTermIDSql = "'".$YearTermID."'";
	$isAnnual = 0;
	
	// uccke: customized to transfer form 6 term report result as consolidated report result 
	if (is_array($eRCTemplateSetting['TransferToiPortfolio']['TransferTermReportAsConsolidatedReportResultFormNumberAry']))
	{
		if (in_array($FormNumber, $eRCTemplateSetting['TransferToiPortfolio']['TransferTermReportAsConsolidatedReportResultFormNumberAry']))
		{
			$semName = "";
			$YearTermID = "";
			$YearTermIDSql = 'null';
			$isAnnual = 1;
		}
	}
}
else
{
	$semName = "";
	$YearTermID = "";
	$YearTermIDSql = 'null';
	$isAnnual = 1;
}

$successAry = array();

##################################################################################
######################## Transfer Academic Result [Start] ########################
##################################################################################
if (in_array('academicResult', (array)$dataTypeAry))
{
	// [2017-0704-1206-51164] fixed cannot delete year result 
	//$yearTermCond2 = $ReportType == "T"? "" : " OR YearTermID IS NULL";
	
    // [2018-0423-1435-32206] fixed cannot delete year result - UCCKE F.6 Term Report
    $yearTermCond2 = $isAnnual == 1 ? " OR YearTermID IS NULL" : "";
	
	## Delete Old Records SQL
	$sql_delete_subject_record = " DELETE FROM 
            							{$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
                                   WHERE
            							AcademicYearID = '$academicYearID'
                						AND
            							( YearTermID = '$YearTermID'".$yearTermCond2." )
                						AND
            							IsAnnual = '$isAnnual' ";
	
	$sql_delete_main_record = "    DELETE FROM 
            							{$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD
            					   WHERE
            							AcademicYearID = '$academicYearID'
            						    AND
            							( YearTermID = '$YearTermID'".$yearTermCond2." )
            						    AND
            							IsAnnual = '$isAnnual' ";
	
	## Prepare data to transfer
	# Get all student Score and class form Order
	// $marksArr[StudentID][SubjectID][ColumnID][Mark, Grade...] = value
	$marksArr = $lreportcard->getMarks($ReportID, '', '', 0, 1);
	// $overallMarksArr[StudentID][ColumnID][GrandTotal, GrandAverage, classOrder...] = value
	$overallMarksArr = $lreportcard->getReportResultScore($ReportID, "");

    // [2019-1030-0914-49066] Set Up Main / Competition Subject
    if($eRCTemplateSetting['TransferToiPortfolio']['HandleCompetitionSubject'])
    {
        $lreportcard->Set_Main_Competition_Subject_List($ClassLevelID);
    }
	
	// [2017-0717-1415-46164] Get Make-up Exam Result
	if($eRCTemplateSetting['ReportGeneration']['MarkupExamGrade'] && $ReportType == 'W' && !empty($marksArr) && is_array($marksArr))
	{
	    $relatedStudentIDArr = array_keys($marksArr);
	    if(!empty($relatedStudentIDArr) && is_array($relatedStudentIDArr))
	    {
	        // Get Mark-up Exam Result
            $MarkupResultAry = $lreportcard->GET_EXTRA_SUBJECT_INFO($relatedStudentIDArr, '', $ReportID);
            
            // Get Graduate Exam Result
            $SchoolType	= $lreportcard->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
            $isSecondaryGraduate = $SchoolType == 'S' && $FormNumber == 6;
            if($isSecondaryGraduate)
            {
                $relatedTermReportAry = $lreportcard->Get_Related_TermReport_Of_Consolidated_Report($ReportID, 1);
                $GradReportInfo = end($relatedTermReportAry);
                $GradReportID = $GradReportInfo['ReportID'];
                if($GradReportID > 0) {
                    $GradExamResultAry = $lreportcard->getMarks($GradReportID, '', $cons=' AND b.DisplayOrder = 1 ', 1);
                }
            }
	    }
	}
	
	# Get Subject Grading Scheme Info
	// $SchemeInfoArr[SubjectID]["SchemeID","ScaleInput","ScaleDisplay"] = value
	$SchemeInfoArr = $lreportcard->Get_Form_Grading_Scheme($ClassLevelID, $ReportID);
	
	# Get Subject Code Info
	$subjectCodeArr = array();
	$subjectComponentCodeArr = array();
	foreach ($SchemeInfoArr as $subjectID => $subjectInfoArr)
	{
		$thisSubjectID = $subjectID;
		$thisSubjectCode = $lreportcard->GET_SUBJECT_NAME($thisSubjectID, 1);
		
		// separate english and chinese
		$subjectCodeArrTemp = explode("&nbsp;", $thisSubjectCode);
		$subjectCodeEn = $subjectCodeArrTemp[0];
		
		// separate component and parent subject code
		$subjectParentCodeArr = explode(" - ", $subjectCodeEn);
		if (count($subjectParentCodeArr) == 2)
		{
			// component subject
			$subjectCodeArr[$thisSubjectID] = $subjectParentCodeArr[0];
			$subjectComponentCodeArr[$thisSubjectID] = $subjectParentCodeArr[1];
		}
		else
		{
			// main subject
			$subjectCodeArr[$thisSubjectID] = $subjectCodeEn;
			$subjectComponentCodeArr[$thisSubjectID] = "";
		}
	}
	$subjectIDCodeArr = $lreportcard->GET_SUBJECTS_CODEID_MAP($withComponent=1, $mapByCode=0);
	$subjectCodeIDArr = $lreportcard->GET_SUBJECTS_CODEID_MAP($withComponent=1, $mapByCode=1);
	
	# Build Data SQL
	$valuesSubjectArr = array();
	$valuesMainArr = array();
	
	# [2017-0228-0958-20066] Build Term Assessment Condition
	$TermAssessmentAry = array();
	$HaveOverallAssessment = false;
	foreach((array)$resultColumnIDAry as $thisReportColumnID)
	{
		if(empty($resultAssessmentAry[$thisReportColumnID])) {
			$HaveOverallAssessment = true;
		}
// 		else if($eRCTemplateSetting['Report']['HKUGACDataTransferHandling']) {
// 		    $TermAssessmentAry[] = "'T".$SemesterNum.$resultAssessmentAry[$thisReportColumnID]."'";
// 		}
		else {
			$TermAssessmentAry[] = "'".$resultAssessmentAry[$thisReportColumnID]."'";
		}
	}
    
	$assessment_cond = "";
	if(!empty($TermAssessmentAry)) {
		$assessment_cond = " TermAssessment IN (".implode(', ', $TermAssessmentAry).") ";
	}
	if($HaveOverallAssessment) {
		$assessment_cond .= $assessment_cond==""? "" : " OR ";
		$assessment_cond .= " TermAssessment IS NULL ";
	}
	if($assessment_cond != "") {
		$assessment_cond = " AND ( $assessment_cond ) ";
	}
	
	// Loop 1: Class
	$numOfClass = count($classArr);
	for($i=0; $i<$numOfClass; $i++)
	{
		// construct the $studentIDArr
		$thisClassID = $classArr[$i]["ClassID"];
		//$thisClassName = Get_Lang_Selection($classArr[$i]["ClassTitleB5"],$classArr[$i]["ClassTitleEN"]);
		$thisClassName = $classArr[$i]["ClassTitleEN"];
		
		// SKIP this class if no students
		$studentIDArr = array();
		$numOfStudentInClass = count($studentArr[$thisClassID]);
		if ($numOfStudentInClass == 0) continue;
		for($j=0; $j<$numOfStudentInClass; $j++) {
			$studentIDArr[] = $studentArr[$thisClassID][$j]["UserID"];
		}
		
		# Delete old data first
		$thisStudentList = implode(', ', $studentIDArr);
		$this_sql_delete_main_record = $sql_delete_main_record." AND UserID IN ($thisStudentList) ".$assessment_cond;
		$successAry['academicResult']['Delete']['Main'][$thisClassID] = $lreportcard->db_db_query($this_sql_delete_main_record);
		
		$this_sql_delete_subject_record = $sql_delete_subject_record." AND UserID IN ($thisStudentList) ".$assessment_cond;
		$successAry['academicResult']['Delete']['Subject'][$thisClassID] = $lreportcard->db_db_query($this_sql_delete_subject_record);
		
		// Loop 2: Class Student
		for($j=0; $j<$numOfStudentInClass; $j++)
		{
			$thisStudentID = $studentArr[$thisClassID][$j]["UserID"];
			$thisClassNumber = $studentArr[$thisClassID][$j]["ClassNumber"];
			$thisWebSAMS = $studentArr[$thisClassID][$j]["WebSAMSRegNo"];
			
			// Loop 3: Report Column
			foreach((array)$resultColumnIDAry as $thisReportColumnID)
			{
				$thisTermAssessment = empty($resultAssessmentAry[$thisReportColumnID])? "NULL" :  "'".$resultAssessmentAry[$thisReportColumnID]."'";
				$thisTempTermAssessment = $thisTermAssessment;
				$thisTempReportColumnID = $thisReportColumnID;
				
				// Loop 4: Subject
				// $marksArr[StudentID][SubjectID][ColumnID][Mark, Grade...] = value
				foreach ($SchemeInfoArr as $subjectID => $subjectInfoArr)
				{
				    $thisTermAssessment = $thisTempTermAssessment;
				    $thisReportColumnID = $thisTempReportColumnID;
				    
					$thisSubjectCode = $subjectCodeArr[$subjectID];
					$thisSubjectComponentCode = $subjectComponentCodeArr[$subjectID];
					$thisSubjectComponentID = 0;
					
					if(!empty($thisSubjectComponentCode)) 
					{
						$thisSubjectID = $lreportcard->GET_PARENT_SUBJECT_ID($subjectID);
						$thisSubjectComponentID = $subjectID;
						
						/*
						 * [2017-0901-1522-34265] Assessment Column - Component > SKIP
						 */
// 						if($eRCTemplateSetting['Report']['HKUGACDataTransferHandling'] && $thisReportColumnID != "0")
// 						{
//                             continue;
// 						}
					}
					else
					{
					    $thisSubjectID = $subjectID;
					    
					    /*
					     * [2017-0901-1522-34265] Assessment Column Result
					     * - T1A1 & T2A1 : Component - Continuous Assessment (Z998)
					     * - T1A2 & T2A2 : Component - Examination (Z997)
					     * - Overall     : Component - Year Grade (Z999)
					     */
					    if($eRCTemplateSetting['Report']['HKUGACDataTransferHandling'])
					    {
					        $targetAssessmentType = empty($resultAssessmentAry[$thisReportColumnID])? "0" : $resultAssessmentAry[$thisReportColumnID];
					        if($targetAssessmentType != '0' && $SemesterNum > 0) {
					            $targetAssessmentType = str_replace('T'.$SemesterNum, '', $targetAssessmentType);
					        }
					        
					        $targetParentCode = $subjectIDCodeArr[$thisSubjectID]['CODEID'];
					        $targetCompCode = $eRCTemplateSetting['Report']['DataTransferCompAssessmentAry'][trim($targetAssessmentType)];
					        $targetSubjectCode = $targetParentCode.'_'.$targetCompCode;
					        $subjectID = $subjectCodeIDArr[$targetSubjectCode];
					        
					        // Case : without target Component
					        if($subjectID == "") {
// 					            // Assessment Column > SKIP
// 					            if($thisReportColumnID != "0") {
// 					                continue;
// 					            }
// 					            // Overall Column > Use Main Subject
// 					            else {
					                $subjectID = $thisSubjectID;
// 					            }
					        } else {
					            $thisReportColumnID = "0";
					        }
					        //$thisSubjectID = $subjectID;     // Commented: prevent Parent Assessment result with Subject Component ID 
					        
// 					        $thisTermAssessment = $thisTermAssessment == "NULL" ? "NULL" : "'T".$SemesterNum.$resultAssessmentAry[$thisReportColumnID]."'";
// 					        $thisReportColumnID = "0";
					    }
					}
				    
					$thisSchemeID = $SchemeInfoArr[$subjectID]['SchemeID'];
					$thisScaleInput = $SchemeInfoArr[$subjectID]['ScaleInput'];
					$thisScaleDisplay = $SchemeInfoArr[$subjectID]['ScaleDisplay'];
					
					# Mark
					$thisReportMark = $marksArr[$thisStudentID][$subjectID][$thisReportColumnID]['Mark'];
					$thisReportRawMark = $marksArr[$thisStudentID][$subjectID][$thisReportColumnID]['RawMark'];
					if($eRCTemplateSetting['TransferToiPortfolio']['AlwaysNotTransferRawMark']) {
					    $thisReportRawMark = $marksArr[$thisStudentID][$subjectID][$thisReportColumnID]['Mark'];
					}
					$thisReportGrade = $marksArr[$thisStudentID][$subjectID][$thisReportColumnID]['Grade'];			
					
					# Order
					$thisOrderMeritClass = $marksArr[$thisStudentID][$subjectID][$thisReportColumnID]['OrderMeritClass'];
					$thisClassNoOfStudent = $marksArr[$thisStudentID][$subjectID][$thisReportColumnID]['ClassNoOfStudent'];
					$thisOrderMeritForm = $marksArr[$thisStudentID][$subjectID][$thisReportColumnID]['OrderMeritForm'];
					$thisFormNoOfStudent = $marksArr[$thisStudentID][$subjectID][$thisReportColumnID]['FormNoOfStudent'];
					$thisOrderMeritSubjectGroup = $marksArr[$thisStudentID][$subjectID][$thisReportColumnID]['OrderMeritSubjectGroup'];
					$thisSubjectGroupNoOfStudent = $marksArr[$thisStudentID][$subjectID][$thisReportColumnID]['SubjectGroupNoOfStudent'];
				    
					# Score Info
					$thisMark = '';
					$thisGrade = '';
//					if(!empty($thisSubjectComponentCode) && $CalculationMethod==2 && !$eRCTemplateSetting['PassComponentMarks']) {
//						continue;
//					}
//					else if ($thisScaleInput=="M" && $thisScaleDisplay=="M") {
//						if ($eRCTemplateSetting['TransferToiPortfolio']['TransferGPA']) {
//							$thisMark = "null";
//							$thisGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($thisSchemeID, $thisReportMark, $ReportID, $thisStudentID, $subjectID, $ClassLevelID, $ReportColumnID=0, $thisReportGrade, $returnGradePoint=1);
//						}
//						else {
//							$thisMark = $thisReportMark;
//							$thisGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($thisSchemeID, $thisMark, $ReportID, $thisStudentID, $subjectID, $ClassLevelID, $ReportColumnID=0);
//						}
//					}
//					else if ($thisScaleInput=="M" && $thisScaleDisplay=="G") {
//						if ($eRCTemplateSetting['TransferToiPortfolio']['TransferGPA']) {
//							$thisMark = "null";
//							$thisGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($thisSchemeID, $thisReportMark, $ReportID, $thisStudentID, $subjectID, $ClassLevelID, $ReportColumnID=0, $thisReportGrade, $returnGradePoint=1);
//						}
//						else {
//							$thisMark = $thisReportRawMark;
//							$thisGrade = $thisReportGrade;
//						}
//					}
//					else if ($thisScaleInput=="G" && $thisScaleDisplay=="G") {
//						$thisMark = "null";
//						$thisGrade = $thisReportGrade;
//						
//						$thisOrderMeritClass = "";
//						$thisClassNoOfStudent = "";
//						$thisOrderMeritForm = "";
//						$thisFormNoOfStudent = "";
//					}
//					
//					
//					# ignore special characters
//					if ($lreportcard->Check_If_Grade_Is_SpecialCase($thisGrade)) {
//						$thisGrade = "";
//					}
					
					if(!empty($thisSubjectComponentCode) && $CalculationMethod==2 && !$eRCTemplateSetting['PassComponentMarks'])
					{
						continue;
					}
					else
					{
						if ($lreportcard->Check_If_Grade_Is_SpecialCase($thisReportGrade))
						{
							$thisMark = "null";
							if ($eRCTemplateSetting['TransferToiPortfolio']['TransferSpecialGrade']) {
								$thisGrade = $lreportcard->GET_MARKSHEET_SPECIAL_CASE_SET1_STRING($thisReportGrade);
							}
							else {
								$thisGrade = "";
							}
						}
						else if ($thisScaleInput=="M" && $thisScaleDisplay=="M")
						{
							if ($eRCTemplateSetting['TransferToiPortfolio']['TransferGPA']) {
								$thisMark = "null";
								$thisGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($thisSchemeID, $thisReportMark, $ReportID, $thisStudentID, $subjectID, $ClassLevelID, $ReportColumnID=$thisReportColumnID, $thisReportGrade, $returnGradePoint=1);
							}
							else {
							    $thisMark = $thisReportMark;
							    if($eRCTemplateSetting['HandleGradeForExcludeRankingOrderStudents']){
							        $thisGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($thisSchemeID, $thisMark, $ReportID, $thisStudentID, $subjectID, $ClassLevelID, $ReportColumnID=$thisReportColumnID, '', 0, true);
							    }
							    else{
							        $thisGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($thisSchemeID, $thisMark, $ReportID, $thisStudentID, $subjectID, $ClassLevelID, $ReportColumnID=$thisReportColumnID);
							    }
							    
							    // [2019-0326-1045-57206] round subject marks before transfer
							    if($eRCTemplateSetting['TransferToiPortfolio']['RoundSubjectMarksBeforeTransfer']) {
							        $roundMarkType = $thisTermAssessment == "NULL"? 'SubjectTotal' : 'SubjectScore';
							        $thisMark = $lreportcard->ROUND_MARK($thisMark, $roundMarkType);
							    }
							}
						}
						else if ($thisScaleInput=="M" && $thisScaleDisplay=="G")
						{
							if ($eRCTemplateSetting['TransferToiPortfolio']['TransferGPA']) {
								$thisMark = "null";
								$thisGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($thisSchemeID, $thisReportMark, $ReportID, $thisStudentID, $subjectID, $ClassLevelID, $ReportColumnID=0, $thisReportGrade, $returnGradePoint=1);
							}
							else {
								$thisMark = $thisReportRawMark;
								$thisGrade = $thisReportGrade;
							}
							
							// [2019-0326-1045-57206] round subject marks before transfer
							if($eRCTemplateSetting['TransferToiPortfolio']['RoundSubjectMarksBeforeTransfer']) {
							    $roundMarkType = $thisTermAssessment == "NULL"? 'SubjectTotal' : 'SubjectScore';
							    $thisMark = $lreportcard->ROUND_MARK($thisMark, $roundMarkType);
							}
						}
						else if ($thisScaleInput=="G" && $thisScaleDisplay=="G")
						{
							$thisMark = "null";
							$thisGrade = $thisReportGrade;
							
							$thisOrderMeritClass = "";
							$thisClassNoOfStudent = "";
							$thisOrderMeritForm = "";
							$thisFormNoOfStudent = "";
						}
						
						// [2017-0717-1415-46164] Handle mark-up exam - set grade to 'D'
						if($eRCTemplateSetting['ReportGeneration']['MarkupExamGrade'] && $ReportType == 'W' && $thisTempTermAssessment == 'NULL' &&
						      !empty($MarkupResultAry) && is_array($MarkupResultAry) && ($thisGrade=='F' || $thisGrade=='+'))
						{
						    $MarkupExamScoreInfo = $MarkupResultAry[$thisStudentID][$subjectID];
						    if(!empty($MarkupExamScoreInfo))
						    {
						        // Set Grade to 'D'
						        $MarkupExamScore = $MarkupExamScoreInfo['Info'];
						        if(!empty($MarkupExamScore) && $MarkupExamScore >= 60)
						        {
						            $thisGrade = 'D';
						        }
						    }
						}
						
						// [2017-0717-1415-46164] Handle graduate exam - set grade to 'D'
						if($eRCTemplateSetting['ReportGeneration']['MarkupExamGrade'] && $ReportType == 'W' && $thisTempTermAssessment == 'NULL' &&
						    $isSecondaryGraduate && !empty($GradExamResultAry) && is_array($GradExamResultAry) && ($thisGrade=='F' || $thisGrade=='+'))
						{
						    $GradExamScoreInfo = $GradExamResultAry[$thisStudentID][$subjectID];
						    if(!empty($GradExamScoreInfo))
						    {
    						    $GradExamScoreInfo = reset($GradExamScoreInfo);
    						    $GradExamScore = $GradExamScoreInfo['Mark'];
    						    if(!empty($GradExamScore) && $GradExamScore >= 60)
    						    {
    						        $thisGrade = 'D';
    						    }
						    }
						}
						
						// [2017-0717-1415-46164] Handle competition subject - not transfer subject result
						if($eRCTemplateSetting['TransferToiPortfolio']['HandleCompetitionSubject'])
						{
    						$thisSubjectIDCode = $subjectIDCodeArr[$subjectID]['CODEID'];
    						if(!empty($lreportcard->CompletitionSubjectList) && !empty($lreportcard->CompletitionSubjectDisplayLimit) && 
    						    is_array($lreportcard->CompletitionSubjectList) && is_array($lreportcard->CompletitionSubjectDisplayLimit) && 
    						    in_array($thisSubjectIDCode, $lreportcard->CompletitionSubjectList) && isset($lreportcard->CompletitionSubjectDisplayLimit[$thisSubjectIDCode]) && 
    						    $lreportcard->CompletitionSubjectDisplayLimit[$thisSubjectIDCode] > 0)
    						{
    						    $thisSubjectDisplayLimit = $lreportcard->CompletitionSubjectDisplayLimit[$thisSubjectIDCode];
    						    if($ReportType == 'W' || $lreportcard->Check_If_Grade_Is_SpecialCase($thisReportGrade) || 
    						        ($thisScaleInput == 'M' && $thisReportRawMark < $thisSubjectDisplayLimit))
    						    {
    						        $thisMark = 'null';
    						        $thisGrade = '';
    						        
    						        $thisOrderMeritClass = "";
    						        $thisClassNoOfStudent = "";
    						        $thisOrderMeritForm = "";
    						        $thisFormNoOfStudent = "";
    						    }
    						}
						}
					}
					
					// [2017-1016-1446-12235] for SDAS Usage - convert mark to -1 if NULL
					if($thisMark == 'null')
					{
					    $thisMark = -1;
					}
					$thisMark = ($thisMark == 'null')? $thisMark : "'".$thisMark."'";
					
					// [2017-1016-1446-12235] convert 11N to 12N (TWGHs Mrs Wu York Yu Memorial College only)
					if($ReportCardCustomSchoolName == "twgh_wu_york_yu_college" && $thisSubjectID == 8)
					{
					    $thisSubjectID = 79;
					}
					
					$thisSubjectComponentCodeSql = ($thisSubjectComponentCode != '')? "'".$lreportcard->Get_Safe_Sql_Query($thisSubjectComponentCode)."'" : 'null';
					$thisSubjectComponentIDSql = ($thisSubjectComponentID != '')? "'".$thisSubjectComponentID."'" : 'null';
					$valuesSubjectArr[] = "('$thisStudentID', '$thisWebSAMS', '$acadermicYear', '$academicYearID', '$semName', $YearTermIDSql, $thisTermAssessment, '$isAnnual',
											'$thisClassName', '$thisClassID', '$thisClassNumber', '$thisSubjectCode', '$thisSubjectID', $thisSubjectComponentCodeSql,
											$thisSubjectComponentIDSql, $thisMark, '$thisGrade', '$thisOrderMeritClass', '$thisClassNoOfStudent',
											'$thisOrderMeritForm', '$thisFormNoOfStudent', '$thisOrderMeritSubjectGroup', '$thisSubjectGroupNoOfStudent',
											now(), now(), '".$ipf_cfg["DB_ASSESSMENT_STUDENT_SUBJECT_RECORD_ComeFrom"]["erc"]."')";
				}
				
				/*
				 * [2017-0901-1522-34265] Assessment Column - Overall Info > SKIP
				 */
// 				if($eRCTemplateSetting['Report']['HKUGACDataTransferHandling'] && $thisTempReportColumnID != "0")
// 				{
// 				    continue;
// 				}
				
				# Overall Info
				// $overallMarksArr[StudentID][ColumnID][GrandTotal, GrandAverage, classOrder...] = value
				if ($eRCTemplateSetting['Calculation']['ActualAverage']) {
					$thisGrandAverage = $overallMarksArr[$thisStudentID][$thisReportColumnID]['ActualAverage'];
				}
				else if ($eRCTemplateSetting['TransferToiPortfolio']['TransferGPA']) {
					$thisGrandAverage = $overallMarksArr[$thisStudentID][$thisReportColumnID]['GPA'];
				}
				else {
					$thisGrandAverage = $overallMarksArr[$thisStudentID][$thisReportColumnID]['GrandAverage'];
				}
				$thisGrandOrderMeritClass = $overallMarksArr[$thisStudentID][$thisReportColumnID]['OrderMeritClass'];
				$thisGrandClassNoOfStudent = $overallMarksArr[$thisStudentID][$thisReportColumnID]['ClassNoOfStudent'];
				$thisGrandOrderMeritForm = $overallMarksArr[$thisStudentID][$thisReportColumnID]['OrderMeritForm'];
				$thisGrandFormNoOfStudent = $overallMarksArr[$thisStudentID][$thisReportColumnID]['FormNoOfStudent'];
				
				$GrandSchemeInfoArr = $lreportcard->Get_Grand_Mark_Grading_Scheme($ClassLevelID,$ReportID);
				$GrandAverageSchemeData = $GrandSchemeInfoArr['-1'];
				$GrandAverageSchemeID = $GrandAverageSchemeData['SchemeID'];
				if($eRCTemplateSetting['HandleGradeForExcludeRankingOrderStudents']){
				    //$thisGrandAverageGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandAverageSchemeID, $thisGrandAverage, '', '', '', '', '', '', 0, true);
				    $thisGrandAverageGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandAverageSchemeID, $thisGrandAverage, $ReportID, $thisStudentID, -1, $ClassLevelID, $ReportColumnID=$thisReportColumnID);
				    if(is_numeric($thisGrandAverage) && $thisGrandAverageGrade == $thisGrandAverage){
				        $thisGrandAverageGrade = '--';
				    }
				}
				else{
				    $thisGrandAverageGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandAverageSchemeID, $thisGrandAverage);
				}
				
				$valuesMainArr[] = "('$thisStudentID', '$thisWebSAMS', '$acadermicYear', '$academicYearID', '$semName', $YearTermIDSql, $thisTermAssessment, '$isAnnual',
									 '$thisClassName', '$thisClassID', '$thisClassNumber', '$thisGrandAverage', '$thisGrandAverageGrade', 
									 '$thisGrandOrderMeritClass', '$thisGrandClassNoOfStudent',
									 '$thisGrandOrderMeritForm', '$thisGrandFormNoOfStudent',
									 now(), now(), '".$ipf_cfg["DB_ASSESSMENT_STUDENT_MAIN_RECORD_ComeFrom"]["erc"]."')";
			}
		}
	}
	
	# Insert Student Subject Scores
	$numOfSubjectData = count($valuesSubjectArr);
	if ($numOfSubjectData > 0)
	{
		$numOfDataPerChunck = 1000;
		$numOfChunk = ceil($numOfSubjectData / $numOfDataPerChunck);
		
		$valuesSubjectSplitedArr = array_chunk($valuesSubjectArr, $numOfChunk);
		foreach((array)$valuesSubjectSplitedArr as $valuesSubjectArr)
		{
			$valuesSubject = implode(", ", $valuesSubjectArr);
			$sql_subject = "INSERT INTO {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD
										(UserID, WebSAMSRegNo, Year, AcademicYearID, Semester, YearTermID, TermAssessment, IsAnnual, ClassName, YearClassID, ClassNumber,
										SubjectCode, SubjectID, SubjectComponentCode, SubjectComponentID, Score, Grade, 
										OrderMeritClass, OrderMeritClassTotal, OrderMeritForm, OrderMeritFormTotal, OrderMeritSubjGroup, OrderMeritSubjGroupTotal,
										InputDate, ModifiedDate, ComeFrom)
							VALUES
										$valuesSubject ";
			$success = $lreportcard->db_db_query($sql_subject);
			$successAry['academicResult']['Insert']['Subject'][] = $success;
		}
	}
	
	# Insert Student Overall Scores
	$valuesOverall = implode(", ", $valuesMainArr);
	$sql_overall = "INSERT INTO {$eclass_db}.ASSESSMENT_STUDENT_MAIN_RECORD
								(UserID, WebSAMSRegNo, Year, AcademicYearID, Semester, YearTermID, TermAssessment, IsAnnual, ClassName, YearClassID, ClassNumber,
								Score, Grade, OrderMeritClass, OrderMeritClassTotal, OrderMeritForm, OrderMeritFormTotal,
								InputDate, ModifiedDate, ComeFrom)
					VALUES
								$valuesOverall ";
	$successAry['academicResult']['Insert']['Main'][] = $lreportcard->db_db_query($sql_overall);
	
//	include_once($PATH_WRT_ROOT."includes/libportfolio.php");
//	$lpf = new libportfolio();
//	$lpf->ComputeSDMean($ClassLevelID, $academicYearID, $YearTermID);
    
	include_once($PATH_WRT_ROOT."includes/cust/student_data_analysis_system/libSDAS.php");
	$lpf = new libSDAS();
	$lpf->ComputeSDMean($ClassLevelID, $academicYearID, $YearTermID);
}
##################################################################################
######################### Transfer Academic Result [End] #########################
##################################################################################


##################################################################################
########################### Transfer Mock Exam [Start] ###########################
##################################################################################
if (in_array('mockExamResult', (array)$dataTypeAry))
{
	include_once($PATH_WRT_ROOT."includes/libpf-exam.php");
	$examId = EXAMID_MOCK;
	$libpf_exam = new libpf_exam($examId);
	
	# Get Subject Grading Scheme Info
	// $SchemeInfoArr[SubjectID]["SchemeID","ScaleInput","ScaleDisplay"] = value
	$SchemeInfoArr = $lreportcard->Get_Form_Grading_Scheme($ClassLevelID, $ReportID);
	
	# Get marks from eRC
	// $marksArr[StudentID][SubjectID][ColumnID][Mark, Grade...] = value
	$marksArr = $lreportcard->getMarks($ReportID);
	
	$insertArr = array();
	$updateArr = array();
	
	// Loop 1: Class
	$numOfClass = count($classArr);
	for($i=0; $i<$numOfClass; $i++)
	{
		// Construct the $studentIDArr
		$thisClassID = $classArr[$i]["ClassID"];
		$thisClassName = $classArr[$i]["ClassTitleEN"];
		
		// SKIP this class if no students
		$studentIDArr = array();
		$numOfStudentInClass = count($studentArr[$thisClassID]);
		if ($numOfStudentInClass == 0) continue;
		for($j=0; $j<$numOfStudentInClass; $j++) {
			$studentIDArr[] = $studentArr[$thisClassID][$j]["UserID"];
		}
		
		/*
		 * [2017-1016-1446-12235] use $academicYearID instead of $acadermicYearID
		 *    > can delete & update Mock Exam data of active year in iPortfolio
		 */
		# Delete old data first
		$libpf_exam->deleteExamData($examId, $academicYearID, $studentIDArr);
		
		# Get existing mock exam from SDAS
		$mockExamAssoAry = BuildMultiKeyAssoc($libpf_exam->getExamData($academicYearID, $examId, '', $studentIDArr), array('StudentID','SubjectID'), array('RecordID'), $SingleValue=1);
		
		// Loop 2: Class Student
		for($j=0; $j<$numOfStudentInClass; $j++)
		{
			$thisStudentID = $studentArr[$thisClassID][$j]["UserID"];
			$thisClassNumber = $studentArr[$thisClassID][$j]["ClassNumber"];
			$thisWebSAMS = $studentArr[$thisClassID][$j]["WebSAMSRegNo"];
			
			// Loop 3: Subject
			// $marksArr[StudentID][SubjectID][ColumnID][Mark, Grade...] = value
			foreach ($SchemeInfoArr as $subjectID => $subjectInfoArr)
			{
				$thisSubjectCode = $subjectCodeArr[$subjectID];
				$thisSubjectComponentCode = $subjectComponentCodeArr[$subjectID];
				$thisSubjectComponentID = 0;
				
				if(!empty($thisSubjectComponentCode)) 
				{
					$thisSubjectID = $lreportcard->GET_PARENT_SUBJECT_ID($subjectID);
					$thisSubjectComponentID = $subjectID;
				}
				else
				{
					$thisSubjectID = $subjectID;
			    }
                
				$thisSchemeID = $SchemeInfoArr[$subjectID]['SchemeID'];
				$thisScaleInput = $SchemeInfoArr[$subjectID]['ScaleInput'];
				$thisScaleDisplay = $SchemeInfoArr[$subjectID]['ScaleDisplay'];
				
				# Mark
				$thisReportMark = $marksArr[$thisStudentID][$subjectID][$mockExamReportColumnId]['Mark'];
				$thisReportRawMark = $marksArr[$thisStudentID][$subjectID][$mockExamReportColumnId]['RawMark'];
				if($eRCTemplateSetting['TransferToiPortfolio']['AlwaysNotTransferRawMark']) {
				    $thisReportRawMark = $marksArr[$thisStudentID][$subjectID][$mockExamReportColumnId]['Mark'];
				}
				$thisReportGrade = $marksArr[$thisStudentID][$subjectID][$mockExamReportColumnId]['Grade'];
				
				# Score Info
				$thisMark = '';
				$thisGrade = '';
				if(!empty($thisSubjectComponentCode) && $CalculationMethod==2 && !$eRCTemplateSetting['PassComponentMarks'])
				{
					continue;
				}
				else
				{
					if ($lreportcard->Check_If_Grade_Is_SpecialCase($thisReportGrade))
					{
						continue;
					}
					else if ($thisScaleInput=="M" && $thisScaleDisplay=="M")
					{
//						if ($eRCTemplateSetting['TransferToiPortfolio']['TransferGPA']) {
//							$thisMark = "";
//							$thisGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($thisSchemeID, $thisReportMark, $ReportID, $thisStudentID, $subjectID, $ClassLevelID, $ReportColumnID=0, $thisReportGrade, $returnGradePoint=1);
//						} else {
						$thisMark = $thisReportMark;
						$thisGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($thisSchemeID, $thisMark, $ReportID, $thisStudentID, $subjectID, $ClassLevelID, $ReportColumnID=0);
						
						// [2019-0326-1045-57206] round subject marks before transfer
						if($eRCTemplateSetting['TransferToiPortfolio']['RoundSubjectMarksBeforeTransfer']) {
						    $roundMarkType = $mockExamReportColumnId == "0"? 'SubjectTotal' : 'SubjectScore';
						    $thisMark = $lreportcard->ROUND_MARK($thisMark, $roundMarkType);
						}
					}
					else if ($thisScaleInput=="M" && $thisScaleDisplay=="G")
					{
//						if ($eRCTemplateSetting['TransferToiPortfolio']['TransferGPA']) {
//							$thisMark = "";
//							$thisGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($thisSchemeID, $thisReportMark, $ReportID, $thisStudentID, $subjectID, $ClassLevelID, $ReportColumnID=0, $thisReportGrade, $returnGradePoint=1);
//						} else {
						$thisMark = $thisReportRawMark;
						$thisGrade = $thisReportGrade;
						
						// [2019-0326-1045-57206] round subject marks before transfer
						if($eRCTemplateSetting['TransferToiPortfolio']['RoundSubjectMarksBeforeTransfer']) {
						    $roundMarkType = $mockExamReportColumnId == "0"? 'SubjectTotal' : 'SubjectScore';
						    $thisMark = $lreportcard->ROUND_MARK($thisMark, $roundMarkType);
						}
					}
					else if ($thisScaleInput=="G" && $thisScaleDisplay=="G")
					{
					    $thisMark = '';
						$thisGrade = $thisReportGrade;
					}
				}
				
				// [2017-1016-1446-12235] for SDAS Usage - convert mark to -1 if NULL
				if($thisMark === '')
				{
				    $thisMark = -1;
				}
				
				// [2017-1016-1446-12235] convert 11N to 12N (TWGHs Mrs Wu York Yu Memorial College only)
				if($ReportCardCustomSchoolName == "twgh_wu_york_yu_college" && $subjectID == 8)
				{
				    $subjectID = 79;
				}
				
				// Store Mock Exam Result
				$___mockExamRecordId = $mockExamAssoAry[$thisStudentID][$subjectID];
				if ($___mockExamRecordId > 0)
				{
					// Update
					$updateArr[$___mockExamRecordId]['Grade'] = $thisGrade;
					$updateArr[$___mockExamRecordId]['Score'] = $thisMark;
				}
				else
				{
					// Insert
					$insertArr[] = "( '$academicYearID', '$examId', '$subjectID', '$thisStudentID', '$thisMark', '$thisGrade', now(), '".$_SESSION['UserID']."', now(),'".$_SESSION['UserID']."' )";
				}
			}
		}
	}
	
	if(count($updateArr) > 0)
	{
		$successAry['mockExamResult']['Update'] = $libpf_exam->updateExamData($updateArr);		
	}
	if(count($insertArr) > 0)
	{
		$successAry['mockExamResult']['Insert'] = $libpf_exam->insertExamData($insertArr);
	}	
}
##################################################################################
############################ Transfer Mock Exam [End] ############################
##################################################################################


##################################################################################
##################### Transfer Class Teacher Comment [Start] #####################
##################################################################################
if (in_array('classTeacherComment', (array)$dataTypeAry) && ($isMainReport || $ReportType == 'W'))
{
	$classTeacherCommentAssoAry = $lreportcard->returnSubjectTeacherCommentByBatch($ReportID, '', 0);
	
	$sql = "Select
					RecordID, UserID
			From
					{$eclass_db}.CONDUCT_STUDENT
			WHERE
					AcademicYearID = '$academicYearID'
					AND	YearTermID = '$YearTermID'
					AND	IsAnnual = '$isAnnual'
					AND ComeFrom = '".$ipf_cfg["DB_CONDUCT_STUDENT_ComeFrom"]["erc"]."'
					AND UserID In ('".implode("','", (array)Get_Array_By_Key($formStudentInfoAry, 'UserID'))."')
			";
	$ipfCommentAssoAry = BuildMultiKeyAssoc($lreportcard->returnResultSet($sql), 'UserID', 'RecordID', $SingleValue=true);
	
	$CommentSubjectID = 0;
	$insertAry = array();
	for ($i=0; $i<$numOfFormStudent; $i++) {
		$_studentId = $formStudentInfoAry[$i]['UserID'];
		//$_className = Get_Lang_Selection($formStudentInfoAry[$i]['ClassTitleCh'], $formStudentInfoAry[$i]['ClassTitleEn']);
		$_className = $formStudentInfoAry[$i]['ClassTitleEn'];
		$_classNumber = $formStudentInfoAry[$i]['ClassNumber'];
		$_comment = $classTeacherCommentAssoAry[$CommentSubjectID][$_studentId]['Comment'];
		
		$_className = $lreportcard->Get_Safe_Sql_Query($_className);
		$_comment = $lreportcard->Get_Safe_Sql_Query($_comment);
		
		$_recordId = $ipfCommentAssoAry[$_studentId];
		
		if ($_recordId == '') {
			$insertAry[] = "('$_studentId', '$academicYearID', '$acadermicYear', '$YearTermID', '$semName', '$isAnnual', '$_className', '$_classNumber', '$_comment', '$_comment',
							now(), now(), '".$ipf_cfg["DB_ASSESSMENT_STUDENT_MAIN_RECORD_ComeFrom"]["erc"]."'
							)";
		}
		else {
			$sql = "Update
							{$eclass_db}.CONDUCT_STUDENT
					Set
							CommentChi = '$_comment',
							CommentEng = '$_comment'
					Where
							RecordID = '".$_recordId."'
					";
			$successAry['classTeacherComment']['Update'][$_recordId] = $lreportcard->db_db_query($sql);
		}
	}
	
	# Insert Student Subject Scores
	$numOfInsert = count($insertAry);
	if ($numOfInsert > 0) {
		$numOfData = count($insertAry);
		$numOfDataPerChunck = 1000;
		$numOfChunk = ceil($numOfData / $numOfDataPerChunck);
		$insertSplittedAry = array_chunk($insertAry, $numOfChunk);
		foreach((array)$insertSplittedAry as $_insertValueAry) {
			$_insertValueText = implode(", ", $_insertValueAry);
			$sql = "
							INSERT INTO {$eclass_db}.CONDUCT_STUDENT
										(UserID, AcademicYearID, Year, YearTermID, Semester, IsAnnual, ClassName, ClassNumber, CommentChi, CommentEng,
										InputDate, ModifiedDate, ComeFrom)
							VALUES
										$_insertValueText
							";
			$successAry['classTeacherComment']['Insert'][] = $lreportcard->db_db_query($sql);
		}
	}
}
##################################################################################
###################### Transfer Class Teacher Comment [End] ######################
##################################################################################


##################################################################################
############################ Transfer Conduct [Start] ############################
##################################################################################
if (in_array('conduct', (array)$dataTypeAry) && ($isMainReport || $ReportType == 'W'))
{
	$otherInfoTermId = ($ReportType == 'T')? $SemID : 0;
	$otherInfoAry = $lreportcard->getReportOtherInfoData($ReportID);
	
	// support transfer of conduct from Class Teacher Comment 
	if($eRCTemplateSetting['ClassTeacherComment_Conduct'])
	{
	    include_once($PATH_WRT_ROOT."includes/libreportcard2008_extrainfo.php");
	    $lreportcard_extrainfo = new libreportcard_extrainfo();
	    
	    // Conduct List
	    $ConductArr = $lreportcard_extrainfo->Get_Conduct();
	    $ConductArr = BuildMultiKeyAssoc($ConductArr, "ConductID", "Conduct", 1);
	    
	    // Get Student Conduct
	    if(!$eRCTemplateSetting['ClassTeacherComment_Conduct_PuiChingGradeLimit'] || $ReportType == 'T') {
    	    $StudentExtraInfo = $lreportcard_extrainfo->Get_Student_Extra_Info($ReportID);
    	    $StudentConductIDArr = BuildMultiKeyAssoc($StudentExtraInfo, "StudentID", "ConductID", 1);
	    }
	    else {
	        // Get Semester Grade
	        $SemesterConductArr = array();
	        $SemesterList = getSemesters($lreportcard->schoolYearID, 0);
    	    for($j=0; $j<3; $j++)
    	    {
    	        $thisReportInfo = $lreportcard->returnReportTemplateBasicInfo("", "Semester='".$SemesterList[$j]["YearTermID"]."' and ClassLevelID = '".$ClassLevelID."' ");
    	        $thisReportID = $thisReportInfo["ReportID"];
    	        if($thisReportID) {
                    $StudentExtraInfo = $lreportcard_extrainfo->Get_Student_Extra_Info($thisReportID);
                    foreach((array)$StudentExtraInfo as $thisStudentExtraInfo){
                        $SemesterConductArr[$thisStudentExtraInfo['StudentID']][$j] = $ConductArr[$thisStudentExtraInfo['ConductID']];
                    }
    	        }
    	    }
            
    	    // Get Overall Grade
    	    $StudentConductGradeArr = array();
    	    foreach((array)$SemesterConductArr as $thisArrStudentID => $thisSemesterConductArr) {
    	        $StudentConductGradeArr[$thisArrStudentID] = $lreportcard->calculateOverallConduct($thisSemesterConductArr, $ConductArr);
    	        if(empty($StudentConductGradeArr[$thisArrStudentID]) || $StudentConductGradeArr[$thisArrStudentID] == $lreportcard->EmptySymbol) {
    	            $StudentConductGradeArr[$thisArrStudentID] = '';
    	        }
    	    }
	    }
	}
	
	$sql = "Select
					RecordID, UserID
			From
					{$eclass_db}.CONDUCT_STUDENT
			WHERE
					AcademicYearID = '$academicYearID'
					AND	YearTermID = '$YearTermID'
					AND	IsAnnual = '$isAnnual'
					AND ComeFrom = '".$ipf_cfg["DB_CONDUCT_STUDENT_ComeFrom"]["erc"]."'
					AND UserID In ('".implode("','", (array)Get_Array_By_Key($formStudentInfoAry, 'UserID'))."')
			";
	$ipfCommentAssoAry = BuildMultiKeyAssoc($lreportcard->returnResultSet($sql), 'UserID', 'RecordID', $SingleValue=true);
		
	$insertAry = array();
	for ($i=0; $i<$numOfFormStudent; $i++) {
		$_studentId = $formStudentInfoAry[$i]['UserID'];
		$_className = $formStudentInfoAry[$i]['ClassTitleEn'];
		$_classNumber = $formStudentInfoAry[$i]['ClassNumber'];
		$_conduct = $otherInfoAry[$_studentId][$otherInfoTermId]['Conduct'];
		
		if($eRCTemplateSetting['ClassTeacherComment_Conduct'])
		{
		    $_conduct = '';
		    if(!empty($StudentConductIDArr)) {
                $_conduct = $ConductArr[$StudentConductIDArr[$_studentId]];
		    }
		    else if(!empty($StudentConductGradeArr)) {
		        $_conduct = $StudentConductGradeArr[$_studentId];
		    }
		}
		
		$_className = $lreportcard->Get_Safe_Sql_Query($_className);
		$_conduct = $lreportcard->Get_Safe_Sql_Query($_conduct);
		
		$_recordId = $ipfCommentAssoAry[$_studentId];
		
		if ($_recordId == '') {
			$insertAry[] = "('$_studentId', '$academicYearID', '$acadermicYear', '$YearTermID', '$semName', '$isAnnual', '$_className', '$_classNumber', '$_conduct',
							now(), now(), '".$ipf_cfg["DB_ASSESSMENT_STUDENT_MAIN_RECORD_ComeFrom"]["erc"]."'
							)";
		}
		else {
			$sql = "Update
							{$eclass_db}.CONDUCT_STUDENT
					Set
							ConductGradeChar = '$_conduct'
					Where
							RecordID = '".$_recordId."'
					";
			$successAry['conduct']['Update'][$_recordId] = $lreportcard->db_db_query($sql);
		}
	}
	
	# Insert Student conduct
	$numOfInsert = count($insertAry);
	if ($numOfInsert > 0) {
		$numOfData = count($insertAry);
		$numOfDataPerChunck = 1000;
		$numOfChunk = ceil($numOfData / $numOfDataPerChunck);
		$insertSplittedAry = array_chunk($insertAry, $numOfChunk);
		foreach((array)$insertSplittedAry as $_insertValueAry) {
			$_insertValueText = implode(", ", $_insertValueAry);
			$sql = "		INSERT INTO {$eclass_db}.CONDUCT_STUDENT
										(UserID, AcademicYearID, Year, YearTermID, Semester, IsAnnual, ClassName, ClassNumber, ConductGradeChar,
										InputDate, ModifiedDate, ComeFrom)
							VALUES
										$_insertValueText
							";
			$successAry['conduct']['Insert'][] = $lreportcard->db_db_query($sql);
		}
	}
}
##################################################################################
############################# Transfer Conduct [End] #############################
##################################################################################


##################################################################################
####################### Transfer Subject Full Mark [Start] #######################
##################################################################################
if (in_array('subjectFullMark', (array)$dataTypeAry))
{
	### Get Subject Grading Scheme Info
	// $subjectSchemeAssoAry[SubjectID]["SchemeID","ScaleInput","ScaleDisplay"] = value
	$subjectSchemeAssoAry = $lreportcard->Get_Form_Grading_Scheme($ClassLevelID, $ReportID, $withRangeData=true);
	
	### Get full mark data in iPortfolio
	$ASSESSMENT_SUBJECT_FULLMARK = $eclass_db.".ASSESSMENT_SUBJECT_FULLMARK";
	$sql = "Select FullMarkID, SubjectID From $ASSESSMENT_SUBJECT_FULLMARK Where AcademicYearID = '".$academicYearID."' And YearID = '".$ClassLevelID."'";
	$fullMarkAssoAry = BuildMultiKeyAssoc($lreportcard->returnResultSet($sql), 'SubjectID', array('FullMarkID'), $SingleValue=0, $BuildNumericArray=0);
	
	foreach ((array)$subjectSchemeAssoAry as $_subjectId => $_subjectSchemeInfoAry) {
		$_scaleDisplay = $_subjectSchemeInfoAry['ScaleDisplay'];
		$_fullMark = $_subjectSchemeInfoAry['FullMark'];
		$_firstGrade = $_subjectSchemeInfoAry['SchemeRangeAry'][0]['Grade'];
		$_fullmarkRecordId = $fullMarkAssoAry[$_subjectId]['FullMarkID'];
		
		/*
		 * Since SLP Report will show the Grade of the Full Mark if the Grade is input as the Full Mark of a Subject
		 * => if display in mark => transfer full mark
		 * => if display in grade => transfer first grade
		 */
		$targetMarkDbField = '';
		$targetGradeDbField = '';
		if ($_scaleDisplay == 'M') {
			$targetMarkDbField = "'".$_fullMark."'";
			$targetGradeDbField = 'null';
		}
		else {
			$targetMarkDbField = 'null';
			$targetGradeDbField = "'".$lreportcard->Get_Safe_Sql_Query($_firstGrade)."'";
		}
		
		if ($_fullmarkRecordId > 0) {
			$sql = "Update
							$ASSESSMENT_SUBJECT_FULLMARK
					Set
							FullMarkInt = $targetMarkDbField,
							FullMarkGrade = $targetGradeDbField,
							DateModified = now(),
							LastModifiedBy = '".$_SESSION['UserID']."'
					Where
							FullMarkID = '".$_fullmarkRecordId."'
					";
			$successAry['subjectFullMark']['update'][$_subjectId] = $lreportcard->db_db_query($sql);
		}
		else {
			$sql = "Insert Into $ASSESSMENT_SUBJECT_FULLMARK 
						(SubjectID, YearID, AcademicYearID, FullMarkInt, FullMarkGrade, DateInput, InputBy, DateModified, LastModifiedBy)
					Values
						('".$_subjectId."', '".$ClassLevelID."', '".$academicYearID."', $targetMarkDbField, $targetGradeDbField, now(), '".$_SESSION['UserID']."', now(), '".$_SESSION['UserID']."')
					";
			$successAry['subjectFullMark']['insert'][$_subjectId] = $lreportcard->db_db_query($sql);
		}
	}
}
##################################################################################
######################## Transfer Subject Full Mark [End] ########################
##################################################################################

##################################################################################
####################### Transfer Promotion Status [Start] ########################
##################################################################################
if($eRCTemplateSetting['TransferToiPortfolio']['TransferPromotionStatus'] && $ReportType == 'W')
{
    $SchoolType       = $lreportcard->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
    $isPrimaryLevel   = $SchoolType == "P";
    $isSecondaryLevel = $SchoolType == "S";
    
    $isPrimaryGraduate 		= $isPrimaryLevel && $FormNumber == 6;
    $isSecondaryJuniorGrad  = $isSecondaryLevel && $FormNumber == 3;
    $isSecondaryGraduate 	= $isSecondaryLevel && $FormNumber == 6;
    
    $promotionStatusAry = $lreportcard->customizedPromotionStatus;
    $studentPromotionStatus = $lreportcard->GetPromotionStatusList($ClassLevelID, "", "", $ReportID);
    
    $sql = "SELECT
                RecordID, UserID
            FROM
                {$eclass_db}.CONDUCT_STUDENT
            WHERE
                AcademicYearID = '$academicYearID' AND YearTermID = '$YearTermID' AND IsAnnual = '$isAnnual'
                AND ComeFrom = '".$ipf_cfg["DB_CONDUCT_STUDENT_ComeFrom"]["erc"]."'
				AND UserID IN ('".implode("','", (array)Get_Array_By_Key($formStudentInfoAry, 'UserID'))."') ";
    $ipfCommentAssoAry = BuildMultiKeyAssoc($lreportcard->returnResultSet($sql), 'UserID', 'RecordID', $SingleValue=true);
    
    $insertAry = array();
    for ($i=0; $i<$numOfFormStudent; $i++) {
        $_studentId = $formStudentInfoAry[$i]['UserID'];
        $_className = $formStudentInfoAry[$i]['ClassTitleEn'];
        $_classNumber = $formStudentInfoAry[$i]['ClassNumber'];
        
        $_promotionStatus = $studentPromotionStatus[$_studentId]["Promotion"];
        switch ($_promotionStatus)
        {
            case $promotionStatusAry['Promoted']:
                $_promotionStatus = $eReportCard["PromotionStatusType"]["StatusType1"];
                break;
            case $promotionStatusAry['Promoted_with_Retention']:
                $_promotionStatus = $eReportCard["PromotionStatusType"]["StatusType2"];
                break;
            case $promotionStatusAry['Graduate']:
                $_promotionStatus = $eReportCard["PromotionStatusType"]["StatusType3"];
                break;
            case $promotionStatusAry['Retained']:
                $_promotionStatus = $eReportCard["PromotionStatusType"]["StatusType4"];
                break;
            case $promotionStatusAry['Dropout']:
                $_promotionStatus = $eReportCard["PromotionStatusType"]["StatusType5"];
                break;
            default:
                $_promotionStatus = '';
                break;
        }
        
        $_className = $lreportcard->Get_Safe_Sql_Query($_className);
        $_promotionStatus = $lreportcard->Get_Safe_Sql_Query($_promotionStatus);
        
        $_recordId = $ipfCommentAssoAry[$_studentId];
        if ($_recordId == '') {
            $insertAry[] = "('$_studentId', '$academicYearID', '$acadermicYear', '$YearTermID', '$semName', '$isAnnual', '$_className', '$_classNumber', '$_promotionStatus',
                                NOW(), NOW(), '".$ipf_cfg["DB_ASSESSMENT_STUDENT_MAIN_RECORD_ComeFrom"]["erc"]."'
							)";
        }
        else {
            $sql = "UPDATE
                        {$eclass_db}.CONDUCT_STUDENT
                    SET
                        PromotionStatus = '$_promotionStatus'
                    WHERE
                        RecordID = '".$_recordId."' ";
            $successAry['promotion']['Update'][$_recordId] = $lreportcard->db_db_query($sql);
        }
    }
    
    # Insert student promotion status
    $numOfInsert = count($insertAry);
    if ($numOfInsert > 0) {
        $numOfData = count($insertAry);
        $numOfDataPerChunck = 1000;
        $numOfChunk = ceil($numOfData / $numOfDataPerChunck);
        $insertSplittedAry = array_chunk($insertAry, $numOfChunk);
        foreach((array)$insertSplittedAry as $_insertValueAry) {
            $_insertValueText = implode(", ", $_insertValueAry);
            $sql = "INSERT INTO {$eclass_db}.CONDUCT_STUDENT
                        (UserID, AcademicYearID, Year, YearTermID, Semester, IsAnnual, ClassName, ClassNumber, PromotionStatus,
                        InputDate, ModifiedDate, ComeFrom)
                    VALUES
                        $_insertValueText ";
            $successAry['promotion']['Insert'][] = $lreportcard->db_db_query($sql);
        }
    }
}
##################################################################################
######################### Transfer Promotion Status [End] ########################
##################################################################################

if (!in_multi_array(false, $successAry)) {
	# update the last transfer time
	$lreportcard->UPDATE_REPORT_LAST_DATE($ReportID, "LastDateToiPortfolio");
	$result = "update";
}
else {
	$result = "update_failed";
}

$liblog = new liblog();
$tmpAry = array();
$tmpAry['AcademicYearName'] = $acadermicYear;
$tmpAry['DbName'] = $lreportcard->DBName;
$tmpAry['ReportID'] = $ReportID;
$tmpAry['ClassLevelID'] = $ClassLevelID;
$tmpAry['ClassLevelName'] = $lreportcard->returnClassLevel($ClassLevelID);
$tmpAry['ReportType'] = $ReportType;
$tmpAry['TermID'] = $SemID;
$tmpAry['TermName'] = $lreportcard->returnSemesters($SemID);
$tmpAry['TransferDataType'] = implode(', ', (array)$dataTypeAry);
$tmpAry['Result'] = $result;
$successAry['TransferLog'] = $liblog->INSERT_LOG($lreportcard->ModuleTitle, 'Data_Transfer_To_iPortfolio', $liblog->BUILD_DETAIL($tmpAry));

intranet_closedb();

$para = "";
$para .= "Result=$result";
//$para .= "&ClassLevelID=$ClassLevelID_filter";
//$para .= "&Semester=$Semester_filter";
header("Location:transfer_to_iportfolio.php?$para");
?>