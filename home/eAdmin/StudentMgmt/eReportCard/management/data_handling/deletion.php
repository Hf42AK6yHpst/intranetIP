<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	$lreportcard_ui = new libreportcard_ui();
	$linterface = new interface_html();
	
	if ($lreportcard->hasAccessRight()) {
	
		# Get ClassLevelID (Form) of the reportcard template
		$FormArr = $lreportcard->GET_ALL_FORMS(1);
		if(count($FormArr) > 0) {
			# Filters - By Form (ClassLevelID)
			$ClassLevelID = ($ClassLevelID == "") ? $FormArr[0][0] : $ClassLevelID;
			$ClassLevelName = $lreportcard->returnClassLevel($ClassLevelID);
			$FormSelection = $linterface->GET_SELECTION_BOX($FormArr, "name='ClassLevelID' class='' onchange='loadReportList(this.options[this.selectedIndex].value)'", "", $ClassLevelID);
			
			# Filters - By Report
			$ReportTypeSelection = '';
			$ReportTypeArr = array();
			$ReportTypeOption = array();
			$ReportIDList = array();
			$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);
			
			if(count($ReportTypeArr) > 0){
				for($j=0 ; $j<count($ReportTypeArr) ; $j++){
					$ReportIDList[] = $ReportTypeArr[$j]['ReportID'];
					$ReportTypeOption[$j][0] = $ReportTypeArr[$j]['ReportID'];
					$ReportTypeOption[$j][1] = $ReportTypeArr[$j]['SemesterTitle'];
				}
			}
			
			$ReportTypeSelectDisabled = "";
			if (sizeof($ReportTypeOption) == 0) {
				$ReportTypeOption[0][0] = "-1";
				$ReportTypeOption[0][1] = $eReportCard['NoReportAvailable'];
				$ReportTypeSelectDisabled = "disabled='disabled'";
			}
			
			$ReportID = ($ReportID == "" || !in_array($ReportID,$ReportIDList)) ? $ReportTypeOption[0][0] : $ReportID;
			$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
			$ReportTypeSelection = $linterface->GET_SELECTION_BOX($ReportTypeOption, 'name="ReportID" id="ReportID" class="" '.$ReportTypeSelectDisabled.' onchange=""', '', $ReportID, '');
			
			// Alternate sets of Reports, they will be used when User selected a different Class Level
			// They will store as JS array
			$OtherReportTypeSelection = array();
			
			for($i=0; $i<sizeof($FormArr); $i++) {
				$ClassLevelName = $lreportcard->returnClassLevel($FormArr[$i][0]);
				$ReportTypeArr = array();
				$ReportTypeOption = array();
				$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($FormArr[$i][0]);
				$ReportTypeOptionCount = 0;
				
				if(count($ReportTypeArr) > 0){
					for($j=0; $j<sizeof($ReportTypeArr); $j++){
						$ReportTypeOption[$ReportTypeOptionCount][0] = $ReportTypeArr[$j]['ReportID'];
						$ReportTypeOption[$ReportTypeOptionCount][1] = $ReportTypeArr[$j]['SemesterTitle'];
						$ReportTypeOptionCount++;
					}
				}
				
				$ReportTypeSelectDisabled = "";
				if (sizeof($ReportTypeOption) == 0) {
					$ReportTypeOption[0][0] = "-1";
					$ReportTypeOption[0][1] = $eReportCard['NoReportAvailable'];
					$ReportTypeSelectDisabled = 'disabled="disabled"';
				}
				$OtherReportTypeSelection[$FormArr[$i][0]] = $linterface->GET_SELECTION_BOX($ReportTypeOption, 'name="ReportID" id="ReportID" '.$ReportTypeSelectDisabled.' onchange="loadColumnList(this.options[this.selectedIndex].value)"', '');
			}
		}
		
		
		### Data Delete Options
		$DeleteOptionTable = '';
		$DeleteOptionTable .= "<table border='0' cellspacing='0' cellpadding='0'>";
		
			# Select all
			$DeleteOptionTable .= "<tr>";
				$DeleteOptionTable .= "<td class='tabletext' colspan='2'>";
					$DeleteOptionTable .= "<input type='checkbox' name='Delete_all' id='Delete_all' onClick=\"js_Select_All();\">";
					$DeleteOptionTable .= "<label for='Delete_all'> ".$button_select_all."</label>";
				$DeleteOptionTable .= "</td>";
			$DeleteOptionTable .= "</tr>";
			
			# Class Teacher Comment
			$DeleteOptionTable .= "<tr>";
				$DeleteOptionTable .= "<td style='width:20px'>&nbsp;</td>";
				$DeleteOptionTable .= "<td class='tabletext'>";
					$DeleteOptionTable .= "<input type='checkbox' name='DeleteComment' id='DeleteComment' value='1' onClick=\"js_Check_Select_All();\">";
					$DeleteOptionTable .= "<label for='DeleteComment'> ".$eReportCard['Management_ClassTeacherComment']."</label>";
				$DeleteOptionTable .= "</td>";
			$DeleteOptionTable .= "</tr>";
			
			# Marksheet
			$DeleteOptionTable .= "<tr>";
				$DeleteOptionTable .= "<td style='width:20px'>&nbsp;</td>";
				$DeleteOptionTable .= "<td class='tabletext'>";
					$DeleteOptionTable .= "<input type='checkbox' name='DeleteMarksheet' id='DeleteMarksheet' value='1' onClick=\"js_Check_Select_All();\">";
					$DeleteOptionTable .= "<label for='DeleteMarksheet'> ".$eReportCard['Marksheet']." <span class='tabletextremark'>(".$eReportCard['DeleteMarksheetRemarks'].")</span></label>";
				$DeleteOptionTable .= "</td>";
			$DeleteOptionTable .= "</tr>";
			
			# Generated & Archived Report
			$DeleteOptionTable .= "<tr>";
				$DeleteOptionTable .= "<td style='width:20px'>&nbsp;</td>";
				$DeleteOptionTable .= "<td class='tabletext'>";
					$DeleteOptionTable .= "<input type='checkbox' name='DeleteReport' id='DeleteReport' value='1' onClick=\"js_Check_Select_All();\">";
					$DeleteOptionTable .= "<label for='DeleteReport'> ".$eReportCard['Generated&ArchivedReport']." <span class='tabletextremark'>(".$eReportCard['DeleteReportRemarks'].")</span></label>";
				$DeleteOptionTable .= "</td>";
			$DeleteOptionTable .= "</tr>";
			
		$DeleteOptionTable .= "</table>";
		
		
		############## Interface Start ##############
		$linterface = new interface_html();
		$CurrentPage = "Management_DataHandling";
		$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
		
		# tag information
		$TAGS_OBJ = $lreportcard_ui->Get_Mgmt_DataHandling_Tab_Array('deletion');
		
		
		# page navigation (leave the array empty if no need)
		//$PAGE_NAVIGATION[] = array($eReportCard['Management_DataHandling']);
		$linterface->LAYOUT_START();
		
?>
<script type="text/JavaScript" language="JavaScript">
<!--
// Preloaded sets of Report list
var classLevelReportList = new Array();

<?php
if(count($FormArr) > 0 && count($ReportTypeArr) > 0) {
	foreach($OtherReportTypeSelection as $key => $value) {
		$value = str_replace("\n", "", $value);
		$x .= "classLevelReportList[".$key."] = '$value';\n";
	}
	echo $x;
}
?>

function loadReportList(classLevelID) {
	document.getElementById("ReportList").innerHTML = classLevelReportList[classLevelID];
	
	var reportIDSelect = document.getElementById("ReportID");
	if (reportIDSelect.disabled) {
		document.getElementById("submitBtn").disabled = true;
	} else {
		document.getElementById("submitBtn").disabled = false;
	}
}

function discardWarning() {
	document.getElementById("WarningMsgRow").style.display = "none";
	document.getElementById("WarningBtnRow").style.display = "none";
	
	document.getElementById("SysMsg").innerHTML = "";
	
	document.getElementById("FormContentRow1").style.display = "";
	document.getElementById("FormContentRow2").style.display = "";
	document.getElementById("FormContentRow3").style.display = "";
	document.getElementById("FormBtnRow").style.display = "";
	
	document.form1.ClassLevelID.focus();
}

function confirmSubmit() {
	
	var Comment 	= $('#DeleteComment').attr('checked');
	var Marksheet 	= $('#DeleteMarksheet').attr('checked');
	var Report 		= $('#DeleteReport').attr('checked');
	
	if (Comment == false && Marksheet == false && Report == false)
	{
		alert("<?=$eReportCard['jsAtLeastOneDeletionRecord']?>");
		return false;
	}
	
	return confirm("<?= $eReportCard['ConfirmDeletion'] ?>");
}

function js_Select_All()
{
	if ($('#Delete_all').attr('checked'))
	{
		$('#DeleteComment').attr('checked', true);
		$('#DeleteMarksheet').attr('checked', true);
		$('#DeleteReport').attr('checked', true);
	}
	else
	{
		$('#DeleteComment').attr('checked', false);
		$('#DeleteMarksheet').attr('checked', false);
		$('#DeleteReport').attr('checked', false);
	}
}

function js_Check_Select_All()
{
	var Comment 	= $('#DeleteComment').attr('checked');
	var Marksheet 	= $('#DeleteMarksheet').attr('checked');
	var Report 		= $('#DeleteReport').attr('checked');
	
	if (Comment == false || Marksheet == false || Report == false)
		$('#Delete_all').attr('checked', false);
}

-->
</script>
<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT."templates/2007a/css/"?>ereportcard.css">
<br />
<form name="form1" action="deletion_update.php" method="POST" onsubmit="return confirmSubmit()">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
<table>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td></td>
					<td id="SysMsg" align="right"><?=$linterface->GET_SYS_MSG($Result);?></td>
				</tr>
				<tr id='WarningMsgRow'>
					<td align='' colspan='2' class='tabletextrequire'>
						<p><?= $eReportCard['DataDeletionWarning'] ?></p>
					</td>
				</tr>
				<tr id='FormContentRow1' style='display:none;'>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['Form'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<?= $FormSelection ?>
					</td>
				</tr>
				<tr id='FormContentRow2' style='display:none;'>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ReportCard'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<span id="ReportList"><?= $ReportTypeSelection ?></span>
					</td>
				</tr>
				<tr id='FormContentRow3' style='display:none;'>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['TargetDeletionRecord'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<span id="ReportList"><?= $DeleteOptionTable ?></span>
					</td>
				</tr>
				<tr>
					<td colspan='2' class="dotline">
						<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
					</td>
				</tr>
				<tr id='WarningBtnRow'>
					<td colspan='2' align="center">
						<?= $linterface->GET_ACTION_BTN($button_continue, "button", "discardWarning()") ?>
					</td>
				</tr>
				<tr id='FormBtnRow' style='display:none;'>
					<td colspan='2' align="center">
						<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "", "id='submitBtn'") ?>
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='deletion.php'") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<?
  	$linterface->LAYOUT_STOP();
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>
