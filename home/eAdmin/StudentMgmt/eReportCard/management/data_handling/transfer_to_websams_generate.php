<?php

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";
// $intranet_session_language = 'en';

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$ClassLevelID= $_POST['ClassLevelID'];
$ReportID = $_POST['ReportID'];
$ColumnID = $_POST['ColumnID'];
if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "")
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	if ($lreportcard->hasAccessRight())
	{
		$lexport = new libexporttext();
		$fcm = new form_class_manage();
		$lsubject = new subject();
		$libyear = new year($ClassLevelID);
		$ClassLevelName = $libyear->YearName;
		$ReportSetting 	= $lreportcard->returnReportTemplateBasicInfo($ReportID);
		$SemID 			= $ReportSetting["Semester"];
		$SemNum 		= $lreportcard->Get_Semester_Seq_Number($SemID);
		$FormNumber		= $lreportcard->GET_FORM_NUMBER($ClassLevelID, 1);
		$SubjectArray 	= $lreportcard->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
		$MainSubjectArray 	= $lreportcard->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID="", $SubjectFieldLang="", $ParDisplayType="Desc", $ExcludeCmpSubject=0, $ReportID);
		$year = $lreportcard->GET_ACTIVE_YEAR();
		
		// 2020-05-25 (Philips) - Hardcode websamscode array
		$subjectCodeOrderMapping = array();
		$subjectCodeOrderMapping['1'] = array('075','080','110','130','165','210','235','240','260','263','280','300','310','350','432');
		$subjectCodeOrderMapping['2'] = array('075','080','110','130','165','210','235','240','260','263','280','300','310','350','432');
// 		$subjectCodeOrderMapping['3'] = array('075','080','110','165','210','235','260','263','280','300','310','350','432','900','920');
		$subjectCodeOrderMapping['3'] = array('075','080','110','165','210','235','045','070','315','263','280','300','310','350','432','900','920');
		$subjectCodeOrderMapping['4'] = array('045','070','075','080','12N','135','13N','165','165','210','22S','24S','265','310','315','81N');
		$subjectCodeOrderMapping['5'] = array('045','070','075','080','12N','135','13N','165','165','210','22S','24S','265','310','315','81N');
		$subjectCodeOrderMapping['6'] = array('045','070','075','080','12N','135','13N','165','165','210','22S','24S','265','310','315','81N');
		
		$thisSubjectCodeOrderMapping = $subjectCodeOrderMapping[$FormNumber];
		
		
		if(sizeof($MainSubjectArray) > 0) {
			foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
		}
		$ColumnData = $lreportcard->returnReportTemplateColumnData($ReportID);
		foreach($ColumnData as $col){
			if($ColumnID == $col['ReportColumnID']) $DisplayOrder = array_search($col, $ColumnData) + 1;
		}
		
		
		$newSubjectArray = array();
		foreach($SubjectArray as $SubjectID => $SubjectName){
			$tempSubj = new subject($SubjectID);
			$tempSubjCode = $tempSubj->CODEID;
			$isSub = 0;
			if(in_array($SubjectID, $MainSubjectIDArray) != true) $isSub = 1;
			$CmpSubjectArr = array();
			$isParentSubject = 0;		// set to "1" when Scale Input of component subjects is Mark (M)
			if (!$isSub) {
				$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
				if(!empty($CmpSubjectArr)) $isParentSubject = 1;
				else {
					if(in_array($tempSubjCode, $thisSubjectCodeOrderMapping)){
						$newSubjectArray[$tempSubjCode] = array($SubjectID, $SubjectName);
					}
				}
			}
			$tempSubjSName = $tempSubj->CH_SNAME;
			if($isSub){
				$parentSubjID = $lreportcard->GET_PARENT_SUBJECT_ID($SubjectID);
				if(in_array($tempSubjCode, $thisSubjectCodeOrderMapping) && !$newSubjectArray[$tempSubjCode]){
					$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($parentSubjID, $ClassLevelID);
					$tempCmpSubjectArr = array();
					foreach($CmpSubjectArr as $cmpSubject){
						$tempCmpSubjectArr[] = array($cmpSubject['SubjectID'], $cmpSubject['SubjectName']);
					}
					$newSubjectArray[$tempSubjCode] = $tempCmpSubjectArr;
				}
			}
			unset($tempSubj);
		}
		$SubjectArray = array();
		foreach($thisSubjectCodeOrderMapping as $thisCode){
			$_subject = $newSubjectArray[$thisCode];
			if(!is_array($_subject[0])){
				if($_subject[0] != null) $SubjectArray[$_subject[0]] = $_subject[1];
			} else {
				foreach ($_subject as $__subject){
					if($__subject[0] != null) $SubjectArray[$__subject[0]] = $__subject[1];
				}
			}
		}
		
		$classAry = $fcm->Get_Class_List($lreportcard->GET_ACTIVE_YEAR_ID(),$ClassLevelID);
		
		$headerAry = array('*School ID', '*School Year', '* School Level', '* School Session', '*Class Level', '*Class', '*Class Number', '*Student Name', '*Reg. No.');
		
		$exportSubjAry = array();
		$exportContentAry = array();
		foreach($SubjectArray as $SubjectID => $SubjectName){
			$tempSubj = new subject($SubjectID);
			$isSub = 0;
			if(in_array($SubjectID, $MainSubjectIDArray) != true) $isSub = 1;
			
			// if parent subject, if yes find its components subjects
			$CmpSubjectArr = array();
			$isParentSubject = 0;		// set to "1" when Scale Input of component subjects is Mark (M)
			if (!$isSub) {
				$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
				if(!empty($CmpSubjectArr)) $isParentSubject = 1;
			}
			if($isParentSubject) continue;
			$exportSubjAry[] = $SubjectID;
			$tempSubjCode = $tempSubj->CODEID;
			$tempSubjSName = $tempSubj->CH_SNAME;
			if($isSub){
				$parentSubjID = $lreportcard->GET_PARENT_SUBJECT_ID($SubjectID);
				$tempParentSubject = new subject($parentSubjID);
				$tempSubjSName = $tempParentSubject->CH_SNAME .'_'.$tempSubjSName;
			}
			$headerAry[] = 'T'.$SemNum.'A'.$DisplayOrder.'_'.$tempSubjCode.'_'.$tempSubjSName.'_C_SCORE';
			unset($tempSubj);
		}
		$filename = 'T'.$SemNum.'A'.$DisplayOrder.'_'.$ClassLevelName.'.csv';
		foreach($classAry as $classInfo){
			// YearClassID
			$yearClassID = $classInfo['YearClassID'];
			$className = $classInfo['ClassTitleEN'];
			$yc = new year_class($yearClassID);
			$yc->Get_Class_Teacher_List(0);
			$classTeacherAry = $yc->ClassTeacherList;
			$teacherName = implode(', ', Get_Array_By_Key($classTeacherAry, 'TeacherName'));
			$studentAry = $fcm->Get_Student_By_Class($yearClassID);
			foreach($studentAry as $std){
				$sid = $std['UserID'];
				$classno = $std['ClassNumber'];
				$lu = new libuser($sid);
				$userlogin = $lu->UserLogin;
				$userlogin = str_replace('czm', '#', $userlogin);
				$userName = $lu->EnglishName;
				$regno = $lu->WebSAMSRegNo;
				
				# Retrieve Subject Marks
				$MarksAry = $lreportcard->getMarks($ReportID, $sid, "", 0, 1);
				$subjectMarkAry = array();
				foreach($exportSubjAry as $subjID){
					# Subject Mark Display
					$thisMSMark = $MarksAry[$subjID][$ColumnID]["Mark"];
// 					$thisMark = ($thisMSMark) ? $thisMSMark : $lreportcard->EmptySymbol;
					$thisMark = ($thisMSMark) ? $thisMSMark : 'N.T.';
					$subjectMarkAry[] = $thisMark;
				}
				$row = array('190110', $year, '3', '3', $ClassLevelName, $className, $classno, $userName, $userlogin);
				$row = array_merge($row, $subjectMarkAry);
				$exportContentAry[] = $row;
			}
		}
		$contentAry = $lexport->GET_EXPORT_TXT($exportContentAry, $headerAry);
		$lexport->EXPORT_FILE($filename, $contentAry);
		?>
		You have no priviledge to access this page.
<?php 
	}
	
} else {
?>
		You have no priviledge to access this page.
<?php
}

intranet_closedb();
?>