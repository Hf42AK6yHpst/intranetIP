<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");


$lreportcard = new libreportcardcustom();
$lreportcard_ui = new libreportcard_ui();
$lreportcard->hasAccessRight();


$returnMsgKey = $_GET['returnMsgKey'];


############## Interface Start ##############
$linterface = new interface_html();
$CurrentPage = "Management_DataHandling";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ = $lreportcard_ui->Get_Mgmt_DataHandling_Tab_Array('copyMarksheet');

$returnMsg = $Lang['eReportCard']['ManagementArr']['DataHandlingArr'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);


### Warning Table
$htmlAry['warningTbl'] = $linterface->GET_WARNING_TABLE($err_msg='', $DeleteItemName='', $DeleteButtonName='', $Lang['eReportCard']['ManagementArr']['DataHandlingArr']['CopyMarksheetWarning']);


### Option Table
$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	// Form
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['General']['Form'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $lreportcard_ui->Get_Form_Selection('YearID', $SelectedYearID='', 'changedFormSelection();', $noFirst=1, $isAll=0, $hasTemplateOnly=1, $checkPermission=0, $IsMultiple=0, $excludeWithoutClass=false);
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	// From Report
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['eReportCard']['ManagementArr']['DataHandlingArr']['FromReport'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= '<div id="FromReportSelDiv"></div>'."\r\n";
			$x .= '<div id="FromReportColumnSelDiv"></div>'."\r\n";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	// To Report
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['eReportCard']['ManagementArr']['DataHandlingArr']['ToReport'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= '<div id="ToReportSelDiv"></div>'."\r\n";
			$x .= '<div id="ToReportColumnSelDiv"></div>'."\r\n";
			$x .= $linterface->Get_Form_Warning_Msg('sameSelectionWarnDiv', $Lang['eReportCard']['ManagementArr']['DataHandlingArr']['SameSelectionWarning'], $Class='WarnDiv');
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['optionTbl'] = $x;


$htmlAry['submitBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "formCheck();");

?>
<script language="javascript">
var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image()?>';
$(document).ready( function() {
	changedFormSelection();
});

function changedFormSelection() {
	reloadReportSelection('From');
	reloadReportSelection('To');
}

function reloadReportSelection(selType) {
	var YearID = $('select#YearID').val();
	var targetDivId = selType + 'ReportSelDiv';
	
	$('div#' + targetDivId).html(loadingImg).load(
		"../../reports/ajax_reload_selection.php",
		{ 
			RecordType: 'Report',
			YearID: YearID,
			SelectionID: selType + 'ReportID',
			onChange: 'reloadReportColumnSelection(\'' + selType + '\')',
			ReportType: 'T'
		},
		function(ReturnData) {
			reloadReportColumnSelection(selType);
		}
	);
}

function reloadReportColumnSelection(selType) {
	var ReportID = $('select#' + selType + 'ReportID').val();
	var targetDivId = selType + 'ReportColumnSelDiv';
	
	$('div#' + targetDivId).html(loadingImg).load(
		"../../reports/ajax_reload_selection.php",
		{ 
			RecordType: 'ReportColumnSelection',
			ReportID: ReportID,
			SelectionID: selType + 'ReportColumnID',
			IsAll: 1,
			NoFirst: 0,
			IncludeOverallOption: 1,
			OverallOptionValue: -1,
			FirstTitle: '<?=$Lang['eReportCard']['ManagementArr']['DataHandlingArr']['AllReportColumn']?>'
		},
		function(ReturnData) {
			
		}
	);
}

function formCheck() {
	$('div.WarnDiv').hide();
	var canSubmit = true;
	
	var fromReport = $('select#FromReportID').val();
	var fromReportColumnId = $('select#FromReportColumnID').val();
	var toReport = $('select#ToReportID').val();
	var toReportColumnId = $('select#ToReportColumnID').val();
	if (fromReport == toReport && fromReportColumnId == toReportColumnId) {
		$('select#FromReportID').focus();
		$('div#sameSelectionWarnDiv').show();
		canSubmit = false;
	}
	
	
	if (canSubmit && confirm('<?=$Lang['eReportCard']['ManagementArr']['DataHandlingArr']['CopyMarksheetConfirmWarning']?>')) {
		$('form#form1').attr('action', 'copy_marksheet_update.php').submit();
	}
}
</script>
<form name="form1" id="form1" method="POST" action="index.php">
	<div class="table_board">
		<div>
			<?=$htmlAry['warningTbl']?>
			<br style="clear:both;" />
			
			<?=$htmlAry['optionTbl']?>
		</div>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<?=$htmlAry['submitBtn']?>
			<p class="spacer"></p>
		</div>
		<br style="clear:both;" />
	</div>
	<br />
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>