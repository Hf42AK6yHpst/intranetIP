<?php

function DELETE_TABLE_ENTRIES($ReportID, $TableList) {
	global $lreportcard;
	$success = array();
	for($i=0; $i<sizeof($TableList); $i++) {
		$table = $lreportcard->DBName.".".$TableList[$i][0];
		
		$conds = '';
		if ($TableList[$i][1] != '')
			$conds = ' And '.$TableList[$i][1];
		
		$sql = "DELETE FROM $table WHERE ReportID = '$ReportID' ".$conds;
		$success[] = $lreportcard->db_db_query($sql);
	}
	return !in_array(false, $success);
}

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$PageRight = "ADMIN";

if (!$plugin['ReportCard2008']) {
	intranet_closedb();
	header("Location:/index.php");
}

if (!isset($ClassLevelID, $ReportID)) {
	header("Location:deletion.php");
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
$lreportcard = new libreportcard();

if (!$lreportcard->hasAccessRight()) {
	intranet_closedb();
	header("Location:/index.php");
}

$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
$ColumnIDList = array_keys($ColumnTitleAry);
$ColumnIDListSql = implode(",", $ColumnIDList);

// Delete records in these tables with the given ReportID
/*
$TableList =	array(
					"RC_CLASS_COMMENT_PROGRESS",
					"RC_MARKSHEET_COMMENT",
					"RC_MARKSHEET_FEEDBACK",
					"RC_MARKSHEET_OVERALL_SCORE",
					"RC_MARKSHEET_SUBMISSION_PROGRESS",
					"RC_MARKSHEET_VERIFICATION_PROGRESS",
					"RC_OTHER_STUDENT_INFO"
				);
*/
/* Add theses tables too if consolidated score also needed to be deleted
"RC_REPORT_RESULT",
"RC_REPORT_RESULT_ARCHIVE",
"RC_REPORT_RESULT_FULLMARK",
"RC_REPORT_RESULT_SCORE",
"RC_REPORT_RESULT_SCORE_ARCHIVE"
*/


$DeleteComment		= $_POST['DeleteComment'];
$DeleteMarksheet 	= $_POST['DeleteMarksheet'];
$DeleteReport 		= $_POST['DeleteReport'];

$TableList = array();
if ($DeleteComment)
{
	$TableList[] = array('RC_MARKSHEET_COMMENT', "(SubjectID Is NULL Or SubjectID = '')");
	$TableList[] = array('RC_CLASS_COMMENT_PROGRESS', "");
}

if ($DeleteMarksheet)
{
	$TableList[] = array('RC_EXTRA_SUBJECT_INFO', "");
	$TableList[] = array('RC_MARKSHEET_OVERALL_SCORE', "");
	$TableList[] = array('RC_MARKSHEET_FEEDBACK', "");
	$TableList[] = array('RC_MARKSHEET_COMMENT', "SubjectID != ''");
	$TableList[] = array('RC_MARKSHEET_SUBMISSION_PROGRESS', "");
	$TableList[] = array('RC_MARKSHEET_VERIFICATION_PROGRESS', "");
	$TableList[] = array('RC_OTHER_STUDENT_INFO', "");
}

if ($DeleteReport)
{
	$TableList[] = array('RC_MANUAL_ADJUSTMENT', "");
	$TableList[] = array('RC_REPORT_CARD_ARCHIVE', "");
	$TableList[] = array('RC_REPORT_RESULT', "");
	$TableList[] = array('RC_REPORT_RESULT_ARCHIVE', "");
	$TableList[] = array('RC_REPORT_RESULT_FULLMARK', "");
	$TableList[] = array('RC_REPORT_RESULT_SCORE', "");
	$TableList[] = array('RC_REPORT_RESULT_SCORE_ARCHIVE', ""); 
}
$success[] = DELETE_TABLE_ENTRIES($ReportID, $TableList);


# Delete SubMS data
if ($DeleteMarksheet)
{
	// These two tables only have ReportColumnID
	$table = $lreportcard->DBName.".RC_MARKSHEET_SCORE";
	$sql = "DELETE FROM $table WHERE ReportColumnID IN ($ColumnIDListSql)";
	$success['MS'] = $lreportcard->db_db_query($sql);
	
	$table = $lreportcard->DBName.".RC_SUB_MARKSHEET_COLUMN";
	$sql = "SELECT ColumnID FROM $table WHERE ReportColumnID IN ($ColumnIDListSql)";
	$subMSColumnIDArr = $lreportcard->returnVector($sql);
	if (count($subMSColumnIDArr) > 0)
	{
		$subMSColumnIDList = implode(', ', $subMSColumnIDArr);
		
		/*
		# Delete Sub MS Column
		$table = $lreportcard->DBName.".RC_SUB_MARKSHEET_COLUMN";
		$sql = "DELETE FROM $table WHERE ReportColumnID IN ($ColumnIDListSql)";
		$success['SubMS_Column'] = $lreportcard->db_db_query($sql);
		*/
		
		# Delete Sub MS Score
		$table = $lreportcard->DBName.".RC_SUB_MARKSHEET_SCORE";
		$sql = "DELETE FROM $table WHERE ColumnID IN ($subMSColumnIDList)";
		$success['SubMS_Score'] = $lreportcard->db_db_query($sql);
	}
}


$Result = (in_array(false, $success)) ? "delete_failed" : "delete";

if ($Result == 'delete')
{
	if ($DeleteMarksheet == 1)
	{
		$lreportcard->UPDATE_REPORT_LAST_DATE($ReportID, 'LastMarksheetInput', 1);
	}

	if ($DeleteReport == 1)
	{
		$lreportcard->UPDATE_REPORT_LAST_DATE($ReportID, 'LastPrinted', 1);
		$lreportcard->UPDATE_REPORT_LAST_DATE($ReportID, 'LastAdjusted', 1);
		$lreportcard->UPDATE_REPORT_LAST_DATE($ReportID, 'LastGenerated', 1);
		$lreportcard->UPDATE_REPORT_LAST_DATE($ReportID, 'LastArchived', 1);
	}
}

intranet_closedb();
header("Location:deletion.php?Result=$Result");
?>

