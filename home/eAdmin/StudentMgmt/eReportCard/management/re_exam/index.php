<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	echo "You have no priviledge to access this page.";
}
	
	
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
}
else
{
	$lreportcard = new libreportcard();
}

$CurrentPage = "Management_ReExam";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

if (!$lreportcard->hasAccessRight())
{
	echo "You have no priviledge to access this page.";
}

$linterface = new interface_html();
$PAGE_TITLE = $eReportCard['ReExam'];

# tag information
$TAGS_OBJ[] = array($PAGE_TITLE, "", 0);
$linterface->LAYOUT_START();


## Construct filters
if($ck_ReportCard_UserType=="TEACHER")
{
	$lteaching = new libteaching();
	
	$FormArrCT = $lreportcard->returnSunjectTeacherForm($UserID);
				
	$TeacherClassAry = $lteaching->returnTeacherClassWithLevel($UserID, $lreportcard->schoolYearID);
	$TeachingClassIDArr = Get_Array_By_Key($TeacherClassAry, 'ClassID');
	$numOfTeachingCount = count($TeachingClassIDArr);
	//$TeacherClass = $TeacherClassAry[0]['ClassName'];
	
	if($numOfTeachingCount > 0)
	{
		for($i=0;$i<sizeof($TeacherClassAry);$i++)
		{
			$thisClassLevelID = $TeacherClassAry[$i]['ClassLevelID'];
			$searchResult = multiarray_search($FormArrCT , "ClassLevelID", $thisClassLevelID);
			if($searchResult == "-1" && $lreportcard->Is_ClassLevel_Has_ReExam_Report($TeacherClassAry[$i]['ClassLevelID']))
			{
				$thisAry = array(
					"0"=>$TeacherClassAry[$i]['ClassLevelID'],
					"ClassLevelID"=>$TeacherClassAry[$i]['ClassLevelID'],
					"1"=>$TeacherClassAry[$i]['LevelName'],
					"LevelName"=>$TeacherClassAry[$i]['LevelName']);
				$FormArrCT = array_merge($FormArrCT, array($thisAry));
			}
		}
		
		# sort $FormArrCT
		foreach ($FormArrCT as $key => $row) 
		{
		    $field1[$key] 	= $row['ClassLevelID'];
			$field2[$key]  	= $row['LevelName'];
		}
		array_multisort($field1, SORT_ASC, $field2, SORT_ASC, $FormArrCT);
	}
	
	$ClassLevelID = ($ClassLevelID == "") ? $FormArrCT[0][0] : $ClassLevelID;
	
}

############################################################################################################

# 3 GET Parameters : ClassLevelID, SubjectID, SemesterType_[ClassLevelID]
# Get ClassLevelID (Form) of the reportcard template

//$FormArr = $lreportcard->GET_ALL_FORMS(1);
$FormArr = $ck_ReportCard_UserType=="ADMIN" ? $lreportcard->GET_ALL_FORMS(1) : $FormArrCT;
# filter class level that do not have re-exam report
$tmpFormArr = array();
for ($i=0; $i<count($FormArr); $i++)
{
	$thisClassLevelID = $FormArr[$i]["ClassLevelID"];
	
	if ($lreportcard->Is_ClassLevel_Has_ReExam_Report($thisClassLevelID))
		$tmpFormArr[] = $FormArr[$i];
}
$FormArr = $tmpFormArr;

$ClassLevelID = ($ClassLevelID == "") ? $FormArr[0][0] : $ClassLevelID;

# Filters - By Form (ClassLevelID)
if (count($FormArr) > 0)
	$FormSelection = $linterface->GET_SELECTION_BOX($FormArr, "name='ClassLevelID' id='ClassLevelID' class='tabletexttoptext' onchange='jCHANGE_RE_EXAM_LIST()'", "", $ClassLevelID);

# Filters - By Type (Term1, Term2, Whole Year, etc)
// Get Semester Type from the reportcard template corresponding to ClassLevelID
$ReportTypeSelection = '';
$ReportTypeArr = array();
$ReportTypeOption = array();
$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);

$ReportType = '';
if(count($ReportTypeArr) > 0){
	$reportCounter = 0;
	for($j=0 ; $j<count($ReportTypeArr) ; $j++){
		if($ReportTypeArr[$j]['ReportID'] == $ReportID){
			$isFormReportType = 1;
			$ReportType = $ReportTypeArr[$j]['Semester'];
		}
		
		if (!$lreportcard->Is_Decide_ReExam_Report($ReportTypeArr[$j]['ReportID']))
			continue;
		
		$ReportTypeOption[$reportCounter][0] = $ReportTypeArr[$j]['ReportID'];
		$ReportTypeOption[$reportCounter][1] = $ReportTypeArr[$j]['SemesterTitle'];
		$reportCounter++;
	}
	$ReportTypeSelection .= $linterface->GET_SELECTION_BOX($ReportTypeOption, 'name="ReportID" id="ReportID" class="tabletexttoptext" onchange="jCHANGE_RE_EXAM_LIST()"', '', $ReportID);
	
	$ReportType = ($ReportType != "") ? $ReportType : $ReportTypeArr[0]['Semester'];
}
$ReportID = ($isFormReportType) ? $ReportID : $ReportTypeOption[0][0];


## Get csv info
$targetFolderPath = $intranet_root."/file/reportcard2008/".$lreportcard->schoolYear."/re_exam";
$targetURLPath = "/file/reportcard2008/".$lreportcard->schoolYear."/re_exam";
$targetFileArr = $lreportcard->GET_CSV_FILE($targetFolderPath);
$csvImg = "<img src='".$PATH_WRT_ROOT."/images/2009a/icon_files/xls.gif' border='0' hspace='5' align='absmiddle' />";


## Construct Info Table
$li = new libfilesystem();
$ClassInfoArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
$numOfClass = count($ClassInfoArr);
$DisplayedClassIDArr = array();

$infoTable = "";
$infoTable .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">";
	$infoTable .= "<tr class=\"tablegreentop tabletopnolink\">";
		$infoTable .= "<td width=\"10%\">".$eReportCard['Class']."</td>";
		$infoTable .= "<td width=\"20%\" align=\"center\">".$eReportCard['ReExamStudentList']."</td>";
		$infoTable .= "<td width=\"20%\" align=\"center\">".$eReportCard['ReExamScore']."</td>";
		$infoTable .= "<td width=\"30%\">".$eReportCard['UploadReExamResult']."</td>";
		$infoTable .= "<td width=\"20%\" align=\"center\">".$eReportCard['LastUploadedDate']."</td>";
	$infoTable .= "</tr>";
	
if (count($FormArr) == 0)
{
	$infoTable .= "<tr class=\"tablegreenrow2\">";
		$infoTable .= "<td class=\"tabletext\" colspan=\"5\" align=\"center\" valign=\"middle\">".$eReportCard['Template']['NoReExamRequired']."</td>";
	$infoTable .= "</tr>";
}
else
{
	for ($i=0; $i<$numOfClass; $i++)
	{
		$thisClassID = $ClassInfoArr[$i]["ClassID"];
		$thisClassName = $ClassInfoArr[$i]["ClassName"];
		
		# if is class teacher, show the teaching class only
		if($ck_ReportCard_UserType=="TEACHER" && $TeachingClassIDArr > 0 && (!in_array($thisClassID, $TeachingClassIDArr)))
			continue;
		
		## check if the re-exam score csv is uploaded
		$targetFileName = $ReportID."_".$thisClassID."_unicode.csv";
		$targetFileNameInLink = $ReportID."_".$thisClassID.".csv";
		
		if (is_array($targetFileArr))
		{
			$thisResult = in_array($targetFileName, $targetFileArr);
		}
		else
		{
			$thisResult = "";
		}
		
		$thisScoreDownloadHTML = "";
		$thisLastUploadedHTML = "";
		if ($thisResult === 0 || $thisResult!="")
		{
			$thisHref = GET_CSV($targetFileNameInLink, $targetFolderPath.'/');
			$thisScoreDownloadHTML .= "<a href='".$thisHref."' target='_blank' class='tablelink'>";
				$thisScoreDownloadHTML .= $csvImg;
				$thisScoreDownloadHTML .= $eReportCard['Download'];
			$thisScoreDownloadHTML .= "</a>";
			
			###### wrong time display now, check next week
			$last_modified_timestamp = filemtime($targetFolderPath."/".$targetFileName);
			$thisLastUploadedHTML = date("Y-m-d H:i:s", $last_modified_timestamp);
		}
		else
		{
			$thisScoreDownloadHTML = "---";
			$thisLastUploadedHTML = "---";
		}
		
		$thisCSS = ($i % 2 == 0)? "tablegreenrow1" : "tablegreenrow2";
		
		$infoTable .= "<tr class=\"".$thisCSS."\">";
			# Class Name
			$infoTable .= "<td>".$thisClassName."</td>";
			# Download Re-exam Student List
			$infoTable .= "<td align=\"center\">";
				$infoTable .= "<a class=\"tablelink\" href=\"javascript:jsGet_ReExam_List_CSV(".$thisClassID.")\">".$csvImg.$eReportCard['Download']."</a>";
			$infoTable .= "</td>";
			# Download Re-exam Score
			$infoTable .= "<td align=\"center\">";
				$infoTable .= $thisScoreDownloadHTML;
			$infoTable .= "</td>";
			# Upload Input
			$infoTable .= "<td>";
				$infoTable .= "<input type=\"file\" id=\"resultCSV_".$thisClassID."\" name=\"resultCSV_".$thisClassID."\" class=\"textboxtext\" size=\"25\" maxlength=\"255\" />";
			$infoTable .= "</td>";
			# Last Modified Date
			$infoTable .= "<td class=\"tabletext\" align=\"center\">".$thisLastUploadedHTML."</td>";
		$infoTable .= "</tr>";
		
		$DisplayedClassIDArr[] = $thisClassID;
	}
}

$infoTable .= "</table>";

## Buttons
if (count($FormArr) > 0)
	$ButtonHTML = $linterface->GET_ACTION_BTN($button_upload, "button", "javascript:jsUpload_Result_CSV()", "btnSubmit");

## SysMsg
$SysMsg = $linterface->GET_SYS_MSG($Result);

?>


<script language="javascript">

/* Global */
<?php

$js_form_var = "var FormArr = new Array(";
if(count($FormArr) > 0){
	for($i=0 ; $i<count($FormArr) ; $i++)
		$js_form_var .= ($i != 0) ? ', "'.$FormArr[$i][0].'"' : '"'.$FormArr[$i][0].'"';
}
$js_form_var .= "); \n";
echo $js_form_var;

$js_classID_var = "var DisplayedClassIDArr = new Array(";
if(count($DisplayedClassIDArr) > 0){
	for($i=0 ; $i<count($DisplayedClassIDArr) ; $i++)
		$js_classID_var .= ($i != 0) ? ', "'.$DisplayedClassIDArr[$i].'"' : '"'.$DisplayedClassIDArr[$i].'"';
}
$js_classID_var .= "); \n";
echo $js_classID_var;

?>

function jGET_SELECTBOX_SELECTED_VALUE(objName){
	var obj = document.getElementById(objName);
	if(obj==null)	return "";
	var index = obj.selectedIndex;
	var val = obj[index].value;
	return val;
}

function jCHANGE_RE_EXAM_LIST()
{
	var form_id = jGET_SELECTBOX_SELECTED_VALUE("ClassLevelID");
	var report_id = jGET_SELECTBOX_SELECTED_VALUE("ReportID");
	
	location.href = "?ClassLevelID="+form_id+"&ReportID="+report_id;
}

function jsGet_ReExam_List_CSV(jsClassID)
{
	document.getElementById("ClassID").value = jsClassID;
	
	var thisForm = document.FormMain;
	thisForm.action = "generate_re_exam_csv.php";
	thisForm.submit();
}

function jsUpload_Result_CSV()
{	
	var i;
	
	var jsAllNull = true;
	for (i=0; i<DisplayedClassIDArr.length; i++)
	{
		var thisClassID = DisplayedClassIDArr[i];
		var thisInputObj = document.getElementById("resultCSV_" + thisClassID);
		
		if (trim(thisInputObj.value) != "") {
			jsAllNull = false;
			break;
		}
	}

	if (jsAllNull)
	{
		alert('<?=$eReportCard['AlertSelectFile']?>');
		document.getElementById("resultCSV_" + DisplayedClassIDArr[0]).focus();
		return false;
	}
	
	var thisForm = document.FormMain;
	thisForm.action = "file_upload.php";
	thisForm.submit();
}

function trim(str) {
	return str.replace(/^\s+|\s+$/g,"");
}

</script>

<br/>
<form name="FormMain" method="post" enctype="multipart/form-data">
	<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><?=$FormSelection?></td>
						<td>&nbsp;</td>
						<td><?=$ReportTypeSelection?></td>
						<td>&nbsp;</td>
						<td align="right" width="100%"><?=$SysMsg?></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		
		<tr><td><?= $infoTable ?></td></tr>
		<tr><td>&nbsp;</td></tr>
		
		<tr>
			<td>
				<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
					<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
					<tr><td align="center"><?=$ButtonHTML?></td></tr>
				</table>
			</td>
		</tr>

	</table>
	
	<input type="hidden" id="ClassID" name="ClassID" />
	
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>