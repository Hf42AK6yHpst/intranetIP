<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

intranet_auth();
intranet_opendb();

# Access right
if (!$plugin['ReportCard2008']) 
{
	echo "You have no priviledge to access this page.";
}
if (($ck_ReportCard_UserType!="TEACHER") && ($ck_ReportCard_UserType!="ADMIN"))
{
	echo "You have no priviledge to access this page.";
}

# Library initialization
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

$fs = new libfilesystem();
	
# Variable Initialization
$URL = "index.php";

$folder_prefix = $intranet_root."/file/reportcard2008/".$lreportcard->schoolYear;

if (!file_exists($folder_prefix))
	$fs->folder_new($folder_prefix);
	
$folder_prefix .= "/re_exam";
$fs->folder_new($folder_prefix);

## Get all Classes
$ClassInfoArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
$numOfClass = count($ClassInfoArr);

//$TeachingClassName = $lreportcard->GET_TEACHING_CLASS();
//$FileClassName = str_replace('.', '', str_replace('/', '', $TeachingClassName));

for ($i=0; $i<$numOfClass; $i++)
{
	$thisClassID = $ClassInfoArr[$i]["ClassID"];
	
	if (${"resultCSV_".$thisClassID} != "")
	{
		$loc = ${"resultCSV_".$thisClassID};
		$filename = ${"resultCSV_".$thisClassID."_name"};
		
		$success[$thisClassID] = 0;
			
		if ($filename=="")
			continue;
		
		$ext = strtolower($fs->file_ext($filename));
		if($ext != ".csv" && $ext != ".txt")
			continue;
			
		if ($loc=="none" || !file_exists($loc))
			continue;
		
		$data = $fs->file_read_csv($loc);
		if (empty($data))
			continue;
		
		if(strpos($filename, ".") != 0) {
			//$filename = $ReportID."_".$thisClassID.substr($filename, strpos($filename, "."));
			$filename = $ReportID."_".$thisClassID."_unicode.csv";
			$success[$thisClassID] = $fs->file_copy($loc, stripslashes($folder_prefix."/".$filename));
		}
	}
}

$URL .= "?Result=update&ClassLevelID=$ClassLevelID&ReportID=$ReportID";
header("Location: $URL");

?>

