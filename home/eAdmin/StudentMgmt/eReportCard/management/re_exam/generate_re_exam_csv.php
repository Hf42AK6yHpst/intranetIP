<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	
	# Library definition
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	}
	else
	{
		$lreportcard = new libreportcard();
	}

	if ($lreportcard->hasAccessRight()) {
		if (!isset($_GET) || !isset($ReportID, $ClassID)) {
			header("Location: index.php");
			exit();
		}
		
		// Class
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		for($i=0 ; $i<count($ClassArr) ;$i++) {
			if($ClassArr[$i][0] == $ClassID)
				$ClassName = $ClassArr[$i][1];
		}
		
		// Student Info Array
		$StudentArr = $lreportcard->GET_STUDENT_BY_CLASS($ClassID);
		$StudentSize = sizeof($StudentArr);
		$StudentIDArr = array();
		$StudentIDList = "";
		for($i=0; $i<$StudentSize; $i++)
			$StudentIDArr[] = $StudentArr[$i][0];
		$StudentIDList = implode(",", $StudentIDArr);
		
		//$table = $lreportcard->DBName.".RC_MARKSHEET_COMMENT";
		$NameField = getNameFieldByLang2("");
		$ClassNumField = getClassNumberField("");
		
		# Get Student Info
		$StudentInfo = $lreportcard->GET_STUDENT_BY_CLASS($ClassID, $StudentIDList, $isShowBothLangs=0, $withClassNumber=0, $isShowStyle=0, $ReturnAsso=0);
		/*
		$sql = "SELECT UserID as StudentID, ClassName, $ClassNumField AS ClassNumber, $NameField AS StudentName 
						FROM INTRANET_USER 
						WHERE UserID IN ($StudentIDList) 
						ORDER BY $ClassNumField
						";
		$StudentInfo = $lreportcard->returnArray($sql,4);
		*/
		
		# retrieve subjects
		$MainSubjectArr = $lreportcard->returnSubjectwOrder($ClassLevelID);
		if(sizeof($MainSubjectArr) > 0)
			foreach($MainSubjectArr as $MainSubjectIDArr[] => $MainSubjectNameArr[]);
			
			
		## Analyst the marks and export to the csv
		$ExportArr = array();
		$ReExamStudentIDArr = array();
		$ReExamStudentInfoArr = array();
		$ReExamStudentFailedSubjectMarkArr = array();
		$ReExamStudent_Counter = 0;
		$numOfStudent = count($StudentInfo);
		for ($i=0; $i<$numOfStudent; $i++)
		{
			$failedSubjectInfoArr = array();
			
			$thisStudentID = $StudentInfo[$i]['UserID'];
			$thisClassName = $StudentInfo[$i]['ClassName'];
			$thisClassNumber = $StudentInfo[$i]['ClassNumber'];
			$thisStudentName = $StudentInfo[$i]['StudentName'];
			//list($thisStudentID, $thisClassName, $thisClassNumber, $thisStudentName) = $StudentInfo[$i];
			
			$failedSubjectInfoArr= $lreportcard->Get_Failed_Subject_InfoArr($ReportID, $thisStudentID, $isAssociative=1);
			
			# All Passed, 4 or more subjects failed, 1 subject less than 40 => need not re-exam
			# 1-3 subjects failed => need re-exam
			$needReExam = (count($failedSubjectInfoArr)==0 || (count($failedSubjectInfoArr) >= 4) 
								|| $lreportcard->Has_Mark_Less_Than_School_Requirement($failedSubjectInfoArr) )? 0 : 1;
								
			if ($needReExam)
			{
				$ReExamStudentIDArr[] = $thisStudentID;
				$ReExamStudentInfoArr[$thisStudentID] = $StudentInfo[$i];
						
				foreach ($failedSubjectInfoArr as $SubjectID => $MarkArr)
				{
					$thisMark = $MarkArr["Mark"];
					
					# consolidate mark in term of Subject
					# so that if no one re-exam this subject, the subject column can be hidden
					$ReExamStudentFailedSubjectMarkArr[$SubjectID][$thisStudentID] = $thisMark;
				}
			}
		}
		
		# Consolidate Subject column (ignore subject which has no one to re-exam)
		$ReExamSubjectInfoArr = array();
		foreach($MainSubjectArr as $SubjectID => $SubjectNameArr)
		{
			if ($ReExamStudentFailedSubjectMarkArr[$SubjectID] != NULL)
			{
				$ReExamSubjectInfoArr[] = array(
												"SubjectID" => $SubjectID,
												"SubjectName" => $SubjectNameArr[0],
												);
			}
		}
		
		
		# Consolidate the export info array
		$ExportArr = array();
		$numOfReExamStudent = count($ReExamStudentIDArr);
		$numOfReExamSubject = count($ReExamSubjectInfoArr);
		for ($i=0; $i<$numOfReExamStudent; $i++)
		{
			$thisStudentID = $ReExamStudentIDArr[$i];
			$thisClassName = $ReExamStudentInfoArr[$thisStudentID]['ClassName'];
			$thisClassNumber = $ReExamStudentInfoArr[$thisStudentID]['ClassNumber'];
			$thisStudentName = $ReExamStudentInfoArr[$thisStudentID]['StudentName'];
			//list($thisStudentID, $thisClassName, $thisClassNumber, $thisStudentName) = $ReExamStudentInfoArr[$thisStudentID];
			
			# student info
			$ExportArr[$i][] = $thisClassName;
			$ExportArr[$i][] = $thisClassNumber;
			$ExportArr[$i][] = $thisStudentName;
			
			# student score info
			for ($j=0; $j<$numOfReExamSubject; $j++)
			{
				$thisSubjectID = $ReExamSubjectInfoArr[$j]["SubjectID"];
				$thisFailedMark = $ReExamStudentFailedSubjectMarkArr[$thisSubjectID][$thisStudentID];
				
				if ($thisFailedMark != "")
					$ExportArr[$i][] = $thisFailedMark;
				else
					$ExportArr[$i][] = "N/A";
			}
		}
		
		$lexport = new libexporttext();
		
		# file name
		$ReportSetting = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		$ReportTitle = $ReportSetting['ReportTitle'];
		$ReportTitle = str_replace(":_:", "_", $ReportTitle);
		$ReportTitle = intranet_htmlspecialchars($ReportTitle);
		$filename = $ClassName."_".$ReportTitle."_"."re-exam_list.csv";
		
		# csv header
		$exportColumn = array("Class Name", "Class Number", "Student Name");
		# add subject names and subjectid as the header
		for ($i=0; $i<$numOfReExamSubject; $i++)
		{
			$thisSubjectID = $ReExamSubjectInfoArr[$i]["SubjectID"];
			$thisSubjectName = $ReExamSubjectInfoArr[$i]["SubjectName"];
			$thisSubjectName = str_replace("<br />", "_", $thisSubjectName);
			$exportColumn[] = $thisSubjectName."_".$thisSubjectID;
		}
		
		$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn);
		
		intranet_closedb();
		
		// Output the file to user browser
		$lexport->EXPORT_FILE($filename, $export_content);
	} else {
		echo "You have no priviledge to access this page.";
	}
} else {
	echo "You have no priviledge to access this page.";
}
?>