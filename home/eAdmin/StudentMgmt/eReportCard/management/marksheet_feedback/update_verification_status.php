<?php
// Modifying by  

/********************************************************
 * Modification log
 * 20170707 Bill [2017-0621-1726-12164]
 * 		- Create File
 * 		- Support Verification Status Update (eRCTemplateSetting['MarksheetVerification']['OverallVerificationStatus'] = true)
 * ******************************************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

# POST Data
$ReportID = IntegerSafe($ReportID);
$StudentID = IntegerSafe($StudentID);
$VStatus = IntegerSafe($VStatus);

# Access Right Checking
if(!$eRCTemplateSetting['MarksheetVerification']['OverallVerificationStatus'] || empty($ReportID) || empty($StudentID)) {
	No_Access_Right_Pop_Up();
}
if ($ck_ReportCard_UserType == "STUDENT") {
	if ($StudentID != $_SESSION['UserID']) {
		No_Access_Right_Pop_Up();
	}
}
else if ($ck_ReportCard_UserType == "PARENT") {
	$lu = new libuser($_SESSION['UserID']);
	$ChildrenList = $lu->getChildrenList();
	$ChlidrenIDAry = Get_Array_By_Key($ChildrenList, 'StudentID');
	
	if (!in_array($StudentID, (array)$ChlidrenIDAry)) {
		No_Access_Right_Pop_Up();
	}
}

# Init
$lreportcard = new libreportcard();
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
}

# Update Verification Status
$StatusArr = array();
$StatusArr[$StudentID] = $VStatus;
$lreportcard->Update_Marksheet_Feedback($ReportID, array(0), $StatusArr);
?>

<html>
<body>
	<form name="form1" action="index.php" method="POST">
		<?=$postValue?>
		<input type="hidden" name="ReportID" value="<?=$ReportID?>"/>
		<input type="hidden" name="StudentID" value="<?=$StudentID?>"/>
	</form>
</body>
</html>

<script>
	document.form1.submit();
</script>