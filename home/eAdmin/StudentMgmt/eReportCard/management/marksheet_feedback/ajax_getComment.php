<?php
// Modifing by 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

intranet_auth();
intranet_opendb();

//header("Content-Type:text/html;charset=utf-8");
$lreportcard = new libreportcardcustom();

$ReportID = IntegerSafe($_POST['ReportID']);
$StudentID = IntegerSafe($_POST['StudentID']);
$SubjectID = IntegerSafe($_POST['SubjectID']);

### Get Subject Teacher Comment
$SubjectTeacherCommentArr = $lreportcard->returnSubjectTeacherComment($ReportID, $StudentID);
$CommentStr = $SubjectTeacherCommentArr[$SubjectID];

echo $CommentStr;

intranet_closedb();

?>