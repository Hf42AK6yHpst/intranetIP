<?php
// Modifying by  

/********************************************************
 * Modification log
 * 20191227 Philips
 * 		- Display Full Mark Column
 * 20190828 Bill
 *      - fixed: cannot load report content through curl  ($eRCTemplateSetting['Management']['MarksheetVerification']['ShowStudentResultSummary'])
 * 20171201 Bill
 * 		- Changed to Print Archived Report (Improved: Checking & Redirect Target) 
 * 20170707 Bill [2017-0621-1726-12164]
 * 		- Support Verification Status Update (eRCTemplateSetting['MarksheetVerification']['OverallVerificationStatus'] = true)
 * 20170621 Bill [2017-0621-1726-12164]
 * 		- Support Absent Student (escola pui ching)
 * 20170508 Bill [2017-0508-1128-10164]
 * 		- Display correct Year Report Column Score when $eRCTemplateSetting['Settings']['TemplateSettings_ConsolidatedReportWithTermReportAssessment'] = true
 * 20170410 Bill:	[2017-0407-1642-14164]
 * 		- Support Print Report - Student (escola pui ching)
 * 20170322 Bill:	[2017-0109-1818-40164]
 * 		- Display Promotion Status for Year Report (within Parent & Student Verification and Preview Report Card Period)
 * 		- Display Print Button (within Report Publishing Period)
 * 20161117 Bill:	[2015-1104-1130-08164]
 * 		- Show print button to parents for generated reports
 * 		- fixed: cannot retrieve correct report if select this student from another class level (Parent)
 * 20120326 Marcus:
 * 		- Cater allow to set time for submission, verification
 * 		- 2012-0112-1622-04073 - 迦密愛禮信中學 - Schedule of Report Card Management 
 * ******************************************************/

$PageRight = array("TEACHER", "STUDENT", "PARENT");

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

# Initilization
$ReportID = ($_GET['ReportID'])? IntegerSafe($_GET['ReportID']) : '';
$SubjectID = ($_GET['SubjectID'])? IntegerSafe($_GET['SubjectID']) : '';
$ClassLevelID = ($_GET['ClassLevelID'])? IntegerSafe($_GET['ClassLevelID']) : '';

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcard();
$linterface = new interface_html();

if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
}
else
{
	$lreportcard = new libreportcard();
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
$lreportcard = new libreportcard2008j();

$CurrentPage = ($ck_ReportCard_UserType == "STUDENT" || $ck_ReportCard_UserType == "PARENT")? "Management_VerifyAssessmentResult" : "Management_MarkSheetRevision";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

if ($lreportcard->hasAccessRight())
{
	$linterface = new interface_html();
	
	### Get ClassLevelID, StudentIDArr for different view group
	$TableTileArr = array();
	if ($ck_ReportCard_UserType == "STUDENT")
	{
		//$StudentInfoArr = $lreportcard->Get_Student_Class_ClassLevel_Info($_SESSION['UserID']);
		//$ClassLevelID = $StudentInfoArr[0]['ClassLevelID'];
		
		$StudentID = $_SESSION['UserID'];
		$ChildrenInputField = "<input type='hidden' name='StudentID' id='StudentID' value='$StudentID'>";
		
		$TableTileArr[] = array( "Title" => $eReportCard['Subject'], "Width" => "");
	}
	else if ($ck_ReportCard_UserType == "PARENT")
	{
		$luser = new libuser($_SESSION['UserID']);
		$ChildrenList = $luser->getChildrenList();
		
		if ($StudentID!='' && !in_array($StudentID, Get_Array_By_Key($ChildrenList, 'StudentID'))) {
			No_Access_Right_Pop_Up();
		}
		
		$StudentID = $StudentID? $StudentID : $ChildrenList[0]['StudentID'];
		if(!empty($ChildrenList)) {
			$ChildrenSelection = getSelectByArray($ChildrenList, " name='StudentID' id='StudentID' onchange=\"js_Reload()\"'", $StudentID, "", 1);
		}
	}
	
	$StudentInfoArr = $lreportcard->Get_Student_Class_ClassLevel_Info($StudentID);
	$ClassLevelID = $StudentInfoArr[0]['ClassLevelID'];
	//$ClassID = $StudentInfoArr[0]['ClassID'];
	
	### Get Reports Info
	# Preset the selected report as the current submission/verification report template if not specified
	if ($ReportID == '')
	{
		$Period_Conds = ' AND NOW() BETWEEN MarksheetVerificationStart AND MarksheetVerificationEnd ';
		$conds = " 	ClassLevelID = '$ClassLevelID' 
					$Period_Conds
				";
		$ReportInfoArr = $lreportcard->returnReportTemplateBasicInfo('', $conds);
		$ReportID = ($ReportInfoArr['ReportID'])? $ReportInfoArr['ReportID'] : '';
		
		# If no report in verification Period => use the 1st one
		if ($ReportID == '')
		{
			$ReportInfoArr = $lreportcard->Get_Report_List($ClassLevelID);
			$ReportID = $ReportInfoArr[0]['ReportID'];
		}
	}
	else
	{
		// Get Class Level Report
		$ReportInfoArr = $lreportcard->Get_Report_List($ClassLevelID);
		$ReportIDArr = Get_Array_By_Key((array)$ReportInfoArr, "ReportID");
		
		// if reportid not within report list => use the 1st one
		if(!in_array($ReportID, $ReportIDArr))
		{
			$ReportID = $ReportInfoArr[0]['ReportID'];
		}
	}
	$ReportSelection = $lreportcard->Get_Report_Selection($ClassLevelID, $ReportID, 'ReportID', 'js_Reload();', $ForVerification=1);
	
	
	### Get the Marksheet Verification Period
	$scheduleEntries = $lreportcard->GET_REPORT_DATE_PERIOD("", $ReportID);
	$VerificationStart = ($scheduleEntries[0]["VerStart"]=="") ? "-" : date("Y-m-d H:i:s",$scheduleEntries[0]["VerStart"]);
	$VerificationEnd = ($scheduleEntries[0]["VerEnd"]=="") ? "-" : date("Y-m-d H:i:s",$scheduleEntries[0]["VerEnd"]);
	
	
	### Get Student Subject List
	$StudentSubjectIDArr = $lreportcard->Get_Student_Studying_SubjectID($ReportID, $StudentID, 1);
	
	
	### Get Subject Selection
	$SubjectSelection = $lreportcard->Get_Subject_Selection($ClassLevelID, $SubjectID, 'SubjectID', 'js_Reload();');
	
	
	### Get Marks for Display
	if ($ck_ReportCard_UserType == "STUDENT" || $ck_ReportCard_UserType == "PARENT")
	{
		$ColumnArr = $lreportcard->returnReportTemplateColumnData($ReportID);
		$ColumnTitleArr = $lreportcard->returnReportColoumnTitle($ReportID);
		$numOfColumn = count($ColumnArr);
		
		// $StudentMarkArr[#StudentID][#SubjectID][#ReportColumnID] = Mark
		
		### Get Studying Subject
		$SubjectIDList = $lreportcard->Get_Student_Studying_SubjectID($ReportID, $StudentID);
		
		$StudentMarkArr = array();
		
		# 20191227 (Philips) - Display Full Mark
		$FullMarkAry = array();
			
			$thisReportColumnID = $ColumnArr[$i]['ReportColumnID'];

			###################################################################################################### 
			#	 Get MarkSheet Score
			#
				# Get MarkSheet Info
				// Tags of MarkSheet(0: Raw Mark, 1: Weighted Mark, 2: Converted Grade
				$TagsType = (isset($TagsType) && $TagsType != "") ? $TagsType : 0;
		
				// Period (Term x, Whole Year, etc)
				$PeriodArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID, $ReportID);
				$PeriodName = (count($PeriodArr) > 0) ? $PeriodArr['SemesterTitle'] : '';
				
				# Load Settings - Calculation
				// Get the Calculation Method of Report Type - 1: Right-Down,  2: Down-Right
				$SettingCategory = 'Calculation';
				$categorySettings = $lreportcard->LOAD_SETTING($SettingCategory);
				$TermCalculationType = $categorySettings['OrderTerm'];
				$FullYearCalculationType = $categorySettings['OrderFullYear'];
				
				# Load Settings - Storage&Display
				$SettingStorDisp = 'Storage&Display';
				$storDispSettings = $lreportcard->LOAD_SETTING($SettingStorDisp);
				
				// Get Absent&Exempt Settings
				$SettingAbsentExempt = 'Absent&Exempt';
				$absentExemptSettings = $lreportcard->LOAD_SETTING($SettingAbsentExempt);
				
				if(count($PeriodArr) > 0 && $PeriodArr['Semester'] == "F")
					$usePercentage = ($FullYearCalculationType == 1) ? 1 : 0;
				else 
					$usePercentage = ($TermCalculationType == 1) ? 1 : 0;
		
			foreach($SubjectIDList as $SubjectID)
			{

				# Get component subject(s) if any
				//$isEdit = 1;					// default "1" as the marksheet is editable
				$CmpSubjectArr = array();
				$isCalculateByCmpSub = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
				$isCmpSubject = $lreportcard->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($SubjectID);
				if(!$isCmpSubject){
					$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
					
					$CmpSubjectIDArr = array();
					if(!empty($CmpSubjectArr)){	
						for($x=0 ; $x<count($CmpSubjectArr) ; $x++){
							$CmpSubjectIDArr[] = $CmpSubjectArr[$x]['SubjectID'];
							
							$CmpSubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $CmpSubjectArr[$x]['SubjectID'],0,0,$ReportID);
							if(count($CmpSubjectFormGradingArr) > 0 && $CmpSubjectFormGradingArr['ScaleInput'] == "M")
								$isCalculateByCmpSub = 1;
						}
					}
					// Check whether the marksheet is editable or not 
					//$isEdit = $lreportcard->CHECK_MARKSHEET_STATUS($SubjectID, $ClassLevelID, $ReportID, $CmpSubjectIDArr);
				}
				
				
				//Initialization of Grading Scheme
				$SchemeID = '';
				$ScaleInput = "M";		// M: Mark, G: Grade
				$ScaleDisplay = "M";	// M: Mark, G: Grade
				$SchemeType = "H";		// H: Honor Based, PF: PassFail
				$SchemeInfo = array();
				$SchemeMainInfo = array();
				$SchemeMarkInfo = array();
				$SubjectFormGradingArr = array();
				$possibleSpeicalCaseSet1 = array();
				$possibleSpeicalCaseSet2 = array();
				
				$SubjectFormGradingArr = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0,0,$ReportID);
				# Check if no any scheme is assigned to this Form(ClassLevelID) of this subject, redirect
				//if(count($SubjectFormGradingArr) > 0){
				//	if($SubjectFormGradingArr['SchemeID'] == "" || $SubjectFormGradingArr['SchemeID'] == null)
				//	{
				//		header("Location: marksheet_revision.php?ClassLevelID=$ClassLevelID&ReportID=$ReportID&SubjectID=$SubjectID&msg=1");
				//	}
				//}
				
				# Get Grading Scheme Info of that subject
				if(count($SubjectFormGradingArr) > 0){
					$SchemeID = $SubjectFormGradingArr['SchemeID'];
					$ScaleInput = $SubjectFormGradingArr['ScaleInput'];
					$ScaleDisplay = $SubjectFormGradingArr['ScaleDisplay'];
					
					$SchemeInfo = $lreportcard->GET_GRADING_SCHEME_INFO($SchemeID);
					$SchemeMainInfo = $SchemeInfo[0];
					$SchemeMarkInfo = $SchemeInfo[1];
					$SchemeType = $SchemeMainInfo['SchemeType'];
					# 20191227 (Philips) - Get Scheme Full Mark
					$SchemeFullMark = $SchemeMainInfo['FullMark'];
					$FullMarkAry[$SubjectID] = ($ScaleDisplay == 'M') ? $SchemeFullMark : $SchemeMarkInfo[0]['Grade'];
				}	
		
				$possibleSpeicalCaseSet1 = $lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
					
				if($SchemeType == "H" && $ScaleInput == "G"){	// Prepare Selection Box using Grade as Input
					$inputTmpOption = array();
					$inputGradeOption = array();
					$SchemeGrade = $lreportcard->GET_GRADE_FROM_GRADING_SCHEME_RANGE($SchemeID);
					$inputGradeOption[0] = array('', '--');
					if(!empty($SchemeGrade)){
						foreach($SchemeGrade as $GradeRangeID => $grade)
							$inputGradeOption[] = array($GradeRangeID, $grade);
					}
					
					for($k=0 ; $k<count($possibleSpeicalCaseSet1) ; $k++){
						//$inputGradeOption[] = array($possibleSpeicalCaseSet1[$k], $lreportcard->GET_MARKSHEET_SPECIAL_CASE_SET1_STRING($possibleSpeicalCaseSet1[$k]));
						$inputGradeOption[] = array($possibleSpeicalCaseSet1[$k], $possibleSpeicalCaseSet1[$k]);
					}
				}
				else if($SchemeType == "PF"){			// Prepare Selection Box using Pass/Fail as Input
					$SchemePassFail = array();
					$SchemePassFail['P'] = $SchemeMainInfo['Pass'];
					$SchemePassFail['F'] = $SchemeMainInfo['Fail'];
				
					$inputPFOption = array();
					$inputPFOption[0] = array('', '--');
					$inputPFOption[1] = array('P', $SchemeMainInfo['Pass']);
					$inputPFOption[2] = array('F', $SchemeMainInfo['Fail']);
					
					$possibleSpeicalCaseSet2 = $lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2();
					for($k=0 ; $k<count($possibleSpeicalCaseSet2) ; $k++){
						//$inputPFOption[] = array($possibleSpeicalCaseSet2[$k], $lreportcard->GET_MARKSHEET_SPECIAL_CASE_SET2_STRING($possibleSpeicalCaseSet2[$k]));
						$inputPFOption[] = array($possibleSpeicalCaseSet2[$k], $possibleSpeicalCaseSet2[$k]);
					}
				}
					
				# Get Data Either Assessment Column(s) in Term Or Term Column(s) in Wholely Report	
				// Loading Report Template Data
				$basic_data = $lreportcard->returnReportTemplateBasicInfo($ReportID);			// Basic  Data
				$column_data = $lreportcard->returnReportTemplateColumnData($ReportID); 		// Column Data
				$ColumnSize = sizeof($column_data);
				$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
				$ReportTitle = $basic_data['ReportTitle'];
				$ReportType = $basic_data['Semester'];
				$isPercentage = $basic_data['PercentageOnColumnWeight'];
								
				# Get Existing Report Calculation Method
				$CalculationType = ($ReportType == "F") ? $FullYearCalculationType : $TermCalculationType;
				
				# Get Report Column Weight
				$WeightAry = array();
				if($CalculationType == 2){
					if(count($column_data) > 0){
				    	for($x=0 ; $x<sizeof($column_data) ; $x++){
							$OtherCondition = "SubjectID IS NULL AND ReportColumnID = ".$column_data[$x]['ReportColumnID'];
							$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
							
							$WeightAry[$weight_data[0]['ReportColumnID']][$SubjectID] = $weight_data[0]['Weight'];
						}
					}
				}
				else {
					$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID);  
					
					for($x=0 ; $x<sizeof($weight_data) ; $x++)
						$WeightAry[$weight_data[$x]['ReportColumnID']][$weight_data[$x]['SubjectID']] = $weight_data[$x]['Weight'];
				}		
				
				### Check if all Column Weight Zero
				$isAllColumnWeightZero = true;
				foreach((array)$WeightAry as $thisReportColumnID => $thisArr)
				{
					if ($thisArr[$SubjectID] != 0 && $thisReportColumnID != '')
					{
						$isAllColumnWeightZero = false;
					}
				}
				
				if ($isAllColumnWeightZero == true && $SchemeType == "H" && $ScaleInput == "M")
					$displayOverallColumnDueToAllZeroWeight = true;
				else
					$displayOverallColumnDueToAllZeroWeight = false;
				
				
				$StudentIDArr = array($StudentID);
				
				# Get MarkSheet OVERALL Score Info for the cases when it is (Term Report OR Whole Year Report) AND 
				# when the scheme type is Honor-Based (H) AND its scaleinput is Grade(G) OR
				# when the scheme type is PassFail-Based (PF)
				$MarksheetOverallScoreInfoArr = array();
				
				if(($TagsType == 0 || $TagsType == 2) && (($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF") ){
					$MarksheetOverallScoreInfoArr = $lreportcard->GET_MARKSHEET_OVERALL_SCORE($StudentIDArr, $SubjectID, $ReportID);
				}
				
				
				##############################################################################################
				# General Approach dealing with Search Term Result From Whole Year Report not From Term Report
				# Can deal with Customization of SIS dealing with secondary template 
				$isAllWholeReport = 0;
				if($ReportType == "F"){
					$ReportTypeArr = array();
					$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);
					if(count($ReportTypeArr) > 0){
						$flag = 0;
						for($j=0 ; $j<count($ReportTypeArr) ; $j++){
							if($ReportTypeArr[$j]['ReportID'] != $ReportID){
								if($ReportTypeArr[$j]['Semester'] != "F")	$flag = 1;
							}
						}
						if($flag == 0) $isAllWholeReport = 1;
					}
				}
				
				# Get the Corresponding Whole Year ReportID from 
				# the ReportColumnID of the Current Whole Year Report 
				$WholeYearColumnHasTemplateArr = array();
				if($isAllWholeReport == 1){
					if($ReportType == "F" && $ColumnSize > 0){
						for($j=0 ; $j<$ColumnSize ; $j++){
							$ReportColumnID = $column_data[$j]['ReportColumnID'];
							# Return Whole Year Report Template ID - ReportID 
							$WholeYearColumnHasTemplateArr[$ReportColumnID] = $lreportcard->CHECK_WHOLE_YEAR_REPORT_TEMPLATE_FROM_COLUMN($ReportID, $column_data[$j]['SemesterNum'], $ClassLevelID);
							
							# Check Whether the Return WholeYear ReportID is not greater the Current ReportID
							# if User creates All Reports sequently
							if($WholeYearColumnHasTemplateArr[$ReportColumnID] != false){
								if($WholeYearColumnHasTemplateArr[$ReportColumnID] > $ReportID)
									$WholeYearColumnHasTemplateArr[$ReportColumnID] = false;
							}
						}
					}
				}
				####################################################################################################
				
				
				# Get MarkSheet Score Info / ParentSubMarksheetScoreInfoArr	
				$MarksheetScoreInfoArr = array();
				$ColumnHasTemplateAry = array();
				$WeightedMark = array();
				$hasDirectInputMark = 0;
				
				if(count($column_data) > 0){
				    for($x=0 ; $x<sizeof($column_data) ; $x++){
				    	$thisReportColumnID = $column_data[$x]['ReportColumnID'];
				    	
				    	if ($x==0)
				    		$orignalIsCalculateByCmpSub = $isCalculateByCmpSub;
				    		
				    	$isAllCmpZeroWeightArr = $lreportcard->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($ReportID, $SubjectID);
				    	if ($isAllCmpZeroWeightArr[$thisReportColumnID])
				    	{
							$isCalculateByCmpSub = 0;
				    	}
						else
						{
							$isCalculateByCmpSub = $orignalIsCalculateByCmpSub;
				    	}
				    	$isCalculateByCmpSubArr[$SubjectID][$thisReportColumnID] = $isCalculateByCmpSub;
						
					if($isAllWholeReport == 1 && $WholeYearColumnHasTemplateArr[$column_data[$x]['ReportColumnID']] != false){
						#######################################################################################################################################
						# for the Whole Year Report which defined any Term Column in other Whole Year Report	added on 22 May by Jason
						// have defined term report template 
						$WholeYearReportID = $WholeYearColumnHasTemplateArr[$column_data[$x]['ReportColumnID']];
						$WholeYearReportColumnData = $lreportcard->returnReportTemplateColumnData($WholeYearReportID); 		// Column Data
						$WholeYearReportColumnWeightData = $lreportcard->returnReportTemplateSubjectWeightData($WholeYearReportID);  
						
						
						if(count($WholeYearReportColumnWeightData) > 0){
							for($j=0 ; $j<sizeof($WholeYearReportColumnWeightData) ; $j++){
								if($WholeYearReportColumnWeightData[$j]['SubjectID'] == $SubjectID && $WholeYearReportColumnWeightData[$j]['ReportColumnID'] != ""){
									$Weight[$WholeYearReportColumnWeightData[$j]['ReportColumnID']] = $WholeYearReportColumnWeightData[$j]['Weight'];
								}
							}
						}
						
						if(count($WholeYearReportColumnData) > 0){
							for($j=0 ; $j<sizeof($WholeYearReportColumnData) ; $j++){
								if($WholeYearReportColumnData[$j]['SemesterNum'] == $column_data[$x]['SemesterNum']){
									if($Weight[$WholeYearReportColumnData[$j]['ReportColumnID']] != 0){
										$MarksheetScoreInfoArr[$column_data[$x]['ReportColumnID']] = $lreportcard->GET_MARKSHEET_SCORE($StudentIDArr, $SubjectID, $WholeYearReportColumnData[$j]['ReportColumnID']);
									} else {
										$MarksheetScoreInfoArr[$column_data[$x]['ReportColumnID']] = '--';
									}
								}
							}
						}
						#######################################################################################################################################
					  } else {
					    # for the general Term Report &
					    # for the Whole Year Report which does not define any Term Column in other Whole Year Report
					    /* 
						* Check for the Mark Column is allowed to fill in mark when any term does not have related report template 
						* by SemesterNum (ReportType) && ClassLevelID
						* An array is initialized as a control variable [$ColumnHasTemplateAry]
						* A variable is initialized as a control variable [$hasDirectInputMark], must be used with [$ReportType == "F"]
						* to control the Import function to be used or not
						*/
						if($ReportType == "F"){
							$ColumnHasTemplateAry[$column_data[$x]['ReportColumnID']] = $lreportcard->CHECK_REPORT_TEMPLATE_FROM_COLUMN($column_data[$x]['SemesterNum'], $ClassLevelID);
							if(!$ColumnHasTemplateAry[$column_data[$x]['ReportColumnID']])
								$hasDirectInputMark = 1;
						} else {
							$ColumnHasTemplateAry[$column_data[$x]['ReportColumnID']] = 1;
						}
					    
						
					    # Get Data from Subject without Component Subjects OR Subject having Component Subjects with ScaleInput is Not Marked
					    if( count($CmpSubjectArr) == 0 || 
					    	(count($CmpSubjectArr) > 0 && !$isCalculateByCmpSub) || 
					    	(count($CmpSubjectArr) >0 && $isCalculateByCmpSub && (($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF")) ){
							$MarksheetScoreInfoArr[$column_data[$x]['ReportColumnID']] = $lreportcard->GET_MARKSHEET_SCORE($StudentIDArr, $SubjectID, $column_data[$x]['ReportColumnID']);
						} else {	
							# Conidtion: Subject contains Component Subjects & Mark is calculated by its components subjects
							// Get Parent Subject Marksheet Score by Marksheet Score of Component Subjects if any
							if($ReportType == "F" && !$ColumnHasTemplateAry[$column_data[$x]['ReportColumnID']]){
								
								// for whole year report & it does not have term report template defined 
								$ParentSubMarksheetScoreInfoArr[$column_data[$x]['ReportColumnID']] = $lreportcard->GET_STUDENT_TERM_SUBJECT_MARK_WITHOUT_TEMPLATE($StudentIDArr, $ReportID, $column_data[$x]['ReportColumnID'], $SubjectID, $CmpSubjectIDArr);
							} else {						
								// for term report use only
								$ParentSubMarksheetScoreInfoArr[$column_data[$x]['ReportColumnID']] = $lreportcard->GET_PARENT_SUBJECT_MARKSHEET_SCORE($StudentIDArr, $SubjectID, $CmpSubjectIDArr, $ReportID, $column_data[$x]['ReportColumnID'], "Term", 0, 0, $checkSpecialFullMark=false);
							}
						}
		
						# For Whole Year Report use only when Case i
						if($ReportType == "F"){
							# Case 1: Get the marksheet OVERALL score when the inputscale is Grade(G) OR PassFail(PF)
							if(($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF"){
								if($ColumnHasTemplateAry[$column_data[$x]['ReportColumnID']]){
									$TermReportID = $ColumnHasTemplateAry[$column_data[$x]['ReportColumnID']];
									$MarksheetOverallScoreInfoArr[$column_data[$x]['ReportColumnID']] = $lreportcard->GET_MARKSHEET_OVERALL_SCORE($StudentIDArr, $SubjectID, $TermReportID);
								}
							}
							else if($SchemeType == "H" && $ScaleInput == "M"){
								# Case 2: Calculating The Weighted Mark of Parent Subjects if having Component Subjects
								if($ColumnHasTemplateAry[$column_data[$x]['ReportColumnID']]){	
									// have defined term report template 
									$TermReportID = $ColumnHasTemplateAry[$column_data[$x]['ReportColumnID']];
									// [2017-0508-1128-10164]
									$TermReportColumnID = $column_data[$x]['TermReportColumnID'];
									$excludedTermReportColumnWeight = $eRCTemplateSetting['Settings']['TemplateSettings_ConsolidatedReportWithTermReportAssessment_ExcludeWeightDisplay'];
									
									if(!$isCalculateByCmpSub){
										# Get Student Term Subject Mark of common subject without any component subjects
										$WeightedMark[$column_data[$x]['ReportColumnID']] = $lreportcard->GET_STUDENT_TERM_SUBJECT_MARK($StudentIDArr, $TermReportID, $SubjectID, $CmpSubjectIDArr, 1, $ReportID, $TermReportColumnID, $excludedTermReportColumnWeight);
									}
									else {
										# Get Student Term Subject Mark of a subject by manipulating data of its component subjects
										$WeightedMark[$column_data[$x]['ReportColumnID']] = $lreportcard->GET_STUDENT_TERM_SUBJECT_MARK($StudentIDArr, $TermReportID, $SubjectID, $CmpSubjectIDArr, $TermCalculationType, $ReportID, $TermReportColumnID, $excludedTermReportColumnWeight);
									}
								}
								else {
									# does not have term report Template for whole year report &
									# subject contains component subjects
									if($isCalculateByCmpSub){
										$ParentSubMarksheetScoreInfoArr[$column_data[$x]['ReportColumnID']] = $lreportcard->GET_STUDENT_TERM_SUBJECT_MARK_WITHOUT_TEMPLATE($StudentIDArr, $ReportID, $column_data[$x]['ReportColumnID'], $SubjectID, $CmpSubjectIDArr);
										$hasDirectInputMark = 0;
									}
								}
							}
						}
				  	  } # End of if($isAllWholeReport == 0)
					}
				}
				# Get Overall Term Subject Mark of a subject having Component Subjects when 
				# Conition: TagsType is 1 - "Preview Mark" & it is Term Report 
				$OverallTermSubjectWeightedMark = array();
				if(($SchemeType != "PF" && $ScaleDisplay == "M" && $ScaleInput != "G") && $ReportType != "F" && !empty($CmpSubjectIDArr) ){
					$OverallTermSubjectWeightedMark = $lreportcard->GET_STUDENT_TERM_SUBJECT_MARK($StudentIDArr, $ReportID, $SubjectID, $CmpSubjectIDArr, $TermCalculationType);
				}
		
			    # Student Content of Marksheet Revision
			    # $OverallSubjectMark is used as a reference for user 
			    # $isShowOverallMark is used as a control to display term/overall subject mark
			  		/*
					$luser = new libuser($StudentID);
					$WebSAMSRegNo = $luser->WebSamsRegNo;
					$StudentNo = $luser->ClassNumber;
					$StudentName = $luser->UserName;
					$ClassName = $luser->ClassName;

					$tr_css = ($j % 2 == 1) ? "tablegreenrow2" : "tablegreenrow1";
					//$td_css = ($j % 2 == 1) ? "attendanceouting" : "attendancepresent";
					
					if ($showClassName)
						$thisClassDisplay = $ClassName.'-'.$StudentNo;
					else
						$thisClassDisplay = $StudentNo;
					*/
			        if(count($column_data) > 0){
				        $OverallSubjectMark = 0;
				        $isShowOverallMark = 1;
				        
				        # Check for Combination of Special Case of Input Mark / Term Mark
				        $SpecialCaseArr = $lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
				        foreach($SpecialCaseArr as $key){
				        	$SpecialCaseCountArr[$key] = array("Count"=>0, "Value"=>'', "isCount"=>'');
			        	}
			        	
			        	# Prepare an Array for converting mark to weighted mark 
						# AllColumnMarkArr is refer the all Column Marks of each student
						if($SchemeType != "PF" && $ScaleDisplay == "M" && $ScaleInput != "G"){
							$AllColumnMarkArr = array();
							for($r=0 ;$r<$ColumnSize ; $r++){
								
								$thisReportColumnID = $column_data[$r]['ReportColumnID'];
								$isCalculateByCmpSub = $isCalculateByCmpSubArr[$SubjectID][$thisReportColumnID];
								
								if( $WeightAry[$column_data[$r]['ReportColumnID']][$SubjectID] != 0 && 
									$WeightAry[$column_data[$r]['ReportColumnID']][$SubjectID] != null && 
									$WeightAry[$column_data[$r]['ReportColumnID']][$SubjectID] != "" ){
										
									if($isAllWholeReport == 1 && $WholeYearColumnHasTemplateArr[$column_data[$x]['ReportColumnID']] != false){
										// new approach 
									} else {
										if($ReportType == "F" && $ColumnHasTemplateAry[$column_data[$r]['ReportColumnID']] && $ScaleInput == "M"){	// whole year with term template
											if(!empty($CmpSubjectArr) && $isCalculateByCmpSub){		// Subject with component Subjects
												$AllColumnMarkArr[$column_data[$r]['ReportColumnID']] = $WeightedMark[$column_data[$r]['ReportColumnID']][$StudentID];
											} else {	// subject without component subject
												$AllColumnMarkArr[$column_data[$r]['ReportColumnID']] = $WeightedMark[$column_data[$r]['ReportColumnID']][$StudentID];
											}
										} else {	// term report OR whole year without term template
											if(!empty($CmpSubjectArr) && $isCalculateByCmpSub){		// Subject with component Subjects
												$AllColumnMarkArr[$column_data[$r]['ReportColumnID']] = $ParentSubMarksheetScoreInfoArr[$column_data[$r]['ReportColumnID']][$StudentID];
											} else {	// subject without component subject
												if($MarksheetScoreInfoArr[$column_data[$r]['ReportColumnID']][$StudentID]['MarkType'] == "SC")
									    			$AllColumnMarkArr[$column_data[$r]['ReportColumnID']] = $MarksheetScoreInfoArr[$column_data[$r]['ReportColumnID']][$StudentID]['MarkNonNum'];
									    		else 
													$AllColumnMarkArr[$column_data[$r]['ReportColumnID']] = $MarksheetScoreInfoArr[$column_data[$r]['ReportColumnID']][$StudentID]['MarkRaw'];
											}
										}
									} # End if($isAllWholeReport)
						    		
						    		
						    	}
						    } # End Report Column Loop				
		
						}
						
						
						# Loop Report Column
					    for($x=0 ; $x<sizeof($column_data) ; $x++){
						    list($ReportColumnID, $ColumnTitle, $DefaultWeight, $SemesterNum) = $column_data[$x];
						    
						    $isCalculateByCmpSub = $isCalculateByCmpSubArr[$SubjectID][$ReportColumnID];
						    $inputMethod = '';
						    
						    if($isAllWholeReport == 1 && $WholeYearColumnHasTemplateArr[$column_data[$x]['ReportColumnID']] != false){
						    	
							    #############################################################################
							    # New Approach dealing with only having Whole Year Report
							    #############################################################################

								if($SchemeType != "PF" && $ScaleDisplay == "M" && $ScaleInput != "G"){
									if($WeightAry[$ReportColumnID][$SubjectID] != 0 && $WeightAry[$ReportColumnID][$SubjectID] != null && $WeightAry[$ReportColumnID][$SubjectID] != ""){
										$MarkType = (count($MarksheetScoreInfoArr) > 0) ? $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkType'] : '';
										if(count($MarksheetScoreInfoArr) > 0){
										  	if($MarkType == "SC"){
										  		$tmpRaw = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkNonNum'];
										  	} else {
										  		$tmpRaw = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkRaw'];
										  	}
												if($tmpRaw == "-" || $tmpRaw == "*" || $tmpRaw == "/" || $tmpRaw == "N.A."){
									    		$inputMethod = $tmpRaw;
								    		} else if($tmpRaw != "NOTCOMPLETED"){
									    		# for displaying mark with weight calculation
											    //$tmpWeightedVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpRaw, $CalculationType);
											    ### 20100322 Ivan - Display Raw Mark instead of weighted mark
										    	$tmpWeightedVal = $tmpRaw;
										    				
								    			//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpWeightedVal, $storDispSettings["SubjectScore"]) : round($tmpWeightedVal, 0);
								    			$roundVal = $lreportcard->ROUND_MARK($tmpWeightedVal, "SubjectScore");
								    			$inputMethod = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $storDispSettings["SubjectScore"]);
								    			
								    			# for calculating real overall subject mark
								    			$tmpWeightedVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpRaw, $CalculationType, $AllColumnMarkArr);
								    			$OverallSubjectMark += $tmpWeightedVal;
							    			} else {
								    			$isShowOverallMark = 0;
							    			}
							    		}
									} else {
										$inputMethod = '--';
									}
									
								} else if($SchemeType != "PF" && $ScaleDisplay == "G"){
									if($WeightAry[$ReportColumnID][$SubjectID] != 0 && $WeightAry[$ReportColumnID][$SubjectID] != null && $WeightAry[$ReportColumnID][$SubjectID] != ""){
										$MarkType = (count($MarksheetScoreInfoArr) > 0) ? $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkType'] : '';
										if(count($MarksheetScoreInfoArr) > 0){
								    		$GradeVal = '';
								    		if($MarkType == "SC"){
								    			$displayVal = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkNonNum'];
								    		} else if($MarkType == "G"){
								    			 $displayVal = $SchemeGrade[$MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkNonNum']];	
								    		} else if($MarkType == "M"){
								    			$tmpRaw = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkRaw'];
								    			$displayVal = ($tmpRaw < 0 || $tmpRaw == "") ? "" : $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $tmpRaw);
								    		}						    	
										    $inputMethod = $displayVal;
									    }
									} else {
										$inputMethod = '--';
									}
									
								}
							} else {
								
								#############################################################################
								# Original Approach dealing with having Term Report & Whole Year Report
								############################################################################# ";
							    // Mark Type : M-Mark, G-Grade, PF-PassFail, SC-SpecialCase
								$MarkType = (count($MarksheetScoreInfoArr) > 0) ? $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkType'] : '';
								
								if($SchemeType != "PF" && $ScaleDisplay == "M" && $ScaleInput != "G"){	# View of Weighted Mark
						    		if($WeightAry[$ReportColumnID][$SubjectID] != 0 && $WeightAry[$ReportColumnID][$SubjectID] != null && $WeightAry[$ReportColumnID][$SubjectID] != ""){
									    if($ColumnHasTemplateAry[$ReportColumnID] && $ReportType == "F" && $ScaleInput == "M"){
									    				
										    if(!empty($CmpSubjectArr) && $isCalculateByCmpSub){	// case for generate the mark of parent subject from component subjects
												$tmpRaw = $WeightedMark[$ReportColumnID][$StudentID];
										    					    
											    if($tmpRaw == "-" || $tmpRaw == "*" || $tmpRaw == "/" || $tmpRaw == "N.A."){
										    		$inputMethod = $tmpRaw;
									    		} else if($tmpRaw != "NOTCOMPLETED"){
									    			# for displaying mark with weight calculation
												    //$tmpWeightedVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpRaw, $CalculationType);
												    ### 20100322 Ivan - Display Raw Mark instead of weighted mark
										    		$tmpWeightedVal = $tmpRaw;
										    		
												    //$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpWeightedVal, $storDispSettings["SubjectScore"]) : round($tmpWeightedVal, 0);
									    			$roundVal = $lreportcard->ROUND_MARK($tmpWeightedVal, "SubjectScore");
									    			$inputMethod = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $storDispSettings["SubjectScore"]);;
									    			
									    			# for calculating real overall subject mark
									    			$tmpWeightedVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpRaw, $CalculationType, $AllColumnMarkArr);
									    			//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpWeightedVal, $storDispSettings["SubjectScore"]) : round($tmpWeightedVal, 0);
									    			$OverallSubjectMark += $tmpWeightedVal;
								    			} else {
									    			$isShowOverallMark = 0;
								    			}
								    			
										    }
										    else {	# case for generate the mark of subject without any component subjects
											    if($SchemeType == "H"){
										    		$tmpVal = $WeightedMark[$ReportColumnID][$StudentID];
										    		if($tmpVal == "-" || $tmpVal == "*" || $tmpVal == "/" || $tmpVal == "N.A."){
											    		$inputMethod = $tmpVal;
										    		}
										    		else if($tmpVal != "NOTCOMPLETED" && $tmpVal != ""){
										    			# for displaying mark with weight calculation
										    			//$tmpWeightedVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpVal, $CalculationType);
										    			
										    			### 20100322 Ivan - Display Raw Mark instead of weighted mark
										    			$tmpWeightedVal = $tmpVal;
										    	
											    		//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpWeightedVal, $storDispSettings["SubjectScore"]) : round($tmpWeightedVal, 0);
											    		$roundVal = $lreportcard->ROUND_MARK($tmpWeightedVal, "SubjectScore");
											    		$inputMethod = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $storDispSettings["SubjectScore"]);
											    		
											    		# for calculating real overall subject mark
											    		//$tmpWeightedVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpVal, $CalculationType, $AllColumnMarkArr);
											    		$tmpWeightedVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpVal, 1, $AllColumnMarkArr);
											    		//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpWeightedVal, $storDispSettings["SubjectScore"]) : round($tmpWeightedVal, 0);
											    		$OverallSubjectMark += $tmpWeightedVal;
									    			}
									    			else {
										    			$inputMethod = "";
										    			$isShowOverallMark = 0;
									    			}
									    		}
								    		}
									    }
									    else {
										    if(!empty($CmpSubjectArr) && $isCalculateByCmpSub){
											    $tmpRaw = $ParentSubMarksheetScoreInfoArr[$ReportColumnID][$StudentID];
											    
											    $SpecialCaseCountArr = $lreportcard->DIFFERENTIATE_SPECIAL_CASE($SpecialCaseCountArr, $tmpRaw, $absentExemptSettings);
											    
											    if($tmpRaw == "-" || $tmpRaw == "*" || $tmpRaw == "/" || $tmpRaw == "N.A."){
										    		$inputMethod = $tmpRaw;
									    		} else {
										    		# for displaying mark with weight calculation
										    		
										    		//$tmpVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpRaw, $CalculationType);
										    		### 20100322 Ivan - Display Raw Mark instead of weighted mark
										    		$tmpVal = $tmpRaw;
												   												    
										    		//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpVal, $storDispSettings["SubjectScore"]) : round($tmpVal, 0);
										    		$roundVal = $lreportcard->ROUND_MARK($tmpVal, "SubjectScore");
										    		$inputMethod = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $storDispSettings["SubjectScore"]);
										    		
										    		# for calculating real overall subject mark
										    		//$tmpVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpRaw, $CalculationType, $AllColumnMarkArr);
										    		//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpVal, $storDispSettings["SubjectScore"]) : round($tmpVal, 0);
										    		$OverallSubjectMark += $roundVal;
									    		}
									    		
										    }
										    else {
									    		if(count($MarksheetScoreInfoArr) > 0){
										    		if($MarkType == "SC") {
										    			$tmpVal = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkNonNum'];
										    			
										    			$SpecialCaseCountArr = $lreportcard->DIFFERENTIATE_SPECIAL_CASE($SpecialCaseCountArr, $tmpVal, $absentExemptSettings);
										    			
										    			$inputMethod = $tmpVal;
									    			} 
									    			else {
										    			$tmpRaw = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkRaw'];
										    			
											    		if($tmpRaw != "" && $tmpRaw >= 0){
												    		# for displaying mark with weight calculation
												    		
												    		### 20100322 Ivan - Display Raw Mark instead of weighted mark
										    				$tmpVal = $tmpRaw;
												    		//$tmpVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpRaw, $CalculationType);
												    		//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpVal, $storDispSettings["SubjectScore"]) : round($tmpVal, 0);
												    		$roundVal = $lreportcard->ROUND_MARK($tmpVal, "SubjectScore");
												    		$inputMethod = $lreportcard->CONVERT_MARK_TO_RELATIVE_DECIMAL_PLACES($roundVal, $storDispSettings["SubjectScore"]);
												    		
												    		# for calculating real overall subject mark
												    		//$tmpVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpRaw, $CalculationType, $AllColumnMarkArr);
												    		$tmpVal = $lreportcard->CONVERT_MARK_TO_WEIGHTED_MARK($ReportID, $ReportColumnID, $SubjectID, $tmpRaw, 1, $AllColumnMarkArr);
												    		//$roundVal = ($storDispSettings["SubjectScore"] != "") ? round($tmpVal, $storDispSettings["SubjectScore"]) : round($tmpVal, 0);
												    		$OverallSubjectMark += $tmpVal;
											    		}
											    		else {
												    		$inputMethod = "";
												    		$isShowOverallMark = 0;
											    		}
											    		
										    		}
									    		}
								    		}
							    		}
						    		}
						    		else {
									    $inputMethod = "--";
								    }
								    
						    	}
						    	else if($SchemeType != "PF" && $ScaleDisplay == "G"){	# View of Converted Grade
						    		if($WeightAry[$ReportColumnID][$SubjectID] != 0 && $WeightAry[$ReportColumnID][$SubjectID] != null && $WeightAry[$ReportColumnID][$SubjectID] != "" ){
						    			if($ColumnHasTemplateAry[$ReportColumnID] && $ReportType == "F"){	// according to student's term subject mark calculated by term report
						    				if(($SchemeType == "H" && $ScaleInput == "G") || $SchemeType == "PF"){
											    if(count($MarksheetOverallScoreInfoArr) > 0){
												    $tmpType = $MarksheetOverallScoreInfoArr[$ReportColumnID][$StudentID]['MarkType'];
												    $tmpNonNum = $MarksheetOverallScoreInfoArr[$ReportColumnID][$StudentID]['MarkNonNum'];
												    if($tmpType != "SC")
												    	$inputMethod = ($SchemeType == "PF") ? $SchemePassFail[$tmpNonNum] : $SchemeGrade[$tmpNonNum];
												    else 
												    	$inputMethod = $tmpNonNum;
											    }
										    }
										    else if($ScaleInput == "M"){
											    if($SchemeType == "H"){
										    		$tmpVal = $WeightedMark[$ReportColumnID][$StudentID];
										    		
										    		if($tmpVal == "+" || $tmpVal == "-" || $tmpVal == "abs"){
											    		$inputMethod = "abs";
										    		} else if($tmpVal == "/" || $tmpVal == "*" || $tmpVal == "N.A."){
											    		$inputMethod = $tmpVal;
										    		} else if($tmpVal != "NOTCOMPLETED" || $tmpVal != ""){
										    			$inputMethod = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $tmpVal);
									    			} else {
										    			$inputMethod = "";
									    			}
									    			
									    		}
								    		}
									    }
									    else {	# according to student's term subject mark provided by teacher without term report
									    
									    	if(count($MarksheetScoreInfoArr) > 0){
									    		$GradeVal = '';
									    		
									    		if($ScaleInput == "G"){ 		// Input Scale is Grade
										    		$tmpVal = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkNonNum'];
											    	$GradeVal = ($MarkType == "SC") ? $tmpVal : $SchemeGrade[$tmpVal];
										    	}
										    	else if($ScaleInput == "M"){	// Input Scale is Mark
										    	
										    		if ($isCalculateByCmpSub)
										    		{
										    			$tmpRaw = $ParentSubMarksheetScoreInfoArr[$ReportColumnID][$StudentID];
										    			$tmpNonNum = '';
										    		}
										    		else
										    		{
										    			$tmpRaw = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkRaw'];
											    		$tmpNonNum = $MarksheetScoreInfoArr[$ReportColumnID][$StudentID]['MarkNonNum'];
										    		}
											    	
											    	if($MarkType == "SC")
											    	{
											    		$GradeVal = $tmpNonNum;
										    		}
											    	else 
											    	{
											    		$GradeVal = ($tmpRaw < 0 || $tmpRaw == "") ? "" : $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $tmpRaw);
											    		
											    	}
										    	}
										    	
											    $inputMethod = $GradeVal;
										    }
										    else
										    {
												if ($ScaleInput=="M" && $ScaleDisplay=="G")
												{
													$thisScore = $ParentSubMarksheetScoreInfoArr[$ReportColumnID][$StudentID];
													$GradeVal = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisScore);
												}   
												$inputMethod = $GradeVal;
										    }
									    }
								    }
								    else {
									    $inputMethod = "--";
								    }
								    
						    	}
				      		} 
				      		
				      		$inputMethod = ($inputMethod == '')? '&nbsp;' : $inputMethod;
						    $StudentMarkSheetScore[$StudentID][$SubjectID][$ReportColumnID]["SubjectTotal"]= $inputMethod;
						    $postValue .= "<input type='hidden' name='Score[$StudentID][$SubjectID][]' value='$inputMethod'>";
				      		
		      			} # End of Report Column Loop
		      			
		      			# Generate Input Selection Box For ScaleInput is Grade(G) OR SchemeType is PassFail(PF)
						if($SchemeType != "PF" && $ScaleDisplay == "G" && $WeightAry[$ReportColumnID][$SubjectID] > 0){
				      		$mark_name = "overall_mark_".$StudentID;
				      		$mark_id = "overall_mark_".$StudentID;
				      		$MarkType = (count($MarksheetOverallScoreInfoArr) > 0) ? $MarksheetOverallScoreInfoArr[$StudentID]['MarkType'] : "";
				      		$tmpValue = (count($MarksheetOverallScoreInfoArr) > 0) ? $MarksheetOverallScoreInfoArr[$StudentID]['MarkNonNum'] : "";
				      		
				      		if($SchemeType == "H" && $ScaleInput == "G"){
					      		$GradeVal = ($MarkType == "SC") ? $tmpValue : $SchemeGrade[$tmpValue];
				      		}
				      		$StudentMarkSheetScore[$StudentID][$SubjectID][$ReportColumnID]["SubjectTotal"] = $GradeVal;
				      		$postValue .= "<input type='hidden' name='Score[$StudentID][$SubjectID][]' value='$GradeVal'>";
				      		
			      		}
						
			    	} //end if(count($column_data) > 0)
				}
			#
			#	Get Marksheet Score End
			############################################################################################
	
	} // end if ($ck_ReportCard_UserType == "STUDENT" || $ck_ReportCard_UserType == "PARENT")
	
	
		
	### Get Marksheet Feedback Info
	# $FeedbackArr[$StudentID][$SubjectID] = InfoArr
	$FeedbackArr = $lreportcard->Get_Marksheet_Feedback($ReportID, '', $StudentID);
	
	
	### Get Manual Adjustment Info
	$ManualAdjustArr = $lreportcard->Get_Manual_Adjustment($ReportID, $StudentID);
	$ManualAdjustArr = $ManualAdjustArr[$StudentID];
	

	### Display Result
	# Tool Bar Row (Left: Filters, Right: System Msg)
	$toolbarRow = '';
	$toolbarRow .= '<tr>';
		
		if ($ck_ReportCard_UserType == "PARENT")
		{
			$toolbarRow .= '<td>'.$ChildrenSelection.'</td>';
			$toolbarRow .= '<td>&nbsp;</td>';
		}
		else if ($eRCTemplateSetting['MarksheetVerification']['OverallVerificationStatus'] && $ck_ReportCard_UserType == "STUDENT")
		{
			$toolbarRow .= $ChildrenInputField;
		}
		
		$toolbarRow .= '<td>'.$ReportSelection.'</td>';
		//$toolbarRow .= '<td>&nbsp;</td>';
		//$toolbarRow .= '<td>'.$SubjectSelection.'</td>';
		
		$toolbarRow .= '<td align="right" width="100%">'.$linterface->GET_SYS_MSG($msg).'</td>';
	$toolbarRow .= '</tr>';
	
	# Build Result Table
	$table = '';
	$table .= '<table  width="100%" border="0" cellspacing="0" cellpadding="4">';
		$table .= '<tr class="tabletop">';
			$table .= '<td class="tabletopnolink">'.$eReportCard['Subject'].'</td>';
			
			for ($i=0; $i<$numOfColumn; $i++)
			{
				if ($ReportType != 'F')
				{
					# Assessment Title for Term Report
					$thisReportColumnTitle = $ColumnArr[$i]['ColumnTitle'];
				}
				else if ($eRCTemplateSetting['Settings']['TemplateSettings_ConsolidatedReportWithTermReportAssessment'])
				{
					$thisReportColumnTitle = $ColumnTitleArr[$ColumnArr[$i]['ReportColumnID']];
				}
				else
				{
					# Term name for Consolidated Report
					$thisYearTermID = $ColumnArr[$i]['SemesterNum'];
					$thisObjYearTerm = new academic_year_term($thisYearTermID);
					$thisReportColumnTitle = $thisObjYearTerm->Get_Year_Term_Name();
				}
				
				$table .= '<td class="tabletopnolink"  style="text-align:center">'.$thisReportColumnTitle.'</td>';
				$postValue .= '<input type="hidden" name="ColumnTitle[]" value="'.$thisReportColumnTitle.'">';
			
			}
			# 20191227 (Philips) - Display Full Mark
			$table .= "<td class='tabletopnolink' style='text-align:center'>" . $eReportCard['SchemesFullMark'] . '</td>';
			if($eRCTemplateSetting['Management']['MarksheetVerification']['ShowSubjectTeacherComment'])
			{
				$table .= '<td class="tabletopnolink"  style="text-align:center">'.$eReportCard['Comment'].'</td>';
			}
			$table .= '<td class="tabletopnolink"  style="text-align:center">'.$eReportCard['Feedback'].'</td>';
			$table .= '<td class="tabletopnolink">'.$eReportCard['LastModifiedDate'].'</td>';
			$table .= '<td class="tabletopnolink">'.$eReportCard['LastModifiedBy'].'</td>';
			$table .= '<td class="tabletopnolink">'.$eReportCard['Status'].'</td>';
		$table .= '</tr>';
		
		$PeriodAction = "Verification";
		$PeriodType = $lreportcard->COMPARE_REPORT_PERIOD($ReportID, $PeriodAction);
		$withinVerificationPeriod = false;
//		if (date('Y-m-d') < $VerificationStart || date('Y-m-d') > $VerificationEnd || ($VerificationStart=='' && $VerificationEnd==''))

		if($PeriodType!=1)
		{
			# not within marksheet verification period
			$withinVerificationPeriod = false;
		}
		else
		{
			# within marksheet verification period
			$withinVerificationPeriod = true;
			
			$SubjectIDArr = $lreportcard->returnSubjectIDwOrder($ClassLevelID);
			$numOfSubjectID = count($SubjectIDArr);
			$mainSubjectCount = 0;
			for ($i=0; $i<$numOfSubjectID; $i++)
			{
				$thisSubjectID = $SubjectIDArr[$i];
				$thisSubjectName = $lreportcard->GET_SUBJECT_NAME($thisSubjectID);
				$thisDateModified = $FeedbackArr[$StudentID][$thisSubjectID]['DateModified'];
				$lstudent = new libuser($FeedbackArr[$StudentID][$thisSubjectID]['LastModifiedBy']);
				$thisLastModifiedBy = $lstudent->UserName();
				$thisStatus = $FeedbackArr[$StudentID][$thisSubjectID]['RecordStatus'];
				
				if (!in_array($thisSubjectID, $StudentSubjectIDArr))
					continue;
					
				$ObjSubject = new subject($thisSubjectID);
				if ($ObjSubject->Is_Component_Subject())
				{
					$prefix = '&nbsp;&nbsp;&nbsp;&nbsp;';
				}
				else
				{
					$prefix = '';
					$mainSubjectCount++;
				}
				$tr_css = ($mainSubjectCount % 2 == 1) ? "tablerow2" : "tablerow1";
				
				# Get the student-subject marks
				//$thisReportColumnMarkArr = $StudentMarkSheetScore[$StudentID][$thisSubjectID];
				
				# Feedback link
				//$thisParameters = "ReportID=".$ReportID."&SubjectID=".$thisSubjectID."&StudentID=".$StudentID;
				//$FeedbackIcon = '<a href="view_feedback.php?'.$thisParameters.'">
				$FeedbackIcon = '<a href="javascript:ViewFeedback('.$thisSubjectID.')">
									<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_view.gif" width="20" height="20" border="0" alt="'.$button_view.' '.$eReportCard['Feedback'].'">
								</a>';
						
				$commentIconDivId = 'commentIconDivID'."_$i";
				$MouseEvent = ' onmouseover="showShortCutPanel(\''.$commentIconDivId.'\', \''.$ReportID.'\', \''.$StudentID.'\', \''.$thisSubjectID.'\');" onmouseout="hideShortCutPanel();" ';
				
				$CommentIcon = "<div id='".$commentIconDivId."' style=\"width:100%;\" >".
									'<img '. $MouseEvent .'src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_remark.gif" width="20" height="20" border="0" alt="'.$button_view.' '.$eReportCard['Comment'].'">'.
								"</div>";
		
		
				$table .= '<tr class="'.$tr_css.'">';
					# Subject Name
					$table .= '<td class="tabletext">'.$prefix.$thisSubjectName.'</td>';
					
					/*
					if (is_array($thisReportColumnMarkArr) && count($thisReportColumnMarkArr) > 0)
					{
						foreach ($thisReportColumnMarkArr as $thisReportColumnID => $thisMark)
						{
							# Column Mark
							$thisMark = ($thisMark=='')? $lreportcard->EmptySymbol : $thisMark;
							$table .= '<td class="tabletext" style="text-align:center">'.$thisMark.'</td>';
						}
					}
					*/
					
					for ($j=0; $j<$numOfColumn; $j++)
					{
						$thisReportColumnID = $ColumnArr[$j]['ReportColumnID'];
						
						if ($eRCTemplateSetting['MarksheetVerification']['ShowManualAdjustedScore'] && isset($ManualAdjustArr[$thisReportColumnID][$thisSubjectID]['Score'])) {
							$thisMark = $ManualAdjustArr[$thisReportColumnID][$thisSubjectID]['Score'];
						}
						else {
							# Column Mark
							$thisMark = $StudentMarkSheetScore[$StudentID][$thisSubjectID][$thisReportColumnID]["SubjectTotal"];
							//$thisMark = ($thisMark=='')? $lreportcard->EmptySymbol : $thisMark;
							$thisMark = ($thisMark=='')? $lreportcard->EmptySymbol : $lreportcard->ROUND_MARK($thisMark, "SubjectScore");
						}
						
						$table .= '<td class="tabletext" style="text-align:center">'.$thisMark.'</td>';
					}
					# 20191227 (Philips) - Display Full Mark
					$table .= '<td class="tabletext" style="text-align:center">' . $FullMarkAry[$thisSubjectID] . '</td>';
					
					# Comment (mouse over)
					if($eRCTemplateSetting['Management']['MarksheetVerification']['ShowSubjectTeacherComment'])
					{
						$table .= '<td class="tabletext" style="text-align:center">'.$CommentIcon.'</td>';
					}
					
					
					# Feedback
					$table .= '<td class="tabletext" style="text-align:center">'.$FeedbackIcon.'</td>';
					
					# Last Modified Date
					$thisDisplay = ($thisDateModified == '')? "---" : $thisDateModified;
					$table .= '<td class="tabletext">'.$thisDisplay.'</td>';
					
					# Last Modified By
					$thisDisplay = ($thisLastModifiedBy == '')? "---" : $thisLastModifiedBy;
					$table .= '<td class="tabletext">'.$thisDisplay.'</td>';
					
					# Status
					switch($thisStatus)
					{
						case "1": 
						$thisDisplay = $eReportCard['Closed'];
						break;
						case "2":
						$thisDisplay = $eReportCard['InProgress']; 
						break;
						default: $thisDisplay = "---";
					}
					$table .= '<td class="tabletext">'.$thisDisplay.'</td>';
					
				$table .= '</tr>';
			}	// End for Subject
		}
	$table .= '</table>';
	
}
	
	$PAGE_TITLE = $eReportCard['Management_MarksheetVerification'];
	
	# Tag Information
	$TAGS_OBJ[] = array($PAGE_TITLE, "", 0);
	$linterface->LAYOUT_START();
	

$htmlAry['notWithinVerificationPeriod'] = '';
if (!$withinVerificationPeriod) {
	$colspan = $numOfColumn + 5;
	$htmlAry['notWithinVerificationPeriod'] = '<tr><td class="tabletext" colspan="'.$colspan.'" style="text-align:center">'.$eReportCard['VerificationPeriodIsNotNow'].'</td></tr>';
}
	

$htmlAry['studentResultSummary'] = '';
if ($eRCTemplateSetting['Management']['MarksheetVerification']['ShowStudentResultSummary'] && $withinVerificationPeriod)
{
	$reportUrl = curPageURL($withQueryString=0, $withPageSuffix=0).'/home/eAdmin/StudentMgmt/eReportCard/reports/student_result_summary/generate.php';

	$strCookie = '';
	foreach ((array)$_COOKIE as $_key => $_value) {
		$strCookie .= $_key.'='.$_value.';';
	}
	$strCookie .= '; path=/';
	
	$studentClassAry = $lreportcard->Get_Student_Class_ClassLevel_Info($StudentID);
	$classLevelId = $studentClassAry[0]['ClassLevelID'];
	$classId = $studentClassAry[0]['ClassID'];
	
	$subjectAssoAry = $lreportcard->returnSubjectwOrderNoL($classLevelId, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);
	$subjectParam = '';
	foreach ((array)$subjectAssoAry as $_subjectId => $_subjectName) {
		$subjectParam .= '&SubjectIDArr[]='.$_subjectId;
	}
	$postString = 'YearID='.$classLevelId.'&ReportID='.$ReportID.'&YearClassIDArr[]='.$classId.$subjectParam.'&IsPrintFromParent=1';

    // Added token to post fields
    $extraUrlToken = $reportUrl.$_SESSION['UserID'];
    $extraUrlToken = str_replace(curPageURL($withQueryString=0, $withPageSuffix=0), '', $extraUrlToken);
    $extraUrlToken = returnSimpleTokenByDateTime($extraUrlToken);
    $postString .= '&token='.$extraUrlToken.'&UserID='.$_SESSION['UserID'];

	session_write_close();
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $reportUrl);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_COOKIE, $strCookie);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postString);
	$html = curl_exec($ch);
	curl_close($ch);
	
	$html = str_replace("\n", "\r\n", trim($html));
	preg_match('#\<\!--startReportOfStudent_'.$StudentID.'--\>(.+?)\<\!--endReportOfStudent_'.$StudentID.'--\>#s', $html, $matches);

	$x = '';
	$x .= '<style type="text/css">
				<!--
				.GrandMSContentTable {
					border-collapse: collapse;
					border-color: #000000;
				}
				.GrandMSContentTable thead {
					font-size: 12px;
					font-family: arial, "lucida console", sans-serif;
				}
				.GrandMSContentTable tbody {
					font-size: 12px;
					font-family: arial, "lucida console", sans-serif;
				};
			</style>';
	$x .= $matches[0];
	$htmlAry['studentResultSummary'] = '<tr><td>'.$x.'</td></tr>';
}
	
	
// [2015-1104-1130-08164] display button for parents and students to print reports
// [2017-0109-1818-40164] updated logic
if ($eRCTemplateSetting['Management']['MarksheetVerification']['PrintStudentReport'] && ($ck_ReportCard_UserType == "STUDENT" || $ck_ReportCard_UserType == "PARENT"))
{
	// Get Print Report Period Settings
	$PeriodAction = "Publish";
	$withinPrintReportPeriod = $lreportcard->COMPARE_REPORT_PERIOD($ReportID, $PeriodAction)==1;
	
	// Get Report Last Archived Date
	if($ReportID && $ClassLevelID) {
		$ReportBasicInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		$isTermReport = $ReportBasicInfo['Semester']!="F";
		$isYearReport = $ReportBasicInfo['Semester']=="F";
		$ReportGenerateDate = $ReportBasicInfo['LastGenerated'];
		$isReportGenerated = !is_date_empty($ReportGenerateDate);
		$ReportArchiveDate = $ReportBasicInfo['LastArchived'];
		$isReportArchived = !is_date_empty($ReportArchiveDate);
	}
	
	# Term Report + Print Report Period
	if($isTermReport && $withinPrintReportPeriod) {
		// Display Print button for Archived Report
		if($isReportArchived) {
			$printReportBtn = $linterface->GET_ACTION_BTN($eReportCard['ReportPreview&Printing'], "button", "javascript:jDO_PRINT()", "printReportBtn");
			$htmlAry['generateStudentReport'] = '<tr><td align="center"><br>'.$printReportBtn.'</td></tr>';
		}
		// Display message for Non-archived Report
		else {
			$htmlAry['generateStudentReport'] = '<tr><td align="center">'.$eReportCard["ReportNotGenerated"].'</td></tr>';
		}
	}
	# Year Report
	else if($isYearReport)
	{
		# Print Report Period
		if($withinPrintReportPeriod) {
			// Display Print button for Archived Report
			if($isReportArchived) {
				$printReportBtn = $linterface->GET_ACTION_BTN($eReportCard['ReportPreview&Printing'], "button", "javascript:jDO_PRINT()", "printReportBtn");
				$htmlAry['generateStudentReport'] = '<tr><td align="center"><br>'.$printReportBtn.'</td></tr>';
			}
			// Display message for Non-archived Report
			else {
				$htmlAry['generateStudentReport'] = '<tr><td align="center">'.$eReportCard["ReportNotGenerated"].'</td></tr>';
			}
		}
		# Verification and Preview Period
		else if($withinVerificationPeriod)
		{
			// Init Cust Object
			$lreportcard_custom = new libreportcardcustom();
			
			// Get Student Info
			$studentInfo = $StudentInfoArr[0];
			$studentName = $studentInfo['ChineseName'];
			$studentClassName = $studentInfo['ClassName'];
			$studentClassNumber = $studentInfo['ClassNumber'];
			
			// Get Student Class Level Type
			$thisSchoolType	= $lreportcard_custom->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
			$thisFormNumber = $lreportcard_custom->GET_FORM_NUMBER($ClassLevelID, 1);
			$isPrimaryLevel = $thisSchoolType == "P";
			$isSecondaryLevel = $thisSchoolType == "S";
			$isGraduateLevel = ($isPrimaryLevel && $thisFormNumber == 6) || ($isSecondaryLevel && $thisFormNumber == 3) || ($isSecondaryLevel && $thisFormNumber == 6);
	
			// Get Promotion Last Generated Date
			$PromotionGenerationDate = $lreportcard->GetPromotionGenerationDate($ClassLevelID);
	        $PromotionGenerationDate = $PromotionGenerationDate["LastPromotionUpdate"];
	        $isPromotionGenerated = !is_date_empty($PromotionGenerationDate);
	        
			// Display Student Promotion Status
	        if($isReportGenerated && $isPromotionGenerated)
			{
				// Get Promotion Status
				$studentPromotionInfo = $lreportcard->GetPromotionStatusList($ClassLevelID, "", $StudentID, $ReportID);
				$studentPromotionInfo = $studentPromotionInfo[$StudentID];
				$studentPromotionStatus = $studentPromotionInfo["Promotion"];
				
				// Get Preset Promotion Status
				$promotionStatusAry = $lreportcard_custom->customizedPromotionStatus;
				
				// Get Promotion related Subjects
				$studentPromotionSubjects = $lreportcard->GET_PROMOTION_RETENTION_RELATED_SUBJECT($ReportID, $StudentID);
				
				// Get Absent Subjects
				$studentAbsentSubjects = BuildMultiKeyAssoc((array)$studentPromotionSubjects, array("SubjectType", "SubjectID"), "SubjectNameB5");
				$studentAbsentSubjects = $studentAbsentSubjects[5];
				$studentAbsentSubjectIDs = array_keys((array)$studentAbsentSubjects);
				$studentAbsentSubjectNames = array_values((array)$studentAbsentSubjects);
				
				// Get Failed Subjects
				$studentFailSubject = BuildMultiKeyAssoc((array)$studentPromotionSubjects, "SubjectID", "SubjectNameB5");
				$studentFailSubjectIDs = array_keys((array)$studentFailSubject);
				
				// loop Absent Subjects
				$absentSubjectResultAry = array();
				if(!empty($studentAbsentSubjectNames)) {
					foreach((array)$studentAbsentSubjectNames as $thisAbsentSubjectName) {
						$thisAbsentSubjectName = $thisAbsentSubjectName["SubjectNameB5"];
						$absentSubjectResultAry[] = $thisAbsentSubjectName.$eReportCard["PromotionStatusType"]["SubjectAbsent"];
					}
				}
				
				// loop Failed Subjects
				$failSubjectResultAry = array();
				$SubjectWaitingMarkupExamAry = array();
				foreach($studentFailSubject as $thisFailSubjectName) {
					if(in_array($thisFailSubjectName, (array)$studentAbsentSubjectNames))
						continue;
					
					$thisFailSubjectName = $thisFailSubjectName["SubjectNameB5"];
					$failSubjectResultAry[] = $thisFailSubjectName.$eReportCard["PromotionStatusType"]["SubjectFail"];
					$SubjectWaitingMarkupExamAry[] = $thisFailSubjectName.$eReportCard["PromotionStatusType"]["MarkupExam"];
				}
				
				// Get Mark-up Exam Result
				if(!empty($studentFailSubjectIDs)) {
					$studentMarkupResult = $lreportcard->GET_EXTRA_SUBJECT_INFO(array($StudentID), $studentFailSubjectIDs, $ReportID);
					$studentMarkupResult = $studentMarkupResult[$StudentID];
					if(!empty($studentMarkupResult))
					{
						// loop Mark-up Exam Result
						$markupSubjectResultAry = array();
						foreach($studentFailSubjectIDs as $MarkupSubjectID) {
							if(in_array($MarkupSubjectID, (array)$studentAbsentSubjectIDs))
								continue;
							
							$thisMarkupResult = $studentMarkupResult[$MarkupSubjectID];
							$thisMarkupResultScore = $thisMarkupResult['Info'];
							if(empty($thisMarkupResult) || $thisMarkupResultScore=="")
								continue;
							
							if($thisMarkupResultScore >= 60)
								$MarkupResultStatus = "MarkupExamPass";
							else
								$MarkupResultStatus = "MarkupExamFail";
							
							$markSubjectName = $studentFailSubject[$MarkupSubjectID]['SubjectNameB5'];
							$markupSubjectResultAry[] = $markSubjectName.$eReportCard["PromotionStatusType"][$MarkupResultStatus];
						}
						
						// Update Promotion Status if Promotion Retention
						if(!empty($markupSubjectResultAry)) {
							if($studentPromotionStatus==$promotionStatusAry['Promoted_with_Retention']) {
								$studentPromotionStatus = $promotionStatusAry['Promoted'];
								if($isGraduateLevel) {
									$studentPromotionStatus = $promotionStatusAry['Graduate'];
								}
							}
						}
					}
				}
				
				// Subject Details
				$SubjectDetailStr = "";
				if($studentPromotionStatus==$promotionStatusAry['Waiting_Makeup'] && count($SubjectWaitingMarkupExamAry) > 0) {
					// Waiting Make-up Exam
					$SubjectDetailStr = implode("、", $SubjectWaitingMarkupExamAry);
				}
				else {
					if(count($markupSubjectResultAry) > 0) {
						// Mark-up Exam Result
						$SubjectDetailStr = implode("、", $markupSubjectResultAry);
					}
					else if(count($failSubjectResultAry) > 0) {
						// Fail Subjects
						$SubjectDetailStr = implode("、", $failSubjectResultAry);
					}
					else if($studentPromotionStatus==$promotionStatusAry['Promoted'] || $studentPromotionStatus==$promotionStatusAry['Graduate']) {
						// Direct Promotion
						$SubjectDetailStr = $eReportCard["PromotionStatusType"]["NoMarkupExamRequired"];
					}
				}
				if(!empty($absentSubjectResultAry))
				{
					$SubjectDetailStr .= $SubjectDetailStr != ""? "，" : "";
					$SubjectDetailStr .= implode("、", $absentSubjectResultAry);
					
					if(empty($markupSubjectResultAry) && !empty($SubjectWaitingMarkupExamAry)) {
						$SubjectDetailStr .= "，";
						$SubjectDetailStr .= implode("、", $SubjectWaitingMarkupExamAry);
					}
					
					if($studentPromotionStatus > 0) {
						$SubjectDetailStr .= "，";
					}
				}
				else if($SubjectDetailStr != "") {
					$SubjectDetailStr .= "，";
				}
				
				// Promotion Status
				$htmlAry['studentPromotionStatusTable'] = '<tr><td align="center" style="font-size:18px;">';
					$htmlAry['studentPromotionStatusTable'] .= $studentClassName.$studentClassNumber.$studentName;
					$htmlAry['studentPromotionStatusTable'] .= "，";
					$htmlAry['studentPromotionStatusTable'] .= $SubjectDetailStr;
					$htmlAry['studentPromotionStatusTable'] .= $eReportCard["PromotionStatusType"]["StatusType".$studentPromotionStatus];
					$htmlAry['studentPromotionStatusTable'] .= "。";
				$htmlAry['studentPromotionStatusTable'] .= '</td></tr>';
				
				// Display Print button for Retained or Dropout Student
//				if(($studentPromotionStatus==$promotionStatusAry['Retained'] || $studentPromotionStatus==$promotionStatusAry['Dropout'])) {
//					$printReportBtn = $linterface->GET_ACTION_BTN($eReportCard['ReportPreview&Printing'], "button", "javascript:jDO_PRINT()", "printReportBtn");
//					$htmlAry['generateStudentReport'] = '<tr><td align="center"><br>'.$printReportBtn.'</td></tr>';
//				}
			}
			else {
				$htmlAry['generateStudentReport'] = '<tr><td align="center">'.$eReportCard["ReportNotGenerated"].'</td></tr>';
			}
		}
	}
}

// [2016-0822-1721-00164] Display button for Parents and Students to update Verification Status
if($eRCTemplateSetting['MarksheetVerification']['OverallVerificationStatus'] && ($ck_ReportCard_UserType == "STUDENT" || $ck_ReportCard_UserType == "PARENT") && $withinVerificationPeriod)
{
	// Get Report Info
	$ReportBasicInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
	$ReportClassLevelID = $ReportBasicInfo['ClassLevelID'];
	
	// Check Student Class Level
	if($ClassLevelID == $ReportClassLevelID)
	{
		// Get Verification Status
		$VerificationStatus = 0;
		$newVerificationStatus = 1;
		$VerStatusLastModifiedBy = "";
		$thisOverallFeedbackInfo = $lreportcard->Get_Marksheet_Feedback_Info($ReportID, array(0), $StudentID);
		if(!empty($thisOverallFeedbackInfo)) {
			$VerificationStatus = $thisOverallFeedbackInfo[0]['RecordStatus'];
			$newVerificationStatus = $VerificationStatus? 0 : 1;
			$VerStatusLastModifiedBy = $thisOverallFeedbackInfo[0]['DateModified']. " (".$thisOverallFeedbackInfo[0]['LastModifiedBy'].")";
		}
		
		// Last Modified Info
		if(!empty($VerStatusLastModifiedBy)) {
			$htmlAry['VerificationStatus'] .= '<tr>';
				$htmlAry['VerificationStatus'] .= '<td align="center">';
					$htmlAry['VerificationStatus'] .= '<p class="spacer"></p>';
	              	$htmlAry['VerificationStatus'] .= '<span class="row_content tabletextremark"> '.$Lang['General']['LastModifiedBy'].": ".$VerStatusLastModifiedBy.'</span>';
	              $htmlAry['VerificationStatus'] .= '</td>';
			$htmlAry['VerificationStatus'] .= '</tr>';
		}
		
		// Update Status Button
		if($VerificationStatus) {
			$htmlAry['VerificationStatus'] .= '<tr><td align="center">'.$linterface->GET_ACTION_BTN($Lang['eReportCard']['IncompletedVerification'], "button", "javascript:jDO_UPDATE_STATUS()", "verificationStatusBtn").'</td></tr>';
		}
		else {
			$htmlAry['VerificationStatus'] .= '<tr><td align="center">'.$linterface->GET_ACTION_BTN($Lang['eReportCard']['CompletedVerification'], "button", "javascript:jDO_UPDATE_STATUS()", "verificationStatusBtn").'</td></tr>';
		}
		$htmlAry['VerificationStatus'] .= "<input type='hidden' name='VStatus' id='VStatus' value='$newVerificationStatus'>";
	}
}
?>

<script language="javascript">
function js_Reload()
{
	var targetReportID = $('#ReportID').val();
	var targetSubjectID = $('#SubjectID').val();
	var targetStudentID = $('#StudentID').val();
	
	var params = "ReportID=" + targetReportID + "&targetSubjectID=" + targetSubjectID + "&StudentID=" + targetStudentID;
	location.href = "index.php?"+params;
}

function ViewFeedback(SubjectID)
{
	var url = "view_feedback.php?";
	url += "ReportID=<?=$ReportID?>";
	url += "&StudentID=<?=$StudentID?>";
	url += "&SubjectID="+SubjectID;
	
	document.FormMain.action= url;
	document.FormMain.submit();
	//window.location.href=url;
}

function js_Change_Layer_Position(jsClickedObjID, jsLayerDivID)
{
	var jsOffsetLeft, jsOffsetTop;
	
	jsOffsetLeft = $('div#' + jsClickedObjID).width();
	jsOffsetTop = 0;
	//jsOffsetTop = $('div#' + jsClickedObjID).height();
		
	var posleft = getPosition(document.getElementById(jsClickedObjID), 'offsetLeft') + jsOffsetLeft;
	var postop = getPosition(document.getElementById(jsClickedObjID), 'offsetTop') + jsOffsetTop;
	
	document.getElementById(jsLayerDivID).style.left = posleft + "px";
	document.getElementById(jsLayerDivID).style.top = postop + "px";
}

function showShortCutPanel(objID,ReportId,StudentId,SubjectId) 
{
	var shortCutPanel = document.getElementById('editComment');
	
	var jsOffsetLeft, jsOffsetTop;	
	jsOffsetLeft = 60;
	jsOffsetTop = 20;
		
	var posleft = getPostion(document.getElementById(objID), 'offsetLeft') + jsOffsetLeft;
	var postop = getPostion(document.getElementById(objID), 'offsetTop') + jsOffsetTop;
	
	document.getElementById('editComment').style.left = posleft + "px";
	document.getElementById('editComment').style.top = postop + "px";

	MM_showHideLayers('editComment','','show'); 
	
	$('div#editCommentContent').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	$('div#editCommentContent').load(
		"ajax_getComment.php",  
		{ 
			ReportID: ReportId,
			StudentID: StudentId,
			SubjectID: SubjectId
		},
		function(returnString)
		{
			if(returnString=='')
			{
				$('div#editCommentContent').html('<?=$Lang['General']['EmptySymbol']?>');
			}	
		}
	);
}

function hideShortCutPanel()
{	
	$('div#editCommentContent').html('');
	MM_showHideLayers('editComment','','hide');	
}

<? if ($eRCTemplateSetting['Management']['MarksheetVerification']['PrintStudentReport'] && ($ck_ReportCard_UserType == "STUDENT" || $ck_ReportCard_UserType == "PARENT") && $isReportArchived) { ?>
	function jDO_PRINT()
	{
		var obj = document.form1;
		obj.action = "../archive_report_card/print_preview.php";
		obj.target = "_blank";
		obj.submit();
	}
<? } ?>

<? if($eRCTemplateSetting['MarksheetVerification']['OverallVerificationStatus'] && ($ck_ReportCard_UserType == "STUDENT" || $ck_ReportCard_UserType == "PARENT") && $withinVerificationPeriod) { ?>
	function jDO_UPDATE_STATUS()
	{
		var obj = document.FormMain;
		obj.action = "update_verification_status.php";
		obj.submit();
	}
<? } ?>

</script>

<br/>
<form name="FormMain" method="POST" action="marksheet_complete_confirm.php">
<table width="98%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<?=$toolbarRow?>
				<tr>
					<td colspan="5" class="tabletext">
						<b><?=$eReportCard['VerificationStartDate']?>:</b> <?=$VerificationStart?> 
						&nbsp;&nbsp; 
						<b><?=$eReportCard['VerificationEndDate']?>:</b> <?=$VerificationEnd?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	
	<? if (!$eRCTemplateSetting['Management']['MarksheetVerification']['HideMarks']) { ?>
		<tr>
			<td align="right" class="tabletextremark"><?=$eReportCard['MarkRemindSet1']?></td>
		</tr>
		<tr>
			<td><?=$table?></td>
		</tr>
	<? } ?>
	
	<?php echo $htmlAry['studentResultSummary'] ?>
	<?php echo $htmlAry['studentPromotionStatusTable'] ?>
	<?php echo $htmlAry['notWithinVerificationPeriod'] ?>
	<?php echo $htmlAry['generateStudentReport'] ?>
	<?php echo $htmlAry['VerificationStatus'] ?>
</table>

<? if ($withinVerificationPeriod) { ?>
	<?=$postValue?>
<? } ?>

<div id="editComment" class="selectbox_layer"  style="visibility:hidden; width:300px; height:200px;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr><td><?=$linterface->GET_NAVIGATION2_IP25($eReportCard['TeacherComment']) ?></td></tr>
		<tr>
			<td>
				<div id="editCommentContent"></div>
			</td>
		</tr>
	</table>
</div>

</form>

<? if ($eRCTemplateSetting['Management']['MarksheetVerification']['PrintStudentReport'] && ($ck_ReportCard_UserType == "STUDENT" || $ck_ReportCard_UserType == "PARENT") && $isReportArchived) { ?>
	<form id="form1" name="form1" method="POST">
		<input type="hidden" name="TargetStudentID[]" id="TargetStudentID[]" value="<?=$StudentID?>" />
		<input type="hidden" name="ReportID" id="ReportID" value="<?=$ReportID?>" />
		<input type="hidden" name="GenerateStudentReport" id="GenerateStudentReport" value="1" />
	</form>
<? } ?>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>