<?php
# Editing by Bill 
/***************************************************
 * 	Modification log:
 * 	20160120 Bill:	[2015-0421-1144-05164]
 * 	- Create File
 * 		$eRCTemplateSetting['Report']['ReportGeneration']['GeneratePromotionStatus_CustomizedLogic']
 * *************************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lreportcard = new libreportcardcustom();
$lreportcard_ui = new libreportcard_ui();

if(!$plugin['ReportCard2008'] || !$lreportcard->hasAccessRight() || !$eRCTemplateSetting['Report']['PromotionSummaryReport'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit;
}

# POST data
$ClassLevelID = $_POST['YearID'];
$format = $_POST['format'];

# Report Format - CSV
if($format == 'csv')
{
	# Initiate
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	$lexport = new libexporttext();
	
	# CSV Content
	$header = array("香港培道中學 升留班會議資料");
	$rows = $lreportcard->Get_Pooi_To_Promotion_Meetting_Record($format, $ClassLevelID);
	
	# Export CSV
	$exportContent = $lexport->GET_EXPORT_TXT($rows, $header);
	$lexport->EXPORT_FILE("Promotion_Meeting_Record.csv", $exportContent);
}
# Report Format - Print
else{
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
	
	# Print Content
	echo $lreportcard->Get_Pooi_To_Promotion_Meetting_Record($format, $ClassLevelID);
	
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
}

intranet_closedb();
?>

<? if($format != 'csv'){ ?>
<style type="text/css">
@charset "utf-8";

/* CSS Document */
body { font-family: "Times New Roman, 新細明體"; padding:0; margin:0;}
html, body { margin: 0px; padding: 0px; }
.page_break { page-break-after:always; margin:0; padding:0; line-height:0; font-size:0; height:0; }

.main_container { display:block; width:680px; height:880px; margin:auto; /*border:1px solid #DDD;*/ padding:30px 20px; position:relative; padding-bottom:90px; }
.report_sch_header td { font-size: 15pt; }
.report_sub_header td { font-size: 10pt; vertical-align: top; text-align:center;}
.report_table td { font-size: 9pt; vertical-align: top; text-align:center; padding:2px; }
.report_title_header td { font-size: 11pt; vertical-align: top; text-align:center; }

</style>
<? } ?>