<?php
# Editing by Bill 

/***************************************************
 * 	Modification log:
 * 	20170519 Bill:
 * 	- Allow View Group to access
 * 	20160120 Bill:	[2015-0421-1144-05164]
 * 	- display imported promotion status
 * 		(related to $eRCTemplateSetting['ReportGeneration']['PromotionDisplayImportStatus'])
 * 	- added tag for Pooi To Promotion Meeting Report
 * 		$eRCTemplateSetting['Report']['PromotionSummaryReport']
 * *************************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "VIEW_GROUP");

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcard();
// [2015-0421-1144-05164] use cust reportcard object for displaying imported promotion status
if($eRCTemplateSetting['ReportGeneration']['PromotionDisplayImportStatus']){
	$lreportcard = new libreportcardcustom();
}
$lreportcard_ui = new libreportcard_ui();

if(!$plugin['ReportCard2008'] || !$lreportcard->hasAccessRight())
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit;
}

$CurrentPage = "Management_Promotion";
$linterface = new interface_html();
        
### Get Tag (List/Generation)
$display_tagstype = '';
$display_tagstype .= '
  <div class="shadetabs">
    <ul>';
$display_tagstype .= '<li class="selected"><a href="#">'.$eReportCard['List'].'</a></li>';
if($ck_ReportCard_UserType != "VIEW_GROUP")
	$display_tagstype .= '<li><a href="generate.php">'.$eReportCard['Generate'].'</a></li>';
// [2015-0421-1144-05164] Added tag for Promotion Meeting Report
if($eRCTemplateSetting['Report']['PromotionSummaryReport']){
	$display_tagstype .= '<li><a href="report_index.php">'.$Lang['eReportCard']['ReportArr']['PromotionArr']['ReportName'].'</a></li>';
}
$display_tagstype .= '
    </ul>
  </div>';


### Get Form selection (show form which has report card only)
$libForm = new Year();
$FormArr = $libForm->Get_All_Year_List();
$YearID = $YearID? $YearID : $FormArr[0]['YearID'];

$FormSelection = $lreportcard_ui->Get_Form_Selection('YearID', $YearID, 'js_Reload();', $noFirst=1);

### Get Class selection
$ObjClass = new year_class($YearClassID);
if ($ObjClass->YearID != $YearID)
{
	// User has switched the form selection
	$YearClassID = 0;
}
$libFCP_ui = new form_class_manage_ui();
$ClassSelection = $libFCP_ui->Get_Class_Selection($lreportcard_ui->schoolYearID, $YearID, 'YearClassID',$YearClassID, 'js_Reload();', $noFirst=0, $isMultiple=0, $isAll=1);
  
### Get Status Selection
$StatusSelection = $lreportcard_ui->GetPromotionStatusSelection("PromotionStatus","js_Reload();", $PromotionStatus, $isAll=1, $noFirst=0, $isMultiple=0,  $OtherTagInfo='');

### show Remark
$showRemark = $linterface->Get_Warning_Message_Box($eReportCard['PromotionInstruction'], $eReportCard['PromotionStatusInstruction']);

### Toolbar (Export & Print)
$toolbar = '';
$toolbar .= $linterface->GET_LNK_EXPORT("javascript:js_Go_Export();", '', '', '', '', 0);
$toolbar .= toolBarSpacer();
$toolbar .= $linterface->GET_LNK_PRINT("javascript:js_Go_Print();", '', '', '', '', 0);

### Generation Date
$GenerationDateAry = $lreportcard->GetPromotionGenerationDate($YearID);
$thisDisplay = ($GenerationDateAry["LastPromotionUpdate"]=='' || $GenerationDateAry["LastPromotionUpdate"]=='0000-00-00 00:00:00')? '--' : $GenerationDateAry["LastPromotionUpdate"];
$GenerationDate = $eReportCard['LastGenerate'].": ".$thisDisplay;


### Gen Promotion Status Table
if($YearClassID != '' && $YearClassID != 0)
	$StudentArr = $lreportcard->GET_STUDENT_BY_CLASS($YearClassID);
else
	$StudentArr = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $YearID);

$PromotionStatusAry = $lreportcard->GetPromotionStatusList($YearID, $YearClassID);

$display = '';
$display .= "<table border='0' cellspacing='0' cellpadding='4' width='100%'>";
    # table head
    $display .= "<tr class='tabletop'>";
    	$display .= "<td class='tabletopnolink' width='20%'>".$eReportCard["Class"]."</td>";
    	$display .= "<td class='tabletopnolink' width='15%'>".$eReportCard["ClassNo"]."</td>";
    	$display .= "<td class='tabletopnolink' width='25%'>".$eReportCard["Student"]."</td>";
    	$display .= "<td class='tabletopnolink' width='20%'>".$eReportCard["GenerationStatus"]."</td>";
    	$display .= "<td class='tabletopnolink' width='20%'>".$eReportCard["FinalStatus"]."</td>";
    $display .= "</tr>";
 
	# table body
	$DisplayDataArr = array();
	$counter_i = 0;
	$counter_j = 0;
	foreach((array)$StudentArr as $key => $thisStudentInfoArr)
	{
		# Get Student Info
		$thisStudentID = $thisStudentInfoArr['UserID'];
		$thisStudentName = $thisStudentInfoArr['StudentName'];
		$thisClassName = Get_Lang_Selection($thisStudentInfoArr['ClassTitleCh'], $thisStudentInfoArr['ClassTitleEn']);
		$thisClassNumber = $thisStudentInfoArr['ClassNumber'];
		
		# Get Student Promotion Info
		$thisPromotionStatusArr = $PromotionStatusAry[$thisStudentID];
		$thisPromotionStatus = $thisPromotionStatusArr['Promotion'];
		$thisGenerationStatusText = $thisPromotionStatusArr['GenerationStatus'];
		$thisFinalStatusText = $thisPromotionStatusArr['FinalStatus'];
		
		# Skip Student according to the Status filter
		if($PromotionStatus != 0 && $thisPromotionStatus != $PromotionStatus)
			continue;
			
		# Status Display
		$thisGenerationStatusDisplay = ($thisGenerationStatusText=='')? '--' : $thisGenerationStatusText;
		$thisFinalStatusDisplay = ($thisFinalStatusText=='')? '--' : $thisFinalStatusText;
			
		$rowcss = "tablerow".(($counter_i%2)+1);
		$display .= "<tr class='$rowcss tabletext'>";
	    	$display .= "<td>".$thisClassName."</td>";
	    	$display .= "<td>".$thisClassNumber."</td>";
	    	$display .= "<td>".$thisStudentName."</td>";
	    	$display .= "<td>".$thisGenerationStatusDisplay."</td>";
	    	$display .= "<td>".$thisFinalStatusDisplay."</td>";
	    $display .= "</tr>";
	    
	    $counter_j = 0;
	    $DisplayDataArr[$counter_i][$counter_j++] = $thisClassName;
	    $DisplayDataArr[$counter_i][$counter_j++] = $thisClassNumber;
	    $DisplayDataArr[$counter_i][$counter_j++] = $thisStudentName;
	    $DisplayDataArr[$counter_i][$counter_j++] = $thisGenerationStatusDisplay;
	    $DisplayDataArr[$counter_i][$counter_j++] = $thisFinalStatusDisplay;
	    $counter_i++;
	}
	
	if ($counter_i == 0)
		$display .= "<tr class='tablerow1'><td class='tabletext' colspan='5' align='center'>".$Lang['General']['NoRecordFound']."</td></tr>";
		
$display .= "</table>";

# tag information
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($eReportCard['Management_Promotion'], "", 0);

$linterface->LAYOUT_START();
?>

<script>
//function js_Reload_Selection(jsYearID)
//{
//	$('#ClassSelectionSpan>select').attr("disabled","disabled");
//		
//	$('#ClassSelectionSpan').load(
//		"../../reports/ajax_reload_selection.php", 
//		{ 
//			RecordType: 'Class',
//			YearID: jsYearID,
//			SelectionID: 'YearClassID',
//			isMultiple: 0,
//			isAll: 1,
//			onchange :'this.form.submit()'
//		},
//		function(ReturnData)
//		{
//			$('#ClassSelectionSpan>select').attr("disabled","");
//			//SelectAll(document.form1.elements['YearClassIDArr[]']);
//			//$('#ClassSelectionSpan').show();
//			//$('#IndexDebugArea').html(ReturnData);
//		}
//	);
//
//}

function js_Reload()
{
	var ObjForm = document.getElementById('FormMain');
	ObjForm.action = 'index.php';
	ObjForm.target = '_self';
	ObjForm.submit();
}

function js_Go_Export()
{
	var ObjForm = document.getElementById('FormMain');
	ObjForm.action = 'export.php';
	ObjForm.target = '_self';
	ObjForm.submit();
}

function js_Go_Print()
{
	var ObjForm = document.getElementById('FormMain');
	ObjForm.action = 'print.php';
	ObjForm.target = '_blank';
	ObjForm.submit();
}
</script>

<br/>
<form id="FormMain" name="FormMain" method="post" >
<table width="98%" border="0" cellpadding="4" cellspacing="0">
  <tr>
    <td align="center">
	  <table width="100%" border="0" cellspacing="0" cellpadding="2">
	  	<tr>
	      <td>
	      	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="right">
              <tr>
                <td><?=$SysMsg?></td>
              </tr>
            </table>
	      	<br>
	      	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	      	  <tr>
	          	<td class="tab_underline"><?=$display_tagstype?></td>
	          </tr>
	      	</table>
	      	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <tr>
	          	<td>
	              <table width="100%" border="0" cellspacing="0" cellpadding="2">
	              	<tr>
   	              	  <td>
   	              	  	<br>
						<?=$FormSelection?> 
						<span id="ClassSelectionSpan"><?=$ClassSelection?></span> 
						<?=$StatusSelection?>
	              	  </td>
	              	</tr>
	              	<tr>
	              		<td><?=$showRemark?></td>
	              	</tr>
	              	<tr>
	              		<td><?=$toolbar?></td>
	              	</tr>
	              	<tr><td>&nbsp;</td></tr>
	              	<tr>
	              		<td class='tabletextremark'><?=$GenerationDate?></td>
	              	</tr>
	        	  </table>
	         	 </td>
	          </tr>
	      	</table>
	      </td>
	  	</tr>
	  	<tr>
	      <td>
	      <!-- Start of Content -->
	      <?=$display?>
	      <!-- End of Content -->
	      </td>
	  	</tr>
	  </table>
	  <br>
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="1" class="dotline"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
        </tr>
      </table>
	</td>
  </tr>
</table>

<input type="hidden" id="DisplayDataArr" name="DisplayDataArr" value="<?=rawurlencode(serialize($DisplayDataArr));?>">

</form>
<br />
<br />

<?
	$linterface->LAYOUT_STOP();
?>