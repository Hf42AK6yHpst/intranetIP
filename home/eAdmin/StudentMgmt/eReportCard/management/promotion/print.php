<?
// Using: Bill

/***************************************************
 * 	Modification log:
 * 	20170519 Bill:
 * 	- Allow View Group to access
 * *************************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "VIEW_GROUP");

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}
	
if ($lreportcard->hasAccessRight()) 
{
	$YearID = $_REQUEST['YearID'];
	$YearClassID = $_REQUEST['YearClassID'];
	$DisplayDataArr = unserialize(rawurldecode($_REQUEST['DisplayDataArr']));
	
	### Report Title
	$ReportTitle .= $eReportCard["PromotionStatusReport"];
	
	### Report Info
	$ReportInfo = '';
	
	$ObjForm = new Year($YearID);
	$FormName .= $ObjForm->YearName;
	$ReportInfo .= $eReportCard['Form'].' : '.$FormName;
	
	if ($YearClassID != '')
	{
		$ObjClass = new year_class($YearClassID);
		$ClassName .= $ObjClass->Get_Class_Name();
		$ReportInfo .= '<br />'.$eReportCard['Class'].' : '.$ClassName;
	}
	
	$table = '';
	$table .= "<table id='ResultTable' class='GrandMSContentTable' width='100%' border='1' cellpadding='2' cellspacing='0'>\n";
		$table .= "<thead>\n";
			$table .= "<tr>\n";
				$table .= "<th>#</th>\n";
				$table .= "<th>".$eReportCard['Class']."</th>\n";
				$table .= "<th>".$eReportCard['ClassNo']."</th>\n";
				$table .= "<th>".$eReportCard['Student']."</th>\n";
				$table .= "<th>".$eReportCard['GenerationStatus']."</th>\n";
				$table .= "<th>".$eReportCard['FinalStatus']."</th>\n";
			$table .= "</tr>\n";
		$table .= "</thead>\n";
		
		$table .= "<tbody>\n";
		$counter = 0;
			foreach ($DisplayDataArr as $row => $rowDataArr)
			{
				$counter++;
				
				$table .= "<tr>\n";
					$table .= "<td align='center'>".$counter."</td>\n";
					
					foreach ($rowDataArr as $column => $cellData)
					{
						$table .= "<td align='center'>".$cellData."</td>\n";
					}
				$table .= "</tr>\n";
			}
		$table .= "</tbody>\n";
	$table .= "</table>\n";
	
	$css = $lreportcard->Get_GrandMS_CSS();
	
	$allTable = '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="center"><h1>'.$ReportTitle.'</h1></td>
					</tr>
					<tr><td class="tabletext">'.$ReportInfo.'</td></tr>
					<tr><td>'.$table.'</td></tr>
				</table>';
		
	echo $lreportcard->Get_Report_Header($ReportTitle);
	echo $allTable.$css;
	echo $lreportcard->Get_Report_Footer();	
}
?>