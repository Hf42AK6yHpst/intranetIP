<?php
// Using: Bill

/***************************************************
 * 	Modification log:
 * 	20170519 Bill:
 * 	- Allow View Group to access
 * *************************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "VIEW_GROUP");

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcard();
	
if ($lreportcard->hasAccessRight()) 
{
	$lexport = new libexporttext();
	
	$YearID = $_REQUEST['YearID'];
	$YearClassID = $_REQUEST['YearClassID'];
	$ExportArr = unserialize(rawurldecode($_REQUEST['DisplayDataArr']));
	
	### If no selected class, show form name in the file name. Otherwise, show the class name
	if ($YearClassID == '')
	{
		$ObjForm = new Year($YearID);
		$FileNameInfo = $ObjForm->YearName;
	}
	else
	{
		$ObjClass = new year_class($YearClassID);
		$FileNameInfo = $ObjClass->Get_Class_Name();
	}
	
	### Export Content
	$exportColumn = array("ClassName", "ClassNumber", "StudentName", "GenerationStatus", "FinalStatus");
	$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn);
	
	intranet_closedb();
	
	// Output the file to user browser
	$filename = "promotion_status_$FileNameInfo.csv";
	$filename = str_replace(' ', '_', $filename);
	$lexport->EXPORT_FILE($filename, $export_content);
}
?>