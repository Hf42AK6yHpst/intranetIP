<?php
# Editing by Bill 
/***************************************************
 * 	Modification log:
 * 	20160120 Bill:	[2015-0421-1144-05164]
 * 	- Create File 
 * 		$eRCTemplateSetting['Report']['ReportGeneration']['GeneratePromotionStatus_CustomizedLogic']
 * *************************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcard();
$lreportcard_ui = new libreportcard_ui();

if(!$plugin['ReportCard2008'] || !$lreportcard->hasAccessRight() || !$eRCTemplateSetting['Report']['PromotionSummaryReport'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit;
}

$linterface = new interface_html();
$CurrentPage = "Management_Promotion";

# Get Tag
$display_tagstype = '';
$display_tagstype .= '
  <div class="shadetabs">
    <ul>';
$display_tagstype .= '<li><a href="index.php">'.$eReportCard['List'].'</a></li>';
$display_tagstype .= '<li><a href="generate.php">'.$eReportCard['Generate'].'</a></li>';
$display_tagstype .= '<li class="selected"><a href="report_index.php">'.$Lang['eReportCard']['ReportArr']['PromotionArr']['ReportName'].'</a></li>';
$display_tagstype .= '
    </ul>
  </div>';

# Get Form selection (show form which has report card only)
$libForm = new Year();
$FormArr = $libForm->Get_All_Year_List();
$YearID = $YearID? $YearID : $FormArr[0]['YearID'];

$FormSelection = $lreportcard_ui->Get_Form_Selection('YearID', $YearID, 'js_Reload();', $noFirst=1);

# Tag information
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($eReportCard['Management_Promotion'], "", 0);

$linterface->LAYOUT_START();
?>

<script>

function submitForm(format)
{
	$('input#format').val(format);
	
	ObjForm = document.getElementById('FormMain');
	if(format == 'csv'){
		ObjForm.action = 'report_print.php';
		ObjForm.target = '_self';
		ObjForm.submit();
	}
	else {
		ObjForm.action = 'report_print.php';
		ObjForm.target = '_blank';
		ObjForm.submit();
	}
}

</script>

<br/>
<form id="FormMain" name="FormMain" method="post" action="report_print.php">
  	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  	<tr>
      	<td class="tab_underline"><?=$display_tagstype?></td>
    </tr>
  	</table>
	<table class="form_table_v30">
		<!-- Year -->
		<tr valign="top">
			<td class="field_title"><?=$Lang['eDiscipline']['TKPClassDemeritRecord']['SchoolYear']?></td>
			<td><?=$lreportcard->GET_ACTIVE_YEAR_NAME()?></td>
		</tr>
		<!-- Form -->
		<tr valign="top">
			<td class="field_title"><span class="tabletextrequire">*</span><?=$eReportCard['Form'] ?></td>
			<td><?=$FormSelection?></td>
		</tr>
	</table>
	
	<?=$linterface->MandatoryField();?>
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "submitForm('');")?>
		&nbsp;
		<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Export'], "button", "submitForm('csv');")?>
	</div>
		
	<input type="hidden" name="format" id="format" value="pdf">
</form>
<br />
<br />

<?
	$linterface->LAYOUT_STOP();
?>