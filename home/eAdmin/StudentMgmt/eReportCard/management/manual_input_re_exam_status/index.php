<?php
# Using : Bill
/***************************************************
 * 	Modification log:
 * *************************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "VIEW_GROUP");

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
    include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
}

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcard();
$lreportcard_ui = new libreportcard_ui();

if(!$plugin['ReportCard2008'] || !$lreportcard->hasAccessRight())
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    intranet_closedb();
    exit;
}

$CurrentPage = "Management_ManualInputReExamStatus";
$linterface = new interface_html();

### Get Form selection (show form which has report card only)
$FormArr = $lreportcard->GET_ALL_FORMS(1, false);
$YearID = $YearID ? $YearID : $FormArr[0]['ClassLevelID'];
$FormSelection = $lreportcard_ui->Get_Form_Selection('YearID', $YearID, 'js_Reload();', $noFirst=1);

### Get Class selection
$ObjClass = new year_class($YearClassID);
if ($ObjClass->YearID != $YearID) {
    // User has switched the form selection
    $YearClassID = 0;
}
$libFCP_ui = new form_class_manage_ui();
$ClassSelection = $libFCP_ui->Get_Class_Selection($lreportcard_ui->schoolYearID, $YearID, 'YearClassID',$YearClassID, 'js_Reload();', $noFirst=0, $isMultiple=0, $isAll=1);

### Get Status Selection
$StatusSelection = $lreportcard_ui->GetPromotionStatusSelection("PromotionStatus","js_Reload();", $PromotionStatus, $isAll=1, $noFirst=0, $isMultiple=0,  $OtherTagInfo='');

### Gen Promotion Status Table
if($YearClassID != '' && $YearClassID != 0) {
    $StudentArr = $lreportcard->GET_STUDENT_BY_CLASS($YearClassID);
} else {
    $StudentArr = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $YearID);
}
$PromotionStatusAry = $lreportcard->GetPromotionStatusList($YearID, $YearClassID);

$display = '';
$display .= "<table border='0' cellspacing='0' cellpadding='4' width='100%'>";

# Table head
$display .= "<tr class='tabletop'>";
    $display .= "<td class='tabletopnolink' width='15%'>".$eReportCard["Class"]."</td>";
    $display .= "<td class='tabletopnolink' width='10%'>".$eReportCard["ClassNo"]."</td>";
    $display .= "<td class='tabletopnolink' width='25%'>".$eReportCard["Student"]."</td>";
    $display .= "<td class='tabletopnolink' width='50%'>".$eReportCard["FinalStatus"]."</td>";
$display .= "</tr>";

$edit_display = '';
$edit_display .= "<table border='0' cellspacing='0' cellpadding='4' width='100%' class='common_table_list_v30 edit_table_list_v30'>";

# Table head
$edit_display .= "<tr class='tabletop'>";
    $edit_display .= "<th class='tabletopnolink' width='15%'>".$eReportCard["Class"]."</th>";
    $edit_display .= "<th class='tabletopnolink' width='10%'>".$eReportCard["ClassNo"]."</th>";
    $edit_display .= "<th class='tabletopnolink' width='25%'>".$eReportCard["Student"]."</th>";
    $edit_display .= "<th class='tabletopnolink' width='50%'>".$eReportCard["FinalStatus"]."</th>";
$edit_display .= "</tr>";

# Table body
$DisplayDataArr = array();
$counter_i = 0;
$counter_j = 0;
foreach((array)$StudentArr as $key => $thisStudentInfoArr)
{
    # Get Student Info
    $thisStudentID = $thisStudentInfoArr['UserID'];
    $thisStudentName = $thisStudentInfoArr['StudentName'];
    $thisClassName = Get_Lang_Selection($thisStudentInfoArr['ClassTitleCh'], $thisStudentInfoArr['ClassTitleEn']);
    $thisClassNumber = $thisStudentInfoArr['ClassNumber'];

    # Get Student Promotion Info
    $thisPromotionStatusArr = $PromotionStatusAry[$thisStudentID];
    $thisPromotionStatus = $thisPromotionStatusArr['Promotion'];
    $thisFinalStatusText = $thisPromotionStatusArr['GenerationStatus'];

    # Skip Student according to the Status filter
    if($PromotionStatus != 0 && $thisPromotionStatus != $PromotionStatus) {
        continue;
    }

    # Status Display
    //$thisFinalStatusDisplay = ($thisFinalStatusText == '') ? '--' : $thisFinalStatusText;
    $thisFinalStatusDisplay = '--';
    if($thisPromotionStatus) {
        $thisFinalStatusDisplay = $eReportCard["ManualInputReExamStatus"][$thisPromotionStatus];
    }

    $rowcss = "tablerow".(($counter_i % 2) + 1);
    $display .= "<tr class='$rowcss tabletext'>";
        $display .= "<td>".$thisClassName."</td>";
        $display .= "<td>".$thisClassNumber."</td>";
        $display .= "<td>".$thisStudentName."</td>";
        $display .= "<td>".$thisFinalStatusDisplay."</td>";
    $display .= "</tr>";

    $edit_display .= "<tr class='$rowcss tabletext'>";
        $edit_display .= "<td>".$thisClassName."</td>";
        $edit_display .= "<td>".$thisClassNumber."</td>";
        $edit_display .= "<td>".$thisStudentName."</td>";
        $edit_display .= "<td>";

        foreach($eRCTemplateSetting['ReportGeneration']['PromoteInputMapping'] as $thisPromotionType => $thisPromotionValue)
        {
            $checkbox_id = 'promotion_'.$thisStudentID.'_'.$thisPromotionValue;
            $checkbox_name = 'promotionArr['.$thisStudentID.']';
            $checkbox_checked = ($thisPromotionStatus == $thisPromotionValue) || ($thisPromotionStatus == '' && $thisPromotionValue == 1);

            $edit_display .= '<input type="radio" value="'.$thisPromotionValue.'" name="'.$checkbox_name.'" id="'.$checkbox_id.'" '.($checkbox_checked ? 'checked' : '').'><label for="'.$checkbox_id.'">'.$eReportCard["ManualInputReExamStatus"][$thisPromotionValue].'</label> &nbsp;&nbsp;&nbsp;&nbsp;';
        }
        $edit_display .= "</td>";
    $edit_display .= "</tr>";

    $counter_j = 0;
    $DisplayDataArr[$counter_i][$counter_j++] = $thisClassName;
    $DisplayDataArr[$counter_i][$counter_j++] = $thisClassNumber;
    $DisplayDataArr[$counter_i][$counter_j++] = $thisStudentName;
    $DisplayDataArr[$counter_i][$counter_j++] = $thisFinalStatusDisplay;
    $counter_i++;
}

if ($counter_i == 0) {
    $display .= "<tr class='tablerow1'><td class='tabletext' colspan='5' align='center'>".$Lang['General']['NoRecordFound']."</td></tr>";
}
$display .= "</table>";
$edit_display .= "</table>";

# Tag information
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($eReportCard['Management_ManualInputReExamStatus'], "", 0);

$linterface->LAYOUT_START();
?>

<script>
    function js_Reload()
    {
        var ObjForm = document.getElementById('FormMain');
        ObjForm.action = 'index.php';
        ObjForm.target = '_self';
        ObjForm.submit();
    }

    function jsEdit()
    {
        $('.Edit_Show').show();
        $('.Edit_Hide').hide();
    }

    function jsCancel()
    {
        // document.form1.reset();
        $('.Edit_Hide').show();
        $('.Edit_Show').hide();
    }

    function js_Check_Form()
    {
        $('input#submitBtn').attr('disabled', true);
        $('form#FormMain').attr('action', 'update.php');
    }
</script>

<br/>
<form id="FormMain" name="FormMain" action="update.php" method="POST" onsubmit="return js_Check_Form();">
    <table width="98%" border="0" cellpadding="4" cellspacing="0">
        <tr>
            <td align="center">
                <table width="100%" border="0" cellspacing="0" cellpadding="2">
                    <tr>
                        <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="right">
                                <tr>
                                    <td><?=$SysMsg?></td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="2">
                                            <tr>
                                                <td>
                                                    <br>
                                                    <?=$FormSelection?>
                                                    <span id="ClassSelectionSpan"><?=$ClassSelection?></span>
                                                    <?=$StatusSelection?>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <!-- Start of Content -->
                            <div class="table_board Edit_Hide">
                                <?=$display?>
                            </div>
                            <div class="table_board Edit_Show" style="display: none">
                                <?=$edit_display?>
                            </div>
                            <!-- End of Content -->
                        </td>
                    </tr>
                </table>
                <br>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td height="1" class="dotline"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <div class="edit_bottom_v30 Edit_Hide">
        <?php if(empty($StudentArr)) { ?>

        <?php } else if (empty($PromotionStatusAry)) { ?>
            <?php echo $eReportCard["ReportNotGenerated"]; ?>
        <?php } else { ?>
            <?php echo $linterface->Get_Action_Btn($Lang['Btn']['Edit'], "button", "jsEdit()"); ?>
        <?php } ?>
    </div>
    <div class="edit_bottom_v30 Edit_Show" style="display: none">
        <?php echo $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "submit", "", "submitBtn"); ?>
        <?php echo $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "jsCancel()"); ?>
    </div>

    <input type="hidden" id="DisplayDataArr" name="DisplayDataArr" value="<?=rawurlencode(serialize($DisplayDataArr));?>">
</form>
<br />
<br />

<?
$linterface->LAYOUT_STOP();
?>