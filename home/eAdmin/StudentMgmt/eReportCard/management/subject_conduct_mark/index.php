<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	echo "You have no priviledge to access this page.";
}
	
	
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
}
else
{
	$lreportcard = new libreportcard();
}

$CurrentPage = "Management_SubjectConductMark";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

if (!$lreportcard->hasAccessRight())
{
	echo "You have no priviledge to access this page.";
}

$linterface = new interface_html();
$PAGE_TITLE = $eReportCard['Management_ConductMark'];

# tag information
$TAGS_OBJ[] = array($PAGE_TITLE, "", 0);
$linterface->LAYOUT_START();


## Form selection
$FormArr = $lreportcard->GET_ALL_FORMS(1);
$ClassLevelID = ($ClassLevelID == "") ? $FormArr[0][0] : $ClassLevelID;

if (count($FormArr) > 0)
	$FormSelection = $linterface->GET_SELECTION_BOX($FormArr, "name='ClassLevelID' id='ClassLevelID' class='tabletexttoptext' onchange='jCHANGE_RE_EXAM_LIST()'", "", $ClassLevelID);


## Class selection
# if the selected class is in the selected form, show that class
# if the selected class is not in the selected form, show the first class of the selected form
$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
$numOfClass = count($ClassArr);

$isClassInSelectedForm = false;
for ($i=0; $i<$numOfClass; $i++)
{
	$thisClassID = $ClassArr[$i]['ClassID'];
	
	if ($thisClassID == $ClassID)
	{
		$isClassInSelectedForm = true;
		break;
	}
}

if (!$isClassInSelectedForm)
{
	$ClassID = $ClassArr[0][0];
}

if (count($ClassArr) > 0)
	$ClassSelection = $linterface->GET_SELECTION_BOX($ClassArr, "name='ClassID' id='ClassID' class='tabletexttoptext' onchange='jCHANGE_RE_EXAM_LIST()'", "", $ClassID);



## Report selection
$ReportTypeSelection = '';
$ReportTypeArr = array();
$ReportTypeOption = array();
$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);

$ReportType = '';
if(count($ReportTypeArr) > 0){
	$reportCounter = 0;
	for($j=0 ; $j<count($ReportTypeArr) ; $j++){
		if($ReportTypeArr[$j]['ReportID'] == $ReportID){
			$isFormReportType = 1;
			$ReportType = $ReportTypeArr[$j]['Semester'];
		}
		
		$ReportTypeOption[$reportCounter][0] = $ReportTypeArr[$j]['ReportID'];
		$ReportTypeOption[$reportCounter][1] = $ReportTypeArr[$j]['SemesterTitle'];
		$reportCounter++;
	}
	$ReportTypeSelection .= $linterface->GET_SELECTION_BOX($ReportTypeOption, 'name="ReportID" id="ReportID" class="tabletexttoptext" onchange="jCHANGE_RE_EXAM_LIST()"', '', $ReportID);
	
	$ReportType = ($ReportType != "") ? $ReportType : $ReportTypeArr[0]['Semester'];
}
$ReportID = ($isFormReportType) ? $ReportID : $ReportTypeOption[0][0];



############################## Data Grathering #######################################
## Get student info and conduct mark of each subject for each studetns
# retrieve student info
$StudentInfoArr = $lreportcard->GET_STUDENT_BY_CLASS($ClassID, "", 1);
$numOfStudent = count($StudentInfoArr);

$StudentIDArr = array();
for ($i=0; $i<$numOfStudent; $i++)
{
	$thisStudentID = $StudentInfoArr[$i]['UserID'];
	$StudentIDArr[] = $thisStudentID;
}

# retrieve subjects
$MainSubjectArr = $lreportcard->returnSubjectwOrder($ClassLevelID, 0, "", $intranet_session_language);
if(sizeof($MainSubjectArr) > 0)
	foreach($MainSubjectArr as $MainSubjectIDArr[] => $MainSubjectNameArr[]);

$numOfMainSubject = count($MainSubjectIDArr);


# get conduct mark of each subject and get finalizied conduct mark if there are any
$thisFinalConductInfoArr = $lreportcard->Get_Student_Final_Conduct_Grade($StudentIDArr, $ReportID);
for ($i=0; $i<$numOfMainSubject; $i++)
{
	$thisSubjectID = $MainSubjectIDArr[$i];	
	$thisSubjectConductMarkArr = $lreportcard->GET_EXTRA_SUBJECT_INFO($StudentIDArr, $thisSubjectID, $ReportID);
	
	for ($j=0; $j<$numOfStudent; $j++)
	{
		$thisStudentID = $StudentInfoArr[$j]['UserID'];
		$thisConductMark = $thisSubjectConductMarkArr[$thisStudentID]["Info"];
		$thisFinalConductMark = $thisFinalConductInfoArr[$thisStudentID]["Conduct"];
		
		$StudentInfoArr[$j][$thisSubjectID] = $thisConductMark;
		$StudentInfoArr[$j]['FinalGrade'] = $thisFinalConductMark;
	}
}

# Calculate converted conduct grade
for ($i=0; $i<$numOfStudent; $i++)
{
	$thisSubjectCount = 0;
	$thisConductTotal = 0;
	for ($j=0; $j<$numOfMainSubject; $j++)
	{
		$thisSubjectID = $MainSubjectIDArr[$j];
		$thisConduct = $StudentInfoArr[$i][$thisSubjectID];
		if ($thisConduct!="" && is_numeric($thisConduct))
		{
			$thisSubjectCount++;
			$thisConductTotal += $thisConduct;
		}
	}
	
	if ($thisSubjectCount != 0)
	{
		$thisConductAverage = my_round($thisConductTotal / $thisSubjectCount, 2);
	}
	else
	{
		$thisConductAverage = 0;
	}
	$thisConductGrade = $lreportcard->convertConductMarkToGrade($thisConductAverage, $ClassLevelID);
	
	$StudentInfoArr[$i]['ConductAverage'] = $thisConductAverage;
	$StudentInfoArr[$i]['ConvertedGrade'] = $thisConductGrade;
}
############################### End of Data Grathering###########################################


## Construct table
$infoTable = "";
$ExportArr = array();

$infoTable .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">";
	# Title
	$infoTable .= "<tr class=\"tablegreentop tabletoplink\">";
		$infoTable .= "<td width=\"7%\">".$i_UserClassNumber."</td>";
		$ExportArr[0][] = $i_UserClassNumber;
		
		$infoTable .= "<td width=\"15%\">".$i_UserStudentName."</td>";
		$ExportArr[0][] = $i_UserStudentName;
		
		for ($i=0; $i<$numOfMainSubject; $i++)
		{
			$thisSubjectName = $MainSubjectNameArr[$i][0];
			
			$infoTable .= "<td align=\"center\">".$thisSubjectName."</td>";
			$ExportArr[0][] = $thisSubjectName;
		}
		
		$infoTable .= "<td align=\"center\">".$eReportCard['Template']['Average']."</td>";
		$ExportArr[0][] = $eReportCard['Template']['Average'];
		
		$infoTable .= "<td align=\"center\">".$eReportCard['ConvertedGrade']."</td>";
		$ExportArr[0][] = $eReportCard['ConvertedGrade'];
		
		$infoTable .= "<td width=\"20%\" align=\"center\">".$eReportCard['FinalGrade']."</td>";
		$ExportArr[0][] = $eReportCard['FinalGrade'];
	$infoTable .= "</tr>";
	
	# Content
	
	for ($i=0; $i<$numOfStudent; $i++)
	{
		$thisStudentID = $StudentInfoArr[$i]['UserID'];
		$thisClassNumber = $StudentInfoArr[$i]['ClassNumber'];
		$thisStudentName = $StudentInfoArr[$i]['StudentName'];
		$thisAverage = $StudentInfoArr[$i]['ConductAverage'];
		$thisConvertedGrade = $StudentInfoArr[$i]['ConvertedGrade'];
		$thisFinalGrade = $StudentInfoArr[$i]['FinalGrade'];
		
		$tr_css = ($i % 2 == 1) ? "tablegreenrow2 tabletext" : "tablegreenrow1 tabletext";
		$td_css = ($i % 2 == 1) ? "attendanceouting" : "attendancepresent";
		
		$infoTable .= "<tr class=\"".$tr_css."\">";
			# Class Number
			$infoTable .= "<td class=\"".$td_css."\">".$thisClassNumber."</td>";
			$ExportArr[$i+1][] = $thisClassNumber;
			
			# Student Name
			$infoTable .= "<td class=\"".$td_css."\">".$thisStudentName."</td>";
			$ExportArr[$i+1][] = $thisStudentName;
			
			# Subject Grade
			for ($j=0; $j<$numOfMainSubject; $j++)
			{
				$thisSubjectID = $MainSubjectIDArr[$j];
				$thisConduct = ($StudentInfoArr[$i][$thisSubjectID])? $StudentInfoArr[$i][$thisSubjectID] : "---";
				
				$infoTable .= "<td class=\"".$td_css."\" align='center'>".$thisConduct."</td>";
				$ExportArr[$i+1][] = $thisConduct;
			}
			
			# Average
			$infoTable .= "<td class=\"".$td_css."\" align='center'>".$thisAverage."</td>";
			$ExportArr[$i+1][] = $thisAverage;
			
			# Grade
			$infoTable .= "<td class=\"".$td_css."\" align='center'>".$thisConvertedGrade."</td>";
			$ExportArr[$i+1][] = $thisConvertedGrade;
			
			# Final Grade
			$thisFinalGrade = ($thisFinalGrade=="")? $thisConvertedGrade : $thisFinalGrade;
			$infoTable .= "<td class=\"".$td_css."\" align='center'>";
			
				$InputVal = $thisFinalGrade;
			    $mark_name = "finalGrade_".$thisStudentID;
			    $mark_id = "finalGrade[".$i."][0]";
			    
			    $otherPar = array(	'class'=>'textboxnum', 
			    					'onBlur'=>'if(!setStart) { this.value=startContent; setStart = 1}', 
			    					'onpaste'=>'pasteTable(\''.$i.'\', \'0\')'
			    				);
			    $infoTable .= $lreportcard->GET_INPUT_TEXT($mark_name, $mark_id, $InputVal, '', $otherPar);
				    	
				//$infoTable .= "<input name='finalGrade_".$thisStudentID."' type='textboxnum' class='tabletext' value='".$thisFinalGrade."' size='2' maxlength='3' />";
			$infoTable .= "</td>";
			$ExportArr[$i+1][] = $thisFinalGrade;
			
		$infoTable .= "</tr>";
	}
$infoTable .= "</table>";

## Last Modified display
$LastModifiedDate = $thisFinalConductInfoArr[$StudentIDArr[0]]['DateModified'];
$LastModifiedDate = ($LastModifiedDate=="")? "-" : $LastModifiedDate;
$LastModifiedDisplay = "";
$LastModifiedDisplay .= $eReportCard['LastModifiedDate'].": ".$LastModifiedDate;

## Warning
$warningHTML = "";
$warningHTML .= $lreportcard->showWarningMsg("<font color='red'>".$eReportCard['Warning']."</font>", $eReportCard['SubjectConductMarkWarning'], "class='tabletext'");

## Tool bar
$toolbar = "";
$toolbar .= $linterface->GET_LNK_EXPORT("javascript: goExport()");
$toolbar .= toolBarSpacer();
$toolbar .= toolBarSpacer();
$toolbar .= $linterface->GET_LNK_PRINT("javascript: goPrint()", $button_print);

## Buttons
if (count($FormArr) > 0)
	$ButtonHTML = $linterface->GET_ACTION_BTN($button_submit, "button", "javascript:js_Submit()", "btnSubmit");

## SysMsg
$SysMsg = $linterface->GET_SYS_MSG($Result);

?>


<script language="javascript">

function jGET_SELECTBOX_SELECTED_VALUE(objName){
	var obj = document.getElementById(objName);
	if(obj==null)	return "";
	var index = obj.selectedIndex;
	var val = obj[index].value;
	return val;
}

function jCHANGE_RE_EXAM_LIST()
{
	var form_id = jGET_SELECTBOX_SELECTED_VALUE("ClassLevelID");
	var class_id = jGET_SELECTBOX_SELECTED_VALUE("ClassID");
	var report_id = jGET_SELECTBOX_SELECTED_VALUE("ReportID");
	
	location.href = "?ClassLevelID="+form_id+"&ClassID="+class_id+"&ReportID="+report_id;
}

function js_Submit()
{
	document.form1.action = "conduct_grade_update.php";
	document.form1.target = "_self";
	document.form1.submit();
}

function goExport()
{
	document.form1.action = "export.php";
	document.form1.target = "_self";
	document.form1.submit();
}

function goPrint()
{
	document.form1.action = "print.php";
	document.form1.target = "_blank";
	document.form1.submit();
}




var rowx = 0, coly = 0;			//init
var xno = "0";yno = "<?=$numOfStudent?>";		// set table size

var startContent = "";
var setStart = 1;
function pasteTable(jPari, jParj) {
	setxy(jPari, jParj);
	paste_clipboard();
}

function setxy(jParX, jParY){
	rowx = jParX;
	coly = jParY;
	document.getElementById('finalGrade['+ jParX +']['+ jParY +']').blur();
}

function paste_clipboard() {

	var temp1 = null;
	var text1 = null;
	var s = "";

	// you must use a 'text' field
	// (a 'hidden' text field will not show anything)

	temp1 = document.getElementById('text1').createTextRange();
	temp1.execCommand( 'Paste' );

	s = document.getElementById('text1').value; // clipboard is pasted to a string here
	setStart = 0;

	document.getElementById('finalGrade['+ (rowx) +']['+ (coly) +']').value = "";

	var row_array = s.split("\n");
	var row_num = 0;

	while (row_num < row_array.length)
	{
		var col_array = row_array[row_num].split("\t");
		var col_num = 0;
		var xPos=0, yPos=0;
		while (col_num < col_array.length)
		{
			if ((col_num == 0) && (row_num == 0)) startContent = Trim(col_array[0]);
			if ((row_array[row_num] != "")&&(col_array[col_num] != ""))
			{
				var temp = document.getElementById('finalGrade['+ (row_num + rowx) +']['+ (col_num + coly) +']');
				xPos = parseInt(row_num) + parseInt(rowx);
				yPos = parseInt(col_num) + parseInt(coly);
				if (document.getElementById('finalGrade['+ xPos +']['+ yPos +']') != null)
				{
					document.getElementById('finalGrade['+ xPos +']['+ yPos +']').value = Trim(col_array[col_num]);
				}
			}
			col_num+=1;
		}
		row_num+=1;
	}
}

</script>

<br/>
<form name="form1" method="post">
	<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="7" width="90%" align="center"><?= $warningHTML ?></td>
					</tr>
					<tr><td colspan="7">&nbsp;</td></tr>
					<tr>
						<td colspan="6"><?=$toolbar?></td>
						<td align="right" width="100%"><?=$SysMsg?></td>
					</tr>
					<tr>
						<td><?=$FormSelection?></td>
						<td>&nbsp;</td>
						<td><?=$ClassSelection?></td>
						<td>&nbsp;</td>
						<td><?=$ReportTypeSelection?></td>
						<td>&nbsp;</td>
						<td align="right" width="100%" class="tabletext"><b><?=$LastModifiedDisplay?></b></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		
		<tr><td><?= $infoTable ?></td></tr>
		<tr><td>&nbsp;</td></tr>
		
		<tr>
			<td>
				<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
					<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
					<tr><td align="center"><?=$ButtonHTML?></td></tr>
				</table>
			</td>
		</tr>

	</table>
	
	<input type="hidden" name="StudentIDArr" id="StudentIDArr" value="<?= rawurlencode(serialize($StudentIDArr)) ?>" />
	<input type="hidden" name="ExportArr" id="ExportArr" value="<?= rawurlencode(serialize($ExportArr)) ?>" />
	<input type="hidden" name="LastModifiedDate" id="LastModifiedDate" value="<?= $LastModifiedDate ?>" />
	
	<textarea style="height: 1px; width: 1px;" id="text1" name="text1"></textarea>

</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>