<?php
# using: 

####################################################
#
#	Date:	2016-12-08	Bill
#			- fixed cannot select multiple students
#
#	Date:	2016-08-05	Bill	[2016-0330-1524-42164]
#			- Student drop down list need to select 1 student only when $eRCTemplateSetting['Report']['ArchiveReport_PrintOneStudentOnly'] = true
#			- Submit to print_preview_by_pdf.php when $eRCTemplateSetting['Report']['ExportPDFReport'] = true
#
####################################################

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");

$lreportcard = new libreportcard2008j();
$linterface = new interface_html();

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}


# Main
$ReportSetting = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$ClassLevelID = $ReportSetting['ClassLevelID'];

if($ClassLevelID != ""){
	# Get Class
	$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
	# Filters - By Form (ClassLevelID)
	if($eRCTemplateSetting['Report']['ArchiveReport_PrintOneStudentOnly'])
		$ClassOptions[] = array("", "-- ".$Lang['Btn']['Select']." --");
	else
		$ClassOptions[] = array("", "--All--");
	if(count($ClassArr) > 0 ){
		for($i=0 ; $i<count($ClassArr) ; $i++){
			$ClassOptions[] = array($ClassArr[$i][0], $ClassArr[$i][1]);
		}
	}
	$ClassSelection = $linterface->GET_SELECTION_BOX($ClassOptions, "name='ClassID' id='ClassID' class='tabletexttoptext' onchange='jFILTER_CHANGE(0)'", "", $ClassID);
	
	if($ClassID!="")
	{
		$isMultiple = $eRCTemplateSetting['Report']['ArchiveReport_PrintOneStudentOnly']? 0 : 1;
		$StudentSelection = $lreportcard->GET_REPORT_STUDENT_SELECTION($ClassID, "", "", $isMultiple);
		$StudentRow = "<tr>
						<td valign='top' nowrap='nowrap' ><span class='tabletext'>".$i_identity_student.": </span></td>
						<td width='80%' style='align: left'>
							<table border='0' cellpadding='0' cellspacing='0' align='left'>
								<tr> 
									<td class='tabletext'>".$StudentSelection."</td>".
									(!$eRCTemplateSetting['Report']['ArchiveReport_PrintOneStudentOnly']?
										"<td style='vertical-align:bottom'>        
											<table width='100%' border='0' cellspacing='0' cellpadding='6'>
												<tr> 
													<td align='left'> 
														".$linterface->GET_BTN($button_select_all, 'button', "SelectAll(this.form.elements['TargetStudentID[]']); return false;")."
													</td>
												</tr>
											</table>
										</td>" : "" ).
								"</tr>
							</table>
						</td>
					</tr>";
	}
}

# Customization for Munsang College (Primary) - Choose Report in Student View or Teacher View
$reportTypeRow = "";
if ($ReportCardCustomSchoolName == "munsang_pri")
{
	include_once($intranet_root."/lang/reportcard_custom/munsang_pri.$intranet_session_language.php");
	
	$reportTypeRow .= "<tr>";
	$reportTypeRow .= "<td nowrap=\"nowrap\" class=\"formfieldtitle tabletext\"><span class=\"tabletext\">".$eReportCard['ReportView'].":</span></td>";
	$reportTypeRow .= "<td>";
		$reportTypeRow .= "<input type=\"radio\" name=\"viewType\" value=\"student\" checked><label for=\"student\">".$eReportCard['Student']."</label>";
		$reportTypeRow .= toolBarSpacer();
		$reportTypeRow .= "<input type=\"radio\" name=\"viewType\" value=\"teacher\"><label for=\"teacher\">".$eReportCard['Teacher']."</label>";
	$reportTypeRow .= "</td>";													
	
	$reportTypeRow .= "</tr>";
	
}

# UI
$CurrentPage = "ReportGeneration";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$MODULE_OBJ['title'] = $eReportCard['ReportPrinting'];
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$formMethod = ($eRCTemplateSetting['Reports']['ReportGeneration']['PrintReportMethod']=='')? 'POST' : $eRCTemplateSetting['Reports']['ReportGeneration']['PrintReportMethod'];

?>

<script language="javascript">
<!--
function jFILTER_CHANGE(jChangeType)
{
	obj = document.form1;

	if(jChangeType==1 && typeof(obj.ClassID)!="undefined")
		obj.ClassID.value = "";

	obj.target = "_parent";
	obj.action = "print_menu.php";
	obj.submit();
}

function jDO_PRINT()
{
	obj = document.form1;
	obj.action = "<?=($eRCTemplateSetting['Report']['ExportPDFReport']? "print_preview_by_pdf.php" : "print_preview.php")?>";
	
	<? if($eRCTemplateSetting['Report']['ArchiveReport_PrintOneStudentOnly']){ ?>
		if(obj.ClassID.value > 0 && $('select#TargetStudentID\\[\\]').val() > 0)
		{
			obj.target = "_blank";
			obj.submit();
		}
		else
		{
			if(!obj.ClassID.value)
				alert('<?=$Lang['General']['JS_warning']['SelectClass']?>');
			else
				alert('<?=$Lang['General']['JS_warning']['SelectStudent']?>');
			return false;
		}
	<? } else { ?>
		obj.target = "_blank";
		obj.submit();
	<? } ?>
}

function SelectAll(obj)
{
	for (i=0; i<obj.length; i++)
	{
		obj.options[i].selected = true;
	}
}

<? if($eRCTemplateSetting['Report']['ArchiveReport_PrintOneStudentOnly']){ ?>
$(document).ready(function() {
	if(document.form1.ClassID.value > 0)
		$('select#TargetStudentID\\[\\] option:first-child').attr('selected', 'selected');
});
<? } ?>

-->
</script>

<br/>
<form name="form1" method="<?=$formMethod?>">
<table border="0" cellpadding="0" cellspacing="0" align="center" width="90%">
  <tr>
	<td align="center">
	  <table width="95%" border="0" cellpadding="5" cellspacing="1">
	  	<?=$reportTypeRow?>
	    <tr>
		  <td nowrap="nowrap" class="formfieldtitle tabletext"><?=$eReportCard['Class']?>:</td>
		  <td class="tabletext" width="75%"><?=$ClassSelection?></td>
		<tr>
		<?=$StudentRow?>
	  </table>
	</td>
  </tr>
  <tr>
    <td>
	  <table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	  	<tr>
	      <td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
	  	</tr>
	  	<tr>
		  <td align="center">
			<?= $linterface->GET_ACTION_BTN($button_continue, "button", "javascript:jDO_PRINT()")?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.close()")?>
		  </td>
	  	</tr>
	  </table>
	  <br/>
	</td>
  </tr>
</table>

<input type="hidden" name="ReportID" id="ReportID" value="<?=$ReportID?>" />

</form>

<script>
// Select all students by default
var studentSelectionObj = document.getElementById('TargetStudentID[]');
if (studentSelectionObj)
	SelectAll(studentSelectionObj);
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>