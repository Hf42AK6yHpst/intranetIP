<?php
// Using: 

####################################################
#
#	Date	:	2016-08-05	Bill	[2016-0330-1524-42164]
#				Create File 
#				- Submit to archive_pdf_update.php when $eRCTemplateSetting['Report']['ExportPDFReport'] = true
#
####################################################

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");


intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$ReportTemplateData = $lreportcard->returnReportTemplateBasicInfo($ReportID);

### Gen Report Info
$ReportTitlePieces = explode(":_:",$ReportTemplateData["ReportTitle"]);
$ReportTitle = implode("<br>",$ReportTitlePieces);
$ClassLevelID = $ReportTemplateData["ClassLevelID"];
$SemID = $ReportTemplateData['Semester'];
$ReportType = $SemID == "F" ? "W" : "T";

if($ReportCardCustomSchoolName == "carmel_alison" && $ReportType=="T")
{
	$simpleReportTypeRow .= "<tr>";
	$simpleReportTypeRow .= "<td class=\"field_title\">".$eReportCard['ReportView']."</td>";
	$simpleReportTypeRow .= "<td>";
		$simpleReportTypeRow .= $linterface->Get_Radio_Button("NormalReportRadio", "SimpleReportType", 'normal', 1, "ArchiveReportTypeClass", $eReportCard['Normal']);
		$simpleReportTypeRow .= toolBarSpacer();
		$simpleReportTypeRow .= $linterface->Get_Radio_Button("SimpleReportRadio", "SimpleReportType", 'simple', 0, "ArchiveReportTypeClass", $eReportCard['Simple']);
	$simpleReportTypeRow .= "</td>";													
	
	$simpleReportTypeRow .= "</tr>";
}
else if ($ReportCardCustomSchoolName == "sekolah_menengah_yu_yuan_ma")
{
	$foreign_html .= "<tr>";
	$foreign_html .= "<td class=\"field_title\">".$eReportCard['AcademicReport']."</td>";
	$foreign_html .= "<td>";
		$foreign_html .= $linterface->Get_Radio_Button("NormalStdRadio", "PrintTemplateType", 'normal', 1, "ArchiveReportTypeClass", $eReportCard['NormalStd']);
		$foreign_html .= toolBarSpacer();
		$foreign_html .= $linterface->Get_Radio_Button("ForeignStdRadio", "PrintTemplateType", 'foreign', 0, "ArchiveReportTypeClass", $eReportCard['ForeignStd']);
	$foreign_html .= "</td>";													
	
	$foreign_html .= "</tr>";
}

$htmlAry['custReportViewTr'] = '';
if (is_array($eRCTemplateSetting['Report']['ReportGeneration']['ReportViewAry'])&&$ReportCardCustomSchoolName != "buddhist_fat_ho") {//2014-0912-1716-13164 
	$reportViewAry = $eRCTemplateSetting['Report']['ReportGeneration']['ReportViewAry'];
	$numOfReportView = count($reportViewAry);
	
	$htmlAry['custReportViewTr'] .= "<tr>";
		$htmlAry['custReportViewTr'] .= "<td class=\"field_title\">".$eReportCard['ReportView']."</td>";
		$htmlAry['custReportViewTr'] .= "<td>";
			for ($i=0; $i<$numOfReportView; $i++) {
				$_reportView = $reportViewAry[$i];
				$_reportViewDisplay = $eReportCard['Template']['ReportView'][$_reportView];
				
				$_isChecked = false;
				if ($i==0 && $reportView=='') {
					$_isChecked = true;
				}
				else if ($_reportView == $reportView) {
					$_isChecked = true;
				}
				
				$htmlAry['custReportViewTr'] .= $linterface->Get_Radio_Button("reportViewRadio_".$_reportView, "PrintTemplateType", $_reportView, $_isChecked, $_class="", $_reportViewDisplay);
				$htmlAry['custReportViewTr'] .= toolBarSpacer();
			}
		$htmlAry['custReportViewTr'] .= "</td>";
	$htmlAry['custReportViewTr'] .= "</tr>";
}



$fcm_ui = new form_class_manage_ui();
$ly = new Year($ClassLevelID);

$FormName = $ly->YearName;
$ClassSelection = $fcm_ui->Get_Class_Selection($lreportcard->schoolYearID, $ClassLevelID, "ClassID[]", '', '', 1, 1);



### Warning table
$WarningBox = '';
if ($lreportcard->Is_Template_Changed()) {
	$warningMsg = implode('<br />', $Lang['eReportCard']['GeneralArr']['TemplateChangedWarning']);
	$warningMsg = str_replace('<!--changesLog-->', $lreportcard->Get_Template_Change_Log_Message(), $warningMsg);
	
	$WarningBox .= $linterface->GET_WARNING_TABLE($err_msg='', $DeleteItemName='', $DeleteButtonName='', $warningMsg);
	$WarningBox .= '<br />';	
}


### Page Layout  
$CurrentPage = "Management_ReportArchive";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($eReportCard['Management_ReportArchive'], "", 0);
$linterface->LAYOUT_START();
?>
<script>
function SelectAll()
{
	$("[name='ClassID[]']").contents().attr("selected",true);
}

function jsCheckForm()
{
	var jsNumChecked = 0;
	$("[name='ClassID[]']").contents().each( function () {
		if ($(this).attr('selected') == true)
			jsNumChecked++;
	});
	
	if (jsNumChecked == 0)
	{
		alert("<?=$eReportCard['jsSelectClassWarning']?>");
		return false;
	}
	
	if(confirm("<?=$eReportCard['jsConfirmToArchiveReport']?>")) {
		document.frm.submit();
	}
}

function jsGoBack()
{
	var classLevelID = $('input#ClassLevelID').val();
	var semester = $('input#Semester').val();
	
	var params = '';
	params += 'Semester='+semester;
	params += '&ClassLevelID='+classLevelID;
	
	window.location='index.php?'+params;
}

$().ready(function(){
	SelectAll();
});
</script>

<br>
<!-- Start Main Table //-->
<form id="frm" name="frm" method="POST" action="<?=($eRCTemplateSetting['Report']['ExportPDFReport']? "archive_pdf_update.php" : "archive_update.php")?>">
<table width="96%" border="0" cellspacing="0" cellpadding="5">
  <tr> 
    <td align="center" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="right"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                
                <tr>
                  <td align="right" class="tabletextremark">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                         
                        <td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($msg);?></td>
                      </tr>
                  </table>
                  </td>
                </tr>
            </table></td>
          </tr>
          
          <tr>
            <td>
            	<?=$WarningBox?>
	            <table width="100%" border="0" cellspacing="0" cellpadding="4" class="form_table_v30">
		            <tr>
						<td class="field_title"><?=$eReportCard['ReportTitle']?></td>
						<td ><?=$ReportTitle?></td>
					</tr>
					
					<?=$simpleReportTypeRow?>
					<tr>
						<td class="field_title"><?=$eReportCard['Form']?></td>
						<td><?=$FormName?></td>
					</tr>
					<tr>
						<td class="field_title"><?=$eReportCard['Class']?></td>
						<td>
							<?=$ClassSelection?>
							<?= $linterface->GET_SMALL_BTN($button_select_all, "button", "SelectAll();", "selectAllBtn01")?>
						</td>
					</tr>
					<?=$foreign_html?>
					<?=$htmlAry['custReportViewTr']?>
				</table>
			</td>
		  </tr>
		  
		  <tr><td>&nbsp;</td></tr>
		  
          <tr>
            <td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
          </tr>
          <tr>
          	<td align="center">
          		<br><br>
          		<?=$linterface->GET_ACTION_BTN($button_archive, "button", "jsCheckForm();", "PrintBtn")?>
          		&nbsp;&nbsp;
          		<?=$linterface->GET_ACTION_BTN($button_back, "button", "jsGoBack();", "BackBtn")?>
          	</td>
          </tr>
        </table>
          </td>
      </tr>
    </table>
    </td>
  </tr>
</table>
<input type="hidden" id="ClassLevelID" name="ClassLevelID" value="<?=$ClassLevelID?>">
<input type="hidden" id="Semester" name="Semester" value="<?=$Semester?>">
<input type="hidden" id="ReportID" name="ReportID" value="<?=$ReportID?>">
</form>
<br>
<br>
<!-- End Main Table //-->
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>