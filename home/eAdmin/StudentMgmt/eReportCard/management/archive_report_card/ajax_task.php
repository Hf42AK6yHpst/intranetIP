<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_da.php");


intranet_auth();
intranet_opendb();

$lreportcard = new libreportcardcustom();
$lreportcard_da = new libreportcard_da();

if (!$lreportcard_da->Check_User_Can_Apply_Digital_Archive($_SESSION['UserID'])){
	exit;
}

$task = trim(stripslashes($_POST['r_task']));
$targetFileType = trim(stripslashes($_POST['r_targetFileType']));
$successAry = array();

if ($task == 'createStudentReportTempFile') {
	$reportID = $_POST['ReportID'];
	$classIdAry = $_POST['ClassID'];
	$fileNamePrefix = trim(stripslashes($_POST['r_fileNamePrefix']));
	$fileNameFormat = $fileNamePrefix.trim(stripslashes($_POST['r_fileNameFormat']));
	
	$successAry['generateBatchFile'] = $lreportcard_da->Generate_Batch_File($reportID, $classIdAry, $fileNameFormat, 'temp');
	
	$checkboxAry = array();
	if ($successAry['generateBatchFile']) {
		$checkboxAry = $lreportcard_da->Generate_Batch_File_Checkboxes_Array($classIdAry, $fileNameFormat);
	}
	
	echo implode("\n", $checkboxAry);
}
else if ($task == 'createStudentReportArchiveFile') {
	$reportID = $_POST['ReportID'];
	$classIdAry = $_POST['ClassID'];
	$fileNamePrefix = trim(stripslashes($_POST['r_fileNamePrefix']));
	$fileNameFormat = $fileNamePrefix.trim(stripslashes($_POST['r_fileNameFormat']));
	
	$successAry['generateBatchFile'] = $lreportcard_da->Generate_Batch_File($reportID, $classIdAry, $fileNameFormat, 'archive');
}
?>