<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_da.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_moduleupload.php");

intranet_auth();
intranet_opendb();
 
$lreportcard = new libreportcardcustom();
$lreportcard_ui = new libreportcard_ui();
$lreportcard_da = new libreportcard_da();
$linterface = new interface_html();
$ldamu = new libdigitalarchive_moduleupload();
$fcm_ui = new form_class_manage_ui();

$lreportcard->hasAccessRight();


$ReportID = $_GET['ReportID'];
$reportInfoAry = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$classLevelId = $reportInfoAry['ClassLevelID'];
$yearTermId = $reportInfoAry['Semester'];
$reportType = ($yearTermId=='F')? 'W' : 'T';


$academicYearName = $lreportcard->GET_ACTIVE_YEAR_NAME();
if ($reportType == 'T') {
	$termName = $lreportcard->returnSemesters($yearTermId);
	$fileNamePrefix = $academicYearName.'_'.$termName.'_';
}
else {
	$fileNamePrefix = $academicYearName.'_';
}

$CurrentPage = "Management_ReportArchive";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($eReportCard['Management_ReportArchive'], "", 0);
$linterface->LAYOUT_START();


$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($eReportCard['Management_ReportArchive'], "javascript:goBack();");
$PAGE_NAVIGATION[] = array($Lang['eReportCard']['GeneralArr']['ToDigitalArchive'], "");
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);


$htmlAry['reportCardInfoNavigation'] = $linterface->GET_NAVIGATION2_IP25($eReportCard['ReportCard']);

$extraRowAry = array();
$extraRowAry[0]['title'] = $Lang['General']['Class'];
$classSelection = $fcm_ui->Get_Class_Selection($lreportcard->schoolYearID, $classLevelId, "ClassID[]", '', '', 1, 1);
$classSelectAllBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('ClassID[]', 1);");
$extraRowAry[0]['content'] = $linterface->Get_MultipleSelection_And_SelectAll_Div($classSelection, $classSelectAllBtn);

$htmlAry['reportCardInfoTable'] = $lreportcard_ui->Get_Settings_ReportCardTemplate_ReportCardInfo_Table($ReportID, '', $extraRowAry);


$htmlAry['archiveOptionNavigation'] = $linterface->GET_NAVIGATION2_IP25($Lang['eReportCard']['GeneralArr']['ArchiveOption']);

$fileNameDefaultFormat = '{classname}_{classnumber}_{studentnameenglish}_{studentnamechinese}';
$fileNameDefaultFormatDisplay = '';
$fileNameDefaultFormatDisplay .= '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/discipline/ClassName.gif">';
$fileNameDefaultFormatDisplay .= '_';
$fileNameDefaultFormatDisplay .= '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/discipline/ClassNumber.gif">';
$fileNameDefaultFormatDisplay .= '_';
$fileNameDefaultFormatDisplay .= '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/discipline/StudentNameEng.gif">';
$fileNameDefaultFormatDisplay .= '_';
$fileNameDefaultFormatDisplay .= '<img src="'.$image_path.'/'.$LAYOUT_SKIN.'/discipline/StudentNameChi.gif">';
$ldamu->Set_Is_Physical_File_Mode(false);
$ldamu->Set_Is_Thickbox_Mode(false);
$ldamu->Set_Batch_File_Name_Default_Format($fileNameDefaultFormat);
$ldamu->Set_Batch_File_Name_Default_Format_Display($fileNameDefaultFormatDisplay);
$ldamu->Set_Batch_File_Name_Default_Prefix($fileNamePrefix);
$ldamu->Add_Default_Tag($lreportcard->ModuleTitle);
$ldamu->Add_Default_Tag('Student Report Card');
$ldamu->Set_Cancel_Button($linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "goBack();"));
$htmlAry['archiveOptionFormTable'] = $ldamu->Get_Module_Files_Group_Folder_List_TB_Form($lreportcard->ModuleTitle, $lreportcard_da->Get_To_Digital_Archive_User_Temp_File_Path());

?>
<script language="javascript">
$(document).ready( function() {
	js_Select_All('ClassID[]', 1);
});

function goBack() {
	window.location = 'index.php';
}

function generateTempFile() {
	$.ajax({
		url:      "ajax_task.php",
		type:     "POST",
		data:     $("#form1").serialize() + '&r_task=createStudentReportTempFile&r_fileNameFormat=' + js_DA_Get_Batch_Filename_Format() + '&r_fileNamePrefix=' + js_DA_Get_Batch_Filename_Prefix(),
		async:	  false,
		success:  function(returnValue) {
					$('div#batchFileCheckboxDiv').html(returnValue);
				  }
	});
	
	return true;
}

function generateBatchFile() {
	$.ajax({
		url:      "ajax_task.php",
		type:     "POST",
		data:     $("#form1").serialize() + '&r_task=createStudentReportArchiveFile&r_fileNameFormat=' + js_DA_Get_Batch_Filename_Format() + '&r_fileNamePrefix=' + js_DA_Get_Batch_Filename_Prefix(),
		async:	  false,
		success:  function(returnValue) {
					
				  }
	});
	
	return true;
}
</script>
<form id="form1" name="form1" method="post">
	<?=$htmlAry['navigation']?>
	<br />
	<?=$htmlAry['reportCardInfoNavigation']?>
	<?=$htmlAry['reportCardInfoTable']?>
	<br />
	<input type="hidden" id="ReportID" name="ReportID" value="<?=$ReportID?>" />
</form>

<?=$htmlAry['archiveOptionNavigation']?>
<br style="clear:both;" />
<?=$htmlAry['archiveOptionFormTable']?>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>