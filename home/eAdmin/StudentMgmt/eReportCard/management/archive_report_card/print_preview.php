<?php
# using:

@SET_TIME_LIMIT(600);

/**************************************************
 * 	Modification log
 *  20200818 Bill   [2020-0818-1009-50206]
 *      Handle IP30 upgrade - report layout changed issue
 *      - IP25 : '2009a'
 *      - IP30 : '2020a'
 *  20191120 Bill   [2019-0924-1029-57289]
 *      added Parent/Student print mode ($eRCTemplateSetting['Report']['ParentStudentReportPrinting'])
 * 	20171201 Bill
 * 		added checking for parent & student to print student report (escola pui ching)
 * ***********************************************/

##############################################################################################################################
### If you have modified this page, please check if you have to modify /management/archive_report_card/archive_update.php also
##############################################################################################################################

$PageRight = "TEACHER";

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	# load specific layout for teacher view of munsang college (primary) report 
	if ($ReportCardCustomSchoolName == "munsang_pri" && $viewType == "teacher")
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName."_teacher.php");	
	}
	else
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	}
	
	$lreportcard = new libreportcardcustom();
}
else
{
	$lreportcard = new libreportcard();
}

$forArchiveToDa = $_REQUEST['forArchiveToDa'];

if(trim($AcademicYear)!='')
{
	$lreportcard->schoolYear = $AcademicYear;
	$lreportcard->DBName = $lreportcard->GET_DATABASE_NAME($lreportcard->schoolYear);
	$lreportcard->ArchiveCSSPath = $lreportcard->Get_Archive_Report_Css_Path();
}

$linterface = new interface_html();

// Print Report by Parent 
if($eRCTemplateSetting['Management']['MarksheetVerification']['PrintStudentReport'] && $ck_ReportCard_UserType == "PARENT" && $_POST["GenerateStudentReport"]==1)
{
	if($ReportID)
	{
		// Get Report Last Archived Date
		$ReportBasicInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
//		$ReportGenerateDate = $ReportBasicInfo['LastGenerated'];
//		$isReportGenerated = !is_date_empty($ReportGenerateDate);
		$ReportArchiveDate = $ReportBasicInfo['LastArchived'];
		$isReportArchived = !is_date_empty($ReportArchiveDate);
	
		$thisTargetStudentID = $TargetStudentID[0];
		if($thisTargetStudentID)
		{
			// Check if target student is child of this user
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$luser = new libuser($_SESSION['UserID']);
			$ChildrenList = $luser->getChildrenList();
			$ChildrenList = Get_Array_By_Key((array)$ChildrenList, 'StudentID');
			$isCurrentUserChild = $thisTargetStudentID != "" && in_array($thisTargetStudentID, $ChildrenList);
		
			// Check if target student is in related Class Level
			$StudentInfoArr = $lreportcard->Get_Student_Class_ClassLevel_Info($thisTargetStudentID);
			$ClassLevelID = $StudentInfoArr[0]['ClassLevelID'];
			$isStudentInReport = !empty($ClassLevelID);
		}
	}
	
	// Redirect if not valid
	if(!$isReportArchived || !$isCurrentUserChild || !$isStudentInReport)
	{
		// always return false except has access right
		if(!$lreportcard->hasAccessRight())
		{
			header ("Location: /");
			intranet_closedb();
			exit();
		}
	}
}
else if($eRCTemplateSetting['Management']['MarksheetVerification']['PrintStudentReport'] && $ck_ReportCard_UserType == "STUDENT" && $_POST["GenerateStudentReport"]==1)
{
	if($ReportID)
	{
		// Get Report Last Archived Date
		$ReportBasicInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
//		$ReportGenerateDate = $ReportBasicInfo['LastGenerated'];
//		$isReportGenerated = !is_date_empty($ReportGenerateDate);
		$ReportArchiveDate = $ReportBasicInfo['LastArchived'];
		$isReportArchived = !is_date_empty($ReportArchiveDate);
	
		$thisTargetStudentID = $TargetStudentID[0];
		if($thisTargetStudentID)
		{
			// Check if target student is in related Class Level
			$StudentInfoArr = $lreportcard->Get_Student_Class_ClassLevel_Info($thisTargetStudentID);
			$ClassLevelID = $StudentInfoArr[0]['ClassLevelID'];
			$isStudentInReport = !empty($ClassLevelID);
		}
	}
	
	// Redirect if not valid
	if(!$isReportArchived || !$isStudentInReport)
	{
		// always return false except has access right
		if(!$lreportcard->hasAccessRight())
		{
			header ("Location: /");
			intranet_closedb();
			exit();
		}
	}
}
else if ($eRCTemplateSetting['Report']['ParentStudentReportPrinting'] && ($ck_ReportCard_UserType == "PARENT" || $ck_ReportCard_UserType == "STUDENT") &&
            $_POST["ClassLevelID"] && $_POST["ClassID"] && $_POST["TargetStudentID"] && $_POST["ReportID"])
{
    if($_POST["ReportID"])
    {
        //$period_filter = $ck_ReportCard_UserType == "STUDENT" ? ' NOW() BETWEEN MarksheetVerificationStart AND MarksheetVerificationEnd ' : '';
        $period_filter = ' NOW() BETWEEN MarksheetVerificationStart AND MarksheetVerificationEnd ';
        $ReportBasicInfo = $lreportcard->returnReportTemplateBasicInfo($_POST["ReportID"], $period_filter);

        // Get Report Last Archived Date
        $ReportArchiveDate = $ReportBasicInfo['LastArchived'];
        $isReportArchived = !is_date_empty($ReportArchiveDate);

        $thisTargetStudentID = $_POST["TargetStudentID"][0];
        if($thisTargetStudentID)
        {
            if($ck_ReportCard_UserType == "PARENT")
            {
                // Check if target student is child of this user
                include_once($PATH_WRT_ROOT."includes/libuser.php");
                $luser = new libuser($_SESSION['UserID']);
                $ChildrenList = $luser->getChildrenList();
                $ChildrenList = Get_Array_By_Key((array)$ChildrenList, 'StudentID');
                $isCurrentUserChild = $thisTargetStudentID != "" && in_array($thisTargetStudentID, $ChildrenList);
            }

            // Check if target student is in related Class Level
            $StudentInfoArr = $lreportcard->Get_Student_Class_ClassLevel_Info($thisTargetStudentID);
            $ClassLevelID = $StudentInfoArr[0]['ClassLevelID'];
            $isStudentInReport = !empty($ClassLevelID);
        }
    }

    // Redirect if not valid
    if(!$isReportArchived || ($ck_ReportCard_UserType == "PARENT" && !$isCurrentUserChild) || !$isStudentInReport)
    {
        // always return false except has access right
        if(!$lreportcard->hasAccessRight())
        {
            header ("Location: /");
            intranet_closedb();
            exit();
        }
    }
}
else if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

# Get the eRC Template Settings
include_once($PATH_WRT_ROOT."includes/eRCConfig.php");
$eRCtemplate = $lreportcard->getCusTemplate();
$eRCtemplate = empty($eRCtemplate) ? $lreportcard : $eRCtemplate;
$eRtemplateCSS = $lreportcard->getTemplateCSS();
// [2020-0818-1009-50206] Handle IP30 upgrade - report layout changed issue
if($ReportCardCustomSchoolName != 'sis')
{
    // [2020-0818-1009-50206]
    if($eRCTemplateSetting['IsApplyIP30Style']) {
        // do nothing
    } else {
        $LAYOUT_SKIN = '2009a';
    }
}

$ReportSetting = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$ClassLevelID = $ReportSetting['ClassLevelID'];
$SemID = $ReportSetting['Semester'];
$ReportType = $SemID == "F" ? "W" : "T";

$ArchiveFolderPath = $lreportcard->ArchiveCSSPath;
$TemplateCSS_Filename = $eRCTemplateSetting['CSS'];
$TemplateCSS_Path = $ArchiveFolderPath.$TemplateCSS_Filename;

$PrintCSS_Path = $ArchiveFolderPath."print.css";
include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/print_header_erc.php");
?>

<link href="<?=$PrintCSS_Path?>" rel="stylesheet" type="text/css" />
<link href="<?=$TemplateCSS_Path?>" rel="stylesheet" type="text/css" />

<? if (!$forArchiveToDa) { ?>
<table border="0" cellpadding="4" width="95%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>
<? } ?>
<!--br style="clear:both;" /-->

<?
if(trim($AcademicYear)!='')
{
	$StudentIDAry = (array)$StudentID;
}
else
{
	$TargetStudentList = (is_array($TargetStudentID)) ? implode(",", $TargetStudentID) : "";
	$PromotionStatus = ($ReportType=="W" && $PromotionStatus > 0) ? $PromotionStatus : "";
	$StudentIDAry = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $ClassID, $TargetStudentList, 0, 0, $PromotionStatus);
	$StudentIDAry = Get_Array_By_Key($StudentIDAry, "UserID");
}

if ($ReportCardCustomSchoolName=='sha_tin_methodist') {
	$formStudentAry = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID);
	$numOfFormStudent = count((array)$formStudentAry);
	$lastStudentId = $formStudentAry[$numOfFormStudent-1]['UserID'];
}

$numOfStudent = count((array)$StudentIDAry);
for($s=0;$s<$numOfStudent;$s++)
{
	$StudentID = $StudentIDAry[$s];
	$thisReportArr = $lreportcard->Get_Archived_Report($ReportID, $StudentID);
	
	$_pageBreak = '';
	if ($ReportCardCustomSchoolName=='sha_tin_methodist') {
		if ($StudentID == $lastStudentId) {
			$_pageBreak = '';
		}
		else {
			$_pageBreak = ' style="page-break-after:always;" ';
		}
	}
	else if(($s<$numOfStudent-1 && $ReportCardCustomSchoolName != "carmel_alison")||$ReportCardCustomSchoolName == "buddhist_fat_ho") {//2014-0912-1716-13164 
		$_pageBreak = ' style="page-break-after:always;" ';
	}
	?>
	
	<div id="container" <?=$_pageBreak?>>
		<? if (($ReportCardCustomSchoolName != "st_stephen_college" && $ReportCardCustomSchoolName != "carmel_alison") || ($ReportCardCustomSchoolName == "st_stephen_college" && $ReportSetting['isMainReport'] != 1) ) { ?>
			<table width="100%" border="0" cellpadding="02" cellspacing="0" valign="top">
		<? } ?> 
				<?= $thisReportArr[0] ?>
		<? if (($ReportCardCustomSchoolName != "st_stephen_college" && $ReportCardCustomSchoolName != "carmel_alison") || ($ReportCardCustomSchoolName == "st_stephen_college" && $ReportSetting['isMainReport'] != 1) ) { ?>
			</table>
		<? } ?>
	</div>
	<?
}
?>

<? if (!$forArchiveToDa) { ?>
<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>
<? } ?>

<?
include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/print_footer_erc.php");
?>