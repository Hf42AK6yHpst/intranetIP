<?php
// Using:
/*
 * 20190624 Bill [2019-0618-1504-34066]
 *      - add auth checking
 *      - add token to curl request
 */

$PATH_WRT_ROOT = "../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

intranet_auth();

if ($plugin['ReportCard2008'])
{
    intranet_opendb();
    
    include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
    if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
        include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
    }
    include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
    $lreportcard = new libreportcard2008j();
    
    if($lreportcard->hasAccessRight())
    {
        //$ms_url = curPageURL($withQueryString=0, $withPageSuffix=0).'/home/eAdmin/StudentMgmt/eReportCard/'.$printTargetFilePath.'?'.$printExtraParam;
        $targetUrl = $_SERVER['HTTP_REFERER'].'&isPrintMode=1';
        $extraUrlToken = $targetUrl.$_SESSION['UserID'];
        $extraUrlToken = str_replace($_SERVER['HTTP_ORIGIN'], '', $extraUrlToken);
        $output = getHtmlByUrl($targetUrl, $extraUrlToken);
        
        preg_match('#\<\!--PrintStart--\>(.+?)\<\!--PrintEnd--\>#s', $output, $matches);
        //print_r( $matches[0] );
    }
    
    intranet_closedb();
}

if ($eRCTemplateSetting['Marksheet']['PrintPage_withPrintTime']) {
	$x = '';
	$x .= '<div style="padding:10px; font-size:10pt;">'."\r\n";
		$x .= $Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['PrintTime'].': '.date('Y-m-d H:i:s');
	$x .= '</div>'."\r\n";
	$x .= '<br />'."\r\n";
	$htmlAry['printTime'] = $x;
}
if ($eRCTemplateSetting['Marksheet']['PrintPage_withSignature']) {
	$x = '';
	$x .= '<div style="padding:10px; font-size:10pt;">'."\r\n";
		$x .= $Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['Signature'].': __________________________________';
	$x .= '</div>'."\r\n";
	$x .= '<br />'."\r\n";
	$htmlAry['signature'] = $x;
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- meta to tell IE8 to run backward compatible mode -->
<META http-equiv="Content-Type"  content="text/html" Charset="UTF-8"  /> 
<META Http-Equiv="Cache-Control" Content="no-cache">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title><?=$Lang['Header']['HomeTitle']?></title>

<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/content_25.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/content_30.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/ereportcard.css" rel="stylesheet" type="text/css">
<script language="javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.3.2.min.js"></script>
<script language="javascript" src="<?=$PATH_WRT_ROOT?>templates/script.js"></script>
<script language="javascript">
$(document).ready( function() {
	$('input.textboxnum, input.textboxtext, textarea').each( function() {
		var jsTargetValue = $(this).val().Trim().replace(/\n\n/g, "<br />");
		jsTargetValue = (jsTargetValue=='')? '<?=$Lang['General']['EmptySymbol']?>' : jsTargetValue;
		
		$(this).replaceWith(jsTargetValue);
	});
	
	$('select').each( function() {
		var jsTargetValue = $(this).find('option:selected').html();
		jsTargetValue = (jsTargetValue==null || jsTargetValue=='')? '<?=$Lang['General']['EmptySymbol']?>' : jsTargetValue.Trim();
		
		$(this).replaceWith(jsTargetValue);
	});
	
	$('.hideInPrint').hide();
});
</script>
</head>
<body>
<div class="content_top_tool">
	<?=$matches[0]?>
	<?=$htmlAry['printTime']?>
	<?=$htmlAry['signature']?>
</div>
</body>
</html>