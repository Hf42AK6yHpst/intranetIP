<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();

// Required fields
$ClassLevelID = $_POST['ClassLevelID'];
$ReportID = $_POST['ReportID'];

if(empty($ClassLevelID) || empty($ReportID)) {
	header("Location: index.php");
}

// Optional fields
$SubjectID = $_POST['SubjectID'];
$ClassID = $_POST['ClassID'];
$ReportColumnID	= $_POST['ReportColumnID'];

$summary = "general";

$parms = "ReportID=".$ReportID."&ClassLevelID=".$ClassLevelID;

if (!empty($SubjectID)) $parms .= "&SubjectID=".$SubjectID;
if (!empty($ClassID)) $parms .= "&ClassID=".$ClassID;
if (!empty($ReportColumnID)) $parms .= "&ReportColumnID=".$ReportColumnID;

$summary_path = "summary_".$summary.".php?".$parms;

header("Location: $summary_path");
?>