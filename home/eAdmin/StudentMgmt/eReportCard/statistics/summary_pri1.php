<?php
######################################################
# Generate Statistic Report for Primary Class Level 1-4
# 	Trigger from: index.php
# 	Required variables: $ClassLevelID, $ReportID
# 	Optional variables: $ReportColumnID
#
# 	Last updated: Andy Chan on 2008/6/3
######################################################

function returnGrandMSBand($Marks='-1') {
	if($Marks == -1)	return array("1","2","3","4");
	if($Marks >= 85)	return "1";
	if($Marks >= 70)	return "2";
	if($Marks >= 50)	return "3";
	return "4";
}

function calculatePercent($Numerator, $Denumerator) {
	return number_format(round(($Numerator/$Denumerator*100) ,1) ,1);
}

// Root path
$PATH_WRT_ROOT = "../../../../../";

// Page access right
$PageRight = "ADMIN";

// Include general libraries
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

// Authorize intranet user & connect Database
intranet_auth();
intranet_opendb();

// Check if ReportCard module is enable
if ($plugin['ReportCard']) {
	// Include ReportCard libraries
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
	
	// Create objects
	$lreportcard = new libreportcard2008w();
	$linterface = new interface_html();
	$lclass = new libclass();
	
	// Check module access right
	if ($lreportcard->hasAccessRight()) {
		// Check required data submitted from index.php
		if (!isset($ClassLevelID, $ReportID)) {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		// Get ReportCard info
		$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		
		// Check to make sure it is a whole year & generated report
		if ($reportTemplateInfo["LastGenerated"] == "0000-00-00 00:00:00" || $reportTemplateInfo["LastGenerated"] == "") {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		// Get subjects of the current ClassLevel
		$FormSubjectArr = $lreportcard->returnSubjectwOrder($ClassLevelID, 1);
		
		// Get subjects' grading scheme of the current ClassLevel
		$FormSubjectGradingSchemeArr = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID,0,$ReportID);
		
		// Get ClassLevel name
		$ClassLevelName = $lreportcard->returnClassLevel($ClassLevelID);
		
		// Get Classes
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		
		// Get current school year
		$SchoolYear = $lreportcard->schoolYear;
		
		// Get columns info of the current ReportCard
		$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
		if (is_array($ColumnTitleAry))
		{
			$ColumnIDList = array_keys($ColumnTitleAry);
		}
		else
		{
			$ColumnIDList = array();
		}
		
		// Check if ReportColumnID have value
		// 	if yes, only term mark is used
		// 	if no, overall marks of all terms are used
		if (isset($ReportColumnID) && $ReportColumnID != "") {
			// Hardcoded Column Name since names in $ColumnTitleAry are not exactly "SA1" & "SA2"
			if ($ReportColumnID == $ColumnIDList[0])	$DisplayColumnName = "SA1";
			if ($ReportColumnID == $ColumnIDList[1])	$DisplayColumnName = "SA2";
			
			$StatReportTitle = "Summary of ".$DisplayColumnName." Results by Subject (Primary)";
		} else {
			$StatReportTitle = "Summary of Overall Results by Subject (Primary)";
		}
		
		$display = "";
		
		// Main table generaeting logic
		// Loop 1: Subjects
		foreach ($FormSubjectArr as $SubjectID => $SubjectName) {
			if ($FormSubjectGradingSchemeArr[$SubjectID]["scaleInput"] != "M")
				continue;
			
			$display .= "<tr><td class='SubjectName'>".str_replace(" ()", "", $SubjectName[0])."</td><td colspan='13'></td></tr>";
			
			// Init variables
			$AbsNumber = array();
			$ExNumber = array();
			$BandNumber = array();
			$BandPercent = array();
			$NumberOfClassStudent = array();
			$ClassStudentTotalmark = array();
			$ClassStudentAverageMark =  array();
			
			$ClassLevelAbsNumber = 0;
			$ClassLevelExNumber = 0;
			$NumberOfClassLevelStudent = 0;
			$ClassLevelBandNumber = array();
			$ClassLevelBandPercent = array();
			$ClassLevelStudentTotalMark = 0;
			$ClassLevelStudentAverageMark = 0;
			
			// Loop 2: Classes
			for($i=0; $i<sizeof($ClassArr); $i++) {
				// Construct StudentIDList in the class
				$ClassStudentArr = $lreportcard->GET_STUDENT_BY_CLASS($ClassArr[$i]["ClassID"]);
				$ClassStudentIDList = array();
				for($j=0; $j<sizeof($ClassStudentArr); $j++) {
					$ClassStudentIDList[] = $ClassStudentArr[$j]["UserID"];
				}
				$ClassStudentIDListQuery = implode("','", $ClassStudentIDList);
				$ClassStudentIDListQuery = "'".$ClassStudentIDListQuery."'";
				
				// Query the consolidated marks of the Class
				$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE";
				$sql = "SELECT Mark, Grade FROM $table ";
				$sql .= "WHERE ReportID = '$ReportID' AND SubjectID = '$SubjectID' AND StudentID IN ($ClassStudentIDListQuery) AND ";
				if (isset($ReportColumnID) && $ReportColumnID != "") {
					$sql .= "ReportColumnID = '$ReportColumnID'";
				} else {
					$sql .= "(ReportColumnID = 0 OR ReportColumnID IS NULL)";
				}
				$ClassResult = $lreportcard->returnArray($sql, 2);
				
				$NumberOfClassStudent[$ClassArr[$i]["ClassID"]] = sizeof($ClassResult);
				
				// Init variables
				$AbsNumber[$ClassArr[$i]["ClassID"]] = 0;
				$ExNumber[$ClassArr[$i]["ClassID"]] = 0;
				$BandNumber["1"][$ClassArr[$i]["ClassID"]] = 0;
				$BandNumber["2"][$ClassArr[$i]["ClassID"]] = 0;
				$BandNumber["3"][$ClassArr[$i]["ClassID"]] = 0;
				$BandNumber["4"][$ClassArr[$i]["ClassID"]] = 0;
				$BandNumber["1-3"][$ClassArr[$i]["ClassID"]] = 0;
				$BandPercent["1"][$ClassArr[$i]["ClassID"]] = 0;
				$BandPercent["2"][$ClassArr[$i]["ClassID"]] = 0;
				$BandPercent["3"][$ClassArr[$i]["ClassID"]] = 0;
				$BandPercent["4"][$ClassArr[$i]["ClassID"]] = 0;
				$BandPercent["1-3"][$ClassArr[$i]["ClassID"]] = 0;
				$ClassStudentTotalmark[$ClassArr[$i]["ClassID"]] = 0;
				$ClassStudentAverageMark[$ClassArr[$i]["ClassID"]] = 0;
				
				// Loop the marks and group into different categories
				for($j=0; $j<sizeof($ClassResult); $j++) {
					$StudentBand = "";
					if ($ClassResult[$j]["Mark"] != "" && $ClassResult[$j]["Grade"] == "") {
						$ClassStudentTotalmark[$ClassArr[$i]["ClassID"]] += $ClassResult[$j]["Mark"];
						$StudentBand = returnGrandMSBand($ClassResult[$j]["Mark"]);
						if (!is_array($StudentBand))
							$BandNumber[$StudentBand][$ClassArr[$i]["ClassID"]]++;
					} else if ($ClassResult[$j]["Grade"] == "-") {
						$AbsNumber[$ClassArr[$i]["ClassID"]]++;
					} else if ($ClassResult[$j]["Grade"] == "/") {
						$ExNumber[$ClassArr[$i]["ClassID"]]++;
					}
				}
				
				$BandNumber["1-3"][$ClassArr[$i]["ClassID"]] = $BandNumber["1"][$ClassArr[$i]["ClassID"]]+$BandNumber["2"][$ClassArr[$i]["ClassID"]]+$BandNumber["3"][$ClassArr[$i]["ClassID"]];
				
				// Prevent divide by Zero
				if ($NumberOfClassStudent[$ClassArr[$i]["ClassID"]] != 0) {
					$BandPercent["1"][$ClassArr[$i]["ClassID"]] = calculatePercent($BandNumber["1"][$ClassArr[$i]["ClassID"]], $NumberOfClassStudent[$ClassArr[$i]["ClassID"]]);
					$BandPercent["2"][$ClassArr[$i]["ClassID"]] = calculatePercent($BandNumber["2"][$ClassArr[$i]["ClassID"]], $NumberOfClassStudent[$ClassArr[$i]["ClassID"]]);
					$BandPercent["3"][$ClassArr[$i]["ClassID"]] = calculatePercent($BandNumber["3"][$ClassArr[$i]["ClassID"]], $NumberOfClassStudent[$ClassArr[$i]["ClassID"]]);
					$BandPercent["4"][$ClassArr[$i]["ClassID"]] = calculatePercent($BandNumber["4"][$ClassArr[$i]["ClassID"]], $NumberOfClassStudent[$ClassArr[$i]["ClassID"]]);
					
					$BandPercent["1-3"][$ClassArr[$i]["ClassID"]] = calculatePercent($BandNumber["1-3"][$ClassArr[$i]["ClassID"]], $NumberOfClassStudent[$ClassArr[$i]["ClassID"]]);
					
					$ClassStudentAverageMark[$ClassArr[$i]["ClassID"]] = round(($ClassStudentTotalmark[$ClassArr[$i]["ClassID"]]/($NumberOfClassStudent[$ClassArr[$i]["ClassID"]]-$ExNumber[$ClassArr[$i]["ClassID"]])), 1);
				}
				
				$CTeacher = array();
				$ClassTeacherNameArr = $lclass->returnClassTeacher($ClassArr[$i]["ClassName"]);
				foreach($ClassTeacherNameArr as $key=>$val) {
					$CTeacher[] = $val['CTeacher'];
				}
				$ClassTeacherName = !empty($CTeacher) ? implode(", ", $CTeacher) : "-";
				
				$display .= "<tr>
												<td>".$ClassArr[$i]["ClassName"]."</td>
												<td>".$ClassTeacherName."</td>
												<td align='center'>".($AbsNumber[$ClassArr[$i]["ClassID"]]+$ExNumber[$ClassArr[$i]["ClassID"]])."</td>
												<td align='center'>".$NumberOfClassStudent[$ClassArr[$i]["ClassID"]]."</td>
												<td align='center'>".$BandNumber["1"][$ClassArr[$i]["ClassID"]]."</td>
												<td align='center'>".$BandPercent["1"][$ClassArr[$i]["ClassID"]]."</td>
												<td align='center'>".$BandNumber["2"][$ClassArr[$i]["ClassID"]]."</td>
												<td align='center'>".$BandPercent["2"][$ClassArr[$i]["ClassID"]]."</td>
												<td align='center'>".$BandNumber["3"][$ClassArr[$i]["ClassID"]]."</td>
												<td align='center'>".$BandPercent["3"][$ClassArr[$i]["ClassID"]]."</td>
												<td align='center'>".$BandNumber["4"][$ClassArr[$i]["ClassID"]]."</td>
												<td align='center'>".$BandPercent["4"][$ClassArr[$i]["ClassID"]]."</td>
												<td align='center'>".$BandNumber["1-3"][$ClassArr[$i]["ClassID"]]."</td>
												<td align='center'>".$BandPercent["1-3"][$ClassArr[$i]["ClassID"]]."</td>
												<td align='center'>".$ClassStudentAverageMark[$ClassArr[$i]["ClassID"]]."</td>
										</tr>";
			} // End Loop 2: Classes
			// Display a row sum up stat of the whole class level
			$ClassLevelAbsNumber = array_sum($AbsNumber);
			$ClassLevelExNumber = array_sum($ExNumber);
			$NumberOfClassLevelStudent = array_sum($NumberOfClassStudent);
			$ClassLevelBandNumber["1"] = array_sum($BandNumber["1"]);
			$ClassLevelBandNumber["2"] = array_sum($BandNumber["2"]);
			$ClassLevelBandNumber["3"] = array_sum($BandNumber["3"]);
			$ClassLevelBandNumber["4"] = array_sum($BandNumber["4"]);
			$ClassLevelBandNumber["1-3"] = $ClassLevelBandNumber["1"]+$ClassLevelBandNumber["2"]+$ClassLevelBandNumber["3"];
			
			$ClassLevelStudentTotalMark = array_sum($ClassStudentTotalmark);
			
			// Prevent divide by Zero
			if ($NumberOfClassLevelStudent != 0) {
				$ClassLevelBandPercent["1"] = calculatePercent($ClassLevelBandNumber["1"], $NumberOfClassLevelStudent);
				$ClassLevelBandPercent["2"] = calculatePercent($ClassLevelBandNumber["2"], $NumberOfClassLevelStudent);
				$ClassLevelBandPercent["3"] = calculatePercent($ClassLevelBandNumber["3"], $NumberOfClassLevelStudent);
				$ClassLevelBandPercent["4"] = calculatePercent($ClassLevelBandNumber["4"], $NumberOfClassLevelStudent);
				
				$ClassLevelBandPercent["1-3"] = calculatePercent($ClassLevelBandNumber["1-3"], $NumberOfClassLevelStudent);
				
				$ClassLevelStudentAverageMark = round(($ClassLevelStudentTotalMark/$NumberOfClassLevelStudent) ,1);
			}
			
			$display .= "<tr>
											<td>&nbsp;</td>
											<td>Level Average</td>
											<td align='center'>&nbsp;</td>
											<td align='center'>".$NumberOfClassLevelStudent."</td>
											<td align='center'>".$ClassLevelBandNumber["1"]."</td>
											<td align='center'>".$ClassLevelBandPercent["1"]."</td>
											<td align='center'>".$ClassLevelBandNumber["2"]."</td>
											<td align='center'>".$ClassLevelBandPercent["2"]."</td>
											<td align='center'>".$ClassLevelBandNumber["3"]."</td>
											<td align='center'>".$ClassLevelBandPercent["3"]."</td>
											<td align='center'>".$ClassLevelBandNumber["4"]."</td>
											<td align='center'>".$ClassLevelBandPercent["4"]."</td>
											<td align='center'>".$ClassLevelBandNumber["1-3"]."</td>
											<td align='center'>".$ClassLevelBandPercent["1-3"]."</td>
											<td align='center'>".$ClassLevelStudentAverageMark."</td>
									</tr>
									<tr><td colspan='14'>&nbsp;</td></tr>";
			
			
		} // End Loop 1: Subjects
		
		#debug_r($FormSubjectGradingSchemeArr);
		############## Interface Start ##############
		
?>
<style type="text/css">
<!--
h1 {
	font-size: 24px;
	font-family: "Times CY", "Times New Roman", serif;
	font-style: italic;
	margin: 0px;
	padding-top: 3px;
	padding-bottom: 3px;
	border-bottom: 5px solid #DDD;
}

h2 {
	font-size: 16px;
	font-family: "Times CY", "Times New Roman", serif;
	margin: 0px;
	padding-top: 2px;
	padding-bottom: 2px;
}

#StatReportWrapper {
	min-height: 800px;
	height: auto;
	_height: 800px;
	border-top: 5px solid #DDD;
	border-bottom: 5px solid #DDD;
}

#StatTable thead {
	font-size: 15px;
	font-family: "Times CY", "Times New Roman", serif;
	font-style: italic;
}

#StatTable tbody {
	font-size: 12px;
	font-family: arial, "lucida console", sans-serif;
}

.SubjectName {
	font-size: 12px;
	font-weight: bold;
	font-family: arial, "lucida console", sans-serif;
	padding-bottom: 5px;
}
-->
</style>
<script type="text/JavaScript" language="JavaScript">

</script>
<div id="StatReportWrapper" style="width:100%;">
	<h1><?=$StatReportTitle?></h1>
	<h2>
		School Year&nbsp;&nbsp;<?= $SchoolYear ?>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		Level:&nbsp;<?= $ClassLevelName ?>
	</h2>
	<table id="StatTable" width="100%" cellpadding="0" cellspacing="0" border="0">
		<thead>
			<tr>
				<th>&nbsp;</th>
				<th>Teacher</th>
				<th>Abs/Ex</th>
				<th>Total</th>
				<th colspan="2">Band 1</th>
				<th colspan="2">Band 2</th>
				<th colspan="2">Band 3</th>
				<th colspan="2">Band 4</th>
				<th colspan="2">Band 1-3</th>
				<th>Average</th>
			</tr>
			<tr>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>Number</th>
				<th>Number</th>
				<th>Number</th>
				<th>&#37;</th>
				<th>Number</th>
				<th>&#37;</th>
				<th>Number</th>
				<th>&#37;</th>
				<th>Number</th>
				<th>&#37;</th>
				<th>Number</th>
				<th>&#37;</th>
				<th>Marks</th>
			</tr>
		</thead>
		<tbody>
			<?= $display ?>
		</tbody>
	</table>
</div>
<?
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>
