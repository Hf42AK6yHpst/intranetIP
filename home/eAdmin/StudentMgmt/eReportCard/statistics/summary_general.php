<?php
// Using: 

######################################################
# Generate Statistic Report
# 	Trigger from: index.php
# 	Required variables: $ClassLevelID, $ReportID
# 	Optional variables: $ReportColumnID
#
# 	Last updated: Andy Chan on 2008/7/10
######################################################

##########################
#
#	Date:	2016-07-21 (Bill)
#			replace split() by explode(), for PHP 5.4
#
#	Date:	2016-07-08 (Anna)
#			embeded Highcharts graph
#
##########################

// Root path
$PATH_WRT_ROOT = "../../../../../";

// Page access right
$PageRight = "ADMIN";

// Include general libraries
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/json.php");

// Authorize intranet user & connect Database
intranet_auth();
intranet_opendb();
$json = new JSON_obj();

function returnNumericMarkStat($markArr, $fullMark) {
	$range = $fullMark/10;
	$returnArr = array();
	for($i=$range; $i<=$fullMark; $i=$i+$range) {
		$key = ($i-$range)."-".$i;
		$returnArr[$key] = 0;
	}
	$returnArr["Others"] = 0;
	
	foreach ($markArr as $studentID => $mark) {
		for($i=$range; $i<=$fullMark; $i=$i+$range) {
			$key = ($i-$range)."-".$i;
			if (is_numeric($mark) && $mark <= $i) {
				$returnArr[$key]++;
				break;
			}
			if (!is_numeric($mark)) {
				$returnArr["Others"]++;
			}
		}
	}
	return $returnArr;
}

function returnGradeMarkStat($markArr, $gradeList, $specialCaseList) {
	$returnArr = array();
	for($i=0; $i<sizeof($gradeList); $i++) {
		$key = $gradeList[$i]["Grade"];
		$returnArr[$key] = 0;
	}
	$returnArr["Others"] = 0;
	
	foreach ($markArr as $studentID => $mark) {
		for($i=0; $i<=sizeof($gradeList); $i++) {
			$key = $gradeList[$i]["Grade"];
			if ($mark == $key) {
				$returnArr[$key]++;
				break;
			}
			if (in_array($mark, $specialCaseList)) {
				$returnArr["Others"]++;
			}
		}
	}
	return $returnArr;
}

function calculatePercent($Numerator, $Denumerator) {
	return number_format(round(($Numerator/$Denumerator*100) ,1) ,1);
}

// Check if ReportCard module is enable
if ($plugin['ReportCard2008']) {
	// Include ReportCard libraries
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
	
	// Create objects
	$lreportcard = new libreportcard2008w();
	$linterface = new interface_html();
	$lclass = new libclass();
	
	// Check module access right
	if ($lreportcard->hasAccessRight()) {
		// Check required data submitted from index.php
		// optional: 
		if (!isset($ClassLevelID, $ReportID) || empty($ClassLevelID) || empty($ReportID)) {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		// Get ReportCard info
		$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		
		// Check to make sure it is a generated report
		if ($reportTemplateInfo["LastGenerated"] == "0000-00-00 00:00:00" || $reportTemplateInfo["LastGenerated"] == "") {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		// Get subjects of the current ClassLevel
		$FormSubjectArr = $lreportcard->returnSubjectwOrder($ClassLevelID, 1);
		
		// Get subjects' grading scheme of the current ClassLevel
		$FormSubjectGradingSchemeArr = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID,0,$ReportID);
		
		// Get ClassLevel name
		$ClassLevelName = $lreportcard->returnClassLevel($ClassLevelID);
		
		// Get Classes
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		
		// Get current school year
		$SchoolYear = $lreportcard->schoolYear;
		
		// Get columns info of the current ReportCard
		$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
		if (is_array($ColumnTitleAry))
		{
			$ColumnIDList = array_keys($ColumnTitleAry);
		}
		else
		{
			$ColumnIDList = array();
		}
		
		// Get the list of students (in a class or entire class level)
		if (!isset($ClassID) || empty($ClassID)) {
			$studentList = $lreportcard->GET_STUDENT_BY_CLASSLEVEL("", $ClassLevelID);
		} else {
			$studentList = $lreportcard->GET_STUDENT_BY_CLASS($ClassID);
			for($i=0; $i<sizeof($ClassArr); $i++) {
				if ($ClassArr[$i]["ClassID"] == $ClassID) {
					$ClassName = $ClassArr[$i]["ClassName"];
					break;
				}
			}
			$ClassLevelName .= " $ClassName";
		}
		
		$studentIDList = array();
		for($i=0; $i<sizeof($studentList); $i++) {
			$studentIDList[] = $studentList[$i]["UserID"];
		}
		$studentIDListSql = implode(",", $studentIDList);
		
		$cond = "";
		
		// Get individual subject mark or grand average (which table to retrieve)
		$SubjectName = "";
		if (!isset($SubjectID) || empty($SubjectID)) {
			$table = $lreportcard->DBName.".RC_REPORT_RESULT";
			$fieldToSelect = "StudentID, GrandAverage AS MarkUsed";
			
			$SubjectName = $eReportCard['Template']['AverageMark'];
		} else {
			$subjectGradingScheme = $FormSubjectGradingSchemeArr[$SubjectID];
			$subjectGradingSchemeInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($subjectGradingScheme["schemeID"]);
			
			if ($subjectGradingScheme["scaleDisplay"] == "M") {
				$fieldToSelect = "StudentID, Mark As MarkUsed";
			} else {
				$fieldToSelect = "StudentID, Grade As MarkUsed";
			}
			$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE";
			$cond .= "AND SubjectID = '$SubjectID' ";
			
			$SubjectName = $FormSubjectArr[$SubjectID][0];
			$gradingSchemeRanges = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($subjectGradingScheme["schemeID"]);
		}
		
		// Get overall mark or term/assessment mark
		$ColumnTitle = "";
		if (!isset($ReportColumnID) || empty($ReportColumnID)) {
			$ColumnTitle = $eReportCard['Template']['OverallCombined'];
			$cond .= "AND (ReportColumnID = '' OR ReportColumnID = '0') ";
		} else {
			$ColumnTitle = $ColumnTitleAry[$ReportColumnID];
			$cond .= "AND ReportColumnID = '$ReportColumnID'";
		}
		
		$sql = "SELECT $fieldToSelect FROM $table WHERE ";
		$sql .= "ReportID = '$ReportID' ";
		$sql .= "AND StudentID IN ($studentIDListSql) ";
		$sql .= $cond;
		
		$result = $lreportcard->returnArray($sql);
		
		$studentMarkArr = array();
		if (sizeof($result) > 0) {
			for($i=0; $i<sizeof($result); $i++) {
				$studentMarkArr[$result[$i]["StudentID"]] = $result[$i]["MarkUsed"];
			}
			
			if (isset($SubjectID) && !empty($SubjectID)) {
				if ($subjectGradingScheme["scaleDisplay"] == "M") {
					$markStatArr = returnNumericMarkStat($studentMarkArr, $subjectGradingSchemeInfo["FullMark"]);
				} else {
					$specialCaseList = $lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
					$markStatArr = returnGradeMarkStat($studentMarkArr, $gradingSchemeRanges, $specialCaseList);
				}
			} else {
				$markStatArr = returnNumericMarkStat($studentMarkArr, 100);
			}
			
			$ConfArr['IsHorizontal'] = 1;
			$ConfArr['ChartY'] = $eReportCard['headcount'];
			$ConfArr['MaxWidth'] = 500;
			$ConfArr['ChartX'] = $eReportCard['Mark'];
			$DataArr = array();
			foreach ($markStatArr as $range => $noOfStudents) {
				$DataArr[] = array($range, $noOfStudents);
			}
			$MarkChart = $linterface->GEN_CHART($ConfArr, $DataArr);
		} else {
			$MarkChart = "";
		}
		
		$DataArrLength = sizeof($DataArr);
		for ($i=0;$i<$DataArrLength;$i++){
			$ScoreArr[$i]=$DataArr[$i][0];	
			$StudentNumberArr[$i]=$DataArr[$i][1];
		}
		
//		$Score = array();
// 		foreach ($ScoreArr as $_value) {
// 			$Score[] = $_value;	
// 		}
// 		$ScoreArr = $Score;
		$ScoreArr = $json->encode($ScoreArr);
		$StudentNumberArr = $json->encode($StudentNumberArr);
		
		//$schoolData = split("\n",get_file_content($PATH_WRT_ROOT."file/school_data.txt"));
		$schoolData = explode("\n",get_file_content($PATH_WRT_ROOT."file/school_data.txt"));
		$schoolName = $schoolData[0];
		
		//$academicYear = get_file_content($PATH_WRT_ROOT."/file/academic_yr.txt");
		//if ($academicYear == "") $academicYear = date("Y");
		$academicYear = $lreportcard->GET_ACTIVE_YEAR("-");
		
		$reportTitlePiece = explode(":_:", $reportTemplateInfo["ReportTitle"]);
		$reportTitle = $reportTitlePiece[0]." ".$reportTitlePiece[1];
		
		$StatHeader = "$schoolName<br />$reportTitle<br />$academicYear $ClassLevelName $ColumnTitle<br />$SubjectName";
		
		############## Interface Start ##############
		$CurrentPage = "Statistics";
		$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
		# tag information
		$TAGS_OBJ[] = array($eReportCard['Statistics']);
		$linterface->LAYOUT_START();
?>
<script type="text/javascript">
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: '<?=$StatHeader?>'
        },
        
        xAxis: {
            categories:  <?=$ScoreArr?>,
         	//categories: ["0-10","10-20","20-30","30-40","40-50","50-60","60-70","70-80","80-90","90-100","Others"],
            
            title: {
                text: '<?=$Lang['Gamma']['Score']?>'
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: '<?=$eReportCard['NumOfStudent']?>',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
       
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
       series: [ {
            name: '<?=$eReportCard['NumOfStudent'] ?>' ,
            data:   <?=$StudentNumberArr?>
        }]
       
    });
});
</script>
<script src="/templates/highchart/highcharts.js"></script>

<div id="container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>

<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="dotline">
			<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
		</td>
	</tr>
	<tr>
		<td align="center">
			<?= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='index.php'", "submitBtn") ?>
		</td>
	</tr>
</table>
		
<!-- table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td class='tablesubtitle' align='center'><?=$StatHeader?></td>
				</tr>
				<tr>
					<td><?=$linterface->GET_NAVIGATION2($eReportCard['Mark']." ".$eReportCard['Statistics'])?></td>
				</tr>
				<tr>
					<td class='tabletext' align='center'><?=$MarkChart?></td>
				</tr>
				<tr>
					<td class="dotline">
						<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
					</td>
				</tr>
				<tr>
					<td align="center">
						<?= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='index.php'", "submitBtn") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table-->
<br />
<?
  	$linterface->LAYOUT_STOP();
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>
