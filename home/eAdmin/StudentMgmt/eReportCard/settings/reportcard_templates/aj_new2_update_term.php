<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcard2008w();

$basic_data = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$ClassLevelID = $basic_data['ClassLevelID'];
$SubjectIDAry = $lreportcard->returnSubjectIDwOrder($ClassLevelID);
$old_column_data = $lreportcard->returnReportTemplateColumnData($ReportID); // Column Data

$ReportType = $basic_data['Semester'];

foreach($old_column_data as $key => $val)
{
	$DefWeightAry[$val['ReportColumnID']] = $DefaultWeight[$key];
	
	foreach($SubjectIDAry as $SubjKey => $SubjID)
	{
		$WeightAry[$val['ReportColumnID']][$SubjID] = ${"Weight{$key}"}[$SubjKey];
	}
}

if($UpdateColumnTitle) $TakeAction = "updateColumnTitle";


// Added on 20080827: convert grading title from UTF-8 to BIG5/GB2312 because the Ajax JS call made it UTF-8
/*
if ($intranet_session_language == "gb") {
	$column_title = iconv("UTF-8//IGNORE","GB2312",$column_title);
} else {
	$column_title = iconv("UTF-8//IGNORE","BIG5",$column_title);
}
$column_title = addslashes($column_title);
*/

switch($TakeAction)
{
	case "add":
		$data2[0]['ReportID'] = $ReportID;
		$data2[0]['DateInput'] = "NOW()";
		
		if($ReportType=="F")
		{
			$data2[0]['DisplayOrder'] = $AddSemester;
			$data2[0]['SemesterNum'] = $AddSemester;
		}
		else
		{	
			# Save database with unicode
			$data2[0]['ColumnTitle'] = $column_title;
			
			$data2[0]['DisplayOrder'] = -1;			// update the DisplayOrder later
			$data2[0]['ShowPosition'] = $show_position;
			if($PositionRange1!="" and $PositionRange2!="" and $show_position==2)
			$data2[0]['PositionRange'] = $PositionRange1.",".$PositionRange2;
			$data2[0]['BlockMarksheet'] = $BlockMarksheet;
		}
		
		$result = $lreportcard->InsertReportTemplateColumn($data2);
		if($result and $ReportType!="F")	//update the DisplayOrder
		{
			$lreportcard->UpdateReportTemplateColumnOrder($ReportID, -1, $DisplayOrder);
		}
		break;
	case "del":		// $AddSemester = "Column ID" in this case
		$lreportcard->DELETE_REPORT_TEMPLATE_SUBJECT_WEIGHT('ReportColumnID', $AddSemester);
		$result = $lreportcard->DELETE_REPORT_TEMPLATE_COLUMN('ReportColumnID', $AddSemester);	
		break;
	case "changeOrder":
 		$lreportcard->ChangeReportTemplateColumnOrder($ReportID, $ColID, $Order);
		break;	
	case "updateColumnTitle":
		$data2['DateModified'] = "NOW()";
		# Convert to Big5 from unicode
		//$data2['ColumnTitle'] = (convert2unicode($column_title, 1, 2));
		# Save database with unicode
		$data2['ColumnTitle'] = $column_title;
		
		$data2['DisplayOrder'] = -1;			// update the DisplayOrder later
		$data2['ShowPosition'] = $show_position;
		if($PositionRange1!="" and $PositionRange2!="" and $show_position==2)
		$data2['PositionRange'] = $PositionRange1.",".$PositionRange2;
		$data2['BlockMarksheet'] =  $BlockMarksheet;
		
		$result = $lreportcard->UpdateReportTemplateColumn($data2, "ReportColumnID=$ColID");
		if($result)								//update the DisplayOrder
		{
			$lreportcard->UpdateReportTemplateColumnOrder($ReportID, -1, $DisplayOrder);
		}
		break;
}
$column_data = $lreportcard->returnReportTemplateColumnData($ReportID); // Column Data


$ReportType = $basic_data['Semester'];
$ClassLevelID = $basic_data['ClassLevelID'];

$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
$ColumnNo = sizeof($ColumnTitleAry);

$SelectedSemAry = $lreportcard->returnReportInvolvedSem($ReportID);
foreach($SelectedSemAry as $semid => $semname)
	$SelectedSem[] = $semid;
	
$ColumnTitle_Display = "";
$ColumnIDAry = array();
/* START Build Subject Weight Array (for loading data) */
for($i=0;$i<sizeof($weight_data);$i++)
{
	$WeightAry[$weight_data[$i]['ReportColumnID']][$weight_data[$i]['SubjectID']] = $weight_data[$i]['Weight'];
}
/* END Build Subject Weight Array (for loading data) */

/* 1 - horizontal, 2 - vertical */
$cal = $lreportcard->returnReportTemplateCalculation($ReportID);
switch($cal)
{
	case "1":
		$ratio_tr = "
			<tr>
				<td colspan=\"2\">&nbsp;</td>
	            <td colspan=\"{$ColumnNo}\" align=\"center\" class=\"tablerow2 term_break_link tabletext retabletop_total\"><span class=\"tabletexttopremark\">". $eReportCard['RatioBetweenAssesments'] ." 
	              <label for=\"usePercentage\"><input name=\"usePercentage\" type=\"checkbox\" id=\"usePercentage\" value=\"1\" onClick=\"clickusePercentage()\" ". ($usePercentage ? "checked":"")."> ". $eReportCard['Use'] ." %</label></span></td>
            </tr>
		";
		break;
	case "2":
		$ratio_tr = "";
		break;
}

if($ColumnNo)
{
	$left_arrow = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_prev_off.gif\" alt=\"". $eReportCard['MoveLeft'] ."\" title=\"". $eReportCard['MoveLeft'] ."\" width=\"11\" height=\"10\" border=\"0\">";
	$right_arrow = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_next_off.gif\" alt=\"". $eReportCard['MoveRight'] ."\" title=\"". $eReportCard['MoveRight'] ."\" width=\"11\" height=\"10\" border=\"0\">";
	$empty = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"11\" height=\"10\" border=\"0\">";
	
	$c = 0;
	foreach($ColumnTitleAry as $ColID => $ColumnTitle)
	{
		$ColumnIDAry[] = $ColID;
		$icon_table = "
			<table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
		      <tr>
		      	<td align=\"left\">". (($c and $ReportType<>"F")? "<a href='javascript:changeOrder(". $ColID .", -1);'>".$left_arrow."</a>" : $empty)."</td>
		        <td align=\"center\">";
		        
		        if(sizeof($ColumnTitleAry)>1)
			        $icon_table .= "<a href=\"javascript:void(0)\" onClick=\"jAJAX_addTerm( ". $ColID .", 'del' )\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" align=\"absmiddle\" border=\"0\"></a>";
		        if($ReportType<>"F")
			        $icon_table .= "&nbsp;<a href=\"javascript:void(0)\" onClick=\"editAssesment(".$ColID.", ". $c .");\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" align=\"absmiddle\" border=\"0\" id=\"editIcon".$c."\"></a>";
				$icon_table .= "</td>
		        <td align=\"right\">". (($c<$ColumnNo-1 and $ReportType<>"F")? "<a href='javascript:changeOrder(". $ColID .", 1);'>".$right_arrow."</a>"  : $empty) ."</td>
		      </tr>
		    </table>
		";
	
		//check css
		if($cal==1)
		{
			$ColTitleCss = "retabletop";
		}
		else
		{
			$ColTitleCss = "retabletop_" . ($c%2?"b":"a");
		}
		
		# Added on 20080827, to solve AJAX chinese encoding problem
		# convert2unicode() have problem get the job done
		/*
		if ($intranet_session_language == "gb") {
			$ColumnTitle = iconv("GB2312","UTF-8//IGNORE",$ColumnTitle);
		} else {
			$ColumnTitle = iconv("BIG5","UTF-8//IGNORE",$ColumnTitle);
		}
		$ColumnTitle = addslashes($ColumnTitle);
		*/
		
		//$ColumnTitle =  $ReportType=="F" ? $ColumnTitle : convert2unicode($ColumnTitle, 1, 2);
		
		$ColumnTitle_Display .= "<td align=\"center\" valign=\"top\" class=\"$ColTitleCss tabletopnolink\">". $icon_table. $ColumnTitle ."<br></td>";
		
		$AssigntoAll_Display .= "<td align=\"center\" valign=\"top\" class=\"retablerow1_total tabletext\">";
		$AssigntoAll_Display .= "<input name=\"DefaultWeight[]\" type=\"text\" class=\"ratebox\" value=\"". $DefWeightAry[$ColID] ."\"><span id=\"percentage[]\">". ($usePercentage?"%":" ") ."</span><input type=\"hidden\" name=\"ReportColumnID[]\" value=\"$ColID\"><br>";
		$AssigntoAll_Display .= "</td>";
		$c++;
	}
	
	if($eRCTemplateSetting['VerticalSubjectWeightForOverallForVerticalHorizontalCalculation'] && $cal==2) {
		$ColumnTitle_Display .= "<td align=\"center\" valign=\"bottom\" class=\"$ColTitleCss tabletopnolink\" style=\"background-color:#444444;\">Overall<br></td>";
		
		$thisReportColumnID = 0;	// overall column only
		$thisSubjectID = 0;
		$thisId = 'VerticalWeightTb_'.$thisReportColumnID.'_'.$thisSubjectID;
		$thisName = 'VerticalWeightArr['.$thisReportColumnID.']['.$thisSubjectID.']';
		$thisColumnDefaultWeight = $VerticalWeightArr[$thisReportColumnID][$thisSubjectID];
		
		$AssigntoAll_Display .= "<td align=\"center\" class=\"retablerow1_total tabletext\" style=\"background-color:#CCCCCC;\">\r\n";
			$AssigntoAll_Display .= "<input id=\"".$thisId."\" name=\"".$thisName."\" class=\"ratebox DefaultWeightTb\" reportColumnId=\"".$thisReportColumnID."\" type=\"text\" value=\"".$thisColumnDefaultWeight."\">\r\n";
		$AssigntoAll_Display .= "</td>\r\n";
	}
}
else			// No column as init.
{
	$ColumnTitle_Display = "<td align=\"center\" valign=\"top\" class=\"retabletop tabletopnolink\">". $eReportCard['NoAssesment'] ."<br></td>";
}

switch($ReportType)
{
	case "F":		// Whole Year Report Type
		$ReportTypeTitle = $eReportCard['WholeYearReport'];
		$addBtn = "<a href=\"javascript:void(0);\" class=\"contenttool\" onClick=\"clickAddbtn('addTermLayer')\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> ". $eReportCard['AddTerm'] ."</a>";
		break;
	default:		// Term Type
		$ReportTypeTitle = $eReportCard['TermReport'] ." - ". $lreportcard->returnSemesters($SelectedSem[0]);
		$addBtn = "<a href=\"javascript:void(0);\" class=\"contenttool\" onClick=\"clickAddbtn('addTermLayer')\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> ". $eReportCard['AddAssesment'] ."</a>";
		break;
}

$SubjectArray = $lreportcard->returnSubjectwOrder($ClassLevelID);

// Get the last sub-subject of each subjects if any & use for css
$LastCmpSubject = array();
if(!empty($SubjectArray))
{
	foreach($SubjectArray as $SubjectID => $Data)
	{
		if(sizeof($Data)>1)
		{
			foreach($Data as $a => $b)
			{
				$LastCmpSubject[$SubjectID] = $a;
			}
		}
	}
}



$Subject_Display = "";
if(!empty($SubjectArray))
{
	$mainSubRow = 0;
	foreach($SubjectArray as $SubjectID => $Data)
	{
		if(is_array($Data))
		{
			foreach($Data as $CmpSubjectID => $SubjectName)
			{	
				if($CmpSubjectID > 0)	// sub subject
				{
	 				$css1 = (count($LastCmpSubject) > 0 && in_array($CmpSubjectID, $LastCmpSubject)===true) ? 'tablelist_subject':'tablelist_subject_head';		
					$css2 = 'tablelist_sub_subject';		
	 				$Subject = "
						<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
						<tr>
							<td width=\"13\">
								<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
								<tr>
									<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"13\" height=\"13\"></td>
								</tr>
								<tr>
									<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"13\" height=\"13\"></td>
								</tr>
								</table>
							</td>
							<td class=\"tabletext\">{$SubjectName}</td>
						</tr>
						</table>
					";
				}
				else					// main subject
				{
					$css1 = 'tablelist_subject';		
					$css2 = 'tablelist_subject';
					$Subject = $SubjectName;
				}
				$SubjectID = ($CmpSubjectID==0) ? $SubjectID : $CmpSubjectID;
				
				$Subject_Display .= "<tr>";
				$Subject_Display .= "
					<td width=\"15\" align=\"left\" class=\"{$css1}\">
						<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
						<tr>
							<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"13\" height=\"13\"></td>
						</tr>
						<tr>
							<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"13\" height=\"13\"></td>
						</tr>
						</table>
					</td>
				";
				$Subject_Display .= "<td width=\"25%\" class=\"{$css2}\" >{$Subject} </td>";
				//check css
				if($cal==1) 
				{
					$rowCss = "retablerow".($mainSubRow%2+1);	
				}
				//Fields Column
				for($tc=0;$tc<$ColumnNo;$tc++)
				{
					if($cal==2) 
					{
						$rowCss = "retablerow".($mainSubRow%2+1)."_".($tc%2?"b":"a");	
					}
					$Subject_Display .="<td align=\"center\" class=\"$rowCss tabletext\">";
					$Subject_Display .= ($CmpSubjectID) ? "<img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"33\" height=\"13\">" : "";
					$Subject_Display .= "<input name=\"Weight{$tc}[]\" type=\"text\" class=\"ratebox\"value=\"". $WeightAry[$ColumnIDAry[$tc]][$SubjectID] ."\"><span id=\"percentage[]\">". ($usePercentage?"%":" ") ."</span></td>";
				}
				
				if($eRCTemplateSetting['VerticalSubjectWeightForOverallForVerticalHorizontalCalculation'] && $cal==2) {
					$thisReportColumnID = 0;	// for overall column only
					$thisTbId = 'VerticalWeightTb_'.$thisReportColumnID.'_'.$SubjectID;
					$thisTbClass = 'VerticalWeightTb_'.$thisReportColumnID;
					$thisTbName = 'VerticalWeightArr['.$thisReportColumnID.']['.$SubjectID.']';
					$thisBgColor = ($mainSubRow%2+1 == 1)? '#EEEEEE' : '#CCCCCC';
					$thisWeight = $VerticalWeightArr[$thisReportColumnID][$SubjectID];
					
					$Subject_Display .= "<td align=\"center\" class=\"$rowCss tabletext\" style=\"background-color:".$thisBgColor.";\">";
						$Subject_Display .= ($CmpSubjectID) ? "<img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"33\" height=\"13\">" : "";
						$Subject_Display .= "<input type=\"text\" id=\"$thisTbId\" name=\"$thisTbName\" class=\"ratebox ".$thisTbClass."\" value=\"". $thisWeight ."\">";
					$Subject_Display .= "</td>";
				}
				
				$Subject_Display .= "</tr>"."\n";
			}
		}
		$mainSubRow++;
	}
}


$x = "
<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\">
<tr>
	<td width=\"15\" align=\"left\">&nbsp;</td>
	<td width=\"25%\" align=\"right\" valign=\"top\">&nbsp;</td>
	<td align=\"center\" valign=\"top\" class=\"tablename\" colspan=\"". $ColumnNo ."\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
			<tr>
                    <td class=\"tabletext\"><strong>". $ReportTypeTitle ."</strong></td>
                    <td align=\"right\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"2\" id=\"addBtnID\">
                        <tr>
                          <td>&nbsp;</td>
                          <td>". $addBtn ."</td>
                        </tr>
                    </table></td>
                  </tr>
	</table></td>
</tr>
<tr>
	<td width=\"15\" align=\"left\">&nbsp;</td>
	<td width=\"25%\" align=\"right\" valign=\"bottom\" class=\"tabletextremark\">&nbsp;</td>
";	
$x .= $ColumnTitle_Display;
$x .= "</tr>";

$x .= $ratio_tr;
$x .= "
	<tr>
		<td width=\"15\" align=\"left\">&nbsp;</td>
		<td width=\"25%\" align=\"right\" valign=\"bottom\" class=\"tabletextremark\">
			<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
				<tr>
					<td><a href=\"javascript:void(0);\" onClick=\"clickAssignAll();\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_copy.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">". $eReportCard['AssignToAll'] ." </a></td>
				</tr>
			</table>
		</td>".
		$AssigntoAll_Display
	."</tr>
";
$x .= $Subject_Display;
$x .= "</table>";

intranet_closedb();

echo $x;
?>





