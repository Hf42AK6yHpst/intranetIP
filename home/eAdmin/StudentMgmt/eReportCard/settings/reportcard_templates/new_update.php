<?php
# using: 

################# Change Log [Start] ############
#
#	Date:	2017-09-15	Bill	[2017-0914-0952-23066]
#			redirect to biba_handle_es_report.php for BIBA ES / KG Report (when new / edit)
#
#	Date:	2015-10-19	Bill
#			support BIBA - 4 School Terms
#
#	Date:	2015-10-15	Bill	[2015-1013-1434-16164]
#			support BIBA KG Report
#
#	Date:	2015-08-14	Bill	[2015-0413-1359-48164]
#			redirect to new4.php or biba_handle_es_report.php for BIBA ES Report
#
#	Date:	2015-07-21	Bill	[2015-0413-1359-48164]
#			create report template according to selected BIBA preset report type
#
################# Change Log [End] ##############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");

intranet_auth();
intranet_opendb();

// not handle BIBA report
if($skip && !$eRCTemplateSetting['Settings']['AutoSelectReportType'])
{
	header("Location: new2.php?ReportID=$ReportID");
	exit;
}

$lreportcard = new libreportcard2008w();

# Get Data
$data = array();
$ReportID				= $_POST['ReportID'];
$ReportTitle			= $_POST['report_title1'] . ":_:" . $_POST['report_title2'];
$data['ReportTitle']	= $ReportTitle;
$data['ClassLevelID'] 	= $_POST['ClassLevelID'];
$term_report			= $_POST['term_report'];
$data['Semester']		= $term_report=="WholeYearReport" ? "F" : $_POST['Semester'];
$data['Description']	= $_POST['Description'];
$data['DateModified'] 	= date("Y-m-d H:i:s");
$data['isMainReport']	= ($_POST['is_main_term_report'] == 'Main')? 1 : 0;
$data['TermStartDate']	= $_POST['TermStartDate'];
$data['TermEndDate']	= $_POST['TermEndDate'];

// [2015-0413-1359-48164] set $data according to $_POST data
if($eRCTemplateSetting['Settings']['AutoSelectReportType']){
	// [2015-1013-1434-16164] check Report Type
	$form_num = $lreportcard->GET_FORM_NUMBER($_POST['ClassLevelID']);
	// ES Report
	if (is_numeric($form_num)) {
		$form_num = intval($form_num);
		$isESReport = $form_num > 0 && $form_num < 6;
	}
	// KG Report
	else {
		$isKGReport = true;
	}
	
	// skip handling for BIBA ES report
	// [2015-1013-1434-16164] add BIBA KG report
	if($skip){
		if($isESReport || $isKGReport){
			header("Location: new4.php?ReportID=$ReportID");
		}
		else {
			header("Location: new2.php?ReportID=$ReportID");
		}
		exit;
	}
	
	// $_POST data
	$targetSem = $_POST['TargetTerm'];
	$targetReport = $_POST['TargetReport'];
	
	if($targetSem!="" && $targetReport!=""){
		// semester list
		$semList = getSemesters($lreportcard->GET_ACTIVE_YEAR_ID(),"",1);
		
		$term_report = "TermReport";
		// All KG Reports are Main Report
		$data['isMainReport'] = (strpos($targetSem,"s")!==false || $isKGReport);
		
		$term_value = intval(preg_replace('/[^0-9]+/', '', $targetSem), 10);
		
		// Main Report
		if($data['isMainReport']){
			//$data['Semester'] = ($term_value == 1)? $semList[0]["YearTermID"] : ($targetReport == "2"? "F" : $semList[1]["YearTermID"]);
			$data['Semester'] = ($term_value==4 && $targetReport=="2")? "F" : $semList[($term_value-1)]["YearTermID"];
			
			// Consolidate report
			// set semester array
			if($data['Semester']=="F"){
				$Sem = array();
				// Term 2 and 4 as Report Column
				$Sem[] = $semList[1]["YearTermID"];
				$Sem[] = $semList[3]["YearTermID"];
				
				$term_report = "WholeYearReport";
			}
		}
		// Extra Report
		else {
			$data['ReportSequence'] = $term_value;
			//$data['Semester'] = ($term_value < 3)? $semList[0]["YearTermID"] : $semList[1]["YearTermID"];
			$data['Semester'] = $semList[($term_value-1)]["YearTermID"];
		}
	}
}

if ($data['Semester'] == 'F')
	$data['isMainReport'] = 1;

if($ReportID)			// Update
{
	$result = $lreportcard->UpdateReportTemplateBasicInfo($ReportID, $data);
	$msg = "update_failed";
	
	if ($result)
		$lreportcard->UPDATE_REPORT_LAST_DATE($ReportID, 'DateModified');
}
else					// Insert
{
	$result = $lreportcard->InsertReportTemplateBasicInfo($data);
	
	$isNewReport = true;
	
	if ($result)
	{
		$ReportID = $result;
		$lreportcard->UPDATE_REPORT_LAST_DATE($ReportID, 'DateModified');

		# Copy a set of grading scheme from global grading scheme of the form 
		$lreportcard->COPY_CLASSLEVEL_SETTINGS($data['ClassLevelID'], $data['ClassLevelID'], 0, $ReportID);
	}
	
	
	if($ReportID && $result && $term_report=="WholeYearReport")
	{
		$data2 = array();
		if($Sem)
		{
			$counter = 0;
			foreach($Sem as $key => $SemID)
			{
				if ($eRCTemplateSetting['Settings']['TemplateSettings_ConsolidatedReportWithTermReportAssessment']) {
					// get term report report columns
					$_termReportInfoAry = $lreportcard->returnReportTemplateBasicInfo('', $others='', $data['ClassLevelID'], $SemID, $isMainReport=1);
					$_termReportId = $_termReportInfoAry['ReportID'];
					
					$_reportColumnAry = $lreportcard->returnReportTemplateColumnData($_termReportId);
					$_numOfColumn = count($_reportColumnAry);
					for ($i=0; $i<$_numOfColumn; $i++) {
						$__reportColumnId = $_reportColumnAry[$i]['ReportColumnID'];
						
						$data2[$counter]['ReportID'] = $ReportID;
						$data2[$counter]['DisplayOrder'] = $counter;
						$data2[$counter]['SemesterNum'] = $SemID;
						$data2[$counter]['TermReportColumnID'] = $__reportColumnId;
						$counter++;
					}	
				}
				else {
					$data2[$counter]['ReportID'] = $ReportID;
					$data2[$counter]['DisplayOrder'] = $counter;
					$data2[$counter]['SemesterNum'] = $SemID;
					$counter++;
				}
			}
			$result = $lreportcard->InsertReportTemplateColumn($data2);
		}
	}
	$msg = "add_failed";
}

intranet_closedb();	

if($result){
	// [2015-0413-1359-48164] redirect to target page for BIBA ES and KG Report
	// [2015-1013-1434-16164] add BIBA KG report
	if($eRCTemplateSetting['Settings']['AutoSelectReportType'] && ($isESReport || $isKGReport)) {
//		if($isNewReport){
//			header("Location: biba_handle_es_report.php?ReportID=$ReportID");
//		} else {
//			header("Location: new4.php?ReportID=$ReportID");
//		}
		// [2017-0914-0952-23066] redirect to biba_handle_es_report.php
		$parms = "";
		if($isNewReport){
			$parms = "&isNewReport=$isNewReport";
		}
		header("Location: biba_handle_es_report.php?ReportID=$ReportID".$parms);
	}
	else{
		header("Location: new2.php?ReportID=$ReportID");
	}
}
else{
	header("Location: new.php?msg=$msg");
}
?>