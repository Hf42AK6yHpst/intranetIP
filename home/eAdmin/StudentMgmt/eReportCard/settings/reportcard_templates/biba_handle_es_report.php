<?
# using: 

################# Change Log [Start] ############
#
#	Date:	2017-09-15	Bill	[2017-0914-0952-23066]
#			Support edit ES / KG Report - auto fill default weight to new subjects
#
#	Date:	2015-10-19	Bill
#			support BIBA - 4 School Terms
#
################# Change Log [End] ##############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcard2008w();

// GET data
$ReportID = $_GET["ReportID"];

// Semester
$currentYearID = $lreportcard->GET_ACTIVE_YEAR_ID();
$semesterList = getSemesters($currentYearID, 0);
//$termID = $semesterList[0]["YearTermID"];

// Report Template Data
$basic_data = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$ClassLevelID = $basic_data["ClassLevelID"];
$ReportType = $basic_data["Semester"];

// Subject Array
$SubjectIDAry = $lreportcard->returnSubjectIDwOrder($ClassLevelID, $ExcludeCmpSubject=0, $ReportID);

$data = array();
$data["DefaultWeight"] = 1;
$data["DateModified"] = date("Y-m-d H:i:s");

# New Report
if($isNewReport)
{
	// Consolidate Report - Update Default Column - from original Step 2 (new2_update.php)
	if($ReportType=="F") {
		$ReportColumnAry = $lreportcard->returnReportTemplateColumnData($ReportID);
		$ReportColumnIDAry = array($ReportColumnAry[0]["ReportColumnID"], $ReportColumnAry[1]["ReportColumnID"]);
		
		// Update Term 2 and 4 Column
		$lreportcard->UpdateReportTemplateColumn($data, "ReportColumnID=".$ReportColumnIDAry[0]);
		$lreportcard->UpdateReportTemplateColumn($data, "ReportColumnID=".$ReportColumnIDAry[1]);
	}
	// Semester Report - Add Default Column - from original Step 2 (aj_new2_update_term.php, new2_update.php)
	else {
		$data = array();
		$data[0]["ReportID"] = $ReportID;
		$data[0]["DefaultWeight"] = 1;
		$data[0]["ColumnTitle"] = "ReportColumn";
		$data[0]["DisplayOrder"] = 0;
		$data[0]["ShowPosition"] = 0;
		$data[0]["BlockMarksheet"] = "";
		$data[0]["DateInput"] = "NOW()";
		$data[0]["DateModified"] = "NOW()";
		
		// Insert Column
		$ReportColumnIDAry = $lreportcard->InsertReportTemplateColumn($data);
	}
}
# [2017-0914-0952-23066] Edit Report
else
{
	// Consolidate Report - Handle Report Column - from original Step 2 (new2_update.php)
	if($ReportType=="F") {
		$ReportColumnAry = $lreportcard->returnReportTemplateColumnData($ReportID);
		$ReportColumnIDAry = array($ReportColumnAry[0]["ReportColumnID"], $ReportColumnAry[1]["ReportColumnID"]);
		
		// Update Term 2 and 4 Column
		$lreportcard->UpdateReportTemplateColumn($data, "ReportColumnID=".$ReportColumnIDAry[0]);
		$lreportcard->UpdateReportTemplateColumn($data, "ReportColumnID=".$ReportColumnIDAry[1]);
		
		// Clear Term 2 and 4 Column Subject Weight
		$lreportcard->DELETE_REPORT_TEMPLATE_SUBJECT_WEIGHT("ReportColumnID", $ReportColumnIDAry[0], "ReportColumnID IS NOT NULL");
		$lreportcard->DELETE_REPORT_TEMPLATE_SUBJECT_WEIGHT("ReportColumnID", $ReportColumnIDAry[1], "ReportColumnID IS NOT NULL");
	}
	// Semester Report - Handle Report Column - from original Step 2 (new2_update.php)
	else {
		$ReportColumnAry = $lreportcard->returnReportTemplateColumnData($ReportID);
		$ReportColumnIDAry = Get_Array_By_Key((array)$ReportColumnAry, "ReportColumnID");
		foreach($ReportColumnIDAry as $thisReportColumnID) {
			// Update Column
			$lreportcard->UpdateReportTemplateColumn($data, "ReportColumnID=".$thisReportColumnID);
			
			// Clear Column Subject Weight
			$lreportcard->DELETE_REPORT_TEMPLATE_SUBJECT_WEIGHT("ReportColumnID", $thisReportColumnID, "ReportColumnID IS NOT NULL");
		}
	}
	
	// Clear Overall Subject Weight - from original Step 3 (new3_update.php)
	$lreportcard->DELETE_REPORT_TEMPLATE_SUBJECT_WEIGHT("ReportID", $ReportID, "ReportColumnID IS NULL AND SubjectID IS NULL");
	$lreportcard->DELETE_REPORT_TEMPLATE_SUBJECT_WEIGHT("ReportID", $ReportID, "ReportColumnID IS NULL AND SubjectID IS NOT NULL");
}

// loop columns
foreach((array)$ReportColumnIDAry as $currentKey => $ReportColumnID)
{
	// Add Default Column Subject Weight - from orginal Step 2 (new2_update.php)
	$i = 0;
	$data = array();
	foreach($SubjectIDAry as $SubjKey => $SubjID) {
		$data[$i]["ReportColumnID"] = $ReportColumnID;
		$data[$i]["SubjectID"] = $SubjID;
		$data[$i]["Weight"] = 1;
		$i++;
	}
	$lreportcard->InsertReportTemplateSubjectWeight($ReportID, $data);
	
//	// Add Default Assessment Ratio - from orginal Step 3 (vertical-horizontal)
//	$data = array();
//	$data[0]["ReportColumnID"] = $ReportColumnID;
//	$data[0]["SubjectID"] = "";
//	$data[0]["Weight"] = 1;
//	$lreportcard->InsertReportTemplateSubjectWeight($ReportID, $data);
}

// Add Default Overall Subject Weight - from orginal Step 3 (horizontal-vertical)
$i = 1;
$data = array();
$data[0]["ReportColumnID"] = "";
$data[0]["SubjectID"] = "";
$data[0]["Weight"] = 1;
foreach($SubjectIDAry as $SubjKey => $SubjID) {
	$data[$i]["ReportColumnID"] = "";
	$data[$i]["SubjectID"] = $SubjID;
	$data[$i]["Weight"] = 1;
	$i++;
}
$lreportcard->InsertReportTemplateSubjectWeight($ReportID, $data);

// Update Report Modified Date
$lreportcard->UPDATE_REPORT_LAST_DATE($ReportID, "DateModified");

// Redirect to Step 4
header("Location: new4.php?ReportID=$ReportID");

?>