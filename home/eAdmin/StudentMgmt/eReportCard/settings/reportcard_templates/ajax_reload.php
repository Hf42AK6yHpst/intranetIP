<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$Action = stripslashes($_REQUEST['Action']);

if ($Action == "Reload_Term_Start_End_Date")
{
	$YearTermID = stripslashes($_REQUEST['YearTermID']);
	$objYearTerm = new academic_year_term($YearTermID);
	
	$TermStartDate = substr($objYearTerm->TermStart, 0, 10);
	$TermEndDate = substr($objYearTerm->TermEnd, 0, 10);
	
	$ReturnString = $TermStartDate.'|||'.$TermEndDate;
}

intranet_closedb();
echo $ReturnString;

?>