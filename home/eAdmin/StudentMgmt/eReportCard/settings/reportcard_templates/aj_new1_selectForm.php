<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcard2008w();

$table = $lreportcard->DBName.".RC_REPORT_TEMPLATE";
$sql = "select Semester from $table where ClassLevelID=$ClassLevelID and Semester<>'F'";
$TermReport_SemesterAryTmp = $lreportcard->returnArray($sql);	
$TermReport_SemesterAry = array();
for($i=0;$i<sizeof($TermReport_SemesterAryTmp);$i++)
{
	$TermReport_SemesterAry[$i] = $TermReport_SemesterAryTmp[$i][0];
}

$sem_ary = $lreportcard->returnSemesters();
$unselect_sem_ary = array();
$i=0;
foreach($sem_ary as $SemID => $SemName)
{
	//if(in_array($SemID, $TermReport_SemesterAry)===true) continue;
	$unselect_sem_ary[$i][0] = $SemID;
	$unselect_sem_ary[$i][1] = $SemName;
	$i++;
}

$SemesterUnSelect = getSelectByArray($unselect_sem_ary, "name='Semester' id='Semester'",$Semester,0,1);
$SemCheckedTable = "<table border='0' cellspacing='0' cellpadding='0'>";
$SemCheckedTable .= "<tr><td class='tabletext'><input type='checkbox' name='Sem_all' id='Sem_all' disabled onClick=(this.checked)?setChecked(1,this.form,'Sem[]'):setChecked(0,this.form,'Sem[]')> <label for='Sem_all'> ". $button_select_all ."</label></td></tr>";
$si = 0;
foreach($sem_ary as $SemID => $SemName)
{
	$SemCheckedTable .= !($si%4) ? "<tr>" : "";
	$SemCheckedTable .= "<td class='tabletext'><input type='checkbox' name='Sem[]' value='{$SemID}' id='Sem_".$si."' disabled> <label for='Sem_".$si."'>".$SemName."</label></td>";
	$si++;
	$SemCheckedTable .= !($si%4) ? "</tr>" : "";
}
$SemCheckedTable .= "</table>";

$ReportTypeTable = "<table border='0' cellspacing='0' cellpadding='1'>";
if(sizeof($unselect_sem_ary)) 
{
$ReportTypeTable .= "<tr>
						<td><input type=\"radio\" name=\"term_report\" id=\"term_report\" value=\"TermReport\" onclick=\"js_Change_Report_Type('Term');\" ". ($term_report_selected=="TermReport"?"checked":"") ."></td>
						<td><label for=\"term_report\"><span class=\"tabletext\">". $eReportCard['TermReport'] ." &nbsp; &nbsp;</span></label>". $SemesterUnSelect ."</td>
					</tr>";
}
else
{
	$ReportTypeTable = "<input type='hidden' name='Semester' value=''>";
}
$ReportTypeTable .= "<tr>
						<td></td>
						<td>
							<input type=\"radio\" name=\"is_main_term_report\" id=\"is_main_term_report\" value=\"Main\" checked>
							<label for=\"is_main_term_report\">
								<span class=\"tabletext\">". $eReportCard['Main'] ." &nbsp; &nbsp;</span>
							</label>
							<input type=\"radio\" name=\"is_main_term_report\" id=\"is_extra_term_report\" value=\"Extra\">
							<label for=\"is_extra_term_report\">
								<span class=\"tabletext\">". $eReportCard['Extra'] ." &nbsp; &nbsp;</span>
							</label>
						</td>
					</tr>";
$ReportTypeTable .= "<tr><td>&nbsp;</td></tr>";
$ReportTypeTable .= "<tr>
						<td><input type=\"radio\" name=\"term_report\" id=\"wholeyear_report\" value=\"WholeYearReport\" onclick=\"js_Change_Report_Type('Consolidate');\" ". (($term_report_selected=="WholeYearReport" or !sizeof($unselect_sem_ary)) ? "checked":"") ."></td>
						<td><label for=\"wholeyear_report\"><span class=\"tabletext\">". $eReportCard['WholeYearReport'] ." </span></label></td>
					</tr>";
$ReportTypeTable .= "<tr>
						<td></td>
						<td>". $SemCheckedTable ."</td>
					</tr>
					";
$ReportTypeTable .= "</table>";
	

intranet_closedb();
echo $ReportTypeTable;

?>