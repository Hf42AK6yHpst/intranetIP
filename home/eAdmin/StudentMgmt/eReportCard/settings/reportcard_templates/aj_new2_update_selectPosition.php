<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcard2008w();

$ColumnTitle2Ary = $lreportcard->returnReportColoumnTitleWithOrder($ReportID);

// START Build Assesment Select for addBtn
$t = 0;
$OrderSelection[$t] = array(-1, "(at the beginning)");
if(sizeof($ColumnTitle2Ary))
{
	foreach($ColumnTitle2Ary as $Order => $ColName)
	{
		$t++;
		$OrderSelection[$t][0] = $Order;
		$OrderSelection[$t][1] = $ColName;
	}
}
$OrderSelect = getSelectByArray($OrderSelection, "name='DisplayOrder'",-1,0,1);
// END Build Assesment selection for addBtn

intranet_closedb();

echo $OrderSelect;

?>