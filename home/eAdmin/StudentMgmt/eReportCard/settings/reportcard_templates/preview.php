<?php
// Modifying:
/**************************************************
 * 	Modification log
 * 	20150519 Bill :
 * 		BIBA Cust
 * 		Redirect to print_preview_by_pdf.php if allow export report in PDF format
 *  20140801 Ryan  : 
 *      Merged SIS Cust
 * 	20100205 Marcus:
 * 		for carmel_alison, separate page3 from the report and than print them after printing page1 and 2 of the report
 * ***********************************************/
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

// BIBA Cust
# Redirect to print_preview_by_pdf.php if allow export report in PDF format
if($eRCTemplateSetting['Report']['ExportPDFReport']){
	header("Location:". $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/reports/generate_reports/print_preview_by_pdf.php?ReportID=$ReportID&PreviewReport=1");
	intranet_closedb();
	exit();
}

# Get the eRC Template Settings
include_once($PATH_WRT_ROOT."includes/eRCConfig.php");

# layout already in $lreportcard (sis.php)
if($ReportCardCustomSchoolName != 'sis'){
	$eRCtemplate = $lreportcard->getCusTemplate();
}else{
	# SIS : Follow PMS Style 
	$LAYOUT_SKIN = '2007a';
}	
$eRCtemplate = empty($eRCtemplate) ? $lreportcard : $eRCtemplate;
$eRtemplateCSS = $lreportcard->getTemplateCSS();
if($eRCTemplateSetting['CusTemplate'])	$ReportCardCustomSchoolName = "sis";

$PrintTemplateType = $_GET['PrintTemplateType'];
$PrintTemplateType = ($PrintTemplateType=='')? 'normal' : $PrintTemplateType;


$CurrentPage = "ReportGeneration";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$MODULE_OBJ['title'] = $button_preview;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();


if($ReportCardCustomSchoolName != "st_stephen_college" && $ReportCardCustomSchoolName != 'hkuga_college')
{
	if ($ReportCardCustomSchoolName == "escola_tong_nam")
	{
		# Different school title banners (Kindergarten/Primary/Secondary) with different classes
		# So, have to pass studentID as well to determind the use of school title banner
		$TitleTable = $eRCtemplate->getReportHeader($ReportID, $StudentID);
	}
	else
	{
		$TitleTable = $eRCtemplate->getReportHeader($ReportID);
	}
	
	$StudentInfoTable = $eRCtemplate->getReportStudentInfo($ReportID, $StudentID, $forPage3='', $PageNum=1, $PrintTemplateType);
	//$MSTable = $eRCtemplate->getMSTable($ReportID, $StudentID,$PrintTemplateType);
	$MSTable = $eRCtemplate->getMSTable($ReportID, $StudentID, $PrintTemplateType, $PageNum=1);	// $PageNum for "wong_kam_fai_secondary"
	$MiscTable = $eRCtemplate->getMiscTable($ReportID, $StudentID);
	# 20140723 Sis SignatureTable
	if($ReportCardCustomSchoolName == 'sis'){
		$SignatureTable = $eRCtemplate->getSignatureTable($eRCTemplateSetting['Signature']);
	}
	else 
		$SignatureTable = $eRCtemplate->getSignatureTable($ReportID, $StudentID);
	$FooterRow = $eRCtemplate->getFooter($ReportID);
}
	
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/print.css" rel="stylesheet" type="text/css" />
<? if($eRtemplateCSS) {?><link href="<?=$eRtemplateCSS?>" rel="stylesheet" type="text/css" /><? } ?>

<table border="0" cellpadding="0" cellspacing="0" align="center" width="90%">
<tr>
<td align="center">
	<div id="container">
		<table width="100%" border="0" cellpadding="02" cellspacing="0" valign="top" <? if($s<sizeof($StudentIDAry)-1) {?>style="page-break-after:always"<? } ?>>
		<?
		if($ReportCardCustomSchoolName == "st_stephen_college")
		{
			$ReportSetting = $eRCtemplate->returnReportTemplateBasicInfo($ReportID);
			////2012-0516-1118-51071
//			if($ReportSetting['isMainReport'] == 1)
//			{
				$x = '';
				$x .= "<tr><td>";
				$x .= $eRCtemplate->FirstPage($ReportID, $StudentID);
				$x .= $eRCtemplate->getSubjectPages($ReportID, $StudentID);
				$x .= "</td></tr>";
				
				# Subejct Pages
				//style="page-break-before:always"
//				$ReportSetting = $eRCtemplate->returnReportTemplateBasicInfo($ReportID);
//				$ClassLevelID = $ReportSetting['ClassLevelID'];
//		 		$MainSubjectArray = $eRCtemplate->returnSubjectwOrder($ClassLevelID);
//		 		foreach($MainSubjectArray as $SubjectID => $SubejctData)
//		 		{
//					$x .= $eRCtemplate->SubjectPage($ReportID, $SubjectID, $StudentID);		
//		 		}
		
				# OLE Page
				
		 		echo $x;
//			}
//			else
//			{
//				$TitleTable = $eRCtemplate->getReportHeader($ReportID, $StudentID);
//				$StudentInfoTable = $eRCtemplate->getReportStudentInfo($ReportID, $StudentID);
//				$MSTable = $eRCtemplate->getMSTable($ReportID, $StudentID);
//				$x = $eRCtemplate->getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID, $StudentID);
//				
//				echo $x;
//			}	
		}
		else if($ReportCardCustomSchoolName == "carmel_alison")
		{
			$Layout = $eRCtemplate->getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID, $StudentID);
			echo $Layout[0];
			$Page3Layout[]= $Layout[1];		
		}else if($ReportCardCustomSchoolName == "sis"){
			?>
			<td align="center">
			<div id="container">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr><td><?=$TitleTable?><br/></td><tr>
			<tr><td><?=$StudentInfoTable?></td><tr>
			<tr><td><?=$MSTable?></td><tr>
			<tr><td><?=$MiscTable?></td><tr>
			<tr><td><?=$SignatureTable?></td><tr>
			<?=$FooterRow?>
			</table>
			</div>
			<?
		}
		else
		{
			if (method_exists($eRCtemplate, "getLayout")) {
				echo $eRCtemplate->getLayout($TitleTable, $StudentInfoTable, $MSTable, $MiscTable, $SignatureTable, $FooterRow, $ReportID, $StudentID);
			} else {
			?>
				<tr><td><?=$TitleTable?></td><tr>
				<tr><td><?=$StudentInfoTable?></td><tr>
				<tr><td><?=$MSTable?></td><tr>
				<tr><td><?=$MiscTable?></td><tr>
				<tr><td><?=$SignatureTable?></td><tr>
				<?=$FooterRow?>
			<?
			}
		}
		?>
		</table>
	</div>
	<?
	if($ReportCardCustomSchoolName == "carmel_alison" && is_array($Page3Layout))
	{
		for($i=0; $i<sizeof($Page3Layout);$i++)
		{
		?>
			<div id="container">
				<table width="100%" border="0" cellpadding="02" cellspacing="0" valign="top" <? if($s<sizeof($Page3Layout)-1) {?>style="page-break-after:always"<? } ?>>
					<?=$Page3Layout[$i]?>
				</table>
			</div>
		<?
		}
	}
	?>
</td>
</tr>

<tr><td>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr>
	<td align="center"><?= $linterface->GET_ACTION_BTN($button_close, "button", "javascript:self.close()")?>
	</td>
</tr>
</table>
<br/>
</td></tr>

</table>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>