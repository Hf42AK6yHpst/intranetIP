<?php
// using :

/***********************************************
 *  modification log
 *  20191120 Bill [2017-0901-1525-31265]
 *      - added logic to update subject column weights in consolidate report (F1-F5)    ($eRCTemplateSetting['Settings']['TemplateSettings_ConsolidatedReportCopyTermReportSubjectColumnWeight'])
 * ***********************************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if($skip)
{
	header("Location: new3.php?ReportID=$ReportID");
	exit;
}

$lreportcard = new libreportcard2008w();

$basic_data = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$ClassLevelID = $basic_data['ClassLevelID'];

$SubjectIDAry = $lreportcard->returnSubjectIDwOrder($ClassLevelID, $ExcludeCmpSubject=0, $ReportID);
$cal = $lreportcard->returnReportTemplateCalculation($ReportID);

if($cal==1)
{
	$data1[PercentageOnColumnWeight]= $usePercentage;
	$lreportcard->UpdateReportTemplateBasicInfo($ReportID, $data1);
}

foreach($ReportColumnID as $key => $ColID)
{
	// update RC_REPORT_TEMPLATE_COLUMN field: DefaultWeight, DateModified
	$data = array();
	$data[DefaultWeight] = ($cal==1 and $usePercentage) ? $DefaultWeight[$key] /100 : $DefaultWeight[$key];
	$data[DateModified] = date("Y-m-d H:i:s");
	$lreportcard->UpdateReportTemplateColumn($data, "ReportColumnID=$ColID");
				
	switch($cal)
	{
		case "1":
			$del_result = $lreportcard->DELETE_REPORT_TEMPLATE_SUBJECT_WEIGHT('ReportColumnID', $ColID, 'ReportColumnID is not NULL');
			break;
			
		case "2":
			$del_result = $lreportcard->DELETE_REPORT_TEMPLATE_SUBJECT_WEIGHT('ReportColumnID', $ColID, 'SubjectID is not NULL');
			break;	
	}
	
	$i = 0;	
	$data = array();
	foreach($SubjectIDAry as $SubjKey => $SubjID)
	{
		$data[$i]['ReportColumnID'] = $ColID;
		$data[$i]['SubjectID'] = $SubjID;
		$data[$i]['Weight'] = ${"Weight{$key}"}[$SubjKey] / ($usePercentage?"100":"1");
		$i++;	
	}
	$lreportcard->InsertReportTemplateSubjectWeight($ReportID, $data);
	
	$lreportcard->UPDATE_REPORT_LAST_DATE($ReportID, 'DateModified');
}

// [2017-0901-1525-31265] column weights in consolidate report
if($eRCTemplateSetting['Settings']['TemplateSettings_ConsolidatedReportCopyTermReportSubjectColumnWeight'])
{
    $SemID 		    = $basic_data['Semester'];
    $ReportType 	= $SemID == "F" ? "W" : "T";
    $thisFormNumber = $lreportcard->GET_FORM_NUMBER($ClassLevelID, $ByWebSAMSCode = 1);

    if($ReportType == "T" && $thisFormNumber && $thisFormNumber != 6)
    {
        # Get related consolidate report
        $consolidateReportID = $lreportcard->Get_Consolidated_Report_List($ClassLevelID, $ReportID);
        $consolidateReportID = $consolidateReportID[0];

        # Get report columns
        $consolidateColumnData = $lreportcard->returnReportTemplateColumnData($consolidateReportID);
        $consolidateColumnData = BuildMultiKeyAssoc($consolidateColumnData, 'TermReportColumnID');

        if($consolidateReportID && !empty($consolidateColumnData))
        {
            // loop report columns
            foreach($ReportColumnID as $key => $ColID)
            {
                $thisConsolidateColumnData = $consolidateColumnData[$ColID];
                $thisConsolidateColumnID = $thisConsolidateColumnData['ReportColumnID'];
                if(!$thisConsolidateColumnID) {
                    continue;
                }
    
                // update RC_REPORT_TEMPLATE_COLUMN [field: DefaultWeight, DateModified]
                $data = array();
                $data[DefaultWeight] = ($cal==1 and $usePercentage) ? $DefaultWeight[$key] /100 : $DefaultWeight[$key];
                $data[DateModified] = date("Y-m-d H:i:s");
                $lreportcard->UpdateReportTemplateColumn($data, "ReportColumnID=$thisConsolidateColumnID");

                switch($cal)
                {
                    case "1":
                        //$conds = " ReportColumnID IS NOT NULL AND SubjectID IN ('".implode("','", (array)$SubjectIDAry)."') ";
                        $del_result = $lreportcard->DELETE_REPORT_TEMPLATE_SUBJECT_WEIGHT('ReportColumnID', $thisConsolidateColumnID, " ReportColumnID IS NOT NULL ");
                        break;

                    case "2":
                        //$conds = " SubjectID IS NOT NULL AND SubjectID IN ('".implode("','", (array)$SubjectIDAry)."') ";
                        $del_result = $lreportcard->DELETE_REPORT_TEMPLATE_SUBJECT_WEIGHT('ReportColumnID', $thisConsolidateColumnID, " SubjectID IS NOT NULL ");
                        break;
                }

                $i = 0;
                $data = array();
                foreach($SubjectIDAry as $SubjKey => $SubjID)
                {
                    $data[$i]['ReportColumnID'] = $thisConsolidateColumnID;
                    $data[$i]['SubjectID'] = $SubjID;
                    $data[$i]['Weight'] = ${"Weight{$key}"}[$SubjKey] / ($usePercentage?"100":"1");
                    $i++;
                }
                $lreportcard->InsertReportTemplateSubjectWeight($consolidateReportID, $data);

                $lreportcard->UPDATE_REPORT_LAST_DATE($consolidateReportID, 'DateModified');
            }
        }
    }
}

if ($eRCTemplateSetting['VerticalSubjectWeightForOverallForVerticalHorizontalCalculation']) {
	### Save the Assessment Vertical Weight
	//$VerticalWeightArr[$ReportColumnID][$SubjectID] = data	<= $SubjectID = 0 means default subject weight	
	$VerticalWeightArr = $_POST['VerticalWeightArr'];
	
	$SuccessArr['SaveAssessmentSubjectVerticalWeight'] = $lreportcard->Save_Assessment_Subject_Vertical_Weight($ReportID, $VerticalWeightArr);
}

//Check  Re-Generate
if($basic_data['LastGenerated'] != "")
	$xmsg = $eReportCard['SettingUpdateReminder'];

if($Continue)
	header("Location: new3.php?ReportID=$ReportID&xmsg=$xmsg");
else
	header("Location: new2.php?ReportID=$ReportID&xmsg=$xmsg");
?>