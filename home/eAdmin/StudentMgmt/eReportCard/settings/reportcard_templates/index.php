<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
$lreportcard_ui = new libreportcard_ui();
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}
if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$CurrentPage = "Settings_ReportCardTemplates";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# Cookie for maintaining filter status after save / cancel
$arrCookies = array();
$arrCookies[] = array("ck_eRC_Setting_eRCTemplate_Form_Selection", "ClassLevelID"); 
if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
}
else 
	updateGetCookies($arrCookies);
	
	
# tag information
$TAGS_OBJ[] = array($eReportCard['Settings_ReportCardTemplates'], "", 0);
$linterface->LAYOUT_START();


//# ClassLevelID Selection box
//$FormArr = $lreportcard->GET_ALL_FORMS(1);
//for($i=0; $i<sizeof($FormArr); $i++) {
//	$classLevelName[$FormArr[$i]["ClassLevelID"]] = $FormArr[$i]["LevelName"];
//}
//$AllFormOption = array(
//						0 => array(
//						0 => "", 
//						1 => $eReportCard['AllForms']
//						)
//					  );
//$FormArr = array_merge($AllFormOption, $FormArr);


# Filters - By Form (ClassLevelID)
//$FormSelection = $linterface->GET_SELECTION_BOX($FormArr, 'name="ClassLevelID" onChange="window.location=\'index.php?ClassLevelID=\'+this.value+\'&Semester='. $Semester .'\'"', "", $ClassLevelID);
$FormSelection = $lreportcard_ui->Get_Form_Selection('ClassLevelID', $ClassLevelID, 'document.form1.submit();', $noFirst=1, $isAll=1, $hasTemplateOnly=1, $checkPermission=0, $IsMultiple=0);

# Filter: Option(ALL) Correction
if ($ClassLevelID == -1) {
	$ClassLevelID = "";
}

# Load the highlight setting data
//$dataDisplay = $lreportcard->ReportTemplateList();
$dataDisplay = $lreportcard->ReportTemplateList($ClassLevelID);
if(!$dataDisplay)
{
	$dataDisplay = "
					<tr class=\"tablerow2\">
					<td class=\"tabletext\" colspan=\"5\" align=\"center\" height=\"50\">{$i_no_record_exists_msg}</td>
					</tr>
				   ";
}
?>

<script language="javascript">
<!--
function copyForm(ClassLevelID)
{
	jAJAX_updateFormSelect(ClassLevelID, '', 1);
	
	eval("obj = document.getElementById('formTD"+ ClassLevelID +"');");
	eval("document.getElementById('copyFormDiv').style.left = getPostion(obj, 'offsetLeft') - 190 + 'px';"); 	
	eval("document.getElementById('copyFormDiv').style.top = getPostion(obj, 'offsetTop') + 10 + 'px';");
	MM_showHideLayers('copyFormDiv','','show');
}

function copyTerm(ReportID)
{
	jAJAX_updateFormSelect('', ReportID, 0);
	
	eval("obj = document.getElementById('formTermTD"+ ReportID +"');");
	eval("document.getElementById('copyFormDiv').style.left = getPostion(obj, 'offsetLeft') - 190 + 'px';"); 	
	eval("document.getElementById('copyFormDiv').style.top = getPostion(obj, 'offsetTop') + 10 + 'px';");
	MM_showHideLayers('copyFormDiv','','show');
}

function cfmDelete(id)
{
	if(confirm("<?=$i_Discipline_System_alert_remove_warning_level?>"))
	{
		window.location="remove.php?ReportID="+id;
	}
}

function jAJAX_updateFormSelect(ClassLevelID, ReportID, FormCopy)
{
	jFormObject = document.copy_form;
	jFormObject.frForm.value = ClassLevelID;
	jFormObject.frReportID.value = ReportID;
	jFormObject.FormCopy.value = FormCopy;
	
	YAHOO.util.Connect.setForm(jFormObject);
	path = "./aj_index_update_selectForm.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_updateFormSelect);			
}

var callback_updateFormSelect = 
{
	success: function (o)
	{
		jChangeContent( "updateFormSelect_div", o.responseText );						
	}
}

function checkCopyForm()
{
	obj = document.copy_form;
	
	obj.submit();	
}
//-->
</script>

<!-- Start Copy Form //-->
<div id="copyFormDiv" style="position:absolute; width:200px; z-index:1; visibility: hidden;" onMouseOver="MM_showHideLayers('copyFormDiv','','show')">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td height="19"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="5" height="19"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_01.gif" width="5" height="19"></td>
          <td height="19" valign="middle" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_02.gif">&nbsp;</td>
          <td width="19" height="19"><a href="javascript:void(0);" onClick="MM_showHideLayers('copyFormDiv','','hide')"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_close_off.gif" name="pre_close21" width="19" height="19" border="0" id="pre_close21" onMouseOver="MM_swapImage('pre_close21','','<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_close_on.gif',1)" onMouseOut="MM_swapImgRestore()"></a></td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td>
    <form name="copy_form" action="copy_update.php" method="post">
    <input type="hidden" name="frForm" value="">
	<input type="hidden" name="FormCopy" value="">
	<input type="hidden" name="frReportID" value="">

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="5" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_04.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_04.gif" width="5" height="19"></td>
          <td bgcolor="#FFFFF7">
          
          <div id="updateFormSelect_div"></div>
            
              <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr> 
                  <td height="5" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="5"></td>
                </tr>
                <tr>
                  <td><table width="100%" border="0" cellspacing="0" cellpadding="2">
                      <tr>
                        <td>&nbsp;</td>
                        <td align="center" valign="bottom">
                        <?= $linterface->GET_SMALL_BTN($button_submit, "button", "checkCopyForm();") ?>
                        <?= $linterface->GET_SMALL_BTN($button_reset, "reset") ?>
                      </tr>
                  </table></td>
                </tr>
              </table>
            </td>
          <td width="6" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_06.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_06.gif" width="6" height="6"></td>
        </tr>
        <tr>
          <td width="5" height="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_07.gif" width="5" height="6"></td>
          <td height="6" background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_08.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_08.gif" width="5" height="6"></td>
          <td width="6" height="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/can_board_09.gif" width="6" height="6"></td>
        </tr>
    </table>
    </form>
    </td>
  </tr>
</table>
</div>
<!-- End Copy Form //-->

<!-- Start Main Table //-->
<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr> 
    <td align="center" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="right"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                
                <tr>
                  <td align="right" class="tabletextremark">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                          <td><table border="0" cellspacing="0" cellpadding="2">
                              <tr>
                                <td><a href="new.php" class="contenttool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_new.gif" width="18" height="18" border="0" align="absmiddle"> <?=$eReportCard['NewReport']?></a></td>
                                <td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif"></td>
                                <td>&nbsp;</td>
                                <td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif"></td>
                              </tr>
                              
                          </table></td>
                        <td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($msg);?></td>
                      </tr>
                  </table>
                  </td>
                </tr>
        
                <tr>
	                <td>
		                <!-- Filter //-->
						<form name="form1" method="POST" action="index.php">
						<table width="100%" border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td align="left"><?=$FormSelection?></td>
								<td valign="bottom" align="right"></td>
							</tr>
						</table>
						<!-- Filter //-->
	                </td>
                </tr>
                
            </table></td>
          </tr>     
          <!--<tr><td><?=$FormSelection?></td></tr>-->
          <tr>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="4">
                <tr class="tabletop">
                  <td><span class="tabletopnolink"><?=$eReportCard['Form']?></span></td>
                  <td><span class="tabletopnolink"><?=$eReportCard['ReportTitle']?></span></td>
                  <td><span class="tabletopnolink"><?=$eReportCard['Type']?></span></td>
                  <td><span class="tabletopnolink"><?=$eReportCard['LastModifiedDate']?></span></td>
                  <td width="75"><span class="tabletopnolink"><?=$eReportCard['SettingsProgress']?></span></td>
                  <td>&nbsp;</td>
                </tr>
                <?=$dataDisplay?>   
            </table></td>
          </tr>
          <tr>
            <td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
          </tr>
        </table>
          </td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</form>
<!-- End Main Table //-->
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
