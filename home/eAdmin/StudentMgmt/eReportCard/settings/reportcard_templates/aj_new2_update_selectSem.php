<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcard2008w();

// START Build Semester selection for addBtn
$AllSemesters = $lreportcard->returnSemesters();

$SelectedSemAry = $lreportcard->returnReportInvolvedSem($ReportID);
foreach($SelectedSemAry as $semid => $semname)
	$SelectedSem[] = $semid;

$t = 0;
foreach($AllSemesters as $SemID => $SemName)
{
	
	if(in_array($SemID, $SelectedSem)===true)	continue;
	$SemesterSelection[$t][0] = $SemID;
	$SemesterSelection[$t][1] = $SemName;
	$t++;
}
$SemesterSelect = getSelectByArray($SemesterSelection, "name='AddSemester'","",0,1);
// END Build Semester selection for addBtn

intranet_closedb();

echo $SemesterSelect;

?>