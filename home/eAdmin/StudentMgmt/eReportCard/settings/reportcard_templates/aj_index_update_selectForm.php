<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcard2008w();
$lclass = new libclass();
$linterface = new interface_html();

$FormArr = $lclass->getLevelArray();
$toFormAry = array();

for($i=0;$i<sizeof($FormArr);$i++)
{
	if($FormCopy)		// not include frForm
	{
		if($FormArr[$i][0]==$frForm) continue;
	}
	
	$toFormAry[]=$FormArr[$i];
}
$toFormSelection = $linterface->GET_SELECTION_BOX($toFormAry, "name='toForm'", "");
$toTermSelection = "";
if(!$FormCopy)
{
	$termList = $lreportcard->getSelectSemester("name='toTerm'");
	
	$data = $lreportcard->returnReportTemplateBasicInfo($frReportID);
	if($data['Semester'] != "F")
	{
		$toTermSelection = $termList;
	}
}

$x = "
	<table align=\"center\" width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">
	  <tr>
	    <td valign=\"top\" nowrap=\"nowrap\" ><span class=\"tabletext\">". $eReportCard['CopyTo'] ."</span></td>
	    <td width=\"80%\" valign=\"top\">". $toFormSelection ;
	    
	    $x .= "<br>" . $toTermSelection;
	    
$x .= "</td>
	    </tr>
	</table>
";

intranet_closedb();

echo $x;


?>