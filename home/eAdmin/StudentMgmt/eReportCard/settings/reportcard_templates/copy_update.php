<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/eRCConfig.php");

intranet_opendb();

if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

//debug_pr('$frReportID = '.$frReportID);
//debug_pr('$toForm = '.$toForm);
//debug_pr('$toTerm = '.$toTerm);

if($FormCopy)
{
	$copyResult = $lreportcard->ReportTemplateFormCopy($frForm, $toForm);
	$lreportcard->COPY_CLASSLEVEL_SETTINGS($frForm, $toForm, 0, 0); // Copy form grading scheme to target form reports
}
else
{
	$copyResult = $lreportcard->ReportTemplateFormCopy('', $toForm, $toTerm, $frReportID);
	
	$ReportSetting = $lreportcard->returnReportTemplateBasicInfo($frReportID);
	$frForm = $ReportSetting['ClassLevelID'];
}
	
$cr = serialize($copyResult);
intranet_closedb();

?>

<body onLoad="document.form1.submit();">
<form name="form1" action="copy_result.php" method="post">
<input type="hidden" name="FormCopy" value="<?=$FormCopy?>">
<input type="hidden" name="frForm" value="<?=$frForm?>">
<input type="hidden" name="frReportID" value="<?=$frReportID?>">
<input type="hidden" name="toForm" value="<?=$toForm?>">
<input type="hidden" name="toTerm" value="<?=$toTerm?>">
<input type="hidden" name="cr" value="<?=$cr?>">
</form>
</body>
