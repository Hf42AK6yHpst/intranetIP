<?php
/*
 * Run reportcard schema before uploading this files to client site
 */
/***************************************************************
 * 	Modification
 * 	20180205 Bill:	[2017-0907-0952-53240]
 * 		- Update customized report display settings
 * 	20150116 Bill:
 * 		- Update Other Info - Principal Name
 * 	20120316 Marcus:
 * 		- Cater GreaterThanMark for $eRCTemplateSetting['DisplayPosition']['SubjectPositionDisplaySettings']
 * 		- 2011-1130-1655-19066 - 蘇浙公學 - Show position setting in report card template is not working
 * *************************************************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcard2008w();
$lfs = new libfilesystem();

$basic_data = $lreportcard->returnReportTemplateBasicInfo($ReportID);

/* Update RC_REPORT_TEMPLATE */
$data['HeaderHeight'] = $Header==-1 ? -1 : $HeaderHeight;
$data['Footer'] = $Footer;
$data['LineHeight'] = $LineHeight;
$data['DisplaySettings'] = serialize($DisplaySettings);
$data['AllowClassTeacherComment'] = $AllowClassTeacherComment ? 1 : 0;
$data['AllowSubjectTeacherComment'] = $AllowSubjectTeacherComment ? 1 : 0;

$data['ShowSubjectComponent'] = $ShowSubjectComponent ? 1 : 0;
$data['ShowSubjectFullMark'] = $ShowSubjectFullMark ? 1 : 0;
$data['ShowSubjectOverall'] = $ShowSubjectOverall ? 1 : 0;

$data['ShowGrandTotal'] = $ShowGrandTotal ? 1 : 0;
$data['ShowGrandAvg'] = $ShowGrandAvg ? 1 : 0;
$data['ShowOverallPositionClass'] = $ShowOverallPositionClass ? 1 : 0;
$data['ShowOverallPositionForm'] = $ShowOverallPositionForm ? 1 : 0;
$data['OverallPositionRangeClass'] = $OverallPositionRangeClass ? $OverallPositionRangeClass : 0;
$data['OverallPositionRangeForm'] = $OverallPositionRangeForm ? $OverallPositionRangeForm : 0;
$data['GreaterThanAverageClass'] = $GreaterThanAverageClass ? $GreaterThanAverageClass : 0;
$data['GreaterThanAverageForm'] = $GreaterThanAverageForm ? $GreaterThanAverageForm : 0;

$data['ShowNumOfStudentClass'] = $ShowNumOfStudentClass ? 1 : 0;
$data['ShowNumOfStudentForm'] = $ShowNumOfStudentForm ? 1 : 0;

$data['AttendanceDays'] = $AttendanceDays ? $AttendanceDays : 0;
$data['SpecialPercentage'] = $SpecialPercentage ? $SpecialPercentage : 0;
$data['DisplayOption'] = $DisplayOption ? $DisplayOption : 1;
$data['PrincipalName'] = $PrincipalName;

$data["DisplayCommentSection"] = $DisplayCommentSection ? 1 : 0;
$data["DisplayAwardSection"] = $DisplayAwardSection ? 1 : 0;
$data["MaxActivityCount"] = $MaxActivityCount ? $MaxActivityCount : 0;
$data["DisplayActivitySection"] = $DisplayActivitySection ? 1 : 0;

$lreportcard->UpdateReportTemplateBasicInfo($ReportID, $data);

/* Update RC_REPORT_TEMPLATE_COLUMN */
$SelectedSem = $lreportcard->returnReportInvolvedSem($ReportID);
foreach($SelectedSem as $semid => $semname)
{
	$data1[IsDetails] = ${IsDetails.$semid};
	$data1[DateModified] = date("Y-m-d H:i:s");
	$lreportcard->UpdateReportTemplateColumn($data1, "ReportID=$ReportID and SemesterNum=$semid");
}

### Update Position Display Settings
$RangeTypeArr = $_REQUEST['RangeType'];
$UpperLimitArr = $_REQUEST['UpperLimit'];
$GreaterThanMarkArr = $_REQUEST['GreaterThanMark'];
$GreaterThanAverageArr = $_REQUEST['GreaterThanAverage'];
$DisplayPositionArr = $_REQUEST['DisplayPosition'];

$SubjectIDArr = (is_array($RangeTypeArr))? array_keys($RangeTypeArr) : array();
$numOfSubject = count($SubjectIDArr);
$RecordTypeArr = array("Class", "Form");

for ($i=0; $i<$numOfSubject; $i++)
{
	$thisSubjectID = $SubjectIDArr[$i];
	
	# Insert / Update class and form position display
	foreach ($RecordTypeArr as $key => $thisRecordType)
	{
		$DataArr = array();
		$DataArr['ReportID'] = $ReportID;
		$DataArr['SubjectID'] = $thisSubjectID;
		$DataArr['RecordType'] = $thisRecordType;
		$DataArr['RangeType'] = $RangeTypeArr[$thisSubjectID][$thisRecordType];
		$DataArr['UpperLimit'] = $UpperLimitArr[$thisSubjectID][$thisRecordType];
		$DataArr['GreaterThanMark'] = $GreaterThanMarkArr[$thisSubjectID][$thisRecordType];
		$DataArr['GreaterThanAverage'] = $GreaterThanAverageArr[$thisSubjectID][$thisRecordType];
		$DataArr['ShowPosition'] = $DisplayPositionArr[$thisSubjectID][$thisRecordType];
		
		$SettingArr = $lreportcard->Get_Position_Display_Setting($ReportID, $thisSubjectID, $thisRecordType);
		$SettingID = $SettingArr[$thisSubjectID][$thisRecordType]['PositionDisplaySettingID'];
		if ($SettingID == '')
		{
			$SuccessArr['PositionDisplaySetting']['Insert'][$thisSubjectID] = $lreportcard->Insert_Position_Display_Setting($DataArr);
		}
		else
		{
			$SuccessArr['PositionDisplaySetting']['Update'][$thisSubjectID] = $lreportcard->Update_Position_Display_Setting($SettingID, $DataArr);
		}
	}
}

if ($eRCTemplateSetting['ClassTeacherComment']['MaxLengthSettings'] || $eRCTemplateSetting['SubjectTeacherComment']['MaxLengthSettings']) {
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_comment.php");
	$lreportcard_comment = new libreportcard_comment();
}

if ($eRCTemplateSetting['ClassTeacherComment']['MaxLengthSettings']) {
	$ctcMaxLength = $_POST['ctcMaxLength'];
	$SuccessArr['MaxLength']['ClassTeacherComment'] = $lreportcard_comment->Save_Comment_MaxLength($ReportID, 0, $ctcMaxLength);
}

if ($eRCTemplateSetting['SubjectTeacherComment']['MaxLengthSettings']) {
	$stcMaxLengthArr = $_POST['stcMaxLengthArr'];
	foreach ((array)$stcMaxLengthArr as $_subjectId => $_maxLength) {
		$SuccessArr['MaxLength']['SubjectTeacherComment'][$_subjectId] = $lreportcard_comment->Save_Comment_MaxLength($ReportID, $_subjectId, $_maxLength);
	}
}

$imageTypeAry = $eRCTemplateSetting['Settings']['TemplateSettings_ImageAry'];
$numOfImage = count($imageTypeAry);
for ($i=0; $i<$numOfImage; $i++)
{
	$_imageType = $imageTypeAry[$i];
	
	$_imageLocation = $_FILES['image_'.$_imageType]["tmp_name"];
	$_imageName = standardizeFormPostValue($_FILES['image_'.$_imageType]["name"]);
	$_imageError = $_FILES['image_'.$_imageType]["error"];
	
	if ($_imageError == 0)
	{
		include_once($PATH_WRT_ROOT."includes/libreportcard2008_file.php");
		$lreportcard_file = new libreportcard_file();
		
		$infoAry = $lreportcard_file->returnFileInfoAry($ReportID, $_imageType);
		$fileId = $infoAry[0]['FileID'];
		
		$fileObj = new libreportcard_file($fileId);
		$fileObj->setReportId($ReportID);
		$fileObj->setFileType($_imageType);
		$SuccessArr['deleteImage'][$_imageType] = $fileObj->deleteFile();
		$SuccessArr['uploadImage'][$_imageType] = $fileObj->uploadFile($_imageLocation, $_imageName);
	}
	else
	{
		$SuccessArr['uploadImage'][$_imageType] = false;
	}
}

$lreportcard->UPDATE_REPORT_LAST_DATE($ReportID, 'DateModified');

//Check  Re-Generate
if($basic_data['LastGenerated'] != "") {
	$xmsg = $eReportCard['SettingUpdateReminder'];
}
	
if($Continue) {
	header("Location: new5.php?ReportID=$ReportID&xmsg=$xmsg");
}
else {
	header("Location: index.php");
}
?>