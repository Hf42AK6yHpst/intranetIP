<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_file.php");

intranet_auth();
intranet_opendb();

$task = standardizeFormPostValue($_POST['task']);
$successAry = array();

if ($task == "deleteImage") {
	$reportId = standardizeFormPostValue($_POST['reportId']);
	$imageType = standardizeFormPostValue($_POST['imageType']);
	
	$lreportcard_file = new libreportcard_file();
	$infoAry = $lreportcard_file->returnFileInfoAry($reportId, $imageType);
	$fileId = $infoAry[0]['FileID'];
		
	$fileObj = new libreportcard_file($fileId);
	$successAry['deleteHeaderImage'] = $fileObj->deleteFile();
	
	$ReturnString = (in_array(false, (array)$ReturnString))? '0' : '1';
}

intranet_closedb();
echo $ReturnString;

?>