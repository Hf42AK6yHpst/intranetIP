<?php
/*
 * Modifiying: 
 * 
 * Modification Log:
 *  2019-11-20  Bill  [2017-0901-1525-31265]
 *      - added logic to load report column weights in term reports (F1-F5)    ($eRCTemplateSetting['Settings']['TemplateSettings_ConsolidatedReportCopyTermReportSubjectColumnWeight'])
 * 	2015-12-01	Ivan [J89803] [ip.2.5.7.1.1]
 * 	- improved: ajax_yahoo.js and ajax_connection.js change to internal relative path
 * 	2015-07-21 Bill	[2015-0413-1359-48164]
 * 	- display BIBA preset report type
 * 	2012-01-16 Ivan [2012-0105-1529-07054]
 * 	- modified js function checkform(), for 100% checking, change parseInt to parseFloat
 */

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");

$lreportcard = new libreportcard();
$lreportcard_ui = new libreportcard_ui();

$linterface = new interface_html();
$lclass = new libclass();

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$CurrentPage = "Settings_ReportCardTemplates";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# Loading Report Template Data
$basic_data = $lreportcard->returnReportTemplateBasicInfo($ReportID);	// Basic Data
$column_data = $lreportcard->returnReportTemplateColumnData($ReportID); // Column Data
$weight_data = $lreportcard->returnReportTemplateSubjectWeightData($ReportID); // Column Data

$ReportTitle = $basic_data['ReportTitle'];
$ReportTitle = str_replace(":_:", "<br>", $ReportTitle);
$ReportType = $basic_data['Semester'];
$ClassLevelID = $basic_data['ClassLevelID'];

// [2017-0901-1525-31265] load report column weights in term reports if not saved before
if($eRCTemplateSetting['Settings']['TemplateSettings_ConsolidatedReportCopyTermReportSubjectColumnWeight'] && empty($weight_data))
{
    $thisFormNumber = $lreportcard->GET_FORM_NUMBER($ClassLevelID, $ByWebSAMSCode = 1);
    if($ReportType == "F" && $thisFormNumber && $thisFormNumber != 6)
    {
        // Get related term reports
        $relatedTermReport = $lreportcard->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
        $relatedTermReportColumns = BuildMultiKeyAssoc((array)$relatedTermReport, 'ReportColumnID');

        // loop report columns
        $weight_data = array();
        foreach((array)$column_data as $this_column_data)
        {
            $thisColumnID = $this_column_data['ReportColumnID'];
            $thisColumnReportID = $relatedTermReportColumns[$thisColumnID]['ReportID'];
            $thisColumnReportColumnID = $this_column_data['TermReportColumnID'];

            // Get term report column weights
            $thisColumnWeight = $lreportcard->returnReportTemplateSubjectWeightData($thisColumnReportID);
            foreach($thisColumnWeight as $thisColumnWeightInfo)
            {
                // set column weight
                if($thisColumnWeightInfo['ReportColumnID'] && $thisColumnWeightInfo['ReportColumnID'] == $thisColumnReportColumnID)
                {
                    $dataArr = array();
                    $dataArr['ReportColumnID'] = $thisColumnID;
                    $dataArr['SubjectID'] = $thisColumnWeightInfo['SubjectID'];
                    $dataArr['Weight'] = $thisColumnWeightInfo['Weight'];
                    $dataArr['IsDisplay'] = $thisColumnWeightInfo['IsDisplay'];
                    $weight_data[] = $dataArr;
                }
            }
        }
    }
}

$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
$ColumnNo = sizeof($ColumnTitleAry);
$ColumnTitle_Display = "";
$ColumnIDAry = array();

$SelectedSem = array();
$SelectedSemAry = $lreportcard->returnReportInvolvedSem($ReportID);
foreach($SelectedSemAry as $semid => $semname)
	$SelectedSem[] = $semid;

/* 1 - horizontal, 2 - vertical */
$cal = $lreportcard->returnReportTemplateCalculation($ReportID);

if ($eRCTemplateSetting['VerticalSubjectWeightForOverallForVerticalHorizontalCalculation'] && $cal==2) {
	// get report column vertical weight info
	$VerticalWeightAssoArr = $lreportcard->Get_Assessment_Subject_Vertical_Weight($ReportID, 0, $ReturnAsso=true);
}

switch($cal)
{
	case "1":
		$usePercentage = $basic_data['PercentageOnColumnWeight'];
		$ratio_tr = "
			<tr>
				<td colspan=\"2\">&nbsp;</td>
	            <td colspan=\"{$ColumnNo}\" align=\"center\" class=\"tablerow2 term_break_link tabletext retabletop_total\"><span class=\"tabletexttopremark\">". $eReportCard['RatioBetweenAssesments'] ." 
	              <label for=\"usePercentage\"><input name=\"usePercentage\" type=\"checkbox\" id=\"usePercentage\" value=\"1\" onClick=\"clickusePercentage()\" ". ($usePercentage ? "checked":"")."> ". $eReportCard['Use'] ." %</label></span></td>
            </tr>
		";
		
		/* START Build Default Weight Array (for loading data) */
		for($i=0;$i<sizeof($column_data);$i++)
		{
			$DefWeightAry[$column_data[$i]['ReportColumnID']] = $column_data[$i]['DefaultWeight'] * ($usePercentage ? 100 : 1);
		}
		/* END Build Default Weight Array (for loading data) */

		break;
	case "2":
		$ratio_tr = "";
		
		/* START Build Default Weight Array (for loading data) */
		for($i=0;$i<sizeof($column_data);$i++)
		{
			$DefWeightAry[$column_data[$i]['ReportColumnID']] = $column_data[$i]['DefaultWeight'];
		}
		/* END Build Default Weight Array (for loading data) */

		break;
}

/* START Build Subject Weight Array (for loading data) */
for($i=0;$i<sizeof($weight_data);$i++)
{
	$WeightAry[$weight_data[$i]['ReportColumnID']][$weight_data[$i]['SubjectID']] = $weight_data[$i]['Weight'] * ($usePercentage ? 100 : 1);
}
/* END Build Subject Weight Array (for loading data) */

if($ColumnNo)
{
	$left_arrow = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_prev_off.gif\" alt=\"". $eReportCard['MoveLeft'] ."\" title=\"". $eReportCard['MoveLeft'] ."\" width=\"11\" height=\"10\" border=\"0\">";
	$right_arrow = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_next_off.gif\" alt=\"". $eReportCard['MoveRight'] ."\" title=\"". $eReportCard['MoveRight'] ."\" width=\"11\" height=\"10\" border=\"0\">";
	$empty = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"11\" height=\"10\" border=\"0\">";
	
	$c = 0;
	foreach($ColumnTitleAry as $ColID => $ColumnTitle)
	{
		$ColumnIDAry[] = $ColID;
		
		$icon_table = "
			<table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
		      <tr>
		      	<td align=\"left\">". (($c and $ReportType<>"F")? "<a href='javascript:void(0)' onClick='changeOrder(". $ColID .", -1);'>".$left_arrow."</a>" : $empty)."</td>
		        <td align=\"center\">"; 
		        
		        if(!$eRCTemplateSetting['Settings']['TemplateSettings_ConsolidatedReportWithTermReportAssessment'] && sizeof($ColumnTitleAry)>1)
			        $icon_table .= "<a href=\"javascript:void(0)\" onClick=\"jAJAX_addTerm( ". $ColID .", 'del' )\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" align=\"absmiddle\" border=\"0\"></a>";
				if($ReportType<>"F")
			        $icon_table .= "&nbsp;<a href=\"javascript:void(0)\" onClick=\"editAssesment(".$ColID.", ". $c .");\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" align=\"absmiddle\" border=\"0\" id=\"editIcon".$c."\"></a>";
				$icon_table .= "
				</td>
		        <td align=\"right\">". (($c<$ColumnNo-1 and $ReportType<>"F")? "<a href='javascript:void(0)' onClick='changeOrder(". $ColID .", 1);'>".$right_arrow."</a>"  : $empty) ."</td>
		      </tr>
		    </table>
		";
	
		// check css
		if($cal==1)
		{
			$ColTitleCss = "retabletop";
		} 
		else 
		{
			$ColTitleCss = "retabletop_" . ($c%2?"b":"a");
		}
		//$ColumnTitle =  $ReportType=="F" ? $ColumnTitle : convert2unicode($ColumnTitle, 1, 2);
		$ColumnTitle_Display .= "<td align=\"center\" valign=\"top\" class=\"$ColTitleCss tabletopnolink\">". $icon_table. $ColumnTitle ."<br></td>";
		
		$AssigntoAll_Display .= "<td align=\"center\" valign=\"top\" class=\"retablerow1_total tabletext\">";
		$AssigntoAll_Display .= "<input name=\"DefaultWeight[]\" type=\"text\" class=\"ratebox\" value=\"". $DefWeightAry[$ColID] ."\"><span id=\"percentage[]\">". ($usePercentage?"%":" ") ."</span><input type=\"hidden\" name=\"ReportColumnID[]\" value=\"$ColID\"><br>";
		$AssigntoAll_Display .= "</td>";
		$c++;
	}
	
	if($eRCTemplateSetting['VerticalSubjectWeightForOverallForVerticalHorizontalCalculation'] && $cal==2)
	{
		$ColumnTitle_Display .= "<td align=\"center\" valign=\"bottom\" class=\"$ColTitleCss tabletopnolink\" style=\"background-color:#444444;\">Overall<br></td>";
		
		$thisReportColumnID = 0;	// overall column only
		$thisSubjectID = 0;
		$thisId = 'VerticalWeightTb_'.$thisReportColumnID.'_'.$thisSubjectID;
		$thisName = 'VerticalWeightArr['.$thisReportColumnID.']['.$thisSubjectID.']';
		$thisColumnDefaultWeight = $VerticalWeightAssoArr[$thisReportColumnID][$thisSubjectID]['Weight'];
		
		$AssigntoAll_Display .= "<td align=\"center\" class=\"retablerow1_total tabletext\" style=\"background-color:#CCCCCC;\">\r\n";
			$AssigntoAll_Display .= "<input id=\"".$thisId."\" name=\"".$thisName."\" class=\"ratebox DefaultWeightTb\" reportColumnId=\"".$thisReportColumnID."\" type=\"text\" value=\"".$thisColumnDefaultWeight."\">\r\n";
		$AssigntoAll_Display .= "</td>\r\n";
	}
}
else			// No column at init.
{
	if($ReportType=="F")
		$noColumMsg = $eReportCard['NoTermSelected'];
	else
		$noColumMsg = $eReportCard['NoAssesment'];

	$ColumnTitle_Display = "<td align=\"center\" valign=\"top\" class=\"retabletop tabletopnolink\">". $noColumMsg ."<br></td>";
	$AssigntoAll_Display .= "<td align=\"center\" valign=\"top\" class=\"retablerow1_total tabletext\">&nbsp;</td>";
}

switch($ReportType)
{
	case "F":		// Whole Year Report Type
		$ReportTypeTitle = $eReportCard['WholeYearReport'];
		
		if (!$eRCTemplateSetting['Settings']['TemplateSettings_ConsolidatedReportWithTermReportAssessment']) {
			$addBtn = "<a href=\"javascript:void(0);\" class=\"contenttool\" onClick=\"clickAddbtn('addTermLayer')\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> ". $eReportCard['AddTerm'] ."</a>";
		}
		
		// START Build Semester selection for addBtn
		$AllSemesters = $lreportcard->returnSemesters();
		$t = 0;
		
		foreach($AllSemesters as $SemID => $SemName)
		{
			if(in_array($SemID, $SelectedSem)===true)	continue;
			$SemesterSelection[$t][0] = $SemID;
			$SemesterSelection[$t][1] = $SemName;
			$t++;
		}
		$SemesterSelect = getSelectByArray($SemesterSelection, "name='AddSemester'","",0,1);
		// END Build Semester selection for addBtn

		// [2015-0413-1359-48164] BIBA report name
		if($eRCTemplateSetting['Settings']['AutoSelectReportType']){
//			$ReportTypeTitle = "S2 Semester Report (Final)";
			$ReportTypeTitle = $lreportcard->getBIBAReportName("", "", true, true);
		}
		
		break;
	default:		// Term Type
		$ReportTypeTitle = $eReportCard['TermReport'] ." - ". $lreportcard->returnSemesters($SelectedSem[0]);
		$addBtn = "<a href=\"javascript:void(0);\" class=\"contenttool\" onClick=\"clickAddbtn('addTermLayer')\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> ". $eReportCard['AddAssesment'] ."</a>";
		
		// START Build Assesment Select for addBtn
		$t = 0;
		$OrderSelection[$t] = array(-1, "(". $eReportCard['AtBegin'] .")");
		$ColumnTitle2Ary = $lreportcard->returnReportColoumnTitleWithOrder($ReportID);
		if(sizeof($ColumnTitle2Ary))
		{
			foreach($ColumnTitle2Ary as $Order => $ColName)
			{
				$t++;
				$OrderSelection[$t][0] = $Order;
				$OrderSelection[$t][1] = $ColName;
			}
		}
		$OrderSelect = getSelectByArray($OrderSelection, "id='DisplayOrder' name='DisplayOrder'",-1,0,1);
		// END Build Assesment selection for addBtn
		
		// [2015-0413-1359-48164] BIBA report name
		if($eRCTemplateSetting['Settings']['AutoSelectReportType']){
			$form_num = $lreportcard->GET_FORM_NUMBER($ClassLevelID);
 			$sem_num = $lreportcard->Get_Semester_Seq_Number($ReportType);
 			
			$ReportTypeTitle = $lreportcard->getBIBAReportName($form_num, $sem_num, $basic_data["isMainReport"], false, $basic_data['ReportSequence']);
		}

		break;
}

// Start Subject Selected
$SubjectArray = $lreportcard->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $ReportID);

// Get the last sub-subject of each subjects if any & use for css
$LastCmpSubject = array();
if(!empty($SubjectArray))
{
	foreach($SubjectArray as $SubjectID => $Data)
	{
		if(sizeof($Data)>1)
		{
			foreach($Data as $a => $b)
			{
				$LastCmpSubject[$SubjectID] = $a;
			}
		}
	}
}

$Subject_Display = "";
if(!empty($SubjectArray))
{
	$mainSubRow = 0;
	foreach($SubjectArray as $SubjectID => $Data)
	{
		if(is_array($Data))
		{
			$r = 0;
			foreach($Data as $CmpSubjectID => $SubjectName)
			{	
				if($CmpSubjectID > 0)	// sub subject
				{
	 				$css1 = (count($LastCmpSubject) > 0 && in_array($CmpSubjectID, $LastCmpSubject)===true) ? 'tablelist_subject':'tablelist_subject_head';		
					$css2 = 'tablelist_sub_subject';	
	 				$Subject = "
						<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
						<tr>
							<td width=\"13\">
								<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
								<tr>
									<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"13\" height=\"13\"></td>
								</tr>
								<tr>
									<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"13\" height=\"13\"></td>
								</tr>
								</table>
							</td>
							<td class=\"tabletext\">{$SubjectName}</td>
						</tr>
						</table>
					";
				}
				else					// main subject
				{
					$css1 = 'tablelist_subject';		
					$css2 = 'tablelist_subject';
					$Subject = $SubjectName;
				}
				$SubjectID = ($CmpSubjectID==0) ? $SubjectID : $CmpSubjectID;
				
				$Subject_Display .= "<tr>";
				$Subject_Display .= "
					<td width=\"15\" align=\"left\" class=\"{$css1}\">
						<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
						<tr>
							<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"13\" height=\"13\"></td>
						</tr>
						<tr>
							<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"13\" height=\"13\"></td>
						</tr>
						</table>
					</td>
				";
				$Subject_Display .= "<td width=\"25%\" class=\"{$css2}\" >{$Subject} </td>";

				// check css
				if($cal==1)
				{
					$rowCss = "retablerow".($mainSubRow%2+1);	
				}
								
				// Fields Column
				for($tc=0;$tc<$ColumnNo;$tc++)
				{
					if($cal==2)
					{
						$rowCss = "retablerow".($mainSubRow%2+1)."_".($tc%2?"b":"a");	
					}
				
					$Subject_Display .="<td align=\"center\" class=\"". $rowCss ." tabletext\">";
					$Subject_Display .= ($CmpSubjectID) ? "<img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"33\" height=\"13\">" : "";
					$Subject_Display .= "<input name=\"Weight{$tc}[]\" type=\"text\" class=\"ratebox\" value=\"". $WeightAry[$ColumnIDAry[$tc]][$SubjectID] ."\"><span id=\"percentage[]\" name=\"percentage[]\">". ($usePercentage?"%":" ") ."</span></td>";
				}
				if(!$ColumnNo)
				{
					$Subject_Display .="<td align=\"center\" class=\"". $rowCss ." tabletext\">--</td>";
				}
				
				if($eRCTemplateSetting['VerticalSubjectWeightForOverallForVerticalHorizontalCalculation'] && $cal==2) {
					$thisReportColumnID = 0;	// for overall column only
					$thisTbId = 'VerticalWeightTb_'.$thisReportColumnID.'_'.$SubjectID;
					$thisTbClass = 'VerticalWeightTb_'.$thisReportColumnID;
					$thisTbName = 'VerticalWeightArr['.$thisReportColumnID.']['.$SubjectID.']';
					$thisBgColor = ($mainSubRow%2+1 == 1)? '#EEEEEE' : '#CCCCCC';
					$thisWeight = $VerticalWeightAssoArr[$thisReportColumnID][$SubjectID]['Weight'];
					
					$Subject_Display .= "<td align=\"center\" class=\"$rowCss tabletext\" style=\"background-color:".$thisBgColor.";\">";
						$Subject_Display .= ($CmpSubjectID) ? "<img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"33\" height=\"13\">" : "";
						$Subject_Display .= "<input type=\"text\" id=\"$thisTbId\" name=\"$thisTbName\" class=\"ratebox ".$thisTbClass."\" value=\"". $thisWeight ."\">";
					$Subject_Display .= "</td>";
				}
				
				$Subject_Display .= "</tr>"."\n";
				$r++;
			}
		}
		$mainSubRow++;
	}
}
else
{
	$Subject_Display .= "<tr>";
	$Subject_Display .="<td align=\"center\" class=\"". $rowCss ." tabletext\" colspan=\"". ($ColumnNo+3) ."\">". $eReportCard['NoSubjectSettings'] ."</td>";
	$Subject_Display .= "</tr>"."\n";
}
// End Subject Selected

# tag information
$TAGS_OBJ[] = array($eReportCard['Settings_ReportCardTemplates'], "", 0);

$linterface->LAYOUT_START();

$STEPS_OBJ[] = array($eReportCard['InputBasicInformation'], 0);
if($ReportType<>"F")	# Term Report type
{
	if($cal==1)
	{
		$STEPS_OBJ[] = array($eReportCard['InputAssessmentRatio'], 1);
		$STEPS_OBJ[] = array($eReportCard['InputOverallSubjectWeight'], 0);	
	}
	else
	{
		$STEPS_OBJ[] = array($eReportCard['InputSubjectWeight'], 1);
		$STEPS_OBJ[] = array($eReportCard['InputAssessmentRatio'], 0);	
	}
}
else					# Whole year report
{
	if($cal==1)
	{
		$STEPS_OBJ[] = array($eReportCard['InputTermsRatio'], 1);
		$STEPS_OBJ[] = array($eReportCard['InputOverallSubjectWeight'], 0);	
	}
	else
	{
		$STEPS_OBJ[] = array($eReportCard['InputSubjectWeight'], 1);
		$STEPS_OBJ[] = array($eReportCard['InputTermsRatio'], 0);	
	}
}
$STEPS_OBJ[] = array($eReportCard['InputReportDisplayInformation'], 0);
$STEPS_OBJ[] = array($button_preview, 0);
#$STEPS_OBJ[] = array($eReportCard['Finishandpreview'], 0);
?>

<!--script type="text/javascript" src="http://<?=$eclass_httppath?>/src/includes/js/ajax_yahoo.js" ></script-->
<!--script type="text/javascript" src="http://<?=$eclass_httppath?>/src/includes/js/ajax_connection.js" ></script-->
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/ajax_yahoo.js" ></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/ajax_connection.js" ></script>
<script language="javascript">

function js_Go_Back() {
	window.location='index.php';
}

<!--
function editAssesment(ColID, iconid)
{
	jFormObject = document.form1;
 	jFormObject.ColID.value = ColID;
 	jFormObject.UpdateColumnTitle.value = 1;
	YAHOO.util.Connect.setForm(jFormObject);
	path = "./aj_new2_regen_layerform.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_editAssesment);		
	
	divName = "addTermLayer";
	btnid = "editIcon"+iconid;
	
	var w = document.getElementById(divName).style.width;
	w = w.substr(0,w.length-2);
 	eval("document.getElementById('"+ divName +"').style.left = getPostion(document.getElementById('" + btnid + "'), 'offsetLeft') + 20 - w + 'px';");
 	eval("document.getElementById('"+ divName +"').style.top = getPostion(document.getElementById('" + btnid + "'), 'offsetTop') + 20 + 'px';");
	eval("MM_showHideLayers('"+ divName +"','','show');");
}

var callback_editAssesment = 
{
	success: function (o)
	{
		jChangeContent( "addTermLayerMain", o.responseText );						
	}
}

function changeOrder(ColID, Order)
{
	MM_showHideLayers('addTermLayer','','hide');
	
	jFormObject = document.form1;
	jFormObject.TakeAction.value = "changeOrder";
 	jFormObject.ColID.value = ColID;
 	jFormObject.Order.value = Order;
 	jFormObject.UpdateColumnTitle.value = "";
	YAHOO.util.Connect.setForm(jFormObject);

	path = "./aj_new2_update_term.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_addbtn);		
}

function clickusePercentage()
{
	obj = document.getElementById('usePercentage').checked;

	x = document.getElementsByName('percentage[]');
	for(i=0;i<x.length;i++)
	{
		if(obj)
			x[i].innerHTML = "%";			
		else
			x[i].innerHTML = "&nbsp;";			
	}
}

function clickAddbtn(divName)
{
	jFormObject = document.form1;
 	jFormObject.UpdateColumnTitle.value = "";
 	jFormObject.DisplayOrder.value = "";
 	jFormObject.PositionRange1.value = "";
 	jFormObject.PositionRange2.value = "";
 	jFormObject.BlockMarksheet.value = "";
	YAHOO.util.Connect.setForm(jFormObject);

	path = "./aj_new2_regen_layerform.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_editAssesment);		
		
	eval("w = document.getElementById('"+ divName +"').style.width;");
	w = w.substr(0,w.length-2);
	eval("document.getElementById('"+ divName +"').style.left = getPostion(document.getElementById('addBtnID'), 'offsetLeft') + 50 - w + 'px';");
	eval("document.getElementById('"+ divName +"').style.top = getPostion(document.getElementById('addBtnID'), 'offsetTop') + 20 + 'px';");
	eval("MM_showHideLayers('"+ divName +"','','show');");
}

function submitAddbtn()
{
	<? if($ReportType=="F") { ?>
		jAJAX_addTerm(document.addTermLayerForm.AddSemester.value, 'add');
	<? } else {?>
		flag = 1;

		// check value
		FormObj = document.addAsseesmentLayerForm;
		if(!Trim(document.getElementById('column_title').value))
		{
			flag = 0;
			alert("<?=$i_alert_pleasefillin?><?=$eReportCard['ColumnTitle']?>");
		}
		
		jFormObject = document.form1;
		jFormObject.column_title.value = document.getElementById('column_title').value;
		jFormObject.DisplayOrder.value = document.getElementById('DisplayOrder').value;
		jFormObject.PositionRange1.value = document.getElementById('PositionRange1').value;
		jFormObject.PositionRange2.value = document.getElementById('PositionRange2').value;
		jFormObject.BlockMarksheet.value = document.getElementById('BlockMarksheet').checked;
		
		Pos = document.getElementsByName('show_positionT');
		for(i=0;i<Pos.length;i++)
		{
			if(Pos[i].checked)	
				jFormObject.show_position.value = i;
		}
			
		if(flag)
			jAJAX_addTerm('', 'add');
	<? } ?>
	
	MM_showHideLayers('addTermLayer','','hide');
}

function jAJAX_addTerm( addsemester, act ) 
{
	jFormObject = document.form1;

	var access = false;
	if(act=="del")
	{
		jFormObject.UpdateColumnTitle.value = "";
 		jFormObject.DisplayOrder.value = "";
		if(confirm("<?=$i_Discipline_System_alert_remove_warning_level?>?"))	
			access = true;	
	} else 
		access = true;
		
	if(access)
	{
		jFormObject.AddSemester.value = addsemester;
		jFormObject.TakeAction.value = act;
		YAHOO.util.Connect.setForm(jFormObject);

		path = "./aj_new2_update_term.php";
		var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_addbtn);		
		
		jAJAX_updateSelectSem();
		jAJAX_updateSelectPosition();
	}
	
	MM_showHideLayers('addTermLayer','','hide');
}

var callback_addbtn = 
{
	success: function (o)
	{
		jChangeContent( "main_content", o.responseText );						
	}
}

function jAJAX_updateSelectSem()
{
	jFormObject = document.form1;
	YAHOO.util.Connect.setForm(jFormObject);

	path = "./aj_new2_update_selectSem.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_selectSem_div);			
}

function jAJAX_updateSelectPosition()
{
	jFormObject = document.form1;
	YAHOO.util.Connect.setForm(jFormObject);

	path = "./aj_new2_update_selectPosition.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_selectPosition_div);			
}

var callback_selectSem_div = 
{
	success: function (o)
	{
		jChangeContent( "selectSem_div", o.responseText );						
	}
}

var callback_selectPosition_div = 
{
	success: function (o)
	{
		jChangeContent( "selectPosition_div", o.responseText );						
	}
}

function clickAssignAll()
{
	subNo = document.getElementsByName('Weight0[]').length;
	defWeight = document.getElementsByName('DefaultWeight[]');
	
 	for(i=0;i<defWeight.length;i++)
 	{
	 	eval("temp = document.getElementsByName('Weight"+ i+"[]');");
	 	val = defWeight[i].value;
		for(j=0;j<subNo;j++)
		{
			temp[j].value = val;
		}
 	}
 	
 	$('input.DefaultWeightTb').each( function () {
 		var jsThisReportColumnID = $(this).attr('reportColumnId');
 		var jsThisDefaultWeight = $(this).val();
 		
 		$('input.VerticalWeightTb_' + jsThisReportColumnID).each( function () {
 			$(this).val(jsThisDefaultWeight);
 		})
 	});
}

function checkform()
{
 	// check Default Weight
 	defaultWeight = document.getElementsByName('DefaultWeight[]');
 	if(!defaultWeight.length)	return false;
 	
 	for(i=0;i<defaultWeight.length;i++)
 	{
	 	if(!Trim(defaultWeight[i].value))
	 	{
		 	alert("<?=$eReportCard['MissingDefaultWeight']?>");
		 	return false;	
	 	}
	 	
	 	if(isNaN(defaultWeight[i].value))
	 	{
		 	alert("<?=$eReportCard['DefaultWeightNumeric']?>");
		 	return false;	
	 	}
 	}
 	
 	// check Weight
 	subNo = document.getElementsByName('Weight0[]').length;
	for(i=0;i<defaultWeight.length;i++)
 	{
	 	eval("temp = document.getElementsByName('Weight"+ i+"[]');");
		for(j=0;j<subNo;j++)
		{
			if(!Trim(temp[j].value))
		 	{
			 	alert("<?=$eReportCard['MissingWeight']?>");
			 	return false;	
		 	}
	 	
			if(isNaN(temp[j].value) || !(Trim(temp[j].value)))
		 	{
			 	alert("<?=$eReportCard['WeightNumeric']?>");
			 	return false;	
		 	}
		}
 	}
 	
 	// Check 100%
 	<? if($cal==1) {?>
 	flag = 1;
 	if(document.form1.usePercentage.checked)
 	{
	 	for(j=0;j<subNo;j++)
	 	{
		 	temp = 0;
		 	for(i=0;i<defaultWeight.length;i++)
		 	{
			 	eval("t = document.getElementsByName('Weight"+ i+"[]');");
			 	//temp = temp + parseInt(t[j].value);
			 	temp = temp + parseFloat(t[j].value);
		 	}
		 	if(temp != 100)
		 	{
				flag = 0;
		 	}
	 	}
 	}
 	
 	if(!flag)
 	{
	 	if(!confirm("<?=$eReportCard['SubjectTotleNot100']?>"))
			return false;	
	}
	<? } ?>
}

function confirmSkip()
{
	defaultWeight = document.getElementsByName('DefaultWeight[]');
 	if(!defaultWeight.length)	return false;
 	
	if(confirm("<?=$eReportCard['SkipStep']?>"))
	{
		document.form1.reset();
		document.form1.skip.value=1;
		return checkform();
	}
	document.form1.skip.value=0;

	return false;
}
//-->
</script>

<? if($ReportType=="F") {?>                            
<!-- START add term div //-->
<div id="addTermLayer" style="position:absolute; width:200px; z-index:1; visibility: hidden;">
<form name="addTermLayerForm">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td height="19"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="5" height="19"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_01.gif" width="5" height="19"></td>
              <td height="19" valign="middle" background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_02.gif">&nbsp;</td>
              <td width="19" height="19"><a href="javascript:void(0);" onClick="MM_showHideLayers('addTermLayer','','hide')"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_close_off.gif" name="pre_close21" width="19" height="19" border="0" id="pre_close21" onMouseOver="MM_swapImage('pre_close21','','<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_close_on.gif',1)" onMouseOut="MM_swapImgRestore()"></a></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="5" background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_04.gif"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_04.gif" width="5" height="19"></td>
              <td bgcolor="#FFFFF7"><table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                  
                  <tr>
                    <td valign="top" nowrap="nowrap" class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;"><span class="tabletext"><?=$eReportCard['SelectTerm']?></span></td>
                    <td valign="top"><div id="selectSem_div"><?=$SemesterSelect?></div></td>
                  </tr>
                  
                </table>
                  <table width="90%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
                    </tr>
                    <tr>
                      <td><table width="100%" border="0" cellspacing="0" cellpadding="2">
                          <tr>
                            <td>&nbsp;</td>
                            <td align="center" valign="bottom" nowrap>
                            	<?= $linterface->GET_SMALL_BTN($button_submit, "button", "submitAddbtn();") ?>
                            	<?= $linterface->GET_SMALL_BTN($button_reset, "reset") ?>
                          </tr>
                      </table></td>
                    </tr>
                  </table>
                <br></td>
              <td width="6" background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_06.gif"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_06.gif" width="6" height="6"></td>
            </tr>
            <tr>
              <td width="5" height="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_07.gif" width="5" height="6"></td>
              <td height="6" background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_08.gif"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_08.gif" width="5" height="6"></td>
              <td width="6" height="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_09.gif" width="6" height="6"></td>
            </tr>
        </table></td>
      </tr>
    </table>
</form>
</div>
<!-- END add term div //-->       
<? } else { ?>
<!-- START add column div//-->
<div id="addTermLayer" style="position:absolute; width:420px; z-index:1; visibility: hidden;">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<form name="addAsseesmentLayerForm">
  <tr>
    <td height="19"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="5" height="19"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_01.gif" width="5" height="19"></td>
          <td height="19" valign="middle" background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_02.gif">&nbsp;</td>
          <td width="19" height="19"><a href="javascript:void(0);" onClick="MM_showHideLayers('addTermLayer','','hide')"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_close_off.gif" name="pre_close21" width="19" height="19" border="0" id="pre_close21" onMouseOver="MM_swapImage('pre_close21','','<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_close_on.gif',1)" onMouseOut="MM_swapImgRestore()"></a></td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="5" background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_04.gif"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_04.gif" width="5" height="19"></td>
          <td bgcolor="#FFFFF7">
          <div id="addTermLayerMain">
          <table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
              <tr>
                <td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eReportCard['ColumnTitle']?>:</span></td>
                <td width="80%" valign="top" class="tabletext"><input id="column_title" name="column_title" type="text" class="textboxtext"></td>
              </tr>
              <tr>
                <td valign="top" nowrap="nowrap" class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;"><span class="tabletext"><?=$eReportCard['InsertAfter']?></span></td>
                <td valign="top"><div id="selectPosition_div"><?=$OrderSelect?></div></td>
              </tr>
              <tr valign="top">
                <td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eReportCard['ShowPosition']?></span></td>
                <td class="tabletext">
					<LABEL for="show_position0"><input type="radio" name="show_positionT" id="show_position0" checked/><?=$eReportCard['DonotShowPosition']?></LABEL><br>
					<LABEL for="show_position1"><input type="radio" name="show_positionT" id="show_position1" /><?=$eReportCard['ShowPositionInClass']?></LABEL><br>
					<LABEL for="show_position2"><input type="radio" name="show_positionT" id="show_position2" /><?=$eReportCard['ShowPositioninClassLevel']?></LABEL><br>
                    <table border="0" cellspacing="0" cellpadding="1">
                      <tr>
                        <td width="20" valign="top" class="tabletext"><?=$i_From?></td>
                        <td class="tabletext"><input name="PositionRange1" id="PositionRange1" type="text" class="textboxnum"></td>
                        <td class="tabletext"><?=$i_To?></td>
                        <td class="tabletext"><input name="PositionRange2" id="PositionRange2" type="text" class="textboxnum"></td>
                      </tr>
                  	</table>
                </td>
              </tr>
              <tr valign="top">
              	<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eReportCard['BlockMarksheet']?></span></td>
      	        <td class="tabletext"><input name="BlockMarksheet" id="BlockMarksheet" type="checkbox"></td>
              </tr>
            </table>
            </div>
              <table width="90%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
                </tr>
                <tr>
                  <td>
					<table width="100%" border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td>&nbsp;</td>
						<td align="center" valign="bottom" nowrap>
							<?= $linterface->GET_SMALL_BTN($button_submit, "button", "submitAddbtn();") ?>
							<?= $linterface->GET_SMALL_BTN($button_reset, "reset") ?>
						</td>
					</tr>
					</table>
                  </td>
                </tr>
              </table>
            <br></td>
          <td width="6" background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_06.gif"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_06.gif" width="6" height="6"></td>
        </tr>
        <tr>
          <td width="5" height="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_07.gif" width="5" height="6"></td>
          <td height="6" background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_08.gif"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_08.gif" width="5" height="6"></td>
          <td width="6" height="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_09.gif" width="6" height="6"></td>
        </tr>
    </table></td>
  </tr>
</form>
</table>
</div>
<!-- START add column div//-->
<? } ?>
<br/>

<form name="form1" method="post" action="new2_update.php">
<table width="96%" border="0" cellspacing="0" cellpadding="4">
<tr>
	<td>
		<?= $lreportcard_ui->Get_Settings_ReportCardTemplate_Nav() ?>
		<br />
		<?= $linterface->GET_STEPS($STEPS_OBJ) ?>   
		<br />
		<?= $lreportcard_ui->Get_Settings_ReportCardTemplate_ReportCardInfo_Table($ReportID) ?>
				
          <table width="100%" border="0" cellspacing="0" cellpadding="2">
          <tr>
          	<td align="right"><?=$linterface->GET_SYS_MSG($msg, $xmsg);?></td>
          	</tr>
      
            <tr>
              <td align="left" valign="top">
              
              <div id="main_content">
              <table width="100%" border="0" cellspacing="0" cellpadding="4">
					<tr>
						<td width="15" align="left">&nbsp;</td>
						<td width="25%" align="right" valign="top">&nbsp;</td>
						<td align="center" valign="top" class="tablename" colspan="<?=$ColumnNo?>"><table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
                                        <td class="tabletext"><strong><?=$ReportTypeTitle?></strong></td>
                                        <td align="right"><table border="0" cellspacing="0" cellpadding="2" id="addBtnID">
                                            <tr>
                                              <td>&nbsp;</td>
                                              <td><?=$addBtn?></td>
                                            </tr>
                                        </table></td>
                                      </tr>
						</table></td>
					</tr>
					<tr>
						<td width="15" align="left">&nbsp;</td>
						<td width="25%" align="right" valign="bottom" class="tabletextremark">&nbsp;</td>
						<!-- Table Header TH//-->
						<?=$ColumnTitle_Display?>
					</tr>
					
					<?=$ratio_tr?>
					
					<tr>
						<td width="15" align="left">&nbsp;</td>
						<td width="25%" align="right" valign="bottom" class="tabletextremark">
							<table  border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td><a href="javascript:void(0);" onClick="clickAssignAll();" class="tabletool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_copy.gif" width="12" height="12" border="0" align="absmiddle"><?=$eReportCard['AssignToAll']?></a></td>
								</tr>
							</table>
						</td>
						<?=$AssigntoAll_Display?>
					</tr>
					<?=$Subject_Display?>
					
				</table>
				</div>					
				</td>
            </tr>
          </table>
	</td>
</tr>
</table>

<br />
<table width="96%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
  </tr>
  <tr>
    <td align="center">
    	<? if(!empty($SubjectArray)) {?>
    	<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "document.form1.Continue.value=0;return checkform();") ?>
      	<?= $linterface->GET_ACTION_BTN($button_submit_continue, "submit", "document.form1.Continue.value=1;return checkform();") ?>
      	<?= $linterface->GET_ACTION_BTN($button_reset, "reset") ?>
      	<?= $linterface->GET_ACTION_BTN($button_skip, "submit", "return confirmSkip();") ?>
      	<? } ?>
		<?= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='new.php?ReportID=$ReportID'") ?>
	</td>
  </tr>
</table>

<input type="hidden" name="ReportID" value="<?=$ReportID?>">
<input type="hidden" name="AddSemester" value="">
<input type="hidden" name="TakeAction" value="">
<input type="hidden" name="column_title" value="">
<input type="hidden" name="DisplayOrder" value="">
<input type="hidden" name="show_position" value="">
<input type="hidden" name="PositionRange1" value="">
<input type="hidden" name="PositionRange2" value="">
<input type="hidden" name="BlockMarksheet" value="">
<input type="hidden" name="Continue" value="0">
<input type="hidden" name="ColID" value="">
<input type="hidden" name="Order" value="">
<input type="hidden" name="UpdateColumnTitle" value="">
<input type="hidden" name="skip" value="0">
</form>

<br />

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
