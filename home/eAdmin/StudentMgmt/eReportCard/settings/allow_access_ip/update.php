<?php
// using : 

############# Change Log (start) #################
#
#	Date:	2017-07-06 Anna
#			create file for update Allow Access IP -- copy from EJ
#
############## Change Log (end) ##################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

if(!$plugin['ReportCard2008'])
{
	intranet_closedb();
	header("Location: index.php");
}
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "")
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

$allowed_IPs = trim($lreportcard->geteReportCardGeneralSetting("AllowAccessIP"));
$ip_addresses = explode("\n", $allowed_IPs);

checkCurrentIP($ip_addresses, $eReportCard['ReportCard']);

include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
$lreportcard_ui = new libreportcard_ui();
if (!$lreportcard->hasAccessRight())
{
	header ("Location: /");
	intranet_closedb();
	exit();
}
// post data
$AllowAccessIP = trim($_POST['AllowAccessIP']);

// get current IP settings
$currentIP = $lreportcard->geteReportCardGeneralSetting("AllowAccessIP");

// update
if(sizeof($currentIP)>0){
	$sql = "Update GENERAL_SETTING
	Set Module='eReportCard', SettingValue='$AllowAccessIP', DateInput=NOW(), ModifiedBy='".$_SESSION['UserID']."'
			WHERE SettingName='AllowAccessIP' AND Module='eReportCard'";
}
// insert
else{
	$sql = "INSERT INTO GENERAL_SETTING (Module, SettingName, SettingValue, DateInput,InputBy, DateModified, ModifiedBy)
	VALUES ('eReportCard', 'AllowAccessIP', '$AllowAccessIP', NOW(),'".$_SESSION['UserID']."', NOW(), '".$_SESSION['UserID']."')";
}
$result = $lreportcard->db_db_query($sql);

if($result)
	$msg = "update";
	else
	$msg = "update_failed";
		
intranet_closedb();
		
header("Location: index.php?msg=$msg");
?>