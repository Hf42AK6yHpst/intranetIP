<?php
// Modifying by Bill

/********************************************************
 * Modification log
 * 20201103 Bill    [2020-0915-1830-00164]
 *      - support term setting update   ($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings'])
 * ******************************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_subjectTopic.php");

$lreportcard = new libreportcardcustom();
$lreportcard_subjectTopic = new libreportcard_subjectTopic();

$lreportcard->hasAccessRight();

$formId = standardizeFormPostValue($_POST['formId']);
$subjectId = standardizeFormPostValue($_POST['subjectId']);
$subjectTopicId = standardizeFormPostValue($_POST['subjectTopicId']);
$code = standardizeFormPostValue($_POST['code']);
$nameEn = standardizeFormPostValue($_POST['nameEn']);
$nameCh = standardizeFormPostValue($_POST['nameCh']);
// [2020-0915-1830-00164]
$targetTermSeq = standardizeFormPostValue($_POST['targetTermSeq']);

//$success = $lreportcard_subjectTopic->saveSubjectTopic($formId, $subjectId, $subjectTopicId, $code, $nameEn, $nameCh);
$success = $lreportcard_subjectTopic->saveSubjectTopic($formId, $subjectId, $subjectTopicId, $code, $nameEn, $nameCh, $targetTermSeq);

if ($success > 0) {
	$returnMsgKey = 'UpdateSuccess';
}
else {
	$returnMsgKey = 'UpdateUnsuccess';
}

intranet_closedb();
header('location: index.php?&returnMsgKey='.$returnMsgKey);
?>