<?php
// Modifying by Bill

/********************************************************
 * Modification log
 * 20201103 Bill    [2020-0915-1830-00164]
 *      - support term setting    ($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings'])
 * ******************************************************/

$PageRight = "ADMIN";

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_subjectTopic.php");

$lreportcard = new libreportcardcustom();
$lreportcard_subjectTopic = new libreportcard_subjectTopic();

$lreportcard->hasAccessRight();

$task = ($_POST['task'])? $_POST['task'] : $_GET['task'];

if ($task == 'validateCode') {
	$formId = standardizeFormPostValue($_POST['formId']);
	$subjectId = standardizeFormPostValue($_POST['subjectId']);
	$code = standardizeFormPostValue($_POST['code']);
	$subjectTopicId = standardizeFormPostValue($_POST['subjectTopicId']);
	
	$isCodeValidate = $lreportcard_subjectTopic->isCodeValid($formId, $subjectId, $code, $subjectTopicId);
	echo $isCodeValidate? "1" : "0";
}
else if ($task == 'getSubjectTopicTableHtml') {
	$formId = standardizeFormPostValue($_POST['formId']);
	$subjectId = standardizeFormPostValue($_POST['subjectId']);
	echo $lreportcard_subjectTopic->getSubjectTopicTableHtml($formId, $subjectId);
}
else if ($task == 'reorderSubjectTopic') {
	$displayOrderString = $_POST['displayOrderString'];
	$recordIdAry = explode(',', $displayOrderString);
	$numOfRecord = count($recordIdAry);
	
	$lreportcard_subjectTopic->Start_Trans();
	
	$successAry = array();
	for ($i=0; $i<$numOfRecord; $i++) {
		$_subjectTopicId = $recordIdAry[$i];
		
		$_dataAry = array();
		$_dataAry['DisplayOrder'] = $i + 1; 
		$successAry[$_subjectTopicId] = $lreportcard_subjectTopic->updateSubjectTopic($_subjectTopicId, $_dataAry, $updateLastModified=false);
	}
	
	if (in_array(false, $successAry)) {
		$lreportcard_subjectTopic->RollBack_Trans();
		echo '0';
	}
	else {
		$lreportcard_subjectTopic->Commit_Trans();
		echo '1';
	}
}
else if ($task == 'validateSubjectTopicImport') {
	include_once($PATH_WRT_ROOT.'includes/libimporttext.php');
	$limport = new libimporttext();
	
	
	$targetFilePath = standardizeFormPostValue($_GET['targetFilePath']);
	$importTempTable = $lreportcard->DBName.'.TEMP_SUBJECT_TOPIC_IMPORT';
	
	
	### Get all forms data
	$formAry = $lreportcard->GET_ALL_FORMS($hasTemplateOnly=false, $excludeWithoutClass=false);
	$formAssoAry = BuildMultiKeyAssoc($formAry, 'LevelName', array('ClassLevelID'), $SingleValue=1);
	
	
	### Get all subject data
	$subjectAssoAry = $lreportcard->GET_SUBJECTS_CODEID_MAP($withComponent=1, $mapByCode=1);
	
	
	### Get all subject topic data
	$subjectTopicAssoAry = BuildMultiKeyAssoc($lreportcard_subjectTopic->getSubjectTopic(), array('YearID', 'SubjectID', 'Code'), array('SubjectTopicID'), $SingleValue=1);
	
	
	### Include js libraries
	$jsInclude = '';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
	echo $jsInclude;
	
	
	### Get Data from the csv file
	$csvHeaderAry = $lreportcard_subjectTopic->getCsvHeader($targetPropertyAry=array(1,3));	// Required Column Only
	$columnTitleAry = $csvHeaderAry['En'];
	$columnPropertyAry = $lreportcard_subjectTopic->getCsvHeaderProperty($forValidation=true);
	
	$csvData = $limport->GET_IMPORT_TXT_WITH_REFERENCE($targetFilePath, '', '', $columnTitleAry, $columnPropertyAry);
	$csvColName = array_shift($csvData);
	$numOfData = count($csvData);
	
	$successAry = array();
	$successAry['deleteOldTempData'] = $lreportcard_subjectTopic->deleteImportTempData();
	
	
	$errorCount = 0;
	$successCount = 0;
	$rowErrorRemarksAry = array();
	$insertTempDataAry = array();
	
	for ($i=0; $i<$numOfData; $i++)
	{
		$_errorMsgAry = array();
		
		$_rowNumber = $i + 3;
		$_yearName = trim($csvData[$i][0]);
		$_subjectCode = trim($csvData[$i][1]);
		$_subjectCmpCode = trim($csvData[$i][2]);
		$_subjectTopicCode = trim($csvData[$i][3]);
		$_subjectTopicNameEn = trim($csvData[$i][4]);
        $_subjectTopicNameCh = trim($csvData[$i][5]);
        // [2020-0915-1830-00164]
        if($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings']) {
            $_subjectTopicTermCode = trim($csvData[$i][6]);
        }

		$_yearId = '';
		$_subjectId = '';
		$_subjectName = '';
		$_subjectCmpId = '';
		$_subjectCmpName = '';
		$_subjectTopicId = '';
        $_subjectTopicTermSeq = '';

		// check form name
		if ($_yearName == '') {
			$_errorMsgAry[] = $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['NoForm'];
		}
		else {
			$_yearId = $formAssoAry[$_yearName];
			if ($_yearId == '') {
				$_errorMsgAry[] = $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['FormNotExisting'];
			}
		}

		// check subject code
		if ($_subjectCode == '') {
			$_errorMsgAry[] = $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['NoSubjectCode'];
		}
		else {
			$_subjectId = $subjectAssoAry[$_subjectCode];
			if ($_subjectId == '') {
				$_subjectId = $subjectAssoAry[$_subjectCode.'_'];
				if ($_subjectId == '') {
					$_errorMsgAry[] = $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['SubjectNotExisting'];
				}
				else {
					$_subjectName = $lreportcard->GET_SUBJECT_NAME_LANG($_subjectId);
				}
			}
		}
		
		// check subject component code
		if ($_subjectCmpCode != '') {
			$_subjectCmpId = $subjectAssoAry[$_subjectCode.'_'.$_subjectCmpCode];
			if ($_subjectCmpId == '') {
				$_errorMsgAry[] = $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['SubjectComponentNotExisting'];
			}
			else {
				$_subjectCmpName = $lreportcard->GET_SUBJECT_NAME_LANG($_subjectCmpId);
			}
		}

		// check subject topic code
		if ($_subjectTopicCode == '') {
			$_errorMsgAry[] = $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['NoSubjectTopicCode'];
		}
		
		// check subject topic name (eng)
//		if ($_subjectTopicNameEn == '') {
//			$_errorMsgAry[] = $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['NoSubjectTopicNameEn'];
//		}
		// check subject topic name (chi)
//		if ($_subjectTopicNameCh == '') {
//			$_errorMsgAry[] = $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['NoSubjectTopicNameCh'];
//		}
		if ($_subjectTopicNameEn == '' && $_subjectTopicNameCh == '') {
			$_errorMsgAry[] = $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['NoSubjectTopicName'];
		}
		
		// map subject topic
		if ($_yearId > 0) {
			if ($_subjectCmpId > 0) {
				$_subjectTopicId = $subjectTopicAssoAry[$_yearId][$_subjectCmpId][$_subjectTopicCode];
			}
			else {
				$_subjectTopicId = $subjectTopicAssoAry[$_yearId][$_subjectId][$_subjectTopicCode];
			}
		}

        // [2020-0915-1830-00164] check term seq number
        if($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings'])
        {
            if ($_subjectTopicTermCode == '') {
                $_errorMsgAry[] = $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['NoSubjectTopicTerm'];
            }
            else if (!in_array($_subjectTopicTermCode, (array)$lreportcard_subjectTopic->subjectTopicRelatedTermArr)) {
                $_errorMsgAry[] = $Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['SubjectTopicTermNotExisting'];
            }
            else {
                $_subjectTopicTermSeq = array_search($_subjectTopicTermCode, (array)$lreportcard_subjectTopic->subjectTopicRelatedTermArr);
            }
        }

		### Prepare sql to insert to the temp csv data table
		$insertTempDataAry[] = " (	'".$_SESSION['UserID']."', '$_rowNumber', '$_yearId', '".$lreportcard->Get_Safe_Sql_Query($_yearName)."',
										'".$_subjectId."', '".$lreportcard->Get_Safe_Sql_Query($_subjectCode)."', '".$lreportcard->Get_Safe_Sql_Query($_subjectName)."',
										'".$_subjectCmpId."', '".$lreportcard->Get_Safe_Sql_Query($_subjectCmpCode)."', '".$lreportcard->Get_Safe_Sql_Query($_subjectCmpName)."',
										'".$_subjectTopicId."', '".$lreportcard->Get_Safe_Sql_Query($_subjectTopicCode)."', '".$lreportcard->Get_Safe_Sql_Query($_subjectTopicNameEn)."', '".$lreportcard->Get_Safe_Sql_Query($_subjectTopicNameCh)."',
										'$_subjectTopicTermSeq', now()
									) ";
		
		
		### Record the error messages				
		if(count($_errorMsgAry) == 0)
			$successCount++;
		else
			$rowErrorRemarksAry[$_rowNumber] = $_errorMsgAry;
		
		
		### Update Processing Display
		Flush_Screen_Display(1);
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= '$("span#BlockUISpan", window.parent.document).html("'.($i + 1).'");';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
	}
	$errorCount = count($rowErrorRemarksAry);
	
	
	### Insert Data in Temp Table
	if (count($insertTempDataAry) > 0) {
		$sql = "Insert Into $importTempTable
					(UserID, RowNumber, YearID, YearName, SubjectID, SubjectCode, SubjectName, SubjectCmpID, SubjectCmpCode, SubjectCmpName, SubjectTopicID, SubjectTopicCode, SubjectTopicNameEn, SubjectTopicNameCh, TargetTermSeq, DateInput)
				Values
					".implode(', ', (array)$insertTempDataAry);
		$successAry['insertCsvDataToTempTable'] = $lreportcard->db_db_query($sql);
	}

	
	### Display Record Error Table
	$errorCountDisplay = ($errorCount ? "<font color=\"red\"> ": "") . $errorCount . ($errorCount ? "</font>" : "");

	$x = '';
	if($errorCount > 0)
	{
		// get table header
		$csvHeaderAry = $lreportcard_subjectTopic->getCsvHeader();	// Required Column Only
		$columnTitleDisplayAry = Get_Lang_Selection($csvHeaderAry['Ch'], $csvHeaderAry['En']);
		$numOfColumn = count($columnTitleDisplayAry);
		
		// get table content data
		$errorRowNumAry = array_keys($rowErrorRemarksAry);
		$sql = "select * from $importTempTable where UserID = '".$_SESSION['UserID']."' And RowNumber In ('".implode("','", (array)$errorRowNumAry)."') ";
		$errorRowInfoAry = $lreportcard->returnResultSet($sql);
		
		$x .= '<table class="common_table_list_v30 view_table_list_v30">';
			$x .= '<thead>';
				$x .= '<tr>';
					$x .= '<th>'.$Lang['General']['ImportArr']['Row'].'</th>';
						for ($i=0; $i<$numOfColumn; $i++) {
							$x .= '<th>'.$columnTitleDisplayAry[$i].'</th>';
						}
					$x .= '<th>'.$Lang['General']['Remark'].'</th>';
				$x .= '</tr>';
			$x .= '</thead>';
			
			$x .= '<tbody>';
				for ($i=0; $i<$errorCount; $i++)
				{
					$_tempId = $errorRowInfoAry[$i]['TempID'];
					$_rowNumber = $errorRowInfoAry[$i]['RowNumber'];
					$_yearName = $errorRowInfoAry[$i]['YearName'];
					$_subjectCode = $errorRowInfoAry[$i]['SubjectCode'];
					$_subjectName = $errorRowInfoAry[$i]['SubjectName'];
					$_subjectCmpCode = $errorRowInfoAry[$i]['SubjectComponentCode'];
					$_subjectCmpName = $errorRowInfoAry[$i]['SubjectComponentName'];
					$_subjectTopicCode = $errorRowInfoAry[$i]['SubjectTopicCode'];
					$_subjectTopicNameEn = $errorRowInfoAry[$i]['SubjectTopicNameEn'];
					$_subjectTopicNameCh = $errorRowInfoAry[$i]['SubjectTopicNameCh'];

                    // [2020-0915-1830-00164]
                    $_subjectTopicTermSeq = $errorRowInfoAry[$i]['TargetTermSeq'];
                    if($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings']) {
                        $_subjectTopicTermSeq = $lreportcard_subjectTopic->subjectTopicRelatedTermArr[$_subjectTopicTermSeq];
                    }
					
					$_yearName = $_yearName ? $_yearName : '<font color="red">***</font>';
					$_subjectCode = $_subjectCode ? $_subjectCode : '<font color="red">***</font>';
					$_subjectTopicCode = $_subjectTopicCode ? $_subjectTopicCode : '<font color="red">***</font>';
					$_subjectTopicNameEn = ($errorRowInfoAry[$i]['SubjectTopicNameCh'] || $errorRowInfoAry[$i]['SubjectTopicNameEn']) ? $_subjectTopicNameEn : '<font color="red">***</font>';
					$_subjectTopicNameCh = ($errorRowInfoAry[$i]['SubjectTopicNameCh'] || $errorRowInfoAry[$i]['SubjectTopicNameEn']) ? $_subjectTopicNameCh : '<font color="red">***</font>';
					
					$_errorRemarksAry = $rowErrorRemarksAry[$_rowNumber];
					$_errorDisplay = '';
					if (is_array($_errorRemarksAry) && count($_errorRemarksAry) > 0) {
						if (in_array($Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['FormNotExisting'], $_errorRemarksAry)) {
							$_yearName = '<font color="red">'.$_yearName.'</font>';
						}
						if (in_array($Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['SubjectNotExisting'], $_errorRemarksAry)) {
							$_subjectCode = '<font color="red">'.$_subjectCode.'</font>';
						}
						if (in_array($Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['SubjectComponentNotExisting'], $_errorRemarksAry)) {
							$_subjectCmpCode = '<font color="red">'.$_subjectCmpCode.'</font>';
						}

                        // [2020-0915-1830-00164]
                        if($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings'])
                        {
                            if (in_array($Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['NoSubjectTopicTerm'], $_errorRemarksAry)) {
                                $_subjectTopicTermSeq = '<font color="red">'.$_subjectTopicTermSeq.'</font>';
                            }
                            if (in_array($Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['SubjectTopicTermNotExisting'], $_errorRemarksAry)) {
                                $_subjectTopicTermSeq = '<font color="red">'.$_subjectTopicTermSeq.'</font>';
                            }
                        }
						
						// Error Message Display
						$_errorDisplay = '- '.implode('<br />- ', $_errorRemarksAry);
					}

					
					$x .= '<tr>';
						$x .= '<td>'. $_rowNumber .'</td>';
						$x .= '<td>'. $_yearName .'</td>';
						$x .= '<td>'. $_subjectCode .'</td>';
						$x .= '<td>'. $_subjectName .'</td>';
						$x .= '<td>'. $_subjectCmpCode .'</td>';
						$x .= '<td>'. $_subjectCmpName .'</td>';
						$x .= '<td>'. $_subjectTopicCode .'</td>';
						$x .= '<td>'. $_subjectTopicNameEn .'</td>';
						$x .= '<td>'. $_subjectTopicNameCh .'</td>';
                        // [2020-0915-1830-00164]
                        if($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings']) {
                            $x .= '<td>'. $_subjectTopicTermSeq .'</td>';
                        }
						$x .= '<td>'. $_errorDisplay .'</td>';
					$x .= '</tr>';
				}
        $x .= '</tbody>';
		$x .= '</table>';
		$htmlAry['errorTbl'] = $x;
	}
	
	$errorCountDisplay = ($errorCount > 0) ? "<font color=\"red\">".$errorCount."</font>" : $errorCount;
	
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= 'window.parent.document.getElementById("ErrorTableDiv").innerHTML = \''.$htmlAry['errorTbl'].'\';';
		$thisJSUpdate .= 'window.parent.document.getElementById("SuccessCountDiv").innerHTML = \''.$successCount.'\';';
		$thisJSUpdate .= 'window.parent.document.getElementById("FailCountDiv").innerHTML = \''.$errorCountDisplay.'\';';
		
		if ($errorCount == 0) {
			$thisJSUpdate .= '$("input#ImportBtn", window.parent.document).removeClass("formbutton_disable").addClass("formbutton");';
			$thisJSUpdate .= '$("input#ImportBtn", window.parent.document).attr("disabled","");';
		}
		
		$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
}
else if ($task == 'importSubjectTopic') {
	$jsInclude = '';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
	echo $jsInclude;
	
	### Get the Temp Data To be Insert
	$importData = $lreportcard_subjectTopic->getImportTempData();
	$numOfData = count($importData);
	
	for ($i=0; $i<$numOfData; $i++) {
		$_yearId = $importData[$i]['YearID'];
		$_subjectId = $importData[$i]['SubjectID'];
		$_subjectCmpId = $importData[$i]['SubjectCmpID'];
		$_subjectTopicId = $importData[$i]['SubjectTopicID'];
		$_subjectTopicCode = $importData[$i]['SubjectTopicCode'];
		$_subjectTopicNameEn = $importData[$i]['SubjectTopicNameEn'];
		$_subjectTopicNameCh = $importData[$i]['SubjectTopicNameCh'];
        // [2020-0915-1830-00164]
        $_subjectTopicTermSeq = $importData[$i]['TargetTermSeq'];
		
		$_targetSubjectId = ($_subjectCmpId > 0)? $_subjectCmpId : $_subjectId;
		if ($_subjectTopicId == 0) {
			$_subjectTopicId = '';
		}
		
		$_subjectTopicId = $lreportcard_subjectTopic->saveSubjectTopic($_yearId, $_targetSubjectId, $_subjectTopicId, $_subjectTopicCode, $_subjectTopicNameEn, $_subjectTopicNameCh, $_subjectTopicTermSeq);

		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.document.getElementById("BlockUISpan").innerHTML = "'.($i + 1).'";';
			$thisJSUpdate .= 'window.parent.document.getElementById("NumOfProcessedPageSpan").innerHTML = "'.($i + 1).'";';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
	}
	
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
	
	$lreportcard_subjectTopic->deleteImportTempData();
}

intranet_closedb();
?>