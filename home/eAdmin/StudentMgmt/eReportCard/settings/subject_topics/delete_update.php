<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_subjectTopic.php");

$lreportcard = new libreportcardcustom();
$lreportcard_subjectTopic = new libreportcard_subjectTopic();

$lreportcard->hasAccessRight();

$formId = standardizeFormPostValue($_POST['formId']);
$subjectId = standardizeFormPostValue($_POST['subjectId']);
$subjectTopicIdAry = $_POST['subjectTopicIdAry'];

$success = $lreportcard_subjectTopic->deleteSubjectTopic($subjectTopicIdAry);

if ($success) {
	$returnMsgKey = 'DeleteSuccess';
}
else {
	$returnMsgKey = 'DeleteUnsuccess';
}

intranet_closedb();

	
header('location: index.php?&returnMsgKey='.$returnMsgKey);
?>