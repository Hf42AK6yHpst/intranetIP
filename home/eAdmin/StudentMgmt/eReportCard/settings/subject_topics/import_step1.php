<?php
// Modifying by Bill

/********************************************************
 * Modification log
 * 20201103 Bill    [2020-0915-1830-00164]
 *      - support term setting import   ($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings'])
 * ******************************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_subjectTopic.php");

$lreportcard = new libreportcardcustom();
$lreportcard_subjectTopic = new libreportcard_subjectTopic();
$linterface = new interface_html();

$lreportcard->hasAccessRight();


### Get parameter
$returnMsgKey = $_GET['returnMsgKey'];

	
$CurrentPage = "Settings_SubjectTopics";	
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($eReportCard['Settings_SubjectTopics']);
$ReturnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);


### navigation
// $navigationAry[] = array($displayLang, $onclickJs);
$navigationAry[] = array($eReportCard['Settings_SubjectTopics'], 'javascript: goBack();');
$navigationAry[] = array($Lang['Btn']['Import']);
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($navigationAry);


### steps
$htmlAry['steps'] = $linterface->GET_IMPORT_STEPS($CurrStep=1);


# column property display
$csvHeaderAry = $lreportcard_subjectTopic->getCsvHeader();
$columnTitleAry = Get_Lang_Selection($csvHeaderAry['Ch'], $csvHeaderAry['En']);
$columnPropertyAry = $lreportcard_subjectTopic->getCsvHeaderProperty();
$RemarksArr = array();
$RemarksArr[0] = '<a id="remarkBtn_form" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer(\'form\');">['.$Lang['General']['Remark'].']</a>';
$RemarksArr[1] = '<a id="remarkBtn_subject" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer(\'subject\');">['.$Lang['General']['Remark'].']</a>';
$RemarksArr[3] = '<a id="remarkBtn_subjectComponent" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer(\'subjectComponent\');">['.$Lang['General']['Remark'].']</a>';
// [2020-0915-1830-00164]
if($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings']) {
    $RemarksArr[8] = '<a id="remarkBtn_term" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer(\'term\');">['.$Lang['General']['Remark'].']</a>';
}
$htmlAry['columnRemarks'] = $linterface->Get_Import_Page_Column_Display($columnTitleAry, $columnPropertyAry, $RemarksArr);


### form table
$x = '';
$x .= '<table id="html_body_frame" width="100%">'."\n";
	$x .= '<tr>'."\n";
		$x .= '<td>'."\n";
			$x .= '<table class="form_table_v30">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['General']['SourceFile'].' <span class="tabletextremark">'.$Lang['General']['CSVFileFormat'].'</span></td>'."\n";
					$x .= '<td class="tabletext"><input class="file" type="file" name="csvfile" id="csvfile"></td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['General']['CSVSample'].'</td>'."\n";
					$x .= '<td><a class="tablelink" href="javascript:goDownloadSample();">'. $Lang['General']['ClickHereToDownloadSample'] .'</a></td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'."\n";
						$x .= $Lang['General']['ImportArr']['DataColumn']."\n";
					$x .= '</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $htmlAry['columnRemarks']."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="tabletextremark" colspan="2">'."\n";
						$x .= $linterface->MandatoryField();
						$x .= $linterface->ReferenceField();
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
$x .= '</table>'."\n";
$htmlAry['formTable'] = $x;


# Form Remarks Layer
$formAry = $lreportcard->GET_ALL_FORMS($hasTemplateOnly=false, $excludeWithoutClass=false);
$numOfForm = count($formAry);
$thisRemarksType = 'form';
$x = '';
$x .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:400px;">'."\r\n";
	$x .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
		$x .= '<tbody>'."\r\n";
			$x .= '<tr>'."\r\n";
				$x .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
					$x .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
				$x .= '</td>'."\r\n";
			$x .= '</tr>'."\r\n";
			$x .= '<tr>'."\r\n";
				$x .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
					$x .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
						$x .= '<thead>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<th>'.$columnTitleAry[0].'</th>'."\n";
							$x .= '</tr>'."\n";
						$x .= '</thead>'."\n";
						$x .= '<tbody>'."\n";
							if ($numOfForm == 0) {
								$x .= '<tr><td colspan="2" align="center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
							}
							else {
								for ($i=0; $i<$numOfForm; $i++) {
									$_levelName = $formAry[$i]['LevelName'];
									
									$x .= '<tr>'."\n";
										$x .= '<td>'.$_levelName.'</td>'."\n";
									$x .= '</tr>'."\n";
								}
							}
						$x .= '</tbody>'."\n";
					$x .= '</table>'."\r\n";
				$x .= '</td>'."\r\n";
			$x .= '</tr>'."\r\n";
		$x .= '</tbody>'."\r\n";
	$x .= '</table>'."\r\n";
$x .= '</div>'."\r\n";
$htmlAry['formRemarksDiv'] = $x;


# Subject Remarks Layer
$subjectAssoAry = $lreportcard->GET_SUBJECTS_CODEID_MAP($withComponent=1, $mapByCode=0);
$numOfSubject = count($subjectAssoAry);
$thisRemarksType = 'subject';
$x = '';
$x .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:400px;">'."\r\n";
	$x .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
		$x .= '<tbody>'."\r\n";
			$x .= '<tr>'."\r\n";
				$x .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
					$x .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
				$x .= '</td>'."\r\n";
			$x .= '</tr>'."\r\n";
			$x .= '<tr>'."\r\n";
				$x .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
					$x .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
						$x .= '<thead>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<th>'.$Lang['General']['InputCode'].'</th>'."\n";
								$x .= '<th>'.$columnTitleAry[2].'</th>'."\n";
							$x .= '</tr>'."\n";
						$x .= '</thead>'."\n";
						$x .= '<tbody>'."\n";
							if ($numOfSubject == 0) {
								$x .= '<tr><td colspan="2" align="center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
							}
							else {
								foreach ((array)$subjectAssoAry as $_subjectId => $_subjectCodeInfoAry) {
									$_subjectCode = $_subjectCodeInfoAry['CODEID'];
									$_subjectCmpCode = $_subjectCodeInfoAry['CMP_CODEID'];
									
									$_targetCode = $_subjectCode;
									if ($_subjectCmpCode != '') {
										continue;
									}
									
									$_subjectName = $lreportcard->GET_SUBJECT_NAME_LANG($_subjectId);
									
									$x .= '<tr>'."\n";
										$x .= '<td>'.$_targetCode.'</td>'."\n";
										$x .= '<td>'.$_subjectName.'</td>'."\n";
									$x .= '</tr>'."\n";
								}
							}
						$x .= '</tbody>'."\n";
					$x .= '</table>'."\r\n";
				$x .= '</td>'."\r\n";
			$x .= '</tr>'."\r\n";
		$x .= '</tbody>'."\r\n";
	$x .= '</table>'."\r\n";
$x .= '</div>'."\r\n";
$htmlAry['subjectRemarksDiv'] = $x;


# Subject Component Remarks Layer
$subjectCodeAssoAry = $lreportcard->GET_SUBJECTS_CODEID_MAP($withComponent=1, $mapByCode=1);
$thisRemarksType = 'subjectComponent';
$x = '';
$x .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:600px;">'."\r\n";
	$x .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
		$x .= '<tbody>'."\r\n";
			$x .= '<tr>'."\r\n";
				$x .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
					$x .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
				$x .= '</td>'."\r\n";
			$x .= '</tr>'."\r\n";
			$x .= '<tr>'."\r\n";
				$x .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
					$x .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
						$x .= '<thead>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<th>'.$Lang['General']['InputCode'].'</th>'."\n";
								$x .= '<th>'.$columnTitleAry[2].'</th>'."\n";
								$x .= '<th>'.$columnTitleAry[4].'</th>'."\n";
							$x .= '</tr>'."\n";
						$x .= '</thead>'."\n";
						$x .= '<tbody>'."\n";
							if ($numOfSubject == 0) {
								$x .= '<tr><td colspan="2" align="center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
							}
							else {
								foreach ((array)$subjectAssoAry as $_subjectId => $_subjectCodeInfoAry) {
									$_subjectCode = $_subjectCodeInfoAry['CODEID'];
									$_subjectCmpCode = $_subjectCodeInfoAry['CMP_CODEID'];
									
									$_targetCode = $_subjectCmpCode;
									if ($_subjectCmpCode == '') {
										continue;
									}
									
									$_parentSubjectId = $subjectCodeAssoAry[$_subjectCode.'_'];
									$_parentSubjectName = $lreportcard->GET_SUBJECT_NAME_LANG($_parentSubjectId);
									$_componentSubjectName = $lreportcard->GET_SUBJECT_NAME_LANG($_subjectId);
									
									$x .= '<tr>'."\n";
										$x .= '<td>'.$_targetCode.'</td>'."\n";
										$x .= '<td>'.$_parentSubjectName.'</td>'."\n";
										$x .= '<td>'.$_componentSubjectName.'</td>'."\n";
									$x .= '</tr>'."\n";
								}
							}
						$x .= '</tbody>'."\n";
					$x .= '</table>'."\r\n";
				$x .= '</td>'."\r\n";
			$x .= '</tr>'."\r\n";
		$x .= '</tbody>'."\r\n";
	$x .= '</table>'."\r\n";
$x .= '</div>'."\r\n";
$htmlAry['subjectComponentRemarksDiv'] = $x;


# Term Remarks Layer
$thisRemarksType = 'term';
$x = '';
// [2020-0915-1830-00164]
if($eRCTemplateSetting['Settings']['SubjectTopics_TermSettings'])
{
    $x .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:400px;">'."\r\n";
        $x .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
            $x .= '<tbody>'."\r\n";
                $x .= '<tr>'."\r\n";
                    $x .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
                        $x .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
                    $x .= '</td>'."\r\n";
                $x .= '</tr>'."\r\n";
                $x .= '<tr>'."\r\n";
                    $x .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
                        $x .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
                            $x .= '<thead>'."\n";
                                $x .= '<tr>'."\n";
                                    $x .= '<th>'.$Lang['General']['InputCode'].'</th>'."\n";
                                    $x .= '<th>'.$Lang['General']['Term'].'</th>'."\n";
                                $x .= '</tr>'."\n";
                            $x .= '</thead>'."\n";
                            $x .= '<tbody>'."\n";
                                foreach((array)$lreportcard_subjectTopic->subjectTopicRelatedTermArr as $thisTermSeq => $thisTermName) {
                                    $thisTermNameDisplay = $thisTermSeq ? $thisTermName : $eReportCard['WholeYear'];
                                    $x .= '<tr>'."\n";
                                        $x .= '<td>'.$thisTermName.'</td>'."\n";
                                        $x .= '<td>'.$thisTermNameDisplay.'</td>'."\n";
                                    $x .= '</tr>'."\n";
                                }
                            $x .= '</tbody>'."\n";
                        $x .= '</table>'."\r\n";
                    $x .= '</td>'."\r\n";
                $x .= '</tr>'."\r\n";
            $x .= '</tbody>'."\r\n";
        $x .= '</table>'."\r\n";
    $x .= '</div>'."\r\n";
}
$htmlAry['termRemarksDiv'] = $x;


### buttons
$htmlAry['continueBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Continue'], "button", "goSubmit()", 'sumbitBtn');
$htmlAry['backBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');


### hidden fields
$htmlAry['exportModeHiddenField'] = $linterface->GET_HIDDEN_INPUT('exportMode', 'exportMode', 4);
?>
<script type="text/javascript">
$(document).ready( function() {
	
});

function goBack() {
	window.location = 'index.php';
}

function goSubmit() {
	$('form#form1').attr('action', 'import_step2.php').submit();
}

function goDownloadSample() {
	$('form#form1').attr('action', 'export.php').submit();
}

function getRemarkDivIdByRemarkType(remarkType) {
	return 'remarkDiv_' + remarkType;
}

function showRemarkLayer(remarkType) {
	var remarkBtnId = 'remarkBtn_' + remarkType;
	var remarkDivId = getRemarkDivIdByRemarkType(remarkType);
	var leftAdjustment = $('#' + remarkBtnId).width();
	var topAdjustment = 0;
	
	changeLayerPosition(remarkBtnId, remarkDivId, leftAdjustment, topAdjustment);
	hideAllRemarkLayer();
	MM_showHideLayers(remarkDivId, '', 'show');
}

function hideRemarkLayer(remarkType) {
	var remarkDivId = getRemarkDivIdByRemarkType(remarkType);
	MM_showHideLayers(remarkDivId, '', 'hide');
}

function hideAllRemarkLayer() {
	$('div.selectbox_layer').each( function() {
		MM_showHideLayers($(this).attr('id'), '', 'hide');
	});
}
</script>
<form name="form1" id="form1" method="POST" enctype="multipart/form-data">
	<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	
	<?=$htmlAry['steps']?>
	
	<div class="table_board">
		<?=$htmlAry['formTable']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['continueBtn']?>
		<?=$htmlAry['backBtn']?>
		<p class="spacer"></p>
	</div>
	
	<?=$htmlAry['formRemarksDiv']?>
	<?=$htmlAry['subjectRemarksDiv']?>
	<?=$htmlAry['subjectComponentRemarksDiv']?>
    <?=$htmlAry['termRemarksDiv']?>
	
	<?=$htmlAry['exportModeHiddenField']?>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>