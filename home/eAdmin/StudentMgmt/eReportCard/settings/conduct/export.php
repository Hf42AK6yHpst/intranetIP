<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_extrainfo.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	if ($lreportcard->hasAccessRight()) {

		$lreportcard_extrainfo = new libreportcard_extrainfo();
		$lexport = new libexporttext();
		
		$exportColumn = array("Conduct");
		
		$ConductArr = $lreportcard_extrainfo->Get_Conduct();
		for($i=0; $i<sizeof($ConductArr); $i++)
			$ConductExportArr[] = array($ConductArr[$i]["Conduct"]);
		
		$export_content = $lexport->GET_EXPORT_TXT($ConductExportArr, $exportColumn, "", "\r\n", "", 0, "11");
		
		intranet_closedb();
		$filename = "conduct.csv";
		$lexport->EXPORT_FILE($filename, $export_content);
	} else {
		echo "You have no priviledge to access this page.";
	}
} else {
	echo "You have no priviledge to access this page.";
}
?>