<?php
// Modifying by: Bill

/***************************************************
 * 	Modification log:
 * 20160630 Bill:	[2015-1104-1130-08164]
 * 		- Correct highlighted page in module menu
 * *************************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	$CurrentPage = "Settings_Conduct";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
		
	if ($lreportcard->hasAccessRight()) {

		$linterface = new interface_html();
		$lreportcard_ui = new libreportcard_ui();
		
		# tag information
		$TAGS_OBJ = array();
		$TAGS_OBJ[] = array($eReportCard['Conduct']);
		
		$linterface->LAYOUT_START();

		echo $lreportcard_ui->Get_Setting_Conduct_Edit_UI($ConductID[0]);
		
?>
<script type="text/JavaScript" language="JavaScript">

var AjaxChecking = false;
var ConductExist = false;
function js_Check_Conduct_Exist()
{
	var Conduct = $("input#Conduct").val();
	var ConductID = $("input#ConductID").val();
	$.post(
		"ajax_reload.php",
		{
			Task:"Check_Conduct_Exist",
			Conduct:Conduct,
			ConductID:ConductID
		},
		function (ReturnData)
		{
			if(ReturnData==1)
			{
				$("div#WarnExistConduct").show();
				ConductExist = true;
			}
			else
			{
				$("div#WarnExistConduct").hide();
				ConductExist = false;
			}
			AjaxChecking = false;
		}
	)
}

function js_Check_Form()
{
	if(AjaxChecking) return false;
	
	var Conduct = $("input#Conduct").val();
	var FormValid = true;

	if(Conduct.Trim()=='')
	{
		return false
		$("div#WarnEmptyCoduct").show();
	}
	
	if(ConductExist)
	{
		return false
	}
	
}
</script>
<?
	print $linterface->FOCUS_ON_LOAD("form1.code");
  	$linterface->LAYOUT_STOP();
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>
