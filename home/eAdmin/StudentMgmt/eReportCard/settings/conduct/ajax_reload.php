<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
$lreportcard = new libreportcard();

if (!$lreportcard->hasAccessRight()) {
	intranet_closedb();
	exit;
}

switch($Task)
{
	case "Check_Conduct_Exist":
		include_once($PATH_WRT_ROOT."includes/libreportcard2008_extrainfo.php");
		$lreportcard_extrainfo = new libreportcard_extrainfo();
		
		$Conduct = trim($Conduct);
		
		echo $lreportcard_extrainfo->Is_Conduct_Exist($Conduct, $ConductID);
	break;
}

intranet_closedb();
?>

