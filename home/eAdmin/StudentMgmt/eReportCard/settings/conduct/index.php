<?php

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

$arrCookies = array();
$arrCookies[] = array("ck_conduct_page_size", "numPerPage");
$arrCookies[] = array("ck_conduct_page_number", "pageNo");
$arrCookies[] = array("ck_conduct_page_order", "order");
$arrCookies[] = array("ck_conduct_page_field", "field");	
updateGetCookies($arrCookies);

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	$lreportcard_ui = new libreportcard_ui();
	
	$CurrentPage = "Settings_Conduct";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    if ($lreportcard->hasAccessRight())
    {
        $linterface = new interface_html();

		# tag information
		$TAGS_OBJ = array();
		$TAGS_OBJ[] = array($eReportCard['Conduct']);
		
		$ReturnMsg = $Lang['General']['ReturnMessage'][$Msg];
		$linterface->LAYOUT_START($ReturnMsg);

		echo $lreportcard_ui->Get_Setting_Conduct_Index_UI($page, $order, $field);

?>
<script>
function js_New()
{
	window.location="edit.php";
}

function js_Import()
{
	window.location="import.php";
}

function js_Export()
{
	window.location="export.php";
}

</script>
<script language="JavaScript" src="/templates/jquery/jquery.CheckboxBatchSelect.js"></script>
<script>
$("input:checkbox.ConductChkbox").CheckboxBatchSelect();
</script>
<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>