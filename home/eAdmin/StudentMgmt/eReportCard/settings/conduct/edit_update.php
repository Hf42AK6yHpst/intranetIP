<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

$PageRight = "ADMIN";

if (!$plugin['ReportCard2008']) {
	intranet_closedb();
	header("Location:/index.php");
}

if (!isset($_POST))
	header("Location:index.php");

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_extrainfo.php");
$lreportcard = new libreportcard();
$lreportcard_extrainfo = new libreportcard_extrainfo();

if (!$lreportcard->hasAccessRight()) {
	intranet_closedb();
	header("Location:/index.php");
}

$Conduct = trim(stripslashes($Conduct));

if(trim($ConductID)!='')
{
	$success = $lreportcard_extrainfo->Update_Conduct($Conduct, $ConductID);
	$Msg = $success?"UpdateSuccess":"UpdateUnsuccess";
}
else
{
	$success = $lreportcard_extrainfo->Insert_Conduct($Conduct);
	$Msg = $success?"AddSuccess":"AddUnsuccess";
}

intranet_closedb();

header("Location:index.php?Msg=$Msg");
?>

