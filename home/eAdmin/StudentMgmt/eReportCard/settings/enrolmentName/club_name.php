<?php
/* //using: anna
 *  Log: 
 * 	Date: 2018-06-12 Anna
 *        Created this page
 * 
 * 
 */
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");


intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libenroll = new libclubsenrol();

$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();


$CurrentPage = "Settings_ChangeEnrolName";
// $CurrentPageArr['eEnrolment'] = 1;
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

$Semester = isset($Semester) && $Semester != ''? $Semester : 0;
$Semester = IntegerSafe($Semester);

$yearFilter = getSelectAcademicYear("AcademicYearID", 'onChange="document.form1.action=\'club_name.php\';document.form1.submit();"', 1, 0, $AcademicYearID);
$SemesterFilter = $libenroll->Get_Term_Selection('Semester', $AcademicYearID, $Semester, $OnChange="document.form1.action='club_name.php'; document.form1.submit()", $NoFirst=1, $NoPastTerm=0, $withWholeYear=1);


$ClubNameAry = array();
$EnrolGroupIDAry = array();
$ClubInfoAry = $libenroll->getGroupsForEnrolSetByConditions($AcademicYearID,'','',$Semester);
foreach ($ClubInfoAry as $ClubInfo){
    $ClubNameAry[] = Get_Lang_Selection($ClubInfo['TitleChinese'],$ClubInfo['Title']);
    $EnrolGroupIDAry[] = $ClubInfo['EnrolGroupID'];
}

$thisYearClubNum = count($ClubInfoAry);


$TAGS_OBJ[] = array($Lang['eEnrolment']['NameSettings']['ClubNameSetting'], "", 1);
$TAGS_OBJ[] = array($Lang['eEnrolment']['NameSettings']['ActivityNameSetting'], "activity_name.php", 0);

$returnMsg = $Lang['General']['ReturnMessage'][$xmsg];
$linterface->LAYOUT_START($returnMsg);
echo $linterface->Include_JS_CSS();	
?>

<script language="javascript">
function editForm(){
	$('#EditBtn').hide();
	$('#UpdateBtn').show();
	$('#cancelbtn').show();
	$('.club_text').hide();
	$('.club_editable').show();	
}
function ResetInfo(){
	document.form1.reset();	
	$('#EditBtn').show();
	$('#UpdateBtn').hide();
	$('#cancelbtn').hide();
	$('.club_text').show();
	$('.club_editable').hide();	
}
</script>

<form id="form1" name="form1" method="post" action="edit_update.php">
    <div class="table_board">
        <table id="html_body_frame" width="100%">
            <tr>
                <td valign="bottom">
                    <div class="table_filter">
                    <?php echo $yearFilter;?>
                    <?php echo $SemesterFilter;?>
                    </div>
                </td>
            </tr>
            <tr>
            	<td>
            		<div class="table_row_tool row_content_tool">
            		<?php echo  $linterface->GET_SMALL_BTN($button_edit, "button","javascript:editForm();","EditBtn");?>
            		</div>
            		<table id="ContentTable" class="common_table_list_v30">
            			<thead>
            				<tr>
            					<th>#</th>
            					<th><?php echo $Lang['eEnrolment']['NameSettings']['CurrentName'];?></th>
            					<th><?php echo $Lang['eEnrolment']['NameSettings']['DisplayName'];?></th>
            				</tr>
            			</thead>
            			<tbody>
            			<?php for ($i=0;$i<$thisYearClubNum;$i++){?>
            			<?php    $_clubName = $ClubNameAry[$i];
            				     $_enrolGroupID = $EnrolGroupIDAry[$i];
            				     $_displayNameAry = $libenroll->get_Club_DisplayName($_enrolGroupID);
            				     $_displayName = $_displayNameAry == ''? $_clubName:$_displayNameAry;
            				
            				?>
            				<tr>
            					
            					<td><?php echo ($i+1);?></td>
            					<td ><?php echo ($_clubName)?> <input type="hidden" name="EnrolGroupIDAry[]" value="<?php echo $_enrolGroupID;?>"></td>
            					<td class="club_text"><?php echo $_displayName;?></td>
            					<td class="club_editable" style="display:none;">
            					<input type="text" value="<?php echo $_displayName;?>" name="ClubNameAry[]"></td>
            					
            				</tr>
            			<?php }?>
            			</tbody>
            		</table>
            		
            	</td>
            </tr>

        </table>
    </div>
    <div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?php  echo $linterface->GET_ACTION_BTN($button_update, "submit","","UpdateBtn", "style='display: none'");?>
		<?php echo $linterface->GET_ACTION_BTN($button_cancel, "button", "ResetInfo();","cancelbtn", "style='display: none'"); ?>
		<p class="spacer"></p>
	</div>
    <input name="changeName" type="hidden" value="clubName">
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>