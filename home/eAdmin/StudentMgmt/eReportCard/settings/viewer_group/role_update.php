<?
// Using :

##########################
#
#   Date:   2020-04-28  Bill    [2019-0924-1029-57289]
#           - Create File    ($eRCTemplateSetting['ViewPageAssignedOnly'])
#
##########################

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

// Check access right
$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();
if(!$sys_custom['eRC']['Settings']['ViewGroupUserType'] || !$eRCTemplateSetting['ViewPageAssignedOnly']) {
    No_Access_Right_Pop_Up();
}

// Remove old assigned pages
$StudentID = IntegerSafe($_POST['StudentID']);
$result = $lreportcard->Delete_VIEW_GROUP_USER_ASSIGNED_PAGE(array($StudentID));

// Update assigned pages
if($result && !empty($assignedPageArr)) {
    $result = $lreportcard->Add_VIEW_GROUP_USER_ASSIGNED_PAGE(array($StudentID), $assignedPageArr);
}

intranet_closedb();

if(sizeof($result) && in_array(false, (array)$result)) {
    $msg = "AddUnsuccess";
} else {
    $msg = "AddSuccess";
}

header("Location:index.php?msg=$msg");
?>