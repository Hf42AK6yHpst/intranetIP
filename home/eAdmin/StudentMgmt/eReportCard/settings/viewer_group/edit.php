<?php
// Using :

##########################
#
#   Date:   2020-04-28  Bill    [2019-0924-1029-57289]
#           - Added assign page settings    ($eRCTemplateSetting['ViewPageAssignedOnly'])
#
#	Date: 	2017-09-22	Bill	[2017-0502-1024-21096]
#			- Create file
#
##########################

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

// Check access right
$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();
if(!$sys_custom['eRC']['Settings']['ViewGroupUserType']) {
	No_Access_Right_Pop_Up();
}

$linterface = new interface_html();
$lreportcard_ui = new libreportcard_ui();

// Get View Group Users
$ViewGroupUsers = $lreportcard->GET_ALL_VIEW_GROUP_USER();
$ViewGroupUserIDs = Get_Array_By_Key((array)$ViewGroupUsers, "UserID");

$name_field = getNameFieldByLang("USR.");	
if(sizeof($ViewGroupUserIDs) > 0) {
	$conds = " AND USR.UserID NOT IN (".implode(',', $ViewGroupUserIDs).")";
}

// Get In-Group Users
$sql = "SELECT 
			USR.UserID, 
			$name_field as name, 
			USR.UserLogin 
		FROM 
			INTRANET_USER USR 
		WHERE 
			USR.RecordType = 1 
			$conds 
		GROUP BY 
			USR.UserID 
		ORDER BY 
			UserLogin";
$result = $lreportcard->returnArray($sql,5);

// Get User Selection & auto fill-in data
$data_ary = array();
for ($j=0; $j<sizeof($result); $j++)  {
	list($this_userid, $this_username, $this_userlogin) = $result[$j];
	$data_ary[] = array($this_userid, $this_username, $this_userlogin);
}
if(!empty($data_ary)) 
{
	# Define YUI array (Search by input format)
	for($i=0; $i<sizeof($data_ary); $i++) {
		list($this_userid, $this_username, $this_userlogin) = $data_ary[$i];
		$liArr .= "[\"". addslashes(intranet_undo_htmlspecialchars($this_username)) ."\", \"". addslashes(intranet_undo_htmlspecialchars($this_username)) ."\", \"". $this_userid ."\"]";
		($i == (sizeof($result)-1)) ? $liArr .= "" : $liArr .= ",\n";
	}
	
	foreach ($data_ary as $key => $row)  {
		$field1[$key] = $row[0];	// user id
		$field2[$key] = $row[1];	// user name
		$field3[$key] = $row[2];	// user login
	}
	array_multisort($field3, SORT_ASC, $data_ary);
	
	# Define YUI array (Search by UserLogin)
	for($i=0; $i<sizeof($data_ary); $i++) {
		list($this_userid, $this_username, $this_userlogin) = $data_ary[$i];
		$liArr2 .= "[\"". $this_userlogin ."\", \"". addslashes(intranet_undo_htmlspecialchars($this_username)) ."\", \"". $this_userid ."\"]";
		($i == (sizeof($result)-1)) ? $liArr2 .= "" : $liArr2 .= ",\n";
	}
}
$staff_selected = $linterface->GET_SELECTION_BOX(array(), "name='staff[]' id='staff[]' class='select_stafflist' size='15' multiple='multiple'", "");
$button_remove_html = $linterface->GET_BTN($button_remove, "button", "javascript:checkOptionRemove(document.getElementById('staff[]'))");

// [2019-0924-1029-57289] Available pages for assign
$selectPageCheckbox = array();
if($eRCTemplateSetting['ViewPageAssignedOnly'] && $eRCTemplateSetting['ViewPageAssignedArr'] && !empty($eRCTemplateSetting['ViewPageAssignedArr'])) {
    foreach((array)$eRCTemplateSetting['ViewPageAssignedArr'] as $thisPageAssigned) {
        $selectPageCheckbox[] = '<input type="checkbox" name="assignedPageArr[]" id="assignedPage_'.$thisPageAssigned.'" value="'.$thisPageAssigned.'" /> <label for="assignedPage_'.$thisPageAssigned.'">'.$eReportCard[$thisPageAssigned].'</label>';
    }
}

// Page
$CurrentPage = "Settings_ViewGroupUser";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

// Tag
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($eReportCard["Settings_ViewGroupUser"]);

# Start layout
$linterface->LAYOUT_START();
?>

<br />

<style type="text/css">
    #statesmod {position:relative;}
    #statesautocomplete {position:relative;width:22em;margin-bottom:1em;}/* set width of widget here*/
    #statesautocomplete {z-index:9000} /* for IE z-index of absolute divs inside relative divs issue */
    #statesinput {_position:absolute;width:100%;height:1.4em;z-index:0;} /* abs for ie quirks */
    #statescontainer, #statescontainerCC, #statescontainerBCC {position:absolute;top:0.3em;width:100%}
    #statescontainer .yui-ac-content, #statescontainerCC .yui-ac-content, #statescontainerBCC .yui-ac-content {position:absolute;width:100%;border:1px solid #404040;background:#eeeeee;overflow:hidden;z-index:9050;}
    #statescontainer .yui-ac-shadow, #statescontainerCC .yui-ac-shadow, #statescontainerBCC .yui-ac-shadow {position:absolute;margin:.3em;width:100%;background:#a0a0a0;z-index:9049;}
    #statescontainer ul, #statescontainerCC ul, #statescontainerBCC ul {padding:5px 0;width:100%;}
    #statescontainer li, #statescontainerCC li, #statescontainerBCC li {padding:0 5px;cursor:default;white-space:nowrap;}
    #statescontainer li.yui-ac-highlight, #statescontainerCC li.yui-ac-highlight, #statescontainerBCC li.yui-ac-highlight {background:#bbbbbb;}
    #statescontainer li.yui-ac-prehighlight, #statescontainerCC li.yui-ac-prehighlight, #statescontainerBCC li.yui-ac-prehighlight {background:#FFFFFF;}

	#statesmod div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,form,fieldset,input,textarea,p,blockquote{margin:0;padding:0;}
	#statesmod table{border-collapse:collapse;border-spacing:0;}
	#statesmod fieldset,img{border:0;}
	#statesmod address,caption,cite,code,dfn,em,strong,th,var{font-style:normal;font-weight:normal;}
	#statesmod ol,ul {list-style:none;}
	#statesmod caption,th {text-align:left;}
	#statesmod h1,h2,h3,h4,h5,h6{font-size:100%;font-weight:normal;}
	#statesmod q:before,q:after{content:'';}
	#statesmod abbr,acronym {border:0;}
	#statesmod {font:13px arial,helvetica,clean,sans-serif;*font-size:small;*font:x-small;}
</style>

<!-- In-memory JS array begins-->
<!-- Libary begins -->
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/yui/autocomplete_files/yahoo.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/yui/autocomplete_files/dom.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/yui/autocomplete_files/event-debug.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/yui/autocomplete_files/animation.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/yui/autocomplete_files/autocomplete-debug-ip20.js"></script>
<!-- Library ends -->

<script language="javascript">
var statesArray = [
    <?= $liArr?>
];
var loginidArray = [
    <?= $liArr2?>
];
var delimArray = [
    ";"
];

function checkForm() {
    obj = document.form1;
	if(obj.elements["staff[]"].length != 0) {
    	checkOptionAll(obj.elements["staff[]"]);
    	return true;
	}
    else {
        alert('<?=$i_Discipline_System_alert_PleaseSelectMember?>');
        return false;
    }
}

function checkCR(evt) {
	var evt  = (evt) ? evt : ((event) ? event : null);
	var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
	if ((evt.keyCode == 13) && (node.type=="text")) {return false;}	
}
document.onkeypress = checkCR;

function goBack() {
	self.location.href = "index.php";	
}
</script>

<form name="form1" method="POST" onsubmit="return checkForm();" action="update.php">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr><td align="center">
			<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr valign="top">
					<td width="40%"><span class="tabletext"><?=$i_Discipline_System_Group_Right_Navigation_Choose_Member?></span></td>
					<td width="10" nowrap="nowrap"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="10"></td>
					<td width="60%" nowrap="nowrap"><span class="tabletext"><?=$i_Discipline_System_Group_Right_Navigation_Selected_Member2?></span></td>
				</tr>
				<tr>
					<td valign="top" class="tablerow2">
						<table width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr>
								<td class="tabletext"><?=$eDiscipline["SelectTeacherStaff"]?> </td>
							</tr>
							<tr>
							<td class="tabletext"><?= $linterface->GET_BTN($button_select, "button", "javascript:newWindow('/home/common_choose/index.php?fieldname=staff[]&page_title=SelectMembers&permitted_type=1', 16)")?></td>
								</tr>
							<tr>
								<td class="tabletext"><i><?=$i_general_or?></i></td>
							</tr>
							<tr>
								<td class="tabletext"><?=$i_general_search_by_loginid?><br\>
									<div id="statesautocomplete">
										<input type="text" class="tabletext" name="search2" id="search2">
										<div id="statescontainerCC" style="left:142px; top:0px;">
											<div style="display: none; width: 199px; height: 0px;" class="yui-ac-content">
												<div style="display: none;" class="yui-ac-hd"></div>
												<div class="yui-ac-bd"></div>
												<div style="display: none;" class="yui-ac-ft"></div>
											</div>
											<div style="width: 0pt; height: 0pt;" class="yui-ac-shadow"></div>
										</div>
									</div>
								</td>
							</tr>
						</table>
					</td>
					<td valign="top" nowrap="nowrap">&nbsp;</td>
					<td valign="top" nowrap="nowrap">
						<table width="100%" border="0" cellpadding="3" cellspacing="0">
						<tr>
							<td><?=$staff_selected?>
							</td>
						</tr>
						<tr>
							<td align="right"><?=$button_remove_html?></td>
						</tr>
						</table>
					</td>
				</tr>
			</table>

            <?php if($eRCTemplateSetting['ViewPageAssignedOnly'] && !empty($selectPageCheckbox)) { ?>
                <br />
                <table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                    <tr valign="top">
                        <td width="40%" class="tablerow2"><span class="tabletext"><?=$eReportCard["UserAccessPages"]?></span></td>
                        <td width="10" nowrap="nowrap"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="10"></td>
                        <td width="60%" nowrap="nowrap">
                            <?= implode('<br /><br />', (array)$selectPageCheckbox) ?>
                        </td>
                    </tr>
                </table>
            <?php } ?>

			<div class="edit_bottom_v30">
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit") ?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:goBack()") ?>
			</div>
		</td>
	</tr>
</table>

<input type="hidden" name="flag" value="0" />
</form>

<script type="text/javascript">
YAHOO.example.ACJSArray = function() {
    var oACDS, oACDS2, oAutoComp, oAutoComp2;
    return {
        init: function() {
            // Instantiate first JS Array DataSource
            oACDS2 = new YAHOO.widget.DS_JSArray(loginidArray);
            oAutoComp2 = new YAHOO.widget.AutoComplete('search2','staff[]', 'statescontainerCC', oACDS2);
            oAutoComp2.queryDelay = 0;
            oAutoComp2.prehighlightClassName = "yui-ac-prehighlight";
            oAutoComp2.useShadow = true;
            oAutoComp2.minQueryLength = 0;
        },

        validateForm: function() {
            // Validate form inputs here
            return false;
        }
    };
}();

YAHOO.util.Event.addListener(this,'load',YAHOO.example.ACJSArray.init);
</script>

<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/dpSyntaxHighlighter.js"></script>
<script type="text/javascript">
    dp.SyntaxHighlighter.HighlightAll('code');
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>