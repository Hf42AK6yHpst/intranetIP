<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

intranet_auth();
intranet_opendb();

if (!isset($studentID) || sizeof($studentID) == 0)
	header("Location: index.php?Result=updated_failed");

$YearTermID = $_POST['YearTermID'];

$lreportcard = new libreportcard();
$success = $lreportcard->ADD_EXCLUDE_ORDER_STUDENTS($YearTermID, $studentID);

intranet_closedb();

$Result = ($success==1) ? "update" : "update_failed";
header("Location: index.php?Result=$Result");
?>