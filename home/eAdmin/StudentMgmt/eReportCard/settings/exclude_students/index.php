<?php
// using: ;
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

$linterface = new interface_html();
$lreportcard = new libreportcardcustom();
$lreportcard_ui = new libreportcard_ui();

$lreportcard->hasAccessRight();


### Toolbar
// $btnAry[] = array($btnClass, $onclickJs, $displayLang, $subBtnAry);
$btnAry = array();
$btnAry[] = array('new', 'javascript: goAdd();', $Lang['Btn']['Add']);
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

### Filtering
$ClassLevelID = -1;
if (isset($_POST['ClassLevelID'])) {
	$ClassLevelID = $_POST['ClassLevelID'];
} else if (isset($_GET['ClassLevelID'])) {
	$ClassLevelID = $_GET['ClassLevelID'];
}
$htmlAry['formSelection'] = $lreportcard_ui->Get_Form_Selection('ClassLevelID', $ClassLevelID, 'document.form1.submit();', $noFirst=1, $isAll=1, $hasTemplateOnly=0);

$YearTermID = '';
if (isset($_POST['YearTermID'])) {
	$YearTermID = $_POST['YearTermID'];
} else if (isset($_GET['YearTermID'])) {
	$YearTermID = $_GET['YearTermID'];
}
$htmlAry['termSelection'] = $lreportcard->Get_Term_Selection('YearTermID', $lreportcard->schoolYearID, $YearTermID, 'document.form1.submit();', $NoFirst=0, $NoPastTerm=0, $withWholeYear=1,$isMultiple=0, $isAll=1, $eReportCard['WholeYearReport'], $defaultFirstOption=true);


### DB table action buttons
// $btnAry[] = array($btnClass, $btnHref, $displayLang);
$btnAry = array();
$btnAry[] = array('delete', 'javascript: goDelete();');
$htmlAry['dbTableActionBtn'] = $linterface->Get_DBTable_Action_Button_IP25($btnAry);




### Db table
$conds_ClassLevelID = '';
if ($ClassLevelID == -1){
	// all class level
}
else {
	$conds_ClassLevelID = " And y.YearID = '".$ClassLevelID."' ";
}

$conds_YearTermID = '';
if ($YearTermID != '') {
	if ($YearTermID == 'F') {
		$conds_YearTermID = " AND ers.YearTermID = 0 ";
	}
	else {
		$conds_YearTermID = " AND ers.YearTermID = '".$YearTermID."' ";
	}
} 	
	
$NameField = getNameFieldByLang2('u.');
$ClassNumField = getClassNumberField('ycu.');
$ClassNameField = Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN');
$TermNameField = Get_Lang_Selection('ayt.YearTermNameB5', 'ayt.YearTermNameEN');
$RC_EXCLUDE_RANKING_STUDENT = $lreportcard->DBName.".RC_EXCLUDE_RANKING_STUDENT";
$sql = "SELECT 
				IF (ers.YearTermID = 0, '".$eReportCard['WholeYearReport']."', $TermNameField) as TermName,
				y.YearName as ClassLevel,
				yc.$ClassNameField as ClassName,
				ycu.ClassNumber AS ClassNum,
				$NameField AS Name, 
				CONCAT('<input type=\"checkbox\" name=\"delRecordIDAry[]\" value=\"', ers.RecordID, '\">') as CheckBox,
				IF (ers.YearTermID = 0, '9999-99-99 23:59:59', ayt.TermStart) as TermStartOrder
		FROM 
				$RC_EXCLUDE_RANKING_STUDENT as ers
				Inner Join INTRANET_USER as u ON (ers.StudentID = u.UserID)
				INNER JOIN YEAR_CLASS_USER as ycu ON (u.UserID = ycu.UserID)
				INNER JOIN YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$lreportcard->schoolYearID."')
				INNER JOIN YEAR as y ON (yc.YearID = y.YearID)
				Left Outer Join ACADEMIC_YEAR_TERM as ayt ON (ers.YearTermID = ayt.YearTermID)
		WHERE 
				1
				$conds_ClassLevelID
				$conds_YearTermID
		GROUP BY
				u.UserID, ers.YearTermID
		";

if (isset($ck_page_size) && $ck_page_size != "") {
	$page_size = $ck_page_size;
}
$pageSizeChangeEnabled = true;
$field = ($field=='')? 0 : $field;
$order = ($order=='')? 1 : $order;
$page = ($page=='')? 1 : $page;
	
$li = new libdbtable2007($field, $order, $pageNo);
$li->sql = $sql;
$li->IsColOff = "IP25_table";
$li->field_array = array("TermStartOrder", "ClassLevel", "ClassName", "ClassNum", "Name");
$li->fieldorder2 = ", ClassName, ClassNum";
$li->column_array = array(0, 0, 0, 0);
$li->wrap_array = array(0, 0, 0, 0);
$li->no_col = count($li->field_array) + 1;

	
$pos=0;
$li->column_list .= "<th class='tabletop tabletopnolink' width='1'>#</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='15%'>".$li->column($pos++, $eReportCard['ReportType'])."</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='10%'>".$li->column($pos++, $i_UserClassLevel)."</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='15%'>".$li->column($pos++, $i_ClassName)."</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='10%'>".$li->column($pos++, $i_UserClassNumber)."</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='45%'>".$li->column($pos++, $i_UserName)."</td>\n";
$li->column_list .= "<th width=1 class='tabletoplink' width='1'>".$li->check("delRecordIDAry[]")."</td>\n";
$li->no_col = $pos + 2;

$htmlAry['dataTable'] = $li->display();


############################################################################################################



$CurrentPage = "Settings_ExcludeStudents";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($eReportCard['Settings_ExcludeStudents']);

$linterface->LAYOUT_START();
?>
<script language="javascript">
function goAdd() {
	window.location = "new.php";
}

function goDelete() {
	checkRemove(document.form1,'delRecordIDAry[]','remove.php');
}
</script>

<form name="form1" id="form1" method="POST" action="">
	<?=$htmlAry['subTab']?>
	
	<div class="content_top_tool">
		<?=$htmlAry['contentTool']?>
		<?=$htmlAry['searchBox']?>
		<br style="clear:both;">
	</div>
	
	<div class="table_board">
		<div class="table_filter">
			<?=$htmlAry['termSelection']?>
			<?=$htmlAry['formSelection']?>
		</div>
		<p class="spacer"></p>
		
		<?=$htmlAry['dbTableActionBtn']?>
		<?=$htmlAry['dataTable']?>
	</div>

	<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
	<input type="hidden" name="order" value="<?php echo $li->order;?>">
	<input type="hidden" name="field" value="<?php echo $li->field; ?>">
	<input type="hidden" name="page_size_change" value="">
	<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>