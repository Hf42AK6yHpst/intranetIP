<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	$CurrentPage = "Settings_BasicSettings";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	
	if ($lreportcard->hasAccessRight()) {
		
		# Load calculation settings
		$storDispSettings = $lreportcard->LOAD_SETTING("Storage&Display");
		
		############## Interface Start ##############
		$linterface = new interface_html();

		# tag information
		$TAGS_OBJ[] = array($eReportCard['StyleSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/highlight.php", 0);
		$TAGS_OBJ[] = array($eReportCard['MarkStorageAndDisplay'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/mark_storage_display.php", 1);
		$TAGS_OBJ[] = array($eReportCard['AbsentAndExemptSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/absent_exempt.php", 0);
		$TAGS_OBJ[] = array($eReportCard['CalculationSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/calculation.php", 0);
		$TAGS_OBJ[] = array($eReportCard['AccessSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/access_setting.php", 0);
        if($eRCTemplateSetting['OtherInfo']['CustUploadType_Post']) {
            $TAGS_OBJ[] = array($eReportCard['PostTableHeader'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/post_table_header.php", 0);
        }

		# page navigation (leave the array empty if no need)
		$PAGE_NAVIGATION[] = array();

		$linterface->LAYOUT_START();
?>
<script type="text/JavaScript" language="JavaScript">
function resetForm() {
	document.form1.reset();
	return true;
}
</script>
<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT."templates/2007a/css/"?>ereportcard.css">
<br />
<form name="form1" action="mark_storage_display_update.php" method="post">
<table width="95%" align="center" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>&nbsp;</td>
		<td align="right"> <?=$linterface->GET_SYS_MSG($Result);?></td>
	</tr>
</table>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['AssessmentMark'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="subjectScore" id="subjectScore0" value="0"<?=$storDispSettings["SubjectScore"]=="0"?" checked='true'":""?> />
						<label for="subjectScore0"><?= $eReportCard['Integer'] ?></label><br />
						<input type="radio" name="subjectScore" id="subjectScore1" value="1"<?=$storDispSettings["SubjectScore"]=="1"?" checked='true'":""?> />
						<label for="subjectScore1"><?= $eReportCard['OneDecimalPlace'] ?></label><br />
						<input type="radio" name="subjectScore" id="subjectScore2" value="2"<?=$storDispSettings["SubjectScore"]=="2"?" checked='true'":""?> />
						<label for="subjectScore2"><?= $eReportCard['TwoDecimalPlaces'] ?></label><br />
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['SubjectOverallMark'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="subjectTotal" id="subjectTotal0" value="0"<?=$storDispSettings["SubjectTotal"]=="0"?" checked='true'":""?> />
						<label for="subjectTotal0"><?= $eReportCard['Integer'] ?></label><br />
						<input type="radio" name="subjectTotal" id="subjectTotal1" value="1"<?=$storDispSettings["SubjectTotal"]=="1"?" checked='true'":""?> />
						<label for="subjectTotal1"><?= $eReportCard['OneDecimalPlace'] ?></label><br />
						<input type="radio" name="subjectTotal" id="subjectTotal2" value="2"<?=$storDispSettings["SubjectTotal"]=="2"?" checked='true'":""?> />
						<label for="subjectTotal2"><?= $eReportCard['TwoDecimalPlaces'] ?></label><br />
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['OverallAverage'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="grandAverage" id="grandAverage0" value="0"<?=$storDispSettings["GrandAverage"]=="0"?" checked='true'":""?> />
						<label for="grandAverage0"><?= $eReportCard['Integer'] ?></label><br />
						<input type="radio" name="grandAverage" id="grandAverage1" value="1"<?=$storDispSettings["GrandAverage"]=="1"?" checked='true'":""?> />
						<label for="grandAverage1"><?= $eReportCard['OneDecimalPlace'] ?></label><br />
						<input type="radio" name="grandAverage" id="grandAverage2" value="2"<?=$storDispSettings["GrandAverage"]=="2"?" checked='true'":""?> />
						<label for="grandAverage2"><?= $eReportCard['TwoDecimalPlaces'] ?></label><br />
					</td>
				</tr><tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['OverallTotal'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="grandTotal" id="grandTotal0" value="0"<?=$storDispSettings["GrandTotal"]=="0"?" checked='true'":""?> />
						<label for="grandTotal0"><?= $eReportCard['Integer'] ?></label><br />
						<input type="radio" name="grandTotal" id="grandTotal1" value="1"<?=$storDispSettings["GrandTotal"]=="1"?" checked='true'":""?> />
						<label for="grandTotal1"><?= $eReportCard['OneDecimalPlace'] ?></label><br />
						<input type="radio" name="grandTotal" id="grandTotal2" value="2"<?=$storDispSettings["GrandTotal"]=="2"?" checked='true'":""?> />
						<label for="grandTotal2"><?= $eReportCard['TwoDecimalPlaces'] ?></label><br />
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td class="dotline">
						<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
					</td>
				</tr>
				<tr>
					<td align="center">
						<?= $linterface->GET_ACTION_BTN($button_save, "submit", "")?>&nbsp;
						<?= $linterface->GET_ACTION_BTN($button_reset, "button", "resetForm()")?>&nbsp;
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<?
  	$linterface->LAYOUT_STOP();
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>
