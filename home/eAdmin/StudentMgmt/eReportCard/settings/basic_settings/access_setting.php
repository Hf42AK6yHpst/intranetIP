<?php
//using: 

/****************************************
 *
 * 	Date:		2017-07-10 (Anna)
 * 				add AuthenticationSetting
 * 	
 *	Date:		2013-02-04 (Rita) 
 * 	Details:	add verification contol setting
 * 
 ****************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();

$CurrentPage = "Settings_BasicSettings";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eReportCard['StyleSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/highlight.php", 0);
$TAGS_OBJ[] = array($eReportCard['MarkStorageAndDisplay'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/mark_storage_display.php", 0);
$TAGS_OBJ[] = array($eReportCard['AbsentAndExemptSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/absent_exempt.php", 0);
$TAGS_OBJ[] = array($eReportCard['CalculationSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/calculation.php", 0);
$TAGS_OBJ[] = array($eReportCard['AccessSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/access_setting.php", 1);
if($eRCTemplateSetting['OtherInfo']['CustUploadType_Post']) {
    $TAGS_OBJ[] = array($eReportCard['PostTableHeader'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/basic_settings/post_table_header.php", 0);
}

$linterface->LAYOUT_START();

# Declare variable value
$SettingCategory	= "AccessSettings";			// Setting Category in database

# Load the highlight setting data
$data = $lreportcard->LOAD_SETTING($SettingCategory);

?>
<form name="form1" action="access_setting_update.php" method="post">
<table width="98%" border="0" cellspacing="0" cellpadding="2">
<tr>
	<td>&nbsp;</td>
	<td align="right"><?=$linterface->GET_SYS_MSG($msg);?></td>
</tr>
<tr>
	<td colspan="2">
		<?=$linterface->GET_NAVIGATION2($eReportCard['Management'])?>
		<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td colspan=2><input type="checkbox" name="AllowClassTeacherUploadCSV" id="AllowClassTeacherUploadCSV" value=1 <?=($data[AllowClassTeacherUploadCSV]==1?"CHECKED='CHECKED'":"")?> />&nbsp;<span class="tabletext"><label for="AllowClassTeacherUploadCSV"><?=$eReportCard['AllowClassTeacherUploadCSV']?></label></span></td>
			</tr>
			<tr>
				<!--<td colspan=2><input type="checkbox" name="EnableVerificationPeriod" id="EnableVerificationPeriod" value=1 <?=($data[EnableVerificationPeriod]==1?"CHECKED='CHECKED'":"")?> />&nbsp;<span class="tabletext"><label for="EnableVerificationPeriod"><?=$eReportCard['EnableVerificationPeriod']?></label></span></td> -->
				<td colspan=2>
				<?=$linterface->Get_Checkbox('EnableVerificationPeriod', 'EnableVerificationPeriod', '1',  ($data[EnableVerificationPeriod]==1?"CHECKED='CHECKED'":""), '', $eReportCard['EnableVerificationPeriod']);?>
				</td>
			</tr>
		</table>
		<br />
		
		<?=$linterface->GET_NAVIGATION2($eReportCard['Reports'])?>
		<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td colspan=2><input type="checkbox" name="AllowTeacherAccessGrandMS" id="AllowTeacherAccessGrandMS" value=1 <?=($data[AllowTeacherAccessGrandMS]==1?"CHECKED='CHECKED'":"")?> />&nbsp;<span class="tabletext"><label for="AllowTeacherAccessGrandMS"><?=$eReportCard['AllowTeacherAccessGrandMS']?></label></span></td>
			</tr>
			<tr>
				<td colspan=2><input type="checkbox" name="AllowTeacherAccessMasterReport" id="AllowTeacherAccessMasterReport" value=1 <?=($data[AllowTeacherAccessMasterReport]==1?"CHECKED='CHECKED'":"")?> />&nbsp;<span class="tabletext"><label for="AllowTeacherAccessMasterReport"><?=$eReportCard['AllowTeacherAccessMasterReport']?></label></span></td>
			</tr>
		</table>
		<br />
		
		<?=$linterface->GET_NAVIGATION2($eReportCard['Security'])?>
		<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td colspan=2><input type="checkbox" name="AuthenticationSetting" id=AuthenticationSetting value=1 <?=($data[AuthenticationSetting]==1?"CHECKED='CHECKED'":"")?> />&nbsp;<span class="tabletext"><label for="AuthenticationSetting"><?=$eReportCard['AuthenticationSetting']?></label></span></td>
			</tr>
		</table>
		<br />
		
		<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td colspan="2" height="1" class="dotline"><img src="images/2007a/10x10.gif" width="10" height="1"></td>
			</tr>
			<tr>
				<td colspan="2" align="center" valign="bottom">
					<?= $linterface->GET_ACTION_BTN($button_save, "submit") ?>
	                <?= $linterface->GET_ACTION_BTN($button_reset, "reset") ?>
				</td>
			</tr>
		</table>
	</td>
</tr>
</table>
</form>

<br />
                                  
<? 
print $linterface->FOCUS_ON_LOAD("form1.AllowClassTeacherUploadCSV[0]");
$linterface->LAYOUT_STOP(); ?>
