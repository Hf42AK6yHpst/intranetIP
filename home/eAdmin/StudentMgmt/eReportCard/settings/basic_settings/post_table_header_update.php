<?php
// Using:

/****************************************
 *
 *  Date:   2020-02-10  (Bill)  [2019-0924-1219-32289]
 *          Create file
 *
 ****************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

if (!isset($_POST)) {
    header("Location:post_table_header.php");
}

if (!$plugin['ReportCard2008']) {
    intranet_closedb();
    header("Location:/index.php");
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
$lreportcard = new libreportcard();

if (!$lreportcard->hasAccessRight() || !$eRCTemplateSetting['OtherInfo']['CustUploadType_Post']) {
    intranet_closedb();
    header("Location:/index.php");
}

# Declare variable value
$SettingCategory = "PostTableHeader";

$calSettings["Column1Ch"] = trim($Column1Ch);
$calSettings["Column1En"] = trim($Column1En);
$calSettings["Column1Width"] = IntegerSafe($Column1Width);
$calSettings["Column2Ch"] = trim($Column2Ch);
$calSettings["Column2En"] = trim($Column2En);
$calSettings["Column2Width"] = 100 - IntegerSafe($Column1Width);

# Save column settings
$temp = $lreportcard->LOAD_SETTING($SettingCategory, "", $returnDBValue=1);
if(sizeof($temp)) {
    $success = $lreportcard->UPDATE_SETTING($SettingCategory, $calSettings);
} else {
    $success = $lreportcard->INSERT_SETTING($SettingCategory, $calSettings);
}

if($success)
{
    ### Config CSV [START]
    // Re-build config csv content
    $data_ary = array();
    $data_ary[] = array('EnglishTitle', 'ChineseTitle', 'Type', 'Length', 'Sample1', 'Sample2', 'Sample3');
    $data_ary[] = array('Class Name', '(班別)', 'str', '', 'F1A', '', '');
    $data_ary[] = array('Class Number', '(班號)', 'str', '', '1', '', '');
    $data_ary[] = array('User Login', '(內聯網帳號)', 'str', '', '', 'student1', '');
    $data_ary[] = array('WebSAMSRegNo', '(WebSAMS 註冊號碼)', 'str', '', '', '', '#012345');
    $data_ary[] = array('Student Name [Ref]', '(學生姓名) [參考用途]', 'str', 'Length', 'Wong Siu Ming', 'Chan Siu Man', '');
    $data_ary[] = array($calSettings["Column1En"], '('.$calSettings["Column1Ch"].')', 'str', 'Length', '', '', '');
    $data_ary[] = array($calSettings["Column2En"], '('.$calSettings["Column2Ch"].')', 'str', 'Length', '', '', '');

    $x = "";
    for($i=0; $i<sizeof($data_ary); $i++) {
        $y = '';
        for ($j = 0; $j < sizeof($data_ary[$i]); $j++) {
            $y .= "".$data_ary[$i][$j]."\t";
        }
        $y .= "\r\n";
        $x .= $y;
    }
    $x = mb_convert_encoding($x,'UTF-16LE','UTF-8');
    $x = "\xFF\xFE".$x;

    // Update config csv
    $lo = new libfilesystem();
    //$lo->file_write($x, $intranet_root."/home/eAdmin/StudentMgmt/eReportCard/management/other_info/fanling_kau_yan_college/post_config.csv");
    $lo->file_write($x, $file_path."/file/reportcard2008/templates/post_config.csv");
    //$lo->chmod_R($target_file_path, 0777);
    ### Config CSV [END]

    ### Other Info Data [START]
    // Old Column name
    $temp["Column1En"] = $temp["Column1En"] ? $temp["Column1En"] : 'School Management Post';
    $temp["Column2En"] = $temp["Column2En"] ? $temp["Column2En"] : 'Others Post';

    // Update Other Info data linked to old column name
    $RC_OTHER_INFO_STUDENT_RECORD = $lreportcard->DBName . ".RC_OTHER_INFO_STUDENT_RECORD";
    $sql = "UPDATE $RC_OTHER_INFO_STUDENT_RECORD SET ItemCode = '".$calSettings["Column1En"]."' WHERE ItemCode = '".$temp["Column1En"]."' ";
    $lreportcard->db_db_query($sql);

    $sql = "UPDATE $RC_OTHER_INFO_STUDENT_RECORD SET ItemCode = '".$calSettings["Column2En"]."' WHERE ItemCode = '".$temp["Column2En"]."' ";
    $lreportcard->db_db_query($sql);
    ### Other Info Data [END]
}

intranet_closedb();

$result = ($success) ? "update" : "update_failed";
header("Location:post_table_header.php?msg=$result");
?>