<?php
/********************************************************
 * Date: 20170710 (Anna)
 * Details: add $setting['AuthenticationSetting']
 * 
 * Date: 20130205 (Rita)
 * Details:add variable $setting['EnableVerificationPeriod']
 *******************************************************/
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcard();

# Declare variable value
$SettingCategory	= "AccessSettings";

$setting['AllowClassTeacherUploadCSV'] = $AllowClassTeacherUploadCSV;
$setting['EnableVerificationPeriod'] = $EnableVerificationPeriod;

$setting['AllowTeacherAccessGrandMS'] = $AllowTeacherAccessGrandMS;
$setting['AllowTeacherAccessMasterReport'] = $AllowTeacherAccessMasterReport;

$setting['AuthenticationSetting'] = $AuthenticationSetting;

//$temp = $lreportcard->LOAD_SETTING($SettingCategory);
//if(!empty($temp))
// 	$result = $lreportcard->UPDATE_SETTING($SettingCategory, $setting);
//else
//	$result = $lreportcard->INSERT_SETTING($SettingCategory, $setting);
$successAry = array();
foreach ($setting as $_settingKey => $_settingVal) {
	$temp = $lreportcard->LOAD_SETTING($SettingCategory, $_settingKey, $returnDBValue=true);
	if(!empty($temp)) {
	 	$successAry[$_settingKey] = $lreportcard->UPDATE_SETTING($SettingCategory, array($_settingKey => $_settingVal));
	}
	else {
		$successAry[$_settingKey] = $lreportcard->INSERT_SETTING($SettingCategory, array($_settingKey => $_settingVal));
	}
}
	
//if($result)
if (!in_array(false, $successAry)) {
	$msg = "update";
}
else {
	$msg = "update_failed";
}
	
intranet_closedb();	

header("Location: access_setting.php?msg=$msg");
?>