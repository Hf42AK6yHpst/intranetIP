<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcard();

# Declare variable value
$SettingCategory	= "HighLight";

# Generate Setting value for database
$setting = array();
$levels = array(							// Level for setting
	array("D", 'Distinction'),
	array("P", 'Pass'),
	array("F", 'Fail')
);

for($i=0;$i<sizeof($levels);$i++)
{
	$index = $levels[$i][0];

	$color 	= $_POST['color_'. $index .'_select'];
	$B 		= substr(${"BUI_".$index."_select"},0,1) ? "bold" : "";
	$U 		= substr(${"BUI_".$index."_select"},1,1) ? "underline" : "";
	$I 		= substr(${"BUI_".$index."_select"},2,1) ? "italic" : "";
	$L		= substr(${"sym_".$index."_select"},0,1);
	$R		= substr(${"sym_".$index."_select"},1,1);
	
	//Format of Setting:  Color|bold|underline|italic|LeftSymbol|RightSymbol
	$temp = $color . "|" . $B . "|" . $U . "|" . $I . "|" . $L . "|" . $R;
	$setting[$levels[$i][1]] = $temp;
}

$temp = $lreportcard->LOAD_SETTING($SettingCategory);

if(sizeof($temp))
 	$result = $lreportcard->UPDATE_SETTING($SettingCategory, $setting);
else
	$result = $lreportcard->INSERT_SETTING($SettingCategory, $setting);

if($result)
	$msg = "update";
else
	$msg = "update_failed";
	
intranet_closedb();	

header("Location: highlight.php?msg=$msg");

?>