<?php
#	Editing by

/**************************************************************
 *	Modification log:
 *		Date: 2016-01-20 (Bill)	[2015-0421-1144-05164]
 *			- copy from EJ
 ***************************************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lreportcard = new libreportcardcustom();

$lreportcard->hasAccessRight();
$lreportcard->Start_Trans();

# Get $_POST data
$SubjectClassArr = $_POST['SubjectClassArr'];
$ClassLevelID = $_POST['ClassLevelID'];

# Save
$SuccessArr['Save'] = $lreportcard->Save_Form_Main_Subject($SubjectClassArr, $ClassLevelID);
if (in_array(false, $SuccessArr)){
	$lreportcard->RollBack_Trans();
	echo '0';
}
else{
	$lreportcard->Commit_Trans();
	echo '1';
}

intranet_closedb();
?>