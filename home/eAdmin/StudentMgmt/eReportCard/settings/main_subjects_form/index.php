<?php
#	Editing by
 
/**
 *	Modification log:
 *		Date: 2016-01-20 (Bill)	[2015-0421-1144-05164]
 *			- copy from EJ
 */
 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lreportcard = new libreportcardcustom();
$lreportcard_ui = new libreportcard_ui();

$lreportcard->hasAccessRight();

############## Interface Start ##############

# Current Page
$CurrentPage = "Settings_FormMainSubject";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# Tag Information
$TAGS_OBJ[] = array($eReportCard['Settings_MainSubject'], "", 0);

// Initialize ClassLevelID to first ClassLevel
$ClassLevelInfoArr = $lreportcard->GET_ALL_FORMS($haveReportTemplate=true);
$ClassLevelID = $ClassLevelInfoArr[0]['ClassLevelID'];

$linterface->LAYOUT_START();

echo $lreportcard_ui->Get_Settings_Form_Main_Subject_UI($ClassLevelID);

?>
<script language="javascript">

var SelectedColor = "#E1FCCC";
var NotSelectedColor = "#FFFFFF";

$(document).ready(function() {
	js_Reload_Subject_Class_Table();
});

function js_Reload_Subject_Class_Table() {
	var ClassLevelID = $("#ClassLevelID").val();
	
	Block_Element("SubjectClassTable");
	$.post(
		"ajax_task.php",
		{
			Task:"Reload_Subject_Class_Table",
			ClassLevelID: ClassLevelID
		},
		function(ReturnData){
			$("div#SubjectClassTable").html(ReturnData);
			UnBlock_Element("SubjectClassTable");
		}
	);
}

function js_Save_Subject_Class() {
	var jsSubmitString = Get_Form_Values(document.getElementById('form1'));
	
	Block_Element("SubjectClassTable");
	$.ajax({
		type: "POST",
		url: "ajax_update.php",
		data: jsSubmitString,
		success: function(data) {
			if (data=='1'){
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>');
			}
			else{
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>');
			}
			UnBlock_Element("SubjectClassTable");
			Scroll_To_Top();
		}
	});
	return false;
}

function jsApplyToAllRadio(typeID){
	var SubjectIDAll = document.form1.SubjectIDList.value.split(",");
	
	for(var sc=0; sc<SubjectIDAll.length; sc++){
		var curSubejctID = SubjectIDAll[sc];
		document.getElementById('Check_'+curSubejctID+'_'+typeID).checked = true;
	}
}

</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>