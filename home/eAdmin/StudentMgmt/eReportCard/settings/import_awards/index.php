<?php

##########################
#
#	Date: 	2016-06-29	Bill	[2015-1104-1130-08164]
#			- Create file
#			- copy from /subject_topics/index.php
#
##########################

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");

// Check access right
$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();

$linterface = new interface_html();
$lreportcard_ui = new libreportcard_ui();
$lreportcard_award = new libreportcard_award();

// Tag
$CurrentPage = "Settings_ImportAwards";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($eReportCard['Settings_ImportAwards']);

// Return message
$returnMsgKey = $_GET['returnMsgKey'];
$ReturnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

// Tool Bar
$btnAry = array();
$btnAry[] = array('import', 'javascript: goImport();');
$htmlAry['toolbar'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

// Action Button
$actionBtnAry = array();
$actionBtnAry[] = array('delete', 'javascript:checkRemove2(document.form1,\'awardIDAry[]\',\'goDelete();\')');
$htmlAry['actionBtn'] = $linterface->Get_DBTable_Action_Button_IP25($actionBtnAry);
		
echo $linterface->Include_JS_CSS();
?>

<script type="text/javascript">
$(document).ready(function() {
	$('input#keyword').keydown( function(evt) {
		if (Check_Pressed_Enter(evt)) {
			// pressed enter
			goSearch();
		}
	});
	
	loadDataTable();
});

function initDndTable() {
	$(".common_table_list_v30").tableDnD({
		onDrop: function(table, DroppedRow) {
			if(table.id == "DataTable") {
				Block_Element(table.id);
				
				// Get the order string
				var rowArr = table.tBodies[0].rows;
				var numOfRow = rowArr.length;
				var recordIDArr = new Array();
				for (var i=0; i<numOfRow; i++) {
					var tempId = parseInt(rowArr[i].id.replace('tr_', ''));
					if (tempId != "" && !isNaN(tempId)) {
						recordIDArr[recordIDArr.length] = tempId;
					}
				}
				var recordIDStr = recordIDArr.join(',');
				
				// Update DB
				$.post(
					"ajax_task.php", 
					{
						task: "reorderAwardTopic",
						displayOrderString: recordIDStr
					},
					function(ReturnData)
					{
						// Reset the ranking display
						var jsRowCounter = 0;
						$('span.rowNumSpan').each( function () {
							$(this).html(++jsRowCounter);
						})
						
						// Get system message
						if (ReturnData == '1') {
							jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
						}
						else {
							jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
						}
						
						UnBlock_Element(table.id);
						Scroll_To_Top();
						Get_Return_Message(jsReturnMsg);
					}
				);
			}
		},
		onDragStart: function(table, DraggedRow) {
			//$('#debugArea').html("Started dragging row "+row.id);
		},
		dragHandle: "Dragable",
		onDragClass: "move_selected"
	});
}

function loadDataTable() {
	$('div#dataTableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_task.php", 
		{
			task: 'getAwardTableHtml',
		},
		function(ReturnData) {
			initDndTable();
		}
	);
}

function goDelete() {
	$('form#form1').attr('action', 'delete_update.php').submit();
}

function goImport() {
	$('form#form1').attr('action', 'import_step1.php').submit();
}

</script>
<form id="form1" name="form1" method="post" action="index.php">
	<div class="content_top_tool">
		<div class="Conntent_tool">
	       <?=$htmlAry['toolbar']?>
		</div>
	</div>
	
	<div class="table_board">
		<p class="spacer"></p>
		<?=$htmlAry['actionBtn']?>
		<div id="dataTableDiv"><!--load by ajax--></div>
	</div>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>