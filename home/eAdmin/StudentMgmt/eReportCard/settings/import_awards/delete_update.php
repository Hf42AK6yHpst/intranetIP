<?php

##########################
#
#	Date: 	2016-06-29	Bill	[2015-1104-1130-08164]
#			- Create file
#			- copy from /subject_topics/delete_update.php
#
##########################

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");

$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();

$lreportcard_award = new libreportcard_award();

$RC_AWARD = $lreportcard_award->DBName.".RC_AWARD";
$RC_AWARD_APPLICABLE_FORM_INFO = $lreportcard_award->DBName.".RC_AWARD_APPLICABLE_FORM_INFO";

$success = array();

// loop awards
$awardIDAry = $_POST['awardIDAry'];
$awardCount = count($awardIDAry);
for($i=0; $i<$awardCount; $i++) {
	$curAwardID = $awardIDAry[$i];
	
	// delete award
	$sql = "Delete From $RC_AWARD Where AwardID = '".$curAwardID."'";
	$success[] = $lreportcard_award->db_db_query($sql);
	
	// delete award related forms
	$sql = "Delete From $RC_AWARD_APPLICABLE_FORM_INFO Where AwardID = '".$curAwardID."'";
	$success[] = $lreportcard_award->db_db_query($sql);
}

// Get all awards
$awardAssoAry = $lreportcard_award->Load_All_School_Award_Info(0, 1);
$totalAwardCount = count($awardAssoAry);

// loop awards
$displayOrder = 1;
foreach((array)$awardAssoAry as $_awardId => $curAward) {
	// reorder award list
	$_dataAry = array();
	$_dataAry['DisplayOrder'] = $displayOrder++; 
	$lreportcard_award->Update_All_Award_Info($_awardId, $_dataAry, $updateLastModified=false);
}

if (!in_array(false, $success)) {
	$returnMsgKey = 'DeleteSuccess';
}
else {
	$returnMsgKey = 'DeleteUnsuccess';
}

intranet_closedb();
	
header('location: index.php?&returnMsgKey='.$returnMsgKey);
?>