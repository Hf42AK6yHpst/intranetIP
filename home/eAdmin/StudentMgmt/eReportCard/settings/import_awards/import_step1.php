<?php

##########################
#
#	Date: 	2016-06-29	Bill	[2015-1104-1130-08164]
#			- Create file
#			- copy from /subject_topics/import_step1.php
#
##########################

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

// Check access right
$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();

$linterface = new interface_html();

// Tag
$CurrentPage = "Settings_ImportAwards";	
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($eReportCard['Settings_ImportAwards']);

// Return message
$returnMsgKey = $_GET['returnMsgKey'];
$ReturnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

// Navigation
$navigationAry[] = array($eReportCard['Settings_ImportAwards'], 'javascript: goBack();');
$navigationAry[] = array($Lang['Btn']['Import']);
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($navigationAry);

// Steps
$htmlAry['steps'] = $linterface->GET_IMPORT_STEPS($CurrStep=1);

// Column property display
$columnTitleAry = array();
$columnTitleAry[] = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardCode'];
$columnTitleAry[] = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardName'];
$columnTitleAry[] = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardForm'];
$columnPropertyAry = array(1, 1, 1);
$RemarksArr = array();
$htmlAry['columnRemarks'] = $linterface->Get_Import_Page_Column_Display($columnTitleAry, $columnPropertyAry, $RemarksArr);

// Form table
$x = '';
$x .= '<table id="html_body_frame" width="100%">'."\n";
	$x .= '<tr>'."\n";
		$x .= '<td>'."\n";
			$x .= '<table class="form_table_v30">'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['General']['SourceFile'].' <span class="tabletextremark">'.$Lang['General']['CSVFileFormat'].'</span></td>'."\n";
					$x .= '<td class="tabletext"><input class="file" type="file" name="csvfile" id="csvfile"></td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$Lang['General']['CSVSample'].'</td>'."\n";
					$x .= '<td><a class="tablelink" href="javascript:goDownloadSample();">'. $Lang['General']['ClickHereToDownloadSample'] .'</a></td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'."\n";
						$x .= $Lang['General']['ImportArr']['DataColumn']."\n";
					$x .= '</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $htmlAry['columnRemarks']."\n";
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<td class="tabletextremark" colspan="2">'."\n";
						$x .= $linterface->MandatoryField();
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
$x .= '</table>'."\n";
$htmlAry['formTable'] = $x;

// Buttons
$htmlAry['continueBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Continue'], "button", "goSubmit()", 'sumbitBtn');
$htmlAry['backBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');
?>

<script type="text/javascript">
$(document).ready( function() {
	
});

function goBack() {
	window.location = 'index.php';
}

function goSubmit() {
	$('form#form1').attr('action', 'import_step2.php').submit();
}

function goDownloadSample() {
	$('form#form1').attr('action', 'export.php').submit();
}

function getRemarkDivIdByRemarkType(remarkType) {
	return 'remarkDiv_' + remarkType;
}

/*
function showRemarkLayer(remarkType) {
	var remarkBtnId = 'remarkBtn_' + remarkType;
	var remarkDivId = getRemarkDivIdByRemarkType(remarkType);
	var leftAdjustment = $('#' + remarkBtnId).width();
	var topAdjustment = 0;
	
	changeLayerPosition(remarkBtnId, remarkDivId, leftAdjustment, topAdjustment);
	hideAllRemarkLayer();
	MM_showHideLayers(remarkDivId, '', 'show');
}

function hideRemarkLayer(remarkType) {
	var remarkDivId = getRemarkDivIdByRemarkType(remarkType);
	MM_showHideLayers(remarkDivId, '', 'hide');
}

function hideAllRemarkLayer() {
	$('div.selectbox_layer').each( function() {
		MM_showHideLayers($(this).attr('id'), '', 'hide');
	});
}
*/
</script>

<form name="form1" id="form1" method="POST" enctype="multipart/form-data">

	<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	<br/>
	
	<?=$htmlAry['steps']?>
	<p class="spacer"></p>
	<br/>
	
	<div class="table_board">
		<?=$htmlAry['formTable']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['continueBtn']?>
		<?=$htmlAry['backBtn']?>
		<p class="spacer"></p>
	</div>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>