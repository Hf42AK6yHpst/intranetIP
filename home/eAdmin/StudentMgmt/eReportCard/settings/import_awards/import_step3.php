<?php

##########################
#
#	Date: 	2016-06-29	Bill	[2015-1104-1130-08164]
#			- Create file
#			- copy from /subject_topics/import_step3.php
#
##########################

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");

$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();

$lreportcard_award = new libreportcard_award();

$lfs = new libfilesystem();
$linterface = new interface_html();

// Tag
$CurrentPage = "Settings_ImportAwards";	
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($eReportCard['Settings_ImportAwards']);

// Return message
$ReturnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

// Navigation
$navigationAry[] = array($eReportCard['Settings_ImportAwards'], 'javascript: goCancel();');
$navigationAry[] = array($Lang['Btn']['Import']);
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($navigationAry);

// Steps
$htmlAry['steps'] = $linterface->GET_IMPORT_STEPS($CurrStep=3);

// Get temp Data to be insert
$importDataAry = $lreportcard_award->Get_Import_All_Temp_Data();
$numOfData = count($importDataAry);

// iFrame for real-time update count
$thisSrc = "ajax_task.php?task=importAward";
$htmlAry['iframe'] = '<iframe id="ImportIFrame" name="ImportIFrame" src="'.$thisSrc.'" style="width:100%;height:300px;display:none;"></iframe>'."\n";
$processingMsg = str_replace('<!--NumOfRecords-->', '<span id="BlockUISpan">0</span> / '.$numOfData, $Lang['General']['ImportArr']['RecordsProcessed']);

// Buttons
$htmlAry['doneBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Done'], "button", "goCancel();");

?>
<script type="text/javascript">

$(document).ready( function() {
	Block_Document('<?=$processingMsg?>');
});

function goCancel() {
	window.location = 'index.php';
}
</script>
<form name="form1" id="form1" method="POST">

	<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	<br/>
	
	<?=$htmlAry['steps']?>
	<br/>
	
	<div class="table_board">
		<table width="100%">
			<tr><td style="text-align:center;">
				<span id="NumOfProcessedPageSpan">0</span> <?=$Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully']?>
			</td></tr>
		</table>
		<?=$htmlAry['iframe']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['doneBtn']?>
		<p class="spacer"></p>
	</div>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>