<?php

##########################
#
#	Date: 	2016-06-30	Bill	[2015-1104-1130-08164]
#			- Create file
#			- copy from /subject_topics/ajax_task.php
#
##########################

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");

$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();

$lreportcard_award = new libreportcard_award();

$linterface = new interface_html();
$lreportcard_ui = new libreportcard_ui();

$task = ($_POST['task'])? $_POST['task'] : $_GET['task'];

if ($task == 'validateCode') {
	$formId = standardizeFormPostValue($_POST['formId']);
	$subjectId = standardizeFormPostValue($_POST['subjectId']);
	$code = standardizeFormPostValue($_POST['code']);
	$subjectTopicId = standardizeFormPostValue($_POST['subjectTopicId']);
	
	$isCodeValidate = $lreportcard_subjectTopic->isCodeValid($formId, $subjectId, $code, $subjectTopicId);
	echo $isCodeValidate? "1" : "0";
}
// Display awards table
else if ($task == 'getAwardTableHtml') {
	echo $lreportcard_ui->Get_Import_Awards_Table();
}
// Reorder awards
else if ($task == 'reorderAwardTopic') {
	
	$lreportcard_award->Start_Trans();
	$successAry = array();
	
	// Get New Order
	$displayOrderString = $_POST['displayOrderString'];
	$recordIdAry = explode(',', $displayOrderString);
	
	// loop awards
	$numOfRecord = count($recordIdAry);
	for ($i=0; $i<$numOfRecord; $i++) {
		$_dataAry = array();
		$_dataAry['DisplayOrder'] = $i + 1; 
		$successAry[$_awardId] = $lreportcard_award->Update_All_Award_Info($recordIdAry[$i], $_dataAry, $updateLastModified=false);
	}
	
	if (in_array(false, $successAry)) {
		$lreportcard_award->RollBack_Trans();
		echo '0';
	}
	else {
		$lreportcard_award->Commit_Trans();
		echo '1';
	}
}
// Validation for awards when import
else if ($task == 'validateAwardImport') {
	include_once($PATH_WRT_ROOT.'includes/libimporttext.php');
	$limport = new libimporttext();
	
	$successAry = array();
	
	$targetFilePath = standardizeFormPostValue($_GET['targetFilePath']);
	$importTempTable = $lreportcard->DBName.'.RC_TEMP_IMPORT_ALL_SCHOOL_AWARD';
	
	// Get all forms
	$formAry = $lreportcard->GET_ALL_FORMS($hasTemplateOnly=false, $excludeWithoutClass=false);
	$formNameList = Get_Array_By_Key((array)$formAry, "LevelName");	
	
	// Get all awards
	$awardAssoAry = $lreportcard_award->Load_All_School_Award_Info(1, 1);
	$awardNameList = Get_Array_By_Key((array)$awardAssoAry, "AwardNameEn");
	
	// Include JS Libraries
	$jsInclude = '';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
	echo $jsInclude;
	
	// Get Data from the CSV file
	//$columnTitleAry = array();
	//$columnTitleAry[] = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['AwardCode'];
	//$columnTitleAry[] = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['AwardName'];
	//$columnTitleAry[] = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['AwardForm'];
	//$columnPropertyAry = array(1, 1, 1);
	$csvData = $limport->GET_IMPORT_TXT($targetFilePath, "", "");
	$csvColNameAry = array_shift($csvData);
	
	// Delete existing temp data
	$successAry['deleteOldTempData'] = $lreportcard_award->Delete_Import_All_Temp_Data();
	
	$errorCount = 0;
	$successCount = 0;
	$rowErrorRemarksAry = array();
	$insertTempDataAry = array();
	
	// loop data from CSV file
	$numOfData = count($csvData);
	for ($i=0; $i<$numOfData; $i++) {
		$_errorMsgAry = array();
		$isNewAward = false;
		
		$_rowNumber = $i + 2;
		$_awardID = 0;
		$_awardCode = trim($csvData[$i][0]);
		$_awardName = trim($csvData[$i][1]);
		$_awardForm = trim($csvData[$i][2]);
		$_awardFormList = str_split($_awardForm, 2);
		
		// Empty award code / award applied forms => No need to handle
		if (trim($_awardCode)=='' || trim($_awardForm)=='') {
			continue;
		}
		else {
			// Check if any existing award using same award code
			if (empty($awardAssoAry[$_awardCode]))
				$isNewAward = true;
			else 
				$_awardID = $awardAssoAry[$_awardCode]["AwardID"];
			
			// Warning (Award Name Empty)
			if ($_awardName=='') {
				$_errorMsgAry[] = $Lang['eReportCard']['ImportWarningArr']['empty_award_name'];
			}
			// Warning (Award Name Duplicated)
			else if(in_array($_awardName, (array)$awardNameList)) {
				if($isNewAward || (!$isNewAward && $_awardName!=trim($awardAssoAry[$_awardCode]["AwardNameEn"])))
				{
					$_errorMsgAry[] = $Lang['eReportCard']['ImportWarningArr']['duplicate_award_name'];
				}
			}
			
			// Warning (Award applied form not exist)
			// loop forms
			for($j=0; $j<count((array)$_awardFormList); $j++){
				if(!in_array($_awardFormList[$j], (array)$formNameList)){
					$_errorMsgAry[] = $Lang['eReportCard']['ImportWarningArr']['award_form_not_found'];
					break;
				}
			}
			$_awardForm = implode(", ",(array)$_awardFormList);
		}
		
		// Prepare sql to insert data to temp table
		$insertTempDataAry[] = " (	'".$_SESSION['UserID']."', '$_rowNumber', '$_awardID', '".$lreportcard->Get_Safe_Sql_Query($_awardCode)."',
										'".$lreportcard->Get_Safe_Sql_Query($_awardName)."', '".$lreportcard->Get_Safe_Sql_Query($_awardForm)."',now()
									) ";
									
		// Check any error message	
		if(count($_errorMsgAry) == 0)
			$successCount++;
		else
			$rowErrorRemarksAry[$_rowNumber] = $_errorMsgAry;
		
		// JS for Processing Display
		Flush_Screen_Display(1);
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= '$("span#BlockUISpan", window.parent.document).html("'.($i + 1).'");';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
	}
	
	// Insert data to temp table
	if (count($insertTempDataAry) > 0) {
		$sql = "Insert Into $importTempTable
					(ImportUserID, RowNumber, AwardID, AwardCode, AwardName, AwardAppliedForm, DateInput)
				Values
					".implode(', ', (array)$insertTempDataAry);
		$successAry['insertCsvDataToTempTable'] = $lreportcard->db_db_query($sql);
	}
	
	$x = '';
	
	// Display Record Error Table
	$errorCount = count($rowErrorRemarksAry);
	$errorCountDisplay = ($errorCount ? "<font color=\"red\"> ": "") . $errorCount . ($errorCount ? "</font>" : "");
	if($errorCount > 0) {
		
		$csvHeaderAry = array();
		$csvHeaderAry[] = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardCode'];
		$csvHeaderAry[] = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardName'];
		$csvHeaderAry[] = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardForm'];
		$numOfColumn = count($csvHeaderAry);
		
		// Table content for error data
		$errorRowNumAry = array_keys($rowErrorRemarksAry);
		$sql = "select * from $importTempTable where ImportUserID = '".$_SESSION['UserID']."' And RowNumber In ('".implode("','", (array)$errorRowNumAry)."') ";
		$errorRowInfoAry = $lreportcard->returnResultSet($sql);
		
		$x .= '<table class="common_table_list_v30 view_table_list_v30">';
		
			// Table Header
			$x .= '<thead>';
				$x .= '<tr>';
					$x .= '<th>'.$Lang['General']['ImportArr']['Row'].'</th>';
						for ($i=0; $i<$numOfColumn; $i++) {
							$x .= '<th>'.$csvHeaderAry[$i].'</th>';
						}
					$x .= '<th>'.$Lang['General']['Remark'].'</th>';
				$x .= '</tr>';
			$x .= '</thead>';
			
			// Table Content
			$x .= '<tbody>';
			
				// loop error data
				for ($i=0; $i<$errorCount; $i++) {
					$_tempId = $errorRowInfoAry[$i]['TempID'];
					$_rowNumber = $errorRowInfoAry[$i]['RowNumber'];
					$_awardCode = $errorRowInfoAry[$i]['AwardCode'];
					$_awardName = $errorRowInfoAry[$i]['AwardName'];
					$_awardForm = $errorRowInfoAry[$i]['AwardAppliedForm'];
					
					$_awardCode = $_awardCode ? $_awardCode : '<font color="red">***</font>';
					$_awardName = $_awardName ? $_awardName : '<font color="red">***</font>';
					$_awardForm = $_awardForm ? $_awardForm : '<font color="red">***</font>';					
					
					// Display error remarks
					$_errorDisplay = '';
					$_errorRemarksAry = $rowErrorRemarksAry[$_rowNumber];
					if (is_array($_errorRemarksAry) && count($_errorRemarksAry) > 0) {
						if (in_array($Lang['eReportCard']['ImportWarningArr']['empty_award_name'], $_errorRemarksAry)) {
							$_awardName = '<font color="red">'.$_awardName.'</font>';
						}
						if (in_array($Lang['eReportCard']['ImportWarningArr']['duplicate_award_name'], $_errorRemarksAry)) {
							$_awardName = '<font color="red">'.$_awardName.'</font>';
						}
						if (in_array($Lang['eReportCard']['ImportWarningArr']['award_form_not_found'], $_errorRemarksAry)) {
							$_awardForm = '<font color="red">'.$_awardForm.'</font>';
						}
						
						// display error message
						$_errorDisplay = '- '.implode('<br />- ', $_errorRemarksAry);
					}
					
					$x .= '<tr>';
						$x .= '<td>'.$_rowNumber.'</td>';
						$x .= '<td>'.$_awardCode.'</td>';
						$x .= '<td>'.$_awardName.'</td>';
						$x .= '<td>'.$_awardForm.'</td>';
						$x .= '<td>'.$_errorDisplay.'</td>';
					$x .= '</tr>';
				}
			$x .= '</tbody>';
		$x .= '</table>';
		$htmlAry['errorTbl'] = $x;
	}
	$errorCountDisplay = ($errorCount > 0) ? "<font color=\"red\">".$errorCount."</font>" : $errorCount;
	
	// JS for Import Result
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= 'window.parent.document.getElementById("ErrorTableDiv").innerHTML = \''.$htmlAry['errorTbl'].'\';';
		$thisJSUpdate .= 'window.parent.document.getElementById("SuccessCountDiv").innerHTML = \''.$successCount.'\';';
		$thisJSUpdate .= 'window.parent.document.getElementById("FailCountDiv").innerHTML = \''.$errorCountDisplay.'\';';
		
		if ($errorCount == 0) {
			$thisJSUpdate .= '$("input#ImportBtn", window.parent.document).removeClass("formbutton_disable").addClass("formbutton");';
			$thisJSUpdate .= '$("input#ImportBtn", window.parent.document).attr("disabled","");';
		}
		
		$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
}
// Insert / Update awards
else if ($task == 'importAward') {
	
	// Include JS Libraries
	$jsInclude = '';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
	echo $jsInclude;
	
	// Get all forms
	$formAry = $lreportcard->GET_ALL_FORMS($hasTemplateOnly=false, $excludeWithoutClass=false);
	$formNameList = BuildMultiKeyAssoc($formAry, array("LevelName"), array("ClassLevelID"), 1, 1);	
	
	// Get all reports
	$reportAry = $lreportcard->Get_Report_List();
	$reportAry = BuildMultiKeyAssoc((array)$reportAry, array("isMainReport", "ClassLevelID", "ReportID"), array("ReportID"));
	$reportAry = $reportAry[1];
	
	// Get temp data
	$importData = $lreportcard_award->Get_Import_All_Temp_Data();
	$numOfData = count($importData);
	
	// loop data
	for ($i=0; $i<$numOfData; $i++) {
		$_awardID = $importData[$i]['AwardID'];
		$_awardCode = $importData[$i]['AwardCode'];
		$_awardName = $importData[$i]['AwardName'];
		$_awardForm = $importData[$i]['AwardAppliedForm'];
		$_awardFormList = explode(", ", $_awardForm);
		
		$DataArr = array();
		$DataArr["AwardCode"] = $_awardCode;
		$DataArr["AwardNameEn"] = $_awardName;
		$DataArr["AwardNameCh"] = $_awardName;
		$DataArr["AwardType"] = "OVERALL";
		$DataArr["ApplicableReportType"] = "ALL";
		$DataArr["SchoolCode"] = $ReportCardCustomSchoolName;
		$DataArr["DisplayOrder"] = ($i + 1);
		
		// Insert award
		if ($_awardID == 0) {
			$lreportcard_award->Insert_Award_Record($DataArr);
			$_awardID = $lreportcard_award->db_insert_id();
		}
		// Update award
		else {
			$lreportcard_award->Update_All_Award_Info($_awardID, $DataArr, $updateLastModified=true);
		}
		
		// loop award related forms
		for($j=0; $j<count((array)$_awardFormList); $j++){
			$_yearname = $_awardFormList[$j];
			$_yearid = $formNameList[$_yearname][0];
			
			if($_yearid > 0){
				$_reportids = $reportAry[$_yearid];
				$_reportids = array_keys((array)$_reportids);
				
				$applicableAry = array();
				$applicableAry[0]['YearID'] = $_yearid;
				$applicableAry[0]['Quota'] = 0; 
				
				// loop form reports and add award to applicable form
				for($k=0; $k<count((array)$_reportids); $k++){
					$lreportcard_award->Update_Award_Applicable_Form_Info($_awardID, $_reportids[$k], $applicableAry);
				}
			}
		}
		
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.document.getElementById("BlockUISpan").innerHTML = "'.($i + 1).'";';
			$thisJSUpdate .= 'window.parent.document.getElementById("NumOfProcessedPageSpan").innerHTML = "'.($i + 1).'";';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
	}
	
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
	
	// Clear all temp data
	$lreportcard_award->Delete_Import_All_Temp_Data();
}

intranet_closedb();
?>