<?php
// Using:

/********************** Change Log ***********************/
#
#	Date:	2019-03-01 (Bill)	[2018-1130-1440-05164]
#			Support Related grade update  ($eRCTemplateSetting['Management']['ClassTeacherComment']['RelatedGradeSettings'])
#           (for Class Teacher's Comment only)
#
/******************* End Of Change Log *******************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

$PageRight = "ADMIN";

if (!$plugin['ReportCard2008']) {
	intranet_closedb();
	header("Location:/index.php");
}

if (!isset($_POST)) {
	header("Location:index.php");
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
$lreportcard = new libreportcard();

# Get HTTP variable
$IsAdditional = (!isset($_POST['IsAdditional']) || $_POST['IsAdditional']=='')? 0 : $_POST['IsAdditional'];

if (!$lreportcard->hasAccessRight()) {
	intranet_closedb();
	header("Location:/index.php");
}

if ($type == "") {
	header("Location:edit.php?Result=update_failed");
} else {
	if ($code == "" || ($comment == "" && $commentEng == "")) {
		header("Location:edit.php?Result=update_failed");
	}
	if ($type == "1" && (($CatText == "" && $CatSelect == "") || $SubjectID == "")) {
		header("Location:edit.php?Result=update_failed");
	}
}

// Get all Comment Code to prevent duplicate
$table = $lreportcard->DBName.".RC_COMMENT_BANK";
$sql = "SELECT UPPER(CommentCode) FROM $table WHERE CommentID != $CommentID And IsAdditional = '".$IsAdditional."' ";
if ($type == 1) {
	$sql .= " AND SubjectID IS NOT NULL AND SubjectID != 0";
} else if ($type == 0) {
	$sql .= " AND (SubjectID IS NULL OR SubjectID = 0)";
}
$commentCodeList = $lreportcard->returnVector($sql);

if (in_array(strtoupper($code),$commentCodeList)) {
	header("Location:edit.php?Result=update_failed");
}

# Format Comment
if(!get_magic_quotes_gpc()) { 
	$comment = mysql_real_escape_string($comment);
	$commentEng = mysql_real_escape_string($commentEng);
}

if (isset($CatSelect)) {
	$cat = $CatSelect;
} else {
	$cat = $CatText;
}

// [2018-1130-1440-05164] Related grade
$grade = $GradeSelect == ''? "NULL" : "'$GradeSelect'";
	
$table = $lreportcard->DBName.".RC_COMMENT_BANK";
$sql  = "UPDATE $table SET ";
if ($type == "0") {
	$sql .= "CommentCode = '$code', Comment = '$comment', CommentEng = '$commentEng', RelatedGrade = $grade, DateModified = NOW() ";
} else if ($type == "1") {
	$sql .= "CommentCode = '$code', CommentCategory = '$cat', SubjectID= '$SubjectID', ";
	$sql .= "Comment = '$comment', CommentEng = '$commentEng', DateModified = NOW() ";
}
$sql .= "WHERE CommentID = '$CommentID'";

$success = $lreportcard->db_db_query($sql);

intranet_closedb();

$Result = ($success) ? "update" : "update_failed";
$CommentType = ($type == "0") ? "class":"subject";
header("Location:index.php?Result=$Result&CommentType=$CommentType&IsAdditional=$IsAdditional");
?>