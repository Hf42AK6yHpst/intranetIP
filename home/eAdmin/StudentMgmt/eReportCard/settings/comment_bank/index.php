<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_comment_bank_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_comment_bank_page_number", $pageNo, 0, "", "", 0);
	$ck_comment_bank_page_number = $pageNo;
}
else if (!isset($pageNo) && $ck_comment_bank_page_number!="")
{
	$pageNo = $ck_comment_bank_page_number;
}

if ($ck_comment_bank_page_order!=$order && $order!="")
{
	setcookie("ck_comment_bank_page_order", $order, 0, "", "", 0);
	$ck_comment_bank_page_order = $order;
}
else if (!isset($order) && $ck_comment_bank_page_order!="")
{
	$order = $ck_comment_bank_page_order;
}

if ($ck_comment_bank_page_field!=$field && $field!="")
{
	setcookie("ck_comment_bank_page_field", $field, 0, "", "", 0);
	$ck_comment_bank_page_field = $field;
}
else if (!isset($field) && $ck_comment_bank_page_field!="")
{
	$field = $ck_comment_bank_page_field;
}

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}

	$CurrentPage = "Settings_CommentBank";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    if ($lreportcard->hasAccessRight())
    {
        $linterface = new interface_html();

############################################################################################################


# Get http variable
$IsAdditional = (!isset($_GET['IsAdditional']) || $_GET['IsAdditional']=='')? 0 : $_GET['IsAdditional'];


# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if (!isset($field)) $field = 0;
if (!isset($order)) $order = 1;
$order = ($order == 1) ? 1 : 0;

$li = new libdbtable2007($field, $order, $pageNo);

$table = $lreportcard->DBName.".RC_COMMENT_BANK";

if ($intranet_session_language == "en") {
	$CommentField = "IF(CommentEng='',Comment,CommentEng)";
} else {
	$CommentField = "IF(Comment='',CommentEng,Comment)";
}

if (!isset($CommentType) || $CommentType == "" || $CommentType == "class") {
	$exportCond = "CommentType=class";
	// for use in going to other page, like new.php
	$redirectType = 0;
	//$cond = " WHERE SubjectID IS NULL OR SubjectID = '' ";
	$sql  = "SELECT 
				CONCAT('<a class=\'tablelink\' href=\'edit.php?CommentID=', CommentID, '\'>', CommentCode, '</a>'),
				CONCAT('<a class=\'tablelink\' href=\'edit.php?CommentID=', CommentID, '\'>', REPLACE($CommentField, '"."//"."', ''), '</a>'),
				CONCAT('<input type=\'checkbox\' name=\'CommentID[]\' value=\'', CommentID ,'\'>')
			FROM
				$table
			WHERE 
				(SubjectID IS NULL OR SubjectID = '')
				And IsAdditional = '".$IsAdditional."'
			 ";
	$li->field_array = array("CommentCode", "$CommentField");
	$li->column_array = array(0,18);
	$li->wrap_array = array(0,0);
	
	
	// TABLE COLUMN
	$pos = 0;
	$li->column_list .= "<th width='10' class='tabletoplink'>#</th>\n";
	$li->column_list .= "<th width='10%' >".$li->column_IP25($pos++, $eReportCard['Code'])."</th>\n";
	$li->column_list .= "<th width='85%'>".$li->column_IP25($pos++, $eReportCard['CommentContent'])."</th>\n";
	$li->column_list .= "<th width='15'>".$li->check("CommentID[]")."</th>\n";
} else if ($CommentType == "subject") {
	$redirectType = 1;
	$exportCond = "CommentType=subject";
	if (isset($SubjectID) && $SubjectID != "") {
		$cond = "WHERE a.SubjectID = '$SubjectID'";
		$exportCond .= "&SubjectID=$SubjectID";
	} else {
		$cond = "WHERE (a.SubjectID IS NOT NULL OR a.SubjectID != '')";
	}
	if (isset($cat) && $cat != "") {
		$cond .= " AND CommentCategory = '$cat'";
		$exportCond .= "&Category=$cat";
	}
	$SubjectField = "CONCAT(b.EN_DES, ' (', b.CH_DES, ')')";
	$sql  = "SELECT 
				CONCAT('<a class=\'tablelink\' href=\'edit.php?CommentID=', a.CommentID, '\'>', a.CommentCode, '</a>'),
				CONCAT('<a class=\'tablelink\' href=\'edit.php?CommentID=', a.CommentID, '\'>', REPLACE($CommentField, '"."//"."', ''), '</a>'),
				$SubjectField AS SubjectName,
				a.CommentCategory,
				CONCAT('<input type=\'checkbox\' name=\'CommentID[]\' value=\'', a.CommentID ,'\'>')
			FROM
				$table as a
				LEFT JOIN ASSESSMENT_SUBJECT as b
				ON a.SubjectID=b.RecordID
			$cond
			 ";
	$li->field_array = array("CommentCode", "$CommentField", "SubjectName", "CommentCategory");
	$li->column_array = array(0,18,0,0);
	$li->wrap_array = array(0,0,0,0);
	// TABLE COLUMN
	$pos = 0;
	$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
	$li->column_list .= "<th width='10%' >".$li->column_IP25($pos++, $eReportCard['Code'])."</th>\n";
	$li->column_list .= "<th width='45%'>".$li->column_IP25($pos++, $eReportCard['CommentContent'])."</th>\n";
	$li->column_list .= "<th width='30%'>".$li->column_IP25($pos++, $eReportCard['Subject'])."</th>\n";
	$li->column_list .= "<th width='15%'>".$li->column_IP25($pos++, $eReportCard['Category'])."</th>\n";
	$li->column_list .= "<th width='1'>".$li->check("CommentID[]")."</th>\n";
}

$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
//$li->IsColOff = 2;
$li->IsColOff = 'IP25_table';

if ($CommentType == "subject") {	
	// Subject fliter
	$SubjectArray = $lreportcard->GET_ALL_SUBJECTS(1);
	$SubjectOption = array();
	$SubjectSelect = "";
	if (sizeof($SubjectArray) > 0) {
		foreach ($SubjectArray as $tempSubjectID => $tempSubjectDetail) {
			$array_subject_data[] = $tempSubjectID;
			$array_subject_name[] = $tempSubjectDetail[0];
		}
		$SelectedSubject = (isset($SubjectID) && !empty($SubjectID)) ? $SubjectID : $SubjectOption[0][0];
		$SubjectSelect = getSelectByValueDiffName($array_subject_data, $array_subject_name , 'name="SubjectID" id="SubjectID" onchange="jsSubjectFilter()"', $SelectedSubject, 1, 0, $eReportCard['AllSubjects']);
	}
	
	$filterbar = $SubjectSelect;
	
	if (isset($SubjectID) && !empty($SubjectID)) {
		// Comment category filter
		$commentCat = $lreportcard->GET_COMMENT_CAT($SubjectID);
		$array_cat_name = $commentCat;
		$array_cat_data = $commentCat;
		$select_cat = getSelectByValueDiffName($array_cat_data, $array_cat_name, "name='cat' onChange='jsCatFilter()'",$cat,1,0, $eReportCard['AllCategories']);
		
		$filterbar .= $select_cat;
	}
} else {
	$filterbar = "";
}

$exportCond .= '&IsAdditional='.$IsAdditional;
$toolbar = '';
$toolbar .= $linterface->GET_LNK_NEW("javascript:checkNew('new.php?type=".$redirectType."&SubjectID=".$SubjectID."&CatSelect=".$cat."&IsAdditional=".$IsAdditional."')", '', '', '', '', 0);
$toolbar .= $linterface->GET_LNK_IMPORT("javascript:checkNew('import.php?".$exportCond."')", '', '', '', '', 0);
$toolbar .= $linterface->GET_LNK_EXPORT("javascript:checkNew('export.php?".$exportCond."')", '', '', '', '', 0);


//$table_tool .= "<td nowrap=\"nowrap\">
//					<a href=\"javascript:checkEdit(document.form1,'CommentID[]','edit.php')\" class=\"tabletool\">
//						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
//						$button_edit
//					</a>
//				</td>";
//$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
//$table_tool .= "<td nowrap=\"nowrap\">
//					<a href=\"javascript:checkRemove(document.form1,'CommentID[]','remove.php')\" class=\"tabletool\">
//						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
//						$button_remove
//					</a>
//				</td>";

$BtnArr = array();
$BtnArr[] = array('edit', 'javascript:checkEdit(document.form1,\'CommentID[]\',\'edit.php\');');
$BtnArr[] = array('delete', 'javascript:checkRemove(document.form1,\'CommentID[]\',\'remove.php\');');
$table_tool = $linterface->Get_DBTable_Action_Button_IP25($BtnArr);

############################################################################################################

if ($Result == "num_records_updated") {
	$SysMsg = $linterface->GET_SYS_MSG("", "<font color='green'>$SuccessCount $i_con_msg_num_records_updated</font>");
} else {
	$SysMsg = $linterface->GET_SYS_MSG($Result);
}

# tag information
$TAGS_OBJ = array();
// [2020-0708-0950-12308]
if ($eRCTemplateSetting['ClassTeacherComment_AdditionalComment_as_BilingualTeacher']) {
    $TAGS_OBJ[] = array($eReportCard['ManagementArr']['ClassTeacherCommentArr']['CommentCustDisplay'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/comment_bank/index.php?CommentType=class", (($CommentType=="class" || !isset($CommentType)) && $IsAdditional==false)?1:0);
} else {
    $TAGS_OBJ[] = array($eReportCard['Management_ClassTeacherComment'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/comment_bank/index.php?CommentType=class", (($CommentType=="class" || !isset($CommentType)) && $IsAdditional==false)?1:0);
}
if ($eRCTemplateSetting['ClassTeacherComment_AdditionalComment']) {
	$TAGS_OBJ[] = array($eReportCard['ManagementArr']['ClassTeacherCommentArr']['AdditionalComment'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/comment_bank/index.php?CommentType=class&IsAdditional=1", ($CommentType=="class" && $IsAdditional==1)?1:0);
}
$TAGS_OBJ[] = array($eReportCard['Template']['SubjectTeacherComment'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/comment_bank/index.php?CommentType=subject", ($CommentType=="subject")?1:0);

$linterface->LAYOUT_START();
?>

<script language="javascript">
<!--
function jsCatFilter(cat) {
	document.form1.submit();
}

function jsSubjectFilter(cat) {
	document.form1.submit();
}
//-->
</script>

<br/>
<form name="form1" method="get">
<table width="100%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?></td>
		<td align="right"><?= $SysMsg ?></td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr class="table-action-bar">
		<td align="left"><?=$filterbar?><?=$searchbar?></td>
		<td valign="bottom" align="right">
			<?=$table_tool?>
		</td>
	</tr>
</table>
<?php echo $li->display(); ?>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="CommentType" value="<?=$CommentType?>" />
<input type="hidden" name="IsAdditional" value="<?=$IsAdditional?>" />
</form>
<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>