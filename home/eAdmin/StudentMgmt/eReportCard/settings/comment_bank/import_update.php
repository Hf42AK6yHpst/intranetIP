<?php
// Using:

/********************** Change Log ***********************/
#	Date:	2019-12-12 (Philips)[2019-1206-1707-57235]
#			Added table column in insert query
#
#	Date:	2019-03-01 (Bill)	[2018-1130-1440-05164]
#			Support Related grade update  ($eRCTemplateSetting['Management']['ClassTeacherComment']['RelatedGradeSettings'])
#           (for Class Teacher's Comment only)
#
/******************* End Of Change Log *******************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	$lreportcard = new libreportcard();
	
	# Get HTTP variable
	$IsAdditional = (!isset($_POST['IsAdditional']) || $_POST['IsAdditional']=='')? 0 : $_POST['IsAdditional'];
	
	if ($lreportcard->hasAccessRight())
	{
		$limport = new libimporttext();
		
		if ($type == "0")
		{
		    $format_array = array("Code","Comment Content","Comment Content English");
		    
		    // [2018-1130-1440-05164]
		    if($eRCTemplateSetting['Management']['ClassTeacherComment']['RelatedGradeSettings']) {
		        $format_array[] = "Related grade";
		    }
		}
		if ($type == "1")
		{
			$format_array = array("Code","Category","Comment Content","Comment Content English");
		}
		
		$li = new libdb();
		$filepath = $userfile;
		$filename = $userfile_name;
		
		// Get all Comment Code to prevent duplicate
		$table = $lreportcard->DBName.".RC_COMMENT_BANK";
		$sql = "SELECT CommentID, SubjectID, UPPER(CommentCode) AS CommentCode FROM $table";
		if ($type == 1) {
			$sql .= " WHERE SubjectID IS NOT NULL AND SubjectID != 0";
		} else if ($type == 0) {
			$sql .= " WHERE (SubjectID IS NULL OR SubjectID = 0) And IsAdditional = '".$IsAdditional."' ";
		}
		$commentArr = $lreportcard->returnArray($sql);
		
		$commentCodeIdMap = array();
		$commentCodeSubjectMap = array();
		for($i=0; $i<sizeof($commentArr); $i++) {
			$commentCodeIdMap[$commentArr[$i]["CommentCode"]] = $commentArr[$i]["CommentID"];
			$commentCodeSubjectMap[$commentArr[$i]["CommentCode"]] = $commentArr[$i]["SubjectID"];
		}
		
		$selfCommentCodeList = array();
		if ($type == 1) {
			$sql = "SELECT UPPER(CommentCode) AS CommentCode FROM $table WHERE SubjectID = '$SubjectID'";
			$selfCommentCodeList = $lreportcard->returnVector($sql);
		}
		
		if($filepath=="none" || $filepath == "") {          # import failed
		    header("Location: import.php?Result=import_failed2&CommentType=$CommentType&IsAdditional=$IsAdditional");
		    exit();
		} else {
		    if($limport->CHECK_FILE_EXT($filename))
		    {
				# read file into array
				# return 0 if fail, return csv array if success
				$data = $limport->GET_IMPORT_TXT($filepath);
				# $toprow = array_shift($data);                   # drop the title bar
				
				$limport->SET_CORE_HEADER($format_array);
				if (!$limport->VALIDATE_HEADER($data, true) || !$limport->VALIDATE_DATA($data)) {
					header("Location: import.php?Result=import_failed&CommentType=$CommentType&IsAdditional=$IsAdditional");
					exit();
				}
				array_shift($data);
				
			    $values = "";
			    $delim = "";
				$successCount = 0;
				$failCount = 0;
				$csvCodeList = array();
			    for ($i=0; $i<sizeof($data); $i++) {
					if (empty($data[$i])) continue;
					
					if ($type == "0")
					{
						list($code,$comment,$commentEng,$grade) = $data[$i];
						$code = strtoupper(trim($code));
						$comment = addslashes(intranet_htmlspecialchars(convert2unicode($comment)));
						$commentEng = addslashes(intranet_htmlspecialchars(convert2unicode($commentEng)));
						
						// [2018-1130-1440-05164] Related grade
						$grade = addslashes(intranet_htmlspecialchars(convert2unicode($grade)));
						$grade = $grade == ''? "NULL" : "'$grade'";
						
						if (isset($commentCodeIdMap[$code])) {
							$updateSql = "UPDATE $table SET Comment = '$comment', CommentEng = '$commentEng', RelatedGrade = $grade, DateModified = NOW() ";
							$updateSql .= "WHERE CommentID = '".$commentCodeIdMap[$code]."'";
							$successCount += $li->db_db_query($updateSql);
						} else if (!in_array($code, $csvCodeList)) {
							$cols = "(CommentCode, CommentCategory, SubjectID, CommentEng, Comment, DateInput, DateModified, IsAdditional, RelatedGrade)";
							$values = " ('$code',NULL,NULL,'$commentEng','$comment',NOW(),NOW(),'$IsAdditional','$grade') ";
							$sql = "INSERT INTO $table $cols VALUES $values";
							$successCount += $li->db_db_query($sql);
							$csvCodeList[] = $code;
						}
// 						$successCount++;
					}
					if ($type == "1")
					{
						list($code,$category,$comment,$commentEng) = $data[$i];
						$code = strtoupper(trim($code));
						$category = addslashes(intranet_htmlspecialchars(convert2unicode($category)));
						$comment = addslashes(intranet_htmlspecialchars(convert2unicode($comment)));
						$commentEng = addslashes(intranet_htmlspecialchars(convert2unicode($commentEng)));
						
						if (!isset($commentCodeSubjectMap[$code]) || $commentCodeSubjectMap[$code] == $SubjectID) {
							if (isset($commentCodeIdMap[$code]) && in_array($code, $selfCommentCodeList)) {
								$updateSql = "UPDATE $table SET CommentCategory='$category', SubjectID= '$SubjectID', Comment = '$comment', CommentEng = '$commentEng', DateModified = NOW() ";
								$updateSql .= "WHERE CommentID = '".$commentCodeIdMap[$code]."'";
								$successCount += $li->db_db_query($updateSql);
// 								$successCount++;
							} else if (!isset($commentCodeIdMap[$code]) && !in_array($code, $csvCodeList)) {
								$cols = "(CommentCode, CommentCategory, SubjectID, CommentEng, Comment, DateInput, DateModified, IsAdditional)";
								$values = " ('$code','$category','$SubjectID','$commentEng','$comment',NOW(),NOW(),'$IsAdditional') ";
								$sql = "INSERT INTO $table $cols VALUES $values";
								$successCount += $li->db_db_query($sql);
								$csvCodeList[] = $code;
// 								$successCount++;
							}	
						}
					}
					//$delim = ",";
// 					debug_pr($sql);
			    }
				
				#$table = $lreportcard->DBName.".RC_COMMENT_BANK";
			    //$sql = "INSERT INTO $table VALUES $values";
			    //$li->db_db_query($sql);
		    }
		    else
		    {
				header("Location: import.php?Result=import_failed&CommentType=$CommentType&IsAdditional=$IsAdditional");
				exit();
			}
		}
		
		intranet_closedb();
		
		$CommentType = ($type == '0') ? "class" : "subject";
		if ($successCount === 0) {
			header("Location: import.php?Result=import_failed2&CommentType=$CommentType&IsAdditional=$IsAdditional");
		} else {
			header("Location: index.php?Result=num_records_updated&SuccessCount=$successCount&CommentType=$CommentType&IsAdditional=$IsAdditional");
		}
	} else {
		echo "You have no priviledge to access this page.";
	}
}
else
{
	echo "You have no priviledge to access this page.";
}
?>