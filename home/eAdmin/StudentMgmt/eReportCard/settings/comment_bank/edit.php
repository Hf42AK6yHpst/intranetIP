<?php
// Using:

/********************** Change Log ***********************/
#
#   Date:   2020-04-28  (Bill)  [2019-0924-1029-57289]
#           Updated related grade settings logic    ($eRCTemplateSetting['Management']['ClassTeacherComment']['RelatedGradeList'])
#
#	Date:	2019-05-13	(Bill)
#			Prevent SQL Injection
#
#	Date:	2019-03-01 (Bill)	[2018-1130-1440-05164]
#			Add Related grade settings  ($eRCTemplateSetting['Management']['ClassTeacherComment']['RelatedGradeSettings'])
#           (for Class Teacher's Comment only)
#
/******************* End Of Change Log *******************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	}
	else {
		$lreportcard = new libreportcard();
	}
	
	# Get HTTP variable
	$IsAdditional = (!isset($_GET['IsAdditional']) || $_GET['IsAdditional']=='')? 0 : $_GET['IsAdditional'];
	
	# Module
	$CurrentPage = "Settings_CommentBank";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	
	if ($lreportcard->hasAccessRight())
	{
	    ### Handle SQL Injection + XSS [START]
	    $CommentID = IntegerSafe($CommentID);
	    $IsAdditional = IntegerSafe($IsAdditional);
	    ### Handle SQL Injection + XSS [END]
	    
		$linterface = new interface_html();
		
		$CommentID = (is_array($CommentID)? $CommentID[0] : $CommentID);
		if (!isset($CommentID) || $CommentID == "") {
			header("Location:index.php");
		}
		
		# Get Comment Info
		$table = $lreportcard->DBName.".RC_COMMENT_BANK";
		$sql = "SELECT * FROM $table WHERE CommentID = '$CommentID' ";
		$commentDetail = $lreportcard->returnArray($sql);
		if (sizeof($commentDetail) != 1) {
			header("Location:index.php");
		}
		$IsSubjectTeacherComment = (!empty($commentDetail[0]["SubjectID"]) && $commentDetail[0]["SubjectID"] != "0");
		
		// Get all Comment Code to prevent duplicate
		$sql = "SELECT CommentCode FROM $table WHERE CommentID != '$CommentID'";
		if ($IsSubjectTeacherComment) {
			$sql .= " AND SubjectID IS NOT NULL AND SubjectID != 0";
		}
		else {
			$sql .= " AND (SubjectID IS NULL OR SubjectID = 0) AND IsAdditional = '".$IsAdditional."' ";
		}
		$commentCodeList = $lreportcard->returnVector($sql);
		
		# Delete Comment Category & Display System Message
		$Result = "";
		$SpMessage = "";
		if (isset($DelCat) && $DelCat != "") {
			$DelCatStatus = $lreportcard->DELETE_COMMENT_CAT($DelCat);
			if ($DelCatStatus) {
				$SpMessage = $i_con_msg_delete_comment_cat_success;
			}
			else {
				$SpMessage = $i_con_msg_delete_comment_cat_failed;
			}
		}
		
		// Subject Selection
		$SubjectSelect = "";
		$SubjectOption = array();
		$SubjectArray = $lreportcard->GET_ALL_SUBJECTS(1);
		if (sizeof($SubjectArray) > 0) {
			$tempCount = 0;
			foreach ($SubjectArray as $tempSubjectID => $tempSubjectDetail) {
				$SubjectOption[$tempCount][0] = $tempSubjectID;
				$SubjectOption[$tempCount][1] = $tempSubjectDetail[0];
				$tempCount++;
			}
			$SelectedSubject = $IsSubjectTeacherComment? $commentDetail[0]["SubjectID"] : $SubjectOption[0][0];
			$SubjectSelect = $linterface->GET_SELECTION_BOX($SubjectOption, 'name="SubjectID" id="SubjectID" onchange="loadCommentCatList(this.options[this.selectedIndex].value)"', '', $SelectedSubject);
		}
		
		// Delete button for deleting item in Comment Category Selection
		$DelCatButtonDisabled = "";
		if (trim($commentDetail[0]["CommentCategory"]) === "") {
			$DelCatButtonDisabled = "disabled='disabled'";
		}
		$DelCatButton = $linterface->GET_SMALL_BTN($eReportCard['DeleteSelectedCategory'], "button", "jsDeleteCommentCat()", "DelCatButton", "id='DelCatButton' $DelCatButtonDisabled");
		
		# Comment Category Selection
		$CatSelect = "";
		$CatOption = array();
		$existCommentCat = $lreportcard->GET_COMMENT_CAT($SelectedSubject);
		if (sizeof($existCommentCat) > 0) {
			for($i=0; $i<sizeof($existCommentCat); $i++) {
				$CatOption[$i][0] = $existCommentCat[$i];
				$CatOption[$i][1] = $existCommentCat[$i];
			}
			$SelectedCat = ($commentDetail[0]["CommentCategory"] !== "") ? $commentDetail[0]["CommentCategory"] : $CatOption[0][0];
			$CatSelect = $linterface->GET_SELECTION_BOX($CatOption, 'name="CatSelect" id="CatSelect" onchange="" '.$DelCatButtonDisabled, '', $SelectedCat);
			
			$CatSelectDisplay = "";
		}
		else {
			$CatSelectDisplay = "none";
		}
		
		// Alternate sets of Comment Category, they will be used when users select different subjects  (stored as JS array)
		$OtherCommentCatSelect = array();
		if (sizeof($SubjectArray) > 0) {
			foreach ($SubjectArray as $tempSubjectID => $tempSubjectDetail) {
				$CatOption = array();
				$existCommentCat = $lreportcard->GET_COMMENT_CAT($tempSubjectID);
				if (sizeof($existCommentCat) > 0) {
					for($i=0; $i<sizeof($existCommentCat); $i++) {
						$CatOption[$i][0] = $existCommentCat[$i];
						$CatOption[$i][1] = $existCommentCat[$i];
					}
					$OtherCommentCatSelect[$tempSubjectID] = $linterface->GET_SELECTION_BOX($CatOption, 'name="CatSelect" id="CatSelect" onchange=""', '', $SelectedCat);
				}
			}
		}
		
		// Comment Category Input (for new category)
		$CatText = "<input onkeydown='return noEsc()' class='textboxnum' type='text' name='CatText' id='CatText' maxlength='64' value='' ".(($CatSelect == "")?"":"disabled='disabled'")." />";
		
		// Comment Type   (Hide Selection if Class Teacher's Comment)
		$SubjectRowStyle = "display:none;";
		$CategoryRowStyle = "display:none;";
		if ($IsSubjectTeacherComment) {
			$SubjectRowStyle = "";
			$CategoryRowStyle = "";
		}
		
		// Text limit
		//$TextLimit = $IsSubjectTeacherComment?$lreportcard->textAreaMaxChar_SubjectTeacherComment:$lreportcard->textAreaMaxChar;
		$TextLimit = $IsSubjectTeacherComment ? $lreportcard->Get_Subject_Teacher_Comment_MaxLength() : $lreportcard->Get_Class_Teacher_Comment_MaxLength();
		
		# [2018-1130-1440-05164] Related Grade Selection
        //if($eRCTemplateSetting['Management']['ClassTeacherComment']['RelatedGradeSettings'] && !$IsSubjectTeacherComment)
		if($eRCTemplateSetting['Management']['ClassTeacherComment']['RelatedGradeSettings'] && !empty($eRCTemplateSetting['Management']['ClassTeacherComment']['RelatedGradeList']) && !$IsSubjectTeacherComment)
		{
		    /*
		    $GradingSchemeInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO();
		    if(!empty($GradingSchemeInfo))
		    {
		        $GradingSchemeRangeInfo = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($GradingSchemeInfo[0]['SchemeID']);
		        
		        $GradingSchemeRangeArr = array();
		        $GradingSchemeRangeArr[] = array('', '---');
		        foreach((array)$GradingSchemeRangeInfo as $thisRangeInfo) {
		            $GradingSchemeRangeArr[] = array($thisRangeInfo['Grade'], $thisRangeInfo['Grade']);
		        }
		        $SelectedGrade = (isset($commentDetail[0]["RelatedGrade"]))? $commentDetail[0]["RelatedGrade"] : $GradingSchemeRangeArr[0][0];
		        $GradeSelectDisplay = $linterface->GET_SELECTION_BOX($GradingSchemeRangeArr, 'name="GradeSelect" id="GradeSelect" onchange=""', '', $SelectedGrade);
		    }
		    */

		    // [2019-0924-1029-57289] Comment Grade Selection
            $commentGradeArr = array();
            $commentGradeArr[] = array('', '---');
            foreach((array)$eRCTemplateSetting['Management']['ClassTeacherComment']['RelatedGradeList'] as $thisGrade) {
                $commentGradeArr[] = array($thisGrade, $thisGrade);
            }
            $SelectedGrade = (isset($commentDetail[0]["RelatedGrade"])) ? $commentDetail[0]["RelatedGrade"] : '';
            $GradeSelectDisplay = $linterface->GET_SELECTION_BOX($commentGradeArr, 'name="GradeSelect" id="GradeSelect" onchange=""', '', $SelectedGrade);
		}
		
		# Tag
		# $TAGS_OBJ[] = array($eReportCard['Settings_CommentBank']);
		$TAGS_OBJ = array();
        // [2020-0708-0950-12308]
        if ($eRCTemplateSetting['ClassTeacherComment_AdditionalComment_as_BilingualTeacher']) {
            $TAGS_OBJ[] = array($eReportCard['ManagementArr']['ClassTeacherCommentArr']['CommentCustDisplay'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/comment_bank/index.php?CommentType=class", (!$IsSubjectTeacherComment && $IsAdditional==0)?1:0);
        } else {
		    $TAGS_OBJ[] = array($eReportCard['Management_ClassTeacherComment'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/comment_bank/index.php?CommentType=class", (!$IsSubjectTeacherComment && $IsAdditional==0)?1:0);
        }
		if ($eRCTemplateSetting['ClassTeacherComment_AdditionalComment']) {
			$TAGS_OBJ[] = array($eReportCard['ManagementArr']['ClassTeacherCommentArr']['AdditionalComment'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/comment_bank/index.php?CommentType=class&IsAdditional=1", (!$IsSubjectTeacherComment && $IsAdditional==1)?1:0);
		}
		$TAGS_OBJ[] = array($eReportCard['Template']['SubjectTeacherComment'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/eReportCard/settings/comment_bank/index.php?CommentType=subject", ($IsSubjectTeacherComment)?1:0);
		
		# Page Navigation
		$PAGE_NAVIGATION[] = array($button_edit);

		$linterface->LAYOUT_START();
?>

<script type="text/JavaScript" language="JavaScript">
// Preload sets of Comment Category
var commentCatList = new Array();
<?php
	foreach($OtherCommentCatSelect as $key => $value) {
		$value = str_replace("\n", "", $value);
		$x .= "commentCatList[".$key."] = '$value';\n";
	}
	echo $x;
	
	for($i=0; $i<sizeof($commentCodeList); $i++) {
		$ccl .= "'".$commentCodeList[$i]."'";
		if ($i != sizeof($commentCodeList)-1) $ccl .= ",";
	}
?>
var commentCodeList = new Array(<?=$ccl?>);

Array.prototype.in_array = function(p_val) {
	for(var i = 0, l = this.length; i < l; i++) {
		if(this[i] == p_val) {
			return true;
		}
	}
	return false;
}

function loadCommentCatList(subjectID) {
	var CatSelectInputControl = document.getElementById("CatSelectInputControl");
	if (commentCatList[subjectID]) {
		document.getElementById("CatSelectSpan").innerHTML = commentCatList[subjectID];
		CatSelectInputControl.style.display = "";
		jsChangeCatInputMethod("0");
	} else {
		CatSelectInputControl.style.display = "none";
		jsChangeCatInputMethod("1");
	}
}

function resetForm() {
	document.form1.reset();
	changePicClass()
	return true;
}

function trim(str) {
	return str.replace(/^\s+|\s+$/g,"");
}

function jsChangeCatInputMethod(inputType) {
	if (inputType == "0") {
		document.getElementById("CatSelect").disabled = false;
		document.getElementById("DelCatButton").disabled = false;
		document.getElementById("CatSelect").focus();
		
		document.getElementById("CatText").disabled = true;
	} else if (inputType == "1") {
		document.getElementById("CatSelect").disabled = true;
		document.getElementById("DelCatButton").disabled = true;
		
		document.getElementById("CatText").disabled = false;
		document.getElementById("CatText").focus();
	}
}

function jsChangeCatCommentType(commentType) {
	var SubjectRow = document.getElementById("SubjectRow");
	var CategoryRow = document.getElementById("CategoryRow");
	
	if (commentType == "0") {
		SubjectRow.style.display = "none";
		CategoryRow.style.display = "none";
	} else if (commentType == "1") {
		SubjectRow.style.display = "";
		CategoryRow.style.display = "";
	}
}

function jsDeleteCommentCat() {
	var CatSelect = document.getElementById("CatSelect");
	var CatSelectedValue = CatSelect.options[CatSelect.selectedIndex].value;
	
	var SubjectSelect = document.getElementById("SubjectID");
	var SubjectSelectedValue = SubjectSelect.options[SubjectSelect.selectedIndex].value;
	
	var CommentID = document.getElementById("CommentID").value;
	var CommentType = document.getElementById("type").value;
	
	if (confirm("<?=$eReportCard['AlertDeleteSelectedCategory']?>")) {
		window.location = "edit.php?CommentID="+CommentID+"&DelCat="+CatSelectedValue;
	}
}

function checkForm() {
	obj = document.form1;
	if (trim(obj.elements["code"].value) == "") {
		 alert('<?=$eReportCard['AlertEnterCommentCode']?>');
		 obj.elements["code"].focus();
		 return false;
	}
	if (commentCodeList.in_array(trim(obj.elements["code"].value))) {
		alert('<?=$eReportCard['AlertDuplicateCommentCode']?>');
		obj.elements["code"].focus();
		return false;
	}
	/*
	if (document.getElementById("cat2").checked && trim(obj.elements["CatText"].value) == "") {
		 alert('<?=$eReportCard['AlertEnterCommentCategory']?>');
		 obj.elements["CatText"].focus();
		 return false;
	}
	*/
	if (trim(obj.elements["comment"].value) == "" && trim(obj.elements["commentEng"].value) == "") {
		 alert('<?=$eReportCard['AlertEnterCommentContent']?>');
		 obj.elements["commentEng"].focus();
		 return false;
	}
	
	obj.submit();
}
</script>

<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT."templates/2007a/css/"?>ereportcard.css">
<br />
<form name="form1" action="edit_update.php" method="POST" onsubmit="return checkForm();">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
		<td align="right"> <?=$linterface->GET_SYS_MSG($Result, $SpMessage);?></td>
	</tr>
	<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
<table>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<?php
				/*
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['Type'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="type" id="type1" value="0" <?=(!$IsSubjectTeacherComment)?"checked":""?> onclick="jsChangeCatCommentType(this.value)" />
						<label for="type1"><?=$eReportCard['ClassTeacher']?></label><br />
						<input type="radio" name="type" id="type2" value="1" <?=($IsSubjectTeacherComment)?"checked":""?> onclick="jsChangeCatCommentType(this.value)" />
						<label for="type2"><?=$eReportCard['SubjectTeacher']?></label><br />
					</td>
				</tr>
				*/
				?>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['Code'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input onkeydown="return noEsc()" class="textboxnum" type="text" name="code" maxlength="16" value="<?=$commentDetail[0]["CommentCode"]?>" />
					</td>
				</tr>
				<tr id="SubjectRow" style="<?=$SubjectRowStyle?>">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['Subject'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<?=$SubjectSelect?>
					</td>
				</tr>
				<tr id="CategoryRow" style="<?=$CategoryRowStyle?>">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['Category'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<span style="display:<?=$CatSelectDisplay?>" id="CatSelectInputControl">
							<input type="radio" name="cat" id="cat1" value="0" <?=$DelCatButtonDisabled===""?"checked='true'":""?> onclick="jsChangeCatInputMethod(this.value)" />
							<label for="cat1"></label>
							<span id="CatSelectSpan"><?=$CatSelect?></span> <?=$DelCatButton?><br />
							<input type="radio" name="cat" id="cat2" value="1" <?=$DelCatButtonDisabled===""?"":"checked='true'"?> onclick="jsChangeCatInputMethod(this.value)" />
							<label for="cat2"></label>
						</span>
						<?=$CatText?>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['CommentContentEng'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<textarea 
							class="textboxtext" name="commentEng" id="commentEng" 
							cols="70" rows="2" wrap="virtual" 
							onFocus="this.rows=5;" 
							onKeyDown="limitText(this,<?=$TextLimit?>);return noEsc();" 
							onKeyUp="limitText(this,<?=$TextLimit?>);"
							ondrop="setTimeout(function() { limitText(document.getElementById('commentEng'), <?=$TextLimit?>); }, 10);"
							onpaste="setTimeout(function() { limitText(document.getElementById('commentEng'), <?=$TextLimit?>); }, 10);"
							><?=$commentDetail[0]["CommentEng"]?></textarea>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['CommentContentChi'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<textarea 
							class="textboxtext" name="comment" id="comment" 
							cols="70" rows="2" wrap="virtual" 
							onFocus="this.rows=5;" 
							onKeyDown="limitText(this,<?=$TextLimit?>);return noEsc();" 
							onKeyUp="limitText(this,<?=$TextLimit?>);"
							ondrop="setTimeout(function() { limitText(document.getElementById('commentEng'), <?=$TextLimit?>); }, 10);"
							onpaste="setTimeout(function() { limitText(document.getElementById('comment'), <?=$TextLimit?>); }, 10);"
							><?=$commentDetail[0]["Comment"]?></textarea>
					</td>
				</tr>
				<?php if($eRCTemplateSetting['Management']['ClassTeacherComment']['RelatedGradeSettings'] && !$IsSubjectTeacherComment) { ?>
    				<tr id="GradeRow">
    					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
    						<?= $eReportCard['CommentRelatedGrade'] ?>
    					</td>
    					<td width="75%" class='tabletext'>
    						<?= $GradeSelectDisplay ?>
    					</td>
    				</tr>
				<?php } ?>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td class="dotline">
						<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
					</td>
				</tr>
				<tr>
					<td align="center">
						<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "")?>&nbsp;
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location = 'index.php?IsAdditional=".$IsAdditional."&CommentType=".($IsSubjectTeacherComment?"subject":"class")."'")?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" name="type" id="type" value="<?=$IsSubjectTeacherComment?"1":"0"?>" />
<input type="hidden" name="CommentID" id="CommentID" value="<?=$CommentID?>" />
<input type="hidden" name="IsAdditional" id="IsAdditional" value="<?=$IsAdditional?>" />
</form>

<?
	print $linterface->FOCUS_ON_LOAD("form1.code");
  	$linterface->LAYOUT_STOP();
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>