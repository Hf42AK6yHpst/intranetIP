<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
$lreportcard = new libreportcardcustom();
$lreportcard_ui = new libreportcard_ui();

$lreportcard->hasAccessRight();


$ScaleID = $_GET['ScaleIDArr'][0];
$ReturnMsgKey = $_GET['ReturnMsgKey'];

$linterface = new interface_html();
$CurrentPage = "PersonalCharacteristics";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ = $lreportcard_ui->Get_Settings_PersonalCharacteristics_Tag_Array('scale');

$ReturnMsg = $Lang['eReportCard']['PersonalCharArr']['ReturnMessageArr'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);
echo $linterface->Include_JS_CSS();
echo $lreportcard_ui->Get_Settings_PersonalCharacteristics_Scale_Step1_UI($ScaleID);
?>
<script language="javascript">
var jsWarmMsgArr = new Array();
jsWarmMsgArr["NameEn"] = "<?=$Lang['General']['JS_warning']['InputEnglishName']?>";
jsWarmMsgArr["NameCh"] = "<?=$Lang['General']['JS_warning']['InputChineseName']?>";
jsWarmMsgArr["Code"] = "<?=$Lang['General']['JS_warning']['InputCode']?>";
jsWarmMsgArr["CodeInUse"] = "<?=$Lang['General']['JS_warning']['CodeIsInUse']?>";

$(document).ready( function() {	
	$('input#Code').focus();
});

function js_Back_To_Scale_List() {
	window.location = 'scale.php';
}

function js_Update_Scale_Info() {
	var jsScaleID = $('input#ScaleID').val();
	var jsCode = $("input#Code").val().Trim();
	
	var jsFormValid = true;
	
	// check info empty
	$("input.required").each(function(){
		var thisID = $(this).attr("id");
		var WarnMsgDivID = thisID + 'WarningDiv';

		if($(this).val().Trim() == '') {
			$("div#" + WarnMsgDivID).html(jsWarmMsgArr[thisID]).show();
			$("input#" + thisID).focus();
			jsFormValid = false;
		}
		else {
			$("div#" + WarnMsgDivID).hide();
		}
	});
	
	
	// ajax check code
	if(jsCode != '') {
		if (jsCode.substr(0, 1) == '0')
		//if(valid_code(jsCode) == false)
		{
			$("div#CodeWarningDiv").html('<?=$Lang['General']['JS_warning']['CodeCannotStartWithZero']?>').show();
			$("input#Code").focus();
			jsFormValid = false;
		}
		else {
			// Check Code duplication
			$.ajax({
				url:      "ajax_validate.php",
				type:     "POST",
				data:     "Action=ScaleCode&Code=" + jsCode + "&ScaleID=" + jsScaleID,
				async:	  false,
				error:    function(xhr, ajaxOptions, thrownError){
							alert(xhr.responseText);
						  },
				success:  function(xml){
								if(xml==1) {
									$("div#CodeWarningDiv").hide();
								}
								else {
									$("div#CodeWarningDiv").html(jsWarmMsgArr["CodeInUse"]).show();
									$("input#Code").focus();
									jsFormValid = false;
								}
						  }
			});
		}
	}
	
	// Check default
	if ($('input#IsDefault').attr('checked')) {
		$.ajax({
			url:      "ajax_validate.php",
			type:     "POST",
			data:     "Action=HaveDefaultScale&ScaleID=" + jsScaleID,
			async:	  false,
			error:    function(xhr, ajaxOptions, thrownError){
						alert(xhr.responseText);
					  },
			success:  function(xml){
							if(xml==1) {
								$("div#IsDefaultWarningDiv").html('<?=$Lang['eReportCard']['GeneralArr']['WarningArr']['HaveScaleDefaultAlready']?>').show();
								$("input#IsDefault").focus();
								jsFormValid = false;
							}
							else {
								$("div#IsDefaultWarningDiv").hide();
							}
					  }
		});
	}
	
	
	if (jsFormValid) {
		$("form#form1").attr('action', 'scale_new_step1_update.php').submit();
	}
}
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>