<?php
$PATH_WRT_ROOT = "../../../../../../";
// $PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

$successArr['DeleteCharItem'] = $lreportcard->removePersonalCharItem($CharID);

# Delete Form setting records

# Delete Student records


intranet_closedb();
header("Location: index.php?filter=$filter&order=$order&field=$field&xmsg=delete&pageNo=$pageNo");

?>
