<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}
$li = new libdb();

$table = $lreportcard->DBName.".RC_LEVEL_SUBJECT_PERSONAL_CHARACTERISTICS";

if($applyothers)
{
	$MainSubjectArray = $lreportcard->returnSubjectwOrder($ClassLevelID);
	foreach($MainSubjectArray as $SubjAry[] => $temp[]);
	
	$SubjAry[] = 0;
}
else
{
	$SubjAry[] = $SubjectID;	
}

foreach($SubjAry as $k=>$SubjectID)
{
	# delete old records
	$sql = "DELETE FROM {$table} WHERE ClassLevelID=$ClassLevelID and SubjectID=$SubjectID";
	$li->db_db_query($sql);
	
	# insert records
	if(!empty($CharIDAry))
	{
		foreach($CharIDAry as $k => $CharID)
		{
			$sql = "insert into {$table} values ('', $ClassLevelID, $SubjectID, $CharID, NOW())";
			$li->db_db_query($sql);
		}
	}
}

intranet_closedb();
header("Location: subject_list.php?ClassLevelID=$ClassLevelID&xmsg=update");

?>