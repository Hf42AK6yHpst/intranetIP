<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_pc.php");
$lreportcard_pc = new libreportcard_pc();
$lreportcard_pc->hasAccessRight();


# Get data
$ScaleID = $_POST['ScaleID'];
$UserIDArr = $_POST['UserIDArr'];
$success = $lreportcard_pc->Delete_Personal_Characteristics_Scale_Mgmt_User($ScaleID, $UserIDArr);

$ReturnMsgKey = ($success)? "DeleteScaleMemberSuccess" : "DeleteScaleMemberFailed";

intranet_closedb();

header("Location: scale_member_list.php?ScaleID=$ScaleID&ReturnMsgKey=".$ReturnMsgKey);
?>