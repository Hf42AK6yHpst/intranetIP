<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();

$lreportcard_ui = new libreportcard_ui();
$linterface = new interface_html();
$lclass = new libclass();

$CurrentPage = "PersonalCharacteristics";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
//$TAGS_OBJ[] = array($eReportCard['PersonalCharacteristicsPool'], "index.php", 0);
//$TAGS_OBJ[] = array($eReportCard['PersonalCharacteristicsFormSettings'], "assign.php", 1);
$TAGS_OBJ = $lreportcard_ui->Get_Settings_PersonalCharacteristics_Tag_Array('formSettings');
$linterface->LAYOUT_START();

$LevelName = $lclass->getLevelName($ClassLevelID);

$PAGE_NAVIGATION[] = array($eReportCard['PersonalCharacteristics'] . " - " . $LevelName );

# Retrieve Data
$temp = $lreportcard->returnPersonalCharSettingData($ClassLevelID);
foreach($temp as $k=>$d)
{
	$data[$d['SubjectID']][] = $d['Title_EN'];
}

# Build the form
$x = "";
$MainSubjectArray = $lreportcard->returnSubjectwOrder($ClassLevelID);
$MainSubjectArray[0] = array();
if(!empty($MainSubjectArray))
{
	foreach($MainSubjectArray as $MainSubjectID => $MainSubjectNameArray)
	{
		if ($MainSubjectID == 0)
			$sname = $eReportCard['Template']['OverallCombined'];
		else
	 		$sname = $lreportcard->GET_SUBJECT_NAME_LANG($MainSubjectID);
	 	
	 	$chars = empty($data[$MainSubjectID]) ? $eReportCard['NoPersonalCharacteristicsSetting'] : implode(", ", $data[$MainSubjectID]);
		$x .= "<tr valign='top'>";
		$x .= "<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>$sname</span></td>";
 		$x .= "<td class='tabletext'><a href='javascript:editSubject($MainSubjectID);' class='tablelink'>". $chars ."</a></td>";
		$x .= "</tr>";
	}
}
else
{
	$x .= "<tr valign='top'>";
	$x .= "<td colspan='2' valign='top' nowrap='nowrap' class='tabletext' align='center'>". $eReportCard['NoSubjectSettings'] ."</td>";
	$x .= "</tr>";
}
?>

<script language="javascript">
<!--
function editSubject(sid)
{
	document.form1.SubjectID.value = sid;
	document.form1.submit();
}
//-->
</script>

<br />   
<form name="form1" method="post" action="assign_edit.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
			<td>
				<br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
					<?=$x?>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2">        
		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center">
				<? if(!empty($MainSubjectArray)) {?>
				<?//= $linterface->GET_ACTION_BTN($button_submit, "submit") ?>
				<?//= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2") ?>
				<? } ?>
				<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='assign.php'") ?>
			</td>
		</tr>
		</table>                                
	</td>
</tr>

</table>                        
<br />

<input type="hidden" name="ClassLevelID" value="<?=$ClassLevelID?>" />
<input type="hidden" name="SubjectID" value="" />
</form>


<?
print $linterface->FOCUS_ON_LOAD("form1.NoticeNumber");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>