<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

if(!$plugin['ReportCard2008'])
{
	intranet_closedb();
	header("Location: index.php");
}
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
$lreportcard_ui = new libreportcard_ui();

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$CurrentPage = "PersonalCharacteristics";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
//$TAGS_OBJ[] = array($eReportCard['PersonalCharacteristicsPool'], "index.php", 1);
//$TAGS_OBJ[] = array($eReportCard['PersonalCharacteristicsFormSettings'], "assign.php", 0);
$TAGS_OBJ = $lreportcard_ui->Get_Settings_PersonalCharacteristics_Tag_Array('pool');


$linterface->LAYOUT_START();
$sql = "
	select 
		CONCAT(\"<a href='edit.php?CharID=\", CharID ,\"' class='tablelink'>\", Title_". ($intranet_session_language=="en" ? "EN" : "CH") .", \"</a>\"),
		DisplayOrder,
		DateModified,
		CONCAT(\"<input type='checkbox' name='CharID[]' value='\", CharID,\"'>\")
	from 
		". $lreportcard->DBName .".RC_PERSONAL_CHARACTERISTICS 
";

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

# initialize sorting order of the table - default order is ASC
if ($order=="")
	$order = 1;
if ($field=="")
	$field = 1;
		
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("Title_".($intranet_session_language=="en" ? "EN" : "CH"), "DisplayOrder", "DateModified");

$li->sql = $sql;
$li->no_col = count($li->field_array) + 1;
$li->IsColOff = "PersonalCharacteristicsList";

$pos = 0;
$li->column_list .= "<td class='tabletop tabletopnolink' width='50%'>".$li->column($pos++, $eReportCard['PersonalCharacteristics'])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink' width='25%'>".$li->column($pos++, $eReportCard['DisplayOrder'])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink' width='25%'>".$li->column($pos++, $eReportCard['LastModifiedDate'])."</td>\n";
$li->column_list .= "<td width=1 class='tabletoplink'>".$li->check("CharID[]")."</td>\n";

$AddBtn = "<a href=\"javascript:newPC();\" class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_new . "</a>";
// $delBtn	= "<a href=\"javascript:checkRemoveThis(document.form1,'CharID[]','remove.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_remove . "</a>";
$delBtn = "<a href=\"javascript:checkRemove(document.form1,'CharID[]','remove.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_remove . "</a>";
$editBtn= "<a href=\"javascript:checkEdit(document.form1,'CharID[]','edit.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";

?>
<script language="javascript">
<!--
function newPC()
{
	with(document.form1)
	{
	    action = "new.php";
	    submit();
	}
}
//-->
</script>

<br />
<form name="form1" method="get">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
		<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<table border="0" cellspacing="0" cellpadding="2">
						<tr>
							<td><p><?=$AddBtn?></p></td>
						</tr>
						</table>
                    </td>
					<td align="right" height='30'><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
				</tr>
				<tr class="table-action-bar">
					<td align="left" valign="bottom"><?=$noticeTypeSelect?><?=$status_select?><?=$select_year?><?=$select_month?></td>
					<td align="right" valign="bottom" height="28">
				        <table border="0" cellspacing="0" cellpadding="0">
				        <tr>
				                <td width="21"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_01.gif" width="21" height="23" /></td>
				                <td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_02.gif">
			                        <table border="0" cellspacing="0" cellpadding="2">
									<tr>
										<td nowrap><?=$editBtn?></td>
	                                    <td><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="5"></td>
	                                    <td nowrap><?=$delBtn?></td>
									</tr>
    								</table>
				                </td>
				                <td width="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_03.gif" width="6" height="23" /></td>
				        </tr>
				        </table>
					</td>
	                </tr>
				<tr>
					<td colspan="2"><?=$li->display();?></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<br />

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="1">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">

</form>



<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>