<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_pc.php");
$lreportcard_pc = new libreportcard_pc();
$lreportcard_pc->hasAccessRight();

$ScaleID = $_POST['ScaleID'];
$DataArr['Code'] = trim(stripslashes($_POST['Code']));
$DataArr['NameEn'] = trim(stripslashes($_POST['NameEn']));
$DataArr['NameCh'] = trim(stripslashes($_POST['NameCh']));
$DataArr['IsDefault'] = $_POST['IsDefault'];

$lreportcard_pc->Start_Trans();
if ($ScaleID == '') {
	$DataArr['DisplayOrder'] = $lreportcard_pc->Get_Scale_Max_DisplayOrder() + 1;
	$ScaleID = $lreportcard_pc->Insert_Scale_Record($DataArr);
	
	if ($ScaleID == 0)	{
		// failed to add
		$lreportcard_pc->RollBack_Trans();
		$ReturnMsgKey = 'AddScaleFailed';
		$NextPage = 'scale.php';
	}
	else {
		// Go to Step 2 to add member into the group
		$lreportcard_pc->Commit_Trans();
		$ReturnMsgKey = 'AddScaleSuccess';
		$NextPage = 'scale_new_step2.php';
		$FromNew = 1;
	}
}
else {
	$success = $lreportcard_pc->Update_Scale_Record($ScaleID, $DataArr);
	
	if ($success) {
		$lreportcard_pc->Commit_Trans();
		$ReturnMsgKey = 'EditScaleSuccess';
	}
	else {
		$lreportcard_pc->RollBack_Trans();
		$ReturnMsgKey = 'EditScaleFailed';
	}
	$NextPage = 'scale.php';
}
intranet_closedb();

$para = "ScaleID=$ScaleID&ReturnMsgKey=$ReturnMsgKey&FromNew=$FromNew";
header("Location: $NextPage?$para");
?>