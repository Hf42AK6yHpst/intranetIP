<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_pc.php");
$lreportcard_pc = new libreportcard_pc();
$lreportcard_pc->hasAccessRight();

# Get data
$ScaleID = $_POST['ScaleID'];
$SelectedUserIDArr = $_POST['SelectedUserIDArr'];
$FromNew = $_POST['FromNew'];

$Success = $lreportcard_pc->Add_Personal_Characteristics_Scale_Mgmt_Member($ScaleID, $SelectedUserIDArr);

intranet_closedb();

$NextPage = ($FromNew)? "scale.php" : "scale_member_list.php";
$ReturnMsgKey = ($Success)? 'AddScaleMemberSuccess' : 'AddScaleMemberFailed';

$para = "ScaleID=$ScaleID&ReturnMsgKey=$ReturnMsgKey";
header("Location: ".$NextPage.'?'.$para);
?>