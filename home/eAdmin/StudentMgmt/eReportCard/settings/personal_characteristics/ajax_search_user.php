<?php
// using ivan
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_pc.php");

$lreportcard_pc = new libreportcard_pc();
$lreportcard_pc->hasAccessRight();


# Get data
$SelectedUserIDList = $_REQUEST['SelectedUserIDList'];
$ScaleID = $_GET['ScaleID'];
$SearchValue = trim(stripslashes(urldecode($_GET['q'])));
$TargetUserType = USERTYPE_STAFF;

### Get Select User and exclude them in the search result
$SelectedUserArr = explode(',', $SelectedUserIDList);
$ExcludeUserIDArr = Get_User_Array_From_Common_Choose($SelectedUserArr, $TargetUserType);

### Get User who can be added to the Admin Group
$AvailableUserInfoArr = $lreportcard_pc->Get_Personal_Characteristics_Scale_Mgmt_Available_User($ScaleID, $SearchValue, $ExcludeUserIDArr, $TargetUserType);
$numOfAvailableUser = count($AvailableUserInfoArr);

$returnString = '';
for ($i=0; $i<$numOfAvailableUser; $i++)
{
	$thisUserID = $AvailableUserInfoArr[$i]['UserID'];
	$thisUserLogin = $AvailableUserInfoArr[$i]['UserLogin'];
	$thisUserName = $AvailableUserInfoArr[$i]['UserName'];
	
	$returnString .= $thisUserName.'|'.$thisUserLogin.'|'.$thisUserID."\n";
}

intranet_closedb();

echo $returnString;
?>