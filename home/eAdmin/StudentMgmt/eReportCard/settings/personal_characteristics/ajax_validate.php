<?php
###################################################
# 	Change Log
#
#	20141023 - Ryan:  return true if 
#				     Editing Scale = Default Scale
#
#
#
###################################################
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_pc.php");
$lreportcard_pc = new libreportcard_pc();
$lreportcard_pc->hasAccessRight();

$Action = $_REQUEST['Action'];
if ($Action == 'ScaleCode') {
	$ScaleID = $_POST['ScaleID'];
	$Code = trim(stripslashes($_POST['Code']));
	$isValid = $lreportcard_pc->Is_Scale_Code_Valid($Code, $ScaleID);
	 
	echo ($isValid)? '1' : '0'; 
}
else if ($Action == 'HaveDefaultScale') {
	$ScaleID = $_POST['ScaleID'];
	$DefaultScaleArr = $lreportcard_pc->Get_Personal_Characteristics_Scale($ScaleIDArr='', $CodeArr='', $IsDefault=1);
	if (count($DefaultScaleArr) > 0){
		if($DefaultScaleArr[0]['ScaleID'] == $ScaleID){
			// editing Scale = default scale
			$NotAbleToSave = 0;
		}else{
			// editing Scale != default scale, not able to save
			$NotAbleToSave = 1;
		}
	}else{
		// no default is set 
		$NotAbleToSave =0;
	}
	echo $NotAbleToSave;
}
else if ($Action == 'Validate_Selected_Member') {
	$ScaleID = $_POST['ScaleID'];
	$SelectedUserList = stripslashes($_REQUEST['SelectedUserList']);
	echo $lreportcard_pc->Get_Invalid_Scale_Mgmt_Member_Selection($ScaleID, $SelectedUserList); 
}
else if ($Action == 'Is_Scale_Linked_Record') {
	$ScaleIDList = $_POST['ScaleIDList'];
	$ScaleIDArr = explode(',', $ScaleIDList);
	echo ($lreportcard_pc->Is_Scale_Linked_Data($ScaleIDArr))? '1' : '0';
}

intranet_closedb();
?>