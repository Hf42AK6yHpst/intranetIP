<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_teacherExtraInfo.php");

$lreportcard = new libreportcardcustom();
$lreportcard_teacherExtraInfo = new libreportcard_teacherExtraInfo();
$lreportcard->hasAccessRight();


### Get parameters
$userIdAry = $_POST['userIdAry'];
$actionType = $_POST['actionType'];


$successAry = array();
$lreportcard->Start_Trans();


### Delete data
$extraInfoAry = $lreportcard_teacherExtraInfo->returnInfoByTeacherId($userIdAry);
$numOfExtraInfo = count($extraInfoAry);
for ($i=0; $i<$numOfExtraInfo; $i++) {
	$_recordId = $extraInfoAry[$i]['RecordID'];
	$_extraInfoObj = new libreportcard_teacherExtraInfo($_recordId);
	
	if ($actionType == 'all') {
		$_extraInfoObj->setTeacherTitle('');
	}
	if ($actionType == 'all' || $actionType == 'signature') {
		$successAry[$_recordId]['deleteSignatureImage'] = $_extraInfoObj->deleteSignatureImage();
	}
	if ($actionType == 'all' || $actionType == 'schoolChop') {
		$successAry[$_recordId]['deleteSchoolChopImage'] = $_extraInfoObj->deleteSchoolChopImage();
	}
	
	$successAry[$_recordId]['save'] = $_extraInfoObj->save();
}


if (in_array(false, $successAry)) {
	$lreportcard->Rollback_Trans();
	$returnMsgKey = 'UpdateUnSuccess';
}
else {
	$lreportcard->Commit_Trans();
	$returnMsgKey = 'UpdateSuccess';
}

intranet_closedb();
header("Location: index.php?returnMsgKey=$returnMsgKey");
?>