<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

$lreportcard = new libreportcardcustom();
$linterface = new interface_html();
$lfs = new libfilesystem();


$lreportcard->hasAccessRight();


### Get parameters
$actionType = $_POST['actionType'];
if ($actionType == 'signature') {
	$actionTypeTitle = $Lang['AccountMgmt']['Signature'];	
}
else if ($actionType == 'schoolChop') {
	$actionTypeTitle = $eReportCard['Template']['SchoolChop'];
}
else {
	No_Access_Right_Pop_Up();
}


$CurrentPage = "Settings_TeacherExtraInfo";	
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($eReportCard['Settings_TeacherExtraInfo']);
$linterface->LAYOUT_START();


### Navigation
$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($eReportCard['Settings_TeacherExtraInfo'], "javascript:goBack();");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Upload'], "");
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);


### Step
$STEPS_OBJ[] = array($Lang['AccountMgmt']['UploadPhoto']['StepArr'][0], 0);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['CSVConfirmation'], 1);
$STEPS_OBJ[] = array($Lang['AccountMgmt']['UploadPhoto']['StepArr'][1], 0);
$htmlAry['step'] = $linterface->GET_STEPS($STEPS_OBJ);


### Process data
$successAry = array();

$fileName = $_FILES['uploadFile']['name'];
$tmpFilePath = $_FILES['uploadFile']['tmp_name'];
$fileExt = strtolower($lfs->file_ext($fileName));
$imageExtAry = array('.gif', '.jpg', '.jpeg', '.png', '.bmp');


### Create temp folder
$curDateTime = date('Ymd_His');
$userPhotoPath = $intranet_root.'/file/reportcard2008/'.$lreportcard->schoolYear;
$tmpFolder = $userPhotoPath.'/tmp';
if (!file_exists($tmpFolder)) {
	$successAry['createTempFolder'] = $lfs->folder_new($tmpFolder);
}
$tmpFolder .= '/'.$_SESSION['UserID'];
if (file_exists($tmpFolder)) {
	$successAry['clearTempFolder'] = $lfs->folder_remove_recursive($tmpFolder);
}
$successAry['createUserTempFolder'] = $lfs->folder_new($tmpFolder);
$tmpFolder .= '/'.$curDateTime;
$successAry['createUserDateTimeTempFolder'] = $lfs->folder_new($tmpFolder);


### Copy images to temp folder
$tmpFile = $tmpFolder.'/'.$fileName;
if($fileExt==".zip") {
	# copy to tmp folder first
	$successAry['copyZipFileToTempFolder'] = $lfs->lfs_copy($tmpFilePath, $tmpFile);
	
	if($lfs->file_unzip($tmpFile, $tmpFolder)) {
		$lfs->lfs_remove($tmpFile);
		$tmpFileList = $lfs->return_folderlist($tmpFolder, $clearRS=true);
	}
}
else if(in_array($fileExt, $imageExtAry)) {
	# copy to tmp folder first
	$successAry['copyFileToTempFolder'] = $lfs->lfs_copy($tmpFilePath, $tmpFile);
	$tmpFileList = $lfs->return_folderlist($tmpFolder, $clearRS=true);
}
$numOfFile = count($tmpFileList);


### Build result table
$successCount = 0;
$failedCount = 0;
$x = '';
$x .= '<table class="common_table_list_v30">'."\n";
	$x .= '<thead>'."\n";
		$x .= '<tr>'."\n";
			$x .= '<th style="width:30px;">#</th>'."\n";
			$x .= '<th style="width:20%;">'.$Lang['AccountMgmt']['LoginID'].'</th>'."\n";
			$x .= '<th>'.$Lang['AccountMgmt']['UserName'].'</th>'."\n";
			$x .= '<th style="width:300px;">'.$actionTypeTitle.'</th>'."\n";
			$x .= '<th style="width:20%;">'.$Lang['General']['Remark'].'</th>'."\n";
		$x .= '</tr>'."\n";
	$x .= '</thead>'."\n";
	$x .= '<tbody>'."\n";
	for ($i=0; $i<$numOfFile; $i++) {
		$_filePath = $tmpFileList[$i];
		$_imageFilePath = str_replace($intranet_root, '', $_filePath);
		
		$_fileBaseName = get_file_basename($_filePath);
		$_fileExt = get_file_ext($_fileBaseName);
		$_userLogin = str_replace($_fileExt, '', $_fileBaseName);
		
		$_userObj = new libuser('', $_userLogin);
		$_teacherName = Get_Lang_Selection($_userObj->ChineseName, $_userObj->EnglishName);
		
		$_remarkAry = array();
		if ($_userObj->UserID == '') {
			// User not found
			$failedCount++;
			$_userLogin = '<span class="tabletextrequire">'.$_userLogin.'</span>';
			$_teacherName = $Lang['General']['EmptySymbol'];
			$_remarkAry[] = 'user login not found';
		}
		else {
			$successCount++;
		}
		$_remarkDisplay = (count($_remarkAry) > 0)? implode('<br />', $_remarkAry) : '&nbsp;';
		
		$x .= '<tr>'."\n";
			$x .= '<td>'.($i + 1).'</td>'."\n";
			$x .= '<td>'.$_userLogin.'</td>'."\n";
			$x .= '<td>'.$_teacherName.'</td>'."\n";
			$x .= '<td><img src="'.$_imageFilePath.'" style="width:100%;" /></td>'."\n";
			$x .= '<td>'.$_remarkDisplay.'</td>'."\n";
		$x .= '</tr>'."\n";
	}
	$x .= '</tbody>'."\n";
$x .= '</table>'."\n";
$htmlAry['contentTable'] = $x;


### Info Table
$failedCountDisplay = ($failedCount > 0)? '<span class="tabletextrequire">'.$failedCount.'</span>' : $failedCount;
$x = '';
$x .= '<table class="form_table_v30">'."\n";
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['SuccessfulRecord'].'</td>'."\n";
		$x .= '<td>'.$successCount.'</td>'."\n";
	$x .= '</tr>'."\n";
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['FailureRecord'].'</td>'."\n";
		$x .= '<td>'.$failedCountDisplay.'</td>'."\n";
	$x .= '</tr>'."\n";
$x .= '</table>'."\n";
$htmlAry['infoTable'] = $x;


### Buttons
$disabled = ($failedCount > 0)? true : false;
$htmlAry['submitBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "goSubmit();", '', '', $disabled);
$htmlAry['backBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "goBack();");
$htmlAry['cancelBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "goCancel();");

?>
<script language="javascript">
function goBack() {
	window.location = 'upload.php?actionType=' + $('input#actionType').val();
}

function goCancel() {
	window.location = 'index.php';
}

function goSubmit() {
	$('form#form1').attr('action', 'upload_update.php').submit();
}
</script>
<br />
<form id="form1" name="form1" method="post">
	<?=$htmlAry['navigation']?>
	<br />
	
	<?=$htmlAry['step']?>
	<br />
	
	<?=$htmlAry['infoTable']?>
	<br />
	
	<?=$htmlAry['contentTable']?>
	<br />
	
	<div class="edit_bottom_v30">
		<?=$htmlAry['submitBtn']?>
		<?=$htmlAry['backBtn']?>
		<?=$htmlAry['cancelBtn']?>
	</div>
	<br />
	
	<input type="hidden" id="actionType" name="actionType" value="<?=$actionType?>" />
	<input type="hidden" id="tf" name="tf" value="<?=getEncryptedText($tmpFolder)?>" />
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>