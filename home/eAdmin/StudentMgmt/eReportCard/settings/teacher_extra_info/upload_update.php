<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_teacherExtraInfo.php");

$lreportcard = new libreportcardcustom();
$lreportcard_teacherExtraInfo = new libreportcard_teacherExtraInfo();
$linterface = new interface_html();
$lfs = new libfilesystem();


$lreportcard->hasAccessRight();


### Get parameters
$actionType = $_POST['actionType'];
$tempFolderPath = getDecryptedText($_POST['tf']);


if ($actionType == 'signature') {
	$actionTypeTitle = $Lang['AccountMgmt']['Signature'];	
}
else if ($actionType == 'schoolChop') {
	$actionTypeTitle = $eReportCard['Template']['SchoolChop'];
}
else {
	No_Access_Right_Pop_Up();
}


$CurrentPage = "Settings_TeacherExtraInfo";	
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($eReportCard['Settings_TeacherExtraInfo']);
$linterface->LAYOUT_START();


### Navigation
$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($eReportCard['Settings_TeacherExtraInfo'], "javascript:goBack();");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Upload'], "");
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);


### Step
$STEPS_OBJ[] = array($Lang['AccountMgmt']['UploadPhoto']['StepArr'][0], 0);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['CSVConfirmation'], 0);
$STEPS_OBJ[] = array($Lang['AccountMgmt']['UploadPhoto']['StepArr'][1], 1);
$htmlAry['step'] = $linterface->GET_STEPS($STEPS_OBJ);


### Process data
$tmpFileList = $lfs->return_folderlist($tempFolderPath, $clearRS=true);
$numOfFile = count($tmpFileList);

$successAry = array();
$successCount = 0;
for ($i=0; $i<$numOfFile; $i++) {
	$_filePath = $tmpFileList[$i];
	$_fileBaseName = get_file_basename($_filePath);
	$_fileExt = get_file_ext($_fileBaseName);
	$_userLogin = str_replace($_fileExt, '', $_fileBaseName);
	
	$_userObj = new libuser('', $_userLogin);
	$_userId = $_userObj->UserID;
	
	$_extraInfoAry = $lreportcard_teacherExtraInfo->returnInfoByTeacherId($_userId);
	$_recordId = $_extraInfoAry[0]['RecordID'];
	
	$_extraInfoObj = new libreportcard_teacherExtraInfo($_recordId);
	$_extraInfoObj->setUserId($_userId);	// if the user does not have db record => no userId in the object => cannot construct correct image name
	if ($actionType == 'signature') {
		$successAry[$_fileBaseName]['deleteSignature'] = $_extraInfoObj->deleteSignatureImage();
		$successAry[$_fileBaseName]['uploadSignature'] = $_extraInfoObj->uploadSignatureImage($_filePath, $_fileBaseName);
	}
	else if ($actionType == 'schoolChop') {
		$successAry[$_fileBaseName]['deleteSchoolChop'] = $_extraInfoObj->deleteSchoolChopImage();
		$successAry[$_fileBaseName]['uploadSchoolChop'] = $_extraInfoObj->uploadSchoolChopImage($_filePath, $_fileBaseName);
	}
	
	if (!in_array(false, $successAry[$_fileBaseName])) {
		$successCount++;
	}
}

$htmlAry['successMsg'] = str_replace('<!--NumOfImage-->', $successCount, $Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['ImagesUploadedSuccessfully']); 



### Buttons
$htmlAry['backToExtraInfoBtn'] = $linterface->GET_ACTION_BTN($Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['BackToTeacherExtraInfo'], "button", "goCancel();");

?>
<script language="javascript">
function goCancel() {
	window.location = 'index.php';
}
</script>
<br />
<?=$htmlAry['navigation']?>
<br />

<?=$htmlAry['step']?>
<br />
<br />

<div style="width:100%; text-align:center;"><?=$htmlAry['successMsg']?></div>
<br />

<?=$htmlAry['contentTable']?>
<br />

<div class="edit_bottom_v30">
	<?=$htmlAry['backToExtraInfoBtn']?>
</div>
<br />

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>