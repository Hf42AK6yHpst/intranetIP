<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_teacherExtraInfo.php");

$lreportcard = new libreportcardcustom();
$linterface = new interface_html();

$lreportcard->hasAccessRight();

$actionType = $_GET['actionType'];
if ($actionType != 'signature' && $actionType != 'schoolChop') {
	No_Access_Right_Pop_Up();
}


$CurrentPage = "Settings_TeacherExtraInfo";	
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($eReportCard['Settings_TeacherExtraInfo']);
$linterface->LAYOUT_START();


### Navigation
$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($eReportCard['Settings_TeacherExtraInfo'], "javascript:goBack();");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Upload'], "");
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);


### Step
$STEPS_OBJ[] = array($Lang['AccountMgmt']['UploadPhoto']['StepArr'][0], 1);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['CSVConfirmation'], 0);
$STEPS_OBJ[] = array($Lang['AccountMgmt']['UploadPhoto']['StepArr'][1], 0);
$htmlAry['step'] = $linterface->GET_STEPS($STEPS_OBJ);


$fileRemarksAry = $Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['UploadFileRemarksArr'];
$numOfRemarks = count($fileRemarksAry);
$fileRemarks = '';
for($i=0; $i<$numOfRemarks; $i++) {
	$fileRemarks .= ($i + 1).'.&nbsp;&nbsp;'.$fileRemarksAry[$i]."<br>";
}
	

$x = '';
$x .= '<table class="form_table_v30">'."\n";
	// File
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['File'].'</td>'."\n";
		$x .= '<td>'."\n";
			$x .= '<input type="file" id="uploadFile" name="uploadFile" />'."\n";
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
	// Remarks
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['Remark'].'</td>'."\n";
		$x .= '<td>'."\n";
			$x .= $fileRemarks."\n";
		$x .= '</td>'."\n";
	$x .= '</tr>'."\n";
$x .= '</table>'."\n";
$htmlAry['editTable'] = $x;


### Buttons
$htmlAry['submitBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "goSubmit();");
$htmlAry['cancelBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "goBack();");

?>
<script language="javascript">
function goBack() {
	window.location = 'index.php';
}

function goSubmit() {
	$('form#form1').attr('action', 'upload_confirm.php').submit();
}
</script>
<br />
<form id="form1" name="form1" method="post" enctype="multipart/form-data">
	<?=$htmlAry['navigation']?>
	<br />
	
	<?=$htmlAry['step']?>
	<br />
	
	<?=$htmlAry['editTable']?>
	<br />
	
	<div class="edit_bottom_v30">
		<?=$htmlAry['submitBtn']?>
		<?=$htmlAry['cancelBtn']?>
	</div>
	<br />
	
	<input type="hidden" id="actionType" name="actionType" value="<?=$actionType?>" />
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>