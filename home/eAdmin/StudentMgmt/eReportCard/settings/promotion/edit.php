<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'] && $sys_custom['eRC']['Settings']['PromotionCriteria'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	$TAGS_OBJ[] = array($eReportCard['Settings_PromotionCriteria'], "");
	$CurrentPage = "PromotionCriteria";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	
	if ($lreportcard->hasAccessRight()) {
    
    # Get class level of school
		$FormArr = $lreportcard->GET_ALL_FORMS();
		$ClassLevelID = (isset($ClassLevelID) && $ClassLevelID != "")? $ClassLevelID : $FormArr[0]['ClassLevelID'];
		if (sizeof($FormArr) > 0) {
			for($i=0; $i<sizeof($FormArr); $i++) {
			  if($FormArr[$i]["ClassLevelID"] == $ClassLevelID){
          $LevelName = $FormArr[$i]["LevelName"];
          break;
        }
			}
		}
    $MaxfailGroup = 3;
    $SubjectOption_MAX = 10;
    $AssignmentOption_MAX = 100;
		
		# Load Promotion Criteria
		$PromotionCriteria = $lreportcard->getPromotionCriteria($ClassLevelID);
		$RecordID = $PromotionCriteria[0]['PromotionCriteriaID']; 		
		$pass_average = $PromotionCriteria[0]['PassGrandAverage']; 
		$submit_ass = $PromotionCriteria[0]['NonSubmissionAssignment']; 
		$SubmitAss_MAX = $PromotionCriteria[0]['MaxAssignment']; 
		
		$FailSubject_MAX1 = $PromotionCriteria[0]['MaxFailNumOfSubject1'];
    $FailSubject_MAX2 = $PromotionCriteria[0]['MaxFailNumOfSubject2'];
    $FailSubject_MAX3 = $PromotionCriteria[0]['MaxFailNumOfSubject3']; 
		$SelectedSubject1 = $PromotionCriteria[0]['SubjectList1']; 
		$SelectedSubjectArr1 = trim($SelectedSubject1)? explode(",", $SelectedSubject1) : array();
		$SelectedSubject2 = $PromotionCriteria[0]['SubjectList2']; 
		$SelectedSubjectArr2 = trim($SelectedSubject2)? explode(",", $SelectedSubject2) : array();
		$SelectedSubject3 = $PromotionCriteria[0]['SubjectList3'];
		$SelectedSubjectArr3 = trim($SelectedSubject3)? explode(",", $SelectedSubject3) : array();
    
    # build select option object
    for($i=0;$i<=$SubjectOption_MAX;$i++)
    	$data[] = array($i, $i);	  
	  for($i=1;$i<=$AssignmentOption_MAX;$i++)
    	$data2[] = array($i, $i);
    
    # get subject list  
    $SubjectList = $lreportcard->returnSubjectwOrderNoL($ClassLevelID, 1, 1);
    $checkGroup1 = (count($SelectedSubjectArr1)>0)? " checked ":"";
    $checkGroup2 = (count($SelectedSubjectArr2)>0)? " checked ":"";
    $checkGroup3 = (count($SelectedSubjectArr3)>0)? " checked ":"";
    
    $disableBtn1 = (count($SelectedSubjectArr1)>0)? "":" disabled ";
    $disableBtn2 = (count($SelectedSubjectArr2)>0)? "":" disabled ";
    $disableBtn3 = (count($SelectedSubjectArr3)>0)? "":" disabled ";
    ##############################################
		# Selected Subject Selection
		// 1st Group
		$SubjectSelection1 = "";
		$SubjectSelection1 .= '<select name="SubjectSelected1[]" id="SubjectSelected1[]" size="10" style="width:99%" multiple="true">';
		foreach($SubjectList as $sid=>$subjname){
      if(in_array($sid, $SelectedSubjectArr1) && !in_array($sid, $SelectedSubjectArr2) && !in_array($sid, $SelectedSubjectArr3)){
        $SubjectSelection1 .= '<option value="'.$sid.'">'.$subjname.'</option>';
      }
    }
		$SubjectSelection1 .= '</select>';
		// 2nd Group
		$SubjectSelection2 = "";
		$SubjectSelection2 .= '<select name="SubjectSelected2[]" id="SubjectSelected2[]" size="10" style="width:99%" multiple="true">';
		foreach($SubjectList as $sid=>$subjname){
		  if(in_array($sid, $SelectedSubjectArr2) && !in_array($sid, $SelectedSubjectArr1) && !in_array($sid, $SelectedSubjectArr3)){
        $SubjectSelection2 .= '<option value="'.$sid.'">'.$subjname.'</option>';
			}
		}
		$SubjectSelection2 .= '</select>';
		// 3rd Group
		$SubjectSelection3 = "";
		$SubjectSelection3 .= '<select name="SubjectSelected3[]" id="SubjectSelected3[]" size="10" style="width:99%" multiple="true">';
		foreach($SubjectList as $sid=>$subjname){
		  if(in_array($sid, $SelectedSubjectArr3) && !in_array($sid, $SelectedSubjectArr1) && !in_array($sid, $SelectedSubjectArr2)){
        $SubjectSelection3 .= '<option value="'.$sid.'">'.$subjname.'</option>';
			}
		}
		$SubjectSelection3 .= '</select>';
		
		##############################################
		# Non-Selected Subject Selection
		// 1st Group
		$NSubjectSelection1 = "";
		$NSubjectSelection1 .= '<select name="Subject1" id="Subject1" size="10" style="width:99%" multiple="true">';
		foreach($SubjectList as $sid=>$subjname){
		  if(in_array($sid, $SelectedSubjectArr1) || in_array($sid, $SelectedSubjectArr2) || in_array($sid, $SelectedSubjectArr3)) 
        continue;
			$NSubjectSelection1 .= '<option value="'.$sid.'">'.$subjname.'</option>';
		}
		$NSubjectSelection1 .= '</select>';
		// 2nd Group
		$NSubjectSelection2 = "";
		$NSubjectSelection2 .= '<select name="Subject2" id="Subject2" size="10" style="width:99%" multiple="true">';
		foreach($SubjectList as $sid=>$subjname){
		  if(in_array($sid, $SelectedSubjectArr2) || in_array($sid, $SelectedSubjectArr1) || in_array($sid, $SelectedSubjectArr3)) 
        continue;
			$NSubjectSelection2 .= '<option value="'.$sid.'">'.$subjname.'</option>';
		}
		$NSubjectSelection2 .= '</select>';
		// 3rd Group
		$NSubjectSelection3 = "";
		$NSubjectSelection3 .= '<select name="Subject3" id="Subject3" size="10" style="width:99%" multiple="true">';
		foreach($SubjectList as $sid=>$subjname){
		  if(in_array($sid, $SelectedSubjectArr3) || in_array($sid, $SelectedSubjectArr1) || in_array($sid, $SelectedSubjectArr2)) 
        continue;
			$NSubjectSelection3 .= '<option value="'.$sid.'">'.$subjname.'</option>';
		}
		$NSubjectSelection3 .= '</select>';
    	
		############## Interface Start ##############
		$linterface = new interface_html();


		# page navigation (leave the array empty if no need)
		$PAGE_NAVIGATION[] = array($eReportCard['PromotionCriteriaSettings']);
		$PAGE_NAVIGATION[] = array($LevelName);
    
		$linterface->LAYOUT_START();
?>
<script type="text/JavaScript" language="JavaScript">
var NumOfgroup = '<?=$MaxfailGroup?>';
function Add_All_Subject(index) {
  var Subjects = document.getElementById('Subject'+index);
  var SubjectSelected = document.getElementById('SubjectSelected'+index+'[]');
  
  for (var i = (Subjects.length -1); i >= 0 ; i--) {
   SubjOption = Subjects.options[i];
   SubjectSelected.options[SubjectSelected.length] = new Option(SubjOption.text,SubjOption.value);
   Subjects.options[i] = null;
   AddRemoveSubject(index, SubjOption, 1);
  }

}

function Add_Selected_Subject(index){
  var Subjects = document.getElementById('Subject'+index);
  var SubjectSelected = document.getElementById('SubjectSelected'+index+'[]');
  
  for (var i = (Subjects.length -1); i >= 0 ; i--) {
    if (Subjects.options[i].selected) {
      SubjOption = Subjects.options[i];
      SubjectSelected.options[SubjectSelected.length] = new Option(SubjOption.text,SubjOption.value);
      Subjects.options[i] = null;
      AddRemoveSubject(index, SubjOption, 1);
    }
  }
}

function Remove_Selected_Subject(index){
  var Subjects = document.getElementById('Subject'+index);
  var SubjectSelected = document.getElementById('SubjectSelected'+index+'[]');

  for (var i = (SubjectSelected.length -1); i >= 0 ; i--) {
    if (SubjectSelected.options[i].selected){
      SubjOption = SubjectSelected.options[i];
      Subjects.options[Subjects.length] = new Option(SubjOption.text,SubjOption.value);
      SubjectSelected.options[i] = null;
      AddRemoveSubject(index, SubjOption, 2);
    }      
  }
}

function Remove_All_Subject(index){
  var Subjects = document.getElementById('Subject'+index);
  var SubjectSelected = document.getElementById('SubjectSelected'+index+'[]');
  
  for (var i = (SubjectSelected.length -1); i >= 0 ; i--) {
   SubjOption = SubjectSelected.options[i];
   Subjects.options[Subjects.length] = new Option(SubjOption.text,SubjOption.value);
   SubjectSelected.options[i] = null;
   AddRemoveSubject(index, SubjOption, 2);
  }
}

function AddRemoveSubject(index, obj, action){  
  var Subj;

  if(action == 1){
    for(var i=1; i <= NumOfgroup; i++){
      if(i == index) continue;    
      
      Subj = document.getElementById('Subject'+i);
      for (var j = (Subj.length -1); j >= 0 ; j--) {
        SubjOpt = Subj.options[j];       
        if(SubjOpt.value == obj.value)
          Subj.options[j] = null;               
      }      
    }  
  }else{
    for(var i=1; i <= NumOfgroup; i++){
      if(i == index) continue;    
      
      Subj = document.getElementById('Subject'+i);
      Subj.options[Subj.length] = new Option(obj.text,obj.value);      
    }  
  }
}

function UnclickGroupConfirm(index){
  var sltgp = document.getElementById('maxfail'+index);
  var sltsubj = document.getElementById('SubjectSelected'+index+'[]');
  
  if(sltgp.checked == false && sltsubj.length > 0){
    if(confirm("<?=$eReportCard['jsAlertToRemoveSelectedSubject']?>")){
        Remove_All_Subject(index);
    }
    else
    {
      sltgp.checked = true;
    }
  }
  
  btnDisable = (sltgp.checked == false)? true : false;
  document.getElementById('AddAll'+index).disabled = btnDisable;
  document.getElementById('Add'+index).disabled = btnDisable;
  document.getElementById('Remove'+index).disabled = btnDisable;
  document.getElementById('RemoveAll'+index).disabled = btnDisable;
  
}

function SubmitForm(obj){

  if(CheckMaxNumber()){
    for(var i=1; i <= NumOfgroup; i++){
      Select_All_Options('SubjectSelected'+i+'[]',true);
    }
    return true;
  }else return false;
  
 
}

function CheckMaxNumber(){

  var maxNum;
  var sltsubjNum;
  var sltgp;
  
  for(var k=1; k <= NumOfgroup; k++){
    maxNum = document.getElementById('FailSubject_MAX'+k).value;
    sltsubjNum = document.getElementById('SubjectSelected'+k+'[]').length;
    sltgp = document.getElementById('maxfail'+k);
    
    if(sltgp.checked == true && maxNum > sltsubjNum){
      alert("<?=$eReportCard['jsAlertGreaterMaxNumber']?>");
      document.getElementById('FailSubject_MAX'+k).focus();
      return false;
    }
  }
  
  return true;

}
</script>
<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT."templates/2007a/css/"?>ereportcard.css">
<br />
<form name="form1" action="edit_update.php" method="post" onSubmit="return SubmitForm(this);">
<table width="95%" align="center" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
		<td align="right"> <?=$linterface->GET_SYS_MSG($xmsg);?></td>
	</tr>
	<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
      <td colspan="2" valign="top" class="formfieldtitle" nowrap>
      <input type="checkbox" name="pass_average" value="1" id="pass_average" <?echo ($pass_average)? "checked":"" ?>>&nbsp;
      <label for="pass_average"><?=$eReportCard['PassGrandAverage']?></label>
      </td>			
		</tr>
		</table>
	</td>
  </tr>
<!-- Maximum fail group start-->
<?for($i=1; $i <= $MaxfailGroup; $i++){?>  
  <tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center" class="formfieldtitle">
		<tr valign='top'>
      <td colspan="2" valign="top" nowrap>
      <input type="checkbox" name="maxfail<?=$i?>" value="1" id="maxfail<?=$i?>" <?=${checkGroup.$i}?> onClick="UnclickGroupConfirm(<?=$i?>);">&nbsp;
      <?=$eReportCard['MaximumFailSubject1']?>
      <?=getSelectByArray($data, "name='FailSubject_MAX$i' id='FailSubject_MAX$i'", ${FailSubject_MAX.$i} , 0, 1)?>
      <?=$eReportCard['MaximumFailSubject2']?>
      </td>			
		</tr>
		<tr>
		  <td colspan="2" valign="top" nowrap>
		    <table width="100%" border="0" cellspacing="0" cellpadding="5">
		      <tr>
		        <td width="50%" bgcolor="#EFFEE2" class="steptitletext">
            <?=$eReportCard['SelectedSubject']?>
            </td>
						<td width="40"></td>
            <td width="50%" bgcolor="#EEEEEE">
						&nbsp;
						</td>
					</tr>	
					<tr>
		        <td width="50%" bgcolor="#EFFEE2" class="steptitletext">
            <?=${SubjectSelection.$i}?>
            </td>
						<td width="40">
            <input name="AddAll<?=$i?>" id="AddAll<?=$i?>" onclick="Add_All_Subject(<?=$i?>);" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&lt;&lt;" style="width:40px;" title="<?=$Lang['Btn']['AddAll']?>" <?=${disableBtn.$i}?>/>
						<br />
						<input name="Add<?=$i?>" id="Add<?=$i?>" onclick="Add_Selected_Subject(<?=$i?>);" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&lt;" style="width:40px;" title="<?=$Lang['Btn']['AddSelected']?>" <?=${disableBtn.$i}?>/>
						<br /><br />
						<input name="Remove<?=$i?>" id="Remove<?=$i?>" onclick="Remove_Selected_Subject(<?=$i?>);" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&gt;" style="width:40px;" title="<?=$Lang['Btn']['RemoveSelected']?>" <?=${disableBtn.$i}?>/>
						<br />
						<input name="RemoveAll<?=$i?>" id="RemoveAll<?=$i?>" onclick="Remove_All_Subject(<?=$i?>);" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&gt;&gt;" style="width:40px;" title="<?=$Lang['Btn']['RemoveAll']?>" <?=${disableBtn.$i}?>/>
            </td>
            <td width="50%" bgcolor="#EEEEEE">
						<?=${NSubjectSelection.$i}?>
						</td>
					</tr>	
    		  <tr>
		        <td width="50%" bgcolor="#EFFEE2" class="steptitletext">
		        &nbsp;
            </td>
						<td width="40"></td>
            <td width="50%" bgcolor="#EEEEEE">
						&nbsp;
						</td>
					</tr>

  		  </table>
		  </td>
		</tr>
		</table>
	</td>
  </tr>  
<?}?>  
<!-- Maximum fail group end-->  
  <tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
      <td colspan="2" valign="top" class="formfieldtitle" nowrap>
      <input type="checkbox" name="submit_ass" value="1" id="submit_ass" <?echo ($submit_ass)? "checked":"" ?>>&nbsp;
      <?=$eReportCard['MaximumNotSubmitAssignment1']?>
      <?=getSelectByArray($data2, "name='SubmitAss_MAX'", $SubmitAss_MAX , 0, 1)?>
      <?=$eReportCard['MaximumNotSubmitAssignment2']?>
      </td>			
		</tr>
		</table>
	</td>
  </tr>
  
  <tr>
		<td class="dotline" colspan="2">
			<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
		</td>
	</tr>
  <tr>
	<td align="center" colspan="2">
		<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "")?>&nbsp;
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "location.href = 'index.php?ClassLevelID=$ClassLevelID'","cancelbtn") ?>
	</td>
  </tr>
</table>

<input type="hidden" name="ClassLevelID" id="ClassLevelID" value="<?=$ClassLevelID?>">
<input type="hidden" name="RecordID" id="RecordID" value="<?=$RecordID?>">
</form>
<?
  	$linterface->LAYOUT_STOP();
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>
