<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'] && $sys_custom['eRC']['Settings']['PromotionCriteria'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	$TAGS_OBJ[] = array($eReportCard['Settings_PromotionCriteria'], "");
	$CurrentPage = "PromotionCriteria";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	
	if ($lreportcard->hasAccessRight()) {
		
		# Get class level of school
		$FormArr = $lreportcard->GET_ALL_FORMS();
		
		$LevelNameArr = array();
		$LevelIDArr = array();
		if (sizeof($FormArr) > 0) {
			for($i=0; $i<sizeof($FormArr); $i++) {
				$LevelIDArr[] = $FormArr[$i]["ClassLevelID"];
				$LevelNameArr[] = $FormArr[$i]["LevelName"];
			}
		}
		$ClassLevelSelect = getSelectByValueDiffName($LevelIDArr, $LevelNameArr, 'name="ClassLevelID" id="ClassLevelID" onchange="checkPost(document.form1, \'index.php\');"', $ClassLevelID, 0, 1);
		
		# show Remark
    $showRemark =  "<fieldset class='instruction_box'>";
		$showRemark .=  "<legend class='instruction_title'>";
		$showRemark .=  $eReportCard['PromotionInstruction'];
		$showRemark .=  "</legend>";
		$showRemark .=  "<span>";
		$showRemark .=  $eReportCard['PromotionInstructionContent'];
		$showRemark .=  "</span>";
		$showRemark .=  "</fieldset>";
			
    # Load promotion settings
    $ClassLevelID = (isset($ClassLevelID) && $ClassLevelID != "")? $ClassLevelID : $FormArr[0]['ClassLevelID'];
		$SubjectList = $lreportcard->returnSubjectwOrderNoL($ClassLevelID, 1, 1);
    $PromotionCriteria = $lreportcard->getPromotionCriteria($ClassLevelID);
		if(count($PromotionCriteria) > 0){
      $displayCriteria = true;
      $RecordID = $PromotionCriteria[0]['PromotionCriteriaID']; 		
  		$pass_average = ($PromotionCriteria[0]['PassGrandAverage'])? $i_general_yes : $i_general_no; 
  		$submit_ass = ($PromotionCriteria[0]['NonSubmissionAssignment'])? $PromotionCriteria[0]['MaxAssignment'] : $i_general_no; 
  		
  		$FailSubject_MAX1 = $PromotionCriteria[0]['MaxFailNumOfSubject1'];
      $FailSubject_MAX2 = $PromotionCriteria[0]['MaxFailNumOfSubject2'];
      $FailSubject_MAX3 = $PromotionCriteria[0]['MaxFailNumOfSubject3']; 
  		$SelectedSubject1 = $PromotionCriteria[0]['SubjectList1']; 
  		$SelectedSubjectArr1 = trim($SelectedSubject1)? explode(",", $SelectedSubject1) : array();
  		$SelectedSubject2 = $PromotionCriteria[0]['SubjectList2']; 
  		$SelectedSubjectArr2 = trim($SelectedSubject2)? explode(",", $SelectedSubject2) : array();
  		$SelectedSubject3 = $PromotionCriteria[0]['SubjectList3'];
  		$SelectedSubjectArr3 = trim($SelectedSubject3)? explode(",", $SelectedSubject3) : array();
      
      if(count($SelectedSubjectArr1) > 0){
        foreach($SelectedSubjectArr1 as $index => $sid) 
          $SSArr1[] = $SubjectList[$sid];
        $SSList1 = implode("<br>", $SSArr1);  
      }else $SSList1 = "";
      if(count($SelectedSubjectArr2) > 0){
        foreach($SelectedSubjectArr2 as $index => $sid) 
          $SSArr2[] = $SubjectList[$sid];
        $SSList2 = implode("<br>", $SSArr2);   
      }else $SSList2 = "";
      if(count($SelectedSubjectArr3) > 0){
        foreach($SelectedSubjectArr3 as $index => $sid) 
          $SSArr3[] = $SubjectList[$sid];
        $SSList3 = implode("<br>", $SSArr3);   
      }else $SSList3 = "";
      
      $LastModifiedDate = $PromotionCriteria[0]['DateModified'];
      $LastModifiedBy = $PromotionCriteria[0]['LastModifiedBy'];
       
      
      # build content table
      $table_row = 0;
      $table .= "<table width='100%' border='0' cellpadding='5' cellspacing='0'>";
      
      $table .= "<tr>";
      $table .= "<td class='tabletop'>". $eReportCard['Settings_PromotionCriteria'] ."</td>";
      $table .= "<td class='tabletop' width='35%'>". $eReportCard['Value'] ."</td>";
      $table .= "</tr>";
      
      $tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
      $table .= "<tr class='". $tablerow_class ."'>";
      $table .= "<td>". $eReportCard['PassGrandAverage'] ."</td>";
      $table .= "<td>". $pass_average ."</td>";
      $table .= "</tr>";
      
      /*** Max. Fail subject group ***/
      if(trim($SelectedSubject1)){
        $tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
        $table .= "<tr class='". $tablerow_class ."'>";
        $table .= "<td>". $eReportCard['MaximumFailSubject1'].$FailSubject_MAX1." ".$eReportCard['MaximumFailSubject2']."</td>";
        $table .= "<td>". $SSList1 ."</td>";
        $table .= "</tr>";
      }
      
      if(trim($SelectedSubject2)){
        $tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
        $table .= "<tr class='". $tablerow_class ."'>";
        $table .= "<td>". $eReportCard['MaximumFailSubject1'].$FailSubject_MAX2." ".$eReportCard['MaximumFailSubject2']."</td>";
        $table .= "<td>". $SSList2 ."</td>";
        $table .= "</tr>";
      }
      
      if(trim($SelectedSubject3)){
        $tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
        $table .= "<tr class='". $tablerow_class ."'>";
        $table .= "<td>". $eReportCard['MaximumFailSubject1'].$FailSubject_MAX3." ".$eReportCard['MaximumFailSubject2']."</td>";
        $table .= "<td>". $SSList3 ."</td>";
        $table .= "</tr>";
      }
      
      /*******************************/
      
      $tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
      $table .= "<tr class='". $tablerow_class ."'>";
      $table .= "<td>". $eReportCard['MaximumNotSubmitAssignment1'] . $eReportCard['MaximumNotSubmitAssignment3'] ."</td>";
      $table .= "<td>". $submit_ass ."</td>";
      $table .= "</tr>";
      
      /*
      $tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
      $table .= "<tr class='". $tablerow_class ."'>";
      $table .= "<td>". $eReportCard['LastModifiedDate'] ."</td>";
      $table .= "<td>". $LastModifiedDate ."</td>";
      $table .= "</tr>";
      
      $tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
      $table .= "<tr class='". $tablerow_class ."'>";
      $table .= "<td>". $eReportCard['LastModifiedBy'] ."</td>";
      $table .= "<td>". $LastModifiedBy ."</td>";
      $table .= "</tr>";
		*/
      $table .= "</table>";
      
      $LastModifiedRemarks = Get_Last_Modified_Remark($LastModifiedDate, $LastModifiedBy);
      $table .= "<br /><span class='tabletextremark'>".$LastModifiedRemarks."</span>";
      
    }else{
      $table = "<table width='100%' border='0' cellpadding='5' cellspacing='0'>";
      $table .= "<tr class='tablerow1'>";
      $table .= "<td colspan=2>". $eReportCard['NoPromotionSettings'] ."</td>";
      $table .= "</tr>";
      $table .= "</table>";
    }
		
		############## Interface Start ##############
		$linterface = new interface_html();

		# page navigation (leave the array empty if no need)
		$PAGE_NAVIGATION[] = array();

		$linterface->LAYOUT_START();
?>
<script type="text/JavaScript" language="JavaScript">
function resetForm() {
	document.form1.reset();
	return true;
}
</script>
<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT."templates/2007a/css/"?>ereportcard.css">
<br />
<form name="form1" action="edit.php" method="post">
<table width="95%" align="center" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>&nbsp;</td>
		<td align="right"> <?=$linterface->GET_SYS_MSG($xmsg);?></td>
	</tr>
</table>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
			  <tr>
			    <td valign="top" class="tabletext">
			    <?=$ClassLevelSelect?>
			    </td>
			  </tr>
			  <tr>
			    <td valign="top" class="tabletext">
			    <?=$showRemark?>
          </td>
			  </tr>
			</table>
		</td>
	</tr>
	<tr> 
  	<td>
  		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
  			<tr>
  				<td colspan="2"><?=$table?></td>
  			</tr>
  		</table>
  	</td>
  </tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td class="dotline">
						<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
					</td>
				</tr>
				<tr>
					<td align="center">
						<?= $linterface->GET_ACTION_BTN($button_edit, "submit")?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<?
  	$linterface->LAYOUT_STOP();
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>
