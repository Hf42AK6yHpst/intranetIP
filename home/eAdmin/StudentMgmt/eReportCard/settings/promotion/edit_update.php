<?php
# using: Kit
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'] && $sys_custom['eRC']['Settings']['PromotionCriteria'])
{
  include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
}


$data = array();
$data['RecordID'] = $RecordID;
$data['ClassLevelID'] = $ClassLevelID;
$data['pass_average'] = $pass_average;
$data['maxfail1'] = $maxfail1;
$data['maxfail2'] = $maxfail2;
$data['maxfail3'] = $maxfail3;
$data['FailSubject_MAX1'] = $FailSubject_MAX1;
$data['FailSubject_MAX2'] = $FailSubject_MAX2;
$data['FailSubject_MAX3'] = $FailSubject_MAX3;
$data['SubjectSelected1'] = (is_array($_REQUEST['SubjectSelected1']))? $_REQUEST['SubjectSelected1']: array();
$data['SubjectSelected2'] = (is_array($_REQUEST['SubjectSelected2']))? $_REQUEST['SubjectSelected2']: array();
$data['SubjectSelected3'] = (is_array($_REQUEST['SubjectSelected3']))? $_REQUEST['SubjectSelected3']: array();
$data['submit_ass'] = $submit_ass;
$data['SubmitAss_MAX'] = $SubmitAss_MAX;


# store in DB
if($RecordID !="")
  $success = $lreportcard->updatePromotionCriteria($data);
else
  $RecordID = $lreportcard->insertPromotionCriteria($data);

header("Location: index.php?ClassLevelID=$ClassLevelID&RecordID=$RecordID");

intranet_closedb();
?>
