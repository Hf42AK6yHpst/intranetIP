<?php
// Using :

/**************************************************************
 *	Modification log:
 *  20200428 Bill:  [2019-0924-1029-57289]
 *      - Create File   ($eRCTemplateSetting['Settings']['PromotionStatusRemarks'])
 ***************************************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!isset($_POST) || empty($remarkArr)) {
    header("Location:index.php");
    exit;
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");

if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
    include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
    $lreportcard = new libreportcardcustom();
}
else {
    $lreportcard = new libreportcard();
}

if (!$eRCTemplateSetting['Settings']['PromotionStatusRemarks'] || !$lreportcard->hasAccessRight()) {
    intranet_closedb();
    header("Location:/index.php");
    exit;
}

# Update Promotion Remarks
$updateRemarkAssocArr = array();
foreach($remarkArr as $thisFormID => $thisFormRemarkArr)
{
    foreach($thisFormRemarkArr as $thisType => $thisRemarks)
    {
        $dataArr = array();
        $dataArr['ClassLevelID'] = IntegerSafe($thisFormID);
        $dataArr['PromotionType'] = stripslashes($thisType);
        $dataArr['Remarks'] = stripslashes($thisRemarks);
        $updateRemarkAssocArr[] = $dataArr;
    }
}
$success = $lreportcard->UpdatePromotion_Status_Remarks($updateRemarkAssocArr);

$msg = $success ? "UpdateSuccess" : "UpdateUnsuccess";
header("location: index.php?msg=$msg");
?>