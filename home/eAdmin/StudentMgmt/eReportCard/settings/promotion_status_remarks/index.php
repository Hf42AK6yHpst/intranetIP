<?php
// Using :

/**************************************************************
 *	Modification log:
 *  20200428 Bill:  [2019-0924-1029-57289]
 *      - Create File   ($eRCTemplateSetting['Settings']['PromotionStatusRemarks'])
 ***************************************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
    include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
    include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");

    if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
        include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
        $lreportcard = new libreportcardcustom();
    }
    else {
        $lreportcard = new libreportcard();
    }

    $CurrentPage = "Settings_PromotionStatusRemarks";
    $MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    $TAGS_OBJ[] = array($eReportCard['Settings_PromotionStatusRemarks'], "", 0);

    if ($lreportcard->hasAccessRight() && $eRCTemplateSetting['Settings']['PromotionStatusRemarks'])
    {
        $linterface = new interface_html();
        $lreportcard_ui = new libreportcard_ui();

        $formArr = $lreportcard->GET_ALL_FORMS(false, true);
        $remarksArr = $lreportcard->Get_Promotion_Status_Remarks();
        $remarksArr = BuildMultiKeyAssoc($remarksArr, array('ClassLevelID', 'PromotionType'), 'Remarks', 1, 0);

        $show_table = "";
        $show_table .= "<table id='Content_Table' width='100%' border='0' cellpadding='4' cellspacing='1' class='common_table_list_v30'>
                        <thead>
                            <tr class='tabletop'>
                                <th width='10%'>". $Lang['General']['Form'] ."</th>
                                <th width='30%'>". $Lang['eReportCard']['SettingArr']['PromotionStatusRemarks']['Promotion']."</th>
                                <th width='30%'>". $Lang['eReportCard']['SettingArr']['PromotionStatusRemarks']['ReExamPassed']."</th>
                                <th width='30%'>". $Lang['eReportCard']['SettingArr']['PromotionStatusRemarks']['ReExamFailed']."</th>
                            </tr>
                        </thead>";

        $edit_table = "";
        $edit_table .= "<table id='Content_Table' width='100%' border='0' cellpadding='4' cellspacing='1' class='common_table_list_v30 edit_table_list_v30'>
                        <thead>
                            <tr class='tabletop'>
                                <th width='10%'>". $Lang['General']['Form'] ."</th>
                                <th width='30%'>". $Lang['eReportCard']['SettingArr']['PromotionStatusRemarks']['Promotion']."</th>
                                <th width='30%'>". $Lang['eReportCard']['SettingArr']['PromotionStatusRemarks']['ReExamPassed']."</th>
                                <th width='30%'>". $Lang['eReportCard']['SettingArr']['PromotionStatusRemarks']['ReExamFailed']."</th>
                            </tr>
                        </thead>";

        $show_table .= "<tbody>";
        $edit_table .= "<tbody>";

        // loop forms
        foreach($formArr as $formIndex => $formInfo)
        {
            list($yearID, $yearName) = $formInfo;

            $ID = 'remarkArrIdx'."_".$formIndex;
            $Name = 'remarkArr['.$yearID.']';
            $isDisabled = '';
            if ($lreportcard->IS_VIEW_GROUP_USER_VIEW($_SESSION['UserID'])) {
                $inputPar['disabled'] = 'true';
                $isDisabled = ' disabled ';
            }

            $show_table .= "<tr>
                                <td class='tabletext' valign='top'>".$yearName."</td>";
            foreach($eRCTemplateSetting['ReportGeneration']['PromoteInputMapping'] as $thisPromotionType => $thisPromotionVal)
            {
                $show_table .= "<td class='tabletext' valign='top'>
                                    ".nl2br($remarksArr[$yearID][$thisPromotionType])."
                                </td>";
            }
            $show_table .= "</tr>";

            $edit_table .= "<tr>
                                <td class='tabletext' valign='top'>".$yearName."</td>";
            foreach($eRCTemplateSetting['ReportGeneration']['PromoteInputMapping'] as $thisPromotionType => $thisPromotionVal)
            {
                $edit_table .= "<td class='tabletext' valign='top'>
                                    ".$linterface->GET_TEXTAREA($Name."[".$thisPromotionType."]", "\n" . $remarksArr[$yearID][$thisPromotionType], 40, 8, NULL, NULL, $isDisabled, " remarkTextInput_".$thisPromotionVal, $ID."_".$thisPromotionVal)."
                                </td>";
            }
            $edit_table .= "</tr>";
        }

        $show_table .= "</tbody>";
        $show_table .= "</table>";

        $edit_table .= "</tbody>";
        $edit_table .= "</table>";

        // Get Btn
        if (!$lreportcard->IS_VIEW_GROUP_USER_VIEW($_SESSION['UserID'])) {
            $SubmitBtn = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "submit", "", "submitBtn");
            $EditBtn = $linterface->Get_Action_Btn($Lang['Btn']['Edit'], "button", "jsEdit()");
        }
        $CancelBtn = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "jsCancel()");

        $show_btnDiv .= '<div class="edit_bottom_v30">'."\n";
            $show_btnDiv .= $EditBtn."\n";
        $show_btnDiv .= '</div>'."\n";

        $edit_btnDiv .= '<div class="edit_bottom_v30">'."\n";
            $edit_btnDiv .= $SubmitBtn."\n";
            $edit_btnDiv .= $CancelBtn."\n";
        $edit_btnDiv .= '</div>'."\n";

        // Return Message
        $ReturnMsg = $Lang['General']['ReturnMessage'][$_GET["msg"]];
        $linterface->LAYOUT_START($ReturnMsg);
        ?>

        <script>
            function jsEdit()
            {
                $('.Edit_Hide').hide();
                $('.Edit_Show').show();
            }

            function jsCancel()
            {
                document.form1.reset();

                $('.Edit_Show').hide();
                $('.Edit_Hide').show();
            }

            function js_Check_Form()
            {
                $('input#submitBtn').attr('disabled', true);
                $('form#form1').attr('action', 'update.php');
            }
        </script>

        <form id="form1" name="form1" action="update.php" method="POST" onsubmit="return js_Check_Form();">
            <br/>
            <br/>
            <div class="table_board Edit_Hide">
                <?php echo $show_table; ?>
                <?php echo $show_btnDiv; ?>
            </div>
            <div class="table_board Edit_Show" style="display: none">
                <?php echo $edit_table; ?>
                <?php echo $edit_btnDiv; ?>
            </div>
        </form>

        <?
        $linterface->LAYOUT_STOP();
        intranet_closedb();
    }
    else
    {
        ?>
        You have no priviledge to access this page.
        <?
    }
}
else
{
    ?>
    You have no priviledge to access this page.
    <?
}
?>