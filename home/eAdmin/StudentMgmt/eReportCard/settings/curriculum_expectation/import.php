<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	$CurrentPage = "Curriculum_Expectation";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	
	if ($lreportcard->hasAccessRight()) {
		
		$ClassLevelID = $_GET['ClassLevelID'];
		$TermID = $_GET['TermID'];
		$lclass = new libclass();
		$LevelName = $lclass->getLevelName($ClassLevelID);
		
		$parameters = '?ClassLevelID='.$ClassLevelID.'&TermID='.$TermID.'&IsImportSample=1';

		$linterface = new interface_html();
		
		
		############## Interface Start ##############
		# tag information
		$TAGS_OBJ[] = array($eReportCard['Settings_CurriculumExpectation']);
		
		# page navigation (leave the array empty if no need)
		$PAGE_NAVIGATION[] = array($eReportCard['Settings_CurriculumExpectation']);
		$PAGE_NAVIGATION[] = array($LevelName);
		$PAGE_NAVIGATION[] = array($button_import);

		$linterface->LAYOUT_START();
?>
<script type="text/JavaScript" language="JavaScript">
function trim(str)
{
	return str.replace(/^\s+|\s+$/g,"");
}

function checkForm() {
	obj = document.form1;
	if (trim(obj.elements["userfile"].value) == "") {
		 alert('<?=$eReportCard['AlertSelectFile']?>');
		 obj.elements["userfile"].focus();
		 return false;
	}
	obj.submit();
}
</script>
<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT."templates/2007a/css/"?>ereportcard.css">
<br />
<form name="form1" action="import_update.php" method="POST" onsubmit="return checkForm();" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
		<td align="right"> <?=$linterface->GET_SYS_MSG($Result);?></td>
	</tr>
	<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
<table>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $i_select_file ?>
					</td>
					<td width="75%" class='tabletext'>
						<input class="file" type='file' name='userfile'>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" colspan='2' class="tabletext">
						<a id="SampleCsvLink" class="tablelink" href="export.php<?=$parameters?>">
							<img src='<?=$PATH_WRT_ROOT?>/images/2009a/icon_files/xls.gif' border='0' align='absmiddle' hspace='3'> 
							<?=$i_general_clickheredownloadsample?>
						</a>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td class="dotline">
						<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
					</td>
				</tr>
				<tr>
					<td align="center">
						<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "")?>&nbsp;
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location = 'index.php'")?>&nbsp;
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="ClassLevelID" id="ClassLevelID" value="<?=$ClassLevelID?>" />
<input type="hidden" name="TermID" id="TermID" value="<?=$TermID?>" />
</form>
<?
	print $linterface->FOCUS_ON_LOAD("form1.userfile");
  	$linterface->LAYOUT_STOP();
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>
