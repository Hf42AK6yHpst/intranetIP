<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();


if ($plugin['ReportCard2008']) {
	$lreportcard = new libreportcard();
	
	if ($lreportcard->hasAccessRight()) {
		
		$ClassLevelID = $_POST['ClassLevelID'];
		$TermID = $_POST['TermID'];
		
		$li = new libdb();
		$limport = new libimporttext();
		$libreportcard = new libreportcard();
		
		$filepath = $userfile;
		$filename = $userfile_name;
		
		if($filepath=="none" || $filepath == ""){          # import failed
		    header("Location: import.php?Result=import_failed2&CommentType=$CommentType");
		    exit();
		} else {
		    if($limport->CHECK_FILE_EXT($filename)) {
				# read file into array
				# return 0 if fail, return csv array if success
				$data = $limport->GET_IMPORT_TXT($filepath, 0, "|||---linebreak---|||");
				#$toprow = array_shift($data);                   # drop the title bar
				
				$format_array = array('SubjectCode', 'SubjectComponentCode', 'Description');
				$limport->SET_CORE_HEADER($format_array);
				if (!$limport->VALIDATE_HEADER($data, true) || !$limport->VALIDATE_DATA($data)) {
					header("Location: import.php?Result=import_failed&CommentType=$CommentType");
					exit();
				}
				array_shift($data);
			    $values = "";
			    $delim = "";
				$successCount = 0;
				$failCount = 0;
				$csvCodeList = array();
				
				$SubjectCodeMapping = $libreportcard->GET_SUBJECTS_CODEID_MAP($withComponent=1, $mapByCode=1);
				
			    for ($i=0; $i<sizeof($data); $i++) {
					if (empty($data[$i])) continue;
					
					list($thisSubjectCode, $thisSubjectComponentCode, $thisDescription) = $data[$i];
					$thisSubjectCode = trim($thisSubjectCode);
					$thisSubjectComponentCode = trim($thisSubjectComponentCode);
					
					$thisDescription = str_replace('|||---linebreak---|||', "\n", $thisDescription);
					$thisDescription = $limport->REPLACE_DQ_IN_STR($thisDescription);
					$thisDescription = addslashes($thisDescription);
					
					if($thisSubjectCode == "OVERALL")
						$thisSubjectID = 0;
					else
					{
						$thisKey = $thisSubjectCode.'_'.$thisSubjectComponentCode;
						$thisSubjectID = $SubjectCodeMapping[$thisKey];
					}
					
					# check if description exist
					if ($lreportcard->Is_Course_Description_Exist($ClassLevelID, $thisSubjectID, $TermID) == true)
					{
						# update
						$result = $lreportcard->Update_Course_Description($ClassLevelID, $thisSubjectID, $TermID, $thisDescription);
					}
					else
					{
						# insert
						$result = $lreportcard->Insert_Course_Description($ClassLevelID, $thisSubjectID, $TermID, $thisDescription);
					}
					
					if ($result)
						$successCount++;
			    }
				
		    } else {
				header("Location: import.php?Result=import_failed&ClassLevelID=$ClassLevelID&TermID=$TermID");
				exit();
			}
		}
		
		intranet_closedb();
		if ($successCount === 0)
			header("Location: import.php?Result=import_failed2&ClassLevelID=$ClassLevelID&TermID=$TermID");
		else
			header("Location: edit.php?Result=num_records_updated&SuccessCount=$successCount&ClassLevelID=$ClassLevelID&TermID=$TermID");
	} else {
		echo "You have no priviledge to access this page.";
	}
} else {
	echo "You have no priviledge to access this page.";
}
?>