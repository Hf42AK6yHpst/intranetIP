<?php
/*******************************************
 * modification log
 *		20100105 Marcus: 
 *			update other terms if "apply to other terms" is checked  
 * ******************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

$table = $lreportcard->DBName.".RC_CURRICULUM_EXPECTATION";

# delete old records
//$lreportcard->db_db_query("delete from $table where ClassLevelID=$ClassLevelID");

# insert records
$flag = 1;
//$MainSubjectArray = $lreportcard->returnSubjectwOrder($ClassLevelID);
//foreach($MainSubjectArray as $MainSubjectID => $MainSubjectNameArray)
//{
//	foreach ($MainSubjectNameArray as $ComponentSubjectID => $SubjectNameBiLing)
//	{
	
	foreach($Content as $thisSubjectID => $thisContent)
	{
//		if ($ComponentSubjectID == 0)
//		{
//			$thisSubjectID = $MainSubjectID;
//		}
//		else
//		{
//			$thisSubjectID = $ComponentSubjectID;
//		}
//		$thisContent = $_POST['Content_'.$thisSubjectID];
		
		if($applytootherterms)
		{
			#get terms of current year 
			$TermsAry = $lreportcard->GET_ALL_SEMESTERS();
			$TermIDAry = array();
			if(is_array($TermsAry))
				$TermIDAry = array_keys($TermsAry);
			
			# include consolidate report
			array_push($TermIDAry,0);
		}
		else
		{
			$TermIDAry = array($TermID);
		}
			
		foreach((array)$TermIDAry as $thisTermID)
		{
			# check if description exist
			if ($lreportcard->Is_Course_Description_Exist($ClassLevelID, $thisSubjectID, $thisTermID) == true)
			{
				# update
				$result = $lreportcard->Update_Course_Description($ClassLevelID, $thisSubjectID, $thisTermID, $thisContent);
			}
			else
			{
				# insert
				$result = $lreportcard->Insert_Course_Description($ClassLevelID, $thisSubjectID, $thisTermID, $thisContent);
			}
		}
		
		if(!$result)	$flag = 0 ;
	}
//	}
//}

$xmsg = $flag ? "update" : "update_failed";

intranet_closedb();
header("Location: index.php?semester=".$TermID."&xmsg=".$xmsg);

?>