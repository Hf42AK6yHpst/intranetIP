<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

intranet_auth();
intranet_opendb();

#################################################################################################

/* Temp Library*/
include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
$lreportcard = new libreportcard2008j();
//$lreportcard = new libreportcard();

# Get Data
$frClassLevelID = $frClassLevel;		// Copy From
$frReportID = $frReportID;		// Copy From
$toClassLevelID = $copyClassLevel;		// Copy To
$toReportID = $toReportID;		// Copy To


# Copy ClassLevel Settings
$copyResult = $lreportcard->COPY_CLASSLEVEL_SETTINGS($frClassLevelID, $toClassLevelID, $frReportID, $toReportID);

$cpResult = htmlspecialchars(serialize($copyResult));



#################################################################################################

#################################################################################################
intranet_closedb();

?>


<body onLoad="document.form1.submit();">
<form name="form1" action="form_settings_copy_result.php" method="post">
<input type="hidden" name="frClassLevel" value="<?=$frClassLevelID?>">
<input type="hidden" name="toClassLevel" value="<?=$toClassLevelID?>">
<input type="hidden" name="frReportID" value="<?=$frReportID?>">
<input type="hidden" name="toReportID" value="<?=$toReportID?>">
<input type="hidden" name="cpResult" value="<?=$cpResult?>">
</form>
</body>