<?php
// ############ MODIFICATION ### LOG ####################
//
// File using : Philips
// Last Modified : 20190628
// Created : 20190628
//
//
// ############ MODIFICATION ### LOG ####################
//
//
//
//
// Date: 20190628 (Philips)
// Created File
//
// ############ MODIFICATION ### LOG ####################

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
// include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libreportcard2008.php");
include_once ($PATH_WRT_ROOT . "includes/libclass.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
    /* Temp Library */
    include_once ($PATH_WRT_ROOT . "includes/libreportcard2008j.php");
    $lreportcard = new libreportcard2008j();
    if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
        include_once ($PATH_WRT_ROOT . "includes/reportcard_custom/" . $ReportCardCustomSchoolName . ".php");
    }
    // $lreportcard = new libreportcard();
    
    $CurrentPage = "Settings_SubjectsAndForms";
    $MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
    
    // tag information
    $link0 = 0;
    $link1 = 0;
    $link2 = 0;
    $link3 = 1;
    $TAGS_OBJ[] = array(
        $eReportCard['SubjectSettings'],
        $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eReportCard/settings/subjects_and_form/subject_settings.php",
        $link0
    );
    $TAGS_OBJ[] = array(
        $eReportCard['FormSettings'],
        $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eReportCard/settings/subjects_and_form/form_settings.php",
        $link1
    );
    if($eRCTemplateSetting['Settings']['FormSubjectTypeSettings']){
        $TAGS_OBJ[] = array(
            $eReportCard['SubjectType']['Type'],
            $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eReportCard/settings/subjects_and_form/credit_settings.php",
            $link2
        );
    }
    $TAGS_OBJ[] = array(
        $eReportCard['GradeSettings'],
        $PATH_WRT_ROOT . "home/eAdmin/StudentMgmt/eReportCard/settings/subjects_and_form/grade_settings.php",
        $link3
    );
    $UpdatePage = "grade_settings_update.php";
    if ($lreportcard->hasAccessRight() && $eRCTemplateSetting['Settings']['GradeDescriptor']) {
        $linterface = new interface_html();
        
        // ###########################################################################################################
        
        // #### Get Grade Range Array
        $rangeAry = $lreportcard->GET_GRADE_FROM_GRADING_SCHEME_RANGE();
        $gradeAry = array_unique($rangeAry);
        $gradeAry = array_filter($gradeAry, 'filterFunc');
        asort($gradeAry);

        $descriptorAry = $lreportcard->getGradeDescriptor();
        $descriptorAry = BuildMultiKeyAssoc($descriptorAry, 'Grade');

        $i = 0;
        $rx = "";
        $taCols = 30;
        foreach($gradeAry as $grade){
            $tr_style = 'retablerow'.($i++%2 + 1);
            $rx .= "<tr class='$tr_style'>";
                $rx .= "<td align='center'>$grade</td>";
                $rx .= "<td align='center'>".$linterface->GET_TEXTAREA("TextEnglish[$grade]", $descriptorAry[$grade]['Descriptor_en'], $taCols)."</td>";
                //$rx .= "<td align='center'>".$linterface->GET_TEXTAREA("TextChinese[$grade]", $descriptorAry[$grade]['Descriptor_ch'], $taCols)."</td>";
                //$rx .= "<td align='center'>".$linterface->GET_TEXTAREA("TextEnglish[$grade]", $descriptorAry[$grade]['Descriptor_en'], $taCols)."</td>";
            $rx .= "</tr>";
        }
        
        //$updateBtn = $linterface->GET_BTN($Lang['Btn']['Submit'], 'submit', $ParOnClick="", $ParName="", $ParOtherAttribute="", $ParDisabled=0);

        $msg = '';
        if($Result == 'update') {
            $msg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
        }else if($Result == 'update_failed'){
            $msg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
        }
        $linterface->LAYOUT_START($msg);
        
        echo '<link href="' . $PATH_WRT_ROOT . 'templates/2007a/css/ereportcard.css" rel="stylesheet" type="text/css" />';
        ?>

<form name="form1" method="POST" action="<?=$UpdatePage;?>">
<table width="75%" border="0" cellpadding="4" cellspacing="0">
    <tr>
        <td align="right">&nbsp;</td>
    </tr>
    <tr>
        <td align="left">
	        <table width="100%" border="0" cellspacing="0" cellpadding="4" align="center">
                <thead>
                <tr>
                    <th width="15%" align="center" valign="top" class="tabletop tabletopnolink"><span class="tabletopnolink"><?=$eReportCard['Grade']?></span></th>
                    <th width="85%" align="center" valign="top" class="tabletop tabletopnolink"><span class="tabletopnolink"><?=$eReportCard['GradeSettings']?></span></th>
        <!--			<th width="40%" align="center" valign="top"-->
        <!--				class="tabletop tabletopnolink"><span class="tabletopnolink">--><?//=$Lang['General']['ChineseName2']?><!--</span></th>-->
        <!--			<th width="40%" align="center" valign="top"-->
        <!--				class="tabletop tabletopnolink"><span class="tabletopnolink">--><?//=$Lang['General']['EnglishName2']?><!--</span></th>-->
                </tr>
                </thead>
                <tbody>
                    <?=$rx?>
                </tbody>
            </table>
        </td>
    </tr>
</table>

<div class="edit_bottom_v30">
    <p class="spacer"></p>
    <?php //echo $updateBtn;?>
    <input name="Save" type="submit" class="formbutton" onMouseOver="this.className='formbuttonon'" onMouseOut="this.className='formbutton'" value="<?=$button_save?>">
    <p class="spacer"></p>
</div>
<br/>
</form>

<?php
        $linterface->LAYOUT_STOP();
    } else {
        ?>
You have no priviledge to access this page.
    <?
    }
} else {
    ?>
You have no priviledge to access this page.
<?
}

function filterFunc($item){
    return $item != '';
}
?>