<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

intranet_auth();
intranet_opendb();

/* Temp Library*/
include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
$lreportcard = new libreportcard2008j();
//$lreportcard = new libreportcard();


$result = array();

# Get class level of school
$FormArr = $lreportcard->GET_ALL_FORMS();

# Get All Subjects
$SubjectArray = $lreportcard->GET_ALL_SUBJECTS(1);


$NewDataArr = array();
$NewCheckSubjectArr = array();

# Get CheckBox Data for each subjects of each ClassLevel(Form)
for($i=0 ; $i<count($FormArr) ; $i++){
	if(!empty($SubjectArray))
	{
		foreach($SubjectArray as $SubjectID => $Data)
		{
			if(is_array($Data))
			{
				foreach($Data as $CmpSubjectID => $SubjectName)
				{	
					$SubjectID = ($CmpSubjectID==0) ? $SubjectID : $CmpSubjectID;
					$id = $SubjectID."_".$FormArr[$i]['ClassLevelID'];
					
//					$NewCheckSubjectArr[$SubjectID] = (${"checkBox_".$id}) ? (int)${"checkBox_".$id} : 0;
					$NewCheckSubjectArr[$SubjectID] = ($_POST["checkBox"][$SubjectID][$FormArr[$i]['ClassLevelID']]) ? (int)$_POST["checkBox"][$SubjectID][$FormArr[$i]['ClassLevelID']] : 0;
				}
			}
		}
	}
	$NewDataArr[$FormArr[$i]['ClassLevelID']] = $NewCheckSubjectArr;
}

/*
$a = $lreportcard->CHECK_FROM_SUBJECT_GRADING(25);
echo '<div>';
var_dump($a);
echo '</div>';
echo '<br /><br /><br />';
echo '<div>';
var_dump($NewDataArr[25]);
echo '</div>';
die();
*/

# INSERT or DELETE Subject Grading Form 
$cnt = 0;
foreach ($NewDataArr as $key => $DataArr){
	$result['change_form_'.$cnt] = $lreportcard->CHANGE_FROM_SUBJECT_GRADING($key, $DataArr);
	$cnt++;
}

if(!in_array(false, $result))
	$msg = "update";
else 
	$msg = "update_failed";

intranet_closedb();

header("Location: subject_settings.php?Result=$msg");

?>