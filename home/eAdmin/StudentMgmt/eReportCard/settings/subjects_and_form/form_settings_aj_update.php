<?php
/***********************************************
 * 		Modification log
 * 	20110610 Marcus:
 * 		1.	cater grading scheme grade description
 * 	20091221 Marcus:
 * 		1.	cater more than 1 distinction
 * **********************************************/


$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();

/* Temp Library*/
include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
$lreportcard = new libreportcard2008j();
//$lreportcard = new libreportcard();
#########################################################################

# Initialization
$SubjectIDArr = array();
$SchemeIDArr = array();

# Get Data
$SubjectIDArr = $subjectID;	// pass by array
$SchemeIDArr = $schemeID;	// pass by array
//debug_pr($SchemeIDArr);
$targetForm = $ClassLevelID;

$SubjectArray = $lreportcard->GET_ALL_SUBJECTS(0);

################################################################################################# 
# Form Subject Setting Data
# including Display Order, SubjectName, SubjectID, corresponding SchemeID, Input Scale, Display Result, IsCmpSubjectFlag
#################################################################################################
$SubjectInfo = array();
for($i=0 ; $i<count($SubjectIDArr) ; $i++){
	$SubjectInfo[$SubjectIDArr[$i]]['displayOrder'] = ${"display_order_".$SubjectIDArr[$i]};
	$SubjectInfo[$SubjectIDArr[$i]]['is_cmpSubject'] = ${"cmpSubjectFlag_".$SubjectIDArr[$i]};
	$SubjectInfo[$SubjectIDArr[$i]]['schemeID'] = ${"select_scheme_".$SubjectIDArr[$i]};
	$SubjectInfo[$SubjectIDArr[$i]]['scaleInput'] = ${"scale_".$SubjectIDArr[$i]};
	$SubjectInfo[$SubjectIDArr[$i]]['scaleDisplay'] = ${"display_".$SubjectIDArr[$i]};
	$SubjectInfo[$SubjectIDArr[$i]]['parentSubjectID'] = ${"parent_subject_id_".$SubjectIDArr[$i]};
	$SubjectInfo[$SubjectIDArr[$i]]['scheme_info'] = array();
	
	if($SubjectInfo[$SubjectIDArr[$i]]['is_cmpSubject'] == 1)
		$SubjectInfo[$SubjectIDArr[$i]]['subjectName'] = $SubjectArray[$SubjectInfo[$SubjectIDArr[$i]]['parentSubjectID']][$SubjectIDArr[$i]];
	else 
		$SubjectInfo[$SubjectIDArr[$i]]['subjectName'] = $SubjectArray[$SubjectIDArr[$i]][0];
}

#################################################################################################
# Grading Schemes Data which is newly created or updated or deleted
# Title, Type, (Honor / PassFail) , Range Info
#################################################################################################
$SchemeInfo = array();
//print_r($SchemeIDArr);
for($i=0 ; $i<count($SchemeIDArr) ; $i++){
	//SchemeID, SchemeTitle, SchemeType, FullMark, PassMark, TopPercentage, Pass, Fail ";
	$SchemeInfo[$i]['SchemeID'] = $SchemeIDArr[$i];
	// Added on 20080820: convert grading title from UTF-8 to BIG5/GB2312 because the Ajax JS call made it UTF-8
	/*
	if ($intranet_session_language == "gb") {
		${"grading_title_".$SchemeIDArr[$i]} = iconv("UTF-8//IGNORE","GB2312",${"grading_title_".$SchemeIDArr[$i]});
	} else {
		${"grading_title_".$SchemeIDArr[$i]} = iconv("UTF-8//IGNORE","BIG5",${"grading_title_".$SchemeIDArr[$i]});
	}
	#${"grading_title_".$SchemeIDArr[$i]} = iconv("UTF-8//IGNORE","BIG5",${"grading_title_".$SchemeIDArr[$i]});
	
	// Added on 20080825: fix BIG5 words problem
	${"grading_title_".$SchemeIDArr[$i]} = addslashes(${"grading_title_".$SchemeIDArr[$i]});
	*/
	
	$SchemeInfo[$i]['SchemeTitle'] = trim(${"grading_title_".$SchemeIDArr[$i]})." ";
	$SchemeInfo[$i]['SchemeType'] = ${"grading_type_".$SchemeIDArr[$i]};
	$SchemeInfo[$i]['TopPercentage'] = ${"grading_range_".$SchemeIDArr[$i]};			// Range <=> TopPercentage
	$SchemeInfo[$i]['FullMark'] = ${"grading_fullmark_".$SchemeIDArr[$i]};//
	$SchemeInfo[$i]['PassMark'] = ${"grading_passingmark_".$SchemeIDArr[$i]};//
	$SchemeInfo[$i]['Pass'] = ${"grading_pass_".$SchemeIDArr[$i]};
	$SchemeInfo[$i]['Fail'] = ${"grading_fail_".$SchemeIDArr[$i]};
	
	$SchemeInfo[$i]['p_cnt'] = ${"P_Cnt_".$SchemeIDArr[$i]};
	$SchemeInfo[$i]['f_cnt'] = ${"F_Cnt_".$SchemeIDArr[$i]};
	$SchemeInfo[$i]['d_cnt'] = ${"D_Cnt_".$SchemeIDArr[$i]};
	
	/*
	* D : Distinction		  , P: Pass						, F: Fail
	* MR: Mark Range		  , PR: Percentage Range for 
	* a : lower limit / top % , b: Grade					, c: Grade Point
	*/
	/*if( ($SchemeInfo[$i]['TopPercentage'] == 0 && ${"D_MR_".$SchemeIDArr[$i]."_a"} == "" && ${"D_".$SchemeIDArr[$i]."_b"} == "" && ${"D_".$SchemeIDArr[$i]."_c"} == "") || 
		($SchemeInfo[$i]['TopPercentage'] != 0 && ${"D_PR_".$SchemeIDArr[$i]."_a"} == "" && ${"D_".$SchemeIDArr[$i]."_b"} == "" && ${"D_".$SchemeIDArr[$i]."_c"} == "") )
	{
		// Ignore the entry whose three fields are NULL
	}
	else {
		$SchemeInfo[$i]['RangeID_D'] = ${"RangeID_D_".$SchemeIDArr[$i]};
		$SchemeInfo[$i]['D_MR_a'] = ${"D_MR_".$SchemeIDArr[$i]."_a"};//
		$SchemeInfo[$i]['D_PR_a'] = ${"D_PR_".$SchemeIDArr[$i]."_a"};//
		$SchemeInfo[$i]['D_b'] = ${"D_".$SchemeIDArr[$i]."_b"};
		$SchemeInfo[$i]['D_c'] = ${"D_".$SchemeIDArr[$i]."_c"};//
		$SchemeInfo[$i]['D_d'] = ${"D_".$SchemeIDArr[$i]."_d"};//
	}*/
	$d_cnt = 0;
	for($j=0 ; $j<$SchemeInfo[$i]['d_cnt'] ; $j++){
		//debug_pr("P_MR_".$SchemeIDArr[$i]."_".$j."_a" .${"P_MR_".$SchemeIDArr[$i]."_".$j."_a"} );
		if(isset(${"D_MR_".$SchemeIDArr[$i]."_".$j."_a"})){
			
			if( ($SchemeInfo[$i]['TopPercentage'] == 0 && ${"D_MR_".$SchemeIDArr[$i]."_".$j."_a"} == "" && ${"D_".$SchemeIDArr[$i]."_".$j."_b"} == "" && ${"D_".$SchemeIDArr[$i]."_".$j."_c"} == "") || 
				($SchemeInfor[$i]['TopPercentage'] != 0 && ${"D_PR_".$SchemeIDArr[$i]."_".$j."_a"} == "" && ${"D_".$SchemeIDArr[$i]."_".$j."_b"} == "" && ${"D_".$SchemeIDArr[$i]."_".$j."_c"} == "") )
			{
				// Ignore the entry whose three fields are NULL
			}
			else {
				$SchemeInfo[$i]['RangeID_D'][] = ${"RangeID_D_".$SchemeIDArr[$i]."_".$j};
				$SchemeInfo[$i]['D_MR_a'][] = ${"D_MR_".$SchemeIDArr[$i]."_".$j."_a"};//
				$SchemeInfo[$i]['D_PR_a'][] = ${"D_PR_".$SchemeIDArr[$i]."_".$j."_a"};//
				$SchemeInfo[$i]['D_b'][] = ${"D_".$SchemeIDArr[$i]."_".$j."_b"};
				$SchemeInfo[$i]['D_c'][] = ${"D_".$SchemeIDArr[$i]."_".$j."_c"};//
				$SchemeInfo[$i]['D_d'][] = ${"D_".$SchemeIDArr[$i]."_".$j."_d"};//
				$SchemeInfo[$i]['D_e'][] = ${"D_".$SchemeIDArr[$i]."_".$j."_e"};// Description
				$d_cnt++;
			}
		}
	}
	$SchemeInfo[$i]['d_cnt'] = $d_cnt;
	
	$p_cnt = 0;
	for($j=0 ; $j<$SchemeInfo[$i]['p_cnt'] ; $j++){
		//debug_pr("P_MR_".$SchemeIDArr[$i]."_".$j."_a" .${"P_MR_".$SchemeIDArr[$i]."_".$j."_a"} );
		if(isset(${"P_MR_".$SchemeIDArr[$i]."_".$j."_a"})){
			
			if( ($SchemeInfo[$i]['TopPercentage'] == 0 && ${"P_MR_".$SchemeIDArr[$i]."_".$j."_a"} == "" && ${"P_".$SchemeIDArr[$i]."_".$j."_b"} == "" && ${"P_".$SchemeIDArr[$i]."_".$j."_c"} == "") || 
				($SchemeInfor[$i]['TopPercentage'] != 0 && ${"P_PR_".$SchemeIDArr[$i]."_".$j."_a"} == "" && ${"P_".$SchemeIDArr[$i]."_".$j."_b"} == "" && ${"P_".$SchemeIDArr[$i]."_".$j."_c"} == "") )
			{
				// Ignore the entry whose three fields are NULL
			}
			else {
				$SchemeInfo[$i]['RangeID_P'][] = ${"RangeID_P_".$SchemeIDArr[$i]."_".$j};
				$SchemeInfo[$i]['P_MR_a'][] = ${"P_MR_".$SchemeIDArr[$i]."_".$j."_a"};//
				$SchemeInfo[$i]['P_PR_a'][] = ${"P_PR_".$SchemeIDArr[$i]."_".$j."_a"};//
				$SchemeInfo[$i]['P_b'][] = ${"P_".$SchemeIDArr[$i]."_".$j."_b"};
				$SchemeInfo[$i]['P_c'][] = ${"P_".$SchemeIDArr[$i]."_".$j."_c"};//
				$SchemeInfo[$i]['P_d'][] = ${"P_".$SchemeIDArr[$i]."_".$j."_d"};//
				$SchemeInfo[$i]['P_e'][] = ${"P_".$SchemeIDArr[$i]."_".$j."_e"};//Description
				$p_cnt++;
			}
		}
	}
	$SchemeInfo[$i]['p_cnt'] = $p_cnt;
	
	$f_cnt = 0;
	for($j=0 ; $j<$SchemeInfo[$i]['f_cnt'] ; $j++){
		if(isset(${"F_MR_".$SchemeIDArr[$i]."_".$j."_a"})){
			
			if( ($SchemeInfo[$i]['range'] == 0 && ${"F_MR_".$SchemeIDArr[$i]."_".$j."_a"} == "" && ${"F_".$SchemeIDArr[$i]."_".$j."_b"} == "" && ${"F_".$SchemeIDArr[$i]."_".$j."_c"} == "") || 
				($SchemeInfo[$i]['range'] != 0 && ${"F_PR_".$SchemeIDArr[$i]."_".$j."_a"} == "" && ${"F_".$SchemeIDArr[$i]."_".$j."_b"} == "" && ${"F_".$SchemeIDArr[$i]."_".$j."_c"} == "") )
			{
				// Ignore the entry whose three fields are NULL
			}
			else {
				$SchemeInfo[$i]['RangeID_F'][] = ${"RangeID_F_".$SchemeIDArr[$i]."_".$j};
				$SchemeInfo[$i]['F_MR_a'][] = ${"F_MR_".$SchemeIDArr[$i]."_".$j."_a"};//
				$SchemeInfo[$i]['F_PR_a'][] = ${"F_PR_".$SchemeIDArr[$i]."_".$j."_a"};//
				$SchemeInfo[$i]['F_b'][] = ${"F_".$SchemeIDArr[$i]."_".$j."_b"};
				$SchemeInfo[$i]['F_c'][] = ${"F_".$SchemeIDArr[$i]."_".$j."_c"};//
				$SchemeInfo[$i]['F_d'][] = ${"F_".$SchemeIDArr[$i]."_".$j."_d"};//
				$SchemeInfo[$i]['F_e'][] = ${"F_".$SchemeIDArr[$i]."_".$j."_e"};//Description
				$f_cnt++;
			}
		}
	}
	$SchemeInfo[$i]['f_cnt'] = $f_cnt;
}

/*
echo "<pre>";
var_dump($SchemeInfo);
echo "</pre>";
die();
*/

#################################################################################################
# Prepare DB data for INSERT, UPDATE or DELETE on particular Scheme
#################################################################################################
$SchemeData = array();
$RangeData = array();

for($i=0 ; $i<count($SchemeInfo) ; $i++){
	if($SchemeInfo[$i]['SchemeID'] == "new" || $SchemeInfo[$i]['SchemeID'] == $sc_id){
		$SchemeData["SchemeID"] = ($ActionCode == "SAVEAS") ? "" : $sc_id;
		$SchemeData["SchemeTitle"] = $SchemeInfo[$i]['SchemeTitle'];
		$SchemeData["SchemeType"] = $SchemeInfo[$i]['SchemeType'];
		$SchemeData["FullMark"] = ($SchemeInfo[$i]['FullMark'] != "") ? $SchemeInfo[$i]['FullMark'] : "NULL";
		$SchemeData["PassMark"] = ($SchemeInfo[$i]['PassMark'] != "") ? $SchemeInfo[$i]['PassMark'] : "NULL";
		$SchemeData["TopPercentage"] = ($SchemeInfo[$i]["SchemeType"] == "H") ? $SchemeInfo[$i]['TopPercentage'] : "NULL";
		$SchemeData["Pass"] = $SchemeInfo[$i]['Pass'];
		$SchemeData["Fail"] = $SchemeInfo[$i]['Fail'];
		$p_cnt=$SchemeInfo[$i]['p_cnt'];
		$f_cnt=$SchemeInfo[$i]['f_cnt'];
		$d_cnt=$SchemeInfo[$i]['d_cnt'];
		
		// Range Info if any
		$num = 0;
		if($SchemeData["SchemeType"] == "H"){
			// Distinction
			/*$RangeData[$num]["GradingSchemeRangeID"] = ($ActionCode == "SAVEAS") ? "" : $SchemeInfo[$i]['RangeID_D'];
			$RangeData[$num]["SchemeID"] = ($ActionCode == "SAVEAS") ? "" : $sc_id;
			$RangeData[$num]["Nature"] = "D";
			$RangeData[$num]["LowerLimit"] = ($SchemeInfo[$i]['D_MR_a'] != "") ? $SchemeInfo[$i]['D_MR_a'] : "NULL";
			$RangeData[$num]["UpperLimit"] = ($SchemeInfo[$i]['D_PR_a'] != "") ? $SchemeInfo[$i]['D_PR_a'] : "NULL";
			$RangeData[$num]["Grade"] = $SchemeInfo[$i]['D_b'];
			$RangeData[$num]["GradePoint"] = ($SchemeInfo[$i]['D_c'] != "") ? $SchemeInfo[$i]['D_c'] : "NULL";
			$RangeData[$num]["AdditionalCriteria"] = ($SchemeInfo[$i]['D_d'] != "") ? $SchemeInfo[$i]['D_d'] : "NULL";
			$num++;*/
			$LowestPassMark = $SchemeData["FullMark"];
			if(count($SchemeInfo[$i]['D_MR_a']) > 0){
				for($j=0 ; $j<count($SchemeInfo[$i]['D_MR_a']) ; $j++){
					// Pass
					$RangeData[$num]["GradingSchemeRangeID"] = ($ActionCode == "SAVEAS") ? "" : $SchemeInfo[$i]['RangeID_D'][$j];
					$RangeData[$num]["SchemeID"] = ($ActionCode == "SAVEAS") ? "" : $sc_id;
					$RangeData[$num]["Nature"] = "D";
					$RangeData[$num]["LowerLimit"] = ($SchemeInfo[$i]['D_MR_a'][$j] != "") ? $SchemeInfo[$i]['D_MR_a'][$j] : "NULL";
					$RangeData[$num]["UpperLimit"] = ($SchemeInfo[$i]['D_PR_a'][$j] != "") ? $SchemeInfo[$i]['D_PR_a'][$j] : "NULL";
					$RangeData[$num]["Grade"] = $SchemeInfo[$i]['D_b'][$j];
					$RangeData[$num]["GradePoint"] = ($SchemeInfo[$i]['D_c'][$j] != "") ? $SchemeInfo[$i]['D_c'][$j] : "NULL";
					$RangeData[$num]["AdditionalCriteria"] = ($SchemeInfo[$i]['D_d'][$j] != "") ? $SchemeInfo[$i]['D_d'][$j] : "NULL";
					$RangeData[$num]["Description"] = ($SchemeInfo[$i]['D_e'][$j] != "") ? $SchemeInfo[$i]['D_e'][$j] : "";
					$num++;
				}
			}
			
			if(count($SchemeInfo[$i]['P_MR_a']) > 0){
				for($j=0 ; $j<count($SchemeInfo[$i]['P_MR_a']) ; $j++){
					// Pass
					$RangeData[$num]["GradingSchemeRangeID"] = ($ActionCode == "SAVEAS") ? "" : $SchemeInfo[$i]['RangeID_P'][$j];
					$RangeData[$num]["SchemeID"] = ($ActionCode == "SAVEAS") ? "" : $sc_id;
					$RangeData[$num]["Nature"] = "P";
					$RangeData[$num]["LowerLimit"] = ($SchemeInfo[$i]['P_MR_a'][$j] != "") ? $SchemeInfo[$i]['P_MR_a'][$j] : "NULL";
					$RangeData[$num]["UpperLimit"] = ($SchemeInfo[$i]['P_PR_a'][$j] != "") ? $SchemeInfo[$i]['P_PR_a'][$j] : "NULL";
					$RangeData[$num]["Grade"] = $SchemeInfo[$i]['P_b'][$j];
					$RangeData[$num]["GradePoint"] = ($SchemeInfo[$i]['P_c'][$j] != "") ? $SchemeInfo[$i]['P_c'][$j] : "NULL";
					$RangeData[$num]["AdditionalCriteria"] = ($SchemeInfo[$i]['P_d'][$j] != "") ? $SchemeInfo[$i]['P_d'][$j] : "NULL";
					$RangeData[$num]["Description"] = ($SchemeInfo[$i]['P_e'][$j] != "") ? $SchemeInfo[$i]['P_e'][$j] : "";
					
					$LowestPassMark = min($LowestPassMark,$RangeData[$num]["LowerLimit"]);
					$num++;
				}
			}
			
			if(count($SchemeInfo[$i]['F_MR_a']) > 0){
				for($j=0 ; $j<count($SchemeInfo[$i]['F_MR_a']) ; $j++){
					// Fail
					$RangeData[$num]["GradingSchemeRangeID"] = ($ActionCode == "SAVEAS") ? "" : $SchemeInfo[$i]['RangeID_F'][$j];
					$RangeData[$num]["SchemeID"] = ($ActionCode == "SAVEAS") ? "" : $sc_id;
					$RangeData[$num]["Nature"] = "F";
					$RangeData[$num]["LowerLimit"] = ($SchemeInfo[$i]['F_MR_a'][$j] != "") ? $SchemeInfo[$i]['F_MR_a'][$j] : "NULL";
					$RangeData[$num]["UpperLimit"] = ($SchemeInfo[$i]['F_PR_a'][$j] != "") ? $SchemeInfo[$i]['F_PR_a'][$j] : "NULL";
					$RangeData[$num]["Grade"] = $SchemeInfo[$i]['F_b'][$j];
					$RangeData[$num]["GradePoint"] = ($SchemeInfo[$i]['F_c'][$j] != "") ? $SchemeInfo[$i]['F_c'][$j] : "NULL";
					$RangeData[$num]["AdditionalCriteria"] = ($SchemeInfo[$i]['F_d'][$j] != "") ? $SchemeInfo[$i]['F_d'][$j] : "NULL";
					$RangeData[$num]["Description"] = ($SchemeInfo[$i]['F_e'][$j] != "") ? $SchemeInfo[$i]['F_e'][$j] : "";
					$num++;
				}
			}
		}
		if($SchemeData["TopPercentage"]==0)
			$SchemeData["PassMark"] = $LowestPassMark;
			
		continue;
	}
	
}

// Get All SchemeID
$AllExistSchemeID = $lreportcard->GET_GRADING_SCHEME_ID();

#################################################################################################
# DB Update Operation
#################################################################################################
$result = array();

if($ActionCode == "SAVE"){
	# Save Grading Scheme Operation
	if($sc_id != "" || $sc_id != null){
		$result['update_scheme'] = $lreportcard->UPDATE_GRADING_SCHEME_AND_RANGE($SchemeData, $RangeData);
		$newScName = $SchemeData["SchemeTitle"];
	}
}
else if($ActionCode == "SAVEAS"){
	# Save As New Grading Scheme Operation
	$oldIDAry = $lreportcard->GET_GRADING_SCHEME_ID();
	$result['save_as_new_scheme'] = $lreportcard->INSERT_GRADING_SCHEME_AND_RANGE($SchemeData, $RangeData);
	$newIDAry = $lreportcard->GET_GRADING_SCHEME_ID();
	$sc_id = array_diff($newIDAry,$oldIDAry); #Get New Scheme ID
	$sc_id = array_pop($sc_id);
	$newScName = $SchemeData["SchemeTitle"];
	
	echo 'jsGradingTypeArr['.$sc_id.'] = "'.$SchemeData["SchemeType"].'";'."\n";
}
else if($ActionCode == "DELETE"){
	# Delete Grading Scheme Operation
	if($sc_id != "" || $sc_id != null){
		$result['delete_scheme'] = $lreportcard->DELETE_GRADING_SCHEME_AND_RANGE($sc_id);
	}
}

# Call JS function of form_settings_edit.php to refresh the selection boxes select_seheme
	echo "RefreshSelectScheme('$ActionCode','$sc_id','$newScName','$p_cnt','$f_cnt','$d_cnt');\n";	

#################################################################################################

if(!in_array(false, $result))
	$msg = true;
else
	$msg = false;
	
	
#################################################################################################	
# Remark: $sc_id is the SchemeID that user is editing
#		  Case 1 : using NEW Scheme Template to save as a new grading scheme
#		  Case 2 : using EXISTING Scheme to save as a new grading scheme
# 		  Case 3 : modifing EXISTING Scheme to update the information
#		  Case 4 : deleting EXISTING Scheme affects other subjects which have assigned to that scheme even io other ClassLevelID
#
# In order to prevent any inconsistence information in Case 2
# Get the info of Existing Scheme, which is using as creating new scheme template, from DB once 
# and re-allocate the info back to the array with corresponding key ie. SchemeID
#################################################################################################
// get the newly inserted Scheme ID
if($ActionCode == "SAVEAS" && !in_array(false, $result)){
	$AllNewSchemeID = $lreportcard->GET_GRADING_SCHEME_ID();
	if(count($AllNewSchemeID) > 0){
		for($i=0 ; $i<count($AllNewSchemeID) ; $i++){
			// special handle if there is no existing grading scheme in the database
			if (count($AllExistSchemeID) == 0)
			{
				$newInsertSchemeID = $AllNewSchemeID[$i];
			}
			else
			{
				for($j=0 ; $j<count($AllExistSchemeID) ; $j++){
					if($AllExistSchemeID[$j] == $AllNewSchemeID[$i])
						continue;
					else if($j == (count($AllExistSchemeID)-1) && $AllExistSchemeID[$j] != $AllNewSchemeID[$i])
						$newInsertSchemeID = $AllNewSchemeID[$i];
				}
			}
		}
	}
} else {
	$newInsertSchemeID = '';
}

$ExistSchemeArr = array();
$ExistSchemeMain =  array();
$ExistSchemeMark =  array();
$DistinctMarkData = array();
$PassMarkData = array();
$FailMarkData = array();

if($ActionCode == "SAVEAS"){
	$updateArr = array();
	
	// restore the info of newly created scheme
	$updateArr[$newInsertSchemeID] = count($SchemeInfo);
	
	// restore the info of Existing Scheme which is used for creating new grading schemes
	if($sc_id != '' && $sc_id != 'new' && $sc_id != null){
		for($i=0 ; $i<count($SchemeInfo) ; $i++){
			if($SchemeInfo[$i]['SchemeID'] == $sc_id){
				//$index = $i;
				$updateArr[$sc_id] = $i;
				continue;
			}
		}
	}
	
	if(count($updateArr) > 0 ){
		foreach ($updateArr as $update_id => $index){
			$ExistSchemeArr = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($update_id);
			$ExistSchemeMain = $ExistSchemeArr;
			
			$DistinctMarkData = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($update_id, 'D');
		    $PassMarkData = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($update_id, 'P');
		    $FailMarkData = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($update_id, 'F');
			
			$SchemeInfo[$index]['SchemeID'] = $update_id;
			$SchemeInfo[$index]['SchemeTitle'] = stripslashes($ExistSchemeMain['SchemeTitle']);
			$SchemeInfo[$index]['SchemeType'] = $ExistSchemeMain['SchemeType'];
			$SchemeInfo[$index]['FullMark'] = $ExistSchemeMain['FullMark'];
			$SchemeInfo[$index]['PassMark'] = $ExistSchemeMain['PassMark'];
			$SchemeInfo[$index]['TopPercentage'] = $ExistSchemeMain['TopPercentage'];
			$SchemeInfo[$index]['Pass'] = $ExistSchemeMain['Pass'];
			$SchemeInfo[$index]['Fail'] = $ExistSchemeMain['Fail'];
			
			$SchemeInfo[$index]['d_cnt'] = count($DistinctMarkData);
			//debug_pr($DistinctMarkData);
			$SchemeInfo[$index]['p_cnt'] = count($PassMarkData);
			$SchemeInfo[$index]['f_cnt'] = count($FailMarkData);
			
			/*$SchemeInfo[$index]['RangeID_D'] = (count($DistinctMarkData) > 0) ? $DistinctMarkData[0]['GradingSchemeRangeID'] : '';
			$SchemeInfo[$index]['D_MR_a'] = (count($DistinctMarkData) > 0) ? $DistinctMarkData[0]['LowerLimit'] : '';
			$SchemeInfo[$index]['D_PR_a'] = (count($DistinctMarkData) > 0) ? $DistinctMarkData[0]['UpperLimit'] : '';
			$SchemeInfo[$index]['D_b'] = (count($DistinctMarkData) > 0) ? $DistinctMarkData[0]['Grade'] : '';
			$SchemeInfo[$index]['D_c'] = (count($DistinctMarkData) > 0) ? $DistinctMarkData[0]['GradePoint'] : '';*/
			if($SchemeInfo[$index]['d_cnt'] > 0){
				$SchemeInfo[$index]['RangeID_D'] = array();
				$SchemeInfo[$index]['D_MR_a'] = array();
				$SchemeInfo[$index]['D_PR_a'] = array();
				$SchemeInfo[$index]['D_b'] = array();
				$SchemeInfo[$index]['D_c'] = array();
				for($j=0 ; $j<$SchemeInfo[$index]['d_cnt'] ; $j++){
					$SchemeInfo[$index]['RangeID_D'][] = $DistinctMarkData[$j]['GradingSchemeRangeID'];
					$SchemeInfo[$index]['D_MR_a'][] = $DistinctMarkData[$j]['LowerLimit'];
					$SchemeInfo[$index]['D_PR_a'][] = $DistinctMarkData[$j]['UpperLimit'];
					$SchemeInfo[$index]['D_b'][] = $DistinctMarkData[$j]['Grade'];
					$SchemeInfo[$index]['D_c'][] = $DistinctMarkData[$j]['GradePoint'];
				}
			}
			
			if($SchemeInfo[$index]['p_cnt'] > 0){
				$SchemeInfo[$index]['RangeID_P'] = array();
				$SchemeInfo[$index]['P_MR_a'] = array();
				$SchemeInfo[$index]['P_PR_a'] = array();
				$SchemeInfo[$index]['P_b'] = array();
				$SchemeInfo[$index]['P_c'] = array();
				for($j=0 ; $j<$SchemeInfo[$index]['p_cnt'] ; $j++){
					$SchemeInfo[$index]['RangeID_P'][] = $PassMarkData[$j]['GradingSchemeRangeID'];
					$SchemeInfo[$index]['P_MR_a'][] = $PassMarkData[$j]['LowerLimit'];
					$SchemeInfo[$index]['P_PR_a'][] = $PassMarkData[$j]['UpperLimit'];
					$SchemeInfo[$index]['P_b'][] = $PassMarkData[$j]['Grade'];
					$SchemeInfo[$index]['P_c'][] = $PassMarkData[$j]['GradePoint'];
				}
			}
		
			if($SchemeInfo[$index]['f_cnt'] > 0){
				$SchemeInfo[$index]['RangeID_F'] = array();
				$SchemeInfo[$index]['F_MR_a'] = array();
				$SchemeInfo[$index]['F_PR_a'] = array();
				$SchemeInfo[$index]['F_b'] = array();
				$SchemeInfo[$index]['F_c'] = array();
				for($j=0 ; $j<$SchemeInfo[$index]['f_cnt'] ; $j++){
					$SchemeInfo[$index]['RangeID_F'][] = $FailMarkData[$j]['GradingSchemeRangeID'];
					$SchemeInfo[$index]['F_MR_a'][] = $FailMarkData[$j]['LowerLimit'];
					$SchemeInfo[$index]['F_PR_a'][] = $FailMarkData[$j]['UpperLimit'];
					$SchemeInfo[$index]['F_b'][] = $FailMarkData[$j]['Grade'];
					$SchemeInfo[$index]['F_c'][] = $FailMarkData[$j]['GradePoint'];
				}
			}
		}
	}
	
	// Reset NEW Scheme Template
	$SchemeInfo[0]['SchemeID'] = 'new';
	$SchemeInfo[0]['SchemeTitle'] = '';
	$SchemeInfo[0]['SchemeType'] = '';
	$SchemeInfo[0]['FullMark'] = '';
	$SchemeInfo[0]['PassMark'] = '';
	$SchemeInfo[0]['TopPercentage'] = '';
	$SchemeInfo[0]['Pass'] = '';
	$SchemeInfo[0]['Fail'] = '';
	
	$SchemeInfo[0]['p_cnt'] = 0;
	$SchemeInfo[0]['f_cnt'] = 0;
	$SchemeInfo[0]['d_cnt'] = 0;
	
	/*$SchemeInfo[0]['RangeID_D'] = '';
	$SchemeInfo[0]['D_MR_a'] = '';
	$SchemeInfo[0]['D_PR_a'] = '';
	$SchemeInfo[0]['D_b'] = '';
	$SchemeInfo[0]['D_c'] = '';*/
	$SchemeInfo[0]['RangeID_D'] = array();
	$SchemeInfo[0]['D_MR_a'] = array();
	$SchemeInfo[0]['D_PR_a'] = array();
	$SchemeInfo[0]['D_b'] = array();
	$SchemeInfo[0]['D_c'] = array();


	$SchemeInfo[0]['RangeID_P'] = array();
	$SchemeInfo[0]['P_MR_a'] = array();
	$SchemeInfo[0]['P_PR_a'] = array();
	$SchemeInfo[0]['P_b'] = array();
	$SchemeInfo[0]['P_c'] = array();
	
	$SchemeInfo[0]['RangeID_F'] = array();
	$SchemeInfo[0]['F_MR_a'] = array();
	$SchemeInfo[0]['F_PR_a'] = array();
	$SchemeInfo[0]['F_b'] = array();
	$SchemeInfo[0]['F_c'] = array();
}

#################################################################################################	
# In order to prevent any inconsistence information in Case 4
# Remove the SchemeID, which is matched with the Deleted SchemeID, from the Array [$SchemeInfo] 
# before re-generating Form Scheme Details
#################################################################################################
if($ActionCode == "DELETE"){
	if(!empty($SubjectInfo)){
		foreach($SubjectInfo as $SubjectID => $Data){
			if($Data['schemeID'] == $sc_id)
				$Data['schemeID'] = "";
		}
	}

	$NewSchemeInfo = array();
	for($i=0 ; $i<count($SchemeInfo) ; $i++){
		if($SchemeInfo[$i]['SchemeID'] != $sc_id){
			$NewSchemeInfo[] = $SchemeInfo[$i];
		}
	}
}
else{
	$NewSchemeInfo = $SchemeInfo;
}


#################################################################################################
# Re-genearte Form Scheme Details 
#################################################################################################

$count = 0;
$row = 1;
$rx = '';

// Get the last sub-subject of each subjects if any & use for css 
$LastParentSubjectID = '';
$LastCmpSubject = array();
$CmpSubject = array();

if(!empty($SubjectInfo)){
	foreach($SubjectInfo as $SubjectID => $Data){
		if($Data['parentSubjectID'] == $SubjectID)
			$LastParentSubjectID = $SubjectID;
			
		if($Data['is_cmpSubject'] == 1){
			$CmpSubject[$Data['parentSubjectID']][] = $SubjectID;
			$LastCmpSubject[$Data['parentSubjectID']] = $SubjectID;
		}
	}
}	

// Main		
$cmpNoUpFlag = 0;	// check for determining the start of sub-subject for each parent subject
$cmpNoDownFlag = 0;	
					
if(!empty($SubjectInfo)){
	foreach($SubjectInfo as $SubjectID => $Data){
		
		$is_CmpSubject = ($Data['is_cmpSubject']==0) ? 0 : 1;
		$row_css = ($count % 2 == 0 || $is_CmpSubject) ? 'retablerow1' : 'retablerow2';
		$head_1_css = ((count($LastCmpSubject) > 0 && $LastCmpSubject[$Data['parentSubjectID']] == $SubjectID) || $Data['is_cmpSubject']==0) ? 'tablelist_subject' : 'tablelist_subject_head';
		$head_2_css = ($is_CmpSubject==0) ? 'tablelist_subject' : 'tablelist_sub_subject';
		$row_css .= ($is_CmpSubject==0) ? '' : ' retablerow_subsubject_line';
		
		if(count($CmpSubject) > 0){
			$cmpNoUpFlag = ($CmpSubject[$Data['parentSubjectID']][0] == $SubjectID) ? 1 : 0;	
			$cmpNoDownFlag = ($CmpSubject[$Data['parentSubjectID']][count($CmpSubject[$Data['parentSubjectID']])-1] == $SubjectID) ? 1 : 0;
		}
					
		// GET Grading Scheme Data
		$noSchemeFlag = 0;	// check for any scheme is assigned to each subject : 1-no scheme, 	0- has scheme
		$SchemeData = array();
		$SchemeMainArr = array();
		
		if($Data['schemeID'] != null || $Data['schemeID'] != ""){
			$SchemeData = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($Data['schemeID']);
			$SchemeMainArr = $SchemeData;
			
			if(count($SchemeMainArr) == 0)
				$noSchemeFlag = 1;
		}
		else {
			$noSchemeFlag = 1;	
		}
		
		// Generate SelectBox of Grading Scheme
		$AllSchemeData = array();
		$AllSchemeData = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO();
		$select = '<option value=""> --'.$eReportCard['SelectScheme'].'-- </option>';
		for($k=0 ; $k<count($AllSchemeData) ; $k++)
			$select .= '<option value="'.$AllSchemeData[$k]['SchemeID'].'" '.(($AllSchemeData[$k]['SchemeID'] == $Data['schemeID']) ? "selected" : "").'>'.$AllSchemeData[$k]['SchemeTitle'].'</option>';
		
		// for Main Subject
		$rx .= '<tr><td width="15" align="left" class="'.$head_1_css.'">';
		$rx .= '<table border="0" cellspacing="0" cellpadding="2">';
		if($Data['is_cmpSubject']){		// for cmpSubject
	        $rx .= '<tr><td><div id="div_up_'.$SubjectID.'" style="display:'.(($cmpNoUpFlag == 1) ? 'none' : 'block').'"><a name="move_up[]" id="move_up'.$SubjectID.'" href="javascript:void(0)" class="contenttool" onclick="jMOVE_TABLE_ROW(this, \'1\', \''.$SubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_sort_a_off.gif" width="13" height="13" border="0" alt="'.$eReportCard['MoveUp'].'"></a></div></td></tr>';
	        $rx .= '<tr><td><div id="div_down_'.$SubjectID.'" style="display:'.(($cmpNoDownFlag == 1) ? 'none' : 'block').'"><a name="move_down[]" id="move_down'.$SubjectID.'" href="javascript:void(0)" class="contenttool" onclick="jMOVE_TABLE_ROW(this, \'-1\', \''.$SubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_sort_d_off.gif" width="13" height="13" border="0" alt="'.$eReportCard['MoveDown'].'"></a></div></td></tr>';
        }
        else {		// for Main Subject
	        $rx .= '<tr><td><div id="div_up_'.$SubjectID.'" style="display:'.(($count != 0) ? 'block' : 'none').'"><a name="move_up[]" id="move_up'.$SubjectID.'" href="javascript:void(0)" class="contenttool" onclick="jMOVE_TABLE_ROW(this, \'1\', \''.$SubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_sort_a_off.gif" width="13" height="13" border="0" alt="'.$eReportCard['MoveUp'].'"></a></div></td></tr>';
	        $rx .= '<tr><td><div id="div_down_'.$SubjectID.'" style="display:'.(($LastParentSubjectID == $Data['parentSubjectID'] || $count == count($SubjectDataArr)-1) ? 'none' : 'block').'"><a name="move_down[]" id="move_down'.$SubjectID.'" href="javascript:void(0)" class="contenttool" onclick="jMOVE_TABLE_ROW(this, \'-1\', \''.$SubjectID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_sort_d_off.gif" width="13" height="13" border="0" alt="'.$eReportCard['MoveDown'].'"></a></div></td></tr>';
        }
		$rx .= '</tr></table>';
		$rx .= '</td>';
		$rx .= '<td width="25%" class="'.$head_2_css.'">'.$Data['subjectName'];
		$rx .= '<input type="hidden" name="subject_name_'.$SubjectID.'" id="subject_name_'.$SubjectID.'" value="'.$Data['subjectName'].'"/>';
		$rx .= '<input type="hidden" name="display_order_'.$SubjectID.'" id="display_order_'.$SubjectID.'" value="'.$Data['displayOrder'].'"/>';
		$rx .= '<input type="hidden" name="cmpSubjectFlag_'.$SubjectID.'" id="cmpSubjectFlag_'.$SubjectID.'" value="'.$is_CmpSubject.'"/>';
		$rx .= '<input type="hidden" name="parent_subject_id_'.$SubjectID.'" id="parent_subject_id_'.$SubjectID.'" value="'.$Data['parentSubjectID'].'"/>';
		$rx .= '<input type="hidden" name="subjectID[]" id="subjectID_'.($row).'" value="'.$SubjectID.'"/></td>';
		$rx .= '<td id="td_select_'.$SubjectID.'" align="center" class="'.$row_css.'">';
		
		$rx .= '<table border="0" cellspacing="0" cellpadding="0"><tr>';
		$rx .= '<td align="center"><select name="select_scheme_'.$SubjectID.'" id="select_scheme_'.$SubjectID.'" class="tabletexttoptext" onMouseOver="jSHOW_GRADING_SCHEME(document.getElementById(\'img_edit_'.$SubjectID.'\'), \''.$SubjectID.'\', \'0\')"  onMouseOut="jHIDE_GRADING_SCHEME(\''.$SubjectID.'\', \'\', \'0\')" onchange="jCHANGE_GRADING_METHOD(\''.$SubjectID.'\')">'.$select.'</select></td>';
		$rx .= '<td><a href="javascript:jSHOW_GRADING_SCHEME(document.getElementById(\'img_edit_'.$SubjectID.'\'), \''.$SubjectID.'\', \'1\')"><img id="img_edit_'.$SubjectID.'" src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" alt="'.$button_edit.' '.$eReportCard['GradingScheme'].'"></a></td>';
		$rx .= '</tr></table>';
		
		$rx .= '</td>';
		/*
		* dsiplay result
		* Input Scale / Display Result: 0-Grade ; 1-Mark
		* Radio : scale_[SubjectID]     , display_[SubjectID]
		*/
        $scale_pf_str = '--';
        $display_pf_str = '--';
        
        $scale_h_str = '<input type="radio" name="scale_'.$SubjectID.'" id="scale_'.$SubjectID.'_2" value="M" '.(($Data['scaleInput'] == "M") ? "checked" : "").'>'.$eReportCard['Mark'].' &nbsp;&nbsp;';
    	$scale_h_str .= '<input type="radio" name="scale_'.$SubjectID.'" id="scale_'.$SubjectID.'_1" value="G" '.(($Data['scaleInput'] != "M") ? "checked" : "").'>'.$eReportCard['Grade'].'';
    	$display_h_str = '<input type="radio" name="display_'.$SubjectID.'" id="display_'.$SubjectID.'_2" value="M" '.(($Data['scaleDisplay'] == "M") ? "checked" : "").'>'.$eReportCard['Mark'].' &nbsp;&nbsp;';
    	$display_h_str .= '<input type="radio" name="display_'.$SubjectID.'" id="display_'.$SubjectID.'_1" value="G" '.(($Data['scaleDisplay'] != "M") ? "checked" : "").'>'.$eReportCard['Grade'].'';
    	
        $rx .= '<td id="td_scale_'.$SubjectID.'" align="center" class="'.$row_css.' '.$dot_css.'"><span class="tabletext"><label>';
        $rx .= '<div id="div_scale_honor_'.$SubjectID.'" style="display:'.(($SchemeMainArr['SchemeType'] == "PF") ? 'none' : 'block').'">'.$scale_h_str.'</div>';
        $rx .= '<div id="div_scale_passfail_'.$SubjectID.'" style="display:'.(($SchemeMainArr['SchemeType'] == "PF") ? 'block' : 'none').'">'.$scale_pf_str.'</div>';
        $rx .= '</label></span></td>';        
        $rx .= '<td id="td_display_'.$SubjectID.'" align="center" class="'.$row_css.' '.$dot_css.'"><span class="tabletext"><label>';
        $rx .= '<div id="div_display_honor_'.$SubjectID.'" style="display:'.(($SchemeMainArr['SchemeType'] == "PF") ? 'none' : 'block').'">'.$display_h_str.'</div>';
        $rx .= '<div id="div_display_passfail_'.$SubjectID.'" style="display:'.(($SchemeMainArr['SchemeType'] == "PF") ? 'block' : 'none').'">'.$display_pf_str.'</div>';
        $rx .= '</label></span></td>';
        $rx .= '</tr>';

		$count++;
		$row++;
	}
	
	############ Grand Mark Granding Scheme ############
	$GrandSchemeIndexArr = $lreportcard->Get_Grand_Mark_Grading_Scheme_Index_Arr($ClassLevelID);
	$GrandSchemeInfoArr = $lreportcard->Get_Grand_Mark_Grading_Scheme($ClassLevelID,$ReportID);
	
	foreach ($GrandSchemeIndexArr as $GrandMarkIndex => $GrandMarkType)
	{
		$GrandAverageSchemeData = $GrandSchemeInfoArr[$GrandMarkIndex];
		$row_css = ($count % 2 == 0) ? 'retablerow1' : 'retablerow2';
		$head_1_css = 'tablelist_subject';
		$head_2_css = 'tablelist_subject';
		
		$cmpNoUpFlag = 0;
		$cmpNoDownFlag = 0;
					
		// GET Grading Scheme Data
		$noSchemeFlag = 0;	// check for any scheme is assigned to each subject : 1-no scheme, 	0- has scheme
		$SchemeData = array();
		$SchemeMainArr = array();
		
		if($GrandAverageSchemeData['SchemeID'] != null || $GrandAverageSchemeData['SchemeID'] != ""){
			$SchemeData = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($GrandAverageSchemeData['SchemeID']);
			$SchemeMainArr = $SchemeData;
			
			if(count($SchemeMainArr) == 0)
				$noSchemeFlag = 1;
		}
		else {
			$noSchemeFlag = 1;	
		}
		
		// Generate SelectBox of Grading Scheme
		$AllSchemeIDNameMap = array();
		$AllSchemeData = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO();
		$select = '<option value=""> --'.$eReportCard['SelectScheme'].'-- </option>';
		for($k=0 ; $k<count($AllSchemeData) ; $k++) {
			$AllSchemeIDNameMap[$AllSchemeData[$k]['SchemeID']] = $AllSchemeData[$k]['SchemeTitle'];
			$select .= '<option value="'.$AllSchemeData[$k]['SchemeID'].'" '.(($AllSchemeData[$k]['SchemeID'] == $GrandAverageSchemeData['SchemeID']) ? "selected" : "").'>'.$AllSchemeData[$k]['SchemeTitle'].'</option>';
		}
		
		// for Main Subject
		//$rx .= '<tr><td>&nbsp;</td></tr>';
		$rx .= '<tr><td width="15" align="left" id="td_head_'.$GrandMarkType.'" class="'.$head_1_css.'">';
		$rx .= '<table border="0" cellspacing="0" cellpadding="2">';
		$rx .= '<tr><td>&nbsp;</td></tr>';
	    $rx .= '<tr><td>&nbsp;</td></tr>';
        $rx .= '</tr></table>';
        $rx .= '</td>';
        $rx .= '<td width="25%" class="'.$head_2_css.'"><b>'.$eReportCard['Template'][$GrandMarkType.'En'].'<br />'.$eReportCard['Template'][$GrandMarkType.'Ch'].'</b>';
         $rx .= '<td id="td_select_GrandAverage" align="center" class="'.$row_css.'">';
        $rx .= '<table border="0" cellspacing="0" cellpadding="0"><tr>';
        
        $rx .= '<td align="center"><select name="select_scheme_'.$GrandMarkType.'" id="select_scheme_'.$GrandMarkType.'" class="tabletexttoptext" onMouseOver="jSHOW_GRADING_SCHEME1(document.getElementById(\'img_edit_'.$GrandMarkType.'\'), \''.$GrandMarkType.'\', \'0\')"  onMouseOut="jHIDE_GRADING_SCHEME1(\''.$GrandMarkType.'\', \'\', \'0\')" onchange="jCHANGE_GRADING_METHOD(\''.$GrandMarkType.'\')">'.$select.'</select></td>';
	    $rx .= '<td><a href="javascript:jSHOW_GRADING_SCHEME1(document.getElementById(\'img_edit_'.$GrandMarkType.'\'), \''.$GrandMarkType.'\', \'1\')"><img id="img_edit_'.$GrandMarkType.'" src="'.$image_path.'/'.$LAYOUT_SKIN.'/icon_edit_b.gif" width="20" height="20" border="0" alt="'.$button_edit.' '.$eReportCard['GradingScheme'].'"></a></td>';
	            
        $rx .= '</tr></table>';
        $rx .= '</td>';
        /*
        * dsiplay result
        * Input Scale / Display Result: 0-Grade ; 1-Mark
        * Radio : scale_[SubjectID]     , display_[SubjectID]
        */
        $scale_pf_str = '--';
        $display_pf_str = '--';
        
		//$scale_h_str = '<input type="radio" name="scale_GrandAverage" id="scale_GrandAverage_2" value="M" '.(($GrandAverageSchemeData['scaleInput'] == "M") ? "checked" : "").'>'.$eReportCard['Mark'].' &nbsp;&nbsp;';
    	//$scale_h_str .= '<input type="radio" name="scale_GrandAverage" id="scale_GrandAverage_1" value="G" '.(($GrandAverageSchemeData['scaleInput'] == "G") ? "checked" : "").'>'.$eReportCard['Grade'];
		$scale_h_str = '--';
    	
		$display_h_str = '<input type="radio" name="display_'.$GrandMarkType.'" id="display_'.$GrandMarkType.'_2" value="M" '.(($GrandAverageSchemeData['ScaleDisplay'] == "M") ? "checked" : "").'>'.$eReportCard['Mark'].' &nbsp;&nbsp;';
		$display_h_str .= '<input type="radio" name="display_'.$GrandMarkType.'" id="display_'.$GrandMarkType.'_1" value="G" '.(($GrandAverageSchemeData['ScaleDisplay'] == "G") ? "checked" : "").'>'.$eReportCard['Grade'];
		
        $rx .= '<td id="td_scale_'.$GrandMarkType.'" align="center" class="'.$row_css.' '.$dot_css.'"><span class="tabletext"><label>';
        $rx .= '<div id="div_scale_honor_'.$GrandMarkType.'" style="display:'.(($SchemeMainArr['SchemeType'] == "PF") ? 'none' : 'block').'">'.$scale_h_str.'</div>';
        $rx .= '<div id="div_scale_passfail_'.$GrandMarkType.'" style="display:'.(($SchemeMainArr['SchemeType'] == "PF") ? 'block' : 'none').'">'.$scale_pf_str.'</div>';
        $rx .= '</label></span></td>';
        $rx .= '<td id="td_display_'.$GrandMarkType.'" align="center" class="'.$row_css.' '.$dot_css.'"><span class="tabletext"><label>';
        $rx .= '<div id="div_display_honor_'.$GrandMarkType.'" style="display:'.(($SchemeMainArr['SchemeType'] == "PF") ? 'none' : 'block').'">'.$display_h_str.'</div>';
        $rx .= '<div id="div_display_passfail_'.$GrandMarkType.'" style="display:'.(($SchemeMainArr['SchemeType'] == "PF") ? 'block' : 'none').'">'.$display_pf_str.'</div>';
        $rx .= '</label></span></td>';
        $rx .= '</tr>';

		$flag = 1;
		$count++;
		$row++;
	}
	############ End of Grand Mark Granding Scheme ############
}
else {
	$rx .= '<tr><td width="15" align="left" class="tablelist_sub_subject">&nbsp;</td>';
    $rx .= '<td colspan="4" align="center" width="100%" class="tabletext tablelist_sub_subject" height="50">There is no record at this moment.</td>';
    $rx .= '</tr>';
}


#################################################################################################
# Regenerate Subject Form Setting
#################################################################################################

// Start of Javascript Global variables
$subStr = "var subjectArr = new Array(";
if(count($SubjectInfo) > 0){
	$i = 0;
	foreach($SubjectInfo as $SubjectID => $Data){
		if($i > 0)
			$subStr .= ", ";
		$subStr .= "'".$SubjectID."'";
		$i++;
	}
}
$subStr .= ");";

$ArrNewSchemeID = array();
$AllNewSchemeID = $lreportcard->GET_GRADING_SCHEME_ID();
$schStr = "var schemeArr = new Array(";
if(count($AllNewSchemeID) > 0){
  	for($i=0 ; $i<count($AllNewSchemeID) ; $i++){
		if($i > 0)
			$schStr .= ", ";
		$schStr .= "'".$AllNewSchemeID[$i]."'";
	}
}
$schStr .= ");";
//echo $schStr;

$js_global_var = '<script language="javascript">';
$js_global_var .= $subStr."\n";
$js_global_var .= $schStr."\n";
$js_global_var .= '</script>';
// End of Javascript Global variables 


// Start of Selection Box of Grading Scheme use as a Template


$AllSchemeData = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO();
$select = '<option value=""> --Select All As-- </option>';
for($k=0 ; $k<count($AllSchemeData) ; $k++)
	$select .= '<option value="'.$AllSchemeData[$k]['SchemeID'].'">'.$AllSchemeData[$k]['SchemeTitle'].'</option>';

$sc_template_str = '<div id="div_scheme_template" style="display:none">';
$sc_template_str .= '<select id="scheme_template" name="scheme_template">';
$sc_template_str .= $select;
$sc_template_str .= '</select>';
$sc_template_str .= '</div>';
// End of Selection Box of Grading Scheme use as a Template;

// Start of grading scheme div for view
$div_str1 = '';
for($i=0 ; $i<count($AllSchemeData) ; $i++){
	$div_str1 .= '<div id="scheme_'.$AllSchemeData[$i]['SchemeID'].'" style="position:absolute; width:280px; z-index:1; visibility: hidden; border-top:solid 1px; border-top-color:#999999; border-left:solid 1px; border-left-color:#999999; border-right:solid 2px; border-right-color:#646464; border-bottom:solid 2px; border-bottom-color:#646464">';
    $div_str1 .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#feffed">';
    $div_str1 .= '<tr><td>';
    $div_str1 .= '<table width="100%" border="0" cellspacing="0" cellpadding="5">';
    $div_str1 .= '<tr><td>';
    
    if($AllSchemeData[$i]['SchemeType'] == "H"){
	    $DistinctMarkData = array();
	    $PassMarkData = array();
	    $FailMarkData = array();
	    
	    $DistinctMarkData = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($AllSchemeData[$i]['SchemeID'], 'D');
	    $PassMarkData = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($AllSchemeData[$i]['SchemeID'], 'P');
	    $FailMarkData = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($AllSchemeData[$i]['SchemeID'], 'F');
	    
	    // Full Mark
	    $full_mark = ($AllSchemeData[$i]['FullMark'] != "") ? $lreportcard->ReturnTextwithStyle($AllSchemeData[$i]['FullMark'], 'HighLight', 'Distinction') : '&nbsp;';
	    
	    // Distinction
	    /*if(count($DistinctMarkData) > 0){
		    $distinct_mark = '<div id="div_D_MR_tips_'.$AllSchemeData[$i]['SchemeID'].'" style="display:'.(($AllSchemeData[$i]['TopPercentage'] == 0) ? "block" : "none").'">';
		    $distinct_mark .= (($DistinctMarkData[0]['LowerLimit'] != "") ? $lreportcard->ReturnTextwithStyle($DistinctMarkData[0]['LowerLimit'], 'HighLight', 'Distinction') : "&nbsp;").'</div>';
		    $distinct_mark .= '<div id="div_D_PR_tips_'.$AllSchemeData[$i]['SchemeID'].'" style="display:'.(($AllSchemeData[$i]['TopPercentage'] != 0) ? "block" : "none").'">';
		    $distinct_mark .= (($DistinctMarkData[0]['UpperLimit'] != "") ? $lreportcard->ReturnTextwithStyle($DistinctMarkData[0]['UpperLimit'], 'HighLight', 'Distinction') : "&nbsp;").' %</div>';
		    $distinct_grade = ($DistinctMarkData[0]['Grade'] != "") ? $lreportcard->ReturnTextwithStyle($DistinctMarkData[0]['Grade'], 'HighLight', 'Distinction') : "&nbsp;";
		}*/
		 if(count($DistinctMarkData) > 0){
		    for($k=0 ; $k<count($DistinctMarkData) ; $k++){
			    $pass_mark = '<div id="div_D_MR_tips_'.$AllSchemeData[$i]['SchemeID'].'_'.$k.'" style="display:'.(($AllSchemeData[$i]['TopPercentage'] == 0) ? "block" : "none").'">';
			    $pass_mark .= (($DistinctMarkData[$k]['LowerLimit'] != "") ? $lreportcard->ReturnTextwithStyle($DistinctMarkData[$k]['LowerLimit'], 'HighLight', 'Pass') : "&nbsp;").'</div>';
			    $pass_range = '<div id="div_D_PR_tips_'.$AllSchemeData[$i]['SchemeID'].'_'.$k.'" style="display:'.(($AllSchemeData[$i]['TopPercentage'] != 0) ? "block" : "none").'">';
			    $pass_range .= (($DistinctMarkData[$k]['UpperLimit'] != "") ? $lreportcard->ReturnTextwithStyle($DistinctMarkData[$k]['UpperLimit'], 'HighLight', 'Pass') : "&nbsp;").' %</div>';
			    $pass_grade = ($DistinctMarkData[$k]['Grade'] != "") ? $lreportcard->ReturnTextwithStyle($DistinctMarkData[$k]['Grade'], 'HighLight', 'Pass') : "&nbsp;";
			    
			    $pass_str .= '<tr>';
			    $pass_str .= '<td class="tabletext">'.(($k==0) ? $eReportCard['Pass'] : '').'</td>';
		        $pass_str .= '<td align="center" class="tabletext formfieldtitle">'.$pass_mark.$pass_range.'</td>';
		        $pass_str .= '<td align="center" class="tabletext formfieldtitle">'.$pass_grade.'</td>';
		        $pass_str .= '</tr>';
	    	}
		}
		else {
			$pass_str .= '<tr>';
		    $pass_str .= '<td class="tabletext">'.$eReportCard['Distinction'].'</td>';
	        $pass_str .= '<td align="center" class="tabletext formfieldtitle">-</td>';
	        $pass_str .= '<td align="center" class="tabletext formfieldtitle">-</td>';
	        $pass_str .= '</tr>';
		}
	    
	    // Pass
	    $pass_str = '';
	    if(count($PassMarkData) > 0){
		    for($k=0 ; $k<count($PassMarkData) ; $k++){
			    $pass_mark = '<div id="div_P_MR_tips_'.$AllSchemeData[$i]['SchemeID'].'_'.$k.'" style="display:'.(($AllSchemeData[$i]['TopPercentage'] == 0) ? "block" : "none").'">';
			    $pass_mark .= (($PassMarkData[$k]['LowerLimit'] != "") ? $lreportcard->ReturnTextwithStyle($PassMarkData[$k]['LowerLimit'], 'HighLight', 'Pass') : "&nbsp;").'</div>';
			    $pass_range = '<div id="div_P_PR_tips_'.$AllSchemeData[$i]['SchemeID'].'_'.$k.'" style="display:'.(($AllSchemeData[$i]['TopPercentage'] != 0) ? "block" : "none").'">';
			    $pass_range .= (($PassMarkData[$k]['UpperLimit'] != "") ? $lreportcard->ReturnTextwithStyle($PassMarkData[$k]['UpperLimit'], 'HighLight', 'Pass') : "&nbsp;").' %</div>';
			    $pass_grade = ($PassMarkData[$k]['Grade'] != "") ? $lreportcard->ReturnTextwithStyle($PassMarkData[$k]['Grade'], 'HighLight', 'Pass') : "&nbsp;";
			    
			    $pass_str .= '<tr>';
			    $pass_str .= '<td class="tabletext">'.(($k==0) ? $eReportCard['Pass'] : '').'</td>';
		        $pass_str .= '<td align="center" class="tabletext formfieldtitle">'.$pass_mark.$pass_range.'</td>';
		        $pass_str .= '<td align="center" class="tabletext formfieldtitle">'.$pass_grade.'</td>';
		        $pass_str .= '</tr>';
	    	}
		}
		else {
			$pass_str .= '<tr>';
		    $pass_str .= '<td class="tabletext">'.$eReportCard['Pass'].'</td>';
	        $pass_str .= '<td align="center" class="tabletext formfieldtitle">-</td>';
	        $pass_str .= '<td align="center" class="tabletext formfieldtitle">-</td>';
	        $pass_str .= '</tr>';
		}
		
	    // Fail
	    $fail_str = '';
	    if(count($FailMarkData) > 0){
		    for($k=0 ; $k<count($FailMarkData) ; $k++){
		    	$fail_mark = '<div id="div_F_MR_tips_'.$AllSchemeData[$i]['SchemeID'].'_'.$k.'" style="display:'.(($AllSchemeData[$i]['TopPercentage'] == 0) ? "block" : "none").'">';
			    $fail_mark .= (($FailMarkData[$k]['LowerLimit'] != "") ? $lreportcard->ReturnTextwithStyle($FailMarkData[$k]['LowerLimit'], 'HighLight', 'Fail') : "&nbsp;").'</div>';
			    $fail_range = '<div id="div_F_PR_tips_'.$AllSchemeData[$i]['SchemeID'].'_'.$k.'" style="display:'.(($AllSchemeData[$i]['TopPercentage'] != 0) ? "block" : "none").'">';
			    $fail_range .= (($FailMarkData[$k]['UpperLimit'] != "") ? $lreportcard->ReturnTextwithStyle($FailMarkData[$k]['UpperLimit'], 'HighLight', 'Fail') : "&nbsp;").' %</div>';
			    $fail_grade = ($FailMarkData[$k]['Grade'] != "") ? $lreportcard->ReturnTextwithStyle($FailMarkData[$k]['Grade'], 'HighLight', 'Fail') : "&nbsp;";
			    
			    $fail_str .= '<tr>';
			    $fail_str .= '<td class="tabletext">'.(($k==0) ? $eReportCard['Fail'] : '').'</td>';
		        $fail_str .= '<td align="center" class="tabletext formfieldtitle">'.$fail_mark.$fail_range.'</td>';
		        $fail_str .= '<td align="center" class="tabletext formfieldtitle">'.$fail_grade.'</td>';
		        $fail_str .= '</tr>';
	    	}
    	}
    	else {
			$fail_str .= '<tr>';
		    $fail_str .= '<td class="tabletext">'.$eReportCard['Fail'].'</td>';
	        $fail_str .= '<td align="center" class="tabletext formfieldtitle">-</td>';
	        $fail_str .= '<td align="center" class="tabletext formfieldtitle">-</td>';
	        $fail_str .= '</tr>';
		}
	    
    	$div_str1 .= '<table width="100%" border="0" cellspacing="0" cellpadding="3">';
    	$div_str1 .= '<tr><td class="tabletext formfieldtitle">'.$eReportCard['SchemesFullMark'].'</td>';
        $div_str1 .= '<td class="formfieldtitle tabletext" colspan="2">'.$full_mark.'</td></tr>';
        
        $div_str1 .= '<tr><td width="25%" class="tabletext">&nbsp;</td>';
        $div_str1 .= '<td width="45%" align="center" class="tabletop tabletopnolink">';
        $div_str1 .= '<div id="div_title1_MR_tips_'.$AllSchemeData[$i]['SchemeID'].'" style="display:'.(($AllSchemeData[$i]['TopPercentage'] == 0) ? "block" : "none").'">'.$eReportCard['LowerLimit'].'</div>';
        $div_str1 .= '<div id="div_title1_PR_tips_'.$AllSchemeData[$i]['SchemeID'].'" style="display:'.(($AllSchemeData[$i]['TopPercentage'] != 0) ? "block" : "none").'">'.$eReportCard['StudentsFromTop'].'</div>';
        $div_str1 .= '</td>';
        $div_str1 .= '<td width="30%" align="center" class="tabletop tabletopnolink">'.$eReportCard['Grade'].'</td></tr>';
        
        $div_str1 .= '<tr><td class="tabletext formfieldtitle">'.$eReportCard['Distinction'].'</td>';
        $div_str1 .= '<td align="center" class="tabletext formfieldtitle">'.$distinct_mark.'</td>';
        $div_str1 .= '<td align="center" class="tabletext formfieldtitle">'.$distinct_grade.'</td>';
        $div_str1 .= '</tr>';
        
        $div_str1 .= '<tr><td width="25%" class="tabletext">&nbsp;</td>';
        $div_str1 .= '<td width="45%" align="center" class="tabletext">';
        $div_str1 .= '<div id="div_title2_MR_tips_'.$AllSchemeData[$i]['SchemeID'].'" style="display:'.(($AllSchemeData[$i]['TopPercentage'] == 0) ? "block" : "none").'">'.$eReportCard['LowerLimit'].'</div>';
        $div_str1 .= '<div id="div_title2_PR_tips_'.$AllSchemeData[$i]['SchemeID'].'" style="display:'.(($AllSchemeData[$i]['TopPercentage'] != 0) ? "block" : "none").'">'.$eReportCard['Next'].'</div>';
        $div_str1 .= '</td>';
        $div_str1 .= '<td width="30%" align="center" class="tabletext">'.$eReportCard['Grade'].'</td></tr>';
        
        $div_str1 .= $pass_str;
        $div_str1 .= $fail_str;
        $div_str1 .= '</table>';
	}
	else if($AllSchemeData[$i]['SchemeType'] == "PF"){
    	$div_str1 .= '<table width="100%" border="0" cellspacing="0" cellpadding="3">';
        $div_str1 .= '<tr><td class="tabletext" width="25%">'.$eReportCard['Pass'].'</td>';
        $div_str1 .= '<td class="style_distinction formfieldtitle" width="75%">'.(($AllSchemeData[$i]['Pass'] == "") ? '&nbsp;' : $AllSchemeData[$i]['Pass']).'</td></tr>';
        $div_str1 .= '<tr><td class="tabletext">'.$eReportCard['Fail'].'</td>';
        $div_str1 .= '<td class="style_distinction formfieldtitle">'.(($AllSchemeData[$i]['Fail'] == "") ? '&nbsp;' : $AllSchemeData[$i]['Fail']).'</td></tr>';
        $div_str1 .= '</table>';
	}
	$div_str1 .= '</td></tr>';
    $div_str1 .= '</table>';
    $div_str1 .= '</td></tr>';
    $div_str1 .= '</table>';
	$div_str1 .= '</div>';
}
// End of div of grading scheme for view

// Start of div of grading scheme details for edit
$noSchemeFlag = 0 ;

$AllSchemeArr = array();
$AllSchemeArr = $NewSchemeInfo;		
$AllSchemeArr[0]['is_new'] = 1;
for($i=1 ; $i<count($AllSchemeArr) ; $i++)
	$AllSchemeArr[$i]['is_new'] = 0;

$div_str2 = '';
for($i=0 ; $i<count($AllSchemeArr) ; $i++){
	$isNewFlag = ($AllSchemeArr[$i]['is_new']) ? 1 : 0;

	$DistinctMarkData = array();
    $PassMarkData = array();
    $FailMarkData = array();
    $DistinctMarkData = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($AllSchemeArr[$i]['SchemeID'], 'D');
    $PassMarkData = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($AllSchemeArr[$i]['SchemeID'], 'P');
    $FailMarkData = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($AllSchemeArr[$i]['SchemeID'], 'F');
	
	$div_str2 .= '
		  <div id="scheme_dtl_'.$AllSchemeArr[$i]['SchemeID'].'" style="position:absolute; width:380px; z-index:1; visibility: hidden;">
		    <table width="100%" border="0" cellpadding="0" cellspacing="0">
			  <tr>
				<td height="19"><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5" height="19"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_01.gif" width="5" height="19"></td>
					<td height="19" valign="middle" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_02.gif">&nbsp;</td>
					<td width="19" height="19"><a href="javascript:jHIDE_GRADING_SCHEME(\'\',\''.$AllSchemeArr[$i]['SchemeID'].'\', \'1\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_close_off.gif" name="pre_close1" width="19" height="19" border="0" id="pre_close1"></a></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif" width="5" height="19"></td>
					<td align="left" bgcolor="#FFFFF7"><table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
						<td><table border="0" cellspacing="0" cellpadding="2" width="100%">
						  <tr>
							<td class="navigation"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/nav_arrow.gif" width="15" height="15" align="absmiddle">'.(($isNewFlag) ? $button_new : $button_edit).' '.$eReportCard['GradingScheme'].'</td>
							<td id="loadingScheme_'.$AllSchemeArr[$i]['SchemeID'].'"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" height="18"></td>
						  </tr>
						</table></td>
					  </tr>
					  <tr>
						<td height="220" align="left" valign="top">';
						/*
						* Div : div_scheme_[SubjectID] , div_honor_based_content_[SubjectID] , div_passfail_based_content_[SubjectID]
						*		div_nature_title1_[SubjectID] , div_nature_title2_[SubjectID], div_nature_title3_[SubjectID]
						*/
						// Content of Scheme
	$div_str2 .= '		  <div id="div_scheme_dtl_'.$AllSchemeArr[$i]['SchemeID'].'" style="height:220px; width:370px; position:absolute; overflow: auto;">
							<div><table align="center" width="95%" border="0" cellpadding="5" cellspacing="0">
							  <tr>
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">'.$eReportCard['GradingTitle'].':</span></td>
								<td width="70%" valign="top"><span class="tabletext"><strong><strong><input name="grading_title_'.$AllSchemeArr[$i]['SchemeID'].'" id="grading_title_'.$AllSchemeArr[$i]['SchemeID'].'" type="text" class="textboxtext" value="'.(($isNewFlag || $AllSchemeArr[$i]['SchemeTitle'] == "") ? '' : stripslashes($AllSchemeArr[$i]['SchemeTitle'])).'"></strong></strong></span></td>
							  </tr>
							  <tr>
							    <td valign="top" nowrap="nowrap" class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;"><span class="tabletext">'.$eReportCard['GradingType'].'</span></td>
							    <td valign="top"><LABEL for="grading_honor"><span class="tabletext">
								  <input type="radio" name="grading_type_'.$AllSchemeArr[$i]['SchemeID'].'" id="grading_type_'.$AllSchemeArr[$i]['SchemeID'].'_0" value="H" onclick="jSET_GRADING_TYPE(\''.$AllSchemeArr[$i]['SchemeID'].'\')" '.(($isNewFlag || $AllSchemeArr[$i]['SchemeType'] == "H") ? "checked" : "").'>'.$eReportCard['HonorBased'].'&nbsp;&nbsp;<br>
								  <input type="radio" name="grading_type_'.$AllSchemeArr[$i]['SchemeID'].'" id="grading_type_'.$AllSchemeArr[$i]['SchemeID'].'_1" value="PF" onclick="jSET_GRADING_TYPE(\''.$AllSchemeArr[$i]['SchemeID'].'\')" '.(($AllSchemeArr[$i]['SchemeType'] == "PF") ? "checked" : "").'>'.$eReportCard['PassFailBased'].' 
								</span></LABEL></td>
							  </tr>
							</table></div>
							<div id="div_honor_based_content_'.$AllSchemeArr[$i]['SchemeID'].'" style="display:'.(($isNewFlag || $AllSchemeArr[$i]['SchemeType'] == "H") ? 'block' : 'none').'"><table align="center" width="95%" border="0" cellpadding="5" cellspacing="0">
							  <tr>
                                <td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">'.$eReportCard['GradingRange'].'</span></td>
                                <td class="tabletext">
                                  <input type="radio" name="grading_range_'.$AllSchemeArr[$i]['SchemeID'].'" id="grading_range_'.$AllSchemeArr[$i]['SchemeID'].'_0" value="0" onclick="jSET_GRADING_RANGE(\''.$AllSchemeArr[$i]['SchemeID'].'\')" '.(($AllSchemeArr[$i]['TopPercentage'] == 0) ? "checked" : "").'>'.$eReportCard['MarkRange'].'<br>
								  <input type="radio" name="grading_range_'.$AllSchemeArr[$i]['SchemeID'].'" id="grading_range_'.$AllSchemeArr[$i]['SchemeID'].'_1" value="1" onclick="jSET_GRADING_RANGE(\''.$AllSchemeArr[$i]['SchemeID'].'\')" '.(($AllSchemeArr[$i]['TopPercentage'] == 1) ? "checked" : "").'>'.$eReportCard['PercentageRangeForAllStudents'].'<br>
								  <input type="radio" name="grading_range_'.$AllSchemeArr[$i]['SchemeID'].'" id="grading_range_'.$AllSchemeArr[$i]['SchemeID'].'_2" value="2" onclick="jSET_GRADING_RANGE(\''.$AllSchemeArr[$i]['SchemeID'].'\')" '.(($AllSchemeArr[$i]['TopPercentage'] == 2) ? "checked" : "").'>'.$eReportCard['PercentageRangeForPassedStudents'].'
								</td>
                              </tr>
							  <tr>
							    <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">'.$eReportCard['SchemesFullMark'].'</span></td>
							    <td width="70%" class="tabletext"><strong><strong><input name="grading_fullmark_'.$AllSchemeArr[$i]['SchemeID'].'" id="grading_fullmark_'.$AllSchemeArr[$i]['SchemeID'].'" type="text" class="textboxnum" value="'.(($noSchemeFlag || $AllSchemeArr[$i]['FullMark'] == "") ? '' : $AllSchemeArr[$i]['FullMark']).'"></strong></strong></td>
							  </tr>
							  <tr>
							    <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">'.$eReportCard['SchemesPassingMark'].'</span></td>
							    <td width="70%" class="tabletext"><strong><strong><input name="grading_passingmark_'.$AllSchemeArr[$i]['SchemeID'].'" id="grading_passingmark_'.$AllSchemeArr[$i]['SchemeID'].'" type="text" class="textboxnum" value="'.(($noSchemeFlag || $AllSchemeArr[$i]['PassMark'] == "") ? '' : $AllSchemeArr[$i]['PassMark']).'"></strong></strong></td>
							  </tr>
							  <tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">'.$eReportCard['Distinction'].'</span></td>
								<td class="tabletext"><table width="100%" border="0" cellspacing="0" cellpadding="2">
								  <tr>
								    <td>
								      <table border="0" cellpadding="2" cellspacing="0" width="100%">
								        <tr>
										  <td width="32%" class="tabletext">
										    <div id="div_naturetitle1_MR_'.$AllSchemeArr[$i]['SchemeID'].'" style="display:'.(($AllSchemeArr[$i]['TopPercentage'] == 0) ? "block" : "none").'">'.$eReportCard['LowerLimit'].'</div>
										    <div id="div_naturetitle1_PR_'.$AllSchemeArr[$i]['SchemeID'].'" style="display:'.(($AllSchemeArr[$i]['TopPercentage'] != 0) ? "block" : "none").'">'.$eReportCard['StudentsFromTop'].'</div>
										  </td>
										  <td width="31%" valign="top" align="center" class="tabletext"> '.$eReportCard['Grade'].'</td>
										  <td width="32%" valign="top" class="tabletext">'.$eReportCard['GradePoint'].'</td>
										  <td style="width:8px">&nbsp;</td>
									  	</tr>
									  </table>
									</td>
								  </tr>
								  <tr>
								    <td>
								      <table border="0" cellpadding="2" cellspacing="0" width="100%">
								        <tr>
										  <td width="32%"><strong><strong>
										    <input type="hidden" name="RangeID_D_'.$AllSchemeArr[$i]['SchemeID'].'" id="RangeID_D_'.$AllSchemeArr[$i]['SchemeID'].'" value="'.$AllSchemeArr[$i]['RangeID_D'].'"/>
										    <div id="div_D_MR_'.$AllSchemeArr[$i]['SchemeID'].'" style="display:'.(($AllSchemeArr[$i]['TopPercentage'] == 0) ? "block" : "none").'">
										      <input name="D_MR_'.$AllSchemeArr[$i]['SchemeID'].'_a" id="D_MR_'.$AllSchemeArr[$i]['SchemeID'].'_a" type="text" class="textboxtext" value="'.$AllSchemeArr[$i]['D_MR_a'].'">
										    </div>
										    <div id="div_D_PR_'.$AllSchemeArr[$i]['SchemeID'].'" style="display:'.(($AllSchemeArr[$i]['TopPercentage'] != 0) ? "block" : "none").'"><span class="retablerow1_total tabletext">
											  <input name="D_PR_'.$AllSchemeArr[$i]['SchemeID'].'_a" id="D_PR_'.$AllSchemeArr[$i]['SchemeID'].'_a" type="text" class="ratebox" value="'.$AllSchemeArr[$i]['D_PR_a'].'" ></span> <span class="tabletext">%</span>
											</div>
										  </strong></strong></td>
										  <td width="31%"><strong><strong><input name="D_'.$AllSchemeArr[$i]['SchemeID'].'_b" id="D_'.$AllSchemeArr[$i]['SchemeID'].'_b" type="text" class="textboxtext" value="'.$AllSchemeArr[$i]['D_b'].'"></strong></strong></td>
										  <td width="32%"><strong><strong><input name="D_'.$AllSchemeArr[$i]['SchemeID'].'_c" id="D_'.$AllSchemeArr[$i]['SchemeID'].'_c" type="text" class="textboxtext" value="'.$AllSchemeArr[$i]['D_c'].'"></strong></strong></td>
										  <td style="width:8px">&nbsp;</td>
									    </tr>
									  </table>
									</td>
								  </tr>
								</table></td>
							  </tr>
							  <tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;"><span class="tabletext">'.$eReportCard['Pass'].'</span></td>
								<td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="2">
								  <tr>
								    <td>
								      <table border="0" cellpadding="2" cellspacing="0" width="100%">
								        <tr>
									      <td width="32%" class="tabletext">
										    <div id="div_naturetitle2_MR_'.$AllSchemeArr[$i]['SchemeID'].'" style="display:'.(($AllSchemeArr[$i]['TopPercentage'] == 0) ? "block" : "none").'">'.$eReportCard['LowerLimit'].'</div>
											<div id="div_naturetitle2_PR_'.$AllSchemeArr[$i]['SchemeID'].'" style="display:'.(($AllSchemeArr[$i]['TopPercentage'] != 0) ? "block" : "none").'">'.$eReportCard['Next'].'</div>
										  </td>
										  <td width="31%" align="center" class="tabletext"> '.$eReportCard['Grade'].'</td>
										  <td width="32%" class="tabletext">'.$eReportCard['GradePoint'].'</td>
										  <td style="width:8px">&nbsp;</td>
									    </tr>
									  </table>
									</td>
								  </tr>
								  <tr>
								    <td>
								      <table id="tb_P_'.$AllSchemeArr[$i]['SchemeID'].'" border="0" cellpadding="2" cellspacing="0" width="100%"><tbody>';
			/*
			* Order of Row / Box starting from "0"
			* Pass_[SubjectID]_[Order of row]_[Order of box]
			* [a: lower limit] ; [b: grade] ; [c:grade point]
			* Remark: the [Order of row] of box is strictly increasing
			*/
			if(count($AllSchemeArr[$i]['p_cnt']) > 0){									  
				for($j=0 ; $j<$AllSchemeArr[$i]['p_cnt'] ; $j++){	
					$div_str2 .= '<tr id="tr_P_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'">
									<td width="32%"><strong><strong>
									  <input type="hidden" name="RangeID_P_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'" id="RangeID_P_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'" value="'.$AllSchemeArr[$i]['RangeID_P'][$j].'"/>
									  <div id="div_P_MR_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'" style="display:'.(($AllSchemeArr[$i]['TopPercentage'] == 0) ? "block" : "none").'">
									    <input name="P_MR_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'_a" type="text" class="textboxtext" value="'.$AllSchemeArr[$i]['P_MR_a'][$j].'">
									  </div>
									  <div id="div_P_PR_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'" style="display:'.(($AllSchemeArr[$i]['TopPercentage'] != 0) ? "block" : "none").'"><span class="retablerow1_total tabletext">
									    <input name="P_PR_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'_a" type="text" class="ratebox" value="'.$AllSchemeArr[$i]['P_PR_a'][$j].'"></span><span class="tabletext">%</span>
									  </div>
									</strong></strong></td>
									<td width="31%"><strong><strong><input name="P_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'_b" id="P_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'_b" type="text" class="textboxtext" value="'.$AllSchemeArr[$i]['P_b'][$j].'"></strong></strong></td>
									<td width="32%"><strong><strong><input name="P_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'_c" id="P_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'_c" type="text" class="textboxtext" value="'.$AllSchemeArr[$i]['P_c'][$j].'"></strong></strong></td>
									<td style="width:8px"><a href="javascript:jREMOVE_GRADING_SCHEME_ROW(\'P\', \''.$AllSchemeArr[$i]['SchemeID'].'\', \''.$j.'\')" class="contenttool">X</a></td>
								  </tr>';
				}
			}
			$div_str2 .= '			  </tbody></table> 
									</td>
								  </tr>
							    </table><a href="javascript:jADD_GRADING_SCHEME_ROW(\'P\',\''.$AllSchemeArr[$i]['SchemeID'].'\');" class="contenttool"><strong>+</strong> '.$eReportCard['AddMore'].'</a><LABEL for="grading_honor"></LABEL></td>
							  </tr>
							  <tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">'.$eReportCard['Fail'].'</span></td>
								<td class="tabletext"><table width="100%" border="0" cellspacing="0" cellpadding="2">
								  <tr>
								    <td>
								      <table border="0" cellpadding="2" cellspacing="0" width="100%">
								      	<tr>
										  <td width="32%" class="tabletext">
										    <div id="div_naturetitle3_MR_'.$AllSchemeArr[$i]['SchemeID'].'" style="display:'.(($AllSchemeArr[$i]['TopPercentage'] == 0) ? "block" : "none").'">'.$eReportCard['LowerLimit'].'</div>
											<div id="div_naturetitle3_PR_'.$AllSchemeArr[$i]['SchemeID'].'" style="display:'.(($AllSchemeArr[$i]['TopPercentage'] != 0) ? "block" : "none").'">'.$eReportCard['Next'].'</div>
										  </td>
										  <td width="31%" align="center" class="tabletext"> '.$eReportCard['Grade'].'</td>
										  <td width="32%" class="tabletext">'.$eReportCard['GradePoint'].'</td>
										  <td style="width:8px">&nbsp;</td>
									    </tr>
									  </table>
									</td>
								  </tr>
								  <tr>
								    <td>
								      <table id="tb_F_'.$AllSchemeArr[$i]['SchemeID'].'" border="0" cellpadding="2" cellspacing="0" width="100%"><tbody>';
			/*
			* Order of Row / Box starting from "0"
			* Fail_[SubjectID]_[Order of row]_[Order of box]
			* [a: lower limit] ; [b: grade] ; [c:grade point]
			*/
			if(count($AllSchemeArr[$i]['f_cnt']) > 0){									  
				for($j=0 ; $j<$AllSchemeArr[$i]['f_cnt'] ; $j++){
					$div_str2 .= '<tr id="tr_F_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'">
									<td width="32%"><strong><strong>
									  <input type="hidden" name="RangeID_F_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'" id="RangeID_F_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'" value="'.$AllSchemeArr[$i]['RangeID_F'][$j].'"/>
									  <div id="div_F_MR_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'" style="display:'.(($AllSchemeArr[$i]['TopPercentage'] == 0) ? "block" : "none").'">
									    <input name="F_MR_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'_a" type="text" class="textboxtext" value="'.$AllSchemeArr[$i]['F_MR_a'][$j].'">
									  </div>
									  <div id="div_F_PR_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'" style="display:'.(($AllSchemeArr[$i]['TopPercentage'] != 0) ? "block" : "none").'"><span class="retablerow1_total tabletext">
									    <input name="F_PR_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'_a" type="text" class="ratebox" value="'.$AllSchemeArr[$i]['F_PR_a'][$j].'"></span><span class="tabletext">%</span>
									  </div>
									</strong></strong></td>
									<td width="31%"><strong><strong><input name="F_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'_b" id="F_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'_b" type="text" class="textboxtext" value="'.$AllSchemeArr[$i]['F_b'][$j].'"></strong></strong></td>
									<td width="32%"><strong><strong><input name="F_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'_c" id="F_'.$AllSchemeArr[$i]['SchemeID'].'_'.$j.'_c" type="text" class="textboxtext" value="'.$AllSchemeArr[$i]['F_c'][$j].'"></strong></strong></td>
									<td style="width:8px"><a href="javascript:jREMOVE_GRADING_SCHEME_ROW(\'F\', \''.$AllSchemeArr[$i]['SchemeID'].'\', \''.$j.'\')" class="contenttool">X</a></td>
								  </tr>';
				}
			}
			$div_str2 .= '			  </tbody></table> 
									</td>
								  </tr>
								</table><a href="javascript:jADD_GRADING_SCHEME_ROW(\'F\',\''.$AllSchemeArr[$i]['SchemeID'].'\');" class="contenttool"><strong>+</strong> '.$eReportCard['AddMore'].'</a></td>
							  </tr>
							</table></div>
							<div id="div_passfail_based_content_'.$AllSchemeArr[$i]['SchemeID'].'" style="display:'.(($AllSchemeArr[$i]['SchemeType'] == 'PF') ? 'block' : 'none').'"><table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							  <tr>
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;"><span class="tabletext">'.$eReportCard['Pass'].'</span></td>
								<td width="70%" valign="top"><span class="tabletext"><strong><strong><input name="grading_pass_'.$AllSchemeArr[$i]['SchemeID'].'" id="grading_pass_'.$AllSchemeArr[$i]['SchemeID'].'" class="textboxnum" type="text" value="'.(($isNewFlag || $AllSchemeArr[$i]['Pass'] == "") ? '' : $AllSchemeArr[$i]['Pass']).'"></strong></strong></span></td>
							  </tr>
							  <tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;"><span class="tabletext">'.$eReportCard['Fail'].'</span></td>
								<td valign="top"><span class="tabletext"><strong><strong><input name="grading_fail_'.$AllSchemeArr[$i]['SchemeID'].'" id="grading_fail_'.$AllSchemeArr[$i]['SchemeID'].'" class="textboxnum" type="text" value="'.(($isNewFlag || $AllSchemeArr[$i]['Fail'] == "") ? '' : $AllSchemeArr[$i]['Fail']).'"></strong></strong></span></td>
							  </tr>
							</table></div>
						  </div>
						</td>
					  </tr>
					  <tr>
						<td><br><table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
						  <tr>
							<td height="1" class="dotline"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>
						  </tr>
						  <tr>
							<td><table width="99%" border="0" align="center" cellpadding="2" cellspacing="0">
							  <tr>
								<td>&nbsp;</td>
								<td align="center" valign="bottom">
								  '.(($isNewFlag) ? '' : '<input name="Save" id="SaveButton_'.$AllSchemeArr[$i]['SchemeID'].'" type="button" class="formsmallbutton" value="'.$button_save.'" onclick="jEDIT_GRADING_SCHEME(\'SAVE\', \''.$AllSchemeArr[$i]['SchemeID'].'\');">').'
								  <input name="SaveAs" id="SaveAsButton_'.$AllSchemeArr[$i]['SchemeID'].'" type="button" class="formsmallbutton" value="'.$button_save_as.'" onclick="jEDIT_GRADING_SCHEME(\'SAVEAS\', \''.$AllSchemeArr[$i]['SchemeID'].'\');">
								  <input name="Cancel" id="CancelButton_'.$AllSchemeArr[$i]['SchemeID'].'" type="button" class="formsmallbutton" value="'.$button_cancel.'" onclick="jHIDE_GRADING_SCHEME(\'\',\''.$AllSchemeArr[$i]['SchemeID'].'\', \'1\')">
								  '.(($isNewFlag) ? '' : '<input name="Delete" id="DeleteButton_'.$AllSchemeArr[$i]['SchemeID'].'" type="button" class="formsmallbutton" value="'.$button_remove.'" onclick="jEDIT_GRADING_SCHEME(\'DELETE\', \''.$AllSchemeArr[$i]['SchemeID'].'\')">').'
								</td>
							  </tr>
							</table></td>
						  </tr>
						</table></td>
					  </tr>
					</table></td>
					<td width="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif" width="6" height="6"></td>
				  </tr>
				  <tr>
					<td width="5" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_07.gif" width="5" height="6"></td>
					<td height="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif" width="5" height="6"></td>
					<td width="6" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_09.gif" width="6" height="6"></td>
				  </tr>
				</table></td>
			  </tr>
			</table>
		  </div>';
}
//<!-- End of div of grading scheme details for edit -->

$rs = '';
$rs .= $js_global_var."\n";
$rs .= $sc_template_str."\n";
$rs .= $div_str1."\n";
$rs .= $div_str2."\n";

$rs .= '
           	<table id="tb_Form_Settings" width="100%" border="0" cellspacing="0" cellpadding="4">
              <tr>
                <td width="15" align="left">
                  <table border="0" cellpadding="0" cellspacing="0">
                    <tr><td><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" name="move_up[]" id="move_up" width="13" height="13"></td></tr>
                    <tr><td><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" name="move_down[]" id="move_down" width="13" height="13"></td></tr>
                  </table>
                </td>
                <td width="25%" align="right" valign="top">&nbsp;</td>
                <td width="30%" align="center" valign="top" class="retabletop tabletopnolink">'.$eReportCard['GradingScheme'].'</td>
                <td align="center" valign="middle" class="retabletop tabletopnolink">'.$eReportCard['InputScaleType'].'</td>
                <td align="center" valign="middle" class="retabletop tabletopnolink">'.$eReportCard['ResultDisplayType'].'</td>
              </tr>
              '.$rx.'
            </table>';
            
$rs .= '<input type="hidden" name="ActionCode" id="ActionCode" value=""/>';

if(count($AllSchemeArr) > 0){
	for($i=0 ; $i<count($AllSchemeArr) ; $i++){
		$rs .= '<input type="hidden" name="P_Cnt_'.$AllSchemeArr[$i]['SchemeID'].'" id="P_Cnt_'.$AllSchemeArr[$i]['SchemeID'].'" value="'.$AllSchemeArr[$i]['p_cnt'].'"/>';
		$rs .= '<input type="hidden" name="F_Cnt_'.$AllSchemeArr[$i]['SchemeID'].'" id="F_Cnt_'.$AllSchemeArr[$i]['SchemeID'].'" value="'.$AllSchemeArr[$i]['f_cnt'].'"/>';
	}
}

$AllSchemeName = array();
for($i=0 ; $i<count($AllSchemeArr) ; $i++) {
	$rs .= '<input type="hidden" name="schemeID[]" id="schemeID_'.$i.'" value="'.$AllSchemeArr[$i]['SchemeID'].'"/>';
	$AllSchemeName[] = $AllSchemeArr[$i]['SchemeTitle'];
}

// JavaScript Code to define the new AllSchemeName array
for($i=0; $i<sizeof($AllSchemeName); $i++) {
	$arrName .= "'".$AllSchemeName[$i]."'";
	if ($i != sizeof($AllSchemeName)-1) $arrName .= ",";
}
$rs .= "%Separator%";
$rs .= "AllSchemeName = new Array($arrName);";

#################################################################################################
//echo convert2unicode($rs, 1);
//echo $rs;

$AllSchemeData = array();
$AllSchemeData = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO();
$select = '<option value=""> --'.$eReportCard['SelectScheme'].'-- </option>';
for($k=0 ; $k<count($AllSchemeData) ; $k++)
	$select .= '<option value="'.$AllSchemeData[$k]['SchemeID'].'">'.$AllSchemeData[$k]['SchemeTitle'].'</option>';

//echo $select;
intranet_closedb();	
?>