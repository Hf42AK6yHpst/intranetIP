<?
/***********************************************
 * 	Modification log
 * 	20151222 Bill:	[2015-1214-1438-52073]
 * 		1.	added Grading Scheme Title to preview window
 * 	20110610 Marcus:
 * 		1.	cater grading scheme grade description
 * 	20091221 Marcus:
 * 		1.	cater more than 1 distinction
 * **********************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();


if ($plugin['ReportCard2008'])
{
	/* Temp Library*/
	//include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
	//$lreportcard = new libreportcard2008j();
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	include($PATH_WRT_ROOT."includes/eRCConfig.php");
	$hasAdditionalCriteria = $eRCTemplateSetting['AdditionalCriteria'];

	switch ($Task)
	{
		case "View":
			$SchemeData = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($schemeID);
		
		    $div_str1 .= '<table border="0" cellpadding="0" cellspacing="0" bgcolor="#feffed">';
		    $div_str1 .= '<tr><td>';
		    $div_str1 .= '<table border="0" cellspacing="0" cellpadding="5">';
		    $div_str1 .= '<tr><td>';
		    
		    if($SchemeData['SchemeType'] == "H"){
			    $DistinctMarkData = array();
			    $PassMarkData = array();
			    $FailMarkData = array();
			    
			    $DistinctMarkData = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($SchemeData['SchemeID'], 'D');
			    $PassMarkData = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($SchemeData['SchemeID'], 'P');
			    $FailMarkData = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($SchemeData['SchemeID'], 'F');
			    
			    // Full Mark
			    $full_mark = ($SchemeData['FullMark'] != "") ? $lreportcard->ReturnTextwithStyle($SchemeData['FullMark'], 'HighLight', 'Distinction') : '&nbsp;';
			    
			    // Distinction
			    /*if(count($DistinctMarkData) > 0){
				    $distinct_mark = '<div id="div_D_MR_tips_'.$SchemeData['SchemeID'].'" style="display:'.(($SchemeData['TopPercentage'] == 0) ? "block" : "none").'">';
				    $distinct_mark .= (($DistinctMarkData[0]['LowerLimit'] != "") ? $lreportcard->ReturnTextwithStyle($DistinctMarkData[0]['LowerLimit'], 'HighLight', 'Distinction') : "&nbsp;").'</div>';
				    $distinct_mark .= '<div id="div_D_PR_tips_'.$SchemeData['SchemeID'].'" style="display:'.(($SchemeData['TopPercentage'] != 0) ? "block" : "none").'">';
				    $distinct_mark .= (($DistinctMarkData[0]['UpperLimit'] != "") ? $lreportcard->ReturnTextwithStyle($DistinctMarkData[0]['UpperLimit'], 'HighLight', 'Distinction') : "&nbsp;").' %</div>';
				    $distinct_grade = ($DistinctMarkData[0]['Grade'] != "") ? $lreportcard->ReturnTextwithStyle($DistinctMarkData[0]['Grade'], 'HighLight', 'Distinction') : "&nbsp;";
				    $distinct_additional_criteria = (!empty($DistinctMarkData[0]['AdditionalCriteria'])) ?$DistinctMarkData[0]['AdditionalCriteria']."%" : "-";
				}*/
				
				 if(count($DistinctMarkData) > 0){
				    for($k=0 ; $k<count($DistinctMarkData) ; $k++){
					    $distinct_mark = '<div id="div_D_MR_tips_'.$SchemeData['SchemeID'].'_'.$k.'" style="display:'.(($SchemeData['TopPercentage'] == 0) ? "block" : "none").'">';
					    $distinct_mark .= (($DistinctMarkData[$k]['LowerLimit'] != "") ? $lreportcard->ReturnTextwithStyle($DistinctMarkData[$k]['LowerLimit'], 'HighLight', 'Distinction') : "&nbsp;").'</div>';
					    $distinct_range = '<div id="div_D_PR_tips_'.$SchemeData['SchemeID'].'_'.$k.'" style="display:'.(($SchemeData['TopPercentage'] != 0) ? "block" : "none").'">';
					    $distinct_range .= (($DistinctMarkData[$k]['UpperLimit'] != "") ? $lreportcard->ReturnTextwithStyle($DistinctMarkData[$k]['UpperLimit'], 'HighLight', 'Distinction') : "&nbsp;").' %</div>';
					    $distinct_grade = ($DistinctMarkData[$k]['Grade'] != "") ? $lreportcard->ReturnTextwithStyle($DistinctMarkData[$k]['Grade'], 'HighLight', 'Distinction') : "&nbsp;";
					    $distinct_additional_criteria = (!empty($DistinctMarkData[$k]['AdditionalCriteria'])) ? $DistinctMarkData[$k]['AdditionalCriteria']."%" : "-";
					    $distinct_grade_desc = (!empty($DistinctMarkData[$k]['Description'])) ? nl2br($DistinctMarkData[$k]['Description']) : "-";

					    $distinct_str .= '<tr>';
					    $distinct_str .= '<td class="tabletext" nowrap>'.(($k==0) ? $eReportCard['Distinction'] : '').'</td>';
				        $distinct_str .= '<td style="text-align:center" class="tabletext formfieldtitle" nowrap="nowrap">'.$distinct_mark.$distinct_range.'</td>';
				        $distinct_str .= '<td style="text-align:center" class="tabletext formfieldtitle">'.$distinct_grade.'</td>';
				        if($hasAdditionalCriteria) $distinct_str .= '<td style="text-align:center" class="tabletext formfieldtitle">'.$distinct_additional_criteria.'</td>';
				        if($eRCTemplateSetting['GradingSchemeGradeDescription']) $distinct_str .= '<td style="text-align:center" class="tabletext formfieldtitle">'.$distinct_grade_desc.'</td>';
				        $distinct_str .= '</tr>';
			    	}
				}
				else {
					$distinct_str .= '<tr>';
				    $distinct_str .= '<td class="tabletext">'.$eReportCard['Distinction'].'</td>';
			        $distinct_str .= '<td style="text-align:center" class="tabletext formfieldtitle">-</td>';
			        $distinct_str .= '<td style="text-align:center" class="tabletext formfieldtitle">-</td>';
			        $distinct_str .= '</tr>';
				}
			    
			    // Pass
			    $pass_str = '';
			    if(count($PassMarkData) > 0){
				    for($k=0 ; $k<count($PassMarkData) ; $k++){
					    $pass_mark = '<div id="div_P_MR_tips_'.$SchemeData['SchemeID'].'_'.$k.'" style="display:'.(($SchemeData['TopPercentage'] == 0) ? "block" : "none").'">';
					    $pass_mark .= (($PassMarkData[$k]['LowerLimit'] != "") ? $lreportcard->ReturnTextwithStyle($PassMarkData[$k]['LowerLimit'], 'HighLight', 'Pass') : "&nbsp;").'</div>';
					    $pass_range = '<div id="div_P_PR_tips_'.$SchemeData['SchemeID'].'_'.$k.'" style="display:'.(($SchemeData['TopPercentage'] != 0) ? "block" : "none").'">';
					    $pass_range .= (($PassMarkData[$k]['UpperLimit'] != "") ? $lreportcard->ReturnTextwithStyle($PassMarkData[$k]['UpperLimit'], 'HighLight', 'Pass') : "&nbsp;").' %</div>';
					    $pass_grade = ($PassMarkData[$k]['Grade'] != "") ? $lreportcard->ReturnTextwithStyle($PassMarkData[$k]['Grade'], 'HighLight', 'Pass') : "&nbsp;";
					    $pass_additional_criteria = (!empty($PassMarkData[$k]['AdditionalCriteria'])) ? $PassMarkData[$k]['AdditionalCriteria']."%" : "-";
					    $pass_grade_desc = (!empty($PassMarkData[$k]['Description'])) ? nl2br($PassMarkData[$k]['Description']) : "-";
					    
					    $pass_str .= '<tr>';
					    $pass_str .= '<td class="tabletext" nowrap>'.(($k==0) ? $eReportCard['Pass'] : '').'</td>';
				        $pass_str .= '<td style="text-align:center" class="tabletext formfieldtitle" nowrap="nowrap">'.$pass_mark.$pass_range.'</td>';
				        $pass_str .= '<td style="text-align:center" class="tabletext formfieldtitle">'.$pass_grade.'</td>';
				        if($hasAdditionalCriteria) $pass_str .= '<td style="text-align:center" class="tabletext formfieldtitle">'.$pass_additional_criteria.'</td>';
				        if($eRCTemplateSetting['GradingSchemeGradeDescription']) $pass_str .= '<td style="text-align:center" class="tabletext formfieldtitle">'.$pass_grade_desc.'</td>';
				        
				        $pass_str .= '</tr>';
			    	}
				}
				else {
					$pass_str .= '<tr>';
				    $pass_str .= '<td class="tabletext">'.$eReportCard['Pass'].'</td>';
			        $pass_str .= '<td style="text-align:center" class="tabletext formfieldtitle">-</td>';
			        $pass_str .= '<td style="text-align:center" class="tabletext formfieldtitle">-</td>';
			        $pass_str .= '</tr>';
				}
				
			    // Fail
			    $fail_str = '';
			    if(count($FailMarkData) > 0){
				    for($k=0 ; $k<count($FailMarkData) ; $k++){
				    	$fail_mark = '<div id="div_F_MR_tips_'.$SchemeData['SchemeID'].'_'.$k.'" style="display:'.(($SchemeData['TopPercentage'] == 0) ? "block" : "none").'">';
					    $fail_mark .= (($FailMarkData[$k]['LowerLimit'] != "") ? $lreportcard->ReturnTextwithStyle($FailMarkData[$k]['LowerLimit'], 'HighLight', 'Fail') : "&nbsp;").'</div>';
					    $fail_range = '<div id="div_F_PR_tips_'.$SchemeData['SchemeID'].'_'.$k.'" style="display:'.(($SchemeData['TopPercentage'] != 0) ? "block" : "none").'">';
					    $fail_range .= (($FailMarkData[$k]['UpperLimit'] != "") ? $lreportcard->ReturnTextwithStyle($FailMarkData[$k]['UpperLimit'], 'HighLight', 'Fail') : "&nbsp;").' %</div>';
					    $fail_grade = ($FailMarkData[$k]['Grade'] != "") ? $lreportcard->ReturnTextwithStyle($FailMarkData[$k]['Grade'], 'HighLight', 'Fail') : "&nbsp;";
					    $fail_additional_criteria = (!empty($FailMarkData[$k]['AdditionalCriteria'])) ? $FailMarkData[$k]['AdditionalCriteria']."%": "-";
					    $fail_grade_desc = (!empty($FailMarkData[$k]['Description'])) ? nl2br($FailMarkData[$k]['Description']) : "-";
					    
					    $fail_str .= '<tr>';
					    $fail_str .= '<td class="tabletext" nowrap>'.(($k==0) ? $eReportCard['Fail'] : '').'</td>';
				        $fail_str .= '<td style="text-align:center"" class="tabletext formfieldtitle" nowrap="nowrap">'.$fail_mark.$fail_range.'</td>';
				        $fail_str .= '<td style="text-align:center"" class="tabletext formfieldtitle">'.$fail_grade.'</td>';
				        if($hasAdditionalCriteria) $fail_str .= '<td style="text-align:center" class="tabletext formfieldtitle">'.$fail_additional_criteria.'</td>';
				        if($eRCTemplateSetting['GradingSchemeGradeDescription']) $fail_str .= '<td style="text-align:center" class="tabletext formfieldtitle">'.$fail_grade_desc.'</td>';
				        
				        $fail_str .= '</tr>';
			    	}
		    	}
		    	else {
					$fail_str .= '<tr>';
				    $fail_str .= '<td class="tabletext">'.$eReportCard['Fail'].'</td>';
			        $fail_str .= '<td style="text-align:center" class="tabletext formfieldtitle">-</td>';
			        $fail_str .= '<td style="text-align:center" class="tabletext formfieldtitle">-</td>';
			        $fail_str .= '</tr>';
				}
			    
		    	$div_str1 .= '<table width="100%" border="0" cellspacing="0" cellpadding="3">';
		    	// [2015-1214-1438-52073] add Grading Scheme Title in preview window
		    	$div_str1 .= '<tr><td class="tabletext formfieldtitle" nowrap>'.$eReportCard['GradingTitle'].'</td>';
		        $div_str1 .= '<td class="formfieldtitle tabletext" colspan="2">'.$SchemeData['SchemeTitle'].'</td></tr>';
		        
		    	$div_str1 .= '<tr><td class="tabletext formfieldtitle" nowrap>'.$eReportCard['SchemesFullMark'].'</td>';
		        $div_str1 .= '<td class="formfieldtitle tabletext" colspan="2">'.$full_mark.'</td></tr>';
		        
		        $div_str1 .= '<tr><td width="20%" class="tabletext">&nbsp;</td>';
		        $div_str1 .= '<td width="25%" align="center" class="tabletop tabletopnolink" nowrap>';
		        $div_str1 .= '<div id="div_title1_MR_tips_'.$SchemeData['SchemeID'].'" style="display:'.(($SchemeData['TopPercentage'] == 0) ? "block" : "none").'">'.$eReportCard['LowerLimit'].'</div>';
		        $div_str1 .= '<div id="div_title1_PR_tips_'.$SchemeData['SchemeID'].'" style="display:'.(($SchemeData['TopPercentage'] != 0) ? "block" : "none").'">'.$eReportCard['StudentsFromTop_nowrap'].'</div>';
		        $div_str1 .= '</td>';
		        $div_str1 .= '<td width="15%" align="center" class="tabletop tabletopnolink" nowrap>'.$eReportCard['Grade'].'</td>';
		        if($hasAdditionalCriteria) $div_str1 .= '<td width="40%" align="center" class="tabletop tabletopnolink" nowrap>'.$eReportCard['AdditionalCriteria']."</td></tr>";
		        if($eRCTemplateSetting['GradingSchemeGradeDescription']) $div_str1 .= '<td width="40%" align="center" class="tabletop tabletopnolink" nowrap>'.$eReportCard['GradingSchemeArr']['Description'] ."</td></tr>";
		       /* $div_str1 .= '<tr><td class="tabletext formfieldtitle" width="20%" nowrap>'.$eReportCard['Distinction'].'</td>';
		        $div_str1 .= '<td style="text-align:center" class="tabletext formfieldtitle" width="25%">'.$distinct_mark.'</td>';
		        $div_str1 .= '<td style="text-align:center" class="tabletext formfieldtitle" with="15%">'.$distinct_grade.'</td>';
		        if($hasAdditionalCriteria) $div_str1 .= '<td style="text-align:center" class="tabletext formfieldtitle" with="40%">'.$distinct_additional_criteria.'</td>';
		        $div_str1 .= '</tr>';*/
		        $div_str1 .= $distinct_str;
		        
		        $div_str1 .= '<tr><td width="20%" class="tabletext">&nbsp;</td>';
		        $div_str1 .= '<td width="25%" align="center" class="tabletext" nowrap>';
		        $div_str1 .= '<div id="div_title2_MR_tips_'.$SchemeData['SchemeID'].'" style="display:'.(($SchemeData['TopPercentage'] == 0) ? "block" : "none").'">'.$eReportCard['LowerLimit'].'</div>';
		        $div_str1 .= '<div id="div_title2_PR_tips_'.$SchemeData['SchemeID'].'" style="display:'.(($SchemeData['TopPercentage'] != 0) ? "block" : "none").'">'.$eReportCard['Next'].'</div>';
		        $div_str1 .= '</td>';
		        $div_str1 .= '<td width="15%" align="center" class="tabletext">'.$eReportCard['Grade'].'</td>';
		        if($hasAdditionalCriteria) $div_str1 .= '<td width="40%" align="center" class="tabletext" nowrap>'.$eReportCard['AdditionalCriteria'].'</td></tr>';
		        if($eRCTemplateSetting['GradingSchemeGradeDescription']) $div_str1 .= '<td width="40%" align="center" class="tabletext" nowrap>'.$eReportCard['GradingSchemeArr']['Description'] .'</td></tr>';
		        
		        
		        $div_str1 .= $pass_str;
		        $div_str1 .= $fail_str;
		        $div_str1 .= '</table>';
			    
			}
			else if($SchemeData['SchemeType'] == "PF"){
		    	$div_str1 .= '<table width="100%" border="0" cellspacing="0" cellpadding="3">';
		    	// [2015-1214-1438-52073] add Grading Scheme Title in preview window
		    	$div_str1 .= '<tr><td class="tabletext" width="25%">'.$eReportCard['GradingTitle'].'</td>';
		        $div_str1 .= '<td class="formfieldtitle" width="75%">'.$SchemeData['SchemeTitle'].'</td></tr>';
		        
		        $div_str1 .= '<tr><td class="tabletext">'.$eReportCard['Pass'].'</td>';
		        $div_str1 .= '<td class="style_distinction formfieldtitle">'.(($SchemeData['Pass'] == "") ? '&nbsp;' : $SchemeData['Pass']).'</td></tr>';
		        $div_str1 .= '<tr><td class="tabletext">'.$eReportCard['Fail'].'</td>';
		        $div_str1 .= '<td class="style_distinction formfieldtitle">'.(($SchemeData['Fail'] == "") ? '&nbsp;' : $SchemeData['Fail']).'</td></tr>';
		        $div_str1 .= '</table>';
			}
			$div_str1 .= '</td></tr>';
		    $div_str1 .= '</table>';
		    $div_str1 .= '</td></tr>';
		    $div_str1 .= '</table>';
		
			echo $div_str1;
		break; //end case "View"
		
		case "Edit":
		
		$MaxPercentageLength = 6;
		
		if(empty($schemeID))
		{
			$isNewFlag=true;
			$SchemeData['SchemeID'] = "new";
			$SchemeData['is_new'] = 1;
			$hidePassingMark = ' style="display:none;" ';
		}
		else
		{
			$SchemeData = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($schemeID);
			if($SchemeData['TopPercentage'] == 0)
				$hidePassingMark = ' style="display:none;" ';
			else
				$hidePassingMark='';
		}	
		
		
		$DistinctMarkData = array();
    	$PassMarkData = array();
    	$FailMarkData = array();
    	$DistinctMarkData = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($SchemeData['SchemeID'], 'D');
    	$PassMarkData = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($SchemeData['SchemeID'], 'P');
    	$FailMarkData = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($SchemeData['SchemeID'], 'F');
		
		$div_str2 .= '
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			  <tr>
				<td height="19"><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5" height="19"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_01.gif" width="5" height="19"></td>
					<td height="19" valign="middle" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_02.gif">&nbsp;</td>
					<td width="19" height="19"><a href="javascript:jHIDE_GRADING_SCHEME1(\'\',\''.$SchemeData['SchemeID'].'\', \'1\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_close_off.gif" name="pre_close1" width="19" height="19" border="0" id="pre_close1"></a></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif" width="5" height="19"></td>
					<td align="left" bgcolor="#FFFFF7"><table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
						<td><table border="0" cellspacing="0" cellpadding="2" width="100%">
						  <tr>
							<td class="navigation"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/nav_arrow.gif" width="15" height="15" align="absmiddle">'.(($isNewFlag) ? $button_new : $button_edit).' '.$eReportCard['GradingScheme'].'</td>
							<td id="loadingScheme_'.$SchemeData['SchemeID'].'"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" height="18"></td>
						  </tr>
						</table></td>
					  </tr>
					  <tr>
						<td height="220" align="left" valign="top">';
						/*
						* Div : div_scheme_[SubjectID] , div_honor_based_content_[SubjectID] , div_passfail_based_content_[SubjectID]
						*		div_nature_title1_[SubjectID] , div_nature_title2_[SubjectID], div_nature_title3_[SubjectID]
						*/
						// Content of Scheme
	$div_str2 .= '		  <div id="div_scheme_dtl_'.$SchemeData['SchemeID'].'" style="height:220px; width:98%; position:absolute; overflow: auto;">
							<div><table align="center" width="95%" border="0" cellpadding="2" cellspacing="0">
							  <tr>
								<td width="25%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">'.$eReportCard['GradingTitle'].':</span></td>
								<td width="75%" valign="top"><span class="tabletext"><strong><strong><input name="grading_title_'.$SchemeData['SchemeID'].'" id="grading_title_'.$SchemeData['SchemeID'].'" type="text" class="textboxtext" value="'.(($isNewFlag || $SchemeData['SchemeTitle'] == "") ? '' : $SchemeData['SchemeTitle']).'"></strong></strong></span></td>
							  </tr>
							  <tr>
							    <td valign="top" nowrap="nowrap" class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;"><span class="tabletext">'.$eReportCard['GradingType'].'</span></td>
							    <td valign="top"><span class="tabletext">
								  <input type="radio" name="grading_type_'.$SchemeData['SchemeID'].'" id="grading_type_'.$SchemeData['SchemeID'].'_0" value="H" onclick="jSET_GRADING_TYPE(\''.$SchemeData['SchemeID'].'\')" '.(($isNewFlag || $SchemeData['SchemeType'] == "H") ? "checked" : "").'><label for="grading_type_'.$SchemeData['SchemeID'].'_0">'.$eReportCard['HonorBased'].'</label>&nbsp;&nbsp;<br>
								  <input type="radio" name="grading_type_'.$SchemeData['SchemeID'].'" id="grading_type_'.$SchemeData['SchemeID'].'_1" value="PF" onclick="jSET_GRADING_TYPE(\''.$SchemeData['SchemeID'].'\')" '.(($SchemeData['SchemeType'] == "PF") ? "checked" : "").'><label for="grading_type_'.$SchemeData['SchemeID'].'_1">'.$eReportCard['PassFailBased'].'</label> 
								</span></td>
							  </tr>
							</table></div>
							<div id="div_honor_based_content_'.$SchemeData['SchemeID'].'" style="display:'.(($isNewFlag || $SchemeData['SchemeType'] == "H") ? 'block' : 'none').'"><table align="center" width="95%" border="0" cellpadding="5" cellspacing="0">
							  <tr>
                                <td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">'.$eReportCard['GradingRange'].'</span></td>
                                <td class="tabletext">
                                  <input type="radio" name="grading_range_'.$SchemeData['SchemeID'].'" id="grading_range_'.$SchemeData['SchemeID'].'_0" value="0" onclick="jSET_GRADING_RANGE(\''.$SchemeData['SchemeID'].'\')" '.(($SchemeData['TopPercentage'] == 0) ? "checked" : "").'><label for="grading_range_'.$SchemeData['SchemeID'].'_0">'.$eReportCard['MarkRange'].'</label><br>
								  <input type="radio" name="grading_range_'.$SchemeData['SchemeID'].'" id="grading_range_'.$SchemeData['SchemeID'].'_1" value="1" onclick="jSET_GRADING_RANGE(\''.$SchemeData['SchemeID'].'\')" '.(($SchemeData['TopPercentage'] == 1) ? "checked" : "").'><label for="grading_range_'.$SchemeData['SchemeID'].'_1">'.$eReportCard['PercentageRangeForAllStudents'].'</label><br>
								  <input type="radio" name="grading_range_'.$SchemeData['SchemeID'].'" id="grading_range_'.$SchemeData['SchemeID'].'_2" value="2" onclick="jSET_GRADING_RANGE(\''.$SchemeData['SchemeID'].'\')" '.(($SchemeData['TopPercentage'] == 2) ? "checked" : "").'><label for="grading_range_'.$SchemeData['SchemeID'].'_2">'.$eReportCard['PercentageRangeForPassedStudents'].'</label>
								</td>
                              </tr>
							  <tr>
							    <td width="25%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">'.$eReportCard['SchemesFullMark'].'</span></td>
							    <td width="75%" class="tabletext"><strong><strong><input name="grading_fullmark_'.$SchemeData['SchemeID'].'" id="grading_fullmark_'.$SchemeData['SchemeID'].'" type="text" class="textboxnum" value="'.(($noSchemeFlag || $SchemeData['FullMark'] == "") ? '' : $SchemeData['FullMark']).'"></strong></strong></td>
							  </tr>
							  <tr id="passing_mark_row" '.$hidePassingMark.'>
							    <td width="25%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">'.$eReportCard['SchemesPassingMark'].'</span></td>
							    <td width="75%" class="tabletext"><strong><strong><input name="grading_passingmark_'.$SchemeData['SchemeID'].'" id="grading_passingmark_'.$SchemeData['SchemeID'].'" type="text" class="textboxnum" value="'.(($noSchemeFlag || $SchemeData['PassMark'] == "") ? '' : $SchemeData['PassMark']).'"></strong></strong></td>
							  </tr>
							  <tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">'.$eReportCard['Distinction'].'</span></td>
								<td class="tabletext"><table width="100%" border="0" cellspacing="0" cellpadding="2">
								  <tr>
								    <td>
								      <table border="0" cellpadding="2" cellspacing="0" width="100%">
								        <tr>
										  <td class="tabletext" style="width:50px;">
										    <div id="div_naturetitle1_MR_'.$SchemeData['SchemeID'].'" style="display:'.(($SchemeData['TopPercentage'] == 0) ? "block" : "none").'">'.$eReportCard['LowerLimit'].'</div>
										    <div id="div_naturetitle1_PR_'.$SchemeData['SchemeID'].'" style="display:'.(($SchemeData['TopPercentage'] != 0) ? "block" : "none").'">'.$eReportCard['StudentsFromTop'].'</div>
										  </td>
										  <td valign="top" class="tabletext" style="width:50px;"> '.$eReportCard['Grade'].'</td>
										  <td valign="top" class="tabletext" style="width:50px;">'.$eReportCard['GradePoint'].'</td>
										  '.($hasAdditionalCriteria?'<td style="width:60px;">'.$eReportCard['AdditionalCriteria'].'</td>':'').'
											'.($eRCTemplateSetting['GradingSchemeGradeDescription']?'<td style="width:120px;">'.$eReportCard['GradingSchemeArr']['Description'] .'</td>':'').'
										  <td style="width:8px">&nbsp;</td>
									  	</tr>
									  </table>
									</td>
								  </tr>
								  <tr>
								    <td>
								      <table  id="tb_D_'.$SchemeData['SchemeID'].'" border="0" cellpadding="2" cellspacing="0" width="100%">
								      	<tbody>';
						/*  $div_str2 .= ' <tr>
										  <td width="25%"><strong><strong>
										    <input type="hidden" name="RangeID_D_'.$SchemeData['SchemeID'].'" id="RangeID_D_'.$SchemeData['SchemeID'].'" value="'.((count($DistinctMarkData) > 0) ? $DistinctMarkData[0]['GradingSchemeRangeID'] : '').'"/>
										    <div id="div_D_MR_'.$SchemeData['SchemeID'].'" style="display:'.(($SchemeData['TopPercentage'] == 0) ? "block" : "none").'">
										      <input name="D_MR_'.$SchemeData['SchemeID'].'_a" id="D_MR_'.$SchemeData['SchemeID'].'_a" type="text" class="textboxtext" value="'.((count($DistinctMarkData) > 0) ? $DistinctMarkData[0]['LowerLimit'] : '').'">
										    </div>
										    <div id="div_D_PR_'.$SchemeData['SchemeID'].'" style="display:'.(($SchemeData['TopPercentage'] != 0) ? "block" : "none").'"><span class="retablerow1_total tabletext">
											  <input name="D_PR_'.$SchemeData['SchemeID'].'_a" id="D_PR_'.$SchemeData['SchemeID'].'_a" type="text" class="ratebox" value="'.((count($DistinctMarkData) > 0) ? $DistinctMarkData[0]['UpperLimit'] : '').'" ></span> <span class="tabletext">%</span>
											</div>
										  </strong></strong></td>
										  <td width="25%"><strong><strong><input name="D_'.$SchemeData['SchemeID'].'_b" id="D_'.$SchemeData['SchemeID'].'_b" type="text" class="textboxtext" value="'.((count($DistinctMarkData) > 0) ? $DistinctMarkData[0]['Grade'] : '').'"></strong></strong></td>
										  <td width="25%"><strong><strong><input name="D_'.$SchemeData['SchemeID'].'_c" id="D_'.$SchemeData['SchemeID'].'_c" type="text" class="textboxtext" value="'.((count($DistinctMarkData) > 0) ? $DistinctMarkData[0]['GradePoint'] : '').'"></strong></strong></td>
										  '.($hasAdditionalCriteria?'<td width="25%"><strong><strong><span class="retablerow1_total tabletext"><input name="D_'.$SchemeData['SchemeID'].'_d" id="D_'.$SchemeData['SchemeID'].'_d" type="text" class="ratebox" value="'.(empty($DistinctMarkData[0]['AdditionalCriteria'])?0:$DistinctMarkData[0]['AdditionalCriteria']).'"></span><span class="tabletext">%</span></strong></strong></td>':'').'
										  <td style="width:8px">&nbsp;</td>
									    </tr>';*/
						if(count($DistinctMarkData) > 0){									  
							for($j=0 ; $j<count($DistinctMarkData) ; $j++){	
								//debug_pr($DistinctMarkData[$j]['UpperLimit']);
								$div_str2 .= '<tr id="tr_D_'.$SchemeData['SchemeID'].'_'.$j.'">
												<td style="width:50px;"><strong><strong>
												  <input type="hidden" name="RangeID_D_'.$SchemeData['SchemeID'].'_'.$j.'" id="RangeID_D_'.$SchemeData['SchemeID'].'_'.$j.'" value="'.$DistinctMarkData[$j]['GradingSchemeRangeID'].'"/>
												  <div id="div_D_MR_'.$SchemeData['SchemeID'].'_'.$j.'" style="display:'.(($SchemeData['TopPercentage'] == 0) ? "block" : "none").'">
												    <input style="width:50px;" name="D_MR_'.$SchemeData['SchemeID'].'_'.$j.'_a" type="text" class="textboxtext" value="'.$DistinctMarkData[$j]['LowerLimit'].'" maxlength="'.$MaxPercentageLength.'">
												  </div>
												  <div id="div_D_PR_'.$SchemeData['SchemeID'].'_'.$j.'" style="display:'.(($SchemeData['TopPercentage'] != 0) ? "block" : "none").'"><span class="retablerow1_total tabletext">
												    <input style="width:50px;" name="D_PR_'.$SchemeData['SchemeID'].'_'.$j.'_a" type="text" class="ratebox" value="'.$DistinctMarkData[$j]['UpperLimit'].'"  maxlength="'.$MaxPercentageLength.'"></span><span class="tabletext">%</span>
												  </div>
												</strong></strong></td>
												<td style="width:50px;"><strong><strong><input style="width:50px;" name="D_'.$SchemeData['SchemeID'].'_'.$j.'_b" id="D_'.$SchemeData['SchemeID'].'_'.$j.'_b" type="text" class="textboxtext" value="'.$DistinctMarkData[$j]['Grade'].'"></strong></strong></td>
												<td style="width:50px;"><strong><strong><input style="width:50px;" name="D_'.$SchemeData['SchemeID'].'_'.$j.'_c" id="D_'.$SchemeData['SchemeID'].'_'.$j.'_c" type="text" class="textboxtext" value="'.$DistinctMarkData[$j]['GradePoint'].'"></strong></strong></td>
												'.($hasAdditionalCriteria?'<td style="width:60px;"><strong><strong><span class="retablerow1_total tabletext"><input style="width:60px;" name="D_'.$SchemeData['SchemeID'].'_'.$j.'_d" id="D_'.$SchemeData['SchemeID'].'_'.$j.'_d" type="text" class="ratebox" value="'.(empty($DistinctMarkData[$j]['AdditionalCriteria'])?0:$DistinctMarkData[$j]['AdditionalCriteria']).'"></span><span class="tabletext">%</span></strong></strong></td>':'').'
												'.($eRCTemplateSetting['GradingSchemeGradeDescription']?'<td style="width:120px;"><strong><strong><span class="retablerow1_total tabletext"><textarea style="width:120px;" name="D_'.$SchemeData['SchemeID'].'_'.$j.'_e" id="D_'.$SchemeData['SchemeID'].'_'.$j.'_e" class="textboxtext" maxlength="'.$eRCTemplateSetting['GradingSchemeGradeDiscriptionLength'].'">'.(empty($DistinctMarkData[$j]['Description'])?'':$DistinctMarkData[$j]['Description']).'</textarea></span></strong></strong></td>':'').'
												<td style="width:8px"><a href="javascript:jREMOVE_GRADING_SCHEME_ROW1(\'D\', \''.$SchemeData['SchemeID'].'\', \''.$j.'\')" class="contenttool">X</a></td>
											  </tr>';
							}
						}			    
						$div_str2 .= ' 		</tbody>
										</table>
									</td>
								  </tr>
								</table><a href="javascript:jADD_GRADING_SCHEME_ROW1(\'D\',\''.$SchemeData['SchemeID'].'\');" class="contenttool"><strong>+</strong> '.$eReportCard['AddMore'].'</a><LABEL for="grading_honor"></LABEL></td>
							  </tr>
							  <tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;"><span class="tabletext">'.$eReportCard['Pass'].'</span></td>
								<td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="2">
								  <tr>
								    <td>
								      <table border="0" cellpadding="2" cellspacing="0" width="100%">
								        <tr>
									      <td style="width:50px;" class="tabletext">
										    <div id="div_naturetitle2_MR_'.$SchemeData['SchemeID'].'" style="display:'.(($SchemeData['TopPercentage'] == 0) ? "block" : "none").'">'.$eReportCard['LowerLimit'].'</div>
											<div id="div_naturetitle2_PR_'.$SchemeData['SchemeID'].'" style="display:'.(($SchemeData['TopPercentage'] != 0) ? "block" : "none").'">'.$eReportCard['Next'].'</div>
										  </td>
										  <td style="width:50px;" class="tabletext"> '.$eReportCard['Grade'].'</td>
										  <td style="width:50px;" class="tabletext">'.$eReportCard['GradePoint'].'</td>
										  '.($hasAdditionalCriteria?'<td style="width:60px;">'.$eReportCard['AdditionalCriteria'].'</td>':'').'
											'.($eRCTemplateSetting['GradingSchemeGradeDescription']?'<td style="width:120px;">'.$eReportCard['GradingSchemeArr']['Description'] .'</td>':'').'
										  <td style="width:8px">&nbsp;</td>
									    </tr>
									  </table>
									</td>
								  </tr>
								  <tr>
								    <td>
								      <table id="tb_P_'.$SchemeData['SchemeID'].'" border="0" cellpadding="2" cellspacing="0" width="100%"><tbody>';
			/*
			* Order of Row / Box starting from "0"
			* Pass_[SubjectID]_[Order of row]_[Order of box]
			* [a: lower limit] ; [b: grade] ; [c:grade point]
			* Remark: the [Order of row] of box is strictly increasing
			*/
			if(count($PassMarkData) > 0){									  
				for($j=0 ; $j<count($PassMarkData) ; $j++){	
					$div_str2 .= '<tr id="tr_P_'.$SchemeData['SchemeID'].'_'.$j.'">
									<td style="width:50px;"><strong><strong>
									  <input type="hidden" name="RangeID_P_'.$SchemeData['SchemeID'].'_'.$j.'" id="RangeID_P_'.$SchemeData['SchemeID'].'_'.$j.'" value="'.$PassMarkData[$j]['GradingSchemeRangeID'].'"/>
									  <div id="div_P_MR_'.$SchemeData['SchemeID'].'_'.$j.'" style="display:'.(($SchemeData['TopPercentage'] == 0) ? "block" : "none").'">
									    <input style="width:50px;" name="P_MR_'.$SchemeData['SchemeID'].'_'.$j.'_a" type="text" class="textboxtext" value="'.$PassMarkData[$j]['LowerLimit'].'"  maxlength="'.$MaxPercentageLength.'">
									  </div>
									  <div id="div_P_PR_'.$SchemeData['SchemeID'].'_'.$j.'" style="display:'.(($SchemeData['TopPercentage'] != 0) ? "block" : "none").'"><span class="retablerow1_total tabletext">
									    <input style="width:50px;" name="P_PR_'.$SchemeData['SchemeID'].'_'.$j.'_a" type="text" class="ratebox" value="'.$PassMarkData[$j]['UpperLimit'].'"  maxlength="'.$MaxPercentageLength.'"></span><span class="tabletext">%</span>
									  </div>
									</strong></strong></td>
									<td style="width:50px;"><strong><strong><input style="width:50px;" name="P_'.$SchemeData['SchemeID'].'_'.$j.'_b" id="P_'.$SchemeData['SchemeID'].'_'.$j.'_b" type="text" class="textboxtext" value="'.$PassMarkData[$j]['Grade'].'"></strong></strong></td>
									<td style="width:50px;"><strong><strong><input style="width:50px;" name="P_'.$SchemeData['SchemeID'].'_'.$j.'_c" id="P_'.$SchemeData['SchemeID'].'_'.$j.'_c" type="text" class="textboxtext" value="'.$PassMarkData[$j]['GradePoint'].'"></strong></strong></td>
									'.($hasAdditionalCriteria?'<td style="width:60px;"><strong><strong><span class="retablerow1_total tabletext"><input style="width:60px;" name="P_'.$SchemeData['SchemeID'].'_'.$j.'_d" id="P_'.$SchemeData['SchemeID'].'_'.$j.'_d" type="text" class="ratebox" value="'.(empty($PassMarkData[$j]['AdditionalCriteria'])?0:$PassMarkData[$j]['AdditionalCriteria']).'"></span><span class="tabletext">%</span></strong></strong></td>':'').'
									'.($eRCTemplateSetting['GradingSchemeGradeDescription']?'<td style="width:120px;"><strong><strong><span class="retablerow1_total tabletext"><textarea style="width:120px;" name="P_'.$SchemeData['SchemeID'].'_'.$j.'_e" id="P_'.$SchemeData['SchemeID'].'_'.$j.'_e" class="textboxtext"  maxlength="'.$eRCTemplateSetting['GradingSchemeGradeDiscriptionLength'].'">'.(empty($PassMarkData[$j]['Description'])?'':$PassMarkData[$j]['Description']).'</textarea></span></strong></strong></td>':'').'
									<td style="width:8px"><a href="javascript:jREMOVE_GRADING_SCHEME_ROW1(\'P\', \''.$SchemeData['SchemeID'].'\', \''.$j.'\')" class="contenttool">X</a></td>
								  </tr>';
				}
			}
			$div_str2 .= '			  </tbody></table> 
									</td>
								  </tr>
							    </table><a href="javascript:jADD_GRADING_SCHEME_ROW1(\'P\',\''.$SchemeData['SchemeID'].'\');" class="contenttool"><strong>+</strong> '.$eReportCard['AddMore'].'</a><LABEL for="grading_honor"></LABEL></td>
							  </tr>
							  <tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">'.$eReportCard['Fail'].'</span></td>
								<td class="tabletext"><table width="100%" border="0" cellspacing="0" cellpadding="2">
								  <tr>
								    <td>
								      <table border="0" cellpadding="2" cellspacing="0" width="100%">
								      	<tr>
										  <td style="width:50px;" class="tabletext">
										    <div id="div_naturetitle3_MR_'.$SchemeData['SchemeID'].'" style="display:'.(($SchemeData['TopPercentage'] == 0) ? "block" : "none").'">'.$eReportCard['LowerLimit'].'</div>
											<div id="div_naturetitle3_PR_'.$SchemeData['SchemeID'].'" style="display:'.(($SchemeData['TopPercentage'] != 0) ? "block" : "none").'">'.$eReportCard['Next'].'</div>
										  </td>
										  <td style="width:50px;" class="tabletext"> '.$eReportCard['Grade'].'</td>
										  <td style="width:50px;" class="tabletext">'.$eReportCard['GradePoint'].'</td>
										  '.($hasAdditionalCriteria?'<td style="width:60px;">'.$eReportCard['AdditionalCriteria'].'</td>':'').'
										'.($eRCTemplateSetting['GradingSchemeGradeDescription']?'<td style="width:120px;">'.$eReportCard['GradingSchemeArr']['Description'] .'</td>':'').'
										  <td style="width:8px">&nbsp;</td>
									    </tr>
									  </table>
									</td>
								  </tr>
								  <tr>
								    <td>
								      <table id="tb_F_'.$SchemeData['SchemeID'].'" border="0" cellpadding="2" cellspacing="0" width="100%"><tbody>';
			/*
			* Order of Row / Box starting from "0"
			* Fail_[SubjectID]_[Order of row]_[Order of box]
			* [a: lower limit] ; [b: grade] ; [c:grade point]
			*/
			if(count($FailMarkData) > 0){									  
				for($j=0 ; $j<count($FailMarkData) ; $j++){
					$div_str2 .= '<tr id="tr_F_'.$SchemeData['SchemeID'].'_'.$j.'">
									<td style="width:50px;"><strong><strong>
									  <input type="hidden" name="RangeID_F_'.$SchemeData['SchemeID'].'_'.$j.'" id="RangeID_F_'.$SchemeData['SchemeID'].'_'.$j.'" value="'.$FailMarkData[$j]['GradingSchemeRangeID'].'"/>
									  <div id="div_F_MR_'.$SchemeData['SchemeID'].'_'.$j.'" style="display:'.(($SchemeData['TopPercentage'] == 0) ? "block" : "none").'">
									    <input style="width:50px;" name="F_MR_'.$SchemeData['SchemeID'].'_'.$j.'_a" type="text" class="textboxtext" value="'.$FailMarkData[$j]['LowerLimit'].'" maxlength="'.$MaxPercentageLength.'">
									  </div>
									  <div id="div_F_PR_'.$SchemeData['SchemeID'].'_'.$j.'" style="display:'.(($SchemeData['TopPercentage'] != 0) ? "block" : "none").'"><span class="retablerow1_total tabletext">
									    <input style="width:50px;" name="F_PR_'.$SchemeData['SchemeID'].'_'.$j.'_a" type="text" class="ratebox" value="'.$FailMarkData[$j]['UpperLimit'].'" maxlength="'.$MaxPercentageLength.'"></span><span class="tabletext">%</span>
									  </div>
									</strong></strong></td>
									<td style="width:50px;"><strong><strong><input style="width:50px;" name="F_'.$SchemeData['SchemeID'].'_'.$j.'_b" id="F_'.$SchemeData['SchemeID'].'_'.$j.'_b" type="text" class="textboxtext" value="'.$FailMarkData[$j]['Grade'].'"></strong></strong></td>
									<td style="width:50px;"><strong><strong><input style="width:50px;" name="F_'.$SchemeData['SchemeID'].'_'.$j.'_c" id="F_'.$SchemeData['SchemeID'].'_'.$j.'_c" type="text" class="textboxtext" value="'.$FailMarkData[$j]['GradePoint'].'"></strong></strong></td>
									'.($hasAdditionalCriteria?'<td style="width:60px;"><strong><strong><span class="retablerow1_total tabletext"><input style="width:60px;" name="F_'.$SchemeData['SchemeID'].'_'.$j.'_d" id="F_'.$SchemeData['SchemeID'].'_'.$j.'_d" type="text" class="ratebox" value="'.(empty($FailMarkData[$j]['AdditionalCriteria'])?0:$FailMarkData[$j]['AdditionalCriteria']).'"></span><span class="tabletext">%</span></strong></strong></td>':'').'
									'.($eRCTemplateSetting['GradingSchemeGradeDescription']?'<td style="width:120px;"><strong><strong><span class="retablerow1_total tabletext"><textarea style="width:120px;" name="F_'.$SchemeData['SchemeID'].'_'.$j.'_e" id="F_'.$SchemeData['SchemeID'].'_'.$j.'_e" class="textboxtext"  maxlength="'.$eRCTemplateSetting['GradingSchemeGradeDiscriptionLength'].'">'.(empty($FailMarkData[$j]['Description'])?'':$FailMarkData[$j]['Description']).'</textarea></span></strong></strong></td>':'').'
									<td style="width:8px"><a href="javascript:jREMOVE_GRADING_SCHEME_ROW1(\'F\', \''.$SchemeData['SchemeID'].'\', \''.$j.'\')" class="contenttool">X</a></td>
								  </tr>';
				}
			}
			$div_str2 .= '			  </tbody></table> 
									</td>
								  </tr>
								</table><a href="javascript:jADD_GRADING_SCHEME_ROW1(\'F\',\''.$SchemeData['SchemeID'].'\');" class="contenttool"><strong>+</strong> '.$eReportCard['AddMore'].'</a></td>
							  </tr>
							</table></div>
							<div id="div_passfail_based_content_'.$SchemeData['SchemeID'].'" style="display:'.(($SchemeData['SchemeType'] == 'PF') ? 'block' : 'none').'"><table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							  <tr>
								<td width="25%" valign="top" nowrap="nowrap" class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;"><span class="tabletext">'.$eReportCard['Pass'].'</span></td>
								<td width="75%" valign="top"><span class="tabletext"><strong><strong><input name="grading_pass_'.$SchemeData['SchemeID'].'" id="grading_pass_'.$SchemeData['SchemeID'].'" class="textboxnum" type="text" value="'.(($isNewFlag || $SchemeData['Pass'] == "") ? '' : $SchemeData['Pass']).'"></strong></strong></span></td>
							  </tr>
							  <tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle" style="border-bottom:1px #EEEEEE solid;"><span class="tabletext">'.$eReportCard['Fail'].'</span></td>
								<td valign="top"><span class="tabletext"><strong><strong><input name="grading_fail_'.$SchemeData['SchemeID'].'" id="grading_fail_'.$SchemeData['SchemeID'].'" class="textboxnum" type="text" value="'.(($isNewFlag || $SchemeData['Fail'] == "") ? '' : $SchemeData['Fail']).'"></strong></strong></span></td>
							  </tr>
							</table></div>
						  </div>
						</td>
					  </tr>
					  <tr>
						<td><br><table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
						  <tr>
							<td height="1" class="dotline"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>
						  </tr>
						  <tr>
							<td><table width="99%" border="0" align="center" cellpadding="2" cellspacing="0">
							  <tr>
								<td>&nbsp;</td>
								<td align="center" valign="bottom">
								  '.(($isNewFlag) ? '' : '<input name="Save" id="SaveButton_'.$SchemeData['SchemeID'].'" type="button" class="formsmallbutton" value="'.$button_save.'" onclick="jEDIT_GRADING_SCHEME1(\'SAVE\', \''.$SchemeData['SchemeID'].'\');">').'
								  <input name="SaveAs" id="SaveAsButton_'.$SchemeData['SchemeID'].'" type="button" class="formsmallbutton" value="'.$button_save_as.'" onclick="jEDIT_GRADING_SCHEME1(\'SAVEAS\', \''.$SchemeData['SchemeID'].'\');">
								  <input name="Cancel" id="CancelButton_'.$SchemeData['SchemeID'].'" type="button" class="formsmallbutton" value="'.$button_cancel.'" onclick="jHIDE_GRADING_SCHEME1(\'\',\''.$SchemeData['SchemeID'].'\', \'1\')">
								  '.(($isNewFlag) ? '' : '<input name="Delete" id="DeleteButton_'.$SchemeData['SchemeID'].'" type="button" class="formsmallbutton" value="'.$button_remove.'" onclick="jEDIT_GRADING_SCHEME1(\'DELETE\', \''.$SchemeData['SchemeID'].'\')">').'
								</td>
							  </tr>
							</table></td>
						  </tr>
						</table></td>
					  </tr>
					</table></td>
					<td width="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif" width="6" height="6"></td>
				  </tr>
				  <tr>
					<td width="5" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_07.gif" width="5" height="6"></td>
					<td height="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif" width="5" height="6"></td>
					<td width="6" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_09.gif" width="6" height="6"></td>
				  </tr>
				</table></td>
			  </tr>
			</table>';
		
		echo $div_str2;
		break;
		
		
	} // end switch ($Task)
}

?>
