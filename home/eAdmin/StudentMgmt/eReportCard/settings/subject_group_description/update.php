<?php
// Using:

/********************** Change Log ***********************/
#
#	Date:	2020-10-30 (Bill)	[2020-0708-0950-12308]
#			create file
#
/******************* End Of Change Log *******************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
    include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
    $lreportcard = new libreportcardcustom();
} else {
    $lreportcard = new libreportcard();
}

### Handle SQL Injection + XSS [START]
$ClassLevelID = IntegerSafe($ClassLevelID);
$ReportID = IntegerSafe($ReportID);
### Handle SQL Injection + XSS [END]

$result = array();
if(!empty($Description))
{
    foreach((array)$Description as $SubjectGroupID => $SGDescription)
    {
        $SGDescription = standardizeFormPostValue($SGDescription);
        $currentSGDesctipion = $lreportcard->getSubjectGroupDescription($ReportID, $SubjectGroupID);
        if(empty($currentSGDesctipion)) {
            $result[] = $lreportcard->insertSubjectGroupDescription($ReportID, $SubjectGroupID, $SGDescription);
        } else {
            $result[] = $lreportcard->updateSubjectGroupDescription($ReportID, $SubjectGroupID, $SGDescription);
        }
    }
}
$msg = !in_array(false, $result) ? "update" : "update_failed";

intranet_closedb();
header("Location: index.php?ClassLevelID=$ClassLevelID&ReportID=$ReportID&msg=$msg");
?>