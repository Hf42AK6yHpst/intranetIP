<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	if ($lreportcard->hasAccessRight())
	{
		$linterface = new interface_html();
		$linterface->LAYOUT_START();
		
		$selection = '';
		$selection .= '<select class="formtextbox" multiple="1" size="20">';
			$selection .= '<optgroup label="ASL">';
				$selection .= '<option>English</option>';
				$selection .= '<option>Chinese</option>';
			$selection .= '</optgroup>';
			$selection .= '<optgroup label="AL">';
				$selection .= '<option>Phy</option>';
				$selection .= '<option>Chem</option>';
				$selection .= '<option>Bio</option>';
			$selection .= '</optgroup>';
		$selection .= '</select>';
	}
}
?>

<?= $selection ?>

<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>
