<?
@SET_TIME_LIMIT(1000);
//using: 

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/flashchart_basic_201301_in_use/php-ofc-library/open-flash-chart.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$YearID = $_POST['YearID'];
$ReportID = $_POST['ReportID'];
$SubjectIDArr = $_POST['SubjectIDArr'];

### Chart Settings
$chartWidth = "586";
$chartHeight = "737";

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
	
	$linterface = new interface_html();
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	$lreportcard_ui = new libreportcard_ui();
	
	if ($lreportcard->hasAccessRight()) 
	{
		$fs = new libfilesystem();
		
		$curTimestamp = time();
		$folder_prefix = $intranet_root."/file/reportcard2008/tmp_chart/raw_mark_distribution/".$_SESSION['UserID']."/";
		
		### Clear the tmp png folder
		if (file_exists($folder_prefix))
			$fs->folder_remove_recursive($folder_prefix);
		
		### Create the tmp png folder if not exist
		//if (!file_exists($folder_prefix))
		//{
			$fs->folder_new($folder_prefix);
			chmod($folder_prefix, 0777);
		//}
			
			
		### Clear the tmp png folder
		//$fs->folder_remove_recursive($folder_prefix);
		
		echo '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-3.3.1.min.js"></script>';
		echo $linterface->Include_HighChart_JS();
			
		### Form Name
		$objYear = new Year($YearID);
		$FormName = $objYear->YearName;
		
		$SchoolName = $lreportcard_ui->Get_School_Name($Bilingual=0);
		$ReportInfoArr = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		$ReportCardTitle = str_replace(':_:', ' ', $ReportInfoArr['ReportTitle']);
		
		$ReportTitle = '';
		$ReportTitle .= $SchoolName.'&nbsp;&nbsp;&nbsp;&nbsp;'.$ReportCardTitle;
		$ReportTitle .= '<br />';
		$ReportTitle .= $eReportCard['Reports_RawMarkDistribution'];
		
		### Get Classes in the Form
		$ClassInfoArr = $lreportcard->GET_CLASSES_BY_FORM($YearID, $ClassID="", $returnAssoName=1, $returnAssoInfo=0);
		$numOfClass = count($ClassInfoArr);
		// evenly distribute the remaining space
		$ClassColumnWidth = ($numOfClass > 0)? floor(90 / $numOfClass) : 0;
		$ClassColumnWidth .= '%';
		
		### Subject Mark-Position Info
		// $MarkStatisticsArr[$SubjectID][$YearClassID][Info...] = value
		$MarkStatisticsArr = $lreportcard->Get_Mark_Statistics_Info($ReportID, $SubjectIDArr);
		$numOfStubject = count($SubjectIDArr);
		
		echo $lreportcard->Get_Report_Header($eReportCard['Reports_RawMarkDistribution']);
		echo $lreportcard->Get_GrandMS_CSS();
		//echo '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
		//				<tr>
		//					<td valign="top">';
							
		$chartArr = array();
		for ($i=0; $i<$numOfStubject; $i++)
		{
			$table = '';
			
			$thisSubjectID = $SubjectIDArr[$i];
			$thisSubjectName = $lreportcard->GET_SUBJECT_NAME($thisSubjectID, $ShortName=0, $Bilingual=0);
			$thisReportTitle = $ReportTitle.'&nbsp;&nbsp;&nbsp;&nbsp;'.$thisSubjectName.'&nbsp;&nbsp;&nbsp;&nbsp;'.$FormName;
			
			### Convert the data to the flash chart format
			$ChartDataArr = array();
			
			if (is_array($MarkStatisticsArr[$thisSubjectID]))
			{
				$MarkIndexArr = array_keys((array)$MarkStatisticsArr[$thisSubjectID][0]['MarkInfo']);
			
				$MinMark = min($MarkIndexArr);
				$MaxMark = max($MarkIndexArr);
				
				for ($j=$MinMark; $j<=$MaxMark; $j++)
				{
					$thisMark = $j;
					$thisMarkInfoArr = $MarkStatisticsArr[$thisSubjectID][0]['MarkInfo'][$thisMark];
					
					if (is_array($thisMarkInfoArr))
					{
						$thisStudentNum = $thisMarkInfoArr['Count'];
						$thisStudentNumPercentage = $thisMarkInfoArr['StudentNumPercentage'];
						$thisAccumlativeStudentNumPercentage = $thisMarkInfoArr['AccumlativeStudentNumPercentage'];
						$thisChartRemarks = $thisStudentNumPercentage.'% ('.$thisAccumlativeStudentNumPercentage.'%)';
					}
					else
					{
						$thisStudentNum = 0;
						$thisChartRemarks = '';
					}
					
					$ChartDataArr[$thisMark]['NumOfStn'] = $thisStudentNum;
					$ChartDataArr[$thisMark]['Remarks'] = $thisChartRemarks;
				}
			}
			else
			{
				$ChartDataArr[0]['NumOfStn'] = 0;
				$ChartDataArr[0]['Remarks'] = '';
			}
			
			
			
			/*
			foreach ((array)$MarkStatisticsArr[$thisSubjectID][0]['MarkInfo'] as $thisMark => $thisMarkInfoArr)
			{
				$thisStudentNum = $thisMarkInfoArr['Count'];
				
				$thisStudentNumPercentage = $thisMarkInfoArr['StudentNumPercentage'];
				$thisAccumlativeStudentNumPercentage = $thisMarkInfoArr['AccumlativeStudentNumPercentage'];
				$thisChartRemarks = $thisStudentNumPercentage.' ('.$thisAccumlativeStudentNumPercentage.')';
				
				$ChartDataArr[$thisMark]['NumOfStn'] = $thisStudentNum;
				$ChartDataArr[$thisMark]['Remarks'] = $thisChartRemarks;
			}
			
 			debug_pr($ChartDataArr);
			${'chart'.$i} = drawChart($ChartDataArr);
			
			*/
			# Use Highchart instead of flash chart
			$chartArr[] = $ChartDataArr;
			### Display the Report
			if ($i == $numOfStubject-1)	// last one => no page break
				$PageBreakStyle = '';
			else
				$PageBreakStyle = "style='page-break-after:always'";
							
			$table .= "<div $PageBreakStyle>";
				### Report Title
				$table .= "<table width='100%' border='0' cellpadding='5' cellspacing='0'>\n";
					$table .= "<tr>\n";
						$table .= "<td>".$thisReportTitle."</td>\n";
					$table .= "</tr>\n";
				$table .= "<table>\n";
				
				$table .= "<br style='clear:both' />\n";
					
				### Mark Distribution png
				$table .= "<table width='95%' border='0' cellpadding='5' cellspacing='0' align='center'>\n";
					$table .= "<tr>\n";
						$table .= "<td>\n";
							### png here
							$table .= "<td align='center' style='width:<?=$chartWidth?>px; height:<?=$chartHeight?>px;'>\n";
								$table .= "<div id='div".$i."' style='width:100%; height:100%'></div>";
// 								$table .= "<div id='div".$i."parent' style='width:100%; height:100%;'>\n";
// 									$table .= "<div id='div".$i."' style='width:100%; height:100%;'>".$Lang['General']['Loading']."</div>\n";
// 								$table .= "</div>\n";
							$table .= "</td>\n";
						$table .= "</td>\n";
					$table .= "</tr>\n";
				$table .= "<table>\n";
				
				$table .= "<br style='clear:both' />\n";
					
				### Form Statistics
				$FormMean = $MarkStatisticsArr[$thisSubjectID][0]['Average_Rounded'];
				$FormSD = $MarkStatisticsArr[$thisSubjectID][0]['SD_Rounded'];
				$FormPassingPercentage = $MarkStatisticsArr[$thisSubjectID][0]['Nature']['TotalPassPercentage'];
				$FormLowestPssingMark = $MarkStatisticsArr[$thisSubjectID][0]['LowestPassingMark'];
				
				$FormMean = ($FormMean=='')? 0 : $FormMean;
				$FormSD = ($FormSD=='')? 0 : $FormSD;
				$FormPassingPercentage = ($FormPassingPercentage=='')? 0 : $FormPassingPercentage;
				
				$table .= "<table width='95%' border='0' cellpadding='0' cellspacing='0' align='center'>\n";
					$table .= "<tr>\n";
						$table .= "<td class='text_12px'>".$eReportCard['Mean']." = ".$FormMean."</td>";
						$table .= "<td class='text_12px' align='right'>".$eReportCard['PassingPercentage']." = ".$FormPassingPercentage."%</td>";
					$table .= "</tr>\n";
					$table .= "<tr>\n";
						$table .= "<td class='text_12px'>".$eReportCard['StandardDeviation']." = ".$FormSD."</td>";
						$table .= "<td class='text_12px' align='right'>".$eReportCard['PassingMark']." = ".$FormLowestPssingMark."</td>";
					$table .= "</tr>\n";
				$table .= "<table>\n";	
				
				$table .= "<br style='clear:both' />\n";
				
				### Class Statistics
				$table .= "<table id='ResultTable' class='GrandMSContentTable' width='95%' border='1' cellpadding='1' cellspacing='0' align='center'>\n";
					$table .= "<tr>";
						$table .= "<td width='10%'>".$eReportCard['Class']."</td>";
						# display classes name
						foreach ($ClassInfoArr as $YearClassID => $YearClassName)
							$table .= "<td align='center' width='".$ClassColumnWidth."'>".$YearClassName."</td>";
					$table .= "</tr>";
					
					### No. Sat
					$table .= "<tr>";
						$table .= "<td>".$eReportCard['No.Sat']."</td>";
						foreach ($ClassInfoArr as $YearClassID => $YearClassName)
						{
							$thisDisplay = ($MarkStatisticsArr[$thisSubjectID][$YearClassID]['NumOfStudent']=='')? 0 : $MarkStatisticsArr[$thisSubjectID][$YearClassID]['NumOfStudent'];
							$table .= "<td align='center'>".$thisDisplay."</td>";
						}
					$table .= "</tr>";
					
					### % Pass
					$table .= "<tr>";
						$table .= "<td>".$eReportCard['PercentagePass']."</td>";
						foreach ($ClassInfoArr as $YearClassID => $YearClassName)
						{
							if ($lreportcard->HardCodePassMark=='')
							{
								$thisPassStudent = $MarkStatisticsArr[$thisSubjectID][$YearClassID]['Nature']['TotalPass'];
								$thisPassPercentage = $MarkStatisticsArr[$thisSubjectID][$YearClassID]['Nature']['TotalPassPercentage'];
							}
							else
							{
								$thisPassStudent = $MarkStatisticsArr[$thisSubjectID][$YearClassID]['Nature_'.$lreportcard->HardCodePassMark]['TotalPass'];
								$thisPassPercentage = $MarkStatisticsArr[$thisSubjectID][$YearClassID]['Nature_'.$lreportcard->HardCodePassMark]['TotalPassPercentage'];
							}
							$thisDisplay = ($thisPassStudent=='')? '---' : $thisPassPercentage."% (".$thisPassStudent.")";
							
							$table .= "<td align='center'>".$thisDisplay."</td>";
						}
					$table .= "</tr>";
					
					### Max. Score
					$table .= "<tr>";
						$table .= "<td>".$eReportCard['MaxScore']."</td>";
						foreach ($ClassInfoArr as $YearClassID => $YearClassName)
						{
							$thisDisplay = ($MarkStatisticsArr[$thisSubjectID][$YearClassID]['MaxMark']=='')? '---' : $MarkStatisticsArr[$thisSubjectID][$YearClassID]['MaxMark'];
							$table .= "<td align='center'>".$thisDisplay."</td>";
						}
					$table .= "</tr>";
					
					### Min. Score
					$table .= "<tr>";
						$table .= "<td>".$eReportCard['MinScore']."</td>";
						foreach ($ClassInfoArr as $YearClassID => $YearClassName)
						{
							$thisDisplay = ($MarkStatisticsArr[$thisSubjectID][$YearClassID]['MinMark']=='')? '---' : $MarkStatisticsArr[$thisSubjectID][$YearClassID]['MinMark'];
							$table .= "<td align='center'>".$thisDisplay."</td>";
						}
					$table .= "</tr>";
					
					### Mean
					$table .= "<tr>";
						$table .= "<td>".$eReportCard['Mean']."</td>";
						foreach ($ClassInfoArr as $YearClassID => $YearClassName)
						{
							$thisDisplay = ($MarkStatisticsArr[$thisSubjectID][$YearClassID]['Average_Rounded']=='')? '---' : $MarkStatisticsArr[$thisSubjectID][$YearClassID]['Average_Rounded'];
							$table .= "<td align='center'>".$thisDisplay."</td>";
						}
					$table .= "</tr>";
					
					### Standard Deviation
					$table .= "<tr>";
						$table .= "<td>".$eReportCard['StandardDeviation']."</td>";
						foreach ($ClassInfoArr as $YearClassID => $YearClassName)
						{
							$thisDisplay = ($MarkStatisticsArr[$thisSubjectID][$YearClassID]['SD_Rounded']=='')? '---' : $MarkStatisticsArr[$thisSubjectID][$YearClassID]['SD_Rounded'];
							$table .= "<td align='center'>".$thisDisplay."</td>";
						}
					$table .= "</tr>";
				$table .= "</table>\n";
				
				$table .= "<br style='clear:both' />\n";
			$table .= "</div>\n";
			
			echo $table;
			ob_flush();
    		flush();
		}
		
		//echo '				</td>
		//				</tr>
		//			</table>';
		echo $lreportcard->Get_Report_Footer();
		?>
		
		<script type='text/javascript'>
			$(document).ready(function(){
				<?php foreach($chartArr as $i => $chart){?>
					data = {};
					 data.xItems = [];
					<?php foreach($chart as $index => $data){?>
						data.xItems.push({y: <?=$data['NumOfStn']?>, x: <?=$index?>, remarks: '<?=$data['Remarks']?>'});
					<?php }?>
					generate_chart('div<?=$i?>', data);
				<?php }?>
			});
			function generate_chart(divId, dataAry){
				Highcharts.chart(divId, {
					chart:{
						type: 'bar',
						width: <?=$chartWidth?>,
						height: <?=$chartHeight?>
					},
					title:{
						text: ''
					},
					yAxis:{
						title: {
							text: 'No. of Students'
						},
						minTickInterval: 1
					},
					xAxis:{
						title: {
							text: 'Raw Marks'
						},
						tickInterval: 1,
						tickWidth: 0,
						tickmarkPlacement: 'on',
				        startOnTick: false,
				        endOnTick: true,
				        minorTickInterval: 1
					},
					series:[{color: '#999999',data:dataAry.xItems, dataLabels:{format: '{point.remarks}', padding: 0,style:{fontSize: '9px', fontWeight: 'normal'}}}],
					credits:{
						enabled: false
					},
					tooltip:{
						enabled: false
					},
					legend:{
						enabled: false
					},
					exporting:{
						enabled: false
					},
				    plotOptions: {
					    series: {
						    pointWidth: 10
					    },
				        bar: {
				            dataLabels: {
				                enabled: true
				            }
				        }
				    }
				});
			}
		</script>
	
		
		<?
		intranet_closedb();
	}
}


?>