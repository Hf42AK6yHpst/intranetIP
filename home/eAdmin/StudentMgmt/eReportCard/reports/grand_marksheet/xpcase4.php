<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_sis.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();

$lreportcard = new libreportcardSIS();
$linterface = new interface_html();

# Retrieve params
$ReportColumnID	= isset($ReportColumnID) ? $ReportColumnID 	: "";
$SubjectID		= isset($SubjectID) 	? $SubjectID 	: "";
$ClassLevelID	= isset($ClassLevelID)	? $ClassLevelID : "";
$ClassID		= isset($ClassID)		? $ClassID : "";
$ReportID		= isset($ReportID)		? $ReportID : "";

$StudentAry = array();
$ListAry = array();
$DataTemp = array();

if(!$ClassLevelID || !$ReportID || $ReportColumnID!="")
{
	intranet_closedb();
	header("Location: index.php");
}

include_once($PATH_WRT_ROOT."templates/2007a/layout/print_header.php");

# retrieve ReportColumnID
$ColID = array();
$SemNameAry = $lreportcard->returnReportColoumnTitle($ReportID);

$ReportColoumnTitleTemp = $lreportcard->returnReportTemplateColumnData($ReportID);
foreach($ReportColoumnTitleTemp as $k=>$d)
{
	$ColID[] = $d['ReportColumnID'];
}

# Grand MS  Title
$SubjectStr = $SubjectID ? $lreportcard->GET_SUBJECT_NAME_LANG($SubjectID) : "OverAll";
$Title = "Pupils' Progress of ". $SubjectStr ." Subject Marks In Same Level";

# Retrieve Student Array
$classArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID, $ClassID);
foreach($classArr as $key => $data)
{
	$thisClassID = $data['ClassID'];
	$StudentAryTemp = $lreportcard->GET_STUDENT_BY_CLASS($thisClassID);
	foreach($StudentAryTemp as $k=>$d)
	{
		list($thisUserID, $WebSAMS, $ClassNumber, $EngName) = $d;
		$StudentAry[] = $thisUserID;
		$lu = new libuser($thisUserID);
		$DataTemp[$thisUserID]['StudentName'] = $lu->EnglishName ." (". $lu->ChineseName .")";
		$DataTemp[$thisUserID]['ClassName'] = $lu->ClassName;
	}
}

# Subject Array
$SubjectAryTmp = $lreportcard->returnSubjectwOrder($ClassLevelID);
foreach($SubjectAryTmp as $s => $temp)
{
	$thisSubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $s);
	$thisScaleDisplay = $thisSubjectFormGradingSettings[ScaleDisplay];
	if($thisScaleDisplay=="M")			$SubjectAry[] = $s;
}

# Table header
$th = array();
$th[]= "&nbsp;";
$th[]= "Student Name";
foreach($ColID as $k => $cid)
	$th[] = $SemNameAry[$cid];	
$th[]= "Difference (%)";
$th[]= "Class Name";

$x = "<tr><td colspan=\"". sizeof($th) ."\" class=\"tabletext tableline\">School Year: ". getCurrentAcademicYear() ."</td></tr>";
$x .= "<tr>";
foreach($th as $k=>$d)
{
	$x .= "<td class=\"tabletext tableline\"><b>".$d."</b></td>";
}
$x .= "</tr>";

# construct data array for Calculate Total
foreach($StudentAry as $k=>$sid)
{
	$MarksAry = $lreportcard->getMarks($ReportID, $sid);
	
	foreach($MarksAry as $sjid=>$data)
	{
		foreach($data as $cid=>$m_ary)
 			$m[$cid][$sjid] = $m_ary['Mark'];
	}
		
	foreach($ColID as $k=>$cid)
	{
		$t = $lreportcard->CALCULATE_COLUMN_MARK($ReportID, $ClassLevelID, $StudentID, $m[$cid]);
		$SemTotal[$sid][$cid] = $t['GrandTotal'];	
	}
}

# construct data array
foreach($StudentAry as $k=>$stu)
{
	$thisStudentID = $stu;
	
	$ListAry[$thisStudentID]['StudentName'] = $DataTemp[$thisStudentID]['StudentName'];
	
	for($i=0; $i<sizeof($ColID); $i++)
	{
		$SemTotalTemp[$i] = $SemTotal[$thisStudentID][$ColID[$i]];
		$ListAry[$thisStudentID][$ColID[$i]] = $SemTotal[$thisStudentID][$ColID[$i]];
	}

	# Difference
	$ListAry[$thisStudentID]['Difference'] = number_format((($SemTotalTemp[1] - $SemTotalTemp[0])/$SemTotalTemp[0]) * 100);
	
	$ListAry[$thisStudentID]['ClassName'] = $DataTemp[$thisStudentID]['ClassName'];
}
# sort by Marks
foreach ($ListAry as $key => $row) 
{
	$field1[$key] = $row['Difference'];
}
array_multisort($field1, SORT_DESC, $ListAry);


$display = "";
$xi = 0;
foreach($ListAry as $key=>$row)
{
	$xi++;
	$css = "tablerow" . (($xi%2)+1);
	
	$display .= "<tr>";		
	
	$display .= "<td class=\"$css tabletext\">$xi</td>";
	
	foreach($row as $k => $d)	
		$display .= "<td class=\"$css tabletext\">$d</td>";
	$display .= "</tr>";
}


?>

<style type='text/css'>
.tableline {
	border-bottom-width: 2px;
	border-bottom-style: solid;
	border-bottom-color: #000000;
}
</style>

<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>			
										
<!-- Start Main Table //-->
<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr> 
    <td align="center" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="center" class="tabletext"><h2><?=$Title?></h2></td>
          </tr>
          <tr>
            <td>
            	
            	<table width="100%" border="0" cellspacing="0" cellpadding="4">
				<?=$x?>
                <?=$display?>
                </table>
            </td>
          </tr>
          <tr><td height="1" class="tabletext tableline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
          <tr><td class="tabletext">&nbsp;<?=date("l, F d, Y");?></td></tr>
        </table>
          </td>
      </tr>
    </table>
    </td>
  </tr>
</table>

<!-- End Main Table //-->

<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>
                        
<?
//debug_r($ListAry);

intranet_closedb();
?>
