<?php
//using : marcus
// Root path
$PATH_WRT_ROOT = "../../../../../../";

// Page access right
$PageRight = "TEACHER";

// Include general libraries
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

// Authorize intranet user & connect Database
intranet_auth();
intranet_opendb();

// Check if ReportCard module is enable
if ($plugin['ReportCard2008']) {
	// Include ReportCard libraries
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
	
	// Create objects
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard2008w();
	}
	#$linterface = new interface_html();
	$lclass = new libclass();
	
	// Check module access right
	if ($lreportcard->hasAccessRight()) {
		// Check required fields submitted from index.php
		// optional: $SubjectID, $ReportColumnID
		if (!isset($ClassLevelID, $ClassID, $ReportID) || empty($ClassLevelID) || empty($ReportID) || empty($ClassID)) {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		# get display options
		// 1. student name display (EnglishName, ChineseName)
		$showEnglishName = false;
		$showChineseName = false;
		if ($StudentNameDisplay == NULL)
		{
			$numOfNameColumn = 0;
		}
		else
		{
			$StudentNameDisplay = explode(",", $StudentNameDisplay);
			$numOfNameColumn = sizeof($StudentNameDisplay);
			foreach ($StudentNameDisplay as $key => $value)
			{
				if ($value == "EnglishName") $showEnglishName = true;
				if ($value == "ChineseName") $showChineseName = true;
			}
		}		
		// 2. ranking display (ClassRanking, OverallRanking)
		$showClassRanking = false;
		$showOverallRanking = false;
		$RankingDisplay = explode(",", $RankingDisplay);
		foreach ($RankingDisplay as $key => $value)
		{
			if ($value == "ClassRanking") $showClassRanking = true;
			if ($value == "OverallRanking") $showOverallRanking = true;
		}		
		// 3. show subject component (0,1)
		$showSubjectComponent = $ShowSubjectComponent;
		// 4. show Summary Info (0,1)
		$showSummaryInfo = $ShowSummaryInfo;		
		// 5. show Grand Total (0,1)
		$showGrandTotal = $ShowGrandTotal;		
		// 6. show Grand Average (0,1)
		$showGrandAverage = $ShowGrandAverage;
		// 7. ForSSPA => no decimal place in all marks
		$ForSSPA = $ForSSPA;
		// 8. show Statistics (0,1)
		$showStatistics = $ShowStatistics;
		
		$reportCalculationOrder = $lreportcard->returnReportTemplateCalculation($ReportID);
		
		// Get ReportCard info
		$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		$SemID = $reportTemplateInfo['Semester'];
		if ($SemID != "F")
			$SemesterTitle = $lreportcard->returnSemesters($SemID);
		else
			$SemesterTitle = $eReportCard['Template']['Annual'];
		
		// Check to make sure it is a generated report
		if ($reportTemplateInfo["LastGenerated"] == "0000-00-00 00:00:00" || $reportTemplateInfo["LastGenerated"] == "") {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		// Get subjects of the current ClassLevel
		$FormSubjectArr = $lreportcard->returnSubjectwOrder($ClassLevelID, 1, '', $intranet_session_language);
		
		$CmpSubjectIDList = array();
		$ParentSubjectIDList = array();
		$ParentCmpSubjectNum = array();
		
		// Reformat the subject array - Display component subjects also
		$FormSubjectArrFlat = array();
		if (sizeof($FormSubjectArr) > 0) {
			foreach($FormSubjectArr as $tmpSubjectID => $tmpSubjectName) {
				if (sizeof($tmpSubjectName) > 1) {
					$ParentSubjectIDList[] = $tmpSubjectID;
					$ParentCmpSubjectNum[$tmpSubjectID] = sizeof($tmpSubjectName) - 1;
					foreach ($tmpSubjectName as $tmpCmpSubjectID => $tmpCmpSubjectName) {
						if ($tmpCmpSubjectID != 0) {
							$CmpSubjectIDList[] = $tmpCmpSubjectID;
							$FormSubjectArrFlat[$tmpCmpSubjectID] = array(0 => $tmpCmpSubjectName);
						}
					}
				}
				$FormSubjectArrFlat[$tmpSubjectID] = array(0 => $tmpSubjectName[0]);
			}
		}
		$FormSubjectArr = $FormSubjectArrFlat;
		
		// Get component subjects if SubjectID is provided
		if (isset($SubjectID) && $SubjectID !== "" && in_array($SubjectID, $ParentSubjectIDList)) {
			$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
			$CmpSubjectIDNameArr = array();
			$CmpSubjectIDArr = array();
			for($i=0; $i<sizeof($CmpSubjectArr); $i++) {
				$CmpSubjectIDArr[] = $CmpSubjectArr[$i]["SubjectID"];
				$CmpSubjectIDNameArr[$CmpSubjectArr[$i]["SubjectID"]] = $CmpSubjectArr[$i]["SubjectName"];
			}
		}
		
		// Get subjects' grading scheme of the current ClassLevel
		$FormSubjectGradingSchemeArr = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID);
		
		// Get ClassLevel name
		$ClassLevelName = $lreportcard->returnClassLevel($ClassLevelID);
		
		// Get Classes
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		
		// Get current school year
		$SchoolYear = $lreportcard->schoolYear;
		
		// Get columns info of the current ReportCard
		$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
		$ColumnIDList = array();
		$ColumnIDList = ($ColumnTitleAry == NULL)? NULL : array_keys($ColumnTitleAry);
		
		// Get the list of students (in a class or entire class level)
		$ClassName = "";
		if (!isset($ClassID) || empty($ClassID)) {
			intranet_closedb();
			header("Location: index.php");
			exit();
		} else {
			$studentList = $lreportcard->GET_STUDENT_BY_CLASS($ClassID, "", 1);
			// Get the Class Name
			for($i=0; $i<sizeof($ClassArr); $i++) {
				if ($ClassArr[$i]["ClassID"] == $ClassID) {
					$ClassName = $ClassArr[$i]["ClassName"];
					break;
				}
			}
		}
		
		$studentIDList = array();
		$studentIDNameMap = array();
		$studentIDRegNoMap = array();
		for($i=0; $i<sizeof($studentList); $i++) {
			$studentIDNameMap[$studentList[$i]["UserID"]] = $studentList[$i][3];
			$studentIDRegNoMap[$studentList[$i]["UserID"]] = $studentList[$i]["WebSAMSRegNo"];
			if ($studentList[$i][2] != "")
				$studentIDList[$studentList[$i][2]] = $studentList[$i]["UserID"];
			else
				$studentIDList[] = $studentList[$i]["UserID"];
		}
		
		// Students should be already sorted by Class Number in the SQL Query
		#asort($studentIDList);
		$studentIDListSql = implode(",", array_values($studentIDList));
		$cond = "";
		
		// Get overall mark or term/assessment mark
		$ColumnTitle = "";
		if (!isset($ReportColumnID) || empty($ReportColumnID)) {
			$ColumnTitle = $eReportCard['Template']['OverallCombined'];
			$cond .= "AND (a.ReportColumnID = '' OR a.ReportColumnID = '0') ";
			$reportColumnID = 0;
		} else {
			$ColumnTitle = $ColumnTitleAry[$ReportColumnID];
			$cond .= "AND a.ReportColumnID = '$ReportColumnID'";
			$reportColumnID = $ReportColumnID;
		}
		
		// Get individual subject mark or grand average (which table to retrieve)
		$SubjectName = "";
		if (!isset($SubjectID) || empty($SubjectID)) {
			// No SubjectID provided, get the average result
			$table = $lreportcard->DBName.".RC_REPORT_RESULT as a";
			$fieldToSelect = "StudentID, GrandTotal, GrandAverage, OrderMeritClass, OrderMeritForm";
			
			$sql = "SELECT $fieldToSelect FROM $table WHERE ";
			$sql .= "ReportID = '$ReportID' ";
			$sql .= "AND StudentID IN ($studentIDListSql) ";
			$sql .= $cond;
			$tempArr = $lreportcard->returnArray($sql);
			

			$resultAverage = array();
			if (sizeof($tempArr) > 0) {
				
				for($j=0; $j<sizeof($tempArr); $j++) {
					$ManualAjustedMark = $lreportcard->Get_Manual_Adjustment($ReportID, $tempArr[$j]["StudentID"],$ReportColumnID);
					$thisGrandMark = $ManualAjustedMark[$tempArr[$j]["StudentID"]][$reportColumnID][0];
					debug($thisGrandMark);
					$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = !($thisGrandMark["GrandTotal"])?$thisGrandMark["GrandTotal"]:$tempArr[$j]["GrandTotal"];
					$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = !($thisGrandMark["GrandAverage"])?$thisGrandMark["GrandAverage"]:$tempArr[$j]["GrandAverage"];
					
					# Round up all the marks if for SSPA
					if ($ForSSPA)
					{
						$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = round($resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"]);
						$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = round($resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"]);
					}
					
					if ($tempArr[$j]["OrderMeritClass"] > 0 || ($thisGrandMark["OrderMeritClass"])) {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritClass"] = !($thisGrandMark["OrderMeritClass"])?$thisGrandMark["OrderMeritClass"]:$tempArr[$j]["OrderMeritClass"];
					} else {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritClass"] = "-";
					}
					if ($tempArr[$j]["OrderMeritForm"] > 0 || ($thisGrandMark["OrderMeritForm"])) {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritForm"] = !($thisGrandMark["OrderMeritForm"])?$thisGrandMark["OrderMeritForm"]:$tempArr[$j]["OrderMeritForm"];
					} else {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritForm"] = "-";
					}
				}
			}
			
			foreach($FormSubjectArr as $tmpSubjectID => $tmpSubjectName) {
				$subjectGradingScheme = $FormSubjectGradingSchemeArr[$tmpSubjectID];
				$subjectGradingSchemeInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($subjectGradingScheme["schemeID"]);
				
				$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				
			/*	if ($subjectGradingScheme["scaleDisplay"] == "M") {
					$fieldToSelect = "StudentID, Mark As MarkUsed, Grade";
				} else {
					$fieldToSelect = "StudentID, Grade As MarkUsed, Grade";
				}
				
				$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE";
				
				$sql = "SELECT $fieldToSelect FROM $table WHERE ";
				$sql .= "ReportID = '$ReportID' ";
				$sql .= "AND StudentID IN ($studentIDListSql) ";
				$sql .= "AND  SubjectID = '$tmpSubjectID' ";
				$sql .= $cond;
				$tempArr = $lreportcard->returnArray($sql);
			*/
			//debug($sql);
			#modifying by marcus
			if ($subjectGradingScheme["scaleDisplay"] == "M") {
				$fieldToSelect = "a.StudentID, a.OrderMeritClass, a.OrderMeritForm, IFNULL(b.AdjustedValue,a.Mark) As MarkUsed, IFNULL(b.AdjustedValue,a.Grade)as Grade";
			} else {
				$fieldToSelect = "a.StudentID, a.OrderMeritClass, a.OrderMeritForm, IFNULL(b.AdjustedValue,a.Grade) As MarkUsed, IFNULL(b.AdjustedValue,a.Grade)as Grade";
			}
			$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE a LEFT JOIN ".$lreportcard->DBName.".RC_MANUAL_ADJUSTMENT b ON a.ReportID = b.ReportID AND a.StudentID = b.StudentID AND a.SubjectID = b.SubjectID AND a.ReportColumnID = b.ReportColumnID";
			$sql = " 
				SELECT 
					$fieldToSelect
				FROM 
					$table 
				WHERE 
					a.ReportID = '$ReportID' 
					AND a.StudentID IN ($studentIDListSql) 
					AND a.SubjectID = '$tmpSubjectID' 
					$cond	
				";
				$tempArr = $lreportcard->returnArray($sql);
			
				$resultSubject[$tmpSubjectID] = array();
				if (sizeof($tempArr) > 0) {
					for($j=0; $j<sizeof($tempArr); $j++) {
						$thisMark = $tempArr[$j]["MarkUsed"];
						$thisGrade = $tempArr[$j]["Grade"];
						list($thisMark, $needStyle) = $lreportcard->checkSpCase($ReportID, $tmpSubjectID, $thisMark, $thisGrade);
						$resultSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = $thisMark;
												
						# Round up all the marks if for SSPA
						if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M" && is_numeric($thisMark))
						{
							$resultSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = round($resultSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]]);
						}
					}
				}
			}
		} else {
			# Specific Subject
			$subjectGradingScheme = $FormSubjectGradingSchemeArr[$SubjectID];
			$subjectGradingSchemeInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($subjectGradingScheme["schemeID"]);
			
			$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID);
			$SchemeID = $SubjectFormGradingSettings['SchemeID'];
			$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
			$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
			
			if ($subjectGradingScheme["scaleDisplay"] == "M") {
				$fieldToSelect = "StudentID, OrderMeritClass, OrderMeritForm, Mark As MarkUsed, Grade";
			} else {
				$fieldToSelect = "StudentID, OrderMeritClass, OrderMeritForm, Grade As MarkUsed, Grade";
			}
			$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE";
			
			$sql = "SELECT $fieldToSelect FROM $table WHERE ";
			$sql .= "ReportID = '$ReportID' ";
			$sql .= "AND StudentID IN ($studentIDListSql) ";
			$sql .= "AND SubjectID = '$SubjectID' ";
			
			$tempArr = $lreportcard->returnArray($sql.$cond);
			
			$resultSubject = array();
			$OrderClassSubject = array();
			if (sizeof($tempArr) > 0) {
				for($j=0; $j<sizeof($tempArr); $j++) {
					
					$thisMark = $tempArr[$j]["MarkUsed"];
					$thisGrade = $tempArr[$j]["Grade"];
					list($thisMark, $needStyle) = $lreportcard->checkSpCase($ReportID, $SubjectID, $thisMark, $thisGrade);
					
					$resultSubject[$tempArr[$j]["StudentID"]] = $thisMark;
										
					# Round up all the marks if for SSPA
					if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M" && is_numeric($thisMark))
					{
						$resultSubject[$tempArr[$j]["StudentID"]] = round($resultSubject[$tempArr[$j]["StudentID"]]);
					}
					
					if ($tempArr[$j]["OrderMeritClass"] > 0) {
						$OrderClassSubject[$tempArr[$j]["StudentID"]] = $tempArr[$j]["OrderMeritClass"];
					} else {
						$OrderClassSubject[$tempArr[$j]["StudentID"]] = "-";
					}
					
					if ($tempArr[$j]["OrderMeritForm"] > 0) {
						$OrderFormSubject[$tempArr[$j]["StudentID"]] = $tempArr[$j]["OrderMeritForm"];
					} else {
						$OrderFormSubject[$tempArr[$j]["StudentID"]] = "-";
					}
				}
			}
			
			// If gettng subject overall mark for a single subject, get the column mark also
			if (!isset($ReportColumnID) || empty($ReportColumnID)) {
				for($i=0; $i<sizeof($ColumnIDList); $i++) {
					$tmpCond = "AND ReportColumnID = '".$ColumnIDList[$i]."'";
					$tempArr = $lreportcard->returnArray($sql.$tmpCond);
					$resultSubjectColumn[$ColumnIDList[$i]] = array();
					if (sizeof($tempArr) > 0) {
						for($j=0; $j<sizeof($tempArr); $j++) {
							$thisMark = $tempArr[$j]["MarkUsed"];
							$thisGrade = $tempArr[$j]["Grade"];
							list($thisMark, $needStyle) = $lreportcard->checkSpCase($ReportID, $SubjectID, $thisMark, $thisGrade);
					
							$resultSubjectColumn[$ColumnIDList[$i]][$tempArr[$j]["StudentID"]] = $thisMark;
							
							# Round up all the marks if for SSPA
							if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M" && is_numeric($thisMark))
							{
								$resultSubjectColumn[$ColumnIDList[$i]][$tempArr[$j]["StudentID"]] = round($resultSubjectColumn[$ColumnIDList[$i]][$tempArr[$j]["StudentID"]]);
							}
						}
					}
				}
			}
			
			#################################
			// Special handling of subject which having component subjects
			if (in_array($SubjectID, $ParentSubjectIDList)) {
				// Push the SubjectID into the same array of its component subjects
				$SubjectPlusCompSubjectIDList = $CmpSubjectIDArr;
				$SubjectPlusCompSubjectIDList[] = $SubjectID;
				
				/*
				// If no ReportColumnID is provided, beside showing the subject overall result, show all column result too
				if (!isset($ReportColumnID) || empty($ReportColumnID)) {
					for($i=0; $i<sizeof($ColumnIDList); $i++) {
					 	$tmpCond1 = "AND ReportColumnID = '".$ColumnIDList[$i]."'";
						for($j=0; $j<sizeof($SubjectPlusCompSubjectIDList); $j++) {
							$tmpSubjectID = $SubjectPlusCompSubjectIDList[$j];
							$subjectGradingScheme = $FormSubjectGradingSchemeArr[$tmpSubjectID];
							$subjectGradingSchemeInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($subjectGradingScheme["schemeID"]);
							
							if ($subjectGradingScheme["scaleDisplay"] == "M") {
								$fieldToSelect = "StudentID, OrderMeritClass, Mark As MarkUsed";
							} else {
								$fieldToSelect = "StudentID, OrderMeritClass, Grade As MarkUsed";
							}
							$sql = "SELECT $fieldToSelect FROM $table WHERE ";
							$sql .= "ReportID = '$ReportID' ";
							$sql .= "AND StudentID IN ($studentIDListSql) ";
							
							$tmpCond2 = "AND SubjectID = '$tmpSubjectID' ";
							$tempArr = $lreportcard->returnArray($sql.$tmpCond1.$tmpCond2);
							if (sizeof($tempArr) > 0) {
								for($k=0; $k<sizeof($tempArr); $k++) {
									$resultSubjectColumnDetail[$ColumnIDList[$i]][$tmpSubjectID][$tempArr[$k]["StudentID"]] = $tempArr[$k]["MarkUsed"];
								}
							}
						}
					}
				}
				*/
				
				for($i=0; $i<sizeof($SubjectPlusCompSubjectIDList); $i++) {
					$tmpSubjectID = $SubjectPlusCompSubjectIDList[$i];
					$subjectGradingScheme = $FormSubjectGradingSchemeArr[$tmpSubjectID];
					$subjectGradingSchemeInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($subjectGradingScheme["schemeID"]);
					
					if ($subjectGradingScheme["scaleDisplay"] == "M") {
						$fieldToSelect = "StudentID, Mark As MarkUsed, Grade";
					} else {
						$fieldToSelect = "StudentID, Grade As MarkUsed, Grade";
					}
					$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE";
					
					$sql = "SELECT $fieldToSelect FROM $table WHERE ";
					$sql .= "ReportID = '$ReportID' ";
					$sql .= "AND StudentID IN ($studentIDListSql) ";
					$sql .= "AND  SubjectID = '$tmpSubjectID' ";
					$sql .= $cond;
					$tempArr = $lreportcard->returnArray($sql);
					
					$resultDetailSubject[$tmpSubjectID] = array();
					if (sizeof($tempArr) > 0) {
						for($j=0; $j<sizeof($tempArr); $j++) {
							$thisMark = $tempArr[$j]["MarkUsed"];
							$thisGrade = $tempArr[$j]["Grade"];
							list($thisMark, $needStyle) = $lreportcard->checkSpCase($ReportID, $tmpSubjectID, $thisMark, $thisGrade);
					
							$resultDetailSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = $thisMark;
							
							# Round up all the marks if for SSPA
							if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M" && is_numeric($thisMark))
							{
								$resultDetailSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = round($resultDetailSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]]);
							}
						}
					}
				}
			}
			#################################
			# get Grand Total and Grand Average
			$table = $lreportcard->DBName.".RC_REPORT_RESULT";
			$fieldToSelect = "StudentID, GrandTotal, GrandAverage, OrderMeritClass, OrderMeritForm";
			
			$sql = "SELECT $fieldToSelect FROM $table WHERE ";
			$sql .= "ReportID = '$ReportID' ";
			$sql .= "AND StudentID IN ($studentIDListSql) ";
			$sql .= $cond;
			$tempArr = $lreportcard->returnArray($sql);
			
			$resultAverage = array();
			if (sizeof($tempArr) > 0) {
				for($j=0; $j<sizeof($tempArr); $j++) {
					$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = $tempArr[$j]["GrandTotal"];
					$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = sprintf("%02.2f", $tempArr[$j]["GrandAverage"]);
					
					# Round up all the marks if for SSPA
					if ($ForSSPA)
					{
						$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = round($resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"]);
						$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = round($resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"]);
					}					
					
					if ($tempArr[$j]["OrderMeritClass"] > 0) {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritClass"] = $tempArr[$j]["OrderMeritClass"];
					} else {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritClass"] = "-";
					}
					if ($tempArr[$j]["OrderMeritForm"] > 0) {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritForm"] = $tempArr[$j]["OrderMeritForm"];
					} else {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritForm"] = "-";
					}
				}
			}
		}
				
		// Get Summary Info from CSV
		# build data array
		$SummaryInfoFields = array("Conduct");
		$ary = array();
		$csvType = $lreportcard->getOtherInfoType();

		if($reportTemplateInfo['Semester']=="F") {
			$InfoTermID = 0;
		} else {
			$InfoTermID = $reportTemplateInfo['Semester'];
		}
		if(!empty($csvType)) {
			foreach($csvType as $k=>$Type) {
				$csvData = $lreportcard->getOtherInfoData($Type, $InfoTermID, $ClassID);	
				if(!empty($csvData)) {
					foreach($csvData as $RegNo=>$data) {
						foreach($data as $key=>$val) {
							if (!is_numeric($key) && trim($key) !== "")
								$ary[$RegNo][$key] = $val;
						}
					}
				}
			}
		}

		# initialize variables to calculate Average, S.D, Variance, Max, Min, Passing Rate
		// $columnSubjectIDMapArr[#column] = SubjectID;  
		$columnSubjectIDMapArr = array();	// For getting the pasing mark from the grading scheme
		// $columnScaleOutputMapArr[#column] = "M" or "G"
		$columnScaleOutputMapArr = array();
		
		$markTable = "<table id='MarkTable' width='100%' border='1' cellpadding='2' cellspacing='0'>";
		
		// Table header
		$cmpNameRow = "";
		$markTable .= "<thead>";
		$markTable .= "<tr>";
		if (!isset($SubjectID) || empty($SubjectID)) {
			$markTable .= "<th rowspan='2'>".$eReportCard['Template']['StudentInfo']['ClassNo']."</th>";
			if ($numOfNameColumn > 0)
			{
				$markTable .= "<th rowspan='2' colspan='$numOfNameColumn'>".$eReportCard['Template']['StudentInfo']['Name']."</th>";
			}
			//$markTable .= "<th rowspan='2'>".$eReportCard['Template']['StudentInfo']['Name']."</th>";
			
			if (!isset($ReportColumnID) || empty($ReportColumnID)) {
				foreach($FormSubjectArr as $tmpSubjectID => $tmpSubjectName) {
					if ($showSubjectComponent)
					{
						if (in_array($tmpSubjectID, $ParentSubjectIDList)) {
							$markTable .= "<th colspan='".($ParentCmpSubjectNum[$tmpSubjectID]+1)."'>".$tmpSubjectName[0]."</th>";
							$cmpNameRow .= "<th>".$eReportCard['Template']['Total']."</th>";
							$columnSubjectIDMapArr[] = $tmpSubjectID;
							
							$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
							$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
								
						} else if (in_array($tmpSubjectID, $CmpSubjectIDList)) {
							$cmpNameRow .= "<th>".$tmpSubjectName[0]."</th>";
							$columnSubjectIDMapArr[] = $tmpSubjectID;
							
							$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
							$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
						} else {
							$markTable .= "<th rowspan='2'>".$tmpSubjectName[0]."</th>";
							$columnSubjectIDMapArr[] = $tmpSubjectID;
							
							$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
							$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
						}
					}
					else
					{
						# Hide component subjects
						if (in_array($tmpSubjectID, $ParentSubjectIDList)) {
							$markTable .= "<th>".$tmpSubjectName[0]."</th>";
							$columnSubjectIDMapArr[] = $tmpSubjectID;
							
							$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
							$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
						}
						else if (!in_array($tmpSubjectID, $CmpSubjectIDList)) 
						{
							$markTable .= "<th rowspan='2'>".$tmpSubjectName[0]."</th>";
							$columnSubjectIDMapArr[] = $tmpSubjectID;
							
							$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
							$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
						}
					}
				}
				
				//$markTable .= "<th rowspan='2'>".$eReportCard['Template']['GrandTotal']."</th>";
				//$markTable .= "<th rowspan='2'>".$eReportCard['Template']['GrandAverage']."</th>";
				//$markTable .= "<th rowspan='2'>".$eReportCard['Template']['Ranking']."</th>";
				$SubjectName = $eReportCard['Template']['AverageMark'];
			} 
			else 
			{
				# Specific Term
				foreach($FormSubjectArr as $tmpSubjectID => $tmpSubjectName) {
					if ($showSubjectComponent)
					{
						if (in_array($tmpSubjectID, $ParentSubjectIDList)) {
							$markTable .= "<th colspan='".($ParentCmpSubjectNum[$tmpSubjectID]+1)."'>".$tmpSubjectName[0]."</th>";
							$cmpNameRow .= "<th>".$eReportCard['Template']['Total']."</th>";
							$columnSubjectIDMapArr[] = $tmpSubjectID;
							
							$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
							$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
						} else if (in_array($tmpSubjectID, $CmpSubjectIDList)) {
							$cmpNameRow .= "<th>".$tmpSubjectName[0]."</th>";
							$columnSubjectIDMapArr[] = $tmpSubjectID;
							
							$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
							$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
						} else {
							$markTable .= "<th rowspan='2'>".$tmpSubjectName[0]."</th>";
							$columnSubjectIDMapArr[] = $tmpSubjectID;
							
							$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
							$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
						}
					}
					else
					{
						# Hide component subjects
						if (in_array($tmpSubjectID, $ParentSubjectIDList)) {
							$markTable .= "<th>".$tmpSubjectName[0]."</th>";
							$columnSubjectIDMapArr[] = $tmpSubjectID;
							
							$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
							$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
						}
						else if (!in_array($tmpSubjectID, $CmpSubjectIDList)) 
						{
							$markTable .= "<th rowspan='2'>".$tmpSubjectName[0]."</th>";
							$columnSubjectIDMapArr[] = $tmpSubjectID;
							
							$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
							$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
						}
					}
				}
				
				if ($reportCalculationOrder == 2) {
					//$markTable .= "<th rowspan='2'>".$eReportCard['Template']['Total']."</th>";
					//$markTable .= "<th rowspan='2'>".$eReportCard['Template']['GrandAverage']."</th>";
					//$markTable .= "<th rowspan='2'>".$eReportCard['Template']['Ranking']."</th>";
					$SubjectName = $eReportCard['Template']['AverageMark'];
				}
			}
		}
		else
		{
			# Specific Subject
			$markTable .= "<th>".$eReportCard['Template']['StudentInfo']['ClassNo']."</th>";
			if ($numOfNameColumn > 0)
			{
				$markTable .= "<th colspan='$numOfNameColumn'>".$eReportCard['Template']['StudentInfo']['Name']."</th>";
			}
			$SubjectName = $FormSubjectArr[$SubjectID][0];
			if (!isset($ReportColumnID) || empty($ReportColumnID)) {
				/*
				for($i=0; $i<sizeof($ColumnIDList); $i++) {
					$markTable .= "<th>".$ColumnTitleAry[$ColumnIDList[$i]]."</th>";
				}
				*/
				##############################
				// Special handling of subject which having component subjects
				if ($showSubjectComponent)
				{
					if (in_array($SubjectID, $ParentSubjectIDList)) 
					{
						$countCmpSubject = 1;
						foreach($CmpSubjectIDNameArr as $tmpSubjectID => $tmpSubjectName) {
							$markTable .= "<th>[".$countCmpSubject."]<br />".$tmpSubjectName."</th>";
							$countCmpSubject++;
							
							$columnSubjectIDMapArr[] = $tmpSubjectID;
							
							$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
							$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
						}
						#$markTable .= "<th>".$eReportCard['Template']['Total']."</th>";
					}
				}
				##############################
				$markTable .= "<th>".$eReportCard['Template']['SubjectOverall']."</th>";
				$columnSubjectIDMapArr[] = $SubjectID;
							
				$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID);
				$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
				
			} else {
				##############################
				// Special handling of subject which having component subjects
				if ($showSubjectComponent && in_array($SubjectID, $ParentSubjectIDList)) {
					foreach($CmpSubjectIDNameArr as $tmpSubjectID => $tmpSubjectName) {
						$markTable .= "<th>".$tmpSubjectName."</th>";
						$columnSubjectIDMapArr[] = $tmpSubjectID;
							
						$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
						$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
					}
					#$markTable .= "<th>".$eReportCard['Template']['Total']."</th>";
				}
				##############################
				
				#$markTable .= "<th>".$ColumnTitleAry[$ReportColumnID]."</th>";
				//$markTable .= "<th>".$eReportCard['Template']['SubjectOverall']."</th>";
				//$markTable .= "<th>".$eReportCard['Template']['Ranking']."</th>";
			}
		}
		
		# Summary Info
		if ($showSummaryInfo)
		{
			for($i=0; $i<sizeof($SummaryInfoFields); $i++) {
				$markTable .= "<th rowspan='2'>".$eReportCard['Template'][$SummaryInfoFields[$i]]."</th>";
				$columnScaleOutputMapArr[] = "G";
			}
		}
		
		$markTable .= ($showGrandTotal)? "<th rowspan='2'>".$eReportCard['Template']['GrandTotal']."</th>" : "";
		$markTable .= ($showGrandAverage)? "<th rowspan='2'>".$eReportCard['Template']['GrandAverage']."</th>" : "";		
		$markTable .= ($showClassRanking)? "<th rowspan='2'>".$eReportCard['Template']['ClassPosition']."</th>" : "";
		$markTable .= ($showOverallRanking)? "<th rowspan='2'>".$eReportCard['Template']['FormPosition']."</th>" : "";
		
		if ($showGrandTotal)
		{
			$columnScaleOutputMapArr[] = "M";
		}
		if ($showGrandAverage)
		{
			$columnScaleOutputMapArr[] = "M";
		}
		
		$markTable .= "</tr>";
		if ($cmpNameRow != "") {
			$markTable .= "<tr>$cmpNameRow</tr>";
		}
		$markTable .= "</thead>";
		
		# initialize variables to calculate Average, S.D, Variance, Max, Min, Passing Rate
		// $markDisplayedArr[#column][#row] = Mark;
		$markDisplayedArr = array();
		// $markNatureArr[#column][#row] = "Distinction" / "Pass" / "Fail";
		$markNatureArr = array();
		$ColumnCounter = 0;
		
		// Mark
		$markTable .= "<tbody>";
		foreach($studentIDList as $classNumber => $studentID) {
			$ColumnCounter = 0;
			
			$markTable .= "<tr>";
			$markTable .= "<td align='center'>$classNumber</td>";			
			//$markTable .= "<td>".$studentIDNameMap[$studentID]."</td>";
			
			// Separate the student name in "English (Chinese)" format			
			$studentNameBi = $studentIDNameMap[$studentID];
			$studentNamePiece = explode("(",$studentNameBi);
			$studentNameEn = trim($studentNamePiece[0]);
			$studentNameCh = trim(str_replace(")", "", $studentNamePiece[1]));
						
			$markTable .= ($showEnglishName)? "<td>".$studentNameEn."</td>" : "";
			$markTable .= ($showChineseName)? "<td>".$studentNameCh."</td>" : "";
			
			if (!isset($SubjectID) || empty($SubjectID)) {
				# all subjects
				
				# overall term
				foreach($FormSubjectArr as $tmpSubjectID => $tmpSubjectName) {
					if ($showSubjectComponent)
					{
						$thisMarks = $resultSubject[$tmpSubjectID][$studentID];
						$markTable .= "<td align='center'>".$thisMarks."</td>";
						
						# Record Marks for Statistics
						$markDisplayedArr[$ColumnCounter][] = (is_numeric($thisMarks))? $thisMarks : '';
							
						# Record Pass Status for Passing Rate
						if (!$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1($thisMarks) && !$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2($thisMarks))
						{
							$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $tmpSubjectID, $thisMarks);
							$markNatureArr[$ColumnCounter][] = $thisNature;
						}
						
						$ColumnCounter++;
					}
					else
					{
						# show parent subject only
						if (!in_array($tmpSubjectID, $CmpSubjectIDList)) {
							$thisMarks = $resultSubject[$tmpSubjectID][$studentID];
							$markTable .= "<td align='center'>".$thisMarks."</td>";
							
							# Record Marks for Statistics
							$markDisplayedArr[$ColumnCounter][] = (is_numeric($thisMarks))? $thisMarks : '';
								
							# Record Pass Status for Passing Rate
							if (!$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1($thisMarks) && !$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2($thisMarks))
							{
								$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $tmpSubjectID, $thisMarks);
								$markNatureArr[$ColumnCounter][] = $thisNature;
							}
							
							$ColumnCounter++;
						}
					}
				}
				
				//$markTable .= "<td align='center'>".$resultAverage[$studentID]["GrandTotal"]."</td>";
				//$markTable .= "<td align='center'>".$resultAverage[$studentID]["GrandAverage"]."</td>";
				//$markTable .= "<td align='center'>".$resultAverage[$studentID]["OrderMeritClass"]."</td>";
				
				# Summary Info
				if ($showSummaryInfo)
				{
					for($i=0; $i<sizeof($SummaryInfoFields); $i++) {
						$thisRegNo = $studentIDRegNoMap[$studentID];
						$thisClass = $studentIDClassMap[$studentID];
						$tmpInfo = isset($ary[$thisRegNo][$SummaryInfoFields[$i]]) ? $ary[$thisRegNo][$SummaryInfoFields[$i]]: "-";
						
						$markTable .= "<td align='center'>".$tmpInfo."</td>";
						
						# Record Marks for Statistics
						$markDisplayedArr[$ColumnCounter][] = (is_numeric($tmpInfo))? $tmpInfo : '';
						
						$ColumnCounter++;
					}
				}
				
				$markTable .= ($showGrandTotal)? "<td align='center'>".$resultAverage[$studentID]["GrandTotal"]."</td>" : "";
				$markTable .= ($showGrandAverage)? "<td align='center'>".$resultAverage[$studentID]["GrandAverage"]."</td>" : "";				
				$markTable .= ($showClassRanking)? "<td align='center'>".$resultAverage[$studentID]["OrderMeritClass"]."</td>" : "";
				$markTable .= ($showOverallRanking)? "<td align='center'>".$resultAverage[$studentID]["OrderMeritForm"]."</td>" : "";
				
				if ($showGrandTotal)
				{
					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandTotal"];
					$ColumnCounter++;
				}
				if ($showGrandAverage)
				{
					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandAverage"];
					$ColumnCounter++;
				}
			} else {
				# specific subject
				if (!isset($ReportColumnID) || empty($ReportColumnID)) {
					# overall term
					/*
					for($i=0; $i<sizeof($ColumnIDList); $i++) {
						$markTable .= "<td align='center'>".$resultSubjectColumn[$ColumnIDList[$i]][$studentID]."</td>";
					}
					*/
					##############################
					// Special handling of subject which having component subjects
					if ($showSubjectComponent && in_array($SubjectID, $ParentSubjectIDList)) {
						foreach($CmpSubjectIDNameArr as $tmpSubjectID => $tmpSubjectName) {
							$thisMarks = $resultDetailSubject[$tmpSubjectID][$studentID];
							$markTable .= "<td align='center'>".$thisMarks."</td>";
							
							# Record Marks for Statistics
							$markDisplayedArr[$ColumnCounter][] = (is_numeric($thisMarks))? $thisMarks : '';
								
							# Record Pass Status for Passing Rate
							if (!$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1($thisMarks) && !$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2($thisMarks))
							{
								$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $tmpSubjectID, $thisMarks);
								$markNatureArr[$ColumnCounter][] = $thisNature;
							}
							
							$ColumnCounter++;
						}
						#$markTable .= "<td align='center'>".$resultDetailSubject[$SubjectID][$studentID]."</td>";
					}
					##############################
					$thisMarks = $resultSubject[$studentID];
					$markTable .= "<td align='center'>".$thisMarks."</td>";
					
					# Record Marks for Statistics
					$markDisplayedArr[$ColumnCounter][] = (is_numeric($thisMarks))? $thisMarks : '';
					
					# Record Pass Status for Passing Rate
					if (!$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1($thisMarks) && !$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2($thisMarks))
					{
						$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $SubjectID, $thisMarks);
						$markNatureArr[$ColumnCounter][] = $thisNature;
					}
					
					$ColumnCounter++;
				} else {
					# specific term
					##############################
					// Special handling of subject which having component subjects
					if ($showSubjectComponent &&  in_array($SubjectID, $ParentSubjectIDList)) {
						foreach($CmpSubjectIDNameArr as $tmpSubjectID => $tmpSubjectName) {
							$thisMarks = $resultDetailSubject[$tmpSubjectID][$studentID];
							$markTable .= "<td align='center'>".$thisMarks."</td>";
							
							# Record Marks for Statistics
							$markDisplayedArr[$ColumnCounter][] = (is_numeric($thisMarks))? $thisMarks : '';
								
							# Record Pass Status for Passing Rate
							if (!$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1($thisMarks) && !$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2($thisMarks))
							{
								$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $tmpSubjectID, $thisMarks);
								$markNatureArr[$ColumnCounter][] = $thisNature;
							}
							
							$ColumnCounter++;
						}
						#$markTable .= "<td align='center'>".$resultDetailSubject[$SubjectID][$studentID]."</td>";
					}
					##############################
					
					//$markTable .= "<td align='center'>".$resultSubject[$studentID]."</td>";
					//$markTable .= "<td align='center'>".$OrderClassSubject[$studentID]."</td>";
				}
				
				# Summary Info
				if ($showSummaryInfo)
				{
					for($i=0; $i<sizeof($SummaryInfoFields); $i++) {
						$thisRegNo = $studentIDRegNoMap[$studentID];
						$thisClass = $studentIDClassMap[$studentID];
						$tmpInfo = isset($ary[$thisRegNo][$SummaryInfoFields[$i]]) ? $ary[$thisRegNo][$SummaryInfoFields[$i]]: "-";
						
						$markTable .= "<td align='center'>".$tmpInfo."</td>";
						$markDisplayedArr[$ColumnCounter][] = $tmpInfo;
						$ColumnCounter++;
					}
				}
				
				$markTable .= ($showGrandTotal)?"<td align='center'>".$resultAverage[$studentID]["GrandTotal"]."</td>" : "";
				$markTable .= ($showGrandAverage)?"<td align='center'>".$resultAverage[$studentID]["GrandAverage"]."</td>" : "";
				$markTable .= ($showClassRanking)? "<td align='center'>".$OrderClassSubject[$studentID]."</td>" : "";
				$markTable .= ($showOverallRanking)? "<td align='center'>".$OrderFormSubject[$studentID]."</td>" : "";
				
				if ($showGrandTotal)
				{
					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandTotal"];
					$ColumnCounter++;
				}
				if ($showGrandAverage)
				{
					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandAverage"];
					$ColumnCounter++;
				}
				
			}
			$markTable .= "</tr>";
		}
		
		if ($showStatistics)
		{
			# Statistics Rows
			$TitleColSpanNum = 1;
			$decimalPlacesShown = 2;
			if ($showEnglishName)
				$TitleColSpanNum++;
			if ($showChineseName)
				$TitleColSpanNum++;
			$TitleColSpan = "colspan='".$TitleColSpanNum."'";
			
			# Average
			$markTable .= "<tr>";
				$markTable .= "<td class='tabletext stat_start_border_top' align='center' $TitleColSpan><b>".$eReportCard['Template']['Average']."</b></td>";
				for ($i=0; $i<count($markDisplayedArr); $i++)
				{
					$thisScaleDisplay = $columnScaleOutputMapArr[$i];
					
					if ($thisScaleDisplay != "M")
					{
						$thisDisplay = $lreportcard->EmptySymbol;
					}
					else
					{
						$thisColumnMarksArr = $markDisplayedArr[$i];
						$thisDisplay = $lreportcard->getAverage($thisColumnMarksArr, $decimalPlacesShown);
					}
					$markTable .= "<td class='tabletext stat_start_border_top' align='center'><b>".$thisDisplay."</b></td>";
				}
				$markTable .= ($showClassRanking)? "<td align='center' class='stat_start_border_top'>".$lreportcard->EmptySymbol."</td>" : "";
				$markTable .= ($showOverallRanking)? "<td align='center' class='stat_start_border_top'>".$lreportcard->EmptySymbol."</td>" : "";
			$markTable .= "<tr>";
			
			# SD
			$markTable .= "<tr>";
				$markTable .= "<td class='tabletext ' align='center' $TitleColSpan><b>".$eReportCard['Template']['SD']."</b></td>";
				for ($i=0; $i<count($markDisplayedArr); $i++)
				{
					$thisScaleDisplay = $columnScaleOutputMapArr[$i];
					
					if ($thisScaleDisplay != "M")
					{
						$thisDisplay = $lreportcard->EmptySymbol;
					}
					else
					{
						$thisColumnMarksArr = $markDisplayedArr[$i];
						$thisDisplay = $lreportcard->getSD($thisColumnMarksArr, $decimalPlacesShown);
					}
					$markTable .= "<td class='tabletext ' align='center'><b>".$thisDisplay."</b></td>";
				}
				$markTable .= ($showClassRanking)? "<td align='center' >".$lreportcard->EmptySymbol."</td>" : "";
				$markTable .= ($showOverallRanking)? "<td align='center' >".$lreportcard->EmptySymbol."</td>" : "";
			$markTable .= "<tr>"; 
			
			# Highest Mark
			$markTable .= "<tr>";
				$markTable .= "<td class='tabletext ' align='center' $TitleColSpan><b>".$eReportCard['Template']['HighestMark']."</b></td>";
				for ($i=0; $i<count($markDisplayedArr); $i++)
				{
					$thisScaleDisplay = $columnScaleOutputMapArr[$i];
					
					if ($thisScaleDisplay != "M")
					{
						$thisDisplay = $lreportcard->EmptySymbol;
					}
					else
					{
						$thisColumnMarksArr = $markDisplayedArr[$i];
						$thisDisplay = $lreportcard->getMaximum($thisColumnMarksArr);
						$thisDisplay = ($thisDisplay == "")? $lreportcard->EmptySymbol : $thisDisplay;
					}
					$markTable .= "<td class='tabletext ' align='center'><b>".$thisDisplay."</b></td>";
				}
				$markTable .= ($showClassRanking)? "<td align='center' >".$lreportcard->EmptySymbol."</td>" : "";
				$markTable .= ($showOverallRanking)? "<td align='center' >".$lreportcard->EmptySymbol."</td>" : "";
			$markTable .= "<tr>";
			
			# Lowest Mark
			$markTable .= "<tr>";
				$markTable .= "<td class='tabletext ' align='center' $TitleColSpan><b>".$eReportCard['Template']['LowestMark']."</b></td>";
				for ($i=0; $i<count($markDisplayedArr); $i++)
				{
					$thisScaleDisplay = $columnScaleOutputMapArr[$i];
					
					if ($thisScaleDisplay != "M")
					{
						$thisDisplay = $lreportcard->EmptySymbol;
					}
					else
					{
						$thisColumnMarksArr = $markDisplayedArr[$i];
						$thisDisplay = $lreportcard->getMinimum($thisColumnMarksArr);
						$thisDisplay = ($thisDisplay == "")? $lreportcard->EmptySymbol : $thisDisplay;
					}
					$markTable .= "<td class='tabletext ' align='center'><b>".$thisDisplay."</b></td>";
				}
				$markTable .= ($showClassRanking)? "<td align='center' >".$lreportcard->EmptySymbol."</td>" : "";
				$markTable .= ($showOverallRanking)? "<td align='center' >".$lreportcard->EmptySymbol."</td>" : "";
			$markTable .= "<tr>";
			
			# Passing Rate
			$markTable .= "<tr>";
				$markTable .= "<td class='tabletext ' align='center' $TitleColSpan><b>".$eReportCard['Template']['PassingRate']."</b></td>";
				for ($i=0; $i<count($markNatureArr); $i++)
				{
					$thisColumnMarksArr = $markNatureArr[$i];
					$thisDisplay = $lreportcard->getPassingRate($thisColumnMarksArr, 2);
					$thisDisplay = ($thisDisplay == "")? $lreportcard->EmptySymbol : $thisDisplay;
					$markTable .= "<td class='tabletext ' align='center'><b>".$thisDisplay."</b></td>";
				}
				$markTable .= ($showSummaryInfo)? "<td align='center' >".$lreportcard->EmptySymbol."</td>" : "";
				$markTable .= ($showGrandTotal)? "<td align='center' >".$lreportcard->EmptySymbol."</td>" : "";
				$markTable .= ($showGrandAverage)? "<td align='center' >".$lreportcard->EmptySymbol."</td>" : "";
				$markTable .= ($showClassRanking)? "<td align='center' >".$lreportcard->EmptySymbol."</td>" : "";
				$markTable .= ($showOverallRanking)? "<td align='center' >".$lreportcard->EmptySymbol."</td>" : "";
			$markTable .= "<tr>";
		}
		$markTable .= "</tbody>";
		$markTable .= "</table>";
		
		
		// Title of the grandmarksheet
		$schoolData = split("\n",get_file_content($PATH_WRT_ROOT."file/school_data.txt"));
		$schoolName = $schoolData[0];
		$academicYear = $lreportcard->GET_ACTIVE_YEAR("-");
		
		$GMSTitle = "$schoolName<br />$academicYear $SemesterTitle $ClassLevelName $ClassName $ColumnTitle $SubjectName ".$eReportCard['Reports_GrandMarksheet']."<br />".$eReportCard['GrandMarksheetTypeOption'][0];
		
		if ($ForSSPA)
		{
			$GMSTitle .= " (SSPA)";
			
			$markTable .= "<table width='100%' border='0' cellpadding='2' cellspacing='0'>";
			$markTable .= "<tr><td>&nbsp;</td></tr>";
			$markTable .= "<tr><td class='footer'>";
			$markTable .= $eReportCard['Template']['SSPA_Footer'];
			$markTable .= "</td></tr>";
			$markTable .= "<tr><td class='signature' align='right'>";
			$markTable .= $eReportCard['Template']['SSPA_Signature'];
			$markTable .= "</td></tr>";
			$markTable .= "</table>";
		}
		
		############## Interface Start ##############
		
?>
<html>
	<head>
		<meta http-equiv='pragma' content='no-cache' />
		<meta http-equiv='content-type' content='text/html; charset=utf-8' />
	</head>
	
<style type="text/css">
<!--
h1 {
	font-size: 20px;
	font-family: arial, "lucida console", sans-serif;
	margin: 0px;
	padding-top: 3px;
	padding-bottom: 10px;
}

#MarkTable {
	border-collapse: collapse;
	border-color: #000000;
}

#MarkTable thead {
	font-size: 12px;
	font-family: arial, "lucida console", sans-serif;
}

#MarkTable tbody {
	font-size: 12px;
	font-family: arial, "lucida console", sans-serif;
}
.footer {
	font-family: "Arial", "Lucida Console";	
	font-size: 12px;
}
.signature {
	font-family: "Arial", "Lucida Console";	
	font-size: 14px;
}
.signature {
	font-family: "Arial", "Lucida Console";	
	font-size: 14px;
}
.stat_start_border_top {
	border-top-width: 2px;
	border-top-style: solid;
	border-top-color: #000000;
}
-->
</style>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center"><h1><?= $GMSTitle ?></h1></td>
	</tr>
	<tr>
		<td><?= $markTable ?></td>
	</tr>
</table>
<br />
</html>
<?
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>
