<?php
// using : 

@SET_TIME_LIMIT(1000);
@ini_set(memory_limit, "800M");

/**********************************************
 * modification log
 *      20200512 Bill:
 *          support Print Date display  ($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayPrintReportDateTime'])
 *      20190604 Bill:  [2019-0123-1731-14066]
 *          added Total Unit(s) Failed column   ($eRCTemplateSetting['Report']['GrandMarksheet']['ShowTotalUnitFailed'])
 *          always display marks according to decimal places settings  ($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayRoundMarks'])
 *      20180628 Bill:  [2017-1204-1601-38164]
 *          set target active year  ($eRCTemplateSetting['Report']['SupportAllActiveYear'])
 *      20180326 Bill:  [2018-0129-1221-32164]
 *          fix Total Result always failed if only 1 subject is selected
 * 		20170801 Bill:	[2016-1214-1548-50240]
 * 			Support Grand Average Grade Adjustment	($eRCTemplateSetting['GrandAverageGradeManualAdjustment'])
 * 		20170717 Bill:	[2017-0706-1012-27236]
 * 			Statistics - Lowest Mark: remove -1 from Minimum Score Calculation 
 * 		20170111 Bill:	[DM#3136]
 * 			fixed PHP 5.4 error split()
 * 		20161213 Bill	[2016-1130-1208-41236]
 * 			use grade instead of mark for special case checking
 * 		20160414 Bill	[DM#2972]
 * 			set time and memory limit, prevent no response
 * 		20160128 Bill	[2016-0128-1008-07066]
 * 			support multiple Other Info data display
 * 		20150402 Bill:
 * 			add display of User Login [2015-0306-1036-36206]
 * 		20110406 Marcus:
 * 			added page break for each classes
 * 		20100707 Marcus:
 * 			modified getCorespondingTermReportID handling - change if(!empty($TermReportID)) to if($TermReportID != $ReportID) as function return value changed
 * 		20100416 Marcus:
 * 			modified ranking - if ordermeritstream exist, show it instead of ordermeritform 
 * 		20100415 Marcus:
 * 			cater multiple class selection 
 * 		20100112 Ivan:
 * 			fix wrong mark nature determination 
 * 		20100129 Marcus:
 * 			modify coding to cater conversion from grand marks to grade 
 * 		20100120 Marcus:
 * 			modify coding to cater multi selection of subjects 
 * 		20100107 Marcus:
 * 			add Display Subject to select Abbr/Desc/ShortName of Subject Name
 * 		20100105 Marcus:
 * 			add display of Gender and GPA
 * ********************************************/

// Page access right
$PageRight = "TEACHER";

// Include general libraries
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

// Authorize intranet user & connect Database
intranet_auth();
intranet_opendb();

// Check if ReportCard module is enable
if ($plugin['ReportCard2008'])
{
	// Include ReportCard libraries
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
	
	// Create objects
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	}
	else {
		$lreportcard = new libreportcard2008w();
	}
	
	// [2017-1204-1601-38164] Set target active year
	if($eRCTemplateSetting['Report']['SupportAllActiveYear'])
	{
	    $targetActiveYear = stripslashes($_REQUEST['targetActiveYear']);
	    if(!empty($targetActiveYear)){
	        $targetYearID = $lreportcard->SET_TEMP_ACTIVE_YEAR($targetActiveYear);
	    }
	}
	
	# $linterface = new interface_html();
	$lclass = new libclass();
	
	// Check module access right
	if ($lreportcard->hasAccessRight())
	{
		// Check required fields submitted from index.php
		// optional: $SubjectID, $ReportColumnID
		if (!isset($ClassLevelID, $ClassID, $ReportID) || empty($ClassLevelID) || empty($ReportID) || empty($ClassID)) {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		# Get display options
		// 1. student name display (EnglishName, ChineseName)
		$showEnglishName = false;
		$showChineseName = false;
		if ($StudentNameDisplay == NULL)
		{
			$numOfNameColumn = 0;
		}
		else
		{
			$StudentNameDisplay = explode(",", $StudentNameDisplay);
			$numOfNameColumn = sizeof($StudentNameDisplay);
			foreach ($StudentNameDisplay as $key => $value)
			{
				if ($value == "EnglishName") $showEnglishName = true;
				if ($value == "ChineseName") $showChineseName = true;
			}
		}
		// 2. ranking display (ClassRanking, OverallRanking)
		$showClassRanking = false;
		$showOverallRanking = false;
		$RankingDisplay = explode(",", $RankingDisplay);
		foreach ($RankingDisplay as $key => $value)
		{
			if ($value == "ClassRanking") $showClassRanking = true;
			if ($value == "OverallRanking") $showOverallRanking = true;
		}
    	// 3.1 show subject (0,1)
		$showSubject = $ShowSubject;		
		// 3.2 show subject component (0,1)
		$showSubjectComponent = $ShowSubjectComponent;
		// 4. show Summary Info (0,1)
		$showSummaryInfo = $ShowSummaryInfo;		
		// 5. show Grand Total (0,1)
		$showGrandTotal = $ShowGrandTotal;		
		// 6. show Grand Average (0,1)
		$showGrandAverage = $ShowGrandAverage;
		// 7. ForSSPA => no decimal place in all marks
		$ForSSPA = $ForSSPA;
		// 8. show Statistics (0,1)
		$showStatistics = $ShowStatistics;
		// 9. show GPA (0,1)
		$showGPA = $ShowGPA;
		// 10. show Gender (0,1)
		$showGender = $ShowGender;
		// 11. show User Login (0,1) [2015-0306-1036-36206]
		$showUserLogin = $ShowUserLogin;
		// 12. show Total Unit(s) Failed (0,1)
		$showTotalUnitFailed = ($_GET['ShowTotalUnitFailed'])? 1 : 0;
		
		$showGrandTotalGrade = $ShowGrandTotalGrade;
		$showGrandAverageGrade= $ShowGrandAverageGrade;
		$showGPAGrade= $ShowGPAGrade;
		$showActualAverage = ($_GET['ShowActualAverage'] && empty($ReportColumnID))? 1 : 0;
		$showActualAverageGrade = ($_GET['ShowActualAverageGrade'] && empty($ReportColumnID))? 1 : 0;
		
		$reportCalculationOrder = $lreportcard->returnReportTemplateCalculation($ReportID);
		
		// SubjectID has 2 Condition
		// 1.no of subject selected >1 , show overall rank
		// 2.no of subject selected ==1 , show rank of specific subject
		$SelectedSubjectID = $SubjectID;
		if(count($SubjectID) == 1) {
			$SubjectID = $SubjectID[0];
		}
		else {
			unset($SubjectID);
		}
        
		// Get ReportCard Info
		$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		$SemID = $reportTemplateInfo['Semester'];
		if ($SemID != "F") {
		    $SemesterTitle = $lreportcard->returnSemesters($SemID);
		    $SemesterNumber = $lreportcard->Get_Semester_Seq_Number($SemID);
		}
		else {
		    $SemesterTitle = $eReportCard['Template']['Annual'];
		}
		
		// Check to make sure it is a generated report
		if ($reportTemplateInfo["LastGenerated"] == "0000-00-00 00:00:00" || $reportTemplateInfo["LastGenerated"] == "") {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		// Get subjects of the current ClassLevel
		$FormSubjectArr = $lreportcard->returnSubjectwOrder($ClassLevelID, 1, '', $intranet_session_language, $SubjDisplay);
		
		$CmpSubjectIDList = array();
		$ParentSubjectIDList = array();
		$ParentCmpSubjectNum = array();
		
		// Reformat the subject array - Display component subjects also
		$FormSubjectArrFlat = array();
		if (sizeof($FormSubjectArr) > 0)
		{
			foreach($FormSubjectArr as $tmpSubjectID => $tmpSubjectName)
			{
				if (sizeof($tmpSubjectName) > 1)
				{
					$ParentSubjectIDList[] = $tmpSubjectID;
					$ParentCmpSubjectNum[$tmpSubjectID] = sizeof($tmpSubjectName) - 1;
					foreach ($tmpSubjectName as $tmpCmpSubjectID => $tmpCmpSubjectName) {
						if ($tmpCmpSubjectID != 0) {
							$CmpSubjectIDList[] = $tmpCmpSubjectID;
							$FormSubjectArrFlat[$tmpCmpSubjectID] = array(0 => $tmpCmpSubjectName);
						}
					}
				}
				$FormSubjectArrFlat[$tmpSubjectID] = array(0 => $tmpSubjectName[0]);
			}
		}
		$FormSubjectArr = $FormSubjectArrFlat;
		
		// Get component subjects if SubjectID is provided
		if (isset($SubjectID) && $SubjectID !== "" && in_array($SubjectID, $ParentSubjectIDList))
		{
			$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID,$intranet_session_language,$SubjDisplay);
			$CmpSubjectIDNameArr = array();
			$CmpSubjectIDArr = array();
			for($i=0; $i<sizeof($CmpSubjectArr); $i++) {
				$CmpSubjectIDArr[] = $CmpSubjectArr[$i]["SubjectID"];
				$CmpSubjectIDNameArr[$CmpSubjectArr[$i]["SubjectID"]] = $CmpSubjectArr[$i]["SubjectName"];
			}
		}
		
		// Get subjects' grading scheme of the current ClassLevel
		$FormSubjectGradingSchemeArr = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID,0,$ReportID);
		
		// Get ClassLevel name
		$ClassLevelName = $lreportcard->returnClassLevel($ClassLevelID);
		
		// Get Classes
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		
		// Get current school year
		$SchoolYear = $lreportcard->schoolYear;
		
		// Get columns info of the current ReportCard
		$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
		$ColumnIDList = array();
		$ColumnIDList = ($ColumnTitleAry == NULL)? NULL : array_keys($ColumnTitleAry);
		
		// Get the list of students (in a class or entire class level)
		$ClassName = "";
		if (!isset($ClassID) || empty($ClassID)) {
			intranet_closedb();
			header("Location: index.php");
			exit();
		} 
		
		$ClassIDArr = $ClassID;
		unset($ClassID);
	    
		foreach($ClassIDArr as $ClassID)
		{
			$GrandAvgNatureArr = array();
			$ActualAvgNatureArr = array();
			
			$studentList = $lreportcard->GET_STUDENT_BY_CLASS($ClassID, "", 1);
			// Get the Class Name
			for($i=0; $i<sizeof($ClassArr); $i++)
			{
				if ($ClassArr[$i]["ClassID"] == $ClassID) {
					$ClassName = $ClassArr[$i]["ClassName"];
					break;
				}
			}
			
			$studentIDList = array();
			$studentIDNameMap = array();
			$studentIDRegNoMap = array();
			$studentFailUnitArr = array();
			for($i=0; $i<sizeof($studentList); $i++)
			{
				$studentIDNameMap[$studentList[$i]["UserID"]]['En'] = $studentList[$i]['StudentNameEn'];
				$studentIDNameMap[$studentList[$i]["UserID"]]['Ch'] = $studentList[$i]['StudentNameCh'];
				
				$studentIDRegNoMap[$studentList[$i]["UserID"]] = $studentList[$i]["WebSAMSRegNo"];
				if ($studentList[$i][2] != "") {
					$studentIDList[$studentList[$i][2]] = $studentList[$i]["UserID"];
				}
				else {
					$studentIDList[] = $studentList[$i]["UserID"];
				}
				
				// [2019-0123-1731-14066] Get Student Total Unit(s) Failed
				if($eRCTemplateSetting['Report']['GrandMarksheet']['ShowTotalUnitFailed'] && $showTotalUnitFailed) {
				    $studentFailUnitInfo = $lreportcard->returnTemplateSubjectCol($ReportID, $ClassLevelID, $studentList[$i]["UserID"]);
				    $studentFailUnitInfo = $studentFailUnitInfo['FailUnitArr'];
				    $studentFailUnitArr[$studentList[$i]["UserID"]] = $SemesterNumber > 0? $studentFailUnitInfo[$SemesterNumber - 1] : $studentFailUnitInfo['Final'];
				}
			}
			
			// Students should be already sorted by Class Number in the SQL Query
			#asort($studentIDList);
			$studentIDListSql = implode(",", array_values($studentIDList));
			$cond = "";
			
			// Get overall mark or term/assessment mark
			$ColumnTitle = "";
			if (!isset($ReportColumnID) || empty($ReportColumnID)) {
				$ColumnTitle = $eReportCard['Template']['OverallCombined'];
				$cond .= "AND (a.ReportColumnID = '' OR a.ReportColumnID = '0') ";
				$reportColumnID = 0;
			}
			else {
				$ColumnTitle = $ColumnTitleAry[$ReportColumnID];
				$cond .= "AND a.ReportColumnID = '$ReportColumnID'";
				$reportColumnID = $ReportColumnID;
			}
			
			// Get individual subject mark or grand average (which table to retrieve)
			$SubjectName = "";
			if (!isset($SubjectID) || empty($SubjectID))
			{
				// No SubjectID provided, get the average result
				$table = $lreportcard->DBName.".RC_REPORT_RESULT as a";
				$fieldToSelect = "StudentID, GrandTotal, GrandAverage, ActualAverage, GPA, OrderMeritClass, IF(OrderMeritStream IS NULL OR TRIM(OrderMeritStream) = '',OrderMeritForm,OrderMeritStream) AS OrderMeritForm";
				
				$sql = "SELECT $fieldToSelect FROM $table WHERE ";
				$sql .= "ReportID = '$ReportID' ";
				$sql .= "AND StudentID IN ($studentIDListSql) ";
				$sql .= $cond;
				$tempArr = $lreportcard->returnArray($sql);
				
				$resultAverage = array();
				if (sizeof($tempArr) > 0)
				{
					for($j=0; $j<sizeof($tempArr); $j++)
					{
						# for manual adjustment
						$ManualAjustedMark = $lreportcard->Get_Manual_Adjustment($ReportID, $tempArr[$j]["StudentID"], $ReportColumnID, 0);
						$thisGrandMark = $ManualAjustedMark[$tempArr[$j]["StudentID"]][$reportColumnID][0];
						
						$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = $thisGrandMark["GrandTotal"]?$thisGrandMark["GrandTotal"]:$tempArr[$j]["GrandTotal"];
						$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = $thisGrandMark["GrandAverage"]?$thisGrandMark["GrandAverage"]:$tempArr[$j]["GrandAverage"];
						$resultAverage[$tempArr[$j]["StudentID"]]["ActualAverage"] = $thisGrandMark["ActualAverage"]?$thisGrandMark["ActualAverage"]:$tempArr[$j]["ActualAverage"];
						$resultAverage[$tempArr[$j]["StudentID"]]["GPA"] = $thisGrandMark["GPA"]?$thisGrandMark["GPA"]:$tempArr[$j]["GPA"];
						$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverageGrade"] = $thisGrandMark["GrandAverageGrade"]?$thisGrandMark["GrandAverageGrade"]:"";
						//$resultAverage[$tempArr[$j]["StudentID"]]["SDScore"] = $thisGrandMark["SDScore"]?$thisGrandMark["SDScore"]:$tempArr[$j]["SDScore"];
						
						# For Term Report Average Grade Adjustment
						if($eRCTemplateSetting['GrandAverageGradeManualAdjustment'] && $SemID == "F" && $ReportColumnID > 0)
						{
							$TermReportID = $lreportcard->getCorespondingTermReportID($ReportID, $ReportColumnID);
							if(!empty($TermReportID) && $TermReportID != $ReportID) {
								$TermManualAjustedMark = $lreportcard->Get_Manual_Adjustment($TermReportID, $tempArr[$j]["StudentID"], 0, 0);
								$thisTermGrandMark = $TermManualAjustedMark[$tempArr[$j]["StudentID"]][0][0];
								$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverageGrade"] = $thisTermGrandMark["GrandAverageGrade"]?$thisTermGrandMark["GrandAverageGrade"]:$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverageGrade"];
							}
						}
						
						# Round up all the marks if for SSPA
						if ($ForSSPA)
						{
							$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = round($thisGrandMark["GrandTotal"]?$thisGrandMark["GrandTotal"]:$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"]);
							$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = round($thisGrandMark["GrandAverage"]?$thisGrandMark["GrandAverage"]:$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"]);
							$resultAverage[$tempArr[$j]["StudentID"]]["ActualAverage"] = round($thisGrandMark["ActualAverage"]?$thisGrandMark["ActualAverage"]:$resultAverage[$tempArr[$j]["StudentID"]]["ActualAverage"]);
							$resultAverage[$tempArr[$j]["StudentID"]]["GPA"] = round($thisGrandMark["GPA"]?$thisGrandMark["GPA"]:$resultAverage[$tempArr[$j]["StudentID"]]["GPA"]);
						}
						
						// [2019-0123-1731-14066] add rounding logic
						if($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayRoundMarks'])
						{
						    if(is_numeric($resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"]) && $resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] != -1) {
                                $resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = $lreportcard->ROUND_MARK($resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"], "GrandTotal");
						    }
						    if(is_numeric($resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"]) && $resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] != -1) {
						        $resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = $lreportcard->ROUND_MARK($resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"], "GrandAverage");
						    }
						    if(is_numeric($resultAverage[$tempArr[$j]["StudentID"]]["GPA"]) && $resultAverage[$tempArr[$j]["StudentID"]]["GPA"] != -1) {
						        $resultAverage[$tempArr[$j]["StudentID"]]["GPA"] = $lreportcard->ROUND_MARK($resultAverage[$tempArr[$j]["StudentID"]]["GPA"], "GrandAverage");
						    }
						}
						
						# SubjectID = -1 => GrandAverage
						//$GrandAvgNatureArr[] = $lreportcard->returnMarkNature($ClassLevelID,-1,$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"]);
						if ($tempArr[$j]["OrderMeritClass"] > 0 || ($thisGrandMark["OrderMeritClass"])) {
							$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritClass"] = $thisGrandMark["OrderMeritClass"]?$thisGrandMark["OrderMeritClass"]:$tempArr[$j]["OrderMeritClass"];
						}
						else {
							$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritClass"] = "-";
						}
						if ($tempArr[$j]["OrderMeritForm"] > 0 || ($thisGrandMark["OrderMeritForm"])) {
							$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritForm"] = $thisGrandMark["OrderMeritForm"]?$thisGrandMark["OrderMeritForm"]:$tempArr[$j]["OrderMeritForm"];
						}
						else {
							$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritForm"] = "-";
						}
					}
				}
				
				foreach($FormSubjectArr as $tmpSubjectID => $tmpSubjectName)
				{
					$subjectGradingScheme = $FormSubjectGradingSchemeArr[$tmpSubjectID];
					$subjectGradingSchemeInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($subjectGradingScheme["schemeID"]);
					
					$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID,0,0,$ReportID);
					$SchemeID = $SubjectFormGradingSettings['SchemeID'];
					$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
					$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
					
    				/*	if ($subjectGradingScheme["scaleDisplay"] == "M") {
    						$fieldToSelect = "StudentID, Mark As MarkUsed, Grade";
    					} else {
    						$fieldToSelect = "StudentID, Grade As MarkUsed, Grade";
    					}
    					
    					$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE";
    					
    					$sql = "SELECT $fieldToSelect FROM $table WHERE ";
    					$sql .= "ReportID = '$ReportID' ";
    					$sql .= "AND StudentID IN ($studentIDListSql) ";
    					$sql .= "AND  SubjectID = '$tmpSubjectID' ";
    					$sql .= $cond;
    					$tempArr = $lreportcard->returnArray($sql);
    				*/
    				
    				# Check if term report exists, if so, set reportID, ReportColumnID to retrieve term report data
    				$TermReportID = $lreportcard->getCorespondingTermReportID($ReportID, $ReportColumnID);
    				if($TermReportID != $ReportID && !empty($TermReportID))
    				{
    					$thisReportID = $TermReportID;
    					$thiscond = " AND (a.ReportColumnID = '' OR a.ReportColumnID = '0') ";
    				}
    				else
    				{
    					$thisReportID = $ReportID;
    					$thiscond = $cond;
    				}
                    
    				# Modified for Manual Adjustment
    				if ($subjectGradingScheme["scaleDisplay"] == "M") {
    					$fieldToSelect = "a.StudentID, a.OrderMeritClass, IF(a.OrderMeritStream IS NULL OR TRIM(a.OrderMeritStream) = '',a.OrderMeritForm,a.OrderMeritStream) AS OrderMeritForm, IFNULL(b.AdjustedValue,a.Mark) As MarkUsed, IFNULL(b.AdjustedValue,a.Grade)as Grade";
    				}
    				else {
    					$fieldToSelect = "a.StudentID, a.OrderMeritClass, IF(a.OrderMeritStream IS NULL OR TRIM(a.OrderMeritStream) = '',a.OrderMeritForm,a.OrderMeritStream) AS OrderMeritForm, IFNULL(b.AdjustedValue,a.Grade) As MarkUsed, IFNULL(b.AdjustedValue,a.Grade)as Grade";
    				}
    				$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE a LEFT JOIN ".$lreportcard->DBName.".RC_MANUAL_ADJUSTMENT b ON a.ReportID = b.ReportID AND a.StudentID = b.StudentID AND a.SubjectID = b.SubjectID AND a.ReportColumnID = b.ReportColumnID AND b.OtherInfoName = 'Score'";
    				$sql = " 
    					SELECT
    						$fieldToSelect
    					FROM
    						$table
    					WHERE
    						a.ReportID = '$thisReportID'
    						AND a.StudentID IN ($studentIDListSql)
    						AND a.SubjectID = '$tmpSubjectID'
    						$thiscond ";
					$tempArr = $lreportcard->returnArray($sql);
					
					$resultSubject[$tmpSubjectID] = array();
					$resultSubjectGrade[$tmpSubjectID] = array();
					if (sizeof($tempArr) > 0)
					{
						for($j=0; $j<sizeof($tempArr); $j++)
						{
							$thisMark = $tempArr[$j]["MarkUsed"];
							$thisGrade = $tempArr[$j]["Grade"];
							list($thisMark, $needStyle) = $lreportcard->checkSpCase($ReportID, $tmpSubjectID, $thisMark, $thisGrade);
							
							$resultSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = $thisMark;
							$resultSubjectGrade[$tmpSubjectID][$tempArr[$j]["StudentID"]] = $thisGrade;
													
							# Round up all the marks if for SSPA
							if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M" && is_numeric($thisMark))
							{
								$resultSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = round($resultSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]]);
							}
						}
					}
				}
			}
			else
			{
				# Specific Subject
				$subjectGradingScheme = $FormSubjectGradingSchemeArr[$SubjectID];
				$subjectGradingSchemeInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($subjectGradingScheme["schemeID"]);
				
				$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 0, 0, $ReportID);
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				
				/*if ($subjectGradingScheme["scaleDisplay"] == "M") {
					$fieldToSelect = "StudentID, OrderMeritClass, OrderMeritForm, Mark As MarkUsed, Grade";
				} else {
					$fieldToSelect = "StudentID, OrderMeritClass, OrderMeritForm, Grade As MarkUsed, Grade";
				}
				$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE a";
				
				$sql = "SELECT $fieldToSelect FROM $table WHERE ";
				$sql .= "ReportID = '$ReportID' ";
				$sql .= "AND StudentID IN ($studentIDListSql) ";
				$sql .= "AND SubjectID = '$SubjectID' ";
				*/
				
				# Modified for Manual Adjustment
				if ($subjectGradingScheme["scaleDisplay"] == "M") {
					$fieldToSelect = "a.StudentID, a.OrderMeritClass, IF(a.OrderMeritStream IS NULL OR TRIM(a.OrderMeritStream) = '',a.OrderMeritForm,a.OrderMeritStream) AS OrderMeritForm, IFNULL(b.AdjustedValue,a.Mark) As MarkUsed, IFNULL(b.AdjustedValue,a.Grade)as Grade";
				}
				else {
					$fieldToSelect = "a.StudentID, a.OrderMeritClass, IF(a.OrderMeritStream IS NULL OR TRIM(a.OrderMeritStream) = '',a.OrderMeritForm,a.OrderMeritStream) AS OrderMeritForm, IFNULL(b.AdjustedValue,a.Grade) As MarkUsed, IFNULL(b.AdjustedValue,a.Grade)as Grade";
				}
				$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE a LEFT JOIN ".$lreportcard->DBName.".RC_MANUAL_ADJUSTMENT b ON a.ReportID = b.ReportID AND a.StudentID = b.StudentID AND a.SubjectID = b.SubjectID AND a.ReportColumnID = b.ReportColumnID AND b.OtherInfoName = 'Score'";
				$sql = " 
					SELECT 
						$fieldToSelect
					FROM 
						$table 
					WHERE 
						a.ReportID = '$ReportID' 
						AND a.StudentID IN ($studentIDListSql) 
						AND a.SubjectID = '$SubjectID' 
						$cond	
					";
				$tempArr = $lreportcard->returnArray($sql);
				//$tempArr = $lreportcard->returnArray($sql.$cond);
				
				$resultSubject = array();
				$resultSubjectGrade = array();
				$OrderClassSubject = array();
				if (sizeof($tempArr) > 0)
				{
					for($j=0; $j<sizeof($tempArr); $j++)
					{
						# for manual adjustment
						$ManualAjustedMark = $lreportcard->Get_Manual_Adjustment($ReportID, $tempArr[$j]["StudentID"], $ReportColumnID, $SubjectID);
						$thisSubjectOrder = $ManualAjustedMark[$tempArr[$j]["StudentID"]][$reportColumnID][$SubjectID];
						
						$thisMark = $tempArr[$j]["MarkUsed"];
						$thisGrade = $tempArr[$j]["Grade"];
						list($thisMark, $needStyle) = $lreportcard->checkSpCase($ReportID, $SubjectID, $thisMark, $thisGrade);
						
						$resultSubject[$tempArr[$j]["StudentID"]] = $thisMark;
						$resultSubjectGrade[$tempArr[$j]["StudentID"]] = $thisGrade;
											
						# Round up all the marks if for SSPA
						if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M" && is_numeric($thisMark))
						{
							$resultSubject[$tempArr[$j]["StudentID"]] = round($resultSubject[$tempArr[$j]["StudentID"]]);
						}
						
						if ($tempArr[$j]["OrderMeritClass"] > 0 ||$thisSubjectOrder["OrderMeritClass"]) {
							$OrderClassSubject[$tempArr[$j]["StudentID"]] = $thisSubjectOrder["OrderMeritClass"]?$thisSubjectOrder["OrderMeritClass"]:$tempArr[$j]["OrderMeritClass"];
						}
						else {
							$OrderClassSubject[$tempArr[$j]["StudentID"]] = "-";
						}
						
						if ($tempArr[$j]["OrderMeritForm"] > 0 || $thisSubjectOrder["OrderMeritForm"]) {
							$OrderFormSubject[$tempArr[$j]["StudentID"]] = $thisSubjectOrder["OrderMeritForm"]?$thisSubjectOrder["OrderMeritForm"]:$tempArr[$j]["OrderMeritForm"];
						}
						else {
							$OrderFormSubject[$tempArr[$j]["StudentID"]] = "-";
						}
					}
				}
				
				// If gettng subject overall mark for a single subject, get the column mark also
				if (!isset($ReportColumnID) || empty($ReportColumnID))
				{
					for($i=0; $i<sizeof($ColumnIDList); $i++)
					{
						$tmpCond = "AND ReportColumnID = '".$ColumnIDList[$i]."'";
						$tempArr = $lreportcard->returnArray($sql.$tmpCond);
						$resultSubjectColumn[$ColumnIDList[$i]] = array();
						if (sizeof($tempArr) > 0)
						{
							for($j=0; $j<sizeof($tempArr); $j++)
							{
								$thisMark = $tempArr[$j]["MarkUsed"];
								$thisGrade = $tempArr[$j]["Grade"];
								list($thisMark, $needStyle) = $lreportcard->checkSpCase($ReportID, $SubjectID, $thisMark, $thisGrade);
						
								$resultSubjectColumn[$ColumnIDList[$i]][$tempArr[$j]["StudentID"]] = $thisMark;
								
								# Round up all the marks if for SSPA
								if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M" && is_numeric($thisMark))
								{
									$resultSubjectColumn[$ColumnIDList[$i]][$tempArr[$j]["StudentID"]] = round($resultSubjectColumn[$ColumnIDList[$i]][$tempArr[$j]["StudentID"]]);
								}
							}
						}
					}
				}
				
				#################################
				// Special handling of subject which having component subjects
				if (in_array($SubjectID, $ParentSubjectIDList))
				{
					// Push the SubjectID into the same array of its component subjects
					$SubjectPlusCompSubjectIDList = $CmpSubjectIDArr;
					$SubjectPlusCompSubjectIDList[] = $SubjectID;
					
					/*
					// If no ReportColumnID is provided, beside showing the subject overall result, show all column result too
					if (!isset($ReportColumnID) || empty($ReportColumnID)) {
						for($i=0; $i<sizeof($ColumnIDList); $i++) {
						 	$tmpCond1 = "AND ReportColumnID = '".$ColumnIDList[$i]."'";
							for($j=0; $j<sizeof($SubjectPlusCompSubjectIDList); $j++) {
								$tmpSubjectID = $SubjectPlusCompSubjectIDList[$j];
								$subjectGradingScheme = $FormSubjectGradingSchemeArr[$tmpSubjectID];
								$subjectGradingSchemeInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($subjectGradingScheme["schemeID"]);
								
								if ($subjectGradingScheme["scaleDisplay"] == "M") {
									$fieldToSelect = "StudentID, OrderMeritClass, Mark As MarkUsed";
								} else {
									$fieldToSelect = "StudentID, OrderMeritClass, Grade As MarkUsed";
								}
								$sql = "SELECT $fieldToSelect FROM $table WHERE ";
								$sql .= "ReportID = '$ReportID' ";
								$sql .= "AND StudentID IN ($studentIDListSql) ";
								
								$tmpCond2 = "AND SubjectID = '$tmpSubjectID' ";
								$tempArr = $lreportcard->returnArray($sql.$tmpCond1.$tmpCond2);
								if (sizeof($tempArr) > 0) {
									for($k=0; $k<sizeof($tempArr); $k++) {
										$resultSubjectColumnDetail[$ColumnIDList[$i]][$tmpSubjectID][$tempArr[$k]["StudentID"]] = $tempArr[$k]["MarkUsed"];
									}
								}
							}
						}
					}
					*/
					
					for($i=0; $i<sizeof($SubjectPlusCompSubjectIDList); $i++)
					{
						$tmpSubjectID = $SubjectPlusCompSubjectIDList[$i];
						$subjectGradingScheme = $FormSubjectGradingSchemeArr[$tmpSubjectID];
						$subjectGradingSchemeInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($subjectGradingScheme["schemeID"]);
						
						/*if ($subjectGradingScheme["scaleDisplay"] == "M") {
							$fieldToSelect = "StudentID, Mark As MarkUsed, Grade";
						} else {
							$fieldToSelect = "StudentID, Grade As MarkUsed, Grade";
						}
						$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE a";
						
						$sql = "SELECT $fieldToSelect FROM $table WHERE ";
						$sql .= "ReportID = '$ReportID' ";
						$sql .= "AND StudentID IN ($studentIDListSql) ";
						$sql .= "AND  SubjectID = '$tmpSubjectID' ";
						$sql .= $cond;
						$tempArr = $lreportcard->returnArray($sql);
						*/
						
						# Modified for Manual Adjustment
						if ($subjectGradingScheme["scaleDisplay"] == "M") {
							$fieldToSelect = "a.StudentID, IFNULL(b.AdjustedValue,a.Mark) As MarkUsed, IFNULL(b.AdjustedValue,a.Grade)as Grade";
						}
						else {
							$fieldToSelect = "a.StudentID, IFNULL(b.AdjustedValue,a.Grade) As MarkUsed, IFNULL(b.AdjustedValue,a.Grade)as Grade";
						}
						$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE a LEFT JOIN ".$lreportcard->DBName.".RC_MANUAL_ADJUSTMENT b ON a.ReportID = b.ReportID AND a.StudentID = b.StudentID AND a.SubjectID = b.SubjectID AND a.ReportColumnID = b.ReportColumnID AND b.OtherInfoName = 'Score'";
						$sql = " 
							SELECT 
								$fieldToSelect
							FROM 
								$table 
							WHERE 
								a.ReportID = '$ReportID' 
								AND a.StudentID IN ($studentIDListSql) 
								AND a.SubjectID = '$tmpSubjectID' 
								$cond ";
						$tempArr = $lreportcard->returnArray($sql);
						
						$resultDetailSubject[$tmpSubjectID] = array();
						$resultDetailSubjectGrade[$tmpSubjectID] = array();
						if (sizeof($tempArr) > 0)
						{
							for($j=0; $j<sizeof($tempArr); $j++)
							{
								$thisMark = $tempArr[$j]["MarkUsed"];
								$thisGrade = $tempArr[$j]["Grade"];
								list($thisMark, $needStyle) = $lreportcard->checkSpCase($ReportID, $tmpSubjectID, $thisMark, $thisGrade);
						
								$resultDetailSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = $thisMark;
								$resultDetailSubjectGrade[$tmpSubjectID][$tempArr[$j]["StudentID"]] = $thisGrade;
								
								# Round up all the marks if for SSPA
								if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M" && is_numeric($thisMark))
								{
									$resultDetailSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = round($resultDetailSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]]);
								}
							}
						}
					}
				}
				
				#################################
				# get Grand Total and Grand Average
				$table = $lreportcard->DBName.".RC_REPORT_RESULT a";
				$fieldToSelect = "StudentID, GrandTotal, GrandAverage, ActualAverage, GPA, OrderMeritClass, IF(OrderMeritStream IS NULL OR TRIM(OrderMeritStream) = '',OrderMeritForm,OrderMeritStream) AS OrderMeritForm";
				
				$sql = "SELECT $fieldToSelect FROM $table WHERE ";
				$sql .= "ReportID = '$ReportID' ";
				$sql .= "AND StudentID IN ($studentIDListSql) ";
				$sql .= $cond;
				$tempArr = $lreportcard->returnArray($sql);
				
				$resultAverage = array();
				if (sizeof($tempArr) > 0)
				{
					for($j=0; $j<sizeof($tempArr); $j++)
					{
						# for manual adjustment
						$ManualAjustedMark = $lreportcard->Get_Manual_Adjustment($ReportID, $tempArr[$j]["StudentID"], $ReportColumnID);
						$thisGrandMark = $ManualAjustedMark[$tempArr[$j]["StudentID"]][$reportColumnID][0];
	
						$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = $thisGrandMark["GrandTotal"]?$thisGrandMark["GrandTotal"]:$tempArr[$j]["GrandTotal"];
						$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = sprintf("%02.2f", $thisGrandMark["GrandAverage"]?$thisGrandMark["GrandAverage"]:$tempArr[$j]["GrandAverage"]);
						$resultAverage[$tempArr[$j]["StudentID"]]["ActualAverage"] = sprintf("%02.2f", $thisGrandMark["ActualAverage"]?$thisGrandMark["ActualAverage"]:$tempArr[$j]["ActualAverage"]);
						$resultAverage[$tempArr[$j]["StudentID"]]["GPA"] = sprintf("%02.2f", $thisGrandMark["GPA"]?$thisGrandMark["GPA"]:$tempArr[$j]["GPA"]);
						$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverageGrade"] = $thisGrandMark["GrandAverageGrade"]?$thisGrandMark["GrandAverageGrade"]:"";
						//$resultAverage[$tempArr[$j]["StudentID"]]["SDScore"] = $thisGrandMark["SDScore"]?$thisGrandMark["SDScore"]:$tempArr[$j]["SDScore"];
						
						# For Term Report Average Grade Adjustment
						if($eRCTemplateSetting['GrandAverageGradeManualAdjustment'] && $SemID == "F" && $ReportColumnID > 0)
						{
							$TermReportID = $lreportcard->getCorespondingTermReportID($ReportID, $ReportColumnID);
							if(!empty($TermReportID) && $TermReportID != $ReportID) {
								$TermManualAjustedMark = $lreportcard->Get_Manual_Adjustment($TermReportID, $tempArr[$j]["StudentID"], 0, 0);
								$thisTermGrandMark = $TermManualAjustedMark[$tempArr[$j]["StudentID"]][0][0];
								$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverageGrade"] = $thisTermGrandMark["GrandAverageGrade"]?$thisTermGrandMark["GrandAverageGrade"]:$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverageGrade"];
							}
						}
						
						# Round up all the marks if for SSPA
						if ($ForSSPA)
						{
							$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = round($thisGrandMark["GrandTotal"]?$thisGrandMark["GrandTotal"]:$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"]);
							$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = round($thisGrandMark["GrandAverage"]?$thisGrandMark["GrandAverage"]:$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"]);
							$resultAverage[$tempArr[$j]["StudentID"]]["ActualAverage"] = round($thisGrandMark["ActualAverage"]?$thisGrandMark["ActualAverage"]:$resultAverage[$tempArr[$j]["StudentID"]]["ActualAverage"]);
							$resultAverage[$tempArr[$j]["StudentID"]]["GPA"] = round($thisGrandMark["GPA"]?$thisGrandMark["GPA"]:$resultAverage[$tempArr[$j]["StudentID"]]["GPA"]);
						}
						
						// [2019-0123-1731-14066] add rounding logic
						if($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayRoundMarks'])
						{
						    if(is_numeric($resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"]) && $resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] != -1) {
                                $resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = $lreportcard->ROUND_MARK($resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"], "GrandTotal");
						    }
						    if(is_numeric($resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"]) && $resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] != -1) {
						        $resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = $lreportcard->ROUND_MARK($resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"], "GrandAverage");
						    }
						    if(is_numeric($resultAverage[$tempArr[$j]["StudentID"]]["GPA"]) && $resultAverage[$tempArr[$j]["StudentID"]]["GPA"] != -1) {
						        $resultAverage[$tempArr[$j]["StudentID"]]["GPA"] = $lreportcard->ROUND_MARK($resultAverage[$tempArr[$j]["StudentID"]]["GPA"], "GrandAverage");
						    }
						}
						
						# SubjectID = -1 => GrandAverage
						//$GrandAvgNatureArr[] = $lreportcard->returnMarkNature($ClassLevelID,-1,$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"]);
						if ($tempArr[$j]["OrderMeritClass"] > 0 ||$thisGrandMark["OrderMeritClass"]) {
							$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritClass"] = $thisGrandMark["OrderMeritClass"]?$thisGrandMark["OrderMeritClass"]:$tempArr[$j]["OrderMeritClass"];
						}
						else {
							$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritClass"] = "-";
						}
						if ($tempArr[$j]["OrderMeritForm"] > 0 || $thisGrandMark["OrderMeritForm"]) {
							$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritForm"] = $thisGrandMark["OrderMeritForm"]?$thisGrandMark["OrderMeritForm"]:$tempArr[$j]["OrderMeritForm"];
						}
						else {
							$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritForm"] = "-";
						}
					}
				}
			}
			
			// Get Summary Info from CSV
			# build data array
			$SummaryInfoFields = $lreportcard->OtherInfoInGrandMS?$lreportcard->OtherInfoInGrandMS:array("Conduct");
//			$ary = array();
//			$csvType = $lreportcard->getOtherInfoType();
//	
//			if($reportTemplateInfo['Semester']=="F") {
//				$InfoTermID = 0;
//			} else {
//				$InfoTermID = $reportTemplateInfo['Semester'];
//			}
//			if(!empty($csvType)) {
//				foreach($csvType as $k=>$Type) {
//					$csvData = $lreportcard->getOtherInfoData($Type, $InfoTermID, $ClassID);	
//					
//					if(!empty($csvData)) {
//						foreach($csvData as $thisStudentID=>$data) {
//							foreach($data as $key=>$val) {
//								if (!is_numeric($key) && trim($key) !== "")
//									$ary[$thisStudentID][$key] = $val;
//							}
//						}
//					}
//				}
//			}
//			
//			if ($ReportCardCustomSchoolName == 'munsang_college')
//			{
//				
//				foreach ($studentIDList as $thisStudentID)
//				{
//					$eDisDataArr = $lreportcard->Get_eDiscipline_Data($InfoTermID, $thisStudentID);
//					$ary[$thisStudentID]['Conduct'] = $eDisDataArr['Conduct'];
//				}
////				foreach ($studentIDRegNoMap as $thisStudentID => $thisWebSAMS)
////				{
////					$eDisDataArr = $lreportcard->Get_eDiscipline_Data($InfoTermID, $thisStudentID);
////					$ary[$thisWebSAMS]['Conduct'] = $eDisDataArr['Conduct'];
////				}
//			}
            
			$OtherInfoArr = $lreportcard->getReportOtherInfoData($ReportID);
			if($reportTemplateInfo['Semester']=="F") {
				$InfoTermID = 0;
			}
			else {
				$InfoTermID = $reportTemplateInfo['Semester'];
			}
			
			$ary = array();
			foreach((array)$OtherInfoArr as $thisStudentID => $thisStudentOtherInfoArr)
			{
				$ary[$thisStudentID] = $thisStudentOtherInfoArr[$InfoTermID];
			}
	       
			# initialize variables to calculate Average, S.D, Variance, Max, Min, Passing Rate
			// $columnSubjectIDMapArr[#column] = SubjectID;  
			$columnSubjectIDMapArr = array();	// For getting the pasing mark from the grading scheme
			// $columnScaleOutputMapArr[#column] = "M" or "G"
			$columnScaleOutputMapArr = array();
			
			$markTable = "<table id='MarkTable' width='100%' border='1' cellpadding='2' cellspacing='0'>";
			$export_content="";
			
			// Table header
			$cmpNameRow = "";
			$numOfNameRow = ($showSubjectComponent)&&empty($SubjectID)? "2" : "";
			$markTable .= "<thead>";
			$markTable .= "<tr>";
			if (!isset($SubjectID) || empty($SubjectID))
			{
				$markTable .= "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['StudentInfo']['ClassNo']."</th>";
				$export_content .= $eReportCard['Template']['StudentInfo']['ClassNo']."\t";
				if ($numOfNameColumn > 0)
				{
					$markTable .= "<th rowspan='$numOfNameRow' colspan='$numOfNameColumn'>".$eReportCard['Template']['StudentInfo']['Name']."</th>";
					$export_content .= $eReportCard['Template']['StudentInfo']['Name']."\t";
				}
				// [2015-0306-1036-36206]
				if($showUserLogin) {
					$markTable .= "<th rowspan='$numOfNameRow' >".$eReportCard['Template']['StudentInfo']['UserLogin']."</th>";
				}
				if($showGender) {
					$markTable .= "<th rowspan='$numOfNameRow' >".$eReportCard['Template']['StudentInfo']['Gender']."</th>";
				}
				//$markTable .= "<th rowspan='2'>".$eReportCard['Template']['StudentInfo']['Name']."</th>";
				
				if (!isset($ReportColumnID) || empty($ReportColumnID))
				{
				    if($showSubject)
				    {
	  				foreach($FormSubjectArr as $tmpSubjectID => $tmpSubjectName)
	  				{
	  					//skip non selected subject and its component
	  					if (in_array($tmpSubjectID, $CmpSubjectIDList)) {
	  						$ck_subject_id = $lreportcard->GET_PARENT_SUBJECT_ID($tmpSubjectID);
	  					}
	  					else {
	  						$ck_subject_id = $tmpSubjectID;
	  					}
	  					if(!in_array($ck_subject_id, $SelectedSubjectID))	continue;
	  					
	  					if ($showSubjectComponent)
	  					{
	  						if (in_array($tmpSubjectID, $ParentSubjectIDList)) {
	  							$markTable .= "<th colspan='".($ParentCmpSubjectNum[$tmpSubjectID]+1)."'>".$tmpSubjectName[0]."</th>";
	  							$cmpNameRow .= "<th>".$eReportCard['Template']['Total']."</th>";
//	  							$columnSubjectIDMapArr[] = $tmpSubjectID;
//	  							$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
//	  							$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
	  						}
	  						else if (in_array($tmpSubjectID, $CmpSubjectIDList)) {
	  							$cmpNameRow .= "<th>".$tmpSubjectName[0]."</th>";
//	  							$columnSubjectIDMapArr[] = $tmpSubjectID;
//	  							$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
//	  							$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
	  						}
	  						else {
	  							$markTable .= "<th rowspan='2'>".$tmpSubjectName[0]."</th>";
//	  							$columnSubjectIDMapArr[] = $tmpSubjectID;
//	  							$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
//	  							$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
	  						}
	  						
	  						$columnSubjectIDMapArr[] = $tmpSubjectID;
  							$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID,0 ,0 ,$ReportID);
  							$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
	  					}
	  					else
	  					{
	  						# Hide component subjects
	  						if (in_array($tmpSubjectID, $ParentSubjectIDList)) {
	  							$markTable .= "<th>".$tmpSubjectName[0]."</th>";
	  							$columnSubjectIDMapArr[] = $tmpSubjectID;
	  							
	  							$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID,0 ,0 ,$ReportID);
	  							$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
	  						}
	  						else if (!in_array($tmpSubjectID, $CmpSubjectIDList)) {
	  							$markTable .= "<th rowspan='$numOfNameRow'>".$tmpSubjectName[0]."</th>";
	  							$columnSubjectIDMapArr[] = $tmpSubjectID;
	  							
	  							$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID,0 ,0 ,$ReportID);
	  							$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
	  						}
	  					}
	  				}
	  				
	  				//$markTable .= "<th rowspan='2'>".$eReportCard['Template']['GrandTotal']."</th>";
	  				//$markTable .= "<th rowspan='2'>".$eReportCard['Template']['GrandAverage']."</th>";
	  				//$markTable .= "<th rowspan='2'>".$eReportCard['Template']['Ranking']."</th>";
	  				$SubjectName = $eReportCard['Template']['AverageMark'];
				    }	
				} 
				else 
				{
					if($showSubject)
					{
	          		# Specific Term
	  				foreach($FormSubjectArr as $tmpSubjectID => $tmpSubjectName)
	  				{
	  					// skip non selected subject and its component
	  					if (in_array($tmpSubjectID, $CmpSubjectIDList)) {
	  						$ck_subject_id = $lreportcard->GET_PARENT_SUBJECT_ID($tmpSubjectID);
	  					}
	  					else {
	  						$ck_subject_id = $tmpSubjectID;
	  					}
	  					if(!in_array($ck_subject_id, $SelectedSubjectID))	continue;
	  					
	  					if ($showSubjectComponent)
	  					{
	  						if (in_array($tmpSubjectID, $ParentSubjectIDList)) {
	  							$markTable .= "<th colspan='".($ParentCmpSubjectNum[$tmpSubjectID]+1)."'>".$tmpSubjectName[0]."</th>";
	  							$cmpNameRow .= "<th>".$eReportCard['Template']['Total']."</th>";
//	  							$columnSubjectIDMapArr[] = $tmpSubjectID;
//	  							
//	  							$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
//	  							$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
	  						}
	  						else if (in_array($tmpSubjectID, $CmpSubjectIDList)) {
	  							$cmpNameRow .= "<th>".$tmpSubjectName[0]."</th>";
//	  							$columnSubjectIDMapArr[] = $tmpSubjectID;
//	  							
//	  							$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
//	  							$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
	  						}
	  						else {
	  							$markTable .= "<th rowspan='2'>".$tmpSubjectName[0]."</th>";
//	  							$columnSubjectIDMapArr[] = $tmpSubjectID;
//	  							
//	  							$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
//	  							$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
	  						}
	  						$columnSubjectIDMapArr[] = $tmpSubjectID;
  							$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID,0 ,0 ,$ReportID);
  							$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
	  					}
	  					else
	  					{
	  						# Hide component subjects
	  						if (in_array($tmpSubjectID, $ParentSubjectIDList)) {
	  							$markTable .= "<th>".$tmpSubjectName[0]."</th>";
	  							$columnSubjectIDMapArr[] = $tmpSubjectID;
	  							
	  							$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID,0 ,0 ,$ReportID);
	  							$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
	  						}
	  						else if (!in_array($tmpSubjectID, $CmpSubjectIDList)) 
	  						{
	  							$markTable .= "<th rowspan='2'>".$tmpSubjectName[0]."</th>";
	  							$columnSubjectIDMapArr[] = $tmpSubjectID;
	  							
	  							$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID,0 ,0 ,$ReportID);
	  							$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
	  						}
	  					}
	  				}
	  				
	  				if ($reportCalculationOrder == 2) {
	  					//$markTable .= "<th rowspan='2'>".$eReportCard['Template']['Total']."</th>";
	  					//$markTable .= "<th rowspan='2'>".$eReportCard['Template']['GrandAverage']."</th>";
	  					//$markTable .= "<th rowspan='2'>".$eReportCard['Template']['Ranking']."</th>";
	  					$SubjectName = $eReportCard['Template']['AverageMark'];
	  				}
					}
				}
			}
			else
			{
				# Specific Subject
				$markTable .= "<th>".$eReportCard['Template']['StudentInfo']['ClassNo']."</th>";
				if ($numOfNameColumn > 0)
				{
					$markTable .= "<th colspan='$numOfNameColumn'>".$eReportCard['Template']['StudentInfo']['Name']."</th>";
				}
				// [2015-0306-1036-36206]
				if($showUserLogin) {
					$markTable .= "<th rowspan='$numOfNameRow' >".$eReportCard['Template']['StudentInfo']['UserLogin']."</th>";
				}
				if($showGender) {
					$markTable .= "<th rowspan='$numOfNameRow' >".$eReportCard['Template']['StudentInfo']['Gender']."</th>";
				}
				
				if($showSubject)
				{
				$SubjectName = $FormSubjectArr[$SubjectID][0];			
	  			if (!isset($ReportColumnID) || empty($ReportColumnID))
	  			{
	  				/*
	  				for($i=0; $i<sizeof($ColumnIDList); $i++) {
	  					$markTable .= "<th>".$ColumnTitleAry[$ColumnIDList[$i]]."</th>";
	  				}
	  				*/
	  				##############################
	  				// Special handling of subject which having component subjects
	  				if ($showSubjectComponent)
	  				{
	  					if (in_array($SubjectID, $ParentSubjectIDList)) 
	  					{
	  						$countCmpSubject = 1;
	  						foreach($CmpSubjectIDNameArr as $tmpSubjectID => $tmpSubjectName) {
	  							$markTable .= "<th>[".$countCmpSubject."]<br />".$tmpSubjectName."</th>";
	  							$countCmpSubject++;
	  							
	  							$columnSubjectIDMapArr[] = $tmpSubjectID;
	  							
	  							$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID,0 ,0 ,$ReportID);
	  							$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
	  						}
	  						#$markTable .= "<th>".$eReportCard['Template']['Total']."</th>";
	  					}
	  				}
	  				##############################
	  				$markTable .= "<th>".$eReportCard['Template']['SubjectOverall']."</th>";
	  				$columnSubjectIDMapArr[] = $SubjectID;
	  							
	  				$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0 ,$ReportID);
	  				$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
	  			}
	  			else
	  			{
	  				##############################
	  				// Special handling of subject which having component subjects
	  				if ($showSubjectComponent && in_array($SubjectID, $ParentSubjectIDList))
	  				{
	  					foreach($CmpSubjectIDNameArr as $tmpSubjectID => $tmpSubjectName) {
	  						$markTable .= "<th>".$tmpSubjectName."</th>";
	  						$columnSubjectIDMapArr[] = $tmpSubjectID;
	  							
	  						$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID,0 ,0 ,$ReportID);
	  						$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
	  					}
	  					#$markTable .= "<th>".$eReportCard['Template']['Total']."</th>";
	  				}
	  				$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0 ,$ReportID);
	  				$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
	  				##############################
	  				
	  				#$markTable .= "<th>".$ColumnTitleAry[$ReportColumnID]."</th>";
	  				$markTable .= "<th>".$eReportCard['Template']['SubjectOverall']."</th>";
	  				//$markTable .= "<th>".$eReportCard['Template']['Ranking']."</th>";
	  			}
	  		}	
			}
			
			# Summary Info
			if ($showSummaryInfo)
			{
				for($i=0; $i<sizeof($SummaryInfoFields); $i++) {
					$markTable .= "<th rowspan='$numOfNameRow'>".$eReportCard['Template'][$SummaryInfoFields[$i]]."</th>";
					$columnScaleOutputMapArr[] = "G";
				}
			}
			
			$markTable .= ($showGrandTotal)? "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['GrandTotal']."</th>" : "";
			$markTable .= ($showGrandTotalGrade)? "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['GrandTotal']." ".$eReportCard['Grade']."</th>" : "";
			$markTable .= ($showGrandAverage)? "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['GrandAverage']."</th>" : "";
			$markTable .= ($showGrandAverageGrade)? "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['GrandAverage']." ".$eReportCard['Grade']."</th>" : "";
			$markTable .= ($showActualAverage)? "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['ActualAverage']."</th>" : "";		
			$markTable .= ($showActualAverageGrade)? "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['ActualAverage']." ".$eReportCard['Grade']."</th>" : "";
			$markTable .= ($showGPA)? "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['GPA']."</th>" : "";		
			$markTable .= ($showGPAGrade)? "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['GPA']." ".$eReportCard['Grade']."</th>" : "";		
			if ($ReportCardCustomSchoolName == "carmel_alison")
				$markTable .= "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['WeightedStandardScore']."</th>";
			$markTable .= ($showClassRanking)? "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['ClassPosition']."</th>" : "";
			$markTable .= ($showOverallRanking)? "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['FormPosition']."</th>" : "";
			$markTable .= ($showSubject)? "<th rowspan='$numOfNameRow'>".$eReportCard['NumOfPassSubject']."</th>" : "";
			// [2019-0123-1731-14066]
			if($eRCTemplateSetting['Report']['GrandMarksheet']['ShowTotalUnitFailed']) {
			    $markTable .= ($showTotalUnitFailed)? "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['TotalUnitFailed']."</th>" : "";
			}
			
			if ($showGrandTotal)
			{
				$columnScaleOutputMapArr[] = "M";
			}
			if ($showGrandTotalGrade)
			{
				$columnScaleOutputMapArr[] = "G";
			}
			if ($showGrandAverage)
			{
				$columnScaleOutputMapArr[] = "M"; 
			}
			if ($showGrandAverageGrade)
			{
				$columnScaleOutputMapArr[] = "G";
			}
			if ($showActualAverage)
			{
				$columnScaleOutputMapArr[] = "M"; 
			}
			if ($showActualAverageGrade)
			{
				$columnScaleOutputMapArr[] = "G";
			}
			if ($showGPA)
			{
				$columnScaleOutputMapArr[] = "M";
			}
			if ($showGPAGrade)
			{
				$columnScaleOutputMapArr[] = "G";
			}
			
			//for Weighted SD Score
			if ($ReportCardCustomSchoolName == "carmel_alison")
				$columnScaleOutputMapArr[] = "M";
				
			$markTable .= "</tr>";
			if ($cmpNameRow != "") {
				$markTable .= "<tr>$cmpNameRow</tr>";
			}
			$markTable .= "</thead>";
			
			# initialize variables to calculate Average, S.D, Variance, Max, Min, Passing Rate
			// $markDisplayedArr[#column][#row] = Mark;
			$markDisplayedArr = array();
			// $markNatureArr[#column][#row] = "Distinction" / "Pass" / "Fail";
			$markNatureArr = array();
			$ColumnCounter = 0;
			
			// Mark
			$markTable .= "<tbody>";
			foreach($studentIDList as $classNumber => $studentID)
			{
				$ColumnCounter = 0;
				$NumberOfPassSubject=0;
				
				$markTable .= "<tr>";
				$markTable .= "<td align='center'>$classNumber</td>";			
				//$markTable .= "<td>".$studentIDNameMap[$studentID]."</td>";
				
				$studentNameEn = trim($studentIDNameMap[$studentID]['En']);
				$studentNameCh = trim($studentIDNameMap[$studentID]['Ch']);
							
				$markTable .= ($showEnglishName)? "<td>".$studentNameEn."</td>" : "";
				$markTable .= ($showChineseName)? "<td>".$studentNameCh."</td>" : "";
				
				# User Login & Gender
				//$StudentInfo = $lreportcard-> Get_Student_Info_By_ClassName_ClassNumber($ClassName,$classNumber);
				$StudentInfo = $lreportcard->Get_Student_Class_ClassLevel_Info($studentID);
				// [2015-0306-1036-36206]
				$markTable .= ($showUserLogin)? "<td align='center'>".$StudentInfo[0]["UserLogin"]."</td>" : "";
				$markTable .= ($showGender)? "<td align='center'>".$StudentInfo[0]["Gender"]."</td>" : "";
				
				if (!isset($SubjectID) || empty($SubjectID))
				{
					#################################################################
					# all subjects Start
					#################################################################
					# overall term
					if($showSubject)
					{
	  				foreach($FormSubjectArr as $tmpSubjectID => $tmpSubjectName)
	  				{
	  					//skip non selected subject and its component
	  					if (in_array($tmpSubjectID, $CmpSubjectIDList)) {
	  						$ck_subject_id = $lreportcard->GET_PARENT_SUBJECT_ID($tmpSubjectID);
	  					}
	  					else {
	  						$ck_subject_id = $tmpSubjectID;
	  					}
	  					if(!in_array($ck_subject_id, $SelectedSubjectID))	continue;
						
	  					if ($showSubjectComponent)
	  					{
	  					    $thisMarks = $resultSubject[$tmpSubjectID][$studentID];
	  					    $thisGrade = $resultSubjectGrade[$tmpSubjectID][$studentID];
	  					    
	  					    // [2019-0123-1731-14066] add rounding logic
	  					    if($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayRoundMarks'] && is_numeric($thisMarks)) {
	  					        $thisMarks = $lreportcard->ROUND_MARK($thisMarks, "SubjectTotal");
	  					    }
	  						
	  						# Record Marks for Statistics
	  						$markDisplayedArr[$ColumnCounter][] = (is_numeric($thisMarks))? $thisMarks : '';
	  							
	  						# Record Pass Status for Passing Rate
	  						$thisNature = '';
	  						//if (!$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1_STRING($thisMarks) && !$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2_STRING($thisMarks))
	  						if (!in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1()) && !in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2()))
	  						{
	  							$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID, 1 ,0 ,$ReportID);
								$SchemeID = $SubjectFormGradingSettings['SchemeID'];
								$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
	  							$SchemeInfoArr = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
								$TopPercentage = $SchemeInfoArr['TopPercentage'];
								
								if($ScaleDisplay == "M" && $TopPercentage > 0)
	  							{
	  								$thisStyleDetermineMarks = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMarks, $ReportID, $studentID, $tmpSubjectID, $ClassLevelID, $ReportColumnID);
	  								$ConvertBy = "G";
	  							}
	  							else
	  							{
	  								$thisStyleDetermineMarks = $thisMarks;
	  								$ConvertBy = "";
	  							}
	  							
	  							$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $tmpSubjectID, $thisStyleDetermineMarks,'',$ConvertBy, $ReportID);
	  							$markNatureArr[$ColumnCounter][] = $thisNature;
	  							if(in_array($thisNature,array("Pass","Distinction"))  && !in_array($tmpSubjectID, $CmpSubjectIDList))	$NumberOfPassSubject++;
	  						
	  						}
	  						else
							{
								$markNatureArr[$ColumnCounter][] = "N.A.";
							}
							
							$thisDisplayMarks = $lreportcard->Get_Score_Display_HTML($thisMarks, $ReportID, $ClassLevelID, $tmpSubjectID, '', '', '', $ReportColumnID, $thisNature);
	  						$markTable .= "<td align='center'>".$thisDisplayMarks."</td>";
	  						
							# Record Subject Weight
							if(!isset($SubjectWeightArr[$ColumnCounter]))
							{
								$cond = " SubjectID = '$tmpSubjectID'";
								if(!empty($ReportColumnID))
								{
									$cond .= " AND ReportColumnID = '$ReportColumnID'";
									$tmpReportColumnID = $ReportColumnID;
								}
								else
								{
									$cond .= " AND( ReportColumnID = '0' OR ReportColumnID IS NULL)";
									$tmpReportColumnID = 0;
								}
//								$thisWeight = $lreportcard->returnReportTemplateSubjectWeightData($ReportID,$cond);
//								$SubjectWeightArr[$ColumnCounter] = $thisWeight[0]["Weight"];
								
								if ($eRCTemplateSetting['VerticalSubjectWeightForOverallForVerticalHorizontalCalculation'] && $tmpReportColumnID == 0) {
									$VerticalWeightArr = $lreportcard->Get_Assessment_Subject_Vertical_Weight($ReportID, $tmpReportColumnID, $ReturnAsso=true);
									$SubjectWeightArr[$ColumnCounter] = $VerticalWeightArr[$tmpReportColumnID][$SubjectID]['Weight'];
								}
								else {
									$thisWeight = $lreportcard->returnReportTemplateSubjectWeightData($ReportID,$cond);
									$SubjectWeightArr[$ColumnCounter] = $thisWeight[0]["Weight"];
								}
							}
							$ColumnCounter++;
	  					}
	  					else
	  					{
	  						# show parent subject only
	  						if (!in_array($tmpSubjectID, $CmpSubjectIDList))
	  						{
	  							$thisMarks = $resultSubject[$tmpSubjectID][$studentID];
	  							$thisGrade = $resultSubjectGrade[$tmpSubjectID][$studentID];
	  							
	  							// [2019-0123-1731-14066] add rounding logic
	  							if($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayRoundMarks'] && is_numeric($thisMarks)) {
	  							    $thisMarks = $lreportcard->ROUND_MARK($thisMarks, "SubjectTotal");
	  							}
	  							
	  							# Record Marks for Statistics
	  							$markDisplayedArr[$ColumnCounter][] = (is_numeric($thisMarks))? $thisMarks : '';
	  							
	  							# Record Pass Status for Passing Rate
	  							$thisNature = '';
	  							//if (!$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1_STRING($thisMarks) && !$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2_STRING($thisMarks))
	  							if (!in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1()) && !in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2()))
		  						{
		  							$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID, 1 ,0 ,$ReportID);
									$SchemeID = $SubjectFormGradingSettings['SchemeID'];
									$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
									$SchemeInfoArr = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
									$TopPercentage = $SchemeInfoArr['TopPercentage'];
		  							
		  							if($ScaleDisplay == "M" && $TopPercentage > 0)
		  							{
		  								$thisStyleDetermineMarks = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMarks, $ReportID, $studentID, $tmpSubjectID, $ClassLevelID, $ReportColumnID);
		  								$ConvertBy = "G";
		  							}
		  							else
		  							{
		  								$thisStyleDetermineMarks = $thisMarks;
		  								$ConvertBy = "";
		  							}
		  								
		  							$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $tmpSubjectID, $thisStyleDetermineMarks,'',$ConvertBy, $ReportID);
		  							$markNatureArr[$ColumnCounter][] = $thisNature;
		  							if(in_array($thisNature,array("Pass","Distinction"))  && !in_array($tmpSubjectID, $CmpSubjectIDList))	$NumberOfPassSubject++;
		  						}
	  							else
	  							{
	  								$markNatureArr[$ColumnCounter][] = "N.A.";
	  							}
	  							
	  							$thisDisplayMarks = $lreportcard->Get_Score_Display_HTML($thisMarks, $ReportID, $ClassLevelID, $tmpSubjectID, '', '', '', 0, $thisNature);
	  							$markTable .= "<td align='center'>".$thisDisplayMarks."</td>";
	  							
		  						# Record Subject Weight
								if(!isset($SubjectWeightArr[$ColumnCounter]))
								{
									$cond = " SubjectID = '$tmpSubjectID'";
									if(!empty($ReportColumnID))
									{
										$cond .= " AND ReportColumnID = '$ReportColumnID'";
										$tmpReportColumnID = $ReportColumnID;
									}
									else
									{
										$cond .= " AND( ReportColumnID = '0' OR ReportColumnID IS NULL)";
										$tmpReportColumnID = 0;
									}
//									$thisWeight = $lreportcard->returnReportTemplateSubjectWeightData($ReportID,$cond);
//									$SubjectWeightArr[$ColumnCounter] = $thisWeight[0]["Weight"];
									if ($eRCTemplateSetting['VerticalSubjectWeightForOverallForVerticalHorizontalCalculation'] && $tmpReportColumnID == 0) {
										$VerticalWeightArr = $lreportcard->Get_Assessment_Subject_Vertical_Weight($ReportID, $tmpReportColumnID, $ReturnAsso=true);
										$SubjectWeightArr[$ColumnCounter] = $VerticalWeightArr[$tmpReportColumnID][$SubjectID]['Weight'];
									}
									else {
										$thisWeight = $lreportcard->returnReportTemplateSubjectWeightData($ReportID,$cond);
										$SubjectWeightArr[$ColumnCounter] = $thisWeight[0]["Weight"];
									}
								}
								$ColumnCounter++;
	  						}
	  					}
	  				}
					}
					//$markTable .= "<td align='center'>".$resultAverage[$studentID]["GrandTotal"]."</td>";
					//$markTable .= "<td align='center'>".$resultAverage[$studentID]["GrandAverage"]."</td>";
					//$markTable .= "<td align='center'>".$resultAverage[$studentID]["OrderMeritClass"]."</td>";
					
					# Summary Info
					if ($showSummaryInfo)
					{
						for($i=0; $i<sizeof($SummaryInfoFields); $i++)
						{
//							$thisRegNo = $studentIDRegNoMap[$studentID];
							$thisClass = $studentIDClassMap[$studentID];
							$tmpInfo = isset($ary[$studentID][$SummaryInfoFields[$i]]) ? $ary[$studentID][$SummaryInfoFields[$i]]: "-";
							// [2016-0128-1008-07066] display multiple Other Info data
							$tmpInfo = is_array($tmpInfo) ? implode("<br/>", $tmpInfo) : $tmpInfo;
							
							$markTable .= "<td align='center'>".$tmpInfo."</td>";
							
							# Record Marks for Statistics
							$markDisplayedArr[$ColumnCounter][] = (is_numeric($tmpInfo))? $tmpInfo : '';
							
							$ColumnCounter++;
						}
					}
					$thisDisplayAverage = $lreportcard->Get_Score_Display_HTML($resultAverage[$studentID]["GrandAverage"], $ReportID, $ClassLevelID, -1,$resultAverage[$studentID]["GrandAverage"],'GrandAverage');
					$thisDisplayActualAverage = $lreportcard->Get_Score_Display_HTML($resultAverage[$studentID]["ActualAverage"], $ReportID, $ClassLevelID, -1,$resultAverage[$studentID]["ActualAverage"],'GrandAverage');
					$thisDisplayTotal = $lreportcard->Get_Score_Display_HTML($resultAverage[$studentID]["GrandTotal"], $ReportID, $ClassLevelID, -2,$resultAverage[$studentID]["GrandTotal"],'GrandTotal');
					$thisDisplayGPA= $lreportcard->Get_Score_Display_HTML($resultAverage[$studentID]["GPA"], $ReportID, $ClassLevelID, -3,$resultAverage[$studentID]["GPA"],'GPA');
					$GrandMarkScheme = $lreportcard->Get_Grand_Mark_Grading_Scheme($ClassLevelID,$ReportID);
					
					$AverageGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandMarkScheme["-1"]['SchemeID'], $resultAverage[$studentID]["GrandAverage"], $ReportID, $studentID, '-1', $ClassLevelID, $ReportColumnID);
					$thisDisplayAverageGrade = $lreportcard->Get_Score_Display_HTML($AverageGrade, $ReportID, $ClassLevelID, -1, $resultAverage[$studentID]["GrandAverage"], 'GrandAverage');
					
					// Handle Adjusted Average Grade
					$adjustedAvgGrade = $resultAverage[$studentID]["GrandAverageGrade"];
					if($eRCTemplateSetting['GrandAverageGradeManualAdjustment'] && !empty($adjustedAvgGrade)) {
						$AverageGrade = $adjustedAvgGrade;
						$thisDisplayAverageGrade = $lreportcard->Get_Score_Display_HTML($AverageGrade, $ReportID, $ClassLevelID, -1, $AverageGrade, 'GrandAverage');
					}
					
					$ActualAverageGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandMarkScheme["-1"]['SchemeID'], $resultAverage[$studentID]["ActualAverage"],$ReportID,$studentID,'-1',$ClassLevelID,$ReportColumnID);
					$thisDisplayActualAverageGrade = $lreportcard->Get_Score_Display_HTML($ActualAverageGrade, $ReportID, $ClassLevelID, -1,$resultAverage[$studentID]["ActualAverage"],'GrandAverage');
					
					$TotalGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandMarkScheme["-2"]['SchemeID'], $resultAverage[$studentID]["GrandTotal"],$ReportID,$studentID,'-2',$ClassLevelID,$ReportColumnID);
					$thisDisplayTotalGrade= $lreportcard->Get_Score_Display_HTML($TotalGrade, $ReportID, $ClassLevelID, -2,$resultAverage[$studentID]["GrandTotal"],'GrandTotal');
					$GPAGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandMarkScheme["-3"]['SchemeID'], $resultAverage[$studentID]["GPA"],$ReportID,$studentID,'-3',$ClassLevelID,$ReportColumnID);
					$thisDisplayGPAGrade= $lreportcard->Get_Score_Display_HTML($GPAGrade, $ReportID, $ClassLevelID, -3,$resultAverage[$studentID]["GPA"],'GPA');
					
					$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, -1, 1 ,0 ,$ReportID);
					$SchemeID = $SubjectFormGradingSettings['SchemeID'];
					$GrandAvgScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
					$SchemeInfoArr = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
					$GrandAvgTopPercentage = $SchemeInfoArr['TopPercentage'];
					
					if ($GrandAvgScaleDisplay == "M" && $GrandAvgTopPercentage > 0) {
						$thisStyleDetermineMarks = $AverageGrade;
						$thisActualAverageStyleDetermineMarks = $ActualAverageGrade;
						$ConvertBy = "G";
					}
					else {
						$thisStyleDetermineMarks = $resultAverage[$studentID]["GrandAverage"];
						$thisActualAverageStyleDetermineMarks = $resultAverage[$studentID]["ActualAverage"];
						$ConvertBy = "";
					}
					
					$GrandAvgNatureArr[] = $lreportcard->returnMarkNature($ClassLevelID,-1,$thisStyleDetermineMarks,'',$ConvertBy, $ReportID);
					$ActualAvgNatureArr[] = $lreportcard->returnMarkNature($ClassLevelID,-1,$thisActualAverageStyleDetermineMarks,'',$ConvertBy, $ReportID);
					
//					$GrandAvgNatureArr[] = $lreportcard->returnMarkNature($ClassLevelID,-1,$AverageGrade,'','G');
	
					$markTable .= ($showGrandTotal)? "<td align='center'>".$thisDisplayTotal."</td>" : "";
					$markTable .= ($showGrandTotalGrade)? "<td align='center'>".$thisDisplayTotalGrade."</td>" : "";
					$markTable .= ($showGrandAverage)? "<td align='center'>".$thisDisplayAverage."</td>" : "";
					$markTable .= ($showGrandAverageGrade)? "<td align='center'>".$thisDisplayAverageGrade."</td>" : "";				
					$markTable .= ($showActualAverage)? "<td align='center'>".$thisDisplayActualAverage."</td>" : "";
					$markTable .= ($showActualAverageGrade)? "<td align='center'>".$thisDisplayActualAverageGrade."</td>" : "";
					$markTable .= ($showGPA)? "<td align='center'>".$thisDisplayGPA."</td>" : "";
					$markTable .= ($showGPAGrade)? "<td align='center'>".$thisDisplayGPAGrade."</td>" : "";
					if ($ReportCardCustomSchoolName == "carmel_alison")
					{
						$GrandMarkArr = $lreportcard->getReportResultScore($ReportID, $reportColumnID, $studentID);
						$thisGrandSDScore = $GrandMarkArr["GrandSDScore"];
						$markTable .= "<td align='center'>".$thisGrandSDScore."</td>";
					}
					$markTable .= ($showClassRanking)? "<td align='center'>".$resultAverage[$studentID]["OrderMeritClass"]."</td>" : "";
					$markTable .= ($showOverallRanking)? "<td align='center'>".$resultAverage[$studentID]["OrderMeritForm"]."</td>" : "";
					$markTable .= ($showSubject)? "<td align='center'>".$NumberOfPassSubject."</td>":"";
					// [2019-0123-1731-14066]
					if($eRCTemplateSetting['Report']['GrandMarksheet']['ShowTotalUnitFailed']) {
					    $markTable .= $showTotalUnitFailed? "<td align='center'>".(isset($studentFailUnitArr[$studentID])? $studentFailUnitArr[$studentID] : $lreportcard->EmptySymbol)."</td>":"";
					}
					
					//$GrandMarkScheme = $lreportcard->Get_Grand_Mark_Grading_Scheme($ClassLevelID);
					
					if ($showGrandTotal)
					{
						$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandTotal"];
						$ColumnCounter++;
					}
					if ($showGrandTotalGrade)
					{
						$markDisplayedArr[$ColumnCounter][] = $TotalGrade;
						$ColumnCounter++;
					}
					if ($showGrandAverage)
					{
						$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandAverage"];
						$ColumnCounter++;
					}
					if ($showGrandAverageGrade)
					{
						$markDisplayedArr[$ColumnCounter][] = $AverageGrade;
						$ColumnCounter++;
					}
					if ($showActualAverage)
					{
						$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["ActualAverage"];
						$ColumnCounter++;
					}
					if ($showActualAverageGrade)
					{
						$markDisplayedArr[$ColumnCounter][] = $ActualAverageGrade;
						$ColumnCounter++;
					}
					if ($showGPA)
					{
						$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GPA"];
						$ColumnCounter++;
					}
					if ($showGPAGrade)
					{
						$markDisplayedArr[$ColumnCounter][] = $GPAGrade;
						$ColumnCounter++;
					}
					if ($ReportCardCustomSchoolName == "carmel_alison")
					{
						$markDisplayedArr[$ColumnCounter][] = $thisGrandSDScore;
						$ColumnCounter++;
					}
					// [2019-0123-1731-14066]
					if($eRCTemplateSetting['Report']['GrandMarksheet']['ShowTotalUnitFailed'] && $showTotalUnitFailed)
					{
					    $markDisplayedArr[$ColumnCounter][] = isset($studentFailUnitArr[$studentID])? $studentFailUnitArr[$studentID] : $lreportcard->EmptySymbol;
					    $ColumnCounter++;
					}
					###############################################################
					# All Subject End
					###############################################################
				}
				else
				{
					###############################################################
					# Specific Subject Start
					###############################################################
					if($showSubject)
					{
	  				if (!isset($ReportColumnID) || empty($ReportColumnID))
	  				{
	  					# overall term
	  					/*
	  					for($i=0; $i<sizeof($ColumnIDList); $i++) {
	  						$markTable .= "<td align='center'>".$resultSubjectColumn[$ColumnIDList[$i]][$studentID]."</td>";
	  					}
	  					*/
	  					##############################
	  					// Special handling of subject which having component subjects
	  					if ($showSubjectComponent && in_array($SubjectID, $ParentSubjectIDList))
	  					{
	  						foreach($CmpSubjectIDNameArr as $tmpSubjectID => $tmpSubjectName)
	  						{
	  							$thisMarks = $resultDetailSubject[$tmpSubjectID][$studentID];
	  							$thisGrade = $resultDetailSubjectGrade[$tmpSubjectID][$studentID];
	  							
	  							// [2019-0123-1731-14066] add rounding logic
	  							if($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayRoundMarks'] && is_numeric($thisMarks)) {
	  							    $thisMarks = $lreportcard->ROUND_MARK($thisMarks, "SubjectTotal");
	  							}
		  						
		  						# Record Marks for Statistics
	  							$markDisplayedArr[$ColumnCounter][] = (is_numeric($thisMarks))? $thisMarks : '';
	  								
	  							# Record Pass Status for Passing Rate
	  							$thisNature = '';
		  						//if (!$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1_STRING($thisMarks) && !$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2_STRING($thisMarks))
		  						if (!in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1()) && !in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2()))
		  						{
		  							$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID, 1 ,0 ,$ReportID);
									$SchemeID = $SubjectFormGradingSettings['SchemeID'];
									$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
		  							$SchemeInfoArr = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
									$TopPercentage = $SchemeInfoArr['TopPercentage'];
									
		  							if($ScaleDisplay == "M" && $TopPercentage > 0)
		  							{
		  								$thisStyleDetermineMarks = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMarks, $ReportID, $studentID, $tmpSubjectID, $ClassLevelID, $ReportColumnID);
		  								$ConvertBy = "G";
		  							}
		  							else
		  							{
		  								$thisStyleDetermineMarks = $thisMarks;
		  								$ConvertBy = "";
		  							}
		  							
		  							$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $tmpSubjectID, $thisStyleDetermineMarks,'',$ConvertBy, $ReportID);
		  							$markNatureArr[$ColumnCounter][] = $thisNature;
		  							if(in_array($thisNature,array("Pass","Distinction"))  && !in_array($tmpSubjectID, $CmpSubjectIDList))	$NumberOfPassSubject++;
		  						}
	  							else
	  							{
	  								$markNatureArr[$ColumnCounter][] = "N.A.";
	  							}
	  							
	  							$thisDisplayMarks = $lreportcard->Get_Score_Display_HTML($thisMarks, $ReportID, $ClassLevelID, $tmpSubjectID, '', '', '', 0, $thisNature);
	  							$markTable .= "<td align='center'>".$thisDisplayMarks."</td>";
	  							
	  							# Record Subject Weight
	  							if(!isset($SubjectWeightArr[$ColumnCounter]))
								{
									$cond = " SubjectID = '$tmpSubjectID'";
									if(!empty($ReportColumnID))
									{
										$cond .= " AND ReportColumnID = '$ReportColumnID'";
										$tmpReportColumnID = $ReportColumnID;
									}
									else
									{
										$cond .= " AND( ReportColumnID = '0' OR ReportColumnID IS NULL)";
										$tmpReportColumnID = 0;
									}
//									$thisWeight = $lreportcard->returnReportTemplateSubjectWeightData($ReportID,$cond);
//									$SubjectWeightArr[$ColumnCounter] = $thisWeight[0]["Weight"];
									if ($eRCTemplateSetting['VerticalSubjectWeightForOverallForVerticalHorizontalCalculation'] && $tmpReportColumnID == 0) {
										$VerticalWeightArr = $lreportcard->Get_Assessment_Subject_Vertical_Weight($ReportID, $tmpReportColumnID, $ReturnAsso=true);
										$SubjectWeightArr[$ColumnCounter] = $VerticalWeightArr[$tmpReportColumnID][$SubjectID]['Weight'];
									}
									else {
										$thisWeight = $lreportcard->returnReportTemplateSubjectWeightData($ReportID,$cond);
										$SubjectWeightArr[$ColumnCounter] = $thisWeight[0]["Weight"];
									}
								}
								$ColumnCounter++;
	  						}
	  						#$markTable .= "<td align='center'>".$resultDetailSubject[$SubjectID][$studentID]."</td>";
	  					}
	  					##############################
	  					
	  					$thisMarks = $resultSubject[$studentID];
	  					$thisGrade = $resultSubjectGrade[$studentID];
	  					
	  					// [2019-0123-1731-14066] add rounding logic
	  					if($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayRoundMarks'] && is_numeric($thisMarks)) {
	  					    $thisMarks = $lreportcard->ROUND_MARK($thisMarks, "SubjectTotal");
	  					}
						
	  					# Record Marks for Statistics
	  					$markDisplayedArr[$ColumnCounter][] = (is_numeric($thisMarks))? $thisMarks : '';
	  					
	  					# Record Pass Status for Passing Rate
	  					$thisNature = '';
						//if (!$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1_STRING($thisMarks) && !$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2_STRING($thisMarks) )
						if (!in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1()) && !in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2()))
						{
							$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 1 ,0 ,$ReportID);
							$SchemeID = $SubjectFormGradingSettings['SchemeID'];
							$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
							$SchemeInfoArr = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
							$TopPercentage = $SchemeInfoArr['TopPercentage'];
							
							// [2018-0129-1221-32164]
							//if($ScaleDisplay == "M")
							if($ScaleDisplay == "M" && $TopPercentage > 0)
							{
								$thisStyleDetermineMarks = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMarks, $ReportID, $studentID, $SubjectID, $ClassLevelID, $ReportColumnID);
								$ConvertBy = "G";
							}
							else
							{
								$thisStyleDetermineMarks = $thisMarks;
								$ConvertBy = "";
							}
							
							$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $SubjectID, $thisStyleDetermineMarks,'',$ConvertBy, $ReportID);
							$markNatureArr[$ColumnCounter][] = $thisNature;
							if(in_array($thisNature,array("Pass","Distinction"))  && !in_array($tmpSubjectID, $CmpSubjectIDList))	$NumberOfPassSubject++;
						}
	  					else
						{
							$markNatureArr[$ColumnCounter][] = "N.A.";
						}
						
						$thisDisplayMarks = $lreportcard->Get_Score_Display_HTML($thisMarks, $ReportID, $ClassLevelID, $SubjectID, '', '', '', 0, $thisNature);
	  					$markTable .= "<td align='center'>".$thisDisplayMarks."</td>";
	  					
						# Record Subject Weight
						if(!isset($SubjectWeightArr[$ColumnCounter]))
						{
							$cond = " SubjectID = '$SubjectID'";
							if(!empty($ReportColumnID))
							{
								$cond .= " AND ReportColumnID = '$ReportColumnID'";
								$tmpReportColumnID = $ReportColumnID;
							}
							else
							{
								$cond .= " AND( ReportColumnID = '0' OR ReportColumnID IS NULL)";
								$tmpReportColumnID = 0;
							}
							
							if ($eRCTemplateSetting['VerticalSubjectWeightForOverallForVerticalHorizontalCalculation'] && $tmpReportColumnID == 0) {
								$VerticalWeightArr = $lreportcard->Get_Assessment_Subject_Vertical_Weight($ReportID, $tmpReportColumnID, $ReturnAsso=true);
								$SubjectWeightArr[$ColumnCounter] = $VerticalWeightArr[$tmpReportColumnID][$SubjectID]['Weight'];
							}
							else {
								$thisWeight = $lreportcard->returnReportTemplateSubjectWeightData($ReportID,$cond);
								$SubjectWeightArr[$ColumnCounter] = $thisWeight[0]["Weight"];
							}
						}
	  					$ColumnCounter++;
	  				}
	  				else
	  				{
	  					##########################################################################
	  					# Specific Subject -> specific term Start
	  					##########################################################################

	  					// Special handling of subject which having component subjects
  						######################################################################
						#	Specific Subject -> Specific Term -> Component Subject Start
						######################################################################  					
	  					if ($showSubjectComponent &&  in_array($SubjectID, $ParentSubjectIDList))
	  					{
	  						foreach($CmpSubjectIDNameArr as $tmpSubjectID => $tmpSubjectName)
	  						{
	  							$thisMarks = $resultDetailSubject[$tmpSubjectID][$studentID];
	  							$thisGrade = $resultDetailSubjectGrade[$tmpSubjectID][$studentID];
	  							
	  							// [2019-0123-1731-14066] add rounding logic
	  							if($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayRoundMarks'] && is_numeric($thisMarks)) {
	  							    $thisMarks = $lreportcard->ROUND_MARK($thisMarks, "SubjectTotal");
	  							}
	  							
	  							# Record Marks for Statistics
	  							$markDisplayedArr[$ColumnCounter][] = (is_numeric($thisMarks))? $thisMarks : '';
	  								
	  							# Record Pass Status for Passing Rate
	  							$thisNature = '';
		  						//if (!$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1_STRING($thisMarks) && !$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2_STRING($thisMarks) )
		  						if (!in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1()) && !in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2()))
		  						{
		  							$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 1 ,0 ,$ReportID);
									$SchemeID = $SubjectFormGradingSettings['SchemeID'];
									$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
		  							$SchemeInfoArr = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
									$TopPercentage = $SchemeInfoArr['TopPercentage'];
									
		  							if($ScaleDisplay == "M" && $TopPercentage > 0)
		  							{
		  								$thisStyleDetermineMarks = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMarks, $ReportID, $studentID, $SubjectID, $ClassLevelID, $ReportColumnID);
		  								$ConvertBy = "G";
		  							}
		  							else
		  							{
										$thisStyleDetermineMarks = $thisMarks;
										$ConvertBy = "";
									}
		  							
		  							$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $SubjectID, $thisStyleDetermineMarks,'',$ConvertBy, $ReportID);
		  							$markNatureArr[$ColumnCounter][] = $thisNature;
		  							if(in_array($thisNature,array("Pass","Distinction"))  && !in_array($tmpSubjectID, $CmpSubjectIDList))	$NumberOfPassSubject++;
		  						}
	  							else
	  							{
	  								$markNatureArr[$ColumnCounter][] = "N.A.";
	  							}
	  							
	  							$thisDisplayMarks = $lreportcard->Get_Score_Display_HTML($thisMarks, $ReportID, $ClassLevelID, $SubjectID, '', '', '', $ReportColumnID, $thisNature);
	  							$markTable .= "<td align='center'>".$thisDisplayMarks."</td>";
	  							
	  							# Record Subject Weight
								if(!isset($SubjectWeightArr[$ColumnCounter]))
								{
									$cond = " SubjectID = '$SubjectID'";
									if(!empty($ReportColumnID))
									{
										$cond .= " AND ReportColumnID = '$ReportColumnID'";
										$tmpReportColumnID = $ReportColumnID;
									}
									else
									{
										$cond .= " AND( ReportColumnID = '0' OR ReportColumnID IS NULL)";
										$tmpReportColumnID = 0;
									}
//									$thisWeight = $lreportcard->returnReportTemplateSubjectWeightData($ReportID,$cond);
//									$SubjectWeightArr[$ColumnCounter] = $thisWeight[0]["Weight"];
									if ($eRCTemplateSetting['VerticalSubjectWeightForOverallForVerticalHorizontalCalculation'] && $tmpReportColumnID == 0) {
										$VerticalWeightArr = $lreportcard->Get_Assessment_Subject_Vertical_Weight($ReportID, $tmpReportColumnID, $ReturnAsso=true);
										$SubjectWeightArr[$ColumnCounter] = $VerticalWeightArr[$tmpReportColumnID][$SubjectID]['Weight'];
									}
									else {
										$thisWeight = $lreportcard->returnReportTemplateSubjectWeightData($ReportID,$cond);
										$SubjectWeightArr[$ColumnCounter] = $thisWeight[0]["Weight"];
									}
								}
								
	  							$ColumnCounter++;
	  						}
	  						#$markTable .= "<td align='center'>".$resultDetailSubject[$SubjectID][$studentID]."</td>";
	  					}
	  					
  						######################################################################
  						# 	Specific Subject -> Specific Term -> Component Subject End
  						######################################################################
  						
  						######################################################################
  						#	Specific Subject -> Specific Term -> Parent Subject / Main Subject Start
  						######################################################################
  						
	  					$thisMarks = $resultSubject[$studentID];
	  					$thisGrade = $resultSubjectGrade[$studentID];
	  					
	  					// [2019-0123-1731-14066] add rounding logic
	  					if($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayRoundMarks'] && is_numeric($thisMarks)) {
	  					    $thisMarks = $lreportcard->ROUND_MARK($thisMarks, "SubjectTotal");
	  					}
						
	  					# Record Marks for Statistics
	  					$markDisplayedArr[$ColumnCounter][] = (is_numeric($thisMarks))? $thisMarks : '';
	  					
	  					# Record Pass Status for Passing Rate
	  					$thisNature = '';
						//if (!$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1_STRING($thisMarks) && !$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2($thisMarks))
						if (!in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1()) && !in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2()))
						{
							$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID, 1 ,0 ,$ReportID);
							$SchemeID = $SubjectFormGradingSettings['SchemeID'];
							$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
							$SchemeInfoArr = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
							$TopPercentage = $SchemeInfoArr['TopPercentage'];
									
							if($ScaleDisplay == "M" && $TopPercentage > 0)
							{
								$thisStyleDetermineMarks = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMarks, $ReportID, $studentID, $tmpSubjectID, $ClassLevelID, $ReportColumnID);
								$ConvertBy = "G";
							}
							else
							{
								$thisStyleDetermineMarks = $thisMarks;
								$ConvertBy = "";
							}
							
							$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $tmpSubjectID, $thisStyleDetermineMarks,'',$ConvertBy, $ReportID);
							$markNatureArr[$ColumnCounter][] = $thisNature;
							
							if(in_array($thisNature,array("Pass","Distinction"))  && !in_array($tmpSubjectID, $CmpSubjectIDList))	$NumberOfPassSubject++;
						}
	  					else
						{
							$markNatureArr[$ColumnCounter][] = "N.A.";
						}
						
						$thisDisplayMarks = $lreportcard->Get_Score_Display_HTML($thisMarks, $ReportID, $ClassLevelID, $SubjectID, '', '', '', $ReportColumnID, $thisNature);
	  					$markTable .= "<td align='center'>".$thisDisplayMarks."</td>";
	
						# Record Subject Weight
	  					if(!isset($SubjectWeightArr[$ColumnCounter]))
						{
							$cond = " SubjectID = '$SubjectID'";
							if(!empty($ReportColumnID))
							{
								$cond .= " AND ReportColumnID = '$ReportColumnID'";
								$tmpReportColumnID = $ReportColumnID;
							}
							else
							{
								$cond .= " AND( ReportColumnID = '0' OR ReportColumnID IS NULL)";
								$tmpReportColumnID = 0;
							}
//							$thisWeight = $lreportcard->returnReportTemplateSubjectWeightData($ReportID,$cond);
//							$SubjectWeightArr[$ColumnCounter] = $thisWeight[0]["Weight"];
							if ($eRCTemplateSetting['VerticalSubjectWeightForOverallForVerticalHorizontalCalculation'] && $tmpReportColumnID == 0) {
								$VerticalWeightArr = $lreportcard->Get_Assessment_Subject_Vertical_Weight($ReportID, $tmpReportColumnID, $ReturnAsso=true);
								$SubjectWeightArr[$ColumnCounter] = $VerticalWeightArr[$tmpReportColumnID][$SubjectID]['Weight'];
							}
							else {
								$thisWeight = $lreportcard->returnReportTemplateSubjectWeightData($ReportID,$cond);
								$SubjectWeightArr[$ColumnCounter] = $thisWeight[0]["Weight"];
							}
						}
	  					$ColumnCounter++;
	  					
  						######################################################################
  						#	Specific Subject -> Specific Term -> Parent Subject / Main Subject End
  						######################################################################
	  					//$markTable .= "<td align='center'>".$resultSubject[$studentID]."</td>";
	  					//$markTable .= "<td align='center'>".$OrderClassSubject[$studentID]."</td>";
						######################################################################
						#	Specific Subject -> Specific Term  End
						######################################################################
	  				}
					}
					
					# Summary Info
					if ($showSummaryInfo)
					{
						for($i=0; $i<sizeof($SummaryInfoFields); $i++)
						{
//							$thisRegNo = $studentIDRegNoMap[$studentID];
							$thisClass = $studentIDClassMap[$studentID];
							$tmpInfo = isset($ary[$studentID][$SummaryInfoFields[$i]]) ? $ary[$studentID][$SummaryInfoFields[$i]]: "-";
							// [2016-0128-1008-07066] display multiple Other Info data
							$tmpInfo = is_array($tmpInfo) ? implode("<br/>", $tmpInfo) : $tmpInfo;
							
							$markTable .= "<td align='center'>".$tmpInfo."</td>";
							$markDisplayedArr[$ColumnCounter][] = $tmpInfo;
							$ColumnCounter++;
						}
					}
					
					$thisDisplayAverage = $lreportcard->Get_Score_Display_HTML($resultAverage[$studentID]["GrandAverage"], $ReportID, $ClassLevelID, -2,$resultAverage[$studentID]["GrandAverage"],'GrandAverage');
					$thisDisplayActualAverage = $lreportcard->Get_Score_Display_HTML($resultAverage[$studentID]["ActualAverage"], $ReportID, $ClassLevelID, -2,$resultAverage[$studentID]["ActualAverage"],'GrandAverage');
					$thisDisplayTotal= $lreportcard->Get_Score_Display_HTML($resultAverage[$studentID]["GrandTotal"], $ReportID, $ClassLevelID, -1,$resultAverage[$studentID]["GrandTotal"],'GrandTotal');
					$thisDisplayGPA= $lreportcard->Get_Score_Display_HTML($resultAverage[$studentID]["GPA"], $ReportID, $ClassLevelID, -1,$resultAverage[$studentID]["GPA"],'GPA');
					
					$GrandMarkScheme = $lreportcard->Get_Grand_Mark_Grading_Scheme($ClassLevelID,$ReportID);
					
					$AverageGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandMarkScheme["-1"]['SchemeID'], $resultAverage[$studentID]["GrandAverage"], $ReportID, $studentID, '-1', $ClassLevelID, $ReportColumnID);
					$thisDisplayAverageGrade = $lreportcard->Get_Score_Display_HTML($AverageGrade, $ReportID, $ClassLevelID, -1, $resultAverage[$studentID]["GrandAverage"], 'GrandAverage');
					
					// Handle Adjusted Average Grade
					$adjustedAvgGrade = $resultAverage[$studentID]["GrandAverageGrade"];
					if($eRCTemplateSetting['GrandAverageGradeManualAdjustment'] && !empty($adjustedAvgGrade)) {
						$AverageGrade = $adjustedAvgGrade;
						$thisDisplayAverageGrade = $lreportcard->Get_Score_Display_HTML($AverageGrade, $ReportID, $ClassLevelID, -1, $AverageGrade, 'GrandAverage');
					}
					
					$ActualAverageGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandMarkScheme["-1"]['SchemeID'], $resultAverage[$studentID]["ActualAverage"],$ReportID,$studentID,'-1',$ClassLevelID,$ReportColumnID);
					$thisDisplayActualAverageGrade = $lreportcard->Get_Score_Display_HTML($ActualAverageGrade, $ReportID, $ClassLevelID, -1,$resultAverage[$studentID]["ActualAverage"],'GrandAverage');
					$TotalGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandMarkScheme["-2"]['SchemeID'], $resultAverage[$studentID]["GrandTotal"],$ReportID,$studentID,'-2',$ClassLevelID,$ReportColumnID);
					$thisDisplayTotalGrade= $lreportcard->Get_Score_Display_HTML($TotalGrade, $ReportID, $ClassLevelID, -2,$resultAverage[$studentID]["GrandTotal"],'GrandTotal');
					$GPAGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandMarkScheme["-3"]['SchemeID'], $resultAverage[$studentID]["GPA"],$ReportID,$studentID,'-3',$ClassLevelID,$ReportColumnID);
					$thisDisplayGPAGrade= $lreportcard->Get_Score_Display_HTML($GPAGrade, $ReportID, $ClassLevelID, -3,$resultAverage[$studentID]["GPA"],'GPA');
	
					$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, -1, 1 ,0 ,$ReportID);
					$SchemeID = $SubjectFormGradingSettings['SchemeID'];
					$GrandAvgScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
					$SchemeInfoArr = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
					$GrandAvgTopPercentage = $SchemeInfoArr['TopPercentage'];
					
					if ($GrandAvgScaleDisplay == "M" && $GrandAvgTopPercentage > 0) {
						$thisStyleDetermineMarks = $AverageGrade;
						$thisActualAverageStyleDetermineMarks = $ActualAverageGrade;
						$ConvertBy = "G";
					}
					else {
						$thisStyleDetermineMarks = $resultAverage[$studentID]["GrandAverage"];
						$thisActualAverageStyleDetermineMarks = $resultAverage[$studentID]["ActualAverage"];
						$ConvertBy = "";
					}
					
					$GrandAvgNatureArr[] = $lreportcard->returnMarkNature($ClassLevelID,-1,$thisStyleDetermineMarks,'',$ConvertBy, $ReportID);
					$ActualAvgNatureArr[] = $lreportcard->returnMarkNature($ClassLevelID,-1,$thisActualAverageStyleDetermineMarks,'',$ConvertBy, $ReportID);
//					$GrandAvgNatureArr[] = $lreportcard->returnMarkNature($ClassLevelID,-1,$AverageGrade,'','G');
					
					$markTable .= ($showGrandTotal)? "<td align='center'>".$thisDisplayTotal."</td>" : "";
					$markTable .= ($showGrandTotalGrade)? "<td align='center'>".$thisDisplayTotalGrade."</td>" : "";
					$markTable .= ($showGrandAverage)? "<td align='center'>".$thisDisplayAverage."</td>" : "";
					$markTable .= ($showGrandAverageGrade)? "<td align='center'>".$thisDisplayAverageGrade."</td>" : "";				
					$markTable .= ($showActualAverage)? "<td align='center'>".$thisDisplayActualAverage."</td>" : "";
					$markTable .= ($showActualAverageGrade)? "<td align='center'>".$thisDisplayActualAverageGrade."</td>" : "";
					$markTable .= ($showGPA)? "<td align='center'>".$thisDisplayGPA."</td>" : "";
					$markTable .= ($showGPAGrade)? "<td align='center'>".$thisDisplayGPAGrade."</td>" : "";
					if ($ReportCardCustomSchoolName == "carmel_alison")
					{
						$GrandMarkArr = $lreportcard->getReportResultScore($ReportID, $reportColumnID, $studentID);
						$thisGrandSDScore = $GrandMarkArr["GrandSDScore"];
						$markTable .= "<td align='center'>".$thisGrandSDScore."</td>";
					}
					$markTable .= ($showClassRanking)? "<td align='center'>".$resultAverage[$studentID]["OrderMeritClass"]."</td>" : "";
					$markTable .= ($showOverallRanking)? "<td align='center'>".$resultAverage[$studentID]["OrderMeritForm"]."</td>" : "";
					$markTable .= ($showSubject)? "<td align='center'>".$NumberOfPassSubject."</td>":"";
					// [2019-0123-1731-14066]
					if($eRCTemplateSetting['Report']['GrandMarksheet']['ShowTotalUnitFailed']) {
					    $markTable .= ($showTotalUnitFailed)? "<td align='center'>".(isset($studentFailUnitArr[$studentID])? $studentFailUnitArr[$studentID] : $lreportcard->EmptySymbol)."</td>":"";
					}
					
					if ($showGrandTotal)
					{
						$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandTotal"];
						$ColumnCounter++;
					}
					if ($showGrandTotalGrade)
					{
						$markDisplayedArr[$ColumnCounter][] = $TotalGrade;
						$ColumnCounter++;
					}
					if ($showGrandAverage)
					{
						$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandAverage"];
						$ColumnCounter++;
					}
					if ($showGrandAverageGrade)
					{
						$markDisplayedArr[$ColumnCounter][] = $AverageGrade;
						$ColumnCounter++;
					}
					if ($showActualAverage)
					{
						$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["ActualAverage"];
						$ColumnCounter++;
					}
					if ($showActualAverageGrade)
					{
						$markDisplayedArr[$ColumnCounter][] = $ActualAverageGrade;
						$ColumnCounter++;
					}
					if ($showGPA)
					{
						$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GPA"];
						$ColumnCounter++;
					}
					if ($showGPAGrade)
					{
						$markDisplayedArr[$ColumnCounter][] = $GPAGrade;
						$ColumnCounter++;
					}
					if ($ReportCardCustomSchoolName == "carmel_alison")
					{
						$markDisplayedArr[$ColumnCounter][] = $thisGrandSDScore;
						$ColumnCounter++;
					}
					// [2019-0123-1731-14066]
					if($eRCTemplateSetting['Report']['GrandMarksheet']['ShowTotalUnitFailed'] && $showTotalUnitFailed) {
					    $markDisplayedArr[$ColumnCounter][] = isset($studentFailUnitArr[$studentID])? $studentFailUnitArr[$studentID] : $lreportcard->EmptySymbol;
					    $ColumnCounter++;
					}
				}
				$markTable .= "</tr>";
			}
			
			if ($showStatistics)
			{
				# Statistics Rows
				$TitleColSpanNum = 1;
				$decimalPlacesShown = 2;
				if ($showEnglishName)
					$TitleColSpanNum++;
				if ($showChineseName)
					$TitleColSpanNum++;
				// [2015-0306-1036-36206]
				if ($ShowUserLogin)
					$TitleColSpanNum++;
				if ($ShowGender)
					$TitleColSpanNum++;
				$TitleColSpan = "colspan='".$TitleColSpanNum."'";
				
				# Average
				$markTable .= "<tr>";
					$markTable .= "<td class='tabletext stat_start_border_top' align='center' $TitleColSpan><b>".$eReportCard['Template']['Average']."</b></td>";
					for ($i=0; $i<count($markDisplayedArr); $i++)
					{
						$thisScaleDisplay = $columnScaleOutputMapArr[$i];
						
						if ($thisScaleDisplay != "M")
						{
							$thisDisplay = $lreportcard->EmptySymbol;
						}
						else
						{
							$thisColumnMarksArr = array_values(array_remove_empty($markDisplayedArr[$i]));
							$thisDisplay = $lreportcard->getAverage($thisColumnMarksArr, $decimalPlacesShown);
						}
						$markTable .= "<td class='tabletext stat_start_border_top' align='center'><b>".$thisDisplay."</b></td>";
					}
					$markTable .= ($showClassRanking)? "<td align='center' class='stat_start_border_top'>".$lreportcard->EmptySymbol."</td>" : "";
					$markTable .= ($showOverallRanking)? "<td align='center' class='stat_start_border_top'>".$lreportcard->EmptySymbol."</td>" : "";
					$markTable .= ($showSubject)? "<td align='center' class='stat_start_border_top'>".$lreportcard->EmptySymbol."</td>":"";
				$markTable .= "</tr>";
				
				# SD
				$markTable .= "<tr>";
					$markTable .= "<td class='tabletext ' align='center' $TitleColSpan><b>".$eReportCard['Template']['SD']."</b></td>";
					for ($i=0; $i<count($markDisplayedArr); $i++)
					{
						$thisScaleDisplay = $columnScaleOutputMapArr[$i];
						
						if ($thisScaleDisplay != "M")
						{
							$thisDisplay = $lreportcard->EmptySymbol;
						}
						else
						{
							$thisColumnMarksArr = array_values(array_remove_empty($markDisplayedArr[$i]));
							$thisDisplay = $lreportcard->getSD($thisColumnMarksArr, $decimalPlacesShown);
						}
						$markTable .= "<td class='tabletext ' align='center'><b>".$thisDisplay."</b></td>";
					}
					$markTable .= ($showClassRanking)? "<td align='center' >".$lreportcard->EmptySymbol."</td>" : "";
					$markTable .= ($showOverallRanking)? "<td align='center' >".$lreportcard->EmptySymbol."</td>" : "";
					$markTable .= ($showSubject)? "<td align='center' >".$lreportcard->EmptySymbol."</td>":"";
				$markTable .= "</tr>"; 
				
				# Highest Mark
				$markTable .= "<tr>";
					$markTable .= "<td class='tabletext ' align='center' $TitleColSpan><b>".$eReportCard['Template']['HighestMark']."</b></td>";
					for ($i=0; $i<count($markDisplayedArr); $i++)
					{
						$thisScaleDisplay = $columnScaleOutputMapArr[$i];
						
						if ($thisScaleDisplay != "M")
						{
							$thisDisplay = $lreportcard->EmptySymbol;
						}
						else
						{
							$thisColumnMarksArr = array_values(array_remove_empty($markDisplayedArr[$i]));
							$thisDisplay = $lreportcard->getMaximum($thisColumnMarksArr);
							$thisDisplay = ($thisDisplay == "")? $lreportcard->EmptySymbol : $thisDisplay;
						}
						$markTable .= "<td class='tabletext ' align='center'><b>".$thisDisplay."</b></td>";
					}
					$markTable .= ($showClassRanking)? "<td align='center' >".$lreportcard->EmptySymbol."</td>" : "";
					$markTable .= ($showOverallRanking)? "<td align='center' >".$lreportcard->EmptySymbol."</td>" : "";
					$markTable .= ($showSubject)? "<td align='center' >".$lreportcard->EmptySymbol."</td>":"";
				$markTable .= "</tr>";
				
				# Lowest Mark
				$markTable .= "<tr>";
					$markTable .= "<td class='tabletext ' align='center' $TitleColSpan><b>".$eReportCard['Template']['LowestMark']."</b></td>";
					for ($i=0; $i<count($markDisplayedArr); $i++)
					{
						$thisScaleDisplay = $columnScaleOutputMapArr[$i];
						
						if ($thisScaleDisplay != "M")
						{
							$thisDisplay = $lreportcard->EmptySymbol;
						}
						else
						{
							$thisColumnMarksArr = array_values(array_remove_empty($markDisplayedArr[$i]));
							
							// [2017-0706-1012-27236] remove -1 in Column Marks
							$thisColumnMarksArr = array_diff((array)$thisColumnMarksArr, array(-1));
							$thisDisplay = $lreportcard->getMinimum($thisColumnMarksArr);
							$thisDisplay = ($thisDisplay == "")? $lreportcard->EmptySymbol : $thisDisplay;
						}
						$markTable .= "<td class='tabletext ' align='center'><b>".$thisDisplay."</b></td>";
					}
					$markTable .= ($showClassRanking)? "<td align='center' >".$lreportcard->EmptySymbol."</td>" : "";
					$markTable .= ($showOverallRanking)? "<td align='center' >".$lreportcard->EmptySymbol."</td>" : "";
					$markTable .= ($showSubject)?"<td align='center' >".$lreportcard->EmptySymbol."</td>":"";
				$markTable .= "</tr>";
				
				# use the following ary to count how many empty symbol should be printed
				$NumOfOtherInfo = $showSummaryInfo?sizeof($SummaryInfoFields):0;
				$DisplayedAry = array($NumOfOtherInfo,$showGrandTotal,$showGrandTotalGrade,$showGrandAverage,$showGrandAverageGrade,$showActualAverage,$showActualAverageGrade,$showGPA,$showGPAGrade,$showClassRanking,$showOverallRanking,$NumberOfPassSubject=$showSubject);
				// [2019-0123-1731-14066]
				if($eRCTemplateSetting['Report']['GrandMarksheet']['ShowTotalUnitFailed'] && $showTotalUnitFailed) {
				    $DisplayedAry[] = $showTotalUnitFailed;
				}
				$NumOfEmptySymbol = array_sum($DisplayedAry);
				$EmptySymbolRow = $lreportcard->GrandMarkSheetGenEmptySymbol("HTML",$NumOfEmptySymbol);
				$EmptySymbolRow2 = $lreportcard->GrandMarkSheetGenEmptySymbol("HTML",$NumOfEmptySymbol-4);
				$EmptySymbolRow3 = $lreportcard->GrandMarkSheetGenEmptySymbol("HTML",$showGrandAverageGrade+$showGPA+$showGPAGrade+$showClassRanking+$showOverallRanking+$NumberOfPassSubject);
				
				# Passing Rate
				$markTable .= "<tr>";
					$markTable .= "<td class='tabletext ' align='center' $TitleColSpan><b>".$eReportCard['Template']['PassingRate']."</b></td>";
					for ($i=0; $i<count($markNatureArr); $i++)
					{
						$thisColumnMarksArr = $markNatureArr[$i];
						$thisDisplay = $lreportcard->getPassingRate($thisColumnMarksArr, 2);
						$thisDisplay = ($thisDisplay == "")? $lreportcard->EmptySymbol : $thisDisplay;
						$markTable .= "<td class='tabletext ' align='center'><b>".$thisDisplay."</b></td>";
					}
					$markTable .= $EmptySymbolRow;
				$markTable .= "</tr>";
				
				# Passing Number
				$markTable .= "<tr>";
					$markTable .= "<td class='tabletext ' align='center' $TitleColSpan><b>".$eReportCard['Template']['PassingNumber']."</b></td>";
					for ($i=0; $i<count($markNatureArr); $i++)
					{
						$thisColumnMarksArr = $markNatureArr[$i];
						$thisDisplay = $lreportcard->getPassingNumber($thisColumnMarksArr);
						$thisDisplay = ($thisDisplay == "")? $lreportcard->EmptySymbol : $thisDisplay;
						$markTable .= "<td class='tabletext ' align='center'><b>".$thisDisplay."</b></td>";
					}
					$markTable .= $EmptySymbolRow;
				$markTable .= "</tr>";
				
				# Subjet Weigth
				$markTable .= "<tr>";
					$markTable .= "<td class='tabletext ' align='center' $TitleColSpan><b>".$eReportCard['Template']['SubjectWeight']."</b></td>";
					for ($i=0; $i<count($SubjectWeightArr); $i++)
					{
						$markTable .= "<td class='tabletext ' align='center'><b>".$SubjectWeightArr[$i]."</b></td>";
					}
					$markTable .= $EmptySymbolRow;
				$markTable .= "</tr>";
				
							
				#GrandAverage Pass Number & Fail Number
				if ($showGrandAverage)
				{
					$NatureCount = @array_count_values($GrandAvgNatureArr);
					$PassCount =  $NatureCount['Distinction']+$NatureCount['Pass'];
					$FailCount =  ($NatureCount['Fail']=='')? 0 : $NatureCount['Fail'];
					$markTable .= "<tr>";
						$markTable .= "<td class='tabletext ' align='center' $TitleColSpan><b>".$eReportCard['Template']['GrandAvgPassNumber']."</b></td>";
						for ($i=0; $i<count($SubjectWeightArr)+3; $i++)
						{
							$markTable .= "<td class='tabletext ' align='center'><b>".$lreportcard->EmptySymbol."</b></td>";
						}
						$markTable .= "<td class='tabletext ' align='center'><b>".$PassCount."</b></td>";
						
						$markTable .= $EmptySymbolRow2;
					$markTable .= "</tr>";
					$markTable .= "<tr>";
						$markTable .= "<td class='tabletext ' align='center' $TitleColSpan><b>".$eReportCard['Template']['GrandAvgFailNumber']."</b></td>";
						for ($i=0; $i<count($SubjectWeightArr)+3; $i++)
						{
							$markTable .= "<td class='tabletext ' align='center'><b>".$lreportcard->EmptySymbol."</b></td>";
						}
						$markTable .= "<td class='tabletext ' align='center'><b>".$FailCount."</b></td>";
						$markTable .= $EmptySymbolRow2;
					$markTable .= "</tr>";
				}
				
				#ActualAverage Pass Number & Fail Number
				if ($showActualAverage)
				{
					$numOfEmptyCell = count($SubjectWeightArr) + 3 + $showGrandAverage + $showGrandAverageGrade;
					
					$NatureCount = @array_count_values($ActualAvgNatureArr);
					$PassCount =  $NatureCount['Distinction']+$NatureCount['Pass'];
					$FailCount =  ($NatureCount['Fail']=='')? 0 : $NatureCount['Fail'];
					$markTable .= "<tr>";
						$markTable .= "<td class='tabletext ' align='center' $TitleColSpan><b>".$eReportCard['Template']['ActualAvgPassNumber']."</b></td>";
						for ($i=0; $i<$numOfEmptyCell; $i++)
						{
							$markTable .= "<td class='tabletext ' align='center'><b>".$lreportcard->EmptySymbol."</b></td>";
						}
						$markTable .= "<td class='tabletext ' align='center'><b>".$PassCount."</b></td>";
						
						$markTable .= $EmptySymbolRow3;
					$markTable .= "</tr>";
					$markTable .= "<tr>";
						$markTable .= "<td class='tabletext ' align='center' $TitleColSpan><b>".$eReportCard['Template']['ActualAvgFailNumber']."</b></td>";
						for ($i=0; $i<$numOfEmptyCell; $i++)
						{
							$markTable .= "<td class='tabletext ' align='center'><b>".$lreportcard->EmptySymbol."</b></td>";
						}
						$markTable .= "<td class='tabletext ' align='center'><b>".$FailCount."</b></td>";
						$markTable .= $EmptySymbolRow3;
					$markTable .= "</tr>";
				}
			}
			$markTable .= "</tbody>";
			$markTable .= "</table>";
			
			// Title of the grandmarksheet
			//$schoolData = split("\n",get_file_content($PATH_WRT_ROOT."file/school_data.txt"));
			$schoolData = explode("\n", get_file_content($PATH_WRT_ROOT."file/school_data.txt"));
			$schoolName = $schoolData[0];
			$academicYear = $lreportcard->GET_ACTIVE_YEAR("-");
			
			$GMSTitle = "$schoolName<br />$academicYear $SemesterTitle $ColumnTitle $SubjectName ".$eReportCard['Reports_GrandMarksheet']."<br />".$eReportCard['GrandMarksheetTypeOption'][0];
			$GMSDetail = "<b>".$eReportCard["FormName"]."</b> : $ClassLevelName&nbsp;&nbsp;<b>".$eReportCard["Class"]."</b> : $ClassName";
			if ($ForSSPA)
			{
				$GMSTitle .= " (SSPA)";
				
				$markTable .= "<table width='100%' border='0' cellpadding='2' cellspacing='0'>";
				$markTable .= "<tr><td>&nbsp;</td></tr>";
				$markTable .= "<tr><td class='footer'>";
				$markTable .= $eReportCard['Template']['SSPA_Footer'];
				$markTable .= "</td></tr>";
				$markTable .= "<tr><td class='signature' align='right'>";
				$markTable .= $eReportCard['Template']['SSPA_Signature'];
				$markTable .= "</td></tr>";
				$markTable .= "</table>";
			}
			
			############## Interface Start ##############
			
			if($ClassID != end($ClassIDArr))
				$page_break = 'style="page-break-after:always;"';
			else // no page break for th last class
				$page_break = '';		
	?>
<html>
    <head>
        <meta http-equiv='pragma' content='no-cache' />
        <meta http-equiv='content-type' content='text/html; charset=utf-8' />
    </head>

	<style type="text/css">
	<!--
	h1 {
		font-size: 20px;
		font-family: arial, "lucida console", sans-serif;
		margin: 0px;
		padding-top: 3px;
		padding-bottom: 10px;
	}

	#MarkTable {
		border-collapse: collapse;
		border-color: #000000;
	}

	#MarkTable thead {
		font-size: 12px;
		font-family: arial, "lucida console", sans-serif;
	}

	#MarkTable tbody {
		font-size: 12px;
		font-family: arial, "lucida console", sans-serif;
	}

	.footer {
		font-family: "Arial", "Lucida Console";
		font-size: 12px;
	}
	.signature {
		font-family: "Arial", "Lucida Console";
		font-size: 14px;
	}
	.signature {
		font-family: "Arial", "Lucida Console";
		font-size: 14px;
	}

	.stat_start_border_top {
		border-top-width: 2px;
		border-top-style: solid;
		border-top-color: #000000;
	}
	-->
	</style>

	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0" <?=$page_break?>>
		<tr>
			<td align="center"><h1><?= $GMSTitle ?></h1></td>
		</tr>
		<tr>
			<td align="left"><?= $GMSDetail ?></td>
		</tr>
		<tr>
			<td><?= $markTable ?></td>
		</tr>
		<tr>
			<td>
				<span class="tabletextrequire">*</span><?=$eReportCard['StatNotShown']?>
			</td>
		</tr>

        <?php if($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayPrintReportDateTime']) { ?>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td align="right">
                    <?php echo $eReportCard['MasterReport']['PrintDate'] . ":&nbsp;" . date('j/n/Y H:i'); ?>
                </td>
            </tr>
        <?php } ?>
	</table>
	<br />

    <?  } ?>
</html>

<?
	}
	else
    {
?>
You have no priviledge to access this page.
<?
	}
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>