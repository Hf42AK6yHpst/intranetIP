<?php
// using : 

@SET_TIME_LIMIT(1000);
@ini_set(memory_limit, "800M");

/**********************************************
 * modification log
 *      20200512 Bill:
 *          support Print Date export  ($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayPrintReportDateTime'])
 *      20200416 Bill:  [2020-0415-1414-56164]
 *          handle if other info data is array
 *      20190605 Bill:  [2019-0123-1731-14066]
 *          added Total Unit(s) Failed column   ($eRCTemplateSetting['Report']['GrandMarksheet']['ShowTotalUnitFailed'])
 *          always display marks according to decimal places settings  ($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayRoundMarks'])
 *      20180628 Bill:  [2017-1204-1601-38164]
 *          set target active year  ($eRCTemplateSetting['Report']['SupportAllActiveYear'])
 * 		20170801 Bill:	[2016-1214-1548-50240]
 * 			Support Grand Average Grade Adjustment	($eRCTemplateSetting['GrandAverageGradeManualAdjustment'])
 * 		20170717 Bill:	[2017-0706-1012-27236]
 * 			Grand Total, Grand Averag & GPA : replace -1 by "-" 
 * 			Statistics - Lowest Mark: remove -1 from Minimum Score Calculation 
 * 		20170524 Bill:
 * 			set time and memory limit, prevent no response
 * 		20170111 Bill:	[DM#3136]
 * 			fixed: PHP 5.4 error split()
 * 		20161213 Bill:
 * 			use grade instead of mark for special case checking	[2016-1130-1208-41236]
 * 		20150402 Bill:
 * 			add display of User Login [2015-0306-1036-36206]
 * 		20100416 Marcus:
 * 			modified ranking - if ordermeritstream exist, show it instead of ordermeritform 
 * 		20100212 Ivan:
 * 			fixed: display N.A. as zero mark 
 * 		20100209 Marcus:
 * 			modify coding to cater conversion from grand marks to grade 
 * 		20100120 Marcus:
 * 			modify coding to cater multi selection of subjects 
 * 		20100107 Marcus:
 * 			add Display Subject to select Abbr/Desc/ShortName of Subject Name
 * 		20100105 Marcus:
 * 			add display of Gender and GPA
 * ********************************************/

$PATH_WRT_ROOT = "../../../../../../";

// Page access right
$PageRight = "TEACHER";

// Include general libraries
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

// Authorize intranet user & connect Database
intranet_auth();
intranet_opendb();

// Check if ReportCard module is enable
if ($plugin['ReportCard2008']) {
	// Include ReportCard libraries
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	}
	else {
		$lreportcard = new libreportcard();
	}
	
	// [2017-1204-1601-38164] Set target active year
	if($eRCTemplateSetting['Report']['SupportAllActiveYear'])
	{
	    $targetActiveYear = stripslashes($_REQUEST['targetActiveYear']);
	    if(!empty($targetActiveYear)){
	        $targetYearID = $lreportcard->SET_TEMP_ACTIVE_YEAR($targetActiveYear);
	    }
	}
	
	#$linterface = new interface_html();
	$lclass = new libclass();
	
	// Check module access right
	if ($lreportcard->hasAccessRight()) {
		// Check required data submitted from index.php
		// optional: $SubjectID, $ReportColumnID
		if (!isset($ClassLevelID, $ClassID, $ReportID) || empty($ClassLevelID) || empty($ReportID) || empty($ClassID)) {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		$exportColumn = array();
		$ExportArr = array();
		$lexport = new libexporttext();
		
		# get display options
		// 1. student name display (EnglishName, ChineseName)
		$showEnglishName = false;
		$showChineseName = false;
		if ($StudentNameDisplay == NULL)
		{
			$numOfNameColumn = 0;
		}
		else
		{
			$StudentNameDisplay = explode(",", $StudentNameDisplay);
			$numOfNameColumn = sizeof($StudentNameDisplay);
			foreach ($StudentNameDisplay as $key => $value)
			{
				if ($value == "EnglishName") $showEnglishName = true;
				if ($value == "ChineseName") $showChineseName = true;
			}
		}		
		// 2. ranking display (ClassRanking, OverallRanking)
		$showClassRanking = false;
		$showOverallRanking = false;
		$RankingDisplay = explode(",", $RankingDisplay);
		foreach ($RankingDisplay as $key => $value)
		{
			if ($value == "ClassRanking") $showClassRanking = true;
			if ($value == "OverallRanking") $showOverallRanking = true;
		}
    	// 3.1 show subject (0,1)
		$showSubject = $ShowSubject;		
		// 3.2 show subject component (0,1)
		$showSubjectComponent = $ShowSubjectComponent;
		// 4. show Summary Info (0,1)
		$showSummaryInfo = $ShowSummaryInfo;		
		// 5. show Grand Total (0,1)
		$showGrandTotal = $ShowGrandTotal;		
		// 6. show Grand Average (0,1)
		$showGrandAverage = $ShowGrandAverage;
		// 7. ForSSPA => no decimal place in all marks
		$ForSSPA = $ForSSPA;
		// 8. show Statistics (0,1)
		$showStatistics = $ShowStatistics;
		// 9. show GPA (0,1)
		$showGPA = $ShowGPA;
		// 10. show Gender (0,1)
		$showGender = $ShowGender;
		// 11. show User Login (0,1) [2015-0306-1036-36206]
		$showUserLogin = $ShowUserLogin;
		// 12. show Total Unit(s) Failed (0,1)
		$showTotalUnitFailed = ($_GET['ShowTotalUnitFailed'])? 1 : 0;
		
		# GrandMarks Grade
		$showGrandTotalGrade = $ShowGrandTotalGrade;
		$showGrandAverageGrade= $ShowGrandAverageGrade;
		$showGPAGrade= $ShowGPAGrade;
		$showActualAverage = ($_GET['ShowActualAverage'] && empty($ReportColumnID))? 1 : 0;
		$showActualAverageGrade = ($_GET['ShowActualAverageGrade'] && empty($ReportColumnID))? 1 : 0;
		
		// SubjectID has 2 Condition
		// 1.no of subject selected >1 , show overall rank
		// 2.no of subject selected ==1 , show rank of specific subject
		$SelectedSubjectID = $SubjectID;
		if(count($SubjectID)==1)
			$SubjectID = $SubjectID[0];
		else
			unset($SubjectID);
		
		$reportCalculationOrder = $lreportcard->returnReportTemplateCalculation($ReportID);
		
		// Get ReportCard info
		$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		$SemID = $reportTemplateInfo['Semester'];
		if ($SemID != "F") {
		    $SemesterNumber = $lreportcard->Get_Semester_Seq_Number($SemID);
		}
		
		// Check to make sure it is a generated report
		if ($reportTemplateInfo["LastGenerated"] == "0000-00-00 00:00:00" || $reportTemplateInfo["LastGenerated"] == "") {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		// Get subjects of the current ClassLevel
		$FormSubjectArr = $lreportcard->returnSubjectwOrder($ClassLevelID, 1, '', $intranet_session_language,$SubjDisplay);
		
		$CmpSubjectIDList = array();
		$ParentSubjectIDList = array();
		$ParentCmpSubjectNum = array();
		
		// Reformat the subject array - Display component subjects also
		$FormSubjectArrFlat = array();
		if (sizeof($FormSubjectArr) > 0) {
			foreach($FormSubjectArr as $tmpSubjectID => $tmpSubjectName) {
				if (sizeof($tmpSubjectName) > 1) {
					$ParentSubjectIDList[] = $tmpSubjectID;
					$ParentCmpSubjectNum[$tmpSubjectID] = sizeof($tmpSubjectName) - 1;
					foreach ($tmpSubjectName as $tmpCmpSubjectID => $tmpCmpSubjectName) {
						if ($tmpCmpSubjectID != 0) {
							$CmpSubjectIDList[] = $tmpCmpSubjectID;
							$FormSubjectArrFlat[$tmpCmpSubjectID] = array(0 => $tmpCmpSubjectName);
						}
					}
				}
				$FormSubjectArrFlat[$tmpSubjectID] = array(0 => $tmpSubjectName[0]);
			}
		}
		$FormSubjectArr = $FormSubjectArrFlat;
		
		// Get component subjects if SubjectID is provided
		if (isset($SubjectID) && $SubjectID !== "" && in_array($SubjectID, $ParentSubjectIDList)) {
			$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID,$intranet_session_language, $SubjDisplay);
			$CmpSubjectIDNameArr = array();
			$CmpSubjectIDArr = array();
			for($i=0; $i<sizeof($CmpSubjectArr); $i++) {
				$CmpSubjectIDArr[] = $CmpSubjectArr[$i]["SubjectID"];
				$CmpSubjectIDNameArr[$CmpSubjectArr[$i]["SubjectID"]] = $CmpSubjectArr[$i]["SubjectName"];
			}
		}
		
		// Get subjects' grading scheme of the current ClassLevel
		$FormSubjectGradingSchemeArr = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID,0,$ReportID);
		
		// Get ClassLevel name
		$ClassLevelName = $lreportcard->returnClassLevel($ClassLevelID);
		
		// Get Classes
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		
		// Get current school year
		$SchoolYear = $lreportcard->schoolYear;
		
		// Get columns info of the current ReportCard
		$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
		$ColumnIDList = array();
		$ColumnIDList = ($ColumnTitleAry == NULL)? NULL : array_keys($ColumnTitleAry);
		
		$studentIDList = array();
		$studentIDNameMap = array();
		$studentIDRegNoMap = array();
		
		// Get the list of students (in entire class level)
		$studentList = array();
		$studentIDList = array();
		$studentIDNameMap = array();
		$studentIDClassMap = array();
		$studentIDClassNumMap = array();
		$studentIDRegNoMap = array();
		$studentUserLoginMap = array();
		$studentFailUnitArr = array();
		$ClassNameArr = array();
		for($i=0; $i<sizeof($ClassArr); $i++) {
			$ClassNameArr[] = $ClassArr[$i]["ClassName"];
			$ClassIDArr[] = $ClassArr[$i]["ClassID"];
			$studentList = $lreportcard->GET_STUDENT_BY_CLASS($ClassArr[$i]["ClassID"], "", 1);
			for($j=0; $j<sizeof($studentList); $j++) {
				$studentIDNameMap[$studentList[$j]["UserID"]]['En'] = $studentList[$j]['StudentNameEn'];
				$studentIDNameMap[$studentList[$j]["UserID"]]['Ch'] = $studentList[$j]['StudentNameCh'];
			
				$studentIDClassNumMap[$studentList[$j]["UserID"]] = $studentList[$j][2];
				$studentIDClassMap[$studentList[$j]["UserID"]] = $ClassArr[$i]["ClassName"];
				$studentIDRegNoMap[$studentList[$j]["UserID"]] = $studentList[$j]["WebSAMSRegNo"];
				$studentUserLoginMap[$studentList[$j]["UserID"]] = $studentList[$j]["UserLogin"];
				$studentIDList[] = $studentList[$j]["UserID"];
				
				// [2019-0123-1731-14066] Get Student Total Unit(s) Failed
				if($eRCTemplateSetting['Report']['GrandMarksheet']['ShowTotalUnitFailed'] && $showTotalUnitFailed) {
				    $studentFailUnitInfo = $lreportcard->returnTemplateSubjectCol($ReportID, $ClassLevelID, $studentList[$j]["UserID"]);
				    $studentFailUnitInfo = $studentFailUnitInfo['FailUnitArr'];
				    $studentFailUnitArr[$studentList[$j]["UserID"]] = $SemesterNumber > 0? $studentFailUnitInfo[$SemesterNumber - 1] : $studentFailUnitInfo['Final'];
				}
			}
		}
		
		// Students should be already sorted by Class Number in the SQL Query
		$studentIDListSql = implode(",", $studentIDList);
		$cond = "";
		
		// Get overall mark or term/assessment mark
		$ColumnTitle = "";
		if (!isset($ReportColumnID) || empty($ReportColumnID)) {
			$ColumnTitle = $eReportCard['Template']['OverallCombined'];
			//$cond .= "AND (ReportColumnID = '' OR ReportColumnID = '0') ";
			$cond .= "AND (a.ReportColumnID = '' OR a.ReportColumnID = '0') ";
			$reportColumnID = 0;
		}
		else {
			$ColumnTitle = $ColumnTitleAry[$ReportColumnID];
			//$cond .= "AND ReportColumnID = '$ReportColumnID' ";
			$cond .= "AND a.ReportColumnID = '$ReportColumnID' ";
			$reportColumnID = $ReportColumnID;
		}
		
		// Get individual subject mark or grand average (which table to retrieve)
		$SubjectName = "";
		if (!isset($SubjectID) || empty($SubjectID)) {
			// No SubjectID provided, get the averaage result
			
			# modified for manual adjustment
			/*$table = $lreportcard->DBName.".RC_REPORT_RESULT";
			$fieldToSelect = "StudentID, GrandTotal, GrandAverage, OrderMeritClass, OrderMeritForm";
			
			$sql = "SELECT $fieldToSelect FROM $table WHERE ";
			$sql .= "ReportID = '$ReportID' ";
			$sql .= "AND StudentID IN ($studentIDListSql) ";
			$sql .= $cond;
			$sql .= "ORDER BY OrderMeritForm";
			*/
			$sql = "
				SELECT 
					a.StudentID, 
					a.GrandTotal, 
					a.GrandAverage,
					a.ActualAverage,
					a.GPA, 
					a.OrderMeritClass, 
					IF(a.OrderMeritStream IS NULL OR TRIM(a.OrderMeritStream) = '',IFNULL(b.AdjustedValue,a.OrderMeritForm),a.OrderMeritStream) as OrderMeritForm
				FROM 
					".$lreportcard->DBName.".RC_REPORT_RESULT a
					LEFT JOIN ".$lreportcard->DBName.".RC_MANUAL_ADJUSTMENT b ON a.ReportID = b.ReportID AND a.StudentID = b.StudentID AND a.ReportColumnID = b.ReportColumnID AND b.OtherInfoName = 'OrderMeritForm' AND b.SubjectID = 0
				WHERE 
					a.ReportID = '$ReportID' 
					AND a.StudentID IN ($studentIDListSql) 
					$cond
				ORDER BY 
					Round(OrderMeritForm)
			";
			$tempArr = $lreportcard->returnArray($sql);
			$resultAverage = array();
			$studentIDListSorted = $studentIDList;
			if (sizeof($tempArr) > 0) {
				$studentIDListSorted = array();
				for($j=0; $j<sizeof($tempArr); $j++)
				{
					# for manual adjustment
					$ManualAjustedMark = $lreportcard->Get_Manual_Adjustment($ReportID, $tempArr[$j]["StudentID"], $ReportColumnID, 0);
					$thisGrandMark = $ManualAjustedMark[$tempArr[$j]["StudentID"]][$reportColumnID][0];
					
					$studentIDListSorted[] = $tempArr[$j]["StudentID"];
					$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = $thisGrandMark["GrandTotal"]?$thisGrandMark["GrandTotal"]:$tempArr[$j]["GrandTotal"];
					$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = $thisGrandMark["GrandAverage"]?$thisGrandMark["GrandAverage"]:$tempArr[$j]["GrandAverage"];
					$resultAverage[$tempArr[$j]["StudentID"]]["ActualAverage"] = $thisGrandMark["ActualAverage"]?$thisGrandMark["ActualAverage"]:$tempArr[$j]["ActualAverage"];
					$resultAverage[$tempArr[$j]["StudentID"]]["GPA"] = $thisGrandMark["GPA"]?$thisGrandMark["GPA"]:$tempArr[$j]["GPA"];
					$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverageGrade"] = $thisGrandMark["GrandAverageGrade"]?$thisGrandMark["GrandAverageGrade"]:"";
						
					# For Term Report Average Grade Adjustment
					if($eRCTemplateSetting['GrandAverageGradeManualAdjustment'] && $SemID == "F" && $ReportColumnID > 0) {
						$TermReportID = $lreportcard->getCorespondingTermReportID($ReportID, $ReportColumnID);
						if(!empty($TermReportID) && $TermReportID != $ReportID) {
							$TermManualAjustedMark = $lreportcard->Get_Manual_Adjustment($TermReportID, $tempArr[$j]["StudentID"], 0, 0);
							$thisTermGrandMark = $TermManualAjustedMark[$tempArr[$j]["StudentID"]][0][0];
							$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverageGrade"] = $thisTermGrandMark["GrandAverageGrade"]?$thisTermGrandMark["GrandAverageGrade"]:$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverageGrade"];
						}
					}
					
					# Round up all the marks if for SSPA
					if ($ForSSPA)
					{
						$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = round($thisGrandMark["GrandTotal"]?$thisGrandMark["GrandTotal"]:$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"]);
						$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = round($thisGrandMark["GrandAverage"]?$thisGrandMark["GrandAverage"]:$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"]);
						$resultAverage[$tempArr[$j]["StudentID"]]["ActualAverage"] = round($thisGrandMark["ActualAverage"]?$thisGrandMark["ActualAverage"]:$resultAverage[$tempArr[$j]["StudentID"]]["ActualAverage"]);
						$resultAverage[$tempArr[$j]["StudentID"]]["GPA"] = round($thisGrandMark["GPA"]?$thisGrandMark["GPA"]:$resultAverage[$tempArr[$j]["StudentID"]]["GPA"]);
					}
					
					// [2019-0123-1731-14066] add rounding logic
					if($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayRoundMarks'])
					{
					    if(is_numeric($resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"]) && $resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] != -1) {
					        $resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = $lreportcard->ROUND_MARK($resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"], "GrandTotal");
					    }
					    if(is_numeric($resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"]) && $resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] != -1) {
					        $resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = $lreportcard->ROUND_MARK($resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"], "GrandAverage");
					    }
					    if(is_numeric($resultAverage[$tempArr[$j]["StudentID"]]["GPA"]) && $resultAverage[$tempArr[$j]["StudentID"]]["GPA"] != -1) {
					        $resultAverage[$tempArr[$j]["StudentID"]]["GPA"] = $lreportcard->ROUND_MARK($resultAverage[$tempArr[$j]["StudentID"]]["GPA"], "GrandAverage");
					    }
					}
					
					# SubjectID = -1 => GrandAverage
					$GrandAvgNatureArr[$tempArr[$j]["StudentID"]] = $lreportcard->returnMarkNature($ClassLevelID,-1,$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"],'','', $ReportID);
					$ActualAvgNatureArr[$tempArr[$j]["StudentID"]] = $lreportcard->returnMarkNature($ClassLevelID,-1,$resultAverage[$tempArr[$j]["StudentID"]]["ActualAverage"],'','', $ReportID);
					
					if ($tempArr[$j]["OrderMeritClass"] > 0 || $thisGrandMark["OrderMeritClass"]) {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritClass"] = $thisGrandMark["OrderMeritClass"]?$thisGrandMark["OrderMeritClass"]:$tempArr[$j]["OrderMeritClass"];
					}
					else {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritClass"] = "-";
					}
					
					if ($tempArr[$j]["OrderMeritForm"] > 0 || $thisGrandMark["OrderMeritForm"]) {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritForm"] = $thisGrandMark["OrderMeritForm"]?$thisGrandMark["OrderMeritForm"]:$tempArr[$j]["OrderMeritForm"];
					}
					else {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritForm"] = "-";
					}
				}
			}
			
			// Get all subject marks as additional information
			foreach($FormSubjectArr as $tmpSubjectID => $tmpSubjectName) {
				$subjectGradingScheme = $FormSubjectGradingSchemeArr[$tmpSubjectID];
				$subjectGradingSchemeInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($subjectGradingScheme["schemeID"]);
				
				/*
				if ($subjectGradingScheme["scaleDisplay"] == "M") {
					$fieldToSelect = "StudentID, Mark As MarkUsed";
				} else {
					$fieldToSelect = "StudentID, Grade As MarkUsed";
				}
				$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE";
				
				$sql = "SELECT $fieldToSelect FROM $table WHERE ";
				$sql .= "ReportID = '$ReportID' ";
				$sql .= "AND StudentID IN ($studentIDListSql) ";
				$sql .= "AND  SubjectID = '$tmpSubjectID' ";
				$sql .= $cond;
				*/
				
				if ($subjectGradingScheme["scaleDisplay"] == "M") {
					$fieldToSelect = "a.StudentID, IFNULL(b.AdjustedValue,a.Mark) As MarkUsed, IFNULL(b.AdjustedValue,a.Grade)as Grade";
				}
				else {
					$fieldToSelect = "a.StudentID, IFNULL(b.AdjustedValue,a.Grade) As MarkUsed, IFNULL(b.AdjustedValue,a.Grade)as Grade";
				}
				$sql = "
					SELECT 
						$fieldToSelect 
					FROM 
						".$lreportcard->DBName.".RC_REPORT_RESULT_SCORE a
						LEFT JOIN ".$lreportcard->DBName.".RC_MANUAL_ADJUSTMENT b 
							ON 
								a.ReportID = b.ReportID 
								AND a.StudentID = b.StudentID 
								AND a.ReportColumnID = b.ReportColumnID 
								AND a.SubjectID = b.SubjectID 
								AND b.OtherInfoName = 'Score'
						
					WHERE 
						a.ReportID = '$ReportID' 
						AND a.StudentID IN ($studentIDListSql) 
						AND a.SubjectID = '$tmpSubjectID' 
						$cond
					";
				
				$tempArr = $lreportcard->returnArray($sql);
				
				$resultSubject[$tmpSubjectID] = array();
				$resultSubjectGrade[$tmpSubjectID] = array();
				if (sizeof($tempArr) > 0) {
					for($j=0; $j<sizeof($tempArr); $j++)
					{
						$thisMark = $tempArr[$j]["MarkUsed"];
						$thisGrade = $tempArr[$j]["Grade"];
						list($thisMark, $needStyle) = $lreportcard->checkSpCase($ReportID, $tmpSubjectID, $thisMark, $thisGrade);
						
						$resultSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = $thisMark;
						$resultSubjectGrade[$tmpSubjectID][$tempArr[$j]["StudentID"]] = $thisGrade;
						
						//$resultSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = $tempArr[$j]["MarkUsed"];
						
						# Round up all the marks if for SSPA
						if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M")
						{
							$resultSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = round($resultSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]]);
						}
					}
				}
			}
		}
		else {
			$subjectGradingScheme = $FormSubjectGradingSchemeArr[$SubjectID];
			$subjectGradingSchemeInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($subjectGradingScheme["schemeID"]);
			
			/*if ($subjectGradingScheme["scaleDisplay"] == "M") {
				$fieldToSelect = "StudentID, OrderMeritClass, OrderMeritForm, Mark As MarkUsed";
			} else {
				$fieldToSelect = "StudentID, OrderMeritClass, OrderMeritForm, Grade As MarkUsed";
			}
			$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE";
			
			$sql = "SELECT $fieldToSelect FROM $table WHERE ";
			$sql .= "ReportID = '$ReportID' ";
			$sql .= "AND StudentID IN ($studentIDListSql) ";
			$sql .= "AND SubjectID = '$SubjectID' ";
			*/
			if ($subjectGradingScheme["scaleDisplay"] == "M") {
				$fieldToSelect = "a.StudentID, a.OrderMeritClass, IF(a.OrderMeritStream IS NULL OR TRIM(a.OrderMeritStream) = '',IFNULL(b.AdjustedValue,a.OrderMeritForm),a.OrderMeritStream) as OrderMeritForm, a.Mark As MarkUsed, IFNULL(b.AdjustedValue,a.Grade)as Grade";
			}
			else {
				$fieldToSelect = "a.StudentID, a.OrderMeritClass, IF(a.OrderMeritStream IS NULL OR TRIM(a.OrderMeritStream) = '',IFNULL(b.AdjustedValue,a.OrderMeritForm),a.OrderMeritStream) as OrderMeritForm, a.Grade As MarkUsed, IFNULL(b.AdjustedValue,a.Grade)as Grade";
			}
			$sql = "
				SELECT 
					$fieldToSelect
				FROM 
					".$lreportcard->DBName.".RC_REPORT_RESULT_SCORE a 
					LEFT JOIN ".$lreportcard->DBName.".RC_MANUAL_ADJUSTMENT b ON 
						a.ReportID = b.ReportID 
						AND a.StudentID = b.StudentID 
						AND a.ReportColumnID = b.ReportColumnID 
						AND a.SubjectID = b.SubjectID 
						AND b.OtherInfoName = 'OrderMeritForm'
				WHERE 
					a.ReportID = '$ReportID' 
					AND a.StudentID IN ($studentIDListSql) 
					AND a.SubjectID = '$SubjectID'
			";
			$tempArr = $lreportcard->returnArray($sql.$cond."ORDER BY round(OrderMeritForm)");
			
			$resultSubject = array();
			$resultSubjectGrade = array();
			$studentIDListSorted = array();
			$OrderFormSubject = array();
			if (sizeof($tempArr) > 0) {
				for($j=0; $j<sizeof($tempArr); $j++)
				{
					$studentIDListSorted[] = $tempArr[$j]["StudentID"];
					
					# for manual adjustment
					$ManualAjustedMark = $lreportcard->Get_Manual_Adjustment($ReportID, $tempArr[$j]["StudentID"], $ReportColumnID, $SubjectID);
					$thisSubjectOrder = $ManualAjustedMark[$tempArr[$j]["StudentID"]][$reportColumnID][$SubjectID];
					
					$thisMark = $tempArr[$j]["MarkUsed"];
					$thisGrade = $tempArr[$j]["Grade"];
					list($thisMark, $needStyle) = $lreportcard->checkSpCase($ReportID, $SubjectID, $thisMark, $thisGrade);
					
					$resultSubject[$tempArr[$j]["StudentID"]] = $thisSubjectOrder["Score"]? $thisSubjectOrder["Score"] : $thisMark;
					$resultSubjectGrade[$tempArr[$j]["StudentID"]] = $thisSubjectOrder["Score"]? $thisSubjectOrder["Score"] : $thisGrade;
					
					# Round up all the marks if for SSPA
					if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M")
					{
						$resultSubject[$tempArr[$j]["StudentID"]] = round($thisSubjectOrder["Score"]?$thisSubjectOrder["Score"]:$resultSubject[$tempArr[$j]["StudentID"]]);
					}
					
					if ($tempArr[$j]["OrderMeritClass"] > 0 || $thisSubjectOrder["OrderMeritClass"]) {
						$OrderClassSubject[$tempArr[$j]["StudentID"]] = $thisSubjectOrder["OrderMeritClass"]?$thisSubjectOrder["OrderMeritClass"]:$tempArr[$j]["OrderMeritClass"];
					}
					else {
						$OrderClassSubject[$tempArr[$j]["StudentID"]] = "-";
					}
					if ($tempArr[$j]["OrderMeritForm"] > 0 || $thisSubjectOrder["OrderMeritForm"]) {
						$OrderFormSubject[$tempArr[$j]["StudentID"]] = $thisSubjectOrder["OrderMeritForm"]?$thisSubjectOrder["OrderMeritForm"]:$tempArr[$j]["OrderMeritForm"];
					}
					else {
						$OrderFormSubject[$tempArr[$j]["StudentID"]] = "-";
					}
				}
			}
			
			// If gettng subject overall mark for a single subject, get the column mark also
			if (!isset($ReportColumnID) || empty($ReportColumnID)) {
				for($i=0; $i<sizeof($ColumnIDList); $i++)
				{
					$tmpCond = "AND a.ReportColumnID = '".$ColumnIDList[$i]."'";
					$tempArr = $lreportcard->returnArray($sql.$tmpCond);
										
					# for manual adjustment
					$ManualAjustedMark = $lreportcard->Get_Manual_Adjustment($ReportID, "", $ColumnIDList[$i], $SubjectID);
					
					$resultSubjectColumn[$ColumnIDList[$i]] = array();
					if (sizeof($tempArr) > 0) {
						for($j=0; $j<sizeof($tempArr); $j++)
						{
							$thisSubjectMark = $ManualAjustedMark[$tempArr[$j]["StudentID"]][$ColumnIDList[$i]][$SubjectID];
							
							$thisMark = $tempArr[$j]["MarkUsed"];
							$thisGrade = $tempArr[$j]["Grade"];
							list($thisMark, $needStyle) = $lreportcard->checkSpCase($ReportID, $SubjectID, $thisMark, $thisGrade);
					
							$resultSubjectColumn[$ColumnIDList[$i]][$tempArr[$j]["StudentID"]] = $thisSubjectMark["Score"]?$thisSubjectMark["Score"]:$thisMark;
							
							# Round up all the marks if for SSPA
							if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M")
							{
								$resultSubjectColumn[$ColumnIDList[$i]][$tempArr[$j]["StudentID"]] = round($thisSubjectMark["Score"]?$thisSubjectMark["Score"]:$resultSubjectColumn[$ColumnIDList[$i]][$tempArr[$j]["StudentID"]]);
							}
						}
					}
				}
			}
			
			#################################
			// Special handling of subject which having component subjects
			if (in_array($SubjectID, $ParentSubjectIDList)) {
				// Push the SubjectID into the same array of its component subjects
				$SubjectPlusCompSubjectIDList = $CmpSubjectIDArr;
				$SubjectPlusCompSubjectIDList[] = $SubjectID;
				
				for($i=0; $i<sizeof($SubjectPlusCompSubjectIDList); $i++)
				{
					$tmpSubjectID = $SubjectPlusCompSubjectIDList[$i];
					$subjectGradingScheme = $FormSubjectGradingSchemeArr[$tmpSubjectID];
					$subjectGradingSchemeInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($subjectGradingScheme["schemeID"]);
					
					/*if ($subjectGradingScheme["scaleDisplay"] == "M") {
						$fieldToSelect = "StudentID, Mark As MarkUsed";
					} else {
						$fieldToSelect = "StudentID, Grade As MarkUsed";
					}
					$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE";
					
					$sql = "SELECT $fieldToSelect FROM $table WHERE ";
					$sql .= "ReportID = '$ReportID' ";
					$sql .= "AND StudentID IN ($studentIDListSql) ";
					$sql .= "AND  SubjectID = '$tmpSubjectID' ";
					$sql .= $cond;*/

					if ($subjectGradingScheme["scaleDisplay"] == "M") {
						$fieldToSelect = "a.StudentID, IFNULL(b.AdjustedValue,a.Mark) As MarkUsed, IFNULL(b.AdjustedValue,a.Grade)as Grade";
					}
					else {
						$fieldToSelect = "a.StudentID, IFNULL(b.AdjustedValue,a.Grade) As MarkUsed, IFNULL(b.AdjustedValue,a.Grade)as Grade";
					}
					$sql = "
						SELECT 
							$fieldToSelect 
						FROM 
							".$lreportcard->DBName.".RC_REPORT_RESULT_SCORE a
							LEFT JOIN ".$lreportcard->DBName.".RC_MANUAL_ADJUSTMENT b 
								ON 
									a.ReportID = b.ReportID 
									AND a.StudentID = b.StudentID 
									AND a.ReportColumnID = b.ReportColumnID 
									AND a.SubjectID = b.SubjectID 
									AND b.OtherInfoName = 'Score'
						WHERE 
							a.ReportID = '$ReportID' 
							AND a.StudentID IN ($studentIDListSql) 
							AND a.SubjectID = '$tmpSubjectID' 
							$cond
						";
					$tempArr = $lreportcard->returnArray($sql);

					$resultDetailSubject[$tmpSubjectID] = array();
					$resultDetailSubjectGrade[$tmpSubjectID] = array();
					if (sizeof($tempArr) > 0) {
						for($j=0; $j<sizeof($tempArr); $j++)
						{
							$thisMark = $tempArr[$j]["MarkUsed"];
							$thisGrade = $tempArr[$j]["Grade"];
							list($thisMark, $needStyle) = $lreportcard->checkSpCase($ReportID, $tmpSubjectID, $thisMark, $thisGrade);
							
							$resultDetailSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = $thisMark;
							$resultDetailSubjectGrade[$tmpSubjectID][$tempArr[$j]["StudentID"]] = $thisGrade;
							
							//$resultDetailSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = $tempArr[$j]["MarkUsed"];
							
							# Round up all the marks if for SSPA
							if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M")
							{
								$resultDetailSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = round($resultDetailSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]]);
							}
						}
					}
				}
			}
			
			#################################
			# get Grand Total and Grand Average
			$table = $lreportcard->DBName.".RC_REPORT_RESULT";
			$fieldToSelect = "StudentID, GrandTotal, GrandAverage, ActualAverage, GPA, OrderMeritClass, IF(OrderMeritStream IS NULL OR TRIM(OrderMeritStream) = '',OrderMeritForm,a.OrderMeritStream)";
			
			$sql = "SELECT $fieldToSelect FROM $table a WHERE ";
			$sql .= "ReportID = '$ReportID' ";
			$sql .= "AND StudentID IN ($studentIDListSql) ";
			$sql .= $cond;
			$tempArr = $lreportcard->returnArray($sql);
			
			$resultAverage = array();

			if (sizeof($tempArr) > 0) {
				for($j=0; $j<sizeof($tempArr); $j++)
				{
					# for manual adjustment
					$ManualAjustedMark = $lreportcard->Get_Manual_Adjustment($ReportID, $tempArr[$j]["StudentID"], $ReportColumnID, 0);
					$thisGrandMark = $ManualAjustedMark[$tempArr[$j]["StudentID"]][$reportColumnID][0];
					
					$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = $thisGrandMark["GrandTotal"]?$thisGrandMark["GrandTotal"]:$tempArr[$j]["GrandTotal"];
					$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = sprintf("%02.2f", $thisGrandMark["GrandAverage"]?$thisGrandMark["GrandAverage"]:$tempArr[$j]["GrandAverage"]);
					$resultAverage[$tempArr[$j]["StudentID"]]["ActualAverage"] = sprintf("%02.2f", $thisGrandMark["ActualAverage"]?$thisGrandMark["ActualAverage"]:$tempArr[$j]["ActualAverage"]);
					$resultAverage[$tempArr[$j]["StudentID"]]["GPA"] = sprintf("%02.2f", $thisGrandMark["GPA"]?$thisGrandMark["GPA"]:$tempArr[$j]["GPA"]);
					$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverageGrade"] = $thisGrandMark["GrandAverageGrade"]?$thisGrandMark["GrandAverageGrade"]:"";
						
					# For Term Report Average Grade Adjustment
					if($eRCTemplateSetting['GrandAverageGradeManualAdjustment'] && $SemID == "F" && $ReportColumnID > 0) {
						$TermReportID = $lreportcard->getCorespondingTermReportID($ReportID, $ReportColumnID);
						if(!empty($TermReportID) && $TermReportID != $ReportID) {
							$TermManualAjustedMark = $lreportcard->Get_Manual_Adjustment($TermReportID, $tempArr[$j]["StudentID"], 0, 0);
							$thisTermGrandMark = $TermManualAjustedMark[$tempArr[$j]["StudentID"]][0][0];
							$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverageGrade"] = $thisTermGrandMark["GrandAverageGrade"]?$thisTermGrandMark["GrandAverageGrade"]:$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverageGrade"];
						}
					}
					
					# Round up all the marks if for SSPA
					if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M")
					{
						$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = round($thisGrandMark["GrandTotal"]?$thisGrandMark["GrandTotal"]:$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"]);
						$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = round($thisGrandMark["GrandAverage"]?$thisGrandMark["GrandAverage"]:$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"]);
						$resultAverage[$tempArr[$j]["StudentID"]]["ActualAverage"] = round($thisGrandMark["ActualAverage"]?$thisGrandMark["ActualAverage"]:$resultAverage[$tempArr[$j]["StudentID"]]["ActualAverage"]);
						$resultAverage[$tempArr[$j]["StudentID"]]["GPA"] = round($thisGrandMark["GPA"]?$thisGrandMark["GPA"]:$resultAverage[$tempArr[$j]["StudentID"]]["GPA"]);
					}
					
					// [2019-0123-1731-14066] add rounding logic
					if($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayRoundMarks'])
					{
					    if(is_numeric($resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"]) && $resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] != -1) {
					        $resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = $lreportcard->ROUND_MARK($resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"], "GrandTotal");
					    }
					    if(is_numeric($resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"]) && $resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] != -1) {
					        $resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = $lreportcard->ROUND_MARK($resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"], "GrandAverage");
					    }
					    if(is_numeric($resultAverage[$tempArr[$j]["StudentID"]]["GPA"]) && $resultAverage[$tempArr[$j]["StudentID"]]["GPA"] != -1) {
					        $resultAverage[$tempArr[$j]["StudentID"]]["GPA"] = $lreportcard->ROUND_MARK($resultAverage[$tempArr[$j]["StudentID"]]["GPA"], "GrandAverage");
					    }
					}
							
					if ($tempArr[$j]["OrderMeritClass"] > 0 ||$thisGrandMark["OrderMeritClass"]) {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritClass"] = $thisGrandMark["OrderMeritClass"]?$thisGrandMark["OrderMeritClass"]:$tempArr[$j]["OrderMeritClass"];
					}
					else {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritClass"] = "-";
					}
					if ($tempArr[$j]["OrderMeritForm"] > 0 || $thisGrandMark["OrderMeritForm"]) {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritForm"] = $thisGrandMark["OrderMeritForm"]?$thisGrandMark["OrderMeritForm"]:$tempArr[$j]["OrderMeritForm"];
					}
					else {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritForm"] = "-";
					}
				}
			}
		}
		
		# get student class and class number of last year for munsang customization
		if ($studentIDList != NULL)
		{
			$studentIDListSql = implode(",", $studentIDList);
		}
		$activeYear = $lreportcard->GET_ACTIVE_YEAR("-");
		$lastYearArr = explode("-", $activeYear);
		$lastYearArr[0] -= 1; 
		$lastYearArr[1] -= 1; 
		$lastYear = implode("-", $lastYearArr);
		$sql = "SELECT UserID, ClassName, ClassNumber FROM PROFILE_CLASS_HISTORY WHERE UserID IN ($studentIDListSql) AND AcademicYear = '$lastYear' ";
		$result = $lreportcard->returnArray($sql, 3);
		$studentClassHistoryArr = array();
		for ($i=0; $i<sizeof($result); $i++)
		{
			list($tempUserID, $tempClassName, $tempClassNumber) = $result[$i];
			$studentClassHistoryArr[$tempUserID]['ClassName'] = $tempClassName;
			$studentClassHistoryArr[$tempUserID]['ClassNumber'] = $tempClassNumber;
		}
		
		// Get Summary Info from CSV
		# build data array
		$SummaryInfoFields = $lreportcard->OtherInfoInGrandMS?$lreportcard->OtherInfoInGrandMS:array("Conduct");
		$ary = array();
		$csvType = $lreportcard->getOtherInfoType();
		if($reportTemplateInfo['Semester']=="F") {
			$InfoTermID = 0;
		}
		else {
			$InfoTermID = $reportTemplateInfo['Semester'];
		}
		if(!empty($csvType)) {
			foreach($csvType as $k=>$Type) {
				for($i=0; $i<sizeof($ClassNameArr); $i++) {
					$csvData = $lreportcard->getOtherInfoData($Type, $InfoTermID, $ClassIDArr[$i]);	
					if(!empty($csvData)) {
						foreach($csvData as $thisStudentID=>$data) {
							foreach($data as $key=>$val) {
								if (!is_numeric($key) && trim($key) !== "")
									$ary[$ClassNameArr[$i]][$thisStudentID][$key] = $val;
							}
						}
					}
				}
			}
		}
		
		if ($ReportCardCustomSchoolName == 'munsang_college')
		{
			foreach ($studentIDList as $thisStudentID)
			{
				$thisClassName = $studentIDClassMap[$thisStudentID];
				
				$eDisDataArr = $lreportcard->Get_eDiscipline_Data($InfoTermID, $thisStudentID);
				$ary[$thisClassName][$thisStudentID]['Conduct'] = $eDisDataArr['Conduct'];
			}
		}		
		
		# initialize variables to calculate Average, S.D, Variance, Max, Min, Passing Rate
		// $columnSubjectIDMapArr[#column] = SubjectID;  
		$columnSubjectIDMapArr = array();	// For getting the pasing mark from the grading scheme
		// $columnScaleOutputMapArr[#column] = "M" or "G"
		$columnScaleOutputMapArr = array();
		
		// Table header
		$exportColumn[] = "WebSAMSRegNumber";
		
		if ($eRCTemplateSetting['Report']['GrandMS']['ExportUserLogin']) {
			$exportColumn[] = $Lang['General']['UserLogin'];
		}
		
		// show last year class and class number for SSPA
		if ($ForSSPA)
		{
			// if year = 2005-2007, display "06 Class" and "05 Class" as the column titles
			$activeYearDisplay = substr($activeYear,2,2);
			$lastYearDisplay = substr($lastYear,2,2);
			
			$exportColumn[] = $activeYearDisplay." ".$eReportCard['Template']['StudentInfo']['ClassName'];
			$exportColumn[] = $lastYearDisplay." ".$eReportCard['Template']['StudentInfo']['ClassName'];
		}
		else
		{
			$exportColumn[] = $eReportCard['Template']['StudentInfo']['ClassName'];
			$exportColumn[] = $eReportCard['Template']['StudentInfo']['ClassNo'];
		}
		
		if ($showEnglishName)
		{
			$exportColumn[] = $i_UserEnglishName;
		}
		if ($showChineseName)
		{
			$exportColumn[] = $i_UserChineseName;
		}
		// [2015-0306-1036-36206]
		if (!$eRCTemplateSetting['Report']['GrandMS']['ExportUserLogin'] && $showUserLogin)
		{
			$exportColumn[] = $eReportCard['Template']['StudentInfo']['UserLogin'];
		}
		if ($showGender)
		{
			$exportColumn[] = $eReportCard['Template']['StudentInfo']['Gender'];
		}
		if (!isset($SubjectID) || empty($SubjectID)) {
			
		# not specific subject
      	if($showSubject){	
  			foreach($FormSubjectArr as $tmpSubjectID => $tmpSubjectName) {
				 //skip non selected subject and its component
  				if (in_array($tmpSubjectID, $CmpSubjectIDList))
					$ck_subject_id = $lreportcard->GET_PARENT_SUBJECT_ID($tmpSubjectID);
				else
					$ck_subject_id = $tmpSubjectID;
				if(!in_array($ck_subject_id, $SelectedSubjectID))	continue;
  				
  				if ($showSubjectComponent)
  				{
  					if (in_array($tmpSubjectID, $ParentSubjectIDList)) 
  					{
  						$thisParentSubjectName = $tmpSubjectName[0];
  						
  						# add parent subject name to the component subjects' title
  						# e.g. "Oral" -> "English Oral"
  						foreach($tmpCmpSubjectTitleArr as $count => $id_title)
  						{
  							$thisSubjectDataArr = explode("::", $id_title);
  							$thisID = $thisSubjectDataArr[0];
  							$thisSubjectName = $thisSubjectDataArr[1];
  							$exportColumn[] = $thisParentSubjectName." ".$thisSubjectName;
  							
  							$columnSubjectIDMapArr[] = $thisID;
  							$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $thisID,0 ,0 ,$ReportID);
  							$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
  						}
  						
  						# initialize $tmpCmpSubjectTitleArr for next component subjects group
  						$tmpCmpSubjectTitleArr = array();
  						
  						# Total Title
  						$exportColumn[] = $thisParentSubjectName." ".$eReportCard['Template']['Total'];
  						
  						$columnSubjectIDMapArr[] = $tmpSubjectID;
  						$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID,0 ,0 ,$ReportID);
  						$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
  					} 
  					else if (in_array($tmpSubjectID, $CmpSubjectIDList)) 
  					{
  						$tmpCmpSubjectTitleArr[] = $tmpSubjectID."::".$tmpSubjectName[0];
  					} 
  					else 
  					{
  						$exportColumn[] = $tmpSubjectName[0];
  						$columnSubjectIDMapArr[] = $tmpSubjectID;
  						$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID,0 ,0 ,$ReportID);
  						$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
  					}
  				}
  				else
  				{
  					# Hide component subjects
  					if (in_array($tmpSubjectID, $ParentSubjectIDList)) {
  						$exportColumn[] = $tmpSubjectName[0];
  						
  						$columnSubjectIDMapArr[] = $tmpSubjectID;
  						$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID,0 ,0 ,$ReportID);
  						$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
  					}
  					else if (!in_array($tmpSubjectID, $CmpSubjectIDList)) 
  					{
  						$exportColumn[] = $tmpSubjectName[0];
  						
  						$columnSubjectIDMapArr[] = $tmpSubjectID;
  						$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID,0 ,0 ,$ReportID);
  						$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
  					}
  				}
  			}
			}			
		}
		else {
			# specific subject
      		if($showSubject){
  			$SubjectName = $FormSubjectArr[$SubjectID][0];
  			if (!isset($ReportColumnID) || empty($ReportColumnID)) {
  				# overall terms
  				##############################
  				// Special handling of subject which having component subjects
  				if ($showSubjectComponent)
  				{
  					if (in_array($SubjectID, $ParentSubjectIDList)) 
  					{
  						$countCmpSubject = 1;
  						foreach($CmpSubjectIDNameArr as $tmpSubjectID => $tmpSubjectName) {
  							$exportColumn[] = "[".$countCmpSubject."] ".$tmpSubjectName;
  							$countCmpSubject++;
  						}
  						#$markTable .= "<th>".$eReportCard['Template']['Total']."</th>";
  					}
  				}
  				
  				##############################
  				$exportColumn[] = $eReportCard['Total'];
  				
  				$columnSubjectIDMapArr[] = $SubjectID;
				$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0 ,$ReportID);
				$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
  			}
  			else {
  				# specific terms
  				##############################
  				// Special handling of subject which having component subjects
  				if ($showSubjectComponent && in_array($SubjectID, $ParentSubjectIDList)) {
  					$countCmpSubject = 1;
  					$ParentMarkFormula = array();
  					foreach($CmpSubjectIDNameArr as $tmpSubjectID => $tmpSubjectName) {
  						$OtherCondition = "SubjectID = ".$tmpSubjectID." AND ReportColumnID = $ReportColumnID";
  						$CmpSubjectWeight = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
  						$CmpSubjectWeight = ($CmpSubjectWeight[0]['Weight']*100)."%";
  						$ParentMarkFormula[] = "[".$countCmpSubject."](".$CmpSubjectWeight.")";
  						
  						$exportColumn[] = "[".$countCmpSubject."] ".$tmpSubjectName;
  						$countCmpSubject++;
  						
  						$columnSubjectIDMapArr[] = $tmpSubjectID;
  						$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID,0 ,0 ,$ReportID);
  						$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
  					}
  					$ParentMarkFormula = implode(" + ", $ParentMarkFormula);
  					$exportColumn[] = $eReportCard['Total']." ".$ParentMarkFormula;
  					
  					$columnSubjectIDMapArr[] = $SubjectID;
  					$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0 ,$ReportID);
  					$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
  					
  				}
  				else {
  					$exportColumn[] = $eReportCard['Total'];
  					
  					$columnSubjectIDMapArr[] = $SubjectID;
  					$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0 ,0 ,$ReportID);
  					$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
  				}
  			}
  		}	
		}
		
		# Summary Info
		if ($showSummaryInfo)
		{
			for($i=0; $i<sizeof($SummaryInfoFields); $i++) {
				$exportColumn[] = $eReportCard['Template'][$SummaryInfoFields[$i]];
				$columnScaleOutputMapArr[] = "G";
			}
		}
		
//		if ($showGrandTotal)
//		{
//			$exportColumn[] = $eReportCard['Template']['GrandTotal'];
//			$columnScaleOutputMapArr[] = "M";
//		}
//		if ($showGrandAverage)
//		{
//			$exportColumn[] = $eReportCard['Template']['GrandAverage'];
//			$columnScaleOutputMapArr[] = "M";
//		}
//		if ($showGPA)
//		{
//			$exportColumn[] = $eReportCard['Template']['GPA'];
//			$columnScaleOutputMapArr[] = "M";
//		}
		if ($showGrandTotal)
		{
			$exportColumn[] = $eReportCard['Template']['GrandTotal'];
			$columnScaleOutputMapArr[] = "M";
		}
		if ($showGrandTotalGrade)
		{
			$exportColumn[] = $eReportCard['Template']['GrandTotal']." ".$eReportCard['Grade'];
			$columnScaleOutputMapArr[] = "G";
		}
		if ($showGrandAverage)
		{
			$exportColumn[] = $eReportCard['Template']['GrandAverage'];
			$columnScaleOutputMapArr[] = "M";
		}
		if ($showGrandAverageGrade)
		{
			$exportColumn[] = $eReportCard['Template']['GrandAverage']." ".$eReportCard['Grade'];
			$columnScaleOutputMapArr[] = "G";
		}
		if ($showActualAverage)
		{
			$exportColumn[] = $eReportCard['Template']['ActualAverage'];
			$columnScaleOutputMapArr[] = "M";
		}
		if ($showActualAverageGrade)
		{
			$exportColumn[] = $eReportCard['Template']['ActualAverage']." ".$eReportCard['Grade'];
			$columnScaleOutputMapArr[] = "G";
		}
		if ($showGPA)
		{
			$exportColumn[] = $eReportCard['Template']['GPA'];
			$columnScaleOutputMapArr[] = "M";
		}
		if ($showGPAGrade)
		{
			$exportColumn[] = $eReportCard['Template']['GPA']." ".$eReportCard['Grade'];
			$columnScaleOutputMapArr[] = "G";
		}
		if ($ReportCardCustomSchoolName == "carmel_alison")
		{
			$exportColumn[] = $eReportCard['Template']['WeightedStandardScore'];
			$columnScaleOutputMapArr[] = "M";
		}
		// [2019-0123-1731-14066]
		if($eRCTemplateSetting['Report']['GrandMarksheet']['ShowTotalUnitFailed'] && $showTotalUnitFailed)
		{
		    $exportColumn[] = $eReportCard['Template']['TotalUnitFailed'];
		}
		if ($showClassRanking)
		{
			$exportColumn[] = $eReportCard['Template']['ClassPosition'];
		}
		if ($showOverallRanking)
		{
			$exportColumn[] = $eReportCard['Template']['FormPosition'];
		}
		
		# initialize variables to calculate Average, S.D, Variance, Max, Min, Passing Rate
		// $markDisplayedArr[#column][#row] = Mark;
		$markDisplayedArr = array();
		// $markNatureArr[#column][#row] = "Distinction" / "Pass" / "Fail";
		$markNatureArr = array();
		$ColumnCounter = 0;
			
		$iCounter = 0;
		$jCounter = 0;
		// Mark
		foreach($studentIDListSorted as $classNumber => $studentID) {
			$ColumnCounter = 0;
			$jCounter = 0;
			
			# WebSAMSNo
			$thisWebSAMSRegNo = $studentIDRegNoMap[$studentID];
			$ExportArr[$iCounter][$jCounter] = $thisWebSAMSRegNo;
			$jCounter++;
			
			if ($eRCTemplateSetting['Report']['GrandMS']['ExportUserLogin']) {
				$ExportArr[$iCounter][$jCounter] = $studentUserLoginMap[$studentID];
				$jCounter++;
			}
			
			# show last year class and class number if for SSPA
			if ($ForSSPA)
			{
				$ExportArr[$iCounter][$jCounter] = $studentIDClassMap[$studentID]." ".$studentIDClassNumMap[$studentID];
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $studentClassHistoryArr[$studentID]['ClassName']." ".$studentClassHistoryArr[$studentID]['ClassNumber'];
				$jCounter++;
			}
			else
			{
				$ExportArr[$iCounter][$jCounter] = $studentIDClassMap[$studentID];
				$jCounter++;
				$ExportArr[$iCounter][$jCounter] = $studentIDClassNumMap[$studentID];
				$jCounter++;
			}
			
			$studentNameEn = trim($studentIDNameMap[$studentID]['En']);
			$studentNameCh = trim($studentIDNameMap[$studentID]['Ch']);
			
			if ($showEnglishName)
			{
				$ExportArr[$iCounter][$jCounter] = $studentNameEn;
				$jCounter++;
			}
			if ($showChineseName)
			{
				$ExportArr[$iCounter][$jCounter] = $studentNameCh;
				$jCounter++;
			}
			
			# Gender & User Login [2015-0306-1036-36206]
			if ($showGender || (!$eRCTemplateSetting['Report']['GrandMS']['ExportUserLogin'] && $showUserLogin))
			{
				//$StudentInfo[$studentID] = $lreportcard-> Get_Student_Info_By_ClassName_ClassNumber($studentIDClassMap[$studentID],$studentIDClassNumMap[$studentID]);
				$StudentInfo[$studentID] = $lreportcard->Get_Student_Class_ClassLevel_Info($studentID);
				// [2015-0306-1036-36206]
				if(!$eRCTemplateSetting['Report']['GrandMS']['ExportUserLogin'] && $showUserLogin){
					$ExportArr[$iCounter][$jCounter] = $StudentInfo[$studentID][0]['UserLogin'];
					$jCounter++;
				}
				if($showGender){
					$ExportArr[$iCounter][$jCounter] = $StudentInfo[$studentID][0]['Gender'];
					$jCounter++;
				}
			}
			if (!isset($SubjectID) || empty($SubjectID)) {
				# overall subjects
				if($showSubject){
  				foreach($FormSubjectArr as $tmpSubjectID => $tmpSubjectName) {
	  				 //skip non selected subject and its component
	  				if (in_array($tmpSubjectID, $CmpSubjectIDList))
						$ck_subject_id = $lreportcard->GET_PARENT_SUBJECT_ID($tmpSubjectID);
					else
						$ck_subject_id = $tmpSubjectID;
					if(!in_array($ck_subject_id, $SelectedSubjectID))	continue;
  					
  					if ($showSubjectComponent)
  					{
  						$thisMarks = $resultSubject[$tmpSubjectID][$studentID];
  						$thisGrade = $resultSubjectGrade[$tmpSubjectID][$studentID];
  						
  						// [2019-0123-1731-14066] add rounding logic
  						if($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayRoundMarks'] && is_numeric($thisMarks)) {
  						    $thisMarks = $lreportcard->ROUND_MARK($thisMarks, "SubjectTotal");
  						}
  						
  						$ExportArr[$iCounter][$jCounter] = $thisMarks;
  						$jCounter++;
  						
  						$markDisplayedArr[$ColumnCounter][] = (is_numeric($thisMarks))? $thisMarks : '';
  						
						# Record Pass Status for Passing Rate
						$thisNature = '';
  						//if (!$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1_STRING($thisMarks) && !$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2_STRING($thisMarks) )
  						if (!in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1()) && !in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2()))
  						{
  							$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID, 1 ,0 ,$ReportID);
							$SchemeID = $SubjectFormGradingSettings['SchemeID'];
							$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
  							$SchemeInfoArr = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
							$TopPercentage = $SchemeInfoArr['TopPercentage'];
							
  							if ($ScaleDisplay == "M" && $TopPercentage > 0)
  							{
  								$thisStyleDetermineMarks = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMarks, $ReportID, $studentID, $tmpSubjectID, $ClassLevelID, $ReportColumnID);
  								$ConvertBy = "G";
  							}
  							else
  							{
  								$thisStyleDetermineMarks = $thisMarks;
  								$ConvertBy = "";
  							}
  							
  							$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $tmpSubjectID, $thisStyleDetermineMarks,'',$ConvertBy, $ReportID);
  							$markNatureArr[$ColumnCounter][] = $thisNature;
  							if(in_array($thisNature,array("Pass","Distinction")) && !in_array($tmpSubjectID, $CmpSubjectIDList))	$NumberOfPassSubject++;
  						}
						else
						{
							$markNatureArr[$ColumnCounter][] = "N.A.";
						}
  						$ColumnCounter++;
  					}
  					else
  					{
  						# show parent subject only
  						if (!in_array($tmpSubjectID, $CmpSubjectIDList)) {
  							$thisMarks = $resultSubject[$tmpSubjectID][$studentID];
  							$thisGrade = $resultSubjectGrade[$tmpSubjectID][$studentID];
  							
  							// [2019-0123-1731-14066] add rounding logic
  							if($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayRoundMarks'] && is_numeric($thisMarks)) {
  							    $thisMarks = $lreportcard->ROUND_MARK($thisMarks, "SubjectTotal");
  							}
  							
  							$ExportArr[$iCounter][$jCounter] = $thisMarks;
  							$jCounter++;
  							
  							$markDisplayedArr[$ColumnCounter][] = (is_numeric($thisMarks))? $thisMarks : '';
  							
							# Record Pass Status for Passing Rate
							$thisNature = '';
	  						//if (!$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1_STRING($thisMarks) && !$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2_STRING($thisMarks) )
  							if (!in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1()) && !in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2()))
	  						{
	  							$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID, 1 ,0 ,$ReportID);
								$SchemeID = $SubjectFormGradingSettings['SchemeID'];
								$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
	  							$SchemeInfoArr = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
								$TopPercentage = $SchemeInfoArr['TopPercentage'];
								
	  							if ($ScaleDisplay == "M" && $TopPercentage > 0)
	  							{
	  								$thisStyleDetermineMarks = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMarks, $ReportID, $studentID, $tmpSubjectID, $ClassLevelID, $ReportColumnID);
	  								$ConvertBy = "G";
	  							}
	  							else
	  							{
	  								$thisStyleDetermineMarks = $thisMarks;
	  								$ConvertBy = "";
	  							}
	  							
	  							$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $tmpSubjectID, $thisStyleDetermineMarks,'',$ConvertBy, $ReportID);
	  							$markNatureArr[$ColumnCounter][] = $thisNature;
	  							if(in_array($thisNature,array("Pass","Distinction")) && !in_array($tmpSubjectID, $CmpSubjectIDList))	$NumberOfPassSubject++;
	  						}
							else
							{
								$markNatureArr[$ColumnCounter][] = "N.A.";
							}
  							$ColumnCounter++;
  						}
  					}
  				}
				}
				
				# Summary Info
				if ($showSummaryInfo)
				{
					for($i=0; $i<sizeof($SummaryInfoFields); $i++) {
//						$thisRegNo = $studentIDRegNoMap[$studentID];
						$thisClass = $studentIDClassMap[$studentID];
						$tmpInfo = isset($ary[$thisClass][$studentID][$SummaryInfoFields[$i]]) ? $ary[$thisClass][$studentID][$SummaryInfoFields[$i]]: "-";

						// [2020-0415-1414-56164] handle if $tmpInfo is array
                        if(is_array($tmpInfo)) {
                            $tmpInfo = !empty($tmpInfo) ? implode(', ', (array)$tmpInfo) : "-";
                        }
						$ExportArr[$iCounter][$jCounter] = $tmpInfo;
						$jCounter++;
						
						$markDisplayedArr[$ColumnCounter][] = $tmpInfo;
						$ColumnCounter++;
					}
				}
				
//				if ($showGrandTotal)
//				{
//					$ExportArr[$iCounter][$jCounter] = $resultAverage[$studentID]["GrandTotal"];
//					$jCounter++;
//					
//					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandTotal"];
//					$ColumnCounter++;
//				}
//				if ($showGrandAverage)
//				{
//					$ExportArr[$iCounter][$jCounter] = $resultAverage[$studentID]["GrandAverage"];
//					$jCounter++;
//					
//					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandAverage"];
//					$ColumnCounter++;
//				}
//				if ($showGPA)
//				{
//					$ExportArr[$iCounter][$jCounter] = $resultAverage[$studentID]["GPA"];
//					$jCounter++;
//					
//					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GPA"];
//					$ColumnCounter++;
//				}

				$GrandMarkScheme = $lreportcard->Get_Grand_Mark_Grading_Scheme($ClassLevelID,$ReportID);
				
				if ($showGrandTotal)
				{
					$thisGrandTotal = $resultAverage[$studentID]["GrandTotal"];
					
					// [2017-0706-1012-27236] replace -1 by "-" 
					$ExportArr[$iCounter][$jCounter] = is_numeric($thisGrandTotal) && ($thisGrandTotal=="" || $thisGrandTotal==-1)? "-" : $thisGrandTotal;
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $thisGrandTotal;
					$ColumnCounter++;
				}
				if ($showGrandTotalGrade)
				{
					$TotalGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandMarkScheme["-2"]['SchemeID'], $resultAverage[$studentID]["GrandTotal"]);
					
					$ExportArr[$iCounter][$jCounter] = $TotalGrade?$TotalGrade:'-';
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $TotalGrade?$TotalGrade:'-';
					$ColumnCounter++;
				}
				if ($showGrandAverage)
				{
					$thisGrandAverage = $resultAverage[$studentID]["GrandAverage"];
					
					// [2017-0706-1012-27236] replace -1 by "-" 
					$ExportArr[$iCounter][$jCounter] = is_numeric($thisGrandAverage) && ($thisGrandAverage=="" || $thisGrandAverage==-1)? "-" : $thisGrandAverage;
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $thisGrandAverage;
					$ColumnCounter++;
				}
				if ($showGrandAverageGrade)
				{
					$adjustedAvgGrade = $resultAverage[$studentID]["GrandAverageGrade"];
					$AverageGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandMarkScheme["-1"]['SchemeID'], $resultAverage[$studentID]["GrandAverage"], $ReportID, $studentID, '-1', $ClassLevelID, $ReportColumnID);
					$AverageGrade = $eRCTemplateSetting['GrandAverageGradeManualAdjustment'] && !empty($adjustedAvgGrade)? $adjustedAvgGrade : $AverageGrade;
					
					$ExportArr[$iCounter][$jCounter] = $AverageGrade?$AverageGrade:'-';
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $AverageGrade?$AverageGrade:'-';
					$ColumnCounter++;
				}
				if ($showActualAverage)
				{
					$thisGrandActualAverage = $resultAverage[$studentID]["ActualAverage"];
					
					// [2017-0706-1012-27236] replace -1 by "-" 
					$ExportArr[$iCounter][$jCounter] = is_numeric($thisGrandActualAverage) && ($thisGrandActualAverage=="" || $thisGrandActualAverage==-1)? "-" : $thisGrandActualAverage;
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $thisGrandActualAverage;
					$ColumnCounter++;
				}
				if ($showActualAverageGrade)
				{
					$ActualAverageGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandMarkScheme["-1"]['SchemeID'], $resultAverage[$studentID]["ActualAverage"],$ReportID,$studentID,'-1',$ClassLevelID,$ReportColumnID);
					
					$ExportArr[$iCounter][$jCounter] = $ActualAverageGrade?$ActualAverageGrade:'-';
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $ActualAverageGrade?$ActualAverageGrade:'-';
					$ColumnCounter++;
				}
				if ($showGPA)
				{
					$thisGPA = $resultAverage[$studentID]["GPA"];
					
					// [2017-0706-1012-27236] replace -1 by "-" 
					$ExportArr[$iCounter][$jCounter] = is_numeric($thisGPA) && ($thisGPA=="" || $thisGPA==-1)? "-" : $thisGPA;
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $thisGPA;
					$ColumnCounter++;
				}
				if ($showGPAGrade)
				{
					$GPAGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandMarkScheme["-3"]['SchemeID'], $resultAverage[$studentID]["GPA"]);
					
					$ExportArr[$iCounter][$jCounter] = $GPAGrade?$GPAGrade:'-';
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $GPAGrade?$GPAGrade:'-';
					$ColumnCounter++;
				}
				if ($ReportCardCustomSchoolName == "carmel_alison")
				{
					$GrandMarkArr = $lreportcard->getReportResultScore($ReportID, $reportColumnID, $studentID);
					$thisGrandSDScore = $GrandMarkArr["GrandSDScore"];
					
					$ExportArr[$iCounter][$jCounter] = $thisGrandSDScore;
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $thisGrandSDScore;
					$ColumnCounter++;
				}
				// [2019-0123-1731-14066]
				if($eRCTemplateSetting['Report']['GrandMarksheet']['ShowTotalUnitFailed'] && $showTotalUnitFailed)
				{
				    $ExportArr[$iCounter][$jCounter] = isset($studentFailUnitArr[$studentID])? $studentFailUnitArr[$studentID] : $lreportcard->EmptySymbol;
				    $jCounter++;
				    
				    $markDisplayedArr[$ColumnCounter][] = isset($studentFailUnitArr[$studentID])? $studentFailUnitArr[$studentID] : $lreportcard->EmptySymbol;
				    $ColumnCounter++;
				}
				
				if ($showClassRanking)
				{
					$ExportArr[$iCounter][$jCounter] = $resultAverage[$studentID]["OrderMeritClass"];
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["OrderMeritClass"];
					$ColumnCounter++;
				}
				if ($showOverallRanking)
				{
					$ExportArr[$iCounter][$jCounter] = $resultAverage[$studentID]["OrderMeritForm"];
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["OrderMeritForm"];
					$ColumnCounter++;
				}
			}
			else {
				# specific subject
				if($showSubject){
  					if (!isset($ReportColumnID) || empty($ReportColumnID)) {
  					# overall terms
  					##############################
  					// Special handling of subject which having component subjects
  					if ($showSubjectComponent && in_array($SubjectID, $ParentSubjectIDList)) {
  						foreach($CmpSubjectIDNameArr as $tmpSubjectID => $tmpSubjectName)
  						{
  							$thisMarks = $resultDetailSubject[$tmpSubjectID][$studentID];
  							$thisGrade = $resultDetailSubjectGrade[$tmpSubjectID][$studentID];
  							
  							// [2019-0123-1731-14066] add rounding logic
  							if($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayRoundMarks'] && is_numeric($thisMarks)) {
  							    $thisMarks = $lreportcard->ROUND_MARK($thisMarks, "SubjectTotal");
  							}
  							
  							$ExportArr[$iCounter][$jCounter] = $thisMarks;
  							$jCounter++;
  							
  							$markDisplayedArr[$ColumnCounter][] = (is_numeric($thisMarks))? $thisMarks : '';
  							
							# Record Pass Status for Passing Rate
							$thisNature = '';
	  						//if (!$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1_STRING($thisMarks) && !$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2_STRING($thisMarks) )
	  						if (!in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1()) && !in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2()))
	  						{
	  							$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID, 1 ,0 ,$ReportID);
								$SchemeID = $SubjectFormGradingSettings['SchemeID'];
								$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
	  							$SchemeInfoArr = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
								$TopPercentage = $SchemeInfoArr['TopPercentage'];
								
	  							if ($ScaleDisplay == "M" && $TopPercentage > 0)
	  							{
	  								$thisStyleDetermineMarks = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMarks, $ReportID, $studentID, $tmpSubjectID, $ClassLevelID, $ReportColumnID);
	  								$ConvertBy = "G";
	  							}
	  							else
	  							{
	  								$thisStyleDetermineMarks = $thisMarks;
	  								$ConvertBy = "";
	  							}
	  							
	  							$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $tmpSubjectID, $thisStyleDetermineMarks,'',$ConvertBy, $ReportID);
	  							$markNatureArr[$ColumnCounter][] = $thisNature;
	  							if(in_array($thisNature,array("Pass","Distinction")) && !in_array($tmpSubjectID, $CmpSubjectIDList))	$NumberOfPassSubject++;
	  						}
							else
							{
								$markNatureArr[$ColumnCounter][] = "N.A.";
							}
  							$ColumnCounter++;
  						}
  						#$markTable .= "<td align='center'>".$resultDetailSubject[$SubjectID][$studentID]."</td>";
  					}
  					##############################
  					
  					$thisMarks = $resultSubject[$studentID];
  					$thisGrade = $resultSubjectGrade[$studentID];
  					
  					// [2019-0123-1731-14066] add rounding logic
  					if($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayRoundMarks'] && is_numeric($thisMarks)) {
  					    $thisMarks = $lreportcard->ROUND_MARK($thisMarks, "SubjectTotal");
  					}
  					
  					$ExportArr[$iCounter][$jCounter] = $thisMarks;
  					$jCounter++;
  					
  					$markDisplayedArr[$ColumnCounter][] = (is_numeric($thisMarks))? $thisMarks : '';
  					
  					# Record Pass Status for Passing Rate
					$thisNature = '';
					//if (!$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1_STRING($thisMarks) && !$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2_STRING($thisMarks) )
					if (!in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1()) && !in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2()))
					{
						$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 1, 0 ,$ReportID);
						$SchemeID = $SubjectFormGradingSettings['SchemeID'];
						$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
						$SchemeInfoArr = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
						$TopPercentage = $SchemeInfoArr['TopPercentage'];
						
						if ($ScaleDisplay == "M" && $TopPercentage > 0)
						{
							$thisStyleDetermineMarks = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMarks, $ReportID, $studentID, $tmpSubjectID, $ClassLevelID, $ReportColumnID);
							$ConvertBy = "G";
						}
						else
						{
							$thisStyleDetermineMarks = $thisMarks;
							$ConvertBy = "";
						}
						
						$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $SubjectID, $thisStyleDetermineMarks,'',$ConvertBy, $ReportID);
						$markNatureArr[$ColumnCounter][] = $thisNature;
						if(in_array($thisNature,array("Pass","Distinction")) && !in_array($SubjectID, $CmpSubjectIDList))	$NumberOfPassSubject++;
					}
					else
					{
						$markNatureArr[$ColumnCounter][] = "N.A.";
					}
  					$ColumnCounter++;
  					
  					//if ($subjectGradingScheme["scaleDisplay"] == "M") {
  					//	$markTable .= "<td align='center'>".$OrderFormSubject[$studentID]."</td>";
  					//}
  				}
  				else {
  					# specific term
  					##############################
  					// Special handling of subject which having component subjects
  					if ($showSubjectComponent && in_array($SubjectID, $ParentSubjectIDList)) {
  						foreach($CmpSubjectIDNameArr as $tmpSubjectID => $tmpSubjectName)
  						{
  							$thisMarks = $resultDetailSubject[$tmpSubjectID][$studentID];
  							$thisGrade = $resultDetailSubjectGrade[$tmpSubjectID][$studentID];
  							
  							// [2019-0123-1731-14066] add rounding logic
  							if($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayRoundMarks'] && is_numeric($thisMarks)) {
  							    $thisMarks = $lreportcard->ROUND_MARK($thisMarks, "SubjectTotal");
  							}
  							
  							$ExportArr[$iCounter][$jCounter] = $thisMarks;
  							$jCounter++;
  							
  							$markDisplayedArr[$ColumnCounter][] = (is_numeric($thisMarks))? $thisMarks : '';
  							
							# Record Pass Status for Passing Rate
							$thisNature = '';
	  						//if (!$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1_STRING($thisMarks) && !$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2_STRING($thisMarks) )
	  						if (!in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1()) && !in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2()))
	  						{
	  							$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID, 1 ,0 ,$ReportID);
								$SchemeID = $SubjectFormGradingSettings['SchemeID'];
								$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
	  							$SchemeInfoArr = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
								$TopPercentage = $SchemeInfoArr['TopPercentage'];
								
	  							if ($ScaleDisplay == "M" && $TopPercentage > 0)
	  							{
	  								$thisStyleDetermineMarks = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMarks, $ReportID, $studentID, $tmpSubjectID, $ClassLevelID, $ReportColumnID);
	  								$ConvertBy = "G";
	  							}
	  							else
	  							{
	  								$thisStyleDetermineMarks = $thisMarks;
	  								$ConvertBy = "";
	  							}
	  							
	  							$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $tmpSubjectID, $thisStyleDetermineMarks,'',$ConvertBy, $ReportID);
	  							$markNatureArr[$ColumnCounter][] = $thisNature;
	  							if(in_array($thisNature,array("Pass","Distinction")) && !in_array($tmpSubjectID, $CmpSubjectIDList))	$NumberOfPassSubject++;
	  						}
							else
							{
								$markNatureArr[$ColumnCounter][] = "N.A.";
							}
								
  							$ColumnCounter++;
  						}
  						#$markTable .= "<td align='center'>".$resultDetailSubject[$SubjectID][$studentID]."</td>";
  					}
  					##############################
  					
  					$thisMarks = $resultSubject[$studentID];
  					$thisGrade = $resultSubjectGrade[$studentID];
  					
  					// [2019-0123-1731-14066] add rounding logic
  					if($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayRoundMarks'] && is_numeric($thisMarks)) {
  					    $thisMarks = $lreportcard->ROUND_MARK($thisMarks, "SubjectTotal");
  					}
  					
  					$ExportArr[$iCounter][$jCounter] = $thisMarks;
  					$jCounter++;
  					
  					$markDisplayedArr[$ColumnCounter][] = (is_numeric($thisMarks))? $thisMarks : '';
  					
					# Record Pass Status for Passing Rate
					$thisNature = '';
					//if (!$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1_STRING($thisMarks) && !$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2_STRING($thisMarks) )
					if (!in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1()) && !in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2()))
					{
						$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 1 ,0 ,$ReportID);
						$SchemeID = $SubjectFormGradingSettings['SchemeID'];
						$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
						$SchemeInfoArr = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
						$TopPercentage = $SchemeInfoArr['TopPercentage'];
						
						if ($ScaleDisplay == "M" && $TopPercentage > 0)
						{
							$thisStyleDetermineMarks = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMarks, $ReportID, $studentID, $tmpSubjectID, $ClassLevelID, $ReportColumnID);
							$ConvertBy = "G";
						}
						else
						{
							$thisStyleDetermineMarks = $thisMarks;
							$ConvertBy = "";
						}
						
						$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $SubjectID, $thisStyleDetermineMarks,'',$ConvertBy, $ReportID);
						$markNatureArr[$ColumnCounter][] = $thisNature;
						if(in_array($thisNature,array("Pass","Distinction")) && !in_array($SubjectID, $CmpSubjectIDList))	$NumberOfPassSubject++;
					}
					else
					{
						$markNatureArr[$ColumnCounter][] = "N.A.";
					}
  					$ColumnCounter++;
  					//$markTable .= "<td align='center'>".$OrderFormSubject[$studentID]."</td>";
  				}
				}
				
				# Summary Info
				if ($showSummaryInfo)
				{
					for($i=0; $i<sizeof($SummaryInfoFields); $i++) {
//						$thisRegNo = $studentIDRegNoMap[$studentID];
						$thisClass = $studentIDClassMap[$studentID];
						$tmpInfo = isset($ary[$thisClass][$studentID][$SummaryInfoFields[$i]]) ? $ary[$thisClass][$studentID][$SummaryInfoFields[$i]]: "-";

                        // [2020-0415-1414-56164] handle if $tmpInfo is array
                        if(is_array($tmpInfo)) {
                            $tmpInfo = !empty($tmpInfo) ? implode(', ', (array)$tmpInfo) : "-";
                        }
						$ExportArr[$iCounter][$jCounter] = $tmpInfo;
						$jCounter++;
						
						$markDisplayedArr[$ColumnCounter][] = $tmpInfo;
						$ColumnCounter++;
					}
				} 
				
//				if ($showGrandTotal)
//				{
//					$ExportArr[$iCounter][$jCounter] = $resultAverage[$studentID]["GrandTotal"];
//					$jCounter++;
//					
//					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandTotal"];
//					$ColumnCounter++;
//				}
//				if ($showGrandAverage)
//				{
//					$ExportArr[$iCounter][$jCounter] = $resultAverage[$studentID]["GrandAverage"];
//					$jCounter++;
//					
//					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandAverage"];
//					$ColumnCounter++;
//				}
//				if ($showGPA)
//				{
//					$ExportArr[$iCounter][$jCounter] = $resultAverage[$studentID]["GPA"];
//					$jCounter++;
//					
//					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GPA"];
//					$ColumnCounter++;
//				}
				$GrandMarkScheme = $lreportcard->Get_Grand_Mark_Grading_Scheme($ClassLevelID,$ReportID);
				
				if ($showGrandTotal)
				{
					$ExportArr[$iCounter][$jCounter] = $resultAverage[$studentID]["GrandTotal"];
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandTotal"];
					$ColumnCounter++;
				}
				if ($showGrandTotalGrade)
				{
					$TotalGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandMarkScheme["-2"]['SchemeID'], $resultAverage[$studentID]["GrandTotal"]);
					
					$ExportArr[$iCounter][$jCounter] = $TotalGrade?$TotalGrade:'-';
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $TotalGrade?$TotalGrade:'-';
					$ColumnCounter++;
				}
				if ($showGrandAverage)
				{
					$ExportArr[$iCounter][$jCounter] = $resultAverage[$studentID]["GrandAverage"];
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandAverage"];
					$ColumnCounter++;
				}
				if ($showGrandAverageGrade)
				{
					$adjustedAvgGrade = $resultAverage[$studentID]["GrandAverageGrade"];
					$AverageGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandMarkScheme["-1"]['SchemeID'], $resultAverage[$studentID]["GrandAverage"], $ReportID, $studentID, '-1', $ClassLevelID, $ReportColumnID);
					$AverageGrade = $eRCTemplateSetting['GrandAverageGradeManualAdjustment'] && !empty($adjustedAvgGrade)? $adjustedAvgGrade : $AverageGrade;
					
					$ExportArr[$iCounter][$jCounter] = $AverageGrade?$AverageGrade:'-';
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $AverageGrade?$AverageGrade:'-';
					$ColumnCounter++;
				}
				if ($showActualAverage)
				{
					$ExportArr[$iCounter][$jCounter] = $resultAverage[$studentID]["ActualAverage"];
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["ActualAverage"];
					$ColumnCounter++;
				}
				if ($showActualAverageGrade)
				{
					$ActualAverageGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandMarkScheme["-1"]['SchemeID'], $resultAverage[$studentID]["ActualAverage"],$ReportID,$studentID,'-1',$ClassLevelID,$ReportColumnID);
					
					$ExportArr[$iCounter][$jCounter] = $ActualAverageGrade?$ActualAverageGrade:'-';
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $ActualAverageGrade?$ActualAverageGrade:'-';
					$ColumnCounter++;
				}
				if ($showGPA)
				{
					$ExportArr[$iCounter][$jCounter] = $resultAverage[$studentID]["GPA"];
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GPA"];
					$ColumnCounter++;
				}
				if ($showGPAGrade)
				{
					$GPAGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandMarkScheme["-3"]['SchemeID'], $resultAverage[$studentID]["GPA"]);
					
					$ExportArr[$iCounter][$jCounter] = $GPAGrade?$GPAGrade:'-';
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $GPAGrade?$GPAGrade:'-';
					$ColumnCounter++;
				}
				if ($ReportCardCustomSchoolName == "carmel_alison")
				{
					$GrandMarkArr = $lreportcard->getReportResultScore($ReportID, $reportColumnID, $studentID);
					$thisGrandSDScore = $GrandMarkArr["GrandSDScore"];
					
					$ExportArr[$iCounter][$jCounter] = $thisGrandSDScore;
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $thisGrandSDScore;
					$ColumnCounter++;
				}
				// [2019-0123-1731-14066]
				if($eRCTemplateSetting['Report']['GrandMarksheet']['ShowTotalUnitFailed'] && $showTotalUnitFailed)
				{
				    $ExportArr[$iCounter][$jCounter] = isset($studentFailUnitArr[$studentID])? $studentFailUnitArr[$studentID] : $lreportcard->EmptySymbol;
				    $jCounter++;
				    
				    $markDisplayedArr[$ColumnCounter][] = isset($studentFailUnitArr[$studentID])? $studentFailUnitArr[$studentID] : $lreportcard->EmptySymbol;
				    $ColumnCounter++;
				}
				
				if ($showClassRanking)
				{
					$ExportArr[$iCounter][$jCounter] = $OrderClassSubject[$studentID];
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $OrderClassSubject[$studentID];
					$ColumnCounter++;
				}
				if ($showOverallRanking)
				{
					$ExportArr[$iCounter][$jCounter] = $OrderFormSubject[$studentID];
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $OrderFormSubject[$studentID];
					$ColumnCounter++;
				}
			}
			$iCounter++;
		}
		
		if ($showStatistics)
		{
			# Statistics Rows
			$decimalPlacesShown = 2;
			$numEmptyColumn = 2;
			if ($eRCTemplateSetting['Report']['GrandMS']['ExportUserLogin']) {
				$numEmptyColumn++;
			}
			if ($showEnglishName)
				$numEmptyColumn++;
			if ($showChineseName)
				$numEmptyColumn++;
			// [2015-0306-1036-36206]
			if (!$eRCTemplateSetting['Report']['GrandMS']['ExportUserLogin'] && $showUserLogin)
				$numEmptyColumn++;
			if ($showGender)
				$numEmptyColumn++;
			
			# Average
			$jCounter = 0;
			$ExportArr[$iCounter][$jCounter] = $eReportCard['Template']['Average'];
			$jCounter++;
			for ($i=0; $i<$numEmptyColumn; $i++)
			{
				$ExportArr[$iCounter][$jCounter] = "";
				$jCounter++;
			}
			
			for ($i=0; $i<count($markDisplayedArr); $i++)
			{
				$thisScaleDisplay = $columnScaleOutputMapArr[$i];

				if ($thisScaleDisplay != "M")
				{
					$thisDisplay = $lreportcard->EmptySymbol;
				}
				else
				{
					$thisColumnMarksArr = array_values(array_remove_empty($markDisplayedArr[$i]));
					$thisDisplay = $lreportcard->getAverage($thisColumnMarksArr, $decimalPlacesShown);
				}
				$ExportArr[$iCounter][$jCounter] = $thisDisplay;
				$jCounter++;
			}
			$iCounter++;
			
			# SD
			$jCounter = 0;
			$ExportArr[$iCounter][$jCounter] = $eReportCard['Template']['SD'];
			$jCounter++;
			for ($i=0; $i<$numEmptyColumn; $i++)
			{
				$ExportArr[$iCounter][$jCounter] = "";
				$jCounter++;
			}
			for ($i=0; $i<count($markDisplayedArr); $i++)
			{
				$thisScaleDisplay = $columnScaleOutputMapArr[$i];
				
				if ($thisScaleDisplay != "M")
				{
					$thisDisplay = $lreportcard->EmptySymbol;
				}
				else
				{
					$thisColumnMarksArr = array_values(array_remove_empty($markDisplayedArr[$i]));
					$thisDisplay = $lreportcard->getSD($thisColumnMarksArr, $decimalPlacesShown);
				}
				$ExportArr[$iCounter][$jCounter] = $thisDisplay;
				$jCounter++;
			}
			$iCounter++;
			
			# Highest Mark
			$jCounter = 0;
			$ExportArr[$iCounter][$jCounter] = $eReportCard['Template']['HighestMark'];
			$jCounter++;
			for ($i=0; $i<$numEmptyColumn; $i++)
			{
				$ExportArr[$iCounter][$jCounter] = "";
				$jCounter++;
			}
			for ($i=0; $i<count($markDisplayedArr); $i++)
			{
				$thisScaleDisplay = $columnScaleOutputMapArr[$i];
				
				if ($thisScaleDisplay != "M")
				{
					$thisDisplay = $lreportcard->EmptySymbol;
				}
				else
				{
					$thisColumnMarksArr = array_values(array_remove_empty($markDisplayedArr[$i]));
					if(count($thisColumnMarksArr)>0)
						$thisDisplay = max($thisColumnMarksArr);
				}
				$ExportArr[$iCounter][$jCounter] = $thisDisplay;
				$jCounter++;
			}
			$iCounter++;
			
			# Lowest Mark
			$jCounter = 0;
			$ExportArr[$iCounter][$jCounter] = $eReportCard['Template']['LowestMark'];
			$jCounter++;
			for ($i=0; $i<$numEmptyColumn; $i++)
			{
				$ExportArr[$iCounter][$jCounter] = "";
				$jCounter++;
			}
			for ($i=0; $i<count($markDisplayedArr); $i++)
			{
				$thisScaleDisplay = $columnScaleOutputMapArr[$i];
				
				if ($thisScaleDisplay != "M")
				{
					$thisDisplay = $lreportcard->EmptySymbol;
				}
				else
				{
					$thisColumnMarksArr = array_values(array_remove_empty($markDisplayedArr[$i]));
					
					// [2017-0706-1012-27236] remove -1 in Column Marks
					$thisColumnMarksArr = array_diff((array)$thisColumnMarksArr, array(-1));
					if(count($thisColumnMarksArr)>0)
						$thisDisplay = min($thisColumnMarksArr);
				}
				$ExportArr[$iCounter][$jCounter] = $thisDisplay;
				$jCounter++;
			}
			$iCounter++;
			
//			# use the following ary to count how many empty symbol should be printed
//			$DisplayedAry = array($showSummaryInfo,$showGrandTotal,$showGrandTotalGrade,$showGrandAverage,$showGrandAverageGrade,$showGPA,$showGPAGrade,$showClassRanking,$showOverallRanking,$NumofPassSubject=0);
//			$NumOfEmptySymbol = array_sum($DisplayedAry);
//			$EmptySymbols = $lreportcard->GrandMarkSheetGenEmptySymbol("CSV",$NumOfEmptySymbol);
			
			# Passing Rate
			$jCounter = 0;
			$ExportArr[$iCounter][$jCounter] = $eReportCard['Template']['PassingRate'];
			$jCounter++;
			for ($i=0; $i<$numEmptyColumn; $i++)
			{
				$ExportArr[$iCounter][$jCounter] = "";
				$jCounter++;
			}
			for ($i=0; $i<count($markNatureArr); $i++)
			{
				$thisScaleDisplay = $columnScaleOutputMapArr[$i];
				
				$thisColumnMarksArr = $markNatureArr[$i];
				$thisDisplay = $lreportcard->getPassingRate($thisColumnMarksArr, 2);
				$thisDisplay = ($thisDisplay == "")? $lreportcard->EmptySymbol : $thisDisplay;
					
				$ExportArr[$iCounter][$jCounter] = $thisDisplay;
				$jCounter++;
			}
//			if ($showSummaryInfo)
//			{
//				$ExportArr[$iCounter][$jCounter] = $lreportcard->EmptySymbol;
//				$jCounter++;
//			}
//			if ($showGrandTotal)
//			{
//				$ExportArr[$iCounter][$jCounter] = $lreportcard->EmptySymbol;
//				$jCounter++;
//			}
//			if ($showGrandAverage)
//			{
//				$ExportArr[$iCounter][$jCounter] = $lreportcard->EmptySymbol;
//				$jCounter++;
//			}
//			if ($showGPA)
//			{
//				$ExportArr[$iCounter][$jCounter] = $lreportcard->EmptySymbol;
//				$jCounter++;
//			}
//			if ($showClassRanking)
//			{
//				$ExportArr[$iCounter][$jCounter] = $lreportcard->EmptySymbol;
//				$jCounter++;
//			}
//			if ($showOverallRanking)
//			{
//				$ExportArr[$iCounter][$jCounter] = $lreportcard->EmptySymbol;
//				$jCounter++;
//			}
//				$ExportArr[$iCounter] = array_merge($ExportArr[$iCounter],$EmptySymbols) ;
//				$iCounter++;
		}
		$jCounter=0;
		$ExportArr[] = array('');
		
		# Pass number of form, each class and gender
		if(empty($SubjectID))
		{
			foreach ((array)$resultAverage as $StudentID => $GrandMarksArr)
			{
				$ClassName = $studentIDClassMap[$StudentID];
				$Gender = $StudentInfo[$StudentID][0]["Gender"];
				$GrandAvgNature = $GrandAvgNatureArr[$StudentID];
				$ClassStudentPassArr[$ClassName]["NumOfStudent"][$Gender]++;
				if($GrandAvgNature!='Fail')
					$ClassStudentPassArr[$ClassName]["Pass"][$Gender]++;
			}
			
			$GenderAry = array("M","F");
			foreach((array)$ClassStudentPassArr as $ClassName => $ClassInfo)
			{
				#Display Class Pass
				$ClassNumOfStudent = @array_sum($ClassInfo["NumOfStudent"]);
				$ClassNumOfPass = @array_sum($ClassInfo["Pass"]);
				
				$ClassExportColArr = array();
				$ClassExportColArr[]=$ClassName;
				$ClassExportColArr[]=$ClassNumOfStudent>0?$ClassNumOfStudent:0;
				$ClassExportColArr[]=$ClassNumOfPass>0?$ClassNumOfPass:0;
				$ClassExportRowArr[] = $ClassExportColArr;
				
				# Display Gender Pass 
				foreach($GenderAry as $Gender)
				{
					$GenderNumOfStudent = $ClassInfo["NumOfStudent"][$Gender];
					$GenderNumOfPass = $ClassInfo["Pass"][$Gender];
					
					$ClassExportColArr = array();
					$ClassExportColArr[]=$eReportCard['GrandMarkSheet']['Gender'][$Gender];
					$ClassExportColArr[]=$GenderNumOfStudent>0?$GenderNumOfStudent:0;
					$ClassExportColArr[]=$GenderNumOfPass>0?$GenderNumOfPass:0;
					$ClassExportRowArr[] = $ClassExportColArr;
					$FormGenderNumOfStudent[$Gender] += $GenderNumOfStudent;
					$FormGenderNumOfPass[$Gender] += $GenderNumOfPass;
				}
				
				$FormNumOfStudent += $ClassNumOfStudent;
				$FormNumOfPass += $ClassNumOfPass;
			}
	
			# Calculate and Display Form Pass  
			# whole form stat
			$FormExportColArr = array();
			$FormExportColArr[]=$eReportCard["WholeForm"];
			$FormExportColArr[]=$FormNumOfStudent>0?$FormNumOfStudent:0;
			$FormExportColArr[]=$FormNumOfPass>0?$FormNumOfPass:0;
			$FormExportRowArr[] = $FormExportColArr;
			
			# form gender stat
			foreach($GenderAry as $Gender)
			{
				$GenderNumOfStudent = $FormGenderNumOfStudent[$Gender];
				$GenderNumOfPass = $FormGenderNumOfPass[$Gender];
				
				$FormExportColArr = array();
				$FormExportColArr[]=$eReportCard['GrandMarkSheet']['Gender'][$Gender];
				$FormExportColArr[]=$GenderNumOfStudent>0?$GenderNumOfStudent:0;
				$FormExportColArr[]=$GenderNumOfPass>0?$GenderNumOfPass:0;
				$FormExportRowArr[] = $FormExportColArr;
				
			}
			
			$HeaderExportColArr = array();
			$HeaderExportColArr[] = '';
			$HeaderExportColArr[] = $eReportCard["TotalNum"];
			$HeaderExportColArr[] = $eReportCard["Template"]["GrandAvgPassNumber"];
			$HeaderExportRowArr[] = $HeaderExportColArr;
			
			$ExportArr = array_merge($ExportArr,$HeaderExportRowArr,$FormExportRowArr,$ClassExportRowArr);
		}

        // Added to export Print Date
        if($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayPrintReportDateTime'])
        {
            $ExportDateTimeArr = array();
            $ExportDateTimeArr[][0] = "";
            $ExportDateTimeArr[][0] = $eReportCard['MasterReport']['PrintDate'] . ":&nbsp;" . date('j/n/Y H:i');

            $ExportArr = array_merge($ExportArr,$ExportDateTimeArr);
        }

		// Title of the grandmarksheet
		//$schoolData = split("\n",get_file_content($PATH_WRT_ROOT."file/school_data.txt"));
		$schoolData = explode("\n", get_file_content($PATH_WRT_ROOT."file/school_data.txt"));
		$schoolName = $schoolData[0];
		$academicYear = $lreportcard->GET_ACTIVE_YEAR("-");
		
		if ($ForSSPA)
		{
			// SSPA report contains last year information, so the acadermic year of the title is across two acadermic years
			$academicYearArr = explode("-", $academicYear);
			$academicYearArr[0] -= 1;
			$academicYear = implode("-", $academicYearArr);
			
			$GMSTitle = "$academicYear $ClassLevelName $ColumnTitle $SubjectName ".$eReportCard['Template']['GrandMarksheet'];
			$GMSTitle .= " (SSPA)";
		}
		else
		{
			$GMSTitle = "$academicYear $ClassLevelName $ColumnTitle $SubjectName ".$eReportCard['Template']['GrandMarksheet'];	
		}
		//$GMSTitle = iconv("UTF-8", "Big5", $GMSTitle);
		//$filename = intranet_undo_htmlspecialchars($GMSTitle.".csv");
		$filename = $GMSTitle.".csv";
		$filename = str_replace(' ', '_', $filename);
		
		$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "", "\r\n", "", 0, "11");
		intranet_closedb();
		
		// Output the file to user browser
		$lexport->EXPORT_FILE($filename, $export_content);
		
	} else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>