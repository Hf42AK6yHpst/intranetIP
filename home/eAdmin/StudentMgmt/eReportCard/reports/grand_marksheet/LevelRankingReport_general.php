<?php
// using :

@SET_TIME_LIMIT(1000);
@ini_set(memory_limit, "800M");
 
/*****************************************
 * modification log
 *  20200512 Bill:
 *      support Print Date display  ($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayPrintReportDateTime'])
 *  20190605 Bill:  [2019-0123-1731-14066]
 *      added Total Unit(s) Failed column   ($eRCTemplateSetting['Report']['GrandMarksheet']['ShowTotalUnitFailed'])
 *      always display marks according to decimal places settings  ($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayRoundMarks'])
 *  20180628 Bill:  [2017-1204-1601-38164]
 *      set target active year  ($eRCTemplateSetting['Report']['SupportAllActiveYear'])
 * 	20170801 Bill:	[2016-1214-1548-50240]
 * 		Support Grand Average Grade Adjustment	($eRCTemplateSetting['GrandAverageGradeManualAdjustment'])
 * 	20170717 Bill:	[2017-0706-1012-27236]
 * 		Statistics - Lowest Mark: remove -1 from Minimum Score Calculation 
 * 	20170524 Bill:
 * 		- set time and memory limit, prevent no response
 * 	20170111 Bill:	[DM#3136]
 * 		- fixed PHP 5.4 error split()
 * 	20161213 Bill:
 * 		use grade instead of mark for special case checking	[2016-1130-1208-41236]
 * 	20160128 Bill:
 * 			- support multiple Other Info data display	[2016-0128-1008-07066]
 * 	20150402 Bill:
 * 			- add Display User Login [2015-0306-1036-36206]
 * 		20100416 Marcus:
 * 			modified ranking - if ordermeritstream exist, show it instead of ordermeritform 
 * 	20100212 Ivan:
 * 		fixed: display N.A. as zero mark 
 * 	20100209 Marcus:
 * 		modify coding to cater conversion from grand marks to grade 
 * 	20100120 Marcus:
 * 		modify coding to cater multi selection of subjects 
 * 	21010107 Marcus:
 *		- add table showing Pass number of form, each class and gender 
 * 		- add Display Subject to select Abbr/Desc/ShortName of Subject Name
 * 		- fixed sql did not sort ordermeritform numerically
 * ***************************************/

// Root path
$PATH_WRT_ROOT = "../../../../../../";

// Page access right
$PageRight = "TEACHER";

// Include general libraries
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

// Authorize intranet user & connect Database
intranet_auth();
intranet_opendb();

// Check if ReportCard module is enable
if ($plugin['ReportCard2008'])
{
	// Include ReportCard libraries
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	}
	else {
		$lreportcard = new libreportcard();
	}
	
	// [2017-1204-1601-38164] Set target active year
	if($eRCTemplateSetting['Report']['SupportAllActiveYear'])
	{
	    $targetActiveYear = stripslashes($_REQUEST['targetActiveYear']);
	    if(!empty($targetActiveYear)){
	        $targetYearID = $lreportcard->SET_TEMP_ACTIVE_YEAR($targetActiveYear);
	    }
	}
	
	# $linterface = new interface_html();
	$lclass = new libclass();
	
	// Check module access right
	if ($lreportcard->hasAccessRight())
	{
		// Check required data submitted from index.php
		// optional: $SubjectID, $ReportColumnID
		if (!isset($ClassLevelID, $ClassID, $ReportID) || empty($ClassLevelID) || empty($ReportID) || empty($ClassID)) {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		# get display options
		// 1. student name display (EnglishName, ChineseName)
		$showEnglishName = false;
		$showChineseName = false;
		if ($StudentNameDisplay == NULL)
		{
			$numOfNameColumn = 0;
		}
		else
		{
			$StudentNameDisplay = explode(",", $StudentNameDisplay);
			$numOfNameColumn = sizeof($StudentNameDisplay);
			foreach ($StudentNameDisplay as $key => $value)
			{
				if ($value == "EnglishName") $showEnglishName = true;
				if ($value == "ChineseName") $showChineseName = true;
			}
		}		
		// 2. ranking display (ClassRanking, OverallRanking)
		$showClassRanking = false;
		$showOverallRanking = false;
		$RankingDisplay = explode(",", $RankingDisplay);
		foreach ($RankingDisplay as $key => $value)
		{
			if ($value == "ClassRanking") $showClassRanking = true;
			if ($value == "OverallRanking") $showOverallRanking = true;
		}
   		// 3.1 show subject (0,1)
		$showSubject = $ShowSubject;		
		// 3.2 show subject component (0,1)
		$showSubjectComponent = $ShowSubjectComponent;
		// 4. show Summary Info (0,1)
		$showSummaryInfo = $ShowSummaryInfo;		
		// 5. show Grand Total (0,1)
		$showGrandTotal = $ShowGrandTotal;		
		// 6. show Grand Average (0,1)
		$showGrandAverage = $ShowGrandAverage;
		// 7. ForSSPA => no decimal place in all marks
		$ForSSPA = $ForSSPA;
		// 8. show Statistics (0,1)
		$showStatistics = $ShowStatistics;
		// 9. show GPA (0,1)
		$showGPA = $ShowGPA;
		// 10. show Gender (0,1)
		$showGender = $ShowGender;
		// 11. show User Login (0,1) [2015-0306-1036-36206]
		$showUserLogin = $ShowUserLogin;
		// 12. show Total Unit(s) Failed (0,1)
		$showTotalUnitFailed = ($_GET['ShowTotalUnitFailed'])? 1 : 0;
		
		# GrandMarks Grade
		$showGrandTotalGrade = $ShowGrandTotalGrade;
		$showGrandAverageGrade= $ShowGrandAverageGrade;
		$showGPAGrade= $ShowGPAGrade;
		$showActualAverage = ($_GET['ShowActualAverage'] && empty($ReportColumnID))? 1 : 0;
		$showActualAverageGrade = ($_GET['ShowActualAverageGrade'] && empty($ReportColumnID))? 1 : 0;
		
		// SubjectID has 2 Condition
		// 1.no of subject selected >1 , show overall rank
		// 2.no of subject selected ==1 , show rank of specific subject
		$SelectedSubjectID = $SubjectID;
		if(count($SubjectID)==1)
			$SubjectID = $SubjectID[0];
		else
			unset($SubjectID);
		
		$reportCalculationOrder = $lreportcard->returnReportTemplateCalculation($ReportID);
		
		// Get ReportCard info
		$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		$SemID = $reportTemplateInfo['Semester'];
		if ($SemID != "F") {
		    $SemesterNumber = $lreportcard->Get_Semester_Seq_Number($SemID);
		}
		
		// Check to make sure it is a generated report
		if ($reportTemplateInfo["LastGenerated"] == "0000-00-00 00:00:00" || $reportTemplateInfo["LastGenerated"] == "") {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		// Get subjects of the current ClassLevel
		$FormSubjectArr = $lreportcard->returnSubjectwOrder($ClassLevelID, 1, '', $intranet_session_language,$SubjDisplay);
		
		$CmpSubjectIDList = array();
		$ParentSubjectIDList = array();
		$ParentCmpSubjectNum = array();
		
		// Reformat the subject array - Display component subjects also
		$FormSubjectArrFlat = array();
		if (sizeof($FormSubjectArr) > 0) {
			foreach($FormSubjectArr as $tmpSubjectID => $tmpSubjectName) {
				if (sizeof($tmpSubjectName) > 1) {
					$ParentSubjectIDList[] = $tmpSubjectID;
					$ParentCmpSubjectNum[$tmpSubjectID] = sizeof($tmpSubjectName) - 1;
					foreach ($tmpSubjectName as $tmpCmpSubjectID => $tmpCmpSubjectName) {
						if ($tmpCmpSubjectID != 0) {
							$CmpSubjectIDList[] = $tmpCmpSubjectID;
							$FormSubjectArrFlat[$tmpCmpSubjectID] = array(0 => $tmpCmpSubjectName);
						}
					}
				}
				$FormSubjectArrFlat[$tmpSubjectID] = array(0 => $tmpSubjectName[0]);
			}
		}
		$FormSubjectArr = $FormSubjectArrFlat;
		
		// Get component subjects if SubjectID is provided
		if (isset($SubjectID) && $SubjectID !== "" && in_array($SubjectID, $ParentSubjectIDList)) {
			$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID,$intranet_session_language, $SubjDisplay);
			$CmpSubjectIDNameArr = array();
			$CmpSubjectIDArr = array();
			for($i=0; $i<sizeof($CmpSubjectArr); $i++) {
				$CmpSubjectIDArr[] = $CmpSubjectArr[$i]["SubjectID"];
				$CmpSubjectIDNameArr[$CmpSubjectArr[$i]["SubjectID"]] = $CmpSubjectArr[$i]["SubjectName"];
			}
		}
		
		// Get subjects' grading scheme of the current ClassLevel
		$FormSubjectGradingSchemeArr = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID,0,$ReportID);
		
		// Get ClassLevel name
		$ClassLevelName = $lreportcard->returnClassLevel($ClassLevelID);
		
		// Get Classes
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		
		// Get current school year
		$SchoolYear = $lreportcard->schoolYear;
		
		// Get columns info of the current ReportCard
		$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
		$ColumnIDList = array();
		$ColumnIDList = ($ColumnTitleAry == NULL)? NULL : array_keys($ColumnTitleAry);
		
		$studentIDList = array();
		$studentIDNameMap = array();
		$studentIDRegNoMap = array();
		
		// Get the list of students (in entire class level)
		$studentList = array();
		$studentIDList = array();
		$studentIDNameMap = array();
		$studentIDClassMap = array();
		$studentIDClassNumMap = array();
		$studentIDRegNoMap = array();
		$studentFailUnitArr = array();
		$ClassNameArr = array();
		for($i=0; $i<sizeof($ClassArr); $i++) {
			$ClassNameArr[] = $ClassArr[$i]["ClassName"];
			$ClassIDArr[] = $ClassArr[$i]["ClassID"];
			$studentList = $lreportcard->GET_STUDENT_BY_CLASS($ClassArr[$i]["ClassID"], "", 1);
			for($j=0; $j<sizeof($studentList); $j++) {
				$studentIDNameMap[$studentList[$j]["UserID"]]['En'] = $studentList[$j]['StudentNameEn'];
				$studentIDNameMap[$studentList[$j]["UserID"]]['Ch'] = $studentList[$j]['StudentNameCh'];
			
				$studentIDClassNumMap[$studentList[$j]["UserID"]] = $studentList[$j][2];
				$studentIDClassMap[$studentList[$j]["UserID"]] = $ClassArr[$i]["ClassName"];
				$studentIDRegNoMap[$studentList[$j]["UserID"]] = $studentList[$j]["WebSAMSRegNo"];
				$studentIDList[] = $studentList[$j]["UserID"];
				
				// [2019-0123-1731-14066] Get Student Total Unit(s) Failed
				if($eRCTemplateSetting['Report']['GrandMarksheet']['ShowTotalUnitFailed'] && $showTotalUnitFailed) {
				    $studentFailUnitInfo = $lreportcard->returnTemplateSubjectCol($ReportID, $ClassLevelID, $studentList[$j]["UserID"]);
				    $studentFailUnitInfo = $studentFailUnitInfo['FailUnitArr'];
				    $studentFailUnitArr[$studentList[$j]["UserID"]] = $SemesterNumber > 0? $studentFailUnitInfo[$SemesterNumber - 1] : $studentFailUnitInfo['Final'];
				}
			}
		}
		
		// Students should be already sorted by Class Number in the SQL Query
		$studentIDListSql = implode(",", $studentIDList);
		$cond = "";
		
		// Get overall mark or term/assessment mark
		$ColumnTitle = "";
		if (!isset($ReportColumnID) || empty($ReportColumnID)) {
			$ColumnTitle = $eReportCard['Template']['OverallCombined'];
			//$cond .= "AND (ReportColumnID = '' OR ReportColumnID = '0') ";
			$cond .= "AND (a.ReportColumnID = '' OR a.ReportColumnID = '0') ";
			$reportColumnID = 0;
		}
		else {
			$ColumnTitle = $ColumnTitleAry[$ReportColumnID];
			//$cond .= "AND ReportColumnID = '$ReportColumnID' ";
			$cond .= "AND a.ReportColumnID = '$ReportColumnID' ";
			$reportColumnID = $ReportColumnID;
		}
		
		// Get individual subject mark or grand average (which table to retrieve)
		$SubjectName = "";
		if (!isset($SubjectID) || empty($SubjectID)) {
			// No SubjectID provided, get the averaage result
			
			# modified for manual adjustment
			/*$table = $lreportcard->DBName.".RC_REPORT_RESULT";
			$fieldToSelect = "StudentID, GrandTotal, GrandAverage, OrderMeritClass, OrderMeritForm";
			
			$sql = "SELECT $fieldToSelect FROM $table WHERE ";
			$sql .= "ReportID = '$ReportID' ";
			$sql .= "AND StudentID IN ($studentIDListSql) ";
			$sql .= $cond;
			$sql .= "ORDER BY OrderMeritForm";
			*/
			$sql = "
				SELECT 
					a.StudentID, 
					a.GrandTotal, 
					a.GrandAverage,
					a.ActualAverage,
					a.GPA, 
					a.OrderMeritClass, 
					IF(a.OrderMeritStream IS NULL OR TRIM(a.OrderMeritStream) = '',IFNULL(b.AdjustedValue,a.OrderMeritForm),a.OrderMeritStream) as OrderMeritForm
				FROM 
					".$lreportcard->DBName.".RC_REPORT_RESULT a
					LEFT JOIN ".$lreportcard->DBName.".RC_MANUAL_ADJUSTMENT b ON a.ReportID = b.ReportID AND a.StudentID = b.StudentID AND a.ReportColumnID = b.ReportColumnID AND b.OtherInfoName = 'OrderMeritForm' AND b.SubjectID = 0
				WHERE 
					a.ReportID = '$ReportID' 
					AND a.StudentID IN ($studentIDListSql) 
					$cond
				ORDER BY 
					Round(OrderMeritForm)
			";
			$tempArr = $lreportcard->returnArray($sql);
			
			$resultAverage = array();
			$studentIDListSorted = $studentIDList;
			if (sizeof($tempArr) > 0) {
				$studentIDListSorted = array();
				for($j=0; $j<sizeof($tempArr); $j++)
				{
					# for manual adjustment
					$ManualAjustedMark = $lreportcard->Get_Manual_Adjustment($ReportID, $tempArr[$j]["StudentID"], $ReportColumnID, 0);
					$thisGrandMark = $ManualAjustedMark[$tempArr[$j]["StudentID"]][$reportColumnID][0];
					
					$studentIDListSorted[] = $tempArr[$j]["StudentID"];
					$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = $thisGrandMark["GrandTotal"]?$thisGrandMark["GrandTotal"]:$tempArr[$j]["GrandTotal"];
					$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = $thisGrandMark["GrandAverage"]?$thisGrandMark["GrandAverage"]:$tempArr[$j]["GrandAverage"];
					$resultAverage[$tempArr[$j]["StudentID"]]["ActualAverage"] = $thisGrandMark["ActualAverage"]?$thisGrandMark["ActualAverage"]:$tempArr[$j]["ActualAverage"];
					$resultAverage[$tempArr[$j]["StudentID"]]["GPA"] = $thisGrandMark["GPA"]?$thisGrandMark["GPA"]:$tempArr[$j]["GPA"];
					$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverageGrade"] = $thisGrandMark["GrandAverageGrade"]?$thisGrandMark["GrandAverageGrade"]:"";
						
					# For Term Report Average Grade Adjustment
					if($eRCTemplateSetting['GrandAverageGradeManualAdjustment'] && $SemID == "F" && $ReportColumnID > 0) {
						$TermReportID = $lreportcard->getCorespondingTermReportID($ReportID, $ReportColumnID);
						if(!empty($TermReportID) && $TermReportID != $ReportID) {
							$TermManualAjustedMark = $lreportcard->Get_Manual_Adjustment($TermReportID, $tempArr[$j]["StudentID"], 0, 0);
							$thisTermGrandMark = $TermManualAjustedMark[$tempArr[$j]["StudentID"]][0][0];
							$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverageGrade"] = $thisTermGrandMark["GrandAverageGrade"]?$thisTermGrandMark["GrandAverageGrade"]:$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverageGrade"];
						}
					}
					
					# Round up all the marks if for SSPA
					if ($ForSSPA)
					{
						$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = round($thisGrandMark["GrandTotal"]?$thisGrandMark["GrandTotal"]:$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"]);
						$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = round($thisGrandMark["GrandAverage"]?$thisGrandMark["GrandAverage"]:$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"]);
						$resultAverage[$tempArr[$j]["StudentID"]]["ActualAverage"] = round($thisGrandMark["ActualAverage"]?$thisGrandMark["ActualAverage"]:$resultAverage[$tempArr[$j]["StudentID"]]["ActualAverage"]);
						$resultAverage[$tempArr[$j]["StudentID"]]["GPA"] = round($thisGrandMark["GPA"]?$thisGrandMark["GPA"]:$resultAverage[$tempArr[$j]["StudentID"]]["GPA"]);
					}
					
					// [2019-0123-1731-14066] add rounding logic
					if($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayRoundMarks'])
					{
					    if(is_numeric($resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"]) && $resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] != -1) {
					        $resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = $lreportcard->ROUND_MARK($resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"], "GrandTotal");
					    }
					    if(is_numeric($resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"]) && $resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] != -1) {
					        $resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = $lreportcard->ROUND_MARK($resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"], "GrandAverage");
					    }
					    if(is_numeric($resultAverage[$tempArr[$j]["StudentID"]]["GPA"]) && $resultAverage[$tempArr[$j]["StudentID"]]["GPA"] != -1) {
					        $resultAverage[$tempArr[$j]["StudentID"]]["GPA"] = $lreportcard->ROUND_MARK($resultAverage[$tempArr[$j]["StudentID"]]["GPA"], "GrandAverage");
					    }
					}
					
					# SubjectID = -1 => GrandAverage
					$GrandAvgNatureArr[$tempArr[$j]["StudentID"]] = $lreportcard->returnMarkNature($ClassLevelID,-1,$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"],'','', $ReportID);
					$ActualAvgNatureArr[$tempArr[$j]["StudentID"]] = $lreportcard->returnMarkNature($ClassLevelID,-1,$resultAverage[$tempArr[$j]["StudentID"]]["ActualAverage"],'','', $ReportID);
					
					if ($tempArr[$j]["OrderMeritClass"] > 0 || $thisGrandMark["OrderMeritClass"]) {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritClass"] = $thisGrandMark["OrderMeritClass"]?$thisGrandMark["OrderMeritClass"]:$tempArr[$j]["OrderMeritClass"];
					}
					else {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritClass"] = "-";
					}
					
					if ($tempArr[$j]["OrderMeritForm"] > 0 || $thisGrandMark["OrderMeritForm"]) {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritForm"] = $thisGrandMark["OrderMeritForm"]?$thisGrandMark["OrderMeritForm"]:$tempArr[$j]["OrderMeritForm"];
					}
					else {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritForm"] = "-";
					}
				}
			}
			
			// Get all subject marks as additional information
			foreach($FormSubjectArr as $tmpSubjectID => $tmpSubjectName) {
				$subjectGradingScheme = $FormSubjectGradingSchemeArr[$tmpSubjectID];
				$subjectGradingSchemeInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($subjectGradingScheme["schemeID"]);
				
				/*
				if ($subjectGradingScheme["scaleDisplay"] == "M") {
					$fieldToSelect = "StudentID, Mark As MarkUsed";
				} else {
					$fieldToSelect = "StudentID, Grade As MarkUsed";
				}
				$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE";
				
				$sql = "SELECT $fieldToSelect FROM $table WHERE ";
				$sql .= "ReportID = '$ReportID' ";
				$sql .= "AND StudentID IN ($studentIDListSql) ";
				$sql .= "AND  SubjectID = '$tmpSubjectID' ";
				$sql .= $cond;
				*/
				
				if ($subjectGradingScheme["scaleDisplay"] == "M") {
					$fieldToSelect = "a.StudentID, IFNULL(b.AdjustedValue,a.Mark) As MarkUsed, IFNULL(b.AdjustedValue,a.Grade)as Grade";
				}
				else {
					$fieldToSelect = "a.StudentID, IFNULL(b.AdjustedValue,a.Grade) As MarkUsed, IFNULL(b.AdjustedValue,a.Grade)as Grade";
				}
				$sql = "
					SELECT 
						$fieldToSelect 
					FROM 
						".$lreportcard->DBName.".RC_REPORT_RESULT_SCORE a
						LEFT JOIN ".$lreportcard->DBName.".RC_MANUAL_ADJUSTMENT b 
							ON 
								a.ReportID = b.ReportID 
								AND a.StudentID = b.StudentID 
								AND a.ReportColumnID = b.ReportColumnID 
								AND a.SubjectID = b.SubjectID 
								AND b.OtherInfoName = 'Score'
						
					WHERE 
						a.ReportID = '$ReportID' 
						AND a.StudentID IN ($studentIDListSql) 
						AND a.SubjectID = '$tmpSubjectID' 
						$cond
					";
				
				$tempArr = $lreportcard->returnArray($sql);
				
				$resultSubject[$tmpSubjectID] = array();
				$resultSubjectGrade[$tmpSubjectID] = array();
				if (sizeof($tempArr) > 0) {
					for($j=0; $j<sizeof($tempArr); $j++)
					{
						$thisMark = $tempArr[$j]["MarkUsed"];
						$thisGrade = $tempArr[$j]["Grade"];
						list($thisMark, $needStyle) = $lreportcard->checkSpCase($ReportID, $tmpSubjectID, $thisMark, $thisGrade);
						
						$resultSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = $thisMark;
						$resultSubjectGrade[$tmpSubjectID][$tempArr[$j]["StudentID"]] = $thisGrade;
						
						//$resultSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = $tempArr[$j]["MarkUsed"];
						
						# Round up all the marks if for SSPA
						if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M")
						{
							$resultSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = round($resultSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]]);
						}
					}
				}
			}
		}
		else {
			$subjectGradingScheme = $FormSubjectGradingSchemeArr[$SubjectID];
			$subjectGradingSchemeInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($subjectGradingScheme["schemeID"]);
			
			/*if ($subjectGradingScheme["scaleDisplay"] == "M") {
				$fieldToSelect = "StudentID, OrderMeritClass, OrderMeritForm, Mark As MarkUsed";
			} else {
				$fieldToSelect = "StudentID, OrderMeritClass, OrderMeritForm, Grade As MarkUsed";
			}
			$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE";
			
			$sql = "SELECT $fieldToSelect FROM $table WHERE ";
			$sql .= "ReportID = '$ReportID' ";
			$sql .= "AND StudentID IN ($studentIDListSql) ";
			$sql .= "AND SubjectID = '$SubjectID' ";
			*/
			if ($subjectGradingScheme["scaleDisplay"] == "M") {
				$fieldToSelect = "a.StudentID, a.OrderMeritClass, IF(a.OrderMeritStream IS NULL OR TRIM(a.OrderMeritStream) = '',IFNULL(b.AdjustedValue,a.OrderMeritForm),a.OrderMeritStream) as OrderMeritForm, a.Mark As MarkUsed, IFNULL(b.AdjustedValue,a.Grade)as Grade";
			}
			else {
				$fieldToSelect = "a.StudentID, a.OrderMeritClass, IF(a.OrderMeritStream IS NULL OR TRIM(a.OrderMeritStream) = '',IFNULL(b.AdjustedValue,a.OrderMeritForm),a.OrderMeritStream) as OrderMeritForm, a.Grade As MarkUsed, IFNULL(b.AdjustedValue,a.Grade)as Grade";
			}
			$sql = "
				SELECT 
					$fieldToSelect
				FROM 
					".$lreportcard->DBName.".RC_REPORT_RESULT_SCORE a 
					LEFT JOIN ".$lreportcard->DBName.".RC_MANUAL_ADJUSTMENT b ON 
						a.ReportID = b.ReportID 
						AND a.StudentID = b.StudentID 
						AND a.ReportColumnID = b.ReportColumnID 
						AND a.SubjectID = b.SubjectID 
						AND b.OtherInfoName = 'OrderMeritForm'
				WHERE 
					a.ReportID = '$ReportID' 
					AND a.StudentID IN ($studentIDListSql) 
					AND a.SubjectID = '$SubjectID'
			";
			$tempArr = $lreportcard->returnArray($sql.$cond."ORDER BY round(OrderMeritForm)");
			
			$resultSubject = array();
			$resultSubjectGrade = array();
			$studentIDListSorted = array();
			$OrderFormSubject = array();
			if (sizeof($tempArr) > 0) {
				for($j=0; $j<sizeof($tempArr); $j++)
				{
					$studentIDListSorted[] = $tempArr[$j]["StudentID"];
					
					# for manual adjustment
					$ManualAjustedMark = $lreportcard->Get_Manual_Adjustment($ReportID, $tempArr[$j]["StudentID"], $ReportColumnID, $SubjectID);
					$thisSubjectOrder = $ManualAjustedMark[$tempArr[$j]["StudentID"]][$reportColumnID][$SubjectID];
					
					$thisMark = $tempArr[$j]["MarkUsed"];
					$thisGrade = $tempArr[$j]["Grade"];
					list($thisMark, $needStyle) = $lreportcard->checkSpCase($ReportID, $SubjectID, $thisMark, $thisGrade);
					
					$resultSubject[$tempArr[$j]["StudentID"]] = $thisSubjectOrder["Score"]? $thisSubjectOrder["Score"] : $thisMark;
					$resultSubjectGrade[$tempArr[$j]["StudentID"]] = $thisSubjectOrder["Score"]? $thisSubjectOrder["Score"] : $thisGrade;
					
					# Round up all the marks if for SSPA
					if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M")
					{
						$resultSubject[$tempArr[$j]["StudentID"]] = round($thisSubjectOrder["Score"]? $thisSubjectOrder["Score"] : $resultSubject[$tempArr[$j]["StudentID"]]);
					}
					
					if ($tempArr[$j]["OrderMeritClass"] > 0 || $thisSubjectOrder["OrderMeritClass"]) {
						$OrderClassSubject[$tempArr[$j]["StudentID"]] = $thisSubjectOrder["OrderMeritClass"]?$thisSubjectOrder["OrderMeritClass"]:$tempArr[$j]["OrderMeritClass"];
					}
					else {
						$OrderClassSubject[$tempArr[$j]["StudentID"]] = "-";
					}
					if ($tempArr[$j]["OrderMeritForm"] > 0 || $thisSubjectOrder["OrderMeritForm"]) {
						$OrderFormSubject[$tempArr[$j]["StudentID"]] = $thisSubjectOrder["OrderMeritForm"]?$thisSubjectOrder["OrderMeritForm"]:$tempArr[$j]["OrderMeritForm"];
					}
					else {
						$OrderFormSubject[$tempArr[$j]["StudentID"]] = "-";
					}
				}
			}
			
			// If gettng subject overall mark for a single subject, get the column mark also
			if (!isset($ReportColumnID) || empty($ReportColumnID)) {
				for($i=0; $i<sizeof($ColumnIDList); $i++)
				{
					$tmpCond = "AND a.ReportColumnID = '".$ColumnIDList[$i]."'";
					$tempArr = $lreportcard->returnArray($sql.$tmpCond);
										
					# for manual adjustment
					$ManualAjustedMark = $lreportcard->Get_Manual_Adjustment($ReportID, "", $ColumnIDList[$i], $SubjectID);
					
					$resultSubjectColumn[$ColumnIDList[$i]] = array();
					if (sizeof($tempArr) > 0) {
						for($j=0; $j<sizeof($tempArr); $j++)
						{
							$thisSubjectMark = $ManualAjustedMark[$tempArr[$j]["StudentID"]][$ColumnIDList[$i]][$SubjectID];
							
							$thisMark = $tempArr[$j]["MarkUsed"];
							$thisGrade = $tempArr[$j]["Grade"];
							list($thisMark, $needStyle) = $lreportcard->checkSpCase($ReportID, $SubjectID, $thisMark, $thisGrade);
					
							$resultSubjectColumn[$ColumnIDList[$i]][$tempArr[$j]["StudentID"]] = $thisSubjectMark["Score"]? $thisSubjectMark["Score"] : $thisMark;
							
							# Round up all the marks if for SSPA
							if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M")
							{
								$resultSubjectColumn[$ColumnIDList[$i]][$tempArr[$j]["StudentID"]] = round($thisSubjectMark["Score"]?$thisSubjectMark["Score"]:$resultSubjectColumn[$ColumnIDList[$i]][$tempArr[$j]["StudentID"]]);
							}
						}
					}
				}
			}
			
			#################################
			// Special handling of subject which having component subjects
			if (in_array($SubjectID, $ParentSubjectIDList)) {
				// Push the SubjectID into the same array of its component subjects
				$SubjectPlusCompSubjectIDList = $CmpSubjectIDArr;
				$SubjectPlusCompSubjectIDList[] = $SubjectID;
				
				for($i=0; $i<sizeof($SubjectPlusCompSubjectIDList); $i++) {
					$tmpSubjectID = $SubjectPlusCompSubjectIDList[$i];
					$subjectGradingScheme = $FormSubjectGradingSchemeArr[$tmpSubjectID];
					$subjectGradingSchemeInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($subjectGradingScheme["schemeID"]);
					
					/*if ($subjectGradingScheme["scaleDisplay"] == "M") {
						$fieldToSelect = "StudentID, Mark As MarkUsed";
					} else {
						$fieldToSelect = "StudentID, Grade As MarkUsed";
					}
					$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE";
					
					$sql = "SELECT $fieldToSelect FROM $table WHERE ";
					$sql .= "ReportID = '$ReportID' ";
					$sql .= "AND StudentID IN ($studentIDListSql) ";
					$sql .= "AND  SubjectID = '$tmpSubjectID' ";
					$sql .= $cond;*/

					if ($subjectGradingScheme["scaleDisplay"] == "M") {
						$fieldToSelect = "a.StudentID, IFNULL(b.AdjustedValue,a.Mark) As MarkUsed, IFNULL(b.AdjustedValue,a.Grade)as Grade";
					}
					else {
						$fieldToSelect = "a.StudentID, IFNULL(b.AdjustedValue,a.Grade) As MarkUsed, IFNULL(b.AdjustedValue,a.Grade)as Grade";
					}
					$sql = "
						SELECT 
							$fieldToSelect 
						FROM 
							".$lreportcard->DBName.".RC_REPORT_RESULT_SCORE a
							LEFT JOIN ".$lreportcard->DBName.".RC_MANUAL_ADJUSTMENT b 
								ON 
									a.ReportID = b.ReportID 
									AND a.StudentID = b.StudentID 
									AND a.ReportColumnID = b.ReportColumnID 
									AND a.SubjectID = b.SubjectID 
									AND b.OtherInfoName = 'Score'
						WHERE 
							a.ReportID = '$ReportID' 
							AND a.StudentID IN ($studentIDListSql) 
							AND a.SubjectID = '$tmpSubjectID' 
							$cond
						";
					$tempArr = $lreportcard->returnArray($sql);

					$resultDetailSubject[$tmpSubjectID] = array();
					$resultDetailSubjectGrade[$tmpSubjectID] = array();
					if (sizeof($tempArr) > 0) {
						for($j=0; $j<sizeof($tempArr); $j++)
						{
							$thisMark = $tempArr[$j]["MarkUsed"];
							$thisGrade = $tempArr[$j]["Grade"];
							list($thisMark, $needStyle) = $lreportcard->checkSpCase($ReportID, $tmpSubjectID, $thisMark, $thisGrade);
							
							$resultDetailSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = $thisMark;
							$resultDetailSubjectGrade[$tmpSubjectID][$tempArr[$j]["StudentID"]] = $thisGrade;
							
							//$resultDetailSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = $tempArr[$j]["MarkUsed"];
							
							# Round up all the marks if for SSPA
							if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M")
							{
								$resultDetailSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = round($resultDetailSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]]);
							}
						}
					}
				}
			}
			
			#################################
			# get Grand Total and Grand Average
			$table = $lreportcard->DBName.".RC_REPORT_RESULT";
			$fieldToSelect = "StudentID, GrandTotal, GrandAverage, ActualAverage, GPA, OrderMeritClass, IF(OrderMeritStream IS NULL OR TRIM(OrderMeritStream) = '',OrderMeritForm,a.OrderMeritStream)";
			
			$sql = "SELECT $fieldToSelect FROM $table a WHERE ";
			$sql .= "ReportID = '$ReportID' ";
			$sql .= "AND StudentID IN ($studentIDListSql) ";
			$sql .= $cond;
			$tempArr = $lreportcard->returnArray($sql);
			
			$resultAverage = array();

			if (sizeof($tempArr) > 0) {
				for($j=0; $j<sizeof($tempArr); $j++)
				{
					# for manual adjustment
					$ManualAjustedMark = $lreportcard->Get_Manual_Adjustment($ReportID, $tempArr[$j]["StudentID"], $ReportColumnID, 0);
					$thisGrandMark = $ManualAjustedMark[$tempArr[$j]["StudentID"]][$reportColumnID][0];
					
					$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = $thisGrandMark["GrandTotal"]?$thisGrandMark["GrandTotal"]:$tempArr[$j]["GrandTotal"];
					$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = sprintf("%02.2f", $thisGrandMark["GrandAverage"]?$thisGrandMark["GrandAverage"]:$tempArr[$j]["GrandAverage"]);
					$resultAverage[$tempArr[$j]["StudentID"]]["ActualAverage"] = sprintf("%02.2f", $thisGrandMark["ActualAverage"]?$thisGrandMark["ActualAverage"]:$tempArr[$j]["ActualAverage"]);
					$resultAverage[$tempArr[$j]["StudentID"]]["GPA"] = sprintf("%02.2f", $thisGrandMark["GPA"]?$thisGrandMark["GPA"]:$tempArr[$j]["GPA"]);
					$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverageGrade"] = $thisGrandMark["GrandAverageGrade"]?$thisGrandMark["GrandAverageGrade"]:"";
						
					# For Term Report Average Grade Adjustment
					if($eRCTemplateSetting['GrandAverageGradeManualAdjustment'] && $SemID == "F" && $ReportColumnID > 0) {
						$TermReportID = $lreportcard->getCorespondingTermReportID($ReportID, $ReportColumnID);
						if(!empty($TermReportID) && $TermReportID != $ReportID) {
							$TermManualAjustedMark = $lreportcard->Get_Manual_Adjustment($TermReportID, $tempArr[$j]["StudentID"], 0, 0);
							$thisTermGrandMark = $TermManualAjustedMark[$tempArr[$j]["StudentID"]][0][0];
							$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverageGrade"] = $thisTermGrandMark["GrandAverageGrade"]?$thisTermGrandMark["GrandAverageGrade"]:$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverageGrade"];
						}
					}
					
					# Round up all the marks if for SSPA
					if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M")
					{
						$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = round($thisGrandMark["GrandTotal"]?$thisGrandMark["GrandTotal"]:$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"]);
						$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = round($thisGrandMark["GrandAverage"]?$thisGrandMark["GrandAverage"]:$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"]);
						$resultAverage[$tempArr[$j]["StudentID"]]["ActualAverage"] = round($thisGrandMark["ActualAverage"]?$thisGrandMark["ActualAverage"]:$resultAverage[$tempArr[$j]["StudentID"]]["ActualAverage"]);
						$resultAverage[$tempArr[$j]["StudentID"]]["GPA"] = round($thisGrandMark["GPA"]?$thisGrandMark["GPA"]:$resultAverage[$tempArr[$j]["StudentID"]]["GPA"]);
					}
					
					// [2019-0123-1731-14066] add rounding logic
					if($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayRoundMarks'])
					{
					    if(is_numeric($resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"]) && $resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] != -1) {
					        $resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = $lreportcard->ROUND_MARK($resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"], "GrandTotal");
					    }
					    if(is_numeric($resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"]) && $resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] != -1) {
					        $resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = $lreportcard->ROUND_MARK($resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"], "GrandAverage");
					    }
					    if(is_numeric($resultAverage[$tempArr[$j]["StudentID"]]["GPA"]) && $resultAverage[$tempArr[$j]["StudentID"]]["GPA"] != -1) {
					        $resultAverage[$tempArr[$j]["StudentID"]]["GPA"] = $lreportcard->ROUND_MARK($resultAverage[$tempArr[$j]["StudentID"]]["GPA"], "GrandAverage");
					    }
					}
							
					if ($tempArr[$j]["OrderMeritClass"] > 0 ||$thisGrandMark["OrderMeritClass"]) {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritClass"] = $thisGrandMark["OrderMeritClass"]?$thisGrandMark["OrderMeritClass"]:$tempArr[$j]["OrderMeritClass"];
					}
					else {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritClass"] = "-";
					}
					if ($tempArr[$j]["OrderMeritForm"] > 0 || $thisGrandMark["OrderMeritForm"]) {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritForm"] = $thisGrandMark["OrderMeritForm"]?$thisGrandMark["OrderMeritForm"]:$tempArr[$j]["OrderMeritForm"];
					}
					else {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritForm"] = "-";
					}
				}
			}
		}
		
		# get student class and class number of last year for munsang customization
		if ($studentIDList != NULL)
		{
			$studentIDListSql = implode(",", $studentIDList);
		}
		$activeYear = $lreportcard->GET_ACTIVE_YEAR("-");
		$lastYearArr = explode("-", $activeYear);
		$lastYearArr[0] -= 1; 
		$lastYearArr[1] -= 1; 
		$lastYear = implode("-", $lastYearArr);
		$sql = "SELECT UserID, ClassName, ClassNumber FROM PROFILE_CLASS_HISTORY WHERE UserID IN ($studentIDListSql) AND AcademicYear = '$lastYear' ";
		$result = $lreportcard->returnArray($sql, 3);
		$studentClassHistoryArr = array();
		for ($i=0; $i<sizeof($result); $i++)
		{
			list($tempUserID, $tempClassName, $tempClassNumber) = $result[$i];
			$studentClassHistoryArr[$tempUserID]['ClassName'] = $tempClassName;
			$studentClassHistoryArr[$tempUserID]['ClassNumber'] = $tempClassNumber;
		}
		
		// Get Summary Info from CSV
		# build data array
		$SummaryInfoFields = $lreportcard->OtherInfoInGrandMS?$lreportcard->OtherInfoInGrandMS:array("Conduct");
		$ary = array();
		$csvType = $lreportcard->getOtherInfoType();
		if($reportTemplateInfo['Semester']=="F") {
			$InfoTermID = 0;
		}
		else {
			$InfoTermID = $reportTemplateInfo['Semester'];
		}
		if(!empty($csvType)) {
			foreach($csvType as $k=>$Type) {
				for($i=0; $i<sizeof($ClassNameArr); $i++) {
					$csvData = $lreportcard->getOtherInfoData($Type, $InfoTermID, $ClassIDArr[$i]);	
					if(!empty($csvData)) {
						foreach($csvData as $thisStudentID=>$data) {
							foreach($data as $key=>$val) {
								if (!is_numeric($key) && trim($key) !== "")
									$ary[$ClassNameArr[$i]][$thisStudentID][$key] = $val;
							}
						}
					}
				}
			}
		}
		
		if ($ReportCardCustomSchoolName == 'munsang_college')
		{
			foreach ($studentIDList as $thisStudentID)
			{
				$thisClassName = $studentIDClassMap[$thisStudentID];
				
				$eDisDataArr = $lreportcard->Get_eDiscipline_Data($InfoTermID, $thisStudentID);
				$ary[$thisClassName][$thisStudentID]['Conduct'] = $eDisDataArr['Conduct'];
			}
		}
				
		# initialize variables to calculate Average, S.D, Variance, Max, Min, Passing Rate
		// $columnSubjectIDMapArr[#column] = SubjectID;  
		$columnSubjectIDMapArr = array();	// For getting the pasing mark from the grading scheme
		// $columnScaleOutputMapArr[#column] = "M" or "G"
		$columnScaleOutputMapArr = array();
		
		$markTable = "<table class='MarkTable' width='100%' border='1' cellpadding='2' cellspacing='0'>";
		
		// Table header
		$cmpNameRow = "";
		$numOfNameRow = ($showSubjectComponent)&&empty($SubjectID)? "2" : "";
		$markTable .= "<thead>";
		$markTable .= "<tr>";
		// show last year class and class number for SSPA
		if ($ForSSPA)
		{
			// if year = 2005-2007, display "06 Class" and "05 Class" as the column titles
			$activeYearDisplay = substr($activeYear,2,2);
			$lastYearDisplay = substr($lastYear,2,2);
			$markTable .= "<th rowspan='$numOfNameRow' colspan='2'>".$activeYearDisplay." ".$eReportCard['Template']['StudentInfo']['ClassName']."</th>";
			$markTable .= "<th rowspan='$numOfNameRow' colspan='2'>".$lastYearDisplay." ".$eReportCard['Template']['StudentInfo']['ClassName']."</th>";
		}
		else
		{
			# not specific subject			
			$markTable .= "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['StudentInfo']['ClassName']."</th>";
			$markTable .= "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['StudentInfo']['ClassNo']."</th>";
		}
		
		if ($numOfNameColumn > 0)
		{
			$markTable .= "<th rowspan='$numOfNameRow' colspan='$numOfNameColumn'>".$eReportCard['Template']['StudentInfo']['Name']."</th>";
		}
		// [2015-0306-1036-36206]
		if($showUserLogin)
			$markTable .= "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['StudentInfo']['UserLogin']."</th>";
		if($showGender)
			$markTable .= "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['StudentInfo']['Gender']."</th>";
		
		if (!isset($SubjectID) || empty($SubjectID)) {
			if($showSubject){
  			# not specific subject	
  			foreach($FormSubjectArr as $tmpSubjectID => $tmpSubjectName) {
				//skip non selected subject and its component
				if (in_array($tmpSubjectID, $CmpSubjectIDList))
					$ck_subject_id = $lreportcard->GET_PARENT_SUBJECT_ID($tmpSubjectID);
				else
					$ck_subject_id = $tmpSubjectID;
				if(!in_array($ck_subject_id, $SelectedSubjectID))	continue;
  				if ($showSubjectComponent)
  				{
  					if (in_array($tmpSubjectID, $ParentSubjectIDList)) {
  						$markTable .= "<th colspan='".($ParentCmpSubjectNum[$tmpSubjectID]+1)."'>".$tmpSubjectName[0]."</th>";
  						$cmpNameRow .= "<th>".$eReportCard['Template']['Total']."</th>";
  						
//  						$columnSubjectIDMapArr[] = $tmpSubjectID;
//  						$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
//  						$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
  					}
  					else if (in_array($tmpSubjectID, $CmpSubjectIDList)) {
  						$cmpNameRow .= "<th>".$tmpSubjectName[0]."</th>";
  						
//  						$columnSubjectIDMapArr[] = $tmpSubjectID;
//  						$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
//  						$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
  					}
  					else {
  						$markTable .= "<th rowspan='2'>".$tmpSubjectName[0]."</th>";
  						
//  						$columnSubjectIDMapArr[] = $tmpSubjectID;
//  						$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
//  						$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
  					}
					$columnSubjectIDMapArr[] = $tmpSubjectID;
					$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID,0 ,0 ,$ReportID);
					$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
  				}
  				else
  				{
  					# Hide component subjects
  					if (in_array($tmpSubjectID, $ParentSubjectIDList)) {
  						$markTable .= "<th>".$tmpSubjectName[0]."</th>";
  						
  						$columnSubjectIDMapArr[] = $tmpSubjectID;
  						$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID,0 ,0 ,$ReportID);
  						$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
  					}
  					else if (!in_array($tmpSubjectID, $CmpSubjectIDList)) 
  					{
  						$markTable .= "<th rowspan='$numOfNameRow'>".$tmpSubjectName[0]."</th>";
  						
  						$columnSubjectIDMapArr[] = $tmpSubjectID;
  						$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID,0 ,0 ,$ReportID);
  						$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
  					}
  				}
  				
  			}
  		}	
		} 
		else 
		{
			# specific subject		
			if($showSubject){
  			$SubjectName = $FormSubjectArr[$SubjectID][0];
  			if (!isset($ReportColumnID) || empty($ReportColumnID)) {  			  
  				# overall terms
  				##############################
  				// Special handling of subject which having component subjects
  				if ($showSubjectComponent)
  				{
  					if (in_array($SubjectID, $ParentSubjectIDList)) 
  					{
  						$countCmpSubject = 1;
  						foreach($CmpSubjectIDNameArr as $tmpSubjectID => $tmpSubjectName) {
  							$markTable .= "<th>[".$countCmpSubject."]<br />".$tmpSubjectName."</th>";
  							$countCmpSubject++;
  							
  							$columnSubjectIDMapArr[] = $tmpSubjectID;
  							$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID,0,0,$ReportID);
  							$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
  						}
  						#$markTable .= "<th>".$eReportCard['Template']['Total']."</th>";
  					}
  				}
  								
				##############################
  				$markTable .= "<th>".$eReportCard['Total']."</th>";
  				$columnSubjectIDMapArr[] = $SubjectID;
				$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0,0,$ReportID);
				$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
  			} 
  			else 
  			{
  				# specific terms
  				##############################
  				// Special handling of subject which having component subjects
  				if ($showSubjectComponent && in_array($SubjectID, $ParentSubjectIDList)) {
  					$countCmpSubject = 1;
  					$ParentMarkFormula = array();
  					foreach($CmpSubjectIDNameArr as $tmpSubjectID => $tmpSubjectName) {
  						$OtherCondition = "SubjectID = ".$tmpSubjectID." AND ReportColumnID = $ReportColumnID";
  						$CmpSubjectWeight = $lreportcard->returnReportTemplateSubjectWeightData($ReportID, $OtherCondition); 
  						$CmpSubjectWeight = ($CmpSubjectWeight[0]['Weight']*100)."%";
  						$ParentMarkFormula[] = "[".$countCmpSubject."](".$CmpSubjectWeight.")";
  						$markTable .= "<th>[".$countCmpSubject."]<br />".$tmpSubjectName."</th>";
  						$countCmpSubject++;
  						
  						$columnSubjectIDMapArr[] = $tmpSubjectID;
  						$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID,0,0,$ReportID);
  						$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
  					}
  					$ParentMarkFormula = implode(" + ", $ParentMarkFormula);
  					$markTable .= "<th>".$eReportCard['Total']."<br />".$ParentMarkFormula."</th>";
  					
  					$columnSubjectIDMapArr[] = $SubjectID;
  					$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0,0,$ReportID);
  					$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
  					
  					#$markTable .= "<th>".$eReportCard['Template']['Total']."</th>";
  				}
  				else {
  					$markTable .= "<th>".$eReportCard['Total']."</th>";
  					
  					$columnSubjectIDMapArr[] = $tmpSubjectID;
  					$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID,0,0,$ReportID);
  					$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
  				}
  			}
  		}
		}
		
		# Summary Info
		if ($showSummaryInfo)
		{
			for($i=0; $i<sizeof($SummaryInfoFields); $i++) {
				$markTable .= "<th rowspan='$numOfNameRow'>".$eReportCard['Template'][$SummaryInfoFields[$i]]."</th>";
				
				# Do not show statistics for summary record
				$columnScaleOutputMapArr[] = "G";
			}
		}
			
//		$markTable .= ($showGrandTotal)? "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['GrandTotal']."</th>" : "";
//		$markTable .= ($showGrandAverage)? "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['GrandAverage']."</th>" : "";
//		$markTable .= ($showGPA)? "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['GPA']."</th>" : "";		
//		$markTable .= ($showClassRanking)? "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['ClassPosition']."</th>" : "";
//		$markTable .= ($showOverallRanking)? "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['FormPosition']."</th>" : "";
		$markTable .= ($showGrandTotal)? "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['GrandTotal']."</th>" : "";
		$markTable .= ($showGrandTotalGrade)? "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['GrandTotal']." ".$eReportCard['Grade']."</th>" : "";
		$markTable .= ($showGrandAverage)? "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['GrandAverage']."</th>" : "";
		$markTable .= ($showGrandAverageGrade)? "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['GrandAverage']." ".$eReportCard['Grade']."</th>" : "";		
		$markTable .= ($showActualAverage)? "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['ActualAverage']."</th>" : "";
		$markTable .= ($showActualAverageGrade)? "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['ActualAverage']." ".$eReportCard['Grade']."</th>" : "";
		$markTable .= ($showGPA)? "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['GPA']."</th>" : "";		
		$markTable .= ($showGPAGrade)? "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['GPA']." ".$eReportCard['Grade']."</th>" : "";		
		if ($ReportCardCustomSchoolName == "carmel_alison") {
		    $markTable .= "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['WeightedStandardScore']."</th>";
		}
	    // [2019-0123-1731-14066]
		if($eRCTemplateSetting['Report']['GrandMarksheet']['ShowTotalUnitFailed']) {
	        $markTable .= ($showTotalUnitFailed)? "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['TotalUnitFailed']."</th>" : "";
	    }
		$markTable .= ($showClassRanking)? "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['ClassPosition']."</th>" : "";
		$markTable .= ($showOverallRanking)? "<th rowspan='$numOfNameRow'>".$eReportCard['Template']['FormPosition']."</th>" : "";
				
//		if ($showGrandTotal)
//		{
//			$columnScaleOutputMapArr[] = "M";
//		}
//		if ($showGrandAverage)
//		{
//			$columnScaleOutputMapArr[] = "M";
//		}
//		if ($showGPA)
//		{
//			$columnScaleOutputMapArr[] = "M";
//		}
		if ($showGrandTotal)
		{
			$columnScaleOutputMapArr[] = "M";
		}
		if ($showGrandTotalGrade)
		{
			$columnScaleOutputMapArr[] = "G";
		}
		if ($showGrandAverage)
		{
			$columnScaleOutputMapArr[] = "M";
		}
		if ($showGrandAverageGrade)
		{
			$columnScaleOutputMapArr[] = "G";
		}
		if ($showActualAverage)
		{
			$columnScaleOutputMapArr[] = "M";
		}
		if ($showActualAverageGrade)
		{
			$columnScaleOutputMapArr[] = "G";
		}
		if ($showGPA)
		{
			$columnScaleOutputMapArr[] = "M";
		}
		if ($showGPAGrade)
		{
			$columnScaleOutputMapArr[] = "G";
		}
		# for display stat of SD Score
		if ($ReportCardCustomSchoolName == "carmel_alison")
		{
			$columnScaleOutputMapArr[] = "M";
		}	
		$markTable .= "</tr>";
		
		if ($cmpNameRow != "") {
			$markTable .= "<tr>$cmpNameRow</tr>";
		}
		$markTable .= "</thead>";
				
		# initialize variables to calculate Average, S.D, Variance, Max, Min, Passing Rate
		// $markDisplayedArr[#column][#row] = Mark;
		$markDisplayedArr = array();
		// $markNatureArr[#column][#row] = "Distinction" / "Pass" / "Fail";
		$markNatureArr = array();
		$ColumnCounter = 0;
		
		// Mark
		$markTable .= "<tbody>";
		foreach($studentIDListSorted as $classNumber => $studentID) {
			$ColumnCounter = 0;
			
			$markTable .= "<tr>";
			$markTable .= "<td align='center'>".$studentIDClassMap[$studentID]."</td>";
			$markTable .= "<td align='center'>".$studentIDClassNumMap[$studentID]."</td>";
			
			# show last year class and class number if for SSPA
			if ($ForSSPA)
			{
				$markTable .= "<td align='center'>{$studentClassHistoryArr[$studentID]['ClassName']}</td>";
				$markTable .= "<td align='center'>{$studentClassHistoryArr[$studentID]['ClassNumber']}</td>";
			}
			
			$studentNameEn = trim($studentIDNameMap[$studentID]['En']);
			$studentNameCh = trim($studentIDNameMap[$studentID]['Ch']);
						
			$markTable .= ($showEnglishName)? "<td>".$studentNameEn."</td>" : "";
			$markTable .= ($showChineseName)? "<td>".$studentNameCh."</td>" : "";
			
			# User Login & Gender
			//$StudentInfo[$studentID] = $lreportcard-> Get_Student_Info_By_ClassName_ClassNumber($studentIDClassMap[$studentID],$studentIDClassNumMap[$studentID]);
			$StudentInfo[$studentID] = $lreportcard->Get_Student_Class_ClassLevel_Info($studentID);
			// [2015-0306-1036-36206]
			$markTable .= ($showUserLogin)? "<td align='center'>".$StudentInfo[$studentID][0]["UserLogin"]."</td>" : "";
			$markTable .= ($showGender)? "<td align='center'>".$StudentInfo[$studentID][0]["Gender"]."</td>" : "";
			
			if (!isset($SubjectID) || empty($SubjectID)) {
				# overall subjects
  			if($showSubject){
        		foreach($FormSubjectArr as $tmpSubjectID => $tmpSubjectName)
        		{
  					//skip non selected subject and its component
  					if (in_array($tmpSubjectID, $CmpSubjectIDList))
  						$ck_subject_id = $lreportcard->GET_PARENT_SUBJECT_ID($tmpSubjectID);
  					else
  						$ck_subject_id = $tmpSubjectID;
  					if(!in_array($ck_subject_id, $SelectedSubjectID))	continue;

  					if ($showSubjectComponent)
  					{
  						$thisMarks = $resultSubject[$tmpSubjectID][$studentID];
  						$thisGrade = $resultSubjectGrade[$tmpSubjectID][$studentID];
  						
  						// [2019-0123-1731-14066] add rounding logic
  						if($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayRoundMarks'] && is_numeric($thisMarks)) {
  						    $thisMarks = $lreportcard->ROUND_MARK($thisMarks, "SubjectTotal");
  						}
	  					
  						$thisDisplayMarks = $lreportcard->Get_Score_Display_HTML($thisMarks, $ReportID, $ClassLevelID, $tmpSubjectID);
  						$markTable .= "<td align='center'>".$thisDisplayMarks."</td>";
  						$markDisplayedArr[$ColumnCounter][] = (is_numeric($thisMarks))? $thisMarks : '';
  						
  						# Record Pass Status for Passing Rate
						$thisNature = '';
  						//if (!$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1_STRING($thisMarks) && !$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2_STRING($thisMarks) )
  						if (!in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1()) && !in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2()))
  						{
  							$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID, 1, 0, $ReportID);
							$SchemeID = $SubjectFormGradingSettings['SchemeID'];
							$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
  							$SchemeInfoArr = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
							$TopPercentage = $SchemeInfoArr['TopPercentage'];
							
  							if ($ScaleDisplay == "M" && $TopPercentage > 0)
  							{
  								$thisStyleDetermineMarks = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMarks, $ReportID, $studentID, $tmpSubjectID, $ClassLevelID, $ReportColumnID);
  								$ConvertBy = "G";
  							}
  							else
  							{
  								$thisStyleDetermineMarks = $thisMarks;
  								$ConvertBy = "";
  							}
  							
  							$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $tmpSubjectID, $thisStyleDetermineMarks,'',$ConvertBy, $ReportID);
  							$markNatureArr[$ColumnCounter][] = $thisNature;
  							if(in_array($thisNature,array("Pass","Distinction")) && !in_array($tmpSubjectID, $CmpSubjectIDList))	$NumberOfPassSubject++;
  						}
						else
						{
							$markNatureArr[$ColumnCounter][] = "N.A.";
						}
  						$ColumnCounter++;
  					}
  					else
  					{
  						# show parent subject only
  						if (!in_array($tmpSubjectID, $CmpSubjectIDList)) {
  							$thisMarks = $resultSubject[$tmpSubjectID][$studentID];
  							$thisGrade = $resultSubjectGrade[$tmpSubjectID][$studentID];
  							
  							// [2019-0123-1731-14066] add rounding logic
  							if($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayRoundMarks'] && is_numeric($thisMarks)) {
  							    $thisMarks = $lreportcard->ROUND_MARK($thisMarks, "SubjectTotal");
  							}
	  						
	  						$thisDisplayMarks = $lreportcard->Get_Score_Display_HTML($thisMarks, $ReportID, $ClassLevelID, $tmpSubjectID);
  							$markTable .= "<td align='center'>".$thisDisplayMarks."</td>";
  							$markDisplayedArr[$ColumnCounter][] = (is_numeric($thisMarks))? $thisMarks : '';
  							
  							# Record Pass Status for Passing Rate
  							$thisNature = '';
	  						//if (!$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1_STRING($thisMarks) && !$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2_STRING($thisMarks))
	  						if (!in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1()) && !in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2()))
	  						{
	  							$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID, 1, 0, $ReportID);
								$SchemeID = $SubjectFormGradingSettings['SchemeID'];
								$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
	  							$SchemeInfoArr = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
								$TopPercentage = $SchemeInfoArr['TopPercentage'];
								
	  							if ($ScaleDisplay == "M" && $TopPercentage > 0)
	  							{
	  								$thisStyleDetermineMarks = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMarks, $ReportID, $studentID, $tmpSubjectID, $ClassLevelID, $ReportColumnID);
	  								$ConvertBy = "G";
	  							}
	  							else
	  							{
	  								$thisStyleDetermineMarks = $thisMarks;
	  								$ConvertBy = "";
	  							}
	  							
	  							$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $tmpSubjectID, $thisStyleDetermineMarks,'',$ConvertBy, $ReportID);
	  							
	  							$markNatureArr[$ColumnCounter][] = $thisNature;
	  							if(in_array($thisNature,array("Pass","Distinction")) && !in_array($tmpSubjectID, $CmpSubjectIDList))	$NumberOfPassSubject++;
	  						}
  							else
  							{
  								$markNatureArr[$ColumnCounter][] = "N.A.";
  							}
  							$ColumnCounter++;
  						}
  					}
  				}
				}
				
				# Summary Info
				if ($showSummaryInfo)
				{
					for($i=0; $i<sizeof($SummaryInfoFields); $i++) {
//						$thisRegNo = $studentIDRegNoMap[$studentID];
						$thisClass = $studentIDClassMap[$studentID];
						$tmpInfo = isset($ary[$thisClass][$studentID][$SummaryInfoFields[$i]]) ? $ary[$thisClass][$studentID][$SummaryInfoFields[$i]]: "-";
						// [2016-0128-1008-07066] display multiple Other Info data
						$tmpInfo = is_array($tmpInfo) ? implode("<br/>", $tmpInfo) : $tmpInfo;
							
						$markTable .= "<td align='center'>".$tmpInfo."</td>";
						
						$markDisplayedArr[$ColumnCounter][] = (is_numeric($tmpInfo))? $tmpInfo : '';
						$ColumnCounter++;
					}
				}
				$thisDisplayAverage = $lreportcard->Get_Score_Display_HTML($resultAverage[$studentID]["GrandAverage"], $ReportID, $ClassLevelID, -2,$resultAverage[$studentID]["GrandAverage"],'GrandAverage');
				$thisDisplayActualAverage = $lreportcard->Get_Score_Display_HTML($resultAverage[$studentID]["ActualAverage"], $ReportID, $ClassLevelID, -2,$resultAverage[$studentID]["ActualAverage"],'GrandAverage');
				$thisDisplayTotal= $lreportcard->Get_Score_Display_HTML($resultAverage[$studentID]["GrandTotal"], $ReportID, $ClassLevelID, -1,$resultAverage[$studentID]["GrandTotal"],'GrandTotal');
				$thisDisplayGPA= $lreportcard->Get_Score_Display_HTML($resultAverage[$studentID]["GPA"], $ReportID, $ClassLevelID, -1,$resultAverage[$studentID]["GPA"],'GPA');

				$GrandMarkScheme = $lreportcard->Get_Grand_Mark_Grading_Scheme($ClassLevelID, $ReportID);
				
				$AverageGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandMarkScheme["-1"]['SchemeID'], $resultAverage[$studentID]["GrandAverage"], $ReportID, $studentID, '-1', $ClassLevelID, $ReportColumnID);
				$thisDisplayAverageGrade = $lreportcard->Get_Score_Display_HTML($AverageGrade, $ReportID, $ClassLevelID, -1, $resultAverage[$studentID]["GrandAverage"], 'GrandAverage');
				
				// Handle Adjusted Average Grade
				$adjustedAvgGrade = $resultAverage[$studentID]["GrandAverageGrade"];
				if($eRCTemplateSetting['GrandAverageGradeManualAdjustment'] && !empty($adjustedAvgGrade)) {
					$AverageGrade = $adjustedAvgGrade;
					$thisDisplayAverageGrade = $lreportcard->Get_Score_Display_HTML($AverageGrade, $ReportID, $ClassLevelID, -1, $AverageGrade, 'GrandAverage');
				}
				
				$ActualAverageGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandMarkScheme["-1"]['SchemeID'], $resultAverage[$studentID]["ActualAverage"],$ReportID,$studentID,'-1',$ClassLevelID,$ReportColumnID);
				$thisDisplayActualAverageGrade = $lreportcard->Get_Score_Display_HTML($ActualAverageGrade, $ReportID, $ClassLevelID, -1,$resultAverage[$studentID]["ActualAverage"],'GrandAverage');
				$TotalGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandMarkScheme["-2"]['SchemeID'], $resultAverage[$studentID]["GrandTotal"],$ReportID,$studentID,'-2',$ClassLevelID,$ReportColumnID);
				$thisDisplayTotalGrade= $lreportcard->Get_Score_Display_HTML($TotalGrade, $ReportID, $ClassLevelID, -2,$resultAverage[$studentID]["GrandTotal"],'GrandTotal');
				$GPAGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandMarkScheme["-3"]['SchemeID'], $resultAverage[$studentID]["GPA"],$ReportID,$studentID,'-3',$ClassLevelID,$ReportColumnID);
				$thisDisplayGPAGrade= $lreportcard->Get_Score_Display_HTML($GPAGrade, $ReportID, $ClassLevelID, -3,$resultAverage[$studentID]["GPA"],'GPA');
								
				$markTable .= ($showGrandTotal)? "<td align='center'>".$thisDisplayTotal."</td>" : "";
				$markTable .= ($showGrandTotalGrade)? "<td align='center'>".$thisDisplayTotalGrade."</td>" : "";
				$markTable .= ($showGrandAverage)? "<td align='center'>".$thisDisplayAverage."</td>" : "";
				$markTable .= ($showGrandAverageGrade)? "<td align='center'>".$thisDisplayAverageGrade."</td>" : "";				
				$markTable .= ($showActualAverage)? "<td align='center'>".$thisDisplayActualAverage."</td>" : "";
				$markTable .= ($showActualAverageGrade)? "<td align='center'>".$thisDisplayActualAverageGrade."</td>" : "";
				$markTable .= ($showGPA)? "<td align='center'>".$thisDisplayGPA."</td>" : "";
				$markTable .= ($showGPAGrade)? "<td align='center'>".$thisDisplayGPAGrade."</td>" : "";
				if ($ReportCardCustomSchoolName == "carmel_alison")
				{
					$GrandMarkArr = $lreportcard->getReportResultScore($ReportID, $reportColumnID, $studentID);
					$thisGrandSDScore = $GrandMarkArr["GrandSDScore"];
					$markTable .= "<td align='center'>".$thisGrandSDScore."</td>";
				}
				// [2019-0123-1731-14066]
				if($eRCTemplateSetting['Report']['GrandMarksheet']['ShowTotalUnitFailed']) {
				    $markTable .= $showTotalUnitFailed? "<td align='center'>".(isset($studentFailUnitArr[$studentID])? $studentFailUnitArr[$studentID] : $lreportcard->EmptySymbol)."</td>":"";
				}
				$markTable .= ($showClassRanking)? "<td align='center'>".$resultAverage[$studentID]["OrderMeritClass"]."</td>" : "";
				$markTable .= ($showOverallRanking)? "<td align='center'>".$resultAverage[$studentID]["OrderMeritForm"]."</td>" : "";
				
				if ($showGrandTotal)
//				{
//					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandTotal"];
//					$ColumnCounter++;
//				}
//				if ($showGrandAverage)
//				{
//					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandAverage"];
//					$ColumnCounter++;
//				}
//				if ($showGPA)
//				{
//					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GPA"];
//					$ColumnCounter++;
//				}
				if ($showGrandTotal)
				{
					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandTotal"];
					$ColumnCounter++;
				}
				if ($showGrandTotalGrade)
				{
					$markDisplayedArr[$ColumnCounter][] = $TotalGrade;
					$ColumnCounter++;
				}
				if ($showGrandAverage)
				{
					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandAverage"];
					$ColumnCounter++;
				}
				if ($showGrandAverageGrade)
				{
					$markDisplayedArr[$ColumnCounter][] = $AverageGrade;
					$ColumnCounter++;
				}
				if ($showActualAverage)
				{
					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["ActualAverage"];
					$ColumnCounter++;
				}
				if ($showActualAverageGrade)
				{
					$markDisplayedArr[$ColumnCounter][] = $ActualAverageGrade;
					$ColumnCounter++;
				}
				if ($showGPA)
				{
					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GPA"];
					$ColumnCounter++;
				}
				if ($showGPAGrade)
				{
					$markDisplayedArr[$ColumnCounter][] = $GPAGrade;
					$ColumnCounter++;
				}
				
				if ($ReportCardCustomSchoolName == "carmel_alison")
				{
					$markDisplayedArr[$ColumnCounter][] = $thisGrandSDScore;
					$ColumnCounter++;
				}
				
				// [2019-0123-1731-14066]
				if($eRCTemplateSetting['Report']['GrandMarksheet']['ShowTotalUnitFailed'] && $showTotalUnitFailed)
				{
				    $markDisplayedArr[$ColumnCounter][] = isset($studentFailUnitArr[$studentID])? $studentFailUnitArr[$studentID] : $lreportcard->EmptySymbol;
				    $ColumnCounter++;
				}
			}
			else {
				# specific subject
				if($showSubject){
  				if (!isset($ReportColumnID) || empty($ReportColumnID)) {
  					# overall terms
  					##############################
  					// Special handling of subject which having component subjects
  					if ($showSubjectComponent && in_array($SubjectID, $ParentSubjectIDList)) {
  						foreach($CmpSubjectIDNameArr as $tmpSubjectID => $tmpSubjectName)
  						{
  							$thisMarks = $resultDetailSubject[$tmpSubjectID][$studentID];
  							$thisGrade = $resultDetailSubjectGrade[$tmpSubjectID][$studentID];
  							
  							// [2019-0123-1731-14066] add rounding logic
  							if($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayRoundMarks'] && is_numeric($thisMarks)) {
  							    $thisMarks = $lreportcard->ROUND_MARK($thisMarks, "SubjectTotal");
  							}
  							
	  						$thisDisplayMarks = $lreportcard->Get_Score_Display_HTML($thisMarks, $ReportID, $ClassLevelID, $tmpSubjectID);
  							$markTable .= "<td align='center'>".$thisDisplayMarks."</td>";
  							$markDisplayedArr[$ColumnCounter][] = (is_numeric($thisMarks))? $thisMarks : '';
  							
  							# Record Pass Status for Passing Rate
  							$thisNature = '';
	  						//if (!$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1_STRING($thisMarks) && !$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2_STRING($thisMarks) )
	  						if (!in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1()) && !in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2()))
	  						{
	  							$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID, 1, 0, $ReportID);
								$SchemeID = $SubjectFormGradingSettings['SchemeID'];
								$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
	  							$SchemeInfoArr = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
								$TopPercentage = $SchemeInfoArr['TopPercentage'];
								
	  							if ($ScaleDisplay == "M" && $TopPercentage > 0)
	  							{
	  								$thisStyleDetermineMarks = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMarks, $ReportID, $studentID, $tmpSubjectID, $ClassLevelID, $ReportColumnID);
	  								$ConvertBy = "G";
	  							}
	  							else
	  							{
	  								$thisStyleDetermineMarks = $thisMarks;
	  								$ConvertBy = "";
	  							}
	  							
	  							$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $tmpSubjectID, $thisStyleDetermineMarks,'',$ConvertBy, $ReportID);
	  							$markNatureArr[$ColumnCounter][] = $thisNature;
	  							if(in_array($thisNature,array("Pass","Distinction"))&& !in_array($tmpSubjectID, $CmpSubjectIDList))	$NumberOfPassSubject++;
	  						}
  							else
  							{
  								$markNatureArr[$ColumnCounter][] = "N.A.";
  							}
  							$ColumnCounter++;
  						}
  						#$markTable .= "<td align='center'>".$resultDetailSubject[$SubjectID][$studentID]."</td>";
  					}
  					##############################
  					
  					$thisMarks = $resultSubject[$studentID];
  					$thisGrade = $resultSubjectGrade[$studentID];
  					
  					// [2019-0123-1731-14066] add rounding logic
  					if($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayRoundMarks'] && is_numeric($thisMarks)) {
  					    $thisMarks = $lreportcard->ROUND_MARK($thisMarks, "SubjectTotal");
  					}
  					
  					$thisDisplayMarks = $lreportcard->Get_Score_Display_HTML($thisMarks, $ReportID, $ClassLevelID, $SubjectID);
  					$markTable .= "<td align='center'>".$thisDisplayMarks."</td>";
  					$markDisplayedArr[$ColumnCounter][] = (is_numeric($thisMarks))? $thisMarks : '';
  					
  					# Record Pass Status for Passing Rate
					$thisNature = '';
					//if (!$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1_STRING($thisMarks) && !$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2_STRING($thisMarks))
					if (!in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1()) && !in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2()))
					{
						$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID, 1, 0, $ReportID);
						$SchemeID = $SubjectFormGradingSettings['SchemeID'];
						$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
						$SchemeInfoArr = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
						$TopPercentage = $SchemeInfoArr['TopPercentage'];
						
						if ($ScaleDisplay == "M" && $TopPercentage > 0)
						{
							$thisStyleDetermineMarks = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMarks, $ReportID, $studentID, $tmpSubjectID, $ClassLevelID, $ReportColumnID);
							$ConvertBy = "G";
						}
						else
						{
							$thisStyleDetermineMarks = $thisMarks;
							$ConvertBy = "";
						}
						
						$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $SubjectID, $thisStyleDetermineMarks,'',$ConvertBy, $ReportID);
						$markNatureArr[$ColumnCounter][] = $thisNature;
						if(in_array($thisNature,array("Pass","Distinction")) && !in_array($tmpSubjectID, $CmpSubjectIDList))	$NumberOfPassSubject++;
					}
					else
					{
						$markNatureArr[$ColumnCounter][] = "N.A.";
					}
  					$ColumnCounter++;
  							
  					//if ($subjectGradingScheme["scaleDisplay"] == "M") {
  					//	$markTable .= "<td align='center'>".$OrderFormSubject[$studentID]."</td>";
  					//}
  				}
  				else {
  					# specific term
  					##############################
  					// Special handling of subject which having component subjects
  					if ($showSubjectComponent && in_array($SubjectID, $ParentSubjectIDList)) {
  						foreach($CmpSubjectIDNameArr as $tmpSubjectID => $tmpSubjectName)
  						{
  							$thisMarks = $resultDetailSubject[$tmpSubjectID][$studentID];
  							$thisGrade = $resultDetailSubjectGrade[$tmpSubjectID][$studentID];
  							
  							// [2019-0123-1731-14066] add rounding logic
  							if($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayRoundMarks'] && is_numeric($thisMarks)) {
  							    $thisMarks = $lreportcard->ROUND_MARK($thisMarks, "SubjectTotal");
  							}
	  						
	  						$thisDisplayMarks = $lreportcard->Get_Score_Display_HTML($thisMarks, $ReportID, $ClassLevelID, $tmpSubjectID);
  							$markTable .= "<td align='center'>".$thisDisplayMarks."</td>";
  							$markDisplayedArr[$ColumnCounter][] = (is_numeric($thisMarks))? $thisMarks : '';
  							
  							# Record Pass Status for Passing Rate
  							$thisNature = '';
	  						//if (!$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1_STRING($thisMarks) && !$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2_STRING($thisMarks) && !in_array($tmpSubjectID, $CmpSubjectIDList))
	  						if (!in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1()) && !in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2()) && !in_array($tmpSubjectID, $CmpSubjectIDList))
	  						{
	  							$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID, 1, 0, $ReportID);
								$SchemeID = $SubjectFormGradingSettings['SchemeID'];
								$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
	  							$SchemeInfoArr = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
								$TopPercentage = $SchemeInfoArr['TopPercentage'];
								
	  							if ($ScaleDisplay == "M" && $TopPercentage > 0)
	  							{
	  								$thisStyleDetermineMarks = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMarks, $ReportID, $studentID, $tmpSubjectID, $ClassLevelID, $ReportColumnID);
	  								$ConvertBy = "G";
	  							}
	  							else
	  							{
	  								$thisStyleDetermineMarks = $thisMarks;
	  								$ConvertBy = "";
	  							}
	  							
	  							$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $tmpSubjectID, $thisStyleDetermineMarks,'',$ConvertBy, $ReportID);
	  							$markNatureArr[$ColumnCounter][] = $thisNature;
	  							if(in_array($thisNature,array("Pass","Distinction")) && !in_array($tmpSubjectID, $CmpSubjectIDList))	$NumberOfPassSubject++;
	  						}
  							else
  							{
  								$markNatureArr[$ColumnCounter][] = "N.A.";
  							}
  							$ColumnCounter++;
  						}
  						#$markTable .= "<td align='center'>".$resultDetailSubject[$SubjectID][$studentID]."</td>";
  					}
  					##############################
  					
  					$thisMarks = $resultSubject[$studentID];
  					$thisGrade = $resultSubjectGrade[$studentID];
  					
  					// [2019-0123-1731-14066] add rounding logic
  					if($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayRoundMarks'] && is_numeric($thisMarks)) {
  					    $thisMarks = $lreportcard->ROUND_MARK($thisMarks, "SubjectTotal");
  					}
	  				
					$thisDisplayMarks = $lreportcard->Get_Score_Display_HTML($thisMarks, $ReportID, $ClassLevelID, $tmpSubjectID);
  					$markTable .= "<td align='center'>".$thisDisplayMarks."</td>";
  					$markDisplayedArr[$ColumnCounter][] = (is_numeric($thisMarks))? $thisMarks : '';
  					
  					# Record Pass Status for Passing Rate
					$thisNature = '';
					//if (!$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1_STRING($thisMarks) && !$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2_STRING($thisMarks) )
					if (!in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1()) && !in_array($thisGrade, (array)$lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2()))
					{
						$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID, 1, 0, $ReportID);
						$SchemeID = $SubjectFormGradingSettings['SchemeID'];
						$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
						$SchemeInfoArr = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
						$TopPercentage = $SchemeInfoArr['TopPercentage'];
						
						if ($ScaleDisplay == "M" && $TopPercentage > 0)
						{
							$thisStyleDetermineMarks = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMarks, $ReportID, $studentID, $tmpSubjectID, $ClassLevelID, $ReportColumnID);
							$ConvertBy = "G";
						}
						else
						{
							$thisStyleDetermineMarks = $thisMarks;
							$ConvertBy = "";
						}
						
						$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $tmpSubjectID, $thisStyleDetermineMarks,'',$ConvertBy, $ReportID);
						$markNatureArr[$ColumnCounter][] = $thisNature;
						if(in_array($thisNature,array("Pass","Distinction"))&& !in_array($tmpSubjectID, $CmpSubjectIDList))	$NumberOfPassSubject++;
					}
					else
					{
						$markNatureArr[$ColumnCounter][] = "N.A.";
					}
  					$ColumnCounter++;
  					//$markTable .= "<td align='center'>".$OrderFormSubject[$studentID]."</td>";
  				}
				}
				
				# Summary Info
				if ($showSummaryInfo)
				{
					for($i=0; $i<sizeof($SummaryInfoFields); $i++) {
//						$thisRegNo = $studentIDRegNoMap[$studentID];
						$thisClass = $studentIDClassMap[$studentID];
						$tmpInfo = isset($ary[$thisClass][$studentID][$SummaryInfoFields[$i]]) ? $ary[$thisClass][$studentID][$SummaryInfoFields[$i]]: "-";
						// [2016-0128-1008-07066] display multiple Other Info data
						$tmpInfo = is_array($tmpInfo) ? implode("<br/>", $tmpInfo) : $tmpInfo;
							
						$markTable .= "<td align='center'>".$tmpInfo."</td>";
						
						$markDisplayedArr[$ColumnCounter][] = (is_numeric($tmpInfo))? $tmpInfo : '';
						$ColumnCounter++;
					}
				}

				$thisDisplayAverage = $lreportcard->Get_Score_Display_HTML($resultAverage[$studentID]["GrandAverage"], $ReportID, $ClassLevelID, -2,$resultAverage[$studentID]["GrandAverage"],'GrandAverage');
				$thisDisplayActualAverage = $lreportcard->Get_Score_Display_HTML($resultAverage[$studentID]["ActualAverage"], $ReportID, $ClassLevelID, -2,$resultAverage[$studentID]["ActualAverage"],'GrandAverage');
				$thisDisplayTotal= $lreportcard->Get_Score_Display_HTML($resultAverage[$studentID]["GrandTotal"], $ReportID, $ClassLevelID, -1,$resultAverage[$studentID]["GrandTotal"],'GrandTotal');
				$thisDisplayGPA= $lreportcard->Get_Score_Display_HTML($resultAverage[$studentID]["GPA"], $ReportID, $ClassLevelID, -1,$resultAverage[$studentID]["GPA"],'GPA');
			
				$GrandMarkScheme = $lreportcard->Get_Grand_Mark_Grading_Scheme($ClassLevelID, $ReportID);
				
				$AverageGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandMarkScheme["-1"]['SchemeID'], $resultAverage[$studentID]["GrandAverage"], $ReportID, $studentID, '-1', $ClassLevelID, $ReportColumnID);
				$thisDisplayAverageGrade = $lreportcard->Get_Score_Display_HTML($AverageGrade, $ReportID, $ClassLevelID, -1, $resultAverage[$studentID]["GrandAverage"], 'GrandAverage');
				
				// Handle Adjusted Average Grade
				$adjustedAvgGrade = $resultAverage[$studentID]["GrandAverageGrade"];
				if($eRCTemplateSetting['GrandAverageGradeManualAdjustment'] && !empty($adjustedAvgGrade)) {
					$AverageGrade = $adjustedAvgGrade;
					$thisDisplayAverageGrade = $lreportcard->Get_Score_Display_HTML($AverageGrade, $ReportID, $ClassLevelID, -1, $AverageGrade, 'GrandAverage');
				}
				
				$ActualAverageGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandMarkScheme["-1"]['SchemeID'], $resultAverage[$studentID]["ActualAverage"],$ReportID,$studentID,'-1',$ClassLevelID,$ReportColumnID);
				$thisDisplayActualAverageGrade = $lreportcard->Get_Score_Display_HTML($ActualAverageGrade, $ReportID, $ClassLevelID, -1,$resultAverage[$studentID]["ActualAverage"],'GrandAverage');
				$TotalGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandMarkScheme["-2"]['SchemeID'], $resultAverage[$studentID]["GrandTotal"],$ReportID,$studentID,'-2',$ClassLevelID,$ReportColumnID);
				$thisDisplayTotalGrade= $lreportcard->Get_Score_Display_HTML($TotalGrade, $ReportID, $ClassLevelID, -2,$resultAverage[$studentID]["GrandTotal"],'GrandTotal');
				$GPAGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandMarkScheme["-3"]['SchemeID'], $resultAverage[$studentID]["GPA"],$ReportID,$studentID,'-3',$ClassLevelID,$ReportColumnID);
				$thisDisplayGPAGrade= $lreportcard->Get_Score_Display_HTML($GPAGrade, $ReportID, $ClassLevelID, -3,$resultAverage[$studentID]["GPA"],'GPA');
								
				$markTable .= ($showGrandTotal)? "<td align='center'>".$thisDisplayTotal."</td>" : "";
				$markTable .= ($showGrandTotalGrade)? "<td align='center'>".$thisDisplayTotalGrade."</td>" : "";
				$markTable .= ($showGrandAverage)? "<td align='center'>".$thisDisplayAverage."</td>" : "";
				$markTable .= ($showGrandAverageGrade)? "<td align='center'>".$thisDisplayAverageGrade."</td>" : "";				
				$markTable .= ($showActualAverage)? "<td align='center'>".$thisDisplayActualAverage."</td>" : "";
				$markTable .= ($showActualAverageGrade)? "<td align='center'>".$thisDisplayActualAverageGrade."</td>" : "";
				$markTable .= ($showGPA)? "<td align='center'>".$thisDisplayGPA."</td>" : "";
				$markTable .= ($showGPAGrade)? "<td align='center'>".$thisDisplayGPAGrade."</td>" : "";
				if ($ReportCardCustomSchoolName == "carmel_alison")
				{
					$GrandMarkArr = $lreportcard->getReportResultScore($ReportID, $reportColumnID, $studentID);
					$thisGrandSDScore = $GrandMarkArr["GrandSDScore"];
					$markTable .= "<td align='center'>".$thisGrandSDScore."</td>";
				}
				// [2019-0123-1731-14066]
				if($eRCTemplateSetting['Report']['GrandMarksheet']['ShowTotalUnitFailed']) {
				    $markTable .= ($showTotalUnitFailed)? "<td align='center'>".(isset($studentFailUnitArr[$studentID])? $studentFailUnitArr[$studentID] : $lreportcard->EmptySymbol)."</td>":"";
				}
				$markTable .= ($showClassRanking)? "<td align='center'>".$OrderClassSubject[$studentID]."</td>" : "";
				$markTable .= ($showOverallRanking)? "<td align='center'>".$OrderFormSubject[$studentID]."</td>" : "";
				

				if ($showGrandTotal)
				{
					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandTotal"];
					$ColumnCounter++;
				}
				if ($showGrandTotalGrade)
				{
					$markDisplayedArr[$ColumnCounter][] = $TotalGrade;
					$ColumnCounter++;
				}
				if ($showGrandAverage)
				{
					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandAverage"];
					$ColumnCounter++;
				}
				if ($showGrandAverageGrade)
				{
					$markDisplayedArr[$ColumnCounter][] = $AverageGrade;
					$ColumnCounter++;
				}
				if ($showActualAverage)
				{
					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["ActualAverage"];
					$ColumnCounter++;
				}
				if ($showActualAverageGrade)
				{
					$markDisplayedArr[$ColumnCounter][] = $ActualAverageGrade;
					$ColumnCounter++;
				}
				if ($showGPA)
				{
					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GPA"];
					$ColumnCounter++;
				}
				if ($showGPAGrade)
				{
					$markDisplayedArr[$ColumnCounter][] = $GPAGrade;
					$ColumnCounter++;
				}
				
				if ($ReportCardCustomSchoolName == "carmel_alison")
				{
					$markDisplayedArr[$ColumnCounter][] = $thisGrandSDScore;
					$ColumnCounter++;
				}
				
				// [2019-0123-1731-14066]
				if($eRCTemplateSetting['Report']['GrandMarksheet']['ShowTotalUnitFailed'] && $showTotalUnitFailed) {
				    $markDisplayedArr[$ColumnCounter][] = isset($studentFailUnitArr[$studentID])? $studentFailUnitArr[$studentID] : $lreportcard->EmptySymbol;
				    $ColumnCounter++;
				}
				
//				$markTable .= ($showGrandTotal)? "<td align='center'>".$thisDisplayTotal."</td>" : "";
//				$markTable .= ($showGrandAverage)? "<td align='center'>".$thisDisplayAverage."</td>" : "";
//				$markTable .= ($showGPA)? "<td align='center'>".$thisDisplayGPA."</td>" : "";
//				$markTable .= ($showClassRanking)? "<td align='center'>".$OrderClassSubject[$studentID]."</td>" : "";
//				$markTable .= ($showOverallRanking)? "<td align='center'>".$OrderFormSubject[$studentID]."</td>" : "";
				
//				if ($showGrandTotal)
//				{
//					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandTotal"];
//					$ColumnCounter++;
//				}
//				if ($showGrandAverage)
//				{
//					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandAverage"];
//					$ColumnCounter++;
//				}
//				if ($showGPA)
//				{
//					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GPA"];
//					$ColumnCounter++;
//				}
			}
			$markTable .= "</tr>";
		}
		
		if ($showStatistics)
		{
			# Statistics Rows
			$TitleColSpanNum = 2;
			$decimalPlacesShown = 2;
			if ($ForSSPA)
				$TitleColSpanNum += 2;
			if ($showEnglishName)
				$TitleColSpanNum++;
			if ($showChineseName)
				$TitleColSpanNum++;
			// [2015-0306-1036-36206]
			if ($showUserLogin)
				$TitleColSpanNum++;
			if ($showGender)
				$TitleColSpanNum++;
			$TitleColSpan = "colspan='".$TitleColSpanNum."'";
			
			# Average
			$markTable .= "<tr>";
				$markTable .= "<td class='tabletext stat_start_border_top' align='center' $TitleColSpan><b>".$eReportCard['Template']['Average']."</b></td>";
				for ($i=0; $i<count($markDisplayedArr); $i++)
				{
					$thisScaleDisplay = $columnScaleOutputMapArr[$i];
					
					if ($thisScaleDisplay != "M")
					{
						$markTable .= "<td class='tabletext stat_start_border_top' align='center'><b>".$lreportcard->EmptySymbol."</b></td>";
					}
					else
					{
						$thisColumnMarksArr = array_values(array_remove_empty($markDisplayedArr[$i]));
						$thisDisplay = $lreportcard->getAverage($thisColumnMarksArr, $decimalPlacesShown);
						$markTable .= "<td class='tabletext stat_start_border_top' align='center'><b>".$thisDisplay."</b></td>";
					}
				}
				$markTable .= ($showClassRanking)? "<td align='center' class='stat_start_border_top'>".$lreportcard->EmptySymbol."</td>" : "";
				$markTable .= ($showOverallRanking)? "<td align='center' class='stat_start_border_top'>".$lreportcard->EmptySymbol."</td>" : "";
			$markTable .= "<tr>";
			
			# SD
			$markTable .= "<tr>";
				$markTable .= "<td class='tabletext ' align='center' $TitleColSpan><b>".$eReportCard['Template']['SD']."</b></td>";
				for ($i=0; $i<count($markDisplayedArr); $i++)
				{
					$thisScaleDisplay = $columnScaleOutputMapArr[$i];
					
					if ($thisScaleDisplay != "M")
					{
						$markTable .= "<td class='tabletext ' align='center'><b>".$lreportcard->EmptySymbol."</b></td>";
					}
					else
					{
						$thisColumnMarksArr = array_values(array_remove_empty($markDisplayedArr[$i]));
						$thisDisplay = $lreportcard->getSD($thisColumnMarksArr, $decimalPlacesShown);
						$markTable .= "<td class='tabletext ' align='center'><b>".$thisDisplay."</b></td>";
					}
				}
				$markTable .= ($showClassRanking)? "<td align='center' >".$lreportcard->EmptySymbol."</td>" : "";
				$markTable .= ($showOverallRanking)? "<td align='center' >".$lreportcard->EmptySymbol."</td>" : "";
			$markTable .= "<tr>"; 
			
			# Highest Mark
			$markTable .= "<tr>";
				$markTable .= "<td class='tabletext ' align='center' $TitleColSpan><b>".$eReportCard['Template']['HighestMark']."</b></td>";
				for ($i=0; $i<count($markDisplayedArr); $i++)
				{
					$thisScaleDisplay = $columnScaleOutputMapArr[$i];
					
					if ($thisScaleDisplay != "M")
					{
						$thisDisplay = $lreportcard->EmptySymbol;
					}
					else
					{
						$thisColumnMarksArr = array_values(array_remove_empty($markDisplayedArr[$i]));
						if(count($thisColumnMarksArr)>0)
							$thisDisplay = max($thisColumnMarksArr);
						$thisDisplay = ($thisDisplay == "")? $lreportcard->EmptySymbol : $thisDisplay;
					}
					$markTable .= "<td class='tabletext ' align='center'><b>".$thisDisplay."</b></td>";
				}
				$markTable .= ($showClassRanking)? "<td align='center' >".$lreportcard->EmptySymbol."</td>" : "";
				$markTable .= ($showOverallRanking)? "<td align='center' >".$lreportcard->EmptySymbol."</td>" : "";
			$markTable .= "<tr>";
			
			# Lowest Mark
			$markTable .= "<tr>";
				$markTable .= "<td class='tabletext ' align='center' $TitleColSpan><b>".$eReportCard['Template']['LowestMark']."</b></td>";
				for ($i=0; $i<count($markDisplayedArr); $i++)
				{
					$thisScaleDisplay = $columnScaleOutputMapArr[$i];
					
					if ($thisScaleDisplay != "M")
					{
						$thisDisplay = $lreportcard->EmptySymbol;
					}
					else
					{
						$thisColumnMarksArr = array_values(array_remove_empty($markDisplayedArr[$i]));
						
						// [2017-0706-1012-27236] remove -1 in Column Marks
						$thisColumnMarksArr = array_diff((array)$thisColumnMarksArr, array(-1));
						if(count($thisColumnMarksArr) > 0)
							$thisDisplay = min($thisColumnMarksArr);
						$thisDisplay = ($thisDisplay == "")? $lreportcard->EmptySymbol : $thisDisplay;
					}
					$markTable .= "<td class='tabletext ' align='center'><b>".$thisDisplay."</b></td>";
				}
				$markTable .= ($showClassRanking)? "<td align='center' >".$lreportcard->EmptySymbol."</td>" : "";
				$markTable .= ($showOverallRanking)? "<td align='center' >".$lreportcard->EmptySymbol."</td>" : "";
			$markTable .= "<tr>";
			
			# use the following ary to count how many empty symbol should be printed
			$NumOfOtherInfo = $showSummaryInfo?sizeof($SummaryInfoFields):0;
			$DisplayedAry = array($NumOfOtherInfo,$showGrandTotal,$showGrandTotalGrade,$showGrandAverage,$showGrandAverageGrade,$showActualAverage,$showActualAverageGrade,$showGPA,$showGPAGrade,$showClassRanking,$showOverallRanking,$NumberOfPassSubject=0);
			// [2019-0123-1731-14066]
			if($eRCTemplateSetting['Report']['GrandMarksheet']['ShowTotalUnitFailed'] && $showTotalUnitFailed) {
			    $DisplayedAry[] = $showTotalUnitFailed;
			}
			$NumOfEmptySymbol = array_sum($DisplayedAry);
			$EmptySymbolRow = $lreportcard->GrandMarkSheetGenEmptySymbol("HTML",$NumOfEmptySymbol);
			
			# Passing Rate
			$markTable .= "<tr>";
				$markTable .= "<td class='tabletext ' align='center' $TitleColSpan><b>".$eReportCard['Template']['PassingRate']."</b></td>";
				for ($i=0; $i<count($markNatureArr); $i++)
				{
					$thisColumnMarksArr = $markNatureArr[$i];
					$thisDisplay = $lreportcard->getPassingRate($thisColumnMarksArr, 2);
					$thisDisplay = ($thisDisplay == "")? $lreportcard->EmptySymbol : $thisDisplay;
					$markTable .= "<td class='tabletext ' align='center'><b>".$thisDisplay."</b></td>";
				}
				$markTable .= $EmptySymbolRow;
			$markTable .= "<tr>";
		}
		
		$markTable .= "</tbody>";
		$markTable .= "</table>";
		
		// Title of the grandmarksheet
		//$schoolData = split("\n",get_file_content($PATH_WRT_ROOT."file/school_data.txt"));
		$schoolData = explode("\n", get_file_content($PATH_WRT_ROOT."file/school_data.txt"));
		$schoolName = $schoolData[0];
		$academicYear = $lreportcard->GET_ACTIVE_YEAR("-");
		
		if ($ForSSPA)
		{
			// SSPA report contains last year information, so the acadermic year of the title is across two acadermic years
			$academicYearArr = explode("-", $academicYear);
			$academicYearArr[0] -= 1;
			$academicYear = implode("-", $academicYearArr);
			
			$GMSTitle = "$academicYear $ClassLevelName $ColumnTitle $SubjectName ".$eReportCard['Template']['GrandMarksheet'];
			$GMSTitle .= " (SSPA)";
			
			$markTable .= "<table width='100%' border='0' cellpadding='2' cellspacing='0'>";
			$markTable .= "<tr><td>&nbsp;</td></tr>";
			$markTable .= "<tr><td class='footer'>";
			$markTable .= $eReportCard['Template']['SSPA_Footer'];
			$markTable .= "</td></tr>";
			$markTable .= "<tr><td class='signature' align='right'>";
			$markTable .= $eReportCard['Template']['SSPA_Signature'];
			$markTable .= "</td></tr>";
			$markTable .= "</table>";
		}
		else
		{
			$GMSTitle = "$academicYear $ClassLevelName $ColumnTitle $SubjectName ".$eReportCard['Template']['GrandMarksheet'];	
		}
		
		# Pass number of form, each class and gender
		if(empty($SubjectID))
		{
			foreach ((array)$resultAverage as $StudentID => $GrandMarksArr)
			{
				$ClassName = $studentIDClassMap[$StudentID];
				$Gender = $StudentInfo[$StudentID][0]["Gender"];
				$GrandAvgNature = $GrandAvgNatureArr[$StudentID];
		
				$ClassStudentPassArr[$ClassName]["NumOfStudent"][$Gender]++;
				if($GrandAvgNature!='Fail')
					$ClassStudentPassArr[$ClassName]["Pass"][$Gender]++;
			}
			
			$GenderAry = array("M","F");
			foreach((array)$ClassStudentPassArr as $ClassName => $ClassInfo)
			{
				#Display Class Pass
				$ClassNumOfStudent = @array_sum($ClassInfo["NumOfStudent"]);
				$ClassNumOfPass = @array_sum($ClassInfo["Pass"]);
				
				$ClassPassTable.="<tr>";
					$ClassPassTable.="<td align='left'>$ClassName</td>";
					$ClassPassTable.="<td align='center'>".($ClassNumOfStudent>0?$ClassNumOfStudent:0)."</td>";
					$ClassPassTable.="<td align='center'>".($ClassNumOfPass>0?$ClassNumOfPass:0)."</td>";
				$ClassPassTable.="</tr>";
				
				# Display Gender Pass 
				foreach($GenderAry as $Gender)
				{
					$GenderNumOfStudent = $ClassInfo["NumOfStudent"][$Gender];
					$GenderNumOfPass = $ClassInfo["Pass"][$Gender];
					
					$ClassPassTable.="<tr>";
						$ClassPassTable.="<td align='right'>".$eReportCard['GrandMarkSheet']['Gender'][$Gender]."</td>";
						$ClassPassTable.="<td align='center'>".($GenderNumOfStudent>0?$GenderNumOfStudent:0)."</td>";
						$ClassPassTable.="<td align='center'>".($GenderNumOfPass>0?$GenderNumOfPass:0)."</td>";
					$ClassPassTable.="</tr>";		
					
					$FormGenderNumOfStudent[$Gender] += $GenderNumOfStudent;
					$FormGenderNumOfPass[$Gender] += $GenderNumOfPass;
				}
				
				$FormNumOfStudent += $ClassNumOfStudent;
				$FormNumOfPass += $ClassNumOfPass;
			}
	
			# Calculate and Display Form Pass  
			# whole form stat
			$FormPassTable.="<tr>";
				$FormPassTable.="<td align='left'>".$eReportCard["WholeForm"]."</td>";
				$FormPassTable.="<td align='center'>".($FormNumOfStudent>0?$FormNumOfStudent:0)."</td>";
				$FormPassTable.="<td align='center'>".($FormNumOfPass>0?$FormNumOfPass:0)."</td>";
			$FormPassTable.="</tr>";
			
			# form gender stat
			foreach($GenderAry as $Gender)
			{
				$GenderNumOfStudent = $FormGenderNumOfStudent[$Gender];
				$GenderNumOfPass = $FormGenderNumOfPass[$Gender];
				
				$FormPassTable.="<tr>";
					$FormPassTable.="<td align='right'>".$eReportCard['GrandMarkSheet']['Gender'][$Gender]."</td>";
					$FormPassTable.="<td align='center'>".($GenderNumOfStudent>0?$GenderNumOfStudent:0)."</td>";
					$FormPassTable.="<td align='center'>".($GenderNumOfPass>0?$GenderNumOfPass:0)."</td>";
				$FormPassTable.="</tr>";		
			}
			
			$PassTable ="<table class='MarkTable' width='40%' border='1' cellpadding='2' cellspacing='0'>";
				$PassTable.="<thead>";	
					$PassTable.="<tr>";
						$PassTable.="<th>&nbsp;</th>";
						$PassTable.="<th align='center'>".$eReportCard["TotalNum"]."</th>";
						$PassTable.="<th align='center'>".$eReportCard["Template"]["GrandAvgPassNumber"]."</th>";
					$PassTable.="</tr>";
				$PassTable.="</thead>";	
				$PassTable.= $FormPassTable;
				$PassTable.=$ClassPassTable;						
			$PassTable.="</table>";
		}		
		############## Interface Start ##############
		
?>
<html>
	<head>
		<meta http-equiv='pragma' content='no-cache' />
		<meta http-equiv='content-type' content='text/html; charset=utf-8' />
		<title><?=$GMSTitle?></title>
	</head>

<style type="text/css">
<!--
h1 {
	font-size: 22px;
	font-family: arial, "lucida console", sans-serif;
	margin: 0px;
	padding-top: 3px;
	padding-bottom: 10px;
}

.MarkTable {
	border-collapse: collapse;
	border-color: #000000;
}

.MarkTable thead {
	font-size: 12px;
	font-family: arial, "lucida console", sans-serif;
}

.MarkTable tbody {
	font-size: 12px;
	font-family: arial, "lucida console", sans-serif;
}

.footer {
	font-family: "Arial", "Lucida Console";	
	font-size: 12px;
}
.signature {
	font-family: "Arial", "Lucida Console";	
	font-size: 14px;
}
.stat_start_border_top {
	border-top-width: 2px;
	border-top-style: solid;
	border-top-color: #000000;
}

-->
</style>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center"><h1><?=$schoolName?><br /><?= $GMSTitle ?></h1></td>
	</tr>
	<tr>
		<td>
			<?= $markTable ?>
		</td>
	</tr>
	<tr>
		<td>
			<span class="tabletextrequire">*</span><?=$eReportCard['StatNotShown']?>
		</td>
	</tr>
	<tr>
		<td><br><?= $PassTable ?></td>
	</tr>

    <?php if($eRCTemplateSetting['Report']['GrandMarksheet']['DisplayPrintReportDateTime']) { ?>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="right">
                <?php echo $eReportCard['MasterReport']['PrintDate'] . ":&nbsp;" . date('j/n/Y H:i'); ?>
            </td>
        </tr>
    <?php } ?>
</table>

<br />
</html>
<?
	}
	else
    {
?>
You have no priviledge to access this page.
<?
	}
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>