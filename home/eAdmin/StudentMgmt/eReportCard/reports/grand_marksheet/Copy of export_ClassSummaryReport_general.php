<?php
// Root path
$PATH_WRT_ROOT = "../../../../../../";

// Page access right
$PageRight = "TEACHER";

// Include general libraries
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

// Authorize intranet user & connect Database
intranet_auth();
intranet_opendb();

// Check if ReportCard module is enable
if ($plugin['ReportCard2008']) {
	// Include ReportCard libraries
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
	
	// Create objects
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard2008w();
	}
	$lclass = new libclass();
	
	$exportColumn = array();
	$ExportArr = array();
	$lexport = new libexporttext();
	
	// Check module access right
	if ($lreportcard->hasAccessRight()) {
		// Check required fields submitted from index.php
		// optional: $SubjectID, $ReportColumnID
		if (!isset($ClassLevelID, $ClassID, $ReportID) || empty($ClassLevelID) || empty($ReportID) || empty($ClassID)) {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		# get display options
		// 1. student name display (EnglishName, ChineseName)
		$showEnglishName = false;
		$showChineseName = false;
		if ($StudentNameDisplay == NULL)
		{
			$numOfNameColumn = 0;
		}
		else
		{
			$StudentNameDisplay = explode(",", $StudentNameDisplay);
			$numOfNameColumn = sizeof($StudentNameDisplay);
			foreach ($StudentNameDisplay as $key => $value)
			{
				if ($value == "EnglishName") $showEnglishName = true;
				if ($value == "ChineseName") $showChineseName = true;
			}
		}		
		// 2. ranking display (ClassRanking, OverallRanking)
		$showClassRanking = false;
		$showOverallRanking = false;
		$RankingDisplay = explode(",", $RankingDisplay);
		foreach ($RankingDisplay as $key => $value)
		{
			if ($value == "ClassRanking") $showClassRanking = true;
			if ($value == "OverallRanking") $showOverallRanking = true;
		}		
		// 3. show subject component (0,1)
		$showSubjectComponent = $ShowSubjectComponent;
		// 4. show Summary Info (0,1)
		$showSummaryInfo = $ShowSummaryInfo;		
		// 5. show Grand Total (0,1)
		$showGrandTotal = $ShowGrandTotal;		
		// 6. show Grand Average (0,1)
		$showGrandAverage = $ShowGrandAverage;
		// 7. ForSSPA => no decimal place in all marks
		$ForSSPA = $ForSSPA;
		// 8. show Statistics (0,1)
		$showStatistics = $ShowStatistics;
		
		$reportCalculationOrder = $lreportcard->returnReportTemplateCalculation($ReportID);
		$SemID = $reportTemplateInfo['Semester'];
		$SemesterTitle = $lreportcard->returnSemesters($SemID);
		
		// Get ReportCard info
		$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		$SemID = $reportTemplateInfo['Semester'];
		if ($SemID != "F")
			$SemesterTitle = $lreportcard->returnSemesters($SemID);
		else
			$SemesterTitle = $eReportCard['Template']['Annual'];
			
		// Check to make sure it is a generated report
		if ($reportTemplateInfo["LastGenerated"] == "0000-00-00 00:00:00" || $reportTemplateInfo["LastGenerated"] == "") {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		// Get subjects of the current ClassLevel
		$FormSubjectArr = $lreportcard->returnSubjectwOrder($ClassLevelID, 1, '', $intranet_session_language);
		
		$CmpSubjectIDList = array();
		$ParentSubjectIDList = array();
		$ParentCmpSubjectNum = array();
		
		// Reformat the subject array - Display component subjects also
		$FormSubjectArrFlat = array();
		if (sizeof($FormSubjectArr) > 0) {
			foreach($FormSubjectArr as $tmpSubjectID => $tmpSubjectName) {
				if (sizeof($tmpSubjectName) > 1) {
					$ParentSubjectIDList[] = $tmpSubjectID;
					$ParentCmpSubjectNum[$tmpSubjectID] = sizeof($tmpSubjectName) - 1;
					foreach ($tmpSubjectName as $tmpCmpSubjectID => $tmpCmpSubjectName) {
						if ($tmpCmpSubjectID != 0) {
							$CmpSubjectIDList[] = $tmpCmpSubjectID;
							$FormSubjectArrFlat[$tmpCmpSubjectID] = array(0 => $tmpCmpSubjectName);
						}
					}
				}
				$FormSubjectArrFlat[$tmpSubjectID] = array(0 => $tmpSubjectName[0]);
			}
		}
		$FormSubjectArr = $FormSubjectArrFlat;
		
		// Get component subjects if SubjectID is provided
		if (isset($SubjectID) && $SubjectID !== "" && in_array($SubjectID, $ParentSubjectIDList)) {
			$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($SubjectID, $ClassLevelID);
			$CmpSubjectIDNameArr = array();
			$CmpSubjectIDArr = array();
			for($i=0; $i<sizeof($CmpSubjectArr); $i++) {
				$CmpSubjectIDArr[] = $CmpSubjectArr[$i]["SubjectID"];
				$CmpSubjectIDNameArr[$CmpSubjectArr[$i]["SubjectID"]] = $CmpSubjectArr[$i]["SubjectName"];
			}
		}
		
		// Get subjects' grading scheme of the current ClassLevel
		$FormSubjectGradingSchemeArr = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID);
		
		// Get ClassLevel name
		$ClassLevelName = $lreportcard->returnClassLevel($ClassLevelID);
		
		// Get Classes
		$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		
		// Get current school year
		$SchoolYear = $lreportcard->schoolYear;
		
		// Get columns info of the current ReportCard
		$ColumnTitleAry = $lreportcard->returnReportColoumnTitle($ReportID);
		$ColumnIDList = array();
		$ColumnIDList = ($ColumnTitleAry == NULL)? NULL : array_keys($ColumnTitleAry);
		
		// Get the list of students (in a class or entire class level)
		$ClassName = "";
		if (!isset($ClassID) || empty($ClassID)) {
			intranet_closedb();
			header("Location: index.php");
			exit();
		} else {
			$studentList = $lreportcard->GET_STUDENT_BY_CLASS($ClassID, "", 1);
			// Get the Class Name
			for($i=0; $i<sizeof($ClassArr); $i++) {
				if ($ClassArr[$i]["ClassID"] == $ClassID) {
					$ClassName = $ClassArr[$i]["ClassName"];
					break;
				}
			}
		}
		
		$studentIDList = array();
		$studentIDNameMap = array();
		$studentIDRegNoMap = array();
		for($i=0; $i<sizeof($studentList); $i++) {
			$studentIDNameMap[$studentList[$i]["UserID"]] = $studentList[$i][3];
			$studentIDRegNoMap[$studentList[$i]["UserID"]] = $studentList[$i]["WebSAMSRegNo"];
			if ($studentList[$i][2] != "")
				$studentIDList[$studentList[$i][2]] = $studentList[$i]["UserID"];
			else
				$studentIDList[] = $studentList[$i]["UserID"];
		}
		
		// Students should be already sorted by Class Number in the SQL Query
		#asort($studentIDList);
		$studentIDListSql = implode(",", array_values($studentIDList));
		$cond = "";
		
		// Get overall mark or term/assessment mark
		$ColumnTitle = "";
		if (!isset($ReportColumnID) || empty($ReportColumnID)) {
			$ColumnTitle = $eReportCard['Template']['OverallCombined'];
			$cond .= "AND (ReportColumnID = '' OR ReportColumnID = '0') ";
			$reportColumnID = 0;
		} else {
			$ColumnTitle = $ColumnTitleAry[$ReportColumnID];
			$cond .= "AND ReportColumnID = '$ReportColumnID'";
			$reportColumnID = $ReportColumnID;
		}
		
		// Get individual subject mark or grand average (which table to retrieve)
		$SubjectName = "";
		if (!isset($SubjectID) || empty($SubjectID)) {
			// No SubjectID provided, get the average result
			$table = $lreportcard->DBName.".RC_REPORT_RESULT";
			$fieldToSelect = "StudentID, GrandTotal, GrandAverage, OrderMeritClass, OrderMeritForm";
			
			$sql = "SELECT $fieldToSelect FROM $table WHERE ";
			$sql .= "ReportID = '$ReportID' ";
			$sql .= "AND StudentID IN ($studentIDListSql) ";
			$sql .= $cond;
			$tempArr = $lreportcard->returnArray($sql);
			
			$resultAverage = array();
			if (sizeof($tempArr) > 0) {
				for($j=0; $j<sizeof($tempArr); $j++) {
					
					$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = $tempArr[$j]["GrandTotal"];
					$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = $tempArr[$j]["GrandAverage"];
					
					# Round up all the marks if for SSPA
					if ($ForSSPA)
					{
						$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = round($resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"]);
						$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = round($resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"]);
					}
					
					if ($tempArr[$j]["OrderMeritClass"] > 0) {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritClass"] = $tempArr[$j]["OrderMeritClass"];
					} else {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritClass"] = "-";
					}
					if ($tempArr[$j]["OrderMeritForm"] > 0) {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritForm"] = $tempArr[$j]["OrderMeritForm"];
					} else {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritForm"] = "-";
					}
				}
			}
			
			foreach($FormSubjectArr as $tmpSubjectID => $tmpSubjectName) {
				$subjectGradingScheme = $FormSubjectGradingSchemeArr[$tmpSubjectID];
				$subjectGradingSchemeInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($subjectGradingScheme["schemeID"]);
				
				$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
				$SchemeID = $SubjectFormGradingSettings['SchemeID'];
				$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
				$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
				
				if ($subjectGradingScheme["scaleDisplay"] == "M") {
					$fieldToSelect = "StudentID, Mark As MarkUsed, Grade";
				} else {
					$fieldToSelect = "StudentID, Grade As MarkUsed, Grade";
				}
				
				$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE";
				
				$sql = "SELECT $fieldToSelect FROM $table WHERE ";
				$sql .= "ReportID = '$ReportID' ";
				$sql .= "AND StudentID IN ($studentIDListSql) ";
				$sql .= "AND  SubjectID = '$tmpSubjectID' ";
				$sql .= $cond;
				$tempArr = $lreportcard->returnArray($sql);
				
				$resultSubject[$tmpSubjectID] = array();
				if (sizeof($tempArr) > 0) {
					for($j=0; $j<sizeof($tempArr); $j++) {
						$thisMark = $tempArr[$j]["MarkUsed"];
						$thisGrade = $tempArr[$j]["Grade"];
						list($thisMark, $needStyle) = $lreportcard->checkSpCase($ReportID, $tmpSubjectID, $thisMark, $thisGrade);
						
						$resultSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = $thisMark;
												
						# Round up all the marks if for SSPA
						if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M" && is_numeric($thisMark))
						{
							$resultSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = round($resultSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]]);
						}
					}
				}
			}
		} else {
			# Specific Subject
			$subjectGradingScheme = $FormSubjectGradingSchemeArr[$SubjectID];
			$subjectGradingSchemeInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($subjectGradingScheme["schemeID"]);
			
			$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID);
			$SchemeID = $SubjectFormGradingSettings['SchemeID'];
			$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
			$ScaleInput = $SubjectFormGradingSettings['ScaleInput'];
			
			if ($subjectGradingScheme["scaleDisplay"] == "M") {
				$fieldToSelect = "StudentID, OrderMeritClass, OrderMeritForm, Mark As MarkUsed, Grade";
			} else {
				$fieldToSelect = "StudentID, OrderMeritClass, OrderMeritForm, Grade As MarkUsed, Grade";
			}
			$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE";
			
			$sql = "SELECT $fieldToSelect FROM $table WHERE ";
			$sql .= "ReportID = '$ReportID' ";
			$sql .= "AND StudentID IN ($studentIDListSql) ";
			$sql .= "AND SubjectID = '$SubjectID' ";
			
			$tempArr = $lreportcard->returnArray($sql.$cond);
			
			$resultSubject = array();
			$OrderClassSubject = array();
			if (sizeof($tempArr) > 0) {
				for($j=0; $j<sizeof($tempArr); $j++) {
					$thisMark = $tempArr[$j]["MarkUsed"];
					$thisGrade = $tempArr[$j]["Grade"];
					list($thisMark, $needStyle) = $lreportcard->checkSpCase($ReportID, $SubjectID, $thisMark, $thisGrade);
					
					$resultSubject[$tempArr[$j]["StudentID"]] = $thisMark;
										
					# Round up all the marks if for SSPA
					if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M" && is_numeric($thisMark))
					{
						$resultSubject[$tempArr[$j]["StudentID"]] = round($resultSubject[$tempArr[$j]["StudentID"]]);
					}
					
					if ($tempArr[$j]["OrderMeritClass"] > 0) {
						$OrderClassSubject[$tempArr[$j]["StudentID"]] = $tempArr[$j]["OrderMeritClass"];
					} else {
						$OrderClassSubject[$tempArr[$j]["StudentID"]] = "-";
					}
					
					if ($tempArr[$j]["OrderMeritForm"] > 0) {
						$OrderFormSubject[$tempArr[$j]["StudentID"]] = $tempArr[$j]["OrderMeritForm"];
					} else {
						$OrderFormSubject[$tempArr[$j]["StudentID"]] = "-";
					}
				}
			}
			
			// If gettng subject overall mark for a single subject, get the column mark also
			if (!isset($ReportColumnID) || empty($ReportColumnID)) {
				for($i=0; $i<sizeof($ColumnIDList); $i++) {
					$tmpCond = "AND ReportColumnID = '".$ColumnIDList[$i]."'";
					$tempArr = $lreportcard->returnArray($sql.$tmpCond);
					$resultSubjectColumn[$ColumnIDList[$i]] = array();
					if (sizeof($tempArr) > 0) {
						for($j=0; $j<sizeof($tempArr); $j++) {
							$thisMark = $tempArr[$j]["MarkUsed"];
							$thisGrade = $tempArr[$j]["Grade"];
							list($thisMark, $needStyle) = $lreportcard->checkSpCase($ReportID, $SubjectID, $thisMark, $thisGrade);
							
							$resultSubjectColumn[$ColumnIDList[$i]][$tempArr[$j]["StudentID"]] = $thisMark;
							
							# Round up all the marks if for SSPA
							if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M" && is_numeric($thisMark))
							{
								$resultSubjectColumn[$ColumnIDList[$i]][$tempArr[$j]["StudentID"]] = round($resultSubjectColumn[$ColumnIDList[$i]][$tempArr[$j]["StudentID"]]);
							}
						}
					}
				}
			}
			
			#################################
			// Special handling of subject which having component subjects
			if (in_array($SubjectID, $ParentSubjectIDList)) {
				// Push the SubjectID into the same array of its component subjects
				$SubjectPlusCompSubjectIDList = $CmpSubjectIDArr;
				$SubjectPlusCompSubjectIDList[] = $SubjectID;
				
				/*
				// If no ReportColumnID is provided, beside showing the subject overall result, show all column result too
				if (!isset($ReportColumnID) || empty($ReportColumnID)) {
					for($i=0; $i<sizeof($ColumnIDList); $i++) {
					 	$tmpCond1 = "AND ReportColumnID = '".$ColumnIDList[$i]."'";
						for($j=0; $j<sizeof($SubjectPlusCompSubjectIDList); $j++) {
							$tmpSubjectID = $SubjectPlusCompSubjectIDList[$j];
							$subjectGradingScheme = $FormSubjectGradingSchemeArr[$tmpSubjectID];
							$subjectGradingSchemeInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($subjectGradingScheme["schemeID"]);
							
							if ($subjectGradingScheme["scaleDisplay"] == "M") {
								$fieldToSelect = "StudentID, OrderMeritClass, Mark As MarkUsed";
							} else {
								$fieldToSelect = "StudentID, OrderMeritClass, Grade As MarkUsed";
							}
							$sql = "SELECT $fieldToSelect FROM $table WHERE ";
							$sql .= "ReportID = '$ReportID' ";
							$sql .= "AND StudentID IN ($studentIDListSql) ";
							
							$tmpCond2 = "AND SubjectID = '$tmpSubjectID' ";
							$tempArr = $lreportcard->returnArray($sql.$tmpCond1.$tmpCond2);
							if (sizeof($tempArr) > 0) {
								for($k=0; $k<sizeof($tempArr); $k++) {
									$resultSubjectColumnDetail[$ColumnIDList[$i]][$tmpSubjectID][$tempArr[$k]["StudentID"]] = $tempArr[$k]["MarkUsed"];
								}
							}
						}
					}
				}
				*/
				
				for($i=0; $i<sizeof($SubjectPlusCompSubjectIDList); $i++) {
					$tmpSubjectID = $SubjectPlusCompSubjectIDList[$i];
					$subjectGradingScheme = $FormSubjectGradingSchemeArr[$tmpSubjectID];
					$subjectGradingSchemeInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($subjectGradingScheme["schemeID"]);
					
					if ($subjectGradingScheme["scaleDisplay"] == "M") {
						$fieldToSelect = "StudentID, Mark As MarkUsed, Grade";
					} else {
						$fieldToSelect = "StudentID, Grade As MarkUsed, Grade";
					}
					$table = $lreportcard->DBName.".RC_REPORT_RESULT_SCORE";
					
					$sql = "SELECT $fieldToSelect FROM $table WHERE ";
					$sql .= "ReportID = '$ReportID' ";
					$sql .= "AND StudentID IN ($studentIDListSql) ";
					$sql .= "AND  SubjectID = '$tmpSubjectID' ";
					$sql .= $cond;
					$tempArr = $lreportcard->returnArray($sql);
					
					$resultDetailSubject[$tmpSubjectID] = array();
					if (sizeof($tempArr) > 0) {
						for($j=0; $j<sizeof($tempArr); $j++) {
							$thisMark = $tempArr[$j]["MarkUsed"];
							$thisGrade = $tempArr[$j]["Grade"];
							list($thisMark, $needStyle) = $lreportcard->checkSpCase($ReportID, $tmpSubjectID, $thisMark, $thisGrade);
							
							$resultDetailSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = $thisMark;
							
							# Round up all the marks if for SSPA
							if ($ForSSPA && $subjectGradingScheme["scaleDisplay"] == "M" && is_numeric($thisMark))
							{
								$resultDetailSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]] = round($resultDetailSubject[$tmpSubjectID][$tempArr[$j]["StudentID"]]);
							}
						}
					}
				}
			}
			#################################
			# get Grand Total and Grand Average
			$table = $lreportcard->DBName.".RC_REPORT_RESULT";
			$fieldToSelect = "StudentID, GrandTotal, GrandAverage, OrderMeritClass, OrderMeritForm";
			
			$sql = "SELECT $fieldToSelect FROM $table WHERE ";
			$sql .= "ReportID = '$ReportID' ";
			$sql .= "AND StudentID IN ($studentIDListSql) ";
			$sql .= $cond;
			$tempArr = $lreportcard->returnArray($sql);
			
			$resultAverage = array();
			if (sizeof($tempArr) > 0) {
				for($j=0; $j<sizeof($tempArr); $j++) {
					$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = $tempArr[$j]["GrandTotal"];
					$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = sprintf("%02.2f", $tempArr[$j]["GrandAverage"]);
					
					# Round up all the marks if for SSPA
					if ($ForSSPA)
					{
						$resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"] = round($resultAverage[$tempArr[$j]["StudentID"]]["GrandTotal"]);
						$resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"] = round($resultAverage[$tempArr[$j]["StudentID"]]["GrandAverage"]);
					}					
					
					if ($tempArr[$j]["OrderMeritClass"] > 0) {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritClass"] = $tempArr[$j]["OrderMeritClass"];
					} else {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritClass"] = "-";
					}
					if ($tempArr[$j]["OrderMeritForm"] > 0) {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritForm"] = $tempArr[$j]["OrderMeritForm"];
					} else {
						$resultAverage[$tempArr[$j]["StudentID"]]["OrderMeritForm"] = "-";
					}
				}
			}
		}
				
		// Get Summary Info from CSV
		# build data array
		$SummaryInfoFields = array("Conduct");
		$ary = array();
		$csvType = $lreportcard->getOtherInfoType();

		if($reportTemplateInfo['Semester']=="F") {
			$InfoTermID = 0;
		} else {
			$InfoTermID = $reportTemplateInfo['Semester']+1;
		}
		if(!empty($csvType)) {
			foreach($csvType as $k=>$Type) {
				$csvData = $lreportcard->getOtherInfoData($Type, $InfoTermID, $ClassName);	
				if(!empty($csvData)) {
					foreach($csvData as $RegNo=>$data) {
						foreach($data as $key=>$val) {
							if (!is_numeric($key) && trim($key) !== "")
								$ary[$RegNo][$key] = $val;
						}
					}
				}
			}
		}
		
		# initialize variables to calculate Average, S.D, Variance, Max, Min, Passing Rate
		// $columnSubjectIDMapArr[#column] = SubjectID;  
		$columnSubjectIDMapArr = array();	// For getting the pasing mark from the grading scheme
		// $columnScaleOutputMapArr[#column] = "M" or "G"
		$columnScaleOutputMapArr = array();
		
		// Table header
		$exportColumn[] = "WebSAMSRegNumber";
		if (!isset($SubjectID) || empty($SubjectID)) {
			# All Subjects
			$exportColumn[] = $eReportCard['Template']['StudentInfo']['ClassNo'];
			if ($showEnglishName)
			{
				$exportColumn[] = $i_UserEnglishName;
			}
			if ($showChineseName)
			{
				$exportColumn[] = $i_UserChineseName;
			}
			
			$tmpCmpSubjectTitleArr = array();
			foreach($FormSubjectArr as $tmpSubjectID => $tmpSubjectName) {
				if ($showSubjectComponent)
				{
					if (in_array($tmpSubjectID, $ParentSubjectIDList)) 
					{
						$thisParentSubjectName = $tmpSubjectName[0];
						
						# add parent subject name to the component subjects' title
						# e.g. "Oral" -> "English Oral"
						foreach($tmpCmpSubjectTitleArr as $count => $id_title)
						{
							$thisSubjectDataArr = explode("::", $id_title);
							$thisID = $thisSubjectDataArr[0];
							$thisSubjectName = $thisSubjectDataArr[1];
							$exportColumn[] = $thisParentSubjectName." ".$thisSubjectName;
							
							$columnSubjectIDMapArr[] = $thisID;
							$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $thisID);
							$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
						}
						
						# initialize $tmpCmpSubjectTitleArr for next component subjects group
						$tmpCmpSubjectTitleArr = array();
						
						# Total Title
						$exportColumn[] = $thisParentSubjectName." ".$eReportCard['Template']['Total'];
						
						$columnSubjectIDMapArr[] = $tmpSubjectID;
						$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
						$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
					} 
					else if (in_array($tmpSubjectID, $CmpSubjectIDList)) 
					{
						$tmpCmpSubjectTitleArr[] = $tmpSubjectID."::".$tmpSubjectName[0];
					} 
					else 
					{
						$exportColumn[] = $tmpSubjectName[0];
						
						$columnSubjectIDMapArr[] = $tmpSubjectID;
						$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
						$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
					}
				}
				else
				{
					# Hide component subjects
					if (in_array($tmpSubjectID, $ParentSubjectIDList)) {
						$exportColumn[] = $tmpSubjectName[0];
						
						$columnSubjectIDMapArr[] = $tmpSubjectID;
						$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
						$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
					}
					else if (!in_array($tmpSubjectID, $CmpSubjectIDList)) 
					{
						$exportColumn[] = $tmpSubjectName[0];
						
						$columnSubjectIDMapArr[] = $tmpSubjectID;
						$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
						$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
					}
				}
			}
			
			# for filename display
			$SubjectName = $eReportCard['Template']['AverageMark'];
			
		} else {
			# Specfic Subject
			
			$exportColumn[] = $eReportCard['Template']['StudentInfo']['ClassNo'];
			if ($showEnglishName)
			{
				$exportColumn[] = $i_UserEnglishName;
			}
			if ($showChineseName)
			{
				$exportColumn[] = $i_UserChineseName;
			}
			
			$SubjectName = $FormSubjectArr[$SubjectID][0];
			if (!isset($ReportColumnID) || empty($ReportColumnID)) {
				# Specfic Subject AND Overall Term
				/*
				for($i=0; $i<sizeof($ColumnIDList); $i++) {
					$markTable `	.= "<th>".$ColumnTitleAry[$ColumnIDList[$i]]."</th>";
				}
				*/
				##############################
				// Special handling of subject which having component subjects
				if ($showSubjectComponent)
				{
					if (in_array($SubjectID, $ParentSubjectIDList)) 
					{
						$countCmpSubject = 1;
						foreach($CmpSubjectIDNameArr as $tmpSubjectID => $tmpSubjectName) {
							$exportColumn[] = "[".$countCmpSubject."]".$tmpSubjectName;
							$countCmpSubject++;
							
							$columnSubjectIDMapArr[] = $tmpSubjectID;
							$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
							$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
						}
					}
				}
				##############################
				$exportColumn[] = str_replace("<br>", " ", $eReportCard['Template']['SubjectOverall']);
				
				$columnSubjectIDMapArr[] = $SubjectID;
				$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID);
				$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
				
				
			} else {
				# Specfic Subject AND Specific Term
				##############################
				// Special handling of subject which having component subjects
				if ($showSubjectComponent && in_array($SubjectID, $ParentSubjectIDList)) {
					foreach($CmpSubjectIDNameArr as $tmpSubjectID => $tmpSubjectName) {
						$exportColumn[] = $tmpSubjectName;
						
						$columnSubjectIDMapArr[] = $tmpSubjectID;
						$thisSchemeInfo = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $tmpSubjectID);
						$columnScaleOutputMapArr[] = $thisSchemeInfo['ScaleDisplay'];
					}
					#$markTable .= "<th>".$eReportCard['Template']['Total']."</th>";
				}
				##############################
			}
		}
		
		# Summary Info
		if ($showSummaryInfo)
		{
			for($i=0; $i<sizeof($SummaryInfoFields); $i++) {
				$exportColumn[] = $eReportCard['Template'][$SummaryInfoFields[$i]];
				$columnScaleOutputMapArr[] = "G";
			}
		}
		
		if ($showGrandTotal)
		{
			$exportColumn[] = $eReportCard['Template']['GrandTotal'];
			$columnScaleOutputMapArr[] = "M";
		}
		if ($showGrandAverage)
		{
			$exportColumn[] = $eReportCard['Template']['GrandAverage'];
			$columnScaleOutputMapArr[] = "M";
		}
		if ($showClassRanking)
		{
			$exportColumn[] = $eReportCard['Template']['ClassPosition'];
		}
		if ($showOverallRanking)
		{
			$exportColumn[] = $eReportCard['Template']['FormPosition'];
		}
			
		
		
		# initialize variables to calculate Average, S.D, Variance, Max, Min, Passing Rate
		// $markDisplayedArr[#column][#row] = Mark;
		$markDisplayedArr = array();
		// $markNatureArr[#column][#row] = "Distinction" / "Pass" / "Fail";
		$markNatureArr = array();
		$ColumnCounter = 0;
		
		$iCounter = 0;
		$jCounter = 0;
		// Mark
		foreach($studentIDList as $classNumber => $studentID) {
			$jCounter = 0;
			$ColumnCounter = 0;
			
			# WebSAMSNo
			$thisWebSAMSRegNo = $studentIDRegNoMap[$studentID];
			$ExportArr[$iCounter][$jCounter] = $thisWebSAMSRegNo;
			$jCounter++;
			
			# Class number
			$ExportArr[$iCounter][$jCounter] = $classNumber;
			$jCounter++;			
			
			# Student Name
			// Separate the student name in "English (Chinese)" format			
			$studentNameBi = $studentIDNameMap[$studentID];
			$studentNamePiece = explode("(",$studentNameBi);
			$studentNameEn = trim($studentNamePiece[0]);
			$studentNameCh = trim(str_replace(")", "", $studentNamePiece[1]));			
			if ($showEnglishName)
			{
				$ExportArr[$iCounter][$jCounter] = $studentNameEn;
				$jCounter++;
			}
			if ($showChineseName)
			{
				$ExportArr[$iCounter][$jCounter] = $studentNameCh;
				$jCounter++;
			}
			
			if (!isset($SubjectID) || empty($SubjectID)) {
				# all subjects
				
				# overall term
				foreach($FormSubjectArr as $tmpSubjectID => $tmpSubjectName) {
					if ($showSubjectComponent)
					{
						$thisMarks = $resultSubject[$tmpSubjectID][$studentID];
						$ExportArr[$iCounter][$jCounter] = $thisMarks;
						$jCounter++;
						
						# Record Marks for Statistics
						$markDisplayedArr[$ColumnCounter][] = (is_numeric($thisMarks))? $thisMarks : '';
						
						# Record Pass Status for Passing Rate
						if (!$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1($thisMarks) && !$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2($thisMarks))
						{
							$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $tmpSubjectID, $thisMarks);
							$markNatureArr[$ColumnCounter][] = $thisNature;
						}
						
						$ColumnCounter++;
					}
					else
					{
						# show parent subject only
						if (!in_array($tmpSubjectID, $CmpSubjectIDList)) {
							$thisMarks = $resultSubject[$tmpSubjectID][$studentID];
							$ExportArr[$iCounter][$jCounter] = $thisMarks;
							$jCounter++;
							
							# Record Marks for Statistics
							$markDisplayedArr[$ColumnCounter][] = (is_numeric($thisMarks))? $thisMarks : '';
								
							# Record Pass Status for Passing Rate
							if (!$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1($thisMarks) && !$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2($thisMarks))
							{
								$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $tmpSubjectID, $thisMarks);
								$markNatureArr[$ColumnCounter][] = $thisNature;
							}
							
							$ColumnCounter++;
						}
					}
				}
				
				# Summary Info
				if ($showSummaryInfo)
				{
					for($i=0; $i<sizeof($SummaryInfoFields); $i++) {
						$thisWebSAMSRegNo = $studentIDRegNoMap[$studentID];
						$thisSummaryInfoField = $SummaryInfoFields[$i];
						$tmpInfo = isset($ary[$thisWebSAMSRegNo][$thisSummaryInfoField]) ? $ary[$thisWebSAMSRegNo][$thisSummaryInfoField] : "-";
						
						$ExportArr[$iCounter][$jCounter] = $tmpInfo;
						$jCounter++;
						
						# Record Marks for Statistics
						$markDisplayedArr[$ColumnCounter][] = (is_numeric($tmpInfo))? $tmpInfo : '';
						
						$ColumnCounter++;
					}
				}
				
				if ($showGrandTotal)
				{
					$ExportArr[$iCounter][$jCounter] = $resultAverage[$studentID]["GrandTotal"];
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandTotal"];
					$ColumnCounter++;
				}
				if ($showGrandAverage)
				{
					$ExportArr[$iCounter][$jCounter] = $resultAverage[$studentID]["GrandAverage"];
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandAverage"];
					$ColumnCounter++;
				}
				if ($showClassRanking)
				{
					$ExportArr[$iCounter][$jCounter] = $resultAverage[$studentID]["OrderMeritClass"];
					$jCounter++;
				}
				if ($showOverallRanking)
				{
					$ExportArr[$iCounter][$jCounter] = $resultAverage[$studentID]["OrderMeritForm"];
					$jCounter++;
				}
				
			} else {
				# specific subject
				if (!isset($ReportColumnID) || empty($ReportColumnID)) {
					# overall term
					/*
					for($i=0; $i<sizeof($ColumnIDList); $i++) {
						$markTable .= "<td align='center'>".$resultSubjectColumn[$ColumnIDList[$i]][$studentID]."</td>";
					}
					*/
					##############################
					// Special handling of subject which having component subjects
					if ($showSubjectComponent && in_array($SubjectID, $ParentSubjectIDList)) {
						foreach($CmpSubjectIDNameArr as $tmpSubjectID => $tmpSubjectName) {
							$thisMarks = $resultDetailSubject[$tmpSubjectID][$studentID];
							$ExportArr[$iCounter][$jCounter] = $thisMarks;
							$jCounter++;
							
							# Record Marks for Statistics
							$markDisplayedArr[$ColumnCounter][] = (is_numeric($thisMarks))? $thisMarks : '';
								
							# Record Pass Status for Passing Rate
							if (!$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1($thisMarks) && !$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2($thisMarks))
							{
								$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $tmpSubjectID, $thisMarks);
								$markNatureArr[$ColumnCounter][] = $thisNature;
							}
							
							$ColumnCounter++;
						}
						#$markTable .= "<td align='center'>".$resultDetailSubject[$SubjectID][$studentID]."</td>";
					}
					##############################
					
					# Subject Overall
					$thisMarks = $resultSubject[$studentID];
					$ExportArr[$iCounter][$jCounter] = $thisMarks;
					$jCounter++;
					
					# Record Marks for Statistics
					$markDisplayedArr[$ColumnCounter][] = (is_numeric($thisMarks))? $thisMarks : '';
					
					# Record Pass Status for Passing Rate
					if (!$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1($thisMarks) && !$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2($thisMarks))
					{
						$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $SubjectID, $thisMarks);
						$markNatureArr[$ColumnCounter][] = $thisNature;
					}
					
					$ColumnCounter++;
					
					if ($subjectGradingScheme["scaleDisplay"] == "M") {
						//$markTable .= "<td align='center'>".$OrderClassSubject[$studentID]."</td>";
					}
				} else {
					# specific term
					##############################
					// Special handling of subject which having component subjects
					if ($showSubjectComponent &&  in_array($SubjectID, $ParentSubjectIDList)) {
						foreach($CmpSubjectIDNameArr as $tmpSubjectID => $tmpSubjectName) {
							$thisMarks = $resultDetailSubject[$tmpSubjectID][$studentID];
							$ExportArr[$iCounter][$jCounter] = $thisMarks;
							$jCounter++;
							
							# Record Marks for Statistics
							$markDisplayedArr[$ColumnCounter][] = (is_numeric($thisMarks))? $thisMarks : '';
								
							# Record Pass Status for Passing Rate
							if (!$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1($thisMarks) && !$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2($thisMarks))
							{
								$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $tmpSubjectID, $thisMarks);
								$markNatureArr[$ColumnCounter][] = $thisNature;
							}
							
							$ColumnCounter++;
						}
						#$markTable .= "<td align='center'>".$resultDetailSubject[$SubjectID][$studentID]."</td>";
					}
					##############################
					
					//$markTable .= "<td align='center'>".$resultSubject[$studentID]."</td>";
					//$markTable .= "<td align='center'>".$OrderClassSubject[$studentID]."</td>";
				}
				
				# Summary Info
				if ($showSummaryInfo)
				{
					for($i=0; $i<sizeof($SummaryInfoFields); $i++) {
						$thisWebSAMSRegNo = $studentIDRegNoMap[$studentID];
						$thisSummaryInfoField = $SummaryInfoFields[$i];
						$tmpInfo = isset($ary[$thisWebSAMSRegNo][$thisSummaryInfoField]) ? $ary[$thisWebSAMSRegNo][$thisSummaryInfoField] : "-";
						
						$ExportArr[$iCounter][$jCounter] = $tmpInfo;
						$jCounter++;
						
						$markDisplayedArr[$ColumnCounter][] = $tmpInfo;
						$ColumnCounter++;
					}
				}
				
				if ($showGrandTotal)
				{
					$ExportArr[$iCounter][$jCounter] = $resultAverage[$studentID]["GrandTotal"];
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandTotal"];
					$ColumnCounter++;
				}
				if ($showGrandAverage)
				{
					$ExportArr[$iCounter][$jCounter] = $resultAverage[$studentID]["GrandAverage"];
					$jCounter++;
					
					$markDisplayedArr[$ColumnCounter][] = $resultAverage[$studentID]["GrandAverage"];
					$ColumnCounter++;
				}
				if ($showClassRanking)
				{
					$ExportArr[$iCounter][$jCounter] = $OrderClassSubject[$studentID];
					$jCounter++;
				}
				if ($showOverallRanking)
				{
					$ExportArr[$iCounter][$jCounter] = $OrderFormSubject[$studentID];
					$jCounter++;
				}
			}
			$iCounter++;
		}
		
		
		if ($showStatistics)
		{
			# Statistics Rows
			$decimalPlacesShown = 2;
			$numEmptyColumn = 1;
			if ($showEnglishName)
				$numEmptyColumn++;
			if ($showChineseName)
				$numEmptyColumn++;
			
			# Average
			$jCounter = 0;
			$ExportArr[$iCounter][$jCounter] = $eReportCard['Template']['Average'];
			$jCounter++;
			for ($i=0; $i<$numEmptyColumn; $i++)
			{
				$ExportArr[$iCounter][$jCounter] = "";
				$jCounter++;
			}
			for ($i=0; $i<count($markDisplayedArr); $i++)
			{
				$thisScaleDisplay = $columnScaleOutputMapArr[$i];
				
				if ($thisScaleDisplay != "M")
				{
					$thisDisplay = $lreportcard->EmptySymbol;
				}
				else
				{
					$thisColumnMarksArr = $markDisplayedArr[$i];
					$thisDisplay = $lreportcard->getAverage($thisColumnMarksArr, $decimalPlacesShown);
				}
				$ExportArr[$iCounter][$jCounter] = $thisDisplay;
				$jCounter++;
			}
			$iCounter++;
			
			# SD
			$jCounter = 0;
			$ExportArr[$iCounter][$jCounter] = $eReportCard['Template']['SD'];
			$jCounter++;
			for ($i=0; $i<$numEmptyColumn; $i++)
			{
				$ExportArr[$iCounter][$jCounter] = "";
				$jCounter++;
			}
			for ($i=0; $i<count($markDisplayedArr); $i++)
			{
				$thisScaleDisplay = $columnScaleOutputMapArr[$i];
				
				if ($thisScaleDisplay != "M")
				{
					$thisDisplay = $lreportcard->EmptySymbol;
				}
				else
				{
					$thisColumnMarksArr = $markDisplayedArr[$i];
					$thisDisplay = $lreportcard->getSD($thisColumnMarksArr, $decimalPlacesShown);
				}
				$ExportArr[$iCounter][$jCounter] = $thisDisplay;
				$jCounter++;
			}
			$iCounter++;
			
			# Highest Mark
			$jCounter = 0;
			$ExportArr[$iCounter][$jCounter] = $eReportCard['Template']['HighestMark'];
			$jCounter++;
			for ($i=0; $i<$numEmptyColumn; $i++)
			{
				$ExportArr[$iCounter][$jCounter] = "";
				$jCounter++;
			}
			for ($i=0; $i<count($markDisplayedArr); $i++)
			{
				$thisScaleDisplay = $columnScaleOutputMapArr[$i];
				
				if ($thisScaleDisplay != "M")
				{
					$thisDisplay = $lreportcard->EmptySymbol;
				}
				else
				{
					$thisColumnMarksArr = $markDisplayedArr[$i];
					$thisDisplay = $lreportcard->getMaximum($thisColumnMarksArr);
				}
				$ExportArr[$iCounter][$jCounter] = $thisDisplay;
				$jCounter++;
			}
			$iCounter++;
			
			# Lowest Mark
			$jCounter = 0;
			$ExportArr[$iCounter][$jCounter] = $eReportCard['Template']['LowestMark'];
			$jCounter++;
			for ($i=0; $i<$numEmptyColumn; $i++)
			{
				$ExportArr[$iCounter][$jCounter] = "";
				$jCounter++;
			}
			for ($i=0; $i<count($markDisplayedArr); $i++)
			{
				$thisScaleDisplay = $columnScaleOutputMapArr[$i];
				
				if ($thisScaleDisplay != "M")
				{
					$thisDisplay = $lreportcard->EmptySymbol;
				}
				else
				{
					$thisColumnMarksArr = $markDisplayedArr[$i];
					$thisDisplay = $lreportcard->getMinimum($thisColumnMarksArr);
				}
				$ExportArr[$iCounter][$jCounter] = $thisDisplay;
				$jCounter++;
			}
			$iCounter++;
			
			# Passing Rate
			$jCounter = 0;
			$ExportArr[$iCounter][$jCounter] = $eReportCard['Template']['PassingRate'];
			$jCounter++;
			for ($i=0; $i<$numEmptyColumn; $i++)
			{
				$ExportArr[$iCounter][$jCounter] = "";
				$jCounter++;
			}
			for ($i=0; $i<count($markNatureArr); $i++)
			{
				$thisScaleDisplay = $columnScaleOutputMapArr[$i];
				
				$thisColumnMarksArr = $markNatureArr[$i];
				$thisDisplay = $lreportcard->getPassingRate($thisColumnMarksArr, 2);
				$thisDisplay = ($thisDisplay == "")? $lreportcard->EmptySymbol : $thisDisplay;
					
				$ExportArr[$iCounter][$jCounter] = $thisDisplay;
				$jCounter++;
			}
			if ($showSummaryInfo)
			{
				$ExportArr[$iCounter][$jCounter] = $lreportcard->EmptySymbol;
				$jCounter++;
			}
			if ($showGrandTotal)
			{
				$ExportArr[$iCounter][$jCounter] = $lreportcard->EmptySymbol;
				$jCounter++;
			}
			if ($showGrandAverage)
			{
				$ExportArr[$iCounter][$jCounter] = $lreportcard->EmptySymbol;
				$jCounter++;
			}
			if ($showClassRanking)
			{
				$ExportArr[$iCounter][$jCounter] = $lreportcard->EmptySymbol;
				$jCounter++;
			}
			if ($showOverallRanking)
			{
				$ExportArr[$iCounter][$jCounter] = $lreportcard->EmptySymbol;
				$jCounter++;
			}
		}
		
		// Title of the grandmarksheet
		$schoolData = split("\n",get_file_content($PATH_WRT_ROOT."file/school_data.txt"));
		$schoolName = $schoolData[0];
		$academicYear = $lreportcard->GET_ACTIVE_YEAR("-");
		$GMSTitle = $academicYear."_".$SemesterTitle."_".$ClassLevelName."_".$ClassName."_".$ColumnTitle."_".$SubjectName."_".$eReportCard['Reports_GrandMarksheet']."_".$eReportCard['GrandMarksheetTypeOption'][0];
		if ($ForSSPA)
		{
			$GMSTitle .= "(SSPA)";
		}
		$filename = intranet_undo_htmlspecialchars($GMSTitle.".csv");
		
		$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "", "\r\n", "", 0, "11");
		intranet_closedb();
		
		// Output the file to user browser
		$lexport->EXPORT_FILE($filename, $export_content);
		
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>
