<?
#  Editing by 

/***********************************************
 * modification log
 *  20190604 Bill:  [2019-0123-1731-14066]
 *      - par $ShowTotalUnitFailed  ($eRCTemplateSetting['Report']['GrandMarksheet']['ShowTotalUnitFailed'])
 *  20180628 Bill:  [2017-1204-1601-38164]
 *      - par $targetActiveYear  ($eRCTemplateSetting['Report']['SupportAllActiveYear'])
 * 	20150402 Bill:
 * 		- par $ShowUserLogin [2015-0306-1036-36206]
 *  20100625 Marcus:
 * 		- modify ReportColumnID, if == '', change to 0 
 *  20100414 Marcus:
 * 		- modify variable $ClassID into array 
 * ***********************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
#include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
#include_once($PATH_WRT_ROOT."includes/libreportcard2008_sis.php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcard();

$ClassLevelID				= $_POST['ClassLevelID'];
$ClassID					= $_POST['ClassID'];
$ReportID					= $_POST['ReportID'];
$SubjectID					= $_POST['SubjectID'];
$ReportColumnID				= $_POST['ReportColumnID']?$_POST['ReportColumnID']:0;
$GrandMarksheetType			= $_POST['GrandMarksheetType'];
$StudentNameDisplay 		= $_POST['StudentNameDisplay'];
$RankingDisplay 			= $_POST['RankingDisplay'];
$ShowSubject 	     		= $_POST['ShowSubject']; # added on 20091203
$ShowSubjectComponent 		= $_POST['ShowSubjectComponent'];
$ShowSummaryInfo 			= $_POST['ShowSummaryInfo'];
$ShowGrandTotal 			= $_POST['ShowGrandTotal'];
$ShowGrandTotalGrade 		= $_POST['ShowGrandTotalGrade'];
$ShowGrandAverage 			= $_POST['ShowGrandAverage'];
$ShowGrandAverageGrade		= $_POST['ShowGrandAverageGrade'];
$ShowActualAverage 			= $_POST['ShowActualAverage'];
$ShowActualdAverageGrade	= $_POST['ShowActualAverageGrade'];
$ShowGPA 					= $_POST['ShowGPA'];
$ShowGPAGrade 				= $_POST['ShowGPAGrade'];
$ForSSPA					= $_POST['ForSSPA'];
$ShowStatistics				= $_POST['ShowStatistics'];
$targetActiveYear		    = $_POST['targetActiveYear'];

if (is_array($StudentNameDisplay))
{
	$StudentNameDisplay = implode(",", $StudentNameDisplay);
}
if (is_array($RankingDisplay))
{
	$RankingDisplay = implode(",", $RankingDisplay);
}

$parms = "ReportID=".$ReportID."&ClassLevelID=".$ClassLevelID."&ReportColumnID=".$ReportColumnID;
$parms .= "&StudentNameDisplay=".$StudentNameDisplay;
$parms .= "&RankingDisplay=".$RankingDisplay;
$parms .= "&SubjDisplay=".$SubjDisplay;
// [2015-0306-1036-36206]
$parms .= "&ShowUserLogin=".$ShowUserLogin;
$parms .= "&ShowGender=".$ShowGender;
$parms .= "&ShowSubject=".$ShowSubject;
$parms .= "&ShowSubjectComponent=".$ShowSubjectComponent;
$parms .= "&ShowSummaryInfo=".$ShowSummaryInfo;
$parms .= "&ShowGrandTotal=".$ShowGrandTotal;
$parms .= "&ShowGrandTotalGrade=".$ShowGrandTotalGrade;
$parms .= "&ShowGrandAverage=".$ShowGrandAverage;
$parms .= "&ShowGrandAverageGrade=".$ShowGrandAverageGrade;
if ($eRCTemplateSetting['Calculation']['ActualAverage']) {
	$parms .= "&ShowActualAverage=".$ShowActualAverage;
	$parms .= "&ShowActualAverageGrade=".$ShowActualAverageGrade;
}
$parms .= "&ShowGPA=".$ShowGPA;
$parms .= "&ShowGPAGrade=".$ShowGPAGrade;
$parms .= "&ForSSPA=".$ForSSPA;
$parms .= "&ShowStatistics=".$ShowStatistics;
for($i=0; $i<sizeof($SubjectID); $i++)
{
	$parms .= "&SubjectID[]=".$SubjectID[$i];	
}
for($i=0; $i<sizeof($ClassID); $i++)
{
	$parms .= "&ClassID[]=".$ClassID[$i];	
}
if($eRCTemplateSetting['Report']['SupportAllActiveYear']){
    $parms .= "&targetActiveYear=".$targetActiveYear;
}
if($eRCTemplateSetting['Report']['GrandMarksheet']['ShowTotalUnitFailed']){
    $parms .= "&ShowTotalUnitFailed=".$ShowTotalUnitFailed;
}

switch($GrandMarksheetType)
{
	case 0:			# Class Summary Report
		if ($ViewFormat == "csv")
		{
//			$url = "export_ClassSummaryReport_".$ReportCardCustomSchoolName.".php";
			if (!file_exists($url)) $url = "export_ClassSummaryReport_general.php";
		}
		else
		{
//			$url = "ClassSummaryReport_".$ReportCardCustomSchoolName.".php";
			if (!file_exists($url)) $url = "ClassSummaryReport_general.php";
		}
		break;
	
	case 1:			# Level Ranking
		if ($ViewFormat == "csv")
		{
//			$url = "export_LevelRankingReport_".$ReportCardCustomSchoolName.".php";
			if (!file_exists($url)) $url = "export_LevelRankingReport_general.php";
		}
		else
		{
//			$url = "LevelRankingReport_".$ReportCardCustomSchoolName.".php";
			if (!file_exists($url)) $url = "LevelRankingReport_general.php";
		}
		
		break;
	/*	
	case 2:			# Progress
		$url = "ProgressReport.php";
		break;
	
	case 3:			# Level Ranking (for Promotion)
		$url = "LevelRankingPromotionReport.php";
		break;
	*/
}

$report_path = $url ."?".$parms;
header("Location: $report_path");

intranet_closedb();

?>