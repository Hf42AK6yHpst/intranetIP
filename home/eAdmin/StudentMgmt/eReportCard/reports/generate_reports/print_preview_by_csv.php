<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
//include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
//$lreportcard = new libreportcard2008w();
include_once($PATH_WRT_ROOT."includes/reportcard_custom/sis.php");
	$lreportcard = new libreportcardcustom();
$linterface = new interface_html();

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

## Consolidate Data from csv file first
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");		
$libgroup = new libgroup($GroupID);
$limport = new libimporttext();

$li = new libdb();
$filepath = $userfile;
$filename = $userfile_name;
		
if($filepath=="none" || $filepath == "")
{   # import failed
    header("Location: generate_by_csv.php");
    exit();
}

if($limport->CHECK_FILE_EXT($filename)) 
{
	# read file into array
	# return 0 if fail, return csv array if success
	$data = $limport->GET_IMPORT_TXT($filepath);
}
else
{
	# import failed
    header("Location: generate_by_csv.php");
    exit();
}

$studentCounter = 0;
$studentInfoArr = array();
$dataSize = count($data);
for ($i=0; $i<$dataSize; $i++)
{
	$thisInfoArr = $data[$i];
	$thisTitle = trim($thisInfoArr[0]);
	
	# skip empty row / row without title / the start row
	if ($thisTitle == "" || strtolower($thisTitle) == "reportinfostart")
	{
		continue;
	}
	
	# if end of student info => initialize variable for next student
	if (strtolower($thisTitle) == "reportinfoend")
	{
		$studentCounter++;
		continue;
	}
	
	# special handling for Column Title
	if (strtolower(substr($thisTitle,0,11)) == "columntitle")
	{
		$thisColumnTitle = stripslashes(trim($thisInfoArr[1]));
		$studentInfoArr[$studentCounter]["ColumnTitle"][] = $thisColumnTitle;
		continue;
	}
	
	# special handling for Subject Info
	if (strtolower(substr($thisTitle,0,7)) == "subject")
	{
		$thisSubjectTitle = stripslashes(trim($thisInfoArr[1]));
		$studentInfoArr[$studentCounter]["Subject"][] = $thisSubjectTitle;
		
		# Store the corresponding subject marks
		$numOfColumn = count($studentInfoArr[$studentCounter]["ColumnTitle"]);
		for ($j=0 ; $j<$numOfColumn ; $j++)
		{
			# marks info starts at $thisInfoArr[2]
			$thisIndex = $j + 2;
			$thisMark = stripslashes(trim($thisInfoArr[$thisIndex]));
			
			$studentInfoArr[$studentCounter][$thisSubjectTitle][] = $thisMark;
		}
		
		continue;
	}
	
	# others data
	$thisInfo = stripslashes(trim($thisInfoArr[1]));
	$studentInfoArr[$studentCounter][$thisTitle] = $thisInfo;
}

# Get the eRC Template Settings
include_once($PATH_WRT_ROOT."includes/eRCConfig.php");
include_once($PATH_WRT_ROOT."templates/2007a/layout/print_header.php");

//$eRCtemplate = $lreportcard->getCusTemplate();
$eRtemplateCSS = $lreportcard->getTemplateCSS();

?>




<? if($eRtemplateCSS) {?><link href="<?=$eRtemplateCSS?>" rel="stylesheet" type="text/css" /><? } ?>

<style type='text/css' media='print'>
 .print_hide {display:none;}
</style>
<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>
<?

for($i=0; $i<sizeof($studentInfoArr); $i++)
{
	$thisInfoArr = $studentInfoArr[$i];
	
	$TitleTable ="";
	$TitleTable = $lreportcard->getReportHeaderByCSV($thisInfoArr);
	$StudentInfoTable = $lreportcard->getReportStudentInfoByCSV($thisInfoArr);
	$MSTable = $lreportcard->getMSTableByCSV($thisInfoArr);
	$MiscTable = $lreportcard->getMiscTableByCSV($thisInfoArr);
	$SignatureTable = $lreportcard->getSignatureTable();
	//$FooterRow = $eRCtemplate->getFooter($ReportID);
	
	$TopTable = "";
	$TopTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" valign='top' align='center'>\n";
	$TopTable .= "<tr valign='top'><td>".$TitleTable."</td></tr>\n";
	$TopTable .= "<tr valign='top'><td>".$StudentInfoTable."</td></tr>\n";
	$TopTable .= "<tr valign='top'><td>".$MSTable."</td></tr>\n";
	$TopTable .= "<tr valign='top'><td>".$MiscTable."</td></tr>\n";
	$TopTable .= "</table>\n";
	
	$BottomTable = "";
	$BottomTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" valign='bottom' align='center'>\n";
	$BottomTable .= "<tr><td>".$SignatureTable."</td></tr>\n";
	$BottomTable .= $FooterRow."\n";
	$BottomTable .= "</table>\n";
	
	?>
	<div id="container">
	<?= ($i==0) ? "<br>":"" ?>
	<table width="100%" border="0" cellpadding="0" cellspacing="0" <? if($i < sizeof($studentInfoArr)-1) {?>style="page-break-after:always"<? } ?>>
	<tr height='570px'><td valign='top' align='center'><?=$TopTable?></td><tr>
	<tr><td valign='bottom' align='center'><?=$BottomTable?></td><tr>
	</table>
	</div>
	<?
}
?>
<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>
