<?php

/****************************************************
 * Modification Log
 * 20170801 Bill:	[2016-1214-1548-50240]
 * 		Support Grand Average Grade Adjustment	($eRCTemplateSetting['GrandAverageGradeManualAdjustment'])
 * 20120220 Marcus:
 * 		Cater manual adjust assessment position  
 * 20120210 Marcus:
 * 		Improved score display for special case 
 * 20090401 Marcus:
 * 		Cater adjust OrderMeritSubjectGroup 
 * 20091216 Marcus:
 * 		Add View Report Card Btn 
 * ***************************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php"); 
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
}
else {
	$lreportcard = new libreportcard();
}

if (!$lreportcard->hasAccessRight()) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
include($PATH_WRT_ROOT."includes/eRCConfig.php");
$OrderingPositionBy = $eRCTemplateSetting['OrderingPositionBy'];

###### Get Report Detail ###### 

	### Get Report Info ###
	$ReportInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
	$SemID = $ReportInfo['Semester'];
	$ReportType = $SemID == "F" ? "W" : "T";		
	$ClassLevelID = $ReportInfo["ClassLevelID"];
	$ReportTitle = $ReportInfo["ReportTitle"];
	$ReportTitle = str_replace(":_:","<br>",$ReportTitle);
	
	### Get Student Info ###
	$luser = new libuser($StudentID);
	$StudentInfo = $lreportcard->Get_Student_Class_ClassLevel_Info($StudentID);
	$ClassName = intranet_session_language=="en"?$StudentInfo[0]["ClassName"]:$StudentInfo[0]["ClassNameCh"];
	$StudentInfoStr = $luser->UserName()." (".$ClassName."-".$StudentInfo[0]["ClassNumber"].")";
	
	### Get Marks ###
	$MarksArr = $lreportcard->getMarks($ReportID, $StudentID,"",0,0);
		
	### Build SemIDColumnIDMap
	$ColumnData = $lreportcard->returnReportTemplateColumnData($ReportID);

	foreach((array)$ColumnData as $Data) {
		$SemIDColumnIDMap[$Data["SemesterNum"]] = $Data["ReportColumnID"];
	}

	### Get Column Title ###
	$DisplayTermID = array();
	$ColumnArr = $ReportType=='W'?$lreportcard->returnReportInvolvedSem($ReportID):$lreportcard->returnReportColoumnTitleWithOrder($ReportID);
	//debug_pr($ColumnArr);
	if ($ReportType=='W' && $eRCTemplateSetting['Report']['ReportGeneration']['ManualAdjustment']['CA_SA_forConsolidatedReport']) {
		$allowAdjustAssessmentPosition = false;
		
		$DisplayTermID[] = '-1';	//CA
		$DisplayTermID[] = '-2';	//SA
		
		$ColumnTitleArr[] = 'CA';
		$ColumnTitleArr[] = 'SA';
		
		$AssessmentMarksArr = $lreportcard->getMarks_consolidateSpecial($ReportID, $StudentID, $DisplayTermID);
		foreach ((array)$AssessmentMarksArr[$StudentID] as $_subjectId => $_subjectMarkAry) {
			$AssessmentMarksArr[$StudentID][$_subjectId][0] = $MarksArr[$_subjectId][0];
		}
		
		$MarksArr = $AssessmentMarksArr[$StudentID];
	}
	else {
		$allowAdjustAssessmentPosition = true;
		
		foreach((array)$ColumnArr as $SemID => $ColumnTitle)
		{
			$TermReport = $lreportcard->checkTermReportExists($ClassLevelID, $SemID);
			if(empty($TermReport) && $ReportType == 'W')
			{
				$DisplayTermID[] = $SemIDColumnIDMap[$SemID];
				$ColumnTitleArr[] = $ColumnTitle;
			}
			else if($ReportType == "T")
			{
				$ColumnTitleArr[] = $ColumnTitle;
			}	
		}
	}
	

	### Get Manual Adjust List ###
	$ManualAdjustedArr = $lreportcard->Get_Manual_Adjustment($ReportID, $StudentID);
	
	### GET_FROM_SUBJECT_GRADING_SCHEME ###
	$GradingScheme = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($ClassLevelID, 0, $ReportID);

	### Get Grand Average Grading Scheme ###
	// [2016-1214-1548-50240]
	if ($eRCTemplateSetting['GrandAverageGradeManualAdjustment']) {
		$GrandSchemeArr = $lreportcard->Get_Grand_Mark_Grading_Scheme($ClassLevelID, $ReportID);
		$AverageSchemeID = $GrandSchemeArr['-1']['SchemeID'];
	}

###### Get Report Detail End ######

###### Update Assessment Position toggle CheckBox ######
	if ($allowAdjustAssessmentPosition) {
		$UpdateAssessmentPositionCheckBox = $linterface->Get_Checkbox("UpdateAssessment", "", 1, 0, $Class='', $eReportCard['ManualAdjustmentArr']['UpdateAssessmentPosition'], "js_Toggle_Assessment_Position(this);", $Disabled='');
	}
###### Update Assessment Position toggle CheckBox End ######

###### Gen Table ######
	### Table Top ###
	$table .= '<table  width="96%" border="0" cellspacing="0" cellpadding="4">';
		$table .= '<tr class="tabletop">';
			$table .= '<td class="tabletopnolink">'.$eReportCard['Subject'].'</td>';
			foreach((array)$ColumnTitleArr as $ColumnTitle)
			{
				$table .= '<td class="tabletopnolink">'.$ColumnTitle."<br>".$eReportCard['Result'].'</td>';
				$table .= '<td class="tabletopnolink AssessmentPosition" style="display:none;">'.$ColumnTitle."<br>".$eReportCard['Template']['ClassPosition'].'</td>';
				$table .= '<td class="tabletopnolink AssessmentPosition" style="display:none;">'.$ColumnTitle."<br>".$eReportCard['Template']['FormPosition'].'</td>';
				if($eRCTemplateSetting['OrderMeritSubjectGroupManualAdjustment'])
					$table .= '<td class="tabletopnolink AssessmentPosition" style="display:none;">'.$eReportCard['Template']['SubjGroupPosition'].'</td>';
			}
			$table .= '<td class="tabletopnolink">'.$eReportCard['Template']['OverallCombined']."<br>".$eReportCard['Result'].'</td>';
			$table .= '<td class="tabletopnolink">'.$eReportCard['Template']['OverallCombined']."<br>".$eReportCard['Template']['ClassPosition'].'</td>';
			$table .= '<td class="tabletopnolink">'.$eReportCard['Template']['OverallCombined']."<br>".$eReportCard['Template']['FormPosition'].'</td>';
			if($eRCTemplateSetting['OrderMeritSubjectGroupManualAdjustment'])
				$table .= '<td class="tabletopnolink">'.$eReportCard['Template']['SubjGroupPosition'].'</td>';
		$table .= '</tr>';
	
	### Table Content ###
	#########################################################################
	#   The ids of the editable field should be in this format:             #
	#   id="(OtherInfoName)_(ReportColumnID)_(SubjectID)_(OriginalValue)"   # 
	#   * For Grand Marks/Positions, SubjectID should be 0                  #
	#########################################################################
	//foreach((array)$MarksArr as $thisSubjectID => $SubjectRow)
	foreach ((array)$GradingScheme as $thisSubjectID => $thisSubjectGradingSchemeInfoAry)
	{
		$SubjectRow = $MarksArr[$thisSubjectID];
		
		# get ScaleDisplay
		$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $thisSubjectID, 0, 0, $ReportID);
		$ScaleDisplay = $SubjectFormGradingSettings['ScaleDisplay'];
			
		$ObjSubject = new subject($thisSubjectID);
		if ($ObjSubject->Is_Component_Subject())
		{
			$prefix = '&nbsp;&nbsp;&nbsp;&nbsp;';
		}
		else
		{
			$prefix = '';
			$mainSubjectCount++;
		}
		$tr_css = ($mainSubjectCount % 2 == 1) ? "tablerow2" : "tablerow1";

		$table .= '	<tr class="'.$tr_css.'">';
		$table .= '		<td class="tabletext" width="35%">'.$prefix.$lreportcard->GET_SUBJECT_NAME($thisSubjectID).'</td>';

		### Column and Overall Marks
		foreach	((array)$SubjectRow as $thisReportColumnID => $ColumnMark)
		{
			if($ReportType=="W"&&!in_array($thisReportColumnID,$DisplayTermID)&&$thisReportColumnID!=0) continue;
			
			$thisAdjustedScore = $ManualAdjustedArr[$StudentID][$thisReportColumnID][$thisSubjectID]['Score'];
			$thisGrade = ($ScaleDisplay=="G" || $ScaleDisplay=="" || $ColumnMark['Grade']!='' )? $ColumnMark['Grade'] : "";
			$thisMark = ($ScaleDisplay=="M" && $thisGrade=='') ? $ColumnMark['Mark'] : "";
			
			$thisMark = ($ScaleDisplay=="M" && strlen($thisMark)) ? $thisMark : $thisGrade;
			
			# check special case
			list($thisMark, $needStyle) = $lreportcard->checkSpCase($ReportID, $thisSubjectID, $thisMark, $ColumnMark['Grade']);
			
			$thisDisplayMark = $thisAdjustedScore?"$thisAdjustedScore<span style='color:#aaa'> ($thisMark)</span>":$thisMark;

			$table .= '		<td class="tabletext" ><div width="30px" class="edit" onmouseout="Hide_Edit_Background(this);" onmouseover="Show_Edit_Background(this);" id="Score_'.$thisReportColumnID.'_'.$thisSubjectID.'_'.$thisMark.'" >'.$thisDisplayMark.'</div></td>';
			
			# Class Position
			$ClassPosition = $ColumnMark['OrderMeritClass']==-1?$lreportcard->EmptySymbol:$ColumnMark['OrderMeritClass']; 
			$AdjustedClassPosition = $ManualAdjustedArr[$StudentID][$thisReportColumnID][$thisSubjectID]['OrderMeritClass'];
			$DisplayClassPosition = $AdjustedClassPosition?$AdjustedClassPosition."<span style='color:#aaa'> ($ClassPosition)</span>":$ClassPosition;
			# Form Position
			$FormPosition = $ColumnMark['OrderMeritForm']==-1?$lreportcard->EmptySymbol:$ColumnMark['OrderMeritForm'];
			$AdjustedFormPosition = $ManualAdjustedArr[$StudentID][$thisReportColumnID][$thisSubjectID]['OrderMeritForm'];
			$DisplayFormPosition =  $AdjustedFormPosition?$AdjustedFormPosition."<span style='color:#aaa'> ($FormPosition)</span>":$FormPosition;
			# Subject Group Position
			if($eRCTemplateSetting['OrderMeritSubjectGroupManualAdjustment'])
			{
				$SubjGroupPosition = $ColumnMark['OrderMeritSubjectGroup']<=0?$lreportcard->EmptySymbol:$ColumnMark['OrderMeritSubjectGroup'];  
				$AdjustedSubjGroupPosition = $ManualAdjustedArr[$StudentID][$thisReportColumnID][$thisSubjectID]['OrderMeritSubjectGroup'];
				$DisplaySubjGroupPosition =  $AdjustedSubjGroupPosition?$AdjustedSubjGroupPosition."<span style='color:#aaa'> ($SubjGroupPosition)</span>":$SubjGroupPosition;
			}

			if($thisReportColumnID!=0 || $thisReportColumnID==='CA' || $thisReportColumnID==='SA') // default show overall only
			{
				$display_none = ' style="display:none;" ';
				$AssessPositionClass = ' AssessmentPosition ';
			}
			else
			{
				$display_none = '';
				$AssessPositionClass = '';
			}

			# Position
			$table .= '		<td class="tabletext '.$AssessPositionClass.'" '.$display_none.'><div width="30px" class="edit" onmouseout="Hide_Edit_Background(this);" onmouseover="Show_Edit_Background(this);" id="OrderMeritClass_'.$thisReportColumnID.'_'.$thisSubjectID.'_'.$ClassPosition.'">'.$DisplayClassPosition.'</div></td>';
			$table .= '		<td class="tabletext '.$AssessPositionClass.'" '.$display_none.'><div width="30px" class="edit" onmouseout="Hide_Edit_Background(this);" onmouseover="Show_Edit_Background(this);" id="OrderMeritForm_'.$thisReportColumnID.'_'.$thisSubjectID.'_'.$FormPosition.'">'.$DisplayFormPosition.'</div></td>';
			if($eRCTemplateSetting['OrderMeritSubjectGroupManualAdjustment'])
				$table .= '		<td class="tabletext '.$AssessPositionClass.'" '.$display_none.'><div width="30px" class="edit" onmouseout="Hide_Edit_Background(this);" onmouseover="Show_Edit_Background(this);" id="OrderMeritSubjectGroup_'.$thisReportColumnID.'_'.$thisSubjectID.'_'.$SubjGroupPosition.'">'.$DisplaySubjGroupPosition.'</div></td>';
		
			### Get Grand Marks ### (for Grand Marks in Table Foot)
			if(!isset($GrandMarksArr[$thisReportColumnID]))
				$GrandMarksArr[$thisReportColumnID] = $lreportcard->getReportResultScore($ReportID, $thisReportColumnID, $StudentID,'',0,0);
			
		}	//end foreach $SubjectRow

		$table .= '	</tr>';
		
	} //end foreach $MarksArr
	
	### Table Foot
	$CalSetting = $lreportcard->LOAD_SETTING("Calculation");
	$CalculationMethod = ($ReportType == 'T')? $CalSetting['OrderTerm'] : $CalSetting['OrderFullYear'];
	
	# Grand Display Settings
	$GrandArr = array();
	$GrandArr["GrandTotal"] = "GrandTotal";
	$GrandArr["GrandAverage"] = "GrandAverage";
	if ($eRCTemplateSetting['Calculation']['ActualAverage']) {
		$GrandArr["ActualAverage"] = "ActualAverage";
	}
	$GrandArr["GPA"] = "GPA";
	if ($eRCTemplateSetting['OrderPositionMethod'] == "WeightedSD") {
		$GrandArr["GrandSDScore"] = "GrandStandardScore";
	}
	if ($eRCTemplateSetting['GrandAverageGradeManualAdjustment']) {
		$GrandArr["GrandAverageGrade"] = "GrandAverageGrade";
	}
	
	foreach((array)$GrandArr as $DBField => $LangName)
	{
		$table .= '	<tr class="tablerow2">';
		$table .= '		<td class="tabletext" width="35%">'.$eReportCard['Template'][$LangName].'</td>';
		foreach((array)$GrandMarksArr as $thisReportColumnID => $thisGrandMarks)
		{
			// [2016-1214-1548-50240] Grand Average Grade row
			if($eRCTemplateSetting['GrandAverageGradeManualAdjustment'] && $DBField=="GrandAverageGrade") {
				// Get Converted Grade and Adjusted Grade
				$thisGrandGrade = $GrandMarksArr[$thisReportColumnID]['GrandAverage'];
				$thisGrandGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($AverageSchemeID, $thisGrandGrade, $ReportID, $StudentID, -1, $ClassLevelID, 0);
				$thisAdjustedGrade = $ManualAdjustedArr[$StudentID][$thisReportColumnID][0]['GrandAverageGrade'];
				$thisDisplayMark = $thisAdjustedGrade? "$thisAdjustedGrade<span style='color:#aaa'> ({$thisGrandGrade})</span>" : $thisGrandGrade;
				
				// Default show overall only
				if($thisReportColumnID!=0 || $thisReportColumnID==='CA' || $thisReportColumnID==='SA') {
					$display_none = ' style="display:none;" ';
					$AssessPositionClass = ' AssessmentPosition ';
				}
				else {
					$display_none = '';
					$AssessPositionClass = '';
				}
				
				// Row Content
				$table .= '	<td class="tabletext" ><div width="30px" class="edit" onmouseout="Hide_Edit_Background(this);" onmouseover="Show_Edit_Background(this);" id="GrandAverageGrade_'.$thisReportColumnID.'_0_'.$thisGrandGrade.'">'.$thisDisplayMark.'</div></td>';
				$table .= '	<td class="tabletext '.$AssessPositionClass.'" '.$display_none.'>'.$lreportcard->EmptySymbol.'</td>';
				$table .= '	<td class="tabletext '.$AssessPositionClass.'" '.$display_none.'>'.$lreportcard->EmptySymbol.'</td>';
				if($eRCTemplateSetting['OrderMeritSubjectGroupManualAdjustment']) {
					$table .= '	<td class="tabletext '.$AssessPositionClass.'" '.$display_none.'>'.$lreportcard->EmptySymbol.'</td>';
				}
				
				continue;
			}
			
			$thisAdjustedScore = $ManualAdjustedArr[$StudentID][$thisReportColumnID][0][$DBField];
			$thisDisplayMark = $thisAdjustedScore?"$thisAdjustedScore<span style='color:#aaa'> ({$GrandMarksArr[$thisReportColumnID][$DBField]})</span>":$GrandMarksArr[$thisReportColumnID][$DBField];
			$table .= '		<td class="tabletext" ><div width="30px" class="edit" onmouseout="Hide_Edit_Background(this);" onmouseover="Show_Edit_Background(this);" id="'.$DBField.'_'.$thisReportColumnID.'_0_'.$GrandMarksArr[$thisReportColumnID][$DBField].'">'.$thisDisplayMark.'</div></td>';
			
			# Class Position
			$GrandClassRank = $GrandMarksArr[$thisReportColumnID]['OrderMeritClass']?$GrandMarksArr[$thisReportColumnID]['OrderMeritClass']:$lreportcard->EmptySymbol;
			$AdjustedGrandClassRank = $ManualAdjustedArr[$StudentID][$thisReportColumnID][0]['OrderMeritClass'];
			$DisplayGrandClassRank = $AdjustedGrandClassRank?$AdjustedGrandClassRank."<span style='color:#aaa'> ($GrandClassRank)</span>":$GrandClassRank;
			
			# Form Position
			$GrandFormRank = $GrandMarksArr[$thisReportColumnID]['OrderMeritForm']?$GrandMarksArr[$thisReportColumnID]['OrderMeritForm']:$lreportcard->EmptySymbol;
			$AdjustedGrandFormRank = $ManualAdjustedArr[$StudentID][$thisReportColumnID][0]['OrderMeritForm'];
			$DisplayGrandFormRank = $AdjustedGrandFormRank?$AdjustedGrandFormRank."<span style='color:#aaa'> ($GrandFormRank)</span>":$GrandFormRank;
			
			# Subject Group Position
			if($eRCTemplateSetting['OrderMeritSubjectGroupManualAdjustment']) {
				$GrandSubjGroupRank = $GrandMarksArr[$thisReportColumnID]['OrderMeritSubjectGroup']?$GrandMarksArr[$thisReportColumnID]['OrderMeritSubjectGroup']:$lreportcard->EmptySymbol;
				$AdjustedGrandSubjGroupRank = $ManualAdjustedArr[$StudentID][$thisReportColumnID][0]['OrderMeritSubjectGroup'];
				$DisplayGrandSubjGroupRank = $AdjustedGrandSubjGroupRank?$AdjustedGrandSubjGroupRank."<span style='color:#aaa'> ($GrandSubjGroupRank)</span>":$GrandSubjGroupRank;
			}
			
			// Default show overall only
			if($thisReportColumnID!=0 || $thisReportColumnID==='CA' || $thisReportColumnID==='SA') {
				$display_none = ' style="display:none;" ';
				$AssessPositionClass = ' AssessmentPosition ';
			}
			else {
				$display_none = '';
				$AssessPositionClass = '';
			}
			
			$table .= '		<td class="tabletext '.$AssessPositionClass.'" '.$display_none.'>'.($CalculationMethod==1&&$OrderingPositionBy==$DBField?'<div width="30px" class="edit" onmouseout="Hide_Edit_Background(this);" onmouseover="Show_Edit_Background(this);" id="OrderMeritClass_'.$thisReportColumnID.'_0_'.$GrandClassRank.'">'.$DisplayGrandClassRank.'</div>':$lreportcard->EmptySymbol).'</td>';
			$table .= '		<td class="tabletext '.$AssessPositionClass.'" '.$display_none.'>'.($CalculationMethod==1&&$OrderingPositionBy==$DBField?'<div width="30px" class="edit" onmouseout="Hide_Edit_Background(this);" onmouseover="Show_Edit_Background(this);" id="OrderMeritForm_'.$thisReportColumnID.'_0_'.$GrandFormRank.'">'.$DisplayGrandFormRank.'</div>':$lreportcard->EmptySymbol).'</td>';
			if($eRCTemplateSetting['OrderMeritSubjectGroupManualAdjustment']) {
				$table .= '		<td class="tabletext '.$AssessPositionClass.'" '.$display_none.'>'.($CalculationMethod==1&&$OrderingPositionBy==$DBField?'<div width="30px" class="edit" onmouseout="Hide_Edit_Background(this);" onmouseover="Show_Edit_Background(this);" id="OrderMeritSubjectGroup_'.$thisReportColumnID.'_0_'.$GrandSubjGroupRank.'">'.$DisplayGrandSubjGroupRank.'</div>':$lreportcard->EmptySymbol).'</td>';
			}
		}
		$table .= '	</tr>';	
	}
	
	if($CalculationMethod==2)
	{
		$PositionArr = array();
		$PositionArr["OrderMeritClass"] = "ClassPosition";
		$PositionArr["OrderMeritForm"] = "FormPosition";
		if($sys_custom['Class']['ClassGroupSettings'] == true) {
			$PositionArr["OrderMeritStream"] = "StreamPosition";
		}
		
		foreach((array)$PositionArr as $DBField => $LangName)
		{
			$table .= '	<tr class="tablerow2">';
			$table .= '		<td class="tabletext" width="35%">'.$eReportCard['Template'][$LangName].'('.$eReportCard['Template'][$OrderingPositionBy].')</td>';
			foreach((array)$GrandMarksArr as $thisReportColumnID => $thisGrandMarks)
			{
				$thisAdjustedScore = $ManualAdjustedArr[$StudentID][$thisReportColumnID][0][$DBField];
				$OriValue = !$GrandMarksArr[$thisReportColumnID][$DBField]?$lreportcard->EmptySymbol:$GrandMarksArr[$thisReportColumnID][$DBField];
				$thisDisplayRank = $thisAdjustedScore?"$thisAdjustedScore<span style='color:#aaa'> ({$OriValue})</span>":$OriValue;	
				$table .= '		<td class="tabletext"><div width="30px" class="edit" onmouseout="Hide_Edit_Background(this);" onmouseover="Show_Edit_Background(this);" id="'.$DBField.'_'.$thisReportColumnID.'_0_'.$OriValue.'">'.$thisDisplayRank.'</div></td>';	
				
				// Default show overall only
				if($thisReportColumnID!=0) {
					$display_none = ' style="display:none;" ';
					$AssessPositionClass = ' AssessmentPosition ';
				}
				else {
					$display_none = '';
					$AssessPositionClass = '';
				}

				$table .= '		<td class="'.$AssessPositionClass.'" '.$display_none.'>'.$lreportcard->EmptySymbol.'</td>';
				$table .= '		<td class="'.$AssessPositionClass.'" '.$display_none.'>'.$lreportcard->EmptySymbol.'</td>';
				if($eRCTemplateSetting['OrderMeritSubjectGroupManualAdjustment']) {
					$table .= '		<td class="'.$AssessPositionClass.'" '.$display_none.'>'.$lreportcard->EmptySymbol.'</td>';
				}
			}

			$table .= '	</tr>';
		}
	}					
	$table .= '</table>';
	
###### Gen Table End ######			

# Tag Information
$CurrentPage = "Reports_GenerateReport";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($eReportCard['Reports_GenerateReport'], "", 0);
$PAGE_NAVIGATION[] = array($eReportCard['ManualAdjustment']);

$linterface->LAYOUT_START();
?>

<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.jeditable.js"></script>
<script>
var popupwin = null;

$(document).ready(function() {
     $('.edit').editable('manual_adjust_ajax_save.php?ReportID=<?=$ReportID?>&StudentID=<?=$StudentID?>', {
        indicator 	:  '<img src="/images/2009a/indicator.gif">',
		tooltip   	: "<?=$Lang['SysMgr']['Location']['ClickToEdit']?>",
		event 		: "click",
		onblur 		: "submit",
		type 		: "text",
		style  		: "display:inline",
		maxlength  	: "255",
		height		: "20px",
		width		: "75px",
		select		: true,
        callback 	: function() {ShowMessage(this)}
     });
     
     $('.edit').click(function() {
     	var InputValue= $(this).find('form input').val();
     	var startIdx = InputValue.search("<span") ;
     	if(startIdx == -1) startIdx = InputValue.search("<SPAN"); // for IE
     	
     	if(startIdx != -1)
     		$(this).find('form input').val(htmlspecialchars_decode(InputValue.substring(0,startIdx)))
     });

});

function ShowMessage(obj)
{
	var returnData = $(obj).html();
	var returnDataAry = returnData.split("|=msg=|");
	if(returnDataAry[1]==0) {
		Get_Return_Message("<?=$eReportCard['MarkAdjustFail']?>")
	}
	else {
		Get_Return_Message("<?=$eReportCard['MarkAdjustSuccess']?>")
		if(popupwin != null && !popupwin.closed)
		{
			popupwin.location.reload();	
			popupwin.focus();
		}
	}
	$(obj).html(returnDataAry[0]);
	
	//eReportCard/reports/generate_reports/print_preview.php?ClassID=34&TargetStudentID[]=1666&TargetStudentID[]=1667&ReportID=6
}

function PopUpPreview()
{
	var ReportID = $("input[name='ReportID']").val();
	var StudentID = $("input[name='StudentID']").val();
	var ClassID = $("input[name='ClassID']").val();
	
	var url = "<?=$PATH_WRT_ROOT?>home/eAdmin/StudentMgmt/eReportCard/reports/generate_reports/print_preview.php?";
	url += "ClassID="+ClassID;
	url += "&TargetStudentID[]="+StudentID;
	url += "&ReportID="+ReportID;
	
	if(popupwin==null || popupwin.closed)
		popupwin = window.open(url,'_blank','',true);
	else
		popupwin.location.reload();

	popupwin.focus();
}

function Show_Edit_Background(Obj) {
	Obj.style.backgroundImage = "url(<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/icon_edit_b.gif)";
	Obj.style.backgroundPosition = "center right";
	Obj.style.backgroundRepeat = "no-repeat";
}

function Hide_Edit_Background(Obj) {
	Obj.style.backgroundImage = "";
	Obj.style.backgroundPosition = "";
	Obj.style.backgroundRepeat = "";
}

function js_Toggle_Assessment_Position(obj){
	if(obj.checked) {
		$(".AssessmentPosition").show();	
	}
	else {
		$(".AssessmentPosition").hide();
	}
}
</script>

<br />

<table border="0" cellspacing="0" cellpadding="4" width="96%">
<tr>
	<td align="left">
		<?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?>
	</td>
</tr>
<tr>
	<td>
		<table border="0" cellspacing="0" cellpadding="4" width="100%">
			<tr>
				<td width="30%" nowrap="nowrap" class="formfieldtitle tabletext" valign="top"><?=$eReportCard['ReportTitle']?></td>
				<td valign="top" class="tabletext" width="70%"><?=$ReportTitle?></td>
			</tr>
			<tr>
				<td width="30%" nowrap="nowrap" class="formfieldtitle tabletext" valign="top"><?=$eReportCard['Student']?></td>
				<td valign="top" class="tabletext" width="70%"><?=$StudentInfoStr?></td>
			</tr>
		</table>
	</td>
</tr>	
<tr>
	<td height="1" class="dotline" ><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="10"></td>
</tr>
</table>
<br>
<?=$UpdateAssessmentPositionCheckBox?>
<form name="form1" method="post" action="manual_adjust.php">
<?=$table?>
<table width="96%" border="0" cellspacing="0" cellpadding="4">
	<tr>
		<td height="1" class="dotline"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td align="center" valign="bottom">
						<?=$linterface->GET_ACTION_BTN($eReportCard['ViewReportCard'], "button", "PopUpPreview()");?>
						<img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif" width="10" height="1">
						<?=$linterface->GET_ACTION_BTN($button_back, "button", "window.location='adjust_list.php?ReportID=$ReportID&ClassID=$ClassID'");?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" name="ReportID" value="<?=$ReportID?>">
<input type="hidden" name="StudentID" value="<?=$StudentID?>">
<input type="hidden" name="ClassID" value="<?=$ClassID?>">
</form>

<br>
<br>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>