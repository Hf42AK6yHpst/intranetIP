<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();


if (!$plugin['ReportCard2008']) 
{
	intranet_closedb();
	header("Location: index.php");
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

if (!$lreportcard->hasAccessRight() || !$ReportID) 
{
	intranet_closedb();
	header("Location: index.php");
}

$linterface = new interface_html();
$CurrentPage = "Reports_GenerateReport";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

$PAGE_NAVIGATION[] = array($eReportCard['ManualAdjustment']);
		
# tag information
$TAGS_OBJ[] = array($eReportCard['Reports_GenerateReport'], "", 0);
$linterface->LAYOUT_START();

$reportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$ClassLevelID = $reportTemplateInfo['ClassLevelID'];
$ReportTitle = $reportTemplateInfo['ReportTitle'];
$ReportTitle = str_replace(":_:", "&nbsp;", $ReportTitle);

// Class
$ClassArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
if (!isset($ClassID) || $ClassID == "")
	$ClassID = $ClassArr[0]["ClassID"];
$ClassSelectBox = $linterface->GET_SELECTION_BOX($ClassArr, "name='ClassID' class='tabletexttoptext' onchange='document.form2.ClassID.value=this.value;document.form2.submit()'", "", $ClassID);

for($i=0 ; $i<count($ClassArr) ;$i++) {
	if ($ClassID == $ClassArr[$i]["ClassID"])
		$ClassName = $ClassArr[$i]["ClassName"];
}

// Student Info Array
$StudentArray = $lreportcard->GET_STUDENT_BY_CLASS($ClassID, '', 1);
$StudentSize = sizeof($StudentArray);
$display = '<table width="95%" cellpadding="5" cellspacing="0" border="0">';
$display .= '<tr>
	          <td width="10%" align="center" class="tablegreentop tabletopnolink">'.$eReportCard['StudentNo_short'].'</td>
	          <td class="tablegreentop tabletopnolink">'.$eReportCard['Student'].'</td>
			  <td width="30%" class="tablegreentop tabletopnolink">'.$eReportCard['LastAdjust'].'</td>
			</tr>';

# Retrieve Last Adjustment Date
$LastAdjustAry = $lreportcard->retrieveLastManualAdjustDate($ReportID);

if (isset($StudentSize) && $StudentSize > 0) 
{
    for($j=0; $j<$StudentSize; $j++){
		list($StudentID, $WebSAMSRegNo, $StudentNo, $StudentName) = $StudentArray[$j];
		$tr_css = ($j % 2 == 1) ? "tablegreenrow2" : "tablegreenrow1";
		
// 		$selectedPromotionStatus = $promotionInfo[$StudentID]["Promotion"];
// 		$selectedPromotionResultID = $promotionInfo[$StudentID]["ResultID"];
		
		$display .= '<tr class="'.$tr_css.'">
						<td align="center" valign="top" class="tabletext">'.$StudentNo.'</td>
						<td class="tabletext" valign="top"><a href="javascript:clickEdit('. $StudentID .');" class="tablelink">'.$StudentName.'</a></td>';
		$display .= '<td class="tabletext" valign="top" width="20%">'.$LastAdjustAry[$StudentID][1]." ". $LastAdjustAry[$StudentID][0].'</td>';
		$display .= '</tr>';
	}
} else {
	$td_colspan = "3";
	$display .=	'<tr class="tablegreenrow1" height="40"><td class="tabletext" colspan="'.$td_colspan.'" align="center">'.$i_no_record_exists_msg.'</td></tr>';
}
$display .= '</table>';


/*
		
		// check if this report have been generated
		if ($reportTemplateInfo["LastGenerated"] == "0000-00-00 00:00:00" || $reportTemplateInfo["LastGenerated"] == "") {
			intranet_closedb();
			header("Location: index.php");
			exit();
		}
		
		
		
		####### Customize checking for SIS Start #######
		if (getenv("SERVER_NAME") == "project6.broadlearning.com" || getenv("SERVER_NAME") == "sis-trial.broadlearning.com") 
		{
			$SemID = $reportTemplateInfo['Semester'];
			$ReportType = $SemID == "F" ? "W" : "T";
			$ColoumnTitle = $lreportcard->returnReportColoumnTitle($ReportID);
			
			if(!((substr($ClassName, 0, 1)=="P" && $ReportType=="W") or (substr($ClassName, 0, 1)=="S" && sizeof($ColoumnTitle)==4))) {
				intranet_closedb();
				header("Location: index.php");
				exit();
			}
		}
		####### Customize checking for SIS Ended #######
		
		// Period (Term or Whole Year)
		#$ReportInfo = $lreportcard->GET_REPORT_TYPES($ClassLevelID, $ReportID);
		$PeriodName = $reportTemplateInfo['SemesterTitle'];
		
		// Student ID list of students in a class
		for($i=0; $i<$StudentSize; $i++)
			$StudentIDArray[] = $StudentArray[$i][0];
		if(!empty($StudentIDArray))
			$StudentIDList = implode(",", $StudentIDArray);
		
		*/
		
// 		$display = '<table class="yui-skin-sam" width="95%" cellpadding="5" cellspacing="0">';
// 		$display .= '<tr>
// 			          <td width="5%" align="center" class="tablegreentop tabletopnolink">'.$eReportCard['StudentNo_short'].'</td>
// 			          <td class="tablegreentop tabletopnolink">'.$eReportCard['Student'].'</td>
// 					  <td width="10%" align="center" class="tablegreentop tabletopnolink">'.
// 						$eReportCard['Promotion'].
// 						'<br />'.$linterface->GET_SELECTION_BOX($promotionOptions, 'name="promotionAll" id="promotionAll" class="tabletexttoptext" onchange="setAll(this.selectedIndex, \'promotionStatus\')"', '', '').
// 					  '</td>
// 					  <td width="10%" class="tablegreentop tabletopnolink">'.$eReportCard['NewClassLevel'].'</td>';
// 	    $display .= '</tr>';
		
		
// 		if (isset($StudentSize) && $StudentSize > 0) {
// 		    for($j=0; $j<$StudentSize; $j++){
// 				list($StudentID, $WebSAMSRegNo, $StudentNo, $StudentName) = $StudentArray[$j];
// 				$tr_css = ($j % 2 == 1) ? "tablegreenrow2" : "tablegreenrow1";
// 				
// 				$selectedPromotionStatus = $promotionInfo[$StudentID]["Promotion"];
// 				$selectedPromotionResultID = $promotionInfo[$StudentID]["ResultID"];
// 				
// 				
// 				$display .= '<tr class="'.$tr_css.'">
// 								<td align="center" valign="top" class="tabletext">'.$StudentNo.'</td>
// 								<td class="tabletext" valign="top">'.$StudentName.'
// 									<input type="hidden" value="'.$selectedPromotionResultID.'" id="SelectedPromotionResultID_'.$selectedPromotionResultID.'" name="SelectedPromotionResultID[]"/>
// 								</td>
// 								<td align="center" class="tabletext" valign="top">';
// 				$display .= $linterface->GET_SELECTION_BOX($promotionSelectBoxArr, "name='promotionStatus[$selectedPromotionResultID]' id='promotionStatus_".$selectedPromotionResultID."' class='tabletexttoptext'", "", $selectedPromotionStatus);
// 				$display .=	'</td>
// 								<td class="tabletext" valign="top">
// 									<input type="text" name="newClassName['.$selectedPromotionResultID.']" id="newClassName_'.$selectedPromotionResultID.'" value="'.$promotionInfo[$StudentID]["NewClassName"].'" class="textboxnum" />
// 								</td>
// 							</tr>';
// 							
// 			}
// 		    //$display .= '</table>';
// 			
// 			$display_button .= $linterface->GET_ACTION_BTN($button_save, "button", "frmSubmit()")."&nbsp";
// 	        $display_button .= $linterface->GET_ACTION_BTN($button_reset, "reset", "")."&nbsp";
// 			$display_button .= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'");
// 		} else {
// 			$td_colspan = "4";
// 			
// 			$display .=	'<tr class="tablegreenrow1" height="40">
// 							<td class="tabletext" colspan="'.$td_colspan.'" align="center">'.$i_no_record_exists_msg.'</td>
// 						</tr>';
// 						
// 		}
		############################################################################################################
        
		
?>
<br>
<script language="javascript">
<!--
function clickEdit(sid)
{
	with(document.form1)
	{
		StudentID.value = sid;
		submit();	
	}
}
//-->
</script>
<br/>
<table border="0" cellspacing="0" cellpadding="4" width="96%">
<tr>
	<td align="left" colspan="2">
		<?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?>
	</td>
</tr>
<tr>
	<td colspan='2'>
		<table border="0" cellspacing="0" cellpadding="4" width="100%">
			<tr>
				<td width="30%" nowrap="nowrap" class="formfieldtitle tabletext" valign="top"><?=$eReportCard['ReportTitle']?></td>
				<td valign="top" class="tabletext" width="70%"><?=$ReportTitle?></td>
			</tr>
			<tr>
				<td width="30%" nowrap="nowrap" class="formfieldtitle tabletext" valign="top"><?=$eReportCard['Class']?></td>
				<td valign="top" class="tabletext" width="70%"><?=$ClassName?></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td><?=$ClassSelectBox?></td>
</tr>
	
</table>

<form name="form1" method="post" action="manual_adjust.php">
<?=$display?>
<table width="96%" border="0" cellspacing="0" cellpadding="4">
	<tr>
		<td height="1" class="dotline"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td align="center" valign="bottom"><?=$linterface->GET_ACTION_BTN($button_back, "button", "window.location='index.php'");?></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="ReportID" value="<?=$ReportID?>">
<input type="hidden" name="StudentID" value="<?=$StudentID?>">
<input type="hidden" name="ClassID" value="<?=$ClassID?>">
</form>

<form name="form2" method="post" action="adjust_list.php">
<input type="hidden" name="ReportID" value="<?=$ReportID?>" />
<input type="hidden" name="ClassID" value="<?=$ClassID?>">
</form>
<br />
<br>
<?
		intranet_closedb();
        $linterface->LAYOUT_STOP();
?>