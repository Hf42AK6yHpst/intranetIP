<?php
// using:

/**
* 20170217 Bill : Added export column ($eRCTemplateSetting['Report']['BatchExportReport'])
* 20160427 Bill	: Added loading message when generating report [2016-0330-1524-42164] 
* 20140801 Ryan : Added Generated by csv for sis cust 
**/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$arrCookies[] = array("eRC_Report_ReportGeneration_ClassLevelID", "eRC_Report_ReportGeneration_ClassLevelID");
$arrCookies[] = array("eRC_Report_ReportGeneration_Semester", "eRC_Report_ReportGeneration_Semester");
if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
	$eRC_Report_ReportGeneration_ClassLevelID = '';
	$eRC_Report_ReportGeneration_Semester = '';
}
else {
	if (isset($_GET['ClassLevelID'])) {
		$eRC_Report_ReportGeneration_ClassLevelID = $_GET['ClassLevelID'];
	}
	if (isset($_GET['Semester'])) {
		$eRC_Report_ReportGeneration_Semester = $_GET['Semester'];
	}
	updateGetCookies($arrCookies);
	
	$ClassLevelID = $eRC_Report_ReportGeneration_ClassLevelID;
	$Semester = $eRC_Report_ReportGeneration_Semester;
}

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
}
else {
	$lreportcard = new libreportcard();
}

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lreportcard_ui = new libreportcard_ui();
$linterface = new interface_html();

$CurrentPage = "Reports_GenerateReport";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eReportCard['Reports_GenerateReport'], "", 0);
$linterface->LAYOUT_START();

# Semester Selection box
$ReportTemplateTerms = $lreportcard->returnReportTemplateTerms();
$array_term_name = array();
$array_term_data = array();
for($i=0; $i<sizeof($ReportTemplateTerms); $i++) {
	$array_term_data[] = $ReportTemplateTerms[$i][0];
	$array_term_name[] = $ReportTemplateTerms[$i][1];
}
$allTypesOption = $eReportCard['AllTypes'];
$ReportTemplateTermsSelection = getSelectByValueDiffName($array_term_data,$array_term_name,'name="Semester" onChange="window.location=\'index.php?Semester=\'+this.value+\'&ClassLevelID='. $ClassLevelID .'\'"',$Semester,1,0, $allTypesOption);

$Semester = isset($Semester) ? $Semester : "";

# ClassLevelID Selection box
$FormArr = $lreportcard->GET_ALL_FORMS(1);
for($i=0; $i<sizeof($FormArr); $i++) {
	$classLevelName[$FormArr[$i]["ClassLevelID"]] = $FormArr[$i]["LevelName"];
}
$AllFormOption = array(
					0 => array(
						0 => "-1", 
						1 => $eReportCard['AllForms']
					)
				);
$FormArr = array_merge($AllFormOption, $FormArr);

if ($ClassLevelID == "") {
	$ClassLevelID = "-1";
}

# Filters - By Form (ClassLevelID)
$FormSelection = $linterface->GET_SELECTION_BOX($FormArr, 'name="ClassLevelID" onChange="window.location=\'index.php?ClassLevelID=\'+this.value+\'&Semester='. $Semester .'\'"', "", $ClassLevelID);

$display = $lreportcard_ui->GenReportList($Semester, $ClassLevelID);

if($eRCTemplateSetting['GenerateReportByCSV']){
	# Generate Report by CSV link SIS 20140725
	$csvGenerateReportLink = "<a href='./generate_by_csv.php' class='tablelink'>".$eReportCard['GenerateReportFromCSV']."</a>";
}
?>

<script language="javascript">
<!--
function clickGenReport(ReportID)
{
	var semester = document.getElementById('semester').value;
	var params = 'ReportID='+ReportID;
	params += (semester != '') ? '&Semester='+semester : '';

	if(confirm("<?=$eReportCard['jsConfirmToGenerateReport']?>"))
	{
		// display loading message
		document.getElementById('GenBtn_'+ReportID).style.display = 'none';
		if($('#sendingDiv_'+ReportID).length){
			document.getElementById('sendingDiv_'+ReportID).style.display = '';
		}
		
		window.location='generate.php?'+params;
	}
}

function clickPrintMenu(ReportID)
{
	newWindow('print_menu.php?ReportID='+ReportID, '15');
}

function clickExportMenu(ReportID)
{
	newWindow('print_menu.php?ReportID='+ReportID+'&isExport=1', '15');
}

function clickPromotion(ReportID, ClassLevelID)
{
	var params = 'ReportID='+ReportID+'&ClassLevelID='+ClassLevelID;
	window.location='promotion_edit.php?'+params;
}

function clickAdjust(ReportID)
{
	var params = 'ReportID='+ReportID;
	window.location='adjust_list.php?'+params;
}

function clickGenerateAwardMenu(ReportID)
{
	var params = 'ReportID='+ReportID;
	window.location='generate_award/generate_award.php?'+params;
}
//-->
</script>
<br />						
										
<!-- Start Main Table //-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td align="center" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="right"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                
                <tr>
                  <td align="right" class="tabletextremark">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                          <td><table border="0" cellspacing="0" cellpadding="2">
                              <tr>
                                <td><?=$FormSelection?> <?=$ReportTemplateTermsSelection?></td>
                                <? if($eRCTemplateSetting['GenerateReportByCSV']){ ?>
                            		<td><?=$csvGenerateReportLink?></td>
                            	<? } ?>
                              </tr>
                          </table></td>
                        <td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($msg);?></td>
                      </tr>
                  </table></td>
                </tr>
            </table></td>
          </tr>
          <tr>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="4">
                <tr class="tabletop">
                  <td><span class="tabletopnolink"><?=$eReportCard['ReportTitle']?></span></td>
                  <td><span class="tabletopnolink"><?=$eReportCard['Type']?></span></td>
                  <td><span class="tabletopnolink"><?=$eReportCard['Form']?></span></td>
                  <td><span class="tabletopnolink"><?=$eReportCard['Preview']?></span></td>
                  <td><span class="tabletopnolink"><?=$eReportCard['IssueDate']?></span></td>
                  <td><span class="tabletopnolink"><?=$eReportCard['Generation']?></span></td>
                  <? if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration']) { ?>
                    <td><span class="tabletopnolink"><?=$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardGeneration']?></span></td>
                  <? } ?>
                  
                  <? if($sys_custom['eRC']['Report']['ReportGeneration']['Promotion']){ ?>
               	  	<td><span class="tabletopnolink"><?=$eReportCard['Promotion']?></span></td>
                  <? } ?>
                  <? if($eRCTemplateSetting['Report']['ReportGeneration']['HideManualAdjustment'] == false){ ?>
                  <td><span class="tabletopnolink"><?=$eReportCard['ManualAdjustment']?></span></td> 
                  <? } ?>
                  <? if($eRCTemplateSetting['Report']['BatchExportReport']) { ?>
                  	<td><span class="tabletopnolink"><?=$button_export?></span></td>
                  <? } ?>
                  <td><span class="tabletopnolink"><?=$button_print?></span></td>
                  <td><span class="tabletopnolink">&nbsp;</span></td>
                </tr>
                
                <?=$display?>
                
            </table></td>
          </tr>
          <tr>
            <td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
          </tr>
        </table>
        </td>
      </tr>
    </table>
    </td>
  </tr>
</table>

<input type="hidden" name="semseter" id="semester" value="<?=$Semester?>"/>
<!-- End Main Table //-->

<br />

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>