<?php
# using: 

@SET_TIME_LIMIT(21600);
@ini_set(memory_limit, -1);

/**************************************************
 * 	Modification log
 *  20201109 Bill   [2020-0708-0950-12308]
 *      - support P.C. Lau Cust
 *  20170213 Bill	[2017-0210-1003-52073]
 * 		- BIBA Cust
 * 		- use pageFooter as default footer
 * 	20160602 Bill	[2016-0330-1524-42164]
 * 		- fixed: preview cannot show english version
 * 	20160427 Bill	[2016-0330-1524-42164]
 * 		- display selected language version for ES Semester Report
 * 	20151015 Bill	[2015-1013-1434-16164]
 * 		- BIBA Cust
 * 		- support KG Progress Report
 *  20150710 Bill
 * 		- BIBA Cust
 * 		- support MS Transcript
 * 	20150519 Bill
 * 		- BIBA Cust
 * 		- empty UserID if preview report
 *  20150514 Bill	
 * 		- Copy from print_preview.php
 * ***********************************************/

##############################################################################################################################
### If you have modified this page, please check if you have to modify /management/archive_report_card/archive_update.php also
##############################################################################################################################

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# Access right checking
if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
    include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	if ($ReportCardCustomSchoolName == "biba_cn")
	{
        $lreportcard = new libreportcardcustom("subject_topic");
	}
	else
	{
        $lreportcard = new libreportcardcustom();
	}
}
else
{
	$lreportcard = new libreportcard();
}

# Access right checking
if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");

# Get the eRC Template Settings
include_once($PATH_WRT_ROOT."includes/eRCConfig.php");

// [2020-0818-1009-50206] Handle IP30 upgrade - report layout changed issue
if($eRCTemplateSetting['IsApplyIP30Style']) {
    // do nothing
} else {
    $LAYOUT_SKIN = '2009a';
}
$eRCtemplate = $lreportcard->getCusTemplate();

# Access right checking
if (!$eRCTemplateSetting['Report']['ExportPDFReport'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

# Data from $_POST
$SubjectID = $_REQUEST['SubjectID'];
$SubjectSectionOnly = $_REQUEST['SubjectSectionOnly'];
$CustomSchoolCode = $_REQUEST['CustomSchoolCode'];
$PrintTemplateType = $_REQUEST['PrintTemplateType'];
$PrintTemplateType = ($PrintTemplateType=='')? 'normal' : $PrintTemplateType;

# Report Info
$ReportSetting = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$isMainReport = $ReportSetting['isMainReport'];
$ClassLevelID = $ReportSetting['ClassLevelID'];
$SemID = $ReportSetting['Semester'];
$ReportType = $SemID == "F" ? "W" : "T";
$SemesterNo = $lreportcard->Get_Semester_Seq_Number($SemID);

// [2015-1013-1434-16164] get Report Type
$isBilingualReport = false;
if($eRCTemplateSetting['Settings']['AutoSelectReportType'])
{
	$FormNumber = $lreportcard->GET_FORM_NUMBER($ClassLevelID);
	
	// ES/MS Report
	if (is_numeric($FormNumber))
	{
		$FormNumber = intval($FormNumber);
		$isESReport = $FormNumber > 0 && $FormNumber < 6;
		$isMSReport = !$isESReport;
		
		// check if need bilingual report
		// [2016-0330-1524-42164] added selected report lang checking
		$isBilingualReport = $eRCTemplateSetting['Report']['ReportGeneration']['GenerateBilingualReport'] && $isMainReport && $isESReport && ($PreviewReport==1 || $ReportLang=="bilingual");
	}
	// KG Report
	else
    {
		$isKGReport = true;
	}
}

$html = "";

# CSS
$eRCtemplate = empty($eRCtemplate) ? $lreportcard : $eRCtemplate;
$eRtemplateCSS = $lreportcard->getCSSContent($ReportID);
if($eRtemplateCSS)
{
	$html .= $eRtemplateCSS;
	$html .= "<body>";
}

# Student list
$TargetStudentList = (is_array($TargetStudentID)) ? implode(",", $TargetStudentID) : "";
$StudentIDAry = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $ClassID, $TargetStudentList);
$numOfStudent = count((array)$StudentIDAry);

// BIBA Cust
// Set UserID as empty if user preview report
if($PreviewReport)
{
	$StudentIDAry = array();
	$StudentIDAry[]['UserID'] = "";
	$numOfStudent = count((array)$StudentIDAry);
}

// get student records
if($eRCTemplateSetting['Settings']['AutoSelectReportType'] && $isMainReport)
{
	// get attendance records for semester report
	include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
	$libcardstudentarrend = new libcardstudentattend2();
	
	$eRCtemplate->EnableEntryLeavePeriod = $libcardstudentarrend->EnableEntryLeavePeriod;
	$eRCtemplate->ProfileAttendCount = $libcardstudentarrend->ProfileAttendCount;
	$eRCtemplate->retrieveAttandanceMonthData($StudentIDAry, $SemID, $SemesterNo);
    
	// get gpa for MS Transcript
	if($isMSReport)
	{
		$eRCtemplate->retrieveGPAData($ReportID, $StudentIDAry);
	}
}

# Set up ePDF Object
$eRCtemplate->reportObjectSetUp($ReportID, $isBilingualReport);
if (!isset($eRCtemplate->pdf))
{
	header ("Location: /");
	intranet_closedb();
	exit();
}
if(isset($eRCtemplate->pdf))
{
	$mpdfObj = $eRCtemplate->pdf;
}

# Define header
$TitleTable = $eRCtemplate->getReportHeader($ReportID, $StudentID, $PrintTemplateType);
$mpdfObj->DefHTMLHeaderByName("titleHeader", $TitleTable);
$mpdfObj->SetHTMLHeaderByName("titleHeader");

# Define footer
$mpdfObj->DefHTMLFooterByName("emptyFooter", "");
$FooterRow = $eRCtemplate->getFooter($ReportID, $PrintTemplateType);
$mpdfObj->DefHTMLFooterByName("pageFooter", $FooterRow);

if ($ReportCardCustomSchoolName == "alliance_pc_lau") {
    // do nothing
} else {
    $mpdfObj->writeHTML($html);
}

for($s=0; $s<$numOfStudent; $s++)
{
	$StudentID = $StudentIDAry[$s]['UserID'];

	if ($ReportCardCustomSchoolName == "alliance_pc_lau")
    {
        $TitleTable = $eRCtemplate->getReportHeader($ReportID, $StudentID, $PrintTemplateType);
        $mpdfObj->DefHTMLHeaderByName("titleHeader", $TitleTable);
        $mpdfObj->SetHTMLHeaderByName("titleHeader");

        if($s == 0) {
            $mpdfObj->writeHTML($html);
        } else {
            $mpdfObj->writeHTML("<pagebreak resetpagenum='1' />");
        }

        $TitleTable2 = $eRCtemplate->getReportHeader($ReportID, $StudentID, $PrintTemplateType, true);
        $mpdfObj->DefHTMLHeaderByName("titleHeader2", $TitleTable2);
    }
	
	// [2017-0210-1003-52073] default use pageFooter
	//$mpdfObj->SetHTMLFooterByName("emptyFooter");
	$mpdfObj->SetHTMLFooterByName("pageFooter");
	
	$html = "";
	
	$reportLoop = $isBilingualReport ? 2 : 1;
	for($i=0; $i<$reportLoop; $i++) 
	{
	    $isBilingualEngVersion = ($isBilingualReport && $i > 0) || $ReportLang == "english";

        # Get HTML content
        // [2016-0330-1524-42164] display english version if $i > 0 (after display chinese version) OR $ReportLang == "english"
        $StudentInfoTable = $eRCtemplate->getReportStudentInfo($ReportID, $StudentID, $forPage3='', $PageNum=1, $PrintTemplateType, $isBilingualEngVersion);
        $MSTable = $eRCtemplate->getMSTable($ReportID, $StudentID, $PrintTemplateType, $PageNum=1, $isBilingualEngVersion);
        $MiscTable = $eRCtemplate->getMiscTable($ReportID, $StudentID, $PrintTemplateType, $isBilingualEngVersion);
        $SignatureTable = $eRCtemplate->getSignatureTable($ReportID, $StudentID, $PrintTemplateType, $isBilingualEngVersion);
        //$FooterRow = $eRCtemplate->getFooter($ReportID, $PrintTemplateType);
		
		if($i > 0)
		{
            $html .= "<pagebreak />";
			$mpdfObj->writeHTML($html);
			
			$html = "";
		}

		# Build HTML
        if ($ReportCardCustomSchoolName == "alliance_pc_lau")
        {
            $html .= "<div id=\"container\" style=\"width:210mm; height:297mm; background-color:white;\">";
            //$mpdfObj->writeHTML($html);

            if($StudentInfoTable != "")
            {
                $html .= $StudentInfoTable;
                $mpdfObj->writeHTML($html);

                $html = "";
            }

            if($MSTable != "")
            {
                $html .= $MSTable;
                $mpdfObj->writeHTML($MSTable);

                $html = "";
            }

            if($MiscTable != "")
            {
                $mpdfObj->SetHTMLHeaderByName("titleHeader2");

                $html .= $MiscTable;
                $mpdfObj->writeHTML($MiscTable);

                $html = "";
            }

            if($SignatureTable != "")
            {
                $html .= $SignatureTable;
                $mpdfObj->writeHTML($SignatureTable);

                $html = "";
            }

            $html = "</div>";

            $mpdfObj->SetHTMLHeaderByName("titleHeader");
        }
        else
        {
            // BIBA MS Transcript
            if($eRCTemplateSetting['Settings']['AutoSelectReportType'] && $isMainReport && $isMSReport)
            {
                $html .= "<div id=\"container\" style=\"width:297mm; height:210mm; background-color:white;\">";
            }
            else
            {
                $html .= "<div id=\"container\" style=\"width:210mm; height:297mm; background-color:white;\">";
            }
            $html .= $StudentInfoTable;
            $html .= $MSTable;
            $html .= $MiscTable;
            $html .= $SignatureTable;
            $html .= "</div>";
        }
	}
	$mpdfObj->writeHTML($html);

	//$mpdfObj->SetHTMLFooterByName("signFooter");

	// Last Page no need to add pagebreak (and reset page number)
    if ($ReportCardCustomSchoolName == "alliance_pc_lau")
    {
        // do nothing
    }
	else if ($s != ($numOfStudent-1))
	{
		$mpdfObj->writeHTML("<pagebreak resetpagenum='1' />");
	}
}

$mpdfObj->writeHTML("</body>");

// report file name
$filename = "";
$filename .= $isESReport? "ES_" : ($isKGReport? "KG_" : "MS_");
$filename .= $isMainReport? ($isKGReport? "Progress_" : "Semester_") : "Progress_";
$filename .= "Report";
$filename = $isMainReport && $isMSReport? "MS_Transcipt" : $filename;

$mpdfObj->Output("$filename.pdf", 'I');
//echo $html;

?>