<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008']) 
{
	intranet_closedb();
	header("Location: index.php");
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_sis.php");
$lreportcard = new libreportcardSIS();

if (!$lreportcard->hasAccessRight() || !$ReportID || !$StudentID || !$ClassID) 
{
	intranet_closedb();
	header("Location: index.php");
}

$ReportSetting = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$ClassLevelID = $ReportSetting['ClassLevelID'];
$ClassLevel = $lreportcard->returnClassLevel($ClassLevelID);
$SemID = $ReportSetting['Semester'];
$ReportType = $SemID == "F" ? "W" : "T";

# Table Header
$TermHeaderAry = $lreportcard->genGrandMSTermHeader($ReportID);
# Subject
$SubjNameAry = $lreportcard->genGrandMSSubject($ReportID);

// debug_r($TermHeaderAry);
// debug_r($SubjNameAry);
// debug_r($ReportID);
// debug_r($StudentID);

$update_success = 1;
switch(substr($ClassLevel, 0, 1))
{
	case "P":					# Primary 
		if($ReportType=="T")	# Term Report
		{
			foreach($SubjNameAry as $SubjectID=>$SubjectName)
			{
				$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID,0, 0, $ReportID);
				$ScaleDisplay = $SubjectFormGradingSettings[ScaleDisplay];
		 		$SchemeID = $SubjectFormGradingSettings[SchemeID];
 		
				$SchemeInfo = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($SchemeID);
				$SchemeType = $SchemeInfo['SchemeType'];
		
				$SpecialCaseArr = $SchemeType=="H" ? $lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1() : $lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2();

				foreach($TermHeaderAry as $ColID=>$ColName)
				{
					$thisMark = $_POST['Mark_'.$SubjectID.'_'.$ColID];
					$Mark = $ScaleDisplay=="M" ? $thisMark : "";
					$Grade = $ScaleDisplay=="G" ? $thisMark : "";
					if(in_array($thisMark, $SpecialCaseArr))
					{
						$Mark = 0;
						$Grade = $thisMark;
					}
					
					$flag = $lreportcard->UPDATE_REPORT_RESULT_SCORE($ReportID, $StudentID, 0, $SubjectID, $Mark, $Grade);
					$update_success = $flag ? $update_success : 0;
				}
			}
		}
		else					# Whole Year Report
		{
// 			foreach($SubjAry as $SubjID=>$SubjName)
// 			{
// 				foreach($TermAry as $cid=>$TermName)
// 				{
// 					$thisMark = $MarkAry[$SubjID][$cid]['Mark'] ? $MarkAry[$SubjID][$cid]['Mark'] : $MarkAry[$SubjID][$cid]['Grade'];
// 					$x[$SubjID][$cid] = "<input name='Mark_".$SubjID."_".$cid."' type='text' class='textboxnum' maxlength='5' value='$thisMark'/>";
// 				}
// 			}
		}
		break;
		
	case "S":					# Secondary
// 		foreach($SubjAry as $SubjID=>$SubjName)
// 		{
// 			$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjID);
// 			$ScaleDisplay = $SubjectFormGradingSettings[ScaleDisplay];
// 		
// 			foreach($TermAry as $cid=>$TermName)
// 			{
// 				$thisMark = $MarkAry[$SubjID][$cid]['Mark'] ? $MarkAry[$SubjID][$cid]['Mark'] : $MarkAry[$SubjID][$cid]['Grade'];
// 				if(!$cid && $ScaleDisplay=="G")	continue;
// 				$x[$SubjID][$cid] = "<input name='Mark_".$SubjID."_".$cid."' type='text' class='textboxnum' maxlength='5' value='$thisMark'/>";
// 			}
// 		}
// 		
// 		# For Final Report Only
// 		$ColumnData = $this->returnReportTemplateColumnData($ReportID);
// 		if(sizeof($ColumnData)==4)	
// 		{
// 			$SecondComOverallMarksAry = $this->returnSecondCombinedOverallMarks($ReportID, $StudentID);

// 			foreach($SecondComOverallMarksAry as $thisSubjID => $data)
// 			{
// 				$SubjectFormGradingSettings = $this->GET_SUBJECT_FORM_GRADING($ClassLevelID, $thisSubjID);
// 				$ScaleDisplay = $SubjectFormGradingSettings[ScaleDisplay];
// 			
// 				$thisMark = $data[0]['Mark'] ? $data[0]['Mark'] : $data[0]['Grade'];
// 				if($ScaleDisplay=="M")
// 					$x[$thisSubjID]['sec'] = "<input name='Mark_".$thisSubjID."_sec' type='text' class='textboxnum' maxlength='5' value='$thisMark'/>";				
// 				else
// 					$x[$thisSubjID]['sec'] = "";				
// 			} 
// 		}
		break;
}


if($update_success)
{
	$data['LastAdjusted'] = date("Y-m-d H:i:s");
	$lreportcard->UpdateReportTemplateBasicInfo($ReportID, $data);
}
		
// foreach($SubjNameAry as $SubjectID=>$SubjectName)
// {
// 	foreach($TermHeaderAry as $ColID=>$ColName)
// 	{
// 		$t = $_POST['Mark_'.$SubjectID.'_'.$ColID];
// 		
// 	}
// }









exit;
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008']) 
{
	intranet_closedb();
	header("Location: index.php");
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
$lreportcard = new libreportcard2008w();
if (!$lreportcard->hasAccessRight() || !$ReportID) 
{
	intranet_closedb();
	header("Location: index.php");
}

$linterface = new interface_html();
$CurrentPage = "Reports_GenerateReport";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($eReportCard['Reports_GenerateReport'], "", 0);
$linterface->LAYOUT_START();

echo $ReportID;
echo "<hr>";
echo $StudentID;












### Button
// $AddBtn 	= "<a href=\"javascript:checkNew('new.php')\" class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_new . "</a>";
// $editBtn 	= "<a href=\"javascript:checkEdit(document.form1,'AnnouncementID[]','edit.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";
// $delBtn 	= "<a href=\"javascript:checkRemove(document.form1,'AnnouncementID[]','remove.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_remove . "</a>";



?>


<br />

<form name="form1" method="get" action="index.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
                              	<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
                                        	<table border="0" cellspacing="0" cellpadding="2">
                                            	<tr>
                                              		<td><p><?=$AddBtn?></p></td>
                                            	</tr>
                                          	</table>
					</td>
                                        <td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($xmsg);?></td>
				</tr>

				<tr>
                                	<td class="tabletext">
                                        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                	<td class="tabletext" nowrap><?=$select_status?></td>
                                                        <td width="100%" align="left" class="tabletext"><?=$searchTag?></td>
						</tr>
                                                </table>
					</td>
					<td align="right" valign="bottom" height="28">
						<table border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="21"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_01.gif" width="21" height="23" /></td>
							<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_02.gif">
								<table border="0" cellspacing="0" cellpadding="2">
								<tr>
									<td nowrap><?=$delBtn?></td>
                                                                        <td><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="5"></td>
                                                                        <td nowrap><?=$editBtn?></td>
								</tr>
        							</table>
							</td>
        						<td width="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_03.gif" width="6" height="23" /></td>
						</tr>
						</table>
					</td>
				</tr>
                                <tr>
                                	<td colspan="2">
						<?//=$li->display();?>
					</td>
				</tr>
                                
                                
				</table>
			</td>
		</tr>
                </table>
        </td>
</tr>
</table>
<br />

</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
