<?php
// editing by
/*
 * 	Date: 2018-04-20 (Bill)     [2018-0420-1705-13235]
 * 			fixed: PHP 5.4 error message when no students received Subject Awards
 */

$PageRight = "ADMIN";

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");

$lexport = new libexporttext();
$lreportcard_award = new libreportcard_award();

$lreportcard_award->hasAccessRight();

$ReportID = $_REQUEST['ReportID'];
$ExportMode = $_REQUEST['ExportMode']; 
$YearClassID = $_REQUEST['YearClassID'];
$AwardName = trim(intranet_undo_htmlspecialchars(urldecode(stripslashes($_REQUEST['AwardName']))));


### Get Report Info
$ReportInfoArr = $lreportcard_award->returnReportTemplateBasicInfo($ReportID);
$ClassLevelID = $ReportInfoArr['ClassLevelID'];


### Csv Header
$ExportColumnTitleArr = array();
$ColumnInfoArr = $lreportcard_award->Get_Report_Award_Csv_Header_Info();
$numOfColumn = count($ColumnTitleInfoArr);

$ColumnTitleArr = $lreportcard_award->Get_Report_Award_Csv_Header_Title();
$ColumnPropertyArr = $lreportcard_award->Get_Report_Award_Csv_Header_Property();
$ExportColumnTitleArr = $lexport->GET_EXPORT_HEADER_COLUMN($ColumnTitleArr, $ColumnPropertyArr);


### Csv Content
$StudentInfoAssoArr = $lreportcard_award->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $YearClassID, $ParStudentID="", $ReturnAsso=1, $isShowStyle=0);
$StudentIDArr = array_keys($StudentInfoAssoArr);


if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_ManageGeneratedResultOnly']) {
	// Subject Info
	$libscm = new subject_class_mapping();
	$SubjectInfoArr = $libscm->Get_Subject_List();
	$SubjectInfoAssoArr = BuildMultiKeyAssoc($SubjectInfoArr, 'SubjectID', array('SubjectDescEN', 'SubjectDescB5', 'WEBSAMSCode'), $SingleValue=0, $BuildNumericArray=0);
}


$ExportDataArr = array();
$RowCounter = 0;
$FileNameArr = array();
$FileNameArr[] = 'report_award';
$FileNameArr[] = $lreportcard_award->returnClassLevel($ClassLevelID);
if ($ExportMode == 'Class') {
	### Get Student Award Info
	if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_ManageGeneratedResultOnly']) {
		$StudentAwardInfoArr = $lreportcard_award->Get_Award_Generated_Student_Record($ReportID, $StudentIDArr, $ReturnAsso=0, $AwardNameWithSubject=0);
		$StudentAwardAssoArr = BuildMultiKeyAssoc($StudentAwardInfoArr, array('StudentID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
	}
	else {
		$StudentAwardAssoArr = $lreportcard_award->Get_Award_Student_Record($ReportID, $StudentIDArr);	
	}
	
	### Build Export Data
	foreach((array)$StudentInfoAssoArr as $thisStudentID => $thisStudentInfoArr)
	{
		if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_ManageGeneratedResultOnly']) {
			$thisNumOfStudentAward = count((array)$StudentAwardAssoArr[$thisStudentID]);
			
			for ($i=0; $i<$thisNumOfStudentAward; $i++) {
				$thisSubjectID = $StudentAwardAssoArr[$thisStudentID][$i]['SubjectID'];
				$thisAwardName = $StudentAwardAssoArr[$thisStudentID][$i]['AwardName'];
				$thisAwardCode = $StudentAwardAssoArr[$thisStudentID][$i]['AwardCode'];
			
				$ExportDataArr[$RowCounter][] = $thisStudentInfoArr['ClassTitleEn'];
				$ExportDataArr[$RowCounter][] = $thisStudentInfoArr['ClassNumber'];
				$ExportDataArr[$RowCounter][] = $thisStudentInfoArr['UserLogin'];
				$ExportDataArr[$RowCounter][] = $thisStudentInfoArr['WebSAMSRegNo'];
				$ExportDataArr[$RowCounter][] = $thisStudentInfoArr['StudentName'];
				$ExportDataArr[$RowCounter][] = $thisAwardName;
				$ExportDataArr[$RowCounter][] = $thisAwardCode;
				$ExportDataArr[$RowCounter][] = Get_Lang_Selection($SubjectInfoAssoArr[$thisSubjectID]['SubjectDescB5'], $SubjectInfoAssoArr[$thisSubjectID]['SubjectDescEN']);
				$ExportDataArr[$RowCounter][] = $SubjectInfoAssoArr[$thisSubjectID]['WEBSAMSCode'];
				
				$RowCounter++;
			}
		}
		else {
			$ExportDataArr[$RowCounter][] = $thisStudentInfoArr['ClassTitleEn'];
			$ExportDataArr[$RowCounter][] = $thisStudentInfoArr['ClassNumber'];
			$ExportDataArr[$RowCounter][] = $thisStudentInfoArr['UserLogin'];
			$ExportDataArr[$RowCounter][] = $thisStudentInfoArr['WebSAMSRegNo'];
			$ExportDataArr[$RowCounter][] = $thisStudentInfoArr['StudentName'];
			$ExportDataArr[$RowCounter][] = $StudentAwardAssoArr[$ReportID][$thisStudentID]['AwardText'];
			
			$RowCounter++;
		}
	}
	
	### Filename
	if ($YearClassID != '') {
		$ClassInfoArr = $lreportcard_award->GET_CLASSES_BY_FORM($ParClassLevelID="", $ClassID="", $returnAssoName=0, $returnAssoInfo=1);
		$FileNameArr[] = $ClassInfoArr[$YearClassID]['ClassTitleEN'];
	}
}
else if ($ExportMode == 'Award') {
	if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_ManageGeneratedResultOnly']) {
		### Get Award List
		$AwardInfoAssoArr = $lreportcard_award->Get_Report_Award_Info($ReportID, $MapByCode=0);
		$numOfAward = count($AwardInfoAssoArr);
		
		### Get Student Award Info
		$StudentAwardInfoArr = $lreportcard_award->Get_Award_Generated_Student_Record($ReportID, $StudentIDArr='', $ReturnAsso=0, $AwardNameWithSubject=1);
		$StudentAwardAssoArr = BuildMultiKeyAssoc($StudentAwardInfoArr, array('AwardID', 'SubjectID', 'StudentID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=0);
		
		
		foreach((array)$AwardInfoAssoArr as $thisAwardID => $thisAwardInfoArr) {
			if ($AwardID != '' && $AwardID != $thisAwardID) {
				continue;
			}
			
			$thisAwardType = $thisAwardInfoArr['BasicInfo']['AwardType'];
			$thisAwardcode = $thisAwardInfoArr['BasicInfo']['AwardCode'];
			$thisAwardName = Get_Lang_Selection($thisAwardInfoArr['BasicInfo']['AwardNameCh'], $thisAwardInfoArr['BasicInfo']['AwardNameEn']);
			
			if ($thisAwardType == 'SUBJECT') {
				$thisAwardSubjectIDArr = array_keys((array)$StudentAwardAssoArr[$thisAwardID]);
			}
			else {
				$thisAwardSubjectIDArr = array();
				$thisAwardSubjectIDArr[] = 0;
			}
			$numOfSubject = count($thisAwardSubjectIDArr);
			
			for ($i=0; $i<$numOfSubject; $i++) {
				$thisSubjectID = $thisAwardSubjectIDArr[$i];
				
				if ($SubjectID != '' && $SubjectID != $thisSubjectID) {
					continue;
				}
				
				foreach ((array)$StudentAwardAssoArr[$thisAwardID][$thisSubjectID] as $thisStudentID => $thisStudentAwardInfoArr) {
					$thisStudentInfoArr = $StudentInfoAssoArr[$thisStudentID];
					
					$ExportDataArr[$RowCounter][] = $thisStudentInfoArr['ClassTitleEn'];
					$ExportDataArr[$RowCounter][] = $thisStudentInfoArr['ClassNumber'];
					$ExportDataArr[$RowCounter][] = $thisStudentInfoArr['UserLogin'];
					$ExportDataArr[$RowCounter][] = $thisStudentInfoArr['WebSAMSRegNo'];
					$ExportDataArr[$RowCounter][] = $thisStudentInfoArr['StudentName'];
					$ExportDataArr[$RowCounter][] = $thisAwardName;
					$ExportDataArr[$RowCounter][] = $thisAwardCode;
					$ExportDataArr[$RowCounter][] = Get_Lang_Selection($SubjectInfoAssoArr[$thisSubjectID]['SubjectDescB5'], $SubjectInfoAssoArr[$thisSubjectID]['SubjectDescEN']);
					$ExportDataArr[$RowCounter][] = $SubjectInfoAssoArr[$thisSubjectID]['WEBSAMSCode'];
					
					$RowCounter++;
				}
			}
		}
	}
	else {
		$AwardInfoArr = $lreportcard_award->Get_Report_Award_Full_List($ReportID, $FromAwardTextOnly=1);
		$numOfAward = count($AwardInfoArr);
		
		for ($i=0; $i<$numOfAward; $i++) {
			$thisAwardName = $AwardInfoArr[$i]['AwardName'];
			$thisAwardNameDisplay = $AwardInfoArr[$i]['AwardNameDisplay'];
			$thisAwardType = $AwardInfoArr[$i]['AwardType'];
			
			if ($AwardName != '' && strstr($thisAwardName, $AwardName) == false) {
				continue;
			}
			
			$thisStudentAwardInfoArr = $lreportcard_award->Get_Student_With_AwardText($ReportID, $thisAwardName);
			$thisAwardStudentIDArr = Get_Array_By_Key($thisStudentAwardInfoArr, 'StudentID');
			
			foreach((array)$StudentInfoAssoArr as $thisStudentID => $thisStudentInfoArr) {		// Loop $StudentInfoAssoArr to maintain Class and Class Number ordering
				if (!in_array($thisStudentID, $thisAwardStudentIDArr)) {
					continue;
				}
				
				$ExportDataArr[$RowCounter][] = $thisStudentInfoArr['ClassTitleEn'];
				$ExportDataArr[$RowCounter][] = $thisStudentInfoArr['ClassNumber'];
				$ExportDataArr[$RowCounter][] = $thisStudentInfoArr['UserLogin'];
				$ExportDataArr[$RowCounter][] = $thisStudentInfoArr['WebSAMSRegNo'];
				$ExportDataArr[$RowCounter][] = $thisStudentInfoArr['StudentName'];
				$ExportDataArr[$RowCounter][] = $thisAwardNameDisplay;
				
				$RowCounter++;
			}
		}
	}
	
	
	### Filename
	if ($AwardName != '') {
		$FileNameArr[] = $AwardName;
	}
}


$export_content = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportDataArr, $ExportColumnTitleArr, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);


$filename = parseFileName(implode('_', $FileNameArr).'.csv');

intranet_closedb();
$lexport->EXPORT_FILE($filename, $export_content);
?>