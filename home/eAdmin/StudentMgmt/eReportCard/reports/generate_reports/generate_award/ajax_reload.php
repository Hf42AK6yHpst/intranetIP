<?php
$PATH_WRT_ROOT = "../../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();


include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");

$linterface = new interface_html();
$lreportcard_award = new libreportcard_award();
$lreportcard_award->hasAccessRight();

$Action = $_REQUEST['Action'];

if ($Action == 'Report_Award_Last_Generated_Display') {
	$ReportID = $_REQUEST['ReportID'];
	
	$ReportInfoArr = $lreportcard_award->returnReportTemplateBasicInfo($ReportID);
	$LastGeneratedAwardDate = $ReportInfoArr['LastGeneratedAward'];
	$LastGeneratedAwardDateDisplay = (is_date_empty($LastGeneratedAwardDate)) ? '&nbsp;': $eReportCard['LastGenerate'].': '.$LastGeneratedAwardDate;
	
	echo $LastGeneratedAwardDateDisplay;
}
else if ($Action == 'Edit_Award_Student_Table') {
	$ReportID = $_REQUEST['ReportID'];
	$EditMode = $_REQUEST['EditMode'];
	$YearClassID = $_REQUEST['YearClassID'];
	$AwardName = trim(intranet_undo_htmlspecialchars(urldecode(stripslashes($_REQUEST['AwardName']))));
	$AwardID = $_REQUEST['AwardID'];
	$SubjectID = $_REQUEST['SubjectID'];
	
	$lreportcard_ui = new libreportcard_ui();
	
	if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_ManageGeneratedResultOnly']) {
		echo $lreportcard_ui->Get_Edit_Award_Student_Table_By_Generated_Result($ReportID, $EditMode, $YearClassID, $AwardName, $AwardID, $SubjectID);
	}
	else {
		echo $lreportcard_ui->Get_Edit_Award_Student_Table($ReportID, $EditMode, $YearClassID, $AwardName);
	}
	
}
else if ($Action == 'Reload_Add_Student_Award_Layer') {
	$StudentID = $_POST['StudentID'];
	$ReportID = $_POST['ReportID'];
	
	$lreportcard_ui = new libreportcard_ui();
	echo $lreportcard_ui->Get_Add_Student_Award_Layer_UI($StudentID, $ReportID);
}
else if ($Action == 'Reload_Award_Subject_Selection') {
	$StudentID = $_POST['StudentID'];
	$ReportID = $_POST['ReportID'];
	$AwardID = $_POST['AwardID'];
	$SelectionID = trim(stripslashes($_POST['SelectionID']));
	
	$ReportBasicInfoArr = $lreportcard_award->returnReportTemplateBasicInfo($ReportID);
	$ClassLevelID = $ReportBasicInfoArr['ClassLevelID'];
	
	$StudentAwardInfoArr = $lreportcard_award->Get_Award_Generated_Student_Record($ReportID, $StudentID);
	$ExcludeSubjectIDArr = array_keys((array)$StudentAwardInfoArr[$StudentID][$AwardID]);
	
	echo $lreportcard_award->Get_Subject_Selection($ClassLevelID, $SubjectID='', $SelectionID, $ParOnchange='', $isMultiple=0, $includeComponent=0, $InputMarkOnly=0, $includeGrandMarks=0, $otherTagInfo='', $ReportID='', $ParentSubjectAsOptGroup=0, $ExcludeWithoutSubjectGroup=0, $TeachingOnly=0, $ExcludeSubjectIDArr);
}
else if ($Action == 'Reload_Add_Award_Student_Layer') {
	$ReportID = $_POST['ReportID'];
	$AwardID = $_POST['AwardID'];
	$SubjectID = $_POST['SubjectID'];
	
	$lreportcard_ui = new libreportcard_ui();
	echo $lreportcard_ui->Get_Add_Award_Student_Layer_UI($ReportID, $AwardID, $SubjectID);
}

intranet_closedb();
?>