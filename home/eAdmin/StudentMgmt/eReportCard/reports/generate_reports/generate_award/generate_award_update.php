<?php
$PATH_WRT_ROOT = "../../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");
$lreportcard_award = new libreportcard_award();
$lreportcard_award->hasAccessRight();


$ReportID = $_REQUEST['ReportID'];
$AwardPageSettingsArr = $_REQUEST['AwardPageSettingsArr'];

if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_CustomizedLogic']) {
	$success = $lreportcard_award->Generate_Report_Award_Customized($ReportID);
}
else {
	$success = $lreportcard_award->Generate_Report_Award($ReportID, $AwardPageSettingsArr);
}

intranet_closedb();

echo ($success)? '1' : '0';
?>