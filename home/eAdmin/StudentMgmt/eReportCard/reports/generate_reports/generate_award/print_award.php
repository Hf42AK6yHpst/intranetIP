<?php
$PATH_WRT_ROOT = "../../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_reportSettings.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");


$ReportID = $_REQUEST['ReportID'];
$ReturnMsgKey = $_REQUEST['ReturnMsgKey'];


$lreportcard = new libreportcardcustom();
$lreportcard_reportSettings = new libreportcard_reportSettings($ReportID);
$linterface = new interface_html();
$lreportcard_ui = new libreportcard_ui();
$lreportcard_award = new libreportcard_award();

$lreportcard->hasAccessRight();

$CurrentPage = "Reports_GenerateReport";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eReportCard['Reports_GenerateReport'], "", 0);
$linterface->LAYOUT_START();

# sub-tag information
$htmlAry['subTag'] = $lreportcard_ui->Get_Generate_Award_SubTag($ReportID, $CurrentTag=3);


### Get Report Card Info
$ReportInfoArr = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$ReportTitle = $ReportInfoArr['ReportTitle'];
$ReportTitle = str_replace(':_:', '<br />', $ReportTitle);
$ClassLevelID = $ReportInfoArr['ClassLevelID'];
$Semseter = $ReportInfoArr['Semester'];
$ReportType = $lreportcard_award->Get_Award_Generate_Report_Type($ReportID);
$ReportTypeDisplay = $lreportcard_ui->Get_Report_Card_Type_Display($ReportID, 1);
$FormName = $lreportcard->returnClassLevel($ClassLevelID);

$certIssueDate = $lreportcard_reportSettings->getSettingsValue('certIssueDate');


### Navaigtion
$PAGE_NAVIGATION[] = array($eReportCard['Reports_GenerateReport'], "javascript:js_Go_Back_To_Report_Generation();");
$PAGE_NAVIGATION[] = array($Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ViewAward'], "");
		
### Buttons
$BackBtn = $linterface->GET_ACTION_BTN($Lang['eReportCard']['ReportArr']['ReportGenerationArr']['BackToReportGeneration'], "button", "js_Go_Back_To_Report_Generation();");

$x = '';
$x .= '<form id="form1" name="form1" method="post">'."\n";
	$x .= '<br style="clear:both" />'."\n";
	$x .= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
	$x .= '<div class="table_board">'."\n";
		$x .= '<table id="html_body_frame" width="100%">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= '<table class="form_table_v30">'."\n";
						$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'.$eReportCard['ReportTitle'].'</td>'."\n";
							$x .= '<td>'.$ReportTitle.'</td>'."\n";
						$x .= '</tr>'."\n";
						$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'.$eReportCard['ReportType'].'</td>'."\n";
							$x .= '<td>'.$ReportTypeDisplay.'</td>'."\n";
						$x .= '</tr>'."\n";
						$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'.$Lang['SysMgr']['FormClassMapping']['Form'].'</td>'."\n";
							$x .= '<td>'.$FormName.'</td>'."\n";
						$x .= '</tr>'."\n";
						$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'.$eReportCard['IssueDate'].'</td>'."\n";
							$x .= '<td>'.$linterface->GET_DATE_PICKER('certIssueDate', $certIssueDate).'</td>'."\n";
						$x .= '</tr>'."\n";
					$x .= '</table>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		$x .= '<br />'."\n";
		$x .= $lreportcard_ui->Get_Print_Award_Table($ReportID);
		$x .= '<br />'."\n";
	$x .= '</div>'."\n";
	
	# Buttons
	$x .= '<div class="edit_bottom_v30">'."\n";
		$x .= $BackBtn;
	$x .= '</div>'."\n";
	
	# Hidden Field
	$x .= '<input type="hidden" id="ReportID" name="ReportID" value="'.$ReportID.'">'."\n";
	
$x .= '</form>'."\n";
$x .= '<br />'."\n";
$htmlAry['pageHtml'] = $x;
?>
<?=$htmlAry['subTag']?>
<?=$htmlAry['pageHtml']?>
<script language="javascript" src="common.js"></script>
<script language="javascript">
$(document).ready( function() {
	
});

function goPrint() {
	if ($('input.awardChk:checked').length == 0) {
		alert(globalAlertMsg2);
	}
	else {
		$('form#form1').attr('action', 'print_cert.php').attr('target', '_blank').submit();
	}
}
</script>
<br />
<br />
                        
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>