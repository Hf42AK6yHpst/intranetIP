<?php
ini_set('memory_limit','128M');
$PATH_WRT_ROOT = "../../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_reportSettings.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");

$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();


$reportId = IntegerSafe($_POST['ReportID']);
$certIssueDate = standardizeFormPostValue($_POST['certIssueDate']);
/*
 * [awardAry] => Array
    (
        [3] => Array		// awardId
            (
                [0] => 0	// subjectId
            )

        [4] => Array		// awardId
            (
                [0] => 1	// subjectId
                [1] => 8	// subjectId
            )

    )
 */
$awardAssoAry = $_POST['awardAry'];


// save issue date for next printing
$lreportcard_reportSettings = new libreportcard_reportSettings($reportId);
$settingsAry = array();
$settingsAry['certIssueDate'] = $certIssueDate;
$lreportcard_reportSettings->saveSettings($settingsAry);


$lreportcard->initializeCertificatePdfObject();
$lreportcard->generateCertificatePdfHtml($reportId, $awardAssoAry, $certIssueDate);

intranet_closedb();
$lreportcard->outputPdfFile("certificate_".date("Ymd_His").".pdf");
?>