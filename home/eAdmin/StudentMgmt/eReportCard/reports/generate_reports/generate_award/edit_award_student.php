<?php
// Using: Bill

#########################################
#
#	Date:	2016-08-09 (Bill)	[2016-0224-1423-31073]
#			exclude students that conduct grade received not high enough (for Award View)
#			($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_LimitByReceivedGrade'] = true)
#
#########################################

$PATH_WRT_ROOT = "../../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");

$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();


$ReportID = $_REQUEST['ReportID'];
$EditMode = $_REQUEST['EditMode'];
$YearClassID = $_REQUEST['YearClassID'];
$AwardName = trim(intranet_undo_htmlspecialchars(urldecode(stripslashes($_REQUEST['AwardName']))));
$AwardID = $_REQUEST['AwardID'];
$SubjectID = $_REQUEST['SubjectID'];
$ReturnMsgKey = $_REQUEST['ReturnMsgKey'];

$linterface = new interface_html();
$lreportcard_ui = new libreportcard_ui();
$lreportcard_award = new libreportcard_award();

$CurrentPage = "Reports_GenerateReport";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eReportCard['Reports_GenerateReport'], "", 0);

$ReturnMsg = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

# sub-tag information
echo $linterface->Include_JS_CSS();
echo $lreportcard_ui->Get_Generate_Award_SubTag($ReportID, $CurrentTag=2);
echo '<br />'."\n";
echo $lreportcard_ui->Get_Edit_Award_Student_UI($ReportID, $EditMode, $YearClassID, $AwardName, $AwardID, $SubjectID);

?>

<script language="javascript" src="common.js"></script>
<script language="javascript">

$(document).ready( function() {
	js_Reload_Student_Table();
});
	
function js_Go_Back() {
	<? if ($EditMode == 'Class') { ?>
		window.location = 'view_award_class_mode.php?ReportID=<?=$ReportID?>';
	<? } else if ($EditMode == 'Award') { ?>
		window.location = 'view_award_award_mode.php?ReportID=<?=$ReportID?>';
	<? } ?>
}

function js_Import() {
	var jsYearClassID = $('select#YearClassID').val();
	window.location = 'import_award_step1.php?ReportID=<?=$ReportID?>&YearClassID=' + jsYearClassID;
}

function js_Export() {
	<? if ($EditMode == 'Class') { ?>
		var jsYearClassID = $('select#YearClassID').val();
		window.location = 'export_award.php?ReportID=<?=$ReportID?>&ExportMode=Class&YearClassID=' + jsYearClassID;
	<? } else if ($EditMode == 'Award') { ?>
		var jsAwardName = $('select#AwardName').val();
		window.location = 'export_award.php?ReportID=<?=$ReportID?>&ExportMode=Award&AwardName=' + encodeURI(jsAwardName) + '&AwardID=<?=$AwardID?>&SubjectID=<?=$SubjectID?>';
	<? } ?>
}

function js_Reload_Student_Table() {
	
	var jsYearClassID = $('select#YearClassID').val();
	if (jsYearClassID == null) {
		jsYearClassID = '';
	}
	
	var jsAwardName = $('select#AwardName').val();
	if (jsAwardName == null) {
		jsAwardName = '';
	}
	
	Block_Element('StudentTableDiv');
	$('div#StudentTableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{
			Action: 'Edit_Award_Student_Table',
			ReportID: '<?=$ReportID?>',
			EditMode: '<?=$EditMode?>',
			YearClassID: jsYearClassID,
			AwardName: jsAwardName,
			AwardID: '<?=$AwardID?>',
			SubjectID: '<?=$SubjectID?>'
		},
		function(ReturnData)
		{
			initThickBox();
			
			<? if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_ManageGeneratedResultOnly']) { ?>
				js_Init_DND_Table();
			<? } ?>
			
			UnBlock_Element('StudentTableDiv');
		}
	);
}

function js_Save_Student_Award() {
	Block_Document('<?=$linterface->Get_Ajax_Loading_Image($noLang=0, $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['SavingAwardsData'])?>');
	var jsSubmitString = Get_Form_Values(document.getElementById('form1'));
	$.ajax({  
		type: "POST",  
		url: "edit_award_student_update.php",
		data: jsSubmitString,  
		success: function(data) {
			if (data=='1') {
				Get_Return_Message('<?=$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardSaveSuccess']?>');
			}
			else {
				Get_Return_Message('<?=$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardSaveFailed']?>');
			}
			js_Reload_Student_Table();
			Scroll_To_Top();
			UnBlock_Document();
		} 
	});
}

<? if ($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_ManageGeneratedResultOnly']) { ?>
function js_Delete_Award(jsStudentID, jsRecordID) {
	if (confirm('<?=$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['DeleteAward']?>')) {
		Block_Element('StudentAwardDiv_' + jsStudentID);
		$.post(
			"ajax_update.php", 
			{ 
				Action: 'Delete_Generated_Award_Record',
				RecordID: jsRecordID
			},
			function(ReturnData)
			{
				if (ReturnData == '1') {
					Get_Return_Message('<?=$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardDeleteSuccess']?>');
					js_Reload_Student_Table();
				}
				else {
					Get_Return_Message('<?=$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardDeleteFailed']?>');
				}
				
				UnBlock_Element('StudentAwardDiv_' + jsStudentID);
			}
		);
	}
}

function js_Show_Add_Student_Award_Layer(jsStudentID, jsReturnMsg) {
	$.post(
		"ajax_reload.php", 
		{ 
			Action: 'Reload_Add_Student_Award_Layer',
			StudentID: jsStudentID,
			ReportID: '<?=$ReportID?>'
		},
		function(ReturnData)
		{
			$('div#TB_ajaxContent').html(ReturnData);
			js_Reload_Subject_Selection();
			
			if (jsReturnMsg != '' && jsReturnMsg != null) {
				Get_Return_Message(jsReturnMsg);
			}
		}
	);	
}

function js_Reload_Subject_Selection() {
	var jsAwardID = $('select#AwardID').val();
	
	$.post(
		"ajax_validate.php", 
		{ 
			Action: 'Get_Award_AwardType',
			AwardID: jsAwardID
		},
		function(ReturnData)
		{
			if (ReturnData == 'OVERALL') {
				$('div#SubjectSelDiv').html('');
				$('tr#SubjectTr').hide();
			}
			else {
				$('tr#SubjectTr').show();
				$('div#SubjectSelDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
					"ajax_reload.php", 
					{
						Action: 'Reload_Award_Subject_Selection',
						ReportID: '<?=$ReportID?>',
						StudentID: $('input#StudentID').val(),
						AwardID: jsAwardID,
						SelectionID: 'SubjectID'
					},
					function(ReturnData)
					{
						
					}
				);
			}
		}
	);
}

function js_Changed_Award_Selection() {
	js_Reload_Subject_Selection();
}

function js_Add_Student_Award(jsAddMore) {
	var jsAwardID = $('select#AwardID').val();
	var jsSubjectID = $('select#SubjectID').val();
	var jsStudentID = $('input#StudentID').val();
	
	$('div.WarningDiv').hide();
	var jsCanSubmit = true;
	
	if (jsAwardID == '' || jsAwardID == null) {
		$('div#AwardEmptyWarningDiv').show();
		jsCanSubmit = false;
	}
	if ($('tr#SubjectTr').is(':visible') && (jsSubjectID == '' || jsSubjectID == null)) {
		$('div#SubjectEmptyWarningDiv').show();
		jsCanSubmit = false;
	}
	
	if (jsCanSubmit) {
		$.post(
			"ajax_update.php", 
			{ 
				Action: 'Add_Generated_Award_Record',
				StudentID: jsStudentID,
				ReportID: '<?=$ReportID?>',
				AwardID: jsAwardID,
				SubjectID: jsSubjectID
			},
			function(ReturnData)
			{
				var jsReturnMsg;
				if (ReturnData == '1') {
					jsReturnMsg = '<?=$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardAddSuccess']?>';
				}
				else {
					jsReturnMsg = '<?=$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardAddFailed']?>';
				}
				
				if (jsAddMore == 1) {
					js_Show_Add_Student_Award_Layer(jsStudentID, jsReturnMsg);
				}
				else {
					js_Hide_ThickBox();
					Scroll_To_Top();
					Get_Return_Message(jsReturnMsg);
				}
				
				js_Reload_Student_Table();
			}
		);
	}
}

function js_Show_Add_Award_Student_Layer(jsAwardID, jsSubjectID, jsReturnMsg) {
	$.post(
		"ajax_reload.php", 
		{ 
			Action: 'Reload_Add_Award_Student_Layer',
			ReportID: '<?=$ReportID?>',
			AwardID: jsAwardID,
			SubjectID: jsSubjectID
		},
		function(ReturnData)
		{
			$('div#TB_ajaxContent').html(ReturnData);
			
			if (jsReturnMsg != '' && jsReturnMsg != null) {
				Get_Return_Message(jsReturnMsg);
			}
		}
	);	
}

function js_Changed_Class_Selection(jsYearClassID) {
	if (jsYearClassID == '') {
		$('tr#StudentSelTr').hide();
	}
	else {
		$('tr#StudentSelTr').show();
		var excludeStudentList = $('input#AwardedStudentIDList').val();
		
		<? if($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_LimitByReceivedGrade']){ ?>
		var failConductStudentList = $('input#failCountStudentIDList').val();
		if(failConductStudentList!="")
		{
			if(excludeStudentList!="")
				excludeStudentList = excludeStudentList + "," + failConductStudentList;
			else
				excludeStudentList = failConductStudentList;
		}
		<? } ?>
		
		$('div#StudentSelDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
			"../../ajax_reload_selection.php",
			{
				RecordType: 'StudentByClass',
				ClassID: jsYearClassID,
				SelectionID: 'StudentIDArr[]',
				noFirst: 1,
				isMultiple: 1,
				isAll: 0,
				withSelectAll: 1,
				ExcludeStudentIDList: excludeStudentList
			},
			function(ReturnData) {
				
			}
		);
	}
}

function js_Add_Award_Student(jsAddMore) {
	var jsAwardID = $('input#AwardID').val();
	var jsSubjectID = $('input#SubjectID').val();
	var jsYearClassID = $('select#YearClassIDInThickBox').val();
	var jsStudentIDList = $('select#StudentIDArr\\[\\]').val().toString();
	
	$('div.WarningDiv').hide();
	var jsCanSubmit = true;
	
	if (jsYearClassID == '' || jsYearClassID == null) {
		$('div#ClassEmptyWarningDiv').show();
		jsCanSubmit = false;
	}
	
	if (jsStudentIDList == '' || jsStudentIDList == null) {
		$('div#StudentEmptyWarningDiv').show();
		jsCanSubmit = false;
	}
	
	if (jsCanSubmit) {
		$.post(
			"ajax_update.php", 
			{ 
				Action: 'Add_Generated_Award_Record',
				StudentIDList: jsStudentIDList,
				ReportID: '<?=$ReportID?>',
				AwardID: jsAwardID,
				SubjectID: jsSubjectID
			},
			function(ReturnData)
			{
				var jsReturnMsg;
				if (ReturnData == '1') {
					jsReturnMsg = '<?=$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardAddSuccess']?>';
				}
				else {
					jsReturnMsg = '<?=$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardAddFailed']?>';
				}
				
				if (jsAddMore == 1) {
					js_Show_Add_Award_Student_Layer(jsAwardID, jsSubjectID, jsReturnMsg);
				}
				else {
					js_Hide_ThickBox();
					Scroll_To_Top();
					Get_Return_Message(jsReturnMsg);
				}
				
				js_Reload_Student_Table();
			}
		);
	}
}

function js_Init_DND_Table() {
	$(".common_table_list_v30").tableDnD({
		onDrop: function(table, DroppedRow) {
			if(table.id == "AwardStudentTable") {
				Block_Element(table.id);
				
				// Get the order string
				var rowArr = table.tBodies[0].rows;
				var numOfRow = rowArr.length;
				var recordIDArr = new Array();
				for (var i=0; i<numOfRow; i++) {
					if (rowArr[i].id != "" && !isNaN(rowArr[i].id)) {
						recordIDArr[recordIDArr.length] = rowArr[i].id;
					}
				}
				var recordIDStr = recordIDArr.join(',');
				
				// Update DB
				$.post(
					"ajax_update.php", 
					{ 
						Action: "Reorder_AwardRank",
						DisplayOrderString: recordIDStr
					},
					function(ReturnData)
					{
						// Reset the ranking display
						var jsRankingCounter = 0;
						$('span.RankingSpan').each( function () {
							$(this).html(++jsRankingCounter);
						})
						
						// Get system message
						if (ReturnData == '1') {
							jsReturnMsg = '<?=$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardReorderSuccess']?>';
						}
						else {
							jsReturnMsg = '<?=$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardReorderFailed']?>';
						}
						
						UnBlock_Element(table.id);
						Scroll_To_Top();
						Get_Return_Message(jsReturnMsg);
					}
				);
			}
		},
		onDragStart: function(table, DraggedRow) {
			//$('#debugArea').html("Started dragging row "+row.id);
			
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
}
<? } ?>
</script>
<br />
<br />
                        
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>