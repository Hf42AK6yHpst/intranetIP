<?php
$PATH_WRT_ROOT = "../../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
}
include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");
$lreportcard_award = new libreportcard_award();
$lreportcard_award->hasAccessRight();

$ReportID = $_REQUEST['ReportID'];
$AwardTextArr = $_REQUEST['AwardTextArr'];

$DataArr = array();
foreach((array)$AwardTextArr as $thisStudentID => $thisStudentAwardInfoArr) {
	foreach((array)$thisStudentAwardInfoArr as $thisRecordID => $thisAwardText) {
		$thisDataArr = array();
		$thisAwardText = trim($thisAwardText);
		
		if ($thisRecordID == 'new') {
			// insert
			$thisDataArr['StudentID'] = $thisStudentID;
		}
		else {
			// update
			$thisDataArr['RecordID'] = $thisRecordID;
		}
		$thisDataArr['AwardText'] = $thisAwardText;
		
		$DataArr[] = $thisDataArr;
	}
}
$success = $lreportcard_award->Update_Award_Student_Record($ReportID, $DataArr, $UpdateLastModified=1);
intranet_closedb();

echo ($success)? '1' : '0';
?>