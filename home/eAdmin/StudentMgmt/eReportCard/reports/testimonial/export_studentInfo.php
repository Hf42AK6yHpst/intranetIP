<?php
# Editing by

/**************************************************
 * 	Modification log
 * 	20180723 Bill:  [2017-0901-1525-31265]
 * 		Allow View Group to access
 * ***********************************************/

$PageRight = array("ADMIN", "VIEW_GROUP");

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();
$lexport = new libexporttext();

$StudentIDAry = explode(',',$StudentList);

$dateOfIssue = standardizeFormGetValue($_GET['dateOfIssue']);
$dateOfGraduation = standardizeFormGetValue($_GET['dateOfGraduation']);




include_once ($PATH_WRT_ROOT . "includes/libaccountmgmt.php");
$laccount = new libaccountmgmt();
$luserObj = new libuser('', '', $StudentIDAry);

$headerAry = array();
$headerAry[] = 'STUDENT NUMBER ';
$headerAry[] = 'NAME IN ENGLISH';
$headerAry[] = 'CLASS';
$headerAry[] = 'CLASS NUMBER';
$headerAry[] = 'NAME IN CHINESE';
$headerAry[] = 'SEX';
$headerAry[] = 'DATE OF BIRTH ';
$headerAry[] = 'DATE OF ADMISSION';
$headerAry[] = 'DATE OF GRADUATION';
$headerAry[] = 'DATE OF ISSUE ';
$headerAry[] = 'TEACHER’S COMMENT';


foreach ($StudentIDAry as $StudentID) {
    $studentClassHistoryAry[$StudentID] = $lreportcard->Get_Student_From_School_Settings($AcademicYearIDAry = '', $StudentID);
    for($i=0;$i<count( $studentClassHistoryAry[$StudentID] );$i++){
//         $AcademicYearNameAry[$StudentID][] = $studentClassHistoryAry[$StudentID][$i]['AcademicYearNameEn'];     
        $LastAcademicYearID[$StudentID] = $studentClassHistoryAry[$StudentID][$i]['AcademicYearID'];
    }
}

$numOfStudent = count($StudentIDAry);
for ($i = 0; $i < $numOfStudent; $i ++)
{  
    // Get Student data
    $_studentId = $StudentIDAry[$i];
    $luserObj->LoadUserData($_studentId);
    $_studentPersonalData = $laccount->getUserPersonalSettingByID($_studentId);
    
    $_thisStudetnAcademicYearIDAry = $StudentAcademicYearIDAry[$_studentId];
    $_thisStudentYearIDAry = $YearIDAry[$_studentId];
    

    $_studentInfoAry = array();
    $_studentInfoAry["EnglishName"] = str_replace('*', '', $luserObj->EnglishName);
    $_studentInfoAry["ChineseName"] = str_replace('*', '', $luserObj->ChineseName);
    $_studentInfoAry["UserLogin"]        = $luserObj->UserLogin;
    $_studentInfoAry["Gender"]      = $luserObj->Gender;
    $_studentInfoAry["DateOfAdmission"] = convertDateFormat($_studentPersonalData['AdmissionDate']);
    $_studentInfoAry["DateOfBirth"] = convertDateFormat($luserObj->DateOfBirth);
    $_studentInfoAry["DateOfGraduation"] = convertDateFormat($dateOfGraduation);
    $_studentInfoAry["DateOfIssue"] = convertDateFormat($dateOfIssue);
    
    
    $_lreportcard = new libreportcard($LastAcademicYearID[$_studentId]);  
    $TeacherComment = $_lreportcard->getLastAcademicYearTeacherComment($_studentId);
    
    
    $dataAry[$i][] = $luserObj->UserLogin;    
    $dataAry[$i][] = str_replace('*', '', $luserObj->EnglishName);
    $dataAry[$i][] = $luserObj->ClassName;
    $dataAry[$i][] = $luserObj->ClassNumber;
    $dataAry[$i][] = str_replace('*', '', $luserObj->ChineseName);
    $dataAry[$i][] = $luserObj->Gender;
    $dataAry[$i][] = convertDateFormat($luserObj->DateOfBirth);
    $dataAry[$i][] =  convertDateFormat($_studentPersonalData['AdmissionDate']);
    $dataAry[$i][] = convertDateFormat($dateOfGraduation);
    $dataAry[$i][] = convertDateFormat($dateOfIssue);
    $dataAry[$i][] = $TeacherComment[0];
    
}


function convertDateFormat($date)
{
    global $emptySymbol;
    
    $convertedDate = $emptySymbol;
    if (! is_date_empty($date)) {
        $convertedDate = date('d M Y', strtotime($date));
    }
    
    return $convertedDate;
}

$export_text = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);
$filename = "student_info.csv";

intranet_closedb();
$lexport->EXPORT_FILE($filename,$export_text);
?>