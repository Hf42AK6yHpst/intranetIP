<?php
# Editing by

/**************************************************
 * 	Modification log
 *  20190211 Bill
 *      - feedback from client
 * 	20180723 Bill:  [2017-0901-1525-31265]
 * 		Allow View Group to access
 * ***********************************************/

$PageRight = array("ADMIN", "VIEW_GROUP");

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();

$lexport = new libexporttext();
$libenroll = new libclubsenrol();

$StudentIDAry = explode(',',$StudentList);
$dateOfIssue = standardizeFormGetValue($_GET['dateOfIssue']);
$dateOfGraduation = standardizeFormGetValue($_GET['dateOfGraduation']);

function convertDateFormat($date)
{
    global $emptySymbol;
    
    $convertedDate = $emptySymbol;
    if (!is_date_empty($date)) {
        $convertedDate = date('d M Y', strtotime($date));
    }
    
    return $convertedDate;
}

function returnStudentYearAwards($ParUserID, $ParAcademicYearID)
{
    global $eclass_db, $luserObj;
    
    $sql = "SELECT
                AwardName
            FROM
                {$eclass_db}.AWARD_STUDENT
            WHERE
                UserID = '".$ParUserID."' AND
                AcademicYearID = '".$ParAcademicYearID."' ";
    return $luserObj->returnVector($sql);
}

include_once ($PATH_WRT_ROOT . "includes/libaccountmgmt.php");
$laccount = new libaccountmgmt();
$luserObj = new libuser('', '', $StudentIDAry);

foreach ($StudentIDAry as $StudentID) {
    $studentClassHistoryAry[$StudentID] = $lreportcard->Get_Student_From_School_Settings($AcademicYearIDAry = '', $StudentID);
    for($i=0;$i<count( $studentClassHistoryAry[$StudentID] );$i++){
        $StudentAcademicYearIDAry[$StudentID][] = $studentClassHistoryAry[$StudentID][$i]['AcademicYearID'];
        $YearIDAry[$StudentID][] = $studentClassHistoryAry[$StudentID][$i]['YearID'];
        $AcademicYearNameAry[$StudentID][] = $studentClassHistoryAry[$StudentID][$i]['AcademicYearNameEn'];
    }
}

$studentCount = 0;
$numOfStudent = count($StudentIDAry);
for ($i=0; $i<$numOfStudent; $i++)
{
    // Get Student data
    $_studentId = $StudentIDAry[$i];
    $luserObj->LoadUserData($_studentId);
    $_studentPersonalData = $laccount->getUserPersonalSettingByID($_studentId);
    
    $_thisStudetnAcademicYearIDAry = $StudentAcademicYearIDAry[$_studentId];
//     $_thisStudentYearIDAry = $YearIDAry[$_studentId];
    
    $_thisStudentYearIDAry = $YearIDAry[$_studentId];
     // $TeacherComment = $_lreportcard->getLastAcademicYearTeacherComment($_studentId);
    
    for($j=0; $j<count($StudentAcademicYearIDAry[$_studentId]); $j++)
    {
        $yearcount = 0;
        $PositionData = '';
        $ActivityyData = '';
        $AwardData = '';
        $PositionAry = array();
        $TitleAry = array();
        $AwardAry = array();
        
        $YearName = $AcademicYearNameAry[$_studentId][$j];
        $thisAcademicYearID = $StudentAcademicYearIDAry[$_studentId][$j];
        $thisYearID = $_thisStudentYearIDAry = $YearIDAry[$_studentId][$j];
        
        $_lreportcard = new libreportcard($StudentAcademicYearIDAry[$_studentId][$j]); 
        
        ############## Position [START] ################
        $PositionDataAry = $_lreportcard->Get_Student_OtherInfo_Data('eca', $TermID = '', $ClassID = '', $_studentId ,'Responsibilities/Services', $thisYearID);
        if(!empty($PositionDataAry)){
            foreach($PositionDataAry as $StudentPositionDataAry){
                $PositionAry[] = $StudentPositionDataAry['Information'];
            }
        }
        $PositionAry = array_values(array_unique(array_filter($PositionAry)));
        
        $PositionData .= implode(',', $PositionAry);
        ############## Position [END] #######################
        
        ############## Activity {START] ##############    
        $StudentEnrolledClubAry = $libenroll->GET_STUDENT_ENROLLED_CLUB_LIST($_studentId,'',$thisAcademicYearID);
        if(!empty($StudentEnrolledClubAry))
        {
            foreach($StudentEnrolledClubAry as $StudentEnrolledClub){
                $thisStudentEnrolGroupID =  $StudentEnrolledClub[0]; 
                $DisplayTitle = $libenroll->get_Club_DisplayName($thisStudentEnrolGroupID);
                $TitleAry [] = $DisplayTitle == ''?$StudentEnrolledClub[1]:$DisplayTitle;
                // $TitleAry[] = $StudentEnrolledClub[1]; //4 TitleChinese
            }
        }
        
        $StudentEnrolledActivityAry = $libenroll->GET_STUDENT_ENROLLED_EVENT_LIST($_studentId,$thisAcademicYearID);
        if(!empty($StudentEnrolledActivityAry))
        {
            foreach($StudentEnrolledActivityAry as $StudentEnrolledActivity){
                $thisStudentEnrolEventID = $StudentEnrolledActivity[0];
                $DisplayTitle = $libenroll->get_Event_DisplayName($thisStudentEnrolEventID);
                $TitleAry[] = $DisplayTitle==''?$StudentEnrolledActivity['EventTitle']:$DisplayTitle;
                // $TitleAry[] = $StudentEnrolledActivity['EventTitle'];
            }
        }
        $TitleAry = array_values(array_unique(array_filter($TitleAry)));
        
        if(!empty($TitleAry)){
            $ActivityyData .= implode(',', $TitleAry);
        }
        ############## Activity [END] ################
        
        ############## Award {START] ############
        $portfolioDataAry = returnStudentYearAwards($_studentId, $thisAcademicYearID);
        if(!empty($portfolioDataAry))
        {
            foreach((array)$portfolioDataAry as $thisPortfolioData) {
                $AwardAry[] = $thisPortfolioData;
            }
        }
        
        $AwardsDataAry = $_lreportcard->Get_Student_OtherInfo_Data('award', $TermID = '', $ClassID = '', $_studentId ,'Awards',$thisYearID);       
        if(!empty($AwardsDataAry)){
            foreach($AwardsDataAry as $StudentAwardsDataAry){
                $AwardAry[] = $StudentAwardsDataAry['Information'];
            }
        }
        $AwardAry = array_values(array_unique(array_filter($AwardAry)));
        
        $AwardData .= implode(',', $AwardAry);
        ############## Award {END] ################
        
//         $PositionData .="\n";
//         $AwardData .="\n";
//         $ActivityyData .="\n";
       
        ########Year Name ##############
        $dataAry[$studentCount][$yearcount++] = $luserObj->UserLogin;
        $dataAry[$studentCount][$yearcount++] = str_replace('*', '', $luserObj->EnglishName);
        $dataAry[$studentCount][$yearcount++] = $luserObj->ClassName;
        $dataAry[$studentCount][$yearcount++] = $luserObj->ClassNumber;
        $dataAry[$studentCount][$yearcount++] = $YearName;
        $dataAry[$studentCount][$yearcount++] = $PositionData;
        $dataAry[$studentCount][$yearcount++] = $ActivityyData;
        $dataAry[$studentCount][$yearcount++] = $AwardData;
        $studentCount++;
     }
}

$headerAry = array();
$headerAry[] = 'STUDENT NUMBER ';
$headerAry[] = 'NAME IN ENGLISH';
$headerAry[] = 'CLASS';
$headerAry[] = 'CLASS NUMBER';
$headerAry[] = 'YEAR';
$headerAry[] = 'POSITIONS OF RESPONSIBILITY';
$headerAry[] = 'ACTIVITIES AND COMMUNITY SERVICE';
$headerAry[] = 'AWARDS';

$export_text = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);
$filename = "student_Records.csv";

intranet_closedb();
$lexport->EXPORT_FILE($filename,$export_text);
?>