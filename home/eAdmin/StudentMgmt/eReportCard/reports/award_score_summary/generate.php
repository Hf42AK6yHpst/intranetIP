<?
// Using: Bill

/*
 *	2018-03-07 (Bill)    [2017-1102-1135-27054]
 *	     - Create file
 */

$PageRight = "ADMIN";

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");

// Subject Weight of last Report Column is zero > NOT Exam Subject
function checkIsExamSubject($ReportID, $SubjectID)
{
    global $lreportcard;
    
    ### Get Report Column
    $ReportColumnInfoArr = $lreportcard->returnReportTemplateColumnData($ReportID, $other='');
    $numOfReportColumn = count($ReportColumnInfoArr);
    $lastReportColumnID = $ReportColumnInfoArr[$numOfReportColumn-1]['ReportColumnID'];
    
    ### Get Subject Weight Info
    $AllSubjectWeightData = $lreportcard->returnReportTemplateSubjectWeightData($ReportID);
    $SubjectWeightAssoArr = BuildMultiKeyAssoc($AllSubjectWeightData, array('ReportColumnID', 'SubjectID'));
    
    $LastColumnWeight = $SubjectWeightAssoArr[$lastReportColumnID][$SubjectID]['Weight'];
    if ($LastColumnWeight == '' || $LastColumnWeight == 0) {
        $IsExamSubject = false;
    }
    else {
        $IsExamSubject = true;
    }
    
    return $IsExamSubject;
}

$lreportcard = new libreportcard_award();
$lreportcard->hasAccessRight();

### POST data
$ReportID = $_POST['ReportID'];
$YearClassIDArr = $_POST['YearClassIDArr'];

### Get Report Info
$ReportInfoArr = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$ClassLevelID = $ReportInfoArr['ClassLevelID'];

### Get Report Column
$ReportColumnInfoArr = $lreportcard->returnReportTemplateColumnData($ReportID);
$numOfReportColumn = count($ReportColumnInfoArr);
$lastReportColumnID = $ReportColumnInfoArr[$numOfReportColumn-1]['ReportColumnID'];

### Get Report Subject
$SubjectArr = $lreportcard->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=0, $ReportID);

### Get Subject Type
$subjectTypeArr = array();
foreach((array)$SubjectArr as $thisSubjectId => $thisSubjectName)
{
    if(!$lreportcard->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($thisSubjectId))
    {
        $subjectTypeArr[$thisSubjectId] = checkIsExamSubject($ReportID, $thisSubjectId) ? 'ES' : 'NES';
    }
}

### Get selected Student
$isShowStyle = ($ViewFormat == 'html') ? true : false;
$StudentInfoArr = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $YearClassIDArr, $ParStudentID="", $ReturnAsso=0, $isShowStyle);
$StudentIDArr = Get_Array_By_Key($StudentInfoArr, 'UserID');
$numOfStudent = count($StudentInfoArr);

### Get Award Info
$AwardInfoArr = $lreportcard->Get_Award_Info();

### Get Student Award Info      // $StudentAwardInfoArr[$StudentID][$AwardID][$SubjectID][Field] = Data
$AwardIDArr = array($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_Config']['AwardArr']['excellent']['AwardID'], $eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_Config']['AwardArr']['good']['AwardID']);
$StudentAwardInfoArr = $lreportcard->Get_Award_Generated_Student_Record($ReportID, $StudentIDArr, $ReturnAsso=1, $AwardNameWithSubject=1, $AwardIDArr);

### Get Student Marksheet Score
$StudentMarksheetScoreArr = array();
foreach((array)$SubjectArr as $thisSubjectId => $thisSubjectName)
{
    if(!$lreportcard->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($thisSubjectId))
    {
        $StudentMarksheetScoreArr[$thisSubjectId] = $lreportcard->GET_MARKSHEET_SCORE($StudentIDArr, $thisSubjectId, $lastReportColumnID, $ConvertToNaIfZeroWeight = 0, $ColumnWeight = '', $SkipSubjectGroupCheckingForGetMarkSheet = 0);
    }
}

### CSV Header
$HeaderArr = array();
$HeaderArr[] = $Lang['SysMgr']['FormClassMapping']['Class'];
$HeaderArr[] = $Lang['SysMgr']['FormClassMapping']['ClassNo'];
$HeaderArr[] = $Lang['SysMgr']['FormClassMapping']['StudentName'];
$HeaderArr[] = $Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardName'];
$HeaderArr[] = $Lang['SysMgr']['SubjectClassMapping']['Subject'];
$HeaderArr[] = $eReportCard['Mark'];
$HeaderArr[] = $eReportCard['TotalMark'];
$HeaderArr[] = $eReportCard['AwardMarkSummary']['EstimatedMark'];
$numOfHeader = count($HeaderArr);

### Build Student data
// loop students
$InfoArr = array();
$InfoCounter = 0;
for ($i=0; $i<$numOfStudent; $i++)
{
    $thisTotalAwardScore = 0;
    $thisStartIndex = $InfoCounter;
    
    $thisStudentID = $StudentInfoArr[$i]['UserID'];
    $thisStudentName = $StudentInfoArr[$i]['StudentName'];
	$thisClassName = Get_Lang_Selection($StudentInfoArr[$i]['ClassTitleCh'], $StudentInfoArr[$i]['ClassTitleEn']);
	$thisClassNumber = $StudentInfoArr[$i]['ClassNumber'];
	
	// loop awards
	foreach ((array)$StudentAwardInfoArr[$thisStudentID] as $thisAwardID => $thisAwardInfoArr)
	{
		$thisAwardName = Get_Lang_Selection($AwardInfoArr[$thisAwardID]['AwardNameCh'], $AwardInfoArr[$thisAwardID]['AwardNameEn']);
		$thisAwardCode = $AwardInfoArr[$thisAwardID]['AwardCode'];
		
		// loop subjects
		foreach ((array)$thisAwardInfoArr as $thisSubjectID => $thisSubjectAwardInfoArr)
		{
		    // Component Subject > SKIP
			if ($thisSubjectID == 0)
			{
				continue;
			}
			
			// Subject Name
			$thisSubjectName = $lreportcard->GET_SUBJECT_NAME_LANG($thisSubjectID);
			
			// Subject Type (ES - Exam Subject)
			$isExamSubject = $subjectTypeArr[$thisSubjectID] == "ES";
			
			// Get Subject Award Score
            $thisAwardScore = "";
			if($thisAwardID == $eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_Config']['AwardArr']['excellent']['AwardID'])
			{
			    $thisAwardScore = $isExamSubject ? 6 : 3;
			}
			else if($eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_Config']['AwardArr']['good']['AwardID'])
			{
			    $thisAwardScore = $isExamSubject ? 3 : 1;
			}
			$thisTotalAwardScore += $thisAwardScore;
			
			// Get Estimate Award Score
			$thisEstimateScore = "";
			$thisMarksheetScore = $StudentMarksheetScoreArr[$thisSubjectID][$thisStudentID];
			if(isset($thisMarksheetScore) && $thisMarksheetScore['IsEstimated'] == '1')
			{
			    $thisEstimateScore = $thisAwardScore;
			}
			
			$InfoArr[$InfoCounter]['ClassName'] = $thisClassName;
			$InfoArr[$InfoCounter]['ClassNumber'] = $thisClassNumber;
			$InfoArr[$InfoCounter]['StudentName'] = $thisStudentName;
			$InfoArr[$InfoCounter]['AwardName'] = $thisAwardName;
			$InfoArr[$InfoCounter]['SubjectName'] = $thisSubjectName;
			$InfoArr[$InfoCounter]['AwardScore'] = $thisAwardScore;
			$InfoArr[$InfoCounter]['TotalAwardScore'] = '';
			$InfoArr[$InfoCounter]['EstimateScore'] = $thisEstimateScore;
			$InfoCounter++;
		}
	}
	
	// Student Total Award Score
	if($thisTotalAwardScore > 0)
	{
	    $InfoArr[$thisStartIndex]['TotalAwardScore'] = $thisTotalAwardScore;
	}
}
$numOfInfo = count($InfoArr);

// if ($ViewFormat=='html') {
// 	$x = '';
// 	$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">'."\r\n";
// 		$x .= '<tr>'."\r\n";
// 			$x .= '<td align="center"><h1>'.$ReportTitle.'</h1></td>'."\r\n";
// 		$x .= '</tr>'."\r\n";
// 		$x .= '<tr>'."\r\n";
// 			$x .= '<td>'."\r\n";
// 				$x .= "<table id='ResultTable' class='GrandMSContentTable' width='100%' border='1' cellpadding='2' cellspacing='0'>\n";
// 					$x .= "<col width='2%' />\n";
// 					$x .= "<col width='8%' />\n";
// 					$x .= "<col width='8%' />\n";
// 					$x .= "<col width='20%' />\n";
// 					$x .= "<col width='15%' />\n";
// 					$x .= "<col width='25%' />\n";
// 					$x .= "<col width='8%' />\n";
// 					$x .= "<col width='14%' />\n";
					
// 					$x .= "<thead>\n";
// 						$x .= "<tr>\n";
// 							for ($i=0; $i<$numOfHeader; $i++) {
// 								$thisHeader = $HeaderArr[$i];
// 								$x .= "<th>".$thisHeader."</th>\n";
// 							}
// 						$x .= "</tr>\n";
// 					$x .= "</thead>\n";
					
// 					$x .= "<tbody>\n";
// 						if ($numOfInfo == 0) {
// 							$x .= "<tr><td colspan='100%' style='text-align:center;'>".$Lang['General']['NoRecordAtThisMoment']."</td></tr>\n";
// 						}
// 						else {
// 							for ($i=0; $i<$numOfInfo; $i++) {
// 								$thisClassName = $InfoArr[$i]['ClassName'];
// 								$thisClassNumber = $InfoArr[$i]['ClassNumber'];
// 								$thisStudentName = $InfoArr[$i]['StudentName'];
// 								$thisAwardName = $InfoArr[$i]['AwardName'];
// 								$thisSubjectName = $InfoArr[$i]['SubjectName'];
// 								$thisAwardRank = $InfoArr[$i]['AwardRank'];
// 								$thisNumOfEstimatedScore = $InfoArr[$i]['NumOfEstimatedScore'];
								
// 								$x .= "<tr>\n";
// 									$x .= "<td style='text-align:center;'>".($i + 1)."</td>\n";
// 									$x .= "<td style='text-align:center;'>".$thisClassName."</td>\n";
// 									$x .= "<td style='text-align:center;'>".$thisClassNumber."</td>\n";
// 									$x .= "<td>".$thisStudentName."</td>\n";
// 									$x .= "<td>".$thisAwardName."</td>\n";
// 									$x .= "<td>".$thisSubjectName."</td>\n";
// 									$x .= "<td style='text-align:center;'>".$thisAwardRank."</td>\n";
// 									$x .= "<td style='text-align:center;'>".$thisNumOfEstimatedScore."</td>\n";
// 								$x .= "</tr>\n";
// 							}
// 						}
// 					$x .= "</tbody>\n";
// 				$x .= "</table>\n";
// 			$x .= '</td>'."\r\n";
// 		$x .= '</tr>'."\r\n";
// 	$x .= '</table>';
	
// 	echo $lreportcard->Get_GrandMS_CSS();
// 	echo $lreportcard->Get_Report_Header($ReportTitle);
// 	echo $x;
// 	echo $lreportcard->Get_Report_Footer();
	
// 	intranet_closedb();
// }
// else if ($ViewFormat=='csv')
{
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	$lexport = new libexporttext();
	
	# Build Export Content
	$ExportContentArr = array();
	for ($i=0; $i<$numOfInfo; $i++)
	{
		$thisClassName = $InfoArr[$i]['ClassName'];
		$thisClassNumber = $InfoArr[$i]['ClassNumber'];
		$thisStudentName = $InfoArr[$i]['StudentName'];
		$thisAwardName = $InfoArr[$i]['AwardName'];
		$thisSubjectName = $InfoArr[$i]['SubjectName'];
		$thisAwardScore = $InfoArr[$i]['AwardScore'];
		$thisTotalAwardScore = $InfoArr[$i]['TotalAwardScore'];
		$thisEstimateScore = $InfoArr[$i]['EstimateScore'];
		
		$ExportContentArr[$i][] = $thisClassName;
		$ExportContentArr[$i][] = $thisClassNumber;
		$ExportContentArr[$i][] = $thisStudentName;
		$ExportContentArr[$i][] = $thisAwardName;
		$ExportContentArr[$i][] = $thisSubjectName;
		$ExportContentArr[$i][] = $thisAwardScore;
		$ExportContentArr[$i][] = $thisTotalAwardScore;
		$ExportContentArr[$i][] = $thisEstimateScore;
	}
	
	$export_content = $lexport->GET_EXPORT_TXT($ExportContentArr, $HeaderArr);
	intranet_closedb();
	
	// Output file
	$lexport->EXPORT_FILE('AwardScoreSummary.csv', $export_content);
}
?>