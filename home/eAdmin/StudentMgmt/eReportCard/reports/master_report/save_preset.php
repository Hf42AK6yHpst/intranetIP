<?php
// Using :

################## Change Log [Start] ##############
#
#   Date:   2019-06-26  (Bill)      [2019-0626-0947-03207]
#           Handle $PresetID converted to integer problem
#
################## Change Log [End] ##############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

intranet_auth();
intranet_opendb();

if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
}
else {
	$lreportcard = new libreportcard();
}

foreach((array)$_POST as $name => $value)
{
	if(in_array($name, array("ReportID","ClassLevelID","PresetID","PresetName")))
	{
		continue;
	}
	else if(in_array($name, array("ColumnOrder","YearClassIDArr","ReportColumnID","ReportTitle")))
	{
		continue;
	}
	
	if(is_array($value))
	{
		$js_field_name = $name."[]";
		foreach($value as $eachVal)
		{
			$ValueArr[$js_field_name][] = urldecode($eachVal);
		}
	}
	else
	{
		$js_field_name = $name;
		$ValueArr[$js_field_name][] = urldecode($value);
	}
}
$ValueStr = serialize($ValueArr);

// if($PresetID == 'new')
$PresetID = IntegerSafe($PresetID);
if($PresetID == '0')
{
	$result["InsertPresetValue"] = $lreportcard->Insert_Master_Report_Preset($PresetName,$ValueStr);
	$PresetID = mysql_insert_id();
}
else
{
    $result["UpdatePresetValue"] = $lreportcard->Update_Master_Report_Preset($PresetID, $ValueStr);
}

header("location: index.php?PresetID=$PresetID");
?>