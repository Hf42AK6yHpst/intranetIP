<?php
#  Editing by Bill

@SET_TIME_LIMIT(21600);
@ini_set(memory_limit, -1);

/****************************************************
 * Modification log
 * 20180628 Bill:  [2017-1204-1601-38164]
 *      set target active year  ($eRCTemplateSetting['Report']['SupportAllActiveYear'])
 * 20171129 Bill:	[2017-1127-1218-28164]
 * 		added OMST column for Position in Stream Option		($eRCTemplateSetting['MasterReport']['AlwaysApplyOrderMeritStream'])
 * 20171127 Bill:	[2017-1127-1218-28164]
 * 		Always display stream order in Subject OMF column	($eRCTemplateSetting['MasterReport']['AlwaysApplyOrderMeritStream'])	[removed]
 * 20170801 Bill:	[2016-1214-1548-50240]
 * 		Support Grand Average Grade Adjustment	($eRCTemplateSetting['GrandAverageGradeManualAdjustment'])
 * 20170525 Bill:	[2017-0109-1818-40164]
 * 		improved: support escola pui ching conduct grade display
 * 20170111 Ivan:	[2017-0111-1242-59235]
 * 		fixed: cannot display fail style due to $ScaleDisplay="M" instead of $ScaleDisplay=="M" in some if clauses
 * 20160722 Bill:	[2016-0406-1633-32096]
 * 		fixed: always display fail style when scale display is mark ($eRCTemplateSetting['MasterReport']['ApplyFailStyleToMarkDisplay'] = true)
 * 20160520 Bill:	[2016-0406-1633-32096]
 * 		added Unit to Grand Mark Options, for Sao Jose (Macau) Master Report Cust
 * 20160128 Bill:
 * 		include line break in CSV
 * 20150512 Bill:
 * 		exclude records of dropped students for CUST // [2014-1218-1621-37164] - 香港華人基督教聯會真道書院 - Customization Job for Yearly Report Card
 * 20150130 Bill:
 * 		add column MarkSheet Score
 * 20140801 Ryan  :
 * 		Merged SIS CUST
 * 20120308 Marcus:
 *  	improved to display archived student name with star // 2011-1111-1552-56042 - 聖若瑟教區中學(第二、三校) Master Report
 * 20120208 Marcus:
 *		modified cater Overall Personal Characteristic  
 * 20111114 Marcus:
 * 		Cater display Personal Characteristic, Class Teacher Comment in Master Report.  
 * 20111012 Marcus:
 * 		Cater show coresponding subject only for subject group  
 * **************************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "")  {
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
}
else {
	$lreportcard = new libreportcard();
}

if (!$lreportcard->hasAccessRight())
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

// [2017-1204-1601-38164] Set target active year
if($eRCTemplateSetting['Report']['SupportAllActiveYear'])
{
    $targetActiveYear = stripslashes($_REQUEST['targetActiveYear']);
    if(!empty($targetActiveYear)){
        $targetYearID = $lreportcard->SET_TEMP_ACTIVE_YEAR($targetActiveYear);
    }
}

//$SubjectTeacherCommentKey = array_search('SubjectTeacherComment', (array)$_POST['SubjectData']);
//$ShowSubjectTeacherComment = false;
//if ($SubjectTeacherCommentKey !== false) {
//	// Selected to display Subject Teacher Comment
//	// Unset the related option and use another flag to store the settings so that the orignial looping logic will not be affected
//	
//	unset($_POST['SubjectData'][$SubjectTeacherCommentKey]);
//	$ShowSubjectTeacherComment = true;
//	$_POST["ReportColumnID"][]="1";
//}

$MasterReportSetting = $_POST;

$ViewFormat = $MasterReportSetting['ViewFormat']==0?"html":"csv";
$ApplyStyleToFailOnly = ($_POST['ApplyStyleToFailOnly']==1)? true : false;
$ExcludeSubjectComponentFieldAry = $_POST['ExcludeSubjectComponentFieldAry'];

$lreportcard_ui = new libreportcard_ui();

// [2017-1204-1601-38164] Set target active year
if($eRCTemplateSetting['Report']['SupportAllActiveYear'] && !empty($targetActiveYear) && $targetYearID) {
    $lreportcard_ui = new libreportcard_ui($targetYearID);
}

###### Build Array for display 

# Get Report Type
$ReportSetting = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$SemID = $ReportSetting["Semester"];
$ReportType = ($SemID == "F")? "W" : "T";

# Get Student List
$StudentList = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID);
$StudentIDList = Get_Array_By_Key($StudentList, "UserID");

# Build Assoc Array $FormStudentList["UserID"] = $StudentInfo
$FormStudentList = BuildMultiKeyAssoc((array)$StudentList, array("UserID"));

# Get Marks
# SIS MasterReport
if($ReportCardCustomSchoolName == 'sis'){
	$lreportcard2008 = new libreportcard();
	$MarksAry = $lreportcard2008->getMarks($ReportID);
	$GrandMarksAry = $lreportcard2008->getReportResultScore($ReportID);
}
else{
	$MarksAry = $lreportcard->getMarks($ReportID);
	$GrandMarksAry = $lreportcard->getReportResultScore($ReportID);
}

# Convert Marks to Grade
if(in_array("Grade", $MasterReportSetting['SubjectData'])) {
	$MarksAry = $lreportcard->Convert_MarksArr_Mark_To_Grade($ReportID,$MarksAry);
}

// [2014-1218-1621-37164] Build Report ColumnID and SemesterID mapping
$ReportColumnTermMap = "";
if($eRCTemplateSetting['Report']['MasterReport']['ExcludeDroppedStudent']){
	$ReportInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
	$Semester = $ReportInfo['Semester'];
	
	if($Semester=='F'){
		// get last semester id of consolidate report
		if($eRCTemplateSetting['Report']['MasterReport']['ExcludeDroppedStudent']){
			$lastTermID = $lreportcard->Get_Last_Semester_Of_Report($ReportID);
		}
		
		$ReportColumnTermMap = BuildMultiKeyAssoc((array)$lreportcard->returnReportTemplateColumnData($ReportID), array('ReportColumnID'));
		// use last term column info as overall column info
		if(count($ReportColumnTermMap) > 0){
			$ReportColumnTermMap[0] = end($ReportColumnTermMap);
		}
	}
	else {
		$ReportColumnTermMap = array();
		foreach($ReportColumnID as $columnID){
			$ReportColumnTermMap[$columnID]['SemesterNum'] = $Semester;
		}
	}
}

# Get ReportColumn Subject StudentNo Array
// [2014-1218-1621-37164] Add parameter
$FormStudentNoAry = $lreportcard->Get_ReportColumn_Subject_Student_Number($ReportID, $ReportColumnTermMap);

# Get Marks Nature
$MarksNatureArr = $lreportcard->Get_Student_Subject_Mark_Nature_Arr($ReportID, $ClassLevelID, $MarksAry);
$GrandMarksNatureArr = $lreportcard->Get_Student_Grand_Mark_Nature_Arr($ReportID, $ClassLevelID, $GrandMarksAry);

// Added: 2015-01-30 - Get MarkSheet Score
if(in_array("MSScore", $MasterReportSetting['SubjectData'])) {
	$MSScoreAry = $lreportcard->GET_MARKSHEET_INPUT_SCORE($ReportID, $ReportColumnID, $StudentIDList, $SubjectIDArr);
}

// [2016-0406-1633-32096] change location in coding
# get Subject Weight
$SubjectWeightRawArr = $lreportcard->returnReportTemplateSubjectWeightData($ReportID);
foreach($SubjectWeightRawArr as $SubjectWeightRec)
{
	$WeightColumnID = $SubjectWeightRec["ReportColumnID"] == ''?0: $SubjectWeightRec["ReportColumnID"];
	$SubjectWeightArr[$SubjectWeightRec["SubjectID"]][$WeightColumnID] = $SubjectWeightRec;
}

# Get Other Info Data
$OtherInfoDataAry = $lreportcard->getReportOtherInfoData($ReportID);

# Get Position Determine Field
$RankDetermineField = $lreportcard->Get_Grand_Position_Determine_Field();

# Get Subject Group Student List 
if($SelectFrom == "subjectgroup")
{
	include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
	$scm = new subject_class_mapping();
	$SubjectGroupStudentList = $scm->Get_Subject_Group_Student_List($SubjectGroupIDArr);
	$SubjectGroupStudentAssoc = BuildMultiKeyAssoc($SubjectGroupStudentList, "SubjectGroupID","UserID",1,1);
	$SubjectGroupUniqueStudentList = array_unique(Get_Array_By_Key($SubjectGroupStudentList, "UserID"));
	unset($SubjectGroupStudentList);
	
	if($ShowCorrespondingSubjectOnly==1)
	{
		$subject = new subject();
		$SubjectGroupArr = $subject->Get_Subject_Group_List('','',$SubjectGroupIDArr);
		$ParentSubjectIDArr = array_values(array_unique(Get_Array_By_Key($SubjectGroupArr, "RecordID")));
		$SubjectComponentAssoc = $lreportcard->GET_COMPONENT_SUBJECT($ParentSubjectIDArr, $ClassLevelID, '', '', '', 1);

		foreach($ParentSubjectIDArr as $thisParentSubjID)
		{
			$SubjectIDArr[]= $thisParentSubjID;
			$SubjectCompArr[$thisParentSubjID][] = $thisParentSubjID;
			foreach((array)$SubjectComponentAssoc[$thisParentSubjID] as $thisSubjectCompInfo)
			{
				$SubjectIDArr[] = $thisSubjectCompInfo['SubjectID'];
				$SubjectCompArr[$thisParentSubjID][] = $thisSubjectCompInfo['SubjectID'];
			}
		}
	
		foreach($SubjectGroupArr as $SubjectGroupInfo) // map SubjectGroup and its Subject and SubjComp
		{
			$SubjectGroupSubjectArr[$SubjectGroupInfo['SubjectGroupID']] = $SubjectCompArr[$SubjectGroupInfo['RecordID']];
		}
		
		$MasterReportSetting['SubjectIDArr'] = $SubjectIDArr; // all involved subject 
		$MasterReportSetting['SubjectGroupSubjectArr'] = $SubjectGroupSubjectArr; // for Subject Gruop > Class Summary to get corresponding subject and SubjComp 
	}
}

# Get Subject Teacher Comment
if(in_array("SubjectTeacherComment",(array)$SubjectOtherData) || in_array("ClassTeacherComment",(array)$GrandMarkDataAry))
{
	# Get Subject Teacher Comment
	$CommentSubjectIDArr = array();
	if(in_array("SubjectTeacherComment",(array)$SubjectOtherData)) //Display Subject Teacher Comment
		$CommentSubjectIDArr = array_merge((array)$CommentSubjectIDArr, (array)$SubjectIDArr);
	if(in_array("ClassTeacherComment",(array)$GrandMarkDataAry)) //Display Class Teacher Comment
		$CommentSubjectIDArr = array_merge((array)$CommentSubjectIDArr, array(0));
	$subjectTeacherCommentArr = $lreportcard->returnSubjectTeacherCommentByBatch($ReportID, $StudentIDList, $CommentSubjectIDArr);
}

# Get Personal Characteristic
if(in_array("PersonalCharacteristics",(array)$SubjectOtherData) || in_array("PersonalCharacteristics",(array)$GrandMarkDataAry) )
{
	$PCDataArr = $lreportcard->returnPersonalCharSettingData($ClassLevelID);
	$PCDataArr = BuildMultiKeyAssoc($PCDataArr, "SubjectID", '',0,1);

	# Get Students' Personal Characteristic
	$PCSubjectArr = array();
	if(in_array("PersonalCharacteristics",(array)$GrandMarkDataAry)) 
		$PCSubjectArr = array_merge($PCSubjectArr, (array)0);
	if(in_array("PersonalCharacteristics",(array)$SubjectOtherData)) 
		$PCSubjectArr = array_merge($PCSubjectArr, (array)$SubjectIDArr);
	
	$PCArr = $lreportcard->getPersonalCharacteristicsProcessedDataByBatch('',$ReportID,$PCSubjectArr,0,1);
}

# Get Effort
if (in_array("Effort",(array)$SubjectOtherData)) {
	//$EffortAssoAry[$thisStudentID][$thisSubjectID]["Info"] = data
	$EffortAssoAry = $lreportcard->GET_EXTRA_SUBJECT_INFO($StudentIDList, '', $ReportID);
}

# Get Other Info Data Source
$latestTerm = "";
if($DataSource=="LatestTerm") // 
{
	$sems = $lreportcard->returnReportInvolvedSem($ReportID);
	foreach($sems as $TermID=>$TermName)
		$latestTerm = $TermID;
}
else
{
//	$ReportSetting = $lreportcard->returnReportTemplateBasicInfo($ReportID);
//	$SemID = $ReportSetting['Semester'];
//	$ReportType = ($SemID == 'F')? 'W' : 'T';
	
	if ($ReportType == 'W') {
		$latestTerm = 0;
	}
	else {
		$latestTerm = $SemID;
	}
}

# [2017-0109-1818-40164] Get Conduct Grade (Escola Pui Ching)
if($eRCTemplateSetting['ClassTeacherComment_Conduct'] && $eRCTemplateSetting['ClassTeacherComment_Conduct_PuiChingGradeLimit'] && in_array("Conduct", (array)$OtherInfoData))
{
    include_once($PATH_WRT_ROOT."includes/libreportcard2008_extrainfo.php");
    $lreportcard_extrainfo = new libreportcard_extrainfo();
    
    // [2017-1204-1601-38164] Set target active year
    if($eRCTemplateSetting['Report']['SupportAllActiveYear'] && !empty($targetActiveYear) && $targetYearID) {
        $lreportcard_extrainfo->SET_TEMP_ACTIVE_YEAR($targetActiveYear);
    }
	
	# Get Conduct List
	$ConductArr = $lreportcard_extrainfo->Get_Conduct();
	$ConductArr = BuildMultiKeyAssoc((array)$ConductArr, "ConductID", "Conduct", 1);
	
	# Get Student Conduct List
	$StudentConductIDArr = array();
	if ($ReportType == "T") {
		$StudentExtraInfo = $lreportcard_extrainfo->Get_Student_Extra_Info($ReportID, $StudentIDList);
		$StudentConductIDArr[0] = BuildMultiKeyAssoc((array)$StudentExtraInfo, "StudentID", "ConductID", 1);
	}
	else {
		// Get Term Report
		$TermReportList = $lreportcard->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
		$TermReportList = Get_Array_By_Key((array)$TermReportList, "ReportID");
		$TermReportList = array_unique($TermReportList);
		$TermReportList = array_values($TermReportList);
		
		for($tr_count=0; $tr_count<count((array)$TermReportList); $tr_count++) {
			$StudentExtraInfo = $lreportcard_extrainfo->Get_Student_Extra_Info($TermReportList[$tr_count], $StudentIDList);
			$StudentConductIDArr[$tr_count] = BuildMultiKeyAssoc($StudentExtraInfo, "StudentID", "ConductID", 1);
		}
	}
}

# Get Class Name Lang
$ClassNameLang = Get_Lang_Selection("ClassTitleCh","ClassTitleEn");

# Get UserID Sorting
switch($SortBy)
{
	case 0: $SortedUserID = $lreportcard->Sort_UserID_By_Class_ClassNumber($StudentList); break;
	case 1: $SortedUserID = $lreportcard->Sort_UserID_By_Position("Class",$StudentList,$MarksAry,$GrandMarksAry,$SortSubjectIDClass); break;
	case 2: $SortedUserID = $lreportcard->Sort_UserID_By_Position("Form",$StudentList,$MarksAry,$GrandMarksAry,$SortSubjectIDForm); break;
	default: ;
} 

if($DisplayFormat=="ClassSummary")
{
	if($SelectFrom == "class")
	{
		$StudentGroupingIDArr = $YearClassIDArr; // group by Class
	}
	else
	{
		$StudentGroupingIDArr = $SubjectGroupIDArr; // group by Subject Group
	}
}
else
	$StudentGroupingIDArr = array($ClassLevelID); // Group By form

# loop by Class / Subject Gruop / Class Level
foreach($StudentGroupingIDArr as $StudentGroupingID)
{
	#loop sorted form stuednt list
	foreach((array)$SortedUserID as $thisStudentID) 
	{
		
		# Group by Class for ClassSummary, Group by Form for FormSummary
		$thisClassID = $FormStudentList[$thisStudentID]["YearClassID"];
		
		if($SelectFrom == "class") // Class
		{
			if($DisplayFormat=="ClassSummary") //Show classes in separate tables 
			{
				if($thisClassID != $StudentGroupingID) continue; //skip student if not in class
			}
			else  //Show classes in one tables 
			{
				if(!in_array($thisClassID, $YearClassIDArr)) continue; //skip student if his class is not in form
			}	
		}
		else // Subject Group
		{
			if($DisplayFormat=="ClassSummary") //Show Subject Group in separate tables 
			{
				if(!in_array($thisStudentID,(array)$SubjectGroupStudentAssoc[$StudentGroupingID])) continue; //skip student if not in subject gruop
			}
			else  //Show Subject Group in one tables 
			{

				if(!in_array($thisStudentID, $SubjectGroupUniqueStudentList)) continue; //skip student if not in class

			}
		}
		
	
		###############	Student Info Data Start ################# 
			
			# loop selected Student Info Data Display only
			
			foreach((array) $StudentData as $DisplayStudentData)
			{
				switch($DisplayStudentData)
				{
					case "ClassName";
						$StudentInfoData[$DisplayStudentData] =  $FormStudentList[$thisStudentID][$ClassNameLang];
					break;
					case "ChineseName";
						$StudentInfoData[$DisplayStudentData] =  $FormStudentList[$thisStudentID]["StudentNameCh"];
					break;
					case "EnglishName";
						$StudentInfoData[$DisplayStudentData] =  $FormStudentList[$thisStudentID]["StudentNameEn"];
					break;
					case "RegNo";
						$StudentInfoData[$DisplayStudentData] =  str_replace('#', '', $FormStudentList[$thisStudentID]["WebSAMSRegNo"]);
					break;
					default:
						$StudentInfoData[$DisplayStudentData] =  $FormStudentList[$thisStudentID][$DisplayStudentData];	
				}
			}
			
			# Append Student Info Data to Student Display Ary 
			$StudentDisplayAry[$StudentGroupingID][$thisStudentID]["StudentInfoData"] = $StudentInfoData;
		################ Student Info Data End ################## 
	
		###############	Subject Marks Data & Statistic Process Start ################# 
			$StatStudentNatureArr = array(); // clear array , used to store nature for calc no. of subj pass
			$StatStudentNatureWithoutComponentArr = array(); // clear array , used to store nature for calc no. of subj pass
			
			// [2016-0406-1633-32096]
			$StudentSubjectUnit = 0;
			
			# loop Subject
			unset($SubjectMarksData);
			foreach((array)$SubjectIDArr as $thisSubjectID)
			{
				$thisColumnMarksAry = $MarksAry[$thisStudentID][$thisSubjectID];
				$thisSubjectIsComponent = $lreportcard->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($thisSubjectID);
				
				# init isSubjectAllNA - use to hide subject with all N.A.
				if(!isset($isSubjectAllNA[$StudentGroupingID][$thisSubjectID]))
					$isSubjectAllNA[$StudentGroupingID][$thisSubjectID] = true;
				
				# get Grading Scheme
				if(!isset($GradingScheme[$thisSubjectID]))
					$GradingScheme[$thisSubjectID] = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $thisSubjectID, 1, 0, $ReportID);
				
				# loop ReportColumn
				
	//			foreach((array)$thisColumnMarksAry as $thisReportColumnID => $thisMarksAry)
	//			{
	//				if(!in_array($thisReportColumnID,(array)$ReportColumnID)) continue; // skip Column
				foreach((array)$MasterReportSetting["ReportColumnID"] as $thisReportColumnID )
				{
					$thisMarksAry = $thisColumnMarksAry[$thisReportColumnID];
					
					# Check if subject is special case or not, if $EmptySubjectDataForNA was set, display empty symbol
					$EmptySubjectDataForNA = 1; // default true, can uncomment coding in libreportcard2008_ui.php if user want to show special case 
					//$DisplayEmptySubjectData = ($EmptySubjectDataForNA && ($lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1( $thisMarksAry["Grade"]) || $lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2( $thisMarksAry["Grade"])));
					 
					# loop selected Subject Data Display only
					$thisDisplayArr = array();
					$ScaleInput = $GradingScheme[$thisSubjectID]["ScaleInput"];
					$ScaleDisplay = $GradingScheme[$thisSubjectID]["ScaleDisplay"];
					
					// [2014-1218-1621-37164] check if student in subject group
					$reportStudentRecordExclude = false;
					if($eRCTemplateSetting['Report']['MasterReport']['ExcludeDroppedStudent']){
						$parent_subject_id = $thisSubjectID;
						if($thisSubjectIsComponent){
							$parent_subject_id = $lreportcard->GET_PARENT_SUBJECT_ID($thisSubjectID);
						}
						// Overall Column of Consolidate Report
						if($SemID=="F" && $thisReportColumnID=="0"){
							$reportStudentRecordExclude = ($lastTermID && $lastTermID > 0 && $lreportcard->Get_Student_Studying_Subject_Group($lastTermID, $thisStudentID, $parent_subject_id)==null);
						} 
						else {
							$currentTermID = $ReportColumnTermMap[$thisReportColumnID]['SemesterNum'];
							$reportStudentRecordExclude = ($currentTermID && $currentTermID > 0 && $lreportcard->Get_Student_Studying_Subject_Group($currentTermID, $thisStudentID, $parent_subject_id)==null);
						}
					}
					
					foreach((array)$MasterReportSetting['SubjectData'] as $DisplaySubjectData) 
					{
						// [2014-1218-1621-37164] display empty symbol if student not in subject group
						if($eRCTemplateSetting['Report']['MasterReport']['ExcludeDroppedStudent'] && $reportStudentRecordExclude){
							$thisDisplayArr[$DisplaySubjectData] = $EmptySymbol;
							continue;
						}
						
						// Checking for MarkSheet Score column - check $MarkType is special case or not [2014-1014-1144-21164]
						if($DisplaySubjectData == 'MSScore')
						{
							$MSInput = $MSScoreAry[$thisStudentID][$thisSubjectID][$thisReportColumnID];
							$MarkType = $MSInput['MarkType'];
							$DisplayEmptySubjectData = ($EmptySubjectDataForNA && $MarkType=='SC');
						}
						// Checking for other column
						else {
							$thisTargetGrade = $thisMarksAry["Grade"];
							$DisplayEmptySubjectData = ($EmptySubjectDataForNA && ($lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1( $thisMarksAry["Grade"]) || $lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2( $thisMarksAry["Grade"])));
						}
						
						if($DisplayEmptySubjectData)
						{
							$thisDisplayArr[$DisplaySubjectData] = $EmptySymbol;
							continue;
						}
						
						// [2017-1127-1218-28164] Always display stream order in OMF column
//						if($eRCTemplateSetting['MasterReport']['AlwaysApplyOrderMeritStream'] && $DisplaySubjectData=="OrderMeritForm") {
//							$DisplaySubjectData = "OrderMeritStream";
//						}
						
						$ProcessedValue = '';
						$RawValue = $thisMarksAry[$DisplaySubjectData];
						switch($DisplaySubjectData)
						{
							case "Mark":
								if($ScaleInput=='G') {
									$ProcessedValue = $EmptySymbol;
								}
								else if ($ApplyStyleToFailOnly && $ViewFormat!='csv') {
									// [2016-0406-1633-32096] for scale display is mark
									//2017-0111-1242-59235
									//if($ScaleInput=="M" && $ScaleDisplay="M" && is_numeric($thisMarksAry['Mark']) && $thisMarksAry['Mark'] > 0)
									if($ScaleInput=="M" && $ScaleDisplay=="M" && is_numeric($thisMarksAry['Mark']) && $thisMarksAry['Mark'] > 0)
										$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $thisSubjectID, $thisMarksAry['Mark'], '', 'M', $ReportID);
									else
										$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $thisSubjectID, $thisMarksAry['Grade'], '', 'G', $ReportID);
									
									if ($thisNature == 'Fail') {
										$ProcessedValue = $lreportcard->ReturnTextwithStyle($RawValue, '', '', $eRCTemplateSetting['MasterReport']['ApplyFailStyleToMarkDisplayStyle']);
									}
									else {
										$ProcessedValue = $RawValue;
									}
								}
								else if(in_array("Mark",(array)$ShowStyle)  && $ViewFormat!='csv') {
									$ProcessedValue = $lreportcard->Format_Mark_With_Style($ReportID, $thisSubjectID, $thisMarksAry["Mark"], $thisMarksAry["Grade"],$ClassLevelID, '','M','',$thisReportColumnID, $ApplyStyleToFailOnly, $thisStudentID);
								}
								else {
									$ProcessedValue = $RawValue;
								}														
							break;
							
							case "RawMark":
								if($ScaleInput=='G') {
									$RawValue = $thisMarksAry["Grade"];
								}
								else if ($ApplyStyleToFailOnly && $ViewFormat!='csv') {
									// [2016-0406-1633-32096] for scale display is mark
									//2017-0111-1242-59235
									//if($ScaleInput=="M" && $ScaleDisplay="M" && is_numeric($thisMarksAry['Mark']) && $thisMarksAry['Mark'] > 0)
									if($ScaleInput=="M" && $ScaleDisplay=="M" && is_numeric($thisMarksAry['Mark']) && $thisMarksAry['Mark'] > 0)
										$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $thisSubjectID, $thisMarksAry['Mark'], '', 'M', $ReportID);
									else
										$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $thisSubjectID, $thisMarksAry['Grade'], '', 'G', $ReportID);
									
									if ($thisNature == 'Fail') {
										$ProcessedValue = $lreportcard->ReturnTextwithStyle($RawValue, '', '', $eRCTemplateSetting['MasterReport']['ApplyFailStyleToMarkDisplayStyle']);
									}
									else {
										$ProcessedValue = $RawValue;
									}
								}	
								else if(in_array("RawMark",(array)$ShowStyle)  && $ViewFormat!='csv')
									$ProcessedValue = $lreportcard->Format_Mark_With_Style($ReportID, $thisSubjectID, $thisMarksAry["Mark"], $thisMarksAry["Grade"],$ClassLevelID, '','M','',$thisReportColumnID, $ApplyStyleToFailOnly);
								else
									$ProcessedValue = $RawValue;	
							break;
							
							case "Grade":
								if($DisplaySpecialCaseAsEmptySymbol && ($lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1($RawValue) || $lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2($RawValue))) {
									$ProcessedValue = $EmptySymbol;
								}
								else if ($ApplyStyleToFailOnly && $ViewFormat!='csv') {
									// [2016-0406-1633-32096] for scale display is mark
									//2017-0111-1242-59235
									//if($ScaleInput=="M" && $ScaleDisplay="M" && is_numeric($thisMarksAry['Mark']) && $thisMarksAry['Mark'] > 0)
									if($ScaleInput=="M" && $ScaleDisplay=="M" && is_numeric($thisMarksAry['Mark']) && $thisMarksAry['Mark'] > 0)
										$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $thisSubjectID, $thisMarksAry['Mark'], '', 'M', $ReportID);
									else
										$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $thisSubjectID, $RawValue, '', 'G', $ReportID);
									
									if ($thisNature == 'Fail') {
										$ProcessedValue = $lreportcard->ReturnTextwithStyle($RawValue, '', '', $eRCTemplateSetting['MasterReport']['ApplyFailStyleToMarkDisplayStyle']);
									}
									else {
										$ProcessedValue = $RawValue;
									}
								}		
								else if(in_array("Grade",(array)$ShowStyle)  && $ViewFormat!='csv') {
									$ProcessedValue = $lreportcard->Format_Mark_With_Style($ReportID, $thisSubjectID, $thisMarksAry["Grade"], '',$ClassLevelID, '','G','',$thisReportColumnID, $ApplyStyleToFailOnly);
								}
								else {
									$ProcessedValue	= $RawValue;
								}	
							break;
							
							// Added: Display column MarkSheet Score [2014-1014-1144-21164]
							case "MSScore":
								$RawValue = $MSInput['MarkRaw'];
								$ProcessedValue = $RawValue;
								
								// Marks: Style
								if($MarkType == "M"){
									if ($ApplyStyleToFailOnly && $ViewFormat!='csv') {
										// [2016-0406-1633-32096] for scale display is mark
										//2017-0111-1242-59235
										//if($ScaleInput=="M" && $ScaleDisplay="M" && is_numeric($RawValue) && $RawValue > 0)
										if($ScaleInput=="M" && $ScaleDisplay=="M" && is_numeric($RawValue) && $RawValue > 0)
											$MarkNature = $lreportcard->returnMarkNature($ClassLevelID, $thisSubjectID, $RawValue, '', 'M', $ReportID);
										else
											$MarkNature = $lreportcard->returnMarkNature($ClassLevelID, $thisSubjectID, $RawValue, '', 'G', $ReportID);
										
										if ($MarkNature == 'Fail') {
											$ProcessedValue = $lreportcard->ReturnTextwithStyle($RawValue, '', '', $eRCTemplateSetting['MasterReport']['ApplyFailStyleToMarkDisplayStyle']);
										}
									}
									else if(in_array("MSScore",(array)$ShowStyle)  && $ViewFormat!='csv') {
										$ProcessedValue = $lreportcard->Format_Mark_With_Style($ReportID, $thisSubjectID, $RawValue, '', $ClassLevelID, '', 'M', '', $thisReportColumnID, $ApplyStyleToFailOnly, $thisStudentID);
									}
								}
								// Grade: Style
								else if($MarkType=='G'){
									if($DisplaySpecialCaseAsEmptySymbol && $lreportcard->Check_If_Grade_Is_SpecialCase($RawValue)) {
										$ProcessedValue = $EmptySymbol;
									}
									else if ($ApplyStyleToFailOnly && $ViewFormat!='csv') {
										// [2016-0406-1633-32096] for scale display is mark
										//2017-0111-1242-59235
										//if($ScaleInput=="M" && $ScaleDisplay="M" && is_numeric($thisMarksAry['Mark']) && $thisMarksAry['Mark'] > 0)
										if($ScaleInput=="M" && $ScaleDisplay=="M" && is_numeric($thisMarksAry['Mark']) && $thisMarksAry['Mark'] > 0)
											$MarkNature = $lreportcard->returnMarkNature($ClassLevelID, $thisSubjectID, $thisMarksAry['Mark'], '', 'M', $ReportID);
										else
											$MarkNature = $lreportcard->returnMarkNature($ClassLevelID, $thisSubjectID, $RawValue, '', 'G', $ReportID);
											
										if ($MarkNature == 'Fail') {
											$ProcessedValue = $lreportcard->ReturnTextwithStyle($RawValue, '', '', $eRCTemplateSetting['MasterReport']['ApplyFailStyleToMarkDisplayStyle']);
										}
									}		
									else if(in_array("MSScore",(array)$ShowStyle)  && $ViewFormat!='csv') {
										$ProcessedValue = $lreportcard->Format_Mark_With_Style($ReportID, $thisSubjectID, $RawValue, '', $ClassLevelID, '', 'G', '', $thisReportColumnID, $ApplyStyleToFailOnly);
									}
								}
								
							break;
							
							default: 
								if ($ApplyStyleToFailOnly) {
									if ($RawValue == -1 || $RawValue == '') {
										$ProcessedValue = $RawValue;
									}
									else {
										// [2016-0406-1633-32096] for scale display is mark
										//2017-0111-1242-59235					
										//if($ScaleInput=="M" && $ScaleDisplay="M" && is_numeric($thisMarksAry['Mark']) && $thisMarksAry['Mark'] > 0)
										if($ScaleInput=="M" && $ScaleDisplay=="M" && is_numeric($thisMarksAry['Mark']) && $thisMarksAry['Mark'] > 0)
											$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $thisSubjectID, $thisMarksAry['Mark'], '', 'M', $ReportID);
										else
											$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $thisSubjectID, $thisMarksAry['Grade'], '', 'G', $ReportID);
										
										if ($thisNature == 'Fail') {
											$ProcessedValue = $lreportcard->ReturnTextwithStyle($RawValue, '', '', $eRCTemplateSetting['MasterReport']['ApplyFailStyleToMarkDisplayStyle']);
										}
										else {
											$ProcessedValue = $RawValue;
										}
									}
								}
								else {
									$ProcessedValue = $RawValue;
								}
						}
						
						$ProcessedValue = ($ProcessedValue==-1)?$EmptySymbol:$ProcessedValue;
						$thisDisplayArr[$DisplaySubjectData] = $ProcessedValue;
					}	// loop SubjectData End
				
	
					$SubjectMarksData[$thisSubjectID][$thisReportColumnID] = 	$thisDisplayArr;
					
					#### Statistic Process ####
					$isSpecialCase=false;
					$thisMark = $lreportcard->Get_Display_Mark_By_Grading_Scheme($thisMarksAry["Mark"],$thisMarksAry["Grade"],$GradingScheme[$thisSubjectID]);
					if ($lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1($thisMark) || $lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2($thisMark))
						$isSpecialCase = true;
					
					// [2014-1218-1621-37164] add empty records to statistics array if student not in subject group
					if($eRCTemplateSetting['Report']['MasterReport']['ExcludeDroppedStudent'] && $reportStudentRecordExclude){
						$StatMarkArr[$StudentGroupingID][$thisSubjectID][$thisReportColumnID][] = "";
						$StatMarkNatureArr[$StudentGroupingID][$thisSubjectID][$thisReportColumnID][] = "N.A.";
						continue;
					}
					
					$StatMarkArr[$StudentGroupingID][$thisSubjectID][$thisReportColumnID][] = $isSpecialCase?"":$thisMarksAry["Mark"];
					$StatMarkNatureArr[$StudentGroupingID][$thisSubjectID][$thisReportColumnID][] = $isSpecialCase?"N.A.":$MarksNatureArr[$thisStudentID][$thisSubjectID][$thisReportColumnID];
					if(!$isSpecialCase) $isSubjectAllNA[$StudentGroupingID][$thisSubjectID] = false;
					
				}// loop column end
				
				
				
				# get the data of last column
				$StatStudentNatureArr[] = $isSpecialCase?"N.A.":$MarksNatureArr[$thisStudentID][$thisSubjectID][$thisReportColumnID];
				if (!$thisSubjectIsComponent) {
					$StatStudentNatureWithoutComponentArr[] = $isSpecialCase?"N.A.":$MarksNatureArr[$thisStudentID][$thisSubjectID][$thisReportColumnID];
				}
				
				# [2016-0406-1633-32096] Count Fail Subject Unit (Subject Weight) - Main Subject only
				if ($eRCTemplateSetting['MasterReport']['DisplayFailSubjetUnitInReport'] && !$thisSubjectIsComponent && !$isSpecialCase) {
					if($MarksNatureArr[$thisStudentID][$thisSubjectID][0] == "Fail")
					{
						$currentSubjectWeight = $SubjectWeightArr[$thisSubjectID][0]["Weight"];
						$StudentSubjectUnit += $currentSubjectWeight? $currentSubjectWeight : 0;
					}
				}

				################## Subject Other Data Start ##################
				foreach((array)$MasterReportSetting['SubjectOtherData'] as $DisplaySubjectOtherData) 
				{
					unset($ProcessedValue);
					switch($DisplaySubjectOtherData)
					{
						case "SubjectTeacherComment":
							$ProcessedValue = $subjectTeacherCommentArr[$thisSubjectID][$thisStudentID]['Comment'];
//							$ProcessedValue = $ViewFormat=='html'?nl2br($ProcessedValue):$ProcessedValue;
						break;
						
						case "PersonalCharacteristics":
							$thisPCArr = array();
							
							foreach((array)$PCDataArr[$thisSubjectID] as $PCData)
							{
								$thisPCArr[] = $PCArr[$ReportID][$thisSubjectID][$thisStudentID][$PCData['CharID']];
							}
							$ProcessedValue = $thisPCArr ;
						break;
						
						case "Effort":
							$ProcessedValue = $EffortAssoAry[$thisStudentID][$thisSubjectID]["Info"];
						break;
						
					}
					$thisSubjectOtherData[$thisSubjectID][$DisplaySubjectOtherData] = $ProcessedValue;					
				}
				################## Subject Other Data End ##################
				
			}// loop subject end
	
			# Append Subject Marks Data to Student Display Ary 
			$StudentDisplayAry[$StudentGroupingID][$thisStudentID]["SubjectMarksData"] = $SubjectMarksData;
			$StudentDisplayAry[$StudentGroupingID][$thisStudentID]["SubjectOtherData"] = $thisSubjectOtherData;
			
		################ Subject Marks Data & Statistic Process End ################## 
	
		################ Other Info Data Start ##################
			
			# loop selected Other Info Data Display only
			foreach((array)$OtherInfoData as $DisplayOtherInfo)
			{
				$StudentOtherInfoData[$DisplayOtherInfo] = $OtherInfoDataAry[$thisStudentID][$latestTerm][$DisplayOtherInfo];
				
				// [2017-0109-1818-40164] Special Handling for Conduct Grade Display (Escola Pui Ching)
				if($eRCTemplateSetting['ClassTeacherComment_Conduct'] && $eRCTemplateSetting['ClassTeacherComment_Conduct_PuiChingGradeLimit'] && $DisplayOtherInfo == "Conduct")
				{
					# Get Student Conduct
					if ($ReportType == 'T') {
						$StudentConductID = $StudentConductIDArr[0][$thisStudentID];
						$StudentConduct = $ConductArr[$StudentConductID];
					}
					else {
						$ConductDataAry = array();
						for($j=0; $j<count((array)$StudentConductIDArr); $j++) {
							$StudentConductID = $StudentConductIDArr[$j][$thisStudentID];
							$ConductDataAry[$j] = $ConductArr[$StudentConductID];
						}
						$StudentConduct = $lreportcard->calculateOverallConduct($ConductDataAry, $ConductArr);
					}
					$StudentConduct = $StudentConduct? $StudentConduct : $EmptySymbol;
					$StudentOtherInfoData[$DisplayOtherInfo] = $StudentConduct;
				}
			}
			
			# Append Other Info Data to Student Display Ary 
			$StudentDisplayAry[$StudentGroupingID][$thisStudentID]["OtherInfoData"] = $StudentOtherInfoData;
			
		################ Other Info Data End ##################
	
		################ Grand Marks Data Start ##################
			# Get Grand Mark Grading Scheme 
			$GrandMarkScheme = $lreportcard->Get_Grand_Mark_Grading_Scheme($ClassLevelID, $ReportID);
			# loop selected Grand Marks Data Display only
			foreach((array)$GrandMarkDataAry as $DisplayGrandMark)
			{
				$thisGrandMarks = $GrandMarksAry[$thisStudentID][0];
				$thisGrandMarksNature = $GrandMarksNatureArr[$thisStudentID][0];
				$RawValue = $thisGrandMarks[$DisplayGrandMark];
				
				$ProcessedValue='';
				switch($DisplayGrandMark)
				{
					// for Statistic , record GrandMark if selected
					case "GrandTotal":	
					case "GrandAverage":
					case "ActualAverage":
					case "GPA":
					case "GrandSDScore":
						if ($DisplayGrandMark != 'GrandSDScore' && $thisGrandMarks[$DisplayGrandMark] != -1) {
							$StatGrandMarkArr[$StudentGroupingID][$DisplayGrandMark][] = $thisGrandMarks[$DisplayGrandMark];
							$StatGrandMarkNatureArr[$StudentGroupingID][$DisplayGrandMark][] = $thisGrandMarksNature[$DisplayGrandMark];
						}
						
						$GrandMarkID = $lreportcard->Get_Grand_Field_SubjectID($RankDetermineField);
						if ($DisplayGrandMark == 'ActualAverage') {
							$tempDisplayGrandMark = 'GrandAverage';
						}
						else {
							$tempDisplayGrandMark = $DisplayGrandMark;
						}
						
						if ($ApplyStyleToFailOnly && $ViewFormat!='csv') {
							if ($RawValue == -1 || $RawValue == '') {
								$ProcessedValue = $RawValue;
							}
							else {
								$GrandMarkID = $lreportcard->Get_Grand_Field_SubjectID($RankDetermineField);
								$GrandMark = $thisGrandMarks[$RankDetermineField];
								$GrandGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandMarkScheme[$GrandMarkID]['SchemeID'], $GrandMark,$ReportID,$thisStudentID,$GrandMarkID,$ClassLevelID,0);
								
								// [2016-0406-1633-32096] for scale display is mark
								$GradeScaleDisplay = $GrandMarkScheme[$GrandMarkID]['ScaleDisplay'];							
								if($GradeScaleDisplay=="M" && is_numeric($GrandMark) && $GrandMark > 0)
									$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $GrandMarkID, $GrandMark, '', 'M', $ReportID);
								else
									$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $GrandMarkID, $GrandGrade, '', 'G', $ReportID);
								
								if ($thisNature == 'Fail') {
									$ProcessedValue = $lreportcard->ReturnTextwithStyle($RawValue, '', '', $eRCTemplateSetting['MasterReport']['ApplyFailStyleToMarkDisplayStyle']);
								}
								else {
									$ProcessedValue = $RawValue;
								}
							}
						}	
						else if(in_array($DisplayGrandMark,(array)$ShowStyle) && $ViewFormat!='csv') {
							$ProcessedValue = $lreportcard->Get_Score_Display_HTML($RawValue, $ReportID, $ClassLevelID, $GrandMarkID,$RawValue,$tempDisplayGrandMark, $thisStudentID, '', '', 0, '', '', $parApplyStyleToFailOnly=false, 'M');
						}
						else {
							$ProcessedValue = $RawValue;
						}
					break;
					
					case "GrandGrade":
						// convert Rank Determine Mark to Grade
						$GrandMarkID = $lreportcard->Get_Grand_Field_SubjectID($RankDetermineField);
						//2014-1014-1144-21164
						if ($GrandMarkID == -4) {
							// if SD score, use GPA grading scheme
							$tmpSchemeId = $GrandMarkScheme[-3]['SchemeID'];
						}
						else {
							$tmpSchemeId = $GrandMarkScheme[$GrandMarkID]['SchemeID'];
						}
						$GrandMark = $thisGrandMarks[$RankDetermineField];
						
						//2014-1014-1144-21164
						//$GrandGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandMarkScheme[$GrandMarkID]['SchemeID'], $GrandMark,$ReportID,$thisStudentID,$GrandMarkID,$ClassLevelID,0);
						$adjustedGrandGrade = $thisGrandMarks['GrandAverageGrade'];
						$GrandGrade = $eRCTemplateSetting['GrandAverageGradeManualAdjustment'] && !empty($adjustedGrandGrade)? $adjustedGrandGrade : $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($tmpSchemeId, $GrandMark, $ReportID, $thisStudentID, $GrandMarkID, $ClassLevelID, 0);
						
						if ($ApplyStyleToFailOnly && $ViewFormat!='csv') {
							if ($GrandMark == -1 || $GrandMark == '') {
								$ProcessedValue = $GrandGrade;
							}
							else {
								// [2016-0406-1633-32096] for scale display is mark
								$GradeScaleDisplay = $GrandMarkScheme[$GrandMarkID]['ScaleDisplay'];
								if($GradeScaleDisplay=="M" && is_numeric($GrandMark) && $GrandMark > 0)
									$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $GrandMarkID, $GrandMark, '', 'M', $ReportID);
								else
									$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $GrandMarkID, $GrandGrade, '', 'G', $ReportID);
								
								// [2016-1214-1548-50240] Use Grade for Mark Nature Checking - Adjusted Average Grade
								if ($eRCTemplateSetting['GrandAverageGradeManualAdjustment'] && !empty($adjustedGrandGrade)) {
									$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $GrandMarkID, $GrandGrade, '', 'G', $ReportID);
								}
								
								if ($thisNature == 'Fail') {
									$ProcessedValue = $lreportcard->ReturnTextwithStyle($GrandGrade, '', '', $eRCTemplateSetting['MasterReport']['ApplyFailStyleToMarkDisplayStyle']);
								}
								else {
									$ProcessedValue = $GrandGrade;
								}
							}
						}	
						else {
							$ProcessedValue = $GrandGrade;
						}
					break;
					
					case "Promotion";
						$ProcessedValue = $eReportCard['MasterReport']['PromotionArr'][$RawValue];
						if($eRCTemplateSetting['MasterReport']['HideTermPromotionStatus'] && $ReportType=="T") {
							$ProcessedValue = $EmptySymbol;
						}
					break;
					
					case "OrderMeritForm";
						$OrderMeritStream = $thisGrandMarks["OrderMeritStream"];
						if($eRCTemplateSetting['MasterReport']['AlwaysApplyOrderMeritStream']) {
							$OrderMeritStream = "";
						}
						$rankValue = trim($OrderMeritStream)!=''?$OrderMeritStream:$RawValue;
						
						// convert Rank Determine Mark to Grade
						$GrandMarkID = $lreportcard->Get_Grand_Field_SubjectID($RankDetermineField);
						$GrandMark = $thisGrandMarks[$RankDetermineField];
						
						if ($ApplyStyleToFailOnly && $ViewFormat!='csv') {
							if ($GrandMark == -1 || $GrandMark == '') {
								$ProcessedValue = $rankValue;
							}
							else {
								$GrandGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandMarkScheme[$GrandMarkID]['SchemeID'], $GrandMark,$ReportID,$thisStudentID,$GrandMarkID,$ClassLevelID,0);
								
								// [2016-0406-1633-32096] for scale display is mark
								$GradeScaleDisplay = $GrandMarkScheme[$GrandMarkID]['ScaleDisplay'];		
								if($GradeScaleDisplay=="M" && is_numeric($GrandMark) && $GrandMark > 0)
									$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $GrandMarkID, $GrandMark, '', 'M', $ReportID);
								else
									$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $GrandMarkID, $GrandGrade, '', 'G', $ReportID);
									
								if ($thisNature == 'Fail') {
									$ProcessedValue = $lreportcard->ReturnTextwithStyle($rankValue, '', '', $eRCTemplateSetting['MasterReport']['ApplyFailStyleToMarkDisplayStyle']);
								}
								else {
									$ProcessedValue = $rankValue;
								}
							}
						}	
						else {
							$ProcessedValue = $rankValue;
						}
					break;
	
					case "NoOfPassSubject";
						if (in_array('GMNoOfPassSubject', (array)$ExcludeSubjectComponentFieldAry)) {
							$targetStatAry = $StatStudentNatureWithoutComponentArr;
						}
						else {
							$targetStatAry = $StatStudentNatureArr;
						}
					
						$PassingAry = $lreportcard->getPassingNumber($targetStatAry,1);
						$ProcessedValue = $PassingAry[0];
					break;
					
					case "NoOfFailSubject";
						if (in_array('GMNoOfFailSubject', (array)$ExcludeSubjectComponentFieldAry)) {
							$targetStatAry = $StatStudentNatureWithoutComponentArr;
						}
						else {
							$targetStatAry = $StatStudentNatureArr;
						}
					
						$PassingAry = $lreportcard->getPassingNumber($targetStatAry,1);
						$ProcessedValue = $PassingAry[1];
					break;
	
					case "ClassTeacherComment";
						$ProcessedValue = $subjectTeacherCommentArr[0][$thisStudentID]['Comment'];
//						$ProcessedValue = $ViewFormat=='html'?nl2br($ProcessedValue):$ProcessedValue;
					break;

					case "PersonalCharacteristics":
						$thisPCArr = array();
						
						foreach((array)$PCDataArr[0] as $PCData)
						{
							$thisPCArr[] = $PCArr[$ReportID][0][$thisStudentID][$PCData['CharID']];
						}
						$ProcessedValue = $thisPCArr;
					break;

					// [2016-0406-1633-32096]
					case "FailSubjectUnit":
						$ProcessedValue = $StudentSubjectUnit;
					break;
	
					default: 
						//$ProcessedValue = $RawValue;
						// convert Rank Determine Mark to Grade
						$GrandMarkID = $lreportcard->Get_Grand_Field_SubjectID($RankDetermineField);
						$GrandMark = $thisGrandMarks[$RankDetermineField];
						
						if ($ApplyStyleToFailOnly && $ViewFormat!='csv') {
							if ($GrandMark == -1 || $GrandMark == '') {
								$ProcessedValue = $RawValue;
							}
							else {
								$GrandGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandMarkScheme[$GrandMarkID]['SchemeID'], $GrandMark,$ReportID,$thisStudentID,$GrandMarkID,$ClassLevelID,0);
								
								// [2016-0406-1633-32096] for scale display is mark
								$GradeScaleDisplay = $GrandMarkScheme[$GrandMarkID]['ScaleDisplay'];
								if($GradeScaleDisplay=="M" && is_numeric($GrandMark) && $GrandMark > 0)
									$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $GrandMarkID, $GrandMark, '', 'M', $ReportID);
								else
									$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $GrandMarkID, $GrandGrade, '', 'G', $ReportID);
									
								if ($thisNature == 'Fail') {
									$ProcessedValue = $lreportcard->ReturnTextwithStyle($RawValue, '', '', $eRCTemplateSetting['MasterReport']['ApplyFailStyleToMarkDisplayStyle']);
								}
								else {
									$ProcessedValue = $RawValue;
								}
							}
						}	
						else {
							$ProcessedValue = $RawValue;
						}
				}
				$ProcessedValue = $ProcessedValue == "-1"?$EmptySymbol:$ProcessedValue;
				$GrandMarkData[$DisplayGrandMark] = $ProcessedValue;
				
			}
			
			# Get Form No of Student
			if(!isset($FormNoOfStudent))
				$FormNoOfStudent = $thisGrandMarks["FormNoOfStudent"];
			if($DisplayFormat == "ClassSummary" && !isset($ClassNoOfStudent[$StudentGroupingID]))
					$ClassNoOfStudent[$StudentGroupingID] = $thisGrandMarks["ClassNoOfStudent"]; 
			
			# Append Grand Mark Data to Student Display Ary 
			$StudentDisplayAry[$StudentGroupingID][$thisStudentID]["GrandMarkData"] = $GrandMarkData;
		
		################ Grand Marks Data End ##################
		
	}// loop Sorted User end

} // loop by Class / Subject Gruop / Class Level end
 

############## Statistic ###############
$decimalPlacesShown = 2;

// [2016-0406-1633-32096]
//# get Subject Weight 
//$SubjectWeightRawArr = $lreportcard->returnReportTemplateSubjectWeightData($ReportID);
//
//foreach($SubjectWeightRawArr as $SubjectWeightRec)
//{
//	$WeightColumnID = $SubjectWeightRec["ReportColumnID"] == ''?0: $SubjectWeightRec["ReportColumnID"];
//	$SubjectWeightArr[$SubjectWeightRec["SubjectID"]][$WeightColumnID] = $SubjectWeightRec;
//}

# Get Subject FullMark
$SubjectFullMark = $lreportcard->returnSubjectFullMark($ClassLevelID, NULL, $SubjectIDArr, 1, $ReportID);

############# Subject Mark Statistic 
$MasterReportArr = array_keys($StudentDisplayAry);

foreach($MasterReportArr as $StudentGroupingID)
{
	
	# loop Statistic Data
	foreach((array)$StatisticData as $DisplayStatisticData)
	{
		if($eRCTemplateSetting['MasterReport']['PassingNumberDetermineByPercentage'])
		{
			$CustPassMarkPercentName = "Stat".$DisplayStatisticData."_val";
			$thisSpecialPassRate = $_POST[$CustPassMarkPercentName];
			if($thisSpecialPassRate)
			$CustValue[$CustPassMarkPercentName] = $thisSpecialPassRate;
		}
		
		# loop Subject
		foreach((array) $SubjectIDArr as $thisSubjectID)
		{
			# loop ReportColumn
			foreach((array)$ReportColumnID as $thisReportColumnID)
			{
				
				$thisMarkArr = (array)$StatMarkArr[$StudentGroupingID][$thisSubjectID][$thisReportColumnID];
				
				$thisMarkNatureArr = (array)$StatMarkNatureArr[$StudentGroupingID][$thisSubjectID][$thisReportColumnID];
				
				switch($DisplayStatisticData)
				{
					
					case "Mean":
						$ProcessedValue = $lreportcard->getAverage(array_values(array_remove_empty((array)$thisMarkArr)),$decimalPlacesShown);
					break;
					
					case "SD":
						$ProcessedValue = $lreportcard->getSD(array_values(array_remove_empty((array)$thisMarkArr)),$decimalPlacesShown);
					break;
					
					case "HighestMark":
						$ProcessedValue = $lreportcard->getMaximum(array_values(array_remove_empty((array)$thisMarkArr)));
					break;
					
					case "LowestMark":
						$ProcessedValue = $lreportcard->getMinimum(array_values(array_remove_empty((array)$thisMarkArr)));
					break;
						
					case "PassingRate":
					case "FailingRate":
						$PassingAry = $lreportcard->getPassingRate($thisMarkNatureArr,$decimalPlacesShown,1);
						$ProcessedValue = $DisplayStatisticData=="PassingRate"?$PassingAry[0]:$PassingAry[1];
					break;
						
					case "PassingNumber":
					case "FailingNumber":
						$PassingAry = $lreportcard->getPassingNumber($thisMarkNatureArr,1);
						$ProcessedValue = $DisplayStatisticData=="PassingNumber"?$PassingAry[0]:$PassingAry[1];
					break;
			
					case "SubjectWeight":
						$ProcessedValue = $SubjectWeightArr[$thisSubjectID][$thisReportColumnID]["Weight"];
					break;
			
					case "NumOfStudentStudyingInClass":
						$ProcessedValue = count(array_remove_empty((array)$thisMarkArr));
					break;

					case "NumOfStudentStudyingInForm":
						$ProcessedValue = $FormStudentNoAry[$thisSubjectID][$thisReportColumnID]["StudentNo"];
						// [2014-1218-1621-37164] return zero if empty
						if($eRCTemplateSetting['Report']['MasterReport']['ExcludeDroppedStudent'] && $ProcessedValue==""){
							$ProcessedValue = 0;
						}
					break;
					
					//2014-1014-1144-21164
					case "LowerQuartile":
						$ProcessedValue = $lreportcard->getLowerQuartile(array_values(array_remove_empty((array)$thisMarkArr)),$decimalPlacesShown);
						$ProcessedValue = ($ProcessedValue==='')? $EmptySymbol : $ProcessedValue;
					break;
					case "Median":
						$ProcessedValue = $lreportcard->getMedian(array_values(array_remove_empty((array)$thisMarkArr)),$decimalPlacesShown);
						$ProcessedValue = ($ProcessedValue==='')? $EmptySymbol : $ProcessedValue;
					break;
					case "UpperQuartile":
						$ProcessedValue = $lreportcard->getUpperQuartile(array_values(array_remove_empty((array)$thisMarkArr)),$decimalPlacesShown);
						$ProcessedValue = ($ProcessedValue==='')? $EmptySymbol : $ProcessedValue;
					break;
					
					// uccke cust $eRCTemplateSetting['MasterReport']['PassingNumberDetermineByPercentage']
					case "CustPassingRate":
					case "CustFailingRate":
						$thisFullMark = $SubjectFullMark[$thisSubjectID];
						$thisSpecialPassMark = $thisFullMark*($thisSpecialPassRate)/100;
						
						$PassingAry = $lreportcard->getPassingRateByMarkArr($thisMarkArr,$thisSpecialPassMark,$decimalPlacesShown);
						$ProcessedValue = $DisplayStatisticData=="CustPassingRate"?$PassingAry[0]:$PassingAry[1];
					break;
						
					case "CustPassingNumber":
					case "CustFailingNumber":
						$thisFullMark = $SubjectFullMark[$thisSubjectID];
						$thisSpecialPassMark = $thisFullMark*($thisSpecialPassRate)/100;
						
						$PassingAry = $lreportcard->getPassingNumberByMarkArr($thisMarkArr,$thisSpecialPassMark);
						$ProcessedValue = $DisplayStatisticData=="CustPassingNumber"?$PassingAry[0]:$PassingAry[1];
					break;
					
					default: $ProcessedValue = '';
				}
				$ProcessedValue = $ProcessedValue == -1?$EmptySymbol:$ProcessedValue;
				$StatisticDisplayAry[$StudentGroupingID][$DisplayStatisticData]["SubjectMark"][$thisSubjectID][$thisReportColumnID] = $ProcessedValue;
			} // loop $ReportColumnID
			
		}// loop $SubjectIDArr
		
		## Grand Statistic
		$thisGrandMarkStat = (array)$StatGrandMarkArr[$StudentGroupingID];
		$thisGrandMarkNatureStat = $StatGrandMarkNatureArr[$StudentGroupingID];
		
		$GrandMarkStat = array();
		switch($DisplayStatisticData)
		{
			case "Mean":
				foreach($thisGrandMarkStat as $GrandMarkType => $thisGrandMarkArr)
					$GrandMarkStat[$GrandMarkType] = $lreportcard->getAverage(array_values(array_remove_empty((array)$thisGrandMarkArr)),$decimalPlacesShown);
			break;
			
			case "SD":
				foreach($thisGrandMarkStat as $GrandMarkType => $thisGrandMarkArr)
					$GrandMarkStat[$GrandMarkType] = $lreportcard->getSD(array_values(array_remove_empty((array)$thisGrandMarkArr)),$decimalPlacesShown);			
			break;
			
			case "HighestMark":
				foreach($thisGrandMarkStat as $GrandMarkType => $thisGrandMarkArr)
					$GrandMarkStat[$GrandMarkType] = $lreportcard->getMaximum(array_values(array_remove_empty((array)$thisGrandMarkArr)));			
			break;
			
			case "LowestMark":
				foreach($thisGrandMarkStat as $GrandMarkType => $thisGrandMarkArr)
					$GrandMarkStat[$GrandMarkType] = $lreportcard->getMinimum(array_values(array_remove_empty((array)$thisGrandMarkArr)));			
			break;
				
			case "PassingRate":
			case "FailingRate":
				// only show in rank determine grade
				$thisGrandMarkNatureArr = $thisGrandMarkNatureStat[$RankDetermineField];
				$PassingAry = $lreportcard->getPassingRate(array_values(array_remove_empty((array)$thisGrandMarkNatureArr)),$decimalPlacesShown,1);
				$GrandMarkStat[$RankDetermineField] = $DisplayStatisticData=="PassingRate"?$PassingAry[0]:$PassingAry[1];				
			break;
							
			case "PassingNumber":
			case "FailingNumber":
				// only show in rank determine grade
				$thisGrandMarkNatureArr = $thisGrandMarkNatureStat[$RankDetermineField];
				$PassingAry = $lreportcard->getPassingNumber(array_values(array_remove_empty((array)$thisGrandMarkNatureArr)),1);
				$GrandMarkStat[$RankDetermineField] = $DisplayStatisticData=="PassingNumber"?$PassingAry[0]:$PassingAry[1];
			break;
				
			case "NumOfStudentStudyingInClass":
			case "NumOfStudentStudyingInForm":
				$GrandMarkStat["OrderMeritForm"] = $FormNoOfStudent;
				$GrandMarkStat["OrderMeritClass"] = $ClassNoOfStudent[$StudentGroupingID];
			break;
				
			//2014-1014-1144-21164
			case "LowerQuartile":
				foreach($thisGrandMarkStat as $GrandMarkType => $thisGrandMarkArr) {
					$ProcessedValue = $lreportcard->getLowerQuartile(array_values(array_remove_empty((array)$thisGrandMarkArr)),$decimalPlacesShown);
					$GrandMarkStat[$GrandMarkType] = ($ProcessedValue==='')? $EmptySymbol : $ProcessedValue;
				}
			break;
			case "Median":
				foreach($thisGrandMarkStat as $GrandMarkType => $thisGrandMarkArr) {
					$ProcessedValue = $lreportcard->getMedian(array_values(array_remove_empty((array)$thisGrandMarkArr)),$decimalPlacesShown);
					$GrandMarkStat[$GrandMarkType] = ($ProcessedValue==='')? $EmptySymbol : $ProcessedValue;
				}
			break;
			case "UpperQuartile":
				foreach($thisGrandMarkStat as $GrandMarkType => $thisGrandMarkArr) {
					$ProcessedValue = $lreportcard->getUpperQuartile(array_values(array_remove_empty((array)$thisGrandMarkArr)),$decimalPlacesShown);
					$GrandMarkStat[$GrandMarkType] = ($ProcessedValue==='')? $EmptySymbol : $ProcessedValue;
				}
			break;
		}
		
		$StatisticDisplayAry[$StudentGroupingID][$DisplayStatisticData]["GrandMark"] = $GrandMarkStat;
		
	}// loop $StatisticData
	
}//  loop report
//
//if($eRCTemplateSetting['MasterReport']['PassingNumberDetermineByPercentage'])
//{
//	$CustTitleArr["CustPassingRate"] =  
//}

###### Build Array for display End
$MasterReport = $lreportcard_ui->Get_Master_Report($StudentDisplayAry,$StatisticDisplayAry,$MasterReportSetting, $isSubjectAllNA, $CustValue, $DisplayFormat, $SelectFrom);


if($MasterReportSetting['ViewFormat']==0)
{
	echo $MasterReport[0];
}
else
{
	$numOfRow = count($MasterReport[1]);
	for ($i=0; $i<$numOfRow; $i++) {
		$_numOfCol = count($MasterReport[1][$i]);
		
		for ($j=0; $j<$_numOfCol; $j++) {
			$MasterReport[1][$i][$j] = strip_tags($MasterReport[1][$i][$j]);
		}
	}
	
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	$lexport = new libexporttext();
	// include line break in CSV
	//$ExportTxt = $lexport->GET_EXPORT_TXT($MasterReport[1], "", "", "\r\n", "", 0, "11");
	$ExportTxt = $lexport->GET_EXPORT_TXT($MasterReport[1], "", "", "\r\n", "", 0, "11", 1);
	$filename = trim($MasterReportSetting['ReportTitle']);
	$filename = str_replace('_','%20',$filename);
	$lexport->EXPORT_FILE("$filename.csv", $ExportTxt);
}

?>