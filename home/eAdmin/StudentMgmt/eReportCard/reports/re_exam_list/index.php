<?php
# Editing by 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

$lreportcard = new libreportcardcustom();
$lreportcard_ui = new libreportcard_ui();
$linterface = new interface_html();

$lreportcard->hasAccessRight();

$CurrentPage = "Reports_ReExamList";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eReportCard['Reports_ReExamList'], "", 0);
$linterface->LAYOUT_START();

echo $lreportcard_ui->Get_Report_Common_Index();

?>
<script>
var loading = '<?=$linterface->Get_Ajax_Loading_Image()?>';

$(document).ready(function(){
	js_Reload_Selection();
});

function js_Reload_Selection() {
	var jsYearID = $("select#ClassLevelID").val(); 
		
	$('span#ReportSelectionSpan').html(loading).load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Report',
			YearID: jsYearID,
			ReportID: '',
			SelectionID: 'ReportID',
			onChange: 'js_Reload_Subject_Selection();',
			HideNonGenerated: 1
		},
		function(ReturnData)
		{
			
		}
	);
	
	$('span#ClassSelectionSpan').html(loading).load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Class',
			YearID: jsYearID,
			SelectionID: 'YearClassIDArr[]',
			isMultiple: 1
		},
		function(ReturnData)
		{
			js_Select_All('YearClassIDArr[]', 1);
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
}

function js_Check_Form() {

	// User must select at least one Class
	if (countOption(document.getElementById('YearClassIDArr[]')) == 0)
	{
		alert('<?=$eReportCard['jsSelectSubjectWarning']?>');
		document.getElementById('YearClassIDArr[]').focus();
		return false;
	}

	document.form1.action = "generate.php";
	document.form1.target = "_blank";
	document.form1.submit();
	
}
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>