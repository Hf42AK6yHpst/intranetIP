<?php
# Editing by 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

$lreportcard = new libreportcardcustom();
$lreportcard_ui = new libreportcard_ui();
$linterface = new interface_html();

$lreportcard->hasAccessRight();


$ClassLevelID = trim(stripslashes($_POST['ClassLevelID']));
$ReportID = trim(stripslashes($_POST['ReportID']));
$YearClassIDArr = $_POST['YearClassIDArr'];
$ViewFormat = trim(stripslashes($_POST['ViewFormat']));


### Get Form Name
$ClassLevelName = $lreportcard->returnClassLevel($ClassLevelID);


### Get Students
$isShowStyle = ($ViewFormat=='html')? true : false;
$StudentInfoAssoArr = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $YearClassIDArr, $ParStudentID="", $ReturnAsso=1, $isShowStyle);
$StudentIDArr = array_keys($StudentInfoAssoArr);
$numOfStudent = count($StudentIDArr);


### Get Re-exam Subject
$ReExamSubjectAssoArr = Get_Student_ReExam_Subject($ReportID, $StudentIDArr);


### Build Report Content
$lineBreakReplacement = ($isShowStyle=='html')? '<br />' : "\n";
$contentArr = array();
$rowCounter = 0;
$maxNumOfReExamSubject = 0;
for ($i=0; $i<$numOfStudent; $i++) {
	$thisStudentID = $StudentIDArr[$i];
	$thisClassNameEn = $StudentInfoAssoArr[$thisStudentID]['ClassTitleEn'];
	$thisClassNameCh = $StudentInfoAssoArr[$thisStudentID]['ClassTitleCh'];
	$thisClassName = Get_Lang_Selection($thisClassNameCh, $thisClassNameEn);
	$thisClassNumber = $StudentInfoAssoArr[$thisStudentID]['ClassNumber'];
	$thisStudentName = $StudentInfoAssoArr[$thisStudentID]['StudentName'];
	
	$thisReExamSubjectAssoArr = $ReExamSubjectAssoArr[$thisStudentID];
	$thisNumOfReExamSubject = count((array)$thisReExamSubjectAssoArr); 
	if ($thisNumOfReExamSubject == 0) {
		// no re-exam subject => skip
		continue;
	}
	
	$contentArr[$rowCounter]['ClassName'] = $thisClassName;
	$contentArr[$rowCounter]['ClassNumber'] = $thisClassNumber;
	$contentArr[$rowCounter]['StudentName'] = $thisStudentName;
	$contentArr[$rowCounter]['ReExamSubjectAssoArr'] = $thisReExamSubjectAssoArr;
	$rowCounter++;
	
	$maxNumOfReExamSubject = max($maxNumOfReExamSubject, $thisNumOfReExamSubject);
}
$numOfContent = count($contentArr);


if ($ViewFormat=='html') {
	$css = $lreportcard->Get_GrandMS_CSS();
	
	$ReportTitle = $ClassLevelName.' '.$eReportCard['Reports_ReExamList'];
	
	$ClassNameWidth = 10;
	$ClassNumberWidth = 5;
	$StudentNameWidth = 25;
	$SubjectWidth = 100 - $ClassNameWidth - $ClassNumberWidth - $StudentNameWidth;
	
	$numOfSubjectPerRow = $maxNumOfReExamSubject;
	if ($maxNumOfReExamSubject > 0) {
		$ReExamSubjectWidth = floor(100 / $maxNumOfReExamSubject);
	}
	else {
		$ReExamSubjectWidth = 100;
	}
	
	$numOfRowPerPage = 10;
	$contentPageArr = array_chunk($contentArr, $numOfRowPerPage);
	$numOfPage = count($contentPageArr);
	
	
	$table = '';
	for ($p=0; $p<$numOfPage; $p++) {
		$thisContentArr = $contentPageArr[$p];
		$thisNumOfPageContent = count($thisContentArr);
		
		$table .= "<table id='ResultTable' class='GrandMSContentTable' width='100%' border='1' cellpadding='2' cellspacing='0' style='page-break-after:always;'>\n";
			$table .= "<thead>\n";
				$table .= "<tr>\n";
					$table .= "<th style='width:".$ClassNameWidth."%; text-align:left;'>".$Lang['SysMgr']['FormClassMapping']['Class']."</th>\n";
					$table .= "<th style='width:".$ClassNumberWidth."%; text-align:left;'>".$Lang['SysMgr']['FormClassMapping']['ClassNo']."</th>\n";
					$table .= "<th style='width:".$StudentNameWidth."%; text-align:left;'>".$Lang['SysMgr']['FormClassMapping']['StudentName']."</th>\n";
					$table .= "<th style='width:".$SubjectWidth."%; text-align:left;' colspan='5'>".$Lang['eReportCard']['ReportArr']['ReExamListArr']['ReExamSubject']."</th>\n";
				$table .= "</tr>\n";
			$table .= "</thead>\n";
			
			$table .= "<tbody>\n";
				for ($i=0; $i<$thisNumOfPageContent; $i++) {
					$thisReExamSubjectAssoArr = $thisContentArr[$i]['ReExamSubjectAssoArr'];
					$thisNumOfReExamSubject = count($thisReExamSubjectAssoArr);
					$thisNumOfEmptyTd = $numOfSubjectPerRow - $thisNumOfReExamSubject;
					
					$table .= "<tr>\n";
						$table .= "<td style='text-align:left;'>".$thisContentArr[$i]['ClassName']."</td>\n";
						$table .= "<td style='text-align:left;'>".$thisContentArr[$i]['ClassNumber']."</td>\n";
						$table .= "<td style='text-align:left;'>".$thisContentArr[$i]['StudentName']."</td>\n";
						$table .= "<td style='text-align:left;'>";
							$table .= "<table width='100%' border='0px'>\n";
								$table .= "<tr>\n";
									foreach ((array)$thisReExamSubjectAssoArr as $thisSubjectID => $thisSubjectName) {
										$table .= "<td style='text-align:left; width:".$ReExamSubjectWidth."%;'>".$thisSubjectName."</td>\n";
									}
									
									for ($j=0; $j<$thisNumOfEmptyTd; $j++) {
										$table .= "<td style='text-align:left; width:".$ReExamSubjectWidth."%;'>&nbsp;</td>\n";
									}
								$table .= "</tr>\n";
							$table .= "</table>\n";
						$table .= "</td>\n";
					$table .= "</tr>\n";
				}
			$table .= "</tbody>\n";
		$table .= "</table>\n";
	}
	
	
	$allTable = '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="center"><h1>'.$ReportTitle.'</h1></td>
					</tr>
					<tr>
						<td align="center" valign="top">'.$table.'</td>
					</tr>
				</table>';
	
	echo $lreportcard->Get_Report_Header($ReportTitle);
	echo $allTable.$css;
	echo $lreportcard->Get_Report_Footer();
	
	intranet_closedb();
}
else {
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	
	$lexport = new libexporttext();
	$headerArr = array();
	$csvContentArr = array();
	
	$headerArr[] = $Lang['SysMgr']['FormClassMapping']['Class'];
	$headerArr[] = $Lang['SysMgr']['FormClassMapping']['ClassNo'];
	$headerArr[] = $Lang['SysMgr']['FormClassMapping']['StudentName'];
	$headerArr[] = $Lang['eReportCard']['ReportArr']['ReExamListArr']['ReExamSubject'];
	for ($i=0; $i<$maxNumOfReExamSubject-1; $i++) {
		$headerArr[] = '';
	}
	
	for ($i=0; $i<$numOfContent; $i++) {
		$csvContentArr[$i][] = $contentArr[$i]['ClassName'];
		$csvContentArr[$i][] = $contentArr[$i]['ClassNumber'];
		$csvContentArr[$i][] = $contentArr[$i]['StudentName'];
		
		foreach((array)$contentArr[$i]['ReExamSubjectAssoArr'] as $thisSubjectID => $thisSubjectName) {
			$csvContentArr[$i][] = $thisSubjectName;
		}
	}
	
	$filename = 're_exam_list_'.$ClassLevelName;
	$filename = str_replace(" ", "_", $filename);
	$filename = $filename.".csv";
	
	$export_content = $lexport->GET_EXPORT_TXT($csvContentArr, $headerArr, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);
	intranet_closedb();
	
	// Output the file to user browser
	$lexport->EXPORT_FILE($filename, $export_content);
}

function Get_Student_ReExam_Subject($ReportID, $StudentIDArr) {
	global $lreportcard, $eReportCard;
		
	if (!is_array($StudentIDArr)) {
		$StudentIDArr = array($StudentIDArr);
	}
	$numOfStudent = count($StudentIDArr);
	
	### Get Report Info
	$ReportInfoArr = $lreportcard->returnReportTemplateBasicInfo($ReportID);
	$ClassLevelID = $ReportInfoArr['ClassLevelID'];
	$FormNumber = $lreportcard->GET_FORM_NUMBER($ClassLevelID);
	
	
	### Get Marks
	//$MarkNatureArr[$StudentID][$SubjectID][$ReportColumnID]['Weight', 'Nature', ...] = data
	$MarkNatureArr = $lreportcard->Get_Student_Subject_Mark_Nature_Arr($ReportID, $ClassLevelID, $lreportcard->getMarks($ReportID), $WithExtraData=true);
	
	
	### Get Subject
	$SubjectAssoInfoArr = $lreportcard->returnSubjectwOrderNoL($ClassLevelID, $ParForSelection=0, $MainSubjectOnly=1, $ReportID, $_SESSION['intranet_session_language']);
	
	
	### Evaluate Student Mark Nature
	$StudentInfoAssoArr = array();
	$ReportColumnID = 0;	// check overall column only
	for ($i=0; $i<$numOfStudent; $i++) {
		$thisStudentID = $StudentIDArr[$i];
		
		// loop $SubjectAssoInfoArr to maintain Subject ordering
		foreach((array)$SubjectAssoInfoArr as $thisSubjectID => $thisSubjectName) {
			$thisNature = $MarkNatureArr[$thisStudentID][$thisSubjectID][$ReportColumnID]['Nature'];
			$thisWeight = $MarkNatureArr[$thisStudentID][$thisSubjectID][$ReportColumnID]['Weight'];
			$thisScaleInput = $MarkNatureArr[$thisStudentID][$thisSubjectID][$ReportColumnID]['ScaleInput'];
			
			if ($thisScaleInput == 'M' && $thisNature == 'Fail') {
				$StudentInfoAssoArr[$thisStudentID]['FailedWeight'] += $thisWeight;
				$StudentInfoAssoArr[$thisStudentID]['FailedSubjectArr'][$thisSubjectID] = $thisSubjectName;
			}
		}
	}
	
	
	### Build Re-exam subject array
	$ReExamSubjectArr = array();
	for ($i=0; $i<$numOfStudent; $i++) {
		$thisStudentID = $StudentIDArr[$i];
		
		$thisSubjectFailedWeight = $StudentInfoAssoArr[$thisStudentID]['FailedWeight'];
		$thisSubjectFailedSubjectAssoArr = $StudentInfoAssoArr[$thisStudentID]['FailedSubjectArr'];
		
		if ($FormNumber==3) {
			// 初三
			if ($thisSubjectFailedWeight < 5) {
				$ReExamSubjectArr[$thisStudentID] = $thisSubjectFailedSubjectAssoArr;
			}
		}
		else if ($FormNumber==6) {
			// 高三
			$ReExamSubjectArr[$thisStudentID] = $thisSubjectFailedSubjectAssoArr;
		}
		else {
			// 初一、初二、高一、高二
			if ($thisSubjectFailedWeight < 5) {
				$ReExamSubjectArr[$thisStudentID] = $thisSubjectFailedSubjectAssoArr;
			}
		}
	}
	
	return $ReExamSubjectArr;
}
?>