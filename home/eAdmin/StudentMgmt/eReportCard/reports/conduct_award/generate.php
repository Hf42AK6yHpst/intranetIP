<?
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$ClassLevelIDArr = $ClassLvID;
$selectedTermsArr = $Terms;
$ConductMarkAry = $ConductMark;
$transferToAward = IntegerSafe(trim($_POST['transferToAward']));


if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	if ($lreportcard->hasAccessRight()) {
		$ReportList = $lreportcard->Get_Report_List();
		
		//hdebug_pr($ReportList);
		
		$ConductAwardList = array();
		#loop each Report
		foreach($ReportList as $Report)
		{
			//skip ClassLevelID which are not chosen by the user
			if(!in_array($Report["ClassLevelID"],$ClassLevelIDArr)) continue;
			
			#Retrieve ClassList
			$ClassList = $lreportcard->GET_CLASSES_BY_FORM($Report["ClassLevelID"]);
			
			#loop each Class
			foreach($ClassList as $Class)
			{
				$OtherInfoAry=$lreportcard->getReportOtherInfoData($Report["ReportID"], "", $Class['ClassID']);
				#$OtherInfoAry[$StudentID][$YearTermID][$UploadType] = Value
				
				#loop each student				
				foreach($OtherInfoAry as $StudentID => $TermAry)
				{	
					if(empty($TermAry)) continue;

					#loop each Selected Term
					$__isAllTermSatisfyConductGrade = true;
					foreach($selectedTermsArr as $TermID)
					{
						if (in_array($TermAry[$TermID]['Conduct'],$ConductMarkAry)) {
							$StudentTermConductAry[$StudentID][$TermID]=$TermAry[$TermID]['Conduct'];
						}
						else {
							$__isAllTermSatisfyConductGrade = false;
						}
//						else if(empty($TermAry[$TermID]) && isset($StudentTermConductAry[$StudentID]))
//							$StudentTermConductAry[$StudentID][$TermID] = $lreportcard->EmptySymbol;
					}
//					if(!empty($StudentTermConductAry[$StudentID]))
//						$ConductAwardList[]=$StudentID;
					
					if ($__isAllTermSatisfyConductGrade) {
						//$ConductAwardList[] = $StudentID;
					}	
				}				
			}
		}
		
		//2013-0703-0927-26140
		//$ConductAwardList = array_keys((array)$StudentTermConductAry);
		$studentFormAssoAry = array();
		$numOfForm = count((array)$ClassLevelIDArr);
		for ($i=0; $i<$numOfForm; $i++) {
			$_classLevelId = $ClassLevelIDArr[$i];
			
			$_studentInfoArr = $lreportcard->GET_STUDENT_BY_CLASSLEVEL('', $_classLevelId);
			$_numOfStudent = count($_studentInfoArr);
			
			for ($j=0; $j<$_numOfStudent; $j++) {
				$__studentId = $_studentInfoArr[$j]['UserID'];
				
				$__isAllTermSatisfyConductGrade = true;
			
				foreach((array)$selectedTermsArr as $___yearTermId) {
					$___conduct = $StudentTermConductAry[$__studentId][$___yearTermId];
					
					if (!in_array($___conduct, (array)$ConductMarkAry)) {
						$__isAllTermSatisfyConductGrade = false;
						break;
					}
				}
				
				if ($__isAllTermSatisfyConductGrade) {
					$ConductAwardList[] = $__studentId;
					$studentFormAssoAry[$__studentId] = $_classLevelId;
				}
			}
		}
		
		//debug_pr($ConductAwardList);
		
	$obj_AcademicYear = new academic_year($lreportcard->schoolYearID);
	#build title
	$ActiveYear = $obj_AcademicYear->Get_Academic_Year_Name();
	$ReportType = $eReportCard['Reports_ConductAwardReport'];
	$ReportTitle = $ActiveYear." ".$ReportType;
	
	#build YearTermArr to get TermName by TermID
	$YearTermArr = $obj_AcademicYear->Get_Term_List(0);
	
	//build ary : $YearTermName[$TermID]=$TermName
	foreach($YearTermArr as $data)
		$YearTermName[$data[0]]=$data[1];
		
		
	if ($transferToAward) {
		include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");
		$laward = new libreportcard_award();
		
		// get award info
		$awardInfoAry = $laward->Get_Award_Info_By_Code('conduct_award');
		$awardId = $awardInfoAry['AwardID'];
		
		// transfer award list to last selected term report => get last selected term report data
		$lastTermId = $selectedTermsArr[count($selectedTermsArr)-1];
		$reportIdAssoAry = array();
		for ($i=0; $i<$numOfForm; $i++) {
			$_classLevelId = $ClassLevelIDArr[$i];
			
			$_lastTermReportInfoAry = $lreportcard->returnReportTemplateBasicInfo('', '', $_classLevelId, $lastTermId);
			$reportIdAssoAry[$_classLevelId] = $_lastTermReportInfoAry['ReportID'];
		}
		
		
		$numOfAwardStudent = count($ConductAwardList);
		$consolidatedAwardStudentAry = array();
		for ($i=0; $i<$numOfAwardStudent; $i++) {
			$_studentId = $ConductAwardList[$i];
			$_classLevelId = $studentFormAssoAry[$_studentId];
			$_reportId = $reportIdAssoAry[$_classLevelId];
			
			// 0 means overall award, not subject award
			$consolidatedAwardStudentAry[$_reportId][$awardId][0][$_studentId]['DetermineValue'] = '';
			$consolidatedAwardStudentAry[$_reportId][$awardId][0][$_studentId]['RankField'] = '';
		}
				
		$involovedReportIdAry = array_values($reportIdAssoAry);
		$successAry['deleteOldAwardResult'] = $laward->Delete_Award_Generated_Student_Record($involovedReportIdAry, $RecordIDArr='', $awardId);
		foreach ((array)$consolidatedAwardStudentAry as $_reportId => $_reportAwardAry) {
			foreach ((array)$_reportAwardAry as $__awardId => $__awardStudentAry) {
				$__orderedAwardStudentAry = $laward->Sort_And_Add_AwardRank_In_InfoArr($__awardStudentAry, 'DetermineValue', 'desc');
				
				$__dbUpdateInfoAry = array();
				foreach ((array)$__orderedAwardStudentAry as $___subjectId => $___subjectStudentAry) {
					foreach ((array)$___subjectStudentAry as $____studentId => $____subjectStudentAwardAry) {
						$____tmpAry = array();
						$____tmpAry = $____subjectStudentAwardAry;
						$____tmpAry['SubjectID'] = $___subjectId;
						$____tmpAry['StudentID'] = $____studentId;
						$__dbUpdateInfoAry[] = $____tmpAry;
						
						unset($____tmpAry);
					}
				}
				
				if (count((array)$__dbUpdateInfoAry) > 0) {
					$successAry['updateAwardGeneratedStudentRecord'][$awardId] = $laward->Update_Award_Generated_Student_Record($awardId, $_reportId, $__dbUpdateInfoAry);
				}
				unset($__dbUpdateInfoAry);
			}
		}
	}
	
	
	## Display the result
	if ($ViewFormat == 'html')
	{
		$table = '';
		#Table Start
		$table .= "<table id='ResultTable' class='GrandMSContentTable' width='100%' border='1' cellpadding='2' cellspacing='0'>\n";
			//$table .= "<h1 align='center' style='text-align:center'>".$ReportTitle."</h1>";

			#THEAD
			$table .= "<thead>\n";
				$table .= "<tr>\n";
					$table .= "<th>#</th>\n";
					$table .= "<th>".$eReportCard['FormName']."</th>\n";
					$table .= "<th>".$eReportCard['Class']."</th>\n";
					$table .= "<th>".$eReportCard['StudentNo_short']."</th>\n";
					$table .= "<th>".$eReportCard['StudentNameEn']."</th>\n";
					$table .= "<th>".$eReportCard['StudentNameCh']."</th>\n";
					foreach($selectedTermsArr as $TermID)
					{
						$table .= "<th>".$YearTermName[$TermID]."</th>\n";
					}	
				$table .= "</tr>\n";
			$table .= "</thead>\n";
			#END THEAD
			
			#TBODY
			$table .= "<tbody>\n";
				#loop Conduct Award Student List 
				foreach($ConductAwardList as $key=>$StudentID)
				{
					$lu = new libuser($StudentID);
					$ClassLevelInfo = $lreportcard->Get_Student_Class_ClassLevel_Info($StudentID);
					$ClassNameField = Get_Lang_Selection('ClassNameCh', 'ClassName');

					$table .= "<tr>\n";
						$table .= "<td align='center'>".($key+1)."</td>\n";
						$table .= "<td align='center'>".$ClassLevelInfo[0]['ClassLevelName']."</td>\n";
						$table .= "<td align='center'>".$ClassLevelInfo[0][$ClassNameField]."</td>\n";
						$table .= "<td align='center'>".$ClassLevelInfo[0]['ClassNumber']."</td>\n";
						$table .= "<td align='center'>".$lu->EnglishName."</td>\n";
						$table .= "<td align='center'>".$lu->ChineseName."</td>\n";
						
						foreach($selectedTermsArr as $TermID)
						{
							$thisConduct = $StudentTermConductAry[$StudentID][$TermID]?$StudentTermConductAry[$StudentID][$TermID]:$lreportcard->EmptySymbol;
							$table .= "<td align='center'>".($thisConduct)."</td>\n";
						}	
					$table .= "</tr>\n"; 
				} # end loop Student List
			$table .= "</tbody>\n";
			#END TBODY
			
		$table .= "</table>\n"; 
		#END TABLE
			
		$css = $lreportcard->Get_GrandMS_CSS();
		
		$allTable = '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td align="center"><h1>'.$ReportTitle.'</h1></td>
						</tr>
						<tr>
							<td>'.$table.'</td>
						</tr>
					</table>';
		
		echo $lreportcard->Get_Report_Header($ReportTitle);
		echo $allTable.$css;
		echo $lreportcard->Get_Report_Footer();

			
	}
	else if ($ViewFormat == 'csv')
	{
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		include_once($PATH_WRT_ROOT."includes/libexporttext.php");
		
		$ExportHeaderArr = array();
		$ExportContentArr = array();
		$lexport = new libexporttext();
		
		
		# Header
		if ($eRCTemplateSetting['Report']['ConductAward']['ExportUserLogin']) {
			$ExportHeaderArr[] = $Lang['General']['UserLogin'];
		}
		$ExportHeaderArr[] = $eReportCard['FormName'];
		$ExportHeaderArr[] = $eReportCard['Class'];
		$ExportHeaderArr[] = $eReportCard['StudentNo_short'];
		$ExportHeaderArr[] = $eReportCard['StudentNameEn'];
		$ExportHeaderArr[] = $eReportCard['StudentNameCh'];
		
		foreach($selectedTermsArr as $TermID)
		{
			$ExportHeaderArr[] = $YearTermName[$TermID];
		}
		
		#Body
		foreach($ConductAwardList as $key=>$StudentID)
		{
			$row=$key;
			$col=0;
			$lu = new libuser($StudentID);
			$ClassLevelInfo = $lreportcard->Get_Student_Class_ClassLevel_Info($StudentID);
			$ClassNameField = ($intranet_session_language=="en")? "ClassName" : "ClassNameCh";
			
			if ($eRCTemplateSetting['Report']['ConductAward']['ExportUserLogin']) {
				$ExportContentArr[$row][$col++] = $lu->UserLogin;
			}
			$ExportContentArr[$row][$col++] = $ClassLevelInfo[0]['ClassLevelName'];
			$ExportContentArr[$row][$col++] = $ClassLevelInfo[0][$ClassNameField];
			$ExportContentArr[$row][$col++] = $ClassLevelInfo[0]['ClassNumber'];
			$ExportContentArr[$row][$col++] = $lu->EnglishName;
			$ExportContentArr[$row][$col++] = $lu->ChineseName;
			foreach($selectedTermsArr as $TermID)
			{
				$thisConduct = $StudentTermConductAry[$StudentID][$TermID]?$StudentTermConductAry[$StudentID][$TermID]: $lreportcard->EmptySymbol;
				$ExportContentArr[$row][$col++] = $thisConduct;
			}	
		}
			
		$ReportTitle = str_replace(" ", "_", $ReportTitle);
		$filename = $ReportTitle.".csv";
		
		$export_content = $lexport->GET_EXPORT_TXT($ExportContentArr, $ExportHeaderArr);
		intranet_closedb();
		
		// Output the file to user browser
		$lexport->EXPORT_FILE($filename, $export_content);
	}
	}
}


?>
