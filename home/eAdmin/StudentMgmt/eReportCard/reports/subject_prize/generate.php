<?
// Using:

################## Change Log [Start] ##############
#
#   Date:   2019-06-28  Bill
#           Term Selection > Change 'TermID' to 'TargetTerm' to prevent IntegerSafe()
#
################## Change Log [End] ##############

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

$ViewFormat = standardizeFormPostValue($_POST['ViewFormat']);
// $TermID = standardizeFormPostValue($_POST['TermID']);
$TermID = standardizeFormPostValue($_POST['TargetTerm']);
if($TermID != 'F') {
    $TermID = IntegerSafe($TermID);
}
$Level = standardizeFormPostValue($_POST['Level']);
$TargetIDArr = $_POST['TargetID'];
$NoOfSubjectPrize = integerSafe(standardizeFormPostValue($_POST['PrizeNum']));
$reportType = standardizeFormPostValue($_POST['reportType']);

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	if ($lreportcard->hasAccessRight()) 
	{
		# $subjectPrizeInfoArr[$SubjectID][$StudentID][Mark, Order, ...] = value
		$subjectPrizeInfoArr = array();
		
		
		if ($eRCTemplateSetting['Report']['GPA_Top3']['MainOrExtraReportOption']) {
			if ($reportType == 'main') {
				$mainReportOnly = 1;
				$extraReportOnly = 0;
				$columnTitle = $Lang['eReportCard']['Exam'].' ';
			}
			else {
				$mainReportOnly = 0;
				$extraReportOnly = 1;
				$columnTitle = $Lang['eReportCard']['Test'].' ';
			}
		}
		else {
			// normal client => get from main report only
			$mainReportOnly = 1;
			$extraReportOnly = 0;
		}
		
		
		# Report Title
		//$ActiveYear = $lreportcard->GET_ACTIVE_YEAR("-");
		$ObjAcademicYear = new academic_year($lreportcard->schoolYearID);
		$ActiveYear = $ObjAcademicYear->Get_Academic_Year_Name();
		$SemTitle = $lreportcard->returnSemesters($TermID);
		$ReportType = $eReportCard['Reports_SubjectPrizeReport'];
		$ReportTitle = $ActiveYear." ".$SemTitle." ".$columnTitle.$ReportType;
		
		
		$numOfTarget = count($TargetIDArr);
		for ($i=0; $i<$numOfTarget; $i++)
		{
			# Get the Student List of each Target form / class
			if ($Level == "Class")
			{
				//$NoOfSubjectPrize = 1;
				$thisClassID = $TargetIDArr[$i];
				$thisClassLevelID = $lreportcard->Get_ClassLevel_By_ClassID($thisClassID);
				$thisOrderField = 'OrderMeritClass';
				
				# Get Student List
				$thisStudentInfoArr = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($thisReportID, $thisClassLevelID, $thisClassID);
				// ignore class / form with no student
				if (!is_array($thisStudentInfoArr) || count($thisStudentInfoArr)==0)
					continue;
			}
			else if ($Level == "Form")
			{
				//$NoOfSubjectPrize = 3;
				$thisClassLevelID = $TargetIDArr[$i];
				$thisClassID = '';
				$thisOrderField = 'OrderMeritForm';
				
				# Get Student List
				$thisStudentInfoArr = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($thisReportID, $thisClassLevelID, $thisClassID);
				// ignore class / form with no student
				if (!is_array($thisStudentInfoArr) || count($thisStudentInfoArr)==0)
					continue;
			}
			else if ($Level == "SubjectGroup")
			{
				//$NoOfSubjectPrize = 1;
				$thisSubjectGroupID = $TargetIDArr[$i];
				$thisOrderField = 'OrderMeritSubjectGroup';
				
				# Get Student List
				$thisSubjectGroupObj = new subject_term_class($thisSubjectGroupID, $GetTeacherList=false, $GetStudentList=true);
				$thisStudentInfoArr = $thisSubjectGroupObj->ClassStudentList;
				$thisClassLevelID = $thisSubjectGroupObj->YearID[0];
				$MainSubjectIDArray = array($thisSubjectGroupObj->SubjectID);
				
				// ignore class / form with no student
				if (!is_array($thisStudentInfoArr) || count($thisStudentInfoArr)==0)
					continue;
			}
			
			# Get Report Info
			$thisReportInfoArr = $lreportcard->GET_REPORT_TYPES($thisClassLevelID, $ReportID="", $TermID, $mainReportOnly, $orderBySubmissionTime=false, $withinSubmissionPeriod=false, $extraReportOnly);
			$thisReportID = $thisReportInfoArr[0]['ReportID'];
				
				
			# Construct StudentID List
			$thisStudentIDArr = Get_Array_By_Key($thisStudentInfoArr, 'UserID');
			$thisStudentInfoAssoArr = BuildMultiKeyAssoc($thisStudentInfoArr, array("UserID"));
			
			# Get Subject Grading Info
			$SubjectGradingInfoArr = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($thisClassLevelID,0,$thisReportID);
			
			# Get Subject List
			if ($Level == "Class" || $Level == "Form")
			{
				$MainSubjectArray = $lreportcard->returnSubjectwOrder($thisClassLevelID);
				$MainSubjectIDArray = array();
				$MainSubjectNameArray = array();
				if (sizeof($MainSubjectArray) > 0)
					foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);
				// ignore class / form with no subject
				if (!is_array($MainSubjectIDArray) || count($MainSubjectIDArray)==0)
					continue;
			}
			
			$numOfSubject = count($MainSubjectIDArray);
			for ($j=0; $j<$numOfSubject; $j++)
			{
				$thisSubjectID = $MainSubjectIDArray[$j];
				$thisScaleInput = $SubjectGradingInfoArr[$thisSubjectID]['scaleInput'];
				$thisScaleDisplay = $SubjectGradingInfoArr[$thisSubjectID]['scaleDisplay'];
				
				if ($Level == "SubjectGroup")
				{
					$isParentSubject = 1;
				}
				else
				{
					$isParentSubject = 0;
					$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($thisSubjectID, $thisClassLevelID);
					if(!empty($CmpSubjectArr)) 
						$isParentSubject = 1;
				}
				
				
				$thisSubjectWeightData = $lreportcard->returnReportTemplateSubjectWeightData($thisReportID, "ReportColumnID IS NULL and SubjectID=$thisSubjectID");
				$thisSubjectWeight = $thisSubjectWeightData[0]['Weight'];
				
				# Get the corresponding Mark field in the DB
				if ($thisScaleInput == "G")
				{
					continue;
				}
				else if ($thisScaleInput == "M" && $thisScaleDisplay == "M")
				{
					$thisMarkField = "Mark";
				}
				else if ($thisScaleInput == "M" && $thisScaleDisplay == "G")
				{
					$thisMarkField = "RawMark";
				}
				
				# Get Marks
				$thisStudentIDList = implode(", ", $thisStudentIDArr);
				$cons = " 	AND a.ReportColumnID='0' 
							AND a.SubjectID = '$thisSubjectID'
							AND a.StudentID IN ($thisStudentIDList) 
							AND a.$thisOrderField > 0 AND a.$thisOrderField <= $NoOfSubjectPrize AND a.$thisOrderField IS NOT NULL
							AND a.$thisMarkField != 0 AND a.$thisMarkField IS NOT NULL
						";
				// $thisMarkArr[$StudentID][$SubjectID][$ReportColumnID] = MarkInfoArr
				$thisMarkArr = $lreportcard->getMarks($thisReportID, '', $cons, 1, 1, "a.$thisOrderField");
												
				foreach ($thisMarkArr as $thisStudentID => $thisSubjectMarkInfoArr)
				{
					### Get Student MArk
					$thisStudentMarkInfoArr = $thisSubjectMarkInfoArr[$thisSubjectID][0];
					$thisMark = my_round($thisStudentMarkInfoArr[$thisMarkField], 2);
					//if ($isParentSubject)
					//	$thisMark = $thisMark * $thisSubjectWeight;
					$thisOrder = $thisStudentMarkInfoArr[$thisOrderField];
					
					### Get Student Info
					$thisStudentInfoArr = $lreportcard->Get_Student_Class_ClassLevel_Info($thisStudentID);
					$thisClassLevelName = $thisStudentInfoArr[0]['ClassLevelName'];
					$thisClassName = Get_Lang_Selection($thisStudentInfoArr[0]['ClassNameCh'], $thisStudentInfoArr[0]['ClassName']);
					$thisClassNumber = $thisStudentInfoArr[0]['ClassNumber'];
					
					if ($TermID == 'F') {
						// do not display subject group info for consolidated report
					}
					else {
						### Get Student Subject Group Info
						$thisSubjectGroupInfoArr = $lreportcard->Get_Student_Studying_Subject_Group_Info($TermID, $thisStudentID, $thisSubjectID);
						$thisSubjectGroupTitle = Get_Lang_Selection($thisSubjectGroupInfoArr[0]['ClassTitleB5'], $thisSubjectGroupInfoArr[0]['ClassTitleEN']);
					
						$subjectPrizeInfoArr[$thisSubjectID][$thisStudentID]['SubjectGroupName'] = $thisSubjectGroupTitle;
					}
					$subjectPrizeInfoArr[$thisSubjectID][$thisStudentID]['ClassLevelName'] = $thisClassLevelName;
					$subjectPrizeInfoArr[$thisSubjectID][$thisStudentID]['ClassName'] = $thisClassName;
					$subjectPrizeInfoArr[$thisSubjectID][$thisStudentID]['ClassNumber'] = $thisClassNumber;
					$subjectPrizeInfoArr[$thisSubjectID][$thisStudentID]['UserLogin'] = $thisStudentInfoAssoArr[$thisStudentID]['UserLogin'];
					$subjectPrizeInfoArr[$thisSubjectID][$thisStudentID]['StudentNameEn'] = $thisStudentInfoAssoArr[$thisStudentID]['StudentNameEn'];
					$subjectPrizeInfoArr[$thisSubjectID][$thisStudentID]['StudentNameCh'] = $thisStudentInfoAssoArr[$thisStudentID]['StudentNameCh'];
					$subjectPrizeInfoArr[$thisSubjectID][$thisStudentID]['Mark'] = $thisMark;
					$subjectPrizeInfoArr[$thisSubjectID][$thisStudentID]['Order'] = $thisOrder;
				}
			}
		}
		
		
		
		//debug_r($subjectPrizeInfoArr);
		
		## Display the result
		if ($ViewFormat == 'html')
		{
			$table = '';
			$table .= "<table id='ResultTable' class='GrandMSContentTable' width='100%' border='1' cellpadding='2' cellspacing='0'>\n";
				$table .= "<thead>\n";
					$table .= "<tr>\n";
						$table .= "<th>#</th>\n";
						$table .= "<th>".$eReportCard['Subject']."</th>\n";
						if ($TermID == 'F') {
							// do not display subject group info for consolidated report
						}
						else {
							$table .= "<th>".$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle']."</th>\n";
						}
						$table .= "<th>".$eReportCard['FormName']."</th>\n";
						$table .= "<th>".$eReportCard['Class']."</th>\n";
						$table .= "<th>".$eReportCard['Template']['StudentInfo']['ClassNo']."</th>\n";
						$table .= "<th>".$eReportCard['StudentNameEn']."</th>\n";
						$table .= "<th>".$eReportCard['StudentNameCh']."</th>\n";
						$table .= "<th>".$eReportCard['Template']['Mark']."</th>\n";
						//$table .= "<th>".$eReportCard['Template']['Position']."</th>\n";
					$table .= "</tr>\n";
				$table .= "</thead>\n";
				
				$table .= "<tbody>\n";
				$counter = 0;
					foreach ($subjectPrizeInfoArr as $SubjectID => $SubjectInfoArr)
					{
						$thisSubjectName = $lreportcard->GET_SUBJECT_NAME_LANG($SubjectID);
						foreach ($SubjectInfoArr as $StudentID => $StudentInfoArr)
						{
							$counter++;
							
							//$thisOrder = $StudentInfoArr['Order'];
							
							$table .= "<tr>\n";
								$table .= "<td align='center'>".$counter."</td>\n";
								$table .= "<td align='left'>".$thisSubjectName."</td>\n";
								if ($TermID == 'F') {
									// do not display subject group info for consolidated report
								}
								else {
									$table .= "<td align='left'>".$StudentInfoArr['SubjectGroupName']."</td>\n";
								}
								$table .= "<td align='center'>".$StudentInfoArr['ClassLevelName']."</td>\n";
								$table .= "<td align='center'>".$StudentInfoArr['ClassName']."</td>\n";
								$table .= "<td align='center'>".$StudentInfoArr['ClassNumber']."</td>\n";
								$table .= "<td align='center'>".$StudentInfoArr['StudentNameEn']."</td>\n";
								$table .= "<td align='center'>".$StudentInfoArr['StudentNameCh']."</td>\n";
								$table .= "<td align='center'>".$StudentInfoArr['Mark']."</td>\n";
								//$table .= "<td align='center'>".$thisOrder."</td>\n";
							$table .= "</tr>\n";
						}
					}
				$table .= "</tbody>\n";
			$table .= "</table>\n";
			
			$css = $lreportcard->Get_GrandMS_CSS();
			
			$allTable = '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center"><h1>'.$ReportTitle.'</h1></td>
							</tr>
							<tr>
								<td>'.$table.'</td>
							</tr>
						</table>';
			
			echo $lreportcard->Get_Report_Header($ReportTitle);
			echo $allTable.$css;
			echo $lreportcard->Get_Report_Footer();
		}
		else if ($ViewFormat == 'csv')
		{
			include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
			include_once($PATH_WRT_ROOT."includes/libexporttext.php");
			
			$ExportHeaderArr = array();
			$ExportContentArr = array();
			$lexport = new libexporttext();
			
			# Header
			$ExportHeaderArr[] = $eReportCard['Subject'];
			if ($TermID == 'F') {
				// do not display subject group info for consolidated report
			}
			else {
				$ExportHeaderArr[] = $Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle'];
			}
			if ($eRCTemplateSetting['Report']['SubjectPrize']['ExportUserLogin']) {
				$ExportHeaderArr[] = $Lang['General']['UserLogin'];
			}
			$ExportHeaderArr[] = $eReportCard['FormName'];
			$ExportHeaderArr[] = $eReportCard['Class'];
			$ExportHeaderArr[] = $eReportCard['Template']['StudentInfo']['ClassNo'];
			$ExportHeaderArr[] = $eReportCard['StudentNameEn'];
			$ExportHeaderArr[] = $eReportCard['StudentNameCh'];
			$ExportHeaderArr[] = $eReportCard['Template']['Mark'];
			
			# Content
			$i_counter = 0;
			foreach ($subjectPrizeInfoArr as $SubjectID => $SubjectInfoArr)
			{
				$thisSubjectName = $lreportcard->GET_SUBJECT_NAME_LANG($SubjectID);
				foreach ($SubjectInfoArr as $StudentID => $StudentInfoArr)
				{
					$j_counter = 0;
					
					$ExportContentArr[$i_counter][$j_counter++] = $thisSubjectName;
					if ($TermID == 'F') {
						// do not display subject group info for consolidated report
					}
					else {
						$ExportContentArr[$i_counter][$j_counter++] = $StudentInfoArr['SubjectGroupName'];
					}
					if ($eRCTemplateSetting['Report']['SubjectPrize']['ExportUserLogin']) {
						$ExportContentArr[$i_counter][$j_counter++] = $StudentInfoArr['UserLogin'];
					}
					$ExportContentArr[$i_counter][$j_counter++] = $StudentInfoArr['ClassLevelName'];
					$ExportContentArr[$i_counter][$j_counter++] = $StudentInfoArr['ClassName'];
					$ExportContentArr[$i_counter][$j_counter++] = $StudentInfoArr['ClassNumber'];
					$ExportContentArr[$i_counter][$j_counter++] = $StudentInfoArr['StudentNameEn'];
					$ExportContentArr[$i_counter][$j_counter++] = $StudentInfoArr['StudentNameCh'];
					$ExportContentArr[$i_counter][$j_counter++] = $StudentInfoArr['Mark'];
					
					$i_counter++;
				}
			}
			
			
			// Title of the grandmarksheet
			$ReportTitle = str_replace(" ", "_", $ReportTitle);
			$filename = $ReportTitle.".csv";
			
			$export_content = $lexport->GET_EXPORT_TXT($ExportContentArr, $ExportHeaderArr);
			intranet_closedb();
			
			// Output the file to user browser
			$lexport->EXPORT_FILE($filename, $export_content);
		}
	}
}


?>