<?php
// Using:

################## Change Log [Start] ##############
#
#   Date:   2019-06-28  Bill
#           Term Selection > Change 'termId' to 'TargetTerm' to prevent IntegerSafe()
#
################## Change Log [End] ##############

// set memory limit to 750M, else no data return
@SET_TIME_LIMIT(1500);
@ini_set('memory_limit', '750M');

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();
 
include_once($PATH_WRT_ROOT."includes/libboxplot.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

// check access right
$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();

// init
$libox = new libboxplot();
$class_list = array();
$reportClassLevelAry = array();
$reportIDColumnMapping = array();
$reportIDColumnMapping[0] = $Lang['eReportCard']['ReportArr']['MarkAnalysisArr']['Test'];
$reportIDColumnMapping[1] = $Lang['eReportCard']['ReportArr']['MarkAnalysisArr']['DailyMark'];
$reportIDColumnMapping[2] = $Lang['eReportCard']['ReportArr']['MarkAnalysisArr']['Exam'];
$reportIDColumnMapping[3] = $eReportCard["Overall"];

// POST data
//$classLevelId = $_POST['ClassLevelID'];
//$reportId = $_POST['ReportID'];
//$reportColumnIdAry = $_POST['ReportColumnIDAry'];
//$classIdAry = $_POST['classIdAry'];
//$subjectIdAry = $_POST['subjectIdAry'];
$reportType = $_POST['reportType'];
$viewFormat = $_POST['ViewFormat'];
// $TermID = $_POST['termId'];
$TermID = $_POST['TargetTerm'];
if($TermID != 'F') {
    $TermID = IntegerSafe($TermID);
}
$ReportColumnNumAry = $_POST['ReportColumnNumAry'];
$TargetID = $_POST['TargetID'];
$YearIDArr = $_POST['YearIDArr'];
$SubjectIDArr = $_POST['SubjectIDArr'];
$fullMarkPercentage = $_POST['fullMarkPercentage'];

// form id list
if($reportType=="class"){
	$YearIDArr = array();
	
	for($i=0; $i<count($TargetID); $i++){
		$currentLevelID = $lreportcard->Get_ClassLevel_By_ClassID($TargetID[$i]);
		$YearIDArr[] = $currentLevelID;
	}
	$YearIDArr = array_filter(array_unique($YearIDArr));
}
else if($reportType=="form"){
	$YearIDArr = $TargetID;
}

// get report list
$reportList = BuildMultiKeyAssoc($lreportcard->Get_Report_List("", ($TermID=="F" ? "W" : "T")), array("ClassLevelID", "Semester", "ReportID"));

// semester name
$semesterName = $lreportcard->returnSemesters($TermID,"en");
if($TermID=="F") $semesterName = "Whole Year";

// semester sequence
$term_num = $lreportcard->Get_Semester_Seq_Number($TermID);
if($TermID=="F") $term_num = "F";

// build preset array - for subject group display
$reportDataAry = array();
if($reportType == 'sg'){
	include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
	
	foreach($SubjectIDArr as $currentSubjectID){
		
		$thisSubjectObj = new subject($currentSubjectID);
		$thisComponentArr = $thisSubjectObj->Get_Subject_Component();
		$numOfCompSubject = count($thisComponentArr);

		for($i=0; $i<4; $i++){
			$reportDataAry[$i][$currentSubjectID] = array();
			
			for($j=0; $j<$numOfCompSubject; $j++){
				$reportDataAry[$i][$thisComponentArr[$j]["RecordID"]] = array();
			}
		}
	}
}
		
if(count($reportList)>0){
	// loop reports
	foreach($reportList as $reportClassLevel => $mainReportContent){

		// report not in selected forms / empty array 
		if(!in_array($reportClassLevel, $YearIDArr) || count($mainReportContent)==0)
			continue;

		// loop report column
		foreach($mainReportContent as $reportSemID => $semReports){				

			// report not in selected forms / empty array
			if($reportSemID!=$TermID || count($semReports)==0)
				continue;
	
			// loop report
			foreach($semReports as $reportId => $reportBasicInfo){
				
				// report not main report
				if(!$reportBasicInfo['isMainReport'])
					continue;
				
				// get selected report information
				$reportId = $reportBasicInfo["ReportID"];
				
				// subject with order
				$subjectAry = $lreportcard->returnSubjectwOrder($reportClassLevel, 0, '', $SubjectFieldLang='en', $ParDisplayType='Desc', $ExcludeCmpSubject=0, $reportId);
				
				// class level
				$reportClassLevelID = $reportBasicInfo["ClassLevelID"];
				$reportClassLevelAry[$reportId] = $lreportcard->GET_FORM_NUMBER($reportClassLevelID, 1);
				
				// build subject id array to query statistics
				$subjectIdAry = array();
				foreach($subjectAry as $currentIDs => $ParentSubject){
					if($reportType == 'sg' && !in_array($currentIDs, $SubjectIDArr))
						continue;
					
					$subjectIdAry[] = $currentIDs;
					
					if(count($ParentSubject)>1){
						foreach($ParentSubject as $componentIds => $ComponentSubject){
							if($componentIds!=0)
								$subjectIdAry[] = $componentIds;
						}
					}
				}
				
				// class, form, subject group list
				$form_name[$reportId] = $lreportcard->returnClassLevel($reportClassLevel);
				if ($reportType == 'sg') {
					$forSubjectGroup = true;
					// get subject group
					$subjectGroupAssoAry[$reportId] = $lreportcard->Get_Subject_Group_List_Of_Report($reportId, '', $subjectIdAry, $IncludeSubject=true);
				}
				else {
					$forSubjectGroup = false;
					if ($reportType == 'class') {
						// get class
						$class_list[$reportId] = $lreportcard->GET_CLASSES_BY_FORM($classLevelId, "", "", 1);
					}
				}
				
				// get report column name
				$reportColumnAssoAry = $lreportcard->returnReportColoumnTitle($reportId);
//				$reportColumnAssoAry[0] = "Overall";
				$reportColumnIdAry = array_keys($reportColumnAssoAry);
	
				// get mark statistics
				//$MarkStatisticsArr[$thisReportColumnID][$thisSubjectID][$thisClassGroupID] = array() of data
				$markStatisticsAry = $lreportcard->Get_Mark_Statistics_Info($reportId, $subjectIdAry, $Average_Rounding=1, $SD_Rounding=1, $Percentage_Rounding=1, $SubjectMarkRounding=0, $IncludeClassStat=1, $reportColumnIdAry, $forSubjectGroup, $fullMarkPercentage);
				// get overall mark statistics
				$markStatisticsAry[0] = $lreportcard->Get_Mark_Statistics_Info($reportId, $subjectIdAry, $Average_Rounding=1, $SD_Rounding=1, $Percentage_Rounding=1, $SubjectMarkRounding=0, $IncludeClassStat=1, 0, $forSubjectGroup, $fullMarkPercentage);

				// build report data array
				foreach ((array)$markStatisticsAry as $_reportColumnId => $_columnDataAry) {
					/*	
					 * 	Return Report Column ID location
					 * 	(Term 1)
					 *	0 - Test
					 *	1 - Daily Mark
					 *	2 - Exam 
					 * 	(Term 2)
					 *	0 - Daily Mark
					 *	1 - Exam
					 */
					// get Report Column ID location
					$reportColumnCount = array_search($_reportColumnId, $reportColumnIdAry);
					// handle Overall Report Column
					if($_reportColumnId==0){
						$reportColumnCount = 3;
					}
					// 2nd Term Report - handle Report Column (as no Report Column - Test)
					else if($term_num==2){
						$reportColumnCount++;
					}
					// Consolidated Report - only show Overall Report Column
					else if($term_num=="F"){
						continue;
					}

					// report column not selected
					if($_reportColumnId && !in_array(($reportColumnCount+1), $ReportColumnNumAry) || !$_reportColumnId && !in_array(0, $ReportColumnNumAry)){
						continue;
					}
					
					foreach ((array)$_columnDataAry as $__subjectId => $__subjectDataAry) {
						foreach ((array)$__subjectDataAry as $__classGroupId => $___classGroupDataAry) {
							
							// skip class data
							if ($reportType == 'form' && $__classGroupId != 0) {
								continue;
							}
							// skip overall data
							if (($reportType == 'class' || $reportType == 'sg') && $__classGroupId == 0) {
								continue;
							}
							
							/*
							 * $__classGroupId: 'class' mode => $classId
							 * 					'form' mode => always zero
							 * 					'sg' mode => $subjectGroupId
							 */
							$___tmpAry = array();
							$___tmpAry['mean'] = $___classGroupDataAry['Average_Rounded'];
							$___tmpAry['sd'] = $___classGroupDataAry['SD_Rounded'];
							$___tmpAry['max'] = $___classGroupDataAry['MaxMark'];
							$___tmpAry['min'] = $___classGroupDataAry['MinMark'];
							$___tmpAry['q1'] = $___classGroupDataAry['LowerQuartile'];
							$___tmpAry['q2'] = $___classGroupDataAry['Median'];
							$___tmpAry['q3'] = $___classGroupDataAry['UpperQuartile'];
							$___tmpAry['passingRate'] = $___classGroupDataAry['Nature']['TotalPassPercentage'];
							$___tmpAry['markPassingRate'] = $___classGroupDataAry['Nature_'.$fullMarkPercentage]['TotalPassPercentage'];
							$___tmpAry['noOfStudent'] = $___classGroupDataAry['NumOfStudent'];
							
							$libox->setMinimum($___classGroupDataAry['MinMark']);
							$libox->setMaximum($___classGroupDataAry['MaxMark']);
							$libox->setQ1($___classGroupDataAry['LowerQuartile']);
							$libox->setQ2($___classGroupDataAry['Median']);
							$libox->setQ3($___classGroupDataAry['UpperQuartile']);
							
							$___tmpAry['Graph'] = $libox->returnBoxPlotContentHtml();
						
							if($reportType == 'sg')	{
								$reportDataAry[$reportColumnCount][$__subjectId][$reportId][$__classGroupId] = $___tmpAry;
							} 
							else {
								$reportDataAry[$reportColumnCount][$__classGroupId][$reportId][$__subjectId] = $___tmpAry;
							}
						}
					}
				}
			}
		}
	}
}

// for report header
$year_name = $lreportcard->GET_ACTIVE_YEAR_NAME("en");
if ($reportType == 'form') {
//	$type_name = $Lang['General']['Class'];
	$type_name = "Form";
}
else if ($reportType == 'class') {
//	$type_name = $Lang['General']['Class'];
	$type_name = "Class";
}
else if ($reportType == 'sg') {
//	$type_name = $Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'];
	$type_name = "Subject Group";
}

if($viewFormat == "pdf")
{
	// Create mPDF object
	require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");
	ob_clean(); 
	$pdf_obj = new mPDF('','A4',0,'',10,10,5,5,5,0);
	
	// mPDF Setting
	$pdf_obj->setAutoTopMargin = 'stretch';
	//split 1 table into 2 pages add border at the bottom
	$pdf_obj->splitTableBorderWidth = 0.1; 

	// Report Header
	$html = "";
	$pdf_obj->DefHTMLHeaderByName("titleHeader", $html);
	$pdf_obj->SetHTMLHeaderByName("titleHeader");	
}

$font_style_pre = "";
$font_style_post = "";

// css
$css = "<style type='text/css'>
			.main_container { display:block; width:725px; /* height:850px; */ margin:auto; /*border:1px solid #CCC;*/ padding:20px 20px; /* position:relative; */ clear:both; border:0px solid green;}
			.page_break { page-break-after:always; }
			.report_header { height:60px; vertical-align:top; }
			.report_header td { font-size:15px; text-align:center }
			.border_table th, .border_table td { font-weight:normal; text-align:center; }
			.border_table th { vertical-align:top; }
			.border_table .subject_name_title { font-weight:bold; text-align:left; }
			.border_table .first_col { text-align:left; }
		</style>";

// css for pdf file
if($viewFormat == "pdf")
{
	$css_html = "<head>";
		$css_html .= "<style TYPE='text/css'>
						body {
							font-family: msjh;
							font-size: 9pt;
						}	
						.border_table{
							border-collapse:collapse;
							border:1px solid #000000;
						}
						.border_table tr td, .border_table tr th{
							border:1px solid #000000;
						}
						.page_break { 
							page-break-after:always; 
							margin:0; 
							padding:0; 
							line-height:0; 
							font-size:0; 
							height:0; 
						}
					</style>";
		$css_html .= $css ;
	$css_html .= "</head>";
	
	$pdf_obj->writeHTML($css_html);
	$pdf_obj->writeHTML("<body>");
	
	$font_style_pre = "<font face='dejavuserifcondensed'>";
	$font_style_post = "</font>";
}

$report_table = "";
$header = array();
$rows = array();
if($reportType == 'sg')
{
	foreach((array)$reportDataAry as $reportColumnNum => $reportColumnAry){
		
		$column_name = $reportIDColumnMapping[$reportColumnNum];
		
		foreach((array)$reportColumnAry as $curSubjectID => $reportSubjectDataAry){
			
			// subject handling
			$cur_subject_name = $lreportcard->GET_SUBJECT_NAME_LANG($curSubjectID, 'en');
			$isComponent = $lreportcard->GET_PARENT_SUBJECT_ID($curSubjectID);
			if($isComponent){
				$curSubjectID = $isComponent;
				$cur_subject_name = $lreportcard->GET_SUBJECT_NAME_LANG($curSubjectID, 'en').' - '.$cur_subject_name;
			}
			
			// if subject / parent subject not selected / empty array
			if(!in_array($curSubjectID,$SubjectIDArr) || count($reportSubjectDataAry)==0)
				continue;
				
			$isFirst = true;

			foreach((array)$reportSubjectDataAry as $currentReportID => $currentSubjectDataAry){
				
				// empty array
				if(count($currentSubjectDataAry)==0)
					continue;
					
				// new table if from F3 to F4 
				$lastFormNumber = $currentFormNumber;
				$currentFormNumber = $reportClassLevelAry[$currentReportID];
				$newTableForF4 = !$isFirst && $currentFormNumber!=$lastFormNumber && $currentFormNumber>3 && $lastFormNumber<=3;
				
				// current form is F1 - F3 and subject is component subject [removed]
//				if($currentFormNumber<=3 && $isComponent)
//					continue;

				// report title
				$report_title = "$year_name $semesterName $column_name Mark Analysis (By $type_name)";
				$cur_subject_group_list = $subjectGroupAssoAry[$currentReportID][$curSubjectID];
				$cur_subject_group_list = BuildMultiKeyAssoc((array)$cur_subject_group_list, array("SubjectGroupID"));
				
				$isFirstSG = true;
			
				foreach((array)$currentSubjectDataAry as $curSubjectGroupID => $currentSubjectGroupAry)
				{
					// subject group not in current subject / subject group not selected
					if(!in_array($curSubjectGroupID, array_keys((array)$cur_subject_group_list)) || !in_array($curSubjectGroupID, $TargetID))
						continue;
					
					$cur_subject_group_name = $cur_subject_group_list[$curSubjectGroupID]['ClassTitleEN'];
					
					if($isFirstSG){
						
						// table header - first table of subject in correct report column / new table for F4 Subject
						if($isFirst || $newTableForF4){
							
							if($newTableForF4){
								$report_table .= "</table>";
								$report_table .= "</div>";
								
								if($viewFormat == "pdf"){
									$pdf_obj->writeHTML($report_table);
									$report_table = " ";
									$rows = array();
								}
							}
							
							// line break
							$page_break = $viewFormat=="pdf"? '' : 'page_break';
							if($report_table != ""){
								$rows[] = array("");
								if($viewFormat == "pdf"){
									$report_table .= "<p class=\"page_break\" clear=\"all\" />";
								}
							}
							
							$report_table .= "<div class='main_container $page_break'>";
								$report_table .= "<table class='report_header' width='100%' border='0' cellspacing='0' cellpadding='4' align='center' valign='top'>";
								$report_table .= "<tr>";
									$report_table .= "<td width='100%'><u>$report_title</u></td>";
								$report_table .= "</tr>";
								$report_table .= "</table>";
								$report_table .= "<table class='border_table' width='100%' border='0' cellspacing='0' cellpadding='4' align='center' valign='top'>";
								$report_table .= "<tr>";
									$report_table .= "<th class='subject_name_title' colspan='12'>$cur_subject_name</th>";
								$report_table .= "</tr>";
								$report_table .= "<tr>";
									$report_table .= "<th width='15%' class='first_col'>".$form_name[$currentReportID]."</th>";
									$report_table .= "<th width='6%'>Mean</th>";
									$report_table .= "<th width='6%'>S.D.</th>";
									$report_table .= "<th width='6%'>Max.</th>";
									$report_table .= "<th width='6%'>Min.</th>";
									$report_table .= "<th width='6%'>Q<sub>1</sub></th>";
									$report_table .= "<th width='6%'>Q<sub>2</sub></th>";
									$report_table .= "<th width='6%'>Q<sub>3</sub></th>";
									$report_table .= "<th width='7%'>A-E</th>";
									$report_table .= "<th width='7%'>Mark $font_style_pre&ge;$font_style_post$fullMarkPercentage%</th>";
									$report_table .= "<th width='8%'>No. of Students</th>";
									// ensure enough width for correct box plot display
									$report_table .= "<th width='130px'>".$libox->returnBoxPlotHeaderHtml()."</th>";
								$report_table .= "</tr>";
								
							if(count($header)==0)
								$header = array("$report_title");
							else
								$rows[] = array("$report_title");
	
							$rows[] = array("$cur_subject_name");
							$rows[] = array($form_name[$currentReportID],"Mean","S.D.","Max.","Min.","Q1","Q2","Q3","A-E","Mark ≥ $fullMarkPercentage%","No. of Students","&nbsp;");
							
							$isFirst = false;
							$newTableForF4 = false;
						}
						// header for another forms in the same table
						else {
							$report_table .= "<tr>";
								$report_table .= "<th width='15%' class='first_col' style='padding-bottom:15px;'>".$form_name[$currentReportID]."</th>";
								$report_table .= "<th width='6%'>&nbsp;</th>";
								$report_table .= "<th width='6%'>&nbsp;</th>";
								$report_table .= "<th width='6%'>&nbsp;</th>";
								$report_table .= "<th width='6%'>&nbsp;</th>";
								$report_table .= "<th width='6%'>&nbsp;</th>";
								$report_table .= "<th width='6%'>&nbsp;</th>";
								$report_table .= "<th width='6%'>&nbsp;</th>";
								$report_table .= "<th width='7%'>&nbsp;</th>";
								$report_table .= "<th width='7%'>&nbsp;</th>";
								$report_table .= "<th width='8%'>&nbsp;</th>";
								$report_table .= "<th width='21%'>&nbsp;</th>";
							$report_table .= "</tr>";
							
							$rows[] = array($form_name[$currentReportID],"&nbsp;","&nbsp;","&nbsp;","&nbsp;","&nbsp;","&nbsp;","&nbsp;","&nbsp;","&nbsp;","&nbsp;","&nbsp;");
						}
						$isFirstSG = false;
					}
					
					// table content row
					$report_table .= "<tr>";
					$report_table .= "<td class='first_col'>$cur_subject_group_name</td>";
					$row = array();
					$row[] = "$cur_subject_group_name";
					foreach((array)$currentSubjectGroupAry as $content_type => $currentSubjectGroupContent)
					{
						$currentSubjectGroupContent = ($content_type=="passingRate" || $content_type=="markPassingRate")? "$currentSubjectGroupContent%" : $currentSubjectGroupContent;
						$report_table .= "<td>$currentSubjectGroupContent</td>";
						$row[] = $content_type=="Graph"? "&nbsp;" : "$currentSubjectGroupContent";
					}
					$report_table .= "</tr>";
					$rows[] = $row;
				}
			}
			
			// close the table
			if(!$isFirst)
			{
				$report_table .= "</table>";
				$report_table .= "</div>";
				
				if($viewFormat == "pdf")
				{
					$pdf_obj->writeHTML($report_table);
					$report_table = " ";
					$rows = array();
				}
			}
		}
	}
}
else
{	
	foreach((array)$reportDataAry as $reportColumnNum => $reportColumnAry){
		
		// column name
		$column_name = $reportIDColumnMapping[$reportColumnNum];
				
		foreach((array)$reportColumnAry as $currentClassID => $reportClassDataAry){
			
			// class not in selected classes
			if(($reportType == 'class' && !in_array($currentClassID,(array)$TargetID)) || count($reportClassDataAry)==0)
				continue;
			
			foreach((array)$reportClassDataAry as $currentReportID => $currentReportDataAry){
				
				if(count($currentReportDataAry) == 0)
					continue;
				
				// report title
				$report_title = "$year_name $semesterName $column_name Mark Analysis (By $type_name)";
				
				// class / form name
				if($currentClassID>0){
					$tablename = $class_list[$currentReportID][$currentClassID]["ClassTitleEN"];
				}
				else {
					$tablename = $form_name[$currentReportID];
				}
					
				// line break
				$page_break = $viewFormat=="pdf"? '' : 'page_break';
				if($report_table != ""){
					$rows[] = array("");
					if($viewFormat == "pdf"){
						$report_table .= "<p class=\"page_break\" clear=\"all\" />";
					}
				}
					
				// report header
				$report_table .= "<div class='main_container'>";
					$report_table .= "<table class='report_header' width='100%' border='0' cellspacing='0' cellpadding='4' align='center' valign='top'>";
					$report_table .= "<tr>";
						$report_table .= "<td width='100%'><u>$report_title</u></td>";
					$report_table .= "</tr>";
					$report_table .= "</table>";
					$report_table .= "<table class='border_table' width='100%' border='0' cellspacing='0' cellpadding='4' align='center' valign='top'>";
					$report_table .= "<tr>";
						$report_table .= "<th width='21%' class='first_col'>$tablename</th>";
						$report_table .= "<th width='6%'>Mean</th>";
						$report_table .= "<th width='6%'>S.D.</th>";
						$report_table .= "<th width='6%'>Max.</th>";
						$report_table .= "<th width='6%'>Min.</th>";
						$report_table .= "<th width='6%'>Q<sub>1</sub></th>";
						$report_table .= "<th width='6%'>Q<sub>2</sub></th>";
						$report_table .= "<th width='6%'>Q<sub>3</sub></th>";
						$report_table .= "<th width='7%'>A-E</th>";
						$report_table .= "<th width='7%'>Mark $font_style_pre&ge;$font_style_post$fullMarkPercentage%</th>";
						$report_table .= "<th width='8%'>No. of Students</th>";
						// ensure enough width for correct box plot display
						$report_table .= "<th width='130px'>".$libox->returnBoxPlotHeaderHtml()."</th>";
					$report_table .= "</tr>";
					
				if(count($header)==0){
					$header = array("$report_title");
				}
				else{
					$rows[] = array("$report_title");
				}
				//$rows[] = array("$tablename");
				$rows[] = array("$tablename","Mean","S.D.","Max.","Min.","Q1","Q2","Q3","A-E","Mark ≥ $fullMarkPercentage%","No. of Students","&nbsp;");
				
				foreach((array)$currentReportDataAry as $curSubjectID => $currentSubjectClassAry)
				{
					// subject handling
					$cur_subject_name = $lreportcard->GET_SUBJECT_NAME_LANG($curSubjectID, 'en');
					$isComponent = $lreportcard->GET_PARENT_SUBJECT_ID($curSubjectID);
					$cur_subject_name = $isComponent? "&nbsp; -$cur_subject_name" : $cur_subject_name;
					
					$report_table .= "<tr>";
					$report_table .= "<td class='first_col'>$cur_subject_name</td>";
					$row = array();
					$row[] = "$cur_subject_name";
					
					foreach((array)$currentSubjectClassAry as $content_type => $currentSubjectClassContent)
					{
						$currentSubjectClassContent = ($content_type=="passingRate" || $content_type=="markPassingRate")? "$currentSubjectClassContent%" : $currentSubjectClassContent;
						$report_table .= "<td>$currentSubjectClassContent</td>";
						$row[] = $content_type=="Graph"? "&nbsp;" : "$currentSubjectClassContent";
					}
					$report_table .= "</tr>";
					$rows[] = $row;
				}
				
				$report_table .= "</table>";
				$report_table .= "</div>";
					
				if($viewFormat == "pdf")
				{
					$pdf_obj->writeHTML($report_table);
					$report_table = " ";
					$rows = array();
				}
			}
		}
	}
}

if ($viewFormat == 'html') {
	$linterface = new interface_html();
	
	$x = '';
	$x .= '<table border="0" cellpadding="4" width="95%" cellspacing="0" class="print_hide">'."\n";
		$x .= '<tr><td align="right">'.$linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit2").'</span></td></tr>'."\n";
	$x .= '</table>'."\n";
	$html['printBtn'] = $x;
	
	include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/print_header_erc.php");
	echo $lreportcard->Get_GrandMS_CSS();
	echo $css;
	
	echo $html['printBtn'];
	
	echo $report_table;
	
	echo $html['printBtn'];
	
	include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/print_footer_erc.php");
}
else if ($viewFormat == 'csv')
{
	// export object
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	$lexport = new libexporttext();
	
	$exportContent .= $lexport->GET_EXPORT_TXT($rows, $header);
	$lexport->EXPORT_FILE("MarkAnalysis.csv", $exportContent);
} 
else if ($viewFormat == 'pdf')
{
	
	$pdf_obj->writeHTML("</body>");
	
	// Export PDF file
	$pdf_obj->Output('MarkAnalysis.pdf', 'I');
	//$pdf_obj->Output('ProcessTime_'.$ProcessTime.'_MemoryUsage_'.number_format((memory_get_usage())/1024/1024, 1).'MB.pdf', 'I');
}

intranet_closedb();
?>