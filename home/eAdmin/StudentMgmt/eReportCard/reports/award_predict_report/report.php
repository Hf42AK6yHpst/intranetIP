<?php
# Editing by 

/****************************************************
 * Modification log
 *
 *  20191101 Bill   [2019-1030-0914-49066]
 *      - Set Up Main / Competition Subject from settings
 * 	20170519 Bill
 * 		- Allow View Group User to access
 * 	20170427 Bill
 * 		- Support Multiple Level Selection
 * 	20170419 Bill:	[2017-0109-1818-40164]
 * 		- Create File
 *  
 * **************************************************/

@SET_TIME_LIMIT(21600);
@ini_set(memory_limit, -1);

$PageRight = array("ADMIN", "VIEW_GROUP");

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

// Access right
if (!$plugin['ReportCard2008']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_extrainfo.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
}
else {
	$lreportcard = new libreportcard();
}
$lreportcard_ui = new libreportcard_ui();

// Access right
if (!$eRCTemplateSetting['Report']['AwardPrediction'] || !$lreportcard->hasAccessRight()) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

// Get Target Semester Number
$SemNumber = 0;
if($PredictType==1) {
	$SemNumber = 2;
}
else if($PredictType==2) {
	$SemNumber = 3;
	$isYearReport = true;
}

$formStudentInfoArr = array();
$AcademicAwardStudentAry = array();
$ConductAwardStudentAry = array();

// loop Class Level
foreach((array)$YearID as $ClassLevelID)
{
	// Get Form Info
	$SchoolType	= $lreportcard->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
	$FormNumber = $lreportcard->GET_FORM_NUMBER($ClassLevelID, 1);
	$isSecondaryGraduate = $SchoolType=="S" && $FormNumber==6;

    // [2019-1030-0914-49066] Set Up Main / Competition Subject
    $lreportcard->Set_Main_Competition_Subject_List($ClassLevelID);

	// Get Form Students
	$StudentInfoArr = $lreportcard->GET_STUDENT_BY_CLASSLEVEL("", $ClassLevelID, $ParClassID="", $ParStudentID="", $ReturnAsso=0, $isShowStyle=0);
	$formStudentInfoArr = array_merge((array)$StudentInfoArr, $formStudentInfoArr);
	$StudentIDArr = Get_Array_By_Key($StudentInfoArr, "UserID");
	
	$numOfStudent = count($StudentIDArr);
	if($SemNumber > 0 && $numOfStudent > 0)
	{
		$isAllGenerated = true;
		
		// Get Year Report
		$thisYearReportInfo = $lreportcard->returnReportTemplateBasicInfo("", $others="", $ClassLevelID, "F", $isMainReport=1);
		$thisYearReportID = $thisYearReportInfo["ReportID"];
		
		// Get Semester
		$SemesterList = getSemesters($lreportcard->schoolYearID, 0);
		
		// Completed Third Term Academic Result (for Confirmation) only
		if($isYearReport)
		{
			// Check if Year Report Generated
			$thisReportGenDate = $thisYearReportInfo["LastGenerated"];
			if($thisReportGenDate=="0000-00-00 00:00:00" || $thisReportGenDate=="") {
				continue;
			}
				
			// Get Year Main Subject
			$YearMainSubjectArray = $lreportcard->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID="", $SubjectFieldLang="", $ParDisplayType="Desc", $ExcludeCmpSubject=0, $thisYearReportID);
			if(sizeof($YearMainSubjectArray) > 0) {
				$YearMainSubjectIDAry = array();
				$YearMainSubjectNameArray = array();
				foreach($YearMainSubjectArray as $YearMainSubjectIDAry[] => $YearMainSubjectNameArray[]);
			}
			
			// Get Year Marks
			$YearMarksAry = $lreportcard->getMarks($thisYearReportID, "", $cons="", $ParentSubjectOnly=1);
				
			// Get Mark-up Exam Result
			$thisMarkUpResult = $lreportcard->GET_EXTRA_SUBJECT_INFO($StudentIDArr, "", $thisYearReportID);
		}
		
		// Get Subject Code Mapping
		$SubjectCodeMapping = $lreportcard->GET_SUBJECT_SUBJECTCODE_MAPPING();
		
		// Get Conduct List
		$lreportcard_extrainfo = new libreportcard_extrainfo();
		$ConductArr = $lreportcard_extrainfo->Get_Conduct();
		$ConductArr = BuildMultiKeyAssoc($ConductArr, "ConductID", "Conduct", 1);
		
		// loop Students
		foreach((array)$StudentIDArr as $thisStudentID)
		{
			if(!$isAllGenerated) {
				break;
			}
			
			$SemAcademicExcellent = 0;
			$SemConductExcellent = 0;
			$ConductDataAry = array();
			$thisStudentList = array($thisStudentID);
			
			// loop Semester
			for($j=0; $j<$SemNumber; $j++)
			{
				// Get Semester Report
				$thisSemester = $SemesterList[$j]["YearTermID"];
				$thisReportInfo = $lreportcard->returnReportTemplateBasicInfo("", $others="", $ClassLevelID, $thisSemester, $isMainReport=1);
				$thisReportID = $thisReportInfo["ReportID"];
				
				// Check if Semester Report Generated
				$thisReportGenDate = $thisReportInfo["LastGenerated"];
				if($thisReportGenDate=="0000-00-00 00:00:00" || $thisReportGenDate=="") {
					$isAllGenerated = false;
					break;
				}
				
				// Get Main Subject
				$MainSubjectArray = $lreportcard->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID="", $SubjectFieldLang="", $ParDisplayType="Desc", $ExcludeCmpSubject=1, $thisReportID);
				if(sizeof($MainSubjectArray) > 0) {
					$MainSubjectIDAry = array();
					$MainSubjectNameArray = array();
					foreach($MainSubjectArray as $MainSubjectIDAry[] => $MainSubjectNameArray[]);
				}
				
				// Get Marks
				$SemMarksAry = $lreportcard->getMarks($thisReportID, "", $cons="", $ParentSubjectOnly=1);
				$SemMarksAry = $SemMarksAry[$thisStudentID];
				if($isSecondaryGraduate) {
					$thisMarkUpResult[$thisStudentID] = $SemMarksAry;
				}
				
				// Get Conduct
				$StudentExtraInfo = $lreportcard_extrainfo->Get_Student_Extra_Info($thisReportID, $thisStudentList);
				$StudentConductID = BuildMultiKeyAssoc($StudentExtraInfo, "StudentID", "ConductID", 1);
				$StudentConductID = $StudentConductID[$thisStudentID];
				$ConductData = $ConductArr[$StudentConductID];
				$ConductDataAry[$j] = $ConductData;
				
				// Get Award in Comments
				list($goodAcademic, $excellentAcademic, $excellentConduct) = $lreportcard->returnReceivedCommentAward($SchoolType, $FormNumber, $SemMarksAry, $MainSubjectIDAry, $ConductData, $SubjectCodeMapping);
				$excellentConduct = strpos($ConductData, "A")!==false;
				
				// Excellent Academic Award in Each Term
				if($excellentAcademic)
					$SemAcademicExcellent++;
				
				// Excellent Conduct (at least Grade A-)
				if($excellentConduct)
					$SemConductExcellent++;
			}
			
			// Execellent Academic Award (Whole Year)
			$YearAcademicAward = $SemAcademicExcellent==$SemNumber;
			if($YearAcademicAward) {
				$AcademicAwardStudentAry[] = $thisStudentID;
			}
			
			// Completed Third Term Academic Result (for Confirmation)
			if($isYearReport)
			{
				// Get Year Marks
				$YearStudentMarksAry = $YearMarksAry[$thisStudentID];
				
				// Get Mark-up Exam Marks
				$thisMarkUpResult = $thisMarkUpResult[$thisStudentID];
				
				// Receive Execellent Conduct Award (Whole Year)
				$YearConductAward = $lreportcard->returnReceivedConductAward($YearStudentMarksAry, $thisMarkUpResult, $YearMainSubjectIDAry, $ConductDataAry, $SubjectCodeMapping, $isSecondaryGraduate);
				if($YearConductAward) {
					$ConductAwardStudentAry[] = $thisStudentID;
				}
			}
			// Completed Second Term Academic Result (for Prediction)
			else {
				$YearConductAward = $SemConductExcellent==$SemNumber;
				if($YearConductAward) {
					$ConductAwardStudentAry[] = $thisStudentID;
				}
			}
		}
	}
}

// Get Student Info
$formStudentInfoArr = BuildMultiKeyAssoc((array)$formStudentInfoArr, "UserID");

// Display Students
$HasAcademicAwardStudent = count($AcademicAwardStudentAry) > 0;
$HasConductAwardStudent = count($ConductAwardStudentAry) > 0;
if($HasAcademicAwardStudent || $HasConductAwardStudent)
{
	// Prediction - Execellent Academic Award
	if($HasAcademicAwardStudent)
	{
		$ReportTitle = $isYearReport? "獲得全年學業成績優異獎" : "第一、二段均獲得學業優異生";
		$ReportHeader = "<div class='main_container'>";
		$ReportHeader .= "	<table class='report_header' width='100%'>
							<tr>
								<td class='report_title' colspan='3'><b>$ReportTitle</b></td>
							</tr>
							<tr>
								<td width='12%'>班級</td>
								<td width='12%'>學號</td>
								<td width='76%'>學生姓名</td>
							</tr>";
		$x .= $ReportHeader;
		
		// loop Student
		$CurrentCount = 1;
		$AcademicAwardStudentCount = count($AcademicAwardStudentAry);
		foreach($AcademicAwardStudentAry as $thisStudentID)
		{
			$thisStudentInfo = $formStudentInfoArr[$thisStudentID];
			$thisStudentClassName = $thisStudentInfo["ClassTitleCh"];
			$thisStudentClassNumber = $thisStudentInfo["ClassNumber"];
			$thisStudentStudentName = $thisStudentInfo["StudentNameCh"];
			
			$x .= "	<tr>
						<td>$thisStudentClassName</td>
						<td>$thisStudentClassNumber</td>
						<td>$thisStudentStudentName</td>
					</tr>";
			
			// Page break
			$CurrentCount++;
			if($CurrentCount % 30 == 0 && $AcademicAwardStudentCount > $CurrentCount) {
				$x .= "	</table>";
				$x .= "	</div>";
				$x .= "<div style='page-break-after:always;'></div>";
				
				$x .= $ReportHeader;
			}
		}
		$x .= "	</table>";
		$x .= "	</div>";
		
		// Page break
		if($HasConductAwardStudent)
			$x .= "<div style='page-break-after:always;'></div>";
	}
	
	// Prediction - Execellent Conduct Award
	if($HasConductAwardStudent)
	{
		$ReportTitle = $isYearReport? "獲得全年操行成績優良獎" : "第一、二段均獲得操行都達A-或以上";
		$ReportHeader = "<div class='main_container'>";
		$ReportHeader .= "	<table class='report_header' width='100%'>
							<tr>
								<td class='report_title' colspan='3'><b>$ReportTitle</b></td>
							</tr>
							<tr>
								<td width='12%'>班級</td>
								<td width='12%'>學號</td>
								<td width='76%'>學生姓名</td>
							</tr>";
		$x .= $ReportHeader;
		
		// loop Student
		$CurrentCount = 1;
		$ConductAwardStudentCount = count($ConductAwardStudentAry);
		foreach($ConductAwardStudentAry as $thisStudentID) {
			$thisStudentInfo = $formStudentInfoArr[$thisStudentID];
			$thisStudentClassName =  $thisStudentInfo["ClassTitleCh"];
			$thisStudentClassNumber = $thisStudentInfo["ClassNumber"];
			$thisStudentStudentName = $thisStudentInfo["StudentNameCh"];
			
			$x .= "	<tr>
						<td>$thisStudentClassName</td>
						<td>$thisStudentClassNumber</td>
						<td>$thisStudentStudentName</td>
					</tr>";
			
			// Page break
			$CurrentCount++;
			if($CurrentCount % 30 == 0 && $ConductAwardStudentCount > $CurrentCount) {
				$x .= "	</table>";
				$x .= "	</div>";
				$x .= "<div style='page-break-after:always;'></div>";
				
				$x .= $ReportHeader;
			}
		}
		$x .= "	</table>";
		$x .= "	</div>";
	}
}
else if (!$isAllGenerated)
{
	$x .= "	<table class='report_header' cellspacing='0' cellpadding='5' border='0' width='100%'>
			<tr>
				<td style='border-left: 0px solid black; font-size: 20px;' align='center'>".$eReportCard["ReportNotGenerated"]."</td>
			</tr>
			</table>";
}
else
{
	$x .= "	<table class='report_header' cellspacing='0' cellpadding='5' border='0' width='100%'>
			<tr>
				<td style='border-left: 0px solid black; font-size: 20px;' align='center'>$no_record_msg</td>
			</tr>
			</table>";
}
	
echo $x;

intranet_closedb();
?>

<style type="text/css">
@charset "utf-8";
/* CSS Document */
.main_container { display: block; width: 773px; height: 920px; margin: auto; padding: 30px 20px; position: relative; }
.main_container .report_header td { font-size: 16px; font-family: "Arial", "PMingLiU", "Lucida Console"; padding: 4px; }
.main_container .report_header td.report_title { font-size: 18px; font-family: "Arial", "PMingLiU", "Lucida Console"; text-align: left; }
.main_container .result_table  { border-collapse: collapse; margin-top: 8px; }
.main_container .result_table td { font-size: 14px; font-family: "Arial", "PMingLiU", "Lucida Console"; text-align: center; }
.page_break { page-break-after: always; margin: 0; padding: 0; line-height: 0; font-size: 0; height: 0; }
</style>