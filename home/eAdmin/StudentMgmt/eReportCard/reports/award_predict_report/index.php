<?php
# Editing by Bill 

/***************************************************
 * 	Modification log:
 * 
 * 	20170519 Bill
 * 		- Allow View Group User to access
 * 	
 * 	20170427 Bill
 * 		- Support Multiple Level Selection
 * 
 * 	20170419 Bill:	[2017-0109-1818-40164]
 * 		- Create File
 *  
 * *************************************************/

$PageRight = array("ADMIN", "VIEW_GROUP");

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcard();
$lreportcard_ui = new libreportcard_ui();

if(!$plugin['ReportCard2008'] || !$eRCTemplateSetting['Report']['AwardPrediction'] || !$lreportcard->hasAccessRight())
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	
	intranet_closedb();
	exit;
}

$linterface = new interface_html();

$lclass = new libclass();

# Get Form selection (show form which has report card only)
$libForm = new Year();
$FormArr = $libForm->Get_All_Year_List();
$YearID = $YearID? $YearID : $FormArr[0]['YearID'];
$select_list = $lreportcard_ui->Get_Form_Selection("YearID[]", $YearID, "js_Reload();", $noFirst=1, $isAll=0, $hasTemplateOnly=1, $checkPermission=0, $IsMultiple=1);
		
//# Get Levels and Classes
//$levels = $lclass->getLevelArray();
//$classes = $lclass->getClassList($lreportcard->schoolYearID);
//$select_list = "<SELECT name=\"TargetID[]\">";
//if ($level==1)
//{
//    for ($i=0; $i<sizeof($levels); $i++)
//    {
//	    list($id,$name) = $levels[$i];
//		$select_list .= "<OPTION value='".$id."' SELECTED>".$name."</OPTION>";
//    }
//}
//else
//{
//    for ($i=0; $i<sizeof($classes); $i++)
//    {
//         list($id, $name, $lvl) = $classes[$i];
//		$select_list .= "<OPTION value='".$id."' SELECTED>".$name."</OPTION>";
//    }
//}
//$select_list .= "</SELECT>";

# Get Predict Type Selection
$select_list2 = "<SELECT name=\"PredictType\">";
$select_list2 .= "<OPTION value='1' SELECTED>".$Lang['eReportCard']['ReportArr']['AwardPredictionArr']['Predict']."</OPTION>";
$select_list2 .= "<OPTION value='2'>".$Lang['eReportCard']['ReportArr']['AwardPredictionArr']['Confirm']."</OPTION>";
$select_list2 .= "</SELECT>";

# Tag Information
$CurrentPage = "Reports_AwardPrediction";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($eReportCard['Reports_AwardPrediction'], "", 0);

$linterface->LAYOUT_START();
?>

<script>
function submitForm(format)
{
	$('input#format').val(format);
	
	ObjForm = document.getElementById('FormMain');
	if(format == 'csv') {
		ObjForm.action = 'report.php';
		ObjForm.target = '_self';
		ObjForm.submit();
	}
	else {
		ObjForm.action = 'report.php';
		ObjForm.target = '_blank';
		ObjForm.submit();
	}
}
</script>

<br/>
<form id="FormMain" name="FormMain" method="post" action="report.php">
  	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  	<tr>
      	<td class="tab_underline"><?=$display_tagstype?></td>
    </tr>
  	</table>
	<table class="form_table_v30">
		<!-- Year -->
		<tr valign="top">
			<td class="field_title"><?=$Lang['General']['SchoolYear']?></td>
			<td><?=$lreportcard->GET_ACTIVE_YEAR_NAME()?></td>
		</tr>
		<!-- Form -->
		<tr valign="top">
			<td class="field_title"><span class="tabletextrequire">*</span><?=$eReportCard['Form'] ?></td>
			<td><?=$select_list?></td>
		</tr>
		<!-- Type -->
		<tr valign="top">
			<td class="field_title"><span class="tabletextrequire">*</span><?=$eReportCard['Type'] ?></td>
			<td><?=$select_list2?></td>
		</tr>
	</table>
	
	<br>
	<?=$linterface->MandatoryField();?>
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "submitForm('');")?>
		&nbsp;
		<!--<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Export'], "button", "submitForm('csv');")?>-->
	</div>
		
	<input type="hidden" name="format" id="format" value="pdf">
</form>
<br />
<br />

<?
	$linterface->LAYOUT_STOP();
?>