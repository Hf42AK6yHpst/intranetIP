<?php
@SET_TIME_LIMIT(600);
# using: 

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//$ReportCardCustomSchoolName = "st_stephen_college";
intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	if (is_file($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName."_assessment.php"))
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName."_assessment.php");
	else
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/general_assessment.php");
	
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

$linterface = new interface_html();

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

# Get the eRC Template Settings
include_once($PATH_WRT_ROOT."includes/eRCConfig.php");
$eRCtemplate = $lreportcard ;
$eRtemplateCSS = $lreportcard->getAssessmentTemplateCSS();

//$ReportSetting = $lreportcard->returnReportTemplateBasicInfo($ReportID);
//$ClassLevelID = $ReportSetting['ClassLevelID'];

include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/print_header.php");
?>

<? if($eRtemplateCSS) {?><link href="<?=$eRtemplateCSS?>" rel="stylesheet" type="text/css" /><? } ?>

<style type='text/css'>
html, body { margin: 0px; padding: 0px; }
</style>
<style type='text/css' media='print'>
	.print_hide {display:none;}
</style>
<table border="0" cellpadding="4" width="95%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span><br /></td></tr>
</table>
<?


//$TargetStudentList = (is_array($TargetStudentID)) ? implode(",", $TargetStudentID) : "";
//$StudentIDAry = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $ClassID);
//for($s=0;$s<sizeof($StudentIDAry);$s++)
//	$StudentIDAry[$s] = $StudentIDAry[$s]['UserID'];
//$StudentIDAry = $StudentID;

//prepare Student Rank Score Array
if($ReportCardCustomSchoolName=="kiansu_chekiang_college")
	$StudentRankScoreAry = $eRCtemplate->Get_Assessment_Report_Rank_Arr($ReportID,$ClassLevelID,$DataSource,$StudentIDAry);

# $StudentIDAry was passed from generate.php
for($s=0;$s<sizeof($StudentIDAry);$s++)
{
	$StudentID = $StudentIDAry[$s];
	if($StudentRankScoreAry[$StudentID]["isAllNA"]===true)
		continue;

		$TitleTable = $eRCtemplate->getReportHeader($ReportID);
		
		if (method_exists($eRCtemplate, "getReportStudentInfo")) $StudentInfoTable = $eRCtemplate->getReportStudentInfo($ReportID, $StudentID, $IssueDate);
		if (method_exists($eRCtemplate, "getAssessmentMSTable")) $MSTable = $eRCtemplate->getAssessmentMSTable($ReportID, $Assessment, $Display, $StudentID, $StudentRankScoreAry[$StudentID], $DataSource);
		//$MiscTable = $eRCtemplate->getMiscTable($ReportID, $StudentID);
		if (method_exists($eRCtemplate, "getAssessmentRemarkTable")) $AssessmentTable = $eRCtemplate->getAssessmentRemarkTable($ReportID, $StudentID,$Assessment);
		if (method_exists($eRCtemplate, "getSignatureTable")) $SignatureTable = $eRCtemplate->getSignatureTable($ReportID, $StudentID, $IssueDate);
		if (method_exists($eRCtemplate, "getFooter")) $FooterRow = $eRCtemplate->getFooter($ReportID);
	
		//if($s==0) echo "<br>";
	?>
	<div id="container" <? if($s<sizeof($StudentIDAry)-1) {?>style="page-break-after:always"<? } ?>>
		<!-- the margin must be set to 5mm, 1086px = A4 size(297mm) - margin(10mm) -->
		<table height="1000px" width="100%" border="0" cellpadding="0" cellspacing="0" valign="top" style='padding:0; margin:0;'>
		<?
			if (method_exists($eRCtemplate, "getLayout")) {
				echo $eRCtemplate->getLayout($TitleTable, $StudentInfoTable, $MSTable, $AssessmentTable, $SignatureTable, $FooterRow, $ReportID, $StudentID);
			} else {
			?>
				<tr><td><?=$TitleTable?></td><tr>
				<tr><td><?=$StudentInfoTable?></td><tr>
				<tr><td><?=$AssessmentTable?></td><tr>
				<tr><td><?=$MiscTable?></td><tr>
				<tr><td><?=$SignatureTable?></td><tr>
				<?=$FooterRow?>
		<? } ?>
		</table>
	</div>
	<?
}
?>
<table border="0" cellpadding="0" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>