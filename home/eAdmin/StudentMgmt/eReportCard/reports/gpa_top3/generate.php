<?
# Editing by  

/****************************************************
 * Modification log
 *  20190620 Bill:
 *      Term Selection > Change 'TermID' to 'TargetTerm' to prevent IntegerSafe()
 * 	20180115 Bill:	[2018-0112-1108-32164]
 * 		Hide Students excluded from Ranking in Report	($eRCTemplateSetting['Report']['GPA_Top3']['ExcludeRankingOrderStudents'])
 * 	20170519 Bill:
 * 		Allow View Group to access
 * **************************************************/

$PageRight = array("ADMIN", "VIEW_GROUP");

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$ViewFormat = standardizeFormPostValue($_POST['ViewFormat']);
$TermID = standardizeFormPostValue($_POST['TargetTerm']);
if($TermID != 'F') {
    $TermID = IntegerSafe($TermID);
}
$Level = standardizeFormPostValue($_POST['Level']);
$TargetIDArr = $_POST['TargetID'];
$Criteria = standardizeFormPostValue($_POST['CriteriaSelected']);
$reportType = standardizeFormPostValue($_POST['reportType']);
$ReportColumnID = 0;

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	}
	else {
		$lreportcard = new libreportcard();
	}
	
	if ($lreportcard->hasAccessRight()) 
	{
		# $GPA_PrizeInfoArr[$StudentID][Position, Mark ...] = value
		$GPA_PrizeInfoArr = array();
		
		if ($eRCTemplateSetting['Report']['GPA_Top3']['MainOrExtraReportOption'])
		{
			if ($reportType == 'main') {
				$mainReportOnly = 1;
				$extraReportOnly = 0;
				$columnTitle = $Lang['eReportCard']['Exam'].' ';
			}
			else {
				$mainReportOnly = 0;
				$extraReportOnly = 1;
				$columnTitle = $Lang['eReportCard']['Test'].' ';
			}
		}
		else
		{
			// normal client => get from main report only
			$mainReportOnly = 1;
			$extraReportOnly = 0;
		}
		
		# Report Title
		//$ActiveYear = $lreportcard->GET_ACTIVE_YEAR("-");
		$ObjAcademicYear = new academic_year($lreportcard->schoolYearID);
		$ActiveYear = $ObjAcademicYear->Get_Academic_Year_Name();
		$SemTitle = $lreportcard->returnSemesters($TermID);
		$ReportType = $eReportCard['Reports_GPATop3Rank'];
		$ReportTitle = $ActiveYear." ".$SemTitle." ".$columnTitle.$ReportType;
		
		$numOfTarget = count($TargetIDArr);
		for ($i=0; $i<$numOfTarget; $i++)
		{
			# Get the Student List of each Target form / class
			if ($Level == "Class")
			{
				$thisClassID = $TargetIDArr[$i];
				$thisClassLevelInfoArr = $lreportcard->Get_ClassLevel_By_ClassID($thisClassID);
				if (is_array($thisClassLevelInfoArr)) {
					$thisClassLevelID = $thisClassLevelInfoArr['ClassLevelID'];
				}
				else {
					$thisClassLevelID = $thisClassLevelInfoArr;
				}
				$thisOrderField = 'OrderMeritClass';
				
			}
			else if ($Level == "Form")
			{
				$thisClassLevelID = $TargetIDArr[$i];
				$thisClassID = '';
				$thisOrderField = 'OrderMeritForm';
			}
			
			# Get Class Level Name
			$thisClassLevelName = $lreportcard->returnClassLevel($thisClassLevelID);
			
			# Get Report Info
			$thisReportInfoArr = $lreportcard->GET_REPORT_TYPES($thisClassLevelID, $ReportID="", $TermID, $mainReportOnly, $orderBySubmissionTime=false, $withinSubmissionPeriod=false, $extraReportOnly);
			$thisReportID = $thisReportInfoArr[0]['ReportID'];
				
			# Get Student List
			$thisStudentInfoArr = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($thisReportID, $thisClassLevelID, $thisClassID);
			// ignore class / form with no student
			if (!is_array($thisStudentInfoArr) || count($thisStudentInfoArr)==0) {
				continue;
			}
			
			# Construct StudentID List
			$thisStudentIDArr = array();
			$numOfStudent = count($thisStudentInfoArr);
			for ($j=0; $j<$numOfStudent; $j++) {
				$thisStudentID = $thisStudentInfoArr[$j]['UserID'];
				$thisStudentIDArr[] = $thisStudentID;
			}
			// ignore class / form with no student
			if (!is_array($thisStudentInfoArr) || count($thisStudentInfoArr)==0) {
				continue;
			}
			
			# Get Students excluded from Ranking
			$excludeStudentIDArr = array();
			if($eRCTemplateSetting['Report']['GPA_Top3']['ExcludeRankingOrderStudents']) {
				$excludeStudentIDArr = $lreportcard->GET_EXCLUDE_ORDER_STUDENTS($thisReportID);
				$excludeStudentIDArr = array_filter($excludeStudentIDArr);
			}
			// Condition
			$excludeStudentCond = "";
			if (is_array($excludeStudentIDArr) && count($excludeStudentIDArr) > 0) {
				$excludeStudentCond = " AND StudentID NOT IN ('".implode("', '", $excludeStudentIDArr)."') ";
			}
			
			# Get GPA
			$thisStudentIDList = implode(", ", $thisStudentIDArr);
			$cons = " 	AND StudentID IN ($thisStudentIDList) $excludeStudentCond
						AND $Criteria != ''
						ORDER BY $Criteria DESC; ";
			$StudentResultArr = $lreportcard->getReportResultScore($thisReportID, $ReportColumnID, '', $cons);
			// ignore if no result generated
			if (!is_array($StudentResultArr) || count($StudentResultArr)==0)
				continue;
			
			$rankCounter = 0;
			$lastMark = 0;
			$tmpSameMarkCount = 0;
			foreach ($StudentResultArr as $thisStudentID => $thisStudentResultArr)
			{
				$thisStudentColumnResultArr = $thisStudentResultArr[$ReportColumnID];
				$thisMark = my_round($thisStudentColumnResultArr[$Criteria], 2);
				
				if ($thisMark != $lastMark)
				{
					if ($eRCTemplateSetting['RankingMethod']=='1223') {
						$rankCounter++;
					}
					else {
						$rankCounter = $rankCounter + 1 + $tmpSameMarkCount;
					}
					$tmpSameMarkCount = 0;
				}
				if ($thisMark == $lastMark) {
					$tmpSameMarkCount++;
				}
				
				if ($rankCounter > $maxRank) {
					break;
				}
				
				$thisOrder = $rankCounter;
				
				$GPA_PrizeInfoArr[$thisStudentID]['Mark'] = $thisMark;
				$GPA_PrizeInfoArr[$thisStudentID]['Order'] = $thisOrder;
				$GPA_PrizeInfoArr[$thisStudentID]['ClassLevelName'] = $thisClassLevelName;
				
				$lastMark = $thisMark;
			}
		}
		
		## Display the result
		if ($ViewFormat == 'html')
		{
			$table = '';
			$table .= "<table id='ResultTable' class='GrandMSContentTable' width='100%' border='1' cellpadding='2' cellspacing='0'>\n";
				$table .= "<thead>\n";
					$table .= "<tr>\n";
						$table .= "<th>#</th>\n";
						$table .= "<th>".$eReportCard['Template']['Position']."</th>\n";
						$table .= "<th>".$eReportCard['FormName']."</th>\n";
						$table .= "<th>".$eReportCard['Class']."</th>\n";
						$table .= "<th>".$eReportCard['StudentNo_short']."</th>\n";
						$table .= "<th>".$eReportCard['StudentNameEn']."</th>\n";
						$table .= "<th>".$eReportCard['StudentNameCh']."</th>\n";
						$table .= "<th>".$eReportCard['Template'][$Criteria]."</th>\n";
					$table .= "</tr>\n";
				$table .= "</thead>\n";
				
				$table .= "<tbody>\n";
				$counter = 0;
					foreach ($GPA_PrizeInfoArr as $StudentID => $StudentInfoArr)
					{
						$counter++;
						
						$lu = new libuser($StudentID);
						//$thisClassName = $lu->ClassName;
						//$thisClassNumber = $lu->ClassNumber;
						$thisEnglishName = $lu->EnglishName;
						$thisChineseName = $lu->ChineseName;
						
						$thisClassInfoArr = $lreportcard->Get_Student_Class_ClassLevel_Info($StudentID);
						$thisClassName = $thisClassInfoArr[0]['ClassName'];
						$thisClassNumber = $thisClassInfoArr[0]['ClassNumber'];
						
						$thisClassLevelName = $StudentInfoArr['ClassLevelName'];
						$thisOrder = $StudentInfoArr['Order'];
						$thisMark = $StudentInfoArr['Mark'];
						
						$table .= "<tr>\n";
							$table .= "<td align='center'>".$counter."</td>\n";
							$table .= "<td align='center'>".$thisOrder."</td>\n";
							$table .= "<td align='center'>".$thisClassLevelName."</td>\n";
							$table .= "<td align='center'>".$thisClassName."</td>\n";
							$table .= "<td align='center'>".$thisClassNumber."</td>\n";
							$table .= "<td align='center'>".$thisEnglishName."</td>\n";
							$table .= "<td align='center'>".$thisChineseName."</td>\n";
							$table .= "<td align='center'>".$thisMark."</td>\n";
						$table .= "</tr>\n";
					}
				$table .= "</tbody>\n";
			$table .= "</table>\n";
			
			$css = $lreportcard->Get_GrandMS_CSS();
			
			$allTable = '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center"><h1>'.$ReportTitle.'</h1></td>
							</tr>
							<tr>
								<td>'.$table.'</td>
							</tr>
						</table>';
		
		echo $lreportcard->Get_Report_Header($ReportTitle);
		echo $allTable.$css;
		echo $lreportcard->Get_Report_Footer();
	}
	else if ($ViewFormat == 'csv')
	{
			include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
			include_once($PATH_WRT_ROOT."includes/libexporttext.php");
			
			$ExportHeaderArr = array();
			$ExportContentArr = array();
			$lexport = new libexporttext();
			
			# Header
			$ExportHeaderArr[] = $eReportCard['Template']['Position'];
			if ($eRCTemplateSetting['Report']['GPA_Top3']['ExportUserLogin']) {
				$ExportHeaderArr[] = $Lang['General']['UserLogin'];
			}
			$ExportHeaderArr[] = $eReportCard['FormName'];
			$ExportHeaderArr[] = $eReportCard['Class'];
			$ExportHeaderArr[] = $eReportCard['StudentNo_short'];
			$ExportHeaderArr[] = $eReportCard['StudentNameEn'];
			$ExportHeaderArr[] = $eReportCard['StudentNameCh'];
			$ExportHeaderArr[] = $eReportCard['Template']['Mark'];
			
			# Content
			$i_counter = 0;
			foreach ($GPA_PrizeInfoArr as $StudentID => $StudentInfoArr)
			{
				$j_counter = 0;
				
				$lu = new libuser($StudentID);
				//$thisClassName = $lu->ClassName;
				//$thisClassNumber = $lu->ClassNumber;
				$thisEnglishName = $lu->EnglishName;
				$thisChineseName = $lu->ChineseName;
				
				$thisStudentInfoArr = $lreportcard->Get_Student_Class_ClassLevel_Info($StudentID);
				$thisClassName = $thisStudentInfoArr[0]['ClassName'];
				$thisClassNumber = $thisStudentInfoArr[0]['ClassNumber'];
				
				$thisClassLevelName = $StudentInfoArr['ClassLevelName'];
				$thisOrder = $StudentInfoArr['Order'];
				$thisMark = $StudentInfoArr['Mark'];
				
				$ExportContentArr[$i_counter][$j_counter++] = $thisOrder;
				if ($eRCTemplateSetting['Report']['GPA_Top3']['ExportUserLogin']) {
					$ExportContentArr[$i_counter][$j_counter++] = $lu->UserLogin;
				}
				$ExportContentArr[$i_counter][$j_counter++] = $thisClassLevelName;
				$ExportContentArr[$i_counter][$j_counter++] = $thisClassName;
				$ExportContentArr[$i_counter][$j_counter++] = $thisClassNumber;
				$ExportContentArr[$i_counter][$j_counter++] = $thisEnglishName;
				$ExportContentArr[$i_counter][$j_counter++] = $thisChineseName;
				$ExportContentArr[$i_counter][$j_counter++] = $thisMark;
				
				$i_counter++;
			}
			
			// Title
			$ReportTitle = str_replace(" ", "_", $ReportTitle);
			$filename = $ReportTitle.".csv";
			
			$export_content = $lexport->GET_EXPORT_TXT($ExportContentArr, $ExportHeaderArr);
			intranet_closedb();
			
			// Output the file to user browser
			$lexport->EXPORT_FILE($filename, $export_content);
		}
	}
}
?>