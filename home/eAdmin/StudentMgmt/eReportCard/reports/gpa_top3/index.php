<?php
# Editing by  

/****************************************************
 * Modification log
 *  20190620 Bill:
 *      Term Selection > Change 'TermID' to 'TargetTerm' to prevent IntegerSafe()
 * 	20170519 Bill:
 * 		Allow View Group to access
 * **************************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "VIEW_GROUP");

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libclass.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	}
	else {
		$lreportcard = new libreportcard();
	}
	$linterface = new interface_html();
	$lclass = new libclass();
	
	if ($lreportcard->hasAccessRight())
	{
		# Get Levels and Classes
		$levels = $lclass->getLevelArray();
		$classes = $lclass->getClassList($lreportcard->schoolYearID);
		
		$select_list = "<SELECT name=\"TargetID[]\" MULTIPLE SIZE=\"5\">";
		if ($level==1)
		{
		    for ($i=0; $i<sizeof($levels); $i++)
		    {
			    list($id, $name) = $levels[$i];
				$select_list .= "<OPTION value='".$id."' SELECTED>".$name."</OPTION>";
		    }
		}
		else
		{
		    for ($i=0; $i<sizeof($classes); $i++)
		    {
                list($id, $name, $lvl) = $classes[$i];
				$select_list .= "<OPTION value='".$id."' SELECTED>".$name."</OPTION>";
		    }
		}
		$select_list .= "</SELECT>";
		
		# Term Selection
		$TermInfoArr = $lreportcard->returnReportTemplateTerms();
		// $TermSel_tag = 'name="TermID" id="TermID"';
		$TermSel_tag = 'name="TargetTerm" id="TargetTerm"';
		$TermSelection = getSelectByArray($TermInfoArr, $TermSel_tag, "", $all=0, $noFirst=1);
		
		# Ranking Selection 
		$RankingSelection = "<select name='maxRank' id='maxRank'>";
		for($i=1; $i<=100; $i++) {
			$RankingSelection .= "<option value='$i' ".($i==3?"SELECTED":"").">$i</option>";
		}
		$RankingSelection .= "</select>";
		 
		# Criteria Selection
		$CriteriaSelection = $lreportcard->Get_Order_Criteria_Selection("CriteriaSelected");
		
		# Main or extra term report radio button
		$htmlAry['testRadio'] = $linterface->Get_Radio_Button('reportType_extra', 'reportType', $Value='extra', $isChecked=1, $Class="", $Lang['eReportCard']['Test']);
		$htmlAry['examRadio'] = $linterface->Get_Radio_Button('reportType_main', 'reportType', $Value='main', $isChecked=0, $Class="", $Lang['eReportCard']['Exam']);
		
		############## Interface Start ##############
		$linterface = new interface_html();
		$CurrentPage = "Reports_GPATop3Rank";
		$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
		
		# Tag information
		$TAGS_OBJ[] = array($eReportCard['Reports_GPATop3Rank']);
		$linterface->LAYOUT_START();
?>

<script language="javascript">
function changeType(form_obj,lvl_value)
{
         obj = form_obj.elements["TargetID[]"]
         
         <? # Clear existing options ?>
         while (obj.options.length > 0)
         {
                obj.options[0] = null;
         }
         if (lvl_value=='Form')
         {
             <?
             for ($i=0; $i<sizeof($levels); $i++)
             {
                  list($id,$name) = $levels[$i];
                  ?>
                  obj.options[<?=$i?>] = new Option('<?=str_replace("'", "\'", $name)?>',<?=$id?>);
                  <?
             }
             ?>
         }
         else
         {
             <?
             for ($i=0; $i<sizeof($classes); $i++)
             {
                  list($id,$name, $lvl_id) = $classes[$i];
                  ?>
                  obj.options[<?=$i?>] = new Option('<?=str_replace("'", "\'", $name)?>',<?=$id?>);
                  <?
             }
             ?>
         }
         SelectAll(obj);
}

function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}

function checkForm()
{
	var obj = document.form1;
	var select_obj = obj.elements['TargetID[]'];
	
	for (var i=0; i<select_obj.length; i++)
	{
		if (select_obj[i].selected)
		{
			return true;
		}
	}
	
	alert('<?=$i_alert_PleaseSelectClassForm?>');
	return false;
}
</script>

<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT."templates/2007a/css/"?>ereportcard.css">

<br />
<form name="form1" action="generate.php" method="POST" onsubmit="return checkForm();" target="_blank">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
<table>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td></td>
					<td align="right"><?=$linterface->GET_SYS_MSG($Result);?></td>
				</tr>
				
				<!-- View Format -->
				<tr id="ViewFormatRow">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ViewFormat'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="ViewFormat" id="ViewHTML" value="html" CHECKED>
							<label for="ViewHTML"><?=$eReportCard['HTML']?></label>
						</input>
						<input type="radio" name="ViewFormat" id="ViewCSV" value="csv">
							<label for="ViewCSV"><?=$eReportCard['CSV']?></label>
						</input>
					</td>
				</tr>
				
				<!-- Term -->
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['Term'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<?= $TermSelection ?>
						<?php if ($eRCTemplateSetting['Report']['GPA_Top3']['MainOrExtraReportOption']) { ?>
							<br>
							<?=$htmlAry['testRadio']?>
							<?=$htmlAry['examRadio']?>
						<?php } ?>
					</td>
				</tr>
				
				<!-- Class / Form -->
				<tr class="tabletext">
					<td class="formfieldtitle" valign="top" width="30%"><?="$i_Discipline_Class/$i_Discipline_Form"?></td>
					<td>
						<SELECT name="Level" onChange="changeType(this.form,this.value)">
							<OPTION value="Class" <?=$level!=1?"SELECTED":""?>><?=$i_Discipline_Class?></OPTION>
							<OPTION value="Form" <?=$level==1?"SELECTED":""?>><?=$i_Discipline_Form?></OPTION>
						</select>
					</td>
				</tr>
				
				<!-- Target Class / Form Selection -->
				<tr class="tabletext">
					<td class="formfieldtitle" valign="top"><?=$i_ClassName ."/".$i_ClassLevel?> <span class="tabletextrequire">*</span></td>
					<td><?=$select_list?>
						<?= $linterface->GET_SMALL_BTN($button_select_all, "button", "javascript:SelectAll(document.form1.elements['TargetID[]'])"); ?>
					</td>
				</tr>
				
				<!-- Criteria -->
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['Criteria'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<?= $CriteriaSelection ?>
					</td>
				</tr>
				
				<!-- Max Ranking -->
				<tr class="tabletext">
					<td class="formfieldtitle" valign="top"><?=$eReportCard['maxRanking']?> </td>
					<td><?=$RankingSelection?></td>
				</tr>
				
				<tr>
					<td colspan="2" class="dotline">
						<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submitBtn"); ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<?
	print $linterface->FOCUS_ON_LOAD("form1.ClassLevelID");
  	$linterface->LAYOUT_STOP();
    } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>

<?
}
?>