<?php
# Editing by 

@SET_TIME_LIMIT(21600);
@ini_set(memory_limit, -1);

/****************************************************
 * Modification log
 *  20191101 Bill:  [2019-1030-0914-49066]
 *      - Set Up Main / Competition Subject from settings
 *  20190503 Bill
 *      - Term Selection > Change 'TermID' to 'TargetTerm' to prevent IntegerSafe()
 *  20180406 Bill
 *      - fixed cannot check mark nature if students excluded from ranking
 * 	20170519 Bill
 * 		- Allow View Group User to access
 * 	20170428 Bill:	[2017-0109-1818-40164]
 * 		- Updated
 * 	20160704 Bill:	[2015-1104-1130-08164]
 * 		- Create File
 * **************************************************/

$PageRight = array("ADMIN", "VIEW_GROUP");

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

// Access Right Checking
if (!$plugin['ReportCard2008']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

# Initial
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_extrainfo.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
}
else {
	$lreportcard = new libreportcard();
}
$lreportcard_ui = new libreportcard_ui();

// Access Right Checking
if (!$eRCTemplateSetting['Report']['SubjectStat'] || !$lreportcard->hasAccessRight()) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

# Initial Last Year Object
$LastYearID = $lreportcard->Get_Previous_YearID_By_Active_Year();
$lreportcardObj = new libreportcard($LastYearID);

$fcm = new form_class_manage();

# Get POST Data
$MasterReportSetting = $_POST;

/* Special Handling for Term due to IntegerSafe() */
$MasterReportSetting['TermID'] = $MasterReportSetting['TargetTerm'];
if($MasterReportSetting['TermID'] != 'F' && $MasterReportSetting['TermID'] != 'Exam') {
    $MasterReportSetting['TermID'] = IntegerSafe($MasterReportSetting['TermID']);
}
unset($MasterReportSetting['TargetTerm']);

# Get School, Form and Class
$ClassID = $MasterReportSetting["TargetID"][0];
$YearClassIDArr = array($ClassID);
$ClassLevelID = $lreportcard->Get_ClassLevel_By_ClassID($ClassID);
$SchoolType	= $lreportcard->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ClassLevelID);
$FormNumber = $lreportcard->GET_FORM_NUMBER($ClassLevelID, $ByWebSAMSCode=1);
$isGraduateLevel = ($SchoolType == "P" && $FormNumber == 6) || ($SchoolType == "S" && $FormNumber == 3) || ($SchoolType == "S" && $FormNumber == 6);
$isSecondaryGraduate = $SchoolType == "S" && $FormNumber == 6;
$MasterReportSetting["TargetID"] = $ClassID;
$MasterReportSetting["YearClassIDArr"] = $YearClassIDArr;
$MasterReportSetting["ClassLevelID"] = $ClassLevelID;
$MasterReportSetting["SchoolType"] = $SchoolType;
$MasterReportSetting["FormNumber"] = $FormNumber;
$MasterReportSetting["isGraduateLevel"] = $isGraduateLevel;
$MasterReportSetting["isSecondaryGraduate"] = $isSecondaryGraduate;

# Get Semester
$TermID = $MasterReportSetting["TermID"];
$SemesterNum = $lreportcard->Get_Semester_Seq_Number($TermID);
$isYearReport = $TermID=="F";
$isGraduateTerm = $isSecondaryGraduate && $TermID=="Exam";
$MasterReportSetting["TermID"] = $TermID;
$MasterReportSetting["SemesterNum"] = $SemesterNum;
$MasterReportSetting["isYearReport"] = $isYearReport;
$MasterReportSetting["isGraduateTerm"] = $isGraduateTerm;

// [2019-1030-0914-49066] Set Up Main / Competition Subject
$lreportcard->Set_Main_Competition_Subject_List($ClassLevelID);

// if Class Level not S6 and Term is Graduate Exam => exit
if(!$isSecondaryGraduate && $TermID=="Exam") {
	echo $eReportCard["SubjStatReport"]["GradExamInappropriate"];
	exit();
}

// Handle Graduate Exam
if($isGraduateTerm) {
	// Get Current Term
	$ReportSemesterAry = $lreportcard->GET_ALL_SEMESTERS($SemID="-1", $haveReportTemplate=false);
	$ReportSemesterAry = array_keys($ReportSemesterAry);
	$TermID = $ReportSemesterAry[2];
	
	// Get Year Report
	$thisYearReportInfo = $lreportcard->returnReportTemplateBasicInfo("", $others="", $ClassLevelID, "F", $isMainReport=1);
	$thisYearReportID = $thisYearReportInfo["ReportID"];
}

# Get Report Template Info
$ReportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo("", $others="", $ClassLevelID, $TermID, $isMainReport=1);
$ReportID = $ReportTemplateInfo["ReportID"];
$MasterReportSetting["ReportID"] = $ReportID;

// if Report is not generated => exit
if(empty($ReportTemplateInfo["LastGenerated"])) {
	echo $eReportCard["ReportNotGenerated"];
	exit();
}

# Get Report Settings
$EmptySymbol = "--";
$SubjectDisplay = "desc";
$DisplayAllNASubject = 0;
$ApplyStyleToFailOnly = true;
$MasterReportSetting["BottomLineNumber"] = 5;
$MasterReportSetting["SubjectDisplay"] = $SubjectDisplay;
$MasterReportSetting["DisplayAllNASubject"] = $DisplayAllNASubject;
$MasterReportSetting["ApplyStyleToFailOnly"] = $ApplyStyleToFailOnly;

# Get Report Data Target
$StudentData = array("ClassNumber", "ChineseName");
$SubjectData = array("Mark");
if($isYearReport || $isGraduateTerm) {
	$GrandMarkDataAry = array("GrandAverage", "OrderMeritClass", "NoOfFailSubject", "FailSubjectUnit");
	$ExtraDataAry = array("PromotionType", "RetentionReason");
}
else {
	$GrandMarkDataAry = array("GrandAverage", "NoOfFailSubject");
	$ExtraDataAry = array("AwardType");
}
$StatisticData = array("FailingNumber", "FailingRate");
$ShowStyle = array("Mark", "GrandAverage");
$MasterReportSetting["StudentData"] = $StudentData;
$MasterReportSetting["SubjectData"] = $SubjectData;
$MasterReportSetting["GrandMarkDataAry"] = $GrandMarkDataAry;
$MasterReportSetting["ExtraDataAry"] = $ExtraDataAry;
$MasterReportSetting["StatisticData"] = $StatisticData;
$MasterReportSetting["ShowStyle"] = $ShowStyle;

# Get Report Column
$OverallColumnID = 0;
if($isYearReport)		// Year Report
{
	// Display Overall Column only
	$ReportColumnID = array(0);
	
	// Get Last Semester of Consolidate Report
	if($eRCTemplateSetting['Report']['MasterReport']['ExcludeDroppedStudent']){
		$LastTermID = $lreportcard->Get_Last_Semester_Of_Report($ReportID);
	}
	
	// Build Report Column Info
	$ReportColumnMapping = $lreportcard->returnReportTemplateColumnData($ReportID);
	$ReportColumnMapping = BuildMultiKeyAssoc((array)$ReportColumnMapping, "ReportColumnID", "SemesterNum");
	if(count($ReportColumnMapping) > 0){
		// Use Last Column as Overall Column Info
		$ReportColumnMapping[0] = end($ReportColumnMapping);
	}
	
	// Get related Term Report
	$RelatedTermReport = $lreportcard->Get_Related_TermReport_Of_Consolidated_Report($ReportID);
	
	// loop Term Report
	foreach((array)$RelatedTermReport as $thisTermReport){
		$thisTermReportID = $thisTermReport["ReportID"];
		$TermMarksAry[] = $lreportcard->getMarks($thisTermReportID, "", $cons="", $ParentSubjectOnly=1);
	}
}
else if ($isGraduateTerm)		// Graduate Exam Report
{
	// Get Report Column
	$ReportColumnID = $lreportcard->returnReportColoumnTitle($ReportID);
	$ReportColumnID = array_keys((array)$ReportColumnID);
	$ReportColumnID = end($ReportColumnID);
	$OverallColumnID = $ReportColumnID;
	
	// Build Report Column Info
	$ReportColumnMapping = array();
	foreach((array)$ReportColumnID as $thisColumnID) {
		$ReportColumnMapping[$thisColumnID]['SemesterNum'] = $TermID;
	}
}
else		// Term Report
{
	// Get Report Column
	$ReportColumnID = $lreportcard->returnReportColoumnTitle($ReportID);
	$ReportColumnID = array_keys((array)$ReportColumnID);

	// Build Report Column Info
	$ReportColumnMapping = array();
	foreach($ReportColumnID as $thisColumnID) {
		$ReportColumnMapping[$thisColumnID]['SemesterNum'] = $TermID;
		if(count($ReportColumnMapping)>=2)		break;
	}
	$ReportColumnID[] = 0;
}
$MasterReportSetting["ReportColumnID"] = $ReportColumnID;

# Get Main Subject
$MainSubjectList = ($SchoolType == "P" && $FormNumber <= 4)? $lreportcard->MainSubjectList2 : $lreportcard->MainSubjectList;

# Get Competition Subject
$CompletitionSubjectList = $lreportcard->CompletitionSubjectList;

# Get Competition Subject Display Limit
$CompletitionSubjectLimit = $lreportcard->CompletitionSubjectDisplayLimit;

# Get Subject Unit
$SubjectUnitList = $lreportcard->SubjectUnitList;

# Get Subject Code Mapping
$SubjectCodeMapping = $lreportcard->GET_SUBJECTS_CODEID_MAP(0, 0);

# Get Report Subject
$SubjectIDArr = $lreportcard->returnSubjectwOrder($ClassLevelID, $ParForSelection=0, $TeacherID="", $SubjectFieldLang="", $ParDisplayType="Desc", $ExcludeCmpSubject=1, $ReportID);
$SubjectIDArr = array_keys((array)$SubjectIDArr);
$SubjectCount = count($SubjectIDArr);
for($count=0; $count<$SubjectCount; $count++)
{
	// Get Subject Code
	$currentSubjectID = $SubjectIDArr[$count];
	$currentSubjectCode = $SubjectCodeMapping[$currentSubjectID];
	
	// Exclude Competition Subject 
	if(trim($currentSubjectCode)!="" && $CompletitionSubjectLimit[$currentSubjectCode] > 0)
		unset($SubjectIDArr[$count]);
}
$MasterReportSetting["SubjectIDArr"] = $SubjectIDArr;

# Get Report Student
$StudentList = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID);
$StudentIDList = Get_Array_By_Key((array)$StudentList, "UserID");

# Get Report Student Info
$StudentInfoList = BuildMultiKeyAssoc((array)$StudentList, "UserID");

# Get Sorted Report Student
$SortedUserID = $lreportcard->Sort_UserID_By_Class_ClassNumber($StudentList);

# Get Student Marks
$MarksAry = $lreportcard->getMarks($ReportID, "", $cons="", $ParentSubjectOnly=1);
if ($isGraduateTerm) {
	$GrandMarksAry = $lreportcard->getReportResultScore($ReportID, $ReportColumnID);
}
else {
	$GrandMarksAry = $lreportcard->getReportResultScore($ReportID);
}

# Get Marks Nature
$MarksNatureArr = $lreportcard->Get_Student_Subject_Mark_Nature_Arr($ReportID, $ClassLevelID, $MarksAry, $WithExtraData = false, $skipOrderCheckingInPercentageCalculation = true);
$GrandMarksNatureArr = $lreportcard->Get_Student_Grand_Mark_Nature_Arr($ReportID, $ClassLevelID, $GrandMarksAry, $skipOrderCheckingInPercentageCalculation = true);

# Get Mark-up Exam Mark
$MarkupExamAry = $lreportcard->GET_EXTRA_SUBJECT_INFO($StudentIDList, "", $ReportID);

# Get Grand Mark Grading Scheme 
$GrandMarkScheme = $lreportcard->Get_Grand_Mark_Grading_Scheme($ClassLevelID, $ReportID);

// Get Retented Subjects
$retentedSubjectAry = $lreportcard->GET_STUDENT_PAST_YEAR_PROMOTION_RETENTION_SUBJECT_RECORD("", "", "2");
$allRetentedSubjectIds = BuildMultiKeyAssoc((array)$retentedSubjectAry, array("StudentID", "SubjectID"), "SubjectID", 1);

# Get Conduct List
$lreportcard_extrainfo = new libreportcard_extrainfo();
$ConductArr = $lreportcard_extrainfo->Get_Conduct();
$ConductArr = BuildMultiKeyAssoc((array)$ConductArr, "ConductID", "Conduct", 1);

# Get Student Conduct Grade
$StudentExtraInfo = $lreportcard_extrainfo->Get_Student_Extra_Info($ReportID, $StudentIDList);
$StudentConductIDArr = BuildMultiKeyAssoc((array)$StudentExtraInfo, "StudentID", "ConductID", 1);

# Get Preset Promotion Status
$promotionStatusAry = $lreportcard->customizedPromotionStatus;

# Get Student Promotion Status
if ($isYearReport || $thisYearReportID) {
	$targetReportID = $thisYearReportID? $thisYearReportID : $ReportID;
	$reportPromotionStatus = $lreportcard->GetPromotionStatusList($ClassLevelID, $ClassID, "", $targetReportID);
}

# Get Last Year Student Promotion Status
$lastYearPromotionStatusAry = array();
$lastYearStudentReportMapping = array(); 
$sql = "SELECT
			report_result.ReportID, report_template.ClassLevelID, report_result.StudentID
		FROM
			".$lreportcardObj->DBName.".RC_REPORT_RESULT report_result
			INNER JOIN ".$lreportcardObj->DBName.".RC_REPORT_TEMPLATE report_template ON (report_result.ReportID = report_template.ReportID)
		WHERE
			report_result.StudentID IN ('".implode("', '", (array)$StudentIDList)."') AND report_result.ReportColumnID = '0' AND report_result.Semester = 'F' AND report_template.Semester = 'F'";
$LastYearStudentReportAry = $lreportcard->returnArray($sql);
if(!empty($LastYearStudentReportAry))
{
	foreach($LastYearStudentReportAry as $thisStudentReportInfo) {
		$thisStudentID = $thisStudentReportInfo["StudentID"];
		$thisStudentReportID = $thisStudentReportInfo["ReportID"];
		$thisStudentClassLevelID = $thisStudentReportInfo["ClassLevelID"];
		
		// Last Year Status
		if(empty($lastYearPromotionStatusAry[$thisStudentReportID])) {
			$lastYearPromotionStatusAry[$thisStudentReportID] = $lreportcardObj->GetPromotionStatusList($thisStudentClassLevelID, "", "", $thisStudentReportID);
		}
		$lastYearStudentReportMapping[$thisStudentID] = $thisStudentReportID;
	}
}

# Get Position Determine Field
$RankDetermineField = $lreportcard->Get_Grand_Position_Determine_Field();

// loop Classes
foreach((array)$YearClassIDArr as $thisYearClassID)
{
	# loop Students
	foreach((array)$SortedUserID as $thisStudentID) 
	{
		# Group by Class for Class Summary
		$thisClassID = $StudentInfoList[$thisStudentID]["YearClassID"];
		if($thisClassID != $thisYearClassID) continue;		// Skip if not in class
		
		# Get Student Retetned Subejcts
		$studentRetentedSubjectIds = $allRetentedSubjectIds[$thisStudentID];
		$thisOverallGradeNotConsidered = false;
		$hidePromotionStatus = false;
		
		###############	Student Info Data Start ################# 
			
			// loop Student Data Display
			foreach((array)$StudentData as $DisplayStudentData) {
				switch($DisplayStudentData)
				{
					case "ClassNumber":
						$StudentInfoData[$DisplayStudentData] = $StudentInfoList[$thisStudentID]["ClassNumber"];
					break;
					
					case "ChineseName":
						$StudentInfoData[$DisplayStudentData] = $StudentInfoList[$thisStudentID]["StudentNameCh"];
					break;
				}
			}
			
			# Store Student Info Data 
			$StudentDisplayAry[$thisYearClassID][$thisStudentID]["StudentInfoData"] = $StudentInfoData;
			
		################ Student Info Data End ################## 
		
		###############	Subject Marks Data & Statistic Process Start ################# 
			
			$SubjectMarksData = array();
			$StatStudentNatureArr = array();
			
			$StudentSubjectUnit = 0;
			$studentFailSubjectAry = array();
			$StudentWithFailSubject = false;
			$StudentWithFailSubjectCol = false;
			$StudentWithMarkupExam = false;
			
			// loop Subject
			foreach((array)$SubjectIDArr as $thisSubjectID)
			{
				// Init - Hide Subject with all N.A.
				if(!isset($isSubjectAllNA[$thisYearClassID][$thisSubjectID]))
					$isSubjectAllNA[$thisYearClassID][$thisSubjectID] = true;
				
				// Check Component Subject
				$thisSubjectIsComponent = $lreportcard->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($thisSubjectID);
				
				// Check Retented Subject
				$thisSubjectRetented = in_array($thisSubjectID, (array)$studentRetentedSubjectIds);
				
				# Get Subject Code
				$thisSubjectCode = $SubjectCodeMapping[$thisSubjectID];
	
				# Get Subject Marks
				$thisColumnMarksAry = $MarksAry[$thisStudentID][$thisSubjectID];
				
				# Get Grading Scheme
				if(!isset($GradingScheme[$thisSubjectID]))
					$GradingScheme[$thisSubjectID] = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $thisSubjectID, $withGrandResult=1, $returnAsso=0, $ReportID);
				$ScaleInput = $GradingScheme[$thisSubjectID]["ScaleInput"];
				$ScaleDisplay = $GradingScheme[$thisSubjectID]["ScaleDisplay"];
				
				// Not Considered Handling
				$displayNotConsideredSymbol = false;
				if($isYearReport)
				{
					// loop Term Marks
					foreach((array)$TermMarksAry as $thisTermMarks){
						$thisStudentTermMarks = $thisTermMarks[$thisStudentID][$thisSubjectID];
						foreach((array)$thisStudentTermMarks as $thisStudentTermColumnMarks)
						{
							$thisTermScoreGrade = $lreportcard->GET_MARKSHEET_SPECIAL_CASE_SET1_STRING($thisStudentTermColumnMarks["Grade"]);
							if($thisTermScoreGrade == $eReportCard['RemarkAbsentNotConsidered']) {
								$displayNotConsideredSymbol = true;
								break;
							}
						}
						if($displayNotConsideredSymbol)		break;
					}
				}
				
				// loop Report Column
				$isFirstColumn = true;
				foreach((array)$ReportColumnID as $thisReportColumnID)
				{
					// Check Student in Subject Group
					$StudentNotStudyingSubjectGroup = false;
					if($eRCTemplateSetting['Report']['MasterReport']['ExcludeDroppedStudent'])
					{
						// Overall Column of Year Report
						if($isYearReport && $thisReportColumnID=="0"){
							$StudentNotStudyingSubjectGroup = $LastTermID > 0 && $lreportcard->Get_Student_Studying_Subject_Group($LastTermID, $thisStudentID, $thisSubjectID)==NULL;
						} 
						else {
							$currentTermID = $ReportColumnMapping[$thisReportColumnID]['SemesterNum'];
							$StudentNotStudyingSubjectGroup = $currentTermID > 0 && $lreportcard->Get_Student_Studying_Subject_Group($currentTermID, $thisStudentID, $thisSubjectID)==NULL;
						}
					}
					
					#### Subject Marks Data ####
					
					# Get Report Column Marks
					$thisMarksAry = $thisColumnMarksAry[$thisReportColumnID];
					$thisColumnMark = $thisMarksAry["Mark"];
					$thisColumnGrade = $thisMarksAry["Grade"];
					$thisColumnGradeNotConsidered = $thisColumnGrade=="-";
					
					# Get Report Overall Marks
					$thisOverallMarkAry = array();
					if($isFirstColumn) {
						$thisOverallMarkAry = $thisColumnMarksAry[$OverallColumnID];
						$thisOverallMark = $thisOverallMarkAry["Mark"];
						$thisOverallGrade = $thisOverallMarkAry["Grade"];
					}
					
					# Get Report Column Mark Nature
					$thisColumnMarkNature = $MarksNatureArr[$thisStudentID][$thisSubjectID][$thisReportColumnID];
				
					# Get Report Overall Mark Nature
					$OverallMarkNature = $MarksNatureArr[$thisStudentID][$thisSubjectID][$OverallColumnID];
					
					// loop Subject Data Display
					$thisDisplayArr = array();
					foreach((array)$SubjectData as $DisplaySubjectData) 
					{
						// Display empty symbol if not in Subject Group
						if($eRCTemplateSetting['Report']['MasterReport']['ExcludeDroppedStudent'] && $StudentNotStudyingSubjectGroup) {
							$thisDisplayArr[$DisplaySubjectData] = $EmptySymbol;
							continue;
						}
						
						// Check Marksheet Score Column - Special Case or not
						$DisplayEmptySubjectData = $lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1($thisColumnGrade) || $lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2($thisColumnGrade);
						if($DisplayEmptySubjectData && !$thisColumnGradeNotConsidered) {
							$thisDisplayArr[$DisplaySubjectData] = $EmptySymbol;
							continue;
						}
						
						$ProcessedValue = '';
						$RawValue = $thisMarksAry[$DisplaySubjectData];
						switch($DisplaySubjectData)
						{
							case "Mark":
								// Absent (Not Considered)
								if($thisColumnGradeNotConsidered || $displayNotConsideredSymbol) {
									$thisOverallGradeNotConsidered = true;
									$ProcessedValue = "缺";
								}
								else if($ScaleInput=="G") {
									$ProcessedValue = $EmptySymbol;
								}
								// Term Report
								else if (!$isYearReport && $ApplyStyleToFailOnly)
								{
									// Check Overall Score Nature => first valid Report Column only
									$thisNature = "";
									if($isFirstColumn && !empty($thisOverallMarkAry))
									{
										if($ScaleInput=="M" && $ScaleDisplay="M" && is_numeric($thisOverallMark) && $thisOverallMark> 0)
											$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $thisSubjectID, $thisOverallMark, "", $ConvertBy="M", $ReportID);
										else
											$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $thisSubjectID, $thisOverallGrade, "", $ConvertBy="G", $ReportID);
											
										$isFirstColumn = false;
									}
									
									// Check Column Score Nature => No fail subject + No fail subject column + Overall Score Pass
									$thisColumnNature = "";
									$needToCheckColumnNature = $thisNature!="Fail" && !$StudentWithFailSubject && !$StudentWithFailSubjectCol;
									if ($needToCheckColumnNature)
									{
										if($ScaleInput=="M" && $ScaleDisplay="M" && is_numeric($thisColumnMark) && $thisColumnMark > 0)
											$thisColumnNature = $lreportcard->returnMarkNature($ClassLevelID, $thisSubjectID, $thisColumnMark, "", $ConvertBy="M", $ReportID);
										else
											$thisColumnNature = $lreportcard->returnMarkNature($ClassLevelID, $thisSubjectID, $thisColumnGrade, "", $ConvertBy="G", $ReportID);
									}
									
									// Display Fail Style - first Report Column only
									if ($thisNature=="Fail") {
										$ProcessedValue = $lreportcard->ReturnTextwithStyle($RawValue, $SettingCategory="", $style="", $parStyle="<span>*<!--score--></span>");
										$StudentWithFailSubject = true;
									}
									else {
										$ProcessedValue = $RawValue;
										if($thisColumnNature=="Fail") {
											$StudentWithFailSubjectCol = true;
										}
									}
								}
								// Year Report
								else if ($isYearReport && $ApplyStyleToFailOnly)
								{
									// Check Column Score Nature
									if($ScaleInput=="M" && $ScaleDisplay="M" && is_numeric($thisColumnMark) && $thisColumnMark > 0)
										$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $thisSubjectID, $thisColumnMark, "", $ConvertBy="M", $ReportID);
									else
										$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $thisSubjectID, $thisColumnGrade, "", $ConvertBy="G", $ReportID);
									
									// Display Fail Style
									if ($thisNature=="Fail") {
										$ProcessedValue = $lreportcard->ReturnTextwithStyle($RawValue, $SettingCategory="", $style="", $parStyle="<span><!--score-->*</span>");
										$StudentWithFailSubjectCol = true;
									}
									else {
										$ProcessedValue = $RawValue;
									}
								}
								else if(in_array("Mark", (array)$ShowStyle))
								{
									$ProcessedValue = $lreportcard->Format_Mark_With_Style($ReportID, $thisSubjectID, $thisColumnMark, $thisColumnGrade, $ClassLevelID, "", "M", $UseWeightedMark="", $thisReportColumnID, $ApplyStyleToFailOnly, $thisStudentID);
								}
								else {
									$ProcessedValue = $RawValue;
								}
							break;
							
							default: 
								if ($ApplyStyleToFailOnly)
								{
									if ($RawValue == -1 || $RawValue == "") {
										$ProcessedValue = $RawValue;
									}
									else {
										// Check Column Score Nature
										$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $thisSubjectID, $thisColumnGrade, "", $ConvertBy="G", $ReportID);
										
										// Display Fail Style
										if ($thisNature == "Fail") {
											$ProcessedValue = $lreportcard->ReturnTextwithStyle($RawValue, $SettingCategory='Highlight', $style='', $parStyle='<b><span style="color:red;"><!--score--></span></b>');
										}
										else {
											$ProcessedValue = $RawValue;
										}
									}
								}
								else {
									$ProcessedValue = $RawValue;
								}
							break;
						}
						
						// Display Empty Symbol
						$ProcessedValue = ($ProcessedValue==-1)? $EmptySymbol : $ProcessedValue;
						
						# Store Subject Marks Column Data
						$thisDisplayArr[$DisplaySubjectData] = $ProcessedValue;
					}
					
					# Store Subject Marks Data
					$SubjectMarksData[$thisSubjectID][$thisReportColumnID] = $thisDisplayArr;
					
					#### Statistic Process ####
					
					// Check Special Case
					$isSpecialCase = false;
					$thisMark = $lreportcard->Get_Display_Mark_By_Grading_Scheme($thisColumnMark, $thisColumnGrade, $GradingScheme[$thisSubjectID]);
					if ($lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1($thisMark) || $lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2($thisMark))
						$isSpecialCase = true;
					
					// Store empty records if not in Subject Group
					if($eRCTemplateSetting['Report']['MasterReport']['ExcludeDroppedStudent'] && $StudentNotStudyingSubjectGroup)
					{
						$StatMarkArr[$thisYearClassID][$thisSubjectID][$thisReportColumnID][] = "";
						$StatMarkNatureArr[$thisYearClassID][$thisSubjectID][$thisReportColumnID][] = "N.A.";
						continue;
					}
					
					# Store Statistic Process
					$StatMarkArr[$thisYearClassID][$thisSubjectID][$thisReportColumnID][] = $isSpecialCase || $displayNotConsideredSymbol? "" : $thisColumnMark;
					$StatMarkNatureArr[$thisYearClassID][$thisSubjectID][$thisReportColumnID][] = $isSpecialCase || $displayNotConsideredSymbol? "N.A." : $thisColumnMarkNature;
					if(!$isSpecialCase) $isSubjectAllNA[$thisYearClassID][$thisSubjectID] = false;
				}
				
				# Store Overall Mark Nature
				$StatStudentNatureArr[] = $isSpecialCase? "N.A." : $MarksNatureArr[$thisStudentID][$thisSubjectID][$OverallColumnID];
				
				# Count Fail Subject Unit (Subject Weight) - Main Subject only
				if ($OverallMarkNature == "Fail" && !$isSpecialCase)
				{
					$SubjectUnit = in_array($thisSubjectCode, $MainSubjectList)? 2 : 1;
					if(!empty($SubjectUnitList) && isset($SubjectUnitList[$thisSubjectCode])) {
                        $SubjectUnit = $SubjectUnitList[$thisSubjectCode];
                        $SubjectUnit = $SubjectUnit ? $SubjectUnit : 0;
                    }
					$SubjectUnit = in_array($thisSubjectCode, $CompletitionSubjectList)? 0 : $SubjectUnit;
					
					$StudentSubjectUnit += $SubjectUnit;
					$studentFailSubjectAry[] = $thisSubjectID;
					
					if($thisSubjectRetented && $thisOverallMark < 50) {
						$StudentSubjectUnit += $SubjectUnit;
					}
					
//					$MarkupExamScore = $MarkupExamAry[$thisStudentID][$thisSubjectID]["Info"];
//					if($OverallMarkNature == "Fail" && (!empty($MarkupExamScore) && $MarkupExamScore < 60))
//					{
//						$SubjectUnit = in_array($thisSubjectCode, $MainSubjectList)? 2 : 1;
//						$SubjectUnit = in_array($thisSubjectCode, $CompletitionSubjectList)? 0 : $SubjectUnit;
//						
//						$StudentSubjectUnit += $SubjectUnit;
//						$studentFailSubjectAry[] = $thisSubjectID;
//					}
//					else if(!empty($MarkupExamScore) && $MarkupExamScore >= 60)
//					{
//						$StudentSubjectNature = "Pass";
//						$StudentWithMarkupExam = true;
//					}
				}
			}
			
			// Update Failed Subject Unit for Retented Subjects (without exam this year)
			if(!empty($studentRetentedSubjectIds) && count($studentRetentedSubjectIds) > 0)
			{
				$notTakenRetentedSubjectIds = array_diff($studentRetentedSubjectIds, $SubjectIDArr);
				if(!empty($notTakenRetentedSubjectIds) && !empty($studentFailSubjectAry))
				{
					foreach((array)$notTakenRetentedSubjectIds as $thisRetentionSubjectID)
					{
						// Get Retention Subject Code
						$thisRetentionSubjectCode = $SubjectCodeMapping[$thisRetentionSubjectID];

						$SubjectUnit = in_array($thisRetentionSubjectCode, $MainSubjectList)? 2 : 1;
                        if(!empty($SubjectUnitList) && isset($SubjectUnitList[$thisRetentionSubjectCode])) {
                            $SubjectUnit = $SubjectUnitList[$thisRetentionSubjectCode];
                            $SubjectUnit = $SubjectUnit ? $SubjectUnit : 0;
                        }
						$SubjectUnit = in_array($thisRetentionSubjectCode, $CompletitionSubjectList)? 0 : $SubjectUnit;
						
						$StudentSubjectUnit += $SubjectUnit;
					}
				}
			}
			
			# Store Subject Mark Data & Statistic Process  
			$StudentDisplayAry[$thisYearClassID][$thisStudentID]["SubjectMarksData"] = $SubjectMarksData;
			
		################ Subject Marks Data & Statistic Process End ################## 
		
		################ Other Info Data Start ##################
			
//			# Store Other Info Data 
//			$StudentDisplayAry[$thisYearClassID][$thisStudentID]["OtherInfoData"] = $StudentOtherInfoData;
			
		################ Other Info Data End ##################
		
		################ Grand Marks Data Start ##################
			
			// loop Grand Marks Display
			foreach((array)$GrandMarkDataAry as $DisplayGrandMark)
			{
				# Get Grand Marks
				if($isGraduateTerm) {
					$thisGrandMarks = $GrandMarksAry[$thisStudentID][$ReportColumnID];
					$thisGrandMarksNature = $GrandMarksNatureArr[$thisStudentID][$ReportColumnID];
				}
				else {
					$thisGrandMarks = $GrandMarksAry[$thisStudentID][$OverallColumnID];
					$thisGrandMarksNature = $GrandMarksNatureArr[$thisStudentID][$OverallColumnID];
				}
				
				$ProcessedValue='';
				$RawValue = $thisGrandMarks[$DisplayGrandMark];
				switch($DisplayGrandMark)
				{
					case "GrandAverage":
						if ($RawValue != -1) {
							$StatGrandMarkArr[$thisYearClassID][$DisplayGrandMark][] = $RawValue;
							$StatGrandMarkNatureArr[$thisYearClassID][$DisplayGrandMark][] = $thisGrandMarksNature[$DisplayGrandMark];
						}
						
						if ($ApplyStyleToFailOnly)
						{
							if ($RawValue == -1 || $RawValue == '') {
								$ProcessedValue = $RawValue;
								if($thisOverallGradeNotConsidered) {
									$hidePromotionStatus = true;
									$ProcessedValue = "缺";
								}
							}
							else {
								# Get Grand Mark Nature
								$GrandMark = $thisGrandMarks[$RankDetermineField];
								$GrandMarkID = $lreportcard->Get_Grand_Field_SubjectID($RankDetermineField);
								$GrandGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandMarkScheme[$GrandMarkID]['SchemeID'], $GrandMark, $ReportID, $thisStudentID, $GrandMarkID, $ClassLevelID, 0);
								$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $GrandMarkID, $GrandGrade, "", $ConvertBy="G", $ReportID);
								
								// Display Fail Style
								if ($thisNature == "Fail") {
									$ProcessedValue = $lreportcard->ReturnTextwithStyle($RawValue, $SettingCategory='Highlight', $style='', $parStyle='<b><span style="color:red;"><!--score--></span></b>');
								}
								else {
									$ProcessedValue = $RawValue;
								}
							}
						}	
						else if(in_array($DisplayGrandMark, (array)$ShowStyle)) {
							$ProcessedValue = $lreportcard->Get_Score_Display_HTML($RawValue, $ReportID, $ClassLevelID, $GrandMarkID, $RawValue, $DisplayGrandMark, $thisStudentID, "", "", $ConvertToGrade=0, $Rounding="", "", $parApplyStyleToFailOnly=false, $parConvertBy="M");
						}
						else {
							$ProcessedValue = $RawValue;
						}
					break;
					
					case "NoOfFailSubject";
						$PassingAry = $lreportcard->getPassingNumber($StatStudentNatureArr, $withFailNumber=1);
						$ProcessedValue = $PassingAry[1];
					break;
					
					case "FailSubjectUnit":
						$ProcessedValue = $StudentSubjectUnit;
					break;
	
					default: 
						// Convert Rank Determine Mark to Grade
						$GrandMark = $thisGrandMarks[$RankDetermineField];
						$GrandMarkID = $lreportcard->Get_Grand_Field_SubjectID($RankDetermineField);
						
						if ($ApplyStyleToFailOnly)
						{
							if ($GrandMark == -1 || $GrandMark == "") {
								$ProcessedValue = $RawValue;
							}
							else {
								# Get Grand Mark Nature
								$GrandGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($GrandMarkScheme[$GrandMarkID]['SchemeID'], $GrandMark, $ReportID, $thisStudentID, $GrandMarkID, $ClassLevelID, 0);
								$thisNature = $lreportcard->returnMarkNature($ClassLevelID, $GrandMarkID, $GrandGrade, "", $ConvertBy="G", $ReportID);
								
								// Display Fail Style
								if ($thisNature == "Fail") {
									$ProcessedValue = $lreportcard->ReturnTextwithStyle($RawValue, $SettingCategory='Highlight', $style='', $parStyle='<b><span style="color:red;"><!--score--></span></b>');
								}
								else {
									$ProcessedValue = $RawValue;
								}
							}
						}	
						else {
							$ProcessedValue = $RawValue;
						}
					break;
				}
				
				// Display Empty Symbol
				$ProcessedValue = $ProcessedValue == "-1"? $EmptySymbol : $ProcessedValue;
				
				# Store Grand Marks Column Data
				$GrandMarkData[$DisplayGrandMark] = $ProcessedValue;
			}
			
			# Get Form No of Student
			if(!isset($ClassNoOfStudent[$thisYearClassID]))
				$ClassNoOfStudent[$thisYearClassID] = $thisGrandMarks["ClassNoOfStudent"]; 
			
			# Store Grand Mark Data 
			$StudentDisplayAry[$thisYearClassID][$thisStudentID]["GrandMarkData"] = $GrandMarkData;
			
		################ Grand Marks Data End ##################
		
		################ Extra Info Data Start ##################
			
			$studentPromotionStatus = "";
			$LastYearPromotionStatus = "";
			$RetentionReason = "";
			$thisRetentedSubjDisplay = "";
			$retentedFormLevel = "";
			$thisStudentStatus = "";
			
			# Get Comment Award
			if(!$isYearReport && !$isGraduateTerm)
			{
				# Get Student Conduct
				$StudentConductID = $StudentConductIDArr[$thisStudentID];
				$ConductData = $ConductArr[$StudentConductID];
				list($goodAcademic, $excellentAcademic, $excellentConduct) = $lreportcard->returnReceivedCommentAward($SchoolType, $FormNumber, $MarksAry[$thisStudentID], $SubjectIDArr, $ConductData, $SubjectCodeMapping, 0, $skipConductChecking=true);
			}
			
			# Get Promotion Status
			if ($isYearReport || $thisYearReportID) {
				$studentPromotionStatus = $reportPromotionStatus[$thisStudentID]["Promotion"];
				if($hidePromotionStatus) {
					$studentPromotionStatus = "";
				}
			}
			
			// Retention Details
			if($SchoolType=="S")
			{
				# Get Retention Reason
				if($studentPromotionStatus==$promotionStatusAry['Retained'] || $studentPromotionStatus==$promotionStatusAry['Dropout']) {
					$sql = "Select StatusReason From ".$lreportcard->DBName.".RC_STUDENT_PROMOTION_RETENTION_STATUS WHERE StudentID = '$thisStudentID' AND Year = '".$lreportcard->schoolYearID."' AND PromotionStatus IN ('".$promotionStatusAry['Retained']."', '".$promotionStatusAry['Dropout']."')";
					$RetentionReason = $lreportcard->returnVector($sql);
					$RetentionReason = $RetentionReason[0];
				}
				
				# Get Retented Subject
				//$thisRetentedSubj = $lreportcard->GET_STUDENT_PAST_YEAR_PROMOTION_RETENTION_SUBJECT_RECORD($thisStudentID, "", $SubjectTarget="2");
				$thisRetentedSubj = array_values((array)$studentRetentedSubjectIds);
				foreach((array)$thisRetentedSubj as $thisSubjectID) {
					$thisRetentedSubjDisplay .= $lreportcard->GET_SUBJECT_NAME_LANG($thisSubjectID, $ParLang="b5", $ParShortName=true, $ParAbbrName=true);
				}
			}
			else if($SchoolType=="P") {
				# Get Retented Form Number
				$retentedFormLevel = $lreportcard->IS_STUDENT_NEED_TO_DROPOUT($thisStudentID, $SchoolType, $FormNumber, $returnFormNumber=true);
				$retentedFormLevel = $retentedFormLevel[1];
				if(!empty($retentedFormLevel))
					$retentedFormLevel = implode(" ", (array)$retentedFormLevel);
			}
			
			# Get Student Status
			// Check new Student
			$LastYearClassInfo = $fcm->Get_Student_Class_Info_In_AcademicYear($thisStudentID, $LastYearID);
			$LastYearLevelID = $LastYearClassInfo[0]["YearID"];
			$isNewStudent = empty($LastYearLevelID);
//			if($LastYearLevelID > 0){
//				$LastYearSchoolType	= $lreportcard->GET_SCHOOL_TYPE_BY_CLASSLEVEL($LastYearLevelID);
//			}
//			$ActiveYearClassInfo = $fcm->Get_Student_Class_Info_In_AcademicYear($thisStudentID, $lreportcard->schoolYearID);
//			$ActiveYearLevelID = $ActiveYearClassInfo[0]["YearID"];
//			if($ActiveYearLevelID > 0){
//				$ActiveYearSchoolType = $lreportcard->GET_SCHOOL_TYPE_BY_CLASSLEVEL($ActiveYearLevelID);
//			}
//			$isNewStudent = empty($LastYearLevelID) || ($LastYearLevelID > 0 && $ClassLevelID > 0 && $LastYearSchoolType != $SchoolType);
			
			// Check retented Student
			if(!$isNewStudent)
			{
				$LastYearReportID = $lastYearStudentReportMapping[$thisStudentID];
				if($LastYearReportID > 0) {
					$LastYearPromotionStatus = $lastYearPromotionStatusAry[$LastYearReportID][$thisStudentID]["Promotion"];
				}
			}
			$isStudentRetented = !$isNewStudent && $LastYearReportID > 0 && $LastYearPromotionStatus==$promotionStatusAry['Retained'];
			
			// Symbol for Student Status
			$thisStudentStatus = $isNewStudent? "☆" : ($isStudentRetented? "△" : "");
			
			$thisSubjectExtraData["studentFailSubjectAry"] = $studentFailSubjectAry;
			$thisSubjectExtraData["StudentWithFailSubject"] = $StudentWithFailSubject;
			$thisSubjectExtraData["StudentWithFailSubjectCol"] = $StudentWithFailSubjectCol;
			$thisSubjectExtraData["StudentWithMarkupExam"] = $StudentWithMarkupExam;
			$thisSubjectExtraData["PromotionStatus"] = $studentPromotionStatus;
			$thisSubjectExtraData["retentionReason"] = $RetentionReason;
			$thisSubjectExtraData["retentionSubject"] = $thisRetentedSubjDisplay;
			$thisSubjectExtraData["retentedFormLevel"] = $retentedFormLevel;
			$thisSubjectExtraData["StudentStatus"] = $thisStudentStatus;
			$thisSubjectExtraData["Award_GoodAcademic"] = $goodAcademic && !$excellentAcademic;
			$thisSubjectExtraData["Award_ExcellentAcademic"] = $excellentAcademic;
			
			# Store Extra Info Data 
			$StudentDisplayAry[$thisYearClassID][$thisStudentID]["SubjectExtraData"] = $thisSubjectExtraData;
			
		################ Extra Info Data End ##################	
	}
}

############## Statistic ###############

$decimalPlacesShown = 2;

# Get Subject Full Mark
//$SubjectFullMark = $lreportcard->returnSubjectFullMark($ClassLevelID, $withSub=NULL, $SubjectIDArr, $ParInputScale=1, $ReportID);

############# Subject Mark Statistic 

$MasterReportArr = array_keys($StudentDisplayAry);
foreach($MasterReportArr as $StudentGroupingID)
{
	# loop Statistic Data
	foreach((array)$StatisticData as $DisplayStatisticData)
	{
		if($eRCTemplateSetting['MasterReport']['PassingNumberDetermineByPercentage'])
		{
			$CustPassMarkPercentName = "Stat".$DisplayStatisticData."_val";
			$thisSpecialPassRate = $_POST[$CustPassMarkPercentName];
			if($thisSpecialPassRate)
				$CustValue[$CustPassMarkPercentName] = $thisSpecialPassRate;
		}
		
		// loop Subject
		foreach((array) $SubjectIDArr as $thisSubjectID)
		{
			if(!$isGraduateTerm)
				$ReportColumnID = array(0);
			
			// loop Report Column
			foreach((array)$ReportColumnID as $thisReportColumnID)
			{
				# Get Stat Mark
				$thisMarkArr = (array)$StatMarkArr[$StudentGroupingID][$thisSubjectID][$thisReportColumnID];	
				$thisMarkNatureArr = (array)$StatMarkNatureArr[$StudentGroupingID][$thisSubjectID][$thisReportColumnID];
				
				switch($DisplayStatisticData)
				{						
					case "FailingRate":
						$PassingAry = $lreportcard->getPassingRate($thisMarkNatureArr, $decimalPlacesShown, 1);
						$ProcessedValue = $DisplayStatisticData=="PassingRate"? $PassingAry[0] : $PassingAry[1];
					break;
						
					case "FailingNumber":
						$PassingAry = $lreportcard->getPassingNumber($thisMarkNatureArr, 1);
						$ProcessedValue = $DisplayStatisticData=="PassingNumber"? $PassingAry[0] : $PassingAry[1];
						$ProcessedValue2 = count(array_remove_empty((array)$thisMarkArr));
						$ProcessedValue = "$ProcessedValue / $ProcessedValue2";
					break;
					
					default:
						$ProcessedValue = '';
					break;
				}
				
				// Display Empty Symbol
				$ProcessedValue = $ProcessedValue == -1? $EmptySymbol : $ProcessedValue;
				
				# Store Subject Stat Data 
				$StatisticDisplayAry[$StudentGroupingID][$DisplayStatisticData]["SubjectMark"][$thisSubjectID][0] = $ProcessedValue;
			}
		}
		
		# Get Grand Statistic
		$thisGrandMarkStat = (array)$StatGrandMarkArr[$StudentGroupingID];
		$thisGrandMarkNatureStat = $StatGrandMarkNatureArr[$StudentGroupingID];
		
		$GrandMarkStat = array();
		switch($DisplayStatisticData)
		{				
			case "FailingRate":
				$thisGrandMarkNatureArr = $thisGrandMarkNatureStat[$RankDetermineField];
				$PassingAry = $lreportcard->getPassingRate(array_values(array_remove_empty((array)$thisGrandMarkNatureArr)), $decimalPlacesShown, 1);
				$GrandMarkStat[$RankDetermineField] = $DisplayStatisticData=="PassingRate" ? $PassingAry[0] : $PassingAry[1];
			break;
			
			case "FailingNumber":
				$thisGrandMarkNatureArr = $thisGrandMarkNatureStat[$RankDetermineField];
				$PassingAry = $lreportcard->getPassingNumber(array_values(array_remove_empty((array)$thisGrandMarkNatureArr)), 1);
				$GrandMarkStat[$RankDetermineField] = $DisplayStatisticData=="PassingNumber" ? $PassingAry[0] : $PassingAry[1];
				$GrandMarkStat[$RankDetermineField] = $GrandMarkStat[$RankDetermineField]? $GrandMarkStat[$RankDetermineField] : "0";
				$GrandMarkStat[$RankDetermineField] = $GrandMarkStat[$RankDetermineField]." / ".$ClassNoOfStudent[$StudentGroupingID];
			break;
		}
		
		# Store Grand Stat Data 
		$StatisticDisplayAry[$StudentGroupingID][$DisplayStatisticData]["GrandMark"] = $GrandMarkStat;	
	}
}

# Display Content
echo $lreportcard->Get_Subject_Stat_Report($StudentDisplayAry, $StatisticDisplayAry, $MasterReportSetting, $isSubjectAllNA, $CustValue);

?>