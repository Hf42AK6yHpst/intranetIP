<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

$lreportcard = new libreportcardcustom();
$lreportcard_ui = new libreportcard_ui();
$linterface = new interface_html();

$lreportcard->hasAccessRight();
		
############## Interface Start ##############
$CurrentPage = "Reports_AwardList";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ[] = array($eReportCard['Reports_AwardList']);
$linterface->LAYOUT_START();
#############################################

### Get Form selection (show form which has report card only)
$FormSelection = $lreportcard_ui->Get_Form_Selection('YearID', '', 'js_Reload_Selection(this.value)', $noFirst=1);
?>
<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT."templates/2007a/css/"?>ereportcard.css">

<script language="javascript">

$(document).ready( function() {
	var jsCurFormID = $('select#YearID').val();
	js_Reload_Selection(jsCurFormID);
});

function SelectAll(obj)
{
     for (i=0; i<obj.length; i++)
     {
          obj.options[i].selected = true;
     }
}

function js_Reload_Selection(jsYearID)
{
	$('#ReportSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Report',
			YearID: jsYearID,
			ReportID: '',
			SelectionID: 'ReportID',
			onChange: 'js_Changed_Report_Selection();'
		},
		function(ReturnData)
		{
			js_Changed_Report_Selection();
		}
	);
	
	$('#ClassSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Class',
			YearID: jsYearID,
			SelectionID: 'YearClassIDArr[]',
			isMultiple: 1
		},
		function(ReturnData)
		{
			SelectAll(document.form1.elements['YearClassIDArr[]']);
		}
	);
}

function js_Changed_Report_Selection() {
	js_Reload_Award_Selection();
}

function js_Reload_Award_Selection() {
	$('div#AwardSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Award',
			ReportID: $('select#ReportID').val(),
			SelectionID: 'AwardIDArr[]',
			isMultiple: 1,
			withSelectAll: 1
		},
		function(ReturnData) {
			SelectAll(document.form1.elements['AwardIDArr[]']);
		}
	);
}

function checkForm() 
{
	var obj = document.form1;
	var select_obj = obj.elements['YearClassIDArr[]'];
	var hasSelectedClass = false;
	for (var i=0; i<select_obj.length; i++)
	{
		if (select_obj[i].selected)
		{
			hasSelectedClass = true;
		}
	}

	if (hasSelectedClass == false)
	{
		alert('<?=$eReportCard["jsWarningSelectClass"]?>');
		obj.elements['YearClassIDArr[]'].focus();
		return false;
	}
	
	return true;
}
</script>

<br />
<form name="form1" action="generate.php" method="POST" onsubmit="return checkForm();" target="_blank">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
<table>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td></td>
					<td align="right"><?=$linterface->GET_SYS_MSG($Result);?></td>
				</tr>
				
				<!-- View Format -->
				<tr id="ViewFormatRow">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ViewFormat'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="ViewFormat" id="ViewHTML" value="html" CHECKED>
							<label for="ViewHTML"><?=$eReportCard['HTML']?></label>
						</input>
						<input type="radio" name="ViewFormat" id="ViewCSV" value="csv">
							<label for="ViewCSV"><?=$eReportCard['CSV']?></label>
						</input>
					</td>
				</tr>
				
				<!-- Form -->
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['FormName'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<?= $FormSelection ?>
					</td>
				</tr>
				
				<!-- Class Selection -->
				<tr class="tabletext">
					<td class="formfieldtitle" valign="top"><?=$eReportCard['Class']?> <span class="tabletextrequire">*</span></td>
					<td>
						<span id="ClassSelectionSpan">
							<?=$ClassSelection?>
						</span>
						<span style="vertical-align:bottom">
							<?= $linterface->GET_SMALL_BTN($button_select_all, "button", "javascript:SelectAll(document.form1.elements['YearClassIDArr[]'])"); ?>
						</span>
					</td>
				</tr>
				
				<!-- Report -->
				<tr class="tabletext">
					<td class="formfieldtitle" valign="top" width="30%"><?=$eReportCard['ReportCard']?> <span class="tabletextrequire">*</span></td>
					<td>
						<div id="ReportSelectionDiv">
							<?= $ReportSelection ?>
						</div>
					</td>
				</tr>
				
				<!-- Award -->
				<tr class="tabletext">
					<td class="formfieldtitle" valign="top" width="30%"><?=$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['Award']?> <span class="tabletextrequire">*</span></td>
					<td>
						<div id="AwardSelectionDiv"></div>
					</td>
				</tr>
				
				<tr>
					<td colspan="2" class="dotline">
						<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submitBtn"); ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>