<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN"; 

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();


$viewFormat = standardizeFormPostValue($_POST['ViewFormat']);
$formIdAry = $_POST['TargetID'];
$numOfForm = count($formIdAry);


include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();

### hardcode SubjectID
//$targetSubjectAry['CHIN'] = array(255, 368, 362, 343);
$targetSubjectAry['CHIN'] = array(368);
//$targetSubjectAry['ENG'] = array(265, 264, 345, 356);
$targetSubjectAry['ENG'] = array(265);
$targetSubjectAry['MATH'] = array(273);

### hardcode class group
//中一至中三：ABC為一組、DE為一組
//中四至中五：全級排序便可以
$classGroupAssoAry[1][] = array('1A', '1B', '1C');
$classGroupAssoAry[1][] = array('1D', '1E');
$classGroupAssoAry[2][] = array('2A', '2B', '2C');
$classGroupAssoAry[2][] = array('2D', '2E');
$classGroupAssoAry[3][] = array('3A', '3B', '3C');
$classGroupAssoAry[3][] = array('3D', '3E');
$classGroupAssoAry[4][] = array('4A', '4B', '4C', '4D', '4E');
$classGroupAssoAry[5][] = array('5A', '5B', '5C', '5D', '5E');
$classGroupAssoAry[6][] = array('6A', '6B', '6C', '6D', '6E');

//初中DE班只需要分中文強、英文強兩類
$ignoreMathsClassAry = array('1D', '1E', '2D', '2E', '3D', '3E');


$fcm = new form_class_manage();
$termAry = $fcm->Get_Academic_Year_Term_List($lreportcard->GET_ACTIVE_YEAR_ID());
$numOfTerm = count($termAry);


$dataAry = array();
$titleAry = array();
$classCount = 0;
for ($i=0; $i<$numOfForm; $i++) {
	$_formId = $formIdAry[$i];
	
	// get form number
	$_formNumber = $lreportcard->GET_FORM_NUMBER($_formId, $ByWebSAMSCode=true); 
	
	// get students
	$_studentAssoAry = BuildMultiKeyAssoc($lreportcard->GET_STUDENT_BY_CLASSLEVEL('', $_formId), 'UserID');
	
	
	// get reports
	$_termInfoAssoAry = array();
	$_studentScoreAssoAry = array();
	for ($j=0; $j<$numOfTerm; $j++) {
		$__yearTermId = $termAry[$j]['YearTermID'];
		
		// get term report info
		$_termInfoAssoAry[$__yearTermId]['reportInfo'] = $lreportcard->returnReportTemplateBasicInfo('', '', $_formId, $__yearTermId);
		$__termReportId = $_termInfoAssoAry[$__yearTermId]['reportInfo']['ReportID'];
		
		// get term report marks
		$_termInfoAssoAry[$__yearTermId]['marksAry'] = $_markAry = $lreportcard->getMarksCommonIpEj($__termReportId, $StudentID='', $cons='', $ParentSubjectOnly=1, $includeAdjustedMarks=1, $OrderBy='', $SubjectID='', $ReportColumnID=0);
	}
	
	
	// consolidate student mark data of chi, eng and maths
	foreach ((array)$_studentAssoAry as $__studentId => $__studentInfoAry) {
		for ($j=0; $j<$numOfTerm; $j++) {
			$___yearTermId = $termAry[$j]['YearTermID'];
			
			foreach ((array)$targetSubjectAry as $____subjectCode => $____subjectIdAry) {
				$____numOfSubjectId = count($____subjectIdAry);
				
				for ($k=0; $k<$____numOfSubjectId; $k++) {
					$_____subjectId = $____subjectIdAry[$k];
					
					$_____subjectMark = $_termInfoAssoAry[$___yearTermId]['marksAry'][$__studentId][$_____subjectId][0]['RawMark'];
					$_____subjectGrade = $_termInfoAssoAry[$___yearTermId]['marksAry'][$__studentId][$_____subjectId][0]['Grade'];
					
					if ($_____subjectGrade == '+') {
						$_____subjectMark = 0;
					}
					else {
						if (!isset($_termInfoAssoAry[$___yearTermId]['marksAry'][$__studentId][$_____subjectId][0]['RawMark']) || $lreportcard->Check_If_Grade_Is_SpecialCase($_____subjectGrade)) {
							continue;
						}
						else {
							$_studentScoreAssoAry[$____subjectCode][$__studentId][$___yearTermId] = $_____subjectMark;
						}
					}
				}	
			}
		}
	}
	
	// calculate the average of each subject of each students
	foreach ((array)$_studentScoreAssoAry as $__subjectCode => $__subjectDataAry) {
		foreach ((array)$__subjectDataAry as $___studentId => $___subjectStudentDataAry) {
			$___subjectAverage = $lreportcard->getAverage(array_values($___subjectStudentDataAry));
			
			$_studentScoreAssoAry[$__subjectCode][$___studentId]['overallAverage'] = $___subjectAverage;
		}
	}
	
	// calculate the SD score of the subject of each students
	foreach ((array)$targetSubjectAry as $__subjectCode => $__subjectIdAry) {
		// calculate subject average and SD
		$__subjectMarkAry = array();
		foreach ((array)$_studentScoreAssoAry[$__subjectCode] as $___studentId => $___subjectStudentDataAry) {
			$__subjectMarkAry[] = $___subjectStudentDataAry['overallAverage'];
		}
		$__subjectAverage = $lreportcard->getAverage($__subjectMarkAry);
		$__subjectSD = $lreportcard->getSD($__subjectMarkAry);
		
		
		// calculate SD score for each students
		foreach ((array)$_studentScoreAssoAry[$__subjectCode] as $___studentId => $___subjectStudentDataAry) {
			$___studentSubjectMark = $___subjectStudentDataAry['overallAverage'];
			
			if ($__subjectSD == 0) {
				$___studentSubjectSdScore = 0;
			}
			else {
				$___studentSubjectSdScore = ($___studentSubjectMark - $__subjectAverage) / $__subjectSD;
			}
			
			$_studentScoreAssoAry[$__subjectCode][$___studentId]['sdScore'] = $___studentSubjectSdScore;
		}
	}
	
//	debug_pr($_studentScoreAssoAry);
//	die();
	
	
	
	// get overall gpa
	$_consolidatedReportInfoAry = $lreportcard->returnReportTemplateBasicInfo('', '', $_formId, 'F');
	$_consolidatedReportId = $_consolidatedReportInfoAry['ReportID'];
	$_grandMarkAry = $lreportcard->getReportResultScore($_consolidatedReportId, $ReportColumnID=0);
	
	
	// get conduct
	$_otherInfoAry = $lreportcard->getReportOtherInfoData($_consolidatedReportId);
	
		
	// get student order by GPA
	$_orderFieldAry = array();
	$_orderFieldAry[0]['OrderField'] = 'GPA';
	$_orderFieldAry[0]['Order'] = 'desc';
	$_studentOrderedIdAry = array_keys((array)$lreportcard->getReportResultScore($_consolidatedReportId, $ReportColumnID=0, $StudentID='', $other_conds='', 0, $includeAdjustedMarks=1, $CheckPositionDisplay=0, $FilterNoRanking=1, $_orderFieldAry));
	$_numOfStudent = count($_studentOrderedIdAry);
	
	
	// analyze students go to which class
	$_numOfGroup = count($classGroupAssoAry[$_formNumber]);
	for ($j=0; $j<$_numOfGroup; $j++) {
		$__classNameAry = $classGroupAssoAry[$_formNumber][$j];
		
		$__classAllocationAssoAry = array();
		for ($k=0; $k<$_numOfStudent; $k++) {
			$___studentId = $_studentOrderedIdAry[$k];
			$___studentClassNameEn = $_studentAssoAry[$___studentId]['ClassTitleEn'];
			
			if (!in_array($___studentClassNameEn, $__classNameAry)) {
				// student not in this group => skip
				continue;
			}
			
			// get student highest score subject
			$___studentSubjectScoreAry = array();
			foreach ((array)$targetSubjectAry as $____subjectCode => $____subjectIdAry) {
				// 初中DE班只需要分中文強、英文強兩類
				
				if ($____subjectCode=='MATH' && in_array($___studentClassNameEn, $ignoreMathsClassAry)) {
					continue;
				}
				
				$___studentSubjectScoreAry[$____subjectCode] = $_studentScoreAssoAry[$____subjectCode][$___studentId]['sdScore'];
			}
			
			// sort scores
			asort($___studentSubjectScoreAry, SORT_NUMERIC);
			// reverse the array to get the greatest score as first element
			$___studentSubjectScoreAry = array_reverse($___studentSubjectScoreAry);
			// get the highest score subject as future class key
			$___studentTargetClass = '';
			foreach ((array)$___studentSubjectScoreAry as $____subjectCode => $____subjectScore) {
				if ($____subjectScore !== null) {
					$___studentTargetClass = $____subjectCode;
					break;
				}
			}
			
			if ($___studentTargetClass != '') {
				$__classAllocationAssoAry[$___studentTargetClass][] = $___studentId;
			}
		}
		
		
		// consolidate class student info in one array
		foreach ((array)$targetSubjectAry as $___classCode => $___subjectIdAry) {
			$___classStudentIdAry = $__classAllocationAssoAry[$___classCode];
			$___numOfStudent = count($___classStudentIdAry);
			
			if ($___numOfStudent == 0) {
				continue;
			}
			
			$___genderCountAssoAry = array();
			for ($k=0; $k<$___numOfStudent; $k++) {
				$____studentId = $___classStudentIdAry[$k];
				$____studentClassName = $_studentAssoAry[$____studentId]['ClassTitleEn'];
				$____studentClassNumber = $_studentAssoAry[$____studentId]['ClassNumber'];
				$____studentNameEn = $_studentAssoAry[$____studentId]['StudentNameEn'];
				$____studentNameCh = $_studentAssoAry[$____studentId]['StudentNameCh'];
				$____studentGender = $_studentAssoAry[$____studentId]['Gender'];
				$____studentUserLogin = $_studentAssoAry[$____studentId]['UserLogin'];
				
				$___genderCountAssoAry[$____studentGender]++;
				
				// student basic info
				$dataAry[$classCount][$k][] = $____studentUserLogin;
				$dataAry[$classCount][$k][] = $____studentClassName;
				$dataAry[$classCount][$k][] = $____studentClassNumber;
				$dataAry[$classCount][$k][] = $____studentNameEn;
				$dataAry[$classCount][$k][] = $____studentNameCh;
				$dataAry[$classCount][$k][] = $____studentGender;
				
				// score of chi, eng, maths
				foreach ((array)$targetSubjectAry as $_____subjectCode => $_____subjectIdAry) {
					// subject term score
					for ($l=0; $l<$numOfTerm; $l++) {
						$______yearTermId = $termAry[$l]['YearTermID'];
						$______score = $_studentScoreAssoAry[$_____subjectCode][$____studentId][$______yearTermId];
						$______score = ($______score=='')? $Lang['General']['EmptySymbol'] : $______score;
						
						$dataAry[$classCount][$k][] = $______score;
					}
					// subject overall average
					$_____overallScore = $_studentScoreAssoAry[$_____subjectCode][$____studentId]['overallAverage'];
					$_____overallScore = ($_____overallScore=='')? $Lang['General']['EmptySymbol'] : $_____overallScore;
					$dataAry[$classCount][$k][] = $_____overallScore;
				}
				
				// SD score of chi, eng, maths
				foreach ((array)$targetSubjectAry as $_____subjectCode => $_____subjectIdAry) {
					$_____sdScore = $_studentScoreAssoAry[$_____subjectCode][$____studentId]['sdScore'];
					$_____sdScore = ($_____sdScore=='')? $Lang['General']['EmptySymbol'] : $_____sdScore;
					$dataAry[$classCount][$k][] = $_____sdScore;
				}
				
				// highest SD score
				$____highestSdScore = $_studentScoreAssoAry[$___classCode][$____studentId]['sdScore'];
				$____highestSdScore = ($____highestSdScore=='')? $Lang['General']['EmptySymbol'] : $____highestSdScore;
				$dataAry[$classCount][$k][] = $____highestSdScore;
				
				// arranged class
				$dataAry[$classCount][$k][] = $___classCode;
				
				// conduct of each terms
				for ($l=0; $l<$numOfTerm; $l++) {
					$_____yearTermId = $termAry[$l]['YearTermID'];
					
					$_____termConduct = trim($_otherInfoAry[$____studentId][$_____yearTermId]['Conduct']);
					$_____termConduct = ($_____termConduct=='')? $Lang['General']['EmptySymbol'] : $_____termConduct;
					
					$dataAry[$classCount][$k][] = $_____termConduct;
				}
				
				// gpa
				$____gpa = $_grandMarkAry[$____studentId][0]['GPA'];
				$____gpa = ($____gpa=='' || $____gpa==-1)? $Lang['General']['EmptySymbol'] : $____gpa;
				$dataAry[$classCount][$k][] = $____gpa;
				
				// L/R
				$dataAry[$classCount][$k][] = '';
				
				// next year class allocation
				$dataAry[$classCount][$k][] = '';
			}
			
			// class gender count display
			for ($l=0; $l<27; $l++) {
				$dataAry[$classCount][$___numOfStudent][] = '';
			}
			$dataAry[$classCount][$___numOfStudent][] = 'TOTAL';
			$dataAry[$classCount][$___numOfStudent][] = 'FEMALE';
			$dataAry[$classCount][$___numOfStudent][] = 'MALE';
			for ($l=0; $l<27; $l++) {
				$dataAry[$classCount][$___numOfStudent+1][] = '';
			}
			$dataAry[$classCount][$___numOfStudent+1][] = $___numOfStudent;
			$dataAry[$classCount][$___numOfStudent+1][] = $___genderCountAssoAry['F'];
			$dataAry[$classCount][$___numOfStudent+1][] = $___genderCountAssoAry['M'];
			
			$classCount++;
		}
	}
}
$numOfTable = count($dataAry);



// build report title
$titleAry[] = 'UserLogin';
$titleAry[] = 'Class';
$titleAry[] = 'No.';
$titleAry[] = 'English Name';
$titleAry[] = 'CName';
$titleAry[] = 'G';
$titleAry[] = 'CT1';
$titleAry[] = 'CT2';
$titleAry[] = 'CT3';
$titleAry[] = 'AVC';
$titleAry[] = 'ET1';
$titleAry[] = 'ET2';
$titleAry[] = 'ET3';
$titleAry[] = 'AVE';
$titleAry[] = 'MT1';
$titleAry[] = 'MT2';
$titleAry[] = 'MT3';
$titleAry[] = 'AVM';
$titleAry[] = 'SCT3';
$titleAry[] = 'SET3';
$titleAry[] = 'SMT3';
$titleAry[] = 'TESTING VALUE';
$titleAry[] = 'CLASS';
$titleAry[] = 'Conduct';
$titleAry[] = 'GPA';
$titleAry[] = 'L/R';

$activeYear = $lreportcard->GET_ACTIVE_YEAR();
$thisYear = substr($activeYear, -2, 2);
$titleAry[] = ($thisYear+1).'/'.($thisYear+2).' Class Allocation';
$numOfTitle = count($titleAry);


$titleCsvAry = array();
for ($i=0; $i<$numOfTitle; $i++) {
	$_title = $titleAry[$i];
	
	if ($_title == 'Conduct') {
		$titleCsvAry[] = 'Conduct 1';
		$titleCsvAry[] = 'Conduct 2';
		$titleCsvAry[] = 'Conduct 3';
	}
	else {
		$titleCsvAry[] = $_title;
	}
}


## Display the result
if ($viewFormat == 'html') {
	$x = '';
	for ($i=0; $i<$numOfTable; $i++) {
		$_dataAry = $dataAry[$i];
		$_numOfData = count($_dataAry);
		
		if ($i==($numOfTable-1)) {
			$_pageBreak = '';
		}
		else {
			$_pageBreak = 'page-break-after:always;';
		}
		
		$x .= '<div style="'.$_pageBreak.'">'."\r\n";
			$x .= '<table id="ResultTable" class="border_table" align="center">'."\r\n";
				$x .= '<tr>'."\r\n";
				for ($j=0; $j<$numOfTitle; $j++) {
					$__title = $titleAry[$j];
					
					$__colspanAttr = '';
					if ($__title == 'Conduct' || $j==$numOfTitle-1) {
						$__colspanAttr = ' colspan="3" ';
					}
					
					$x .= '<td '.$__colspanAttr.'>'.$__title.'</td>'."\r\n";
				}
				$x .= '</tr>'."\r\n";
				
				for ($j=0; $j<$_numOfData; $j++) {
					$__studentDataAry = $_dataAry[$j];
					$__numOfData = count($__studentDataAry);
					
					$x .= '<tr>'."\r\n";
						for ($k=0; $k<$__numOfData; $k++) {
							$___cellData = $__studentDataAry[$k];
							
							$___textAlign = 'center';
							if ($k==3) {
								// student name English
								$___textAlign = 'left';
							}
							
							$___colspanAttr = '';
							if ($k==$__numOfData-1) {
								// class allocation column
								$___colspanAttr = ' colspan="3" ';
							}
							
							$x .= '<td style="text-align:'.$___textAlign.';" '.$___colspanAttr.' >'.$___cellData.'</td>'."\r\n";
						}
					$x .= '</tr>'."\r\n";
				}
			$x .= '</table>'."\r\n";
			$x .= '<br />'."\r\n";
			$x .= '<br />'."\r\n";
		$x .= '</div>'."\r\n";
	}
	
	
	$css = '
<style>
body {
	font-size: small;
}

.border_table{
	border-collapse: collapse;
	border: 1px solid #000000;
	width: 900px;
}

.border_table tr td, .border_table tr th{
	border: 1px solid #000000;
	vertical-align: top;
	padding: 3px;
	font-size: 10pt;
}
</style>';
	
//	$allTable = '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
//					<tr>
//						<td>'.$x.'</td>
//					</tr>
//				</table>';
	$allTable = $x;
	
	echo $lreportcard->Get_Report_Header($ReportTitle);
	echo $allTable.$css;
	echo $lreportcard->Get_Report_Footer();
}
else if ($viewFormat == 'csv') {
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	
	$lexport = new libexporttext();
	
	$exportContentAry = array();
	
	for ($i=0; $i<$numOfTable; $i++) {
		$_dataAry = $dataAry[$i];
		$_numOfData = count($_dataAry);
		
		$exportContentAry[] = $titleCsvAry;
		
		for ($j=0; $j<$_numOfData; $j++) {
			$exportContentAry[] = $_dataAry[$j];
		}
		
		$exportContentAry[] = array('');
		$exportContentAry[] = array('');
	}
	
	
	// Title of the grandmarksheet
	$filename = "class_allocation.csv";
	
	$export_content = $lexport->GET_EXPORT_TXT($exportContentAry, array());
	intranet_closedb();
	
	// Output the file to user browser
	$lexport->EXPORT_FILE($filename, $export_content);
}
?>