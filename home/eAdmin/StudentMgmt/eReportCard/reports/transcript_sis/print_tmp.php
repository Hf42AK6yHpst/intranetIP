<?php
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}


include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_sis.php");
$lreportcard = new libreportcardSIS();
$linterface = new interface_html();

# For NAPFA Preview
include_once($PATH_WRT_ROOT."includes/libPMS.php");
include_once($PATH_WRT_ROOT."includes/config.inc.php");
$objDB = new libdb();
$sGradeArr = getFitnessStandardGrade();

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

# Get the eRC Template Settings
include_once($PATH_WRT_ROOT."includes/eRCConfig.php");
$eRCtemplate = $lreportcard->getCusTemplate();
$eRtemplateCSS = $lreportcard->getTemplateCSS();

$ReportSetting = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$ClassLevelID = $ReportSetting['ClassLevelID'];

include_once($PATH_WRT_ROOT."templates/2007a/layout/print_header.php");
?>


<? if($eRtemplateCSS) {?><link href="<?=$eRtemplateCSS?>" rel="stylesheet" type="text/css" /><? } ?>

<style type='text/css' media='print'>
 .print_hide {display:none;}
</style>
<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>
<?
//echo $StudentID."<br/>";

//for testing now , support with two mode (query from index.php / index2.php)
if($StudentID != ""){
	//from index2.php
	$StudentIDArr = array($StudentID);
}else{
	//from index.php
	$StudentIDArr = $_POST['StudentIDArr'];
}


//$StudentIDAry = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $ClassID, $TargetStudentList);

for($s=0;$s<sizeof($StudentIDArr);$s++)
{
	$StudentID = $StudentIDArr[$s];
	$ReportHTML = $lreportcard->Get_Student_Transcript_HTML($StudentID, 1);
	
	//$thisPageBreak = ($s==sizeof($StudentIDAry)-1)? '' : 'style="page-break-after:always"';
	
	$x = '';
	
	if ($s==0)
		$x .= '<br />'."\n";
		
	$x .= '<div id="container_transcript" valign="top">';
		//$x .= ($s==0)? '<br />' : '';
		//$x .= '<table valign="top" align="center" width="100%" border="0" cellpadding="0" cellspacing="0" '.$thisPageBreak.'>';
		//	$x .= '<tr><td>'.$ReportHTML.'</td></tr>';
		//$x .= '</table>';
		$x .= $ReportHTML;
	$x .= '</div>';
	
	echo $x;
}
?>
<table border="0" cellpadding="4" width="100%" cellspacing="0" class="print_hide">
<tr><td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></span></td></tr>
</table>
