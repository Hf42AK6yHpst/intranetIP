<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";
 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
//include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008j.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_generate_sis.php");
//include_once($PATH_WRT_ROOT."includes/libreportcard2008_sis.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/sis.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

//$lreportcard = new libreportcardSIS();
$lreportcard = new libreportcardcustom();
$lreportcardgenerate = new libreportcard2008_generate();

$lclass = new libclass();

# Retrieve params
$ReportColumnID	= $ReportColumnID? $ReportColumnID : "0";
$ClassLevelID	= $ClassLevelID	? $ClassLevelID : "";
$ReportID		= $ReportID		? $ReportID : "";
$ClassID		= "";
$SubjectID		= "";
$StudentAry = array();
$ListAry = array();
$ListAry2 = array();
$DataTemp = array();
$ColTotal = array();
$LastGenerated = $lreportcard->GET_CURRENT_DATETIME();

$ExportArr = array();
$lexport = new libexporttext();

# check condition
if(!$ClassLevelID || !$ReportID)
{
	intranet_closedb();
	header("Location: index.php");
}

# Retrieve Student Array
$classArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID, $ClassID);

foreach($classArr as $key => $data)
{
	$thisClassID = $data['ClassID'];
	$StudentAryTemp = $lreportcard->GET_STUDENT_BY_CLASS($thisClassID);
	foreach($StudentAryTemp as $k=>$d)
	{
		list($thisUserID, $WebSAMS, $ClassNumber, $EngName) = $d;
		$StudentAry[] = $thisUserID;
		
		$lu = new libuser($thisUserID);
		$DataTemp[$thisUserID]['StudentName'] = $lu->EnglishName ." (". $lu->ChineseName .")";
		$DataTemp[$thisUserID]['ClassName'] = $lu->ClassName;
//		$DataTemp[$thisUserID]['AdmissionNo'] = $lu->AdmissionNo;
		$DataTemp[$thisUserID]['AdmissionNo'] = strtoupper($lu->UserLogin);
		$DataTemp[$thisUserID]['ClassNumber'] = $lu->ClassNumber; 
		$DataTemp[$thisUserID]['Gender'] = $lu->Gender; 
	}
}

# Grand MS  Title
$Title = "Level_Ranking(for_Promotion)";
$thisLevelName = $lclass->getLevelName($ClassLevelID);
$thisAcadermicYear = getCurrentAcademicYear();
$filename = intranet_undo_htmlspecialchars($thisAcadermicYear."_".$thisLevelName."_".$Title.".csv");

# define column title
$exportColumn = array();
$exportColumn[]= $Title;
$numEmptyTitle = 7;
for ($i=0; $i<$numEmptyTitle; $i++)
{
	$exportColumn[]= "";
}

$iCounter = 0;
$jCounter = 0;

# School Year, Level
$ExportArr[$iCounter][$jCounter] = "School Year ".$thisAcadermicYear;
$jCounter++;		
$ExportArr[$iCounter][$jCounter] = "Level: ".$thisLevelName;
$jCounter++;
$iCounter++;

# Title Row
$jCounter = 0;
$ExportArr[$iCounter][$jCounter] = "Ranking";
$jCounter++;
$ExportArr[$iCounter][$jCounter] = "Name";
$jCounter++;
$ExportArr[$iCounter][$jCounter] = "Student No.";
$jCounter++;
$ExportArr[$iCounter][$jCounter] = "Gender";
$jCounter++;
$ExportArr[$iCounter][$jCounter] = "existing Class";
$jCounter++;
$ExportArr[$iCounter][$jCounter] = "Class Index";
$jCounter++;
$ExportArr[$iCounter][$jCounter] = "% Total";
$jCounter++;
$ExportArr[$iCounter][$jCounter] = "Promotion Status";
$jCounter++;
$iCounter++;
		
foreach($StudentAry as $key => $sid)
{
	$t = $lreportcard->getReportResultScore($ReportID, 0, $sid);
	$thisTargetMark = $lreportcard->Get_Raw_Or_Rounded_Mark($t['RawGrandAverage'], $t['GrandAverage']);
	
	$DataTemp[$sid]['Mark'] = my_round($thisTargetMark,1);
	$DataTemp[$sid]['NewClassName'] = $t['NewClassName'];
	
	$ColTotal[$ReportColumnID]['Avg'] += my_round($thisTargetMark,1);
	$ColTotal[$ReportColumnID]['no']++;
}

foreach($StudentAry as $k=>$stu)
{
	$thisStudentID = $stu;

	$ListAry[$thisStudentID]['StudentName'] = $DataTemp[$thisStudentID]['StudentName'];
	$ListAry[$thisStudentID]['AdmissionNo'] = $DataTemp[$thisStudentID]['AdmissionNo'];
	$ListAry[$thisStudentID]['Gender'] = $DataTemp[$thisStudentID]['Gender'];
	$ListAry[$thisStudentID]['ClassName'] = $DataTemp[$thisStudentID]['ClassName'];
	$ListAry[$thisStudentID]['ClassNumber'] = $DataTemp[$thisStudentID]['ClassNumber'];
	
	$ListAry[$thisStudentID]['Total'] = $DataTemp[$thisStudentID]['Mark'];
	$ListAry[$thisStudentID]['NewClassName'] = $DataTemp[$thisStudentID]['NewClassName'];
}

# sort by Marks
foreach ($ListAry as $key => $row) 
{
	$field1[$key] = $row['Total'];
}
array_multisort($field1, SORT_DESC, $ListAry);
$ListAry = array_merge($ListAry, $ListAry2);

$display = "";
$rankingCounter = 0;
$sameRankingCounter = 0;
$rowCounter = 0;
$lastOverall = 0;

if (!isset($iCounter))
	$iCounter = 0;

foreach($ListAry as $key=>$row)
{
	$jCounter = 1;	# $jCounter=0 reserved for ranking
	$jLocal = 0;
	
	$ki = 0;
	$thisRowOthersInfo = "";
	$thisRowRanking = "";
	foreach($row as $k => $d)	
	{
		
		$ExportArr[$iCounter][$jCounter] = $d;
		
		# updated on 11 Dec by Ivan - same ranking if the same % Total
		if ($jCounter == 6) // % Total
		{
			if ($d != $lastOverall)
			{
				$rankingCounter++;
				$rankingCounter += $sameRankingCounter;		// Ranking: 1, 2, 2, 4
				
				$sameRankingCounter = 0;
			}
			else
			{
				$sameRankingCounter++;
			}
			$ExportArr[$iCounter][0] = $rankingCounter;
			$lastOverall = $d;
		}
		
		$jCounter++;
	}
	$iCounter++;	
}
$jCounter = 0;

# Footer
$ExportArr[$iCounter][$jCounter] = "";
$jCounter++;

$ExportArr[$iCounter][$jCounter] = "Total Students: ".sizeof($StudentAry);
$jCounter++;

$ExportArr[$iCounter][$jCounter] = "";
$jCounter++;
$ExportArr[$iCounter][$jCounter] = "";
$jCounter++;
$ExportArr[$iCounter][$jCounter] = "";
$jCounter++;

$ExportArr[$iCounter][$jCounter] = "Average:";
$jCounter++;
$ExportArr[$iCounter][$jCounter] = @my_round(($ColTotal[$ReportColumnID]['Avg'] / $ColTotal[$ReportColumnID]['no']), 1);
$jCounter++;

# Date and Time
$iCounter++;
$jCounter = 0;
$ExportArr[$iCounter][$jCounter] = $LastGenerated;

$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "", "\r\n", "", 0, "11");
intranet_closedb();

// Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);
?>

