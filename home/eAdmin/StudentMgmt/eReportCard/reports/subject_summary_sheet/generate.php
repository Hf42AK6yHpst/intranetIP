<?
/* Modifying: ivan
 * 
 * Log:
 * 
 * 2019-03-12 Bill [2019-0312-1107-21066]
 *  - fixed division by zero problem
 *  
 * 2011-05-13 Ivan [2011-0512-1435-17067]
 * 	- added Specified Passing Percentage Logic
 * 
 */
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$ViewFormat = $_POST['ViewFormat'];
$YearID = $_POST['YearID'];
$ReportID = $_POST['ReportID'];
$TargetIDArr = $_POST['YearClassIDArr'];
$SubjectIDArr = $_POST['SubjectIDArr'];
$SpecifiedPassingPercentage = $_POST['SpecifiedPassingPercentage'];

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	if ($lreportcard->hasAccessRight()) 
	{
		
		$numOfTarget = count($TargetIDArr);
		$ClassNameArr= array();
		$ClassIDReportIDMap = array(); 
		$SubjectNameArr = array();
		
		$thisReportID = $ReportID;
		
		# Get Special Case
		$SpecialCaseArr1 = $lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET1();
		$SpecialCaseArr2 = $lreportcard->GET_POSSIBLE_MARKSHEET_SPECIAL_CASE_SET2();

		# Get MarkAry
		$MarkAry = $lreportcard->getMarks($thisReportID); 
		
		# Get Grading Scheme Info 
		$thisClassLevelID = $YearID;
		$GradingInfo = $lreportcard->Get_Form_Grading_Scheme($thisClassLevelID, $thisReportID); 
		
		if($ViewBy == "class")
		{
			for ($j=0; $j<$numOfTarget; $j++)
			{
				$thisClassID = $TargetIDArr[$j];
				
				$thisStudentInfoArr = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($thisReportID, $thisClassLevelID, $thisClassID);
				
				// ignore class / form with no student
				if (!is_array($thisStudentInfoArr) || count($thisStudentInfoArr)==0)
					continue;
					
				# Loop each student		
				$numOfStudent = count($thisStudentInfoArr);		
				
				foreach((array)$SubjectIDArr as $thisSubjectID)
				{
					for ($i=0; $i<$numOfStudent; $i++)
					{
						
						$thisStudentID = $thisStudentInfoArr[$i]["UserID"];
						$thisColumnInfo = $MarkAry[$thisStudentID][$thisSubjectID];
	
						if(!in_array($thisSubjectID,$SubjectIDArr)) continue;
						$thisSchemeID = $GradingInfo[$thisSubjectID]['SchemeID'];
						
						if( $thisColumnInfo[0]["Grade"] == "N.A.")
						{
							$thisMark = $thisGrade = $thisRank = $thisFormRank = $lreportcard->EmptySymbol;
						}
						else
						{
							$thisMark = my_round($thisColumnInfo[0]["RawMark"], 2);
							if(!$thisColumnInfo[0]["Grade"])
								$thisGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($thisSchemeID, $thisMark,$thisReportID,$thisStudentID,$thisSubjectID,$thisClassLevelID,0);
							else if(!$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1($thisColumnInfo[0]["Grade"]) && !$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2($thisColumnInfo[0]["Grade"]) )
								$thisGrade = $thisColumnInfo[0]["Grade"];
							else
								$thisGrade = $lreportcard->EmptySymbol;
							$thisGrade = $thisColumnInfo[0]["Grade"]?$thisColumnInfo[0]["Grade"]:$lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($thisSchemeID, $thisMark,$thisReportID,$thisStudentID,$thisSubjectID,$thisClassLevelID,0);
							$thisRank = $thisColumnInfo[0]["OrderMeritClass"];
							$thisFormRank = $thisColumnInfo[0]["OrderMeritForm"];
							
							if(!empty($thisMark) && !in_array($thisGrade, $SpecialCaseArr1) && !in_array($thisGrade, $SpecialCaseArr2)) $MarksForStat[$thisSubjectID][$thisClassID][] = $thisMark;
						}
						
						if(!$lreportcard->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($thisSubjectID)) 
						{
							$resultArr[$thisSubjectID][$thisClassID][$thisStudentID]['Mark'] = $thisMark;
							$resultArr[$thisSubjectID][$thisClassID][$thisStudentID]['Grade'] = $thisGrade;
							$resultArr[$thisSubjectID][$thisClassID][$thisStudentID]['Rank'] = ($thisRank==-1)? $lreportcard->EmptySymbol : $thisRank;
							$resultArr[$thisSubjectID][$thisClassID][$thisStudentID]['FormRank'] = ($thisFormRank==-1)? $lreportcard->EmptySymbol : $thisFormRank;
							
						}
					}
				}
			}
		}
		else //View by Subject Group
		{
			foreach((array)$SubjectIDArr as $thisSubjectID)
			{
				$ReportSetting = $lreportcard->returnReportTemplateBasicInfo($ReportID);
				$thisClassLevelID = $ReportSetting["ClassLevelID"];			
				$SubjectGroupList = $lreportcard->Get_Subject_Group_List_Of_Report($ReportID,'',$thisSubjectID);
				
				foreach((array) $SubjectGroupList as $SubjectGroupID)
				{
					include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
					$scm = new subject_term_class($SubjectGroupID,1,1);
					
					$thisStudentInfoArr = $scm->ClassStudentList;
					$subjGroupNameList[$SubjectGroupID] = Get_Lang_Selection($scm->ClassTitleB5,$scm->ClassTitleEN);
					$ChineseNameAry = Get_Array_By_Key($scm->ClassTeacherList,"ChineseName");
					$EnglishNameAry = Get_Array_By_Key($scm->ClassTeacherList,"EnglishName");
					$NameList = Get_Lang_Selection($ChineseNameAry,$EnglishNameAry);
					$subjGroupTeacherList[$SubjectGroupID] = implode(",",(array)$NameList);
					$numOfStudent = count($thisStudentInfoArr);
					
					$thisNumOfSpecialPercentageStudent = 0;
					$thisNumOfPassedSpecialPercentageStudent = 0;
					for ($i=0; $i<$numOfStudent; $i++)
					{
						if($thisStudentInfoArr[$i]["YearID"] != $thisClassLevelID) continue;
						
						$thisStudentID = $thisStudentInfoArr[$i]["UserID"];
						$thisColumnInfo = $MarkAry[$thisStudentID][$thisSubjectID];
	
						if(!in_array($thisSubjectID,$SubjectIDArr)) continue;
						$thisSchemeID = $GradingInfo[$thisSubjectID]['SchemeID'];
						
						if( $thisColumnInfo[0]["Grade"] == "N.A.")
						{
							$thisMark = $thisGrade = $thisRank = $thisFormRank = $lreportcard->EmptySymbol;
						}
						else
						{
							$thisMark = my_round($thisColumnInfo[0]["RawMark"], 2);
							if(!$thisColumnInfo[0]["Grade"])
								$thisGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($thisSchemeID, $thisMark,$thisReportID,$thisStudentID,$thisSubjectID,$thisClassLevelID,0);
							else if(!$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1($thisColumnInfo[0]["Grade"]) && !$lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2($thisColumnInfo[0]["Grade"]) )
								$thisGrade = $thisColumnInfo[0]["Grade"];
							else
								$thisGrade = $lreportcard->EmptySymbol;
								
							$thisRank = $thisColumnInfo[0]["OrderMeritSubjectGroup"];
							$thisFormRank = $thisColumnInfo[0]["OrderMeritForm"];
							
							if(!empty($thisMark) && !in_array($thisGrade, $SpecialCaseArr1) && !in_array($thisGrade, $SpecialCaseArr2)) $MarksForStat[$thisSubjectID][$SubjectGroupID][] = $thisMark;
						}
						
						if(!$lreportcard->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($thisSubjectID)) 
						{
							$resultArr[$thisSubjectID][$SubjectGroupID][$thisStudentID]['Mark'] = $thisMark;
							$resultArr[$thisSubjectID][$SubjectGroupID][$thisStudentID]['Grade'] = $thisGrade;
							$resultArr[$thisSubjectID][$SubjectGroupID][$thisStudentID]['Rank'] = ($thisRank==-1)? $lreportcard->EmptySymbol : $thisRank;
							$resultArr[$thisSubjectID][$SubjectGroupID][$thisStudentID]['FormRank'] = ($thisFormRank==-1)? $lreportcard->EmptySymbol : $thisFormRank;
						}
					}
				}
			}	
		}
	
		//debug_pr($resultArr); die;
		## Display the result
		########################################### html ###########################################
		if ($ViewFormat == 'html')
		{
			//$studentCtr=0;
			$thisClassName='';
				
			foreach((array)$resultArr as $thisSubjectID => $SubjectSummary)
			{
				foreach((array)$SubjectSummary as $thisClassGroupID => $thisStudentResultArr)
				{
					#Get Class Name
					$thisClassInfo = $lreportcard->GET_CLASSES_BY_FORM($thisClassLevelID,$thisClassGroupID);
					$thisClassName = $thisClassInfo[0]["ClassTitleEN"];
					
					### Special Passing Percentage Handling
					$thisNumOfPassedSpecificMark = 0;
					$thisNumOfMark = count((array)$MarksForStat[$thisSubjectID][$thisClassGroupID]);
					$thisPassingMark = $GradingInfo[$thisSubjectID]['FullMark'] * ($SpecifiedPassingPercentage / 100);
					for ($i=0; $i<$thisNumOfMark; $i++)
					{
						$thisMark = $MarksForStat[$thisSubjectID][$thisClassGroupID][$i];
						if ($thisMark >= $thisPassingMark) {
							$thisNumOfPassedSpecificMark++;
						}
					}
					
					if ($thisNumOfMark > 0) {
						$thisPassingRateSpecific = my_round(($thisNumOfPassedSpecificMark / $thisNumOfMark) * 100, 2);
					}
					else {
						$thisPassingRateSpecific = my_round(0, 2);
					}
					 
						
					
					#Subject Title
					$thead = "<thead>";
						$thead .= "<tr>";
							$thead .= "<td colspan='6'>";
								$thead .= "<table cellpadding=0 cellspacing=0 border=0 width='100%'>";
									$thead .= "<tr>";
										$thead .= "<td>".$eReportCard["Subject"].": ".$lreportcard->GET_SUBJECT_NAME_LANG($thisSubjectID)."</td>";
									$thead .= "</tr>";
									$thead .= "<tr>";
										if($ViewBy=='subjgroup')
										{
											$thead .= "<td align='right' width='50%'>".$subjGroupTeacherList[$thisClassGroupID]."</td>";
											$thead .= "<td align='right' width='50%'>".$eReportCard["SubjectGroup"].": ".$subjGroupNameList[$thisClassGroupID]."</td>";
										}
										else
											$thead .= "<td align='right'>".$eReportCard["Class"].": ".$thisClassName."</td>";
									$thead .= "</tr>";
								$thead .= "</table>";
							$thead .= "</td>";
						$thead .= "</tr>";
						$thead .= "<tr>";
							$thead .= "<td width='15%'>".$eReportCard["ClassNo"]."</td>";
							$thead .= "<td>".$eReportCard['Template']['StudentInfo']['Name']."</td>";
							$thead .= "<td width='15%' align='center'>".$eReportCard['Mark']."</td>";
							$thead .= "<td width='15%' align='center'>".$eReportCard['Grade']."</td>";
							$thead .= "<td width='15%' align='center'>".($ViewBy=='subjgroup'?$eReportCard['Template']['SubjGroupPosition']:$eReportCard['Template']['ClassPosition'])."</td>";
							$thead .= "<td width='15%' align='center'>".$eReportCard['Template']['FormPosition']."</td>";
						$thead .= "</tr>";
					$thead .= "</thead>";
					
					$ResultDetail ='';
					foreach((array)$thisStudentResultArr as $thisStudentID => $thisStudentResult)
					{
						$lu = new libuser($thisStudentID);
						$thisEnglishName = Get_Lang_Selection($lu->ChineseName,$lu->EnglishName);
						
						$thisStudentInfoArr = $lreportcard->Get_Student_Class_ClassLevel_Info($thisStudentID);
						$thisClassNumber = $thisStudentInfoArr[0]['ClassNumber'];
						
						if($ViewBy == "subjgroup")
						{
							$thisClassName = $thisStudentInfoArr[0]['ClassName'];
							$thisClassNumber = "$thisClassName($thisClassNumber)";
						}
						
						$ResultDetail .= "<tr>";
							$ResultDetail .= "<td>$thisClassNumber</td>";
							$ResultDetail .= "<td>$thisEnglishName</td>";
							$ResultDetail .= "<td align='center'>".$thisStudentResult['Mark']."</td>";
							$ResultDetail .= "<td align='center'>".$thisStudentResult['Grade']."</td>";
							$ResultDetail .= "<td align='center'>".$thisStudentResult['Rank']."</td>";
							$ResultDetail .= "<td align='center'>".$thisStudentResult['FormRank']."</td>";
						$ResultDetail .= "</tr>";
					}
					
					//$thisReportID = $ClassIDReportIDMap[$thisClassGroupID];
					$forSubjectGroup = $ViewBy=='subjgroup'?1:0;
					$ResultStat = $lreportcard->Get_Scheme_Grade_Statistics_Info($thisReportID, $thisSubjectID, $forSubjectGroup);
										
					$ResultSummary = "<tr>";
						$ResultSummary .= "<td>".$eReportCard['Grade']."</td>";
						$ResultSummary .= "<td>".$eReportCard['Mark']."</td>";
						$ResultSummary .= "<td align='center'>".$eReportCard['Total']."</td>";
						$ResultSummary .= "<td align='center'>".$eReportCard['SubjectSummarySheet']['Percentage']."</td>";
						$ResultSummary .= "<td>&nbsp;</td>";
						$ResultSummary .= "<td>&nbsp;</td>";
					$ResultSummary .= "</tr>";
					
					$thisClassMax = null;
					$thisClassMin = null;
					
					$thisPassRate=0;
					foreach((array)$ResultStat as $thisGrade => $ClassDetail)
					{
						$thisMinMark = my_round($ClassDetail[$thisClassGroupID]['MinMark'],2);
						$thisMaxMark = my_round($ClassDetail[$thisClassGroupID]['MaxMark'],2);
						$thisPercent = $ClassDetail[$thisClassGroupID]['Percentage'];
						$thisPercentRaw = $ClassDetail[$thisClassGroupID]['PercentageRaw'];
						$thisNoOfStudent = $ClassDetail[$thisClassGroupID]['NumOfStudent'];
					
						if($ClassDetail[0]['Nature']!='F')	$thisPassRate += $thisPercentRaw;
						
						if(($thisMaxMark>$thisClassMax || !$thisClassMax)&& $thisNoOfStudent!=0) $thisClassMax = $thisMaxMark;
						if(($thisMinMark<$thisClassMin || !$thisClassMin)&& $thisNoOfStudent!=0)$thisClassMin = $thisMinMark;
						
						$ResultSummary .= "<tr>";
							$ResultSummary .= "<td>$thisGrade</td>";
							$ResultSummary .= "<td>".($thisNoOfStudent==0?$lreportcard->EmptySymbol:($thisMinMark==$thisMaxMark?$thisMaxMark:"$thisMinMark - $thisMaxMark"))."</td>";
							$ResultSummary .= "<td align='center'>$thisNoOfStudent</td>";
							$ResultSummary .= "<td align='center'>$thisPercent</td>";
							$ResultSummary .= "<td>&nbsp;</td>";
							$ResultSummary .= "<td>&nbsp;</td>";
						$ResultSummary .= "</tr>";
						
					}
					
					# added by ivan on 20091231 (return 4 d.p. from the function, round here for more accurate percentage)
					$thisPassRate = my_round($thisPassRate, 2);
				
					# Class Max & Min Mark
					$ResultSummary .= "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>"; // empty row;
					$ResultSummary .= "<tr>";
						$ResultSummary .= "<td>".$eReportCard['SubjectSummarySheet']['MaxMark']."</td>";
						$ResultSummary .= "<td>".($thisClassMax>=0?$thisClassMax:'-')."</td>";
						$ResultSummary .= "<td align='center'>".$eReportCard['SubjectSummarySheet']['MinMark']."</td>";
						$ResultSummary .= "<td align='center'>".($thisClassMin>=0?$thisClassMin:'-')."</td>";
						$ResultSummary .= "<td>&nbsp;</td>";
						$ResultSummary .= "<td>&nbsp;</td>";
					$ResultSummary .= "</tr>";					

					# SD and Average 
					$thisSD = $thisAVG = 0;
					if(!empty($MarksForStat[$thisSubjectID][$thisClassGroupID]))
					{
						$thisSD = $lreportcard->getSD($MarksForStat[$thisSubjectID][$thisClassGroupID],1);
						$thisAVG = $lreportcard->getAverage($MarksForStat[$thisSubjectID][$thisClassGroupID],1);
					}
					
					$ResultSummary .= "<tr>";
						$ResultSummary .= "<td>".$eReportCard['Template']['SD']."</td>";
						$ResultSummary .= "<td>".($thisSD>=0?$thisSD:'-')."</td>";
						$ResultSummary .= "<td align='center'>".$eReportCard['Template']['Average']."</td>";
						$ResultSummary .= "<td align='center'>".($thisAVG>=0?$thisAVG:'-')."</td>";
						$ResultSummary .= "<td>&nbsp;</td>";
						$ResultSummary .= "<td>&nbsp;</td>";
					$ResultSummary .= "</tr>";
					
					#Pass rate
					$ResultSummary .= "<tr>";
						$ResultSummary .= "<td>".$eReportCard['PassRate']."</td>";
						$ResultSummary .= "<td>".($thisPassRate>=0?$thisPassRate:'-')."</td>";
						$ResultSummary .= "<td align='center'>&nbsp;</td>";
						$ResultSummary .= "<td align='center'>&nbsp;</td>";
						$ResultSummary .= "<td>&nbsp;</td>";
						$ResultSummary .= "<td>&nbsp;</td>";
					$ResultSummary .= "</tr>";		
					
					#Specific Pass rate
					if ($eRCTemplateSetting['SubjectSummarySheet']['PassingNumberDetermineByPercentage'])
					{
						$ResultSummary .= "<tr>";
							$ResultSummary .= "<td>".$eReportCard['PassRate']." (".$SpecifiedPassingPercentage.$eReportCard['SubjectSummarySheet']['%_OfFullMark'].")</td>";
							$ResultSummary .= "<td>".($thisPassingRateSpecific>=0?$thisPassingRateSpecific:'-')."</td>";
							$ResultSummary .= "<td align='center'>&nbsp;</td>";
							$ResultSummary .= "<td align='center'>&nbsp;</td>";
							$ResultSummary .= "<td>&nbsp;</td>";
							$ResultSummary .= "<td>&nbsp;</td>";
						$ResultSummary .= "</tr>";
					}

					$page_break_css = "style='page-break-after:always'";
					$table .= "<table id='ResultTable' class='GrandMSContentTable' width='80%' border='1' cellpadding='2' cellspacing='0' $page_break_css>\n";
						$table .= $thead;	
						$table .= $ResultDetail;
						$table .= "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>"; // empty row
						$table .= $ResultSummary;
					$table .= "</table><br><br>\n";
				}
			}
			
			$css = $lreportcard->Get_GrandMS_CSS();
			
			$allTable = '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>'.$table.'</td>
							</tr>
						</table>';
			
			echo $lreportcard->Get_Report_Header($ReportTitle);
			echo $table.$css;
			echo $lreportcard->Get_Report_Footer();
		}
		else if ($ViewFormat == 'csv')
		########################################### csv ###########################################
		{
			include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
			include_once($PATH_WRT_ROOT."includes/libexporttext.php");
			
			$ExportHeaderArr = array();
			$ExportRowArr = array();
			$lexport = new libexporttext();
			
			$thisClassName='';
			$forSubjectGroup = $ViewBy=='subjgroup'?1:0;
			
			foreach((array)$resultArr as $thisSubjectID => $SubjectSummary)
			{
				foreach((array)$SubjectSummary as $thisClassGroupID => $thisStudentResultArr)
				{
					#Get Class Name
					$thisClassInfo = $lreportcard->GET_CLASSES_BY_FORM($thisClassLevelID,$thisClassGroupID);
					$thisClassName = $thisClassInfo[0]["ClassTitleEN"];
					
					### Special Passing Percentage Handling
					$thisNumOfPassedSpecificMark = 0;
					$thisNumOfMark = count((array)$MarksForStat[$thisSubjectID][$thisClassGroupID]);
					$thisPassingMark = $GradingInfo[$thisSubjectID]['FullMark'] * ($SpecifiedPassingPercentage / 100);
					for ($i=0; $i<$thisNumOfMark; $i++)
					{
						$thisMark = $MarksForStat[$thisSubjectID][$thisClassGroupID][$i];
						if ($thisMark >= $thisPassingMark) {
							$thisNumOfPassedSpecificMark++;
						}
					}
					
					// [2019-0312-1107-21066]
					//$thisPassingRateSpecific = my_round(($thisNumOfPassedSpecificMark / $thisNumOfMark) * 100, 2);
					if ($thisNumOfMark > 0) {
					    $thisPassingRateSpecific = my_round(($thisNumOfPassedSpecificMark / $thisNumOfMark) * 100, 2);
					}
					else {
					    $thisPassingRateSpecific = my_round(0, 2);
					}
					
					#Subject Title
					$ExportColArr = array();
					$ExportColArr[] = $eReportCard["Subject"];
					if ($eRCTemplateSetting['Report']['SubjectSummarySheet']['ExportUserLogin']) {
						$ExportColArr[] = $Lang['General']['UserLogin'];
					}
					$ExportColArr[] = $eReportCard["Class"];
					$ExportColArr[] = $eReportCard["ClassNo"];
					$ExportColArr[] = $eReportCard['Template']['StudentInfo']['Name'];
					$ExportColArr[] = $eReportCard["Mark"];
					$ExportColArr[] = $eReportCard["Grade"];
					$ExportColArr[] = $forSubjectGroup?$eReportCard['Template']['SubjGroupPosition']:$eReportCard['Template']['ClassPosition'];
					$ExportColArr[] = $eReportCard['Template']['FormPosition'];
					
					$ExportRowArr[] = $ExportColArr;
					
					$ResultDetail ='';
					foreach((array)$thisStudentResultArr as $thisStudentID => $thisStudentResult)
					{
						$lu = new libuser($thisStudentID);
						$thisEnglishName = Get_Lang_Selection($lu->ChineseName,$lu->EnglishName);
						$thisStudentInfoArr = $lreportcard->Get_Student_Class_ClassLevel_Info($thisStudentID);
						$thisClassNumber = $thisStudentInfoArr[0]['ClassNumber'];
						$thisClassName = $thisStudentInfoArr[0]['ClassName'];
						
						$ExportColArr = array();
						$ExportColArr[] =  $lreportcard->GET_SUBJECT_NAME_LANG($thisSubjectID);
						if ($eRCTemplateSetting['Report']['SubjectSummarySheet']['ExportUserLogin']) {
							$ExportColArr[] = $lu->UserLogin;
						}
						$ExportColArr[] =  $thisClassName;
						$ExportColArr[] =  $thisClassNumber;
						$ExportColArr[] =  $thisEnglishName;
						$ExportColArr[] =  $thisStudentResult['Mark'];
						$ExportColArr[] =  $thisStudentResult['Grade'];
						$ExportColArr[] =  $thisStudentResult['Rank'];
						$ExportColArr[] =  $thisStudentResult['FormRank'];
						
						$ExportRowArr[] = $ExportColArr;
					}
					
					//$thisReportID = $ClassIDReportIDMap[$thisClassGroupID];
					
					$ResultStat = $lreportcard->Get_Scheme_Grade_Statistics_Info($thisReportID, $thisSubjectID,$forSubjectGroup);
					
					$ExportColArr = array();
					$ExportColArr[] =  $eReportCard['Grade'];
					$ExportColArr[] =  $eReportCard['Mark'];
					$ExportColArr[] =  $eReportCard['Total'];
					$ExportColArr[] =  $eReportCard['SubjectSummarySheet']['Percentage'];
					
					$ExportRowArr[] = $ExportColArr;
					
					$thisClassMax = null;
					$thisClassMin = null;
					$thisPassRate = 0;
					foreach((array)$ResultStat as $thisGrade => $ClassDetail)
					{
						$thisMinMark = my_round($ClassDetail[$thisClassGroupID]['MinMark'],2);
						$thisMaxMark = my_round($ClassDetail[$thisClassGroupID]['MaxMark'],2);
						$thisPercentRaw = $ClassDetail[$thisClassGroupID]['PercentageRaw'];
						$thisPercent = $ClassDetail[$thisClassGroupID]['Percentage'];
						$thisNoOfStudent = $ClassDetail[$thisClassGroupID]['NumOfStudent'];
						
						if($ClassDetail[0]['Nature']!='F')	$thisPassRate += $thisPercentRaw;
						
						if(($thisMaxMark>$thisClassMax || !$thisClassMax)&& $thisNoOfStudent!=0) $thisClassMax = $thisMaxMark;
						if(($thisMinMark<$thisClassMin || !$thisClassMin)&& $thisNoOfStudent!=0)$thisClassMin = $thisMinMark;
						
						$ExportColArr = array();
						
						$ExportColArr[] = $thisGrade;
						$ExportColArr[] = ($thisNoOfStudent==0?$lreportcard->EmptySymbol:($thisMinMark==$thisMaxMark?$thisMaxMark:"$thisMinMark - $thisMaxMark"));
						$ExportColArr[] = $thisNoOfStudent;
						$ExportColArr[] = $thisPercent;

						$ExportRowArr[] = $ExportColArr;
					}
					
					# added by ivan on 20091231 (return 4 d.p. from the function, round here for more accurate percentage)
					$thisPassRate = my_round($thisPassRate, 2);
				
					# Class Max & Min Mark
					$ExportColArr = array();
					$ExportColArr[] =  $eReportCard['SubjectSummarySheet']['MaxMark'];
					$ExportColArr[] =  ($thisClassMax>=0?$thisClassMax:'-');
					$ExportColArr[] =  $eReportCard['SubjectSummarySheet']['MinMark'];
					$ExportColArr[] =  ($thisClassMin>=0?$thisClassMin:'-');
					$ExportRowArr[] = $ExportColArr;
					
					# SD and Average 
					$thisSD = $lreportcard->getSD($MarksForStat[$thisSubjectID][$thisClassGroupID],1);
					$thisAVG = $lreportcard->getAverage($MarksForStat[$thisSubjectID][$thisClassGroupID],1);
					
					$ExportColArr = array();
					$ExportColArr[] =  $eReportCard['Template']['SD'];
					$ExportColArr[] =  ($thisSD>=0?$thisSD:'-');
					$ExportColArr[] =  $eReportCard['Template']['Average'];
					$ExportColArr[] =  ($thisAVG>=0?$thisAVG:'-');
					$ExportRowArr[] = $ExportColArr;
					
					# Pass Rate
					$ExportColArr = array();
					$ExportColArr[] =  $eReportCard['PassRate'];
					$ExportColArr[] =  ($thisPassRate>=0?$thisPassRate:'-');
					$ExportRowArr[] = $ExportColArr;
					
					#Specific Pass rate
					if ($eRCTemplateSetting['SubjectSummarySheet']['PassingNumberDetermineByPercentage'])
					{
						$ExportColArr = array();
						$ExportColArr[] =  $eReportCard['PassRate']." (".$SpecifiedPassingPercentage.$eReportCard['SubjectSummarySheet']['%_OfFullMark'].")";
						$ExportColArr[] =  ($thisPassingRateSpecific>=0?$thisPassingRateSpecific:'-');
						$ExportRowArr[] = $ExportColArr;
					}
					
					$ExportRowArr[] = array("");
				}
			}
			
			
			// Title of the grandmarksheet
			//$ReportTitle = str_replace(" ", "_", $ReportTitle);
			$filename = "subject_summary_sheet.csv";
			//debug_pr($ExportRowArr);
			$export_content = $lexport->GET_EXPORT_TXT($ExportRowArr,'');
			
			intranet_closedb();
			
			// Output the file to user browser
			$lexport->EXPORT_FILE($filename, $export_content);
		}
	}
}


?>