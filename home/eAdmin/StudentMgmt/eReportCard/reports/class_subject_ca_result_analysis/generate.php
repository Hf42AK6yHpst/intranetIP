<?
//using: Bill

//@SET_TIME_LIMIT(1000);

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
	
$lreportcard = new libreportcardcustom();
$lreportcard_ui = new libreportcard_ui();

$x = "";
$emptyDisplay = "---";
$exportArr = array();
$exportColumn = array();

$isFirst = true;
$table_count = 0;

$MSScoreArr = array();
$MSStatArr = array();
$SubMSScoreArr = array();
$SubMSStatArr = array();
$fullMarkArr = array();
$TermReportColumnArr = array();
$subjectTeacherArr = array();
$statTitleArr = array('最高分', '最低分', '平均分', '及格率');

# POST
$YearID = $_POST['YearID'];
$ReportID = $_POST['ReportID'];
$SubjectIDArr = $_POST['SubjectIDArr'];
$exportCSV = $_POST['submit_type'];

// [2015-1209-1703-15206]
if ($_SESSION["SSV_PRIVILEGE"]["reportcard"]["is_subject_panel"] || $lreportcard->hasAccessRight()) {
     	
	# Report Template Info
    $ReportTemplate = $lreportcard->returnReportTemplateBasicInfo($ReportID);
    $ReportSemester = $ReportTemplate['Semester'];
     	     	
	# Semester
    $Semester = $lreportcard->GET_ALL_SEMESTERS();
    if($ReportSemester != 'F'){
       	$Semester = array();
		$Semester[$ReportSemester] = $lreportcard->GET_ALL_SEMESTERS($ReportSemester);
	}
	
	$currentForm = $lreportcard->returnClassLevel($YearID);
    $Subjects = $lreportcard->returnSubjectwOrder($YearID, 0, '', 'b5', '', 0, $ReportID, 0);
	
	# Year Class
	$YearClassNameArr = $lreportcard->GET_CLASSES_BY_FORM($YearID, "", 1, 1);
	foreach($YearClassIDArr as $YearClassID){
		$students = $lreportcard->GET_STUDENT_BY_CLASS($YearClassID, $ParStudentIDList="", $isShowBothLangs=0, $withClassNumber=0, $isShowStyle=0, $ReturnAsso=1);
		$classStudentArr[$YearClassID] = $students;
	}

	# Related Reports
	$relatedReports = $lreportcard->Get_Related_TermReport_Of_Consolidated_Report($ReportID, $MainReportOnly=1, $ReportColumnID="ALL");
	// For term report, only process 1 report 
	if($ReportTemplate['Semester'] != 'F'){
		$relatedReports = array();
		$relatedReports[0] = $ReportTemplate;
	}
	
	# Loop Reports
	// Build settings array
	for($i=0; $i<count($relatedReports); $i++)
	{
		# Related Report Info
		$relatedReportID = $relatedReports[$i]['ReportID'];
		$TermID = $relatedReports[$i]['Semester'];
		$grading_scheme = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($YearID, 0, $relatedReportID);
		
		# ReportColumns
		$ReportColumn = $lreportcard->returnReportTemplateColumnData($relatedReportID);
		$TermReportColumnArr[$TermID] = BuildMultiKeyAssoc($ReportColumn, array('ReportColumnID'));
		
		$subjectList = array();
		foreach ((array)$ReportColumn as $ReportColumnObj)
		{
			# ReportColumn Info
			$ReportColumnID = $ReportColumnObj['ReportColumnID'];
			$ReportColumnInfo = $lreportcard->GET_SUB_MARKSHEET_COLUMN_INFO('', $ReportColumnID);
			
			# Loop Submarksheet Column
			for($j=0; $j<count($ReportColumnInfo); $j++)
			{
				# Submarksheet Column Info
				$columnSettings = $ReportColumnInfo[$j];
				$columnID = $columnSettings['ColumnID'];
				$SubjectID = $columnSettings['SubjectID'];
				$subjectList[] = $SubjectID;
				
				$subjectGrading = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($grading_scheme[$SubjectID]['schemeID']);
				
				$allColumns[$TermID][$ReportColumnID][$SubjectID][] = $columnSettings;
				$fullMarkArr[$TermID][$ReportColumnID][$SubjectID] = $subjectGrading;
				
				# Set Parent and Component Subject ID
				$componentSubjectID = 0;
				$parentSubjectID = $lreportcard->GET_PARENT_SUBJECT_ID($SubjectID);
				// for component subject
				if($parentSubjectID){
					$componentSubjectID = $SubjectID;
					$SubjectID = $parentSubjectID;
					$subjectList[] = $SubjectID;
				}
				
				// if current subject not in selected subjects
				if(!in_array($SubjectID, $SubjectIDArr)){
					continue;
				}
				
				# Submarksheet Score
				$SubMSScore = $lreportcard->GET_SUB_MARKSHEET_SCORE($ReportColumnID, ($componentSubjectID? $componentSubjectID : $SubjectID));
				$SubMSScoreArr[$TermID][$ReportColumnID][$SubjectID][$componentSubjectID] = $SubMSScore;
				
				foreach((array)$YearClassIDArr as $YearClassID){
					$studentList = array_keys((array)$classStudentArr[$YearClassID]);
					
					$Marks = array();
					$minMark = 0;
					$maxMark = 0;
					$averageMark = 0.00;
					$passRatio = 0.00;
					$passStudent = 0;
					$totalStudent = 0;
					foreach((array)$studentList as $currentStudentID){
						$studentInput = $SubMSScore[$currentStudentID];

						$score = $studentInput[$columnID];
						if(isset($score) && $score != '-3'){
							$Marks[] = $score;
							$totalStudent++;
							
							if($score >= $columnSettings['PassMark']){
								$passStudent++;
							}
						}
					}
					
					if(count($Marks) > 0){
						$minMark = min($Marks);
						$maxMark = max($Marks);
						$averageMark = $lreportcard->getAverage($Marks);
						$averageMark = my_round($averageMark, 2);
						$passRatio = $passStudent / $totalStudent * 100;
						$passRatio = my_round($passRatio, 2);
					}
					$SubMSStatArr[$YearClassID][$TermID][$SubjectID][$componentSubjectID][$columnID] = array($maxMark, $minMark, $averageMark, $passRatio);
				}
				
				# Marksheet Score
				foreach((array)$YearClassIDArr as $YearClassID){
					$studentList = array_keys((array)$classStudentArr[$YearClassID]);
					$MarkSheetScore = $lreportcard->getMarks($relatedReportID, '', " AND a.StudentID IN ('".implode("','", $studentList)."')");
					$MSScoreArr[$YearClassID][$TermID] = $MarkSheetScore;
						
					if(!isset($subjectTeacherArr[$YearClassID][$ReportColumnID][$SubjectID])){
						$subjectTeacher = $lreportcard->returnSubjectTeacher($YearClassID, $SubjectID, $relatedReportID, $studentList[0], 1);
						$subjectTeacherArr[$YearClassID][$ReportColumnID][$SubjectID] = $subjectTeacher[0]['ChineseName'];
					}
							
//					$Marks = array();
//					$minMark = 0;
//					$maxMark = 0;
//					$passRatio = 0.00;
//					$passStudent = 0;
//					$averageMark = 0.00;
//					$totalStudent = 0;
//					foreach((array)$studentList as $currentStudentID){
//						$studentInput = $MarkSheetScore[$currentStudentID][$componentSubjectID][$ReportColumnID];
//						
//						if($studentInput['Grade'] != 'N.A.'){
//							$score = $studentInput['Mark'];
//							$Marks[] = $score;
//							$totalStudent++;
//							
//							if($score > $subjectGrading['PassMark']){
//								$passStudent++;
//							}
//						}
//					}
//					if(count($Marks) > 0){
//						$minMark = min($Marks);		
//						$maxMark = max($Marks);
//						$averageMark = $lreportcard->getAverage($Marks);
//						$averageMark = my_round($averageMark, 2);
//						$passRatio = $passStudent / $totalStudent * 100;
//						$passRatio = my_round($passRatio, 2);
//					}
//					$MSStatArr[$YearClassID][$TermID][$SubjectID][$componentSubjectID][$ReportColumnID] = array($maxMark, $minMark, $averageMark, $passRatio);
					
					# Parent Subject
					if(!isset($MSStatArr[$YearClassID][$TermID][$SubjectID][0][$ReportColumnID])){
						$parentSubjectGrading = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($grading_scheme[$SubjectID]['schemeID']);
						$fullMarkArr[$TermID][$ReportColumnID][$SubjectID] = $parentSubjectGrading;
						
//						$Marks = array();
//						$minMark = 0;
//						$maxMark = 0;
//						$passRatio = 0.00;
//						$passStudent = 0;
//						$averageMark = 0.00;
//						$totalStudent = 0;
//						foreach((array)$studentList as $currentStudentID){
//							$studentInput = $MarkSheetScore[$currentStudentID][$SubjectID][$ReportColumnID];
//							
//							if($studentInput['Grade'] != 'N.A.'){
//								$score = $studentInput['Mark'];
//								$Marks[] = $score;
//								$totalStudent++;
//								
//								if($score > $parentSubjectGrading['PassMark']){
//									$passStudent++;
//								}
//							}
//						}
//						if(count($Marks) > 0){
//							$minMark = min($Marks);		
//							$maxMark = max($Marks);
//							$averageMark = $lreportcard->getAverage($Marks);
//							$averageMark = my_round($averageMark, 2);
//							$passRatio = $passStudent / $totalStudent * 100;
//							$passRatio = my_round($passRatio, 2);
//						}
//						$MSStatArr[$YearClassID][$TermID][$SubjectID][0][$ReportColumnID] = array($maxMark, $minMark, $averageMark, $passRatio);
					}
				}
			}
		}
		
		$subjectList = (array)array_unique((array)$subjectList);
		sort($subjectList);
		$MarkStatistics[$TermID] = $lreportcard->Get_Mark_Statistics_Info($relatedReportID, $subjectList, 2, 2, 2, 0, 1, "", 0);
	}
    
//	$x .= "<div class='main_container'>"; 

	foreach((array)$YearClassIDArr as $YearClassID){
		$currentClassName = $YearClassNameArr[$YearClassID];
		$currentClassStudent = $classStudentArr[$YearClassID];
		
		foreach((array)$allColumns as $termID => $columnInfo){
			$SemesterSq = $lreportcard->Get_Semester_Seq_Number($termID);
		
			foreach((array)$SubjectIDArr as $parentSubjectID){
				$componentSubject = $Subjects[$parentSubjectID];
				$parentSubjectName = $componentSubject[0];
				
				foreach((array)$componentSubject as $subjectComponentID => $subjectName){
		
					$assessmentSq = 1;
					foreach((array)$columnInfo as $currentReportColumnID => $currentReportColumn){
						$currentColumn = $currentReportColumn[($subjectComponentID? $subjectComponentID : $parentSubjectID)];
						$currentReportColumnName = $TermReportColumnArr[$termID][$currentReportColumnID]['ColumnTitle'];
						$currentSubjectTeacher = $subjectTeacherArr[$YearClassID][$currentReportColumnID][$parentSubjectID];
						$assessmentCode = "T".$SemesterSq."A".$assessmentSq;
						
//						if(!$isFirst){
//							$x .= "</div>";
//							$x .= "<div class='page_break'>&nbsp;</div>";
//							$x .= "<div class='main_container'>";
//						}
						# 1st Header [Start]
						$x .= "<div class='main_container'>";
						$x .= "<table class='border_table'>";
						$x .= "<tr>";
						$x .= "<td width='12%'>".$Semester[$termID]."<br>".$currentReportColumnName."<br>(".$assessmentCode.")</td><td width='22%'>".$YearClassNameArr[$YearClassID]."</td><td colspan='".(count($currentColumn)+1)."'>任教老師:$currentSubjectTeacher</td>";
						$x .= "</tr>";
						$dataArr = array();
						$dataArr[] = $Semester[$termID].$currentReportColumnName."(".$assessmentCode.")";
						$dataArr[] = $YearClassNameArr[$YearClassID];
						$dataArr[] = "任教老師:".$currentSubjectTeacher;
						if($isFirst){
							$exportColumn = $dataArr;
							$isFirst = false;
						}
						else {
							$exportArr[] = $dataArr;
						}
						# 1st Header [End]
					
						# 2nd Header [Start]
						$x .= "<tr>";
						$x .= "<td>$parentSubjectName</td><td>&nbsp;</td>";
						$dataArr = array();
						$dataArr[] = $parentSubjectName;
						$dataArr[] = "";
						
						$rowWidth = floor(66/(count($currentColumn)+1));
						
						foreach((array)$currentColumn as $columnContent){
							$reportColumnName = $columnContent['ColumnTitle'];	
							$x .= "<td width='$rowWidth%'>$reportColumnName</td>";
							$dataArr[] = $reportColumnName;
						}
						$x .= "<td>".$subjectName."總分</td>";
						$x .= "</tr>";
						$dataArr[] = $subjectName."總分";
						$exportArr[] = $dataArr;
						# 2nd Header [End]
						
						# Ratio Header [Start]
						$x .= "<tr>";
						$x .= "<td>&nbsp;</td><td>比重</td>";
						$dataArr = array();
						$dataArr[] = "";
						$dataArr[] = "比重";
						foreach((array)$currentColumn as $columnContent){
							$reportColumnRatio = $columnContent['Ratio'];	
							$x .= "<td>$reportColumnRatio</td>";
							$dataArr[] = $reportColumnRatio;
						}
						$x .= "<td>&nbsp;</td>";
						$x .= "</tr>";
						$dataArr[] = "";
						$exportArr[] = $dataArr;
						# Ratio Header [End]
						
						# Full Mark Header [Start]
						$x .= "<tr>";
						$x .= "<td>&nbsp;</td><td>滿分</td>";
						$dataArr = array();
						$dataArr[] = "";
						$dataArr[] = "滿分";
						foreach((array)$currentColumn as $columnContent){
							$reportColumnFullMark = $columnContent['FullMark'];	
							$x .= "<td>$reportColumnFullMark</td>";
							$dataArr[] = $reportColumnFullMark;
						}
						$x .= "<td>".$fullMarkArr[$termID][$currentReportColumnID][($subjectComponentID? $subjectComponentID : $parentSubjectID)]['FullMark']."</td>";
						$x .= "</tr>";
						$dataArr[] = $fullMarkArr[$termID][$currentReportColumnID][($subjectComponentID? $subjectComponentID : $parentSubjectID)]['FullMark'];
						$exportArr[] = $dataArr;
						# Full Mark Header [End]
						
						$studentCount = 0;
						foreach((array)$currentClassStudent as $studentID => $classInfo){
							$studentName = $classInfo['StudentNameCh'];
							$studentNum = $classInfo['ClassNumber'];
							
							$x .= "<tr>";
							$x .= "<td>$studentNum</td><td>$studentName</td>";
							$dataArr = array();
							$dataArr[] = $studentNum;
							$dataArr[] = $studentName;
							
							# CA Score
							foreach((array)$currentColumn as $columnContent){
								$columnID = $columnContent['ColumnID'];
								$studentSocre = $SubMSScoreArr[$termID][$currentReportColumnID][$parentSubjectID][$subjectComponentID][$studentID][$columnID];
								if(!isset($studentSocre) || $studentSocre == "-1" || $studentSocre == "-3"){
									$studentSocre = $emptyDisplay;
								}
								$x .= "<td>$studentSocre</td>";
								$dataArr[] = $studentSocre;
							}
							
							# Total Score
							$studentTermInput = $MSScoreArr[$YearClassID][$termID][$studentID][($subjectComponentID? $subjectComponentID : $parentSubjectID)][$currentReportColumnID];
							$studentTotalSocre = $studentTermInput['Mark'];
							$studentTotalGrade = $studentTermInput['Grade'];
							$inputValid = $lreportcard->Check_If_Grade_Is_SpecialCase($studentTotalGrade) || $studentTotalGrade == "N.A." || !isset($studentTotalSocre) || $studentTotalSocre == -1 || $studentTotalSocre == -3;
							if($inputValid){
								$studentTotalSocre = $emptyDisplay;
							}
							$x .= "<td>$studentTotalSocre</td>";
							$x .= "</tr>";
							$dataArr[] = $studentTotalSocre;
							$exportArr[] = $dataArr;
							
							$studentCount++;
							if($studentCount > 31){
								$x .= "</table>";
								$x .= "</div>";
								$x .= "<div class='page_break'>&nbsp;</div>";
								
								$x .= "<div class='main_container'>";
								$x .= "<table class='border_table'>";
								$x .= "<tr>";
								$x .= "<td width='12%'>".$Semester[$termID]."<br>".$currentReportColumnName."<br>(".$assessmentCode.")</td><td width='22%'>".$YearClassNameArr[$YearClassID]."</td><td colspan='".(count($currentColumn)+1)."'>&nbsp;</td>";
								$x .= "</tr>";
								
								$studentCount = 0;
							}
						}
						
						$statContent = $MarkStatistics[$termID][$currentReportColumnID][($subjectComponentID? $subjectComponentID : $parentSubjectID)][$YearClassID];
						
						$statInfo = array();		
						$statInfo[] = $statContent['MaxMark'];
						$statInfo[] = $statContent['MinMark'];
						$statInfo[] = $statContent['Average_Rounded'];
						$statInfo[] = $statContent['Nature']['TotalPassPercentage'];
						
						# Stat Content
						for($i=0; $i<4; $i++){
							$statTitle = $statTitleArr[$i];
							$x .= "<tr>";
							$x .= "<td colspan='2'>$statTitle</td>";
							$dataArr = array();
							$dataArr[] = $statTitle;
							$dataArr[] = "";
							
							foreach((array)$currentColumn as $columnContent){
								$columnID = $columnContent['ColumnID'];
								$statDisplay = $SubMSStatArr[$YearClassID][$termID][$parentSubjectID][$subjectComponentID][$columnID][$i];
								$statDisplay = isset($statDisplay)? $statDisplay : $emptyDisplay;
								$statDisplay .= ($i==3? "%" : "");
								$x .= "<td>$statDisplay</td>";
								$dataArr[] = $statDisplay;
							}
//							$statDisplay = $MSStatArr[$YearClassID][$termID][$parentSubjectID][$subjectComponentID][$currentReportColumnID][$i];
							$statDisplay = $statInfo[$i];
							$statDisplay = isset($statDisplay)? $statDisplay : $emptyDisplay;
							$statDisplay .= ($i==3? "%" : "");
							$x .= "<td>$statDisplay</td>";
							$x .= "</tr>";
							$dataArr[] = $statDisplay;
							$exportArr[] = $dataArr;
						}
						$assessmentSq++;
					
						$x .= "</table>";
						$x .= "</div>";
						$x .= "</div><div class='page_break'>&nbsp;</div>";
						$exportArr[] = array("");
					}
				}
			}
		}
	}
	
	if($exportCSV == "1"){
		$lexport = new libexporttext();
		$export_content = $lexport->GET_EXPORT_TXT($exportArr, $exportColumn,  "", "\r\n", "", 0, "11",1);		
		
		intranet_closedb();
		
		// Output CSV File
		$filename = "Class_Subject_CA_Result_Analysis.csv";
		$filename = str_replace(' ', '_', $filename);
		$lexport->EXPORT_FILE($filename, $export_content);
	
	} else {
		
		$x .= "</div>";
		echo $x;
		
		echo'<style>';
		include_once($PATH_WRT_ROOT."file/reportcard2008/templates/general.css");
		echo '</style>';	
		
		$style_css  = "html, body { margin:0px; padding:0px; }\n";
		$style_css .= "body { font-family:'Times New Roman, Times, serif, 新細明體'; font-size:13px }\n";
//		$style_css .= ".main_container {display:block; width:750px; height:850px; margin:auo; padding:120px 20px; position:relative; padding-bottom:100px; clear:both; border:0px solid green;}\n";
		$style_css .= ".main_container { display:block; width:680px; height:850px; margin:auto; padding:60px 20px; position:relative; padding-bottom:20px; clear:both; border:0px solid green; }\n";
		$style_css .= ".page_break {page-break-after:always; margin:0; padding:0; line-height:0; font-size:0; height:0;}";
		
		$style_css .= ".border_table { width: 100%; margin-bottom:60px; }";
		$style_css .= ".border_table td { padding-left:4px; text-align:center }";
		$style_css .= ".border_table > tbody > tr:first-child td { vertical-align:middle; }";
		
		echo "<style>";
		echo $style_css;
		echo "</style>";		
		
		intranet_closedb();
	}
	
} else {
	
	echo "You have no priviledge to access this page.";
	intranet_closedb();
	die();
	
}

?>