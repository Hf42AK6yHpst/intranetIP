<?
//using: Bill

//@SET_TIME_LIMIT(1000);

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcardcustom();

$x = "";
$emptyDisplay = "---";
$exportArr = array();
$exportColumn = array();
$statTitleArr = array('學生總數', '平均分', '及格率');

$isFirst = true; 
$table_count = 0;

$SubMSStatArr = array();
$MarkSheetScore = array();
$MarkStatistics = array();
$TermReportColumnArr = array();
$subjectTeacherArr = array();
$markPercentageArr = array();

# POST
$YearID = $_POST['YearID'];
$ReportID = $_POST['ReportID'];
$SubjectIDArr = $_POST['SubjectIDArr'];
$dataType = $_POST['data_type'];
$exportCSV = $_POST['submit_type'];

// [2015-1209-1703-15206]
if ($_SESSION["SSV_PRIVILEGE"]["reportcard"]["is_subject_panel"] || $lreportcard->hasAccessRight()) {
     	
    # Report Template Info
    $ReportTemplate = $lreportcard->returnReportTemplateBasicInfo($ReportID);
    $ReportSemester = $ReportTemplate['Semester'];
     	     	
	# Semester
    $Semester = $lreportcard->GET_ALL_SEMESTERS();
    if($ReportSemester != 'F'){
       	$Semester = array();
		$Semester[$ReportSemester] = $lreportcard->GET_ALL_SEMESTERS($ReportSemester);
	}
	
	$currentForm = $lreportcard->returnClassLevel($YearID);
    $Subjects = $lreportcard->returnSubjectwOrder($YearID, 0, '', 'b5', '', 0, $ReportID);
	
	# Year Class
	$YearClassNameArr = $lreportcard->GET_CLASSES_BY_FORM($YearID, "", 1, 1);
	$YearClassIDArr = array_keys($YearClassNameArr); 
	foreach($YearClassIDArr as $YearClassID){
		$classStudents = $lreportcard->GET_STUDENT_BY_CLASS($YearClassID, "", 0, 0, 0, 1);
		$classStudentArr[$YearClassID] = $classStudents;
	}
	
	# Related Reports
	$relatedReports = $lreportcard->Get_Related_TermReport_Of_Consolidated_Report($ReportID, $MainReportOnly=1, $ReportColumnID="ALL");
	// For term report, only process 1 report 
	if($ReportTemplate['Semester'] != 'F'){
		$relatedReports = array();
		$relatedReports[0] = $ReportTemplate;
	} else {
		$relatedReports[] = $ReportTemplate;
	}

	# Loop Reports
	// Build settings array
	for($i=0; $i<count($relatedReports); $i++)
	{
		# Related Report Info
		$relatedReportID = $relatedReports[$i]['ReportID'];
		$TermID = $relatedReports[$i]['Semester'];
		$grading_scheme = $lreportcard->GET_FROM_SUBJECT_GRADING_SCHEME($YearID, 0, $relatedReportID);
		
		# Report Column
		$ReportColumn = $lreportcard->returnReportTemplateColumnData($relatedReportID);
		$TermReportColumnArr[$TermID] = BuildMultiKeyAssoc($ReportColumn, array('ReportColumnID'));
		
		# MarkSheet Score - Related Report
		foreach((array)$YearClassIDArr as $YearClassID){
			$studentList = array_keys((array)$classStudentArr[$YearClassID]);				
			$MarkSheetScore[$YearClassID] = $lreportcard->getMarks($relatedReportID, '', " AND a.StudentID IN ('".implode("','", $studentList)."')");
		}
		
		foreach ((array)$ReportColumn as $ReportColumnObj)
		{
			# Report Column Info
			$ReportColumnID = $ReportColumnObj['ReportColumnID'];
			$ReportColumnInfo = $lreportcard->GET_SUB_MARKSHEET_COLUMN_INFO('', $ReportColumnID);
			
			# Loop Submarksheet Column
			for($j=0; $j<count($ReportColumnInfo); $j++)
			{
				# Submarksheet Column Info
				$columnSettings = $ReportColumnInfo[$j];
				$columnID = $columnSettings['ColumnID'];
				$SubjectID = $columnSettings['SubjectID'];
				$SubMS_grading_FullMark = $columnSettings['FullMark'];
				$SubMS_grading_PassingMark = $columnSettings['PassMark'];
				
				# Grading Info
				$subjectGrading = $lreportcard->GET_GRADING_SCHEME_MAIN_INFO($grading_scheme[$SubjectID]['schemeID']);
				$grading_FullMark = $subjectGrading['FullMark'];
				$grading_PassMark = $subjectGrading['PassMark'];
				
				# Set Parent and Component Subject ID
				$componentSubjectID = 0;
				$parentSubjectID = $lreportcard->GET_PARENT_SUBJECT_ID($SubjectID);
				// for component subject
				if($parentSubjectID){
					$componentSubjectID = $SubjectID;
					$SubjectID = $parentSubjectID;
				}
				
				// skip if both parent and component subject are not selected 
				if(!in_array($SubjectID, (array)$SubjectIDArr) && !in_array($componentSubjectID, (array)$SubjectIDArr))
					continue;
				
				$allColumns[$TermID][$ReportColumnID][$SubjectID][$componentSubjectID][] = $columnSettings;
				
				# Type of Analysis - Continuous Assessment
				if($dataType == 3){
					
					# Submarksheet Score
					$SubMSScore = $lreportcard->GET_SUB_MARKSHEET_SCORE($ReportColumnID, ($componentSubjectID? $componentSubjectID : $SubjectID));
					$SubMSScoreArr[$TermID][$ReportColumnID][$SubjectID][$componentSubjectID] = $SubMSScore;
					
					# Percentage Calculation
					$formMarks = array();
					$formPassStudent = 0;
					$formTotalStudent = 0;
					$formMarkPercentage = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
					foreach((array)$YearClassIDArr as $YearClassID){
						$studentList = array_keys((array)$classStudentArr[$YearClassID]);
						
						# Teacher Name
						if(!isset($subjectTeacherArr[$YearClassID][$ReportColumnID][$SubjectID])){
							$subjectTeacher = $lreportcard->returnSubjectTeacher($YearClassID, $SubjectID, $relatedReportID, $studentList[0], 1);
							$subjectTeacherArr[$YearClassID][$ReportColumnID][$SubjectID] = $subjectTeacher[0]['ChineseName'];
						}
						
						$Marks = array();
						$passStudent = 0;
						$totalStudent = 0;
						$passRatio = 0.00;
						$averageMark = 0.00;
						$MarkPercentage = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
						foreach((array)$studentList as $currentStudentID){
							$score = $SubMSScore[$currentStudentID][$columnID];
							
							# Skip if invalid score
							if($score == "" || $score == '-3'){
								continue;
							}
							
							$Marks[] = $score;
							$formMarks[] = $score;
							$totalStudent++;
							$formTotalStudent++;
							
							if($score >= $SubMS_grading_PassingMark){
								$passStudent++;
								$formPassStudent++;
							}
						
							$markRange = $SubMS_grading_FullMark > 0? $score / $SubMS_grading_FullMark * 100 : 0;
							if($markRange <= 10){
								$MarkPercentage[0]++;
								$formMarkPercentage[0]++;
							}
							else if($markRange <= 20){
								$MarkPercentage[1]++;
								$formMarkPercentage[1]++;
							}
							else if($markRange <= 30){
								$MarkPercentage[2]++;
								$formMarkPercentage[2]++;
							}
							else if($markRange <= 40){
								$MarkPercentage[3]++;
								$formMarkPercentage[3]++;
							}
							else if($markRange <= 50){
								$MarkPercentage[4]++;
								$formMarkPercentage[4]++;
							}
							else if($markRange <= 60){
								$MarkPercentage[5]++;
								$formMarkPercentage[5]++;
							}
							else if($markRange <= 70){
								$MarkPercentage[6]++;
								$formMarkPercentage[6]++;
							}
							else if($markRange <= 80){
								$MarkPercentage[7]++;
								$formMarkPercentage[7]++;
							}
							else if($markRange <= 90){
								$MarkPercentage[8]++;
								$formMarkPercentage[8]++;
							}
							else if($markRange > 90){
								$MarkPercentage[9]++;
								$formMarkPercentage[9]++;
							}
						}
						
						if(count($Marks) > 0){
							$averageMark = $lreportcard->getAverage($Marks);
							$averageMark = my_round($averageMark, 2);
							$passRatio = $passStudent / $totalStudent * 100;
							$passRatio = my_round($passRatio, 2);
						}
						
						$SubMSStatArr[$YearClassID][$TermID][$SubjectID][$componentSubjectID][$columnID] = array($totalStudent, $averageMark, $passRatio);
						$markPercentageArr[$YearClassID][$TermID][$SubjectID][$componentSubjectID][$columnID] = $MarkPercentage;
					}
				}
				
				# Type of Analysis - Term / Assessment
				else {
					
					# Marksheet Score
					$formMarkPercentage = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
					foreach((array)$YearClassIDArr as $YearClassID){
						$studentList = array_keys((array)$classStudentArr[$YearClassID]);
						
//						$MarkSheetScore = $lreportcard->getMarks($relatedReportID, '', " AND a.StudentID IN ('".implode("','", $studentList)."')");
						$MSScore = $MarkSheetScore[$YearClassID];
						
						if(!isset($subjectTeacherArr[$YearClassID][$ReportColumnID][$SubjectID])){
							$subjectTeacher = $lreportcard->returnSubjectTeacher($YearClassID, $SubjectID, $relatedReportID, $studentList[0], 1);
							$subjectTeacherArr[$YearClassID][$ReportColumnID][$SubjectID] = $subjectTeacher[0]['ChineseName'];
						}
								
						# Percentage Calculation
						$MarkPercentage = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
						foreach((array)$studentList as $currentStudentID){
							
							# Type of Analysis - Assessment
							if($dataType == 2){
								$studentInput = $MSScore[$currentStudentID][($componentSubjectID? $componentSubjectID : $SubjectID)][$ReportColumnID];
							} 
							# Type of Analysis - Term
							else {
								$studentInput = $MSScore[$currentStudentID][($componentSubjectID? $componentSubjectID : $SubjectID)][0];
							}
							
							// skip if invalid mark input
//							if($studentInput['Grade'] == 'N.A.')
							if($lreportcard->Check_If_Grade_Is_SpecialCase($studentInput['Grade']))
								continue;
							
							$score = $studentInput['Mark'];
							$markRange = $grading_FullMark > 0? $score / $grading_FullMark * 100 : 0;
							if($markRange <= 10){
								$MarkPercentage[0]++;
								$formMarkPercentage[0]++;
							}
							else if($markRange <= 20){
								$MarkPercentage[1]++;
								$formMarkPercentage[1]++;
							}
							else if($markRange <= 30){
								$MarkPercentage[2]++;
								$formMarkPercentage[2]++;
							}
							else if($markRange <= 40){
								$MarkPercentage[3]++;
								$formMarkPercentage[3]++;
							}
							else if($markRange <= 50){
								$MarkPercentage[4]++;
								$formMarkPercentage[4]++;
							}
							else if($markRange <= 60){
								$MarkPercentage[5]++;
								$formMarkPercentage[5]++;
							}
							else if($markRange <= 70){
								$MarkPercentage[6]++;
								$formMarkPercentage[6]++;
							}
							else if($markRange <= 80){
								$MarkPercentage[7]++;
								$formMarkPercentage[7]++;
							}
							else if($markRange <= 90){
								$MarkPercentage[8]++;
								$formMarkPercentage[8]++;
							}
							else if($markRange > 90){
								$MarkPercentage[9]++;
								$formMarkPercentage[9]++;
							}
						}
						
						# Type of Analysis - Assessment
						if($dataType == 2){
							$markPercentageArr[$YearClassID][$TermID][$SubjectID][$componentSubjectID][$ReportColumnID] = $MarkPercentage;
						} 
						# Type of Analysis - Term
						else {
							$markPercentageArr[$YearClassID][$TermID][$SubjectID][$componentSubjectID][0] = $MarkPercentage;
						}
					}
				}
				
				# Type of Analysis - Term
				if($dataType == 3){
					$passRatio = $emptyDisplay;
					$averageMark = $emptyDisplay;
					
					if(count($formMarks) > 0){
						$averageMark = $lreportcard->getAverage($formMarks);
						$averageMark = my_round($averageMark, 2);
						$averageMark = $averageMark >= 0? $averageMark : $emptyDisplay;
						$passRatio = $formPassStudent / $formTotalStudent * 100;
						$passRatio = my_round($passRatio, 2);
						$passRatio = $passRatio >= 0? $passRatio : $emptyDisplay;
					}
					
					$SubMSStatArr[0][$TermID][$SubjectID][$componentSubjectID][$columnID] = array($formTotalStudent, $averageMark, $passRatio);
					$markPercentageArr[0][$TermID][$SubjectID][$componentSubjectID][$columnID] = $formMarkPercentage;
					
				}
				
				# Type of Analysis - Term / Assessment
				else {
					$isParentCalculated = false;
					
					# Type of Analysis - Assessment
					if($dataType == 2){
						$markPercentageArr[0][$TermID][$SubjectID][$componentSubjectID][$ReportColumnID] = $formMarkPercentage;
						$isParentCalculated = !isset($markPercentageArr[0][$TermID][$SubjectID][0][$ReportColumnID]);
					}
					# Type of Analysis - Term
					else {
						$markPercentageArr[0][$TermID][$SubjectID][$componentSubjectID][0] = $formMarkPercentage;
						$isParentCalculated = !isset($markPercentageArr[0][$TermID][$SubjectID][0][0]);
					}
					
					# Parent Subject - Marksheet Score
//					if($dataType != 3 && !isset($markPercentageArr[0][$TermID][$SubjectID][0][0])){
					if($isParentCalculated){
										
						$formMarkPercentage = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
						foreach((array)$YearClassIDArr as $YearClassID){
							$studentList = array_keys((array)$classStudentArr[$YearClassID]);
							
//							$MSScoreArr[$YearClassID][$TermID] = $MarkSheetScore;
							$MSScore = $MarkSheetScore[$YearClassID];
									
							# Percentage Calculation
							$MarkPercentage = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
							foreach((array)$studentList as $currentStudentID){
								
								# Type of Analysis - Assessment
								if($dataType == 2){
									$studentInput = $MSScore[$currentStudentID][$SubjectID][$ReportColumnID];
								} 
								# Type of Analysis - Term
								else {
									$studentInput = $MSScore[$currentStudentID][$SubjectID][0];
								}
								
//								if($studentInput['Grade'] == 'N.A.')
								if($lreportcard->Check_If_Grade_Is_SpecialCase($studentInput['Grade']))
									continue;
								
								$score = $studentInput['Mark'];
								$markRange = $grading_FullMark > 0? $score / $grading_FullMark * 100 : 0;
								if($markRange <= 10){
									$MarkPercentage[0]++;
									$formMarkPercentage[0]++;
								}
								else if($markRange <= 20){
									$MarkPercentage[1]++;
									$formMarkPercentage[1]++;
								}
								else if($markRange <= 30){
									$MarkPercentage[2]++;
									$formMarkPercentage[2]++;
								}
								else if($markRange <= 40){
									$MarkPercentage[3]++;
									$formMarkPercentage[3]++;
								}
								else if($markRange <= 50){
									$MarkPercentage[4]++;
									$formMarkPercentage[4]++;
								}
								else if($markRange <= 60){
									$MarkPercentage[5]++;
									$formMarkPercentage[5]++;
								}
								else if($markRange <= 70){
									$MarkPercentage[6]++;
									$formMarkPercentage[6]++;
								}
								else if($markRange <= 80){
									$MarkPercentage[7]++;
									$formMarkPercentage[7]++;
								}
								else if($markRange <= 90){
									$MarkPercentage[8]++;
									$formMarkPercentage[8]++;
								}
								else if($markRange > 90){
									$MarkPercentage[9]++;
									$formMarkPercentage[9]++;
								}
							}
							
							# Type of Analysis - Assessment
							if($dataType == 2){
								$markPercentageArr[$YearClassID][$TermID][$SubjectID][0][$ReportColumnID] = $MarkPercentage;
							} else {
								$markPercentageArr[$YearClassID][$TermID][$SubjectID][0][0] = $MarkPercentage;
							}
						}
					
//						# Type of Analysis - Assessment
						if($dataType == 2){
							$markPercentageArr[0][$TermID][$SubjectID][0][$ReportColumnID] = $formMarkPercentage;
						} else {
							$markPercentageArr[0][$TermID][$SubjectID][0][0] = $formMarkPercentage;
						}
					}
				}
				
//				if($dataType == 1){
//					break;		
//				}
			}
			// prevent 
			if($dataType == 1){
				break;		
			}
		}
		$MarkStatistics[$TermID] = $lreportcard->Get_Mark_Statistics_Info($relatedReportID, (array)$SubjectIDArr, 2, 2, 2, 0, 1, "", 0);
	}
	
	$column_width = floor(80 / (count($YearClassIDArr) + 1));
	$x .= "<div class='main_container'>";
	
	$displayArr = array();
	foreach((array)$SubjectIDArr as $parentSubjectID){
		$withParentSubjectID = $lreportcard->GET_PARENT_SUBJECT_ID($parentSubjectID);
		
		$currentComponent = 0;
		// for component subject
		if($withParentSubjectID){
			$currentComponent = $parentSubjectID;
			$parentSubjectID = $withParentSubjectID;
		}
		
		$componentSubject = $Subjects[$parentSubjectID];
		foreach((array)$componentSubject as $subjectComponentID => $subjectName){
			$displaySubjectName = $subjectComponentID? $componentSubject[0]."_".$subjectName : $subjectName;
			$checkingSubjectID = $subjectComponentID? $subjectComponentID : $parentSubjectID;
			
			// skip to add table content if
			// 1. check if subject displayed or not
			// 2. for component subject in $SubjectIDArr - allow selected component subject in index.php only
			// 3. for major subject in $SubjectIDArr - allow selected major subject in index.php only
			if(in_array($checkingSubjectID, (array)$displayArr) || ($withParentSubjectID && $currentComponent != $subjectComponentID)
						|| (!$withParentSubjectID && !in_array($checkingSubjectID, (array)$SubjectIDArr)))
				continue;
				
//			if($dataType == 3 && $subjectComponentID == 0 && count($componentSubject) > 1)
//				continue;			
			
			$displayArr[] = $subjectComponentID? $subjectComponentID : $parentSubjectID;
			foreach((array)$allColumns as $termID => $columnInfo){
				foreach((array)$columnInfo as $currentReportColumnID => $currentReportColumn){
					$currentReportColumnName = $TermReportColumnArr[$termID][$currentReportColumnID]['ColumnTitle'];
					$currentSubjectTeacher = $subjectTeacherArr[$YearClassID][$currentReportColumnID][$parentSubjectID];
						
					$currentColumn = $currentReportColumn[$parentSubjectID][$subjectComponentID];
					
					# Term
					if($dataType == 3){
						foreach((array)$currentColumn as $columnContent){
							$reportColumnName = $columnContent['ColumnTitle'];
							
							if($table_count != 0 && !($table_count % 2)){
								$x .= "</div>";
								$x .= "<div class='page_break'>&nbsp;</div>";
								$x .= "<div class='main_container'>";
							}
							$table_count++;
							
							$x .= "<table class='border_table'><tr><td width='20%'>".$displaySubjectName."<br>".$reportColumnName."</td><td width='$column_width%'>全級</td>";
							$dataArr = array();
							$dataArr[] = $displaySubjectName.$reportColumnName;
							$dataArr[] = "全級";
							
							foreach((array)$YearClassIDArr as $YearClassID){
								$currentClassName = $YearClassNameArr[$YearClassID];
								$x .= "<td width='$column_width%'>$currentClassName</td>";
								$dataArr[] = $currentClassName;
							}
							$x .= "</tr>";
							
							if($isFirst){
								$exportColumn = $dataArr;
								$isFirst = false;
							}
							else 
								$exportArr[] = $dataArr;
							
							$x .= "<tr><td>任教老師</td><td>&nbsp;</td>";
							$dataArr = array();
							$dataArr[] = "任教老師";
							$dataArr[] = "";
							foreach((array)$YearClassIDArr as $YearClassID){
								$x .= "<td>".$subjectTeacherArr[$YearClassID][$currentReportColumnID][$parentSubjectID]."</td>";
								$dataArr[] = $subjectTeacherArr[$YearClassID][$currentReportColumnID][$parentSubjectID];
							}
							$x .= "</tr>";
							$exportArr[] = $dataArr;
							
							$x .= "<tr><td>分數(%)</td><td colspan='".(count($YearClassIDArr)+1)."'>學生數目</td></tr>";
							$dataArr = array();
							$dataArr[] = "分數(%)";
							$dataArr[] = "學生數目";
							$exportArr[] = $dataArr;
							
							for($i=0; $i<10; $i++){
								$currentPencentage = ($i*10 + ($i?1:0))." - ".(($i+1)*10);
								$currentPencentageContnet = $markPercentageArr[0][$termID][$parentSubjectID][$subjectComponentID][$columnContent['ColumnID']][$i];
								$currentPencentageContnet = isset($currentPencentageContnet)? $currentPencentageContnet : $emptyDisplay;
								
								$x .= "<tr><td>$currentPencentage</td>";
								$x .= "<td>$currentPencentageContnet</td>";
								$dataArr = array();
								$dataArr[] = " ".$currentPencentage;
								$dataArr[] = $currentPencentageContnet;
								
								foreach((array)$YearClassIDArr as $YearClassID){
									$currentPencentageContnet = $markPercentageArr[$YearClassID][$termID][$parentSubjectID][$subjectComponentID][$columnContent['ColumnID']][$i];
									$currentPencentageContnet = isset($currentPencentageContnet)? $currentPencentageContnet : $emptyDisplay;
									
									$x .= "<td>$currentPencentageContnet</td>";
									$dataArr[] = $currentPencentageContnet;
								}
								$x .= "</tr>";
								$exportArr[] = $dataArr;
							}
									
							for($i=0; $i<3; $i++){
								$currentStatContent = $SubMSStatArr[0][$termID][$parentSubjectID][$subjectComponentID][$columnContent['ColumnID']][$i];
								$currentStatContent = isset($currentStatContent)? $currentStatContent : $emptyDisplay;
								
								$x .= "<tr><td>".$statTitleArr[$i]."</td>";
								$x .= "<td>".$currentStatContent;
								$x .= $i==2? "%" : "";
								$x .= "</td>";
								
								$dataArr = array();
								$dataArr[] = $statTitleArr[$i];
								$dataArr[] = $currentStatContent.($i==2? "%" : "");
								
								foreach((array)$YearClassIDArr as $YearClassID){
									$currentClassName = $YearClassID;
									$currentClassStudent = $classStudentArr[$YearClassID];
									$currentStatContent = $SubMSStatArr[$YearClassID][$termID][$parentSubjectID][$subjectComponentID][$columnContent['ColumnID']][$i];
									$currentStatContent = isset($currentStatContent)? $currentStatContent : $emptyDisplay;
								
									$x .= "<td>".$currentStatContent;
									$x .= $i==2? "%" : "";
									$x .= "</td>";
									$dataArr[] = $currentStatContent.($i==2? "%" : "");
								}
							$x .= "</tr>";
							$exportArr[] = $dataArr;
							}
						$x .= "</table>";
						$exportArr[] = array("");
						}
					} 
					# Assessment
					else if($dataType == 2){
						
						if($table_count != 0 && !($table_count % 2)){
							$x .= "</div>";
							$x .= "<div class='page_break'>&nbsp;</div>";
							$x .= "<div class='main_container'>";
						}
						$table_count++;
						
						$x .= "<table class='border_table'><tr><td width='20%'>".$displaySubjectName."<br>".$currentReportColumnName."</td><td width='$column_width%'>全級</td>";
						$dataArr = array();
						$dataArr[] = $displaySubjectName.$currentReportColumnName;
						$dataArr[] = "全級";
							
						foreach((array)$YearClassIDArr as $YearClassID){
							$currentClassName = $YearClassNameArr[$YearClassID];
							$x .= "<td width='$column_width%'>$currentClassName</td>";
							$dataArr[] = $currentClassName;
						}
						$x .= "</tr>";
							
						if($isFirst){
							$exportColumn = $dataArr;
							$isFirst = false;
						}
						else 
							$exportArr[] = $dataArr;
							
						$x .= "<tr><td>任教老師</td><td>&nbsp;</td>";
						$dataArr = array();
						$dataArr[] = "任教老師";
						$dataArr[] = "";
						foreach((array)$YearClassIDArr as $YearClassID){
							$x .= "<td>".$subjectTeacherArr[$YearClassID][$currentReportColumnID][$parentSubjectID]."</td>";
							$dataArr[] = $subjectTeacherArr[$YearClassID][$currentReportColumnID][$parentSubjectID];
						}
						$x .= "</tr>";
						$exportArr[] = $dataArr;
						
						$x .= "<tr><td>分數(%)</td><td colspan='".(count($YearClassIDArr)+1)."'>學生數目</td></tr>";
						$dataArr = array();
						$dataArr[] = "分數(%)";
						$dataArr[] = "學生數目";
						$exportArr[] = $dataArr;

						for($i=0; $i<10; $i++){
							$currentPencentage = ($i*10 + ($i?1:0))." - ".(($i+1)*10);
							$currentPencentageContnet = $markPercentageArr[0][$termID][$parentSubjectID][$subjectComponentID][$currentReportColumnID][$i];
							$currentPencentageContnet = isset($currentPencentageContnet)? $currentPencentageContnet : $emptyDisplay;
							
							$x .= "<tr><td>$currentPencentage</td>";
							$x .= "<td>$currentPencentageContnet</td>";
							$dataArr = array();
							$dataArr[] = " ".$currentPencentage;
							$dataArr[] = $currentPencentageContnet;
								
							foreach((array)$YearClassIDArr as $YearClassID){
								$currentPencentageContnet = $markPercentageArr[$YearClassID][$termID][$parentSubjectID][$subjectComponentID][$currentReportColumnID][$i];
								$currentPencentageContnet = isset($currentPencentageContnet)? $currentPencentageContnet : $emptyDisplay;
								
								$x .= "<td>$currentPencentageContnet</td>";
								$dataArr[] = $currentPencentageContnet;
							}
							$x .= "</tr>";
							$exportArr[] = $dataArr;
						}
						
						$statInfo = array();
						foreach((array)$YearClassIDArr as $YearClassID){
							$statContent = $MarkStatistics[$termID][$currentReportColumnID][($subjectComponentID? $subjectComponentID : $parentSubjectID)][$YearClassID];
							
							$classStatContent = array();
							$classStatContent[] = $statContent['NumOfStudent'];
							$classStatContent[] = $statContent['Average_Rounded'];
							$classStatContent[] = $statContent['Nature']['TotalPassPercentage'];
							
							$statInfo[$YearClassID] = $classStatContent;
						}
						$statContent = $MarkStatistics[$termID][$currentReportColumnID][($subjectComponentID? $subjectComponentID : $parentSubjectID)][0];
						
						$classStatContent = array();
						$classStatContent[] = $statContent['NumOfStudent'];
						$classStatContent[] = $statContent['Average_Rounded'];
						$classStatContent[] = $statContent['Nature']['TotalPassPercentage'];
							
						$statInfo[0] = $classStatContent;
						
						for($i=0; $i<3; $i++){
//							$currentStatContent = $MSStatArr[0][$termID][$parentSubjectID][$subjectComponentID][$currentReportColumnID][$i];
							$currentStatContent = $statInfo[0][$i];
							
							$currentStatContent = isset($currentStatContent)? $currentStatContent : $emptyDisplay;
							$x .= "<tr><td>".$statTitleArr[$i]."</td>";
							$x .= "<td>".$currentStatContent;
							$x .= $i==2? "%" : "";
							$x .= "</td>";
							
							$dataArr = array();
							$dataArr[] = $statTitleArr[$i];
							$dataArr[] = $currentStatContent.($i==2? "%" : "");
							
							foreach((array)$YearClassIDArr as $YearClassID){
								$currentClassName = $YearClassID;
								$currentClassStudent = $classStudentArr[$YearClassID];
//								$currentStatContent = $MSStatArr[$YearClassID][$termID][$parentSubjectID][$subjectComponentID][$currentReportColumnID][$i];
								$currentStatContent = $statInfo[$YearClassID][$i];
								$currentStatContent = isset($currentStatContent)? $currentStatContent : $emptyDisplay;
							
								$x .= "<td>".$currentStatContent;
								$x .= $i==2? "%" : "";
								$x .= "</td>";
								$dataArr[] = $currentStatContent.($i==2? "%" : "");
							}
							$x .= "</tr>";
							$exportArr[] = $dataArr;
						}
						$x .= "</table>";
						$exportArr[] = array("");$x .= "</table>";
					
					} else {
						
						if($table_count != 0 && !($table_count % 2)){
							$x .= "</div>";
							$x .= "<div class='page_break'>&nbsp;</div>";
							$x .= "<div class='main_container'>";
						}
						$table_count++;
						
						$x .= "<table class='border_table'><tr><td width='20%'>".$displaySubjectName."<br>".$Semester[$termID]."</td><td width='$column_width%'>全級</td>";
						$dataArr = array();
						$dataArr[] = $displaySubjectName.$Semester[$termID];
						$dataArr[] = "全級";
							
						foreach((array)$YearClassIDArr as $YearClassID){
							$currentClassName = $YearClassNameArr[$YearClassID];
							$x .= "<td width='$column_width%'>$currentClassName</td>";
							$dataArr[] = $currentClassName;
						}
						$x .= "</tr>";
							
						if($isFirst){
							$exportColumn = $dataArr;
							$isFirst = false;
						}
						else 
							$exportArr[] = $dataArr;
							
						$x .= "<tr><td>任教老師</td><td>&nbsp;</td>";
						$dataArr = array();
						$dataArr[] = "任教老師";
						$dataArr[] = "";
						foreach((array)$YearClassIDArr as $YearClassID){
							$x .= "<td>".$subjectTeacherArr[$YearClassID][$currentReportColumnID][$parentSubjectID]."</td>";
							$dataArr[] = $subjectTeacherArr[$YearClassID][$currentReportColumnID][$parentSubjectID];
						}
						$x .= "</tr>";
						$exportArr[] = $dataArr;
						
						$x .= "<tr><td>分數(%)</td><td colspan='".(count($YearClassIDArr)+1)."'>學生數目</td></tr>";
						$dataArr = array();
						$dataArr[] = "分數(%)";
						$dataArr[] = "學生數目";
						$exportArr[] = $dataArr;

						for($i=0; $i<10; $i++){
							$currentPencentage = ($i*10 + ($i?1:0))." - ".(($i+1)*10);
							$currentPencentageContnet = $markPercentageArr[0][$termID][$parentSubjectID][$subjectComponentID][0][$i];
							$currentPencentageContnet = isset($currentPencentageContnet)? $currentPencentageContnet : $emptyDisplay;
							
							$x .= "<tr><td>$currentPencentage</td>";
							$x .= "<td>$currentPencentageContnet</td>";
							$dataArr = array();
							$dataArr[] = " ".$currentPencentage;
							$dataArr[] = $currentPencentageContnet;
								
							foreach((array)$YearClassIDArr as $YearClassID){
								$currentPencentageContnet = $markPercentageArr[$YearClassID][$termID][$parentSubjectID][$subjectComponentID][0][$i];
								$currentPencentageContnet = isset($currentPencentageContnet)? $currentPencentageContnet : $emptyDisplay;
								
								$x .= "<td>$currentPencentageContnet</td>";
								$dataArr[] = $currentPencentageContnet;
							}
							$x .= "</tr>";
							$exportArr[] = $dataArr;
						}
						
						$statInfo = array();
						foreach((array)$YearClassIDArr as $YearClassID){
							$statContent = $MarkStatistics[$termID][0][($subjectComponentID? $subjectComponentID : $parentSubjectID)][$YearClassID];
							
							$classStatContent = array();
							$classStatContent[] = $statContent['NumOfStudent'];
							$classStatContent[] = $statContent['Average_Rounded'];
							$classStatContent[] = $statContent['Nature']['TotalPassPercentage'];
							
							$statInfo[$YearClassID] = $classStatContent;
						}
						$statContent = $MarkStatistics[$termID][0][($subjectComponentID? $subjectComponentID : $parentSubjectID)][0];
						
						$classStatContent = array();
						$classStatContent[] = $statContent['NumOfStudent'];
						$classStatContent[] = $statContent['Average_Rounded'];
						$classStatContent[] = $statContent['Nature']['TotalPassPercentage'];
							
						$statInfo[0] = $classStatContent;
						
						for($i=0; $i<3; $i++){
//							$currentStatContent = $MSStatArr[0][$termID][$parentSubjectID][$subjectComponentID][0][$i];
							$currentStatContent = $statInfo[0][$i];
							
							$currentStatContent = isset($currentStatContent)? $currentStatContent : $emptyDisplay;
							$x .= "<tr><td>".$statTitleArr[$i]."</td>";
							$x .= "<td>".$currentStatContent;
							$x .= $i==2? "%" : "";
							$x .= "</td>";
							
							$dataArr = array();
							$dataArr[] = $statTitleArr[$i];
							$dataArr[] = $currentStatContent.($i==2? "%" : "");
							
							foreach((array)$YearClassIDArr as $YearClassID){
								$currentClassName = $YearClassID;
								$currentClassStudent = $classStudentArr[$YearClassID];
//								$currentStatContent = $MSStatArr[$YearClassID][$termID][$parentSubjectID][$subjectComponentID][0][$i];
								$currentStatContent = $statInfo[$YearClassID][$i];
								$currentStatContent = isset($currentStatContent)? $currentStatContent : $emptyDisplay;
							
								$x .= "<td>".$currentStatContent;
								$x .= $i==2? "%" : "";
								$x .= "</td>";
								$dataArr[] = $currentStatContent.($i==2? "%" : "");
							}
							$x .= "</tr>";
							$exportArr[] = $dataArr;
						}
					$x .= "</table>";
					$exportArr[] = array("");
					}
				}
			}
		}
	}
	
	if($exportCSV == "1"){
		$lexport = new libexporttext();
		$export_content = $lexport->GET_EXPORT_TXT($exportArr, $exportColumn,  "", "\r\n", "", 0, "11",1);		
		
		intranet_closedb();
		
		// Output CSV File
		$filename = "Form_Subject_Result_Analysis.csv";
		$filename = str_replace(' ', '_', $filename);
		$lexport->EXPORT_FILE($filename, $export_content);
	
	} else {
		
		if(!($table_count % 2)){
			$x .= "</div>";
		}
		echo $x;
		
		echo "<style>";
		include_once($PATH_WRT_ROOT."file/reportcard2008/templates/general.css");
		echo "</style>";	
		
		$style_css  = "html, body { margin:0px; padding:0px; }\n";
		$style_css .= "body { font-family:'Times New Roman, Times, serif, 新細明體'; font-size:13px }\n";
//		$style_css .= ".main_container {display:block; width:750px; height:850px; margin:auo; padding:120px 20px; position:relative; padding-bottom:100px; clear:both; border:0px solid green;}\n";
		$style_css .= ".main_container { display:block; width:680px; height:850px; margin:auto; padding:60px 20px; position:relative; padding-bottom:20px; clear:both; border:0px solid green; }\n";
		$style_css .= ".page_break { page-break-after:always; margin:0; padding:0; line-height:0; font-size:0; height:0; }";
		
		$style_css .= ".border_table { width: 100%; margin-bottom:60px; }";
		$style_css .= ".border_table td { text-align: center }";
		$style_css .= ".border_table > tbody > tr:first-child td { height:45px; vertical-align:middle; }";
		
		echo "<style>";
		echo $style_css;
		echo "</style>";	
		
		intranet_closedb();
	}
	
} else {
	
	echo "You have no priviledge to access this page.";
	intranet_closedb();
	die();
	
}

?>