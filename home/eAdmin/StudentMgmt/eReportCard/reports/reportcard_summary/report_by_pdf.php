<?php
# Editing by

@SET_TIME_LIMIT(21600);
@ini_set(memory_limit, -1);

// ini_set('display_errors',1);
// error_reporting(E_ALL ^ E_NOTICE);

/**************************************************
 * 	Modification log
 * 	20180723 Bill:  [2017-0901-1525-31265]
 * 		Allow View Group to access
 *  20180226 Bill	[2017-0901-1525-31265]
 * 		- Create file
 * ***********************************************/

$PageRight = array("ADMIN", "VIEW_GROUP");

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

// Access right
if (!$plugin['ReportCard2008'])
{
    header ("Location: /");
    intranet_closedb();
    exit();
}

function returnReportSummaryCSS()
{
    $css = '';
    $css = '<head>
			<style type="text/css">
                table {
                    line-height: 1.40;
                }
                
                table td.report-title {
                    font-family: "balthazar";
                    font-size: 16pt;
                    text-align: center;
                    vertical-align: middle;
                    padding: 15px;
                }
                
                table.table-stud {
                    padding-left: 18px;
                    padding-bottom: 14px;
                }
                table.table-stud td.ch_font {
	                font-family: "msjh";
                }
                table.table-stud td {
                    font-size: 13px;
	                font-family: "Times New Roman";
                    vertical-align: middle;
                }
                
                table.table-Junior, table.table-Junior tr, table.table-Junior th, table.table-Junior td {
                    font-size: 12px;
                    font-weight: bold;
	                font-family: "Times New Roman";
                    text-align: center;
                    vertical-align: middle;
                    border-collapse: collapse;
                    padding-top: 4px;
                    padding-bottom: 2px;
                }
            </style>';
    
    return $css;
}

function returnReportSummaryStudentInfoTable($tableKeyAry, $dataAry)
{
    global $Lang;
    
    $table = '';
    $table .= '<tr><td class="report-title" colspan="2">';
        $table .= '<span style="font-size:18pt;">R</span>'.$Lang['eReportCard']['ReportArr']['ReportSummaryArr']['ReportTitle1En'];
        $table .= '<span style="font-size:18pt;">S</span>'.$Lang['eReportCard']['ReportArr']['ReportSummaryArr']['ReportTitle2En'];
    $table .= '</td></tr>' . "\r\n";
    $table .= '<tr>' . "\r\n";
    
    foreach ($tableKeyAry as $thisTableKeyAry)
    {
        $table .= ' <td width="50%">
                        <table class="table-stud" width="100%">
                            <tr>
                                <td width="36%"></td>
                                <td width="4%"></td>
                                <td width="60%"></td>
                            </tr>';
        
        foreach ($thisTableKeyAry as $thisKey)
        {
            $table .= returnReportSummaryStudentInfo($thisKey, $dataAry[$thisKey]);
        }
        
        $table .= '      </table>
                    </td>';
    }
    
    $table .= '</tr>' . "\r\n";
    
    return $table;
}

function returnReportSummaryStudentInfo($key, $val)
{
    global $Lang;
    
    $font_class = $key == "ChineseName" ? "ch_font" : "";
    
    $row = '';
    $row .= '<tr>';
        $row .= '<td>' . $Lang['eReportCard']['ReportArr']['ReportSummaryArr'][$key . 'En'] . '</td>';
        $row .= '<td style="text-align: left">:</td>';
        $row .= '<td class="'.$font_class.'">' . $val . '</td>';
    $row .= '</tr>';
    
    return $row;
}

function returnReportSummaryReportHeader($yearAry, $levelAry)
{
    global $Lang;
    
    $formCount = count((array) $yearAry);
    $table_width = $formCount == 1 ? 32 : 20;
    
    $header .= '<tr>';
        $header .= '<td rowspan="3" width="'.$table_width.'%">';
            $header .= $Lang['eReportCard']['ReportArr']['ReportSummaryArr']['SubjectsEn'] . '<br/>(' . $Lang['eReportCard']['ReportArr']['ReportSummaryArr']['juniorFormsEn'] . ')';
        $header .= '</td>';
        $header .= returnReportSummaryReportYearHeaderRow($yearAry);
    $header .= '</tr>';
    
    $header .= '<tr>';
        $header .= returnReportSummaryReportLevelHeaderRow($levelAry);
    $header .= '</tr>';
        
    $header .= '<tr>';
        $header .= returnReportSummaryReportAssessmentHeaderRow($levelAry);
    $header .= '</tr>';
    
    return $header;
}

function returnReportSummaryReportYearHeaderRow($year)
{
    $headerRow = '';
    $yearCount = count((array) $year);
    for ($i = 0; $i < $yearCount; $i ++)
    {
        if ($year[$i] == '') {
            continue;
        }
        $headerRow .= '<td colspan="4">' . $year[$i] . '</td>';
    }
    
    return $headerRow;
}

function returnReportSummaryReportLevelHeaderRow($form)
{
    global $Lang;
    
    $headerRow = '';
    $formCount = count((array) $form);
    for ($i = 0; $i < $formCount; $i ++)
    {
        if ($form[$i] == '') {
            continue;
        }
        
        $form[$i] = $Lang['eReportCard']['ReportArr']['ReportSummaryArr']['SecondaryEn'] . ' ' . $form[$i];
        $headerRow .= '<td colspan="4">' . $form[$i] . '</td>';
    }
    
    return $headerRow;
}

function returnReportSummaryReportAssessmentHeaderRow($form)
{
    global $Lang;
    global $termAssessmentAry;
    
    $headerRow = '';
    
    $formCount = count((array) $form);
    if($formCount > 0)
    {
        $table_width = $formCount == 1 ? 68 : 80;
        $table_width = $table_width / ($formCount * 4);
    }
    
    for ($i = 0; $i < $formCount; $i ++)
    {
        if ($form[$i] == '') {
            continue;
        }
        
        foreach($termAssessmentAry as $_termNumber => $_termAssessmentAry)
        {
            foreach($_termAssessmentAry as $__termAssessment)
            {
                switch ($__termAssessment) {
                    case "A1":
                        $__termAssessmentType = 'Term'.$_termNumber.'CA';
                        break;
                    case "A2":
                        $__termAssessmentType = 'Term'.$_termNumber.'Exam';
                        break;
                    default:
                        $__termAssessmentType = 'YearGrade';
                        break;
                }
                $headerRow .= '<td width="'.$table_width.'%">' . $Lang['eReportCard']['ReportArr']['ReportSummaryArr'][$__termAssessmentType.'En']. '</td>';
            }
        }
    }
    
    return $headerRow;
}

include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();

// ## Set Common Variables
$emptySymbol = '--';
$subjectNameStringLengthAdjustLimit = 30;
$StudentInfoKeyAry = array();
$StudentInfoKeyAry[0] = array(
    'Class',
    'EnglishName',
    'Gender'
);
$StudentInfoKeyAry[1] = array(
    'ClassNumber',
    'ChineseName',
    'UserLogin'
);
$termAssessmentAry = array();
$termAssessmentAry[1] = array('A1');
$termAssessmentAry[2] = array('A1', 'A2', '0');

// ## POST data
$studentIdAry = $_POST['studentIdAry'];
$includeCurrentYear = $_POST['includeCurrentYear'];

// ## Get Subject Code
$subjectCodeAry = array();
$subjectCodeAry['id'] = $lreportcard->GET_SUBJECTS_CODEID_MAP(1, 0);
$subjectCodeAry['code'] = $lreportcard->GET_SUBJECTS_CODEID_MAP(1, 1);

// ## Load all Target Student
$luserObj = new libuser('', '', $studentIdAry);
$numOfStudent = count($studentIdAry);

// ## Get Student Class History Info
$studentClassHistoryAry = $lreportcard->Get_Student_From_School_Settings($AcademicYearIDAry = '', $studentIdAry);
$numOfData = count($studentClassHistoryAry);

// ## Analyze Student Class History Info
$studentDataAssoAry = array();
for ($i = 0; $i < $numOfData; $i ++)
{
    $_studentId = $studentClassHistoryAry[$i]['UserID'];
    $_formId = $studentClassHistoryAry[$i]['YearID'];
    $_formName = $studentClassHistoryAry[$i]['YearName'];
    $_formNumber = $lreportcard->GET_FORM_NUMBER($_formId, 1);
    $_academicYearId = $studentClassHistoryAry[$i]['AcademicYearID'];
    $_academicYearName = $studentClassHistoryAry[$i]['AcademicYearNameEn'];
    
    // Handle S1-Testing in client site
    if ($_formId == 10)
    {
        continue;
    }
    
    // for Junior Level (S1 - S2) only
    if($_formNumber && $_formNumber > 2)
    {
        continue;
    }
    
    // Exclude Current Year
    $isActiveYear = $_academicYearId == $lreportcard->GET_ACTIVE_YEAR_ID();
    $IsIncludeCurrentYear = $includeCurrentYear && ($_formNumber == 1 || $_formNumber == 2);
    if($isActiveYear && !$IsIncludeCurrentYear)
    {
        continue;
    }
    
    // Define data first
    if (! isset($studentDataAssoAry[$_studentId]))
    {
        $studentDataAssoAry[$_studentId]['firstFormNumber'] = $_formNumber;
        $studentDataAssoAry[$_studentId]['firstAcademicYearId'] = $_academicYearId;
        
        $studentDataAssoAry[$_studentId]['classHistoryAry'] = array();
        $studentDataAssoAry[$_studentId]['classHistoryAry']['firstFormId'] = '';
        $studentDataAssoAry[$_studentId]['classHistoryAry']['firstFormNumber'] = '';
        $studentDataAssoAry[$_studentId]['classHistoryAry']['firstAcademicYearId'] = '';
    }
    
    $studentDataAssoAry[$_studentId]['classHistoryAry']['levelAcademicYearAssoAry'][$_academicYearId]['formId'] = $_formId;
    $studentDataAssoAry[$_studentId]['classHistoryAry']['levelAcademicYearAssoAry'][$_academicYearId]['formName'] = $_formName;
    $studentDataAssoAry[$_studentId]['classHistoryAry']['levelAcademicYearAssoAry'][$_academicYearId]['formNumber'] = $_formNumber;
    $studentDataAssoAry[$_studentId]['classHistoryAry']['levelAcademicYearAssoAry'][$_academicYearId]['academicYearId'] = $_academicYearId;
    $studentDataAssoAry[$_studentId]['classHistoryAry']['levelAcademicYearAssoAry'][$_academicYearId]['academicYearName'] = $_academicYearName;
    $studentDataAssoAry[$_studentId]['classHistoryAry']['levelAcademicYearAssoAry'][$_academicYearId]['studentStudied'] = true;
}

// ## Remove year data after student left school
foreach ((array) $studentDataAssoAry as $_studentId => $_studentDataAry)
{
    // Find student last year in school
    $_studentLastStudiedAcademicYearId = '';
    foreach ((array) $_studentDataAry['classHistoryAry']['levelAcademicYearAssoAry'] as $___academicYearId => $___academicYearInfoAry)
    {
        $___studentStudied = $___academicYearInfoAry['studentStudied'];
        if ($___studentStudied) {
            $_studentLastStudiedAcademicYearId = $___academicYearId;
        }
    }
    
    // Remove year data after last studied year
    $_startToDelete = false;
    foreach ((array) $_studentDataAry['classHistoryAry']['levelAcademicYearAssoAry'] as $___academicYearId => $___academicYearInfoAry)
    {
        if ($_startToDelete) {
            unset($studentDataAssoAry[$_studentId]['classHistoryAry']['levelAcademicYearAssoAry'][$___academicYearId]);
        }
        
        if ($___academicYearId == $_studentLastStudiedAcademicYearId) {
            $_startToDelete = true;
        }
    }
}

// ## Remove year data if student not studied
foreach ((array) $studentDataAssoAry as $_studentId => $_studentDataAry)
{
    $__studentNotStudyingAllForm = true;
    foreach ((array) $_studentDataAry['classHistoryAry']['levelAcademicYearAssoAry'] as $___academicYearId => $___academicYearInfoAry)
    {
        $___isStudentStudiedThisYear = $___academicYearInfoAry['studentStudied'];
        if ($___isStudentStudiedThisYear) {
            $__studentNotStudyingAllForm = false;
            break;
        }
    }
    
    if ($__studentNotStudyingAllForm) {
        unset($studentDataAssoAry[$_studentId]['classHistoryAry']);
    }
}

// ## Get year data
foreach ((array) $studentDataAssoAry as $_studentId => $_studentDataAry)
{
    foreach ((array) $_studentDataAry['classHistoryAry']['levelAcademicYearAssoAry'] as $__academicYearId => $__academicYearInfoAry)
    {
        $__formId = $__academicYearInfoAry['formId'];
        $__formNumber = $__academicYearInfoAry['formNumber'];
        
        if ($studentDataAssoAry[$_studentId]['classHistoryAry']['firstFormId'] === '')
        {
            $studentDataAssoAry[$_studentId]['firstFormNumber'] = $__formNumber;
            $studentDataAssoAry[$_studentId]['firstAcademicYearId'] = $__academicYearId;
            
            $studentDataAssoAry[$_studentId]['classHistoryAry']['firstFormId'] = $__formId;
            $studentDataAssoAry[$_studentId]['classHistoryAry']['firstFormNumber'] = $__formNumber;
            $studentDataAssoAry[$_studentId]['classHistoryAry']['firstAcademicYearId'] = $__academicYearId;
        }
    }
}

// ## Get all years
$allAcademicYearFormInfoAry = array();
foreach ((array) $studentDataAssoAry as $_studentId => $_studentDataAry)
{
    foreach ((array) $_studentDataAry['classHistoryAry']['levelAcademicYearAssoAry'] as $___academicYearId => $___academicYearInfoAry)
    {
        $____formId = $___academicYearInfoAry['formId'];
        if ($____formId != '' && ! in_array($____formId, (array) $allAcademicYearFormInfoAry[$___academicYearId])) {
            $allAcademicYearFormInfoAry[$___academicYearId][] = $____formId;
        }
    }
}

// ## Get Report related data
$markAry = array();
$subjectAry = array();
$subjectGradingSchemeAry = array();
$ReportSummaryRemarksAry = array();
foreach ((array) $allAcademicYearFormInfoAry as $_academicYearId => $_formIdAry)
{
    $isActiveYear = $_academicYearId == $lreportcard->GET_ACTIVE_YEAR_ID();
    $IsIncludeCurrentYear = $includeCurrentYear;
    if($isActiveYear && !$IsIncludeCurrentYear)
    {
        continue;
    }
    
    $_lreportcard = new libreportcard($_academicYearId);
    $SemesterList = getSemesters($_academicYearId, 0);
    
    $_numOfForm = count($_formIdAry);
    for ($i = 0; $i < $_numOfForm; $i ++)
    {
        foreach($termAssessmentAry as $_termNumber => $_termAssessmentAry)
        {
            $__formId = $_formIdAry[$i];
            $__reportSemesterId = $SemesterList[($_termNumber - 1)]['YearTermID'];
            
            // Get 2nd Term Report (S6 : 1st Term Report)
            $__reportInfoAry = $_lreportcard->returnReportTemplateBasicInfo($ReportID = '', $others = '', $__formId, $__reportSemesterId, $isMainReport = 1);
            $__reportId = $__reportInfoAry['ReportID'];
            
            // Get Target Student Marks
            $cons = " AND a.StudentID IN ('" . implode("', '", (array) $studentIdAry) . "')";
            $markAry[$_academicYearId][$_termNumber][$__formId] = $_lreportcard->getMarksCommonIpEj($__reportId, $StudentID = '', $cons, $ParentSubjectOnly = 0, $includeAdjustedMarks = 1, $OrderBy = '', $SubjectID = '', $ReportColumnID = '0');
            
            // Get form Subjects
            $subjectAry[$_academicYearId][$_termNumber][$__formId] = $_lreportcard->returnSubjectwOrder($__formId, $ParForSelection = 0, $TeacherID = '', $SubjectFieldLang = 'en', $ParDisplayType = 'Desc', $ExcludeCmpSubject = 0, $__reportId);
            
            // Get form Grading Scheme Info
            $subjectGradingSchemeAry[$_academicYearId][$_termNumber][$__formId] = $_lreportcard->GET_SUBJECT_FORM_GRADING($__formId, $SubjectID = '', $withGrandResult = 0, $returnAsso = 1, $__reportId);
        }
    }
}

// ## Analyze Student Mark Info
foreach ((array) $studentDataAssoAry as $_studentId => $_dataAry)
{
    $__levelDataAry = $_dataAry['classHistoryAry'];
    $__levelKeyFirstFormId = $__levelDataAry['firstFormId'];
    $__levelKeyFirstAcademicYearId = $__levelDataAry['firstAcademicYearId'];
    $__studentClassHistoryAry = $__levelDataAry['levelAcademicYearAssoAry'];
    
    // Merge all studied subjects in same level
    $__mergedSubjectAry = array();
    foreach ((array) $__studentClassHistoryAry as $___academicYearId => $___academicYearInfoAry)
    {
        foreach($termAssessmentAry as $___termNumber => $___termAssessmentAry)
        {
            $___formId = $___academicYearInfoAry['formId'];
            $__mergedSubjectAry = $__mergedSubjectAry + (array) $subjectAry[$___academicYearId][$___termNumber][$___formId];
        }
    }
    
    foreach ((array) $__mergedSubjectAry as $___parentSubjectId => $___cmpSubjectAry)
    {
        foreach ((array) $___cmpSubjectAry as $____cmpSubjectID => $____cmpSubjectName)
        {
            $____isComponentSubject = ($____cmpSubjectID == 0) ? false : true;
            $____subjectId = ($____isComponentSubject) ? $____cmpSubjectID : $___parentSubjectId;
            
            // Get Student Subject Marks
            foreach ((array) $__studentClassHistoryAry as $_____academicYearId => $_____academicYearDataAry)
            {
                $_____formId = $_____academicYearDataAry['formId'];
                $_____studentStudiesThisForm = $_____academicYearDataAry['studentStudied'];
                
                foreach($termAssessmentAry as $___termNumber => $___termAssessmentAry)
                {
                    // Get Subject Grading Scheme
                    $_____subjectGradingSchemeInfoAry = $subjectGradingSchemeAry[$_____academicYearId][$___termNumber][$_____formId][$____subjectId];
                    $_____subjectScaleDisplay = $_____subjectGradingSchemeInfoAry['ScaleDisplay'];
                    $studentDataAssoAry[$_studentId]['classHistoryAry']['levelAcademicYearAssoAry'][$_____academicYearId]['markAry'][$___termNumber][$____subjectId]['scaleDisplay'] = $_____subjectScaleDisplay;
                    
                    /*
                     * - Subject Mark : Show "--"
                     * 1. Not studied in this year
                     * 2. No Grading Settings
                     * 3. Subject Marks Empty
                     */
                    if (! $_____studentStudiesThisForm || $_____subjectScaleDisplay == '' || ! isset($markAry[$_____academicYearId][$___termNumber][$_____formId][$_studentId][$____subjectId][$thisReportColumnID = 0]))
                    {
                        $_____markDisplay = $emptySymbol;
                    }
                    else
                    {
                        // $_____mark = my_round($markAry[$_____academicYearId][$_____formId][$_studentId][$____subjectId][$thisReportColumnID=0]['Mark'], 1);
                        $_____grade = $markAry[$_____academicYearId][$___termNumber][$_____formId][$_studentId][$____subjectId][$thisReportColumnID = 0]['Grade'];
                        
                        $_____markDisplay = '';
                        if ($_____grade != '' && $lreportcard->Check_If_Grade_Is_SpecialCase($_____grade)) {
                            $_____markDisplay = $emptySymbol;
                        }
                        else {
                            $_____markDisplay = $_____grade;
                        }
                        
                        if ($_____markDisplay == '') {
                            $_____markDisplay = $emptySymbol;
                        }
                    }
                    
                    $studentDataAssoAry[$_studentId]['classHistoryAry']['levelAcademicYearAssoAry'][$_____academicYearId]['markAry'][$___termNumber][$____subjectId]['markDisplay'] = $_____markDisplay;
                }
            }
        }
    }
}

include_once ($PATH_WRT_ROOT . "includes/libaccountmgmt.php");
$laccount = new libaccountmgmt();

// ## Build Report Content
$ReportSummaryContentAry = array();
for ($i = 0; $i < $numOfStudent; $i ++)
{
    $_isLastStudent = false;
    if ($i == $numOfStudent - 1) {
        $_isLastStudent = true;
    }
    
    // Get Student data
    $_studentId = $studentIdAry[$i];
    $luserObj->LoadUserData($_studentId);
    $_studentClassInfo = $lreportcard->Get_Student_Class_ClassLevel_Info($_studentId);
    
    $_studentInfoAry = array();
    $_studentInfoAry["EnglishName"] = str_replace('*', '', ($luserObj->EnglishName? $luserObj->EnglishName : $emptySymbol));
    $_studentInfoAry["ChineseName"] = str_replace('*', '', ($luserObj->ChineseName? $luserObj->ChineseName: $emptySymbol));
    $_studentInfoAry['Class']       = $_studentClassInfo[0]['ClassName'];
    $_studentInfoAry['ClassNumber'] = $_studentClassInfo[0]['ClassNumber'];
    $_studentInfoAry["Gender"]      = $luserObj->Gender? $luserObj->Gender : $emptySymbol;
    $_studentInfoAry["UserLogin"]   = $luserObj->UserLogin? $luserObj->UserLogin : $emptySymbol;
    //$_studentInfoAry["RegNo"]       = str_replace('#', '', $luserObj->WebSAMSRegNo);
    
    // Get Years
    $_tableNumber = 0;
    $_reportPageAry = array();
    $__levelAcademicYearCount = 0;
    $__levelDataAry = $studentDataAssoAry[$_studentId]['classHistoryAry']['levelAcademicYearAssoAry'];
    $__numOfLevelAcademicYear = count((array) $__levelDataAry);
    foreach ((array) $__levelDataAry as $___academicYearId => $___academicYearInfoAry)
    {
        $isActiveYear = $___academicYearId == $lreportcard->GET_ACTIVE_YEAR_ID();
        $IsIncludeCurrentYear = $includeCurrentYear;
        if($isActiveYear && !$IsIncludeCurrentYear)
        {
            continue;
        }
        
        $_reportPageAry[$_tableNumber]['levelKey'] = 'junior';
        $_reportPageAry[$_tableNumber]['academicYearIdAry'][] = $___academicYearId;
        
        // Open new table for another level
        $__levelAcademicYearCount ++;
        if ($__levelAcademicYearCount == $__numOfLevelAcademicYear) {
            $_tableNumber ++;
        }
    }
    
    // Merge all studied subjects of same school level
    $__mergedSubjectAry = array();
    $__levelDataAry = $studentDataAssoAry[$_studentId]['classHistoryAry']['levelAcademicYearAssoAry'];
    foreach ((array) $__levelDataAry as $___academicYearId => $___academicYearInfoAry)
    {
        foreach($termAssessmentAry as $___termNumber => $___termAssessmentAry)
        {
            $___formId = $___academicYearInfoAry['formId'];
            $__mergedSubjectAry = $__mergedSubjectAry + (array) $subjectAry[$___academicYearId][$___termNumber][$___formId];
        }
    }
    
    // Get Display Subject
    $_reportDisplaySubjectIdAry = array();
    foreach ((array) $__mergedSubjectAry as $___parentSubjectId => $___cmpSubjectAry)
    {
        foreach ((array) $___cmpSubjectAry as $____cmpSubjectID => $____cmpSubjectName)
        {
            $____isComponentSubject = ($____cmpSubjectID == 0) ? false : true;
            $____subjectId = ($____isComponentSubject) ? $____cmpSubjectID : $___parentSubjectId;
            $____subjectCode = $subjectCodeAry['id'][$____subjectId]['CODEID'];
            
            // No need to display Component => SKIP
            if ($____isComponentSubject) {
                continue;
            }
            
            // Use Component Marks for checking
            $____isAllNA = true;
            $____didNotStudiedThisLevel = true;
            foreach ((array) $__levelDataAry as $_____academicYearId => $_____academicYearInfoAry)
            {
                foreach($termAssessmentAry as $_termNumber => $___termAssessmentAry)
                {
                    foreach($___termAssessmentAry as $_____assessmentType)
                    {
                        $_____assessmentSubjectCode = $eRCTemplateSetting['Report']['DataTransferCompAssessmentAry'][$_____assessmentType];
                        $_____assessmentSubjectId = $subjectCodeAry['code'][$____subjectCode . '_' . $_____assessmentSubjectCode];
                        $_____assessmentSubjectId = $_____assessmentSubjectId ? $_____assessmentSubjectId: - 999;
                        
                        $_____markDisplay = $_____academicYearInfoAry['markAry'][$_termNumber][$_____assessmentSubjectId]['markDisplay'];
                        $_____studentStuided = $_____academicYearInfoAry['studentStudied'];
                        
                        if ($_____studentStuided) {
                            $____didNotStudiedThisLevel = false;
                        }
                        if ($_____markDisplay != '' && $_____markDisplay != $emptySymbol) {
                            $____isAllNA = false;
                        }
                    }
                }
            }
            
            // Not studied this level => Shows "--"
            if ($____didNotStudiedThisLevel || ! $____isAllNA) {
                $_reportDisplaySubjectIdAry[] = $____subjectId;
            }
        }
    }
    
    // Get Report Header
    $_reportHeaderHtml = returnReportSummaryStudentInfoTable($StudentInfoKeyAry, $_studentInfoAry);
    
    // Get Report Main Content
    $_reportResultHtml = '';
    foreach ((array) $_reportPageAry as $___tableNumber => $___tableDataAry)
    {
        $___academicYearIdAry = $___tableDataAry['academicYearIdAry'];
        
        // Seperator (between 2 table)
        if ($___tableNumber > 0)
        {
            $_reportResultHtml .= '<td width="2%">&nbsp;</td>';
        }
        
        // Get Table Header row
        $____yearAry = array();
        $____levelAry = array();
        $numOfAcademicYearPerRow = count((array) $___academicYearIdAry);
        for ($j = 0; $j < $numOfAcademicYearPerRow; $j ++)
        {
            $____academicYearId = $___academicYearIdAry[$j];
            if ($____academicYearId == '')
            {
                $____yearAry[] = $emptySymbol;
                $____levelAry[] = $emptySymbol;
            }
            else
            {
                $____yearAry[] = $studentDataAssoAry[$_studentId]['classHistoryAry']['levelAcademicYearAssoAry'][$____academicYearId]['academicYearName'];
                $____levelAry[] = $studentDataAssoAry[$_studentId]['classHistoryAry']['levelAcademicYearAssoAry'][$____academicYearId]['formNumber'];
            }
        }
        $_reportResultHeaderHtml = returnReportSummaryReportHeader($____yearAry, $____levelAry);
        
        // Get Table Content
        // Merge all studied subjects of same school level
        $___mergedSubjectAry = array();
        $___levelDataAry = $studentDataAssoAry[$_studentId]['classHistoryAry'];
        foreach ((array) $___levelDataAry['levelAcademicYearAssoAry'] as $____academicYearId => $____academicYearInfoAry)
        {
            foreach($termAssessmentAry as $_termNumber => $___termAssessmentAry)
            {
                $____formId = $____academicYearInfoAry['formId'];
                $___mergedSubjectAry = $___mergedSubjectAry + (array) $subjectAry[$____academicYearId][$_termNumber][$____formId];
            }
        }
        
        // Sort merged subjects (Main Subject only)
        $___mergedMainSubjectAry = array();
        foreach ($___mergedSubjectAry as $____SubjectId => $____subjectCmpAry)
        {
            $___mergedMainSubjectAry[$____SubjectId] = $____subjectCmpAry[0];
        }
        $___mergedMainSubjectAry = asorti($___mergedMainSubjectAry);
        
        // Build Table Subject rows
        $_reportResultContentHtml = '';
        $___displayedSubjectIdAry = array();
        foreach ((array) $___mergedMainSubjectAry as $____parentSubjectId => $____parentSubjectName)
        {
            // No need to display > SKIP
            if (! in_array($____parentSubjectId, (array) $_reportDisplaySubjectIdAry)) {
                continue;
            }
            // Already displayed > SKIP
            if (in_array($____parentSubjectId, (array) $___displayedSubjectIdAry)) {
                continue;
            }
            
            $____cmpSubjectAry = $___mergedSubjectAry[$____parentSubjectId];
            foreach ((array) $____cmpSubjectAry as $_____cmpSubjectID => $_____cmpSubjectName)
            {
                $_____isComponentSubject = ($_____cmpSubjectID == 0) ? false : true;
                $_____subjectId = ($_____isComponentSubject) ? $_____cmpSubjectID : $____parentSubjectId;
                $____subjectCode = $subjectCodeAry['id'][$_____subjectId]['CODEID'];
                
                // No need to display Component => SKIP
                if ($_____isComponentSubject) {
                    continue;
                }
                
                // Get Subject Name
                $_____subjectNameEn = $lreportcard->GET_SUBJECT_NAME_LANG($_____subjectId, 'en');
                        
                $_____subjectTr = '';
                $_____subjectTr .= '<tr>' . "\r\n";
                    $_____subjectTr .= '<td style="text-align: left; padding-left: 4px;">' . $_____subjectNameEn . '</td>' . "\r\n";
                
                // Subject Year Grade
                $numOfAcademicYearPerRow = count((array) $___academicYearIdAry);
                for ($j = 0; $j < $numOfAcademicYearPerRow; $j ++)
                {
                    $______academicYearId = $___academicYearIdAry[$j];
                    $______formId = $studentDataAssoAry[$_studentId]['classHistoryAry']['levelAcademicYearAssoAry'][$______academicYearId]['formId'];
                    $______formName = $studentDataAssoAry[$_studentId]['classHistoryAry']['levelAcademicYearAssoAry'][$______academicYearId]['formName'];
                    $______formNumber = $studentDataAssoAry[$_studentId]['classHistoryAry']['levelAcademicYearAssoAry'][$______academicYearId]['formNumber'];
                    $______studentStudied = $studentDataAssoAry[$_studentId]['classHistoryAry']['levelAcademicYearAssoAry'][$______academicYearId]['studentStudied'];
                    
                    foreach($termAssessmentAry as $___termNumber => $___termAssessmentAry)
                    {
                        foreach($___termAssessmentAry as $_____assessmentType)
                        {
                            $_____assessmentSubjectCode = $eRCTemplateSetting['Report']['DataTransferCompAssessmentAry'][$_____assessmentType];
                            $_____assessmentSubjectId = $subjectCodeAry['code'][$____subjectCode . '_' . $_____assessmentSubjectCode];
                            $_____assessmentSubjectId = $_____assessmentSubjectId ? $_____assessmentSubjectId: - 999;
                           
                            // Not studied in this year => Show "--"
                            if ($______formId == '' && $______formName != '') {
                                $_____subjectTr .= '<td>' . $emptySymbol . '</td>' . "\r\n";
                            }
                            // Empty Year Column => show "--"
                            else if ($______formId == '' && $______formName == '') {
                                $_____subjectTr .= '<td>' . $emptySymbol . '</td>' . "\r\n";
                            }
                            // Studied this year => Show Grade
                            else {
                                $_____targetSubjectId = $_____assessmentSubjectId;
                                $______subjectMarkDisplay = $studentDataAssoAry[$_studentId]['classHistoryAry']['levelAcademicYearAssoAry'][$______academicYearId]['markAry'][$___termNumber][$_____targetSubjectId]['markDisplay'];
                                $_____subjectTr .= '<td>' . $______subjectMarkDisplay . '</td>' . "\r\n";
                            }
                        }
                    }
                    
                    $___displayedSubjectIdAry[] = $_____targetSubjectId;
                }
                
                $_____subjectTr .= '</tr>' . "\r\n";
                $_reportResultContentHtml .= $_____subjectTr;
            }
        }
        if ($_reportResultContentHtml == '') {
            continue;
        }
        
        $table_width = count((array)$____levelAry) == 1 ? "55%" : "95%";
        $_reportResultHtml .= '<td valign="top" width="100%">';
            $_reportResultHtml .= '<table cellspacing="0" cellpadding="0" border="1" class="table-Junior" style="width:'.$table_width.'">' . "\r\n";
                $_reportResultHtml .= $_reportResultHeaderHtml;
                $_reportResultHtml .= $_reportResultContentHtml;
            $_reportResultHtml .= '</table>' . "\r\n"; 
        $_reportResultHtml .= '</td>';
    }
    $needPageBreak = $_isLastStudent ? '' : '<pagebreak />';
    
    // Report Header & Table
    $x = '';
    $x .= '<div style="width:100%;" align="center">' . "\r\n";
        $x .= '<div class="container" height = "240mm;">' . "\r\n";
            $x .= '<table cellpadding="0" cellspacing="0" align="center" style="width:100%; vertical-align:top;">' . "\r\n";
                $x .= $_reportHeaderHtml;
            $x .= '</table>' . "\r\n";
            $x .= '<table cellspacing="0" cellpadding="0" style="width:100%;"><tr>' . "\r\n";
                $x .= $_reportResultHtml;
            $x .= '</tr></table>' . "\r\n";
        $x .= '</div>' . "\r\n";
        $x .= $needPageBreak;
    $x .= '</div>' . "\r\n";
    $ReportSummaryContentAry[] = $x;
}

// Report Margin
$margin_top = 18;
$margin_bottom = 5;
$margin_left = 8;
$margin_right = 8;

// Create mPDF object
require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");
$mpdfObj = new mPDF('zh', "A4", 0, "", $margin_left, $margin_right, $margin_top, $margin_bottom, $margin_header, $margin_footer);

// PDF Setting
$mpdfObj->charset_in = "UTF-8";
$mpdfObj->allow_charset_conversion = true;
$mpdfObj->list_auto_mode = "mpdf";
$mpdfObj->backupSubsFont = array('times', 'kaiu', 'mingliu', 'msjh');
$mpdfObj->useSubstitutions = true;

# Define header
$mpdfObj->DefHTMLHeaderByName("titleHeader", "");
$mpdfObj->SetHTMLHeaderByName("titleHeader");

# Define footer
$mpdfObj->DefHTMLFooterByName("emptyFooter", "");
$mpdfObj->SetHTMLFooterByName("emptyFooter");

$ReportSummaryCSS = returnReportSummaryCSS();
$mpdfObj->writeHTML($ReportSummaryCSS);

$mpdfObj->writeHTML("<body>");
foreach ($ReportSummaryContentAry as $thisReportSummaryContent)
{
    $mpdfObj->writeHTML($thisReportSummaryContent);
}
$mpdfObj->writeHTML("</body>");

// Output file
$mpdfObj->Output("ReportCard Summary.pdf", 'I');

intranet_closedb();

// echo $ReportSummaryCSS;
// foreach($ReportSummaryContentAry as $thisReportSummaryContent)
// {
// echo $thisReportSummaryContent;
// }

// For performance tunning info
// echo '<script language="JavaScript" src="/templates/jquery/jquery-1.3.2.min.js"></script>';
// debug_pr('convert_size(memory_get_usage(true)) = '.convert_size(memory_get_usage(true)));
// debug_pr('Query Count = '.$GLOBALS[debug][db_query_count]);
// $lreportcard->db_show_debug_log();
// die();
?>