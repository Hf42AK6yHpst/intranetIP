<?
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$ViewFormat = $_POST['ViewFormat'];
$YearID = $_POST['YearID'];

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	if ($lreportcard->hasAccessRight()) 
	{
		
		# Report Title
		$reportInfoAry = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		$isMainReport = $reportInfoAry['isMainReport'];
		
		//$ActiveYear = $lreportcard->GET_ACTIVE_YEAR("-");
		//$ObjAcademicYear = new academic_year($lreportcard->schoolYearID);
		//$ActiveYear = $ObjAcademicYear->Get_Academic_Year_Name();
		//$SemTitle = $lreportcard->returnSemesters($TermID);
		//$ReportType = $eReportCard['Reports_SubjectPrizeReport'];
		//$ReportTitle = $ActiveYear." ".$SemTitle." ".$ReportType;
		
		//$numOfTarget = count($TargetIDArr);
		$ClassNameArr= array();
		$ClassIDReportIDMap = array(); 
		$FormMaxMark = array();
		$FormMinMark = array();
		//for ($j=0; $j<$numOfTarget; $j++)
		//{
			$thisClassLevelID = $YearID;
			$thisClassIDList = Get_Array_By_Key($lreportcard->GET_CLASSES_BY_FORM($thisClassLevelID),"ClassID");
			
			# Get Report Info
			$thisReportInfoArr = $lreportcard->GET_REPORT_TYPES($thisClassLevelID, $ReportID);
			
			$thisReportID = $thisReportInfoArr['ReportID'];
			$TermID = $thisReportInfoArr['Semester'];
			
			$SubjectList = $lreportcard->returnSubjectIDwOrder($thisClassLevelID);
			$numOfSubject = count($SubjectList);
			for($i=0; $i<$numOfSubject; $i++)
			{
				$thisSubjectID = $SubjectList[$i];
				
				//skip subject which are not chosen
				if(!in_array($thisSubjectID,(array)$SubjectIDArr))	continue;
				
				$ResultStat = $lreportcard->Get_Scheme_Grade_Statistics_Info($thisReportID, $thisSubjectID);

				if(empty($ResultStat)) continue;
				
				$GradeList[$hisClassLevel][$thisSubjectID] = array_keys($ResultStat);
				foreach((array)$ResultStat as $thisGrade => $ClassSummaryArr)
				{
					$thisGradeNature = $ClassSummaryArr[0]["Nature"];
					$FormMaxMark[$thisClassLevelID][$thisSubjectID][$thisGrade] = $ClassSummaryArr[0]["MaxMark"];
					$FormMinMark[$thisClassLevelID][$thisSubjectID][$thisGrade] = $ClassSummaryArr[0]["MinMark"];
					
					foreach((array)$ClassSummaryArr as $thisClassID => $ClassSummary)
					{
						if (isset($SD_Avg_Arr[$thisClassID]) == false)
						{
							$SD_Avg_Arr[$thisClassID] = $lreportcard->getFormSubjectSDAndAverage($ReportID, '', $thisClassID);
						}
						
						$SDList[$thisClassLevelID][$thisSubjectID][$thisClassID] = my_round($SD_Avg_Arr[$thisClassID][0][$thisSubjectID], 2);
						$AVGList[$thisClassLevelID][$thisSubjectID][$thisClassID] = my_round($SD_Avg_Arr[$thisClassID][1][$thisSubjectID], 2);
						$ClassGradeArr[$thisClassLevelID][$thisSubjectID][$thisClassID][$thisGrade] = $ClassSummary;
						$TotalStudent[$thisClassLevelID][$thisSubjectID][$thisClassID] += $ClassSummary["NumOfStudent"];
						if($thisGradeNature != "F")
							$TotalPass[$thisClassLevelID][$thisSubjectID][$thisClassID] += $ClassSummary["NumOfStudent"];
						
					}
					
				}
			}
		//}

		## Display the result
		if ($ViewFormat == 'html')
		{
			$ObjYear = new academic_year($lreportcard->schoolYearID);
			$AcademicYear = $ObjYear->Get_Academic_Year_Name();

			# Get Sem Title 
			$SemsData = array_keys((array)getSemesters($lreportcard->schoolYearID));
			
			$SemSeqNo = array_search($TermID,(array)$SemsData) +1;
			//2014-1118-1057-12206
			//$SemTitle = $TermID=="F"?$eReportCard['ExaminationSummary']['YearResult']:($eReportCard['ExaminationSummary']['Exam']." ".$SemSeqNo);
			if ($TermID=="F") {
				$SemTitle = $eReportCard['ExaminationSummary']['YearResult'];
			}
			else {
				if ($isMainReport) {
					$SemTitle = $eReportCard['ExaminationSummary']['Exam']." ".$SemSeqNo;
				}
				else {
					$SemTitle = $eReportCard['ExaminationSummary']['FormTest']." ".$SemSeqNo;
				}
			}
			
			
					
			foreach((array)$ClassGradeArr as $thisClassLevelID => $SubjectSummaryArr)
			{
				$ClassLevelName = $lreportcard->returnClassLevel($thisClassLevelID);
			
				foreach((array)$SubjectSummaryArr as $thisSubjectID => $ClassSummaryArr)
				{
					$thisGradeList = $GradeList[$hisClassLevel][$thisSubjectID];
					$numOfColumn = count($thisGradeList)+4;
					$tdwidth = "width='".(round(100/$numOfColumn))."%'";
					
					if (isset($SubjectNameArr[$thisSubjectID]) == false)
						$SubjectNameArr[$thisSubjectID] = $lreportcard->GET_SUBJECT_NAME_LANG($thisSubjectID);
					$thisSubjectName = $SubjectNameArr[$thisSubjectID];
					
					$FormSummary = $ClassSummaryArr[0]; // take away the first element (Form Summary)
					$gradeTitleRow = $gradeRangeRow = '';
					foreach((array)$thisGradeList as $thisGrade)
					{
						$gradeTitleRow .= "<td align='right' $tdwidth>$thisGrade</td>";
						$thisMax = $FormSummary[$thisGrade]["MaxMark"];
						$thisMin = $FormSummary[$thisGrade]["MinMark"];
						$thisRange = ($thisMax>0||$thisMin>0)?($thisMax==$thisMin?$thisMax:$thisMax." - ".$thisMin):" - ";
						$gradeRangeRow .= "<td align='right' $tdwidth>".$thisRange."</td>";
					}
					
					$thead = "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
						$thead .= "<tr>";
							$thead .= "<td align='right'>Form: $ClassLevelName</td>";
							$thead .= "<td align='right'>&nbsp;</td>";
							$thead .= "<td align='center' colspan='".($numOfColumn-2)."'>".$eReportCard['ExaminationSummary']['Year']." :".$AcademicYear."($SemTitle)</td>";	
						$thead .= "</tr>";
						$thead .= "<tr>";
							$thead .= "<td $tdwidth>&nbsp;</td>";
							$thead .= $gradeTitleRow;
							$thead .= "<td $tdwidth align='right'>".$thisSubjectName."</td>";	
						$thead .= "</tr>";
						$thead .= "<tr>";
							$thead .= "<td align='right' $tdwidth>".$eReportCard["Class"]."</td>";
							$thead .= $gradeRangeRow;
							$thead .= "<td $tdwidth align='right'>".$eReportCard["GradeDistribution"]["total_pass"]."</td>";	
							$thead .= "<td $tdwidth align='right'>".$eReportCard["Template"]["Average"]."</td>";
							$thead .= "<td $tdwidth align='right'>".$eReportCard["Template"]["SD"]."</td>";
						$thead .= "</tr>";
					$thead .= "</table>";		
						
					//tbody			
					$tbody = "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
					
					$FormTotalStudent = 0;
					$FormPassStudent = 0;
					foreach((array)$ClassSummaryArr as $thisClassID => $ClassSummary)
					{
						//skip form statistic
						if($thisClassID==0)	continue;		
						
						if (isset($ClassInfoArr[$thisClassID]) == false)
							$ClassInfoArr[$thisClassID] = $lreportcard->GET_CLASSES_BY_FORM($thisClassLevelID, $thisClassID);
						$ClassInfo = $ClassInfoArr[$thisClassID];
						
						$ClassName = Get_Lang_Selection($ClassInfo[0]["ClassTitleB5"],$ClassInfo[0]["ClassTitleEN"]);
						$ClassPassStudent = $TotalPass[$thisClassLevelID][$thisSubjectID][$thisClassID];
						$ClassTotalStudent = $TotalStudent[$thisClassLevelID][$thisSubjectID][$thisClassID];
						$ClassPassPercent = $ClassTotalStudent>0&&$ClassPassStudent>0?my_round($ClassPassStudent/$ClassTotalStudent*100,1):0;						
						$FormTotalStudent += $ClassTotalStudent;
						$FormPassStudent += $ClassPassStudent;
						
						# Average , Standard deviation
						//list($SD,$Avg) = $SDAvgList[$thisClassLevelID][$thisSubjectID][$thisClassID];
						$SD = $SDList[$thisClassLevelID][$thisSubjectID][$thisClassID];
						$Avg = $AVGList[$thisClassLevelID][$thisSubjectID][$thisClassID];

						$tbody .= "<tr>";
							$tbody .= "<td align='right' $tdwidth>$ClassName</td>";
							foreach((array)$thisGradeList as $thisGrade)
							{
								if($ClassSummary[$thisGrade]["NumOfStudent"]>0&&$TotalStudent[$thisClassLevelID][$thisSubjectID][$thisClassID]>0)
									$PercentOfPass = my_round($ClassSummary[$thisGrade]["NumOfStudent"]/$TotalStudent[$thisClassLevelID][$thisSubjectID][$thisClassID]*100,1);
								else
									$PercentOfPass = 0;
									
								$tbody .= "<td align='right' $tdwidth>";
								$tbody .= $ClassSummary[$thisGrade]["NumOfStudent"];
								$tbody .= "( $PercentOfPass%)";
								$tbody .= "</td>";
								
							}
							$tbody .= "<td align='right' $tdwidth>$ClassPassStudent ( $ClassPassPercent%)</td>";
							$tbody .= "<td align='right' $tdwidth>$Avg</td>";
							$tbody .= "<td align='right' $tdwidth>$SD</td>";
						$tbody .= "</tr>";
					}
					$tbody .= "</table>";
					
					//tfoot
					$FormPassPercent = $FormPassStudent>0&&$FormTotalStudent>0?my_round($FormPassStudent/$FormTotalStudent*100,1):0;
					$tfoot = "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
						$tfoot .= "<tr>";
							$tfoot .= "<td $tdwidth>&nbsp;</td>";
							foreach((array)$thisGradeList as $thisGrade)
							{
								if($FormPassStudent>0&&$FormTotalStudent>0)
									$PercentOfPass = my_round($ClassSummaryArr[0][$thisGrade]["NumOfStudent"]/$FormTotalStudent*100,1);
								else
									$PercentOfPass = 0;
								
								$tfoot .= "<td align='right' $tdwidth>";
								$tfoot .= $ClassSummaryArr[0][$thisGrade]["NumOfStudent"];
								$tfoot .= "( $PercentOfPass%)";
								$tfoot .= "</td>";
							}
							$tfoot .= "<td align='right' $tdwidth>$FormPassStudent ( $FormPassPercent%)</td>";
						$tfoot .= "</tr>";
						$tfoot .= "<tr>";
							$tfoot .= "<td align='right' colspan='".($numOfColumn-3)."'>".$eReportCard["GradeDistribution"]["TotalInForm"]."=</td>";
							$tfoot .= "<td align='right'>$FormTotalStudent</td>";
							$tfoot .= "<td align='right'>&nbsp;</td>";
							$tfoot .= "<td align='right'>&nbsp;</td>";
						$tfoot .= "</tr>";
					$tfoot .= "</table>";
					
					$page_break_css = ($subjectCtr++)%5==4?"style='page-break-after:always'":"";
					$table .= "<table id='ResultTable' class='GrandMSContentTable' width='80%' border='1' cellpadding='2' cellspacing='0' $page_break_css>\n";
						$table .= "<tr><td>";
							$table .= $thead;
						$table .= "</td></tr>";
						$table .= "<tr><td>";
							$table .= $tbody; 
						$table .= "</td></tr>";
						$table .= "<tr><td>";
							$table .= $tfoot;
						$table .= "</td></tr>";
					$table .= "</table><br><br>\n";
				}
				
			}
						
			$css = $lreportcard->Get_GrandMS_CSS();
			
			$allTable = '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>'.$table.'</td>
							</tr>
						</table>';
			
			echo $lreportcard->Get_Report_Header($ReportTitle);
			echo $table.$css;
			echo $lreportcard->Get_Report_Footer();
		}
		else if ($ViewFormat == 'csv')
		{
			include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
			include_once($PATH_WRT_ROOT."includes/libexporttext.php");
			
			$ExportHeaderArr = array();
			$ExportContentArr = array();
			$lexport = new libexporttext();
			
			foreach((array)$ClassGradeArr as $thisClassLevelID => $SubjectSummaryArr)
			{
				$ClassLevelName = $lreportcard->returnClassLevel($thisClassLevelID);
			
				foreach((array)$SubjectSummaryArr as $thisSubjectID => $ClassSummaryArr)
				{
					$thisGradeList = $GradeList[$hisClassLevel][$thisSubjectID];
					
					$FormSummary = $ClassSummaryArr[0]; // take away the first element (Form Summary)
					$gradeTitleRow = $gradeRangeRow = '';
					
					$ExportRowArr = array();
					$ExportRowArr[] = $ClassLevelName;
					$ExportRowArr[] = $lreportcard->schoolYear;
					$ExportRowArr[] = $lreportcard->GET_SUBJECT_NAME_LANG($thisSubjectID);
					$ExportContentArr[] = $ExportRowArr;
					
					$ExportRowArr = array();
					$ExportRowArr[] =  $eReportCard['Class'];
					foreach((array)$thisGradeList as $thisGrade)
					{
						$thisMax = $FormSummary[$thisGrade]["MaxMark"];
						$thisMin = $FormSummary[$thisGrade]["MinMark"];
						$thisRange = ($thisMax>=0&&$thisMin>=0)?$thisMax." - ".$thisMin:" - ";
						$ExportRowArr[] = $thisGrade."($thisRange) ".$eReportCard["GradeDistribution"]["NumOfStudent"];
						$ExportRowArr[] = $eReportCard["GradeDistribution"]["Percentage"];
					}
					$ExportRowArr[] = $eReportCard["GradeDistribution"]["total_pass"];
					$ExportRowArr[] = $eReportCard["GradeDistribution"]["Percentage"];
					$ExportRowArr[] = $eReportCard["Template"]["Average"];
					$ExportRowArr[] = $eReportCard["Template"]["SD"];
					$ExportContentArr[] = $ExportRowArr;
											
					//tbody			
					$FormTotalStudent = 0;
					$FormPassStudent = 0;
					foreach((array)$ClassSummaryArr as $thisClassID => $ClassSummary)
					{
						//skip form statistic
						if($thisClassID==0)	continue;		
						
						$ClassInfo = $lreportcard->GET_CLASSES_BY_FORM($thisClassLevelID,$thisClassID);
						$ClassName = $ClassInfo[0]["ClassTitleEN"];
						$ClassPassStudent = $TotalPass[$thisClassLevelID][$thisSubjectID][$thisClassID];
						$ClassTotalStudent = $TotalStudent[$thisClassLevelID][$thisSubjectID][$thisClassID];
						$ClassPassPercent = $ClassTotalStudent>0&&$ClassPassStudent>0?my_round($ClassPassStudent/$ClassTotalStudent*100,1):0;						
						$FormTotalStudent += $ClassTotalStudent;
						$FormPassStudent += $ClassPassStudent;
						
							$SD = $SDList[$thisClassLevelID][$thisSubjectID][$thisClassID];
							$Avg = $AVGList[$thisClassLevelID][$thisSubjectID][$thisClassID];
						
							$ExportRowArr = array();
							$ExportRowArr[] = $ClassName;
							foreach((array)$thisGradeList as $thisGrade)
							{
								if($ClassSummary[$thisGrade]["NumOfStudent"]>0&&$TotalStudent[$thisClassLevelID][$thisSubjectID][$thisClassID]>0)
									$PercentOfPass = my_round($ClassSummary[$thisGrade]["NumOfStudent"]/$TotalStudent[$thisClassLevelID][$thisSubjectID][$thisClassID]*100,1);
								else
									$PercentOfPass = 0;
								
								$ExportRowArr[] = $ClassSummary[$thisGrade]["NumOfStudent"] ;
								$ExportRowArr[] = "$PercentOfPass%";
							}
							$ExportRowArr[] = $ClassPassStudent;
							$ExportRowArr[] = "$ClassPassPercent%";
							$ExportRowArr[] = $Avg;
							$ExportRowArr[] = $SD;
							$ExportContentArr[] = $ExportRowArr;
					}
					
					//tfoot
					$FormPassPercent = $FormPassStudent>0&&$FormTotalStudent>0?my_round($FormPassStudent/$FormTotalStudent*100,1):0;
					$ExportRowArr = array();
					$ExportRowArr[] = ""; 
							foreach((array)$thisGradeList as $thisGrade)
							{
								if($FormPassStudent>0&&$FormTotalStudent>0)
									$PercentOfPass = my_round($ClassSummaryArr[0][$thisGrade]["NumOfStudent"]/$FormTotalStudent*100,1);
								else
									$PercentOfPass = 0;
								
								$ExportRowArr[] = $ClassSummaryArr[0][$thisGrade]["NumOfStudent"];
								$ExportRowArr[] = "$PercentOfPass%";
							}
							$ExportRowArr[] = $FormPassStudent;
							$ExportRowArr[] = "$FormPassPercent%";
							$ExportContentArr[] = $ExportRowArr; 
							
							$ExportRowArr=array();
							$ExportRowArr[] = $eReportCard["GradeDistribution"]["TotalInForm"];
							$ExportRowArr[] = $FormTotalStudent;
							$ExportContentArr[] = $ExportRowArr;
							$ExportContentArr[] = array(""); 

				}
				
			}
			
			$filename = "examiniation_summary.csv";
			
			$export_content = $lexport->GET_EXPORT_TXT($ExportContentArr,'');
			
			intranet_closedb();
			
			// Output the file to user browser
			$lexport->EXPORT_FILE($filename, $export_content);
		}
	}
}


?>