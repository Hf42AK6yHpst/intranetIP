<?php
// using : 

/***********************************************
 *  modification log
 *  20191120 Bill [2017-0901-1525-31265]
 *      - create file
 * ***********************************************/

$PageRight = array("STUDENT", "PARENT");

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
    }

	# Initial
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}

	if ($lreportcard->hasAccessRight())
	{
		$linterface = new interface_html();
		
		# Page Tag
		$CurrentPage = "Reports_PrintStudentReportCard";
		$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
		$TAGS_OBJ[] = array($eReportCard['ReportPrinting']);
		
		$linterface->LAYOUT_START();
		
		$submitBtn = $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submitBtn");

        $StudentHtmlDiv = '';
		if($ck_ReportCard_UserType == "STUDENT")
		{
            $StudentID = $_SESSION['UserID'];
            $StudentInfoArr = $lreportcard->Get_Student_Class_ClassLevel_Info($StudentID);
            $StudentInfoArr = $StudentInfoArr[0];

            $StudentName = Get_Lang_Selection($StudentInfoArr['ChineseName'], $StudentInfoArr['EnglishName']);
            $StudentClassName = Get_Lang_Selection($StudentInfoArr['ClassNameCh'], $StudentInfoArr['ClassName']);
            $StudentClassNumber = $StudentInfoArr['ClassNumber'];

            $StudentHtmlDiv .= "$StudentName ($StudentClassName-$StudentClassNumber)";
            $StudentHtmlDiv .= "<input type='hidden' name='TargetStudentID[]' id='TargetStudentID' value='$StudentID'>";
        }
        else if ($ck_ReportCard_UserType == "PARENT")
        {
            # Get Children
            $lu = new libuser($UserID);
            $ChildrenList = $lu->getChildrenList();

            $StudentID = $TargetStudentID[0];
            if ($StudentID == '' || !in_array($StudentID, Get_Array_By_Key($ChildrenList, 'StudentID'))) {
                $StudentID = $ChildrenList[0]['StudentID'];
            }
            $StudentInfoArr = $lreportcard->Get_Student_Class_ClassLevel_Info($StudentID);
            $StudentInfoArr = $StudentInfoArr[0];

            if(!empty($ChildrenList)) {
                $StudentHtmlDiv .= getSelectByArray($ChildrenList, " name='TargetStudentID[]' id='TargetStudentID' onchange=\"js_Reload()\"'", $StudentID, "", 1);
            }
		}
        $ClassLevelID = $StudentInfoArr['ClassLevelID'];
        $YearClassID = $StudentInfoArr['ClassID'];

        $StudentHtmlDiv .= "<input type='hidden' name='ClassLevelID' id='ClassLevelID' value='$ClassLevelID'>";
        $StudentHtmlDiv .= "<input type='hidden' name='ClassID[]' id='ClassID' value='$YearClassID'>";

        # Filters - By Report
        $ReportIDList = array();
        $ReportTypeOption = array();
        $ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);
        if(count($ReportTypeArr) > 0)
        {
            for($j=0; $j<count($ReportTypeArr); $j++)
            {
                //$period_filter = $ck_ReportCard_UserType == "STUDENT" ? ' NOW() BETWEEN MarksheetVerificationStart AND MarksheetVerificationEnd ' : '';
                $period_filter = ' NOW() BETWEEN MarksheetVerificationStart AND MarksheetVerificationEnd ';
                $ReportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportTypeArr[$j]['ReportID'], $period_filter);

                if (!empty($ReportTemplateInfo) && $ReportTemplateInfo["LastArchived"] != "0000-00-00 00:00:00" && $ReportTemplateInfo["LastArchived"] != "") {
                    $ReportIDList[] = $ReportTypeArr[$j]['ReportID'];
                    $ReportTypeOption[] = array($ReportTypeArr[$j]['ReportID'], $ReportTypeArr[$j]['SemesterTitle']);
                }
            }
        }
        if (sizeof($ReportTypeOption) > 0) {
            $ReportID = ($ReportID == "" || !in_array($ReportID, $ReportIDList)) ? $ReportTypeOption[0][0] : $ReportID;
            //$ReportTypeSelection = $lreportcard->Get_Report_Selection($ClassLevelID, $ReportID, "ReportID", "", 0, 0, 1);
        }
        else {
            $ReportID = -1;
            $ReportTypeOption[] = array(-1, $eReportCard['NoReportAvailable']);
            $ReportTypeSelectDisabled = 'disabled="disabled"';
        }
        $ReportTypeSelection = getSelectByArray($ReportTypeOption, ' id="ReportID" name="ReportID" '.$ReportTypeSelectDisabled, $ReportID, false, true);
?>

<script type="text/JavaScript" language="JavaScript">
function js_Reload()
{
    document.form1.action = 'index.php';
    document.form1.target = '';
    document.form1.submit();
}

function checkForm()
{
	return true;
}
</script>

<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT."templates/2007a/css/"?>ereportcard.css">
<br />
<form name="form1" action="../../management/archive_report_card/print_preview.php" method="POST" onsubmit="return checkForm();" target="_blank">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
<table>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td></td>
					<td align="right"><?=$linterface->GET_SYS_MSG($Result);?></td>
                </tr>
				<tr id="StudentRow">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['Student'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<div id="studentSelDiv"><?= $StudentHtmlDiv ?></div>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ReportCard'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<span id="ReportList"><?= $ReportTypeSelection ?></span>
					</td>
				</tr>
                <?php if (sizeof($ReportTypeOption) > 0) { ?>
                    <tr>
                        <td colspan="2" align="center">
                            <?= $submitBtn ?>
                        </td>
                    </tr>
                <?php } ?>
			</table>
		</td>
	</tr>
</table>
</form>
<?
		print $linterface->FOCUS_ON_LOAD("form1.ClassLevelID");
	  	$linterface->LAYOUT_STOP();
	}
	else {
?>
You have no priviledge to access this page.
<?
	}
}
else {
?>
You have no priviledge to access this page.
<?
}
?>