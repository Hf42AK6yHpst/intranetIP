<?
$PATH_WRT_ROOT = "../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	if ($lreportcard->hasAccessRight()) 
	{
		$ReportID = 6;	// F1 1st Term
		$SubjectID = 8; // Eng
		
		debug_pr($lreportcard->Get_Scheme_Grade_Statistics_Info($ReportID, $SubjectID));
		
		/*
		$SchemeGradeStatInfoArr = array();
		
		$ReportSetting = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		$ClassLevelID = $ReportSetting['ClassLevelID'];
		
		### Get Marks of the Subject
		# $MarkArr[$StudentID][$SubjectID][$ReportColumnID][Mark, Grade, etc...]
		$MarkArr = $lreportcard->getMarks($ReportID, '', '', $ParentSubjectOnly=1, $includeAdjustedMarks=1, '', $SubjectID, 0);
		
		### Get Grading Scheme Info of the Subject
		$SubjectFormGradingSettings = $lreportcard->GET_SUBJECT_FORM_GRADING($ClassLevelID, $SubjectID);
		$SchemeID = $SubjectFormGradingSettings['SchemeID'];
		$SchemeRangeInfo = $lreportcard->GET_GRADING_SCHEME_RANGE_INFO($SchemeID);
		$SchemeGradeArr = Get_Array_By_Key($SchemeRangeInfo, 'Grade');
		$numOfSchemeGrade = count($SchemeGradeArr);
		
		### Get Student List
		# Get Class List
		$YearClassInfoArr = $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID);
		$YearClassIDArr = Get_Array_By_Key($YearClassInfoArr, 'ClassID');
		$numOfClass = count($YearClassIDArr);
		
		# Get Form Student List
		$FormStudentIDArr = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID);
		$FormStudentIDArr = Get_Array_By_Key($FormStudentIDArr, 'UserID');
		$numOfStudentInForm = count($FormStudentIDArr);
		
		# Get Class Student List
		$ClassStudentIDArr = array();
		for ($i=0; $i<$numOfClass; $i++)
		{
			$thisYearClassID = $YearClassIDArr[$i];
			$thisClassStudentIDArr = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID, $ClassLevelID, $thisYearClassID);
			$ClassStudentIDArr[$thisYearClassID] = Get_Array_By_Key($thisClassStudentIDArr, 'UserID');
		}
		
		
		### Get Form Grade Range
		# initialize mark info array
		$FormSchemeMarkArr = array();
		for ($i=0; $i<$numOfSchemeGrade; $i++)
		{
			$thisSchemeGrade = $SchemeGradeArr[$i];
			$FormSchemeMarkArr[$thisSchemeGrade] = array();
		}
		
		# Get the raw mark of each student in different grades
		for ($i=0; $i<$numOfStudentInForm; $i++)
		{
			$thisStudentID 	= $FormStudentIDArr[$i];
			$thisMarkRaw 	= $MarkArr[$thisStudentID][$SubjectID][0]['Mark'];
			$thisGradeRaw 	= $MarkArr[$thisStudentID][$SubjectID][0]['Grade'];
			
			if ($thisGradeRaw=="") {
				$thisGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMarkRaw);
			} else {
				$thisGrade = $thisGradeRaw;
			}
			
			# exclude special grade
			if (in_array($thisGrade, $SchemeGradeArr))
			{
				$FormSchemeMarkArr[$thisGrade][] = $thisMarkRaw;
			}
		}
		
		# Consolidate Form Mark Info in to the array
		for ($i=0; $i<$numOfSchemeGrade; $i++)
		{
			$thisSchemeGrade = $SchemeGradeArr[$i];
			$thisSchemeMarkArr = $FormSchemeMarkArr[$thisSchemeGrade];
			
			$thisNumOfStudent = count($thisSchemeMarkArr);
			$thisPercentage = ($numOfStudentInForm > 0)? ($thisNumOfStudent / $numOfStudentInForm) * 100 : 0;
			$thisMin = ($thisNumOfStudent > 0)? min($thisSchemeMarkArr) : 0;
			$thisMax = ($thisNumOfStudent > 0)? max($thisSchemeMarkArr) : 0;
			
			$SchemeGradeStatInfoArr[$thisSchemeGrade][0]['NumOfStudent'] = $thisNumOfStudent;
			$SchemeGradeStatInfoArr[$thisSchemeGrade][0]['Percentage'] = $thisPercentage;
			$SchemeGradeStatInfoArr[$thisSchemeGrade][0]['MinMark'] = $thisMin;
			$SchemeGradeStatInfoArr[$thisSchemeGrade][0]['MaxMark'] = $thisMax;
		}
		
		
		### Get Class Grade Range
		for($i=0; $i<$numOfClass; $i++)
		{
			$thisYearClassID = $YearClassIDArr[$i];
			$thisClassStudentIDArr = $ClassStudentIDArr[$thisYearClassID];
			$thisNumOfStudentInClass = count($thisClassStudentIDArr);
			
			# initialize mark info array
			$thisClassSchemeMarkArr = array();
			for ($j=0; $j<$numOfSchemeGrade; $j++)
			{
				$thisSchemeGrade = $SchemeGradeArr[$j];
				$thisClassSchemeMarkArr[$thisSchemeGrade] = array();
			}
			
			# Get the raw mark of each student in different grades
			for ($j=0; $j<$thisNumOfStudentInClass; $j++)
			{
				$thisStudentID 	= $thisClassStudentIDArr[$j];
				$thisMarkRaw 	= $MarkArr[$thisStudentID][$SubjectID][0]['Mark'];
				$thisGradeRaw 	= $MarkArr[$thisStudentID][$SubjectID][0]['Grade'];
				
				if ($thisGradeRaw=="") {
					$thisGrade = $lreportcard->CONVERT_MARK_TO_GRADE_FROM_GRADING_SCHEME($SchemeID, $thisMarkRaw);
				} else {
					$thisGrade = $thisGradeRaw;
				}
				
				# exclude special grade
				if (in_array($thisGrade, $SchemeGradeArr))
				{
					$thisClassSchemeMarkArr[$thisGrade][] = $thisMarkRaw;
				}
			}
			
			# Consolidate Form Mark Info in to the array
			for ($j=0; $j<$numOfSchemeGrade; $j++)
			{
				$thisSchemeGrade = $SchemeGradeArr[$j];
				$thisSchemeMarkArr = $thisClassSchemeMarkArr[$thisSchemeGrade];
				
				$thisNumOfStudent = count($thisSchemeMarkArr);
				$thisPercentage = ($numOfStudentInForm > 0)? ($thisNumOfStudent / $thisNumOfStudentInClass) * 100 : 0;
				$thisMin = ($thisNumOfStudent > 0)? min($thisSchemeMarkArr) : 0;
				$thisMax = ($thisNumOfStudent > 0)? max($thisSchemeMarkArr) : 0;
				
				$SchemeGradeStatInfoArr[$thisSchemeGrade][$thisYearClassID]['NumOfStudent'] = $thisNumOfStudent;
				$SchemeGradeStatInfoArr[$thisSchemeGrade][$thisYearClassID]['Percentage'] = $thisPercentage;
				$SchemeGradeStatInfoArr[$thisSchemeGrade][$thisYearClassID]['MinMark'] = $thisMin;
				$SchemeGradeStatInfoArr[$thisSchemeGrade][$thisYearClassID]['MaxMark'] = $thisMax;
			}
		}
		*/
	}
}

intranet_closedb();

?>