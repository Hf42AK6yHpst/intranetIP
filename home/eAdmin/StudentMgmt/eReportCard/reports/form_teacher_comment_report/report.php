<?php
# using: 

@SET_TIME_LIMIT(21600);
@ini_set(memory_limit, -1);

/**************************************************
 * 	Modification log
 *  20200622 Bill:  [2020-0515-1423-31164]
 *      Add 2 preset text input settings
 * 	20171211 Bill:
 * 		Create File
 * ***********************************************/

$PageRight = "ADMIN";

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$plugin["ReportCard2008"])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

include_once($PATH_WRT_ROOT."includes/eRCConfig.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008w.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
}
else {
	$lreportcard = new libreportcard();
}

if (!$lreportcard->hasAccessRight() || !$eRCTemplateSetting['Report']['FormTeacherCommentReport']) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

# Get POST data
$ClassLevelID = $_POST["ClassLevelID"];
$StudentIDAry = $_POST["TargetStudentID"];
$TermIDAry = $_POST["TermID"];

# [2020-0515-1423-31164] Set Preset Settings
$presetDataArr = array();
$presetDataArr['yearlyTheme'] = stripslashes(urldecode($yearlyTheme));
$presetDataArr['themeVerse'] = stripslashes(urldecode($themeVerse));
$presetDataStr = base64_encode(serialize($presetDataArr));

### Store as report preset settings
$reportType = 'FormTeacherCommentReport';
$presetSetting = $lreportcard->Get_Report_Preset_Setting($reportType);
if(empty($presetSetting)) {
    $lreportcard->Insert_Report_Preset_Setting($reportType, $presetDataStr);
}
else {
    $presetID = $presetSetting[0]['PresetID'];
    $lreportcard->Update_Report_Preset_Setting($presetID, $presetDataStr);
}

# Build Term Report Info
$TermCount = 0;
$TermReportInfoAry = array();
foreach($TermIDAry as $thisTermID)
{
	$thisReportInfo = $lreportcard->returnReportTemplateBasicInfo("", "", $ClassLevelID, $thisTermID);
	$thisReportID = $thisReportInfo["ReportID"];
	$thisTermName = $lreportcard->returnSemesters($thisTermID, "en");

	$thisReportCommentAry = $lreportcard->returnSubjectTeacherCommentByBatch($thisReportID, $StudentIDAry, "0");
	$thisReportCommentAry = $thisReportCommentAry[0];
	
	$TermReportInfoAry[$TermCount]["ReportID"] = $thisReportID;
	$TermReportInfoAry[$TermCount]["TermID"] = $thisTermID;
	$TermReportInfoAry[$TermCount]["TermName"] = $thisTermName;
	$TermReportInfoAry[$TermCount]["TermClassTeacherComment"] = $thisReportCommentAry;
	
	$TermCount++;
}

# Get Basic Data
//$thisReportName = "Form Teacher's Comments";
$thisReportName = "Words of Encouragement and Comments";
$thisYearName = $lreportcard->GET_ACTIVE_YEAR("-");

# Build Student Comment Report Content
$CommentReport = "";
$studentCount = sizeof($StudentIDAry);
for($i=0; $i<$studentCount; $i++)
{
	// Student Info
	$thisStudentID   = $StudentIDAry[$i];
	$thisStudentInfo = $lreportcard->Get_Student_Class_ClassLevel_Info($thisStudentID);
	$thisEnglishName = $thisStudentInfo[0]["EnglishName"];
	$thisChineseName = $thisStudentInfo[0]["ChineseName"];
	$thisClassName   = $thisStudentInfo[0]["ClassName"];
	$thisClassNumber = $thisStudentInfo[0]["ClassNumber"];
	
	# Report Header
	$_pageBreak = (($i + 1) == $studentCount) ? "" : " style='page-break-after:always;' "; 
	$CommentReport .= "	<div id='main_container' {$_pageBreak}>
						<table width='100%' border='0' cellpadding='02' cellspacing='0' valign='top' class='stu_table'>
							<tr><td colspan='3' class='table_header'>".$thisReportName."</td></tr>
							<tr><td colspan='3' class='table_yearname'>".$thisYearName."</td></tr>
							<tr>
								<td width='68%' class='stu_info'>Student Name: ".$thisEnglishName." (".$thisChineseName.")</td>
								<td width='17%' class='stu_info'>Class: ".$thisClassName."</td>
								<td width='15%' class='stu_info'>Class No.: ".$thisClassNumber."</td>
							</tr>
							<tr><td colspan='3' class='remarks_content'>Yearly Theme: ".nl2br(stripslashes($yearlyTheme))."</td></tr>
							<tr><td colspan='3' class='remarks_content2'>Theme Verse: ".nl2br(stripslashes($themeVerse))."</td></tr>";
	
	# Report Term Comments Table
	for($j=0; $j<$TermCount; $j++)
	{
		// Term Data Info
		$thisTermReportInfo = $TermReportInfoAry[$j];
		//$thisTermName = $thisTermReportInfo["TermName"];
		$thisTeacherComment = $thisTermReportInfo["TermClassTeacherComment"][$thisStudentID]["Comment"];
		$thisTeacherComment = $thisTeacherComment=="" ? "&nbsp;" : stripslashes(nl2br($thisTeacherComment));

		$thisSectionName = '';
		if ($j == 0) {
            $thisSectionName = 'Principal\'s Message:';
        }
        else if ($j == 1) {
            $thisSectionName = 'Form Coordinator\'s Encouragement:';
        }
        else if ($j == 2) {
            $thisSectionName = 'Form Teacher\'s Comments:';
        }
		
		// 20180625 - email "[eClass eReportCard] Adjusted the box height of Form Teacher's Comments"
		$_tableHeight = ($j == 2) ? 210 : 158;
		$_style = ' style="height:'.$_tableHeight.'px;" ';
		
		# Comment Table
		$CommentReport .= "	<tr><td colspan='3'>
								<table width='100%' border='0' cellpadding='02' cellspacing='0' valign='top' class='comment_table' ".$_style.">
									<!--<tr><td colspan='3' class='comment_title'>".$thisReportName." (".$thisTermName.")</td></tr>-->
									<tr><td colspan='3' class='comment_title'>".$thisSectionName."</td></tr>
									<tr>
										<td width='4.5%'>&nbsp;</td>
										<td width='90.5%' class='stu_comment'>".$thisTeacherComment."</td>
										<td width='5%'>&nbsp;</td>
									</tr>
								</table>
							</td></tr>";
	}
	$CommentReport .= "</table></div>\n";
}
?>

<style type="text/css">
@charset "utf-8";

/* CSS Document */
div#main_container { margin: 0 auto; width: 712px; }

table td { font-size: 16px; font-family: "Times New Roman", "Arial", "Helvetica", "sans-serif", "細明體_HKSCS", "新細明體"; }
table td.table_header { font-size: 21px; font-weight: bold; font-family: "Arial", "Helvetica", "sans-serif", "細明體_HKSCS"; padding-bottom: 3px; }
table td.table_yearname { padding-top: 2px; padding-bottom:1px; }
table td.table_header, table td.table_yearname { text-align: center }
table td.table_header, table td.table_yearname, table td.stu_info { line-height: 38px; }
table td.remarks_content { padding-top: 11px; padding-bottom: 11px; }
table td.remarks_content2 { padding-top: 0px; padding-bottom: 2px; }

table.stu_table { padding-top: 126px; }

table.comment_table { padding-top: 16px; padding-bottom: 14px; }
table.comment_table td.comment_title { padding-top: 5px; padding-bottom: 12px; padding-left: 0px; }
table.comment_table td.stu_comment { padding: 3px; border: 1px solid #D3D3D3; vertical-align: top; line-height: 22px; height: 175px; }
</style>

<?=$CommentReport?>