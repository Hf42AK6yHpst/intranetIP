<?php

/********************************************************
 * 	modification log
 *  20180628 Bill:  [2017-1204-1601-38164]
 *      set target active year  ($eRCTemplateSetting['Report']['SupportAllActiveYear'])
 *  
 *  20111102 Connie:
 * 	modified 
 * 
 * 	20110613 Marcus:
 * 		modified Class Selection, use Get_Class_Selection in libreportcard_ui instead of form_class_manage_ui.php
 *
 * 	20100611 Marcus:
 * 		add $IncludeAssessmentLib to include assessment lib instead ,add loadStudentList
 *
 * 	20100301 Marcus:
 * 		add param $isAll, $onchange to $RecordType == "Class"
 * *******************************************************/
 
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();

if($IncludeAssessmentLib)
{
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		if (is_file($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName."_assessment.php"))
			include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName."_assessment.php");
		else
			include_once($PATH_WRT_ROOT."includes/reportcard_custom/general_assessment.php");
		
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
}
else
{
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
}

if($eRCTemplateSetting['Report']['SupportAllActiveYear'])
{
    $targetActiveYear = stripslashes($_REQUEST['ActiveYear']);
    if(!empty($targetActiveYear)){
        $targetYearID = $lreportcard->SET_TEMP_ACTIVE_YEAR($targetActiveYear);
    }
}

$RecordType = stripslashes($_REQUEST['RecordType']);

$returnString = '';
if ($RecordType == "Report")
{
	$YearID = stripslashes($_REQUEST['YearID']);
	$ReportID = stripslashes($_REQUEST['ReportID']);
	$SelectionID = stripslashes($_REQUEST['SelectionID']);
	$onChange = stripslashes($_REQUEST['onChange']);
	$ForVerification = stripslashes($_REQUEST['ForVerification']);
	$ForSubmission = stripslashes($_REQUEST['ForSubmission']);
	$HideNonGenerated = stripslashes($_REQUEST['HideNonGenerated']);
	$OtherTags = stripslashes($_REQUEST['OtherTags']);
	$ForGradingSchemeSettings = stripslashes($_REQUEST['ForGradingSchemeSettings']);
	$excludeReportID = stripslashes($_REQUEST['excludeReportID']);
	$ReportType = trim(stripslashes($_REQUEST['ReportType']));
	
	$returnString = $lreportcard->Get_Report_Selection($YearID, $ReportID, $SelectionID,$onChange,$ForVerification, $ForSubmission, $HideNonGenerated, $OtherTags, $ForGradingSchemeSettings, $excludeReportID, $ReportType);
}
else if ($RecordType == "Subject")
{
	$YearID = stripslashes($_REQUEST['YearID']);
	$SelectionID = stripslashes($_REQUEST['SelectionID']);
	$IncludeComponent = stripslashes($_REQUEST['IncludeComponent']);
	$InputMarkOnly = stripslashes($_REQUEST['InputMarkOnly']);
	$onChange = stripslashes($_REQUEST['onChange']);
	$includeGrandMarks = stripslashes($_REQUEST['includeGrandMarks']);
	$OtherTagInfo = stripslashes($_REQUEST['OtherTagInfo']);
	$ReportID = stripslashes($_REQUEST['ReportID']);
	$isMultiple = trim($_REQUEST['isMultiple'])==''?1:stripslashes($_REQUEST['isMultiple']);
	$TeachingOnly = stripslashes($_REQUEST['TeachingOnly']);
	$ParentSubjectAsOptGroup = stripslashes($_REQUEST['ParentSubjectAsOptGroup']);
	$ExceptWithoutSubjectGroup = stripslashes($_REQUEST['ExceptWithoutSubjectGroup']);
	
	$returnString = $lreportcard->Get_Subject_Selection($YearID, '', $SelectionID, $onChange, $isMultiple, $IncludeComponent, $InputMarkOnly, $includeGrandMarks, $OtherTagInfo, $ReportID, $ParentSubjectAsOptGroup, $ExceptWithoutSubjectGroup, $TeachingOnly);
}
//else if ($RecordType == "Class")
//{
//	$YearID = stripslashes($_REQUEST['YearID']);
//	$SelectionID = stripslashes($_REQUEST['SelectionID']);
//	$isMultiple = stripslashes($_REQUEST['isMultiple']);
//	$isAll = stripslashes($_REQUEST['isAll']);
//	$noFirst = $isAll?0:1;
//	$onchange = stripslashes($_REQUEST['onchange']);
//	$TeachingOnly = stripslashes($_REQUEST['TeachingOnly']);
//	
//	include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
//	$libFCP_ui = new form_class_manage_ui();
//	$returnString = $libFCP_ui->Get_Class_Selection($lreportcard->schoolYearID, $YearID, $SelectionID, '', $onchange, $noFirst, $isMultiple,$isAll, $TeachingOnly);
//} 
else if ($RecordType == "Class")
{
	$YearID = stripslashes($_REQUEST['YearID']);
	$SelectionID = stripslashes($_REQUEST['SelectionID']);
	$isMultiple = stripslashes($_REQUEST['isMultiple']);
	$isAll = stripslashes($_REQUEST['isAll']);
	$noFirst = $isAll?0:1;
	$onchange = stripslashes($_REQUEST['onchange']);
	$checkPermission = stripslashes($_REQUEST['TeachingOnly']);
	$WithSelectAllButton = stripslashes($_REQUEST['WithSelectAllButton']);
	$AcademicYearID = stripslashes($_REQUEST['AcademicYearID']);
	if($eRCTemplateSetting['Report']['SupportAllActiveYear'] && !empty($targetActiveYear) && $targetYearID){
	    $AcademicYearID = $targetYearID;
	}
	
//	include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
//	$libFCP_ui = new form_class_manage_ui();
//	$returnString = $libFCP_ui->Get_Class_Selection($lreportcard->schoolYearID, $YearID, $SelectionID, '', $onchange, $noFirst, $isMultiple,$isAll, $TeachingOnly);
	
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
	$lreportcard_ui = new libreportcard_ui($AcademicYearID);
	$classSel = $lreportcard_ui->Get_Class_Selection($SelectionID, $YearID, '', $onchange, $noFirst, $isAll, $checkPermission, $isMultiple);
	
	if ($WithSelectAllButton) {
		$selectAllBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('".$SelectionID."');");
		$returnString = $linterface->Get_MultipleSelection_And_SelectAll_Div($classSel, $selectAllBtn, $SpanID='');
	}
	else {
		$returnString = $classSel;
	}
} 
else if($RecordType == "ReportColumnCheckbox")
{
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	$linterface = new interface_html();
	
	$ReportID = stripslashes($_REQUEST['ReportID']);
	$ReportTitle = $lreportcard->Get_Report_Title($ReportID);
	$ReportTitle = str_replace(":_:"," ",$ReportTitle);
	$ColumnDataAry = $lreportcard->returnReportColoumnTitle($ReportID);
	$ReportSetting = $lreportcard->returnReportTemplateBasicInfo($ReportID);
	$isTermReport = $ReportSetting["Semester"] == "F"?0:1;
	$ReportFunction = stripslashes($_REQUEST['ReportFunction']);
	
	/*
	$i=0;
	foreach((array)$ColumnDataAry as $ColumnID => $ColumnTitle)
	{
		$returnString .= '<input type="checkbox" id="ReportColumn'.$i.'" name="ReportColumnID[]" value="'.$ColumnID.'" class="ReportColumnChk" CHECKED>&nbsp;';
		$returnString .= '<label for="ReportColumn'.$i.'">'.$ColumnTitle.'</label><br>'."\n";
		$i++;
	}
	$returnString .= '<input type="checkbox" id="ReportColumn'.$i.'" name="ReportColumnID[]" value="0" class="ReportColumnChk" CHECKED>&nbsp;';
	$returnString .= '<label for="ReportColumn'.$i.'">'.$eReportCard["Overall"].'</label><br>'."\n";
	*/
	
	$returnString = '';
	
	if($ReportFunction=='subject_academic_result')
	{
		$returnString .= $linterface->Get_Checkbox('ReportColumn_All', 'ReportColumn_All', '', $isChecked=1, $Class='', $Lang['Btn']['SelectAll'], "jsSelectAllCheckbox(this.checked, 'ReportColumnChk')", $Disabled='');
	}
	else if($ReportFunction=='mark_analysis')
	{
		$returnString .= $linterface->Get_Checkbox('ReportColumn_All', 'ReportColumn_All', '', $isChecked=1, $Class='', $Lang['Btn']['SelectAll'], "Check_All_Options_By_Class('ReportColumnChk', this.checked);", $Disabled='');
	}
	else
	{
		$returnString .= $linterface->Get_Checkbox('ReportColumn_All', 'ReportColumn_All', '', $isChecked=0, $Class='', $Lang['Btn']['SelectAll'], "jsSelectAllCheckbox(this.checked, 'ReportColumnChk')", $Disabled='');
	}
	
	$returnString .= '<br>'."\n";
	$i = 0;
	foreach((array)$ColumnDataAry as $ColumnID => $ColumnTitle)
	{
		$returnString .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		
		if($ReportFunction=='subject_academic_result')
		{
			$returnString .= $linterface->Get_Checkbox('ReportColumn'.$ColumnID, 'ReportColumnID[]', $ColumnID, $isChecked=1, 'ReportColumnChk', $ColumnTitle, "jsCheckUnSelectAll(this.checked, 'ReportColumn_All'); jsShowHideColumnLabelOption();", $Disabled='');
		}
		else if($ReportFunction=='mark_analysis')
		{
			$returnString .= $linterface->Get_Checkbox('ReportColumn_'.$ColumnID, 'ReportColumnIDAry[]', $ColumnID, $isChecked=1, 'ReportColumnChk', $ColumnTitle, "Uncheck_SelectAll('ReportColumn_All', this.checked);", $Disabled='');
		}
		else
		{
			$returnString .= $linterface->Get_Checkbox('ReportColumn'.$ColumnID, 'ReportColumnID[]', $ColumnID, $isChecked=0, 'ReportColumnChk', $ColumnTitle, "jsCheckUnSelectAll(this.checked, 'ReportColumn_All'); jsShowHideColumnLabelOption();", $Disabled='');
		}
		
		$returnString .= '<br>'."\n";
		$i++;
	}
	$returnString .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	if ($ReportFunction=='mark_analysis'){
		$returnString .= $linterface->Get_Checkbox('ReportColumn_'.$i, 'ReportColumnIDAry[]', '0', $isChecked=1, 'ReportColumnChk', $eReportCard["Overall"], "Uncheck_SelectAll('ReportColumn_All', this.checked);", $Disabled='');
	}
	else {
		$returnString .= $linterface->Get_Checkbox('ReportColumn'.$i, 'ReportColumnID[]', '0', $isChecked=1, 'ReportColumnChk', $eReportCard["Overall"], "jsCheckUnSelectAll(this.checked, 'ReportColumn_All'); jsShowHideColumnLabelOption();", $Disabled='');
	}
	$returnString .= '<br>'."\n";
	
	$returnString =  $ReportTitle.":_delimiter_:".$isTermReport.":_delimiter_:".$returnString;
	
}
else if($RecordType=="loadStudentList")
{
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	$linterface = new interface_html();
	
	$ClassIDArr = explode(",",$ClassIDStr);
	$StudentList = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ReportID,$ClassLevelID);
	
	
	if($ReportCardCustomSchoolName=="kiansu_chekiang_college") {
		$StudentIDAry = Get_Array_By_Key($StudentList,"UserID");
		//$StudentRankScoreAry = $lreportcard->Get_Assessment_Report_Rank_Arr($ReportID,$ClassLevelID,$DataSource,$StudentIDAry);
	}
	
	foreach((array)$StudentList as $Student) {
		if($StudentRankScoreAry[$Student["UserID"]]["isAllNA"]===true)
			continue;
		if(!in_array($Student["YearClassID"],$ClassIDArr))
			continue;
		
		$thisStudentName = $Student["StudentName"];
		$thisClassName = Get_Lang_Selection($Student["ClassTitleCh"], $Student["ClassTitleEn"]);	
		$thisClassNumber = $Student["ClassNumber"];
		$thisDisplay = $thisStudentName.' ('.$thisClassName.'-'.$thisClassNumber.')';
		
		$SelectArr[] = array($Student["UserID"], $thisDisplay);
	}
	
	$selection = getSelectByArray($SelectArr, "id='StudentID' name='StudentIDAry[]' multiple size=10", $selected="", $all=0, $noFirst=1, $FirstTitle="", $ParQuoteValue=1);
	if(!$noSelectAllBtn)
		$selectall = $linterface->GET_SMALL_BTN($button_select_all, "button", "SelectAll('StudentID');", "selectAllBtn02");
	$returnString = $selection.$selectall;	
}
else if($RecordType=="SubjectGroup")
{
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
	$linterface = new interface_html();
	
	if($ReportID)
	{
		$ReportSetting = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		$SemID = $ReportSetting['Semester'];
		$ClassLevelID = $ReportSetting['ClassLevelID'];
	}
	else
	{
		$SemID = stripslashes($_REQUEST['SemID']);
		$SemID =  explode(",",$SemID);
		$ClassLevelID = $_REQUEST['ClassLevelID'];
	}
	$SelectionID = stripslashes($_REQUEST['SelectionID']);
	$isMultiple = trim($_REQUEST['isMultiple'])?" multiple size=10 ":"";
	$Selected = $Selected;
	$isAll = stripslashes($_REQUEST['isAll']);
	$noFirst = stripslashes($_REQUEST['noFirst']);
	$FirstTitle = stripslashes($_REQUEST['FirstTitle']);
	$TeachingID = stripslashes($_REQUEST['TeachingOnly'])?$_SESSION['UserID']:'';
	$SubjectID = stripslashes($_REQUEST['SubjectID']);
	
	
	$SubjectIDArr = explode(",",$SubjectID);
	
	
	$NameLang = Get_Lang_Selection("ClassTitleB5","ClassTitleEN");
	$SelectArr = array();
	foreach((array)$SubjectIDArr as $SubjectID)
	{
		$obj_Subject = new Subject($SubjectID);
		$AllSubjectGroupArr = $obj_Subject->Get_Subject_Group_List($SemID, $ClassLevelID,'',$TeachingID);
		
		foreach((array)$AllSubjectGroupArr as $SubjGroup)
		{
			$SelectArr[$SubjGroup["SubjectGroupID"]] = $SubjGroup[$NameLang];
		}	
		
	}
	$thisTag = 'id="'.$SelectionID.'"  name="'.$SelectionID.'" '.$isMultiple.'  onchange="js_Reload_Student_Selection(\'StudentBySubjectGroup\')"';
	$selection = getSelectByAssoArray($SelectArr, $thisTag, $Selected, $isAll, $noFirst, $FirstTitle);
	$returnString = $selection;

}
else if($RecordType=="StudentByClass")
{
	$SelectionID = stripslashes($_REQUEST['SelectionID']);
	$Onchange = stripslashes($_REQUEST['Onchange']);
	$noFirst = stripslashes($_REQUEST['noFirst']);
	$isMultiple = stripslashes($_REQUEST['isMultiple']);
	$isAll = stripslashes($_REQUEST['isAll']);
	$withSelectAll = stripslashes($_REQUEST['withSelectAll']);
	$ExcludeStudentIDList = stripslashes($_REQUEST['ExcludeStudentIDList']);
	$AcademicYearID = stripslashes($_REQUEST['AcademicYearID']);
	if($eRCTemplateSetting['Report']['SupportAllActiveYear'] && !empty($targetActiveYear) && $targetYearID){
	    $AcademicYearID = $targetYearID;
	}
	
	if ($AcademicYearID != '') {
		$lreportcard = new libreportcard($AcademicYearID);
	}
	
	$YearClassIDArr = (array)explode(",",$ClassID);

	
	if ($ExcludeStudentIDList == '') {
		$ExcludeStudentIDArr = '';
	}
	else {
		$ExcludeStudentIDArr = (array)explode(",",$ExcludeStudentIDList);
	}
	
	$StudentSelection = $lreportcard->Get_Student_Selection_By_Class($SelectionID, $YearClassIDArr, $SelectedStudentID, $Onchange, $noFirst, $isMultiple, $isAll, $ExcludeStudentIDArr);
	
	
	if ($withSelectAll) {
		include_once($PATH_WRT_ROOT."includes/libinterface.php");
		$linterface = new interface_html();
		
		$SelectAllBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('".$SelectionID."', 1)");
		$returnString = $linterface->Get_MultipleSelection_And_SelectAll_Div($StudentSelection, $SelectAllBtn, $SpanID='');
	}
	else {
		$returnString = $StudentSelection;
	}
}
else if($RecordType=="StudentBySubjectGroup")
{
	$SelectionID = stripslashes($_REQUEST['SelectionID']);
	$Onchange = stripslashes($_REQUEST['Onchange']);
	$noFirst = stripslashes($_REQUEST['noFirst']);
	$isMultiple = trim($_REQUEST['isMultiple'])?" multiple='true' size=10 ":"";
	$isAll = stripslashes($_REQUEST['isAll']);
	
	$SubjectGroupIDArr = (array)explode(",",$SubjectGroupID);
	
	$StudentList = $lreportcard->GET_STUDENT_BY_SUBJECT_GROUP($SubjectGroupIDArr, '', '', 0, 0, 0);
	
	$SelectArr=array();
	foreach((array)$StudentList as $Student)
	{
		$thisClassName = Get_Lang_Selection($Student['ClassTitleCh'],$Student['ClassTitleEn']);
		$thisClassNumber = $Student['ClassNumber'];
		$SelectArr[$Student["UserID"]] = $Student['StudentName'].' ('.$thisClassName.'-'.$thisClassNumber.')';

	}
	$returnString = getSelectByAssoArray($SelectArr, "id='$SelectionID' name='$SelectionID' $isMultiple ", $Selected, $isAll, $noFirst, $FirstTitle);
		
}

else if($RecordType=="TermByReport")
{
	$SelectionID = $_POST['SelectionID'];
	$ReportID = $_POST['ReportID'];
	
	$SelectedYearTermIDArr = $lreportcard->returnReportInvolvedSem($ReportID);

	$SelectedYearTermID = key ($SelectedYearTermIDArr);
	$SelectedYearTermVal = $SelectedYearTermIDArr[$SelectedYearTermID];

	$onchange = 'onchange="js_Reload_Subject_Group_Selection();"';
	$multiple = 'multiple';
	$selectionTags = ' id="'.$SelectionID.'" name="'.$SelectionID.'" '.$onchange.' '.$multiple;
	$returnString = getSelectByAssoArray($SelectedYearTermIDArr, $selectionTags, $SelectedYearTermID, $all=0, $NoFirst=1, $firstTitle); 
}
else if($RecordType=="Award")
{
	$ReportID = stripslashes($_POST['ReportID']);
	$SelectionID = stripslashes($_POST['SelectionID']);
	$isMultiple = stripslashes($_POST['isMultiple']);
	$withSelectAll = stripslashes($_POST['withSelectAll']);
		
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
	$lreportcard_ui = new libreportcard_ui();
	if($eRCTemplateSetting['Report']['SupportAllActiveYear'] && !empty($targetActiveYear) && $targetYearID){
	    $lreportcard_ui = new libreportcard_ui($targetYearID);
	}
	
	$AwardSelection = $lreportcard_ui->Get_Award_Selection($ReportID, $SelectionID, $SelectedValue='', $ShowHaveRankingAwardOnly=0, $ExcludeAwardIDArr='', $TargetAwardType='', $OnChange='', $WithAwardType=1, $isMultiple);
	
	if ($withSelectAll) {
		include_once($PATH_WRT_ROOT."includes/libinterface.php");
		$linterface = new interface_html();
		
		$SelectAllBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('".$SelectionID."', 1)");
		$returnString = $linterface->Get_MultipleSelection_And_SelectAll_Div($AwardSelection, $SelectAllBtn, $SpanID='');
	}
	else {
		$returnString = $StudentSelection;
	}
}
else if ($RecordType == 'ClassHistoryClass')
{
	$SelectionID = $_REQUEST['SelectionID'];
	$AcademicYear = $_REQUEST['AcademicYear'];
	$Selected = $_REQUEST['Selected'];
	$Onchange = stripslashes($_REQUEST['Onchange']);
	$NoFirst = $_REQUEST['NoFirst'];
	$IsAll = $_REQUEST['IsAll'];
	$FirstTitle = $_REQUEST['FirstTitle']; 
	
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
	$lreportcard_ui = new libreportcard_ui();
	if($eRCTemplateSetting['Report']['SupportAllActiveYear'] && !empty($targetActiveYear) && $targetYearID){
	    $lreportcard_ui = new libreportcard_ui($targetYearID);
	}
	echo $lreportcard_ui->Get_Class_History_Class_Selection($SelectionID, $AcademicYear, $YearID, $Onchange, $NoFirst, $IsAll, $FirstTile);
}
else if ($RecordType == 'ClassHistoryStudent')
{
	$SelectionID = $_REQUEST['SelectionID'];
	$AcademicYear = $_REQUEST['AcademicYear'];
	$ClassName = $_REQUEST['ClassName'];
	$Selected = $_REQUEST['Selected'];
	$Onchange = stripslashes($_REQUEST['Onchange']);
	$NoFirst = $_REQUEST['NoFirst'];
	$IsAll = $_REQUEST['IsAll'];
	$FirstTitle = $_REQUEST['FirstTitle']; 
	
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
	$lreportcard_ui = new libreportcard_ui();
	if($eRCTemplateSetting['Report']['SupportAllActiveYear'] && !empty($targetActiveYear) && $targetYearID){
	    $lreportcard_ui = new libreportcard_ui($targetYearID);
	}
	echo $lreportcard_ui->Get_Class_History_Student_Selection($SelectionID, $AcademicYear, $ClassName, $YearID, $Onchange, $NoFirst, $IsAll, $FirstTile);
}
else if ($RecordType == 'ArchiveReport')
{
	$SelectionID = $_REQUEST['SelectionID'];
	$AcademicYear = $_REQUEST['AcademicYear'];
	$StudentID = $_REQUEST['StudentID'];
	$Selected = $_REQUEST['Selected'];
	$Onchange = stripslashes($_REQUEST['Onchange']);
	$NoFirst = $_REQUEST['NoFirst'];
	$IsAll = $_REQUEST['IsAll'];
	$FirstTitle = $_REQUEST['FirstTitle']; 
	
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
	$lreportcard_ui = new libreportcard_ui();
	if($eRCTemplateSetting['Report']['SupportAllActiveYear'] && !empty($targetActiveYear) && $targetYearID){
	    $lreportcard_ui = new libreportcard_ui($targetYearID);
	}
	echo $lreportcard_ui->Get_Archive_Report_Selection($SelectionID, $StudentID, $AcademicYear, $YearID, $Onchange, $NoFirst, $IsAll, $FirstTile);
}
else if ($RecordType == 'ReportColumnSelection') {
	$SelectionID = trim(stripslashes($_POST['SelectionID']));
	$ReportID = trim(stripslashes($_POST['ReportID']));
	$IncludeOverallOption = trim(stripslashes($_POST['IncludeOverallOption']));
	$OverallOptionValue = trim(stripslashes($_POST['OverallOptionValue']));
	$IsAll = trim(stripslashes($_POST['IsAll']));
	$NoFirst = trim(stripslashes($_POST['NoFirst']));
	$FirstTitle = trim(stripslashes($_POST['FirstTitle']));
	
	if ($IncludeOverallOption == '') {
		$IncludeOverallOption = true;
	}
	
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
	$lreportcard_ui = new libreportcard_ui();
	if($eRCTemplateSetting['Report']['SupportAllActiveYear'] && !empty($targetActiveYear) && $targetYearID){
	    $lreportcard_ui = new libreportcard_ui($targetYearID);
	}
	echo $lreportcard_ui->Get_ReportColumn_Selection($ReportID, $SelectionID, $SelectedValue='', $IncludeOverallOption, $IsAll, $NoFirst, $FirstTitle, $OverallOptionValue);
}
else if ($RecordType == 'ClassLevelSelection') {
    include_once ($PATH_WRT_ROOT."includes/form_class_manage.php");
    $libForm = new Year();
    $FormArr = $libForm->Get_All_Year_List();
    $ClassLevelID = $FormArr[0]['YearID'];
    
    include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
    $lreportcard_ui = new libreportcard_ui();
    if($eRCTemplateSetting['Report']['SupportAllActiveYear'] && !empty($targetActiveYear) && $targetYearID){
        $lreportcard_ui = new libreportcard_ui($targetYearID);
    }
    
    $checkPermission = ($lreportcard_ui->IS_ADMIN_USER_VIEW($_SESSION['UserID']) || $lreportcard_ui->IS_VIEW_GROUP_USER_VIEW($_SESSION['UserID'])) ? 0 : 3;
    echo $lreportcard_ui->Get_Form_Selection('ClassLevelID', $ClassLevelID, 'js_Reload_Selection(this.value)', $noFirst = 1, $isAll = 0, $hasTemplateOnly = 1, $checkPermission);
}
else if ($RecordType == 'YearTermSelection') {
    include_once($PATH_WRT_ROOT."includes/libinterface.php");
    $linterface = new interface_html();
    $SelectAllTermBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All_Term()");
    
    include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
    $lreportcard_ui = new libreportcard_ui();
    if($eRCTemplateSetting['Report']['SupportAllActiveYear'] && !empty($targetActiveYear) && $targetYearID){
        $lreportcard_ui = new libreportcard_ui($targetYearID);
    }
    $TermSelection = $lreportcard_ui->Get_Term_Selection("TermID", $lreportcard->schoolYearID, '', 'js_Reload_Subject_Group_Selection()', 1, 0, 1);
    
    echo $TermSelection.$SelectAllTermBtn;
}

echo $returnString;
?>