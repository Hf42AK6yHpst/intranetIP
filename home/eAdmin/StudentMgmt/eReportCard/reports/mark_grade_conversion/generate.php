<?
//modifying by Kit
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$ViewFormat = $_POST['ViewFormat'];
$YearID = $_POST['YearID'];
$ReportID = $_POST['ReportID'];
$SubjectIDArr = $_POST['SubjectIDArr'];


if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	if ($lreportcard->hasAccessRight()) 
	{
		### Form Name
		$objYear = new Year($YearID);
		$FormName = $objYear->YearName;
		
		
		### Subject Mark-Position Info
		$SubjectMarkPositionArr = array();
		$numOfSubject = count($SubjectIDArr);
		for ($i=0; $i<$numOfSubject; $i++)
		{
			### Get subject name
			$thisSubjectID = $SubjectIDArr[$i];
			
			$GradeArr = $lreportcard->Get_Scheme_Grade_Statistics_Info($ReportID, $thisSubjectID);
			//debug_r($GradeArr);
			foreach($GradeArr as $SubjectGrade => $SubjectGradeArr)
			{
        $MaxArr = array();
        $MinArr = array();
        $Max = "";
        $Min = "";
        foreach($SubjectGradeArr as $YearClassID => $GradeStatInfo){
          $tmpMax = $GradeStatInfo['MaxMark'];
          $tmpMin = $GradeStatInfo['MinMark'];
          
          if(trim($tmpMax)) $MaxArr[] = $tmpMax;
          if(trim($tmpMin)) $MinArr[] = $tmpMin;   
        }

        if(count($MaxArr)>0) $Max = max($MaxArr);
        if(count($MinArr)>0) $Min = min($MinArr);
			  $ListMarkGrade[$thisSubjectID][$SubjectGrade] = array($Max, $Min);
      }
      //debug_r($ListMarkGrade);
		}
		
		## Display the result
		if ($ViewFormat == 'html')
		{
			$table = '';
			$count = 1;
			$NumOfTable = count($ListMarkGrade);
			$TableArr = array();
			foreach ((array)$ListMarkGrade as $thisSubjectID => $SubjectGradeArr)
			{
				$thisSubjectName = $lreportcard->GET_SUBJECT_NAME_LANG($thisSubjectID);
				
				//$style = ($count%3 == 0 && $count!=$NumOfTable)? "style='page-break-after:always'" : "";
				$TableArr[$thisSubjectID] = '';
				$TableArr[$thisSubjectID] .= "<table id='ResultTable' class='GrandMSContentTable' width='100%' border='1' cellpadding='5' cellspacing='0' $style>\n";
					$TableArr[$thisSubjectID] .= "<thead>";
						$TableArr[$thisSubjectID] .= "<tr>";
							$TableArr[$thisSubjectID] .= "<td colspan='5'>";
								$TableArr[$thisSubjectID] .= "<div width='100%'>".$eReportCard["Subject"].": ".$thisSubjectName."</div>";
								$TableArr[$thisSubjectID] .= "<div width='100%'>".$eReportCard["FormName"].": ".$FormName."</div>";
							$TableArr[$thisSubjectID] .= "</td>";
						$TableArr[$thisSubjectID] .= "</tr>";
						$TableArr[$thisSubjectID] .= "<tr>";
							$TableArr[$thisSubjectID] .= "<td width='50%'>".$eReportCard['Mark']."</td>";
							$TableArr[$thisSubjectID] .= "<td width='50%'>".$eReportCard['Grade']."</td>";
						$TableArr[$thisSubjectID] .= "</tr>";
					$TableArr[$thisSubjectID] .= "</thead>";
					
					$TableArr[$thisSubjectID] .= "<tbody>\n";
					
						ksort($SubjectGradeArr);
						foreach ($SubjectGradeArr as $SubjectGrade => $value)
						{
							$MaxVal = $value[0];
							$MinVal = $value[1];
							
							if($MaxVal == "" && $MinVal == "")
								$MarkDisplay = "-";
							else if($MaxVal == $MinVal)
								$MarkDisplay = $MaxVal;
							else 
								$MarkDisplay = $MaxVal."-".$MinVal;
							
							$TableArr[$thisSubjectID] .= "<tr>\n";
								$TableArr[$thisSubjectID] .= "<td>".$MarkDisplay."</td>\n";
								$TableArr[$thisSubjectID] .= "<td>".$SubjectGrade."</td>\n";
							$TableArr[$thisSubjectID] .= "</tr>\n";
						}
					$TableArr[$thisSubjectID] .= "</tbody>\n";
				$TableArr[$thisSubjectID] .= "</table>\n";
				$TableArr[$thisSubjectID] .= "<br style='clear:both' />\n";
				$count++;
			}
			
			
			$css = $lreportcard->Get_GrandMS_CSS();
			
			# add temp table so that the last table will not be widden
			$numEmptyTable = 8 - ($numOfSubject % 8);
			for ($i=0; $i<$numEmptyTable; $i++)
				$TableArr['temp'.$i] = '&nbsp;';
			$numOfSubject = count($TableArr);
			
			$allTable = '';
			$subjectCounter = 0;
			foreach ($TableArr as $thisSubjectID => $thisTableHTML)
			{
				# last table => no page-break
				if ($numOfSubject - $subjectCounter <= 8)
					$style = '';
				else
					$style = 'style="page-break-after:always"';
				
				# Start new page table
				if ($subjectCounter % 8 == 0)
					$allTable .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0" align="center" '.$style.'>';
					
				# Start new row
				if ($subjectCounter % 2 == 0)
					$allTable .= '<tr>';
					
					$allTable .= '<td valign="top" style="width:45%">'.$thisTableHTML.'</td>';
					
				# middle Empty row
				if ($subjectCounter % 2 == 0)
					$allTable .= '<td valign="top" style="width:5%">&nbsp;</td>';
				
				# End row (last subject => end row also)
				if ($subjectCounter % 2 == 1 || ($subjectCounter == $numOfSubject - 1))
					$allTable .= '</tr>';
					
				# End table (last subject => end table also)
				if ($subjectCounter % 8 == 7 || ($subjectCounter == $numOfSubject - 1))
					$allTable .= '</table>';
					
				$subjectCounter++;
			}
			
			echo $lreportcard->Get_Report_Header($ReportTitle);
			echo $allTable.$css;
			echo $lreportcard->Get_Report_Footer();
			
			intranet_closedb();
		}
		else if ($ViewFormat == 'csv')
		{
			include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
			include_once($PATH_WRT_ROOT."includes/libexporttext.php");
			
			$ExportHeaderArr = array();
			$ExportContentArr = array();
			$lexport = new libexporttext();
			
			# Header
			$ExportHeaderArr[] = $eReportCard['Subject'];
			$ExportHeaderArr[] = $eReportCard['FormName'];
			$ExportHeaderArr[] = $eReportCard['Mark'];
			$ExportHeaderArr[] = $eReportCard['Grade'];
			
			# Content
			$i_counter = 0;
			foreach ($ListMarkGrade as $thisSubjectID => $SubjectGradeArr)
			{
				$thisSubjectName = $lreportcard->GET_SUBJECT_NAME_LANG($thisSubjectID);
				
				ksort($SubjectGradeArr);
				foreach ($SubjectGradeArr as $SubjectGrade => $value)
				{
					$MaxVal = $value[0];
					$MinVal = $value[1];
					
					if($MaxVal == "" && $MinVal == "")
            $MarkDisplay = "-";
					else if($MaxVal == $MinVal)
            $MarkDisplay = $MaxVal;
          else $MarkDisplay = $MaxVal."-".$MinVal;
					
					$j_counter = 0;
					$thisPosition = $thisMarkInfoArr['PositionDisplay'];
					
					$ExportContentArr[$i_counter][$j_counter++] = $FormName;
					$ExportContentArr[$i_counter][$j_counter++] = $thisSubjectName;
					$ExportContentArr[$i_counter][$j_counter++] = $MarkDisplay;
					$ExportContentArr[$i_counter][$j_counter++] = $SubjectGrade;
					
					$i_counter++;
				}
			}			
			
			// Title of the grandmarksheet
			$filename = 'Mark_Grade_Conversion_of_'.$FormName;
			$filename = str_replace(" ", "_", $filename);
			$filename = $filename.".csv";
			
			$export_content = $lexport->GET_EXPORT_TXT($ExportContentArr, $ExportHeaderArr);
			intranet_closedb();
			
			// Output the file to user browser
			$lexport->EXPORT_FILE($filename, $export_content);
		}
	}
}


?>