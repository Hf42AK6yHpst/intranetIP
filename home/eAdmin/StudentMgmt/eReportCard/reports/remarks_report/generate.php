<?
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$ViewFormat = $_POST['ViewFormat'];
$YearID = $_POST['YearID'];
$YearClassIDArr = $_POST['YearClassIDArr'];
$ReportID = $_POST['ReportID'];
$FontSize = $_POST['FontSize'];

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	if ($lreportcard->hasAccessRight()) 
	{
		# Report Title
		$ObjAcademicYear = new academic_year($lreportcard->schoolYearID);
		$ActiveYear = $ObjAcademicYear->Get_Academic_Year_Name();
		
		$ReportCardInfoArr = $lreportcard->returnReportTemplateBasicInfo($ReportID);
		$ReportCardTitle = str_replace(':_:', ' ', $ReportCardInfoArr['ReportTitle']);
		
		$ReportType = $eReportCard['RemarksReport']['ReportTitle'];
		$ReportTitle = strtoupper($eReportCard['Template']['SchoolNameEn'])." ".$ReportCardTitle." ".$ActiveYear." ".$eReportCard['RemarksReport']['ReportTitle'];
		
		$RemarksReportContentArr = array();
		$numOfClass = count($YearClassIDArr);
		for ($i=0; $i<$numOfClass; $i++)
		{
			$thisYearClassID = $YearClassIDArr[$i];
			
			# Get Student List
			$thisStudentInfoArr = $lreportcard->GET_STUDENT_BY_CLASS($thisYearClassID);
			// ignore class / form with no student
			if (!is_array($thisStudentInfoArr) || count($thisStudentInfoArr)==0)
				continue;
			
			$numOfStudent = count($thisStudentInfoArr);
			for ($j=0; $j<$numOfStudent; $j++)
			{
				$thisStudentID = $thisStudentInfoArr[$j]['UserID'];
				$thisStudentName = $thisStudentInfoArr[$j]['StudentName'];
				$thisClassNumber = $thisStudentInfoArr[$j]['ClassNumber'];
				$thisRemarksArr = array_remove_empty($lreportcard->Get_Report_Remarks($ReportID, $thisStudentID, $IncludePromotion=0));
				
				$RemarksReportContentArr[$thisYearClassID][$thisStudentID]['StudentName'] = $thisStudentName;
				$RemarksReportContentArr[$thisYearClassID][$thisStudentID]['ClassNumber'] = $thisClassNumber;
				$RemarksReportContentArr[$thisYearClassID][$thisStudentID]['RemarksArr'] = $thisRemarksArr;
			}
		}
		
		//debug_r($RemarksReportContentArr);
		
		## Display the result
		if ($ViewFormat == 'html')
		{
			$table = '';
			$numOfYearClass = count($RemarksReportContentArr);
			$YearClassCounter = 0;
			foreach ((array)$RemarksReportContentArr as $thisYearClassID => $thisRemarksContentArr)
			{
				$YearClassCounter++;
				$PageBreakCss = ($YearClassCounter==$numOfYearClass)? "" : "style='page-break-after:always'";
				$FirstBr = ($YearClassCounter==1)? "<br />" : "";
				
				# Get Class Name
				$Obj_YearClass = new year_class($thisYearClassID);
				$thisClassName = $Obj_YearClass->Get_Class_Name();
				
				# Build Report Title
				$thisReportTitle = $ReportTitle.' '.$thisClassName;
				
				
				$table .= '	<tr>
								<td align="center">'.$FirstBr.'<h1 style="text-align:left">'.$thisReportTitle.'</h1></td>
							</tr>
							<tr>
								<td>
							';
						$table .= "<table id='ResultTable' class='GrandMSContentTable' width='100%' border='0' cellpadding='2' cellspacing='0' $PageBreakCss>\n";
							$table .= "<thead>\n";
								$table .= "<tr>\n";
									$table .= "<th style='text-align:left' class='border_bottom' width='3%'>".$eReportCard['RemarksReport']['No.']."</th>\n";
									$table .= "<th style='text-align:left' class='border_bottom' width='18%'>".$eReportCard['RemarksReport']['Name']."</th>\n";
									$table .= "<th style='text-align:left' class='border_bottom'>".$eReportCard['RemarksReport']['Remarks']."</th>\n";
								$table .= "</tr>\n";
							$table .= "</thead>\n";
							$table .= "<tbody>\n";
								$numOfStudent = count($thisRemarksContentArr);
								$StudentCounter = 0;
								foreach($thisRemarksContentArr as $thisStudentID => $thisStudentInfoArr)
								{
									$StudentCounter++;
									if ($StudentCounter == $numOfStudent)
										$thisCss = "style='border-bottom:1px double #000; border-width:3px;'";
									else
										$thisCss = ($StudentCounter % 10 == 0)? "class='border_bottom'" : "";
									
									$thisRemark = implode('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', (array)$thisStudentInfoArr['RemarksArr']);
									$thisRemark = str_replace("<br />", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", $thisRemark);
									
									$table .= "<tr>\n";
										$table .= "<td align='left' $thisCss valign='top'>".$thisStudentInfoArr['ClassNumber']."</td>\n";
										$table .= "<td align='left' $thisCss valign='top' nowrap>".$thisStudentInfoArr['StudentName']."</td>\n";
										$table .= "<td align='left' $thisCss valign='top' style='font-size:".$FontSize.";'>".$thisRemark."</td>\n";
									$table .= "</tr>\n";
								}
							$table .= "</tbody>\n";
						$table .= "</table>\n";
						$table .= "<br style='clear:both'>\n";
					$table .= "</td>\n";
				$table .= "</tr>\n";
			}
			
			$css = $lreportcard->Get_GrandMS_CSS();
			
			$allTable = '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
							'.$table.'
						</table>';
			
			echo $lreportcard->Get_Report_Header($eReportCard['Reports_RemarksReport']);
			echo $allTable.$css;
			echo $lreportcard->Get_Report_Footer();
		}
		else if ($ViewFormat == 'csv')
		{
			include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
			include_once($PATH_WRT_ROOT."includes/libexporttext.php");
			
			$ExportHeaderArr = array();
			$ExportContentArr = array();
			$lexport = new libexporttext();
			
			# Header
			$ExportHeaderArr[] = '';
			$ExportHeaderArr[] = '';
			$ExportHeaderArr[] = '';
			
			# Content
			$i_counter = 0;
			$j_counter = 0;
			foreach ($RemarksReportContentArr as $thisYearClassID => $thisRemarksContentArr)
			{
				# Get Class Name
				$Obj_YearClass = new year_class($thisYearClassID);
				$thisClassName = $Obj_YearClass->Get_Class_Name();
				
				# Build Report Title
				$thisReportTitle = $ReportTitle.' '.$thisClassName;
				
				$ExportContentArr[$i_counter][$j_counter++] = '';
				$ExportContentArr[$i_counter][$j_counter++] = '';
				$ExportContentArr[$i_counter][$j_counter++] = $thisReportTitle;
				$i_counter++;
				$j_counter = 0;
				
				$ExportContentArr[$i_counter][$j_counter++] = $eReportCard['RemarksReport']['No.'];
				$ExportContentArr[$i_counter][$j_counter++] = $eReportCard['RemarksReport']['Name'];
				$ExportContentArr[$i_counter][$j_counter++] = $eReportCard['RemarksReport']['Remarks'];
				$i_counter++;
				$j_counter = 0;
				
				foreach($thisRemarksContentArr as $thisStudentID => $thisStudentInfoArr)
				{
					$thisRemark = implode('      ', (array)$thisStudentInfoArr['RemarksArr']);
					$thisRemark = str_replace("<br />", "      ", $thisRemark);
					
					$ExportContentArr[$i_counter][$j_counter++] = $thisStudentInfoArr['ClassNumber'];
					$ExportContentArr[$i_counter][$j_counter++] = $thisStudentInfoArr['StudentName'];
					$ExportContentArr[$i_counter][$j_counter++] = $thisRemark;
					
					$i_counter++;
					$j_counter = 0;
				}
				
				$ExportContentArr[$i_counter][$j_counter++] = '';
				$ExportContentArr[$i_counter][$j_counter++] = '';
				$ExportContentArr[$i_counter][$j_counter++] = '';
				$i_counter++;
				$j_counter = 0;
			}
			
			
			// Title of the grandmarksheet
			$filename = str_replace(' ', '_', $eReportCard['Reports_RemarksReport']).".csv";
			
			$export_content = $lexport->GET_EXPORT_TXT($ExportContentArr, $ExportHeaderArr);
			intranet_closedb();
			
			// Output the file to user browser
			$lexport->EXPORT_FILE($filename, $export_content);
		}
	}
}


?>