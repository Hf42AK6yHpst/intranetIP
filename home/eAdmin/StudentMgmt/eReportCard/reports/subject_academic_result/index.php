<?php
#  Editing by  connie

/****************************************************
 * Modification log
 * 	20111012 Marcus:
 * 		Cater show coresponding subject only for subject group  
 * **************************************************/
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$plugin['ReportCard2008'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
{
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$lreportcard = new libreportcardcustom();
} else {
	$lreportcard = new libreportcard();
}

if (!$lreportcard->hasAccessRight()) 
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lreportcard_ui = new libreportcard_ui();
$linterface = new interface_html();

$CurrentPage = "Reports_SubjectAcademicResult";
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eReportCard['Reports_SubjectAcademicResult'], "", 0);

$linterface->LAYOUT_START();

echo $lreportcard_ui->Get_SubjectAcademicResult_UI();

?>

<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-ui-1.7.3.custom.min.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.tablednd_0_5.js"></script>
<script>
var SchoolName = "<?=addslashes(htmlspecialchars_decode(GET_SCHOOL_NAME()))?>";
var loading = '<?=$linterface->Get_Ajax_Loading_Image()?>';


$().ready(function(){
	js_Reload_Selection();

	ChangeSortBy();
	$("ul.sortable").sortable({
		placeholder: 'placeholder'
	});
	$("ul.sortable").disableSelection();
	js_Toggle_Student_Selection(0);
	js_Select_All_Term();
	
});

function showOption(id)
{
	var tableID = "#table"+id;
	var showspanID = "#spanShowOption"+id;
	var hidespanID = "#spanHideOption"+id;

	$(tableID).show();
	$(hidespanID).show();
	$(showspanID).hide();
}

function hideOption(id)
{
	var tableID = "#table"+id;
	var showspanID = "#spanShowOption"+id;
	var hidespanID = "#spanHideOption"+id;

	$(tableID).hide();
	$(hidespanID).hide();
	$(showspanID).show();	
}

function js_Reload_Selection()
{
	var jsYearID = $("select#ClassLevelID").val(); 
	$('span#ReportSelectionSpan').html(loading);
	$('span#ClassSelectionSpan').html(loading);
	$('span#SubjectSelectionSpan').html(loading);
		
	$('span#ReportSelectionSpan').load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Report',
			YearID: jsYearID,
			ReportID: '',
			SelectionID: 'ReportID',
			onChange: 'js_Reload_Report_Column(); js_Reload_Subject_Selection(); js_Reload_Term_Selection();',
			HideNonGenerated: 1
		},
		function(ReturnData)
		{
			js_Reload_Report_Column();
			//$('#IndexDebugArea').html(ReturnData);
			js_Reload_Subject_Selection();
		}
	);
	
	onchange='';
	
	$('span#ClassSelectionSpan').load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Class',
			YearID: jsYearID,
			onchange: "js_Reload_Student_Selection('StudentByClass')",
			SelectionID: 'YearClassIDArr[]',
			isMultiple: 1
		},
		function(ReturnData)
		{	
			SelectAll(document.form1.elements['YearClassIDArr[]']);

			if($('.SelectFrom:checked').val()=='class')
			{
				js_Reload_Student_Selection("StudentByClass");
			}

		}
	);
	
}

function js_Reload_Term_Selection()
{
	var jsYearID = $("select#ClassLevelID").val(); 
	$('span#TermSelectionSpan').html(loading);
	
	var ReportID = $("select#ReportID").val();
	
	$('span#TermSelectionSpan').load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'TermByReport',
			SelectionID: 'TermID',
			ReportID: ReportID
		},
		function(ReturnData)
		{
			js_Select_All('TermID');
			js_Reload_Subject_Group_Selection();

		}
	);
	
}

function js_Reload_Subject_Selection()
{
	var YearID = $("#ClassLevelID").val();
	var ReportID = $("#ReportID").val();
	
	$('span#SubjectSelectionSpan').html(loading);
	$('span#StdSubjectSelectionSpan').html(loading);
	$('span#SortByClassSelectionSpan').html(loading);
	$('span#ReportColumnSelectionSpan').html(loading);
	

	$('span#SubjectSelectionSpan').load(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Subject',
			YearID: YearID,
			SelectionID: 'SubjectIDArr[]',
			onChange: 'js_Reload_Subject_Group_Selection()'
			
		},
		function(ReturnData)
		{
			js_Select_All_Subject_Group_Subject();
		}
	);
	
	$.post
	(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Subject',
			YearID: YearID,
			SelectionID: 'SortSubjectIDClass',
			IncludeComponent: 1,
			InputMarkOnly: 1,
			includeGrandMarks: 1,
			isMultiple:0,
			OtherTagInfo: "class='SortByClassSubjectSelection'",
			ReportID:ReportID
		},
		function(ReturnData)
		{
			$('span#SortByClassSelectionSpan').html(ReturnData);
			ChangeSortBy();
		}
	);
	
	$.post
	(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'Subject',
			YearID: YearID,
			SelectionID: 'SortSubjectIDForm',
			IncludeComponent: 1,
			InputMarkOnly: 1,
			includeGrandMarks: 1,
			isMultiple:0,
			OtherTagInfo: "class='SortByFormSubjectSelection'",
			ReportID:ReportID
		},
		function(ReturnData)
		{
			$('span#SortByFormSelectionSpan').html(ReturnData);
			ChangeSortBy();
		}
	);
}

function js_Reload_Report_Column()
{
	var ReportID = $("select#ReportID").val();
	
	$.post(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'ReportColumnCheckbox',
			ReportID: ReportID,
			ReportFunction: 'subject_academic_result'
		},
		function(ReturnData)
		{
			var DataArr = ReturnData.split(":_delimiter_:");
			$('span#ReportColumnSelectionSpan').html(DataArr[2]);
			
			//change report title
			var ReportTitle = DataArr[0];
			$("#ReportTitle").val(SchoolName+" "+ReportTitle);
	
			// show/ hide item
			var isTermReport = DataArr[1];
			handleTermReport(isTermReport);
			refreshSortingTable();
			jsShowHideColumnLabelOption();
						
		}
	);

}

function handleTermReport(isTermReport)
{
	if(isTermReport==1)
	{
		$("span#OrderMeritSubjectGroupSpan").show().find("input#OrderMeritSubjectGroup").attr("disabled","");
		$("tr#DataSourceRow").hide().find("input").attr("disabled","disabled");
		
	}
	else
	{
		$("span#OrderMeritSubjectGroupSpan").hide().find("input#OrderMeritSubjectGroup").attr("disabled","disabled");
		$("tr#DataSourceRow").show().find("input").attr("disabled","");
	}
}

// refresh Column Order List for user to adjust column order
function refreshSortingTable()
{

}


function SelectAll(obj)
{
	$(obj).children().attr("selected","selected")
}

function jsSelectAllCheckbox(jsChecked, jsCheckboxClass)
{
	$('input.' + jsCheckboxClass).each( function() {
		$(this).attr('checked', jsChecked);
	})
}

function jsCheckUnSelectAll(jsChecked, jsSelectAllChkID)
{
	if (jsChecked == false)
		$('input#' + jsSelectAllChkID).attr('checked', false);
}

function ChangeSortBy()
{
	var sortby = $("input[name='SortBy']:checked").val();
	
	switch(sortby)
	{
		case '0' : //class class no
			$("select.SortByClassSubjectSelection").attr("disabled","disabled");
			$("select.SortByFormSubjectSelection").attr("disabled","disabled");
		break;
		case '1' : //class rank
			$("select.SortByClassSubjectSelection").attr("disabled",'');
			$("select.SortByFormSubjectSelection").attr("disabled","disabled");
		break;
		case '2' : //form rank			
			$("select.SortByFormSubjectSelection").attr("disabled",'');
			$("select.SortByClassSubjectSelection").attr("disabled","disabled");
		break;	
	}
}


function jsShowHideColumnLabelOption()
{
	var ColumnCheckedCount =$("input.ReportColumnChk:checked").length; 

	if(ColumnCheckedCount==1)
	{
		$("tr#ColumnLabelOptionRow:hidden").show();
	}
	else
	{
		$("input#DisplayColumnLabel").attr("checked","checked");
		$("tr#ColumnLabelOptionRow:visible").hide();
	}
}

function js_Toggle_Student_Selection(ParLoadStdSelection)
{
	if($(".SelectFrom:checked").val()=='class')
	{
		$(".SubjectGroupSelectOption").hide()
		$(".ClassSelectOption").show()
	}
	else
	{
		$(".SubjectGroupSelectOption").show()
		$(".ClassSelectOption").hide()
	}
	js_Toggle_Subject_Selection();
	

	if(ParLoadStdSelection==0)
	{		
	}
	else
	{
		js_Reload_Term_Selection();
	}
 

}

function js_Select_All_Term()
{
	js_Select_All('TermID');
	js_Reload_Subject_Group_Selection();
}

function js_Select_All_Class()
{
	js_Select_All('YearClassIDArr[]');
	if($(".SelectFrom:checked").val()=='class')
	{
		js_Reload_Student_Selection("StudentByClass");
	}
	else if($(".SelectFrom:checked").val()=='subjectgroup')
	{
		js_Reload_Student_Selection('StudentBySubjectGroup');
	} 
}

function js_Select_All_Subject()
{
	js_Select_All('SubjectIDArr[]');
	js_Reload_Subject_Group_Selection();
}

function js_Select_All_SubjectGroup()
{
	js_Select_All('SubjectGroupIDArr[]');
	
	if($(".SelectFrom:checked").val()=='class')
	{
		js_Reload_Student_Selection("StudentByClass");
	}
	else if($(".SelectFrom:checked").val()=='subjectgroup')
	{
		js_Reload_Student_Selection('StudentBySubjectGroup');
	} 
}

function js_Reload_Subject_Group_Selection()
{
	var ClassLevelID = $("#ClassLevelID").val();
	var TermID = '';

	if($("select#TermID").val())
	{
		TermID = $("select#TermID").val().toString();	
	}

	if(!$("select#TermID").val())
	{
		return false;
	}
		
	 var SubjectID = '';
	 
	 if( $("#SubjectIDArr\\[\\]").val())
	 {
	 	SubjectID = $("#SubjectIDArr\\[\\]").val().toString();
	 }
			
	$("#SubjectGroupSelectionSpan").html(loading);	
		
	$.post(
		"../ajax_reload_selection.php", 
		{ 
			RecordType: 'SubjectGroup',
			SelectionID: 'SubjectGroupIDArr[]',
			ClassLevelID: ClassLevelID,
			SubjectID: SubjectID,
			SemID:TermID,
			isMultiple:1,
			noFirst:1
		},
		function(ReturnData)
		{
			$("#SubjectGroupSelectionSpan").html(ReturnData);	
	//		$("select#SubjectGroupIDArr\\[\\]").attr('onchange','js_Reload_Student_Selection("StudentBySubjectGroup")');
					
			js_Select_All('SubjectGroupIDArr[]');
			
			
			if($(".SelectFrom:checked").val()=='subjectgroup')
			{
				js_Reload_Student_Selection('StudentBySubjectGroup');
			}

		}
	);
	
}

function js_Reload_Student_Selection(ParRecordType)
{

	var ClassID = '';
	var TermID = '';
	var SubjectID = '';
	var SubjectGroupID='';
	
	var SelectionID ='';

	SelectionID = 'StudentIDArr[]';
	if(ParRecordType=='StudentByClass')
	{	
		if($("#YearClassIDArr\\[\\]").val())
		{
			ClassID = $("#YearClassIDArr\\[\\]").val().toString();
		}		
	}
	else if (ParRecordType=='StudentBySubjectGroup')
	{
		if($("#SubjectGroupIDArr\\[\\]").val())
		{
			SubjectGroupID = $("#SubjectGroupIDArr\\[\\]").val().toString();
		}	
	}

	if($("select#TermID").val())
	{
		TermID = $("select#TermID").val().toString();	
	}

	if($("select#SubjectGroupIDArr\\[\\]").val())
	{
		SubjectGroupID = $("select#SubjectGroupIDArr\\[\\]").val().toString();	
	}

//
//	$("#StudentSelectionSpan").html(loading);	
//
//	$.post(
//		"../ajax_reload_selection.php", 
//		{ 
//			async: false,
//			RecordType: ParRecordType,
//			SelectionID: SelectionID,
//			ClassID: ClassID,
//			SubjectGroupID: SubjectGroupID,
//			isMultiple:1,
//			noFirst:1
//		},
//		function(ReturnData)
//		{
//			$("#StudentSelectionSpan").html(ReturnData);			
//			js_Select_All('StudentIDArr[]');
//		}
//	);
	
}


function js_Select_All_Subject_Group_Subject()
{
	js_Select_All('SubjectIDArr[]');
	js_Reload_Subject_Group_Selection();
}




function js_Check_Form()
{
	if($(".SelectFrom:checked").val()=='class')
	{
		// User must select at least one Class
		if (countOption(document.getElementById('YearClassIDArr[]')) == 0)
		{
			alert('<?=$eReportCard['jsSelectSubjectWarning']?>');
			document.getElementById('YearClassIDArr[]').focus();
			return false;
		}
	}
	else
	{
		if (countOption(document.getElementById('SubjectGroupIDArr[]')) == 0)
		{
			alert('<?=$Lang['eReportCard']['WarningArr']['Select']['SubjectGroup']?>');
			document.getElementById('SubjectGroupIDArr[]').focus();
			return false;
		}
	}
	
	var jsFocusElementID;
	var jsHasChecked = false;
	var jsElementCounter = 0;
	
	// User must select at least one student display data 
	$('input.StudentDataDisplayChk').each( function() {
		if (jsElementCounter == 0)
			jsFocusElementID = $(this).attr('id');	// focus the first element if the user selected nth
			
		jsElementCounter++;
		
		if ($(this).attr('checked') == true)
		{
			jsHasChecked = true;
			return false;
		}
	});
	

	
	// User must select at least one Subject
	if (countOption(document.getElementById('YearClassIDArr[]')) == 0)
	{
		alert('<?=$eReportCard['jsSelectClassWarning']?>');
		document.getElementById('YearClassIDArr[]').focus();
		return false;
	}
	
	// user must select at least one report column for display
	jsHasChecked = false;
	jsElementCounter = 0;
	$('input.ReportColumnChk').each( function() {
		if (jsElementCounter == 0)
			jsFocusElementID = $(this).attr('id');	// focus the first element if the user selected nth
			
		jsElementCounter++;
		
		if ($(this).attr('checked') == true)
		{
			jsHasChecked = true;
			return false;
		}
	});
	
	if (jsHasChecked == false)
	{
		alert('<?=$eReportCard['MasterReport']['jsWarningArr']['SelectReportColumn']?>');
		$('input#' + jsFocusElementID).focus();
		return false;
	}
	
	// user must select at least one subject data for display  (??)
	jsHasChecked = false;
	jsElementCounter = 0;
	$('input.SubjectDataChk').each( function() {
		if (jsElementCounter == 0)
			jsFocusElementID = $(this).attr('id');	// focus the first element if the user selected nth
			
		jsElementCounter++;
		
		if ($(this).attr('checked') == true && $(this).attr('disabled') == false)	// for disabled SubjectGroup selection if Consolidated Report
		{
			jsHasChecked = true;
			return false;
		}
	});
	
	
	document.form1.action = "subject_academic_result.php";
	document.form1.target = "_blank";
	document.form1.submit();
	
}



function js_Toggle_Subject_Selection()
{
	var isChecked = $("input#ShowCorrespondingSubjectOnly").attr("checked")
	
	if($(".SelectFrom:checked").val()=='subjectgroup' && isChecked)
	{
		js_Select_All('SubjectIDArr[]');
		refreshSortingTable();
		$("select#SubjectIDArr\\[\\]").attr("disabled","disabled");
	}
	else
	{
		$("select#SubjectIDArr\\[\\]").attr("disabled","");
	}

}

</script>
<style>
ul { list-style-type: none; margin:0; padding-left:20px; }
.sortable li label, .sortable li{ cursor:move; }
.placeholder {border: 1px dotted #888; width:200px; height:14px; }
.common_table_list tr td {padding: 0px 0px;}
</style>
<?

$linterface->LAYOUT_STOP();

?>