<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libclass.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
		
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") 
	{
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	$linterface = new interface_html();
	$lclass = new libclass();
	$fcm = new form_class_manage();
	$YearTermArr = $fcm->Get_Academic_Year_Term_List($lreportcard->GET_ACTIVE_YEAR_ID());
	
	if ($lreportcard->hasAccessRight()) {
		if(count($YearTermArr)>1) // at least 2 academic term
		{
			# Form Selection
			$ClassLvlArr = $lclass->getLevelArray();
			$SelectedFormArr = explode(",", $SelectedForm);
			$SelectedFormTextArr = explode(",", $SelectedFormText);
			$selectFormHTML = "<select id=\"ClassLvID[]\" name=\"ClassLvID[]\" multiple size=\"5\">\n";
			for ($i = 0; $i < sizeof($ClassLvlArr); $i++) {
				$selectFormHTML .= "<option value=\"".$ClassLvlArr[$i][0]."\"";
				if ($_POST["submit_flag"] != "YES" || in_array($ClassLvlArr[$i][0], $SelectedFormArr)) {
					$selectFormHTML .= " selected";
				}
				$selectFormHTML .= ">".$ClassLvlArr[$i][1]."</option>\n";
			}
			$selectFormHTML .= "</select>\n";
			$selectFormHTML .= $linterface->GET_SMALL_BTN($button_select_all, "button", "js_Select_All('ClassLvID[]'); return false;");
			
			# Term Selection
			$FromTerm=$lreportcard->Get_Term_Selection("FromTerm",$lreportcard->GET_ACTIVE_YEAR_ID(),$YearTermArr[0][0],"",1,0,0); 
			$ToTerm=$lreportcard->Get_Term_Selection("ToTerm",$lreportcard->GET_ACTIVE_YEAR_ID(),$YearTermArr[1][0],"",1,0,0); 
			$TermSelection =$i_From." ".$FromTerm." ".$i_To." ".$ToTerm;
		
			# Conduct Selection
			$conductcheckboxname= "ConductMark[]";
			$thisConductSelection = '';
			$thisConductSelection = $lreportcard->Get_Conduct_CheckBoxes($conductcheckboxname);
			
			# Conduct Improvement Selection
			$ConductAry = (array)$lreportcard->ConductAry;
			$maxno=count($ConductAry);
			$ConductImprovement .= "<select name='requiredGradeImprovement' id='requiredGradeImprovement'>";
			for($i=1;$i<$maxno;$i++)
			{
				$ConductImprovement .= "<option value='$i'>$i</option>";
			}
			$ConductImprovement .= "</select>";
			
			#Get Last Two Conduct
			$failCondunt= array();
			$failConduct[]=array_pop($ConductAry);
			$failConduct[]=array_pop($ConductAry);
			
			# Conduct
			if($thisConductSelection)
			{
				#Grade Improvement
				$thisRow .= '<tr>'."\n";
					$thisRow .= '<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'."\n";
						$thisRow .= $eReportCard['GradeImprovement']."\n";
					$thisRow .= '</td>'."\n";
					$thisRow .= '<td width="75%" class="tabletext">'."\n";
						$thisRow .= $ConductImprovement." ".$eReportCard['orabove']."\n";
					$thisRow .= '</td>'."\n";
				$thisRow .= '</tr>'."\n";			
				
				# Excluded Conduct
				$thisRow .= '<tr>'."\n";
					$thisRow .= '<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'."\n";
						$thisRow .= $eReportCard['Excluded'].$eReportCard['Management_ConductMark']."\n";
					$thisRow .= '</td>'."\n";
					$thisRow .= '<td width="75%" class="tabletext">'."\n";
						$thisRow .= $thisConductSelection."\n";
					$thisRow .= '</td>'."\n";
				$thisRow .= '</tr>'."\n";
			}
	
			$ConductSelection .= $thisRow;
	
		} 
		else
		{
			$warnOneTerm = "test";
		}
		// end if (at least 2 academic term)
		
		
	
		############## Interface Start ##############
		$linterface = new interface_html();
		$CurrentPage = "Reports_ConductProgressReport";
		$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
		# tag information
		$TAGS_OBJ[] = array($eReportCard['Reports_ConductProgressReport']);
		$linterface->LAYOUT_START();
		
		if ($eRCTemplateSetting['Report']['ConductProgress']['ResultToAwardStudentList']) {
			// transfer to award list option
			$x = '';
			$x .= '<tr id="TransferToAwardTr">'."\r\n";
				$x .= '<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'."\r\n";
					$x .= $Lang['eReportCard']['ReportArr']['TransferResultToAwardStudentList'];
				$x .= '</td>'."\r\n";
				$x .= '<td width="75%" class="tabletext">'."\r\n";
					$x .= $linterface->Get_Radio_Button('transferToAward_yes', 'transferToAward', 1, $isChecked=1, $Class="", $Lang['General']['Yes']);
					$x .= '&nbsp;';
					$x .= $linterface->Get_Radio_Button('transferToAward_no', 'transferToAward', 0, $isChecked=0, $Class="", $Lang['General']['No']);
				$x .= '</td>'."\r\n";
			$x .= '</tr>'."\r\n";
			$htmlAry['transferToAwardTr'] = $x;
		}

?>
<script language="javascript">
<!--
//function SelectAll(obj){
//	for (i=0; i<obj.length; i++){
//		obj.options[i].selected = true;
//	}
//}

function checkForm() 
{
	var obj = document.form1;
	var select_obj = obj.elements['ClassLvID[]'];

	var selected=false;
	for (var i=0; i<select_obj.length; i++)
	{
		if (select_obj[i].selected)
			 selected = true;
	}
	
	if(!selected)
	{
		alert('<?=$i_Discipline_System_alert_PleaseSelectClassLevel?>');
		return false;
	}

	if(obj.elements['FromTerm'].selectedIndex == obj.elements['ToTerm'].selectedIndex)
	{
		alert('<?=$eReportCard['SelectDiffTerm']?>');
		return false;
	}
	else if(obj.elements['FromTerm'].selectedIndex > obj.elements['ToTerm'].selectedIndex)
	{
		alert("<?=$eReportCard['warnTermOrder']?>")
		return false;
	}
	
	if($("[name=<?=$conductcheckboxname?>]:checked").length == $("[name=<?=$conductcheckboxname?>]").length) 
	{
		alert("<?=$eReportCard['warnExcludedAllConduct']?>")
		return false
	}
	
	return true;
}

//checked fail conduct initially
$().ready(function () {
	<?
	foreach($failConduct as $conduct)
		echo "$(\"#$conduct\").attr('checked','true');\n";
	?>
}
);
//-->
</script>
<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT."templates/2007a/css/"?>ereportcard.css">
<br />

<form name="form1" action="generate.php" method="POST" target="_blank" onsubmit="return checkForm();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
<table>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<?if(count($YearTermArr)<=1){ //if less than 2 term?>
				<tr>
					<td colspan=2><?=$eReportCard['OneTermOnly']?></td>
				</tr>
				<?
				}
				else //at least 2 term
				{
				?>
				<tr>
					<td width="30%"></td>
					<td width="70%" align="right"><?=$linterface->GET_SYS_MSG($Result);?></td>
				</tr>
				<tr id="ViewFormatRow">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ViewFormat'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="ViewFormat" id="ViewHTML" value="html" CHECKED>
							<label for="ViewHTML"><?=$eReportCard['HTML']?></label>
						</input>
						<input type="radio" name="ViewFormat" id="ViewCSV" value="csv">
							<label for="ViewCSV"><?=$eReportCard['CSV']?></label>
						</input>
					</td>
				</tr>
			<?php if(count($ClassLvlArr) == 0) { ?>
				<tr>
					<td align="center" colspan="2" class='tabletext'>
						<br /><?= $eReportCard['NoFormSetting'] ?><br />
					</td>
				</tr>
			<?php } else { ?>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['Form'] ?><span class="tabletextrequire">*</span>
					</td>
					<td width="75%" class='tabletext'>
						<?= $selectFormHTML ?>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['Term'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<?= $TermSelection ?>
					</td>
				</tr>
				
				<?= $ConductSelection ?>
				<?=$htmlAry['transferToAwardTr']?>
				
				<tr>
					<td colspan="2" class="dotline">
						<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submitBtn"); ?>
					</td>
				</tr>
			<?php } ?>
			<?php } //end if less than 2 term?>
			</table>
		</td>
	</tr>
</table>
</form>
<?
	print $linterface->FOCUS_ON_LOAD("form1.ClassLevelID");
  	$linterface->LAYOUT_STOP();
  } else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>

<?
}
?>
