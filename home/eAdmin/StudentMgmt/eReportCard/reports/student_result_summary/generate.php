<?
// Modifying by

/********************************************************
 * Modification log
 * 20190828 Bill
 *      - allow parent / student access  ($eRCTemplateSetting['Management']['MarksheetVerification']['ShowStudentResultSummary'])
 * ******************************************************/

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

$IsPrintFromParent = false;
if ($plugin['ReportCard2008'])
{
    if (is_file($intranet_root."/includes/eRCConfig.php")) {
        include_once ($intranet_root."/includes/eRCConfig.php");
    }

    // Check if valid access in Print Mode
    $IsPrintFromParent = $_POST['IsPrintFromParent'];
    if($eRCTemplateSetting['Management']['MarksheetVerification']['ShowStudentResultSummary'] && $IsPrintFromParent && performSimpleTokenByDateTimeChecking($_POST['token'], $_SERVER['REQUEST_URI'].$_POST['UserID']))
    {
        @session_start();
        $_SESSION['UserID'] = $_POST['UserID'];

        $specialPrintHandling = true;
    }
}

intranet_auth();
intranet_opendb();

//$ViewFormat = $_POST['ViewFormat'];
/*$TermID = $_POST['TermID'];
$Level = $_POST['Level'];
$TargetIDArr = $_POST['TargetID'];*/

$ViewFormat = 'html'; //$_REQUEST['ViewFormat'];
//$ViewRank = $_REQUEST['ViewRank'];
$YearID = $_REQUEST['YearID'];
$thisReportID = $_REQUEST['ReportID'];
$TargetIDArr = $_REQUEST['YearClassIDArr'];
$SubjectIDArr = $_REQUEST['SubjectIDArr'];
$IsPrintFromParent = $_POST['IsPrintFromParent'];

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	if (($IsPrintFromParent && $specialPrintHandling) || $lreportcard->hasAccessRight())
	{
		# Report Title
		$ObjAcademicYear = new academic_year($lreportcard->schoolYearID);
		$ActiveYear = $ObjAcademicYear->Get_Academic_Year_Name();
		
		# prepare Student List of TargetClass
		$thisClassLevelID = $YearID;
		$StudentList = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($thisReportID, $thisClassLevelID, $TargetIDArr);
		$ClassStudentList = BuildMultiKeyAssoc($StudentList,array("YearClassID","UserID"));
		
		# Report Column 
		$ReportColumnTitle = $lreportcard->returnReportColoumnTitle($thisReportID);
		$NumOfColumn = count($ReportColumnTitle);		
		
		# Get Grand Mark Arr
		$GrandMarksArr = $lreportcard->getReportResultScore($thisReportID, 0);	
		
		# Load CalSetting
		$CalSetting = $lreportcard->LOAD_SETTING("Calculation");
		
		$numOfTarget = count($TargetIDArr);
		for ($j=0; $j<$numOfTarget; $j++)
		{
			$thisClassID = $TargetIDArr[$j];
			
			# Get Report Info
			$ReportTemplateSetting = $lreportcard->returnReportTemplateBasicInfo($thisReportID);
			$TermID = $ReportTemplateSetting["Semester"];
			$ClassLevelID = $ReportTemplateSetting["ClassLevelID"];
			$ReportType = $TermID == "F" ? "W" : "T";
			$CalOrder = ($ReportType == "W") ? $CalSetting["OrderFullYear"] : $CalSetting["OrderTerm"];
			$thisStudentInfoArr = array_values((array)$ClassStudentList[$thisClassID]);

			# Get Sem Title 
			$SemsArr = getSemesters($lreportcard->schoolYearID);
			$SemsData = array_keys((array)$SemsArr);
			$SemSeqNo = array_search($TermID,(array)$SemsData) +1;
			$SemTitle = $TermID=="F"?$eReportCard['ExaminationSummary']['YearResult']:$SemsArr[$TermID];

			# Prepare MainSubject Array
			$MainSubjectArray 	= $lreportcard->returnSubjectwOrder($ClassLevelID);
			if(sizeof($MainSubjectArray) > 0)
				foreach($MainSubjectArray as $MainSubjectIDArray[] => $MainSubjectNameArray[]);

			// ignore class / form with no student
			if (!is_array($thisStudentInfoArr) || count($thisStudentInfoArr)==0)
				continue;
				
			# Get MarkAry
			$MarkAry = $lreportcard->getMarks($thisReportID); 
			
			# Get Grading Scheme Info 
			$GradingInfo = $lreportcard->Get_Form_Grading_Scheme($thisClassLevelID, $thisReportID); 
			
			# Get Subject Code ID Mapping
//			$SubjectIDCodeMap = $lreportcard->GET_SUBJECTS_CODEID_MAP();

			# Loop each student		
			$numOfStudent = count($thisStudentInfoArr);			
			for ($i=0; $i<$numOfStudent; $i++)
			{
				$thisStudentID = $thisStudentInfoArr[$i]["UserID"];

				$SubjectColumnArr = $MarkAry[$thisStudentID];
				
				if(!isset($thisClassInfo[$thisClassID]))
					$thisClassInfo[$thisClassID] = $lreportcard->Get_Student_Class_ClassLevel_Info($thisStudentID);
				$thisClassName = $thisClassInfo[$thisClassID]["ClassTitleEn"];
				
				# Get Other Info (e.g. Attendance, Conduct)
				$thisOtherInfo = $lreportcard->getReportOtherInfoData($thisReportID,$thisStudentID); 
				$last_sem = $TermID=="F"?0:$TermID;
				
				$resultArr[$thisClassID][$thisStudentID]["OtherInfo"] = $thisOtherInfo[$thisStudentID][$last_sem];
								
				# Get Grand Average
				$GrandMarks = $GrandMarksArr[$thisStudentID][0];
				$resultArr[$thisClassID][$thisStudentID]["GPA"] = 	$GrandMarks["GPA"];
								
				foreach((array)$SubjectColumnArr as $thisSubjectID => $thisColumnInfoArr)
				{
					// skip subj not chosen
					if(!in_array($thisSubjectID,$SubjectIDArr)) continue;
					
					$isSub = 0;
					if(in_array($thisSubjectID, $MainSubjectIDArray)!=true)	$isSub=1;
					
					$CmpSubjectArr = array();
					$isParentSubject = 0;		// set to "1" when one ScaleInput of component subjects is Mark(M)
					if (!$isSub) {
						$CmpSubjectArr = $lreportcard->GET_COMPONENT_SUBJECT($thisSubjectID, $ClassLevelID);
						if(!empty($CmpSubjectArr)) $isParentSubject = 1;
					}
					
					$isAllCmpZeroWeightArr = $lreportcard->IS_ALL_CMP_SUBJECT_ZERO_WEIGHT($thisReportID, $thisSubjectID);
					$isAllCmpZeroWeight = !in_array(false,$isAllCmpZeroWeightArr);
					
//					if(!$lreportcard->CHECK_SUBJECT_IS_COMPONENT_SUBJECT($thisSubjectID))
//					{
						$TmpMarkArr = array();
						$isAllNA = true; 
						foreach((array)$thisColumnInfoArr as $ReportColumnID => $thisColumnInfo)
						{
//							$columnWeightConds = " ReportColumnID = '$ReportColumnID' AND SubjectID = '$thisSubjectID' " ;
//							$columnSubjectWeightArr = $lreportcard->returnReportTemplateSubjectWeightData($thisReportID, $columnWeightConds);
//							$columnSubjectWeightTemp = $columnSubjectWeightArr[0]['Weight'];
							
							$thisMark = trim($thisColumnInfo["Grade"])!=''?$thisColumnInfo["Grade"]:$thisColumnInfo["Mark"];
							if(trim($thisMark) != "N.A.")
								$isAllNA = false;

							if ($isParentSubject && $CalOrder == 1 && $ReportColumnID != 0) 
								$thisMark = $lreportcard->EmptySymbol;
							
							if($lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET1($thisMark) || $lreportcard->CHECK_MARKSHEET_SPECIAL_CASE_SET2($thisMark) )
								$thisMark = $lreportcard->EmptySymbol;
								
							$TmpMarkArr[$ReportColumnID]['Mark'] = $thisMark;	
						}
						if(!$isAllNA)
							$resultArr[$thisClassID][$thisStudentID]["SubjectMarks"][$thisSubjectID] = $TmpMarkArr;
//					}
				}
			}
		}
		
//				debug_pr($resultArr);
		## Display the result
//		if ($ViewFormat == 'html')
//		{
			
			$studentCtr=0;
			$thisClassName='';
			
			foreach((array)$resultArr as $thisClassID => $StudentSummary)
			{
				$lu = new libuser('','',array_keys((array)$StudentSummary));

				foreach($StudentSummary as $thisStudentID => $thisSummaryDetail)
				{ 
					# Class Student Info 
					$thisStudentInfoArr = $lreportcard->Get_Student_Class_ClassLevel_Info($thisStudentID);
					$thisClassName = $thisStudentInfoArr[0]['ClassName'];
					$thisClassNumber = $thisStudentInfoArr[0]['ClassNumber'];
					
					$lu->LoadUserData($thisStudentID);
					$ClassInfo = '';
					$ClassInfo .= "<table border='0' cellpadding='0' cellspacing='0'>";
						$ClassInfo .= "<tr>";
							$ClassInfo .= "<td>";
								$ClassInfo .= $eReportCard['ExaminationSummary']['Year'].": ".$lreportcard->GET_ACTIVE_YEAR_NAME();
							$ClassInfo .= "</td>";
						$ClassInfo .= "</tr>";
						$ClassInfo .= "<tr>";
							$ClassInfo .= "<td>";
								$ClassInfo .= $eReportCard['Term'].": ".$SemTitle;
							$ClassInfo .= "</td>";
						$ClassInfo .= "</tr>";
						$ClassInfo .= "<tr>";
							$ClassInfo .= "<td>";
								$ClassInfo .= Get_Lang_Selection($lu->ChineseName,$lu->EnglishName);	
							$ClassInfo .= "</td>";
						$ClassInfo .= "</tr>";
						$ClassInfo .= "<tr>";
							$ClassInfo .= "<td>";
								$ClassInfo .= $eReportCard["Class"].": ".$thisClassName."-".$thisClassNumber;
							$ClassInfo .= "</td>";
						$ClassInfo .= "</tr>";
					$ClassInfo .= "</table>"; 
						
					# Subject Marks 	
					$thead = "<thead>\n";
						$thead .= "<tr>\n";
//							$thead .= "<th>&nbsp;</th>\n";
							for($i=0;$i<3;$i++)
							{
								$thead .= "<th width='40px' align='center' >".$eReportCard['ExaminationSummary']['Subj']."</th>\n";
								foreach($ReportColumnTitle as $thisTitle)
								{
									$thead .= "<th width='45px' align='center' >".$thisTitle."</th>\n";
								}
								$thead .= "<th width='45px' align='center' >".$eReportCard['Overall']."</th>\n";
							}
						$thead .= "</tr>\n";
					$thead .= "</thead>\n";

					$SubjMarks = '';
					$SubjMarks .= "<table border='1' width='100%' cellpadding='0' cellspacing='0' class='GrandMSContentTable'>";
						# thead
						$SubjMarks .= "<thead>\n";
							$SubjMarks .= "<tr>\n";
								for($i=0;$i<3;$i++)
								{
									$SubjMarks .= "<th width='40px' align='center' >".$eReportCard['ExaminationSummary']['Subj']."</th>\n";
									foreach($ReportColumnTitle as $thisTitle)
									{
										$SubjMarks .= "<th width='40px' align='center' >".$thisTitle."</th>\n";
									}
									$SubjMarks .= "<th width='40px' align='center' >".$eReportCard['Overall']."</th>\n";
								}
							$SubjMarks .= "</tr>\n";
						$SubjMarks .= "</thead>\n";
						
						# tbody						
						$ctr=0;
						foreach((array)$thisSummaryDetail["SubjectMarks"] as $thisSubjID => $thisSubjDetailArr)
						{
							$ParentSubjectID = $lreportcard->GET_PARENT_SUBJECT_ID($thisSubjID);
							$ParentSubjectName = $lreportcard->GET_SUBJECT_NAME_LANG($ParentSubjectID,"EN",1);
							
							if($ctr%3 == 0)	$SubjMarks .= "<tr>";
							if(!isset($thisSubjectName[$thisSubjID]))
								$thisSubjectName[$thisSubjID] = $lreportcard->GET_SUBJECT_NAME_LANG($thisSubjID,"EN",1);
							$thisSubjDisplay = $thisSubjectName[$thisSubjID];
							if(!empty($ParentSubjectID))
								$thisSubjDisplay = $ParentSubjectName."[".$thisSubjectName[$thisSubjID]."]";
							else
								$thisSubjDisplay = $thisSubjectName[$thisSubjID];
							
							$SubjMarks .= "<td align='center'>".$thisSubjDisplay."&nbsp;</td>";
							foreach($thisSubjDetailArr as $thisColumnID => $thisSubjDetail)
								$SubjMarks .= "<td align='center'>".($thisSubjDetail["Mark"]?$thisSubjDetail["Mark"]:'')."&nbsp;</td>";
							if($ctr%3 == 2)	$SubjMarks .= "</tr>\n";
							$ctr++;
						}
						for($i=0; $i<(3-$ctr%3) && $ctr%3!=0; $i++) // print empty td on the last row
						{
							$SubjMarks .= str_repeat("<td>&nbsp;</td>",$NumOfColumn+2);
						}
						if($ctr%3 != 0)	$SubjMarks .= "</tr>";
							
					$SubjMarks .= "</table>"; 
					
					# Other Info & Other Info
					$GrandInfo = '';
					$GrandInfo .= "<table border='0' width='40%' cellpadding='0' cellspacing='0'>";
						$GrandInfo .= "<tr>";
							$GrandInfo .= "<td>".$eReportCard["Template"]["Conduct"].": ".$thisSummaryDetail["OtherInfo"]["Conduct"]."</td>";
							$GrandInfo .= "<td>".$eReportCard['StudentResultSummary']['Reading'].": ".$thisSummaryDetail["OtherInfo"]["Reading"]."</td>";
						$GrandInfo .= "</tr>";
						$GrandInfo .= "<tr>";
							$GrandInfo .= "<td>".$eReportCard["GPA"].": ".$thisSummaryDetail["GPA"]."</td>";
							$GrandInfo .= "<td>&nbsp;</td>";
						$GrandInfo .= "</tr>";
					$GrandInfo .= "</table>";
					
					$page_break_css = (++$studentCtr)%6==0?"style='page-break-after:always'":"";
					$table .= '<!--startReportOfStudent_'.$thisStudentID.'-->';
					$table .= "<br><br><table id='ResultTable' class='GrandMSContentTable' width='80%' border='0' cellpadding='2' cellspacing='0' $page_break_css>\n";
						$table .= "<tbody>\n";
							$table .="<tr>";				
								$table .="<td width='180px'>";		
									$table .= $ClassInfo;		
								$table .="</td>";							
								$table .="<td valign='top'>";
									$table .=$SubjMarks;				
								$table .="</td>";											
							$table .="</tr>";	
							$table .="<tr>";				
								$table .="<td valign='bottom'>";		
									$table .= "&nbsp;";		
								$table .="</td>";							
								$table .="<td>";
									$table .=$GrandInfo;				
								$table .="</td>";											
							$table .="</tr>";	
						$table .= "</tbody>\n";
					$table .= "</table>\n";
					$table .= '<!--endReportOfStudent_'.$thisStudentID.'-->';
				}
			}
			
			$css = $lreportcard->Get_GrandMS_CSS();
			
			echo $lreportcard->Get_Report_Header($ReportTitle);
			echo $table.$css;
			echo $lreportcard->Get_Report_Footer();
//		}
//		else if ($ViewFormat == 'csv')
//		{
//			include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//			include_once($PATH_WRT_ROOT."includes/libexporttext.php");
//			
//			$ExportHeaderArr = array();
//			$ExportContentArr = array();
//			$lexport = new libexporttext();
//			
//			foreach((array)$resultArr as $thisClassID => $StudentSummary)
//			{
//				$ExportClassArr= array();
//				foreach($StudentSummary as $thisStudentID => $thisSummaryDetail)
//				{ 
//					$ExportRowArr = array();
//					# Class Student Info 
//					$lu = new libuser($thisStudentID);
//					$thisEnglishName = $lu->EnglishName;
//					$thisChineseName = $lu->ChineseName;
//					
//					$thisStudentInfoArr = $lreportcard->Get_Student_Class_ClassLevel_Info($thisStudentID);
//					$thisClassName = $thisStudentInfoArr[0]['ClassName'];
//					$thisClassNumber = $thisStudentInfoArr[0]['ClassNumber'];
//					
//					$thisClassLevelName = $thisStudentInfoArr[0]['ClassLevelName'];
//						//debug_pr($StudentInfoArr);
//					if(!isset($SubjList[$thisClassLevelName]))
//					{
//						$ExportHeaderArr = array();
//						$ExportHeaderArr[] = $eReportCard['FormName'];
//						$ExportHeaderArr[] = $eReportCard['Class'];
//						$ExportHeaderArr[] = $eReportCard['Template']['StudentInfo']['ClassNo'];
//						$ExportHeaderArr[] = $eReportCard['StudentNameEn'];
//						$ExportHeaderArr[] = $eReportCard['StudentNameCh'];						
//					}
//					
//					$ExportRowArr[] = $thisClassLevelName;
//					$ExportRowArr[] = $thisClassName;
//					$ExportRowArr[] = $thisClassNumber;
//					$ExportRowArr[] = $thisEnglishName;
//					$ExportRowArr[] = $thisChineseName;
//					
//					foreach((array)$thisSummaryDetail["SubjectMarks"] as $thisSubjCode => $thisSubjDetail)
//					{
//						if(!isset($SubjList[$thisClassLevelName]))
//						{
//							$ExportHeaderArr[] = $thisSubjCode." ".$eReportCard["ExaminationSummary"]['Mark'];
//							$ExportHeaderArr[] = $thisSubjCode." ".$eReportCard["ExaminationSummary"]['Grade'];
//							$ExportHeaderArr[] = $thisSubjCode." ".$eReportCard["ExaminationSummary"]['Rank'];
//						}
//						
//						$ExportRowArr[] = $thisSubjDetail['Mark'];
//						$ExportRowArr[] = $thisSubjDetail['Grade'];
//						$ExportRowArr[] = $thisSubjDetail['Rank'];
//					}
//					
//					if(!isset($SubjList[$thisClassLevelName]))
//					{
//						$ExportHeaderArr[] = $eReportCard['ExaminationSummary']['Conduct'];
//						$ExportHeaderArr[] = $eReportCard['ExaminationSummary']['AverageMark'];
//						$ExportHeaderArr[] = $eReportCard['ExaminationSummary']['Position'];
//						//$ExportHeaderArr[] = $eReportCard['ExaminationSummary']['Late'];
//						$ExportHeaderArr[] = $eReportCard['ExaminationSummary']['Absent'];
//						$ExportHeaderArr[] = $eReportCard['ExaminationSummary']['Cd1'];
//						$ExportHeaderArr[] = $eReportCard['ExaminationSummary']['Cd2'];
//						$ExportHeaderArr[] = $eReportCard['ExaminationSummary']['Cd3'];
//						$ExportHeaderArr[] = $eReportCard['ExaminationSummary']['Cd4'];
//						$SubjList[$thisClassLevelName] = true;
//					}
//					
//					
//					$OtherInfoAry = array();
//					if(is_array($thisSummaryDetail["OtherInfo"])) 
//						$OtherInfoAry = $thisSummaryDetail["OtherInfo"];
//						
////					$ConductAry = $lreportcard->Get_eDiscipline_Data($TermID, $thisStudentID);
////					foreach((array) $ConductAry as $key => $val)
////					{
////						$ConductAry[$key] = trim($val)?$lreportcard->Get_Conduct_Wordings($val):' - ';
////					}
//					
//					$DisAry = array("Diligence","Discipline","Manner","Sociability");
//					foreach((array) $DisAry as $key)
//					{
//						$ConductAry[$key] = trim($OtherInfoAry[$key])?$lreportcard->Get_Conduct_Wordings(trim($OtherInfoAry[$key])):" - ";
//					}
//					
//					$ExportRowArr[] = $OtherInfoAry["Conduct"];
//					$ExportRowArr[] = $thisSummaryDetail["GrandAverage"];
//					$ExportRowArr[] = $thisSummaryDetail["OrderMerit"];
//					//$ExportRowArr[] = $OtherInfoAry["Time Late"];
//					$ExportRowArr[] = $OtherInfoAry["Days Absent"];
//					$ExportRowArr[] = $ConductAry["Diligence"];
//					$ExportRowArr[] = $ConductAry["Discipline"];
//					$ExportRowArr[] = $ConductAry["Manner"];
//					$ExportRowArr[] = $ConductAry["Sociability"];
//					
//					$ExportClassArr[] = $ExportRowArr;		
//							
//				}
//				array_unshift($ExportClassArr,$ExportHeaderArr);
//				$ExportClassArr[] = array("");
//				$ExportContentArr = array_merge($ExportContentArr,$ExportClassArr);
//			}
//			
//			
//			// Title of the grandmarksheet
//			//$ReportTitle = str_replace(" ", "_", $ReportTitle);
//			$filename = "examiniation_summary.csv";
//			
//			$export_content = $lexport->GET_EXPORT_TXT($ExportContentArr,'');
//			
//			intranet_closedb();
//			
//			// Output the file to user browser
//			$lexport->EXPORT_FILE($filename, $export_content);
//		}
	}
}


?>