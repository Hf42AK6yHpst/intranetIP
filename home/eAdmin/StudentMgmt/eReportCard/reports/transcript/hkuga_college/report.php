<?php
// Editing by Bill

/**************************************************
 * 	Modification log
 *  20180226 Bill	[2017-0901-1525-31265]
 * 		- Create file
 * ***********************************************/

// ini_set('display_errors',1);
// error_reporting(E_ALL ^ E_NOTICE);

// $DebugMode = true;

function convertDateFormat($date)
{
    global $emptySymbol;
    
    $convertedDate = $emptySymbol;
    if (!is_date_empty($date)) {
        $convertedDate = date('d M Y', strtotime($date));
    }
    if($convertedDate == '01 Jan 1970') {
        $convertedDate = $emptySymbol;
    }
    
    return $convertedDate;
}

function returnTranscriptCSS()
{
    $css = '';
    $css = '<head>
			<style type="text/css">
                table {
                    line-height: 1.40;
                }
                table td.report-title {
                    font-family: "balthazar";
                    font-size: 16pt;
                    text-align: center;
                    vertical-align: middle;
                    padding: 15px;
                }
                table.table-stud {
                    padding-left: 18px;
                    padding-bottom: 14px;
                }
                table.table-stud td.ch_font {
	                font-family: "msjh";
                }
                table.table-stud td {
                    font-size: 13px;
	                font-family: "Times New Roman";
                    vertical-align: middle;
                }
                table.table-Junior, table.table-Junior tr, table.table-Junior th, table.table-Junior td,
                table.table-Senior, table.table-Senior tr, table.table-Senior th, table.table-Senior td {
                    font-size: 12px;
                    font-weight: bold;
	                font-family: "Times New Roman";
                    text-align: center;
                    vertical-align: middle;
                    border-collapse: collapse;
                    padding-top: 4px;
                    padding-bottom: 2px;
                }
                table.table-remark {
                    padding-top: 30px;
                }
                table.table-remark td {
                    font-size: 12px;
                    font-weight: normal;
	                font-family: "Times New Roman";
                    vertical-align: top;
                }
                table.table-remark td.ch_font {
	                font-family: "Times New Roman", "msjh";
                }
                div.sign-space {
                    margin: 0;
                    width: 202mm;
                }
                div.sign-space td {
                    font-size: 14px;
	                font-family: "Times New Roman";
                    line-height: 1.65;
                    text-align: center;
                    vertical-align: top;
                    padding: 0px;
                    padding-bottom: 5px;
                }
                div.sign-space tr.sign-line td {
                    line-height: 1;
                    padding: 0px;
                }
                div.sign-space tr.end-line td {
                    padding-top: 5px;
                }
            </style>';
    
    return $css;
}

function returnTranscriptStudentInfoTable($tableKeyAry, $dataAry)
{
    global $Lang, $emptySymbol;
    
    $table = '';
    $table .= '<tr><td class="report-title" colspan="2"><span style="font-size:18pt;">T</span>' . $Lang['eReportCard']['ReportArr']['TranscriptArr']['ReportTitleEn'] . '</td></tr>' . "\r\n";
    $table .= '<tr>' . "\r\n";
    
    foreach ($tableKeyAry as $thisTableKeyAry)
    {
        $table .= ' <td width="50%">
                        <table class="table-stud" width="100%">
                            <tr>
                                <td width="44%"></td>
                                <td width="3%"></td>
                                <td width="53%"></td>
                            </tr>';
        
        foreach ($thisTableKeyAry as $thisKey)
        {
            if($thisKey == 'DateOfGraduation' && $dataAry[$thisKey] == $emptySymbol && $dataAry['DateOfLeave'] != $emptySymbol) {
                $thisKey = 'DateOfLeave';
            }
            $table .= returnTranscriptStudentInfo($thisKey, $dataAry[$thisKey]);
        }
        
        $table .= '      </table>
                    </td>';
    }
    
    $table .= '</tr>' . "\r\n";
    
    return $table;
}

function returnTranscriptStudentInfo($key, $val)
{
    global $Lang;
    
    $font_class = $key == "ChineseName" ? "ch_font" : "";
    $val = $val == "" ? $emptySymbol : $val;
    
    $row = '';
    $row .= '<tr>';
        $row .= '<td>' . $Lang['eReportCard']['ReportArr']['TranscriptArr'][$key . 'En'] . '</td>';
        $row .= '<td style="text-align: left">:</td>';
        $row .= '<td class="'.$font_class.'">' . $val . '</td>';
    $row .= '</tr>';
    
    return $row;
}

// function drawResultTable($arr, $form){
// $resultTable = '<table border="1" class="table-'.$form.'" style="">';
// // start of table head
// $resultTable .= drawResultHead($arr['year'], $arr['form'], $form);
// // end of table head
//
// // start of table body
// $resultTable .= drawResultBody($arr['subjects']);
// // end of table body
//
// return $resultTable . '</table>';
// }
function returnTranscriptReportHeader($yearAry, $levelAry, $levelType, $emptyColNum)
{
    global $Lang;
    
    $emptyBeforeColSize = $emptyColNum[$levelType]['first'] ? $emptyColNum[$levelType]['first'] : 0;
    $emptyAfterColSize = $emptyColNum[$levelType]['last'] ? $emptyColNum[$levelType]['last'] : 0;
    $headerColSize = count((array) $yearAry) + $emptyBeforeColSize + $emptyAfterColSize;
    
    $header .= '<tr>';
        $header .= '<td rowspan="3" width="30%">';
            $header .= $Lang['eReportCard']['ReportArr']['TranscriptArr']['SubjectsEn'] . '<br/>(' . $Lang['eReportCard']['ReportArr']['TranscriptArr'][$levelType . 'FormsEn'] . ')';
        $header .= '</td>';
        $header .= '<td colspan="' . $headerColSize . '">';
            $header .= $Lang['eReportCard']['ReportArr']['TranscriptArr']['YearGradeEn'];
        $header .= '</td>';
    $header .= '</tr>';
    
    $header .= '<tr>';
        $header .= returnTranscriptReportHeaderYearRow($yearAry, $emptyBeforeColSize, $emptyAfterColSize);
    $header .= '</tr>';
    
    $header .= '<tr>';
        $header .= returnTranscriptReportHeaderFormRow($levelAry, $emptyBeforeColSize, $emptyAfterColSize);
    $header .= '</tr>';
    
    return $header;
}

function returnTranscriptReportHeaderYearRow($year, $emptyBeforeColNum, $emptyAfterColNum)
{
    global $emptySymbol;
    
    $yearCount = count((array) $year);
    if ($yearCount > 0 || $emptyBeforeColNum > 0 || $emptyAfterColNum > 0) {
        $header_width = 70 / ($yearCount + $emptyBeforeColNum + $emptyAfterColNum);
    }
    
    $headerRow = '';
    for ($i = 0; $i < $emptyBeforeColNum; $i ++)
    {
        $headerRow .= '<td width="' . $header_width . '%">' . $emptySymbol . '</td>';
    }
    for ($i = 0; $i < $yearCount; $i ++)
    {
        if ($year[$i] == '') {
            $year[$i] = $emptySymbol;
        }
        $headerRow .= '<td width="' . $header_width . '%">' . $year[$i] . '</td>';
    }
    for ($i = 0; $i < $emptyAfterColNum; $i ++)
    {
        $headerRow .= '<td width="' . $header_width . '%">' . $emptySymbol . '</td>';
    }
    
    return $headerRow;
}

function returnTranscriptReportHeaderFormRow($form, $emptyBeforeColNum, $emptyAfterColNum)
{
    global $Lang, $emptySymbol;
    
    $formCount = count((array) $form);
    if ($formCount > 0 || $emptyBeforeColNum > 0 || $emptyAfterColNum > 0) {
        $header_width = 70 / ($formCount + $emptyBeforeColNum + $emptyAfterColNum);
    }
    
    $headerRow = '';
    for ($i = 0; $i < $emptyBeforeColNum; $i ++)
    {
        $headerRow .= '<td width="' . $header_width . '%">' . $emptySymbol . '</td>';
    }
    for ($i = 0; $i < $formCount; $i ++)
    {
        $form[$i] = $form[$i] == '' ? $emptySymbol : ($Lang['eReportCard']['ReportArr']['TranscriptArr']['SecondaryEn'] . ' ' . $form[$i]);
        $headerRow .= '<td width="' . $header_width . '%">' . $form[$i] . '</td>';
    }
    for ($i = 0; $i < $emptyAfterColNum; $i ++)
    {
        $headerRow .= '<td width="' . $header_width . '%">' . $emptySymbol . '</td>';
    }
    
    return $headerRow;
}
// function drawResultBody($subjects){
// // tag start
// $resultBody = '<tbody>';
// // loop for getting rows of subjects
// foreach($subjects as $key => $value){
// $resultBody .= printResultBody($key, $value);
// }
// // tag end and return
// return $resultBody . '</tbody>';
// }
// function printResultBody($key, $value){
// $rBody = '<tr style="padding: 1mm; padding-bottom: 0; font-size: 90%;">';
// $rBody .= '<td style="">' . $key . '</td>';
// $rBody .= showResultBody($value);
// return $rBody . '</tr>';
// }
// function showResultBody($value){
// $text = '';
// for($i=0; $i<=2; $i++){
// if($value[$i] == '') $value[$i] = '--';
// $text .= '<td style="text-align: center;">'.$value[$i].'</td>';
// }
// return $text;
// }

// ## Set Common Variables
$emptySymbol = '--';
$subjectNameStringLengthAdjustLimit = 30;
$StudentInfoKeyAry = array();
$StudentInfoKeyAry[0] = array(
    'HKID',
    'EnglishName',
    'ChineseName',
    'Gender'
);
$StudentInfoKeyAry[1] = array(
    'DateOfBirth',
    'DateOfAdmission',
    'DateOfGraduation',
    'DateOfIssue'
);

// ## POST data
$studentSource = standardizeFormPostValue($_POST['studentSource']);
// $yearId = standardizeFormPostValue($_POST['yearId']);
// $yearClassIdAry = $_POST['yearClassIdAry'];
$userLogin = standardizeFormPostValue($_POST['userLogin']);
$studentIdAry = $_POST['studentIdAry'];
$dateOfGraduation = standardizeFormPostValue($_POST['dateOfGraduation']);
$dateOfLeaving = standardizeFormPostValue($_POST['dateOfLeaving']);
$dateOfIssue = standardizeFormPostValue($_POST['dateOfIssue']);
$princialName = standardizeFormPostValue($_POST['princialName']);

// ## Get Target Student (Input Source: User Login)
if ($studentSource == 'userLogin')
{
    $luser = new libuser('', $userLogin);
    
    $studentIdAry = array();
    $studentIdAry[] = $luser->UserID;
}

// ## Get Level Type (Junior / Senior)
$levelTypeAry = array(
    'junior',
    'senior'
);
$numOfLevelKey = count($levelTypeAry);

// ## Get Subject Code
$subjectCodeAry = array();
$subjectCodeAry['id'] = $lreportcard->GET_SUBJECTS_CODEID_MAP(1, 0);
$subjectCodeAry['code'] = $lreportcard->GET_SUBJECTS_CODEID_MAP(1, 1);

// ## Load all Target Student
$luserObj = new libuser('', '', $studentIdAry);
$numOfStudent = count($studentIdAry);

// ## Get Student Class History Info
$studentClassHistoryAry = $lreportcard->Get_Student_From_School_Settings($AcademicYearIDAry = '', $studentIdAry);
$numOfData = count($studentClassHistoryAry);

// ## Analyze Student Class History Info
$studentDataAssoAry = array();
for ($i = 0; $i < $numOfData; $i ++)
{
    $_studentId = $studentClassHistoryAry[$i]['UserID'];
    $_formId = $studentClassHistoryAry[$i]['YearID'];
    $_formName = $studentClassHistoryAry[$i]['YearName'];
    $_formNumber = $lreportcard->GET_FORM_NUMBER($_formId, 1);
    $_formLevelKey = ($_formNumber < 4) ? 'junior' : 'senior';
    $_academicYearId = $studentClassHistoryAry[$i]['AcademicYearID'];
    $_academicYearName = $studentClassHistoryAry[$i]['AcademicYearNameEn'];
    
    // Handle S1-Testing in client site
    if ($_formId == 10)
    {
        continue;
    }
    
    // Define data first
    if (! isset($studentDataAssoAry[$_studentId]))
    {
        $studentDataAssoAry[$_studentId]['firstFormNumber'] = $_formNumber;
        $studentDataAssoAry[$_studentId]['firstAcademicYearId'] = $_academicYearId;
        
        $studentDataAssoAry[$_studentId]['classHistoryAry']['junior'] = array();
        $studentDataAssoAry[$_studentId]['classHistoryAry']['junior']['firstFormId'] = '';
        $studentDataAssoAry[$_studentId]['classHistoryAry']['junior']['firstFormNumber'] = '';
        $studentDataAssoAry[$_studentId]['classHistoryAry']['junior']['firstAcademicYearId'] = '';
        $studentDataAssoAry[$_studentId]['classHistoryAry']['junior']['lastFormId'] = '';
        $studentDataAssoAry[$_studentId]['classHistoryAry']['junior']['lastFormNumber'] = '';
        $studentDataAssoAry[$_studentId]['classHistoryAry']['junior']['lastAcademicYearId'] = '';
        
        $studentDataAssoAry[$_studentId]['classHistoryAry']['senior'] = array();
        $studentDataAssoAry[$_studentId]['classHistoryAry']['senior']['firstFormId'] = '';
        $studentDataAssoAry[$_studentId]['classHistoryAry']['senior']['firstFormNumber'] = '';
        $studentDataAssoAry[$_studentId]['classHistoryAry']['senior']['firstAcademicYearId'] = '';
        $studentDataAssoAry[$_studentId]['classHistoryAry']['senior']['lastFormId'] = '';
        $studentDataAssoAry[$_studentId]['classHistoryAry']['senior']['lastFormNumber'] = '';
        $studentDataAssoAry[$_studentId]['classHistoryAry']['senior']['lastAcademicYearId'] = '';
    }
    
    $studentDataAssoAry[$_studentId]['classHistoryAry'][$_formLevelKey]['levelAcademicYearAssoAry'][$_academicYearId]['formId'] = $_formId;
    $studentDataAssoAry[$_studentId]['classHistoryAry'][$_formLevelKey]['levelAcademicYearAssoAry'][$_academicYearId]['formName'] = $_formName;
    $studentDataAssoAry[$_studentId]['classHistoryAry'][$_formLevelKey]['levelAcademicYearAssoAry'][$_academicYearId]['formNumber'] = $_formNumber;
    $studentDataAssoAry[$_studentId]['classHistoryAry'][$_formLevelKey]['levelAcademicYearAssoAry'][$_academicYearId]['academicYearId'] = $_academicYearId;
    $studentDataAssoAry[$_studentId]['classHistoryAry'][$_formLevelKey]['levelAcademicYearAssoAry'][$_academicYearId]['academicYearName'] = $_academicYearName;
    $studentDataAssoAry[$_studentId]['classHistoryAry'][$_formLevelKey]['levelAcademicYearAssoAry'][$_academicYearId]['studentStudied'] = true;
}

// ## Remove year data after student left school
foreach ((array) $studentDataAssoAry as $_studentId => $_studentDataAry)
{
    // Find student last year in school
    $_studentLastStudiedAcademicYearId = '';
    for ($i = 0; $i < $numOfLevelKey; $i ++)
    {
        $__levelKey = $levelTypeAry[$i];
        foreach ((array) $_studentDataAry['classHistoryAry'][$__levelKey]['levelAcademicYearAssoAry'] as $___academicYearId => $___academicYearInfoAry)
        {
            $___studentStudied = $___academicYearInfoAry['studentStudied'];
            if ($___studentStudied) {
                $_studentLastStudiedAcademicYearId = $___academicYearId;
            }
        }
    }
    
    // Remove year data after last studied year
    $_startToDelete = false;
    for ($i = 0; $i < $numOfLevelKey; $i ++)
    {
        $__levelKey = $levelTypeAry[$i];
        foreach ((array) $_studentDataAry['classHistoryAry'][$__levelKey]['levelAcademicYearAssoAry'] as $___academicYearId => $___academicYearInfoAry)
        {
            if ($_startToDelete) {
                unset($studentDataAssoAry[$_studentId]['classHistoryAry'][$__levelKey]['levelAcademicYearAssoAry'][$___academicYearId]);
            }
            
            if ($___academicYearId == $_studentLastStudiedAcademicYearId) {
                $_startToDelete = true;
            }
        }
    }
}

// ## Remove year data if student not studied
foreach ((array) $studentDataAssoAry as $_studentId => $_studentDataAry)
{
    for ($i = 0; $i < $numOfLevelKey; $i ++)
    {
        $__levelKey = $levelTypeAry[$i];
        $__studentNotStudyingAllForm = true;
        foreach ((array) $_studentDataAry['classHistoryAry'][$__levelKey]['levelAcademicYearAssoAry'] as $___academicYearId => $___academicYearInfoAry)
        {
            $___isStudentStudiedThisYear = $___academicYearInfoAry['studentStudied'];
            if ($___isStudentStudiedThisYear) {
                $__studentNotStudyingAllForm = false;
                break;
            }
        }
        
        if ($__studentNotStudyingAllForm) {
            unset($studentDataAssoAry[$_studentId]['classHistoryAry'][$__levelKey]);
        }
    }
}

// ## Get year data
foreach ((array) $studentDataAssoAry as $_studentId => $_studentDataAry)
{
    $_juniorAry = $_studentDataAry['classHistoryAry']['junior']['levelAcademicYearAssoAry'];
    foreach ((array) $_juniorAry as $__academicYearId => $__academicYearInfoAry)
    {
        $__formId = $__academicYearInfoAry['formId'];
        $__formNumber = $__academicYearInfoAry['formNumber'];
        
        if ($studentDataAssoAry[$_studentId]['classHistoryAry']['junior']['firstFormId'] === '')
        {
            $studentDataAssoAry[$_studentId]['firstFormNumber'] = $__formNumber;
            $studentDataAssoAry[$_studentId]['firstAcademicYearId'] = $__academicYearId;
            
            $studentDataAssoAry[$_studentId]['classHistoryAry']['junior']['firstFormId'] = $__formId;
            $studentDataAssoAry[$_studentId]['classHistoryAry']['junior']['firstFormNumber'] = $__formNumber;
            $studentDataAssoAry[$_studentId]['classHistoryAry']['junior']['firstAcademicYearId'] = $__academicYearId;
        }
        
        $studentDataAssoAry[$_studentId]['classHistoryAry']['junior']['lastFormId'] = $__formId;
        $studentDataAssoAry[$_studentId]['classHistoryAry']['junior']['lastFormNumber'] = $__formNumber;
        $studentDataAssoAry[$_studentId]['classHistoryAry']['junior']['lastAcademicYearId'] = $__academicYearId;
    }
    
    $_seniorAry = $_studentDataAry['classHistoryAry']['senior']['levelAcademicYearAssoAry'];
    foreach ((array) $_seniorAry as $__academicYearId => $__academicYearInfoAry)
    {
        $__formId = $__academicYearInfoAry['formId'];
        $__formNumber = $__academicYearInfoAry['formNumber'];
        
        if ($studentDataAssoAry[$_studentId]['classHistoryAry']['senior']['firstFormId'] === '')
        {
            $studentDataAssoAry[$_studentId]['classHistoryAry']['senior']['firstFormId'] = $__formId;
            $studentDataAssoAry[$_studentId]['classHistoryAry']['senior']['firstFormNumber'] = $__formNumber;
            $studentDataAssoAry[$_studentId]['classHistoryAry']['senior']['firstAcademicYearId'] = $__academicYearId;
        }
        
        $studentDataAssoAry[$_studentId]['classHistoryAry']['senior']['lastFormId'] = $__formId;
        $studentDataAssoAry[$_studentId]['classHistoryAry']['senior']['lastFormNumber'] = $__formNumber;
        $studentDataAssoAry[$_studentId]['classHistoryAry']['senior']['lastAcademicYearId'] = $__academicYearId;
    }
}

// ## Handle empty year column and level table width
$emptyColSizeAry = array();
$levelTableWidth = array();
foreach ((array) $studentDataAssoAry as $_studentId => $_studentDataAry)
{
    // Empty year columns
    // Junior Level (empty before)
    $firstFormNumber = $studentDataAssoAry[$_studentId]['classHistoryAry']['junior']['firstFormNumber'];
    if (! isset($emptyColSizeAry[$_studentId]['junior']['first']))
    {
        if ($firstFormNumber > 1) {
            $emptyColSizeAry[$_studentId]['junior']['first'] = ($firstFormNumber - 1);
        }
        else if ($firstFormNumber == "") {
            $emptyColSizeAry[$_studentId]['junior']['first'] = 3;
        }
    }
    // Junior Level (empty after)
    $lastFormNumber = $studentDataAssoAry[$_studentId]['classHistoryAry']['junior']['lastFormNumber'];
    if (! isset($emptyColSizeAry[$_studentId]['junior']['last']))
    {
        if ($lastFormNumber > 0 && $lastFormNumber < 3) {
            $emptyColSizeAry[$_studentId]['junior']['last'] = abs($lastFormNumber - 3);
        }
    }
    
    // Senior Level (empty before)
    $firstFormNumber = $studentDataAssoAry[$_studentId]['classHistoryAry']['senior']['firstFormNumber'];
    if (! isset($emptyColSizeAry[$_studentId]['senior']['first']))
    {
        if ($firstFormNumber > 4) {
            $emptyColSizeAry[$_studentId]['senior']['first'] = ($firstFormNumber - 4);
        }
        else if ($firstFormNumber == "") {
            $emptyColSizeAry[$_studentId]['senior']['first'] = 3;
        }
    }
    // Senior Level (empty after)
    $lastFormNumber = $studentDataAssoAry[$_studentId]['classHistoryAry']['senior']['lastFormNumber'];
    if (! isset($emptyColSizeAry[$_studentId]['senior']['last']))
    {
        if ($lastFormNumber > 3 && $lastFormNumber < 6) {
            $emptyColSizeAry[$_studentId]['senior']['last'] = abs($lastFormNumber - 6);
        }
    }
    
    // Junior / Senior Level Table width
    // Junior Table columns
    $_juniorTableCols = count((array) $studentDataAssoAry[$_studentId]['classHistoryAry']['junior']['levelAcademicYearAssoAry']);
    if ($_juniorTableCols > 0)
    {
        $_juniorTableCols += $emptyColSizeAry[$_studentId]['junior']['first'] ? $emptyColSizeAry[$_studentId]['junior']['first'] : 0;
        $_juniorTableCols += $emptyColSizeAry[$_studentId]['junior']['last'] ? $emptyColSizeAry[$_studentId]['junior']['last'] : 0;
    }
    
    // Senior Table columns
    $_SeniorTableCols = count((array) $studentDataAssoAry[$_studentId]['classHistoryAry']['senior']['levelAcademicYearAssoAry']);
    if ($_SeniorTableCols > 0)
    {
        $_SeniorTableCols += $emptyColSizeAry[$_studentId]['senior']['first'] ? $emptyColSizeAry[$_studentId]['senior']['first'] : 0;
        $_SeniorTableCols += $emptyColSizeAry[$_studentId]['senior']['last'] ? $emptyColSizeAry[$_studentId]['senior']['last'] : 0;
    }
    
    // Get Level Table width
    $levelTableWidth[$_studentId]['junior'] = 49;
    $levelTableWidth[$_studentId]['senior'] = 49;
    if ($_juniorTableCols > 3 && $_SeniorTableCols == 3) {
        $levelTableWidth[$_studentId]['junior'] = 54;
        $levelTableWidth[$_studentId]['senior'] = 44;
    }
    else if ($_juniorTableCols == 3 && $_SeniorTableCols > 3) {
        $levelTableWidth[$_studentId]['junior'] = 44;
        $levelTableWidth[$_studentId]['senior'] = 54;
    }
    else if ($_juniorTableCols == 0) {
        $levelTableWidth[$_studentId]['junior'] = 0;
        $levelTableWidth[$_studentId]['senior'] = 100;
    }
    else if ($_SeniorTableCols == 0) {
        $levelTableWidth[$_studentId]['junior'] = 100;
        $levelTableWidth[$_studentId]['senior'] = 0;
    }
}

// ## Get all years
$allAcademicYearFormInfoAry = array();
foreach ((array) $studentDataAssoAry as $_studentId => $_studentDataAry)
{
    foreach ((array) $_studentDataAry['classHistoryAry'] as $__formLevelKey => $__formLevelAry)
    {
        foreach ((array) $__formLevelAry['levelAcademicYearAssoAry'] as $___academicYearId => $___academicYearInfoAry)
        {
            $____formId = $___academicYearInfoAry['formId'];
            if ($____formId != '' && ! in_array($____formId, (array) $allAcademicYearFormInfoAry[$___academicYearId])) {
                $allAcademicYearFormInfoAry[$___academicYearId][] = $____formId;
            }
        }
    }
}

// ## Get Report related data
$markAry = array();
$subjectAry = array();
$subjectGradingSchemeAry = array();
$transcriptRemarksAry = array();
foreach ((array) $allAcademicYearFormInfoAry as $_academicYearId => $_formIdAry)
{
    $_lreportcard = new libreportcard($_academicYearId);
    $isActiveYear = $_academicYearId == $lreportcard->GET_ACTIVE_YEAR_ID();
    $SemesterList = getSemesters($_academicYearId, 0);
    
    $_numOfForm = count($_formIdAry);
    for ($i = 0; $i < $_numOfForm; $i ++)
    {
        $__formId = $_formIdAry[$i];
        $__formNumber = $_lreportcard->GET_FORM_NUMBER($__formId, 1);
        if ($__formNumber == 6) {
            $__reportSemesterId = $SemesterList[0]["YearTermID"];
        }
        else {
            $__reportSemesterId = $SemesterList[1]["YearTermID"];
        }
        
        ### Normal Handling
        // Get 2nd Term Report (S6 : 1st Term Report)
        $__reportInfoAry = $_lreportcard->returnReportTemplateBasicInfo($ReportID = '', $others = '', $__formId, $__reportSemesterId, $isMainReport = 1);
        $__reportId = $__reportInfoAry['ReportID'];
        
        // Get Target Student Marks
        $cons = " AND a.StudentID IN ('" . implode("', '", (array) $studentIdAry) . "')";
        $markAry[$_academicYearId][$__formId]['orig'] = $_lreportcard->getMarksCommonIpEj($__reportId, $StudentID = '', $cons, $ParentSubjectOnly = 0, $includeAdjustedMarks = 1, $OrderBy = '', $SubjectID = '', $ReportColumnID = '0');
        
        // Get form Subjects
        $subjectAry[$_academicYearId][$__formId] = $_lreportcard->returnSubjectwOrder($__formId, $ParForSelection = 0, $TeacherID = '', $SubjectFieldLang = 'en', $ParDisplayType = 'Desc', $ExcludeCmpSubject = 0, $__reportId);
        
        // Get form Grading Scheme Info
        $subjectGradingSchemeAry[$_academicYearId][$__formId]['orig'] = $_lreportcard->GET_SUBJECT_FORM_GRADING($__formId, $SubjectID = '', $withGrandResult = 0, $returnAsso = 1, $__reportId);
        
        // Get current year Remarks
        if($isActiveYear)
        {
            $transcriptRemarksAry = $lreportcard->getReportOtherInfoData($__reportId);
            $targetSemester = $__reportSemesterId;
        }
        
        ### Extra Handling if no Subject Year Grade
        // Get 1st Term Report
        $__extraReportSemesterId = $SemesterList[0]["YearTermID"];
        $__extraReportInfoAry = $_lreportcard->returnReportTemplateBasicInfo($ReportID = '', $others = '', $__formId, $__extraReportSemesterId, $isMainReport = 1);
        $__extraReportId = $__extraReportInfoAry['ReportID'];
        
        // Get Target Student Marks
        $markAry[$_academicYearId][$__formId]['extra'] = $_lreportcard->getMarksCommonIpEj($__extraReportId, $StudentID = '', $cons, $ParentSubjectOnly = 0, $includeAdjustedMarks = 1, $OrderBy = '', $SubjectID = '', $ReportColumnID = '0');
        
        // Get form Subjects if empty
        if(empty($subjectAry[$_academicYearId][$__formId])) {
            $subjectAry[$_academicYearId][$__formId] = $_lreportcard->returnSubjectwOrder($__formId, $ParForSelection = 0, $TeacherID = '', $SubjectFieldLang = 'en', $ParDisplayType = 'Desc', $ExcludeCmpSubject = 0, $__extraReportId);
        }
        
        // Get form Grading Scheme Info
        $subjectGradingSchemeAry[$_academicYearId][$__formId]['extra'] = $_lreportcard->GET_SUBJECT_FORM_GRADING($__formId, $SubjectID = '', $withGrandResult = 0, $returnAsso = 1, $__extraReportId);
    }
}

// ## Analyze Student Mark Info
foreach ((array) $studentDataAssoAry as $_studentId => $_dataAry)
{
    for ($i=0; $i<$numOfLevelKey; $i++)
    {
        $__levelKey = $levelTypeAry[$i];
        $__levelDataAry = $studentDataAssoAry[$_studentId]['classHistoryAry'][$__levelKey];
        $__levelKeyFirstFormId = $__levelDataAry['firstFormId'];
        $__levelKeyFirstAcademicYearId = $__levelDataAry['firstAcademicYearId'];
        
        $__studentClassHistoryAry = $__levelDataAry['levelAcademicYearAssoAry'];
        
        // Merge all studied subjects in same level
        $__mergedSubjectAry = array();
        foreach ((array) $__studentClassHistoryAry as $___academicYearId => $___academicYearInfoAry)
        {
            $___formId = $___academicYearInfoAry['formId'];
            $__mergedSubjectAry = $__mergedSubjectAry + (array) $subjectAry[$___academicYearId][$___formId];
        }
        
        foreach ((array) $__mergedSubjectAry as $___parentSubjectId => $___cmpSubjectAry)
        {
            foreach ((array) $___cmpSubjectAry as $____cmpSubjectID => $____cmpSubjectName)
            {
                $____isComponentSubject = ($____cmpSubjectID == 0) ? false : true;
                $____subjectId = ($____isComponentSubject) ? $____cmpSubjectID : $___parentSubjectId;
                
                // Get Student Subject Marks
                foreach ((array) $__studentClassHistoryAry as $_____academicYearId => $_____academicYearDataAry)
                {
                    $_____formId = $_____academicYearDataAry['formId'];
                    $_____studentStudiesThisForm = $_____academicYearDataAry['studentStudied'];
                    
                    // Get Subject Grading Scheme
                    $_____subjectGradingSchemeInfoAry = $subjectGradingSchemeAry[$_____academicYearId][$_____formId]['orig'][$____subjectId];
                    $_____subjectScaleDisplay = $_____subjectGradingSchemeInfoAry['ScaleDisplay'];
                    $studentDataAssoAry[$_studentId]['classHistoryAry'][$__levelKey]['levelAcademicYearAssoAry'][$_____academicYearId]['markAry'][$____subjectId]['scaleDisplay'] = $_____subjectScaleDisplay;
                    
                    /*
                     * - Subject Mark : Show "--"
                     * 1. Not studied in this year
                     * 2. No Grading Settings
                     * 3. Subject Marks Empty
                     */
                    $isNeedToCheckExtraMarkAry = false;
                    if (!$_____studentStudiesThisForm || $_____subjectScaleDisplay == '' || !isset($markAry[$_____academicYearId][$_____formId]['orig'][$_studentId][$____subjectId][$thisReportColumnID = 0]))
                    {
                        $_____markDisplay = $emptySymbol;
                        $isNeedToCheckExtraMarkAry = !(!$_____studentStudiesThisForm || $_____subjectScaleDisplay == '');
                    }
                    else
                    {
                        // $_____mark = my_round($markAry[$_____academicYearId][$_____formId][$_studentId][$____subjectId][$thisReportColumnID=0]['Mark'], 1);
                        $_____grade = $markAry[$_____academicYearId][$_____formId]['orig'][$_studentId][$____subjectId][$thisReportColumnID = 0]['Grade'];
                        
                        $_____markDisplay = '';
                        if ($_____grade != '' && $lreportcard->Check_If_Grade_Is_SpecialCase($_____grade)) {
                            $_____markDisplay = $emptySymbol;
                        }
                        else {
                            $_____markDisplay = $_____grade;
                        }
                        
                        if ($_____markDisplay == '') {
                            $_____markDisplay = $emptySymbol;
                        }
						$isNeedToCheckExtraMarkAry = ($_____markDisplay == $emptySymbol);
                    }
                    $isNeedToCheckExtraMarkAry = $__levelKey == 'junior' && $isNeedToCheckExtraMarkAry;
                    
                    // Get Extra Subject Grade if needed
                    if($isNeedToCheckExtraMarkAry && $____isComponentSubject)
                    {
                        // Get Subject Year Grade (Code : Z999)
                        $____parentSubjectCode = $subjectCodeAry['id'][$___parentSubjectId]['CODEID'];
                        $____yearGradeCode = $eRCTemplateSetting['Report']['DataTransferCompAssessmentAry']["0"];
                        $____gradeSubjectId = $subjectCodeAry['code'][$____parentSubjectCode. '_' . $____yearGradeCode];
                        
                        // Replace Year Grade by 1st Term Exam Grade / CA Grade
                        if($____gradeSubjectId && $____gradeSubjectId == $____cmpSubjectID)
                        {
                            $____gradeCodeAry = array($eRCTemplateSetting['Report']['DataTransferCompAssessmentAry']["A2"], $eRCTemplateSetting['Report']['DataTransferCompAssessmentAry']["A1"]);
                            foreach((array)$____gradeCodeAry as $_____gradeCode)
                            {
                                // Skip > if Exam Grade / CA Grade not exist
                                $_____gradeSubjectId = $subjectCodeAry['code'][$____parentSubjectCode. '_' . $_____gradeCode];
                                if(!$_____gradeSubjectId) {
                                    continue;
                                }
                                
                                // Get Subject Grading Scheme
                                $_____subjectGradingSchemeInfoAry = $subjectGradingSchemeAry[$_____academicYearId][$_____formId]['extra'][$_____gradeSubjectId];
                                $_____subjectScaleDisplay = $_____subjectGradingSchemeInfoAry['ScaleDisplay'];
                                
                                // Check if Exam Grade / CA Grade can be applied
                                if (isset($markAry[$_____academicYearId][$_____formId]['extra'][$_studentId][$_____gradeSubjectId][$thisReportColumnID = 0]) && $_____subjectScaleDisplay != '')
                                {
                                    $_____grade = $markAry[$_____academicYearId][$_____formId]['extra'][$_studentId][$_____gradeSubjectId][$thisReportColumnID = 0]['Grade'];
                                    if ($_____grade == '' || ($_____grade != '' && $lreportcard->Check_If_Grade_Is_SpecialCase($_____grade))) {
                                        // do nothing
                                    }
                                    else {
                                        $_____markDisplay = $_____grade;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    
                    $studentDataAssoAry[$_studentId]['classHistoryAry'][$__levelKey]['levelAcademicYearAssoAry'][$_____academicYearId]['markAry'][$____subjectId]['markDisplay'] = $_____markDisplay;
                }
            }
        }
    }
}

include_once ($PATH_WRT_ROOT . "includes/libaccountmgmt.php");
$laccount = new libaccountmgmt();

// ## Build Report Content
$transcriptContentAry = array();
for ($i = 0; $i < $numOfStudent; $i ++)
{
    $_isLastStudent = false;
    if ($i == $numOfStudent - 1) {
        $_isLastStudent = true;
    }
    
    // Get Student data
    $_studentId = $studentIdAry[$i];
    $luserObj->LoadUserData($_studentId);
    $_studentPersonalData = $laccount->getUserPersonalSettingByID($_studentId);
    
    $_studentInfoAry = array();
    $_studentInfoAry["EnglishName"] = str_replace('*', '', ($luserObj->EnglishName? $luserObj->EnglishName : $emptySymbol));
    $_studentInfoAry["ChineseName"] = str_replace('*', '', ($luserObj->ChineseName? $luserObj->ChineseName: $emptySymbol));
    $_studentInfoAry["HKID"]        = $luserObj->HKID? $luserObj->HKID : $emptySymbol;
    $_studentInfoAry["Gender"]      = $luserObj->Gender? $luserObj->Gender : $emptySymbol;
    $_studentInfoAry["DateOfAdmission"] = convertDateFormat($_studentPersonalData['AdmissionDate']);
    $_studentInfoAry["DateOfBirth"]     = convertDateFormat($luserObj->DateOfBirth);
    $_studentInfoAry["DateOfGraduation"] = convertDateFormat($dateOfGraduation);
    //$_studentInfoAry["DateOfLeave"]   = convertDateFormat(trim($luserObj->Remark));
    $_studentInfoAry["DateOfLeave"]     = convertDateFormat($dateOfLeaving);
    $_studentInfoAry["DateOfIssue"]     = convertDateFormat($dateOfIssue);
    
    // Get Junior & Senior Years
    $_tableNumber = 0;
    $_reportPageAry = array();
    foreach ((array) $studentDataAssoAry[$_studentId]['classHistoryAry'] as $__levelKey => $__levelDataAry)
    {
        $__numOfLevelAcademicYear = count((array) $__levelDataAry['levelAcademicYearAssoAry']);
        $__levelAcademicYearCount = 0;
        
        foreach ((array) $__levelDataAry['levelAcademicYearAssoAry'] as $___academicYearId => $___academicYearInfoAry)
        {
            $_reportPageAry[$_tableNumber]['levelKey'] = $__levelKey;
            $_reportPageAry[$_tableNumber]['academicYearIdAry'][] = $___academicYearId;
            
            // Open new table for another level
            $__levelAcademicYearCount ++;
            if ($__levelAcademicYearCount == $__numOfLevelAcademicYear) {
                $_tableNumber ++;
            }
        }
    }
    
    // Get Display Subject
    $_reportDisplaySubjectIdAry = array();
    foreach ((array) $studentDataAssoAry[$_studentId]['classHistoryAry'] as $__levelKey => $__levelDataAry)
    {
        // Merge all studied subjects of same school level
        $__mergedSubjectAry = array();
        foreach ((array) $__levelDataAry['levelAcademicYearAssoAry'] as $___academicYearId => $___academicYearInfoAry)
        {
            $___formId = $___academicYearInfoAry['formId'];
            $__mergedSubjectAry = $__mergedSubjectAry + (array) $subjectAry[$___academicYearId][$___formId];
        }
        
        foreach ((array) $__mergedSubjectAry as $___parentSubjectId => $___cmpSubjectAry)
        {
            foreach ((array) $___cmpSubjectAry as $____cmpSubjectID => $____cmpSubjectName)
            {
                $____isComponentSubject = ($____cmpSubjectID == 0) ? false : true;
                $____subjectId = ($____isComponentSubject) ? $____cmpSubjectID : $___parentSubjectId;
                
                // No need to display Component => SKIP
                if ($____isComponentSubject) {
                    continue;
                }
                
                // Get Subject Year Grade (Code : Z999)
                $____subjectCode = $subjectCodeAry['id'][$____subjectId]['CODEID'];
                $____yearGradeCode = $eRCTemplateSetting['Report']['DataTransferCompAssessmentAry']["0"];
                $____gradeSubjectId = $subjectCodeAry['code'][$____subjectCode . '_' . $____yearGradeCode];
                $____gradeSubjectId = $____gradeSubjectId ? $____gradeSubjectId : - 999;
                
                // Use Year Grade Component Marks for checking
                $____isAllNA = true;
                $____didNotStudiedThisLevel = true;
                foreach ((array) $__levelDataAry['levelAcademicYearAssoAry'] as $_____academicYearId => $_____academicYearInfoAry)
                {
                    $_____markDisplay = $_____academicYearInfoAry['markAry'][$____gradeSubjectId]['markDisplay'];
                    $_____studentStuided = $_____academicYearInfoAry['studentStudied'];
                    
                    if ($_____studentStuided) {
                        $____didNotStudiedThisLevel = false;
                    }
                    if ($_____markDisplay != '' && $_____markDisplay != $emptySymbol) {
                        $____isAllNA = false;
                    }
                }
                
                // Not studied this level => Shows "--"
                if ($____didNotStudiedThisLevel || ! $____isAllNA) {
                    $_reportDisplaySubjectIdAry[$__levelKey][] = $____subjectId;
                }
            }
        }
    }
    
    // Get Remarks
    $_reportRemarks = '';
    if($targetSemester != '' && !empty($transcriptRemarksAry[$_studentId]) && !empty($transcriptRemarksAry[$_studentId][$targetSemester]['Remark']))
    {
        $_reportRemarks = $transcriptRemarksAry[$_studentId][$targetSemester]['Remark'];
    }
    
    // Get Report Header
    $_reportHeaderHtml = returnTranscriptStudentInfoTable($StudentInfoKeyAry, $_studentInfoAry);
    
    // Get Report Main Content
    $_reportResultHtml = '';
    foreach ((array) $_reportPageAry as $___tableNumber => $___tableDataAry)
    {
        $___levelKey = $___tableDataAry['levelKey'];
        $___academicYearIdAry = $___tableDataAry['academicYearIdAry'];
        
        // Seperator (between 2 table)
        if ($___tableNumber > 0)
        {
            $_reportResultHtml .= '<td width="2%">&nbsp;</td>';
        }
        
        // Get Table Header row
        $____yearAry = array();
        $____levelAry = array();
        $numOfAcademicYearPerRow = count((array) $___academicYearIdAry);
        for ($j = 0; $j < $numOfAcademicYearPerRow; $j ++)
        {
            $____academicYearId = $___academicYearIdAry[$j];
            if ($____academicYearId == '')
            {
                $____yearAry[] = $emptySymbol;
                $____levelAry[] = $emptySymbol;
            }
            else
            {
                $____yearAry[] = $studentDataAssoAry[$_studentId]['classHistoryAry'][$___levelKey]['levelAcademicYearAssoAry'][$____academicYearId]['academicYearName'];
                $____levelAry[] = $studentDataAssoAry[$_studentId]['classHistoryAry'][$___levelKey]['levelAcademicYearAssoAry'][$____academicYearId]['formNumber'];
            }
        }
        $_reportResultHeaderHtml = returnTranscriptReportHeader($____yearAry, $____levelAry, $___levelKey, $emptyColSizeAry[$_studentId]);
        
        // Get Table Content
        // Merge all studied subjects of same school level
        $___mergedSubjectAry = array();
        $___levelDataAry = $studentDataAssoAry[$_studentId]['classHistoryAry'][$___levelKey];
        foreach ((array) $___levelDataAry['levelAcademicYearAssoAry'] as $____academicYearId => $____academicYearInfoAry)
        {
            $____formId = $____academicYearInfoAry['formId'];
            $___mergedSubjectAry = $___mergedSubjectAry + (array) $subjectAry[$____academicYearId][$____formId];
        }
        
        // Sort merged subjects (Main Subject only)
        $___mergedMainSubjectAry = array();
        foreach ($___mergedSubjectAry as $____SubjectId => $____subjectCmpAry)
        {
            $___mergedMainSubjectAry[$____SubjectId] = $____subjectCmpAry[0];
			
            // Add prefix for Science Subject (Junior only) > for subject sorting
			$___mergedMainSubjectCode = $subjectCodeAry['id'][$____SubjectId]['CODEID'];
			if($___levelKey == "junior" && $___mergedMainSubjectCode != '' && isset($eRCTemplateSetting['Report']['TranscriptHandling']['ScienceSubjectCode']) && 
					in_array($___mergedMainSubjectCode, (array)$eRCTemplateSetting['Report']['TranscriptHandling']['ScienceSubjectCode']))
			{
				$___mergedMainSubjectAry[$____SubjectId] = $eRCTemplateSetting['Report']['TranscriptHandling']['ScienceSubjectPrefix'].$___mergedMainSubjectAry[$____SubjectId];
			}
        }
        $___mergedMainSubjectAry = asorti($___mergedMainSubjectAry);
        
        // Build Table Subject rows
        $_reportResultContentHtml = '';
        $___displayedSubjectIdAry = array();
        foreach ((array) $___mergedMainSubjectAry as $____parentSubjectId => $____parentSubjectName)
        {
            // No need to display > SKIP
            if (! in_array($____parentSubjectId, (array) $_reportDisplaySubjectIdAry[$___levelKey])) {
                continue;
            }
            // Already displayed > SKIP
            if (in_array($____parentSubjectId, (array) $___displayedSubjectIdAry)) {
                continue;
            }
            
            // Add prefix for Science Subject (Junior only) > for subject name display
			$_____subjectNameEnPrefix = '';
			$___parentSubjectCode = $subjectCodeAry['id'][$____parentSubjectId]['CODEID'];
			if($___levelKey == "junior" && $___parentSubjectCode != '' && isset($eRCTemplateSetting['Report']['TranscriptHandling']['ScienceSubjectCode']) && 
					in_array($___parentSubjectCode, (array)$eRCTemplateSetting['Report']['TranscriptHandling']['ScienceSubjectCode']))
			{
				$_____subjectNameEnPrefix = $eRCTemplateSetting['Report']['TranscriptHandling']['ScienceSubjectPrefix'];
			}
            
            $____cmpSubjectAry = $___mergedSubjectAry[$____parentSubjectId];
            foreach ((array) $____cmpSubjectAry as $_____cmpSubjectID => $_____cmpSubjectName)
            {
                $_____isComponentSubject = ($_____cmpSubjectID == 0) ? false : true;
                $_____subjectId = ($_____isComponentSubject) ? $_____cmpSubjectID : $____parentSubjectId;
                
                // No need to display Component => SKIP
                if ($_____isComponentSubject) {
                    continue;
                }
                
                // Get Subject Name
                $_____subjectNameEn = $_____subjectNameEnPrefix.$lreportcard->GET_SUBJECT_NAME_LANG($_____subjectId, 'en');
                
                // Get Subject Year Grade (Code : Z999)
                $____subjectCode = $subjectCodeAry['id'][$_____subjectId]['CODEID'];
                $____yearGradeCode = $eRCTemplateSetting['Report']['DataTransferCompAssessmentAry']["0"];
                $____gradeSubjectId = $subjectCodeAry['code'][$____subjectCode . '_' . $____yearGradeCode];
                $____gradeSubjectId = $____gradeSubjectId ? $____gradeSubjectId : - 999;
                
                $_____subjectTr = '';
                $_____subjectTr .= '<tr>' . "\r\n";
                    $_____subjectTr .= '<td style="text-align: left; padding-left: 4px;">' . $_____subjectNameEn . '</td>' . "\r\n";
                
                $emptyBeforeColSize = $emptyColSizeAry[$_studentId][$___levelKey]['first'] ? $emptyColSizeAry[$_studentId][$___levelKey]['first'] : 0;
                for ($j = 0; $j < $emptyBeforeColSize; $j ++)
                {
                    $_____subjectTr .= '<td>' . $emptySymbol . '</td>' . "\r\n";
                }
                
                // Subject Year Grade
                $numOfAcademicYearPerRow = count((array) $___academicYearIdAry);
                for ($j = 0; $j < $numOfAcademicYearPerRow; $j ++)
                {
                    $______academicYearId = $___academicYearIdAry[$j];
                    $______formId = $studentDataAssoAry[$_studentId]['classHistoryAry'][$___levelKey]['levelAcademicYearAssoAry'][$______academicYearId]['formId'];
                    $______formName = $studentDataAssoAry[$_studentId]['classHistoryAry'][$___levelKey]['levelAcademicYearAssoAry'][$______academicYearId]['formName'];
                    $______formNumber = $studentDataAssoAry[$_studentId]['classHistoryAry'][$___levelKey]['levelAcademicYearAssoAry'][$______academicYearId]['formNumber'];
                    $______studentStudied = $studentDataAssoAry[$_studentId]['classHistoryAry'][$___levelKey]['levelAcademicYearAssoAry'][$______academicYearId]['studentStudied'];
                    
                    // Not studied in this year => Show "--"
                    if ($______formId == '' && $______formName != '') {
                        $_____subjectTr .= '<td>' . $emptySymbol . '</td>' . "\r\n";
                    }                    // Empty Year Column => show "--"
                    else if ($______formId == '' && $______formName == '') {
                        $_____subjectTr .= '<td>' . $emptySymbol . '</td>' . "\r\n";
                    }                    // Studied this year => Show Grade
                    else {
                        $_____targetSubjectId = $____gradeSubjectId;
                        $______subjectMarkDisplay = $studentDataAssoAry[$_studentId]['classHistoryAry'][$___levelKey]['levelAcademicYearAssoAry'][$______academicYearId]['markAry'][$_____targetSubjectId]['markDisplay'];
                        $_____subjectTr .= '<td>' . $______subjectMarkDisplay . '</td>' . "\r\n";
                    }
                    
                    $___displayedSubjectIdAry[] = $_____targetSubjectId;
                }
                
                $emptyAfterColSize = $emptyColSizeAry[$_studentId][$___levelKey]['last'] ? $emptyColSizeAry[$_studentId][$___levelKey]['last'] : 0;
                for ($j = 0; $j < $emptyAfterColSize; $j ++)
                {
                    $_____subjectTr .= '<td>' . $emptySymbol . '</td>' . "\r\n";
                }
                
                $_____subjectTr .= '</tr>' . "\r\n";
                $_reportResultContentHtml .= $_____subjectTr;
            }
        }
        if ($_reportResultContentHtml == '') {
            continue;
        }
        
        // Get Table Remarks (Senior Table only)
        $_reportResultRemarksHtml = '';
        if ($___levelKey == "senior" && !empty($_reportRemarks))
        {
            $_reportResultRemarksHtml .= '<table class="table-remark" width="100%">';
                $_reportResultRemarksHtml .= '<tr><td colspan="2">'.$Lang['eReportCard']['ReportArr']['TranscriptArr']['RemarksEn'].':</td></tr>';
            
            $__reportRemarksCount = 1;
            $reportRemarksTotalCount = count((array)$_reportRemarks);
            foreach ((array)$_reportRemarks as $__reportRemarks)
            {
                $_reportResultRemarksHtml .= '<tr>';
                    if($reportRemarksTotalCount == 1) {
                        $_reportResultRemarksHtml .= '<td width="8%">&nbsp;</td>';
                    } else {
                        $_reportResultRemarksHtml .= '<td width="8%">&nbsp;('.$__reportRemarksCount.')</td>';
                    }
                    $_reportResultRemarksHtml .= '<td width="92%" class="ch_font">'.$__reportRemarks.'</td>';
                $_reportResultRemarksHtml .= '</tr>';
                
                $__reportRemarksCount++;
            }
            
            $_reportResultRemarksHtml .= '</table>';
        }
        
        $tableClass = $___levelKey == "senior" ? 'table-Senior' : 'table-Junior';
        $tableWidth = $levelTableWidth[$_studentId][$___levelKey] ? $levelTableWidth[$_studentId][$___levelKey] : 50;
        $_reportResultHtml .= '<td valign="top" width="' . $tableWidth . '%">';
            $_reportResultHtml .= '<table cellspacing="0" cellpadding="0" border="1" class="' . $tableClass . '" style="width:100%; ">' . "\r\n";
                $_reportResultHtml .= $_reportResultHeaderHtml;
                $_reportResultHtml .= $_reportResultContentHtml;
            $_reportResultHtml .= '</table>' . "\r\n";
            $_reportResultHtml .= $_reportResultRemarksHtml;    
        $_reportResultHtml .= '</td>';
    }
    
    // Report Footer
    $displayPrincialName = trim($princialName) != '' ? trim($princialName) . '<br/>' : $eReportCard['Template']['PrincipalName'].'<br/>';
    // $displayPrincialName = trim($princialName) != '' ? trim($princialName) . '<br/>' : '<br/>';
    
    //$needPageBreak = $_isLastStudent ? '' : ' page-break-after:always; ';
    $_reportFooterHtml = ' <div class="sign-space" height="20mm" style=" ' . $needPageBreak . '" align="center">
                                <table width="100%">
                                    <tr class="sign-line">
                                    	<td width="15%" style="line-height: 1"></td>
                                        <td width="27.5%" style="line-height: 1">_________________________</td>
                                        <td width="15%" style="line-height: 1"></td>
                                        <td width="27.5%" style="line-height: 1";>______________________________</td>
                                        <td width="15%" style="line-height: 1"></td>
                                    </tr>
                                    <tr>
                                    	<td></td>
                                    	<td style="border-top: 1px black solid; text-align: center">' . $Lang['eReportCard']['ReportArr']['TranscriptArr']['SchoolChopEn'] . '</td>
                                    	<td></td>
                                    	<td style="border-top: 1px black solid; text-align: center">' . $displayPrincialName . $Lang['eReportCard']['ReportArr']['TranscriptArr']['PrincipalEn'] . '</td>
                                    	<td></td>
                                    </tr>
                                    <tr class="end-line">
                                    	<td colspan="5">' . $Lang['eReportCard']['ReportArr']['TranscriptArr']['EndOfTranscriptEn'] . '</td>
                                    </tr>
                                </table>
                            </div>';
    $needPageBreak = $_isLastStudent ? '' : '<pagebreak />';
    
    // Report Header & Table
    $x = '';
    $x .= '<div style="width:100%;" align="center">' . "\r\n";
        // $x .= '<div class="container" height = "240mm;">' . "\r\n";
        $x .= '<div class="container" height = "224mm;">' . "\r\n";
            $x .= '<table cellpadding="0" cellspacing="0" align="center" style="width:100%; vertical-align:top;">' . "\r\n";
                $x .= $_reportHeaderHtml;
            $x .= '</table>' . "\r\n";
            $x .= '<table cellspacing="0" cellpadding="0" style="width:100%;"><tr>' . "\r\n";
                $x .= $_reportResultHtml;
            $x .= '</tr></table>' . "\r\n";
        $x .= '</div>' . "\r\n";
        $x .= $_reportFooterHtml;
        $x .= $needPageBreak;
    $x .= '</div>' . "\r\n";
    $transcriptContentAry[] = $x;
}

# Define header
$mpdfObj->DefHTMLHeaderByName("titleHeader", "");
$mpdfObj->SetHTMLHeaderByName("titleHeader");

# Define footer
$mpdfObj->DefHTMLFooterByName("emptyFooter", "");
$mpdfObj->SetHTMLFooterByName("emptyFooter");

$transcriptCSS = returnTranscriptCSS();
$mpdfObj->writeHTML($transcriptCSS);

$mpdfObj->writeHTML("<body>");
foreach ($transcriptContentAry as $thisTranscriptContent)
{
    $mpdfObj->writeHTML($thisTranscriptContent);
}
$mpdfObj->writeHTML("</body>");

// echo $transcriptCSS;
// foreach($transcriptContentAry as $thisTranscriptContent)
// {
// echo $thisTranscriptContent;
// }

// For performance tunning info
// echo '<script language="JavaScript" src="/templates/jquery/jquery-1.3.2.min.js"></script>';
// debug_pr('convert_size(memory_get_usage(true)) = '.convert_size(memory_get_usage(true)));
// debug_pr('Query Count = '.$GLOBALS[debug][db_query_count]);
// $lreportcard->db_show_debug_log();
// die();
?>