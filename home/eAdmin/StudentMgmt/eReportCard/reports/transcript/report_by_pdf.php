<?php
# Editing by

@SET_TIME_LIMIT(21600);
@ini_set(memory_limit, -1);

/**************************************************
 * 	Modification log
 *  20190625 Bill:  [2017-0901-1525-31265]
 *      Allow pdf margin settings   ($eRCTemplateSetting['Report']['TranscriptPDF']['Margin'])
 * 	20180723 Bill:  [2017-0901-1525-31265]
 * 		- Allow View Group to access
 *  20180226 Bill	[2017-0901-1525-31265]
 * 		- Create file
 * ***********************************************/

$PageRight = array("ADMIN", "VIEW_GROUP");

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

// Access right
if (!$plugin['ReportCard2008'])
{
    header ("Location: /");
    intranet_closedb();
    exit();
}
 
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");

$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();

// Report Margin
$margin_top = 18;
$margin_bottom = 5;
$margin_left = 8;
$margin_right = 8;

// [2017-0901-1525-31265]
if(isset($eRCTemplateSetting['Report']['TranscriptPDF']['Margin'])) {
    list($margin_top, $margin_right, $margin_bottom, $margin_left) = $eRCTemplateSetting['Report']['TranscriptPDF']['Margin'];
}

// Create mPDF object
require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");
$mpdfObj = new mPDF('zh', "A4", 0, "", $margin_left, $margin_right, $margin_top, $margin_bottom, $margin_header, $margin_footer);

// PDF Setting
$mpdfObj->charset_in = "UTF-8";
$mpdfObj->allow_charset_conversion = true;
$mpdfObj->list_auto_mode = "mpdf";
$mpdfObj->backupSubsFont = array('times', 'kaiu', 'mingliu', 'msjh');
$mpdfObj->useSubstitutions = true;
// $mpdfObj->backupSubsFont = array("sun-exta", 'timesnewroman');
// $mpdfObj->setAutoTopMargin = 'stretch';
// $mpdfObj->splitTableBorderWidth = 0.1;  // split 1 table into 2 pages add border at the bottom

if (file_exists($ReportCardCustomSchoolName.'/report.php')) {
	include_once($ReportCardCustomSchoolName.'/report.php');
}

// Output file
$mpdfObj->Output("Transcipt.pdf", 'I');

intranet_closedb();
?>