<?php
// using : 

/***********************************************
 * modification log
 * 20191120 Bill [2019-0924-1029-57289]
 *  - exclude promotion status handling ($eRCTemplateSetting['Report']['ClassTeacherReportPrinting_NoPromotion'])
 * 20180723 Bill [2017-0901-1525-31265]
 *  - print generated report instead of archived report ($eRCTemplateSetting['ViewGroupAccessReportOnly'])
 * 20171201 Bill:
 * 	- Added Student Selection and Check archived reports
 * 20170623 Bill:
 * 	- Added Promotion Status Filtering
 * 20170519	Bill:
 * 	- Allow View Group to access all options
 * ***********************************************/

$PageRight = "TEACHER";

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
    }
	
	# Initial
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		$lreportcard = new libreportcardcustom();
	}
	else {
		$lreportcard = new libreportcard();
	}
	
	if ($lreportcard->hasAccessRight())
	{
		$lteaching = new libteaching();
		$linterface = new interface_html();
		
		# Page Tag
		$CurrentPage = "Reports_ClassTeacherReportPrinting";
		$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
		$TAGS_OBJ[] = array($eReportCard['ReportPrinting']);
		
		$linterface->LAYOUT_START();
		
		$submitBtn = $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submitBtn");
		
		if($ck_ReportCard_UserType == "TEACHER")
		{
			# Check is Class Teacher or not
			$TeacherClassAry = $lteaching->returnTeacherClassWithLevel($UserID, $lreportcard->GET_ACTIVE_YEAR_ID());
			$TeacherClass = $TeacherClassAry[0]['ClassName'];
			
			# Build Form Array
			$FormArr = array();
			if(!empty($TeacherClass)) {
				for($i=0; $i<sizeof($TeacherClassAry); $i++)
				{
					$thisClass = $TeacherClassAry[$i];
					$thisClassLevelID = $thisClass['ClassLevelID'];
					
					$searchResult = multiarray_search($FormArr, "ClassLevelID", $thisClassLevelID);
					if($searchResult == "-1") {
						$thisResultAry = array(
							"0" => $thisClass['ClassLevelID'],
							"ClassLevelID" => $thisClass['ClassLevelID'],
							"1" => $thisClass['LevelName'],
							"LevelName" => $thisClass['LevelName']);
						$FormArr = array_merge($FormArr, array($thisResultAry));
					}
				}
				
				# Sort Form Array
				foreach ($FormArr as $key => $row) {
					$field1[$key] = $row['ClassLevelID'];
					$field2[$key] = $row['LevelName'];
				}
				array_multisort($field1, SORT_ASC, $field2, SORT_ASC, $FormArr);
			}
			$ClassLevelID = ($ClassLevelID == "") ? $FormArr[0][0] : $ClassLevelID;
			
			# Build Class Array
			$ClassArr = array();
			if(!empty($TeacherClass))
			{
				$searchResult = multiarray_search($TeacherClassAry, "ClassLevelID", $ClassLevelID);
				if($searchResult != "-1")
				{
					$searchResult2 = multiarray_search($ClassArr, "ClassName", $TeacherClassAry[$searchResult]['ClassName']);
					if($searchResult2 == "-1") {
						$thisResultAry = array(
							"0" => $TeacherClassAry[$searchResult]['ClassID'],
							"ClassID" => $TeacherClassAry[$searchResult]['ClassID'],
							"1" => $TeacherClassAry[$searchResult]['ClassName'],
							"ClassName" => $TeacherClassAry[$searchResult]['ClassName'],
							"2" => $TeacherClassAry[$searchResult]['ClassLevelID'],
							"ClassLevelID" => $TeacherClassAry[$searchResult]['ClassLevelID']);
						$ClassArr = array_merge($ClassArr, array($thisResultAry));
					}
				}
			}
		}
		
		# Get Form Array
		$FormArr = ($ck_ReportCard_UserType=="ADMIN" || $ck_ReportCard_UserType=="VIEW_GROUP")? $lreportcard->GET_ALL_FORMS(1) : $FormArr;
		if(count($FormArr) > 0)
		{
			# Filters - By Form
			$ClassLevelID = ($ClassLevelID == "") ? $FormArr[0][0] : $ClassLevelID;
			$FormSelection = $linterface->GET_SELECTION_BOX($FormArr, "name='ClassLevelID' id='ClassLevelID' class='' onchange='reloadFormSelection(this.options[this.selectedIndex].value)'", "", $ClassLevelID);
			
			# Filters - By Class
			$ClassArr = ($ck_ReportCard_UserType=="ADMIN" || $ck_ReportCard_UserType=="VIEW_GROUP")? $lreportcard->GET_CLASSES_BY_FORM($ClassLevelID) : $ClassArr;
			$ClassID = ($ClassID == "") ? $ClassArr[0][0] : $ClassID;
			$SelectAllClassBtn = $linterface->GET_SMALL_BTN($button_select_all, "button", "js_Select_All('ClassID', 1); js_Reload_Student_Selection(); return false;");
			
			# Filters - By Report
			$ReportIDList = array();
			$ReportTypeOption = array();
			$ReportTypeOptionCount = 0;
			$ReportTypeArr = array();
			$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($ClassLevelID);
			if(count($ReportTypeArr) > 0)
			{
				for($j=0; $j<count($ReportTypeArr); $j++) {
					$ReportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportTypeArr[$j]['ReportID']);
					if (($eRCTemplateSetting['ViewGroupAccessReportOnly'] && $ReportTemplateInfo["LastGenerated"] != "0000-00-00 00:00:00" && $ReportTemplateInfo["LastGenerated"] != "") || 
					       ($ReportTemplateInfo["LastArchived"] != "0000-00-00 00:00:00" && $ReportTemplateInfo["LastArchived"] != "")) {
						$ReportIDList[] = $ReportTypeArr[$j]['ReportID'];
						$ReportTypeOption[$ReportTypeOptionCount][0] = $ReportTypeArr[$j]['ReportID'];
						$ReportTypeOption[$ReportTypeOptionCount][1] = $ReportTypeArr[$j]['SemesterTitle'];
						$ReportTypeOptionCount++;
					}
				}
			}
			if (sizeof($ReportTypeOption) > 0) {
				$ReportID = ($ReportID == "" || !in_array($ReportID, $ReportIDList)) ? $ReportTypeOption[0][0] : $ReportID;
				$ReportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportID);
				$ReportTypeSelection = $lreportcard->Get_Report_Selection($ClassLevelID, $ReportID, "ReportID", "", 0, 0, 1);
			}
			else {
				$ReportTypeOption[0][-1] = $eReportCard['NoReportAvailable'];
				$ReportTypeSelectDisabled = 'disabled="disabled"';
				$ReportTypeSelection = getSelectByAssoArray($ReportTypeOption, ' id="ReportID" name="ReportID" '.$ReportTypeSelectDisabled, -1, false, true);
			}
			
			for($i=0; $i<sizeof($FormArr); $i++)
			{
				$ClassArr = array();
				if(!empty($TeacherClass))
				{
					$searchResult = multiarray_search($TeacherClassAry, "ClassLevelID", $FormArr[$i][0]);
					if($searchResult != "-1")
					{	
						$searchResult2 = multiarray_search($ClassArr, "ClassName", $TeacherClassAry[$searchResult]['ClassName']);
						if($searchResult2 == "-1") {
							$thisResultAry = array(
								"0" => $TeacherClassAry[$searchResult]['ClassID'],
								"ClassID" => $TeacherClassAry[$searchResult]['ClassID'],
								"1" => $TeacherClassAry[$searchResult]['ClassName'],
								"ClassName" => $TeacherClassAry[$searchResult]['ClassName'],
								"2"=>$TeacherClassAry[$searchResult]['ClassLevelID'],
								"ClassLevelID" => $TeacherClassAry[$searchResult]['ClassLevelID']);
							$ClassArr = array_merge($ClassArr, array($thisResultAry));
						}
					}
				}
				
				# Filters - By Class (ClassID)
				$ClassArr = ($ck_ReportCard_UserType=="ADMIN" || $ck_ReportCard_UserType=="VIEW_GROUP")? $lreportcard->GET_CLASSES_BY_FORM($FormArr[$i][0]) : $ClassArr;
				$ClassID = $ClassArr[0][0];
				$OtherClassSelection[$FormArr[$i][0]] = $linterface->GET_SELECTION_BOX($ClassArr, 'id="ClassID" name="ClassID[]" class="" onchange="js_Reload_Student_Selection()" multiple size=6', "", $ClassID);
			}
			
			// Other Reports - used when User selected different Class Level (store as JS array)
			$OtherReportTypeSelection = array();
			$PromotionOptionDisplay = array();
			for($i=0; $i<sizeof($FormArr); $i++)
			{
				$ReportIDList = array();
				$ReportIDOrder = 0;
				$ReportTypeOption = array();
				$ReportTypeOptionCount = 0;
				$excludeReportIDList = array();
				
				$ReportTypeArr = array();
				$ReportTypeArr = $lreportcard->GET_REPORT_TYPES($FormArr[$i][0]);
				if(count($ReportTypeArr) > 0) {
					for($j=0; $j<sizeof($ReportTypeArr); $j++)
					{
						$ReportTemplateInfo = $lreportcard->returnReportTemplateBasicInfo($ReportTypeArr[$j]['ReportID']);
						//if ($ReportTemplateInfo["LastGenerated"] != "0000-00-00 00:00:00" && $ReportTemplateInfo["LastGenerated"] != "") {
					    if (($eRCTemplateSetting['ViewGroupAccessReportOnly'] && $ReportTemplateInfo["LastGenerated"] != "0000-00-00 00:00:00" && $ReportTemplateInfo["LastGenerated"] != "") ||
					        ($ReportTemplateInfo["LastArchived"] != "0000-00-00 00:00:00" && $ReportTemplateInfo["LastArchived"] != "")) {
							$ReportIDList[] = $ReportTypeArr[$j]['ReportID'];
							$ReportTypeOption[$ReportTypeOptionCount][0] = $ReportTypeArr[$j]['ReportID'];
							$ReportTypeOption[$ReportTypeOptionCount][1] = $ReportTypeArr[$j]['SemesterTitle'];
							$ReportTypeOptionCount++;
							
							if($ReportTypeArr[$j]['Semester'] == "F") {
								$PromotionOptionDisplay[$ReportTypeArr[$j]['ReportID']] = "1";
							}
							else {
								$PromotionOptionDisplay[$ReportTypeArr[$j]['ReportID']] = "0";
							}
						}
						else {
							$excludeReportIDList[] = $ReportTypeArr[$j]['ReportID'];
						}
					}
					sort($ReportIDList);
				}
				if (sizeof($ReportTypeOption) > 0) {
					//$OtherReportTypeSelection[$FormArr[$i][0]] = $lreportcard->Get_Report_Selection($FormArr[$i][0], "", "ReportID", " updatePromotionListDisplay(this) ", 0, 0, 1);
					$OtherReportTypeSelection[$FormArr[$i][0]] = $lreportcard->Get_Report_Selection($FormArr[$i][0], "", "ReportID", " updatePromotionListDisplay(this) ", 0, 0, 0, "", 0, $excludeReportIDList);
				}
				else {
					$ReportTypeOption[0][-1] = $eReportCard['NoReportAvailable'];
					$ReportTypeSelectDisabled = 'disabled="disabled"';
					$OtherReportTypeSelection[$FormArr[$i][0]] = getSelectByAssoArray($ReportTypeOption, ' id="ReportID" name="ReportID" '.$ReportTypeSelectDisabled, -1, false, true);
				}
			}
		}
		
		# Status Array
        if($eRCTemplateSetting['Report']['ClassTeacherReportPrinting_NoPromotion']) {
		    // do nothing
        }
        else {
            $promotionStatusSelectionAry = array();
            $promotionStatusSelectionAry[] = array("1", $eReportCard["PromotionStatus"][3]." / ".$eReportCard["PromotionStatus"][1]." / ".$eReportCard["PromotionStatus"][2]);
            $promotionStatusSelectionAry[] = array("2", $eReportCard["PromotionStatus"][4]." / ".$eReportCard["PromotionStatus"][5]." (".$eReportCard["PromotionStatusType"]["BeforeMarkupExam"].")");
            $promotionStatusSelectionAry[] = array("3", $eReportCard["PromotionStatus"][4]." / ".$eReportCard["PromotionStatus"][5]." (".$eReportCard["PromotionStatusType"]["AfterMarkupExam"].")");
            $promotionStatusSelection = getSelectByArray($promotionStatusSelectionAry, " id='PromotionStatus' name='PromotionStatus' ' ", "", 1, 0)."&nbsp;";
        }
?>

<script type="text/JavaScript" language="JavaScript">
// Preload Report list
var classLevelReportList = new Array();
var classLevelClassList = new Array();
var reportTypeList = new Array();
var promotionOptionSetting = new Array();

<?php
if(count($FormArr) > 0) {
	foreach($OtherReportTypeSelection as $key => $value)
	{
		$value = addslashes(str_replace("\n", "", $value));
		$x .= "classLevelReportList[".$key."] = '$value';\n";	
	}
	echo $x;
	
	foreach($OtherClassSelection as $key => $value)
	{
		$value = addslashes(str_replace("\n", "", $value));
		$w .= "classLevelClassList[".$key."] = '$value';\n";
	}
	echo $w;
	
	foreach((array)$PromotionOptionDisplay as $key => $value)
	{
		$y .= "promotionOptionSetting[".$key."] = '$value';\n";
	}
	echo $y;
}
?>

function reloadFormSelection(classLevelID)
{
	document.getElementById("ReportList").innerHTML = classLevelReportList[classLevelID];
	document.getElementById("ClassList").innerHTML = classLevelClassList[classLevelID];
	
	var reportIDSelect = document.getElementById("ReportID");	
	if (reportIDSelect.disabled) {
		document.getElementById("submitBtn").disabled = true;
		document.getElementById("submitBtn").className = 'formbutton_disable';
		document.getElementById("submitBtn").onmouseover = " this.className = 'formbutton_disable' ";
		document.getElementById("submitBtn").onmouseout = " this.className = 'formbutton_disable' ";
	}
	else {
		document.getElementById("submitBtn").disabled = false;
		document.getElementById("submitBtn").className = 'formbutton';
		document.getElementById("submitBtn").onmouseover = " this.className = 'formbutton' ";
		document.getElementById("submitBtn").onmouseout = " this.className = 'formbutton' ";
	}
	
	js_Select_All('ClassID', 1);
	js_Reload_Student_Selection();
	updatePromotionListDisplay(reportIDSelect);
}

function js_Reload_Student_Selection()
{
	var classIdList = $("select#ClassID").val()
	if  (classIdList) {
		classIdList = classIdList.join(',');
	}
	else {
		classIdList = '-1'; 	// no class will be returned
	}
	
	$.post(
		"../ajax_reload_selection.php",
		{
			RecordType: 'StudentByClass',
			SelectionID: 'TargetStudentID[]',
			ClassID: classIdList,
			noFirst: 1,
			isMultiple: 1,
			isAll: 1,
			withSelectAll: 1
		},
		function(ReturnData)
		{
			$("#studentSelDiv").html(ReturnData);
			js_Select_All('TargetStudentID[]', 1);
		}
	)
}

function updatePromotionListDisplay(obj)
{
	<?php if($eRCTemplateSetting['ViewGroupAccessReportOnly'] || $eRCTemplateSetting['Report']['ClassTeacherReportPrinting_NoPromotion']) { ?>
		// do nothing
	<?php } else { ?>
    	if(obj)
    	{
    		if (obj.disabled) {
    			$('tr#promotion_tr').hide();
    		}
    		else {
    			var selectedReportID = obj.value;
    			if(selectedReportID && promotionOptionSetting[selectedReportID]=='1') {
    				$('tr#promotion_tr').show();
    			}
    			else {
    				$('tr#promotion_tr').hide();
    			}
    		}
    	}
	<?php } ?>
}

function checkForm()
{
	var FormValid = true;
	$("div.warnMsgDiv").hide();
	
	var classIdList = $('select#ClassID').val();
	var stuSelId = getJQuerySaveId('TargetStudentID[]');
	var stuIdList = $('select#'+stuSelId).val();
	
	if(classIdList == null || classIdList == '')
	{
		FormValid = false;
		$("div#classWarningDiv").show();
	}
	else if(stuIdList == null || stuIdList == '')
	{
		FormValid = false;
		$("div#studentWarningDiv").show();
	}
	
	return FormValid;
}

$().ready(function() {
	reloadFormSelection($("#ClassLevelID").val());
})
</script>

<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT."templates/2007a/css/"?>ereportcard.css">
<br />
<form name="form1" action="<?= ($eRCTemplateSetting['ViewGroupAccessReportOnly']? '../generate_reports/print_preview.php' : '../../management/archive_report_card/print_preview.php')?>" method="POST" onsubmit="return checkForm();" target="_blank">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
<table>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td></td>
					<td align="right"><?=$linterface->GET_SYS_MSG($Result);?></td>
				</tr>
			<?php if(count($FormArr) == 0 || count($OtherReportTypeSelection) == 0) { ?>
				<tr>
					<td align="center" colspan="2" class='tabletext'>
						<br /><?= $i_no_record_exists_msg ?><br />
					</td>
				</tr>
			<?php }
			else { ?>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['Form'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<?= $FormSelection ?>
					</td>
				</tr>
				<tr id="ClassRow" style="<?= $ClassRowDisplay ?>">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['Class'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<span id="ClassList"><?= $ClassSelection ?></span><?=$SelectAllClassBtn?>
						<?= $linterface->Get_Form_Warning_Msg("classWarningDiv", $eReportCard['jsWarningSelectClass'], "warnMsgDiv") ?>
					</td>
				</tr>
				<tr id="StudentRow">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['Student'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<div id="studentSelDiv"></div>
						<?= $linterface->Get_Form_Warning_Msg("studentWarningDiv", $eReportCard['jsWarningArr']['PleaseSelectStudent'], "warnMsgDiv") ?>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard['ReportCard'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<span id="ReportList"><?= $ReportTypeSelection ?></span>
					</td>
				</tr>
				<tr id="promotion_tr" style="display: none">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $eReportCard["PromotionStatusType"]["PromotionStatus"] ?>
					</td>
					<td width="75%" class='tabletext'>
						<span id="StatusList"><?= $promotionStatusSelection ?></span>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="dotline">
						<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<?= $submitBtn ?>
					</td>
				</tr>
			<?php } ?>
			</table>
		</td>
	</tr>
</table>
</form>
<?
		print $linterface->FOCUS_ON_LOAD("form1.ClassLevelID");
	  	$linterface->LAYOUT_STOP();
	}
	else {
?>
You have no priviledge to access this page.
<?
	}
}
else {
?>
You have no priviledge to access this page.
<?
}
?>