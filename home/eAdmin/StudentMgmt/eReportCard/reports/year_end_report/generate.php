<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();


$viewFormat = standardizeFormPostValue($_POST['ViewFormat']);
$formIdAry = $_POST['TargetID'];
$numOfForm = count($formIdAry);


include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
$lreportcard = new libreportcardcustom();
$lreportcard->hasAccessRight();


$fcm = new form_class_manage();
$termAry = $fcm->Get_Academic_Year_Term_List($lreportcard->GET_ACTIVE_YEAR_ID());
$numOfTerm = count($termAry);


$dataAry = array();
$titleAry = array();
for ($i=0; $i<$numOfForm; $i++) {
	$_formId = $formIdAry[$i];
	
	// get students
	$_studentAssoAry = BuildMultiKeyAssoc($lreportcard->GET_STUDENT_BY_CLASSLEVEL('', $_formId), 'UserID');
	
	
	// get consolidated report
	$_consolidatedReportInfoAry = $lreportcard->returnReportTemplateBasicInfo('', '', $_formId, $Semester='F');
	$_consolidatedReportId = $_consolidatedReportInfoAry['ReportID'];
	$_reportColumnAry = $lreportcard->returnReportTemplateColumnData($_consolidatedReportId);
	$_numOfReportColumn = count($_reportColumnAry);
	
	
	// get marks
	$_markAry = $lreportcard->getMarksCommonIpEj($_consolidatedReportId, $StudentID='', $cons='', $ParentSubjectOnly=1, $includeAdjustedMarks=1, $OrderBy='', $SubjectID='', $ReportColumnID=0);
	$_grandMarkAry = $lreportcard->getReportResultScore($_consolidatedReportId, $ReportColumnID='');
	
	
	// get conduct
	$_otherInfoAry = $lreportcard->getReportOtherInfoData($_consolidatedReportId);
	
	
	// get subjects
	$_subjectAry = $lreportcard->returnSubjectwOrder($_formId, $ParForSelection=0, $TeacherID='', $SubjectFieldLang='en', $ParDisplayType='ShortName', $ExcludeCmpSubject=1, $_consolidatedReportId);
	$_subjectAssoAry = array();
	foreach ((array)$_subjectAry as $__subjectId => $__subjectInfoAry) {
		$_subjectAssoAry[$__subjectId] = $__subjectInfoAry[0];
	}
	
	
	// get student order by GPA
	$_orderFieldAry = array();
	$_orderFieldAry[0]['OrderField'] = 'GPA';
	$_orderFieldAry[0]['Order'] = 'desc';
	$_studentOrderedIdAry = array_keys((array)$lreportcard->getReportResultScore($_consolidatedReportId, $ReportColumnID=0, $StudentID='', $other_conds='', 0, $includeAdjustedMarks=1, $CheckPositionDisplay=0, $FilterNoRanking=1, $_orderFieldAry));
	$_numOfStudent = count($_studentOrderedIdAry);
	
	
	// build report title
	$titleAry[$_formId][] = 'UserLogin';
	$titleAry[$_formId][] = 'Class';
	$titleAry[$_formId][] = 'Class No.';
	$titleAry[$_formId][] = 'Name in English';
	$titleAry[$_formId][] = 'Name in Chinese';
	$titleAry[$_formId][] = 'Sex';
	for ($j=0; $j<$numOfTerm; $j++) {
		$__yearTermName = $termAry[$j]['YearTermNameEN'];
		$titleAry[$_formId][] = $__yearTermName.' Conduct';
	}
	$titleAry[$_formId][] = 'Average Conduct';
	for ($j=0; $j<$numOfTerm; $j++) {
		$__yearTermName = $termAry[$j]['YearTermNameEN'];
		$titleAry[$_formId][] = $__yearTermName.' GPA';
	}
	$titleAry[$_formId][] = 'Overall GPA';
	foreach ((array)$_subjectAssoAry as $__subjectId => $__subjectName) {
		$titleAry[$_formId][] = $__subjectName;
	}
	
	
	// build report data
	for ($j=0; $j<$_numOfStudent; $j++) {
		$__studentId = $_studentOrderedIdAry[$j];
		
		$__userLogin = $_studentAssoAry[$__studentId]['UserLogin'];
		$__className = $_studentAssoAry[$__studentId]['ClassName'];
		$__classNumber = $_studentAssoAry[$__studentId]['ClassNumber'];
		$__englishName = $_studentAssoAry[$__studentId]['StudentNameEn'];
		$__chineseName = $_studentAssoAry[$__studentId]['StudentNameCh'];
		$__gender = $_studentAssoAry[$__studentId]['Gender'];
		
		$__gender = ($__gender=='')? $Lang['General']['EmptySymbol'] : $__gender;
		
		// student basic info
		$dataAry[$_formId][$j][] = $__userLogin;
		$dataAry[$_formId][$j][] = $__className;
		$dataAry[$_formId][$j][] = $__classNumber;
		$dataAry[$_formId][$j][] = $__englishName;
		$dataAry[$_formId][$j][] = $__chineseName;
		$dataAry[$_formId][$j][] = $__gender;
		
		
		// student conduct of each terms
		$__conductFailCount = 0;
		for ($k=0; $k<$numOfTerm; $k++) {
			$___yearTermId = $termAry[$k]['YearTermID'];
			
			$___termConduct = trim($_otherInfoAry[$__studentId][$___yearTermId]['Conduct']);
			$___termConduct = ($___termConduct=='')? $Lang['General']['EmptySymbol'] : $___termConduct;
			
			$dataAry[$_formId][$j][] = $___termConduct;
			
			if ($___termConduct == 'NI' || $___termConduct == 'UN' || $___termConduct == 'PR') {
				$__conductFailCount++;
			}
		}
		
		// average conduct: If a student fails his conduct in two or more terms, then his average conduct will be "FAIL", otherwise his average conduct will be "PASS".
		if ($__conductFailCount >= 2) {
			$__averageConduct = 'FAIL';
		}
		else {
			$__averageConduct = 'PASS';
		}
		$dataAry[$_formId][$j][] = $__averageConduct;
		
		
		// student GPA of each terms
		for ($k=0; $k<$_numOfReportColumn; $k++) {
			$___reportColumnId = $_reportColumnAry[$k]['ReportColumnID'];
			
			$___termGpa = $_grandMarkAry[$__studentId][$___reportColumnId]['GPA'];
			$___termGpa = ($___termGpa=='' || $___termGpa==-1)? $Lang['General']['EmptySymbol'] : $___termGpa;
			$dataAry[$_formId][$j][] = $___termGpa;
		}
		
		// student overall GPA
		$__termGpa = $_grandMarkAry[$__studentId][0]['GPA'];
		$__termGpa = ($__termGpa=='' || $__termGpa==-1)? $Lang['General']['EmptySymbol'] : $__termGpa;
		$dataAry[$_formId][$j][] = $__termGpa;
		
		// student subject grade
		foreach ((array)$_subjectAry as $___subjectId => $___subjectName) {
			$___subjectGrade = $_markAry[$__studentId][$___subjectId][0]['Grade'];
			
			$dataAry[$_formId][$j][] = $___subjectGrade;
		}
	}
}


## Display the result
if ($viewFormat == 'html') {
	$x = '';
	for ($i=0; $i<$numOfForm; $i++) {
		$_formId = $formIdAry[$i];
		
		$_titleAry = $titleAry[$_formId];
		$_numOfTitle = count($_titleAry);
		$_dataAry = $dataAry[$_formId];
		$_numOfData = count($_dataAry);
		
		if ($i==($numOfForm-1)) {
			$_pageBreak = '';
		}
		else {
			$_pageBreak = 'page-break-after:always;';
		}
		
		$x .= '<div style="'.$_pageBreak.'">'."\r\n";
			$x .= '<table id="ResultTable" class="border_table" align="center">'."\r\n";
				$x .= '<tr>'."\r\n";
				for ($j=0; $j<$_numOfTitle; $j++) {
					$__title = $_titleAry[$j];
					
					$x .= '<td style="text-align:center;">'.$__title.'</td>'."\r\n";
				}
				$x .= '</tr>'."\r\n";
				for ($j=0; $j<$_numOfData; $j++) {
					$__studentDataAry = $_dataAry[$j];
					$__numOfData = count($__studentDataAry);
					
					$x .= '<tr>'."\r\n";
						for ($k=0; $k<$__numOfData; $k++) {
							$___cellData = $__studentDataAry[$k];
							
							$___textAlign = 'center';
							if ($k==3) {
								// student name English
								$___textAlign = 'left';
							}
							$x .= '<td style="text-align:'.$___textAlign.';">'.$___cellData.'</td>'."\r\n";
						}
					$x .= '</tr>'."\r\n";
				}
			$x .= '</table>'."\r\n";
			$x .= '<br />'."\r\n";
			$x .= '<br />'."\r\n";
		$x .= '</div>'."\r\n";
	}
	
	
	$css = '
<style>
body {
	font-size: small;
}

.border_table{
	border-collapse: collapse;
	border: 1px solid #000000;
	width: 900px;
}

.border_table tr td, .border_table tr th{
	border: 1px solid #000000;
	vertical-align: top;
	padding: 3px;
	font-size: 10pt;
}
</style>';
	
//	$allTable = '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
//					<tr>
//						<td>'.$x.'</td>
//					</tr>
//				</table>';
	$allTable = $x;
	
	echo $lreportcard->Get_Report_Header($ReportTitle);
	echo $allTable.$css;
	echo $lreportcard->Get_Report_Footer();
}
else if ($viewFormat == 'csv') {
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	
	$lexport = new libexporttext();
	
	$exportContentAry = array();
	
	for ($i=0; $i<$numOfForm; $i++) {
		$_formId = $formIdAry[$i];
		
		$_titleAry = $titleAry[$_formId];
		$_dataAry = $dataAry[$_formId];
		$_numOfData = count($_dataAry);
		
		$exportContentAry[] = $_titleAry;
		
		for ($j=0; $j<$_numOfData; $j++) {
			$exportContentAry[] = $_dataAry[$j];
		}
		
		$exportContentAry[] = array('');
		$exportContentAry[] = array('');
	}
	
	
	// Title of the grandmarksheet
	$filename = "year_end_report.csv";
	
	$export_content = $lexport->GET_EXPORT_TXT($exportContentAry, array());
	intranet_closedb();
	
	// Output the file to user browser
	$lexport->EXPORT_FILE($filename, $export_content);
}
?>