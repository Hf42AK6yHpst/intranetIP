<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_opendb();

unset($ck_ReportCard_UserType);
unset($IntranetReportCardType);
	
if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eReportCard"]==1)
{
	$IntranetReportCardType = "ADMIN";
}
else
{
	include_once ("$PATH_WRT_ROOT/includes/libuser.php");
	$header_lu = new libuser($_SESSION['UserID']);
	
	if ($header_lu->isTeacherStaff())
		$IntranetReportCardType = "TEACHER";
	else if ($header_lu->isStudent())
		$IntranetReportCardType = "STUDENT";
	else if ($header_lu->isParent())
		$IntranetReportCardType = "PARENT";
}

//$ck_ReportCard_UserType = ($ck_ReportCard_UserType == "ADMIN" && $top_menu_mode==1) ? "ADMIN" : "TEACHER";
//$ck_ReportCard_UserType = ($ck_ReportCard_UserType != "STUDENT" && $top_menu_mode==1) ? "ADMIN" : "TEACHER";
//setcookie("ck_ReportCard_UserType", $ck_ReportCard_UserType, 0, "", "", 0);

if (isset($IntranetReportCardType))
{
	# TYPES: ADMIN, TEACHER, STUDENT
	setcookie("ck_ReportCard_UserType", $IntranetReportCardType, 0, "", "", 0);
	$ck_ReportCard_UserType = $IntranetReportCardType;
}



if($ck_ReportCard_UserType=="TEACHER") {
	header("Location: management/marksheet_revision/marksheet_revision.php");
}
else if($ck_ReportCard_UserType=="STUDENT" || $ck_ReportCard_UserType=="PARENT") {
	header("Location: management/marksheet_feedback/index.php");
}
else {
	# Admin
	#$top_menu_mode = 1;
	#header("Location: settings/basic_settings/highlight.php");
	
	if ($top_menu_mode == 1)
		header("Location: management/schedule/schedule.php");
	else
		header("Location: management/marksheet_revision/marksheet_revision.php");
}

?>