<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$viewItem = array();
$viewValue = array();
$thisModule = "Discipline";

# Deletion of Discipline->Settings->Access Right->Teacher Right (old access right)
$GroupID = $ldiscipline->GET_ACCESS_RIGHT_GROUP_ID('T','');

$ldiscipline->DELETE_ACCESS_RIGHT($GroupID, 'T', 'Discipline', 'MGMT','',''); 
$ldiscipline->DELETE_ACCESS_RIGHT($GroupID, 'T', 'Discipline', 'STAT','',''); 
$ldiscipline->DELETE_ACCESS_RIGHT($GroupID, 'T', 'Discipline', 'REPORTS','',''); 
$ldiscipline->DELETE_ACCESS_RIGHT($GroupID, 'T', 'Discipline', 'SETTINGS','',''); 

# Addition of Discipline->Settings->Access Right->Teacher Right (new access right)
$dataAry = array();
# - Management -
# Award_Punishment
if($_POST['MGMT_Award_Punishment_View'] != '') { $dataAry['MGMT']['Award_Punishment'][] = $_POST['MGMT_Award_Punishment_View'];}
if($_POST['MGMT_Award_Punishment_New'] != '') { $dataAry['MGMT']['Award_Punishment'][] = $_POST['MGMT_Award_Punishment_New'] ;}
if($_POST['MGMT_Award_Punishment_Edit'] != '') { $dataAry['MGMT']['Award_Punishment'][] = $_POST['MGMT_Award_Punishment_Edit'].$_POST['f10_OwnAll'] ;}
if($_POST['MGMT_Award_Punishment_Delete'] != '') { $dataAry['MGMT']['Award_Punishment'][] = $_POST['MGMT_Award_Punishment_Delete'].$_POST['f14_OwnAll'] ;}
if($_POST['MGMT_Award_Punishment_Waive'] != '') { $dataAry['MGMT']['Award_Punishment'][] = $_POST['MGMT_Award_Punishment_Waive'] ;}
if($_POST['MGMT_Award_Punishment_Release'] != '') { $dataAry['MGMT']['Award_Punishment'][] = $_POST['MGMT_Award_Punishment_Release'] ;}
if($_POST['MGMT_Award_Punishment_Approval'] != '') { $dataAry['MGMT']['Award_Punishment'][] = $_POST['MGMT_Award_Punishment_Approval'] ;}
if($_POST['MGMT_Award_Punishment_Lock'] != '') { $dataAry['MGMT']['Award_Punishment'][] = $_POST['MGMT_Award_Punishment_Lock'] ;}

# GoodConduct_Misconduct
if($_POST['MGMT_GoodConduct_Misconduct_View'] != '') { $dataAry['MGMT']['GoodConduct_Misconduct'][] = $_POST['MGMT_GoodConduct_Misconduct_View'];}
if($_POST['MGMT_GoodConduct_Misconduct_New'] != '') { $dataAry['MGMT']['GoodConduct_Misconduct'][] = $_POST['MGMT_GoodConduct_Misconduct_New'] ;}
if($_POST['MGMT_GoodConduct_Misconduct_Edit'] != '') { $dataAry['MGMT']['GoodConduct_Misconduct'][] = $_POST['MGMT_GoodConduct_Misconduct_Edit'].$_POST['f11_OwnAll'] ;}
if($_POST['MGMT_GoodConduct_Misconduct_Delete'] != '') { $dataAry['MGMT']['GoodConduct_Misconduct'][] = $_POST['MGMT_GoodConduct_Misconduct_Delete'].$_POST['f15_OwnAll'] ;}
if($_POST['MGMT_GoodConduct_Misconduct_Waive'] != '') { $dataAry['MGMT']['GoodConduct_Misconduct'][] = $_POST['MGMT_GoodConduct_Misconduct_Waive'] ;}
if($_POST['MGMT_GoodConduct_Misconduct_Approval'] != '') { $dataAry['MGMT']['GoodConduct_Misconduct'][] = $_POST['MGMT_GoodConduct_Misconduct_Approval'] ;}

# Case_Record
if($_POST['MGMT_Case_Record_View'] != '') { $dataAry['MGMT']['Case_Record'][] = $_POST['MGMT_Case_Record_View'];}
if($_POST['MGMT_Case_Record_New'] != '') { $dataAry['MGMT']['Case_Record'][] = $_POST['MGMT_Case_Record_New'] ;}
if($_POST['MGMT_Case_Record_Edit'] != '') { $dataAry['MGMT']['Case_Record'][] = $_POST['MGMT_Case_Record_Edit'].$_POST['f12_OwnAll'] ;}
if($_POST['MGMT_Case_Record_Delete'] != '') { $dataAry['MGMT']['Case_Record'][] = $_POST['MGMT_Case_Record_Delete'].$_POST['f16_OwnAll'] ;}
if($_POST['MGMT_Case_Record_Lock'] != '') { $dataAry['MGMT']['Case_Record'][] = $_POST['MGMT_Case_Record_Lock'] ;}
if($_POST['MGMT_Case_Record_NewNotes'] != '') { $dataAry['MGMT']['Case_Record'][] = $_POST['MGMT_Case_Record_NewNotes'] ;}
if($_POST['MGMT_Case_Record_EditNotes'] != '') { $dataAry['MGMT']['Case_Record'][] = $_POST['MGMT_Case_Record_EditNotes'].$_POST['f28_OwnAll'] ;}
if($_POST['MGMT_Case_Record_DeleteNotes'] != '') { $dataAry['MGMT']['Case_Record'][] = $_POST['MGMT_Case_Record_DeleteNotes'].$_POST['f29_OwnAll'] ;}

# Conduct_Mark
if($_POST['MGMT_Conduct_Mark_View'] != '') { $dataAry['MGMT']['Conduct_Mark'][] = $_POST['MGMT_Conduct_Mark_View'] ;}
if($_POST['MGMT_Conduct_Mark_Adjust'] != '') { $dataAry['MGMT']['Conduct_Mark'][] = $_POST['MGMT_Conduct_Mark_Adjust'] ;}

#Detention
if($_POST['MGMT_Detention_View'] != '') { $dataAry['MGMT']['Detention'][] = $_POST['MGMT_Detention_View'] ;}
if($_POST['MGMT_Detention_Edit'] != '') { $dataAry['MGMT']['Detention'][] = $_POST['MGMT_Detention_Edit'].$_POST['f9_OwnAll'] ;}
if($_POST['MGMT_Detention_New'] != '') { $dataAry['MGMT']['Detention'][] = $_POST['MGMT_Detention_New'] ;}
if($_POST['MGMT_Detention_Delete'] != '') { $dataAry['MGMT']['Detention'][] = $_POST['MGMT_Detention_Delete'].$_POST['f18_OwnAll'] ;}
if($_POST['MGMT_Detention_Take_Attendance'] != '') { $dataAry['MGMT']['Detention'][] = $_POST['MGMT_Detention_Take_Attendance'] ;}
if($_POST['MGMT_Detention_RearrangeStudent'] != '') { $dataAry['MGMT']['Detention'][] = $_POST['MGMT_Detention_RearrangeStudent'] ;}


# - Statistics -
# Award_Punishment
if($_POST['STAT_Award_Punishment_View'] != '') { $dataAry['STAT']['Award_Punishment'][] = $_POST['STAT_Award_Punishment_View'] ;}

# GoodConduct_Misconduct
if($_POST['STAT_GoodConduct_Misconduct_View'] != '') { $dataAry['STAT']['GoodConduct_Misconduct'][] = $_POST['STAT_GoodConduct_Misconduct_View'] ;}

# Conduct_Mark
if($_POST['STAT_Conduct_Mark_View'] != '') { $dataAry['STAT']['Conduct_Mark'][] = $_POST['STAT_Conduct_Mark_View'] ;}

# Detention
if($_POST['STAT_Detention_View'] != '') { $dataAry['STAT']['Detention'][] = $_POST['STAT_Detention_View'] ;}



# - Reports -
# Award_Punishment
if($_POST['REPORTS_Award_Punishment_View'] != '') { $dataAry['REPORTS']['Award_Punishment'][] = $_POST['REPORTS_Award_Punishment_View'] ;}

# GoodConduct_Misconduct
if($_POST['REPORTS_GoodConduct_Misconduct_View'] != '') { $dataAry['REPORTS']['GoodConduct_Misconduct'][] = $_POST['REPORTS_GoodConduct_Misconduct_View'] ;}

# GoodConduct_Misconduct
if($_POST['REPORTS_Case_Record_View'] != '') { $dataAry['REPORTS']['Case_Record'][] = $_POST['REPORTS_Case_Record_View'] ;}

# Conduct_Mark
if($_POST['REPORTS_Conduct_Mark_View'] != '') { $dataAry['REPORTS']['Conduct_Mark'][] = $_POST['REPORTS_Conduct_Mark_View'] ;}

# Detention
if($_POST['REPORTS_Detention_View'] != '') { $dataAry['REPORTS']['Detention'][] = $_POST['REPORTS_Detention_View'] ;}

# Top10
if($_POST['REPORTS_Top10_View'] != '') { $dataAry['REPORTS']['Top10'][] = $_POST['REPORTS_Top10_View'] ;}

# - Settings -
# Award_Punishment
if($_POST['SETTINGS_Award_Punishment_Access'] != '') { $dataAry['SETTINGS']['Award_Punishment'][] = $_POST['SETTINGS_Award_Punishment_Access'] ;}

# GoodConduct_Misconduct
if($_POST['SETTINGS_GoodConduct_Misconduct_Access'] != '') { $dataAry['SETTINGS']['GoodConduct_Misconduct'][] = $_POST['SETTINGS_GoodConduct_Misconduct_Access'] ;}

# Conduct_Mark
if($_POST['SETTINGS_Conduct_Mark_Access'] != '') { $dataAry['SETTINGS']['Conduct_Mark'][] = $_POST['SETTINGS_Conduct_Mark_Access'] ;}

# Detention
if($_POST['SETTINGS_Detention_Access'] != '') { $dataAry['SETTINGS']['Detention'][] = $_POST['SETTINGS_Detention_Access'] ;}

# Email
if($_POST['SETTINGS_Email_Access'] != '') { $dataAry['SETTINGS']['Email'][] = $_POST['SETTINGS_Email_Access'] ;}

# Letter
if($_POST['SETTINGS_Letter_Access'] != '') { $dataAry['SETTINGS']['Letter'][] = $_POST['SETTINGS_Letter_Access'] ;}

$ldiscipline->INSERT_ACCESS_RIGHT($GroupID, $thisModule, $dataAry);


header("Location: teacher_access_right.php?xmsg=update");

intranet_closedb();

?>