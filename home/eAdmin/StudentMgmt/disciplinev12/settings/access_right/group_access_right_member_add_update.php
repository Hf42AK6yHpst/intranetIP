<?php
# modifying : 

##### Change Log [Start] #####
#
#	Date	:	2011-03-10 (Henry Chow)
#				allow assign student to access right group if $ldiscipline->AllowToAssignStudentToAccessRightGroup is true
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

for($i=0, $i_max=sizeof($student); $i<$i_max; $i++) {
	$student[$i] = (is_numeric($student[$i])) ? $student[$i] : substr($student[$i],1);	
}

$ldiscipline->NEW_ACCESS_GROUP_MEMBER($GroupID, $student);	

header("Location: group_access_right_member_list.php?xmsg=update&GroupID=$GroupID");

intranet_closedb();
?>