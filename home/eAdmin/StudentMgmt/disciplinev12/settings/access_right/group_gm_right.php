<?php 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$GroupDetails = $ldiscipline->getAccessRightGroupInfo($GroupID);
list($GroupTitle, $GroupDescription, $no_of_members) = $GroupDetails[0];
$GroupTitle = str_replace("<","&lt;",$GroupTitle);
$GroupDescription = str_replace("<","&lt;",$GroupDescription);
$GroupDescription = nl2br($GroupDescription);

$canAccessItem = $ldiscipline->getAccessGroupAccessItem($GroupID);

$good_cats = $ldiscipline->RETRIEVE_ALL_CONDUCT_CATEGORY(1);
$mis_cats = $ldiscipline->RETRIEVE_ALL_CONDUCT_CATEGORY(-1, 1);

# table content 
$tableContent = "";
$tableContent .= "<div class='table_board'>";
$tableContent .= "<table class='common_table_list rights_table_list'>";
$tableContent .= "	<tr class='seperate_row'>";
$tableContent .= "		<th width='100%' colspan='3' class='sub_row_top'>$i_Discipline_GoodConduct</td>";
$tableContent .= "	</tr>";
$tableContent .= "	<tr class='tabletop'>";
$tableContent .= "		<th width='3%' class='num_check'>#</th>";
$tableContent .= "		<th width='47%' class='row_content'>".$eDiscipline['Setting_Category']."</th>";
$tableContent .= "		<th width='30%'><span class='row_content'>".$Lang['eDiscipline']['Items']."</span>";
$tableContent .= "		<div align='right'><a href=\"javascript:goSelectAll('good', true)\">".$Lang['eDiscipline']['SelectAll']."</a>&nbsp;|&nbsp;<a href=\"javascript:goSelectAll('good', false)\">".$Lang['eDiscipline']['UnselectAll']."</a></div></th>";
$tableContent .= "	</tr>";
for($i=0; $i<sizeof($good_cats); $i++) {
	$items = $ldiscipline->getGMItemByCategoryIDWithCheckHomework($good_cats[$i][0]);
	for($j=0; $j<sizeof($items); $j++) {
		$tableContent .= "	<tr class='tabletext'>";
		if($j==0) {
			$tableContent .= "		<td valgin='top' rowspan='".sizeof($items)."'>".($i+1)."</td>";
			$tableContent .= "		<td valgin='top' rowspan='".sizeof($items)."'><input type='checkbox' name='good_cat_".$good_cats[$i][0]."' id='good_cat_".$good_cats[$i][0]."' value='1' onclick=\"goCheckSelection('category', this)\"><label for='good_cat_".$good_cats[$i][0]."'>".$good_cats[$i][1]."</label></td>";
		}
		$defaultCheck = ($canAccessItem[$good_cats[$i][0]][$items[$j][1]]) ? " checked" : "";
		$defaultClassName = ($canAccessItem[$good_cats[$i][0]][$items[$j][1]]) ? "rights_selected" : "";
		$tableContent .= "			<td class='$defaultClassName'><input type='checkbox' name='good_cat_".$good_cats[$i][0]."_".$items[$j][1]."' id='good_cat_".$good_cats[$i][0]."_".$items[$j][1]."' value='1' onclick=\"if(this.checked==false){document.getElementById('good_cat_".$good_cats[$i][0]."').checked=false;}; goCheckSelection('item',this)\" $defaultCheck><label for='good_cat_".$good_cats[$i][0]."_".$items[$j][1]."'>".$items[$j][2]."</label></td>";
		$tableContent .= "	</tr>";
	}
}
$tableContent .= "<tr><td colspan='3'>&nbsp;</td></tr>";
$tableContent .= "	<tr class='seperate_row'>";
$tableContent .= "		<th width='100%' colspan='3' class='sub_row_top'>$i_Discipline_Misconduct</th>";
$tableContent .= "	</tr>";
$tableContent .= "	<tr class='tabletop'>";
$tableContent .= "		<th width='3%'>#</th>";
$tableContent .= "		<th width='47%' class='num_check'>".$eDiscipline['Setting_Category']."</th>";
$tableContent .= "		<th width='30%'><span class='row_content'>".$Lang['eDiscipline']['Items']."</span>";
$tableContent .= "		<div align='right'><a href=\"javascript:goSelectAll('mis', true)\">".$Lang['eDiscipline']['SelectAll']."</a>&nbsp;|&nbsp;<a href=\"javascript:goSelectAll('mis', false)\">".$Lang['eDiscipline']['UnselectAll']."</a></div></th>";
$tableContent .= "	</tr>";
/*
$tableContent .= "	<tr class='tabletext'>";
$tableContent .= "		<td width='3%'>1</th>";
$defaultCheck = ($canAccessItem[1][0]) ? " checked" : "";
$tableContent .= "		<td width='47%'><input type='checkbox' name='mis_cat_1' id='mis_cat_1' value='1' onclick=\"goCheckSelection('category', this)\" $defaultCheck><label for='mis_cat_1'>".$ldiscipline->getGMCatNameByCatID(1)."</label></th>";
$tableContent .= "		<td width='50%'>--</td>";
$tableContent .= "	</tr>";
*/
$k = 1;
for($i=0; $i<sizeof($mis_cats); $i++) {
	if($mis_cats[$i][0] != '') {
		$items = $ldiscipline->getGMItemByCategoryIDWithCheckHomework($mis_cats[$i][0]);
		for($j=0; $j<sizeof($items); $j++) {
			$tableContent .= "	<tr class='tabletext'>";
			if($j==0) {
				$tableContent .= "		<td width='3%' valgin='top' rowspan='".sizeof($items)."'>$k</td>";
				$tableContent .= "		<td width='47%' valgin='top' rowspan='".sizeof($items)."'><input type='checkbox' name='mis_cat_".$mis_cats[$i][0]."' id='mis_cat_".$mis_cats[$i][0]."' value='1' onclick=\"goCheckSelection('category', this)\"><label for='mis_cat_".$mis_cats[$i][0]."'>".$mis_cats[$i][1]."</label></td>";
			}
			$defaultCheck = ($canAccessItem[$mis_cats[$i][0]][$items[$j][1]]) ? " checked" : "";
			$defaultClassName = ($canAccessItem[$mis_cats[$i][0]][$items[$j][1]]) ? "rights_selected" : "";
			$tableContent .= "			<td class='$defaultClassName'><input type='checkbox' name='mis_cat_".$mis_cats[$i][0]."_".$items[$j][1]."' id='mis_cat_".$mis_cats[$i][0]."_".$items[$j][1]."' value='1' onclick=\"if(this.checked==false){document.getElementById('mis_cat_".$mis_cats[$i][0]."').checked=false;}; goCheckSelection('item', this)\" $defaultCheck><label for='mis_cat_".$mis_cats[$i][0]."_".$items[$j][1]."'>".$items[$j][2]."</label></td>";
			$tableContent .= "	</tr>";
		}
		$k++;
	}
}
$tableContent .= "</table>";
$tableContent .= "</div>";


# Top menu highlight setting
$CurrentPage = "Settings_AccessRight";

$TAGS_OBJ[] = array($i_Discipline_System_Discipline_Members_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/group_access_right.php", 1);
//TAGS_OBJ[] = array($i_Discipline_System_Teacher_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/teacher_access_right.php", 0);
$TAGS_OBJ[] = array($i_Discipline_System_Student_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/student_access_right.php", 0);

$subTag[] = array($Lang['eDiscipline']['AccessRight_General_Right'], "group_access_right_view.php?GroupID=$GroupID", 0);
$subTag[] = array($Lang['eDiscipline']['AccessRight_GM_Right'], "group_gm_right.php?GroupID=$GroupID", 1);
$subTag[] = array($Lang['eDiscipline']['AccessRight_AP_Right'], "group_ap_right.php?GroupID=$GroupID", 0);
$subTag[] = array($Lang['eDiscipline']['AccessRight_UserList'], "group_access_right_member_list.php?GroupID=$GroupID", 0);

$PAGE_NAVIGATION[] = array($i_Discipline_System_Discipline_Group_Access_Rights, "group_access_right.php");
$PAGE_NAVIGATION[] = array($GroupTitle, "group_access_right_view.php?GroupID=$GroupID");
//$PAGE_NAVIGATION[] = array($button_edit, "");

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

?>
<script language="javascript">
<!--
function goCheckSelection(elementType, elementRef) {
	if(elementType=='item') {
		elementRef.parentNode.className = (elementRef.checked) ? 'rights_selected' : '';
	} else {
		var len = document.form1.elements.length;
		var catNameLen = elementRef.name.toString().length;
		for(var i=0; i<len; i++) {
			if(document.form1.elements[i].name.substring(0,catNameLen+1) == (elementRef.name+"_")) {
				if(elementRef.checked) {
					document.form1.elements[i].checked = true;	
				} else {
					document.form1.elements[i].checked = false;	
				}
				goCheckSelection('item', document.form1.elements[i]);
			}
		}
	}
}

function goSelectAll(name, flag) {
	var len = document.form1.elements.length;
	for(var i=0; i<len; i++) {
		if(document.form1.elements[i].name.substring(0,4)==name || document.form1.elements[i].name.substring(0,3)==name) {
			document.form1.elements[i].checked = flag;
			
			if((document.form1.elements[i].name.split("_").length - 1) > 2)
				goCheckSelection('item', document.form1.elements[i]);
		}
	}
}

function goReset() {
	document.form1.reset();
	var len = document.form1.elements.length;
	for(var i=0; i<len; i++) {
		if(document.form1.elements[i].checked==true && (document.form1.elements[i].name.split("_").length - 1) > 2) {
			goCheckSelection('item', document.form1.elements[i]);
		} else {
			document.form1.elements[i].parentNode.className = "";
		}
	}
}
//-->
</script>
<br />
<form name="form1" method="post" action="group_gm_right_update.php">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="40%"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
					<td align="right"><?= $linterface->GET_SYS_MSG($xmsg) ?></td>
				</tr>
			</table>
			<br />
			<table align="center" width="95%" border="0" cellpadding="5" cellspacing="0">
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Group_Right_Navigation_Group_Title?></td>
					<td><?=$GroupTitle ?></td>
				</tr>
				<tr valign="top">
					<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Group_Right_Navigation_Group_Description?></td>
					<td><?=$GroupDescription ?></td>
				</tr>
				<!--
				<tr valign="top">
					<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Group_Right_No_Of_Members?></td>
					<td><a class="tablelink" href="group_access_right_member_list.php?GroupID=<?=$GroupID?>"><?=$no_of_members ?></a></td>
				</tr>
				-->
			</table>
			<br />
			<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td class="tab_underline">
						<div class="shadetabs">
							<ul>
								<?=$ldiscipline->getSubTag($subTag)?>
							</ul>
						</div>
					</td>
				</tr>
			</table>
			<br style="clear:both" />
			<?=$tableContent?>							
			<br>
			<input type="hidden" name="GroupID" value="<?= $GroupID ?>">
			<div align="center">
			<?= $linterface->GET_ACTION_BTN($button_save, "submit")?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "goReset()")?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='group_access_right.php'")?>
			</div>
	</td>
	</tr>
</table>
</form>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>