<?php

// Using: 
/*************************************************
 *	Date: 2013-06-14 (Rita) 
 *  Details: create this page for yy3 cust
 *************************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

# Access Right 
if(!$sys_custom['eDiscipline']['yy3']){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

# Page Setting
$PageSettings = 1;
$CurrentPage = "Settings_Max_Gain_Deduct_Mark";
$CurrentPageArr['eDisciplinev12'] = 1;

# Tag
$TAGS_OBJ[] = array($i_Merit_Award, "category.php?Merit=1", 0);
$TAGS_OBJ[] = array($i_Merit_Punishment, "category.php?Merit=-1", 0);
$TAGS_OBJ[] = array($Lang['eDiscipline']['Tag'], "tag.php", 1);
$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Semester, "semester.php", 0);

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$xmsg = $_GET['xmsg'];
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
   
		
# Get Tag Info Arr 
$tagInfoArr = $ldiscipline->getTagSetting();
$numOfMeritCatArr = count($tagInfoArr);

$displayTable = '';
$displayTable = '<form name="form1" id="form1" method="post" action="tag_edit.php">';
	$displayTable .='<div class="common_table_tool">';
	$displayTable .='<a href="javascript:submitForm(document.form1,\'TagID[]\',\'award_category_item_edit.php\')" class="tool_edit">'.$Lang['Button']['Edit'].'</a>';
	$displayTable .='</div>';
					 
	$displayTable .= '<table class="common_table_list_v30" width="100%" align="center" border="0" cellspacing="0" cellpadding="4">';
		$displayTable .= '<thead>';
			$displayTable .= '<tr>';
				$displayTable .= '<th width="5" class="tabletop tabletopnolink">#</th>';
				$displayTable .= '<th width="50%" class="tabletop tabletopnolink">'.$Lang['eDiscipline']['TagName'].'</th>';
				$displayTable .= '<th width="25%" class="tabletop tabletopnolink">'.$Lang['eDiscipline']['MaximumGainMark'].'</th>';
				$displayTable .= '<th width="25%" class="tabletop tabletopnolink">'.$Lang['eDiscipline']['MaximumDeductMark'].'</th>';
				$displayTable .= '<th width="5" class="tabletop tabletopnolink"><input type="checkbox" onclick="javascript:js_Select_All_Item()"></th>';
			$displayTable .= '</tr>';
		$displayTable .= '</thead>';
	
		$displayTable .= '<tbody>';
		for($i=0;$i<$numOfMeritCatArr;$i++){
			$thisTagId = $tagInfoArr[$i]['TagID'];
			$thisTagName = $tagInfoArr[$i]['TagName'];
			$thisMaxGainMark = Get_String_Display($tagInfoArr[$i]['MaxGainMark']);
			$thisMaxDeductMark = Get_String_Display($tagInfoArr[$i]['MaxDeductMark']);
			$thisCheckBox = $tagInfoArr[$i]['CheckBox'];
			
			$displayTable .= '<tr>';			
			$displayTable .= '<td>' . ($i+1) . '</td>';	
			$displayTable .= '<td>' . $thisTagName . '</td>';			
			$displayTable .= '<td>' . $thisMaxGainMark . '</td>';
			$displayTable .= '<td>' . $thisMaxDeductMark . '</td>';
			$displayTable .= '<td>' .  $thisCheckBox . '</td>';			
			$displayTable .= '</tr>';
		}
		
		$displayTable .= '</tbody>';
	$displayTable .= '</table>';
$displayTable .= '</form>';

echo $displayTable;

?>
<script language="javascript">
function submitForm(obj,page,action)
{
	if($('.TagID:checkbox:checked').length==0){
		alert(globalAlertMsg2);
	}else{
		obj.submit();
	}
}

function js_Select_All_Item(){
	if($('.TagID:checkbox:checked').length==0){
		$('.TagID').attr('checked', 'checked');
	}else{
		$('.TagID').attr('checked', '');
	}	
}
</script>

<?
echo $linterface->FOCUS_ON_LOAD("form1.TagName"); 
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
