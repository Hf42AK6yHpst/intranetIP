<?php

// Using: 
/*************************************************
 *	Date: 2013-06-14 (Rita) 
 *  Details: create this page for yy3 cust
 *************************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

# Access Right 
if(!$sys_custom['eDiscipline']['yy3']){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}
				
# menu highlight setting
$PageSettings = 1;
$CurrentPage = "Settings_Max_Gain_Deduct_Mark";
$CurrentPageArr['eDisciplinev12'] = 1;

$TAGS_OBJ[] = array($i_Merit_Award, "category.php?Merit=1", 0);
$TAGS_OBJ[] = array($i_Merit_Punishment, "category.php?Merit=-1", 0);
$TAGS_OBJ[] = array($Lang['eDiscipline']['Tag'], "tag.php", 1);
$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Semester, "semester.php", 0);

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

# Get Post Value 
$tagIDArr = $_POST['TagID'];

# Get Tag Info Arr 
$tagInfoArr = $ldiscipline->getTagSetting($tagIDArr);
$numOfMeritCatArr = count($tagInfoArr);

$displayTable = '';
//$displayTable = '<form name="form1" id="form1" method="post" action="tag_update.php">';				 
	$displayTable .= '<table class="common_table_list view_table_list" width="100%" align="center" border="0" cellspacing="0" cellpadding="4">';
		
		## Table Header 
		$displayTable .= '<thead>';
			$displayTable .= '<tr>';
			$displayTable .= '<th width="5" class="tabletop tabletopnolink">#</th>';
			$displayTable .= '<th width="50%" class="tabletop tabletopnolink">'.$Lang['eDiscipline']['TagName'].'</th>';
			$displayTable .= '<th width="25%" class="tabletop tabletopnolink">'.$Lang['eDiscipline']['MaximumGainMark'].'</th>';
			$displayTable .= '<th width="25%" class="tabletop tabletopnolink">'.$Lang['eDiscipline']['MaximumDeductMark'].'</th>';
			$displayTable .= '</tr>';
		$displayTable .= '</thead>';
	
		$displayTable .= '<tbody>';
		for($i=0;$i<$numOfMeritCatArr;$i++){
			$thisTagId = $tagInfoArr[$i]['TagID'];
			$thisTagName = $tagInfoArr[$i]['TagName'];
			$thisGainMarkTextBox= $tagInfoArr[$i]['GainMarkTextBox'];
			$thisDeductMarkTextBox= $tagInfoArr[$i]['DeductMarkTextBox'];
			
			$displayTable .= '<tr>';
			$displayTable .= '<td>' . ($i+1) . '</td>';
			$displayTable .= '<td>' . $thisTagName . '</td>';
			$displayTable .= '<td>' . $thisGainMarkTextBox . '</td>';
			$displayTable .= '<td>' . $thisDeductMarkTextBox . '</td>';			
			$displayTable .= '</tr>';
		}
		
		$displayTable .= '</tbody>';
	$displayTable .= '</table>';
//$displayTable .= '</form>';


?>
<script language="javascript" type="text/javascript">
function checkForm(obj) {
	var error = 0;
	
	$('.MaxGainMark').each(	
		function(){
			if($.trim($(this).val())==''){
				 error++;
			}
		}
	);
	$('.MaxDeductMark').each(	
		function(){
			if($.trim($(this).val())==''){
				 error++;
			}
		}
	);
	
	if(error>0){
		alert('<?php echo $i_alert_pleasefillin.$Lang['eDiscipline']['MaximumGainDeductMark'];?>');
		return false;	
	}else{
		return true;
	}
}
</script>
<form name="form1" method="post" action="tag_update.php" onsubmit="return checkForm(this);">
<br />
<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>

<table width="100%" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td>
		<?=$displayTable?>
		</td>
	</tr>
	<tr>
		<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td align="right">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">									
						<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='tag.php'")?>									
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" name="merit" value="<?= $merit?>">
<input type="hidden" name="CatID" value="<?= $CatID?>">
<input type="hidden" name="ItemID" value="<?=$ItemID?>">
</form>
<?
echo $linterface->FOCUS_ON_LOAD("form1.TagName"); 

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
