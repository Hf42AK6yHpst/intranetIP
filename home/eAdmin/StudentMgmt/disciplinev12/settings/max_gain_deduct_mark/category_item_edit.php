<?php

// Using: 
/*************************************************
 *	Date: 2013-06-14 (Rita) 
 *  Details: create this page for yy3 cust
 *************************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

# Access Right 
if(!$sys_custom['eDiscipline']['yy3']){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

# Page Setting
$PageSettings = 1;
$CurrentPage = "Settings_Max_Gain_Deduct_Mark";
$CurrentPageArr['eDisciplinev12'] = 1;

# Get Post Value
$Merit = $_POST['Merit'];
$catIDItemIDArr = $_POST['CatIDItemID'];
$catIdArr = array_keys($catIDItemIDArr);
$catId = $catIdArr[0];


# Tag 
$awardTag = 0;
$punishTag = 0;

if($Merit==1){
	$awardTag = 1;
}else{
	$punishTag = 1;
}

$TAGS_OBJ[] = array($i_Merit_Award, "category.php?Merit=1", $awardTag);
$TAGS_OBJ[] = array($i_Merit_Punishment, "category.php?Merit=-1", $punishTag);
$TAGS_OBJ[] = array($Lang['eDiscipline']['Tag'], "tag.php", 0);
$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Semester, "semester.php", 0);

# Navigation
$catName = $ldiscipline->getAwardPunishCategoryName($catId);
$catNameDisplay = str_replace("<","&lt;",$catName);



# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

if($Merit==1){
	$catInfoArr = $ldiscipline->GetMeritCat($catId);
}else{
	$catInfoArr = $ldiscipline->GetDemeritCat($catId);
}


$PAGE_NAVIGATION[] = array($i_Discipline_System_CategoryList, 'category.php');
$PAGE_NAVIGATION[] = array($catInfoArr[0]['CategoryName'],"category_item.php?Merit=$Merit&id=$catId");
$PAGE_NAVIGATION[] = array($Lang['Button']['Edit'],"");



$displayTable = '';
$displayTable .= '<table class="common_table_list view_table_list" width="100%" align="center" border="0" cellspacing="0" cellpadding="4">';
$displayTable .= '<thead>';
$displayTable .= '<tr>';
$displayTable .= '<th width="1" class="tabletop tabletopnolink">#</th>';
$displayTable .= '<th width="32%" class="tabletop tabletopnolink">'.$i_Discipline_System_CategoryName.'</th>';
$displayTable .= '<th width="40%" class="tabletop tabletopnolink">'.$Lang['eDiscipline']['sdbnsm_ap_item'].'</th>';
if($Merit==1){
	$maxMarkNameDisplay = $Lang['eDiscipline']['MaximumGainMark'];
}else{
	$maxMarkNameDisplay = $Lang['eDiscipline']['MaximumDeductMark'];
}



$displayTable .= '<th width="25%" class="tabletop tabletopnolink">';
$displayTable .= $maxMarkNameDisplay . '<br />';


$displayTable .='<input type="textbox"  id="AllMaxGainDeductMark" value="" style="float:left"/> ';
//$displayTable .='<input type="button" class="formsmallbutton " onclick="javascript:Js_Apply_MaxGainDeductMark();" value="Set All" onmouseover="this.className=\'formsmallbuttonon \'" onmouseout="this.className=\'formsmallbutton \'">';

$displayTable .= $linterface->Get_Apply_All_Icon('javascript:Js_Apply_MaxGainDeductMark();', $Title='', $OtherTag='style="float:left;"');

$displayTable .='</th>';

$displayTable .= '</tr>';
$displayTable .= '</thead>';
$displayTable .= '<tbody>';

$numOfCatInfoArr = count($catInfoArr);
for($i=0;$i<$numOfCatInfoArr;$i++){
	$thisCatName = $catInfoArr[$i]['CategoryName'];
	$thisCatID = $catInfoArr[$i]['CatID'];
	$thisItemIDArr = $catIDItemIDArr[$thisCatID];
	
	if($Merit==1){
		$thisItemInfoArr = $ldiscipline->GetMeritCatItem($thisCatID,$thisItemIDArr);
	}else{
		$thisItemInfoArr = $ldiscipline->GetDemeritCatItem($thisCatID,$thisItemIDArr);
	}
	
	$numOfthisItemInfoArr = count($thisItemInfoArr);
	
	$displayTable .= '<tr>';
	$displayTable .= '<td rowspan="'.$numOfthisItemInfoArr.'">'.($i+1).'</td>';
	$displayTable .= '<td rowspan="'.$numOfthisItemInfoArr.'">'.$thisCatName.'</td>';
	
	for($j=0;$j<$numOfthisItemInfoArr;$j++){
		$thisItemName = $thisItemInfoArr[$j]['ItemName'];
		$thisTextBox = $thisItemInfoArr[$j]['TextBox'];
		
		$displayTable .= '<td>';
		$displayTable .= $thisItemName;
		$displayTable .= '</td>';	
		$displayTable .= '<td>';
		$displayTable .= $thisTextBox;	
		$displayTable .= '</td>';
		$displayTable .= '</tr>';	
	}
		
	$displayTable .= '</tr>';
}

$displayTable .= '</tbody>';
$displayTable .= '</table>';

?>
<script language="javascript">
function checkForm(obj) {
	var error = 0;
	
	$('.MaxGainDeductMark').each(	
		function(){
			var thisVal = $.trim($(this).val());
			if(!isPositiveFloatOrZero(thisVal)){
				 error++;
			}
		}
	);

	if(error>0){
		alert('<?php echo $i_alert_pleasefillin.$maxMarkNameDisplay;?>');
		return false;	
	}
	
	
}

function Js_Apply_MaxGainDeductMark(){
	var allMaxGainDeductMark = $.trim($('#AllMaxGainDeductMark').val());
	if(allMaxGainDeductMark != '')
		$('.MaxGainDeductMark').val(allMaxGainDeductMark);

}

function isPositiveFloatOrZero(val)
{
	var f = parseFloat(val);
	if(f != 0 && (isNaN(f) || f == '' || val == '' || f < 0.0)) {
		return false;
	}
	return true;
}
</script>
<form name="form1" method="post" action="category_item_update.php" onSubmit="return checkForm(form1)">
<br />
<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>

<table width="100%" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td>
		<?=$displayTable?>
		</td>
	</tr>
	<tr>
		<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td align="right">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">						
						<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='category_item.php?id=$catId&Merit=$Merit'")?>						
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" name="Merit" value="<?=$Merit?>">
<input type="hidden" name="CatID" value="<?=$catId?>">
</form>
<?
echo $linterface->FOCUS_ON_LOAD("form1.TagName"); 

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
