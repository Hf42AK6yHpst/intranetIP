<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$ldiscipline_ui = new libdisciplinev12_ui_cust();

# Access Right 
if(!$sys_custom['eDiscipline']['yy3']){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

# Page Setting
$PageSettings = 1;
$CurrentPage = "Settings_Conduct_Down_Grade_Condition";
$CurrentPageArr['eDisciplinev12'] = 1;

$TAGS_OBJ[] = array($Lang['eDiscipline']['ConductDownGradeCondition'], "index.php", 1);

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$down_grade_items = $ldiscipline->GetConductDownGradeConditionRecords();
$down_grade_item_ids = Get_Array_By_Key($down_grade_items,'ItemID');

$demerit_cats = $ldiscipline->returnMeritItemCategoryByType(-1);

$demerit_cat_selection = getSelectByArray($demerit_cats, ' id="CatID" name="CatID" onchange="jsOnChangeCat();" ', $selected=$demerit_cats[0]['CatID'], $all=0, $noFirst=1, $FirstTitle="", $ParQuoteValue=1);
$demerit_item_selection = getSelectByArray(array(), ' id="ItemID" name="ItemID" ', $selected="", $all=0, $noFirst=1, $FirstTitle="", $ParQuoteValue=1);

$addRowHtml = '<tr id="NewRow">';
$addRowHtml.= '<td>'.$demerit_cat_selection.'</td>';
$addRowHtml.= '<td>'.$demerit_item_selection.'&nbsp;<span id="ItemWarnDiv" style="display:none;color:red;">'.$Lang['eDiscipline']['WarningMsgArr']['InvalidItem'].'</span></td>';
$addRowHtml.= '<td><input type="text" id="Quantity" name="Quantity" value="" class="textboxnum" size="5" onchange="jsOnChangeQuantity(this);" />&nbsp;<span id="QuantityWarnDiv" style="display:none;color:red;">'.$Lang['eDiscipline']['WarningMsgArr']['InvalidQuantity'].'</span></td>';
$addRowHtml.= '<td>';
$addRowHtml.= $ldiscipline_ui->GET_SMALL_BTN($Lang['Btn']['Submit'], "button", $ParOnClick="jsSubmitNewRow(this);", $ParName="", $ParOtherAttribute="", $OtherClass="", $ParTitle='').'&nbsp;';
$addRowHtml.= $ldiscipline_ui->GET_SMALL_BTN($Lang['Btn']['Cancel'], "button", $ParOnClick="jsCancelNewRow(this);", $ParName="", $ParOtherAttribute="", $OtherClass="", $ParTitle='');
$addRowhtml.= '</td>';
$addRowHtml.= '</tr>';

$js_cat_item_array = 'var cat_item_array = [];'."\n";
for($i=0;$i<count($demerit_cats);$i++) {
	$sql = "SELECT ItemID,ItemName FROM DISCIPLINE_MERIT_ITEM WHERE CatID='".$demerit_cats[$i]['CatID']."' AND (RecordStatus IS NULL OR RecordStatus!=-1) ORDER BY ItemName";
	$item_ary = $ldiscipline->returnArray($sql);
	$option_str = '';
	for($j=0;$j<count($item_ary);$j++) {
		if(in_array($item_ary[$j]['ItemID'],$down_grade_item_ids)) {
			continue;
		}
		$option_str .= str_replace('"','\"','<option value="'.$item_ary[$j]['ItemID'].'">'.$item_ary[$j]['ItemName'].'</option>');
	}
	$js_cat_item_array .= 'cat_item_array['.$demerit_cats[$i]['CatID'].']="'.$option_str.'";'."\n";
}

# Start layout
$xmsg = $_GET['xmsg'];
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

$x = '';
$x .= $ldiscipline_ui->getConductDownGradeSettingTable();

?>
<script type="text/javascript" language="JavaScript">
<?=$js_cat_item_array?>

var newRowHtml = '<?=str_replace(array('\'',"\n"), array('\\\'',""),$addRowHtml)?>';

function jsAddRow()
{
	var btnRow = $('tr#AddToolRow');
	if($('tr#NewRow').length==0) {
		btnRow.before(newRowHtml);
		jsOnChangeCat();
	}else{
		$('tr#NewRow select#CatID').focus();
	}
}

function jsSubmitNewRow()
{
	var CatID = $('select#CatID').val();
	var ItemID = $('select#ItemID').val();
	var Quantity = $.trim($('#Quantity').val());
	var Quantity_num = parseInt(Quantity);
	var pattern = /^\d+$/g;
	var item_warn_dv = $('#ItemWarnDiv');
	var quantity_warn_dv = $('#QuantityWarnDiv');
	var is_valid = true;
	
	if(Quantity == '' || isNaN(Quantity_num) || Quantity_num <=0 || !pattern.test(Quantity)){
		quantity_warn_dv.show();
		is_valid = false;
		$('#Quantity').focus();
	}else{
		quantity_warn_dv.hide();
	}
	
	if(ItemID == '') {
		item_warn_dv.show();
		is_valid = false;
		$('select#ItemID').focus();
	}else{
		item_warn_dv.hide();
	}
	
	if(is_valid) {
		var frm = document.getElementById('form1');
		frm.action = 'add_update.php';
		frm.submit();
	}
}

function jsCancelNewRow()
{
	$('tr#NewRow').remove();
}

function jsOnChangeCat()
{
	var CatID = $('select#CatID').val();
	if(cat_item_array[CatID]) {
		$('select#ItemID').html(cat_item_array[CatID]);
	}else{
		$('select#ItemID').html('<option value=""><?=$Lang['General']['EmptySymbol']?></option>');
	}
}

function jsOnChangeQuantity(obj)
{
	var val = obj.value.Trim();
	var num = parseInt(val);
	var pattern = /^\d+$/g;
	var warn_dv = $(obj).siblings('span');
	if(val == '' || isNaN(num) || num <= 0 || !pattern.test(val)) {
		warn_dv.show();
		//obj.value = '';
		obj.focus();
		return false;
	}else{
		warn_dv.hide();
		return true;
	}
}

function jsDeleteItem(itemId)
{
	if(confirm('<?=$Lang['General']['JS_warning']['ConfirmDelete']?>')) {
		var frm = document.getElementById('form1');
		frm.action = 'delete_update.php?ItemID='+itemId;
		frm.submit();
	}
}

function jsShowEditBackground(obj)
{
	obj.style.backgroundImage = "url(<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/icon_edit_b.gif)";
	obj.style.backgroundPosition = "center right";
	obj.style.backgroundRepeat = "no-repeat";
}

function jsHideEditBackground(obj)
{
	obj.style.backgroundImage = "";
	obj.style.backgroundPosition = "";
	obj.style.backgroundRepeat = "";
}

function jsEditQuantity(itemId)
{
	var display_dv = $('div#DisplayQuantityDiv_'+itemId);
	var edit_dv = $('div#EditQuantityDiv_'+itemId);
	
	$('#Quantity_'+itemId).val($('#OrigQuantity_'+itemId).val());
	$('#Quantity_'+itemId).focus();
	
	display_dv.hide();
	edit_dv.show();
}

function jsSubmitEditItem(itemId)
{
	var quantityObj = document.getElementById('Quantity_'+itemId);
	
	if(jsOnChangeQuantity(quantityObj)) {
		var Quantity = $.trim(quantityObj.value);
		$.post(
			'edit_update.php',
			{
				'ItemID':itemId,
				'Quantity':Quantity
			},
			function(ReturnMsg){
				var ResultCode = ReturnMsg.split('|=|')[0];
				Get_Return_Message(ReturnMsg);
				
				if(ResultCode == '1'){
					$('div#DisplayQuantityDiv_'+itemId).html(Quantity);
					$('#OrigQuantity_'+itemId).val(Quantity);
				}
				jsCancelEditItem(itemId);
			}
		);
	}
}

function jsSubmitEditItemByEnterKey(evt, itemId)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		jsSubmitEditItem(itemId);
	}
}

function jsCancelEditItem(itemId)
{
	var display_dv = $('div#DisplayQuantityDiv_'+itemId);
	var edit_dv = $('div#EditQuantityDiv_'+itemId);
	
	$('div#EditQuantityDiv_' + itemId + ' span').hide();
	
	display_dv.show();
	edit_dv.hide();
}
</script>
<br />
<form name="form1" id="form1" method="post" action="">
<div id="MainTableDiv">
<?=$x?>
</div>
</form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>