<?php
// using : Bill

/**************************************** Change Log ****************************************/
#
#	Date	:	20190513 (Bill)
#	Details :	Prevent SQL Injection
#
#	Date	:	20160318 (Bill)
#	Details :	Fixed: ensure re-calculate correct conduct mark on the calculation method (Year to Term)
#
#	Date	:	20091208 (Ronald)
#	Details :	Re-calculate the conduct mark base on the calculation method (Keyword: Log #1)
#
/************************************ End Of Change Log ************************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

## Get Current Year ID 
$curr_year_id = Get_Current_Academic_Year_ID();

$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Conduct_Mark-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

## Update Conduct Mark Calculation Period
$ldiscipline->updateConductMarkCalculationMethod($CalculationMethod);

## Update Base Mark
$baseMark = IntegerSafe($baseMark);
$pre_baseMark = $ldiscipline->editConductMarkRule('baseMark', $baseMark); 
$difference = $baseMark - $pre_baseMark;
if($pre_baseMark != '' && $difference != 0)
{
	# Update Student Conduct Balance & Change Log 
	$ldiscipline->updateConductBalanceFrBaseMark($difference);
}

## Re-calculate the conduct mark base on the calculation method (Log #1)
if($CalculationMethod == 0)
{
	## re-calcuate : from year to term (not accumlate)
	$sql = "SELECT DISTINCT StudentID FROM DISCIPLINE_STUDENT_CONDUCT_BALANCE WHERE AcademicYearID = '$curr_year_id' AND (IsAnnual IS NULL OR IsAnnual != 1)";
	$student = $ldiscipline->returnArray($sql,1);
	for($i=0; $i<sizeof($student); $i++)
	{
		list($student_id) = $student[$i];
		
		// Get total conduct score change from approved and released AP records
		// $sql = "SELECT YearTermID, SUM(ConductScoreChange) FROM DISCIPLINE_MERIT_RECORD WHERE StudentID = '$student_id' AND AcademicYearID = '$curr_year_id' GROUP BY YearTermID";
		$sql = "SELECT YearTermID, SUM(ConductScoreChange) FROM DISCIPLINE_MERIT_RECORD WHERE StudentID = '$student_id' AND AcademicYearID = '$curr_year_id' AND RecordStatus = 1 AND ReleaseStatus = 1 GROUP BY YearTermID";
		$temp_result = $ldiscipline->returnArray($sql,2);
		if(sizeof($temp_result) > 0)
		{
			for($j=0; $j<sizeof($temp_result); $j++) {
				list($year_term_id, $conduct_score_change) = $temp_result[$j];
				$updated_conduct_mark = $baseMark + $conduct_score_change;
				
				$sql = "UPDATE DISCIPLINE_STUDENT_CONDUCT_BALANCE SET ConductScore = '$updated_conduct_mark' WHERE StudentID = '$student_id' AND AcademicYearID = '$curr_year_id' AND YearTermID = '$year_term_id' AND (IsAnnual IS NULL OR IsAnnual != 1)";
				$ldiscipline->db_db_query($sql);
			}
		}
	}
}
else
{
	## re-calcuate : from term to year (accumlate)
	$sql = "SELECT YearTermID FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '$curr_year_id'";
	$arrYearTerm = $ldiscipline->returnArray($sql,1);
	
	$sql = "SELECT DISTINCT StudentID FROM DISCIPLINE_STUDENT_CONDUCT_BALANCE WHERE AcademicYearID = '$curr_year_id' AND (IsAnnual IS NULL OR IsAnnual != 1)";
	$student = $ldiscipline->returnArray($sql,1);
	for($i=0; $i<sizeof($student); $i++)
	{
		list($student_id) = $student[$i];
		$temp_baseMark = $baseMark;
		
		for($k=0; $k<sizeof($arrYearTerm); $k++)
		{
			list($target_year_term_id) = $arrYearTerm[$k];
            
			$sql = "SELECT YearTermID, SUM(ConductScoreChange) FROM DISCIPLINE_MERIT_RECORD WHERE StudentID = '$student_id' AND AcademicYearID = '$curr_year_id' AND YearTermID = '$target_year_term_id' AND RecordStatus = 1 AND ReleaseStatus = 1 GROUP BY YearTermID";
			$temp_result = $ldiscipline->returnArray($sql,2);
			if(sizeof($temp_result) > 0)
			{
				for($j=0; $j<sizeof($temp_result); $j++) {
					list($year_term_id, $conduct_score_change) = $temp_result[$j];
					$updated_conduct_mark = $temp_baseMark + $conduct_score_change;
	
					$sql = "UPDATE DISCIPLINE_STUDENT_CONDUCT_BALANCE SET ConductScore = '$updated_conduct_mark' WHERE StudentID = '$student_id' AND AcademicYearID = '$curr_year_id' AND YearTermID = '$year_term_id' AND (IsAnnual IS NULL OR IsAnnual != 1)";
					$ldiscipline->db_db_query($sql);
					
					$temp_baseMark = $updated_conduct_mark;
				}
			}
			else
			{
				$sql = "DELETE FROM DISCIPLINE_STUDENT_CONDUCT_BALANCE WHERE AcademicYearID = '$curr_year_id' AND YearTermID = '$target_year_term_id' AND StudentID = '$student_id'";
				$ldiscipline->db_db_query($sql);
			}
		}
		/*
		$sql = "SELECT YearTermID, SUM(ConductScoreChange) FROM DISCIPLINE_MERIT_RECORD WHERE StudentID = '$student_id' AND AcademicYearID = '$curr_year_id' GROUP BY YearTermID";
		$temp_result = $ldiscipline->returnArray($sql,2);
		$temp_baseMark = $baseMark;
		if(sizeof($temp_result)>0) {
			for($j=0; $j<sizeof($temp_result); $j++) {
				list($year_term_id, $conduct_score_change) = $temp_result[$j];
				$updated_conduct_mark = $temp_baseMark + $conduct_score_change;

				$sql = "UPDATE DISCIPLINE_STUDENT_CONDUCT_BALANCE SET ConductScore = '$updated_conduct_mark' WHERE StudentID = '$student_id' AND AcademicYearID = '$curr_year_id' AND YearTermID = '$year_term_id' AND (IsAnnual is null or IsAnnual!=1)";
				$ldiscipline->db_db_query($sql);
				
				$temp_baseMark = $updated_conduct_mark;
			}
		}
		*/
	}
}

header("Location: base_mark.php?xmsg=update");

intranet_closedb();
?>