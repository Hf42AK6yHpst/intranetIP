<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Conduct_Mark-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# menu highlight setting
$CurrentPage = "Settings_ConductMark";

$CalculationMethod = $ldiscipline->retriveConductMarkCalculationMethod();

$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Warning_Rules, "warning_rule.php", 0);
$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Base_Mark, "base_mark.php", 1);
$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Grading_Scheme, "grading_scheme.php", 0);
if($CalculationMethod == 0) {
	$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Semester_Ratio, "semester_ratio.php", 0);
}
if($sys_custom['eDiscipline']['WSCSS_Conduct_Grade']) {
	$TAGS_OBJ[] = array($Lang['eDiscipline']['ConductGradeSetting'], "wscss_grading_grade_setting.php", 0);		
}

$baseMark = $ldiscipline->getConductMarkRule('baseMark');
$baseMark = ($baseMark=='') ? "N/A" : $baseMark;

if($xmsg != '') {
	$showmsg = "<table width=90% cellspacing=0 cellpadding=5 border=0>";	
	$showmsg .= "<tr><td align=right>";
	$showmsg .= $linterface->GET_SYS_MSG($xmsg);
	$showmsg .= "</td></tr>";
	$showmsg .= "</table>";
}

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

if($CalculationMethod != 0){
	$str_CalculationMethod = $Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['ByAcademicYear'];
}else{
	$str_CalculationMethod = $Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['ByTerm'];
}

# Start layout
$linterface->LAYOUT_START();
?>
<script language="javascript">
<!--
function showAlertMsg() {
	var alertMsg = "<?=$i_Discipline_System_Conduct_Conduct_Balance_Auto_Update?>";	
    if(confirm(alertMsg)){
		self.location.href = 'base_mark_edit.php';
    } else 
    	return false;
	
}
//-->
</script>
<form name="form1" method="post" action="">
<br />
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center"><?=$showmsg ?><br>
			<?/*= $ldiscipline->showWarningMsg($i_Discipline_System_Conduct_Instruction, $i_Discipline_System_Conduct_Instruction_Msg_Content,"class='tabletextrequire'") */ ?>
			<br>
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['FieldTitle']['CalculationMethod'];?></td>
								<td><?=$str_CalculationMethod;?></td>
							</tr>
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Conduct_Base_Mark?></td>
								<td><?=$baseMark ?> </td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center"><?= $linterface->GET_ACTION_BTN($button_edit, "button", "javascript:showAlertMsg();")?></td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
