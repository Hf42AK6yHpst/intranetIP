<?php
// using : 

############# Change Log (start) #################
#
#	Date:	2017-07-06 Anna
#			create file for update Allow Access IP -- copy from EJ
#
############## Change Log (end) ##################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

$allowed_IPs = trim($ldiscipline->getDisciplineGeneralSetting("AllowAccessIP"));
$ip_addresses = explode("\n", $allowed_IPs);
checkCurrentIP($ip_addresses, $ip20TopMenu['eDisciplinev12']);

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

// post data
$AllowAccessIP = trim($_POST['AllowAccessIP']);

// get current IP settings
$currentIP = $ldiscipline->getDisciplineGeneralSetting("AllowAccessIP");

// update
if(sizeof($currentIP)>0){
	$sql = "Update DISCIPLINE_GENERAL_SETTING 
				Set SettingValue='$AllowAccessIP', DateInput=NOW(), ModifiedBy='".$_SESSION['UserID']."'
			WHERE SettingName='AllowAccessIP'";
}
// insert
else{
	$sql = "INSERT INTO DISCIPLINE_GENERAL_SETTING (SettingName, SettingValue, DateInput, DateModified, ModifiedBy)
				VALUES ('AllowAccessIP', '$AllowAccessIP', NOW(), NOW(), '".$_SESSION['UserID']."')";
}
$result = $ldiscipline->db_db_query($sql);

if($result)
	$msg = "update";
else
	$msg = "update_failed";
	
intranet_closedb();	

header("Location: index.php?msg=$msg");
?>