<?php
// Modifying by: 

#############################################
#
#	Date	:	2017-07-11	(Bill)	[2016-1125-0939-00240]
#	Detail 	:	support import items with Study Score Limit ($sys_custom['eDiscipline']['ApplyMaxStudyScore'])
#
#	Date	:	2017-03-17	(Bill)	[2016-1207-1221-39240]
#	Detail 	:	support import item with Activity Score
#
#	Date	:	2016-03-09 (Kenneth)
#	Detail 	:	add update import
#
#	Date	:	2010-06-14 (Henry)
#	Detail 	:	import of Item
#
#############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check Access Right
$ldiscipline->CONTROL_ACCESS("Discipline-SETTINGS-Award_Punishment-Access");

$linterface = new interface_html();
$lsp = new libstudentprofile();

# Menu Highlight
$CurrentPage = "Settings_AwardPunishment";
$CurrentPageArr['eDisciplinev12'] = 1;

# Left Menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Tag Information
$TAGS_OBJ[] = array($eDiscipline['Award_and_Punishment']);

# Step Information
$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$PAGE_NAVIGATION[] = array($Lang['eDiscipline']['ImportItem']);

if(!isset($MeritType) || $MeritType=="") $MeritType = 1;

//if($sys_custom['Discipline_AP_Item_Tag'] && $ldiscipline->UseSubScore) {
//	$sample_file = "award_punishment_item_settings_withTag_withStudyScore_unicode.csv";
//	$sample_file_update = "award_punishment_item_settings_withTag_withStudyScore_update_unicode.csv";;
//}
//else if($sys_custom['Discipline_AP_Item_Tag'] && !$ldiscipline->UseSubScore) {
//	$sample_file = "award_punishment_item_settings_withTag_unicode.csv";
//	$sample_file_update = "award_punishment_item_settings_withTag_update_unicode.csv";
//}
//else if(!$sys_custom['Discipline_AP_Item_Tag'] && $ldiscipline->UseSubScore) {
//	$sample_file = "award_punishment_item_settings_withStudyScore_unicode.csv";
//	$sample_file_update = "award_punishment_item_settings_withStudyScore_update_unicode.csv";
//}
//else {
//	$sample_file = "award_punishment_item_settings_unicode.csv";
//	$sample_file_update = "award_punishment_item_settings_update_unicode.csv";
//}
$sample_file = "award_punishment_item_settings";
$sample_file_update = "award_punishment_item_settings";
if($sys_custom['Discipline_AP_Item_Tag']) {
	$sample_file .= "_withTag";
	$sample_file_update .= "_withTag";
}
if($ldiscipline->UseSubScore) {
	$sample_file .= "_withStudyScore";
	$sample_file_update .= "_withStudyScore";
}
// [2016-1125-0939-00240]
if($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']) {
	$sample_file .= "_withMaxPoint";
	$sample_file_update .= "_withMaxPoint";
}
if($ldiscipline->UseActScore) {
	$sample_file .= "_withActivityScore";
	$sample_file_update .= "_withActivityScore";
}
$sample_file .= "_unicode.csv";
$sample_file_update .= "_update_unicode.csv";

$csvFile = "<a class=\"tablelink\" href=\"/templates/get_sample_csv.php?file=$sample_file\" target=\"_self\">".$Lang['General']['ClickHereToDownloadSample']."</a>";
$csvFileUpdate = "<a class=\"tablelink\" href=\"/templates/get_sample_csv.php?file=$sample_file_update\" target=\"_self\">".$Lang['General']['ClickHereToDownloadSample']."</a>";

$csv_format = "";
$csv_format_update= $Lang['SysMgr']['Homework']['Column']." 1 : " ."<span class='tabletextrequire'>*</span>".$Lang['eDiscipline']['Import']['ExistingItemCode'] .'<br>';
$delim = "<br>";

$a = 1;
for($i=0; $i<sizeof($Lang['eDiscipline']['Import_AP_Item_Col']); $i++) {
	if($sys_custom['Discipline_AP_Item_Tag'] || (!$sys_custom['Discipline_AP_Item_Tag'] && $i!=(sizeof($Lang['eDiscipline']['Import_AP_Item_Col'])-1))) {
		//if($ldiscipline->UseSubScore || (!$ldiscipline->UseSubScore && $i!=4)) {
		if($ldiscipline->UseActScore || ($ldiscipline->UseSubScore && !$ldiscipline->UseActScore && $i!=5) || (!$ldiscipline->UseSubScore && !$ldiscipline->UseActScore && $i!=4 && $i!=5)) {
			$csv_format .= $Lang['SysMgr']['Homework']['Column']." ".$a." : ".$Lang['eDiscipline']['Import_AP_Item_Col'][$i];
			$csv_format .= $delim;
			$csv_format_update .= $Lang['SysMgr']['Homework']['Column']." ".($a+1)." : ".$Lang['eDiscipline']['Import_AP_Item_Col'][$i];
			$csv_format_update .= $delim;
			$a++;
			
			// [2016-1125-0939-00240] Add Study Score Limit Column
			if($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore'] && $i==4) {
				$csv_format .= $Lang['SysMgr']['Homework']['Column']." ".$a." : ".$Lang['eDiscipline']['MeritItemMaxStudyScore']." ".$Lang['eDiscipline']['MeritItemMaxStudyScoreRemarks'];
				$csv_format .= $delim;
				$csv_format_update .= $Lang['SysMgr']['Homework']['Column']." ".($a+1)." : ".$Lang['eDiscipline']['MeritItemMaxStudyScore']." ".$Lang['eDiscipline']['MeritItemMaxStudyScoreRemarks'];
				$csv_format_update .= $delim;
				$a++;
			}
		}
	}
}

$reference_str = "<div id=\"ref_list\" style='position:absolute; height:150px; z-index:1; visibility: hidden;'></div>";
$reference_str .= "
				<a class=\"tablelink\" href=javascript:show_ref_list('category','cc_click')>".$eDiscipline['Setting_Category']."</a>,<span id=\"cc_click\">&nbsp;</span>
				<a class=\"tablelink\" href=javascript:show_ref_list('meritType','mc_click')>".$eDiscipline['Type']."</a>,<span id=\"mc_click\">&nbsp;</span>
				<a class=\"tablelink\" href=javascript:show_ref_list('status','sc_click')>".$eDiscipline['Setting_Status']."</a>";
$reference_str .= ($sys_custom['Discipline_AP_Item_Tag']) ? ",<span id=\"sc_click\">&nbsp;</span>
																<a class=\"tablelink\" href=javascript:show_ref_list('tag','tc_click')>".$Lang['eDiscipline']['Tag']."</a><span id=\"tc_click\">&nbsp;</span>" : "<span id=\"sc_click\">&nbsp;</span>";
$reference_str .= "</p>";	

$linterface->LAYOUT_START();

$url = ($MeritType==1) ? "index.php" : "punish_category_item.php";

## Import Mode Radio buttons
$radioButtons = $linterface->Get_Radio_Button('insertNew', 'importMode', 'new', $isChecked=1, $Class="", $Lang['eDiscipline']['Import']['ImportNew'], $Onclick="onClickImportMode(this);",$isDisabled=0);
$radioButtons .= $linterface->Get_Radio_Button('updateExist', 'importMode', 'update', $isChecked=0, $Class="", $Lang['eDiscipline']['Import']['UpdateExisting'], $Onclick="onClickImportMode(this);",$isDisabled=0);
?>

<script language="JavaScript">
var ClickID = '';
var callback_show_ref_list = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('ref_list').innerHTML = tmp_str;
	    DisplayPosition();
	}
}

function show_ref_list(type,click)
{
	ClickID = click;
	document.getElementById('task').value = type;
	
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	
	var path = "ajax_ap.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_show_ref_list);
}

function Hide_Window(pos){
  document.getElementById(pos).style.visibility='hidden';
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function DisplayPosition(){
  document.getElementById('ref_list').style.left=getPosition(document.getElementById(ClickID),'offsetLeft') +10;
  document.getElementById('ref_list').style.top=getPosition(document.getElementById(ClickID),'offsetTop');
  document.getElementById('ref_list').style.visibility='visible';
}

function onClickImportMode(obj){
	var importMode = obj.value;
	switch (importMode){
		case "new":
			//todo when new is click
			$('.update').hide();
			$('.new').show();
		break;
		case "update":
			//todo when update is click
			$('.update').show();
			$('.new').hide();
		break;
	}
}
</script>
<br />

<form name="form1" method="post" action="importItem_result.php" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
</table>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
	</tr>
	<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td class="formfieldtitle" align="left" width="30%"><?=$Lang['General']['SourceFile']." <span class='tabletextremark'>".$Lang['General']['CSVFileFormat']."</span>"?>
					</td>
					<td class="tabletext" width="70%">
						<input class="file" type="file" name="csvfile">
					</td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="left"><?= $Lang['eDiscipline']['Import']['Mode'] ?></td>
					<td class="tabletext"><?= $radioButtons ?></td>
				</tr>
				<tr class="new">
					<td class="formfieldtitle" align="left" ><?= $Lang['General']['CSVSample'].' ('.$Lang['eDiscipline']['Import']['ImportNew'].')' ?></td>
					<td class="tabletext"><?=$csvFile?></td>
				</tr>
				<tr style="display:none" class="update">
					<td class="formfieldtitle" align="left" ><?=$Lang['General']['CSVSample'].' ('.$Lang['eDiscipline']['Import']['UpdateExisting'].')' ?></td>
					<td class="tabletext"><?=$csvFileUpdate?></td>
				</tr>
				<tr class="new">
					<td class="formfieldtitle" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?></td>
					<td class="tabletext"><?=$csv_format?></td>
				</tr>
				<tr class="update" style="display:none">
					<td class="formfieldtitle" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?></td>
					<td class="tabletext"><?=$csv_format_update?></td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="left" height="30"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['Reference']?></td>
					<td class="tabletext"><?=$reference_str?></td>
				</tr>
				<tr>
					<td class="tabletextremark" colspan="2"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['RequiredField']?></td>
				</tr>
				
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_continue, "submit") ?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='$url'") ?>
		</td>
	</tr>
</table>

<input type="hidden" id="task" name="task"/>
<input type="hidden" id="MeritType" name="MeritType" value="<?=$MeritType?>"/>
</form>
<br />

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>