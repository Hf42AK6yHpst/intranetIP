<?php
# modifying by : henry chow
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_right_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
	$ck_right_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_right_page_number!="")
{
	$pageNo = $ck_right_page_number;
}

if ($ck_right_page_order!=$order && $order!="")
{
	setcookie("ck_right_page_order", $order, 0, "", "", 0);
	$ck_right_page_order = $order;
} else if (!isset($order) && $ck_right_page_order!="")
{
	$order = $ck_right_page_order;
}

if ($ck_right_page_field!=$field && $field!="")
{
	setcookie("ck_right_page_field", $field, 0, "", "", 0);
	$ck_right_page_field = $field;
} else if (!isset($field) && $ck_right_page_field!="")
{
	$field = $ck_right_page_field;
}


$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lstudentprofile = new libstudentprofile();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Award_Punishment-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

$ldiscipline->checkMeritTypeExist();

$i_MeritType[1][] = $i_Merit_Merit;
if (!$lstudentprofile->is_merit_disabled) {	
	$i_MeritType[1][] = 1;
	$i_MeritType[1][] = 1;
}
$i_MeritType[2][] = $i_Merit_MinorCredit;
if (!$lstudentprofile->is_min_merit_disabled) {	
	$i_MeritType[2][] = 1;
	$i_MeritType[2][] = 2;
}
$i_MeritType[3][] = $i_Merit_MajorCredit;
if (!$lstudentprofile->is_maj_merit_disabled) {	
	$i_MeritType[3][] = 1;
	$i_MeritType[3][] = 3;
}
$i_MeritType[4][] = $i_Merit_SuperCredit;
if (!$lstudentprofile->is_sup_merit_disabled) {	
	$i_MeritType[4][] = 1;
	$i_MeritType[4][] = 4;
}
$i_MeritType[5][] = $i_Merit_UltraCredit;
if (!$lstudentprofile->is_ult_merit_disabled) {	
	$i_MeritType[5][] = 1;
	$i_MeritType[5][] = 5;
}


$sql  = "SELECT
			s.MeritType,
			s.ApproveNum,
			IFNULL(ap.Name,'-') as ApprovalGroup,
			CONCAT('<input type=\'checkbox\' name=\'merit[]\' value=\'', s.MeritType ,'\'>')
				FROM 
					DISCIPLINE_MERIT_TYPE_SETTING s LEFT OUTER JOIN DISCIPLINE_AP_APPROVAL_GROUP ap ON (s.ApprovalGroupID=ap.GroupID)
				WHERE
					s.MeritType>0
                ";


$li = new libdbtable2007($field, 1, $pageNo);
$li->field_array = array("MeritType", "ApproveNum", "ApprovalGroup");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(13,0,0);
$li->wrap_array = array(0,0,0);
$li->IsColOff = 2;

$pos = 0;
$li->column_list .= "<td width='3%' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='40%' >".$i_Merit_Award."</td>\n";
$li->column_list .= "<td width='20%'>".$i_Discipline_System_NeedApproval." *</td>\n";
$li->column_list .= "<td width='40%'>".$Lang['eDiscipline']['ApprovalGroup']."</td>\n";
$li->column_list .= "<td width='1'>&nbsp;</td>\n";

# menu highlight setting
$CurrentPage = "Settings_AwardPunishment";

$TAGS_OBJ[] = array($i_Merit_Award, "index.php", 1);
$TAGS_OBJ[] = array($i_Merit_Punishment, "punish_category_item.php", 0);
$TAGS_OBJ[] = array($Lang['eDiscipline']['ApprovalGroup'], "approvalGroup.php", 0);
if($sys_custom['Disciplinev12_show_AP_tag']) {
	$TAGS_OBJ[] = array($Lang['eDiscipline']['Tag'], "tag.php", 0);
}

$subTag[] = array($i_Discipline_System_Award_Punishment_Submenu_Category_Item, "index.php", 0);
$subTag[] = array($i_Discipline_System_Award_Punishment_Submenu_Approval, "award_approval.php", 1);
$subTag[] = array($i_Discipline_System_Award_Punishment_Submenu_Promotion, "award_promotion.php", 0);

$CurrentPageArr['eDisciplinev12'] = 1;

$instructionBox = showWarningMsg($Lang['General']['Instruction'], $Lang['eDiscipline']['ApprovalGroupApplyInstruction']['Award']);

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>
<form name="form1" method="post" action="">
<br>
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr> 
		<td align="left">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="tab_underline">
						<div class="shadetabs">
							<ul>
								<?=$ldiscipline->getSubTag($subTag)?>
							</ul>
						</div>
					</td>
				</tr>
				<tr>
					<td align="right"><?= $linterface->GET_SYS_MSG($xmsg) ?>
					</td>
				</tr>
			</table>
			<br style="clear:both">
			<?=$instructionBox?>
			
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr class="table-action-bar">
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td height="28" align="right" valign="bottom" >
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td>&nbsp;</td>
											<td align="right" valign="bottom">
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
														<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
															<table border="0" cellspacing="0" cellpadding="2">
																<tr>
																	<td nowrap><a href="javascript:checkEdit(document.form1,'merit[]','award_approval_edit.php')" class="tabletool" title="<?=$button_edit?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_edit ?></a></td>
																</tr>
															</table>
														</td>
														<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<?=$li->displayFormat_meritType("Merit",$i_MeritType); ?>
					</td>
				</tr>
			</table>
			<span class="tabletextremark">* <?=$i_Discipline_System_Description_Approval?> </span><br>
		</td>
	</tr>
</table>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
