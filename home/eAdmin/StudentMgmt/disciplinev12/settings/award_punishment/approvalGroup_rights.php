<?php
// editing by : henry chow

############# Change Log [Start]
#
#	Date	:	2012-07-03 (Henry Chow)
#	Detail	:	revise the UI of approval rights
#
############# Change Log [End]

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!isset($GroupID) || $GroupID=="") {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();	
}

$linterface = new interface_html();
$ldiscipline_ui = new libdisciplinev12_ui();


$CurrentPageArr['eDisciplinev12'] = 1;
$CurrentPage = "Settings_AwardPunishment";

$TAGS_OBJ[] = array($i_Merit_Award, "index.php", 0);
$TAGS_OBJ[] = array($i_Merit_Punishment, "punish_category_item.php", 0);
$TAGS_OBJ[] = array($Lang['eDiscipline']['ApprovalGroup'], "approvalGroup.php", 1);
if($sys_custom['Disciplinev12_show_AP_tag']) {
	$TAGS_OBJ[] = array($Lang['eDiscipline']['Tag'], "tag.php", 0);
}

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

if($msg!="") $Msg = $Lang['General']['ReturnMessage'][$msg];

$linterface->LAYOUT_START($Msg);

echo $ldiscipline_ui->Get_ApprovalGroup_Right_Index($GroupID, $xmsg);

$linterface->LAYOUT_STOP();

intranet_closedb();
?>
<script type="text/JavaScript" language="JavaScript">
function goSubmit() {
	if(document.getElementById('GroupTitle').value=="") {
		setTimeout("submitAction();", 500);	
	} else {
		var meritCategoryLength = document.AccessRightForm.elements['meritCategory'].length;
		for(i=0; i<meritCategoryLength; i++) {
			document.AccessRightForm.elements['meritCategory'][i].selected = true;	
		}
		var demeritCategoryLength = document.AccessRightForm.elements['demeritCategory'].length;
		for(i=0; i<demeritCategoryLength; i++) {
			document.AccessRightForm.elements['demeritCategory'][i].selected = true;	
		}
		//checkOptionAll($('#meritCategory[]'));
		//checkOptionAll(document.getElementById('demeritCategory'));
		document.form1.submit();		
	}
	
}

function submitAction() {
	
	//checkOptionAll(document.getElementById('meritCategory[]'));
	checkOptionAll(document.getElementById('meritCategory[]'));
	checkOptionAll(document.getElementById('demeritCategory'));

	document.form1.submit();	
}

// dom function 
{
function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		Get_Group_User_List();
	else
		return false;
}
}

// ajax function
{
function Get_Group_User_List()
{
	var PostVar = {
			"GroupID": $('Input#GroupID').val(),
			"Keyword": encodeURIComponent($('Input#Keyword').val())
			}

	Block_Element("UserListLayer");
	$.post('ajax_get_group_user_list.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#UserListLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("UserListLayer");
						}
					});
}

function Get_Add_Member_Form()
{
	var PostVar = {
		"GroupID": $('Input#GroupID').val()
	};
	
	$.post('ajax_get_add_group_member_form.php',PostVar,
		function(data) {
			if (data == "die") 
				window.top.location = '/';
			else 
				$('div#TB_ajaxContent').html(data);
		});
}

function Add_Member()
{
	var PostVar = {
		"GroupID":$('input#GroupID').val(),
		"AddUserID[]":Get_Selection_Value('AddUserID[]','Array')
	}
	
	Block_Thickbox();
	$.post('ajax_add_member.php',PostVar,
		function (data) {
			if (data == "die") 
				window.top.location = '/';
			else {
				Get_Return_Message(data);
				Get_Group_User_List();
				window.top.tb_remove();
				Scroll_To_Top();
			}
		});
}

function Delete_Member()
{
	var StaffID = Get_Check_Box_Value('StaffID[]','Array');
	
	if (StaffID.length > 0) {
		if (confirm('<?=$Lang['StaffAttendance']['AccessRightDeleteGroupMemberWarning']?>')) {
			var PostVar = {
				"GroupID":$('input#GroupID').val(),
				"StaffID[]":StaffID
			}
			
			$.post('ajax_delete_member.php',PostVar,
				function(data) {
				
					if (data == "die") {
						window.top.location = '/';
					}
					else {
						Get_Return_Message(data);
						Get_Group_User_List();
						Scroll_To_Top();
					}
				});
		}
	}
	else {
		alert('<?=$Lang['StaffAttendance']['SelectAtLeastOneWarning']?>');
	}
}

}

function Check_Group_Title(GroupTitle,GroupID) {

	var GroupID = GroupID || "";
	var PostVar = {
			"GroupID": GroupID,
			"GroupTitle": encodeURIComponent(GroupTitle)
			}
	var ElementObj = $('div#GroupTitleWarningLayer');
	
	if (Trim(GroupTitle) != "") {
		$.post('ajax_check_group_title.php',PostVar,
				function(data){

					if (data == "die") 
						window.top.location = '/';
					else if (data == "1") {
						ElementObj.html('');
						ElementObj.hide();
						if (document.getElementById('GroupTitleWarningRow')) 
							document.getElementById('GroupTitleWarningRow').style.display = 'none';
					}
					else {
						ElementObj.html('<?=$Lang['StaffAttendance']['GroupTitleDuplicateWarning']?>');
						ElementObj.show('fast');
						if (document.getElementById('GroupTitleWarningRow')) 
							document.getElementById('GroupTitleWarningRow').style.display = '';
					}
				});
	}
	else if(Trim(GroupTitle) == ""){
		ElementObj.html('<?=$Lang['SysMgr']['RoleManagement']['RoleNameDuplicateWarning']?>');
		ElementObj.show('fast');
		
		if (document.getElementById('GroupTitleWarningRow')) 
			document.getElementById('GroupTitleWarningRow').style.display = '';
	}
}

function Show_Edit_Icon(IconID)
{
	$('#'+IconID).attr('class', '');
	//LayerObj.style.backgroundImage = "url(<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/icon_edit_b.gif)";
	//LayerObj.style.backgroundPosition = "center right";
	//LayerObj.style.backgroundRepeat = "no-repeat";
}

function Hide_Edit_Icon(IconID)
{
	$('#'+IconID).attr('class', 'edit_dim');
	//LayerObj.style.backgroundImage = "";
	//LayerObj.style.backgroundPosition = "";
	//LayerObj.style.backgroundRepeat = "";
}

// jEditable function 
function Init_JEdit_Input(objDom)
{
	var WarningLayer = "div#GroupTitleWarningLayer";
	$(objDom).editable
	( 
      function(value, settings)
      {
    	var ElementObj = $(this);
    	
    	if (Trim($(WarningLayer).html()) == "" && ElementObj[0].revert != value)
    	{
    		if($('Input#GroupID').val() == '')
	    	{
	    		ElementObj.html(value);
	    		$('span#GroupTitleNavLayer').html(value);
				$('Input#GroupTitle').val(value);
	    	}
	    	else
	    	{
	    		var PostVar = {
	    			"GroupID":$('Input#GroupID').val(),
	    			"GroupTitle":encodeURIComponent(value)
	    		};
		    				     
		    	$.post('ajax_rename_group.php',PostVar,
		    		function (data) {
		    			Get_Return_Message(Trim(data));
		    			ElementObj.html(value);
						$('span#GroupTitleNavLayer').html(value);
						$('Input#GroupTitle').val(value);
		    		});
	    	}
		}
		else {
			ElementObj[0].reset();
		}
	  }, 
	  {
	    tooltip   : "<?=$Lang['SysMgr']['FormClassMapping']['ClickToEdit']?>",
	    event : "click",
	    onblur : "submit",
	    type : "text",     
	    style  : "display: inline",
	    height: "20px",
	    maxlength: 100,
	    onreset: function() {
	    	$(WarningLayer).html('');
	    	$(WarningLayer).hide();
	  	}
  	  }
  	);
  
  $(objDom).keyup
  (
  	 function()
  	 {
		var GroupTitle = Trim($('form input').val());
		var GroupID = $('Input#GroupID').val();
		Check_Group_Title(GroupTitle,GroupID);
		
	 }
  );
  
}

function Init_JEdit_Input2(objDom)
{
	$(objDom).editable
	( 
      function(value, settings)
      {
    	var ElementObj = $(this);
    	ElementObj.html(value);
		$('Input#GroupDescription').val(value);
		
		if($('Input#GroupID').val() != '' && ElementObj[0].revert != value)
		{
			var PostVar = {
	    			"GroupID":$('Input#GroupID').val(),
	    			"GroupDescription":encodeURIComponent(value)
	    		};
		    				     
		    	$.post('ajax_set_group_description.php',PostVar,
		    		function (data) {
		    			Get_Return_Message(Trim(data));
		    		});
		}
	  }, 
	  {
	    tooltip   : "<?=$Lang['SysMgr']['FormClassMapping']['ClickToEdit']?>",
	    event : "click",
	    onblur : "submit",
	    type : "text",     
	    style  : "display: inline",
	    height: "20px",
	    maxlength: 100,
	    onreset: function() {
	    	
	  	}
  	  }
  	);
}

function jsClickEdit()
{
	$('span#EditableGroupTitle').click();
}

function jsClickEdit2()
{
	$('span#EditableGroupDescription').click();
}

// thick box function 
{
//on page load call tb_init
function Thick_Box_Init()
{   
	tb_init('a.thickbox, area.thickbox, input.thickbox');//pass where to apply thickbox
}
}

$(document).ready(
	function()
	{		
		Init_JEdit_Input('span.jEditInput');
		Init_JEdit_Input2('span.jEditInput2');
	}
);

function checkForm() {
	if(document.getElementById('GroupTitle').value=="") {
		alert("<?=$i_alert_pleasefillin." ".$Lang['AccountMgmt']['Settings']['GroupName']?>");
		document.getElementById('GroupTitle').select();
		return false;
	}
	return true;
	
}

function SelectAllAction(emtId, flag) {
	var emtLen = document.getElementById(emtId).length;
	
	var thisAction = (flag) ? true : false;
	
	for(var i=0; i<emtLen; i++) {
		document.getElementById(emtId).options[i].selected = thisAction;
	}
}

</script>