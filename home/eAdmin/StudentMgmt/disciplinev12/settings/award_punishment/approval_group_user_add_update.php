<?php
// editing by : henry chow

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

$result = $ldiscipline->Approval_Group_Add_Member($GroupID, $student);

intranet_closedb();

$msg = ($result) ? "add" : "add_failed";

header("Location: approvalGroup_detail.php?GroupID=$GroupID&xmsg=$msg&GroupID=".$GroupID);
?>
