<?php
//editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

$GroupDescription= trim(urldecode(stripslashes($GroupDescription)));

if ($ldiscipline->Set_Group_Description($GroupDescription,$GroupID)) {
	echo $Lang['StaffAttendance']['ChangeGroupDescriptionSuccess'];
}
else {
	echo $Lang['StaffAttendance']['ChangeGroupDescriptionFail'];
}

intranet_closedb();
?>