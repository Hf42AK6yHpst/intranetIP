<?php
# using: 

######################################################
#
#	Date:	2017-07-11	(Bill)	[2016-1125-0939-00240]
#			support Study Score Limit ($sys_custom['eDiscipline']['ApplyMaxStudyScore'])
#
#	Date:	2017-03-16	Bill	[2016-1207-1221-39240]
#			support preset Activity Score ($sys_custom['ActScore'])
#			support single item tag ($sys_custom['Discipline_AP_Item_Tag_SingleOnly'])
#
#	Date:	2013-04-29	YatWoon
#			check with $sys_custom['eDiscipline']['ConductMark1DecimalPlace'] [Case#2013-0429-0942-25073]
#
#	Date:	2012-09-05	YatWoon
#			allow update category with flag $sys_custom['eDiscipline']['AllowEditAPItemCategory'] [Case#2012-0905-1216-26077]
#
######################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lstudentprofile = new libstudentprofile();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Award_Punishment-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# Menu Highlight
$CurrentPage = "Settings_AwardPunishment";
$CurrentPageArr['eDisciplinev12'] = 1;

$TAGS_OBJ[] = array($i_Merit_Award, "index.php", 1);
$TAGS_OBJ[] = array($i_Merit_Punishment, "punish_category_item.php", 0);
$TAGS_OBJ[] = array($Lang['eDiscipline']['ApprovalGroup'], "approvalGroup.php", 0);
if($sys_custom['Disciplinev12_show_AP_tag']) {
	$TAGS_OBJ[] = array($Lang['eDiscipline']['Tag'], "tag.php", 0);
}

$subTag[] = array($i_Discipline_System_Award_Punishment_Submenu_Category_Item, "index.php", 1);
$subTag[] = array($i_Discipline_System_Award_Punishment_Submenu_Approval, "award_approval.php", 0);
$subTag[] = array($i_Discipline_System_Award_Punishment_Submenu_Promotion, "award_promotion.php", 0);

if(is_array($ItemID)) {
	$ItemID = $ItemID[0];
}
else {
	$ItemID = $ItemID;	
}

list($itemCode, $itemName, $conductScore, $numMerit, $meritType, $StudyScore, $compulsoryInReport, $ActivityScore, $MaxStudyScore) = $ldiscipline->getAwardPunishItemInfo($ItemID);
$itemName = str_replace("<","&lt;",$itemName);

$catName = $ldiscipline->getAwardPunishCategoryName($CatID);
$catNameDisplay = str_replace("<","&lt;",$catName);
	
if($sys_custom['eDiscipline']['AllowEditAPItemCategory'])
{
	$catName = $ldiscipline->ReturnCategorySelection(1, $CatID, "NewCatID",1);
}
else
{
	$catName = $catNameDisplay;
}

if($compulsoryInReport==1)
	$compulsoryChecked = "checked";

$itemInfo = $ldiscipline->getAPItemInfoBYItemID($ItemID);
$Status = $itemInfo['RecordStatus'];

$tagArray = $ldiscipline->getTagSetting();
//$selectTagMenu = getSelectByArray($tagArray, "name='TagID' id='TagID'", $TagID, 0, 0, "--- $button_select ---", 2);
$itemTagID = $ldiscipline->getItemTagIDInfo($ItemID);

$selectTagMenu = "";
for($i=0; $i<sizeof($tagArray);$i++) {
	$checked = (in_array($tagArray[$i][0], $itemTagID)) ? " checked" : "";
	$inputTagType = $sys_custom['Discipline_AP_Item_Tag_SingleOnly']? "radio" : "checkbox";
	$selectTagMenu .= "<input type='".$inputTagType."' name='TagID[]' id='TagID_".$tagArray[$i][0]."' value='".$tagArray[$i][0]."' $checked><label for='TagID_".$tagArray[$i][0]."'>".$tagArray[$i][1]."</label>";
}

$PAGE_NAVIGATION[] = array($i_Discipline_System_CategoryList, "index.php");
$PAGE_NAVIGATION[] = array($catNameDisplay, "award_category_item_item_list.php?CatID=$CatID");
$PAGE_NAVIGATION[] = array($itemName, "");

$conductMarkInterval = $sys_custom['eDiscipline']['ConductMark1DecimalPlace'] ? 0.1 : 1;
$selectScore .= "<SELECT name='ConductScore'>";
for($i=0; $i<=$ldiscipline->ConductMarkIncrement_MAX; $i=$i+$conductMarkInterval){
	$selectScore .= "<OPTION value='$i'";
	$selectScore .= (round($i,2)==$conductScore) ? " selected" : "";
	$selectScore .= ">". round($i,2) ."</OPTION>";
}
$selectScore .= "</SELECT>";

$selectStudyScore .= "<SELECT name='StudyScore'>";
for($i=0; $i<=$ldiscipline->SubScoreIncrement_MAX; $i++) {
	$selectStudyScore .= "<OPTION value='$i'";
	$selectStudyScore .= ($i==$StudyScore) ? " selected" : "";
	$selectStudyScore .= ">".$i."</OPTION>";
}
$selectStudyScore .= "</SELECT>";

// [2016-1125-0939-00240]
$selectMaxStudyScore .= "<SELECT name='MaxStudyScore'>";
$selectMaxStudyScore .= "<OPTION value='0'";
$selectMaxStudyScore .= ($MaxStudyScore==0) ? " selected" : "";
$selectMaxStudyScore .= ">".$Lang['General']['NotApplicable']."</OPTION>";
for($i=1; $i<=$ldiscipline->SubScoreIncrement_MAX; $i++) {
	$selectMaxStudyScore .= "<OPTION value='$i'";
	$selectMaxStudyScore .= ($i==$MaxStudyScore) ? " selected" : "";
	$selectMaxStudyScore .= ">".$i."</OPTION>";
}
$selectMaxStudyScore .= "</SELECT>";

$selectActivityScore .= "<SELECT name='ActivityScore'>";
for($i=0; $i<=$ldiscipline->ActScoreIncrement_MAX; $i++) {
	$selectActivityScore .= "<OPTION value='$i'";
	$selectActivityScore .= ($i==$ActivityScore) ? " selected" : "";
	$selectActivityScore .= ">".$i."</OPTION>";
}
$selectActivityScore .= "</SELECT>";

$selectMeritNum .= "<SELECT name='meritNum'>";
$AP_Interval = $ldiscipline->AP_Interval_Value ? $ldiscipline->AP_Interval_Value : 1;
for ($i=0; $i<=$ldiscipline->AwardPunish_MAX; $i=$i+$AP_Interval)
{
	$selectMeritNum .= "<OPTION value='".$i."'";
	$selectMeritNum .= ($i==$numMerit) ? " selected" : "";
	$selectMeritNum .= ">".$i."</OPTION>";
}
$selectMeritNum .= "</select>";

$selectMenu .= "<SELECT name='MeritType'>";
$selectMenu .= "<option value='-999'";
$selectMenu .= ($meritType=='-999') ? " selected" : "";
$selectMenu .= ">$i_Merit_NoAwardPunishment</option>";
$selectMenu .= ($lstudentprofile->is_merit_disabled) ? "" : "<option value='1'";
$selectMenu .= ($meritType==1) ? " selected" : "";
$selectMenu .= ">$i_Merit_Merit</option>";
$selectMenu .= ($lstudentprofile->is_min_merit_disabled) ? "" : "<option value='2'";
$selectMenu .= ($meritType==2) ? " selected" : "";
$selectMenu .= ">$i_Merit_MinorCredit</option>";
$selectMenu .= ($lstudentprofile->is_maj_merit_disabled) ? "" : "<option value='3'";
$selectMenu .= ($meritType==3) ? " selected" : "";
$selectMenu .= ">$i_Merit_MajorCredit</option>";
$selectMenu .= ($lstudentprofile->is_sup_merit_disabled) ? "" : "<option value='4'";
$selectMenu .= ($meritType==4) ? " selected" : "";
$selectMenu .= ">$i_Merit_SuperCredit</option>";
$selectMenu .= ($lstudentprofile->is_ult_merit_disabled) ? "" : "<option value='5'";
$selectMenu .= ($meritType==5) ? " selected" : "";
$selectMenu .= ">$i_Merit_UltraCredit</option>";
$selectMenu .= "</SELECT>";

if($xmsg2==11)
	$message = $Lang['eDiscipline']['ChangeAPItemStatusWarning'];

# Left Menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
function checkForm(obj) {
<!--
if(obj.ItemCode.value=="" || obj.ItemCode.value==" ") {
	alert("<?=$i_alert_pleasefillin ?><?=$i_Discipline_System_CategoryName?>");
	obj.ItemCode.focus();
	return false;
}
if(obj.ItemName.value=="" || obj.ItemName.value==" ") {
	alert("<?=$i_alert_pleasefillin ?><?=$i_Discipline_System_ItemName?>");
	obj.ItemName.focus();
	return false;
}

if(obj.meritNum.value==0)	obj.MeritType.selectedIndex = 0;
if(obj.MeritType.value==-999)	obj.meritNum.selectedIndex = 0;

<? if($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']) { ?>
	if(obj.StudyScore && obj.MaxStudyScore && parseInt(obj.MaxStudyScore.value) > 0 && parseInt(obj.StudyScore.value) > parseInt(obj.MaxStudyScore.value)) {
		alert('<?=$Lang['eDiscipline']['ExceedStudyScoreMaxGain']?>');
		obj.StudyScore.focus();
		return false;
	}
<? } ?>
//-->
}
</script>

<form name="form1" method="post" action="award_category_item_item_edit_update.php" onSubmit="return checkForm(form1)">
<br />
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr> 
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="tab_underline">
						<div class="shadetabs">
							<ul>
								<?=$ldiscipline->getSubTag($subTag)?>
							</ul>
						</div>
					</td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="80%" class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
					<td width="20%" align="right"><?= $linterface->GET_SYS_MSG($xmsg,$message) ?></td>
				</tr>
			</table>
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_CategoryName?></td>
								<td><?= $catName?></td>
							</tr>
							<tr valign="top">
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_ItemCode?> <span class="tabletextrequire">*</span></td>
								<td>
									<label for="grading_passfail" class="tabletext">
									<INPUT maxLength="80" value="<?= str_replace("\"","&quot;",$itemCode)?>" name="ItemCode" class="tabletext">
									</label>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_ItemName?> <span class="tabletextrequire">* </span></td>
								<td valign="top"><INPUT name="ItemName" class="tabletext" value="<?= str_replace("\"","&quot;",$itemName) ?>" size="100%" maxLength="255"></td>
							</tr>
							<? if($sys_custom['Discipline_AP_Item_Tag']) {?>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['TagName']?></td>
								<td valign="top"><?=$selectTagMenu?></td>
							</tr>
							<? } ?>
							
							<? if(!$ldiscipline->Hidden_ConductMark) {?>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_ConductScore?> (<?=$i_Discipline_System_general_increment?>)</td>
								<td><?=$selectScore?>
								</td>
							</tr>
							<? } ?>
							<? if($ldiscipline->UseSubScore) { ?>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Subscore1?> (<?=$i_Discipline_System_general_increment?>)</td>
								<td><?=$selectStudyScore?>
								</td>
							</tr>
							<? } ?>
							<? if($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']) { ?>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Subscore1?> (<?=$Lang['eDiscipline']['MaxGainMark']?>)</td>
								<td><?=$selectMaxStudyScore?>
								</td>
							</tr>
							<? } ?>
							<? if($ldiscipline->UseActScore) { ?>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['ActivityScore']?> (<?=$i_Discipline_System_general_increment?>)</td>
								<td><?=$selectActivityScore?>
								</td>
							</tr>
							<? } ?>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Merit_Award?></td>
								<td><?=$selectMeritNum?>
									<?=$selectMenu?>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eDiscipline['Setting_Status']?></td>
								<td>
									<input type="radio" name="Status" value="2" id='Status2' <? if($Status==2) echo " checked"; ?>><label for="Status2"><?=$eDiscipline['Setting_Status_Drafted'];?></label>
									<input type="radio" name="Status" value="1" id='Status1' <? if($Status==1 || $Status=="" || $Status==0) echo " checked"; ?>><label for="Status1"><?=$eDiscipline['Setting_Status_Published'];?></label>								
								</td>
							</tr>
							<? if($sys_custom['skss']['StudentAwardReport']) {?>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['SKSS']['CompulsoryInReport']?></td>
								<td><input name="compulsoryInReport" id="compulsoryInReport" type="checkbox" value="1" <?=$compulsoryChecked?>><label for="compulsoryInReport"><?=$i_general_yes?></label></td>
							</tr>
							<? } ?>
						</table>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr>
								<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
								<td width="80%">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
										<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
										<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
										<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='award_category_item_item_list.php?CatID=$CatID'")?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<input type="hidden" name="merit" value="<?= $merit?>">
<input type="hidden" name="CatID" value="<?= $CatID?>">
<input type="hidden" name="ItemID" value="<?=$ItemID?>">
</form>

<?
echo $linterface->FOCUS_ON_LOAD("form1.ItemCode"); 

$linterface->LAYOUT_STOP();
intranet_closedb();
?>