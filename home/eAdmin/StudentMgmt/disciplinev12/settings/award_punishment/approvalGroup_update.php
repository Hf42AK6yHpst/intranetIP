<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();
/*
$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-SETTINGS-AccessRight-Access"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
*/
$ldiscipline = new libdisciplinev12();

$GroupTitle = trim(urldecode(stripslashes($GroupTitle)));
$GroupDescription = trim(urldecode(stripslashes($GroupDescription)));

if(trim($GroupTitle) == "")
{
	header("Location: approvalGroup.php");
	intranet_closedb();
	exit();
}

$ldiscipline->Start_Trans();
$GroupID = $ldiscipline->Insert_ApprovalGroup($GroupID, $GroupTitle, $GroupDescription);

if($GroupID!="") {
	$ldiscipline->Commit_Trans();
	$Msg = $Lang['StaffAttendance']['SettingApplySuccess'];
}
else {
	$ldiscipline->RollBack_Trans();
	$Msg = $Lang['StaffAttendance']['SettingApplyFail'];
}

header("Location: approvalGroup.php?Msg=".urlencode($Msg));
intranet_closedb();
?>