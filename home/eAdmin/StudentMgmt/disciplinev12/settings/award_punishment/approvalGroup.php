<?php
# modifying : henry chow

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui.php");

intranet_auth();
intranet_opendb();


$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$ldiscipline_ui = new libdisciplinev12_ui();

# menu highlight setting
$TAGS_OBJ[] = array($i_Merit_Award, "index.php", 0);
$TAGS_OBJ[] = array($i_Merit_Punishment, "punish_category_item.php", 0);
$TAGS_OBJ[] = array($Lang['eDiscipline']['ApprovalGroup'], "approvalGroup.php", 1);
if($sys_custom['Disciplinev12_show_AP_tag']) {
	$TAGS_OBJ[] = array($Lang['eDiscipline']['Tag'], "tag.php", 0);
}

$instructionBox = showWarningMsg($Lang['General']['Instruction'], $Lang['eDiscipline']['ApprovalGroupSettingInstruction']);

$CurrentPageArr['eDisciplinev12'] = 1;
$CurrentPage = "Settings_AwardPunishment";
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();
//exit;
# Start layout
$linterface->LAYOUT_START($Msg);

echo $ldiscipline_ui->Include_JS_CSS();
echo "<br>";
echo $instructionBox;
echo "<br>";
echo $ldiscipline_ui->Get_Settings_ApprovalGroup_UI("",1);
          
$btnAdd = str_replace("'", "\'", $linterface->GET_SMALL_BTN($Lang['Btn']['Add'], "button", "js_Insert();", "NewSubmitBtn"));
$btnCancel = str_replace("'", "\'", $linterface->GET_SMALL_BTN($Lang['Btn']['Cancel'], "button", "js_Clear_Temp_Info();", "NewCancelBtn"));



?>

<script language="javascript">
<!--

{
function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		Get_Approval_Group_List();
	else
		return false;
}
}

{
function Get_Approval_Group_List()
{
	var PostVar = {
			Keyword: encodeURIComponent($('Input#Keyword').val())
			}

	Block_Element("ApprovalGroupListLayer");
	$.post('ajax_reload.php',PostVar,
					function(data){
					//alert(data);
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#ApprovalGroupListLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("ApprovalGroupListLayer");
						}
					});
}

function Delete_ApprovalGroup(GroupID)
{
	
	if (confirm('<?=$Lang['StaffAttendance']['AccessRightDeleteGroupWarning']?>')) {
		var PostVar = {
			"GroupID":GroupID
		}
		
		$.post('ajax_delete_group.php',PostVar,
			function(data) {
			
				if (data == "die") 
					window.top.location = '/';
				else {
					Get_Return_Message(data);
					Get_Approval_Group_List();
					Scroll_To_Top();
				}
			});
	}
}
}

function Add_Form_Row() {
	if (!document.getElementById('GenAddRow')) {
		var TableBody = document.getElementById("FollowUpListTable").tBodies[0];
		var RowIndex = document.getElementById("AddRoleRow").rowIndex-1;
	  var NewRow = TableBody.insertRow(RowIndex);

	  NewRow.id = "GenAddRow";

		var NewCell0 = NewRow.insertCell(0);
		var temp = '<input name="FollowUpName" type="text" id="FollowUpName" value="" class="textbox" onkeyup="js_Check_FollowUpName();" style="width:98%;"/>';
		temp += '<div id="FollowUpNameWarningLayer" style="display: none; color:red;"></div>';
		NewCell0.innerHTML = temp;

		var NewCell1 = NewRow.insertCell(1);
		var temp = '<?=$btnAdd?> <?=$btnCancel?>';
		NewCell1.colSpan = '2';
		NewCell1.innerHTML = temp;
		
		$('input#FollowUpName').focus();
	}
}

function js_Clear_Temp_Info()
{
	// Delete the Row
	var TableBody = document.getElementById("FollowUpListTable").tBodies[0];
	var RowIndex = document.getElementById("GenAddRow").rowIndex-1;
	TableBody.deleteRow(RowIndex);
}

function js_Check_FollowUpName(event)
{
	if (Get_KeyNum(event)==13) // keynum==13 => Enter
		js_Insert();
}

function js_Insert() {
	var FollowUpName = Trim($('input#FollowUpName').val());
	
	if (js_Is_Input_Blank('FollowUpName', 'FollowUpNameWarningLayer', '<?=$Lang['eDiscipline']['FollowUpBlank']?>'))
	{
		$('input#FollowUpName').focus();
		return false;
	}
	
	$.post(
		"ajax_update_followup.php", 
		{
			ActionType: 'add_title',
			fname: FollowUpName
		},
		function(ReturnData)
		{
			if(ReturnData=="1")
			{
				$('#DIV_ListTable').load(
					'ajax_reload.php', 
					{}, 
					function (data){
						Get_Return_Message('<?=$Lang['General']['ReturnMessage']['AddSuccess']?>');
						js_Init_DND_Table();
						js_Init_JEdit();
					});
			}
			else
			{
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['AddUnsuccess']?>');
			}
		});
}

function js_Init_DND_Table() {
		
	var JQueryObj = $(".common_table_list_v30");
	JQueryObj.tableDnD({
		onDrop: function(table, DroppedRow) {
			
			// Get the order string
			if(table.id == "FollowUpListTable"){
				var rows = table.tBodies[0].rows;
				var RecordOrder = "";
				for (var i=0; i<rows.length; i++) {
					if (rows[i].id != "" && !isNaN(rows[i].id))
						RecordOrder += rows[i].id+",";
				}
				
				$.post(
					"ajax_update_followup.php", 
					{ 
						ActionType: 'order',
						DisplayOrderString: RecordOrder
					},
					function(ReturnData)
					{
						$('#DIV_ListTable').load(
							'ajax_reload.php', 
							{}, 
							function (data){
								js_Init_DND_Table();
								js_Init_JEdit();
								
							});
					}
				);
			}
		},
		onDragStart: function(table, DraggedRow) {
			
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
}

function js_Init_JEdit()
{
	$('div.jEditCode').editable( 
		function(jsInputValue, settings) 
		{			
			var jsDivID = $(this).attr("id");
			var idArr = jsDivID.split('_');
			var jsActionID = idArr[1];
			
			var ElementObj = $(this);
			jsInputValue = Trim(jsInputValue);
			
			if (jsInputValue=='' || $(this)[0].revert == jsInputValue)
			{
				// Empty or same Code => do nothing
				$('#CodeWarningDiv_' + jsActionID).hide();
				$(this)[0].reset();
			}
			else
			{
				// Check if the code is vaild
				$.post(
					"ajax_update_followup.php", 
					{
						ActionType: 'update_title',
						fname: jsInputValue,
						ExcludeActionID: jsActionID
					},
					function(ReturnData)
					{
						if (ReturnData == "1")
						{
							$('#DIV_ListTable').load(
							'ajax_reload.php', 
							{}, 
							function (data){
								Get_Return_Message('<?=$Lang['General']['ReturnMessage']['AddSuccess']?>');
								js_Init_DND_Table();
								js_Init_JEdit();
								
							});
						}
						else
						{
							// invalid code => show warning
							ElementObj[0].reset();
							$('div#CodeWarningDiv_' + jsActionID).html('<?=$Lang['eDiscipline']['FollowUpDuplicate']?>').show();
							
						}
					}
				);
			}
		}, 
		{
			tooltip   : "<?=$Lang['General']['ClickToEdit']?>",
			event : "click",
			onblur : "submit",
			type : "text",
			style  : "display:inline",
			height: "20px",
			onreset: function() {
						$(this).parent().next().html('');
						$(this).parent().next().hide();
					}
		}
	);
}

function js_Delete_Action(jsActionID)
{
	if (confirm('<?=$Lang['eDiscipline']['ConfirmDeleteFollowUp']?>'))
	{
		$.post(
			"ajax_update_followup.php", 
			{ 
				ActionType: "delete",
				ActionID: jsActionID
			},
			function(ReturnData)
			{
				if (ReturnData == '1')
				{
					$('#DIV_ListTable').load(
					'ajax_reload.php', 
					{}, 
					function (data){
						Get_Return_Message('<?=$Lang['General']['ReturnMessage']['DeleteSuccess']?>');
						js_Init_DND_Table();
						js_Init_JEdit();
					});
				}
				else
				{
					Get_Return_Message('<?=$Lang['General']['ReturnMessage']['DeleteUnsuccess']?>');
				}
			}
		);
	}
}

function click_toggle(jsActionID)
{
	$.post(
			"ajax_update_followup.php", 
			{ 
				ActionType: "toggle",
				ActionID: jsActionID
			},
			function(ReturnData)
			{
				if (ReturnData == '1')
				{
					$('#DIV_ListTable').load(
					'ajax_reload.php', 
					{}, 
					function (data){
						js_Init_DND_Table();
						js_Init_JEdit();
					});
				}
			}
		);
}

js_Init_DND_Table();
js_Init_JEdit();

{
//on page load call tb_init
function Thick_Box_Init()
{   
	tb_init('a.thickbox, area.thickbox, input.thickbox');//pass where to apply thickbox
}
}
//Get_Approval_Group_List()
//-->
</script>




<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
