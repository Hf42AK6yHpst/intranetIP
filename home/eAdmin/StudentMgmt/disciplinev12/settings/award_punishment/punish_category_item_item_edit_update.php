<?php
# using: 

######################################################
#
#	Date:	2017-07-11	(Bill)	[2016-1125-0939-00240]
#			support Study Score Limit update ($sys_custom['eDiscipline']['ApplyMaxStudyScore'])
#
#	Date:	2017-03-16	Bill	[2016-1207-1221-39240]
#			support preset Activity Score update ($sys_custom['ActScore'])
#
#	Date:	2012-09-05	YatWoon
#			allow update category with flag $sys_custom['eDiscipline']['AllowEditAPItemCategory'] [Case#2012-0905-1216-26077]
#
######################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Award_Punishment-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# check whether this itemID is used in GM Conversion
$itemInfo = $ldiscipline->getAPItemInfoBYItemID($_POST['ItemID']);

if($_POST['Status']==2 && $itemInfo['RecordStatus']!=2) {
	$itemIDAry[] = $_POST['ItemID'];
	$result = $ldiscipline->checkAPItemUsedInGMConversion($itemIDAry);
	
	if($result) {
		//$msg = $Lang['eDiscipline']['ChangeAPItemStatusWarning'];
		header("Location: punish_category_item_item_edit.php?xmsg2=11&CatID=$CatID&ItemID=$ItemID");
		exit;
	}
}
# end of checking

$ItemID = $_POST['ItemID'];
$ItemCode = $_POST['ItemCode'];
$ItemName = $_POST['ItemName'];
$MeritType = $_POST['MeritType'];
$ConductScore = $_POST['ConductScore'];
$StudyScore = $_POST['StudyScore'];
$ActivityScore = $_POST['ActivityScore'];
$MeritNum = $_POST['MeritNum'];
$mtype = $_POST['merit'];
$CatID = $_POST['CatID'];
if($sys_custom['eDiscipline']['AllowEditAPItemCategory'] && !empty($NewCatID))
	$CatID = $NewCatID;

$TagID = $_POST['TagID'];
$Status = $_POST['Status'];
$MaxStudyScore = $_POST['MaxStudyScore'];

if ($mtype!=-1) $mtype=1;

### Check if Item Code duplicate
$dup_result = $ldiscipline->checkDuplicateItemCode($ItemCode, $ItemID);
if($dup_result==0)
{
	$ldiscipline->editCategoryItem($ItemID, $CatID, $ItemCode, $ItemName, $ConductScore, $meritNum, $MeritType, $mtype, $StudyScore, $TagID, $Status, "", $ActivityScore, $MaxStudyScore);
	header("Location: punish_category_item_item_list.php?xmsg=update&ItemID=$itemID&CatID=$CatID");
}
else
{
	header("Location: punish_category_item_item_edit.php?xmsg2=duplicate Item code&ItemID=$ItemID&CatID=$CatID");
}

intranet_closedb();
?>