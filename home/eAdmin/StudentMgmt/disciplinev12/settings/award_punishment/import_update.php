<?php
// Modifying by: 

########## Change Log ###############
#
#	Date	:	2015-04-13 (Bill)	[2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073]
#	Detail	:	import auto select detention and eNotice option value
#
#	Date	:	2010-06-14 (Henry)
#	Detail 	:	import of Category
#
#####################################

$PATH_WRT_ROOT="../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-SETTINGS-Award_Punishment-Access");

$sql = "select * from temp_award_punishment_settings_import where UserID=$UserID";
$recordArr = $ldiscipline->returnArray($sql);

### List out the import result
$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
$x .= "<tr>";
$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$eDiscipline['Setting_CategoryName']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$eDiscipline['Type']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$eDiscipline['Setting_Status']."</td>";
// [2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073] add table header - auto select detention and eNotice option 
$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eDiscipline']['DefaultSetDetention']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eDiscipline']['DefaultSendNotice']."</td>";
//$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eDiscipline']['Import_GM_Col'][4]."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eDiscipline']['Import_GM_Col'][6]."</td>";
$x .= "</tr>";

$typeAry = array('1'=>$i_Discipline_System_Award_Punishment_Awards,'-1'=>$i_Discipline_System_Award_Punishment_Punishments);
$statusAry = array(1=>$eDiscipline['Setting_Status_Using'],2=>$eDiscipline['Setting_Status_NonUsing']);
// [2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073] add $defaultOptionAry
$defaultOptionAry = array(0=>$i_general_no,1=>$i_general_yes);

for($a=0;$a<sizeof($recordArr);$a++)
{
	unset($dataAry);
	$templateInfo = $ldiscipline->retrieveTemplateDetails($recordArr[$a]['TemplateID']);
	$displayTemplateName = ($templateInfo[0]['Title']=="") ? "---" : $templateInfo[0]['Title'];
	
	$x .= "<tr class=\"tablebluerow".($a%2+1)."\">";
	$x .= "<td class=\"tabletext\">".($a+1)."</td>";
	$x .= "<td class=\"tabletext\">". $recordArr[$a]['CategoryName']."</td>";
	$x .= "<td class=\"tabletext\">". $typeAry[$recordArr[$a]['MeritType']] ."</td>";
	$x .= "<td class=\"tabletext\">". $statusAry[$recordArr[$a]['RecordStatus']] ."</td>";
	// [2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073] display auto select detention and eNotice option value
	$x .= "<td class=\"tabletext\">". $defaultOptionAry[$recordArr[$a]['AutoSelectDetention']] ."</td>";
	$x .= "<td class=\"tabletext\">". $defaultOptionAry[$recordArr[$a]['AutoSelecteNotice']] ."</td>";
	$x .= "<td class=\"tabletext\">". $displayTemplateName ."</td>";
	$x .= "</tr>";
	
	$dataAry[] = $recordArr[$a]['MeritType'];
	$dataAry[] = $recordArr[$a]['CategoryName'];
	$dataAry[] = $recordArr[$a]['RecordStatus'];
	$dataAry[] = $recordArr[$a]['TemplateID'];
	// [2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073] add auto select detention and eNotice option value to $dataAry
	$dataAry[] = $recordArr[$a]['AutoSelectDetention'];
	$dataAry[] = $recordArr[$a]['AutoSelecteNotice'];
	//debug_pr($dataAry);
	$ldiscipline->editAwardPunishCategory("add", $dataAry);
	//$accu_record_id = $ldiscipline->insertCategory($recordArr[$a]['CategoryName'],$recordArr[$a]['RecordStatus'],$recordArr[$a]['ConversionPeriod'],$recordArr[$a]['MeritType'],$recordArr[$a]['SeriousLate'], $recordArr[$a]['TemplateID']);

	/*
	if(!$accu_record_id)
	{
		$xmsg="import_failed";		
	}
	*/
}
//exit;
$x .= "</table>";

$xmsg="import_success";


$linterface = new interface_html();

$url = ($MeritType==1) ? "index.php" : "punish_category_item.php";

$import_button = $linterface->GET_ACTION_BTN($button_finish, "button","self.location='$url'")."&nbsp;".
					$linterface->GET_ACTION_BTN($iDiscipline['ImportOtherRecords'], "button", "window.location='import.php?MeritType=$MeritType'");

# menu highlight setting
$CurrentPage = "Settings_AwardPunishment";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eDiscipline['Award_and_Punishment']);

$PAGE_NAVIGATION[] = array($Lang['eDiscipline']['ImportCategory']);

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

$linterface->LAYOUT_START();
?>

<form name="form1" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
</table>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg,$xmsg2);?></td>
</tr>
<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td><?= $x ?></td>
	</tr>
	<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>


<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>