<?php
//editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

$GroupTitle = trim(urldecode(stripslashes($GroupTitle)));


if ($ldiscipline->Rename_Group($GroupTitle,$GroupID)) {
	echo $Lang['StaffAttendance']['GroupRenameSuccess'];
}
else {
	echo $Lang['StaffAttendance']['GroupRenameFail'];
}

intranet_closedb();
?>