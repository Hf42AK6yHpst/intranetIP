<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Award_Punishment-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$i_DemeritType[1] = $i_Merit_BlackMark;
$i_DemeritType[2] = $i_Merit_MinorDemerit;
$i_DemeritType[3] = $i_Merit_MajorDemerit;
$i_DemeritType[4] = $i_Merit_SuperDemerit;
$i_DemeritType[5] = $i_Merit_UltraDemerit;

$merit = (is_array($merit)) ? $merit[0] : $merit;

$selectNeedApproval = $ldiscipline->getAwardPunishNeedApproval($merit);

# current ApprovalGroupID
$sql = "SELECT ApprovalGroupID FROM DISCIPLINE_MERIT_TYPE_SETTING where MeritType=$merit";
$temp = $ldiscipline->returnVector($sql);
$ApprovalGroupID = $temp[0];

# Approval Group select menu
$approvalGroup = $ldiscipline->getAllApprovalGroup();
$approvalGroupSelect = getSelectByArray($approvalGroup, ' name="ApprovalGroupID" id="ApprovalGroupID" ', $ApprovalGroupID, 0, 0, $Lang['General']['NA']);

# menu highlight setting
$CurrentPage = "Settings_AwardPunishment";

$TAGS_OBJ[] = array($i_Merit_Award, "index.php", 0);
$TAGS_OBJ[] = array($i_Merit_Punishment, "punish_category_item.php", 1);
$TAGS_OBJ[] = array($Lang['eDiscipline']['ApprovalGroup'], "approvalGroup.php", 0);
if($sys_custom['Disciplinev12_show_AP_tag']) {
	$TAGS_OBJ[] = array($Lang['eDiscipline']['Tag'], "tag.php", 0);
}


$PAGE_NAVIGATION[] = array($i_Discipline_System_Award_Punishment_Merit_List, "punish_approval.php");
$PAGE_NAVIGATION[] = array($i_Discipline_System_Award_Punishment_Edit, "");

$subTag[] = array($i_Discipline_System_Award_Punishment_Submenu_Category_Item, "punish_category_item.php", 0);
$subTag[] = array($i_Discipline_System_Award_Punishment_Submenu_Approval, "punish_approval.php", 1);
$subTag[] = array($i_Discipline_System_Award_Punishment_Submenu_Promotion, "punish_promotion.php", 0);

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>
<form name="form1" method="post" action="punish_approval_edit_update.php">
<br />
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr> 
		<td align="left">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="tab_underline">
						<div class="shadetabs">
							<ul>
								<?=$ldiscipline->getSubTag($subTag)?>
							</ul>
						</div>
					</td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Merit_Punishment?> </span></td>
								<td><?=$i_DemeritType[abs($merit)] ?></td>
							</tr>
							<tr valign="top">
								<td valign="top" nowrap="nowrap"><?=$i_Discipline_System_NeedApproval?><span class="tabletextrequire">*</span></td>
								<td span class="tabletext">
									<?=$selectNeedApproval?>
								</td>
							</tr>
							<tr valign="top">
								<td valign="top" nowrap="nowrap" class="formfieldtitle">&nbsp;</td>
								<td class="tabletextremark">(<?=$Lang['eDiscipline']['AP_Approval_Number_Note_1']?>)</td>
							</tr>
							<tr valign="top">
								<td valign="top" nowrap="nowrap"><?=$Lang['eDiscipline']['ApprovalGroup']?><span class="tabletextrequire">*</span></td>
								<td span class="tabletext">
									<?=$approvalGroupSelect?>
								</td>
							</tr>
							<tr valign="top">
								<td valign="top" nowrap="nowrap" class="formfieldtitle">&nbsp;</td>
								<td class="tabletextremark">(<?=$Lang['eDiscipline']['AP_Approval_Number_Note_2']?>)</td>
							</tr>
							<tr valign="top">
								<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
								<td>&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
										<?= $linterface->GET_ACTION_BTN($button_save, "submit")?>&nbsp;
										<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
										<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='punish_approval.php'")?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" name="meritType" value="<?=$merit?>">
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
