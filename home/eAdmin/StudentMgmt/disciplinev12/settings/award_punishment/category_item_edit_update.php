<?php
# using : 

########## Change Log ###############
#
#	Date	:	2015-04-10 (Bill)	[2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073]
#	Detail	:	update auto select detention and eNotice option value
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Award_Punishment-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# check whether this itemID is used in GM Conversion
$sql = "SELECT ItemID FROM DISCIPLINE_MERIT_ITEM WHERE CatID=$CatID AND (RecordStatus IS NULL OR RecordStatus=1)";
$itemIDAry = $ldiscipline->returnVector($sql);

if($_POST['Status']==2 && $itemInfo['RecordStatus']!=2) {
	$result = $ldiscipline->checkAPItemUsedInGMConversion($itemIDAry);
	
	if($result && sizeof($itemIDAry)>0) {
		//$msg = $Lang['eDiscipline']['ChangeAPItemStatusWarning'];
		if($merit == 1) {
			header("Location: index.php?xmsg2=11");
		} else {
			header("Location: punish_category_item.php?xmsg2=11");
		}
		exit;
	}
}
# end of checking
$dataAry = array();

$dataAry[] = $merit;
$dataAry[] = $_POST['ItemCode'];
$dataAry[] = $CatID;
$dataAry[] = $Status;
$dataAry[] = $SelectNotice;
// [2015-0128-1044-22170] / [2015-0313-1420-56054] - auto select detention option
$dataAry[] = $DefaultDetention? 1 : 0;
// [2015-0127-1101-48073] - auto select eNotice option
$dataAry[] = $DefaultSendNotice? 1 : 0;

$ldiscipline->editAwardPunishCategory("edit", $dataAry);

intranet_closedb();

if($merit == 1) {
	header("Location: index.php?xmsg=update");
} else {
	header("Location: punish_category_item.php?xmsg=update");
}

?>
