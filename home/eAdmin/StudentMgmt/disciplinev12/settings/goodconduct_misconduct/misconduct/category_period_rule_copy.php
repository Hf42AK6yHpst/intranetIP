<?
# using: Bill

#################################################
#
#	Date:	2019-05-13	(Bill)
#			Prevent SQL Injection
#
#	Date:	2017-09-21	(Bill)	[2017-0710-1151-50054]
#			copy settings - Email to PIC / Class Teacher / eDiscipline Administrator
#
#################################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$CategoryID = IntegerSafe($CategoryID);
$RuleID = IntegerSafe($RuleID);
### Handle SQL Injection + XSS [END]

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-GoodConduct_Misconduct-Access")){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

if(is_array($RuleID)){
	$rule_id = $RuleID[0];
}
else{
	$rule_id = $RuleID;
}

$sql = "SELECT 
				CategoryID, PeriodID, NextNumLate, ProfileMeritType, 
				ProfileMeritNum, ReasonItemID, Reason, ConductScore, SubScore1, 
				DetentionMinutes, DetentionReason, TemplateID, SendNoticeAction, RecordType,
				SendEmailToPIC, SendEmailToClassTeacher, SendEmailToAdmin
		FROM 
				DISCIPLINE_ACCU_CATEGORY_UPGRADE_RULE 
		WHERE 
				RuleID = '$rule_id'";
$arr_result = $ldiscipline->returnArray($sql,13);
if(sizeof($arr_result) > 0)
{
	list($categoryID, $periodID, $next_num_late, $merit_type, $merit_num, $reason_id, $reason, $conduct_score, $sub_score, $detention_mins, $detention_reason, $TemplateID, $sendNoticeAction, $record_type, $SendEmailToPIC, $SendEmailToClassTeacher, $SendEmailToAdmin) = $arr_result[0];
	
	$sql = "SELECT COUNT(*)+1 FROM DISCIPLINE_ACCU_CATEGORY_UPGRADE_RULE WHERE CategoryID = '$CategoryID'";
	$tmp_rule_order = $ldiscipline->returnVector($sql);
	$rule_order = $tmp_rule_order[0];
	
	if(!$sys_custom['UseSubScore1']) {
		$sub_score = 'NULL';
	}
	
	$sql = "INSERT INTO DISCIPLINE_ACCU_CATEGORY_UPGRADE_RULE 
    			(CategoryID, PeriodID, RuleOrder, NextNumLate, ProfileMeritType, ProfileMeritNum, ReasonItemID, Reason, 
    			ConductScore, SubScore1, DetentionMinutes, DetentionReason, TemplateID, SendNoticeAction, RecordType,
    			SendEmailToPIC, SendEmailToClassTeacher, SendEmailToAdmin, DateInput, DateModified)
			VALUES
    			($categoryID, $periodID, $rule_order, $next_num_late, $merit_type, $merit_num, $reason_id, '$reason', 
    			$conduct_score, $sub_score, $detention_mins, '$detention_reason', '$TemplateID', '$sendNoticeAction', $record_type, 
    			'$SendEmailToPIC', '$SendEmailToClassTeacher', '$SendEmailToAdmin', NOW(), NOW())";
	$ldiscipline->db_db_query($sql);
	
	if($ldiscipline->db_affected_rows() > 0){
		header("Location: category_period_rule.php?MeritType=-1&CategoryID=$CategoryID&Scheme=$Scheme&PeriodType=$PeriodType&PeriodID=$PeriodID&msg=1");
		exit();
	}
	else{
		header("Location: category_period_rule.php?MeritType=-1&CategoryID=$CategoryID&Scheme=$Scheme&PeriodType=$PeriodType&PeriodID=$PeriodID&msg=12");
		exit();
	}
}
?>