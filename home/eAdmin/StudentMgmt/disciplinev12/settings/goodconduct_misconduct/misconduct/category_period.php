<?
// Modifying by : 

########## Change Log ###############
#
#	Date	:	2019-05-13	(Bill)
#	Details :	Prevent SQL Injection
#
#	Date	:	2017-10-13 (Bill)	[DM#3273]
#	Details :	fixed Session Problem in PHP 5.4
#
#####################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# Preserve Table View
if ($ck_category_page_number!=$pageNo && $pageNo!="")
{
        setcookie("ck_category_page_number", $pageNo, 0, "", "", 0);
        $ck_category_page_number = $pageNo;
}
else if (!isset($pageNo) && $ck_category_page_number!="")
{
        $pageNo = $ck_category_page_number;
}

if ($ck_category_page_order!=$order && $order!="")
{
        setcookie("ck_category_page_order", $order, 0, "", "", 0);
        $ck_category_page_order = $order;
}
else if (!isset($order) && $ck_category_page_order!="")
{
        $order = $ck_category_page_order;
}

if ($ck_category_page_field!=$field && $field!="")
{
        setcookie("ck_category_page_field", $field, 0, "", "", 0);
        $ck_category_page_field = $field;
}
else if (!isset($field) && $ck_category_page_field!="")
{
        $field = $ck_category_page_field;
}

if (!isset($meritType) || $meritType == "")
{
	   $meritType = -1;	
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if(!isset($CategoryID)) {
	header("category.php");
	intranet_closedb();
	exit();
}

### Handle SQL Injection + XSS [START]
$CategoryID = IntegerSafe($CategoryID);
$PeriodType = IntegerSafe($PeriodType);
### Handle SQL Injection + XSS [END]

$ldiscipline = new libdisciplinev12();
$lfile = new libfilesystem();
$linterface = new interface_html();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-GoodConduct_Misconduct-Access")){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$CurrentPage = "Settings_GoodConductMisconduct";
$CurrentPageArr['eDisciplinev12'] = 1;


### Tag Object ###
$TAGS_OBJ[] = array($eDiscipline['Setting_GoodConduct'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/goodconduct/category.php", 0);
$TAGS_OBJ[] = array($eDiscipline['Setting_Misconduct'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/category.php", 1);

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$SUBTAGS_OBJ[] = array($eDiscipline['Setting_CategorySettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/category.php", 1);
$SUBTAGS_OBJ[] = array($eDiscipline['Setting_DefaultPeriodSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/default_period.php", 0);
if($ldiscipline->use_newleaf && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-NewLeaf")) {
	$SUBTAGS_OBJ[] = array($eDiscipline['Setting_NewLeaf'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/newleaf.php", 0);
}

$subtags_table .= "<table width=\"96%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
$subtags_table .= "<tr height=\"5px\"><td></td></tr>";
$subtags_table .= "<tr>
					<td class=\"tab_underline\"><div class=\"shadetabs\">
						<ul>
					";
for($i=0; $i<sizeof($SUBTAGS_OBJ); $i++){
	if($SUBTAGS_OBJ[$i][2] == 1){
		$subtags_table .= "<li class=\"selected\"><a href=\"".$SUBTAGS_OBJ[$i][1]."\"><strong>".$SUBTAGS_OBJ[$i][0]."</strong></a></li>";
	}
	else{
		$subtags_table .= "<li><a href=\"".$SUBTAGS_OBJ[$i][1]."\">".$SUBTAGS_OBJ[$i][0]."</a></li>";
	}
}
$subtags_table .= "</ul></td></tr>";
$subtags_table .= "</table>";

### Navigation ###
$sql = "SELECT Name FROM DISCIPLINE_ACCU_CATEGORY WHERE CategoryID = '$CategoryID'";
$arr_cat_name = $ldiscipline->returnVector($sql);
$CatName = $arr_cat_name[0];

$temp = array(array($eDiscipline['Setting_NAV_CategoryList'],"category.php"),array("$CatName",""));
$infobar1 .= "<tr><td colspan=\"2\" class=\"navigation\">".$linterface->GET_NAVIGATION($temp)."</td></tr>"; 

if($PeriodType != 0){
	$toolbar = $linterface->GET_LNK_NEW("javascript:checkNew('category_period_new.php?CategoryID=$CategoryID&PeriodType=$PeriodType')","","","","",0);
}

$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkEdit(document.form1,'PeriodID[]','category_specific_period_edit.php?CategoryID=$CategoryID&PeriodType=$PeriodType')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_edit
					</a>
				</td>";
$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkRemove(document.form1,'PeriodID[]','category_specific_period_remove.php?CategoryID=$CategoryID')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_remove
					</a>
				</td>";

# Generate System Message #
if($error == 1)
{
	$SysMsg = $linterface->GET_SYS_MSG("",$eDiscipline['Accumulative_Period_InUse_Warning']);
}
else if($error == 2)
{
	$SysMsg = $linterface->GET_SYS_MSG("",$eDiscipline['Accumulative_Period_InUse_Warning_Update']);
}
else
{
	if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
	if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
	if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
	if ($msg == 12) $SysMsg = $linterface->GET_SYS_MSG("add_failed");
	if ($msg == 13) $SysMsg = $linterface->GET_SYS_MSG("delete_failed");
	if ($msg == 14) $SysMsg = $linterface->GET_SYS_MSG("update_failed");
}
# End #

### Check the semester mode - auto change / manual change ###
$semester_mode = $lfile->file_read($intranet_root."/file/semester_mode.txt");

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if (!isset($field)) $field = 0;
if (!isset($order)) $order = 1;
$order = ($order == 1) ? 1 : 0;

$PeriodSet = $ldiscipline->returnPeriodSetID($CategoryID);

#echo $iDiscipline['Accumulative_Category_Upgrade_Calculation_Scheme_1']."<BR>";
#echo $iDiscipline['Accumulative_Category_Upgrade_Calculation_Scheme_2']."<BR>";
#echo $iDiscipline['Accumulative_Category_Upgrade_Calculation_Scheme_Undefined'];

#$defined_field_link = $CategoryID==1?$iDiscipline['Accumulative_Category_Upgrade_Calculation_Scheme_1']:$eDiscipline['Setting_CalculationSchema_Static'];
$defined_field_link = $eDiscipline['Setting_CalculationSchema_Static'];

$period_field = " CONCAT(a.DateStart, ' $i_To ' , a.DateEnd,'  ')";
$scheme_field = " IF(c.RuleID IS NOT NULL,'".$eDiscipline['Setting_CalculationSchema_Floating']."', (IF(b.SettingID IS NOT NULL, '".$defined_field_link."', '".$eDiscipline['Setting_CalculationSchema_Undefined']."')))";

if($PeriodType == CATEGORY_PERIOD_CONDITION_DEFAULT)
{
	$sql = "SELECT
					DISTINCT $period_field,
					If(c.RuleID IS NOT NULL,CONCAT('<a class=\"tablelink\" href=\'category_period_rule.php?meritType=".MISCONDUCT."&CategoryID=$CategoryID&Scheme=1&PeriodType=$PeriodType&PeriodID=',a.PeriodID,'\'>',$scheme_field,'</a>   '),
						IF(b.SettingID IS NOT NULL, CONCAT('<a class=\"tablelink\" href=\'category_period_edit.php?meritType=".MISCONDUCT."&CategoryID=$CategoryID&Scheme=0&PeriodType=$PeriodType&PeriodID=',a.PeriodID,'\'>',$scheme_field,'</a>   '),
							CONCAT('<a class=\"tablelink\" href=\'category_period_new2.php?meritType=".MISCONDUCT."&CategoryID=$CategoryID&PeriodType=$PeriodType&PeriodID=',a.PeriodID,'\'>',$scheme_field,'</a>   '))
					)
			FROM 
					DISCIPLINE_ACCU_PERIOD AS a LEFT OUTER JOIN 
					DISCIPLINE_ACCU_CATEGORY_PERIOD_SETTING AS b ON (a.PeriodID = b.PeriodID AND b.CategoryID = '$CategoryID') LEFT OUTER JOIN 
					DISCIPLINE_ACCU_CATEGORY_UPGRADE_RULE AS c ON (c.CategoryID = '$CategoryID' AND c.PeriodID = a.PeriodID) 
			WHERE
					(a.RecordType = ".MISCONDUCT." OR a.RecordType IS NULL) AND
					a.SetID = '$PeriodSet' ";
}
else
{
	$sql = "SELECT 
					DISTINCT CONCAT('<a class=\"tablelink\" href=\"category_specific_period_edit.php?meritType=".MISCONDUCT."&CategoryID=$CategoryID&Scheme=1&PeriodType=$PeriodType&PeriodID=',a.PeriodID,'\">',$period_field,'</a>   '),
					If(c.RuleID IS NOT NULL,CONCAT('<a class=\"tablelink\" href=\'category_period_rule.php?meritType=".MISCONDUCT."&CategoryID=$CategoryID&Scheme=1&PeriodType=$PeriodType&PeriodID=',a.PeriodID,'\'>',$scheme_field,'</a>   '),
						IF(b.SettingID IS NOT NULL, CONCAT('<a class=\"tablelink\" href=\'category_period_edit.php?meritType=".MISCONDUCT."&CategoryID=$CategoryID&Scheme=0&PeriodType=$PeriodType&PeriodID=',a.PeriodID,'\'>',$scheme_field,'</a>   '),
							CONCAT('<a class=\"tablelink\" href=\'category_period_new2.php?meritType=".MISCONDUCT."&CategoryID=$CategoryID&PeriodType=$PeriodType&PeriodID=',a.PeriodID,'\'>',$scheme_field,'</a>   '))
					),
					CONCAT('<input type=\"checkbox\" name=\"PeriodID[]\" value=\"',a.PeriodID,'\">')
			FROM 			
					DISCIPLINE_ACCU_PERIOD AS a LEFT OUTER JOIN 
					DISCIPLINE_ACCU_CATEGORY_PERIOD_SETTING AS b ON (a.PeriodID = b.PeriodID AND b.CategoryID = '$CategoryID') LEFT OUTER JOIN 
					DISCIPLINE_ACCU_CATEGORY_UPGRADE_RULE AS c ON (c.CategoryID = '$CategoryID' AND c.PeriodID = a.PeriodID)
			WHERE
					(a.RecordType = ".MISCONDUCT." OR a.RecordType IS NULL) AND
					a.SetID = '$PeriodSet' ";
}
/*
if($semester_mode == SEMESTER_MODE_MANUALLY){
	$li = new libdbtable2007($field, $order, $pageNo);
	$li->field_array = array("a.DateStart, a.DateEnd","a.RuleID");
	$li->sql = $sql;
	$li->no_col = sizeof($li->field_array)+2;
	$li->title = "";
	$li->column_array = array(0,0,0,0);
	$li->wrap_array = array(0,0,0,0);
	$li->IsColOff = 2;
	//echo $li->built_sql();

	// TABLE COLUMN
	$pos = 0;
	$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
	$li->column_list .= "<td width='30%' >".$li->column($pos++, $eDiscipline['Setting_Period'])."</td>\n";
	$li->column_list .= "<td width='70%' >".$li->column($pos++, $eDiscipline['Setting_ConversionScheme'])."</td>\n";
	//$li->column_list .= "<td width='30%'>".$li->column($pos++, $i_Discipline_Semester)."</td>\n";
	$li->column_list .= "<td width='1'>".$li->check("PeriodID[]")."</td>\n";
}else{
*/
	$li = new libdbtable2007($field, $order, $pageNo);
	$li->field_array = array("a.DateStart, a.DateEnd","a.RuleID");
	$li->sql = $sql;
	$li->no_col = sizeof($li->field_array)+2;
	$li->title = "";
	$li->column_array = array(0,0,0);
	$li->wrap_array = array(0,0,0);
	$li->IsColOff = 2;
	//echo $li->built_sql();

	// TABLE COLUMN
	$pos = 0;
	$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
	$li->column_list .= "<td width='49%' >".$li->column($pos++, $eDiscipline['Setting_Period'])."</td>\n";
	$li->column_list .= "<td width='49%' >".$li->column($pos++, $eDiscipline['Setting_CalculationScheme'])."</td>\n";
	$li->column_list .= ($PeriodType==1) ? "<td width='1'>".$li->check("PeriodID[]")."</td>\n" : "<td width='1'>&nbsp;</td>";
//}
?>

<form name="form1" action="" method="POST">
<?=$subtags_table;?>
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<?=$infobar1?>
	<tr>
		<td align="right"><?=$SysMsg?></td>
	</tr>
	<tr>
		<td align="left"><?=$toolbar?></td>
	</tr>
</table>

<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
<? if($PeriodType==1) { ?>
	<tr>	
		<td align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<?=$table_tool?>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
<? } ?>
	<tr><td colspan="2"><?= $li->display() ?></td></tr>
</table>



<table border="0" width="96%" cellpadding="5" cellspacing="0">
<?=$table_content;?>
</table>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>