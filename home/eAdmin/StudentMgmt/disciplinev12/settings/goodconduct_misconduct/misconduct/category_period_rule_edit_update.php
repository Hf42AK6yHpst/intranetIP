<?
### This page is only for floating calcuation use ###

# using: Bill

#################################################
#
#	Date:	2017-09-21	(Bill)	[2017-0710-1151-50054]
#			update settings - Email to PIC / Class Teacher / eDiscipline Administrator
#
#################################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-GoodConduct_Misconduct-Access")){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

if(!$sendNoticeAction) {
	$sql = "SELECT TemplateID FROM DISCIPLINE_ACCU_CATEGORY_UPGRADE_RULE WHERE RuleID=$RuleID";
	$temp = $ldiscipline->returnVector($sql);
	$SelectNotice0 = $temp[0]; 
}

$result = $ldiscipline->editCategoryRule($RuleID, $accumlativeTimes, $meritType, $meritNum, $conductMark, $studyMark, $meritItemID, $SelectNotice0, $sendNoticeAction, $SendEmailToPIC, $SendEmailToClassTeacher, $SendEmailToAdmin);

intranet_closedb();
		
if($result){
	header("Location: category_period_rule.php?meritType=-1&CategoryID=$CategoryID&Scheme=$Scheme&PeriodType=$PeriodType&PeriodID=$PeriodID&msg=2");
	exit();
}
else{
	header("Location: category_period_rule.php?meritType=-1&CategoryID=$CategoryID&Scheme=$Scheme&PeriodType=$PeriodType&PeriodID=$PeriodID&msg=14");
	exit();
}
?>