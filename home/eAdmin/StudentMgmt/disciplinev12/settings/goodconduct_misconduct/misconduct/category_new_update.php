<?
// Using : 

########## Change Log ###############
#
#	Date	:	2015-04-10 (Bill)	[2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073]
#	Detail	:	insert auto select detention and eNotice option value
#
#	Date	:	2010-06-10 (Henry)
#	Detail 	:	can choose default eNtotice Template
#
#####################################

$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

##$sql = "INSERT INTO DISCIPLINE_ACCU_CATEGORY (Name,RecordStatus,DateInput,DateModified,MeritType) VALUES ('$CategoryName',$Status,NOW(),NOW(),GOOD_CONDUCT)";
##$CategoryID = $lpayment->db_insert_id();
##$sql = "INSERT INTO DISCIPLINE_ACCU_CATEGORY_PERIOD (CategoryID, RecordType, RecordStatus) VALUES ($CategoryID,GOOD_CONDUCT,$Period)";
##DISCIPLINE_ACCU_CATEGORY_PERIOD >>> $Period = RecordStatus

##DISCIPLINE_ACCU_CATEGORY >>> RecordStatus = $Status

// [2015-0127-1101-48073] pass auto select detention and eNotice option value
$result = $ldiscipline->insertCategory($CategoryName, $Status, $Period, MISCONDUCT, $LateMinutes, $SelectNotice, ($DefaultDetention? 1 : 0), ($DefaultSendNotice? 1 : 0));

intranet_closedb();
if($result){
	header("Location: category.php?msg=1");
	exit();
}else{
	header("Location: category.php?msg=12");
	exit();
}

?>