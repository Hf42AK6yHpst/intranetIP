<?
# using : 

########## Change Log ###############
#
#	Date	:	2015-04-10 (Bill)	[2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073]
#	Detail	:	update auto select detention and eNotice option value
#
#	Date	:	2014-05-28 (Carlos)
#				*** REMOVED *** $sys_custom['eDiscipline']['PooiToMiddleSchool'] - add a score setting for preset late category
#
#	Date	:	2010-06-10 (Henry)
#	Detail 	:	can choose default eNtotice Template
#
#####################################

$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

#echo $CategoryName;
#echo $Status;
#echo $CategoryID;
#$sql = "UPDATE DISCIPLINE_ACCU_CATEGORY SET CategoryName = '$CategoryName', RecordStatus = $Status WHERE CategoryID = $CategoryID";

//if($sys_custom['eDiscipline']['PooiToMiddleSchool'] && $CategoryID==PRESET_CATEGORY_LATE){
//	$result = $ldiscipline->updateCategory($CategoryID,$CategoryName,$Status,$Period,MISCONDUCT,$LateMinutes,$SelectNotice,$Score);
//}else{
	// [2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073] pass auto select detention and eNotice option value
	$result = $ldiscipline->updateCategory($CategoryID, $CategoryName, $Status, $Period, MISCONDUCT, $LateMinutes, $SelectNotice, '', ($DefaultDetention? 1 : 0), ($DefaultSendNotice? 1 : 0));
//}

intranet_closedb();

if($result){
	header("Location: category.php?msg=2");
	exit();
}else{
	header("Location: category.php?msg=14");
	exit();
}

?>