<?
# using : 

########## Change Log ###############
#
#   Date    :   2018-10-18 (Bill)   [2018-0831-0952-30073]
#               Allow Late Category to get Category's Accumulation Period Settings  ($sys_custom['eDiscipline']['LateApplyCategoryPeriodSetting'])
#
#   Date    :   2018-09-04 (Bill)   [2018-0831-0952-30073]
#               Allow Late Category to set Category's Accumulation Period   ($sys_custom['eDiscipline']['LateApplyCategoryPeriodSetting'])
#
#	Date	:	2015-04-10 (Bill)	[2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073]
#	Detail	:	add auto select detention and eNotice option
#
#	Date	:	2014-06-26 (Carlos)
#				$sys_custom['eDiscipline']['PooiToMiddleSchool'] - hide period type setting				
#
#	Date	:	2014-05-28 (Carlos)
#				*** REMOVED *** $sys_custom['eDiscipline']['PooiToMiddleSchool'] - add a score setting for preset late category
#
#	Date	:	2010-06-10 (Henry)
#	Detail 	:	can choose default eNtotice Template
#
#####################################

$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-GoodConduct_Misconduct-Access")){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$CurrentPage = "Settings_GoodConductMisconduct";


if($CategoryID <= 3){
	$ShowSeriousLate = 0;
}else{
	$ShowSeriousLate = 1;
}

### Tag Object ###
$TAGS_OBJ[] = array($eDiscipline['Setting_GoodConduct'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/goodconduct/category.php", 0);
$TAGS_OBJ[] = array($eDiscipline['Setting_Misconduct'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/category.php", 1);


$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$SUBTAGS_OBJ[] = array($eDiscipline['Setting_CategorySettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/category.php", 1);
if(!$sys_custom['eDiscipline']['PooiToMiddleSchool']){
	$SUBTAGS_OBJ[] = array($eDiscipline['Setting_DefaultPeriodSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/default_period.php", 0);
	if($ldiscipline->use_newleaf && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-NewLeaf"))
		$SUBTAGS_OBJ[] = array($eDiscipline['Setting_NewLeaf'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/newleaf.php", 0);
}
						
$subtags_table .= "<table width=\"96%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
$subtags_table .= "<tr height=\"5px\"><td></td></tr>";
$subtags_table .= "<tr>
					<td class=\"tab_underline\"><div class=\"shadetabs\">
						<ul>
					";
for($i=0; $i<sizeof($SUBTAGS_OBJ); $i++){
	if($SUBTAGS_OBJ[$i][2] == 1){
		$subtags_table .= "<li class=\"selected\"><a href=\"".$SUBTAGS_OBJ[$i][1]."\"><strong>".$SUBTAGS_OBJ[$i][0]."</strong></a></li>";
	}else{
		$subtags_table .= "<li><a href=\"".$SUBTAGS_OBJ[$i][1]."\">".$SUBTAGS_OBJ[$i][0]."</a></li>";
	}
}
$subtags_table .= "</ul></td></tr>";
$subtags_table .= "</table>";

### Navigation ###
$temp = array(array($eDiscipline['Setting_NAV_CategoryList'],"category.php"),array($eDiscipline['Setting_NAV_EditCategory'],""));
$infobar1 .= "<tr><td colspan=\"2\" class=\"navigation\">".$linterface->GET_NAVIGATION($temp)."</td></tr>"; 

# Generate System Message #
if ($msg != "") {
	if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
	if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
	if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
	if ($msg == 12) $SysMsg = $linterface->GET_SYS_MSG("add_failed");
	if ($msg == 13) $SysMsg = $linterface->GET_SYS_MSG("delete_failed");
	if ($msg == 14) $SysMsg = $linterface->GET_SYS_MSG("update_failed");
}
# End #

if(is_array($CategoryID)){
	$category_id = $CategoryID[0];
}else{
	$category_id = $CategoryID;
}

// [2018-0831-0952-30073]
if($sys_custom['eDiscipline']['LateApplyCategoryPeriodSetting'] || $category_id != 1) {
	$sql = "SELECT 
					a.Name, a.RecordStatus, b.RecordStatus, a.LateMinutes, a.TemplateID, a.AutoSelectDetention, a.AutoSelecteNotice
			FROM 
					DISCIPLINE_ACCU_CATEGORY AS a LEFT OUTER JOIN 
					DISCIPLINE_ACCU_CATEGORY_PERIOD AS b ON (a.CategoryID = b.CategoryID)
			WHERE 
					a.CategoryID = $category_id";
					
	$arr_Category = $ldiscipline->returnArray($sql,3);
	if(sizeof($arr_Category) > 0) {
		// [2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073] - get auto select detention and eNotice option value
		list($cat_name, $cat_status, $period, $late_minutes, $TemplateID, $DefaultDetention, $DefaultSendNotice) = $arr_Category[0];
	}
} else {
	$sql = "SELECT 
					a.Name, a.RecordStatus, a.TemplateID, a.AutoSelectDetention, a.AutoSelecteNotice ";
	//if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
	//	$sql .= ",a.Score ";
	//}
	$sql.= "FROM 
					DISCIPLINE_ACCU_CATEGORY AS a 
			WHERE 
					a.CategoryID = $category_id";
					
	$arr_Category = $ldiscipline->returnArray($sql,2);
	
	if(sizeof($arr_Category)>0){
		// [2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073] - get auto select detention and eNotice option value
		list($cat_name, $cat_status, $TemplateID, $DefaultDetention, $DefaultSendNotice) = $arr_Category[0];
		//if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
		//	$Score = $arr_Category[0]['Score'];
		//}
	}
}
if($TemplateID=="")
	$TemplateID = 0;
	
$TemplateAry = $ldiscipline->retrieveTemplateDetails($TemplateID);

$TemplateCategoryID = $TemplateAry[0]['CategoryID'];


//if($category_id>=1 && $category_id<=3) {
//	$showCatName = "<input class='formtextbox' type='text' name='CategoryName' value='$cat_name'>";
//} else {
	$showCatName = "<input class='formtextbox' type='text' name='CategoryName' maxlength='80' value='$cat_name'>";	
//}

# eNotice Template
$catTmpArr = $ldiscipline->TemplateCategory();
if(is_array($catTmpArr))
{
	$catArr[0] = array("0","-- $button_select --");
	foreach($catTmpArr as $Key=>$Value)
	{
		# check the Template Catgory has template or not
		$NoticeTemplateAvaTemp = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO("",1, $Key);
		if(!empty($NoticeTemplateAvaTemp))
			$catArr[] = array($Key,$Key);
	}
}
$templateCat = $linterface->GET_SELECTION_BOX($catArr, 'id="TemplateCategoryID", name="TemplateCategoryID" onChange="changeCat(this.value)"', "", $TemplateCategoryID);

// Preset template items (for javascript)
for ($i=0; $i<sizeof($catArr); $i++) {
	$result = $ldiscipline->getDetentionENoticeByCategoryID("DISCIPLINE", $catArr[$i][0]);
	for ($j=0; $j<=sizeof($result); $j++) {
		if ($j==0) {
			$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]."Value = new Array(".(sizeof($result)+1).");\n";
			$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]." = new Array(".(sizeof($result)+1).");\n";
			$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."Value[$j] = \"0\"\n";
			$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."[$j] = \"-- $button_select --\"\n";
			//$js .= "obj.options[$j] = new Option('-- $button_select --',0);\n";
		} else {
			$tempTemplate = $result[$j-1][1];
			$tempTemplate=str_replace("&lt;", "<", $tempTemplate);
	        $tempTemplate=str_replace("&gt;", ">", $tempTemplate);
	        $tempTemplate=str_replace("&#039;","'",$tempTemplate);
	        $tempTemplate=str_replace("&amp;", "&", $tempTemplate);
			//$tempTemplate=str_replace("&quot;", "\"", $tempTemplate);

			$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."Value[$j] = \"".$result[$j-1][0]."\"\n";
			$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."[$j] = \"".$tempTemplate."\"\n";
			//$js .= "obj.options[$j] = new Option('$tempTemplate',".$result[$j-1][0].");\n";
		}
	}
}
?>

<script language="javascript">
<?=$jTemplateString?>


function checkForm(){
	var obj = document.form1;
	var LateMinCheck = 1;
	var AttendancePlugIn = <?= ($plugin['attendancestudent'] ? 1 : 0) ?>;
	var displayAttendanceAlert = 0;
	
	<? if($sys_custom['Discipline_SeriousLate'] && $ShowSeriousLate) { ?>
			if(obj.SeriousLate[0].checked)
			{
				if(check_positive_int(obj.LateMinutes,"Invalid Mins",0,0)){
					LateMinCheck = 1;
				}else{
					LateMinCheck = 0;
				}
			}
	<? } ?>
	
	if(AttendancePlugIn && document.getElementById('CategoryID').value==1 && document.getElementById('original_status').value==1 && document.getElementById('Status2').checked) {
		displayAttendanceAlert = 1;
	} 
	
	if(check_text(obj.CategoryName,"<?=$eDiscipline['Setting_Warning_InputCategoryName'];?>")){
		if(LateMinCheck == 1){
			if(displayAttendanceAlert) {
				if(confirm('<?=$Lang['eDiscipline']['js_affect_eAttendance_Alert']?>')) {
					return true;
				} else {
					return false;	
				}
			} else {
				return true;	
			}
		}	
	}
	
	
	return false;
}

function showLateMinsDiv(val)
{
	if(val == 1){
		$("#LateMinutesDiv1").show();
		$("#LateMinutesDiv2").show();
		$("#LateMinutesTD").attr("class","formfieldtitle tabletext");
	}else{
		$("#LateMinutesDiv1").hide();
		$("#LateMinutesDiv2").hide();
		$("#LateMinutesTD").attr("class","");
	}
}

function changeCat(cat){

	var x=document.getElementById("SelectNotice");

	if(x)
	{
		var listLength = x.length;
	}
	for (var i=listLength-1; i>=0; i--) {
		x.remove(i);
	}
	try {
		var tmpCatLength = eval("jArrayTemplate"+cat).length;
	} catch (err) {
		var tmpCatLength = 0;
	}


	for (var j=0; j<tmpCatLength; j++) {
		var y = document.createElement('option');
		y.text = eval("jArrayTemplate"+cat)[j];
		y.value = eval("jArrayTemplate"+cat+"Value")[j];

		try {
			x.add(y,null); // standards compliant
		} catch(ex) {
			x.add(y); // IE only
		}
	}

	/****************
	obj = x;
	<?
	
	for ($i=0; $i<sizeof($catArr); $i++) {
		
		$result = $ldiscipline->getDetentionENoticeByCategoryID("DISCIPLINE", $catArr[$i][0]);?>
		
		if(cat=="<?=$catArr[$i][0]?>") {
			<?
			for ($j=0; $j<=sizeof($result); $j++) { 
				if($result[$j][0]==0) {
					echo "obj.options[0] = new Option('-- $button_select --',0);\n";	
				} else {
					$tempTemplate = $result[$j-1][1];
					echo "obj.options[$j] = new Option('$tempTemplate',".$result[$j-1][0].");\n";
				}
			}
			
		?> } <?
	}
	
	?>
	****************/
}

function itemSelected(itemID) {
	var obj = document.form1;
	var item_length = obj.SelectNotice.length;
	for(var i=0; i<item_length; i++) {
		if(obj.elements['SelectNotice'].options[i].value==<?=$TemplateID?>) {
			obj.elements['SelectNotice'].selectedIndex = i;
			//alert(i);
			break;	
		}
	}
}

</script>

<?=$subtags_table;?>
<table width="98%" border="0" cellpadding="5" cellspacing="0" align="center">
	<?=$infobar1?>
	<tr><td align="right"><?=$SysMsg;?></td></tr>
</table>

<form name="form1" id="form1" action="category_edit_update.php" method="POST" onSubmit="return checkForm();">
<table border="0" width="88%">
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['Setting_CategoryName']?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext" valign="top"><?=$showCatName?></td>
	</tr>
<? if($sys_custom['eDiscipline']['LateApplyCategoryPeriodSetting'] || ($category_id != 1 && !$sys_custom['eDiscipline']['PooiToMiddleSchool'])){ ?>
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['Setting_Period'];?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext" valign="top">
		<?
			if($period == 0 || $period == ""){
				$period_default_checked = " CHECKED ";
				$period_specify_checked = " ";
			}else{
				$period_default_checked = " ";
				$period_specify_checked = " CHECKED ";
			}
		?>
			<input type="radio" name="Period" id="Period0" value="0" <?=$period_default_checked;?> ><label for="Period0"><?=$eDiscipline['Setting_Period_Default_Setting'];?></label>
			<input type="radio" name="Period" id="Period1" value="1" <?=$period_specify_checked;?> ><label for="Period1"><?=$eDiscipline['Setting_Period_Specify_Setting'];?></label>
		</td>
	</tr>
<?php } ?>
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['Setting_Status'];?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext" valign="top">
			<?
				if($cat_status == 1){
					$publish_selected = " CHECKED ";
					$draft_selected = "";
				}
				if($cat_status == 2){
					$publish_selected = "";
					$draft_selected = " CHECKED ";
				}
			?>
			<input type="radio" name="Status" id="Status2" value="2" <?=$draft_selected;?> ><label for="Status2"><?=$eDiscipline['Setting_Status_Drafted'];?></label>
			<input type="radio" name="Status" id="Status1" value="1" <?=$publish_selected;?> ><label for="Status1"><?=$eDiscipline['Setting_Status_Published'];?></label>
		</td>
	</tr>
							
	<!-- Default Set Detention -->
	<tr valign="top">
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['DefaultSetDetention']?></td>
		<td class="tabletext">
			<input type="radio" name="DefaultDetention" value="0" id='DefaultDetention0' <? if(!$DefaultDetention) echo " checked";?>><label for="DefaultDetention0"><?=$Lang['General']['No'];?></label>
			<input type="radio" name="DefaultDetention" value="1" id='DefaultDetention1' <? if($DefaultDetention) echo " checked";?>><label for="DefaultDetention1"><?=$Lang['General']['Yes'];?></label>
		</td>
	</tr>
							
	<!-- Default Set Send eNotice -->
	<tr valign="top">
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['DefaultSendNotice']?></td>
		<td class="tabletext">
			<input type="radio" name="DefaultSendNotice" value="0" id='DefaultSendNotice0' <? if(!$DefaultSendNotice) echo " checked";?>><label for="DefaultSendNotice0"><?=$Lang['General']['No'];?></label>
			<input type="radio" name="DefaultSendNotice" value="1" id='DefaultSendNotice1' <? if($DefaultSendNotice) echo " checked";?>><label for="DefaultSendNotice1"><?=$Lang['General']['Yes'];?></label>
		</td>
	</tr>
	
	<? if($sys_custom['Discipline_SeriousLate'] && $ShowSeriousLate){ ?>
	<!-- Show Late Minutes -->
	<?  if($late_minutes!="") { 
			$DisplayStyle = "";	
			$TDClass = "class=\"formfieldtitle tabletext\"";
		}else{
			$DisplayStyle = "style=\"display:none;\"";
			$TDClass = "";
		}
	?>
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"> <?=$Lang['eDiscipline']['FieldTitle']['SeriousLate']?><span class="tabletextrequire">*</span></td>
		<td>
			<input type="radio" name="SeriousLate" id="SeriousLate1" value="1" onClick="showLateMinsDiv(this.value)" <? if($late_minutes!=""){ ?> CHECKED <?}?> ><label for="SeriousLate1"> <?=$Lang['General']['Yes']?></label>
			<input type="radio" name="SeriousLate" id="SeriousLate2" value="2" onClick="showLateMinsDiv(this.value)" <? if($late_minutes==""){ ?> CHECKED <?}?> ><label for="SeriousLate2"> <?=$Lang['General']['No']?></label>
		</td>
	</tr>
	<tr>
		<td id="LateMinutesTD" valign="top" width="25%" nowrap="nowrap" <?=$TDClass?> >
			<div id="LateMinutesDiv1" <?=$DisplayStyle;?> >
				<?=$Lang['eDiscipline']['FieldTitle']['LateMinutes']?><span class="tabletextrequire">*</span>
			</div>
		</td>
		<td>
			<div id="LateMinutesDiv2" <?=$DisplayStyle;?> >
				<input type="text" name="LateMinutes" id="LateMinutes" value="<?=$late_minutes?>">
			</div>
		</td>
	</tr>
	<? } ?>
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['eNoticeTemplate']?></td>
		<td class="tabletext" valign="top">
			<?=$templateCat?>
			<select name="SelectNotice" id="SelectNotice"><option value="0">-- <?=$button_select?> --</option></select>
		</td>
	</tr>
	
	<?php
	/*
	if($sys_custom['eDiscipline']['PooiToMiddleSchool'] && $category_id == PRESET_CATEGORY_LATE)
	{
		$score_selection = '<SELECT name="Score" id="Score">'."\n";
		$conductMarkInterval = $sys_custom['eDiscipline']['ConductMark1DecimalPlace'] ? 0.1 : 1;
		$increment_max = $ldiscipline->MisconductMaxScoreDeduction != '' ? $ldiscipline->MisconductMaxScoreDeduction : 3;
		$i = 0;
		$loopConductMark = true;
		while($loopConductMark) {
			$score_selection .= '<OPTION value="'.$i.'"'.($Score == $i?' selected':'').'>'.$i.'</OPTION>'."\n";    
			$i = (float)(string)($i + $conductMarkInterval);
			if(floatcmp($i ,">" ,$increment_max)) {
				$loopConductMark = false;
			}
		}
		$score_selection .= '</SELECT>'."\n";
		
		echo '<tr> 
				<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext">'.$Lang['eDiscipline']['Score'].'</td>
				<td class="tabletext" valign="top">'.$Lang['eDiscipline']['Deduct'].' '.$score_selection.'<td>
			</tr>';
	}
	*/
	?>
	
	<tr>
		<td colspan="2" align="left" class="tabletextremark"><?=$i_general_required_field2?></td>
	</tr>
	<tr>
		<td colspan="2" class="dotline"><img src="<?=$image_path;?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td height="10px"></td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<? echo $linterface->GET_ACTION_BTN($button_submit, "submit"); ?>
			<? echo $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\""); ?>
			<? echo $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back();","cancel"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\""); ?>
		</td>
	</tr>
	<input type="hidden" name="CategoryID" id="CategoryID" value="<?=$category_id;?>">
	<input type="hidden" name="originaL_status" id="original_status" value="<?=$cat_status;?>">
</table>
</form>

<?
if($TemplateCategoryID!="0" && $TemplateCategoryID!="") {
	echo "<script language='javascript'>changeCat('$TemplateCategoryID');itemSelected('$TemplateID')</script>";
}

intranet_closedb();
$linterface->LAYOUT_STOP();
?>