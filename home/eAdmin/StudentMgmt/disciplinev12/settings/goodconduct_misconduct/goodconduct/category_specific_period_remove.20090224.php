<?
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-GoodConduct_Misconduct-Access")){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$CurrentPageArr['eDisciplinev12'] = 1;
$CurrentPage = "Settings_GoodConductMisconduct";

//if(is_array($PeriodID)){
//	$targetPeriod = implode(",",$PeriodID);
//}else{
//	$targetPeriod = $PeriodID;
//}

## Get the PeriodID - Using STATIC increment
$sql = "SELECT DISTINCT PeriodID FROM DISCIPLINE_ACCU_CATEGORY_PERIOD_SETTING WHERE CategoryID = $CategoryID";
$arr_result1 = $ldiscipline->returnVector($sql);

## Get the PeriodID - Using FLOATING increment
$sql = "SELECT DISTINCT PeriodID FROM DISCIPLINE_ACCU_CATEGORY_UPGRADE_RULE WHERE CategoryID = $CategoryID";
$arr_result2 = $ldiscipline->returnVector($sql);

if(is_array($arr_result1) AND is_array($arr_result2))
{
	$tmp_arr = array_unique(array_merge($arr_result1,$arr_result2));
}
if(is_array($arr_result1) AND !is_array($arr_result2))
{
	$tmp_arr = $arr_result1;
}
if(!is_array($arr_result1) AND is_array($arr_result2))
{
	$tmp_arr = $arr_result2;
}
if(sizeof($tmp_arr)>0){
	$targetPeriod = implode(",",$tmp_arr);
}

### Check any Accumulate merit record related to this period ###
$sql = "SELECT a.PeriodID, a.DateStart, a.DateEnd, b.CategoryID FROM DISCIPLINE_ACCU_PERIOD AS a INNER JOIN DISCIPLINE_ACCU_CATEGORY_PERIOD_SETTING AS b ON (a.PeriodID = b.PeriodID) WHERE a.PeriodID IN ($targetPeriod)";
$arr_result = $ldiscipline->returnArray($sql,4);
if(sizeof($arr_result)>0){
	for($i=0; $i<sizeof($arr_result); $i++){
		list($periodID, $startDate, $endDate, $categoryID) = $arr_result[$i];
		$arr_periodID[] = $periodID;
		$arrStartDate[$periodID] = $startDate;
		$arrEndDate[$periodID] = $endDate;
		$arrCategoryID[$periodID] = $categoryID;
	}
}
if(sizeof($arr_periodID)>0){
	foreach($arr_periodID as $period_id){
		$sql = "SELECT COUNT(*) 
				FROM DISCIPLINE_ACCU_RECORD 
				WHERE (RecordDate >= '$arrStartDate[$period_id]' AND RecordDate <= '$arrEndDate[$period_id]') AND CategoryID = $arrCategoryID[$period_id]";
		$tmp_result = $ldiscipline->returnVector($sql);
		$exist_rec = $exist_rec + $tmp_result[0];
	}
}else{
	header("Location: category_period.php?CategoryID=$CategoryID&PeriodType=".MISCONDUCT."&msg=13");
	exit();
}

if($exist_rec>0){
	header("Location: category_period.php?CategoryID=$CategoryID&PeriodType=".MISCONDUCT."&error=1");
	exit();
}

$result = $ldiscipline->removeCategorySpecificPeriod($PeriodID);

intranet_closedb();

if($result){
	header("Location: category_period.php?CategoryID=$CategoryID&PeriodType=".MISCONDUCT."&msg=3");
	exit();
}else{
	header("Location: category_period.php?CategoryID=$CategoryID&PeriodType=".MISCONDUCT."&msg=13");
	exit();
}
?>