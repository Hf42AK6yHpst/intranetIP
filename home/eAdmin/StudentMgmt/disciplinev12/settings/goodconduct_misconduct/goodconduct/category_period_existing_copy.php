<?
// Using: Bill

#########################################
#
#	Date:	2017-09-21	(Bill)	[2017-0710-1151-50054]
#			copy settings - Email to PIC / Class Teacher / eDiscipline Administrator
#
# 	Date:	2016-02-29	Bill	[2015-0807-1048-01073]
#			- create file
#
#########################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-GoodConduct_Misconduct-Access")){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

// Current Period Info
$PeriodID = $_POST["PeriodID"];
$StartDate = $_POST["StartDate"];
$EndDate = $_POST["EndDate"];
$TargetSemester = $_POST["TargetSemester"];

// Good Conduct
$MeritType = GOOD_CONDUCT;

// Conduct Category
$CategoryID = $_POST["CategoryID"];

// Conversion Type
$isStatic = $_POST["Scheme"]==CALCULATION_SCHEME_STATIC;

// Target Period
$TargetPeriodID = $_POST[($isStatic? "staticPeriodList" : "floatSettingsList")];

// Target Period Info
$sql = "SELECT * FROM DISCIPLINE_ACCU_PERIOD WHERE PeriodID = '$TargetPeriodID'";
$TargetPeriodInfo = $ldiscipline->returnArray($sql);

// redirect with error message if required data is invalid
if(empty($PeriodID) || empty($CategoryID) || empty($StartDate) || empty($EndDate) || empty($TargetPeriodID) || empty($TargetPeriodInfo))
{
	header("Location: category_period.php?CategoryID=$CategoryID&PeriodType=$PeriodType&msg=12");
	intranet_closedb();
	die();
}

// Target Period Setting
$TargetPeriodSettings = $ldiscipline->RETRIEVE_TARGET_CATEGORY_PERIOD_SETTING($TargetPeriodInfo[0]["DateStart"], $MeritType, $CategoryID);
$TargetPeriodScheme = $TargetPeriodSettings[0];
$TargetPeriodRules = $TargetPeriodSettings[1];

$result = array();

// Current Period Scheme & Target Period Scheme - Match? 
if(($isStatic || $TargetPeriodScheme=="static") || (!$isStatic || $TargetPeriodScheme=="specific"))
{
	$NewPeriodRange = "";
	
	// loop each settings
	for($j=0; $j<count($TargetPeriodRules); $j++)
	{
		// Upgrade Calculation
		$currentRule = $TargetPeriodRules[$j];
		
		# Static Increment
		if($TargetPeriodScheme == "static")
		{
			// Calculation Info
			$UpgradeCount = $currentRule["UpgradeCount"];
			$UpgradeDemeritType = $currentRule["UpgradeDemeritType"];
			$UpgradeDemeritCount = $currentRule["UpgradeDemeritCount"];
			$UpgradeToItemID = $currentRule["UpgradeToItemID"];
			$UpgradeDeductConductScore = $currentRule["UpgradeDeductConductScore"];
			$UpgradeDeductSubScore1 = $currentRule["UpgradeDeductSubScore1"];
			$TemplateID = $currentRule["TemplateID"];
			$SendNoticeAction = $currentRule["SendNoticeAction"];
			$SendEmailToPIC = $currentRule["SendEmailToPIC"];
			$SendEmailToClassTeacher = $currentRule["SendEmailToClassTeacher"];
			$SendEmailToAdmin = $currentRule["SendEmailToAdmin"];
			
			// Insert
			//$result = $ldiscipline->insertCategoryPeriodCalculationScheme($NewPeriodRange, $StartDate, $EndDate, $TargetSemester, CALCULATION_SCHEME_STATIC, $CategoryID, $PeriodID, $accumlativeTimes, $meritType, $meritNum, $meritItemID, $conductMark, $studyMark, GOOD_CONDUCT, "", "", "", $SelectNotice0, $sendNoticeAction);
			$result[] = $ldiscipline->insertCategoryPeriodCalculationScheme($NewPeriodRange, $StartDate, $EndDate, $TargetSemester, CALCULATION_SCHEME_STATIC, $CategoryID, $PeriodID, $UpgradeCount, $UpgradeDemeritType, $UpgradeDemeritCount, $UpgradeToItemID, $UpgradeDeductConductScore, $UpgradeDeductSubScore1, GOOD_CONDUCT, "", "", "", $TemplateID, $SendNoticeAction, $SendEmailToPIC, $SendEmailToClassTeacher, $SendEmailToAdmin);
		}
		# Floating Increment
		else if($TargetPeriodScheme == "specific")
		{
			// Calculation Info
			$NextNumLate = $currentRule["NextNumLate"];
			$ProfileMeritType = $currentRule["ProfileMeritType"];
			$ProfileMeritNum = $currentRule["ProfileMeritNum"];
			$ReasonItemID = $currentRule["ReasonItemID"];
			$ConductScore = $currentRule["ConductScore"];
			$SubScore = $currentRule["SubScore1"];
			$TemplateID = $currentRule["TemplateID"];
			$SendNoticeAction = $currentRule["SendNoticeAction"];
			$SendEmailToPIC = $currentRule["SendEmailToPIC"];
			$SendEmailToClassTeacher = $currentRule["SendEmailToClassTeacher"];
			$SendEmailToAdmin = $currentRule["SendEmailToAdmin"];
			
			// Insert
			//$result = $ldiscipline->insertCategoryPeriodCalculationScheme($NewPeriodRange, $StartDate, $EndDate, $TargetSemester, CALCULATION_SCHEME_FLOATING, $CategoryID, $PeriodID, $accumlativeTimes, $meritType, $meritNum, $meritItemID, $conductMark, $studyMark, GOOD_CONDUCT, "", "", "", $SelectNotice0, $sendNoticeAction);
			$result[] = $ldiscipline->insertCategoryPeriodCalculationScheme($NewPeriodRange, $StartDate, $EndDate, $TargetSemester, CALCULATION_SCHEME_FLOATING, $CategoryID, $PeriodID, $NextNumLate, $ProfileMeritType, $ProfileMeritNum, $ReasonItemID, $ConductScore, $SubScore, GOOD_CONDUCT, "", "", "", $TemplateID, $SendNoticeAction, $SendEmailToPIC, $SendEmailToClassTeacher, $SendEmailToAdmin);
		}
	}
}
else {
	$result[] = false;
}

if(!in_array(false, $result))
{
	if($isStatic)
	{
		header("Location: category_period.php?CategoryID=$CategoryID&PeriodType=$PeriodType&msg=1");
	}
	else
	{
		header("Location: category_period_rule.php?CategoryID=$CategoryID&PeriodType=$PeriodType&PeriodID=$PeriodID&Scheme=$Scheme&msg=1");
	}
}
else
{
	header("Location: category_period.php?CategoryID=$CategoryID&PeriodType=$PeriodType&msg=12");
}

intranet_closedb();
?>