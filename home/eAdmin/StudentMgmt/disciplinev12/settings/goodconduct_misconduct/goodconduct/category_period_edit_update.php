<?
### This page is only for static calcuation use ###

# using: Bill

#################################################
#
#	Date:	2019-05-13	(Bill)
#			Prevent SQL Injection
#
#	Date:	2017-09-21	(Bill)	[2017-0710-1151-50054]
#			update settings - Email to PIC / Class Teacher / eDiscipline Administrator
#
#################################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-GoodConduct_Misconduct-Access")){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$CurrentPage = "Settings_GoodConductMisconduct";

### Handle SQL Injection + XSS [START]
$SettingID = IntegerSafe($SettingID);
$CategoryID = IntegerSafe($CategoryID);

$accumlativeTimes = IntegerSafe($accumlativeTimes);
$meritType = IntegerSafe($meritType);
$meritNum = IntegerSafe($meritNum);
$meritItemID = IntegerSafe($meritItemID);
if($conductMark == ""){
    $conductMark = IntegerSafe($conductMark);
}
if($studyMark == ""){
    $studyMark = IntegerSafe($studyMark);
}

$SelectNotice0 = IntegerSafe($SelectNotice0);
$sendNoticeAction = IntegerSafe($sendNoticeAction);
$SendEmailToPIC = IntegerSafe($SendEmailToPIC);
$SendEmailToClassTeacher = IntegerSafe($SendEmailToClassTeacher);
$SendEmailToAdmin = IntegerSafe($SendEmailToAdmin);
### Handle SQL Injection + XSS [END]

if(!$sendNoticeAction) {
	$sql = "SELECT TemplateID FROM DISCIPLINE_ACCU_CATEGORY_PERIOD_SETTING WHERE SettingID = '$SettingID' ";
	$temp = $ldiscipline->returnVector($sql);
	$SelectNotice0 = $temp[0]; 
}

$result = $ldiscipline->editCategoryPeriod($SettingID, $accumlativeTimes, $meritType, $meritNum, $meritItemID, $conductMark, $studyMark, $SelectNotice0, $sendNoticeAction, $SendEmailToPIC, $SendEmailToClassTeacher, $SendEmailToAdmin);

intranet_closedb();

if($result){
	header("Location: category_period.php?CategoryID=$CategoryID&PeriodType=$PeriodType&msg=2");
	exit();
}
else{
	header("Location: category_period.php?CategoryID=$CategoryID&PeriodType=$PeriodType&msg=14");
	exit();
}
?>