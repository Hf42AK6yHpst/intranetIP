<?
// Using:

#############################################
#
#	Date	:	2019-05-13	(Bill)
#				Prevent SQL Injection
#
#############################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$CategoryID = IntegerSafe($CategoryID);
$PeriodID = IntegerSafe($PeriodID);

if((isset($StartDate) && !intranet_validateDate($StartDate)) || (isset($EndDate) && !intranet_validateDate($EndDate))) {
    header("Location: category_specific_period_edit.php?msg=14&CategoryID=$CategoryID&PeriodType=".CATEGORY_PERIOD_CONDITION_SPECIFY);
    exit();
}
### Handle SQL Injection + XSS [END]

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();
$lfile = new libfilesystem();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-GoodConduct_Misconduct-Access")){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$CurrentPage = "Settings_GoodConductMisconduct";

if(is_array($PeriodID)){
	$PeriodID = $PeriodID[0];
}
else{
	$PeriodID = $PeriodID;
}
if($PeriodID)
{
	$cond = " AND PeriodID NOT IN ('$PeriodID') ";	
}

$semester_mode = $lfile->file_read($intranet_root."/file/semester_mode.txt");

# check any exist record in G&M record
$sql = "SELECT DateStart, DateEnd, CategoryID FROM DISCIPLINE_ACCU_PERIOD as a left outer join DISCIPLINE_ACCU_CATEGORY_PERIOD as b ON (a.SetID = b.SetID) WHERE a.PeriodID = '$PeriodID'";
$result = $ldiscipline->returnArray($sql,3);
list($dateStart, $dateEnd, $catid) = $result[0];

if(($StartDate != $original_startDate || $EndDate != $original_endDate) || (isset($original_semester) && $original_semester != '' && $original_semester != 'NULL'))
{
	$sql = "SELECT COUNT(*) FROM DISCIPLINE_ACCU_RECORD 
			WHERE (RecordDate >= '$dateStart' AND RecordDate <= '$dateEnd') AND CategoryID = '$catid' ";
	$tmp_result = $ldiscipline->returnVector($sql);
	if($tmp_result[0] > 0){
		header("Location: category_period.php?CategoryID=$CategoryID&PeriodType=".$PeriodType."&error=2");
		exit();
	}
}

### To-do: Add checking of overlapping periods
if(isset($StartDate) && isset($EndDate))
{
	$PeriodSet = $ldiscipline->returnPeriodSetID($CategoryID);
	
	if ($meritType == 1)
	{
		$sql = "SELECT COUNT(*) FROM DISCIPLINE_ACCU_PERIOD WHERE DateStart <= '$EndDate' AND DateEnd >= '$StartDate' AND RecordType = 1 AND SetID = '$PeriodSet' ".$cond;
	}
	else
	{
		$sql = "SELECT COUNT(*) FROM DISCIPLINE_ACCU_PERIOD WHERE DateStart <= '$EndDate' AND DateEnd >= '$StartDate' AND (RecordType = -1 OR RecordType IS NULL) AND SetID = '$PeriodSet' ".$cond;
	}
}
$temp = $ldiscipline->returnVector($sql);
$overlapped = sizeof($temp) > 0 && $temp[0] > 0? 1 : 0;
if($overlapped)
{
	header("Location: category_specific_period_edit.php?error=1&CategoryID=$CategoryID&PeriodType=".CATEGORY_PERIOD_CONDITION_SPECIFY."&StartDate=$StartDate&EndDate=$EndDate");
	#header("Location: category_period_new.php?error=1&StartDate=$StartDate&EndDate=$EndDate");
	exit();
}

### Check across different school terms
$sql = "SELECT LEFT(TermEnd,10) FROM ACADEMIC_YEAR_TERM WHERE ('$StartDate' BETWEEN TermStart AND TermEnd)";
$temp = $ldiscipline->returnVector($sql);
$outBound = (sizeof($temp)==0)? 3 : ((sizeof($temp) > 0 && $temp[0] < $EndDate) ? 2 : 0);
# outBound==2: across different school terms
# outBound==3: school term not yet defined

if($outBound)
{
	header("Location: category_specific_period_edit.php?error=$outBound&CategoryID=$CategoryID&PeriodType=".CATEGORY_PERIOD_CONDITION_SPECIFY."&StartDate=$StartDate&EndDate=$EndDate");
	exit();
}

### End Checking ###
$semester = isset($semester)? $semester : intranet_htmlspecialchars($original_semester);
if($semester=="") $semester = "NULL";

$result = $ldiscipline->editCategorySpecificPeriod($PeriodID, $StartDate, $EndDate, $semester);

intranet_closedb();

if($result){
	header("Location: category_period.php?CategoryID=$CategoryID&PeriodType=".$PeriodType."&msg=2");
	exit();
}
else{
	header("Location: category_period.php?CategoryID=$CategoryID&PeriodType=".$PeriodType."&msg=14");
	exit();
}
?>