<?php
// Modifying by: henry chow

########## Change Log ###############
#
#	Date	:	2011-09-19 (Henry)
#	Detail 	:	assign $mType=-1 if it is NULL (i.e. RecordType of its category is NULL)
#
#	Date	:	2010-06-11 (Henry)
#	Detail 	:	import of Item
#
#####################################

$PATH_WRT_ROOT="../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-SETTINGS-GoodConduct_Misconduct-Access");

$sql = "select * from temp_goodconduct_misconduct_settings_item_import where UserID=$UserID";
$recordArr = $ldiscipline->returnArray($sql);

### List out the import result
$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
$x .= "<tr>";
$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_Discipline_System_ItemCode."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$eDiscipline['Setting_ItemName']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$eDiscipline['Setting_CategoryName']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$eDiscipline["Type"]."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$eDiscipline['Setting_Status']."</td>";
$x .= "</tr>";

$typeAry = array('1'=>$eDiscipline['Setting_GoodConduct'],'-1'=>$eDiscipline['Setting_Misconduct']);
$statusAry = array(1=>$eDiscipline['Setting_Status_Using'],2=>$eDiscipline['Setting_Status_NonUsing']);

for($a=0;$a<sizeof($recordArr);$a++)
{	
	$sql = "SELECT MeritType FROM DISCIPLINE_ACCU_CATEGORY WHERE CategoryID=".$recordArr[$a]['CategoryID'];
	$result = $ldiscipline->returnVector($sql);
	$mType = $result[0];
	if($mType=="") $mType = -1;

	$x .= "<tr class=\"tablebluerow".($a%2+1)."\">";
	$x .= "<td class=\"tabletext\">".($a+1)."</td>";
	$x .= "<td class=\"tabletext\">". $recordArr[$a]['ItemCode']."</td>";
	$x .= "<td class=\"tabletext\">". $recordArr[$a]['ItemName']."</td>";
	$x .= "<td class=\"tabletext\">". $ldiscipline->getGMCatNameByCatID($recordArr[$a]['CategoryID']) ."</td>";
	$x .= "<td class=\"tabletext\">". $typeAry[$mType] ."</td>";
	$x .= "<td class=\"tabletext\">". $statusAry[$recordArr[$a]['RecordStatus']] ."</td>";
	$x .= "</tr>";
	
	$accu_record_id = $ldiscipline->insertCategoryItem($recordArr[$a]['CategoryID'],$recordArr[$a]['ItemName'],$recordArr[$a]['RecordStatus'],$mType,$recordArr[$a]['ItemCode']);

	if(!$accu_record_id)
	{
		$xmsg="import_failed";		
	}
}
$x .= "</table>";


$linterface = new interface_html();

$url = ($MeritType==1) ? "goodconduct/category.php" : "misconduct/category.php";

$import_button = $linterface->GET_ACTION_BTN($button_finish, "button","self.location='$url'")."&nbsp;".
					$linterface->GET_ACTION_BTN($iDiscipline['ImportOtherRecords'], "button", "window.location='importItem.php?MeritType=$MeritType'");

# menu highlight setting
$CurrentPage = "Settings_GoodConductMisconduct";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eDiscipline['Good_Conduct_and_Misconduct']);

$PAGE_NAVIGATION[] = array($Lang['eDiscipline']['ImportItem']);

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

$linterface->LAYOUT_START();
?>

<form name="form1" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
</table>

<table id="html_body_frame" width="95%" border="0" cellspacing="0" cellpadding="5">
<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg,$xmsg2);?></td>
</tr>
<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td><?= $x ?></td>
	</tr>
	<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>


<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>