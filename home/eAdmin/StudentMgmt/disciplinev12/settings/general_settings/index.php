<?php
# using: 

########## Change Log [Start] ###########
#
#	Date	:	2017-07-07 (Anna)
#				added AuthenticationSetting
#
#	Date	:	2017-03-16	(Bill)	[2016-1207-1221-39240]
#				added Activty Score settings
#
#	Date	:	2015-08-24	Bill
#				added Redeem setting - Award must be earlier than Punishment
#
#	Date	:	2014-05-22	Carlos
#				$sys_custom['eDiscipline']['PooiToMiddleSchool'] - added misconduct to punishment item setting
#																 - added late to punishment item setting
#																 - added misconduct max score deduction setting
#
#	Date	:	2014-03-24	YatWoon
#				updated wordings [Case#E60337]
#
#	Date	:	2013-06-13	Carlos
#				$sys_custom['eDiscipline']['yy3'] - add Customized Settings > Conduct Adjustment Base Mark
#																			> Conduct Mark Exceed Limit Remark
#																			> Conduct Mark Warning Limit
#
#	Date	:	2013-05-15	YatWoon
#				add yy3 customization settings
#
#	Date	:	2011-04-19	YatWoon
#				add Max. Study Score Increment/Decrement setting
#
########## Change Log [Ebd] ###########

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# menu highlight setting
$CurrentPage = "Settings_GeneralSettings";

# build select option object
for($i=1;$i<=$ldiscipline->GeneralSettingsOption_MAX;$i++)
	$data[] = array($i, $i);
	
for($i=0;$i<=$ldiscipline->GeneralSettingsOption_MAX;$i++)
	$data3[] = array($i, $i);
	
$SemesterRatio_MAX_selection = getSelectByArray($data, "name='SemesterRatio_MAX'", $ldiscipline->SemesterRatio_MAX , 0, 1);
$ConductMarkIncrement_MAX_selection = getSelectByArray($data, "name='ConductMarkIncrement_MAX'", $ldiscipline->ConductMarkIncrement_MAX , 0, 1);
$SubScoreIncrement_MAX_selection = getSelectByArray($data, "name='SubScoreIncrement_MAX'", $ldiscipline->SubScoreIncrement_MAX , 0, 1);
$ActScoreIncrement_MAX_selection = getSelectByArray($data, "name='ActScoreIncrement_MAX'", $ldiscipline->ActScoreIncrement_MAX , 0, 1);
$AwardPunish_MAX_selection = getSelectByArray($data, "name='AwardPunish_MAX'", $ldiscipline->AwardPunish_MAX , 0, 1);
$AccumulativeTimes_MAX_selection = getSelectByArray($data, "name='AccumulativeTimes_MAX'", $ldiscipline->AccumulativeTimes_MAX , 0, 1);
$ApprovalLevel_MAX_selection = getSelectByArray($data, "name='ApprovalLevel_MAX'", $ldiscipline->ApprovalLevel_MAX , 0, 1);
$AP_Conversion_MAX_selection = getSelectByArray($data, "name='AP_Conversion_MAX'", $ldiscipline->AP_Conversion_MAX , 0, 1);
$DetentinoSession_MAX_selection = getSelectByArray($data, "name='DetentinoSession_MAX'", $ldiscipline->DetentinoSession_MAX , 0, 1);
$MaxRecordDayBeforeAllowSelection = "<input type='text' name='MaxRecordDayBeforeAllow' id='MaxRecordDayBeforeAllow' size='4' value='".(($ldiscipline->MaxRecordDayBeforeAllow!="") ? $ldiscipline->MaxRecordDayBeforeAllow : 0)."'> " . $eDiscipline['MaxRecordDayBeforeAllow_Day'];
$MaxSchoolDayToCallForActionSelection = "<input type='text' name='MaxSchoolDayToCallForAction' id='MaxSchoolDayToCallForAction' size='4' value='".$ldiscipline->MaxSchoolDayToCallForAction."'>";
$MaxDetentionDayBeforeAllowSelection = "<input type='text' name='MaxDetentionDayBeforeAllow' id='MaxDetentionDayBeforeAllow' size='4' value='".(($ldiscipline->MaxDetentionDayBeforeAllow!="") ? $ldiscipline->MaxDetentionDayBeforeAllow : 0)."'> " . $Lang['eDiscipline']['AllowToAssignStudentToPreviousSession_Day'];

if($sys_custom['eDiscipline']['yy3'])
{
	for($i=1;$i<=10;$i++)	$notice_copies_option[] = array($i, $i);
	$notice_copies_MAX_selection = getSelectByArray($notice_copies_option, "name='Notice_Copies'", $ldiscipline->Notice_Copies , 0, 1);
	
	$NoticeRemarkInput = "<input type='text' name='NoticeRemark' id='NoticeRemark' size=100 value='".(($ldiscipline->NoticeRemark!="") ? $ldiscipline->NoticeRemark : "")."'>";
	$ConductMarkRemarkInput = "<input type='text' name='ConductMarkRemark' id='ConductMarkRemark' size=100 value='".(($ldiscipline->ConductMarkRemark!="") ? $ldiscipline->ConductMarkRemark : "")."'>";
	$ConductMarkWarningLimitInput = '<input type="text" name="ConductMarkWarningLimit" id="ConductMarkWarningLimit" size="5" onchange="forceInputNumber(this);" value="'.(($ldiscipline->ConductMarkWarningLimit!='') ? $ldiscipline->ConductMarkWarningLimit : '').'">';
	
	$ConductAdjustmentBaseMarkInput = '<input type="text" name="ConductAdjustmentBaseMark" id="ConductAdjustmentBaseMark" size="5" onchange="forceInputNumber(this);" value="'.(($ldiscipline->ConductAdjustmentBaseMark!='') ? $ldiscipline->ConductAdjustmentBaseMark : '').'">';

	$ConductMarkExceedLimitRemarkInput = "<input type='text' name='ConductMarkExceedLimitRemark' id='ConductMarkExceedLimitRemark' size=100 value='".(($ldiscipline->ConductMarkExceedLimitRemark!="") ? $ldiscipline->ConductMarkExceedLimitRemark : "")."'>";
}

if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
	include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui.php");
	$ldiscipline_ui = new libdisciplinev12_ui();
	
	if($ldiscipline->MisconductToPunishmentItem != ''){
		$MisconductToPunishmentItemInfo = $ldiscipline->getAwardPunishItemInfo($ldiscipline->MisconductToPunishmentItem);
		$MisconductToPunishmentItemText = $MisconductToPunishmentItemInfo['ItemName'];
	}else{
		$MisconductToPunishmentItemText = $Lang['General']['EmptySymbol'];
	}
	$MisconductToPunishmentItemSelection = $ldiscipline_ui->Get_All_AP_Item_Display(array($ldiscipline->MisconductToPunishmentItem), "MisconductToPunishmentItem", "MisconductToPunishmentItem", "", false, "--".$Lang['General']['PleaseSelect']."--", -1);
	
	if($ldiscipline->LateToPunishmentItem != ''){
		$LateToPunishmentItemInfo = $ldiscipline->getAwardPunishItemInfo($ldiscipline->LateToPunishmentItem);
		$LateToPunishmentItemText = $LateToPunishmentItemInfo['ItemName'];
	}else{
		$LateToPunishmentItemText = $Lang['General']['EmptySymbol'];
	}
	$LateToPunishmentItemSelection = $ldiscipline_ui->Get_All_AP_Item_Display(array($ldiscipline->LateToPunishmentItem), "LateToPunishmentItem", "LateToPunishmentItem", "", false, "--".$Lang['General']['PleaseSelect']."--", -1);
	
	
	for($i=0;$i<=$ldiscipline->GeneralSettingsOption_MAX;$i++)
		$misconduct_max_score_deduction_data[] = array($i, $i);
	
	$MisconductMaxScoreDeductionText = $ldiscipline->MisconductMaxScoreDeduction != ''? $ldiscipline->MisconductMaxScoreDeduction : 3;
	$MisconductMaxScoreDeductionSelection = getSelectByArray($misconduct_max_score_deduction_data, ' id="MisconductMaxScoreDeduction" name="MisconductMaxScoreDeduction" ', $MisconductMaxScoreDeductionText , 0, 1);
}

$data2 = array();
$data2[] = array(0.25, 0.25);
$data2[] = array(0.5, 0.5);
$data2[] = array(1, 1);
$AP_Interval_Value_selection = getSelectByArray($data2, "name='AP_Interval_Value'", ($ldiscipline->AP_Interval_Value ? $ldiscipline->AP_Interval_Value : 1) , 0, 1);

# Left menu 
$TAGS_OBJ[] = array($Lang['eDiscipline']['SysProperties']['SysProperties'], "");

$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
<!--
function EditInfo()
{
	$('#EditBtn').attr('style', 'visibility: hidden'); 
	$('.Edit_Hide').attr('style', 'display: none');
	$('.Edit_Show').attr('style', '');
	
	$('#UpdateBtn').attr('style', '');
	$('#cancelbtn').attr('style', '');
}		

function ResetInfo()
{
	document.form1.reset();
	$('#EditBtn').attr('style', '');
	$('.Edit_Hide').attr('style', '');
	$('.Edit_Show').attr('style', 'display: none');
	
	$('#UpdateBtn').attr('style', 'display: none');
	$('#cancelbtn').attr('style', 'display: none');
}


var ClickID = '';
var callback_show_remark = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('ref_list').innerHTML = tmp_str;
	    DisplayPosition();
	}
}


function show_remark(type,click)
{
	ClickID = click;
	document.getElementById('task').value = type;
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	var path = "ajax_remark.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_show_remark);
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function DisplayPosition(){
	
	document.getElementById('ref_list').style.left = (parseInt(getPosition(document.getElementById(ClickID),'offsetLeft'))-10)+"px";
	document.getElementById('ref_list').style.top = (parseInt(getPosition(document.getElementById(ClickID),'offsetTop'))+20)+"px";
	document.getElementById('ref_list').style.visibility='visible';

}

function js_Hide_Detail_Layer() {
	MM_showHideLayers('ImportantNoteLayer','','hide');
}

function forceInputNumber(obj)
{
	var num = obj.value.Trim();
	if(isNaN(num) || !parseFloat(num)) {
		obj.value = '';
	}else{
		obj.value = parseFloat(num);
	}
}

function forceInputPositiveInt(obj)
{
	var num = obj.value.Trim();
	var num_int = parseInt(num);
	if(isNaN(num) || !num || num_int < 0) {
		obj.value = '';
	}else{
		obj.value = num_int;
	}
}
//-->
</script>

<form name="form1" method="get" action="edit_update.php">

<div id="ref_list" style='position:absolute; height:150px; z-index:1; visibility: hidden;'></div>

<div class="table_board">
	<div class="table_row_tool row_content_tool">
		<?= $linterface->GET_SMALL_BTN($button_edit, "button","javascript:EditInfo();","EditBtn")?>
	</div>
	<p class="spacer"></p>
	
	
	<div class="form_sub_title_v30"><em>- <span class="field_title"> <?=$Lang['eDiscipline']['SysProperties']['AccessRights']?> </span>-</em>
		<p class="spacer"></p>
	</div>
	<table class="form_table_v30">
		<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eDiscipline']['OnlyAdminSelectPIC']?> </td>
				<td>
				<span class="Edit_Hide"><?=($ldiscipline->OnlyAdminSelectPIC  ? $i_general_yes:$i_general_no)?></span>
				<span class="Edit_Show" style="display:none">
					<input type="radio" name="OnlyAdminSelectPIC" value="1" <?=$ldiscipline->OnlyAdminSelectPIC ? "checked":"" ?> id="OnlyAdminSelectPIC1"> <label for="OnlyAdminSelectPIC1"><?=$i_general_yes?></label> 
					<input type="radio" name="OnlyAdminSelectPIC" value="0" <?=$ldiscipline->OnlyAdminSelectPIC ? "":"checked" ?> id="OnlyAdminSelectPIC0"> <label for="OnlyAdminSelectPIC0"><?=$i_general_no?></label>
				</span></td>
			</tr>
		<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eDiscipline']['PIC_SelectionDefaultOwn']?> </td>
				<td nowrap>
				<span class="Edit_Hide"><?=($ldiscipline->PIC_SelectionDefaultOwn  ? $i_general_yes:$i_general_no)?></span>
				<span class="Edit_Show" style="display:none">
					<input type="radio" name="PIC_SelectionDefaultOwn" value="1" <?=$ldiscipline->PIC_SelectionDefaultOwn ? "checked":"" ?> id="PIC_SelectionDefaultOwn1"> <label for="PIC_SelectionDefaultOwn1"><?=$i_general_yes?></label> 
					<input type="radio" name="PIC_SelectionDefaultOwn" value="0" <?=$ldiscipline->PIC_SelectionDefaultOwn ? "":"checked" ?> id="PIC_SelectionDefaultOwn0"> <label for="PIC_SelectionDefaultOwn0"><?=$i_general_no?></label>
				</span></td>
			</tr>	
		<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eDiscipline']['DisplayIconInPortal']?> </td>
				<td>
				<span class="Edit_Hide"><?=($ldiscipline->DisplayIconInPortal  ? $i_general_yes:$i_general_no)?></span>
				<span class="Edit_Show" style="display:none">
					<input type="radio" name="DisplayIconInPortal" value="1" <?=$ldiscipline->DisplayIconInPortal ? "checked":"" ?> id="DisplayIconInPortal1"> <label for="DisplayIconInPortal1"><?=$i_general_yes?></label> 
					<input type="radio" name="DisplayIconInPortal" value="0" <?=$ldiscipline->DisplayIconInPortal ? "":"checked" ?> id="DisplayIconInPortal0"> <label for="DisplayIconInPortal0"><?=$i_general_no?></label>
				</span></td>
			</tr>
		<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eDiscipline']['AllowToAssignStudentToAccessRightGroup']?> <a class="tablelink" href=javascript:show_remark('studentToAccessRightGroup','sarg_click')><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_remark.gif" align="absmiddle" width="20" height="20" border="0" alt="<?=$i_UserRemark?>"></a><span id="sarg_click">&nbsp;</span></td>
				<td>
				<span class="Edit_Hide"><?=($ldiscipline->AllowToAssignStudentToAccessRightGroup  ? $i_general_yes:$i_general_no)?></span>
				<span class="Edit_Show" style="display:none">
					<input type="radio" name="AllowToAssignStudentToAccessRightGroup" value="1" <?=$ldiscipline->AllowToAssignStudentToAccessRightGroup ? "checked":"" ?> id="AllowToAssignStudentToAccessRightGroup1"> <label for="AllowToAssignStudentToAccessRightGroup1"><?=$i_general_yes?></label> 
					<input type="radio" name="AllowToAssignStudentToAccessRightGroup" value="0" <?=$ldiscipline->AllowToAssignStudentToAccessRightGroup ? "":"checked" ?> id="AllowToAssignStudentToAccessRightGroup0"> <label for="AllowToAssignStudentToAccessRightGroup0"><?=$i_general_no?></label>
				</span></td>
			</tr>
	</table>
	
	<div class="form_sub_title_v30"><em>- <span class="field_title"> <?=$Lang['eDiscipline']['SysProperties']['General']?> </span>-</em><p class="spacer"></p>
	</div>
	<table class="form_table_v30">
		<tr valign='top'>
				<td class="field_title" nowrap><?=$eDiscipline['SemesterRatio_MAX']?> </td>
				<td>
				<span class="Edit_Hide"><?=$ldiscipline->SemesterRatio_MAX ? $ldiscipline->SemesterRatio_MAX : "---" ?></span>
				<span class="Edit_Show" style="display:none"><?=$SemesterRatio_MAX_selection?></span></td>
			</tr>
		<tr valign='top'>
				<td class="field_title" nowrap><?=$eDiscipline['Activate_NewLeaf']?> </td>
				<td>
				<span class="Edit_Hide"><?=($ldiscipline->use_newleaf ? $i_general_yes:$i_general_no)?></span>
				<span class="Edit_Show" style="display:none">
					<input type="radio" name="use_newleaf" value="1" <?=$ldiscipline->use_newleaf ? "checked":"" ?> id="use_newleaf1"> <label for="use_newleaf1"><?=$i_general_yes?></label> 
					<input type="radio" name="use_newleaf" value="0" <?=$ldiscipline->use_newleaf ? "":"checked" ?> id="use_newleaf0"> <label for="use_newleaf0"><?=$i_general_no?></label>
				</span></td>
			</tr>
		<? if($sys_custom['mansung_disciplinev12_conduct_grade']) { ?>
		<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eDiscipline']['DisplayOtherTeacherGrade']?> </td>
				<td>
				<span class="Edit_Hide"><?=($ldiscipline->displayOtherTeacherGradeInConductGrade  ? $i_general_yes:$i_general_no)?></span>
				<span class="Edit_Show" style="display:none">
					<input type="radio" name="displayOtherTeacherGradeInConductGrade" value="1" <?=$ldiscipline->displayOtherTeacherGradeInConductGrade ? "checked":"" ?> id="displayOtherTeacherGradeInConductGrade1"> <label for="RejdisplayOtherTeacherGradeInConductGrade1ctRecordEmailNotification1"><?=$i_general_yes?></label> 
					<input type="radio" name="displayOtherTeacherGradeInConductGrade" value="0" <?=$ldiscipline->displayOtherTeacherGradeInConductGrade ? "":"checked" ?> id="displayOtherTeacherGradeInConductGrade0"> <label for="displayOtherTeacherGradeInConductGrade0"><?=$i_general_no?></label>
				</span></td>
			</tr>	
		<? } ?>	
		<tr valign='top'>
				<td class="field_title" nowrap><?=$eDiscipline['MaxRecordDayBeforeAllow']?> </td>
				<td>
				<span class="Edit_Hide"><?=$ldiscipline->MaxRecordDayBeforeAllow ? $ldiscipline->MaxRecordDayBeforeAllow  ." " . $eDiscipline['MaxRecordDayBeforeAllow_Day'] : "---" ?></span>
				<span class="Edit_Show" style="display:none"><?=$MaxRecordDayBeforeAllowSelection?></span></td>
			</tr>	
		<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eDiscipline']['SendEmailtoClassTeacherWhenStudentTriggerWarningReminderPoint']?> </td>
				<td>
				<span class="Edit_Hide"><?=($ldiscipline->WarningReminderPointEmailNotification  ? $i_general_yes:$i_general_no)?></span>
				<span class="Edit_Show" style="display:none">
					<input type="radio" name="WarningReminderPointEmailNotification" value="1" <?=$ldiscipline->WarningReminderPointEmailNotification ? "checked":"" ?> id="WarningReminderPointEmailNotification1"> <label for="WarningReminderPointEmailNotification1"><?=$i_general_yes?></label> 
					<input type="radio" name="WarningReminderPointEmailNotification" value="0" <?=$ldiscipline->WarningReminderPointEmailNotification ? "":"checked" ?> id="WarningReminderPointEmailNotification0"> <label for="WarningReminderPointEmailNotification0"><?=$i_general_no?></label>
				</span></td>
			</tr>		
		<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eDiscipline']['SendEmailtoDisciplineAdminWhenStudentTriggerWarningReminderPoint']?> </td>
				<td>
				<span class="Edit_Hide"><?=($ldiscipline->WarningReminderPointEmailNotificationToDisciplineAdmin  ? $i_general_yes:$i_general_no)?></span>
				<span class="Edit_Show" style="display:none">
					<input type="radio" name="WarningReminderPointEmailNotificationToDisciplineAdmin" value="1" <?=$ldiscipline->WarningReminderPointEmailNotificationToDisciplineAdmin ? "checked":"" ?> id="WarningReminderPointEmailNotificationToDisciplineAdmin1"> <label for="WarningReminderPointEmailNotificationToDisciplineAdmin1"><?=$i_general_yes?></label> 
					<input type="radio" name="WarningReminderPointEmailNotificationToDisciplineAdmin" value="0" <?=$ldiscipline->WarningReminderPointEmailNotificationToDisciplineAdmin ? "":"checked" ?> id="WarningReminderPointEmailNotificationToDisciplineAdmin0"> <label for="WarningReminderPointEmailNotificationToDisciplineAdmin0"><?=$i_general_no?></label>
				</span></td>
			</tr>
				
		<? if($ldiscipline->UseSubScore) {?>
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eDiscipline']['SubScoreWarningEmail']?> </td>
				<td>
				<span class="Edit_Hide"><?=($ldiscipline->SubScoreWarningReminderPointEmailNotification  ? $i_general_yes:$i_general_no)?></span>
				<span class="Edit_Show" style="display:none">
					<input type="radio" name="SubScoreWarningReminderPointEmailNotification" value="1" <?=$ldiscipline->SubScoreWarningReminderPointEmailNotification ? "checked":"" ?> id="SubScoreWarningReminderPointEmailNotification1"> <label for="SubScoreWarningReminderPointEmailNotification1"><?=$i_general_yes?></label> 
					<input type="radio" name="SubScoreWarningReminderPointEmailNotification" value="0" <?=$ldiscipline->SubScoreWarningReminderPointEmailNotification ? "":"checked" ?> id="SubScoreWarningReminderPointEmailNotification0"> <label for="SubScoreWarningReminderPointEmailNotification0"><?=$i_general_no?></label>
				</span></td>
			</tr>
		<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eDiscipline']['SubScoreWarningEmailToDisciplineAdmin']?> </td>
				<td>
				<span class="Edit_Hide"><?=($ldiscipline->SubScoreWarningReminderPointEmailNotificationToDisciplineAdmin  ? $i_general_yes:$i_general_no)?></span>
				<span class="Edit_Show" style="display:none">
					<input type="radio" name="SubScoreWarningReminderPointEmailNotificationToDisciplineAdmin" value="1" <?=$ldiscipline->SubScoreWarningReminderPointEmailNotificationToDisciplineAdmin ? "checked":"" ?> id="SubScoreWarningReminderPointEmailNotificationToDisciplineAdmin1"> <label for="SubScoreWarningReminderPointEmailNotificationToDisciplineAdmin1"><?=$i_general_yes?></label> 
					<input type="radio" name="SubScoreWarningReminderPointEmailNotificationToDisciplineAdmin" value="0" <?=$ldiscipline->SubScoreWarningReminderPointEmailNotificationToDisciplineAdmin ? "":"checked" ?> id="SubScoreWarningReminderPointEmailNotificationToDisciplineAdmin0"> <label for="SubScoreWarningReminderPointEmailNotificationToDisciplineAdmin0"><?=$i_general_no?></label>
				</span></td>
			</tr>
		<? } ?>
		
		<? if($ldiscipline->UseActScore) { ?>
		<tr valign='top'>
			<td class="field_title" nowrap><?=$Lang['eDiscipline']['ActivityScoreWarningEmail']?> </td>
			<td>
			<span class="Edit_Hide"><?=($ldiscipline->ActScoreWarningReminderPointEmailNotification  ? $i_general_yes:$i_general_no)?></span>
			<span class="Edit_Show" style="display:none">
				<input type="radio" name="ActScoreWarningReminderPointEmailNotification" value="1" <?=$ldiscipline->ActScoreWarningReminderPointEmailNotification ? "checked":"" ?> id="ActScoreWarningReminderPointEmailNotification1"> <label for="ActScoreWarningReminderPointEmailNotification1"><?=$i_general_yes?></label> 
				<input type="radio" name="ActScoreWarningReminderPointEmailNotification" value="0" <?=$ldiscipline->ActScoreWarningReminderPointEmailNotification ? "":"checked" ?> id="ActScoreWarningReminderPointEmailNotification0"> <label for="ActScoreWarningReminderPointEmailNotification0"><?=$i_general_no?></label>
			</span></td>
		</tr>		
		<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eDiscipline']['ActivityScoreWarningEmailToDisciplineAdmin']?> </td>
				<td>
				<span class="Edit_Hide"><?=($ldiscipline->ActScoreWarningReminderPointEmailNotificationToDisciplineAdmin  ? $i_general_yes:$i_general_no)?></span>
				<span class="Edit_Show" style="display:none">
					<input type="radio" name="ActScoreWarningReminderPointEmailNotificationToDisciplineAdmin" value="1" <?=$ldiscipline->ActScoreWarningReminderPointEmailNotificationToDisciplineAdmin ? "checked":"" ?> id="ActScoreWarningReminderPointEmailNotificationToDisciplineAdmin1"> <label for="ActScoreWarningReminderPointEmailNotificationToDisciplineAdmin1"><?=$i_general_yes?></label> 
					<input type="radio" name="ActScoreWarningReminderPointEmailNotificationToDisciplineAdmin" value="0" <?=$ldiscipline->ActScoreWarningReminderPointEmailNotificationToDisciplineAdmin ? "":"checked" ?> id="ActScoreWarningReminderPointEmailNotificationToDisciplineAdmin0"> <label for="ActScoreWarningReminderPointEmailNotificationToDisciplineAdmin0"><?=$i_general_no?></label>
				</span></td>
			</tr>		
		<? } ?>
			
		<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eDiscipline']['NotAllowSameItemInSameDay']?> </td>
				<td>
				<span class="Edit_Hide"><?=($ldiscipline->NotAllowSameItemInSameDay  ? $i_general_yes:$i_general_no)?></span>
				<span class="Edit_Show" style="display:none">
					<input type="radio" name="NotAllowSameItemInSameDay" value="1" <?=$ldiscipline->NotAllowSameItemInSameDay ? "checked":"" ?> id="NotAllowSameItemInSameDay1"> <label for="NotAllowSameItemInSameDay1"><?=$i_general_yes?></label> 
					<input type="radio" name="NotAllowSameItemInSameDay" value="0" <?=$ldiscipline->NotAllowSameItemInSameDay ? "":"checked" ?> id="NotAllowSameItemInSameDay0"> <label for="NotAllowSameItemInSameDay0"><?=$i_general_no?></label>
				</span></td>
			</tr>			
		<tr>
	
		<tr valign='top'>
			<td  class="field_title" nowrap><?= str_replace("<!--ModuleName-->", $ip20TopMenu['eDisciplinev12'], $Lang['Security']['AuthenticationSetting']) ?></td>
			<td>
				<span class="Edit_Hide"><?=($ldiscipline->AuthenticationSetting? $i_general_yes:$i_general_no)?></span>
				<span class="Edit_Show" style="display:none">
					<input type="radio" name="AuthenticationSetting" value="1" <?=$ldiscipline->AuthenticationSetting ? "checked":"" ?> id="AuthenticationSetting1"> <label for="AuthenticationSetting1"><?=$i_general_yes?></label> 
					<input type="radio" name="AuthenticationSetting" value="0" <?=$ldiscipline->AuthenticationSetting ? "":"checked" ?> id="AuthenticationSetting0"> <label for="AuthenticationSetting0"><?=$i_general_no?></label>
				</span>
			</td>
		</tr>
	</table>
	
	<div class="form_sub_title_v30"><em>- <span class="field_title"> <?=$Lang['eDiscipline']['SysProperties']['DisciplineRecord']?> </span>-</em>
		<p class="spacer"></p>
	</div>
	<table class="form_table_v30">
		<tr valign='top'>
				<td class="field_title" nowrap><?=$eDiscipline['ConductMarkIncrement_MAX']?> </td>
				<td>
				<span class="Edit_Hide"><?=$ldiscipline->ConductMarkIncrement_MAX ? $ldiscipline->ConductMarkIncrement_MAX : "---" ?></span>
				<span class="Edit_Show" style="display:none"><?=$ConductMarkIncrement_MAX_selection?></span></td>
			</tr>
		<? if($sys_custom['UseSubScore1']) {?>
		<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eDiscipline']['StudyScoreIncrement_MAX']?> </td>
				<td>
				<span class="Edit_Hide"><?=$ldiscipline->SubScoreIncrement_MAX ? $ldiscipline->SubScoreIncrement_MAX : "---" ?></span>
				<span class="Edit_Show" style="display:none"><?=$SubScoreIncrement_MAX_selection?></span></td>
			</tr>
		<? } ?>
		<? if($ldiscipline->UseActScore) { ?>
		<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eDiscipline']['ActivityScoreIncrement_MAX']?> </td>
				<td>
				<span class="Edit_Hide"><?=$ldiscipline->ActScoreIncrement_MAX ? $ldiscipline->ActScoreIncrement_MAX : "---" ?></span>
				<span class="Edit_Show" style="display:none"><?=$ActScoreIncrement_MAX_selection?></span></td>
			</tr>
		<? } ?>
		<tr valign='top'>
				<td class="field_title" nowrap><?=$eDiscipline['AwardPunish_MAX']?> </td>
				<td>
				<span class="Edit_Hide"><?=$ldiscipline->AwardPunish_MAX ? $ldiscipline->AwardPunish_MAX : "---" ?></span>
				<span class="Edit_Show" style="display:none"><?=$AwardPunish_MAX_selection?></span></td>
			</tr>
		<tr valign='top'>
				<td class="field_title" nowrap><?=$eDiscipline['ApprovalLevel_MAX']?> </td>
				<td>
				<span class="Edit_Hide"><?=$ldiscipline->ApprovalLevel_MAX ? $ldiscipline->ApprovalLevel_MAX : "---" ?></span>
				<span class="Edit_Show" style="display:none"><?=$ApprovalLevel_MAX_selection?></span></td>
			</tr>
		<tr valign='top'>
				<td class="field_title" nowrap><?=$eDiscipline['UseSubject']?> </td>
				<td>
				<span class="Edit_Hide"><?=($ldiscipline->use_subject ? $i_general_yes:$i_general_no) ?></span>
				<span class="Edit_Show" style="display:none">
					<input type="radio" name="use_subject" value="1" <?=$ldiscipline->use_subject ? "checked":"" ?> id="use_subject1"> <label for="use_subject1"><?=$i_general_yes?></label> 
					<input type="radio" name="use_subject" value="0" <?=$ldiscipline->use_subject ? "":"checked" ?> id="use_subject0"> <label for="use_subject0"><?=$i_general_no?></label>
				</span></td>
			</tr>
		<tr valign='top'>
				<td class="field_title" nowrap><?=$eDiscipline['AP_Conversion_MAX']?> </td>
				<td>
				<span class="Edit_Hide"><?=$ldiscipline->AP_Conversion_MAX ? $ldiscipline->AP_Conversion_MAX : "---" ?></span>
				<span class="Edit_Show" style="display:none"><?=$AP_Conversion_MAX_selection?></span></td>
			</tr>
		<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eDiscipline']['DisplayConductMarkInAP']?> </td>
				<td>
				<span class="Edit_Hide"><?=($ldiscipline->Display_ConductMarkInAP ? $i_general_yes:$i_general_no)?></span>
				<span class="Edit_Show" style="display:none">
					<input type="radio" name="Display_ConductMarkInAP" value="1" <?=$ldiscipline->Display_ConductMarkInAP ? "checked":"" ?> id="Display_ConductMarkInAP1"> <label for="Display_ConductMarkInAP1"><?=$i_general_yes?></label> 
					<input type="radio" name="Display_ConductMarkInAP" value="0" <?=$ldiscipline->Display_ConductMarkInAP ? "":"checked" ?> id="Display_ConductMarkInAP0"> <label for="Display_ConductMarkInAP0"><?=$i_general_no?></label>
				</span></td>
			</tr>
		<tr valign='top'>
				<td class="field_title" nowrap><?=$eDiscipline['AP_Interval_Value']?> </td>
				<td>
				<span class="Edit_Hide"><?=($ldiscipline->AP_Interval_Value ? $ldiscipline->AP_Interval_Value : "1")?></span>
				<span class="Edit_Show" style="display:none"><?=$AP_Interval_Value_selection?></span></td>
			</tr>	
		<tr valign='top'>
				<td class="field_title" nowrap><?=$eDiscipline['AccumulativeTimes_MAX']?> </td>
				<td>
				<span class="Edit_Hide"><?=$ldiscipline->AccumulativeTimes_MAX ? $ldiscipline->AccumulativeTimes_MAX : "---" ?></span>
				<span class="Edit_Show" style="display:none"><?=$AccumulativeTimes_MAX_selection?></span></td>
			</tr>
			
		<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eDiscipline']['AutoApproveAPRecord']?> </td>
				<td>
				<span class="Edit_Hide"><?=($ldiscipline->AutoApproveAPRecord  ? $i_general_yes:$i_general_no)?></span>
				<span class="Edit_Show" style="display:none">
					<input type="radio" name="AutoApproveAPRecord" value="1" <?=$ldiscipline->AutoApproveAPRecord ? "checked":"" ?> id="AutoApproveAPRecord1"> <label for="AutoApproveAPRecord1"><?=$i_general_yes?></label> 
					<input type="radio" name="AutoApproveAPRecord" value="0" <?=$ldiscipline->AutoApproveAPRecord ? "":"checked" ?> id="AutoApproveAPRecord0"> <label for="AutoApproveAPRecord0"><?=$i_general_no?></label>
				</span></td>
			</tr>
					
		<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eDiscipline']['AutoReleaseAGM2AP']?> </td>
				<td>
				<span class="Edit_Hide"><?=($ldiscipline->AutoReleaseAGM2AP ? $i_general_yes:$i_general_no)?></span>
				<span class="Edit_Show" style="display:none">
					<input type="radio" name="AutoReleaseAGM2AP" value="1" <?=$ldiscipline->AutoReleaseAGM2AP ? "checked":"" ?> id="AutoReleaseAGM2AP1"> <label for="AutoReleaseAGM2AP1"><?=$i_general_yes?></label> 
					<input type="radio" name="AutoReleaseAGM2AP" value="0" <?=$ldiscipline->AutoReleaseAGM2AP ? "":"checked" ?> id="AutoReleaseAGM2AP0"> <label for="AutoReleaseAGM2AP0"><?=$i_general_no?></label>
				</span></td>
			</tr>		
		<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eDiscipline']['AutoReleaseForAPRecord']?> </td>
				<td>
				<span class="Edit_Hide"><?=($ldiscipline->AutoReleaseForAPRecord ? $i_general_yes:$i_general_no)?></span>
				<span class="Edit_Show" style="display:none">
					<input type="radio" name="AutoReleaseForAPRecord" value="1" <?=$ldiscipline->AutoReleaseForAPRecord ? "checked":"" ?> id="AutoReleaseForAPRecord1"> <label for="AutoReleaseForAPRecord1"><?=$i_general_yes?></label> 
					<input type="radio" name="AutoReleaseForAPRecord" value="0" <?=$ldiscipline->AutoReleaseForAPRecord ? "":"checked" ?> id="AutoReleaseForAPRecord0"> <label for="AutoReleaseForAPRecord0"><?=$i_general_no?></label>
				</span></td>
			</tr>	
		<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eDiscipline']['RejectRecordEmailNotification']?> </td>
				<td>
				<span class="Edit_Hide"><?=($ldiscipline->RejectRecordEmailNotification  ? $i_general_yes:$i_general_no)?></span>
				<span class="Edit_Show" style="display:none">
					<input type="radio" name="RejectRecordEmailNotification" value="1" <?=$ldiscipline->RejectRecordEmailNotification ? "checked":"" ?> id="RejectRecordEmailNotification1"> <label for="RejectRecordEmailNotification1"><?=$i_general_yes?></label> 
					<input type="radio" name="RejectRecordEmailNotification" value="0" <?=$ldiscipline->RejectRecordEmailNotification ? "":"checked" ?> id="RejectRecordEmailNotification0"> <label for="RejectRecordEmailNotification0"><?=$i_general_no?></label>
				</span></td>
			</tr>		
		<? if($sys_custom['wscss_disciplinev12_ap_prompt_action']) { ?>
		<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eDiscipline']['CallForActionAfterEffectiveDate']?> </td>
				<td>
				<span class="Edit_Hide"><?=($ldiscipline->MaxSchoolDayToCallForAction ? $ldiscipline->MaxSchoolDayToCallForAction : "---")?></span>
				<span class="Edit_Show" style="display:none"><?=$MaxSchoolDayToCallForActionSelection;?></span></td>
			</tr>		
		<? } ?>	
		<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eDiscipline']['APSetting_BatchEmptyInput']?> </td>
				<td>
				<span class="Edit_Hide"><?=($ldiscipline->NotAllowEmptyInputInAP  ? $i_general_yes:$i_general_no)?></span>
				<span class="Edit_Show" style="display:none">
					<input type="radio" name="NotAllowEmptyInputInAP" value="1" <?=$ldiscipline->NotAllowEmptyInputInAP ? "checked":"" ?> id="NotAllowEmptyInputInAP1"> <label for="NotAllowEmptyInputInAP1"><?=$i_general_yes?></label> 
					<input type="radio" name="NotAllowEmptyInputInAP" value="0" <?=$ldiscipline->NotAllowEmptyInputInAP ? "":"checked" ?> id="NotAllowEmptyInputInAP0"> <label for="NotAllowEmptyInputInAP0"><?=$i_general_no?></label>
				</span></td>
			</tr>		
	</table>
	

	
	
	
	

	
	<div class="form_sub_title_v30"><em>- <span class="field_title"> <?=$Lang['eDiscipline']['SysProperties']['Detention']?> </span>-</em>
		<p class="spacer"></p>
	</div>
	<table class="form_table_v30">
		<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eDiscipline']['DetentinoSession_MAX']?> </td>
				<td>
				<span class="Edit_Hide"><?=$ldiscipline->DetentinoSession_MAX ? $ldiscipline->DetentinoSession_MAX : "---" ?></span>
				<span class="Edit_Show" style="display:none"><?=$DetentinoSession_MAX_selection?></span></td>
			</tr>
			<tr valign='top'>
				<td class="field_title" nowrap><?=$eDiscipline['Detention_Sat']?> </td>
				<td>
				<span class="Edit_Hide"><?=($ldiscipline->Detention_Sat ? $i_general_yes:$i_general_no)?></span>
				<span class="Edit_Show" style="display:none">
					<input type="radio" name="Detention_Sat" value="1" <?=$ldiscipline->Detention_Sat ? "checked":"" ?> id="Detention_Sat1"> <label for="Detention_Sat1"><?=$i_general_yes?></label> 
					<input type="radio" name="Detention_Sat" value="0" <?=$ldiscipline->Detention_Sat ? "":"checked" ?> id="Detention_Sat0"> <label for="Detention_Sat0"><?=$i_general_no?></label>
				</span></td>
			</tr>
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eDiscipline']['AllowToAssignStudentToPreviousSession']?> </td>
				<td>
				<span class="Edit_Hide"><?=$ldiscipline->MaxDetentionDayBeforeAllow ? $ldiscipline->MaxDetentionDayBeforeAllow . " " . $Lang['eDiscipline']['AllowToAssignStudentToPreviousSession_Day'] : "---" ?></span>
				<span class="Edit_Show" style="display:none"><?=$MaxDetentionDayBeforeAllowSelection?></span></td>
			</tr>
	</table>
	
	
	
	
	<div class="form_sub_title_v30"><em>- <span class="field_title"> <?=$i_Discipline_System_Award_Punishment_Redeem?> </span>-</em>
		<p class="spacer"></p>
	</div> 
	<table class="form_table_v30">
		<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eDiscipline']['AwardMustEarlierThanPunishment']?> </td>
				<td>
				<span class="Edit_Hide"><?=($ldiscipline->NoLimitOnAwardPunishmentDate ? $Lang['eDiscipline']['NoLimit'] : $i_general_yes)?></span>
				<span class="Edit_Show" style="display:none">
					<input type="radio" name="NoLimitOnAwardPunishmentDate" value="0" <?=$ldiscipline->NoLimitOnAwardPunishmentDate ? "":"checked" ?> id="NoLimitOnAwardPunishmentDate0"> <label for="NoLimitOnAwardPunishmentDate0"><?=$i_general_yes?></label>
					<input type="radio" name="NoLimitOnAwardPunishmentDate" value="1" <?=$ldiscipline->NoLimitOnAwardPunishmentDate ? "checked":"" ?> id="NoLimitOnAwardPunishmentDate1"> <label for="NoLimitOnAwardPunishmentDate1"><?=$Lang['eDiscipline']['NoLimit']?></label> 
				</span></td>
			</tr>
	</table>
	
	
	
	
	<div class="form_sub_title_v30"><em>- <span class="field_title"> <?=$Lang['eDiscipline']['SysProperties']['Report']?> </span>-</em>
		<p class="spacer"></p>
	</div> 
	<table class="form_table_v30">
		<tr valign='top'>
				<td class="field_title" nowrap><?=$eDiscipline['Hidden_ConductMark']?> </td>
				<td>
				<span class="Edit_Hide"><?=($ldiscipline->Hidden_ConductMark ? $i_general_yes:$i_general_no)?></span>
				<span class="Edit_Show" style="display:none">
					<input type="radio" name="Hidden_ConductMark" value="1" <?=$ldiscipline->Hidden_ConductMark ? "checked":"" ?> id="Hidden_ConductMark1"> <label for="Hidden_ConductMark1"><?=$i_general_yes?></label> 
					<input type="radio" name="Hidden_ConductMark" value="0" <?=$ldiscipline->Hidden_ConductMark ? "":"checked" ?> id="Hidden_ConductMark0"> <label for="Hidden_ConductMark0"><?=$i_general_no?></label>
				</span></td>
			</tr>
	</table>

	<? ############ yy3 [start] ############### ?>
	<? if($sys_custom['eDiscipline']['yy3']) {?>
	<div class="form_sub_title_v30"><em>- <span class="field_title"> <?=$Lang['General']['CustomizedSettings']?> </span>-</em>
		<p class="spacer"></p>
	</div> 
	<table class="form_table_v30">
		<tr valign='top'>
			<td class="field_title" nowrap><?=$Lang['eDiscipline']['ConductMarkRemark']?> </td>
			<td>
				<span class="Edit_Hide"><?=$ldiscipline->ConductMarkRemark ? $ldiscipline->ConductMarkRemark : "" ?></span>
				<span class="Edit_Show" style="display:none"><?=$ConductMarkRemarkInput?></span>
			</td>
		</tr>
		
		<tr valign='top'>
			<td class="field_title" nowrap><?=$Lang['eDiscipline']['NoticeCopies']?> </td>
			<td>
				<span class="Edit_Hide"><?=$ldiscipline->Notice_Copies ? $ldiscipline->Notice_Copies : "1" ?></span>
				<span class="Edit_Show" style="display:none"><?=$notice_copies_MAX_selection?></span>
			</td>
		</tr>
		
		<tr valign='top'>
			<td class="field_title" nowrap><?=$Lang['eDiscipline']['NoticeRemark']?> </td>
			<td>
				<span class="Edit_Hide"><?=$ldiscipline->NoticeRemark ? $ldiscipline->NoticeRemark : "" ?></span>
				<span class="Edit_Show" style="display:none"><?=$NoticeRemarkInput?></span>
			</td>
		</tr>
		
		<tr valign='top'>
			<td class="field_title" nowrap><?=$Lang['eDiscipline']['ConductAdjustmentBaseMark']?></td>
			<td>
				<span class="Edit_Hide"><?=$ldiscipline->ConductAdjustmentBaseMark ? $ldiscipline->ConductAdjustmentBaseMark : $Lang['General']['EmptySymbol'] ?></span>
				<span class="Edit_Show" style="display:none"><?=$ConductAdjustmentBaseMarkInput?></span>
			</td>
		</tr>
		
		<tr valign='top'>
			<td class="field_title" nowrap><?=$Lang['eDiscipline']['ConductMarkWarningLimit']?></td>
			<td>
				<span class="Edit_Hide"><?=$ldiscipline->ConductMarkWarningLimit ? $ldiscipline->ConductMarkWarningLimit : $Lang['General']['EmptySymbol'] ?></span>
				<span class="Edit_Show" style="display:none"><?=$ConductMarkWarningLimitInput?></span>
			</td>
		</tr>
		
		<tr valign='top'>
			<td class="field_title" nowrap><?=$Lang['eDiscipline']['ConductMarkExceedLimitRemark']?> </td>
			<td>
				<span class="Edit_Hide"><?=$ldiscipline->ConductMarkExceedLimitRemark ? $ldiscipline->ConductMarkExceedLimitRemark : "" ?></span>
				<span class="Edit_Show" style="display:none"><?=$ConductMarkExceedLimitRemarkInput?></span>
			</td>
		</tr>
			
	</table>
	<? } ?>
	<? ############ yy3 [end] ############### ?>
	
	<?php if($sys_custom['eDiscipline']['PooiToMiddleSchool']){ ?>
	<div class="form_sub_title_v30"><em>- <span class="field_title"> <?=$Lang['General']['CustomizedSettings']?> </span>-</em>
		<p class="spacer"></p>
	</div> 
	<table class="form_table_v30">
		<tr valign='top'>
			<td class="field_title" nowrap><?=$Lang['eDiscipline']['MisconductToPunishmentItemWhenReachSixMarks']?> </td>
			<td>
				<span class="Edit_Hide"><?=$MisconductToPunishmentItemText?></span>
				<span class="Edit_Show" style="display:none"><?=$MisconductToPunishmentItemSelection?></span>
			</td>
		</tr>
		<tr valign='top'>
			<td class="field_title" nowrap><?=$Lang['eDiscipline']['LateToPunishmentItemWhenReachSixMarks']?> </td>
			<td>
				<span class="Edit_Hide"><?=$LateToPunishmentItemText?></span>
				<span class="Edit_Show" style="display:none"><?=$LateToPunishmentItemSelection?></span>
			</td>
		</tr>
		<tr valign='top'>
			<td class="field_title" nowrap><?=$Lang['eDiscipline']['MisconductMaxScoreDeduction']?> </td>
			<td>
				<span class="Edit_Hide"><?=$MisconductMaxScoreDeductionText?></span>
				<span class="Edit_Show" style="display:none"><?=$MisconductMaxScoreDeductionSelection?></span>
			</td>
		</tr>
	</table>
	<?php } ?>
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($button_update, "submit","","UpdateBtn", "style='display: none'") ?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "ResetInfo();","cancelbtn", "style='display: none'") ?>
		<p class="spacer"></p>
	</div>
</div>
<input type="hidden" name="task" id="task" value="">
<input type="hidden" name="ClickID" id="ClickID" value="">
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
