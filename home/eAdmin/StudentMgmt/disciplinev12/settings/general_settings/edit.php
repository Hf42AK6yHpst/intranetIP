<?php
# using:  ### this file is no longer used ###

########## Change Log [Start] ###########
#	
#	Date	: 	2010-08-24 (Henry Chow)
#				Add settings "Display eDis icon in portal page"
#	
#	Date	: 	2010-05-07 YatWoon
#				Add settings "Auto-release Award/Punishment Records generated from accumulative Good Conduct/Misconduct Records"
#				Add settings "PIC selection default own"
#
#	Date	: 	2010-04-14 YatWoon
#				Add settings "Only allow admin to select record's PIC"
#
# - 2010-02-24 (Henry)
#	Add option "Not allow same student with same 'Award & Punishment' or 'Good Conduct & Misconduct' item in a same day"
#
# - 2009-12-30 YatWoon
#	Add option "Send email to class teacher when student trigger warning reminder point"
########## Change Log [Ebd] ###########

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# menu highlight setting
$CurrentPage = "Settings_GeneralSettings";

$PAGE_NAVIGATION[] = array($button_edit);

# Left menu 
$TAGS_OBJ[] = array($eDiscipline['General_Settings'], "");

$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

# build select option object
for($i=1;$i<=$ldiscipline->GeneralSettingsOption_MAX;$i++)
	$data[] = array($i, $i);
	
for($i=0;$i<=$ldiscipline->GeneralSettingsOption_MAX;$i++)
	$data3[] = array($i, $i);

$SemesterRatio_MAX_selection = getSelectByArray($data, "name='SemesterRatio_MAX'", $ldiscipline->SemesterRatio_MAX , 0, 1);
$ConductMarkIncrement_MAX_selection = getSelectByArray($data, "name='ConductMarkIncrement_MAX'", $ldiscipline->ConductMarkIncrement_MAX , 0, 1);
$AwardPunish_MAX_selection = getSelectByArray($data, "name='AwardPunish_MAX'", $ldiscipline->AwardPunish_MAX , 0, 1);
$AccumulativeTimes_MAX_selection = getSelectByArray($data, "name='AccumulativeTimes_MAX'", $ldiscipline->AccumulativeTimes_MAX , 0, 1);
$ApprovalLevel_MAX_selection = getSelectByArray($data, "name='ApprovalLevel_MAX'", $ldiscipline->ApprovalLevel_MAX , 0, 1);
$AP_Conversion_MAX_selection = getSelectByArray($data, "name='AP_Conversion_MAX'", $ldiscipline->AP_Conversion_MAX , 0, 1);
$DetentinoSession_MAX_selection = getSelectByArray($data, "name='DetentinoSession_MAX'", $ldiscipline->DetentinoSession_MAX , 0, 1);

$data2 = array();
$data2[] = array(0.25, 0.25);
$data2[] = array(0.5, 0.5);
$data2[] = array(1, 1);
$AP_Interval_Value_selection = getSelectByArray($data2, "name='AP_Interval_Value'", ($ldiscipline->AP_Interval_Value ? $ldiscipline->AP_Interval_Value : 1) , 0, 1);

//$MaxRecordDayBeforeAllowSelection = getSelectByArray($data3, "name='MaxRecordDayBeforeAllow'", ($ldiscipline->MaxRecordDayBeforeAllow ? $ldiscipline->MaxRecordDayBeforeAllow: 0), 0, 1);
$MaxRecordDayBeforeAllowSelection = "<input type='text' name='MaxRecordDayBeforeAllow' id='MaxRecordDayBeforeAllow' size='4' value='".(($ldiscipline->MaxRecordDayBeforeAllow!="") ? $ldiscipline->MaxRecordDayBeforeAllow : 0)."'>";

$MaxSchoolDayToCallForActionSelection = "<input type='text' name='MaxSchoolDayToCallForAction' id='MaxSchoolDayToCallForAction' size='4' value='".$ldiscipline->MaxSchoolDayToCallForAction."'>";

?>

<script language="javascript">
<!--
function checkform() {
	if(isNaN(document.getElementById('MaxRecordDayBeforeAllow').value) || document.getElementById('MaxRecordDayBeforeAllow').value=='' || document.getElementById('MaxRecordDayBeforeAllow').value<0 || (Math.floor(document.getElementById('MaxRecordDayBeforeAllow').value) != document.getElementById('MaxRecordDayBeforeAllow').value)) {
		alert("<?=$i_CampusMail_New_AddressBook_Alert_NeedInteger?>");	
		document.getElementById('MaxRecordDayBeforeAllow').select();
		return false;
	}
	if(isNaN(document.getElementById('MaxSchoolDayToCallForAction').value) || document.getElementById('MaxSchoolDayToCallForAction').value=='' || document.getElementById('MaxSchoolDayToCallForAction').value<=0 || (Math.floor(document.getElementById('MaxSchoolDayToCallForAction').value) != document.getElementById('MaxSchoolDayToCallForAction').value)) {
		alert("<?=$i_CampusMail_New_AddressBook_Alert_NeedInteger?>");	
		document.getElementById('MaxSchoolDayToCallForAction').select();
		return false;
	}
}
//-->
</script>
<br />   
<form name="form1" method="get" action="edit_update.php" onSubmit="return checkform()">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$eDiscipline['SemesterRatio_MAX']?></td>
			<td class='tabletext'><?=$SemesterRatio_MAX_selection?></td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$eDiscipline['ConductMarkIncrement_MAX']?></td>
			<td class='tabletext'><?=$ConductMarkIncrement_MAX_selection?></td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$eDiscipline['AwardPunish_MAX']?></td>
			<td class='tabletext'><?=$AwardPunish_MAX_selection?></td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$eDiscipline['AccumulativeTimes_MAX']?></td>
			<td class='tabletext'><?=$AccumulativeTimes_MAX_selection?></td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$eDiscipline['ApprovalLevel_MAX']?></td>
			<td class='tabletext'><?=$ApprovalLevel_MAX_selection?></td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$Lang['eDiscipline']['DetentinoSession_MAX']?></td>
			<td class='tabletext'><?=$DetentinoSession_MAX_selection?></td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$eDiscipline['UseSubject']?></td>
			<td class='tabletext'>
			<input type="radio" name="use_subject" value="1" <?=$ldiscipline->use_subject ? "checked":"" ?> id="use_subject1"> <label for="use_subject1"><?=$i_general_yes?></label> 
			<input type="radio" name="use_subject" value="0" <?=$ldiscipline->use_subject ? "":"checked" ?> id="use_subject0"> <label for="use_subject0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$eDiscipline['AP_Conversion_MAX']?></td>
			<td class='tabletext'><?=$AP_Conversion_MAX_selection?></td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$eDiscipline['Activate_NewLeaf']?></td>
			<td class='tabletext'>
			<input type="radio" name="use_newleaf" value="1" <?=$ldiscipline->use_newleaf ? "checked":"" ?> id="use_newleaf1"> <label for="use_newleaf1"><?=$i_general_yes?></label> 
			<input type="radio" name="use_newleaf" value="0" <?=$ldiscipline->use_newleaf ? "":"checked" ?> id="use_newleaf0"> <label for="use_newleaf0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$eDiscipline['Detention_Sat']?></td>
			<td class='tabletext'>
			<input type="radio" name="Detention_Sat" value="1" <?=$ldiscipline->Detention_Sat ? "checked":"" ?> id="Detention_Sat1"> <label for="Detention_Sat1"><?=$i_general_yes?></label> 
			<input type="radio" name="Detention_Sat" value="0" <?=$ldiscipline->Detention_Sat ? "":"checked" ?> id="Detention_Sat0"> <label for="Detention_Sat0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$eDiscipline['Hidden_ConductMark']?></td>
			<td class='tabletext'>
			<input type="radio" name="Hidden_ConductMark" value="1" <?=$ldiscipline->Hidden_ConductMark ? "checked":"" ?> id="Hidden_ConductMark1"> <label for="Hidden_ConductMark1"><?=$i_general_yes?></label> 
			<input type="radio" name="Hidden_ConductMark" value="0" <?=$ldiscipline->Hidden_ConductMark ? "":"checked" ?> id="Hidden_ConductMark0"> <label for="Hidden_ConductMark0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$Lang['eDiscipline']['DisplayConductMarkInAP']?></td>
			<td class='tabletext'>
			<input type="radio" name="Display_ConductMarkInAP" value="1" <?=$ldiscipline->Display_ConductMarkInAP ? "checked":"" ?> id="Display_ConductMarkInAP1"> <label for="Display_ConductMarkInAP1"><?=$i_general_yes?></label> 
			<input type="radio" name="Display_ConductMarkInAP" value="0" <?=$ldiscipline->Display_ConductMarkInAP ? "":"checked" ?> id="Display_ConductMarkInAP0"> <label for="Display_ConductMarkInAP0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$eDiscipline['AP_Interval_Value'];?></td>
			<td class='tabletext'><?=$AP_Interval_Value_selection?></td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$eDiscipline['MaxRecordDayBeforeAllow'];?></td>
			<td class='tabletext'><?=$MaxRecordDayBeforeAllowSelection;?></td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$Lang['eDiscipline']['AutoReleaseForAPRecord']?></td>
			<td class='tabletext'>
			<input type="radio" name="AutoReleaseForAPRecord" value="1" <?=$ldiscipline->AutoReleaseForAPRecord ? "checked":"" ?> id="AutoReleaseForAPRecord1"> <label for="AutoReleaseForAPRecord1"><?=$i_general_yes?></label> 
			<input type="radio" name="AutoReleaseForAPRecord" value="0" <?=$ldiscipline->AutoReleaseForAPRecord ? "":"checked" ?> id="AutoReleaseForAPRecord0"> <label for="AutoReleaseForAPRecord0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$Lang['eDiscipline']['AutoReleaseAGM2AP']?></td>
			<td class='tabletext'>
			<input type="radio" name="AutoReleaseAGM2AP" value="1" <?=$ldiscipline->AutoReleaseAGM2AP ? "checked":"" ?> id="AutoReleaseAGM2AP1"> <label for="AutoReleaseAGM2AP1"><?=$i_general_yes?></label> 
			<input type="radio" name="AutoReleaseAGM2AP" value="0" <?=$ldiscipline->AutoReleaseAGM2AP ? "":"checked" ?> id="AutoReleaseAGM2AP0"> <label for="AutoReleaseAGM2AP0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$Lang['eDiscipline']['RejectRecordEmailNotification']?></td>
			<td class='tabletext'>
			<input type="radio" name="RejectRecordEmailNotification" value="1" <?=$ldiscipline->RejectRecordEmailNotification ? "checked":"" ?> id="RejectRecordEmailNotification1"> <label for="RejectRecordEmailNotification1"><?=$i_general_yes?></label> 
			<input type="radio" name="RejectRecordEmailNotification" value="0" <?=$ldiscipline->RejectRecordEmailNotification ? "":"checked" ?> id="RejectRecordEmailNotification0"> <label for="RejectRecordEmailNotification0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<? if($sys_custom['mansung_disciplinev12_conduct_grade']) { ?>
<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$Lang['eDiscipline']['DisplayOtherTeacherGrade']?></td>
			<td class='tabletext'>
			<input type="radio" name="displayOtherTeacherGradeInConductGrade" value="1" <?=$ldiscipline->displayOtherTeacherGradeInConductGrade ? "checked":"" ?> id="displayOtherTeacherGradeInConductGrade1"> <label for="RejdisplayOtherTeacherGradeInConductGrade1ctRecordEmailNotification1"><?=$i_general_yes?></label> 
			<input type="radio" name="displayOtherTeacherGradeInConductGrade" value="0" <?=$ldiscipline->displayOtherTeacherGradeInConductGrade ? "":"checked" ?> id="displayOtherTeacherGradeInConductGrade0"> <label for="displayOtherTeacherGradeInConductGrade0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>
<? } ?>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$Lang['eDiscipline']['SendEmailtoClassTeacherWhenStudentTriggerWarningReminderPoint']?></td>
			<td class='tabletext'>
			<input type="radio" name="WarningReminderPointEmailNotification" value="1" <?=$ldiscipline->WarningReminderPointEmailNotification ? "checked":"" ?> id="WarningReminderPointEmailNotification1"> <label for="WarningReminderPointEmailNotification1"><?=$i_general_yes?></label> 
			<input type="radio" name="WarningReminderPointEmailNotification" value="0" <?=$ldiscipline->WarningReminderPointEmailNotification ? "":"checked" ?> id="WarningReminderPointEmailNotification0"> <label for="WarningReminderPointEmailNotification0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$Lang['eDiscipline']['SendEmailtoDisciplineAdminWhenStudentTriggerWarningReminderPoint']?></td>
			<td class='tabletext'>
			<input type="radio" name="WarningReminderPointEmailNotificationToDisciplineAdmin" value="1" <?=$ldiscipline->WarningReminderPointEmailNotificationToDisciplineAdmin ? "checked":"" ?> id="WarningReminderPointEmailNotificationToDisciplineAdmin1"> <label for="WarningReminderPointEmailNotificationToDisciplineAdmin1"><?=$i_general_yes?></label> 
			<input type="radio" name="WarningReminderPointEmailNotificationToDisciplineAdmin" value="0" <?=$ldiscipline->WarningReminderPointEmailNotificationToDisciplineAdmin ? "":"checked" ?> id="WarningReminderPointEmailNotificationToDisciplineAdmin0"> <label for="WarningReminderPointEmailNotificationToDisciplineAdmin0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<? if($sys_custom['wscss_disciplinev12_ap_prompt_action']) { ?>
<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$Lang['eDiscipline']['CallForActionAfterEffectiveDate'];?></td>
			<td class='tabletext'><?=$MaxSchoolDayToCallForActionSelection;?></td>
		</tr>
		</table>
	</td>
</tr>
<? } ?>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$Lang['eDiscipline']['NotAllowSameItemInSameDay']?></td>
			<td class='tabletext'>
			<input type="radio" name="NotAllowSameItemInSameDay" value="1" <?=$ldiscipline->NotAllowSameItemInSameDay ? "checked":"" ?> id="NotAllowSameItemInSameDay1"> <label for="NotAllowSameItemInSameDay1"><?=$i_general_yes?></label> 
			<input type="radio" name="NotAllowSameItemInSameDay" value="0" <?=$ldiscipline->NotAllowSameItemInSameDay ? "":"checked" ?> id="NotAllowSameItemInSameDay0"> <label for="NotAllowSameItemInSameDay0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$Lang['eDiscipline']['APSetting_BatchEmptyInput']?></td>
			<td class='tabletext'>
			<input type="radio" name="NotAllowEmptyInputInAP" value="1" <?=$ldiscipline->NotAllowEmptyInputInAP ? "checked":"" ?> id="NotAllowEmptyInputInAP1"> <label for="NotAllowEmptyInputInAP1"><?=$i_general_yes?></label> 
			<input type="radio" name="NotAllowEmptyInputInAP" value="0" <?=$ldiscipline->NotAllowEmptyInputInAP ? "":"checked" ?> id="NotAllowEmptyInputInAP0"> <label for="NotAllowEmptyInputInAP0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$Lang['eDiscipline']['OnlyAdminSelectPIC']?></td>
			<td class='tabletext'>
			<input type="radio" name="OnlyAdminSelectPIC" value="1" <?=$ldiscipline->OnlyAdminSelectPIC ? "checked":"" ?> id="OnlyAdminSelectPIC1"> <label for="OnlyAdminSelectPIC1"><?=$i_general_yes?></label> 
			<input type="radio" name="OnlyAdminSelectPIC" value="0" <?=$ldiscipline->OnlyAdminSelectPIC ? "":"checked" ?> id="OnlyAdminSelectPIC0"> <label for="OnlyAdminSelectPIC0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$Lang['eDiscipline']['PIC_SelectionDefaultOwn']?></td>
			<td class='tabletext'>
			<input type="radio" name="PIC_SelectionDefaultOwn" value="1" <?=$ldiscipline->PIC_SelectionDefaultOwn ? "checked":"" ?> id="PIC_SelectionDefaultOwn1"> <label for="PIC_SelectionDefaultOwn1"><?=$i_general_yes?></label> 
			<input type="radio" name="PIC_SelectionDefaultOwn" value="0" <?=$ldiscipline->PIC_SelectionDefaultOwn ? "":"checked" ?> id="PIC_SelectionDefaultOwn0"> <label for="PIC_SelectionDefaultOwn0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$Lang['eDiscipline']['DisplayIconInPortal']?></td>
			<td class='tabletext'>
			<input type="radio" name="DisplayIconInPortal" value="1" <?=$ldiscipline->DisplayIconInPortal ? "checked":"" ?> id="DisplayIconInPortal1"> <label for="DisplayIconInPortal1"><?=$i_general_yes?></label> 
			<input type="radio" name="DisplayIconInPortal" value="0" <?=$ldiscipline->DisplayIconInPortal ? "":"checked" ?> id="DisplayIconInPortal0"> <label for="DisplayIconInPortal0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2">        
        <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
        <tr>
        	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        </tr>
        <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit") ?>
                <?= $linterface->GET_ACTION_BTN($button_reset, "reset") ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back()","cancelbtn") ?>
			</td>
		</tr>
        </table>                                
	</td>
</tr>

</table>                        
<br />

</form>


<?
intranet_closedb();
print $linterface->FOCUS_ON_LOAD("form1.SemesterRatio_MAX");
$linterface->LAYOUT_STOP();
?>