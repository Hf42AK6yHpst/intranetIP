<?

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$ldiscipline_ui = new libdisciplinev12_ui();

echo $ldiscipline_ui->Get_Settings_CurriculumPool_UI();
intranet_closedb();

?>