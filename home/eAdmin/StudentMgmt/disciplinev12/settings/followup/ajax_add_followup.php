<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$li = new libdb();

# check duplicate
$sql = "select count(*) from DISCIPLINE_FOLLOWUP_ACTION where Title='$fname'";
$c = $li->returnVector($sql);

if(!$c[0])
{
	# found out the max sequence
	$sql = "select max(Sequence) from DISCIPLINE_FOLLOWUP_ACTION";
	$result = $li->returnVector($sql);
	$s = $result[0]+1;
	
	$sql = "insert into DISCIPLINE_FOLLOWUP_ACTION (Title, Sequence, RecordStatus, DateInput, InputBy) values ('$fname', $s, 1, now(), $UserID)";
	$li->db_db_query($sql);
	$x = "1";
} 
else 
{
	$x = "-1";
}

intranet_closedb();

echo $x;
?>