<?php
// using : 

#############################################
#
#	Date	:	2019-05-13	(Bill)
#				Prevent SQL Injection
#
#	Date	:	2017-03-17	(Bill)	[2016-1207-1221-39240]
#				Fixed: Re-calculate the study score base not work (from year to term)
#
#############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Conduct_Mark-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

## Get Current Year ID 
$curr_year_id = Get_Current_Academic_Year_ID();

## Update Study Score Calculation Method
$ldiscipline->updateStudyScoreCalculationMethod($CalculationMethod);

## Update Base Mark
$baseMark = IntegerSafe($baseMark);
$pre_baseMark = $ldiscipline->editStudyScoreRule('baseMark', $baseMark);
$difference = $baseMark - $pre_baseMark;
if($pre_baseMark != '' && $difference != 0)
{
	# Update Student Study Score Balance & Change Log
	$ldiscipline->updateStudyScoreBalanceFrBaseMark($difference);
}

## Re-calculate Study Score Base
if($CalculationMethod == 0)
{
	## re-calcuate : from year to term (not accumlate)
	$sql = "SELECT DISTINCT StudentID FROM DISCIPLINE_STUDENT_SUBSCORE_BALANCE WHERE AcademicYearID = '$curr_year_id' AND (IsAnnual IS NULL OR IsAnnual != 1)";
	$student = $ldiscipline->returnArray($sql,1);
	
	$sql = "SELECT YearTermID FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '$curr_year_id'";
	$arrYearTerm = $ldiscipline->returnVector($sql);
	for($a=0, $a_max=count($arrYearTerm); $a<$a_max; $a++)
	{
		$thisYearTermID = $arrYearTerm[$a];
        
		for($i=0;$i<sizeof($student);$i++)
		{
			list($student_id) = $student[$i];
			
			//$sql = "SELECT YearTermID, SUM(SubScore1Change) FROM DISCIPLINE_MERIT_RECORD WHERE StudentID = '$student_id' AND AcademicYearID = '$curr_year_id' GROUP BY YearTermID";
			$sql = "SELECT YearTermID, SUM(SubScore1Change) FROM DISCIPLINE_MERIT_RECORD WHERE StudentID = '$student_id' AND AcademicYearID = '$curr_year_id' AND YearTermID = '$thisYearTermID' GROUP BY YearTermID";
			$temp_result = $ldiscipline->returnArray($sql,2);
			if(sizeof($temp_result) > 0)
			{
				for($j=0; $j<sizeof($temp_result); $j++) {
					list($year_term_id, $study_score_change) = $temp_result[$j];
					$updated_study_score = $baseMark + $study_score_change;
					
					$sql = "UPDATE DISCIPLINE_STUDENT_SUBSCORE_BALANCE SET SubScore = '$updated_study_score' WHERE StudentID = '$student_id' AND AcademicYearID = '$curr_year_id' AND YearTermID = '$year_term_id' AND (IsAnnual IS NULL OR IsAnnual != 1)";
					$ldiscipline->db_db_query($sql);
					//if($student_id==1666) debug_pr($sql);
				}
			}
			else
			{
				$sql = "DELETE FROM DISCIPLINE_STUDENT_SUBSCORE_BALANCE WHERE AcademicYearID = '$curr_year_id' AND YearTermID = '$thisYearTermID' AND StudentID = '$student_id'";
				$ldiscipline->db_db_query($sql);
				//if($student_id==1666) echo $sql.'<br>'; 
				
				$sql = "INSERT INTO DISCIPLINE_STUDENT_SUBSCORE_BALANCE SET AcademicYearID = '$curr_year_id', YearTermID = '$thisYearTermID', StudentID = '$student_id', SubScore = '$baseMark', DateInput = NOW(), DateModified = NOW()";
				$ldiscipline->db_db_query($sql);
				//if($student_id==1666) echo $sql.'<br>';	
			}
		}
	}
} 
else
{
	## re-calcuate : from term to year (accumlate)
	$sql = "SELECT YearTermID FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '$curr_year_id'";
	$arrYearTerm = $ldiscipline->returnArray($sql,1);
	
	$sql = "SELECT DISTINCT StudentID FROM DISCIPLINE_STUDENT_SUBSCORE_BALANCE WHERE AcademicYearID = '$curr_year_id' AND (IsAnnual IS NULL OR IsAnnual != 1)";
	$student = $ldiscipline->returnArray($sql,1);
	for($i=0;$i<sizeof($student);$i++)
	{
		list($student_id) = $student[$i];
		$temp_baseMark = $baseMark;
		
		for($k=0; $k<sizeof($arrYearTerm); $k++)
		{
			list($target_year_term_id) = $arrYearTerm[$k];
            
			$sql = "SELECT YearTermID, SUM(SubScore1Change) FROM DISCIPLINE_MERIT_RECORD WHERE StudentID = '$student_id' AND AcademicYearID = '$curr_year_id' AND YearTermID = '$target_year_term_id' AND RecordStatus = 1 AND ReleaseStatus = 1 GROUP BY YearTermID";
			$temp_result = $ldiscipline->returnArray($sql,2);
			if(sizeof($temp_result) > 0)
			{
				for($j=0; $j<sizeof($temp_result); $j++) {
					list($year_term_id, $study_score_change) = $temp_result[$j];
					$updated_study_score = $temp_baseMark + $study_score_change;
	
					$sql = "UPDATE DISCIPLINE_STUDENT_SUBSCORE_BALANCE SET SubScore = '$updated_study_score' WHERE StudentID = '$student_id' AND AcademicYearID = '$curr_year_id' AND YearTermID = '$year_term_id' AND (IsAnnual IS NULL OR IsAnnual != 1)";
					$ldiscipline->db_db_query($sql);
					//if($student_id==1666) echo $sql.'<br><br>';
					
					$temp_baseMark = $updated_study_score;
				}
			}
			else
			{
				$sql = "DELETE FROM DISCIPLINE_STUDENT_SUBSCORE_BALANCE WHERE AcademicYearID = '$curr_year_id' AND YearTermID = '$target_year_term_id' AND StudentID = '$student_id' ";
				$ldiscipline->db_db_query($sql);
				
				$sql = "INSERT INTO DISCIPLINE_STUDENT_SUBSCORE_BALANCE SET AcademicYearID = '$curr_year_id', YearTermID = '$target_year_term_id', StudentID = '$student_id', SubScore = '$temp_baseMark', DateInput = NOW(), DateModified = NOW()";
				$ldiscipline->db_db_query($sql);
				//if($student_id==1666) debug_pr($sql);
			}
		}
	}
}

header("Location: base_mark.php?msg=1");

intranet_closedb();
?>