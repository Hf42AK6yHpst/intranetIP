<?php

#############################################
#
#	Date:	2017-01-20	Bill	[2015-0120-1200-33164]
#			Update Tab wording
#
#	Date:	2012-11-09	YatWoon
#			Add "Semester Ratio" tab
#
#############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Conduct_Mark-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# menu highlight setting
$CurrentPage = "Settings_StudyScore";

$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Warning_Rules, "warning_rule.php", 0);
$TAGS_OBJ[] = array($Lang['eDiscipline']['SubScoreBaseMark'], "base_mark.php", 0);
$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Grading_Scheme, "grading_scheme.php", 1);
$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Semester_Ratio, "semester_ratio.php", 0);

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

?>
<script language="javascript">
<!--

function checkForm(form1) {
	if(form1.studyScore.value=="")	 {
		alert("<?=$i_alert_pleasefillin.$Lang['eDiscipline']['StudyScoreLowerBoundary'] ?>");	
		document.form1.studyScore.focus();
		return false;
	}
	if(isNaN(form1.studyScore.value)) {
		alert("<?=$i_ServiceMgmt_System_Warning_Numeric?>");	
		document.form1.studyScore.focus();
		return false;
	}
	
	if(form1.calculatedGrade.value=="")	 {
		alert("<?=$i_alert_pleasefillin ?><?=$i_Discipline_System_Conduct_ConductGradeRule_CalculatedGrade?>");	
		document.form1.calculatedGrade.focus();
		return false;
	}
	
	return true;
}
function IsAlphabetic(sText)
{
	var ValidChars = "abcdefghijklmnopqrstuvwxyz-+";
	var Char;

	sText = sText.toLowerCase();
	for (i = 0; i < sText.length; i++)
    {
        Char = sText.charAt(i);
        if (ValidChars.indexOf(Char) == -1)
        {
            return false;
        }
    }
    return true;

}
function isInteger(sText)
{
	var ValidChars = "0123456789";
	var Char;

	sText = sText.toLowerCase();
	for (i = 0; i < sText.length; i++)
    {
        Char = sText.charAt(i);
        if (ValidChars.indexOf(Char) == -1)
        {
	        return false;
        }
    }
    return true;

}
//-->
</script>
<br/>
<form name="form1" action="grading_scheme_add_update.php" method="POST" ONSUBMIT="return checkForm(this)">
<table id="html_body_frame" width="90%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td width="100%" align="right" colspan="2"><?= $linterface->GET_SYS_MSG('',$xmsg2) ?></td>
</tr>
<tr>
	<td colspan="2"><?= $ldiscipline->showWarningMsg($i_Discipline_System_Conduct_Instruction, $Lang['eDiscipline']['GenerateGradeOfStudyScoreInstruction']) ?><br /></td>
</tr>
<tr>
	<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
		<span class="tabletext"><?=$i_Discipline_System_Conduct_ConductGradeRule_MinScore?></span> <span class="tabletextrequire">*</span>
	</td>
	<td>
		<input type="text" name="studyScore" id="studyScore" value="" class="textboxnum">
	</td>
</tr>
<tr>
	<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
		<span class="tabletext"><?=$i_Discipline_System_Conduct_ConductGradeRule_CalculatedGrade?></span> <span class="tabletextrequire">*</span>
	</td>
	<td>
		<input type="text" name="calculatedGrade" id="calculatedGrade" value="" class="textboxnum">
	</td>
</tr>
</table>

<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field ?></td></tr>
	<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<tr>
		<td align="center">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='grading_scheme.php'")?>
		</td>
	</tr>
</table>

<input type=hidden name=previousScore value="<?= $score ?>">
</form>

<?
echo $linterface->FOCUS_ON_LOAD("form1.conductScore"); 

$linterface->LAYOUT_STOP();
intranet_closedb();

?>
