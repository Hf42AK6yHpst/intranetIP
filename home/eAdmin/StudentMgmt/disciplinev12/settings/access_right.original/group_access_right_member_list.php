<?php 

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_right_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
	$ck_right_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_right_page_number!="")
{
	$pageNo = $ck_right_page_number;
}

if ($ck_right_page_order!=$order && $order!="")
{
	setcookie("ck_right_page_order", $order, 0, "", "", 0);
	$ck_right_page_order = $order;
} else if (!isset($order) && $ck_right_page_order!="")
{
	$order = $ck_right_page_order;
}

if ($ck_right_page_field!=$field && $field!="")
{
	setcookie("ck_right_page_field", $field, 0, "", "", 0);
	$ck_right_page_field = $field;
} else if (!isset($field) && $ck_right_page_field!="")
{
	$field = $ck_right_page_field;
}

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}


$GroupID = is_array($GroupID)? $GroupID[0] : $GroupID;

$temp = $ldiscipline->getAccessRightGroupInfo($GroupID);
list($GroupTitle) = $temp[0];
$GroupTitle = str_replace('<','&lt;',$GroupTitle);

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

$GroupType = 'A';
$name_field = getNameFieldByLang('b.');

$sql = "SELECT 
			$name_field as showName,
			LEFT(a.DateInput,10) as DateInput,
			CONCAT('<input type=\'checkbox\' name=\'userID[]\' value=\'', a.UserID ,'\'>')
		FROM ACCESS_RIGHT_GROUP_MEMBER as a 
		LEFT OUTER JOIN INTRANET_USER as b ON (a.UserID=b.UserID)
		WHERE GroupID=$GroupID
		";
                
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("showName", "DateInput");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0);
$li->wrap_array = array(0,0);
$li->IsColOff = 2;

$pos = 0;
$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='60%' >".$li->column($pos++, $i_Discipline_System_Access_Right_Member_Name)."</td>\n";
$li->column_list .= "<td width='40%'>".$li->column($pos++, $i_Discipline_System_Access_Right_Added_Date)."</td>\n";
$li->column_list .= "<td width='1'>".$li->check("userID[]")."</td>\n";

$functionbar = $linterface->GET_BTN($button_remove, "button", "javascript:removeCat(document.form1,'userID[]','group_access_right_member_remove.php?GroupID=$GroupID')");

$toolbar = $linterface->GET_LNK_NEW("group_access_right_member_add.php?GroupID=$GroupID",$i_Discipline_System_Group_Right_Navigation_New_Member);


# Top menu highlight setting
$CurrentPage = "Settings_AccessRight";

$TAGS_OBJ[] = array($i_Discipline_System_Discipline_Members_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/group_access_right.php", 1);
//$TAGS_OBJ[] = array($i_Discipline_System_Teacher_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/teacher_access_right.php", 0);
//$TAGS_OBJ[] = array($i_Discipline_System_Student_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/student_access_right.php", 0);

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

# no navigation in this page
$PAGE_NAVIGATION[] = array($i_Discipline_System_Group_Right_Navigation_Group_List, "group_access_right.php");
$PAGE_NAVIGATION[] = array($GroupTitle, "group_access_right_view.php?GroupID=$GroupID");
?>
<script language="javascript">
<!--
function removeCat(obj,element,page){
		var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(alertConfirmRemove)){
                obj.action=page;
                obj.method="post";
                obj.submit();
                }
        }
}
//-->
</script>
<br />
<form name="form1" method="post" action="">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="70%"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="70%"><?= $toolbar ?></td>
								<td width="30%" align="right"><?= $linterface->GET_SYS_MSG($xmsg) ?></td>
							</tr>
						</table>
					<td>
				</tr>
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td height="28" align="right" valign="bottom" >
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td>&nbsp;</td>
											<td align="right" valign="bottom">
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
														<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
															<table border="0" cellspacing="0" cellpadding="2">
																<tr>
																	<td nowrap="nowrap"><a href="javascript:removeCat(document.form1,'userID[]','group_access_right_member_remove.php?GroupID=<?=$GroupID ?>')" class="tabletool" title="<?= $button_delete ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle"> <?= $button_delete ?></a></td>
																</tr>
															</table>
														</td>
														<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><?= $li->display() ?>
					</td>
				</tr>
				</table><br>
				<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
				<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
				<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
				<input type="hidden" name="page_size_change" value="" />
				<input type="hidden" name="GroupID" value="<?= $GroupID ?>" />
				<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
			</td>
		</tr>
	</table>
</form>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>