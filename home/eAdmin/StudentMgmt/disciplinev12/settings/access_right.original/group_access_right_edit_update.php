<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$dataAry = array();

$thisModule = 'Discipline';

$GroupTitle = $_POST['GroupTitle'];
$GroupDescription = $_POST['GroupDescription'];
$GroupID = $_POST['GroupID'];

# Check Duplicate Group Title
$GroupIDRecord = $ldiscipline->GET_ACCESS_RIGHT_GROUP_ID('A',$GroupTitle, $GroupID);



# check duplication of Group Title with existing group
if($GroupID != '' && $GroupIDRecord != '' && $GroupIDRecord != $GroupID) {
	header("Location: group_access_right_edit.php?xmsg2=$i_Discipline_System_alert_exist_group_title&GroupID=$GroupID");
}
else {

# Get the GROUP ID 
#$GroupID = $ldiscipline->GET_ACCESS_RIGHT_GROUP_ID('A',$GroupTitle);


# Delete Current Access Right of this group
$ldiscipline->DELETE_ACCESS_RIGHT($GroupID, 'T', $thisModule, 'MGMT','',''); 
$ldiscipline->DELETE_ACCESS_RIGHT($GroupID, 'T', $thisModule, 'STAT','',''); 
$ldiscipline->DELETE_ACCESS_RIGHT($GroupID, 'T', $thisModule, 'REPORTS','',''); 
$ldiscipline->DELETE_ACCESS_RIGHT($GroupID, 'T', $thisModule, 'SETTINGS','',''); 

# Addition of Discipline->Settings->Access Right->Group Right (new access right)

# - Management -
# Award_Punishment
if($_POST['MGMT_Award_Punishment_View'] != '') { $dataAry['MGMT']['Award_Punishment'][] = $_POST['MGMT_Award_Punishment_View'] ;}
if($_POST['MGMT_Award_Punishment_New'] != '') { $dataAry['MGMT']['Award_Punishment'][] = $_POST['MGMT_Award_Punishment_New'] ;}
if($_POST['MGMT_Award_Punishment_Edit'] != '') { $dataAry['MGMT']['Award_Punishment'][] = $_POST['MGMT_Award_Punishment_Edit'].$_POST['f10_OwnAll'] ;}
if($_POST['MGMT_Award_Punishment_Delete'] != '') { $dataAry['MGMT']['Award_Punishment'][] = $_POST['MGMT_Award_Punishment_Delete'].$_POST['f14_OwnAll'] ;}
if($_POST['MGMT_Award_Punishment_Waive'] != '') { $dataAry['MGMT']['Award_Punishment'][] = $_POST['MGMT_Award_Punishment_Waive'] ;}
if($_POST['MGMT_Award_Punishment_Lock'] != '') { $dataAry['MGMT']['Award_Punishment'][] = $_POST['MGMT_Award_Punishment_Lock'] ;}
if($_POST['MGMT_Award_Punishment_Release'] != '') { $dataAry['MGMT']['Award_Punishment'][] = $_POST['MGMT_Award_Punishment_Release'] ;}
if($_POST['MGMT_Award_Punishment_Approval'] != '') { $dataAry['MGMT']['Award_Punishment'][] = $_POST['MGMT_Award_Punishment_Approval'] ;}

# GoodConduct_Misconduct
if($_POST['MGMT_GoodConduct_Misconduct_View'] != '') { $dataAry['MGMT']['GoodConduct_Misconduct'][] = $_POST['MGMT_GoodConduct_Misconduct_View'] ;}
if($_POST['MGMT_GoodConduct_Misconduct_New'] != '') { $dataAry['MGMT']['GoodConduct_Misconduct'][] = $_POST['MGMT_GoodConduct_Misconduct_New'] ;}
if($_POST['MGMT_GoodConduct_Misconduct_Edit'] != '') { $dataAry['MGMT']['GoodConduct_Misconduct'][] = $_POST['MGMT_GoodConduct_Misconduct_Edit'].$_POST['f11_OwnAll'] ;}
if($_POST['MGMT_GoodConduct_Misconduct_Delete'] != '') { $dataAry['MGMT']['GoodConduct_Misconduct'][] = $_POST['MGMT_GoodConduct_Misconduct_Delete'].$_POST['f15_OwnAll'] ;}
if($_POST['MGMT_GoodConduct_Misconduct_Approval'] != '') { $dataAry['MGMT']['GoodConduct_Misconduct'][] = $_POST['MGMT_GoodConduct_Misconduct_Approval'] ;}
if($_POST['MGMT_GoodConduct_Misconduct_NewLeaf'] != '') { $dataAry['MGMT']['GoodConduct_Misconduct'][] = $_POST['MGMT_GoodConduct_Misconduct_NewLeaf'] ;}

# Case_Record
if($_POST['MGMT_Case_Record_View'] != '') { $dataAry['MGMT']['Case_Record'][] = $_POST['MGMT_Case_Record_View'] ;}
if($_POST['MGMT_Case_Record_New'] != '') { $dataAry['MGMT']['Case_Record'][] = $_POST['MGMT_Case_Record_New'] ;}
if($_POST['MGMT_Case_Record_Edit'] != '') { $dataAry['MGMT']['Case_Record'][] = $_POST['MGMT_Case_Record_Edit'].$_POST['f12_OwnAll'] ;}
if($_POST['MGMT_Case_Record_Delete'] != '') { $dataAry['MGMT']['Case_Record'][] = $_POST['MGMT_Case_Record_Delete'].$_POST['f16_OwnAll'] ;}
if($_POST['MGMT_Case_Record_Lock'] != '') { $dataAry['MGMT']['Case_Record'][] = $_POST['MGMT_Case_Record_Lock'] ;}
if($_POST['MGMT_Case_Record_NewNotes'] != '') { $dataAry['MGMT']['Case_Record'][] = $_POST['MGMT_Case_Record_NewNotes'] ;}
if($_POST['MGMT_Case_Record_EditNotes'] != '') { $dataAry['MGMT']['Case_Record'][] = $_POST['MGMT_Case_Record_EditNotes'].$_POST['f28_OwnAll'] ;}
if($_POST['MGMT_Case_Record_DeleteNotes'] != '') { $dataAry['MGMT']['Case_Record'][] = $_POST['MGMT_Case_Record_DeleteNotes'].$_POST['f29_OwnAll'] ;}
if($_POST['MGMT_Case_Record_Release'] != '') { $dataAry['MGMT']['Case_Record'][] = $_POST['MGMT_Case_Record_Release'] ;}
if($_POST['MGMT_Case_Record_Finish'] != '') { $dataAry['MGMT']['Case_Record'][] = $_POST['MGMT_Case_Record_Finish'] ;}

# Conduct_Mark
if($_POST['MGMT_Conduct_Mark_View'] != '') { $dataAry['MGMT']['Conduct_Mark'][] = $_POST['MGMT_Conduct_Mark_View'] ;}
if($_POST['MGMT_Conduct_Mark_Adjust'] != '') { $dataAry['MGMT']['Conduct_Mark'][] = $_POST['MGMT_Conduct_Mark_Adjust'] ;}

# Detention
if($_POST['MGMT_Detention_View'] != '') { $dataAry['MGMT']['Detention'][] = $_POST['MGMT_Detention_View'] ;}
if($_POST['MGMT_Detention_Edit'] != '') { $dataAry['MGMT']['Detention'][] = $_POST['MGMT_Detention_Edit'].$_POST['f13_OwnAll'] ;}
if($_POST['MGMT_Detention_New'] != '') { $dataAry['MGMT']['Detention'][] = $_POST['MGMT_Detention_New'] ;}
if($_POST['MGMT_Detention_Delete'] != '') { $dataAry['MGMT']['Detention'][] = $_POST['MGMT_Detention_Delete'].$_POST['f17_OwnAll'] ;}
if($_POST['MGMT_Detention_Take_Attendance'] != '') { $dataAry['MGMT']['Detention'][] = $_POST['MGMT_Detention_Take_Attendance'] ;}
if($_POST['MGMT_Detention_RearrangeStudent'] != '') { $dataAry['MGMT']['Detention'][] = $_POST['MGMT_Detention_RearrangeStudent'] ;}

# - Statistics -
# Award_Punishment
if($_POST['STAT_Award_Punishment_View'] != '') { $dataAry['STAT']['Award_Punishment'][] = $_POST['STAT_Award_Punishment_View'] ;}

# GoodConduct_Misconduct
if($_POST['STAT_GoodConduct_Misconduct_View'] != '') { $dataAry['STAT']['GoodConduct_Misconduct'][] = $_POST['STAT_GoodConduct_Misconduct_View'] ;}

# Conduct_Mark
if($_POST['STAT_Conduct_Mark_View'] != '') { $dataAry['STAT']['Conduct_Mark'][] = $_POST['STAT_Conduct_Mark_View'] ;}

# Detention
if($_POST['STAT_Detention_View'] != '') { $dataAry['STAT']['Detention'][] = $_POST['STAT_Detention_View'] ;}

# - Reports -
# Student Report
if($_POST['REPORTS_StudentReport_View'] != '') { $dataAry['REPORTS']['StudentReport'][] = $_POST['REPORTS_StudentReport_View'] ;}

# Ranking Report
if($_POST['REPORTS_RankingReport_View'] != '') { $dataAry['REPORTS']['RankingReport'][] = $_POST['REPORTS_RankingReport_View'] ;}

# Class Summary
if($_POST['REPORTS_ClassSummary_View'] != '') { $dataAry['REPORTS']['ClassSummary'][] = $_POST['REPORTS_ClassSummary_View'] ;}

# Monthly Report
if($_POST['REPORTS_MonthlyReport_View'] != '') { $dataAry['REPORTS']['MonthlyReport'][] = $_POST['REPORTS_MonthlyReport_View'] ;}

/*
# Award_Punishment
if($_POST['REPORTS_Award_Punishment_View'] != '') { $dataAry['REPORTS']['Award_Punishment'][] = $_POST['REPORTS_Award_Punishment_View'] ;}

# GoodConduct_Misconduct
if($_POST['REPORTS_GoodConduct_Misconduct_View'] != '') { $dataAry['REPORTS']['GoodConduct_Misconduct'][] = $_POST['REPORTS_GoodConduct_Misconduct_View'] ;}

# Case_Record
if($_POST['REPORTS_Case_Record_View'] != '') { $dataAry['REPORTS']['Case_Record'][] = $_POST['REPORTS_Case_Record_View'] ;}

# Conduct_Mark
if($_POST['REPORTS_Conduct_Mark_View'] != '') { $dataAry['REPORTS']['Conduct_Mark'][] = $_POST['REPORTS_Conduct_Mark_View'] ;}

# Detention
if($_POST['REPORTS_Detention_View'] != '') { $dataAry['REPORTS']['Detention'][] = $_POST['REPORTS_Detention_View'] ;}

# Top10
if($_POST['REPORTS_Top10_View'] != '') { $dataAry['REPORTS']['Top10'][] = $_POST['REPORTS_Top10_View'] ;}
*/

# - Settings -
# Award_Punishment
if($_POST['SETTINGS_Award_Punishment_Access'] != '') { $dataAry['SETTINGS']['Award_Punishment'][] = $_POST['SETTINGS_Award_Punishment_Access'] ;}

# GoodConduct_Misconduct
if($_POST['SETTINGS_GoodConduct_Misconduct_Access'] != '') { $dataAry['SETTINGS']['GoodConduct_Misconduct'][] = $_POST['SETTINGS_GoodConduct_Misconduct_Access'] ;}

#Conduct_Mark
if($_POST['SETTINGS_Conduct_Mark_Access'] != '') { $dataAry['SETTINGS']['Conduct_Mark'][] = $_POST['SETTINGS_Conduct_Mark_Access'] ;}

# Detention
if($_POST['SETTINGS_Detention_Access'] != '') { $dataAry['SETTINGS']['Detention'][] = $_POST['SETTINGS_Detention_Access'] ;}

# eNotice Template
if($_POST['SETTINGS_eNoticeTemplate_Access'] != '') { $dataAry['SETTINGS']['eNoticeTemplate'][] = $_POST['SETTINGS_eNoticeTemplate_Access'] ;}

/*
# Email
if($_POST['SETTINGS_Email_Access'] != '') { $dataAry['SETTINGS']['Email'][] = $_POST['SETTINGS_Email_Access'] ;}

# Letter
if($_POST['SETTINGS_Letter_Access'] != '') { $dataAry['SETTINGS']['Letter'][] = $_POST['SETTINGS_Letter_Access'] ;}
*/
$ldiscipline->UPDATE_ACCESS_RIGHT_GROUP($GroupID, $GroupTitle, $GroupDescription) ;

$ldiscipline->INSERT_ACCESS_RIGHT($GroupID, $thisModule, $dataAry) ;

header("Location: group_access_right.php?xmsg=update");

intranet_closedb();

}

?>