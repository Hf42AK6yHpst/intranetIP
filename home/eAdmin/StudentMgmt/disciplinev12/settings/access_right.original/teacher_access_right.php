<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$TAGS_OBJ[] = array($i_Discipline_System_Discipline_Members_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/group_access_right.php", 0);
$TAGS_OBJ[] = array($i_Discipline_System_Teacher_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/teacher_access_right.php", 1);
$TAGS_OBJ[] = array($i_Discipline_System_Student_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/student_access_right.php", 0);

# Top menu highlight setting
$CurrentPage = "Settings_AccessRight";

$GroupID = $ldiscipline->GET_ACCESS_RIGHT_GROUP_ID('T','');
$show = 0;
$showTDContent = array();
$showTDContent[0][0] = "tabletextremark";
$showTDContent[0][1] = "<img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='10' align='absmiddle'>";
$showTDContent[1][0] = "tablegreenrow3";
$showTDContent[1][1] = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_tick_green.gif' width='20' height='20' align='absmiddle'>";
$RestoreTD = array();

### TABLE CONTENT ###
$tableMGMT .= "<table width='100%' border='0' cellpadding='0' cellspacing='1' bgcolor='#A6A6A6'>";
$tableMGMT .= "<tr class='tabletop'>";
$tableMGMT .= "<td>".$i_Discipline_System_Access_Right_Award_Punish."</td>";
$tableMGMT .= "<td>".$i_Discipline_System_Access_Right_Good_Conduct_Misconduct."</td>";
$tableMGMT .= "<td>".$i_Discipline_System_Access_Right_Conduct_Mark."</td>";
$tableMGMT .= "<td>".$i_Discipline_System_Access_Right_Detention."</td>";
$tableMGMT .= "</tr><tr class='tablerow1'>";

// Return Function & Action
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Award_Punishment','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0;}
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td1'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View." </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','GoodConduct_Misconduct','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td2'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View."</td>";
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Conduct_Mark','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td4'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View." </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Detention','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td5'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View." </td>";

$tableMGMT .= "</tr><tr class='tablerow2'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Award_Punishment','New','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td6'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_New." </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','GoodConduct_Misconduct','New','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td7'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_New." </td>";
$tableMGMT .= "<td>&nbsp;</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Detention','EditAll','EditOwn');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td9'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Edit." ";
if($tempResult[0][1]=='EditAll') { $tableMGMT .= "(".$i_Discipline_System_Access_Right_All.")";} else if($tempResult[0][1]=='EditOwn'){ $tableMGMT .= "(".$i_Discipline_System_Access_Right_Own.")"; }
$tableMGMT .= "</td>";


$tableMGMT .= "</tr><tr class='tablerow1'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Award_Punishment','EditAll','EditOwn');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td10'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Edit." ";
if($tempResult[0][1]=='EditAll') { $tableMGMT .= "(".$i_Discipline_System_Access_Right_All.")";} else if($tempResult[0][1]=='EditOwn'){ $tableMGMT .= "(".$i_Discipline_System_Access_Right_Own.")"; }
$tableMGMT .= "</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','GoodConduct_Misconduct','EditAll','EditOwn');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td11'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Edit." ";
if($tempResult[0][1]=='EditAll') { $tableMGMT .= "(".$i_Discipline_System_Access_Right_All.")";} else if($tempResult[0][1]=='EditOwn'){ $tableMGMT .= "(".$i_Discipline_System_Access_Right_Own.")"; }
$tableMGMT .= "</td>";
$tableMGMT .= "<td>&nbsp;</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Detention','New','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td13'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_New." </td>";

$tableMGMT .= "</tr><tr class='tablerow2'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Award_Punishment','DeleteAll','DeleteOwn');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td14'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Delete." ";
if($tempResult[0][1]=='DeleteAll') { $tableMGMT .= "(". $i_Discipline_System_Access_Right_All .")";} else if($tempResult[0][1]=='DeleteOwn') { $tableMGMT .= "(".$i_Discipline_System_Access_Right_Own.")";}
$tableMGMT .= "</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','GoodConduct_Misconduct','DeleteAll','DeleteOwn');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td15'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Delete." ";
if($tempResult[0][1]=='DeleteAll') { $tableMGMT .= "(". $i_Discipline_System_Access_Right_All .")";} else if($tempResult[0][1]=='DeleteOwn') { $tableMGMT .= "(".$i_Discipline_System_Access_Right_Own.")";}
$tableMGMT .= "<td>&nbsp;</td>";
$tableMGMT .= "<td>&nbsp;</td>";
$tableMGMT .= "</tr><tr class='tablerow1'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Award_Punishment','Waive','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td17'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Waive." </td>";
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','GoodConduct_Misconduct','Waive','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td40'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Waive." </td>";
$tableMGMT .= "<td>&nbsp;</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Detention','DeleteAll','DeleteOwn');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td18'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Delete." ";
if($tempResult[0][1]=='DeleteAll') { $tableMGMT .= "(". $i_Discipline_System_Access_Right_All .")";} else if($tempResult[0][1]=='DeleteOwn'){ $tableMGMT .= "(".$i_Discipline_System_Access_Right_Own.")";}
$tableMGMT .= "</td>";

$tableMGMT .= "</tr><tr class='tablerow2'>";
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Award_Punishment','Release','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td19'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Release."</td>";

$tableMGMT .= "<td>&nbsp;</td>";
$tableMGMT .= "<td>&nbsp;</td>";
$tableMGMT .= "<td>&nbsp;</td>";
$tableMGMT .= "</tr><tr class='tablerow1'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Award_Punishment','Approval','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td20'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Approval."</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','GoodConduct_Misconduct','Approval','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td21'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Approval."</td>";

$tableMGMT .= "<td>&nbsp;</td>";
$tableMGMT .= "<td>&nbsp;</td>";

$tableMGMT .= "</tr><tr class='tablerow2'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Award_Punishment','Lock','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td22'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Lock."</td>";
$tableMGMT .= "<td>&nbsp;</td>";
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Conduct_Mark','Adjust','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td24'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Adjust." </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Detention','TakeAttendance','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td25'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Take_Attendance."</td>";
$tableMGMT .= "</tr><tr class='tablerow1'>";
$tableMGMT .= "<td>&nbsp;</td>";
$tableMGMT .= "<td>&nbsp;</td>";
$tableMGMT .= "<td>&nbsp;</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Detention','RearrangeStudent','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td27'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Rearrange_Student."</td>";
$tableMGMT .= "</tr><tr class='tablerow2'>";
$tableMGMT .= "<td>&nbsp;</td>";
$tableMGMT .= "<td>&nbsp;</td>";

$tableMGMT .= "<td>&nbsp;</td>";
$tableMGMT .= "<td>&nbsp;</td>";
$tableMGMT .= "</tr><tr class='tablerow1'>";
$tableMGMT .= "<td>&nbsp;</td>";
$tableMGMT .= "<td>&nbsp;</td>";
$tableMGMT .= "<td>&nbsp;</td>";
$tableMGMT .= "<td>&nbsp;</td>";

$tableMGMT .= "</tr></table>";

$tableSTAT .= "<table width='100%' border='0' cellpadding='0' cellspacing='1' bgcolor='#A6A6A6'>";
$tableSTAT .= "<tr class='tabletop'>";
$tableSTAT .= "<td>".$i_Discipline_System_Access_Right_Award_Punish."</td>";
$tableSTAT .= "<td>".$i_Discipline_System_Access_Right_Good_Conduct_Misconduct."</td>";
$tableSTAT .= "<td>".$i_Discipline_System_Access_Right_Conduct_Mark."</td>";
$tableSTAT .= "<td>".$i_Discipline_System_Access_Right_Detention."</td>";
$tableSTAT .= "</tr><tr class='tablerow1'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','STAT','Award_Punishment','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableSTAT .= "<td class='".$showTDContent[$show][0]."' id='td30'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View." </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','STAT','GoodConduct_Misconduct','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableSTAT .= "<td class='".$showTDContent[$show][0]."' id='td31'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View." </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','STAT','Conduct_Mark','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableSTAT .= "<td class='".$showTDContent[$show][0]."' id='td32'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View." </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','STAT','Detention','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableSTAT .= "<td class='".$showTDContent[$show][0]."' id='td33'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View." </td>";

$tableSTAT .= "</tr></table>";

$tableREPORTS .= "<table width='100%' border='0' cellpadding='0' cellspacing='1' bgcolor='#A6A6A6'>";
$tableREPORTS .= "<tr class='tabletop'>";
$tableREPORTS .= "<td>".$i_Discipline_System_Access_Right_Award_Punish."</td>";
$tableREPORTS .= "<td>".$i_Discipline_System_Access_Right_Good_Conduct_Misconduct."</td>";
$tableREPORTS .= "<td>".$i_Discipline_System_Access_Right_Conduct_Mark."</td>";
$tableREPORTS .= "<td>".$i_Discipline_System_Access_Right_Detention."</td>";
$tableREPORTS .= "<td>".$i_Discipline_System_Access_Right_Top10."</td>";
$tableREPORTS .= "</tr><tr class='tablerow1'>";
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','REPORTS','Award_Punishment','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."' id='td34'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View." </td>";
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','REPORTS','GoodConduct_Misconduct','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."' id='td35'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View." </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','REPORTS','Conduct_Mark','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."' id='td37'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View." </td>";
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','REPORTS','Detention','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."' id='td38'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View." </td>";
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','REPORTS','Top10','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."' id='td39'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View." </td>";
$tableREPORTS .= "</tr></table>";



$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($i_Discipline_System_Teacher_Rights, "");

?>

<br />
<form name="form1" method="post" action="">
<table width="88%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right"><?=$linterface->GET_SYS_MSG($xmsg);?></td>
	</tr>
</table>
<table width="88%" border="0" cellspacing="0" cellpadding="5"><tr><td>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td class="form_sep_title"> <i><?= $i_Discipline_System_Teacher_Right_Management ?></i>
				<?= $tableMGMT ?>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="form_sep_title"><i> <?= $i_Discipline_System_Teacher_Right_Statistics ?></i>
				<?= $tableSTAT ?>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="form_sep_title"><i><?= $i_Discipline_System_Teacher_Right_Reports ?></i>
				<?= $tableREPORTS ?>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>
				<table width="88%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="center">
							<?= $linterface->GET_ACTION_BTN($button_edit, "button", "javascript:location.href='teacher_access_right_edit.php'")?></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</td></tr></table>
</form>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>