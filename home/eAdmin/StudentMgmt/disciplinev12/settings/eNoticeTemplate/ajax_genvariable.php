<?php
# using: 

################## Change Log [Start]
#
#   Date    :   2019-05-01 (Bill)
#               Category Selection > Change 'CategoryID' to 'CategoryType' to prevent IntegerSafe()    [DM#1189]
#
#	Date	:	2012-09-07	YatWoon
#				Hide the selection if $CategoryID is empty [Case#2012-0718-0943-51073]
#
#	Date	:	2010-07-27	YatWoon
#				$CategoryID is not a MUST, due to "edit reply slip" need display all variables
#
################## Change Log [end] 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
	
intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

if($CategoryType)
{
	$tmpVariableArr = $ldiscipline->TemplateVariable($CategoryType);
	if(is_array($tmpVariableArr))
	{
		$a = 0;
		foreach($tmpVariableArr as $Key=>$Value)
		{
			$variableArr[$a][0] ="<img src='".$image_path."/".$LAYOUT_SKIN."/discipline/".$Key.".gif'>";
	// 		$variableArr[$a][1] = convert2unicode($Value[0],1);
			$variableArr[$a][1] = $Value[0];
			$a++;
		}
	}
    
	$t = $linterface->GET_SELECTION_BOX($variableArr, "id=\"genVariable\" name=\"genVariable\"","","$CategoryType");
	
	$x = "<table align='center' width='100%' border='0' cellpadding='5' cellspacing='0'>
			<tr valign='top'>
				<td valign='top' nowrap='nowrap'>". $Lang['eDiscipline']['AutoFillIn'] ."</td>
				<td class='tabletext'>". $t ."</td>
				<td width='100%'><input type='button' onClick='FillIn()' value='". $Lang['eDiscipline']['Insert'] ."'></td>
			</tr>
		</table>";
}
echo $x;
//echo $linterface->GET_SELECTION_BOX($variableArr, "id=\"genVariable\" name=\"genVariable\"","","$CategoryID");

?>