<?php
# using: 

###### Change Log [Start] ######
#
#   Date    :   2019-05-01 (Bill)
#               Category Selection > Change 'CategoryID' to 'CategoryType' to prevent IntegerSafe()    [DM#1189]
#
#	Date	:	2017-01-16 (Bill)	[2017-0113-1120-22207]
#				replace intranet_undo_htmlspecialchars() by htmlspecialchars_decode(), fixed some font-family not work
#
#	Date	:	2016-06-13 (Anna)
#			   	change CSS of page
#
#	Date	:	2013-02-04 (YatWoon)
#				Fixed: display auto fill-in selection
#
####### Change Log [End] #######

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");

intranet_auth();
intranet_opendb();

DisAllowiPadAndriod();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lfilesystem = new libfilesystem();

$ldiscipline->CONTROL_ACCESS("Discipline-SETTINGS-eNoticeTemplate-Access");
if(is_array($TemplateID))
{
	$TemplateID = $TemplateID[0];
}
$tempDetails = $ldiscipline->retrieveTemplateDetails($TemplateID);
list($Module, $CategoryType, $Title, $Subject, $Content, $ReplySlip, $RecordType, $RecordStatus, $ReplySlipContent, $SendReplySlip) = $tempDetails[0];
/*
if($ReplySlip || $ReplySlipContent)
	$ReplySlipChecked = "checked";
*/
if($SendReplySlip)
	$ReplySlipChecked = "checked";

if($RecordStatus==0)
{
	$draftChecked = 'checked';
}
elseif($RecordStatus==1)
{
	$publishChecked = 'checked';
}
$catTmpArr = $ldiscipline->TemplateCategory();
if($CategoryType=='')
{
	$CategoryType = $catTmpArr[0][0];
}
if(is_array($catTmpArr))
{
	
	$a = 0;
	foreach($catTmpArr as $Key=>$Value)
	{
		$catArr[$a] = array($Key,$Value);
		$a++;
	}
}
$catSelection = $linterface->GET_SELECTION_BOX($catArr, 'id="CategoryType" name="CategoryType" onChange="changeGenVariables(this.value)"', "", $CategoryType);
//$editReplySlipBtn = $linterface->GET_BTN($button_edit, 'button', "newWindow('editform.php',1)", $ParName, $ParOtherAttribute);
$ReplySlipPage = "select_reply_slip_type.php";
if($ReplySlipContent)	$ReplySlipPage = "editReplySlip.php?TemplateID=$TemplateID";
if($ReplySlip)	$ReplySlipPage = "editform.php";
//$editReplySlipBtn = $linterface->GET_BTN($button_edit, 'button', "newWindow('$ReplySlipPage',1)", $ParName, $ParOtherAttribute);
$editReplySlipBtn = $linterface->GET_BTN($button_edit, "button","click_edit()");

$previewBtn = $linterface->GET_BTN($button_preview, "button","newWindow('preview.php',10)");

# menu highlight setting
$CurrentPage = "Settings_eNoticeTemplate";

$TAGS_OBJ[] = array($eDiscipline['eNoticeTemplate']);

$PAGE_NAVIGATION[] = array($eDiscipline['eNoticeTemplate'], "index.php");
$PAGE_NAVIGATION[] = array($button_edit_template);

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>
<script type="text/javascript">
var callback_changeGenVariables = {
	success: function ( o )
    {
	    if(document.form1.temp_form_display_alert.value==1)
	    {
		    alert("<?=$Lang['eDiscipline']['ChangeReasonNotice']?>");
	    }
	    document.form1.temp_form_display_alert.value = 1;
	    var tmp_str = o.responseText;
	    document.getElementById('genVariableDiv').innerHTML = tmp_str;
	}
}

function changeGenVariables()
{
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	var path = "ajax_genvariable.php";
	
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_changeGenVariables);
}
function checkForm(obj) 
{
	if(!check_text(obj.Title,'<?=$i_Discipline_System_Discipline_Template_Title_JS_warning?>'))
	{
		return false
	}
	if(!check_text(obj.Subject,'<?=$i_Discipline_System_Discipline_Template_Subject_JS_warning?>'))
	{
		return false
	}
	/*
	if(obj.Content.value=="")
	{
		alert('<?=$i_Form_pls_fill_in?>');
		return false
	}
	*/
	
	var field = FCKeditorAPI.GetInstance('Content');
	var content_var = field.GetHTML(true);
	if(content_var=="" || content_var=="<br />")
	{
		alert('<?=$i_Form_pls_fill_in?>');
		return false
	}
	
	return true
}

function click_edit()
{
	newWindow("<?=$ReplySlipPage?>",1);
	document.form2.cStr.value = document.form1.cStr.value;
	document.form2.CategoryType.value = document.form1.CategoryType.value;
	document.form2.submit();
	
}

function FillIn()
{
	var field = FCKeditorAPI.GetInstance('Content');
	//alert(field.GetHTML(true));
	field.InsertHtml(document.form1.genVariable.value);
}
						
</script>
<form name="form1" method="post" action="edit_update.php" onSubmit="return checkForm(form1)">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr> 
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table class="form_table_v30">
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="field_title"><?=$i_Discipline_System_Discipline_Template_Title?>
								<span class="tabletextrequire">* </span></td>
								<td class="tabletext"><INPUT maxLength="80" name="Title" class="textboxtext" value="<?=$Title?>">
								</td>
							</tr>
						
							<tr valign="top">
								<td class="field_title"><?=$i_Discipline_System_Discipline_Reason_For_Issue?>
								</td>
								<td class="tabletext"><?=$catSelection?>
								</td>
							</tr>
						
							<tr valign="top">
								<td class="field_title"><?=$i_Notice_ReplySlip?>
								</td>
								<td class="tabletext"><INPUT type="checkbox" name="ReplySlip" id="ReplySlip" <?=$ReplySlipChecked?> value="1"><?=$i_general_yes2?><?=$editReplySlipBtn?><?=$previewBtn?>
								</td>
							</tr>
					
							<tr valign="top">
								<td class="field_title"><?=$eDiscipline['Subject']?>
								<span class="tabletextrequire">* </span></td>
								<td class="tabletext"><INPUT type="text" maxLength="80" name="Subject" id="Subject" class="textboxtext" value="<?=$Subject?>">
								</td>
							</tr>
						
							<tr valign="top">
								<td class="field_title"><?=$i_Discipline_Status?>
								<span class="tabletextrequire">* </span></td>
								<td class="tabletext">
								<input type='radio' id='RecordStatus0' name='RecordStatus' value=0 <?=$draftChecked?>><label for="RecordStatus0"><?=$i_Discipline_System_Discipline_Template_Draft?></label>
								<input type='radio' id='RecordStatus1' name='RecordStatus' value=1 <?=$publishChecked?>><label for="RecordStatus1"><?=$i_Discipline_System_Discipline_Template_Published?></label>
								</td>
							</tr>
			
							<tr valign="top">
								<td class="field_title"><?=$eDiscipline['Content']?> <span class="tabletextrequire">* </span></td>
								<td>
									<div id="genVariableDiv"></div>
								</td>
							</tr>
						</table>
						
						
						<?
							include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
							$objHtmlEditor = new FCKeditor ( 'Content' , "100%", "320", "", "Basic2_withInsertImageFlash", htmlspecialchars_decode($Content));
							$objHtmlEditor->Config['FlashImageInsertPath'] = $lfilesystem->returnFlashImageInsertPath($cfg['fck_image']['SchoolNews'], $id);
							$objHtmlEditor->Create();
						?>
						
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr>
								<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
								<td width="80%">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<span class="dotline">
									<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'")?>
									</span></td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<input type="hidden" name="TemplateID" value="<?= $TemplateID?>">
<input type="hidden" name="qStr" value="<?=$ReplySlip?>">
<input type="hidden" name="aStr" value="">
<input type="hidden" name="cStr" value="<?=$ReplySlipContent?>">
<input type="hidden" name="temp_form_display_alert" value="0">

<script>changeGenVariables()</script>
</form>

<form name="form2" action="<?=$ReplySlipPage?>" method="post" target="intranet_popup1">
<input type="hidden" name="cStr" value="">
<input type="hidden" name="CategoryType" value="">
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>