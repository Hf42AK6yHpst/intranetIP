<?php
# using: yat

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# menu highlight setting
$CurrentPage = "Settings_GeneralSettings";

# build content table
$table_row = 0;
$table .= "<table width='100%' border='0' cellpadding='5' cellspacing='0'>";

$table .= "<tr>";
$table .= "<td class='tabletop'>". $eDiscipline['SettingName'] ."</td>";
$table .= "<td class='tabletop'>". $eDiscipline['Value'] ."</td>";
$table .= "</tr>";

$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
$table .= "<tr class='". $tablerow_class ."'>";
$table .= "<td>". $eDiscipline['SemesterRatio_MAX'] ."</td>";
$table .= "<td>". $ldiscipline->SemesterRatio_MAX ."</td>";
$table .= "</tr>";

$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
$table .= "<tr class='". $tablerow_class ."'>";
$table .= "<td>". $eDiscipline['ConductMarkIncrement_MAX'] ."</td>";
$table .= "<td>". $ldiscipline->ConductMarkIncrement_MAX ."</td>";
$table .= "</tr>";

$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
$table .= "<tr class='". $tablerow_class ."'>";
$table .= "<td>". $eDiscipline['AwardPunish_MAX'] ."</td>";
$table .= "<td>". $ldiscipline->AwardPunish_MAX ."</td>";
$table .= "</tr>";

$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
$table .= "<tr class='". $tablerow_class ."'>";
$table .= "<td>". $Lang['eDiscipline']['DetentinoSession_MAX'] ."</td>";
$table .= "<td>". $ldiscipline->DetentinoSession_MAX ."</td>";
$table .= "</tr>";

$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
$table .= "<tr class='". $tablerow_class ."'>";
$table .= "<td>". $eDiscipline['AccumulativeTimes_MAX'] ."</td>";
$table .= "<td>". $ldiscipline->AccumulativeTimes_MAX ."</td>";
$table .= "</tr>";

$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
$table .= "<tr class='". $tablerow_class ."'>";
$table .= "<td>". $eDiscipline['ApprovalLevel_MAX'] ."</td>";
$table .= "<td>". $ldiscipline->ApprovalLevel_MAX ."</td>";
$table .= "</tr>";

$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
$table .= "<tr class='". $tablerow_class ."'>";
$table .= "<td>". $eDiscipline['UseSubject'] ."</td>";
$table .= "<td>". ($ldiscipline->use_subject ? $i_general_yes:$i_general_no)."</td>";
$table .= "</tr>";

$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
$table .= "<tr class='". $tablerow_class ."'>";
$table .= "<td>". $eDiscipline['AP_Conversion_MAX'] ."</td>";
$table .= "<td>". $ldiscipline->AP_Conversion_MAX ."</td>";
$table .= "</tr>";

$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
$table .= "<tr class='". $tablerow_class ."'>";
$table .= "<td>". $eDiscipline['Activate_NewLeaf'] ."</td>";
$table .= "<td>". ($ldiscipline->use_newleaf ? $i_general_yes:$i_general_no)."</td>";
$table .= "</tr>";

$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
$table .= "<tr class='". $tablerow_class ."'>";
$table .= "<td>". $eDiscipline['Detention_Sat'] ."</td>";
$table .= "<td>". ($ldiscipline->Detention_Sat ? $i_general_yes:$i_general_no)."</td>";
$table .= "</tr>";

$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
$table .= "<tr class='". $tablerow_class ."'>";
$table .= "<td>". $eDiscipline['Hidden_ConductMark'] ."</td>";
$table .= "<td>". ($ldiscipline->Hidden_ConductMark ? $i_general_yes:$i_general_no)."</td>";
$table .= "</tr>";

$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
$table .= "<tr class='". $tablerow_class ."'>";
$table .= "<td>". $Lang['eDiscipline']['DisplayConductMarkInAP'] ."</td>";
$table .= "<td>". ($ldiscipline->Display_ConductMarkInAP ? $i_general_yes:$i_general_no)."</td>";
$table .= "</tr>";

$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
$table .= "<tr class='". $tablerow_class ."'>";
$table .= "<td>". $eDiscipline['AP_Interval_Value'] ."</td>";
$table .= "<td>". ($ldiscipline->AP_Interval_Value ? $ldiscipline->AP_Interval_Value : "1") ."</td>";
$table .= "</tr>";

$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
$table .= "<tr class='". $tablerow_class ."'>";
$table .= "<td>". $eDiscipline['MaxRecordDayBeforeAllow'] ."</td>";
$table .= "<td>". $ldiscipline->MaxRecordDayBeforeAllow ."</td>";
$table .= "</tr>";

$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
$table .= "<tr class='". $tablerow_class ."'>";
$table .= "<td>". $Lang['eDiscipline']['AutoReleaseForAPRecord'] ."</td>";
$table .= "<td>". ($ldiscipline->AutoReleaseForAPRecord ? $i_general_yes:$i_general_no) ."</td>";
$table .= "</tr>";

$tablerow_class = "tablerow". (($table_row++%2 ==1) ? "2":"");
$table .= "<tr class='". $tablerow_class ."'>";
$table .= "<td>". $Lang['eDiscipline']['RejectRecordEmailNotification'] ."</td>";
$table .= "<td>". ($ldiscipline->RejectRecordEmailNotification  ? $i_general_yes:$i_general_no) ."</td>";
$table .= "</tr>";

$table .= "</table>";


# Left menu 
$TAGS_OBJ[] = array($eDiscipline['General_Settings'], "");

$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>

<form name="form1" method="post" action="edit.php">
<br />
<table width="96%" border="0" cellspacing="0" cellpadding="5">
<tr> 
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="2"><?=$table?></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
</tr>
<tr>
	<td align="right"><table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="center">
					<span class="dotline">
						<?= $linterface->GET_ACTION_BTN($button_edit, "submit")?>
					</span>
				</td>
			</tr>
		</table>
	</td>
</tr>
</table>


</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
