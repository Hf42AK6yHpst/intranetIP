<?php
# using: 

##############################
#
#	Date	:	2017-03-17	(Bill)	[2016-1207-1221-39240]
#				create File
#
##############################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Conduct_Mark-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# menu highlight setting
$CurrentPage = "Settings_ActivityScore";

$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Warning_Rules, "warning_rule.php", 0);
$TAGS_OBJ[] = array($Lang['eDiscipline']['ActivityScoreBaseMark'], "base_mark.php", 0);
$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Grading_Scheme, "grading_scheme.php", 0);
$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Semester_Ratio, "semester_ratio.php", 1);

$selectedYear = $_POST['schoolYear'];

$yearID = $selectedYear;
$schoolYear = $ldiscipline->getAcademicYearNameByYearID($yearID);
$semester_data = getSemesters($yearID);

$SemesterRatio_MAX = $ldiscipline->SemesterRatio_MAX;

$total = 0;
$ary = array();
$i = 0;
foreach($semester_data as $id=>$name) {
	$select .= "<tr><td>";
	$select .= $name;
	$select .= "</td><td><SELECT name='ratio".$i."'>";
	for ($j=0; $j<=$SemesterRatio_MAX; $j++)
	{
		$temp = $ldiscipline->getSemesterRatio('', '', $yearID, $id, 'A');
		$select .= "<OPTION value=$j ".($j==$temp?"SELECTED":"").">".$j."</OPTION>\n";
	}
	$select .= "</SELECT></td></tr>";
	$i++;
}
$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
function confirmCalculation()
{
     if (confirm('<?=$i_Discipline_System_alert_calculate?>'))
     {
         return true;
     }
     return false;
}
</script>

<form name="form1" method="post" action="semester_ratio_edit_update.php">
<br />
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Conduct_School_Year?></td>
								<td>
									<Strong><?=$schoolYear ?></Strong>
								</td>
							</tr>
							<tr valign="top">
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Discipline_System_Conduct_Semester_Ratio?></span></td>
								<td>
									<table border="0" cellspacing="0" cellpadding="3">
										<tr>
											<td><U><?=$i_Discipline_System_Conduct_Semester?></U></td>
											<td align="center"><u><?=$i_Discipline_System_Conduct_Ratio?></u></td>
										</tr>
											<?=$select ?>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" alt="" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<?= $linterface->GET_ACTION_BTN($button_save, "submit")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button","javascript:location.href='semester_ratio.php?schoolYear=$yearID'")?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<input type="hidden" name="yearID" value="<?=$yearID ?>">

</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>