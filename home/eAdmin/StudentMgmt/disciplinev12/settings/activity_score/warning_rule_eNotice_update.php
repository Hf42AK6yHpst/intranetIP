<?php

#############################################
#
#	Date	:	2017-03-17	(Bill)	[2016-1207-1221-39240]
#				create File
#
#############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# check access right
if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Conduct_Mark-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$categoryValue = $CategoryID0;
$subCategoryValue = $SelectNotice0;
$additionInfo = intranet_htmlspecialchars($TextAdditionalInfo0);

$ldiscipline->editWarningRuleTemplate($categoryValue, $subCategoryValue, $additionInfo, "ActivityScoreWarningRuleTemplate");

header("Location: warning_rule.php?xmsg=update");

intranet_closedb();
?>