<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

//header("Content-Type:text/html;charset=utf-8");

$ldiscipline = new libdisciplinev12();
$lclass = new libclass();
$result = $ldiscipline->getRankTarget($target);

$RTDetail = explode(",",$rankTargetDetail);

$temp = "<select name='rankTargetDetail[]' class='formtextbox' multiple size='5'>";
for($i=0;$i<sizeof($result);$i++) {
	if($target=='form') {
		$ClassLvlArr = $lclass->getLevelArray();

		$RTDetail = explode(",", $rankTargetDetail);
		$SelectedFormTextArr = explode(",", $SelectedFormText);
		
		$temp = "<select name=\"rankTargetDetail[]\" multiple size=\"5\">\n";
		for ($i = 0; $i < sizeof($ClassLvlArr); $i++) {
			$temp .= "<option value=\"".$ClassLvlArr[$i][0]."\"";
			$temp .= (in_array($ClassLvlArr[$i][0], $RTDetail)) ? " selected" : "";
			$temp .= ">".$ClassLvlArr[$i][1]."</option>\n";
		}
		$temp .= "</select>\n";
	}
	
	if($target=="class") {
		$classResult = $ldiscipline->getRankClassList($result[$i][0]);
		//$temp .= "<optgroup label='Form {$result[$i][0]}'></optgroup>";
		//debug_r($classResult);
		for($k=0;$k<sizeof($classResult);$k++) {
			$temp .= "<option value={$classResult[$k][0]}";
			for($j=0;$j<sizeof($RTDetail);$j++) {
				$temp .= ($RTDetail[$j]==$classResult[$k][0]) ? " selected" : "";	
			}
			$temp .= ">{$classResult[$k][0]}</option>";
		}
	}
}

$temp .= "</select>";

echo $temp;
return $temp;

intranet_closedb();

?>