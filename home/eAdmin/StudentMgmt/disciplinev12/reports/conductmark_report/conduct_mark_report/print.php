<?php
# using: 

################################
#
#	Date:	2019-05-13 (Bill)
#           Prevent SQL Injection
#
#	Date:	2016-05-12 (Bill)
#			Fixed: PHP error due to implode()
#
###############################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();
$lclass = new libclass();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-ConductMarkReport-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$target = IntegerSafe($target);
for($i=0; $i<sizeof($target); $i++)
{
	if ($level == 1)
	{
		$sql = "SELECT YearName FROM YEAR WHERE YearID = '".$target[$i]."'";
		$result = $ldiscipline->returnVector($sql);
		$target[$i] = $result[0];
	}
	else
	{
		$target[$i] = $lclass->getClassName($target[$i]);
	}
}
?>

<style type='text/css' media='print'>
 .print_hide {display:none;}
</style>

<br>
<table border="0" width="100%">
	<tr class='print_hide'>
		<td align="right">
			<?=$linterface->GET_BTN($button_print, "button","javascript:window.print()")?>
		</td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="2" cellspacing="1" align="center">
	<tr class="tabletext"><td><?= $iDiscipline['Period'].":". $ldiscipline->getAcademicYearNameByYearID($selectYear)." ".$ldiscipline->getTermNameByTermID($semester) ?></td></tr>
	<tr class="tabletext"><td><?= (($level == 1) ? $i_ClassLevel : $i_ClassName).": ".implode(",", (array)$target) ?></td></tr>
	<tr class="tabletext"><td><?= $eDiscipline['Conduct_Mark'].":".$startMark ." $i_To ".$endMark?></td></tr>
	<tr class="tabletext"><td><?= urldecode($content); ?></td></tr>
</table>
<br />
<?

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
intranet_closedb();
?>