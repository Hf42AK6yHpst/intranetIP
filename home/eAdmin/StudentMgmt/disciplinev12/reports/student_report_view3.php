<?php
$perPage = 10;

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

if($ck_page_size=='') {
	$ck_page_size = $perPage;	
}

# preserve table view
if ($ck_right_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
	$ck_right_page_number = $pageNo;
} else if (!isset($pageNo))
{
	$pageNo = 1;
}
/*} else if (!isset($pageNo) && $ck_right_page_number!="")
{
	$pageNo = $ck_right_page_number;
}*/

if ($ck_right_page_order!=$order && $order!="")
{
	setcookie("ck_right_page_order", $order, 0, "", "", 0);
	$ck_right_page_order = $order;
} else if (!isset($order) && $ck_right_page_order!="")
{
	$order = $ck_right_page_order;
}

if ($ck_right_page_field!=$field && $field!="")
{
	setcookie("ck_right_page_field", $field, 0, "", "", 0);
	$ck_right_page_field = $field;
} else if (!isset($field) && $ck_right_page_field!="")
{
	$field = $ck_right_page_field;
}


$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lstudentprofile = new libstudentprofile();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && (!$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-Award_Punishment-View") && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-GoodConduct_Misconduct-View") && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-Case_Record-View") && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-Conduct_Mark-View") && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-Detention-View") && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-Top10-View"))) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# check parameter exist
if($studentID=='' || $targetClass == '') {
	header("Location: student_report.php");	
}


# Temp Assign memory of this page
ini_set("memory_limit", "200M"); 

if (!$lstudentprofile->is_merit_disabled) {	
	$meritType .= "<input name='merit[]' type='checkbox' value='1' id='merit1' onClick=\"checkClickBox(this,'all_award')\"";
	for($i=0;$i<sizeof($merit);$i++) {
		$meritType .= ($merit[$i]==1) ? " checked" : "";
	}
	$meritType .= "><label for='merit1'>".$i_Merit_Merit."</label>";
}
if (!$lstudentprofile->is_min_merit_disabled) {	
	$meritType .= "<input name='merit[]' type='checkbox' value='2' id='merit2' onClick=\"checkClickBox(this,'all_award')\"";
	for($i=0;$i<sizeof($merit);$i++) {
		$meritType .= ($merit[$i]==2) ? " checked" : "";
	}
	$meritType .= "><label for='merit2'>".$i_Merit_MinorCredit."</label>";
}
if (!$lstudentprofile->is_maj_merit_disabled) {	
	$meritType .= "<input name='merit[]' type='checkbox' value='3' id='merit3' onClick=\"checkClickBox(this,'all_award')\"";
	for($i=0;$i<sizeof($merit);$i++) {
		$meritType .= ($merit[$i]==3) ? " checked" : "";
	}
	$meritType .= "><label for='merit3'>".$i_Merit_MajorCredit."</label>";
}
if (!$lstudentprofile->is_sup_merit_disabled) {	
	$meritType .= "<input name='merit[]' type='checkbox' value='4' id='merit4' onClick=\"checkClickBox(this,'all_award')\"";
	for($i=0;$i<sizeof($merit);$i++) {
		$meritType .= ($merit[$i]==4) ? " checked" : "";
	}
	$meritType .= "><label for='merit4'>".$i_Merit_SuperCredit."</label>";
}
if (!$lstudentprofile->is_ult_merit_disabled) {	
	$meritType .= "<input name='merit[]' type='checkbox' value='5' id='merit5' onClick=\"checkClickBox(this,'all_award')\"";
	for($i=0;$i<sizeof($merit);$i++) {
		$meritType .= ($merit[$i]==5) ? " checked" : "";
	}
	$meritType .= "><label for='merit5'>".$i_Merit_UltraCredit."</label>";
}

if (!$lstudentprofile->is_black_disabled) {	
	$demeritType .= "<input name='demerit[]' type='checkbox' value='-1' id='demerit1' onClick=\"checkClickBox(this,'all_punishment')\"";
	for($i=0;$i<sizeof($demerit);$i++) {
		$demeritType .= ($demerit[$i]==-1) ? " checked" : "";
	}
	$demeritType .= "><label for='demerit1'>".$i_Merit_BlackMark."</label>";
}
if (!$lstudentprofile->is_min_demer_disabled) {	
	$demeritType .= "<input name='demerit[]' type='checkbox' value='-2' id='demerit2' onClick=\"checkClickBox(this,'all_punishment')\"";
	for($i=0;$i<sizeof($demerit);$i++) {
		$demeritType .= ($demerit[$i]==-2) ? " checked" : "";
	}
	$demeritType .= "><label for='demerit2'>".$i_Merit_MinorDemerit."</label>";
}
if (!$lstudentprofile->is_maj_demer_disabled) {	
	$demeritType .= "<input name='demerit[]' type='checkbox' value='-3' id='demerit3' onClick=\"checkClickBox(this,'all_punishment')\"";
	for($i=0;$i<sizeof($demerit);$i++) {
		$demeritType .= ($demerit[$i]==-3) ? " checked" : "";
	}
	$demeritType .= "><label for='demerit3'>".$i_Merit_MajorDemerit."</label>";
}
if (!$lstudentprofile->is_sup_demer_disabled) {	
	$demeritType .= "<input name='demerit[]' type='checkbox' value='-4' id='demerit4' onClick=\"checkClickBox(this,'all_punishment')\"";
	for($i=0;$i<sizeof($demerit);$i++) {
		$demeritType .= ($demerit[$i]==-4) ? " checked" : "";
	}
	$demeritType .= "><label for='demerit4'>".$i_Merit_SuperDemerit."</label>";
}
if (!$lstudentprofile->is_ult_demer_disabled) {	
	$demeritType .= "<input name='demerit[]' type='checkbox' value='-5' id='demerit5' onClick=\"checkClickBox(this,'all_punishment')\"";
	for($i=0;$i<sizeof($demerit);$i++) {
		$demeritType .= ($demerit[$i]==-5) ? " checked" : "";
	}
	$demeritType .= "><label for='demerit5'>".$i_Merit_UltraDemerit."</label>";
}

$SchoolYear = getCurrentAcademicYear();
$selectSchoolYear1 .= "<option value='0'";
$selectSchoolYear1 .= ($SchoolYear1==0) ? " selected" : "";
$selectSchoolYear1 .= ">".$i_Discipline_System_Award_Punishment_All_School_Year."</option>";
$selectSchoolYear1 .= $ldiscipline->getConductSchoolYear($SchoolYear1);

$selectSchoolYear2 .= "<option value='0'";
$selectSchoolYear2 .= ($SchoolYear2==0) ? " selected" : "";
$selectSchoolYear2 .= ">".$i_Discipline_System_Award_Punishment_All_School_Year."</option>";
$selectSchoolYear2 .= $ldiscipline->getConductSchoolYear($SchoolYear2);

# Semester Menu #
$semester_data = split("\n",get_file_content("$intranet_root/file/semester.txt"));

$SemesterMenu1 .= "<option value='WholeYear'";
$SemesterMenu1 .= ($semester1 != 'WholeYear') ? "" : " selected";
$SemesterMenu1 .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";
for ($i=0; $i<sizeof($semester_data); $i++)
{
	$target_sem = $semester_data[$i];
	$line = split("::",$target_sem);
	list ($name,$current) = $line;
	$SemesterMenu1 .= "<option value='".$name."'";
	if($name==$semester1) { $SemesterMenu1 .= " selected"; }
	$SemesterMenu1 .= ">".$name."</option>";
}

$SemesterMenu2 .= "<option value='WholeYear'";
$SemesterMenu2.= ($semester2 != 'WholeYear') ? "" : " selected";
$SemesterMenu2 .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";
for ($i=0; $i<sizeof($semester_data); $i++)
{
	$target_sem = $semester_data[$i];
	$line = split("::",$target_sem);
	list ($name,$current) = $line;
	$SemesterMenu2 .= "<option value='".$name."'";
	if($name==$semester2) { $SemesterMenu2 .= " selected"; }
	$SemesterMenu2 .= ">".$name."</option>";
}
# Class #
$select_class = $ldiscipline->getSelectClassWithWholeForm("name=\"targetClass\" onChange=\"showResult(this.value,'')\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes, $i_alert_pleaseselect);

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

$classAry = array();
$studentAry = array();

$studentAry = $ldiscipline->storeStudent($studentID, $targetClass);

$meritImg = "<img src={$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_merit.gif width=20 height=20 border=0 align=absmiddle border=0>";
$demeritImg = "<img src={$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_demerit.gif width=20 height=20 border=0 align=absmiddle border=0>";
$waivedImg = "<img src={$image_path}/{$LAYOUT_SKIN}/icon_waived.gif width=20 height=20 align=absmiddle title=Waived border=0>";

$semester1Text = ($semester1=='WholeYear') ? $i_Discipline_System_Award_Punishment_Whole_Year : $semester1;
$semester2Text = ($semester2=='WholeYear') ? $i_Discipline_System_Award_Punishment_Whole_Year : $semester2;


# SQL conditions (A&P) #

if($award_punish_period == 1) {
	if($SchoolYear1 != '0') {		# specific school year
		$date1 = $SchoolYear1." ".$semester1Text;
		list($startYear1, $endYear1) = split('-',$SchoolYear1);
		
		$semester_data = split("\n",get_file_content("$intranet_root/file/semester.txt"));
		for ($i=0; $i<sizeof($semester_data); $i++)
		{
			$target_sem = $semester_data[$i];
			$line = split("::",$target_sem);
			list ($name,$current,$fr,$to) = $line;
			if($name==$semester1 || $semester1=='WholeYear') {			# selected semester
				if($semester1 != 'WholeYear' && $fr != '' && $to != '') {	# specific semester (in specific year)
					$tempStart = (substr($fr,5,2)>='09' && substr($fr,5,2)<='12') ? $startYear1 : $endYear1;
					$tempEnd = (substr($to,5,2)>='01' && substr($to,5,2)<='08') ? $endYear1 : $startYear1;
					
					$tempStart .= "-".substr($fr,5);		# yyyy-mm-dd
					$tempEnd .= "-".substr($to,5);			# yyyy-mm-dd
					$conds .= " AND a.RecordDate BETWEEN '$tempStart' AND '$tempEnd'";
				} else {	
					$conds .= " AND a.Year='$SchoolYear1'";
				}
				break;
			}
		}
	} else {						# all school year
		$date1 = $i_Discipline_System_Award_Punishment_All_School_Year." ".$semester1;
		$academicYear = $ldiscipline->generateAllSchoolYear();
		$tempConds = "";
					 
		$semester_data = split("\n",get_file_content("$intranet_root/file/semester.txt"));
		for ($i=0; $i<sizeof($semester_data); $i++)
		{
			$target_sem = $semester_data[$i];
			$line = split("::",$target_sem);
			list ($name,$current,$fr,$to) = $line;
			if($name==$semester1 || $semester1=='WholeYear') {			# selected semester
				if($semester1 != 'WholeYear' && $fr != '' && $to != '') {	# specific semester (in specific year)
					for($j=0;$j<sizeof($academicYear);$j++) {
						list($startYear1, $endYear1) = split('-',$academicYear[$j][0]);
						
						$tempStart = (substr($fr,5,2)>='09' && substr($fr,5,2)<='12') ? $startYear1 : $endYear1;
						$tempEnd = (substr($to,5,2)>='01' && substr($to,5,2)<='08') ? $endYear1 : $startYear1;
						
						$tempStart .= "-".substr($fr,5);		# yyyy-mm-dd
						$tempEnd .= "-".substr($to,5);			# yyyy-mm-dd
						$tempConds .= ($tempConds != "") ? " OR " : "";
						$tempConds .= "(a.RecordDate BETWEEN '$tempStart' AND '$tempEnd')";
					}
				}			# all school year + whole year (semester) , no need to add condition in SQL
				break;
			}
		}
		if($tempConds != "") { $conds .= " AND (".$tempConds.")"; }
	}
} else {
	$date1 = $Section1Fr." To ".$Section1To;
	$conds .= " AND (a.RecordDate BETWEEN '$Section1Fr' AND '$Section1To')";
}


$conds .= ($waive_record1 == '') ? " AND a.RecordStatus!=2" : "";

# Award & Punishment option #
if($merit != '') {
	$meritChoice = implode(",",$merit);
}
if($demerit != '') {
	$demeritChoice = implode(",",$demerit);
}

if($all_award != '' && $all_punishment != '') {				# award + punishment
	$conds .= " AND (a.MeritType=1 OR a.MeritType=-1)";
} else if($all_award != '' && $demerit != '') {			# award + demerit
	$conds .= " AND (a.MeritType=1 OR a.ProfileMeritType IN ($demeritChoice))";
} else if($all_punishment != '' && $merit != '') {		# punish + merit
	$conds .= " AND (a.MeritType=-1 OR a.ProfileMeritType IN ($meritChoice))";
} else if($merit != '' && $demerit != '') {				# merit + demerit
	$conds .= " AND (a.ProfileMeritType IN ($meritChoice) or a.ProfileMeritType IN ($demeritChoice))";
} else if($all_award != '')	{							# only award
	$conds .= " AND a.MeritType=1";
} else if($all_punishment != '') {						# only punishment
	$conds .= " AND a.MeritType=-1";
} else if($merit != '')	 {								# merit
	$conds .= " AND a.ProfileMeritType IN ($meritChoice)";
} else if($demerit != '') {								# demerit
	$conds .= " AND a.ProfileMeritType IN ($demeritChoice)";
}

# end of SQL conditions

# loop of table content #
$displayContent = "";
$tempContent = "";

$apResult = array();
$gcResult = array();
$mcResult = array();

$apResult = $ldiscipline->apStudentReport($studentAry, $conds, $waiveImg, $meritImg, $demeritImg);
$gcResult = $ldiscipline->goodConductReport($studentAry, $waive_record2, $all_gd_conduct, $gdConductChoice, $SchoolYear2, $semester2, 'row_approved');
$mcResult = $ldiscipline->misconductReport($studentAry, $waive_record2, $all_misconduct, $misConductChoice, $SchoolYear2, $semester2, 'row_approved');
//debug_r($apResult);

$pointer = 0;

for($i=0;$i<sizeof($studentAry);$i++) {
	
	$stdName = $ldiscipline->getStudentNameByID($studentAry[$i]);
	list($userID, $std_name, $className, $classNumber) = $stdName[0];
	
	$classDisplay = $className;
	$classDisplay .= ($classNumber != '') ? ' - '.$classNumber : $classNumber;
	
	if($userID != '') {
		$tempContent = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
		$tempContent .= "<tr class='tabletop'>";
		$tempContent .= "<td width='1' class='tabletoplink'>#</td>";
		$tempContent .= "<td width='15%'>{$i_RecordDate}</td>";
		$tempContent .= "<td width='1'>&nbsp;</td>";
		$tempContent .= "<td width='1'>&nbsp;</td>";
		$tempContent .= "<td width='15%'>{$i_Discipline_Reason2}</td>";
		$tempContent .= "<td width='40%'>{$eDiscipline['Category_Item2']}</td>";
		$tempContent .= "<td width='30%'>{$i_Profile_PersonInCharge}</td>";
		$tempContent .= "</tr>";
		
		//debug_r($apResult[$userID]);
		if(sizeof($apResult[$userID]['RecordDate'])==0) {
			$tempContent .= "<tr class='row_approved'>";
			$tempContent .= "<td class='tabletext' height='40' colspan='7' align='center'>{$i_no_record_exists_msg}</td>";
			$tempContent .= "</tr>";
		} else {
			for($m=$pointer;$m<sizeof($apResult);$m++) {
				//$k = ($m%2)+1;
				$j = $m+1;

				$tempContent .= "<tr class='row_approved'>";
				$tempContent .= "<td class='tabletext'>{$j}</td>";
				$tempContent .= "<td class='tabletext tablerow'>{$apResult[$userID]['RecordDate'][$m]}&nbsp;</td>";
				$tempContent .= "<td class='tabletext tablerow'>{$apResult[$userID]['waive'][$m]}&nbsp;</td>";
				$tempContent .= "<td class='tabletext tablerow'>{$apResult[$userID]['meritImg'][$m]}&nbsp;</td>";
				$tempContent .= "<td class='tabletext tablerow'>{$apResult[$userID]['record'][$m]}&nbsp;</td>";
				$tempContent .= "<td class='tabletext tablerow'>{$apResult[$userID]['item'][$m]}&nbsp;</td>";
				$tempContent .= "<td class='tabletext tablerow'>";
				
				$name_field = getNameFieldByLang();
                if($apResult[$userID]['PICID'][$m] != '' && $apResult[$userID]['PICID'][$m] != 0) {
                	$sql = "SELECT $name_field FROM INTRANET_USER WHERE UserID IN ({$apResult[$userID]['PICID'][$m]})";
                	$temp = $ldiscipline->returnArray($sql,1);
                	if(sizeof($temp)!=0) {
                    	for($p=0;$p<sizeof($temp);$p++) {
                        	$tempContent .= $temp[$p][0];
                        	$tempContent .= ($p != (sizeof($temp)-1)) ? ", " : "";
                    	}
            		} else {
                		$tempContent .= "---";
            		}
        		} else {
            		$tempContent .= "---";	
        		}

				$tempContent .= "&nbsp;</td></tr>";
			}
			if($pointer==0) { $pointer = $m; }
		}
		$tempContent .= "</table>";
		
		# A&P Record
		if($i != 0) {
			$displayContent .= "<tr class='row_approved'><td height='1'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1'></td></tr>";	
		}
		
		$displayContent .= "
			<tr>
				<td align='left' class='tablegreenrow2'>
					<table border='0' cellspacing='0' cellpadding='3'>
						<tr>
							<td><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_stu_ind.gif' width='20' height='20' align='absmiddle'>{$i_Discipline_Student} : <span class='sectiontitle'>{$std_name}</span> </td>
							<td>{$i_Discipline_Class} : <span class='sectiontitle'>{$classDisplay}</span></td>
						</tr>
					</table>
				</td>
			</tr>
		";
		
		if($award_punishment_flag==1) {
			
			$displayContent .= "
				<tr>
					<td>
						<table width='100%' border='0' cellspacing='0' cellpadding='0'>
							<tr>
								<td align='left' class='form_sep_title'><i> - {$eDiscipline['Award_and_Punishment']} -</i></td>
								<td align='right'>{$date1}</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align='left' class='tabletextremark'>
						{$tempContent}								
						<img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_waived.gif' width='20' height='20' align='absmiddle'>= {$i_Discipline_System_WaiveStatus_Waived}
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			";
			
		}
	}

}


/*
for($i=0;$i<sizeof($studentAry);$i++) {
	# student info
	$stdName = $ldiscipline->getStudentNameByID($studentAry[$i]);
	list($userID, $std_name, $className, $classNumber) = $stdName[0];
	
	$classDisplay = $className;
	$classDisplay .= ($classNumber != '') ? ' - '.$classNumber : $classNumber;

	# --------- Table A&P ---------- 
	if($userID != '') {
		
		if($award_punishment_flag==1) {
			$result = $ldiscipline->awardPunishmentStudentReport($userID, $conds, $waiveImg, $meritImg, $demeritImg);
			
			$tempContent = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
			$tempContent .= "<tr class='tabletop'>";
			$tempContent .= "<td width='1' class='tabletoplink'>#</td>";
			$tempContent .= "<td width='15%'>{$i_RecordDate}</td>";
			$tempContent .= "<td width='1'>&nbsp;</td>";
			$tempContent .= "<td width='1'>&nbsp;</td>";
			$tempContent .= "<td width='15%'>{$i_Discipline_Reason2}</td>";
			$tempContent .= "<td width='40%'>{$eDiscipline['Category_Item2']}</td>";
			$tempContent .= "<td width='30%'>{$i_Profile_PersonInCharge}</td>";
			$tempContent .= "</tr>";
			
			if(sizeof($result)==0) {
				$tempContent .= "<tr class='row_approved'>";
				$tempContent .= "<td class='tabletext' height='40' colspan='7' align='center'>{$i_no_record_exists_msg}</td>";
				$tempContent .= "</tr>";
			}
			
			for($m=0;$m<sizeof($result);$m++) {
				$k = ($m%2)+1;
				$j = $m+1;
				$tempContent .= "<tr class='row_approved'>";
				$tempContent .= "<td class='tabletext'>{$j}</td>";
				$tempContent .= "<td class='tabletext tablerow'>{$result[$m][0]}&nbsp;</td>";
				$tempContent .= "<td class='tabletext tablerow'>{$result[$m][1]}&nbsp;</td>";
				$tempContent .= "<td class='tabletext tablerow'>{$result[$m][2]}&nbsp;</td>";
				$tempContent .= "<td class='tabletext tablerow'>{$result[$m][3]}&nbsp;</td>";
				$tempContent .= "<td class='tabletext tablerow'>{$result[$m][4]}&nbsp;</td>";
				$tempContent .= "<td class='tabletext tablerow'>";
				
				$name_field = getNameFieldByLang();
                if($result[$m][5] != '' && $result[$m][5] != 0) {
                	$sql = "SELECT $name_field FROM INTRANET_USER WHERE UserID IN ({$result[$m][5]})";
                	$temp = $ldiscipline->returnArray($sql,1);
                	if(sizeof($temp)!=0) {
                    	for($p=0;$p<sizeof($temp);$p++) {
                        	$tempContent .= $temp[$p][0];
                        	$tempContent .= ($p != (sizeof($temp)-1)) ? ", " : "";
                    	}
            		} else {
                		$tempContent .= "---";
            		}
        		} else {
            		$tempContent .= "---";	
        		}

				$tempContent .= "&nbsp;</td></tr>";
			}
			$tempContent .= "</table>";
			
		}
		
		# --------- Good Conduct & Misconduct Table --------------- #
		# SQL conditions (G&M) #
		
		if($gdConduct_miscondict_period == 1) {
			$date2 = ($SchoolYear2 != '0') ? $SchoolYear2." ".$semester2Text : $i_Discipline_System_Award_Punishment_All_School_Year." ".$semester2Text;
		
		} else {
			$date2 = $Section2Fr." To ".$Section2To;
		}
		
		# GoodConduct & Misconduct Option #
		if($gdConductCategory != '') {
			$gdConductChoice = implode(",", $gdConductCategory);	
		}
		if($misConductCategory != '') {
			$misConductChoice = implode(",", $misConductCategory);	
		}		

		$gdConductContent = $ldiscipline->getGdConductContent($userID, $waive_record2, $all_gd_conduct, $gdConductChoice, $SchoolYear2, $semester2,$intranet_root, $i_no_record_exists_msg,'row_approved');
		$misConductContent = $ldiscipline->getMisConductContent($userID, $waive_record2, $all_misconduct, $misConductChoice, $SchoolYear2, $semester2,$intranet_root, $i_no_record_exists_msg,'row_approved');
		
		# --------- End of Good Conduct & Misconduct Table --------- #
		
		# display content #
		if($i != 0) {
			$displayContent .= "<tr class='row_approved'><td height='1'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1'></td></tr>";	
		}
		
		$displayContent .= "
			<tr>
				<td align='left' class='tablegreenrow2'>
					<table border='0' cellspacing='0' cellpadding='3'>
						<tr>
							<td><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_stu_ind.gif' width='20' height='20' align='absmiddle'>{$i_Discipline_Student} : <span class='sectiontitle'>{$std_name}</span> </td>
							<td>{$i_Discipline_Class} : <span class='sectiontitle'>{$classDisplay}</span></td>
						</tr>
					</table>
				</td>
			</tr>
		";
		
		if($award_punishment_flag==1) {
			$displayContent .= "
				<tr>
					<td>
						<table width='100%' border='0' cellspacing='0' cellpadding='0'>
							<tr>
								<td align='left' class='form_sep_title'><i> - {$eDiscipline['Award_and_Punishment']} -</i></td>
								<td align='right'>{$date1}</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align='left' class='tabletextremark'>
						{$tempContent}								
						<img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_waived.gif' width='20' height='20' align='absmiddle'>= {$i_Discipline_System_WaiveStatus_Waived}
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			";
		}
		if($gdConduct==1 || $misconduct==1) {
			$displayContent .= "
				<tr>
					<td>
						<table width='100%' border='0' cellspacing='0' cellpadding='0'>
							<tr>
								<td align='left' class='form_sep_title'><i> - {$eDiscipline['Good_Conduct_and_Misconduct']} -</i></td>
								<td align='right'>{$date2}</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align='left'>
						<table width='100%' border='0' cellspacing='0' cellpadding='3'>
							<tr>
			";
			if($gdConduct==1) {
				$displayContent .= "	
					<td width='50%' valign='top'>
						<table width='100%' border='0' cellspacing='0' cellpadding='0'>
							<tr class='tablebottom'>
								<td valign='top'>
									<img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_gd_conduct.gif' width='20' height='20' border='0' align='absmiddle'>{$eDiscipline['Setting_GoodConduct']} </td>
								<td align='center' valign='middle'>全年累積次數</td>
							</tr>
							{$gdConductContent}
						</table>
					</td>
				";
			}
			if($misconduct==1) {
				$displayContent .= "
					<td width='50%' valign='top'>
						<table width='100%' border='0' cellspacing='0' cellpadding='0'>
							<tr class='tablebottom'>
								<td valign='top'>
									<img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_misconduct.gif' width='20' height='20' border='0' align='absmiddle'>{$eDiscipline['Setting_Misconduct']}</td>
								<td align='center' valign='middle'>全年累積次數 </td>
							</tr>
							{$misConductContent}
						</table>
					</td>
				";
			}

			$displayContent .= "</tr></table></td></tr>";
			$displayContent .= "<tr><td>
				<table width='100%' border='0' cellspacing='0' cellpadding='0'>
				<tr><td align='left' class='tabletextremark'>() = {$i_Discipline_System_WaiveStatus_Waived}</td></tr>
				</table></td></tr>";
		}
	}
	# end of display content #
		
	#--------- End Table ------
}
*/

# Report Option #
$reportOptionContent = "<table align='center' width='100%' border='0' cellpadding='5' cellspacing='0'>";
$reportOptionContent .= "<tr>";
$reportOptionContent .= "<td colspan='2' valign='middle' nowrap='nowrap' class='form_sep_title'><i> - {$i_Discipline_Student} -</i></td>";
$reportOptionContent .= "</tr><tr>";
$reportOptionContent .= "<td valign='top' nowrap='nowrap' class='formfieldtitle'>{$i_Discipline_Class}<span class='tabletextrequire'>*</span></td>";
$reportOptionContent .= "<td class='navigation'>";
$reportOptionContent .= "{$select_class}";
$reportOptionContent .= "</td></tr>";
$reportOptionContent .= "<tr valign='top'>";
$reportOptionContent .= "<td valign='top' nowrap='nowrap'>&nbsp;</td>";
$reportOptionContent .= "<td><div id='studentID' style='position:absolute; width:280px; height:20px; z-index:0;'></div>";
$reportOptionContent .= "<select name='studentID' class='formtextbox'></select>";
$reportOptionContent .= "</td></tr><tr>";
$reportOptionContent .= "<td colspan='2' valign='middle' nowrap='nowrap' class='form_sep_title'><i>{$i_Discipline_System_Reports_First_Section}</i></td>";
$reportOptionContent .= "</tr><tr>";
$reportOptionContent .= "<td valign='top' nowrap='nowrap' class='formfieldtitle'>{$i_general_show} <font color='green'>#</font></td>";
$reportOptionContent .= "<td width='80%'>";
$reportOptionContent .= "<table width='100%' border='0' cellspacing='0' cellpadding='3'>";
$reportOptionContent .= "<tr><td valign='top'><input type='checkbox' name='all_award' id='all_award' onClick=\"resetOption(this,'merit[]')\"";
$reportOptionContent .= ($all_award != '') ? " checked" : "";
$reportOptionContent .= "><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_merit.gif' width='20' height='20' align='absmiddle'> <label for='all_award'>{$i_Discipline_System_Reports_All_Awards}</label> ";
$reportOptionContent .= "</td><td>{$meritType}</td></tr>";
$reportOptionContent .= "<tr><td valign='top'><input type='checkbox' name='all_punishment' id='all_punishment' onClick=\"resetOption(this,'demerit[]')\"";
$reportOptionContent .= ($all_punishment != '') ? " checked" : "";
$reportOptionContent .= "><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_demerit.gif' width='20' height='20' align='absmiddle'><label for='all_punishment'>{$i_Discipline_System_Reports_All_Punishment}</label> </td>";
$reportOptionContent .= "<td>{$demeritType}</td></tr></table><br>";
$reportOptionContent .= "<input type='checkbox' name='waive_record1' id='waive_record1' value='1'";
$reportOptionContent .= ($waive_record1==1) ? " checked" : "";
$reportOptionContent .= "><label for='waive_record1'>{$i_Discipline_System_Reports_Include_Waived_Record}</label>";
$reportOptionContent .= "</td></tr><tr valign='top'>";
$reportOptionContent .= "<td height='57' valign='top' nowrap='nowrap' class='formfieldtitle'>{$iDiscipline['Period']}</td>";
$reportOptionContent .= "<td>";
$reportOptionContent .= "<table border='0' cellspacing='0' cellpadding='3'><tr>";
$reportOptionContent .= "<td height='30' colspan='6'>";
$reportOptionContent .= "<input name='award_punish_period' type='radio' id='award_punish_period' value='1'";
$reportOptionContent .= ($award_punish_period==1) ? " checked" : "";
$reportOptionContent .= ">{$i_Discipline_School_Year} <select name='SchoolYear1'>{$selectSchoolYear1}</select>";
$reportOptionContent .= "{$i_Discipline_Semester} <select name='semester1'>{$SemesterMenu1}</select>";
$reportOptionContent .= "</td></tr><tr>";
$reportOptionContent .= "<td><input name='award_punish_period' type='radio' id='award_punish_period' value='2'";
$reportOptionContent .= ($award_punish_period==2) ? " checked" : "";
$reportOptionContent .= ">{$iDiscipline['Period_Start']}</td>";
$reportOptionContent .= "<td><input name='Section1Fr' type='text' class='tabletext' value='{$Section1Fr}' /></td>";
$reportOptionContent .= "<td align='center'>".$linterface->GET_CALENDAR('form1', 'Section1Fr')."</td>";
$reportOptionContent .= "<td width='25' align='center'>{$iDiscipline['Period_End']}</td>";
$reportOptionContent .= "<td><input name='Section1To' type='text' class='tabletext' value='{$Section1To}' /></td>";
$reportOptionContent .= "<td align='center'>".$linterface->GET_CALENDAR('form1', 'Section1To')."</td>";
$reportOptionContent .= "</tr></table></td></tr>";
$reportOptionContent .= "<tr><td colspan='2' valign='middle' nowrap='nowrap' class='form_sep_title'><i> {$i_Discipline_System_Reports_Second_Section}</i></td>";
$reportOptionContent .= "</tr><tr>";
$reportOptionContent .= "<td valign='top' nowrap='nowrap' class='formfieldtitle'>{$i_general_show} <font color='green'>#</font></td>";
$reportOptionContent .= "<td>";
$reportOptionContent .= "<table width='100%' border='0' cellspacing='0' cellpadding='3'>";
$reportOptionContent .= "<tr><td valign='top'><input type='checkbox' name='all_gd_conduct' id='all_gd_conduct' onClick=\"resetOption(this, 'gdConductCategory[]')\"";
$reportOptionContent .= ($all_gd_conduct != '') ? " checked" : "";
$reportOptionContent .= "><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_gd_conduct.gif' width='20' height='20' border='0' align='absmiddle'><label for='all_gd_conduct'>{$i_Discipline_System_Reports_All_Good_Conducts}</label></td>";
$reportOptionContent .= "<td align='left'>".$ldiscipline->showMeritOption($gdConductCategory)."</td></tr>";
$reportOptionContent .= "<tr><td valign='top'><input type='checkbox' name='all_misconduct' id='all_misconduct' onClick=\"resetOption(this, 'misConductCategory[]')\"";
$reportOptionContent .= ($all_misconduct != '') ? " checked" : "";
$reportOptionContent .= "><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_misconduct.gif' width='20' height='20' border='0' align='absmiddle'><label for='all_misconduct'>{$i_Discipline_System_Reports_All_Misconduct}</label><br></td>";
$reportOptionContent .= "<td align='left'>";
$reportOptionContent .= $ldiscipline->showDemeritOption($misConductCategory);
$reportOptionContent .= "</td></tr></table><br>";
$reportOptionContent .= "<input type='checkbox' name='waive_record2' id='waive_record2' value='2'";
$reportOptionContent .= ($waive_record2==2) ? "checked" : "";
$reportOptionContent .= "><label for='waive_record2'>{$i_Discipline_System_Reports_Include_Waived_Record}</label>";
$reportOptionContent .= "</td></tr><tr valign='top'>";
$reportOptionContent .= "<td height='57' valign='top' nowrap='nowrap' class='formfieldtitle'>{$iDiscipline['Period']}</td>";
$reportOptionContent .= "<td><table border='0' cellspacing='0' cellpadding='3'><tr>";
$reportOptionContent .= "<td height='30' colspan='6'><input name='gdConduct_miscondict_period' type='radio' id='radio' value='1' checked>";
$reportOptionContent .= "{$i_Discipline_School_Year} <select name='SchoolYear2'>{$selectSchoolYear2}</select>";
$reportOptionContent .= "{$i_Discipline_Semester} <select name='semester2'>{$SemesterMenu2}</select>";
$reportOptionContent .= "</td></tr><tr>";
$reportOptionContent .= "<td><input name='gdConduct_miscondict_period' type='radio' id='gdConduct_miscondict_period' value='2'";
$reportOptionContent .= ($gdConduct_miscondict_period==2) ? " checked" : "";
$reportOptionContent .= ">{$iDiscipline['Period_Start']}</td>";
$reportOptionContent .= "<td><input name='Section2Fr' type='text' class='tabletext' value='{$Section2Fr}' /></td>";
$reportOptionContent .= "<td align='center'>".$linterface->GET_CALENDAR('form1', 'Section2Fr')."</td>";
$reportOptionContent .= "<td width='25' align='center'>{$iDiscipline['Period_End']}</td>";
$reportOptionContent .= "<td><input name='Section2To' type='text' class='tabletext' value='{$Section2To}' /></td>";
$reportOptionContent .= "<td align='center'>".$linterface->GET_CALENDAR('form1', 'Section2To')."</td>";
$reportOptionContent .= "</tr></table></td></tr><tr>";
$reportOptionContent .= "<td valign='top' nowrap='nowrap' class='tabletextremark'>{$i_general_required_field}</td>";
$reportOptionContent .= "<td>&nbsp;</td></tr><tr><td colspan='2' align='center'>";
//$reportOptionContent .= "<input type='hidden' name='gdConduct' value=''><input type='hidden' name='misconduct' value=''><input type='hidden' name='award_punishment_flag' value=''>";
$reportOptionContent .= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Report, 'submit','document.form1.target=\'\';document.form1.action=\'student_report_view3.php\'')."</td></tr></table>";

# End of Report Option #

$TAGS_OBJ[] = array($eDiscipline['StudentReport'], "");

# menu highlight setting
$CurrentPage = "StudentReport";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

$i = 1;
?>
<script language="javascript">
function showSpan(span){
	document.getElementById(span).style.display="inline";
}

function hideSpan(span){
	document.getElementById(span).style.display="none";
}

function showOption(){
	hideSpan('spanShowOption');
	showSpan('spanHideOption');
	showSpan('divReportOption');
	document.getElementById('showReportOption').value = '1';
}

function hideOption(){
	hideSpan('divReportOption');
	hideSpan('spanHideOption');
	showSpan('spanShowOption');
	document.getElementById('showReportOption').value = '0';
}

function checkClickBox(obj, ref) {
	var field = "";
	if(obj.checked==true) {
		field = eval("form1." + ref);
		field.checked = false;
	}
}

function resetOption(obj, element_name) {
	var field = "";
	if(obj.checked == true)	 {
		var len = document.form1.elements.length;
		for(i=0;i<len;i++) {
			if(document.form1.elements[i].name==element_name) 
				document.form1.elements[i].checked = false;
		}
	}
}


function goCheck(form1) {
	var flag = 0;
	var temp = "";
	var region1 = 0;
	var region2 = 0;
	var region3 = 0;
	var region4 = 0;
	var APfieldChecked = 0;
	var gdFieldChecked = 0;
	var misFieldChecked = 0;
	
	// region 1
	if(form1.all_award.checked==true || form1.all_punishment.checked==true) {
		region1 = 1;
		//form1.award_punishment_flag.value = 1;
		APfieldChecked = 1;
	} 
	
	var len = form1.elements.length;
	var fieldChecked = 0;
	for(i=0;i<len;i++) {
		//region2 = (form1.elements[i].name=='merit[]' && form1.elements[i].checked==true) ? 1 : 0;
		//region2 = (form1.elements[i].name=='demerit[]' && form1.elements[i].checked==true) ? 1 : 0;
		if(form1.elements[i].name=='merit[]' && form1.elements[i].checked==true) {
			region2 = 1;	
			//form1.award_punishment_flag.value = 1;
			APfieldChecked = 1;
		} 
		//form1.award_punishment_flag.value = (fieldChecked==1) ? 1 : 0;
		
		if(form1.elements[i].name=='demerit[]' && form1.elements[i].checked==true) {
			region2 = 1;	
			//form1.award_punishment_flag.value = 1;
			APfieldChecked = 1;
		} 
		//form1.award_punishment_flag.value = (fieldChecked==1) ? 1 : 0;
		
		if(form1.elements[i].name=='gdConductCategory[]' && form1.elements[i].checked==true) {
			region4 = 1;
			//form1.gdConduct.value = 1;
			gdFieldChecked = 1;
		} 
		//form1.gdConduct.value = (fieldChecked==1) ? 1 : 0;
		
		fieldChecked = 0;
		if(form1.elements[i].name=='misConductCategory[]' && form1.elements[i].checked==true) {
			region4 = 1;
			//form1.misconduct.value = 1;
			misFieldChecked = 1;
		} 
		//form1.misconduct.value = (fieldChecked==1) ? 1 : 0;
	}
	
	// region 2
	/*
	for(i=0;i<form1.merit.length;i++) {
		temp = eval("form1.merit");
		if(temp[i].checked==true) {
			region2 = 1;	
		}
	}
	for(i=0;i<form1.demerit.length;i++) {
		temp = eval("form1.demerit");
		if(temp[i].checked==true) {
			region2 = 1;	
		}
	}
*/
	// region 3
	if(form1.all_gd_conduct.checked==true || form1.all_misconduct.checked==true) {
		region3 = 1;
	}
	/*
	// region 4
	for(i=0;i<form1.gdConductCategory.length;i++) {
		temp = eval("form1.gdConductCategory");
		if(temp[i].checked==true) {
			region4 = 1;	
			//form1.gdConduct.value = 1;
			gdFieldChecked = 1;
		}
		//form1.gdConduct.value = (fieldChecked == 1) ? 1 : 0;
	}
	for(i=0;i<form1.misConductCategory.length;i++) {
		temp = eval("form1.misConductCategory");
		if(temp[i].checked==true) {
			region4 = 1;	
			//form1.misconduct.value = 1;
			misFieldChecked = 1;
		}
		//form1.misconduct.value = (fieldChecked == 1) ? 1 : 0;
	}
	*/
	if(form1.targetClass.value == '#') {
		alert("<?=$i_alert_pleaseselect?><?=$i_Discipline_Class?>");	
		return false;
	}
	if(region1!=0 || region2!=0){
		if(form1.award_punish_period[1].checked==true && (form1.Section1Fr.value=='' && form1.Section1To.value=='')) {
			alert("<?=$i_alert_pleasefillin?><?=$i_Discipline_System_Reports_Report_Period?>");	
			return false;
		}	
		if(form1.award_punish_period[1].checked==true && ((!check_date(form1.Section1Fr,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) || (!check_date(form1.Section1To,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))))
		{
			return false;
		}
		
		if(form1.award_punish_period[1].checked==true && (form1.Section1Fr.value > form1.Section1To.value)) {
			alert("<?=$i_Discipline_System_Reports_Invalid_Date_Compare?>");
			return false;	
		}
	}
	if(region3!=0 || region4!=0){
		if(form1.gdConduct_miscondict_period[1].checked==true && (form1.Section2Fr.value=='' && form1.Section2To.value=='')) {
			alert("<?=$i_alert_pleasefillin?><?=$i_Discipline_System_Reports_Report_Period?>");	
			return false;
		}	
		if(form1.gdConduct_miscondict_period[1].checked==true && ((!check_date(form1.Section2Fr,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) || (!check_date(form1.Section2To,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))))
		{
			return false;
		}
		if(form1.gdConduct_miscondict_period[1].checked==true && (form1.Section2Fr.value > form1.Section2To.value)) {
			alert("<?=$i_Discipline_System_Reports_Invalid_Date_Compare?>");
			return false;	
		}
	}
	
	
	if(form1.all_gd_conduct.checked==true || gdFieldChecked==1) {
		form1.gdConduct.value = 1;	
	} else {
		form1.gdConduct.value = 0;	
	}
	
	if(form1.all_misconduct.checked==true || misFieldChecked==1) {
		form1.misconduct.value = 1;	
	} else {
		form1.misconduct.value = 0;	
	}
	
	if(APfieldChecked == 1) {
		form1.award_punishment_flag.value = 1;	
	} else {
		form1.award_punishment_flag.value = 0;	
	}
	
	if(form1.award_punishment_flag.value == 0 && form1.gdConduct.value == 0 && form1.misconduct.value == 0) {
		alert("<?=$i_alert_pleasefillin?><?=$i_Discipline_System_Reports_Award_Punishment?><?=$i_alert_or?><?=$i_Discipline_System_Reports_Conduct_Behavior?>");
		return false;	
	}
//	alert(form1.award_punishment_flag.value + "/" + form1.gdConduct.value + "/" + form1.misconduct.value);
	return true;
}


function showDiv(div)
{
	document.getElementById(div).style.display = "inline";
}

function hideDiv(div)
{
	document.getElementById(div).style.display = "none";
}

//window.onunload = goCheck(document.form1);
</script>
<script language="javascript">
var xmlHttp

showResult('<?=$targetClass?>','<?=$studentID?>');

function showResult(str,std)
{
	if (str.length==0)
		{ 
			document.getElementById("studentID").innerHTML = "";
			document.getElementById("studentID").style.border = "0px";
			return
		}
		
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 

	var url = "";
		
	url = "get_live.php";
	url = url + "?targetClass=" + str + "&sid=" + std
	
	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById("studentID").innerHTML = xmlHttp.responseText;
		document.getElementById("studentID").style.border = "0px solid #A5ACB2";
	} 
}


function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}
</script>
<form name="form1" method="POST" action="" onSubmit="return goCheck(this)">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr> 
		<td align="center">
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td class="report_show_option">
									<!--<a href="javascript:;" class="contenttool" onClick="javascript:if(document.getElementById('showReportOption').value=='0'){document.getElementById('showReportOption').value='1';showDiv('divReportOption');}else{document.getElementById('showReportOption').value='0';hideDiv('divReportOption');}"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_show_option.gif" width="20" height="20" border="0" align="absmiddle"><?=$i_Discipline_System_Reports_Show_Reports_Option?></a>//-->
									<span id="spanShowOption"><a href="javascript:showOption();" class="contenttool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_show_option.gif" width="20" height="20" border="0" align="absmiddle"><?=$i_Discipline_Show_Statistics_Option?></a></span>
									<span id="spanHideOption" style="display:none"><a href="javascript:hideOption();" class="contenttool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_hide_option.gif" width="20" height="20" border="0" align="absmiddle"><?=$i_Discipline_Hide_Statistics_Option?></a></span>
									<br>
									<div id='divReportOption' style='display:none'><?=$reportOptionContent?></div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br>
			<table width="88%" border="0" cellpadding="5" cellspacing="0" class="result_box">
				<tr>
					<td align="center">
						<table border="0" cellspacing="5" cellpadding="5">
							<tr>
								<td align="center" valign="middle" class="Chart_title"><?=$eDiscipline['StudentReport']?></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<?=$displayContent?>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<?= $linterface->GET_ACTION_BTN($button_export, "submit","document.form1.target='';document.form1.action='export_student.php'")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_print, "submit","document.form1.target='_blank';document.form1.action='print_student.php'")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_back, "button","javascript:self.location.href='student_report.php'")?>&nbsp;
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<input type="hidden" name="pageNo" value="<?= $li->pageNo; ?>"/>
<input type="hidden" name="order" value="<?= $li->order; ?>"/>
<input type="hidden" name="field" value="<?= $li->field; ?>"/>
<input type="hidden" name="page_size_change" value=""/>
<input type="hidden" name="numPerPage" value="<?= $li->page_size?>"/>
<input type="hidden" name="showReportOption" value="0">
<input type='hidden' name='gdConduct' value='<?=$gdConduct?>'>
<input type='hidden' name='misconduct' value='<?=$misconduct?>'>
<input type='hidden' name='award_punishment_flag' value='<?=$award_punishment_flag?>'>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
