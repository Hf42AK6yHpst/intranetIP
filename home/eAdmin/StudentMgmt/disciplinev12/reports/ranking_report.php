<?php
# using:  
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");

intranet_auth();
intranet_opendb();


$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lstudentprofile = new libstudentprofile();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-RankingReport-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}


# School Year Menu #
$SchoolYear = getCurrentAcademicYear();

//$selectSchoolYear .= "<select name='SchoolYear'>";
$selectSchoolYear .= "<option value='0'";
$selectSchoolYear .= ($SchoolYear==0) ? " selected" : "";
$selectSchoolYear .= ">".$i_Discipline_System_Award_Punishment_All_School_Year."</option>";
$selectSchoolYear .= $ldiscipline->getConductSchoolYear($SchoolYear);
//$selectSchoolYear .= "</select>";

# Semester Menu #
$semester_data = split("\n",get_file_content("$intranet_root/file/semester.txt"));

//$SemesterMenu = "<select name='semester'>";
$SemesterMenu .= "<option value='WholeYear'";
$SemesterMenu .= ($semester!='WholeYear') ? "" : " selected";
$SemesterMenu .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";
for ($i=0; $i<sizeof($semester_data); $i++)
{
	$target_sem = $semester_data[$i];
	$line = split("::",$target_sem);
	list ($name,$current) = $line;
	$SemesterMenu .= "<option value='".$name."'";
	if($name==$semester) { $SemesterMenu .= " selected"; }
	$SemesterMenu .= ">".$name."</option>";
}
//$SemesterMenu .= "</select>";

# Ranking Range
for($i=1;$i<=20;$i++) {
	$rankRangeMenu .= "<option value=$i";
	$rankRangeMenu .= ($i==10) ? " SELECTED" : "";
	$rankRangeMenu .= ">$i</option>";
}

# Class #
$select_class = $ldiscipline->getSelectClassWithWholeForm("name=\"targetClass\" onChange=\"showResult(this.value)\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes, $i_general_please_select);

/*
$ClassLvlArr = $lclass->getLevelArray();
$SelectedFormArr = explode(",", $SelectedForm);
$SelectedFormTextArr = explode(",", $SelectedFormText);
$selectFormHTML = "<select name=\"selectForm[]\" multiple size=\"5\">\n";
for ($i = 0; $i < sizeof($ClassLvlArr); $i++) {
	$selectFormHTML .= "<option value=\"".$ClassLvlArr[$i][0]."\"";
	if ($_POST["submit_flag"] != "YES" || in_array($ClassLvlArr[$i][0], $SelectedFormArr)) {
		$selectFormHTML .= " selected";
	}
	$selectFormHTML .= ">".$ClassLvlArr[$i][1]."</option>\n";
}
$selectFormHTML .= "</select>\n";
*/

$TAGS_OBJ[] = array($eDiscipline['Top10'], "");

# menu highlight setting
$CurrentPage = "Top10";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

?>

<script language="javascript">
<!--
function SelectAll(obj)
{
		var choice = document.getElementById('rankTargetDetail[]');

         for (var i=0; i<choice.options.length; i++)
         {
              choice.options[i].selected = true;
         }
}

function goCheck(form1) {

	if(form1.dateChoice[0].checked==false && form1.dateChoice[1].checked==false)	 {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['Period']?>");	
		return false;
	}
	if(form1.dateChoice[1].checked==true && (form1.startDate.value == "" || form1.endDate.value == "")) {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['Period']?>");	
		return false;
	}
	if(form1.dateChoice[1].checked==true && ((!check_date(form1.startDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) || (!check_date(form1.endDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))))
	{
		return false;
	}
	if(form1.dateChoice[1].checked==true && (form1.startDate.value > form1.endDate.value)) {
		alert("<?=$i_Discipline_System_Reports_Invalid_Date_Compare?>");
		return false;	
	}
	if(form1.rankTarget.value == "#") {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['RankingTarget']?>");	
		return false;
	}

	var choiceSelected = 0;
	var choice = document.getElementById('rankTargetDetail[]');

	for(i=0;i<choice.options.length;i++) {
		if(choice.options[i].selected==true)
			choiceSelected = 1;
	}
	
	if(choiceSelected==0) {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['RankingTarget']?>");
		return false;
	}
}
//-->
</script>
<script language="javascript">
var xmlHttp

function showResult(str)
{
	if (str.length==0)
		{ 
			document.getElementById("rankTargetDetail").innerHTML = "";
			document.getElementById("rankTargetDetail").style.border = "0px";
			return
		}
		
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 

	var url = "";
		
	url = "get_live2.php";
	url = url + "?target=" + str
	
	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById("rankTargetDetail").innerHTML = xmlHttp.responseText;
		document.getElementById("rankTargetDetail").style.border = "0px solid #A5ACB2";
		SelectAll(form1.elements['rankTargetDetail[]']);
	} 
}


function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}
</script>
<form name="form1" method="post" action="ranking_report_view.php" onSubmit="return goCheck(this)">
<table width="99%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td align="center" bgcolor="#FFFFFF">
	      <table width="98%" border="0" cellspacing="0" cellpadding="5">
	          <tr> 
	            <td align="center">
		            <table width="88%" border="0" cellpadding="5" cellspacing="0">
						<tr>
							<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="3">
									<tr>
										<td height="20" class="tabletextremark"><i><?=$i_Discipline_System_Reports_Report_Option?></i></td>
									</tr>
								</table>
								<?= $ldiscipline->showWarningMsg($i_Discipline_System_Conduct_Instruction, $i_Discipline_System_Ranking_Report_Msg) ?>
								<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
									<tr valign="top">
										<td height="57" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$iDiscipline['Period']?><span class="tabletextrequire">*</span></td>
										<td>
											<table border="0" cellspacing="0" cellpadding="3">
												<tr>
													<td height="30" colspan="6">
														<input name="dateChoice" type="radio" id="dateChoice[0]" value="1" checked>
														<?=$i_Discipline_School_Year?>
														<select name="SchoolYear" onFocus="document.form1.dateChoice[0].checked=true">
															<?=$selectSchoolYear?>
														</select>
														<?=$i_Discipline_Semester?> 
														<select name="semester" onFocus="document.form1.dateChoice[0].checked=true" >
															<?=$SemesterMenu?>
														</select>
													</td>
												</tr>
												<tr>
													<td><input name="dateChoice" type="radio" id="dateChoice[1]" value="2">
														<?=$iDiscipline['Period_Start']?></td>
													<td><input name="startDate" type="text" class="tabletext" onFocus="document.form1.dateChoice[1].checked=true" /></td>
													<td align="center" onClick="form1.dateChoice[1].checked=true"><?=$linterface->GET_CALENDAR("form1", "startDate")?></td>
													<td width="25" align="center"><?=$iDiscipline['Period_End']?></td>
													<td><input name="endDate" type="text" class="tabletext" onFocus="document.form1.dateChoice[1].checked=true" /></td>
													<td align="center" onClick="form1.dateChoice[1].checked=true"><?=$linterface->GET_CALENDAR("form1", "endDate")?></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr valign="top">
										<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$iDiscipline['RankingTarget']?><span class="tabletextrequire">*</span></td>
										<td width="80%">
											<table width="0%" border="0" cellspacing="2" cellpadding="0">
												<tr>
													<td valign="top">
														<select name="rankTarget" onChange="showResult(this.value)">
															<option value="#" selected>-- <?=$i_general_please_select?> --</option>
															<option value="form"><?=$i_Discipline_Form?></option>
															<option value="class" ><?=$i_Discipline_Class?></option>
														</select>
															<br><div id='rankTargetDetail' style='position:absolute; width:280px; height:100px; z-index:0;'></div>
															<select name="rankTargetDetail[]" multiple size="5" id="rankTargetDetail[]">
															</select>
															<br><?= $linterface->GET_ACTION_BTN($button_select_all, "button", "onClick=SelectAll(this.form.elements['rankTargetDetail[]']);return false;")?>
															<br>
															<span class="tabletextremark">(<?=$i_Discipline_Press_Ctrl_Key?>)</span>
														</td>
													<td valign="top"><br></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr valign="top">
										<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$iDiscipline['RankingRange']?><span class="tabletextrequire">*</span></td>
										<td><?=$i_general_highest?> 
											<select name="rankRange">
												<?=$rankRangeMenu?>
											</select>
										</td>
									</tr>
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$iDiscipline['RecordType']?><span class="tabletextrequire">*</span>
										</td>
										<td><input name="meritType" type="radio" id="meritType[0]" value="1" checked>
											<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/ediscipline/icon_merit.gif" width="20" height="20" border="0" align="absmiddle"> <label for="meritType[0]"><?=$i_Merit_Award?></label>
											<input name="meritType" type="radio" id="meritType[1]" value="-1" >
											<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/ediscipline/icon_demerit.gif" width="20" height="20" border="0" align="absmiddle"> <label for="meritType[1]"><?=$i_Merit_Punishment?> </label>
										</td>
									</tr>
									<tr>
										<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
										<td>&nbsp;</td>
									</tr>
								</table>
								</td>
							</tr>
							<tr>
								<td height="1" class="dotline"><img src="/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
							</tr>
							<tr>
								<td align="right">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="center">
												<?= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Report, "submit", "document.form1.action='ranking_report_view.php'")?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
			            </td>
			          </tr>
			        </table>
		        </td>
		    </tr>
			</table><br>
			
</form>

<script>
<!--
function init()
{
	var obj = document.form1;
	
	obj.rankTarget.selectedIndex=1;		// Form
	showResult("form");
}

init();
//-->
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
