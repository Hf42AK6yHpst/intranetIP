<?
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

$meritDemeritType['1'] = $i_Merit_Merit;
$meritDemeritType['2'] = $i_Merit_MinorCredit;
$meritDemeritType['3'] = $i_Merit_MajorCredit;
$meritDemeritType['4'] = $i_Merit_SuperCredit;
$meritDemeritType['5'] = $i_Merit_UltraCredit;
$meritDemeritType['0'] = $i_Merit_Warning;
$meritDemeritType['-1'] = $i_Merit_BlackMark;
$meritDemeritType['-2'] = $i_Merit_MinorDemerit;
$meritDemeritType['-3'] = $i_Merit_MajorDemerit;
$meritDemeritType['-4'] = $i_Merit_SuperDemerit;
$meritDemeritType['-5'] = $i_Merit_UltraDemerit;

if($ajaxType=='recordDate') {
	
	$result = $ldiscipline->getAPRecordInfoInLayerByDateRange($ajaxStudentID, $ajaxPeriod, $ajaxDate1, $ajaxDate2, $ajaxLevel, $ajaxTarget, $ajaxRecordType);
	
	$data = '<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">';
	$data .='	<tr class="tabletop">
					<td align="center" valign="middle">#</td>
					<td align="left">'.$i_Discipline_Reason.'</td>
					<td align="left">'.$i_Discipline_Reason2.'</td>
					<td >'.$Lang['eDiscipline']['EventDate'].'</td>
				</tr>';
	for($a=0; $a<sizeof($result); $a++)
	{
		$AdjustedScore = $result[$a]['ConductScoreChange'];
		$data .='<tr class="tablerow1">
				 <td>'.($a+1).'</td>
				 <td>'.$result[$a]['ItemText'].'</td>
				 <td>'.$ldiscipline->TrimDeMeritNumber_00($meritDemeritType[$result[$a]['ProfileMeritType']].' '.$result[$a]['ProfileMeritCount'].' '.$Lang['eDiscipline']['Times']).'</td>
				 <td>'.$result[$a]['RecordDate'].'</td></tr>';
	}
	$data .= '</table>';
		
	$div_deb = '<div id="'.$targetDivID.'" style="position:absolute; width:380px; z-index:1; ">
				  <table width="100%" border="0" cellpadding="0" cellspacing="0">
					  <tr>
						<td height="19"><table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td width="5" height="19"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_01.gif" width="5" height="19"></td>
							<td height="19" valign="middle" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_02.gif">&nbsp;</td>
							<td width="19" height="19"><a href="javascript:Hide_Window(\''.$targetDivID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_close_off.gif" name="pre_close1" width="19" height="19" border="0" id="pre_close1"></a></td>
						  </tr>
						</table></td>
					  </tr>
					  <tr>
						<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
						  <tr>
							<td width="5" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif" width="5" height="19"></td>
							<td align="left" bgcolor="#FFFFF7"><table width="100%" border="0" cellspacing="0" cellpadding="0">
							  
							  <tr>
								<td height="150" align="left" valign="top">
								'.$data.'
								
								</td></tr>
									  <tr>
								    <td><br><table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
								      <tr>
									       <td height="1" class="dotline"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>
								      </tr>
								     </table></td>
								     </tr>
		                 </table></td>
							<td width="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif" width="6" height="6"></td>
						  </tr>
						  <tr>
							<td width="5" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_07.gif" width="5" height="6"></td>
							<td height="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif" width="5" height="6"></td>
							<td width="6" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_09.gif" width="6" height="6"></td>
						  </tr>
							</table></td>
					  </tr>
					</table>
				 </div>';  

	echo $div_deb;
		
}
intranet_closedb();
?>