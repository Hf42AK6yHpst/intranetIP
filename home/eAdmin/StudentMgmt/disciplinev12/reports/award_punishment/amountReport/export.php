<?php

$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

$lexport = new libexporttext();

$filename = "weekly_report_report.csv";

$rawDataAry = array();
$rawDataAry['PeriodChoice'] = $exportPeriodChoice;
$rawDataAry['date1'] = $exportDate1;
$rawDataAry['date2'] = $exportDate2;
$rawDataAry['level'] = $exportLevel;	# class / form
$rawDataAry['target'] = $target;
$rawDataAry['amount'] = $exportAmount;
$rawDataAry['meritType'] = $exportMeritType;

$result = $ldiscipline->getMeritDemeritCountAmount($rawDataAry);

$ary = array();
$row = 0;
$exportContent = "";
$exportColumn = array();

	$exportColumn[] = $i_ClassName;
	$exportColumn[] = $i_ClassNumber;
	$exportColumn[] = $i_general_name;
	$exportColumn[] = $i_Discipline_Reason2." ".$list_total;
	$exportColumn[] = $eDiscipline['RecordDetails']."($i_Discipline_Reason)";
	$exportColumn[] = "($i_Discipline_Reason2)";
	$exportColumn[] = "($i_EventDate)";
	
	if(sizeof($result)==0) {
		$ary[$row][] = $i_no_record_exists_msg;
		$row++;
	} else {
		for($i=0; $i<sizeof($result); $i++) {
			list($sid, $cls, $clsNo, $name, $type, $total) = $result[$i];
			$css = ($i%2==0) ? 1 : 2;
			
			# export content 
			$tempData = $ldiscipline->getMeritDemeritDetailByStudentID($sid, $rawDataAry);
			
			for($j=0; $j<sizeof($tempData); $j++) {
				list($type, $count, $item, $date) = $tempData[$j];
				
				if($j==0) {
					$ary[$row][] .= $cls;
					$ary[$row][] .= $clsNo;
					$ary[$row][] .= $name;
					$ary[$row][] .= $total;
				} else {
					$ary[$row][] .= "";
					$ary[$row][] .= "";
					$ary[$row][] .= "";
					$ary[$row][] .= "";
				}
				$ary[$row][] .= $item;
				$ary[$row][] .= $ldiscipline->TrimDeMeritNumber_00($meritDemeritType[$type]." ".$count." ".$Lang['eDiscipline']['Times']);
				$ary[$row][] .= $date;
				$row++;
			}
		}
	}
	
$export_content .= $lexport->GET_EXPORT_TXT($ary, $exportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

# Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);
	
	/*
	# export header
	$exportContent .= "\"$i_ClassName\"\t";
	$exportContent .= "\"$i_ClassNumber\"\t";
	$exportContent .= "\"$i_general_name\"\t";
	$exportContent .= "\"$i_Discipline_Reason2 $list_total\"\t";
	$exportContent .= "\"".$eDiscipline['RecordDetails']."($i_Discipline_Reason)\"\t";
	$exportContent .= "\"($i_Discipline_Reason2)\"\t";
	$exportContent .= "\"($i_EventDate)\"\n";

	if(sizeof($result)==0) {
		$exportContent .= "\"$i_no_record_exists_msg\"\n";
	} else {
		for($i=0; $i<sizeof($result); $i++) {
			list($sid, $cls, $clsNo, $name, $type, $total) = $result[$i];
			$css = ($i%2==0) ? 1 : 2;
			
			# export content 
			$tempData = $ldiscipline->getMeritDemeritDetailByStudentID($sid, $rawDataAry);
			
			for($j=0; $j<sizeof($tempData); $j++) {
				list($type, $count, $item, $date) = $tempData[$j];
				//$item = addslashes($item);
				$item = intranet_htmlspecialchars($item);
				
				if($j==0) {
					$exportContent .= "\"$cls\"\t";
					$exportContent .= "\"$clsNo\"\t";
					$exportContent .= "\"$name\"\t";
					$exportContent .= "\"$total\"\t";
				} else {
					$exportContent .= "\"\"\t";
					$exportContent .= "\"\"\t";
					$exportContent .= "\"\"\t";
					$exportContent .= "\"\"\t";
				}
				$exportContent .= "\"$item\"\t";
				$exportContent .= "\"".$ldiscipline->TrimDeMeritNumber_00($meritDemeritType[$type]." ".$count." ".$Lang['eDiscipline']['Times'])."\"\t";
				$exportContent .= "\"$date\"\n";
			}
		}
	}

if ($g_encoding_unicode) {
	$Temp = explode("\n", urldecode($exportContent));
	$exportColumn = explode("\t", trim($Temp[0], "\r"));
	for ($i = 1; $i < sizeof($Temp); $i++) {
		$result[] = explode("\t", trim($Temp[$i], "\r"));
	}
	
	$export_content = $lexport->GET_EXPORT_TXT($result, $exportColumn);
	$lexport->EXPORT_FILE($filename, $export_content);
	
} else {
	$export_content .= stripslashes($exportContent);	
	$lexport->EXPORT_FILE($filename, $export_content);
}

intranet_closedb();
	*/
?>