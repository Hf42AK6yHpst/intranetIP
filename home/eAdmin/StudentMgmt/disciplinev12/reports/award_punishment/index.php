<?php
// Modifying by :
/********************** Change Log ***********************
#
#	Date	:	2010-10-15	(Yuen)
#				fix wrong date - use $generationDate['mday'] instead of $generationDate['wday'] (which is the day of a week)
#
******************* End Of Change Log *******************/


 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-Award_Punishment_Report-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$CurrentPage = "AwardPunishmentReport";
$CurrentPageArr['eDisciplinev12'] = 1;
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($eDiscipline['AwardPunishmentReport'], "", 1);

$lclass = new libclass();
$linterface = new interface_html();

# School year
$selectYear = ($selectYear == '') ? Get_Current_Academic_Year_ID() : $selectYear;
$years = $ldiscipline->returnAllYearsSelectionArray();
$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='selectYear' id='selectYear' onclick='changeRadioSelection(\"YEAR\")' onChange='hideSpan(\"spanStudent\"); changeTerm(this.value); document.form1.rankTarget.selectedIndex=1; showResult(\"form\",\"\")';", "", $selectYear);

# Semester
$currentSemester = getCurrentSemester();
$selectSemesterHTML = "<select name=\"selectSemester\" id=\"selectSemester\" onclick=\"changeRadioSelection('YEAR')\">";
$selectSemesterHTML .= "</select>";

# Form
$ClassLvlArr = $lclass->getLevelArray();
$SelectedFormArr = explode(",", $SelectedForm);
$SelectedFormTextArr = explode(",", $SelectedFormText);
$selectFormHTML = "<select name=\"rankTargetDetail[]\" id=\"rankTargetDetail[]\" multiple size=\"5\">\n";
for ($i = 0; $i < sizeof($ClassLvlArr); $i++) {
	$selectFormHTML .= "<option value=\"".$ClassLvlArr[$i][0]."\"";
	if ($_POST["submit_flag"] != "YES" || in_array($ClassLvlArr[$i][0], $SelectedFormArr)) {
		$selectFormHTML .= " selected";
	}
	$selectFormHTML .= ">".$ClassLvlArr[$i][1]."</option>\n";
}
$selectFormHTML .= "</select>\n";

# Class
$ClassListArr = $lclass->getClassList($selectYear);
$SelectedClassArr = explode(",", $SelectedClass);
$SelectedClassTextArr = explode(",", $SelectedClassText);
$selectClassHTML = "<select name=\"rankTargetDetail[]\" id=\"rankTargetDetail[]\" multiple size=\"5\">\n";
for ($i = 0; $i < sizeof($ClassListArr); $i++) {
	$selectClassHTML .= "<option value=\"".$ClassListArr[$i][0]."\"";
	if ($_POST["submit_flag"] != "YES" || in_array($ClassListArr[$i][0], $SelectedClassArr)) {
		$selectClassHTML .= " selected";
	}
	$selectClassHTML .= ">".$ClassListArr[$i][1]."</option>\n";
}
$selectClassHTML .= "</select>\n";

// Get Category selection
$award_category = $ldiscipline->getAPCategoryByType(1);
$punishment_category = $ldiscipline->getAPCategoryByType(-1);
$items = $ldiscipline->retrieveMeritItemswithCodeGroupByCatInStatistics();
$award_items = $ldiscipline->getAPItemsByType(1);
$punishment_items = $ldiscipline->getAPItemsByType(-1);



// Good Conduct Category
$selectGoodCatHTML = "<select name=\"selectGoodCat\" id=\"selectGoodCat\" onChange=\"changeCat(this.value);\">\n";
$selectGoodCatHTML .= "<option value=\"0\"";
$selectGoodCatHTML .= ($_POST["submit_flag"] != "YES")?" selected":"";
$selectGoodCatHTML .= ">-- ".$i_Discipline_System_Reports_All_Awards." --</option>";
for ($i = 0; $i < sizeof($award_category); $i++) {
	$selectGoodCatHTML .= "<option value=\"".$award_category[$i][0]."\"";
	if ($selectGoodCat == $award_category[$i][0]) {
		$selectGoodCatHTML .= " selected";
	}
	$selectGoodCatHTML .= ">".$award_category[$i][1]."</option>\n";
}
$selectGoodCatHTML .= "</select>\n";

// Misconduct Category
$selectMisCatHTML = "<select name=\"selectMisCat\" id=\"selectMisCat\" onChange=\"changeCat(this.value);\">\n";
$selectMisCatHTML .= "<option value=\"0\"";
$selectMisCatHTML .= ($_POST["submit_flag"] != "YES")?" selected":"";
//$selectMisCatHTML .= ">-- ".$eDiscipline["SelectRecordCategory"]." --</option>";
$selectMisCatHTML .= ">-- $i_Discipline_System_Reports_All_Punishment --</option>";
for ($i = 0; $i < sizeof($punishment_category); $i++) {
	$selectMisCatHTML .= "<option value=\"".$punishment_category[$i][0]."\"";
	if ($selectMisCat == $punishment_category[$i][0]) {
		$selectMisCatHTML .= " selected";
	}
	$selectMisCatHTML .= ">".$punishment_category[$i][1]."</option>\n";
}
$selectMisCatHTML .= "</select>\n";

// Items
if($record_type==1) {
	$selectedItem = $award_items;	
} else {
	$selectedItem = $punishment_items;	
}

$SelectedItemIDArr = explode(",", $SelectedItemID);
$SelectedItemIDTextArr = explode(",", $SelectedItemIDText);


$selectItemIDHTML = "<select name=\"ItemID[]\" id=\"ItemID[]\" multiple size=\"5\">\n";
if ($_POST["submit_flag"] == "YES") {
	for ($i = 0; $i < sizeof($selectedItem); $i++) {
		list($r_catID, $r_itemID, $r_itemName) = $selectedItem[$i];
		if (($record_type == 1 && $selectGoodCat == $r_catID) || ($record_type == -1 && $selectMisCat == $r_catID) || ($selectGoodCat==0 && $selectMisCat==0)) {
			$selectItemIDHTML .= "<option value=\"".$r_itemID."\"";
			if (in_array($r_itemID, $ItemID)) {
				$selectItemIDHTML .= " selected";
			}
			$selectItemIDHTML .= ">".$r_itemName."</option>\n";
		}
	}
}
$selectItemIDHTML .= "</select>\n";


// Submit from form
if ($_POST["submit_flag"] == "YES") {
	$optionString = "<td id=\"tdOption\" class=\"tabletextremark\"><span id=\"spanShowOption\"><a href=\"javascript:showOption();\" class=\"contenttool\"><img src=\"$image_path/$LAYOUT_SKIN/icon_show_option.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">$i_Discipline_Show_Statistics_Option</a></span>";
	$optionString .= "<span id=\"spanHideOption\" style=\"display:none\"><a href=\"javascript:hideOption();\" class=\"contenttool\"><img src=\"$image_path/$LAYOUT_SKIN/icon_hide_option.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">$i_Discipline_Hide_Statistics_Option</a></span></td>";

	$jsRankTargetDetail = implode(',', $rankTargetDetail);
	
	if ($Period == "YEAR") {
		$parPeriodField1 = $selectYear;
		$parPeriodField2 = $selectSemester;
	} else {
		$parPeriodField1 = $textFromDate;
		$parPeriodField2 = $textToDate;
	}
	
	if ($rankTarget == "form") {
		$parByFieldArr = $SelectedFormTextArr;
		
	} else {
		$parByFieldArr = $SelectedClassTextArr;
	}

	if($categoryID==1) {	# "Late"
		$parStatsFieldArr[0] = 0;
		$parStatsFieldTextArr[0] = 'Late';
	} else if($categoryID==2) {	# "Homework not submitted"
		
		$parStatsFieldArr = $SelectedItemIDArr;
		$parStatsFieldTextArr = $SelectedItemIDTextArr;
		
	} else {
		$parStatsFieldArr = $SelectedItemIDArr;
		$parStatsFieldTextArr = $SelectedItemIDTextArr;
	}
	
	### Table Data ###
	$data = $ldiscipline->retrieveAwardPunishReport($Period, $parPeriodField1, $parPeriodField2, $rankTarget, $parByFieldArr, $StatType, $parStatsFieldArr, $parStatsFieldTextArr, $categoryID, $studentID);
	//debug_pr($data);
	################################################
	############ Report Table Content ##############
	
	if($record_type == 1) {
		$i_MeritDemerit[1] = $i_Merit_Merit;
		$i_MeritDemerit[2] = $i_Merit_MinorCredit;
		$i_MeritDemerit[3] = $i_Merit_MajorCredit;
		$i_MeritDemerit[4] = $i_Merit_SuperCredit;
		$i_MeritDemerit[5] = $i_Merit_UltraCredit;
	} else if($record_type == -1) {
		$i_MeritDemerit[0] = $i_Merit_Warning;
		$i_MeritDemerit[-1] = $i_Merit_BlackMark;
		$i_MeritDemerit[-2] = $i_Merit_MinorDemerit;
		$i_MeritDemerit[-3] = $i_Merit_MajorDemerit;
		$i_MeritDemerit[-4] = $i_Merit_SuperDemerit;
		$i_MeritDemerit[-5] = $i_Merit_UltraDemerit;
	}
		
	$itemTotal = array();
	$columnMeritDemeritCount = array();

	$detail_table =  "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">\n";
	$detail_table .= "<tr class=\"tabletext\">";
	$detail_table .= "<td align=\"center\" colspan=\"".(sizeof($parStatsFieldTextArr)+sizeof($i_MeritDemerit)+2)."\">";	
	$detail_table .= $eDiscipline['AwardPunishmentReport'];
	$detail_table .= "</td></tr>";	
	$detail_table .= "<tr class=\"tabletext\">";
	$detail_table .= "<td align=\"right\" colspan=\"".(sizeof($parStatsFieldTextArr)+sizeof($i_MeritDemerit)+2)."\">";	
	if($Period=="YEAR") {
		$termName = ($parPeriodField2==0) ? $i_Discipline_System_Award_Punishment_Whole_Year : $ldiscipline->getTermNameByTermID($parPeriodField2);
		$detail_table .= $ldiscipline->getAcademicYearNameByYearID($parPeriodField1)." ".$termName."\n\n";
	} else {
		$detail_table .= $iDiscipline['Period_Start']." ".$parPeriodField1." ".$iDiscipline['Period_End']." ".$parPeriodField2."\n\n";
	}
	$generationDate = getdate();
	$detail_table .= "<br>".$eDiscipline['ReportGeneratedDate']." ".$generationDate['year']."/".$generationDate['mon']."/".$generationDate['mday']." ".$generationDate['hours'].":".$generationDate['minutes'].":".$generationDate['seconds'];
	$detail_table .= "</td></tr>";	
	$detail_table .= "<tr class=\"tablebluetop\">";
	if($rankTarget=="form") {
		$detail_table .= "<td class=\"tabletop\">".$i_Discipline_Form."</td>";	
	} else if($rankTarget=="class") {
		$detail_table .= "<td class=\"tabletop\">".$i_Discipline_Class."</td>";	
	} else {
		$detail_table .= "<td class=\"tabletop\">".$i_general_name."</td>";	
		$detail_table .= "<td class=\"tabletop\">".$i_ClassNumber."</td>";	
	}
	# header of report table
	for($i=0; $i<sizeof($parStatsFieldTextArr); $i++) {
		$css = (sizeof($parStatsFieldTextArr)==$i+1) ? "seperator_right" : "";
		$detail_table .= "<td class=\"tablebluetop $css tabletopnolink\" align=\"center\">".intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$i]))."</td>";	
	}
	foreach($i_MeritDemerit as $tempMeritDemerit) {
		$detail_table .= "<td align=\"center\" class=\"tablegreentop tabletopnolink\">".$tempMeritDemerit."</td>";	
	}
	$detail_table .= "</tr>";
	
	#content of report table
	$p = 0;
	
if($rankTarget != "student") {
	for($i=0; $i<sizeof($parByFieldArr); $i++) {						# for each class/form
		if($rankTarget=="form") {
			
			$css = ($i%2) ? "2" : "1";
			$detail_table .= "<tr class=\"tablerow$css tabletext\">";
			
			$detail_table .= "<td class=\"tablerow$css tabletext\">".stripslashes($parByFieldArr[$i])."</td>";
			
			for($j=0; $j<sizeof($parStatsFieldArr); $j++) {
				$css = ($i%2==0) ? "" : "2";
				$css2 = (sizeof($parStatsFieldArr)==$j+1) ? "seperator_right" : "seperator_left";
				//$detail_table .= (isset($data[(stripslashes($parStatsFieldArr[$j]))][stripslashes($parByFieldArr[$i])])) ? "<td align=\"center\" class=\"tabletext tablerow_total".$css." ".$css2."\">".$data[(stripslashes($parStatsFieldArr[$j]))][stripslashes($parByFieldArr[$i])]."</td>" : "<td align=\"center\" class=\"tabletext tablerow_total".$css." ".$css2."\">0</td>";
				$detail_table .= (isset($data[$parStatsFieldArr[$j]][stripslashes($parByFieldArr[$i])])) ? "<td align=\"center\" class=\"tabletext tablerow_total".$css." ".$css2."\">".$data[$parStatsFieldArr[$j]][stripslashes($parByFieldArr[$i])]."</td>" : "<td align=\"center\" class=\"tabletext tablerow_total".$css." ".$css2."\">0</td>";
				$itemTotal[$parStatsFieldArr[$j]] += $data[$parStatsFieldArr[$j]][stripslashes($parByFieldArr[$i])];
			}
																				# (YEAR/DATE , Period1, Period2, Class/Form, ClassName/FormName, SelectedItemID, SelectedItemName)
			$meritDemeritCount = $ldiscipline->retrieveMeritDemeritCountFromAP($Period, $parPeriodField1, $parPeriodField2, $rankTarget, $parByFieldArr[$i], $parStatsFieldArr, $parStatsFieldTextArr, $categoryID);

				if($record_type==1) {
					$m = 1;
				} else {
					$m = 0;
				}
				foreach($i_MeritDemerit as $columnNo) {
					$tempCount = 0;
					for($n=0; $n<sizeof($meritDemeritCount); $n++) {
						if($meritDemeritCount[$n]['ProfileMeritType']==$m)
							$tempCount += $meritDemeritCount[$n]['countTotal'];
					}
					$detail_table .= "<td align=\"center\">";
					$detail_table .= ($tempCount != 0) ? $tempCount : "0";
					$columnMeritDemeritCount[$m] += $tempCount;
					$detail_table .= "</td>";
					
					if($record_type==1) {
						$m++;
					} else {
						$m--;
					}
				}
				
			$detail_table .= "</tr>";
			
		} else {
			
				$css = ($p%2) ? "2" : "1";
				$detail_table .= "<tr class=\"tablerow$css tabletext\">";
				$detail_table .= "<td class=\"tablerow$css tabletext\">".$parByFieldArr[$i]."</td>";
				
				for($j=0; $j<sizeof($parStatsFieldArr); $j++) {
					$css = ($p%2==0) ? "" : "2";
					$css2 = (sizeof($parStatsFieldArr)==$j+1) ? "seperator_right" : "seperator_left";
					$detail_table .= (isset($data[$parStatsFieldArr[$j]][$parByFieldArr[$i]])) ? "<td align=\"center\" class=\"tabletext tablerow_total".$css." ".$css2."\">".$data[$parStatsFieldArr[$j]][$parByFieldArr[$i]]."</td>" : "<td align=\"center\" class=\"tabletext tablerow_total".$css." ".$css2."\">0</td>";
					$itemTotal[$parStatsFieldArr[$j]] += $data[$parStatsFieldArr[$j]][$parByFieldArr[$i]];
				}
				$p++;
				
				$meritDemeritCount = $ldiscipline->retrieveMeritDemeritCountFromAP($Period, $parPeriodField1, $parPeriodField2, $rankTarget, $parByFieldArr[$i], $parStatsFieldArr, $parStatsFieldTextArr, $categoryID);
			
				if($record_type==1) {
					$m = 1;
				} else {
					$m = 0;
				}
				foreach($i_MeritDemerit as $columnNo) {
					$tempCount = 0;
					for($n=0; $n<sizeof($meritDemeritCount); $n++) {
						if($meritDemeritCount[$n]['ProfileMeritType']==$m)
							$tempCount += $meritDemeritCount[$n]['countTotal'];
					}
					$detail_table .= "<td align=\"center\">";
					$detail_table .= ($tempCount != 0) ? $tempCount : "0";
					$columnMeritDemeritCount[$m] += $tempCount;
					$detail_table .= "</td>";
					
					if($record_type==1) {
						$m++;
					} else {
						$m--;
					}
				}

				$detail_table .= "</tr>";
		}
		
	}
} else {		# rankTarget = student
			for($k=0; $k<sizeof($studentID); $k++) {
				$stdInfo = $ldiscipline->getStudentNameByID($studentID[$k]);
				
				$css = ($p%2) ? "2" : "1";
				$detail_table .= "<tr class=\"tablerow$css tabletext\">";
				$detail_table .= "<td class=\"tablerow$css tabletext\">".$stdInfo[0][1]."</td>";
				$detail_table .= "<td class=\"tablerow$css tabletext\">".$stdInfo[0][2]." - ".$stdInfo[0][3]."</td>";
				
				for($j=0; $j<sizeof($parStatsFieldArr); $j++) {
					$css = ($p%2==0) ? "" : "2";
					$css2 = (sizeof($parStatsFieldArr)==$j+1) ? "seperator_right" : "seperator_left";
					
					$detail_table .= (isset($data[$parStatsFieldArr[$j]][$stdInfo[0][2]][$stdInfo[0][3]])) ? "<td align=\"center\" class=\"tabletext tablerow_total".$css." ".$css2."\">".$data[$parStatsFieldArr[$j]][$stdInfo[0][2]][$stdInfo[0][3]]."</td>" : "<td align=\"center\" class=\"tabletext tablerow_total".$css." ".$css2."\">0</td>";
					$itemTotal[$parStatsFieldArr[$j]] += $data[$parStatsFieldArr[$j]][$stdInfo[0][2]][$stdInfo[0][3]];
				}
				$p++;
				
				$meritDemeritCount = $ldiscipline->retrieveMeritDemeritCountFromAP($Period, $parPeriodField1, $parPeriodField2, $rankTarget, $stdInfo[$k][2], $parStatsFieldArr, $parStatsFieldTextArr, $categoryID, $studentID[$k]);
			
				if($record_type==1) {
					$m = 1;
				} else {
					$m = 0;
				}
				foreach($i_MeritDemerit as $columnNo) {
					$tempCount = 0;
					for($n=0; $n<sizeof($meritDemeritCount); $n++) {
						if($meritDemeritCount[$n]['ProfileMeritType']==$m)
							$tempCount += $meritDemeritCount[$n]['countTotal'];
					}
					$detail_table .= "<td align=\"center\">";
					$detail_table .= ($tempCount != 0) ? $tempCount : "0";
					$columnMeritDemeritCount[$m] += $tempCount;
					$detail_table .= "</td>";
					
					if($record_type==1) {
						$m++;
					} else {
						$m--;
					}
				}

				$detail_table .= "</tr>";
			}
	
}
	
	$detail_table .= "</tr>";
	
	$detail_table .= "<tr><td colspan=\"".(sizeof($parStatsFieldTextArr)+sizeof($i_MeritDemerit)+2)."\"><table cellspacing=0 cellpadding=0 width=100%><tr><td class=\"tablebluetop\"><img src=\"{$image_path}/{$LAYOUT_SKIN}\"/10x10.gif\" width=\"100%\" height=\"1\"></td></tr></table></td>";
	$detail_table .= "<tr><td>".$i_Discipline_System_Report_Class_Total."</td>";
	if($rankTarget=="student") {
		$detail_table .= "<td>&nbsp;</td>";
	}


	for($i=0; $i<sizeof($parStatsFieldArr); $i++) {
		$css = (sizeof($parStatsFieldArr)==$i+1) ? "class=\"seperator_right\"" : "class=\"seperator_left\"";
		//$detail_table .= (isset($itemTotal[intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$i]))])) ? "<td align=\"center\" $css>".$itemTotal[intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$i]))]."</td>" : "<td align=\"center\" $css>0</td>";
		$detail_table .= (isset($itemTotal[(stripslashes($parStatsFieldArr[$i]))])) ? "<td align=\"center\" $css>".$itemTotal[(stripslashes($parStatsFieldArr[$i]))]."</td>" : "<td align=\"center\" $css>0</td>";
	}
	
	if($record_type==1) {
		$m = 1;
	} else {
		$m = 0;
	}
	foreach($i_MeritDemerit as $columnNo) {
		$detail_table .= "<td align=\"center\">";
		$detail_table .= $columnMeritDemeritCount[$m];
		
		if($record_type==1) {
			$m++;
		} else {
			$m--;
		}
		$detail_table .= "</td>";
	}
	
	
	$detail_table .= "</tr>";
	
	$detail_table .=  "</table>\n";
	
	$tableButton .=  "<tr><td class=\"dotline\"><img src=\"{$image_path}/{$LAYOUT_SKIN}\"/10x10.gif\" width=\"10\" height=\"1\"></td></tr>";
	$tableButton .=  "<tr>";
	$tableButton .=  "<td align=\"center\">";
	$tableButton .=  $linterface->GET_ACTION_BTN($button_export, "submit", "document.form1.action='index_export.php';document.form1.target='_self';return checkForm();")."&nbsp;";
	$tableButton .=  $linterface->GET_ACTION_BTN($button_print, "submit", "document.form1.action='index_print.php';document.form1.target='_blank';return checkForm();")."&nbsp;";
	$tableButton .=  $linterface->GET_ACTION_BTN($button_back, "button", "self.location='index.php'")."&nbsp;";
	$tableButton .=  "</td>";
	$tableButton .=  "</tr>";
	
	############ End of Report Table ###############
	################################################
	
	
	$initialString = "<script language=\"javascript\">hideOption();</script>\n";
} else {
	$optionString = "<td height=\"20\" class=\"tabletextremark\"><em>- $i_Discipline_Statistics_Options -</em></td>";
	$initialString = "<script language=\"javascript\">jPeriod='YEAR';hideSpan('spanMisCat');showSpan('spanGoodCat');changeCat('0');document.getElementById('selectGoodCat').value='0';</script>\n";
}

$linterface->LAYOUT_START();
?>
<script language="javascript">
var xmlHttp3

function changeTerm(val) {

	if (val.length==0)
	{ 
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
		
	xmlHttp3 = GetXmlHttpObject()
	
	if (xmlHttp3==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "../ajaxGetSemester.php";
	url += "?year=" + val;
	url += "&term=<?=$selectSemester?>";
	url += "&field=selectSemester";
	
	xmlHttp3.onreadystatechange = stateChanged3 
	xmlHttp3.open("GET",url,true)
	xmlHttp3.send(null)
} 

function stateChanged3() 
{ 
	if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete")
	{ 
		document.getElementById("spanSemester").innerHTML = xmlHttp3.responseText;
		document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
	} 
}

var xmlHttp

function showResult(str, choice)
{
	if(str != "student2ndLayer")
		document.getElementById("studentFlag").value = 0;
	
	if (str.length==0)
		{ 
			document.getElementById("rankTargetDetail").innerHTML = "";
			document.getElementById("rankTargetDetail").style.border = "0px";
			return
		}
		
	if(document.getElementById('rankTarget').value=='form' || document.getElementById('rankTarget').value=='class') 
	{
		document.getElementById('selectAllBtn01').disabled = false;			
		document.getElementById('selectAllBtn01').style.visibility = 'visible';
	}
		
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 
		
	url = "get_live.php";
	url = url + "?target=" + str + "&rankTargetDetail=" + choice;
	url += "&year="+document.getElementById('selectYear').value;
	//url += (document.getElementById("studentFlag").value==1) ? "&student=1&value="+document.getElementById('rankTargetDetail[]').value+"&rankTargetDetail="+document.getElementById('rankTargetDetail[]').value : "";
	url += "&student=1&value="+document.getElementById('rankTargetDetail[]').value;
	<? if($studentID != '') { 
		echo "url += \"&studentid=\" + ".implode(',',$studentID).";";
	} ?>
	//url += (document.getElementById("studentFlag").value==1) ? "&student=1&value="+document.getElementById('rankTargetDetail[]').value : "";
		
	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
	//SelectAll(form1.elements['rankTargetDetail[]']);
} 

function stateChanged() 
{ 
	
	var showIn = "";
	if(document.getElementById("studentFlag").value == 0) {
		showIn = "rankTargetDetail";
	} else {
		showIn = "spanStudent";	
	}
	
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById(showIn).innerHTML = xmlHttp.responseText;
		document.getElementById(showIn).style.border = "0px solid #A5ACB2";
		
	} 
}


function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

function studentSelection(val) {
	var rank = document.getElementById('rankTargetDetail[]');
	for(i=0; i<rank.length; i++) {
		if(rank.options[i].value == val) {
			rank.options[i].selected = true;	
		} else {
			rank.options[i].selected = false;	
		}
	}
}
</script>

<script language="javascript">
var jPeriod="<?=$Period?>";
var jStatType="<?=$StatType?>";

// Generate Good Conduct Category
var good_cat_select = new Array();
good_cat_select[0] = new Option("-- <?=$eDiscipline["SelectRecordCategory2"]?> --",0);
<?	foreach($award_category as $k=>$d)	{ ?>
		good_cat_select[<?=$k+1?>] = new Option("<?=str_replace('"', '\"', $d['Name'])?>",<?=$d['CategoryID']?>);
<?	} ?>

// Generate Misconduct Category
var mis_cat_select = new Array();
mis_cat_select[0] = new Option("-- <?=$eDiscipline["SelectRecordCategory2"]?> --",0);
<?	foreach($punishment_category as $k=>$d)	{ ?>
		mis_cat_select[<?=$k+1?>] = new Option("<?=str_replace('"', '\"', $d['Name'])?>",<?=$d['CategoryID']?>);
<?	} ?>

function changeCat(value)
{
	var item_select = document.getElementById('ItemID[]');
	var selectedCatID = value;
	var record_type_good = document.getElementById('record_type_good');

	while (item_select.options.length > 0)
	{
		item_select.options[0] = null;
	}
	
	if (selectedCatID == 0) {
	<?
		$curr_cat_id = "";
	?>
		if(document.form1.record_type_good.checked) {
	<?
		$pos = 0;
			for ($i=0; $i<sizeof($award_items); $i++)
			{
				list($r_catID, $r_itemID, $r_itemName) = $award_items[$i];
				$r_itemName = intranet_undo_htmlspecialchars($r_itemName);
				$r_itemName = str_replace('"', '\"', $r_itemName);
				$r_itemName = str_replace("'", "\'", $r_itemName);
				echo "item_select.options[".$pos++."] = new Option(\"".$r_itemName."\",".$r_itemID.");\n";
			}
	?>	
		} else {
	<?
			$pos = 0;
			for ($i=0; $i<sizeof($punishment_items); $i++)
			{
				list($r_catID, $r_itemID, $r_itemName) = $punishment_items[$i];
				$r_itemName = intranet_undo_htmlspecialchars($r_itemName);
				$r_itemName = str_replace('"', '\"', $r_itemName);
				$r_itemName = str_replace("'", "\'", $r_itemName);
				echo "item_select.options[".$pos++."] = new Option(\"".$r_itemName."\",".$r_itemID.");\n";
			}
	?>	
		}
	}
	else if (selectedCatID == '')
	{

	<? 
		$curr_cat_id = "";
		$pos = 0;
		for ($i=0; $i<sizeof($items); $i++)
		{
			list($r_catID, $r_itemID, $r_itemName) = $items[$i];
			$r_itemName = intranet_undo_htmlspecialchars($r_itemName);
			$r_itemName = str_replace('"', '\"', $r_itemName);
			$r_itemName = str_replace("'", "\'", $r_itemName);
			if ($r_catID != $curr_cat_id)
			{
				$pos = 0;
	?>
	}
	else if (selectedCatID == "<?=$r_catID?>")
	{
	<?
				$curr_cat_id = $r_catID;
			}
	?>
		item_select.options[<?=$pos++?>] = new Option("<?=$r_itemName?>",<?=$r_itemID?>);
		<?	if($r_itemID == $ItemID) {	?>
		item_select.selectedIndex = <?=$pos-1?>;
		<? 	} 
		}
	?>
	}
	SelectAll(form1.elements['ItemID[]']);
}

function showSpan(span) {
	document.getElementById(span).style.display="inline";
	if(span == 'spanStudent') {
		//form1.elements['rankTargetDetail[]'].multiple = false;
		document.getElementById('selectAllBtn01').disabled = true;
		document.getElementById('selectAllBtn01').style.visibility = 'hidden';
		var temp = document.getElementById('studentID[]');
		
		while(temp.options.length>0) {
			temp.options[0] = null;	
		}
	} 
}

function hideSpan(span) {
	document.getElementById(span).style.display="none";
	if(span=='spanStudent') {
		//SelectAll(document.getElementById('rankTargetDetail[]'));
		document.getElementById('selectAllBtn01').disabled = false;
		document.getElementById('selectAllBtn01').style.visibility = 'visible';
	}
}

function showOption(){
	hideSpan('spanShowOption');
	showSpan('spanHideOption');
	showSpan('spanOptionContent');
	document.getElementById('tdOption').className = '';
}

function hideOption(){
	hideSpan('spanOptionContent');
	hideSpan('spanHideOption');
	showSpan('spanShowOption');
	document.getElementById('tdOption').className = 'report_show_option';
}

function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function SelectAllItem(obj, flag) {
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = flag;
	}
}

function getSelectedString(obj, targetObj){
	var tmpString = '';
	for (i=0; i<obj.length; i++){
		if (obj.options[i].selected == true){
			tmpString += obj.options[i].value + ',';
		}
	}
	if (tmpString.length > 0){
		document.getElementById(targetObj).value = tmpString.substr(0, tmpString.length-1);
	} else {
		document.getElementById(targetObj).value = tmpString;
	}
}

function getSelectedTextString(obj, targetObj){
	var tmpString = '';
	for (i=0; i<obj.length; i++){
		if (obj.options[i].selected == true){
			tmpString += obj.options[i].text + ',';
		}
	}
	if (tmpString.length > 0){
		document.getElementById(targetObj).value = tmpString.substr(0, tmpString.length-1);
	} else {
		document.getElementById(targetObj).value = tmpString;
	}
}

function doPrint()
{
	document.form1.action = "index_print.php";
	document.form1.target = "_blank";
	document.form1.submit();
	document.form1.action = "";
	document.form1.target = "_self";
}

function doExport()
{
	document.form1.action = "index_export.php";
	document.form1.submit();
	document.form1.action = "";
}

function changeRadioSelection(type)
{
	var jsYearRadioObj = document.getElementById("radioPeriod_Year");
	var jsDateRadioObj = document.getElementById("radioPeriod_Date");
	
	if (type == "YEAR")
	{
		jsYearRadioObj.checked = true;
		jsDateRadioObj.checked = false;
		jPeriod = type;
	}
	else
	{
		jsYearRadioObj.checked = false;
		jsDateRadioObj.checked = true;
		jPeriod = type;
	}
}

function checkForm(){
	var choiceSelected;
	var choice = "";
	
	if (jPeriod=='DATE' && document.getElementById('textFromDate').value=='') {
		alert('<?=$i_alert_pleasefillin.$i_general_startdate?>');
		return false;
	}
	if (jPeriod=='DATE' && document.getElementById('textToDate').value=='') {
		alert('<?=$i_alert_pleasefillin.$i_general_enddate?>');
		return false;
	}
	if(jPeriod=='DATE' && !check_date(document.getElementById('textFromDate'), '<?=$i_invalid_date?>')) {
		return false;
	}
	if(jPeriod=='DATE' && !check_date(document.getElementById('textToDate'), '<?=$i_invalid_date?>')) {
		return false;
	}
	if(jPeriod=='DATE' && (document.form1.textFromDate.value > document.form1.textToDate.value)) {
		alert("<?=$i_invalid_date?>");
		return false;	
	}
	if (document.getElementById('rankTarget').value=='#') {
		alert("<?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?>");	
		return false;
	}	
	if(choiceSelected==0) {
		alert("<?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?>");
		return false;
	}	 	
	if (document.getElementById('rankTarget').value=='form' && countOption(document.getElementById('rankTargetDetail[]'))<1) {
		alert('<?=$i_alert_pleaseselect.$i_Discipline_Form?>');
		return false;
	}
	if (document.getElementById('rankTarget').value=='class' && countOption(document.getElementById('rankTargetDetail[]'))<1) {
		alert('<?=$i_alert_pleaseselect.$i_Discipline_Class?>');
		return false;
	}
	if(document.getElementById('rankTarget').value == "student") {
		choiceSelected = 0;
		choice = document.getElementById('studentID[]');
	
		for(i=0;i<choice.options.length;i++) {
			if(choice.options[i].selected==true)
				choiceSelected = 1;
		}
		if(choiceSelected==0) {
			alert("<?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?>");	
			return false;
		}
	}

	choice = document.getElementById('rankTargetDetail[]');
	choiceSelected = 0;
	
	for(i=0;i<choice.options.length;i++) {
		if(choice.options[i].selected==true)
			choiceSelected = 1;
	}
	if (document.getElementById('selectMisCat').value!=1 && document.getElementById('selectMisCat').value!=2 && countOption(document.getElementById('ItemID[]'))<1) {
		alert('<?=$i_alert_pleaseselect.$i_Discipline_Award_Punishment_Titles?>');
		return false;
	}
	document.getElementById('Period').value = jPeriod;
	document.getElementById('StatType').value = jStatType;
	getSelectedString(document.getElementById('rankTargetDetail[]'), 'SelectedForm');
	getSelectedTextString(document.getElementById('rankTargetDetail[]'), 'SelectedFormText');
	getSelectedString(document.getElementById('rankTargetDetail[]'), 'SelectedClass');
	getSelectedTextString(document.getElementById('rankTargetDetail[]'), 'SelectedClassText');
	getSelectedString(document.getElementById('ItemID[]'), 'SelectedItemID');
	getSelectedTextString(document.getElementById('ItemID[]'), 'SelectedItemIDText');
	document.getElementById('submit_flag').value='YES';
	return true;
}
</script>

<form name="form1" method="post" action="index.php">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr align="left">
					<td width="50">&nbsp;</td>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr align="left" <? if($_POST["submit_flag"]=="YES") echo "class=\"report_show_option\""; ?>>
								<?=$optionString?>
							</tr>
						</table>
						<span id="spanOptionContent">
						<table width="100%" border="0" cellpadding="5" cellspacing="0" <? if($_POST["submit_flag"]=="YES") echo "class=\"report_show_option\""; ?>>
							<tr valign="top">
								<td height="57" width="10%" valign="top" nowrap="nowrap" class="formfieldtitle">
									<?=$iDiscipline['Period']?> <span class="tabletextrequire">*</span>
								</td>
								<td width="90%">
									<table border="0" cellspacing="0" cellpadding="3">
										<tr>
											<td height="30" colspan="6" onClick="document.form1.radioPeriod_Year.checked=true">
												<input name="radioPeriod" type="radio" id="radioPeriod_Year" value="YEAR" onClick="javascript:jPeriod=this.value"<? echo ($Period!="DATE")?" checked":"" ?>>
												<?=$i_Discipline_School_Year?>
												<?=$selectSchoolYearHTML?>&nbsp;
												<?=$i_Discipline_Semester?>
												<span id="spanSemester"><?=$selectSemesterHTML?></span>
											</td>
										</tr>
										<tr>
											<td><input name="radioPeriod" type="radio" id="radioPeriod_Date" value="DATE" onClick="javascript:jPeriod=this.value"<? echo ($Period=="DATE")?" checked":"" ?>> <?=$i_From?> </td>
											<td onClick="changeRadioSelection('DATE')" onFocus="changeRadioSelection('DATE')"><?=$linterface->GET_DATE_PICKER("textFromDate",$textFromDate)?><?/*=$linterface->GET_DATE_FIELD2("textFromDateSpan", "form1", "textFromDate", $textFromDate, 1, "textFromDate", "onclick=\"changeRadioSelection('DATE')\"")*/?>&nbsp;</td>
											<td> <?=$i_To?> </td>
											<td onClick="changeRadioSelection('DATE')" onFocus="changeRadioSelection('DATE')"><?=$linterface->GET_DATE_PICKER("textToDate",$textToDate)?><?/*=$linterface->GET_DATE_FIELD2("textToDateSpan", "form1", "textToDate", $textToDate, 1, "textToDate", "onclick=\"changeRadioSelection('DATE')\"")*/?>&nbsp;</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr valign="top">
								<td valign="top" nowrap="nowrap" class="formfieldtitle">
									<?=$i_Discipline_Target?> <span class="tabletextrequire">*</span>
								</td>
								<td>
									<table width="0%" border="0" cellspacing="2" cellpadding="0">
										<tr>
											<td valign="top">
												<table border="0">
													<tr>
														<td valign="top">
															<select name="rankTarget" id="rankTarget" onChange="showResult(this.value,'');if(this.value=='student') {showSpan('spanStudent');} else {hideSpan('spanStudent');}">
																<option value="#">-- <?=$i_general_please_select?> --</option>
																<option value="form" <? if($rankTarget=="form") { echo "selected";} ?>><?=$i_Discipline_Form?></option>
																<option value="class" <? if($rankTarget=="class") { echo "selected";} ?>><?=$i_Discipline_Class?></option>
																<option value="student" <? if($rankTarget=="student") { echo "selected";} ?>><?=$i_UserStudentName?></option>
															</select>
														</td>
														<td>
															<div id='rankTargetDetail' style='position:absolute; width:280px; height:0px; z-index:0;'></div>
															<select name="rankTargetDetail[]" id="rankTargetDetail[]" size="5" id="rankTargetDetail[]"></select>
															<br><?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('rankTargetDetail[]'));return false;", "selectAllBtn01")?>
															<br>
															<div id='spanStudent' style='position:relative; width:280px; height:80px; z-index:1;<? if($rankTarget=="student") { echo "display:inline;";} else { echo "display:none;";} ?>'>
																<select name="studentID[]" id="studentID[]" multiple size="5"></select>
																<br><?= $linterface->GET_BTN($button_select_all, "button", "SelectAllItem(document.getElementById('studentID[]'), true);return false;")?>
															</div>
														</td>
													</tr>
													<tr><td colspan="3" class="tabletextremark">(<?=$i_Discipline_Press_Ctrl_Key?>)</td></tr>
												</table>
											</td>
											<td valign="top"><br /></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle">
									<span><?=$iDiscipline['RecordType2']?> <span class="tabletextrequire">*</span></span>
								</td>
								<td width="85%">
									<table width="0%" border="0" cellspacing="2" cellpadding="0">
										<tr>
											<td valign="top">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td>
															<input name="record_type" type="radio" id="record_type_good" value="1" <?=($record_type==1 || $record_type=="")?"checked":""?> onClick="hideSpan('spanMisCat');showSpan('spanGoodCat');changeCat('0');document.getElementById('selectGoodCat').value='0';"><label for="record_type_good"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ediscipline/icon_gd_conduct.gif" width="20" height="20" border="0" align="absmiddle"> <?=$i_Merit_Award?></label>
															<input name="record_type" type="radio" id="record_type_mis" value="-1" <?=($record_type==-1)?"checked":""?> onClick="hideSpan('spanGoodCat');showSpan('spanMisCat');changeCat('0');document.getElementById('selectMisCat').value='0';"><label for="record_type_mis"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ediscipline/icon_misconduct.gif" width="20" height="20" border="0" align="absmiddle"> <?=$i_Merit_Punishment?></label><br />
															<span id="spanGoodCat"<? echo ($record_type==1 || $record_type=="")?"":" style=\"display:none\"" ?>>
																<?=$selectGoodCatHTML?>
															</span>
															<span id="spanMisCat"<? echo ($record_type==-1)?"":" style=\"display:none\"" ?>>
																<?=$selectMisCatHTML?>
															</span>
														</td>
													</tr>
													<tr>
														<td>
															<?=$selectItemIDHTML?>
															<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('ItemID[]')); return false;") ?>
														</td>
													</tr>
													<tr>
														<td>
															<span class="tabletextremark">(<?=$i_Discipline_Press_Ctrl_Key?>)</span>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<input type="hidden" name="selectShowOnlyType" id="selectShowOnlyType" value="ALL">
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap">
									<span class="tabletextremark"><?=$i_general_required_field?></span>
								</td>
								<td>&nbsp;</td>
							</tr>
<!--							<tr>
								<td height="1" class="dotline" colspan="2">
									<img src="<?="$image_path/$LAYOUT_SKIN"?>/10x10.gif" width="10" height="1"></td>
							</tr>//-->
							<tr>
								<td>&nbsp;</td>
								<td><?= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Statistic, "submit", "document.form1.action='index.php';document.form1.target='_self';return checkForm();", "submitBtn01")?>
								</td>
							</tr>
						</table>
						</span>
					</td>
				</tr>
			</table>
			<br />
				<?=$detail_table?>
			<br />
			
		</td>
	</tr>
	<?=$tableButton?>
</table>

<input type="hidden" name="submit_flag" id="submit_flag" value="NO" />
<input type="hidden" name="categoryID" id="categoryID" value="<?=$categoryID?>" />
<input type="hidden" name="Period" id="Period" value="<?=$Period?>" />
<input type="hidden" name="submitSelectBy" id="submitSelectBy" value="<?=$rankTarget?>" />
<input type="hidden" name="StatType" id="StatType" value="<?=$StatType?>" />
<input type="hidden" name="PeriodField1" id="PeriodField1" value="<?=$parPeriodField1?>" />
<input type="hidden" name="PeriodField2" id="PeriodField2" value="<?=$parPeriodField2?>" />
<? if($submit_flag=="YES") { ?>
<input type="hidden" name="ByField" id="ByField" value="<?=implode(",", $parByFieldArr)?>" />
<input type="hidden" name="StatsField" id="StatsField" value="<?=implode(",", $parStatsFieldArr)?>" />
<input type="hidden" name="StatsFieldText" id="StatsFieldText" value="<?=intranet_htmlspecialchars(stripslashes(implode(",", $parStatsFieldTextArr)))?>" />
<input type="hidden" name="newItemID" id="newItemID" value="<?=implode(",", $parStatsFieldArr)?>">
<? } ?>
<input type="hidden" name="SelectedForm" id="SelectedForm" />
<input type="hidden" name="SelectedFormText" id="SelectedFormText" />
<input type="hidden" name="SelectedClass" id="SelectedClass" />
<input type="hidden" name="SelectedClassText" id="SelectedClassText" />
<input type="hidden" name="SelectedItemID" id="SelectedItemID" />
<input type="hidden" name="SelectedItemIDText" id="SelectedItemIDText" />
<input type="hidden" name="id" id="id" value="">
<input type="hidden" name="itemNameColourMappingArr" id="itemNameColourMappingArr" value="<?=rawurlencode(serialize($itemNameColourMappingArr));?>">
<input type="hidden" name="studentFlag" id="studentFlag" value="0">			

</form>
<br />
<?=$initialString?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic/js/swfobject.js"></script>
<script language="javascript">
<!--

function init()
{
	var obj = document.form1;
	
	obj.rankTarget.selectedIndex=1;		// Form
	showResult("form","");
}
<?
if($submitBtn01 == "")
	echo "init();\n";
?>

//-->
</script>
<script language="javascript">
<!--
<?
if($submitBtn01 != "") {
	echo "showResult(\"$rankTarget\", \"$jsRankTargetDetail\");";
}
?>
<? if(sizeof($studentID)>0) { ?>
	
initialStudent();	
	
function initialStudent() {
	xmlHttp2 = GetXmlHttpObject()
	url = "get_live.php?target=student2ndLayer&value=<?=implode(',',$rankTargetDetail)?>&rankTargetDetail=<?=implode(',',$rankTargetDetail)?>&student=1&studentid=<? if(sizeof($studentID)>0) echo implode(',',$studentID);?>";
	xmlHttp2.onreadystatechange = stateChanged2
	xmlHttp2.open('GET',url,true);
	xmlHttp2.send(null);
	
	document.getElementById('selectAllBtn01').disabled = true;
	document.getElementById('selectAllBtn01').style.visibility = 'hidden';
}
<? } ?>

function stateChanged2() 
{ 
	if (xmlHttp2.readyState==4 || xmlHttp2.readyState=="complete")
	{ 
		document.getElementById('spanStudent').innerHTML = xmlHttp2.responseText;
		document.getElementById('spanStudent').style.border = "0px solid #A5ACB2";
		
		//SelectAll(form1.elements['studentID[]']);
	} 
}

changeTerm('<?=$selectYear?>');

//-->
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();

?>