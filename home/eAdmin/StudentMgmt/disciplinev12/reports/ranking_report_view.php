<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");

intranet_auth();
intranet_opendb();


$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lstudentprofile = new libstudentprofile();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-RankingReport-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

if($rankTarget == '') {
	header("Location: ranking_report.php");	
}

# School Year Menu #

$selectSchoolYear .= "<option value='0'";
$selectSchoolYear .= ($SchoolYear==0) ? " selected" : "";
$selectSchoolYear .= ">".$i_Discipline_System_Award_Punishment_All_School_Year."</option>";
$selectSchoolYear .= $ldiscipline->getConductSchoolYear($SchoolYear);

$conds .= " AND a.RecordStatus=".DISCIPLINE_STATUS_APPROVED;

# merit Type
if($meritType==1) {
	$meritTitle = $iDiscipline['Awarded'];	
	$conds .= " AND a.MeritType=1";
} else {
	$meritTitle = $iDiscipline['Punished'];	
	$conds .= " AND a.MeritType=-1";
}

$semesterText = ($semester=='WholeYear') ? $i_Discipline_System_Award_Punishment_Whole_Year : $semester;

# Semester Menu #
$semester_data = split("\n",get_file_content("$intranet_root/file/semester.txt"));
$SemesterMenu .= "<option value='WholeYear'";
$SemesterMenu .= ($semester1 != 'WholeYear') ? "" : " selected";
$SemesterMenu .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";
for ($i=0; $i<sizeof($semester_data); $i++)
{
	$target_sem = $semester_data[$i];
	$line = split("::",$target_sem);
	list ($name,$current) = $line;
	$SemesterMenu .= "<option value='".$name."'";
	$SemesterMenu .= ($name==$semester) ? " selected" : "";
	$SemesterMenu .= ">".$name."</option>";
}

//----
if($dateChoice==2) {
	$date = $startDate ." {$i_To} ".$endDate;
	$conds .= " AND a.RecordDate BETWEEN '$startDate' AND '$endDate'";
} else {
	if($SchoolYear != '0') {		# specific school year (not "All School Year")
		$date = $SchoolYear." ".$semesterText;
		list($startYear, $endYear) = split('-',$SchoolYear);
		$conds .= " AND a.Year='$SchoolYear'";
	} else {						# all school year
		$date = $i_Discipline_System_Award_Punishment_All_School_Year." ".$semesterText;
		$academicYear = $ldiscipline->generateAllSchoolYear();
		$tempConds = "";
	}
	$conds .= ($semester != 'WholeYear') ? " AND a.Semester='$semester'" : "";
}
	

# Ranking Range
for($i=1;$i<=20;$i++) {
	$rankRangeMenu .= "<option value=$i";
	$rankRangeMenu .= ($i==$rankRange) ? " SELECTED" : "";
	$rankRangeMenu .= ">$i</option>";
}

# Rank Target 
$rankTargetMenu .= "<select name='rankTarget' onChange=\"showResult(this.value,'')\">";
$rankTargetMenu .= "<option value='#'>-- $i_general_please_select --</option>";
$rankTargetMenu .= "<option value='form'";
$rankTargetMenu .= ($rankTarget=='form') ? " selected" : "";
$rankTargetMenu .= ">$i_Discipline_Form</option>";
$rankTargetMenu .= "<option value='class'";
$rankTargetMenu .= ($rankTarget=='class') ? " selected" : "";
$rankTargetMenu .= ">$i_Discipline_Class</option>";
$rankTargetMenu .= "</select>";

# form / class

if(is_array($rankTargetDetail)) {
	$rankTargetDetail = $rankTargetDetail;
}
else {
	$rankTargetDetail[0] = $rankTargetDetail;
}

$tempConds = "";
if($rankTarget=='form') {
	for($i=0;$i<sizeof($rankTargetDetail);$i++) {
		$tempConds .= ($tempConds!="") ? " OR " : "";
//		$tempConds .= " b.ClassName LIKE '$rankTargetDetail[$i]%'";
		$tempConds .= " d.ClassLevelID=$rankTargetDetail[$i]";
	}
} else if($rankTarget=='class') {
	for($i=0;$i<sizeof($rankTargetDetail);$i++) {
		$tempConds .= ($tempConds!="") ? " OR " : "";
		$tempConds .= " b.ClassName='$rankTargetDetail[$i]'";
	}
}
$conds .= " AND ($tempConds)";
$jsRankTargetDetail = implode(',', $rankTargetDetail);

# table content
if($rankTarget=='form') {
	$sql = "SELECT
				d.LevelName as form,
				COUNT(*) as total,
				b.ClassName as name
			FROM
				DISCIPLINE_MERIT_RECORD as a
				LEFT OUTER JOIN
					INTRANET_USER as b ON (a.StudentID=b.UserID)
				LEFT OUTER JOIN
					INTRANET_CLASS as c ON (b.ClassName=c.ClassName)
				LEFT OUTER JOIN 
					INTRANET_CLASSLEVEL as d ON (c.ClassLevelID=d.ClassLevelID)
			WHERE
				a.DateInput IS NOT NULL
				$conds
				GROUP BY form
				ORDER BY total DESC
				LIMIT $rankRange
			";
} else {
	$name_field = getNameFieldByLang('b.');
	$sql = "SELECT
				b.ClassName,
				COUNT(*) as total,
				$name_field as name
			FROM
				DISCIPLINE_MERIT_RECORD as a
				LEFT OUTER JOIN
					INTRANET_USER as b ON (a.StudentID=b.UserID)
			WHERE
				a.DateInput IS NOT NULL
				$conds
				GROUP BY b.ClassName
				ORDER BY total DESC
				LIMIT $rankRange
			";
}
//echo $sql;
$result = $ldiscipline->returnArray($sql,3);

$criteria = ($rankTarget=='class') ? $i_Discipline_Class : $i_Discipline_Form;

$tableContent .= "<table width='100%' border='0' cellpadding='4' cellspacing='1' bgcolor='#CCCCCC'>";
$tableContent .= "<tr class='tablebluetop'>";
$tableContent .= "<td><a class='tabletoplink'>{$iDiscipline['Rank']}</a></td>";
$tableContent .= "<td><a class='tabletoplink'>{$criteria} </a></span></td>";
$tableContent .= "<td><a class='tabletoplink'>{$i_Discipline_No_of_Records}</a></td>";
$tableContent .= "</tr>";

if(sizeof($result)==0) {
	$tableContent .= "<tr class='tablebluerow1'>";
	$tableContent .= "<td class='tabletext' colspan='3' height='40' align='center'>{$i_no_record_exists_msg}</td>";
	$tableContent .= "</tr>";
}

for($i=0;$i<sizeof($result);$i++) {
	$k = $i+1;
	$css = ($i%2)+1;
	$tableContent .= "<tr class='tablebluerow{$css}'>";
	$tableContent .= "<td class='tabletext'>{$k}</td>";
	$tableContent .= "<td >";
	//$tableContent .= ($rankTarget=='form') ? "Form " : "";
	$tableContent .= "{$result[$i][0]}</td>";
	$tableContent .= "<td >{$result[$i][1]}</td>";
	$tableContent .= "</tr>";
}
$tableContent .= "</table>";
#

# Class #
$select_class = $ldiscipline->getSelectClassWithWholeForm("name=\"targetClass\" onChange=\"showResult(this.value)\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes, $i_general_please_select);

$reportOptionContent .= "<table width='88%' border='0' cellpadding='5' cellspacing='0'>";
$reportOptionContent .= "<tr><td>";
$reportOptionContent = "<table align='center' width='100%' border='0' cellpadding='5' cellspacing='0'>";
$reportOptionContent .= "<tr valign='top'>";
$reportOptionContent .= "<td height='57' valign='top' nowrap='nowrap' class='formfieldtitle'>{$iDiscipline['Period']}<span class='tabletextrequire'>*</span></td>";
$reportOptionContent .= "<td><table border='0' cellspacing='0' cellpadding='3'>";
$reportOptionContent .= "<tr><td height='30' colspan='6'>";
$reportOptionContent .= "<input name='dateChoice' type='radio' id='dateChoice[0]' value='1'";
$reportOptionContent .= ($dateChoice==1) ? " checked" : "";
$reportOptionContent .= ">$i_Discipline_School_Year";
$reportOptionContent .= "<select name='SchoolYear' onFocus=\"document.form1.dateChoice[0].checked=true\" >";
$reportOptionContent .= $selectSchoolYear;
$reportOptionContent .= "</select>$i_Discipline_Semester ";
$reportOptionContent .= "<select name='semester' onFocus=\"document.form1.dateChoice[0].checked=true\" >";
$reportOptionContent .= $SemesterMenu;
$reportOptionContent .= "</select></td></tr>";
$reportOptionContent .= "<tr><td><input name='dateChoice' type='radio' id='dateChoice[1]' value='2'";
$reportOptionContent .= ($dateChoice==2) ? " checked" : "";
$reportOptionContent .= ">{$iDiscipline['Period_Start']}</td>";
$reportOptionContent .= "<td><input name='startDate' type='text' class='tabletext' value='$startDate' onFocus=\"document.form1.dateChoice[1].checked=true\" /></td>";
$reportOptionContent .= "<td align='center' onClick='form1.dateChoice[1].checked=true'>";
$reportOptionContent .= $linterface->GET_CALENDAR('form1', 'startDate');
$reportOptionContent .= "</td><td width='25' align='center'>{$iDiscipline['Period_End']}</td>";
$reportOptionContent .= "<td><input name='endDate' type='text' class='tabletext' value='$endDate' onFocus=\"document.form1.dateChoice[1].checked=true\" /></td>";
$reportOptionContent .= "<td align='center' onClick='form1.dateChoice[1].checked=true'>";
$reportOptionContent .= $linterface->GET_CALENDAR('form1', 'endDate');
$reportOptionContent .= "</td></tr></table></td></tr>";
$reportOptionContent .= "<tr valign='top'>";
$reportOptionContent .= "<td valign='top' nowrap='nowrap' class='formfieldtitle'>{$iDiscipline['RankingTarget']}<span class='tabletextrequire'>*</span></td>";
$reportOptionContent .= "<td width='80%'>";
$reportOptionContent .= "<table width='0%' border='0' cellspacing='2' cellpadding='0'>";
$reportOptionContent .= "<tr><td valign='top'>";
$reportOptionContent .= $rankTargetMenu;
$reportOptionContent .= "<br><div id='rankTargetDetail' style='position:absolute; width:280px; height:100px; z-index:0;'></div>";
$reportOptionContent .= "<select name='rankTargetDetail[]' multiple size='5'></select><br>";
$reportOptionContent .= $linterface->GET_ACTION_BTN($button_select_all, 'button', "javascript:SelectAll(this.form.elements['rankTargetDetail[]']);return false;");
$reportOptionContent .= "<br><span class='tabletextremark'>({$i_Discipline_Press_Ctrl_Key})</span>";
$reportOptionContent .= "</td><td valign='top'><br></td></tr></table></td></tr>";
$reportOptionContent .= "<tr valign='top'>";
$reportOptionContent .= "<td valign='top' nowrap='nowrap' class='formfieldtitle'>{$iDiscipline['RankingRange']}<span class='tabletextrequire'>*</span></td>";
$reportOptionContent .= "<td>{$i_general_highest} <select name='rankRange'>";
$reportOptionContent .= $rankRangeMenu;
$reportOptionContent .= "</select></td></tr>";
$reportOptionContent .= "<tr><td valign='top' nowrap='nowrap' class='formfieldtitle'><span>{$iDiscipline['RecordType']}<span class='tabletextrequire'>*</span></span></td>";
$reportOptionContent .= "<td><input name='meritType' type='radio' id='meritType[0]' value='1'";
$reportOptionContent .= ($meritType==1) ? " checked" : "";
$reportOptionContent .= "><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_merit.gif' width='20' height='20' border='0' align='absmiddle'> <label for='meritType[0]'>";
$reportOptionContent .= $i_Merit_Award;
$reportOptionContent .= "</label><input name='meritType' type='radio' id='meritType[1]' value='-1'";
$reportOptionContent .= ($meritType==-1) ? " checked" : "";
$reportOptionContent .= "><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_demerit.gif' width='20' height='20' border='0' align='absmiddle'> <label for='meritType[1]'>";
$reportOptionContent .= $i_Merit_Punishment;
$reportOptionContent .= "</label></td></tr><tr>";
$reportOptionContent .= "<td valign='top' nowrap='nowrap' class='tabletextremark'>{$i_general_required_field}</td>";
$reportOptionContent .= "<td align='center'>";
$reportOptionContent .= "</td></tr><tr><td align='center' colspan='2'>";
$reportOptionContent .= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Report, "submit", "document.form1.target='';document.form1.action='ranking_report_view.php'");
$reportOptionContent .= "</td></tr></table>";


$TAGS_OBJ[] = array($eDiscipline['Top10'], "");

# menu highlight setting
$CurrentPage = "Top10";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

?>

<script language="javascript">
<!--
function showSpan(span){
	document.getElementById(span).style.display="inline";
}

function hideSpan(span){
	document.getElementById(span).style.display="none";
}

function showOption(){
	hideSpan('spanShowOption');
	showSpan('spanHideOption');
	showSpan('divReportOption');
	document.getElementById('showReportOption').value = '1';
}

function hideOption(){
	hideSpan('divReportOption');
	hideSpan('spanHideOption');
	showSpan('spanShowOption');
	document.getElementById('showReportOption').value = '0';
}

function SelectAll(obj)
{
		var choice = document.getElementById('rankTargetDetail[]');

         for (var i=0; i<choice.options.length; i++)
         {
              choice.options[i].selected = true;
         }
}

function goCheck(form1) {
	if(form1.dateChoice[0].checked==false && form1.dateChoice[1].checked==false)	 {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['Period']?>");	
		return false;
	}
	if(form1.dateChoice[1].checked==true && (form1.startDate.value == "" || form1.endDate.value == "")) {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['Period']?>");	
		return false;
	}
	if(form1.dateChoice[1].checked==true && ((!check_date(form1.startDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) || (!check_date(form1.endDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))))
	{
		return false;
	}
	if(form1.dateChoice[1].checked==true && (form1.startDate.value > form1.endDate.value)) {
		alert("<?=$i_Discipline_System_Reports_Invalid_Date_Compare?>");
		return false;	
	}
	if(form1.rankTarget.value == "#") {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['RankingTarget']?>");	
		return false;
	}

	var choiceSelected = 0;
	var choice = document.getElementById('rankTargetDetail[]');

	for(i=0;i<choice.options.length;i++) {
		if(choice.options[i].selected==true)
			choiceSelected = 1;
	}
	
	if(choiceSelected==0) {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['RankingTarget']?>");
		return false;
	}
	
	if(form1.meritType[0].checked==false && form1.meritType[1].checked==false) {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['RecordType']?>");
		return false;
	}

}

function showDiv(div)
{
	document.getElementById(div).style.display = "inline";
}

function hideDiv(div)
{
	document.getElementById(div).style.display = "none";
}

//-->
</script>
<script language="javascript">
var xmlHttp

function showResult(str, choice)
{
	if (str.length==0)
		{ 
			document.getElementById("rankTargetDetail").innerHTML = "";
			document.getElementById("rankTargetDetail").style.border = "0px";
			return
		}
		
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 

	var url = "";
		
	url = "get_live2.php";
	url = url + "?target=" + str + "&rankTargetDetail=" + choice;
	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById("rankTargetDetail").innerHTML = xmlHttp.responseText;
		document.getElementById("rankTargetDetail").style.border = "0px solid #A5ACB2";
		//SelectAll(form1.elements['rankTargetDetail[]']);
	} 
}


function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}
</script>
<form name="form1" method="post" action="ranking_report_view.php" onSubmit="return goCheck(this)">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr> 
		<td align="center">
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td class="report_show_option">
									<!--<a href="javascript:;" class="contenttool" onClick="javascript:if(document.getElementById('showReportOption').value=='0'){document.getElementById('showReportOption').value='1';showDiv('divReportOption');}else{document.getElementById('showReportOption').value='0';hideDiv('divReportOption');}"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_show_option.gif" width="20" height="20" border="0" align="absmiddle"><?=$i_Discipline_System_Reports_Show_Reports_Option?></a>//-->
									<span id="spanShowOption"><a href="javascript:showOption();" class="contenttool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_show_option.gif" width="20" height="20" border="0" align="absmiddle"><?=$i_Discipline_Show_Statistics_Option?></a></span>
									<span id="spanHideOption" style="display:none"><a href="javascript:hideOption();" class="contenttool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_hide_option.gif" width="20" height="20" border="0" align="absmiddle"><?=$i_Discipline_Hide_Statistics_Option?></a></span>
									<div id='divReportOption' style='display:none'><?=$reportOptionContent?></div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br>
			<table width="88%" border="0" cellpadding="5" cellspacing="0" class="result_box">
				<tr>
					<td align="center">
						<table border="0" cellspacing="5" cellpadding="5">
							<tr>
								<td align="center" valign="middle" class="Chart_title"><?=$date?> <?=$i_general_most?> <?=$meritTitle?> <?=$criteria?></td>
							</tr>
						</table>
						<br>
						<?=$tableContent?>

					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<?= $linterface->GET_ACTION_BTN($button_export, "submit", "document.form1.target='_self';document.form1.action='export_ranking.php'")?>
									<?= $linterface->GET_ACTION_BTN($button_print, "submit", "document.form1.target='_blank';document.form1.action='print_ranking.php'")?>
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location.href='ranking_report.php'")?>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<input type="hidden" name="showReportOption" value="0">
</form>
<script language="javascript">
<!--
	showResult('<?=$rankTarget?>', '<?=$jsRankTargetDetail?>');
//-->
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
