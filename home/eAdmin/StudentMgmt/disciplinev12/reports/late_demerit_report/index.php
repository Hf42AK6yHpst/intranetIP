<?php
// Editing by 

/***************
 * 
 * Date:	2017-01-16	Bill	[2016-0825-1456-31225]
 * 			- Create file
 * 
 ***************/
 
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# Access right checking
$ldiscipline = new libdisciplinev12();
if(!$sys_custom['eDiscipline']['CustLateDemeritReport'] || !$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$linterface = new interface_html();

# Setting of Current Page
$CurrentPage = "Reports_CustLateDemeritReport";
$CurrentPageArr['eDisciplinev12'] = 1;

# Tab
$TAGS_OBJ[] = array($Lang['eDiscipline']['CustLateDemeritReport']['Title'], "", 1);
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Year and Date
$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
$textDate = date("Y-m-d");

# School Year
$selectYear = ($selectYear == '') ? Get_Current_Academic_Year_ID() : $selectYear;
$years = $ldiscipline->returnAllYearsSelectionArray();
$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='selectYear' id='selectYear' onclick='showResult()' ", "", $selectYear);

# Semester
$currentSemester = getCurrentSemester();
$selectSemesterHTML = "<select name=\"selectSemester\" id=\"selectSemester\" onchange=\"changeYearTermDateRange();\">";
$selectSemesterHTML .= "</select>";

# Date
$textFromDate = date("Y-m-d", strtotime(getStartDateOfAcademicYear($selectYear, '')));
$textToDate = date("Y-m-d", strtotime(getEndDateOfAcademicYear($selectYear, '')));

$linterface->LAYOUT_START();
?>
<script type="text/javascript" language="JavaScript">

function changeTerm(val)
{	
	if (val.length==0)
	{
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
	
	$.get(
		'../ajaxGetSemester.php?year='+val+'&term=<?=$selectSemester?>'+'&field=selectSemester&onchange=changeYearTermDateRange()',
		{},
		function(responseText){
			document.getElementById("spanSemester").innerHTML = responseText;
			document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
			changeYearTermDateRange();
		}
	);
}

function changeYearTermDateRange()
{
	var yearTermId = $("#selectSemester").val() == '0'? '' : $("#selectSemester").val();
	$.post(
		"../ajaxGetYearTermDateRange.php",
		{
			"AcademicYearID": $('#selectYear').val(),
			"YearTermID":yearTermId
		},
		function(returnData)
		{
			var obj = JSON.parse(returnData);
			$('#textFromDate').val(obj['StartDate']);
			$('#textToDate').val(obj['EndDate']);
		}
	);
}

function showResult()
{
	changeTerm($("#selectYear").val());
	
	var yearClassId = '';
	
	var url = "../ajaxGetLive.php?target=class";
	url += "&fieldId=rankTargetDetail[]&fieldName=rankTargetDetail[]";
	url += "&academicYearId=" + $('#selectYear').val();
	$.get(
		url,
		{},
		function(responseText){
			var showIn = "rankTargetDetail";
			$('#'+showIn).html(responseText);
			document.getElementById(showIn).style.border = "0px solid #A5ACB2";
		}
	);
}

function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function submitForm(format)
{
	var valid = true;
	var form_obj = $('#form1');
	
	// Date
	var from_date = $.trim($('#textFromDate').val());
	var to_date = $.trim($('#textToDate').val());
	if(from_date == '' || to_date == ''){
		valid = false;
		$('#textFromDate').focus();
	}
	if(valid){
		if(!check_date_without_return_msg(document.getElementById('textFromDate'))){
			valid = false;
			$('#textFromDate').focus();
		}
		if(!check_date_without_return_msg(document.getElementById('textToDate'))){
			valid = false;
			$('#textToDate').focus();
		}
	}
	if(valid){
		if(from_date > to_date){
			valid = false;
			$('#DateWarnDiv span').html('<?=$Lang['General']['JS_warning']['InvalidDateRange']?>');
			$('#DateWarnDiv').show();
			$('#textFromDate').focus();
		}
	}
	
	if(valid){
		$('#DateWarnDiv span').html('');
		$('#DateWarnDiv').hide();
	}
	
	if(!valid) return;
	$('input#format').val(format);
	if(format == 'web'){
		form_obj.attr('target', '_blank');
		form_obj.submit();
	}
	else if(format == 'csv'){
		form_obj.attr('target', '');
		form_obj.submit();
	}
	else if(format == 'pdf'){
		form_obj.attr('target', '_blank');
		form_obj.submit();
	}
}

$(document).ready(function(){
	showResult();
});

</script>

<div id="PageDiv">
<div id="div_form">
	<p class="spacer"></p> 
	
	<span class="Form_Span">
		<form id="form1" name="form1" method="post" action="report.php">
		<table class="form_table_v30">
		
			<!-- Year -->
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$iDiscipline['Period']?></td>
				<td><table class="inside_form_table">
						<tr>
							<td colspan="6">
								<?=$Lang['General']['SchoolYear']?>
								<?=$selectSchoolYearHTML?>&nbsp;
								<?=$Lang['General']['Semester']?>
								<span id="spanSemester"><?=$selectSemesterHTML?></span>
						</td>
						</tr>
						<tr>
							<td>
								<?=$Lang['General']['From']?>
								<?=$linterface->GET_DATE_PICKER("textFromDate",$textFromDate,"")?>
								<?=$Lang['General']['To']?>
								<?=$linterface->GET_DATE_PICKER("textToDate",$textToDate,"")?>
								<?=$linterface->Get_Form_Warning_Msg("DateWarnDiv","")?>
							</td>
						</tr>
				</table></td>
			</tr>
		
			<!-- Target -->
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$i_general_class?></td>
				<td>
					<table class="inside_form_table">
						<tr id='classSelectionTR'>
							<td valign="top" nowrap>
								<span id='rankTargetDetail'>
									<select name="rankTargetDetail[]" id="rankTargetDetail[]" size="5"></select>
								</span>
								<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('rankTargetDetail[]'));return false;", "selectAllBtn01")?>
							</td>
						</tr>
						<tr id='remarksTR'><td colspan="3" class="tabletextremark">(<?=$Lang['eDiscipline']['TWGHWFNS_StudentDetention']['PressCtrlKey']?>)</td></tr>
					</table>
					<span id='div_Target_err_msg'></span>
					<?=$linterface->Get_Form_Warning_Msg("TargetWarnDiv","")?>
				</td>
			</tr>
		</table>
		
		<?=$linterface->MandatoryField();?>
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "submitForm('web');")?>
		</div>
		
		<input type="hidden" name="format" id="format" value="web">
		</form>
	</span>
</div>

<div id="ReportDiv"></div>
</div>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>