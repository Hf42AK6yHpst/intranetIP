<head>
<meta http-equip="Content-style-Type" content="text/css">
<style>
<!--
#style1 {background-color: #669999; font-weight:bold; color: yellow; font-size:14;}
#style2 {background-color: #EEEEFF; font-size:14;}
-->
</style>
</head>

<?php
// modifying by: Shingo

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lstudentprofile = new libstudentprofile();
$lclass = new libclass();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-MonthlyReport-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# Class #
$select_class = $ldiscipline->getSelectClass("name=\"targetClass\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes, false);


function RepQuotes($strx) {
	$patterns = array("/\"/", "/&quot;/");
	$replacements = "/&quot;&quot;/";
	return preg_replace($patterns, $replacements, $strx);
}

# menu highlight setting
$CurrentPage = "UniformRecords";

$TAGS_OBJ[] = array($eDiscipline['UniformRecords'], "");


$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();


echo '<form name="form1" method="post" action="uniform_record.php">';
echo '<br><br>';
echo 'Student ID: <input type=text name=studentID>';


echo '<img src=" {$image_path}/{$LAYOUT_SKIN}/10x10.gif" width="10" height="1">';
echo "<br><br>";
echo $linterface->GET_ACTION_BTN($button_view, "submit");
echo '</form>';

if (!empty($studentID))
{
	echo '<form name="form1" method="post" action="">';

	// Get Student's Information
	$name = $lclass ->getStudentNameByStudentID($studentID);
	$classNo = $lclass ->getClassNoByStudentID($studentID);
	$class = $lclass ->getClassByStudentID($studentID);
	$gender = $lclass ->getGenderByStudentID($studentID);
	
	if (!empty($class))
	{
		$sql = "SELECT PhotoLink FROM INTRANET_USER WHERE UserID=$studentID";
		$photo = $lclass->returnVector($sql);
		echo '<img src="'.$photo[0].'" width=100 height=130 border=1><br>';
		// Display Student's Information
		echo '<br>';
		echo '<table border=0>';
		echo '<tr>';
		echo '<td id=style1 align=right>Student ID : </td>';
		echo '<td id=style2 align=left>'.$studentID.'</td>';
		echo '</tr>';
		echo '<tr>';
		echo '<td id=style1 align=right>Name : </td>';
		echo '<td id=style2 align=left>'.$name[0][0].'('.$name[0][1].')</td>';
		echo '</tr>';
		echo '<tr>';
		echo '<td id=style1 align=right>Class : </td>';
		echo '<td id=style2 align=left>'.$class[0].'('.$classNo[0].')</td>';
		echo '</tr>';
		echo '<tr>';
		echo '<td id=style1 align=right>Gender : </td>';
		echo '<td id=style2 align=left>'.$gender[0].'</td>';
		echo '</tr>';
		echo '</table>';
		echo "<br>";
		
		# date range
		$ts = time();
		if ($startdate == "")
		{
		    $startdate = date('Y-m-d',getStartOfAcademicYear($ts));
		}
		if ($enddate == "")
		{
		    $enddate = date('Y-m-d',getEndOfAcademicYear($ts));
		}
		if ($year=="")
		{
			$year = getCurrentAcademicYear();
		}
		//$semester = getSemesters();
		$show_waive_status=0;
		
		$detail_table = "<table width=\"95%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n";
				$detail_table .= "<tr class=\"tablegreentop\"><td class=\"tabletoplink\" width=\"5\">#</td>
				                     <td class=\"tabletoplink\" width=\"80\">$i_general_record_date</td>
				                     <td class=\"tabletoplink\" width=\"30%\">".$iDiscipline['Accumulative_Category_Name']."</td>
				                     <td class=\"tabletoplink\" width=\"30%\">".$iDiscipline['Accumulative_Category_Item_Name']."</td>
				                     <td class=\"tabletoplink\" width=\"10%\">$i_Profile_PersonInCharge</td>
				                     <td class=\"tabletoplink\" width=\"150\">$i_Discipline_System_general_remark</td>";
				 $detail_table .="</tr>";
		
		$result = $ldiscipline->retrieveStudentConductRecord($studentID,$startdate,$enddate,$year,$semester,"-1",0);
				
				for ($i=0; $i<sizeof($result); $i++)
				{
				  list ($record_id,$record_date,$cat_name,$item_name,$remark,$upgrade_record_id,$record_status,$r_pic_name)= $result[$i];
				  $css = ($i%2?"2":"1");
				  				 				  
				      $detail_table .= "<tr class=\"tablegreenrow$css tabletext\"><td>".($i+1)."</td>
				                         <td>$record_date</td>
				                         <td>$cat_name</td>
				                         <Td>$item_name</td>
				                         <td>".($r_pic_name==""?"--":$r_pic_name)."</td>
				                         <Td>". intranet_htmlspecialchars($remark) ."</td>
				                        ";
				      $detail_table.="</tr>";
				}
		$detail_table .= "</table>";
		echo $detail_table;
		echo '<br>';
				
		echo $linterface->GET_ACTION_BTN($button_print, "button", "javascript:doPrint()");
		echo '&nbsp;&nbsp;';
		echo $linterface->GET_ACTION_BTN($button_export, "button", "javascript:doExport()");
	}
	else 
		echo '<font size=4>No information about this student</font>';
	echo '</form>';
}
?>
<?php
  $linterface->LAYOUT_STOP();
  intranet_closedb();
?>
