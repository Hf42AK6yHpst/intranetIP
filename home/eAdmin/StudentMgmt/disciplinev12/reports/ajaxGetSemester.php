<?php
// Using :

######## Change Log [Start] ################
#
#	Date:	2019-05-07 (Bill)   [2019-0506-0940-13207]
#			- Get $field from POST and GET first
#           - prevent SQL Injection
#
######## Change Log [End] ################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

//header("Content-Type:text/html;charset=utf-8");

$ldiscipline = new libdisciplinev12();
//$yearID = $ldiscipline->getAcademicYearIDByYearName($year);

$yearID = IntegerSafe($year);
$result = getSemesters($yearID);

$action_str = "";
if($action != "") {
	$action_str = $action . " =\" " . $action_value . "\"";
	$action_str = stripslashes($action_str);
}

$onchangeAction = $onchange != ''? ' onchange="'.stripslashes(rawurldecode($onchange)).'" ' : '';

// [2019-0506-0940-13207] Get $field from POST and GET first
$field = $_POST['field']? $_POST['field'] : ($_GET['field']? $_GET['field'] : $field);
$field = cleanCrossSiteScriptingCode($field);

$temp = "<select name='$field' id='$field' class='formtextbox' $action_str $onchangeAction>";
if($hideWholeYear!=1)
{
	$temp .= "<option value='0'";
	$temp .= ($year=='0') ? " selected" : "";
	$temp .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";
}
if($year!=0 and $result)
{
	foreach((array)$result as $termID => $termName) {
		if($field=='semester1') {
			$selected = ($termID==$term1)? " selected" : "";
		} else if ($field=='semester2') {
			$selected = ($termID==$term2)? " selected" : "";
		} else {
			$selected = ($termID==$term)? " selected" : "";
		}
		
		$temp .= "<option value='$termID' $selected>$termName</option>";
	}
}
$temp .= "</select>";

echo $temp;

intranet_closedb();
?>