<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

$yearID = Get_Current_Academic_Year_ID();
//debug_pr($_GET);
if($flag==1) {
	$clsName = Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN');
	
	$teachingSubjectGroup = $ldiscipline->getSubjectGroupIDByTeacherID($teacherID, $yearID, $semester);
	for($i=0; $i<sizeof($teachingSubjectGroup); $i++) {
		$subjectGroupIDs[] = $teachingSubjectGroup[$i][0];
	}
	
	if(sizeof($subjectGroupIDs)>0)
		$conds = " OR (stcu.SubjectGroupID IN (".implode(',',$subjectGroupIDs)."))";
	/*
	$sql = "SELECT
				yc.YearClassID, 
				$clsName
			FROM
				INTRANET_USER USR LEFT OUTER JOIN 
				YEAR_CLASS_TEACHER yct ON (yct.UserID=USR.UserID) LEFT OUTER JOIN 
				SUBJECT_TERM_CLASS_USER stcu ON (stcu.UserID=USR.UserID) LEFT OUTER JOIN
				YEAR_CLASS_USER ycu ON (ycu.UserID=stcu.UserID) LEFT OUTER JOIN 
				YEAR_CLASS yc ON ((yc.YearClassID=yct.YearClassID OR yc.YearClassID=ycu.YearClassID) AND yc.AcademicYearID=$yearID) LEFT OUTER JOIN  
				YEAR y ON (y.YearID=yc.YearID)
			WHERE
				(yct.UserID=$teacherID $conds) AND
				yc.YearClassID IS NOT NULL 
				
			GROUP BY 
				yc.YearClassID
			ORDER BY	
				y.Sequence, yc.Sequence
			
			";
	*/

	$sql = "SELECT 
				yc.YearClassID, $clsName 
			FROM 
				INTRANET_USER USR LEFT JOIN 
				YEAR_CLASS_TEACHER yct ON (yct.UserID=USR.UserID) INNER JOIN 
				SUBJECT_TERM_CLASS_TEACHER stct ON (stct.UserID=USR.UserID) LEFT OUTER JOIN 
				SUBJECT_TERM st ON (st.SubjectGroupID=stct.SubjectGroupID AND st.YearTermID='$semester') LEFT OUTER JOIN 
				SUBJECT_TERM_CLASS_USER stcu ON (stcu.SubjectGroupID=st.SubjectGroupID) LEFT OUTER JOIN 
				YEAR_CLASS_USER ycu ON (ycu.UserID=stcu.UserID) INNER JOIN 
				YEAR_CLASS yc ON ((yc.YearClassID=ycu.YearClassID OR yct.YearClassID=yc.YearClassID) AND yc.AcademicYearID='$year') INNER JOIN
				YEAR y ON (y.YearID=yc.YearID)
			where 
				stct.UserID='$teacherID' 
			GROUP BY y.Sequence, yc.Sequence
				
			";	
	
	$result = $ldiscipline->returnArray($sql, 2);
	
	$data = getSelectByArray($result, "name='class' id='class'", $selectedClass, 1, 1, "");

} else {
	$name_field = getNameFieldByLang("USR.");
	
	# subject teachers
	$stdAry = $ldiscipline->storeStudent('0','::'.$classID);
	
	/*
	$sql = "SELECT 
				DISTINCT USR.UserID, $name_field as name
			FROM 
				INTRANET_USER USR LEFT OUTER JOIN
				SUBJECT_TERM_CLASS_TEACHER STCT ON (STCT.UserID=USR.UserID) LEFT OUTER JOIN 
				SUBJECT_TERM_CLASS_USER STCU ON (STCU.SubjectGroupID=STCT.SubjectGroupID) LEFT OUTER JOIN 
				SUBJECT_TERM ST ON (ST.SubjectGroupID=STCU.SubjectGroupID AND ST.YearTermID='$semester') LEFT OUTER JOIN
				YEAR_CLASS_TEACHER YCT ON (YCT.UserID=USR.UserID) LEFT OUTER JOIN
				YEAR_CLASS_USER ycu ON (ycu.YearClassID=YCT.YearClassID) LEFT OUTER JOIN
				YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID=$year)
			WHERE 
				(ST.YearTermID=$semester OR yc.AcademicYearID=$year) AND 
				(STCU.UserID IN (".implode(',',$stdAry).") OR ycu.UserID IN (".implode(',',$stdAry)."))
			ORDER BY	
				name";
				*/
	$sql = "SELECT
				DISTINCT USR.UserID, $name_field as name
			FROM
				INTRANET_USER USR LEFT OUTER JOIN 
				YEAR_CLASS_TEACHER yct ON (yct.UserID=USR.UserID) LEFT OUTER JOIN 
				YEAR_CLASS yc ON (yc.YearClassID=yct.YearClassID AND yc.AcademicYearID='$year') LEFT OUTER JOIN 
				SUBJECT_TERM_CLASS_TEACHER stct ON (stct.UserID=USR.UserID) LEFT OUTER JOIN 
				SUBJECT_TERM st ON (st.SubjectGroupID=stct.SubjectGroupID AND st.YearTermID='$semester') LEFT OUTER JOIN 
				SUBJECT_TERM_CLASS_USER stcu ON (stcu.SubjectGroupID=st.SubjectGroupID) 
			where 
				(stcu.UserID in (".implode(',',$stdAry).") or yct.YearClassID='$classID')
			ORDER BY
				name
			";	
				
	$teacherAry = $ldiscipline->returnArray($sql,2);
//echo $sql;
	$data = getSelectByArray($teacherAry, "name='teacher' id='teacher'", $selectedTeacher, 1, 0, $Lang['SysMgr']['Homework']['AllTeachers']);
}

echo $data;

intranet_closedb();
?>