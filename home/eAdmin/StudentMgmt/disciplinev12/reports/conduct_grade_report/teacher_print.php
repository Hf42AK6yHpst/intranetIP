<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();


if($displayMode==1) {
	$result = $ldiscipline->getTeacherConductGradeReport($year, $semester, $class, $teacherID, 1);
	$class = $class;
}
else if($displayMode==2) {
	$result = $ldiscipline->getTeacherConductGradeReportByTeacherID($year, $semester, $teacher, $classID, 1);
	$class = $classID;
}

$name = $ldiscipline->getUserNameByID($UserID);
$printedBy = $iDiscipline['PrintedBy']." : ".$name[0];

$teacherName = $ldiscipline->getUserNameByID($teacherID);

?>
<!--<link href='<?=$PATH_WRT_ROOT?>/templates/2007a/css/print.css' rel='stylesheet' type='text/css'>-->
<style type='text/css'>
<!--
print_hide {display:none;}
-->
</style>
<form name="form1" method="post" action="">
<table width="95%" cellpadding="4" cellspacing="0" border="0">
	<tr>
		<td class='print_hide' align="right">
			<?=$linterface->GET_BTN($button_print, "button","javascript:window.print()") ?>
		</td>
	<tr>
	<? if($flag==1) { ?>
	<tr>
		<td class='tabletext'><?=$i_general_Teacher." : ".$teacherName[0]?><br /><?=$i_general_class." : ". $ldiscipline->getClassNameByClassID($class)?><br /><?=$printedBy?><br /><?=$i_general_print_date." : ".(Date("d-M-y"))?></td>
	<tr>
	<? } ?>
	<tr>
		<td class="tabletext" valign="top">
			<?=$result?>
		</td>
	<tr>
</table>
</form>
<?
intranet_closedb();
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");

?>
