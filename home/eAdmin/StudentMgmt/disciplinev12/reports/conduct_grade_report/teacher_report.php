<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$lclass = new libclass();
$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lu = new libuser();


if($submitBtn != "") {
	if($displayMode==1)
		$result = $ldiscipline->getTeacherConductGradeReport($year, $semester, $class, $teacherID, $print);
	else if($displayMode==2)
		$result = $ldiscipline->getTeacherConductGradeReportByTeacherID($year, $semester, $teacher, $classID, $print);
}

# menu highlight setting
$CurrentPageArr['eDisciplinev12'] = 1;

$CurrentPage = "ConductGradeReport";

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($iDiscipline['Class_Report'], "/home/eAdmin/StudentMgmt/disciplinev12/reports/conduct_grade_report/class_report.php", 0);
$TAGS_OBJ[] = array($iDiscipline['Teacher_Report'], "/home/eAdmin/StudentMgmt/disciplinev12/reports/conduct_grade_report/teacher_report.php", 1);

$yearID = Get_Current_Academic_Year_ID();

# Semester Menu
$semester_data = getSemesters($yearID,0);	# 0 : not return associated array
$selectSemesterMenu = getSelectByArray($semester_data, "name='semester' id='semester'", $semester, 0,1, "", 1);


# teacher menu
//$teacherList = $lu->returnUsersType(1);
$teacherList = $ldiscipline->getTeachingList();

$SelectTeacher = "<select name='teacherID' id='teacherID' onChange='showResult(this.value)'>\n";
$SelectTeacher .= "<option value='#'>-- ".$button_select." --</option>";
for($i=0; $i<sizeof($teacherList); $i++) {
	$SelectTeacher .= "<option value='".$teacherList[$i][0]."'";
	$SelectTeacher .= ($teacherID==$teacherList[$i][0]) ? " selected" : "";
	$SelectTeacher .= ">".$teacherList[$i][1]."</option>\n";
}
$SelectTeacher .= "</select>\n";

# class menu
//$ClassListArr = $lclass->getClassList();
//$ClassListArr = $lclass->getSelectClass("name='classID' id='classID'",$classID,$optionSelect, $optionFirst);
$ClassListArr = $ldiscipline->getSelectClass("name='classID' id='classID' onChange='showResult(this.value)'", $classID, "-- ".$button_select." --");



# Start layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
<!--
function checkForm() {
	var obj = document.form1;
	if(obj.semester.value=="#") {
		alert("<?=$i_general_please_select." ".$i_Discipline_System_Conduct_Semester?>");	
		return false;
	} 
	if(obj.flag.value==1 && obj.teacherID.value=="#") {
		alert("<?=$i_general_please_select." ".$i_general_Teacher?>");	
		return false;
	}
	
	document.getElementById('print').value = "";
	document.getElementById('view').value = 1;
	obj.target = "_self";
	obj.action = "teacher_report.php";
	
	
}

function goPrint() {
	document.getElementById('print').value = 1;
	document.getElementById('view').value = 1;
	form1.target = "_blank";
	form1.action = "teacher_print.php";
	form1.submit();	
}
//-->
</script>
<script language="javascript">
<!--
function showResult(id) {
	
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 

	var url = "";
		
	url = "get_live.php?year="+document.getElementById('year').value;
	url += "&semester=" + document.getElementById('semester').value;
	//url += "&displayMode=" + document.getElementById('displayMode1').checked;
	url += "&flag=" + document.form1.flag.value;
	url += "&teacherID=" + id;
	url += "&classID=" + id;
	<? if($class!="") { ?>
	url += "&selectedClass=<?=$class?>";
	<? } ?>
	<? if($teacher!="") { ?>
	url += "&selectedTeacher=<?=$teacher?>";
	<? } ?>

	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	spanDisplay = (document.getElementById('displayMode1').checked) ? "spanClass" : "spanTeacher";
	
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById(spanDisplay).innerHTML = xmlHttp.responseText;
		document.getElementById(spanDisplay).style.border = "0px solid #A5ACB2";
	} 
}

function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

function showSpan(span) {
	document.getElementById(span).style.display = "inline";	
}

function hideSpan(span) {
	document.getElementById(span).style.display = "none";	
}


//-->
</script>

<br />
<form name="form1" method="post" action="">
<table width="95%" cellpadding="4" cellspacing="0" border="0">
	<tr>
		<td width="20%" class="formfieldtitle"><?=$i_Discipline_System_Conduct_School_Year?></td>
		<td width="80%" class="tablerow1"><?=getCurrentAcademicYear()?></td>
	<tr>
	<tr>
		<td width="20%" class="formfieldtitle"><?=$i_Discipline_System_Conduct_Semester?></td>
		<td width="80%" class="tablerow1"><?=$selectSemesterMenu?></td>
	<tr>
	<tr>
		<td width="20%" class="formfieldtitle"><?=$Lang['eDiscipline']['SelectionMode']?></td>
		<td width="80%" class="tablerow1">
			<input type="radio" name="displayMode" id="displayMode1" value="1" onClick="showSpan('teacherSpan');hideSpan('classSpan');document.form1.flag.value=1;" <? if($displayMode=="" || $displayMode=="1") echo " checked";?>> <label for="displayMode1"><?=$i_general_Teacher?></label>
			<input type="radio" name="displayMode" id="displayMode2" value="2" onClick="showSpan('classSpan');hideSpan('teacherSpan');document.form1.flag.value=2;" <? if($displayMode=="2") echo " checked";?>> <label for="displayMode2"><?=$i_general_class?></label>
		</td>
	<tr>
	<tr>
		<td colspan="2">
			<span id="teacherSpan" style="display:<? if($displayMode=="" || $displayMode=="1") echo "inline"; else echo "none";?>">
				<table width="100%" cellpadding="2" cellspacing="0">
					<tr>
						<td width="20%" class="formfieldtitle"><?=$i_general_Teacher?></td>
						<td width="80%" class="tablerow1"><?=$SelectTeacher?></td>
					<tr>
					<tr>
						<td width="20%" class="formfieldtitle"><?=$i_general_class?></td>
						<td width="80%" class="tablerow1"><span id="spanClass"><select></select></span></td>
					<tr>
				</table>
			</span>
			<span id="classSpan" style="display:<? if($displayMode=="2") echo "inline"; else echo "none";?>">
				<table width="100%" cellpadding="2" cellspacing="0">
					<tr>
						<td width="20%" class="formfieldtitle"><?=$i_general_class?></td>
						<td width="80%" class="tablerow1"><?=$ClassListArr?></td>
					<tr>
					<tr>
						<td width="20%" class="formfieldtitle"><?=$i_general_Teacher?></td>
						<td width="80%" class="tablerow1"><span id="spanTeacher"><select></select></span></td>
					<tr>
				</table>
			</span>		
		</td>
	<tr>


	<tr>
		<td width="20%">&nbsp;</td>
		<td width="80%" class="tablerow1"><?= $linterface->GET_ACTION_BTN($button_submit, "submit", "return checkForm()", "submitBtn")?></td>
	<tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	<tr>
	<tr>
		<td colspan="2"><?=$result?></td>
	<tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	<tr>
	<? if($submitBtn != "") { ?>
	<tr>
		<td colspan="2" align="center"><?=$linterface->GET_ACTION_BTN($button_print, "button", "goPrint()")?></td>
	<tr>
	<? } ?>
</table>

<input type="hidden" name="flag" id="flag" value="<? if($flag=="" || $flag==1) echo 1; else echo 2; ?>">
<input type="hidden" name="year" id="year" value="<?=$yearID?>">
<input type="hidden" name="print" id="print" value="<?=$print?>">
<input type="hidden" name="view" id="view" value="<?=$view?>">
</form>
<br />
<script language="javascript">
<!--
if(document.getElementById('view').value==1) {
	if(document.getElementById('flag').value==1) {
		showResult(document.getElementById('teacherID').value);	
		document.getElementById('classID').selectedIndex = 0;
	}
	else {
		showResult(document.getElementById('classID').value);
		document.getElementById('teacherID').selectedIndex = 0;	
	}
}
//-->
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
