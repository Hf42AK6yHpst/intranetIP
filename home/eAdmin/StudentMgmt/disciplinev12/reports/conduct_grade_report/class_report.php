<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$lclass = new libclass();
$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if($submitBtn != "") $result = $ldiscipline->getClassConductGradeReport($year, $semester, $class, $print);

# menu highlight setting
$CurrentPageArr['eDisciplinev12'] = 1;
$CurrentPage = "ConductGradeReport";

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($iDiscipline['Class_Report'], "/home/eAdmin/StudentMgmt/disciplinev12/reports/conduct_grade_report/class_report.php", 1);
$TAGS_OBJ[] = array($iDiscipline['Teacher_Report'], "/home/eAdmin/StudentMgmt/disciplinev12/reports/conduct_grade_report/teacher_report.php", 0);

$yearID = Get_Current_Academic_Year_ID();

# Semester Menu
$semester_data = getSemesters($yearID,0);	# 0 : not return associated array
$selectSemesterMenu = getSelectByArray($semester_data, "name='semester' id='semester'", $semester, 0,1, "", 1);


# class menu
$ClassListArr = $lclass->getClassList();

$SelectedClassArr = explode(",", $SelectedClass);
$SelectedClassTextArr = explode(",", $SelectedClassText);
$selectClassHTML = "<select name=\"class\">\n";
for ($i = 0; $i < sizeof($ClassListArr); $i++) {
	$selectClassHTML .= "<option value=\"".$ClassListArr[$i][0]."\"";
	$selectClassHTML .= ($ClassListArr[$i][0]==$class) ? " selected" : "";
	$selectClassHTML .= ">".$ClassListArr[$i][1]."</option>\n";
}
$selectClassHTML .= "</select>\n";

# Start layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
<!--
function checkForm() {
	var obj = document.form1;
	if(form1.semester.value=="#") {
		alert("<?=$i_general_please_select." ".$i_Discipline_System_Conduct_Semester?>");	
		return false;
	} else {
		document.getElementById('print').value = "";
		form1.target = "_self";
		form1.action = "class_report.php";
	}
	
}

function goPrint() {
	document.getElementById('print').value = 1;
	form1.target = "_blank";
	form1.action = "class_print.php";
	form1.submit();	
}
//-->
</script>
<br />
<form name="form1" method="post" action="">
<table width="95%" cellpadding="4" cellspacing="0" border="0">
	<tr>
		<td width="20%" class="formfieldtitle"><?=$i_Discipline_System_Conduct_School_Year?></td>
		<td width="80%" class="tablerow1"><?=getCurrentAcademicYear()?></td>
	<tr>
	<tr>
		<td width="20%" class="formfieldtitle"><?=$i_Discipline_System_Conduct_Semester?></td>
		<td width="80%" class="tablerow1"><?=$selectSemesterMenu?></td>
	<tr>
	<tr>
		<td width="20%" class="formfieldtitle"><?=$i_general_class?></td>
		<td width="80%" class="tablerow1"><?=$selectClassHTML?></td>
	<tr>
	<tr>
		<td width="20%">&nbsp;</td>
		<td width="80%" class="tablerow1"><?= $linterface->GET_ACTION_BTN($button_submit, "submit", "return checkForm()", "submitBtn")?></td>
	<tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	<tr>
	<tr>
		<td colspan="2"><?=$result?></td>
	<tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	<tr>
	<? if($submitBtn != "") { ?>
	<tr>
		<td colspan="2" align="center"><?=$linterface->GET_ACTION_BTN($button_print, "button", "goPrint()")?></td>
	<tr>
	<? } ?>
</table>

<input type="hidden" name="year" id="year" value="<?=$yearID?>">
<input type="hidden" name="print" id="print" value="<?=$print?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
