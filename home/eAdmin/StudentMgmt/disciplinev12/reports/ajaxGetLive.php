<?php
// Modified by
/*
 * Modification Log:
 *  Date: 2019-01-04 (Bill) [2018-0627-1537-01277]
 *      - Support Class Teacher to get teaching classes only
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();

$ldiscipline = new libdisciplinev12();
$lclass = new libclass();

function getLevelArray($ParCurrentAcademicYearId) {
	$libdb = new libdb();
	$sql = "
			SELECT DISTINCT
				Y.YEARID AS CLASSLEVELID,
				Y.YEARNAME AS LEVELNAME
			FROM YEAR Y
			INNER JOIN YEAR_CLASS YC ON Y.YEARID = YC.YEARID
			INNER JOIN ACADEMIC_YEAR AY ON YC.ACADEMICYEARID = AY.ACADEMICYEARID
			WHERE AY.ACADEMICYEARID = '$ParCurrentAcademicYearId'
			ORDER BY Y.SEQUENCE, YC.SEQUENCE
		";
		$returnArray = $libdb->returnArray($sql) or die(mysql_error());
	return $returnArray;
}

$target = $_REQUEST['target']; // form | class | student | student2ndLayer
$fieldId = $_REQUEST['fieldId']; // <select> id
$fieldName = $_REQUEST['fieldName']; // <select> name
$academicYearId = $_REQUEST['academicYearId'];
$studentFieldId = $_REQUEST['studentFieldId']; // student <select> id
$studentFieldName = $_REQUEST['studentFieldName']; // student <select> name
$getTeachingClassOnly = $_REQUEST['teachingClassOnly']; // for class teacher > get related classes
//$yearClassIdAry = (array)$_REQUEST['YearClassID']; // use to get students from classes if target=student2ndLayer 
$selectSize = $_REQUEST['selectSize']!='' && $_REQUEST['selectSize']>0 ? $_REQUEST['selectSize'] : 5; // multiple selection size

if($target=='form') {
	$temp = '<select name="'.$fieldName.'" id="'.$fieldId.'" class="formtextbox" multiple="multiple" size="'.$selectSize.'" >';
	//$ClassLvlArr = $lclass->getLevelArray();
	$ClassLvlArr = getLevelArray($academicYearId);
	
	for ($i = 0; $i < sizeof($ClassLvlArr); $i++) {
		$temp .= '<option value="'.$ClassLvlArr[$i][0].'"';
		$temp .= ' selected="selected" ';
		$temp .= '>'.$ClassLvlArr[$i][1].'</option>'."\n";
	}
	$temp .= '</select>'."\n";
}

if($target=="class") {
	$temp = '<select name="'.$fieldName.'" id="'.$fieldId.'" class="formtextbox" multiple="multiple" size="'.$selectSize.'" >';
	$classResult = $ldiscipline->getRankClassList("",$academicYearId);
	
	// [2018-0627-1537-01277] Class Teacher get teaching classes only
	if($getTeachingClassOnly)
	{
	    $classTitleKey = Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN');
	    
	    $classResult = array();
	    $teachingClassResult = $ldiscipline->getTeachingClassesByTeacherID("", $academicYearId);
	    foreach((array)$teachingClassResult as $thisTeachingClass) {
	        $classResult[] = array($thisTeachingClass[$classTitleKey], $thisTeachingClass['YearClassID']);
	    }
	}
	
	for($k=0;$k<sizeof($classResult);$k++) {
		$temp .= '<option value="'.$classResult[$k][1].'" selected="selected" ';
		$temp .= '>'.$classResult[$k][0].'</option>';
	}
	$temp .= '</select>'."\n";
}

if($target=="student") {
	$temp = '<select name="'.$fieldName.'" id="'.$fieldId.'" class="formtextbox" multiple="multiple" size="'.$selectSize.'" onchange="'.stripslashes(rawurldecode($onchange)).'">';
	$classResult = $ldiscipline->getRankClassList("", $academicYearId);
			
	for($k=0;$k<sizeof($classResult);$k++) {
		$temp .= '<option value="'.$classResult[$k][1].'" selected="selected" ';
		$temp .= '>'.$classResult[$k][0].'</option>';
	}
	$temp .= '</select>'."\n";
	if($divStudentSelection != ""){
		$temp .= '<div id="'.$divStudentSelection.'"></div>'."\n";
	}
}

if($target=="student2ndLayer") {
	$name_field = getNameFieldByLang("USR.");
	if(strstr($_REQUEST['YearClassID'],',')){
		$yearClassIdAry = explode(',',$_REQUEST['YearClassID']);
	}else{
		$yearClassIdAry = (array)$_REQUEST['YearClassID'];
	}
	//$temp .= '<br />';
	$temp .= '<select name="'.$studentFieldName.'" id="'.$studentFieldId.'" class="formtextbox" multiple="multiple" size="'.$selectSize.'">';
	
	$clsName = ($intranet_session_language=="en") ? "yc.ClassTitleEN" : "yc.ClassTitleB5";
	$sql = "SELECT $clsName, ycu.ClassNumber, $name_field, USR.UserID FROM INTRANET_USER USR
			LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) 
			LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)
			WHERE yc.YearClassID IN ('".implode("','",$yearClassIdAry)."') AND USR.RecordType=2 
			ORDER BY ycu.ClassNumber
			";
	
	$studentResult = $ldiscipline->returnArray($sql, 3);

	for($k=0;$k<sizeof($studentResult);$k++) {
		$temp .= '<option value="'.$studentResult[$k][3].'" selected="selected" ';
		$temp .= '>'.$studentResult[$k][0].'-'.$studentResult[$k][1].' '.$studentResult[$k][2].'</option>';
	}
	
	$temp .= '</select>'."\n";
	//$temp .= '<br />';
	
	$temp .= $linterface->GET_BTN($button_select_all, "button", "Select_All_Options('$studentFieldId', true);return false;");
}	

echo $temp;

intranet_closedb();

?>