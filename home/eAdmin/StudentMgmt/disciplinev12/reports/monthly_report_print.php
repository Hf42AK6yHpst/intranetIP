<?php

$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-MonthlyReport-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$lclass = new libclass();

$linterface = new interface_html();

for($i=0; $i<sizeof($target); $i++)
{
	
if ($level == 1)
	{
		$sql = "SELECT LevelName FROM INTRANET_CLASSLEVEL WHERE RecordStatus = 1 AND ClassLevelID = '".$target[$i]."'";
		$result = $ldiscipline->returnVector($sql);
		$target[$i] = $result[0];
	}
	else
	{
		$target[$i] = $lclass->getClassName($target[$i]);
	}
}

include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
?>

<style type='text/css' media='print'>
 .print_hide {display:none;}
</style>

<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
<tr class="tabletext"><td><h3><u>Monthly Report</u></h3></td></tr>
<tr class="tabletext"><td><br>
<?php
// echo stripslashes($content);

echo urldecode($content);
?>
</td></tr>

<br>
<table border="0" width="100%">
<tr><td align="center">
<?= $linterface->GET_ACTION_BTN($button_print, "submit", "window.print(); return false;"); ?>
</td></tr>
</table>
<?
  
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
intranet_closedb();
?>