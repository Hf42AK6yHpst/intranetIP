<?php
# using: henry chow

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$sys_custom['lcd_ap_report']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$lclass = new libclass();

########################################################

# Get Levels and Classes
$levels = $lclass->getLevelArray();
$classes = $lclass->getClassList();

$select_list = "<SELECT name=\"target[]\" id=\"target[]\" MULTIPLE SIZE=\"5\">";
for ($i=0; $i<sizeof($classes); $i++)
{
	list($id, $name, $lvl) = $classes[$i];
	$selected_tag = ( is_array($target) && in_array($id,$target) )?"SELECTED":"";
	$select_list .= "<OPTION value='".$id."' $selected_tag>". $name."</OPTION>";
}
$select_list .= "</SELECT>";

$AcademicYearID = Get_Current_Academic_Year_ID();


# year selection
$years = $ldiscipline->returnAllYearsSelectionArray();
$select_year = $linterface->GET_SELECTION_BOX($years, "name='AcademicYearID' id='AcademicYearID' onChange='changeClassForm(this.value)'", "", $AcademicYearID);

function RepQuotes($strx) {
	$patterns = array("/\"/", "/&quot;/");
	$replacements = "/&quot;&quot;/";
	return preg_replace($patterns, $replacements, $strx);
}

########################################################

$TAGS_OBJ[] = array($Lang['eDiscipline']['TermConductReport']);

# menu highlight setting
$CurrentPage = "LCD_TermConductReport";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

$ConductMarkCalculationMethod = $ldiscipline->retriveConductMarkCalculationMethod();

$hideWholeYear = ($sys_custom['tsk_disciplinev12_class_summary'] || $ConductMarkCalculationMethod!=0) ? 1 : 0;

?>

<script language="javascript">

function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}

function doExport()
{
	document.form1.action = "export.php";
	document.form1.submit();
	document.form1.action = "";
}


function checkform()
{
	var obj = document.form1;
	var select_obj = obj.elements['target[]'];

	if (!check_text(document.form1.year, "<?=$i_Discipline_System_alert_SchoolYear?>"))
	{
		return false;
	}

	for (var i=0; i<select_obj.length; i++)
	{
		if (select_obj[i].selected)
		{
			return true;
		}
	}

	alert('<?=$i_Discipline_System_alert_PleaseSelectClassLevel?>');
	return false;
}


function changeType(form_obj,lvl_value)
{
         obj = form_obj.elements["target[]"]
         <? # Clear existing options
         ?>
         while (obj.options.length > 0)
         {
                obj.options[0] = null;
         }
         if (lvl_value==1)
         {
             <?
             for ($i=0; $i<sizeof($levels); $i++)
             {
                  list($id,$name) = $levels[$i];
                  ?>
                  obj.options[<?=$i?>] = new Option('<?=intranet_htmlspecialchars($name)?>',<?=$id?>);
                  <?

             }
             ?>
         }
         else
         {
             <?
             for ($i=0; $i<sizeof($classes); $i++)
             {
                  list($id,$name, $lvl_id) = $classes[$i];
                  ?>
                  obj.options[<?=$i?>] = new Option('<?=$name?>',<?=$id?>);
                  <?

             }
             ?>
         }
}

</script>
<script language="javascript">

var xmlHttp3

function changeClassForm(val) {
	if (val.length==0)
	{ 
		document.getElementById("spanTarget").innerHTML = "";
		document.getElementById("spanTarget").style.border = "0px";
		return
	}
		
	xmlHttp3 = GetXmlHttpObject()
	
	if (xmlHttp3==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 
	var url = "";
		
	url = "../ajaxChangeClass.php";
	url += "?year=" + val;
	//url += "&selectedTarget="+document.getElementById('selectedTarget').value;
	url += "&level=" + document.getElementById('level').value;

	xmlHttp3.onreadystatechange = stateChanged3 
	xmlHttp3.open("GET",url,true)
	xmlHttp3.send(null)
	
} 

function stateChanged3() 
{ 
	if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete") { 
		document.getElementById("spanTarget").innerHTML = xmlHttp3.responseText;
		document.getElementById("spanTarget").style.border = "0px solid #A5ACB2";
	} 
}

function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

</script>
<br/>
<form name="form1" action="" method="POST">
<table class="form_table_v30">
	<tr>
		<td width="30%" class="field_title" valign="top"><?=$i_Discipline_School_Year?> <span class="tabletextrequire">*</span></td>
		<td>
			<?=$select_year?>
		</td>
	</tr>
	<tr>
		<td class="field_title" valign="top" rowspan="2">
			<?="$i_Discipline_Class/$i_Discipline_Form"?> <span class="tabletextrequire">*</span>
		</td>
		<td>
			<SELECT name="level" id='level' onChange="changeType(this.form,this.value);changeClassForm(document.getElementById('AcademicYearID').value)">
				<OPTION value="0"><?=$i_Discipline_Class?></OPTION>
				<OPTION value="1"><?=$i_Discipline_Form?></OPTION>
			</select>
		</td>
	</tr>
	<tr>
		<td>
			<span id='spanTarget'></span>
			<?= $linterface->GET_BTN($button_select_all, "button", "javascript:SelectAll(document.getElementById('target[]'))"); ?>
		</td>
	</tr>
	<tr>
		<td class="field_title" valign="top">&nbsp;</td>
		<td>
			<input type="checkbox" name="include_waive" id="include_waive" value="1" <? if($include_waive==1) echo " checked"; ?>><label for="include_waive"><?=$Lang['eDiscipline']['IncludeWaivedRecords']?></label>
		</td>
	</tr>
</table>

<div class="tabletextremark"><?=$i_general_required_field?></div>


<div class="edit_bottom_v30">
	<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Export'], "button", "javascript:doExport()")?>
</div>

<input type="hidden" name="flag" id="flag" value="1" />

<script language="javascript">
	changeClassForm('<?=$AcademicYearID?>');
</script>
</form>

<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>
