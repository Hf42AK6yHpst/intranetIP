<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_cust.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12_cust();
if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || !$sys_custom['eDiscipline']['DetentionNotice']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$ldiscipline_ui = new libdisciplinev12_ui_cust();

$DetentionDate = $_REQUEST['DetentionDate'];
$YearClassIdAry = $_REQUEST['YearClassID'];
$OptionsAry = array('Signature'=>$_REQUEST['Signature']);

include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

echo $ldiscipline_ui->getDetentionNoticeTable($DetentionDate, $YearClassIdAry, $OptionsAry);

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
intranet_closedb();
?>