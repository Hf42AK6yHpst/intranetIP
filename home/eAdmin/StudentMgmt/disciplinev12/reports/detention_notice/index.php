<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_cust.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12_cust();
if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || !$sys_custom['eDiscipline']['DetentionNotice']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$CurrentPage = "Reports_Detention_Notice";

$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($Lang['eDiscipline']['DetentionNotice'], "", 1);

$lclass = new libclass();
$linterface = new interface_html();

//$yearClassSelection = $lclass->getSelectClassWithWholeForm('id="YearClassID" name="YearClassID[]" multiple="multiple" size="10" ', $selected="", $firstValue="", $pleaseSelect="", $year="", $FormIDArr='', $TeachingClassOnly=0, $HideNoClassForm=0, $WebSAMSCodeArr='', $ClassIDArr='');
$yearClassSelection = $lclass->getSelectClassID('id="YearClassID" name="YearClassID[]" multiple="multiple" size="10" ', $selected="", $DisplaySelect=2, $AcademicYearID='',$optionFirst='', $DisplayClassIDArr='', $TeachingOnly=0);

$linterface->LAYOUT_START();

?>
<script type="text/javascript" language="JavaScript">
function checkForm(formObj)
{
	var isDateValid  = check_date_without_return_msg(document.getElementById('DetentionDate'));
	var numSelectedClass = $('select#YearClassID option:selected').length;
	var isValid = true;
	
	Date_Picker_Check_Date_Format(document.getElementById('DetentionDate'),'DPWL-DetentionDate','');
	if(!isDateValid) {
		isValid = false;
	}
	
	if(numSelectedClass == 0) {
		$('#WarnYearClassID').show();
		isValid = false;
	}else{
		$('#WarnYearClassID').hide();
	}
	
	if(isValid) {
		formObj.submit();
	}
}

</script>
<form name="form1" method="post" action="detention_notice.php" target="_blank" onsubmit="checkForm(this);return false;">
<table width="100%">
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0">
				<tr align="left">
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr align="left" class="report_show_option">
								<td height="20" class="tabletextremark"><em>- <?=$i_Discipline_Statistics_Options?> -</em></td>
							</tr>
						</table>
						<table  class="form_table_v30">
							<tr>
								<td valign="top" nowrap="nowrap"  class="field_title">
									<?=$Lang['eDiscipline']['Date']?> <span class="tabletextrequire">*</span>
								</td>
								<td>
									<?=$linterface->GET_DATE_PICKER("DetentionDate",date("Y-m-d"));?>
								</td>
							</tr>
							<tr valign="top">
								<td valign="top" nowrap="nowrap" class="field_title">
									<?=$i_Discipline_Target?> <span class="tabletextrequire">*</span>
								</td>
								<td>
									<?=$yearClassSelection?>
									<?=$linterface->GET_BTN($Lang['Btn']['SelectAll'], "button", "Select_All_Options('YearClassID', true);return false;")?>
									<?=$linterface->Get_Form_Warning_Msg('WarnYearClassID', $Lang['General']['JS_warning']['SelectClasses'], 'WarnYearClassID', false);?>
									<div class="tabletextremark">(<?=$i_Discipline_Press_Ctrl_Key?>)</div>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap"  class="field_title">
									<?=$Lang['eDiscipline']['TeachersSignature']?>
								</td>
								<td>
									<?=$linterface->Get_Checkbox('Signature', 'Signature', '1', $isChecked=0, $Class='', $Display='', $Onclick='', $Disabled='');?>
								</td>
							</tr>
						</table>
						<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
							<tr><td align="left" class="tabletextremark"><?=$i_general_required_field?></td></tr>
							<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
							<tr>
								<td align="center">
									<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "", "submitBtn")?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
	
</form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>