<?php
// Using:

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lexport = new libexporttext();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-RankingReport-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

ini_set("memory_limit", "150M"); 

//$yearID = $ldiscipline->getAcademicYearIDByYearName($SchoolYear);
$yearID = $SchoolYear;
$semesterInfo = getAllSemesterByYearID($yearID);
$baseMark = $ldiscipline->getConductMarkRule('baseMark');

$startYear = substr(getStartDateOfAcademicYear($yearID),0,4);
$endYear = $startYear + 1;

$m = 0;
$exportContentArr = array();

$jsRankTargetDetail = implode(',', $rankTargetDetail);

# prepare student list
$studentIDAry = array();

if($rankTarget!="student")
	$studentIDAry = $ldiscipline->getStudentIDByTarget($rankTarget, $rankTargetDetail, $yearID);
else 
	$studentIDAry = $studentID;

$m = 0;

$termInfo = getSemesters($yearID);

$data = array();
$total = array();

				# Award Record
				# term 1
				$sql = "SELECT DISTINCT MR.RecordID, MR.StudentID, YearTermID, MR.ConductScoreChange, LEFT(MR.RecordDate,7) as partDate FROM DISCIPLINE_MERIT_RECORD MR
						LEFT OUTER JOIN INTRANET_USER USR ON (USR.UserID=MR.StudentID)
						LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID)
						LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)
						LEFT OUTER JOIN YEAR y ON (y.YearID=yc.YearID)
						WHERE MR.AcademicYearID=$yearID AND (MR.RecordDate BETWEEN '$startYear-09-01' AND '$endYear-01-31') AND MR.RecordStatus=1 AND MR.MeritType=1 AND MR.StudentID IN (".implode(',',$studentIDAry).")
						order BY y.YearName, yc.Sequence, ycu.ClassNumber";
				$result = $ldiscipline->returnArray($sql, 4);
				
				for($j=0; $j<sizeof($result); $j++) {
					list($rid, $userid, $termID, $scoreChange, $month) = $result[$j];
					$termID = 1;
					$currentMark[$userid][$termID][$month] = isset($currentMark[$userid][$termID][$month]) ? $currentMark[$userid][$termID][$month] + $scoreChange : $baseMark + $scoreChange;
					if(isset($sys_custom['sdbnsm_disciplinev12_limit_conduct_upper_value'])) {
						$currentMark[$userid][$termID][$month] = ($currentMark[$userid][$termID][$month]>$sys_custom['sdbnsm_disciplinev12_limit_conduct_upper_value'])	 ? $sys_custom['sdbnsm_disciplinev12_limit_conduct_upper_value'] : $currentMark[$userid][$termID][$month];
					}
				}
				# term 1
				$sql = "SELECT DISTINCT MR.RecordID, MR.StudentID, YearTermID, MR.ConductScoreChange, LEFT(MR.RecordDate,7) as partDate FROM DISCIPLINE_MERIT_RECORD MR
						LEFT OUTER JOIN INTRANET_USER USR ON (USR.UserID=MR.StudentID)
						LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID)
						LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)
						LEFT OUTER JOIN YEAR y ON (y.YearID=yc.YearID)
						WHERE MR.AcademicYearID=$yearID AND (MR.RecordDate BETWEEN '$endYear-02-01' AND '$endYear-08-31') AND MR.RecordStatus=1 AND MR.MeritType=1 AND MR.StudentID IN (".implode(',',$studentIDAry).")
						order BY y.YearName, yc.Sequence, ycu.ClassNumber";
				$result = $ldiscipline->returnArray($sql, 4);
				
				for($j=0; $j<sizeof($result); $j++) {
					list($rid, $userid, $termID, $scoreChange, $month) = $result[$j];
					$termID = 2;
					$currentMark[$userid][$termID][$month] = isset($currentMark[$userid][$termID][$month]) ? $currentMark[$userid][$termID][$month] + $scoreChange : $baseMark + $scoreChange;
					if(isset($sys_custom['sdbnsm_disciplinev12_limit_conduct_upper_value'])) {
						$currentMark[$userid][$termID][$month] = ($currentMark[$userid][$termID][$month]>$sys_custom['sdbnsm_disciplinev12_limit_conduct_upper_value'])	 ? $sys_custom['sdbnsm_disciplinev12_limit_conduct_upper_value'] : $currentMark[$userid][$termID][$month];
					}
				}
				
				# Punishment Record
				# term 1
				$sql = "SELECT DISTINCT MR.RecordID, MR.StudentID, YearTermID, MR.ConductScoreChange, LEFT(MR.RecordDate,7) as partDate FROM DISCIPLINE_MERIT_RECORD MR
						LEFT OUTER JOIN INTRANET_USER USR ON (USR.UserID=MR.StudentID)
						LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID)
						LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)
						LEFT OUTER JOIN YEAR y ON (y.YearID=yc.YearID)
						WHERE MR.AcademicYearID=$yearID AND (MR.RecordDate BETWEEN '$startYear-09-01' AND '$endYear-01-31') AND MR.RecordStatus=1 AND MR.MeritType=-1 AND MR.StudentID IN (".implode(',',$studentIDAry).")
						order BY y.YearName, yc.Sequence, ycu.ClassNumber";
				$result = $ldiscipline->returnArray($sql, 4);
				
				$tempMark = 0;
				for($j=0; $j<sizeof($result); $j++) {
					list($rid, $userid, $termID, $scoreChange, $month) = $result[$j];
					$termID = 1;
					$currentMark[$userid][$termID][$month] = isset($currentMark[$userid][$termID][$month]) ?  $currentMark[$userid][$termID][$month] + $scoreChange : $baseMark + $scoreChange;
				}
				# term 2
				$sql = "SELECT DISTINCT MR.RecordID, MR.StudentID, YearTermID, MR.ConductScoreChange, LEFT(MR.RecordDate,7) as partDate FROM DISCIPLINE_MERIT_RECORD MR
						LEFT OUTER JOIN INTRANET_USER USR ON (USR.UserID=MR.StudentID)
						LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID)
						LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)
						LEFT OUTER JOIN YEAR y ON (y.YearID=yc.YearID)
						WHERE MR.AcademicYearID=$yearID AND (MR.RecordDate BETWEEN '$endYear-02-01' AND '$endYear-08-31') AND MR.RecordStatus=1 AND MR.MeritType=-1 AND MR.StudentID IN (".implode(',',$studentIDAry).")
						order BY y.YearName, yc.Sequence, ycu.ClassNumber";
				$result = $ldiscipline->returnArray($sql, 4);
				
				$tempMark = 0;
				for($j=0; $j<sizeof($result); $j++) {
					list($rid, $userid, $termID, $scoreChange, $month) = $result[$j];
					$termID = 2;
					$currentMark[$userid][$termID][$month] = isset($currentMark[$userid][$termID][$month]) ?  $currentMark[$userid][$termID][$month] + $scoreChange : $baseMark + $scoreChange;
				}
				if(isset($sys_custom['sdbnsm_disciplinev12_limit_conduct_lower_value'])) {
					$currentMark[$userid][$termID][$month] = ($currentMark[$userid][$termID][$month]<$sys_custom['sdbnsm_disciplinev12_limit_conduct_lower_value'])	 ? $sys_custom['sdbnsm_disciplinev12_limit_conduct_lower_value'] : $currentMark[$userid][$termID][$month];
				}

# table content
$exportContentArr[$m][] = $i_UserStudentName;
$exportContentArr[$m][] = $i_ClassNumber;
for($i=0; $i<sizeof($semesterInfo); $i++) {
	$exportContentArr[$m][] = $semesterInfo[$i][1];	
}
$exportContentArr[$m][] = $Lang['eDiscipline']['sdbnsm_whole_year'];	
$m++;

$termInfo = getSemesters($yearID);

# table content 
for($i=0; $i<sizeof($studentIDAry); $i++) {
	$StudentID = $studentIDAry[$i];
	$sumValue = array();
	
	$stdInfo = $ldiscipline->getStudentNameByID($studentIDAry[$i]);
	list($userid, $name, $clsName, $clsNo) = $stdInfo[0];
	
	$exportContentArr[$m][] = $name;
	$exportContentArr[$m][] = $clsName." - ".$clsNo;
	
				$total['Sum'] = 0;
				$total['Count'] = 0;
				$total['Count2'] = 0;

					# term 1
					$sql = "SELECT DISTINCT LEFT(RecordDate,7) as partDate FROM DISCIPLINE_MERIT_RECORD WHERE RecordDate BETWEEN '$startYear-09-01' AND '$endYear-08-31' order by partDate limit 5";
					$temp = $ldiscipline->returnVector($sql);
					$c = 0;
					foreach($temp as $month) {
						if($month!='0000-00') {
							$sumValue[$StudentID][1] += isset($currentMark[$StudentID][1][$month]) ? $currentMark[$StudentID][1][$month] : $baseMark;	
							$total['Count']++;
							$c++;
						}
					}
					if($c!=5) $sumValue[$StudentID][1] += (5-$c) * 80;
					
					$thisValue = round(($sumValue[$StudentID][1]/5),2);					
					$total['Sum'] += $thisValue;
					$exportContentArr[$m][] = $thisValue;
					$total['Count2']++;
					
					# term 2
					$sql = "SELECT DISTINCT LEFT(RecordDate,7) as partDate FROM DISCIPLINE_MERIT_RECORD WHERE RecordDate BETWEEN '$endYear-02-01' AND '$endYear-08-31'";
					$temp = $ldiscipline->returnVector($sql);
					$c = 0;
					foreach($temp as $month) {
						if($month!='0000-00') {
							$sumValue[$StudentID][2] += isset($currentMark[$StudentID][2][$month]) ? $currentMark[$StudentID][2][$month] : $baseMark;	
							$total['Count']++;
							$c++;
						}
					}
					if($c!=5) $sumValue[$StudentID][2] += (5-$c) * 80;
					$thisValue = round(($sumValue[$StudentID][2]/5),2);					
					$total['Sum'] += $thisValue;
					$total['Count2']++;
					$exportContentArr[$m][] = $thisValue;	
					
	$exportContentArr[$m][] = 	($total['Count2']==0) ? 0 : round(($total['Sum']/$total['Count2']),2);
	$m++;
}

$exportContentArr[$m][] = "";
$m++;

if(sizeof($studentIDAry)==0) {
	$exportContentArr[$m][] = $i_no_record_exists_msg;
	$m++;
}

$exportContentArr[$m][] = "";
$m++;

$filename = "monthlyReport_$month.csv";	
$exportColumn = "";

$export_content .= $lexport->GET_EXPORT_TXT($exportContentArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

$lexport->EXPORT_FILE($filename, $export_content);

?>