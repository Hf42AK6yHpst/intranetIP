<?php
# using: yat

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();


$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lexport = new libexporttext();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-RankingReport-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

ini_set("memory_limit", "150M"); 


//$yearID = $ldiscipline->getAcademicYearIDByYearName($SchoolYear);
$yearID = $SchoolYear;

$month = $month+1;
$reportMonth = $month;
$month = ($month<=9) ? "0".$month : $month;

$awardCategory = $ldiscipline->getAPCategoryByLeftJoinType(1);
$punishmentCategory = $ldiscipline->getAPCategoryByLeftJoinType(-1);

$m = 0;
$exportContentArr = array();

	
$jsRankTargetDetail = implode(',', $rankTargetDetail);

# prepare student list
$studentIDAry = array();

if($rankTarget!="student")
	$studentIDAry = $ldiscipline->getStudentIDByTarget($rankTarget, $rankTargetDetail);
else 
	$studentIDAry = $studentID;

$m = 0;	
	
foreach($studentIDAry as $id) {
	$dataCount = array();
	$dataScore = array();
	
	$stdInfo = $ldiscipline->getStudentNameByID($id);
	
	# student info
	$exportContentArr[$m][] = $i_general_name." : ".$stdInfo[0][1];
	$m++;
	$exportContentArr[$m][] = $i_ClassName." : ".$stdInfo[0][2];
	$exportContentArr[$m][] = $i_ClassNumber." : ".$stdInfo[0][3];
	$m++;

	$exportContentArr[$m][] = "";	
	$m++;
	
	# table header
	$exportContentArr[$m][] = $Lang['eDiscipline']['sdbnsm_ap_item'];	
	$exportContentArr[$m][] = $Lang['eDiscipline']['Month2'][$reportMonth];	
	$m++;	
	$exportContentArr[$m][] = $Lang['eDiscipline']['sdbnsm_punish_A'];	
	$exportContentArr[$m][] = $Lang['eDiscipline']['sdbnsm_times'];	
	$exportContentArr[$m][] = $Lang['eDiscipline']['sdbnsm_scoreChange'];	
	$m++;
	

	$sql = "SELECT MIC.CatID, MIC.CategoryName, COUNT(MR.RecordID), SUM(ConductScoreChange) FROM DISCIPLINE_MERIT_RECORD MR 
			LEFT OUTER JOIN DISCIPLINE_MERIT_ITEM MI ON (MI.ItemID=MR.ItemID) 
			LEFT OUTER JOIN DISCIPLINE_MERIT_ITEM_CATEGORY MIC ON (MIC.CatID=MI.CatID)
			WHERE StudentID=$id AND AcademicYearID=$yearID AND MR.RecordDate LIKE '%-$month-%' AND MR.RecordStatus=1 
			GROUP BY MIC.CatID";
	$result = $ldiscipline->returnArray($sql,4);		
	
	for($j=0; $j<sizeof($result); $j++) {
		list($catid, $catName, $totalRecord, $sumScore) = $result[$j];
		$dataCount[$catid] += $totalRecord;
		$dataScore[$catid] = $sumScore;				
	}

	$pos = 1;
	$punishConductScoreChange = 0;
	
	# Punishment Summary
	for($i=0; $i<sizeof($punishmentCategory); $i++) {		
		list($ID, $name) = $punishmentCategory[$i];
		$exportContentArr[$m][] = "(".$pos.") ".$name;
		$exportContentArr[$m][] = ($dataCount[$ID]!=0) ? $dataCount[$ID] : 0;
		$exportContentArr[$m][] = ($dataScore[$ID]!=0) ? $dataScore[$ID] : 0;
		$punishConductScoreChange += $dataScore[$ID];
		$pos++;
		$m++;
	}
	/*
	$exportContentArr[$m][] = "";
	$m++;
	*/		
	$exportContentArr[$m][] = $Lang['eDiscipline']['sdbnsm_award_B'];	
	$exportContentArr[$m][] = $Lang['eDiscipline']['sdbnsm_times'];	
	$exportContentArr[$m][] = $Lang['eDiscipline']['sdbnsm_scoreChange'];	
	$m++;
	
	$awardConductScoreChange = 0;
	# Award Summary
	$pos = 21;
	$k = 0;
	for($i=0; $i<sizeof($awardCategory); $i++) {			
		list($ID, $name) = $awardCategory[$i];
		$exportContentArr[$m][] = 	"(".$pos.") ".$name;
		$exportContentArr[$m][] = 	($dataCount[$ID]!=0) ? $dataCount[$ID] : 0;
		$exportContentArr[$m][] = 	($dataScore[$ID]!=0) ? $dataScore[$ID] : 0;
		$awardConductScoreChange += $dataScore[$ID];
		$pos++;
		$k++;
		$m++;
	}
	
	$exportContentArr[$m][] = "";
	$m++;
	
		/*

	# merit & demerit name
	$mtypeAry["1"] = $i_Merit_Merit;
	$mtypeAry["2"] = $i_Merit_MinorCredit;
	$mtypeAry["3"] = $i_Merit_MajorCredit;
	$mtypeAry["4"] = $i_Merit_SuperCredit;
	$mtypeAry["5"] = $i_Merit_UltraCredit;
	$mtypeAry["0"] = $i_Merit_Warning;
	$mtypeAry["-1"] = $i_Merit_BlackMark;
	$mtypeAry["-2"] = $i_Merit_MinorDemerit;
	$mtypeAry["-3"] = $i_Merit_MajorDemerit;
	$mtypeAry["-4"] = $i_Merit_SuperDemerit;
	$mtypeAry["-5"] = $i_Merit_UltraDemerit;
	
	
	# Summary of "Others" category (both Award & Punishment)
	$sql = "SELECT MR.RecordDate, MR.MeritType, MR.ItemText, ProfileMeritType, ProfileMeritCount, ConductScoreChange FROM DISCIPLINE_MERIT_RECORD MR LEFT OUTER JOIN DISCIPLINE_MERIT_ITEM MI ON (MI.ItemID=MR.ItemID)
			WHERE MR.StudentID=$id AND MR.AcademicYearID=$yearID AND MR.RecordDate LIKE '%-$month-%' AND MR.RecordStatus=1 AND (MI.CatID=3 OR MI.CatID=20)
			ORDER BY MR.RecordDate DESC";
	$result = $ldiscipline->returnArray($sql,3);
		
	$exportContentArr[$m][] = $Lang['eDiscipline']['sdbnsm_ap_list'];
	$m++;
	
	# table header
	$exportContentArr[$m][] = $Lang['eDiscipline']['sdbnsm_record_date'];
	$exportContentArr[$m][] = $Lang['eDiscipline']['sdbnsm_award'];
	$exportContentArr[$m][] = $Lang['eDiscipline']['sdbnsm_punish'];
	$exportContentArr[$m][] = $Lang['eDiscipline']['sdbnsm_item'];
	$m++;
	
	for($k=0; $k<sizeof($result); $k++) {			
		list($date, $type, $item, $mtype, $mCount, $scoreChange) = $result[$k];
		$exportContentArr[$m][] = $date;
		$exportContentArr[$m][] = ($type==1) ? "Y" : "";
		$exportContentArr[$m][] = ($type==-1) ? "Y" : "";
		$exportContentArr[$m][] = $item.$Lang['eDiscipline']['sdbnsm_fullStop'].$Lang['eDiscipline']['sdbnsm_record'].$mtypeAry[$mtype]." ".$mCount." ".$Lang['eDiscipline']['sdbnsm_times2'].$Lang['eDiscipline']['sdbnsm_comma'].($scoreChange>=0?$Lang['eDiscipline']['sdbnsm_add']:$Lang['eDiscipline']['sdbnsm_minus']).$Lang['eDiscipline']['sdbnsm_conductMark'].abs($scoreChange).$Lang['eDiscipline']['sdbnsm_fullStop'];
		$m++;
	}
	if(sizeof($result)==0) {
		$exportContentArr[$m][] = $i_no_record_exists_msg;
		$m++;
	}
	
	$exportContentArr[$m][] = "";
	$m++;
	
		*/

	
	# extra data from import file (if any)
	//$conductScoreChange = ($conductScoreChange>20) ? 20 : $conductScoreChange;	# max. add 20 marks
	$baseMark = $ldiscipline->getConductMarkRule('baseMark');
	$currentMark = (($baseMark + $awardConductScoreChange)>$sys_custom['sdbnsm_disciplinev12_limit_conduct_upper_value']) ? $sys_custom['sdbnsm_disciplinev12_limit_conduct_upper_value'] : $baseMark + $awardConductScoreChange;
	$currentMark = $currentMark + $punishConductScoreChange;
	$grade = $ldiscipline->getConductGradeByScore($currentMark);
	
	list($userID, $name, $clsName, $clsNo) = $stdInfo[0];
	$sql = "SELECT * FROM temp_monthly_ap_import WHERE ClassName='$clsName' AND ClassNumber=$clsNo";
	$result = $ldiscipline->returnArray($sql);
	//if(sizeof($result)>0) {
		list($clsNameImport, $clsNoImport, $absent, $late, $earlyLeave, $comment) = $result[0];
		if($absent == floor($absent)) {
			$halfDay = 0;
			$wholeDay = floor($absent);
		} else {
			$halfDay = 1;
			$wholeDay = floor($absent);
		}
		if($absent=="") $absent = 0;
		if($late=="") $late = 0;
		if($earlyLeave=="") $earlyLeave = 0;
		if($comment=="") $comment = "---";
		
		$exportContentArr[$m][] = $Lang['eDiscipline']['sdbnsm_baseMark_1']."\t".$baseMark.$Lang['eDiscipline']['sdbnsm_baseMark_2'];
		$m++;
		$exportContentArr[$m][] = $Lang['eDiscipline']['sdbnsm_absent'];
		//$exportContentArr[$m][] = $halfDay.$Lang['eDiscipline']['sdbnsm_half_day'];
		//$exportContentArr[$m][] = $wholeDay.$Lang['eDiscipline']['sdbnsm_whole_day'];
		$exportContentArr[$m][] = $absent.$Lang['eDiscipline']['sdbnsm_day'];
		$m++;
		$exportContentArr[$m][] = $Lang['eDiscipline']['sdbnsm_late'];
		$exportContentArr[$m][] = $late.$Lang['eDiscipline']['sdbnsm_times_2'];
		$m++;
		$exportContentArr[$m][] = $Lang['eDiscipline']['sdbnsm_earlyLeave'];
		$exportContentArr[$m][] = $earlyLeave.$Lang['eDiscipline']['sdbnsm_times_2'];
		$m++;
		$exportContentArr[$m][] = $Lang['eDiscipline']['sdbnsm_conduct_mark'];
		$exportContentArr[$m][] = $currentMark.$Lang['eDiscipline']['sdbnsm_mark'];
		$m++;
		$exportContentArr[$m][] = $Lang['eDiscipline']['sdbnsm_conduct_grade'];
		$exportContentArr[$m][] = $grade;
		$m++;
		$exportContentArr[$m][] = $Lang['eDiscipline']['sdbnsm_teacher_comment']." : ".$comment;
		$m++;

		$exportContentArr[$m][] = "";	
		$m++;
	//}
	
	
	$exportContentArr[$m][] = "";	
	$m++;
	
}


$filename = "monthlyReport_$month.csv";	
$exportColumn = "";

//$export_content = $date." ".$i_general_most." ".$meritTitle." ".$criteria."\n\n";
$export_content .= $lexport->GET_EXPORT_TXT($exportContentArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

$lexport->EXPORT_FILE($filename, $export_content);

?>
