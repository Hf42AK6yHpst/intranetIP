<?php
// Editing by 

/***************
 * 
 * 	Date:	2017-01-19 (Bill)	[2017-0111-1622-06164]
 * 			Merit Type count - hide AP records which is approved in Rehabilitation Program
 * 
 * 	Date: 	2016-12-20 (Bill)	[2016-1117-1427-18066]
 * 			updated access right
 * 
 ***************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_cust.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# user access right checking
$ldiscipline = new libdisciplinev12_cust();
if(!$sys_custom['eDiscipline']['CSCProbation'] || !($ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || $ldiscipline->CHECK_ACCESS("Discipline-REPORTS-CWC_AP_Records-View"))) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$ldiscipline_ui = new libdisciplinev12_ui_cust();

# POST data
$format = $_POST['format'];
$selectYear = $_POST['selectYear'];
$textFromDate = $_POST['textFromDate'];
$textToDate = $_POST['textToDate'];
$dateRangeStart = date("d/m/Y", strtotime($textFromDate));
$dateRangeEnd = date("d/m/Y", strtotime($textToDate));

# Semester
$semester = (array)getSemesters($selectYear);
$semester = array_keys($semester);

# Rank Target
$classIdCond = " AND yc.YearClassID IN ('".implode("','",(array)$_POST['rankTargetDetail'])."') ";

# Style
if($format == 'pdf')
{
	$StylePrefix = '<span class="tabletextrequire">';
	$StyleSuffix = '</span>';
}

$merit_type_ary = $ldiscipline->RETURN_MERIT_NAME("","",true);

# Get Target Students
$clsName = "yc.ClassTitleEN";	
$sql = "SELECT 
			iu.UserID,
			yc.YearClassID,
			yc.ClassTitleEN as ClassName,
			ycu.ClassNumber,
			iu.UserLogin,
			CONCAT(
				IF($clsName IS NULL OR $clsName='' OR ycu.ClassNumber IS NULL OR ycu.ClassNumber='' Or iu.RecordStatus != 1, '".$StylePrefix."^".$StyleSuffix."', ''),
				iu.ChineseName
			) as StudentName
		FROM INTRANET_USER as iu 
		LEFT JOIN YEAR_CLASS_USER as ycu ON ycu.UserID = iu.UserID
		LEFT JOIN YEAR_CLASS as yc ON (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '$selectYear')
		LEFT JOIN YEAR as y ON y.YearID = yc.YearID
		WHERE yc.AcademicYearID = '".$selectYear."' $classIdCond 
		GROUP BY iu.UserID 
		ORDER BY y.Sequence, yc.Sequence, ycu.ClassNumber";
$students = $ldiscipline->returnResultSet($sql);
$student_ids = Get_Array_By_Key($students, "UserID");
$student_ary = BuildMultiKeyAssoc((array)$students, array("YearClassID","UserID"));
$student_count = count($students);

# Awards / Punishment - Approved and Released
$sql = "SELECT 
			r.RecordID,
			r.StudentID,
			mc.CategoryName,
			m.ItemCode,
			m.ItemName,
			DATE_FORMAT(r.RecordDate,'%d/%m/%Y') as RecordDate,
			r.YearTermID,
			r.ProfileMeritType,
			ROUND(r.ProfileMeritCount) AS MeritRecordCount,
			r.Remark,
			r.PICID
		FROM 
			DISCIPLINE_MERIT_RECORD as r
		INNER JOIN 
			DISCIPLINE_MERIT_ITEM as m ON m.ItemID = r.ItemID
		INNER JOIN 
			DISCIPLINE_MERIT_ITEM_CATEGORY as mc ON mc.CatID = m.CatID
		WHERE 
			r.AcademicYearID = '$selectYear' AND r.RecordDate >= '$textFromDate' AND r.RecordDate <= '$textToDate'  AND 
			r.StudentID IN ('".implode("','", (array)$student_ids)."') AND 
			r.RecordStatus = '".DISCIPLINE_STATUS_APPROVED."' AND r.ReleaseStatus = '".DISCIPLINE_STATUS_RELEASED."' AND 
			(r.RehabilStatus IS NULL OR r.RehabilStatus != '3')
		ORDER BY
			r.StudentID, r.RecordDate, r.RecordID";
$ap_records = $ldiscipline->returnResultSet($sql);
$ap_records = BuildMultiKeyAssoc((array)$ap_records, array("StudentID","RecordID"));

# Report Format - CSV
if($format == 'csv')
{
	# Initiate Export Object
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	$lexport = new libexporttext();
	$exportContent = '';
	
	# CSV Header
	$rows = array();
	
	# CSV Content
	$level_num = count((array)$_POST['rankTargetDetail']);
	if($level_num)
	{
		foreach((array)$_POST['rankTargetDetail'] as $yearclass_id)
		{
			if($resultRow==""){
				$header = array("{$Lang['eDiscipline']['CWC']['StudentAPRecord']} - ".$ldiscipline->getClassNameByClassID($yearclass_id));
			}
			
			$resultRow = $ldiscipline_ui->getCWCPunishmentRecord($format, $yearclass_id, $student_ary, $ap_records, $resultRow=="");
			$rows = array_merge($rows,$resultRow);
		}
	}

	$exportContent .= $lexport->GET_EXPORT_TXT($rows, $header);
	$lexport->EXPORT_FILE("ClassDemeritRecord.csv", $exportContent);
}
# Report Format - PDF
//else if($format == 'pdf')
//{
//	# Initiate mPDF Object
//	require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");
//	$pdf_obj = "";
//		
//	// Create mPDF object
//	$pdf_obj = new mPDF('','A4',0,'',10,10,10,10,5,0);
//	
//	// mPDF Setting
//	$pdf_obj->setAutoTopMargin = 'stretch';
//	//split 1 table into 2 pages add border at the bottom
//	$pdf_obj->splitTableBorderWidth = 0.1; 
//
//	# Report Header
//	$html = "<table class='report_header' cellspacing='0' cellpadding='0' border='0' width='100%'>";
//		$html .= "<tr><td align='left'>違規班報表 (".$dateRangeStart."-".$dateRangeEnd.")</td></tr>";
//	$html .= "</table>";
//	$pdf_obj->DefHTMLHeaderByName("titleHeader", $html);
//	$pdf_obj->SetHTMLHeaderByName("titleHeader");
//	
//	# CSS
//	$pdf_obj->writeHTML("<head>");
//	$html = "<style TYPE='text/css'>
//				body 
//				{
//					font-family: msjh; 
//					font-size: 9pt; 
//					font-style: italic;
//				}
//				.report_header
//				{
//					font-size: 12pt;	
//				}
//				table.result_table th, table.result_table td 
//				{
//					text-align: center;
//					padding: 1px;
//					font-style: italic;
//				}
//				table.result_table th.result_table_header
//				{
//					border-top: 0.1mm solid black;
//					border-bottom: 0.5mm solid black;
//					font-weight: normal;
//					font-size: 11pt;
//					font-style: italic;
//				}
//				table.result_table td 
//				{
//					line-height: 6mm;
//				}
//			</style>";
//	$pdf_obj->writeHTML($html);
//	$pdf_obj->writeHTML("</head>");
//	
//	$level_count = 0;
//	$level_num = count((array)$_POST['rankTargetDetail']);
//	
//	# Report Table
//	$pdf_obj->writeHTML("<body>");
//	if($level_num)
//	{
//		foreach((array)$_POST['rankTargetDetail'] as $yearclass_id)
//		{
//			$html = $ldiscipline_ui->getTKPClassDemeritRecord($format, $yearclass_id, $student_ary, $ap_pics, $ap_records, ($level_count==$level_num-1));
//			$pdf_obj->writeHTML($html);
//			
//			$level_count++;
//		}
//	}
//	$pdf_obj->writeHTML("</body>");
//	
//	# Export PDF file
//	$pdf_obj->Output('ClassDemeritRecord.pdf', 'I');
//} 
else 
{	
	include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
	
	# CSS
	$html = "<style TYPE='text/css'>
				div.container {
					display:block; width:880px; height:1000px; margin:auto; /*border:1px solid #DDD;*/ padding:30px 20px; position:relative; padding-bottom:90px;
				}
				th, td {
					text-align:left;
					font-size: 12px; 
				}
				.report_header td{
					font-size: 15px;
					font-weight:bold;
					padding-bottom: 15px;
					text-align:center;
				}
/*
				body 
				{
					font-family: msjh; 
					font-size: 9pt; 
					font-style: italic;
				}
				.report_header
				{
					font-size: 12pt;	
				}
				table.result_table th, table.result_table td 
				{
					text-align: center;
					padding: 1px;
					font-style: italic;
				}
				table.result_table th.result_table_header
				{
					border-top: 0.1mm solid black;
					border-bottom: 0.5mm solid black;
					font-weight: normal;
					font-size: 11pt;
					font-style: italic;
				}
				table.result_table td 
				{
					line-height: 6mm;
				}
*/
			</style>";

	$linterface = new interface_html();
	$html .= "<table width='100%' align='center' class='print_hide'><tr>";
	$html .= "<td align='right'>";
	$html .= $linterface->GET_BTN($button_print, "button","javascript:window.print()");
	$html .= "</td></tr></table>";
	
	$level_count = 0;
	$level_num = count((array)$_POST['rankTargetDetail']);

	# Report Table
	if($level_num)
	{
		foreach((array)$_POST['rankTargetDetail'] as $yearclass_id)
		{
			# Content
			$html .= $ldiscipline_ui->getCWCPunishmentRecord($format, $yearclass_id, $student_ary, $ap_records);
			
			if($level_count!=$level_num-1)
				$html .= "<div style='page-break-after:always;'>&nbsp;</div>";
			
			$level_count++;
		}
	}
	echo $html;
	
	include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
}

intranet_closedb();
?>