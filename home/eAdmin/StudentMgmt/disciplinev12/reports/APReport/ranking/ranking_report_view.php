<?php
// Modifying by : 

############ Change Log Start #############################
#
#	Date	:	2016-04-11 (Bill)
#				Replace deprecated split() by explode() for PHP 5.4
#
############ Change Log End #############################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");

intranet_auth();
intranet_opendb();


$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lstudentprofile = new libstudentprofile();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-Award_Punishment_Ranking_Report-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}
if($rankTarget == '') {
	header("Location: index.php");	
}

$academicYearID = Get_Current_Academic_Year_ID();

# School Year Menu #
$years = $ldiscipline->returnAllYearsSelectionArray();
$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='SchoolYear' id='SchoolYear' onFocus='document.form1.dateChoice[0].checked=true' onChange='changeTerm(this.value)'", "", $SchoolYear);


$conds .= ($include_waive) ? " AND (a.RecordStatus=".DISCIPLINE_STATUS_APPROVED." OR a.RecordStatus=".DISCIPLINE_STATUS_WAIVED.")" : " AND a.RecordStatus=".DISCIPLINE_STATUS_APPROVED;

# merit Type
if($meritType==1) {
	$meritTitle = $iDiscipline['Awarded'];	
	$conds .= " AND a.MeritType=1";
} else {
	$meritTitle = $iDiscipline['Punished'];	
	$conds .= " AND a.MeritType=-1";
}

$semesterText = ($semester=='WholeYear' || $semester==0) ? $i_Discipline_System_Award_Punishment_Whole_Year : $ldiscipline->getTermNameByTermID($semester);

# Semester Menu #
$SemesterMenu .= "<option value='WholeYear'";
$SemesterMenu .= ($semester1 != 'WholeYear') ? "" : " selected";
$SemesterMenu .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";

if($dateChoice==2) {
	$date = $startDate ." {$i_To} ".$endDate;
	$conds .= " AND a.RecordDate BETWEEN '$startDate' AND '$endDate'";
} else {
	if($SchoolYear != '0') {		# specific school year (not "All School Year")
		$date = $ldiscipline->getAcademicYearNameByYearID($SchoolYear)." ".$semesterText;

		// replace split() by explode() - for PHP 5.4
		//list($startYear, $endYear) = split('-',$SchoolYear);
		list($startYear, $endYear) = explode('-',$SchoolYear);
		
		//$conds .= " AND a.Year='$SchoolYear'";
		$conds .= " AND a.AcademicYearID=$SchoolYear";
	} else {						# all school year
		$date = $i_Discipline_System_Award_Punishment_All_School_Year." ".$semesterText;
		$academicYear = $ldiscipline->generateAllSchoolYear();
		$tempConds = "";
	}
	$conds .= ($semester != 'WholeYear' && $semester!=0) ? " AND a.YearTermID='$semester'" : "";
}

$conds .= ($include_unreleased) ? "" : " AND a.ReleaseStatus=".DISCIPLINE_STATUS_RELEASED;

//echo $conds;
# Ranking Range
for($i=1;$i<=20;$i++) {
	$rankRangeMenu .= "<option value=$i";
	$rankRangeMenu .= ($i==$rankRange) ? " SELECTED" : "";
	$rankRangeMenu .= ">$i</option>";
}

# Rank Target 
$rankTargetMenu .= "<select name='rankTarget' id='rankTarget' onChange=\"showResult(this.value,'');if(this.value=='student') {showSpan('spanStudent');} else {hideSpan('spanStudent');}\">";
$rankTargetMenu .= "<option value='#'>-- $i_general_please_select --</option>";
$rankTargetMenu .= "<option value='form'";
$rankTargetMenu .= ($rankTarget=='form') ? " selected" : "";
$rankTargetMenu .= ">$i_Discipline_Form</option>";
$rankTargetMenu .= "<option value='class'";
$rankTargetMenu .= ($rankTarget=='class') ? " selected" : "";
$rankTargetMenu .= ">$i_Discipline_Class</option>";
$rankTargetMenu .= "<option value='student'";
$rankTargetMenu .= ($rankTarget=='student') ? " selected" : "";
$rankTargetMenu .= ">$i_Discipline_Student</option>";
$rankTargetMenu .= "</select>";


# form / class

if(is_array($rankTargetDetail)) {
	$rankTargetDetail = $rankTargetDetail;
}
else {
	$rankTargetDetail[0] = $rankTargetDetail;
}

$tempConds = "";
if($rankTarget=='form') {
	for($i=0;$i<sizeof($rankTargetDetail);$i++) {
		$tempConds .= ($tempConds!="") ? " OR " : "";
		$tempConds .= " y.YearID=$rankTargetDetail[$i]";
	}
} else if($rankTarget=='class') {
	for($i=0;$i<sizeof($rankTargetDetail);$i++) {
		$tempConds .= ($tempConds!="") ? " OR " : "";
		$tempConds .= " yc.YearClassID='$rankTargetDetail[$i]'";
	}
} else {
	$tempConds .= " a.StudentID IN (".implode(',',$studentID).")";	
}
$conds .= " AND ($tempConds)";
$jsRankTargetDetail = implode(',', $rankTargetDetail);



switch($rankTarget) {
	case("class") : $criteria = $i_Discipline_Class; break;
	case("form") : $criteria = $i_Discipline_Form; break;
	case("student") : $criteria = $i_Discipline_Student; break;
	default: break;
}	

$clsName = ($intranet_session_language=="en") ? "yc.ClassTitleEN" : "yc.ClassTitleB5";

# table content
if($rankTarget=='form') {
	$sql = "SELECT
				y.YearName as form,
				COUNT(*) as total,
				$clsName as name
			FROM
				DISCIPLINE_MERIT_RECORD as a
				INNER JOIN INTRANET_USER as b ON (a.StudentID=b.UserID)
				LEFT OUTER JOIN YEAR_CLASS_USER ycu	ON (ycu.UserID=b.UserID)				 
				LEFT OUTER JOIN YEAR_CLASS yc ON (ycu.YearClassID=yc.YearClassID AND yc.AcademicYearID=$academicYearID)
				LEFT OUTER JOIN YEAR y ON (y.YearID=yc.YearID)
			WHERE
				a.DateInput IS NOT NULL
				$conds
				GROUP BY form
				ORDER BY total DESC
				LIMIT $rankRange
			";
} else if($rankTarget=='class'){
	$name_field = getNameFieldByLang('b.');
	
	$sql = "SELECT
				$clsName,
				COUNT(*) as total,
				$name_field as name
			FROM
				DISCIPLINE_MERIT_RECORD as a
				INNER JOIN	INTRANET_USER as b ON (a.StudentID=b.UserID)
				LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (b.UserID=ycu.UserID)
				LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID=$academicYearID)
			WHERE
				a.DateInput IS NOT NULL
				$conds
				GROUP BY b.ClassName
				ORDER BY total DESC
				LIMIT $rankRange
			";
} else {	# rankTarget == 'student'
	$name_field = getNameFieldByLang('b.');
	$sql = "SELECT
				$name_field as name,
				COUNT(*) as total,
				b.ClassName
			FROM
				DISCIPLINE_MERIT_RECORD as a
				INNER JOIN
					INTRANET_USER as b ON (a.StudentID=b.UserID)
			WHERE
				a.DateInput IS NOT NULL
				$conds
				GROUP BY b.UserID
				ORDER BY total DESC
				LIMIT $rankRange
			";
}

$result = $ldiscipline->returnArray($sql,3);


$tableContent .= "<table width='100%' border='0' cellpadding='4' cellspacing='1' bgcolor='#CCCCCC'>";
$tableContent .= "<tr class='tablebluetop'>";
$tableContent .= "<td class='tabletopnolink'>{$iDiscipline['Rank']}</td>";
$tableContent .= "<td class='tabletopnolink'>{$criteria}</td>";
$tableContent .= "<td class='tabletopnolink'>{$i_Discipline_No_of_Records}</td>";
$tableContent .= "</tr>";

if(sizeof($result)==0) {
	$tableContent .= "<tr class='tablebluerow1'>";
	$tableContent .= "<td class='tabletext' colspan='3' height='40' align='center'>{$i_no_record_exists_msg}</td>";
	$tableContent .= "</tr>";
}

for($i=0;$i<sizeof($result);$i++) {
	$k = $i+1;
	$css = ($i%2)+1;
	$tableContent .= "<tr class='tablebluerow{$css}'>";
	$tableContent .= "<td class='tabletext'>{$k}</td>";
	$tableContent .= "<td >";
	$tableContent .= "{$result[$i][0]}</td>";
	$tableContent .= "<td >{$result[$i][1]}</td>";
	$tableContent .= "</tr>";
}
$tableContent .= "</table>";

# Class #
$select_class = $ldiscipline->getSelectClassWithWholeForm("name=\"targetClass\" onChange=\"showResult(this.value)\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes, $i_general_please_select);

$reportOptionContent .= "<table width='88%' border='0' cellpadding='5' cellspacing='0'>";
$reportOptionContent .= "<tr><td>";
$reportOptionContent = "<table align='center' width='100%' border='0' cellpadding='5' cellspacing='0'>";
$reportOptionContent .= "<tr valign='top'>";
$reportOptionContent .= "<td height='57' valign='top' nowrap='nowrap' class='formfieldtitle'>{$iDiscipline['Period']}<span class='tabletextrequire'>*</span></td>";
$reportOptionContent .= "<td><table border='0' cellspacing='0' cellpadding='3'>";
$reportOptionContent .= "<tr><td height='30' colspan='6'>";
$reportOptionContent .= "<input name='dateChoice' type='radio' id='dateChoice[0]' value='1'";
$reportOptionContent .= ($dateChoice==1) ? " checked" : "";
$reportOptionContent .= ">$i_Discipline_School_Year";
//$reportOptionContent .= "<select name='SchoolYear' id='SchoolYear' onFocus=\"document.form1.dateChoice[0].checked=true\" onChange=\"changeTerm(this.value)\">";
$reportOptionContent .= $selectSchoolYearHTML;
//$reportOptionContent .= "</select>";
$reportOptionContent .= $i_Discipline_Semester;
$reportOptionContent .= "<span id='spanSemester'><select name='semester' id='semester' onFocus=\"document.form1.dateChoice[0].checked=true\" >";
$reportOptionContent .= $SemesterMenu;
$reportOptionContent .= "</select></span></td></tr>";
$reportOptionContent .= "<tr><td><input name='dateChoice' type='radio' id='dateChoice[1]' value='2'";
$reportOptionContent .= ($dateChoice==2) ? " checked" : "";
$reportOptionContent .= ">{$iDiscipline['Period_Start']}</td>";
$reportOptionContent .= "<td align='center' onClick='form1.dateChoice[1].checked=true'>";
$reportOptionContent .= $linterface->GET_DATE_PICKER("startDate",$startDate);
$reportOptionContent .= "</td><td width='25' align='center'>{$iDiscipline['Period_End']}</td>";
$reportOptionContent .= "<td align='center' onClick='form1.dateChoice[1].checked=true'>";
$reportOptionContent .= $linterface->GET_DATE_PICKER("endDate",$endDate);
$reportOptionContent .= "</td></tr></table></td></tr>";
$reportOptionContent .= "<tr valign='top'>";
$reportOptionContent .= "<td valign='top' nowrap='nowrap' class='formfieldtitle'>{$iDiscipline['RankingTarget']}<span class='tabletextrequire'>*</span></td>";
$reportOptionContent .= "<td width='80%'>";
$reportOptionContent .= "<table width='0%' border='0' cellspacing='2' cellpadding='0'>";
$reportOptionContent .= "<tr><td valign='top'>";
$reportOptionContent .= $rankTargetMenu;
$reportOptionContent .= "</td><td><div id='rankTargetDetail' style='position:absolute; width:280px; height:100px; z-index:0;'></div>";
$reportOptionContent .= "<select name='rankTargetDetail[]' id='rankTargetDetail[]' multiple size='5'></select><br>";
$reportOptionContent .= $linterface->GET_BTN($button_select_all, 'button', "javascript:SelectAll(this.form.elements['rankTargetDetail[]']);return false;", "selectAllBtn01");
$reportOptionContent .= "<td><div id='spanStudent' style='position:relative; left:60px; width:280px; height:80px; z-index:0;display:";
$reportOptionContent .= ($rankTarget=='student') ? "inline" : "none";
$reportOptionContent .= ";'><select name='studentID[]' id='studentID[]' multiple size='5' id='studentID[]'>
								</select>
									<br>".$linterface->GET_BTN($button_select_all, "button", "onClick=SelectAllItem(this.form.elements['studentID[]'], true);return false;")."
								</div>
							</td>";
$reportOptionContent .= "</tr><tr><td colspan='3' class='tabletextremark'>({$i_Discipline_Press_Ctrl_Key})</td></tr></table></td></tr>";
$reportOptionContent .= "<tr valign='top'>";
$reportOptionContent .= "<td valign='top' nowrap='nowrap' class='formfieldtitle'>{$iDiscipline['RankingRange']}<span class='tabletextrequire'>*</span></td>";
$reportOptionContent .= "<td>{$i_general_highest} <select name='rankRange' id='rankRange'>";
$reportOptionContent .= $rankRangeMenu;
$reportOptionContent .= "</select></td></tr>";
$reportOptionContent .= "<tr><td valign='top' nowrap='nowrap' class='formfieldtitle'><span>{$iDiscipline['RecordType']}<span class='tabletextrequire'>*</span></span></td>";
$reportOptionContent .= "<td><input name='meritType' type='radio' id='meritType[0]' value='1'";
$reportOptionContent .= ($meritType==1) ? " checked" : "";
$reportOptionContent .= "><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_merit.gif' width='20' height='20' border='0' align='absmiddle'> <label for='meritType[0]'>";
$reportOptionContent .= $i_Merit_Award;
$reportOptionContent .= "</label><input name='meritType' type='radio' id='meritType[1]' value='-1'";
$reportOptionContent .= ($meritType==-1) ? " checked" : "";
$reportOptionContent .= "><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_demerit.gif' width='20' height='20' border='0' align='absmiddle'> <label for='meritType[1]'>";
$reportOptionContent .= $i_Merit_Punishment;
$reportOptionContent .= "</label></td></tr><tr>";
$reportOptionContent .= "<td>&nbsp;</td><td><input type='checkbox' name='include_waive' id='include_waive' value='1' ";
if($include_waive) $reportOptionContent.= " checked";
$reportOptionContent .= "><label for='include_waive'>".$i_Discipline_System_Reports_Include_Waived_Record."</label>";
$reportOptionContent .= "<br><input type='checkbox' name='include_unreleased' id='include_unreleased' value='1' ";
if($include_unreleased) $reportOptionContent.= " checked";
$reportOptionContent .= "><label for='include_unreleased'>".$Lang['eDiscipline']['IncludeUnreleasedRecords']."</label>";
$reportOptionContent .= "</td></tr><tr>";
$reportOptionContent .= "<td valign='top' nowrap='nowrap' class='tabletextremark'>{$i_general_required_field}</td>";
$reportOptionContent .= "<td align='center'>";
$reportOptionContent .= "</td></tr><tr><td align='center' colspan='2'>";
$reportOptionContent .= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Report, "submit", "document.form1.target='';document.form1.action='ranking_report_view.php'");
$reportOptionContent .= "</td></tr></table>";

$TAGS_OBJ = $ldiscipline->return_APReport_TAGS(2);
$CurrentPageArr['eDisciplinev12'] = 1;
$CurrentPage = "Reports_APReport";

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$CurrentPageArr['eDisciplinev12'] = 1;

# Start layout
$linterface->LAYOUT_START();

?>

<script language="javascript">
<!--
function showSpan(span){
	document.getElementById(span).style.display="inline";
	if(span == 'spanStudent') {
		form1.elements['rankTargetDetail[]'].multiple = false;
		document.getElementById('selectAllBtn01').disabled = true;
		document.getElementById('selectAllBtn01').style.visibility = 'hidden';
		var temp = document.getElementById('studentID[]');
		
		while(temp.options.length>0) {
			temp.options[0] = null;	
		}
	} 
}

function hideSpan(span){
	document.getElementById(span).style.display="none";
	if(span == 'spanStudent') {
		document.getElementById('selectAllBtn01').disabled = false;
		document.getElementById('selectAllBtn01').style.visibility = 'visible';
		form1.elements['rankTargetDetail[]'].multiple = true;
	}
}

function showOption(){
	hideSpan('spanShowOption');
	showSpan('spanHideOption');
	showSpan('divReportOption');
	document.getElementById('showReportOption').value = '1';
}

function hideOption(){
	hideSpan('divReportOption');
	hideSpan('spanHideOption');
	showSpan('spanShowOption');
	document.getElementById('showReportOption').value = '0';
}

function SelectAll(obj)
{
		var choice = document.getElementById('rankTargetDetail[]');

         for (var i=0; i<choice.options.length; i++)
         {
              choice.options[i].selected = true;
         }
}

function goCheck(form1) {
	var choiceSelected;
	var choice = "";
	
	if(form1.dateChoice[0].checked==false && form1.dateChoice[1].checked==false)	 {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['Period']?>");	
		return false;
	}
	if(form1.dateChoice[1].checked==true && (form1.startDate.value == "" || form1.endDate.value == "")) {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['Period']?>");	
		return false;
	}
	if(form1.dateChoice[1].checked==true && ((!check_date(form1.startDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) || (!check_date(form1.endDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))))
	{
		return false;
	}
	if(form1.dateChoice[1].checked==true && (form1.startDate.value > form1.endDate.value)) {
		alert("<?=$i_Discipline_System_Reports_Invalid_Date_Compare?>");
		return false;	
	}
	if(form1.rankTarget.value == "#") {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['RankingTarget']?>");	
		return false;
	}
	if (document.getElementById('rankTarget').value=='form' && countOption(document.getElementById('rankTargetDetail[]'))<1) {
		alert('<?=$i_alert_pleaseselect.$i_Discipline_Form?>');
		return false;
	}
	if (document.getElementById('rankTarget').value=='class' && countOption(document.getElementById('rankTargetDetail[]'))<1) {
		alert('<?=$i_alert_pleaseselect.$i_Discipline_Class?>');
		return false;
	}
	if(document.getElementById('rankTarget').value == "student") {
		choiceSelected = 0;
		choice = document.getElementById('studentID[]');
	
		for(i=0;i<choice.options.length;i++) {
			if(choice.options[i].selected==true)
				choiceSelected = 1;
		}
		if(choiceSelected==0) {
			alert("<?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?>");	
			return false;
		}
	}

	var choiceSelected = 0;
	var choice = document.getElementById('rankTargetDetail[]');

	for(i=0;i<choice.options.length;i++) {
		if(choice.options[i].selected==true)
			choiceSelected = 1;
	}
	
	if(choiceSelected==0) {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['RankingTarget']?>");
		return false;
	}
	
	if(form1.meritType[0].checked==false && form1.meritType[1].checked==false) {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['RecordType']?>");
		return false;
	}

}

function showDiv(div)
{
	document.getElementById(div).style.display = "inline";
}

function hideDiv(div)
{
	document.getElementById(div).style.display = "none";
}

function SelectAllItem(obj, flag){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = flag;
	}
}

function studentSelection(val) {
	var rank = document.getElementById('rankTargetDetail[]');
	for(i=0; i<rank.length; i++) {
		if(rank.options[i].value == val) {
			rank.options[i].selected = true;	
		} else {
			rank.options[i].selected = false;	
		}
	}
}
//-->
</script>
<script language="javascript">
var xmlHttp3

function changeTerm(val) {

	if (val.length==0)
	{ 
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
		
	xmlHttp3 = GetXmlHttpObject()
	
	if (xmlHttp3==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "../../ajaxGetSemester.php";
	url += "?year=" + val;
	url += "&term=<?=$semester?>";
	url += "&field=semester";
	
	xmlHttp3.onreadystatechange = stateChanged3 
	xmlHttp3.open("GET",url,true)
	xmlHttp3.send(null)
} 

function stateChanged3() 
{ 
	if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete")
	{ 
		document.getElementById("spanSemester").innerHTML = xmlHttp3.responseText;
		document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
	} 
}

var xmlHttp

function showResult(str, choice)
{
	if(str != "student2ndLayer")
		document.getElementById("studentFlag").value = 0;
	
	if (str.length==0)
		{ 
			document.getElementById("rankTargetDetail").innerHTML = "";
			document.getElementById("rankTargetDetail").style.border = "0px";
			return
		}
		
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 
		
	url = "get_live2.php";
	url += "?target=" + str + "&rankTargetDetail=" + choice;
	url += "&year="+document.getElementById('SchoolYear').value;
	<? if($studentID != '') { 
		echo "url += \"&studentid=\" + ".implode(',',$studentID).";";
	} ?>
	url += (document.getElementById("studentFlag").value==1) ? "&student=1&value="+document.getElementById('rankTargetDetail[]').value : "";
	
	xmlHttp.onreadystatechange = stateChanged 
	
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
	//SelectAll(form1.elements['rankTargetDetail[]']);
} 

function stateChanged() 
{ 

	var showIn = "";
	if(document.getElementById("studentFlag").value == 0) {
		showIn = "rankTargetDetail";
	} else {
		showIn = "spanStudent";	
	}
	
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById(showIn).innerHTML = xmlHttp.responseText;
		document.getElementById(showIn).style.border = "0px solid #A5ACB2";
		
		if(document.getElementById("studentFlag").value == 0)
			if(document.getElementById('rankTarget').value != 'student') {
				//SelectAll(form1.elements['rankTargetDetail[]']);
				//document.getELementById('selectAllBtn01').display:none;
			}
	} 
	//document.getElementById("studentFlag").value = 0;
}

/*
function showResult(str, choice)
{
	if (str.length==0)
		{ 
			document.getElementById("rankTargetDetail").innerHTML = "";
			document.getElementById("rankTargetDetail").style.border = "0px";
			return
		}
		
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 

	var url = "";
		
	url = "get_live2.php";
	url = url + "?target=" + str + "&rankTargetDetail=" + choice;
	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById("rankTargetDetail").innerHTML = xmlHttp.responseText;
		document.getElementById("rankTargetDetail").style.border = "0px solid #A5ACB2";
		//SelectAll(form1.elements['rankTargetDetail[]']);
	} 
}
*/

function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}
</script>
<form name="form1" method="post" action="ranking_report_view.php" onSubmit="return goCheck(this)">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr> 
		<td align="center">
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td class="report_show_option">
									<span id="spanShowOption"><a href="javascript:showOption();" class="contenttool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_show_option.gif" width="20" height="20" border="0" align="absmiddle"><?=$i_Discipline_Show_Statistics_Option?></a></span>
									<span id="spanHideOption" style="display:none"><a href="javascript:hideOption();" class="contenttool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_hide_option.gif" width="20" height="20" border="0" align="absmiddle"><?=$i_Discipline_Hide_Statistics_Option?></a></span>
									<div id='divReportOption' style='display:none'><?=$reportOptionContent?></div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br>
			<table width="88%" border="0" cellpadding="5" cellspacing="0" class="result_box">
				<tr>
					<td align="center">
						<table border="0" cellspacing="5" cellpadding="5">
							<tr>
								<td align="center" valign="middle" class="Chart_title"><?=$date?> <?=$i_general_most?> <?=$meritTitle?> <?=$criteria?></td>
							</tr>
						</table>
						<br>
						<?=$tableContent?>

					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<?= $linterface->GET_ACTION_BTN($button_export, "submit", "document.form1.target='_self';document.form1.action='export_ranking.php'")?>
									<?= $linterface->GET_ACTION_BTN($button_print, "submit", "document.form1.target='_blank';document.form1.action='print_ranking.php'")?>
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location.href='index.php'")?>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<input type="hidden" name="showReportOption" id="showReportOption" value="0">
<input type="hidden" name="studentFlag" id="studentFlag" value="<? if(sizeof($studentID)>0){echo "1";} else { echo "0";} ?>">			
</form>
<script language="javascript">
<!--
showResult("<?=$rankTarget?>", "<?=$jsRankTargetDetail?>");

<?
if(sizeof($studentID)>0) 
	echo "initialStudent()\n";	
?>

function initialStudent() {
	//document.getElementById("studentFlag").value = 1;
	xmlHttp2 = GetXmlHttpObject()
	url = "get_live2.php?target=student2ndLayer&value=<?=implode(',',$rankTargetDetail)?>&rankTargetDetail=<?=implode(',',$rankTargetDetail)?>&student=1&studentid=<? if(sizeof($studentID)>0) echo implode(',',$studentID)?>";
	xmlHttp2.onreadystatechange = stateChanged2
	xmlHttp2.open('GET',url,true);
	xmlHttp2.send(null);
	document.getElementById('selectAllBtn01').disabled = true;
	document.getElementById('selectAllBtn01').style.visibility = 'hidden';
}

function stateChanged2() 
{ 
	if (xmlHttp2.readyState==4 || xmlHttp2.readyState=="complete")
	{ 
		document.getElementById('spanStudent').innerHTML = xmlHttp2.responseText;
		document.getElementById('spanStudent').style.border = "0px solid #A5ACB2";
		
		//SelectAll(form1.elements['studentID[]']);
	} 
}

changeTerm('<?=$SchoolYear?>');
//-->
</script><?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
