<?php
# modifying : yat

################################################
#
#	Date:	2014-11-20	Bill
#			Improved: export both record data and modified date when sort by modified date
#
#	Date:	2014-11-10	Bill
#			Added: display modified date - $sys_custom['eDiscipline']['BFHMC_IndividualDisciplineRecord']
#
#	Date:	2011-12-08	YatWoon
#			Improved: display Merit/Demerit Record(s) [Case#2011-1110-1418-20066]
#
################################################

$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-Award_Punishment_Class_Report-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$linterface = new interface_html();
$fc = new form_class_manage();
$lexport = new libexporttext();

$startdate = $startdate==""?date('Y-m-d'):$startdate;
$enddate = $enddate==""?date('Y-m-d'):$enddate;

$pos = strpos($targetClass,"::");
if($pos !== false)
{
	# select single class 
	$selectedClass = str_replace("::","",$targetClass);
}
else
{
	$TempClassInfo = $fc->Get_Class_List_By_YearID($targetClass);
	if(!empty($TempClassInfo))
	{
		foreach($TempClassInfo as $k=>$d)
		$selectedClass.= $d['YearClassID'] . ",";
	}
	$selectedClass = substr($selectedClass, 0, strlen($selectedClass)-1);
}

if(!empty($targetClass))
{
	$class_sql = ' and d.YearClassID in ('. $selectedClass .')';
}

$statusConds = ($include_waive) ? " AND (a.RecordStatus=".DISCIPLINE_STATUS_APPROVED." OR a.RecordStatus=".DISCIPLINE_STATUS_WAIVED.")" : " AND a.RecordStatus=".DISCIPLINE_STATUS_APPROVED;
$statusConds .= ($include_unreleased) ? "" : " AND a.ReleaseStatus=".DISCIPLINE_STATUS_RELEASED;

//$dateField = 'a.RecordDate';
//$dateCol = $i_general_record_date;
//if($sys_custom['eDiscipline']['BFHMC_IndividualDisciplineRecord']){
//	$dateField = $dateSearchType? 'DATE_FORMAT(a.DateModified, \'%Y-%m-%d\')' : $dateField;
//	$dateCol = $dateSearchType? $Lang['eDiscipline']['APReportCust']['ModifiedDate'] : $dateCol;
//}

# Fat Ho Cust
$dateSql = 'a.RecordDate';
$display_DateModified = false;
if($sys_custom['eDiscipline']['BFHMC_IndividualDisciplineRecord']){
	$dateSql = $dateSearchType? 'DATE_FORMAT(a.DateModified, \'%Y-%m-%d\')' : $dateSql;
	$display_DateModified = $dateSearchType? true : false;
}

$Current_Academic_Year_ID = Get_Current_Academic_Year_ID();

$namefield = getNameFieldByLang("b.");
$pic_namefield = getNameFieldByLang("e.");
$sql = '
		select 
			b.ClassName,
			b.ClassNumber,
			'. $namefield .' as studentname,
			a.RecordDate, 
			a.ItemText, 
			a.Remark, 
			a.ProfileMeritType, 
			a.ProfileMeritCount, 
			a.ConductScoreChange, 
			a.PICID,
			a.SubScore1Change,
			DATE_FORMAT(a.DateModified, \'%Y-%m-%d\')
		from 
			DISCIPLINE_MERIT_RECORD as a
			inner join INTRANET_USER as b on (b.UserID=a.StudentID)
			inner join YEAR_CLASS_USER as c on (c.UserID = b.UserID)
			inner join YEAR_CLASS as d on (d.YearClassID = c.YearClassID  and d.AcademicYearID='. $Current_Academic_Year_ID .' '. $class_sql .')
			inner join YEAR as f on (f.YearID = d.YearID)
		where 
			a.MeritType = '. $meritType .' and 
			'.$dateSql.' >= "'. $startdate .'" and '.$dateSql.'<="'. $enddate .'" and
			b.ClassNumber > 0
		'.$statusConds.'		
		order by 
			f.Sequence, d.Sequence, b.ClassName, b.ClassNumber,'.$dateSql.'
';
$result = $ldiscipline->returnArray($sql);

### build data array
$DataAry = array();
// $display = "";
$export_ary = array();

if(!empty($result))
{
	foreach($result as $k=>$d)
	{
		list($this_classname,$this_classnumber,$this_name,$this_date,$this_item,$this_remark,$this_merittype,$this_meritcount,$this_conduct,$this_pic, $this_study_score, $this_modified) = $d;
		
		$DataAry[$this_classname][$this_classnumber][] = array($this_name,$this_date,$this_item,$this_remark,$this_merittype,$this_meritcount,$this_conduct,$this_pic,$this_study_score,$this_modified);
	}
}

if(!empty($DataAry))
{
	### build display
	foreach($DataAry as $this_classname=>$d)
	{
		$this_class_ary = array();	
		$class_conduct_total = 0;
		$class_study_total = 0;
		
		if($ldiscipline->UseSubScore) {
			if($display_DateModified) {
				$this_class_ary[] = array($i_ClassNumber,$i_UserStudentName,$i_general_record_date,$Lang['eDiscipline']['APReportCust']['ModifiedDate'],$i_Discipline_System_general_record,$i_Discipline_System_general_remark,$eDiscipline["MeritDemeritContent"],$i_Discipline_System_ConductScore,$i_Discipline_System_Subscore1,$eDiscipline['PICTeacher']);
			} else {
				$this_class_ary[] = array($i_ClassNumber,$i_UserStudentName,$i_general_record_date,$i_Discipline_System_general_record,$i_Discipline_System_general_remark,$eDiscipline["MeritDemeritContent"],$i_Discipline_System_ConductScore,$i_Discipline_System_Subscore1,$eDiscipline['PICTeacher']);
			}
		} else {
			if($display_DateModified) {
				$this_class_ary[] = array($i_ClassNumber,$i_UserStudentName,$i_general_record_date,$Lang['eDiscipline']['APReportCust']['ModifiedDate'],$i_Discipline_System_general_record,$i_Discipline_System_general_remark,$eDiscipline["MeritDemeritContent"],$i_Discipline_System_ConductScore,$eDiscipline['PICTeacher']);
			} else {
				$this_class_ary[] = array($i_ClassNumber,$i_UserStudentName,$i_general_record_date,$i_Discipline_System_general_record,$i_Discipline_System_general_remark,$eDiscipline["MeritDemeritContent"],$i_Discipline_System_ConductScore,$eDiscipline['PICTeacher']);
			}
		}			
		foreach($d as $this_classnumber=>$d2)
		{
			$i=0;
			foreach($d2 as $k=>$d3)
			{
				$this_row = array();
				list($this_name,$this_date,$this_item,$this_remark,$this_merittype,$this_meritcount,$this_conduct,$this_pic,$this_study_score,$this_modified) = $d3;
				
				$display .= '<tr>';
				$this_row[] = $i==0 ? $this_classnumber : "";
				$this_row[] = $i==0 ? $this_name : "";
				$this_row[] = $this_date;
				if($display_DateModified) {
					$this_row[] = $this_modified;
				}
				$this_row[] = $this_item;
				$this_row[] = ($this_remark ? $this_remark : $Lang['General']['EmptySymbol']);
				$this_row[] = $ldiscipline->returnDeMeritStringWithNumber($this_meritcount, $ldiscipline->RETURN_MERIT_NAME($this_merittype));
				$this_row[] = $this_conduct;
				if($ldiscipline->UseSubScore)
					$this_row[] = $this_study_score;
				$this_row[] = ($this_pic ? $ldiscipline->getPICNameList($this_pic) : $Lang['General']['EmptySymbol']);
				
				$this_class_ary[] = $this_row;
				$i++;	
				$class_conduct_total += $this_conduct;
				$class_study_total += $this_study_score;
			}		
		}
		
		$export_ary_tablehead = array();
		$export_ary_tablehead[] = array($i_general_class, $this_classname);
		$export_ary_tablehead[] = array($Lang['eDiscipline']['DateRange'], $startdate. ' '. $i_To .' '. $enddate);
		$export_ary_tablehead[] = array($i_general_print_date, date("Y-m-d"));
		$export_ary_tablehead[] = array($Lang['eDiscipline']['TotalConductScore'], $class_conduct_total);
		if($ldiscipline->UseSubScore) {
			$export_ary_tablehead[] = array($Lang['eDiscipline']['TotalSubScore'], $class_study_total);
		}
		$export_ary_tablehead[] = array($Lang['eDiscipline']['TotalNoStudent'], sizeof($d));
		
		$export_ary = array_merge($export_ary, $export_ary_tablehead, $this_class_ary);
		
		$export_ary[] = array();
	}		
}
else
{
	$display .= "<div class='no_record_find_short_v30'>". $Lang['General']['NoRecordFound'] ."</div>";
}

$utf_content = "";

foreach($export_ary as $k=>$d)
{
	$utf_content .= implode("\t", $d);
	$utf_content .= "\r\n";
}

intranet_closedb();
	
$filename = "ap_class_report.csv";
$lexport->EXPORT_FILE($filename, $utf_content); 

// $export_text = $lexport->GET_EXPORT_TXT($export_ary,array(''));
// $filename = "abc.csv";
// $lexport->EXPORT_FILE($filename,$export_text); 

// debug_pr($export_ary);

intranet_closedb();
?>