<?php
// Editing by Bill

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# Access right checking
$ldiscipline = new libdisciplinev12();
if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$linterface = new interface_html();

// Condition
if($rankTarget == 'form'){
	$yearIdCond = " AND y.YearID IN ('".implode("','",(array)$rankTargetDetail)."') ";
}
else if($rankTarget == 'class'){
	$classIdCond = " AND yc.YearClassID IN ('".implode("','",(array)$rankTargetDetail)."') ";
}
else if($rankTarget == 'student'){
	$classIdCond = " AND yc.YearClassID IN ('".implode("','",(array)$rankTargetDetail)."') ";
	$studentIdCond = " AND ycu.UserID IN ('".implode("','",(array)$studentID)."')";
}
if($reportType=="H") {
	$catCond = " AND ar_log.CategoryID = '".$sys_custom['eDiscipline']['BWWTC_WithoutSubmCatID']."' ";
}
else if($reportType=="L") {
	$catCond = " AND ar_log.CategoryID = '".PRESET_CATEGORY_LATE."' ";
}
else {
	$catCond = " AND ar_log.CategoryID = '' ";
}

// Style
$EmptySymbol = $Lang['General']['EmptySymbol'];
$StylePrefix = '<span class="tabletextrequire">';
$StyleSuffix = '</span>';
if($format == 'csv'){
	$StylePrefix = '';
	$StyleSuffix = '';
}

// Get Cancel Records
$sql = "SELECT 
			ar_log.StudentID,
			yc.YearClassID,
			yc.ClassTitleB5 as ClassName,
			ycu.ClassNumber,
			IF(iu.UserID IS NOT NULL, iu.ChineseName, au.ChineseName) as StudentName,
			DATE_FORMAT(ar_log.GMRecordDate,'%e/%c/%Y') as GMRecordDate,
			DATE_FORMAT(ar_log.CancelGMDate,'%e/%c/%Y') as CancelGMDate,
			DATE_FORMAT(ar_log.CancelAPDate,'%e/%c/%Y') as CancelAPDate,
			mi.ItemName,
			ar_log.LogID,
			ar_log.MeritType,
			ar_log.NoticeID,
			ar_log.RemainingCount
		FROM
			DISCIPLINE_ACCU_RECORD_CANCEL_LOG as ar_log 
			LEFT JOIN DISCIPLINE_MERIT_ITEM as mi ON (mi.ItemID = ar_log.ItemID)
			LEFT JOIN INTRANET_USER as iu ON (iu.UserID = ar_log.StudentID) 
			LEFT JOIN INTRANET_ARCHIVE_USER as au ON (au.UserID = ar_log.StudentID) 
			LEFT JOIN YEAR_CLASS_USER as ycu ON (ycu.UserID = ar_log.StudentID OR au.UserID = ycu.UserID) 
			LEFT JOIN YEAR_CLASS as yc ON (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '$selectYear') 
			LEFT JOIN YEAR as y ON (y.YearID = yc.YearID) 
		WHERE
			ar_log.AcademicYearID = '$selectYear' AND (ar_log.CancelGMDate BETWEEN '$textFromDate 00:00:00' AND '$textToDate 23:59:59')
			$catCond
			$yearIdCond $classIdCond $studentIdCond 
		GROUP BY
			ar_log.LogID 
		ORDER BY
			ar_log.CancelGMDate DESC, y.Sequence ASC, yc.Sequence ASC, ycu.ClassNumber+0 ASC";
$records = $ldiscipline->returnResultSet($sql);

// Report Header
if($format == 'print'){
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
}
$x .= "<style type='text/css' media='print'> .print-style { page-break-inside: avoid; } </style>";
$x .= "<style type='text/css'>";
	$x .= ".main_container { display: block; width: 713px; height: 920px; margin: auto; padding: 30px 20px; position: relative; }";
	$x .= ".print-style table { padding: 4px; } ";
	$x .= ".print-style td { font-size: 19px; padding: 2px; text-align: left; } ";
	$x .= ".print-style td.report_main_content { font-size: 21px; } ";
	$x .= ".print-style table.report_header td { padding-bottom: 7px; } ";
	$x .= ".print-style table.report_result td.report_main_content { padding: 6px 2px 6px 2px; } ";
	$x .= ".print-style table.report_footer { padding-bottom: 30px; } ";
	$x .= ".page_break { page-break-after: always; margin: 0; padding: 0; line-height: 0; font-size: 0; height: 0; }";
$x .= "</style>";

if($format == 'web'){
	$x .= '<div class="content_top_tool">
			<div class="Conntent_tool">
				<!--<a href="javascript:submitForm(\'csv\');" class="export">'.$Lang['Btn']['Export'].'</a>-->
				<a href="javascript:submitForm(\'print\')" class="print">'.$Lang['Btn']['Print'].'</a>
			</div>
			<br style="clear:both" />
		  </div><br />';
}
if($format == 'print'){
	$x .= '<table width="100%" align="center" class="print_hide" border="0">
			<tr>
				<td align="right">'.$linterface->GET_SMALL_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit_print").'</td>
			</tr>
			</table>';
}

$record_size = count($records);
if($record_size > 0) {
	$YearClassIDAry = Get_Array_By_Key((array)$records, "YearClassID");
	$YearClassIDAry = array_unique((array)$YearClassIDAry);
	
	$sql = "Select 
				yct.YearClassID, IF(iu.UserID IS NOT NULL, iu.ChineseName, au.ChineseName) as TeacherName
			From
				YEAR_CLASS_TEACHER as yct 
				LEFT JOIN INTRANET_USER as iu ON (yct.UserID = iu.UserID)
				LEFT JOIN INTRANET_ARCHIVE_USER as au ON (yct.UserID = au.UserID) 
			Where 
				yct.YearClassID IN ('".implode("', '", (array)$YearClassIDAry)."')";
	$teacherAry = $ldiscipline->returnResultSet($sql);
	$teacherAry = BuildMultiKeyAssoc((array)$teacherAry, array("YearClassID", "TeacherName"));
	
	$x .= "<div class='main_container'>";
	
	// loop records
	$records = BuildMultiKeyAssoc((array)$records, array("CancelGMDate", "YearClassID", "MeritType", "LogID"));
	foreach((array)$records as $date_records) {
		foreach((array)$date_records as $class_records) {
			foreach((array)$class_records as $merit_records)
			{
				$isFirstRecords = true;
				foreach((array)$merit_records as $cancelInfo)
				{
					// Record Grid Header
					if($isFirstRecords)
					{
						// Item Name (old data : 屢欠功課(依家課管理計劃守則))
						$thisMeritItemName = $cancelInfo["ItemName"];
						$thisMeritItemName = $thisMeritItemName==""? "屢欠功課(依家課管理計劃守則)" : $thisMeritItemName;
						
						// Merit Type (old data : 缺點)
						$thisMeritType = $cancelInfo["MeritType"];
						$thisMeritType = empty($thisMeritType) && $thisMeritType!=="0"? "-1" : $thisMeritType;
						$thisMeritTypeName = $Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['PunishmentAry'][$thisMeritType];
						$thisMeritTypeName = $thisMeritTypeName==""? "" : " ".$thisMeritTypeName;
						
						// Release Notice (old data : 家長信)
						$thisHasNotice = $cancelInfo["NoticeID"]!=="0";
						$thisHasNoticeDisplay = $thisHasNotice? " 家長信" : "";
						
						// Class Teacher
						$thisClassTeacherDisplay = "";
						$thisClassTeacher = $teacherAry[$cancelInfo["YearClassID"]];
						if(!empty($thisClassTeacher)) {
							$thisClassTeacher = array_keys((array)$thisClassTeacher);
							$thisClassTeacherDisplay = implode('、', (array)$thisClassTeacher);
						}
						
						$y = "";
						$y .= "<div class='print-style'>";
							$y .= "<table class='report_header' style='width:100%; border:0px; text-align:center;'>";
								$y .= "<tr><td colspan='5' class='report_main_content' style='text-align: center'>TWGHs Wong Fut Nam College</td></tr>";
								$y .= "<tr>";
									$y .= "<td width='5%'>&nbsp;</td>";
									$y .= "<td width='25%'>".$cancelInfo["ClassName"]."</td>";
									$y .= "<td width='50%'>班主任: ".$thisClassTeacherDisplay."</td>";
									$y .= "<td width='15%' style='text-align: right'>".$cancelInfo["CancelGMDate"]."</td>";
									$y .= "<td width='5%'>&nbsp;</td>";
								$y .= "</tr>";
							$y .= "</table>";
							$y .= "<table class='report_result' style='width:100%; border:4px solid black; text-align:center;'>";
								$y .= "<tr>";
									$y .= "<td colspan='2'>&nbsp;</td>";
									$y .= "<td style='border-bottom: 1px solid black;'>&nbsp;欠交功課／遲到總數</td>";
								$y .= "</tr>";
								$y .= "<tr>";
										//$y .= "<td colspan='3' class='report_main_content'>於".$cancelInfo["CancelAPDate"]."所發之屢欠功課(依家課管理計劃守則) 缺點 家長信已取消</td>";
										$y .= "<td colspan='3' class='report_main_content'>於".$cancelInfo["CancelAPDate"]."所發之".$thisMeritItemName.$thisMeritTypeName.$thisHasNoticeDisplay."已取消</td>";
								$y .= "</tr>";
								
						$isFirstRecords = false;
					}
					
					// Grid Content Row 
					$y .= "<tr class='data_tr'>";
						$y .= "<td width='20%'>&nbsp;</td>";
						$y .= "<td width='52%'>".$cancelInfo["ClassNumber"]."&nbsp;&nbsp;".$cancelInfo["StudentName"]."</td>";
						$y .= "<td width='28%' style='text-align:center;'>".$cancelInfo["RemainingCount"]."</td>";
					$y .= "</tr>";
				}
				
				// Record Grid Footer
				if(!$isFirstRecords)
				{
							$y .= "<tr>";
								$y .= "<td colspan='3' style='border-top: 1px solid black; font-size: 18px;'>&nbsp;</td>";
							$y .= "</tr>";
						$y .= "</table>";
						$y .= "<table class='report_footer' style='width:100%; border:0px; text-align:center;'>";
							$y .= "<tr>";
								$y .= "<td width='80%' align='left'>*請班主任通知學生</td>";
								$y .= "<td width='20%' align='right'>(班主任保存)</td>";
							$y .= "</tr>";
						$y .= "</table>";
					$y .= "</div>";
					$x .= $y;
				}
			}
		}
	}
	$x .= "</div>";
}

//if($format == 'csv'){
//	$lexport->EXPORT_FILE("eDiscipline_Accumulated_Late_Report.csv", $exportContent);
//}else{
	echo $x;
//}

if($format == 'print'){
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
}

intranet_closedb();
?>