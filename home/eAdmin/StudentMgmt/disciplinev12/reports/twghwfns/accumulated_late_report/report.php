<?php
// Editing by Carlos
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# user access right checking
$ldiscipline = new libdisciplinev12();
if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$linterface = new interface_html();

/*
$format = $_POST['format'];
$rankTarget = $_POST['rankTarget'];
$radioPeriod = $_POST['radioPeriod'];
$selectYear = $_POST['selectYear']; 
$selectSemester = $_POST['selectSemester'];
$textFromDate = $_POST['textFromDate'];
$textToDate = $_POST['textToDate'];
*/

if($rankTarget == 'form'){
	$yearIdCond = " AND y.YearID IN ('".implode("','",(array)$rankTargetDetail)."') ";
}else if($rankTarget == 'class'){
	$classIdCond = " AND yc.YearClassID IN ('".implode("','",(array)$rankTargetDetail)."') ";
}else if($rankTarget == 'student'){
	$classIdCond = " AND yc.YearClassID IN ('".implode("','",(array)$rankTargetDetail)."') ";
	$studentIdCond = " AND ycu.UserID IN ('".implode("','",(array)$studentID)."')";
}

$EmptySymbol = $Lang['General']['EmptySymbol'];
$StylePrefix = '<span class="tabletextrequire">';
$StyleSuffix = '</span>';
if($format == 'csv'){
	$StylePrefix = '';
	$StyleSuffix = '';
}
		
$YearClassNameField = Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN');
$StudentNameField = getNameFieldByLang("iu.");

$clsName = $YearClassNameField;

$late_category_id = PRESET_CATEGORY_LATE;

$sql = "SELECT 
			r.StudentID,
			$YearClassNameField as ClassName,
			ycu.ClassNumber,
			IF(iu.UserID IS NOT NULL,iu.ChineseName,au.ChineseName) as ChineseName,
			IF(iu.UserID IS NOT NULL,iu.EnglishName,au.EnglishName) as EnglishName,
			DATE_FORMAT(r.RecordDate,'%Y-%m-%d') as RecordDate  
		FROM DISCIPLINE_ACCU_RECORD as r 
		LEFT JOIN INTRANET_USER as iu ON r.StudentID=iu.UserID 
		LEFT JOIN INTRANET_ARCHIVE_USER as au ON au.UserID=r.StudentID 
		LEFT JOIN YEAR_CLASS_USER as ycu ON (ycu.UserID=r.StudentID OR au.UserID=ycu.UserID) 
		LEFT JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='$selectYear' 
		LEFT JOIN YEAR as y ON y.YearID=yc.YearID 
		WHERE r.CategoryID='$late_category_id' AND r.RecordType='-1' AND r.RecordStatus='1' AND r.AcademicYearID='$selectYear' ".(!in_array($selectSemester,array('','0'))?" AND r.YearTermID='$selectSemester' ":"")." 
			AND (r.RecordDate BETWEEN '$textFromDate 00:00:00' AND '$textToDate 23:59:59')  
			AND yc.AcademicYearID = '".$selectYear."' 
		$yearIdCond $classIdCond $studentIdCond 
		GROUP BY r.RecordID 
		ORDER BY y.Sequence, yc.Sequence, ycu.ClassNumber+0,r.RecordDate";
$records = $ldiscipline->returnResultSet($sql);
$record_size = count($records);

$studentIdToRecords = array();

for($i=0;$i<$record_size;$i++)
{
	if(!isset($studentIdToRecords[$records[$i]['StudentID']])){
		$studentIdToRecords[$records[$i]['StudentID']] = array();
	}
	$studentIdToRecords[$records[$i]['StudentID']][] = $records[$i];
}

$student_size = count($studentIdToRecords);

if($format == 'print'){
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
}

$x .= '<style type="text/css" media="print"> .print-style { page-break-inside: avoid;}</style>';

if($format == 'web'){
	$x .= '<div class="content_top_tool">
			<div class="Conntent_tool">
				<!--<a href="javascript:submitForm(\'csv\');" class="export">'.$Lang['Btn']['Export'].'</a>-->
				<a href="javascript:submitForm(\'print\')" class="print">'.$Lang['Btn']['Print'].'</a>
			</div>
			<br style="clear:both" />
		  </div><br />';
}

if($format == 'print'){
	$x .= '<table width="100%" align="center" class="print_hide" border="0">
			<tr>
				<td align="right">'.$linterface->GET_SMALL_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit_print").'</td>
			</tr>
			</table>';
}

//if($format == 'csv'){
//	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
//	$lexport = new libexporttext();	
//	$exportContent = '';
//}

if($format == 'print'){
	$school_name = GET_SCHOOL_NAME();
	
	$x .= '<h2 style="text-align:center;">'.$school_name.'</h2>';
}

if($student_size > 0)
{
	foreach($studentIdToRecords as $student_id => $ary)
	{
		
		$ary_count = count($ary);
		$y = '<div class="print-style">';
		$y .= '<h3>'.str_replace(array('<!--DATE-->','<!--COUNT-->'),array(date("d/m/Y",strtotime($textToDate)),$ary_count),'下列學生已於 <!--DATE--> 遲到累積達 <!--COUNT--> 次').':</h3>';
		$y .= '<div style="border:2px solid black;padding:0.5em;">';
			$y .= '班別    '.$ary[0]['ClassName'];
			$y .= '<div style="padding:1em 2em 1em 5em;">';
			$y .= '<table style="width:100%;border-collapse:collapse;border:1px solid black;text-align:center;">';
				$y .= '<tr style="border-bottom:1px solid black;font-style:italic;">';
					$y .= '<th width="10%" style="padding:4px;">班號</th><th colspan="2" width="40%" style="padding:4px;">學生姓名</th><th width="30%" style="padding:4px;">日期</th><th width="10%" style="padding:4px;">豁免</th><th width="10%" style="padding:4px;">請假</th>';
				$y .= '</tr>';
				$y.='<tr">';
					$y .= '<td style="padding:4px;">'.$ary[0]['ClassNumber'].'</td><td style="padding:4px;">'.$ary[0]['ChineseName'].'</td><td style="padding:4px;">'.$ary[0]['EnglishName'].'</td><td colspan="3">&nbsp;</td>';
				$y.='</tr>';
				for($i=0;$i<$ary_count;$i++){
					$format_date = date("d/m/Y",strtotime($ary[$i]['RecordDate']));
					$y .= '<tr>';
						$y .= '<td colspan="3">&nbsp;</td><td style="padding:4px;">'.$format_date.'</td><td><span style="display:inline-block;border:1px solid black;width:1em;height:1em;"></span></td><td><span style="display:inline-block;border:1px solid black;width:1em;height:1em;"></span></td>';
					$y .= '</tr>';
				}
			$y .= '</table>';
			$y .= '</div>';
		$y .= '</div><br />';
		$y .= '</div>';
		$x .= $y;
	}
}

//if($format == 'csv'){
//	$lexport->EXPORT_FILE("eDiscipline_Accumulated_Late_Report.csv", $exportContent);
//}else{
	echo $x;
//}

if($format == 'print'){
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
}

intranet_closedb();
?>