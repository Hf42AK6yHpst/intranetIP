<?php
// Editing by 

/***************
 * 
 ***************/
 
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Access right checking
if(!$sys_custom['eDiscipline']['WFN_Reports'] || !$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$linterface = new interface_html();

# Setting of Current Page
$CurrentPage = "Reports_TWGHWFNS_StudentDetention";
$CurrentPageArr['eDisciplinev12'] = 1;

# Tab
$TAGS_OBJ[] = array($Lang['eDiscipline']['TWGHWFNS_StudentDetention']['Title'], "", 1);
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Year and Date
$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
$textDate = date("Y-m-d");

# School Year
$selectYear = ($selectYear == '') ? Get_Current_Academic_Year_ID() : $selectYear;
$years = $ldiscipline->returnAllYearsSelectionArray();
$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='selectYear' id='selectYear' onclick='showResult()' ", "", $selectYear);

$linterface->LAYOUT_START();
?>
<script type="text/javascript" language="JavaScript">

function showResult()
{
	var yearClassId = '';
	
	var url = "../../ajaxGetLive.php?target=class";
	url += "&fieldId=rankTargetDetail[]&fieldName=rankTargetDetail[]";
	url += "&academicYearId=" + $('#selectYear').val();
	$.get(
		url,
		{},
		function(responseText){
			var showIn = "rankTargetDetail";
			$('#'+showIn).html(responseText);
			document.getElementById(showIn).style.border = "0px solid #A5ACB2";
		}
	);
}

function changeSelectReportTarget(type)
{
	if(type == 1){
		$("#classSelectionTR").hide();
		$("#remarksTR").hide();
	}
	else{
		$("#classSelectionTR").show();
		$("#remarksTR").show();
	}
}

function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function submitForm(format)
{
	var valid = true;
	var form_obj = $('#form1');
	
	var text_date = $.trim($('#textDate').val());
	if(text_date == ''){
		valid = false;
		$('#textDate').focus();
	}
	if(valid){
		if(!check_date_without_return_msg(document.getElementById('textDate'))){
			valid = false;
			$('#textDate').focus();
		}
	}
	
	if($('select#rankTargetDetail\\[\\] option:selected').length == 0){
		valid = false;
		$('#TargetWarnDiv span').html('<?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?>');
		$('#TargetWarnDiv').show();
	}
	else{
		$('#TargetWarnDiv').hide();
	}
	
	if(!valid) return;
	$('input#format').val(format);
	if(format == 'web'){
		form_obj.attr('target', '_blank');
		form_obj.submit();
	}
	else if(format == 'csv'){
		form_obj.attr('target', '');
		form_obj.submit();
	}
	else if(format == 'pdf'){
		form_obj.attr('target', '_blank');
		form_obj.submit();
	}
}

$(document).ready(function(){
	changeSelectReportTarget('1');
	showResult();
});

</script>

<div id="PageDiv">
<div id="div_form">
	<p class="spacer"></p> 
	
	<span class="Form_Span">
		<form id="form1" name="form1" method="post" action="report.php">
		<table class="form_table_v30">
		
			<!-- Year -->
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['General']['SchoolYear']?></td>
				<td><table class="inside_form_table">
						<tr><td colspan="6"">
								<?=$selectSchoolYearHTML?>
						</td></tr>
				</table></td>
			</tr>
			
			<!-- Date Range -->
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$i_Discipline_Date?></td>
				<td><table class="inside_form_table">
						<tr><td>
								<?=$linterface->GET_DATE_PICKER("textDate", $textDate, "")?>
						</td></tr>
				</table></td>
			</tr>
		
			<!-- Target -->
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$i_Discipline_Target?></td>
				<td>
					<table class="inside_form_table">
						<tr><td colspan="3">
								<input type="radio" name="targetType" value="school" id="TargetType_School" onclick="changeSelectReportTarget('1')" checked><label for="TargetType_School"><?=$i_general_WholeSchool?></label>&nbsp;&nbsp;
								<input type="radio" name="targetType" value="class" id="TargetType_Class" onclick="changeSelectReportTarget('2')"><label for="TargetType_Class"><?=$i_general_class?></label>
						</td></tr>
						<tr id='classSelectionTR'><td valign="top" nowrap>
								<span id='rankTargetDetail'>
									<select name="rankTargetDetail[]" id="rankTargetDetail[]" size="5"></select>
								</span>
								<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('rankTargetDetail[]'));return false;", "selectAllBtn01")?>
						</td></tr>
						<tr id='remarksTR'><td colspan="3" class="tabletextremark">(<?=$Lang['eDiscipline']['TWGHWFNS_StudentDetention']['PressCtrlKey']?>)</td></tr>
					</table>
					<span id='div_Target_err_msg'></span>
					<?=$linterface->Get_Form_Warning_Msg("TargetWarnDiv","")?>
				</td>
			</tr>
		</table>
		
		<?=$linterface->MandatoryField();?>
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "submitForm('web');")?>
		</div>
		
		<input type="hidden" name="format" id="format" value="web">
		</form>
	</span>
</div>

<div id="ReportDiv"></div>
</div>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>