<?php
// Using: Bill

############# Change Log [Start] ################
#
#   Date:   2018-12-18 (Bill)   [2018-0627-1537-01277]
#           Create file
#
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_cust.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libattendance.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12_cust();

# Access right checking
$teachingClasses = $ldiscipline->getTeachingClassesByTeacherID();
if(!$sys_custom['eDiscipline']['HCY_ClassRecordCountReport'] || (!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && empty($teachingClasses))) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$ldiscipline_ui = new libdisciplinev12_ui_cust();
$lattend = new libattendance();

# POST data
$format = $_POST['format'];
$rankTarget = $_POST['rankTarget'];
$rankTargetDetail = $_POST['rankTargetDetail'];
$radioPeriod = $_POST['radioPeriod'];
$selectYear = $_POST['selectYear'];
$textFromDate = $_POST['textFromDate'];
$textToDate = $_POST['textToDate'];

# Period Settings
if($radioPeriod == 'YEAR') {
    $textFromDate = date("Y-m-d", strtotime(getStartDateOfAcademicYear($selectYear, '')));
    $textToDate = date("Y-m-d", strtotime(getEndDateOfAcademicYear($selectYear, '')));
} else {
    $selectYear = Get_Current_Academic_Year_ID();
}

# Date range
list($start_year, $start_month, $start_day) = explode("-", $textFromDate);
list($end_year, $end_month, $end_day) = explode("-", $textToDate);
$date_range = "$start_year 年 $start_month 月 $start_day 日 - $end_year 年 $end_month 月 $end_day 日";

# Semester
// $semester = (array)getSemesters($selectYear);
// $semester = array_keys($semester);

# Rank Target
if($rankTarget == 'form') {
    $rankTargetDetail = $ldiscipline->getClassIDByLevelID($rankTargetDetail, $selectYear);
}
$classIdCond = " AND yc.YearClassID IN ('".implode("','", (array)$rankTargetDetail)."') ";

# Prefix Style
if($format == 'print')
{
	$StylePrefix = '<span class="tabletextrequire">';
	$StyleSuffix = '</span>';
}

# Get Target Students
$clsName = "yc.ClassTitleEN";	
$sql = "SELECT 
			iu.UserID,
            y.YearID,
            y.YearName,
			yc.YearClassID,
			yc.ClassTitleEN as ClassName,
			ycu.ClassNumber,
			CONCAT(
				IF($clsName IS NULL OR $clsName='' OR ycu.ClassNumber IS NULL OR ycu.ClassNumber='' OR iu.RecordStatus != 1, '".$StylePrefix."^".$StyleSuffix."', ''),
				iu.ChineseName
			) as StudentName
		FROM
            INTRANET_USER as iu 
    		LEFT JOIN YEAR_CLASS_USER as ycu ON ycu.UserID = iu.UserID
    		LEFT JOIN YEAR_CLASS as yc ON (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '$selectYear')
    		LEFT JOIN YEAR as y ON y.YearID = yc.YearID
		WHERE
            yc.AcademicYearID = '".$selectYear."' 
            $classIdCond 
		GROUP BY
            iu.UserID 
		ORDER BY
            y.Sequence, yc.Sequence, ycu.ClassNumber + 0";
$students = $ldiscipline->returnResultSet($sql);
$student_ids = Get_Array_By_Key($students, "UserID");

//$targetFields = $rankTarget == 'form'? 'YearID' : 'YearClassID';
$student_ary = BuildMultiKeyAssoc((array)$students, array('YearClassID', 'UserID'));
$student_count = count($students);

# Good Conduct / Misconduct - Approved
$sql = "SELECT
			r.RecordID,
			r.StudentID,
			r.CategoryID,
			cat.Name,
			r.RecordType,
			SUM(1) AS GMRecordCount
		FROM 
			DISCIPLINE_ACCU_RECORD as r
            INNER JOIN DISCIPLINE_ACCU_CATEGORY as cat ON cat.CategoryID = r.CategoryID
		WHERE 
			r.StudentID IN ('".implode("','", (array)$student_ids)."') AND 
            r.AcademicYearID = '$selectYear' AND r.RecordDate >= '$textFromDate 00:00:00' AND r.RecordDate <= '$textToDate 23:59:59' AND 
            r.RecordStatus = '".DISCIPLINE_STATUS_APPROVED."'
		GROUP BY 
			r.StudentID, r.RecordType, r.CategoryID";
$gm_records = $ldiscipline->returnResultSet($sql);
$gm_records = BuildMultiKeyAssoc((array)$gm_records, array("StudentID", "CategoryID"));

# Punishment - Approved and Released
$sql = "SELECT 
			r.RecordID,
			r.StudentID,
			m.ItemCode,
			m.ItemName,
			r.ProfileMeritType,
			DATE_FORMAT(r.RecordDate,'%m-%d') as RecordDate,
			ROUND(r.ProfileMeritCount) AS ProfileMeritCount
		FROM 
			DISCIPLINE_MERIT_RECORD as r
            INNER JOIN DISCIPLINE_MERIT_ITEM as m ON m.ItemID = r.ItemID
		WHERE 
			r.StudentID IN ('".implode("','", (array)$student_ids)."') AND 
            r.AcademicYearID = '$selectYear' AND r.RecordDate >= '$textFromDate' AND r.RecordDate <= '$textToDate' AND 
			r.MeritType = '-1' AND r.ProfileMeritType IN ('-1', '-2', '-3') AND 
            r.RecordStatus = '".DISCIPLINE_STATUS_APPROVED."' AND r.ReleaseStatus = '".DISCIPLINE_STATUS_RELEASED."'
		ORDER BY 
			r.RecordDate";
$ap_records = $ldiscipline->returnResultSet($sql);
$ap_records = BuildMultiKeyAssoc((array)$ap_records, array("StudentID", "RecordID"));

# Punishment - Merit Type Count
$ap_record_count = array();
foreach((array)$ap_records as $student_id => $student_records)
{
    $ap_record_count[$student_id] = array();
    $ap_record_count[$student_id][-1] = 0;
    $ap_record_count[$student_id][-2] = 0;
    $ap_record_count[$student_id][-3] = 0;
    foreach((array)$student_records as $this_records)
    {
        $ap_record_count[$student_id][$this_records['ProfileMeritType']] += $this_records['ProfileMeritCount'];
    }
}

# Absent
$absent_records = $lattend->getAttendanceListByClass('', $textFromDate, $textToDate, '', $student_ids);
$absent_records = BuildMultiKeyAssoc((array)$absent_records, array(0));

# Report Format - CSV
if($format == 'csv')
{
	# Initiate Object
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	$lexport = new libexporttext();
	
	# CSV Header
	$header = array("各班操行數字總表(".getAYNameByAyId($selectYear,"b5").")", "$date_range");
	
	# CSV Data
    $rows = $ldiscipline_ui->getHCYClassRecordSummary($format, $rankTargetDetail, $student_ary, $gm_records, $ap_records, $ap_record_count, $absent_records);
	
    # Export CSV
	$exportContent = $lexport->GET_EXPORT_TXT($rows, $header);
	$lexport->EXPORT_FILE("ClassRecordTable.csv", $exportContent);
}
# Report Format - print
else if($format == 'print')
{
	# Report CSS
	$x = "  <head>
            <style TYPE='text/css'>
                div#container {
                	margin: 0 auto;
                	width: 760px;
                }
				.report_header {
					font-size: 14px;
				}
				.result_table {
                    border-collapse: collapse;
				}
				table.result_table td {
					border: 1px solid black;
					padding: 1px;
					font-size: 11px;
					text-align: center;
				}
			</style>
            </head>";
	echo $x;
	
	# Report Header
	$report_header_row = "<tr><td align='center'>各班操行數字總表(".getAYNameByAyId($selectYear, 'b5').")</td></tr>
                            <tr><td align='right'>$date_range</td></tr>";
	
	# Report Content
	echo '<body>';
	echo $ldiscipline_ui->getHCYClassRecordSummary($format, $rankTargetDetail, $student_ary, $gm_records, $ap_records, $ap_record_count, $absent_records, $report_header_row);
    echo '</body>';
}
else 
{
	echo "Not support";
}

intranet_closedb();
?>