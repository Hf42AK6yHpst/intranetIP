<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();


$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lexport = new libexporttext();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-GoodConduct_Misconduct_Ranking_Report-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

ini_set("memory_limit", "150M"); 
$filename = "gm_ranking_report.csv";	

//header("Content-Type:text/html;charset=utf-8");


# School Year Menu #

//$selectSchoolYear .= "<select name='SchoolYear'>";
$selectSchoolYear .= "<option value='0'";
$selectSchoolYear .= ($SchoolYear==0) ? " selected" : "";
$selectSchoolYear .= ">".$i_Discipline_System_Award_Punishment_All_School_Year."</option>";
$selectSchoolYear .= $ldiscipline->getConductSchoolYear($SchoolYear);
//$selectSchoolYear .= "</select>";

$conds .= " AND a.RecordStatus=".DISCIPLINE_STATUS_APPROVED;

$meritTitle = $i_Discipline_GoodConduct." & ".$i_Discipline_Misconduct;

$semesterText = ($semester=='WholeYear' || $semester==0) ? $i_Discipline_System_Award_Punishment_Whole_Year : $ldiscipline->getTermNameByTermID($semester);

# Semester Menu #
if($dateChoice==2) {
	$date = $startDate ." {$i_To} ".$endDate;
	$conds .= " AND a.RecordDate BETWEEN '$startDate' AND '$endDate'";
} else {
	if($SchoolYear != '0') {		# specific school year (not "All School Year")
		$date = $ldiscipline->getAcademicYearNameByYearID($SchoolYear)." ".$semesterText;
		list($startYear, $endYear) = split('-',$SchoolYear);
		//$conds .= " AND a.Year='$SchoolYear'";
		$conds .= " AND a.AcademicYearID=$SchoolYear";
	} else {						# all school year
		$date = $i_Discipline_System_Award_Punishment_All_School_Year." ".$semesterText;
		$academicYear = $ldiscipline->generateAllSchoolYear();
		$tempConds = "";
	}
	$conds .= ($semester != 'WholeYear' && $semester!=0) ? " AND a.Semester='$semester'" : "";
}

if($typeBy=='byCategory') {
	if(is_array($selectGoodCat) && is_array($selectMisCat))
		$categoryID = array_merge($selectGoodCat, $selectMisCat);
	else if(is_array($selectGoodCat))
		$categoryID = $selectGoodCat;
	else 
		$categoryID = $selectMisCat;
}
if($typeBy=='byItem') {
	if(is_array($ItemID1) && is_array($ItemID2))
		$ItemID = array_merge($ItemID1, $ItemID2);
	else if(is_array($ItemID1))
		$ItemID = $ItemID1;
	else 
		$ItemID = $ItemID2;
}

$tempConds = "";
if($rankTarget=='form') {
	for($i=0;$i<sizeof($rankTargetDetail);$i++) {
		$tempConds .= ($tempConds!="") ? " OR " : "";
		//$tempConds .= " d.ClassLevelID=$rankTargetDetail[$i]";
		$tempConds .= " y.YearID=$rankTargetDetail[$i]";
	}
} else if($rankTarget=='class') {
	for($i=0;$i<sizeof($rankTargetDetail);$i++) {
		$tempConds .= ($tempConds!="") ? " OR " : "";
		//$tempConds .= " b.ClassName='$rankTargetDetail[$i]'";
		$tempConds .= " yc.YearClassID='$rankTargetDetail[$i]'";
	}
} else {
	$tempConds .= " a.StudentID IN (".implode(',',$studentID).")";	
}
$conds .= " AND ($tempConds)";

if($typeBy=='byCategory')
	$categoryID = array_merge($selectGoodCat, $selectMisCat);
	
if($typeBy=='byCategory' && sizeof($categoryID)>0)
	$conds .= " AND f.CategoryID IN (".implode(',',$categoryID).")";
	
$selectedItemTextAry = explode(',',$SelectedItemText);
	
if($typeBy=='byItem' && sizeof($ItemID)>0) {
	
	for($i=0; $i<sizeof($ItemID); $i++) {
		$itemData = $ldiscipline->getConductItemDataByID($ItemID[$i], $selectedItemTextAry[$i]);
		$tempConds .= ($tempConds!='') ? " OR (a.ItemID='".$ItemID[$i]."' AND a.CategoryID='".$itemData['ID']."')" : " (a.ItemID='".$ItemID[$i]."' AND a.CategoryID='".$itemData['ID']."')";
		
	}
	
	if($tempConds!="")
		$conds .= " AND (".$tempConds.")";
	
}

# Group By
if($rankTarget=="form") 
	$groupBy = "form";
else if($rankTarget=="class")
	$groupBy = "name";
else 
	$groupBy = "b.UserID";
	
if($typeBy=='byType')
	//$groupBy .= ", a.RecordType ";
	$groupBy .= "";
else if($typeBy=='byCategory')	
	$groupBy .= ", e.CategoryID ";
else 
	$groupBy .= ", a.ItemID ";

	
switch($rankTarget) {
	case("class") : $criteria = $i_Discipline_Class; break;
	case("form") : $criteria = $i_Discipline_Form; break;
	case("student") : $criteria = $i_Discipline_Student; break;
	default: break;
}

$exportArr = array();
//$exportColumn = array($iDiscipline['Rank'], $i_Discipline_Class, $i_Discipline_No_of_Records);
$exportColumn = array("","","");

$row = 0;

# table content 
if($typeBy=='byType') {
	
	for($i=0; $i<sizeof($record_type); $i++) {
		
		$tempConds = ($record_type[$i]==1) ? " AND a.RecordType=1" : " AND (a.RecordType=-1 OR a.RecordType IS NULL)";

		$temp = $ldiscipline->getGMRankingData('type', $record_type[$i], $conds.$tempConds, $groupBy);
		list($lvlName, $countTotal, $className, $catid, $catname, $itemid, $itemname, $meritType, $stdname) = $temp[$i];
		$exportArr[$row][0] = ($record_type[$i]==1) ? $i_Discipline_GoodConduct : $i_Discipline_Misconduct;
		$row++;
		
		if(sizeof($temp)>0) {
			$exportArr[$row][0] = $iDiscipline['Rank'];
			$exportArr[$row][1] = $criteria;
			$exportArr[$row][2] = $i_Discipline_No_of_Records;
			$row++;
			
			for($j=0; $j<sizeof($temp) && $j<$rankRange; $j++) {
				$k = $j+1;
				
				switch($rankTarget) {
					case("class") : $showName = $temp[$j][2]; break;
					case("form") : $showName = $temp[$j][0]; break;
					case("student") : $showName = $temp[$j][8]; break;
					default: break;
				}	
				$exportArr[$row][0] = $k;
				$exportArr[$row][1] = $showName;
				$exportArr[$row][2] = $temp[$j][1];
				
				$row++;
			}
		} else {
			$exportArr[$row][0] = $i_no_record_exists_msg;	
			$row++;
		}
		$exportArr[$row][0] = "";
		$row++;
	}
	
} else if($typeBy=='byCategory'){
	
	$selectedCategory = array_merge($selectGoodCat, $selectMisCat);
	
	for($i=0; $i<sizeof($selectedCategory); $i++) {
	
		$temp = $ldiscipline->getGMRankingData('category', $selectedCategory[$i], $conds, $groupBy);
		
		list($lvlName, $countTotal, $className, $catid, $catname, $itemid, $itemname, $meritType, $stdname) = $temp[$i];
		
		$categoryData = $ldiscipline->getGMCategoryInfoByCategoryID($selectedCategory[$i]);
		$meritTypeByCat = $categoryData['MeritType'];
		
		$exportArr[$row][0] = ($meritTypeByCat==1) ? $i_Discipline_GoodConduct : $i_Discipline_Misconduct;
		
		$GMCategory = $ldiscipline->getGMCategoryInfoByCategoryID($selectedCategory[$i]);
		$exportArr[$row][0] .= " > ".$GMCategory['Name'];
		$row++;
		
		if(sizeof($temp)>0) {
			$exportArr[$row][0] = $iDiscipline['Rank'];
			$exportArr[$row][1] = $criteria;
			$exportArr[$row][2] = $i_Discipline_No_of_Records;
			$row++;
			
			for($j=0; $j<sizeof($temp) && $j<$rankRange; $j++) {
				$k = $j+1;
				
				switch($rankTarget) {
					case("class") : $showName = $temp[$j][2]; break;
					case("form") : $showName = $temp[$j][0]; break;
					case("student") : $showName = $temp[$j][8]; break;
					default: break;
				}	
				
				$exportArr[$row][0] = $k;
				$exportArr[$row][1] = $showName;
				$exportArr[$row][2] = $temp[$j][1];
				
				$row++;
			}
		} else {
			$exportArr[$row][0] = $i_no_record_exists_msg;	
			$row++;
		}
		$exportArr[$row][0] = "";
		$row++;
	}		
} else if($typeBy=='byItem'){

	$selectedItem = array_merge($ItemID1, $ItemID2);
	$selectedItemText = explode(',',$SelectedItemText);
	
	for($i=0; $i<sizeof($selectedItem); $i++) {
		
		$temp = $ldiscipline->getGMRankingData('item', $selectedItem[$i], $conds, $groupBy);
	
		list($lvlName, $countTotal, $className, $catid, $catname, $itemid, $itemname, $meritType, $stdname) = $temp[$i];
		
		$itemData = $ldiscipline->getConductItemDataByID($selectedItem[$i], $selectedItemTextAry[$i]);		
		$meritTypeByItem = $itemData['RecordType'];
		
		$exportArr[$row][0] = ($meritTypeByItem==1) ? $i_Discipline_GoodConduct : $i_Discipline_Misconduct;
	
		$GMItem = $ldiscipline->getGMItemInfoByCategoryID($selectedItem[$i], $selectedItemText[$i]);
        if(sizeof($GMItem)==0) {
                $subjectName = " > ".$ldiscipline->RETURN_CONDUCT_REASON($selectedItem[$i], 2);
        }
		$exportArr[$row][0] .= ($subjectName) ? " > ".$subjectName : " > ".$GMItem['CategoryName']." > ".$GMItem['ItemName'];
		$row++;
		
		if(sizeof($temp)>0) {
			$exportArr[$row][0] = $iDiscipline['Rank'];
			$exportArr[$row][1] = $criteria;
			$exportArr[$row][2] = $i_Discipline_No_of_Records;
			$row++;
			
			for($j=0; $j<sizeof($temp) && $j<$rankRange; $j++) {
				$k = $j+1;
				
				switch($rankTarget) {
					case("class") : $showName = $temp[$j][2]; break;
					case("form") : $showName = $temp[$j][0]; break;
					case("student") : $showName = $temp[$j][8]; break;
					default: break;
				}	
				
				$exportArr[$row][0] = $k;
				$exportArr[$row][1] = $showName;
				$exportArr[$row][2] = $temp[$j][1];
				
				$row++;
			}
		} else {
			$exportArr[$row][0] = $i_no_record_exists_msg;	
			$row++;
		}
		$exportArr[$row][0] = "";
		$row++;
		
	}	
		
}

# end of table content


$export_content = $date." ".$i_general_most." ".$meritTitle." ".$criteria."\n\n";
$export_content .= $lexport->GET_EXPORT_TXT($exportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

$lexport->EXPORT_FILE($filename, $export_content);

?>
