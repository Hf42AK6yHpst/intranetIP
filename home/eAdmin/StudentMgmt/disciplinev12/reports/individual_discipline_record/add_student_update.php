<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_cust.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12_cust();

# user access right checking
if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || !$sys_custom['eDiscipline']['BFHMC_IndividualDisciplineRecord']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

# Add lang records to target students
$Result = $ldiscipline->updateStudentReportTargetLang($_REQUEST);

intranet_closedb();

$Result = ($Result==1) ? "update" : "update_failed";
header("Location: student_lang_settings.php?xmsg=$Result&YearID=$selectYear");

?>

