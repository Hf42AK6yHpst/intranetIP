<?php
// using: 

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# user access right checking
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || !$sys_custom['eDiscipline']['BFHMC_IndividualDisciplineRecord']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$linterface = new interface_html();

# Setting of Current Page
$CurrentPage = "Reports_Individual_Discipline_Record";
$CurrentPageArr['eDisciplinev12'] = 1;
# Tab
$TAGS_OBJ[] = array($Lang['eDiscipline']['IndividualDisciplineRecord']['Title'], "index.php", 0);
$TAGS_OBJ[] = array($Lang['eDiscipline']['IndividualDisciplineRecord']['LangSettings'], "", 1);
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

### Toolbar
$btnAry = array();
$btnAry[] = array('new', 'javascript: goAdd();', $Lang['Btn']['Add']);
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

### Filtering
$YearID = $YearID? $YearID : Get_Current_Academic_Year_ID();
$htmlAry['yearSelection'] = getSelectAcademicYear($objName='YearID', $tag='', $noFirst=0, $noPastYear=0, $targetYearID=$YearID, $displayAll=0, $pastAndCurrentYearOnly=0, $OrderBySequence=1, $excludeCurrentYear=0, $excludeYearIDArr=array());

### DB table action buttons
$btnAry = array();
$btnAry[] = array('delete', 'javascript: goDelete();');
$htmlAry['dbTableActionBtn'] = $linterface->Get_DBTable_Action_Button_IP25($btnAry);

### Db table
//$conds_ClassLevelID = '';
//if ($ClassLevelID == -1){
//	// all class level
//}
//else {
//	$conds_ClassLevelID = " And y.YearID = '".$ClassLevelID."' ";
//}
$conds_YearID = '';
if ($YearID != '') {
	$conds_YearID = " AND rlang.AcademicYearID = '".$YearID."' ";
}

$NameField = getNameFieldByLang2('u.');
$ClassNumField = getClassNumberField('ycu.');
$ClassNameField = Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN');
//$TermNameField = Get_Lang_Selection('ay.YearNameB5', 'ay.YearNameEN');

# SQL: students using english report
$sql = "SELECT 
				$NameField AS Name, 
				y.YearName as ClassLevel,
				yc.$ClassNameField as ClassName,
				ycu.ClassNumber AS ClassNum,
				CONCAT('<input type=\"checkbox\" name=\"delRecordIDAry[]\" value=\"', rlang.RecordID, '\">') as CheckBox
		FROM 
				DISCIPLINE_STUDENT_REPORT_LANG as rlang
				INNER JOIN INTRANET_USER as u ON (rlang.StudentID = u.UserID)
				INNER JOIN YEAR_CLASS_USER as ycu ON (u.UserID = ycu.UserID)
				INNER JOIN YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$YearID."')
				INNER JOIN YEAR as y ON (yc.YearID = y.YearID)
		WHERE 
				rlang.RecordLang = 'en'
				$conds_YearID
		";

# Table Settings
if (isset($ck_page_size) && $ck_page_size != "") {
	$page_size = $ck_page_size;
}
$pageSizeChangeEnabled = true;
$field = ($field=='')? 1 : $field;
$order = ($order=='')? 1 : $order;
$page = ($page=='')? 1 : $page;
	
$li = new libdbtable2007($field, $order, $pageNo);
$li->sql = $sql;
$li->IsColOff = "IP25_table";
$li->field_array = array("Name", "ClassLevel", "ClassName", "ClassNum", );
$li->fieldorder2 = ",ClassLevel, ClassName, ClassNum";
$li->column_array = array(0, 0, 0, 0);
$li->wrap_array = array(0, 0, 0, 0);
//$li->no_col = count($li->field_array) + 1;

$pos = 0;
$li->column_list .= "<th class='tabletop tabletopnolink' width='1'>#</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='45%'>".$li->column($pos++, $i_UserName)."</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='10%'>".$li->column($pos++, $i_UserClassLevel)."</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='15%'>".$li->column($pos++, $i_ClassName)."</td>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' width='10%'>".$li->column($pos++, $i_UserClassNumber)."</td>\n";
$li->column_list .= "<th width=1 class='tabletoplink' width='1'>".$li->check("delRecordIDAry[]")."</td>\n";
$li->no_col = 6;

$htmlAry['dataTable'] = $li->display();

############################################################################################################

$linterface->LAYOUT_START();
?>
<script language="javascript">
function goAdd() {
	var year = $('#YearID').val();
	window.location = "add_student.php?targetYearID="+year;
}

function goDelete() {
	checkRemove(document.form1,'delRecordIDAry[]','remove.php');
}

$(document).ready(function(){
	$('#YearID').change(function(){
	    $('#form1').action = "student_lang_settings.php";
	    $('#form1').submit();
	});
});
</script>

<form name="form1" id="form1" method="POST" action="">
	<?=$htmlAry['subTab']?>
	
	<div class="content_top_tool">
		<?=$htmlAry['contentTool']?>
		<br style="clear:both;">
	</div>
	
	<div class="table_board">
		<div class="table_filter">
			<?=$htmlAry['yearSelection']?>
		</div>
		<p class="spacer"></p>
		
		<?=$htmlAry['dbTableActionBtn']?>
		<?=$htmlAry['dataTable']?>
	</div>

	<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
	<input type="hidden" name="order" value="<?php echo $li->order;?>">
	<input type="hidden" name="field" value="<?php echo $li->field; ?>">
	<input type="hidden" name="page_size_change" value="">
	<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>