<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libclass.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdisciplinev12.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();

$ldiscipline = new libdisciplinev12();
$lclass = new libclass();

$target = $_REQUEST['target']; // form | class | student | student2ndLayer
$fieldId = $_REQUEST['fieldId']; // <select> id
$fieldName = $_REQUEST['fieldName']; // <select> name
$academicYearId = $_REQUEST['academicYearId'];
$studentFieldId = $_REQUEST['studentFieldId']; // student <select> id
$studentFieldName = $_REQUEST['studentFieldName']; // student <select> name
                                                   // $yearClassIdAry = (array)$_REQUEST['YearClassID']; // use to select students if target=class

if ($target == 'form') {
    $temp = '<select name="' . $fieldName . '" id="' . $fieldId . '" class="formtextbox" multiple size="5" >';
    $ClassLvlArr = $lclass->getLevelArray();
    
    for ($i = 0; $i < sizeof($ClassLvlArr); $i ++) {
        $temp .= '<option value="' . $ClassLvlArr[$i][0] . '"';
        $temp .= ' selected';
        $temp .= '>' . $ClassLvlArr[$i][1] . '</option>' . "\n";
    }
    $temp .= '</select>' . "\n";
}

if ($target == "class") {
    $temp = '<select name="' . $fieldName . '" id="' . $fieldId . '" class="formtextbox" multiple size="5" >';
    $classResult = $ldiscipline->getRankClassList("", $academicYearId);
    
    for ($k = 0; $k < sizeof($classResult); $k ++) {
        $temp .= '<option value="' . $classResult[$k][1] . '" selected';
        $temp .= '>' . $classResult[$k][0] . '</option>';
    }
    $temp .= '</select>' . "\n";
}

if ($target == "student") {
    
    $temp = '<select name="' . $fieldName . '" id="' . $fieldId . '" class="formtextbox" multiple size="5" onchange="' . $onchange . '">';
    $classResult = $ldiscipline->getRankClassList("", $academicYearId);
    
    for ($k = 0; $k < sizeof($classResult); $k ++) {
        $temp .= '<option value="' . $classResult[$k][1] . '" selected';
        $temp .= '>' . $classResult[$k][0] . '</option>';
    }
    $temp .= '</select>' . "\n";
    $temp .= '<div id="' . $divStudentSelection . '"></div>' . "\n";
}

if ($target == "student2ndLayer") {
    $name_field = getNameFieldByLang("USR.");
    $yearClassIdAry = (array) $_REQUEST['YearClassID'];
    $temp .= '<br />';
    $temp .= '<select name="' . $studentFieldName . '" id="' . $studentFieldId . '" class="formtextbox" multiple size="5">';
    
    $clsName = ($intranet_session_language == "en") ? "yc.ClassTitleEN" : "yc.ClassTitleB5";
    $sql = "SELECT $clsName, ycu.ClassNumber, $name_field, USR.UserID FROM INTRANET_USER USR
			LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) 
			LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)
			WHERE yc.YearClassID IN (" . implode(",", $yearClassIdAry) . ") AND USR.RecordType=2 
			ORDER BY ycu.ClassNumber
			";
    
    $studentResult = $ldiscipline->returnArray($sql, 3);
    
    for ($k = 0; $k < sizeof($studentResult); $k ++) {
        $temp .= '<option value="' . $studentResult[$k][3] . '" selected';
        $temp .= '>' . $studentResult[$k][0] . '-' . $studentResult[$k][1] . ' ' . $studentResult[$k][2] . '</option>';
    }
    
    $temp .= '</select>' . "\n";
    $temp .= '<br />';
    
    $temp .= $linterface->GET_BTN($button_select_all, "button", "Select_All_Options('$studentFieldId', true);return false;");
}

echo $temp;

intranet_closedb();

?>