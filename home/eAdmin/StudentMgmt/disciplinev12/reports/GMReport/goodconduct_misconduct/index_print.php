<?php
// Modifying by : 

##### Change Log [Start] #####
#
#	Date	:	2017-09-27 (Bill)	[2017-0927-1530-03066]
#				- Use Display Mode Setting to control print layout instead of $sys_custom['eDiscipline']['GM_Report_Display_OriginalLayout']
#
#	Date	:	2016-11-01 (Bill)	[2016-0808-1526-52240]
#				- Change layout 	X-Axis: GM Item		Y-Axis: Target
#				- if use old layout, set $sys_custom['eDiscipline']['GM_Report_Display_OriginalLayout'] = true
#
#	Date	:	2016-04-08	(Bill)
#				Replace deprecated split() by explode() for PHP 5.4
#
#	Date	:	2012-11-15 (YatWoon)
#				Cater with subject with ",", use ",," as separator [Case#2012-1005-1150-06132] [Case#2012-1114-0941-57156]
#
#	Date	:	2012-03-21 (Henry Chow)
#				revise calculation of displaying "Total Not Submit Times" [CRM : 2012-0117-1703-41042]
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal']) {
	$defaultSecondCategory = 7;
	$defaultSecondItem = 2;	
}

$ldiscipline = new libdisciplinev12();
//$ldiscipline->CONTROL_ACCESS("Discipline-STAT-GoodConduct_Misconduct-View");

$lclass = new libclass();
$linterface = new interface_html();

$selectYear = ($selectYear == '') ? getCurrentAcademicYear() : $selectYear;

// replace split() by explode() - for PHP 5.4
//$semester_data = split("\n",get_file_content("$intranet_root/file/semester.txt"));
$semester_data = explode("\n",get_file_content("$intranet_root/file/semester.txt"));

$SelectedFormArr = explode(",", $SelectedForm);
$SelectedFormTextArr = explode(",,", $SelectedFormText);

$SelectedClassArr = explode(",", $SelectedClass);
$SelectedClassTextArr = explode(",,", $SelectedClassText);

$SelectedItemIDArr = explode(",", $SelectedItemID);
$SelectedItemIDTextArr = explode(",,", $SelectedItemIDText);


if ($Period == "YEAR") {
	$parPeriodField1 = $selectYear;
	$parPeriodField2 = $selectSemester;
} else {
	$parPeriodField1 = $textFromDate;
	$parPeriodField2 = $textToDate;
}

if ($rankTarget == "form") {
	$parByFieldArr = $SelectedFormTextArr;
} else {
	$parByFieldArr = $SelectedClassTextArr;
}

if($categoryID==1) {		# "Late"
	$parStatsFieldArr[0] = 0;
	//$parStatsFieldTextArr[0] = 'Late';
	$parStatsFieldTextArr[0] = $ldiscipline->getGMCatNameByCatID(1);
} else if($categoryID==2) {	# "Homework not submitted"
	
	$parStatsFieldArr = $SelectedItemIDArr;
	$parStatsFieldTextArr = $SelectedItemIDTextArr;
	
} else {
	$parStatsFieldArr = $SelectedItemIDArr;
	$parStatsFieldTextArr = $SelectedItemIDTextArr;
}

if($categoryID!=1 && $categoryID!=2) { 	
	$all_item_name = $ldiscipline->RETRIEVE_ALL_CONDUCT_ITEM($categoryID, 'name', $record_type);
}

### Table Data ###
$data = $ldiscipline->retrieveGoodMisReport($Period, $parPeriodField1, $parPeriodField2, $rankTarget, $parByFieldArr, $StatType, $parStatsFieldArr, $parStatsFieldTextArr, $categoryID, $studentID, $include_waive);
//debug_pr($data);

################################################
############ Report Table Content ##############

if($record_type == 1) {
	$i_MeritDemerit[1] = $i_Merit_Merit;
	$i_MeritDemerit[2] = $i_Merit_MinorCredit;
	$i_MeritDemerit[3] = $i_Merit_MajorCredit;
	$i_MeritDemerit[4] = $i_Merit_SuperCredit;
	$i_MeritDemerit[5] = $i_Merit_UltraCredit;
} else if($record_type == -1) {
	$i_MeritDemerit[0] = $i_Merit_Warning;
	$i_MeritDemerit[-1] = $i_Merit_BlackMark;
	$i_MeritDemerit[-2] = $i_Merit_MinorDemerit;
	$i_MeritDemerit[-3] = $i_Merit_MajorDemerit;
	$i_MeritDemerit[-4] = $i_Merit_SuperDemerit;
	$i_MeritDemerit[-5] = $i_Merit_UltraDemerit;
}
	
$itemTotal = array();
$columnMeritDemeritCount = array();
/*
$detail_table .= "<head>";
$detail_table .= "<link href='{$PATH_WRT_ROOT}/templates/{$LAYOUT_SKIN}/css/print.css' rel='stylesheet' type='text/css'>";
$detail_table .= "<style type='text/css'>";
$detail_table .= "<!--";
$detail_table .= ".print_hide {display:none;}";
$detail_table .= "-->";
$detail_table .= "</style>";
$detail_table .= "</head>";
*/
$detail_table .=  "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\" class='tableborder_print'>\n";
$detail_table .= "<tr class=\"tabletext row_print\">";
$detail_table .= "<td class=\"tabletext row_print\" align=\"right\" colspan=\"100%\">";

if($Period=="YEAR") {
	$detail_table .= $ldiscipline->getAcademicYearNameByYearID($parPeriodField1)." ".$ldiscipline->getTermNameByTermID($parPeriodField2)."\n\n";
} else {
	$detail_table .= $iDiscipline['Period_Start']." ".$parPeriodField1." ".$iDiscipline['Period_End']." ".$parPeriodField2."\n\n";
}

//if(!$sys_custom['eDiscipline']['GM_Report_Display_OriginalLayout'])
if($display_hori) 
{
	$targetDisplay = "";
	if($rankTarget=="form")
		$targetDisplay = $i_Discipline_Form;
	else if($rankTarget=="class")
		$targetDisplay = $i_Discipline_Class;
	else if($rankTarget=="student")
		$targetDisplay = $i_Discipline_Student;
	$detail_table .= "<br>".$i_Discipline_Target." : ".$targetDisplay." ";
}

//$generationDate = getdate();
$generationDate = date("Y/m/d H:i:s");
$detail_table .= "<br>".$eDiscipline['ReportGeneratedDate']." ".$generationDate;

$detail_table .= "</td></tr>";	

// [2016-0808-1526-52240] new layout
//if(!$sys_custom['eDiscipline']['GM_Report_Display_OriginalLayout'])
if($display_hori) 
{
	// Target: Form / Class
	if($rankTarget=="form" || $rankTarget=="class") 
	{
		// Initial
		$p = 0;
		$subtotal = array();
		$targetMeritAry = array();
		$ItemCategoryAry = array();
		
		$temp_detail_table = "";
			
		# Table Header
		$detail_table .= "<tr class=\"tabletop_print tabletext\">";
		$detail_table .= "<td class=\"tabletop_print tabletext\" align='center'>".($record_type==1? $i_Discipline_GoodConduct : $i_Discipline_Misconduct)."</td>";
		$detail_table .= "<td class=\"border_left\">".($rankTarget=="form"? $Lang['eDiscipline']['WholeSchoolTotal'] : $i_Discipline_System_Report_Class_Total)."</td>";
		for($i=0; $i<sizeof($parByFieldArr); $i++)
		{
			$css = ($i==0) ? "border_left" : "";	
			$detail_table .= "<td class=\"$css\">".stripslashes($parByFieldArr[$i])."</td>";
		}
		$detail_table .= "</tr>";
		
		// loop items
		$isFirstCommonItem = false;
		for($j=0; $j<sizeof($parStatsFieldTextArr); $j++)
		{
			$temp_details = "";
			
			// Item name
			if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'] && in_array($parStatsFieldArr[$j], array(0, $defaultSecondItem))) {
				$detail_table .= "<tr class=\"row_print\">";
				$detail_table .= "<td class=\"tabletext row_print\" align=\"center\">".intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))."</td>";
			}
			else {
				$css2 = ($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'] && !$isFirstCommonItem) ? "border_top" : "";
				$temp_detail_table .= "<tr class=\"row_print\">";
				$temp_detail_table .= "<td class=\"tabletext row_print $css2\" align=\"center\">".intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))."</td>";
			}
			
			// loop targets
			$finalTotal = 0;
			for($i=0; $i<sizeof($parByFieldArr); $i++)
			{
				$css = ($i==0) ? "border_left" : "";
				$css2 = ($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'] && !$isFirstCommonItem) ? "border_top" : "";
			
				// Target Count
				if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'] && in_array($parStatsFieldArr[$j], array(0, $defaultSecondItem))) {
					$temp_details .= (isset($data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])])) ? "<td align=\"center\" class=\"tabletext row_print $css\">".$data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])]."</td>" : "<td align=\"center\" class=\"tabletext row_print $css\">0</td>";
				}
				else {
					$temp_details .= (isset($data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])])) ? "<td align=\"center\" class=\"tabletext row_print $css $css2\">".$data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])]."</td>" : "<td align=\"center\" class=\"tabletext row_print $css $css2\">0</td>";
				}
				// add total count
				$finalTotal += $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])];
				
				// handle Total Not Submit Times
				if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'])
				{
					// get category count
					if(!isset($ItemCategoryAry[intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))]))
					{
						$sql = "SELECT COUNT(c.CategoryID) FROM DISCIPLINE_ACCU_CATEGORY c INNER JOIN DISCIPLINE_ACCU_CATEGORY_ITEM i ON (c.CategoryID=i.CategoryID AND i.ItemID='".$parStatsFieldArr[$j]."' AND c.Name LIKE '%".$Lang['eDiscipline']['NotSubmit']."%')";
						$ItemCategoryAry[intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))] = $ldiscipline->returnVector($sql);
					}
					$tempResult = $ItemCategoryAry[intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))];
					
					// add to target count (Total Not Submit Times)
					if($tempResult[0]>0) {
						$subtotal[stripslashes($parByFieldArr[$i])] += $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])];
					}
				}
			}
			
			// Total Count
			if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'] && in_array($parStatsFieldArr[$j], array(0, $defaultSecondItem))) {
				$detail_table .= (isset($finalTotal)) ? "<td align=\"center\" class=\"tabletext row_print border_left\">".$finalTotal."</td>" : "<td align=\"center\" class=\"tabletext row_print border_left\">0</td>";
				$detail_table .= $temp_details;
				$detail_table .= "</tr>";
			}
			else {
				$css2 = ($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'] && !$isFirstCommonItem) ? "border_top" : "";
				$isFirstCommonItem = true;
				
				$temp_detail_table .= (isset($finalTotal)) ? "<td align=\"center\" class=\"tabletext row_print border_left $css2\">".$finalTotal."</td>" : "<td align=\"center\" class=\"tabletext row_print border_left $css2\">0</td>";
				$temp_detail_table .= $temp_details;
				$temp_detail_table .= "</tr>";
			}
		}
		unset($ItemCategoryAry);
		
		// Total Not Submit Times Column
		if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'])
		{
			$temp_details = "";
			
			$detail_table .= "<tr class=\"row_print\">";
			$detail_table .= "<td class=\"tabletext row_print border_top\" align=\"center\">".$Lang['eDiscipline']['TotalNotSubmitTimes']."</td>";
			
			// loop targets
			$finalTotal = 0;
			for($i=0; $i<sizeof($parByFieldArr); $i++)
			{
				$css = ($i==0) ? "border_left" : "";
				$temp_details .= (isset($subtotal[stripslashes($parByFieldArr[$i])])) ? "<td align=\"center\" class=\"tabletext row_print border_top $css\">".$subtotal[stripslashes($parByFieldArr[$i])]."</td>" : "<td align=\"center\" class=\"tabletext row_print border_top $css\">0</td>";
				$finalTotal += (isset($subtotal[stripslashes($parByFieldArr[$i])])) ? $subtotal[stripslashes($parByFieldArr[$i])] : 0;
			}
			
			// Total Count (Total Not Submit Times)
			$detail_table .= (isset($finalTotal)) ? "<td align=\"center\" class=\"tabletext row_print border_top border_left\">".$finalTotal."</td>" : "<td align=\"center\" class=\"tabletext row_print border_top border_left\">0</td>";
			$detail_table .= $temp_details;
			$detail_table .= "</tr>";
		}
		$detail_table .= $temp_detail_table;
		
		// Merit / Demerit
		if($include_merit)
		{
			$a = 0;		
			if($record_type==1)
				$m = 1;
			else
				$m = 0;
			
			// loop merit type
			foreach($i_MeritDemerit as $tempMeritDemerit)
			{
				$temp_details = "";
				
				$thisCss = ($a==0) ? " border_top" : "";
				$detail_table .= "<tr class=\"row_print\">";
				$detail_table .= "<td align=\"center\" class=\"tabletext row_print $thisCss\">".$tempMeritDemerit."</td>";
				
				// loop target
				$finalTotal = 0;
				for($i=0; $i<sizeof($parByFieldArr); $i++)
				{
					$css = ($i==0) ? "border_left" : "";
				
					// Get Merit / Demerit
					if(!isset($targetMeritAry[stripslashes($parByFieldArr[$i])]))
						$targetMeritAry[stripslashes($parByFieldArr[$i])] = $ldiscipline->retrieveMeritDemeritCountFromGM($Period, $parPeriodField1, $parPeriodField2, $rankTarget, $parByFieldArr[$i], $parStatsFieldArr, $parStatsFieldTextArr, $categoryID, "",  $include_waive, $include_unreleased);
					$meritDemeritCount = $targetMeritAry[stripslashes($parByFieldArr[$i])];
					
					$tempCount = 0;
					for($n=0; $n<sizeof($meritDemeritCount); $n++)
					{
						if($meritDemeritCount[$n]['ProfileMeritType']==$m)
							$tempCount += $meritDemeritCount[$n]['countTotal'];
					}
					
					// Merit / Demerit Count
					$temp_details .= "<td align=\"center\" class=\"tabletext row_print $css $thisCss\">";
					$temp_details .= ($tempCount != 0) ? $tempCount : "0";
					$temp_details .= "</td>";
					
					$finalTotal += $tempCount;
				}
				
				// Total Count (Merit / Demerit) 
				$detail_table .= (isset($finalTotal)) ? "<td align=\"center\" class=\"tabletext row_print border_left $thisCss\">".$finalTotal."</td>" : "<td align=\"center\" class=\"tabletext row_print border_left $thisCss\">0</td>";
				$detail_table .= $temp_details;
				$detail_table .= "</tr>";
				
				// update $m and $a
				if($record_type==1)
					$m++;
				else
					$m--;
				$a++;
			}
			unset($targetMeritAry);
		}
	}
	// Target: Student
	else
	{
		// Initial
		$p = 0;
		$subtotal = array();
		$stutentAry = array();
		$studentMeritAry = array();
		$ItemCategoryAry = array();
		
		$temp_detail_table = "";
		
		# Table Header
		$detail_table .= "<tr class=\"tabletop_print tabletext\">";
		$detail_table .= "<td class=\"tabletop_print tabletext\" align='center'>".($record_type==1? $i_Discipline_GoodConduct : $i_Discipline_Misconduct)."</td>";
		$detail_table .= "<td class=\"border_left\" align=\"center\">".$Lang['eDiscipline']['StudentTotal']."</td>";
		for($k=0; $k<sizeof($studentID); $k++)
		{
			$css = ($k==0) ? "border_left" : "";
			
			// store student info
			$stutentAry[$studentID[$k]] = $ldiscipline->getStudentNameByID($studentID[$k], $selectYear); 
			$stdInfo = $stutentAry[$studentID[$k]];
			
			$detail_table .= "<td class=\"$css\" align=\"center\">".$stdInfo[0][1]."<br/>(".$stdInfo[0][2]." - ".$stdInfo[0][3].")</td>";
		}
		$detail_table .= "</tr>";
		
		// loop item
		$isFirstCommonItem = false;
		for($j=0; $j<sizeof($parStatsFieldTextArr); $j++)
		{
			$temp_details = "";
			
			// Item Name
			if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'] && in_array($parStatsFieldArr[$j], array(0, $defaultSecondItem))) {
				$detail_table .= "<tr class=\"row_print\">";
				$detail_table .= "<td class=\"tabletext row_print\" align=\"center\">".intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))."</td>";
			}
			else {
				$css2 = ($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'] && !$isFirstCommonItem) ? "border_top" : "";
				
				$temp_detail_table .= "<tr class=\"row_print\">";
				$temp_detail_table .= "<td class=\"tabletext row_print $css2\" align=\"center\">".intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))."</td>";
			}
		
			// loop students
			$finalTotal = 0;
			for($k=0; $k<sizeof($studentID); $k++)
			{
				$css = ($k==0) ? "border_left" : "";
				$css2 = ($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'] && !$isFirstCommonItem) ? "border_top" : "";
				
				$stdInfo = $stutentAry[$studentID[$k]];
				
				// Student Count
				if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'] && in_array($parStatsFieldArr[$j], array(0, $defaultSecondItem))) {
					$temp_details .= (isset($data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]])) ? "<td align=\"center\" class=\"tabletext row_print $css\">".$data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]]."</td>" : "<td align=\"center\" class=\"tabletext row_print $css\">0</td>";
				}
				else {
					$temp_details .= (isset($data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]])) ? "<td align=\"center\" class=\"tabletext row_print $css $css2\">".$data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]]."</td>" : "<td align=\"center\" class=\"tabletext row_print $css $css2\">0</td>";
				}
				// add total count
				$finalTotal += $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]];
				
				// handle Total Not Submit Times
				if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'])
				{
					// get category count
					if(!isset($ItemCategoryAry[intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))]))
					{
						$sql = "SELECT COUNT(c.CategoryID) FROM DISCIPLINE_ACCU_CATEGORY c INNER JOIN DISCIPLINE_ACCU_CATEGORY_ITEM i ON (c.CategoryID=i.CategoryID AND i.ItemID='".$parStatsFieldArr[$j]."' AND c.Name LIKE '%".$Lang['eDiscipline']['NotSubmit']."%')";
						$ItemCategoryAry[intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))] = $ldiscipline->returnVector($sql);
					}
					$tempResult = $ItemCategoryAry[intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))];
					
					// add to target count (Total Not Submit Times)
					if($tempResult[0]>0) {
						$subtotal[$stdInfo[0][2]][$stdInfo[0][3]] += $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]];
					}
				}
			}
			
			// Total Count
			if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'] && in_array($parStatsFieldArr[$j], array(0, $defaultSecondItem))) {
				$detail_table .= (isset($finalTotal)) ? "<td align=\"center\" class=\"tabletext row_print border_left\">".$finalTotal."</td>" : "<td align=\"center\" class=\"tabletext row_print border_left\">0</td>";
				$detail_table .= $temp_details;
				$detail_table .= "</tr>";
			}
			else {
				$css2 = ($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'] && !$isFirstCommonItem) ? "border_top" : "";
				$isFirstCommonItem = true;
				
				$temp_detail_table .= (isset($finalTotal)) ? "<td align=\"center\" class=\"tabletext row_print border_left $css2\">".$finalTotal."</td>" : "<td align=\"center\" class=\"tabletext row_print border_left $css2\">0</td>";
				$temp_detail_table .= $temp_details;
				$temp_detail_table .= "</tr>";
			}
		}
		unset($ItemCategoryAry);
		
		// Total Not Submit Times Column
		if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'])
		{
			$temp_details = "";
			
			$detail_table .= "<tr class=\"row_print\">";
			$detail_table .= "<td class=\"tabletext row_print border_top\" align=\"center\">".$Lang['eDiscipline']['TotalNotSubmitTimes']."</td>";
			
			// loop students
			$finalTotal = 0;
			for($k=0; $k<sizeof($studentID); $k++)
			{
				$css = ($k==0) ? "border_left" : "";
				
				$stdInfo = $stutentAry[$studentID[$k]];
				
				$temp_details .= (isset($subtotal[$stdInfo[0][2]][$stdInfo[0][3]])) ? "<td align=\"center\" class=\"tabletext row_print border_top $css\">".$subtotal[$stdInfo[0][2]][$stdInfo[0][3]]."</td>" : "<td align=\"center\" class=\"tabletext row_print border_top $css\">0</td>";
				$finalTotal += (isset($subtotal[$stdInfo[0][2]][$stdInfo[0][3]])) ? $subtotal[$stdInfo[0][2]][$stdInfo[0][3]] : 0;
			}
			
			// Total Count (Total Not Submit Times)
			$detail_table .= (isset($finalTotal)) ? "<td align=\"center\" class=\"tabletext row_print border_top border_left\">".$finalTotal."</td>" : "<td align=\"center\" class=\"tabletext row_print border_top border_left\">0</td>";
			$detail_table .= $temp_details;
			$detail_table .= "</tr>";
		}
		$detail_table .= $temp_detail_table;
		
		// Merit / Demerit
		if($include_merit)
		{
			$a = 0;
			if($record_type==1)
				$m = 1;
			else
				$m = 0;
			
			// loop merit type
			foreach($i_MeritDemerit as $tempMeritDemerit)
			{
				$temp_details = "";
				
				$thisCss = ($a==0) ? " border_top" : "";
				$detail_table .= "<tr class=\"row_print\">";
				$detail_table .= "<td align=\"center\" class=\"tabletext row_print $thisCss\">".$tempMeritDemerit."</td>";
				
				// loop students
				$finalTotal = 0;
				for($k=0; $k<sizeof($studentID); $k++)
				{
					$css = ($k==0) ? "border_left" : "";
					
					// Get Merit / Demerit
					$stdInfo = $stutentAry[$studentID[$k]];
					if(!isset($studentMeritAry[$studentID[$k]]))
						$studentMeritAry[$studentID[$k]] = $ldiscipline->retrieveMeritDemeritCountFromGM($Period, $parPeriodField1, $parPeriodField2, $rankTarget, $stdInfo[$k][2], $parStatsFieldArr, $parStatsFieldTextArr, $categoryID, $studentID[$k], $include_waive, $include_unreleased);
					$meritDemeritCount = $studentMeritAry[$studentID[$k]];
					
					$tempCount = 0;
					for($n=0; $n<sizeof($meritDemeritCount); $n++)
					{
						if($meritDemeritCount[$n]['ProfileMeritType']==$m)
							$tempCount += $meritDemeritCount[$n]['countTotal'];
					}
					
					// Merit / Demerit Count
					$temp_details .= "<td align=\"center\" class=\"tabletext row_print $css $thisCss\">";
					$temp_details .= ($tempCount != 0) ? $tempCount : "0";
					$temp_details .= "</td>";
					
					$finalTotal += $tempCount;
				}
				
				// Total Count (Merit / Demerit) 
				$detail_table .= (isset($finalTotal)) ? "<td align=\"center\" class=\"tabletext row_print border_left $thisCss\">".$finalTotal."</td>" : "<td align=\"center\" class=\"tabletext row_print border_left $thisCss\">0</td>";
				$detail_table .= $temp_details;
				$detail_table .= "</tr>";
				
				// update $m and $a
				if($record_type==1)
					$m++;
				else
					$m--;
				$a++;
			}
			unset($studentMeritAry);
		}
	}
}
else
{
	$detail_table .= "<tr class=\"tabletop_print tabletext\">";
		if($rankTarget=="form") {
			$detail_table .= "<td class=\"tabletop\">".$i_Discipline_Form."</td>";	
		} else if($rankTarget=="class") {
			$detail_table .= "<td class=\"tabletop\">".$i_Discipline_Class."</td>";	
		} else {
			$detail_table .= "<td class=\"tabletop\">".$i_general_name."</td>";	
			$detail_table .= "<td class=\"tabletop\">".$i_ClassNumber."</td>";	
		}
	
	# header of report table
	$temp_detail_table = "";
	for($i=0; $i<sizeof($parStatsFieldTextArr); $i++) {
		# style=\"writing-mode: tb-rl\"
		$css = ($i==0) ? "border_left" : "";
		
		if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'] && in_array($parStatsFieldArr[$i], array(0,$defaultSecondItem))) {
			$detail_table .= "<td class=\"$css\" align=\"center\">".intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$i]))."</td>";	
		} else {
			$temp_detail_table .= "<td class=\"$css\" align=\"center\">".intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$i]))."</td>";
		}
	}
	if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal']) {
		$detail_table .= "<td class=\"tabletop\" align=\"center\">".$Lang['eDiscipline']['TotalNotSubmitTimes']."</td>";	
	}
	$detail_table .= $temp_detail_table;
	
	// Merit / Demerit
	if($include_merit)
	{
		$i = 0;
		foreach($i_MeritDemerit as $tempMeritDemerit) {
			# style=\"writing-mode: tb-rl\"
			$css = ($i==0) ? "border_left" : "";
			$detail_table .= "<td align=\"center\" class=\"$css\">".$tempMeritDemerit."</td>";	
			$i++;
		}
	}
	
	$detail_table .= "</tr>";
			
	
	$finalTotal = 0;
	
	#content of report table
	if($rankTarget != "student") {
		for($i=0; $i<sizeof($parByFieldArr); $i++) {						# for each class/form
			if($rankTarget=="form") {
				
				$css = ($i%2) ? "2" : "1";
				$detail_table .= "<tr class=\"row_print\">";
				
				$detail_table .= "<td class='tabletext row_print'>".stripslashes($parByFieldArr[$i])."</td>";
				
				$subtotal = 0;
				$temp_detail_table = "";
				for($j=0; $j<sizeof($parStatsFieldTextArr); $j++) {			# for each item
					$css = ($j==0) ? "border_left" : "";
					
					if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'] && in_array($parStatsFieldArr[$j], array(0,$defaultSecondItem))) {
						$detail_table .= (isset($data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])])) ? "<td align=\"center\" class='tabletext row_print $css'>".$data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])]."</td>" : "<td align=\"center\" class='tabletext row_print $css'>0</td>";
					} else {
						$temp_detail_table .= (isset($data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])])) ? "<td align=\"center\" class='tabletext row_print $css'>".$data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])]."</td>" : "<td align=\"center\" class='tabletext row_print $css'>0</td>";
					}
					
					$itemTotal[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))] += $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])];
					
					if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal']) {
						$sql = "SELECT COUNT(c.CategoryID) FROM DISCIPLINE_ACCU_CATEGORY c INNER JOIN DISCIPLINE_ACCU_CATEGORY_ITEM i ON (c.CategoryID=i.CategoryID AND i.ItemID='".$parStatsFieldArr[$j]."' AND c.Name LIKE '%".$Lang['eDiscipline']['NotSubmit']."%')";
						$tempResult = $ldiscipline->returnVector($sql);
						
						if($tempResult[0]>0) {
							$subtotal += $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])];
						}
					}
				}
				if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal']) {
					$detail_table .= "<td class=\"tablerow$css tabletext seperator_right\" align='center'>".$subtotal."</td>";
					$finalTotal += $subtotal;
				}
				$detail_table .= $temp_detail_table;
				
				// Merit / Demerit
				if($include_merit)
				{
					# (YEAR/DATE , Period1, Period2, Class/Form, ClassName/FormName, SelectedItemID, SelectedItemName)
					$meritDemeritCount = $ldiscipline->retrieveMeritDemeritCountFromGM($Period, $parPeriodField1, $parPeriodField2, $rankTarget, $parByFieldArr[$i], $parStatsFieldArr, $parStatsFieldTextArr, $categoryID, "", $include_waive, $include_unreleased);
				
					if($record_type==1) {
						$m = 1;
					} else {
						$m = 0;
					}
					
					$k = 0;
					foreach($i_MeritDemerit as $columnNo) {
						$tempCount = 0;
						for($n=0; $n<sizeof($meritDemeritCount); $n++) {
							if($meritDemeritCount[$n]['ProfileMeritType']==$m)
								$tempCount += $meritDemeritCount[$n]['countTotal'];
						}
						$css = ($k==0) ? "border_left" : "";
						$detail_table .= "<td align=\"center\" class='tabletext row_print $css'>";
						$detail_table .= ($tempCount != 0) ? $tempCount : "0";
						$columnMeritDemeritCount[$m] += $tempCount;
						$detail_table .= "</td>";
						
						if($record_type==1) {
							$m++;
						} else {
							$m--;
						}
						$k++;
					}
				}
					
				$detail_table .= "</tr>";
				
			} else {		# rankTarget = class
				
				//$stdInfo = $ldiscipline->retrieveNameClassNoByClassName($parByFieldArr[$i]);
				
				//for($k=0; $k<sizeof($stdInfo); $k++) {
					$css = ($k%2) ? "2" : "1";
					$detail_table .= "<tr class=\"row_print\">";
					$detail_table .= "<td class='tabletext row_print'>".$parByFieldArr[$i]."</td>";
					//$detail_table .= "<td class='tabletext row_print'>".$parByFieldArr[$i]." - ".$stdInfo[$k][0]."</td>";
					
					$subtotal = 0;
					$temp_detail_table = "";
					
					for($j=0; $j<sizeof($parStatsFieldTextArr); $j++) {		# for each item
						$css2 = ($j==0) ? "border_left" : "";
						
						if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'] && in_array($parStatsFieldArr[$j], array(0,$defaultSecondItem))) {
							$detail_table .= (isset($data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])])) ? "<td align=\"center\" class='tabletext row_print $css2'>".$data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])]."</td>" : "<td align=\"center\" class='tabletext row_print $css2 '>0</td>";
						} else {
							$temp_detail_table .= (isset($data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])])) ? "<td align=\"center\" class='tabletext row_print $css2'>".$data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])]."</td>" : "<td align=\"center\" class='tabletext row_print $css2 '>0</td>";
						}
						$itemTotal[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))] += $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])];
						
						if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal']) {
							$sql = "SELECT COUNT(c.CategoryID) FROM DISCIPLINE_ACCU_CATEGORY c INNER JOIN DISCIPLINE_ACCU_CATEGORY_ITEM i ON (c.CategoryID=i.CategoryID AND i.ItemID='".$parStatsFieldArr[$j]."' AND c.Name LIKE '%".$Lang['eDiscipline']['NotSubmit']."%')";
							$tempResult = $ldiscipline->returnVector($sql);
							
							if($tempResult[0]>0) {
								$subtotal += $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])];
							}
						}
					}
					if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal']) {
						$detail_table .= "<td class=\"tablerow$css tabletext seperator_right\" align='center'>".$subtotal."</td>";
						$finalTotal += $subtotal;
					}
					$detail_table .= $temp_detail_table;
					
					// Merit / Demerit
					if($include_merit)
					{
						$meritDemeritCount = $ldiscipline->retrieveMeritDemeritCountFromGM($Period, $parPeriodField1, $parPeriodField2, $rankTarget, $parByFieldArr[$i], $parStatsFieldArr, $parStatsFieldTextArr, $categoryID, "", $include_waive, $include_unreleased);
					
						if($record_type==1) {
							$m = 1;
						} else {
							$m = 0;
						}
						
						$a = 0;
						foreach($i_MeritDemerit as $columnNo) {
							$tempCount = 0;
							for($n=0; $n<sizeof($meritDemeritCount); $n++) {
								if($meritDemeritCount[$n]['ProfileMeritType']==$m)
									$tempCount += $meritDemeritCount[$n]['countTotal'];
							}
							$css2 = ($a==0) ? "border_left" : "";
							$detail_table .= "<td align=\"center\" class='tabletext row_print $css2'>";
							$detail_table .= ($tempCount != 0) ? $tempCount : "0";
							$columnMeritDemeritCount[$m] += $tempCount;
							$detail_table .= "</td>";
							
							if($record_type==1) {
								$m++;
							} else {
								$m--;
							}
							$a++;
						}
					}
		
					$detail_table .= "</tr>";
				//}
				
			}
			
		}
	} else {	# rankTarget = student
		
				for($k=0; $k<sizeof($studentID); $k++) {
					$stdInfo = $ldiscipline->getStudentNameByID($studentID[$k], $selectYear);
					//debug_r($stdInfo);
					$css = ($p%2) ? "2" : "1";
					$detail_table .= "<tr class=\"tabletext row_print\">";
					$detail_table .= "<td class=\"tabletext row_print\">".$stdInfo[0][1]."</td>";
					$detail_table .= "<td class=\"tabletext row_print\">".$stdInfo[0][2]." - ".$stdInfo[0][3]."</td>";
					
					$subtotal = 0;
					$temp_detail_table = "";
					
					for($j=0; $j<sizeof($parStatsFieldTextArr); $j++) {		# for each item
						$css = ($p%2==0) ? "" : "2";
						$css2 = ($j==0) ? "border_left" : "";
						
						if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'] && in_array($parStatsFieldArr[$j], array(0,$defaultSecondItem))) {
							$detail_table .= (isset($data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]])) ? "<td align=\"center\" class=\"tabletext tablerow_total".$css." ".$css2."\">".$data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]]."</td>" : "<td align=\"center\" class=\"tabletext tablerow_total".$css." ".$css2."\">0</td>";
						} else {
							$temp_detail_table .= (isset($data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]])) ? "<td align=\"center\" class=\"tabletext tablerow_total".$css." ".$css2."\">".$data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]]."</td>" : "<td align=\"center\" class=\"tabletext tablerow_total".$css." ".$css2."\">0</td>";
						}
						$itemTotal[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))] += $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]];
						
						if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal']) {
							$sql = "SELECT COUNT(c.CategoryID) FROM DISCIPLINE_ACCU_CATEGORY c INNER JOIN DISCIPLINE_ACCU_CATEGORY_ITEM i ON (c.CategoryID=i.CategoryID AND i.ItemID='".$parStatsFieldArr[$j]."' AND c.Name LIKE '%".$Lang['eDiscipline']['NotSubmit']."%')";
							$tempResult = $ldiscipline->returnVector($sql);
							
							if($tempResult[0]>0) {
								$subtotal += $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]];
							}
						}
					}
					if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal']) {
						$detail_table .= "<td class=\"tablerow$css tabletext seperator_right\" align='center'>".$subtotal."</td>";
						$finalTotal += $subtotal;
					}
					$detail_table .= $temp_detail_table;
					
					$p++;
	//				$detail_table .= "<td width=\"1\" class=\"tablebluetop\"><img src=\"{$image_path}/{$LAYOUT_SKIN}\"/10x10.gif\" width=\"1\" height=\"1\"></td>";
					
					// Merit / Demerit
					if($include_merit)
					{	
						$meritDemeritCount = $ldiscipline->retrieveMeritDemeritCountFromGM($Period, $parPeriodField1, $parPeriodField2, $rankTarget, $stdInfo[$k][2], $parStatsFieldArr, $parStatsFieldTextArr, $categoryID, $studentID[$k], $include_waive, $include_unreleased);
					
						if($record_type==1) {
							$m = 1;
						} else {
							$m = 0;
						}
						
						$a = 0;
						foreach($i_MeritDemerit as $columnNo) {
							$tempCount = 0;
							for($n=0; $n<sizeof($meritDemeritCount); $n++) {
								if($meritDemeritCount[$n]['ProfileMeritType']==$m)
									$tempCount += $meritDemeritCount[$n]['countTotal'];
							}
							$css2 = ($a==0) ? "border_left" : "";
							$detail_table .= "<td align=\"center\" class='tabletext row_print $css2'>";
							$detail_table .= ($tempCount != 0) ? $tempCount : "0";
							$columnMeritDemeritCount[$m] += $tempCount;
							$detail_table .= "</td>";
							
							if($record_type==1) {
								$m++;
							} else {
								$m--;
							}
							$a++;
						}
					}
	
					$detail_table .= "</tr>";
				}
	}
	
	$detail_table .= "</tr>";
	
	$detail_table .= "<tr colspan='tabletoplink'><td colspan=\"100%\" class='tabletext row_print'><table cellspacing=0 cellpadding=0 width=100%><tr><td class='tabletext row_print'><img src=\"{$image_path}/{$LAYOUT_SKIN}\"/10x10.gif\" width=\"100%\" height=\"1\"></td></tr></table></td>";
	$detail_table .= "<tr class='row_print'><td class='tabletext row_print'>".$i_Discipline_System_Report_Class_Total."</td>";
	if($rankTarget=="student") {
		$detail_table .= "<td class='tabletext row_print'>&nbsp;</td>";
	}
	
	//$finalTotal = 0;
	
	$temp_detail_table = "";
	for($i=0; $i<sizeof($parStatsFieldTextArr); $i++) {
		$css = ($i==0) ? "border_left" : "";
		
		if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'] && in_array($parStatsFieldArr[$i], array(0,$defaultSecondItem))) {
			$detail_table .= (isset($itemTotal[$parStatsFieldArr[$i]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$i]))])) ? "<td align=\"center\" class='tabletext row_print $css'>".$itemTotal[$parStatsFieldArr[$i]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$i]))]."</td>" : "<td align=\"center\" class='tabletext row_print $css'>0</td>";
		} else {
			$temp_detail_table .= (isset($itemTotal[$parStatsFieldArr[$i]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$i]))])) ? "<td align=\"center\" class='tabletext row_print $css'>".$itemTotal[$parStatsFieldArr[$i]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$i]))]."</td>" : "<td align=\"center\" class='tabletext row_print $css'>0</td>";
		}
		
	}
	if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal']) {
		$detail_table .= "<td align=\"center\" class='tabletext row_print $css'>$finalTotal</td>";	
	}
	$detail_table .= $temp_detail_table;
	
	// Merit / Demerit
	if($include_merit)
	{
		if($record_type==1) {
			$m = 1;
		} else {
			$m = 0;
		}
		$k = 0;
		foreach($i_MeritDemerit as $columnNo) {
			$css = ($k==0) ? "border_left" : "";
			$detail_table .= "<td align=\"center\" class='tabletext row_print $css'>";
			$detail_table .= $columnMeritDemeritCount[$m];
			
			if($record_type==1) {
				$m++;
			} else {
				$m--;
			}
			$detail_table .= "</td>";
			$k++;
		}
	}
	
	$detail_table .= "</tr>";
}

$detail_table .=  "</table>\n";

############ End of Report Table ###############
################################################
	
?>


<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right" class='print_hide'>
			<?=$linterface->GET_BTN($button_print, "button","javascript:window.print()")?>
		</td>
	</tr>
	<tr>
		<td class='tabletext'>
			<?=$eDiscipline['GoodConductMisconductReport']?>
		</td>
	</tr>
	<tr>
		<td>
			<?=$detail_table?>
		</td>
	</tr>
</table>
<?
intranet_closedb();

include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");

?>