<?php
# Class Accumulative Punishment Report
# - show selected students Accumulative Punishment Records

$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-GoodConduct_Misconduct_Daily_Report-View")) 
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$linterface = new interface_html();

$CurrentPage = "Reports_GMReport";
$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

if($ldiscipline->CHECK_ACCESS("Discipline-REPORTS-StudentReport-View"))
{
	$TAGS_OBJ[] = array($Lang['eDiscipline']['GMReport'],"../goodconduct_misconduct/");
}
if($ldiscipline->CHECK_ACCESS("Discipline-REPORTS-GoodConduct_Misconduct_Ranking_Report-View"))
{
	$TAGS_OBJ[] = array($Lang['eDiscipline']['GMRankingReport'],"../ranking/");
}
if($ldiscipline->CHECK_ACCESS("Discipline-REPORTS-GoodConduct_Misconduct_Ranking_Detail_Report-View"))
{
	$TAGS_OBJ[] = array($Lang['eDiscipline']['Misconduct_Detail_Ranking_Report'],"../ranking_detail/");
}
if($ldiscipline->CHECK_ACCESS("Discipline-REPORTS-GoodConduct_Misconduct_Daily_Report-View"))
{
	$TAGS_OBJ[] = array($Lang['eDiscipline']['GMDailyReport'],"../daily_report/",1);
}

$linterface->LAYOUT_START();


$startdate = $startdate==""?date('Y-m-d'):$startdate;
$enddate = $enddate==""?date('Y-m-d'):$enddate;

$select_list = $ldiscipline->showMisconductList($misconductid,0);
$CategoryID = $misconductid[0];

if(!empty($CategoryID))
{
	$use_intranet_subject = $ldiscipline->accumulativeUseIntranetSubjectList();
	
	$item_list = array();

	if($CategoryID==2 && $use_intranet_subject){ # Homework
		$sql = "SELECT RecordID, ".Get_Lang_Selection('CH_DES','EN_DES')." as SubjectName FROM ASSESSMENT_SUBJECT WHERE RecordStatus = 1 order by DisplayOrder";
	}
	else{ # Others
		$sql="SELECT ItemID,Name FROM DISCIPLINE_ACCU_CATEGORY_ITEM WHERE CategoryID='$CategoryID' AND RecordStatus=1 ORDER BY DisplayOrder";
	}
    $temp = $ldiscipline->returnArray($sql,2);
	$item_order = array();
	
	for($i=0;$i<sizeof($temp);$i++){
		list($item_id,$item_name) = $temp[$i];
		$item_list[$item_id]['name'] = $item_name;
		$item_list[$item_id]['records']="";
		$item_order[]= $item_id;
	}	
	

	$namefield = getNameFieldByLang("b.");
	
	## Get ACCU. punish Record
	
	$conds .= ($include_waive) ? " AND a.RecordStatus IN (".DISCIPLINE_STATUS_APPROVED.",".DISCIPLINE_STATUS_WAIVED.")" : "AND a.RecordStatus=".DISCIPLINE_STATUS_APPROVED;
	
	$sql="SELECT a.StudentID,$namefield,a.ItemID,b.ClassName,DATE_FORMAT(a.RecordDate,'%Y-%m-%d'),b.ClassNumber 
	FROM DISCIPLINE_ACCU_RECORD AS a 
	LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) 
	WHERE a.CategoryID='$CategoryID' AND a.RecordDate>='$startdate' AND a.RecordDate<='$enddate' $conds
	ORDER BY b.ClassName,b.ClassNumber,a.RecordDate";
	$temp = $ldiscipline->returnArray($sql);
	
	## build result array
	for($i=0;$i<sizeof($temp);$i++){
		list($student_id,$student_name,$item_id,$class_name,$record_date,$class_number)=$temp[$i];
		if($CategoryID==1)
			$item_list[1]['records'][$class_name][] = array($student_id,$student_name,$record_date,$class_number);
		else 
			$item_list[$item_id]['records'][$class_name][]= array($student_id,$student_name,$record_date,$class_number);
	}
	
	$display = "";
	if(!empty($item_list))
	{
		foreach($item_list as $k=>$d)
		{
			$this_item_name = $d['name'];
			$this_records_ary = $d['records'];
			
			if(!empty($this_item_name))
				$display .= $linterface->GET_NAVIGATION2($this_item_name);
			
			if(!empty($this_records_ary))
			{
				
				$display.= '<div class="table_board">';
				$display.= '<table class="common_table_list_v30 view_table_list_v30">';
				$display.= '<tr>
						<th width="25%">'. $i_ClassName.'</th>
						<th width="25%" class="sub_row_top">'. $i_ClassNumber.'</th>
						<th width="25%" class="sub_row_top">'. $i_UserStudentName.'</th>
						<th width="25%" class="sub_row_top">'. $i_general_record_date.'</th>
						</tr>';
			
			
				foreach($this_records_ary as $this_classname=>$d1)
				{
					$i=0;
					foreach($d1 as $k2 => $this_record)
					{
						list($this_student_id, $this_student_name, $this_record_date, $this_class_number) = $this_record;
						$display .= '<tr class="sub_row">';
						if($i==0)
							$display .= '<td rowspan='. sizeof($d1) .' class="sub_row_group">'. $this_classname .'</td>';
						$display .= '<td>'. $this_class_number .'</td>';
						$display .= '<td>'. $this_student_name .'</td>';
						$display .= '<td>'. $this_record_date .'</td>';
						$display .= '</tr>';
						$i++;
					}	
				}
				
				$display.= '</table>';
			}	
			else
			{
				$display .= "<div class='no_record_find_short_v30'>". $Lang['General']['NoRecordFound'] ."</div>";
			}
			
			$display.= '</div>';
			
			$display .= "<br>";
		}
	}
	
	
}

?>

<SCRIPT LANGUAGE="JavaScript">

function doPrint()
{
	document.form1.action = "daily_print.php";
	document.form1.target = "_blank";
	document.form1.submit();
	document.form1.action = "";
	document.form1.target = "_self"; 
}

function doExport()
{
	document.form1.action = "daily_export.php";
	document.form1.submit();
	document.form1.action = "";
}

function view()
{
		document.form1.action = "";
		document.form1.flag.value=1;
		document.form1.submit();
}

</SCRIPT>

<form name="form1" action="" method="POST">

<div class="this_form">
<table class="form_table_v30">
<tr>
	<td class="field_title"><?=$i_general_startdate?></td>
	<td><?=$linterface->GET_DATE_PICKER("startdate",$startdate);?></td>
</tr>
<tr>
	<td class="field_title"><?=$i_general_enddate?></td>
	<td><?=$linterface->GET_DATE_PICKER("enddate",$enddate);?></td>
</tr>
<tr>
	<td class="field_title"><?=$iDiscipline['Accumulative_Category']?></td>
	<td><?=$select_list?><br><input type="checkbox" name="include_waive" id="include_waive" value="1" <? if($include_waive==1) echo " checked"; ?>><label for="include_waive"><?=$Lang['eDiscipline']['IncludeWaivedRecords']?></label></td>
</tr>
</table>

<div class="edit_bottom_v30">
<p class="spacer"></p>
	<?= $linterface->GET_ACTION_BTN($button_view, "button", "javascript:view()")?>
<p class="spacer"></p>
</div>

<?=$display?>

<? if(!empty($CategoryID)) {?>
<div class="edit_bottom_v30">
<p class="spacer"></p>
	<?= $linterface->GET_ACTION_BTN($button_export, "button", "javascript:doExport();"); ?>&nbsp;
	<?= $linterface->GET_ACTION_BTN($button_print, "button", "javascript:doPrint();"); ?>&nbsp;
<p class="spacer"></p>
</div>
<? } ?>
			
			
</div>

<input type="hidden" name="flag" value="0">
<input type="hidden" name="printContent" value="<?=urlencode($print_content)?>">
<input type="hidden" name="exportContent" value="<?=urlencode($export_content)?>">
<input type="hidden" name="meritType" value="<?=$meritType?>"/>
</form>


<?
 $linterface->LAYOUT_STOP();
?>