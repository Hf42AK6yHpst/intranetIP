<?php

$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-GoodConduct_Misconduct_Daily_Report-View")) 
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$linterface = new interface_html();

$startdate = $startdate==""?date('Y-m-d'):$startdate;
$enddate = $enddate==""?date('Y-m-d'):$enddate;

$CategoryID = $misconductid[0];

if(!empty($CategoryID))
{
	$use_intranet_subject = $ldiscipline->accumulativeUseIntranetSubjectList();
	
	$item_list = array();

	if($CategoryID==2 && $use_intranet_subject){ # Homework
		$sql = "SELECT RecordID, ".Get_Lang_Selection('CH_DES','EN_DES')." as SubjectName FROM ASSESSMENT_SUBJECT WHERE RecordStatus = 1 order by DisplayOrder";
	}
	else{ # Others
		$sql="SELECT ItemID,Name FROM DISCIPLINE_ACCU_CATEGORY_ITEM WHERE CategoryID='$CategoryID' AND RecordStatus=1 ORDER BY DisplayOrder";
	}
    $temp = $ldiscipline->returnArray($sql,2);
	$item_order = array();
	
	for($i=0;$i<sizeof($temp);$i++){
		list($item_id,$item_name) = $temp[$i];
		$item_list[$item_id]['name'] = $item_name;
		$item_list[$item_id]['records']="";
		$item_order[]= $item_id;
	}	
	
	$namefield = getNameFieldByLang("b.");
	
	## Get ACCU. punish Record
	$conds .= ($include_waive) ? " AND a.RecordStatus IN (".DISCIPLINE_STATUS_APPROVED.",".DISCIPLINE_STATUS_WAIVED.")" : "AND a.RecordStatus=".DISCIPLINE_STATUS_APPROVED;
	
	$sql="SELECT a.StudentID,$namefield,a.ItemID,b.ClassName,DATE_FORMAT(a.RecordDate,'%Y-%m-%d'),b.ClassNumber 
	FROM DISCIPLINE_ACCU_RECORD AS a 
	LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) 
	WHERE a.CategoryID='$CategoryID' AND a.RecordDate>='$startdate' AND a.RecordDate<='$enddate' $conds
	ORDER BY b.ClassName,b.ClassNumber,a.RecordDate";
	$temp = $ldiscipline->returnArray($sql);
	
	## build result array
	for($i=0;$i<sizeof($temp);$i++){
		list($student_id,$student_name,$item_id,$class_name,$record_date,$class_number)=$temp[$i];
		if($CategoryID==1)
			$item_list[1]['records'][$class_name][] = array($student_id,$student_name,$record_date,$class_number);
		else 
			$item_list[$item_id]['records'][$class_name][]= array($student_id,$student_name,$record_date,$class_number);
	}
	
	$display = "";
	if(!empty($item_list))
	{
		foreach($item_list as $k=>$d)
		{
			$this_item_name = $d['name'];
			$this_records_ary = $d['records'];
			
			if(!empty($this_item_name))
				$display .= $linterface->GET_NAVIGATION2($this_item_name);
			
			if(!empty($this_records_ary))
			{
				
				$display.= '<div class="table_board">';
				$display.= '<table class="common_table_list_v30 view_table_list_v30">';
				$display.= '<tr>
						<th width="25%">'. $i_ClassName.'</th>
						<th width="25%" class="sub_row_top">'. $i_ClassNumber.'</th>
						<th width="25%" class="sub_row_top">'. $i_UserStudentName.'</th>
						<th width="25%" class="sub_row_top">'. $i_general_record_date.'</th>
						</tr>';
			
			
				foreach($this_records_ary as $this_classname=>$d1)
				{
					$i=0;
					foreach($d1 as $k2 => $this_record)
					{
						list($this_student_id, $this_student_name, $this_record_date, $this_class_number) = $this_record;
						$display .= '<tr class="sub_row">';
						if($i==0)
							$display .= '<td rowspan='. sizeof($d1) .'>'. $this_classname .'</td>';
						$display .= '<td>'. $this_class_number .'</td>';
						$display .= '<td>'. $this_student_name .'</td>';
						$display .= '<td>'. $this_record_date .'</td>';
						$display .= '</tr>';
						$i++;
					}	
				}
				
				$display.= '</table>';
			}	
			else
			{
				$display .= "<div class='no_record_find_short_v30'>". $Lang['General']['NoRecordFound'] ."</div>";
			}
			
			$display.= '</div>';
			
			$display .= "<br>";
		}
	}
	
	
}

$cat_name = $ldiscipline->RETRIEVE_ACCU_CATEGORY_NAME($CategoryID);


?>
<table width="100%" align="center" class="print_hide" border="0">
<tr>
	<td align="right"><?= $linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
</tr>
</table>

<div class="this_form">
<table class="form_table_v30">
<tr>
	<td class="field_title"><?=$i_general_startdate?></td>
	<td><?=$startdate?></td>
</tr>
<tr>
	<td class="field_title"><?=$i_general_enddate?></td>
	<td><?=$enddate?></td>
</tr>
<tr>
	<td class="field_title"><?=$iDiscipline['Accumulative_Category']?></td>
	<td><?=$cat_name?></td>
</tr>
</table>

<?=$display?>
			
</div>

