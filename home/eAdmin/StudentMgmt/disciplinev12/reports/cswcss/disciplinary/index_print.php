<?php
// Modifying by : Bill

########## Change Log ###############
#
#	Date	:	2016-12-09	(Bill)	[2016-1103-1545-24206]
#				support print alumni disciplinary record
#
##################################### 

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

$lclass = new libclass();
$linterface = new interface_html();

### Table Data ###

if ($radioPeriod == "YEAR") {
	$parPeriodField1 = $selectYear;
	$parPeriodField2 = $selectSemester;
} else {
	$parPeriodField1 = $textFromDate;
	$parPeriodField2 = $textToDate;
}

// Current Student
$isAlumni = false;
if($studentTarget=="current") {
	$parByFieldArr = $rankTargetDetail;
}
// Alumni
else {
	$isAlumni = true;
	$parByFieldArr = $AlumniStudentID;
}

$tableContent = $ldiscipline->retrieve_CSWCSS_DisciplinaryRecord($radioPeriod, $parPeriodField1, $parPeriodField2, $rankTarget, $parByFieldArr, $studentID, $display=2, $isAlumni);

############ End of Report Table ###############
################################################
	
?>

<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right" class='print_hide'>
			<?=$linterface->GET_BTN($button_print, "button","javascript:window.print()")?>
		</td>
	</tr>
	<tr>
		<td valign="top">
			<?=$tableContent?>
		</td>
	</tr>
</table>

<?

intranet_closedb();

include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");

?>