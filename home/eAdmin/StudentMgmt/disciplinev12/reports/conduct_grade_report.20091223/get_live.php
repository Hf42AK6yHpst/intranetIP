<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

header("Content-Type:text/html;charset=BIG5");

$ldiscipline = new libdisciplinev12();

$yearID = Get_Current_Academic_Year_ID();

$clsName = Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN');

$sql = "SELECT
			$clsName, 
			$clsName
		FROM
			INTRANET_USER USR LEFT OUTER JOIN
			SUBJECT_TERM_CLASS_TEACHER stct ON (stct.UserID=USR.UserID) LEFT OUTER JOIN
			SUBJECT_TERM_CLASS_USER stcu ON (stcu.SubjectGroupID=stct.SubjectGroupID) LEFT OUTER JOIN
			YEAR_CLASS_USER ycu ON (stcu.UserID=ycu.UserID) LEFT OUTER JOIN
			YEAR_CLASS_TEACHER yct ON (yct.UserID=USR.UserID) LEFT OUTER JOIN
			YEAR_CLASS yc ON (yc.YearClassID=yct.YearClassID OR ycu.YearClassID=yc.YearClassID) LEFT OUTER JOIN
			YEAR y ON (yc.YearID=y.YearID) LEFT OUTER JOIN
			SUBJECT_TERM_CLASS_YEAR_RELATION stcyr ON (stcyr.SubjectGroupID=stct.SubjectGroupID AND y.YearID=stcyr.YearID)
		WHERE
			USR.UserID=$teacherID AND
			yc.AcademicYearID=$yearID
		GROUP BY 
			yc.YearClassID
		ORDER BY y.Sequence, yc.Sequence
		";

$result = $ldiscipline->returnArray($sql, 2);

$data = getSelectByArray($result, "name='class' id='class'", $class, 1, 1, "");

echo $data;

intranet_closedb();
?>