<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");


intranet_auth();
intranet_opendb();

$linterface = new interface_html("popup.html");
$ldiscipline = new libdisciplinev12();

$result = $ldiscipline->getTeacherConductGradeReport($year, $semester, $class, $teacherID, $print);

$name = $ldiscipline->getUserNameByID($_SESSION['UserID']);
$printedBy = $iDiscipline['PrintedBy']." : ".$name[0];

$teacherName = $ldiscipline->getUserNameByID($teacherID);
# Start layout
?>
<link href='<?=$PATH_WRT_ROOT?>/templates/2007a/css/print.css' rel='stylesheet' type='text/css'>
<style type='text/css'>
<!--
print_hide {display:none;}
-->
</style>

<br />
<form name="form1" method="post" action="">
<table width="95%" cellpadding="4" cellspacing="0" border="0">
	<tr>
		<td class='tabletext'><?=$i_general_Teacher." : ".$teacherName[0]?><br /><?=$i_general_class." : ". $class?><br /><?=$printedBy?><br /><?=$i_general_print_date." : ".(Date("d-M-y"))?></td>
		<td valign='top' class='print_hide'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print()")?></td>
	<tr>
	<tr>
		<td class="tabletext">
			<?=$result?>
		</td>
	<tr>
</table>

<input type="hidden" name="year" id="year" value="<?=getCurrentAcademicYear()?>">
</form>
<?
intranet_closedb();
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");

?>
