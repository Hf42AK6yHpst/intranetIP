<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$lclass = new libclass();
$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lu = new libuser();

if($submitBtn != "") $result = $ldiscipline->getTeacherConductGradeReport($year, $semester, $class, $teacherID, $print);

# menu highlight setting
$CurrentPage = "ConductGradeReport";

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($iDiscipline['Class_Report'], "/home/eAdmin/StudentMgmt/disciplinev12/reports/conduct_grade_report/class_report.php", 0);
$TAGS_OBJ[] = array($iDiscipline['Teacher_Report'], "/home/eAdmin/StudentMgmt/disciplinev12/reports/conduct_grade_report/teacher_report.php", 1);

$yearID = Get_Current_Academic_Year_ID();

# Semester Menu
$semester_data = getSemesters($yearID,0);	# 0 : not return associated array
$selectSemesterMenu = getSelectByArray($semester_data, "name='semester' id='semester'", $semester, 0,1, "", 1);

/*
$semester_data = split("\n",get_file_content("$intranet_root/file/semester.txt"));
$currentSemester = getCurrentSemester();
$selectSemesterMenu = "<select name=\"semester\">";
$selectSemesterMenu .= "<option value='#'>-- ".$button_select." --</option>";
for ($i=0; $i<sizeof($semester_data); $i++) {
	$target_sem = $semester_data[$i];
	$line = split("::",$target_sem);
	list ($name,$current) = $line;
	$selectSemesterMenu .= "<option value=\"".$name."\"";
	$selectSemesterMenu .= ($semester==$name) ? " selected" : "";
	$selectSemesterMenu .= ">".$name."</option>";
}
$selectSemesterMenu .= "</select>";
*/

# teacher menu
//$teacherList = $lu->returnUsersType(1);
$teacherList = $ldiscipline->getTeachingList();
$SelectTeacher = "<select name='teacherID' onChange='showResult(this.value)'>\n";
$SelectTeacher .= "<option value='#'>-- ".$button_select." --</option>";
for($i=0; $i<sizeof($teacherList); $i++) {
	$SelectTeacher .= "<option value='".$teacherList[$i][0]."'";
	$SelectTeacher .= ($teacherID==$teacherList[$i][0]) ? " selected" : "";
	$SelectTeacher .= ">".$teacherList[$i][1]."</option>\n";
}
$SelectTeacher .= "</select>\n";

# class menu
$ClassListArr = $lclass->getClassList();
$SelectedClassArr = explode(",", $SelectedClass);
$SelectedClassTextArr = explode(",", $SelectedClassText);
$selectClassHTML = "<select name=\"class\">\n";
for ($i = 0; $i < sizeof($ClassListArr); $i++) {
	$selectClassHTML .= "<option value=\"".$ClassListArr[$i][1]."\"";
	$selectClassHTML .= ($ClassListArr[$i][1]==$class) ? " selected" : "";
	$selectClassHTML .= ">".$ClassListArr[$i][1]."</option>\n";
}
$selectClassHTML .= "</select>\n";

# Start layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
<!--
function checkForm() {
	var obj = document.form1;
	if(form1.semester.value=="#") {
		alert("<?=$i_general_please_select." ".$i_Discipline_System_Conduct_Semester?>");	
		return false;
	} 
	if(form1.teacherID.value=="#") {
		alert("<?=$i_general_please_select." ".$i_general_Teacher?>");	
		return false;
	}
	
	document.getElementById('print').value = "";
	document.getElementById('view').value = 1;
	form1.target = "_self";
	form1.action = "teacher_report.php";
	
	
}

function goPrint() {
	document.getElementById('print').value = 1;
	document.getElementById('view').value = 1;
	form1.target = "_blank";
	form1.action = "teacher_print.php";
	form1.submit();	
}
//-->
</script>
<script language="javascript">
<!--
function showResult(teacherid) {
	
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 

	var url = "";
		
	url = "get_live.php?year="+document.getElementById('year').value;
	url += "&semester=" + document.getElementById('semester').value;
	url += "&teacherID=" + teacherid;
	<? if($class!="") { ?>
	url += "&class=<?=$class?>";
	<? } ?>


	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById("spanClass").innerHTML = xmlHttp.responseText;
		document.getElementById("spanClass").style.border = "0px solid #A5ACB2";
	} 
}

function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

//-->
</script>

<br />
<form name="form1" method="post" action="">
<table width="95%" cellpadding="4" cellspacing="0" border="0">
	<tr>
		<td width="20%" class="formfieldtitle"><?=$i_Discipline_System_Conduct_School_Year?></td>
		<td width="80%" class="tablerow1"><?=getCurrentAcademicYear()?></td>
	<tr>
	<tr>
		<td width="20%" class="formfieldtitle"><?=$i_Discipline_System_Conduct_Semester?></td>
		<td width="80%" class="tablerow1"><?=$selectSemesterMenu?></td>
	<tr>
	<tr>
		<td width="20%" class="formfieldtitle"><?=$i_general_Teacher?></td>
		<td width="80%" class="tablerow1"><?=$SelectTeacher?></td>
	<tr>
	<tr>
		<td width="20%" class="formfieldtitle"><?=$i_general_class?></td>
		<td width="80%" class="tablerow1"><span id="spanClass"><select></select></span></td>
	<tr>
	<tr>
		<td width="20%">&nbsp;</td>
		<td width="80%" class="tablerow1"><?= $linterface->GET_ACTION_BTN($button_submit, "submit", "return checkForm()", "submitBtn")?></td>
	<tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	<tr>
	<tr>
		<td colspan="2"><?=$result?></td>
	<tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	<tr>
	<? if($submitBtn != "") { ?>
	<tr>
		<td colspan="2" align="center"><?=$linterface->GET_ACTION_BTN($button_print, "button", "goPrint()")?></td>
	<tr>
	<? } ?>
</table>

<input type="hidden" name="year" id="year" value="<?=getCurrentAcademicYear()?>">
<input type="hidden" name="print" id="print" value="<?=$print?>">
<input type="hidden" name="view" id="view" value="<?=$view?>">
</form>
<br />
<script language="javascript">
<!--
if(document.getElementById('view').value==1) {
	showResult(document.getElementById('teacherID').value);	
}
//-->
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
