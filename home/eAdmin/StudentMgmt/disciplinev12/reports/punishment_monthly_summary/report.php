<?php
// Editing by 

/**************************************************
 * 	Modification log
 * 	20151110 Bill	[2015-0303-1550-45164]
 * 		- create file
 * ***********************************************/
 
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_cust.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# user access right checking
$ldiscipline = new libdisciplinev12_cust();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || !$sys_custom['eDiscipline']['MonthlySummaryReport']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$ldiscipline_ui = new libdisciplinev12_ui_cust();
$lexport = new libexporttext();

# Term Name
$sql = "SELECT YearTermNameB5 FROM ACADEMIC_YEAR_TERM WHERE '$Year-$Month-01' BETWEEN TermStart AND TermEnd";
$TermName = $ldiscipline->returnVector($sql);
$TermName = $TermName[0];

# CSV Header
$header = array("$Month/$Year ($TermName) 違規紀錄總覽 (級別)");
	
# CSV Content
$rows = $ldiscipline_ui->getMonthlySummary($_POST);
//debug_pr($rows);

# Export
$exportContent = "";
$exportContent .= $lexport->GET_EXPORT_TXT($rows, $header);
$lexport->EXPORT_FILE("MonthlySummary.csv", $exportContent);

intranet_closedb();

//debug_pr('convert_size(memory_get_usage()) = '.convert_size(memory_get_usage()));
//debug_pr('convert_size(memory_get_usage(true)) = '.convert_size(memory_get_usage(true)));
//debug_pr('Query Count = '.$GLOBALS[debug][db_query_count]);

?>