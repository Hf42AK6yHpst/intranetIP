<?php
// Editing by 

/**************************************************
 * 	Modification log
 * 	20151110 Bill	[2015-0303-1550-45164]
 * 		- create file
 * 		- commented coding related to class and form selection
 * ***********************************************/
 
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");;
//include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# user access right checking
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || !$sys_custom['eDiscipline']['MonthlySummaryReport']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$linterface = new interface_html();

# Page
$CurrentPage = "Reports_Punishment_Monthly_Summary";
$CurrentPageArr['eDisciplinev12'] = 1;

# Tab
$TAGS_OBJ[] = array($Lang['eDiscipline']['MonthlySummaryReport']['Title'], "", 1);
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# School Year
$CurrentYearID = Get_Current_Academic_Year_ID();
$YearAry = $ldiscipline->returnAllYearsSelectionArray();
$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($YearAry, "name='selectYear' id='selectYear' onChange='showResult(); changeYearMonth(this);'", "", $CurrentYearID);

# Year
$CurrentYear = date('Y');
$CurrentSchoolYearName = getAcademicYearByAcademicYearID($CurrentYearID);
$CurrentSchoolYearName = explode("-", $CurrentSchoolYearName);
$selectYearHTML = "<select name=\"Year\" id=\"Year\">";
for ($i=0; $i<sizeof($CurrentSchoolYearName); $i++)
{
     $YearName = $CurrentSchoolYearName[$i];
     $string_selected = ($YearName==$CurrentYear? "selected":"");
     $selectYearHTML .= "<option value='$YearName' $string_selected>$YearName</option>\n";
}
$selectYearHTML .= "</select>\n";

# Month
$CurrentMonth = date('n')-1;
$Months = $i_general_MonthShortForm;
$selectMonthHTML = "<select name=\"Month\" id=\"Month\">";
for ($i=0; $i<sizeof($Months); $i++)
{
     $MonthName = $Months[$i];
     $string_selected = ($CurrentMonth==$i? "selected":"");
     $selectMonthHTML .= "<option value=".($i+1)." $string_selected>$MonthName</option>\n";
}
$selectMonthHTML .= "</select>\n";

$linterface->LAYOUT_START();

?>
<script type="text/javascript" language="JavaScript">

/*
function changeType(form_obj,lvl_value)
{
     obj = form_obj.elements["target[]"]
     <? # Clear existing options
     ?>
     while (obj.options.length > 0)
     {
            obj.options[0] = null;
     }
     if (lvl_value==1)
     {
         <?
         for ($i=0; $i<sizeof($levels); $i++)
         {
              list($id,$name) = $levels[$i];
              ?>
              obj.options[<?=$i?>] = new Option('<?=intranet_htmlspecialchars($name)?>',<?=$id?>);
              <?

         }
         ?>
     }
     else
     {
         <?
         for ($i=0; $i<sizeof($classes); $i++)
         {
              list($id,$name, $lvl_id) = $classes[$i];
              ?>
              obj.options[<?=$i?>] = new Option('<?=$name?>',<?=$id?>);
              <?

         }
         ?>
     }
}
*/

function showResult()
{
	/*
	var curYear = $("select#selectYear").val();
	var targetType = $("select#level").val();
	
	var str = "";
	if(targetType == 1){
		str = "form";
	} 
	else if(targetType == 2){
		str = "class";
	}
	else{
		$("#rankTargetDetail").html("<select name='rankTargetDetail[]' id='rankTargetDetail[]' class='formtextbox' multiple='multiple' size='5'></select>");
		document.getElementById("rankTargetDetail").style.border = "0px solid #A5ACB2";
		return false;
	} 
	var url = "../ajaxGetLive.php?target="+str+"&fieldId=rankTargetDetail[]&fieldName=rankTargetDetail[]&academicYearId="+curYear;
	
	$.get(
		url,
		{},
		function(responseText){
			var showIn = "rankTargetDetail";
			$("#"+showIn).html(responseText);
			document.getElementById(showIn).style.border = "0px solid #A5ACB2";
		}
	);
	*/
}

function changeYearMonth(obj){
	var schoolyear = $("#selectYear option:selected").text();
	schoolyear = schoolyear.split("-");
	$("#Year").html("<option value='"+schoolyear[0]+"' selected>"+schoolyear[0]+"</option><option value='"+schoolyear[1]+"'>"+schoolyear[1]+"</option>");
}

/*
function showRankTargetResult(val) {
	showResult(val);
}

function hideOptionLayer()
{
	$('.Form_Span').hide();
	$('.spanHideOption').hide();
	$('.spanShowOption').show();
	document.getElementById('div_form').className = 'report_option report_hide_option';
}

function showOptionLayer()
{
	$('.Form_Span').show();
	$('.spanHideOption').show();
	$('.spanShowOption').hide();
	document.getElementById('div_form').className = 'report_option report_show_option';
}

function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function SelectAllItem(obj, flag) {
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = flag;
	}
}
*/

function submitForm(format)
{
	/*
	var valid = true;
	var form_obj = $("#form1");
	
	if($("select#level").val()!=0 && $("select#rankTargetDetail\\[\\] option:selected").length == 0){
		valid = false;
		$("#TargetWarnDiv span").html("<?=$i_alert_pleaseselect.$Lang['eDiscipline']['MonthlySmmaryReport']['Target']?>");
		$("#TargetWarnDiv").show();
	}else{
		$("#TargetWarnDiv").hide();
	}
	
	if(!valid) return;
	*/
	
	document.form1.action = "report.php";
	document.form1.submit();
	//document.form1.action = "";
	
//	$('input#format').val(format);
//	if(format == 'web'){
//		form_obj.attr('target','');
//		Block_Element('PageDiv');
//		
//		$.post(
//			'report.php',
//			$('#form1').serialize(),
//			function(data){
//				$('#ReportDiv').html(data);
//				document.getElementById('div_form').className = 'report_option report_hide_option';
//				$('.spanShowOption').show();
//				$('.spanHideOption').hide();
//				$('.Form_Span').hide();
//				UnBlock_Element('PageDiv');
//			}
//		);
//	} else if(format == 'print'){
//		form_obj.attr('target','_blank');
//		form_obj.submit();
//	}
}

</script>

<div id="PageDiv">
<div id="div_form">
	<!--
	<span id="spanShowOption" class="spanShowOption" style="display:none">
		<a href="javascript:showOptionLayer();"><?=$Lang['Btn']['ShowOption']?></a>
	</span>
	<span id="spanHideOption" class="spanHideOption" style="display:none">
		<a href="javascript:hideOptionLayer();"><?=$Lang['Btn']['HideOption']?></a>
	</span>
	-->
	<p class="spacer"></p> 
	
	<span class="Form_Span">
		<form id="form1" name="form1" method="post" action="report.php" onsubmit="submitForm('print'); return false;">
		<table class="form_table_v30">
		
			<!-- School Year -->
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eDiscipline']['SchoolYear']?></td>
				<td><table class="inside_form_table">
						<tr><td colspan="6"><?=$selectSchoolYearHTML?></td></tr>
				</table></td>
			</tr>
		
			<!-- Period -->
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eDiscipline']['Period']?></td>
				<td><table class="inside_form_table">
						<tr><td colspan="6"><?=$selectYearHTML?>&nbsp;&nbsp;<?=$selectMonthHTML?></td></tr>
				</table></td>
			</tr>
			
			<!-- Target -->
			<!--
			<tr class="tabletext">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eDiscipline']['Target']?></td>
				<td>
					<select name="level" id="level" onChange="showResult()">
						<option value="0" selected><?=$i_general_WholeSchool?></option>
						<option value="1"><?=$i_Discipline_Form?></option>
						<option value="2"><?=$i_Discipline_Class?></option>
					</select>
					 
					<table class="inside_form_table">
						<tr>
							<td valign="top" nowrap>
								<span id='rankTargetDetail'>
									<select name="rankTargetDetail[]" id="rankTargetDetail[]" class="formtextbox" multiple="multiple" size="5"></select>
								</span>
							</td>
						</tr>
						<tr><td colspan="3" class="tabletextremark">(<?=$Lang['eDiscipline']['PressCtrlKey']?>)</td></tr>
					</table>
					<span id='div_Target_err_msg'></span>
					<?=$linterface->Get_Form_Warning_Msg("TargetWarnDiv","")?>
				</td>
			</tr>
			-->
		</table>
		
		<?=$linterface->MandatoryField();?>
		
		<div class="edit_bottom_v30">
		<p class="spacer"></p>
			<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit")?>
		<p class="spacer"></p>
		</div>
		
		<!--<input type="hidden" name="format" id="format" value="print">-->
		</form>
	</span>
</div>

<!--
<div id="ReportDiv"></div>
</div>
-->

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
