<?php
# using: 

###########################################################
#
#	Date:	2019-05-13  Bill
#           Prevent SQL Injection
#
#	Date:	2017-01-23	Bill	[2015-0120-1200-33164]
#			Create File
#			- Copy from conductmark_report
#
###########################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$selectYear = IntegerSafe($selectYear);
$semester = IntegerSafe($semester);
$level = IntegerSafe($level);
$target = IntegerSafe($target);

$startMark = IntegerSafe($startMark);
$endMark = IntegerSafe($endMark);

if($listBy != 'byClass' && $listBy != 'byIndividual') {
    $listBy = '';
}
if($sortBy != 'byMark' && $sortBy != 'byClass') {
    $sortBy = '';
}
### Handle SQL Injection + XSS [END]

$ldiscipline = new libdisciplinev12();
if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-ConductMarkReport-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$linterface = new interface_html();
$lclass = new libclass();

# Year Menu 
if ($selectYear == "") {
	$selectYear = Get_Current_Academic_Year_ID();
}

$years = $ldiscipline->returnAllYearsSelectionArray();
$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='selectYear' id='selectYear' onChange='changeTerm(this.value); changeClassForm(this.value)'", "", $selectYear);

$StudyScoreCalculationMethod = $ldiscipline->retriveStudyScoreCalculationMethod();

########################################################

$meritDemeritType['1'] = $i_Merit_Merit;
$meritDemeritType['2'] = $i_Merit_MinorCredit;
$meritDemeritType['3'] = $i_Merit_MajorCredit;
$meritDemeritType['4'] = $i_Merit_SuperCredit;
$meritDemeritType['5'] = $i_Merit_UltraCredit;
$meritDemeritType['0'] = $i_Merit_Warning;
$meritDemeritType['-1'] = $i_Merit_BlackMark;
$meritDemeritType['-2'] = $i_Merit_MinorDemerit;
$meritDemeritType['-3'] = $i_Merit_MajorDemerit;
$meritDemeritType['-4'] = $i_Merit_SuperDemerit;
$meritDemeritType['-5'] = $i_Merit_UltraDemerit;

if($startMark == "") {
	$startMark = 0;
}
if($endMark == "") {
	$endMark = 100;
}

if($flag == 1)
{
	$optionString = "<td id=\"tdOption\" class=\"tabletextremark\"><span id=\"spanShowOption\"><a href=\"javascript:showOption();\" class=\"contenttool\"><img src=\"$image_path/$LAYOUT_SKIN/icon_show_option.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">$i_Discipline_Show_Statistics_Option</a></span>";
	$optionString .= "<span id=\"spanHideOption\" style=\"display:none\"><a href=\"javascript:hideOption();\" class=\"contenttool\"><img src=\"$image_path/$LAYOUT_SKIN/icon_hide_option.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">$i_Discipline_Hide_Statistics_Option</a></span></td>";
	$initialString = "<script language=\"javascript\">hideOption();</script>\n";

	for($i=0; $i<sizeof($target); $i++)
	{
		if ($level == 1) {
			$sql = "SELECT YearName FROM YEAR WHERE YearID = '".$target[$i]."'";
			$result = $ldiscipline->returnVector($sql);
			$storeTarget[$i] = $result[0];
		}
		else {
			$storeTarget[$i] = $lclass->getClassName($target[$i]);
		}
	}
	
	$tableContent = "";
	$exportContent = "&quot;".$iDiscipline['Period'].":". $ldiscipline->getAcademicYearNameByYearID($selectYear)." ".$ldiscipline->getTermNameByTermID($semester)."&quot;\n";
	$exportContent .= "&quot;".(($level == 1) ? $i_ClassLevel : $i_ClassName).": ".implode(",", $storeTarget)."&quot;\n";
	$exportContent .= "&quot;".$i_Discipline_System_Subscore1.":".$startMark ." $i_To ".$endMark."&quot;\n\n";
	
	$rawDataAry = array();
	$rawDataAry['PeriodChoice'] = $Period;
	$rawDataAry['year'] = $selectYear;
	$rawDataAry['semester'] = $semester;
	$rawDataAry['level'] = $level;	# class / form
	$rawDataAry['target'] = $target;
	$rawDataAry['startMark'] = $startMark;
	$rawDataAry['endMark'] = $endMark;
	$rawDataAry['listBy'] = $listBy;
	$rawDataAry['sortBy'] = $sortBy;
	
	$levelName = ($level==0) ? "class" : "form";
	
	$studentIDs = $ldiscipline->getStudentIDByTarget($levelName, $target, $selectYear);
	if(sizeof($studentIDs)>0) {
		//$conductScoreAry = $ldiscipline->getConductMarkReport($rawDataAry);
		$baseMark = $ldiscipline->getStudyScoreRule("baseMark");
		## MethodType : 0 - By Term (Do Not Accumulate)
		##				1 - By Academic Year (Accumulate)
		$StudyScoreCalculationMethod = $ldiscipline->retriveStudyScoreCalculationMethod();
		
		foreach($studentIDs as $key=>$student_id){
			
			if($StudyScoreCalculationMethod == 0)		# not accumulate
			{
				
				if($semester=='' || $semester=='0') {
					$cond = " AND IsAnnual = 1";
				}
				else {
					$cond = " AND YearTermID = '".$semester."'";
				}
				
				$sql = "SELECT SubScore FROM DISCIPLINE_STUDENT_SUBSCORE_BALANCE WHERE 
							StudentID = '$student_id' and AcademicYearID = '$selectYear'
							$cond ";
							//echo $sql.'<br>';
				$tempArr = $ldiscipline->returnVector($sql);
				
				if($tempArr[0]!=''){
					$studyScore = $tempArr[0];
				}else{
					$studyScore = $baseMark;
				}
				
				//$adjustMark = $ldiscipline->getSemesterConductScoreAdjustByStudentID($student_id, $selectYear, $semester);
				$adjustMark = 0;
				$studyScore = $studyScore + $adjustMark;
				
				if($rawDataAry['listBy']=='byClass') {
					if(($rawDataAry['startMark'] <= $studyScore) && ($studyScore <= $rawDataAry['endMark'])){
						
						$sql = "select a.YearClassID from YEAR_CLASS as a inner join YEAR_CLASS_USER as b on(a.YearClassID = b.YearClassID) where a.academicyearid = '$selectYear' and b.userid = '$student_id'";
						$classID = $ldiscipline->returnVector($sql);
								
						$studyScoreAry[$classID[0]]++;
					}
				} else {
					if(($rawDataAry['startMark'] <= $studyScore) && ($studyScore <= $rawDataAry['endMark'])){
						$studyScoreAry[$student_id] = $studyScore;
					}
				}
				
			}
			else
			{
				$sql = "SELECT COUNT(*) FROM DISCIPLINE_STUDENT_SUBSCORE_BALANCE WHERE StudentID = '$student_id' AND AcademicYearID = '$selectYear'";
				$newRecord = $ldiscipline->returnVector($sql);
			
				if($newRecord[0] == 0) {
					$studyScore = $baseMark;		# base mark
					
//					$sql = "SELECT SUM(AdjustMark) FROM DISCIPLINE_CONDUCT_ADJUSTMENT WHERE StudentID = $student_id AND AcademicYearID = $selectYear";
//					$adjustMark = $ldiscipline->returnVector($sql);
//					$studyScore += $adjustMark[0];
										
				} else {
					$studyScore = "";
					if($semester == "") {
						$sql = "SELECT YearTermID FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '$selectYear' ORDER BY TermStart DESC LIMIT 0,1";
						$FinalSemester = $ldiscipline->returnVector($sql);
						$semester = $FinalSemester[0];
					}
					
					$TermStartDate = getStartDateOfAcademicYear($selectYear,$semester);
					
					$sql = "SELECT YearTermID FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '$selectYear' AND TermStart <= '$TermStartDate' ORDER BY TermStart DESC";
					$result = $ldiscipline->returnVector($sql);
					if(sizeof($result) > 0)
					{
						for($i=0; $i<sizeof($result); $i++)
						{
							$TermID = $result[$i];
							
							# get the latest study score balance
							$sql = "SELECT SubScore FROM DISCIPLINE_STUDENT_SUBSCORE_BALANCE WHERE StudentID = '$student_id' AND YearTermID = '$TermID'";
							$StudyScoreBalance = $ldiscipline->returnVector($sql);
							if($StudyScoreBalance[0] != "")
							{
								$studyScore = $StudyScoreBalance[0];
								break;
							}
						}
					}
					
					if($studyScore == "") {
						$studyScore = $baseMark;
					}
					
					$final_adjustMark = 0;
					
//					if(sizeof($result)>0){
//						for($i=0; $i<sizeof($result); $i++){
//							$TermID = $result[$i];
//							$sql = "SELECT SUM(AdjustMark) FROM DISCIPLINE_CONDUCT_ADJUSTMENT WHERE StudentID = $student_id AND AcademicYearID = $selectYear AND YearTermID = $TermID";
//							$adjustMark = $ldiscipline->returnVector($sql);
//							$final_adjustMark = $final_adjustMark+$adjustMark[0];
//						}
//					}
					
					$studyScore = $studyScore + $final_adjustMark;
				}
				
				if($rawDataAry['listBy']=='byClass')
				{
					if(($rawDataAry['startMark'] <= $studyScore) && ($studyScore <= $rawDataAry['endMark']))
					{
						$sql = "select a.YearClassID from YEAR_CLASS as a inner join YEAR_CLASS_USER as b on (a.YearClassID = b.YearClassID) where a.academicyearid = '$selectYear' and b.userid = '$student_id'";
						$classID = $ldiscipline->returnVector($sql);
						
						$studyScoreAry[$classID[0]]++;
					}
				}
				else
				{
					if(($rawDataAry['startMark'] <= $studyScore) && ($studyScore <= $rawDataAry['endMark'])){
						$studyScoreAry[$student_id] = $studyScore;
					}
				}
				
			}
		}
	}
	//debug_pr($conductScoreAry);
	
	if($sortBy=="byMark") {
		$studyScoreAry = (array)$studyScoreAry;
		arsort($studyScoreAry);	
	}
	
	# table header
	$tableContent .= "<table width='90%' cellpadding='4' cellspacing='0' border='0'>";
	$tableContent .= "	<tr class='tablebluetop'>";
	$tableContent .= "		<td class='tabletopnolink' width=''>$i_general_class</td>";
	if($listBy=='byClass') {
		$tableContent .= "		<td class='tabletopnolink' width=''>$list_total</td>";
	}
	else {
		$tableContent .= "		<td class='tabletopnolink' width='15%'>$i_ClassNumber</td>";
		$tableContent .= "		<td class='tabletopnolink' width='40%'>$i_general_name</td>";
		$tableContent .= "		<td class='tabletopnolink' width='30%'>".$i_Discipline_System_Subscore1."</td>";
	}
	$tableContent .= "	</tr>";
	
	# export header
	$exportContent .= "&quot;$i_ClassName&quot;\t";
	if($listBy=='byClass') {
		$exportContent .= "&quot;$list_total&quot;\n";	
	}
	else {
		$exportContent .= "&quot;$i_ClassNumber&quot;\t";
		$exportContent .= "&quot;$i_general_name&quot;\t";
		$exportContent .= "&quot;".$i_Discipline_System_Subscore1."&quot;\n";
	}
	
	if(sizeof($studentIDs)==0 || sizeof($studyScoreAry)==0) {
		$colspan = ($listBy=='byClass') ? 2 : 4;
		$tableContent .= "	<tr><td colspan='$colspan' class='tabletext' align='center' height='80'>$i_no_record_exists_msg</td></tr>";
		
		$exportContent .= "&quot;$i_no_record_exists_msg&quot;\n";
	}
	else {
		$i = 0;
		foreach($studyScoreAry as $id=>$value)
		{	
			$css = ($i%2==0) ? 1 : 2;
			
			#table content 
			$tableContent .= "	<tr class='tablebluerow$css'>";
			if($listBy=='byClass') {
				$tableContent .= "		<td class='tabletext'>".$ldiscipline->getClassNameByClassID($id)."</td>";
				$tableContent .= "		<td class='tabletext'>$value</td>";
			}
			else {
				/*
				$stdInfo = $ldiscipline->retrieveStudentInfoByID2($id);			
				list($ename, $cname, $cls, $clsNo) = $stdInfo[0];
				$displayName = ($intranet_session_language=="en") ? $ename : $cname;
				*/
				$stdInfo = $ldiscipline->getStudentNameByID($id);
				list($uid, $name, $cls, $clsNo) = $stdInfo[0];
				
				$tableContent .= "		<td class='tabletext'>$cls</td>";
				$tableContent .= "		<td class='tabletext'>$clsNo</td>";
				$tableContent .= "		<td class='tabletext'>$name</td>";
				$tableContent .= "		<td class='tabletext'>$value</td>";
			}
			$tableContent .= "	</tr>";
			
			# export content 
			if($listBy=='byClass') {
				$exportContent .= "&quot;".$ldiscipline->getClassNameByClassID($id)."&quot;\t";
				$exportContent .= "&quot;$value&quot;\n";
			}
			else {
				/*
				$stdInfo = $ldiscipline->retrieveStudentInfoByID2($id);			
				list($ename, $cname, $cls, $clsNo) = $stdInfo[0];
				$displayName = ($intranet_session_language=="en") ? $ename : $cname;
				*/
				$stdInfo = $ldiscipline->getStudentNameByID($id);
				list($uid, $name, $cls, $clsNo) = $stdInfo[0];
				$exportContent .= "&quot;$cls&quot;\t";
				$exportContent .= "&quot;$clsNo&quot;\t";
				$exportContent .= "&quot;$name&quot;\t";
				$exportContent .= "&quot;$value&quot;\n";
			}
			$i++;
		}
	}
	$tableContent .= "</table>";
}
else
{
	$optionString = "<td height=\"20\" class=\"tabletextremark\"><em>- $i_Discipline_Statistics_Options -</em></td>";
	//$initialString = "<script language=\"javascript\">jPeriod='YEAR';hideSpan('spanMisCat');showSpan('spanGoodCat');changeCat('0');document.getElementById('selectGoodCat').value='0';</script>\n";	
}
########################################################

$TAGS_OBJ[] = array($Lang['eDiscipline']['StudyScoreReport'],"");

# Menu highlight setting
$CurrentPage = "StudyScoreReport";
$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
//SelectAll(form1.elements['target[]']);
function changeType(form_obj,lvl_value)
{
         obj = form_obj.elements["target[]"]
         <? # Clear existing options
         ?>
         while (obj.options.length > 0)
         {
                obj.options[0] = null;
         }
         if (lvl_value==1)
         {
             <?
             for ($i=0; $i<sizeof($levels); $i++)
             {
                  list($id,$name) = $levels[$i];
                  ?>
                  obj.options[<?=$i?>] = new Option('<?=intranet_htmlspecialchars($name)?>',<?=$id?>);
                  <?

             }
             ?>
         }
         else
         {
             <?
             for ($i=0; $i<sizeof($classes); $i++)
             {
                  list($id,$name, $lvl_id) = $classes[$i];
                  ?>
                  obj.options[<?=$i?>] = new Option('<?=$name?>',<?=$id?>);
                  <?

             }
             ?>
         }
//	SelectAll(form1.elements['target[]']);

}
function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}

function doPrint()
{
	document.form1.action = "print.php";
	document.form1.target = "_blank";
	document.form1.submit();
	document.form1.action = "";
	document.form1.target = "";
}

function doExport()
{
	document.form1.action = "export.php";
	document.form1.submit();
	document.form1.action = "";
}
/*
function view()
{
	if (checkform())
	{
		document.form1.action = "";
		document.form1.submit();
	}
}
*/
function checkform()
{
	var obj = document.form1;
	var select_obj = obj.elements['target[]'];
	var total = 0;
	
	for (var i=0; i<select_obj.length; i++)
	{
		if (select_obj[i].selected)
		{
			total++;
		}
	}
	
	if(total==0) {
		alert('<?=$i_Discipline_System_alert_PleaseSelectClassLevel?>');
		return false;
	}
	
	//if(isNaN(obj.startMark.value) || obj.startMark.value=="" || obj.startMark.value<0 || isNaN(obj.endMark.value) || obj.endMark.value=="" || obj.endMark.value<0) 
	if(isNaN(obj.startMark.value) || obj.startMark.value=="" || isNaN(obj.endMark.value) || obj.endMark.value=="") 
	{
		alert("<?=$Lang['eDiscipline']['MissStudyScoreRange']?>");	
		return false;
	}
	return true;	
}

function showSpan(span) {
	document.getElementById(span).style.display="inline";
}

function hideSpan(span) {
	document.getElementById(span).style.display="none";
}

function showOption(){
	hideSpan('spanShowOption');
	showSpan('spanHideOption');
	showSpan('spanOptionContent');
	document.getElementById('tdOption').className = '';
}

function hideOption(){
	hideSpan('spanOptionContent');
	hideSpan('spanHideOption');
	showSpan('spanShowOption');
	document.getElementById('tdOption').className = 'report_show_option';
}
</script>
<script language="javascript">
var xmlHttp2

function changeTerm(val) {

	if (val.length==0)
	{ 
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
		
	xmlHttp2 = GetXmlHttpObject()
	
	if (xmlHttp2==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "../ajaxGetSemester.php";
	url += "?year=" + val;
	url += "&term=<?=$semester?>";
	url += "&field=semester";
	url += "&hideWholeYear=<?=$StudyScoreCalculationMethod?>";
	
	xmlHttp2.onreadystatechange = stateChanged2 
	xmlHttp2.open("GET",url,true)
	xmlHttp2.send(null)
} 

function stateChanged2() 
{ 
	if (xmlHttp2.readyState==4 || xmlHttp2.readyState=="complete")
	{ 
		document.getElementById("spanSemester").innerHTML = xmlHttp2.responseText;
		document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
	} 
}

var xmlHttp 

function showResult(str)
{
	if (str.length==0)
		{ 
			document.getElementById("rankTargetDetail").innerHTML = "";
			document.getElementById("rankTargetDetail").style.border = "0px";
			return
		}
		
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 

	var url = "";
		
	url = "../get_live2.php";
	url = url + "?target=" + str
	
	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById("rankTargetDetail").innerHTML = xmlHttp.responseText;
		document.getElementById("rankTargetDetail").style.border = "0px solid #A5ACB2";
	} 
}

var xmlHttp3

function changeClassForm(val) {
	if (val.length==0)
	{ 
		document.getElementById("spanTarget").innerHTML = "";
		document.getElementById("spanTarget").style.border = "0px";
		return
	}
		
	xmlHttp3 = GetXmlHttpObject()
	
	if (xmlHttp3==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 
	var url = "";
		
	url = "../ajaxChangeClass.php";
	url += "?year=" + val;
	url += "&selectedTarget="+document.getElementById('selectedTarget').value;
	url += "&level=" + document.getElementById('level').value;

	xmlHttp3.onreadystatechange = stateChanged3 
	xmlHttp3.open("GET",url,true)
	xmlHttp3.send(null)
	
} 

function stateChanged3() 
{ 
	if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete")
	{ 
		document.getElementById("spanTarget").innerHTML = xmlHttp3.responseText;
		document.getElementById("spanTarget").style.border = "0px solid #A5ACB2";
	} 
}

function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

</script>
<br/>
<form name="form1" action="" method="POST" onSubmit="return checkform()">
<table width="90%" border="0" cellspacing="0" cellpadding="3" align="center">
	<tr align="left" <? if($_POST["submit_flag"]=="YES") echo "class=\"report_show_option\""; ?>>
		<?=$optionString?>
	</tr>
</table>
<br>
<span id="spanOptionContent">
<table width="90%" border="0" cellpadding="4" cellspacing="0">
	<tr class="tabletext">
		<td width="20%" class="formfieldtitle" valign="top"><?=$iDiscipline['Period']?> <span class="tabletextrequire">*</span></td>
		<td>
			<table border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td height="30">
						<?=$i_Discipline_School_Year?>
						<?=$selectSchoolYearHTML?>&nbsp;
						<?=$i_Discipline_Semester?>
						<span id="spanSemester"></span>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr class="tabletext">
		<td class="formfieldtitle" valign="top"><?="$i_Discipline_Class/$i_Discipline_Form"?> <span class="tabletextrequire">*</span></td>
		<td>
			<SELECT name="level" id='level' onChange="changeType(this.form,this.value);changeClassForm(document.getElementById('selectYear').value)">
				<OPTION value="0" <?=$level!=1?"SELECTED":""?>><?=$i_Discipline_Class?></OPTION>
				<OPTION value="1" <?=$level==1?"SELECTED":""?>><?=$i_Discipline_Form?></OPTION>
			</select>
			<br><br>
			<span id='spanTarget'></span>
			<?= $linterface->GET_BTN($button_select_all, "button", "javascript:SelectAll(document.getElementById('target[]'))"); ?>
		</td>
	</tr>
	<tr class="tabletext">
		<td class="formfieldtitle" valign="top"><?=$i_Discipline_System_Subscore1?> <span class="tabletextrequire">*</span></td>
		<td>
			<input type="text" name="startMark" id="startMark" value="<?=$startMark?>" size="4" maxlength="5"> <?=$i_To?> <input type="text" name="endMark" id="endMark" value="<?=$endMark?>" size="4" maxlength="5">
		</td>
	</tr>
	<tr class="tabletext">
		<td class="formfieldtitle" valign="top"><?=$Lang['eDiscipline']['DisplayIn']?></td>
		<td><input name="listBy" type="radio" id="listBy[0]" value="byClass" <? if($listBy=="" || $listBy=="byClass") echo "checked";?>>
			<label for="listBy[0]"><?=$i_general_class?></label>
			<input name="listBy" type="radio" id="listBy[1]" value="byIndividual" <? if($listBy=="byIndividual") echo "checked";?>>
			<label for="listBy[1]"><?=$Lang['eDiscipline']['Individual']?></label>
		</td>
	</tr>
	<tr class="tabletext">
		<td class="formfieldtitle" valign="top"><?=$list_sortby?></td>
		<td>
			<input name="sortBy" type="radio" id="sortBy[0]" value="byMark"  <? if($sortBy=="" || $sortBy=="byMark") echo "checked";?>>
			<label for="sortBy[0]"><?=$i_Discipline_System_Subscore1?></label>
			<input name="sortBy" type="radio" id="sortBy[1]" value="byClass" <? if($sortBy=="byClass") echo "checked";?>>
			<label for="sortBy[1]"><?=$i_general_class?></label>
		</td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>
	<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<tr>
		<td align="center"><?= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Report, "submit")?>&nbsp;
		</td>
	</tr>
</table>
<br>
<input type="hidden" name="flag" value="1" />
<input type="hidden" name="selectedTarget" id="selectedTarget" value="<? if(sizeof($target)>0) { echo implode(',',$target); }?>">
</span>
<?=$initialString?>
<?
	if ($flag == 1)
	{
		?>
		<?=$tableContent?>
		<input type="hidden" name="content" id="content" value='<?=urlencode($tableContent)?>' />
		<input type="hidden" name="exportContent" value='<?=$exportContent?>' />

		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">

			<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
			<tr>
				<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_export, "button", "javascript:doExport()");?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_print, "button", "javascript:doPrint()");?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_back, "button", "javascript:self.location.href='index.php'");?>&nbsp;
				<div id="div_form"></div>
				</td>
			</tr>
		</table>

		</p>
		<?
	}
?>
<script language="javascript">
<?
if($flag==1) {
	echo "changeTerm('$selectYear');\n";
	echo "changeClassForm('$selectYear');\n";
} else {
	echo "changeTerm(document.getElementById('selectYear').value);\n";
	echo "changeClassForm(document.getElementById('selectYear').value);\n";
}
?>
changeClassForm('<?=$selectYear?>');
//SelectAll(document.getElementById('target[]'));
</script>
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>