<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_cust.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# user access right checking
$ldiscipline = new libdisciplinev12_cust();

//if(!$sys_custom['eDiscipline']['TKPClassDemeritRecord'] || !$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
if(!$sys_custom['eDiscipline']['TKPClassDemeritRecord'] || (!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-TKP_Class_Demerit_Record-View"))) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$ldiscipline_ui = new libdisciplinev12_ui_cust();

# POST data
$format = $_POST['format'];
$selectYear = $_POST['selectYear'];
$textFromDate = $_POST['textFromDate'];
$textToDate = $_POST['textToDate'];
$dateRangeStart = date("d/m/Y", strtotime($textFromDate));
$dateRangeEnd = date("d/m/Y", strtotime($textToDate));

# Semester
$semester = (array)getSemesters($selectYear);
$semester = array_keys($semester);

# Rank Target
$classIdCond = " AND yc.YearClassID IN ('".implode("','",(array)$_POST['rankTargetDetail'])."') ";

# Style
if($format == 'pdf')
{
	$StylePrefix = '<span class="tabletextrequire">';
	$StyleSuffix = '</span>';
}

// [2015-1029-1156-29207] get punishment name from settings in admin console
$merit_type_ary = array();
//$merit_type_ary[0] = "口頭警告";
//$merit_type_ary[-1] = "書面警告";
//$merit_type_ary[-2] = "紀缺點";
//$merit_type_ary[-3] = "小過";
$merit_type_ary[0] = $i_Merit_Warning;
$merit_type_ary[-1] = $i_Merit_BlackMark;
$merit_type_ary[-2] = $i_Merit_MinorDemerit;
$merit_type_ary[-3] = $i_Merit_MajorDemerit;
$merit_type_ary[-4] = $i_Merit_SuperDemerit;

# Get Target Students
$clsName = "yc.ClassTitleEN";	
$sql = "SELECT 
			iu.UserID,
			yc.YearClassID,
			yc.ClassTitleEN as ClassName,
			ycu.ClassNumber,
			CONCAT(
				IF($clsName IS NULL OR $clsName='' OR ycu.ClassNumber IS NULL OR ycu.ClassNumber='' Or iu.RecordStatus != 1, '".$StylePrefix."^".$StyleSuffix."', ''),
				iu.ChineseName
			) as StudentName
		FROM INTRANET_USER as iu 
		LEFT JOIN YEAR_CLASS_USER as ycu ON ycu.UserID = iu.UserID
		LEFT JOIN YEAR_CLASS as yc ON (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '$selectYear')
		LEFT JOIN YEAR as y ON y.YearID = yc.YearID
		WHERE yc.AcademicYearID = '".$selectYear."' $classIdCond 
		GROUP BY iu.UserID 
		ORDER BY y.Sequence, yc.Sequence, ycu.ClassNumber";
		
$students = $ldiscipline->returnResultSet($sql);
$student_ids = Get_Array_By_Key($students, "UserID");
$student_ary = BuildMultiKeyAssoc((array)$students, array("YearClassID","UserID"));
$student_count = count($students);

# Awards / Punishment - Approved and Released
$sql = "SELECT 
			r.RecordID,
			r.StudentID,
			m.ItemCode,
			m.ItemName,
			DATE_FORMAT(r.RecordDate,'%d/%m/%Y') as RecordDate,
			r.YearTermID,
			r.ProfileMeritType,
			ROUND(r.ProfileMeritCount) AS MeritRecordCount,
			r.Remark,
			r.PICID
		FROM 
			DISCIPLINE_MERIT_RECORD as r
		INNER JOIN 
			DISCIPLINE_MERIT_ITEM as m ON m.ItemID = r.ItemID
		WHERE 
			r.StudentID IN ('".implode("','", (array)$student_ids)."') AND r.MeritType = -1 AND 
			r.RecordStatus = '".DISCIPLINE_STATUS_APPROVED."' AND r.ReleaseStatus = '".DISCIPLINE_STATUS_RELEASED."' AND 
			r.AcademicYearID = '$selectYear' AND r.RecordDate >= '$textFromDate' AND r.RecordDate <= '$textToDate' 
		ORDER BY
			r.StudentID, r.RecordDate, r.RecordID";
		
$ap_records = $ldiscipline->returnResultSet($sql);
$ap_pics = Get_Array_By_Key($ap_records, "PICID");
$ap_records = BuildMultiKeyAssoc((array)$ap_records, array("StudentID","RecordID"));
$ap_pics = array_unique((array)$ap_pics);

# Get Target PICs
$sql = "SELECT 
			iu.UserID,
			CONCAT(
				IF(iu.RecordStatus != 1, '".$StylePrefix."^".$StyleSuffix."', ''),
				iu.ChineseName
			) as PICName
		FROM INTRANET_USER as iu 
		WHERE iu.UserID IN ('".implode("','", (array)$ap_pics)."') 
		GROUP BY iu.UserID";	
$ap_pics = $ldiscipline->returnResultSet($sql);
$ap_pics = BuildMultiKeyAssoc((array)$ap_pics, array("UserID"));

# Report Format - CSV
if($format == 'csv')
{
	# Initiate Export Object
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	$lexport = new libexporttext();
	$exportContent = '';
	
	# CSV Header
	$header = array("田家炳中學", "輔委會學生校內生活紀錄總表(".getAYNameByAyId($selectYear,"b5").")");
	$rows = array();
	
	$level_num = count((array)$_POST['rankTargetDetail']);
	if($level_num)
	{
		foreach((array)$_POST['rankTargetDetail'] as $yearclass_id)
		{
			$resultRow = $ldiscipline_ui->getTKPClassDemeritRecord($format, $yearclass_id, $student_ary, $ap_pics, $ap_records, ($level_count==$level_num-1));
			$rows = array_merge($rows,$resultRow);
		}
	}

	$exportContent .= $lexport->GET_EXPORT_TXT($rows, $header);
	$lexport->EXPORT_FILE("ClassDemeritRecord.csv", $exportContent);
}
# Report Format - PDF
else if($format == 'pdf')
{
	# Initiate mPDF Object
	require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");
	$pdf_obj = "";
		
	// Create mPDF object
	$pdf_obj = new mPDF('','A4',0,'',10,10,10,10,5,0);
	
	// mPDF Setting
	$pdf_obj->setAutoTopMargin = 'stretch';
	//split 1 table into 2 pages add border at the bottom
	$pdf_obj->splitTableBorderWidth = 0.1; 

	# Report Header
	$html = "<table class='report_header' cellspacing='0' cellpadding='0' border='0' width='100%'>";
		$html .= "<tr><td align='left'>違規班報表 (".$dateRangeStart."-".$dateRangeEnd.")</td></tr>";
	$html .= "</table>";
	$pdf_obj->DefHTMLHeaderByName("titleHeader", $html);
	$pdf_obj->SetHTMLHeaderByName("titleHeader");
	
	# CSS
	$pdf_obj->writeHTML("<head>");
	$html = "<style TYPE='text/css'>
				body 
				{
					font-family: msjh; 
					font-size: 9pt; 
					font-style: italic;
				}
				.report_header
				{
					font-size: 12pt;	
				}
				table.result_table th, table.result_table td 
				{
					text-align: center;
					padding: 1px;
					font-style: italic;
				}
				table.result_table th.result_table_header
				{
					border-top: 0.1mm solid black;
					border-bottom: 0.5mm solid black;
					font-weight: normal;
					font-size: 11pt;
					font-style: italic;
				}
				table.result_table td 
				{
					line-height: 6mm;
				}
			</style>";
	$pdf_obj->writeHTML($html);
	$pdf_obj->writeHTML("</head>");
	
	$level_count = 0;
	$level_num = count((array)$_POST['rankTargetDetail']);
	
	# Report Table
	$pdf_obj->writeHTML("<body>");
	if($level_num)
	{
		foreach((array)$_POST['rankTargetDetail'] as $yearclass_id)
		{
			$html = $ldiscipline_ui->getTKPClassDemeritRecord($format, $yearclass_id, $student_ary, $ap_pics, $ap_records, ($level_count==$level_num-1));
			$pdf_obj->writeHTML($html);
			
			$level_count++;
		}
	}
	$pdf_obj->writeHTML("</body>");
	
	# Export PDF file
	$pdf_obj->Output('ClassDemeritRecord.pdf', 'I');
} 
else 
{
	echo "Not support";
}

intranet_closedb();
?>
