<?php
// modifying by: yat
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_kyd.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12_kyd();
$lclass = new libclass();

/*
if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-MonthlyReport-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}
*/

$CurrentPageArr['eDisciplinev12'] = 1;

# menu highlight setting
$TAGS_OBJ[] = array($eDiscipline['MonthlyReport'], "");
$CurrentPage = "MonthlyReport";

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>

<?=$ldiscipline->display_monthly_report_form_ui();?>

<?
  $linterface->LAYOUT_STOP();
  intranet_closedb();
?>
