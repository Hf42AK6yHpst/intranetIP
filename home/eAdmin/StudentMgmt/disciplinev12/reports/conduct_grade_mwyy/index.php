<?php
# using: henry chow

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");



intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$sys_custom['eDiscipline']['Conduct_Grade']['MWYY']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

if ($year=="")
{
	$year = Get_Current_Academic_Year_ID();
}

$years = $ldiscipline->returnAllYearsSelectionArray();
$select_year = $linterface->GET_SELECTION_BOX($years, "name='year' id='year' onChange='changeTerm(this.value);changeClassForm(this.value)'", "", $year);

$TAGS_OBJ[] = array($Lang['eDiscipline']['MWYY']['ConductGradeReport']);

# menu highlight setting
$CurrentPage = "Reports_Conduct_Grade_mwyy";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

$hideWholeYear = ($sys_custom['tsk_disciplinev12_class_summary'] || $ConductMarkCalculationMethod!=0) ? 1 : 0;

?>

<script language="javascript">


function doExport()
{
	document.form1.action = "export.php";
	document.form1.submit();
	document.form1.action = "";
}

var xmlHttp2

function changeTerm(val) {

	if (val.length==0)
	{ 
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
		
	xmlHttp2 = GetXmlHttpObject()
	
	if (xmlHttp2==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "../ajaxGetSemester.php";
	url += "?year=" + val;
	url += "&term=<?=$semester?>";
	url += "&field=semester";
	url += "&hideWholeYear=<?=$hideWholeYear?>";
	
	xmlHttp2.onreadystatechange = stateChanged2 
	xmlHttp2.open("GET",url,true)
	xmlHttp2.send(null)
} 

function stateChanged2() 
{ 
	if (xmlHttp2.readyState==4 || xmlHttp2.readyState=="complete")
	{ 
		document.getElementById("spanSemester").innerHTML = xmlHttp2.responseText;
		document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
	} 
}

var xmlHttp

function showResult(str)
{
	if (str.length==0)
		{ 
			document.getElementById("rankTargetDetail").innerHTML = "";
			document.getElementById("rankTargetDetail").style.border = "0px";
			return
		}
		
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 

	var url = "";
		
	url = "../get_live2.php";
	url = url + "?target=" + str
	
	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById("rankTargetDetail").innerHTML = xmlHttp.responseText;
		document.getElementById("rankTargetDetail").style.border = "0px solid #A5ACB2";
	} 
}

var xmlHttp3

function changeClassForm(val) {
	if (val.length==0)
	{ 
		document.getElementById("spanTarget").innerHTML = "";
		document.getElementById("spanTarget").style.border = "0px";
		return
	}
		
	xmlHttp3 = GetXmlHttpObject()
	
	if (xmlHttp3==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 
	var url = "";
		
	url = "../ajaxChangeClass.php";
	url += "?year=" + val;
	url += "&single=1&withWholeForm=1";
	//url += "&selectedTarget=322";

	xmlHttp3.onreadystatechange = stateChanged3 
	xmlHttp3.open("GET",url,true)
	xmlHttp3.send(null)
	
	
} 

function stateChanged3() 
{ 
	if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete")
	{ 
		document.getElementById("spanTarget").innerHTML = xmlHttp3.responseText;
		document.getElementById("spanTarget").style.border = "0px solid #A5ACB2";
	} 
}

function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

</script>
<br/>
<form name="form1" action="" method="POST">
<table class="form_table_v30">

<tr>
	<td width="30%" class="field_title" valign="top"><?=$Lang['General']['SchoolYear']?> <span class="tabletextrequire">*</span></td>
	<td class="tabletext">
		
		<?=$select_year?> 
		
		<span id="spanSemester"><?=$select_sem?></span>
	</td>
</tr>


<tr>
	<td class="field_title" valign="top"><?=$i_ClassName?> <span class="tabletextrequire">*</span></td>
	<td><span id='spanTarget'></span>
	</td>
</tr>

</table>

<table width="100%" border="0" cellpadding="5" cellspacing="0">
	<tr><td align="left" class="tabletextremark"><?=$i_general_required_field?></td></tr>
	<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<tr>
		<td align="center"><?= $linterface->GET_ACTION_BTN($Lang['Btn']['Export'], "button", "javascript:doExport()")?>&nbsp;
		</td>
	</tr>
</table>
<br>

<script language="javascript">
changeTerm('<?=$year?>');
changeClassForm('<?=$year?>');
</script>
</form>

<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>
