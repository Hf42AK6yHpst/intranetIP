<?php
# using: 
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lsp = new libstudentprofile();

/*
if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}
*/

# Year Menu 
if ($selectYear=="") {
	$selectYear = Get_Current_Academic_Year_ID();
}

$years = $ldiscipline->returnAllYearsSelectionArray();
$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='selectYear' id='selectYear' onclick='changeRadioSelection(\"YEAR\")' onChange='changeTerm(this.value); changeClassForm(this.value)'", "", $selectYear);

$amount = ($amount=='') ? 1 : $amount;
$amountMenu = "<select name='amount' id='amount'>";
for($i=1; $i<=60; $i++) {
	$amountMenu .= "<option value='$i'";
	$amountMenu .= ($i==$amount) ? " selected" : "";
	$amountMenu .= ">$i</option>";	
}
$amountMenu .= "</select>";


########################################################

if($flag == 1) {
	$studentIDAry = array();
	foreach($target as $val) {
		if($level == "0") {	# class
			$tempStudentAry = $ldiscipline->storeStudent(0,'::'.$val);	
		} else {
			$tempStudentAry = $ldiscipline->storeStudent(0,$val);	
		}
		if(sizeof($tempStudentAry)>0) $studentIDAry = array_merge($studentIDAry, $tempStudentAry);
	}
	
	$exportContent = "";
	
	$rawDataAry = array();
	$rawDataAry['PeriodChoice'] = $Period;
	if($Period=="YEAR") {
		$rawDataAry['date1'] = $selectYear;
		$rawDataAry['date2'] = $semester;
	} else {
		$rawDataAry['date1'] = $startDate;
		$rawDataAry['date2'] = $endDate;
	}
	$rawDataAry['show_waive_status'] = $show_waive_status;
	
	$tableContent = $ldiscipline->getMisconductReport($studentIDAry, $rawDataAry);
	
	
}
########################################################

$TAGS_OBJ[] = array($Lang['eDiscipline']['WSCSS_MisconductReport'],"");

# menu highlight setting
$CurrentPage = "WSCSS_MisconductReport";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

?>

<script language="javascript">
function changeType(form_obj,lvl_value)
{
         obj = form_obj.elements["target[]"]
         <? # Clear existing options
         ?>
         while (obj.options.length > 0)
         {
                obj.options[0] = null;
         }
         if (lvl_value==1)
         {
             <?
             for ($i=0; $i<sizeof($levels); $i++)
             {
                  list($id,$name) = $levels[$i];
                  ?>
                  obj.options[<?=$i?>] = new Option('<?=intranet_htmlspecialchars($name)?>',<?=$id?>);
                  <?

             }
             ?>
         }
         else
         {
             <?
             for ($i=0; $i<sizeof($classes); $i++)
             {
                  list($id,$name, $lvl_id) = $classes[$i];
                  ?>
                  obj.options[<?=$i?>] = new Option('<?=$name?>',<?=$id?>);
                  <?

             }
             ?>
         }
//	SelectAll(form1.elements['target[]']);

}
function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}

function changeRadioSelection(type)
{
	var jsYearRadioObj = document.getElementById("radioPeriod_Year");
	var jsDateRadioObj = document.getElementById("radioPeriod_Date");
	
	if (type == "YEAR")
	{
		jsYearRadioObj.checked = true;
		jsDateRadioObj.checked = false;
		jPeriod = type;
	}
	else
	{
		jsYearRadioObj.checked = false;
		jsDateRadioObj.checked = true;
		jPeriod = type;
	}
}

function doExport()
{
	document.form1.action = "export.php";
	document.form1.submit();
	document.form1.action = "";
}
function doPrint()
{
	document.form1.action = "print.php";
	document.form1.target = "_blank";
	document.form1.submit();
	document.form1.action = "";
	document.form1.target = "";
}
function view()
{
	if (checkform())
	{
		document.form1.action = "";
		document.form1.submit();
	}
}

var jPeriod = "";
function checkform()
{
	var select_obj = document.getElementById('target[]');

	if (jPeriod=='DATE' && document.getElementById('startDate').value=='') {
		alert('<?=$i_alert_pleasefillin.$i_general_startdate?>');
		return false;
	}
	if (jPeriod=='DATE' && document.getElementById('endDate').value=='') {
		alert('<?=$i_alert_pleasefillin.$i_general_enddate?>');
		return false;
	}
	if(jPeriod=='DATE' && !check_date(document.getElementById('startDate'), '<?=$i_invalid_date?>')) {
		return false;
	}
	if(jPeriod=='DATE' && !check_date(document.getElementById('endDate'), '<?=$i_invalid_date?>')) {
		return false;
	}
	if(jPeriod=='DATE' && (document.form1.startDate.value > document.form1.endDate.value)) {
		alert("<?=$i_Discipline_System_Reports_Invalid_Date_Compare?>");
		return false;	
	}
	
	for (var i=0; i<select_obj.length; i++)
	{
		if (select_obj[i].selected)
		{
			return true;
		}
	}

	alert('<?=$i_Discipline_System_alert_PleaseSelectClassLevel?>');
	return false;
}
</script>
<script language="javascript">
var xmlHttp2

function changeTerm(val) {

	if (val.length==0)
	{ 
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
		
	xmlHttp2 = GetXmlHttpObject()
	
	if (xmlHttp2==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "../../ajaxGetSemester.php";
	url += "?year=" + val;
	url += "&term=<?=$semester?>";
	url += "&field=semester";
	
	xmlHttp2.onreadystatechange = stateChanged2 
	xmlHttp2.open("GET",url,true)
	xmlHttp2.send(null)
} 

function stateChanged2() 
{ 
	if (xmlHttp2.readyState==4 || xmlHttp2.readyState=="complete")
	{ 
		document.getElementById("spanSemester").innerHTML = xmlHttp2.responseText;
		document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
	} 
}

var xmlHttp

function showResult(str)
{
	if (str.length==0)
		{ 
			document.getElementById("rankTargetDetail").innerHTML = "";
			document.getElementById("rankTargetDetail").style.border = "0px";
			return
		}
		
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 

	var url = "";
		
	url = "../../get_live2.php";
	url = url + "?target=" + str
	
	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById("rankTargetDetail").innerHTML = xmlHttp.responseText;
		document.getElementById("rankTargetDetail").style.border = "0px solid #A5ACB2";
	} 
}

var xmlHttp3

function changeClassForm(val) {
	if (val.length==0)
	{ 
		document.getElementById("spanTarget").innerHTML = "";
		document.getElementById("spanTarget").style.border = "0px";
		return
	}
		
	xmlHttp3 = GetXmlHttpObject()
	
	if (xmlHttp3==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 
	var url = "";
		
	url = "../../ajaxChangeClass.php";
	url += "?year=" + val;
	url += "&selectedTarget="+document.getElementById('selectedTarget').value;
	url += "&level=" + document.getElementById('level').value;

	xmlHttp3.onreadystatechange = stateChanged3 
	xmlHttp3.open("GET",url,true)
	xmlHttp3.send(null)
	
} 

function stateChanged3() 
{ 
	if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete")
	{ 
		document.getElementById("spanTarget").innerHTML = xmlHttp3.responseText;
		document.getElementById("spanTarget").style.border = "0px solid #A5ACB2";
	} 
}

function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

</script>
<br/>
<form name="form1" action="" method="POST">
<table width="90%" border="0" cellpadding="4" cellspacing="0">
	<tr class="tabletext">
		<td width="20%" class="formfieldtitle" valign="top"><?=$iDiscipline['Period']?> <span class="tabletextrequire">*</span></td>
		<td>
			<table border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td height="30" onClick="changeRadioSelection('YEAR')">
						<input name="Period" type="radio" id="radioPeriod_Year" value="YEAR" onClick="javascript:jPeriod=this.value"<? echo ($Period!="DATE")?" checked":"" ?>>
						<?=$i_Discipline_School_Year?>
						<?=$selectSchoolYearHTML?>&nbsp;
						<?=$i_Discipline_Semester?>
						<span id="spanSemester"></span>
					</td>
				</tr>
				<tr>
					<td onClick="changeRadioSelection('DATE')" onFocus="changeRadioSelection('DATE')">
						<input name="Period" type="radio" id="radioPeriod_Date" value="DATE" onClick="javascript:jPeriod=this.value"<? echo ($Period=="DATE")?" checked":"" ?>> <?=$i_From?> 
						<?=$linterface->GET_DATE_PICKER("startDate",$startDate)?>&nbsp;
						 <?=$i_To?> 
						<?=$linterface->GET_DATE_PICKER("endDate",$endDate)?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr class="tabletext">
		<td class="formfieldtitle" valign="top"><?="$i_Discipline_Class/$i_Discipline_Form"?></td>
		<td>
			<SELECT name="level" id='level' onChange="changeType(this.form,this.value);changeClassForm(document.getElementById('selectYear').value)">
				<OPTION value="0" <?=$level!=1?"SELECTED":""?>><?=$i_Discipline_Class?></OPTION>
				<OPTION value="1" <?=$level==1?"SELECTED":""?>><?=$i_Discipline_Form?></OPTION>
			</select>
			<br><br>
			<span id='spanTarget'></span>
			<?= $linterface->GET_BTN($button_select_all, "button", "javascript:SelectAll(document.getElementById('target[]'))"); ?>
		</td>
	</tr>
	<tr class="tabletext">
		<td class="formfieldtitle" valign="top"><?=$i_Discipline_System_Show_WaiveStatus?></td>
		<td>
			<input type="radio" name="show_waive_status" id="show_waive_yes" value="1" <?=($show_waive_status==1 || $show_waive_status=='')?" checked" : ""?>><label for="show_waive_yes"><?=$i_general_yes?></label>
			<input type="radio" name="show_waive_status" id="show_waive_no" value="0" <?=($show_waive_status==0)?" checked" : ""?>><label for="show_waive_no"><?=$i_general_no?></label>
		</td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>
	<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<tr>
		<td align="center"><?= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Report, "button", "javascript:view()")?>&nbsp;
		</td>
	</tr>
</table>
<br>
<input type="hidden" name="flag" value="1" />
<input type="hidden" name="targetDivID" id="targetDivID" />
<input type="hidden" name="ajaxStudentID" id="ajaxStudentID" />
<input type="hidden" name="ajaxPeriod" id="ajaxPeriod" />
<input type="hidden" name="ajaxDate1" id="ajaxDate1" />
<input type="hidden" name="ajaxDate2" id="ajaxDate2" />
<input type="hidden" name="ajaxLevel" id="ajaxLevel" />
<input type="hidden" name="ajaxTarget" id="ajaxTarget" />
<input type="hidden" name="ajaxRecordType" id="ajaxRecordType" />
<input type="hidden" name="ajaxType" id="ajaxType" />
<input type="hidden" name="selectedTarget" id="selectedTarget" value="<? if(sizeof($target)>0) { echo implode(',',$target); }?>">
<input type="hidden" name="title" value="<?=urlencode($Lang['eDiscipline']['WSCSS_MisconductReport'])?>" />
<input type="hidden" name="content" value='<?=urlencode($tableContent)?>' />
<?
	if ($flag == 1)
	{
		?>
		<?=$tableContent?>

		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">

			<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
			<tr>
				<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_export, "button", "javascript:doExport()");?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_print, "button", "javascript:doPrint()");?>
				<div id="div_form"></div>
				</td>
			</tr>
		</table>

		</p>
		<?
	}
?>
<script language="javascript">
<?
if($flag==1) {
	echo "changeTerm('$selectYear');\n";
	echo "changeClassForm('$selectYear');\n";
} else {
	echo "changeTerm(document.getElementById('selectYear').value);\n";
	echo "changeClassForm(document.getElementById('selectYear').value);\n";
}
?>
changeClassForm('<?=$year?>');
</script>
</form>

<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>
