<?php
// Modifying by : 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/flashchart_basic/php-ofc-library/open-flash-chart.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$ldiscipline->CONTROL_ACCESS("Discipline-STAT-GoodConduct_Misconduct-View");

$CurrentPage = "MisconductReport";
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($eDiscipline['MisconductReport'], "", 1);

$lclass = new libclass();
$linterface = new interface_html();

// School year
$selectYear = ($selectYear == '') ? getCurrentAcademicYear() : $selectYear;
$selectSchoolYearHTML = "<select name=\"selectYear\" onclick=\"changeRadioSelection('YEAR')\">";
$selectSchoolYearHTML .= "<option value=\"$i_Discipline_System_Award_Punishment_All_School_Year\"";
$selectSchoolYearHTML .= ($selectYear!=$i_Discipline_System_Award_Punishment_All_School_Year) ? "" : " selected";
$selectSchoolYearHTML .= ">".$i_Discipline_System_Award_Punishment_All_School_Year."</option>";
$selectSchoolYearHTML .= $ldiscipline->getConductSchoolYear($selectYear);
$selectSchoolYearHTML .= "</select>";

// Semester
$semester_data = split("\n",get_file_content("$intranet_root/file/semester.txt"));
$currentSemester = getCurrentSemester();
$selectSemesterHTML = "<select name=\"selectSemester\" onclick=\"changeRadioSelection('YEAR')\">";
$selectSemesterHTML .= "<option value=\"$i_Discipline_System_Award_Punishment_Whole_Year\"";
$selectSemesterHTML .= ($selectSemester!=$i_Discipline_System_Award_Punishment_Whole_Year) ? "" : " selected";
$selectSemesterHTML .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";
for ($i=0; $i<sizeof($semester_data); $i++) {
	$target_sem = $semester_data[$i];
	$line = split("::",$target_sem);
	list ($name,$current) = $line;
	$selectSemesterHTML .= "<option value=\"".$name."\"";
	if ($selectSemester == "") {		// first time
		if($name==$currentSemester) { $selectSemesterHTML .= " selected"; }
	} else {		// submit from form
		if($name==$selectSemester) { $selectSemesterHTML .= " selected"; }
	}
	$selectSemesterHTML .= ">".$name."</option>";
}
$selectSemesterHTML .= "</select>";

// Form
$ClassLvlArr = $lclass->getLevelArray();
$SelectedFormArr = explode(",", $SelectedForm);
$SelectedFormTextArr = explode(",", $SelectedFormText);
$selectFormHTML = "<select name=\"selectForm[]\" multiple size=\"5\">\n";
for ($i = 0; $i < sizeof($ClassLvlArr); $i++) {
	$selectFormHTML .= "<option value=\"".$ClassLvlArr[$i][0]."\"";
	if ($_POST["submit_flag"] != "YES" || in_array($ClassLvlArr[$i][0], $SelectedFormArr)) {
		$selectFormHTML .= " selected";
	}
	$selectFormHTML .= ">".$ClassLvlArr[$i][1]."</option>\n";
}
$selectFormHTML .= "</select>\n";

// Class
$ClassListArr = $lclass->getClassList();
$SelectedClassArr = explode(",", $SelectedClass);
$SelectedClassTextArr = explode(",", $SelectedClassText);
$selectClassHTML = "<select name=\"selectClass[]\" multiple size=\"5\">\n";
for ($i = 0; $i < sizeof($ClassListArr); $i++) {
	$selectClassHTML .= "<option value=\"".$ClassListArr[$i][0]."\"";
	if ($_POST["submit_flag"] != "YES" || in_array($ClassListArr[$i][0], $SelectedClassArr)) {
		$selectClassHTML .= " selected";
	}
	$selectClassHTML .= ">".$ClassListArr[$i][1]."</option>\n";
}
$selectClassHTML .= "</select>\n";

// Get Category selection
$good_cats = $ldiscipline->RETRIEVE_CONDUCT_CATEGORY(1);
$mis_cats = $ldiscipline->RETRIEVE_CONDUCT_CATEGORY(-1);
//$items = $ldiscipline->RETRIEVE_CONDUCT_ITEM_GROUPBY_CAT('',0);		# "Homework" subjects not included
$items = $ldiscipline->RETRIEVE_CONDUCT_ITEM_GROUPBY_CAT('',1);
$good_items = $ldiscipline->RETRIEVE_CONDUCT_ITEM_GROUPBY_CAT(1,0);
//$mis_items = $ldiscipline->RETRIEVE_CONDUCT_ITEM_GROUPBY_CAT(-1,0);
$mis_items = $ldiscipline->RETRIEVE_CONDUCT_ITEM_GROUPBY_CAT(-1,1);


$all_item = array();

$categoryID = ($record_type==1) ? $selectGoodCat : $selectMisCat;

if($categoryID==1) {	 # "Late"
	$all_item[0] = 0;	
} else {
	$all_item = $ldiscipline->RETRIEVE_ALL_CONDUCT_ITEM($categoryID,'id', $record_type);
}

// Good Conduct Category
$selectGoodCatHTML = "<select name=\"selectGoodCat\" onChange=\"changeCat(this.value);\">\n";
$selectGoodCatHTML .= "<option value=\"0\"";
$selectGoodCatHTML .= ($_POST["submit_flag"] != "YES")?" selected":"";
$selectGoodCatHTML .= ">-- ".$i_Discipline_System_Reports_All_Good_Conducts." --</option>";
for ($i = 0; $i < sizeof($good_cats); $i++) {
	$selectGoodCatHTML .= "<option value=\"".$good_cats[$i][0]."\"";
	if ($selectGoodCat == $good_cats[$i][0]) {
		$selectGoodCatHTML .= " selected";
	}
	$selectGoodCatHTML .= ">".$good_cats[$i][1]."</option>\n";
}
$selectGoodCatHTML .= "</select>\n";

// Misconduct Category
$selectMisCatHTML = "<select name=\"selectMisCat\" onChange=\"changeCat(this.value);\">\n";
$selectMisCatHTML .= "<option value=\"0\"";
$selectMisCatHTML .= ($_POST["submit_flag"] != "YES")?" selected":"";
//$selectMisCatHTML .= ">-- ".$eDiscipline["SelectRecordCategory"]." --</option>";
$selectMisCatHTML .= ">-- $i_Discipline_System_Reports_All_Misconduct --</option>";
for ($i = 0; $i < sizeof($mis_cats); $i++) {
	$selectMisCatHTML .= "<option value=\"".$mis_cats[$i][0]."\"";
	if ($selectMisCat == $mis_cats[$i][0]) {
		$selectMisCatHTML .= " selected";
	}
	$selectMisCatHTML .= ">".$mis_cats[$i][1]."</option>\n";
}
$selectMisCatHTML .= "</select>\n";

// Items
if($record_type==1) {
	$selectedItem = $good_items;	
} else {
	$selectedItem = $mis_items;	
}

$SelectedItemIDArr = explode(",", $SelectedItemID);
$SelectedItemIDTextArr = explode(",", $SelectedItemIDText);


$selectItemIDHTML = "<select name=\"ItemID[]\" multiple size=\"5\">\n";
if ($_POST["submit_flag"] == "YES") {
	for ($i = 0; $i < sizeof($selectedItem); $i++) {
		list($r_catID, $r_itemID, $r_itemName) = $selectedItem[$i];
		//$r_itemName = intranet_undo_htmlspecialchars($r_itemName);
		if (($record_type == 1 && $selectGoodCat == $r_catID) || ($record_type == -1 && $selectMisCat == $r_catID) || ($selectGoodCat==0 && $selectMisCat==0)) {
			$selectItemIDHTML .= "<option value=\"".$r_itemID."\"";
			//if (in_array($r_itemID, $SelectedItemIDArr)) {	# use ItemID to check selected fields
			if (in_array(addslashes(intranet_undo_htmlspecialchars($r_itemName)), $SelectedItemIDTextArr) && in_array(addslashes(intranet_undo_htmlspecialchars($r_itemID)), $SelectedItemIDArr)) {
				$selectItemIDHTML .= " selected";
			}
			$selectItemIDHTML .= ">".$r_itemName."</option>\n";
		}
	}
}
$selectItemIDHTML .= "</select>\n";


// Submit from form
if ($_POST["submit_flag"] == "YES") {
	$optionString = "<td id=\"tdOption\" class=\"tabletextremark\"><span id=\"spanShowOption\"><a href=\"javascript:showOption();\" class=\"contenttool\"><img src=\"$image_path/$LAYOUT_SKIN/icon_show_option.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">$i_Discipline_Show_Statistics_Option</a></span>";
	$optionString .= "<span id=\"spanHideOption\" style=\"display:none\"><a href=\"javascript:hideOption();\" class=\"contenttool\"><img src=\"$image_path/$LAYOUT_SKIN/icon_hide_option.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">$i_Discipline_Hide_Statistics_Option</a></span></td>";

	if ($Period == "YEAR") {
		$parPeriodField1 = $selectYear;
		$parPeriodField2 = $selectSemester;
	} else {
		$parPeriodField1 = $textFromDate;
		$parPeriodField2 = $textToDate;
	}

	if ($selectBy == "FORM") {
		$parByFieldArr = $SelectedFormTextArr;
	} else {
		$parByFieldArr = $SelectedClassTextArr;
	}

	if($categoryID==1) {	# "Late"
		$parStatsFieldArr[0] = 0;
		$parStatsFieldTextArr[0] = 'Late';
	} else if($categoryID==2) {	# "Homework not submitted"
		
		$parStatsFieldArr = $SelectedItemIDArr;
		$parStatsFieldTextArr = $SelectedItemIDTextArr;
		
	} else {
		$parStatsFieldArr = $SelectedItemIDArr;
		$parStatsFieldTextArr = $SelectedItemIDTextArr;
	}

	if($categoryID!=1 && $categoryID!=2) { 	
		$all_item_name = $ldiscipline->RETRIEVE_ALL_CONDUCT_ITEM($categoryID, 'name', $record_type);
	}
	
	/*
	$meritDemeritValue = $ldiscipline->retrieveGMItemSettings($record_type);
	debug_r($meritDemeritValue);
	*/
	
	### Table Data ###
	$data = $ldiscipline->retrieveGoodMisReport($Period, $parPeriodField1, $parPeriodField2, $selectBy, $parByFieldArr, $StatType, $parStatsFieldArr, $parStatsFieldTextArr, $categoryID);
	
# chart properties # 
	if ($Period == "YEAR") {
		$titleString = $selectYear." ".$selectSemester." ";
	} else {
		$titleString = $i_From." ".$textFromDate." ".$i_To." ".$textToDate." ";
	}
	$titleString .= $eDiscipline['Good_Conduct_and_Misconduct_Statistics'];
	$title = new title( Big5ToUnicode($titleString) );
	$title->set_style( "{font-size: 16px; font-family: _sans; color: #333333; font-weight:bold; text-align:center;}" );
	
	$x_legend = new x_legend( 'class' );
	$x_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );
	
	$x = new x_axis();
	$x->set_stroke( 2 );
	$x->set_tick_height( 2 );
	$x->set_colour( '#999999' );
	$x->set_grid_colour( '#CCCCCC' );
	$x->set_labels_from_array($parByFieldArr);
	
	$y_legend = new y_legend( Big5ToUnicode($i_Discipline_Quantity) );
	$y_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );
	
	$y = new y_axis();
	$y->set_stroke( 2 );
	$y->set_tick_length( 2 );
	$y->set_colour( '#999999' );
	$y->set_grid_colour( '#CCCCCC' );
	$y->set_range( 0, 100, 5 );
	$y->set_offset(true);	# true / false
	
	$tooltip = new tooltip();
	$tooltip->set_hover();
	
	########
	$colourArr = array('#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414');
	
	$chart = new open_flash_chart();
	
	$chart->set_tooltip($tooltip);
	
	$key = new key_legend();
	$key->set_selectable(true);
	
	########
	
	$chart->set_bg_colour( '#FFFFFF' );
	$chart->set_title( $title );
	$chart->set_x_legend( $x_legend );
	$chart->set_x_axis( $x );
	$chart->set_y_legend( $y_legend );
	$chart->set_y_axis( $y );
	$chart->add_element( $bar0 );
	$chart->add_element( $bar1 );
	$chart->add_element( $bar2 );
	$chart->add_element( $bar3 );
	$chart->add_element( $bar4 );
	$chart->add_element( $bar5 );
	$chart->set_tooltip( $tooltip );
	$chart->set_key_legend( $key );
	
	
	################################################
	############ Report Table Content ##############
	
	if($record_type == 1) {
		$i_MeritDemerit[1] = $i_Merit_Merit;
		$i_MeritDemerit[2] = $i_Merit_MinorCredit;
		$i_MeritDemerit[3] = $i_Merit_MajorCredit;
		$i_MeritDemerit[4] = $i_Merit_SuperCredit;
		$i_MeritDemerit[5] = $i_Merit_UltraCredit;
	} else if($record_type == -1) {
		$i_MeritDemerit[0] = $i_Merit_Warning;
		$i_MeritDemerit[-1] = $i_Merit_BlackMark;
		$i_MeritDemerit[-2] = $i_Merit_MinorDemerit;
		$i_MeritDemerit[-3] = $i_Merit_MajorDemerit;
		$i_MeritDemerit[-4] = $i_Merit_SuperDemerit;
		$i_MeritDemerit[-5] = $i_Merit_UltraDemerit;
	}
		
	$itemTotal = array();
	$columnMeritDemeritCount = array();

	$detail_table =  "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">\n";
	$detail_table .= "<tr class=\"tabletext\">";
	$detail_table .= "<td align=\"center\" colspan=\"".(sizeof($parStatsFieldTextArr)+sizeof($i_MeritDemerit)+2)."\">";	
	$detail_table .= $eDiscipline['MisconductReport'];
	$detail_table .= "</td></tr>";	
	$detail_table .= "<tr class=\"tabletext\">";
	$detail_table .= "<td align=\"right\" colspan=\"".(sizeof($parStatsFieldTextArr)+sizeof($i_MeritDemerit)+2)."\">";	
	if($Period=="YEAR") {
		$detail_table .= $parPeriodField1." ".$parPeriodField2."\n\n";
	} else {
		$detail_table .= $iDiscipline['Period_Start']." ".$parPeriodField1." ".$iDiscipline['Period_End']." ".$parPeriodField2."\n\n";
	}
	$generationDate = getdate();
	$detail_table .= "<br>".$eDiscipline['ReportGeneratedDate']." ".$generationDate['year']."/".$generationDate['mon']."/".$generationDate['wday']." ".$generationDate['hours'].":".$generationDate['minutes'].":".$generationDate['seconds'];
	$detail_table .= "</td></tr>";	
	$detail_table .= "<tr class=\"tablebluetop\">";
	if($selectBy=="FORM") {
		//$detail_table .= "<td class=\"tabletop\">".$i_Discipline_Form."</td>";	
		$detail_table .= "<td class=\"tabletop\">".$i_Discipline_Class."</td>";	
	} else {
		$detail_table .= "<td class=\"tabletop\">".$i_general_name."</td>";	
		$detail_table .= "<td class=\"tabletop\">".$i_Discipline_Class."</td>";	
	}
	# header of report table
	for($i=0; $i<sizeof($parStatsFieldTextArr); $i++) {
		$css = (sizeof($parStatsFieldTextArr)==$i+1) ? "seperator_right" : "";
		$detail_table .= "<td class=\"tablebluetop $css tabletoplink\" align=\"center\">".intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$i]))."</td>";	
	}
//	$detail_table .= "<td width=\"1\"><img src=\"{$image_path}/{$LAYOUT_SKIN}\"/10x10.gif\" width=\"100%\" height=\"1\"></td>";
	foreach($i_MeritDemerit as $tempMeritDemerit) {
		$detail_table .= "<td align=\"center\" class=\"tablegreentop tabletopnolink\">".$tempMeritDemerit."</td>";	
	}
	$detail_table .= "</tr>";
	
	#content of report table
	$p = 0;
	
		if ($selectBy == "FORM") {
			$sql = "SELECT CLS.ClassName FROM INTRANET_CLASS CLS LEFT OUTER JOIN INTRANET_CLASSLEVEL LVL ON (CLS.ClassLevelID=LVL.ClassLevelID) WHERE LVL.LevelName IN ('".implode('\',\'',$parByFieldArr)."')";
			$temp = $ldiscipline->returnVector($sql);
			$parByFieldArr = $temp;
		}

	
	for($i=0; $i<sizeof($parByFieldArr); $i++) {						# for each class/form
		if($selectBy=="FORM") {
			
			$css = ($i%2) ? "2" : "1";
			$detail_table .= "<tr class=\"tablerow$css tabletext\">";
			
			$detail_table .= "<td class=\"tablerow$css tabletext\">".$parByFieldArr[$i]."</td>";
			
			for($j=0; $j<sizeof($parStatsFieldTextArr); $j++) {			# for each item
				$css = ($i%2==0) ? "" : "2";
				$css2 = (sizeof($parStatsFieldTextArr)==$j+1) ? "seperator_right" : "seperator_left";
				$detail_table .= (isset($data[intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$parByFieldArr[$i]])) ? "<td align=\"center\" class=\"tabletext tablerow_total".$css." ".$css2."\">".$data[intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$parByFieldArr[$i]]."</td>" : "<td align=\"center\" class=\"tabletext tablerow_total".$css." ".$css2."\">0</td>";
				$itemTotal[intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))] += $data[intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$parByFieldArr[$i]];
			}
//			$detail_table .= "<td width=\"1\"><img src=\"{$image_path}/{$LAYOUT_SKIN}\"/10x10.gif\" width=\"1\" height=\"1\"></td>";
																				# (YEAR/DATE , Period1, Period2, Class/Form, ClassName/FormName, SelectedItemID, SelectedItemName)
			$meritDemeritCount = $ldiscipline->retrieveMeritDemeritCountFromGM($Period, $parPeriodField1, $parPeriodField2, $selectBy, $parByFieldArr[$i], $parStatsFieldArr, $parStatsFieldTextArr, $categoryID);

				if($record_type==1) {
					$m = 1;
				} else {
					$m = 0;
				}
				foreach($i_MeritDemerit as $columnNo) {
					$tempCount = 0;
					for($n=0; $n<sizeof($meritDemeritCount); $n++) {
						if($meritDemeritCount[$n]['ProfileMeritType']==$m)
							$tempCount += $meritDemeritCount[$n]['countTotal'];
					}
					$detail_table .= "<td align=\"center\">";
					$detail_table .= ($tempCount != 0) ? $tempCount : "0";
					$columnMeritDemeritCount[$m] += $tempCount;
					$detail_table .= "</td>";
					
					if($record_type==1) {
						$m++;
					} else {
						$m--;
					}
				}
				
			$detail_table .= "</tr>";
			
		} else {
			
			$stdInfo = $ldiscipline->retrieveNameClassNoByClassName($parByFieldArr[$i]);
			
			for($k=0; $k<sizeof($stdInfo); $k++) {
				$css = ($p%2) ? "2" : "1";
				$detail_table .= "<tr class=\"tablerow$css tabletext\">";
				$detail_table .= "<td class=\"tablerow$css tabletext\">".$stdInfo[$k][1]."</td>";
				$detail_table .= "<td class=\"tablerow$css tabletext\">".$parByFieldArr[$i]." - ".$stdInfo[$k][0]."</td>";
				
				for($j=0; $j<sizeof($parStatsFieldTextArr); $j++) {		# for each item
					$css = ($p%2==0) ? "" : "2";
					$css2 = (sizeof($parStatsFieldTextArr)==$j+1) ? "seperator_right" : "seperator_left";
					$detail_table .= (isset($data[intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$parByFieldArr[$i]][$stdInfo[$k][0]])) ? "<td align=\"center\" class=\"tabletext tablerow_total".$css." ".$css2."\">".$data[intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$parByFieldArr[$i]][$stdInfo[$k][0]]."</td>" : "<td align=\"center\" class=\"tabletext tablerow_total".$css." ".$css2."\">0</td>";
					$itemTotal[intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))] += $data[intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$parByFieldArr[$i]][$stdInfo[$k][0]];
				}
				$p++;
//				$detail_table .= "<td width=\"1\" class=\"tablebluetop\"><img src=\"{$image_path}/{$LAYOUT_SKIN}\"/10x10.gif\" width=\"1\" height=\"1\"></td>";
				
				$meritDemeritCount = $ldiscipline->retrieveMeritDemeritCountFromGM($Period, $parPeriodField1, $parPeriodField2, $selectBy, $stdInfo[$k][2], $parStatsFieldArr, $parStatsFieldTextArr, $categoryID);
				//debug_r($meritDemeritCount);
			
				if($record_type==1) {
					$m = 1;
				} else {
					$m = 0;
				}
				foreach($i_MeritDemerit as $columnNo) {
					$tempCount = 0;
					for($n=0; $n<sizeof($meritDemeritCount); $n++) {
						if($meritDemeritCount[$n]['ProfileMeritType']==$m)
							$tempCount += $meritDemeritCount[$n]['countTotal'];
					}
					$detail_table .= "<td align=\"center\">";
					$detail_table .= ($tempCount != 0) ? $tempCount : "0";
					$columnMeritDemeritCount[$m] += $tempCount;
					$detail_table .= "</td>";
					
					if($record_type==1) {
						$m++;
					} else {
						$m--;
					}
				}

				$detail_table .= "</tr>";
			}
		}
		
	}
	
	$detail_table .= "</tr>";
	
	$detail_table .= "<tr><td colspan=\"".(sizeof($parStatsFieldTextArr)+sizeof($i_MeritDemerit)+2)."\"><table cellspacing=0 cellpadding=0 width=100%><tr><td class=\"tablebluetop\"><img src=\"{$image_path}/{$LAYOUT_SKIN}\"/10x10.gif\" width=\"100%\" height=\"1\"></td></tr></table></td>";
	$detail_table .= "<tr><td>".$i_Discipline_System_Report_Class_Total."</td>";
	if($selectBy=="CLASS") {
		$detail_table .= "<td>&nbsp;</td>";
	}


	for($i=0; $i<sizeof($parStatsFieldTextArr); $i++) {
		$css = (sizeof($parStatsFieldTextArr)==$i+1) ? "class=\"seperator_right\"" : "class=\"seperator_left\"";
		$detail_table .= (isset($itemTotal[intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$i]))])) ? "<td align=\"center\" $css>".$itemTotal[intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$i]))]."</td>" : "<td align=\"center\" $css>0</td>";
	}
//	$detail_table .= "<td width=\"1\" class=\"tablebluetop\"><img src=\"{$image_path}/{$LAYOUT_SKIN}\"/10x10.gif\" width=\"1\" height=\"1\"></td>";
	
	if($record_type==1) {
		$m = 1;
	} else {
		$m = 0;
	}
	foreach($i_MeritDemerit as $columnNo) {
		$detail_table .= "<td align=\"center\">";
		$detail_table .= $columnMeritDemeritCount[$m];
		
		if($record_type==1) {
			$m++;
		} else {
			$m--;
		}
		$detail_table .= "</td>";
	}
	
	
	$detail_table .= "</tr>";
	
	$detail_table .=  "</table>\n";
	
	$tableButton .=  "<tr><td class=\"dotline\"><img src=\"{$image_path}/{$LAYOUT_SKIN}\"/10x10.gif\" width=\"10\" height=\"1\"></td></tr>";
	$tableButton .=  "<tr>";
	$tableButton .=  "<td align=\"center\">";
	$tableButton .=  $linterface->GET_ACTION_BTN($button_export, "submit", "document.frmStats.action='index_export.php';document.frmStats.target='_self';return checkForm();")."&nbsp;";
	$tableButton .=  $linterface->GET_ACTION_BTN($button_print, "submit", "document.frmStats.action='index_print.php';document.frmStats.target='_blank';return checkForm();")."&nbsp;";
	$tableButton .=  $linterface->GET_ACTION_BTN($button_back, "button", "self.location='indexStatistics.php'")."&nbsp;";
	$tableButton .=  $linterface->GET_ACTION_BTN($button_view_statistics, "button", "showStatistics()", "printStatBtn");
	$tableButton .=  "</td>";
	$tableButton .=  "</tr>";
	
	############ End of Report Table ###############
	################################################
	
	
	$initialString = "<script language=\"javascript\">hideOption();</script>\n";
} else {
	$optionString = "<td height=\"20\" class=\"tabletextremark\"><em>- $i_Discipline_Statistics_Options -</em></td>";
	$initialString = "<script language=\"javascript\">jPeriod='YEAR';hideSpan('spanMisCat');showSpan('spanGoodCat');changeCat('0');document.getElementById('selectGoodCat').value='0';</script>\n";
}

$linterface->LAYOUT_START();
?>

<script language="javascript">
var jPeriod="<?=$Period?>";
var jStatType="<?=$StatType?>";

// Generate Good Conduct Category
var good_cat_select = new Array();
good_cat_select[0] = new Option("-- <?=$eDiscipline["SelectRecordCategory"]?> --",0);
<?	foreach($good_cats as $k=>$d)	{ ?>
		good_cat_select[<?=$k+1?>] = new Option("<?=str_replace('"', '\"', $d['Name'])?>",<?=$d['CategoryID']?>);
<?	} ?>

// Generate Misconduct Category
var mis_cat_select = new Array();
mis_cat_select[0] = new Option("-- <?=$eDiscipline["SelectRecordCategory"]?> --",0);
<?	foreach($mis_cats as $k=>$d)	{ ?>
		mis_cat_select[<?=$k+1?>] = new Option("<?=str_replace('"', '\"', $d['Name'])?>",<?=$d['CategoryID']?>);
<?	} ?>

function changeCat(value)
{
	var item_select = document.getElementById('ItemID[]');
	var selectedCatID = value;
	var record_type_good = document.getElementById('record_type_good');

	while (item_select.options.length > 0)
	{
		item_select.options[0] = null;
	}
	
	if (selectedCatID == 0) {
	<?
		$curr_cat_id = "";
	?>
		if(document.frmStats.record_type_good.checked) {
	<?
		$pos = 0;
			for ($i=0; $i<sizeof($good_items); $i++)
			{
				list($r_catID, $r_itemID, $r_itemName) = $good_items[$i];
				$r_itemName = intranet_undo_htmlspecialchars($r_itemName);
				$r_itemName = str_replace('"', '\"', $r_itemName);
				$r_itemName = str_replace("'", "\'", $r_itemName);
				//$r_itemName = str_replace('"', '&quot;', $r_itemName);
				echo "item_select.options[".$pos++."] = new Option(\"".$r_itemName."\",".$r_itemID.");\n";
			}
	?>	
		} else {
	<?
			$pos = 0;
			for ($i=0; $i<sizeof($mis_items); $i++)
			{
				list($r_catID, $r_itemID, $r_itemName) = $mis_items[$i];
				$r_itemName = intranet_undo_htmlspecialchars($r_itemName);
				$r_itemName = str_replace('"', '\"', $r_itemName);
				$r_itemName = str_replace("'", "\'", $r_itemName);
				//$r_itemName = str_replace('"', '&quot;', $r_itemName);
				echo "item_select.options[".$pos++."] = new Option(\"".$r_itemName."\",".$r_itemID.");\n";
			}
	?>	
		}
	}
	else if (selectedCatID == '')
	{

	<? 
		$curr_cat_id = "";
		$pos = 0;
		for ($i=0; $i<sizeof($items); $i++)
		{
			list($r_catID, $r_itemID, $r_itemName) = $items[$i];
			$r_itemName = intranet_undo_htmlspecialchars($r_itemName);
			$r_itemName = str_replace('"', '\"', $r_itemName);
			$r_itemName = str_replace("'", "\'", $r_itemName);
			//$r_itemName = str_replace('"', '&quot;', $r_itemName);
			if ($r_catID != $curr_cat_id)
			{
				$pos = 0;
	?>
	}
	else if (selectedCatID == "<?=$r_catID?>")
	{
	<?
				$curr_cat_id = $r_catID;
			}
	?>
		item_select.options[<?=$pos++?>] = new Option("<?=$r_itemName?>",<?=$r_itemID?>);
		<?	if($r_itemID == $ItemID) {	?>
		item_select.selectedIndex = <?=$pos-1?>;
		<? 	} 
		}
	?>
	}
	SelectAll(frmStats.elements['ItemID[]']);
}

function showSpan(span){
	document.getElementById(span).style.display = "inline";
}

function hideSpan(span){
	document.getElementById(span).style.display = "none";
}

function showOption(){
	hideSpan('spanShowOption');
	showSpan('spanHideOption');
	showSpan('spanOptionContent');
	document.getElementById('tdOption').className = '';
}

function hideOption(){
	hideSpan('spanOptionContent');
	hideSpan('spanHideOption');
	showSpan('spanShowOption');
	document.getElementById('tdOption').className = 'report_show_option';
}

function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function getSelectedString(obj, targetObj){
	var tmpString = '';
	for (i=0; i<obj.length; i++){
		if (obj.options[i].selected == true){
			tmpString += obj.options[i].value + ',';
		}
	}
	if (tmpString.length > 0){
		document.getElementById(targetObj).value = tmpString.substr(0, tmpString.length-1);
	} else {
		document.getElementById(targetObj).value = tmpString;
	}
}

function getSelectedTextString(obj, targetObj){
	var tmpString = '';
	for (i=0; i<obj.length; i++){
		if (obj.options[i].selected == true){
			tmpString += obj.options[i].text + ',';
		}
	}
	if (tmpString.length > 0){
		document.getElementById(targetObj).value = tmpString.substr(0, tmpString.length-1);
	} else {
		document.getElementById(targetObj).value = tmpString;
	}
}

function doPrint()
{
	document.frmStats.action = "index_print.php";
	document.frmStats.target = "_blank";
	document.frmStats.submit();
	document.frmStats.action = "";
	document.frmStats.target = "_self";
}

function doExport()
{
	document.frmStats.action = "index_export.php";
	document.frmStats.submit();
	document.frmStats.action = "";
}

function changeRadioSelection(type)
{
	var jsYearRadioObj = document.getElementById("radioPeriod_Year");
	var jsDateRadioObj = document.getElementById("radioPeriod_Date");
	
	if (type == "YEAR")
	{
		jsYearRadioObj.checked = true;
		jsDateRadioObj.checked = false;
		jPeriod = type;
	}
	else
	{
		jsYearRadioObj.checked = false;
		jsDateRadioObj.checked = true;
		jPeriod = type;
	}
}

function checkForm(){
	if (jPeriod=='DATE' && document.getElementById('textFromDate').value=='') {
		alert('<?=$i_alert_pleasefillin.$i_general_startdate?>');
		return false;
	}
	if (jPeriod=='DATE' && document.getElementById('textToDate').value=='') {
		alert('<?=$i_alert_pleasefillin.$i_general_enddate?>');
		return false;
	}
	if(jPeriod=='DATE' && !check_date(document.getElementById('textFromDate'), '<?=$i_invalid_date?>')) {
		return false;
	}
	if(jPeriod=='DATE' && !check_date(document.getElementById('textToDate'), '<?=$i_invalid_date?>')) {
		return false;
	}
	if(jPeriod=='DATE' && (document.frmStats.textFromDate.value > document.frmStats.textToDate.value)) {
		alert("<?=$i_invalid_date?>");
		return false;	
	}
	if (document.getElementById('selectBy').value=='FORM' && countOption(document.getElementById('selectForm[]'))<1) {
		alert('<?=$i_alert_pleaseselect.$i_Discipline_Form?>');
		return false;
	}
	if (document.getElementById('selectBy').value=='CLASS' && countOption(document.getElementById('selectClass[]'))<1) {
		alert('<?=$i_alert_pleaseselect.$i_Discipline_Class?>');
		return false;
	}
	if (document.getElementById('selectMisCat').value!=1 && document.getElementById('selectMisCat').value!=2 && jStatType=='GM_TITLES' && countOption(document.getElementById('ItemID[]'))<1) {
		alert('<?=$i_alert_pleaseselect.$i_Discipline_GoodConductMisconduct_Titles?>');
		return false;
	}
	document.getElementById('Period').value = jPeriod;
	document.getElementById('StatType').value = jStatType;
	getSelectedString(document.getElementById('selectForm[]'), 'SelectedForm');
	getSelectedTextString(document.getElementById('selectForm[]'), 'SelectedFormText');
	getSelectedString(document.getElementById('selectClass[]'), 'SelectedClass');
	getSelectedTextString(document.getElementById('selectClass[]'), 'SelectedClassText');
	getSelectedString(document.getElementById('ItemID[]'), 'SelectedItemID');
	getSelectedTextString(document.getElementById('ItemID[]'), 'SelectedItemIDText');
	document.getElementById('submit_flag').value='YES';
	return true;
}
</script>

<form name="frmStats" method="post" action="indexStatistics.php">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr align="left">
					<td width="50">&nbsp;</td>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr align="left" <? if($_POST["submit_flag"]=="YES") echo "class=\"report_show_option\""; ?>>
								<?=$optionString?>
							</tr>
						</table>
						<span id="spanOptionContent">
						<table width="100%" border="0" cellpadding="5" cellspacing="0" <? if($_POST["submit_flag"]=="YES") echo "class=\"report_show_option\""; ?>>
							<tr valign="top">
								<td height="57" width="10%" valign="top" nowrap="nowrap" class="formfieldtitle">
									<?=$iDiscipline['Period']?> <span class="tabletextrequire">*</span>
								</td>
								<td width="90%">
									<table border="0" cellspacing="0" cellpadding="3">
										<tr>
											<td height="30" colspan="6">
												<input name="radioPeriod" type="radio" id="radioPeriod_Year" value="YEAR" onClick="javascript:jPeriod=this.value"<? echo ($Period!="DATE")?" checked":"" ?>>
												<?=$i_Discipline_School_Year?>
												<?=$selectSchoolYearHTML?>&nbsp;
												<?=$i_Discipline_Semester?>
												<?=$selectSemesterHTML?>
											</td>
										</tr>
										<tr>
											<td><input name="radioPeriod" type="radio" id="radioPeriod_Date" value="DATE" onClick="javascript:jPeriod=this.value"<? echo ($Period=="DATE")?" checked":"" ?>> <?=$i_From?> </td>
											<td><?=$linterface->GET_DATE_FIELD2("textFromDateSpan", "frmStats", "textFromDate", $textFromDate, 1, "textFromDate", "onclick=\"changeRadioSelection('DATE')\"")?>&nbsp;</td>
											<td> <?=$i_To?> </td>
											<td><?=$linterface->GET_DATE_FIELD2("textToDateSpan", "frmStats", "textToDate", $textToDate, 1, "textToDate", "onclick=\"changeRadioSelection('DATE')\"")?>&nbsp;</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr valign="top">
								<td valign="top" nowrap="nowrap" class="formfieldtitle">
									<?=$i_Discipline_Target?> <span class="tabletextrequire">*</span>
								</td>
								<td>
									<table width="0%" border="0" cellspacing="2" cellpadding="0">
										<tr>
											<td valign="top">
												<select name="selectBy" onChange="javascript:if(this.value=='FORM'){hideSpan('spanClass');showSpan('spanForm');}else{hideSpan('spanForm');showSpan('spanClass');}">
													<option value="FORM"<? echo ($selectBy!="CLASS")?" selected":"" ?>><?=$i_Discipline_Form?></option>
													<option value="CLASS"<? echo ($selectBy=="CLASS")?" selected":"" ?>><?=$i_Discipline_Class?></option>
												</select>
												<br />
												<span id="spanForm"<? echo ($selectBy!="CLASS")?"":" style=\"display:none\"" ?>>
													<?=$selectFormHTML?>
													<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['selectForm[]']); return false;") ?>
												</span>
												<span id="spanClass"<? echo ($selectBy=="CLASS")?"":" style=\"display:none\"" ?>>
													<?=$selectClassHTML?>
													<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['selectClass[]']); return false;") ?>
												</span>
												<br />
												<span class="tabletextremark">(<?=$i_Discipline_Press_Ctrl_Key?>)</span>
											</td>
											<td valign="top"><br /></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle">
									<span><?=$iDiscipline['RecordType2']?> <span class="tabletextrequire">*</span></span>
								</td>
								<td width="85%">
									<table width="0%" border="0" cellspacing="2" cellpadding="0">
										<tr>
											<td valign="top">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td>
															<input name="record_type" type="radio" id="record_type_good" value="1" <?=($record_type==1 || $record_type=="")?"checked":""?> onClick="hideSpan('spanMisCat');showSpan('spanGoodCat');changeCat('0');document.getElementById('selectGoodCat').value='0';"><label for="record_type_good"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ediscipline/icon_gd_conduct.gif" width="20" height="20" border="0" align="absmiddle"> <?=$i_Discipline_GoodConduct?></label>
															<input name="record_type" type="radio" id="record_type_mis" value="-1" <?=($record_type==-1)?"checked":""?> onClick="hideSpan('spanGoodCat');showSpan('spanMisCat');changeCat('0');document.getElementById('selectMisCat').value='0';"><label for="record_type_mis"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ediscipline/icon_misconduct.gif" width="20" height="20" border="0" align="absmiddle"> <?=$i_Discipline_Misconduct?></label><br />
															<span id="spanGoodCat"<? echo ($record_type==1 || $record_type=="")?"":" style=\"display:none\"" ?>>
																<?=$selectGoodCatHTML?>
															</span>
															<span id="spanMisCat"<? echo ($record_type==-1)?"":" style=\"display:none\"" ?>>
																<?=$selectMisCatHTML?>
															</span>
														</td>
													</tr>
													<tr>
														<td>
															<?=$selectItemIDHTML?>
															<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['ItemID[]']); return false;") ?>
														</td>
													</tr>
													<tr>
														<td>
															<span class="tabletextremark">(<?=$i_Discipline_Press_Ctrl_Key?>)</span>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<input type="hidden" name="selectShowOnlyType" value="ALL">
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap">
									<span class="tabletextremark"><?=$i_general_required_field?></span>
								</td>
								<td>&nbsp;</td>
							</tr>
<!--							<tr>
								<td height="1" class="dotline" colspan="2">
									<img src="<?="$image_path/$LAYOUT_SKIN"?>/10x10.gif" width="10" height="1"></td>
							</tr>//-->
							<tr>
								<td>&nbsp;</td>
								<td><?= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Statistic, "submit", "document.frmStats.action='indexStatistics.php';document.frmStats.target='_self';return checkForm();", "submitBtn01")?>
								</td>
							</tr>
						</table>
						</span>
					</td>
				</tr>
			</table>
			<br />
				<?=$detail_table?>
			<br />
			
		</td>
	</tr>
	<?=$tableButton?>
	<tr>
		<td>
			<span id="spanStatistics" style="display:none"></span>
		<td>
	</tr>
</table>

<input type="hidden" name="submit_flag" id="submit_flag" value="NO" />
<input type="hidden" name="categoryID" id="categoryID" value="<?=$categoryID?>" />
<input type="hidden" name="Period" id="Period" value="<?=$Period?>" />
<input type="hidden" name="submitSelectBy" id="submitSelectBy" value="<?=$selectBy?>" />
<input type="hidden" name="StatType" id="StatType" value="<?=$StatType?>" />
<input type="hidden" name="PeriodField1" id="PeriodField1" value="<?=$parPeriodField1?>" />
<input type="hidden" name="PeriodField2" id="PeriodField2" value="<?=$parPeriodField2?>" />
<input type="hidden" name="ByField" id="ByField" value="<?=implode(",", $parByFieldArr)?>" />
<input type="hidden" name="StatsField" id="StatsField" value="<?=implode(",", $parStatsFieldArr)?>" />
<input type="hidden" name="StatsFieldText" id="StatsFieldText" value="<?=intranet_htmlspecialchars(stripslashes(implode(",", $parStatsFieldTextArr)))?>" />
<input type="hidden" name="SelectedForm" id="SelectedForm" />
<input type="hidden" name="SelectedFormText" id="SelectedFormText" />
<input type="hidden" name="SelectedClass" id="SelectedClass" />
<input type="hidden" name="SelectedClassText" id="SelectedClassText" />
<input type="hidden" name="SelectedItemID" id="SelectedItemID" />
<input type="hidden" name="SelectedItemIDText" id="SelectedItemIDText" />
<input type="hidden" name="id" id="id" value="">
<input type="hidden" name="itemNameColourMappingArr" id="itemNameColourMappingArr" value="<?=rawurlencode(serialize($itemNameColourMappingArr));?>">
<input type="hidden" name="showStatButton" id="showStatButton" value="0" />
<input type="hidden" name="newItemID" id="newItemID" value="<?=implode(",", $parStatsFieldArr)?>">

</form>
<br />
<?=$initialString?>

	<table width="88%" border="0" cellpadding="5" cellspacing="0" class="result_box">
	<tr><td align="center">

	<div id="my_chart">no flash?</div>
	<script type="text/javascript" src="".$PATH_WRT_ROOT."includes/flashchart_basic/js/json/json2.js"></script>
	<script type="text/javascript" src="".$PATH_WRT_ROOT."includes/flashchart_basic/js/swfobject.js"></script>
	<script type="text/javascript">
		<?/*swfobject.embedSWF("".$PATH_WRT_ROOT."includes/flashchart_basic/open-flash-chart.swf", "my_chart", "".$width."", "".$height."", "9.0.0", "expressInstall.swf")*/?>
	</script>
	
	<script type="text/javascript">
	function ofc_ready(){
	}
	
	function open_flash_chart_data(){
		//alert( 'reading data' );
		return JSON.stringify(data);
	}
	
	function findSWF(movieName) {
	  if (navigator.appName.indexOf("Microsoft")!= -1) {
		return window[movieName];
	  } else {
		return document[movieName];
	  }
	}
	

	var itemID = new Array(); var newItemID = new Array();
	itemID = [<?=implode(",", $parStatsFieldArr)?>];
	
	function setChart(id, display) {
		var j = 0;
		var itemIDList = "";
		newItemID = [];		// initiate newItemID[]
		for(var i=0;i<itemID.length;i++) {
			if(display==true) {
				if(i==0) 
				{
					newItemID[j] = id;
					j++;
				}
				newItemID[j] = itemID[i];
				j++;
			} else {
				if(id != itemID[i])	{
					newItemID[j] = itemID[i];
					j++;
				}
			}
		}
		if(itemID.length==0){
			newItemID[j] = id;
			j++;
		}
		itemID = [];
		for(i=0;i<newItemID.length;i++) {
				itemID[i] = newItemID[i];
				itemIDList += (itemIDList != "") ? "," + newItemID[i] : newItemID[i];
		}
		document.getElementById('newItemID').value = itemIDList;
	}
	

	</script>
	<!--
	############################################ END - flash chart ###############################################################
	-->

	</td></tr>
	<tr><td height="1" class="dotline"><img src="$image_path/$LAYOUT_SKIN/10x10.gif" width="10" height="1"></td></tr>
	<tr><td align="right">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr><td align="center">
	<?=$linterface->GET_ACTION_BTN($button_print_statistics, "button", "printStats();") ?>
	</td></tr>
	</table>
	</td></tr>
	</table>


<script language="javascript">

var xmlHttpStatistics

function showStatistics()
{
	if(document.getElementById('showStatButton').value == 1) {
		document.getElementById('showStatButton').value = 0;
		document.getElementById('printStatBtn').value = "<?=$button_view_statistics?>";
		hideSpan("spanStatistics");
	} else {
		document.getElementById('showStatButton').value = 1;
		document.getElementById('printStatBtn').value = "<?=$button_hide_statistics?>";
		showSpan("spanStatistics");
	}
		
	
	document.getElementById("spanStatistics").innerHTML = "";
	document.getElementById("spanStatistics").style.border = "0px";

	xmlHttpStatistics = GetXmlHttpObject()
	
	if (xmlHttpStatistics==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 

<?
	if ($selectBy == "FORM") {
		$parByFieldArr = $SelectedFormTextArr;
	} else {
		$parByFieldArr = $SelectedClassTextArr;
	}

	
	if($submitBtn01 != "") {
?>		
	var url = "genStatistics2.php?";
	url += "parStatsFieldArr=<?=implode(',',$parStatsFieldArr)?>";
	url += "&Period=<?=$Period?>";
	url += "&parPeriodField1=<?=$parPeriodField1?>";
	url += "&parPeriodField2=<?=$parPeriodField2?>";
	url += "&selectBy=<?=$selectBy?>";
	url += "&parByFieldArr=<?=implode(',',$parByFieldArr)?>";
	url += "&StatType=<?=$StatType?>";
	url += "&parStatsFieldTextArr=<?=implode(',',$parStatsFieldTextArr)?> ";
	url += "&categoryID=<?=$categoryID?>";
	
	xmlHttpStatistics.onreadystatechange = stateChangedStat 
		
	xmlHttpStatistics.open("POST",url,true)
	xmlHttpStatistics.send(null)
	
	<? } ?>
} 

function stateChangedStat() 
{ 
	var showIn = "";
	
	if (xmlHttpStatistics.readyState==4 || xmlHttpStatistics.readyState=="complete")
	{ 
		//document.getElementById("spanStatistics").innerHTML = xmlHttpStatistics.responseText;
		//document.getElementById("spanStatistics").style.border = "0px solid #A5ACB2";
		swfobject.embedSWF("<?=$PATH_WRT_ROOT?>includes/flashchart_basic/open-flash-charto.swf", "my_chart", "850", "350", "9.0.0");
	} 
}

	var data = <?=$chart->toPrettyString()?>;
	swfobject.embedSWF("<?=$PATH_WRT_ROOT?>includes/flashchart_basic/open-flash-chart.swf", "my_chart", "850", "350", "9.0.0");


function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}


</script>
<?
$linterface->LAYOUT_STOP();
?>
 