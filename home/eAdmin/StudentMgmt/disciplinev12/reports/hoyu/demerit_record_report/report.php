<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_cust.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# user access right checking
$ldiscipline = new libdisciplinev12_cust();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || !$sys_custom['eDiscipline']['HoYuPrimarySchCust']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$ldiscipline_ui = new libdisciplinev12_ui_cust();

if($format == 'print'){
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
}
if($format == 'csv'){
	$ldiscipline_ui->getHoYuDemeritRecordReport($_REQUEST);
}else{
	echo $ldiscipline_ui->getHoYuDemeritRecordReport($_REQUEST);
}
if($format == 'print'){
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
}

intranet_closedb();
?>

<? if($format == 'print'){ ?>
	
<style type="text/css">
@charset "utf-8";

/* CSS Document */
.main_container { display:block; width:1000px; height:880px; margin:auto; /*border:1px solid #DDD;*/ padding:30px 20px; position:relative; padding-bottom:90px; }

</style>

<? } ?>