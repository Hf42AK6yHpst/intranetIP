<?php
// Modifying by : yat

###################################
##
##	Date:	2010-02-11 YatWoon
##			add access right checking, only allow eDis admin access this page
##
###################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right (View page)
if(!$ldiscipline->IS_ADMIN_USER()) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$linterface = new interface_html();

$CurrentPage = "ConductPotentialsReport";

$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($Lang['eDiscipline']['SKSS']['ConductPotentialsAssessmentReport']['Name'],"index.php",0);
$TAGS_OBJ[] = array($Lang['eDiscipline']['SKSS']['ClearAssessmentGrade'],"clearGrade.php",1);


$sql = "SELECT MAX(LogDate) FROM MODULE_RECORD_DELETE_LOG WHERE Module='".$ldiscipline->Module."' AND Section='Conduct_Potentials_Report' ORDER BY LogID";
$temp = $ldiscipline->returnVector($sql);
$lastModified = $temp[0];
if($lastModified=="") $lastModified = "---";


$linterface->LAYOUT_START();
?>
<script language="javascript">
function checkform(obj)
{
	if(confirm('<?=$button_continue?>?'))	{
		doDelete();
	}
}

function doDelete()
{
	Block_Thickbox();
	var PostString = Get_Form_Values(document.getElementById("form1"));
	
	var PostVar = PostString;
	//alert(PostVar);
	
	//Block_Element("spanOptionContent");
	$.post('clearGrade_update.php',PostVar,
		function(data){
			//alert(data);
			if (data == "") 
				window.top.location = '/';
			else {
				Get_Return_Message(data);
			}
		});

}
</script>
<br />
<form name="form1" method="post" ACTION="">

	<table width="96%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td align="right" colspan="2"><?= $linterface->GET_SYS_MSG($msg, $xmsg) ?></td>
		</tr>		
		<tr>
			<td align="right" colspan="2"><?= $linterface->GET_WARNING_TABLE($Lang['eDiscipline']['SKSS']['ClearAssessmentGradeMessage']) ?></td>
		</tr>
		<tr>
			<td align="left" class="tabletextremark"><?=$i_LastModified?> : <?=$lastModified?></td>
		</tr>
		
		<tr>
			<td colspan="2" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
				<?= $linterface->GET_ACTION_BTN($button_submit, "button", "checkform()", "", "", "", "formbutton_alert")?>&nbsp;<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location.href='index.php'","","","","formbutton_v30")?>
			</td>
		</tr>
	</table>
</form>	


<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>