<?php
// Modifying by : yat

###################################
##
##	Date:	2010-02-11 YatWoon
##			add access right checking, only allow eDis admin access "Clear Assessment Grade"
##
###################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

$CurrentPage = "ConductPotentialsReport";

$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($Lang['eDiscipline']['SKSS']['ConductPotentialsAssessmentReport']['Name'],"index.php",1);
if($ldiscipline->IS_ADMIN_USER())
	$TAGS_OBJ[] = array($Lang['eDiscipline']['SKSS']['ClearAssessmentGrade'],"clearGrade.php",0);

$lclass = new libclass();
$linterface = new interface_html();

# Academic Year ID
$academicYearID = Get_Current_Academic_Year_ID();

# School year
$selectSchoolYearHTML = getAYNameByAyId();

# Semester
$currentSemester = getCurrentSemester();
$selectSemesterHTML = "<select name=\"selectSemester\" id=\"selectSemester\">";
$selectSemesterHTML .= "</select>";

$classAry = $lclass->getSelectClassWithoutWholeForm($academicYearID);
$clsSelectionMenu = getSelectByArray($classAry, ' name="classID" id="classID" onChange="Get_Supplementary()"', $classID); 

$signature = "<table>";
foreach($Lang['eDiscipline']['SKSS']['ConductPotentialSignature'] as $id=>$name) {
	$signatureDisplay = Get_Lang_Selection($name[1],$name[0]);
	$signature .= '<tr><td><input type="checkbox" name="option[]" id="option'.($id).'" value="'.($id).'" '.(sizeof($option)>0 && in_array(($id), $option) ? "checked" : "").'><label for="option'.($id).'">'.$signatureDisplay.'</td></tr>';
}
$signature .= "</table>";

# Submit from form
if ($_POST["submit_flag"] == "YES") {
	### Table Data ###
	
	if($submit_flag=="YES") {

		$tableButton .= "<table width='90%' border='0' align='center'><tr><td align='center'>"
							.$linterface->GET_ACTION_BTN($button_print, "button", "doPrint();")."&nbsp;"
							.$linterface->GET_ACTION_BTN($button_cancel, "button", "resetSpan()")."&nbsp;</td></tr></table>";	
							
		$bottom = '<div class="edit_bottom_v30">
			<p class="spacer"></p>
				'.$tableButton.'
			</div>';															
	}

	
	############ End of Report Table ###############
	################################################
	
} 

$linterface->LAYOUT_START();
echo $linterface->Include_JS_CSS();

?>
<script language="javascript">

function doPrint()
{
	document.form1.action = "index_print.php";
	document.form1.target = "_blank";
	document.form1.submit();
	document.form1.action = "index.php";
	document.form1.target = "_self";
}

function doExport()
{
	document.form1.action = "index_export.php";
	//document.form1.target = "_blank";
	document.form1.submit();
	document.form1.action = "index.php";
	//document.form1.target = "_self";
}


function Get_Supplementary()
{
	var PostVar = {
			searchText: encodeURIComponent($('Input#searchText').val()),
			AcademicYearID: encodeURIComponent($('#AcademicYearID').val()),
			classID: encodeURIComponent($('#classID').val()),
			selectSemester: encodeURIComponent($('#selectSemester').val()),
			issueDate: encodeURIComponent($('#issueDate').val()),
			startDate: encodeURIComponent($('#startDate').val()),
			endDate: encodeURIComponent($('#endDate').val())
			}

	Block_Element("spanOptionContent");
	$.post('ajax_get_supplementary.php',PostVar,
		function(data){
			//alert(data);
			if (data == "die") 
				window.top.location = '/';
			else {
				$('div#spanOptionContent').html(data);
				Thick_Box_Init();
				UnBlock_Element("spanOptionContent");
			}
		});
}

function doSave(print)
{
	Block_Thickbox();
	var PostString = Get_Form_Values(document.getElementById("form1"));
	

	var PostVar = PostString;
	//alert(PostVar);
	
	//Block_Element("spanOptionContent");
	$.post('ajax_save_grade.php',PostVar,
		function(data){
			//alert(data);
			if (data == "") 
				window.top.location = '/';
			else {
				Get_Return_Message(data);
			}
		});
	
	if(print==1) {
		doPrint();
	}
}

function resetSpan() {
	document.getElementById('spanOptionContent').innerHTML = "";
	document.form1.reset();
}

{
//on page load call tb_init
function Thick_Box_Init()
{   
	tb_init('a.thickbox, area.thickbox, input.thickbox');//pass where to apply thickbox
}
}
</script>

<form name="form1" id="form1" method="post" action="index.php">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0">
				<tr align="left">
					<td>
						<table class="form_table_v30">
							<tr valign="top">
								<td width="10%" valign="top" nowrap="nowrap" class="field_title">
									<?=$Lang['SysMgr']['Homework']['AcademicYear']?>
								</td>
								<td width="90%">												
									<?=$selectSchoolYearHTML?>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap"  class="field_title">
									<?=$i_ClassName?>
								</td>
								<td>
									<?=$clsSelectionMenu?>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap"  class="field_title">
									<?=$Lang['eDiscipline']['SKSS']['IssueDate']?>
								</td>
								<td>
									<?=$linterface->GET_DATE_PICKER("issueDate",$issueDate)?>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap"  class="field_title">
									<?=$Lang['eDiscipline']['SKSS']['Signature']?>
								</td>
								<td>
									<?=$signature?>
								</td>
							</tr>

						</table>
						<div id="spanOptionContent">
							<input type="hidden" name="studentSize" id="studentSize" value="0">
						</div>
					</td>
				</tr>
			</table>
			<?=$bottom?>
		</td>
	</tr>
</table>

<input type="hidden" name="submit_flag" id="submit_flag" value="NO" />
<input type="hidden" name="id" id="id" value="">
<input type="hidden" name="studentFlag" id="studentFlag" value="0">			
<input type="hidden" name="academicYearID" id="academicYearID" value="<?=$academicYearID?>">
</form>
<br />

<?
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
 