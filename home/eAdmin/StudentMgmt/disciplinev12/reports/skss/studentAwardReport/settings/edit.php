<?php
# using: henry chow

$PATH_WRT_ROOT = "../../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");

intranet_auth();
intranet_opendb();


$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lfilesystem = new libfilesystem();

//$ldiscipline->CONTROL_ACCESS("Discipline-SETTINGS-eNoticeTemplate-Access");
if(is_array($TemplateID))
{
	$TemplateID = $TemplateID[0];
}
$tempDetails = $ldiscipline->retrieveTemplateDetails($TemplateID);
list($Module,$CategoryID,$Title,$Subject,$Content,$ReplySlip,$RecordType,$RecordStatus,$ReplySlipContent) = $tempDetails[0];
/*
if($ReplySlip || $ReplySlipContent)
	$ReplySlipChecked = "checked";
*/
if($RecordStatus==0)
{
	$draftChecked = 'checked';
}
elseif($RecordStatus==1)
{
	$publishChecked = 'checked';
}


$tmpVariableArr = $ldiscipline->TemplateVariable_SKSS();

if(is_array($tmpVariableArr))
{
	$a = 0;
	foreach($tmpVariableArr as $Key=>$Value)
	{
		$variableArr[$a][0] ="<img src='".$image_path."/".$LAYOUT_SKIN."/discipline/".$Key.".gif'>";
// 		$variableArr[$a][1] = convert2unicode($Value[0],1);
		$variableArr[$a][1] = $Value[0];
		$a++;
	}
}

$t = $linterface->GET_SELECTION_BOX($variableArr, "id=\"genVariable\" name=\"genVariable\"","","$CategoryID");
/*
$x = "<table align='center' width='100%' border='0' cellpadding='5' cellspacing='0'>
		<tr valign='top'>
			<td valign='top' nowrap='nowrap'>". $Lang['eDiscipline']['AutoFillIn'] ."</td>
			<td class='tabletext'>". $t ."</td>
			<td width='100%'><input type='button' onClick='FillIn()' value='". $Lang['eDiscipline']['Insert'] ."'></td>
		</tr>
	</table>";
*/
$x = $Lang['eDiscipline']['AutoFillIn']." ".$t." <input type='button' onClick='FillIn()' value='". $Lang['eDiscipline']['Insert'] ."'>";
	
	
# menu highlight setting
$CurrentPage = "StudentAwardReport";

$TAGS_OBJ[] = array($Lang['eDiscipline']['SKSS']['StudentAwardReport'],"/home/eAdmin/StudentMgmt/disciplinev12/reports/skss/studentAwardReport/index.php",0);
$TAGS_OBJ[] = array($Lang['eDiscipline']['SKSS']['TemplateManagement'],"/home/eAdmin/StudentMgmt/disciplinev12/reports/skss/studentAwardReport/settings/index.php",1);

$PAGE_NAVIGATION[] = array($Lang['eDiscipline']['SKSS']['TemplateManagement'], "index.php");
$PAGE_NAVIGATION[] = array($button_edit_template);

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>
<script type="text/javascript">
var callback_changeGenVariables = {
	success: function ( o )
    {
	    if(document.form1.temp_form_display_alert.value==1)
	    {
		    alert("<?=$Lang['eDiscipline']['ChangeReasonNotice']?>");
	    }
	    document.form1.temp_form_display_alert.value = 1;
	    var tmp_str = o.responseText;
	    document.getElementById('genVariableDiv').innerHTML = tmp_str;
	}
}

function changeGenVariables()
{
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	var path = "ajax_genvariable.php";
	
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_changeGenVariables);
}
function checkForm(obj) 
{
	if(!check_text(obj.Title,'<?=$i_Discipline_System_Discipline_Template_Title_JS_warning?>'))
	{
		return false
	}
	
	var field = FCKeditorAPI.GetInstance('Content');
	var content_var = field.GetHTML(true);
	if(content_var=="")
	{
		alert('<?=$i_Form_pls_fill_in?>');
		return false
	}
	
	return true
}

function click_edit()
{
	newWindow("<?=$ReplySlipPage?>",1);
	document.form2.cStr.value = document.form1.cStr.value;
	document.form2.submit();
	
}

function FillIn()
{
	var field = FCKeditorAPI.GetInstance('Content');
	//alert(field.GetHTML(true));
	field.InsertHtml(document.form1.genVariable.value);
}
						
</script>
<form name="form1" method="post" action="edit_update.php" onSubmit="return checkForm(form1)">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr> 
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table class="form_table_v30">
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="field_title"><?=$i_Discipline_System_Discipline_Template_Title?>
								<span class="tabletextrequire">* </span></td>
								<td class="tabletext"><INPUT maxLength="80" name="Title" class="textboxtext" value="<?=$Title?>">
								</td>
							</tr>
						
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="field_title"><?=$i_Discipline_Status?>
								<span class="tabletextrequire">* </span></td>
								<td class="tabletext">
								<input type='radio' id='RecordStatus0' name='RecordStatus' value=0 <?=$draftChecked?>><label for="RecordStatus0"><?=$i_Discipline_System_Discipline_Template_Draft?></label>
								<input type='radio' id='RecordStatus1' name='RecordStatus' value=1 <?=$publishChecked?>><label for="RecordStatus1"><?=$i_Discipline_System_Discipline_Template_Published?></label>
								</td>
							</tr>
						
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="field_title"><?=$eDiscipline['Content']?> <span class="tabletextrequire">* </span></td>
								<td>
									<?=$x?>
								</td>
							</tr>
						</table>
						
						
						<?
						
						include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
						$objHtmlEditor = new FCKeditor ( 'Content' , "100%", "320", "", "Basic2_withInsertImageFlash", intranet_undo_htmlspecialchars($Content));
						$objHtmlEditor->Config['FlashImageInsertPath'] = $lfilesystem->returnFlashImageInsertPath($cfg['fck_image']['SchoolNews'], $id);
						$objHtmlEditor->Create();
						?>
						
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr>
								<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
								<td width="80%">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<span class="dotline">
									<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'")?>
									</span></td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<input type="hidden" name="TemplateID" value="<?= $TemplateID?>">
<input type="hidden" name="qStr" value="<?=$ReplySlip?>">
<input type="hidden" name="aStr" value="">
<input type="hidden" name="cStr" value="<?=$ReplySlipContent?>">
<input type="hidden" name="temp_form_display_alert" value="0">

<script>changeGenVariables()</script>
</form>

<form name="form2" action="<?=$ReplySlipPage?>" method="post" target="intranet_popup1">
<input type="hidden" name="cStr" value="">
</form>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>


