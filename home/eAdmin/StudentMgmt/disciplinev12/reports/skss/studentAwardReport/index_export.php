<?php

$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$noticeAry = explode(',',$noticeList);

$ldiscipline = new libdisciplinev12();
$lexport = new libexporttext();

$filename = "student_award_report.csv";



$ExportArr = array();
$row = 0;
foreach($noticeAry as $noticeID) {
	$sql = "SELECT RecipientID FROM INTRANET_NOTICE WHERE NoticeID='$noticeID'";
	$noticeData = $ldiscipline->returnVector($sql);
	if(!is_numeric($noticeData[0]))
		$recipientID = substr($noticeData[0],1);
	else 
		$recipientID = $noticeData[0];
		
	$stdInfo = $ldiscipline->retrieveStudentInfoByID2($recipientID, Get_Current_Academic_Year_ID());
	
	list($engname, $chiname, $clsName, $clsNo) = $stdInfo[0];

	$ExportArr[$row][] = Get_Lang_Selection($chiname, $engname). "($clsName - $clsNo)";
	$row++;

	if($radioPeriod=="YEAR") {
		if($selectSemester=="0") $selectSemester = "";
		$startDate = getStartDateOfAcademicYear($selectYear, $selectSemester);
		$endDate = getEndDateOfAcademicYear($selectYear, $selectSemester);
	} else {
		$startDate = $textFromDate;
		$endDate = $textToDate;
	}
	$cond = " AND RecordDate BETWEEN '$startDate' AND '$endDate'";

	$studentWithRecord = 0;
	
	# waived record (Rainbow Scheme)
	$sql = "SELECT SUM(ProfileMeritCount), COUNT(RecordID), Remark FROM DISCIPLINE_MERIT_RECORD WHERE StudentID='$recipientID' AND RecordStatus=".DISCIPLINE_STATUS_WAIVED." AND ReleaseStatus=".DISCIPLINE_STATUS_RELEASED.$cond."  group by Remark";
	
	$waivedRecord = $ldiscipline->returnArray($sql,3);
	
	$total = 0;
	$delim = "";
	$remarkText = "";
	for($i=0; $i<sizeof($waivedRecord); $i++) {
		list($sum, $count, $remark) = $waivedRecord[$i];
		$total += $sum;
		$remarkText .= $delim.$remark."&nbsp;";
		if($count!=1) $remarkText .= "(x".$count.")";
		$delim = ", ";
	}
	
	if(sizeof($waivedRecord)>0) {
		
		//if($intranet_session_language=="en")
		//	$totalText = $total." ".$Lang['eDiscipline']['SKSS']['WaivedDemerit']."(s)";
		//else 
			$totalText = $Lang['eDiscipline']['SKSS']['Mark_1'].$Lang['eDiscipline']['SKSS']['WaivedDemerit']." ".$total." ".$Lang['eDiscipline']['SKSS']['Mark_2'];
			
		$ExportArr[$row][] = "---";
		$ExportArr[$row][] = $Lang['eDiscipline']['SKSS']['RainbowScheme'];
		$ExportArr[$row][] = $totalText;
		$ExportArr[$row][] = $remarkText;
		
		$studentWithRecord = 1;
		
		$row++;	
	}

	# Good Conduct records
	$sql = "SELECT i.ItemCode, i.Name, COUNT(r.RecordID), Remark, i.ItemID FROM DISCIPLINE_ACCU_CATEGORY_ITEM i LEFT OUTER JOIN DISCIPLINE_ACCU_RECORD r ON (i.ItemID=r.ItemID) WHERE r.StudentID='$recipientID' and r.RecordType=".GOOD_CONDUCT." and r.RecordStatus=".DISCIPLINE_STATUS_APPROVED." $cond  Group By r.ItemID, r.Remark ORDER BY i.ItemCode";
	$result = $ldiscipline->returnArray($sql);
	//echo $sql.'<br>';
	$delim = "";
	$gcAry = array();

	for($i=0; $i<sizeof($result); $i++) {
		list($itemCode, $itemName, $count, $remark, $itemID) = $result[$i];
		
		# remark
		$remarkText = $remark;
		if($remark!="" && $count>1) $remarkText .= " (x".$count.")";
		//$countText = $Lang['eDiscipline']['SKSS']['Mark_1'].$Lang['eDiscipline']['SKSS']['GoodConduct']." ".$count." ".$Lang['eDiscipline']['SKSS']['Mark_2'];

		$delim = ($gcAry[$itemCode]['RemarkText']!="") ? ", " : "";
		$gcAry[$itemCode]['ItemCode'] = $itemCode;
		$gcAry[$itemCode]['ItemName'] = $itemName;
		$gcAry[$itemCode]['CountText'] += $count;
		$gcAry[$itemCode]['RemarkText'] .= ($remarkText!="") ? $delim.$remarkText : "";
		$gcAry[$itemCode]['Display'] = 1;
	}
	
	$sql = "SELECT ItemID, ItemCode, Name FROM DISCIPLINE_ACCU_CATEGORY_ITEM WHERE CompulsoryInReport=1 AND RecordStatus=".DISCIPLINE_STATUS_APPROVED." ORDER BY ItemCode";
	$compulsoryField = $ldiscipline->returnArray($sql, 3);
	
	$pos = sizeof($gcAry);
	for($i=0; $i<sizeof($compulsoryField); $i++) {
		list($itemID, $itemCode, $itemName) = $compulsoryField[$i];
		$exist = 0;
		for($j=0; $j<sizeof($gcAry); $j++) {
			if($gcAry[$itemCode]['ItemCode']==$itemCode) {
				$exist = 1;
				break;
			}
		}
		
		if($exist==0) {

			$gcAry[$itemCode]['ItemCode'] = $itemCode;
			$gcAry[$itemCode]['ItemName'] = $itemName;
			$gcAry[$itemCode]['CountText'] = 0;
			$gcAry[$itemCode]['RemarkText'] = "&nbsp;";
			$gcAry[$itemCode]['Display'] = 0;
			$pos++;
		}	
	}
	if(sizeof($gcAry)>0)
		sort($gcAry);
	

	if(sizeof($gcAry)>0) {
		$i = 0;
		foreach($gcAry as $code=>$itemAry[]) {
			$countText = $Lang['eDiscipline']['SKSS']['Mark_1'].$Lang['eDiscipline']['SKSS']['GoodConduct']." ".($itemAry[$i]['CountText']==0?" 0 ":$itemAry[$i]['CountText'])." ".$Lang['eDiscipline']['SKSS']['Mark_2'];
			
			$ExportArr[$row][] = $itemAry[$i]['ItemCode'];
			$ExportArr[$row][] = $itemAry[$i]['ItemName'];
			$ExportArr[$row][] = $countText;
			$ExportArr[$row][] = $itemAry[$i]['RemarkText'];
			$row++;
			$i++;
			$studentWithRecord = 1;
		}
	}
	
	# Award Record
	
	$string_type["1"] = $Lang['eDiscipline']['SKSS']['MeritType'][1];
	$string_type["2"] = $Lang['eDiscipline']['SKSS']['MeritType'][2];
	$string_type["3"] = $Lang['eDiscipline']['SKSS']['MeritType'][3];
	$string_type["4"] = $Lang['eDiscipline']['SKSS']['MeritType'][4];
	$string_type["5"] = $Lang['eDiscipline']['SKSS']['MeritType'][5];

	//$sql = "SELECT i.ItemCode, i.ItemName, SUM(r.ProfileMeritCount), r.ProfileMeritType, r.Remark, COUNT(r.RecordID) FROM DISCIPLINE_MERIT_RECORD r LEFT OUTER JOIN DISCIPLINE_MERIT_ITEM i ON (i.ItemID=r.ItemID) WHERE r.StudentID='$recipientID' AND MeritType=1 AND r.RecordStatus=".DISCIPLINE_STATUS_APPROVED." AND ReleaseStatus=".DISCIPLINE_STATUS_RELEASED.$cond." GROUP BY r.ItemID, r.Remark ORDER BY i.ItemCode";
	$sql = "SELECT i.ItemCode, i.ItemName, SUM(r.ProfileMeritCount), r.ProfileMeritType, r.Remark, COUNT(r.RecordID), i.ItemID FROM DISCIPLINE_MERIT_ITEM i LEFT OUTER JOIN DISCIPLINE_MERIT_RECORD r ON (i.ItemID=r.ItemID) WHERE r.StudentID='$recipientID' AND MeritType=1 AND r.RecordStatus=".DISCIPLINE_STATUS_APPROVED." AND ReleaseStatus=".DISCIPLINE_STATUS_RELEASED.$cond." GROUP BY r.ItemID, r.ProfileMeritType, r.Remark ORDER BY i.ItemCode";
	$aRecord = $ldiscipline->returnArray($sql,7);
	//echo $sql;
	$delim = "";
	$aAry = array();
	
	for($i=0; $i<sizeof($aRecord); $i++) {
		list($itemCode, $itemName, $sum, $mType, $remark, $count, $itemID) = $aRecord[$i];
		//$sumText = $Lang['eDiscipline']['SKSS']['Mark_1'].$string_type[$mType]." ".$ldiscipline->TrimDeMeritNumber_00($sum)." ".$Lang['eDiscipline']['SKSS']['Mark_2'];
		$remarkText = $remark;
		if($remark!="" && $count>1) $remarkText .= " (x".$count.")";			
		$delim = ($aAry[$itemCode]['RemarkText']!="") ? ", " : "";
		/*	
		$remarkText = $remark."&nbsp;";
		if($remark!="" && $count>1) $remarkText .= "(x".$count.")";			
		
		$aAry[$i][] = $itemCode;
		$aAry[$i][] = $itemName;
		$aAry[$i][] = $sumText;
		$aAry[$i][] = $remarkText;
		*/
		$aAry[$itemCode][$mType]['ItemCode'] = $itemCode;
		$aAry[$itemCode][$mType]['ItemName'] = $itemName;
		$aAry[$itemCode][$mType]['SumText'] += $ldiscipline->TrimDeMeritNumber_00($sum);
		$aAry[$itemCode][$mType]['RemarkText'] .= ($remarkText!="") ? $delim.$remarkText : "";
		$aAry[$itemCode][$mType]['Display'] = 1;
		
	}			
	//debug_pr($aAry);
	$sql = "SELECT ItemCode, ItemName, RelatedMeritType, ItemID FROM DISCIPLINE_MERIT_ITEM WHERE CompulsoryInReport=1 AND RecordStatus=".DISCIPLINE_STATUS_APPROVED." ORDER BY ItemCode";
	$compulsoryField = $ldiscipline->returnArray($sql,3);
	
	$pos = sizeof($aAry);
	
	for($i=0; $i<sizeof($compulsoryField); $i++) {
		list($itemCode, $itemName, $mType, $itemID) = $compulsoryField[$i];
		$exist = 0;
		for($j=0; $j<sizeof($aAry); $j++) {
			if($aAry[$itemCode]['ItemCode']==$itemCode) {
				$exist = 1;
				break;
			}
		}
		if(!isset($aAry[$itemCode])) {
			//$sumText = $Lang['eDiscipline']['SKSS']['Mark_1'].$string_type[$mType]." 0 ".$Lang['eDiscipline']['SKSS']['Mark_2'];
			$aAry[$itemCode][$mType]['ItemCode'] = $itemCode;
			$aAry[$itemCode][$mType]['ItemName'] = $itemName;
			$aAry[$itemCode][$mType]['SumText'] = 0;
			$aAry[$itemCode][$mType]['RemarkText'] = "&nbsp;";
			$aAry[$itemCode][$mType]['Display'] = 0;
			$pos++;
		}
	}
	if(sizeof($aAry)>0)
		sort($aAry);
	//debug_pr($aAry);
	$i = 0;
	/*
	for($i=0; $i<sizeof($aAry); $i++) {

		list($itemCode, $itemName, $count, $remark) = $aAry[$i];

		//$countText = $Lang['eDiscipline']['SKSS']['Mark_1'].$Lang['eDiscipline']['SKSS']['GoodConduct']." ".$count." ".$Lang['eDiscipline']['SKSS']['Mark_2'];
		//$remarkText = $remark."&nbsp;";
		//if($remark!="" && $count>1) $remarkText .= "(x".$count.")";
		
		$ExportArr[$row][] = $itemCode;
		$ExportArr[$row][] = $itemName;
		$ExportArr[$row][] = $count;
		$ExportArr[$row][] = $remark;
		$row++;
		
		$studentWithRecord = 1;
	}
	*/
	if(sizeof($aAry)>0) {
		foreach($aAry as $code=>$dataAry) {
			foreach($dataAry as $mType=>$itemAry) {
				$sumText = $Lang['eDiscipline']['SKSS']['Mark_1'].$string_type[$mType]." ".($itemAry['SumText'])." ".$Lang['eDiscipline']['SKSS']['Mark_2'];
				$ExportArr[$row][] = $itemAry['ItemCode'];
				$ExportArr[$row][] = $itemAry['ItemName'];
				$ExportArr[$row][] = $sumText;
				$ExportArr[$row][] = $itemAry['RemarkText'];
				$row++;
				$i++;
				$studentWithRecord = 1;
			}
		}
	}
		
	if($studentWithRecord==0) {
		$ExportArr[$row][] = $i_no_record_exists_msg;
		$row++;	
	}		
	
	$ExportArr[$row][] = "";
	$row++;
	
} 
//debug_pr($ExportArr);

$ExportArr[$row][] = "";
$row++;
//debug_pr($ExportArr); exit;	
//$exportColumn[] = $i_ClassName;

		
$export_content .= $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

# Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);


?>