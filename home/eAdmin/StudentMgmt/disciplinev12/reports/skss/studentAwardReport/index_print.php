<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

if(!sizeof($_POST)) {
	header("Location: index.php");	
}

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lnotice = new libnotice();
$ldiscipline = new libdisciplinev12();

include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

############################################
### generate notice content [Start]
############################################
if ($radioPeriod == "YEAR") {
		$parPeriodField1 = $selectYear;
		$parPeriodField2 = $selectSemester;
	} else {
		$parPeriodField1 = $textFromDate;
		$parPeriodField2 = $textToDate;
	}
$parByFieldArr = $rankTargetDetail;
$noticeList = $ldiscipline->retrieve_SKSS_StudentAwardReport($radioPeriod, $parPeriodField1, $parPeriodField2, $rankTarget, $parByFieldArr, $studentID, $periodDescription, $issueDate, $TemplateID, $option, $display=4);
############################################
### generate notice content [End]
############################################
$noticeAry = explode(',',$noticeList);
$noticeData = $lnotice->returnRecord("",$noticeAry);
?>
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class='print_hide'>
	<tr>
		<td align="right">
			<?= $linterface->GET_BTN($button_print, "submit", "window.print(); return false;"); ?></td>
	</tr>
</table>
<?
//debug_pr($option);
$totalHeight = 940;
$bottomHeight = 50 * sizeof($option);
$topHeight = $totalHeight - $bottomHeight;

for($i=0; $i<sizeof($noticeAry); $i++) {
	$noticeID = $noticeAry[$i];
	$lnotice->LoadNoticeData($noticeID);
	
	// 2013-0704-1632-21073
	$_pageBreak = '';
	if($i!=(sizeof($noticeAry)-1)) {
		$_pageBreak = " style='page-break-after:always;' ";
	}
	
	echo "<table width='100%' ".$_pageBreak.">";
	echo "<tr><td valign='top' height='$topHeight'>";
	echo $lnotice->Description;
	echo "</td></tr>";
	echo "<tr><td height='$bottomHeight'>";
	echo $lnotice->footer;
	echo "</td></tr>";
	echo "</table>";
	
	// 2013-0704-1632-21073
//	if($i!=(sizeof($noticeAry)-1))
//		echo "<div style='page-break-after:always'>&nbsp;</div>";
}
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
intranet_closedb();
?>