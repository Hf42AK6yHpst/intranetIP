<?php
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
        
intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if (!($plugin['Disciplinev12'] && $sys_custom['ediscipline_case_report']))
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$id_ary = array();

for($i=0;$i<sizeof($student);$i++)
{
		$targetStudentID = $student[$i];
		
		#Retrieve Class Name and Number of the Student
        $classInfo = $ldiscipline->retrieveStudentInfoByID($targetStudentID);
        $class_name = $classInfo[0][1];
        $class_num = $classInfo[0][2];
        
        $RecordDate = $RecordDate=="" ? date("Y-m-d") : $RecordDate;
        if(isset($reason))	$Reasons = implode(",",$reason);
        $other_reason = $other_r ? $other_reason : "";
        $other_venue = $Venue ? "" : $other_venue;
        $VenueFields = $other_venue ? "" : $VenueFields;
        $other_action = $ActionTaken ? "" : $other_action;
        $ActionFields = $other_action ? "" : $ActionFields;
                
        #uniform fields
        if(isset($problems))	$Problems = implode(",",$problems);
        $other_problem = $other_p ? $other_problem : "";
       
        if(isset($studentinvolved))	$StudentsInvolved = implode(",",$studentinvolved);
        if(isset($teacherinvolved))	$TeachersInvolved = implode(",",$teacherinvolved);
        
        $sql = "
	        insert into DISCIPLINE_CASE_REPORT 
	        (
	        CaseType, RefNo, RecordDate, RecordTime, StudentID, ClassName, ClassNumber, UserID, Reasons, OthersReason, Remark, DateInput,
	        DatesTimes, Details, Punishment, RecordInBlackBook,
	        Problems, OthersProblem, ActionTaken, ActionFields, OtherAction,
	        Venue, VenueFields, OtherVenue, StudentsInvolved, TeachersInvolved,
	        CaseHandledby, FollowUp
	        )
	        values
	        (
	        $CaseType, '$RefNo', '$RecordDate', '$RecordTime', '$targetStudentID', '$class_name', '$class_num', '$UserID', '$Reasons', '$other_reason', '$Remark', now(),
	        '$DatesTimes', '$Details', '$Punishment', '$RecordInBlackBook',
	        '$Problems', '$other_problem', '$ActionTaken', '$ActionFields', '$other_action',
	        '$Venue', '$VenueFields', '$other_venue', '$StudentsInvolved', '$TeachersInvolved',
	        '$CaseHandledby', '$FollowUp'
	        )
        ";
        
        $ldiscipline->db_db_query($sql);
        $id_ary[] = $ldiscipline->db_insert_id();
}

?>

<body>

<form name="form1" action="new_msg_prompt.php" method="POST">
<input type="hidden" name="print_letter" value="<?=$print_letter?>">
<input type="hidden" name="CaseType" value="<?=$CaseType?>">
<input type="hidden" name="msg" value="add">
<?
	for($i=0;$i<sizeof($id_ary);$i++)
		echo "<input type=hidden name='id_ary[]' value='".$id_ary[$i]."'>";
?>
</form>

<script language="Javascript">
<!--
document.form1.submit();
//-->
</script>
</body>