<?php

$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if (!($plugin['Disciplinev12'] && $sys_custom['ediscipline_case_report']))
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPageArr['eDisciplinev12'] = 1;
$CurrentPage = "WahYan_CaseReport";

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$startdate = $startdate=="yyyy-mm-dd" ? "" : $startdate;
$enddate = $enddate=="yyyy-mm-dd" ? "" : $enddate;

$linterface = new interface_html();

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($iDiscipline['CaseReport'], "index.php", 1);
$TAGS_OBJ[] = array($iDiscipline['CreateReport'], "new.php", 0);
$linterface->LAYOUT_START();

if($targetID)
{
	$StudentID = $targetID;
}
else if($targetLogin)
{
	$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin = '$targetLogin' AND RecordType=2 ";
	$temp = $ldiscipline->returnVector($sql);
	$StudentID = $temp[0];
}
	
if($StudentID)
{
	$namefield = getNameFieldWithClassNumberByLang();
	$sql = "SELECT $namefield FROM INTRANET_USER WHERE UserID = '$StudentID' AND RecordType=2 ";
	$temp = $ldiscipline->returnVector($sql);
	$stu_name = $temp[0];
	
	$DisplayStudent = "
		<tr>
			<td class=\"tabletext formfieldtitle\" valign=\"top\" width=\"30%\">". $i_identity_student ."</td>
			<td class=\"tabletext\">$stu_name</td>
		</tr>
	";
}

if($CaseType)
{
	$DisplayCase = "
		<tr>
			<td class=\"tabletext formfieldtitle\" valign=\"top\" width=\"30%\">". $iDiscipline['CaseReport'] ."</td>
			<td class=\"tabletext\">". $ldiscipline->returnCaseName($CaseType) ."</td>
		</tr>
	";
}

if(!($startdate=="" or $startdate=="yyyy-mm-dd"))
{
	$DisplayStartDate = "
		<tr>
			<td class=\"tabletext formfieldtitle\" valign=\"top\" width=\"30%\">". $i_general_startdate ."</td>
			<td class=\"tabletext\">". $startdate ."</td>
		</tr>
	";
}

if(!($enddate=="" or $enddate=="yyyy-mm-dd"))
{
	$DisplayEndDate = "
		<tr>
			<td class=\"tabletext formfieldtitle\" valign=\"top\" width=\"30%\">". $i_general_enddate ."</td>
			<td class=\"tabletext\">". $enddate ."</td>
		</tr>
	";
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("RecordDate");

$namefield = getNameFieldWithClassNumberByLang("b.");

$cases = $ldiscipline->returnCaseReportList();
$t = "CASE (CaseType) ";
for($c=0;$c<sizeof($cases);$c++)
{
	$t .= " WHEN ". $cases[$c][0]." THEN '". $cases[$c][1] ."'";
}
$t .= "
	ELSE '' 
END
";

$edit_link1 = "'<a href=edit.php?targetID=$StudentID&startdate=$startdate&enddate=$enddate&RecordID=', RecordID ,' class=tablelink>'";
$edit_link2 = "'</a>'";
$select_fields = "";
if(!$StudentID)	$select_fields .= ", concat($edit_link1, $namefield, $edit_link2) ";
if(!$CaseType)	$select_fields .= ", concat($edit_link1, $t, $edit_link2) ";

$cond = array();
if($StudentID) 	$cond[] = "StudentID = $StudentID ";	
if($CaseType) 	$cond[] = "CaseType = $CaseType ";	
if($startdate)	$cond[] = "RecordDate >= '$startdate' ";
if($enddate)	$cond[] = "RecordDate <= '$enddate' ";
$conds = implode(" and ", $cond);

$li->sql = "
	select 
		concat($edit_link1, RecordDate, $edit_link2)
		$select_fields
		, CONCAT('<input type=\"checkbox\" name=\"RecordID[]\" value=\"', RecordID ,'\">')
	from 
		DISCIPLINE_CASE_REPORT as a,
		INTRANET_USER as b
	where 
		a.StudentID = b.UserID and
		$conds
	";

$li->no_col = 3;
$li->IsColOff = "eDisCaseReportList";

$li->column_list .= "<td class='tabletop tabletopnolink' nowrap width='10'>#</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink' nowrap width='100'>".$i_general_record_date."</td>\n";
if(!$StudentID)
{
	$li->column_list .= "<td class='tabletop tabletopnolink' nowrap width='100%'>".$i_identity_student."</td>\n";
	$li->no_col++;
}
if(!$CaseType)
{
	$li->column_list .= "<td class='tabletop tabletopnolink' nowrap width='100%'>".$iDiscipline['CaseReport']."</td>\n";
	$li->no_col++;
}
$li->column_list .= "<td width=1 class='tabletoplink'>".$li->check("RecordID[]")."</td>\n";

$delBtn 	= "<a href=\"javascript:checkRemove(document.form1,'RecordID[]','remove.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_remove . "</a>";
$editBtn 	= "<a href=\"javascript:checkEdit(document.form1,'RecordID[]','edit.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";

?>

<br />
<form name="form1" action="list.php" method="get" >
<table border="0" cellpadding="5" cellspacing="0" width="90%">
	<?=$DisplayStudent?>
	<?=$DisplayCase?>
	<?=$DisplayStartDate?>
	<?=$DisplayEndDate?>
</table>

<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<tr>
		<td align="center"><?= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='index.php'"); ?></td>
	</tr>
</table>

<br/>

<!----------------- List -----------------//-->
<table width="95%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td align="right" valign="bottom" colspan="2" height="28">
		<table border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="21"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_01.gif" width="21" height="23" /></td>
			<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_02.gif">
				<table border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td nowrap><?=$delBtn?></td>
	                <td><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="5"></td>
	                <td nowrap><?=$editBtn?></td>
				</tr>
				</table>
			</td>
			<td width="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_03.gif" width="6" height="23" /></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td align='center'>
		<?=$li->display();?>
	</td>
</tr>
<tr>
	<td height='1' class='dotline'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1'></td>
</tr>
</table>

<input type="hidden" name="CaseType" value="<?=$CaseType?>" />
<input type="hidden" name="targetID" value="<?=$targetID?>" />
<input type="hidden" name="targetLogin" value="<?=$targetLogin?>" />
<input type="hidden" name="startdate" value="<?=$startdate?>" />
<input type="hidden" name="enddate" value="<?=$enddate?>" />

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

</form>

<br />
<?

$linterface->LAYOUT_STOP();
?>