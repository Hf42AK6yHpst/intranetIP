<?php
//Modifying by: YAT

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$ldiscipline = new libdisciplinev12();

$linterface         = new interface_html();
$CurrentPage = "DeleteLog";

if (!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$thisModule = strtoupper($ldiscipline->Module);
$StartDate = $StartDate ? $StartDate : date("Y-m-d");
$EndDate = $EndDate ? $EndDate : date("Y-m-d");

# TABLE INFO
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
if($field=="") $field = 0;
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("a.LogDate");

$name_field = getNameFieldWithClassNumberByLang("b.");
$sql = "select 
		left(a.LogDate,10),
		$name_field as logby,
		case 
			when a.Section='Award_and_Punishment' then '". $eDiscipline['Award_and_Punishment'] ."'
			when a.Section='Good_Conduct_and_Misconduct' then '". $eDiscipline['Good_Conduct_and_Misconduct'] ."'
			when a.Section='Case_Record' then '". $eDiscipline['Case_Record'] ."'
			else '". $Lang['eDiscipline']['Others'] ."'
		end,
		a.RecordDetail
		from 
			MODULE_RECORD_DELETE_LOG as a 
			INNER join INTRANET_USER as b on (b.UserID = a.LogBy)
		where
			left(a.LogDate,10) >= '$StartDate' and left(a.LogDate,10) <= '$EndDate'
			and a.Module = '". $ldiscipline->Module."'
		";

$li->sql = $sql;
$li->no_col = 4;
$li->IsColOff = "GeneralDisplayWithNavigation";

// TABLE COLUMN
$li->column_list .= "<td class='tabletop tabletopnolink'>".$Lang['eDiscipline']['DeletedDate']."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$Lang['eDiscipline']['DeletedBy']."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$Lang['eDiscipline']['RecordType']."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$Lang['eDiscipline']['RecordInfo']."</td>\n";

### Button
$delBtn         = "<a href=\"javascript:clickDelete()\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $Lang['eDiscipline']['DeleteRecordsInDateRange'] . "</a>";

### Filter - Date Range
$date_select = $eNotice['Period_Start'] .": ";
$date_select .= $linterface->GET_DATE_FIELD2("subStartSpan", "form1", "StartDate", $StartDate, 1, "StartDate") . "&nbsp;";
$date_select .= $eNotice['Period_End']."&nbsp;";
$date_select .= $linterface->GET_DATE_FIELD2("subEndSpan", "form1", "EndDate", $EndDate, 1, "EndDate");
$date_select .= $linterface->GET_BTN($button_submit, "submit");

### Title ###
$TAGS_OBJ[] = array($Lang['eDiscipline']['DeleteLog']);
$CurrentPageArr['eDisciplinev12'] = 1;
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

?>
<SCRIPT LANGUAGE=Javascript>
<!--//
function clickDelete()
{
	document.form1.action = "remove_update.php";
	AlertPost(document.form1, "remove_update.php", "<?=$Lang['eDiscipline']['ConfirmDeleteRecordsInDateRange']?>");
}

function check_form()
{
	obj = document.form1;
	
	if(!check_date(obj.StartDate,"<?=$i_invalid_date?>"))
	{
		obj.StartDate.focus();
		return false;
	}
	
	if(!check_date(obj.EndDate,"<?=$i_invalid_date?>"))
	{
		obj.EndDate.focus();
		return false;
	}
	
	if(compareDate(obj.EndDate.value, obj.StartDate.value)<0) {
		obj.StartDate.focus();
		alert("<?=$i_con_msg_date_startend_wrong_alert?>");
		return false;
	}	
	
	return true;
}
//-->
</SCRIPT>

<br />
<form name="form1" method="get" action="index.php" onSubmit="return check_form();">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">

<tr>
        <td align="center">
                <table width="96%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                        <td align="left" class="tabletext">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                        <td align="right" valign="bottom" colspan="2"><?=$linterface->GET_SYS_MSG($msg, $xmsg2);?></td>
                                </tr>
								
                                <tr>
                                        <td align="left" valign="bottom"><?=$date_select?></td>
                                        <td align="right" valign="bottom" height="28">
                                        	<table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                        <td width="21"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_01.gif" width="21" height="23" /></td>
                                                        <td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_02.gif">
                                                                <table border="0" cellspacing="0" cellpadding="2">
                                                                <tr>
                                                                        <td nowrap><?=$delBtn?></td>
                                                                </tr>
                                                                </table>
                                                        </td>
                                                        <td width="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_03.gif" width="6" height="23" /></td>
                                                </tr>
                                                </table>
                                        </td>
                                </tr>
                                <tr>
                                        <td colspan="2">
                                                <?=$li->display();?>
                                        </td>
                                </tr>
                                </table>
                        </td>
                </tr>
                </table>
        </td>
</tr>
</table>
<br />

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>

