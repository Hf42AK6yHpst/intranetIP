<?php
# using:  
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");


intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lsp = new libstudentprofile();
$lclass = new libclass();


# Get Levels and Classes
$levels = $lclass->getLevelArray();
$classes = $ldiscipline->getRankClassList("",Get_Current_Academic_Year_ID());
$select_list = "<SELECT name=\"target[]\" id=\"target[]\" MULTIPLE SIZE=\"5\">";
if ($level==1)
{
    for ($i=0; $i<sizeof($levels); $i++)
    {
         list($id,$name) = $levels[$i];

                $selected_tag = (is_array($target) && in_array($id,$target) )?"SELECTED":"";
                $select_list .= "<OPTION value='".$id."' $selected_tag>".$name."</OPTION>";
    }
}
else
{
    for ($i=0; $i<sizeof($classes); $i++)
    {
         //list($id, $name, $lvl) = $classes[$i];
         list($name, $id) = $classes[$i];

                $selected_tag = ( is_array($target) && in_array($id,$target) )?"SELECTED":"";
                $select_list .= "<OPTION value='".$id."' $selected_tag>". $name."</OPTION>";
    }
}
$select_list .= "</SELECT>";


# School Year Menu #
$SchoolYear = $SchoolYear =="" ? Get_Current_Academic_Year_ID() : $SchoolYear;
$years = $ldiscipline->returnAllYearsSelectionArray();
$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='SchoolYear' id='SchoolYear' onFocus='document.form1.dateChoice[0].checked=true' onChange='changeTerm(this.value);changeClassForm(this.value)'", "", $SchoolYear);


$SemesterMenu .= "<option value='WholeYear'";
$SemesterMenu .= ($semester!='WholeYear') ? "" : " selected";
$SemesterMenu .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";

$interval = 5;
$topBoundary = 50;
$meritTypeInUse = 0;
$demeritTypeInUse = 0;

$topSelectMenu = "<select name='top' id='top'>";
for($i=1;$i<=$topBoundary;$i++) {
	if($i%5==0)
		$topSelectMenu .= "<option value='$i'";
		$topSelectMenu .= ($i==$topBoundary) ? " selected" : "";
		$topSelectMenu .= ">$i</option>";
}
$topSelectMenu .= "</select>";

$meritScore = "<table width='100%' cellpadding='0' cellspacing='0' border='0'>";
$meritScore .= "	<tr>";
$meritScore .= "		<td width='50%' valign='top'>";
$meritScore .= "			<table width='90%' ceppadding='2' cellspacing='0' border='0'>";
if (!$lsp->is_merit_disabled) {
	$meritTypeInUse += 1;
	$meritScore .= "			<tr><td width='60%'>$i_Merit_Merit</td><td width='40%'><input type='text' name='merit1' id='merit1' value='0' size='4' maxlength='5' onBlur=\"if(this.value=='') this.value=0\"></td></tr>";	
}
if (!$lsp->is_min_merit_disabled) {
	$meritTypeInUse += 1;
	$meritScore .= "			<tr><td width='60%'>$i_Merit_MinorCredit</td><td width='40%'><input type='text' name='merit2' id='merit2' value='0' size='4' maxlength='5' onBlur=\"if(this.value=='') this.value=0\"></td></tr>";	
}
if (!$lsp->is_maj_merit_disabled) {
	$meritTypeInUse += 1;
	$meritScore .= "			<tr><td width='60%'>$i_Merit_MajorCredit</td><td width='40%'><input type='text' name='merit3' id='merit3' value='0' size='4' maxlength='5' onBlur=\"if(this.value=='') this.value=0\"></td></tr>";	
}
if (!$lsp->is_sup_merit_disabled) {
	$meritTypeInUse += 1;
	$meritScore .= "			<tr><td width='60%'>$i_Merit_SuperCredit</td><td width='40%'><input type='text' name='merit4' id='merit4' value='0' size='4' maxlength='5' onBlur=\"if(this.value=='') this.value=0\"></td></tr>";	
}
if (!$lsp->is_ult_merit_disabled) {
	$meritTypeInUse += 1;
	$meritScore .= "			<tr><td width='60%'>$i_Merit_UltraCredit</td><td width='40%'><input type='text' name='merit5' id='merit5' value='0' size='4' maxlength='5' onBlur=\"if(this.value=='') this.value=0\"></td></tr>";	
}
if($meritTypeInUse==0) {
	$meritScore .= "			<tr><td width='100%'>".$Lang['eDiscipline']['NoMeritTypeInUse']."</td></tr>";	
}
$meritScore .= "			</table>";
$meritScore .= "		<td>";
$meritScore .= "		<td width='50%' valign='top'>";
$meritScore .= "			<table width='90%' ceppadding='2' cellspacing='0' border='0'>";
if (!$lsp->is_warning_disabled) {
	$demeritTypeInUse += 1;
	$meritScore .= "			<tr><td width='60%'>$i_Merit_Warning</td><td width='40%'><input type='text' name='demerit0' id='demerit0' value='0' size='4' maxlength='5' onBlur=\"if(this.value=='') this.value=0\"></td></tr>";	
}
if (!$lsp->is_black_disabled) {
	$demeritTypeInUse += 1;
	$meritScore .= "			<tr><td width='60%'>$i_Merit_BlackMark</td><td width='40%'><input type='text' name='demerit1' id='demerit1' value='1' size='4' maxlength='5' onBlur=\"if(this.value=='') this.value=0\"></td></tr>";	
}
if (!$lsp->is_min_demer_disabled) {
	$demeritTypeInUse += 1;
	$meritScore .= "			<tr><td width='60%'>$i_Merit_MinorDemerit</td><td width='40%'><input type='text' name='demerit2' id='demerit2' value='7' size='4' maxlength='5' onBlur=\"if(this.value=='') this.value=0\"></td></tr>";	
}
if (!$lsp->is_maj_demer_disabled) {
	$demeritTypeInUse += 1;
	$meritScore .= "			<tr><td width='60%'>$i_Merit_MajorDemerit</td><td width='40%'><input type='text' name='demerit3' id='demerit3' value='21' size='4' maxlength='5' onBlur=\"if(this.value=='') this.value=0\"></td></tr>";	
}
if (!$lsp->is_sup_demer_disabled) {
	$demeritTypeInUse += 1;
	$meritScore .= "			<tr><td width='60%'>$i_Merit_SuperDemerit</td><td width='40%'><input type='text' name='demerit4' id='demerit4' value='63' size='4' maxlength='5' onBlur=\"if(this.value=='') this.value=0\"></td></tr>";	
}
if (!$lsp->is_ult_demer_disabled) {
	$demeritTypeInUse += 1;
	$meritScore .= "			<tr><td width='60%'>$i_Merit_UltraDemerit</td><td width='40%'><input type='text' name='demerit5' id='demerit5' value='0' size='4' maxlength='5' onBlur=\"if(this.value=='') this.value=0\"></td></tr>";	
}
if($demeritTypeInUse==0) {
	$meritScore .= "			<tr><td width='100%'>".$Lang['eDiscipline']['NoDemeritTypeInUse']."</td></tr>";	
}
$meritScore .= "			</table>";
$meritScore .= "		</td>";
$meritScore .= "	</tr>";
$meritScore .= "</table>";


# menu highlight setting
$TAGS_OBJ[] = array($Lang['eDiscipline']['TermAwardScheme'], "");

$CurrentPage = "UCCKE_TermAwardScheme";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

?>

<script language="javascript">
<!--
function SelectAll(obj)
{
		var choice = document.getElementById('rankTargetDetail[]');
		//var choice = obj;

         for (var i=0; i<choice.options.length; i++)
         {
              choice.options[i].selected = true;
         }
}

function goCheck(form1) {
	var choiceSelected;
	var choice = "";

	if(form1.dateChoice[0].checked==false && form1.dateChoice[1].checked==false)	 {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['Period']?>");	
		return false;
	}
	
	if(form1.dateChoice[1].checked==true && (form1.startDate.value == "" || form1.endDate.value == "")) {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['Period']?>");	
		return false;
	}
	
	if(form1.dateChoice[1].checked==true && ((!check_date(form1.startDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) || (!check_date(form1.endDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))))
	{
		return false;
	}
	
	if(form1.dateChoice[1].checked==true && (form1.startDate.value > form1.endDate.value)) {
		alert("<?=$i_Discipline_System_Reports_Invalid_Date_Compare?>");
		return false;	
	}
	if(form1.level.value=='') {
		alert("");	
		return false;
	}
	choice = document.getElementById('target[]');
	choiceSelected = 0;
	
	for(i=0;i<choice.options.length;i++) {
		if(choice.options[i].selected==true)
			choiceSelected = 1;
	}
	
	if(choiceSelected==0) {
		alert("<?=$i_alert_pleaseselect." ".$i_Discipline_Class."/".$i_Discipline_Form?>");
		return false;
	}	 
		
}

function SelectAllItem(obj, flag) {
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = flag;
	}
}


//-->
</script>
<script language="javascript">
function changeType(form_obj,lvl_value)
{
     obj = form_obj.elements["target[]"];
     <? # Clear existing options
     ?>
     while (obj.options.length > 0)
     {
            obj.options[0] = null;
     }
     if (lvl_value==1)
     {
         <?
         for ($i=0; $i<sizeof($levels); $i++)
         {
              list($id,$name) = $levels[$i];
              ?>
              obj.options[<?=$i?>] = new Option('<?=intranet_htmlspecialchars($name)?>',<?=$id?>);
              <?

         }
         ?>
     }
     else
     {
         <?
         for ($i=0; $i<sizeof($classes); $i++)
         {
          //    list($id,$name, $lvl_id) = $classes[$i];
         		list($name, $id) = $classes[$i];
              ?>
              obj.options[<?=$i?>] = new Option('<?=$name?>',<?=$id?>);
              <?

         }
         ?>
     }
     SelectAllItem(document.getElementById('target[]'), true)
}


var xmlHttp
var xmlHttp3

function changeTerm(val) {

	if (val.length==0)
	{ 
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
		
	xmlHttp3 = GetXmlHttpObject()
	
	if (xmlHttp3==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "../ajaxGetSemester.php";
	url += "?year=" + val;
	url += "&term=<?=$semester?>";
	url += "&field=semester";
	
	xmlHttp3.onreadystatechange = stateChanged3 
	xmlHttp3.open("GET",url,true)
	xmlHttp3.send(null)
} 

function stateChanged3() 
{ 
	if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete")
	{ 
		document.getElementById("spanSemester").innerHTML = xmlHttp3.responseText;
		document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
	} 
}

function changeClassForm(val) {

	if (val.length==0)
	{ 
		document.getElementById("spanTarget").innerHTML = "";
		document.getElementById("spanTarget").style.border = "0px";
		return
	}
		
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "../ajaxChangeClass.php";
	url += "?year=" + val;
	url += "&level=" + document.getElementById('level').value;
	
	xmlHttp.onreadystatechange = stateChanged 
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
	
} 

function stateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById("spanTarget").innerHTML = xmlHttp.responseText;
		document.getElementById("spanTarget").style.border = "0px solid #A5ACB2";
	} 
}


function GetXmlHttpObject()
{

	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

</script>
<form name="form1" method="post" action="report_view.php" onSubmit="return goCheck(this)">
<table width="99%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td align="center" bgcolor="#FFFFFF">
	      <table width="98%" border="0" cellspacing="0" cellpadding="5">
	          <tr> 
	            <td align="center">
		            <table width="88%" border="0" cellpadding="5" cellspacing="0">
						<tr>
							<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="3">
									<tr>
										<td height="20" class="tabletextremark"><i><?=$i_Discipline_System_Reports_Report_Option?></i></td>
									</tr>
								</table>
								<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
									<tr valign="top">
										<td height="57" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$iDiscipline['Period']?><span class="tabletextrequire">*</span></td>
										<td>
											<table border="0" cellspacing="0" cellpadding="3">
												<tr>
													<td height="30" colspan="6" onClick="document.form1.dateChoice[0].checked=true">
														<input name="dateChoice" type="radio" id="dateChoice[0]" value="1" checked>
														<?=$i_Discipline_School_Year?>
														<?=$selectSchoolYearHTML?>
														<?=$i_Discipline_Semester?> 
														<span id="spanSemester"><select name="semester" id="semester" onFocus="document.form1.dateChoice[0].checked=true" >
															<?=$SemesterMenu?>
														</select></span>
													</td>
												</tr>
												<tr>
													<td><input name="dateChoice" type="radio" id="dateChoice[1]" value="2">
														<?=$iDiscipline['Period_Start']?></td>
													<td align="center" onClick="form1.dateChoice[1].checked=true" onFocus="form1.dateChoice[1].checked=true"><?=$linterface->GET_DATE_PICKER("startDate",$startDate)?></td>
													<td width="25" align="center"><?=$iDiscipline['Period_End']?></td>
													<td align="center" onClick="form1.dateChoice[1].checked=true" onFocus="form1.dateChoice[1].checked=true"><?=$linterface->GET_DATE_PICKER("endDate",$endDate)?></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr valign="top">
										<td valign="top" nowrap="nowrap" class="formfieldtitle"><?="$i_Discipline_Class/$i_Discipline_Form"?><span class="tabletextrequire">*</span></td>
										<td width="80%">
											<table width="100%" border="0" cellspacing="2" cellpadding="0">
												<tr>
													<td valign="top">
														<table border="0">
															<tr class="tabletext">
																<td>
																	<SELECT name="level" onChange="changeClassForm(document.getElementById('SchoolYear').value)">
																		<OPTION value="0" selected><?=$i_Discipline_Class?></OPTION>
																		<OPTION value="1"><?=$i_Discipline_Form?></OPTION>
																	</select>
																</td>
															</tr>
															<tr class="tabletext">
																<td><span id='spanTarget'><?=$select_list?></span>
																	<?= $linterface->GET_BTN($button_select_all, "button", "javascript:SelectAllItem(document.getElementById('target[]'), true)"); ?>
																</td>
															</tr>
															<tr><td colspan="3" class="tabletextremark">(<?=$i_Discipline_Press_Ctrl_Key?>)</td></tr>
														</table>
													</td>
													<td valign="top"><br></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['uccke_MeritScore']?><span class="tabletextrequire">*</span>
										</td>
										<td>
											<?=$meritScore?>
										</td>
									</tr>
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_Top?><span class="tabletextrequire">*</span>
										</td>
										<td>
											<?=$topSelectMenu?>
										</td>
									</tr>
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$list_sortby?><span class="tabletextrequire">*</span>
										</td>
										<td>
											<input type="radio" name="sortBy" id="sortBy1" value="asc" checked> <label for="sortBy1"><?=$list_asc?> </label>
											<input type="radio" name="sortBy" id="sortBy2" value="desc"> <label for="sortBy2"><?=$list_desc?> </label>
										</td>
									</tr>
									<tr>
										<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
										<td>&nbsp;</td>
									</tr>
								</table>
								</td>
							</tr>
							<tr>
								<td height="1" class="dotline"><img src="/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
							</tr>
							<tr>
								<td align="right">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="center">
												<?= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Report, "submit", "document.form1.action='report_view.php'", "submitBtn01")?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
			            </td>
			          </tr>
			        </table>
		        </td>
		    </tr>
			</table><br>
<input type="hidden" name="studentFlag" id="studentFlag" value="0">			
<input type="hidden" name="submit_flag" id="submit_flag" value="YES" />

</form>

<script>
<!--
changeTerm('<?=$SchoolYear?>');
SelectAllItem(document.getElementById('target[]'), true)
//-->
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
