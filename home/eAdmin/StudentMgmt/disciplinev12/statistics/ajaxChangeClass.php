<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lclass = new libclass();

$levels = $lclass->getLevelArray();
$classes = $ldiscipline->getRankClassList("",$year);


if($selectedTarget!="")
	$targetArr = explode(',',$selectedTarget);


$select_list = "<SELECT name=\"target[]\" id=\"target[]\" MULTIPLE SIZE=\"5\">";

if ($level==1)
{
    for ($i=0; $i<sizeof($levels); $i++)
    {
         list($id,$name) = $levels[$i];

                //$selected_tag = (is_array($target) && in_array($id,$target) )?"SELECTED":"";
                $selected_tag = (is_array($targetArr) && in_array($id,$targetArr) )?"SELECTED":"";
                $select_list .= "<OPTION value='".$id."' $selected_tag>".$name."</OPTION>";
    }
}
else
{
    for ($i=0; $i<sizeof($classes); $i++)
    {
         //list($id, $name, $lvl) = $classes[$i];
         list($name, $id) = $classes[$i];

                //$selected_tag = ( is_array($target) && in_array($id,$target) )?"SELECTED":"";
                $selected_tag = ( is_array($targetArr) && in_array($id,$targetArr) )?"SELECTED":"";
                $select_list .= "<OPTION value='".$id."' $selected_tag>". $name."</OPTION>";
    }
}
$select_list .= "</SELECT>";

echo $select_list;
//echo "<script language='javascript'>SelectAllItem(document.getElementById('target[]'),true);</script>";

intranet_closedb();

?>