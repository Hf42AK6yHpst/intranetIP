<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

//header("Content-Type:text/html;charset=utf-8");

$ldiscipline = new libdisciplinev12();
//$yearID = $ldiscipline->getAcademicYearIDByYearName($year);
$yearID = $year;
$result = getSemesters($yearID);


$temp = "<select name='$field' id='$field' class='formtextbox'>";
if($hideWholeYear!=1) {
	$temp .= "<option value='0'";
	$temp .= ($year=='0') ? " selected" : "";
	$temp .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";
}
if($year!=0 && sizeof($result)>0) {

	foreach($result as $termID=>$termName) {
		if($field=='semester1') {
			$selected = ($termName==$term1) ? " selected" : "";
		} else if($field=='semester2') {
			$selected = ($termName==$term2) ? " selected" : "";	
		} else {
			$selected = ($termID==$term) ? " selected" : "";	
		}
		
		$temp .= "<option value='$termID' $selected>$termName</option>";
	}
}
	
$temp .= "</select>";

echo $temp;

intranet_closedb();

?>