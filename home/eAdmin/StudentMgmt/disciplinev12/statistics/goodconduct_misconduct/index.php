<?php
// Modifying by : 

###########################################################
#
#	Date:	2016-04-15	Bill	[DM#2968]
#			fixed: cannot display stack bar chart, use open-flash-chart-develop.swf for stack bar chart
#
###########################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/flashchart_basic/php-ofc-library/open-flash-chart.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$ldiscipline->CONTROL_ACCESS("Discipline-STAT-GoodConduct_Misconduct-View");

$CurrentPage = "Statistics_GoodConductMisconduct";

$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($eDiscipline['Good_Conduct_and_Misconduct_Statistics'], "", 1);

$lclass = new libclass();
$linterface = new interface_html();

# School year
$selectYear = ($selectYear == '') ? Get_Current_Academic_Year_ID() : $selectYear;
$years = $ldiscipline->returnAllYearsSelectionArray();
$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='selectYear' id='selectYear' onclick='changeRadioSelection(\"YEAR\")' onChange='changeTerm(this.value);changeClassForm(this.value)'", "", $selectYear);
/*
# School year
$selectYear = ($selectYear == '') ? Get_Current_Academic_Year_ID() : $selectYear;
$selectSchoolYearHTML = "<select name=\"selectYear\" id=\"selectYear\" onclick=\"changeRadioSelection('YEAR')\" onChange=\"changeTerm(this.value);changeClassForm(this.value)\">";
*/

/*
$selectSchoolYearHTML .= "<option value=\"$i_Discipline_System_Award_Punishment_All_School_Year\"";
$selectSchoolYearHTML .= ($selectYear!=$i_Discipline_System_Award_Punishment_All_School_Year) ? "" : " selected";
$selectSchoolYearHTML .= ">".$i_Discipline_System_Award_Punishment_All_School_Year."</option>";
$selectSchoolYearHTML .= $ldiscipline->getConductSchoolYear($selectYear);
$selectSchoolYearHTML .= "</select>";
*/

# Semester
$ConductMarkCalculationMethod = $ldiscipline->retriveConductMarkCalculationMethod();
$currentSemester = getCurrentSemester();
$selectSemesterHTML = "<select name=\"selectSemester\" id=\"selectSemester\" onclick=\"changeRadioSelection('YEAR')\">";
if($ConductMarkCalculationMethod!=1)
	$selectSemesterHTML .= "<option value=\"$i_Discipline_System_Award_Punishment_Whole_Year\"";
$selectSemesterHTML .= ($selectSemester!=$i_Discipline_System_Award_Punishment_Whole_Year) ? "" : " selected";
$selectSemesterHTML .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";
$selectSemesterHTML .= "</select>";

# Form
$ClassLvlArr = $lclass->getLevelArray();
$SelectedFormArr = explode(",", $SelectedForm);
$SelectedFormTextArr = explode("\',\'", substr($SelectedFormText,2,-2));
$selectFormHTML = "<select name=\"selectForm[]\" id=\"selectForm[]\" multiple size=\"5\">\n";
for ($i = 0; $i < sizeof($ClassLvlArr); $i++) {
	$selectFormHTML .= "<option value=\"".$ClassLvlArr[$i][0]."\"";
	if ($_POST["submit_flag"] != "YES" || in_array($ClassLvlArr[$i][0], $SelectedFormArr)) {
		$selectFormHTML .= " selected";
	}
	$selectFormHTML .= ">".$ClassLvlArr[$i][1]."</option>\n";
}
$selectFormHTML .= "</select>\n";

# Class
$ClassListArr = $lclass->getClassList();
$SelectedClassArr = explode(",", $SelectedClass);
$SelectedClassTextArr = explode("\',\'", substr($SelectedClassText,2,-2));
$selectClassHTML = "<select name=\"selectClass[]\" id=\"selectClass[]\" multiple size=\"5\">\n";
for ($i = 0; $i < sizeof($ClassListArr); $i++) {
	$selectClassHTML .= "<option value=\"".$ClassListArr[$i][0]."\"";
	if ($_POST["submit_flag"] != "YES" || in_array($ClassListArr[$i][0], $SelectedClassArr)) {
		$selectClassHTML .= " selected";
	}
	$selectClassHTML .= ">".$ClassListArr[$i][1]."</option>\n";
}
$selectClassHTML .= "</select>\n";

# Get Category selection
$good_cats = $ldiscipline->RETRIEVE_CONDUCT_CATEGORY(1);
$mis_cats = $ldiscipline->RETRIEVE_CONDUCT_CATEGORY(-1);
$items = $ldiscipline->RETRIEVE_CONDUCT_ITEM_GROUPBY_CAT('',1);
$good_items = $ldiscipline->RETRIEVE_CONDUCT_ITEM_GROUPBY_CAT(1,0);
$mis_items = $ldiscipline->RETRIEVE_CONDUCT_ITEM_GROUPBY_CAT(-1,1);


$all_item = array();

$categoryID = ($record_type==1) ? $selectGoodCat : $selectMisCat;

if($radioType=="GM_TITLES") {
	if($categoryID==1) {
		$all_item[0] = 0;	
	} else {
		$all_item = $ldiscipline->RETRIEVE_ALL_CONDUCT_ITEM($categoryID,'id', $record_type);
	}
}

if($categoryID==1) {	 # "Late"
	$all_item[0] = "0";	
}

# Good Conduct Category
$selectGoodCatHTML = "<select name=\"selectGoodCat\" id=\"selectGoodCat\" onChange=\"changeCat(this.value);\">\n";
$selectGoodCatHTML .= "<option value=\"0\"";
$selectGoodCatHTML .= ($_POST["submit_flag"] != "YES")?" selected":"";
$selectGoodCatHTML .= ">-- ".$i_Discipline_System_Reports_All_Good_Conducts." --</option>";
for ($i = 0; $i < sizeof($good_cats); $i++) {
	$selectGoodCatHTML .= "<option value=\"".$good_cats[$i][0]."\"";
	if ($selectGoodCat == $good_cats[$i][0]) {
		$selectGoodCatHTML .= " selected";
	}
	$selectGoodCatHTML .= ">".$good_cats[$i][1]."</option>\n";
}
$selectGoodCatHTML .= "</select>\n";

# Misconduct Category
$selectMisCatHTML = "<select name=\"selectMisCat\" id=\"selectMisCat\" onChange=\"changeCat(this.value);\">\n";
$selectMisCatHTML .= "<option value=\"0\"";
$selectMisCatHTML .= ($_POST["submit_flag"] != "YES")?" selected":"";
$selectMisCatHTML .= ">-- $i_Discipline_System_Reports_All_Misconduct --</option>";
for ($i = 0; $i < sizeof($mis_cats); $i++) {
	$selectMisCatHTML .= "<option value=\"".$mis_cats[$i][0]."\"";
	if ($selectMisCat == $mis_cats[$i][0]) {
		$selectMisCatHTML .= " selected";
	}
	$selectMisCatHTML .= ">".$mis_cats[$i][1]."</option>\n";
}
$selectMisCatHTML .= "</select>\n";

// Items
if($record_type==1) {
	$selectedItem = $good_items;	
} else {
	$selectedItem = $mis_items;	
}


$SelectedItemIDArr = explode(",", $SelectedItemID);
$SelectedItemIDTextArr = explode(",", str_replace("\',\'",",",(substr($SelectedItemIDText,2,-2))));

$selectItemIDHTML = "<select name=\"ItemID[]\" id=\"ItemID[]\" multiple size=\"5\">\n";
if ($_POST["submit_flag"] == "YES") {
	for ($i = 0; $i < sizeof($selectedItem); $i++) {
		list($r_catID, $r_itemID, $r_itemName) = $selectedItem[$i];
		if (($record_type == 1 && $selectGoodCat == $r_catID) || ($record_type == -1 && $selectMisCat == $r_catID) || ($selectGoodCat==0 && $selectMisCat==0)) {
			$selectItemIDHTML .= "<option value=\"".$r_itemID."\"";
			if (in_array($r_itemID, $SelectedItemIDArr)) {
				$selectItemIDHTML .= " selected";
			}
			$selectItemIDHTML .= ">".$r_itemName."</option>\n";
		}
	}
}
$selectItemIDHTML .= "</select>\n";


# Submit from form
if ($_POST["submit_flag"] == "YES") {
	$optionString = "<td id=\"tdOption\"><span id=\"spanShowOption\"><a href=\"javascript:showOption();\" class=\"contenttool\"><img src=\"$image_path/$LAYOUT_SKIN/icon_show_option.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">$i_Discipline_Show_Statistics_Option</a></span>";
	$optionString .= "<span id=\"spanHideOption\" style=\"display:none\"><a href=\"javascript:hideOption();\" class=\"contenttool\"><img src=\"$image_path/$LAYOUT_SKIN/icon_hide_option.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">$i_Discipline_Hide_Statistics_Option</a></span></td>";

	if ($Period == "YEAR") {
		$parPeriodField1 = $selectYear;
		$parPeriodField2 = $selectSemester;
	} else {
		$parPeriodField1 = $textFromDate;
		$parPeriodField2 = $textToDate;
	}

	if ($selectBy == "FORM") {
		$parByFieldArr = $SelectedFormTextArr;
	} else {
		$parByFieldArr = $SelectedClassTextArr;
	}

	if ($StatType == "NO_OF_GM") {
		$parStatsFieldArr = array();
		$parStatsFieldTextArr = array($i_Discipline_Misconduct, $i_Discipline_GoodConduct);
	} else {
		if($categoryID==1) {	# "Late"
			$parStatsFieldArr[0] = 0;
			$parStatsFieldTextArr[0] = 'Late';
		} else if($categoryID==2) {	# "Homework not submitted"
			$parStatsFieldArr = $SelectedItemIDArr;
			$parStatsFieldTextArr = $SelectedItemIDTextArr;
			
		} else {
			$parStatsFieldArr = $SelectedItemIDArr;
			$parStatsFieldTextArr = $SelectedItemIDTextArr;
		}
		$tempParStatsFieldTextArr = intranet_htmlspecialchars(stripslashes(implode(',',$parStatsFieldTextArr)));
		$parStatsFieldTextArr = explode(',', $tempParStatsFieldTextArr);
	}

	if($categoryID!=1 && $categoryID!=2) { 	
		$all_item_name = $ldiscipline->RETRIEVE_ALL_CONDUCT_ITEM($categoryID, 'name', $record_type);
	}
	//debug_pr($all_item_name);
	//debug_pr($parStatsFieldTextArr);
	$chartDataArr = $ldiscipline->getGoodConductMisconductStatisticsAll($Period, $parPeriodField1, $parPeriodField2, $selectBy, $parByFieldArr, $StatType, $parStatsFieldArr, $parStatsFieldTextArr, $selectShowOnlyType, $selectShowOnlyNum, $all_item, $categoryID);
//debug_pr($chartDataArr);
##############################################################################################################################
############################################### flash chart ##################################################################
##############################################################################################################################
# flash chart

$colourArr = array('#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414');

$chart = new open_flash_chart();

$tooltip = new tooltip();
$tooltip->set_hover();
$chart->set_tooltip($tooltip);

$key = new key_legend();
if($display=="horizontal") {
	$key->set_selectable(true);
}
// [DM#2968] set not selectable key for stack bar chart
else {
	$key->set_selectable(false);
}

$maxValue = 0;
$tempMax = array();
$i = 0;

# Item-Colour Mapping - $itemNameColourMappingArr[#ItemName] = #colour
$itemNameColourMappingArr = array();

if($categoryID==1 || $categoryID==2) {
	$all_item_name = $parStatsFieldTextArr;	
}
if($StatType == "NO_OF_GM") {
	sort($parStatsFieldTextArr);
	$SelectedItemID = array(1,0);
	
	foreach ($parStatsFieldTextArr as $statsValue) 	# selected ItemID	-- original
	{
		$statsUnicodeValue = stripslashes($statsValue);
		$valueArr = array();
		for($j=0;$j<sizeof($valueArr);$j++) {
			$valueArr[$j] = "";
		}
		
		if($display=="horizontal")
			$bar = new bar_glass();
		else 
			$bar = new bar_stack_group();
			
		if($StatType != "GM_TITLES") {
			$bar->set_on_click('expandSubLevel'); 
		} else {
			//$bar->set_expandable( false );
		}
		$bar->set_colour("$colourArr[$i]");
		# remember the column colour for printing
		$itemNameColourMappingArr[$statsValue] = $colourArr[$i];
		
		$j = 0;
		foreach ($parByFieldArr as $byValue) {		# selected class/form
			$byValue = stripslashes($byValue);
			$tmpValue = (int)$chartDataArr[$byValue][$statsValue];
			
			$valueArr[] = $tmpValue;
			if($display=="horizontal") {
				if ($tmpValue > $maxValue) $maxValue = $tmpValue;
			}
			else {
				$tempMax[$byValue] += $tmpValue;
			}
								
			$bar->set_values($valueArr);
			$j++;		
		}
		
		$bar->set_tooltip(intranet_htmlspecialchars($statsUnicodeValue).":#val#");	# x-axis item 
		$bar->set_key("$statsUnicodeValue", "12");
		$bar->set_id( $SelectedItemID[$i] );
		$flag = 0;
		
		$bar->set_visible( true );
		$chart->add_element($bar);
		$i++;
	}	
		
} else {

	foreach ($parStatsFieldTextArr as $statsValue) 	# selected ItemID	-- original
	{
		$statsUnicodeValue = intranet_undo_htmlspecialchars(stripslashes($statsValue));
		//$statsUnicodeValue = intranet_htmlspecialchars(stripslashes($statsValue));
		$statsValue = stripslashes($statsValue);

		$valueArr = array();
		for($j=0;$j<sizeof($valueArr);$j++) {
			$valueArr[$j] = "";
		}
		
		if($display=="horizontal")
			$bar = new bar_glass();
		else 
			$bar = new bar_stack_group();
			
		$bar->set_colour("$colourArr[$i]");
		# remember the column colour for printing
		$itemNameColourMappingArr[$statsValue] = $colourArr[$i];
		
		$j = 0;
		foreach ($parByFieldArr as $byValue) {		# selected class/form
		
			$byValue = stripslashes($byValue);
			//$tmpValue = (int)$chartDataArr[$byValue][$statsValue];
			
			$tmpValue = (int)$chartDataArr[$byValue][intranet_htmlspecialchars($statsUnicodeValue)];
			
			$valueArr[] = $tmpValue;
			if($display=="horizontal") {
				if ($tmpValue > $maxValue) $maxValue = $tmpValue;
			}
			else 
				$tempMax[$byValue] += $tmpValue;
				
			$bar->set_values($valueArr);
			$j++;
		}
		
		$bar->set_tooltip(intranet_htmlspecialchars($statsUnicodeValue).":#val#");	# x-axis item 
		$bar->set_key("$statsUnicodeValue", "12");
		$bar->set_id( $parStatsFieldArr[$i] );
		$flag = 0;
		
		if($categoryID==1 || $categoryID==2) {
			$bar->set_visible( true );
		} else {
			foreach ($parStatsFieldTextArr as $statsValue2) {
				$statsValue2 = stripslashes($statsValue2);				
				if($flag==0) {
					if($statsValue==$statsValue2) {
						$bar->set_visible( true );
						$flag = 1;
					} else {
						$bar->set_visible( false );
					}
				}
			}
		}
		$chart->add_element($bar);
		$i++;
	}

}
if($display=="stack") {	
	foreach($tempMax as $val) {
		if ($val > $maxValue) $maxValue = $val;
	}	
}		

if ($Period == "YEAR") {
	$titleString = $ldiscipline->getAcademicYearNameByYearID($selectYear)." ";
	$titleString .= ($selectSemester==0) ? $i_Discipline_System_Award_Punishment_Whole_Year." " : $ldiscipline->getTermNameByTermID($selectSemester)." ";
} else {
	$titleString = $i_From." ".$textFromDate." ".$i_To." ".$textToDate." ";
}
$titleString .= $eDiscipline['Good_Conduct_and_Misconduct_Statistics'];
$title = new title($titleString);
$title->set_style( "{font-size: 16px; font-family: _sans; color: #333333; font-weight:bold; text-align:center;}" );

if ($selectBy=="FORM") {
	$x_legend = new x_legend($i_Discipline_Form);
} else if ($selectBy=="CLASS") {
	$x_legend = new x_legend($i_Discipline_Class);
}
$x_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );

$x = new x_axis();
$x->set_stroke( 2 );
$x->set_tick_height( 2 );
$x->set_colour( '#999999' );
$x->set_grid_colour( '#CCCCCC' );
$parByFieldArr = explode(',',stripslashes(implode(',',$parByFieldArr)));	# remove the slashes in the forms / classes
$x->set_labels_from_array($parByFieldArr);

$y_legend = new y_legend($i_Discipline_Quantity);
$y_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );

$y = new y_axis();
$y->set_stroke( 2 );
$y->set_tick_length( 2 );
$y->set_colour( '#999999' );
$y->set_grid_colour( '#CCCCCC' );

// $maxY = 5, 10, 20, 30...
if ($maxValue <= 5) {
	$maxY = 5;
} else {
	if (substr($maxValue, strlen($maxValue)-1, 1) == "0") {		// Last digit = 0
		$maxY = $maxValue;
	} else {
		$maxY = (substr($maxValue, 0, strlen($maxValue)-1) + 1)."0";
	}
}

$y->set_range( 0, $maxY, $maxY / 5 );
$y->set_offset(false);

$chart->set_bg_colour( '#FFFFFF' );
$chart->set_title( $title );
$chart->set_x_legend( $x_legend );
$chart->set_x_axis( $x );
$chart->set_y_legend( $y_legend );
$chart->set_y_axis( $y );
$chart->set_key_legend( $key );

##############################################################################################################################
############################################ END - flash chart ###############################################################
##############################################################################################################################

$height = 350;

# Adjust the width of flash depends on the no. of class/form
$fixedWidthForAcceptableItem = 5;
if($StatType == "NO_OF_GM") {
	$width = 850;
} else {
	$width = 850 + ((sizeof($parByFieldArr)+sizeof($parStatsFieldTextArr))-$fixedWidthForAcceptableItem) * 5;	
}

	$chartContent = "<table width=\"88%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" class=\"result_box\">";
	$chartContent .= "<tr><td align=\"center\">";

	$chartContent .= "<!--\n";
	$chartContent .= "##############################################################################################################################\n";
	$chartContent .= "############################################### flash chart ##################################################################\n";
	$chartContent .= "##############################################################################################################################\n";
	$chartContent .= "-->\n";
	$chartContent .= "<div id=\"my_chart\">no flash?</div>\n";
	$chartContent .= "\n";
	$chartContent .= "<script type=\"text/javascript\" src=\"".$PATH_WRT_ROOT."includes/flashchart_basic/js/json/json2.js\"></script>\n";
	$chartContent .= "<script type=\"text/javascript\" src=\"".$PATH_WRT_ROOT."includes/flashchart_basic/js/swfobject.js\"></script>\n";
	$chartContent .= "<script type=\"text/javascript\">\n";

	if($display=="horizontal") {
		$chartContent .= "	swfobject.embedSWF(\"".$PATH_WRT_ROOT."includes/flashchart_basic/open-flash-chart.swf\", \"my_chart\", \"".$width."\", \"".$height."\", \"9.0.0\", \"expressInstall.swf\");\n";
	}
	// [DM#2968] use open-flash-chart-develop.swf as open-flash-chart.swf not work for stack bar chart
	else {
		$chartContent .= "	swfobject.embedSWF(\"".$PATH_WRT_ROOT."includes/flashchart_basic/open-flash-chart-develop.swf\", \"my_chart\", \"".$width."\", \"".($height+150)."\", \"9.0.0\", \"expressInstall.swf\");\n";
	}
	
	$chartContent .= "</script>\n";
	$chartContent .= "\n";
	$chartContent .= "<script type=\"text/javascript\">\n";
	$chartContent .= "function ofc_ready(){\n";
	$chartContent .= "	//alert('ofc_ready');\n";
	$chartContent .= "}\n";
	$chartContent .= "\n";
	$chartContent .= "function open_flash_chart_data(){\n";
	$chartContent .= "	//alert( 'reading data' );\n";
	$chartContent .= "	return JSON.stringify(data);\n";
	$chartContent .= "}\n";
	$chartContent .= "\n";
	$chartContent .= "function findSWF(movieName) {\n";
	$chartContent .= "  if (navigator.appName.indexOf(\"Microsoft\")!= -1) {\n";
	$chartContent .= "	return window[movieName];\n";
	$chartContent .= "  } else {\n";
	$chartContent .= "	return document[movieName];\n";
	$chartContent .= "  }\n";
	$chartContent .= "}\n";
	$chartContent .= "\n";
	
	$chartContent .= "function expandSubLevel(id){\n";
	$chartContent .= "var popup = document.getElementById(\"testPopup\");\n";
	$chartContent .= "//alert(id);\n";
	$chartContent .= "while(popup.hasChildNodes()){\n";
	$chartContent .= "popup.removeChild(popup.firstChild);\n";
	$chartContent .= "}\n";
	$chartContent .= "//var p = document.createTextNode(String(id));\n";
	$chartContent .= "//popup.appendChild(p);\n";
	$chartContent .= "popupDetail(id);\n";
	$chartContent .= "}\n";
	$chartContent .= "\n";
	
	$chartContent .= "function popupDetail(id) {\n";
	$chartContent .= "document.frmStats.id.value = id;";
	$chartContent .= "document.frmStats.action = 'popupDetail.php';";
	$chartContent .= "document.frmStats.target = '_blank';";
	$chartContent .= "document.frmStats.submit();";
	$chartContent .= "document.frmStats.action = '';";
	$chartContent .= "document.frmStats.target = '';";
	$chartContent .= "}\n\n";

	$chartContent .= "var itemID = new Array(); var newItemID = new Array();\n";
	if($StatType=="NO_OF_GM") {
		$item = "";
		for($i=0;$i<sizeof($parStatsFieldTextArr);$i++) {
			if($parStatsFieldTextArr[$i] == $i_Discipline_Misconduct) {
				$item .= ($item=="") ? "0" : ",0";
			} else if($parStatsFieldTextArr[$i] == $i_Discipline_GoodConduct) {
				$item .= ($item=="") ? "1" : ",1";
			}
		}
		$chartContent .= "itemID = [".$item."];\n\n";	
	} else {
		$chartContent .= "itemID = [".implode(",", $parStatsFieldArr)."];\n\n";	
	}
	$chartContent .= "function setChart(id, display) {\n";
	$chartContent .= "var j = 0;\n";
	$chartContent .= "var itemIDList = \"\";\n";
	$chartContent .= "newItemID = [];\n";	# initiate newItemID[]
	$chartContent .= "for(var i=0;i<itemID.length;i++) {\n";
	$chartContent .= "	if(display==true) {\n";
	$chartContent .= "		if(i==0) { newItemID[j] = id;\n j++;\n}\n";
	$chartContent .= "		newItemID[j] = itemID[i];\n j++;\n";			
	$chartContent .= "	} else {\n";
	$chartContent .= "		if(id != itemID[i])	{\n";
	$chartContent .= "			newItemID[j] = itemID[i];\n j++;\n";
	$chartContent .= "		}\n";
	$chartContent .= "	}\n";
	$chartContent .= "}\n";
	$chartContent .= "if(itemID.length==0){newItemID[j] = id;\n j++;\n} \n";	
	$chartContent .= "itemID = [];\n";
	$chartContent .= "for(i=0;i<newItemID.length;i++) {\n";
	$chartContent .= "	//if(newItemID[i] != \"\") {\n";
	$chartContent .= "		itemID[i] = newItemID[i];\n";
	$chartContent .= "		itemIDList += (itemIDList != \"\") ? \",\" + newItemID[i] : newItemID[i];\n";
	$chartContent .= "	/*}*/}\n";
	$chartContent .= "document.getElementById('newItemID').value = itemIDList;\n";
	
	$chartContent .= "}\n";
	
	$chartContent .= "\n";
	$chartContent .= "var data = ".$chart->toPrettyString().";\n";
	$chartContent .= "</script>\n";
	$chartContent .= "<!--\n";
	$chartContent .= "##############################################################################################################################\n";
	$chartContent .= "############################################ END - flash chart ###############################################################\n";
	$chartContent .= "##############################################################################################################################\n";
	$chartContent .= "-->\n";

	$chartContent .= "</td></tr>";
	$chartContent .= "<tr><td height=\"1\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\"></td></tr>";
	$chartContent .= "<tr><td align=\"right\">";
	$chartContent .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
	$chartContent .= "<tr><td align=\"center\">";
	$chartContent .= $linterface->GET_ACTION_BTN($button_export, "button", "exportStats();")."&nbsp;";
	$chartContent .= $linterface->GET_ACTION_BTN($button_print, "button", "printStats();");
	$chartContent .= "</td></tr>";
	$chartContent .= "</table>";
	$chartContent .= "</td></tr>";
	$chartContent .= "</table>";

	$initialString = "<script language=\"javascript\">hideOption();</script>\n";
} else {
	$optionString = "<td height=\"20\" class=\"tabletextremark\"><em>- $i_Discipline_Statistics_Options -</em></td>";
	$initialString = "<script language=\"javascript\">jPeriod='YEAR';jStatType='NO_OF_GM';</script>\n";
}

$linterface->LAYOUT_START();


?>

<script language="javascript">
var jPeriod="<?=$Period?>";
var jStatType="<?=$StatType?>";

// Generate Good Conduct Category
var good_cat_select = new Array();
good_cat_select[0] = new Option("-- <?=$eDiscipline["SelectRecordCategory"]?> --",0);
<?	foreach($good_cats as $k=>$d)	{ ?>
		good_cat_select[<?=$k+1?>] = new Option("<?=str_replace('"', '\"', $d['Name'])?>",<?=$d['CategoryID']?>);
<?	} ?>

// Generate Misconduct Category
var mis_cat_select = new Array();
mis_cat_select[0] = new Option("-- <?=$eDiscipline["SelectRecordCategory"]?> --",0);
<?	foreach($mis_cats as $k=>$d)	{ ?>
		mis_cat_select[<?=$k+1?>] = new Option("<?=str_replace('"', '\"', $d['Name'])?>",<?=$d['CategoryID']?>);
<?	} ?>

function changeCat(value)
{
	var item_select = document.getElementById('ItemID[]');
	var selectedCatID = value;
	var record_type_good = document.getElementById('record_type_good');

	while (item_select.options.length > 0)
	{
		item_select.options[0] = null;
	}
	
	if (selectedCatID == 0) {
	<?
		$curr_cat_id = "";
	?>
		if(document.frmStats.record_type_good.checked) {
	<?
		$pos = 0;
			for ($i=0; $i<sizeof($good_items); $i++)
			{
				list($r_catID, $r_itemID, $r_itemName) = $good_items[$i];
				$r_itemName = intranet_undo_htmlspecialchars($r_itemName);
				$r_itemName = str_replace('"', '\"', $r_itemName);
				$r_itemName = str_replace("'", "\'", $r_itemName);
				//$r_itemName = str_replace('"', '&quot;', $r_itemName);
				echo "item_select.options[".$pos++."] = new Option(\"".$r_itemName."\",".$r_itemID.");\n";
			}
	?>	
		} else {
	<?
			$pos = 0;
			for ($i=0; $i<sizeof($mis_items); $i++)
			{
				list($r_catID, $r_itemID, $r_itemName) = $mis_items[$i];
				$r_itemName = intranet_undo_htmlspecialchars($r_itemName);
				$r_itemName = str_replace('"', '\"', $r_itemName);
				$r_itemName = str_replace("'", "\'", $r_itemName);
				echo "item_select.options[".$pos++."] = new Option(\"".$r_itemName."\",".$r_itemID.");\n";
			}
	?>	
		}
	}
	else if (selectedCatID == '')
	{

	<? 
		$curr_cat_id = "";
		$pos = 0;
		for ($i=0; $i<sizeof($items); $i++)
		{
			list($r_catID, $r_itemID, $r_itemName) = $items[$i];
			$r_itemName = intranet_undo_htmlspecialchars($r_itemName);
			$r_itemName = str_replace('"', '\"', $r_itemName);
			$r_itemName = str_replace("'", "\'", $r_itemName);
			if ($r_catID != $curr_cat_id)
			{
				$pos = 0;
	?>
	}
	else if (selectedCatID == "<?=$r_catID?>")
	{
	<?
				$curr_cat_id = $r_catID;
			}
	?>
		item_select.options[<?=$pos++?>] = new Option("<?=$r_itemName?>",<?=$r_itemID?>);
		<?	if($r_itemID == $ItemID) {	?>
		item_select.selectedIndex = <?=$pos-1?>;
		<? 	} 
		}
	?>
	}
	SelectAll(frmStats.elements['ItemID[]']);
}


function showSpan(span){
	document.getElementById(span).style.display="inline";
}

function hideSpan(span){
	document.getElementById(span).style.display="none";
}

function showOption(){
	hideSpan('spanShowOption');
	showSpan('spanHideOption');
	showSpan('spanOptionContent');
	document.getElementById('tdOption').className='';
}

function hideOption(){
	hideSpan('spanOptionContent');
	hideSpan('spanHideOption');
	showSpan('spanShowOption');
	document.getElementById('tdOption').className='report_show_option';
}

function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function getSelectedString(obj, targetObj){
	var tmpString = '';
	for (i=0; i<obj.length; i++){
		if (obj.options[i].selected == true){
			tmpString += obj.options[i].value + ',';
		}
	}
	if (tmpString.length > 0){
		document.getElementById(targetObj).value = tmpString.substr(0, tmpString.length-1);
	} else {
		document.getElementById(targetObj).value = tmpString;
	}
}

function getSelectedTextString(obj, targetObj){
	var tmpString = '';
	for (i=0; i<obj.length; i++){
		if (obj.options[i].selected == true){
			tmpString += obj.options[i].text + "','";
		}
	}
	
	if (tmpString.length > 0){
		document.getElementById(targetObj).value = "'"+tmpString.substr(0, tmpString.length-3)+"'";
	} else {
		document.getElementById(targetObj).value = "'"+tmpString+"'";
	}
}

function exportStats(){
	if(document.getElementById('newItemID').value != '') {
		document.frmStats.action = "index_export.php";
		document.frmStats.submit();
		document.frmStats.action = "";
	} else {
		alert('<?=$msg_check_at_least_one_item?>');	
	}
}

function printStats(){
	if(document.getElementById('newItemID').value != '') {
		document.frmStats.action = "index_print.php";
		document.frmStats.target = "_blank";
		document.frmStats.submit();
		document.frmStats.action = "";
		document.frmStats.target = "_self";
	} else {
		alert('<?=$msg_check_at_least_one_item?>');	
	}
}

function changeRadioSelection(type)
{
	var jsYearRadioObj = document.getElementById("radioPeriod_Year");
	var jsDateRadioObj = document.getElementById("radioPeriod_Date");
	
	if (type == "YEAR")
	{
		jsYearRadioObj.checked = true;
		jsDateRadioObj.checked = false;
		jPeriod = type;
	}
	else
	{
		jsYearRadioObj.checked = false;
		jsDateRadioObj.checked = true;
		jPeriod = type;
	}
}

function checkForm(){
	if (jPeriod=='DATE' && document.getElementById('textFromDate').value=='') {
		alert('<?=$i_alert_pleasefillin.$i_general_startdate?>');
		return false;
	}
	if (jPeriod=='DATE' && document.getElementById('textToDate').value=='') {
		alert('<?=$i_alert_pleasefillin.$i_general_enddate?>');
		return false;
	}
	if(jPeriod=='DATE' && !check_date(document.getElementById('textFromDate'), '<?=$i_invalid_date?>')) {
		return false;
	}
	if(jPeriod=='DATE' && !check_date(document.getElementById('textToDate'), '<?=$i_invalid_date?>')) {
		return false;
	}
	if(jPeriod=='DATE' && (document.frmStats.textFromDate.value > document.frmStats.textToDate.value)) {
		alert("<?=$i_invalid_date?>");
		return false;	
	}
	if (document.getElementById('selectBy').value=='FORM' && countOption(document.getElementById('target[]'))<1) {
		alert('<?=$i_alert_pleaseselect.$i_Discipline_Form?>');
		return false;
	}
	if (document.getElementById('selectBy').value=='CLASS' && countOption(document.getElementById('target[]'))<1) {
		alert('<?=$i_alert_pleaseselect.$i_Discipline_Class?>');
		return false;
	}
	if (document.getElementById('selectMisCat').value!=1 && document.getElementById('selectMisCat').value!=2 && jStatType=='GM_TITLES' && countOption(document.getElementById('ItemID[]'))<1) {
		alert('<?=$i_alert_pleaseselect.$i_Discipline_GoodConductMisconduct_Titles?>');
		return false;
	}
	document.getElementById('Period').value = jPeriod;
	document.getElementById('StatType').value = jStatType;
	/*
	getSelectedString(document.getElementById('selectForm[]'), 'SelectedForm');
	getSelectedTextString(document.getElementById('selectForm[]'), 'SelectedFormText');
	getSelectedString(document.getElementById('selectClass[]'), 'SelectedClass');
	getSelectedTextString(document.getElementById('selectClass[]'), 'SelectedClassText');
	*/
	getSelectedString(document.getElementById('target[]'), 'SelectedForm');
	getSelectedTextString(document.getElementById('target[]'), 'SelectedFormText');
	getSelectedString(document.getElementById('target[]'), 'SelectedClass');
	getSelectedTextString(document.getElementById('target[]'), 'SelectedClassText');
	getSelectedString(document.getElementById('ItemID[]'), 'SelectedItemID');
	getSelectedTextString(document.getElementById('ItemID[]'), 'SelectedItemIDText');
	document.getElementById('submit_flag').value='YES';
<? if ($_POST["submit_flag"] == "YES") {
	echo "document.getElementById('newItemID').value = newItemID;";
} ?>
	return true;
}
</script>
<script language="javascript">
<!--
var xmlHttp
var xmlHttp3

function changeTerm(val) {

	if (val.length==0)
	{ 
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
		
	xmlHttp3 = GetXmlHttpObject()
	
	if (xmlHttp3==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "../ajaxGetSemester.php";
	url += "?year=" + val;
	url += "&term=<?=$selectSemester?>";
	url += "&field=selectSemester";
	url += "&hideWholeYear=<?=$ConductMarkCalculationMethod?>";
	
	xmlHttp3.onreadystatechange = stateChanged3 
	xmlHttp3.open("GET",url,true)
	xmlHttp3.send(null)
} 

function stateChanged3() 
{ 
	if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete")
	{ 
		document.getElementById("spanSemester").innerHTML = xmlHttp3.responseText;
		document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
	} 
}

function changeClassForm(val) {

	levelValue = "";
	
	if (val.length==0)
	{ 
		document.getElementById("spanTarget").innerHTML = "";
		document.getElementById("spanTarget").style.border = "0px";
		return
	}
		
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	if(document.getElementById('selectBy').value=="FORM")
		levelValue = 1
	else 
		levelValue = 2;
	
		
	url = "../ajaxChangeClass.php";
	url += "?year=" + val;
	url += "&level=" + levelValue;
	url += "&selectedTarget="+document.getElementById('selectedTarget').value;
	
	xmlHttp.onreadystatechange = stateChanged 
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
	
} 

function stateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById("spanTarget").innerHTML = xmlHttp.responseText;
		document.getElementById("spanTarget").style.border = "0px solid #A5ACB2";
		//SelectAll(document.getElementById('target[]'));
	} 
}
//-->
</script>
<br />
<form name="frmStats" method="post" action="index.php">

<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr align="left">
					<td width="50">&nbsp;</td>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr align="left">
								<?=$optionString?>
							</tr>
						</table>
						<br />
						<span id="spanOptionContent">
						<table width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td height="57" width="10%" valign="top" nowrap="nowrap" class="formfieldtitle">
									<?=$iDiscipline['Period']?> <span class="tabletextrequire">*</span>
								</td>
								<td width="90%">
									<table border="0" cellspacing="0" cellpadding="3">
										<tr>
											<td height="30" colspan="6">
												<input name="radioPeriod" type="radio" id="radioPeriod_Year" value="YEAR" onClick="javascript:jPeriod=this.value"<? echo ($Period!="DATE")?" checked":"" ?>>
												<?=$i_Discipline_School_Year?>
												<?=$selectSchoolYearHTML?>&nbsp;
												<?=$i_Discipline_Semester?>
												<span id="spanSemester" onclick="changeRadioSelection('YEAR')"><?=$selectSemesterHTML?></span>
											</td>
										</tr>
										<tr>
											<td><input name="radioPeriod" type="radio" id="radioPeriod_Date" value="DATE" onClick="javascript:jPeriod=this.value"<? echo ($Period=="DATE")?" checked":"" ?>> <?=$i_From?> </td>
											<td onclick="changeRadioSelection('DATE')" onFocus="changeRadioSelection('DATE')"><?=$linterface->GET_DATE_PICKER("textFromDate",$textFromDate)?><?/*=$linterface->GET_DATE_FIELD2("textFromDateSpan", "frmStats", "textFromDate", $textFromDate, 1, "textFromDate", "onclick=\"changeRadioSelection('DATE')\"")*/?>&nbsp;</td>
											<td> <?=$i_To?> </td>
											<td onclick="changeRadioSelection('DATE')" onFocus="changeRadioSelection('DATE')"><?=$linterface->GET_DATE_PICKER("textToDate",$textToDate)?><?/*=$linterface->GET_DATE_FIELD2("textToDateSpan", "frmStats", "textToDate", $textToDate, 1, "textToDate", "onclick=\"changeRadioSelection('DATE')\"")*/?>&nbsp;</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr valign="top">
								<td valign="top" nowrap="nowrap" class="formfieldtitle">
									<?=$i_Discipline_Target?> <span class="tabletextrequire">*</span>
								</td>
								<td>
									<table width="0%" border="0" cellspacing="2" cellpadding="0">
										<tr>
											<td valign="top">
												<select name="selectBy" id="selectBy" onChange="changeClassForm(document.getElementById('selectYear').value)">
													<option value="FORM"<? echo ($selectBy!="CLASS")?" selected":"" ?>><?=$i_Discipline_Form?></option>
													<option value="CLASS"<? echo ($selectBy=="CLASS")?" selected":"" ?>><?=$i_Discipline_Class?></option>
												</select>
												<br />
												
												<span id="spanTarget" style="display:inline"></span>
												<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('target[]')); return false;") ?>
												<br />
												<span class="tabletextremark">(<?=$i_Discipline_Press_Ctrl_Key?>)</span>
											</td>
											<td valign="top"><br /></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle">
									<span><?=$eDiscipline["CompareShow"]?> <span class="tabletextrequire">*</span></span>
								</td>
								<td width="80%">
									<table width="0%" border="0" cellspacing="2" cellpadding="0">
										<tr>
											<td valign="top">
												<input name="radioType" type="radio" id="radioType1" value="NO_OF_GM" onClick="javascript:jStatType=this.value;hideSpan('spanGMTitles');showSpan('spanNoOfGM');"<? echo ($StatType!="GM_TITLES")?" checked":"" ?> /><label for="radioType1"><?=$i_Discipline_No_Of_GoodConductsMisconducts?></label>
												<input name="radioType" type="radio" id="radioType2" value="GM_TITLES" onClick="javascript:jStatType=this.value;hideSpan('spanNoOfGM');showSpan('spanGMTitles');changeCat('0');"<? echo ($StatType=="GM_TITLES")?" checked":"" ?> /><label for="radioType2"><?=$i_Discipline_GoodConductMisconduct_Titles?></label>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<span id="spanGMTitles"<? echo ($StatType=="GM_TITLES")?"":" style=\"display:none\"" ?>>
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td valign="top" nowrap="nowrap" class="formfieldtitle">
													<span><?=$iDiscipline['RecordType2']?> <span class="tabletextrequire">*</span></span>
												</td>
												<td width="85%">
													<table width="0%" border="0" cellspacing="2" cellpadding="0">
														<tr>
															<td valign="top">
																<span id="spanNoOfGM"<? echo ($StatType!="GM_TITLES")?"":" style=\"display:none\"" ?>>
																	<br />
																</span>
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td>
																			<input name="record_type" type="radio" id="record_type_good" value="1" <?=($record_type==1 || $record_type=="")?"checked":""?> onClick="hideSpan('spanMisCat');showSpan('spanGoodCat');changeCat('0');document.getElementById('selectGoodCat').value='0';"><label for="record_type_good"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ediscipline/icon_gd_conduct.gif" width="20" height="20" border="0" align="absmiddle"> <?=$i_Discipline_GoodConduct?></label>
																			<input name="record_type" type="radio" id="record_type_mis" value="-1" <?=($record_type==-1)?"checked":""?> onClick="hideSpan('spanGoodCat');showSpan('spanMisCat');changeCat('0');document.getElementById('selectMisCat').value='0';"><label for="record_type_mis"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ediscipline/icon_misconduct.gif" width="20" height="20" border="0" align="absmiddle"> <?=$i_Discipline_Misconduct?></label><br />
																			<span id="spanGoodCat"<? echo ($record_type==1 || $record_type=="")?"":" style=\"display:none\"" ?>>
																				<?=$selectGoodCatHTML?>
																			</span>
																			<span id="spanMisCat"<? echo ($record_type==-1)?"":" style=\"display:none\"" ?>>
																				<?=$selectMisCatHTML?>
																			</span>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<?=$selectItemIDHTML?>
																			<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['ItemID[]']); return false;") ?>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<span class="tabletextremark">(<?=$i_Discipline_Press_Ctrl_Key?>)</span>
																		</td>
																	</tr>																
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<input type="hidden" name="selectShowOnlyType" value="ALL">
										</table>
									</span>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap">
									<span class="formfieldtitle"><?=$i_general_display_format?> <span class="tabletextrequire">*</span></span>
								</td>
								<td>
									<input type="radio" name="display" id="horizontalDisplay" value="horizontal" <? if($display=="" || $display=="horizontal") {echo "checked";} ?>><label for="horizontalDisplay"><?=$iDiscipline['HorizontalDisplay']?></label>
									<input type="radio" name="display" id="stackDisplay" value="stack" <? if($display=="stack") {echo "checked";} ?>><label for="stackDisplay"><?=$iDiscipline['StackDisplay']?></label>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap">
									<span class="tabletextremark"><?=$i_general_required_field?></span>
								</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td height="1" class="dotline" colspan="2">
									<img src="<?="$image_path/$LAYOUT_SKIN"?>/10x10.gif" width="10" height="1"></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td><?= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Statistic, "submit", "document.frmStats.action='index.php';document.frmStats.target='_self';return checkForm();")?>
								</td>
							</tr>
						</table>
						</span>
					</td>
				</tr>
			</table>
			<br />
				<?=$chartContent?><span id="testPopup"></span>
			<br />
		</td>
	</tr>
</table>

<input type="hidden" name="submit_flag" id="submit_flag" value="NO" />
<input type="hidden" name="Period" id="Period" value="<?=$Period?>" />
<input type="hidden" name="submitSelectBy" id="submitSelectBy" value="<?=$selectBy?>" />
<input type="hidden" name="StatType" id="StatType" value="<?=$StatType?>" />
<input type="hidden" name="PeriodField1" id="PeriodField1" value="<?=$parPeriodField1?>" />
<input type="hidden" name="PeriodField2" id="PeriodField2" value="<?=$parPeriodField2?>" />
<? if($submit_flag=="YES") { ?>
<input type="hidden" name="ByField" id="ByField" value="<?=intranet_htmlspecialchars(implode(",", $parByFieldArr))?>" />
<input type="hidden" name="StatsField" id="StatsField" value="<?=implode(",", $parStatsFieldArr)?>" />
<input type="hidden" name="StatsFieldText" id="StatsFieldText" value="<?=intranet_htmlspecialchars(stripslashes(implode(",", $parStatsFieldTextArr)))?>" />
<? } ?>
<input type="hidden" name="SelectedForm" id="SelectedForm" />
<input type="hidden" name="SelectedFormText" id="SelectedFormText" />
<input type="hidden" name="SelectedClass" id="SelectedClass" />
<input type="hidden" name="SelectedClassText" id="SelectedClassText" />
<input type="hidden" name="SelectedItemID" id="SelectedItemID" />
<input type="hidden" name="SelectedItemIDText" id="SelectedItemIDText" />
<? if ($_POST["submit_flag"] == "YES") {
	if($categoryID!=1) {
		if($StatType=="NO_OF_GM") {
			echo "<input type=\"hidden\" name=\"newItemID\" id=\"newItemID\" value=\"".$item."\">";
		} else {
			echo "<input type=\"hidden\" name=\"newItemID\" id=\"newItemID\" value=\"".implode(",", $parStatsFieldArr)."\">";
		}
	} else if($categoryID==1){
		echo "<input type=\"hidden\" name=\"newItemID\" id=\"newItemID\" value=\"0\">";
	}
} 

?>
<input type="hidden" name="id" id="id" value="">
<input type="hidden" name="itemNameColourMappingArr" id="itemNameColourMappingArr" value="<?=rawurlencode(serialize($itemNameColourMappingArr));?>">


	<input type="hidden" name="selectedTarget" id="selectedTarget" value="<? if(sizeof($target)>0) { echo implode(',',$target); }?>">
</form>
<br />
<?=$initialString?>
<script language="javascript">
<!--
changeTerm('<?=$selectYear?>');
changeClassForm('<?=$selectYear?>');
//-->
</script>
<?
$linterface->LAYOUT_STOP();
?>
 