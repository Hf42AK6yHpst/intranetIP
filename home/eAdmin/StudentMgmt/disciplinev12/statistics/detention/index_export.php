<?php
// Modifying by : 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

// Temp Assign memory of this page
ini_set("memory_limit", "150M");

$ldiscipline = new libdisciplinev12();
$lexport = new libexporttext();

$ldiscipline->CONTROL_ACCESS("Discipline-STAT-Detention-View");

$filename = "detention_stats.csv";

$selectBy = $submitSelectBy;
$parPeriodField1 = $PeriodField1;
$parPeriodField2 = $PeriodField2;
$parByFieldArr = explode(",", $ByField);
$parStatsFieldArr = explode(",", $StatsField);

$tmpReasonIDArr = explode("|||", $newItemID);
$tmpDetentionReason = $ldiscipline->getAllDistinctDetentionReasonWithRecordID();

$k = 0;

for($i=0;$i<sizeof($tmpReasonIDArr);$i++) {
	for($j=0;$j<sizeof($tmpDetentionReason);$j++) {
		if($tmpReasonIDArr[$i]==$tmpDetentionReason[$j][1]) {
			$ReasonArr[$k] = $tmpDetentionReason[$j][0];
			//$ReasonArr[$k] = intranet_htmlspecialchars($tmpDetentionReason[$j][0]);
			//$ReasonArr[$k] = str_replace("\\\"","\"",$ReasonArr[$k]);	
			$k++;
			break;
		}
	}
}

// Data array for export

if(sizeof($ReasonArr) != 0) 
	$chartDataArr = $ldiscipline->getDetentionStatisticsAll($Period, $parPeriodField1, $parPeriodField2, $selectBy, $parByFieldArr, $StatType, $parStatsFieldArr, $ReasonArr);


$ExportArr = array();
$i = 0;
$ExportArr[$i][0] = $iDiscipline["Period"].":";
if ($Period == "YEAR") {
	$ExportArr[$i][1] = $ldiscipline->getAcademicYearNameByYearID($parPeriodField1)." (";
	$ExportArr[$i][1] .= ($parPeriodField2==0) ? $i_Discipline_System_Award_Punishment_Whole_Year : $ldiscipline->getTermNameByTermID($parPeriodField2);
	$ExportArr[$i][1] .= ")";
} else {
	$ExportArr[$i][1] = $i_From." ".$parPeriodField1." ".$i_To." ".$parPeriodField2;
}
$i++;
$ExportArr[$i][0] = $eDiscipline["Type"].":";
if ($StatType == "REASON") {
	$ExportArr[$i][1] = $i_Discipline_Reasons;
} else {
	$ExportArr[$i][1] = $i_Discipline_No_Of_Detentions;
}
$i++;
$ExportArr[$i][0] = "";		// a blank line
$i++;
if ($selectBy == "FORM") {
	$ExportArr[$i][0] = $i_Discipline_Form;
} else if ($selectBy == "CLASS") {
	$ExportArr[$i][0] = $i_Discipline_Class;
}
if ($StatType == "REASON") {
	$ExportArr[$i][1] = $i_Discipline_Reasons;
	$i++;
	$ExportArr[$i][0] = "";
//	for ($j=0; $j<sizeof($parStatsFieldArr); $j++) {
	for ($j=0; $j<sizeof($ReasonArr); $j++) {	
		$ExportArr[$i][$j+1] = $ReasonArr[$j];
	}
} else {
	$ExportArr[$i][1] = $i_Discipline_Quantity;
}
$i++;
if ($StatType != "REASON") {
	foreach ($parByFieldArr as $byValue) {
		$byValue = stripslashes($byValue);
		$k = 0;
		$ExportArr[$i][$k] = $byValue;
		$k++;
		$ExportArr[$i][$k] = $chartDataArr["$byValue"];
		$k++;
		$i++;
	}
} else if(sizeof($ReasonArr)!=0) {
	foreach ($parByFieldArr as $byValue) {
		$byValue = stripslashes($byValue);
		$k = 0;
		$ExportArr[$i][$k] = $byValue;
		$k++;
//		foreach ($parStatsFieldArr as $statsValue) {
		foreach ($ReasonArr as $statsValue) {
			$ExportArr[$i][$k] = $chartDataArr[$byValue][$statsValue];
			$k++;
		}
		$i++;
	}
}

$numOfColumn = sizeof($ReasonArr) + 1;
$export_content = $lexport->GET_EXPORT_TXT($ExportArr, "", "", "\r\n", "", $numOfColumn, "11");

intranet_closedb();

// Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);

?>
