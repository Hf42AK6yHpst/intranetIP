<?php
// Modifying by : 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

# Temp Assign memory of this page
ini_set("memory_limit", "150M");

$ldiscipline = new libdisciplinev12();
$lclass = new libclass();
$lexport = new libexporttext();

$filename = "conduct_grade.csv";

# School Year
if($Year=='')
{
	$Year = Get_Current_Academic_Year_ID();
}
$AcademicYearID = $Year;

# Semester 
if(!isset($Semester))
{
	$Semester = getCurrentSemesterID();
}

if(!isset($Semester))
{
	$Semester = $semArr[0][0];
}

$YearTermID = $Semester;
if($YearTermID=='' &&$YearTermID!='0') 
	$YearTermID = 0;
	
# Class	
$YearClassID = $lclass->getClassID($ClassID, $AcademicYearID);

# Conditions	
$extraCriteria = "";
$extraCriteria .= ($ClassID=='') ? "" : " AND ycu.YearClassID=$YearClassID";
$extraCriteria .= ($searchStr=='')? "" : " AND (iu.EnglishName like '%".trim($searchStr)."%' or iu.ChineseName like '%".trim($searchStr)."%' )";

$conds_year = ($AcademicYearID=='0')? "": " AND (conGrade.AcademicYearID=$AcademicYearID)";
$conds_semester = ($YearTermID=='0' || $YearTermID=='')? " AND (conGrade.IsAnnual = 1)" : " AND (conGrade.YearTermID = $YearTermID)";

$name_field = getNameFieldByLang("iu.");


# SQL statement
$sql = "SELECT 
			CONCAT(".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN').",' - ',ycu.ClassNumber) as ClassNameNum,
			$name_field as name,
			IF(GradeChar IS NULL, '--',GradeChar) as GradeChar
		FROM 
			INTRANET_USER iu
			LEFT JOIN DISCIPLINE_STUDENT_CONDUCT_GRADE conGrade ON (iu.UserID = conGrade.StudentID $conds_semester $conds_year)
			LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=iu.UserID)
			LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)
			LEFT OUTER JOIN YEAR y ON (y.YearID=yc.YearID)
		WHERE 1 $extraCriteria and iu.RecordType=2 AND iu.RecordStatus IN (0,1,2) AND yc.AcademicYearID=$AcademicYearID
		GROUP BY iu.UserID 
		ORDER BY y.Sequence, yc.Sequence, ycu.ClassNumber";
$result = $ldiscipline->returnArray($sql, 3);

$columnNo = array("classNameNumber", "name", "GradeChar");

# export content 
$ExportArr = array();
$i = 0;

$ExportArr[$i][0] = $ldiscipline->getAcademicYearNameByYearID($AcademicYearID)." ";
$ExportArr[$i][0] .= ($YearTermID=='0' || $YearTermID=='') ? $i_Discipline_System_Award_Punishment_Whole_Year : $ldiscipline->getTermNameByTermID($YearTermID);
$i++;

$ExportArr[$i][0] = $i_general_class." : ";
$ExportArr[$i][1] = ($ClassID=='') ? $i_general_all_classes : $ClassID;
$i++;

$ExportArr[$i][0] = "";
$i++;

$ExportArr[$i][0] = "#";
$ExportArr[$i][1] = $i_general_class;
$ExportArr[$i][2] = $i_general_name;
$ExportArr[$i][3] = $i_Discipline_System_Report_ClassReport_Grade;
$i++;


if(sizeof($result)==0) {
	$Exportarr[$i][0] = $i_no_record_exists_msg;
	$i++;
} else {
	for($k=0; $k<sizeof($result); $k++) {
		list($classNameNumber, $name, $GradeChar) = $result[$k];
		
		$ExportArr[$i][0] = $k+1;
		$ExportArr[$i][1] = $classNameNumber;
		$ExportArr[$i][2] = $name;
		$ExportArr[$i][3] = $GradeChar;
		$i++;
	}
}

$numOfColumn = sizeof($columnNo);
$export_content = $lexport->GET_EXPORT_TXT($ExportArr, "", "", "\r\n", "", $numOfColumn, "11");

intranet_closedb();

// Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);

?>
