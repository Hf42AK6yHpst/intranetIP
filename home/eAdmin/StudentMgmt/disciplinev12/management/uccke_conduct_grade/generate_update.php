<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# update the conduct grade
$ldiscipline->updateConductGradeAccordingAPGM($Year, $semester, $targetClass, $SettingName);

intranet_closedb();
$class = (is_numeric($targetClass)) ? $targetClass : substr($targetClass,2);
header("Location: index.php?xmsg=grade_generated");
?>