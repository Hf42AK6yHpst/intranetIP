<?php
# using : 

########## Change Log ###############
#
#	Date	:	2015-07-23	(Bill)	[2015-0611-1642-26164]
#				Copy from /common_choose/index.php
#
#####################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");

intranet_auth();
intranet_opendb();

# Layout
$MODULE_OBJ['title'] = $Lang['CommonChoose']['PageTitle']['SelectAudience'];
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$luser = new libuser();
$li = new libgrouping();
$lclass = new libclass();
$laccount = new libaccountmgmt();

$libdiscipline = new libdisciplinev12_ui_cust();

// Only for CWC Cust
if(!$sys_custom['eDiscipline']['CSCProbation']){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

// Class drop donw list
$ClassSelection = $lclass->getSelectClassID("id='YearClassID' name='YearClassID' onChange='this.form.submit();'", $YearClassID); 

// Student photo display table
if($YearClassID){
	$StudentPhotoListTable = $libdiscipline->getClassStudentImageTable($YearClassID);
}

//$StudentPhotoListTable = $tables;
$OpenerFormName = ($OpenerFormName=="") ? "form1" : $OpenerFormName;
?>

<style type="text/css">

.photo_div { display: block; position: relative; background: #FFF; min-height: 265px; border: 3px dashed #006699; padding: 10px; margin-top: 15px; margin-bottom: 15px; text-align: center; box-shadow: 0 0 5px #CCC; overflow: auto; }
.photo_div ul { display: block; margin: 0; padding: 0; }
.photo_div ul li { display: block; height: 170px; padding: 0; margin: 5px; margin-bottom: 10px; list-style-type: none; float: left; border-radius:0px 0px 5px 5px; -moz-border-radius: 0px 0px 5px 5px; -webkit-border-radius: 0px 0px 5px 5px; }
.photo_div ul li span { display: block; width: 150px; word-wrap: break-word; }
.photo_div ul li div { border: 2px solid #FFF; }
.photo_div ul li div:hover { box-shadow:0 0 3px #0e68ae; }

</style>

<script language="javascript">
<!--
function AddOptions(obj){
	// get selected students
	<?php if($from_thickbox){ ?>
	  	 par = parent.window;
	     parObj = parent.window.document.<?=$OpenerFormName?>.elements["<?=$fieldname?>"];
	<?php } else { ?>
	  	 par = opener.window;
	     parObj = opener.window.document.<?=$OpenerFormName?>.elements["<?=$fieldname?>"];
	<?php } ?>
	
	// handle any selected photos
    var selectStatusAry = document.form1.elements["select[]"];
	var aryNum = selectStatusAry.length;
  	if(aryNum==1){
       	var valid = selectStatusAry.value;
 	}
 	else {
 		// loop items
     	for(var i=0; i<aryNum; i++){
	       	var valid = selectStatusAry[i].value;
   			if(valid==1)	break;
	 	}
	}
	
    // alert if no selected photo
	if(!valid){
	 	alert("<?=$Lang['eDiscipline']['CWCProbation']['PleaseSelectPhoto']?>");
	 	document.getElementById("submit_btn").disabled = false;
		document.getElementById('submit_btn').className = "formbutton_v30 print_hide";
		 
		return false;
	}
	 
    parSelectedOptions = par.Get_Selection_Value("<?=$fieldname?>","Array",true);
    par.checkOption(parObj);
    
    // class name
	var class_name = $("#YearClassID option:selected").text();
	class_name = $.trim(class_name);
	// important for IE - please not remove or change content inside replace()
	class_name = class_name.replace(/ /g, '');
    
	// only 1 item
 	if(aryNum==1){
       	// user details
       	var student_id = document.getElementById("userid_0").innerHTML;
 		var student_name = document.getElementById("name_0").innerHTML;
 		var class_num_value = document.getElementById("class_num_0").innerHTML;
 		
 		// add option
    	var addtext = student_name + " (" + class_name + "-" + class_num_value + ")";
		par.checkOptionAdd(parObj, addtext, student_id);
 	}
 	else {
 		// loop items
     	for(var i=0; i<aryNum; i++){
	       	var valid = selectStatusAry[i].value;
	       	
   			if(valid==1){
	   			// user details
	       		var student_id = document.getElementById("userid_" + i).value;
		       	var student_name = document.getElementById("name_" + i).value;
		 		var class_num_value = document.getElementById("classno_" + i).value;
		 	
		 		// add option
		       	var addtext = student_name + " (" + class_name + "-" + class_num_value + ")";
				par.checkOptionAdd(parObj, addtext, student_id);
			}
	 	}
	}
    par.checkOptionAdd(parObj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
     
    // update parent window's auto complete parameter
    if(typeof par.Update_Auto_Complete_Extra_Para == 'function') 
    	par.Update_Auto_Complete_Extra_Para();
     	
    ///// For Repair System variable [Start]
    if(par.form1 && par.form1.flag)
    {
		par.form1.flag.value = 0;
    }
    ///// For Repair System variable [End]

    //par.generalFormSubmitCheck(par.form1);
    
    self.close();
}

function CheckAll(objname)
{
	if($("#CheckAllPhoto").attr("checked"))
	{
		$("[name='"+objname+"']").attr("checked","checked")
	}
	else
	{
		$("[name='"+objname+"']").attr("checked","")
	}
}

function clickedPhoto(obj, locat){
	var selected_value = 'select_'+locat;
	var selected = document.getElementById(selected_value).value;
	
	if(!selected){
		obj.firstChild.style.border = "2px solid #87ceeb";
		document.getElementById(selected_value).value = 1;
	} else {
		obj.firstChild.style.border = "2px solid #FFF";
		document.getElementById(selected_value).value = "";
	}
}

//-->
</script>

<form name="form1" action="" method="post">

	<div class="form_table">
		<table class="form_table_v30">
		<tr>
			<td class="field_title"><?=$Lang['General']['SchoolYear']?></td>
			<td ><?=getCurrentAcademicYear();?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['CommonChoose']['SelectTarget']?></td>
			<td><?=$i_identity_student?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$i_UserParentLink_SelectClass?></td>
			<td><?=$ClassSelection?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$i_frontpage_campusmail_select_user?></td>
			<td><?=$StudentPhotoListTable?></td>
		</tr>
		</table>
	
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?= $linterface->GET_ACTION_BTN($button_add, "button","this.disabled=true; this.className='formbutton_disable_v30 print_hide'; AddOptions(document.form1.elements['UserID[]']);","submit_btn")?>
			&nbsp;
			<?php
				if($from_thickbox){
					echo $linterface->GET_ACTION_BTN($button_close, "button", "parent.js_Hide_ThickBox();");
				}else{
					echo $linterface->GET_ACTION_BTN($button_close, "button", "self.close();");
				} 
			?>
			<p class="spacer"></p>
		</div>
	</div>

	<input type=hidden name=fieldname value="<?=$fieldname?>">

</form>
<?php
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>