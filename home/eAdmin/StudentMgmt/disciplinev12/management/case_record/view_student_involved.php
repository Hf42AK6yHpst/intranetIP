<?php
# using: henry
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_right_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
	$ck_right_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_right_page_number!="")
{
	$pageNo = $ck_right_page_number;
}

if ($ck_right_page_order!=$order && $order!="")
{
	setcookie("ck_right_page_order", $order, 0, "", "", 0);
	$ck_right_page_order = $order;
} else if (!isset($order) && $ck_right_page_order!="")
{
	$order = $ck_right_page_order;
}

if ($ck_right_page_field!=$field && $field!="")
{
	setcookie("ck_right_page_field", $field, 0, "", "", 0);
	$ck_right_page_field = $field;
} else if (!isset($field) && $ck_right_page_field!="")
{
	$field = $ck_right_page_field;
}

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libucc.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

//$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Case_Record-View");
/*
if(!($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-View") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-View")))
{
		$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
		exit();
}
*/

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

$selectClass = $ldiscipline->caseClassMenu($CaseID, $targetClass, "name=\"targetClass\" class=\"formtextbox\" onChange=\"javascript:reloadForm()\"");

$conds = "";

if($targetClass != "" && $targetClass != "0") {
    $conds .= " AND (yc.ClassTitleEN = '$targetClass' OR yc.ClassTitleB5 = '$targetClass')";
}

$conds2 = "";

# Waiting for Approval #
if($waitApproval == 1) {
	$waitApprovalChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.RecordStatus = ".DISCIPLINE_STATUS_PENDING;	
}

# Approved #  // released record also be approved
if($approved == 1) {
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	//$conds2 .= "(a.RecordStatus = ".DISCIPLINE_STATUS_APPROVED." OR a.RecordStatus = ".DISCIPLINE_STATUS_WAIVED.")";
	$conds2 .= "(a.RecordStatus = ".DISCIPLINE_STATUS_APPROVED." )";
	$approvedChecked = "checked";
}

# Rejected #
if($rejected == 1) {
	$rejectedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.RecordStatus = ".DISCIPLINE_STATUS_REJECTED;	
}

# Released #
if($released == 1) {
	$releasedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.ReleaseStatus = ".DISCIPLINE_STATUS_RELEASED;	
}

# Waived #
if($waived == 1) {
	$waivedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.RecordStatus = ".DISCIPLINE_STATUS_WAIVED;	
}

# Locked #
if($locked == 1) {
	$lockedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.LockStatus = ".DISCIPLINE_STATUS_LOCK;	
}

$conds2 .= ($conds2 != "") ? ")" : "";


# Status Layer
$StatusLayer = "<div class='selectbox_group'> <a href='javascript:;' onClick=\"MM_showHideLayers('status_option','','show')\">{$i_Discipline_System_Award_Punishment_Select_Status}</a> </div>";
$StatusLayer .= "<br style='clear:both'>";
$StatusLayer .= "<div id='status_option' class='selectbox_layer'>";
$StatusLayer .= "<table width='180' border='0' cellspacing='0' cellpadding='3'>";
$StatusLayer .= "<tr>";
$StatusLayer .= "<td><input type='checkbox' name='waitApproval' id='waitApproval' value=1 {$waitApprovalChecked}>";
$StatusLayer .= "<label for=waitApproval>{$i_Discipline_System_Award_Punishment_Pending}</label><br>";
$StatusLayer .= "<input type='checkbox' name='approved' id='approved' value=1 {$approvedChecked}>";
$StatusLayer .= "<label for=approved>{$i_Discipline_System_Award_Punishment_Approved}</label><br>";
$StatusLayer .= "<input type='checkbox' name='rejected' id='rejected' value=1 {$rejectedChecked}>";
$StatusLayer .= "<label for=rejected>{$i_Discipline_System_Award_Punishment_Rejected}</label></td>";
$StatusLayer .= "</tr><tr>";
$StatusLayer .= "<td align='left' valign='top' class='dotline' height='1'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' height='1'></td>";
$StatusLayer .= "</tr><tr>";
$StatusLayer .= "<td><input type='checkbox' name='released' id='released' value=1 {$releasedChecked}><label for=released>{$i_Discipline_System_Award_Punishment_Released}</label></td>";
$StatusLayer .= "</tr><tr>";
$StatusLayer .= "<td align='left' valign='top' class='dotline' height='1'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' height='1'></td>";
$StatusLayer .= "</tr><tr>";
$StatusLayer .= "<td><input type='checkbox' name='waived' id='waived' value=1 {$waivedChecked}><label for=waived>{$i_Discipline_System_Award_Punishment_Waived}</label></td>";
$StatusLayer .= "</tr>";
/*
$StatusLayer .= "<tr>";
$StatusLayer .= "<td align='left' valign='top' class='dotline' height='1'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' height='1'></td>";
$StatusLayer .= "</tr><tr>";
$StatusLayer .= "<td><input type='checkbox' name='locked' id='locked' value=1 {$lockedChecked}><label for=locked>{$i_Discipline_System_Award_Punishment_Locked}</label></td>";
$StatusLayer .= "</tr>";
*/
$StatusLayer .= "<tr>";
$StatusLayer .= "<td align='left' valign='top' class='dotline' height='2'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' height='2'></td>";
$StatusLayer .= "</tr><tr>";
$StatusLayer .= "<td align='center' class='tabletext'>";
$StatusLayer .= $linterface->GET_BTN($button_apply, "button", "javascript:reloadForm()");
$StatusLayer .= $linterface->GET_BTN($button_cancel, "button", "javascript: onClick=MM_showHideLayers('status_option','','hide');form.reset();");
$StatusLayer .= "</td></tr></table></div>";

# Change Status Layer
if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-Approval")) {
	$actionLayer = "<tr>";
	$actionLayer .= "<td align='left' valign='top' class='tabletext'><a href='javascript:;' class='tabletool'><img src='{$image_path}/{$LAYOUT_SKIN}/icon_approve.gif' width='12' height='12' border='0' align='absmiddle'> {$i_Discipline_System_Award_Punishment_Approve}</a></td>";
	$actionLayer .= "</tr>";
	$actionLayer .= "<tr>";
	$actionLayer .= "<td align='left' valign='top'><a href='javascript:;' class='tabletool'><img src='{$image_path}/{$LAYOUT_SKIN}/icon_reject.gif' width='12' height='12' border='0' align='absmiddle'> {$i_Discipline_System_Award_Punishment_Reject}</a></td>";
	$actionLayer .= "</tr>";
}
if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-Release")) {
	if($actionLayer != '') {
		$actionLayer .= "<tr>";
		$actionLayer .= "<td align='left' valign='top' class='dotline' height='2'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' height='2'></td>";
		$actionLayer .= "</tr>";
	}
	$actionLayer .= "<tr>";
	$actionLayer .= "<td align='left' valign='top' class='tabletext'><a href='javascript:;' class='tabletool'><img src='{$image_path}/{$LAYOUT_SKIN}/icon_to_student_s.gif' width='12' height='12' border='0' align='absmiddle'> {$i_Discipline_System_Award_Punishment_Release}</a></td>";
	$actionLayer .= "</tr>";
	$actionLayer .= "<tr>";
	$actionLayer .= "<td align='left' valign='top'><a href='javascript:;' class='tabletool'><img src='{$image_path}/{$LAYOUT_SKIN}/icon_private.gif' width='12' height='12' border='0' align='absmiddle'> {$i_Discipline_System_Award_Punishment_UnRelease}</a></td>";
	$actionLayer .= "</tr>";
}
if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-Waive")) {
	if($actionLayer != '') {
		$actionLayer .= "<tr>";
		$actionLayer .= "<td align='left' valign='top' class='dotline' height='2'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' height='2'></td>";
		$actionLayer .= "</tr>";
	}
	$actionLayer .= "<tr>";
	$actionLayer .= "<td align='left' valign='top'><a href='javascript:;' class='tabletool'><img src='{$image_path}/{$LAYOUT_SKIN}/icon_waive.gif' width='12' height='12' border='0' align='absmiddle'> {$i_Discipline_System_Award_Punishment_Waive}</a></td>";
	$actionLayer .= "</tr>";
	$actionLayer .= "<tr>";
	$actionLayer .= "<td align='left' valign='top'><a href='javascript:;' class='tabletool'><img src='{$image_path}/{$LAYOUT_SKIN}/icon_waive.gif' width='12' height='12' border='0' align='absmiddle'> {$i_Discipline_System_Award_Punishment_Redeem}</a></td>";
	$actionLayer .= "</tr>";
}
$meritImg = "<img src={$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_merit.gif width=20 height=20 border=0 align=absmiddle border=0>";
$demeritImg = "<img src={$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_demerit.gif width=20 height=20 border=0 align=absmiddle border=0>";

$meritArr = array();
$meritArr[5] = $i_Merit_Warning;
$meritArr[6] = $i_Merit_Merit;
$meritArr[7] = $i_Merit_MinorCredit;
$meritArr[8] = $i_Merit_MajorCredit;
$meritArr[9] = $i_Merit_SuperCredit;
$meritArr[10] = $i_Merit_UltraCredit;
$meritArr[4] = $i_Merit_BlackMark;
$meritArr[3] = $i_Merit_MinorDemerit;
$meritArr[2] = $i_Merit_MajorDemerit;
$meritArr[1] = $i_Merit_SuperDemerit;
$meritArr[0] = $i_Merit_UltraDemerit;

$li = new libdbtable2007($field, $order, $pageNo);

$yearID = Get_Current_Academic_Year_ID();

$clsName = ($intranet_session_language=="en") ? "yc.ClassTitleEN" : "yc.ClassTitleB5";
$student_namefield = getNamefieldByLang("b.");

$sql = "SELECT 
			IF($clsName IS NULL,'--',CONCAT($clsName, ' - ', ycu.ClassNumber)) as ClassNameNum,
			$student_namefield as std_name,
			IF(a.MeritType=1,'$meritImg', IF(a.MeritType=-1,'$demeritImg','')) as meritImg,
			if(a.MeritType=0,'', CONCAT(a.ProfileMeritCount,' ',a.ProfileMeritType)) as record,
			CONCAT(c.ItemCode,' - ',REPLACE(c.ItemName,'<','&lt;')) as reason,
			a.TemplateID as action,
			LEFT(a.DateModified,10) as modifiedDate,
			CONCAT('-','-') as status,";
			
$sql .= ((($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditOwn") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditAll")) && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditAll") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditOwn")) || (($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-DeleteOwn") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-DeleteAll")) && ($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditAll") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditOwn")))) ? "CONCAT('<input type=checkbox name=RecordID[] value=', a.RecordID ,'>')," : "NULL," ;
$sql .= "
			
			a.NoticeID,
			a.RecordID,
			a.RecordStatus,
			a.ProfileMeritType,
			a.ProfileMeritCount,
			b.UserID,
			ReleaseStatus,
			IF($clsName IS NULL AND (b.ClassName IS NOT NULL AND b.ClassName!=''), '0', '1') as flag
		FROM DISCIPLINE_MERIT_RECORD as a
			LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
			LEFT OUTER JOIN DISCIPLINE_MERIT_ITEM as c ON a.ItemID = c.ItemID
			LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=b.UserID)
			LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) 
			LEFT OUTER JOIN YEAR y ON (y.YearID=yc.YearID)
			
		WHERE a.CaseID=$CaseID AND yc.AcademicYearID=$yearID
			$conds
			
		";
$sql .= ($conds2 != "") ? " AND ".$conds2 : "";
$sql .= " GROUP BY a.StudentID";
//$sql .= " HAVING flag = 1";


$templateIDLocation = 5;
$noticeIDLocation = 9;
$recordIDLocation = 10;
$statusLocation = 11;

$li->field_array = array("concat(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', 1)), IF(INSTR(ClassNameNum, '-') = 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', -1)), 10, '0')))", "std_name", "meritImg", "record", "reason", "action", "modifiedDate", "status");

$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td>".$li->column($pos++, $i_general_class)."</td>\n";
$li->column_list .= "<td>".$li->column($pos++, $i_general_name)."</td>\n";
$li->column_list .= "<td>&nbsp;</td>\n";$pos++;
$li->column_list .= "<td>".$li->column($pos++, $i_Discipline_Reason2)."</td>\n";
$li->column_list .= "<td>".$li->column($pos++, $eDiscipline["Award_Punishment_RecordItem"])."</td>\n";
$li->column_list .= "<td>".$li->column($pos++, $eDiscipline["Action"])."</td>\n";
$li->column_list .= "<td>".$li->column($pos++, $i_Discipline_Last_Updated)."</td>\n";
$li->column_list .= "<td>".$i_Discipline_System_Discipline_Status."</td>\n";
$li->column_list .= ((($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditOwn") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditAll")) && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditAll") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditOwn")) || (($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-DeleteOwn") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-DeleteAll")) && ($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditAll") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditOwn")))) ? "<td width='1'>".$li->check("RecordID[]")."</td>\n" : "<td width='1'>&nbsp;</td>";
		

$CaseArr = $ldiscipline->getCaseRecordByCaseID($CaseID);
$CaseTitle = $CaseArr[0][1];
//list($CaseNumber,$CaseTitle,$Year,$Semester,$Location,$Category,$EventDate,$PICID,$Attachment,$RecordStatus,$DateModified,$ModifiedBy,$RecordStatusNum, $ReleaseStatus, $ReleasedDate, $ReleasedBy, $FinishedDate, $FinishedBy, $Remark, $PIC) = $CaseArr[0];

# menu highlight setting
$CurrentPage = "Management_CaseRecord";

$TAGS_OBJ[] = array($i_Discipline_System_Access_Right_Case_Record);

$PAGE_NAVIGATION[] = array($i_Discipline_System_Discipline_Case_Record_Case_List, "index.php");
$PAGE_NAVIGATION[] = array($CaseTitle);

$subTag[] = array($i_Discipline_System_Case_Record_Case_Info, "view.php?CaseID={$CaseID}&Page=info", 0);
$subTag[] = array($i_Discipline_System_Case_Record_Student_Involved, "view_student_involved.php?CaseID={$CaseID}&Page=involved", 1);

if(!$ldiscipline->CASE_IS_FINISHED($CaseID) && ($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditOwn") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditAll")) && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-New"))
{
	$toolbar .= $linterface->GET_LNK_NEW("student_new1.php?CaseID={$CaseID}",$i_Discipline_System_Case_Record_New_Student,"","","",0);
	$toolbar .= toolBarSpacer().$linterface->GET_LNK_IMPORT("import_student.php?CaseID={$CaseID}","","","","",0);
}
$toolbar .= toolBarSpacer().$linterface->GET_LNK_EXPORT("export_student.php?targetClass=$targetClass&waitApproval=$waitApproval&approved=$approved&rejected=$rejected&released=$released&waived=$waived&locked=$locked&CaseID=$CaseID","","","","",0);
if($sys_custom['eDisciplinev12']['CheungShaWanCatholic']['CaseRecordReport'])
	$toolbar .= toolBarSpacer().$linterface->GET_LNK_PRINT("javascript:printCase($CaseID)","","","","",0);
	
$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>
<script language="javascript">
<!--
function printCase(caseID) {
	newWindow('friendly_print_case.php?CaseID='+caseID, 10);
}

function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) 
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}


function reloadForm() {
	document.form1.submit();	
}

function removeCat(obj,element,page){
		var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(alertConfirmRemove)){
                obj.action=page;
                obj.method="post";
                obj.submit();
                }
        }
}

function changeClickID(id) {
	document.form1.clickID.value = id;
	hideAllOtherLayer();

}

//-->
</script>
<script language="javascript">
<!--
var xmlHttp
var jsDetention = [];
var jsWaive = [];
//var jshistory = [];

function showResult(str,flag)
{
		
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 

	var url = "";
		
	url = "get_live.php?flag="+flag;
	url = url + "&RecordID=" + str
	url = url + "&sid=" + Math.random()
	
	if(flag=='detention') {
		xmlHttp.onreadystatechange = stateChanged 
	} else if(flag=='waived') {
		xmlHttp.onreadystatechange = stateChanged2 
	}
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_detail"+id).style.zIndex = "1";
		document.getElementById("show_detail"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_detail"+id).style.border = "0px solid #A5ACB2";
	} 
}

function stateChanged2() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_waive"+id).style.zIndex = "1";
		document.getElementById("show_waive"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_waive"+id).style.border = "0px solid #A5ACB2";
	} 
}

function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

function hideAllOtherLayer() {
	var layer = "";
	
	if(jsDetention.length != 0) {
		for(i=0;i<jsDetention.length;i++) {
			layer = "show_detail" + jsDetention[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	if(jsWaive.length != 0) {
		for(i=0;i<jsWaive.length;i++) {
			layer = "show_waive" + jsWaive[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	/*
	if(jsHistory.length != 0) {
		for(i=0;i<jsHistory.length;i++) {
			layer = "show_history" + jsHistory[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	*/
}


/*********************************/
function getObject( obj ) {

	if ( document.getElementById ) {
		obj = document.getElementById( obj );
	
	} else if ( document.all ) {
		obj = document.all.item( obj );
	
	} else {
		obj = null;
	}
	
	return obj;
}

function moveObject( obj, e ) {
	var tempX = 0;
	var tempY = 0;
	var offset = 5;
	var objHolder = obj;
	
	obj = getObject( obj );
	if (obj==null) {return;}
	
	if (document.all) {
		tempX = event.clientX + document.body.scrollLeft;
		tempY = event.clientY + document.body.scrollTop;
	} else {
		tempX = e.pageX;
		tempY = e.pageY;
	}
	
	if (tempX < 0){tempX = 0} else {tempX = tempX-276}
	if (tempY < 0){tempY = 0} else {tempY = tempY}
	
	obj.style.top  = (tempY + offset) + 'px';
	obj.style.left = (tempX + offset) + 'px';
	
	displayObject( objHolder, true );
}

function displayObject( obj, show ) {

	obj = getObject( obj );
	if (obj==null) return;
	
	obj.style.display = show ? 'block' : 'none';
	obj.style.visibility = show ? 'visible' : 'hidden';
}


//-->
</script>

<form name="form1" method="post">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr> 
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
					</td>
				</tr>
			</table>
			<div class="shadetabs">
				<ul>
					<?=$ldiscipline->getSubTag($subTag)?>
				</ul>
			</div>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right"><br>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td height="28" align="left" valign="bottom">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td>
												<table width="100%" border="0" cellspacing="0" cellpadding="2">
													<tr>
														<td width="70%"><?=$toolbar?></td>
														<td align="right"><?= $linterface->GET_SYS_MSG($xmsg) ?></td>
													</tr>
												</table>
											</td>
											<td align="right"><?=$linterface->GET_SYS_MSG($msg)?></td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr class="table-action-bar">
											<td>
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td><?=$selectClass?></td>
													</tr>
													<tr>
														<td><?=$StatusLayer?></td>
													</tr>
												</table>												 
											</td>
											<td>&nbsp;</td>
											<td align="right" valign="bottom">
												<? 
												if(!$ldiscipline->CASE_IS_FINISHED($CaseID)) {
												if(
													(($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditOwn") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditAll")) && ($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditAll") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditOwn")))
													||
													(($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-DeleteOwn") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-DeleteAll")) && ($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditAll") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditOwn")))
												) { ?>
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
														<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
															<table border="0" cellspacing="0" cellpadding="2">
																<tr>
																	<? if(($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-DeleteOwn") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-DeleteAll")) && ($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditAll") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditOwn"))) { ?>
																	<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
																	<td nowrap><a href="javascript:removeCat(document.form1,'RecordID[]','student_involved_remove_update.php')" class="tabletool" title="<?= $button_delete ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle"> <?= $button_delete ?></a></td>
																	<? } ?>
																	<? if(($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditOwn") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditAll")) && ($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditAll") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditOwn"))) { ?>
																	<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
																	<td nowrap><a href="javascript:checkEdit(document.form1,'RecordID[]','student_involved_edit.php')" class="tabletool" title="<?= $button_edit ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_edit ?></a></td>
																	<? } ?>
																</tr>
															</table>
														</td>
														<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
													</tr>
												</table>
												<? } 
												}?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<?=$li->displayFormat_Case_Record("{$image_path}/{$LAYOUT_SKIN}", $templateIDLocation, $noticeIDLocation, $recordIDLocation, $statusLocation, $meritArr);?>
						<?/*=$li->display()*/?>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<input type="hidden" name="pageNo" value="<?=$li->pageNo; ?>"/>
<input type="hidden" name="order" value="<?=$li->order; ?>"/>
<input type="hidden" name="field" value="<?=$li->field; ?>"/>
<input type="hidden" name="page_size_change" value=""/>
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>"/>
<input type="hidden" name="CaseID" value="<?=$CaseID ?>">
<input type="hidden" name="clickID" value="">
<input type="hidden" name="pic" value="<?=$pic?>">
</form>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>


