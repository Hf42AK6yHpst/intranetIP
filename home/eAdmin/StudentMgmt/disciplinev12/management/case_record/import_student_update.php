<?php
// Modifying by:

$PATH_WRT_ROOT="../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();


$li = new libdb();
$lu = new libuser();
$filepath = $csvfile;
$filename = $csvfile_name;

$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Award_Punishment-New");

$result_ary = array();
$imported_student = array();
$student = array();

## findout student ary
for($i=0;$i<sizeof($data);$i++)
{
	### get data
	$ClassName = $data[$i][0];
	$ClassNumber = $data[$i][1];

	### checking - valid class & class number (student found)
	$StudentID = $lu->returnUserID($ClassName, $ClassNumber, 2, '0,1,2');
	if($StudentID)
		array_push($student, $StudentID);
}

# get csv data from db
//$sql = "select * from temp_profile_student_merit_import where UserID=". $_SESSION["UserID"];
$sql = "select * from temp_profile_student_merit_import";
$data = $ldiscipline->returnArray($sql);

### List out the import result
$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
$x .= "<tr>";
$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_general_class</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_identity_student</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eDiscipline']['EventDate']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_SettingsSemester</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$iDiscipline['RecordType']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$eDiscipline["RecordItem"]."</td>";
if ($ldiscipline->use_subject)
{
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_Subject</td>";
}
$x .= "<td class=\"tablebluetop tabletopnolink\">".$eDiscipline['Conduct_Mark']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_general_receive</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_System_Discipline_Case_Record_PIC</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_UserRemark</td>";
//$x .= "<td class=\"tablebluetop tabletopnolink\">".$iDiscipline['AccumulativePunishment_Import_Failed_Reason']."</td>";
$x .= "</tr>";
//debug_pr($data);
for($a=0;$a<sizeof($data);$a++)
{
	$StudentID = $data[$a]['StudentID'];
	$lu = new libuser($StudentID);
	$thisClassNumber = $lu->ClassNumber;
	$thisClassName = $lu->ClassName;

	### insert into DISCIPLINE_MERIT_RECORD (status = pending)
	$dataAry = array();
	$dataAry['RecordDate'] = $data[$a]['RecordDate'];
	$dataAry['Year'] = $data[$a]['Year'];
	$dataAry['Semester'] = $data[$a]['Semester'];
	$dataAry['StudentID'] = $data[$a]['StudentID'];
	$dataAry['ItemID'] = $data[$a]['ItemID'];
	$dataAry['ItemText'] = addslashes($data[$a]['ItemText']);
	$dataAry['MeritType'] = $data[$a]['MeritType'];
	$dataAry['Remark'] = addslashes($data[$a]['Remark']);
	$dataAry['ProfileMeritType'] = $data[$a]['ProfileMeritType'];
	$dataAry['ProfileMeritCount'] = $data[$a]['ProfileMeritCount'];
	$dataAry['ConductScoreChange'] = $data[$a]['ConductScoreChange'];
	$dataAry['Subject'] = $data[$a]['Subject'];
	$dataAry['PICID'] = $data[$a]['PICID'];
	$dataAry['RecordStatus'] = DISCIPLINE_STATUS_PENDING;
	$dataAry['CaseID'] = $CaseID;
	$yearAndTermInfo = getAcademicYearInfoAndTermInfoByDate($dataAry['RecordDate']);
	$dataAry['AcademicYearID'] = $yearAndTermInfo[0];
	$dataAry['YearTermID'] = $yearAndTermInfo[2];

	
	if($dataAry['MeritType']==1)
	{
		$MeritRecordType = $ldiscipline->returnValidMeritTypeFieldWithOrder(1);
	}
	else
	{
		$MeritRecordType = $ldiscipline->returnValidMeritTypeFieldWithOrder(-1);
	}
	for($i=1;$i<sizeof($MeritRecordType)+1;$i++)
	{
		if($dataAry['ProfileMeritType'] == $MeritRecordType[$i][0])
		{
			$MeritTypeName = $MeritRecordType[$i][1];
		}
	}
	
	$x .= "<tr class=\"tablebluerow".($a%2+1)."\">";
	$x .= "<td class=\"tabletext\">".($a+1)."</td>";
	$x .= "<td class=\"tabletext\">". $data[$a]['ClassName']."-".$data[$a]['ClassNumber'] ."</td>";
	$x .= "<td class=\"tabletext\">". $data[$a]['StudentName'] ."</td>";
	$x .= "<td class=\"tabletext\">". substr($data[$a]['RecordDate'],0,10) ."</td>";
	$x .= "<td class=\"tabletext\">". $data[$a]['Semester'] ."</td>";
	$x .= "<td class=\"tabletext\">". $data[$a]['Category'] ."</td>";
	$x .= "<td class=\"tabletext\">". intranet_htmlspecialchars($data[$a]['ItemText']) ."</td>";
	if ($ldiscipline->use_subject)
	{
		$x .= "<td class=\"tabletext\">". $data[$a]['Subject'] ."</td>";
	}
	$x .= "<td class=\"tabletext\">". $data[$a]['ConductScoreChange'] ."</td>";
	$x .= "<td class=\"tabletext\">". $data[$a]['Count'].$MeritTypeName."</td>";
	
	$x .= "<td class=\"tabletext\">". $data[$a]['Teacher'] ."</td>";
	$x .= "<td class=\"tabletext\">". $data[$a]['Remark'] ."</td>";
	$x .= "</tr>";
	
	$MeritRecordID = $ldiscipline->INSERT_MERIT_RECORD($dataAry);

	if($MeritRecordID)
	{
		$import_success++;
				
		$MeritRecordIDAry[] = $MeritRecordID;

		# Detention
		if ($detentionFlag == "YES") {
			for ($i=0; $i<sizeof($DetentionArr); $i=$i+3) {
				// array sequence: StudentID, DetentionID, Remark
				if ($StudentID == $DetentionArr[$i]) {
					$detentionDataArr = array();
					$detentionDataArr['StudentID'] = "'".$DetentionArr[$i]."'";
					if ($DetentionArr[$i+1] == "") $DetentionArr[$i+1] = "NULL";
					$detentionDataArr['DetentionID'] = $DetentionArr[$i+1];
					$detentionDataArr['Remark'] = "'".stripslashes($DetentionArr[$i+2])."'";
					$detentionDataArr['DemeritID'] = "'".$MeritRecordID."'";
					$detentionDataArr['RequestedBy'] = "'".$UserID."'";
					if ($DetentionArr[$i+1] != "NULL") {
						$detentionDataArr['ArrangedBy'] = "'".$UserID."'";
						$detentionDataArr['ArrangedDate'] = 'NOW()';
					}
					$detStuRecordID = $ldiscipline->insertDetention($detentionDataArr);
				}
			}
		}

		# check need approve or not
		$need_approval = $ldiscipline->CHECK_APPROVAL_REQUIRED($MeritType,$MeritNum);

		if($need_approval)
		{
			# can do nothing
		}
		else
		{
			# set APPROVE
			$ldiscipline->APPROVE_MERIT_RECORD($MeritRecordID);
		}

	}
	
	
}

$sql = "delete from temp_profile_student_merit_import";
$ldiscipline->db_db_query($sql);


if($import_success>0)
{
	$xmsg2= "<font color=green>".($import_success)." ".$iDiscipline['Reord_Import_Successfully']."</font>";
}
else
{
	$xmsg="import_failed";	
}

$x .= "</table>";

$linterface = new interface_html();
$import_button = $linterface->GET_ACTION_BTN($button_finish, "button","self.location='view_student_involved.php?CaseID=$CaseID&Page=involved'")."&nbsp;".$linterface->GET_ACTION_BTN($iDiscipline['ImportOtherRecords'], "button", "window.location='import_student.php?CaseID=$CaseID'");

$CurrentPage = "Management_CaseRecord";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($i_Discipline_System_Access_Right_Case_Record);

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

$linterface->LAYOUT_START();

?>

<form name="form1" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
</tr>
<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td><?= $x ?></td>
	</tr>
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>		
</table>
<input type="hidden" name="CaseID" id="CaseID" value="<?=$CaseID?>"/>
</form>


<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>