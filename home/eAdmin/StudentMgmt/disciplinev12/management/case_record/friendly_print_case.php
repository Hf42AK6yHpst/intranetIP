<?php
// Modifying by: henry chow

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
//$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Detention-View");
if(!($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-View") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-View")))
{
		$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
		exit();
}

$CaseArr = $ldiscipline->getCaseRecordByCaseID($CaseID);
list($CaseNumber,$CaseTitle,$Year,$Semester,$Location,$Category,$EventDate,$PICID,$Attachment,$RecordStatus,$DateModified,$ModifiedBy,$RecordStatusNum, $ReleaseStatus, $ReleasedDate, $ReleasedBy, $FinishedDate, $FinishedBy, $Remark, $AcademicYearID, $YearTermID, $PIC) = $CaseArr[0];

$studentInvolve = $ldiscipline->RETRIEVE_CASE_INVOLVED_STUDENT($CaseID);

$currentYearName = getCurrentAcademicYear();
$yearName = $currentYearName."-".($currentYearName+1);

$caseDate = substr($EventDate,0,4)."/".substr($EventDate,5,2)."/".substr($EventDate,8,2);

# school badge (if any)
if(file_exists($PATH_WRT_ROOT."file/CSWCSS_logo_01.jpg"))
	$schoolBadge = "<td width=\"90\"><img src='".$PATH_WRT_ROOT."file/CSWCSS_logo_01.jpg"."' border=\"0\" width=\"70\" height=\"90\"></td>";

# header of print out (school name)
$print_header = "<u>".$Lang['eDiscipline']['RecordOfMisbehavedStudent']['CH']." ".$yearName."</u><br>(".$yearName.$Lang['eDiscipline']['RecordOfMisbehavedStudent']['EN'].")";

for($i=0; $i<40; $i++)
	$underline .= "&nbsp;";
$underline = "<u>$underline</u>";
for($i=0; $i<10; $i++)
	$smallUnderline .= "&nbsp;";
$smallUnderline = "<u>$smallUnderline</u>";

for($i=0; $i<sizeof($studentInvolve); $i++) {
	
	//$sql = "SELECT USR.ChineseName, USR.EnglishName, ClassName, ClassNumber, MR.RecordID FROM DISCIPLINE_MERIT_RECORD MR LEFT OUTER JOIN INTRANET_USER USR ON (USR.UserID=MR.StudentID) LEFT OUTER JOIN DISCIPLINE_REFERENCE_NUMBER REFNO ON (REFNO.RecordID=MR.RecordID) WHERE MR.StudentID=".$studentInvolve[$i]." AND REFNO.ReferenceToTable='DISCIPLINE_MERIT_RECORD'";
	$sql = "SELECT USR.ChineseName, USR.EnglishName, ClassName, ClassNumber, MR.RecordID FROM DISCIPLINE_MERIT_RECORD MR LEFT OUTER JOIN INTRANET_USER USR ON (USR.UserID=MR.StudentID) LEFT OUTER JOIN DISCIPLINE_REFERENCE_NUMBER REFNO ON (REFNO.RecordID=MR.RecordID AND REFNO.ReferenceToTable='DISCIPLINE_MERIT_RECORD') WHERE MR.StudentID=".$studentInvolve[$i]." AND MR.CaseID='$CaseID'";
	$result = $ldiscipline->returnArray($sql,4);
	//debug_pr($result);
	$refno = $ldiscipline->RETURN_REFERNCE_NUMBER('DISCIPLINE_MERIT_RECORD', $result[0]['RecordID']);
	
	if($i!=0)
		$tableContent .= "<div style='page-break-after:always'>&nbsp;</div>";
		
	# Page 1	
	$tableContent .= "
		<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center'>
			<tr>
				".$schoolBadge."
				<td class='eSportprinttitle' valign='top'><b>".$print_header."</b></td>
				<td align='right' valign='top'>
					<table align='right' height='100'>
						<tr><td><u>".$Lang['eDiscipline']['FormA']."</u></td></tr>
						<tr><td valign='bottom'><table class='report_border'><tr><td>".$Lang['eDiscipline']['RefNo']." : <u>$refno</u></td></tr></table></td></tr>
					</table>
				</td>
			</tr>
		</table>
		<br>
		<br />
		<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center'>
			<tr>
				<td valign='top'>
					<table width='700' border='0'>
						<tr>
							<td valign='top' width='120'>".$Lang['eDiscipline']['NameOfStudent']."</td>
							<td valign='top' width='1'>:</td>
							<td valign='top' width=''>$smallUnderline<u>".$result[0]['ChineseName']." ".$result[0]['EnglishName']." (".$result[0]['ClassNumber'].")"."$smallUnderline</u></td>
							<td valign='top' width='60'>".$Lang['eDiscipline']['ClassOfStudent']."</td>
							<td valign='top' width='1'>:</td>
							<td valign='top' width=''>$smallUnderline<u>".$result[0]['ClassName']."$smallUnderline</u></td>
						</tr>
					</table>							
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td><b>".$Lang['eDiscipline']['ContactInformation']." : </b></td>
			</tr>
			<tr>
				<td>
					<table width='100%' class='report_border' height='50'>
						<tr>
							<td valign='middle'>".$Lang['eDiscipline']['HomePhone']."</td>
							<td valign='middle'> : ".$underline."</td>
							<td valign='middle'>".$Lang['eDiscipline']['Father']."</td>
							<td valign='middle'> : ".$underline."</td>
							<td valign='middle'>".$Lang['eDiscipline']['Mother']."</td>
							<td valign='middle'> : ".$underline."</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td><b>".$Lang['eDiscipline']['DescriptionOfIncident']." : </b></td>
			</tr>
			<tr>
				<td>
					<table width='100%' class='report_border' border='0'>
						<tr>
							<td valign='top' width='150' height='40'>".$Lang['eDiscipline']['Event']." : </td>
							<td valign='top' width='85%' valign='top'>".$CaseTitle."</td>
						</tr>
						<tr>
							<td valign='top' width='150' height='40'>".$Lang['eDiscipline']['DateTime']." : </td>
							<td valign='top' valign='top'>".$caseDate."</td>
						</tr>
						<tr>
							<td valign='top' width='150' height='40'>".$Lang['eDiscipline']['Place']." : </td>
							<td valign='top'>&nbsp;</td>
						</tr>
						<tr>
							<td valign='top' width='150' height='40'>".$Lang['eDiscipline']['EventDescription']." : </td>
							<td valign='top'><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p></td>
						</tr>
						<tr>
							<td colspan='2'>
								<table width='600' border='0' align='right'>
									<tr>
										<td>".$Lang['eDiscipline']['ReportedBy']."</td>
										<td> : $underline</td>
										<td>".$Lang['eDiscipline']['Date_Bilingual']."</td>
										<td> : $underline</td>	
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td><b>".$Lang['eDiscipline']['StudentBackground']." : </b></td>
			</tr>
			<tr>
				<td>
					<table width='100%' class='report_border' border='0'>
						<tr>
							<td>
								<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
								<table><tr>
									<td>&nbsp;</td>
									<td><table width='25' class='report_border'><tr><td height='20'>&nbsp;</td></tr></table></td>
									<td>".$Lang['eDiscipline']['Discuss']."</td>
								</tr></table>
								<table align='right'><tr>
									<td>".$Lang['eDiscipline']['FormTeacher']."</td>
									<td valign='top'> : </td>
									<td valign='top'>$underline</td>
								</tr></table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align='right'>Page 1/2</td>
			</tr>
		</table>

	";
	$tableContent .= "<div style='page-break-after:always'>&nbsp;</div>";
	
	# Page 2
	$tableContent .= "
		<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center'>
			<tr>
				<td><b>".$Lang['eDiscipline']['InvestigationByDC']." : </b></td>
				<td align='right' height='30'><table class='report_border'><tr><td>".$Lang['eDiscipline']['RefNo']." : <u>$refno</u></td></tr></table></td>
			</tr>
			<tr>
				<td colspan='2'>
					<table width='100%' class='report_border' height='50'>
						<tr><td valign='top' height='80'>".$Lang['eDiscipline']['Results']." :</td></tr>
						<tr><td valign='top' height='80'>".$Lang['eDiscipline']['Punishment']." :</td></tr>
						<tr><td valign='top' height='80'>
							<table width='100%'><tr>
								<td><table width='25' class='report_border'><tr><td height='20'>&nbsp;</td></tr></table></td>
								<td>".$Lang['eDiscipline']['InformedParent']."</td>
								<td>".$Lang['eDiscipline']['PIC']." : $smallUnderline$smallUnderline$smallUnderline</td>
								<td>".$Lang['eDiscipline']['Date']." : $smallUnderline$smallUnderline$smallUnderline</td>
								<td>".$Lang['eDiscipline']['Time']." : $smallUnderline$smallUnderline$smallUnderline</td>
							</tr></table>
						</td></tr>
						<tr><td valign='top' height='80'>".$Lang['eDiscipline']['Remark']." :</td></tr>
						<tr><td valign='top' height='80'>
							<table width='100%'>
								<tr>
									<td><table width='25' class='report_border'><tr><td height='20'>&nbsp;</td></tr></table></td>
									<td>".$Lang['eDiscipline']['Referral']."</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td height='40'>".$Lang['eDiscipline']['OtherReferral']." : $underline</td>
								</tr>
							</table>
						</td></tr>
						<tr>
							<td>
								<table align='right'><tr>
									<td>".$Lang['eDiscipline']['TeacherInCharge']."</td>
									<td valign='top'> : </td>
									<td valign='top'>$underline</td>
								</tr></table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan='2'>&nbsp;</td>
			</tr>
			<tr>
				<td colspan='2'><b>".$Lang['eDiscipline']['PrincipalDecision']." : </b></td>
			</tr>
			<tr>
				<td colspan='2'>
					<table width='100%' class='report_border' border='0'>
						<tr>
							<td>
								<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
								<table align='right'><tr>
									<td>".$Lang['eDiscipline']['PrincipalSignature']."</td>
									<td valign='top'> : </td>
									<td valign='top'>$underline</td>
								</tr></table>
							</td>
						</tr>
					</table>
				</td>
			</tr>			<tr>
				<td colspan='2'>&nbsp;</td>
			</tr>
			<tr>
				<td colspan='2'><b>".$Lang['eDiscipline']['RemindialAction']." : </b></td>
			</tr>
			<tr>
				<td colspan='2'>
					<table width='100%' class='report_border' border='0'>
						<tr>
							<td>
								<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
								<table align='right'>
									<tr>
										<td>".$Lang['eDiscipline']['NameOfTeacherInCharge']."</td>
										<td valign='top'> : </td>
										<td valign='top'>$underline</td>
									</tr>
									<tr>
										<td>".$Lang['eDiscipline']['Signature']."</td>
										<td valign='top'> : </td>
										<td valign='top'>$underline</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align='right' colspan='2'>Page 2/2</td>
			</tr>
		</table>
	";
	
}


$linterface = new interface_html();

include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/edis_print_header.php");

?>
<style type='text/css'>
 td {font-family:"Times New Roman";font-size:12pt}
</style>
<table width="100%" align="center">
	<tr>
		<td align="right" class="print_hide">
			<input name="btn_print" type="button" class="printbutton" value="<?=$button_print?>" onClick="window.print(); return false;" onMouseOver="this.className='printbuttonon'" onMouseOut="this.className='printbutton'">
		</td>
	</tr>
</table>
<table cellspacing="0" cellpadding="4" width="100%" border="0">
	<tbody>
		<?=$tableContent?>
	</tbody>
</table>
<br />
<?
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
?>
