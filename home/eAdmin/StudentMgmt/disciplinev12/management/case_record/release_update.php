<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");

intranet_auth();
intranet_opendb();


$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Case_Record-Release");

# update case status
$ldiscipline->UPDATE_CASE_RELEASE_STATUS($CaseID, DISCIPLINE_STATUS_RELEASED);

/*
# update all the students' a&p records
$RecordIDAry = $ldiscipline->RETRIEVE_CASE_INVOLVED_MERITID($CaseID);
foreach($RecordIDAry as $MeritRecordID)
{
	$datainfo = $ldiscipline->RETRIEVE_MERIT_RECORD_INFO_BY_ID($MeritRecordID);
	$original_RecordStatus = $datainfo[0]['RecordStatus'];
	$StudentID = $datainfo[0]['StudentID'];
	
// 	# if(previous status is REJECTED or PENDING) then set APPROVE first
// 	if($original_RecordStatus==DISCIPLINE_STATUS_REJECTED || $original_RecordStatus==DISCIPLINE_STATUS_PENDING)
// 	{
// 		if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Approval"))
// 		{
// 			# set APPROVE
// 			$ldiscipline->APPROVE_MERIT_RECORD($MeritRecordID);
// 		}
// 	}
	
	# set RELEASE if the status is APPROVED and not RELEASED
	$thisStatus = $ldiscipline->RETRIEVE_MERIT_RECORD_STATUS($MeritRecordID);
	$isReleased = $ldiscipline->RETRIEVE_MERIT_RECORD_RELEASE_STATUS($MeritRecordID);
	if(($thisStatus == DISCIPLINE_STATUS_APPROVED || $thisStatus == DISCIPLINE_STATUS_WAIVED) && !$isReleased)
	{
		# UPDATE [DISCIPLINE_MERIT_RECORD] field ReleasedBy, ReleasedDate, RecordStatus = RELEASE
		$ldiscipline->UPDATE_MERIT_RECORD_RELEASE_STATUS($MeritRecordID, DISCIPLINE_STATUS_RELEASED);

		$templateID = $datainfo[0]['TemplateID'];
		if($templateID && !$datainfo[0]['NoticeID'])# send eNotice if no notice is sent before
		{
			$template_info = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO($templateID);
			if(sizeof($template_info))
			{
				$template_category = $template_info[0]['CategoryID'];
				
				$SpecialData = array();
				$SpecialData['MeritRecordID'] = $MeritRecordID;
				$template_data = $ldiscipline->TEMPLATE_VARIABLE_CONVERSION($template_category, $StudentID, $SpecialData);
				$UserName = $ldiscipline->getUserNameByID($UserID);
				
				$Module = $this->Module;
				$NoticeNumber = time();
				$TemplateID = $templateID;
				$Variable = $template_data;
				$IssueUserID = $UserID;
				$IssueUserName = $eDiscipline["DisciplineName"];
				$TargetRecipientType = "U";
				$RecipientID = array($StudentID);
				$RecordType = NOTICE_TO_SOME_STUDENTS;
				
				include_once($PATH_WRT_ROOT."includes/libucc.php");
				$lc = new libucc();
				$ReturnArr = $lc->setNoticeParameter($Module, $NoticeNumber, $TemplateID, $Variable, $IssueUserID, $IssueUserName, $TargetRecipientType,$RecipientID, $RecordType);
				$NoticeID = $lc->sendNotice();

				if($NoticeID != -1)
				{
					# update [DISCIPLINE_MERIT_RECORD] field NoticeID
					$updateDataAry = array();
					$updateDataAry['NoticeID'] = $NoticeID;
					$ldiscipline->UPDATE_MERIT_RECORD($updateDataAry, $MeritRecordID);
				}
			}
		}
	}
}
*/

header("Location: view.php?CaseID=$CaseID&xmsg=release");

intranet_closedb();
?>