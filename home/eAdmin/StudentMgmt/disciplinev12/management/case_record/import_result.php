<?
// Modifying by: 

############ Change Log Start #############################
#
#	Date	:	2016-04-11 (Bill)
#				Replace deprecated split() by explode() for PHP 5.4
#
############ Change Log End #############################

$PATH_WRT_ROOT="../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();

$limport = new libimporttext();
$lo = new libfilesystem();
$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));

if($ext != ".CSV" && $ext != ".TXT")
{
	header("location: import.php?xmsg=import_failed");
	exit();
}
$data = $limport->GET_IMPORT_TXT($csvfile);

if(is_array($data))
{
	$col_name = array_shift($data);
}
//$file_format = array('Case Number','Case Name','Category','Event Date','Semester','Location','PIC');
$file_format = array('Case Number','Case Name','Event Date','Location','PIC','Remark');
$format_wrong = false;
for($i=0; $i<sizeof($file_format); $i++)
{
	if ($col_name[$i]!=$file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}
if($format_wrong)
{
	header("location: import.php?xmsg=wrong_header");
	exit();
}
if(sizeof($data)==0)
{
	header("location: import.php?xmsg=import_no_record");
	exit();
}
$ldiscipline = new libdisciplinev12();
$lu = new libuser();

$linterface = new interface_html();

# menu highlight setting
$CurrentPage = "Management_CaseRecord";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($i_Discipline_System_Access_Right_Case_Record);

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$linterface->LAYOUT_START();


### List out the import result
$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
$x .= "<tr>";
$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>";

$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_System_Discipline_Case_Record_Case_Number</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_System_Discipline_Case_Record_Case_Title</td>";
//$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_System_Discipline_Case_Record_Category</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eDiscipline']['EventDate']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_SettingsSemester</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_System_Discipline_Case_Record_Case_Location</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_System_Discipline_Case_Record_PIC</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Merit_Remark</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$iDiscipline['AccumulativePunishment_Import_Failed_Reason']."</td>";
$x .= "</tr>";


// $sql = "drop table temp_case_record_import";
// $ldiscipline->db_db_query($sql);
//		 Category varchar(255),
$sql = "create table temp_case_record_import
	(
		 CaseNumber varchar(50),
		 CaseName varchar(128),
		 EventDate date,
		 Semester varchar(100),
		 Location  varchar(255),
		 PICID varchar(255),
		 PIC varchar(255),
		 Remark text
		 
	) ENGINE=InnoDB DEFAULT CHARSET=utf8";
$ldiscipline->db_db_query($sql);

$fields = mysql_list_fields($intranet_db, 'temp_case_record_import');
$columns = mysql_num_fields($fields);
for ($i = 0; $i < $columns; $i++) {$field_array[] = mysql_field_name($fields, $i);}
if (!in_array('UserID', $field_array))
{
	$result = mysql_query('ALTER TABLE temp_case_record_import ADD UserID int(11)');
}
# delete the temp data in temp table 
$sql = "delete from temp_case_record_import where UserID=".$_SESSION["UserID"];
$ldiscipline->db_db_query($sql) or die(mysql_error());

$error_occured = 0;

for($i=0;$i<sizeof($data);$i++)
{
	$error = array();
	$PicIDArr = array();
	### get data
//	list($CaseNumber,$CaseName, $Category, $EventDate, $Semester, $Location,$PIC) = $data[$i];
	list($CaseNumber,$CaseName, $EventDate, $Location,$PIC,$Remark) = $data[$i];
	
	$CaseNumber = intranet_htmlspecialchars($CaseNumber);
	$CaseName = intranet_htmlspecialchars($CaseName);
	$Remark = intranet_htmlspecialchars($Remark);
	$Location = intranet_htmlspecialchars($Location);
			
	#CaseNumber
	if(trim($CaseNumber)=='')
	{
		$error['Casenumber']=$iDiscipline['Case_Missing_CaseNumber'];
	}
	else
	{
		if($ldiscipline->caseRecordCaseNumberIsDuplicate(($CaseNumber)))
		{
			$error['Casenumber']=$i_Discipline_System_Discipline_Case_Record_Case_Number_Exists_JS_alert;
		}
	}
	#CaseName
	if(trim($CaseName)=='')
	{
		$error['Casename']=$iDiscipline['Case_Missing_CaseName'];	
	}

/*	
	#Category
	if(trim($Category)=='')
	{
		$error['Category']=$iDiscipline['Case_Missing_Category'];	
	}
*/	
	#EventDate
	if(trim($EventDate)=='')
	{
		$error['EventDate']=$iDiscipline['Case_Missing_EventDate'];
	}
	else
	{
		// replace split() by explode() - for PHP 5.4
		//list($recorddate_year, $recorddate_month, $recorddate_day) = split('-',$EventDate);
		list($recorddate_year, $recorddate_month, $recorddate_day) = explode('-',$EventDate);
		
		if(strtotime($EventDate)==-1)
		{
			$error['RecordDate'] = $iDiscipline['Award_Punishment_RecordDate_Error'];
		}
		//$today = date("Y-m-d");
		//if(strtotime($EventDate)>strtotime($today))
		//{
		//	$error['RecordDate'] = $iDiscipline['Award_Punishment_RecordDate_In_Future'];
		//}
		else 
		{
			$date_format = false;
			$date_format = $ldiscipline->checkDateFormat($EventDate);
		
			if(!$date_format)
			{
				$error['RecordDate'] = $iDiscipline['Invalid_Date_Format'];			
			}else{
				$NumOfMaxRecordDays = $ldiscipline->MaxRecordDayBeforeAllow;
				if($NumOfMaxRecordDays > 0) {
					$EnableMaxRecordDaysChecking = 1;
					$DateMinLimit = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d")-$NumOfMaxRecordDays, date("Y")));
					$DateMaxLimit = date("Y-m-d");
				}else{
					$EnableMaxRecordDaysChecking = 0;
				}
				
				if($EnableMaxRecordDaysChecking == 1)
				{
					if($DateMinLimit > $EventDate)
					{
						$error['RecordDate'] = $Lang['eDiscipline']['JSWarning']['EventDateIsNotAllow'];
					}else{
						if($EventDate > $DateMaxLimit) {
							$error['RecordDate'] = $Lang['eDiscipline']['JSWarning']['EventDateIsNotAllow'];
						}
					}
				}
			}
		}
		
	}
	
	$year_info = getAcademicYearInfoAndTermInfoByDate($RecordDate);
	$Year = $year_info[1];
	$Semester =  $year_info[3];

	if(trim($Semester)=='')
	{
		$error['No_Semester'] = $eDiscipline['SemesterSettingError'];		
	}	
	
	/*
	#Semester
	$semester_mode = getSemesterMode();
	if($semester_mode==2)
	{
		$Semester = retrieveSemester($EventDate);
		if($Semester=='')
		{
			$error['Semester'] = $eDiscipline["SemesterSettingError"];
		}
	}
	else
	{
		if($Semester=='')
		{
			$error['Semester'] = $iDiscipline['Award_Punishment_Missing_Semester'];
		}
		else
		{
		
			$yearID = Get_Current_Academic_Year_ID();
			$semester_data = getSemesters($yearID);	
			foreach($semester_data as $key=>$val) {
				$SemesterArr[] = $val;	
			}
			
			if(!in_array($Semester,$SemesterArr))
			{
				$error['Wrong_Semester'] = $iDiscipline['Case_Student_Import_No_Semester'];
			}
		}
	}
	*/
	
	#Location
	if(trim($Location)=='')
	{
		$error['Location']=$iDiscipline['Case_Missing_Location'];	
	}
	#PIC
	if(trim($PIC)=='')
	{
		$error['PIC']=$iDiscipline['Case_Missing_PIC'];	
	}
	else
	{
		### checking - valid PIC
		$PicArr = explode(",",$PIC);
		$PicName = array();	
		for($ct=0;$ct<sizeof($PicArr);$ct++)
		{
			$sql = "select UserID as PICID, ".getNamefieldByLang()." as Name from INTRANET_USER where UserLogin = '".trim($PicArr[$ct])."' and RecordType = 1";
			$tmpResult = $lu->returnArray($sql);
			$PicID = $tmpResult[0]['PICID'];	
			if(trim($PicID)=="")
			{
				$error['staff']=$iDiscipline['Good_Conduct_No_Staff'];
			}
			else
			{
				$PicIDArr[] = $PicID;
				$PicName[] = $tmpResult[0]['Name'];
			}
		}	
	}
		
	$css = (sizeof($error)==0) ? "tabletext":"red";

	$x .= "<tr class=\"tablebluerow".($i%2+1)."\">";
	$x .= "<td class=\"$css\">".($i+1)."</td>";
	$x .= "<td class=\"$css\">".$CaseNumber."</td>";
	$x .= "<td class=\"$css\">".$CaseName."</td>";
//	$x .= "<td class=\"$css\">".$Category."</td>";
	$x .= "<td class=\"$css\">".$EventDate."</td>";
	$x .= "<td class=\"$css\">".$Semester."</td>";
	$x .= "<td class=\"$css\">".$Location."</td>";
	$x .= "<td class=\"$css\">". implode(",",$PicName) ."</td>";
	$x .= "<td class=\"$css\">".$Remark."</td>";
	$x .= "<td class=\"$css\">";
	
	if(sizeof($error)>0)
	{
		foreach($error as $Key=>$Value)
		{
			$x .=$Value.'<br/>';
		}	
	}
	else
	{	
		$x .= "--";
	}
	$x.="</td>";
	$x .= "</tr>";

//	$sql = "insert into temp_case_record_import values ('".addslashes($CaseNumber)."','".addslashes($CaseName)."','".addslashes($Category)."',
	$sql = "insert into temp_case_record_import values ('".addslashes($CaseNumber)."','".addslashes($CaseName)."',
	'$EventDate', '".$Semester."', '".addslashes($Location)."' ,'".implode(',',$PicIDArr)."','".implode(",",$PicName)."','".addslashes(addslashes($Remark))."', '". $_SESSION["UserID"] ."')";

	$ldiscipline->db_db_query($sql);


	if($error)
	{
		$error_occured++;
	}	
}
$x .= "</table>";

if($error_occured)
{
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import.php'");
}
else
{	
	$import_button = $linterface->GET_ACTION_BTN($button_import, "submit")." &nbsp;".$linterface->GET_ACTION_BTN($button_back, "button", "window.location='import.php'");	
}

?>

<br />
<form name="form1" method="post" action="import_update.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center">
		<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2"><?=$x?></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>