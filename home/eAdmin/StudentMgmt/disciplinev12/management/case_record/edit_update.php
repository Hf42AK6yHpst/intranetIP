<?php
// Modifying by:

############ Change Log Start #############################
#
#   Date    :   2020-11-24 Bill     [IP30 DM#1014]
#               fixed set incorrect YearTermID to case records
#
############ Change Log End #############################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
if(!($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditAll") || (($ldiscipline->isCasePIC($CaseID) || $ldiscipline->isCaseOwn($CaseID)) && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditOwn"))))
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$CaseNumber = intranet_htmlspecialchars(trim($CaseNumber));
$CaseTitle = intranet_htmlspecialchars(trim($CaseTitle));
$Category = intranet_htmlspecialchars(trim($Category));
$EventDate = intranet_htmlspecialchars(trim($EventDate));
$Location = intranet_htmlspecialchars(trim($Location));
$Remark = intranet_htmlspecialchars(trim($Remark));

$Year = GET_ACADEMIC_YEAR3($EventDate);
if($Semester!='')
{
	$Semester = intranet_htmlspecialchars(trim($Semester));
}
else
{
	//$Semester = getCurrentSemester();
	$Semester = retrieveSemester($EventDate);
}

// [IP30 DM#1014]
//$AcademicYearID = $ldiscipline->getAcademicYearIDByYearName($Year);
//$YearTermID = $ldiscipline->getTermIDByTermName($Semester);
$yearAndTermInfo = getAcademicYearInfoAndTermInfoByDate($EventDate);
$academicYearID = $yearAndTermInfo[0];
$yearTermID = $yearAndTermInfo[2];

if(is_array($PIC))
{
	$IDs = implode(",",$PIC);
}

//set CaseNumber = '$CaseNumber' ,CaseTitle = '$CaseTitle', Category = '$Category', Year='$Year',
$sql = "UPDATE 
			DISCIPLINE_CASE 
		SET 
			CaseNumber = '$CaseNumber',
			CaseTitle = '$CaseTitle', 
			Year = '$Year', 
			AcademicYearID = '$academicYearID',
			Semester = '$Semester', 
			YearTermID = '$yearTermID', 
			EventDate = '$EventDate', 
			Location = '$Location', 
			PICID = '$IDs' ,
			Attachment = '$FolderLocation',
			ModifiedBy = '$UserID',
			RecordStatus = '$status',
			DateModified = now(),
			Remark = '$Remark'
		WHERE 
			CaseID = '$CaseID'";
if($ldiscipline->db_db_query($sql))
{
	$SysMsg = "update";
}
else
{
	$SysMsg = "update_failed";
}	
		
$ReturnPage = "index.php?xmsg=$SysMsg";


intranet_closedb();
header("Location: $ReturnPage");
?>
