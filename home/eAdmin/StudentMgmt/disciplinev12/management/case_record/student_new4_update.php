<?php
// Modifying by:

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libucc.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Case_Record-New");

if(!$CaseID)
{
	header("Location: index.php");
	exit;
}

$lclass = new libclass();
$lc = new libucc();

#############################################################################################################
# A&P record approval/Release is based on Case Finish/Release status, so no need to process
#############################################################################################################
# $approved = $released ? $released : $approved;

foreach($MeritRecordID as $RecordID)
{
	$datainfo = $ldiscipline->RETRIEVE_MERIT_RECORD_INFO_BY_ID($RecordID);
	$thisProfileMeritID = $datainfo[0]['ProfileMeritID'];
	$StudentID = $datainfo[0]['StudentID'];

	$lu = new libuser($StudentID);
	$thisClassNumber = $lu->ClassNumber;
	$thisClassName = $lu->ClassName;

	/*
	#############################################################################################################
	# A&P record approval/Release is based on Case Finish/Release status, so no need to process
	#############################################################################################################
	# approve
	if($approved && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Approval"))
	{
		if($thisProfileMeritID)		# already approved
		{
			# do nothing
		}
		else
		{
			# set APPROVE
			$ldiscipline->APPROVE_MERIT_RECORD($RecordID);
		}
	}

	# release
	if($released && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Release"))
	{
		# UPDATE [DISCIPLINE_MERIT_RECORD] field ReleasedBy, ReleasedDate, RecordStatus = RELEASE
		$ldiscipline->UPDATE_MERIT_RECORD_RELEASE_STATUS($RecordID, DISCIPLINE_STATUS_RELEASED);

		# Send eNotice
		$lc->setNoticeParameter("discipline", time(), $datainfo[0]['TemplateID'], array("StudentName"=>$StudentName[0], "AdditionalInfo"=>$datainfo[0]['TemplateOtherInfo']), $UserID,$UserName[0], 'U',array($datainfo[0]['StudentID']), NOTICE_TO_SOME_STUDENTS);
		$NoticeID = $lc->sendNotice();
		$dataAry = array();
		$dataAry['NoticeID'] = $NoticeID;
		$ldiscipline->UPDATE_MERIT_RECORD($dataAry, $RecordID);
	}
	*/

	# send email
	if($email_PIC || $email_ClassTeacher || $email_DisciplineAdmin)
	{
		$lcampusmail = new libcampusmail();
		$student_name = $lu->UserNameClassNumber();
		$CatID = $ldiscipline->returnCatIDByItemID($datainfo[0]['ItemID']);
		$CatInfo = $ldiscipline->returnMeritItemCategoryByID($CatID);
		$senderID = $_SESSION['UserID'];

		# retrieve email subject and content and other data
		$email_content = $ldiscipline->EMAIL_CONTENT_AWARD_PUNISHMENT($datainfo[0]['MeritType'], $student_name, $CatInfo['CategoryName'], $datainfo[0]['ItemText'], $datainfo[0]['RecordDate']);
		list($subject, $content) = $email_content;
		$senderID = $UserID;
		$receiver = array();

		if($email_PIC)
		{
			$receiver = explode(",",$datainfo[0]['PICID']);
		}
		if($email_ClassTeacher)
		{
			$ClassTeacherID = $lclass->returnClassTeacherID($thisClassName);
			foreach($ClassTeacherID as $k=>$d)
			{
				$receiver[] = $d['UserID'];
			}
		}
		if($email_DisciplineAdmin)
		{
			$DisciplineAdmin = $ldiscipline->GET_ADMIN_USER();
			$receiver = array_merge($receiver, explode(",",$DisciplineAdmin));
		}
		$receiver = array_unique($receiver);

		$lcampusmail->sendMail($receiver, $subject, $content, $senderID);
	}

}

header("Location: view_student_involved.php?CaseID=$CaseID&msg=add");

intranet_closedb();
?>
