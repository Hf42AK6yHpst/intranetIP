<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
 
# Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$ldiscipline = new libdisciplinev12();
$lexport = new libexporttext();

$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Case_Record-View");

$ExportArr = array();

$Semester = $ldiscipline->getTermNameByTermID($Semester);
$extraCriteria = ($Year=='0')? "" : " AND dc.AcademicYearID = $Year";
$extraCriteria .= ($Semester=='' || $Semester==0)? "  " : " AND dc.YearTermID = $Semester ";
if($pic != "") 
	$extraCriteria .= " AND (dc.PICID='$pic' OR dc.PICID LIKE '$pic,%' OR dc.PICID LIKE '%,$pic' OR dc.PICID LIKE '%,$pic,%')";

if($Processing && $Finished)
{
	
}
elseif($Processing)
{
 	$extraCriteria .= " and dc.RecordStatus = ".DISCIPLINE_CASE_RECORD_PROCESSING;
}
elseif($Finished) $extraCriteria .= " and dc.RecordStatus = ".DISCIPLINE_CASE_RECORD_FINISHED;
$extraCriteria .= ($searchStr=='')? "" : " AND (dc.CaseTitle like '%".trim($searchStr)."%' or dc.CaseNumber like '%".trim($searchStr)."%' )";
//		dc.Category,
/*
$sql = "select 
		dc.CaseNumber,
		dc.CaseTitle,
		dc.EventDate,
		".getNameFieldByLang('teacher_iu.')." as TeacherName,
		count(merit_rec.caseID) as StudentsInv,
		dc.DateModified,
		if(dc.RecordStatus=".DISCIPLINE_CASE_RECORD_PROCESSING.",concat('$i_Discipline_System_Discipline_Case_Record_Processing'),
		concat('$i_Discipline_System_Discipline_Case_Record_Processed') ) as RecordStatus
		from DISCIPLINE_CASE as dc
		left join INTRANET_USER as teacher_iu on (dc.PICID = teacher_iu.UserID)
		left join DISCIPLINE_MERIT_RECORD merit_rec on (merit_rec.CaseID = dc.CaseID  AND dc.AcademicYearID=merit_rec.AcademicYearID)
		LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=teacher_iu.UserID)
		LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID=merit_rec.AcademicYearID)
		where 1 $extraCriteria group by dc.CaseID order by dc.CaseNumber";
*/		
$sql = "select 
		dc.CaseNumber,
		dc.CaseTitle,
		dc.EventDate,
		dc.PICID as PIC,
		count(merit_rec.caseID) as StudentsInv,
		dc.DateModified,
		if(dc.RecordStatus=".DISCIPLINE_CASE_RECORD_PROCESSING.",concat('$i_Discipline_System_Discipline_Case_Record_Processing'),
		concat('$i_Discipline_System_Discipline_Case_Record_Processed') ) as RecordStatus
		from DISCIPLINE_CASE as dc
		left join INTRANET_USER as teacher_iu on (dc.PICID = teacher_iu.UserID)
		left join DISCIPLINE_MERIT_RECORD merit_rec on (merit_rec.CaseID = dc.CaseID  AND dc.AcademicYearID=merit_rec.AcademicYearID)
		LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=teacher_iu.UserID)
		LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID=merit_rec.AcademicYearID)
		where 1 $extraCriteria group by dc.CaseID order by dc.CaseNumber";
		
$row = $ldiscipline->returnArray($sql,9);

// Create data array for export
for($i=0; $i<sizeof($row); $i++)
{
	 	
 	$ExportArr[$i][0] = $row[$i]['CaseNumber'];		//CaseNumber
	$ExportArr[$i][1] = $row[$i]['CaseTitle'];		//CaseTitle
	$ExportArr[$i][2] = $row[$i]['EventDate'];		//EventDate
	$ExportArr[$i][3] = $ldiscipline->getPICNameList($row[$i]['PIC']);		//TeacherName
	$ExportArr[$i][4] = $row[$i]['StudentsInv'];		//StudentsInv
//	$ExportArr[$i][5] = $row[$i]['Category'];		//Category
	/*$modifiedDate = strtotime($row[$i]['DateModified']);
	$now = time();
	$dateDiff = $now - $modifiedDate;   
	$fullDays = floor($dateDiff/(60*60*24));
 	$DateModified = $fullDays." $i_Discipline_System_Discipline_Case_Record_days_ago";*/
 	$DateModified = $ldiscipline->convertDatetoDays($row[$i]['DateModified']);
//	$ExportArr[$i][6] = $DateModified;		//DateModified
//	$ExportArr[$i][7] = $row[$i]['RecordStatus'];		//RecordStatus
	$ExportArr[$i][5] = $DateModified;		//DateModified
	$ExportArr[$i][6] = $row[$i]['RecordStatus'];		//RecordStatus
}


//define column title
//$exportColumn = array($i_Discipline_System_Discipline_Case_Record_CaseNumber,$i_Discipline_System_Discipline_Case_Record_Case_Title,$eDiscipline["EventDate"],$i_Discipline_System_Discipline_Case_Record_Case_PIC,$i_Discipline_System_Case_Record_Student_Involved,$i_Discipline_System_Discipline_Case_Record_Category,$i_Discipline_Last_Updated,$eDiscipline["Status"]);
$exportColumn = array($i_Discipline_System_Discipline_Case_Record_CaseNumber,$i_Discipline_System_Discipline_Case_Record_Case_Title,$Lang['eDiscipline']['EventDate'],$i_Discipline_System_Discipline_Case_Record_Case_PIC,$i_Discipline_System_Case_Record_Student_Involved,$i_Discipline_Last_Updated,$eDiscipline["Status"]);


$export_content .= $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

// Output the file to user browser
$filename = 'case_record.csv';
$lexport->EXPORT_FILE($filename, $export_content);

?>
