<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Case_Record-Release");

# update case status
$ldiscipline->UPDATE_CASE_RELEASE_STATUS($CaseID, DISCIPLINE_STATUS_UNRELEASED);

# update all the students' a&p records
$RecordIDAry = $ldiscipline->RETRIEVE_CASE_INVOLVED_MERITID($CaseID);
foreach($RecordIDAry as $MeritRecordID)
{
	# set UNRELEASE
	$isReleased = $ldiscipline->RETRIEVE_MERIT_RECORD_RELEASE_STATUS($MeritRecordID);
	if($isReleased)
	{
		# UPDATE [DISCIPLINE_MERIT_RECORD] field ReleasedBy, ReleasedDate, ReleaseStatus= UNRELEASE
		$ldiscipline->UPDATE_MERIT_RECORD_RELEASE_STATUS($MeritRecordID, DISCIPLINE_STATUS_UNRELEASED);
	}

}

header("Location: view.php?CaseID=$CaseID&xmsg=unrelease");

intranet_closedb();
?>