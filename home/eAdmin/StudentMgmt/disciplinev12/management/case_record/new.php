<?php
# modifying : 

####################### Change Log [start] ###########
#
#	Date:	2016-07-11	Bill
#			change split() to explode(), for PHP 5.4
# 
#	Date:	2010-05-10 YatWoon
#			display the max 5 case number for reference
#
#	Date:	2010-04-14 YatWoon
#			check setting $ldiscipline->OnlyAdminSelectPIC, only admin can select PIC
#
####################### Change Log [end] ###########

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");


intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Case_Record-New");

## Get The max. no of record day(s) ##
$NumOfMaxRecordDays = $ldiscipline->MaxRecordDayBeforeAllow;
if($NumOfMaxRecordDays > 0) {
	$EnableMaxRecordDaysChecking = 1;
	$DateMinLimit = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d")-$NumOfMaxRecordDays, date("Y")));
	$DateMaxLimit = date("Y-m-d");
} else {
	$EnableMaxRecordDaysChecking = 0;
}

# prepare semester array for generating semester selection box
/*$semester_data = getSemesters();
$number = sizeof($semester_data);
$semArr[] = array('0',$ec_iPortfolio['whole_year']);
for ($i=0; $i<$number; $i++)
{
	$linedata[]= split("::",$semester_data[$i]);
	$semArr[] = array($linedata[$i][0],$linedata[$i][0]);
}
if($Semester=='')
{
	$Semester = getCurrentSemester();
}*/


$select_sem_html = "<input type='hidden' name='Semester' value=''>";
/*
$semester_mode = getSemesterMode();
if($semester_mode==1)
{
	$currentYearID = Get_Current_Academic_Year_ID();
	//$select_sem = getSelectSemester2("name=Semester", $Semester, 1, $currentYearID);	# semester in current year
	$select_sem = getSelectSemester("name=Semester", $Semester);	
	
	$select_sem_html = '<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="formfieldtitle">'.$i_Discipline_System_Discipline_Case_Record_Case_Semester.'
								</td>
								<td class="tabletext">'.$select_sem.'
								</td>
							</tr>
						</table>';
}
else
{
	$select_sem_html = "<input type='hidden' name='Semester' value=''>";
}
*/

$catTmpArr = $ldiscipline->TemplateCategory();
if($CategoryID=='')
{
	$CategoryID	= $catTmpArr[0][0];
}

if(is_array($catTmpArr))
{
	foreach($catTmpArr as $Key=>$Value)
	{
		$catArr[] = array($Key,$Key);
	}
}

if(empty($PIC))		$PIC[] = $UserID;
if (is_array($PIC) && sizeof($PIC)>0)
{
	$list = implode(",", $PIC);
	$namefield = getNameFieldWithLoginByLang();
	$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE RecordType = 1 AND UserID IN ($list) AND RecordStatus = 1 ORDER BY $namefield";
	$array_PIC = $ldiscipline->returnArray($sql);
}
$select = $linterface->GET_SELECTION_BOX($array_PIC,"id=\"PIC[]\" name=\"PIC[]\"  multiple='multiple'","");

$sessionTime = session_id()."-".(time());
//$semSelection = $linterface->GET_SELECTION_BOX($semArr, 'id="Semester", name="Semester"', "", $Semester);
$PICSelection = "<table width=\"20%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">
				<tr><td>".$select."
				</td>
				<td valign=\"bottom\">";
				
				if(!$ldiscipline->OnlyAdminSelectPIC || ($ldiscipline->OnlyAdminSelectPIC && $ldiscipline->IS_ADMIN_USER()) ) 
				{
					$PICSelection .= $linterface->GET_BTN($button_select, "button", "newWindow('../choose_pic.php?fieldname=PIC[]', 9)");
					$PICSelection .= $linterface->GET_BTN($button_remove_selected, "button", "checkOptionRemove(document.form1.elements['PIC[]'])");
				}
$PICSelection .= "</td>
				</tr>
				</table>";
$attSelect = $linterface->GET_SELECTION_BOX($a,"id=\"Attachment[]\" name=\"Attachment[]\"  multiple='multiple'","");
$attachment = "<table width=\"20%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">
				<tr><td>".$attSelect."
				</td>
				<td valign=\"bottom\">".$linterface->GET_BTN($button_add, "button", "newWindow('attach.php?FolderLocation=".$sessionTime."', 9)")."
				<br/>".$linterface->GET_BTN($button_remove_selected, "button", "ajaxRemoveAttachment()")."
				</td>
				</tr>
				</table>";
# menu highlight setting
$CurrentPage = "Management_CaseRecord";

$TAGS_OBJ[] = array($i_Discipline_System_Access_Right_Case_Record);

$PAGE_NAVIGATION[] = array($i_Discipline_System_Discipline_Case_Record_Case_List, "index.php");
$PAGE_NAVIGATION[] = array($i_Discipline_System_Discipline_Case_Record_New_Case);

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# generate semester checking js
if($semester_mode==2)
{
	$sem_data = getSemesters();
	$sem_js.="var sem_js = new Array();\n";
	for($i=0;$i<sizeof($sem_data);$i++)
	{
		list($sem_name, $sem_type, $sem_start, $sem_end) = explode("::", $sem_data[$i]);
		$sem_js.="sem_js.push(new Array('$sem_start','$sem_end'));\n";
	}
}
$CaseNumberArr = $ldiscipline->getAllCaseNumber();
$js = "var case_number = new Array();\n";
$js .= "var case_id = new Array();\n";
for($a=0;$a<sizeof($CaseNumberArr);$a++)
{
	$js .="case_number[$a] = '".(($CaseNumberArr[$a]['CaseNumber']))."';\n";
	$js .="case_id[$a]  = '".(($CaseNumberArr[$a]['CaseID']))."';\n";
}
# Start layout
$linterface->LAYOUT_START();

# retrieve the max 5 case number for reference
$c = $ldiscipline->LastCaseNumberForRef();
$CaseNumberReferStr = implode(", ",$c);
?>


<script type="text/javascript">
<?=$sem_js?>
<?=$js?>
var callback_ajaxRemoveAttachment = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    checkOptionRemove(document.form1.elements['Attachment[]']);
	}
}

function ajaxRemoveAttachment()
{
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	var path = "ajax_remove_attachment.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_ajaxRemoveAttachment);
}
function check_semester(objDate)
{
	semester_periods = sem_js;
	
	valid = false;
	if(semester_periods!=null)
	{
		for(j=0;j<semester_periods.length;j++){
			dateStart = semester_periods[j][0];
			dateEnd = semester_periods[j][1];
			if(dateStart <= objDate.value && dateEnd >=objDate.value){
				valid = true;
				break;
			}
		}
	}
	
	if(!valid)
	{
		objDate.focus();
		objDate.className ='textboxnum textboxhighlight';
		alert('<?=$eDiscipline["NoSemesterSettings"]?>');
		return false;
	}
	
	return true;
}

function CompareDates(date_string1, date_string2, EqualDateAllow)
{
	var yr1  = parseInt(date_string1.substring(0,4),10);
	var mon1 = parseInt(date_string1.substring(5,7),10);
	var dt1  = parseInt(date_string1.substring(8,10),10);
	var yr2  = parseInt(date_string2.substring(0,4),10);
	var mon2 = parseInt(date_string2.substring(5,7),10);
	var dt2  = parseInt(date_string2.substring(8,10),10);
	var date1 = new Date(yr1, mon1-1, dt1);
	var date2 = new Date(yr2, mon2-1, dt2);
	
	if(EqualDateAllow == 0)
	{
		if(date2 < date1) {
			return true;
		}else{
			return false;
		}
	}else{
		if(date2 <= date1) {
			return true;
		}else{
			return false;
		}
	}
}

function checkForm(obj) 
{
	
	if(!check_text(obj.CaseNumber,'<?=$i_Discipline_System_Discipline_Case_Record_Case_Number_JS_alert?>'))
	{
		return false
	}
	fieldValue = obj.CaseNumber.value.replace(/'/g, "&#039;").replace(/`/g, "&#039;").replace(/"/g, "&quot;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
	for(a=0;a<case_number.length;a++)
	{
		if(fieldValue==case_number[a])
		{
			alert('<?=$i_Discipline_System_Discipline_Case_Record_Case_Number_Exists_JS_alert?>');
			return false;
		}
	}
	if(!check_text(obj.CaseTitle,'<?=$i_Discipline_System_Discipline_Case_Record_Case_Title_JS_alert?>'))
	{
		return false
	}
	/*if(!check_text(obj.Category,'<?=$i_Discipline_System_Discipline_Case_Record_Category_JS_alert?>'))
	{
		return false
	}*/
	if(!check_text(obj.EventDate,'<?=$i_Discipline_System_Discipline_Case_Record_Event_Date_JS_alert?>'))
	{
		return false
	}else{
		<? if($EnableMaxRecordDaysChecking == 1) { ?>
			// Check Event Date is allow or not
			var DateMinLimit = "<?=$DateMinLimit;?>";
			var DateMaxLimit = "<?=$DateMaxLimit;?>";
			if(!CompareDates(obj.EventDate.value, DateMinLimit, 1))
			{
				alert("<?=$Lang['eDiscipline']['JSWarning']['EventDateIsNotAllow'];?>");
				return false;
			}else{
				if(!CompareDates(DateMaxLimit, obj.EventDate.value, 1))
				{
					alert("<?=$Lang['eDiscipline']['JSWarning']['EventDateIsNotAllow'];?>");
					return false;
				}
			}
		<? } ?>
	}
	if(!check_text(obj.Location,'<?=$i_Discipline_System_Discipline_Case_Record_Location_JS_alert?>'))
	{
		return false
	}
	var PICobj = document.getElementById('PIC[]');
	if(PICobj.length==0)
    {    
	    alert('<?=$i_Discipline_System_Discipline_Case_Record_PIC_JS_alert?>');
	    return false
 	}
 	if (!check_date(obj.EventDate,"<?php echo $i_invalid_date; ?>")) return false;
 	if (compareDate(obj.EventDate.value, '<?=date('Y-m-d')?>') > 0) 
 	{
	 	alert ("<?php echo $i_invalid_date; ?>");
	 	return false;
	}
	// Semester
	<? if($semester_mode==2) {?>
	if(!check_semester(obj.EventDate))
	{
		return false;
	}
	<? } ?>
		
    checkOptionAll(obj.elements["Attachment[]"]);
    checkOptionAll(obj.elements["PIC[]"]);
	return true
}
</script>
<form name="form1" method="post" action="new_update.php" onSubmit="return checkForm(form1)">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>

					<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<label for="grading_passfail" class="tabletext"><em class="form_sep_title"> - <?=$i_Discipline_System_Discipline_Case_Record_Case_Details?>-</em></label>
							</tr>
						</table>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Discipline_Case_Record_Case_Number?>
								<span class="tabletextrequire">* </span></td>
								<td class="tabletext"><INPUT type="text" name="CaseNumber" id="CaseNumber" class="tabletext" maxlength="50">
								(<?=$Lang['eDiscipline']['LastCaseNumberRef']?>: <?=$CaseNumberReferStr?>)
								</td>
							</tr>
						</table>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Discipline_Case_Record_Case_Title?>
								<span class="tabletextrequire">* </span></td>
								<td class="tabletext"><input name="CaseTitle" id="CaseTitle" type="text" class="textboxtext" value="">
								</td>
							</tr>
						</table>
						<!--<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Discipline_Case_Record_Category?>
								<span class="tabletextrequire">* </span></td>
								<td class="tabletext"><INPUT type="text" name="Category" id="Category" class="tabletext">
								</td>
							</tr>
						</table>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Discipline_Case_Record_Case_School_Year?>
								</td>
								<td class="tabletext"><INPUT type="text" name="Year" id="Year" class="tabletext" value="<?=$Year = getCurrentAcademicYear();?>">
								</td>
							</tr>
						</table>-->
						
								<?=$select_sem_html?>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Discipline_Case_Record_Case_Event_Date?>
								<span class="tabletextrequire">* </span></td>
								<td class="tabletext"><?=$linterface->GET_DATE_PICKER("EventDate",$EventDate)?>
								</td>
							</tr>
						</table>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Discipline_Case_Record_Case_Location?>
								<span class="tabletextrequire">* </span></td>
								<td class="tabletext"><input name="Location" id="Location" type="text" class="textboxtext" value="">
								</td>
							</tr>
						</table>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Discipline_Case_Record_Case_PIC?>
								<span class="tabletextrequire">* </span></td>
								<td class="tabletext"><?=$PICSelection?>
								</td>
							</tr>
						</table>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Discipline_Case_Record_Attachment?>
								</td>
								<td class="tabletext"><?=$attachment?>
								</td>
							</tr>
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_UserRemark?></td>
								<td class="tabletext"><?=$linterface->GET_TEXTAREA("Remark", $Remark)?></td>
							</tr>
							
						</table>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr>
								<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
								<td width="80%">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<span class="dotline">
									<?= $linterface->GET_ACTION_BTN($button_submit, "submit","","submit")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'")?>
									</span></td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<input type="hidden" id="FolderLocation" name="FolderLocation" value="<?=$sessionTime?>">
</form>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>


