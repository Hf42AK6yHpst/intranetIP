<?php
# using: yat


/********************** Change Log ***********************/
#
#	Date:	2010-04-14 YatWoon
#			check setting $ldiscipline->OnlyAdminSelectPIC, only admin can select PIC
#
/******************* End Of Change Log *******************/
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$lnotice = new libnotice();
# Check access right
if(!(($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditOwn") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditAll")) && ($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditOwn") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditAll")))) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}


if(!$CaseID)
{
	header("Location: index.php");
	exit;
}

$linterface = new interface_html();
$lteaching = new libteaching();

$NoticeTemplateAva = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO("",1);

# menu highlight setting
$CurrentPage = "Management_CaseRecord";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($i_Discipline_System_Access_Right_Case_Record);

# navigation bar
$PAGE_NAVIGATION[] = array($i_Discipline_System_Access_Right_Case_Record, "view.php?CaseID=$CaseID");
$PAGE_NAVIGATION[] = array($eDiscipline["EditRecord"]);

## Get The max. no of record day(s) ##
$NumOfMaxRecordDays = $ldiscipline->MaxRecordDayBeforeAllow;
if($NumOfMaxRecordDays > 0) {
	$EnableMaxRecordDaysChecking = 1;
	$DateMinLimit = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d")-$NumOfMaxRecordDays, date("Y")));
	$DateMaxLimit = date("Y-m-d");
} else {
	$EnableMaxRecordDaysChecking = 0;
}

$RecordID = (is_array($RecordID)) ? $RecordID[0] : $RecordID;

if(empty($RecordID))	
	header("Location: view_student_involved.php?CaseID=".$CaseID);

$resultAry = $ldiscipline->getMeritRecordFromCaseID($RecordID);
$StudentID = $resultAry['StudentID'];
		
# build html part
$selected_student_table = "<table width='100%' border='0' cellspacing='0' cellpadding='5'>";

$lu = new libuser($StudentID);
$student_name = $lu->UserNameLang();
$class_name = $lu->ClassName;
$class_no = $lu->ClassNumber;

$selected_student_table .= "<tr><td width='50%' valign='top'><a href='javascript:viewCurrentRecord($StudentID)' class='tablelink'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_stu_ind.gif' width='20' height='20' border='0' align='absmiddle'>";
$selected_student_table .= $class_name. ($class_no?"-":"") .$class_no." ".$student_name;
$selected_student_table .= "</a></td></tr>";
$selected_student_table .= "</table>";

/*
$semester_mode = getSemesterMode();
$semester = $resultAry['Semester'];

if($semester_mode==1)
{
	$select_sem = getSelectSemester("name=semester", $semester);	
	$select_sem_html = "
		<tr>
			<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\">{$i_SettingsSemester}</td>
			<td valign=\"top\">{$select_sem}</td>
		</tr>
	";
}
else
{
	$select_sem_html = "<input type='hidden' name='semester' value='{$semester}'>";
}
*/
$select_sem_html = "<input type='hidden' name='semester' id='semester' value='{$semester}'>";

$RecordDate = $resultAry['RecordDate'];

# Last PIC selection
$PIC = $resultAry['PICID'];

if ($PIC != '')
{
	//$list = implode(",", $PIC);
	$namefield = getNameFieldWithLoginByLang();
	$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE RecordType = 1 AND UserID IN ($PIC) AND RecordStatus = 1 ORDER BY $namefield";
	$array_PIC = $ldiscipline->returnArray($sql);
}
$PIC_selected = $linterface->GET_SELECTION_BOX($array_PIC, "name='PIC[]' ID='PIC[]' class='select_studentlist' size='6' multiple='multiple'", "");

# Subject Selection
if ($ldiscipline->use_subject)
{
	/*
	$subjectList = $lteaching->returnSubjectList();

    $select_subject = "<SELECT name='Subject'>\n";
    $select_subject .= "<OPTION value=''>-- $i_notapplicable --</OPTION>\n";
    foreach ($subjectList AS $k=>$d)
    {
        $select_subject .= "<OPTION value='". $d['SubjectID'] ."'";
        $select_subject .= ($d['SubjectName']==$resultAry['Subject']) ? " selected" : "";
        $select_subject .= ">". $d['SubjectName'] ."</OPTION>\n";
    }
    $select_subject .= "</SELECT>\n";
    */
    
    $SubjectID = $resultAry['SubjectID'];
    $subjectList = $lteaching->returnSubjectList();
    $select_subject = "<SELECT name='SubjectID' id='SubjectID'>\n";
    $select_subject .= "<OPTION value=''>-- $i_notapplicable --</OPTION>\n";
    foreach ($subjectList AS $k=>$d)
    {
	    $selected = $SubjectID == $d[0] ? " selected":"";
        $select_subject .= "<OPTION value='". $d[0] ."' $selected>". $d[1] ."</OPTION>\n";
    }
    $select_subject .= "</SELECT>\n";
}

$remark = $resultAry['Remark'];
    
$record_type = $resultAry['MeritType'];
$ProfileMeritType = $resultAry['ProfileMeritType'];
$MeritNum = $resultAry['ProfileMeritCount'];
$ConductScore = abs($resultAry['ConductScoreChange']);
$StudyScore = abs($resultAry['SubScore1Change']);

$ProfileMeritType = $ProfileMeritType ? $ProfileMeritType : 0;
$MeritNum = $MeritNum ? $MeritNum : 0;

# Get Category selection
$merit_cats = $ldiscipline->returnMeritItemCategoryByType(1);
$demerit_cats = $ldiscipline->returnMeritItemCategoryByType(-1);

$items = $ldiscipline->retrieveMeritItemswithCodeGroupByCat();

# Merit Count selection
$select_merit_num = "<SELECT name=MeritNum id=MeritNum>\n";
$AP_Interval = $ldiscipline->AP_Interval_Value ? $ldiscipline->AP_Interval_Value : 1;
for ($i=0; $i<=$ldiscipline->AwardPunish_MAX; $i=$i+$AP_Interval)
{
     $select_merit_num .= "<OPTION value=".$i;
     $select_merit_num .= ($i==$resultAry['ProfileMeritCount']) ? " selected" : "";
     $select_merit_num .= ">{$i}</OPTION>\n";
}
$select_merit_num .= "</SELECT>\n";

# Conduct Mark selection
$select_conduct_mark = "<SELECT name='ConductScore' id='ConductScore'>\n";
for ($i=0; $i<=$ldiscipline->ConductMarkIncrement_MAX; $i++)
{
     $select_conduct_mark .= "<OPTION value=".$i;
     $select_conduct_mark .= ($i==$resultAry['ConductScoreChange']) ? " SELECTED" : "";
     $select_conduct_mark .= ">$i</OPTION>\n";
}
$select_conduct_mark .= "</SELECT>\n";

# Subscore selection
$select_study_score = "<SELECT name='StudyScore' id='StudyScore'>\n";
for ($i=0; $i<=$ldiscipline->SubScoreIncrement_MAX; $i++)
{
	 $select_study_score .= "<OPTION value=".$i;
     $select_study_score .= ($i==$resultAry['SubScore1Change']) ? " SELECTED" : "";
     $select_study_score .= ">$i</OPTION>\n";
     
}
$select_study_score .= "</SELECT>\n";

$merit_record_type = $ldiscipline->returnValidMeritTypeFieldWithOrder(1);
$demerit_record_type = $ldiscipline->returnValidMeritTypeFieldWithOrder(-1);
 
$ItemID = $resultAry['ItemID'];
$CatID = $ldiscipline->getCategoryIDFromItemID($ItemID);

# check user can access/edit this item or not
$AccessItem = 0;
if($ldiscipline->IS_ADMIN_USER($UserID) || !$ItemID)
{
	$AccessItem = 1;
}
else
{
	$ava_item = $ldiscipline->Retrieve_User_Can_Access_AP_Category_Item($UserID, 'item');
	if($ava_item[$CatID])
		$AccessItem = in_array($ItemID, $ava_item[$CatID]);
}

########## For eNotice (Begin) ##########

$catTmpArr = $ldiscipline->TemplateCategory();
if(is_array($catTmpArr))
{
	$catArr[0] = array('0',$i_Discipline_Select_Category);
	foreach($catTmpArr as $Key=>$Value)
	{
		# check the Template Catgory has template or not
		$NoticeTemplateAvaTemp = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO("",1, $Key);
		if(!empty($NoticeTemplateAvaTemp))
		$catArr[] = array($Key,$Key);
	}
}

$eNoticeSubCategory = $resultAry['TemplateID'];
$additionInfo = $resultAry['TemplateOtherInfo'];
$eNoticeSubCategory = ($eNoticeSubCategory == '') ? '0' : $eNoticeSubCategory;
$sql = "SELECT CategoryID FROM INTRANET_NOTICE_MODULE_TEMPLATE WHERE TemplateID=$eNoticeSubCategory";
$temp = $ldiscipline->returnVector($sql);
$eNoticeCategory = $temp[0];
if($eNoticeSubCategory != '' && $eNoticeSubCategory != 0) {
	$eNoticeTemplateChecked = "checked";
	$eNoticeTemplateVisibility = "inline";
} else {
	$eNoticeTemplateChecked = "";
	$eNoticeTemplateVisibility = "none";	
}

$catSelection0 = $linterface->GET_SELECTION_BOX($catArr, 'id="CategoryID0" name="CategoryID0" onChange="changeCatTemplate(this.value, 0)"', "", $eNoticeCategory);

// Preset template items (for javascript)
for ($i=0; $i<sizeof($catArr); $i++) {
	$result = $ldiscipline->getDetentionENoticeByCategoryID("DISCIPLINE", $catArr[$i][0]);
	
	for ($j=0; $j<sizeof($result); $j++) {
		if($catArr[$i][0]==$eNoticeCategory) {
			$optionSubCategory .= "<option value='{$result[$j][0]}'";
			$optionSubCategory .= ($eNoticeSubCategory==$result[$j][0]) ? " selected" : "";
			$optionSubCategory .= ">{$result[$j][1]}</option>";
		}
		
		if ($j==0) {
			$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]."Value = new Array(".sizeof($result).");\n";
			$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]." = new Array(".sizeof($result).");\n";
			$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]."Selected = new Array(".sizeof($result).");\n";
		}
		$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."Value[$j] = \"".$result[$j][0]."\"\n";
		$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."[$j] = \"".$result[$j][1]."\"\n";
		$jTemplateString .= ($eNoticeSubCategory==$result[$j][0]) ? ("jArrayTemplate".$catArr[$i][0]."Selected[$j] = \"1\"\n") : ("jArrayTemplate".$catArr[$i][0]."Selected[$j] = \"0\"\n");
	}
}

########## For eNotice (End) ##########

/*
# generate semester checking js
if($semester_mode==2)
{
	$sem_data = getSemesters();
	$sem_js.="var sem_js = new Array();\n";
	for($i=0;$i<sizeof($sem_data);$i++)
	{
		list($sem_name, $sem_type, $sem_start, $sem_end) = split("::", $sem_data[$i]);
		$sem_js.="sem_js.push(new Array('$sem_start','$sem_end'));\n";
	}
}
*/
# Start layout
$linterface->LAYOUT_START();

?>

<script type="text/javascript" src = "<?=$PATH_WRT_ROOT?>templates/2007a/js/jslb_ajax.js" charset = "utf-8"></script>

<SCRIPT LANGUAGE=Javascript>
<?=$jTemplateString?>

<?//=$sem_js?>

// Generate Merit Category
var merit_cat_select = new Array();
merit_cat_select[0] = new Option("-- <?=$button_select?> --",0);
<?	foreach($merit_cats as $k=>$d)	{ ?>
		merit_cat_select[<?=$k+1?>] = new Option("<?=str_replace('"', '\"', $d['CategoryName'])?>",<?=$d['CatID']?>);
<?	} ?>

// Generate Demerit Category
var demerit_cat_select = new Array();
demerit_cat_select[0] = new Option("-- <?=$button_select?> --",0);
<?	foreach($demerit_cats as $k=>$d)	{ ?>
		demerit_cat_select[<?=$k+1?>] = new Option("<?=str_replace('"', '\"', $d['CategoryName'])?>",<?=$d['CatID']?>);
<?	} ?>

// Generate Merit Type
var merit_record_type = new Array();
<?	foreach($merit_record_type as $k=>$d)	{ ?>
		merit_record_type[<?=$k?>] = new Option("<?=$d[1]?>",<?=$d[0]?>,<?=($d[0]==$ProfileMeritType?"1":"0")?>);
<?	} ?>

// Generate Demerit Type
var demerit_record_type = new Array();
<?	foreach($demerit_record_type as $k=>$d)	{ ?>
		demerit_record_type[<?=$k?>] = new Option("<?=$d[1]?>",<?=$d[0]?>,<?=($d[0]==$ProfileMeritType?"1":"0")?>);
<?	} ?>

function viewCurrentRecord(id)
{
         newWindow('../award_punishment/viewcurrent.php?StudentID='+id, 8);
}

function changeMeritType(val) 
{
	var obj = document.form1;
	var cat_select = document.getElementById('CatID');
	var item_select = document.getElementById('ItemID');
	var meritType = document.getElementById('MeritType');
	var increment_de = document.getElementById("increment_de");
	var tr_RecordItem = document.getElementById("tr_RecordItem");
	var tr_MeritDemerit = document.getElementById("tr_MeritDemerit");
	var tr_Conduct_Mark = document.getElementById("tr_Conduct_Mark");
	var tr_SubScore = document.getElementById("tr_SubScore");

	while (item_select.options.length > 0)
	{
	    item_select.options[0] = null;
	}
	
	item_select.options[0] = new Option("-- <?=$button_select?> --",0);
	
	if(obj.record_type[0].checked)		// Award
	{
		cat_select.options.length = merit_cat_select.length;
		for(i=0;i<merit_cat_select.length;i++)
		{
			cat_select.options[i] = new Option(merit_cat_select[i].text,merit_cat_select[i].value);	
			<? if($CatID) {?>
			if(merit_cat_select[i].value == <?=$CatID?>) {
				cat_select.selectedIndex = i;
			}
			<? } ?>
		}
			
		meritType.options.length = merit_record_type.length;
		meritType.options[0]=new Option("--", "-999");	
		for(i=1;i<merit_record_type.length;i++) { 
			meritType.options[i]=new Option(merit_record_type[i].text,merit_record_type[i].value);	
 			if(merit_record_type[i].value == <?=$ProfileMeritType?>) 	meritType.selectedIndex = i;

		}
			
		document.getElementById('increment_de').innerHTML = "<?=$Lang['eDiscipline']['Gain']?>";
		document.getElementById('MeritNum').selectedIndex = <?=$MeritNum?>;
		document.getElementById('ConductScore').selectedIndex = <?=$ConductScore?>;
		document.getElementById('StudyScore').selectedIndex = <?=$StudyScore?>;
	}
	else								// Punishment
	{
		cat_select.options.length = demerit_cat_select.length;
		
		for(i=0;i<demerit_cat_select.length;i++)
		{
			cat_select.options[i]=new Option(demerit_cat_select[i].text,demerit_cat_select[i].value);
			<? if($CatID) {?>
			if(demerit_cat_select[i].value == <?=$CatID?>) 	cat_select.selectedIndex = i;
			<? } ?>
		}
			
		meritType.options.length = demerit_record_type.length;
		meritType.options[0]=new Option("--", "-999");	
		for(i=1;i<demerit_record_type.length;i++) {
			meritType.options[i]=new Option(demerit_record_type[i].text,demerit_record_type[i].value);	
 			//if(demerit_record_type[i].value == <?=$ProfileMeritType?>) 		meritType.selectedIndex = i-1;
		}
			
		document.getElementById('increment_de').innerHTML = "<?=$Lang['eDiscipline']['Deduct']?>";
		document.getElementById('MeritNum').selectedIndex = <?=$MeritNum ?>;
		document.getElementById('ConductScore').selectedIndex = <?=$ConductScore?>;
		document.getElementById('StudyScore').selectedIndex = <?=$StudyScore?>;
	}
	
	if(obj.record_type[2].checked)
	{
		
		tr_RecordItem.style.display="none";
		tr_MeritDemerit.style.display="none";
		tr_Conduct_Mark.style.display="none";
		tr_SubScore.style.display="none";
	}
	else
	{
		if(navigator.appName == 'Microsoft Internet Explorer')
			display_var = "block";
		else
			display_var = "table-row";
		
		tr_RecordItem.style.display=display_var;
		tr_MeritDemerit.style.display=display_var;
		<? if(!$ldiscipline->Hidden_ConductMark) {?>
		tr_Conduct_Mark.style.display=display_var;
		<? } ?>
		<? if($ldiscipline->UseSubScore) {?>
		tr_SubScore.style.display=display_var;
		<? } ?>
	}
}

function changeCat(value)
{
	
	var item_select = document.getElementById('ItemID');
	//var selectedCatID = document.getElementById('CatID').value;
	var selectedCatID = value;
	
	while (item_select.options.length > 0)
	{
	    item_select.options[0] = null;
	}
	
	item_select.options[0] = new Option("-- <?=$button_select?> --",0);
	
	if (selectedCatID == '')
	{
	<?
		$curr_cat_id = "";
		$pos = 1;
		for ($i=0; $i<sizeof($items); $i++)
		{
	     list($r_catID, $r_itemID, $r_itemName) = $items[$i];
	     $r_itemName = intranet_undo_htmlspecialchars($r_itemName);
	     $r_itemName = str_replace('"', '\"', $r_itemName);
	     $r_itemName = str_replace("'", "\'", $r_itemName);
	     if ($r_catID != $curr_cat_id)
	     {
	         $pos = 1;
	?>
     }
	else if (selectedCatID == "<?=$r_catID?>")
	{
	     <?
	         $curr_cat_id = $r_catID;
	     }
	     ?>
		item_select.options[<?=$pos++?>] = new Option("<?=$r_itemName?>",<?=$r_itemID?>);
		<?	if($r_itemID == $ItemID) {	?>
			item_select.selectedIndex = <?=$pos-1?>;
		<? } ?>
		<?
	}
	?>
     }
}

function changeItem(value)
{
	var i;
	var meritType = document.getElementById('MeritType');
	var meritNum = document.getElementById('MeritNum');
	var conductScore = document.getElementById('ConductScore');
	<? if ($ldiscipline->UseSubScore) { ?>
		var subScore1 = document.getElementById('StudyScore');
	<? } ?>
	<?
	for ($i=0; $i<sizeof($items); $i++)
	{
     list($r_catID, $r_itemID, $r_itemName, $r_meritType, $r_meritNum, $r_conduct, $r_punish, $r_detention, $r_subscore1) = $items[$i];
     ?>
     if (value=="<?=$r_itemID?>")
     {
		if(document.form1.temp_flag.value==1)
		{
			check_meritType = <?=$r_meritType?>;
			check_meritNum = <?=$r_meritNum?>;
			check_conductScore = <?=$r_conduct?>;
			check_studyScore = <?=($r_subscore1?$r_subscore1:0)?>;
		}
		else
		{
			check_meritType = <?=($ProfileMeritType?$ProfileMeritType:0) ?>;
			check_meritNum = <?=($MeritNum?$MeritNum:0)?>;
			check_conductScore = <?=($ConductScore?$ConductScore:0)?>;
			check_studyScore = <?=($StudyScore?$StudyScore:0)?>;
		}
		
         // Merit Type
         for (i=0; i<meritType.options.length; i++)
         {
	         
              if (meritType.options[i].value==check_meritType)
              {
	              
                  meritType.selectedIndex = i;
                  break;
              }
         }
         // Merit Num
         for (i=0; i<meritNum.options.length; i++)
         {
              if (meritNum.options[i].value==check_meritNum)
              {
                  meritNum.selectedIndex = i;
                  break;
              }
         }

         // Conduct Score
         for (i=0; i<conductScore.options.length; i++)
         {
	         
              if (conductScore.options[i].value==check_conductScore)
              {
                  conductScore.selectedIndex = i;
                  break;
              }
         }

         <? if ($ldiscipline->UseSubScore) { ?>
          // Sub Score
         for (i=0; i<subScore1.options.length; i++)
         {
	         
              if (subScore1.options[i].value==check_studyScore)
              {
                  subScore1.selectedIndex = i;
                  break;
              }
         }
         <? } ?>
     }
        <?
}
?>

	document.form1.temp_flag.value=1;
}

function changeCatTemplate(cat, selectIdx){
	var x = document.getElementById("SelectNotice"+selectIdx);
	var listLength = x.length;
	for (var i = listLength-1; i>=0; i--) {
		x.remove(i);
	}

	if (cat != 0) {
		for (var j=0; j<eval("jArrayTemplate"+cat).length; j++) {
			var y = document.createElement('option');
			y.text = eval("jArrayTemplate"+cat)[j];
			y.value = eval("jArrayTemplate"+cat+"Value")[j];
			//y.selected = eval("jArrayTemplate"+cat+"Selected")[j];
			if(<?=$eNoticeSubCategory?>==y.value)
				y.selected = "selected";

			try {
				x.add(y,null); // standards compliant
			} catch(ex) {
				x.add(y); // IE only
			}
		}
	}
}

function CompareDates(date_string1, date_string2, EqualDateAllow)
{
	var yr1  = parseInt(date_string1.substring(0,4),10);
	var mon1 = parseInt(date_string1.substring(5,7),10);
	var dt1  = parseInt(date_string1.substring(8,10),10);
	var yr2  = parseInt(date_string2.substring(0,4),10);
	var mon2 = parseInt(date_string2.substring(5,7),10);
	var dt2  = parseInt(date_string2.substring(8,10),10);
	var date1 = new Date(yr1, mon1-1, dt1);
	var date2 = new Date(yr2, mon2-1, dt2);
	
	if(EqualDateAllow == 0)
	{
		if(date2 < date1) {
			return true;
		}else{
			return false;
		}
	}else{
		if(date2 <= date1) {
			return true;
		}else{
			return false;
		}
	}
}

function check_form()
{
	obj = document.form1;
	
	// Record Date
	if(!check_date(obj.RecordDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))
	{
		return false;
	}else{
		<? if($EnableMaxRecordDaysChecking == 1) { ?>
			// Check Event Date is allow or not
			var DateMinLimit = "<?=$DateMinLimit;?>";
			var DateMaxLimit = "<?=$DateMaxLimit;?>";
			if(!CompareDates(obj.RecordDate.value, DateMinLimit, 1))
			{
				alert("<?=$eDiscipline['JSWarning']['RecordDateIsNotAllow'];?>");
				return false;
			}else{
				if(!CompareDates(DateMaxLimit, obj.RecordDate.value, 1))
				{
					alert("<?=$eDiscipline['JSWarning']['RecordDateIsNotAllow'];?>");
					return false;
				}
			}
		<? } ?>
	}
	
	// Semester
	<?/* if($semester_mode==2) {?>
	if(!check_semester(obj.RecordDate))
	{
		return false;
	}
	<? } */?>
	
	// Teacher/Staff
	objPic = document.getElementById('PIC[]');
	checkOption(objPic);
	if (objPic.length==0) {
		checkOptionAdd(objPic, "<? for($i = 0; $i < 40; $i++) echo " "; ?>", "");
		alert("<?=$i_alert_pleaseselect.$i_Discipline_Duty_Teacher?>");
		return false;
	}
	checkOptionAll(objPic);
	
	<? if($AccessItem) {?>
	/* check the merit type 0 - N.A. */
	if(document.getElementById('MeritNum').value==0)	document.getElementById('MeritType').selectedIndex = 0;
	if(document.getElementById('MeritType').value==-999)	document.getElementById('MeritNum').selectedIndex = 0;
	
	if(!obj.record_type[2].checked)
	{
		// Category / Items
	    if(document.getElementById('CatID').value == 0 || document.getElementById('ItemID').value == 0)
	    {
	            alert('<?=$i_alert_pleaseselect.$i_Discipline_System_general_record?>');
	            return false;
	    }
    
	    <?if (!$lnotice->disabled && $record_type!=0 && !empty($NoticeTemplateAva))
		{?>
	    // eNotice
	    if(document.getElementById('action_notice').checked==true && document.getElementById('SelectNotice0').value==0) {
		 	alert('<?=$i_alert_pleaseselect.$i_Discipline_Template?>');   
		 	return false;
	    }
	    <?}?>
		<? /*if(!$ldiscipline->Hidden_ConductMark) {?>
	    else if(document.getElementById('ConductScore').value == 0 && document.getElementById('MeritNum').value == 0)
	    {
	        if (window.confirm('<?=$eDiscipline['no_ConductScore_MeritNum_selected'] ?>'))
	        {
	            obj.action='student_involved_edit_update.php';
	            return true;
	        }
	        return false;
	    }
	    <? } */?>
    // checking on MeritNum, ConductScore & Subscore (if any)
    <? if($ldiscipline->NotAllowEmptyInputInAP) { ?>
	    <? 
	    if(!$ldiscipline->Hidden_ConductMark) {
			if($ldiscipline->UseSubScore) {
				$jsScript = ' && obj.elements["StudyScore"].value == 0';	
				$alertMsg = $eDiscipline['MeritDemerit'].", ".$eDiscipline['Conduct_Mark']." ".$i_alert_or." ".$i_Discipline_System_Subscore1;
			} else {
				$alertMsg = $eDiscipline['MeritDemerit']." ".$i_alert_or." ".$eDiscipline['Conduct_Mark'];
			}
			$alertMsg = $i_alert_pleaseselect.$alertMsg;
		?>
	    else if(obj.elements["ConductScore"].value == 0 && obj.elements["MeritNum"].value == 0 <?=$jsScript?>)
	    {
		    alert("<?=$alertMsg ?>");
        	return false;
	    }
	    <? } else {
			if($ldiscipline->UseSubScore) {
				$jsScript = ' && obj.elements["StudyScore"].value == 0';	
				$alertMsg = $eDiscipline['MeritDemerit']." ".$i_alert_or." ".$i_Discipline_System_Subscore1;
			} else {
				$alertMsg = $eDiscipline['MeritDemerit'];
			}
			$alertMsg = $i_alert_pleaseselect.$alertMsg;
		    ?>
	    else if(obj.elements["MeritNum"].value == 0 <?=$jsScript?>)
	    {
		    alert("<?=$alertMsg ?>");
        	return false;
	    }
	    <? } ?>
    <? } else { ?>
    	// allow null input
	    <? 
	    if(!$ldiscipline->Hidden_ConductMark) {
			if($ldiscipline->UseSubScore) {
				$jsScript = ' && obj.elements["StudyScore"].value == 0';	
				$alertMsg = $eDiscipline['MeritDemerit'].", ".$eDiscipline['Conduct_Mark']." ".$i_alert_or." ".$i_Discipline_System_Subscore1;
			} else {
				$alertMsg = $eDiscipline['MeritDemerit']." ".$i_alert_or." ".$eDiscipline['Conduct_Mark'];
			}
			if($intranet_session_language=="en")
				$alertMsg = $Lang['eDiscipline']['EmptyInput']." ".$alertMsg." ".$Lang['eDiscipline']['IsSelected'].$button_confirm."?";
			else 
				$alertMsg = $Lang['eDiscipline']['IsSelected'].$alertMsg.$Lang['eDiscipline']['sdbnsm_fullStop'].$button_confirm."?";
		    ?>
	    else if(obj.elements["ConductScore"].value == 0 && obj.elements["MeritNum"].value == 0 <?=$jsScript?>)
	    {
		    if (window.confirm('<?=$alertMsg?>'))
	        {
	            obj.action='edit_update.php';
	            //return true;
	        }
	        else
	        {
	        	return false;
	    	}
	    }
	    <? } else { 
			if($ldiscipline->UseSubScore) {
				$jsScript = ' && obj.elements["StudyScore"].value == 0';	
				$alertMsg = $eDiscipline['MeritDemerit']." ".$i_alert_or." ".$i_Discipline_System_Subscore1;
			} else {
				$alertMsg = $eDiscipline['MeritDemerit'];
			}
			if($intranet_session_language=="en")
				$alertMsg = $Lang['eDiscipline']['EmptyInput']." ".$alertMsg." ".$Lang['eDiscipline']['IsSelected'].$button_confirm."?";
			else 
				$alertMsg = $Lang['eDiscipline']['IsSelected'].$alertMsg.$Lang['eDiscipline']['sdbnsm_fullStop'].$button_confirm."?";
		    ?>
	    else if(obj.elements["MeritNum"].value == 0 <?=$jsScript?>)
	    {
		    if (window.confirm('<?=$alertMsg?>'))
	        {
	            obj.action='new3.php';
	            //return true;
	        }
	        else
	        {
	        	return false;
	    	}
	    }
	    <? } ?>
    <? } ?>
    }
    <? } ?>
    obj.action='student_involved_edit_update.php';
}

function goreset() {
	document.form1.reset();
	
	<? if($AccessItem) {?>
	changeMeritType("<?=$ProfileMeritType?>");
	changeCat(<?=$CatID?>);
	changeItem(<?=$ItemID?>);	
	<? } ?>
	
	<? if (!$lnotice->disabled && $record_type!=0 && !empty($NoticeTemplateAva)) {?>
	changeCatTemplate(document.form1.CategoryID0.value, 0);
	if(document.form1.action_notice.checked==true) {
		document.getElementById('noticeFlag').value='YES';
		showDiv('divNotice');	
	} else {
		document.getElementById('noticeFlag').value='NO';
		hideDiv('divNotice');	
	}
	<?}?>
}


function showDiv(div)
{
	document.getElementById(div).style.display="inline";
}

function hideDiv(div)
{
	document.getElementById(div).style.display="none";
}

function check_semester(objDate)
{
	semester_periods = sem_js;
	
	valid = false;
	if(semester_periods!=null)
	{
		for(j=0;j<semester_periods.length;j++){
			dateStart = semester_periods[j][0];
			dateEnd = semester_periods[j][1];
			if(dateStart <= objDate.value && dateEnd >=objDate.value){
				valid = true;
				break;
			}
		}
	}
	
	if(!valid)
	{
		objDate.focus();
		objDate.className ='textboxnum textboxhighlight';
		alert('<?=$eDiscipline["NoSemesterSettings"]?>');
		return false;
	}
	
	return true;
}

</SCRIPT>

<form name="form1" method="get" onsubmit="return check_form();">

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td class="navigation">
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
		</table>
	</td>
</tr>
<!--
<tr>
	<td align="center"><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
</tr>
//-->
<tr>
	<td>
		<table align="center" width="90%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td valign="top" colspan="2" align="right"><?=$linterface->GET_SYS_MSG($msg, $xmsg)?></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="tablerow2"><?=$i_general_students_selected?></td>
			<td valign="top" class="tablerow2"><?=$selected_student_table?></td>
		</tr>
		<tr>
			<td colspan="2" valign="middle" nowrap="nowrap" class="form_sep_title"><i> - <?=$eDiscipline["BasicInfo"]?> -</i></td>
		</tr>
		
		<?=$select_sem_html?>
		
		<tr>
                <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_EventDate?><span class="tabletextrequire">* </span></td>
                <td> <?= $linterface->GET_DATE_FIELD("theRecordDate", "form1", "RecordDate", ($RecordDate ? $RecordDate : date('Y-m-d')), 1)?></td>
        </tr>
        
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Contact_PIC?><span class="tabletextrequire">* </span></td>
			<td valign="top">
				<table>
				<tr>
					<td><?=$PIC_selected?>
					</td>
					<td valign="bottom">
					<? if(!$ldiscipline->OnlyAdminSelectPIC || ($ldiscipline->OnlyAdminSelectPIC && $ldiscipline->IS_ADMIN_USER()) ) { ?>
						<?=$linterface->GET_BTN($i_Discipline_Select, "button","newWindow('../choose_pic.php?fieldname=PIC[]',9)");?><br />
						<?=$linterface->GET_BTN($i_Discipline_Remove, "button","checkOptionRemove(document.getElementById('PIC[]'))");?>
						<? } ?>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		<? if ($ldiscipline->use_subject) {?>
        <tr>
            <td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Subject_name?></span></td>
            <td><?=$select_subject?></td>
        </tr>
        <? } ?>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_UserRemark?></td>
			<td><?=$linterface->GET_TEXTAREA("remark", $remark);?></td>
		</tr>
		<tr>
			<td colspan="2" valign="middle" nowrap="nowrap" class="form_sep_title"><i> - <?=$eDiscipline["DisciplineDetails"]?> -</i></td>
		</tr>
		
		<tr valign="top">
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eDiscipline["Type"]?></span></td>
			<td>
				<? if($AccessItem) { ?>
				<input name="record_type" type="radio" id="record_type_award" value="1" <?=($record_type==1)?"checked":""?> onClick="changeMeritType();"><label for="record_type_award"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ediscipline/icon_merit.gif" width="20" height="20" border="0" align="absmiddle"> <?=$i_Merit_Award?></label>
				<input name="record_type" type="radio" id="record_type_punishment" value="-1" <?=$record_type==-1?"checked":""?> onClick="changeMeritType();"><label for="record_type_punishment"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ediscipline/icon_demerit.gif" width="20" height="20" border="0" align="absmiddle"> <?=$i_Merit_Punishment?></label>
				<input name="record_type" type="radio" id="record_type_other" value="0" <?=$record_type==0?"checked":""?> onClick="changeMeritType();"><label for="record_type_other"> <?=$i_Merit_NoAwardPunishment?></label>
				<? } else { 
					switch($record_type) 
					{
						case 1:		echo $i_Merit_Award; 				break;
						case -1:	echo $i_Merit_Punishment; 			break;
						case 0:		echo $i_Merit_NoAwardPunishment; 	break;
					}
				} ?>
			</td>
		</tr>
		<tr valign="top" id="tr_RecordItem" style="visibility:visible">
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_general_record?><? if($AccessItem) {?><span class="tabletextrequire">*</span><? } ?></td>
			<td>
			<? if($AccessItem) { ?>
			<select name="CatID" id="CatID" onChange="changeCat(this.value);"></select> 
				- 
				<SELECT onChange="changeItem(this.value)" name="ItemID" id="ItemID">
					<OPTION value="0" selected>-- <?=$button_select?> --</OPTION>
				</SELECT>
			<? } else {
				# Category
				$CatInfo= $ldiscipline->returnMeritItemCategoryByID($CatID);
				$CatDisplay = $CatInfo['CategoryName'];
				
				# Category Item
				$ItemDisplay = $resultAry['ItemText'];
				
				echo $CatDisplay . " - " . $ItemDisplay . "<input type='hidden' name='ItemID' value='". $ItemID ."'>";
			} ?>
			</td>
		</tr>
		
		<tr valign="top" id="tr_MeritDemerit" style="visibility:visible">
			<td valign="top" nowrap="nowrap" class="formfieldtitle"> <?=$i_Discipline_System_Add_Merit_Demerit?><? if($AccessItem) {?><span class="tabletextrequire">*</span><? } ?></td>
			<td>
			<? if($AccessItem) { ?>
			<?=$select_merit_num?> <select name="MeritType" id="MeritType"></select>
			<? } else {
				$thisMeritType = $ldiscipline->RETURN_MERIT_NAME($resultAry['ProfileMeritType']);
				echo $ldiscipline->returnDeMeritStringWithNumber($resultAry['ProfileMeritCount'], $thisMeritType);
			} ?>
			</td>
		</tr>
		
		<tr valign="top" id="tr_Conduct_Mark" style="<?=($ldiscipline->Hidden_ConductMark?"display:none":"visibility:visible")?>"> 
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eDiscipline['Conduct_Mark']?> <? if($AccessItem) {?><span class="tabletextrequire">*</span><? } ?></td>
			<td>
			<? if($AccessItem) { ?>
				<span id="increment_de"><?=$Lang['eDiscipline']['Gain']?></span>  <?=$select_conduct_mark?>
			<? } else {
				echo ($record_type==1 ? $Lang['eDiscipline']['Gain'] : $Lang['eDiscipline']['Deduct']) . " " . $resultAry['ConductScoreChange'];
			} ?>
			</td>
		</tr>
		
		<tr valign="top" id="tr_SubScore" style="<?=($ldiscipline->UseSubScore?"visibility:visible":"display:none")?>"> 
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Subscore1?></td>
			<td>
			<? if($AccessItem) { ?>
				<span id="increment_de2"><?=$Lang['eDiscipline']['Gain']?></span> <?=$select_study_score?>
			<? } else {
				echo ($record_type==1 ? $Lang['eDiscipline']['Gain'] : $Lang['eDiscipline']['Deduct']) . " " . $resultAry['SubScore1Change'];
			} ?>
			</td>
		</tr>
		
		<tr>
			<td valign="top" nowrap="nowrap"colspan="2">
				<? if (!$lnotice->disabled && $record_type!=0 && !empty($NoticeTemplateAva)) {?>
				<input name="action_notice" type="checkbox" id="action_notice" value="1" onClick="javascript:if(this.checked){document.getElementById('noticeFlag').value='YES';showDiv('divNotice');}else{document.getElementById('noticeFlag').value='NO';hideDiv('divNotice');}" <?=$eNoticeTemplateChecked?>/>
				
				<label for="action_notice"><?=$eDiscipline["SendNoticeWhenReleased"]?></label>
				<div id="divNotice" style="display:<?=$eNoticeTemplateVisibility?>">
					<fieldset class="form_sub_option">
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr>
								<td colspan="2" valign="top" nowrap="nowrap">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td class="sectiontitle"><img src="<?="$image_path/$LAYOUT_SKIN"?>/icon_section.gif" width="20" height="20" align="absmiddle"><?=$eDiscipline['eNoticeTemplate'] ?></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr valign="top">
								<td width="20%" nowrap="nowrap" class="tabletext"><?=$i_Discipline_Template?><span class="tabletextrequire">* </span></td>
								<td>
									<?=$catSelection0?> <select name="SelectNotice0" id="SelectNotice0"><?=$optionSubCategory?></select>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_Additional_Info?></td>
								<td valign="top"><?=$linterface->GET_TEXTAREA('TextAdditionalInfo0', $additionInfo);?></td>
							</tr>
						</table>
					</fieldset>
				</div>			
				<?}?>
			</td>
		</tr>
	</table>
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_save, "submit")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_reset, "button", "javascript:goreset()")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='view_student_involved.php?CaseID=$CaseID'")?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>

<br />

	<input type="hidden" name="CaseID" id="CaseID" value="<?=$CaseID?>" />
	<input type="hidden" name="student[]" id="student[]" value="<?=$StudentID?>" />	
	<input type="hidden" name="semester_flag" id="semester_flag" value="1" />
	<input type="hidden" name="RecordID" id="RecordID" value="<?=$RecordID?>" />
	<input type="hidden" name="noticeFlag" id="noticeFlag" />
	<input type="hidden" name="pic" id="<?=$pic?>" />
	
	<input type="hidden" name="temp_flag" id="temp_flag" value="0" />
</form>


<script language="javascript">
<!--
	<? if($AccessItem) {?>
	changeMeritType("<?=$ProfileMeritType?>");
	changeCat(<?=$CatID?>);
	changeItem(<?=$ItemID?>);
	<? } ?>
//-->
</script>

<?
print $linterface->FOCUS_ON_LOAD("form1.RecordDate");

$linterface->LAYOUT_STOP();
intranet_closedb();

?>