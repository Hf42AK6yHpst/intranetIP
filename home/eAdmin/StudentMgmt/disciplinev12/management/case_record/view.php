<?php
# using: 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

include_once($PATH_WRT_ROOT."includes/libfiletable.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Case_Record-View");
$CaseArr = $ldiscipline->getCaseRecordByCaseID($CaseID);

list($CaseNumber,$CaseTitle,$Year,$Semester,$Location,$Category,$EventDate,$PICID,$Attachment,$RecordStatus,$DateModified,$ModifiedBy,$RecordStatusNum, $ReleaseStatus, $ReleasedDate, $ReleasedBy, $FinishedDate, $FinishedBy, $Remark, $AcademicYearID, $YearTermID, $PIC) = $CaseArr[0];
$Remark = nl2br($CaseArr[0]['Remark']);

$Year = $ldiscipline->getAcademicYearNameByYearID($AcademicYearID);
$fcm = new academic_year_term($YearTermID);
$Semester = $fcm->Get_YearNameByID($YearTermID, $intranet_session_language);


if(is_array($PIC))
{
	foreach($PIC as $Key=>$Value)
	{
		$PICUser .=	$Value."<br/>";
	}
}

$Attachment = $ldiscipline->displayAttachment($Attachment);
			
				
function genCaseRecordViewPageTable($Page)
{
	global $CaseNumber,$CaseTitle,$Year,$Semester,$Location,$Category,$EventDate,$PICUser,$Attachment,$RecordStatus,$DateModified,$ModifiedBy, $ReleaseStatus, $ReleasedDate, $ReleasedBy, $FinishedDate, $FinishedBy;
	global $image_path,$LAYOUT_SKIN,$CaseID,$linterface, $ldiscipline, $Lang;
	global $button_edit, $button_delete,$RecordStatusNum, $i_Discipline_System_Discipline_Case_Record_Processing,$button_finish, $button_unrelease, $button_release;
	global $eDiscipline, $i_Discipline_By, $i_Discipline_System_Discipline_Case_Record_Case_Number, $i_Discipline_System_Discipline_Case_Record_Case_Title, $i_Discipline_System_Discipline_Case_Record_Case_School_Year, $i_Discipline_System_Discipline_Case_Record_Case_Semester, $i_Discipline_System_Award_Punishment_Change_To_Processed, $i_Discipline_System_Award_Punishment_Change_To_Processing, $i_Discipline_System_Award_Punishment_ProcessedBy, $i_Discipline_System_Award_Punishment_Processed;
	global $i_Discipline_System_Discipline_Case_Record_Case_Event_Date, $i_Discipline_System_Discipline_Case_Record_Case_Location, $i_Discipline_System_Discipline_Case_Record_Case_Details, $i_Discipline_System_Discipline_Case_Record_Case_PIC, $eDiscipline, $i_Discipline_System_Case_Record_Case_Info,$i_Discipline_System_Case_Record_Student_Involved, $i_Discipline_Last_Updated;
	global $intranet_session_language,$i_Discipline_System_Award_Punishment_Change_To_Unrelease, $i_Discipline_Last_Updated_Str;
	global $i_UserRemark,$Remark;
	
	if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditAll") || (($ldiscipline->isCasePIC($CaseID) || $ldiscipline->isCaseOwn($CaseID)) && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditOwn")))
		$edit_btn = $linterface->GET_ACTION_BTN($button_edit,'button',"window.location='edit.php?CaseID=".$CaseID."'");
	if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-DeleteAll") || (($ldiscipline->isCasePIC($CaseID) || $ldiscipline->isCaseOwn($CaseID)) && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-DeleteOwn")))
		$del_btn = $linterface->GET_ACTION_BTN($button_delete,'button','deleteRecord()');
	if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-Finish"))
		$statusButton = ($RecordStatusNum==DISCIPLINE_CASE_RECORD_FINISHED)?$linterface->GET_ACTION_BTN($i_Discipline_System_Award_Punishment_Change_To_Processing,'button','window.location=\'status_update.php?status=0&CaseID='.$CaseID.'\''):$linterface->GET_ACTION_BTN($i_Discipline_System_Award_Punishment_Change_To_Processed,'button','confirm_finish();');	
 	//if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-Release") && $RecordStatusNum==DISCIPLINE_CASE_RECORD_FINISHED)
 	//	$release_btn = ($ReleaseStatus==DISCIPLINE_STATUS_RELEASED)?$linterface->GET_ACTION_BTN($i_Discipline_System_Award_Punishment_Change_To_Unrelease,'button','confirm_unrelease();'):$linterface->GET_ACTION_BTN($button_release,'button','confirm_release();');	
	
	if($Page=='info' || $Page=='')
	{
		$lastModifiedString = $i_Discipline_Last_Updated_Str;
		$lastModifiedString = str_replace("%DATE%",$DateModified,$lastModifiedString);
		$lastModifiedString = str_replace("%NAME%",$ModifiedBy,$lastModifiedString);
		
		/*
		if ($intranet_session_language == "en")
		{
			$lastModifiedString = $i_Discipline_Last_Updated.' : '.$DateModified.' by '.$ModifiedBy;
		}
		else
		{
			$lastModifiedString = "最近由 ". $ModifiedBy ." 於 ". $DateModified. " 更新";
		}
		*/
		
		$x = '<div class="shadetabs">
				<ul>
				<li class="selected"><a href="view.php?CaseID='.$CaseID.'&Page=info">'.$i_Discipline_System_Case_Record_Case_Info.'</a></li>
				<!--<li><a href="view.php?CaseID='.$CaseID.'&Page=notes">Notes</a></li>-->
				<li><a href="view_student_involved.php?CaseID='.$CaseID.'&Page=involved">'.$i_Discipline_System_Case_Record_Student_Involved.'</a></li>
				</ul>
             </div>
             
             <table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="7" height="7"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/iPortfolio/scheme/scheme_board_01_off.gif" width="7" height="7"></td>
				<td height="7" background="'.$image_path.'/'.$LAYOUT_SKIN.'/iPortfolio/scheme/scheme_board_02_off.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/iPortfolio/scheme/scheme_board_02_off.gif" width="7" height="7"></td>
				<td width="7" height="7"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/iPortfolio/scheme/scheme_board_03_off.gif" width="9" height="7"></td>
			</tr>
			<tr>
				<td width="7" background="'.$image_path.'/'.$LAYOUT_SKIN.'/iPortfolio/scheme/scheme_board_04_off.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/iPortfolio/scheme/scheme_board_04_off.gif" width="7" height="7"></td>
				<td align="right" background="'.$image_path.'/'.$LAYOUT_SKIN.'/iPortfolio/scheme/scheme_board_05_off.gif">'.$lastModifiedString.'
					<table width="100%" border="0" cellspacing="5" cellpadding="5">
					<tr>
						<td width="20%" valign="top">'.$i_Discipline_System_Discipline_Case_Record_Case_Number.'</td>
						<td valign="top" bgcolor="#FFFFFF" >'.$CaseNumber.'</td>
					</tr>
					<tr>
						<td valign="top">'.$i_Discipline_System_Discipline_Case_Record_Case_Title.'</td>
						<td valign="top" bgcolor="#FFFFFF">'.$CaseTitle.'</td>
					</tr>
					<tr>
						<td valign="top">'.$i_Discipline_System_Discipline_Case_Record_Case_School_Year.'</td>
						<td valign="top" bgcolor="#FFFFFF" >'.$Year.'</td>
					</tr>
					<tr>
						<td valign="top">'.$i_Discipline_System_Discipline_Case_Record_Case_Semester.'</td>
						<td valign="top" bgcolor="#FFFFFF" >'.$Semester.'</td>
					</tr>
					<tr>
						<td valign="top">'.$Lang['eDiscipline']['EventDate'].'</td>
						<td valign="top" bgcolor="#FFFFFF" >'.$EventDate.'</td>
					</tr>
					<!--<tr>
						<td valign="top">'.$i_Discipline_System_Discipline_Case_Record_Case_Details.'</td>
						<td valign="top" bgcolor="#FFFFFF" >'.$Category.'</td>
					</tr>-->
					<tr>
						<td valign="top">'.$i_Discipline_System_Discipline_Case_Record_Case_PIC.'</td>
						<td valign="top" bgcolor="#FFFFFF">'.$PICUser.'</td>
					</tr>
					'.$Attachment.'
					<tr>
						<td valign="top">'.$i_UserRemark.'</td>
						<td valign="top" bgcolor="#FFFFFF">'.$Remark.'</td>
					</tr>
					<tr>
						<td valign="top">'.$eDiscipline["Status"].'</td>
						<td valign="top" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
					';
					if($FinishedBy and $RecordStatusNum)
					{
						$lu = new libuser($FinishedBy);
						if ($intranet_session_language == "en") {
							$RecordStatus .= " ".$i_Discipline_System_Award_Punishment_ProcessedBy." " . $lu->UserName() . " " . $FinishedDate;
						} else {
							$RecordStatus .= " ".$i_Discipline_System_Award_Punishment_ProcessedBy." " . $lu->UserName() . " " . $eDiscipline["ApprovedBy2"] . $FinishedDate . " ".$i_Discipline_System_Award_Punishment_Processed;
						}
					}
									
					$x .='
								<tr>
									<td>'. $RecordStatus.'</td>
								</tr>
					';
					
					/*
					# display Release Status info
					if($ReleasedBy)
					{
						$lu = new libuser($ReleasedBy);
						
						switch($ReleaseStatus)
						{
							case DISCIPLINE_STATUS_RELEASED:
								$icon = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_to_student.gif' width='20' height='20' border='0' align='absmiddle'>";
								$release_status_str = $icon . " " . $eDiscipline["ReleasedBy"] . " " . $lu->UserName() . " " . $ReleasedDate;
								break;
						 	case DISCIPLINE_STATUS_UNRELEASED:
					 			$icon = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_to_student.gif' width='20' height='20' border='0' align='absmiddle'>";
					 			$release_status_str = $icon . " " . $eDiscipline["UnReleasedBy"] . " " . $lu->UserName() . " " . $ReleasedDate;
						 		break;
						}
						
						if($release_status_str)
						{
							$x .= "
								<tr>
									<td>". $release_status_str ."</td>
								</tr>
							";
						}
					}
					*/
					//$ReleaseStatus, $ReleasedDate, $ReleasedBy;
					/*
								<tr>
									<td><table width="100%" border="0" cellspacing="0" cellpadding="3">
											<tr>
												<td><strong><a href="#"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/ediscipline/icon_lock.gif" width="20" height="20" border="0" align="absmiddle"></a></strong><a href="#" class="tablelink">Locked</a> by Miss Chan 2008-10-08</td>
											</tr>
									</table></td>
								</tr>
					*/
					$x .= '
							</table></td>
					</tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="3">

				<tr>
				<td align="center">
				'.$statusButton.'
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				'.$edit_btn.'
				'.$del_btn.'</td>
					</tr>
				</table>
							<br></td>
						<td width="7" background="'.$image_path.'/'.$LAYOUT_SKIN.'/iPortfolio/scheme/scheme_board_06_off.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/iPortfolio/scheme/scheme_board_06_off.gif" width="9" height="7"></td>
					</tr>
					<tr>
						<td width="7" height="7"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/iPortfolio/scheme/scheme_board_07_off.gif" width="7" height="7"></td>
						<td height="7" background="'.$image_path.'/'.$LAYOUT_SKIN.'/iPortfolio/scheme/scheme_board_08_off.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/iPortfolio/scheme/scheme_board_08_off.gif" width="9" height="7"></td>
						<td width="7" height="7"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/iPortfolio/scheme/scheme_board_09_off.gif" width="9" height="7"></td>
					</tr>
				</table>';
	}
	return $x;		
}

# menu highlight setting
$CurrentPage = "Management_CaseRecord";
$CurrentPageArr['eDisciplinev12'] = 1;

$TAGS_OBJ[] = array($i_Discipline_System_Access_Right_Case_Record);

$PAGE_NAVIGATION[] = array($i_Discipline_System_Discipline_Case_Record_Case_List, "index.php");
$PAGE_NAVIGATION[] = array($CaseTitle);

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>
<script type="text/javascript">

function deleteRecord()
{
	if(confirm('<?=$i_Discipline_System_Case_Record_Delete_Msg?>'))
		window.location='remove_update.php?CaseID=<?=$CaseID?>';	
}

function confirm_finish()
{
	if(confirm("<?=$eDiscipline["FinishCaseConfirm"]?>"))
		window.location='status_update.php?status=1&CaseID=<?=$CaseID?>';
}

function confirm_release()
{
	if(confirm("<?=$eDiscipline["ReleaseCaseConfirm"]?>"))
		window.location='release_update.php?CaseID=<?=$CaseID?>';
}

function confirm_unrelease()
{
	if(confirm("<?=$eDiscipline["UnReleaseCaseConfirm"]?>"))
		window.location='unrelease_update.php?CaseID=<?=$CaseID?>';
}

</script>
<form name="form1" method="post" action="new_update.php" >
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
				<tr>
					<td align="right"><?= $linterface->GET_SYS_MSG($xmsg);?></td>
				</tr>
				
			</table>
			<? echo genCaseRecordViewPageTable($Page)?>
		</td>
	</tr>
</table>
</form>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>


