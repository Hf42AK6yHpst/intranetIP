<?php
# using: henry


/********************** Change Log ***********************/
#
#	Date:	2010-04-14 YatWoon
#			check setting $ldiscipline->OnlyAdminSelectPIC, only admin can select PIC
#
/******************* End Of Change Log *******************/

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right
//$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Case_Record-New");
if(!(!$ldiscipline->CASE_IS_FINISHED($CaseID) && ($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditOwn") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditAll")) && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-New")))
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}


if(!$CaseID)
{
	header("Location: index.php");
	exit;
}

$linterface = new interface_html();
$lteaching = new libteaching();
$lgrouping = new libgrouping();

# menu highlight setting
$CurrentPage = "Management_CaseRecord";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($i_Discipline_System_Access_Right_Case_Record);

# step information
$STEPS_OBJ[] = array($eDiscipline["SelectStudents"], 0);
$STEPS_OBJ[] = array($eDiscipline["AddRecordToStudents"], 1);
$STEPS_OBJ[] = array($eDiscipline["SelectActions"], 0);
$STEPS_OBJ[] = array($eDiscipline["FinishNotification"], 0);

# navigation bar
$PAGE_NAVIGATION[] = array($i_Discipline_System_Access_Right_Case_Record, "view.php?CaseID=$CaseID");
$PAGE_NAVIGATION[] = array($eDiscipline["AddStudents"]);

# get data from preious page
$student = $_POST['student'];

## Get The max. no of record day(s) ##
$NumOfMaxRecordDays = $ldiscipline->MaxRecordDayBeforeAllow;
if($NumOfMaxRecordDays > 0) {
	$EnableMaxRecordDaysChecking = 1;
	$DateMinLimit = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d")-$NumOfMaxRecordDays, date("Y")));
	$DateMaxLimit = date("Y-m-d");
} else {
	$EnableMaxRecordDaysChecking = 0;
}

# retrieve all the student record (from U- user, G-group)
$student2 = array();
$permitted[] = 2;

foreach($student as $k=>$d)
{
	$type = substr($d, 0, 1);
	$type_id = substr($d, 1);
	switch($type)
	{
		case "U":
			$student2[] = $type_id;
			break;
		case "G":
			$groupIDAry[] = $type_id;
			$temp = $lgrouping->returnGroupUsersInIdentity($groupIDAry, $permitted);		
			//$temp = $lgrouping->returnGroupUsers($type_id);
			foreach($temp as $k1=>$d1)
			{
				$student2[] = $d1['UserID'];	
			}
			break;
		case "C":
			$temp = $ldiscipline->getStudentListByClassID($type_id);
			for($i=0; $i<sizeof($temp); $i++) {
				$student2[] = $temp[$i][0];	
			}
			break;
		default:
			$student2[] = $d;
			break;
			
	}
}
if(is_array($student2))
{
	$student= array_keys(array_count_values($student2));
}

# check student is duplicated in the same case
$involved_stu = $ldiscipline->RETRIEVE_CASE_INVOLVED_STUDENT($CaseID);
$student_temp = array();

foreach($student as $s)
{
	$stu_msg = "duplicate_students";
	if(!in_array($s,$involved_stu))
		array_push($student_temp, $s);
}
$student = $student_temp;


$stu_msg = $stu_msg ? $stu_msg : "no_student_select";
if(empty($student))	header("Location: student_new1.php?CaseID=$CaseID&msg=$stu_msg");
$student = array_unique($student);

# build html part
$selected_student_table = "<table width='100%' border='0' cellspacing='0' cellpadding='5'>";

foreach($student as $k=>$StudentID)
{
	$lu = new libuser($StudentID);
	$student_name = $lu->UserNameLang();
	$class_name = $lu->ClassName;
	$class_no = $lu->ClassNumber;
	
	$selected_student_table .= "<tr><td width='50%' valign='top'><a href='javascript:viewCurrentRecord($StudentID)' class='tablelink'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_stu_ind.gif' width='20' height='20' border='0' align='absmiddle'>";
	$selected_student_table .= $class_name. ($class_no?"-":"") .$class_no." ".$student_name;
	$selected_student_table .= "</a></td>";
	if(sizeof($sid)>0)	
		$selected_student_table .= (in_array($StudentID, $sid)) ? "<td>".$linterface->GET_SYS_MSG("",$Lang['eDiscipline']['ItemAlreadyAdded'])."</td>" : "";
	$selected_student_table .= "</tr>";
}
$selected_student_table .= "</table>";

/*
$semester_mode = getSemesterMode();
if($semester_mode==1)
{
	$select_sem = getSelectSemester("name=semester", $semester);	
	$select_sem_html = "
		<tr>
			<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\">{$i_SettingsSemester}</td>
			<td valign=\"top\">{$select_sem}</td>
		</tr>
	";
}
else
{
	$select_sem_html = "<input type='hidden' name='semester' value=''>";
}
*/
$select_sem_html = "<input type='hidden' name='semester' id='semester' value=''>";

# Last PIC selection
if (is_array($PIC) && sizeof($PIC)>0)
{
	$list = implode(",", $PIC);
}
else	# retrieve case PIC
{
	$list_temp = $ldiscipline->RETRIEVE_CASE_PIC($CaseID);
	$list = $list_temp[0];
}
$namefield = getNameFieldWithLoginByLang();
$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE RecordType = 1 AND UserID IN ($list) AND RecordStatus = 1 ORDER BY $namefield";
$array_PIC = $ldiscipline->returnArray($sql);
$PIC_selected = $linterface->GET_SELECTION_BOX($array_PIC, "name='PIC[]' ID='PIC[]' class='select_studentlist' size='6' multiple='multiple'", "");

# Subject Selection
if ($ldiscipline->use_subject)
{
	$subjectList = $lteaching->returnSubjectList();

    $select_subject = "<SELECT name='SubjectID' id='SubjectID'>\n";
    $select_subject .= "<OPTION value=''>-- $i_notapplicable --</OPTION>\n";
    foreach ($subjectList AS $k=>$d)
    {
        $select_subject .= "<OPTION value='". $d['SubjectID'] ."'>". $d['SubjectName'] ."</OPTION>\n";
    }
    $select_subject .= "</SELECT>\n";
}
    
# Get Category selection
$merit_cats = $ldiscipline->returnMeritItemCategoryByType(1);
$demerit_cats = $ldiscipline->returnMeritItemCategoryByType(-1);

$items = $ldiscipline->retrieveMeritItemswithCodeGroupByCat();

# Merit Count selection
$select_merit_num = "<SELECT name=MeritNum id=MeritNum>\n";
$AP_Interval = $ldiscipline->AP_Interval_Value ? $ldiscipline->AP_Interval_Value : 1;
for ($i=0; $i<=$ldiscipline->AwardPunish_MAX; $i=$i+$AP_Interval)
{
     $select_merit_num .= "<OPTION value=$i>$i</OPTION>\n";
}
$select_merit_num .= "</SELECT>\n";

# Conduct Mark selection
$select_conduct_mark = "<SELECT name='ConductScore' id='ConductScore'>\n";
for ($i=0; $i<=$ldiscipline->ConductMarkIncrement_MAX; $i++)
{
     $select_conduct_mark .= "<OPTION value=$i>$i</OPTION>\n";
}
$select_conduct_mark .= "</SELECT>\n";

# Subscore selection
$select_study_score = "<SELECT name='StudyScore' id='StudyScore'>\n";
for ($i=0; $i<=$ldiscipline->SubScoreIncrement_MAX; $i++)
{
     $select_study_score .= "<OPTION value=$i>$i</OPTION>\n";
}
$select_study_score .= "</SELECT>\n";

$merit_record_type = $ldiscipline->returnValidMeritTypeFieldWithOrder(1);
$demerit_record_type = $ldiscipline->returnValidMeritTypeFieldWithOrder(-1);

# Start layout
$linterface->LAYOUT_START();

/*
# generate semester checking js
if($semester_mode==2)
{
	$sem_data = getSemesters();
	$sem_js.="var sem_js = new Array();\n";
	for($i=0;$i<sizeof($sem_data);$i++)
	{
		list($sem_name, $sem_type, $sem_start, $sem_end) = split("::", $sem_data[$i]);
		$sem_js.="sem_js.push(new Array('$sem_start','$sem_end'));\n";
	}
}
*/

?>


<SCRIPT LANGUAGE=Javascript>
<!--

<?//=$sem_js?>

// Generate Merit Category
var merit_cat_select = new Array();
merit_cat_select[0] = new Option("-- <?=$eDiscipline["SelectRecordCategory"]?> --",0);
<?	foreach($merit_cats as $k=>$d)	{ ?>
		merit_cat_select[<?=$k+1?>] = new Option("<?=str_replace('"', '\"', $d['CategoryName'])?>",<?=$d['CatID']?>);
<?	} ?>

// Generate Demerit Category
var demerit_cat_select = new Array();
demerit_cat_select[0] = new Option("-- <?=$eDiscipline["SelectRecordCategory"]?> --",0);
<?	foreach($demerit_cats as $k=>$d)	{ ?>
		demerit_cat_select[<?=$k+1?>] = new Option("<?=str_replace('"', '\"', $d['CategoryName'])?>",<?=$d['CatID']?>);
<?	} ?>

// Generate Merit Type
var merit_record_type = new Array();
<?	foreach($merit_record_type as $k=>$d)	{ ?>
		merit_record_type[<?=$k?>] = new Option("<?=$d[1]?>",<?=$d[0]?>);
<?	} ?>

// Generate Demerit Type
var demerit_record_type = new Array();
<?	foreach($demerit_record_type as $k=>$d)	{ ?>
		demerit_record_type[<?=$k?>] = new Option("<?=$d[1]?>",<?=$d[0]?>);
<?	} ?>

function viewCurrentRecord(id)
{
         newWindow('../award_punishment/viewcurrent.php?StudentID='+id, 8);
}

function changeMeritType() 
{
	var obj = document.form1;
	var cat_select = document.getElementById('CatID');
	var item_select = document.getElementById('ItemID');
	var meritType = document.getElementById('MeritType');
	var increment_de = document.getElementById("increment_de");
	var increment_de2 = document.getElementById("increment_de2");
	var tr_RecordItem = document.getElementById("tr_RecordItem");
	var tr_MeritDemerit = document.getElementById("tr_MeritDemerit");
	var tr_Conduct_Mark = document.getElementById("tr_Conduct_Mark");
	var tr_SubScore = document.getElementById("tr_SubScore");
	
	while (item_select.options.length > 0)
	{
	    item_select.options[0] = null;
	}
	
	item_select.options[0] = new Option("-- <?=$eDiscipline["SelectRecordItem"]?> --",0);
	
	
	if(obj.record_type[0].checked)		// Award
	{
		cat_select.options.length = merit_cat_select.length;
		for(i=0;i<merit_cat_select.length;i++)
		{
			cat_select.options[i]=new Option(merit_cat_select[i].text,merit_cat_select[i].value);	
			<? if($CatID) {?>
			if(merit_cat_select[i].value == <?=$CatID?>) 	cat_select.selectedIndex = i;
			<? } ?>
		}
			
		meritType.options.length = merit_record_type.length;
		meritType.options[0]=new Option("--", "-999");	
		for(i=1;i<merit_record_type.length;i++)
			meritType.options[i]=new Option(merit_record_type[i].text,merit_record_type[i].value);	
			
		document.getElementById('increment_de').innerHTML = "<?=$Lang['eDiscipline']['Gain']?>";
		document.getElementById('increment_de2').innerHTML = "<?=$Lang['eDiscipline']['Gain']?>";
		document.getElementById('MeritNum').selectedIndex = <?=$MeritNum ? $MeritNum : 0?>;
		document.getElementById('ConductScore').selectedIndex = <?=$ConductScore ? $ConductScore : 0?>;
	}
	else								// Punishment
	{
		cat_select.options.length = demerit_cat_select.length;
		
		for(i=0;i<demerit_cat_select.length;i++)
		{
			cat_select.options[i]=new Option(demerit_cat_select[i].text,demerit_cat_select[i].value);
			<? if($CatID) {?>
			if(demerit_cat_select[i].value == <?=$CatID?>) 	cat_select.selectedIndex = i;
			<? } ?>
		}
			
		meritType.options.length = demerit_record_type.length;
		meritType.options[0]=new Option("--", "-999");	
		for(i=1;i<demerit_record_type.length;i++)
			meritType.options[i]=new Option(demerit_record_type[i].text,demerit_record_type[i].value);	
			
		document.getElementById('increment_de').innerHTML = "<?=$Lang['eDiscipline']['Deduct']?>";
		document.getElementById('increment_de2').innerHTML = "<?=$Lang['eDiscipline']['Deduct']?>";
		document.getElementById('MeritNum').selectedIndex = <?=$MeritNum ? $MeritNum : 0?>;
		document.getElementById('ConductScore').selectedIndex = <?=$ConductScore ? $ConductScore : 0?>;
	}
	
	if(obj.record_type[2].checked)
	{
		
		tr_RecordItem.style.display="none";
		tr_MeritDemerit.style.display="none";
		tr_Conduct_Mark.style.display="none";
		tr_SubScore.style.display="none";
		
	}
	else
	{
		if(navigator.appName == 'Microsoft Internet Explorer')
			display_var = "block";
		else
			display_var = "table-row";
		
		tr_RecordItem.style.display=display_var;
		tr_MeritDemerit.style.display=display_var;
		<? if(!$ldiscipline->Hidden_ConductMark) {?>
		tr_Conduct_Mark.style.display=display_var;
		<? } ?>
		<? if($ldiscipline->UseSubScore) {?>
		tr_SubScore.style.display=display_var;
		<? } ?>
	}
}

function changeCat(value)
{
	var item_select = document.getElementById('ItemID');
	//var selectedCatID = document.getElementById('CatID').value;
	var selectedCatID = value;
	
	while (item_select.options.length > 0)
	{
	    item_select.options[0] = null;
	}
	
	item_select.options[0] = new Option("-- <?=$eDiscipline["SelectRecordItem"]?> --",0);
	
	if (selectedCatID == '')
	{
	<?
		$curr_cat_id = "";
		$pos = 1;
		for ($i=0; $i<sizeof($items); $i++)
		{
	     list($r_catID, $r_itemID, $r_itemName) = $items[$i];
	     $r_itemName = intranet_undo_htmlspecialchars($r_itemName);
	     $r_itemName = str_replace('"', '\"', $r_itemName);
	     $r_itemName = str_replace("'", "\'", $r_itemName);
	     if ($r_catID != $curr_cat_id)
	     {
	         $pos = 1;
	?>
     }
	else if (selectedCatID == "<?=$r_catID?>")
	{
	     <?
	         $curr_cat_id = $r_catID;
	     }
	     ?>
		item_select.options[<?=$pos++?>] = new Option("<?=$r_itemName?>",<?=$r_itemID?>);
		<?	if($r_itemID == $ItemID) {	?>
			item_select.selectedIndex = <?=$pos-1?>;
		<? } ?>
		<?
	}
	?>
     }
}

function changeItem(value)
{
	var i;
	var meritType = document.getElementById('MeritType');
	var meritNum = document.getElementById('MeritNum');
	var conductScore = document.getElementById('ConductScore');
	<? if ($ldiscipline->UseSubScore) { ?>
		var subScore1 = document.getElementById('StudyScore');
	<? } ?>
	<?
	for ($i=0; $i<sizeof($items); $i++)
	{
     list($r_catID, $r_itemID, $r_itemName, $r_meritType, $r_meritNum, $r_conduct, $r_punish, $r_detention, $r_subscore1) = $items[$i];
     ?>
     if (value=="<?=$r_itemID?>")
     {
	     if(document.form1.temp_flag.value==1)
		{
			check_meritType = <?=$r_meritType?>;
			check_meritNum = <?=$r_meritNum?>;
			check_conductScore = <?=$r_conduct?>;
			//check_studyScore = <?=$r_subscore1?>;
		}
		else
		{
			check_meritType = <?=($MeritType?$MeritType:0) ?>;
			check_meritNum = <?=($MeritNum?$MeritNum:0)?>;
			check_conductScore = <?=($ConductScore?$ConductScore:0)?>;
			//check_studyScore = <?=($StudyScore?$StudyScore:0)?>;
		}
		
         // Merit Type
         for (i=0; i<meritType.options.length; i++)
         {
              if (meritType.options[i].value==check_meritType)
              {
                  meritType.selectedIndex = i;
                  break;
              }
         }
         
         // Merit Num
         for (i=0; i<meritNum.options.length; i++)
         {
              if (meritNum.options[i].value==check_meritNum)
              {
                  meritNum.selectedIndex = i;
                  break;
              }
         }

         // Conduct Score
         for (i=0; i<conductScore.options.length; i++)
         {
              if (conductScore.options[i].value==check_conductScore)
              {
                  conductScore.selectedIndex = i;
                  break;
              }
         }
          // Sub Score
         <? if ($ldiscipline->UseSubScore) { ?>
          for (i=0; i<subScore1.options.length; i++)
          {
               if (subScore1.options[i].value=="<?=$r_subscore1?>")
               {
                   subScore1.selectedIndex = i;
                   break;
               }
          }
         <? } ?>

     }
        <?
}
?>

document.form1.temp_flag.value=1;

}

function CompareDates(date_string1, date_string2, EqualDateAllow)
{
	var yr1  = parseInt(date_string1.substring(0,4),10);
	var mon1 = parseInt(date_string1.substring(5,7),10);
	var dt1  = parseInt(date_string1.substring(8,10),10);
	var yr2  = parseInt(date_string2.substring(0,4),10);
	var mon2 = parseInt(date_string2.substring(5,7),10);
	var dt2  = parseInt(date_string2.substring(8,10),10);
	var date1 = new Date(yr1, mon1-1, dt1);
	var date2 = new Date(yr2, mon2-1, dt2);
	
	if(EqualDateAllow == 0)
	{
		if(date2 < date1) {
			return true;
		}else{
			return false;
		}
	}else{
		if(date2 <= date1) {
			return true;
		}else{
			return false;
		}
	}
}

function check_form()
{
	obj = document.form1;
	
	// Record Date
	if(!check_date(obj.RecordDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))
	{
		return false;
	}else{
		<? if($EnableMaxRecordDaysChecking == 1) { ?>
			// Check Event Date is allow or not
			var DateMinLimit = "<?=$DateMinLimit;?>";
			var DateMaxLimit = "<?=$DateMaxLimit;?>";
			if(!CompareDates(obj.RecordDate.value, DateMinLimit, 1))
			{
				alert("<?=$eDiscipline['JSWarning']['RecordDateIsNotAllow'];?>");
				return false;
			}else{
				if(!CompareDates(DateMaxLimit, obj.RecordDate.value, 1))
				{
					alert("<?=$eDiscipline['JSWarning']['RecordDateIsNotAllow'];?>");
					return false;
				}
			}
		<? } ?>
	}
	
	// Semester
	<? if($semester_mode==2) {?>
	if(!check_semester(obj.RecordDate))
	{
		return false;
	}
	<? } ?>
	
	// Teahcer/Staff
	objPic = document.getElementById('PIC[]');
	checkOption(objPic);
	if (objPic.length==0) {
		checkOptionAdd(objPic, "<? for($i = 0; $i < 40; $i++) echo " "; ?>", "");
		alert("<?=$i_alert_pleaseselect.$i_Discipline_Duty_Teacher?>");
		return false;
	}
	checkOptionAll(objPic);
	
	/* check the merit type 0 - N.A. */
	if(document.getElementById('MeritNum').value==0)	document.getElementById('MeritType').selectedIndex = 0;
	if(document.getElementById('MeritType').value==-999)	document.getElementById('MeritNum').selectedIndex = 0;
	
	if(!obj.record_type[2].checked)
	{
		// Category / Items
	    if(document.getElementById('CatID').value == 0 || document.getElementById('ItemID').value == 0)
	    {
	            alert('<?=$i_alert_pleaseselect.$i_Discipline_System_general_record?>');
	            return false;
	    }
	    <?/* if(!$ldiscipline->Hidden_ConductMark) {?>
	    else if(document.getElementById('ConductScore').value == 0 && document.getElementById('MeritNum').value == 0)
	    {
	        if (window.confirm('<?=$eDiscipline['no_ConductScore_MeritNum_selected'] ?>'))
	        {
	            obj.action='student_new3.php';
	            return true;
	        }
	        return false;
	    }
	    <? } */?>
    // checking on MeritNum, ConductScore & Subscore (if any)
    <? if($ldiscipline->NotAllowEmptyInputInAP) { ?>
	    <? 
	    if(!$ldiscipline->Hidden_ConductMark) {
			if($ldiscipline->UseSubScore) {
				$jsScript = ' && obj.elements["StudyScore"].value == 0';	
				$alertMsg = $eDiscipline['MeritDemerit'].", ".$eDiscipline['Conduct_Mark']." ".$i_alert_or." ".$i_Discipline_System_Subscore1;
			} else {
				$alertMsg = $eDiscipline['MeritDemerit']." ".$i_alert_or." ".$eDiscipline['Conduct_Mark'];
			}
			$alertMsg = $i_alert_pleaseselect.$alertMsg;
		?>
	    else if(obj.elements["ConductScore"].value == 0 && obj.elements["MeritNum"].value == 0 <?=$jsScript?>)
	    {
		    alert("<?=$alertMsg ?>");
        	return false;
	    }
	    <? } else {
			if($ldiscipline->UseSubScore) {
				$jsScript = ' && obj.elements["StudyScore"].value == 0';	
				$alertMsg = $eDiscipline['MeritDemerit']." ".$i_alert_or." ".$i_Discipline_System_Subscore1;
			} else {
				$alertMsg = $eDiscipline['MeritDemerit'];
			}
			$alertMsg = $i_alert_pleaseselect.$alertMsg;
		    ?>
	    else if(obj.elements["MeritNum"].value == 0 <?=$jsScript?>)
	    {
		    alert("<?=$alertMsg ?>");
        	return false;
	    }
	    <? } ?>
    <? } else { ?>
    	// allow null input
	    <? 
	    if(!$ldiscipline->Hidden_ConductMark) {
			if($ldiscipline->UseSubScore) {
				$jsScript = ' && obj.elements["StudyScore"].value == 0';	
				$alertMsg = $eDiscipline['MeritDemerit'].", ".$eDiscipline['Conduct_Mark']." ".$i_alert_or." ".$i_Discipline_System_Subscore1;
			} else {
				$alertMsg = $eDiscipline['MeritDemerit']." ".$i_alert_or." ".$eDiscipline['Conduct_Mark'];
			}
			if($intranet_session_language=="en")
				$alertMsg = $Lang['eDiscipline']['EmptyInput']." ".$alertMsg." ".$Lang['eDiscipline']['IsSelected'].$button_confirm."?";
			else 
				$alertMsg = $Lang['eDiscipline']['IsSelected'].$alertMsg.$Lang['eDiscipline']['sdbnsm_fullStop'].$button_confirm."?";
		    ?>
	    else if(obj.elements["ConductScore"].value == 0 && obj.elements["MeritNum"].value == 0 <?=$jsScript?>)
	    {
		    if (window.confirm('<?=$alertMsg?>'))
	        {
	            obj.action='edit_update.php';
	            //return true;
	        }
	        else
	        {
	        	return false;
	    	}
	    }
	    <? } else { 
			if($ldiscipline->UseSubScore) {
				$jsScript = ' && obj.elements["StudyScore"].value == 0';	
				$alertMsg = $eDiscipline['MeritDemerit']." ".$i_alert_or." ".$i_Discipline_System_Subscore1;
			} else {
				$alertMsg = $eDiscipline['MeritDemerit'];
			}
			if($intranet_session_language=="en")
				$alertMsg = $Lang['eDiscipline']['EmptyInput']." ".$alertMsg." ".$Lang['eDiscipline']['IsSelected'].$button_confirm."?";
			else 
				$alertMsg = $Lang['eDiscipline']['IsSelected'].$alertMsg.$Lang['eDiscipline']['sdbnsm_fullStop'].$button_confirm."?";
		    ?>
	    else if(obj.elements["MeritNum"].value == 0 <?=$jsScript?>)
	    {
		    if (window.confirm('<?=$alertMsg?>'))
	        {
	            obj.action='new3.php';
	            //return true;
	        }
	        else
	        {
	        	return false;
	    	}
	    }
	    <? } ?>
    <? } ?>
    }
   
    obj.action='student_new3.php';
}

function check_semester(objDate)
{
	semester_periods = sem_js;
	
	valid = false;
	if(semester_periods!=null)
	{
		for(j=0;j<semester_periods.length;j++){
			dateStart = semester_periods[j][0];
			dateEnd = semester_periods[j][1];
			if(dateStart <= objDate.value && dateEnd >=objDate.value){
				valid = true;
				break;
			}
		}
	}
	
	if(!valid)
	{
		objDate.focus();
		objDate.className ='textboxnum textboxhighlight';
		alert('<?=$eDiscipline["NoSemesterSettings"]?>');
		return false;
	}
	
	return true;
}

//-->
</SCRIPT>

<form name="form1" method="POST" onsubmit="return check_form();">

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td class="navigation">
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
		</table>
	</td>
</tr>
<tr>
	<td align="center"><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
</tr>

<tr>
	<td>
		<table align="center" width="90%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td valign="top" colspan="2" align="right"><?=$linterface->GET_SYS_MSG($msg, $xmsg)?></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="tablerow2"><?=$i_general_students_selected?></td>
			<td valign="top" class="tablerow2"><?=$selected_student_table?></td>
		</tr>
		<tr>
			<td colspan="2" valign="middle" nowrap="nowrap" class="form_sep_title"><i> - <?=$eDiscipline["BasicInfo"]?> -</i></td>
		</tr>
		
		<?=$select_sem_html?>
		
		<tr>
                <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['EventDate']?><span class="tabletextrequire">* </span></td>
                <td> <?= $linterface->GET_DATE_PICKER("RecordDate",$RecordDate)?></td>
        </tr>
        
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Contact_PIC?><span class="tabletextrequire">* </span></td>
			<td valign="top">
				<table>
				<tr>
					<td><?=$PIC_selected?>
						<? /* ?><select name=PIC[] size=6 multiple><option><?=$space?></option></select><? */ ?>
					</td>
					<td valign="bottom">
					<? if(!$ldiscipline->OnlyAdminSelectPIC || ($ldiscipline->OnlyAdminSelectPIC && $ldiscipline->IS_ADMIN_USER()) ) { ?>
						<?=$linterface->GET_BTN($button_select, "button","newWindow('../choose_pic.php?fieldname=PIC[]',9)");?><br />
						<?=$linterface->GET_BTN($button_remove, "button","checkOptionRemove(document.getElementById('PIC[]'))");?>
						<? } ?>
					</td>
				</tr>
				</table> 
			</td>
		</tr>
		<? if ($ldiscipline->use_subject) {?>
        <tr>
            <td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Subject_name?></span></td>
            <td><?=$select_subject?></td>
        </tr>
        <? } ?>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_UserRemark?></td>
			<td><?=$linterface->GET_TEXTAREA("remark", $remark);?></td>
		</tr>
		<tr>
			<td colspan="2" valign="middle" nowrap="nowrap" class="form_sep_title"><i> - <?=$eDiscipline["DisciplineDetails"]?> -</i></td>
		</tr>
		<tr valign="top">
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eDiscipline["Type"]?></span></td>
			<td>
				<input name="record_type" type="radio" id="record_type_award" value="1" <?=($record_type==1)?"checked":""?> onClick="changeMeritType();"><label for="record_type_award"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ediscipline/icon_merit.gif" width="20" height="20" border="0" align="absmiddle"> <?=$i_Merit_Award?></label>
				<input name="record_type" type="radio" id="record_type_punishment" value="-1" <?=$record_type==-1?"checked":""?> onClick="changeMeritType();"><label for="record_type_punishment"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ediscipline/icon_demerit.gif" width="20" height="20" border="0" align="absmiddle"> <?=$i_Merit_Punishment?></label>
				<input name="record_type" type="radio" id="record_type_other" value="0" <?=$record_type==0?"checked":""?> onClick="changeMeritType();"><label for="record_type_other"> <?=$i_Merit_NoAwardPunishment?></label>
			</td>
		</tr>
		<tr valign="top" id="tr_RecordItem" style="display:inline">
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eDiscipline["RecordItem"]?><span class="tabletextrequire">*</span></span></td>
			<td><select name="CatID" id="CatID" onChange="changeCat(this.value);"></select> 
				- 
				<SELECT onChange="changeItem(this.value)" name="ItemID" id="ItemID">
					<OPTION value="0" selected>-- <?=$eDiscipline["SelectRecordItem"]?> --</OPTION>
				</SELECT>
				</td>
		</tr>
		
		<tr valign="top" id="tr_MeritDemerit" style="display:inline">
			<td valign="top" nowrap="nowrap" class="formfieldtitle"> <?=$eDiscipline['MeritDemerit']?></td>
			<td><?=$select_merit_num?> <select name="MeritType" id="MeritType"></select></td>
		</tr>
		
		<tr valign="top" id="tr_Conduct_Mark" style="<?=($ldiscipline->Hidden_ConductMark?"display:none":"visibility:visible")?>"> 
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eDiscipline['Conduct_Mark']?></td>
			<td><span id="increment_de"><?=$Lang['eDiscipline']['Gain']?></span> <?=$select_conduct_mark?></td>
		</tr>
		
		<tr valign="top" id="tr_SubScore" style="<?=($ldiscipline->UseSubScore?"visibility:visible":"display:none")?>"> 
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Subscore1?></td>
			<td><span id="increment_de2"><?=$Lang['eDiscipline']['Gain']?></span> <?=$select_study_score?></td>
		</tr>
		
		
		<? /* ?>
		<tr>
			<td colspan="2" valign="middle" nowrap="nowrap"><em class="form_sep_title"> - Status -</em></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle">Lock This Case</td>
			<td><label for="grading_passfail" class="tabletext"></label>
					<label for="grading_passfail" class="tabletext">
					<input name="checkbox" type="checkbox" id="checkbox">
					<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ediscipline/icon_lock.gif" width="20" height="20" border="0" align="absmiddle">Lock </label></td>
		</tr>
		<? */ ?>
	</table>
	
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_continue, "submit")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_back, "button", "this.form.action='student_new1.php';checkOptionAll(document.getElementById('PIC[]'));this.form.submit();")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='view_student_involved.php?CaseID=$CaseID'")?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>

<br />

<input type="hidden" name="CaseID" id="CaseID" value="<?=$CaseID?>" />
<input type="hidden" name="temp_flag" value="0" />
<!-- Step 1 data //-->
<? foreach($student as $k=>$d) { ?>
	<input type="hidden" name="student[]" id="student[]" value="<?=$d?>" />	
<? } ?>
<input type="hidden" name="semester_flag" id="semester_flag" value="1" />
</form>


<script language="javascript">
<!--
changeMeritType();
changeCat(<?=$CatID?>);
changeItem(<?=$ItemID?>);
//-->
</script>

<?
print $linterface->FOCUS_ON_LOAD("form1.RecordDate");

$linterface->LAYOUT_STOP();
intranet_closedb();

?>