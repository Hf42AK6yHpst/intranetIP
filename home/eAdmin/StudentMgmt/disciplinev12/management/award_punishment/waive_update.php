<?php
# using: 
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Award_Punishment-Waive");
$RecordIDAry = $RecordID;
$return_filter = "SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&num_per_page=$num_per_page&pageNo=$pageNo&order=$order&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&MeritType=$MeritType&s=$s&clickID=$clickID&pic=$pic&fromPPC=$fromPPC&passedActionDueDate=$passedActionDueDate";
if($back_page)
	$returnPath = $back_page."?id=$RecordIDAry[0]";
else
	$returnPath = "index.php?$return_filter";

foreach($RecordIDAry as $MeritRecordID)
{
	$thisReason = ${"reason_".$MeritRecordID};
	$thisReason = intranet_htmlspecialchars($thisReason);
	$ldiscipline->WAIVE_MERIT_RECORD($MeritRecordID, $thisReason); 
}

$msg = "waive";
header("Location: ".$returnPath."&".$return_filter."&msg=".$msg);

intranet_closedb();
?>