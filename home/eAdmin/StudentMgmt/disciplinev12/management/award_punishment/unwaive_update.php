<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Award_Punishment-Waive");

$RecordIDAry = array();

if(is_array($RecordID))
{
	$RecordIDAry = $RecordID;
	$returnPath = "index.php?msg=unwaive";
}
else
{
	$RecordIDAry[] = $RecordID;
	$returnPath = "detail.php?id=$RecordID&msg=unwaive";
}

foreach($RecordIDAry as $MeritRecordID)
{
	$ldiscipline->UNWAIVE_MERIT_RECORD($MeritRecordID);
}

header("Location: $returnPath");

intranet_closedb();
?>