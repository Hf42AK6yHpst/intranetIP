<?php
# modifying by : Bill

############ Change Log Start #############################
#
#   Date    :   2020-11-06  (Bill)  [2020-0824-1151-40308]
#               Support Follow-up Feedback    ($sys_custom['eDiscipline']['APFollowUpRemarks'])
#
#   Date    :   2019-07-16  Bill    [2019-0712-1439-39207]
#               fixed Year Selection cannot select "Whole Year"
#
#   Date    :   2019-04-30  (Bill)
#               prevent Cross-site Scripting
#
#   Date    :   2019-02-20 (Bill)   [DM#3541]
#               fixed: not using event date as default sorting if enabled study / activity scores
#
#	Date	:	2018-04-09 (Bill)	[2017-1206-0959-15236]
#				fixed: only show "WebSAMS System Scheme" option when Export in WebSAMS Format
#
#	Date	:	2018-03-02 (Bill)	[2017-0317-1025-03225]
# 				added "Add to Rainbow Scheme" to Change Status Layer  ($sys_custom['eDiscipline']['TKP_RainbowScheme'])
#
#	Date	:	2018-03-01 (Bill)	[2017-1206-0959-15236]
#				added another option "WebSAMS System Scheme" when Export in WebSAMS Format
#
#	Date	:	2017-06-28 (Bill)	[2015-0120-1200-33164]
#				Add Column Study Score and Activity Score 
#				Add filter Tag Type 	($sys_custom['Discipline_AP_Item_Tag_Seperate_Function'])
#				Support add Merit with target Tag Type
#
#	Date	:	2017-05-11 (Bill)	[2017-0510-1427-47054]
#				fix images display problem under Change Status drop down list
#
#	Date	:	2016-04-08 (Bill)	[2016-0224-1423-31073]
#				added remarks for prefix style of deleted teacher account
#
#	Date	:	2016-04-06 (Bill)	[2015-0807-1048-01073]
#				Cust: redirect to Personal Report when clicking student name ($sys_custom['eDiscipline']['MoPuiChingRedirToPersonalReport'])
#
#	Date	:	2015-11-04 (Bill)	[2015-0416-1040-06164]
#				only show self created records for Student Prefect - HKUGA Cust
#
# 	Date	:	2015-10-26 (Bill)	[2015-0611-1642-26164]
# 				added Probation to Change Status Layer
#
#	Date	:	2015-09-25	(Bill)	[2015-0416-1040-06164]
#				Hide Redeem Button for HKUGA Cust - $sys_custom['eDiscipline']['HideRedeem']
#
# 	Date	:	2015-07-28 (Bill)	[2015-0611-1642-26164]
# 				display "Rehabilitation Program" in column "Action"
#				added layer to display info of Rehabilitation Program
#
#	Date	:	2015-07-28 (Evan)
#				fix bug of search bar with extra slashes
#
#	Date	:	2015-07-17 (Evan)
#				Update style of search bar
#
#	Date	:	2015-04-30 (Bill)	[2014-1216-1347-28164]
#				display "Send push message" in column "Action" 
#
#	Date	:	2015-04-28 (Bill) [2015-0318-1624-34073]
#				add content tool to add notice for multiple AP records
#
#	Date	:	2014-06-27 (Carlos)
#				$sys_custom['eDiscipline']['PooiToMiddleSchool'] - never display [Re-calculate Accumulated Record]
#				2014-06-06 (Carlos)
#				$sys_custom['eDiscipline']['PooiToMiddleSchool'] - added record type button [Generate from GM]
#
#	Date	:	2013-06-21 (Carlos)
#				Display conduct score change with masked symbol for those overflow score merit items 	
#
#	Date	:	2013-03-01 (YatWoon)
#				Improved: display WebSAMS export as general (requested by Sales)
#				add date range checking for websSAMS export [Case#2013-0228-1113-49073]
#
#	Date	:	2013-02-07 (YatWoon)
#				Fixed: display "approve/reject" shortcut in "Change Status" if no approval group setting (need check with access rights setting also) [Case#2013-0131-1117-50156]
#
#	Date	:	2012-12-27 (YatWoon)
#				Improved: update doExport(), using "POST" instead of "GET" [Cae#2012-1227-1206-15132]
#
#	Date	:	2012-10-09 (YatWoon)
#				Missing to check the approval group right for approve / reject [Case#2012-1004-1026-21054]
#
#	Date	:	2011-03-24 (Henry Chow)
#				PIC selection menu only display the staff name of selected semester
#
#	Date	:	2010-12-29 Henry Chow
#				add "Date Range" option when export
# 
#	Date	:	2010-09-24 Henry Chow
#				Display "Form" in class filter
# 
#	Date	:	2010-05-10	YatWoon
#				Display approved by, released by, rejected by.... data in the index page
#				
#	Date	: 	2010-05-07 YatWoon
#				According to the setting $PIC_SelectionDefaultOwn to check PIC default selected value
#
#	Date:	2010-03-09	Henry
#			update $conds2, not include "ReleaseStatus" checking for some cases
#
#	Date:	2010-02-19	Henry
#			update javascript, allow to filter the selections by both pulldown and search box
#
#	Date:	2010-02-18	YatWoon
#			update query for search PIC, use "%,4,%" rather than "%4%" 
#
#	Date:	2010-02-12	YatWoon
#			combine the search with filter option
#
#	Date:	2010-02-01	YatWoon
#			don't display removed student's record (use INNER JOIN instead of LEFT OUTER JOIN)
#
#	Date:	2010-01-27	YatWoon
#			set the de/merit count as char for display to cater if the de/merit name with special char (UCCKE)
#
############ Change Log End #############################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libucc.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lclass = new libclass();

$arrCookies[] = array("eDis_Mgmt_AP_SchoolYear", "SchoolYear");
$arrCookies[] = array("eDis_Mgmt_AP_semester", "semester");
$arrCookies[] = array("eDis_Mgmt_AP_targetClass", "targetClass");
$arrCookies[] = array("eDis_Mgmt_AP_pic", "pic");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}

if($sys_custom['eDiscipline']['yy3'])
{
	include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");
	$ldiscipline_ui = new libdisciplinev12_ui_cust();
}

// [2014-1216-1347-28164]
$canSendPushMsg = $plugin['eClassApp'] && $sys_custom['eDiscipline']['SendDetentionPushMessage'];

$useApprovalGroup = $ldiscipline->useApprovalGroup();

if($ldiscipline->Display_ConductMarkInAP) {
	$sortEventDate = 6;			# Event Date is the 6th SQL column
}
else {
	$sortEventDate = 5;			# Event Date is the 5th SQL column
}

// [DM#3541]
if($ldiscipline->UseSubScore) {
    $sortEventDate++;
}
if($ldiscipline->UseActScore) {
    $sortEventDate++;
}

# Change "num per page"
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
# Preserve table view
if ($ck_approval_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_approval_page_number", $pageNo, 0, "", "", 0);
	$ck_approval_page_number = $pageNo;
}
else if (!isset($pageNo) && $ck_approval_page_number!="")
{
	$pageNo = $ck_approval_page_number;
}
if ($ck_approval_page_order!=$order && $order!="")
{
	setcookie("ck_approval_page_order", $order, 0, "", "", 0);
	$ck_approval_page_order = $order;
}
else if (!isset($order) && $ck_approval_page_order!="")
{
	$order = $ck_approval_page_order;
}
if ($ck_approval_page_field!=$field && $field!="")
{
	setcookie("ck_approval_page_field", $field, 0, "", "", 0);
	$ck_approval_page_field = $field;
}
else if (!isset($field) || $ck_approval_page_field == "")
{
	$field = $sortEventDate;
}

### Handle SQL Injection + XSS [START]
if($SchoolYear != '' && $SchoolYear != '0') {
    $APSchoolYearArr = $ldiscipline->getAPSchoolYear();
    $APSchoolYearArr = Get_Array_By_Key((array)$APSchoolYearArr, 'AcademicYearID');
    if(!in_array($SchoolYear, (array)$APSchoolYearArr)) {
        $SchoolYear = '0';
    }
}
if($semester != '' && $semester != 'WholeYear') {
    $semester = IntegerSafe($semester);
}
if ($targetClass != '' && $targetClass != '0') {
    if(is_numeric($targetClass)) {
        $sql = " SELECT YearID FROM YEAR ";
        $YearIDArr = $ldiscipline->returnVector($sql);
        if(!in_array($targetClass, (array)$YearIDArr)) {
            $targetClass = '0';
        }
    }
    else {
        $sql = " SELECT YearClassID FROM YEAR_CLASS WHERE AcademicYearID = '".Get_Current_Academic_Year_ID()."' ";
        $YearClassIDArr = $ldiscipline->returnVector($sql);
        
        $check_class_id = substr($targetClass, 2);
        if(!in_array($check_class_id, (array)$YearClassIDArr)) {
            $targetClass = '0';
        }
    }
}
$pic = IntegerSafe($pic);
$s = cleanCrossSiteScriptingCode($s);

$MeritType = IntegerSafe($MeritType);
if(isset($waitApproval)) {
    $waitApproval = IntegerSafe($waitApproval);
}
if(isset($approved)) {
    $approved = IntegerSafe($approved);
}
if(isset($rejected)) {
    $rejected = IntegerSafe($rejected);
}
if(isset($released)) {
    $released = IntegerSafe($released);
}
if(isset($unreleased)) {
    $unreleased = IntegerSafe($unreleased);
}
if(isset($waived)) {
    $waived = IntegerSafe($waived);
}
if(isset($fromPPC)) {
    $fromPPC = IntegerSafe($fromPPC);
}
$passedActionDueDate = IntegerSafe($passedActionDueDate);
### Handle SQL Injection + XSS [END]

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

# Check access right (View page)
if(!$ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$currentYearInfo = Get_Current_Academic_Year_ID();
$currentYearID = ($SchoolYear=="") ? $currentYearInfo : $SchoolYear;

$recordType = ($MeritType!="1" && $MeritType != "-1" && $MeritType != "-2") ? $i_Discipline_System_Award_Punishment_All_Records : "<a href='javascript:reloadForm(0)'>".$i_Discipline_System_Award_Punishment_All_Records."</a>";
$recordType .= " | ";
$recordType .= ($MeritType=="1") ? "<img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_merit.gif' width='20' height='20' border='0' align='absmiddle'> ".$i_Discipline_System_Award_Punishment_Awards : "<a href='javascript:reloadForm(1)'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_merit.gif' width='20' height='20' border='0' align='absmiddle'> ".$i_Discipline_System_Award_Punishment_Awards."</a>";
$recordType .= " | ";
$recordType .= ($MeritType=="-1") ? "<img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_demerit.gif' width='20' height='20' border='0' align='absmiddle'> ".$i_Discipline_System_Award_Punishment_Punishments : "<a href='javascript:reloadForm(-1)'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_demerit.gif' width='20' height='20' border='0' align='absmiddle'> ".$i_Discipline_System_Award_Punishment_Punishments."</a>";
if($sys_custom['eDiscipline']['PooiToMiddleSchool'])
{
	$recordType .= " | ";
	$recordType .= ($MeritType == "-2" ? $Lang['eDiscipline']['GenerateFromGM'] : '<a href="javascript:void(0);" onclick="reloadForm(\'-2\');">'.$Lang['eDiscipline']['GenerateFromGM'].'</a>');
}

######### Conditions #########
$conds = "";
$extraTable = "";

# Class #
if ($targetClass != '' && $targetClass!="0")
{
    if(is_numeric($targetClass)) {
    	$sql = "SELECT YearClassID FROM YEAR_CLASS WHERE YearID='$targetClass'";
    	$temp = $ldiscipline->returnVector($sql);
    	$conds .= (sizeof($temp)>0) ? " AND g.YearClassID IN (".implode(',', $temp).")" : ""; 
	}
    else {
    	$conds .= " AND g.YearClassID=".substr($targetClass,2);
    }
}

# Merit Type #
if($sys_custom['eDiscipline']['PooiToMiddleSchool'] && $MeritType == "-2"){
	$conds .= " AND a.fromConductRecords='1' ";
}
else if ($MeritType != '' && $MeritType != 0) {
    $conds .= " AND a.MeritType = $MeritType";
}
else {
	# some AP records from CASE may not have MeritType
	//$conds .= " AND (a.MeritType = 1 or a.MeritType=-1 or a.MeritType=0)";
	$MeritType = 0;	
}

$yearName = $ldiscipline->getAcademicYearNameByYearID($SchoolYear);

# Year Menu #
$yearSelected = 0;

$SchoolYear = ($SchoolYear == '') ? $currentYearInfo : $SchoolYear;
$yearArr = $ldiscipline->getAPSchoolYear($SchoolYear);

$selectSchoolYear = "<select name='SchoolYear' id='SchoolYear' onChange='document.form1.semester.value=\"\";reloadForm($MeritType)'>";
$selectSchoolYear .= "<option value='0'";
$selectSchoolYear .= ($SchoolYear==0) ? " selected" : "";
$selectSchoolYear .= ">".$i_Discipline_System_Award_Punishment_All_School_Year."</option>";
for($i=0; $i<sizeof($yearArr); $i++)
{
	$selectSchoolYear .= "<option value='".$yearArr[$i][0]."'";
	if($SchoolYear==$yearArr[$i][0]) {
		$selectSchoolYear .= " SELECTED";
	}
	if($currentYearInfo==$yearArr[$i][0]) {
		$yearSelected = 1;
	}
	$selectSchoolYear .= ">".$yearArr[$i][1]."</option>";
}
if($yearSelected==0)
{
	$selectSchoolYear .= "<option value='$currentYearInfo'";
	$selectSchoolYear .= ($currentYearInfo==$SchoolYear) ? " selected" : "";
	$selectSchoolYear .= ">".$ldiscipline->getAcademicYearNameByYearID($currentYearInfo)."</option>";
}
$selectSchoolYear .= "</select>";

if($SchoolYear != '' && $SchoolYear != 0) {
	$conds .= " AND a.AcademicYearID = $SchoolYear";	
}
//$extraConds = " AND g.AcademicYearID = a.AcademicYearID";

# Semester Menu #
$sql = "SELECT YearTermID, YearTermNameEN, YearTermNameB5 FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID=$SchoolYear ORDER BY TermStart";
$semResult = $ldiscipline->returnArray($sql, 3);
$SemesterMenu = "<select name='semester' id='semester' onChange='reloadForm($MeritType)'>";
$SemesterMenu .= "<option value='WholeYear'";
$SemesterMenu .= ($semester!='WholeYear') ? "" : " selected";
$SemesterMenu .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";
for($i=0; $i<sizeof($semResult); $i++)
{
	list($id, $nameEN, $nameB5) = $semResult[$i];
	$semName = Get_Lang_Selection($nameB5, $nameEN);
	$SemesterMenu .= "<option value='$id'";
	$SemesterMenu .= ($semester==$id) ? " selected" : "";
	$SemesterMenu .= ">$semName</option>";
}
$SemesterMenu .= "</select>";

if($semester != '' && $semester != 'WholeYear') {
	$conds .= " AND a.YearTermID = $semester";	
}

# Class #
//$select_class = $ldiscipline->getSelectClass("name=\"targetClass\" onChange=\"reloadForm($MeritType)\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes);
$select_class = $lclass->getSelectClassWithWholeForm("name=\"targetClass\" onChange=\"reloadForm($MeritType)\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes);

# PIC # 
if($semester != '' && $semester != 'WholeYear') $thisSemester = $semester;
$PICArray = $ldiscipline->getPICInArray("DISCIPLINE_MERIT_RECORD", $currentYearID, $thisSemester);
if(sizeof($PICArray)==0)
{
	$PICArray = array();
}

if($ldiscipline->PIC_SelectionDefaultOwn)
{
    //if(sizeof($_POST)==0 && sizeof($_GET)==0 && !isset($_POST['pic']))		    # default display own PIC records
	if(sizeof($_POST)==0 && !isset($_POST['pic']))
	{
		//$flag = 0;
		for($i=0; $i<sizeof($PICArray); $i++)
		{
			if($PICArray[$i][0]==$UserID)
			{
				$pic = $UserID;
				break;
			}
		}
	}
}

$picSelection = getSelectByArray($PICArray," name=\"pic\" onChange=\"reloadForm($MeritType)\"",$pic,0,0,"".$i_Discipline_Detention_All_Teachers."");
if($pic != "") {
    $conds .= " AND concat(',',a.PICID,',') LIKE '%,$pic,%'";
    //$conds .= " AND (a.PICID='$pic' OR a.PICID LIKE '$pic,%' OR a.PICID LIKE '%,$pic' OR a.PICID LIKE '%,$pic,%')";
}

// [2015-0416-1040-06164] Student Prefect can view self-created records only
if($sys_custom['eDiscipline']['add_AP_records_student_prefect'] && $ldiscipline->isStudentPrefect())
{
	$conds .= " AND a.CreatedBy = '$UserID'";
}

// [2015-0120-1200-33164] # Merit Item Tag Type #
if($sys_custom['Discipline_AP_Item_Tag_Seperate_Function'])
{
	$itemTagArray = array();
	$itemTagArray[] = array("1", $eDiscipline['Conduct_Mark']);
	$itemTagArray[] = array("2", $i_Discipline_System_Subscore1);
	$ItemTagSelection = getSelectByArray($itemTagArray, " name=\"itemTag\" onChange=\"reloadForm($MeritType)\"", $itemTag, 0, 0, "".$i_Discipline_System_Award_Punishment_All_Records."");
	
	if($itemTag > 0)
	{
		$extraTable .= " LEFT JOIN DISCIPLINE_AP_ITEM_TAG ait ON (a.ItemID = ait.APItemID AND ait.TagID = '$itemTag') ";
		$conds .= " AND ait.TagID = '$itemTag' ";
	}
}

$conds2 = "";

# Waiting for Approval #
if($waitApproval == 1 || (!isset($released) && !isset($unreleased) && !isset($fromPPC) && !isset($fromOverview)))
{
	$waitApprovalChecked = "checked";
	
	if($fromOverview && $useApprovalGroup)
	{
		if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eDiscipline"])
		{
			$hasRightToApproveRecordID = $ldiscipline->RETURN_AP_RECORDID_CAN_BE_APPROVED($UserID, $currentYearID);
			if(sizeof($hasRightToApproveRecordID) > 0) {
				$tempConds .= " AND a.RecordID IN (".implode(',',$hasRightToApproveRecordID).")";
			}
			else { 
				$tempConds .= " AND a.RecordID=0 ";		# if no record waiting for approval by current user, then return no record in SQL
			}
		}
	}
	
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.RecordStatus = ".DISCIPLINE_STATUS_PENDING.$tempConds;	
}
/*
if($unreleased != '') {
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "((a.RecordStatus=".DISCIPLINE_STATUS_APPROVED." OR a.RecordStatus=".DISCIPLINE_STATUS_WAIVED.") AND (a.ReleaseStatus=".DISCIPLINE_STATUS_UNRELEASED." OR a.ReleaseStatus IS NULL OR a.ReleaseStatus=NULL))";
}
*/

# Unreleased #
if($unreleased == '0' || (!isset($released) && !isset($unreleased) && !isset($fromPPC) && !isset($fromOverview)))
{
	$unreleasedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "((a.RecordStatus=".DISCIPLINE_STATUS_APPROVED." OR a.RecordStatus=".DISCIPLINE_STATUS_WAIVED.") AND (a.ReleaseStatus=".DISCIPLINE_STATUS_UNRELEASED." OR a.ReleaseStatus IS NULL OR a.ReleaseStatus=NULL))";
}

# Approved #  (waived record also be approved)
if($approved == 1 || (!isset($released) && !isset($unreleased) && !isset($fromPPC) && !isset($fromOverview)))
{
	if($released == DISCIPLINE_STATUS_UNRELEASED) {
		$conds2 .= ($conds2=="") ? "(" : " OR ";
		$conds2 .= "(a.RecordStatus = ".DISCIPLINE_STATUS_APPROVED." AND (a.ReleaseStatus = ".DISCIPLINE_STATUS_UNRELEASED." OR a.ReleaseStatus IS NULL OR a.ReleaseStatus=NULL))";
	}
	else if($released == DISCIPLINE_STATUS_RELEASED) {
		$conds2 .= ($conds2=="") ? "(" : " OR ";
		$conds2 .= "(a.RecordStatus = ".DISCIPLINE_STATUS_APPROVED." AND (a.ReleaseStatus = ".DISCIPLINE_STATUS_UNRELEASED." OR a.ReleaseStatus IS NULL OR a.ReleaseStatus=NULL))";
	}
	else {
		$conds2 .= ($conds2=="") ? "(" : " OR ";
		$conds2 .= "(a.RecordStatus = ".DISCIPLINE_STATUS_APPROVED.")";
	}
	$approvedChecked = "checked";
}

# Rejected #
if($rejected == 1 || (!isset($released) && !isset($unreleased) && !isset($fromPPC) && !isset($fromOverview)))
{
	$rejectedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "(a.RecordStatus = ".DISCIPLINE_STATUS_REJECTED.")";	
}

# Released #
if($released == 1 || (!isset($released) && !isset($unreleased) && !isset($fromPPC) && !isset($fromOverview)))
{
	$releasedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.ReleaseStatus = ".DISCIPLINE_STATUS_RELEASED;	
}

# Waived #
if($waived == 1 || (!isset($released) && !isset($unreleased) && !isset($fromPPC) && !isset($fromOverview)))
{
	$waivedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "(a.RecordStatus = ".DISCIPLINE_STATUS_WAIVED.")";	
}
/*
# not use in this phase
# Locked #
if($locked == 1) {
	$lockedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.LockStatus = ".DISCIPLINE_STATUS_LOCK;	
}
*/
$conds2 .= ($conds2 != "") ? ")" : "";

# Check whether current user has the approval right (according to settings in "Approval Group")
$hasRightToRejectRecordID = array();
if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eDiscipline"])
{
	$hasRightToRejectRecordID = $ldiscipline->RETURN_AP_RECORDID_CAN_BE_APPROVED($UserID, "");
	$hasApprovalRight = (sizeof($hasRightToRejectRecordID)>0) ? 1 : 0;
	
	# any access rights setting for approval
	$hasApprovalRight = $hasApprovalRight || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-APPROVAL");
}
else 
{
	$hasApprovalRight = 1;
}
/*
$useApprovalGroup = $ldiscipline->useApprovalGroup();
debug($useApprovalGroup);
if($useApprovalGroup && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eDiscipline"])
{
	$hasApprovalRight = (sizeof($hasRightToRejectRecordID)>0 && in_array($id,$hasRightToRejectRecordID)) ? 1 : 0;
debug_r($hasRightToRejectRecordID);
}
*/
##############

$i_Merit_Merit = addslashes($i_Merit_Merit);
$i_Merit_MinorCredit = addslashes($i_Merit_MinorCredit);
$i_Merit_MajorCredit = addslashes($i_Merit_MajorCredit);
$i_Merit_SuperCredit = addslashes($i_Merit_SuperCredit);
$i_Merit_UltraCredit = addslashes($i_Merit_UltraCredit);

$li = new libdbtable2007($field, $order, $pageNo);

//$const_status_pending = 0;

$meritImg = "<img src={$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_merit.gif width=20 height=20 border=0 align=absmiddle border=0 title=\'$i_Merit_Award\'>";
$demeritImg = "<img src={$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_demerit.gif width=20 height=20 border=0 align=absmiddle border=0 title=\'$i_Merit_Punishment\'>";

$student_namefield = getNamefieldByLang("b.");
$clsName = ($intranet_session_language=="en") ? "g.ClassTitleEN" : "g.ClassTitleB5";

// Search Bar
$searchTag = $linterface->Get_Search_Box_Div('text', stripslashes($s));

// [2014-1216-1347-28164]
$pushMsgDisplay = $canSendPushMsg? " ,a.PushMessageID" : "";

// [2015-0611-1642-26164]
$RehabilDisplay = $sys_custom['eDiscipline']['CSCProbation']? " ,a.RehabilPIC" : "";

// [2015-0120-1200-33164]
$SubScoreDisplay = $ldiscipline->UseSubScore? " a.SubScore1Change, " : "";
$SubScoreDisplay = $ldiscipline->UseActScore? " a.SubScore1Change, a.SubScore2Change, " : $SubScoreDisplay;

// [2020-0824-1151-40308]
$followUpRemarkDisplay = $sys_custom['eDiscipline']['APFollowUpRemarks'] ? " ,a.FollowUpRemark" : "";

if($intranet_session_language=="en")
{
	$meritStr = "CONCAT(
                    a.ProfileMeritCount, ' ',
    				CASE (a.ProfileMeritType)
        				WHEN 0 THEN '$i_Merit_Warning'
        				WHEN 1 THEN '$i_Merit_Merit'
        				WHEN 2 THEN '$i_Merit_MinorCredit'
        				WHEN 3 THEN '$i_Merit_MajorCredit'
        				WHEN 4 THEN '$i_Merit_SuperCredit'
        				WHEN 5 THEN '$i_Merit_UltraCredit'
        				WHEN -1 THEN '$i_Merit_BlackMark'
        				WHEN -2 THEN '$i_Merit_MinorDemerit'
        				WHEN -3 THEN '$i_Merit_MajorDemerit'
        				WHEN -4 THEN '$i_Merit_SuperDemerit'
        				WHEN -5 THEN '$i_Merit_UltraDemerit'
				    ELSE 'Error' END, '(s)'
                )";
}
else
{
	$meritStr = "CONCAT(
    				CASE (a.ProfileMeritType)
        				WHEN 0 THEN '$i_Merit_Warning'
        				WHEN 1 THEN '$i_Merit_Merit'
        				WHEN 2 THEN '$i_Merit_MinorCredit'
        				WHEN 3 THEN '$i_Merit_MajorCredit'
        				WHEN 4 THEN '$i_Merit_SuperCredit'
        				WHEN 5 THEN '$i_Merit_UltraCredit'
        				WHEN -1 THEN '$i_Merit_BlackMark'
        				WHEN -2 THEN '$i_Merit_MinorDemerit'
        				WHEN -3 THEN '$i_Merit_MajorDemerit'
        				WHEN -4 THEN '$i_Merit_SuperDemerit'
        				WHEN -5 THEN '$i_Merit_UltraDemerit'
    				ELSE 'Error' END, 
				    cast(a.ProfileMeritCount as char), '". $Lang['eDiscipline']['Times'] ."', ' '
				)";
}

if($fromPPC==1 || $passedActionDueDate==1)
{
	$result = ($fromPPC==1) ? $ldiscipline->retrieveMeritRecordFromPPC() : $ldiscipline->getPassedActionDueDateAPRecord($SchoolYear);
	$MeritRecordID = (sizeof($result)>0) ? implode(',', $result) : "''";
	
	$sql = "SELECT
    			CONCAT($clsName, ' - ', f.ClassNumber) as ClassNameNum,
    			$student_namefield as EnglishName, 
    			IF(a.MeritType>0, '$meritImg', '$demeritImg') as meritImg,
    			IF(a.ProfileMeritCount=0, '--', ". $meritStr .") as record,";
	if($ldiscipline->Display_ConductMarkInAP)
	{
		if($sys_custom['eDiscipline']['yy3']) {
			$sql .= "IF((a.OverflowConductMark='1' OR a.OverflowMeritItemScore='1' OR a.OverflowTagScore='1') AND a.ConductScoreChange=0, '".$ldiscipline_ui->displayMaskedMark('0')."', a.ConductScoreChange) as ConductScoreChange,";	
		}
		else {
			$sql .= "a.ConductScoreChange,";
		}
	}
	$sql .= $SubScoreDisplay;
	$sql .= "   IF(a.ItemID=0, 
    			     CONCAT('<a href=\"javascript:go_detail(', a.RecordID, ')\">--</a>'),
    			     CONCAT('<a title=\"', REPLACE(c.ItemCode, '\"', '&quot;'), ' - ', REPLACE(c.ItemName, '\"', '&quot;'), '\" href=\"javascript:go_detail(', a.RecordID, ')\">', c.ItemCode, ' - ', REPLACE(c.ItemName, '<', '&lt;'), '</a>')
    			) as item,
    			a.RecordDate,
    			PICID, 
    			a.fromConductRecords as reference,
    			a.TemplateID as action,
    			LEFT(a.DateModified, 10),
    			a.ReleaseStatus as status,
    			CONCAT('<input type=\'checkbox\' name=\'RecordID[]\' id=\'RecordID[]\' value=', a.RecordID ,'>'),
    			a.WaivedBy, 
    			a.RecordStatus,
    			a.RecordID,
    			a.CaseID,
    			a.NoticeID,
    			b.UserID,
    			a.Remark
    			$pushMsgDisplay
    			$RehabilDisplay
    			$followUpRemarkDisplay
			FROM 
			    DISCIPLINE_MERIT_RECORD as a
				INNER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
				LEFT OUTER JOIN DISCIPLINE_MERIT_ITEM as c ON a.ItemID = c.ItemID
				LEFT OUTER JOIN DISCIPLINE_MERIT_TYPE_SETTING as d ON a.ProfileMeritType = d.MeritType
				LEFT OUTER JOIN INTRANET_USER as e ON a.PICID = e.UserID
				LEFT OUTER JOIN YEAR_CLASS_USER as f ON (b.UserID=f.UserID)
				LEFT OUTER JOIN YEAR_CLASS as g ON (f.YearClassID=g.YearClassID)
				$extraTable
			WHERE
                a.DateInput IS NOT NULL AND g.AcademicYearID=$currentYearInfo AND
                a.RecordID IN ($MeritRecordID) AND b.RecordStatus IN (0, 1, 2)
				$conds ";
		$sql .= ($conds2 != '') ? " AND ".$conds2 : "";
		$sql .= " GROUP BY a.RecordID";
		
		//if($sys_custom['eDis_AP_export_WEBSAMS_format']) {
		$sqlRecord = "  SELECT
                            a.RecordID
                		FROM DISCIPLINE_MERIT_RECORD as a
            				INNER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
            				LEFT OUTER JOIN DISCIPLINE_MERIT_ITEM as c ON a.ItemID = c.ItemID
            				LEFT OUTER JOIN DISCIPLINE_MERIT_TYPE_SETTING as d ON a.ProfileMeritType = d.MeritType
            				LEFT OUTER JOIN INTRANET_USER as e ON a.PICID = e.UserID
            				LEFT OUTER JOIN YEAR_CLASS_USER as f ON (b.UserID=f.UserID)
            				LEFT OUTER JOIN YEAR_CLASS as g ON (f.YearClassID=g.YearClassID)
            			WHERE a.DateInput IS NOT NULL AND g.AcademicYearID=$currentYearInfo AND 
                            a.RecordID IN ($MeritRecordID) AND b.RecordStatus IN (0,1,2)
            				$conds ";
		$sqlRecord .= ($conds2 != '') ? " AND ".$conds2 : "";
		$sqlRecord .= " GROUP BY a.RecordID";
		$sqlResultSet = $ldiscipline->returnVector($sqlRecord);
		//}
}
else    			# filter by option & search
{
	$s = addslashes(trim($s));
	$sql = "SELECT
    			CONCAT($clsName, ' - ', f.ClassNumber) as ClassNameNum,
    			$student_namefield as EnglishName, 
    			IF(a.MeritType>0, '$meritImg', '$demeritImg') as meritImg,
    			IF(a.ProfileMeritCount=0, '--', ". $meritStr .") as record,";
	if($ldiscipline->Display_ConductMarkInAP)
	{
		if($sys_custom['eDiscipline']['yy3']) {
			$sql .= "IF((a.OverflowConductMark='1' OR a.OverflowMeritItemScore='1' OR a.OverflowTagScore='1') AND a.ConductScoreChange=0, '".$ldiscipline_ui->displayMaskedMark('0')."', a.ConductScoreChange) as ConductScoreChange,";	
		}
		else {
			$sql .= "a.ConductScoreChange,";
		}
	}
	$sql .= $SubScoreDisplay;
	$sql .= "  IF(a.ItemID=0, 
        			CONCAT('<a href=\"javascript:go_detail(', a.RecordID, ')\">--</a>'),
        			CONCAT('<a title=\"', REPLACE(c.ItemCode, '\"', '&quot;'), ' - ', REPLACE(c.ItemName, '\"', '&quot;'), '\" href=\"javascript:go_detail(', a.RecordID, ')\">', c.ItemCode, ' - ', REPLACE(c.ItemName, '<', '&lt;'), '</a>')
    			) as item,
    			a.RecordDate,
    			PICID, 
    			a.fromConductRecords as reference,
    			a.TemplateID as action,
    			LEFT(a.DateModified, 10),
    			a.ReleaseStatus as status,
    			CONCAT('<input type=\'checkbox\' name=\'RecordID[]\' id=\'RecordID[]\' value=', a.RecordID ,'>'),
    			a.WaivedBy, 
    			a.RecordStatus,
    			a.RecordID,
    			a.CaseID,
    			a.NoticeID,
    			b.UserID,
    			a.Remark,
    			a.ApprovedBy,
    			a.ApprovedDate,
    			a.ReleasedBy,
    			a.ReleasedDate,
    			a.RejectedBy,
    			a.RejectedDate,
    			a.WaivedDate
    			$pushMsgDisplay
    			$RehabilDisplay
    			$followUpRemarkDisplay
			FROM 
			    DISCIPLINE_MERIT_RECORD as a
				INNER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
				LEFT OUTER JOIN DISCIPLINE_MERIT_ITEM as c ON a.ItemID = c.ItemID
				LEFT OUTER JOIN DISCIPLINE_MERIT_TYPE_SETTING as d ON a.ProfileMeritType = d.MeritType
				LEFT OUTER JOIN YEAR_CLASS_USER as f ON (a.StudentID=f.UserID AND f.UserID=a.StudentID)
				LEFT OUTER JOIN YEAR_CLASS as g ON (f.YearClassID=g.YearClassID)
				$extraTable
			WHERE
                a.DateInput IS NOT NULL AND g.AcademicYearID=$currentYearInfo AND
                b.RecordStatus IN (0,1,2)
				$conds
				AND (
					g.ClassTitleEN LIKE '%$s%' 
					OR g.ClassTitleB5 LIKE '%$s%'
					OR f.ClassNumber LIKE '%$s%'
					OR $student_namefield LIKE '%$s%'
					OR c.ItemCode LIKE '%$s%'
					OR c.ItemName LIKE '%$s%'
					OR a.RecordDate LIKE '%$s%'
				) ";
		$sql .= ($conds2 != '') ? " AND ".$conds2 : "";
		
		//if($sys_custom['eDis_AP_export_WEBSAMS_format']) {
		$sqlRecord = "  SELECT
                            a.RecordID 
				        FROM DISCIPLINE_MERIT_RECORD as a
            				INNER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
            				LEFT OUTER JOIN DISCIPLINE_MERIT_ITEM as c ON a.ItemID = c.ItemID
            				LEFT OUTER JOIN DISCIPLINE_MERIT_TYPE_SETTING as d ON a.ProfileMeritType = d.MeritType
            				LEFT OUTER JOIN YEAR_CLASS_USER as f ON (a.StudentID=f.UserID AND f.UserID=a.StudentID)
            				LEFT OUTER JOIN YEAR_CLASS as g ON (f.YearClassID=g.YearClassID)
				        WHERE 
                            a.DateInput IS NOT NULL AND g.AcademicYearID=$currentYearInfo AND 
                            b.RecordStatus IN (0, 1, 2)
            				$conds
            				AND (
            					g.ClassTitleEN LIKE '%$s%' 
            					OR g.ClassTitleB5 LIKE '%$s%'
            					OR f.ClassNumber LIKE '%$s%'
            					OR $student_namefield LIKE '%$s%'
            					OR c.ItemCode LIKE '%$s%'
            					OR c.ItemName LIKE '%$s%'
            					OR a.RecordDate LIKE '%$s%'
            				) ";	
		$sqlRecord .= ($conds2 != '') ? " AND ".$conds2 : "";
		$sqlResultSet = $ldiscipline->returnVector($sqlRecord);
		//}
}
//echo $sql;

$extraLocation = 0;
$MeritCountLocation = 3;
if($ldiscipline->Display_ConductMarkInAP)
{
	$conductMarkLocation = 4;
	if($ldiscipline->UseSubScore) {
		$SubScoreLocation = 5;
		$extraLocation++;
	}
	if($ldiscipline->UseActScore) {
		$SubScore2Location = 6;
		$extraLocation++;
	}
	$categoryItemLocation = 5 + $extraLocation;
	$PICLocation = 7 + $extraLocation;
	$historyLocation = 8 + $extraLocation;
	$actionLocation = 9 + $extraLocation;
	$waivedByLocation = 11 + $extraLocation;
	$checkboxLocation = 12 + $extraLocation;
	$StatusLocation = 14 + $extraLocation;
	$recordIDLocation = 15 + $extraLocation;
	$caseIDLocation = 16 + $extraLocation;
	$noticeIDLocation = 17 + $extraLocation;
	$remarkLocation = 19 + $extraLocation;
	
	// [2014-1216-1347-28164] location of Push Message ID
	$currentLocation = 27 + $extraLocation;
	if($canSendPushMsg){
		//$pushMsgLocation = 27;
		$pushMsgLocation = $currentLocation++;
	}
	else {
		$pushMsgLocation = 99;	# no use
	}
	// [2015-0611-1642-26164] location of Rehabilitation PIC
	if($sys_custom['eDiscipline']['CSCProbation']){
		//$RehabilLocation = $canSendPushMsg? 28 : 27;
		$RehabilLocation = $currentLocation++;
	}
	else {
		$RehabilLocation = 99;	# no use
	}

	// [2020-0824-1151-40308]
    if($sys_custom['eDiscipline']['APFollowUpRemarks']){
        $followUpRemarkLocation = $currentLocation++;
    }
    else {
        $followUpRemarkLocation = 99;	# no use
    }
}
else
{
	if($ldiscipline->UseSubScore) {
		$SubScoreLocation = 4;
		$extraLocation++;
	}
	if($ldiscipline->UseActScore) {
		$SubScore2Location = 5;
		$extraLocation++;
	}
	$categoryItemLocation = 4 + $extraLocation;
	$PICLocation = 6 + $extraLocation;
	$historyLocation = 7 + $extraLocation;
	$actionLocation = 8 + $extraLocation;
	$waivedByLocation = 10 + $extraLocation;
	$checkboxLocation = 11 + $extraLocation;
	$StatusLocation = 13 + $extraLocation;
	$recordIDLocation = 14 + $extraLocation;
	$caseIDLocation = 15 + $extraLocation;
	$noticeIDLocation = 16 + $extraLocation;
	$remarkLocation = 18 + $extraLocation;
	
	// [2014-1216-1347-28164] location of Push Message ID
	$currentLocation = 26 + $extraLocation;
	if($canSendPushMsg){
//		$pushMsgLocation = 26;
		$pushMsgLocation = $currentLocation++;
	}
	else {
		$pushMsgLocation = 99;	# no use
	}
	// [2015-0611-1642-26164] location of Rehabilitation PIC
	if($sys_custom['eDiscipline']['CSCProbation']){
//		$RehabilLocation = $canSendPushMsg? 27 : 26;
		$RehabilLocation = $currentLocation++;
	}
	else {
		$RehabilLocation = 99;	# no use
	}

	// [2020-0824-1151-40308]
    if($sys_custom['eDiscipline']['APFollowUpRemarks']){
        $followUpRemarkLocation = $currentLocation++;
    }
    else {
        $followUpRemarkLocation = 99;	# no use
    }
}

if($sys_custom['eDiscipline']['MoPuiChingRedirToPersonalReport'])
{
	$nameLocation = 1;
}

//$li->field_array[] = "ClassNameNum";
$li->field_array[] = "concat(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', 1)), IF(INSTR(ClassNameNum, '-') = 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', -1)), 10, '0')))";
$li->field_array[] = "b.EnglishName";
$li->field_array[] = "meritImg";
$li->field_array[] = "record";
if($ldiscipline->Display_ConductMarkInAP) {
	$li->field_array[] = "ConductScoreChange";
}
if($ldiscipline->UseSubScore) {
	$li->field_array[] = "SubScore1Change";
}
if($ldiscipline->UseActScore) {
	$li->field_array[] = "SubScore2Change";
}
$li->field_array[] = "item";
$li->field_array[] = "a.RecordDate";
$li->field_array[] = "PICID";
$li->field_array[] = "reference";
$li->field_array[] = "action";
$li->field_array[] = "a.DateModified";
$li->field_array[] = "status";

$li->sql = $sql;
$li->no_col = sizeof($li->field_array) + 2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0,0,0,0,0,0,0,0,0,0);
if($ldiscipline->Display_ConductMarkInAP) {
	$li->column_array[] = "0";
}
if($ldiscipline->UseSubScore) {
	$li->column_array[] = "0";
}
if($ldiscipline->UseActScore) {
	$li->column_array[] = "0";
}
$li->wrap_array = array(0,0,0,0,0,0,0,0,0,0,0);
if($ldiscipline->Display_ConductMarkInAP) {
	$li->wrap_array[] = "0";
}
if($ldiscipline->UseSubScore) {
	$li->wrap_array[] = "0";
}
if($ldiscipline->UseActScore) {
	$li->wrap_array[] = "0";
}

$li->IsColOff = 2;
//$li->fieldorder2 = ", RecordID desc";
//$li->fieldorder2 = ", ClassNameNum";
$li->fieldorder2 = ", a.DateInput desc, ClassNameNum";

# Table Column
$pos = 0;
$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='8%' nowrap>".$li->column($pos++, $i_general_class)."</td>\n";
$li->column_list .= "<td width='8%' nowrap>".$li->column($pos++, $i_general_name)."</td>\n";
$li->column_list .= "<td width='1'>&nbsp;</td>\n";$pos++;
$li->column_list .= "<td width='80' nowrap>".$li->column($pos++, $i_Discipline_Reason2)."</td>\n";
if($ldiscipline->Display_ConductMarkInAP) {
	$li->column_list .= "<td width='80' nowrap>".$li->column($pos++, $eDiscipline['Conduct_Mark'])."</td>\n";
}
if($ldiscipline->UseSubScore) {
	$li->column_list .= "<td width='60' nowrap>".$li->column($pos++, $i_Discipline_System_Subscore1)."</td>\n";
}
if($ldiscipline->UseActScore) {
	$li->column_list .= "<td width='60' nowrap>".$li->column($pos++, $Lang['eDiscipline']['ActivityScore'])."</td>\n";
}
$li->column_list .= "<td>".$li->column($pos++, $i_Discipline_System_Item_Code_Item_Name)."</td>\n";
$li->column_list .= "<td width='10%' nowrap>".$li->column($pos++, $Lang['eDiscipline']['EventDate'])."</td>\n";
$li->column_list .= "<td width='15%' nowrap>".$i_Profile_PersonInCharge."</td>\n";$pos++;
$li->column_list .= "<td width='80' nowrap>$i_Discipline_System_Award_Punishment_Reference</td>\n";$pos++;
$li->column_list .= "<td width='80' nowrap>".$eDiscipline["Action"]."</td>\n";$pos++;
$li->column_list .= "<td width='10%' nowrap>".$li->column($pos++, $i_Discipline_Last_Updated)."</td>\n";
$li->column_list .= "<td>".$i_Discipline_System_Discipline_Status."</td>\n";
$li->column_list .= "<td>".$li->check("RecordID[]")."</td>\n";

/*
$MeritCountLocation = 3;
$conductMarkLocation = 4;
$categoryItemLocation = 5;
$PICLocation = 7;
$historyLocation = 8;
$actionLocation = 9;
$waivedByLocation = 11;
$checkboxLocation = 12;
$StatusLocation = 14;
$recordIDLocation = 15;
$caseIDLocation = 16;
$noticeIDLocation = 17;
$remarkLocation = 19;
*/

######## end of table content #########
#######################################

# Access Right Checking
$actionLayer = "";

//if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Approval") || $hasApprovalRight) {
if($hasApprovalRight)
{
	$actionLayer .= "<tr>";
	$actionLayer .= "<td align='left' valign='top' class='tabletext'><a href=\"javascript:changeFormAction(document.form1,'RecordID[]','approve_update.php')\" class='tabletool'><img src='{$image_path}/{$LAYOUT_SKIN}/icon_approve.gif' width='12' height='12' border='0' align='absmiddle'> $i_Discipline_System_Award_Punishment_Approve</a></td>";
	$actionLayer .= "</tr><tr>";
	$actionLayer .= "<td align='left' valign='top'><a href=\"javascript:changeFormAction(document.form1, 'RecordID[]','reject_update.php')\" class='tabletool'><img src='{$image_path}/{$LAYOUT_SKIN}/icon_reject_l.gif' width='12' height='12' border='0' align='absmiddle'> $i_Discipline_System_Award_Punishment_Reject</a></td>";
	$actionLayer .= "</tr>";
	if($sys_custom['eDiscipline']['CSCProbation'])
	{
		$actionLayer .= "<tr>";
		$actionLayer .= "<td align='left' valign='top'><a href=\"javascript:changeFormAction(document.form1, 'RecordID[]','probation_update.php')\" class='tabletool'><img src='{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif' width='12' height='12' border='0' align='absmiddle'> ".$Lang['eDiscipline']['CWCProbation']['Probation']."</a></td>";
		$actionLayer .= "</tr>";
	}
}
if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Release"))
{
	if($actionLayer != "")
	{
		$actionLayer .= "<tr>";
		$actionLayer .= "<td align='left' valign='top' class='dotline' height='2'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' height='2'></td>";
		$actionLayer .= "</tr>";
	}
	$actionLayer .= "<tr>";
	$actionLayer .= "<td align='left' valign='top' class='tabletext'><a href=\"javascript:changeFormAction(document.form1,'RecordID[]','release_update.php')\" class='tabletool'><img src='{$image_path}/{$LAYOUT_SKIN}/icon_to_student_s.gif' width='12' height='12' border='0' align='absmiddle'> $i_Discipline_System_Award_Punishment_Release</a></td>";
	$actionLayer .= "</tr><tr>";
	$actionLayer .= "<td align='left' valign='top'><a href=\"javascript:changeFormAction(document.form1,'RecordID[]','unrelease_update.php')\" class='tabletool'><img src='{$image_path}/{$LAYOUT_SKIN}/icon_private.gif' width='12' height='12' border='0' align='absmiddle'> $i_Discipline_System_Award_Punishment_UnRelease</a></td>";
	$actionLayer .= "</tr>";
}
if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Waive"))
{
	if($actionLayer != "")
	{
		$actionLayer .= "<tr>";
		$actionLayer .= "<td align='left' valign='top' class='dotline' height='2'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' height='2'></td>";
		$actionLayer .= "</tr>";
	}
	
	# Waive layer
	$actionLayer .= "<tr>";
	$actionLayer .= "<td align='left' valign='top'><a href=\"javascript:changeFormAction(document.form1,'RecordID[]','waive.php')\" class='tabletool'><img src='{$image_path}/{$LAYOUT_SKIN}/icon_waive.gif' width='12' height='12' border='0' align='absmiddle'> $i_Discipline_System_Award_Punishment_Waive</a></td>\n";
	$actionLayer .= "</tr>";

	if(!$sys_custom['eDiscipline']['HideRedeem'])
	{
		$actionLayer .= "<tr>";
		$actionLayer .= "<td align='left' valign='top' class='dotline' height='2'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' height='2'></td>";
		$actionLayer .= "</tr>";
	
		# Redeem layer
		$actionLayer .= "<tr>";
		$actionLayer .= "<td align='left' valign='top'><a href=\"javascript:changeFormAction(document.form1,'RecordID[]','redeem_step1.php', 'redeem')\" class='tabletool'><img src='{$image_path}/{$LAYOUT_SKIN}/icon_waive.gif' width='12' height='12' border='0' align='absmiddle'> $i_Discipline_System_Award_Punishment_Redeem</a></td>\n";
		$actionLayer .= "</tr>";
	}
}
// [2017-0317-1025-03225]
if($sys_custom['eDiscipline']['TKP_RainbowScheme'] && $ldiscipline->IS_ADMIN_USER($UserID) && $hasApprovalRight)
{
    $actionLayer .= "<tr>";
    $actionLayer .= "<td align='left' valign='top' class='dotline' height='2'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' height='2'></td>";
    $actionLayer .= "</tr>";
    
    $actionLayer .= "<tr>";
    $actionLayer .= "<td align='left' valign='top'><a href=\"javascript:changeFormAction(document.form1, 'RecordID[]', 'rb_scheme_update.php')\" class='tabletool'><img src='{$image_path}/{$LAYOUT_SKIN}/icon_waive.gif' width='12' height='12' border='0' align='absmiddle'> ".$Lang['eDiscipline']['TKP']['JoinRainbowScheme']."</a></td>";
    $actionLayer .= "</tr>";
}

# Menu Highlight
$CurrentPage = "Management_AwardPunishment";
$CurrentPageArr['eDisciplinev12'] = 1;

# Left Menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($eDiscipline['Award_and_Punishment'], "");

$exportLink = "export.php?s=$s&SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&MeritType=$MeritType&waitApproval=$waitApproval&approved=$approved&rejected=$rejected&released=$released&unreleased=$unreleased&waived=$waived&locked=$locked&fromPPC=$fromPPC&fromOverview=$fromOverview&passedActionDueDate=$passedActionDueDate&pic=$pic";

if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-New"))
{
	$toolbar = $linterface->GET_LNK_NEW("new1.php",$btn_new,"","","",0);
	
	// [2015-0120-1200-33164] Add Merit with Tag Type
	if($sys_custom['Discipline_AP_Item_Tag_Seperate_Function'])
	{
		$toolbar = $linterface->GET_LNK_NEW("new1.php?type=1", "$button_new (".$eDiscipline['Conduct_Mark'].")", "", "", "", 0);
		$toolbar .= $linterface->GET_LNK_NEW("new1.php?type=2", "$button_new ($i_Discipline_System_Subscore1)", "", "", "", 0);
	}
	
	$toolbar .= $linterface->GET_LNK_IMPORT("import.php", $btn_import, "", "", "", 0);
}

//if($sys_custom['eDis_AP_export_WEBSAMS_format']) {
// 	$toolbar .= $linterface->GET_LNK_EXPORT("javascript:displayExport()",$btn_export,"","","",0);
	$generalExport = "<a href=\"javascript:;\" onClick=\"document.form1.export_format.value='general';showSpan('spanExport')\" class=\"sub_btn\"> ".$Lang['eDiscipline']['NormalExport']."</a>";
	//$websamsExport = "<a href=\"javascript:;\" onClick=\"goExportWebSAMS()\" class=\"sub_btn\"> ".$Lang['eDiscipline']['WebSAMSFormatExport']."</a>";
	$websamsExport = "<a href=\"javascript:;\" onClick=\"document.form1.export_format.value='websams';showSpan('spanExport')\" class=\"sub_btn\"> ".$Lang['eDiscipline']['WebSAMSFormatExport']."</a>";
	$toolbar .= '
				<div class="btn_option"  id="ExportDiv"  >

					<a onclick="js_Clicked_Option_Layer(\'export_option\', \'btn_export\');" id="btn_export" class="export option_layer" href="javascript:void(0);"> '.$Lang['Btn']['Export'].'</a>
					<br style="clear: both;">
					<div onclick="js_Clicked_Option_Layer_Button(\'export_option\', \'btn_export\');" id="export_option" class="btn_option_layer" style="visibility: hidden;">
					'.$generalExport.'
					'.$websamsExport.'
					</div>
				</div>'; 
// } else {
// 	$toolbar .= $linterface->GET_LNK_EXPORT("javascript:showSpan('spanExport')",$btn_export,"","","",0);
// }

if($sys_custom['ReCalculateAccumulatedRecord'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-eDiscipline"] && !$sys_custom['eDiscipline']['PooiToMiddleSchool']) {
	$toolbar .= $linterface->GET_LNK_GENERATE("recalculateRecord.php",$Lang['eDiscipline']['ReCalculateAccumulatedRecord'],"","","",0);
}
//$toolbar .= $linterface->GET_LNK_GENERATE("generateConductAP.php", "Generate Conduct AP", "", "", "", 0);

# Start layout
$linterface->LAYOUT_START();
?>

<script language="javascript">

var targetDivID = "";
var clickDivID = "";

function Show_Waive_Window(record_id){
    obj = document.form1;
    obj.targetDivID.value = record_id;
    obj.flag.value = 'waived';
    obj.ajaxRecordID.value = record_id;
    clickDivID = 'show_waive'+record_id;
    targetDivID = record_id;
    YAHOO.util.Connect.setForm(obj);	
    
    var path;
    path = "get_live.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback2);
}

var callback2 = {
    success: function ( o )
    {
		DisplayDefDetail(o.responseText);
	}
}

function DisplayDefDetail(text){
	document.getElementById('div_form').innerHTML = text;
	DisplayPosition();
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}
	return pos_value;
}

function DisplayPosition() {
	var posleft = getPosition(document.getElementById(clickDivID),'offsetLeft') - 370;
	var postop = getPosition(document.getElementById(clickDivID),'offsetTop') + 10;

	document.getElementById(targetDivID).style.left = posleft+"px";
	document.getElementById(targetDivID).style.top = postop+"px";
	document.getElementById(targetDivID).style.visibility = 'visible';
}

function Hide_Window(pos){
  document.getElementById(pos).style.visibility = 'hidden';
}

function displayExport() {
	if(document.getElementById('export_option').style.visibility=='visible') {
		document.getElementById('export_option').style.visibility = 'hidden';
	}
	else {
		document.getElementById('export_option').style.visibility = 'visible';
	}
}

function goExportWebSAMS() {
	document.form1.action = "exportToWebSAMS.php";
	document.form1.submit();
	document.form1.action = "";
}
</script>

<script language="javascript">
<!--
function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) 
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}

function removeCat(obj,element,page){
	var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
	if(countChecked(obj,element)==0)
        alert(globalAlertMsg2);
	else
	{
        if(confirm(alertConfirmRemove)){
	        obj.action=page;
	        obj.method="post";
	        obj.submit();
        }
	}
}

function reloadForm(val) {
	document.form1.MeritType.value = val;
	document.form1.s.value = document.form2.text.value;
	document.form1.pageNo.value = 1;
	//alert(document.form1.s.value);
	document.form1.action = "index.php";
	document.form1.submit();	
}

//function goSearch(obj) {
//	/*
//	if(document.form2.text.value=="") {
//		alert("<?=$i_Discipline_System_Award_Punishment_Search_Alert?>");	
//		document.form2.text.focus();
//		return false;
//	}
//	else {
//	*/
//		document.form1.s.value = document.form2.text.value;
//		document.form1.pageNo.value = 1;
//		document.form1.submit();
//		return false;
//	//}
//}

function goSearch(obj) {
	document.form1.s.value = document.form2.text.value;
	<? if($lhomework->exportAllowed) {?>
	document.form1.form1ExportDate.value = document.form2.exportDate.value;
	<?}?>
	document.form1.pageNo.value = 1;
	document.form1.submit();
	return false;
}

function changeFormAction(obj,element,url,type) {
	if(countChecked(obj,element)==0)
		alert(globalAlertMsg2);
	else
	{
		if (type=='redeem')
		{
			redeemCheck();
			return;
		}
		document.form1.action = url;
		document.form1.submit();
	}
}

function changeClickID(id) {
	document.form1.clickID.value = id;
	hideAllOtherLayer();	
}

function checkCR(evt) {
	var evt  = (evt) ? evt : ((event) ? event : null);
	var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
	if ((evt.keyCode == 13) && (node.type=="text")) {return false;}	
}

$(document).ready( function() {
	$('input#text').keydown( function(evt) {
		if (Check_Pressed_Enter(evt)) {
			// pressed enter
			goSearch();
		}
	});
});

//document.onkeypress = checkCR;
//-->
</script>

<script language="javascript">
<!--
var xmlHttp
var jsDetention = [];
var jsWaive = [];
var jsHistory = [];
var jsRemark = [];
<?php if($sys_custom['eDiscipline']['CSCProbation']) { ?>
    var jsRehabil = [];
<?php } ?>	
var jsConduct = [];
<?php if($sys_custom['eDiscipline']['APFollowUpRemarks']){ ?>
    var jsFollowUpRemark = [];
<?php } ?>
	
function showResult(str,flag)
{
	xmlHttp = GetXmlHttpObject()
	if (xmlHttp==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	}

	var url = "";
	url = "get_live.php?flag="+flag;
	url = url + "&RecordID=" + str
	url = url + "&sid=" + Math.random()
	
	if(flag=='detention') {
		xmlHttp.onreadystatechange = stateChanged 
	}
	else if(flag=='waived') {
		xmlHttp.onreadystatechange = stateChanged2 
	}
	else if(flag=='record_remark') {
		xmlHttp.onreadystatechange = stateChanged3 
	}
	else if(flag=='history') {
		xmlHttp.onreadystatechange = stateChanged4 
	}  
	<?php if($sys_custom['eDiscipline']['CSCProbation']) { ?>
        else if(flag=='rehabil') {
            xmlHttp.onreadystatechange = stateChanged5
        }
	<?php } ?>
	else if(flag=='conduct') {
		xmlHttp.onreadystatechange = stateChanged6 
	}
    <?php if($sys_custom['eDiscipline']['APFollowUpRemarks']) { ?>
        else if(flag=='follow_up_remark') {
            xmlHttp.onreadystatechange = stateChanged7
        }
    <?php } ?>
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_detail"+id).style.zIndex = "1";
		document.getElementById("show_detail"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_detail"+id).style.border = "0px solid #A5ACB2";
	} 
}

function stateChanged2() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_waive"+id).style.zIndex = "1";
		document.getElementById("show_waive"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_waive"+id).style.border = "0px solid #A5ACB2";
	} 
}

function stateChanged3() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_remark"+id).style.zIndex = "1";
		document.getElementById("show_remark"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_remark"+id).style.border = "0px solid #A5ACB2";
	} 
}

function stateChanged4() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_history"+id).style.zIndex = "1";
		document.getElementById("show_history"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_history"+id).style.border = "0px solid #A5ACB2";
	} 
}

<?php if($sys_custom['eDiscipline']['CSCProbation']) { ?>
    function stateChanged5()
    {
        if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
        {
            var id = document.form1.clickID.value;
            document.getElementById("show_detail"+id).style.zIndex = "1";
            document.getElementById("show_detail"+id).innerHTML = xmlHttp.responseText;
            document.getElementById("show_detail"+id).style.border = "0px solid #A5ACB2";
        }
    }
<?php } ?>

function stateChanged6()
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_conduct_"+id).style.zIndex = "1";
		document.getElementById("show_conduct_"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_conduct_"+id).style.border = "0px solid #A5ACB2";
	}
}

<?php if($sys_custom['eDiscipline']['APFollowUpRemarks']) { ?>
    function stateChanged7()
    {
        if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
        {
            var id = document.form1.clickID.value;
            document.getElementById("show_follow_up_remark"+id).style.zIndex = "1";
            document.getElementById("show_follow_up_remark"+id).innerHTML = xmlHttp.responseText;
            document.getElementById("show_follow_up_remark"+id).style.border = "0px solid #A5ACB2";
        }
    }
<?php } ?>

function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

function hideAllOtherLayer() {
	var layer = "";
	
	if(jsDetention.length != 0) {
		for(i=0;i<jsDetention.length;i++) {
			layer = "show_detail" + jsDetention[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	if(jsWaive.length != 0) {
		for(i=0;i<jsWaive.length;i++) {
			layer = "show_waive" + jsWaive[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	if(jsRemark.length != 0) {
		for(i=0;i<jsRemark.length;i++) {
			layer = "show_remark" + jsRemark[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	if(jsHistory.length != 0) {
		for(i=0;i<jsHistory.length;i++) {
			layer = "show_history" + jsHistory[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	<?php if($sys_custom['eDiscipline']['CSCProbation']) { ?>
        if(jsRehabil.length != 0) {
            for(i=0;i<jsRehabil.length;i++) {
                layer = "show_detailr" + jsRehabil[i];
                MM_showHideLayers(layer,'','hide');
            }
        }
	<?php } ?>
	if(jsConduct.length != 0) {
		for(i=0;i<jsConduct.length;i++) {
			layer = "show_conduct_" + jsConduct[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
    <?php if($sys_custom['eDiscipline']['APFollowUpRemarks']) { ?>
        if(jsFollowUpRemark.length != 0) {
            for(i=0;i<jsFollowUpRemark.length;i++) {
                layer = "show_follow_up_remark" + jsFollowUpRemark[i];
                MM_showHideLayers(layer,'','hide');
            }
        }
    <?php } ?>
}

/*********************************/
function getObject( obj ) {

	if ( document.getElementById ) {
		obj = document.getElementById( obj );
	}
	else if ( document.all ) {
		obj = document.all.item( obj );
	}
	else {
		obj = null;
	}
	
	return obj;
}

function moveObject( obj, e, moveX ) {
	var tempX = 0;
	var tempY = 0;
	var offset = 5;
	var objHolder = obj;
	
	obj = getObject( obj );
	if (obj==null) {return;}
	
	if (document.all) {
		tempX = event.clientX + document.documentElement.scrollLeft;
		tempY = event.clientY + document.documentElement.scrollTop;
	}
	else {
		tempX = e.pageX;
		tempY = e.pageY;
	}
	if(moveX != undefined)
		tempX = tempX - moveX
	else 
		if (tempX < 0){tempX = 0} else {tempX = tempX - 320}
	if (tempY < 0){tempY = 0} else {tempY = tempY}
	obj.style.top  = (tempY + offset) + 'px';
	obj.style.left = (tempX + offset) + 'px';
	
	displayObject( objHolder, true );
}

function displayObject( obj, show ) {
	obj = getObject( obj );
	if (obj==null) return;
	
	obj.style.display = show ? 'block' : 'none';
	obj.style.visibility = show ? 'visible' : 'hidden';
}
//-->
</script>

<script language="javascript">
<!--
<?php if(!$sys_custom['eDiscipline']['HideRedeem']){ ?>
// Start of AJAX 
var callback = {
    success: function ( o )
    {
		if (o.responseText != "")
		{
			alert(o.responseText);
			return false;
		}
		else
		{
			document.form1.action = 'redeem_step1.php';
			document.form1.submit();
		}
	}
}

// Main
function redeemCheck()
{
	obj = document.form1;
    YAHOO.util.Connect.setForm(obj);
    
    var path = "redeem_check_aj.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
    
}
// End of AJAX
<?php } ?>

<?php if($sys_custom['eDiscipline']['MoPuiChingRedirToPersonalReport']){ ?>
function redirectToReport(TargetClassName, TargetUserID)
{
	var TargetSchoolYear = $('#SchoolYear').val();
	var TargetSemester = $('#semester').val();
	
	var parms = '?targetClass='+TargetClassName+'&targetID='+TargetUserID+'&startdate=&enddate=&detailType=4&show_waive_status=&ClickID=&RecordID=';
	parms += '&fromMgmt=1&targetSchoolYear='+TargetSchoolYear+'&targetSemester='+TargetSemester;
	
	var url = '/home/eAdmin/StudentMgmt/disciplinev12/reports/master_report/personal_report/personal_check.php'+parms;
	var win = window.open(url, '_blank');
  	win.focus();
}
<?php } ?>
//-->
</script>

<script language="javascript">
<!--
function go_detail(id)
{
	document.form1.id.value = id;
	document.form1.action="detail.php";
	document.form1.submit();
}

$().ready(function (){
	hideAllOtherLayer();
});

function checkboxCheck(flag) {
	document.form1.waitApproval.checked = flag;
	document.form1.approved.checked = flag;
	document.form1.rejected.checked = flag;
	document.form1.released.checked = flag;
	document.form1.unreleased.checked = flag;
	document.form1.waived.checked = flag;
}

function doExport() {
	document.form1.sDate.value = document.form2.startDate.value; 
	document.form1.eDate.value = document.form2.endDate.value; 
	document.form1.targetWebSAMSMode.value = document.form2.webSamsMode.value;
	document.form1.method = "POST";
	
	if(document.form1.export_format.value=="general")
	{
		document.form1.action = "export.php";
	}
	else
	{
		document.form1.action = "exportToWebSAMS.php";
	}
	document.form1.submit();
	document.form1.action = "";
}

function showSpan(layername) {
	if(layername == 'spanExport')
	{
		if(document.form1.export_format.value=="general") {
			$('table#export_mode_table').hide();
		}
		else {
			$('table#export_mode_table').show();
		}
	}
	document.getElementById(layername).style.visibility = "visible";
}

function hideSpan(layername) {
	document.getElementById(layername).style.visibility = "hidden";
}
//-->
</script>

<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr> 
		<td align="center">
			<form name="form2" method="post" action="" onSubmit="return goSearch('document.form2')">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<table border="0" cellspacing="0" cellpadding="2" width="750">
							<tr><td>
								    <div class="Conntent_tool">
										<?= $toolbar ?><?=$exportOption?>
										<div class='print_option_layer' id='spanExport' style="width:250px; left:300px">
				                        	<em > - <?=$Lang['General']['ExportOptions']?> -</em>
				                          	<table id='export_period_table' class='form_table'>
			                            		<col class='field_title' />
			                             		<col class='field_c' />
			                             		<tr>
			                             			<td colspan='3'><em > - <?=$Lang['eDiscipline']['Period']?> -</em></td>
			                             		</tr>
			                            		<tr>
				                             		<td width='22%'><?=$i_From?></td>
				                              		<td>:</td>
				                              		<td><?=$linterface->GET_DATE_PICKER("startDate",$startDate)?></td>
				                            	</tr>
				                            	<tr>
				                              		<td><?=$i_To?></td>
				                              		<td>:</td>
				                              		<td><?=$linterface->GET_DATE_PICKER("endDate",$endDate)?></td>
				                            	</tr>
				                          	</table>
				                          	<table id='export_mode_table' class='form_table'>
			                            		<col class='field_title' />
			                             		<col class='field_c' />
			                             		<tr>
			                             			<td><em > - <?=$Lang['eDiscipline']['WebSAMSSystemMode']?> -</em></td>
			                             		</tr>
			                             		<tr>
			                             			<td>
                                						<input type="radio" name="webSamsMode" id="webSamsMode" value="" checked>
                                						<label for="webSamsMode"><?=$Lang['eDiscipline']['WebSAMSSystemMode_All']?></label>
                                						</br>
                                						<input type="radio" name="webSamsMode" id="webSamsMode_Conduct" value="Conduct">
                                						<label for="webSamsMode_Conduct"><?=$Lang['eDiscipline']['WebSAMSSystemMode_Conduct']?></label>
                                						<br/> 
                                						<input type="radio" name="webSamsMode" id="webSamsMode_Merit" value="Merit">
                                						<label for="webSamsMode_Merit"><?=$Lang['eDiscipline']['WebSAMSSystemMode_Merit']?></label>
                                					</td>
			                             		</tr>
				                          	</table>
				                          	<div class='edit_bottom'>
				                            	<p class='spacer'></p>
				                            	<input type="button" name="button1" id="button1" value="<?=$button_submit?>" onClick="doExport()">
				                            	<input type="button" name="button2" id="button2" value="<?=$button_cancel?>" onClick="hideSpan('spanExport')">
				                            	<p class='spacer'></p>
				                          	</div>
				                        </div>
				            	    </div>
							</td></tr>
						</table>
					</td>
					<td align="right">
						<?=$searchTag?>
					</td>
				</tr>
			</table>
			</form>
			<form name="form1" method="post" action="">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td height="28" align="right" valign="bottom" >
									<table width="100%" border="0" cellspacing="0" cellpadding="5">
										<tr>
											<td height="30">
												<?=$selectSchoolYear?>
												<?=$SemesterMenu?>
												<?=$select_class?>
												<?=$picSelection?>
												<?=$ItemTagSelection?>
											</td>
											<td align="right" valign="middle" class="thumb_list"><?=$recordType?></td>
										</tr>
										<tr>
											<td colspan="2" align="center">
												<?= $ldiscipline->showWarningMsg($i_Discipline_System_Conduct_Instruction, $i_Discipline_System_Award_Punishment_Warning) ?>
											</td>
										</tr>
									</table>

									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="right"><?= $linterface->GET_SYS_MSG($msg) ?>
											</td>
										</tr>
									</table>

									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr class="table-action-bar">
											<td><div class="selectbox_group"> <a href="javascript:;" onClick="MM_showHideLayers('status_option','','show')"><?=$i_Discipline_System_Award_Punishment_Select_Status?></a> </div>
												<br style="clear:both">
												<div id="status_option" class="selectbox_layer">
													<table width="" border="0" cellspacing="0" cellpadding="3">
														<tr>
															<td><input type="checkbox" name="waitApproval" id="waitApproval" value="1" <?=$waitApprovalChecked?>>
																<label for="waitApproval"><?=$i_Discipline_System_Award_Punishment_Pending?></label><br>
																<input type="checkbox" name="approved" id="approved" value="1" <?=$approvedChecked?>>
																<label for="approved"><?=$i_Discipline_System_Award_Punishment_Approved?></label><br>
																<input type="checkbox" name="rejected" id="rejected" value="1" <?=$rejectedChecked?>>
																<label for="rejected"><?=$i_Discipline_System_Award_Punishment_Rejected?></label>
															</td>
														</tr>
														<tr>
															<td align="left" valign="top" class="dotline" height="1"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" height="1"></td>
														</tr>
														<tr>
															<td><input type="checkbox" name="released" id="released" value="1" <?=$releasedChecked?>>
																<label for="released"><?=$i_Discipline_System_Award_Punishment_Released?></label><br>
																<input type="checkbox" name="unreleased" id="unreleased" value="0" <?=$unreleasedChecked?>>
																<label for="unreleased"><?=$i_Discipline_System_Award_Punishment_UnReleased?></label></td>
														</tr>
														<tr>
															<td align="left" valign="top" class="dotline" height="1"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" height="1"></td>
														</tr>
														<tr>
															<td><input type="checkbox" name="waived" id="waived" value="1" <?=$waivedChecked?>>
																<label for="waived"><?=$i_Discipline_System_Award_Punishment_Waived?></label></td>
														</tr>
<!--
														<tr>
															<td align="left" valign="top" class="dotline" height="1"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" height="1"></td>
														</tr>
														<tr>
															<td><input type="checkbox" name="locked"  id="locked" value="1" <?=$lockedChecked?>>
																<label for="locked"><?=$i_Discipline_System_Award_Punishment_Locked?></label> </td>
														</tr>
//-->
														<tr>
															<td align="left" valign="top" class="dotline" height="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" height="2"></td>
														</tr>
														<tr>
															<td align="center" class="tabletext">
																<?= $linterface->GET_BTN($button_view, "submit", "javascript:reloadForm($MeritType)")?>
																<?= $linterface->GET_BTN($button_cancel, "button", "javascript: onClick=MM_showHideLayers('status_option','','hide');form.reset();")?>
																<?= $linterface->GET_BTN($Lang['eDiscipline']['SelectAll'], "button", "javascript:checkboxCheck(true)")?>
																<?= $linterface->GET_BTN($Lang['eDiscipline']['UnselectAll'], "button", "javascript:checkboxCheck(false)")?>
															</td>
														</tr>
													</table>
												</div>
											</td>
											<td align="right" valign="bottom">
                                                <? if($hasApprovalRight || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Approval") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Release") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Waive") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditOwn") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditAll") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-DeleteOwn") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-DeleteAll")) { ?>
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
														<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
															<table border="0" cellspacing="0" cellpadding="1">
																<tr>
                                                                    <? if($hasApprovalRight || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Approval") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Release") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Waive")) { ?>
																	<td nowrap><a href="javascript:;" onClick="MM_showHideLayers('status_list','','show')" class="tabletool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_update.gif" width="12" height="12" border="0" align="absmiddle"> <?=$i_Discipline_System_Award_Punishment_Change_Status?></a>
																		<div id="status_list" style="position:absolute;  width:105px; z-index:1; visibility: hidden;">
																			<table width="100%" border="0" cellpadding="0" cellspacing="0" >
																				<tr>
																					<td height="19">
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td width="5" height="19"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_01.gif" width="5" height="19"></td>
																								<td height="19" valign="middle" background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_02.gif">&nbsp;</td>
																								<td width="19" height="19"><a href="javascript:;" onClick="MM_showHideLayers('status_list','','hide')"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_close_off.gif" name="pre_close22" width="19" height="19" border="0" id="pre_close22" onMouseOver="MM_swapImage('pre_close22','','<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_close_on.gif',1)" onMouseOut="MM_swapImgRestore()"></a></td>
																							</tr>
																						</table>
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td width="5" background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_04.gif"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_04.gif" width="5" height="19"></td>
																								<td bgcolor="#FFFFF7">
																									<table width="98%" border="0" cellpadding="0" cellspacing="3">
																										<?=$actionLayer?>
																									</table>
																								</td>
																								<td width="6" background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_06.gif"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_06.gif" width="6" height="6"></td>
																							</tr>
																							<tr>
																								<td width="5" height="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_07.gif" width="5" height="6"></td>
																								<td height="6" background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_08.gif"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_08.gif" width="5" height="6"></td>
																								<td width="6" height="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_09.gif" width="6" height="6"></td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																			</table>
																		</div>
																	</td>
																	<? } ?>
																	<? if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditAll") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditOwn")) { ?>
																	<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
																	<td nowrap><a href="javascript:checkEditMultiple(document.form1,'RecordID[]','batch_notice_new.php?field=<?=$li->field?>&order=<?=$li->order?>&pageNo=<?=$li->pageNo?>&numPerPage=<?=$li->page_size?>&MeritType=<?=$MeritType?>&s=<?=$s?>&SchoolYear2=<?=$SchoolYear?>&semester2=<?=$semester?>&targetClass2=<?=$targetClass?>&waived2=<?=$waived?>&approved2=<?=$approved?>&waitApproval2=<?=$waitApproval?>&released2=<?=$released?>&rejected2=<?=$rejected?>&fromPPC=<?=$fromPPC?>&passedActionDueDate=<?=$passedActionDueDate?>')" class="tabletool" title="<?= $Lang['eDiscipline']['AddNotice'] ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_handle.gif" width="12" height="12" border="0" align="absmiddle"> <?= $Lang['eDiscipline']['AddNotice'] ?> </a></td>
																	<? } ?>
																	<? if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditAll") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditOwn")) { ?>
																	<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
																	<td nowrap><a href="javascript:checkEdit(document.form1,'RecordID[]','edit.php?field=<?=$li->field?>&order=<?=$li->order?>&pageNo=<?=$li->pageNo?>&numPerPage=<?=$li->page_size?>&MeritType=<?=$MeritType?>&s=<?=$s?>&SchoolYear2=<?=$SchoolYear?>&semester2=<?=$semester?>&targetClass2=<?=$targetClass?>&waived2=<?=$waived?>&approved2=<?=$approved?>&waitApproval2=<?=$waitApproval?>&released2=<?=$released?>&rejected2=<?=$rejected?>&fromPPC=<?=$fromPPC?>&passedActionDueDate=<?=$passedActionDueDate?>')" class="tabletool" title="<?= $button_edit ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_edit ?></a></td>
																	<? } ?>
																	<? if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-DeleteAll") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-DeleteOwn")) { ?>
																	<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
																	<td nowrap><a href="javascript:removeCat(document.form1,'RecordID[]','remove_update.php')" class="tabletool" title="<?= $button_delete ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle"> <?= $button_delete ?></a></td>
																	<? } ?>
																</tr>
															</table>
														</td>
														<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
													</tr>
												</table>
                                                <? } ?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><?=$li->displayFormat_Split_PIC("Award_Punishment",$PICLocation,$StatusLocation,$checkboxLocation,$actionLocation,$recordIDLocation,"{$image_path}/{$LAYOUT_SKIN}",$historyLocation, $caseIDLocation, $waivedByLocation, $noticeIDLocation, "", $categoryItemLocation, $remarkLocation, "", $MeritCountLocation, $conductMarkLocation, "", $pushMsgLocation, $RehabilLocation, $nameLocation, "", $SubScoreLocation, $SubScore2Location, $followUpRemarkLocation);?>
					<div id="div_form"></div>		
					</td>
				</tr>
			</table><br>
			<div width="100%" align="left"><?=$Lang['SysMgr']['FormClassMapping']['DeletedUserLegend']?></div>
			
			<input type="hidden" name="pageNo" id="pageNo" value="<?php echo $li->pageNo; ?>"/>
			<input type="hidden" name="order" id="order" value="<?php echo $li->order; ?>"/>
			<input type="hidden" name="field" id="field" value="<?php echo $li->field; ?>"/>
			<input type="hidden" name="page_size_change" id="page_size_change" value=""/>
			<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>"/>
			<input type="hidden" name="MeritType" id="MeritType" value="<?=$MeritType ?>">
			<input type="hidden" name="s" id="s" value="<?=stripslashes(stripslashes($s))?>"/>
			<input type="hidden" name="clickID" id="clickID" value=""/>
			<input type="hidden" name="id" id="id" value=""/>
			<input type="hidden" name="fromPPC" id="fromPPC" value="<?=$fromPPC?>"/>
			<input type="hidden" name="passedActionDueDate" id="passedActionDueDate" value="<?=$passedActionDueDate?>"/>
			
			<input type="hidden" name="SchoolYear2" id="SchoolYear2" value="<?=$SchoolYear?>"/>
			<input type="hidden" name="semester2" id="semester2" value="<?=$semester?>"/>
			<input type="hidden" name="targetClass2" id="targetClass2" value="<?=$targetClass?>"/>
			<input type="hidden" name="waived2" id="waived2" value="<?=$waived?>"/>
			<input type="hidden" name="approved2" id="approved2" value="<?=$approved?>"/>
			<input type="hidden" name="waitApproval2" id="waitApproval2" value="<?=$waitApproval?>"/>
			<input type="hidden" name="released2" id="released2" value="<?=$released?>"/>
			<input type="hidden" name="rejected2" id="rejected2" value="<?=$rejected?>"/>
			<input type="hidden" name="MeritType2" id="MeritType2" value="<?=$MeritType ?>">
			
			<input type="hidden" name="back_page" id="back_page" value="index.php"/>
			<input type="hidden" name="flag" id="flag" />
			<input type="hidden" name="targetDivID" id="targetDivID" />
			<input type="hidden" name="ajaxRecordID" id="ajaxRecordID" />
			<input type="hidden" name="sDate" id="sDate" value="">
			<input type="hidden" name="eDate" id="eDate" value="">
			<input type="hidden" name="targetWebSAMSMode" id="targetWebSAMSMode" value="">
			<?
// 				if($sys_custom['eDis_AP_export_WEBSAMS_format'] && count($sqlResultSet)>0) {
					echo '<input type="hidden" name="RecordIdForExportWebSAMS" id="RecordIdForExportWebSAMS" value="'.(implode(',',$sqlResultSet)).'">';	
// 				}
			?>
			<input type="hidden" name="export_format" id="export_format" value="">
			</form>
		</td>
	</tr>
</table>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>