<?php
// Editing by 
/*
 * 2015-06-17 (Carlos): Added $RecordID to be excluded for edit case in $ldiscipline->GetMeritItemTagConductOverflowScoreStudents()
 * 2013-10-10 (Carlos): Improved to classify conduct mark overflow type (Semester or ConductMarkLimit) warning msg
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$conductMarkWarningLimit = $ldiscipline->ConductMarkWarningLimit;

$RecordDate = $_REQUEST['RecordDate'];
$StudentID = $_REQUEST['StudentID'];
$ItemID = $_REQUEST['ItemID'];
$RecordType = $_REQUEST['RecordType'];
$ConductScore = $_REQUEST['ConductScore']; 
$RecordID = $_REQUEST['RecordID'];

$semInfo = getAcademicYearAndYearTermByDate($RecordDate);
$AcademicYearID = $semInfo['AcademicYearID'];
$YearTermID = $semInfo['YearTermID'];

$resultAry = $ldiscipline->GetMeritItemTagConductOverflowScoreStudents($StudentID, $AcademicYearID, $YearTermID, $ItemID, $RecordType, $ConductScore, true, $RecordID);

if(count($resultAry['MeritScoreOverflowStudentID']) > 0) {
	if($RecordType==-1){
		$title = $Lang['eDiscipline']['WarningMsgArr']['StudentExceedDemeritItemMaxDeductConductScore'];
	}else{
		$title = $Lang['eDiscipline']['WarningMsgArr']['StudentExceedMeritItemMaxGainConductScore'];
	}
	$contentAry = array();
	for($i=0;$i<count($resultAry['MeritScoreOverflowStudentID']);$i++) {
		$contentAry[] = $resultAry['StudentDetail'][$resultAry['MeritScoreOverflowStudentID'][$i]];
	}
	$x .= $linterface->Get_Warning_Message_Box($title, $contentAry);
}

if(count($resultAry['ConductScoreOverflowStudentID']) > 0) {
	if($RecordType==-1){
		$title = $Lang['eDiscipline']['WarningMsgArr']['StudentExceedSemesterMinConductMark'];
	}else{
		$title = $Lang['eDiscipline']['WarningMsgArr']['StudentExceedSemesterMaxConductMark'];
	}
	$contentAry = array(); // Store students that exceed semester conduct mark limit
	$contentAry2 = array(); // Store students that exceed conduct mark limit
	for($i=0;$i<count($resultAry['ConductScoreOverflowStudentID']);$i++) {
		if(isset($resultAry['ConductScoreOverflowType'][$resultAry['ConductScoreOverflowStudentID'][$i]])){
			if(in_array('Semester',$resultAry['ConductScoreOverflowType'][$resultAry['ConductScoreOverflowStudentID'][$i]])){
				$contentAry[] = $resultAry['StudentDetail'][$resultAry['ConductScoreOverflowStudentID'][$i]];
			}
			if(in_array('ConductMarkLimit',$resultAry['ConductScoreOverflowType'][$resultAry['ConductScoreOverflowStudentID'][$i]])){
				$contentAry2[] = $resultAry['StudentDetail'][$resultAry['ConductScoreOverflowStudentID'][$i]];
			}
		}
	}
	if(count($contentAry)>0) {
		$x .= $linterface->Get_Warning_Message_Box($title, $contentAry);
	}
	
	if($RecordType == -1 && count($contentAry2)>0) {
		$title = str_replace("<!--WARN_MARK-->",$conductMarkWarningLimit,$Lang['eDiscipline']['WarningMsgArr']['StudentLowerThanConductMarkWarningLimit']);
		$x .= $linterface->Get_Warning_Message_Box($title, $contentAry2);
	}
}

if(count($resultAry['TagScoreOverflowStudentID']) > 0) {
	$tagAry = array();
	
	foreach($resultAry['TagScoreOverflowDetail'] as $student_id => $tag_ary){
		foreach($tag_ary as $tag_id => $tag_detail) {
			$tagAry[$tag_id] = $tag_detail['TagName'];
		}
	}
	
	foreach($tagAry as $tag_id => $tag_name) {
		if($RecordType == -1) {
			$title = str_replace("<!--TAG_NAME-->",$tag_name,$Lang['eDiscipline']['WarningMsgArr']['StudentExceedTagMaxDeductConductScore']);
		}else{
			$title = str_replace("<!--TAG_NAME-->",$tag_name,$Lang['eDiscipline']['WarningMsgArr']['StudentExceedTagMaxGainConductScore']);
		}
		$contentAry = array();
		for($i=0;$i<count($resultAry['TagScoreOverflowStudentID']);$i++) {
			if(isset($resultAry['TagScoreOverflowDetail'][$resultAry['TagScoreOverflowStudentID'][$i]][$tag_id])){
				$contentAry[] = $resultAry['StudentDetail'][$resultAry['TagScoreOverflowStudentID'][$i]];
			}
		}
		$x .= $linterface->Get_Warning_Message_Box($title, $contentAry);
	}
}

if(count($resultAry['MeritScoreOverflowStudentID']) > 0 || count($resultAry['ConductScoreOverflowStudentID']) > 0 || count($resultAry['TagScoreOverflowStudentID']) > 0) {
	$x .= '<input type="hidden" id="OverflowConductMarkFlag" name="OverflowConductMarkFlag" value="1" />';
	
	if($ldiscipline->ConductMarkExceedLimitRemark != ""){
		$x = '<div>'.$ldiscipline->ConductMarkExceedLimitRemark.'</div>'.$x;
	}
}

echo $x;

intranet_closedb();
?>