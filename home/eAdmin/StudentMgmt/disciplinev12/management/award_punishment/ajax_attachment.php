<?php
// modify by : 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lu = new libfilesystem();
/*
if(!($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Goodconduct_Misconduct-EditAll") || (($ldiscipline->isCasePIC($RecordID) || $ldiscipline->isCaseOwn($RecordID)) && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditOwn"))))
{
		$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}
*/

if($task=="remove") {
	$name = substr($fieldname,0,-2);
	$Attachment = ${$name};
	
	if(sizeof($Attachment)>0)
		$Attachment = array_unique($Attachment);
			
	$path = "$file_path/file/disciplinev12/award_punishment/".$FolderLocation."/";
	//echo $path;
	for($a=0;$a<sizeof($Attachment);$a++)
	{
		$file = $path.$Attachment[$a];
		$file = stripslashes($file);
		$lu->lfs_remove($file);
	}
	$attachment_list = $ldiscipline->getAttachmentArray($path, '');
	
	if(sizeof($attachment_list)==0) {
		$ldiscipline->deleteDirectory($path);	
	}

}


intranet_closedb();
?>


