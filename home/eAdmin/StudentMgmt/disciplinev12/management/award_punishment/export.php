<?php
# using: 

############ Change Log Start #############################
#
#   Date    :   2020-11-06  (Bill)  [2020-0824-1151-40308]
#               Support Follow-up Feedback    ($sys_custom['eDiscipline']['APFollowUpRemarks'])
#
#	Date	:	2017-06-13 (Bill)	[2016-1207-1221-39240]
#				Support Activity Score Export
#
#	Date	:	2017-03-15 (Anna)	[2017-0309-1027-13206]
#				added user login column ($sys_custom['eDiscipline']['ExportRecordWithUserLogin'])			
#
#	Date	:	2016-04-18 (Bill)	[DM#2973]
#				allow export name of deleted teacher account
#
#	Date	:	2016-04-11 (Bill)
#				Replace deprecated split() by explode() for PHP 5.4
#
#	Date	:	2015-12-30 (Bill)	[2015-0416-1040-06164]
#				only export self created records for Student Prefect - HKUGA Cust
#
#	Date	:	2013-11-21 (YatWoon)
#	Detail	: 	Customization: Add 2 extra fields: FileNumber, RecordNumber, $sys_custom['eDiscipline']['MST']['AP_Extra_Fields']	[Case#2013-0520-1008-41072]
#
#	Date	:	2013-06-21 (Carlos)
#			:	$sys_custom['eDiscipline']['yy3'] - Display conduct score change with masked symbol for those overflow score merit items 
#
#	Date	:	2013-06-05 (YatWoon)
#				export all status date for client ($special_feature['eDiscipline']['APExportAllDate']) [Case#2013-0527-1516-55132]
#
#	Date	:	2013-05-16 (YatWoon)
#	Datail	:	add RecordInputDate (customization for yy3 - $sys_custom['eDiscipline']['yy3'])
#
#	Date	:	2012-06-28 Henry Chow
#				split the record array if it is too large (more than 20000 records) [CRM : 2012-0625-1601-02132]
# 
#	Date	:	2010-09-24 Henry Chow
#				handle "Form" in targetClass
#
############ Change Log End #############################

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# Temp Assign memory of this page
ini_set("memory_limit", "800M"); 

$ldiscipline = new libdisciplinev12();
$lexport = new libexporttext();

$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Award_Punishment-View");

if($sys_custom['eDiscipline']['yy3']){
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");
	$ldiscipline_ui = new libdisciplinev12_ui_cust();
}

$ArrayUpperLimit = 20000;

$currentYearInfo = Get_Current_Academic_Year_ID();
$SchoolYear = ($SchoolYear == '') ? $currentYearInfo : $SchoolYear;

$ExportArr = array();

$conds = "";
if ($targetClass != '' && $targetClass!="0")
{
    //$conds .= " AND g.YearClassID=$targetClass";
    if(is_numeric($targetClass)) {
    	$sql = "SELECT YearClassID FROM YEAR_CLASS WHERE YearID='$targetClass'";
    	$temp = $ldiscipline->returnVector($sql);
    	$conds .= (sizeof($temp)>0) ? " AND g.YearClassID IN (".implode(',', $temp).")" : ""; 
	}
    else {
    	$conds .= " AND g.YearClassID=".substr($targetClass,2);
    }
}

if ($MeritType != '' && $MeritType != 0)
{
    $conds .= " AND a.MeritType = $MeritType";
}
else {
	$conds .= " AND (a.MeritType = 1 or a.MeritType=-1 or a.MeritType=0)";
	$MeritType = 0;	
}

if($SchoolYear != '' && $SchoolYear != 0) {
	$conds .= " AND a.AcademicYearID=$SchoolYear";	
	//$conds .= " AND g.AcademicYearID = $SchoolYear";
}

if($semester != '' && $semester != 'WholeYear') {
	//$conds .= " AND a.Semester = '$semester'";	
	$conds .= " AND a.YearTermID = $semester";
}

if($pic != "") 
	$conds .= " AND concat(',',a.PICID,',') LIKE '%,$pic,%'";

if($sDate!="" && $eDate!="")
	$conds .= " AND a.RecordDate BETWEEN '$sDate' AND '$eDate'";

// [2015-0416-1040-06164] Student Prefect can export self-created records only
if($sys_custom['eDiscipline']['add_AP_records_student_prefect'] && $ldiscipline->isStudentPrefect()){
	$conds .= " AND a.CreatedBy = '$UserID'";
}

$conds2 = "";

# Waiting for Approval #
if($waitApproval == 1) {
	$waitApprovalChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.RecordStatus = ".DISCIPLINE_STATUS_PENDING;	
}
# unreleased #
if($unreleased == '0' || (!isset($released) && !isset($unreleased) && !isset($fromPPC) && !isset($fromOverview))) {
	$unreleasedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "((a.RecordStatus=".DISCIPLINE_STATUS_APPROVED." OR a.RecordStatus=".DISCIPLINE_STATUS_WAIVED.") AND (a.ReleaseStatus=".DISCIPLINE_STATUS_UNRELEASED." OR a.ReleaseStatus IS NULL OR a.ReleaseStatus=NULL))";
}
# Approved #  // released record also be approved
if($approved == 1 || (!isset($released) && !isset($unreleased) && !isset($fromPPC) && !isset($fromOverview))) {
	if($released == DISCIPLINE_STATUS_UNRELEASED) {
		$conds2 .= ($conds2=="") ? "(" : " OR ";
		$conds2 .= "(a.RecordStatus = ".DISCIPLINE_STATUS_APPROVED." AND (a.ReleaseStatus = ".DISCIPLINE_STATUS_UNRELEASED." OR a.ReleaseStatus IS NULL OR a.ReleaseStatus=NULL))";
	}
	else if($released == DISCIPLINE_STATUS_RELEASED) {
		$conds2 .= ($conds2=="") ? "(" : " OR ";
		$conds2 .= "(a.RecordStatus = ".DISCIPLINE_STATUS_APPROVED." AND (a.ReleaseStatus = ".DISCIPLINE_STATUS_UNRELEASED." OR a.ReleaseStatus IS NULL OR a.ReleaseStatus=NULL))";
	}
	else {
		$conds2 .= ($conds2=="") ? "(" : " OR ";
		$conds2 .= "(a.RecordStatus = ".DISCIPLINE_STATUS_APPROVED.")";
	}
}

# Rejected #
//if($rejected == 1) {
if($rejected == 1 || (!isset($released) && !isset($unreleased) && !isset($fromPPC) && !isset($fromOverview))) {
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "(a.RecordStatus = ".DISCIPLINE_STATUS_REJECTED.")";	
}

# Released #
//if($released == 1) {
if($released == 1 || (!isset($released) && !isset($unreleased) && !isset($fromPPC) && !isset($fromOverview))) {
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.ReleaseStatus = ".DISCIPLINE_STATUS_RELEASED;	
}

# Waived #
//if($waived == 1) {
if($waived == 1 || (!isset($released) && !isset($unreleased) && !isset($fromPPC) && !isset($fromOverview))) {
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "(a.RecordStatus = ".DISCIPLINE_STATUS_WAIVED.")";	
}

# Locked #
/*
if($locked == 1) {
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.LockStatus = ".DISCIPLINE_STATUS_LOCK;	
}
*/
$conds2 .= ($conds2 != "") ? ")" : "";

$filename = "award_punishment.csv";	

# SQL Statement
$student_namefield = getNamefieldByLang("b.");
$clsName = ($intranet_session_language=="en") ? "g.ClassTitleEN" : "g.ClassTitleB5";
$history = $eDiscipline["HISTORY"];

$conductScoreChangeField = "a.ConductScoreChange,";
if($sys_custom['eDiscipline']['yy3']) {
	$conductScoreChangeField = "IF((a.OverflowConductMark='1' OR a.OverflowMeritItemScore='1' OR a.OverflowTagScore='1') AND a.ConductScoreChange=0,'".$ldiscipline_ui->displayMaskedMark('0')."',a.ConductScoreChange) as ConductScoreChange,";
}
$activityScoreChangeField = ",a.SubScore2Change";

if($sys_custom['eDiscipline']['MST']['AP_Extra_Fields'])
{
	$cond_mst = ", a.FileNumber, a.RecordNumber";	
}
if($sys_custom['eDiscipline']['ExportRecordWithUserLogin']){
	$cond_userlogin = ",b.UserLogin";
}

// [2020-0824-1151-40308]
if($sys_custom['eDiscipline']['APFollowUpRemarks']) {
    $followUpRemarkField = ", a.FollowUpRemark";
}

if($fromPPC==1 || $passedActionDueDate==1) {
	$result = ($fromPPC==1) ? $ldiscipline->retrieveMeritRecordFromPPC() : $ldiscipline->getPassedActionDueDateAPRecord($SchoolYear);
	$MeritRecordID = implode(',', $result);
	
	$sql = "SELECT $clsName, 
                $student_namefield as EnglishName, 
                IF(a.MeritType=1,'$i_Merit_Award','$i_Merit_Punishment') as type,
                CONCAT(a.ProfileMeritCount,'::',a.ProfileMeritType) as record,
                if(a.ItemID=0, '--',CONCAT(c.ItemCode,' - ',REPLACE(c.ItemName,'\"','&quot;'))) as item,
                a.RecordDate as recordDate,
                PICID, 
                CONCAT(
                    IF(a.fromConductRecords=1,'$history',''), 
                    IF(a.fromConductRecords=1 && (a.CaseID!=0 && a.CaseID!=''),',',''),
                    IF(a.CaseID!=0 && a.CaseID!='','$i_Discipline_System_Discipline_Case_Record_Case','')
                ) as reference,
                LEFT(a.DateModified,10) as modifiedDate,
                CONCAT(
                    IF(a.ReleaseStatus=1,'$i_Discipline_System_Award_Punishment_Released,',''),
                    CASE(a.RecordStatus)
                        WHEN 0 THEN '$i_Discipline_System_Award_Punishment_Pending'
                        WHEN 1 THEN '$i_Discipline_System_Award_Punishment_Approved'
                        WHEN 2 THEN '$i_Discipline_System_Award_Punishment_Waived'
                        WHEN -1 THEN '$i_Discipline_System_Award_Punishment_Rejected'
                        ELSE 'Error' 
                    END
                ) as status,
                $conductScoreChangeField 
                f.ClassNumber,
                a.ActionDueDate,
                a.Remark,
                a.SubjectID,
                a.SubScore1Change,
                a.RecordInputDate,
                if(a.RecordStatus=1, a.ApprovedDate, '') as ApprovedDate,
                a.ReleasedDate,
                if(a.RecordStatus=-1, a.RejectedDate, '') as RejectedDate,
                a.WaivedDate
                $cond_mst
                $cond_userlogin
                $activityScoreChangeField
                $followUpRemarkField
			FROM 
			    DISCIPLINE_MERIT_RECORD as a
				INNER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
				LEFT OUTER JOIN DISCIPLINE_MERIT_ITEM as c ON a.ItemID = c.ItemID
				LEFT OUTER JOIN DISCIPLINE_MERIT_TYPE_SETTING as d ON a.ProfileMeritType = d.MeritType
				LEFT OUTER JOIN INTRANET_USER as e ON a.PICID = e.UserID
				LEFT OUTER JOIN YEAR_CLASS_USER as f ON (b.UserID = f.UserID)
				LEFT OUTER JOIN YEAR_CLASS as g ON (f.YearClassID = g.YearClassID)
			WHERE 
			    a.DateInput IS NOT NULL AND g.AcademicYearID = $currentYearInfo AND  
				a.RecordID IN ($MeritRecordID) AND b.RecordStatus IN (0,1,2)
				$conds
			";
			$sql .= ($conds2 != "") ? " AND ".$conds2 : "";
			$sql .= " GROUP BY a.RecordID ORDER BY a.RecordDate DESC, a.RecordID DESC";
}
else {
	$sql = "SELECT $clsName, 
                $student_namefield as EnglishName, 
                IF(a.MeritType=1,'$i_Merit_Award','$i_Merit_Punishment') as type,
                CONCAT(a.ProfileMeritCount,'::',a.ProfileMeritType) as record,
                CONCAT(c.ItemCode,' - ',REPLACE(c.ItemName,'\"','&quot;')) as item,
                a.RecordDate as recordDate,
                PICID, 
                CONCAT(
                    IF(a.fromConductRecords=1,'$history',''), 
                    IF(a.fromConductRecords=1 && (a.CaseID!=0 && a.CaseID!=''),',',''),
                    IF(a.CaseID!=0 && a.CaseID!='','$i_Discipline_System_Discipline_Case_Record_Case','')
                ) as reference,
                LEFT(a.DateModified,10) as modifiedDate,
                CONCAT(
                    IF(a.ReleaseStatus=1,'$i_Discipline_System_Award_Punishment_Released,',''),
                    CASE(a.RecordStatus)
                        WHEN 0 THEN '$i_Discipline_System_Award_Punishment_Pending'
                        WHEN 1 THEN '$i_Discipline_System_Award_Punishment_Approved'
                        WHEN 2 THEN '$i_Discipline_System_Award_Punishment_Waived'
                        WHEN -1 THEN '$i_Discipline_System_Award_Punishment_Rejected'
                        ELSE 'Error' 
                    END
                ) as status,
                $conductScoreChangeField 
                f.ClassNumber,
                a.ActionDueDate,
                a.Remark,
                a.SubjectID,
                a.SubScore1Change,
                a.RecordInputDate,
                if(a.RecordStatus=1 or a.RecordStatus=2, a.ApprovedDate, '') as ApprovedDate,
                if(a.ReleaseStatus=1, a.ReleasedDate, '') as ReleasedDate,
                if(a.RecordStatus=-1, a.RejectedDate, '') as RejectedDate,
                if(a.RecordStatus=2, a.WaivedDate, '') as WaivedDate
                $cond_mst
                $cond_userlogin
                $activityScoreChangeField
                $followUpRemarkField
			FROM 
			    DISCIPLINE_MERIT_RECORD as a
				INNER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
				LEFT OUTER JOIN DISCIPLINE_MERIT_ITEM as c ON a.ItemID = c.ItemID
				LEFT OUTER JOIN DISCIPLINE_MERIT_TYPE_SETTING as d ON a.ProfileMeritType = d.MeritType
				LEFT OUTER JOIN YEAR_CLASS_USER as f ON (a.StudentID=f.UserID AND f.UserID = a.StudentID)
				LEFT OUTER JOIN YEAR_CLASS as g ON (f.YearClassID = g.YearClassID)
				WHERE a.DateInput IS NOT NULL AND g.AcademicYearID = $currentYearInfo AND b.RecordStatus IN (0,1,2)
					$conds
                    AND (
                            g.ClassTitleEN like '%$s%' 
                            OR g.ClassTitleB5 like '%$s%'
                            OR f.ClassNumber like '%$s%'
                            OR $student_namefield like '%$s%'
                            OR c.ItemCode like '%$s%'
                            OR c.ItemName like '%$s%'
                            OR a.RecordDate like '%$s%'
                    )";
		$sql .= ($conds2 != "") ? " AND ".$conds2 : "";				
		$sql .= " GROUP BY a.RecordID ORDER BY a.RecordDate DESC, a.RecordID DESC";
}

$meritArr = array();
$meritArr['0'] = $i_Merit_Warning;			
$meritArr['1'] = $i_Merit_Merit;			
$meritArr['2'] = $i_Merit_MinorCredit;		
$meritArr['3'] = $i_Merit_MajorCredit;		
$meritArr['4'] = $i_Merit_SuperCredit;		
$meritArr['5'] = $i_Merit_UltraCredit;		
$meritArr['-1'] = $i_Merit_BlackMark;		
$meritArr['-2'] = $i_Merit_MinorDemerit;	
$meritArr['-3'] = $i_Merit_MajorDemerit;	
$meritArr['-4'] = $i_Merit_SuperDemerit;	
$meritArr['-5'] = $i_Merit_UltraDemerit;	

$row = $ldiscipline->returnArray($sql,9);
$noOfRecord = count($row);

# Create data array for export
//$thisMax = $noOfRecord < $ArrayUpperLimit ? $noOfRecord : $ArrayUpperLimit;
$noOfLoop = ceil($noOfRecord / $ArrayUpperLimit);
for($a=0; $a<$noOfLoop; $a++) {
	$thisMin = $a * $ArrayUpperLimit;
	$thisMax = $noOfRecord<$thisMin + $ArrayUpperLimit ? $noOfRecord : $thisMin + $ArrayUpperLimit;
	
	for($i=$thisMin; $i<$thisMax; $i++) {
		$pos = 0;
		$ExportArr[$i][$pos] = $row[$i][0];		$pos++;					// Class Name
		$ExportArr[$i][$pos] = $row[$i][11];	$pos++;					// Class Number
		$ExportArr[$i][$pos] = $row[$i][1]; 	$pos++;					// Student Name
		if($sys_custom['eDiscipline']['ExportRecordWithUserLogin']) {
			$ExportArr[$i][$pos] = $row[$i]['UserLogin']; 	$pos++;		// User Login
		}
		$ExportArr[$i][$pos] = $row[$i][2]; 	$pos++;					// Record Type
		if($ldiscipline->Display_ConductMarkInAP) {
			$ExportArr[$i][$pos] = $row[$i][10]; 	$pos++;				// Conduct Score Change
		}
		if($ldiscipline->UseSubScore) {
			$ExportArr[$i][$pos] = $row[$i][15]; 	$pos++;				// Study Score Change
		}
		if($ldiscipline->UseActScore) {
			$ExportArr[$i][$pos] = $row[$i]['SubScore2Change'];			// Activity Score Change
			$pos++;
		}
	
		// replace split() by explode() - for PHP 5.4
		//$temp = split('::', $row[$i][3]);
		$temp = explode('::', $row[$i][3]);
		list($amount,$type) = $temp;
	
		$record_data = $ldiscipline->returnDeMeritStringWithNumber($amount, $meritArr[$type]);
		$ExportArr[$i][$pos] = $record_data; 	$pos++;
		$ExportArr[$i][$pos] = $row[$i][4]; 	$pos++;					// Reason
		if($ldiscipline->use_subject) {
			$ExportArr[$i][$pos] = $ldiscipline->getSubjectNameBySubjectID($row[$i][14]); 	// Subject
			$pos++;
		}

        // Handle next line in remarks
        $remark = trim($row[$i][13]);
        $remark = str_replace("\t","    ",$remark);
        $remark = str_replace("
","\r\n",$remark);
		$ExportArr[$i][$pos] = $remark;     $pos++;					    // Remark

        // [2020-0824-1151-40308]
        if($sys_custom['eDiscipline']['APFollowUpRemarks'])
        {
            // Handle next line in remarks
            $followUpRemark = trim($row[$i]['FollowUpRemark']);
            $followUpRemark = str_replace("\t","    ",$followUpRemark);
            $followUpRemark = str_replace("
","\r\n",$followUpRemark);
            $ExportArr[$i][$pos] = $followUpRemark;     $pos++;         // Follow-up Remark
        }

		$ExportArr[$i][$pos] = $row[$i][5]; 	$pos++;					// Event Date				
	
		if($sys_custom['eDiscipline']['yy3']) {
			$ExportArr[$i][$pos] = $row[$i][16]; 	$pos++;				// Event Record Date
		}
		$name_field = getNameFieldByLang();								// PIC
		if($row[$i][6] != '')
		{
			$pic = $row[$i][6];
	
			$sql2 = "SELECT $name_field FROM INTRANET_USER WHERE UserID IN ($pic)";
			$temp = $ldiscipline->returnArray($sql2,1);
	
			// [DM#2973] INTRANET_ARCHIVE_USER
			$sql2 = "SELECT CONCAT('*', $name_field) FROM INTRANET_ARCHIVE_USER WHERE UserID IN ($pic)";
			$temp2 = $ldiscipline->returnArray($sql2,1);
			
			$temp = array_merge($temp, $temp2);
			if(sizeof($temp)!=0) {
		    	for($k=0; $k<sizeof($temp); $k++) {
		        	$ExportArr[$i][$pos] .= $temp[$k][0];
		        	$ExportArr[$i][$pos] .= ($k != (sizeof($temp)-1)) ? ", " : "";
		    	}
			}
			else {
				$ExportArr[$i][$pos] .= "---";
			}
		}
		else {
			$ExportArr[$i][$pos] .= "---";	
		}
		$pos++;
	
		$ExportArr[$i][$pos] = $row[$i][7]; 	$pos++; 				// Reference
		$ExportArr[$i][$pos] = $row[$i][8]; 	$pos++; 				// Date Modified
		$ExportArr[$i][$pos] = $row[$i][9]; 	$pos++; 				// Status
		if($sys_custom['wscss_disciplinev12_ap_prompt_action'])
		{
			$ExportArr[$i][$pos] = $row[$i][12]; 						// Action Due Date
			$pos++; 		
		}
		if($special_feature['eDiscipline']['APExportAllDate'])
		{
			$ExportArr[$i][$pos] = $row[$i][17]; $pos++; 				// ApprovedDate
			$ExportArr[$i][$pos] = $row[$i][18]; $pos++; 				// ReleasedDate
			$ExportArr[$i][$pos] = $row[$i][19]; $pos++; 				// RejectedDate
			$ExportArr[$i][$pos] = $row[$i][20]; $pos++; 				// WaivedDate
		}	
		
		if($sys_custom['eDiscipline']['MST']['AP_Extra_Fields'])
		{
			$ExportArr[$i][$pos] = $row[$i]['FileNumber']; $pos++; 		// FileNumber
			$ExportArr[$i][$pos] = $row[$i]['RecordNumber']; $pos++; 	// RecordNumber
		}
	}
}

/*
for($i=0; $i<$thisMax; $i++){
	$pos = 0;
	$ExportArr[$i][$pos] = $row[$i][0];	$pos++;				//Class Name
	$ExportArr[$i][$pos] = $row[$i][11];	$pos++;			//Class Number
	$ExportArr[$i][$pos] = $row[$i][1]; 	$pos++;			//Student Name
	$ExportArr[$i][$pos] = $row[$i][2]; 	$pos++;			//Record Type
	if($ldiscipline->Display_ConductMarkInAP) {
		$ExportArr[$i][$pos] = $row[$i][10]; 	$pos++;		//Conduct Score Change
	}
	if($ldiscipline->UseSubScore) {
		$ExportArr[$i][$pos] = $row[$i][15]; 	$pos++;		//Conduct Score Change
	}

	$temp = split('::', $row[$i][3]);
	list($amount,$type) = $temp;

	$record_data = $ldiscipline->returnDeMeritStringWithNumber($amount, $meritArr[$type]);
	$ExportArr[$i][$pos] = $record_data; 	$pos++;
	$ExportArr[$i][$pos] = $row[$i][4]; 	$pos++;		//Reason
	if($ldiscipline->use_subject) {
		$ExportArr[$i][$pos] = $ldiscipline->getSubjectNameBySubjectID($row[$i][14]); 		//Subject
		$pos++;
	}
	$ExportArr[$i][$pos] = $row[$i][13];	$pos++;		//Remark
	$ExportArr[$i][$pos] = $row[$i][5]; 	$pos++;		//Event Date				

	$name_field = getNameFieldByLang();					//PIC
	if($row[$i][6] != '') {
		$pic = $row[$i][6];

		$sql2 = "SELECT $name_field FROM INTRANET_USER WHERE UserID IN ($pic)";
		$temp = $ldiscipline->returnArray($sql2,1);
		
		if(sizeof($temp)!=0) {
	    	for($k=0; $k<sizeof($temp); $k++) {
	        	$ExportArr[$i][$pos] .= $temp[$k][0];
	        	$ExportArr[$i][$pos] .= ($k != (sizeof($temp)-1)) ? ", " : "";
	    	}
		} else {
			$ExportArr[$i][$pos] .= "---";
		}
	} else {
		$ExportArr[$i][$pos] .= "---";	
	}
	$pos++;

	$ExportArr[$i][$pos] = $row[$i][7]; 	$pos++; 		//Reference
	$ExportArr[$i][$pos] = $row[$i][8]; 	$pos++; 		//Date Modified
	$ExportArr[$i][$pos] = $row[$i][9]; 	$pos++; 		//Status
	if($sys_custom['wscss_disciplinev12_ap_prompt_action'])
		$ExportArr[$i][$pos] = $row[$i][12]; 	$pos++; 		//Action Due Date
}

// if the record is too large, prepare the $ExportArr separately.
if($ArrayUpperLimit < $noOfRecord) {
	for($i=$ArrayUpperLimit; $i<$noOfRecord; $i++){
		$pos = 0;
		$ExportArr[$i][$pos] = $row[$i][0];	$pos++;				//Class Name
		$ExportArr[$i][$pos] = $row[$i][11];	$pos++;			//Class Number
		$ExportArr[$i][$pos] = $row[$i][1]; 	$pos++;			//Student Name
		$ExportArr[$i][$pos] = $row[$i][2]; 	$pos++;			//Record Type
		if($ldiscipline->Display_ConductMarkInAP) {
			$ExportArr[$i][$pos] = $row[$i][10]; 	$pos++;		//Conduct Score Change
		}
		if($ldiscipline->UseSubScore) {
			$ExportArr[$i][$pos] = $row[$i][15]; 	$pos++;		//Conduct Score Change
		}
	
		$temp = split('::', $row[$i][3]);
		list($amount,$type) = $temp;
	
		$record_data = $ldiscipline->returnDeMeritStringWithNumber($amount, $meritArr[$type]);
		$ExportArr[$i][$pos] = $record_data; 	$pos++;
		$ExportArr[$i][$pos] = $row[$i][4]; 	$pos++;		//Reason
		if($ldiscipline->use_subject) {
			$ExportArr[$i][$pos] = $ldiscipline->getSubjectNameBySubjectID($row[$i][14]); 		//Subject
			$pos++;
		}
		$ExportArr[$i][$pos] = $row[$i][13];	$pos++;		//Remark
		$ExportArr[$i][$pos] = $row[$i][5]; 	$pos++;		//Event Date				
	
		$name_field = getNameFieldByLang();					//PIC
		if($row[$i][6] != '') {
			$pic = $row[$i][6];
	
			$sql2 = "SELECT $name_field FROM INTRANET_USER WHERE UserID IN ($pic)";
			$temp = $ldiscipline->returnArray($sql2,1);
			
			if(sizeof($temp)!=0) {
		    	for($k=0; $k<sizeof($temp); $k++) {
		        	$ExportArr[$i][$pos] .= $temp[$k][0];
		        	$ExportArr[$i][$pos] .= ($k != (sizeof($temp)-1)) ? ", " : "";
		    	}
			} else {
				$ExportArr[$i][$pos] .= "---";
			}
		} else {
			$ExportArr[$i][$pos] .= "---";	
		}
		$pos++;
	
		$ExportArr[$i][$pos] = $row[$i][7]; 	$pos++; 		//Reference
		$ExportArr[$i][$pos] = $row[$i][8]; 	$pos++; 		//Date Modified
		$ExportArr[$i][$pos] = $row[$i][9]; 	$pos++; 		//Status
		if($sys_custom['wscss_disciplinev12_ap_prompt_action'])
			$ExportArr[$i][$pos] = $row[$i][12]; 	$pos++; 		//Action Due Date
	}
}
*/

# define column title
$exportColumn[] = $i_ClassName;
$exportColumn[] = $i_ClassNumber;
$exportColumn[] = $i_general_name;
if($sys_custom['eDiscipline']['ExportRecordWithUserLogin']) {
	$exportColumn[] = $Lang['eDiscipline']['UserLogin'];	
}
$exportColumn[] = $i_Merit_Type;
if($ldiscipline->Display_ConductMarkInAP) {
	$exportColumn[] = $eDiscipline['Conduct_Mark'];
}
if($ldiscipline->UseSubScore) {
	$exportColumn[] = $i_Discipline_System_Subscore1;
}
if($ldiscipline->UseActScore) {
	$exportColumn[] = $Lang['eDiscipline']['ActivityScore'];
}
$exportColumn[] = $eDiscipline["Record"];
$exportColumn[] = $i_Discipline_Reason;
if($ldiscipline->use_subject) {
	$exportColumn[] = $i_Homework_subject;
}
$exportColumn[] = $i_UserRemark;
// [2020-0824-1151-40308]
if($sys_custom['eDiscipline']['APFollowUpRemarks']) {
    $exportColumn[] = $Lang['eDiscipline']['FollowUpRemark'];
}
$exportColumn[] = $Lang['eDiscipline']['EventDate'];
if($sys_custom['eDiscipline']['yy3']) {
	$exportColumn[] = $Lang['eDiscipline']['EventRecordDate'];
}
$exportColumn[] = $i_Profile_PersonInCharge;
$exportColumn[] = $i_Discipline_System_Award_Punishment_Reference;
$exportColumn[] = $i_Discipline_Last_Updated;
$exportColumn[] = $i_Discipline_System_Discipline_Status;
if($sys_custom['wscss_disciplinev12_ap_prompt_action']) {
	$exportColumn[] = $Lang['eDiscipline']['AP_ActionDueDate'];
}
if($special_feature['eDiscipline']['APExportAllDate']) {
	$exportColumn[] = $Lang['eDiscipline']['ApprovedDate'];
	$exportColumn[] = $Lang['eDiscipline']['ReleasedDate'];
	$exportColumn[] = $Lang['eDiscipline']['RejectedDate'];
	$exportColumn[] = $Lang['eDiscipline']['WaivedDate'];
}
if($sys_custom['eDiscipline']['MST']['AP_Extra_Fields']) {
	$exportColumn[] = $Lang['eDiscipline']['FileNumber'];
	$exportColumn[] = $Lang['eDiscipline']['RecordNumber'];
}

// Allow exxport data with next line
//$export_content .= $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");
$export_content .= $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11", 1);

intranet_closedb();

# Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);
?>