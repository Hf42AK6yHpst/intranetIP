<?php
# using: 

############ Change Log Start #############################
#
# 	Date	:	2015-10-26 (Bill)	[2015-0611-1642-26164]
# 				copy from reject_update2.php
#
############################################################

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
	
# Check access right
if(!$ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-View") || !$sys_custom['eDiscipline']['CSCProbation'])
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$RecordIDAry = array();
$return_filter = "SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&num_per_page=$num_per_page&pageNo=$pageNo&order=$order&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&MeritType=$MeritType&s=$s&clickID=$clickID&fromPPC=$fromPPC&pic=$pic&passedActionDueDate=$passedActionDueDate";

$RecordIDAry = $RecordID;
if($back_page)
	$returnPath = $back_page."?id=$RecordIDAry[0]";
else
	$returnPath = "index.php?$return_filter";

foreach($RecordIDAry as $MeritRecordID)
{
	$ldiscipline->PENDING_MERIT_RECORD($MeritRecordID);
	# double check user can approve the record or not 
//	$can_approve = $ldiscipline->RETURN_AP_RECORDID_CAN_BE_APPROVED_BY_USER($MeritRecordID);
//	if($can_approve)
//	{
//		$ldiscipline->REJECT_MERIT_RECORD($MeritRecordID);
//	}
}

$msg = "update";
header("Location: ".$returnPath."&".$return_filter."&msg=".$msg);

intranet_closedb();
?>