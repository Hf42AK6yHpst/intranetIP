<?php
# Using: 

##### Change Log [Start] #####
#
#   Date    :   2019-05-21 (Bill)   [2019-0517-1540-40235]
#               prevent IntegerSafe() - change $MeritID to $MeritIDStr + $DemeritID to $DemeritIDStr
#
#	Date	:	2017-06-29	Bill	[2017-0629-1414-28066]
#				fixed cannot show Demrit Records
#
#	Date	:	2015-08-24	Bill
#				no need to check event date if No Event Date Limit
#
#	Date	:	2013-06-21 (Carlos)
#			:	$sys_custom['eDiscipline']['yy3'] - Display conduct score change with masked symbol for those overflow score merit items 
#
#	Date	:	2011-06-30 [Henry Chow]
#				can only redeem demerit record of current year
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libucc.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$ay = new academic_year();

# Check Access Right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Award_Punishment-Waive");

# Back Parameters
$back_para = "pageNo=$pageNo&order=$order&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&MeritType=$MeritType&s=$s&clickID=$clickID&pic=$pic";

# UI Settigns
$linterface = new interface_html();

if($sys_custom['eDiscipline']['yy3']){
	include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");
	$ldiscipline_ui = new libdisciplinev12_ui_cust();
}

# Menu Highlight
$CurrentPage = "Management_AwardPunishment";
$CurrentPageArr['eDisciplinev12'] = 1;

# Left Menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Tag Information
$TAGS_OBJ[] = array($eDiscipline['Award_and_Punishment']);

# Step Information
$STEPS_OBJ[] = array($eDiscipline["Redeem"]["Step1"], 1);
$STEPS_OBJ[] = array($eDiscipline["Redeem"]["Step2"], 0);

# Navigation Bar
//$PAGE_NAVIGATION[] = array($eDiscipline['RecordList'], "index.php?$back_para");
$PAGE_NAVIGATION[] = array($eDiscipline['RecordList'], "javascript:click_cancel()");
$PAGE_NAVIGATION[] = array($eDiscipline["Redemption"], "");

$linterface->LAYOUT_START();

# Initialization
$MeritIDArr = array();
if (isset($MeritIDStr))
{
	# from Step 2
    $MeritIDArr = unserialize(rawurldecode($MeritIDStr));
}
else
{
	# from Index Page
	if(is_array($RecordID))
		$MeritIDArr = $RecordID;
	else
		$MeritIDArr[] = $RecordID;
}

# Get DemeritID list from Step 2
if (isset($DemeritIDStr))
{
    $SelectedDemeritIDArr = unserialize(rawurldecode($DemeritIDStr));
}
else
{
	$SelectedDemeritIDArr = array();
}

$meritImg = "<img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_merit.gif' width='20' height='20' border='0' align='absmiddle' border='0' title=\'$i_Merit_Award\'>";
$demeritImg = "<img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_demerit.gif' width='20' height='20' border='0' align='absmiddle' border='0' title=\'$i_Merit_Punishment\'>";
$pendingImg = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif' width='20' height='20' align='absmiddle' title='".$i_Discipline_System_Award_Punishment_Pending."' border='0'>";
$rejectImg = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_reject.gif' width='20' height='20' align='absmiddle' title='".$i_Discipline_System_Award_Punishment_Rejected."' border='0'>";
$releasedImg = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_to_student.gif' width='20' height='20' align='absmiddle' title='".$i_Discipline_Released_To_Student."' border='0'>";
$approvedImg = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_approve_b.gif' width='20' height='20' align='absmiddle' title='".$i_Discipline_System_Award_Punishment_Approved."' border='0'>";
$waivedImg = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_waived.gif' width='20' height='20' align='absmiddle' title='".$i_Discipline_System_Award_Punishment_Waived."' border='0'>";
$remarksImg = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_remark.gif\" width=\"20\" height=\"20\" align=\"absmiddle\" title=\"".$iDiscipline['Remarks']."\" border=\"0\">";

$i_Merit_MinorCredit = addslashes($i_Merit_MinorCredit);
$i_Merit_MajorCredit = addslashes($i_Merit_MajorCredit);
$i_Merit_SuperCredit = addslashes($i_Merit_SuperCredit);
$i_Merit_UltraCredit = addslashes($i_Merit_UltraCredit);

$lastestMeritDate = "";
$earliestMeritDate = "";

# Get Student Name
$MeritInfo = $ldiscipline->RETRIEVE_MERIT_RECORD_INFO_BY_ID($MeritIDArr[0]);
$StudentID = $MeritInfo[0]['StudentID'];
$lu = new libuser($StudentID);
$student_name = $lu->UserNameLang();
$class_name = $lu->ClassName;
$class_no = $lu->ClassNumber;

$selected_student_table = "<table width='90%' border='0' cellspacing='0' cellpadding='5'>";
$selected_student_table .= "<tr><td width='50%' valign='top' class='sectiontitle'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_stu_ind.gif' width='20' height='20' border='0' align='absmiddle' />";
$selected_student_table .= $class_name. ($class_no?"-":"") .$class_no." ".$student_name;
$selected_student_table .= "</td></tr>";
$selected_student_table .= "</table>";

### Construct selected merit table ###
$MeritIDList = implode(", ", $MeritIDArr);

if($intranet_session_language=="en")
	{
		$meritStr = "CONCAT(a.ProfileMeritCount,' ',
					CASE (a.ProfileMeritType)
					WHEN 0 THEN '$i_Merit_Warning'
					WHEN 1 THEN '$i_Merit_Merit'
					WHEN 2 THEN '$i_Merit_MinorCredit'
					WHEN 3 THEN '$i_Merit_MajorCredit'
					WHEN 4 THEN '$i_Merit_SuperCredit'
					WHEN 5 THEN '$i_Merit_UltraCredit'
					WHEN -1 THEN '$i_Merit_BlackMark'
					WHEN -2 THEN '$i_Merit_MinorDemerit'
					WHEN -3 THEN '$i_Merit_MajorDemerit'
					WHEN -4 THEN '$i_Merit_SuperDemerit'
					WHEN -5 THEN '$i_Merit_UltraDemerit'
					ELSE 'Error' END, '(s)')";
	}
	else
	{
		$meritStr = "CONCAT(
					CASE (a.ProfileMeritType)
					WHEN 0 THEN '$i_Merit_Warning'
					WHEN 1 THEN '$i_Merit_Merit'
					WHEN 2 THEN '$i_Merit_MinorCredit'
					WHEN 3 THEN '$i_Merit_MajorCredit'
					WHEN 4 THEN '$i_Merit_SuperCredit'
					WHEN 5 THEN '$i_Merit_UltraCredit'
					WHEN -1 THEN '$i_Merit_BlackMark'
					WHEN -2 THEN '$i_Merit_MinorDemerit'
					WHEN -3 THEN '$i_Merit_MajorDemerit'
					WHEN -4 THEN '$i_Merit_SuperDemerit'
					WHEN -5 THEN '$i_Merit_UltraDemerit'
					ELSE 'Error' END,
					a.ProfileMeritCount,
					'". $Lang['eDiscipline']['Times'] ."'
					)";
	}

if($sys_custom['eDiscipline']['yy3']){
	$overflowMarkFlagFields = ",a.OverflowConductMark,a.OverflowMeritItemScore,a.OverflowTagScore ";
}
	
$sql = "SELECT
				IF(a.MeritType>0,\"$meritImg\",\"$demeritImg\") as meritImg,
				if(a.ProfileMeritCount=0, '--',
				". $meritStr ."				
				) as record,
				CONCAT(c.ItemCode, ' - ', REPLACE(c.ItemName,'<','&lt;')) as item,
				a.RecordDate,
				PICID, 
				a.fromConductRecords as reference,
				a.TemplateID as action,
				a.ReleaseStatus as status,
				a.RecordStatus,
				a.RecordID,
				a.CaseID,
				a.WaivedBy, 
				a.NoticeID,
				a.Remark,
				a.ConductScoreChange,
				a.SubScore1Change,
				a.AcademicYearID 
				$overflowMarkFlagFields
		FROM 
				DISCIPLINE_MERIT_RECORD as a
				LEFT OUTER JOIN 
					INTRANET_USER as b ON a.StudentID = b.UserID
				LEFT OUTER JOIN 
					DISCIPLINE_MERIT_ITEM as c ON a.ItemID = c.ItemID
		WHERE 
				a.DateInput IS NOT NULL
			AND
				a.RecordID IN ($MeritIDList)
		ORDER BY
			a.RecordDate desc ";
$MeritRecordInfo = $ldiscipline->returnArray($sql, 15);

# Navigation
$meritTable = "<table width='90%' border='0' cellspacing='0' cellpadding='4'>";
	$meritTable .= "<tr><td width='50%' valign='top' class='tabletext'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_merit.gif' width='20' height='20' border='0' align='absmiddle' />";
	$meritTable .= "<b>".$eDiscipline["AwardRedemption"]."</b>";
	$meritTable .= "</td></tr>";
$meritTable .= "</table>";

# Merit Table
$meritTable .= "<table width='90%' border='0' cellspacing='0' cellpadding='4'>";
	# Title
	$meritTable .= "<tr>";
		$meritTable .= "<td width='1' class='tabletop'>#</td>\n";
		$meritTable .= "<td width='1' class='tabletop'>&nbsp;</td>\n";
		$meritTable .= "<td width='10%' class='tabletop'>".$eDiscipline["Record"]."</td>\n";
		if($ldiscipline->Display_ConductMarkInAP) {
			$meritTable .= "<td width='5%' class='tabletop'>".$eDiscipline['Conduct_Mark']."</td>";
		}
		if($ldiscipline->UseSubScore) {
			$meritTable .= "<td width='5%' class='tabletop'>".$i_Discipline_System_Subscore1."</td>";
		}
		$meritTable .= "<td width='25%' class='tabletop'>".$eDiscipline['Category_Item']."</td>\n";
		$meritTable .= "<td width='15%' class='tabletop'>".$Lang['eDiscipline']['EventDate']."</td>\n";
		$meritTable .= "<td width='20%' class='tabletop'>".$i_Profile_PersonInCharge."</td>\n";
		$meritTable .= "<td width='10%' class='tabletop'>".$i_Discipline_System_Award_Punishment_Reference."</td>\n";
		$meritTable .= "<td width='10%' class='tabletop'>".$eDiscipline["Action"]."</td>\n";
		$meritTable .= "<td width='10%' class='tabletop'>".$i_Discipline_System_Discipline_Status."</td>\n";
	$meritTable .= "</tr>";
	
	# Content
	$iMax = count($MeritRecordInfo);
	$selectedYears = array();
	//$jsHistoryAry = "";
	for ($i=0; $i<$iMax; $i++)
	{
		list($thisMeritImg, $thisRecord, $thisItem, $thisRecordDate, $thisPICID, $thisReference, $thisAction, 
		$thisReleaseStatus, $thisRecordStatus, $thisRecordID, $thisCaseID, $thisWaivedBy, $thisNoticeID, $thisRemark, $thisConductScoreChange, $thisSubscoreChange, $RecordAcademicYearID) = $MeritRecordInfo[$i];
		
		if(!in_array($RecordAcademicYearID, $selectedYears))
			$selectedYears[] = $RecordAcademicYearID;
		
		# Use the earliest Merit Date to determine if Punishment Record can be redeemed or not
		if ($i==$iMax-1)
		{
			$earliestMeritDate = $thisRecordDate;
		}
		
		# Get CSS
		switch ($thisRecordStatus)
		{
			case DISCIPLINE_STATUS_PENDING: $css = "class='row_waiting'"; break;
			case DISCIPLINE_STATUS_APPROVED: $css = "class='row_approved'"; break;
			case DISCIPLINE_STATUS_WAIVED: $css = "class='row_approved record_Waived'"; break;
			case DISCIPLINE_STATUS_REJECTED: $css = "class='row_suspend'"; break;
			default: $css = "class='row_approved'"; break;
		}
							
		$meritTable .= "<tr {$css}>";
		$meritTable .= "<td class='tabletext'>".($i + 1)."</td>\n";
		
		# Merit Image
		$meritTable .= "<td>".$thisMeritImg."</td>";
		
		# Record
		$meritTable .= "<td class='tabletext'>". $ldiscipline->TrimDeMeritNumber_00($thisRecord)."</td>\n";
		
		# Conduct Score Change
		if($ldiscipline->Display_ConductMarkInAP) {
			if($thisConductScoreChange=="") $thisConductScoreChange = 0;
			if($sys_custom['eDiscipline']['yy3'] && ($MeritRecordInfo[$i]['OverflowConductMark']=='1' || $MeritRecordInfo[$i]['OverflowMeritItemScore']=='1' || $MeritRecordInfo[$i]['OverflowTagScore']=='1') && $thisConductScoreChange==0) {
				$meritTable .= "<td class='tabletext'>".$ldiscipline_ui->displayMaskedMark($thisConductScoreChange)."</td>\n";
			}
			else{
				$meritTable .= "<td class='tabletext'>".$thisConductScoreChange."</td>\n";
			}
		}
		
		# Study Score Change
		if($ldiscipline->UseSubScore) {
			$meritTable .= "<td class='tabletext'>".$thisSubscoreChange."</td>\n";
		}
		
		# Reason
		$meritTable .= "<td class='tabletext'>";
			if($thisItem=="") $thisItem = "---";
			$meritTable .= $thisItem;
			if ($thisRemark != "")
			{
				$meritTable .= "&nbsp;";
				$meritTable .= "<a href='javascript:;' onClick=\"changeClickID({$thisRecordID}); moveObject('show_remark".$thisRecordID."', true); 
									showResult({$thisRecordID},'record_remark'); MM_showHideLayers('show_remark".$thisRecordID."','','show');\">";
					$meritTable .= $remarksImg;
				$meritTable .= "</a>";
				$meritTable .= "<div onload='changeClickID({$thisRecordID}); moveObject(\'show_remark".$thisRecordID."}\', true);'
									id='show_remark".$thisRecordID."' style='position:absolute; width:320px; height:150px; z-index:-1;'></div>";
									
				$jsRemarkAry .= ($jsRemarkAry != '') ? ",".$thisRecordID : $thisRecordID;
			}
		$meritTable .= "</td>\n";
		
		# Event Date
		$meritTable .= "<td class='tabletext'>".$thisRecordDate."</td>\n";
		
		# PIC
		$meritTable .= "<td><table class='tbclip'><tr><td style='border-bottom: none'>";
		$name_field = getNameFieldByLang();
		if($thisPICID != '') {
			$sql2 = "SELECT $name_field FROM INTRANET_USER WHERE UserID IN ($thisPICID)";
			$PICNameArr = $ldiscipline->returnVector($sql2);
			if(sizeof($PICNameArr)!=0) 
			{
				$meritTable .= implode(", ", $PICNameArr);
			} 
			else 
			{
				$meritTable .= $thisPICID;
			}
		} 
		else 
		{
			$meritTable .= "---";	
		}
		$meritTable .= "</td></tr></table></td>\n";
		
		# Reference
		$meritTable .= "<td>";
        $meritTable .= ($thisReference==1) ? $ldiscipline->getAwardPunishHistory($thisRecordID, $image_path."/".$LAYOUT_SKIN) : "";
        $jsHistoryAry .= ($thisReference==1) ? (($jsHistoryAry != "") ? ",".$thisRecordID : $thisRecordID) : ""; 
        $meritTable .= ($thisReference==1 && $thisCaseID != 0) ? "<br>" : "";
        $meritTable .= ($thisCaseID != 0) ? "<a href=../case_record/view.php?CaseID=".$thisCaseID.">".$i_Discipline_System_Discipline_Case_Record_Case."</a>" : "";
        $meritTable .= ($thisReference!=1 && $thisCaseID == 0) ? "---" : "";
        $meritTable .= "</td>\n";
		
		# Action
		$meritTable .= "<td>";
		if($thisAction != '' && $thisAction != 0) # TemplateID != NULL / 0
		{
			# NoticeID != NULL / 0
			$lc = new libucc($thisNoticeID);
			$result = $lc->getNoticeDetails();
			$url = $result['url'];
			
			$meritTable .= ($thisNoticeID != '' && $thisAction != 0) ? "<a href='javascript:;' onClick=\"newWindow('{$url}',1)\">".$eDiscipline["SendNotice"]."</a>" : $eDiscipline["SendNotice"];
		} 
		else 
		{
			$meritTable .= "";	
		}
		
		# Detention
		$sql = "SELECT COUNT(*) FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE DemeritID = '$thisRecordID'";
		$detentionCount = $ldiscipline->returnVector($sql);
		if($detentionCount[0]!=0) {
			$meritTable .= ($thisAction != '' && $thisAction != 0) ? "<br>" : "";
			if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-View")) {
				$meritTable .= "<a href='javascript:;' onClick=\"changeClickID({$thisRecordID});moveObject('show_detail{$thisRecordID}', true);showResult({$thisRecordID},'detention');MM_showHideLayers('show_detail".$thisRecordID."','','show');\">".$eDiscipline['Detention']."</a><div onload='changeClickID({$thisRecordID});moveObject(\'show_detail{$thisRecordID}\', true);' id='show_detail".$thisRecordID."' style='position:absolute; width:320px; height:180px; z-index:-1;'></div>";
				$jsDetentionAry .= ($jsDetentionAry != '') ? ",".$thisRecordID : $thisRecordID;
			}
			else {
				$meritTable .= $eDiscipline['Detention'];
			}
			$merit_JS_Function .= " document.onload=moveObject('show_detail{$thisRecordID}', true); ";
		}
		$meritTable .= ($thisAction == "" && $thisAction == 0 && $detentionCount[0]==0) ? "---" : "";
		$meritTable .= "&nbsp;</td>\n";
		
		# Status
		$sql = "SELECT RecordStatus, ReleaseStatus FROM DISCIPLINE_MERIT_RECORD WHERE RecordID = '$thisRecordID'";
		$result = $ldiscipline->returnArray($sql,2);
		
		$meritTable .= "<td>";
		$meritTable .= ($result[0][0] == DISCIPLINE_STATUS_PENDING) ? $pendingImg : "";
		$meritTable .= ($result[0][0] == DISCIPLINE_STATUS_REJECTED) ? $rejectImg : "";
		$meritTable .= ($result[0][1] == DISCIPLINE_STATUS_RELEASED) ? $releasedImg : "";
		$meritTable .= ($result[0][0] == DISCIPLINE_STATUS_APPROVED) ? $approvedImg : "";
		if($result[0][0] == DISCIPLINE_STATUS_WAIVED) 
		{
			$meritTable .= "<a href='javascript:;' onClick=\"changeClickID({$thisRecordID});moveObject('show_waive{$thisRecordID}', true);showResult({$thisRecordID},'waived');MM_showHideLayers('show_waive".$thisRecordID."','','show');\">".$waivedImg."</a><div onload='changeClickID({$thisRecordID});moveObject(\'show_waive{$thisRecordID}\', true);' id='show_waive".$thisRecordID."' style='position:absolute; width:320px; height:180px; z-index:-1;'></div>";
			$merit_JS_Function .= "document.onload=moveObject('show_waive{$thisRecordID}', true);";
			$jsWaiveAry .= ($jsWaiveAry != '') ? ",".$thisRecordID : $thisRecordID;
		}
		$meritTable .= "</td>\n";
		
		$meritTable .= "</tr>\n";
	}

$meritTable .= "</table>\n";
/*
if($jsHistoryAry!="") {
	$meritTable .= "<script language='javascript'>\njsHistory=[".$jsHistoryAry."];\n</script>\n";
}
*/

if($merit_JS_Function != "") 
{
	$meritTable .= "<script languahe='javascript'>window.onload = pageOnLoad;function pageOnLoad() {".$merit_JS_Function."}</script>\n";
}
### End of Merit table ###

### Construct demerit table for selection ###
# Get demerit records of the students from the school year

# Get current school year
$currentYear = date("Y");
$currentMonth = date("m");
// $academic_year_start_month & $academic_year_start_day are set in settings.php
// if not set, 1 Sept will be the default start month and start date
$acadermicMonth = ($academic_year_start_month == "")? 9 : $academic_year_start_month;
$acadermicDay = ($academic_year_start_day == "")? 1 : $academic_year_start_day;
if ($currentMonth < $academic_year_start_month)
{
	# 2009-01-01 => search for records after 2008-09-01 also
	$acadermicYear = $currentYear - 1;
}
else
{
	# 2008-12-01 => search for records before 2008-09-01 only
	$acadermicYear = $currentYear;
}
//$acadermicDate = $acadermicYear."-".$acadermicMonth."-".$acadermicDay;
$academicDate = getStartDateOfAcademicYear(Get_Current_Academic_Year_ID());

//$AcademicYearID = ($AcademicYearID=="") ? Get_Current_Academic_Year_ID() : $AcademicYearID;
if($AcademicYearID!=0) {
	if(sizeof($selectedYears)==1)
		$yearConds = " AND a.AcademicYearID = '".$selectedYears[0]."'";
	else 
		$yearConds = " AND a.AcademicYearID = '$AcademicYearID'";
}
if($AcademicYearID=="" && sizeof($selectedYears)==1) {
	$yearConds = " AND a.AcademicYearID = '".$selectedYears[0]."'";
}

$sql = "SELECT
				IF(a.MeritType>0,\"$meritImg\",\"$demeritImg\") as meritImg,
				if(a.ProfileMeritCount=0, '--',
				". $meritStr ."
				) as record,
				CONCAT(c.ItemCode,' - ',REPLACE(c.ItemName,'<','&lt;')) as item,
				a.RecordDate,
				PICID, 
				a.fromConductRecords as reference,
				a.TemplateID as action,
				a.ReleaseStatus as status,
				a.RecordStatus,
				a.RecordID,
				a.CaseID,
				a.WaivedBy, 
				a.NoticeID,
				a.Remark,
				a.ConductScoreChange,
				a.SubScore1Change 
				$overflowMarkFlagFields 
		FROM 
				DISCIPLINE_MERIT_RECORD as a
				LEFT OUTER JOIN 
					INTRANET_USER as b ON a.StudentID = b.UserID
				LEFT OUTER JOIN 
					DISCIPLINE_MERIT_ITEM as c ON a.ItemID = c.ItemID
		WHERE 
				a.DateInput IS NOT NULL
			AND
				a.MeritType = '-1'
			AND
				a.StudentID = '$StudentID'
			/*
			AND
				((UNIX_TIMESTAMP(a.RecordDate) - UNIX_TIMESTAMP('$acadermicDate')) > 0)
			*/
			$yearConds
		ORDER BY
			a.RecordDate desc ";
$DemeritRecordInfo = $ldiscipline->returnArray($sql, 15);

# Year Selection
$allyears = $ay->Get_All_Year_List();
$allyears = build_assoc_array($allyears);

$yearSelection = "<SELECT name='AcademicYearID' id='AcademicYearID' onChange=\"document.form1.action='redeem_step1.php';document.form1.reset();document.form1.submit()\">\n";
$selected = ($AcademicYearID==0 || sizeof($selectedYears)>1) ? " selected" : "";
$yearSelection .= "<option value='0' $selected>$i_Discipline_System_Award_Punishment_All_School_Year</option>\n";
for($i=0; $i<sizeof($selectedYears); $i++) {
	//echo $AcademicYearID.'/'.$selectedYears[$i].'/';
	$selected = ($AcademicYearID==$selectedYears[$i] || ($AcademicYearID!="0" && sizeof($selectedYears)==1)) ? " selected" : "";
	//echo $selected.'<br>';
	$yearSelection .= "<option value='".$selectedYears[$i]."' $selected>".$allyears[$selectedYears[$i]]."</option>\n";
}
$yearSelection .= "</SELECT>\n";

//echo $sql;
# Navigation
$demeritTable = "<table width='90%' border='0' cellspacing='0' cellpadding='4'>";
$demeritTable .= "<tr><td width='50%' valign='top' class='tabletext'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_demerit.gif' width='20' height='20' border='0' align='absmiddle'>";
$demeritTable .= "<b>".$eDiscipline["PunishmentRedemption"]." $yearSelection</b>";
$demeritTable .= "</td></tr>";
$demeritTable .= "</table>";

# Demerit Table
$demeritTable .= "<table width='90%' border='0' cellspacing='0' cellpadding='4'>";
	# Title
	$demeritTable .= "<tr>";
		$demeritTable .= "<td width='1' class='tablegreentop tabletopnolink'>#</td>\n";
		$demeritTable .= "<td width='1' class='tablegreentop tabletopnolink'>&nbsp;</td>\n";
		$demeritTable .= "<td width='10%' class='tablegreentop tabletopnolink'>".$i_Discipline_Reason2."</td>\n";
		if($ldiscipline->Display_ConductMarkInAP) {
			$demeritTable .= "<td width='5%' class='tablegreentop tabletopnolink'>".$eDiscipline['Conduct_Mark']."</td>";
		}
		if($ldiscipline->UseSubScore) {
			$demeritTable .= "<td width='5%' class='tablegreentop tabletopnolink'>".$i_Discipline_System_Subscore1."</td>";
		}
		$demeritTable .= "<td width='25%' class='tablegreentop tabletopnolink'>".$i_Discipline_Reason."</td>\n";
		$demeritTable .= "<td width='15%' class='tablegreentop tabletopnolink'>".$Lang['eDiscipline']['EventDate']."</td>\n";
		$demeritTable .= "<td width='20%' class='tablegreentop tabletopnolink'>".$i_Profile_PersonInCharge."</td>\n";
		$demeritTable .= "<td width='10%' class='tablegreentop tabletopnolink'>".$i_Discipline_System_Award_Punishment_Reference."</td>\n";
		$demeritTable .= "<td width='10%' class='tablegreentop tabletopnolink'>".$eDiscipline["Action"]."</td>\n";
		$demeritTable .= "<td width='10%' class='tablegreentop tabletopnolink'>".$i_Discipline_System_Discipline_Status."</td>\n";
		$demeritTable .= "<td width='1' class='tablegreentop tabletopnolink' align='center'><input type=\"checkbox\" onClick=\"(this.checked)?setChecked(1,document.form1,'DemeritID[]'):setChecked(0,document.form1,'DemeritID[]')\"></td>\n";
	$demeritTable .= "</tr>";

	# Content
	$iMax = count($DemeritRecordInfo);
//	$jsHistoryAry = "";
	if ($iMax == 0)
	{
		$demeritTable .= "<tr class='table{$tablecolor}row2'><td height='80' class='tabletext' align='center' colspan='10'>".$no_record_msg."</td></tr>\n";
	}
	else
	{
		for ($i=0; $i<$iMax; $i++)
		{
			list($thisMeritImg, $thisRecord, $thisItem, $thisRecordDate, $thisPICID, $thisReference, $thisAction, $thisReleaseStatus, 
			$thisRecordStatus, $thisRecordID, $thisCaseID, $thisWaivedBy, $thisNoticeID, $thisRemark, $thisConductScoreChange, $thisSubscoreChange) = $DemeritRecordInfo[$i];
			
			# Get CSS
			switch ($thisRecordStatus)
			{
				case DISCIPLINE_STATUS_PENDING: $css = "class='row_waiting'"; break;
				case DISCIPLINE_STATUS_APPROVED: $css = "class='row_approved'"; break;
				case DISCIPLINE_STATUS_WAIVED: $css = "class='row_approved record_Waived'"; break;
				case DISCIPLINE_STATUS_REJECTED: $css = "class='row_suspend'"; break;
				default: $css = "class='row_approved'"; break;
			}
			
			$demeritTable .= "<tr {$css}>";
			$demeritTable .= "<td class='tabletext'>".($i + 1)."</td>\n";
			
			# Merit Image
			$demeritTable .= "<td>".$thisMeritImg."</td>";
			
			# Record
			$demeritTable .= "<td class='tabletext'>". $ldiscipline->TrimDeMeritNumber_00($thisRecord)."</td>\n";
			
			# Conduct Score Change
			if($ldiscipline->Display_ConductMarkInAP) {
				if($sys_custom['eDiscipline']['yy3'] && ($DemeritRecordInfo[$i]['OverflowConductMark']=='1' || $DemeritRecordInfo[$i]['OverflowMeritItemScore']=='1' || $DemeritRecordInfo[$i]['OverflowTagScore']=='1') ){
					$demeritTable .= "<td class='tabletext'>".$ldiscipline_ui->displayMaskedMark($thisConductScoreChange)."</td>\n";
				}
				else{
					$demeritTable .= "<td class='tabletext'>".$thisConductScoreChange."</td>\n";
				}
			}
			
			# Study Score Change
			if($ldiscipline->UseSubScore) {
				$demeritTable .= "<td class='tabletext'>". ($thisSubscoreChange ? $thisSubscoreChange : 0)."</td>\n";
			}
			
			# Reason
			$demeritTable .= "<td class='tabletext'>";
				$demeritTable .= $thisItem;
				if ($thisRemark != "")
				{
					$demeritTable .= "&nbsp;";
					$demeritTable .= "<a href='javascript:;' onClick=\"changeClickID({$thisRecordID}); moveObject('show_remark".$thisRecordID."', true); 
										showResult({$thisRecordID},'record_remark'); MM_showHideLayers('show_remark".$thisRecordID."','','show');\">";
						$demeritTable .= $remarksImg;
					$demeritTable .= "</a>";
					$demeritTable .= "<div onload='changeClickID({$thisRecordID}); moveObject(\'show_remark".$thisRecordID."}\', true);'
										id='show_remark".$thisRecordID."' style='position:absolute; width:320px; height:150px; z-index:-1;'></div>";
					
					$jsRemarkAry .= ($jsRemarkAry != '') ? ",".$thisRecordID : $thisRecordID;
										
				}
			$demeritTable .= "</td>\n";
			
			# Event Date
			$demeritTable .= "<td class='tabletext'>".$thisRecordDate."</td>\n";
			
			# PIC
			$demeritTable .= "<td><table class='tbclip'><tr><td style='border-bottom: none'>";
			$name_field = getNameFieldByLang();
			if($thisPICID != '') {
				$sql2 = "SELECT $name_field FROM INTRANET_USER WHERE UserID IN ($thisPICID)";
				$PICNameArr = $ldiscipline->returnVector($sql2);
				if(sizeof($PICNameArr)!=0) 
				{
					$demeritTable .= implode(", ", $PICNameArr);
				} 
				else 
				{
					$demeritTable .= $thisPICID;
				}
			} 
			else 
			{
				$demeritTable .= "---";	
			}
			$demeritTable .= "</td></tr></table></td>\n";
			
			# Reference
			$demeritTable .= "<td>";
	        $demeritTable .= ($thisReference==1) ? $ldiscipline->getAwardPunishHistory($thisRecordID, $image_path."/".$LAYOUT_SKIN) : "";
	        $jsHistoryAry .= ($thisReference==1) ? (($jsHistoryAry != "") ? ",".$thisRecordID : $thisRecordID) : ""; 
	        $demeritTable .= ($thisReference==1 && $thisCaseID != 0) ? "<br>" : "";
	        $demeritTable .= ($thisCaseID != 0) ? "<a href=../case_record/view.php?CaseID=".$thisCaseID.">".$i_Discipline_System_Discipline_Case_Record_Case."</a>" : "";
	        $demeritTable .= ($thisReference!=1 && $thisCaseID == 0) ? "---" : "";
	        $demeritTable .= "</td>\n";
			
			# Action
			$demeritTable .= "<td>";
			if($thisAction != '' && $thisAction != 0) # TemplateID != NULL / 0
			{
				# NoticeID != NULL / 0
				$lc = new libucc($thisNoticeID);
				$result = $lc->getNoticeDetails();
				$url = $result['url'];
				
				$demeritTable .= ($thisNoticeID != '' && $thisAction != 0) ? "<a href='javascript:;' onClick=\"newWindow('{$url}',1)\">".$eDiscipline["SendNotice"]."</a>" : $eDiscipline["SendNotice"];
			} 
			else 
			{
				$demeritTable .= "";	
			}
			
			# Detention
			$sql = "SELECT COUNT(*) FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE DemeritID = '$thisRecordID'";
			$detentionCount = $ldiscipline->returnVector($sql);
			if($detentionCount[0]!=0) {
				$demeritTable .= ($thisAction != '' && $thisAction != 0) ? "<br>" : "";
				if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-View")) {
					$demeritTable .= "<a href='javascript:;' onClick=\"changeClickID({$thisRecordID});moveObject('show_detail{$thisRecordID}', true);showResult({$thisRecordID},'detention');MM_showHideLayers('show_detail".$thisRecordID."','','show');\">".$eDiscipline['Detention']."</a><div onload='changeClickID({$thisRecordID});moveObject(\'show_detail{$thisRecordID}\', true);' id='show_detail".$thisRecordID."' style='position:absolute; width:320px; height:180px; z-index:-1;'></div>";
					$jsDetentionAry .= ($jsDetentionAry != '') ? ",".$thisRecordID : $thisRecordID;
				}
				else {
					$demeritTable .= $eDiscipline['Detention'];
				}
				$demerit_JS_Function .= " document.onload=moveObject('show_detail{$thisRecordID}', true); ";
			}
			$demeritTable .= ($thisAction == "" && $thisAction == 0 && $detentionCount[0]==0) ? "---" : "";
			$demeritTable .= "&nbsp;</td>\n";
			
			# Status
			$sql = "SELECT RecordStatus, ReleaseStatus FROM DISCIPLINE_MERIT_RECORD WHERE RecordID=$thisRecordID";
			$result = $ldiscipline->returnArray($sql,2);
			
			$demeritTable .= "<td>";
			$demeritTable .= ($result[0][0] == DISCIPLINE_STATUS_PENDING) ? $pendingImg : "";
			$demeritTable .= ($result[0][0] == DISCIPLINE_STATUS_REJECTED) ? $rejectImg : "";
			$demeritTable .= ($result[0][1] == DISCIPLINE_STATUS_RELEASED) ? $releasedImg : "";
			$demeritTable .= ($result[0][0] == DISCIPLINE_STATUS_APPROVED) ? $approvedImg : "";
			if($result[0][0] == DISCIPLINE_STATUS_WAIVED) 
			{
				$demeritTable .= "<a href='javascript:;' onClick=\"changeClickID(".$thisRecordID.");moveObject('show_waive{$thisRecordID}', true);showResult({$thisRecordID},'waived');MM_showHideLayers('show_waive".$thisRecordID."','','show');\">".$waivedImg."</a><div onload='changeClickID(".$thisRecordID.");moveObject(\'show_waive".$thisRecordID."\', true);' id='show_waive".$thisRecordID."' style='position:absolute; width:320px; height:180px; z-index:-1;'></div>";
				$demerit_JS_Function .= "document.onload=moveObject('show_waive".$thisRecordID."', true);";
				$jsWaiveAry .= ($jsWaiveAry != '') ? ",".$thisRecordID : $thisRecordID;
			}
			$demeritTable .= "</td>\n";
			
			# Checkbox
			if ($thisRecordStatus == DISCIPLINE_STATUS_APPROVED)
			{
				$checked = "";
				if (in_array($thisRecordID, $SelectedDemeritIDArr))
				{
					$checked = " checked ";
				}
				
				// Check if Input Date in the future
				$earliestMeritDateTime = strtotime($earliestMeritDate);
				$thisRecordDateTime = strtotime($thisRecordDate);
				
				// No need to check Event Date if Settings > System Properties > Award must be earlier than Punishment - No Event Date Limit  
//				if ($earliestMeritDateTime >= $thisRecordDateTime)
				if ($ldiscipline->NoLimitOnAwardPunishmentDate || $earliestMeritDateTime >= $thisRecordDateTime)
				{
					$demeritTable .= "<td valign=\"top\" align=\"center\"><input type=\"checkbox\" name=\"DemeritID[]\" value=\"".$thisRecordID."\" $checked></td>\n";
				}
				else
				{
					$demeritTable .= "<td>&nbsp;</td>";
				}
			}
			else
			{
				$demeritTable .= "<td>&nbsp;</td>";
			}
			
			$demeritTable .= "</tr>\n";
		}
	}

$demeritTable .= "</table>\n";
/*
if($jsHistoryAry != "") {
	$demeritTable .= "<script language='javascript'>jsHistory = [{$jsHistoryAry}];</script>";	
}
*/
if($demerit_JS_Function != "") 
{
	$demeritTable .= "<script languahe='javascript'>window.onload = pageOnLoad;function pageOnLoad() {".$demerit_JS_Function."}</script>\n";
}

# Declare JS Variable
$y = "<script language=javascript>";
$y .= ($jsDetentionAry != '') ? "var jsDetention=[{$jsDetentionAry}];\n" : "var jsDetention=[];\n";
$y .= ($jsWaiveAry != '') ? "var jsWaive=[{$jsWaiveAry}];\n" : "var jsWaive=[];\n";
$y .= ($jsRemarkAry != '') ? "var jsRemark=[{$jsRemarkAry}];\n" : "var jsRemark=[];\n";
$y .= ($jsHistoryAry != '') ? "var jsHistory=[{$jsHistoryAry}];\n" : "var jsHistory=[];\n";
$y .= "</script>";
echo $y;

### End of demerit table for selection ###
?>

<script language="javascript">
<!--
var xmlHttp

function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) 
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}

function click_cancel() {
	document.form1.action = "index.php";	
	document.form1.submit();
}

function getObject( obj ) {
	if ( document.getElementById ) {
		obj = document.getElementById( obj );
	}
	else if ( document.all ) {
		obj = document.all.item( obj );
	}
	else {
		obj = null;
	}
	
	return obj;
}

function moveObject( obj, e ) {
	var tempX = 0;
	var tempY = 0;
	var offset = 5;
	var objHolder = obj;
	
	obj = getObject( obj );
	if (obj==null) {return;}
	
	if (document.all) {
		tempX = event.clientX + document.body.scrollLeft;
		tempY = event.clientY + document.body.scrollTop;
	}
	else {
		tempX = e.pageX;
		tempY = e.pageY;
	}
	
	if (tempX < 0){tempX = 0} else {tempX = tempX - 320}
	if (tempY < 0){tempY = 0} else {tempY = tempY}
	
	obj.style.top  = (tempY + offset) + 'px';
	obj.style.left = (tempX + offset) + 'px';
	
	displayObject( objHolder, true );
}

function displayObject( obj, show ) {
	obj = getObject( obj );
	if (obj==null) return;
	
	obj.style.display = show ? 'block' : 'none';
	obj.style.visibility = show ? 'visible' : 'hidden';
}

function changeClickID(id) {
	document.form1.clickID.value = id;
	hideAllOtherLayer();
}

function checkCR(evt) {
	var evt  = (evt) ? evt : ((event) ? event : null);
	var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
	if ((evt.keyCode == 13) && (node.type=="text")) {return false;}	
}

function hideAllOtherLayer() {
	var layer = "";
	
	if(jsDetention.length != 0) {
		for(i=0;i<jsDetention.length;i++) {
			layer = "show_detail" + jsDetention[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	if(jsWaive.length != 0) {
		for(i=0;i<jsWaive.length;i++) {
			layer = "show_waive" + jsWaive[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	if(jsRemark.length != 0) {
		for(i=0;i<jsRemark.length;i++) {
			layer = "show_remark" + jsRemark[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	if(jsHistory.length != 0) {
		for(i=0;i<jsHistory.length;i++) {
			layer = "show_history" + jsHistory[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
}

document.onkeypress = checkCR;

/*********************************/
var xmlHttp

function showResult(str,flag)
{
	xmlHttp = GetXmlHttpObject()
	if (xmlHttp==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	}

	var url = "";
	url = "get_live.php?flag="+flag;
	url = url + "&RecordID=" + str
	url = url + "&sid=" + Math.random()
	
	if(flag=='detention') {
		xmlHttp.onreadystatechange = stateChanged 
	}
	else if(flag=='waived') {
		xmlHttp.onreadystatechange = stateChanged2 
	}
	else if(flag=='record_remark') {
		xmlHttp.onreadystatechange = stateChanged3 
	}
	else if(flag=='history') {
		xmlHttp.onreadystatechange = stateChanged4 
	}
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
}

function stateChanged() 
{
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{
		var id = document.form1.clickID.value;
		document.getElementById("show_detail"+id).style.zIndex = "1";
		document.getElementById("show_detail"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_detail"+id).style.border = "0px solid #A5ACB2";
	}
}

function stateChanged2() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_waive"+id).style.zIndex = "1";
		document.getElementById("show_waive"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_waive"+id).style.border = "0px solid #A5ACB2";
	} 
}

function stateChanged3() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_remark"+id).style.zIndex = "1";
		document.getElementById("show_remark"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_remark"+id).style.border = "0px solid #A5ACB2";
	} 
}

function stateChanged4() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_history"+id).style.zIndex = "1";
		document.getElementById("show_history"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_history"+id).style.border = "0px solid #A5ACB2";
	} 
}

function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}
/*********************************/

function checkForm(obj,element)
{
	if(countChecked(obj,element)==0)
		alert(globalAlertMsg2);
	else {
		document.form1.action = 'redeem_step2.php';
		document.form1.submit();
	}
}
//-->
</script>

<form name="form1" method="POST" action="redeem_step1_update.php">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="navigation">
				<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
					<tr><td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
					<tr><td>&nbsp;</td></tr>
					<tr><td><?=$linterface->GET_STEPS($STEPS_OBJ)?></td></tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center">
				<?= $ldiscipline->showWarningMsg($i_Discipline_System_Conduct_Instruction, $eDiscipline["SelectPunishmentToRedeem"]) ?>
			</td>
		</tr>
		<tr>
			<td align="center">
				<?= $selected_student_table ?>
			</td>
		</tr>
		<tr>
			<td align="center">
				<?= $meritTable ?>
			</td>
		</tr>
		<tr>
			<td align="center">
				<?= $demeritTable ?>
			</td>
		</tr>
	</table>
	
	<br />
	
	<table width="90%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td height="1" class="dotline"><img src="<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td align="center">
				<? if (count($DemeritRecordInfo) > 0) { ?>
					<?= $linterface->GET_ACTION_BTN($button_continue, "button", "javascript:checkForm(document.form1,'DemeritID[]');", "submitBtn") ?>&nbsp;
					<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "") ?>&nbsp;
				<? } ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:click_cancel()") ?>&nbsp;
			</td>
		</tr>
	</table>
	
	<br />
	<br />
	
	<input type="hidden" name="pageNo" value="<?php echo $pageNo; ?>"/>
	<input type="hidden" name="order" value="<?php echo $order; ?>"/>
	<input type="hidden" name="field" value="<?php echo $field; ?>"/>
	<input type="hidden" name="page_size_change" value=""/>
	<input type="hidden" name="numPerPage" value="<?=$page_size?>"/>
	<input type="hidden" name="MeritType" value="<?=$MeritType ?>">
	<input type="hidden" name="s" value="<?=$s?>"/>
	<input type="hidden" name="pic" value="<?=$pic?>"/>
	
	<input type="hidden" name="clickID" value=""/>
	<input type="hidden" name="MeritIDStr" value="<?=rawurlencode(serialize($MeritIDArr));?>">
	<input type="hidden" name="waitApproval" value="<?=$waitApproval?>" />
	<input type="hidden" name="approved" value="<?=$approved?>" />
	<input type="hidden" name="rejected" value="<?=$rejected?>" />
	<input type="hidden" name="released" value="<?=$released?>" />
	<input type="hidden" name="waived" value="<?=$waived?>" />
</form>

<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>