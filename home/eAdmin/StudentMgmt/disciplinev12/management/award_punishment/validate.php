<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

if(!is_array($student))
	$student = explode(",",$student);

$reason = array();	
$count = 0;	

for($l=0; $l<sizeof($student); $l++) {
	$i = $l+1;
	${"submitDetentionCount".$i} = $_GET["DetentionCount".$i];
	
	for ($m=1; $m<=${"submitDetentionCount".$i}; $m++) {
		${"submitSelectSession".$i."_".$m} = $_GET["SelectSession".$i."_".$m];
		if (${"submitSelectSession".$i."_".$m} <> "AUTO" && ${"submitSelectSession".$i."_".$m} <> "LATER") {
			$selectedSessionForOneStudent[$i][] = ${"submitSelectSession".$i."_".$m};
			$allSelectedSession[] = ${"submitSelectSession".$i."_".$m};
		}
	}
	${"submitTextRemark".$i} = $_GET["TextRemark".$i];

	$assignedSessionForEachStudent = array();
	
	for ($m=1; $m<=${"submitDetentionCount".$i}; $m++) {
		//echo ${"submitSelectSession".$i."_".$m};
		if (${"submitSelectSession".$i."_".$m} == "AUTO") {		# auto assign session
			$result = $ldiscipline->getDetentionList("FUTURE", "AVAILABLE", "THIS_FORM", $student[$i-1], "ASC");
			//debug_r($result);
			$TmpDetentionID = "";
			if (is_array($allSelectedSession)) {
				$TmpSessionArrayCount = array_count_values($allSelectedSession);
			}
			$AutoSuccess = false;
			for ($a=0; $a<sizeof($result); $a++) {
				list($TmpDetentionID,$TmpDetentionDate,$TmpDetentionDayName,$TmpStartTime,$TmpEndTime,$TmpPeriod,$TmpLocation,$TmpForm,$TmpPIC,$TmpVacancy,$TmpAssigned,$TmpAvailable) = $result[$a];
				if (!(is_array($selectedSessionForOneStudent[$i]) && in_array($TmpDetentionID, $selectedSessionForOneStudent[$i])) && ($TmpSessionArrayCount[$TmpDetentionID] < $TmpAvailable) && !($ldiscipline->checkStudentInSession($student[$i-1], $TmpDetentionID))) {
					$AutoSuccess = true;
					break;
				}
			}
			
			if ($TmpDetentionID == "" || $AutoSuccess == false) {
				$AvailableFlag[$i][$m] = "NO";
			} else {
				$AvailableFlag[$i][$m] = "YES";
				$selectedSessionForOneStudent[$i][] = $TmpDetentionID;
				$allSelectedSession[] = $TmpDetentionID;
				$AutoDetentionID[$i][$m] = $TmpDetentionID;
			}
		} else if (${"submitSelectSession".$i."_".$m} != "LATER") {		# with Detention ID
			$StudentInSessionFlag[$i][$m] = ($ldiscipline->checkStudentInSession($student[$i-1], ${"submitSelectSession".$i."_".$m}))?"YES":"NO";	# check whether the student already assigned in this detention session
			$PastFlag[$i][$m] = ($ldiscipline->checkPastByDetentionID(${"submitSelectSession".$i."_".$m}))?"YES":"NO";		# detention session in the past ?
			if (is_array($assignedSession)) {		// array for sessions that are not assigned by AUTO
				$TmpAssignedSessionArrayCount = array_count_values($assignedSession);
			}
			if (in_array(${"submitSelectSession".$i."_".$m}, $assignedSessionForEachStudent)) {
				$StudentInSessionFlag[$i][$m] = "YES";
				$AvailableFlag[$i][$m] = "NO";	# new add
			} else {
				$AvailableFlag[$i][$m] = "YES";	# new add
			}
			$FullFlag[$i][$m] = ($ldiscipline->checkFullByDetentionID(${"submitSelectSession".$i."_".$m}, $TmpAssignedSessionArrayCount[${"submitSelectSession".$i."_".$m}] + 1))?"YES":"NO";
			if ($StudentInSessionFlag[$i][$m] == "NO" && $PastFlag[$i][$m] == "NO" && $FullFlag[$i][$m] == "NO") {
				$assignedSession[] = ${"submitSelectSession".$i."_".$m};
				$assignedSessionForEachStudent[] = ${"submitSelectSession".$i."_".$m};
			}
		}
		//debug_r($assignedSessionForEachStudent);
	}
	
	for ($m=1; $m<=${"submitDetentionCount".$i}; $m++) {
		$allAvailableFlag[] = $AvailableFlag[$i][$m];
		$allStudentInSessionFlag[] = $StudentInSessionFlag[$i][$m];
		$allPastFlag[] = $PastFlag[$i][$m];
		$allFullFlag[] = $FullFlag[$i][$m];
	}
	
	
	
	$FailFlag = false;
	
	if (!((is_array($allAvailableFlag) && in_array("NO", $allAvailableFlag)) || (is_array($allStudentInSessionFlag) && in_array("YES", $allStudentInSessionFlag)) || (is_array($allPastFlag) && in_array("YES", $allPastFlag)) || (is_array($allFullFlag) && in_array("YES", $allFullFlag)))) {
		
		for ($m=1; $m<=${"submitDetentionCount".$i}; $m++) {
			// StudentID
			$DetentionArr[] = $student[$i-1];
			// DetentionID
			if (${"submitSelectSession".$i."_".$m}=="AUTO") {
				$DetentionArr[] = $AutoDetentionID[$i][$m];
			} else if (${"submitSelectSession".$i."_".$m}=="LATER") {
				$DetentionArr[] = "";
			} else {
				$DetentionArr[] = ${"submitSelectSession".$i."_".$m};
			}
			// Remark
			$DetentionArr[] = ${"submitTextRemark".$i};
		}
	
	} else {
		$FailFlag = true;	
	}

	if ($FailFlag) {
		$SessionNAMsg = str_replace("\n", "", str_replace("'", "\"", $linterface->GET_SYS_MSG("session_not_available")));
		$SessionPastMsg = str_replace("\n", "", str_replace("'", "\"", $linterface->GET_SYS_MSG("session_past")));
		$SessionFullMsg = str_replace("\n", "", str_replace("'", "\"", $linterface->GET_SYS_MSG("session_full")));
		$SessionAssignedBeforeMsg = str_replace("\n", "", str_replace("'", "\"", $linterface->GET_SYS_MSG("session_assigned_before")));

		for ($m=1; $m<=${"submitDetentionCount".$i}; $m++) {
			if ($AvailableFlag[$i][$m] == "NO") {
				$reason[$count] = 1;
			} else if ($FullFlag[$i][$m] == "YES") {
				$reason[$count] = 2;
			} else if ($PastFlag[$i][$m] == "YES") {
				$reason[$count] = 3;
			} else if ($StudentInSessionFlag[$i][$m] == "YES") {
				$reason[$count] = 4;
			} else {
				$reason[$count]	= 0;
			}
			$count++;
		}
//		echo $i.')'.$count.'/';
		/*
		$jInitialForm = "";
		
		for ($p=1; $p<=sizeof($student); $p++) {
			$jInitialForm .= "if(document.getElementById('DetentionCount".$p."')) document.getElementById('DetentionCount".$p."').value='".${"submitDetentionCount".$p}."';\n";
			$jInitialForm .= "if(document.getElementById('HiddenDetentionCount".$p."')) document.getElementById('HiddenDetentionCount".$p."').value='1';\n";
			$jInitialForm .= "updateSessionCell(".$p.");\n";
			for ($m=1; $m<=${"submitDetentionCount".$p}; $m++) {
				$jInitialForm .= "if(document.getElementById('SelectSession".$p."_".$m."')) document.getElementById('SelectSession".$p."_".$m."').value='".${"submitSelectSession".$p."_".$m}."';\n";
				if ($AvailableFlag[$p][$m] == "NO") {
					$jInitialForm .= "if(document.getElementById('SpanSysMsg".$p."_".$m."')) document.getElementById('SpanSysMsg".$p."_".$m."').innerHTML='".$SessionNAMsg."';\n";
					$reason[] = 1;
				} else if ($FullFlag[$p][$m] == "YES") {
					$jInitialForm .= "if(document.getElementById('SpanSysMsg".$p."_".$m."')) document.getElementById('SpanSysMsg".$p."_".$m."').innerHTML='".$SessionFullMsg."';\n";
					$reason[] = 2;
				} else if ($PastFlag[$p][$m] == "YES") {
					$jInitialForm .= "if(document.getElementById('SpanSysMsg".$p."_".$m."')) document.getElementById('SpanSysMsg".$p."_".$m."').innerHTML='".$SessionPastMsg."';\n";
					$reason[] = 3;
				} else if ($StudentInSessionFlag[$p][$m] == "YES") {
					$jInitialForm .= "if(document.getElementById('SpanSysMsg".$p."_".$m."')) document.getElementById('SpanSysMsg".$p."_".$m."').innerHTML='".$SessionAssignedBeforeMsg."';\n";
					$reason[] = 4;
				} else {
					$reason[] = 0;	
				}
			}
			$jInitialForm .= "if(document.getElementById('TextRemark".$p."')) document.getElementById('TextRemark".$p."').value='".${"submitTextRemark".$p}."';\n";
		}
		*/
		
	} else {
		for ($m=1; $m<=${"submitDetentionCount".$i}; $m++) {
			$reason[$count]	= 0;
			$count++;
		}
	
	}
	
}
//debug_r($AvailableFlag);
//echo implode(',',$allAvailableFlag);
$m = 0;
$showMsg = "";
for($l=0; $l<sizeof($student); $l++) {
	$i = $l+1;
	${"submitDetentionCount".$i} = $_GET["DetentionCount".$i];
	
	for($a=1; $a<=${"submitDetentionCount".$i}; $a++) {
		if($allAvailableFlag[$m]=="NO") {
			$showMsg .= "{show_result_".$i."_".$a.",$reason[$m])\n";
		} else {
			$showMsg .= "(show_result_".$i."_".$a.",$reason[$m])\n";	
		}
		$m++;
	}
}
if($showMsg != "") {
	echo $showMsg;
}

intranet_closedb();

?>