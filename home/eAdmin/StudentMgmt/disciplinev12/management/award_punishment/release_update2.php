<?php
// Modifying by: Bill

########## Change Log ###############
#
#	Date	:	2020-11-13 (Bill)	[2020-1009-1753-46096]
# 				delay notice start date & end date		($sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'])
#
#	Date	:	2017-03-02	(Bill)	[2016-0823-1255-04240]
#				default not distribute notice to parent	($sys_custom['eDiscipline']['NotReleaseNotice'])
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$lnotice = new libnotice();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Award_Punishment-Release");

$RecordIDAry = array();
$return_filter = "SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&num_per_page=$num_per_page&pageNo=$pageNo&order=$order&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&MeritType=$MeritType&s=$s&clickID=$clickID&fromPPC=$fromPPC&pic=$pic&passedActionDueDate=$passedActionDueDate";

$RecordIDAry = $RecordID;
if($back_page)
	$returnPath = $back_page."?id=$RecordIDAry[0]";
else
	$returnPath = "index.php?$return_filter";

foreach($RecordIDAry as $MeritRecordID)
{
	$datainfo = $ldiscipline->RETRIEVE_MERIT_RECORD_INFO_BY_ID($MeritRecordID);
	$original_RecordStatus = $datainfo[0]['RecordStatus'];
	$StudentID = $datainfo[0]['StudentID'];
	
	# set RELEASE if the status is APPROVED and not RELEASED
	$thisStatus = $ldiscipline->RETRIEVE_MERIT_RECORD_STATUS($MeritRecordID);
	$isReleased = $ldiscipline->RETRIEVE_MERIT_RECORD_RELEASE_STATUS($MeritRecordID);
	if(($thisStatus == DISCIPLINE_STATUS_APPROVED || $thisStatus == DISCIPLINE_STATUS_WAIVED) && !$isReleased)
	{
		# UPDATE [DISCIPLINE_MERIT_RECORD] field ReleasedBy, ReleasedDate, RecordStatus = RELEASE
		$ldiscipline->UPDATE_MERIT_RECORD_RELEASE_STATUS($MeritRecordID, DISCIPLINE_STATUS_RELEASED);
		$ReleasedNo++;
		
		$templateID = $datainfo[0]['TemplateID'];
		if($templateID && !$datainfo[0]['NoticeID'])# send eNotice if no notice is sent before
		{
			$template_info = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO($templateID);
			if(sizeof($template_info))
			{
				$template_category = $template_info[0]['CategoryID'];
				
				$SpecialData = array();
				$SpecialData['MeritRecordID'] = $MeritRecordID;
				
				$template_data = $ldiscipline->TEMPLATE_VARIABLE_CONVERSION($template_category, $StudentID, $SpecialData, $datainfo[0]['TemplateOtherInfo']);
				$UserName = $ldiscipline->getUserNameByID($UserID);
				
				# no need send notice if record is waived
				if($thisStatus != DISCIPLINE_STATUS_WAIVED && !$lnotice->disabled)
				{
					$Module = $ldiscipline->Module;
					$NoticeNumber = time();
					$TemplateID = $templateID;
					$Variable = $template_data;
					$IssueUserID = $UserID;
					$IssueUserName = $eDiscipline["DisciplineName"];
					$TargetRecipientType = "U";
					$RecipientID = array($StudentID);
					$RecordType = NOTICE_TO_SOME_STUDENTS;
					
					include_once($PATH_WRT_ROOT."includes/libucc.php");
					$lc = new libucc();
					$ReturnArr = $lc->setNoticeParameter($Module, $NoticeNumber, $TemplateID, $Variable, $IssueUserID, $IssueUserName, $TargetRecipientType,$RecipientID, $RecordType);

                    // [2020-1009-1753-46096] delay notice start date & end date
                    if (isset($sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay']) && $sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'] > 0)
                    {
                        $delayedHrs = $sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'];

                        $DateStart = date('Y-m-d H:i:s', strtotime("+".$delayedHrs." hours"));
                        $lc->setDateStart($DateStart);

                        if ($lc->defaultDisNumDays > 0) {
                            $DateEndTs = time() + ($lc->defaultDisNumDays * 24 * 60 * 60) + ($delayedHrs * 60 * 60);
                            $DateEnd = date('Y-m-d', $DateEndTs).' 23:59:59';
                            $lc->setDateEnd($DateEnd);
                        }
                    }

					$NoticeID = $lc->sendNotice();
	
					if($NoticeID != -1)
					{
						# update [DISCIPLINE_MERIT_RECORD] field NoticeID
						$updateDataAry = array();
						$updateDataAry['NoticeID'] = $NoticeID;
						$ldiscipline->UPDATE_MERIT_RECORD($updateDataAry, $MeritRecordID);
					}
				}
			} 
		}
		// [2016-0823-1255-04240]
		else if($datainfo[0]['NoticeID'] && !$sys_custom['eDiscipline']['NotReleaseNotice'])
		{
			include_once($intranet_root."/includes/libnotice.php");
			$lnotice = new libnotice();
			$lnotice->changeNoticeStatus($datainfo[0]['NoticeID'], DISTRIBUTED_NOTICE_RECORD);
		}
	}
}

$msg = "release";
header("Location: ".$returnPath."&".$return_filter."&msg=".$msg);

intranet_closedb();
?>