<?php
// Modifying by:  

############ Change Log Start #############
#
#   Date    :   2020-11-06  (Bill)  [2020-0824-1151-40308]
#               - Support Follow-up Feedback    ($sys_custom['eDiscipline']['APFollowUpRemarks'])
#
#   Date    :   2019-05-24  Bill
#               apply download_attachment.php
#
#	Date	:	2018-03-02 (Bill)	[2017-0317-1025-03225]
# 				- added button for "Rainbow Scheme"   ($sys_custom['eDiscipline']['TKP_RainbowScheme'])
#
#	Date	:	2017-03-16	(Bill)	[2016-1207-1221-39240]
#				support Activity Score display
#
#	Date	:	2016-06-07	(Bill)	[2016-0329-1757-08164]
#	Detail	:	$sys_custom['eDiscipline']['CSCProbation']
#				- display button to set current record in probation only for merit type is minor/major demerit
#
#	Date	:	2016-04-08 (Bill)	[2016-0224-1423-31073]
#				added prefix style to teacher name of deleted account
#
#	Date	:	2016-03-02 (Bill)	[2016-0224-1423-31073]
#				return PIC name even teacher account is inactive or removed
#
#	Date	:	2015-11-04 (Bill)	[2015-0416-1040-06164]
#				Hide Notice for Student Prefect - HKUGA Cust
#
# 	Date	:	2015-10-26 (Bill)	[2015-0611-1642-26164]
# 				add Probation button if punishment records is approved
#
# 	Date	:	2015-07-24 (Bill)	[2015-0611-1642-26164]
# 				support CWC Rehabilitation Scheme - Display info and reply button
#
#	Date	:	2015-06-29 (Bill)	[2015-0609-1638-43222]
#				display masked input marks (ConductScoreFakeChange) if user is eDisicpline admin
#
#	Date	:	2015-04-30 (Bill)	[2014-1216-1347-28164]
#				add button to select template for push message
#				display "Send Push Message" in Action
#
#	Date	:	2014-11-28 (Bill)
#				improved: sort conduct records by record date
#
#	Date	:	2014-06-13 (YatWoon)	- ip.2.5.5.8.1
#				fixed: missing to check eNotice is disabled, cause create discipline error [Case#K63069]
#
#	Date	:	2013-11-18 (YatWoon)
#				Customization: Add 2 extra fields: FileNumber, RecordNumber, $sys_custom['eDiscipline']['MST']['AP_Extra_Fields']	[Case#2013-0520-1008-41072]
#
#	Date	:	2013-10-22 (YatWoon)
#				update check approve right checking [Case#2013-1010-1749-02071]
#
#	Date	:	2013-06-21 (Carlos)
#	Detail	:	$sys_custom['eDiscipline']['yy3'] - Display conduct score change with masked symbol for those overflow score merit items 
#
#	Date	:	2013-05-15 (YatWoon)
#	Datail	:	add RecordInputDate (customization for yy3 - $sys_custom['eDiscipline']['yy3'])
#
#	Date	:	2012-10-09	YatWoon
#				update approve button display checking with ( || $hasApprovalRight )  [Case#2012-1004-1026-21054]
#
#	Date	:	2010-08-18	YatWoon
#				display student internal remark for client reference
#
#	Date	:	2010-06-25 (Henry)
#	Detail 	:	retrieve Year Name according to the AcademicYearID
#
#	Date	:	2010-06-10 (Henry)
#	Detail 	:	add attachment to record
#
#	Date:	2010-05-07 [YatWoon]
#			update the "released by" sentance if the record is auto released.
#	
#	Date:	2010-04-01 (Henry)
#			correct the link to GM record inside Reference table
# 
#	Date:	2010-02-01	Yat Woon
#			Don't display "Semester" data from database, need display semester name with YearTermID
#
############ Change Log End #############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libucc.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$lnotice = new libnotice();

if($sys_custom['eDiscipline']['yy3'])
{
	include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");
	$ldiscipline_ui = new libdisciplinev12_ui_cust();
}

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Award_Punishment-View");

// [2014-1216-1347-28164]
$canSendPushMsg = false;
if($plugin['eClassApp'] && $sys_custom['eDiscipline']['SendDetentionPushMessage'])
{
	$luser = new libuser();
	$canSendPushMsg = true;
}

$linterface = new interface_html();

# Menu Highlight
$CurrentPage = "Management_AwardPunishment";
$CurrentPageArr['eDisciplinev12'] = 1;

# Left Menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Tag information
$TAGS_OBJ[] = array($eDiscipline['Award_and_Punishment']);

# Navigation Bar
$PAGE_NAVIGATION[] = array($eDiscipline['RecordList'], "index.php");
$PAGE_NAVIGATION[] = array($eDiscipline["RecordDetails"], "");

$datainfo = $ldiscipline->RETRIEVE_MERIT_RECORD_INFO_BY_ID($id);
if(empty($datainfo))
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

# If case record and the case is FINISHED, then cannot approve/reject/edit/waive
if($datainfo[0]['CaseID']) {
	$isCase = 1;
}
# If case record and the case is FINISHED, then cannot approve/reject/edit/waive
if($ldiscipline->CASE_IS_FINISHED($datainfo[0]['CaseID'])) {
	$isFinishedCase = 1;
}

$data = $datainfo[0];
$sbjName = $ldiscipline->getSubjectNameBySubjectID($data['SubjectID']);
$remark = nl2br(stripslashes(($data['Remark'] ? $data['Remark'] : "---")));
// [2020-0824-1151-40308]
$followUpRemark = nl2br(stripslashes(($data['FollowUpRemark'] ? $data['FollowUpRemark'] : "---")));
$isAcc = $data['fromConductRecords'];
$modified_by = $data['ModifiedBy'];

# Retrieve Year
$yearname = $ldiscipline->getAcademicYearNameByYearID($data['AcademicYearID']);

# Retrieve Semester
$fcm = new academic_year_term($data['YearTermID']);
$semester = $fcm->Get_Year_Term_Name();

# Build html part
$lu = new libuser($modified_by);
if ($intranet_session_language == "en")
{
	$last_update = $i_Discipline_Last_Updated ." : " . $data['DateModified']. " " . $iDiscipline['By'] . " " . $lu->UserName();
}
else
{
	$last_update = $i_Discipline_Last_Updated ." : " . $data['DateModified']. " (" .$iDiscipline['By'] . $lu->UserName() . ")";
}

/*
$stu = new libuser($data['StudentID']);
$stu_class = $stu->ClassName;
$stu_classnumber = $stu->ClassNumber;
$stu_name = $stu_class . ($stu_classnumber ? "-".$stu_classnumber: "") . " " . $stu->UserName();
*/
$stu = $ldiscipline->getStudentNameByID($data['StudentID']);
$stu_class = $stu[0][2];
$stu_classnumber = $stu[0][3];
$stu_name = $stu_class . ($stu_classnumber ? "-".$stu_classnumber: "") . " " . $stu[0][1];

$PIC = $data['PICID'];
$namefield = getNameFieldWithLoginByLang();

// [2016-0224-1423-31073]
// INTRANET_USER
//$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE RecordType = 1 AND UserID IN ($PIC) AND RecordStatus = 1 ORDER BY $namefield";
$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE RecordType = 1 AND UserID IN ($PIC) ORDER BY $namefield";
$array_PIC = $ldiscipline->returnArray($sql);
				
// INTRANET_ARCHIVE_USER
//$sql = "SELECT UserID, $namefield FROM INTRANET_ARCHIVE_USER WHERE RecordType = 1 AND UserID IN ($PIC) ORDER BY $namefield";
$sql = "SELECT UserID, CONCAT('<span class=\"tabletextrequire\">*</span>', $namefield) FROM INTRANET_ARCHIVE_USER WHERE RecordType = 1 AND UserID IN ($PIC) ORDER BY $namefield";
$array_ArchivePIC = $ldiscipline->returnArray($sql);

// Merge user and archive user
$PICAry = array();
$array_PIC = array_merge($array_PIC, $array_ArchivePIC);
foreach($array_PIC as $k => $d) {
	$PICAry[] = $d[1];
}
$PICname = empty($PICAry) ? (($PIC != "") ? $PIC : "---") : implode("<br>", $PICAry);

if($data['Attachment'] != "")
{
	$path = "$file_path/file/disciplinev12/award_punishment/".$data['Attachment']."/";
	$attachment_list = $ldiscipline->getAttachmentArray($path, '');
	if(sizeof($attachment_list) > 0)
	{
		$attachmentHTML = "";
		for($i=0; $i<sizeof($attachment_list); $i++)
		{
			list($displayFilename) = $attachment_list[$i];
			// $linkFilename = str_replace(" ", "%20", $displayFilename);
			// $displayFilename = "<a href=/file/disciplinev12/award_punishment/".$data['Attachment']."/".$linkFilename." target=\"_blank\">$displayFilename</a>";
			
			$url = $path.$displayFilename;
			$displayFilename = "<a href='/home/download_attachment.php?target_e=".getEncryptedText($url)."' target=\"_blank\">$displayFilename</a>";
			$attachmentHTML .= ($attachmentHTML == "") ? $displayFilename : ", ".$displayFilename;
		}
		$attachmentHTML = "[".$attachmentHTML."]";
	}
	else
	{
		$attachmentHTML = "---";		
	}
}
else
{
	$attachmentHTML = "---";	
}

$thisMeritType = $ldiscipline->RETURN_MERIT_NAME($data['ProfileMeritType']);

if($data['MeritType']==1)	# Award setting
{
	$thisicon = "icon_merit";
	$AwardPunishment = $i_Merit_Award;
	$IncrementDecrement = "(".$i_Discipline_System_general_increment.")";
}
else if($data['MeritType']==-1)	# Punishment setting
{
	$thisicon = "icon_demerit";
	$AwardPunishment = $i_Merit_Punishment;
	$IncrementDecrement = "(".$i_Discipline_System_general_decrement.")";
}
else
{
	$thisicon = "";
	$AwardPunishment = "--";
	$IncrementDecrement = "";
}

if($data['fromConductRecords'])
{
	$conduct_records = $ldiscipline->RETRIEVE_GROUPED_CONDUCT_RECORD($id, $orderByTime=true);
	
	$Reference = "<td class='detail_box'>". $eDiscipline["HISTORY"] ."<br>";
	if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodconductMisconduct-View"))
	{
		//$Reference .= "<span class='tabletext form_sep_title'><i>- ". $eDiscipline["CorrespondingConductRecords"] ." -</i></span>";
		$Reference .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' bgcolor='#CCCCCC' class='layer_table_detail'>";
		$Reference .= "<tr class='tabletop'>
							<td width='15' align='center' valign='middle'>#</td>
							<td width='20' align='left'>&nbsp;</td>
							<td align='left'>". $i_Merit_Reason ."</td>";
		if($sys_custom['eDiscipline_GM_Times'])
		{
			$Reference .= "<td>{$Lang['eDiscipline']['Times']}</td>";	
		}
		$Reference .= "
							<td>". $i_EventDate ."</td>
							<td>". $i_Discipline_PIC ."</a></td>
						</tr>";
        
		for($i=0;$i<sizeof($conduct_records);$i++)
		{
			$thisIcon = ($conduct_records[$i]['RecordType']==1) ? "icon_gd_conduct.gif":"icon_misconduct.gif";
			$thisCatID = $conduct_records[$i]['CategoryID'];
			$thisItemID = $conduct_records[$i]['ItemID'];
			$thisRecordDate = substr($conduct_records[$i]['RecordDate'],0,10);
			$record_reason = $ldiscipline->RETURN_CONDUCT_REASON($thisItemID, $thisCatID);
			//$record_reason = stripslashes(intranet_htmlspecialchars($record_reason));
			$thisGmCount = (floor($conduct_records[$i]['GMCount'])==$conduct_records[$i]['GMCount']) ? floor($conduct_records[$i]['GMCount']) : round($conduct_records[$i]['GMCount'],1);
			
			$thisPIC = $ldiscipline->RETRIEVE_CONDUCT_PIC($conduct_records[$i]['RecordID']);
			$thisPIC = $thisPIC ? $thisPIC : "---";
			
			$Reference .= "<tr class='row_approved'>
								<td align='center' valign='top'>". ($i+1)."</td>
								<td align='left' valign='top' class='tabletext'><img src='". $image_path."/".$LAYOUT_SKIN ."/ediscipline/". $thisIcon ."' width='20' height='20'></td>
								<td valign='top' class='row_approved'><a href='../goodconduct_misconduct/edit.php?RecordID=".$conduct_records[$i]['RecordID']."'>". $record_reason ."</a></td>";
			if($sys_custom['eDiscipline_GM_Times'])
			{
				$Reference .= "<td>{$thisGmCount}</td>";	
			}
			$Reference .= "
								<td valign='top'>". $thisRecordDate ."</td>
								<td valign='top'>". $thisPIC ."</td>
							</tr>";
		}
		$Reference .= "</table>";
	}
	$Reference .= "</td>";
}

if($data['CaseID'])
{
	$case_info = $ldiscipline->RETRIEVE_CASE_INFO($data['CaseID']);
	$hasCaseViewRight = $ldiscipline->CHECK_FUNCTION_ACCESS("Discipline-MGMT-Case_Record-View");
	if($hasCaseViewRight) {
		$Reference = "<td><a href='../case_record/view.php?CaseID=". $data['CaseID']."' class='tablelink'>". $i_Discipline_System_Discipline_Case_Record_Case ." ". $case_info[0]['CaseNumber'] ."</a></td>";
	}
	else {
		$Reference = "<td><a href='javascript:no_view_right();' class='tablelink'>". $i_Discipline_System_Discipline_Case_Record_Case ." ". $case_info[0]['CaseNumber'] ."</a></td>";
	}
}

$Reference = $Reference ? $Reference :"<td>---</td>";

# Detention
//$DetentionPICFlag = false;
$assignedDetentionCount = 0;
$TempAllForms = $ldiscipline->getAllFormString();
$DetentionList = $ldiscipline->getDetentionListByDemeritID($data['RecordID']);
for ($i=0; $i<sizeof($DetentionList); $i++)
{
	list($TmpDetentionID, $TmpDetentionDate, $TmpDetentionDayName, $TmpStartTime, $TmpEndTime, $TmpPeriod, $TmpLocation, $TmpForm, $TmpPIC, $TmpVacancy, $TmpAssigned, $TmpAvailable) = $DetentionList[$i];
	if ($TmpForm == $TempAllForms) $TmpForm = $i_Discipline_All_Forms;
	$TmpAttendanceStatus = $ldiscipline->getAttendanceStatusByStudentIDDetentionID($data['StudentID'], $TmpDetentionID);
	if ($TmpAttendanceStatus[0] == "PRE") {
		$TmpAttendanceStatus = "<img src=\"$image_path/$LAYOUT_SKIN/smartcard/icon_present.gif\" width=\"20\" height=\"20\" title=\"$i_Discipline_Attended\">";
	}
	else if ($TmpAttendanceStatus[0] == "ABS") {
		$TmpAttendanceStatus = "<img src=\"$image_path/$LAYOUT_SKIN/smartcard/icon_absent.gif\" width=\"20\" height=\"20\" title=\"$i_Discipline_Absent\">";
	}
	else {
		$TmpAttendanceStatus = $i_Discipline_Not_Yet;
	}
	
	$DetentionOutputRow .= "<tr class=\"row_approved\">\n";
	$DetentionOutputRow .= "<td valign=\"top\">".($i+1)."</td>\n";
	if ($TmpDetentionID == "")
	{
		$DetentionOutputRow .= "<td valign=\"top\">".$i_Discipline_System_Not_Yet_Assigned."</td>\n";
	}
	else
	{
		$DetentionOutputRow .= "<td valign=\"top\">$TmpDetentionDate | $TmpStartTime - $TmpEndTime | $TmpLocation | $TmpPIC</td>\n";
	}
	$DetentionOutputRow .= "<td valign=\"top\"><span class=\"tabletextremark\">$TmpAttendanceStatus</span></td>\n";
	$DetentionOutputRow .= "</tr>\n";
	
	/*if($ldiscipline->isDetentionSessionPIC($TmpDetentionID))
		$DetentionPICFlag = true;
	*/	
	$assignedDetentionCount += ($TmpDetentionID!='') ? 1 : 0;
}

//if($assignedDetentionCount==0) $DetentionPICFlag = true;
if ($DetentionOutputRow != "")
{
	$DetentionOutputPre .= "<tr><td>".$eDiscipline['Detention']."</td></tr>\n";
	if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-View"))
	{
		$DetentionOutputPre .= "<tr><td style=\"border:1px solid #CCCCCC\">\n";
		$DetentionOutputPre .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
		$DetentionOutputPre .= "<tr class=\"tabletop\">\n";
		$DetentionOutputPre .= "<td width=\"15\" align=\"center\">#</td>\n";
		$DetentionOutputPre .= "<td>$i_Discipline_Arranged_Session</td>\n";
		$DetentionOutputPre .= "<td>$i_Discipline_Attendance</td>\n";
		$DetentionOutputPre .= "</tr>\n";
		$DetentionOutputPost = "</table></td></tr>\n";
		$DetentionOutputData = $DetentionOutputPre.$DetentionOutputRow.$DetentionOutputPost;
	}
	else
	{
		$DetentionOutputData = $DetentionOutputPre.$DetentionOutputPost;
	}
}
if ($DetentionOutputData) {
	$DetentionButtonLabel = $eDiscipline["EditDetention"];
	$DetentionButtonAction = "document.form1.action='edit_detention.php';document.form1.DetentionAction.value='Edit';document.form1.submit();";
}
else {
	$DetentionButtonLabel = $eDiscipline["AddDetention"];
	$DetentionButtonAction = "document.form1.action='edit_detention.php';document.form1.DetentionAction.value='Add';document.form1.submit();";
}

# [2014-1216-1347-28164] eClass App	
$msgButtonLabel = "";
$msgAvailable = false;
if($canSendPushMsg && $data['MeritType']==-1 && $luser->getParentUsingParentApp($data['StudentID'], true))
{
	$messageButton = $linterface->GET_BTN($Lang['eDiscipline']['DetentionMgmt']['SendPushMessage'], "button", "document.form1.action='notice_new.php?isMsg=1'; document.form1.submit();");
	$msgAvailable = true;
}
if($msgAvailable && $data['PushMessageID'])
{
	$msgButtonLabel = "<tr><td>".$Lang['eDiscipline']['DetentionMgmt']['SendPushMessage']."</td></tr>";
}

# eNotice
$lc = new libucc($data['NoticeID']);
$result = $lc->getNoticeDetails();
$is_disable = $lnotice->disabled ? "1" : "";
if ($data['NoticeID'])
{
	$eNoticePath = "<a href=\"#\" class=\"tabletext\" onClick=\"newWindow('".$result['url']."', 1)\">".$eDiscipline["PreviewNotice"]."<img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"></a>";
	$actionContent = $eNoticePath;
	$noticeButton = $linterface->GET_BTN($Lang['eDiscipline']['DeleteNotice'], "button", "deleteNotice()","","",$is_disable);
}
else if ($data['TemplateID'])
{
	$actionContent = $eDiscipline["SendNotice"];
	$noticeButton = $linterface->GET_BTN($Lang['eDiscipline']['DeleteNotice'], "button", "deleteNotice()","","",$is_disable);
}
else if (($msgAvailable && $DetentionOutputData == "" && $msgButtonLabel == "") || (!$msgAvailable && $DetentionOutputData == ""))
{
	$actionContent = $i_Discipline_No_Action;
	$noticeButton = $linterface->GET_BTN($Lang['eDiscipline']['AddNotice'], "button", "document.form1.action='notice_new.php';document.form1.submit();","","",$is_disable);
}
else
{
	$noticeButton = $linterface->GET_BTN($Lang['eDiscipline']['AddNotice'], "button", "document.form1.action='notice_new.php';document.form1.submit();","","",$is_disable);
}

switch($data['RecordStatus'])
{
	case DISCIPLINE_STATUS_REJECTED:
		$lu = new libuser($data['RejectedBy']);
		$icon = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_reject_l.gif' width='20' height='20' border='0' align='absmiddle'>";
		$status_str = $icon . " " . $eDiscipline["RejectedBy"] . " " . $lu->UserName() . " " . $data['RejectedDate']." ".$eDiscipline["RejectedBy3"];
		break;
	case DISCIPLINE_STATUS_APPROVED:
		$icon = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_approve_b.gif' width='20' height='20' border='0' align='absmiddle'>";
		if($data['ApprovedBy']==-1)
		{
			if ($intranet_session_language == "en") {
				$status_str = $icon . " " . $Lang['eDiscipline']['RecordisAutoApproved'] . " " . $eDiscipline["ApprovedBy2"]  .  " (" . $data['ApprovedDate'] . ")";
			}
			else {
				$status_str = $icon . " " . $Lang['eDiscipline']['RecordisAutoApproved'] . $eDiscipline["ApprovedBy2"]  . $data['ApprovedDate'];
			}
		}
		else
		{
			$lu = new libuser($data['ApprovedBy']);
			if ($intranet_session_language == "en") {
				$status_str = $icon . " " . $eDiscipline["ApprovedBy"] . " " . $lu->UserName() . " " . $eDiscipline["ApprovedBy2"] . " (" . $data['ApprovedDate'] . ") " . $eDiscipline["ApprovedBy3"];
			}
			else {
				$status_str = $icon . " " . $eDiscipline["ApprovedBy"] . " " . $lu->UserName() . " " . $eDiscipline["ApprovedBy2"] . " " . $data['ApprovedDate'] . " " . $eDiscipline["ApprovedBy3"];
			}
		}
		break;
	case DISCIPLINE_STATUS_WAIVED:
		$lu = new libuser($data['WaivedBy']);
		$icon = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_waived.gif' width='20' height='20' border='0' align='absmiddle'>";
		$status_str = $icon . " " . $eDiscipline["WaivedBy"] . " " . $lu->UserName() . " " . $data['WaivedDate']. " ".$eDiscipline["WaivedBy3"];

		$waived_info = $ldiscipline->RETRIEVE_WAVIED_RECORD_INFO($id);
		
		# Get other redeem records info if the record is waived by redemption
		$redeemInfoTable = "";
		if ($data['RedeemID'] != NULL)
		{
			$isRedeemed = 1;
			# Get info of the Redeem Group
			$thisRecordID = $data['RecordID'];
			$thisRedeemID = $data['RedeemID'];
			$RecordIDArr = $ldiscipline->getRedeemGroup($thisRedeemID, $thisRecordID);
			
			# Header (Corresponding waived records)
			$redeemInfoTable .= $ldiscipline->generateRedeemInfoTableHeader($thisRedeemID, $thisRecordID);
			
			# Table Content
			$TableColumnArr = array("Record", "Reason", "EventDate", "PIC", "Reference");
			$redeemInfoTable .= $ldiscipline->generateRedeemInfoTable($RecordIDArr, $TableColumnArr);
		}
		
		//$waive_reason = stripslashes(intranet_htmlspecialchars($waived_info[0]['Reason']));
		$waive_reason = $waived_info[0]['Reason'] ? $waived_info[0]['Reason'] : "---";
		
		$thisDisplay = "";
		$thisDisplay .= "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
		$thisDisplay .= "<tr><td class='tabletext'>".$eDiscipline["WaivedReason"] .": ". $waive_reason."</tr></td>";
		$thisDisplay .= "<tr><td class='tabletext'>".$redeemInfoTable."</tr></td>";
		$thisDisplay .= "</table>";
		
		$status_str .= "<tr><td>&nbsp;</td>";
		$status_str .= "<td class='detail_box'>". $thisDisplay ."</td>";
		$status_str .= "</tr>";

		break;
	case DISCIPLINE_STATUS_PENDING:
		$icon = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif' width='20' height='20' border='0' align='absmiddle'>";
		$status_str = $icon . " " . $i_Discipline_System_Award_Punishment_Pending;
		break;
}

switch($data['ReleaseStatus'])
{
	case DISCIPLINE_STATUS_RELEASED:
		$icon = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_to_student.gif' width='20' height='20' border='0' align='absmiddle'>";

		if($data['ReleasedBy']==-1)	# auto release
		{
			if ($intranet_session_language == "en") {
				$release_status_str = $icon . " " . $Lang['eDiscipline']['RecordisAutoReleased'] . " " . $eDiscipline["ApprovedBy2"]  .  " (" . $data['ReleasedDate'] . ")";
			}
			else {
				$release_status_str = $icon . " " . $Lang['eDiscipline']['RecordisAutoReleased'] . $eDiscipline["ApprovedBy2"]  . $data['ReleasedDate'];
			}
		}
		else
		{
			$lu = new libuser($data['ReleasedBy']);
			if ($intranet_session_language == "en") {
				$release_status_str = $icon . " " . $eDiscipline["ReleasedBy"] . " " . $lu->UserName() . " " . $eDiscipline["ApprovedBy2"] . " (" . $data['ReleasedDate'] . ")";
			}
			else {
				$release_status_str = $icon . " " . $eDiscipline["ReleasedBy"] . " " . $lu->UserName() . " " . $eDiscipline["ApprovedBy2"] . " " . $data['ReleasedDate']." ".$eDiscipline["ReleasedBy3"];
			}
		}
		break;
// 	case DISCIPLINE_STATUS_UNRELEASED:
// 		if($data['ReleasedBy'])
// 		{
// 			$lu = new libuser($data['ReleasedBy']);
// 			$icon = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_to_student.gif' width='20' height='20' border='0' align='absmiddle'>";
// 			$release_status_str = $icon . " " . $eDiscipline["UnReleasedBy"] . " " . $lu->UserName() . " " . $data['ReleasedDate'];
// 		}
// 		break;
}

if($release_status_str)
{
	$release_status_row = "
		<tr>
			<td valign='top' nowrap='nowrap'>&nbsp;</td>
			<td>". $release_status_str ."</td>
		</tr>
	";
}

# CWC Rehabilitation Scheme	[2015-0611-1642-26164]
/*
 * 1. Demerit Record
 * 2. Approved or Released Record (removed)
 */
$rehabilDetails = false;
//$rehabilSubmit = false;
if($sys_custom['eDiscipline']['CSCProbation'] && $data['MeritType'] == -1)
{
    // $data['RecordStatus']==DISCIPLINE_STATUS_APPROVED && $data['ReleaseStatus']==DISCIPLINE_STATUS_RELEASED){
	// merit record with rehabilitation settings
	if($data['RehabilPIC'])
	{
		// Rehabilitation Teacher PIC
		$lu = new libuser($data['RehabilPIC']);
		
		// Rehabilitation Status
		switch($data['RehabilStatus'])
		{
			case 1:
				$rehabil_status_icon = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_reject_l.gif' width='20' height='20' border='0' align='absmiddle'>";
				$rehabil_status_str = $Lang['eDiscipline']['CWCRehabil']['Status']['Rejected'];
				break;
			case 2:
				$rehabil_status_icon = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif' width='20' height='20' border='0' align='absmiddle'>";
				$rehabil_status_str = $Lang['eDiscipline']['CWCRehabil']['Status']['Processing'];
				break;
			case 3:
				$rehabil_status_icon = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_approve_b.gif' width='20' height='20' border='0' align='absmiddle'>";
				$rehabil_status_str = $Lang['eDiscipline']['CWCRehabil']['Status']['Complete'];
				break;
			case 4:
				$rehabil_status_icon = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_reject_l.gif' width='20' height='20' border='0' align='absmiddle'>";
				$rehabil_status_str = $Lang['eDiscipline']['CWCRehabil']['Status']['Incomplete'];
				break;
			default:
				$rehabil_status_icon = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif' width='20' height='20' border='0' align='absmiddle'>";
				$rehabil_status_str = $Lang['eDiscipline']['CWCRehabil']['Status']['WaitingApproval'];
				break;
		}
		
		// Display Content
		$rehabilContent = "";
		$rehabilContent .= "<tr>
								<td colspan=\"2\" valign=\"middle\" nowrap=\"nowrap\" class=\"form_sep_title\"><i> - ".$Lang['eDiscipline']['CWCRehabil']['ApplicationContent']." -</i></td>
							</tr>
							<tr>
								<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\">".$Lang['eDiscipline']['CWCRehabil']['FollowUpTeacher']."</td>
								<td>".$lu->UserName()."</td>
							</tr>
							<tr>
								<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\">".$Lang['eDiscipline']['CWCRehabil']['RehabilReason']."</td>
								<td>".nl2br($data['RehabilReason'])."</td>
							</tr>";
		
		if($data['RehabilFeedback'])
		{
    		$rehabilContent .= "<tr>
    								<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\">".$Lang['eDiscipline']['CWCRehabil']['RehabilResponse']."</td>
    								<td>".$data['RehabilFeedback']."</td>
    							</tr>";
		}
		
		$rehabilContent .= "<tr>
								<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\">".$Lang['eDiscipline']['CWCRehabil']['Status']['Title']."</td>
								<td>$rehabil_status_icon $rehabil_status_str";
		
		// Status Update Details
		if($data['RehabilFeedback'] && $data['RehabilUpdateDate'] && $data['RehabilLastModify'])
		{
            $lu = new libuser($data['RehabilLastModify']);
			if ($intranet_session_language == "en") {
				$update_str = $i_Discipline_Last_Updated ." : " . $data['RehabilUpdateDate']. " " . $iDiscipline['By'] . " " . $lu->UserName();
			}
			else {
				$update_str = $i_Discipline_Last_Updated ." : " . $data['RehabilUpdateDate']. " (" .$iDiscipline['By'] . $lu->UserName() . ")";
			}
			$rehabilContent .= " ($update_str)";
		}
		$rehabilContent .= "	</td>
							</tr>
							<tr>
								<td colspan='2' align='right' valign='middle' nowrap='nowrap'>".$linterface->GET_BTN($Lang['eDiscipline']['CWCRehabil']['ResponseToRequest'],"button","document.form1.action='edit_rehali.php'; document.form1.submit();")."</td>
							</tr>";

		$rehabilDetails = true;
	}
	// merit record without rehabilitation settings - allow student to submit
//	else if($_SESSION['UserType']==2){
//		$rehabilSubmit = true;
//	}
}

# Tin Ka Ping Rainbow Scheme	[2017-1206-0959-15236]
$rsSchemelDetails = false;
if($sys_custom['eDiscipline']['TKP_RainbowScheme'] && $data['RehabilStatus'] == "1" && $data['MeritType'] == -1 && ($data['ProfileMeritType'] == -2 || $data['ProfileMeritType'] == -3))
{
    if($data['RehabilLastModify'] != "" && $data['RehabilUpdateDate'] != "")
    {
        $lu = new libuser($data['RehabilLastModify']);
        
        $icon = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_waived.gif' width='20' height='20' border='0' align='absmiddle'>";
        if ($intranet_session_language == "en") {
            $rb_scheme_str = $icon . " " . $Lang['eDiscipline']['TKP']['JoinRainbowBy']. " " . $lu->UserName() . " " . $Lang['eDiscipline']['TKP']['JoinRainbowBy2']. " (" . $data['RehabilUpdateDate'] . ") " . $Lang['eDiscipline']['TKP']['JoinRainbowBy3'];
        }
        else {
            $rb_scheme_str = $icon . " " . $Lang['eDiscipline']['TKP']['JoinRainbowBy']. " " . $lu->UserName() . " " . $Lang['eDiscipline']['TKP']['JoinRainbowBy2'] . " " . $data['RehabilUpdateDate'] . " " . $Lang['eDiscipline']['TKP']['JoinRainbowBy3'];
        }
        $rb_scheme_row = " <tr>
                    			<td valign='top' nowrap='nowrap'>&nbsp;</td>
                    			<td>". $rb_scheme_str."</td>
                    		</tr> ";
    }
}

# Start layout
$linterface->LAYOUT_START();

# Reason Item
$ItemText = stripslashes(intranet_htmlspecialchars($data['ItemText']));
$ItemText = intranet_htmlspecialchars($data['ItemText']);
$ItemText = $ItemText ? $ItemText : "--";
/*
$itemAry = $ldiscipline->getAwardPunishItemByItemID($data['ItemID']);
if(sizeof($itemAry)>0)
	$ItemText = $itemAry[0]." - ".$itemAry[1]; 
$ItemText = $ItemText ? $ItemText : "--";
*/
//$receive = $data['ProfileMeritCount']>0 ? $data['ProfileMeritCount']." ".$thisMeritType: "--";
$receive = $ldiscipline->returnDeMeritStringWithNumber($data['ProfileMeritCount'], $thisMeritType);

# Check whether current user has the approval right (according to settings in "Approval Group")
$hasRightToApproveRecordID = array();
if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eDiscipline"])
{
	$hasRightToApproveRecordID = $ldiscipline->RETURN_AP_RECORDID_CAN_BE_APPROVED($UserID, "");
	if($hasRightToApproveRecordID[0] == -1)
	{
		$hasApprovalRight = 1;
	}
	else
	{
		$hasApprovalRight = (sizeof($hasRightToApproveRecordID)>0 && in_array($data['RecordID'],$hasRightToApproveRecordID)) ? 1 : 0;
	}
}
else
{
	$hasApprovalRight = 1;
}
$useApprovalGroupOnLevel = $ldiscipline->useApprovalGroup();

if ($hasApprovalRight)
{
	# if has right but not admin, the record may require higher admin group according to merit/demerit record approval settings
	if ($useApprovalGroupOnLevel && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eDiscipline"] && $hasApprovalRight)
	{
		if ($ldiscipline->CHECK_APPROVAL_REQUIRED($data['ProfileMeritType'],$data['ProfileMeritCount']))
		{
			$MeritRecordOnAmmount = $ldiscipline->RETURN_AP_RECORDID_CAN_BE_APPROVED($UserID, "", DISCIPLINE_STATUS_PENDING.",".DISCIPLINE_STATUS_REJECTED.",".DISCIPLINE_STATUS_WAIVED, "OnMeritAmount");
			if (sizeof($MeritRecordOnAmmount)<=0 || !in_array($data['RecordID'], $MeritRecordOnAmmount))
			{
				$hasApprovalRight = 0;
			}
		}
		//debug($MeritRecordID, $hasNoApprovalRight);
	}
}
?>

<script language="javascript">
<!--
var jsDetention = [];
var jsWaive = [];
var jsHistory = [];

function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) 
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
    
}

function changeClickID(id) {
	document.form1.clickID.value = id;
	hideAllOtherLayer();
}

function getObject( obj ) {
	if ( document.getElementById ) {
		obj = document.getElementById( obj );
	}
	else if ( document.all ) {
		obj = document.all.item( obj );
	}
	else {
		obj = null;
	}
	
	return obj;
}

function moveObject( obj, e ) {
	//alert(event.clientX+" "+event.clientY);
	var tempX = 0;
	var tempY = 0;
	var offset = 5;
	var objHolder = obj;
	
	obj = getObject( obj );
	if (obj==null) {return;}
	
	if (document.all) {
		tempX = event.clientX + document.body.scrollLeft;
		tempY = event.clientY + document.body.scrollTop;
	}
	else {
		tempX = e.pageX;
		tempY = e.pageY;
	}
	if (tempX < 0){tempX = 0} else {tempX = tempX - 300}
	if (tempY < 0){tempY = 0} else {tempY = tempY}
	
	obj.style.top  = (tempY + offset) + 'px';
	obj.style.left = (tempX + offset) + 'px';
	
	displayObject( objHolder, true );
}

function displayObject( obj, show ) {
	obj = getObject( obj );
	if (obj==null) return;
	
	obj.style.display = show ? 'block' : 'none';
	obj.style.visibility = show ? 'visible' : 'hidden';
}

function submit_form(act)
{
	eval("document.form1.action = '"+ act +"_update.php';");
	document.form1.submit();
}

function alert_reject()
{
	if(confirm("<?=$eDiscipline["RecordRejectAlert"]?>"))
		submit_form("reject");
}

function alert_remove()
{
	if(confirm("<?=$eDiscipline["RecordRemoveAlert"]?>"))
		submit_form("remove");
}

<?php if($sys_custom['eDiscipline']['CSCProbation']){ ?>
    function alert_probation()
    {
    	if(confirm("<?=$Lang['eDiscipline']['CWCProbation']['SetRecordToProbationAlert']?>")) {
    		submit_form("probation");
    	}
    }
<?php } ?>

<?php if($sys_custom['eDiscipline']['TKP_RainbowScheme'] && $ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) { ?>
    function alert_rbScheme()
    {
    	if(confirm("<?=$Lang['eDiscipline']['TKPRainbowScheme']['SetRecordToRBSchemeAlert']?>")) {
    		submit_form("rb_scheme");
    	}
    }

    function alert_rbSchemeLeave()
    {
    	if(confirm("<?=$Lang['eDiscipline']['TKPRainbowScheme']['SetRecordToLeaveRBSchemeAlert']?>")) {
    		submit_form("rb_scheme_remove");
    	}
    }
<?php } ?>

function hideAllOtherLayer() {
	var layer = "";
	
	if(jsDetention.length != 0) {
		for(i=0;i<jsDetention.length;i++) {
			layer = "show_detail" + jsDetention[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	if(jsWaive.length != 0) {
		for(i=0;i<jsWaive.length;i++) {
			layer = "show_waive" + jsWaive[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	if(jsHistory.length != 0) {
		for(i=0;i<jsHistory.length;i++) {
			layer = "show_history2_" + jsHistory[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
}

function go_detail(id)
{
	document.form1.id.value = id;
	document.form1.action="detail.php";
	document.form1.submit();
}

/*
window.onload = initialMove;

function initialMove() {
	if(jsHistory.length != 0)	 {
		for(i=0;i<jsHistory.length;i++) {
			layer = "show_history2_" + jsHistory[i];
			moveObject(layer,true);	
		}
	}
}
*/
//*******************
function showResult(str,flag)
{
	xmlHttp = GetXmlHttpObject()
	if (xmlHttp==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	}
	
	var url = "";
	url = "get_live.php?flag="+flag;
	url = url + "&RecordID=" + str
	url = url + "&sid=" + Math.random()
	
	if(flag=='detention') {
		xmlHttp.onreadystatechange = stateChanged 
	}
	else if(flag=='waived') {
		xmlHttp.onreadystatechange = stateChanged2 
	}
	else if(flag=='history') {
		xmlHttp.onreadystatechange = stateChanged3 
	}
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{
		var id = document.form1.clickID.value;
		document.getElementById("show_detail"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_detail"+id).style.border = "0px solid #A5ACB2";
	}
}

function stateChanged2() 
{
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{
		var id = document.form1.clickID.value;
		document.getElementById("show_waive"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_waive"+id).style.border = "0px solid #A5ACB2";
	}
}

function stateChanged3() 
{
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{
		var id = document.form1.clickID.value;
		document.getElementById("show_history2_"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_history2_"+id).style.border = "0px solid #A5ACB2";
	}
}

function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

function no_view_right()
{
	alert("<?=$i_general_no_access_right?>");	
}

function deleteNotice() {
	if(confirm("<?=$Lang['eDiscipline']['DeleteNotice']?>?")) {
		document.form1.action = 'notice_remove.php';
		document.form1.submit();	
	}
}
//-->
</script>

<form name="form1" method="POST">

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td class="navigation">
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr><td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
		</table>
	</td>
</tr>

<tr>
	<td align='center'>
		<table align='center' width='90%' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td align='right'><?=$linterface->GET_SYS_MSG($msg)?></td>
			</tr>
			<tr>
				<td align='right'><?=$last_update?></td>
			</tr>
		</table>
	</td>
</tr>

<tr>
	<td align='center'>
		<table align="center" width="90%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td valign="top" nowrap="nowrap" class="tablerow2"><?=$i_identity_student?></td>
			<td valign="top" class="tablerow2"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ediscipline/icon_stu_ind.gif" width="20" height="20" border="0" align="absmiddle"><?=$stu_name?></td>
		</tr>
		<tr>
			<td colspan="2" valign="middle" nowrap="nowrap" class="form_sep_title"><i> - <?=$eDiscipline["BasicInfo"]?> -</i></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Conduct_School_Year?></td>
			<td width="80%" valign="top"><?=$yearname?></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Conduct_Semester?></td>
			<td valign="top"><?=$semester?></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['EventDate']?></td>
			<td valign="top"><?=$data['RecordDate']?></td>
		</tr>
		
		<? if($sys_custom['eDiscipline']['yy3']) {?>
    		<tr>
    			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['EventRecordDate']?></td>
    			<td valign="top"><?=$data['RecordInputDate']?></td>
    		</tr>
		<? } ?>
		
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Contact_PIC?></td>
			<td valign="top"><?=$PICname?></td>
		</tr>
		
		<? if($data['Subject']) {?>
    		<tr>
    			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Homework_subject?></td>
    			<td><?=nl2br($sbjName)?></td>
    		</tr>
		<? } ?>
		
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Discipline_Case_Record_Attachment?></td>
			<td valign="top"><?=$attachmentHTML?></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_general_remark?></td>
			<td><?=$remark?></td>
		</tr>

        <? if ($sys_custom['eDiscipline']['APFollowUpRemarks']) {?>
            <tr>
                <td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['FollowUpRemark']?></td>
                <td><?=$followUpRemark?></td>
            </tr>
        <? } ?>
		
    	<? if($sys_custom['eDiscipline']['MST']['AP_Extra_Fields'])  {?>
    		<tr>
    			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['FileNumber']?></td>
    			<td><?=$data['FileNumber'] ? $data['FileNumber'] : "---"?></td>
    		</tr>
    		
    		<tr>
    			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['RecordNumber']?></td>
    			<td><?=$data['RecordNumber'] ? $data['RecordNumber'] : "---"?></td>
    		</tr>
    	<? } ?>
		
		<? if(trim($stu[0]['Remark'])) {?>
    		<tr>
    			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['General']['StudentInternalRemark']?></td>
    			<td><?=nl2br($stu[0]['Remark'])?></td>
    		</tr>
		<? } ?>
		
		<tr>
			<td colspan="2" valign="middle" nowrap="nowrap" class="form_sep_title"><i> - <?=$eDiscipline["RecordDetails"]?> -</i></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eDiscipline["Type"] ?></td>
			<td><?if($thisicon) {?><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ediscipline/<?=$thisicon?>.gif" width="20" height="20" border="0" align="absmiddle"> <? } ?><?=$AwardPunishment?></td>
		</tr>
		<tr valign="top">
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eDiscipline["Category_Item"]?></td>
			<td><?=$ItemText?></td>
		</tr>
		<tr valign="top">
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Discipline_Reason2?></span></td>
			<td><?=$receive?></td>
		</tr>
		
		<? if(!$ldiscipline->Hidden_ConductMark) {?>
    		<tr>
    			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eDiscipline['Conduct_Mark']?> <?=$IncrementDecrement?></td>
    			<td><label for="grading_passfail" class="tabletext"></label>
    					<label for="grading_passfail" class="tabletext">
    					<?php
    						if($sys_custom['eDiscipline']['yy3'] && ($data['OverflowConductMark']=='1' || $data['OverflowMeritItemScore']=='1' || $data['OverflowTagScore']=='1') && $data['ConductScoreChange']==0){
    							echo $ldiscipline_ui->displayMaskedMark($data['ConductScoreChange']);
    							if($ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])){
    								echo " ( ".$data['ConductScoreFakeChange']." )";
    							}
    						}
    						else{
    							echo ($data['ConductScoreChange'] ? $data['ConductScoreChange'] : 0);
    						}
    					?>
    					</label></td>
    		</tr>
		<? } ?>
		
		<? if($ldiscipline->UseSubScore) {?>
    		<tr>
    			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Subscore1?> <?=$IncrementDecrement?></td>
    			<td>
    				<label for="grading_passfail" class="tabletext"></label>
    				<label for="grading_passfail" class="tabletext"><?= (($data['SubScore1Change'])? $data['SubScore1Change'] : 0) ?></label>
    			</td>
    		</tr>
		<? } ?>
		
    	<? if($ldiscipline->UseActScore) {?>
    		<tr>
    			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['ActivityScore']?> <?=$IncrementDecrement?></td>
    			<td>
    				<label for="grading_passfail" class="tabletext"></label>
    				<label for="grading_passfail" class="tabletext"><?= (($data['SubScore2Change'])? $data['SubScore2Change'] : 0) ?></label>
    			</td>
    		</tr>
		<? } ?>
		
		<? if($sys_custom['wscss_disciplinev12_ap_prompt_action']) {?>
    		<tr>
    			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['AP_ActionDueDate']?></td>
    			<td><label for="grading_passfail" class="tabletext"><?= (($data['ActionDueDate']!="" && $data['ActionDueDate']!="0000-00-00") ? $data['ActionDueDate'] : "---") ?></label></td>
    		</tr>
		<? } ?>
		
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Award_Punishment_Reference?></td>
			<?=$Reference?>
		</tr>
		
		<? if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditAll") || (($ldiscipline->isMeritRecordPIC($id) || $ldiscipline->isMeritRecordOwn($id)) && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditOwn"))) {?>
    		<tr>
    			<td colspan='2' align='right' valign='middle' nowrap='nowrap'><?= $linterface->GET_BTN($eDiscipline["EditRecordInfo"], "button","document.form1.action='edit.php';document.form1.submit();")?></td>
    		</tr>
		<? } ?>
		
		<tr>
			<td colspan='2' valign='top' nowrap='nowrap' class='formfieldtitle'><img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif' width='10' height='1'></td>
		</tr>
		
		<?php if($sys_custom['eDiscipline']['CSCProbation'] && $rehabilDetails) {
			echo $rehabilContent;
		?>
			<tr>
				<td colspan='2' valign='top' nowrap='nowrap' class='formfieldtitle'><img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif' width='10' height='1'></td>
			</tr>
		<?php } ?>
		
		<?php if($sys_custom['eDiscipline']['add_AP_records_student_prefect'] && $ldiscipline->isStudentPrefect()){
				// do nothing
			  }
			  else { ?>
    		<tr>
    			<td colspan="2" valign="middle" nowrap="nowrap"><em class="form_sep_title"> - <?=$eDiscipline["Actions"]?> -</em></td>
    		</tr>
    		<tr>
    			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$iDiscipline['Accumulative_Category_Item']?></td>
    			<td>
    				<table width="100%" border="0" cellspacing="0" cellpadding="3">
    					<?=$DetentionOutputData?>
    					<tr>
    						<td><?=$actionContent?></td>
    					</tr>
    					<?=$msgButtonLabel?>
    				</table>
    			</td>
    		</tr>
    		<tr>
    			<td colspan='2' align='right' valign='middle' nowrap='nowrap'>
    			<?
    				if ($data['MeritType'] != 1)
    				{
    					if(($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditAll") || ($ldiscipline->isMeritRecordPIC($id) && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditOwn"))) && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-RearrangeStudent"))
    					{
    						if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-New") && $DetentionOutputData == "")
    							echo $linterface->GET_BTN($DetentionButtonLabel, "button", $DetentionButtonAction);
    						//if(($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-EditAll") || ($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-EditOwn") && $DetentionPICFlag)) && $DetentionOutputData)
    						if(($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-EditAll") || ($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-EditOwn"))) && $DetentionOutputData)
    							echo $linterface->GET_BTN($DetentionButtonLabel, "button", $DetentionButtonAction);
    					}
    				}
    			?>&nbsp;<?=$noticeButton?><?php if($msgAvailable){ ?>&nbsp;<?=$messageButton?><?php } ?>
    			</td>
    		</tr>
    		<tr>
    			<td colspan='2' valign='top' nowrap='nowrap' class='formfieldtitle'><img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif' width='10' height='1'></td>
    		</tr>
		<?php } ?>
		
		<tr>
			<td colspan="2" valign="middle" nowrap="nowrap"><em class="form_sep_title"> - <?=$eDiscipline["Status"]?> -</em></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap">&nbsp;</td>
			<td><?=$status_str?></td>
		</tr>
		<?=$release_status_row?>
		<?=$rb_scheme_row?>
	</table>
	</td>
</tr>

<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "<?=$image_path?>/<?=$LAYOUT_SKIN?>" ?>/10x10.gif" width="10" height="1"></td></tr>
		
		<? if ($isRedeemed) { ?>
    		<tr>
    			<td align="center">
    				<?= $linterface->GET_ACTION_BTN($button_finish, "button", "document.form1.action='index.php';document.form1.submit()")?>
    			</td>
    		</tr>
		<? }
		else {
		?>
    		<tr>
    			<td align="center">
    			<? if(!$isFinishedCase) {?>
        			<? if($hasApprovalRight) {?>
        				<? if($sys_custom['eDiscipline']['CSCProbation'] && $data['MeritType']==-1 && ($data['ProfileMeritType']==-2 || $data['ProfileMeritType']==-3) && $data['RecordStatus']==DISCIPLINE_STATUS_APPROVED){?>
        					<?= $linterface->GET_ACTION_BTN($Lang['eDiscipline']['CWCProbation']['Probation'], "button", "alert_probation();")?>
        				<? } ?>
        				<? if($sys_custom['eDiscipline']['TKP_RainbowScheme'] && $ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && $data['RehabilStatus'] != "1" && $data['MeritType'] == -1 && ($data['ProfileMeritType'] == -3 || $data['ProfileMeritType'] == -4) && $data['RecordStatus'] == DISCIPLINE_STATUS_APPROVED){?>
        					<?= $linterface->GET_ACTION_BTN($Lang['eDiscipline']['TKP']['JoinRainbowScheme'], "button", "alert_rbScheme();")?>
        				<? } ?>
        				<? if($sys_custom['eDiscipline']['TKP_RainbowScheme'] && $ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && $data['RehabilStatus'] == "1" && $data['MeritType'] == -1 && ($data['ProfileMeritType'] == -2 || $data['ProfileMeritType'] == -3) && $data['RecordStatus'] == DISCIPLINE_STATUS_APPROVED){?>
        					<?= $linterface->GET_ACTION_BTN($Lang['eDiscipline']['TKP']['LeaveRainbowScheme'], "button", "alert_rbSchemeLeave();")?>
        				<? } ?>
        				<? if(($data['RecordStatus']==DISCIPLINE_STATUS_REJECTED || $data['RecordStatus']==DISCIPLINE_STATUS_PENDING)){?>
        					<?= $linterface->GET_ACTION_BTN($button_approve, "button", "submit_form('approve');")?>
        				<? } ?>
        				<? if(($data['RecordStatus']==DISCIPLINE_STATUS_APPROVED || $data['RecordStatus']==DISCIPLINE_STATUS_PENDING)){?>
        					<?= $linterface->GET_ACTION_BTN($button_reject, "button", "alert_reject();")?>
        				<? } ?>
        			<? } ?>
    			<? } ?>
    			
    			<?// if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Lock")) {?>
    				<?//= $linterface->GET_ACTION_BTN($button_lock, "button")?>
    				<?//= $linterface->GET_ACTION_BTN($button_unlock, "button")?>
    			<?// } ?>
    			
    			<? if(!$isCase) {?>
        			<? if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Release") && $data['RecordStatus']==DISCIPLINE_STATUS_APPROVED) {?>
        				<? if($data['ReleaseStatus']==DISCIPLINE_STATUS_RELEASED){?>
        					<?= $linterface->GET_ACTION_BTN($button_unrelease, "button", "submit_form('unrelease');")?>
        				<? }
        				else {?>
        					<?= $linterface->GET_ACTION_BTN($button_release, "button", "submit_form('release');")?>
        				<? } ?>
        			<? } ?>
    			<? } ?>
    			
    			<? if(!$isFinishedCase) {?>
        			<? if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Waive")) {?>
        				<? if($data['RecordStatus']==DISCIPLINE_STATUS_APPROVED){?>
        					<?= $linterface->GET_ACTION_BTN($i_Discipline_System_Award_Punishment_Waive, "button", "document.form1.action='waive.php';document.form1.submit();")?>
        				<? } ?>
        				<? if($data['RecordStatus']==DISCIPLINE_STATUS_WAIVED){?>
        					<?= $linterface->GET_ACTION_BTN($Lang['eDiscipline']['UnwaiveRecord'], "button", "submit_form('unwaive');")?>
        				<? } ?>
        			<? } ?>
    			<? } ?>
    			
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<? if(!$isFinishedCase) {?>
    				<? if(!$isAcc && ($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-DeleteAll") || ($ldiscipline->isMeritRecordPIC($id) && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-DeleteOwn")))) {?>
    					<?= $linterface->GET_ACTION_BTN($button_delete, "button", "alert_remove();")?>
    				<? } ?>
				<? } ?>
				<?= $linterface->GET_ACTION_BTN($button_finish, "button", "document.form1.action='index.php';document.form1.submit()")?>
    			</td>
    		</tr>
		<? } ?>
	</table>
	</td>
</tr>
</table>

<br />
<input type="hidden" name="RecordID" id="RecordID" value="<?=$id?>">
<input type="hidden" name="DetentionAction" id="DetentionAction">
<!--<input type="hidden" name="clickID" value=""/>//-->

<input type="hidden" name="back_page" id="back_page" value="detail.php"/>

<input type="hidden" name="SchoolYear2" id="SchoolYear2" value="<?=$SchoolYear2?>" />
<input type="hidden" name="semester2" id="semester2" value="<?=$semester2?>" />
<input type="hidden" name="targetClass2" id="targetClass2" value="<?=$targetClass2?>" />
<input type="hidden" name="num_per_page" id="num_per_page" value="<?=$num_per_page?>" />
<input type="hidden" name="pageNo" id="pageNo" value="<?=$pageNo?>" />
<input type="hidden" name="order" id="order" value="<?=$order?>" />
<input type="hidden" name="field" id="field" value="<?=$field?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$numPerPage?>" />
<input type="hidden" name="MeritType" id="MeritType" value="<?=$MeritType?>" />
<input type="hidden" name="s" id="s" value="<?=$s?>" />
<input type="hidden" name="pic" id="pic" value="<?=$pic?>" />
<input type="hidden" name="clickID" id="clickID" value="<?=$clickID?>" />
<input type="hidden" name="waived2" id="waived2" value="<?=$waived2?>"/>
<input type="hidden" name="approved2" id="approved2" value="<?=$approved2?>"/>
<input type="hidden" name="waitApproval2" id="waitApproval2" value="<?=$waitApproval2?>"/>
<input type="hidden" name="released2" id="released2" value="<?=$released2?>"/>
<input type="hidden" name="rejected2" id="rejected2" value="<?=$rejected2?>"/>
<input type="hidden" name="fromPPC" id="fromPPC" value="<?=$fromPPC?>"/>
<input type="hidden" name="passedActionDueDate" id="passedActionDueDate" value="<?=$passedActionDueDate?>"/>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>