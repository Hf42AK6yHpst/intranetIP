<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

# Check if the selected merits are valid to redeem
# 1. all selection should be Merit
# 2. Only Record of one student can be selected

# initialization
$ldiscipline = new libdisciplinev12();
$RecordIDList = implode(", ", $RecordID);
$returnString = "";

# Build SQL
$sql = "SELECT
				MeritType, StudentID, RecordStatus
		FROM
				DISCIPLINE_MERIT_RECORD
		WHERE
				RecordID IN ($RecordIDList)
		";
$MeritInfoArr = $ldiscipline->returnArray($sql, 3);

$isAllMerit = 1;
$isSameStudentID = 1;
$lastStudentID = "";
$arraySize = count($MeritInfoArr);
for($i=0; $i<$arraySize; $i++)
{
	list($thisMeritType, $thisStudentID, $thisRecordStatus) = $MeritInfoArr[$i];
	
	# check merit type - only award record is accepted
	if ($thisMeritType == -1)
	{
		$returnString = $eDiscipline["jsWarning"]["MeritOnly"];
		break;
	}
	
	# check StudentID - only records of the same student can be selected
	if ( ($lastStudentID != "") && ($thisStudentID != $lastStudentID) )
	{
		$returnString = $eDiscipline["jsWarning"]["OneStudentOnly"];
		break;
	}
	$lastStudentID = $thisStudentID;
	
	# check if the record is approved - only approved record is accepted
	if ($thisRecordStatus != DISCIPLINE_STATUS_APPROVED)
	{
		$returnString = $eDiscipline["jsWarning"]["ApprovedRecordOnly"];
		break;
	}
}

//echo convert2unicode($returnString, 1);
echo $returnString;
intranet_closedb();

?>