<?php
// Modifying by: 

########## Change Log ###############
#
#	Date	:	2020-11-13 (Bill)	[2020-1009-1753-46096]
# 				delay notice start date & end date		($sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'])
#
#	Date	:	2015-04-30 (Bill)	[2014-1216-1347-28164]
#				send out notification if $eClassAppNotify = 1
#				update DB after send out push message
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libucc.php");

intranet_auth();
intranet_opendb();

$lnotice = new libnotice();
$lc = new libucc();
$ldiscipline = new libdisciplinev12();

$datainfo = $ldiscipline->RETRIEVE_MERIT_RECORD_INFO_BY_ID($RecordID);
$data = $datainfo[0];

$template_info = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO($SelectNotice);

if(sizeof($template_info) && isset($SelectNotice) && $SelectNotice!="") {
	$template_category = $template_info[0]['CategoryID'];
	$SpecialData = array();
	$SpecialData['MeritRecordID'] = $RecordID;
	$includeReferenceNo = 1;
	$template_data = $ldiscipline->TEMPLATE_VARIABLE_CONVERSION($template_category, $StudentID, $SpecialData, $TextAdditionalInfo, $includeReferenceNo);
	$UserName = $ldiscipline->getUserNameByID($UserID);
	$Module = $ldiscipline->Module;
	$NoticeNumber = time();
	$TemplateID = $SelectNotice;
	$Variable = $template_data;
	$IssueUserID = $UserID;
	$IssueUserName = $UserName[0];
	$TargetRecipientType = "U";
	$RecipientID = array($StudentID);
	$RecordType = NOTICE_TO_SOME_STUDENTS;
	
	// [2014-1216-1347-28164] Send eNotice
	if(!$isMsg){
		if (!$lnotice->disabled)
		{
			$ReturnArr = $lc->setNoticeParameter($Module, $NoticeNumber, $TemplateID, $Variable, $IssueUserID, $IssueUserName, $TargetRecipientType, $RecipientID, $RecordType);

			// [2020-1009-1753-46096] delay notice start date & end date
			if (isset($sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay']) && $sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'] > 0)
			{
				$delayedHrs = $sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'];

				$DateStart = date('Y-m-d H:i:s', strtotime("+".$delayedHrs." hours"));
				$lc->setDateStart($DateStart);

				if ($lc->defaultDisNumDays > 0) {
					$DateEndTs = time() + ($lc->defaultDisNumDays * 24 * 60 * 60) + ($delayedHrs * 60 * 60);
					$DateEnd = date('Y-m-d', $DateEndTs).' 23:59:59';
					$lc->setDateEnd($DateEnd);
				}
			}

			if(!isset($emailNotify) || $emailNotify=="") $emailNotify = 0;
			$NoticeID = $lc->sendNotice($emailNotify);

			if($data['ReleaseStatus']!=DISCIPLINE_STATUS_RELEASED) {
				$lnotice->changeNoticeStatus($NoticeID, SUSPENDED_NOTICE_RECORD);
			}
		}
	
		if($NoticeID != -1)
		{
			# update Merit Record
			$updateDataAry = array();
			$dataAry['NoticeID'] = $NoticeID;
			$dataAry['TemplateID'] = $TemplateID;
			$dataAry['TemplateOtherInfo'] = $TextAdditionalInfo;
			$ldiscipline->UPDATE_MERIT_RECORD($dataAry, $RecordID);
		}
	}
	// [2014-1216-1347-28164] eClass App - Send Notification
	else {
		if($plugin['eClassApp'] && $sys_custom['eDiscipline']['SendDetentionPushMessage']){
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
			include_once($PATH_WRT_ROOT."includes/libucc.php");
			
			$luser = new libuser();
			$leClassApp = new libeClassApp();
			
			$lc = new libucc();
			$ReturnArr = $lc->setNoticeParameter($Module, $NoticeNumber, $TemplateID, $Variable, $IssueUserID, $IssueUserName, $TargetRecipientType, $RecipientID, $RecordType);
				
			// structure data for sending message
			$lc->restructureData();
	
			// get ParentID and StudentID mapping
			$sql = "SELECT 
							ip.ParentID, ip.StudentID
					from 
							INTRANET_PARENTRELATION AS ip 
							Inner Join INTRANET_USER AS iu ON (ip.ParentID=iu.UserID) 
					WHERE 
							ip.StudentID IN ('".implode("','", (array)$RecipientID)."') 
							AND iu.RecordStatus = 1";
			$parentStudentAssoAry = BuildMultiKeyAssoc($lnotice->returnResultSet($sql), 'ParentID', array('StudentID'), $SingleValue=1, $BuildNumericArray=1);
			
			// prepare push message data
			$messageInfoAry = array();
			$messageInfoAry[0]['relatedUserIdAssoAry'] = $parentStudentAssoAry;
			$messageContent = removeHTMLtags($lc->Description);
			$messageContent = intranet_undo_htmlspecialchars($messageContent);
			$messageContent = str_replace("&nbsp;", "", $messageContent);
			$notifyMessageId = $leClassApp->sendPushMessage($messageInfoAry, $lc->Subject, $messageContent);
			if($notifyMessageId){
				$sql = "UPDATE DISCIPLINE_MERIT_RECORD SET PushMessageID='$notifyMessageId' WHERE RecordID='$RecordID'";
				$ldiscipline->db_db_query($sql);
			}
		}
	}
}


intranet_closedb();

$url = "detail.php?id=$RecordID&msg=update&num_per_page=$num_per_page&pageNo=$pageNo&order=$order&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&s=$s&clickID=$clickID&fromPPC=$fromPPC&passedActionDueDate=$passedActionDueDate&SchoolYear2=$SchoolYear2&semester2=$semester2&targetClass2=$targetClass2&MeritType2=$MeritType2";

header("Location: $url");
?>
