<?php
# using: 

############ Change Log Start #############################
#
#	Date	:	2018-03-02 (Bill)	[2017-0317-1025-03225]
#				Creete file
#
############################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right
if(!$ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-View") || !$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || !$sys_custom['eDiscipline']['TKP_RainbowScheme'])
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

// Please don't change to "\n" or "<br/>" - otherwise cannot remove "參加彩虹計劃" in Remarks
$nextLine = '
';

$RecordIDAry = is_array($RecordID) ? $RecordID : array($RecordID);

$return_filter = "SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&num_per_page=$num_per_page&pageNo=$pageNo&order=$order&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&MeritType=$MeritType&s=$s&clickID=$clickID&fromPPC=$fromPPC&pic=$pic&passedActionDueDate=$passedActionDueDate";
if($back_page)
	$returnPath = $back_page."?id=$RecordIDAry[0]";
else
	$returnPath = "index.php?$return_filter";

foreach($RecordIDAry as $MeritRecordID)
{
    $recordData = $ldiscipline->RETRIEVE_MERIT_RECORD_INFO_BY_ID($MeritRecordID);
    $recordData = $recordData[0];
    $recordMeritType = $recordData['ProfileMeritType'];
    $recordIsJoined  = $recordData['RehabilStatus'];
    
    if($recordIsJoined == "" && ($recordMeritType == "-3" || $recordMeritType == "-4"))
    {
        $dataAry = array();
        $dataAry['ProfileMeritType'] = intval($recordData['ProfileMeritType']) + 1;
        $dataAry['Remark'] = addslashes($recordData['Remark']);
        $dataAry['Remark'] .= (trim($recordData['Remark']) == '' ? '' : $nextLine) . $Lang['eDiscipline']['TKP']['JoinRainbowSchemeCH'];
        $dataAry['RehabilStatus'] = "1";
        $dataAry['RehabilLastModify'] = $UserID;
        $ldiscipline->UPDATE_MERIT_RECORD($dataAry, $MeritRecordID);
        
        $sql = "UPDATE DISCIPLINE_MERIT_RECORD SET RehabilUpdateDate = NOW(), DateModified = NOW() WHERE RecordID = '$MeritRecordID'";
        $ldiscipline->db_db_query($sql);
    }
}

$msg = "update";
header("Location: ".$returnPath."&".$return_filter."&msg=".$msg);

intranet_closedb();
?>