<?php
// Modifying by: 

########## Change Log ###############
#
#	Date	:	2020-11-13 (Bill)	[2020-1009-1753-46096]
# 				delay notice start date & end date		($sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'])
#
#	Date	:	2015-11-05 (Bill)	[2015-1104-1048-46222]
#				fixed 3 problem
#				1. Auto fill-in related to AP records not work
#				2. AP release status checking not work, so added notices always suspended
#				3. Stored incorrect Issue User Name in DB (not affect any function or display)
#
#	Date	:	2015-05-04 (Bill)
#				Create file [2014-1216-1347-28164]
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libucc.php");

intranet_auth();
intranet_opendb();

$lnotice = new libnotice();
$lc = new libucc();
$ldiscipline = new libdisciplinev12();

for($i=0; $i<$record_number; $i++)
{
	# Data from $_POST
	$record_id = $_POST["RecordID".($i+1)];
	$student_id = $_POST["StudentID".($i+1)];
	$notice_id = $_POST["SelectNotice".($i+1)];
	$cateogory_id = $_POST["CategoryID".($i+1)];
	$additional_info = $_POST["TextAdditionalInfo".($i+1)];
	$emailNotify = $_POST["emailNotify".$i];
	$with_template = $_POST["withNoticeTemplate".($i+1)];
	
	# skip if eNotice set template before
	if($with_template == 1)
		continue;

	// [2015-1104-1048-46222] fixed: notice always suspended
	// get merit record info for release status checking
	$datainfo = $ldiscipline->RETRIEVE_MERIT_RECORD_INFO_BY_ID($record_id);
	$data = $datainfo[0];
	
	$template_info = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO($notice_id);
	if(sizeof($template_info) && isset($notice_id) && $notice_id!="") {
		$SpecialData = array();
		// [2015-1104-1048-46222] fixed: auto fill-in not work as passing empty MeritRecordID to TEMPLATE_VARIABLE_CONVERSION()
		//$SpecialData['ConductRecordID'] = $record_id;
		$SpecialData['MeritRecordID'] = $record_id;
		$includeReferenceNo = 1;
		$template_data = $ldiscipline->TEMPLATE_VARIABLE_CONVERSION($cateogory_id, $student_id, $SpecialData, $additional_info, $includeReferenceNo);
		// [2015-1104-1048-46222] fixed: get incorrect issue user name
		//$UserName = $ldiscipline->getUserNameByID($student_id);
		$UserName = $ldiscipline->getUserNameByID($UserID);
		$Module = $ldiscipline->Module;
		$NoticeNumber = time();
		$TemplateID = $notice_id;
		$Variable = $template_data;
		$IssueUserID = $UserID;
		$IssueUserName = $UserName[0];
		$TargetRecipientType = "U";
		$RecipientID = array($student_id);
		$RecordType = NOTICE_TO_SOME_STUDENTS;
		
		$NoticeID = -1;
		if (!$lnotice->disabled)
		{
			$ReturnArr = $lc->setNoticeParameter($Module, $NoticeNumber, $TemplateID, $Variable, $IssueUserID, $IssueUserName, $TargetRecipientType, $RecipientID, $RecordType);

			// [2020-1009-1753-46096] delay notice start date & end date
			if (isset($sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay']) && $sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'] > 0)
			{
				$delayedHrs = $sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'];

				$DateStart = date('Y-m-d H:i:s', strtotime("+".$delayedHrs." hours"));
				$lc->setDateStart($DateStart);

				if ($lc->defaultDisNumDays > 0) {
					$DateEndTs = time() + ($lc->defaultDisNumDays * 24 * 60 * 60) + ($delayedHrs * 60 * 60);
					$DateEnd = date('Y-m-d', $DateEndTs).' 23:59:59';
					$lc->setDateEnd($DateEnd);
				}
			}

			if(!isset($emailNotify) || $emailNotify=="") $emailNotify = 0;
			$NoticeID = $lc->sendNotice($emailNotify);
			
			// set notice to suspend if record not release
			if($data['ReleaseStatus']!=DISCIPLINE_STATUS_RELEASED) {
				$lnotice->changeNoticeStatus($NoticeID, SUSPENDED_NOTICE_RECORD);
			}
		}
	
		if($NoticeID != -1)
		{
			# update record
			$dataAry = array();
			$dataAry['NoticeID'] = $NoticeID;
			$dataAry['TemplateID'] = $TemplateID;
			$dataAry['TemplateOtherInfo'] = $additional_info;
			$ldiscipline->UPDATE_MERIT_RECORD($dataAry, $record_id);
		}
	}
}

intranet_closedb();

$url = "index.php?msg=update&field=$field&order=$order&pageNo=$pageNo&numPerPage=$numPerPage&num_per_page=$num_per_page&page_size_change=$page_size_change&SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&MeritType=$MeritType2&pic=$pic&s=$s&approved=$approved&waitApproval=$waitApproval&released=$released&waived=$waived&rejected=$rejected&clickID=$clickID&fromPPC=$fromPPC&passedActionDueDate=$passedActionDueDate";

header("Location: $url");
?>
