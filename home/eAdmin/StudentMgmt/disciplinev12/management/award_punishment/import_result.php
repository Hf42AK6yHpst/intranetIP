<?php
// Modifying by: 

###################################
#
#   Date    :   2020-11-06  (Bill)  [2020-0824-1151-40308]
#   Detail	:	Support Follow-up Feedback Import       ($sys_custom['eDiscipline']['APFollowUpRemarks'])
#               Improved: Handle next line inputted in csv file
#
#	Date	:	2020-06-26  (Philips) [2020-0626-0935-22206]
#	Detail	:	- move alter table script before delete record prevent UserID column not exist
#
#	Date	:	2020-02-14	(Bill)	[2020-0212-1033-21073]
#	Detail	:	improve performance if import many records (> 100)
#               - apply temp data array to reduce query number
#               - group insert sql to reduce insert number
#               - use include_once() instead of include()
#
#	Date	:	2017-07-11	(Bill)	[2016-1125-0939-00240]
#	Detail	:	support Study Score Limit Checking ($sys_custom['eDiscipline']['ApplyMaxStudyScore'])
#
#	Date	:	2017-07-10 (Bill)	[2017-0704-1554-59236]
#	Detail	:	Fixed: cannot match AP items due to unwanted spaces in Item Code
#
#	Date	:	2017-06-13 (Bill)	[2017-0613-1311-05236]
#	Detail	:	Fixed: cannot import AP record if Award have Merit Type -999 only
#
#	Date	:	2017-06-12 (Bill)	[2016-1207-1221-39240]
#	Detail	:	Support Activity Score Import
#
#	Date	:	2017-05-11 (Bill)	[2017-0511-1242-12235]
#	Detail	:	Fixed: set merit number to 0 if merit type = -999
#
#	Date	:	2015-07-31 (Bill)	[DM#2895]
#	Detail	:	Fixed: remove addslashes() when import remarks of ap records, so as to prevent extra / from display
#
#	Date	:	2014-02-11 (YatWoon)
#	Detail	:	Fixed: admin failed to import other teacher as PIC records [Case#J58566]
#
#	Date	:	2013-11-21 (YatWoon)
#	Detail	: 	Customization: Add 2 extra fields: FileNumber, RecordNumber, $sys_custom['eDiscipline']['MST']['AP_Extra_Fields']	[Case#2013-0520-1008-41072]
#
#	Date	:	2013-09-03 (YatWoon)
#	Detail	:	add checking for $ldiscipline->OnlyAdminSelectPIC [Case#2013-0903-0925-36071]
#
#	Date	:	2013-06-24 (Carlos)
#	Detail	:	$sys_custom['eDiscipline']['yy3'] - added warning column for reminding user any conduct score will exceed limit
#
#	Date	:	2013-05-15 (YatWoon)
#	Datail	:	add RecordInputDate (customization for yy3 - $sys_custom['eDiscipline']['yy3'])
#
#	Date	:	2013-05-09 (YatWoon)
#	Detail	:	add checking not allow input future record
#
###################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$limport = new libimporttext();
$lo = new libfilesystem();
$ldiscipline = new libdisciplinev12();

$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));

if($ext != ".CSV" && $ext != ".TXT")
{
	header("location: import.php?xmsg=import_failed");
	exit();
}

// Handle next line inputted in csv file (convert to <br> first)
//$data = $limport->GET_IMPORT_TXT($csvfile);
$data = $limport->GET_IMPORT_TXT($csvfile, 0, '<br/>');
if(is_array($data)) {
	$col_name = array_shift($data);
}

if ($ldiscipline->use_subject)
{
	if($ldiscipline->UseSubScore) {
		$file_format = array('Class Name', 'Class Number', 'Event Date', 'Item Code', 'Subject', 'Conduct Mark', 'Study Score', 'Quantity', 'Type', 'PIC', 'Remark');
		if($ldiscipline->UseActScore) {
			if($sys_custom['eDiscipline']['CustomizeAPImport']) {
				$file_format = array('Class Name', 'Class Number', 'Event Date', 'Item Code', 'Subject', 'Conduct Mark', 'Attitude Score', 'Activity Score', 'Quantity', 'Type', 'PIC', 'Remark');
			}
			else {
				$file_format = array('Class Name', 'Class Number', 'Event Date', 'Item Code', 'Subject', 'Conduct Mark', 'Study Score', 'Activity Score', 'Quantity', 'Type', 'PIC', 'Remark');
			}
		}
	}
	else {
		$file_format = array('Class Name', 'Class Number', 'Event Date', 'Item Code', 'Subject', 'Conduct Mark', 'Quantity', 'Type', 'PIC', 'Remark');
	}
}
else
{
	if($ldiscipline->UseSubScore) {
		$file_format = array('Class Name', 'Class Number', 'Event Date', 'Item Code', 'Conduct Mark', 'Study Score', 'Quantity', 'Type', 'PIC', 'Remark');
		if($ldiscipline->UseActScore) {
			if($sys_custom['eDiscipline']['CustomizeAPImport']) {
				$file_format = array('Class Name', 'Class Number', 'Event Date', 'Item Code', 'Conduct Mark', 'Attitude Score', 'Activity Score', 'Quantity', 'Type', 'PIC', 'Remark');
			}
			else {
				$file_format = array('Class Name', 'Class Number', 'Event Date', 'Item Code', 'Conduct Mark', 'Study Score', 'Activity Score', 'Quantity', 'Type', 'PIC', 'Remark');
			}
		}
	}
	else {
		if($sys_custom['eDiscipline']['yy3']) {
			$file_format = array('Class Name', 'Class Number', 'Event Date', 'Event Record Date', 'Item Code', 'Conduct Mark', 'Quantity', 'Type', 'PIC', 'Remark');
        }
        else if($sys_custom['eDiscipline']['MST']['AP_Extra_Fields']) {
			$file_format = array('Class Name', 'Class Number', 'Event Date', 'Item Code', 'Conduct Mark', 'Quantity', 'Type', 'PIC', 'Remark', 'FileNumber', 'RecordNumber');
        }
		else {
			$file_format = array('Class Name', 'Class Number', 'Event Date', 'Item Code', 'Conduct Mark', 'Quantity', 'Type', 'PIC', 'Remark');
        }
	}
}
if($sys_custom['eDisciplinev12']['CheungShaWanCatholic']['AP_eNoticeChecked']) {
	$file_format[] = "Send Notice";
}

// [2020-0824-1151-40308]
if($sys_custom['eDiscipline']['APFollowUpRemarks']) {
    $file_format[] = 'Follow-up Remark';
}

$format_wrong = false;
for($i=0; $i<sizeof($file_format); $i++)
{
	if ($col_name[$i]!=$file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}
if($format_wrong)
{
	header("location: import.php?xmsg=wrong_header");
	exit();
}

$lu = new libuser();
$linterface = new interface_html();

# Menu Highlight
$CurrentPage = "Management_AwardPunishment";
$CurrentPageArr['eDisciplinev12'] = 1;

# Left Menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Tag Information
$TAGS_OBJ[] = array($eDiscipline['Award_and_Punishment']);

# Step Information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($i_general_confirm_import_data, 1);
$STEPS_OBJ[] = array($i_general_complete, 0);

$linterface->LAYOUT_START();

### List out the import result
$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"common_table_list_v30 view_table_list_v30\">";
$x .= "<tr>";
$x .= "<th class=\"field_title\" width=\"10\">#</td>";
$x .= "<th class=\"field_title\">$i_general_class</td>";
$x .= "<th class=\"field_title\">$i_identity_student</td>";
$x .= "<th class=\"field_title\">".$Lang['eDiscipline']['EventDate']."</td>";
if($sys_custom['eDiscipline']['yy3']) {
	$x .= "<th class=\"field_title\">".$Lang['eDiscipline']['EventRecordDate']."</td>";
}
$x .= "<th class=\"field_title\">$i_SettingsSemester</td>";
$x .= "<th class=\"field_title\">".$iDiscipline['RecordType']."</td>";
$x .= "<th class=\"field_title\">".$eDiscipline["RecordItem"]."</td>";
if ($ldiscipline->use_subject) {
	$x .= "<th class=\"field_title\">$i_Discipline_Subject</td>";
}
$x .= "<th class=\"field_title\">".$eDiscipline['Conduct_Mark']."</td>";
if($ldiscipline->UseSubScore) {
	$x .= "<th class=\"field_title\">".$i_Discipline_System_Subscore1."</td>";
}
if($ldiscipline->UseActScore) {
	$x .= "<th class=\"field_title\">".$Lang['eDiscipline']['ActivityScore']."</td>";
}
$x .= "<th class=\"field_title\">$i_general_receive</td>";
$x .= "<th class=\"field_title\">$i_Discipline_System_Discipline_Case_Record_PIC</td>";
$x .= "<th class=\"field_title\">$i_UserRemark</td>";
// [2020-0824-1151-40308]
if($sys_custom['eDiscipline']['APFollowUpRemarks']) {
    $x .= "<th class=\"field_title\">".$Lang['eDiscipline']['FollowUpRemark']."</td>";
}
if($sys_custom['eDisciplinev12']['CheungShaWanCatholic']['AP_eNoticeChecked']) {
	$x .= "<th class=\"field_title\">".$i_Discipline_System_Award_Punishment_Send_Notice."</td>";
}
if($sys_custom['eDiscipline']['MST']['AP_Extra_Fields']) {
	$x .= "<th class=\"field_title\">". $Lang['eDiscipline']['FileNumber'] ."</td>";
	$x .= "<th class=\"field_title\">". $Lang['eDiscipline']['RecordNumber'] ."</td>";
}
$x .= "<th class=\"field_title\">".$iDiscipline['AccumulativePunishment_Import_Failed_Reason']."</td>";
if($sys_custom['eDiscipline']['yy3']){
	$x .= "<th class=\"field_title\">".$Lang['General']['Warning']."</td>";	
}
$x .= "</tr>";

/*
# 20091109 yat - cannot drop the table if there is another user is importing...
$sql = "drop table temp_profile_student_merit_import";
$ldiscipline->db_db_query($sql);
*/
$sql = "CREATE TABLE IF NOT EXISTS temp_profile_student_merit_import
        (
            ClassName varchar(10),
            ClassNumber int(3),
            RecordDate datetime,
            RecordInputDate date,
            Year varchar(100),
            Semester varchar(100),
            StudentID int(11),
            ItemID int(11),
            ItemText text,
            ItemCode text,
            MeritType int(11),
            Remark text,
            ProfileMeritType int(11),
            ProfileMeritCount int(11),
            ConductScoreChange int(11),
            Subject varchar(255),
            PICID varchar(255),
            Teacher varchar(255),
            Count int(5),
            Category varchar(255),
            Type varchar(255),
            StudentName varchar(255),
            MeritTypeName varchar(255)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
$ldiscipline->db_db_query($sql);

# Check need to add "RecordInputDate" column or not
if($sys_custom['eDiscipline']['yy3'])
{
	$warning_occured = 0;
	$sql = "DESC temp_profile_student_merit_import";
	$result = $ldiscipline->returnArray($sql);

	$found = 0;
	foreach($result as $k => $d)
	{
		if($d['Field'] == "RecordInputDate")
		{
			$found=1;
			break;
		}
	}
	if(!$found)
	{
		$sql = "ALTER TABLE temp_profile_student_merit_import ADD RecordInputDate date DEFAULT NULL AFTER RecordDate";
		$ldiscipline->db_db_query($sql);
	}
}

# Check need to add "FileNumber" and "RecordNumber" column or not
if($sys_custom['eDiscipline']['MST']['AP_Extra_Fields'])
{
	$warning_occured = 0;
	$sql = "DESC temp_profile_student_merit_import";
	$result = $ldiscipline->returnArray($sql);

	$found = 0;
	foreach($result as $k => $d)
	{
		if($d['Field'] == "FileNumber")
		{
			$found=1;
			break;
		}
	}
	if(!$found)
	{
		$sql = "ALTER TABLE temp_profile_student_merit_import ADD FileNumber varchar(255) DEFAULT NULL, ADD RecordNumber varchar(255) DEFAULT NULL";
		$ldiscipline->db_db_query($sql);
	}
}

# Change ProfileMeritCount to float
$sql = "ALTER TABLE temp_profile_student_merit_import CHANGE ProfileMeritCount ProfileMeritCount float(8,2)";
$ldiscipline->db_db_query($sql);

# Change the temp table to UTF-8 format (avoid old table still using default character set)
$sql = "ALTER TABLE temp_profile_student_merit_import CONVERT TO CHARACTER SET utf8";
$ldiscipline->db_db_query($sql);

# Change ProfileMeritCount to float [CRM : 2012-0322-1710-27067]
if($sys_custom['eDiscipline']['ConductMark1DecimalPlace']) {
	$sql = "ALTER TABLE temp_profile_student_merit_import CHANGE ConductScoreChange ConductScoreChange float(8,1)";
	$ldiscipline->db_db_query($sql);
}

$fields = mysql_list_fields($intranet_db, 'temp_profile_student_merit_import');
$columns = mysql_num_fields($fields);
for ($i = 0; $i < $columns; $i++) {
    $field_array[] = mysql_field_name($fields, $i);
}
if (!in_array('UserID', $field_array)) {
	$result = mysql_query('ALTER TABLE temp_profile_student_merit_import ADD UserID int(11)');
}
if (!in_array('MeritTypeName', $field_array)) {     # some existing temp table may not have "MeritTypeName"
	$result = mysql_query('ALTER TABLE temp_profile_student_merit_import ADD MeritTypeName varchar(255)');
}
if (!in_array('SendNotice', $field_array)) {        # Add column SendNotice
	$sql = "ALTER TABLE temp_profile_student_merit_import ADD COLUMN SendNotice varchar(1)";
	$ldiscipline->db_db_query($sql);
}
if (!in_array('SubScoreChange', $field_array)) {    # Add column SubScoreChange
	$sql = "ALTER TABLE temp_profile_student_merit_import ADD COLUMN SubScoreChange int(11) AFTER ConductScoreChange";
	$ldiscipline->db_db_query($sql);
}
if (!in_array('ActScoreChange', $field_array)) {    # Add column ActScoreChange
	$sql = "ALTER TABLE temp_profile_student_merit_import ADD COLUMN ActScoreChange int(11) AFTER SubScoreChange";
	$ldiscipline->db_db_query($sql);
}
// [2020-0824-1151-40308]
if (!in_array('FollowUpRemark', $field_array)) {    # Add column FollowUpRemark
    $sql = "ALTER TABLE temp_profile_student_merit_import ADD COLUMN FollowUpRemark text";
    $ldiscipline->db_db_query($sql);
}

# 2020-06-26 (Philips) [#C188409] - delete after alter table prevent UserID column not existed
# Delete the temp data in temp table
$sql = "DELETE FROM temp_profile_student_merit_import WHERE UserID = ".$_SESSION["UserID"];
$ldiscipline->db_db_query($sql) or die(mysql_error());

/*
$semester_data = split("\n",get_file_content("$intranet_root/file/semester.txt"));
for ($i=0; $i<sizeof($semester_data); $i++)
{
	$target_sem = $semester_data[$i];
	$line = split("::",$target_sem);
	list ($name,$current,$fr,$to) = $line;
	$SemesterArr[] = $name;
}
*/
# get semester info
/*
$yearID = Get_Current_Academic_Year_ID();

$semester_data = getSemesters($yearID);
foreach($semester_data as $key=>$val) {
	$SemesterArr[] = $val;
}
*/

### reduce sql number
// data storage ary
$import_data = array();
$ap_item_ary = array();
$year_term_info_ary = array();
$subject_name_ary = array();
$item_cat_id_ary = array();
$class_id_ary = array();
$pic_info_ary = array();
$category_name_ary = array();

// static data ary
if ($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore'])
{
    $item_mapping = $ldiscipline->retrieveMeritItemswithCodeGroupByCat();
    $item_mapping = BuildMultiKeyAssoc((array)$item_mapping, array("ItemID"));
}
$ava_item = $ldiscipline->Retrieve_User_Can_Access_AP_Category_Item($UserID, 'item');
$merit_record_type = $ldiscipline->returnValidMeritTypeFieldWithOrder(1);
$demerit_record_type = $ldiscipline->returnValidMeritTypeFieldWithOrder(-1);
### reduce sql number

$error_occured = 0;
for($i=0; $i<sizeof($data); $i++)
{
	$error = array();
	$warning = array();
	$PicIDArr = array();
	$MeritType ='';
	$MeritNum = '';
	$MeritTypeName = '';
	$MeritNum = '';
	$ConductScoreChange = '';
	$SubScoreChange = '';
	$ActScoreChange = '';

    // Handle next line inputted in csv file (convert back to next line)
	for($j=0; $j<sizeof($data[$i]); $j++) {
        $data[$i][$j] = str_replace('<br/>', '
', $data[$i][$j]);
    }
	
	### Get Data
	if ($ldiscipline->use_subject)
	{
		if($sys_custom['eDisciplinev12']['CheungShaWanCatholic']['AP_eNoticeChecked']) {
			list($ClassName, $ClassNumber, $RecordDate, $ItemCode, $SubjectCode, $ConductScore, $MeritNum, $MeritType, $PIC, $Remark, $sendNotice) = $data[$i];
        }
		else {
			if($ldiscipline->UseSubScore) {
				list($ClassName, $ClassNumber, $RecordDate, $ItemCode, $SubjectCode, $ConductScore, $SubScore, $MeritNum, $MeritType, $PIC, $Remark) = $data[$i];
				if($ldiscipline->UseActScore){
					list($ClassName, $ClassNumber, $RecordDate, $ItemCode, $SubjectCode, $ConductScore, $SubScore, $ActScore, $MeritNum, $MeritType, $PIC, $Remark) = $data[$i];
				}
			}
			else {
				list($ClassName, $ClassNumber, $RecordDate, $ItemCode, $SubjectCode, $ConductScore, $MeritNum, $MeritType, $PIC, $Remark) = $data[$i];
			}

            // [2020-0824-1151-40308]
            if ($sys_custom['eDiscipline']['APFollowUpRemarks']) {
                $FollowUpRemark = $data[$i][sizeof($data[$i]) - 1];
            }
		}
	}
	else
	{
		if($sys_custom['eDisciplinev12']['CheungShaWanCatholic']['AP_eNoticeChecked']) {
			list($ClassName, $ClassNumber, $RecordDate, $ItemCode, $ConductScore, $MeritNum, $MeritType, $PIC, $Remark, $sendNotice) = $data[$i];
		}
		else if($sys_custom['eDiscipline']['yy3']) {
			list($ClassName, $ClassNumber, $RecordDate, $RecordInputDate, $ItemCode, $ConductScore, $MeritNum, $MeritType, $PIC, $Remark) = $data[$i];
		}
		else if($sys_custom['eDiscipline']['MST']['AP_Extra_Fields']) {
			list($ClassName, $ClassNumber, $RecordDate, $ItemCode, $ConductScore, $MeritNum, $MeritType, $PIC, $Remark, $FileNumber, $RecordNumber) = $data[$i];
		}
		else {
			if($ldiscipline->UseSubScore) {
				list($ClassName, $ClassNumber, $RecordDate, $ItemCode, $ConductScore, $SubScore, $MeritNum, $MeritType, $PIC, $Remark) = $data[$i];
				if($ldiscipline->UseActScore){
					list($ClassName, $ClassNumber, $RecordDate, $ItemCode, $ConductScore, $SubScore, $ActScore, $MeritNum, $MeritType, $PIC, $Remark) = $data[$i];
				}
			}
			else {
				list($ClassName, $ClassNumber, $RecordDate, $ItemCode, $ConductScore, $MeritNum, $MeritType, $PIC, $Remark) = $data[$i];
			}

            // [2020-0824-1151-40308]
            if ($sys_custom['eDiscipline']['APFollowUpRemarks']) {
                $FollowUpRemark = $data[$i][sizeof($data[$i]) - 1];
            }
		}
	}
	
	//$today = date("Y-m-d");
	/*if(strtotime($RecordDate)>strtotime($today))
	{
		$error['RecordDate'] = $iDiscipline['Award_Punishment_RecordDate_In_Future'];
	}*/

	# Merit Type
	// [2017-0704-1554-59236] trim space in Item Code
	$ItemCode = trim($ItemCode);
    // $ItemInfo = $ldiscipline->getAwardPunishItemByItemCode(addslashes($ItemCode));
    if(!isset($ap_item_ary[$ItemCode])) {
        $ap_item_ary[$ItemCode] = $ldiscipline->getAwardPunishItemByItemCode(addslashes($ItemCode));
    }
    $ItemInfo = $ap_item_ary[$ItemCode];
	$MeritTypeNum = "";
	if($MeritType == -999)
	{
		$MeritTypeNum = $ItemInfo['RecordType'];
		$RecordCat = "";
		
		// [2017-0511-1242-12235] set merit number to 0
		$MeritNum = 0;
	}
	else if($MeritType > 0)
	{
		$MeritTypeNum = 1;
		$RecordCat = $eDiscipline["AwardRecord"];
	}
	else if($MeritType < 0)
	{
		$MeritTypeNum = -1;	
		$RecordCat = $i_Discipline_System_Award_Punishment_Punishments;
	}
	else if($MeritType == '0')
	{
		$MeritTypeNum = -1;	
		$RecordCat = $i_Discipline_System_Award_Punishment_Punishments;
	}
	else
	{
		//$error['Wrong_MeritType']=$iDiscipline['Award_Punishment_Wrong_Merit_Type'];
	}

	/*
	// [2016-1125-0939-00240] Build Merit Item Mapping
	if ($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore'])
	{
		$item_mapping = $ldiscipline->retrieveMeritItemswithCodeGroupByCat();
		$item_mapping = BuildMultiKeyAssoc((array)$item_mapping, array("ItemID"));
	}
	*/

	if($ldiscipline->NotAllowEmptyInputInAP)
	{
		$msg = ($intranet_session_language=="en") ? $Lang['eDiscipline']['EmptyInput'].$eDiscipline['Conduct_Mark']." ".$eEnrollment['and']." ".$i_Discipline_System_No_Of_Award_Punishment.$Lang['eDiscipline']['IsEntered'] : $Lang['eDiscipline']['IsEntered'].$eDiscipline['Conduct_Mark']." ".$eEnrollment['and']." ".$i_Discipline_System_No_Of_Award_Punishment;
		
		if(($MeritNum == "" || $MeritNum == 0) && ($ConductScore == "" || $ConductScore == 0))
		{
			if(!$ldiscipline->UseSubScore) {
				$error['EmptyInputMeritDemerit'] = $msg;
			}
			else if($ldiscipline->UseSubScore && ($SubScore == "" || $SubScore == 0)) {
				if(!$ldiscipline->UseActScore) {
					$error['EmptyInputMeritDemerit'] = $msg;
				}
				else if ($ldiscipline->UseActScore && ($ActScore == "" || $ActScore == 0)) {
					$error['EmptyInputMeritDemerit'] = $msg;
				}
			}
		}
	}
	
	# Record Date
	if(trim($RecordDate) == '')
	{
		$error['RecordDate'] = $iDiscipline['Award_Punishment_Missing_RecordDate'];
	}
	else 
	{
	    //$RecordDate = $li
		$oriRecordDate = $RecordDate;
		$RecordDate = getDefaultDateFormat($RecordDate);
	
		$date_format = false;
		$date_format = $ldiscipline->checkDateFormat($RecordDate);
		
		if(!$date_format)
		{
			$error['RecordDate'] = $iDiscipline['Invalid_Date_Format'];			
		}
		else
		{
			# Check is future record or not
			if($RecordDate > date("Y-m-d"))
			{
				$error['RecordDate'] = $eDiscipline['Import']['Error']['RecordDateIsNotAllow'];
			}
			else
			{
				$NumOfMaxRecordDays = $ldiscipline->MaxRecordDayBeforeAllow;
				if($NumOfMaxRecordDays > 0) {
					$EnableMaxRecordDaysChecking = 1;
					$DateMinLimit = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d")-$NumOfMaxRecordDays, date("Y")));
					$DateMaxLimit = date("Y-m-d");
				}
				else {
					$EnableMaxRecordDaysChecking = 0;
				}
				
				if($EnableMaxRecordDaysChecking == 1)
				{
					if($DateMinLimit > $RecordDate) {
						$error['RecordDate'] = $eDiscipline['Import']['Error']['RecordDateIsNotAllow'];
					}
					else {
						if($RecordDate > $DateMaxLimit) {
							$error['RecordDate'] = $eDiscipline['Import']['Error']['RecordDateIsNotAllow'];
						}
					}
				}
			}
		}
	}
	
	if($sys_custom['eDiscipline']['yy3'])
	{
		# Record Input Date
		if(trim($RecordInputDate) == '')
		{
			$error['RecordInputDate'] = $iDiscipline['Award_Punishment_Missing_EventRecordDate'];
		}
		else 
		{
			$oriRecordInputDate = $RecordInputDate;
			$RecordInputDate = getDefaultDateFormat($RecordInputDate);
		
			$date_format = false;
			$date_format = $ldiscipline->checkDateFormat($RecordInputDate);
			
			if(!$date_format)
			{
				$error['RecordInputDate'] = $iDiscipline['Invalid_Date_Format'];			
			}
		}
	}

    # Semester
    // $year_info = getAcademicYearInfoAndTermInfoByDate($RecordDate);
    if(!isset($year_term_info_ary[$RecordDate])) {
        $year_term_info_ary[$RecordDate] = getAcademicYearInfoAndTermInfoByDate($RecordDate);
    }
    $year_info = $year_term_info_ary[$RecordDate];
	//$Year = GET_ACADEMIC_YEAR_WITH_FORMAT($RecordDate);
	//$Semester = retrieveSemester($RecordDate);
    $Year = $year_info[1];
    $Semester = $year_info[3];
	if(trim($Semester) == '')
	{
		$error['No_Semester'] = $eDiscipline['SemesterSettingError'];		
	}	
	
	/*
	$semester_mode = getSemesterMode();
	if($semester_mode==2)
	{
		$Semester = retrieveSemester($RecordDate);
		if(trim($Semester)=='')
		{
			$error['No_Semester'] = $eDiscipline['SemesterSettingError'];		
		}	
	}
	else
	{
		if(trim($Semester)=='')
		{
			$error['No_Semester'] = $iDiscipline['Award_Punishment_Missing_Semester'];
		}
		else
		{
			if(!in_array($Semester,$SemesterArr) && $Semester!='Whole Year')
			{
				$error['Wrong_Semester'] = "No such semester";
			}
		}
	}
	*/
	
	# Subject
	$SubjectName = '--';
	if ($ldiscipline->use_subject)
	{
		if(trim($SubjectCode) != '')
		{
			//$sql = "select count(*) from INTRANET_SUBJECT where SubjectName = '$SubjectCode'";
			//$sql = "select SubjectName from INTRANET_SUBJECT where CONCAT('SUBJ',SubjectID) = '$SubjectCode'";
			//$sql = "SELECT ".Get_Lang_Selection('CH_DES','EN_DES')." as SubjectName FROM ASSESSMENT_SUBJECT WHERE CONCAT('SUBJ', RecordID) = '$SubjectCode'";
			//$SubjectArr = $ldiscipline->returnVector($sql);
            if(!isset($subject_name_ary[$SubjectCode])) {
                $sql = "SELECT ".Get_Lang_Selection('CH_DES','EN_DES')." as SubjectName FROM ASSESSMENT_SUBJECT WHERE CONCAT('SUBJ', RecordID) = '$SubjectCode'";
                $subject_name_ary[$SubjectCode] = $ldiscipline->returnVector($sql);
            }
            $SubjectArr = $subject_name_ary[$SubjectCode];
			
			if(!$SubjectArr[0])
			{
				$error['No_Subject'] = $iDiscipline['Award_Punishment_No_Subject'];
			}
			else
			{
				$SubjectName = $SubjectArr[0];
			}
		}
	}
	
	# Merit Num
	if(trim($MeritNum) == '')
	{
		//$error ['No_MeritNum'] = $iDiscipline['Award_Punishment_No_Merit_Num'];	
	}
	else
	{
		if(!is_numeric($MeritNum))
		{
			$error ['MeritNum_Numeric'] = $iDiscipline['Award_Punishment_Merit_Numeric'];
		}
		else
		{
			$IsFloat = (strpos($MeritNum,"."));
			if($MeritNum < 0)
			{
				$error ['MeritNum_Negative'] = $iDiscipline['Award_Punishment_Merit_Negative'];
			}
			if($MeritNum > $ldiscipline->AwardPunish_MAX)
			{
				$error['MeritNum'] = $iDiscipline['Case_Student_Import_Quantity_OutRange'];
			}
		}
	}
	
    // if($error['Wrong_MeritType']=='')
	{
		//$ItemInfo = $ldiscipline->getAwardPunishItemByItemCode(addslashes($ItemCode));
		if(sizeof($ItemInfo) == 0)
		{
			$error['No_ItemInfo'] = $iDiscipline['Award_Punishment_No_ItemCode'];
		}
		else
		{
			$ItemText = $ItemInfo['ItemName'];
			$ItemID = $ItemInfo['ItemID'];
			$RecordType = $ItemInfo['RecordType'];

			if($MeritTypeNum != $RecordType)
			{
				$error['Wrong_MeritType'] = $iDiscipline['Award_Punishment_Wrong_Merit_Type'];
			}
			
			# Check user can access item or not
			//$CatID = $ldiscipline->returnCatIDByItemID($ItemID);
            if(!isset($item_cat_id_ary[$ItemID])) {
                $item_cat_id_ary[$ItemID] = $ldiscipline->returnCatIDByItemID($ItemID);
            }
            $CatID = $item_cat_id_ary[$ItemID];
			$AccessItem = 0;
			if($ldiscipline->IS_ADMIN_USER($UserID) || !$ItemID)
			{
				$AccessItem = 1;
			}
			else
			{
				// $ava_item = $ldiscipline->Retrieve_User_Can_Access_AP_Category_Item($UserID, 'item');
				if($ava_item[$CatID]) {
					$AccessItem = in_array($ItemID, $ava_item[$CatID]);
                }
			}
			
			if(!$AccessItem)
			{
				$error['No_Item_Access_Right'] = $iDiscipline['AP_No_Access_Right'];
			}
		}
	}

	### Checking - valid class & class number (student found)
	//$classID = $ldiscipline->getClassIDByClassName($ClassName, Get_Current_Academic_Year_ID());
    if(!isset($class_id_ary[$ClassName])) {
        $class_id_ary[$ClassName] = $ldiscipline->getClassIDByClassName($ClassName, Get_Current_Academic_Year_ID());
    }
    $classID = $class_id_ary[$ClassName];
    $StudentID = $lu->returnUserIDByClassIDClassNumber($classID, $ClassNumber);
	
	$lu = new libuser($StudentID);
	$ClassInfo = $ClassName."-".$ClassNumber;
	$StudentName = ($intranet_session_language=='en') ? $lu->EnglishName : $lu->ChineseName;
	
	# Profile Merit Type
	if($error['Wrong_MeritType'] == '')
	{
		// include("$intranet_root/lang/lang.en.php");
        include_once("$intranet_root/lang/lang.en.php");
		
		$MeritRecordType = array();
		if($MeritTypeNum == 1)
		{
			//$MeritRecordType = $ldiscipline->returnValidMeritTypeFieldWithOrder(1);
            $MeritRecordType = $merit_record_type;
		}
		else
		{
			//$MeritRecordType = $ldiscipline->returnValidMeritTypeFieldWithOrder(-1);
            $MeritRecordType = $demerit_record_type;
		}
		
		// [2017-0613-1311-05236]
		if(empty($MeritRecordType)) {
			$MeritRecordType[1] = array("-999", $i_general_na);
		}
		else {
			$MeritRecordType[] = array("-999", $i_general_na);
		}
		
		// include("$intranet_root/lang/lang.".$intranet_session_language.".php");
        include_once("$intranet_root/lang/lang.".$intranet_session_language.".php");

		$RecordType = '';
		for($a=1; $a<sizeof($MeritRecordType)+1; $a++)
		{
			if($MeritType == $MeritRecordType[$a][0] && strlen($MeritType) == strlen($MeritRecordType[$a][0]))
			{
				$RecordType = $MeritRecordType[$a][0];
				$MeritTypeName = $MeritRecordType[$a][1];
			}
		}
		if(trim($RecordType) == '')
		{
			$error['MeritType']=$iDiscipline['Award_Punishment_Wrong_Merit_Type'];
		}
	}	

	//if(!$error['Wrong_MeritType'])
	{
		# Conduct Score Checking
		if(trim($ConductScore) == '')
		{
			$error['ConductScore'] = $iDiscipline['Good_Conduct_Missing_Conduct_Score'];
		}
		else
		{
			if($MeritTypeNum != "")
			{
				if($MeritTypeNum == -1 && $ConductScore > 0)
				{
					$error['ConductScore'] = $iDiscipline['Case_Student_Import_Conduct_Score_Negative'];
				}
				if($MeritTypeNum == 1 && $ConductScore < 0)
				{
					$error['ConductScore'] = $iDiscipline['Case_Student_Import_Conduct_Score_Positive'];
				}
				if(!$sys_custom['eDiscipline']['ConductMark1DecimalPlace']) {
					$IsFloat = (strpos($ConductScore,"."));
				} 
				if(!is_numeric($ConductScore) || $IsFloat || abs($ConductScore) > $ldiscipline->ConductMarkIncrement_MAX) {
					$error['ConductScore'] = $iDiscipline['Case_Student_Import_Conduct_Score_OutRange'];
				}
				//$ConductScoreChange = $ConductScore * $MeritTypeNum;
				$ConductScoreChange = $ConductScore;
			}
			if($ConductScoreChange > $ldiscipline->ConductMarkIncrement_MAX)
			{
				$error['ConductScore'] = $iDiscipline['Case_Student_Import_Conduct_Score_OutRange'];
			}
		}

		if($ldiscipline->UseSubScore)
		{ 
			# Study Score Checking
			if(trim($SubScore) == '')
			{
				$error['SubScore'] = $Lang['eDiscipline']['MissingStudyScore'];
			}
			else
			{
				if($MeritTypeNum != "")
				{
					if($MeritTypeNum == -1 && $SubScore > 0)
					{
						$error['SubScore'] = $Lang['eDiscipline']['StudyScoreShouldBeNegative'];
					}
					if($MeritTypeNum == 1 && $SubScore < 0)
					{
						$error['SubScore'] = $Lang['eDiscipline']['StudyScoreShouldBePositive'];
					}

					$IsFloat = (strpos($SubScore,"."));
					if(!is_numeric($SubScore) || $IsFloat || abs($SubScore) > $ldiscipline->SubScoreIncrement_MAX)
					{
						$error['SubScore'] = $Lang['eDiscipline']['StudyScoreOutOfRange'];
					}

					// [2016-1125-0939-00240]
					if($sys_custom['eDiscipline']['ApplyMaxStudyScore'])
					{
						$currentItemID = $ItemInfo['ItemID'];
						$MaxSubScoreChange = $item_mapping[$currentItemID]['MaxGainDeductSubScore1'];
						if($MaxSubScoreChange > 0 && abs($SubScore) > $MaxSubScoreChange)
						{
							$error['SubScore'] = str_replace("<!--MAX_SCORE-->", $MaxSubScoreChange, $Lang['eDiscipline']['StudentExceedMeritItemMaxStudyScore']);
						}
					}
					$SubScoreChange = $SubScore;
				}
				if($SubScoreChange > $ldiscipline->SubScoreIncrement_MAX)
				{
					$error['SubScore'] = $Lang['eDiscipline']['StudyScoreOutOfRange'];
				}
			}
		}

		if($ldiscipline->UseActScore)
		{
			# Activity Score Checking
			if(trim($ActScore) == '')
			{
				$error['ActScore'] = $Lang['eDiscipline']['MissingActivityScore'];
			}
			else
			{
				if($MeritTypeNum != "")
				{
					if($MeritTypeNum == -1 && $ActScore > 0)
					{
						$error['ActScore'] = $Lang['eDiscipline']['ActivityScoreShouldBeNegative'];
					}
					if($MeritTypeNum == 1 && $ActScore < 0)
					{
						$error['ActScore'] = $Lang['eDiscipline']['ActivityScoreShouldBePositive'];
					}
					$IsFloat = (strpos($ActScore, "."));
					if(!is_numeric($ActScore) || $IsFloat || abs($ActScore) > $ldiscipline->ActScoreIncrement_MAX)
					{
						$error['ActScore'] = $Lang['eDiscipline']['ActivityScoreOutOfRange'];
					}
					$ActScoreChange = $ActScore;
				}
				if($ActScoreChange > $ldiscipline->ActScoreIncrement_MAX)
				{
					$error['ActScore'] = $Lang['eDiscipline']['ActivityScoreOutOfRange'];
				}
			}
		}
	}
	
	# Remark
	$Remark = intranet_htmlspecialchars(addslashes($Remark));

    # [2020-0824-1151-40308] Follow-up Remark
    $FollowUpRemark = intranet_htmlspecialchars(addslashes($FollowUpRemark));
	
	# PIC
	$PicName = array();
	$PicArr = explode(",",$PIC);
	for($ct=0; $ct<sizeof($PicArr); $ct++)
	{
        //$sql = "SELECT UserID as PICID, ".getNamefieldByLang()." as Name FROM INTRANET_USER WHERE UserLogin = '".$PicArr[$ct]."' AND RecordType = 1";
        //$tmpResult = $lu->returnArray($sql);
	    if(!isset($pic_info_ary[$PicArr[$ct]])) {
            $sql = "SELECT UserID as PICID, ".getNamefieldByLang()." as Name FROM INTRANET_USER WHERE UserLogin = '".$PicArr[$ct]."' AND RecordType = 1";
            $pic_info_ary[$PicArr[$ct]] = $lu->returnArray($sql);
        }
        $tmpResult = $pic_info_ary[$PicArr[$ct]];
		$PicID = $tmpResult[0]['PICID'];	
		
		if($ldiscipline->OnlyAdminSelectPIC && !$ldiscipline->IS_ADMIN_USER($UserID))
		{
			if($PicID != $UserID) {
				$error['PIC'] = $Lang['eDiscipline']['OnlyAdminSelectPIC'];
            }
		}
		else if(trim($PicID) == "")
		{
			$error['PIC'] = $iDiscipline['Good_Conduct_No_Staff'];
		}
		
		$PicIDArr[] = $PicID;
		$PicName[] = $tmpResult[0]['Name'];
	}

	if(trim($StudentID) == "")
	{
		$error['No_Student'] = $iDiscipline['Good_Conduct_No_Student'];
	}
	
	# (START) Check whether same item already added to student on same day (if $ldiscipline->NotAllowSameItemInSameDay = 1)
	if($ldiscipline->NotAllowSameItemInSameDay)     # Not allow same student with same item in a same day
    {
		$isExist = array();
		$hiddenField = "";
		$success = 1;
	
		$thisItemID = $ItemID;
		$thisRecordDate = $RecordDate;
		$isExist[$StudentID] = $ldiscipline->checkSameAPItemInSameDay($StudentID, $thisItemID, $thisRecordDate);
		if($isExist[$StudentID] == 1) {
			$error['duplicateOnSameDay'] = $Lang['eDiscipline']['ItemAlreadyAdded'];
        }
	}
	# (END) check whether same item already added to student on same day (if $ldiscipline->NotAllowSameItemInSameDay = 1)
	
	if($sys_custom['eDiscipline']['yy3'])
	{
		$overflowMarkInfoAry = $ldiscipline->GetMeritItemTagConductOverflowScoreStudents($StudentID, $year_info[0], $year_info[2], $ItemID, $RecordType, abs($ConductScore));
		if($sys_custom['eDiscipline']['yy3']) {
			if(in_array($StudentID,$overflowMarkInfoAry['MeritScoreOverflowStudentID'])){
				$warning['MeritScoreExceed'] = $Lang['eDiscipline']['WarningMsgArr']['MeritScoreExceeded'];
			}
			if(in_array($StudentID,$overflowMarkInfoAry['ConductScoreOverflowStudentID'])){
				$warning['ConductScoreExceed'] = $Lang['eDiscipline']['WarningMsgArr']['SemesterConductScoreExceeded'];
			}
			if(in_array($StudentID,$overflowMarkInfoAry['TagScoreOverflowStudentID']) && isset($overflowMarkInfoAry['TagScoreOverflowDetail'][$StudentID])){
				foreach($overflowMarkInfoAry['TagScoreOverflowDetail'][$StudentID] as $tag_id => $tag_detail) {
					$warning['TagScoreExceed_'.$tag_id] = str_replace("<!--TAG_NAME-->",$tag_detail['TagName'],$Lang['eDiscipline']['WarningMsgArr']['TagScoreExceeded']);
				}
			}
		}
	}

	$css = (sizeof($error)==0) ? "tabletext" : "red";

	$x .= "<tr class=\"tablebluerow".($i%2+1)."\">";
	$x .= "<td class=\"$css\">".($i+1)."</td>";
	$x .= "<td class=\"$css\">". $ClassInfo ."</td>";
	$x .= "<td class=\"$css\">". $StudentName ."</td>";
	$x .= "<td class=\"$css\">". $oriRecordDate ."</td>";
	if($sys_custom['eDiscipline']['yy3']) {
		$x .= "<td class=\"$css\">". $oriRecordInputDate ."</td>";
	}
	$x .= "<td class=\"$css\">". $Semester ."</td>";
	$x .= "<td class=\"$css\">". (($MeritTypeNum==1)?$eDiscipline["AwardRecord"]:$i_Discipline_System_Award_Punishment_Punishments)."</td>";
	//$x .= "<td class=\"$css\">". $RecordCat ."</td>";
	$x .= "<td class=\"$css\">". intranet_htmlspecialchars(($ItemText)) ."</td>";
	if ($ldiscipline->use_subject) {
		$x .= "<td class=\"$css\">". $SubjectName ."</td>";
	}
	$x .= "<td class=\"$css\">". $ConductScoreChange ."</td>";
	if($ldiscipline->UseSubScore) {
		$x .= "<td class=\"$css\">". $SubScoreChange ."</td>";
	}
	if($ldiscipline->UseActScore) {
		$x .= "<td class=\"$css\">". $ActScoreChange ."</td>";
	}

	$receive = $ldiscipline->returnDeMeritStringWithNumber($MeritNum, $MeritTypeName);
	$x .= "<td class=\"$css\">". $receive."</td>";
	$x .= "<td class=\"$css\">". implode(",",$PicName) ."</td>";
    //$x .= "<td class=\"$css\">". stripslashes($Remark) ."</td>";
	$x .= "<td class=\"$css\">". nl2br(stripslashes($Remark)) ."</td>";
	// [2020-0824-1151-40308]
    if($sys_custom['eDiscipline']['APFollowUpRemarks']){
        $x .= "<td class=\"$css\">". nl2br(stripslashes($FollowUpRemark)) ."</td>";
    }
	if($sys_custom['eDisciplinev12']['CheungShaWanCatholic']['AP_eNoticeChecked']) {
		$x .= "<td class=\"$css\">$sendNotice</td>";
    }
	if($sys_custom['eDiscipline']['MST']['AP_Extra_Fields']) {
		$x .= "<td class=\"$css\">". stripslashes($FileNumber) ."</td>";
		$x .= "<td class=\"$css\">". stripslashes($RecordNumber) ."</td>";
	}
	
	$x .= "<td class=\"$css\">";
	if(sizeof($error) > 0)
	{
		foreach($error as $Key => $Value)
		{
			$x .= $Value.'<br/>';
		}	
	}
	else
	{	
		$x .= "--";
	}
	$x .= "</td>";

	if($sys_custom['eDiscipline']['yy3'])
	{
		$x .= "<td class=\"".(count($warning)>0?"red":"tabletext")."\">";
			if(count($warning) == 0) {
				$x .= "--";
			}
			else {
				foreach($warning as $warning_msg) {
					$warning_occured++;
					$x .= $warning_msg."<br />";
				}
			}
		$x .= "</td>";
	}
	
	$x .= "</tr>";

	// $categoryName = $ldiscipline->getAwardPunishCategoryName($ldiscipline->getCategoryIDFromItemID($ItemID));
    if(!isset($category_name_ary[$ItemID])) {
        $category_name_ary[$ItemID] = $ldiscipline->getAwardPunishCategoryName($ldiscipline->getCategoryIDFromItemID($ItemID));
    }
    $categoryName = $category_name_ary[$ItemID];

	//$sql = "insert into temp_profile_student_merit_import values ('$ClassName','$ClassNumber','$RecordDate','$Year','$Semester' ,'$StudentID','$ItemID','".addslashes($ItemText)."','".addslashes($ItemCode)."','$MeritTypeNum','".addslashes($Remark)."','$RecordType
	//','$MeritNum','$ConductScoreChange','".addslashes(addslashes($SubjectName))."','".implode(',',$PicIDArr)."','".implode(",",$PicName)."',$MeritNum,'".addslashes($ProfileMeritType)."','".addslashes($MeritType)."','".addslashes($StudentName)."','".addslashes($MeritTypeName)."', '". $_SESSION["UserID"] ."')";

    /*
	if($sys_custom['eDiscipline']['yy3'])
	{
		// [DM#2895] remove addslashes() from $Remark
		//$sql = "insert into temp_profile_student_merit_import(ClassName, ClassNumber, RecordDate, RecordInputDate, Year, Semester, StudentID, ItemID, ItemText, ItemCode, MeritType, Remark, ProfileMeritType, ProfileMeritCount, ConductScoreChange, SubScoreChange, Subject, PICID, Teacher, Count, Category, Type, MeritTypeName,StudentName, UserID, SendNotice) values ('$ClassName','$ClassNumber','$RecordDate','$RecordInputDate','$Year','".addslashes($Semester)."' ,'$StudentID','$ItemID','".addslashes($ItemText)."','".addslashes($ItemCode)."','$MeritTypeNum','".addslashes($Remark)."','$RecordType','$MeritNum','$ConductScoreChange','$SubScoreChange','".addslashes(addslashes($SubjectName))."','".implode(',',$PicIDArr)."','".implode(",",$PicName)."','$MeritNum','".addslashes($categoryName)."','".addslashes($ProfileMeritType)."','".addslashes($MeritType)."','".addslashes($StudentName)."','". $_SESSION["UserID"] ."', '$sendNotice')";
		$sql = "INSERT INTO temp_profile_student_merit_import(ClassName, ClassNumber, RecordDate, RecordInputDate, Year, Semester, StudentID, ItemID, ItemText, ItemCode, MeritType, Remark, ProfileMeritType, ProfileMeritCount, ConductScoreChange, SubScoreChange, ActScoreChange, Subject, PICID, Teacher, Count, Category, Type, MeritTypeName, StudentName, UserID, SendNotice) VALUES ('$ClassName','$ClassNumber','$RecordDate','$RecordInputDate','$Year','".addslashes($Semester)."' ,'$StudentID','$ItemID','".addslashes($ItemText)."','".addslashes($ItemCode)."','$MeritTypeNum','".$Remark."','$RecordType','$MeritNum','$ConductScoreChange','$SubScoreChange','$ActScoreChange','".addslashes(addslashes($SubjectName))."','".implode(',',$PicIDArr)."','".implode(",",$PicName)."','$MeritNum','".addslashes($categoryName)."','".addslashes($ProfileMeritType)."','".addslashes($MeritType)."','".addslashes($StudentName)."','". $_SESSION["UserID"] ."', '$sendNotice')";
	}
	else if($sys_custom['eDiscipline']['MST']['AP_Extra_Fields'])
	{
		// [DM#2895] remove addslashes() from $Remark
		//$sql = "insert into temp_profile_student_merit_import(ClassName, ClassNumber, RecordDate, Year, Semester, StudentID, ItemID, ItemText, ItemCode, MeritType, Remark, ProfileMeritType, ProfileMeritCount, ConductScoreChange, SubScoreChange, Subject, PICID, Teacher, Count, Category, Type, MeritTypeName,StudentName, UserID, SendNotice, FileNumber, RecordNumber) values ('$ClassName','$ClassNumber','$RecordDate','$Year','".addslashes($Semester)."' ,'$StudentID','$ItemID','".addslashes($ItemText)."','".addslashes($ItemCode)."','$MeritTypeNum','".addslashes($Remark)."','$RecordType','$MeritNum','$ConductScoreChange','$SubScoreChange','".addslashes(addslashes($SubjectName))."','".implode(',',$PicIDArr)."','".implode(",",$PicName)."','$MeritNum','".addslashes($categoryName)."','".addslashes($ProfileMeritType)."','".addslashes($MeritType)."','".addslashes($StudentName)."','". $_SESSION["UserID"] ."', '$sendNotice', '". addslashes($FileNumber)."', '". addslashes($RecordNumber)."')";
		$sql = "INSERT INTO temp_profile_student_merit_import(ClassName, ClassNumber, RecordDate, Year, Semester, StudentID, ItemID, ItemText, ItemCode, MeritType, Remark, ProfileMeritType, ProfileMeritCount, ConductScoreChange, SubScoreChange, ActScoreChange, Subject, PICID, Teacher, Count, Category, Type, MeritTypeName, StudentName, UserID, SendNotice, FileNumber, RecordNumber) VALUES ('$ClassName','$ClassNumber','$RecordDate','$Year','".addslashes($Semester)."' ,'$StudentID','$ItemID','".addslashes($ItemText)."','".addslashes($ItemCode)."','$MeritTypeNum','".$Remark."','$RecordType','$MeritNum','$ConductScoreChange','$SubScoreChange','$ActScoreChange','".addslashes(addslashes($SubjectName))."','".implode(',',$PicIDArr)."','".implode(",",$PicName)."','$MeritNum','".addslashes($categoryName)."','".addslashes($ProfileMeritType)."','".addslashes($MeritType)."','".addslashes($StudentName)."','". $_SESSION["UserID"] ."', '$sendNotice', '". addslashes($FileNumber)."', '". addslashes($RecordNumber)."')";
	}
	else
	{
		// [DM#2895] remove addslashes() from $Remark
		//$sql = "insert into temp_profile_student_merit_import(ClassName, ClassNumber, RecordDate, Year, Semester, StudentID, ItemID, ItemText, ItemCode, MeritType, Remark, ProfileMeritType, ProfileMeritCount, ConductScoreChange, SubScoreChange, Subject, PICID, Teacher, Count, Category, Type, MeritTypeName,StudentName, UserID, SendNotice) values ('$ClassName','$ClassNumber','$RecordDate','$Year','".addslashes($Semester)."' ,'$StudentID','$ItemID','".addslashes($ItemText)."','".addslashes($ItemCode)."','$MeritTypeNum','".addslashes($Remark)."','$RecordType','$MeritNum','$ConductScoreChange','$SubScoreChange','".addslashes(addslashes($SubjectName))."','".implode(',',$PicIDArr)."','".implode(",",$PicName)."','$MeritNum','".addslashes($categoryName)."','".addslashes($ProfileMeritType)."','".addslashes($MeritType)."','".addslashes($StudentName)."','". $_SESSION["UserID"] ."', '$sendNotice')";
		$sql = "INSERT INTO temp_profile_student_merit_import(ClassName, ClassNumber, RecordDate, Year, Semester, StudentID, ItemID, ItemText, ItemCode, MeritType, Remark, ProfileMeritType, ProfileMeritCount, ConductScoreChange, SubScoreChange, ActScoreChange, Subject, PICID, Teacher, Count, Category, Type, MeritTypeName, StudentName, UserID, SendNotice) VALUES ('$ClassName','$ClassNumber','$RecordDate','$Year','".addslashes($Semester)."' ,'$StudentID','$ItemID','".addslashes($ItemText)."','".addslashes($ItemCode)."','$MeritTypeNum','".$Remark."','$RecordType','$MeritNum','$ConductScoreChange','$SubScoreChange','$ActScoreChange','".addslashes(addslashes($SubjectName))."','".implode(',',$PicIDArr)."','".implode(",",$PicName)."','$MeritNum','".addslashes($categoryName)."','".addslashes($ProfileMeritType)."','".addslashes($MeritType)."','".addslashes($StudentName)."','". $_SESSION["UserID"] ."', '$sendNotice')";
	}
	$ldiscipline->db_db_query($sql) or die(mysql_error());
    */

    $import_index = floor($i / 50);
    if(!isset($import_data[$import_index])) {
        $import_data[$import_index] = array();
    }

    $data_str = "";
    if($sys_custom['eDiscipline']['yy3'])
    {
        $data_str = " ('$ClassName','$ClassNumber','$RecordDate','$RecordInputDate','$Year','".addslashes($Semester)."','$StudentID','$ItemID','".addslashes($ItemText)."','".addslashes($ItemCode)."','$MeritTypeNum','".$Remark."','$RecordType','$MeritNum','$ConductScoreChange','$SubScoreChange','$ActScoreChange','".addslashes(addslashes($SubjectName))."','".implode(',',$PicIDArr)."','".implode(",",$PicName)."','$MeritNum','".addslashes($categoryName)."','".addslashes($ProfileMeritType)."','".addslashes($MeritType)."','".addslashes($StudentName)."','".$_SESSION["UserID"]."','$sendNotice') ";
    }
    else if($sys_custom['eDiscipline']['MST']['AP_Extra_Fields'])
    {
        $data_str = " ('$ClassName','$ClassNumber','$RecordDate','$Year','".addslashes($Semester)."','$StudentID','$ItemID','".addslashes($ItemText)."','".addslashes($ItemCode)."','$MeritTypeNum','".$Remark."','$RecordType','$MeritNum','$ConductScoreChange','$SubScoreChange','$ActScoreChange','".addslashes(addslashes($SubjectName))."','".implode(',',$PicIDArr)."','".implode(",",$PicName)."','$MeritNum','".addslashes($categoryName)."','".addslashes($ProfileMeritType)."','".addslashes($MeritType)."','".addslashes($StudentName)."','".$_SESSION["UserID"]."','$sendNotice','".addslashes($FileNumber)."','".addslashes($RecordNumber)."') ";
    }
    else
    {
        // [2020-0824-1151-40308]
        //$data_str = " ('$ClassName','$ClassNumber','$RecordDate','$Year','".addslashes($Semester)."','$StudentID','$ItemID','".addslashes($ItemText)."','".addslashes($ItemCode)."','$MeritTypeNum','".$Remark."','$RecordType','$MeritNum','$ConductScoreChange','$SubScoreChange','$ActScoreChange','".addslashes(addslashes($SubjectName))."','".implode(',',$PicIDArr)."','".implode(",",$PicName)."','$MeritNum','".addslashes($categoryName)."','".addslashes($ProfileMeritType)."','".addslashes($MeritType)."','".addslashes($StudentName)."','".$_SESSION["UserID"]."','$sendNotice')";
        $data_str = " ('$ClassName','$ClassNumber','$RecordDate','$Year','".addslashes($Semester)."','$StudentID','$ItemID','".addslashes($ItemText)."','".addslashes($ItemCode)."','$MeritTypeNum','".$Remark."','$RecordType','$MeritNum','$ConductScoreChange','$SubScoreChange','$ActScoreChange','".addslashes(addslashes($SubjectName))."','".implode(',',$PicIDArr)."','".implode(",",$PicName)."','$MeritNum','".addslashes($categoryName)."','".addslashes($ProfileMeritType)."','".addslashes($MeritType)."','".addslashes($StudentName)."','".$_SESSION["UserID"]."','$sendNotice','".$FollowUpRemark."')";
    }
    $import_data[$import_index][] = $data_str;

    if($error)
    {
        $error_occured++;
    }
}
$x .= "</table>";

if(!empty($import_data))
{
    if($sys_custom['eDiscipline']['yy3'])
    {
        $sql = " INSERT INTO temp_profile_student_merit_import (ClassName, ClassNumber, RecordDate, RecordInputDate, Year, Semester, StudentID, ItemID, ItemText, ItemCode, MeritType, Remark, ProfileMeritType, ProfileMeritCount, ConductScoreChange, SubScoreChange, ActScoreChange, Subject, PICID, Teacher, Count, Category, Type, MeritTypeName, StudentName, UserID, SendNotice) ";
    }
    else if($sys_custom['eDiscipline']['MST']['AP_Extra_Fields'])
    {
        $sql = " INSERT INTO temp_profile_student_merit_import (ClassName, ClassNumber, RecordDate, Year, Semester, StudentID, ItemID, ItemText, ItemCode, MeritType, Remark, ProfileMeritType, ProfileMeritCount, ConductScoreChange, SubScoreChange, ActScoreChange, Subject, PICID, Teacher, Count, Category, Type, MeritTypeName, StudentName, UserID, SendNotice, FileNumber, RecordNumber) ";
    }
    else
    {
        // [2020-0824-1151-40308]
        //$sql = " INSERT INTO temp_profile_student_merit_import (ClassName, ClassNumber, RecordDate, Year, Semester, StudentID, ItemID, ItemText, ItemCode, MeritType, Remark, ProfileMeritType, ProfileMeritCount, ConductScoreChange, SubScoreChange, ActScoreChange, Subject, PICID, Teacher, Count, Category, Type, MeritTypeName, StudentName, UserID, SendNotice)  ";
        $sql = " INSERT INTO temp_profile_student_merit_import (ClassName, ClassNumber, RecordDate, Year, Semester, StudentID, ItemID, ItemText, ItemCode, MeritType, Remark, ProfileMeritType, ProfileMeritCount, ConductScoreChange, SubScoreChange, ActScoreChange, Subject, PICID, Teacher, Count, Category, Type, MeritTypeName, StudentName, UserID, SendNotice, FollowUpRemark) ";
    }

    foreach((array)$import_data as $this_data) {
        $this_sql = $sql." VALUES ".implode(",", (array)$this_data);
        $ldiscipline->db_db_query($this_sql) or die(mysql_error());
    }
}

if($sys_custom['eDiscipline']['yy3'] && $warning_occured > 0) {
	$js_onsubmit = ' onsubmit="submitConfirm(this);return false;"';
}

if($error_occured)
{
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import.php'");
}
else
{	
	$import_button = $linterface->GET_ACTION_BTN($button_import, "submit")." &nbsp;".$linterface->GET_ACTION_BTN($button_back, "button", "window.location='import.php'");	
}
?>

<?php if($sys_custom['eDiscipline']['yy3'] && $warning_occured > 0){  ?>
<script type="text/javascript" language="JavaScript">
function submitConfirm(obj)
{
	if(confirm('<?=$Lang['eDiscipline']['WarningMsgArr']['StudentExceedConductMarkLimitConfirmSubmit']?>')) {
		obj.submit();
	}
}
</script>	
<?php } ?>

<br />
<form name="form1" method="post" action="import_update.php" enctype="multipart/form-data"<?=$js_onsubmit?>>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center">
		<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2"><?=$x?></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			    <?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>