<?php
# using:  

########## Change Log ###############
#
#   Date    :   2020-11-06  (Bill)  [2020-0824-1151-40308]
#               - Support Follow-up Feedback    ($sys_custom['eDiscipline']['APFollowUpRemarks'])
#
#   Date    :   2019-01-14  (Bill)  [2018-0417-1222-44170]
#               added options to send push message
#
#	Date	:	2017-03-16	(Bill)	[2016-1207-1221-39240]
#				support Activity Score display
#
#	Date	:	2016-07-08 (Bill)	[2016-0329-1757-08164]	
#				display option to allow cancel probation
#
#	Date    :	2015-04-30	Bill	[2014-1216-1347-28164]
#			    add "Send Push Message" in Action
#
#	Date    :	2013-06-24	YatWoon
#			    improved: disable the submit button prevent multiple clicks [Case#2013-0605-1717-42156]
#
#	Date	:	2013-06-21 (Carlos)
#				$sys_custom['eDiscipline']['yy3'] - Display conduct score change with masked symbol for those overflow score merit items 
#
#	Date	:	2012-02-13 (Henry Chow)
#	Detail 	:	disable "Submit" button on form submission  (2013-06-24: it's not work!)
#
#	Date    :	2010-10-18	Henry Chow
#			    add flag $sys_custom['EmailToDefaultlySelected_In_AP_Step4'] to control the default selection of "Email to" checkbox
#
#	Date    :	2010-08-26	Henry Chow
#			    display "Reference Number" (Munsang cust)
#
#	Date    :	2010-05-07	YatWoon
#			    display "record is auto approved"
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Award_Punishment-New");

if(sizeof($_POST)==0) {
	header("Location: index.php");
	exit;
}

if($sys_custom['eDiscipline']['yy3']){
	include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");
	$ldiscipline_ui = new libdisciplinev12_ui_cust();
}	

$linterface = new interface_html();

# Menu
$CurrentPage = "Management_AwardPunishment";
$CurrentPageArr['eDisciplinev12'] = 1;

# Left Menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Tag
$TAGS_OBJ[] = array($eDiscipline['Award_and_Punishment']);

# Step
$STEPS_OBJ[] = array($eDiscipline["SelectStudents"], 0);
$STEPS_OBJ[] = array($eDiscipline["AddRecordToStudents"], 0);
$STEPS_OBJ[] = array($eDiscipline["SelectActions"], 0);
$STEPS_OBJ[] = array($eDiscipline["FinishNotification"], 1);

# Navigation Bar
$PAGE_NAVIGATION[] = array($eDiscipline['RecordList'], "index.php");
$PAGE_NAVIGATION[] = array($eDiscipline["NewRecord"], "");

# build html part
$xmsg = sizeof($MeritRecordID) ? "add" : "add_failed";

$record_table = "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
$record_table .= "<tr class='tabletop'>";
$record_table .= "<td width='15' align='center'>#</td>";
$record_table .= "<td>". $i_general_class ."</td>";
$record_table .= "<td>". $i_general_name ."</td>";
$record_table .= "<td>&nbsp;</td>";
$record_table .= "<td>". $eDiscipline["Record"] ."</td>";
if($ldiscipline->Display_ConductMarkInAP) {
	$record_table .= "<td nowrap>". $eDiscipline['Conduct_Mark'] ."</td>\n";
}
if($ldiscipline->UseSubScore) {
	$record_table .= "<td nowrap>". $i_Discipline_System_Subscore1 ."</td>\n";
}
if($ldiscipline->UseActScore) {
	$record_table .= "<td nowrap>". $Lang['eDiscipline']['ActivityScore'] ."</td>\n";
}
$record_table .= "<td>". $eDiscipline["RecordItem"] ."</td>";
$record_table .= "<td width='100'>". $Lang['eDiscipline']['EventDate'] ."</td>";
$record_table .= "<td>". $i_Discipline_PIC."</td>";
$record_table .= "<td align='left'>". $eDiscipline["Action"] ."</td>";
if($sys_custom['eDisciplinev12_add_referenceNo'] || $sys_custom['eDisciplinev12']['CheungShaWanCatholic']['AP_ReferenceNo']) {
	$record_table .= "<td align='left'>". $iDiscipline["ReferenceNo"] ."</td>";
}
if($sys_custom['eDisciplinev12_display_grouped_record']) {
	$record_table .= "<td class=\"tabletoplink\" width='13%'>".$Lang['eDiscipline']['RecordGenerated']." [".$Lang['eBooking']['eService']['Ref']."]</td>";
}
	
$record_table .= "</tr>";

$i = 1;
foreach($MeritRecordID as $RecordID)
{
	$datainfo = $ldiscipline->RETRIEVE_MERIT_RECORD_INFO_BY_ID($RecordID);
	
	$thisStudentID = $datainfo[0]['StudentID'];
	/*
	$lu = new libuser($thisStudentID);
	$thisClass = $lu->ClassName . ($lu->ClassNumber ? " - ". $lu->ClassNumber: "");
	*/
	$stu = $ldiscipline->getStudentNameByID($thisStudentID);
	$student_name = $stu[0][1];
	$class_name = $stu[0][2];
	$class_no = $stu[0][3];
	$thisClass = $class_name . ($class_no ? " - ". $class_no: "");
	
	$thisicon = $datainfo[0]['MeritType']==1 ? "icon_merit" : "icon_demerit";
	$thisMeritType = $ldiscipline->RETURN_MERIT_NAME($datainfo[0]['ProfileMeritType']);
	$PIC = $datainfo[0]['PICID'];
	$namefield = getNameFieldWithLoginByLang();
	$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE RecordType = 1 AND UserID IN ($PIC) AND RecordStatus = 1 ORDER BY $namefield";
	$array_PIC = $ldiscipline->returnArray($sql);
	
	$PICAry = array();
	if(sizeof($array_PIC)==0) {
		$array_PIC[0] = $PIC;
	}
	foreach($array_PIC as $k=>$d) {
		$PICAry[] = $d[1];
	}
		
	$TemplateID = $datainfo[0]['TemplateID'];
	$action_str = ($TemplateID) ? $eDiscipline["SendNotice"] : "";
	
	// [2014-1216-1347-28164] show push message has been sent
	if($plugin['eClassApp'] && $sys_custom['eDiscipline']['SendDetentionPushMessage']) {
		$pushMsg_str = $datainfo[0]['PushMessageID']? $Lang['eDiscipline']['DetentionMgmt']['SendPushMessage'] : "";
		$action_str .= (($action_str && $pushMsg_str)? "<br>" :"") . $pushMsg_str;
	}

	# check have detention or not
	$DetentionInfo = $ldiscipline->getStudentDetentionCountByDemeritID($RecordID);
	//$detention_str = sizeof($DetentionInfo) ? $eDiscipline['Detention'] : "";
	$detention_str = ($DetentionInfo[0]) ? $eDiscipline['Detention'] : "";
	$action_str .= (($action_str && $detention_str)? "<br>" :"") . $detention_str;

    // [2020-0824-1151-40308]
    if ($sys_custom['eDiscipline']['APFollowUpRemarks']) {
        $follow_up_remark_str = trim($datainfo[0]['FollowUpRemark']) != '' ? $Lang['eDiscipline']['FollowUpRemark'] : "";
        $action_str .= (($action_str && $follow_up_remark_str) ? "<br>" : "") . $follow_up_remark_str;
    }
	
	//$ItemText = stripslashes(intranet_htmlspecialchars($datainfo[0]['ItemText']));
	//$ItemText = $datainfo[0]['ItemText'];
	$ItemText = intranet_htmlspecialchars($datainfo[0]['ItemText']);
 	
	$record_table .= "<tr class='row_approved'>";
	$record_table .= "<td valign='top' > ". ($i++) ."</td>";
	$record_table .= "<td valign='top'>". $thisClass ."</td>";
	$record_table .= "<td valign='top'>". $student_name ."</td>";
	$record_table .= "<td width='20' valign='top'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/{$thisicon}.gif' width='20' height='20' border='0' align='absmiddle'></td>";
	/*
	if($datainfo[0]['ProfileMeritCount'])
		$record_data = $datainfo[0]['ProfileMeritCount'] ." ". $thisMeritType;
	else
		$record_data = "(".$i_general_na.")";
	*/
	
	$record_data = $ldiscipline->returnDeMeritStringWithNumber($datainfo[0]['ProfileMeritCount'], $thisMeritType);
	$record_table .= "<td valign='top'>". $record_data ."</td>";
	if($ldiscipline->Display_ConductMarkInAP) {
		if($sys_custom['eDiscipline']['yy3'] && ($datainfo[0]['OverflowConductMark']=='1' || $datainfo[0]['OverflowMeritItemScore']=='1' || $datainfo[0]['OverflowTagScore']=='1')){
			$record_table .= "<td valign='top'>". $ldiscipline_ui->displayMaskedMark( $datainfo[0]['ConductScoreChange'] )."</td>";
		}
		else {
			$record_table .= "<td valign='top'>". $datainfo[0]['ConductScoreChange'] ."</td>";
		}
	}
	if($ldiscipline->UseSubScore) {
		$record_table .= "<td valign='top'>". $datainfo[0]['SubScore1Change'] ."</td>";
	}
	if($ldiscipline->UseActScore) {
		$record_table .= "<td valign='top'>". $datainfo[0]['SubScore2Change'] ."</td>";
	}
	
	$record_table .= "<td valign='top'>". $ItemText ."</td>";
	$record_table .= "<td valign='top'>". $datainfo[0]['RecordDate'] ."</td>";
	$record_table .= "<td valign='top'>";
	$record_table .= ($picType==1 || $picType=="") ? implode("<br>",$PICAry) : $PIC;
	$record_table .= "</td>";
	$record_table .= "<td align='left' valign='top'>". $action_str ."&nbsp;</td>";
	if($sys_custom['eDisciplinev12_add_referenceNo'] || $sys_custom['eDisciplinev12']['CheungShaWanCatholic']['AP_ReferenceNo']) {
		$ref_id = $ldiscipline->RETURN_REFERNCE_NUMBER("DISCIPLINE_MERIT_RECORD", $RecordID);
		$record_table .= "<td valign='top'>". $ref_id ."</td>";
	}
	if($sys_custom['eDisciplinev12_display_grouped_record']) {
		$ap_record = $ldiscipline->RETURN_GROUPED_MERIT_TYPE($RecordID);
		$record_table .= "<td valign='top'>".(($ap_record!="") ? $ap_record : $record_data)."</td>";
	}
	$record_table .= "</tr>";
}
$record_table .= "</table>";

# Status option
if($datainfo[0]['RecordStatus'] == DISCIPLINE_STATUS_APPROVED)
{
	if($datainfo[0]['ReleaseStatus'] == DISCIPLINE_STATUS_RELEASED)
	{
		$releasedImg = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_to_student.gif' width='20' height='20' align='absmiddle' \"". $i_Discipline_Released_To_Student."\" border='0'>";
		$status_option = $releasedImg . " ". $Lang['eDiscipline']['AutoReleasedToStudent'];
		$status_option .= "<input name='released' type='hidden' id='released' value='1'>";
	}
	else
	{
		if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Release")) {
			$status_option = "<input name='released' type='checkbox' id='released' value='1'><label for='released'>". $i_Discipline_System_Access_Right_Release ."</label>";
		}
	}
	
	$lu = new libuser($UserID);
	$icon = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_approve_b.gif' width='20' height='20' border='0' align='absmiddle'>";
// 	$status_str = $icon . " " . $eDiscipline["ApprovedBy"] . $lu->UserName(0) . $eDiscipline["ApprovedBy2"] . $data['ApprovedDate'] . $eDiscipline["ApprovedBy3"];
	$status_str = $icon . " " . $Lang['eDiscipline']['RecordisAutoApproved'];
}
else
{
	$hasRightToApproveRecordID = array();
	if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eDiscipline"]) {
		$hasRightToApproveRecordID = $ldiscipline->RETURN_AP_RECORDID_CAN_BE_APPROVED($UserID, "");
	}
    
	$useApprovalGroup = $ldiscipline->useApprovalGroup();
	if($useApprovalGroup && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eDiscipline"]) {
		$hasNoApprovalRight = (sizeof($hasRightToApproveRecordID)==0 || !in_array($MeritRecordID, $hasRightToApproveRecordID)) ? 1 : 0;
	}
	else {
		$hasNoApprovalRight = 0;
	}
	
	//if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Approval"))
	if(!$hasNoApprovalRight)  # has right to approve record
	{
		$status_option = "<input name='approved' type='checkbox' id='approved' value='1' onClick='check_checkbox(\"approved\")'><label for='approved'>". $i_status_approve ."</label>";
        
		if(!$ldiscipline->AutoReleaseForAPRecord)
		{
			if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Release")) {
				$status_option .= "<br> <input name='released' type='checkbox' id='released' value='1' onClick='check_checkbox(\"released\")'><label for='released'>". $eDiscipline["ApproveRelease"] ."</label>";
			}
		}
		
		// [2016-0329-1757-08164] Display option to approve current probation record
		if($sys_custom['eDiscipline']['CSCProbation'] && $datainfo[0]['MeritType']==-1 && ($datainfo[0]['ProfileMeritType']==-2 || $datainfo[0]['ProfileMeritType']==-3))
		{
			$status_option = "<input name='approved' type='checkbox' id='approved' value='1' onClick='check_checkbox(\"approved\")'><label for='approved'>". $Lang['eDiscipline']['CWCProbation']['CancelProbation'] ."</label>";
		}
		
		$icon = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif' width='20' height='20' border='0' align='absmiddle'>";
		$status_str = $icon . " " . $i_Discipline_System_Award_Punishment_Pending;
	}
	else
	{
		$icon = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif' width='20' height='20' border='0' align='absmiddle'>";
		$status_str = $icon . " " . $i_Discipline_System_Award_Punishment_Pending;
	}
}

if($sys_custom['EmailToDefaultlySelected_In_AP_Step4']) {
	$emailToChecked = "checked";
}

# Start layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
<!--
function check_checkbox(x)
{
	if(x=="approved")
	{
		document.form1.released.checked = false;
	}
	else
	{
		document.form1.approved.checked = false;
	}
}

function check_form() {
	//$("#submitBtn").attr("disabled", true);
	document.getElementById('submit_btn').disabled  = true; 
	document.getElementById('submit_btn').className  = "formbutton_disable_v30 print_hide"; 
	return true;
}
//-->
</script>

<form name="form1" method="POST" action="new4_update.php" onSubmit="return check_form()">
<br />

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td class="navigation">
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr><td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
		</table>
	</td>
</tr>
<tr>
	<td align="center"><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
</tr>

<tr>
	<td align='center'>
		<table align='center' width='90%' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td align='right'><?=$linterface->GET_SYS_MSG($xmsg)?></td>
			</tr>
		</table>
	</td>
</tr>

<tr>
	<td align='center'>
		<table align='center' width='90%' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td align='right'><?=$record_table?></td>
			</tr>
		</table>
	</td>
</tr>

<tr>
	<td>
		<table align="center" width="90%" border="0" cellpadding="5" cellspacing="0">
    		<tr>
    			<td colspan="2" valign="middle" nowrap="nowrap" class="form_sep_title"><i> - <?=$eDiscipline["Status"]?> -</i></td>
    		</tr>
    		<tr>
                <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">&nbsp;</td>
                <td><?=$status_str?></td>
    		</tr>
    		<tr>
                <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">&nbsp;</td>
                <td><?=$status_option?></td>
    		</tr>
    		<tr>
    			<td colspan="2" valign="middle" nowrap="nowrap" class="form_sep_title"><i> - <?=$eDiscipline["Notification"]?> -</i></td>
    		</tr>
    		<tr>
                <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eDiscipline["EmailTo"]?></td>
                <td>
    				<input name="email_PIC" type="checkbox" id="email_PIC" value="1" <?=$emailToChecked?>><label for="email_PIC"><?=$i_Discipline_PIC?></label>
    				<input name="email_ClassTeacher" type="checkbox" id="email_ClassTeacher" value="1" <?=$emailToChecked?>><label for="email_ClassTeacher"><?=$i_Teaching_ClassTeacher?></label>
    				<input name="email_DisciplineAdmin" type="checkbox" id="email_DisciplineAdmin" value="1" <?=$emailToChecked?>><label for="email_DisciplineAdmin"><?=$eDiscipline["DisciplineAdmin2"]?></label>
    			</td>
    		</tr>
    		
            <?php if($plugin['eClassApp'] && $plugin['eClassTeacherApp']) { ?>
        		<tr>
                    <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['AppNotifyMessage']['MessageToStaff']?></td>
                    <td>
        				<input name="pushmessagenotify_PIC" type="checkbox" id="pushmessagenotify_PIC" value="1"><label for="pushmessagenotify_PIC"><?=$i_Discipline_PIC?></label>
        				<input name="pushmessagenotify_ClassTeacher" type="checkbox" id="pushmessagenotify_ClassTeacher" value="1"><label for="pushmessagenotify_ClassTeacher"><?=$i_Teaching_ClassTeacher?></label>
        				<input name="pushmessagenotify_DisciplineAdmin" type="checkbox" id="pushmessagenotify_DisciplineAdmin" value="1"><label for="pushmessagenotify_DisciplineAdmin"><?=$eDiscipline["DisciplineAdmin2"]?></label>
        			</td>
        		</tr>
            <?php } ?>
            
		</table>
	</td>
</tr>

<tr>
	<td>
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
    		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
    		<tr>
    			<td align="center">
    				<?= $linterface->GET_ACTION_BTN($button_finish, "submit", "", "submit_btn")?>&nbsp;
    			</td>
    		</tr>
	</table>
	</td>
</tr>
</table>

<br />
<? foreach($MeritRecordID as $RecordID) {?>
	<input type="hidden" name="MeritRecordID[]" value="<?=$RecordID?>">
<? } ?>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>