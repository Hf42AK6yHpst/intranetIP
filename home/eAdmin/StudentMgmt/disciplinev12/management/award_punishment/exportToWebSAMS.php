<?php
# using: 

##################################################
#
#	Date:	2018-03-01 (Bill)	[2017-1206-0959-15236]
#			updated Text Format for Event Code Column
#
#	Date:	2017-10-10	Bill	[2017-1009-1012-34206]
#			support new Award and Punishment record Specification
#
#	Date:	2017-06-28	Carlos	- Applied MeritType filter.
#
#	Date:	2015-12-30	Bill	[2015-0416-1040-06164]
#			only export self created records for Student Prefect - HKUGA Cust
#
#	Date:	2015-11-20	Kenneth		[2015-1117-1439-24071]
#			adding alternative of export format : xls
#
#	Date:	2014-12-09	Bill
#			add filtering - Year, Semester, Form/Class and PICs
#
#	Date:	2013-03-01	YatWoon
#			add date range checking for websSAMS export [Case#2013-0228-1113-49073]
#
##################################################

@ini_set('zend.ze1_compatibility_mode', '0');

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

// if(!$sys_custom['eDis_AP_export_WEBSAMS_format']) {
// 	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
// 	exit;
// }

$lexport = new libexporttext();

$filename = "award_punishment_websams_format.csv";

/*
if($dateChoice==1) {	# by Year / Semester
	if($semester == "0") $semester = "";
	$startDate = substr(getStartDateOfAcademicYear($SchoolYear, $semester),0,10);
	$endDate = substr(getEndDateOfAcademicYear($SchoolYear, $semester),0,10);
} else {
	// do nothing
}
*/

# Filtering
# Year
$SchoolYear = ($SchoolYear == "") ? Get_Current_Academic_Year_ID() : $SchoolYear;
if($SchoolYear != "" && $SchoolYear != 0) {
	$conds .= " AND a.AcademicYearID=$SchoolYear";	
}
# Semester
if($semester != "" && $semester != "WholeYear") {
	$conds .= " AND a.YearTermID = $semester";
}
# Form / Year Class
if ($targetClass != "" && $targetClass!="0")
{
	# Form
    if(is_numeric($targetClass)) {
    	$sql = "SELECT YearClassID FROM YEAR_CLASS WHERE YearID='$targetClass'";
    	$temp = $ldiscipline->returnVector($sql);
    	
    	# Condition: Year Class in current year
    	$conds .= (sizeof($temp)>0) ? " AND g.YearClassID IN (".implode(",", $temp).") AND g.AcademicYearID='".Get_Current_Academic_Year_ID()."' " : ""; 
	}
	# Year Class
    else {
    	$conds .= " AND g.YearClassID=".substr($targetClass,2);
    }
}

# Merit Type
if($MeritType != "" && in_array($MeritType, array(1,-1)))
{
	$conds .= " AND a.MeritType='$MeritType' ";
}

# PICs
if($pic != "") {
	$conds .= " AND concat(',',a.PICID,',') LIKE '%,$pic,%'";
}

// [2015-0416-1040-06164] Student Prefect can export self-created records only
if($sys_custom['eDiscipline']['add_AP_records_student_prefect'] && $ldiscipline->isStudentPrefect()){
	$conds .= " AND a.CreatedBy = '$UserID'";
}

# Check with Date Setting
//$sql = "select RecordID from DISCIPLINE_MERIT_RECORD where RecordDate BETWEEN '$sDate' AND '$eDate'";

# Get record with filtering
$sql = "SELECT 
			a.RecordID 
		FROM DISCIPLINE_MERIT_RECORD as a
			LEFT OUTER JOIN INTRANET_USER as b ON (a.StudentID = b.UserID)
			LEFT OUTER JOIN YEAR_CLASS_USER as f ON (b.UserID=f.UserID)
			LEFT OUTER JOIN YEAR_CLASS as g ON (f.YearClassID=g.YearClassID)
		WHERE a.RecordDate BETWEEN '$sDate' AND '$eDate' $conds";
$result = $ldiscipline->returnVector($sql);
$recordId = sizeof($result)>0 ? implode(",", $result) : "";

# Get Export Content
/*
 *  $isWebSAMSFormatV2 : Use new WebSAMS Excel Format
 *  $targetWebSAMSMode : Conduct - WebSAMS System using Conduct Mark Scheme     (Column : LVL1 - LVL5 > empty)
 *                       Merit   - WebSAMS System using Merit / Demerit System  (Column : CONDUCT MARK > empty)
 *  $targetMeritFields : Name    - Event Description for mapping in WebSAMS     (Column : DESCRIPTION (Eng))
 *                       Code    - Event Code for mapping in WebSAMS            (Column : EVENT CODE)
 */
$targetMeritFields = $sys_custom['eDiscipline']['ExportWebSAMS_ExportCode'] ? 'Code' : $targetMeritFields;
//$exportContent = $ldiscipline->Get_AP_Interface_File($RecordIdForExportWebSAMS);
//$export_content = $lexport->GET_EXPORT_TXT($exportContent, $exportColumn);
$exportContent = $ldiscipline->Get_AP_Interface_File($recordId, $isWebSAMSFormatV2=true, $targetWebSAMSMode, $targetMeritFields);
$export_content .= stripslashes($exportContent);

// [2015-1117-1439-24071] Export data in xls format
if($sys_custom['eDiscipline']['ExportWebSAMAS3'])
{
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	/** PHPExcel_IOFactory */
	include_once($PATH_WRT_ROOT."includes/phpxls/Classes/PHPExcel/IOFactory.php");
	include_once($PATH_WRT_ROOT."includes/phpxls/Classes/PHPExcel.php");
	
	$libfs = new libfilesystem();

	## Prepare xls and zip file folder
	$TempFolder = $intranet_root."/file/temp/award_punishment/export";
	$TempUserFolder = $TempFolder."/".$_SESSION["UserID"];
	$TempCsvFolder = $TempUserFolder."/to_websams";
	
//	$ZipFileName = "to_websams.zip";
//	$ZipFilePath = $TempUserFolder.'/'.$ZipFileName;
	
	if (!file_exists($TempCsvFolder)) {
		$SuccessArr["CreateTempCsvFolder"] = $libfs->folder_new($TempCsvFolder);
	}
	else {
		// Omas - Clear Temp files from last session
		$fileList = array_diff(scandir($TempCsvFolder), array("..","."));
		if(count($fileList) > 0)
		{
			foreach((array)$fileList as $filename) {
				$libfs->file_remove($TempCsvFolder."/".$filename);
			}
		}
	}
	
	## Build xls files
	# Split Export Content
	$row_export_content_array = explode("\n", $export_content);
	
	# Define xls Header
	$xlsHeaderArr = explode("\t",$row_export_content_array[0]);
	$numOfHeader = count($xlsHeaderArr);
	
	# Init PHPExcel 
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator("eClass")
										 ->setLastModifiedBy("eClass")
										 ->setTitle("eClass award and punishment records")
										 ->setSubject("eClass pastoral records")
										 ->setDescription("to websams")
										 ->setKeywords("eClass award and punishment records")
										 ->setCategory("eClass");
	
	// Create Active Sheet
	$objPHPExcel->setActiveSheetIndex(0);
	$ActiveSheet = $objPHPExcel->getActiveSheet();
	
	// Build xls Header
	for ($j=0; $j<$numOfHeader; $j++)
	{
		if($j < 26) {
			$ActiveSheet->setCellValue(chr(ord("A")+$j)."1", $xlsHeaderArr[$j]);
		}
		else {
			$ActiveSheet->setCellValue(chr(ord("A")+($j/26)-1).chr(ord("A")+($j%26))."1", $xlsHeaderArr[$j]);
		}
	}
	
	// Write xls Content Row
	$_rowCount = 0;
	$numOfRow = count($row_export_content_array);
	for($i=1; $i<$numOfRow; $i++)
	{
		$split_data_of_one_row = explode("\t",$row_export_content_array[$i]);
		$numOfData = count($split_data_of_one_row);
		for($j = 0; $j<$numOfData; $j++)
		{
			if($j < 26)
			{
//				if($j>=6&&$j<=11&&$split_data_of_one_row[$j]!=""){
//					$ActiveSheet->getCell(chr(ord("A")+$j).($i+1))->setValueExplicit($split_data_of_one_row[$j], PHPExcel_Cell_DataType::TYPE_NUMERIC);	
//				}
//				else{
//					$ActiveSheet->getCell(chr(ord("A")+$j).($i+1))->setValueExplicit($split_data_of_one_row[$j], PHPExcel_Cell_DataType::TYPE_STRING);
//				}
                
				// Text Format - General (Data Type: Numeric)
				if(($j==1 || ($j>=7 && $j<=11) || $j==14) && $split_data_of_one_row[$j] != "") {
					$ActiveSheet->getCell(chr(ord("A")+$j).($i+1))->setValueExplicit($split_data_of_one_row[$j], PHPExcel_Cell_DataType::TYPE_NUMERIC);
				}
				// Text Format - General (Data Type: String) 
				else if($j==1 || $j==2 || ($j>=7 && $j<=11) || $j==14 || $j==21 || $j==22) {
					$ActiveSheet->getCell(chr(ord("A")+$j).($i+1))->setValueExplicit($split_data_of_one_row[$j], PHPExcel_Cell_DataType::TYPE_STRING);
				}
				// Text Format - Text
				else {
					$ActiveSheet->getCell(chr(ord("A")+$j).($i+1))->setValueExplicit($split_data_of_one_row[$j], PHPExcel_Cell_DataType::TYPE_STRING);
					$ActiveSheet->getStyle(chr(ord("A")+$j).($i+1))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				}
			}
			// Text Format - Text
			else
			{
				$ActiveSheet->getCell(chr(ord("A")+($j/26)-1).chr(ord("A")+($j%26)).($i+1))->setValueExplicit($split_data_of_one_row[$j], PHPExcel_Cell_DataType::TYPE_STRING);
				$ActiveSheet->getStyle(chr(ord("A")+($j/26)-1).chr(ord("A")+($j%26)).($i+1))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
			}
		}
	}
	
	$_xlsFileName = "award_punishment_websams_format.xls";
	$path2write = $TempCsvFolder."/".$_xlsFileName;
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel5");
	$successArr = array();
	$successArr[$_enrolGroupId]["GenerateXls"] = $objWriter->save($path2write);
	
	## Output xls files
	output2browser(get_file_content($path2write), $_xlsFileName);
}
// Export data in CSV format
else
{
	$lexport->EXPORT_FILE($filename, $export_content);
}

intranet_closedb();
?>