<?php
# using: YW

####################### Change Log [start] ###########
#
#   Date    :   2020-11-06  (Bill)  [2020-0824-1151-40308]
#               - Support Follow-up Feedback    ($sys_custom['eDiscipline']['APFollowUpRemarks'])
#
#	Date	:	2020-10-08 (YatWoon) [Case#2020-1007-1112-42170]
#				change PIC selection (use common_choose)
#
#   Date    :   2019-04-30 (Bill)
#               prevent Cross-site Scripting
#
#	Date	:	2017-10-11 (Bill)	[DM#3270]
#				- return to step 1 if all selected groups without any users
#
#	Date	:	2017-07-11	(Bill)	[2016-1125-0939-00240]
#				support Study Score Limit Checking ($sys_custom['eDiscipline']['ApplyMaxStudyScore'])
#
#	Date	:	2017-06-28 (Bill)	[2015-0120-1200-33164]
#				Support display Merit Item with target Tag Type only
#
#	Date	:	2017-03-16	(Bill)	[2016-1207-1221-39240]
#				- support Activity Score	($sys_custom['UseActScore'])
#				- display different Score row depending on discipline item tag	($sys_custom['Discipline_AP_Item_Tag_ControlConductType'])
#
#	Date	:	2016-06-07	(Bill)	[2016-0329-1757-08164]
#	Detail	:	$sys_custom['eDiscipline']['CSCProbation']
#				- add option to set record in probation when merit type is minor/major demerit
#
#	Date	:	2016-04-28	(Bill)	[2016-0425-1704-58207]
#				hide Warning from Punishment drop down list ($sys_custom['eDiscipline']['HeungToHideWarningwhenAddAP'])
#
#	Date	:	2015-09-25	(Bill)	[2015-0416-1040-06164]
#				Hide Step 3 & 4, Merit & Conduct Mark and Continue button for HKUGA Cust
#				($sys_custom['eDiscipline']['add_AP_records_skip_step3n4'], $sys_custom['eDiscipline']['add_AP_records_hide_meritnconduct'])
#
#	Date	:	2014-12-12 (Bill)
#				disable "Finish Now" button after user click
#
#	Date	:	2013-11-07 (Carlos)
#				$sys_custom['eDiscipline']['yy3'] do not need submit alert for admin user
#
#	Date	:	2013-11-05 (YatWoon)
#				Customization: Add 2 extra fields: FileNumber, RecordNumber, $sys_custom['eDiscipline']['MST']['AP_Extra_Fields']	[Case#2013-0520-1008-41072]
#
#	Date	:	2013-06-13 (Carlos)
#	Detail	:	$sys_custom['eDiscipline']['yy3'] 
#				- disable MeritNumber and MeritType selection for non admin users
#				- display warning if score overflow of merit item limit, related tags limit and conduct mark limit
#
#	Date	:	2013-05-15 (YatWoon)
#	Datail	:	add RecordInputDate (customization for yy3 - $sys_custom['eDiscipline']['yy3'])
#				display ConductMarkRemark
#
#	Date	:	2013-05-09 (YatWoon)
#	Detail	:	update check_form(), add checking not allow input future record
#
#	Date	:	2012-02-13 (Henry Chow)
#	Detail 	:	disable "Submit" button on form submission
#
#	Date	:	2011-01-13 (Henry Chow)
#	Detail 	:	add hidden field "detentionID" in order to handle the detention ID from "Detention > Student List"
#
#	Date	:	2010-06-18 (YatWoon)
#	Detail 	:	update $student retrieve data method (detention can direct pass the student list to this page)
# 
#	Date	:	2010-06-10 (Henry)
#	Detail 	:	add attachment to record
# 
#	Date:	2010-04-14 YatWoon
#			check setting $ldiscipline->OnlyAdminSelectPIC, only admin can select PIC
#
####################### Change Log [end] ###########

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

### Handle SQL Injection + XSS [START]
if(!intranet_validateDate($RecordDate)) {
    $RecordDate = '';
}
if(!intranet_validateDate($RecordInputDate)) {
    $RecordInputDate = '';
}
if(!intranet_validateDate($ActionDueDate)) {
    $ActionDueDate = '';
}
if(empty($PIC)) {
    $PIC[] = $UserID;
}
if(is_array($PIC) && sizeof($PIC) > 0) {
    foreach($PIC as $thisKey => $thisPICID) {
        $PIC[$thisKey] = IntegerSafe($thisPICID);
    }
}
$sessionTime = cleanCrossSiteScriptingCode($sessionTime);

$record_type = IntegerSafe($record_type);
$CatID = IntegerSafe($CatID);
$ItemID = IntegerSafe($ItemID);
if($ldiscipline->AP_Interval_Value) {
    $MeritNum = (float)$MeritNum;
}
else {
    $MeritNum = IntegerSafe($MeritNum);
}
$MeritType = IntegerSafe($MeritType);
if($sys_custom['eDiscipline']['ConductMark1DecimalPlace']) {
    $ConductScore = (float)$ConductScore;
}
else {
    $ConductScore = IntegerSafe($ConductScore);
}
$semester = cleanCrossSiteScriptingCode($semester);
$Subject = IntegerSafe($Subject);
$SubjectID = IntegerSafe($SubjectID);
$TargetTagType = cleanCrossSiteScriptingCode($TargetTagType);
$detentionID = IntegerSafe($detentionID);

$applyProbation = IntegerSafe($applyProbation);
$maxStudyScore = IntegerSafe($maxStudyScore);
### Handle SQL Injection + XSS [END]

$li = new libgrouping();

## Get The max. no of record day(s) ##
$NumOfMaxRecordDays = $ldiscipline->MaxRecordDayBeforeAllow;
if($NumOfMaxRecordDays > 0) {
	$EnableMaxRecordDaysChecking = 1;
	$DateMinLimit = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d")-$NumOfMaxRecordDays, date("Y")));
	$DateMaxLimit = date("Y-m-d");
	//$DateMaxLimiy = '2011-11-11';
}
else {
	$EnableMaxRecordDaysChecking = 0;
}

# Check Access Right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Award_Punishment-New");

$linterface = new interface_html();
$lteaching = new libteaching();
$lgrouping = new libgrouping();

# Menu Highlight
$CurrentPage = "Management_AwardPunishment";
$CurrentPageArr['eDisciplinev12'] = 1;

# Left Menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Tag Information
$TAGS_OBJ[] = array($eDiscipline['Award_and_Punishment']);

# Step Information
$STEPS_OBJ[] = array($eDiscipline["SelectStudents"], 0);
$STEPS_OBJ[] = array($eDiscipline["AddRecordToStudents"], 1);
// [2015-0416-1040-06164] hide from $STEPS_OBJ
if(!$sys_custom['eDiscipline']['add_AP_records_skip_step3n4']){
	$STEPS_OBJ[] = array($eDiscipline["SelectActions"], 0);
	$STEPS_OBJ[] = array($eDiscipline["FinishNotification"], 0);
}

# Navigation Bar
$PAGE_NAVIGATION[] = array($eDiscipline['RecordList'], "index.php");
$PAGE_NAVIGATION[] = array($eDiscipline["NewRecord"], "");

# Current Academic Year ID
$academicYearID = Get_Current_Academic_Year_ID();

# Get data from preious page
if(!is_array($_POST['student'])) {
	$student = explode(',', $student);	
}
else {
	$student = $_POST['student'];
}
if(empty($student)) {
	header("Location: new1.php");
	exit;
}
$student = array_unique($student);

# Retrieve all students (from U - User, G - Group)
$student2 = array();
$permitted[] = 2;
foreach($student as $k => $d)
{
	$type = substr($d, 0, 1);
	$type_id = substr($d, 1);
	$type_id = IntegerSafe($type_id);
	
	switch($type)
	{
		case "U":
			$student2[] = $type_id;
			break;
		case "G":
			$groupIDAry[] = $type_id;
			$temp = $li->returnGroupUsersInIdentity($groupIDAry, $permitted);		
			//$temp = $lgrouping->returnGroupUsers($type_id);			
			foreach($temp as $k1=>$d1)
			{
				$student2[] = $d1['UserID'];	
			}
			break;
		case "C":
			$temp = $ldiscipline->getStudentListByClassID($type_id);
			for($i=0; $i<sizeof($temp); $i++) {
				$student2[] = $temp[$i][0];	
			}
			break;
		default:
		    $d = IntegerSafe($d);
			$student2[] = $d;
			break;
	}
}
if(is_array($student2)) {
	$student = array_keys(array_count_values($student2));
}
if(empty($student)) {
	header("Location: new1.php");
	exit;
}

if($sessionTime == "") {
	$sessionTime = session_id()."-".(time());
}

# Build HTML Part
$selected_student_table = "<table width='100%' border='0' cellspacing='0' cellpadding='5'>";
foreach($student as $k => $StudentID)
{
	/*
	$lu = new libuser($StudentID);
	$student_name = $lu->UserNameLang();
	$class_name = $lu->ClassName;
	$class_no = $lu->ClassNumber;
	*/
	$stu = $ldiscipline->getStudentNameByID($StudentID);
	$student_name = $stu[0][1];
	$class_name = $stu[0][2];
	$class_no = $stu[0][3];
	$internal_remark = $stu[0][4];
	
	$selected_student_table .= "<tr><td valign='top'><a href='javascript:viewCurrentRecord($StudentID)' class='tablelink'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_stu_ind.gif' width='20' height='20' border='0' align='absmiddle'>";
	$selected_student_table .= $class_name. ($class_no?"-":"") .$class_no." ".$student_name;
	$selected_student_table .= "</a>";
	if(trim($internal_remark))
		$selected_student_table .= "<br>". nl2br($internal_remark);
	$selected_student_table ."</td>";
	if(sizeof($sid)>0)	
		$selected_student_table .= (in_array($StudentID, $sid)) ? "<td>".$linterface->GET_SYS_MSG("",$Lang['eDiscipline']['ItemAlreadyAdded'])."</td>" : "";
	$selected_student_table .= "</tr>";
}
$selected_student_table .= "</table>";

$select_sem_html = "<input type='hidden' name='semester' id='semester' value=''>";

/*
$semester_mode = getSemesterMode();
if($semester_mode==1)
{
	$currentYearID = Get_Current_Academic_Year_ID();
	//$select_sem = getSelectSemester2("name=semester", $semester, 1, $currentYearID);		# semester in current year
	$select_sem = getSelectSemester("name=semester", $semester);	
	$select_sem_html = "
		<tr>
			<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\">{$i_SettingsSemester}</td>
			<td valign=\"top\">{$select_sem}</td>
		</tr>
	";
}
else
{
	$select_sem_html = "<input type='hidden' name='semester' value=''>";
}
*/

# Last PIC Selection
// if(empty($PIC))		$PIC[] = $UserID;
if (is_array($PIC) && sizeof($PIC) > 0)
{
	$list = implode("', '", $PIC);
	$namefield = getNameFieldWithLoginByLang();
	$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE RecordType = 1 AND UserID IN ('$list') AND RecordStatus = 1 ORDER BY $namefield";
	$array_PIC = $ldiscipline->returnArray($sql);
}
$PIC_selected = $linterface->GET_SELECTION_BOX($array_PIC, "name='PIC[]' ID='PIC[]' class='select_studentlist' size='6' multiple='multiple'", "");

# Attachment Selection
$path = "$file_path/file/disciplinev12/award_punishment/".$sessionTime;
$attSelect = $linterface->GET_SELECTION_BOX($ldiscipline->getAttachmentArray($path,''),"id=\"Attachment[]\" name=\"Attachment[]\"  multiple='multiple'","");
$attachment_selected = "<table width=\"20%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">
							<tr><td>".$attSelect."
							</td>
							<td valign=\"bottom\">".$linterface->GET_BTN($button_add, "button", "newWindow('attach.php?FolderLocation=".$sessionTime."&fieldname=Attachment[]', 9)")."
							<br/>".$linterface->GET_BTN($button_remove_selected, "button", "ajaxRemoveAttachment('Attachment[]', 'layer01')")."<div id=\"layer01\"></div>
							</td>
							</tr>
							</table>";

# Subject Selection
if ($ldiscipline->use_subject)
{
	$subjectList = $lteaching->returnSubjectList();
    $select_subject = "<SELECT name='SubjectID' id='SubjectID'>\n";
    $select_subject .= "<OPTION value=''>-- $i_notapplicable --</OPTION>\n";
    foreach ($subjectList AS $k=>$d)
    {
	    $selected = $SubjectID == $d[0] ? " selected":"";
        $select_subject .= "<OPTION value='". $d[0] ."' $selected>". $d[1] ."</OPTION>\n";
    }
    $select_subject .= "</SELECT>\n";
}
    
# Category Selection
$merit_cats = $ldiscipline->returnMeritItemCategoryByType(1);
$demerit_cats = $ldiscipline->returnMeritItemCategoryByType(-1);

# Item Selection
$items = $ldiscipline->retrieveMeritItemswithCodeGroupByCat();
if($sys_custom['Discipline_AP_Item_Tag_Seperate_Function']) {
	$item_tags = $ldiscipline->getAllItemTagIDMapping();
	$item_tags = BuildMultiKeyAssoc((array)$item_tags, "APItemID", "TagID", 1, 1);
}

# Merit Count Selection
$select_merit_num = "<SELECT name=MeritNum id=MeritNum ".($sys_custom['eDiscipline']['yy3'] && !$ldiscipline->IS_ADMIN_USER()?"disabled":"").">\n";
$AP_Interval = $ldiscipline->AP_Interval_Value ? $ldiscipline->AP_Interval_Value : 1;
for ($i=0; $i<=$ldiscipline->AwardPunish_MAX; $i=$i+$AP_Interval)
{
     $select_merit_num .= "<OPTION value=$i>$i</OPTION>\n";
}
$select_merit_num .= "</SELECT>\n";

# Conduct Mark Selection
$select_conduct_mark = "<SELECT name=ConductScore id=ConductScore";
if($sys_custom['eDiscipline']['yy3']){
	$select_conduct_mark .= " onchange=\"checkOverflowConductMark();\" ";
}
$select_conduct_mark.= ">\n";
$conductMarkInterval = $sys_custom['eDiscipline']['ConductMark1DecimalPlace'] ? 0.1 : 1;
/*
for ($i=0; $i<=(int)$ldiscipline->ConductMarkIncrement_MAX; $i+=$conductMarkInterval)
{
     $select_conduct_mark .= "<OPTION value=$i>$i</OPTION>\n";    
}
*/
$i = 0;
$loopConductMark = true;
while($loopConductMark) {
	$select_conduct_mark .= "<OPTION value=$i>$i</OPTION>\n";    
	//$i+=$conductMarkInterval;
	$i = (float)(string)($i + $conductMarkInterval);
	if(floatcmp($i ,">" ,$ldiscipline->ConductMarkIncrement_MAX)) {
		$loopConductMark = false;
	}
}
$select_conduct_mark .= "</SELECT>\n";

# Study Score Selection
$select_study_score = "<SELECT name=StudyScore id=StudyScore";
// [2016-1125-0939-00240]
if($sys_custom['eDiscipline']['ApplyMaxStudyScore']) {
	$select_study_score .= " onchange=\"checkOverflowStudyScore();\" ";
}
$select_study_score.= ">\n";
for ($i=0; $i<=$ldiscipline->SubScoreIncrement_MAX; $i++)
{
     $select_study_score .= "<OPTION value=$i>$i</OPTION>\n";
}
$select_study_score .= "</SELECT>\n";

# Activity Score Selection
$select_activity_score = "<SELECT name=ActivityScore id=ActivityScore>\n";
for ($i=0; $i<=$ldiscipline->ActScoreIncrement_MAX; $i++)
{
     $select_activity_score .= "<OPTION value=$i>$i</OPTION>\n";
}
$select_activity_score .= "</SELECT>\n";

$merit_record_type = $ldiscipline->returnValidMeritTypeFieldWithOrder(1);
$demerit_record_type = $ldiscipline->returnValidMeritTypeFieldWithOrder(-1);

// [2016-0425-1704-58207] not get Warning from merit type
if($sys_custom['eDiscipline']['HeungToHideWarningwhenAddAP']){
	$demerit_record_type = $ldiscipline->returnValidMeritTypeFieldWithOrder(-1, true);
}

# Remark
$remark = stripslashes(intranet_htmlspecialchars($remark));

# [2020-0824-1151-40308] Follow-up Remark
$followUpRemark = stripslashes(intranet_htmlspecialchars($followUpRemark));

# Tag Settings
if($sys_custom['Discipline_AP_Item_Tag_ControlConductType']) {
	$tagArray = $ldiscipline->getTagSetting();
	$tagArray = BuildMultiKeyAssoc((array)$tagArray, "TagID");
}

/*
# generate semester checking js
if($semester_mode==2)
{
	$sem_data = getSemesters();
	$sem_js.="var sem_js = new Array();\n";
	for($i=0;$i<sizeof($sem_data);$i++)
	{
		list($sem_name, $sem_type, $sem_start, $sem_end) = split("::", $sem_data[$i]);
		$sem_js.="sem_js.push(new Array('$sem_start','$sem_end'));\n";
	}
}
*/

# Start layout
$linterface->LAYOUT_START();
?>

<SCRIPT LANGUAGE=Javascript>
<!--
<?/*=$sem_js*/?>
var ClickID = '';
var callback_ajaxRemoveAttachment = {
	success: function ( o )
    {
		var fname = document.getElementById('fieldname').value;
	    var tmp_str = o.responseText;
	    checkOptionRemove(document.form1.elements[fname]);
	    document.getElementById('ref_list').innerHTML = tmp_str;
	    DisplayPosition();
	}
}

function ajaxRemoveAttachment(fname, layername)
{
	ClickID = layername;
	
	obj = document.form1;
	obj.fieldname.value = fname;
	obj.task.value = "remove";
	obj.FolderLocation.value = "<?=$sessionTime?>";
	YAHOO.util.Connect.setForm(obj);
	
	var path = "ajax_attachment.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_ajaxRemoveAttachment);
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function DisplayPosition()
{
	var posleft = getPosition(document.getElementById(ClickID),'offsetLeft') +10;
	var postop = getPosition(document.getElementById(ClickID),'offsetTop') +10;
	var divwidth = document.getElementById('ref_list').clientWidth;
	
	if(posleft+divwidth > document.body.clientWidth)
		document.getElementById('ref_list').style.right= "5px";
	else
		document.getElementById('ref_list').style.left= posleft+"px";
	
  	document.getElementById('ref_list').style.top= postop+"px";
  	document.getElementById('ref_list').style.visibility='visible';
}

// Generate Merit Category
var merit_cat_select = new Array();
merit_cat_select[0] = new Option("-- <?=$eDiscipline["SelectRecordCategory2"]?> --",0);
<? foreach($merit_cats as $k => $d) { ?>
	merit_cat_select[<?=$k+1?>] = new Option("<?=str_replace('"', '\"', $d['CategoryName'])?>",<?=$d['CatID']?>);
<? } ?>

// Generate Demerit Category
var demerit_cat_select = new Array();
demerit_cat_select[0] = new Option("-- <?=$eDiscipline["SelectRecordCategory2"]?> --",0);
<? foreach($demerit_cats as $k => $d) { ?>
	demerit_cat_select[<?=$k+1?>] = new Option("<?=str_replace('"', '\"', $d['CategoryName'])?>",<?=$d['CatID']?>);
<? } ?>

// Generate Merit Type
var merit_record_type = new Array();
<? if(sizeof($merit_record_type)>0) {
	foreach($merit_record_type as $k => $d) { ?>
		merit_record_type[<?=$k?>] = new Option("<?=$d[1]?>",<?=$d[0]?>);
<? } 
} ?>

// Generate Demerit Type
var demerit_record_type = new Array();
<? if(sizeof($demerit_record_type)>0) {
foreach($demerit_record_type as $k => $d) { ?>
	demerit_record_type[<?=$k?>] = new Option("<?=$d[1]?>",<?=$d[0]?>);
<? } 
} ?>

function viewCurrentRecord(id)
{
	newWindow('viewcurrent.php?StudentID='+id, 8);
}

function changeMeritType() 
{
	var obj = document.form1;
	var cat_select = obj.elements["CatID"];
	var item_select = obj.elements["ItemID"];
	var meritType = obj.elements["MeritType"];
	var increment_de_conduct = document.getElementById("increment_de_conduct");
	var increment_de_subsore = document.getElementById("increment_de_subsore");
	
	while (item_select.options.length > 0)
	{
	    item_select.options[0] = null;
	}
	item_select.options[0] = new Option("-- <?=$eDiscipline["SelectRecordItem"]?> --",0);
	
	if(obj.record_type[0].checked)		// Award
	{
		cat_select.options.length = merit_cat_select.length;
		for(i=0;i<merit_cat_select.length;i++)
		{
			cat_select.options[i]=new Option(merit_cat_select[i].text,merit_cat_select[i].value);	
			<? if($CatID) {?>
				if(merit_cat_select[i].value == <?=$CatID?>) 	cat_select.selectedIndex = i;
			<? } ?>
		}
		
		meritType.options.length = merit_record_type.length;
		meritType.options[0]=new Option("--", "-999");
		for(i=1;i<merit_record_type.length;i++)
			meritType.options[i]=new Option(merit_record_type[i].text,merit_record_type[i].value);	
		
		obj.elements["MeritNum"].selectedIndex = <?=$MeritNum ? $MeritNum : 0?>;
		increment_de_conduct.innerHTML="<?=$Lang['eDiscipline']['Gain']?>";
		<? if($ldiscipline->UseSubScore) { ?>
			increment_de_subscore.innerHTML="<?=$Lang['eDiscipline']['Gain']?>";
		<? } ?>
		<? if($ldiscipline->UseActScore) { ?>
			increment_de_actscore.innerHTML="<?=$Lang['eDiscipline']['Gain']?>";
		<? } ?>
		obj.elements["ConductScore"].selectedIndex = <?=$ConductScore ? $ConductScore : 0?>;
	}
	else								// Punishment
	{
		cat_select.options.length = demerit_cat_select.length;
		for(i=0;i<demerit_cat_select.length;i++)
		{
			cat_select.options[i]=new Option(demerit_cat_select[i].text,demerit_cat_select[i].value);
			<? if($CatID) {?>
				if(demerit_cat_select[i].value == <?=$CatID?>) 	cat_select.selectedIndex = i;
			<? } ?>
		}
		
		meritType.options.length = demerit_record_type.length;
		meritType.options[0]=new Option("--", "-999");	
		for(i=1;i<demerit_record_type.length;i++)
			meritType.options[i]=new Option(demerit_record_type[i].text,demerit_record_type[i].value);	
		
		obj.elements["MeritNum"].selectedIndex = <?=$MeritNum ? $MeritNum : 0?>;
		increment_de_conduct.innerHTML="<?=$Lang['eDiscipline']['Deduct']?>";
		<? if($ldiscipline->UseSubScore) { ?>
			increment_de_subscore.innerHTML="<?=$Lang['eDiscipline']['Deduct']?>";
		<? } ?>
		<? if($ldiscipline->UseActScore) { ?>
			increment_de_actscore.innerHTML="<?=$Lang['eDiscipline']['Deduct']?>";
		<? } ?>
		obj.elements["ConductScore"].selectedIndex = <?=$ConductScore ? $ConductScore : 0?>;
	}
	
	<?php if($sys_custom['eDiscipline']['yy3']){ ?>
		checkOverflowConductMark();
	<?php } ?>
}

function changeCat(value)
{
	var item_select = document.form1.elements["ItemID"];
	//var selectedCatID = document.form1.elements["CatID"].value;
	var selectedCatID = value;
	
	while (item_select.options.length > 0)
	{
	    item_select.options[0] = null;
	}
	item_select.options[0] = new Option("-- <?=$eDiscipline["SelectRecordItem"]?> --",0);
	
	if (selectedCatID == '')
	{
	<?
		$curr_cat_id = "";
		$pos = 1;
		for ($i=0; $i<sizeof($items); $i++)
		{
		    list($r_catID, $r_itemID, $r_itemName) = $items[$i];
		    
		    if($sys_custom['Discipline_AP_Item_Tag_Seperate_Function'] && $TargetTagType != "") {
				$r_itemTag = $item_tags[$r_itemID][0];
				if(($TargetTagType=="Conduct" && $r_itemTag==1) || ($TargetTagType=="Attitude" && $r_itemTag==2)) {
					// do nothing
				}
				else {
					continue;
				}
			}
		    
		    $r_itemName = intranet_undo_htmlspecialchars($r_itemName);
		    $r_itemName = str_replace('"', '\"', $r_itemName);
		    $r_itemName = str_replace("'", "\'", $r_itemName);
		    if ($r_catID != $curr_cat_id)
			{
		         $pos = 1;
	?>
    }
	else if (selectedCatID == "<?=$r_catID?>")
	{
		<? 		$curr_cat_id = $r_catID;
	     	}
	    ?>
		item_select.options[<?=$pos++?>] = new Option("<?=$r_itemName?>",<?=$r_itemID?>);
		<? if($r_itemID == $ItemID) { ?>
			item_select.selectedIndex = <?=$pos-1?>;
		<? } ?>
<? 		} ?>
    }
}

function changeItem(value)
{
<?php //[2015-0416-1040-06164] empty the function for HKUGA Cust
	if($sys_custom['eDiscipline']['add_AP_records_hide_meritnconduct']){
  		// do nothing
  	}
  	// Normal
  	else{
?>
	var i;
	var meritType = document.getElementById('MeritType');
	var meritNum = document.getElementById('MeritNum');
	var conductScore = document.getElementById('ConductScore');
	<? if ($ldiscipline->UseSubScore) { ?>
		var studyScore = document.getElementById('StudyScore');
	<? } ?>
	<? if ($ldiscipline->UseActScore) { ?>
		var activityScore = document.getElementById('ActivityScore');
	<? } ?>
	
	<?
	for ($i=0; $i<sizeof($items); $i++)
	{
     list($r_catID, $r_itemID, $r_itemName, $r_meritType, $r_meritNum, $r_conduct, $r_punish, $r_detention, $r_subscore1) = $items[$i];
     $r_subscore2 = $items[$i]["SubScore2"];
     $r_MaxGainDeductSubScore1 = $items[$i]['MaxGainDeductSubScore1'];
     
     if($sys_custom['Discipline_AP_Item_Tag_ControlConductType']) {
     	$r_tagType = "";
	 	$r_tagID = $ldiscipline->getItemTagIDInfo($r_itemID);
	    if($r_tagID[0] > 0) {
	     	$r_tagType = $tagArray[$r_tagID[0]]["TagName"];
	    }
	    $r_tagType = $r_tagType? $r_tagType : "conduct";
	   	$r_tagType = strtolower(trim($r_tagType));
	   	
	    if($r_tagType=="learning attitude") {
	    	$r_tagType = "study";
	    }
	    if($r_tagType=="activity/service") {
	    	$r_tagType = "activity";
	    }
     }
     ?>
     
     if (value=="<?=$r_itemID?>")
     {
		if(document.form1.temp_flag.value==1)
		{
			check_meritType = <?=$r_meritType?>;
			check_meritNum = <?=$r_meritNum?>;
			check_conductScore = <?=$r_conduct?>;
			//check_studyScore = <?=$r_subscore1?>;
			//check_activityScore = <?=$r_subscore2?>;
		}
		else
		{
			check_meritType = <?=($r_meritType?$r_meritType:0) ?>;
			check_meritNum = <?=($r_meritNum?$r_meritNum:0)?>;
			check_conductScore = <?=($r_conduct?$r_conduct:0)?>;
			//check_studyScore = <?=($r_subscore1?$r_subscore1:0)?>;
			//check_activityScore = <?=($r_subscore2?$r_subscore2:0)?>;
		}
		
		<?php if($sys_custom['eDiscipline']['HeungToHideWarningwhenAddAP']){ ?>
			if(check_meritType==0){
				check_meritType = -999;
			}
		<?php } ?>
		
		<?php if($sys_custom['Discipline_AP_Item_Tag_ControlConductType']){ ?>
			check_type = "<?=$r_tagType?>";
		<?php } ?>
		
        // Merit Type
        for (i=0; i<meritType.options.length; i++)
        {
              if (meritType.options[i].value==check_meritType)
              {
                  meritType.selectedIndex = i;
                  break;
              }
        }
        
        // Merit Num
        for (i=0; i<meritNum.options.length; i++)
        {
              if (meritNum.options[i].value==check_meritNum)
              {
                  meritNum.selectedIndex = i;
                  break;
              }
        }

        // Conduct Score
        for (i=0; i<conductScore.options.length; i++)
        {
              if (conductScore.options[i].value==check_conductScore)
              {
                  conductScore.selectedIndex = i;
                  break;
              }
        }
         
        // Sub Score
        <? if ($ldiscipline->UseSubScore) { ?>
         	for (i=0; i<studyScore.options.length; i++)
        	{
               if (studyScore.options[i].value=="<?=$r_subscore1?>")
               {
                   studyScore.selectedIndex = i;
                   break;
               }
         	}
        <? } ?>
         
        // Act Score
        <? if ($ldiscipline->UseActScore) { ?>
         	for (i=0; i<activityScore.options.length; i++)
          	{
               if (activityScore.options[i].value=="<?=$r_subscore2?>")
               {
                   activityScore.selectedIndex = i;
                   break;
               }
          	}
        <? } ?>
         
        <? if ($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']) { ?>
        	if(document.form1.maxStudyScore) {
        		document.form1.maxStudyScore.value = "<?=$r_MaxGainDeductSubScore1?>";
        		$('#MaxStudyScoreWarnDiv').html('<?=str_replace("<!--MAX_SCORE-->", $r_MaxGainDeductSubScore1, $Lang['eDiscipline']['StudentExceedMeritItemMaxStudyScore'])?>');
        		$('#MaxStudyScoreWarnDiv').hide();
        		resetOverflowStudyScoreDisable();
        	}
        <? } ?>
		
		<?php if($sys_custom['Discipline_AP_Item_Tag_ControlConductType']){ ?>
			// Hide all rows
			$('#conductScoreTr').hide();
        	<? if ($ldiscipline->UseSubScore) { ?>
				$('#studyScoreTr').hide();
         	<? } ?>
         	<? if ($ldiscipline->UseActScore) { ?>
				$('#activityScoreTr').hide();
         	<? } ?>
         	
         	// Disable all rows
			conductScore.disabled = true;
        	<? if ($ldiscipline->UseSubScore) { ?>
				studyScore.disabled = true;
         	<? } ?>
         	<? if ($ldiscipline->UseActScore) { ?>
				activityScore.disabled = true;
         	<? } ?>
         	
         	// Show and enable matched row 
			$('#'+check_type+'ScoreTr').show();
			if(<?=$r_tagType?>Score)
				<?=$r_tagType?>Score.disabled = false;
        <? } ?>
     }
<?
}
?>
	document.form1.temp_flag.value=1;
	<?php if($sys_custom['eDiscipline']['yy3']){ ?>
		checkOverflowConductMark();
	<?php } ?>
	
	<? if($sys_custom['eDiscipline']['CSCProbation']){ ?>
		displayProbationSetting();
	<? } ?>
<?php } ?>
}

function CompareDates(date_string1, date_string2, EqualDateAllow)
{
	var yr1  = parseInt(date_string1.substring(0,4),10);
	var mon1 = parseInt(date_string1.substring(5,7),10);
	var dt1  = parseInt(date_string1.substring(8,10),10);
	var yr2  = parseInt(date_string2.substring(0,4),10);
	var mon2 = parseInt(date_string2.substring(5,7),10);
	var dt2  = parseInt(date_string2.substring(8,10),10);
	var date1 = new Date(yr1, mon1-1, dt1);
	var date2 = new Date(yr2, mon2-1, dt2);
	
	if(EqualDateAllow == 0)
	{
		if(date2 < date1) {
			return true;
		}
		else{
			return false;
		}
	}
	else{
		if(date2 <= date1) {
			return true;
		}
		else{
			return false;
		}
	}
}

function check_form()
{
	obj = document.form1;
	
	// Record Date
	if(CompareDates(obj.RecordDate.value,"<?=date("Y-m-d")?>", 0))
	{
		alert("<?=$eDiscipline['JSWarning']['RecordDateIsNotAllow'];?>");
		return false;
	}
	
	if(!check_date(obj.RecordDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))
	{
		return false;
	}
	else
	{
		<? if($EnableMaxRecordDaysChecking == 1) { ?>
			// Check Record Date is allow or not
			var DateMinLimit = "<?=$DateMinLimit;?>";
			var DateMaxLimit = "<?=$DateMaxLimit;?>";
			if(!CompareDates(obj.RecordDate.value, DateMinLimit, 1))
			{
				alert("<?=$eDiscipline['JSWarning']['RecordDateIsNotAllow'];?>");
				return false;
			}
			else {
				if(!CompareDates(DateMaxLimit, obj.RecordDate.value, 1))
				{
					alert("<?=$eDiscipline['JSWarning']['RecordDateIsNotAllow'];?>");
					return false;
				}
			}
		<? } ?>
	}
	
	<? if($sys_custom['eDiscipline']['yy3']) {?>
		if(!check_date(obj.RecordInputDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))
		{
			return false;
		}
		
		if(CompareDates(obj.RecordInputDate.value,"<?=date("Y-m-d")?>", 0))
		{
			alert("<?=$eDiscipline['JSWarning']['RecordInputDateIsNotAllow'];?>");
			return false;
		}
		
		if(CompareDates(obj.RecordDate.value, obj.RecordInputDate.value, 0))
		{
			alert("<?=$eDiscipline['JSWarning']['RecordInputDateIsNotAllow'];?>");
			return false;
		}
	<? } ?>
	
	<? if($sys_custom['wscss_disciplinev12_ap_prompt_action']) { ?>
		if(!CompareDates(obj.ActionDueDate.value, obj.RecordDate.value, 1))
		{
			alert("<?=$Lang['eDiscipline']['ActionDateCompareWithRecordDate']?>");
			return false;
		}
	<? } ?>
	
	// Semester
	<? if($semester_mode==2) {?>
		if(!check_semester(obj.RecordDate))
		{
			return false;
		}
	<? } ?>
	
	// Teahcer/Staff
	<? if($sys_custom['winglung_ap_report']) { ?>
		if(obj.picType[0].checked==true) {
			objPic = document.getElementById('PIC[]');
			checkOption(objPic);
			if (objPic.length==0) {
				checkOptionAdd(objPic, "<? for($i = 0; $i < 40; $i++) echo " "; ?>", "");
				alert("<?=$iDiscipline['Assign_Duty_Teacher']?>");
				return false;
			}
			checkOptionAll(objPic);
		} 
		if(obj.picType[1].checked==true && obj.picName.value=="") {
			alert("<?=$iDiscipline['Assign_Duty_Teacher']?>");
			return false;
		}
	<? }
	else { ?>
		objPic = document.getElementById('PIC[]');
		checkOption(objPic);
		if (objPic.length==0) {
			checkOptionAdd(objPic, "<? for($i = 0; $i < 40; $i++) echo " "; ?>", "");
			alert("<?=$iDiscipline['Assign_Duty_Teacher']?>");
			return false;
		}
		checkOptionAll(objPic);
	<? } ?>
	
	checkOptionAll(document.getElementById('Attachment[]'));

	/* Check Merit Type 0 - N.A. */
	if(obj.elements["MeritNum"].value==0)	obj.elements["MeritType"].selectedIndex = 0;
	if(obj.elements["MeritType"].value==-999)	obj.elements["MeritNum"].selectedIndex = 0;
	
	// Category / Items
    if(obj.elements["CatID"].value == 0 || obj.elements["ItemID"].value == 0)
    {
        alert('<?=$i_alert_pleaseselect.$i_Discipline_System_general_record?>');
        return false;
    }
    
    // Checking on Merit Num, Conduct Score & Subscore & ActScore (if any)
	<?php // [2015-0416-1040-06164] Skip checking for Merit and Consuct Score 
	if($sys_custom['eDiscipline']['add_AP_records_hide_meritnconduct']) {
	  	 //  do nothing
	}
	// Normal
	else {
   	?>
    <? if($ldiscipline->NotAllowEmptyInputInAP) { ?>
	    <? 
	    if(!$ldiscipline->Hidden_ConductMark) {
	    	if($ldiscipline->UseActScore) {
				$jsScript = ' && obj.elements["StudyScore"].value == 0 && obj.elements["ActivityScore"].value == 0';	
				$alertMsg = $eDiscipline['MeritDemerit'].", ".$eDiscipline['Conduct_Mark']." ".$i_alert_or." ".$i_Discipline_System_Subscore1." ".$i_alert_or." ".$Lang['eDiscipline']['ActivityScore'];
			}
			else if($ldiscipline->UseSubScore) {
				$jsScript = ' && obj.elements["StudyScore"].value == 0';	
				$alertMsg = $eDiscipline['MeritDemerit'].", ".$eDiscipline['Conduct_Mark']." ".$i_alert_or." ".$i_Discipline_System_Subscore1;
			}
			else {
				$alertMsg = $eDiscipline['MeritDemerit']." ".$i_alert_or." ".$eDiscipline['Conduct_Mark'];
			}
			$alertMsg = $i_alert_pleaseselect.$alertMsg;
		?>
	    else if(obj.elements["ConductScore"].value == 0 && obj.elements["MeritNum"].value == 0 <?=$jsScript?>)
	    {
		    alert("<?=$alertMsg ?>");
        	return false;
	    }
	    <? }
	    else {
			if($ldiscipline->UseActScore) {
				$jsScript = ' && obj.elements["StudyScore"].value == 0 && obj.elements["ActivityScore"].value == 0';	
				$alertMsg = $eDiscipline['MeritDemerit']." ".$i_alert_or." ".$i_Discipline_System_Subscore1." ".$i_alert_or." ".$Lang['eDiscipline']['ActivityScore'];
			}
			else if($ldiscipline->UseSubScore) {
				$jsScript = ' && obj.elements["StudyScore"].value == 0';	
				$alertMsg = $eDiscipline['MeritDemerit']." ".$i_alert_or." ".$i_Discipline_System_Subscore1;
			}
			else {
				$alertMsg = $eDiscipline['MeritDemerit'];
			}
			$alertMsg = $i_alert_pleaseselect.$alertMsg;
		    ?>
	    else if(obj.elements["MeritNum"].value == 0 <?=$jsScript?>)
	    {
		    alert("<?=$alertMsg ?>");
        	return false;
	    }
	    <? } ?>
    <? }
    else { ?>
    	// allow null input
	    <? 
	    if(!$ldiscipline->Hidden_ConductMark) {
			if($ldiscipline->UseActScore) {
				$jsScript = ' && obj.elements["StudyScore"].value == 0 && obj.elements["ActivityScore"].value == 0';	
				$alertMsg = $eDiscipline['MeritDemerit'].", ".$eDiscipline['Conduct_Mark']." ".$i_alert_or." ".$i_Discipline_System_Subscore1." ".$i_alert_or." ".$Lang['eDiscipline']['ActivityScore'];
			}
			else if($ldiscipline->UseSubScore) {
				$jsScript = ' && obj.elements["StudyScore"].value == 0';	
				$alertMsg = $eDiscipline['MeritDemerit'].", ".$eDiscipline['Conduct_Mark']." ".$i_alert_or." ".$i_Discipline_System_Subscore1;
			}
			else {
				$alertMsg = $eDiscipline['MeritDemerit']." ".$i_alert_or." ".$eDiscipline['Conduct_Mark'];
			}
			
			if($intranet_session_language=="en")
				$alertMsg = $Lang['eDiscipline']['EmptyInput']." ".$alertMsg." ".$Lang['eDiscipline']['IsSelected'].$button_confirm."?";
			else 
				$alertMsg = $Lang['eDiscipline']['IsSelected'].$alertMsg.$Lang['eDiscipline']['sdbnsm_fullStop'].$button_confirm."?";
		    ?>
	    else if(obj.elements["ConductScore"].value == 0 && obj.elements["MeritNum"].value == 0 <?=$jsScript?>)
	    {
		    if (window.confirm('<?=$alertMsg?>'))
	        {
	            obj.action='new3.php';
	            //return true;
	        }
	        else
	        {
	        	return false;
	    	}
	    }
	    <? }
	    else { 
			if($ldiscipline->UseActScore) {
				$jsScript = ' && obj.elements["StudyScore"].value == 0 && obj.elements["ActivityScore"].value == 0';	
				$alertMsg = $eDiscipline['MeritDemerit']." ".$i_alert_or." ".$i_Discipline_System_Subscore1." ".$i_alert_or." ".$Lang['eDiscipline']['ActivityScore'];
			}
			else if($ldiscipline->UseSubScore) {
				$jsScript = ' && obj.elements["StudyScore"].value == 0';	
				$alertMsg = $eDiscipline['MeritDemerit']." ".$i_alert_or." ".$i_Discipline_System_Subscore1;
			}
			else {
				$alertMsg = $eDiscipline['MeritDemerit'];
			}
			
			if($intranet_session_language=="en")
				$alertMsg = $Lang['eDiscipline']['EmptyInput']." ".$alertMsg." ".$Lang['eDiscipline']['IsSelected'].$button_confirm."?";
			else 
				$alertMsg = $Lang['eDiscipline']['IsSelected'].$alertMsg.$Lang['eDiscipline']['sdbnsm_fullStop'].$button_confirm."?";
		    ?>
	    else if(obj.elements["MeritNum"].value == 0 <?=$jsScript?>)
	    {
		    if (window.confirm('<?=$alertMsg?>'))
	        {
	            obj.action='new3.php';
	            //return true;
	        }
	        else
	        {
	        	return false;
	    	}
	    }
	    <? } ?>
    <? } ?>
<?php } ?>

    // Check Merit Type
	<? if($sys_custom['wscss_disciplinev12_ap_prompt_action']) { ?>
		if(!check_date(obj.ActionDueDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) {
			return false;
		}
	<? } ?>
	
	<?php if($sys_custom['eDiscipline']['yy3'] && !$ldiscipline->IS_ADMIN_USER()){ ?>
		if($('#OverflowConductMarkFlag').length==0 || ($('#OverflowConductMarkFlag').length > 0 && confirm('<?=$Lang['eDiscipline']['WarningMsgArr']['StudentExceedConductMarkLimitConfirmSubmit']?>'))){
	<?php } ?>	
	if(obj.stop_step2.value==1) {
//		$("#submitBtn").attr("disabled", true);
		document.getElementById('submitBtn').disabled  = true; 
		document.getElementById('submitBtn').className  = "formbutton_disable_v30 print_hide"; 
		obj.action = 'new3_update.php';	
	}
	else {
		obj.action='new3.php';
	}
	//obj.action='new3.php';
	
	<?php if($sys_custom['eDiscipline']['yy3'] && !$ldiscipline->IS_ADMIN_USER()){ ?>
		$('#MeritNum').after('<input type="hidden" name="MeritNum" value="'+$('#MeritNum').val()+'" />');
		$('#MeritType').after('<input type="hidden" name="MeritType" value="'+$('#MeritType').val()+'" />');
	<?php } ?>
	
    return true;
    
	<?php if($sys_custom['eDiscipline']['yy3'] && !$ldiscipline->IS_ADMIN_USER()){ ?>
		}
		else{
			return false;
		}
	<?php } ?>  
}

function check_semester(objDate)
{
	semester_periods = sem_js;
	
	valid = false;
	if(semester_periods!=null)
	{
		for(j=0;j<semester_periods.length;j++){
			dateStart = semester_periods[j][0];
			dateEnd = semester_periods[j][1];
			if(dateStart <= objDate.value && dateEnd >=objDate.value){
				valid = true;
				break;
			}
		}
	}
	
	if(!valid)
	{
		objDate.focus();
		objDate.className ='textboxnum textboxhighlight';
		alert('<?=$eDiscipline["NoSemesterSettings"]?>');
		return false;
	}
	
	return true;
}

function skipStep2() {
	document.form1.action='new3_update.php';
	checkOptionAll(document.getElementById('PIC[]'));
	checkOptionAll(document.getElementById('Attachment[]'));
	document.form1.stop_step2.value=1;
	if(check_form()) {
		document.form1.submit();
	}
}

<? if($sys_custom['eDiscipline']['CSCProbation']) {?>
function displayProbationSetting(){
	if(document.form1.MeritType){
		var meritValue = $('select#MeritType').val();
		
		if(meritValue==-2 || meritValue==-3)
		{
			document.getElementById('ProbationRow').style.display = '';
		}
		else
		{
			document.getElementById('ProbationRow').style.display = 'none';
		}
	}
}
<? } ?>

<?php if($sys_custom['eDiscipline']['yy3']){ ?>
function checkOverflowConductMark()
{
	var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image()?>';
	var studentObjs = document.getElementsByName('student[]');
	var studentIdAry = [];
	var recordDate = document.getElementById('RecordDate').value.Trim();
	var recordType = $('input[name=record_type]:checked').val();
	var itemId = $('select[name=ItemID]').val();
	var conductScore = $('select[name=ConductScore]').val();
	
	for(var i=0;i<studentObjs.length;i++) {
		studentIdAry.push(studentObjs[i].value);
	}
	
	if(itemId > 0 && conductScore > 0 && studentIdAry.length > 0 && recordDate != '') {
		$('input#continueBtn').attr('disabled',true);
		$('input#submitBtn').attr('disabled',true);
		
		$('#OverflowMarkWarnDiv').html(loadingImg).load(
			'ajax_check_overflow_mark.php',
			{
				'StudentID[]': studentIdAry,
				'RecordDate':recordDate,
				'RecordType':recordType,
				'ItemID':itemId,
				'ConductScore':conductScore
			},
			function(data){
				$('input#continueBtn').attr('disabled',false);
				$('input#submitBtn').attr('disabled',false);
			}
		);
	}
	else{
		$('#OverflowMarkWarnDiv').html('');
	}
}

$(document).ready(function(){
	checkOverflowConductMark();
	
	<? if($sys_custom['eDiscipline']['CSCProbation']){ ?>
	displayProbationSetting();
	<? } ?>
});
<?php } ?>

<? if ($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']) { ?>
function checkOverflowStudyScore()
{
	resetOverflowStudyScoreDisable();
	
	// Exceed Max Study Score (Display Warning & Disable Submit Button)
	if(document.form1.StudyScore && document.form1.maxStudyScore && parseInt(document.form1.maxStudyScore.value) > 0 && parseInt(document.form1.StudyScore.value) > parseInt(document.form1.maxStudyScore.value)) {
		document.form1.StudyScore.focus();
		$('#MaxStudyScoreWarnDiv').show();
		$('input#continueBtn').attr('disabled', true);
		$('input#submitBtn').attr('disabled', true);
		document.getElementById('continueBtn').className  = "formbutton_disable_v30 print_hide"; 
		document.getElementById('submitBtn').className  = "formbutton_disable_v30 print_hide"; 
	}
}

function resetOverflowStudyScoreDisable()
{
	// Reset
	$('#MaxStudyScoreWarnDiv').hide();
	$('input#continueBtn').attr('disabled', false);
	$('input#submitBtn').attr('disabled', false);
	document.getElementById('continueBtn').className = "formbutton_v30 print_hide"; 
	document.getElementById('submitBtn').className = "formbutton_v30 print_hide";
}
<? } ?>

//-->
</SCRIPT>
<div id="ref_list"></div>
<form name="form1" method="POST" onsubmit="return check_form();">

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td class="navigation">
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
		</table>
	</td>
</tr>
<tr>
	<td align="center"><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
</tr>

<tr>
	<td>
		<table align="center" width="90%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td valign="top" colspan="2" align="right"><?=$linterface->GET_SYS_MSG($msg, $xmsg)?></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="tablerow2"><?=$i_general_students_selected?></td>
			<td valign="top" class="tablerow2"><?=$selected_student_table?></td>
		</tr>
		<tr>
			<td colspan="2" valign="middle" nowrap="nowrap" class="form_sep_title"><i> - <?=$eDiscipline["BasicInfo"]?> -</i></td>
		</tr>
		
		<?=$select_sem_html?>
		
		<tr>
            <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['EventDate']?><span class="tabletextrequire">* </span></td>
            <td> <?=$linterface->GET_DATE_PICKER("RecordDate",$RecordDate)?></td>
        </tr>
        
        <? if($sys_custom['eDiscipline']['yy3']) {?>
	        <tr>
                <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['EventRecordDate']?><span class="tabletextrequire">* </span></td>
                <td> <?=$linterface->GET_DATE_PICKER("RecordInputDate",$RecordInputDate)?></td>
	        </tr>
        <? } ?>
        
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Contact_PIC?><span class="tabletextrequire">* </span></td>
			<td valign="top">
				<table>
				<? if($sys_custom['winglung_ap_report']) { ?>
				<tr>
					<td><input type="radio" name="picType" id="picType1" value="1" <? if($picType=="" || $picType==1) echo "checked";?>></td>
					<td><?=$PIC_selected?></td>
					<td valign="bottom">
						<?=$linterface->GET_BTN($button_select, "button","form1.picType1.checked=true; newWindow('../choose_pic.php?fieldname=PIC[]',11)");?><br />
						<?=$linterface->GET_BTN($button_remove, "button","checkOptionRemove(document.form1.elements['PIC[]']); form1.picType1.checked=true");?>
					</td>
				</tr>
				<tr>
					<td><input type="radio" name="picType" id="picType2" value="2" <? if($picType==2) echo "checked";?>></td>
					<td colspan="2"><input name="picName" type="text" class="tabletext" value="<?=$picName?>" onFocus="form1.picType2.checked=true"/></td>
				</tr>
				<? }
				else { ?>
				<tr>
					<td><?=$PIC_selected?>
					</td>
					<td valign="bottom">
						<? if(!$ldiscipline->OnlyAdminSelectPIC || ($ldiscipline->OnlyAdminSelectPIC && $ldiscipline->IS_ADMIN_USER()) ) { ?>
						<?//=$linterface->GET_BTN($button_select, "button","newWindow('../choose_pic.php?fieldname=PIC[]',11)");?>
						<?=$linterface->GET_BTN($Lang['Btn']['Select'], "button", "newWindow('/home/common_choose/index.php?fieldname=PIC[]&permitted_type=1&DisplayGroupCategory=1&Disable_AddGroup_Button=1', 11)")?><br />
						<?=$linterface->GET_BTN($button_remove, "button","checkOptionRemove(document.form1.elements['PIC[]'])");?><br />
						
						<? } ?>
					</td>
				</tr>
				<? } ?>
				</table>
			</td>
		</tr>
		<tr>
            <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Discipline_Case_Record_Attachment?></td>
            <td> <?=$attachment_selected?></td>
        </tr>
        
		<? if ($ldiscipline->use_subject) {?>
	        <tr>
	            <td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Subject_name?></span></td>
	            <td><?=$select_subject?></td>
	        </tr>
        <? } ?>
        
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_UserRemark?></td>
			<td><?=$linterface->GET_TEXTAREA("remark", $remark);?></td>
		</tr>

        <? if ($sys_custom['eDiscipline']['APFollowUpRemarks']) {?>
            <tr>
                <td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['FollowUpRemark']?></td>
                <td><?=$linterface->GET_TEXTAREA("followUpRemark", $followUpRemark);?></td>
            </tr>
        <? } ?>
		
		<? if($sys_custom['eDiscipline']['MST']['AP_Extra_Fields']) {?>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['FileNumber']?></td>
			<td><input type="text" name="file_number" value="<?=$file_number?>"></td>
		</tr>
		
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['RecordNumber']?></td>
			<td><input type="text" name="record_number" value="<?=$record_number?>"></td>
		</tr>
		<? } ?>
		
		<tr>
			<td colspan="2" valign="middle" nowrap="nowrap" class="form_sep_title"><i> - <?=$eDiscipline["DisciplineDetails"]?> -</i></td>
		</tr>
		<tr valign="top">
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eDiscipline["Type"]?></span></td>
			<td>
				<input name="record_type" type="radio" id="record_type_award" value="1" <?=$record_type==1?"checked":""?> onClick="changeMeritType();"><label for="record_type_award"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ediscipline/icon_merit.gif" width="20" height="20" border="0" align="absmiddle"> <?=$i_Merit_Award?></label>
				<input name="record_type" type="radio" id="record_type_punishment" value="-1" <?=($record_type==-1  || $record_type=="")?"checked":""?> onClick="changeMeritType();"><label for="record_type_punishment"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ediscipline/icon_demerit.gif" width="20" height="20" border="0" align="absmiddle"> <?=$i_Merit_Punishment?></label>
			</td>
		</tr>
		<tr valign="top">
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$eDiscipline["RecordCategoryItem"]?><span class="tabletextrequire">*</span></span></td>
			<td><select name="CatID" onChange="changeCat(this.value);"></select> 
				- 
				<SELECT onChange="changeItem(this.value)" name="ItemID" id="ItemID">
					<OPTION value="0" selected>-- <?=$eDiscipline["SelectRecordItem"]?> --</OPTION>  
				</SELECT>
			</td>
		</tr>
		
		<tr id="MeritRow"  style="<?=($sys_custom['eDiscipline']['add_AP_records_hide_meritnconduct']?"display:none":"visibility:visible")?>">
			<td valign="top" nowrap="nowrap" class="formfieldtitle"> <?=$eDiscipline["MeritDemerit"]?></td>
			<td><?=$select_merit_num?> <select name="MeritType" id="MeritType" <?=($sys_custom['eDiscipline']['yy3'] && !$ldiscipline->IS_ADMIN_USER()?'disabled':'')?> <?=($sys_custom['eDiscipline']['CSCProbation']? "onchange='displayProbationSetting()'" : "")?>></select></td>
		</tr>
		
		<tr id="conductScoreTr" style="<?=(($ldiscipline->Hidden_ConductMark || $sys_custom['eDiscipline']['add_AP_records_hide_meritnconduct'] || ($sys_custom['Discipline_AP_Item_Tag_Seperate_Function'] && $TargetTagType=="Attitude"))?"display:none":"visibility:visible")?>">
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eDiscipline['Conduct_Mark']?></td>
			<td><span id="increment_de_conduct"><?=$Lang['eDiscipline']['Gain']?></span> <?=$select_conduct_mark?> <?=$ldiscipline->ConductMarkRemark?><?=($sys_custom['eDiscipline']['yy3']?'<div id="OverflowMarkWarnDiv" style="color:red;"></div>':'')?><td>
		</tr>
		
		<tr id="studyScoreTr" style="<?=($ldiscipline->UseSubScore && !($sys_custom['Discipline_AP_Item_Tag_Seperate_Function'] && $TargetTagType=="Conduct")?"visibility:visible":"display:none")?>"> 
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Subscore1?></td>
			<td>
				<span id="increment_de_subscore"><?=$Lang['eDiscipline']['Gain']?></span> <?=$select_study_score?>
				<? if($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']) { ?>
					<div id="MaxStudyScoreWarnDiv" style="color:red;"></div>
					<input type="hidden" name="maxStudyScore" value="<?=($maxStudyScore > 0? $maxStudyScore : 0)?>" />
		        <? } ?>
	        </td>
		</tr>
		
		<tr id="activityScoreTr" style="<?=($ldiscipline->UseActScore?"visibility:visible":"display:none")?>"> 
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['ActivityScore']?></td>
			<td><span id="increment_de_actscore"><?=$Lang['eDiscipline']['Gain']?></span> <?=$select_activity_score?></td>
		</tr>
		
		<? if($sys_custom['wscss_disciplinev12_ap_prompt_action']) { 
			$maxDate = $ldiscipline->MaxSchoolDayToCallForAction;
			$date = ($ActionDueDate=='') ? $ldiscipline->retrieveWaivePassDay(date('Y-m-d'), $maxDate) : $ActionDueDate;
		?>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['AP_ActionDueDate'] ?></td>
				<td><?=$linterface->GET_DATE_PICKER("ActionDueDate",$date)?></td>
			</tr>
		<? } ?>
		
		<? if($sys_custom['eDiscipline']['CSCProbation']) { ?>
			<tr id="ProbationRow" style="display:none">
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['CWCProbation']['SetAsProbation']?></td>
				<td><select name="applyProbation" id="applyProbation">
	     				<option value="0" <?=(($applyProbation=="" || $applyProbation=="0")? "selected" : "")?> > <?=$i_general_no?> </option>
	     				<option value="1" <?=($applyProbation=="1"? "selected" : "")?> > <?=$i_general_yes?> </option>
				</select></td>
			</tr>
		<? } ?>
	</table>
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<?php // [2015-0416-1040-06164] Hide Remark
		if(!$sys_custom['eDiscipline']['add_AP_records_skip_step3n4']){ ?>
			<tr>
				<td class="tabletextremark">(<?=$Lang['eDiscipline']['AddAPStopStep2_Remark']?>)</td>
			</tr>
	<?php } ?>
		<tr>
			<td align="center"> 
			<?php // [2015-0416-1040-06164] Hide Submit Button
			if($sys_custom['eDiscipline']['add_AP_records_skip_step3n4']){ ?>
				<?= $linterface->GET_ACTION_BTN($Lang['eDiscipline']['AddAPStopStep2'], "button", "skipStep2()", "submitBtn")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_back, "button", "this.form.action='new1.php';checkOptionAll(document.getElementById('PIC[]'));checkOptionAll(document.getElementById('Attachment[]'));this.form.submit();")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'")?>
			<?php }
			else { ?>
				<?= $linterface->GET_ACTION_BTN($button_continue, "submit", "document.form1.stop_step2.value=''","continueBtn")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_back, "button", "this.form.action='new1.php';checkOptionAll(document.getElementById('PIC[]'));checkOptionAll(document.getElementById('Attachment[]'));this.form.submit();")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'")?>
				&nbsp;<span class="tabletextremark">|</span>&nbsp;
				<?= $linterface->GET_ACTION_BTN($Lang['eDiscipline']['AddAPStopStep2'], "button", "skipStep2()", "submitBtn")?>
			<?php } ?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>

<br />

<!-- Step 1 data //-->
<? foreach($student as $k=>$d) { ?>
	<input type="hidden" name="student[]" value="<?=$d?>" />	
<? } ?>
<input type="hidden" name="semester_flag" id="semester_flag" value="1" />
<input type="hidden" name="temp_flag" id="temp_flag" value="0" />
<input type="hidden" name="stop_step2" id="stop_step2" value="" />
<input type="hidden" name="FolderLocation" id="FolderLocation" value="<?=$sessionTime?>" />
<input type="hidden" name="fieldname" id="fieldname" value="" />
<input type="hidden" name="ClickID" id="ClickID" value="" />
<input type="hidden" name="task" id="task" value="" />
<input type="hidden" name="sessionTime" id="sessionTime" value="<?=$sessionTime?>" />
<input type="hidden" name="detentionID" id="detentionID" value="<?=$detentionID?>" />
<input type="hidden" name="TargetTagType" value="<?=$TargetTagType?>" />
</form>

<script language="javascript">
<!--
changeMeritType();
changeCat(<?=$CatID?>);
changeItem(<?=$ItemID?>);
//-->
</script>

<?
print $linterface->FOCUS_ON_LOAD("form1.RecordDate");

$linterface->LAYOUT_STOP();
intranet_closedb();
?>