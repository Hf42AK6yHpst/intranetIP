<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Award_Punishment-Waive");

$RecordIDAry = array();

if(is_array($RecordID))
	$RecordIDAry = $RecordID;
else
	$RecordIDAry[] = $RecordID;
	
if(empty($RecordIDAry))	header("Location: new1.php");

$linterface = new interface_html();

# menu highlight setting
$CurrentPage = "Management_AwardPunishment";

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eDiscipline['Award_and_Punishment']);

# navigation bar
$PAGE_NAVIGATION[] = array($eDiscipline['RecordList'], "javascript:history.back()");
$PAGE_NAVIGATION[] = array($eDiscipline["WaiveRecord"], "");

# build html part
$i = 0;
$imgPath = "{$image_path}/{$LAYOUT_SKIN}";

foreach($RecordIDAry as $MeritRecordID)
{
	$i++;
	$reference = "";
	$datainfo = $ldiscipline->RETRIEVE_MERIT_RECORD_INFO_BY_ID($MeritRecordID);
	$data = $datainfo[0];
	$StudentID = $data['StudentID'];
	$lu = new libuser($StudentID);
	$thisClassNumber = $lu->ClassNumber;
	$thisClassName = $lu->ClassName.($thisClassNumber ? "-".$thisClassNumber : "");
	$namefield = getNameFieldWithLoginByLang();
	$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE RecordType = 1 AND UserID IN (". $data['PICID'] .") AND RecordStatus = 1 ORDER BY $namefield";
	$array_PIC = $ldiscipline->returnArray($sql);
	$PICAry = array();
	foreach($array_PIC as $k=>$d)
		$PICAry[] = $d[1];
	$PICName = empty($PICAry) ? "---" : implode("<br>",$PICAry);
	
	$actionAry = array();
	if($ldiscipline->HAS_DETENTION($MeritRecordID))
	{
		$actionAry[] = "<a href='#'>". $eDiscipline['Detention'] ."</a>";
	}
	if($data['NoticeID'])
	{
		$actionAry[] = "<a href='#'>". $ip20_enotice ."</a>";
	}
	if($data['fromConductRecords']==1) {
		$reference .= $ldiscipline->getAwardPunishHistory($MeritRecordID, $imgPath);	
	}
	$reference .= ($data['fromConductRecords']==1 && $data['CaseID'] != 0) ? "<br>" : "";
	if($data['CaseID'] != 0) {
		$reference .= "<a href=../case_record/view.php?CaseID=".$data['CaseID'].">Case</a>";	
	}
	$reference .= ($data['fromConductRecords']!=1 && $data['CaseID'] == 0) ? "---" : "";

	$row .= "<tr class='row_approved'>";
	$row .= "<td valign='top' >". $i."</td>";
	$row .= "<td valign='top'>". $thisClassName ."</td>";
	$row .= "<td valign='top'>". $lu->UserName()."</td>";
	$row .= "<td valign='top'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/". ($data['MeritType']==1? "icon_merit":"icon_demerit").".gif' width='20' height='20'></td>";
	$row .= "<td valign='top'>". $data['ProfileMeritCount'] ." ". $ldiscipline->RETURN_MERIT_NAME($data['ProfileMeritType']) ."</td>";
	$row .= "<td valign='top'><a href='detail.php?id=$MeritRecordID'>". $data['ItemText'] ."</a></td>";
	$row .= "<td valign='top'>". $data['RecordDate'] ."</td>";
	$row .= "<td valign='top'>". $PICName ."</td>";
	$row .= "<td valign='top'>".$reference."</td>";
	$row .= "<td align='right' valign='top'>". implode("<br>",$actionAry) ."&nbsp;</td>";
	$row .= "<td valign='top' nowrap><input name='reason_{$MeritRecordID}' type='text' class='textboxtext' value='' ></td>";
	$row .= "</tr>";
}


# Start layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
<!--
function apply_all()
{
	<? foreach($RecordIDAry as $MeritRecordID)	{ ?>
		document.form1.reason_<?=$MeritRecordID?>.value=document.form1.top_reason.value;
	<? }?>
}
//-->
</script>
<form name="form1" method="POST" action="waive_update.php">

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td class="navigation">
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
		</table>
	</td>
</tr>

<tr>
	<td align="center">
		<table width="90%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="form_sep_title"><i>- <?=$eDiscipline["SelectedRecord"]?> -</i></td>
		</tr>
		<tr>
			<td align="center">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr class="tabletop">
					<td align="center">#</td>
					<td><?=$i_general_class?></td>
					<td><?=$i_general_name?></td>
					<td>&nbsp;</td>
					<td><?=$eDiscipline["Record"]?></td>
					<td><?=$i_Merit_Reason?></td>
					<td><?=$i_EventDate?></td>
					<td><?=$i_Discipline_PIC?></td>
					<td><?=$i_Discipline_System_Award_Punishment_Reference?></td>
					<td align="center"><?=$eDiscipline["Action"]?></td>
					<td><?=$eDiscipline["WaiveReason"]?><br></td>
				</tr>
				
				<tr class="tablebottom">
					<td width="15" align="center">&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td width="100">&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td align="center">&nbsp;</td>
					<td><table width="100%" border="0" cellpadding="0" cellspacing="2" class="layer_table">
						<tr>
							<td><input name="top_reason" type="text" class="textboxtext" value="" ></td>
							<td width="25" align="center"><a href="#" onClick="javascript:apply_all();"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_assign.gif" width="12" height="12" border="0"></a></td>
						</tr>
					</table></td>
				</tr>
			
				<?=$row?>
		
		
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_continue, "submit")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back();")?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>

<br />

<? foreach($RecordIDAry as $k=>$d) { ?>
	<input type="hidden" name="RecordID[]" value="<?=$d?>" />	
<? } ?>

</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();

?>