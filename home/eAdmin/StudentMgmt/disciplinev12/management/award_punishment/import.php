<?php
// Modifying by:

###############################################
#
#   Date    :   2020-11-06  (Bill)  [2020-0824-1151-40308]
#               - Support Follow-up Feedback Import     ($sys_custom['eDiscipline']['APFollowUpRemarks'])
#
#	Date	:	2017-06-12 (Bill)	[2016-1207-1221-39240]
#				- Support Activity Score Import
#				- Fixed CSV Format display
#
#	Date	:	2015-06-17 (Shan)
#				Updated UI standard
#
#	Date	:	2015-01-19 (Bill)
#				For language = "gb", get image of language = "b5"
#
#	Date	:	2013-11-21 (YatWoon)
#				Customization: Add 2 extra fields: FileNumber, RecordNumber, $sys_custom['eDiscipline']['MST']['AP_Extra_Fields']	[Case#2013-0520-1008-41072]
#
#	Date	:	2013-05-15 (YatWoon)
#	Datail	:	add RecordInputDate (customization for yy3 - $sys_custom['eDiscipline']['yy3'])
#
###############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");

intranet_auth();
intranet_opendb();
 
$ldiscipline = new libdisciplinev12();

# Check Access Right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Award_Punishment-New");

$linterface = new interface_html();
$lsp = new libstudentprofile();

# Menu Highlight Setting
$CurrentPage = "Management_AwardPunishment";
$CurrentPageArr['eDisciplinev12'] = 1;

# Left Menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Tag Information
$TAGS_OBJ[] = array($eDiscipline['Award_and_Punishment']);

# Navigation Information
$navigationAry[] = array($eDiscipline['Award_and_Punishment'], 'index.php');
$navigationAry[] = array($Lang['Btn']['Import']);
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($navigationAry);

# Step Information
$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($i_general_confirm_import_data, 0);
$STEPS_OBJ[] = array($i_general_complete, 0);

# CSV File
if(!$ldiscipline->use_subject)
	$con = "_no_subject";
if($ldiscipline->UseSubScore)
	$con .= "_subscore";
if($ldiscipline->UseActScore)
	$con .= "_actscore";
if($sys_custom['eDiscipline']['CustomizeAPImport'])
	$con .= "_lsc";
if($sys_custom['eDisciplinev12']['CheungShaWanCatholic']['AP_eNoticeChecked'])
	$con .= "_cswcss";
if($sys_custom['eDiscipline']['yy3'])
	$con .= "_yy3";
if($sys_custom['eDiscipline']['MST']['AP_Extra_Fields'])
	$con .= "_mst";
// [2020-0824-1151-40308]
if($sys_custom['eDiscipline']['APFollowUpRemarks'])
    $con .= "_followup";
//$csvFile = ($ldiscipline->use_subject)?"award_punishment.csv":"award_punishment_no_subject.csv";
$csvFile = "award_punishment$con.csv";
//debug_pr($csvFile);

$layer_html = "<div id=\"ref_list\"  class=\"selectbox_layer\"'></div>";
//$format_str = $layer_html; 
$image_lang = ($intranet_session_language == "gb")? "b5" : $intranet_session_language;
//$format_str .= "<p><span id=\"GM__to\"><strong>".$iDiscipline['Import_Instruct_Main']."</strong></span></p>";
//$format_str .= "<div class=\"p\"><br/><ol type=\"a\"><li>".$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_a_1']."
//				<strong>&lt;<a class=\"tablelink\" href=\"". GET_CSV($csvFile) ."\">".$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_a_2']."</a>&gt;</strong>".
//				$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_a_3']."</li>";
//$format_str .= "<li>".$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_b_1']."</span><br/>";
//$format_str .= "<img src=\"".$image_path."/".$LAYOUT_SKIN."/discipline/apcasestud_import_illus_$image_lang.png\"/><br/><div class=\"note\">";
//$format_str .= "<b>".$iDiscipline['Import_Instruct_Note']."</b><br/>";
//$format_str .= "<ul><li>".$Lang['eDiscipline']['Award_Punishment_Import_Instruct_Note_1_1'];
//$format_str .= "<strong>&lt;<a href=\"javascript:show_ref_list('ItemCode','ic_click')\" class=\"tablelink\">".$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_Note_1_2']."</a>&gt;</strong><span id=\"ic_click\">&nbsp;</span>";
// $format_str .= "<li><span id=\"GM__date\">".$Lang['eDiscipline']['Good_Conduct_and_Misconduct_Import_Instruct_Note_2_1']."</span></li></ul></div></li><br/>";
//$format_str .= "<li><span id=\"GM__savee\">".$Lang['eDiscipline']['Good_Conduct_and_Misconduct_Import_Instruct_c_1']."</span></li></ol></div>";
//$format_str .=  "</br></br>";
//$format_str .= "</li>";


//if($intranet_session_language!='en'){
//	$format_str .= $iDiscipline['Award_Punishment_Import_Instruct_Note_1_2_1'];
//	$format_str .=  "</br>";
//}

//if($ldiscipline->use_subject)
//{
//	$format_str .= "<li>".$iDiscipline['Award_Punishment_Import_Instruct_Note_2_1']."<strong>&lt;<a href=\"javascript:show_ref_list('SubjCode','sc_click')\" class=\"tablelink\">".$iDiscipline['Award_Punishment_Import_Instruct_Note_2_2']."</a>&gt</strong><span id=\"sc_click\">&nbsp;</span>".$iDiscipline['Award_Punishment_Import_Instruct_Note_2_3']."</li>";
//}
//$subcode_ref .= $iDiscipline['Award_Punishment_Import_Instruct_Note_3_1'];
//$subcode_ref .= "<strong>&lt;<a href=\"javascript:show_ref_list('SubjCode','sc_click')\" class=\"tablelink\">".$Lang['eDiscipline']['Good_Conduct_and_Misconduct_Import_Instruct_Note_1_4']."</a>&gt;</strong><span id=\"sc_click\">&nbsp;</span>";

### Data Column
// Title
$DataColumnTitleArr[] = $Lang['eDiscipline']['Import_GM_Column']['0'];
$DataColumnTitleArr[] = $Lang['General']['ClassNumber'];
$DataColumnTitleArr[] = $Lang['eDiscipline']['EventDate'];
$DataColumnTitleArr[] = $Lang['eDiscipline']['Import_GM_Column']['3'];
if($ldiscipline->use_subject) {
	$DataColumnTitleArr[] = $i_Discipline_Subject;
}
$DataColumnTitleArr[] = $Lang['eDiscipline']['ConductMark'];
if($ldiscipline->UseSubScore) {
	$DataColumnTitleArr[] = $i_Discipline_System_Subscore1;
}
if($ldiscipline->UseActScore) {
	$DataColumnTitleArr[] = $Lang['eDiscipline']['ActivityScore'];
}
$DataColumnTitleArr[] = $Lang['eDiscipline']['Quantity'];
$DataColumnTitleArr[] = $Lang['General']['Type'];
$DataColumnTitleArr[] = $Lang['eDiscipline']['Import_GM_Column']['4'];
$DataColumnTitleArr[] = $Lang['eDiscipline']['Import_GM_Column']['5'];
// [2020-0824-1151-40308]
if($sys_custom['eDiscipline']['APFollowUpRemarks']) {
    $DataColumnTitleArr[] = $Lang['eDiscipline']['FollowUpRemark'];
}

// Property
//$DataColumnPropertyArr = array(1,1,1,1,1,1,1,1,1,3);
$DataColumnPropertyArr = array(1,1,1,1);
if($ldiscipline->use_subject) {
	$DataColumnPropertyArr[] = 1;
}
$DataColumnPropertyArr[] = 1;
if($ldiscipline->UseSubScore) {
	$DataColumnPropertyArr[] = 1;
}
if($ldiscipline->UseActScore) {
	$DataColumnPropertyArr[] = 1;
}
$DataColumnPropertyArr[] = 1;
$DataColumnPropertyArr[] = 1;
$DataColumnPropertyArr[] = 1;
$DataColumnPropertyArr[] = 3;
// [2020-0824-1151-40308]
if($sys_custom['eDiscipline']['APFollowUpRemarks']) {
    $DataColumnPropertyArr[] = 3;
}
 
// Remarks
$DataColumnRemarksArr = array();
$DataColumnRemarksArr[] = '';
$DataColumnRemarksArr[] = '';
$DataColumnRemarksArr[] = '<span class="tabletextremark">&nbsp;'.$Lang['eDiscipline']['Import_GM_Remark'][2].'</span>';
$DataColumnRemarksArr[] =  " <a href=\"javascript:show_ref_list('ItemCode','ic_click')\" class=\"tablelink\">[".$Lang['eDiscipline']['Import_GM_Reference'][3][0]."]</a><span id=\"".$Lang['eDiscipline']['Import_GM_Reference'][3][2]."\">&nbsp;</span>";
if($ldiscipline->use_subject) {
	$DataColumnRemarksArr[] = "<a href=\"javascript:show_ref_list('SubjCode','sc_click')\" class=\"tablelink\">[".$Lang['SysMgr']['Homework']['SubjectCode']."]</a><span id=\"sc_click\">&nbsp;</span>"; 
}
$DataColumnRemarksArr[] = '';
$DataColumnRemarksArr[] = '';
if($ldiscipline->UseSubScore) {
	$DataColumnRemarksArr[] = '';
}
if($ldiscipline->UseActScore) {
	$DataColumnRemarksArr[] = '';
}
$DataColumnRemarksArr[] = " <a href=\"javascript:show_ref_list('MeritCode','mc_click')\" class=\"tablelink\">[".$iDiscipline['Award_Punishment_Import_Instruct_Note_3_2']."]</a><span id=\"mc_click\">&nbsp;</span>";
$DataColumnRemarksArr[] = '<span class="tabletextremark">('.$Lang['SysMgr']['SubjectClassMapping']['ImportCSVField']['6'].')</span>';
//$DataColumnRemarksArr[3] = '<span class="tabletextremark">&nbsp;'.$Lang['eDiscipline']['Import_GM_Remark'][2].'</span>';
//$DataColumnRemarksArr[4] =  " <a href=\"javascript:show_ref_list('".$Lang['eDiscipline']['Import_GM_Reference'][3][1]."','".$Lang['eDiscipline']['Import_GM_Reference'][3][2]."')\">[".$Lang['eDiscipline']['Import_GM_Reference'][3][0]."]</a><span id=\"".$Lang['eDiscipline']['Import_GM_Reference'][3][2]."\">&nbsp;</span>";
$DataColumn = $linterface->Get_Import_Page_Column_Display($DataColumnTitleArr, $DataColumnPropertyArr, $DataColumnRemarksArr);

$format_str .= $DataColumn;
if($ldiscipline->use_subject) {
	$format_str .= "<li>".$iDiscipline['Award_Punishment_Import_Instruct_Note_2_1']."<strong>&lt;<a href=\"javascript:show_ref_list('SubjCode','sc_click')\" class=\"tablelink\">".$iDiscipline['Award_Punishment_Import_Instruct_Note_2_2']."</a>&gt</strong><span id=\"sc_click\">&nbsp;</span>".$iDiscipline['Award_Punishment_Import_Instruct_Note_2_3']."</li>";
}
//$format_str .=  "</br></br>";
//$format_str .= $iDiscipline['Award_Punishment_Import_Instruct_Note_3_1']."</br><a href=\"javascript:show_ref_list('MeritCode','mc_click')\" class=\"tablelink\">".$iDiscipline['Award_Punishment_Import_Instruct_Note_3_2']."</a><span id=\"mc_click\">&nbsp;</span></br>".$iDiscipline['Award_Punishment_Import_Instruct_Note_3_3'];

$linterface->LAYOUT_START();
?>

<script language="JavaScript">
var ClickID = '';
var callback_show_ref_list = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('ref_list').innerHTML = tmp_str;
	    DisplayPosition();
	}
}

function show_ref_list(type,click)
{
	ClickID = click;
	document.getElementById('task').value = type;
	
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	
	var path = "ajax_ap.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_show_ref_list);
}

function Hide_Window(pos)
{
  	document.getElementById(pos).style.visibility='hidden';
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function DisplayPosition(){
	var posleft = getPosition(document.getElementById(ClickID),'offsetLeft') ;
	var postop = getPosition(document.getElementById(ClickID),'offsetTop');
	var divwidth = document.getElementById('ref_list').clientWidth;
	
	$('#ref_list').removeAttr('style');
	if(posleft+divwidth > document.body.clientWidth){
		document.getElementById('ref_list').style.right= "1px";
	}
	else{
		document.getElementById('ref_list').style.left= posleft+"px";
	}
		
  	document.getElementById('ref_list').style.top= postop+"px";
 	document.getElementById('ref_list').style.visibility='visible';
}
</script>

<br />
<form name="form1" method="post" action="import_result.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" >
	<tr>
		<td align="right"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
	</tr>
	
	<tr>
		<td><?=$htmlAry['navigation']?></td>
	</tr>
    
	<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	
	<tr>
	    <td>
        <table width="91%" border="0" cellpadding="0" cellspacing="0" align="center" >
        <tr>
            <td>
				<table class="form_table_v30">
				<tr>
				    <td width="100%" valign="top" nowrap="nowrap" class="field_title" ><?=$linterface->RequiredSymbol().$Lang['General']['SourceFile']." <span class='tabletextremark'>".$Lang['General']['CSVFileFormat']."</span>"?>: </td>
			  	    <td class="tabletext"><input class="file" type="file" name="csvfile"></td>
			  	</tr>		
				
				<tr>
				    <td width="100%" valign="top" nowrap="nowrap" class="field_title"><?= $Lang['General']['CSVSample'] ?></td>
				    <td valign="top" nowrap="nowrap" colspan='2' class="tabletext">
                        <a id="SampleCsvLink" class="tablelink" href="<?=GET_CSV($csvFile)?>" target="_blank">
                        <img src='<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/icon_files/xls.gif' border='0' align='absmiddle' hspace='3'>
                        <?=$i_general_clickheredownloadsample?>
                    </td>
				</tr>
				
				<tr>
				    <td width="35%" valign="top" nowrap="nowrap" class="field_title" ><?= $i_general_Format ?></td>
		  		    <td class="tabletext"><?=$format_str?></td>
		  		</tr>
		        </table>
            </td>
		</tr>
		    
        <tr>
            <td>
   			    <table width="99%" border="0" cellpadding="0" cellspacing="2" align="center">
		    	<tr>
                    <td class="tabletext"><?= $linterface->MandatoryField();?> </td>
		    	</tr>
   	   
		   		<tr>
                    <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>

		  		<tr>
                    <td align="center">
                        <?= $linterface->GET_ACTION_BTN($button_continue, "submit") ?>&nbsp;
                        <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='index.php'") ?>
                    </td>
				</tr>
			    </table>
		    </td>
        </tr>
		</table>
		</td>
    </tr>
</table>

<?=$layer_html?>

<input type="hidden" id="task" name="task"/>
</form>
<br />

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>