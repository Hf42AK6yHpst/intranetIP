<?php
// Using:

##### Change Log [Start] #####
#
#   Date    :   2020-11-06  (Bill)  [2020-0824-1151-40308]
#               Added "follow_up_remark" to display layer for Follow-up Feedback
#
#	Date	:	2015-07-28	(Bill)	[2015-0611-1642-26164]
#				Added "rehabil" to display layer for Rehabilitation Program 
#
#	Date	:	2012-04-11 (Henry Chow)
#				add "loadConversionPeriod"
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

//header("Content-Type:text/html;charset=utf-8");

$ldiscipline = new libdisciplinev12();

if($flag=='detention') {
	$tempLayer = $ldiscipline->getDetentionLayer($RecordID, "{$image_path}/{$LAYOUT_SKIN}");
} else if($flag=='waived') {
	$tempLayer = $ldiscipline->getWaivedInfo($RecordID, "{$image_path}/{$LAYOUT_SKIN}");
} else if($flag=='record_remark') {
	$tempLayer = $ldiscipline->getRecordRemarkLayer($RecordID, "{$image_path}/{$LAYOUT_SKIN}");
} else if($flag=='history') {
	$tempLayer = $ldiscipline->getAwardPunishHistoryDetailPage($RecordID, "{$image_path}/{$LAYOUT_SKIN}");
} else if($flag=='loadConversionPeriod') {
	$AcademicYearID = (!isset($AcademicYearID) || $AcademicYearID=="") ? Get_Current_Academic_Year_ID() : $AcademicYearID;
	$PeriodAry = $ldiscipline->RETRIEVE_CONDUCT_CATEGORY_PERIOD_LIST($recordType,$CatID);
	$NoOfPeriod = count($PeriodAry);
	//debug_pr($PeriodAry);
	$startDate = getStartDateOfAcademicYear($AcademicYearID);
	$endDate = getEndDateOfAcademicYear($AcademicYearID);
	$thisPeriod = array();	
	for($i=0; $i<$NoOfPeriod; $i++) {
		list($thisCatId, $thisPeriodId, $thisStartDate, $thisEndDate) = $PeriodAry[$i];
		$startDate = substr($startDate,0,10);
		$endDate = substr($endDate,0,10);
		
		if($thisStartDate>=$startDate && $thisEndDate<=$endDate) {
			$thisPeriod[$thisPeriodId] = "$thisStartDate $i_To $thisEndDate";
		}
	}
	asort($thisPeriod);
	$a = array();
	foreach($thisPeriod as $_f=>$_v) {
		$a[] = array($_f, $_v);
	}
	$thisPeriod = $a;
	
	if(count($thisPeriod)>0) {
		$tempLayer = getSelectByArray($thisPeriod, ' name="PeriodID" id="PeriodID"', $selectedPeriod, 0, 0, $Lang['Btn']['All']);
	} else {
		$tempLayer = $Lang['General']['EmptySymbol'];	
	}
} 
// [2015-0611-1642-26164]
else if($flag=='rehabil') {
	$tempLayer = $ldiscipline->getRehabiInfoLayer($RecordID, "{$image_path}/{$LAYOUT_SKIN}");
}
// [2020-0824-1151-40308]
else if($flag=='follow_up_remark') {
    $tempLayer = $ldiscipline->getFollowUpRemarkLayer($RecordID, "{$image_path}/{$LAYOUT_SKIN}");
}

echo $tempLayer;

intranet_closedb();

?>