<?php
// Modifying by: henry

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();


$ldiscipline = new libdisciplinev12();

$datainfo = $ldiscipline->RETRIEVE_MERIT_RECORD_INFO_BY_ID($RecordID);
$data = $datainfo[0];

$NoticeID = $data['NoticeID'];

if(isset($NoticeID) && $NoticeID!="") {

	# delete Notice & Reply Slip
	$ldiscipline->removeNotice($NoticeID);
		
	# update Merit Record
	$dataAry['NoticeID'] = "";
	$dataAry['TemplateID'] = "";
	$dataAry['TemplateOtherInfo'] = "";
	$ldiscipline->UPDATE_MERIT_RECORD($dataAry, $RecordID);
}

intranet_closedb();

$url = "detail.php?id=$RecordID&msg=update&num_per_page=$num_per_page&pageNo=$pageNo&order=$order&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&s=$s&clickID=$clickID&fromPPC=$fromPPC&passedActionDueDate=$passedActionDueDate&SchoolYear2=$SchoolYear2&semester2=$semester2&targetClass2=$targetClass2&MeritType2=$MeritType2";

header("Location: $url");
?>
