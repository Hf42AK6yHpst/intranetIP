<?php
// Modifying by: 

########## Change Log ###############
#
#	Date	:	2017-03-02	(Bill)	[2016-0823-1255-04240]
#				default uncheck checkbox "Send email to notify parents"	($sys_custom['eDiscipline']['DefaultUncheckSendEmailCheckbox'])
#
#	Date	:	2015-05-04 (Bill)
#				Create file	[2014-1216-1347-28164]
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libucc.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

$linterface = new interface_html();

if(empty($RecordID)) header("Location: index.php");

# Menu
$CurrentPage = "Management_AwardPunishment";
$CurrentPageArr['eDisciplinev12'] = 1;

# Left Menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Tag
$TAGS_OBJ[] = array($eDiscipline['Award_and_Punishment']);
$PAGE_NAVIGATION[] = array($i_Notice_New, "");

$js = "";
$TableNotice = "";
$validRecordCount = 0;

# Record data
$sql = "SELECT * FROM DISCIPLINE_MERIT_RECORD WHERE RecordID IN ('".implode("','", (array)$RecordID)."')";
$ap_records = $ldiscipline->returnArray($sql);

# Template Category
$catTmpArr = $ldiscipline->TemplateCategory();
if(is_array($catTmpArr))
{
	$catArr[0] = array("0","-- $button_select --");
	foreach($catTmpArr as $Key=>$Value)
	{
		$NoticeTemplateAvaTemp = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO("",1, $Key);
		if(!empty($NoticeTemplateAvaTemp))
			$catArr[] = array($Key,$Value);
	}
}

# Apply All - Template category drop down list
if(sizeof($ap_records) > 1){
	$catSelection0 = $linterface->GET_SELECTION_BOX($catArr, 'id="CategoryID0" name="CategoryID0" onChange="changeCat(this.value, 0)"', "", "");
}

# Loop AP records
for ($i=1; $i<=sizeof($ap_records); $i++) {
	# AP record data
	$data = $ap_records[$i-1];
	$record_id = $data['RecordID'];
	$student_id = $data['StudentID'];
	$item_name = $data['ItemText'];
	$record_date = $data['RecordDate'];
	$item_type = $data['MeritType'];
	$template_id = $data['TemplateID'];
	$notice_id = $data['NoticeID'];
	
	# Current AP record with notice template ($viewOnly = true)
	$viewOnly = false;
	if($template_id > 0)
		$viewOnly = true;
	
	# Category of AP records
	$cat_data = $ldiscipline->getAPItemInfoBYItemID($data['ItemID']);
	$cat_id = $cat_data['CatID'];
	$cat_code = $cat_data['ItemCode'];
	
	# eNotice template
	$ap_info = $ldiscipline->getAPCategoryInfoBYCatID($cat_id);
	$TemplateID = $template_id? $template_id : $ap_info['TemplateID'];
	$TemplateAry = $ldiscipline->retrieveTemplateDetails($TemplateID);
	$TemplateCategoryID = $TemplateAry[0]['CategoryID'];
	$TemplateTitle = $TemplateAry[0]['Title'];
	
	# Student Info
	$resultStudentName = $ldiscipline->getStudentNameByID($student_id);
	list($TmpUserID, $TmpName, $TmpClassName, $TmpClassNumber) = $resultStudentName[0];
	
	# AP records - Template category drop down list
	${"catSelection".$i} = $linterface->GET_SELECTION_BOX($catArr, 'id="CategoryID'.$i.'" name="CategoryID'.$i.'" onChange="changeCat(this.value, '.$i.')"'.($viewOnly?" disabled":""), "", $TemplateCategoryID);	
	if($viewOnly)
	{
		${"templateSelection".$i} = " <select name=\"SelectNotice$i\" id=\"SelectNotice$i\" disabled><option value=\"".$template_id."\"> ".$TemplateTitle." </option></select>\n";
	}
	else 
	{
		${"templateSelection".$i} = " <select name=\"SelectNotice$i\" id=\"SelectNotice$i\"><option value=\"0\">-- $button_select --</option></select>\n";
		if($TemplateCategoryID!="0" && $TemplateCategoryID!=""){
			$js .= "changeCat('{$TemplateCategoryID}', $i);itemSelected('$TemplateID', $i, '$TemplateID');\n";
		}
		$validRecordCount++;
	}
	
	# AP records Table
	$TableNotice .= "<table align=\"center\" width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">\n";
	// Section Title
	$TableNotice .= "<tr>\n";
	$TableNotice .= "<td colspan=\"2\" valign=\"top\" nowrap>\n";
	$TableNotice .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
	$TableNotice .= "<tr>\n";
	$TableNotice .= "<td>$i. <img src=\"$image_path/$LAYOUT_SKIN/ediscipline/icon_stu_ind.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"> <span class=\"sectiontitle\">$TmpClassName-$TmpClassNumber $TmpName</span></td>\n";
	if(!$notice_id){
		$TableNotice .= "<td align=\"right\"><span class=\"sectiontitle\">\n";
		$TableNotice .= $linterface->GET_BTN($button_view_template, "button", "javascript:previewNotice($i);");
		$TableNotice .= "</span></td>\n";
	}
	$TableNotice .= "</tr>\n";
	$TableNotice .= "<tr>\n";
	$TableNotice .= "<td>&nbsp;&nbsp;&nbsp; <img src=\"$image_path/$LAYOUT_SKIN/ediscipline/".($item_type==1?"icon_merit":"icon_demerit").".gif\" width=\"20\" height=\"20\" align=\"absmiddle\"> <span class=\"sectiontitle\">$cat_code - $item_name - ($record_date)</span></td>\n";
	$TableNotice .= "</tr>\n";
	$TableNotice .= "</table>\n";
	$TableNotice .= "</td>\n";
	$TableNotice .= "</tr>\n";
	// eNotice status
	// records with released eNotice
	if($notice_id){
		// preview eNotice
		$lc = new libucc($notice_id);
		$result = $lc->getNoticeDetails();
		
		$eNoticePath = "<a href=\"#\" class=\"tabletext\" onClick=\"newWindow('".$result['url']."', 1)\">".$eDiscipline["PreviewNotice"]."<img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"></a>";
		$TableNotice .= "<tr>\n";
		$TableNotice .= "<td valign=\"top\">".$Lang['General']['Status']."</td>\n";
		$TableNotice .= "<td valign=\"top\">$eNoticePath</td>\n";
		$TableNotice .= "</tr>\n";
	}
	// records that set eNotice template
	else if($viewOnly){
		$TableNotice .= "<tr>\n";
		$TableNotice .= "<td valign=\"top\">".$Lang['General']['Status']."</td>\n";
		$TableNotice .= "<td valign=\"top\">".$eDiscipline["SendNotice"]."</td>\n";
		$TableNotice .= "</tr>\n";
	}
	// eNotice Template
	$TableNotice .= "<tr valign=\"top\" class=\"tablerow1\">\n";
	$TableNotice .= "<td width=\"20%\" nowrap class=\"formfieldtitle\"><span class=\"tabletext\">$i_Discipline_Usage_Template <span class=\"tabletextrequire\">*</span></span></td>\n";
	$TableNotice .= "<td>\n";
	// hidden input field
	$TableNotice .= "<input type=\"hidden\" name=\"RecordID$i\" id=\"RecordID$i\" value=\"".$record_id."\" />";
	$TableNotice .= "<input type=\"hidden\" id=\"StudentID$i\" name=\"StudentID$i\" value=\"".$TmpUserID."\" />";
	$TableNotice .= "<input type=\"hidden\" id=\"StudentName$i\" name=\"StudentName$i\" value=\"".$TmpName."\" />";
	$TableNotice .= "<input type=\"hidden\" id=\"withNoticeTemplate$i\" name=\"withNoticeTemplate$i\" value=\"".$viewOnly."\" />";
	// Template category drop down list
	$TableNotice .= ${"catSelection".$i}.${"templateSelection".$i}."\n";
	$TableNotice .= "</td>\n";
	$TableNotice .= "</tr>\n";
	// Additional Info
	if(!$viewOnly){
		$TableNotice .= "<tr class=\"tablerow1\">\n";
		$TableNotice .= "<td valign=\"top\" nowrap class=\"formfieldtitle\">$i_Discipline_Additional_Info</td>\n";
		$TableNotice .= "<td valign=\"top\">".$linterface->GET_TEXTAREA("TextAdditionalInfo".($i), "")."</td>";
		$TableNotice .= "</tr>\n";
		$TableNotice .= "<tr class=\"tablerow1\">\n";
		$TableNotice .= "<td valign=\"top\">&nbsp;</td>\n";
		$TableNotice .= "<td valign=\"top\"><input type='checkbox' name=\"emailNotify$i\" id=\"emailNotify$i\" value='1' ".($sys_custom['eDiscipline']['DefaultUncheckSendEmailCheckbox']? "" : "checked")."><label for=\"emailNotify$i\"'>".$Lang['eDiscipline']['EmailNotifyParent']."</label></td>\n";
		$TableNotice .= "</tr>\n";
	}
	
	if ($i != sizeof($ap_records)) {
		$TableNotice .= "<tr>\n";
		$TableNotice .= "<td height=\"1\" colspan=\"2\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\"></td>\n";
		$TableNotice .= "</tr>\n";
	}
	$TableNotice .= "</table>\n";
}

// Preset template items (for javascript)
for ($i=0; $i<sizeof($catArr); $i++) {
	$result = $ldiscipline->getDetentionENoticeByCategoryID("DISCIPLINE", $catArr[$i][0]);
	for ($j=0; $j<=sizeof($result); $j++) {
		if ($j==0) {
			$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]."Value = new Array(".(sizeof($result)+1).");\n";
			$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]." = new Array(".(sizeof($result)+1).");\n";
			$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."Value[$j] = \"0\"\n";
			$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."[$j] = \"-- $button_select --\"\n";
		} else {
			$tempTemplate = $result[$j-1][1];
			$tempTemplate=str_replace("&lt;", "<", $tempTemplate);
	        $tempTemplate=str_replace("&gt;", ">", $tempTemplate);
	        $tempTemplate=str_replace("&#039;","'",$tempTemplate);
	        $tempTemplate=str_replace("&amp;", "&", $tempTemplate);

			$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."Value[$j] = \"".$result[$j-1][0]."\"\n";
			$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."[$j] = \"".$tempTemplate."\"\n";
		}
	}
}

# Start layout
$linterface->LAYOUT_START();

?>

<!--<script type="text/javascript" src = "<?=$PATH_WRT_ROOT?>templates/2007a/js/jslb_ajax.js" charset = "utf-8"></script>-->

<script language="Javascript">
<!--
<?=$jTemplateString?>

function applyNoticeToAll(recordCount){
	for(var i=1; i<=recordCount; i++){
		if(document.getElementById('withNoticeTemplate'+i).value==1){
			continue;
		}
		
		changeCat(document.getElementById('CategoryID0').value, i);
		document.getElementById('CategoryID'+i).value = document.getElementById('CategoryID0').value;
		document.getElementById('SelectNotice'+i).value = document.getElementById('SelectNotice0').value;
		document.getElementById('TextAdditionalInfo'+i).value = document.getElementById('TextAdditionalInfo0').value;		
		document.getElementById('emailNotify'+i).checked = document.getElementById('emailNotify0').checked;
	}
}

function checkForm(recordCount)
{	
	for(var i=1; i<=recordCount; i++){
		if(document.getElementById('SelectNotice'+i).value==0) {
			document.getElementById('submit_btn').disabled  = false; 
			document.getElementById('submit_btn').className  = "formbutton_v30 print_hide"; 
			alert("<?=$i_alert_pleaseselect." ".$i_Discipline_Template?>");
			return false;
		}
	}
	document.form1.submit();
}

function jsCancel()
{
	window.location="index.php?field=<?=$field?>&order=<?=$order?>&pageNo=<?=$pageNo?>&numPerPage=<?=$numPerPage?>&num_per_page=<?=$num_per_page?>&page_size_change=<?=$page_size_change?>&SchoolYear=<?=$SchoolYear?>&SchoolYear=<?=$SchoolYear2?>&semester=<?=$semester?>&semester=<?=$semester2?>&targetClass=<?=$targetClass?>&targetClass=<?=$targetClass2?>&pic=<?=$pic?>&s=<?=$s?>&MeritType=<?=$MeritType?>&approved=<?=$approved2?>&waitApproval=<?=$waitApproval2?>&released=<?=$released2?>&waived=<?=$waived2?>&rejected=<?=$rejected2?>&clickID=<?=$clickID?>&fromPPC=<?=$fromPPC?>&passedActionDueDate=<?=$passedActionDueDate?>";
}

function changeCat(cat, selectIdx){
	var x = document.getElementById("SelectNotice"+selectIdx);

	if(x)
	{
		var listLength = x.length;
	}
	for (var i=listLength-1; i>=0; i--) {
		x.remove(i);
	}

	try {
		var tmpCatLength = eval("jArrayTemplate"+cat).length;
	} catch (err) {
		var tmpCatLength = 0;
	}
	
	for (var j=0; j<tmpCatLength; j++) {
		var y = document.createElement('option');
		y.text = eval("jArrayTemplate"+cat)[j];
		tempText = eval("jArrayTemplate"+cat)[j];
		y.value = eval("jArrayTemplate"+cat+"Value")[j];
		tempValue = eval("jArrayTemplate"+cat+"Value")[j];
		try {
			x.add(y,null); // standards compliant
			//x.options[x.options.length] = new Option(tempText, tempValue);
		} catch(ex) {
			x.add(y); // IE only
			//x.options[x.options.length] = new Option(tempText, tempValue);
		}
	}
}

function itemSelected(itemID, selectIdx, templateID) 
{
	var obj = document.form1;
	
	var item_length = eval("obj.SelectNotice"+selectIdx+".length");
	for(var i=0; i<item_length; i++) {
		if(obj.elements['SelectNotice'+selectIdx].options[i].value==templateID) {
			obj.elements['SelectNotice'+selectIdx].selectedIndex = i;
			//alert(i);
			break;	
		}
	}
}

function previewNotice(index){
	var templateID = document.getElementById("SelectNotice"+index).value;
	if(templateID==0) {
		alert("<?=$i_alert_pleaseselect." ".$i_Discipline_Template?>");
	} else {
		var studentName = '';
		if(index!=0) {
			studentName = document.getElementById("StudentName"+index).value;
		}
		if(document.getElementById("TextAdditionalInfo"+index)){
			var additionInfo = document.getElementById("TextAdditionalInfo"+index).value;
		}
		newWindow("preview_notice.php?tid="+templateID+"&pv_studentname="+studentName+"&pv_additionalinfo="+additionInfo);
	}
}
//-->
</script>

<form name="form1" method="POST" action="batch_notice_new_update.php">

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td class="navigation">
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="left">
		<tr><td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
		</table>
	</td>
</tr>

<tr>
	<td>
		<br />
		<div id="divNotice">
			<fieldset class="form_sub_option">
			<? if(sizeof($RecordID)>1) {?>
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0" class="tablerow2">
					<tr>
						<td colspan="2" valign="top" nowrap="nowrap">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td><img src="<?="$image_path/$LAYOUT_SKIN"?>/icon_section.gif" width="20" height="20" align="absmiddle"><span class="sectiontitle"><?=$eDiscipline["GlobalSettingsToAllStudent"]?></span></td>
									<td align="right">
										<span class="sectiontitle">
											<?=$linterface->GET_BTN($i_Discipline_Apply_To_All, "button", "javascript:applyNoticeToAll(".sizeof($RecordID).");")?>&nbsp;
											<?=$linterface->GET_BTN($button_view_template, "button", "javascript:previewNotice(0);")?>
										</span>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr valign="top">
						<td width="20%" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Discipline_Template?> <span class="tabletextrequire">*</span></span></td>
						<td>
							<?=$catSelection0?> <select name="SelectNotice0" id="SelectNotice0"><option value="0"><?="-- $button_select --"?></option></select>
						</td>
					</tr>
					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_Additional_Info?></td>
						<td valign="top"><textarea name="TextAdditionalInfo0" id="TextAdditionalInfo0" rows="2" wrap="virtual" class="textboxtext"></textarea></td>
					</tr>		
					<tr>
						<td valign="top" nowrap="nowrap">&nbsp;</td>
						<td valign="top"><input type='checkbox' name='emailNotify0' id='emailNotify0' value='1' checked><label for='emailNotify0'><?=$Lang['eDiscipline']['EmailNotifyParent']?></label></td>
					</tr>
				</table>
				<br />
			<? } ?>
				
				<?=$TableNotice?>
			</fieldset>
		</div>
		<table align="center" width="95%" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td valign="top" nowrap="nowrap">
					<span class="tabletextremark"><?=$i_general_required_field?></span>
				</td>
				<td width="80%">&nbsp;</td>
			</tr>
		</table>
	</td>
</tr>

<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center">
				<?php if($validRecordCount > 0){ ?>
					<?= $linterface->GET_ACTION_BTN($button_continue, "button", "this.disabled=true; this.className='formbutton_disable_v30 print_hide'; return checkForm(".sizeof($RecordID).");", "submit_btn")?>&nbsp;
				<?php } ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "jsCancel()")?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>

<br />

<input type="hidden" name="submit_flag" id="submit_flag" value="NO" />
<input type="hidden" id="id" name="id" />

<!-- for back action //-->
<input type="hidden" name="field" id="field" value="<?=$field?>" />
<input type="hidden" name="order" id="order" value="<?=$order?>" />
<input type="hidden" name="pageNo" id="pageNo" value="<?=$pageNo?>" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$numPerPage?>" />
<input type="hidden" name="num_per_page" id="num_per_page" value="<?=$num_per_page?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="<?=$page_size_change?>" />

<input type="hidden" name="s" id="s" value="<?=$s?>" />
<input type="hidden" name="clickID" id="clickID" value="<?=$clickID?>" />

<input type="hidden" name="record_number" id="record_number" value="<?=sizeof($RecordID)?>" />

<input type="hidden" name="SchoolYear" id="SchoolYear" value="<?=$SchoolYear?>" />
<input type="hidden" name="semester" id="semester" value="<?=$semester?>" />
<input type="hidden" name="targetClass" id="targetClass" value="<?=$targetClass?>" />
<input type="hidden" name="waived" id="waived" value="<?=$waived?>"/>
<input type="hidden" name="approved" id="approved" value="<?=$approved?>"/>
<input type="hidden" name="waitApproval" id="waitApproval" value="<?=$waitApproval?>"/>
<input type="hidden" name="released" id="released" value="<?=$released?>"/>
<input type="hidden" name="rejected" id="rejected" value="<?=$rejected?>"/>
<input type="hidden" name="MeritType2" id="MeritType2" value="<?=$MeritType?>"/>
<input type="hidden" name="pic" id="pic" value="<?=$pic?>"/>
<input type="hidden" name="back_page" id="back_page" value="<?=$back_page?>"/>

<input type="hidden" name="fromPPC" id="fromPPC" value="<?=$fromPPC?>"/>
<input type="hidden" name="passedActionDueDate" id="passedActionDueDate" value="<?=$passedActionDueDate?>"/>

</form>

<?
//print $linterface->FOCUS_ON_LOAD("form1.DetentionCount0");

echo "<script language='javascript'>$js</script>";

$linterface->LAYOUT_STOP();
intranet_closedb();
?>