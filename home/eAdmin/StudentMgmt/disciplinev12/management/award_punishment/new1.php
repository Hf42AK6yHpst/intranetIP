<?php
## using by : 

########## Change Log ###############
#
#   Date    :   2019-04-30  Bill
#               prevent Cross-site Scripting
#
#	Date	:	2017-06-28 (Bill)	[2015-0120-1200-33164]
#				Support display Merit Item with target Tag Type only
#
#	Date	:	2016-10-20	(Bill)	[DM#3090]
#				exclude alumni from student selection popup window
#
#	Date	:	2015-09-25	(Bill)	[2015-0416-1040-06164]
#				Hide Step 3 & 4 if $sys_custom['eDiscipline']['add_AP_records_skip_step3n4'] = true (HKUGA Cust)
#
#	Date	:	2015-07-23	(Bill)	[2015-0611-1642-26164]
#				Add button to select students using photos
#
#	Date	:	2013-08-26	(Carlos)
#				escape " and remove \n and \r in js statesArray
#
#	Date	:	2012-11-28	(YAtWoon)
#				use default method to select student [Case#2012-1113-1844-40071]
#
#	Date	:	2010-06-10 (Henry)
#	Detail 	:	add attachment to record
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

### Handle SQL Injection + XSS [START]
$student = $_POST['student'];
if(is_array($student) && sizeof($student) > 0) {
    foreach($student as $thisKey => $thisStudentID) {
        $student[$thisKey] = IntegerSafe($thisStudentID);
    }
}

if(!intranet_validateDate($RecordDate)) {
    $RecordDate = '';
}
if(!intranet_validateDate($ActionDueDate)) {
    $ActionDueDate = '';
}
if(is_array($PIC) && sizeof($PIC) > 0) {
    foreach($PIC as $k => $d) {
        $PIC[$k] = IntegerSafe($d);
    }
}
$sessionTime = cleanCrossSiteScriptingCode($sessionTime);

$record_type = IntegerSafe($record_type);
$CatID = IntegerSafe($CatID);
$ItemID = IntegerSafe($ItemID);
if($ldiscipline->AP_Interval_Value) {
    $MeritNum = (float)$MeritNum;
}
else {
    $MeritNum = IntegerSafe($MeritNum);
}
$MeritType = IntegerSafe($MeritType);
if($sys_custom['eDiscipline']['ConductMark1DecimalPlace']) {
    $ConductScore = (float)$ConductScore;
}
else {
    $ConductScore = IntegerSafe($ConductScore);
}
$semester = cleanCrossSiteScriptingCode($semester);
$Subject = IntegerSafe($Subject);
$TargetTagType = cleanCrossSiteScriptingCode($TargetTagType);
### Handle SQL Injection + XSS [END]

# Check Access Right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Award_Punishment-New");

$linterface = new interface_html();

# Menu Highlight
$CurrentPage = "Management_AwardPunishment";
$CurrentPageArr['eDisciplinev12'] = 1;

# Left Menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Tag Information
$TAGS_OBJ[] = array($eDiscipline['Award_and_Punishment']);

# Step Information
$STEPS_OBJ[] = array($eDiscipline["SelectStudents"], 1);
$STEPS_OBJ[] = array($eDiscipline["AddRecordToStudents"], 0);
// [2015-0416-1040-06164] hide from $STEPS_OBJ
if(!$sys_custom['eDiscipline']['add_AP_records_skip_step3n4']){
	$STEPS_OBJ[] = array($eDiscipline["SelectActions"], 0);
	$STEPS_OBJ[] = array($eDiscipline["FinishNotification"], 0);
}

# Merit Tag Type
if($sys_custom['Discipline_AP_Item_Tag_Seperate_Function'] && $TargetTagType=="" && $_GET['type'] > 0) {
	if($_GET['type']==1) {
		$TargetTagType = "Conduct";
	}
	else {
		$TargetTagType = "Attitude";
	}
}

# Last Selection
// $student = $_POST['student'];
if (is_array($student) && sizeof($student)>0)
{
    $list = implode("', '", $student);
    $namefield = getNameFieldWithClassNumberByLang();
    
    # sort by class and class number
    $sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE RecordType = 2 AND UserID IN ('$list') ORDER BY ClassName ASC, ClassNumber ASC";
    $array_students = $ldiscipline->returnArray($sql,2);
}

### Start for Searching ###
# Get Required Data
$data_ary = array();
$currentYear = getCurrentAcademicYear();
/*
$sql = "SELECT AcademicYearID FROM ACADEMIC_YEAR WHERE YearNameEN='$currentYear' OR YearNameB5='$currentYear'";
$year = $ldiscipline->returnVector($sql);
*/
$year = Get_Current_Academic_Year_ID();

//$sql = "SELECT ClassID,ClassName FROM INTRANET_CLASS WHERE RecordStatus = 1 ORDER BY ClassName";
# IP25 SQL
$orderBy = ($intranet_session_language=="en") ? " ORDER BY ClassTitleEN" : " ORDER BY ClassTitleB5";
$sql = "SELECT YearClassID, ClassTitleEN, ClassTitleB5 FROM YEAR_CLASS WHERE AcademicYearID=$year $orderBy";
$result = $ldiscipline->returnArray($sql,3);
for ($i=0; $i<sizeof($result); $i++)
{
	list($this_classid, $this_classnameEN, $this_classnameB5) = $result[$i];

	$className = Get_Lang_Selection($this_classnameB5, $this_classnameEN);
	$name_field = getNameFieldByLang();
	//$sql1 = "SELECT UserID, $name_field, ClassNumber,UserLogin FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1 AND ClassName = '".$className."' ORDER BY ClassNumber";
	$sql1 = "SELECT USR.UserID, $name_field, ycu.ClassNumber, USR.UserLogin FROM INTRANET_USER USR 
				LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID)
				WHERE USR.RecordType = 2 AND USR.RecordStatus = 1 AND ycu.YearClassID=$this_classid ORDER BY ycu.ClassNumber";
	$result1 = $ldiscipline->returnArray($sql1, 4);
	for ($j=0; $j<sizeof($result1); $j++)
	{
		list($this_userid, $this_stu_name, $this_class_number, $this_userlogin) = $result1[$j];
		$data_ary[] = array($this_userid, $className, $this_class_number, $this_stu_name, $this_userlogin);
	}
}

if(!empty($data_ary))
{
	# Define YUI array (Search by input format)
	for ($i=0; $i<sizeof($data_ary); $i++)
	{
		list($this_userid, $this_classname, $this_class_number, $this_stu_name, $this_userlogin) = $data_ary[$i];
		
		$temp_str = $this_classname . $this_class_number. " ". $this_stu_name;
		if($this_class_number) {
			$temp_str2 = $this_stu_name . " (". $this_classname ."-". $this_class_number .")";
		}
		else {
			$temp_str2 = $this_stu_name;
		}
        
		//($i == 0) ? $liList = "<li class=\"\" style=\"display: none;\">". $temp_str ."</li>\n" : $liList = "<li style=\"display: none;\">". $temp_str ."</li>\n";
		$liArr .= "[\"". str_replace(array('"',"\n","\r"),array('\"','',''),$temp_str) ."\", \"". str_replace(array('"',"\n","\r"),array('\"','',''),$temp_str2) ."\", \"". $this_userid ."\"]";
		//($i == (sizeof($result)-1)) ? $liArr .= "" : $liArr .= ",\n";
		//$liArr .= ",\n";
		($i == (sizeof($data_ary)-1)) ? $liArr .= "" : $liArr .= ",\n";
	}
    //echo $liArr;
    
	foreach ($data_ary as $key => $row)
	{
		$field1[$key] = $row[0];	//user id
		$field2[$key] = $row[1];	//class name
		$field3[$key] = $row[2];	//class number
		$field4[$key] = $row[3];	//stu name
		$field5[$key] = $row[4];	//login id
	}
	array_multisort($field5, SORT_ASC, $data_ary);
    
	# Define YUI array (Search by login id)
	for ($i=0; $i<sizeof($data_ary); $i++)
	{
		list($this_userid, $this_classname, $this_class_number, $this_stu_name, $this_userlogin) = $data_ary[$i];
		if($this_class_number) {
			$temp_str2 = $this_stu_name . " (". $this_classname ."-". $this_class_number .")";
		}
		else {
			$temp_str2 = $this_stu_name;
		}
        
		$liArr2 .= "[\"". $this_userlogin ."\", \"". str_replace(array('"',"\n","\r"),array('\"','',''),$temp_str2) ."\", \"". $this_userid ."\"]";
		($i == (sizeof($data_ary)-1)) ? $liArr2 .= "" : $liArr2 .= ",\n";
	}
}
### END for Searching ###

$student_selected = $linterface->GET_SELECTION_BOX($array_students, "name='student[]' ID='student[]' class='select_studentlist' size='15' multiple='multiple'", "");
$button_remove_html = $linterface->GET_BTN($button_remove_selected_student, "button", "javascript:checkOptionRemove(document.form1.elements['student[]'])");

# Remark
$remark = stripslashes(intranet_htmlspecialchars($remark));

# Start layout
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($eDiscipline['RecordList'], "index.php");
$PAGE_NAVIGATION[] = array($eDiscipline["NewRecord"], "");
?>

<link type="text/css" rel="stylesheet" href="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/dpSyntaxHighlighter.css">

<style type="text/css">
	#statesmod {position:relative;}
	#statesautocomplete {position:relative;width:22em;margin-bottom:1em;}/* set width of widget here*/
	#statesautocomplete {z-index:9000} /* for IE z-index of absolute divs inside relative divs issue */
	#statesinput {_position:absolute;width:100%;height:1.4em;z-index:0;} /* abs for ie quirks */
	#statescontainer, #statescontainerCC, #statescontainerBCC {position:absolute;top:0.3em;width:100%}
	#statescontainer .yui-ac-content, #statescontainerCC .yui-ac-content, #statescontainerBCC .yui-ac-content {position:absolute;width:100%;border:1px solid #404040;background:#eeeeee;overflow:hidden;z-index:9050;}
	#statescontainer .yui-ac-shadow, #statescontainerCC .yui-ac-shadow, #statescontainerBCC .yui-ac-shadow {position:absolute;margin:.3em;width:100%;background:#a0a0a0;z-index:9049;}
	#statescontainer ul, #statescontainerCC ul, #statescontainerBCC ul {padding:5px 0;width:100%;}
	#statescontainer li, #statescontainerCC li, #statescontainerBCC li {padding:0 5px;cursor:default;white-space:nowrap;}
	#statescontainer li.yui-ac-highlight, #statescontainerCC li.yui-ac-highlight, #statescontainerBCC li.yui-ac-highlight {background:#bbbbbb;}
	#statescontainer li.yui-ac-prehighlight, #statescontainerCC li.yui-ac-prehighlight, #statescontainerBCC li.yui-ac-prehighlight {background:#FFFFFF;}


	#statesmod div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,form,fieldset,input,textarea,p,blockquote{margin:0;padding:0;}
	#statesmod table{border-collapse:collapse;border-spacing:0;}
	#statesmod fieldset,img{border:0;}
	#statesmod address,caption,cite,code,dfn,em,strong,th,var{font-style:normal;font-weight:normal;}
	#statesmod ol,ul {list-style:none;}
	#statesmod caption,th {text-align:left;}
	#statesmod h1,h2,h3,h4,h5,h6{font-size:100%;font-weight:normal;}
	#statesmod q:before,q:after{content:'';}
	#statesmod abbr,acronym {border:0;}
	#statesmod {font:13px arial,helvetica,clean,sans-serif;*font-size:small;*font:x-small;}
</style>

<script language="javascript">
function addByLogin()
{
	obj = document.form1;
	obj.flag.value = 1;
	generalFormSubmitCheck(obj);
}

function finishSelection()
{
	obj = document.form1;
	obj.action = 'new2.php';
	checkOptionAll(obj.elements["student[]"]);
	obj.submit();
	return true;
}

function generalFormSubmitCheck(obj)
{
	checkOptionAll(obj.elements["student[]"]);
	obj.submit();
}

function formSubmit(obj)
{
	if (obj.flag.value == 0)
	{
		obj.flag.value = 1;
		generalFormSubmitCheck(obj);
		return true;
	}
	else
	{
		return finishSelection();
	}
}

function checkForm()
{
	obj = document.form1;
	if (obj.flag.value==1)
	{
		return addByLogin();
	}
	
	if(obj.elements["student[]"].length != 0)
	{
		return formSubmit(obj);
	}
	else
	{
		alert('<?=$i_Discipline_System_alert_PleaseSelectStudent?>');
		return false;
	}
}

function checkCR(evt) {
	var evt  = (evt) ? evt : ((event) ? event : null);
	var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
	if ((evt.keyCode == 13) && (node.type=="text")) {return false;}	
}

document.onkeypress = checkCR;
</script>

<form name="form1" method="POST" onsubmit="return checkForm();">

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td class="navigation">
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
		</table>
	</td>
</tr>
<tr>
	<td align="center"><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
</tr>
<tr>
	<td>
		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr>
			<td class="tabletext" width="40%"><?=$i_general_choose_student?></td>
			<td class="tabletext" ><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
			<td class="tabletext" width="60%"><?=$i_general_selected_students; ?></td>
		</tr>
		<tr>
			<td class="tablerow2" valign="top">
				<table width="100%" border="0" cellpadding="3" cellspacing="0">
				<tr>
					<td class="tabletext"><?=$i_general_from_class_group?></td>
				</tr>
				<tr>
					<td class="tabletext">
						<?= $linterface->GET_BTN($button_select, "button","newWindow('/home/common_choose/index.php?fieldname=student[]&page_title=SelectAudience&permitted_type=2&excluded_type=4&DisplayGroupCategory=1&UserRecordStatus=0,1',16)") ?>
						<?php if($sys_custom['eDiscipline']['CSCProbation']) { ?>
							&nbsp;
							<?= $linterface->GET_BTN($Lang['eDiscipline']['CWCProbation']['SelectStudentPhoto'], "button","newWindow('../cwc_photo_list.php?fieldname=student[]', 10)") ?>
						<?php } ?>
					</td>
				</tr>
				<tr>
					<td class="tabletext"><i><?=$i_general_or?></i></td>
				</tr>
				<tr>
					<td class="tabletext"><?=$i_general_search_by_inputformat?><br />
					<div id="statesautocomplete">
						<input type="text" class="tabletext" name="search1" ID="search1">
						<div id="statescontainer" style=" left:142px; top:0px;">
							<div style="display: none; width: 199px; height: 0px;" class="yui-ac-content">
								<div style="display: none;" class="yui-ac-hd"></div>
								<div class="yui-ac-bd"></div>
								<div style="display: none;" class="yui-ac-ft"></div>
							</div>
							<div style="width: 0pt; height: 0pt;" class="yui-ac-shadow"></div>
						</div>
					</div>
					</td>
				</tr>
				<tr>
					<td class="tabletext"><i><?=$i_general_or?></i></td>
				</tr>
				<tr>
					<td class="tabletext"><?=$i_general_search_by_loginid?><br />
					<div id="statesautocomplete">
						<input type="text" class="tabletext" name="search2" ID="search2">
						<div id="statescontainerCC" style=" left:142px; top:0px;">
							<div style="display: none; width: 199px; height: 0px;" class="yui-ac-content">
								<div style="display: none;" class="yui-ac-hd"></div>
								<div class="yui-ac-bd"></div>
								<div style="display: none;" class="yui-ac-ft"></div>
							</div>
							<div style="width: 0pt; height: 0pt;" class="yui-ac-shadow"></div>
						</div>
					</div>
					</td>
				</tr>
				</table>
			</td>
			<td class="tabletext" ><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
			<td align="left" valign="top">
				<table width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td align="left"><?= $student_selected ?></td>
				</tr>
				<tr>
					<td align="right"><?=$button_remove_html?></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_continue, "submit", "this.form.flag.value=3")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='./'")?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>

<br />
<input type="hidden" name="flag" value="0" />

<!-- Step 2 data //-->
<input type="hidden" name="RecordDate" value="<?=$RecordDate?>" />	
<input type="hidden" name="ActionDueDate" value="<?=$ActionDueDate?>" />	
<? 
if (is_array($PIC) && sizeof($PIC)>0)
{
	foreach($PIC as $k=>$d) { ?>
		<input type="hidden" name="PIC[]" value="<?=$d?>" />	
<? } 
}
?>
<input type="hidden" name="sessionTime" value="<?=$sessionTime?>" />	
<input type="hidden" name="remark" value="<?=$remark?>" />	
<input type="hidden" name="record_type" value="<?=$record_type?>" />	
<input type="hidden" name="CatID" value="<?=$CatID?>" />	
<input type="hidden" name="ItemID" value="<?=$ItemID?>" />	
<input type="hidden" name="MeritNum" value="<?=$MeritNum?>" />	
<input type="hidden" name="MeritType" value="<?=$MeritType?>" />	
<input type="hidden" name="ConductScore" value="<?=$ConductScore?>" />	
<input type="hidden" name="semester" value="<?=$semester?>" />	
<input type="hidden" name="Subject" value="<?=$Subject?>" />
<input type="hidden" name="TargetTagType" value="<?=$TargetTagType?>" />

</form>

<?
print $linterface->FOCUS_ON_LOAD("form1.search1");

$linterface->LAYOUT_STOP();
intranet_closedb();
?>

<!-- Libary begins -->
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/yahoo.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/dom.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/event-debug.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/animation.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/autocomplete-debug-ip20.js"></script>
<!-- Library ends -->

<!-- In-memory JS array begins-->
<script type="text/javascript">
var statesArray = [
	<?= $liArr?>
];

var loginidArray = [
	<?= $liArr2?>
];

var delimArray = [
	";"
];
</script>
<!-- In-memory JS array ends-->

<script type="text/javascript">
YAHOO.example.ACJSArray = function() {
	var oACDS, oAutoComp;
	return {
		init: function() {
			// Instantiate first JS Array DataSource
			oACDS = new YAHOO.widget.DS_JSArray(statesArray);

			// Instantiate first AutoComplete
			oAutoComp = new YAHOO.widget.AutoComplete('search1','student[]', 'statescontainer', oACDS);
			oAutoComp.queryDelay = 0;
			oAutoComp.prehighlightClassName = "yui-ac-prehighlight";
			oAutoComp.useShadow = true;
			oAutoComp.minQueryLength = 0;

			oACDS2 = new YAHOO.widget.DS_JSArray(loginidArray);
			oAutoComp2 = new YAHOO.widget.AutoComplete('search2','student[]', 'statescontainerCC', oACDS2);
			oAutoComp2.queryDelay = 0;
			oAutoComp2.prehighlightClassName = "yui-ac-prehighlight";
			oAutoComp2.useShadow = true;
			oAutoComp2.minQueryLength = 0;
		},

		validateForm: function() {
			// Validate form inputs here
			return false;
		}
	};
}();

YAHOO.util.Event.addListener(this,'load',YAHOO.example.ACJSArray.init);
</script>

<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/dpSyntaxHighlighter.js"></script>
<script type="text/javascript">
dp.SyntaxHighlighter.HighlightAll('code');
</script>