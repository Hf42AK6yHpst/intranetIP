<?php
// Using :

######## Change Log [Start] ################
#
#	Date:	2019-03-12 (Bill)   [2018-0710-1351-39240]
#			- Create file
#
######## Change Log [End] ################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_cust.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12_cust();
$result = $ldiscipline->generateStudentPunishmentNotice($_POST);

header("Location: generate.php?msg=generate");

?>