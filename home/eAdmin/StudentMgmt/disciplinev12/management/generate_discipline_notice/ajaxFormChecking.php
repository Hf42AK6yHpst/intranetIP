<?php
// Editing by

#################### Change Log [start] ###########
#
#	Date:	2019-03-12 (Bill)   [2018-0710-1351-39240]
#			- Create file
#
#################### Change Log [end] #############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_cust.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12_cust();

$AcademicYearID = IntegerSafe($AcademicYearID);
$SemesterID = IntegerSafe($SemesterID);
if($AcademicYearID && $SemesterID && !is_date_empty($StartDate) && !is_date_empty($EndDate)) {
    $overlapLog = $ldiscipline->getGeneratedNoticeLog($AcademicYearID, $SemesterID, $StartDate, $EndDate, $LogID='', $forSelection=false, $checkOverlap=true);
    echo count($overlapLog);
}

intranet_closedb();
?>