<?php
// Editing by

#################### Change Log [start] ###########
#
#	Date:	2019-03-12 (Bill)   [2018-0710-1351-39240]
#			- Create file
#
#################### Change Log [end] #############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_cust.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12_cust();
$ldiscipline_ui = new libdisciplinev12_ui_cust();

$AcademicYearID = IntegerSafe($AcademicYearID);
$SemesterID = IntegerSafe($SemesterID);
if($AcademicYearID && $SemesterID && !is_date_empty($StartDate) && !is_date_empty($EndDate)) {
    echo $ldiscipline_ui->getGeneratedNoticeLogSelection($AcademicYearID, $SemesterID, $StartDate, $EndDate);
}

intranet_closedb();
?>