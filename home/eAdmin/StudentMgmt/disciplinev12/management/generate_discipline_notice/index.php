<?php
// Editing by 

#################### Change Log [start] ###########
#
#	Date:	2019-03-12 (Bill)   [2018-0710-1351-39240]
#			- Create file
#
#################### Change Log [end] #############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_cust.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# Access right checking
$ldiscipline = new libdisciplinev12_cust();
if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || !$sys_custom['eDiscipline']['HYKPersonalReport'] || !$sys_custom['eDiscipline']['HYKPrintDisciplineNotice']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$linterface = new interface_html();
$ldiscipline_ui = new libdisciplinev12_ui_cust();

# Report Mode
$curSubType = $curSubType==''? 'single' : $curSubType;
$is_monthly_mode = $curSubType=='monthly';

# Page
$CurrentPage = "Management_HYKPrintDisciplineNotice";
$CurrentPageArr['eDisciplinev12'] = 1;

# Tags
if($is_monthly_mode) {
    $TAGS_OBJ[] = array($Lang['eDiscipline']['HYKPrintDisciplineNotice']['Single'], "index.php", 0);
    $TAGS_OBJ[] = array($Lang['eDiscipline']['HYKPrintDisciplineNotice']['Monthly'], "", 1);
}
else {
    $TAGS_OBJ[] = array($Lang['eDiscipline']['HYKPrintDisciplineNotice']['Single'], "", 1);
    $TAGS_OBJ[] = array($Lang['eDiscipline']['HYKPrintDisciplineNotice']['Monthly'], "index.php?curSubType=monthly", 0);
}
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Sub-Tags
$SUB_TAGS_OBJ = array();
if($is_monthly_mode) {
    $SUB_TAGS_OBJ[] = array($Lang['Btn']['Print'], 'javascript: void(0);', 1);
    $SUB_TAGS_OBJ[] = array($Lang['eDiscipline']['HYKPrintDisciplineNotice']['GenerateMonthlyLetter'], 'generate.php', 0);
    $htmlAry['SUB_TAGS'] = $linterface->GET_SUBTAGS($SUB_TAGS_OBJ);
}

// Get School Year
$currentYearID = Get_Current_Academic_Year_ID();
$selectYear = $selectYear == ''? $currentYearID : $selectYear;

# School Year Selection
$yearSelection = $ldiscipline->returnAllYearsSelectionArray();
$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($yearSelection, "name='selectYear' id='selectYear' onclick='changeRadioSelection()' onChange='changeTerm(this.value, 0);' ", "", $selectYear);

// Get Semester
$currentSemesterID = getCurrentSemesterID();
$selectSemester = $selectSemester == ''? $currentSemesterID : $selectSemester;
$Semesters = getSemesters($selectYear, 0);

# Semester Selection
$selectSemesterHTML = "<select name=\"selectSemester\" id=\"selectSemester\" onclick=\"changeRadioSelection()\" onchange=\"changeYearTermDateRange();\">";
$selectSemesterHTML .= "</select>";

$linterface->LAYOUT_START();
?>

<script type="text/javascript" language="JavaScript">
// Semester Data
var SemesterStartDate = new Array();
var SemesterEndDate = new Array();
<? for($i=0; $i<count((array)$Semesters); $i++){
	$thisSemester = $Semesters[$i];
	echo "SemesterStartDate[".$thisSemester["YearTermID"]."] = '".substr($thisSemester["TermStart"], 0, 10)."'; \r\n";
	echo "SemesterEndDate[".$thisSemester["YearTermID"]."] = '".substr($thisSemester["TermEnd"], 0, 10)."'; \r\n";
} ?>

function changeTerm(val, skipDateHandle)
{
	if (val.length==0)
	{
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
	
	// Set default Semester
	var selectTermId = $('#selectSemester').val();
	if(selectTermId == undefined || selectTermId == null)
	{
		selectTermId = '<?=$selectSemester?>';
	}
	
	$.get(
		'../../reports/ajaxGetSemester.php?year='+val+'&term='+selectTermId+'&hideWholeYear=1&field=selectSemester&onchange=changeYearTermDateRange()',
		{},
		function(responseText) {
			document.getElementById("spanSemester").innerHTML = responseText;
			document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
			
			if(skipDateHandle) {
				// do nothing
			} else {
				changeYearTermDateRange();
			}
		}
	);
}

function changeRadioSelection()
{
	showRankTargetResult($('#rankTarget').val());
}

function showSpan(span)
{
	document.getElementById(span).style.display="inline";
	if(span == 'spanStudent')
	{
		$('#selectAllBtn01').hide();
		
		var temp = document.getElementById('studentID[]');
		while(temp.options.length>0) {
			temp.options[0] = null;	
		}
	}
}

function hideSpan(span)
{
	document.getElementById(span).style.display="none";
	if(span=='spanStudent') {
		$('#selectAllBtn01').show();
	}
}

function showResult(str)
{
	if (str.length==0)
	{
		document.getElementById("rankTargetDetail").innerHTML = "";
		document.getElementById("rankTargetDetail").style.border = "0px";
		return
	}
	
	var yearClassId = '';
	if(str == 'student2ndLayer')
	{
		var delim = '';
		var options = $('select#rankTargetDetail\\[\\] option:selected');
		for(var i=0; i<options.length; i++) {
			yearClassId += delim + options.get(i).value;
			delim = ',';
		}
	}
	
	var url = "../../reports/ajaxGetLive.php";
	var data = { "target" : str, "selectSize" : 8 };
	if(str == 'form') {
		data["fieldId"] = "rankTargetDetail[]";
		data["fieldName"] = "rankTargetDetail[]";
	}
	else if(str == 'class') {
		data["fieldId"] = "rankTargetDetail[]";
		data["fieldName"] = "rankTargetDetail[]";
	}
	else if(str == 'student') {
		data["fieldId"] = "rankTargetDetail[]";
		data["fieldName"] = "rankTargetDetail[]";
		data["onchange"] = encodeURIComponent("showRankTargetResult('student2ndLayer');");
	}
	else if(str == 'student2ndLayer') {
		data["fieldId"] = "rankTargetDetail[]";
		data["fieldName"] = "rankTargetDetail[]";
		data["studentFieldId"] = "studentID[]";
		data["studentFieldName"] = "studentID[]";
		data["YearClassID"] = yearClassId;
	}
	data["academicYearId"] = $('#selectYear').val();
	
	$.post(
		url,
		data,
		function(responseText)
		{
			var showIn = "rankTargetDetail";
			if(str == 'student2ndLayer'){
				showIn = "spanStudent";	
			}
			$('#'+showIn).html(responseText);
			document.getElementById(showIn).style.border = "0px solid #A5ACB2";
			
			if(str == 'student') {
				showRankTargetResult('student2ndLayer');
			}
		}
	);
}

function showRankTargetResult(val)
{
	showResult(val);
	if (val=='student2ndLayer') {
		showSpan('spanStudent');
	} else {
		hideSpan('spanStudent');
	}
}

function hideOptionLayer()
{
	$('.Form_Span').hide();
	$('.spanHideOption').hide();
	$('.spanShowOption').show();
	
	document.getElementById('div_form').className = 'report_option report_hide_option';
}

function showOptionLayer()
{
	$('.Form_Span').show();
	$('.spanHideOption').show();
	$('.spanShowOption').hide();
	
	document.getElementById('div_form').className = 'report_option report_show_option';
}

function SelectAll(obj)
{
	for (i=0; i<obj.length; i++) {
		obj.options[i].selected = true;
	}
}

function SelectAllItem(obj, flag)
{
	for (i=0; i<obj.length; i++) {
		obj.options[i].selected = flag;
	}
}

function submitForm(format)
{
	var valid = true;
	var form_obj = $('#form1');
	var rank_target = $('#rankTarget').val();

	<?php if($is_monthly_mode) { ?>
    	if(rank_target == 'form' || rank_target == 'class')
    	{
    		if($('select#rankTargetDetail\\[\\] option:selected').length == 0) {
    			valid = false;
    			$('#TargetWarnDiv span').html('<?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?>');
    			$('#TargetWarnDiv').show();
    		}
    		else {
    			$('#TargetWarnDiv').hide();
    		}
    	}
    	else if(rank_target == 'student')
    	{
    		if($('select#studentID\\[\\] option:selected').length == 0) {
    			valid = false;
    			$('#TargetWarnDiv span').html('<?=$i_alert_pleaseselect.$Lang['Btn']['Student']?>');
    			$('#TargetWarnDiv').show();
    		}
    		else {
    			$('#TargetWarnDiv').hide();
    		}
    	}

    	if($('select#GenerateLogID') && $('select#GenerateLogID option').length > 0)
    	{
    		var selectedLogID = $('select#GenerateLogID option:selected').val();
    		if(selectedLogID == "") {
    			valid = false;
    			$('#LogWarnDiv span').html('<?=$i_alert_pleaseselect.$Lang['eDiscipline']['HYKPrintDisciplineNotice']['Records']?>');
    			$('#LogWarnDiv').show();
    		}
    		else {
    			$('#LogWarnDiv').hide();
    		}
    	}
	<?php } else { ?>
		if(countChecked(document.form1, "NoticeIDArr[]") > 0) {
			$('#NoticeWarnDiv').hide();
		}
		else {
			valid = false;
			$('#NoticeWarnDiv span').html('<?=$i_alert_pleaseselect.$ip20_enotice?>');
			$('#NoticeWarnDiv').show();
		}
	<?php } ?>
	if(!valid) return;
	$('input#format').val(format);
	
	form_obj.attr('action', '../../reports/hyk_personal_report/report.php');
	if(format == 'csv' || format == 'word')
	{
		form_obj.attr('target','');
		form_obj.submit();
	}
	else if(format == 'print')
	{
		form_obj.attr('target','_blank');
		form_obj.submit();
	}
}

function changeYearTermDateRange()
{
	var yearTermId = $("#selectSemester").val() == '0'? '' : $("#selectSemester").val();
	$.post(
		"../../reports/ajaxGetYearTermDateRange.php",
		{
			"AcademicYearID": $('#selectYear').val(),
			"YearTermID":yearTermId
		},
		function(returnData)
		{
			var obj = JSON.parse(returnData);
			$('#textFromDate').val(obj['StartDate']);
			$('#textToDate').val(obj['EndDate']);
			
			getGeneratedNoticeSelection();
		}
	);
}

function getGeneratedNoticeSelection()
{
	$.post(
		"ajaxGetGenerateDate.php",
		{
			"AcademicYearID": $('#selectYear').val(),
			"SemesterID": $('#selectSemester').val(),
			"StartDate": $('#textFromDate').val(),
			"EndDate": $('#textToDate').val()
		},
		function(returnData)
		{
			$('#spanGenerateLog').html(returnData);

			if(returnData == '<?=$Lang['General']['NoRecordFound']?>') {
				$('#generateBtnDiv').hide();
			} else {
				$('#generateBtnDiv').show();
			}
		}
	);
}

function getNoticeTableList()
{
	var valid = true;
	var form_obj = $('#form1');
	var rank_target = $('#rankTarget').val();
	
	var from_date = $.trim($('#textFromDate').val());
	var to_date = $.trim($('#textToDate').val());
	if(from_date == '' || to_date == '') {
		valid = false;
		$('#textFromDate').focus();
	}
	
	if(valid)
	{
		if(!check_date_without_return_msg(document.getElementById('textFromDate'))) {
			valid = false;
			$('#textFromDate').focus();
		}
		if(!check_date_without_return_msg(document.getElementById('textToDate'))) {
			valid = false;
			$('#textToDate').focus();
		}
	}
	
	if(valid)
	{
		if(from_date > to_date) {
			valid = false;
			$('#DateWarnDiv span').html('<?=$Lang['General']['JS_warning']['InvalidDateRange']?>');
			$('#DateWarnDiv').show();
			$('#textFromDate').focus();
		}
	}
	
	if(valid)
	{
		var semester_id = $('#selectSemester').val();
		var semester_start = SemesterStartDate[semester_id];
		var semester_end = SemesterEndDate[semester_id];
		if(compareDate(from_date, semester_start) < 0 || compareDate(semester_end, to_date) < 0) {
			valid = false;
			$('#DateWarnDiv span').html('<?=$Lang['eDiscipline']['HYKPrintDisciplineNotice']['Message']['OutOfTermPeriod']?>');
			$('#DateWarnDiv').show();
			$('#textFromDate').focus();
		}
	}
	
	if(valid)
	{
		$('#DateWarnDiv span').html('');
		$('#DateWarnDiv').hide();
	}
    
    if(rank_target == 'form' || rank_target == 'class')
    {
    	if($('select#rankTargetDetail\\[\\] option:selected').length == 0) {
    		valid = false;
    		$('#TargetWarnDiv span').html('<?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?>');
    		$('#TargetWarnDiv').show();
    	}
    	else {
    		$('#TargetWarnDiv').hide();
    	}
    }
    else if(rank_target == 'student')
    {
    	if($('select#studentID\\[\\] option:selected').length == 0) {
    		valid = false;
    		$('#TargetWarnDiv span').html('<?=$i_alert_pleaseselect.$Lang['Btn']['Student']?>');
    		$('#TargetWarnDiv').show();
    	}
    	else {
    		$('#TargetWarnDiv').hide();
    	}
    }
	
	if(!valid) {
		return;
	}
	else {
    	$.post(
    		"ajaxGetNoticeList.php",
    		$('#form1').serialize(),
    		function(returnData)
    		{
    			$('#NoticeListDiv').html(returnData);
    		}
    	);
	}
}

$(document).ready(function(){
	<?php if($is_monthly_mode) { ?>
		changeTerm($("#selectYear").val(), 0);
	<?php } else { ?>
		changeTerm($("#selectYear").val(), 1);
	<?php } ?>
	
	showRankTargetResult($("#rankTarget").val());
});
</script>

<div id="PageDiv">
<div id="div_form">
	<!--
	<span id="spanShowOption" class="spanShowOption" style="display:none">
		<a href="javascript:showOptionLayer();"><?=$Lang['Btn']['ShowOption'] ?></a>
	</span>
	<span id="spanHideOption" class="spanHideOption" style="display:none">
		<a href="javascript:hideOptionLayer();"><?=$Lang['Btn']['HideOption']?></a>
	</span>
	-->
	<?= $htmlAry['SUB_TAGS'] ?>
	<br/>
	
	<p class="spacer"></p> 
	<span class="Form_Span">
		<form id="form1" name="form1" method="post" action="index.php" onsubmit="return false;">
		<table class="form_table_v30">
			<!-- Periods -->
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eDiscipline']['HYKMeritStatisticsReport']['Period']?></td>
				<td>
					<table class="inside_form_table">
						<tr>
							<td>
								<?=$Lang['eDiscipline']['HYKMeritStatisticsReport']['SchoolYear']?>
								<?=$selectSchoolYearHTML?>&nbsp;
								<?=$Lang['eDiscipline']['HYKMeritStatisticsReport']['Semester']?>
								<span id="spanSemester"><?=$selectSemesterHTML?></span>
							</td>
						</tr>
						<tr <?=($is_monthly_mode? ' style="display: none" ' : '')?>>
							<td>
								<?=$Lang['General']['From']?>
								<?=$linterface->GET_DATE_PICKER("textFromDate", $textFromDate, "")?>
								<?=$Lang['General']['To']?>
								<?=$linterface->GET_DATE_PICKER("textToDate", $textToDate, "")?>
								<?=$linterface->Get_Form_Warning_Msg("DateWarnDiv","")?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			
			<!-- Targets -->
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eDiscipline']['HYKMeritStatisticsReport']['Target']?></td>
				<td>
					<table class="inside_form_table">
						<tr>
							<td valign="top">
								<select name="rankTarget" id="rankTarget" onChange="showRankTargetResult(this.value)">
									<option value="form" <? if($rankTarget=="form" || !isset($rankTarget) || $rankTarget=="") { echo "selected"; } ?>><?=$i_Discipline_Form?></option>
									<option value="class" <? if($rankTarget=="class") { echo "selected"; } ?>><?=$i_Discipline_Class?></option>
									<option value="student" <? if($rankTarget=="student") { echo "selected"; } ?>><?=$i_UserStudentName?></option>
								</select>
							</td>
							<td valign="top" nowrap>
								<!-- Form / Class //-->
								<span id='rankTargetDetail'>
									<select name="rankTargetDetail[]" id="rankTargetDetail[]" size="5"></select>
								</span>
								<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('rankTargetDetail[]'));return false;", "selectAllBtn01")?>
								
								<!-- Student //-->
								<span id='spanStudent' style='<? if($rankTarget=="student") { echo "display:inline;";} else { echo "display:none;"; } ?>'>
									<select name="studentID[]" multiple size="5" id="studentID[]"></select> <?= $linterface->GET_BTN($button_select_all, "button", "SelectAllItem(document.getElementById('studentID[]'), true);return false;")?>
								</span>
								<?=$linterface->Get_Form_Warning_Msg("TargetWarnDiv","")?>
							</td>
						</tr>
						<tr><td colspan="3" class="tabletextremark">(<?=$Lang['eDiscipline']['HYKMeritStatisticsReport']['PressCtrlKey']?>)</td></tr>
					</table>
				</td>
			</tr>
			
			<?php if($is_monthly_mode) { ?>
    			<!-- Records -->
    			<tr valign="top">
    				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eDiscipline']['HYKPrintDisciplineNotice']['Records']?></td>
    				<td id="spanGenerateLog"></td>	
    			</tr>
			<?php } ?>
		</table>
		<?=$linterface->MandatoryField();?>
		
		<?php if($is_monthly_mode) { ?>
    		<div class="edit_bottom_v30" id="generateBtnDiv">
        		<p class="spacer"></p>
        		<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Print'], "button", "submitForm('print')")?>
        		&nbsp;
        		<?= $linterface->GET_ACTION_BTN($Lang['eDiscipline']['HYKClassSummaryReport']['ExportToWord'], "button", "submitForm('word')")?>
        		<p class="spacer"></p>
    		</div>
		<?php } else { ?>
    		<div class="edit_bottom_v30">
        		<p class="spacer"></p>
    			<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "getNoticeTableList()")?>
    		</div>
    		
			<div id="NoticeListDiv"></div>
		<?php } ?>
		
		<input type="hidden" name="format" id="format" value="print">
		<input type="hidden" name="curSubType" id="curSubType" value="<?=$curSubType?>">
		<input type="hidden" name="HideNoRecordStudent" id="HideNoRecordStudent" value="1">
		</form>
	</span>
</div>

<div id="ReportDiv"></div>
</div>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>