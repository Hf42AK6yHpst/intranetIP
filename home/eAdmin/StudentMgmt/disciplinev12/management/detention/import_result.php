<?
// Modifying by: 

$PATH_WRT_ROOT="../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();

$limport = new libimporttext();
$lo = new libfilesystem();
$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));

if($ext != ".CSV" && $ext != ".TXT")
{
	header("location: import.php?xmsg=import_failed");
	exit();
}
$data = $limport->GET_IMPORT_TXT($csvfile);

$col_name = array_shift($data);

$ldiscipline = new libdisciplinev12();
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Detention-New");
$lu = new libuser();

$linterface = new interface_html();

# menu highlight setting
$CurrentPage = "Management_Detention";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eDiscipline['Detention_Arrangement'], "", 1);
$TAGS_OBJ[] = array($eDiscipline['Session_Management'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/management/detention/settings/calendar.php", 0);

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);


//$file_format = array($i_UserClassName,$i_ClassNumber,$i_Attendance_Reason,$i_UserRemark);
$file_format = array("Class Name","Class Number","Reason","PIC","Remark");

$format_wrong = false;
for($i=0; $i<sizeof($file_format); $i++)
{
	if ($col_name[$i]!=$file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}
if($format_wrong)
{
	header("location: import.php?xmsg=import_header_failed");
	exit();
}
$linterface->LAYOUT_START();

### List out the import result
$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
$x .= "<tr>";
$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_general_class</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_identity_student</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Attendance_Reason</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_PIC</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_UserRemark</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$iDiscipline['AccumulativePunishment_Import_Failed_Reason']."</td>";
$x .= "</tr>";

$sql = "drop table temp_detention_import";
$ldiscipline->db_db_query($sql);

$sql = "create table temp_detention_import
	(
		 StudentID    INT(11),
		 Reason       VARCHAR(128),
		 Remark       TEXT,
		 StudentName varchar(255),
		 PICID		varchar(255)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8";
$ldiscipline->db_db_query($sql);

$errorCount = 0;

for($i=0;$i<sizeof($data);$i++)
{
	$error = array();

	### get data
	list($ClassName, $ClassNumber, $Reason, $PIC, $Remark) = $data[$i];

	### checking - valid class & class number (student found)
	$StudentID = $lu->returnUserID($ClassName, $ClassNumber, 2, '0,1,2');

	if(trim($StudentID)=="")
	{
		$error['No_Student'] = $iDiscipline['Good_Conduct_No_Student'];
		$errorCount++;
	}
	$lu = new libuser($StudentID);
	
	$ClassInfo = $ClassName."-".$ClassNumber;
	$StudentName = ($intranet_session_language=='en')?$lu->EnglishName:$lu->ChineseName;
	
	if(trim($Reason)=="")
	{
		$error['Missing_Reason'] = $iDiscipline['Detention_Missing_Reason'];
		$errorCount++;
	}

	if($PIC=="") {
		$error['Missing_PICID']	 = $ec_html_editor['missing']." ".$i_Discipline_PIC;
		$errorCount++;
	} else {
		$PICAry = explode(',', $PIC);
		$picList = "";
		for($k=0; $k<sizeof($PICAry); $k++) {
			$picData = $ldiscipline->getUserInfoByUserLogin(trim($PICAry[$k]),1);
			
			if(sizeof($picData)==0) {		# no such PIC
				$error['PIC_incorrect'] .= ($error['PIC_incorrect']!="") ? ", \"".trim($PICAry[$k])."\"" : "\"".trim($PICAry[$k])."\"";
				$picList .= ($picList!="") ? ", \"".trim($PICAry[$k])."\"" : "\"".trim($PICAry[$k])."\"";
				$errorCount++;
			} else {
				$picName = Get_Lang_Selection($picData['ChineseName'], $picData['EnglishName']);
				$picList .= ($picList!="") ? ", ".$picName : $picName;
			}
		}
		if($error['PIC_incorrect']!="") {
			$error['PIC_incorrect'] = $ec_html_editor['incorrect']." ".$i_Discipline_PIC." ".$error['PIC_incorrect'];
		} 
	}
	$displayPIC = $picList;
	$css = (sizeof($error)==0) ? "tabletext":"red";

	$x .= "<tr class=\"tablebluerow".($i%2+1)."\">";
	$x .= "<td class=\"$css\">".($i+1)."</td>";
	$x .= "<td class=\"$css\">". $ClassInfo ."</td>";
	$x .= "<td class=\"$css\">". $StudentName ."</td>";
	$x .= "<td class=\"$css\">". intranet_htmlspecialchars($Reason) ."</td>";
	$x .= "<td class=\"$css\">". $displayPIC ."</td>";
	$x .= "<td class=\"$css\">". intranet_htmlspecialchars($Remark) ."</td>";
	$x .= "<td class=\"$css\">";
	if(sizeof($error)>0)
	{
		foreach($error as $Key=>$Value)
		{
			$x .= $Value.'<br />';
		}
	}
	else
	{
		$x .= "--";
	}
	$x .= "</td>";
	$x .= "</tr>";

	$sql = "insert into temp_detention_import values ('$StudentID','".addslashes($Reason)."','".addslashes($Remark)."', '".addslashes($StudentName)."' ,'".addslashes($PIC)."')";
	$ldiscipline->db_db_query($sql);

}
$x .= "</table>";

if($errorCount>0)
{
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import.php'");
}
else
{
	$import_button = $linterface->GET_ACTION_BTN($button_import, "submit")." &nbsp;".$linterface->GET_ACTION_BTN($button_back, "button", "window.location='import.php'");
}
?>

<br />
<form name="form1" method="post" action="import_update.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center">
		<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2"><?=$x?></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
