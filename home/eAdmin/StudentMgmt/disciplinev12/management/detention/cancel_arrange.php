<?php
// Modifying by:

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-RearrangeStudent")))
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}
else
{
	$SuccessFlag = true;

	if ($did == "") {		// from student_record.php
		if (is_array($RecordID) && $RecordID[0] <> "") {
			for ($i=0; $i<sizeof($RecordID); $i++) {
				$rid = $RecordID[$i];
				$PastFlag = $PastFlag || $ldiscipline->checkPastByRecordID($rid);
			}
			
			if (!$PastFlag) {
				if (is_array($RecordID) && $RecordID[0] <> "") {
					for ($i=0; $i<sizeof($RecordID); $i++) {
						$rid = $RecordID[$i];
						$NoticeID = $ldiscipline->getNoticeIDByRecordID($rid);
						$DetentionArr['DetentionID'] = "NULL";
						$DetentionArr['ArrangedBy'] = "NULL";
						$DetentionArr['ArrangedDate'] = "NULL";
						$result = $ldiscipline->updateDetentionByRecordID($DetentionArr, $rid);
						if (!$result) $SuccessFlag = false;
						if($NoticeID[0] && $result) {
							include_once($intranet_root."/includes/libnotice.php");
							$lnotice = new libnotice($NoticeID[0]);
							$lnotice->deleteNotice($NoticeID[0]);
						}
					}
				}
			}
		}
	} else {	// from detail.php
		//$PastFlag = $ldiscipline->checkPastByDetentionID($did);	### this checking not valid again since now user can assign student to previous detention session of current day
		
		### new checking
		$PastFlag = false;
		$detentionInfo = $ldiscipline->getDetentionInfoByDetentionID($did);
		$PastFlag = $ldiscipline->checkPastByRecordID($detentionInfo[0]['RecordID']);
		
		if (!$PastFlag) {
			if (is_array($RecordID) && $RecordID[0] <> "") {
				for ($i=0; $i<sizeof($RecordID); $i++) {
					$rid = $RecordID[$i];
					$NoticeID = $ldiscipline->getNoticeIDByRecordID($rid);
					$DetentionArr['DetentionID'] = "NULL";
					$DetentionArr['ArrangedBy'] = "NULL";
					$DetentionArr['ArrangedDate'] = "NULL";
					$result = $ldiscipline->updateDetentionByRecordID($DetentionArr, $rid);
					if (!$result) $SuccessFlag = false;
					if($NoticeID[0] && $result) {
						include_once($intranet_root."/includes/libnotice.php");
						$lnotice = new libnotice($NoticeID[0]);
						$lnotice->deleteNotice($NoticeID[0]);
					}
				}
			}
		}
	}

	if ($PastFlag) {
		$SysMsg = "cancel_past";
	} else if ($SuccessFlag) {
		$SysMsg = "cancel";
	} else {
		$SysMsg = "cancel_failed";
	}

	intranet_closedb();
	if ($cpage == "srec") {
		$ReturnPage = "student_record.php";
		$Par = "?msg=$SysMsg";
	} else {
		$ReturnPage = "detail.php";
		$Par = "?did=$did&msg=$SysMsg";
	}
	header("Location: $ReturnPage$Par");
}
?>
