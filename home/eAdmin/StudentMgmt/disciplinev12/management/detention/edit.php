<?php
// Modifying by: yat

###### Change Log [Start] #############
#
#	Date	:	2010-05-10	YatWoon
#				display more detention sesstion information
#
###### Change Log [End] #############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
	
$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if (is_array($RecordID) && $RecordID[0] <> "") $rid = $RecordID[0];

if(!($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-EditAll") || ($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-EditOwn") && $ldiscipline->isDetentionSessionOwn($rid))))
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$CurrentPage = "Management_Detention";

$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($eDiscipline['Detention_Arrangement'], "", 1);
$TAGS_OBJ[] = array($eDiscipline['Session_Management'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/management/detention/settings/calendar.php", 0);

$PAGE_NAVIGATION[] = array($i_Discipline_Detention_List, "list.php");
$PAGE_NAVIGATION[] = array($button_edit." ".$i_Discipline_Student_Record, "");

$student = $ldiscipline->getDetentionStudentIDByRecordID($rid);

$resultReasonRemark = $ldiscipline->getReasonRemarkByRecordID($rid);
list($TmpReason, $TmpRemark) = $resultReasonRemark[0];

$linterface->LAYOUT_START();

$detentionSesstionInfoAry = $ldiscipline->getDetentionSessionInfoByRecordID($rid);
$detentionSesstionInfo = $detentionSesstionInfoAry[0];

// Loop for individual student
$TableIndStudent = "";
for ($i=0; $i<sizeof($student); $i++) {
	$resultStudentName = $ldiscipline->getStudentNameByID($student[$i]);
	list($TmpUserID, $TmpName, $TmpClassName, $TmpClassNumber) = $resultStudentName[0];
	$TableIndStudent .= "<br />";
	$TableIndStudent .= "<table align=\"center\" width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
	$TableIndStudent .= "<tr>";
	$TableIndStudent .= "<td colspan=\"2\" valign=\"top\" nowrap=\"nowrap\">";
	$TableIndStudent .= "<img src=\"".$image_path."/".$LAYOUT_SKIN."/ediscipline/icon_stu_ind.gif\" width=\"20\" height=\"20\" align=\"absmiddle\">";
	$TableIndStudent .= "<span class=\"sectiontitle\">".$TmpClassName."-".$TmpClassNumber." ".$TmpName."</span>";
	$TableIndStudent .= "</td>";
	$TableIndStudent .= "</tr>";
	
	# Reason
	$TableIndStudent .= "<tr valign=\"top\" class=\"tablerow1\">";
	$TableIndStudent .= "<td nowrap=\"nowrap\" class=\"formfieldtitle\">";
	$TableIndStudent .= "<span class=\"tabletext\">".$i_Discipline_Reason." <span class=\"tabletextrequire\">*</span></span>";
	$TableIndStudent .= "</td>";
	$TableIndStudent .= "<td width=\"80%\">";
	$TableIndStudent .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
	$TableIndStudent .= "<tr>";
	$TableIndStudent .= "<td><input name=\"TextReason".($i+1)."\" type=\"text\" class=\"textboxtext\" value=\"".intranet_htmlspecialchars($TmpReason)."\"/></td>";
	$TableIndStudent .= "<td width=\"25\" align=\"center\">";
	$TableIndStudent .= $linterface->GET_PRESET_LIST("jArrayDetentionReason", ($i+1), "TextReason".($i+1));
	$TableIndStudent .= "</td>";
	$TableIndStudent .= "<td width=\"2\" align=\"center\"></td>";
	$TableIndStudent .= "</tr>";
	$TableIndStudent .= "</table>";
	$TableIndStudent .= "</td>";
	$TableIndStudent .= "</tr>";
	
	# PIC
	$pic_str = $ldiscipline->getPICNameList($detentionSesstionInfo['PICID']);
	$TableIndStudent .= "<tr class=\"tablerow1\">";
	$TableIndStudent .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\">".$i_Discipline_System_Contact_PIC."</td>";
	$TableIndStudent .= "<td valign=\"top\">". ($pic_str ? $pic_str : "---") ."</td>";
	$TableIndStudent .= "</tr>";
	
	# Session
	$session_str = $ldiscipline->displayDetentionSessionString($detentionSesstionInfo['DetentionID']);
	$TableIndStudent .= "<tr class=\"tablerow1\">";
	$TableIndStudent .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\">".$i_Discipline_Session."</td>";
	$TableIndStudent .= "<td valign=\"top\">". ($session_str ? $session_str : "---") ."</td>";
	$TableIndStudent .= "</tr>";
	
	# Remark
	$TableIndStudent .= "<tr class=\"tablerow1\">";
	$TableIndStudent .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\">".$i_Discipline_Remark."</td>";
	$TableIndStudent .= "<td valign=\"top\">".$linterface->GET_TEXTAREA("TextRemark".($i+1), intranet_htmlspecialchars($TmpRemark))."</td>";
	$TableIndStudent .= "</tr>";
	
	# Created By
	$pic_str = $ldiscipline->getPICNameList($detentionSesstionInfo['CreatedBy']);
	$TableIndStudent .= "<tr class=\"tablerow1\">";
	$TableIndStudent .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\">".$Lang['eDiscipline']['RecordCreatedBy']."</td>";
	$TableIndStudent .= "<td valign=\"top\">". ($pic_str ? $pic_str : "---") ." (". $detentionSesstionInfo['DateInput'] .") </td>";
	$TableIndStudent .= "</tr>";
	
	if ($i != sizeof($student)-1) {
		$TableIndStudent .= "<tr>";
		$TableIndStudent .= "<td height=\"1\" colspan=\"2\" class=\"dotline\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"1\"></td>";
		$TableIndStudent .= "</tr>";
	}
	$TableIndStudent .= "</table>";
}

// Preset detention reason (for javascript)
$lf = new libwordtemplates();
$detention_reason = $lf->getWordListDemerit();
$jReasonString = "var jArrayDetentionReason = new Array(".sizeof($detention_reason).");\n";
for ($i=0; $i<sizeof($detention_reason); $i++) {
	$jReasonString .= "jArrayDetentionReason[".$i."] = new Array(1);\n";
	$jReasonString .= "jArrayDetentionReason[".$i."][0] = '".$detention_reason[$i]."';\n";
}

?>

<script language="javascript">

<?=$jReasonString?>

function checkForm(){
	for(var i=1;i<=<?=sizeof($student)?>;i++){
		if (document.getElementById('TextReason'+i).value=='') {
			alert('<?=$i_alert_pleasefillin.$i_Discipline_Reason?>');
			return false;
		}
	}
	return true;
}

</script>

<br />
<form name="form1" method="post" action="edit_update.php">
<table width="97%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="navigation"><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?></td>
	</tr>
</table>
<table width="88%" border="0" cellpadding="5" cellspacing="0">
<tr>
	<td>
		<br />
<?=$TableIndStudent?>
		<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td valign="top" nowrap="nowrap">
					<span class="tabletextremark"><?=$i_general_required_field?></span>
				</td>
				<td width="80%">&nbsp;</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td height="1" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1"></td>
</tr>
<tr>
	<td align="right">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="center">
<?= $linterface->GET_ACTION_BTN($button_continue, "submit", "return checkForm();")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='student_record.php'")?>
				</td>
			</tr>
		</table>
	</td>
</tr>
</table>

<input type="hidden" name="RecordID" value="<?=$rid?>" />
</form>
<br />
<?
print $linterface->FOCUS_ON_LOAD("form1.TextReason1");
$linterface->LAYOUT_STOP();
?>
