<?php
// Modifying by : 

######## Change Log [Start] #########
#
#	Date	:	2017-10-18	(Bill)	[2017-1003-1124-05235]
#				apply temp array to reduce query number
#
#	Date	:	2017-10-11 (Bill)	[DM#3270]
#				return to step 1 if all selected groups without any users
#
#	Date	:	2011-03-18 (Henry Chow)
#				change default selection of "Assign Session" to "Auto"
#
######### Change Log [End] ##########

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");

intranet_auth();
intranet_opendb();

if(sizeof($student)==0) {
	header("Location: new.php");
	exit;
}

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Detention-New");

$lgrouping = new libgrouping();

$CurrentPage = "Management_Detention";
$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($eDiscipline['Detention_Arrangement'], "", 1);
$TAGS_OBJ[] = array($eDiscipline['Session_Management'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/management/detention/settings/calendar.php", 0);

$PAGE_NAVIGATION[] = array($i_Discipline_Detention_List, "list.php");
$PAGE_NAVIGATION[] = array($i_Discipline_New_Student_Records, "");

$STEPS_OBJ[] = array($i_Discipline_Select_Student, 0);
$STEPS_OBJ[] = array($i_Discipline_Add_Record, 1);

$NoticeTemplateAva = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO("",1);
if (!$lnotice->disabled && !empty($NoticeTemplateAva))
	$STEPS_OBJ[] = array($i_Discipline_Enotice_Setting, 0);
else
	$STEPS_OBJ[] = array($button_finish, 0);

$student = $_POST['student'];

# Retrieve all students (from U - User, G - Group)
$student2 = array();
$permitted[] = 2;
foreach($student as $k=>$d)
{
	$type = substr($d, 0, 1);
	$type_id = substr($d, 1);
	switch($type)
	{
		case "U":
			$student2[] = $type_id;
			break;
		case "G":
			$groupIDAry[] = $type_id;
			$temp = $lgrouping->returnGroupUsersInIdentity($groupIDAry, $permitted);		
			//$temp = $lgrouping->returnGroupUsers($type_id);
			foreach($temp as $k1=>$d1) {
				$student2[] = $d1['UserID'];	
			}
			break;
		case "C":
			$temp = $ldiscipline->getStudentListByClassID($type_id);
			for($i=0; $i<sizeof($temp); $i++) {
				$student2[] = $temp[$i][0];	
			}
			break;
		default:
			$student2[] = $d;
			break;
	}
}
if(is_array($student2)) {
	$student = array_keys(array_count_values($student2));
}
if(empty($student)) {
	header("Location: new.php");
	exit;
}
if (!is_array($student)) $student = explode(",", $student);

$lc = new libclass();

if ($_POST["submit_flag"] == "YES")
{
	// Get Submitted Data
//	$submitStudent = explode(",", $_POST["student"]);
	$submitStudent = $student;
	$submitDetentionCount0 = $_POST["DetentionCount0"];
	$submitTextReason0 = $_POST["TextReason0"];
	for ($k=1; $k<=$submitDetentionCount0; $k++) {
		${"submitSelectSession0_".$k} = $_POST["SelectSession0_".$k];
	}
	
	$submitTextRemark0 = $_POST["TextRemark0"];
	for ($l=1; $l<=sizeof($submitStudent); $l++) {
		${"submitDetentionCount".$l} = $_POST["DetentionCount".$l];
		${"submitTextReason".$l} = $_POST["TextReason".$l];
		for ($m=1; $m<=${"submitDetentionCount".$l}; $m++) {
			${"submitSelectSession".$l."_".$m} = $_POST["SelectSession".$l."_".$m];
			if (${"submitSelectSession".$l."_".$m} <> "AUTO" && ${"submitSelectSession".$l."_".$m} <> "LATER") {
				$selectedSessionForOneStudent[$l][] = ${"submitSelectSession".$l."_".$m};
				$allSelectedSession[] = ${"submitSelectSession".$l."_".$m};
			}
		}
		${"submitTextRemark".$l} = $_POST["TextRemark".$l];
	}
	
	// Check Past Session and Available Session
	for ($l=1; $l<=sizeof($submitStudent); $l++) {
		$assignedSessionForEachStudent = array();
		
		# Performance Improvement	[2017-1003-1124-05235]
		// if any auto assign options selected, get detention list outside looping
		$_hasAuto = false;
		for ($m=1; $m<=${"submitDetentionCount".$l}; $m++) {
			if (${"submitSelectSession".$l."_".$m} == "AUTO")
			{
				$_hasAuto = true;
				break;
			}
		}
		if ($_hasAuto) {
			$result = $ldiscipline->getDetentionList("FUTURE", "AVAILABLE", "THIS_FORM", $submitStudent[$l-1], "ASC");
		}
		
		for ($m=1; $m<=${"submitDetentionCount".$l}; $m++) {
			if (${"submitSelectSession".$l."_".$m} == "AUTO")
			{
				//$result = $ldiscipline->getDetentionList("FUTURE", "AVAILABLE", "THIS_FORM", $submitStudent[$l-1], "ASC");
				
				$TmpDetentionID = "";
				if (is_array($allSelectedSession)) {
					$TmpSessionArrayCount = array_count_values($allSelectedSession);
				}
				
				$AutoSuccess = false;
				for ($i=0; $i<sizeof($result); $i++) {
					list($TmpDetentionID,$TmpDetentionDate,$TmpDetentionDayName,$TmpStartTime,$TmpEndTime,$TmpPeriod,$TmpLocation,$TmpForm,$TmpPIC,$TmpVacancy,$TmpAssigned,$TmpAvailable) = $result[$i];
					
					$currentTime = date('Y-m-d H:i:s');
					$detentionSession = $TmpDetentionDate." ".$TmpStartTime;
					if($currentTime>$detentionSession) {		# cannot auto assign to detention session which is already passed 
						//echo $currentTime.'/'.$detentionSession;
						$AutoSuccess = false;
					}
					else if (!(is_array($selectedSessionForOneStudent[$l]) && in_array($TmpDetentionID, $selectedSessionForOneStudent[$l])) && ($TmpSessionArrayCount[$TmpDetentionID] < $TmpAvailable) && !($ldiscipline->checkStudentInSession($submitStudent[$l-1], $TmpDetentionID))) {
						$AutoSuccess = true;
						break;
					}
				}
				
				if ($TmpDetentionID == "" || $AutoSuccess == false) {
					$AvailableFlag[$l][$m] = "NO";
				}
				else {
					$AvailableFlag[$l][$m] = "YES";
					$selectedSessionForOneStudent[$l][] = $TmpDetentionID;
					$allSelectedSession[] = $TmpDetentionID;
					$AutoDetentionID[$l][$m] = $TmpDetentionID;
				}
			}
			else if (${"submitSelectSession".$l."_".$m} != "LATER") {
				$StudentInSessionFlag[$l][$m] = ($ldiscipline->checkStudentInSession($submitStudent[$l-1], ${"submitSelectSession".$l."_".$m}))?"YES":"NO";
				$PastFlag[$l][$m] = ($ldiscipline->checkPastByDetentionID(${"submitSelectSession".$l."_".$m}))?"YES":"NO";
				if (is_array($assignedSession)) {		// array for sessions that are not assigned by AUTO
					$TmpAssignedSessionArrayCount = array_count_values($assignedSession);
				}
				if (in_array(${"submitSelectSession".$l."_".$m}, $assignedSessionForEachStudent)) {
					$StudentInSessionFlag[$l][$m] = "YES";
				}
				$FullFlag[$l][$m] = ($ldiscipline->checkFullByDetentionID(${"submitSelectSession".$l."_".$m}, $TmpAssignedSessionArrayCount[${"submitSelectSession".$l."_".$m}] + 1))?"YES":"NO";
				if ($StudentInSessionFlag[$l][$m] == "NO" && $PastFlag[$l][$m] == "NO" && $FullFlag[$l][$m] == "NO") {
					$assignedSession[] = ${"submitSelectSession".$l."_".$m};
					$assignedSessionForEachStudent[] = ${"submitSelectSession".$l."_".$m};
				}
			}
		}
	}
	
	$FailFlag = false;
	for ($l=1; $l<=sizeof($submitStudent); $l++) {
		for ($m=1; $m<=${"submitDetentionCount".$l}; $m++) {
			$allAvailableFlag[] = $AvailableFlag[$l][$m];
			$allStudentInSessionFlag[] = $StudentInSessionFlag[$l][$m];
			$allPastFlag[] = $PastFlag[$l][$m];
			$allFullFlag[] = $FullFlag[$l][$m];
		}
	}
	
	if (!((is_array($allAvailableFlag) && in_array("NO", $allAvailableFlag)) || (is_array($allStudentInSessionFlag) && in_array("YES", $allStudentInSessionFlag)) || (is_array($allPastFlag) && in_array("YES", $allPastFlag)) || (is_array($allFullFlag) && in_array("YES", $allFullFlag))))
	{
		$allResult = true;
		for ($l=1; $l<=sizeof($submitStudent); $l++) {
			for ($m=1; $m<=${"submitDetentionCount".$l}; $m++) {
				$DetentionArr['StudentID'] = "'".$submitStudent[$l-1]."'";
				
				if (${"submitSelectSession".$l."_".$m}=="AUTO") {
					$DetentionArr['DetentionID'] = "'".$AutoDetentionID[$l][$m]."'";
				}
				else if (${"submitSelectSession".$l."_".$m}=="LATER") {
					$DetentionArr['DetentionID'] = "NULL";
				}
				else {
					$DetentionArr['DetentionID'] = "'".${"submitSelectSession".$l."_".$m}."'";
				}
				//$DetentionArr['DetentionID'] = "'".((${"submitSelectSession".$l."_".$m}=="AUTO")?$AutoDetentionID[$l][$m]:${"submitSelectSession".$l."_".$m})."'";
				
				$DetentionArr['Reason'] = "'".${"submitTextReason".$l}."'";
				$DetentionArr['Remark'] = "'".${"submitTextRemark".$l}."'";
				$DetentionArr['RequestedBy'] = "'".$UserID."'";
				$DetentionArr['ArrangedBy'] = "'".$UserID."'";
				$DetentionArr['ArrangedDate'] = 'NOW()';
				$DetentionArr['PICID'] = "'".implode(',',${"PIC".$l})."'";
				
				$result = $ldiscipline->insertDetention($DetentionArr);
				$RecordID[] = $result;
				$allResult = $allResult && $result;
			}
		}
		
		if ($allResult) {
			$SysMsg = "add";
		}
		else {
			$SysMsg = "add_failed";
		}
		
		$RecordIDStr = implode(",",$RecordID);
		$RedirectPage = "new3.php?msg=$SysMsg&rid=$RecordIDStr";
		
		intranet_closedb();
		
		header("Location: $RedirectPage");
	}
	else {
		$FailFlag = true;
	}
}	// End of submit_flag = "YES"

$linterface->LAYOUT_START();

# PIC Selection Menu
if(empty($PIC))		
	$PIC[] = $_SESSION['UserID'];
if (is_array($PIC) && sizeof($PIC)>0)
{
	$list = implode(",", $PIC);
	$namefield = getNameFieldWithLoginByLang();
	$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE RecordType = 1 AND UserID IN ($list) AND RecordStatus = 1 ORDER BY $namefield";
	$array_PIC = $ldiscipline->returnArray($sql);
}
$PIC_selected = $linterface->GET_SELECTION_BOX($array_PIC, "name='PIC[]' ID='PIC[]' class='select_studentlist' size='6' multiple='multiple'", "");

$session_times = $ldiscipline->DetentinoSession_MAX ? $ldiscipline->DetentinoSession_MAX : 10;
for ($i = 1; $i <= $session_times; $i++) $OptionForDetentionCount .= "<option value=\"$i\">$i</option>";

//$optgroupContent = "{$i_Discipline_Date} | {$i_Discipline_Time} | {$i_Discipline_Location} | {$i_Discipline_Form} | {$i_Discipline_PIC} | {$i_Discipline_Vacancy}";
//$optgroupContent = "{$i_Discipline_Date} | {$i_Discipline_Time} | {$i_Discipline_Location} | {$i_Discipline_PIC} | {$i_Discipline_Vacancy}";


### Performance Improvement - Call function / Perform looping outside getSelectSessionString()	[2017-1003-1124-05235]		[Start]

// Preset String
$optgroupContent = $ldiscipline->displayDetentionSessionDefaultString();
$TmpAllForms = $ldiscipline->getAllFormString();

function getSelectSessionString($parIndex="", $parSessionNumber="", $parAvailableSessionCount="", $parDetentionSessionStr="")
{
	global $ldiscipline, $i_Discipline_Auto_Assign_Session, $i_Discipline_All_Forms, $optgroupContent, $TmpAllForms, $i_Discipline_Put_Into_Unassign_List;
	
//	$TmpAllForms = $ldiscipline->getAllFormString();
//	if ($parUserID == "") {
//		$result = $ldiscipline->getDetentionList("FUTURE", "AVAILABLE", "ALL_FORMS", "", "ASC");
//	}
//	else {
//		$result = $ldiscipline->getDetentionList("FUTURE", "AVAILABLE", "THIS_FORM", $parUserID, "ASC");
//	}
	
	$SelectSession = "<table><tr><td>";
	$SelectSession .= "<select name=\"SelectSession".$parIndex."_".$parSessionNumber."\" id=\"SelectSession".$parIndex."_".$parSessionNumber."\" class=\"formtextbox\">";
	
	// Hide auto assign option if no available session
	$UnassignChecked = "";
	if ($parAvailableSessionCount > 0) {
		$SelectSession .= "<option class=\"row_avaliable\" value=\"AUTO\" selected>".$i_Discipline_Auto_Assign_Session."</option>";
	}
	else {
		$UnassignChecked = " selected";
	}
	$SelectSession .= "<option class=\"row_avaliable\" value=\"LATER\" ".$UnassignChecked.">".$i_Discipline_Put_Into_Unassign_List."</option>";
	
//	$SelectSession .= "<optgroup label=\"Date | Time | Location | Form | PIC | Vacancy\">";
	$SelectSession .= "<optgroup label=\"".$optgroupContent."\">";
	
//	for ($i=0; $i<sizeof($result); $i++) {
//		list($TmpDetentionID[$i],$TmpDetentionDate[$i],$TmpDetentionDayName[$i],$TmpStartTime[$i],$TmpEndTime[$i],$TmpPeriod[$i],$TmpLocation[$i],$TmpForm[$i],$TmpPIC[$i],$TmpVacancy[$i],$TmpAssigned[$i],$TmpAvailable[$i]) = $result[$i];
//		if ($TmpForm[$i] == $TmpAllForms) $TmpForm[$i] = $i_Discipline_All_Forms;
//		$SessionString = $ldiscipline->displayDetentionSessionString($TmpDetentionID[$i]);
//		$SelectSession .= "<option class=\"row_avaliable\" value=\"".$TmpDetentionID[$i]."\">".$SessionString."</option>";
//	}
	$SelectSession .= $parDetentionSessionStr;
	
	$SelectSession .= "</optgroup>";
	$SelectSession .= "</select>";
	$SelectSession .= "</td><td>";
	$SelectSession .= "<span id=\"SpanSysMsg".$parIndex."_".$parSessionNumber."\"></span>";
	$SelectSession .= "</td></tr></table>";
	
	return $SelectSession;
}

// Temp Data array
$detentionSessionAry = array();
$detentionSessionCountAry = array();
$detentionSessionStringAry = array();

# Apply All Detention Settings
// Get Detention Session
$detentionSessionList = $ldiscipline->getDetentionList("FUTURE", "AVAILABLE", "ALL_FORMS", "", "ASC");

// loop Sessions
$detentionSessionStr = "";
$detentionSessionCount = count($detentionSessionList);
for ($d=0; $d<$detentionSessionCount; $d++)
{
	$thisDetentionID = $detentionSessionList[$d][0];
	$thisDetentionStr = $ldiscipline->displayDetentionSessionString($thisDetentionID);
	$detentionSessionStringAry[$thisDetentionID] = $thisDetentionStr;
	
	$detentionSessionStr .= "<option class=\"row_avaliable\" value=\"".$thisDetentionID."\">".$thisDetentionStr."</option>";
}

// Data for Apply All Detention Settings
$detentionSessionAry[0] = $detentionSessionStr;
$detentionSessionCountAry[0] = $detentionSessionCount;

// loop for Individual Student
$TableIndStudent = "";
for ($i=0; $i<sizeof($student); $i++)
{
	# Student Detention Settings
	// Get available Detention Session
	$detentionSessionList = $ldiscipline->getDetentionList("FUTURE", "AVAILABLE", "THIS_FORM", $student[$i], "ASC");
	
	// loop Sessions
	$detentionSessionStr = "";
	$detentionSessionCount = count($detentionSessionList);
	for ($d=0; $d<$detentionSessionCount; $d++) {
		$thisDetentionID = $detentionSessionList[$d][0];
		
		if(isset($detentionSessionStringAry[$thisDetentionID])) {
			$thisDetentionStr = $detentionSessionStringAry[$thisDetentionID];
		}
		else {
			$thisDetentionStr = $ldiscipline->displayDetentionSessionString($thisDetentionID);
			$detentionSessionStringAry[$thisDetentionID] = $thisDetentionStr;
		}
		
		$detentionSessionStr .= "<option class=\"row_avaliable\" value=\"".$thisDetentionID."\">".$thisDetentionStr."</option>";
	}
	
	// Data for Student Detention Settings
	$detentionSessionAry[$i+1] = $detentionSessionStr;
	$detentionSessionCountAry[$i+1] = $detentionSessionCount;
	
### Performance Improvement - Call function / Perform looping outside getSelectSessionString()	[2017-1003-1124-05235]		[End]
	
	
	// Get Student Name
	$resultStudentName = $ldiscipline->getStudentNameByID($student[$i]);
	list($TmpUserID, $TmpName, $TmpClassName, $TmpClassNumber) = $resultStudentName[0];
	
	$TableIndStudent .= "<br />";
	$TableIndStudent .= "<table align=\"center\" width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
	$TableIndStudent .= "<tr>";
	$TableIndStudent .= "<td colspan=\"2\" valign=\"top\" nowrap=\"nowrap\">".($i+1).".";
	$TableIndStudent .= "<img src=\"".$image_path."/".$LAYOUT_SKIN."/ediscipline/icon_stu_ind.gif\" width=\"20\" height=\"20\" align=\"absmiddle\">";
	$TableIndStudent .= "<span class=\"sectiontitle\">".$TmpClassName."-".$TmpClassNumber." ".$TmpName."</span>";
	$TableIndStudent .= "</td>";
	$TableIndStudent .= "</tr>";
	$TableIndStudent .= "<tr valign=\"top\" class=\"tablerow1\">";
	$TableIndStudent .= "<td nowrap=\"nowrap\" class=\"formfieldtitle\">";
	$TableIndStudent .= "<span class=\"tabletext\">".$i_Discipline_Detention_Times." <span class=\"tabletextrequire\">*</span></span>";
	$TableIndStudent .= "</td>";
	$TableIndStudent .= "<td width=\"80%\">";
	$TableIndStudent .= "<input type=\"hidden\" id=\"HiddenDetentionCount".($i+1)."\" name=\"HiddenDetentionCount".($i+1)."\" value=\"1\" />";
	$TableIndStudent .= "<select name=\"DetentionCount".($i+1)."\" id=\"DetentionCount".($i+1)."\" onChange=\"javascript:updateSessionCell(".($i+1).");\">".$OptionForDetentionCount."</select>";
	$TableIndStudent .= "</td>";
	$TableIndStudent .= "</tr>";
	$TableIndStudent .= "<tr valign=\"top\" class=\"tablerow1\">";
	$TableIndStudent .= "<td nowrap=\"nowrap\" class=\"formfieldtitle\">";
	$TableIndStudent .= "<span class=\"tabletext\">".$i_Discipline_Reason." <span class=\"tabletextrequire\">*</span></span>";
	$TableIndStudent .= "</td>";
	$TableIndStudent .= "<td>";
	$TableIndStudent .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
	$TableIndStudent .= "<tr>";
	$TableIndStudent .= "<td width='50%'><input name=\"TextReason".($i+1)."\" id=\"TextReason".($i+1)."\" type=\"text\" class=\"tabletext\" style=\"width:98%\" /></td>";
	$TableIndStudent .= "<td width=\"25\" align=\"left\">";
	$TableIndStudent .= $linterface->GET_PRESET_LIST("jArrayDetentionReason", ($i+1), "TextReason".($i+1));
	$TableIndStudent .= "</td>";
	$TableIndStudent .= "<td width=\"2\" align=\"center\"></td>";
	$TableIndStudent .= "</tr>";
	$TableIndStudent .= "</table>";
	$TableIndStudent .= "</td>";
	$TableIndStudent .= "</tr>";
	$TableIndStudent .= "		
		<tr>
			<td valign='top' nowrap='nowrap' class='formfieldtitle'>$i_Discipline_System_Contact_PIC<span class='tabletextrequire'>* </span></td>
			<td>		
				<table>
					<tr>
						<td>".$linterface->GET_SELECTION_BOX($array_PIC, "name='PIC".($i+1)."[]' id='PIC".($i+1)."[]' class='select_studentlist' size='6' multiple='multiple'", "")."</td>
						<td valign='bottom'>";
						if(!$ldiscipline->OnlyAdminSelectPIC || ($ldiscipline->OnlyAdminSelectPIC && $ldiscipline->IS_ADMIN_USER()) )
						{
							$TableIndStudent .= $linterface->GET_BTN($button_select, "button","newWindow('../choose_pic.php?fieldname=PIC".($i+1)."[]',11)")."<br />";
							$TableIndStudent .= $linterface->GET_BTN($button_remove, "button","checkOptionRemove(document.form1.elements['PIC".($i+1)."[]'])");
						}
						$TableIndStudent .= "<input type='hidden' name='temp' id='temp' value='1'>
						</td>
					</tr>
				</table>
			</td>			
		</tr>
	";
	$TableIndStudent .= "<tr class=\"tablerow1\">";
	$TableIndStudent .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\">".$i_Discipline_Session." <span class=\"tabletextrequire\">*</span></td>";
	$TableIndStudent .= "<td valign=\"top\"><table name=\"TableSession".($i+1)."\" id=\"TableSession".($i+1)."\" width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\"><tr><td width=\"15\" nowrap>1.</td><td nowrap>".getSelectSessionString($i+1, 1, $detentionSessionCount, $detentionSessionStr)."</td></tr></table></td>";
	$TableIndStudent .= "</tr>";
	$TableIndStudent .= "<tr class=\"tablerow1\">";
	$TableIndStudent .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\">".$i_Discipline_Remark."</td>";
	$TableIndStudent .= "<td valign=\"top\">".$linterface->GET_TEXTAREA("TextRemark".($i+1), "")."</td>";
	$TableIndStudent .= "</tr>";
	if ($i != sizeof($student)-1) {
		$TableIndStudent .= "<tr>";
		$TableIndStudent .= "<td height=\"1\" colspan=\"2\" class=\"dotline\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"1\"></td>";
		$TableIndStudent .= "</tr>";
	}
	$TableIndStudent .= "</table>";
}

// Preset Detention Reason (for javascript)
$lf = new libwordtemplates();
$detention_reason = $lf->getWordListDemerit();
$jReasonString = "var jArrayDetentionReason = new Array(".sizeof($detention_reason).");\n";
for ($i=0; $i<sizeof($detention_reason); $i++) {
	$jReasonString .= "jArrayDetentionReason[".$i."] = new Array(1);\n";
	$jReasonString .= "jArrayDetentionReason[".$i."][0] = '".$detention_reason[$i]."';\n";
}

// Preset Detention Session (for javascript)
$jSessionString = "var SelectSessionArr=new Array(".(sizeof($student)+1).");\n";
for ($k=0; $k<=sizeof($student); $k++) {
	$jSessionString .= "SelectSessionArr[".$k."]=new Array(".$session_times.");\n";
}
for ($i=0; $i<=sizeof($student); $i++) {
	for ($j=0; $j<$session_times; $j++) {
		//$jSessionString .= "SelectSessionArr[".$i."][".$j."]='".str_replace("'", "&#039;", getSelectSessionString($i, $j+1, ($i==0)?"":$student[$i-1]))."';\n";
		$jSessionString .= "SelectSessionArr[".$i."][".$j."]='".str_replace("'", "&#039;", getSelectSessionString($i, $j+1, $detentionSessionCountAry[$i], $detentionSessionAry[$i]))."';\n";
	}
}

// Initial Form after Submit Fails (for javascript)
if ($FailFlag)
{
	$SessionNAMsg = str_replace("\n", "", str_replace("'", "\"", $linterface->GET_SYS_MSG("session_not_available2")));
	$SessionPastMsg = str_replace("\n", "", str_replace("'", "\"", $linterface->GET_SYS_MSG("session_past")));
	$SessionFullMsg = str_replace("\n", "", str_replace("'", "\"", $linterface->GET_SYS_MSG("session_full")));
	$SessionAssignedBeforeMsg = str_replace("\n", "", str_replace("'", "\"", $linterface->GET_SYS_MSG("session_assigned_before")));

	$jInitialForm = "";
	for ($l=0; $l<=sizeof($submitStudent); $l++) {
		$jInitialForm .= "if(document.getElementById('DetentionCount".$l."')) document.getElementById('DetentionCount".$l."').value='".${"submitDetentionCount".$l}."';\n";
		$jInitialForm .= "if(document.getElementById('HiddenDetentionCount".$l."')) document.getElementById('HiddenDetentionCount".$l."').value='1';\n";
		$jInitialForm .= "updateSessionCell(".$l.");\n";
		$jInitialForm .= "if(document.getElementById('TextReason".$l."')) document.getElementById('TextReason".$l."').value='".${"submitTextReason".$l}."';\n";
		for ($m=1; $m<=${"submitDetentionCount".$l}; $m++) {
			$jInitialForm .= "if(document.getElementById('SelectSession".$l."_".$m."')) document.getElementById('SelectSession".$l."_".$m."').value='".${"submitSelectSession".$l."_".$m}."';\n";
			if ($AvailableFlag[$l][$m] == "NO") {
				$jInitialForm .= "if(document.getElementById('SpanSysMsg".$l."_".$m."')) document.getElementById('SpanSysMsg".$l."_".$m."').innerHTML='".$SessionNAMsg."';\n";
			}
			else if ($FullFlag[$l][$m] == "YES") {
				$jInitialForm .= "if(document.getElementById('SpanSysMsg".$l."_".$m."')) document.getElementById('SpanSysMsg".$l."_".$m."').innerHTML='".$SessionFullMsg."';\n";
			}
			else if ($PastFlag[$l][$m] == "YES") {
				$jInitialForm .= "if(document.getElementById('SpanSysMsg".$l."_".$m."')) document.getElementById('SpanSysMsg".$l."_".$m."').innerHTML='".$SessionPastMsg."';\n";
			}
			else if ($StudentInSessionFlag[$l][$m] == "YES") {
				$jInitialForm .= "if(document.getElementById('SpanSysMsg".$l."_".$m."')) document.getElementById('SpanSysMsg".$l."_".$m."').innerHTML='".$SessionAssignedBeforeMsg."';\n";
			}
		}
		$jInitialForm .= "if(document.getElementById('TextRemark".$l."')) document.getElementById('TextRemark".$l."').value='".${"submitTextRemark".$l}."';\n";
	}
}
?>

<script language="javascript">
<?=$jReasonString?>
<?=$jSessionString?>

function updateSessionCell(studentIndex){
	if(studentIndex==undefined) studentIndex='0';

	if(document.getElementById('HiddenDetentionCount'+studentIndex))
	{
		var oldCount=parseInt(document.getElementById('HiddenDetentionCount'+studentIndex).value);
		var newCount=parseInt(document.getElementById('DetentionCount'+studentIndex).value);
	}
	
	if(oldCount<newCount)
	{
		var newRow, newCell;
		for(var i=oldCount;i<newCount;i++){
			newRow=document.getElementById('TableSession'+studentIndex).insertRow(document.getElementById('TableSession'+studentIndex).rows.length);
			newCell0=newRow.insertCell(0);
			newCell1=newRow.insertCell(1);
			newCell0.innerHTML=(i+1)+'.';
			newCell1.setAttribute("noWrap", true);
			newCell1.innerHTML=SelectSessionArr[studentIndex][i];
		}
		document.getElementById('HiddenDetentionCount'+studentIndex).value = newCount;
	}
	else if(oldCount>newCount){
		for(var i=oldCount;i>newCount;i--){
			document.getElementById('TableSession'+studentIndex).deleteRow(i-1);
		}
		document.getElementById('HiddenDetentionCount'+studentIndex).value = newCount;
	}
}

function applyToAll(studentCount){
	var obj = document.form1;
	var pic_length = obj.elements['PIC[]'].length;
	
	for(var i=1;i<=studentCount;i++){
		var pic = "PIC"+i+"[]";
		for(j=0; j<obj.elements[pic].length; j++) {
			obj.elements[pic].options[j] = null;
		}
		for(j=0; j<pic_length; j++) {
			obj.elements[pic].options[j] = new Option(obj.elements['PIC[]'].options[j].text,obj.elements['PIC[]'].options[j].value);
		}
		
		document.getElementById('DetentionCount'+i).value = document.getElementById('DetentionCount0').value;
		updateSessionCell(i);
		
		for(var j=1;j<=document.getElementById('DetentionCount0').value;j++){
			document.getElementById('SelectSession'+i+'_'+j).value = document.getElementById('SelectSession0_'+j).value;
			if (document.getElementById('SelectSession'+i+'_'+j).value != document.getElementById('SelectSession0_'+j).value){
				document.getElementById('SelectSession'+i+'_'+j).value = 'AUTO';
			}
		}
		document.getElementById('TextReason'+i).value = document.getElementById('TextReason0').value;
		document.getElementById('TextRemark'+i).value = document.getElementById('TextRemark0').value;
	}
}

function checkForm(){
	for(var i=1;i<=<?=sizeof($student)?>;i++){
		if (document.getElementById('TextReason'+i).value=='') {
			alert('<?=$i_alert_pleasefillin.$i_Discipline_Reason?>');
			return false;
		}
	}
	
	for(var i=1;i<=<?=sizeof($student)?>;i++){
		var pic = "PIC"+i+"[]";
		if(document.form1.elements[pic].length<=0) {
			alert("<?=$i_alert_pleaseselect." ".$i_Discipline_System_Contact_PIC?>");
			return false;
		}
	}
	
	document.getElementById('submit_flag').value='YES';
	for(var i=1;i<=<?=sizeof($student)?>;i++){
		var pic = "PIC"+i+"[]";
		for(j=0; j<document.form1.elements[pic].length; j++) {
			document.form1.elements[pic].options[j].selected = true;
		}
	}
	
	return true;
}

</script>

<br />
<form name="form1" method="post" onsubmit="return checkForm();" action="new2.php">
<table width="97%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="navigation"><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?><br /><br /><br /><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
	</tr>
</table>
<table width="88%" border="0" cellpadding="5" cellspacing="0">
<tr>
	<td>
	<br />
	<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0" class="tablerow2">
<? if(sizeof($student)>1)
{
	?>	
	<tr>
		<td colspan="2" valign="top" nowrap="nowrap">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_section.gif" width="20" height="20" align="absmiddle">
						<span class="sectiontitle"><?=$eDiscipline["GlobalSettingsToAllStudent"]?></span>
					</td>
					<td align="right">
						<span class="sectiontitle">
							<?=$linterface->GET_BTN($i_Discipline_Apply_To_All, "button", "javascript:applyToAll(".sizeof($student).");")?>
						</span>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr valign="top">
		<td nowrap="nowrap" class="formfieldtitle">
			<span class="tabletext"><?=$i_Discipline_Detention_Times?> <span class="tabletextrequire">*</span></span>
		</td>
		<td width="80%">
			<input type="hidden" id="HiddenDetentionCount0" name="HiddenDetentionCount0" value="1" />
			<select name="DetentionCount0" id="DetentionCount0" onChange="javascript:updateSessionCell();"><?=$OptionForDetentionCount?></select>
		</td>
	</tr>
	<tr valign="top">
		<td nowrap="nowrap" class="formfieldtitle">
			<span class="tabletext"><?=$i_Discipline_Reason?> <span class="tabletextrequire">*</span></span>
		</td>
		<td>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td><input name="TextReason0" id="TextReason0"type="text" class="textboxtext" /></td>
					<td width="25" align="left">
						<?=$linterface->GET_PRESET_LIST("jArrayDetentionReason", 0, "TextReason0")?>
					</td>
					<td width="2" align="center"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Contact_PIC?><span class="tabletextrequire">* </span></td>
		<td>		
			<table>
				<tr>
					<td><?=$PIC_selected?></td>
					<td valign="bottom">
					<? if(!$ldiscipline->OnlyAdminSelectPIC || ($ldiscipline->OnlyAdminSelectPIC && $ldiscipline->IS_ADMIN_USER()) ) { ?>
						<?=$linterface->GET_BTN($button_select, "button","newWindow('../choose_pic.php?fieldname=PIC[]',11)");?><br />
						<?=$linterface->GET_BTN($button_remove, "button","checkOptionRemove(document.form1.elements['PIC[]'])");?>
					</td>
					<? } ?>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_Session?> <span class="tabletextrequire">*</span></td>
		<td valign="top"><table name="TableSession0" id="TableSession0" width="100%" border="0" cellpadding="5" cellspacing="0"><tr><td width="15" nowrap>1.</td><td nowrap><?=getSelectSessionString(0, 1, $detentionSessionCountAry[0], $detentionSessionAry[0])?></td></tr></table></td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_Remark?></td>
		<td valign="top"><?=$linterface->GET_TEXTAREA("TextRemark0", "")?></td>
	</tr>
<? } ?>	
	</table>
	<?=$TableIndStudent?>
	<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td valign="top" nowrap="nowrap">
				<span class="tabletextremark"><?=$i_general_required_field?></span>
			</td>
			<td width="80%">&nbsp;</td>
		</tr>
	</table>
	</td>
</tr>
<tr>
	<td height="1" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1"></td>
</tr>
<tr>
	<td align="right">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_continue, "submit")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_back, "button", "this.form.action='new.php';this.form.submit();")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'")?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>

<input type="hidden" name="submit_flag" id="submit_flag" value="NO" />
<input type="hidden" name="student" id="student" value="<?=implode(",",$student)?>" />

<!-- Step 1 data //-->
<? foreach($student as $k=>$d) { ?>
	<input type="hidden" name="student[]" id="student[]" value="<?=$d?>" />	
<? } ?>

</form>
<br />

<?
print $linterface->FOCUS_ON_LOAD("form1.DetentionCount0");
$linterface->LAYOUT_STOP();
?>

<?
if ($FailFlag) {
?>
<script language="javascript">
	<?=$jInitialForm?>
</script>
<?
}
?>