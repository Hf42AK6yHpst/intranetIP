<?php
# using : 

###### Change Log [Start] #####
#
#	Date	:	2011-03-04 (Henry Chow)
#				export 1 more column "PIC"
#
####### Change Log [End] ###### 

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
 
# Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$ldiscipline = new libdisciplinev12();
$lexport = new libexporttext();

$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Case_Record-View");

$ExportArr = array();

if(isset($AcademicYearID) && $AcademicYearID!="") {
	$startDate = getStartDateOfAcademicYear($AcademicYearID);
	$endDate = getEndDateOfAcademicYear($AcademicYearID);
	$conds .= " AND (SES.DateInput BETWEEN '$startDate' AND '$endDate')";	
}

if ($targetClass != "All_Classes" && $targetClass != "") {
	$conds .= " AND USR.ClassName = '$targetClass'";
}

if ($targetStudent == "Assigned") {
	$conds .= " AND SES.DetentionID IS NOT NULL";
} else if ($targetStudent == "Not_Assigned") {
	$conds .= " AND SES.DetentionID IS NULL";
}

if ($targetStatus == "Detented") {
	$conds .= " AND SES.AttendanceStatus = 'PRE'";
} else if ($targetStatus == "Absent") {
	$conds .= " AND SES.AttendanceStatus = 'ABS'";
}

$name_field = getNameFieldByLang("USR.");
if ($searchString != "") {
	$conds .= " AND (CONCAT(USR.ClassName, '-', USR.ClassNumber) LIKE '%$searchString%' OR $name_field LIKE '%$searchString%' OR SES.Reason LIKE '%$searchString%' OR SES.Remark LIKE '%$searchString%')";
}

$yearID = Get_Current_Academic_Year_ID();
$clsName = ($intranet_session_language=="en") ? "yc.ClassTitleEN" : "yc.ClassTitleB5";
$name_field = getNameFieldByLang("USR.");

$sql = "SELECT ";
//			CONCAT($clsName, '-', ycu.ClassNumber) AS Class,
$sql .= " $clsName as ClassName, ";
$sql .= "
			$name_field AS Student,
			SES.Reason,
			IFNULL(SES.Remark, '') AS Remark,
			SUBSTRING(SES.DateInput,1,10) AS DateInput,
			SES.DetentionID,
			SES.AttendanceStatus,
			CONCAT('<input type=checkbox name=RecordID[] value=', SES.RecordID ,'>') AS CheckBoxField,
			SES.StudentID,
			CONCAT(D.DetentionDate, D.StartTime, D.EndTime, D.Location) AS TempForSort,
			SES.RecordID,
			ycu.ClassNumber
		FROM
			DISCIPLINE_DETENTION_STUDENT_SESSION SES
			LEFT JOIN DISCIPLINE_DETENTION_SESSION D ON (D.DetentionID=SES.DetentionID)
			LEFT OUTER JOIN INTRANET_USER USR ON (SES.StudentID = USR.UserID)
			LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID = USR.UserID)
			LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID = ycu.YearClassID)
		WHERE 1 AND SES.RecordStatus = 1 AND yc.AcademicYearID=$yearID $conds
		GROUP BY SES.RecordID 
		ORDER BY $clsName, ycu.ClassNumber
		";
		
$row = $ldiscipline->returnArray($sql);
//debug_pr($row);
// Create data array for export
for($i=0; $i<sizeof($row); $i++)
{
	$TempAllForms = $ldiscipline->getAllFormString();
	$TempList = $ldiscipline->getDetentionListByDetentionID($row[$i]['DetentionID']);
	list($TmpDetentionID,$TmpDetentionDate,$TmpDetentionDayName,$TmpStartTime,$TmpEndTime,$TmpPeriod,$TmpLocation,$TmpForm,$TmpPIC,$TmpVacancy,$TmpAssigned,$TmpAvailable) = $TempList[0];
	if ($TmpForm == $TempAllForms) $TmpForm = $i_Discipline_All_Forms;
	if($row[$i]['DetentionID']=='')
	{
		$SessionString='--';
	}
	else
	{
		$SessionString = substr($TmpDetentionDate,2)." (".substr($TmpDetentionDayName,0,3).") | ".$TmpPeriod." | ".intranet_htmlspecialchars($TmpLocation)." | ".$TmpForm." | ".$TmpPIC;
	}
	
	//Attendance 
	if($row[$i]['AttendanceStatus']=="PRE")
	{
		$attendance = $i_Discipline_Attended;
	}
	else if($row[$i]['AttendanceStatus']=="ABS")
	{
		$attendance = $i_Discipline_Absent;
	}
	else
	{
		$attendance = $i_Discipline_Not_Yet;
	}
						
 	$ExportArr[$i][0] = $row[$i]['ClassName'];		//Class Name
 	$ExportArr[$i][1] = $row[$i]['ClassNumber'];		//Class Number
	$ExportArr[$i][2] = $row[$i]['Student'];	//Student
	$ExportArr[$i][3] = $row[$i]['Reason'];		//Reason
	$ExportArr[$i][4] = $row[$i]['Remark'];		//Remark
	$ExportArr[$i][5] = $row[$i]['DateInput'];	//DateInput
	$ExportArr[$i][6] = $SessionString;			//SessionString
	$ExportArr[$i][7] = $attendance;		//Assigned
	//$ExportArr[$i][8] = $Status;		//Status
	
	$nameList = ""; 
	$picName = $ldiscipline->getDetentionStudentSessionPIC($row[$i]['RecordID'],"","");
	$nameList = $ldiscipline->getPICNameList($picName[0]);
	
	//$nameList = $ldiscipline->getPICNameList($row[$i]['DetentionID']);
	
	$ExportArr[$i][8] = $nameList;
}



//define column title
$exportColumn = array($i_ClassName,$i_ClassNumber,$i_Discipline_Student,$i_Discipline_Detention_Reason,$i_Discipline_Remark,$i_Discipline_Request_Date,$i_Discipline_Arranged_Session,$i_Discipline_Attendance,$i_Discipline_PIC);


$export_content .= $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

// Output the file to user browser
$filename = 'detention_student.csv';
$lexport->EXPORT_FILE($filename, $export_content);

?>
