<?php
// Editing by 
/*
 * 2016-01-10 (Carlos): Created for $sys_custom['eDiscipline']['WFN_Reports']
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
if(!$sys_custom['eDiscipline']['WFN_Reports']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Detention-View");


$linterface = new interface_html();

include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/edis_print_header.php");



// [2014-1216-1347-28164] get detention content with session type
$TempList = $ldiscipline->getDetentionListByDetentionID($did, ($sys_custom['eDiscipline']['CreativeSchDetentionCust']?true:false));
if($sys_custom['eDiscipline']['CreativeSchDetentionCust']){
	// [2014-1216-1347-28164] get detention session type
	list($TmpType, $TmpDetentionID,$TmpDetentionDate,$TmpDetentionDayName,$TmpStartTime,$TmpEndTime,$TmpPeriod,$TmpLocation,$TmpForm,$TmpPIC,$TmpVacancy,$TmpAssigned,$TmpAvailable) = $TempList[0];
} else {
	list($TmpDetentionID,$TmpDetentionDate,$TmpDetentionDayName,$TmpStartTime,$TmpEndTime,$TmpPeriod,$TmpLocation,$TmpForm,$TmpPIC,$TmpVacancy,$TmpAssigned,$TmpAvailable) = $TempList[0];
}

//$order_array = array("concat(TRIM(SUBSTRING_INDEX(Class, '-', 1)), IF(INSTR(Class, '-') = 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(Class, '-', -1)), 10, '0')))", "Student", "Reason", "DateInput", "Status");
$order_array = array("concat(TRIM(SUBSTRING_INDEX(ClassSort, '-', 1)), IF(INSTR(ClassSort, '-') = 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(ClassSort, '-', -1)), 10, '0')))", "Student", "Reason", "DateInput", "Status");
$orderby = $order_array[$field] . " " . ($order==1?"asc":"desc");
$classSort = ($field == 0);
$result = $ldiscipline->getDetentionInfoByDetentionID($did, $orderby, $classSort);
$result_size = count($result);


for ($i=0; $i<$result_size; $i++) {
	// [2015-0923-1037-19207] Get PICID
	//list($InfoClass[], $InfoClassNumber[], $InfoStudent[], $InfoStudentID[], $InfoDetentionID[], $InfoDemeritID[], $InfoReason[], $InfoRemark[], $InfoAttendanceStatus[], $InfoNoticeID[], $InfoRequestedBy[], $InfoArrangedBy[], $InfoArrangedDate[], $InfoAttendanceRemark[], $InfoAttendanceTakenBy[], $InfoAttendanceTakenDate[], $InfoDateInput[], $InfoDateModified[]) = $result[$i];
	if($classSort)
		list($InfoClass[], $InfoClassNumber[], $InfoStudent[], $InfoStudentID[], $InfoDetentionID[], $InfoDemeritID[], $InfoReason[], $InfoRemark[], $InfoAttendanceStatus[], $InfoNoticeID[], $InfoRequestedBy[], $InfoArrangedBy[], $InfoArrangedDate[], $InfoAttendanceRemark[], $InfoAttendanceTakenBy[], $InfoAttendanceTakenDate[], $InfoDateInput[], $InfoDateModified[], $InfoRecorID[], $InfoClassSort[], $InfoPIC[]) = $result[$i];
	else
		list($InfoClass[], $InfoClassNumber[], $InfoStudent[], $InfoStudentID[], $InfoDetentionID[], $InfoDemeritID[], $InfoReason[], $InfoRemark[], $InfoAttendanceStatus[], $InfoNoticeID[], $InfoRequestedBy[], $InfoArrangedBy[], $InfoArrangedDate[], $InfoAttendanceRemark[], $InfoAttendanceTakenBy[], $InfoAttendanceTakenDate[], $InfoDateInput[], $InfoDateModified[], $InfoRecorID[], $InfoPIC[]) = $result[$i];


	
}


$x .= '<style type="text/css" media="print"> .print-style { page-break-inside: avoid;}</style>';
$x .= '<style type="text/css">body {font-size:1em;}</style>';
$x .= '<table width="100%" align="center" class="print_hide" border="0">
		<tr>
			<td align="right">'.$linterface->GET_SMALL_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit_print").'</td>
		</tr>
		</table>';

$school_name = GET_SCHOOL_NAME();
$x .= '<h2 style="text-align:center;">'.$school_name.'</h2>';
$x .= '<h3 style="text-align:center;">'.str_replace(array('<!--DATE-->','<!--LOCATION-->'),array(date("d/m/Y", strtotime($TmpDetentionDate)),$TmpLocation), '下列學生因欠交功課，需要於 <!--DATE--> 放學後立即到 <!--LOCATION--> 室').'</h3>';
$x .= '<h3 style="text-align:center;">'.'當值老師簽署: <span style="display:inline-block;border-bottom:4px solid black;">'.str_repeat('&nbsp;',50).'</span>'.'</h3>';
$x .= '<div style="margin:0em 1em;;padding:0.2em;border:4px solid #CCC;background-color:#000;color:#FFF;font-style:italic;"> * 留堂學生如因事請假，次日小息必須到訓導室走廊等候處理。</div>';


for($i=0;$i<$result_size;$i++)
{
	$y = '<div class="print-style" style="margin:0.5em;">';
		$y .= '<table align="center" style="width:90%;border-collapse:collapse;border:4px solid black;font-family:標楷體,sans-serif;">';
			$y .= '<tr>';
				$y .= '<td style="border-bottom:2px solid #000;padding:4px;font-weight:bold;">班別</td><td style="border-bottom:2px solid #000;padding:4px;font-weight:bold;">學號</td><td style="border-bottom:2px solid #000;padding:4px;font-weight:bold;">學生姓名</td><td style="padding:4px;text-align:right;"><span style="display:inline-block;border:1px solid black;width:1em;height:1em;"></span></td><td style="padding:4px;font-weight:bold;">出席</td>';
			$y .= '</tr>';
			$y .= '<tr>';
				$y .= '<td style="padding:4px;">'.$InfoClass[$i].'</td><td style="padding:4px;">'.$InfoClassNumber[$i].'</td><td style="padding:4px;">'.$InfoStudent[$i].'</td><td style="padding:4px;text-align:right;"><span style="display:inline-block;border:1px solid black;width:1em;height:1em;"></span></td><td style="padding:4px;font-weight:bold;">請假原因: <span style="display:inline-block;border-bottom:1px solid black;padding:4px;">'.str_repeat('&nbsp;',17).'</span></td>';
			$y .= '</tr>';
			$y .= '<tr>';
				$y .= '<td colspan="3" rowspan="2" style="padding:4px;" width="50%">';
					$y .= '<table style="width:100%;border-collapse:collapse;border:1px solid black;">';
						$y .= '<tr style="border-bottom:1px solid black;">';
							$y .= '<td width="30%" style="padding:4px;text-align:center;font-weight:bold;">功課名稱</td><td width="30%" style="padding:4px;text-align:center;font-weight:bold;">日期</td><td width="40%" style="padding:4px;text-align:center;font-weight:bold;">科目</td>';
						$y .= '</tr>';
						$reason_date = date("d/m/Y", strtotime(substr($InfoReason[$i],0,10)));
						$parts = explode("-", substr($InfoReason[$i],11));
						$reason_number = trim($parts[1]);
						$reason_subject = trim($parts[0]);
						$y .= '<tr>';
							$y .= '<td style="padding:4px;text-align:center;">'.$reason_number.'</td><td style="padding:4px;text-align:center;">'.$reason_date.'</td><td style="padding:4px;text-align:center;">'.$reason_subject.'</td>';
						$y .= '</tr>';
					$y .= '</table>';
				$y .= '</td><td width="4%">&nbsp;</td><td style="padding:4px;font-weight:bold;" width="46%">簽到時間: <span style="display:inline-block;border-bottom:1px solid black;padding:4px;">'.str_repeat('&nbsp;',17).'</span></td>';
			$y .= '</tr>';
			$y .= '<tr>';
				$y .= '<td>&nbsp;</td><td style="padding:4px;font-weight:bold;">學生簽署: <span style="display:inline-block;border-bottom:1px solid black;padding:4px;">'.str_repeat('&nbsp;',17).'</span></td>';
			$y .= '</tr>';
		$y .= '</table><br />';
	$y .= '</div>';
	
	$x .= $y;
}

echo $x;

include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
intranet_closedb();
?>