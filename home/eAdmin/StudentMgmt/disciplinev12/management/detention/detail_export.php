<?php
// Modifying by : 

################################
# Date:	2015-10-05	Bill	[2015-0923-1037-19207]
#		added PIC
# Date:	2015-09-24	Bill	[2015-0924-1046-51071]
#		added Attendance Status & Attendance Remark
# Date:	2015-04-23	Bill	[2014-1216-1347-28164]
#		add detention session type to Session Info
# Date:	2014-11-27	Bill
#		improved: Change the first element in $order_array (Class->ClassSort)
# Date: 2014-09-12	YatWoon
#		improved: separate class and class number [Case#P67603]
#		Deploy: IPv10.1
################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lexport = new libexporttext();
$ldiscipline = new libdisciplinev12();
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Detention-View");

// [2014-1216-1347-28164] get detention content with session type
$TempList = $ldiscipline->getDetentionListByDetentionID($did, ($sys_custom['eDiscipline']['CreativeSchDetentionCust']?true:false));
if($sys_custom['eDiscipline']['CreativeSchDetentionCust']){
	// [2014-1216-1347-28164] get detention session type
	list($TmpType,$TmpDetentionID,$TmpDetentionDate,$TmpDetentionDayName,$TmpStartTime,$TmpEndTime,$TmpPeriod,$TmpLocation,$TmpForm,$TmpPIC,$TmpVacancy,$TmpAssigned,$TmpAvailable) = $TempList[0];
} else {
	list($TmpDetentionID,$TmpDetentionDate,$TmpDetentionDayName,$TmpStartTime,$TmpEndTime,$TmpPeriod,$TmpLocation,$TmpForm,$TmpPIC,$TmpVacancy,$TmpAssigned,$TmpAvailable) = $TempList[0];
}

if($sys_custom['eDiscipline']['CreativeSchDetentionCust']){
	// [2014-1216-1347-28164] add detention type
	$detentionDetail = $Lang['eDiscipline']['DetentionTypes']['Title'].":".$TmpType."\n";		//Detention Session Type
	$detentionDetail .= $i_Discipline_Location.":".intranet_htmlspecialchars($TmpLocation)."\n";//Location
} else {
	$detentionDetail = $i_Discipline_Location.":".intranet_htmlspecialchars($TmpLocation)."\n";	//Location
}
$detentionDetail .= $i_Discipline_Duty_Teacher.":".$TmpPIC."\n";								//PIC
$detentionDetail .= $i_Discipline_Date.":".$TmpDetentionDate."\n";								//Detention Date
$detentionDetail .= $i_Discipline_Time.":".$TmpPeriod."\n";										//Period
$detentionDetail .= $i_Discipline_Vacancy.":".$TmpVacancy."\n";									//Vacancy
$detentionDetail .= $i_Discipline_Students_Attending.":".$TmpAssigned."\n\n";					//Attending student

//$order_array = array("concat(TRIM(SUBSTRING_INDEX(Class, '-', 1)), IF(INSTR(Class, '-') = 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(Class, '-', -1)), 10, '0')))", "Student", "Reason", "DateInput", "Status");
$order_array = array("concat(TRIM(SUBSTRING_INDEX(ClassSort, '-', 1)), IF(INSTR(ClassSort, '-') = 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(ClassSort, '-', -1)), 10, '0')))", "Student", "Reason", "DateInput", "Status");
$orderby = $order_array[$field] . " " . ($order==1?"asc":"desc");
$classSort = ($field == 0);
$result = $ldiscipline->getDetentionInfoByDetentionID($did, $orderby, $classSort);

for ($i=0; $i<sizeof($result); $i++) {
	// [2015-0923-1037-19207] Get PICID
	//list($InfoClass, $InfoClassNumber, $InfoStudent, $InfoStudentID, $InfoDetentionID, $InfoDemeritID, $InfoReason, $InfoRemark, $InfoAttendanceStatus, $InfoNoticeID, $InfoRequestedBy, $InfoArrangedBy, $InfoArrangedDate, $InfoAttendanceRemark, $InfoAttendanceTakenBy, $InfoAttendanceTakenDate, $InfoDateInput, $InfoDateModified) = $result[$i];
	if($classSort)
		list($InfoClass, $InfoClassNumber, $InfoStudent, $InfoStudentID, $InfoDetentionID, $InfoDemeritID, $InfoReason, $InfoRemark, $InfoAttendanceStatus, $InfoNoticeID, $InfoRequestedBy, $InfoArrangedBy, $InfoArrangedDate, $InfoAttendanceRemark, $InfoAttendanceTakenBy, $InfoAttendanceTakenDate, $InfoDateInput, $InfoDateModified, $InfoRecorID, $InfoClassSort, $InfoPIC) = $result[$i];
	else
		list($InfoClass, $InfoClassNumber, $InfoStudent, $InfoStudentID, $InfoDetentionID, $InfoDemeritID, $InfoReason, $InfoRemark, $InfoAttendanceStatus, $InfoNoticeID, $InfoRequestedBy, $InfoArrangedBy, $InfoArrangedDate, $InfoAttendanceRemark, $InfoAttendanceTakenBy, $InfoAttendanceTakenDate, $InfoDateInput, $InfoDateModified, $InfoRecorID, $InfoPIC) = $result[$i];

	if ($InfoReason == "") {
		$InfoReason = "&nbsp;";
	}

	if ($InfoNoticeID == "") {
		$InfoStatus = "---";
	} else {
		$InfoStatus = $i_Discipline_Released_To_Student;
	}

	if ($InfoAttendanceStatus == "") {
		$InfoAttendanceStatus = "&nbsp;";
	} else if ($InfoAttendanceStatus == "PRE") {
		$InfoAttendanceStatus = $i_Discipline_Present;
	} else if ($InfoAttendanceStatus == "ABS") {
		$InfoAttendanceStatus = $i_Discipline_Absent;
	}
	
	// [2015-0924-1046-51071] display same as print view
//	if($InfoAttendanceRemark == "") {
//		$InfoAttendanceRemark = "---";	
//	}
	
	$TmpNoticeID = $ldiscipline->getNoticeIDByStudentIDDetentionID($InfoStudentID, $InfoDetentionID);
	if ($TmpNoticeID[0] == "") {
		$TmpSendNotice = "---";
	} else {
		$TmpSendNotice = $eDiscipline["SendNotice"];
	}
	
	// [2015-0923-1037-19207] Get PIC Name
	if($InfoPIC == ""){
		$InfoPIC = "---";
	} else {
		$InfoPIC = $ldiscipline->getPICNameList($InfoPIC);
	}
	
 	$ExportArr[$i][0] = $InfoClass;	
 	$ExportArr[$i][1] = $InfoClassNumber;	
	$ExportArr[$i][2] = $InfoStudent;			
	$ExportArr[$i][3] = $InfoReason;		
	$ExportArr[$i][4] = $InfoRemark;						
	$ExportArr[$i][5] = $InfoDateInput;
	// [2015-0923-1037-19207] add output column		
	$ExportArr[$i][6] = $InfoPIC;
	$ExportArr[$i][7] = $TmpSendNotice;
	// [2015-0924-1046-51071] add output column
	$ExportArr[$i][8] = $InfoAttendanceStatus;
	$ExportArr[$i][9] = $InfoAttendanceRemark;
	
}

//$exportColumn = array($i_Discipline_Class,$i_Discipline_Student,$i_Discipline_Detention_Reason,$i_Discipline_Remark,$i_Discipline_Request_Date,$i_Discipline_Status);
//$exportColumn = array($i_general_class, $Lang['General']['ClassNumber'], $i_Discipline_Student,$i_Discipline_Detention_Reason,$i_Discipline_Remark,$i_Discipline_Request_Date,$i_Discipline_Status);
// [2015-0924-1046-51071] added header
// [2015-0923-1037-19207] added header - PIC
$exportColumn = array($i_general_class, $Lang['General']['ClassNumber'], $i_Discipline_Student,$i_Discipline_Detention_Reason,$i_Discipline_Remark,$i_Discipline_Request_Date,$i_Discipline_PIC,$i_Discipline_Status,$Lang['eDiscipline']['Detention']['AttendanceStatus'],$Lang['eDiscipline']['Detention']['AttendanceRemark']);
$export_content .= $detentionDetail.$lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");
intranet_closedb();

// Output the file to user browser
$filename = 'detention_detail.csv';
$lexport->EXPORT_FILE($filename, $export_content);

/*

<table width="100%" align="center">
	<tr>
		<td align="right">
			<input name="btn_print" type="button" class="printbutton" value="<?=$button_print?>" onClick="window.print(); return false;" onMouseOver="this.className='printbuttonon'" onMouseOut="this.className='printbutton'">
		</td>
	</tr>
</table>
<table cellspacing="0" cellpadding="4" width="100%" align="center" border="0">
	<tbody>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="page_title_print"><?=$i_Discipline_Attendance_Sheet?></td>
					</tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<img src="<?="$image_path/$LAYOUT_SKIN"?>/icon_section.gif" width="20" height="20" align="absmiddle" />
							<span class="sectiontitle_print"><?=$i_Discipline_Session_Info?></span>
						</td>
					</tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="right" background="<?="$image_path/$LAYOUT_SKIN"?>/iPortfolio/scheme/scheme_board_05_off.gif">
							<table width="100%" border="0" cellspacing="5" cellpadding="5" class="tableborder_print_alone">
								<tr>
									<td width="20%" align="left" valign="top"><?=$i_Discipline_Location?></td>
									<td align="left" valign="top" class="row_underline"><?=intranet_htmlspecialchars($TmpLocation)?></td>
								</tr>
								<tr>
									<td align="left" valign="top"><?=$i_Discipline_Duty_Teacher?></td>
									<td align="left" valign="top" class="row_underline"><?=$TmpPIC?></td>
								</tr>
								<tr>
									<td align="left" valign="top"><?=$i_Discipline_Date?></td>
									<td align="left" valign="top" class="row_underline"><?=$TmpDetentionDate?></td>
								</tr>
								<tr>
									<td align="left" valign="top"><?=$i_Discipline_Time?></td>
									<td align="left" valign="top" class="row_underline"><?=$TmpPeriod?></td>
								</tr>
								<tr>
									<td align="left" valign="top"><?=$i_Discipline_Vacancy?></td>
									<td align="left" valign="top" class="row_underline"><?=$TmpVacancy?></td>
								</tr>
								<tr>
									<td align="left" valign="top"><?=$i_Discipline_Students_Attending?></td>
									<td align="left" valign="top" class="row_underline"><?=$TmpAssigned?></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<br />
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<img src="<?="$image_path/$LAYOUT_SKIN"?>/icon_section.gif" width="20" height="20" align="absmiddle" />
							<span class="sectiontitle_print"><?=$i_Discipline_Student_List?></span>
						</td>
					</tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableborder_print">
								<tr class="tabletop_print">
									<td width="15" align="center">#</td>
									<td><?=$i_Discipline_Class?></td>
									<td><?=$i_Discipline_Student?></td>
									<td><?=$i_Discipline_Detention_Reason?></td>
									<td><?=$i_Discipline_Remark?></td>
									<td><?=$i_Discipline_Request_Date?></td>
									<td><?=$i_Discipline_Status?></td>
									<td><?=$i_Discipline_Attendance?></td>
								</tr>
<?=$OutputRow?>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</tbody>
</table>

*/
?>
