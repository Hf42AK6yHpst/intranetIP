<?php
// Modifying by: 

######################################## Change Log #################################################
#
#	Date	:	2017-02-15	Bill	[2016-0808-1743-20240]		($sys_custom['eDiscipline']['BWWTC_AutoCreateDetention'])
#				Display remark layer for homework related detention
#
# 	Date	:	2017-02-14	Bill	[2017-0126-1152-19247]		($sys_custom['eDiscipline']['BWWTC_CalendarWithReason'])
#				Add search box for detention reason searching
#				Add date picker for detention date filter (target date or earlier)
#
#	Date	:	2017-01-18 (Bill) 	[2016-0823-1255-04240]
#				get unassigned student detention created by script when $sys_custom['eDiscipline']['SyncDataFromHomeworkByCronScript'] = true
#
# 	Date	:	2017-01-09	Bill	[2016-0808-1743-20240]
#				Show all records for students ($sys_custom['eDiscipline']['BWWTC_CalendarWithReason'])
# 
#	Date	: 	2010-05-07 YatWoon
#				According to the setting $PIC_SelectionDefaultOwn to check PIC default selected value
#
#	Date 	:	20100118 (Ronald)
#	Details :	Now will use getSessionPeriodByMonth() to get all the detention sessions using in the selected year & month
#
############################################ End ###################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
	
$li = new libdbtable();
$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Detention-View");

$CurrentPage = "Management_Detention";
$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($eDiscipline['Detention_Arrangement'], "", 1);
$TAGS_OBJ[] = array($eDiscipline['Session_Management'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/management/detention/settings/calendar.php", 0);

$SysMsg = $linterface->GET_SYS_MSG("$msg");

if ($cmonth == "") $cmonth = date("Ym");
$CurrentYear = substr($cmonth, 0, 4);
$CurrentMonth = substr($cmonth, 4, 2);
$NoOfDays = date("t", mktime(0,0,0,$CurrentMonth,1,$CurrentYear));
$CurrentDay = date("Y-m-d");
$CurrentTime = date("H:i", time());

if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-New")) {
	$menuBar1 = $linterface->GET_LNK_NEW("new.php?cpage=cal&cmonth=$cmonth", $button_new,"","","",0);
	$menuBar2 = $linterface->GET_LNK_IMPORT("import.php?cpage=cal&cmonth=$cmonth", $button_import,"","","",0);
	$menuBar = "<td>{$menuBar1}<img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif'/></td>";
	$menuBar .= "<td>{$menuBar2}<img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif'/></td>";
}
//$menuBar3 = $linterface->GET_LNK_EXPORT("export.php?cpage=cal&cmonth=$cmonth&targetDate=$cmonth", $button_export);
$menuBar3 = $linterface->GET_LNK_EXPORT("export.php?targetStatus=$targetStatus&targetPeriod=$targetPeriod&targetPIC=$targetPIC&cmonth=$cmonth", $button_export,"","","",0);
$menuBar .= "<td>{$menuBar3}</td>";

// Previous month and next month
if ($CurrentMonth == 1) {
	$PreviousStr = ($CurrentYear-1)."12";
	$NextStr = $CurrentYear.str_pad(($CurrentMonth+1), 2, "0", STR_PAD_LEFT);
}
else if ($CurrentMonth == 12) {
	$PreviousStr = $CurrentYear.($CurrentMonth-1);
	$NextStr = ($CurrentYear+1)."01";
}
else {
	$PreviousStr = $CurrentYear.str_pad(($CurrentMonth-1), 2, "0", STR_PAD_LEFT);
	$NextStr = $CurrentYear.str_pad(($CurrentMonth+1), 2, "0", STR_PAD_LEFT);
}

// Empty box on first week
if (date("w", mktime(0,0,0,$CurrentMonth,1,$CurrentYear)) == 0) {	// sun
	$FirstWeekday = 2;
	$PreEmptyBox = 0;
}
else if (date("w", mktime(0,0,0,$CurrentMonth,1,$CurrentYear)) == 1) {	// mon
	$FirstWeekday = 1;
	$PreEmptyBox = 0;
}
else if (date("w", mktime(0,0,0,$CurrentMonth,1,$CurrentYear)) == 2) {	// tue
	$FirstWeekday = 1;
	$PreEmptyBox = 1;
}
else if (date("w", mktime(0,0,0,$CurrentMonth,1,$CurrentYear)) == 3) {	// wed
	$FirstWeekday = 1;
	$PreEmptyBox = 2;
}
else if (date("w", mktime(0,0,0,$CurrentMonth,1,$CurrentYear)) == 4) {	// thu
	$FirstWeekday = 1;
	$PreEmptyBox = 3;
}
else if (date("w", mktime(0,0,0,$CurrentMonth,1,$CurrentYear)) == 5) {	// fri
	$FirstWeekday = 1;
	$PreEmptyBox = 4;
}
else if (date("w", mktime(0,0,0,$CurrentMonth,1,$CurrentYear)) == 6) {	// sat
	#modify by marcus 2.7.09 Bug Fixed
	//$FirstWeekday = 3;
	//$PreEmptyBox = $ldiscipline->Detention_Sat ? 1 : 0;
	$FirstWeekday = 1;
	$PreEmptyBox = $ldiscipline->Detention_Sat ? 5 : 0;
};

// Empty box on last week
if (date("w", mktime(0,0,0,$CurrentMonth,$NoOfDays,$CurrentYear)) == 0) {	// sun
	$PostEmptyBox = 0;
}
else if (date("w", mktime(0,0,0,$CurrentMonth,$NoOfDays,$CurrentYear)) == 1) {	// mon
	$PostEmptyBox = 4;
}
else if (date("w", mktime(0,0,0,$CurrentMonth,$NoOfDays,$CurrentYear)) == 2) {	// tue
	$PostEmptyBox = 3;
}
else if (date("w", mktime(0,0,0,$CurrentMonth,$NoOfDays,$CurrentYear)) == 3) {	// wed
	$PostEmptyBox = 2;
}
else if (date("w", mktime(0,0,0,$CurrentMonth,$NoOfDays,$CurrentYear)) == 4) {	// thu
	$PostEmptyBox = 1;
}
else if (date("w", mktime(0,0,0,$CurrentMonth,$NoOfDays,$CurrentYear)) == 5) {	// fri
	$PostEmptyBox = 0;
}
else if (date("w", mktime(0,0,0,$CurrentMonth,$NoOfDays,$CurrentYear)) == 6) {	// sat
	$PostEmptyBox = 0;
};

// Get list of available DetentionID and list of full DetentionID
$AvailableDetentionID = $ldiscipline->getDetentionArr("Available");
$FullDetentionID = $ldiscipline->getDetentionArr("Full");

// Get Distinct Period
//$AllPeriod = $ldiscipline->getSessionPeriod();
$AllPeriod = $ldiscipline->getSessionPeriodByMonth($CurrentYear,$CurrentMonth);

// Get Distinct PIC
$AllPIC = $ldiscipline->getSessionPIC();

// Get Distinct Class
$AllClass = $ldiscipline->getClassWithUnassignStudent();

if($ldiscipline->PIC_SelectionDefaultOwn)
{
	if(sizeof($_POST)==0 && sizeof($_GET)==0 && !isset($_POST['targetPIC'])) {		# default display own PIC records
		$flag = 0;
		for($i=0; $i<sizeof($AllPIC); $i++) {
			if($AllPIC[$i][0]==$UserID) {
				$targetPIC = $UserID;		
				$flag = 1;
				break;
			}
		}
	}
}

if (($targetPIC != "All_Duty") && ($targetPIC != "")) {
	// Get list of DetentionID with selected PIC
	$result = $ldiscipline->getDetentionIDByPIC($targetPIC);
	for ($i=0; $i<sizeof($result); $i++) {
		list($PICDetentionID[]) = $result[$i];
	}
}

// Get list of unassigned students
$result = $ldiscipline->getUnassignStudentByClass($targetClass, $searchString, $searchDate);
for ($i=0; $i<sizeof($result); $i++) {
	list($UnassignedStudentID[], $UnassignedStudentCount[]) = $result[$i];
}

$conds = "";
if ($targetStatus == "Available") {
	$conds .= " AND (DetentionDate > '$CurrentDay' OR (DetentionDate = '$CurrentDay' AND StartTime > '$CurrentTime:00'))";
	$conds .= " AND DetentionID IN (".implode(', ', $AvailableDetentionID).")";
}
else if ($targetStatus == "Full") {
	$conds .= " AND (DetentionDate > '$CurrentDay' OR (DetentionDate = '$CurrentDay' AND StartTime > '$CurrentTime:00'))";
	$conds .= " AND DetentionID IN (".implode(', ', $FullDetentionID).")";
}
else if ($targetStatus == "Finished") {
	$conds .= " AND (DetentionDate < '$CurrentDay' OR (DetentionDate = '$CurrentDay' AND StartTime < '$CurrentTime:00'))";
}
if (($targetPeriod != "All_Time") && ($targetPeriod != "")) {
	$conds .= " AND CONCAT(SUBSTRING(StartTime,1,5), '-', SUBSTRING(EndTime,1,5)) = '$targetPeriod'";
}
if (($targetPIC != "All_Duty") && ($targetPIC != "")) {
	$conds .= " AND DetentionID IN (".implode(', ', $PICDetentionID).")";
}

$sql = "SELECT DetentionID, DetentionDate, StartTime, EndTime, Location, Vacancy
				FROM DISCIPLINE_DETENTION_SESSION WHERE MONTH(DetentionDate) = $CurrentMonth AND YEAR(DetentionDate) = $CurrentYear
				$conds
				ORDER BY DetentionDate, StartTime, EndTime, Location";
$result = $ldiscipline->returnArray($sql, 6);
for ($i=0; $i<sizeof($result); $i++) {
	list($DetentionIDRec[], $DetentionDateRec[], $StartTimeRec[], $EndTimeRec[], $LocationRec[], $VacancyRec[]) = $result[$i];
}

if($ldiscipline->MaxDetentionDayBeforeAllow!="" && $ldiscipline->MaxDetentionDayBeforeAllow!=0) {
	$sql = "SELECT DATE_SUB(CURDATE(), INTERVAL ".$ldiscipline->MaxDetentionDayBeforeAllow." DAY) as DateBefore";
	$DateResult = $ldiscipline->returnResultSet($sql);
	$allowDetentionDate = $DateResult[0]['DateBefore'];
}
else {
	$allowDetentionDate = $CurrentDay;
}

// Get the count of assigned students for current month
$result = $ldiscipline->getAssignedStudentCountByYearMonth($CurrentYear, $CurrentMonth);
for ($i=0; $i<sizeof($result); $i++) {
	list($DetentionIDStudentRec[], $StudentIDCountRec[]) = $result[$i];
}

// Get the names of PIC for current month
$result = $ldiscipline->getDetentionPICByYearMonth($CurrentYear, $CurrentMonth);
for ($i=0; $i<sizeof($result); $i++) {
	list($DetentionIDPICRec[], $PICRec[], $NameRec[]) = $result[$i];
}

for ($i=0; $i<sizeof($DetentionIDRec); $i++) {
	$DisplayDetentionID[$DetentionDateRec[$i]][] = $DetentionIDRec[$i];
	$DisplayPeriod[$DetentionDateRec[$i]][] = substr($StartTimeRec[$i],0,5)."-".substr($EndTimeRec[$i],0,5);
	$DisplayLocation[$DetentionDateRec[$i]][] = intranet_htmlspecialchars($LocationRec[$i]);
	$DisplayVacancy[$DetentionDateRec[$i]][] = $VacancyRec[$i];
	$TmpPIC = array();
	for ($j=0; $j<sizeof($DetentionIDPICRec); $j++) {
		if ($DetentionIDRec[$i] == $DetentionIDPICRec[$j]) {
			$TmpPIC[] = $NameRec[$j];
		}
	}
	$DisplayPIC[$DetentionDateRec[$i]][] = implode(", ", $TmpPIC);
	$TmpCount = 0;
	for ($k=0; $k<sizeof($DetentionIDStudentRec); $k++) {
		if ($DetentionIDRec[$i] == $DetentionIDStudentRec[$k]) {
			$TmpCount = $StudentIDCountRec[$k];
		}
	}
	$DisplayAssigned[$DetentionDateRec[$i]][] = $TmpCount;
	$resultAttendanceCount = $ldiscipline->getAttendanceCountByDetentionID($DetentionIDRec[$i], "PRE");
	if ($resultAttendanceCount <> "--" && $resultAttendanceCount < $TmpCount) $resultAttendanceCount = "<span class=\"Warningtitletext\">".$resultAttendanceCount."</span>";
	$DisplayAttended[$DetentionDateRec[$i]][] = $resultAttendanceCount;
}

# filters
$select_status = "<SELECT name=\"targetStatus\" id=\"targetStatus\" onChange=\"this.form.submit()\">\n";
$select_status .= "<OPTION value='All_Status' ".($targetStatus=="All_Status"?"SELECTED":"").">$i_Discipline_All_Status</OPTION>\n";
$select_status .= "<OPTION value='Available' ".($targetStatus=="Available"?"SELECTED":"").">$i_Discipline_Available</OPTION>\n";
$select_status .= "<OPTION value='Full' ".($targetStatus=="Full"?"SELECTED":"").">$i_Discipline_Full</OPTION>\n";
$select_status .= "<OPTION value='Finished' ".($targetStatus=="Finished"?"SELECTED":"").">$i_Discipline_Finished</OPTION>\n";
$select_status .= "</SELECT>\n";

$select_period = "<SELECT name=\"targetPeriod\" id=\"targetPeriod\" onChange=\"this.form.submit()\">\n";
$select_period .= "<OPTION value='All_Time' ".($targetPeriod=="All_Time"?"SELECTED":"").">$i_Discipline_Detention_All_Time_Slot</OPTION>\n";
for ($i=0; $i<sizeof($AllPeriod); $i++) {
	list($TempPeriod) = $AllPeriod[$i];
	$select_period .= "<OPTION value='$TempPeriod' ".($targetPeriod==$TempPeriod?"SELECTED":"").">$TempPeriod</OPTION>\n";
}
$select_period .= "</SELECT>\n";

$select_pic = "<SELECT name=\"targetPIC\" id=\"targetPIC\" onChange=\"this.form.submit()\">\n";
$select_pic .= "<OPTION value='All_Duty' ".($targetPIC=="All_Duty"?"SELECTED":"").">$i_Discipline_Detention_All_Teachers</OPTION>\n";
for ($i=0; $i<sizeof($AllPIC); $i++) {
	list($TempUserID, $TempPIC) = $AllPIC[$i];
	$select_pic .= "<OPTION value='$TempUserID' ".($targetPIC==$TempUserID?"SELECTED":"").">$TempPIC</OPTION>\n";
}
$select_pic .= "</SELECT>\n";

$select_class = "<SELECT name=\"targetClass\" id=\"targetClass\" onChange=\"this.form.submit()\">\n";
$select_class .= "<OPTION value='All_Classes' ".($targetClass=="All_Classes"?"SELECTED":"").">$i_Discipline_Unassign_Student</OPTION>\n";
for ($i=0; $i<sizeof($AllClass); $i++) {
	$ClassName = $AllClass[$i];
	$select_class .= "<OPTION value='$ClassName' ".($targetClass==$ClassName?"SELECTED":"").">$ClassName</OPTION>\n";
}
$select_class .= "</SELECT>\n";

$searchbar = "$select_status $select_period $select_pic";

$remarksImg = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_remark.gif' width=20 height=20 align=absmiddle title=\"".$iDiscipline['Remarks']."\" border=0>";

$table_unassigned_student = "";
if (sizeof($UnassignedStudentID) == 0) {
	$table_unassigned_student .= "<tr class=\"tablerow1\">";
	$table_unassigned_student .= "<td colspan=\"4\" valign=\"top\" align=\"center\"><br />".$i_no_record_exists_msg."</td></tr>";
}
else {
	$rowCount = 0;
	for ($i=0; $i<sizeof($UnassignedStudentID); $i++)
	{
		$resultUnassignedStudent = $ldiscipline->getStudentNameByID($UnassignedStudentID[$i]);
		list($TmpStudentID, $TmpStudentName, $TmpClassName, $TmpClassNumber) = $resultUnassignedStudent[0];

		// Preset unassign student detail (for javascript)
		$TmpLayerContent = "";
		$resultUnassignStudentDetail = $ldiscipline->getUnassignStudentDetailByStudentID($TmpStudentID, $sys_custom['eDiscipline']['SyncDataFromHomeworkByCronScript'], $searchString, $searchDate);
		for ($j=0; $j<sizeof($resultUnassignStudentDetail); $j++) {
			list($TmpUnassignReason, $TmpUnassignRemark, $TmpUnassignPICName, $TmpUnassignStudentCount, $TmpUnassignPICID) = $resultUnassignStudentDetail[$j];
			if ($j == 0) {
				$TmpLayerContent .= "<a href=\"student_record.php?sid=".$TmpStudentID."\" class=\"tablelink\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">".$i_Discipline_View_Student_Detention_Arrangement."</a><br />";
				$TmpLayerContent .= "<table width=\"98%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
				$TmpLayerContent .= "<tr class=\"tabletop\">";
				$TmpLayerContent .= "<td align=\"center\">#</td>";
				$TmpLayerContent .= "<td align=\"left\">".$i_Discipline_Reason."</td>";
				$TmpLayerContent .= "<td align=\"left\">".$i_Discipline_Times."</td>";
				$TmpLayerContent .= "<td align=\"left\">".$i_Discipline_PIC."</td>";
				$TmpLayerContent .= "<td align=\"left\">".$i_Discipline_Remark."</td>";
				$TmpLayerContent .= "</tr>";
			}
			$TmpLayerContent .= "<tr class=\"tablerow1\">";
			$TmpLayerContent .= "<td align=\"center\" class=\"tabletext\">".($j+1)."</td>";
			$TmpLayerContent .= "<td align=\"left\" class=\"tabletext\" nowrap>".intranet_htmlspecialchars($TmpUnassignReason)."</td>";
			$TmpLayerContent .= "<td align=\"left\" class=\"tabletext\" nowrap>".$TmpUnassignStudentCount."</td>";
			$TmpLayerContent .= "<td align=\"left\" class=\"tabletext\" nowrap>".(($TmpUnassignPICID) ? $ldiscipline->getPICNameList($TmpUnassignPICID) : "---")."</td>";
			$TmpLayerContent .= "<td align=\"left\" class=\"tabletext\" nowrap>".str_replace("\r\n","<br>",intranet_htmlspecialchars($TmpUnassignRemark))."</td>";
			$TmpLayerContent .= "</tr>";
			if ($j == sizeof($resultUnassignStudentDetail) - 1) {
				$TmpLayerContent .= "</table>";
			}
		}

		$jUnassignStudentDetailString .= "var jArrayUnassignStudentDetail".$TmpStudentID." = '".str_replace("'", "&#039;", $TmpLayerContent)."';\n";
		
		// [2016-0808-1743-20240] Show all detention (not grouped by student)
		if($sys_custom['eDiscipline']['BWWTC_CalendarWithReason'])
		{
			for ($j=0; $j<sizeof($resultUnassignStudentDetail); $j++)
			{
				list($TmpUnassignReason, $TmpUnassignRemark, $TmpUnassignPICName, $TmpUnassignStudentCount, $TmpUnassignPICID, $TmpUnassignDetentionID) = $resultUnassignStudentDetail[$j];
				
				// [2016-0808-1743-20240] Check if this detention arrangement related to Homework
				$detentionLinkedToHW = false;
				if($sys_custom['eDiscipline']['BWWTC_AutoCreateDetention'] && $TmpUnassignDetentionID) {
					$detentionLinkedToHW = $ldiscipline->isStudentDetentionMappedToHW($TmpUnassignDetentionID);
				}
				
				$table_unassigned_student .= "<tr class=\"tablerow".((($rowCount % 2) == 0)? "1" : "2" )."\">\n";
				$table_unassigned_student .= "<td width=\"12\" valign=\"top\">".($rowCount + 1)."</td>\n";
				$table_unassigned_student .= "<td valign=\"top\">".$linterface->GET_PRESET_LINK("jArrayUnassignStudentDetail".$TmpStudentID, ($i+1), $TmpClassName."-".$TmpClassNumber." ".$TmpStudentName, "tablelink", sizeof($resultUnassignStudentDetail))."</td>\n";
				$table_unassigned_student .= "<td valign=\"top\">";
					$table_unassigned_student .= str_replace("\r\n", "<br>", intranet_htmlspecialchars($TmpUnassignReason));
				
				// [2016-0808-1743-20240] Remark icon to get homework layer
				if($detentionLinkedToHW) {
					$table_unassigned_student .= "&nbsp;";
					$table_unassigned_student .= "<a href='javascript:;' onClick=\"changeClickID({$TmpUnassignDetentionID}); moveObject('show_remark".$TmpUnassignDetentionID."', true); showResult({$TmpUnassignDetentionID},'record_remark'); MM_showHideLayers('show_remark".$TmpUnassignDetentionID."','','show');\">";
						$table_unassigned_student .= $remarksImg;
					$table_unassigned_student .= "</a>";
					$table_unassigned_student .= "<div onload='changeClickID({$TmpUnassignDetentionID}); moveObject(\'show_remark".$TmpUnassignDetentionID."}\', true);' id='show_remark".$TmpUnassignDetentionID."' style='position:absolute; width:320px; height:150px; z-index:-1;'></div>";
					
					$jsRemarkAry .= ($jsRemarkAry != '') ? ",".$TmpUnassignDetentionID : $TmpUnassignDetentionID;
					$displayHWRemark = true;
				}
				
				$table_unassigned_student .= "</td>\n";
				$table_unassigned_student .= "<td><input type=\"checkbox\" name=\"RecordID[]\" value=\"".$TmpUnassignDetentionID."\" /></td></tr>\n";
				$rowCount++;
			}
		}
		else
		{
			$table_unassigned_student .= "<tr class=\"tablerow".((($i%2)==0)?"1":"2")."\">\n";
			$table_unassigned_student .= "<td width=\"12\" valign=\"top\">".($i+1)."</td>\n";
			$table_unassigned_student .= "<td valign=\"top\">".$linterface->GET_PRESET_LINK("jArrayUnassignStudentDetail".$TmpStudentID, ($i+1), $TmpClassName."-".$TmpClassNumber." ".$TmpStudentName, "tablelink", sizeof($resultUnassignStudentDetail))."</td>\n";
			$table_unassigned_student .= "<td valign=\"top\">".$UnassignedStudentCount[$i]."</td>\n";
			$table_unassigned_student .= "<td><input type=\"checkbox\" name=\"RecordID[]\" value=\"".$TmpStudentID."\" /></td></tr>\n";
			if ($UnassignedStudentCount[$i] == 1) {		// Display remark only for one detention left
				$resultRemark = $ldiscipline->getRemarkByStudentID($TmpStudentID);
				$TmpRemark = $resultRemark[0];
				if ($TmpRemark <> "") {
					$table_unassigned_student .= "<tr class=\"tablerow".((($i%2)==0)?"1":"2")."\">\n";
					$table_unassigned_student .= "<td valign=\"top\">&nbsp;</td>\n";
					$table_unassigned_student .= "<td valign=\"top\">".str_replace("\r\n","<br>",intranet_htmlspecialchars($TmpRemark))."</td>\n";
					$table_unassigned_student .= "<td valign=\"top\">&nbsp;</td><td>&nbsp;</td></tr>\n";
				}
			}
		}
	}
}
$linterface->LAYOUT_START();

if($sys_custom['eDiscipline']['BWWTC_CalendarWithReason'] && $sys_custom['eDiscipline']['BWWTC_AutoCreateDetention'] && $displayHWRemark) {
$y = "<script language=javascript>";
$y .= ($jsRemarkAry != '') ? "var jsRemark=[{$jsRemarkAry}];\n" : "var jsRemark=[];\n";
$y .= "</script>";
echo $y;
}
?>

<? if($sys_custom['eDiscipline']['BWWTC_CalendarWithReason'] && $sys_custom['eDiscipline']['BWWTC_AutoCreateDetention'] && $displayHWRemark) { ?>
<script language="javascript">
<!--
function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) 
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}
//-->
</script>

<script language="javascript">
<!--
var xmlHttp
function showResult(str, flag)
{
	xmlHttp = GetXmlHttpObject()
	if (xmlHttp==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
	url = "get_live.php?flag="+flag;
	url = url + "&RecordID=" + str
	url = url + "&sid=" + Math.random()
	
	if(flag=='record_remark') {
		xmlHttp.onreadystatechange = stateChanged
		xmlHttp.open("GET", url, true)
		xmlHttp.send(null)
	}
} 

function stateChanged() 
{
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{
		var id = document.FormCalendar.clickID.value;
		document.getElementById("show_remark"+id).style.zIndex = "1";
		document.getElementById("show_remark"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_remark"+id).style.border = "0px solid #A5ACB2";
	}
}

function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

/*********************************/
function getObject( obj ) {
	if ( document.getElementById ) {
		obj = document.getElementById( obj );
	}
	else if ( document.all ) {
		obj = document.all.item( obj );
	}
	else {
		obj = null;
	}
	return obj;
}

function moveObject( obj, e ) {
	var tempX = 0;
	var tempY = 0;
	var offset = 5;
	
	var objHolder = obj;
	obj = getObject( obj );
	if (obj==null) { return; }
	
	if (document.all) {
		tempX = event.clientX + document.body.scrollLeft;
		tempY = event.clientY + document.body.scrollTop;
	}
	else {
		tempX = e.pageX;
		tempY = e.pageY;
	}
	if (tempX < 0){tempX = 0} else {tempX = tempX - 300}
	if (tempY < 0){tempY = 0} else {tempY = tempY}
	
	obj.style.top  = (tempY + offset) + 'px';
	obj.style.left = (tempX + offset) + 'px';
	displayObject( objHolder, true );
}

function displayObject( obj, show ) {
	obj = getObject( obj );
	if (obj==null) return;
	
	obj.style.display = show ? 'block' : 'none';
	obj.style.visibility = show ? 'visible' : 'hidden';
}

function changeClickID(id) {
	document.FormCalendar.clickID.value = id;
	hideAllOtherLayer();
}

function hideAllOtherLayer() {
	var layer = "";
	if(jsRemark.length != 0) {
		for(i=0; i<jsRemark.length; i++) {
			layer = "show_remark" + jsRemark[i];
			MM_showHideLayers(layer, '', 'hide');	
		}
	}
}
//-->
</script>
<? } ?>

<script language="javascript">
<?=$jUnassignStudentDetailString?>
<?=$jUnassignStudentTitleString?>

function changeMonth(targetMonth){
	obj = document.getElementById('cmonth');
	obj.value = targetMonth;
	document.FormCalendar.submit();
}

function assignCheck(did) {
	if (document.getElementById('vacancy'+did).innerHTML - document.getElementById('assign'+did).innerHTML - countChecked(document.FormCalendar, 'RecordID[]') < 0){
		alert("<?=$i_Discipline_Vacancy_Is_Not_Enough?>");
		return false;
	}
	else {
		if (countChecked(document.FormCalendar, 'RecordID[]') == 0) {
			alert(globalAlertMsg2);
			return false;
		}
		else {
			if(confirm("<?=$i_Discipline_Assign_To_Here_Alert?>")){
				document.getElementById('DetentionID').value = did;
				document.FormCalendar.action="assign.php";
				document.FormCalendar.submit();
			}
			else {
				return false;
			}
		}
	}
}

function arrangeCheck() {
	if (countChecked(document.FormCalendar, 'RecordID[]') == 0) {
		alert(globalAlertMsg2);
		return false;
	}
	else {
		if(confirm("<?=$i_Discipline_Arrange_Alert?>")){
			document.FormCalendar.action="arrange.php";
			document.FormCalendar.submit();
		}
		else {
			return false;
		}
	}
}

function autoArrangeCheck() {
	if (countChecked(document.FormCalendar, 'RecordID[]') == 0) {
		alert(globalAlertMsg2);
		return false;
	}
	else {
		if(confirm("<?=$i_Discipline_Auto_Arrange_Alert?>")){
			document.FormCalendar.action="auto_arrange.php";
			document.FormCalendar.submit();
		}
		else {
			return false;
		}
	}
}

function checkForm(){
	for(var i=1;i<=<?=sizeof($student)?>;i++){
		if (document.getElementById('TextReason'+i).value=='') {
			alert('<?=$i_alert_pleasefillin.$i_Discipline_Reason?>');
			return false;
		}
	}
	
	if(document.getElementById('submit_flag'))
		document.getElementById('submit_flag').value='YES';
	return true;
}
</script>

<br />
<form name="FormCalendar" method="post" onsubmit="return checkForm();" action="calendar.php">
<table width="97%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table border="0" cellspacing="0" cellpadding="2">
				<tr>
					<?=$menuBar?>
				</tr>
			</table>
		</td>
		<td align="right"><?=$SysMsg?>&nbsp;</td>
	</tr>
</table>

<table width="97%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="right">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td height="28" align="right" valign="bottom" class="tabletextremark">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr></tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td height="30" width="75%"><?= $ldiscipline->showWarningMsg($i_Discipline_System_Conduct_Instruction, $i_Discipline_System_Detention_Calentar_Instruction_Msg) ?></td>
					<td align="right" valign="middle" class="thumb_list"><span><?=$i_Discipline_Calendar?></span> | <a href="list.php"><?=$i_Discipline_Session2?></a> | <a href="student_record.php"><?=$i_Discipline_Student?></a></td>
				</tr>
				<tr><td>&nbsp;</td></tr>
			</table>
		</td>
	</tr>
</table>

<table width="97%" cellspacing="0" cellpadding="3" border="0">
	<tr>
		<td width="70%" valign="top">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<td><?= $searchbar ?></td>
							</tr>
						</table>
					</td>
					<td align="right">
						<span class="icalendar_title">
							<a href="javascript:changeMonth('<?=$PreviousStr?>');"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icalendar/icon_prev_off.gif" width="21" height="21" border="0" align="absmiddle" title="<?=$i_Discipline_Previous?>"></a>
<?=date("F", mktime(0,0,0,$CurrentMonth,1,$CurrentYear))?> <?=date("Y", mktime(0,0,0,$CurrentMonth,1,$CurrentYear))?>
							<a href="javascript:changeMonth('<?=$NextStr?>');"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icalendar/icon_next_off.gif" width="21" height="21" border="0" align="absmiddle" title="<?=$i_Discipline_Next?>"></a>
						</span>
					</td>
				</tr>
			</table>

			<table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#d2d2d2">
				<tr>
					<td width="16%" align="center" class="icalendar_weektitle"><?=$i_DayType0[1]?></td>
					<td width="16%" align="center" class="icalendar_weektitle"><?=$i_DayType0[2]?></td>
					<td width="16%" align="center" class="icalendar_weektitle"><?=$i_DayType0[3]?></td>
					<td width="16%" align="center" class="icalendar_weektitle"><?=$i_DayType0[4]?></td>
					<td width="16%" align="center" class="icalendar_weektitle"><?=$i_DayType0[5]?></td>
					<? if($ldiscipline->Detention_Sat) {?>
					<td width="16%" align="center" class="icalendar_weektitle"><?=$i_DayType0[6]?></td>
					<? } ?>
				</tr>
				<tr height="140">
		<? for ($i=0; $i<$PreEmptyBox; $i++) { ?>
					<td valign="top" bgcolor="#FFFFFF">&nbsp;</td>
		<? }
		
		$FirstWeekFlag = true;
		for ($j=$FirstWeekday; $j<=$NoOfDays; $j++)
		{
			if ((date("w", mktime(0,0,0,$CurrentMonth,$j,$CurrentYear)) == 1) && !$FirstWeekFlag) { ?>
					<tr height="140">
		<? 	}

		if($ldiscipline->Detention_Sat)
			$if_condition = (date("w", mktime(0,0,0,$CurrentMonth,$j,$CurrentYear)) != 0);
		else
			$if_condition = (date("w", mktime(0,0,0,$CurrentMonth,$j,$CurrentYear)) != 0) && (date("w", mktime(0,0,0,$CurrentMonth,$j,$CurrentYear)) != 6);
		
		if ($if_condition) { ?>
					<td valign="top" bgcolor="#FFFFFF"<?=($CurrentYear.$CurrentMonth.$j==date("Ymj"))?" style=\"background-color:#FCFFD7\"":""?>><strong<?=($CurrentYear.$CurrentMonth.$j==date("Ymj"))?" style=\"color:#FFFFFF; background-color:#D97200; padding:2px \"":""?>><?=$j?></strong><br>
		<?	
			$TmpDate = $CurrentYear."-".$CurrentMonth."-".str_pad($j, 2, "0", STR_PAD_LEFT);
			$TmpArray = $DisplayPeriod[$TmpDate];
			if (is_array($TmpArray))
			{
				foreach ($TmpArray as $TmpKey => $TmpValue) {
					//if (($TmpDate < $CurrentDay) || (($TmpDate == $CurrentDay) && (substr($DisplayPeriod[$TmpDate][$TmpKey],0,5) < $CurrentTime))) {
					if (($TmpDate < $allowDetentionDate)) {		# can assign to detention session even start time is passed on current date, and allow check the parameter "$ldiscipline->MaxDetentionDayBeforeAllow" 
						$TableClass = "detent_room_past";
						$AssignFlag = false;
						$TakeAttendanceFlag = false;
					}
					else if ($DisplayVacancy[$TmpDate][$TmpKey] <= $DisplayAssigned[$TmpDate][$TmpKey]) {
						$TableClass = "detent_room_full";
						$AssignFlag = false;
						$TakeAttendanceFlag = $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-TakeAttendance");
					}
					else {
						$TableClass = "detent_room_vacancy";
						$AssignFlag = true;
						$TakeAttendanceFlag = $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-TakeAttendance");
					}

					if ($TakeAttendanceFlag) {
						$ImgSrcEclass = $image_path."/".$LAYOUT_SKIN."/ediscipline/icon_eclass.gif";
						$ImgSrcTeacher = $image_path."/".$LAYOUT_SKIN."/ediscipline/icon_teacher.gif";
						$ImgSrcTime = $image_path."/".$LAYOUT_SKIN."/ediscipline/icon_time.gif";
						$ImgSrcVacancy = $image_path."/".$LAYOUT_SKIN."/ediscipline/icon_vancany.gif";
						$ImgSrcAttend = $image_path."/".$LAYOUT_SKIN."/ediscipline/icon_attend.gif";
						$ImgSrcTakeAttend = $image_path."/".$LAYOUT_SKIN."/ediscipline/icon_takeattendance.gif";
					}
					else {
						$ImgSrcEclass = $image_path."/".$LAYOUT_SKIN."/ediscipline/icon_eclass_g.gif";
						$ImgSrcTeacher = $image_path."/".$LAYOUT_SKIN."/ediscipline/icon_teacher_g.gif";
						$ImgSrcTime = $image_path."/".$LAYOUT_SKIN."/ediscipline/icon_time_g.gif";
						$ImgSrcVacancy = $image_path."/".$LAYOUT_SKIN."/ediscipline/icon_vancany_g.gif";
						$ImgSrcAttend = $image_path."/".$LAYOUT_SKIN."/ediscipline/icon_attend_g.gif";
						$ImgSrcTakeAttend = $image_path."/".$LAYOUT_SKIN."/ediscipline/icon_takeattendance_g.gif";
					}
		?>
						<table width="100%" border="0" cellpadding="2" cellspacing="0" class="<?=$TableClass?>">
							<tr>
								<td valign="top" nowrap width="20">
									<img src="<?=$ImgSrcEclass?>" width="20" height="20" border="0" align="absmiddle" title="<?=$i_Discipline_Location?>">
								</td>
								<td nowrap>
									<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tbclip">
										<tr>
											<td nowrap><a href="detail.php?did=<?=$DisplayDetentionID[$TmpDate][$TmpKey]?>" class="tablelink"><?=$DisplayLocation[$TmpDate][$TmpKey]?></a></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap width="20">
									<img src="<?=$ImgSrcTeacher?>" width="20" height="20" border="0" align="absmiddle" title="<?=$i_Discipline_Duty_Teacher?>">
								</td>
								<td nowrap>
									<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tbclip">
										<tr>
											<td nowrap><?=$DisplayPIC[$TmpDate][$TmpKey]?></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap width="20">
									<img src="<?=$ImgSrcTime?>" width="20" height="20" border="0" align="absmiddle" title="<?=$i_Discipline_Time?>">
								</td>
								<td nowrap>
									<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tbclip">
										<tr>
											<td><?=$DisplayPeriod[$TmpDate][$TmpKey]?></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="2" valign="top" nowrap>
									<table width="100%" border="0" cellpadding="1" cellspacing="0">
										<tr>
											<td><img src="<?=$ImgSrcVacancy?>" width="20" height="20" border="0" align="absmiddle" title="<?=$i_Discipline_Vacancy?>"></td>
											<td id="vacancy<?=$DisplayDetentionID[$TmpDate][$TmpKey]?>"><?=$DisplayVacancy[$TmpDate][$TmpKey]?></td>
											<td><img src="<?=$ImgSrcAttend?>" width="20" height="20" border="0" align="absmiddle" title="<?=$i_Discipline_Assigned?>"></td>
											<td id="assign<?=$DisplayDetentionID[$TmpDate][$TmpKey]?>"><?=$DisplayAssigned[$TmpDate][$TmpKey]?></td>
											<td><img src="<?=$ImgSrcTakeAttend?>" width="20" height="20" border="0" align="absmiddle" title="<?=$i_Discipline_Attended?>"></td>
											<td id="attend<?=$DisplayDetentionID[$TmpDate][$TmpKey]?>"><?=$DisplayAttended[$TmpDate][$TmpKey]?></td>
										</tr>
									</table>
								</td>
							</tr>
				<? 	if ($TakeAttendanceFlag) { ?>
							<tr>
								<td valign="top" nowrap>&nbsp;</td>
								<td nowrap><a href="take_attendance.php?did=<?=$DisplayDetentionID[$TmpDate][$TmpKey]?>" class="contenttool"><?=$i_Discipline_Take_Attendance?></a></td>
							</tr>
				<? 	} 
					if ($AssignFlag) { ?>
							<tr>
								<td valign="top" nowrap>&nbsp;</td>
								<td nowrap><a href="#" class="contenttool" onClick="return assignCheck(<?=$DisplayDetentionID[$TmpDate][$TmpKey]?>);"><?=$i_Discipline_Assign_To_Here?></a></td>
							</tr>
				<? 	} ?>
						</table>
		<?		}
			} ?>
					</td>
	<? 	
		}
		if (date("w", mktime(0,0,0,$CurrentMonth,$j,$CurrentYear)) == ($ldiscipline->Detention_Sat ? 6 : 5)) {
			$FirstWeekFlag = false;
	?>
				</tr>
<?		}
	}
	for ($i=0; $i<$PostEmptyBox; $i++) {
?>
					<td valign="top" bgcolor="#FFFFFF">&nbsp;</td>
<?		if ($i==$PostEmptyBox-1) { ?>
				</tr>
<?		}
	} ?>
			</table>
		</td>
		<? if(!$special_feature['eDis_hidden_unassign_list']) { ?>
		<td valign="top" style="border: 1px solid rgb(204, 204, 204);">
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tbody>
					<tr>
						<td valign="top"><span class="indextabon"><?=$i_Discipline_Unassign_Student_List?></span></td>
					</tr>
					<tr>
						<td height="30" align="left">
							<table cellspacing="0" cellpadding="0" border="0" width="100%">
								<tr>
									<td width="50%"><span class="indextabon"><?=$select_class?></span></td>
								<? if($sys_custom['eDiscipline']['BWWTC_CalendarWithReason']) { ?>
									<td width="50%" align="right">
										<?=$linterface->GET_DATE_PICKER("searchDate", $searchDate, "", "yy-mm-dd", "", "", "", "", "", 0, 1);?><br/>
										<input name="searchString" type="text" class="formtextbox" value="<?=stripslashes(stripslashes($searchString))?>" />
										<?=$linterface->GET_BTN($button_search, "submit", "");?>
									</td>
								<? } ?>
								</tr>
							</table>
						</td>
					</tr>
					<tr class="table-action-bar">
						<td align="right">
							<table cellspacing="0" cellpadding="0" border="0">
								<tbody>
									<tr>
										<td width="21"><img width="21" height="23" src="<?="$image_path/$LAYOUT_SKIN"?>/table_tool_01.gif"/></td>
										<td background="<?="$image_path/$LAYOUT_SKIN"?>/table_tool_02.gif">
											<table cellspacing="0" cellpadding="2" border="0">
												<tbody>
													<tr>
														<td><img width="5" src="<?="$image_path/$LAYOUT_SKIN"?>/10x10.gif"/></td>
														<td nowrap="">
															<a class="tabletool" href="#" onClick="javascript:autoArrangeCheck();">
																<img width="12" height="12" border="0" align="absmiddle" src="<?="$image_path/$LAYOUT_SKIN"?>/icon_handle.gif"/><?=$i_Discipline_Auto_Arrange?></a>
														</td>
														<td><img width="5" src="<?="$image_path/$LAYOUT_SKIN"?>/10x10.gif"/></td>
														<td nowrap="">
															<a class="tabletool" href="#" onClick="javascript:arrangeCheck();">
																<img width="12" height="12" border="0" align="absmiddle" src="<?="$image_path/$LAYOUT_SKIN"?>/icon_handle.gif"/><?=$i_Discipline_Arrange?></a>
														</td>
													</tr>
												</tbody>
											</table>
										</td>
										<td width="6"><img width="6" height="23" src="<?="$image_path/$LAYOUT_SKIN"?>/table_tool_03.gif"/></td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
			<table width="100%" cellspacing="0" cellpadding="4" border="0">
				<tbody>
					<tr class="tabletop">
						<td>#</td>
						<td><?=$i_Discipline_Student?></td>
					<? if($sys_custom['eDiscipline']['BWWTC_CalendarWithReason']){ ?>
						<td><?=$eDiscipline['Detention_Reason']?></td>
					<? }
					else { ?>
						<td><?=$i_Discipline_Times_Remain?></td>
					<? } ?>
						<td width="25"><?=$li->check("RecordID[]")?></td>
					</tr>
<?=$table_unassigned_student?>
				</tbody>
			</table>
		</td>
		<? } ?>
	</tr>
</table>

<input type="hidden" id="cmonth" name="cmonth" value="<?=$cmonth?>">
<input type="hidden" id="DetentionID" name="DetentionID">
<input type="hidden" id="isAssignFromCalendar" name="isAssignFromCalendar" value="1">
<? if($sys_custom['eDiscipline']['BWWTC_CalendarWithReason'] && $sys_custom['eDiscipline']['BWWTC_AutoCreateDetention'] && $displayHWRemark) { ?>
<input type="hidden" name="clickID" id="clickID" value="<?=$clickID?>" />
<? } ?>
</form>
<br />

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>