<?php
// Modifying by:

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Detention-New");

$linterface = new interface_html();
$lsp = new libstudentprofile();

# menu highlight setting
$CurrentPage = "Management_Detention";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eDiscipline['Detention_Arrangement'], "", 1);
$TAGS_OBJ[] = array($eDiscipline['Session_Management'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/management/detention/settings/calendar.php", 0);

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$navigationAry[] = array($eDiscipline['Detention']);
$navigationAry[] = array($eDiscipline['Detention_Arrangement'], 'index.php');
$navigationAry[] = array($Lang['Btn']['Import']);
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($navigationAry);

//$FieldsStr = $iDiscipline['Detention_Import_FileDescription'];
$csvFile = "discipline-detention-import-sample.csv";

/*$format_str = $FieldsStr . "<br>".
					"<a class=\"tablelink\" href=\"". GET_CSV($csvFile) ."\" target=\"_blank\">[". $i_general_clickheredownloadsample ."]</a><br>".
					"<br>";

$format_str .= "<br><br>". $iDiscipline['Award_Import_Remarks'] ."<br>";*/
//$format_str .= "<p><span id=\"GM__to\"><strong>".$iDiscipline['Import_Instruct_Main']."</strong></span></p>";
//$format_str .= "<div class=\"p\"><br/><ol type=\"a\"><li>".$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_a_1']."
//				<strong>&lt;<a class=\"tablelink\" href=\"". GET_CSV($csvFile) ."\" target=\"_blank\">".$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_a_2']."</a>&gt;</strong>".
//				$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_a_3']."</li>";
				
//$format_str .= "<li>".$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_b_1']."</span><br/>";
//$format_str .= "<img src=\"".$image_path."/".$LAYOUT_SKIN."/discipline/detention_import_illus_$intranet_session_language.png\"/><br/><div class=\"note\">";
//$format_str .= "<li><span id=\"GM__date\">".$Lang['eDiscipline']['Good_Conduct_and_Misconduct_Import_Instruct_c_1']."</span></li></ul></div></li><br/>";
//$format_str .= "</ol></div>";

$DataColumnTitleArr = array($i_ClassName,$Lang['General']['ClassNumber'], $Lang['General']['Reason'],$eDiscipline['PICTeacher'],$i_Discipline_Remark);
$DataColumnPropertyArr = array(1,1,1,1,0);
$DataColumnRemarksArr = array('','','','<span class="tabletextremark">('.$Lang['SysMgr']['SubjectClassMapping']['ImportCSVField']['6'].')</span>');
//$DataColumnRemarksArr[2] = '<span class="tabletextremark">&nbsp;'.$Lang['eDiscipline']['Import_GM_Remark'][2].'</span>';
$DataColumn = $linterface->Get_Import_Page_Column_Display($DataColumnTitleArr, $DataColumnPropertyArr, $DataColumnRemarksArr);
 $format_str .= $DataColumn;
$linterface->LAYOUT_START();
?>
<br />
<form name="form1" method="post" action="import_result.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
	</tr>
	<tr>
		<td><?=$htmlAry['navigation']?></td>
	</tr>
	<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
			<tr>
				<td>
		  
				<table width="1%" class="form_table_v30">	
							
				<tr>
				 <td valign="top" nowrap="nowrap" class="field_title" ><?=$linterface->RequiredSymbol().$Lang['General']['SourceFile']." <span class='tabletextremark'>".$Lang['General']['CSVFileFormat']."</span>"?>: </td>
			  	 <td class="tabletext"><input class="file" type="file" name="csvfile"></td>
			  	</tr>		
				
				<tr>
				 <td  valign="top" nowrap="nowrap" class="field_title"><?= $Lang['General']['CSVSample'] ?></td>
				 <td valign="top" nowrap="nowrap" colspan='2' class="tabletext">
					<a id="SampleCsvLink" class="tablelink" href="<?=GET_CSV($csvFile)?>" target="_blank">
					<img src='<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/icon_files/xls.gif' border='0' align='absmiddle' hspace='3'> 
					<?=$i_general_clickheredownloadsample?></td>   
				</tr>
				
				<tr>
				 <td width="35%" valign="top" nowrap="nowrap" class="field_title" ><?= $i_general_Format ?></td>					
		  		 <td class="tabletext"><?=$format_str?></td>
		  		</tr>  
		  		
		      </table>
		      
			 </td> 
			</tr>  
		
	 

			 <tr>
				 <td class="tabletext"><?= $linterface->MandatoryField();?> </td>
			</tr>
			<tr>
				<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			</tr>
		</table>
	 </td>
	</tr>
	
	<tr>
		<td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_continue, "submit") ?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='index.php'") ?>
		</td>
	</tr>
</table>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
