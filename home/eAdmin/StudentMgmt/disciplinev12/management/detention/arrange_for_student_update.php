<?php
// Using : 

#################### Change Log #####################
##
##	Date    :	20190513 (Bill)
##	Details	:	Prevent SQL Injection
##
##	Date	:	20190326 (Bill)		[2019-0322-1117-36066]
##	Details	:	Fixed: overlapped sessions checking
##
##	Date	:	20171013 (Bill)		[2017-1013-1512-05066]
##	Details	:	Fixed: cannot assign students even detention session not full
##
##	Date 	: 	20100915 (Henry Chow)
##	Details :	Modified reassign of detention
##
#####################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

## Get Current Year ##
$CurrAcademicYearID = Get_Current_Academic_Year_ID();

### Handle SQL Injection + XSS [START]
$arrRecordID = explode(",", IntegerSafe($TargetRecordID));
$arrTargetStudnetID = explode(",", IntegerSafe($TargetStudentID));
### Handle SQL Injection + XSS [END]

$ldiscipline->Start_Trans();

if($ldiscipline->MaxDetentionDayBeforeAllow!="" && $ldiscipline->MaxDetentionDayBeforeAllow!=0) {
	$allowedDetentionDate = " (DATE_SUB(CURDATE(), INTERVAL ".$ldiscipline->MaxDetentionDayBeforeAllow." DAY))";
	$conds .= " AND (DetentionDate >= $allowedDetentionDate)";
}
else {
	$conds .= " AND (DetentionDate > CURDATE() OR (DetentionDate = CURDATE()))";
}

for($i=0; $i<sizeof($arrTargetStudnetID); $i++)
{
	$SelectedStudentID = $arrTargetStudnetID[$i];
	
	## Get All Possible Avaliable Detention Session for selected student
	$sql = "SELECT 
					a.DetentionID, 
					a.DetentionDate, 
					a.StartTime, 
					a.EndTime, 
					a.Location,
					a.Vacancy,
					e.RecordID
			FROM 
					DISCIPLINE_DETENTION_SESSION AS a INNER JOIN 
					DISCIPLINE_DETENTION_SESSION_CLASSLEVEL AS b ON (a.DetentionID = b.DetentionID) INNER JOIN
					YEAR_CLASS AS c ON (b.ClassLevelID = c.YearID) INNER JOIN
					YEAR_CLASS_USER AS d ON (c.YearClassID = d.YearClassID) LEFT OUTER JOIN
					DISCIPLINE_DETENTION_STUDENT_SESSION AS e ON (a.DetentionID = e.DetentionID AND e.StudentID = '$SelectedStudentID')
			WHERE 
					(e.RecordID IS NULL OR e.RecordStatus = 0) AND
					c.AcademicYearID = '$CurrAcademicYearID' AND 
					d.UserID = '$SelectedStudentID'
					$conds
			ORDER BY
					a.DetentionDate, a.StartTime ASC";	
/*
	$sql = "SELECT 
					a.DetentionID, 
					a.DetentionDate, 
					a.StartTime, 
					a.EndTime, 
					a.Location,
					a.Vacancy,
					e.RecordID
			FROM 
					DISCIPLINE_DETENTION_SESSION AS a INNER JOIN 
					DISCIPLINE_DETENTION_SESSION_CLASSLEVEL AS b ON (a.DetentionID = b.DetentionID) INNER JOIN
					YEAR_CLASS AS c ON (b.ClassLevelID = c.YearID) INNER JOIN
					YEAR_CLASS_USER AS d ON (c.YearClassID = d.YearClassID) LEFT OUTER JOIN
					DISCIPLINE_DETENTION_STUDENT_SESSION AS e ON (a.DetentionID = e.DetentionID AND e.StudentID = $SelectedStudentID)
			WHERE 
					e.RecordID IS NULL AND 
					e.RecordStatus IS NULL AND 
					c.AcademicYearID = $CurrAcademicYearID AND 
					d.UserID = $SelectedStudentID AND 
					(a.DetentionDate > CURDATE() OR (a.DetentionDate = CURDATE() AND a.StartTime > CURTIME()))
			ORDER BY
					a.DetentionDate, a.StartTime ASC";
*/
	
	$arr_avaliable_session = $ldiscipline->returnArray($sql,7);
	if(sizeof($arr_avaliable_session) > 0)
	{
		for($a=0; $a<sizeof($arr_avaliable_session); $a++)
		{
			list($detention_id, $detention_date, $detention_start, $detention_end, $detention_location, $space_avaliable, $detention_record_id) = $arr_avaliable_session[$a];
			$AvaliableSessionID[] = $detention_id;
			
			$AvaliableSessionArray[$detention_id]['Date'] = $detention_date;
			$AvaliableSessionArray[$detention_id]['StartTime'] = $detention_start;
			$AvaliableSessionArray[$detention_id]['EndTime'] = $detention_end;
			$AvaliableSessionArray[$detention_id]['StartTimestamp'] = strtotime($detention_date." ".$detention_start);
			$AvaliableSessionArray[$detention_id]['EndTimestamp'] = strtotime($detention_date." ".$detention_end);
			$AvaliableSessionArray[$detention_id]['Location'] = $detention_location;
			$AvaliableSessionArray[$detention_id]['Vacancy'] = $space_avaliable;
		}
	}
	
	## Get Assigned Detention Session of selected student
	$sql = "SELECT 
					a.DetentionID,
					a.DetentionDate, 
					a.StartTime, 
					a.EndTime, 
					a.Location 
			FROM 
					DISCIPLINE_DETENTION_SESSION AS a INNER JOIN 
					DISCIPLINE_DETENTION_STUDENT_SESSION AS b ON (a.DetentionID = b.DetentionID) 
			WHERE 
					b.RecordStatus = 1 AND 
					b.StudentID = '$SelectedStudentID' AND 
					(a.DetentionDate > CURDATE() OR (a.DetentionDate = CURDATE() AND a.StartTime > CURTIME()))";
	$arr_assigned_session = $ldiscipline->returnArray($sql,5);
	
	$AssignedSessionID = array();
	if(sizeof($arr_assigned_session) > 0)
	{
		for($a=0; $a<sizeof($arr_assigned_session); $a++)
		{
			list($detention_id, $detention_date, $detention_start, $detention_end, $detention_location) = $arr_assigned_session[$a];
			$AssignedSessionID[] = $detention_id;
			
			$AssignedSessionArray[$detention_id]['Date'] = $detention_date;
			$AssignedSessionArray[$detention_id]['StartTime'] = $detention_start;
			$AssignedSessionArray[$detention_id]['EndTime'] = $detention_end;
			$AssignedSessionArray[$detention_id]['StartTimestamp'] = strtotime($detention_date." ".$detention_start);
			$AssignedSessionArray[$detention_id]['EndTimestamp'] = strtotime($detention_date." ".$detention_end);
			$AssignedSessionArray[$detention_id]['Location'] = $detention_location;
		}
	}
    
	## Final Avaliable Session for auto assign
	$arr_duplicate_session = array();
	$FinalAvaliableSessionArray = array_diff($AvaliableSessionID, $arr_duplicate_session);
	$tempFinalAvaliableSessionArray = $FinalAvaliableSessionArray;
	
	$duplicate_array = array();
	$AutoAssignedSessionArray[$SelectedStudentID][] = array();
	for($j=1; $j<=${"NumOfRecord_$SelectedStudentID"}; $j++)
	{
		$is_absent_record = ${"AbsentRecord_".$SelectedStudentID."_$j"};
		$recordID =  ${"RecordBelongTo_".$SelectedStudentID."_$j"};
		
		$SelectedSession = ${"SelectDetentionSession_".$SelectedStudentID."_$j"};
		if(!in_array($SelectedSession, $AutoAssignedSessionArray[$SelectedStudentID]))
		{
			$TargetStudentArray[] = $SelectedStudentID;
			$TargetStudentRecordID[$SelectedStudentID][] = $recordID;
			
			$AutoAssignedSessionArray[$SelectedStudentID][] = $SelectedSession;
			for($k=1; $k<sizeof($AutoAssignedSessionArray[$SelectedStudentID]); $k++)
			{
				if($AvaliableSessionArray[$AutoAssignedSessionArray[$SelectedStudentID][$k]]['Date'] == $AvaliableSessionArray[$SelectedSession]['Date'])
				{
					if($AutoAssignedSessionArray[$SelectedStudentID][$k] != $SelectedSession)
					{
						if(($AvaliableSessionArray[$AutoAssignedSessionArray[$SelectedStudentID][$k]]['StartTimestamp'] <= $AvaliableSessionArray[$SelectedSession]['StartTimestamp']) && ($AvaliableSessionArray[$SelectedSession]['StartTimestamp'] < $AvaliableSessionArray[$AutoAssignedSessionArray[$SelectedStudentID][$k]]['EndTimestamp']))
						{
							$arr_duplicate_session[$SelectedStudentID][] = $SelectedSession;
							break;
						}
						else if(($AvaliableSessionArray[$SelectedSession]['StartTimestamp'] <= $AvaliableSessionArray[$AutoAssignedSessionArray[$SelectedStudentID][$k]]['StartTimestamp']) && ($AvaliableSessionArray[$AutoAssignedSessionArray[$SelectedStudentID][$k]]['StartTimestamp'] < $AvaliableSessionArray[$SelectedSession]['EndTimestamp']))
						{
							$arr_duplicate_session[$SelectedStudentID][] = $SelectedSession;
							break;
						}
						else
						{
							// Continue
						}
					}
					else
					{
						$FinalAssignedSessionArray[$SelectedStudentID][$recordID] = $SelectedSession;
					}
				}
				else
				{
					$FinalAssignedSessionArray[$SelectedStudentID][$recordID] = $SelectedSession;
				}
			}
		}
		else
		{
			$arr_duplicate_session[$SelectedStudentID][] = $SelectedSession;
		}
	}
	
	if(sizeof($arr_duplicate_session[$SelectedStudentID]) > 0)
	{
		$AutoAssignedSessionArray[$SelectedStudentID] = array_diff($AutoAssignedSessionArray[$SelectedStudentID], $arr_duplicate_session[$SelectedStudentID]);
	}
	
	for($j=1; $j<sizeof($AutoAssignedSessionArray[$SelectedStudentID]); $j++)
	{
		$SelectedSession = $AutoAssignedSessionArray[$SelectedStudentID][$j];
		$NoOfSpaceNeed[$SelectedSession]++;
	}
}

$new_id = "";
for($i=0; $i<sizeof($arrTargetStudnetID); $i++)
{
	$SelectedStudentID = $arrTargetStudnetID[$i];
	for($j=0; $j<sizeof($TargetStudentRecordID[$SelectedStudentID]); $j++)
	{
		$record_id = $TargetStudentRecordID[$SelectedStudentID][$j];
		$SelectedSession = $FinalAssignedSessionArray[$SelectedStudentID][$record_id];
		
		# Get Session used Vacancy
		$sql = "SELECT COUNT(*) FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE DetentionID = '$SelectedSession' AND RecordStatus = 1";
		$UsedVacancy = $ldiscipline->returnVector($sql);
		
		if($record_id != "")
		{
			if(($AvaliableSessionArray[$SelectedSession]['Vacancy']-$NoOfSpaceNeed[$SelectedSession]-$UsedVacancy[0]) >= 0)
			{
				$sql = "SELECT AttendanceStatus, RecordStatus FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE RecordID = '$record_id'";
				$AbsentRecord = $ldiscipline->returnArray($sql);
				list($attendanceStatus, $recordstatus) = $AbsentRecord[0];
				if($recordstatus==1 && $attendanceStatus=='ABS')
				{
					$sql = "INSERT INTO DISCIPLINE_DETENTION_STUDENT_SESSION (StudentID, DetentionID, DemeritID, Reason, Remark, RequestedBy, ArrangedBy, RecordStatus, DateInput, DateModified, CreatedBy, RelatedTo) 
							SELECT StudentID, '$SelectedSession', DemeritID, Reason, Remark, RequestedBy, ".$_SESSION['UserID'].", 1, NOW(), NOW(), ".$_SESSION['UserID'].", RecordID FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE RecordStatus = 1 AND RecordID = '$record_id'";
					$ldiscipline->db_db_query($sql);
					$new_detention_id = $ldiscipline->db_insert_id();
					$new_id .= ($new_id!="") ? ",$new_detention_id" : $new_detention_id;
					
					$sql = "UPDATE DISCIPLINE_DETENTION_STUDENT_SESSION SET RelatedTo = '$new_detention_id', DateModified = NOW(), RecordStatus = 2 WHERE StudentID = '$SelectedStudentID' AND RecordID = '$record_id'";
					$result[] = $ldiscipline->db_db_query($sql);
					$cnt++;
				}
				else
				{
					$sql = "SELECT RecordID FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE StudentID = '$SelectedStudentID' AND DetentionID = '$SelectedSession' AND RecordStatus = 0 AND AttendanceStatus IS NULL";
					$temp = $ldiscipline->returnVector($sql);
					if(count($temp) > 0)
					{
						$old_record_id = $temp[0];
						
						# Remove old one
						$sql = "DELETE FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE RecordID = '$old_record_id'";
						$ldiscipline->db_db_query($sql);
						
						# Update new one
						$sql = "UPDATE DISCIPLINE_DETENTION_STUDENT_SESSION SET DetentionID = '$SelectedSession', ArrangedBy = '".$_SESSION['UserID']."', ArrangedDate = NOW(), DateModified = NOW() WHERE StudentID = '$SelectedStudentID' AND RecordStatus = 1 AND RecordID = '$record_id'";
						$result[] = $ldiscipline->db_db_query($sql);
					}
					else
					{
						$sql = "UPDATE DISCIPLINE_DETENTION_STUDENT_SESSION SET DetentionID = '$SelectedSession', ArrangedBy = ".$_SESSION['UserID'].", ArrangedDate = NOW(), DateModified = NOW() WHERE StudentID = '$SelectedStudentID' AND DetentionID IS NULL AND RecordStatus = 1 AND RecordID = '$record_id'";
						$result[] = $ldiscipline->db_db_query($sql);
					}
					
					$cnt++;
					$new_id .= ($new_id!="") ? ",$record_id" : $record_id;
				}
				
				// [2017-1013-1512-05066] Update no. of space need for current session
				$NoOfSpaceNeed[$SelectedSession]--;
			}
		}
	}
}

if($cnt == sizeof($arrRecordID))
{
	$ldiscipline->Commit_Trans();
	if($from == 'calendar') {
		//header("location: calendar.php?msg=update");
		header("location: arrange2.php?msg=update&rid=$TargetRecordID&from=calendar&new_detention_id=$new_id");
	}
	else{
		//header("location: student_record.php?msg=update");
		header("location: arrange2.php?msg=update&rid=$TargetRecordID&from=&new_detention_id=$new_id");
	}
	exit();
}
else
{
	$ldiscipline->RollBack_Trans();
	if($from == 'calendar') {
		header("location: calendar.php?msg=update_failed");
	}
	else{
		header("location: student_record.php?msg=update_failed");
	}
	exit();
}

intranet_closedb();
?>