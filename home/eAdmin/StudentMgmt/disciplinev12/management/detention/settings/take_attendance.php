<?php
// Modifying by:

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
	
$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Detention-TakeAttendance");

$CurrentPage = "Management_Detention";

$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($eDiscipline['Detention_Arrangement'], "../", 0);
$TAGS_OBJ[] = array($eDiscipline['Session_Management'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/management/detention/settings/calendar.php", 1);

$PAGE_NAVIGATION[] = array($i_Discipline_Take_Attendance, "");

$SysMsg = $linterface->GET_SYS_MSG("$msg");


if (!isset($field)) $field = 0;		// default: sort by class
if (!isset($order)) $order = 1;		// default: asc order

$pageSizeChangeEnabled = true;

if ($page_size_change == 1) {
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
	$ck_page_size = $numPerPage;
}
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;

$li = new libdbtable2007($field, $order, $pageNo);

$name_field = getNameFieldByLang("USR.");
$clsName = ($intranet_session_language=="en") ? "yc.ClassTitleEN" : "yc.ClassTitleB5";

$yearID = Get_Current_Academic_Year_ID();

/*
$sql = "SELECT CONCAT(USR.ClassName, '-', USR.ClassNumber) AS Class, $name_field AS Student, SES.Reason, IF(SES.AttendanceStatus='PRE', CONCAT('<span id=\"span', SES.StudentID, '\"><img src=\"$image_path/$LAYOUT_SKIN/smartcard/icon_present.gif\" width=\"20\" height=\"20\" title=\"$i_Discipline_Attended\"></span> <select name=\"select', SES.StudentID, '\" onChange=\"javascript:changeStatus(', SES.StudentID, ');\"><option value=\"\"></option><option value=\"PRE\" selected>$i_Discipline_Present</option><option value=\"ABS\">$i_Discipline_Absent</option></select>'), IF(SES.AttendanceStatus='ABS', CONCAT('<span id=\"span', SES.StudentID, '\"><img src=\"$image_path/$LAYOUT_SKIN/smartcard/icon_absent.gif\" width=\"20\" height=\"20\" title=\"$i_Discipline_Absent\"></span> <select name=\"select', SES.StudentID, '\" onChange=\"javascript:changeStatus(', SES.StudentID, ');\"><option value=\"\"></option><option value=\"PRE\">$i_Discipline_Present</option><option value=\"ABS\" selected>$i_Discipline_Absent</option></select>'), CONCAT('<span id=\"span', SES.StudentID, '\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"></span> <select name=\"select', SES.StudentID, '\" onChange=\"javascript:changeStatus(', SES.StudentID, ');\"><option value=\"\" selected></option><option value=\"PRE\">$i_Discipline_Present</option><option value=\"ABS\">$i_Discipline_Absent</option></select>'))) AS AttendanceStatus, CONCAT('<input name=\"textAttendanceRemark', SES.StudentID, '\" type=\"text\" class=\"textbox\" style=\"background:#ffffff\" value=\"', REPLACE(IFNULL(SES.AttendanceRemark, ''), '\"', '&quot;') , '\">') AS AttendanceRemark, SES.StudentID
				FROM DISCIPLINE_DETENTION_STUDENT_SESSION SES, INTRANET_USER USR
				WHERE SES.StudentID = USR.UserID
				AND SES.DetentionID = '$did'";
*/

$sql = "SELECT CONCAT($clsName, '-', ycu.ClassNumber) AS Class, 
			$name_field AS Student, 
			SES.Reason, 
			IF(SES.AttendanceStatus='PRE', CONCAT('<span id=\"span', SES.StudentID, '\"><img src=\"$image_path/$LAYOUT_SKIN/smartcard/icon_present.gif\" width=\"20\" height=\"20\" title=\"$i_Discipline_Attended\"></span> <select name=\"select', SES.StudentID, '\" id=\"select',SES.StudentID,'\" onChange=\"javascript:changeStatus(', SES.StudentID, ');\"><option value=\"\"></option><option value=\"PRE\" selected>$i_Discipline_Present</option><option value=\"ABS\">$i_Discipline_Absent</option></select>'), IF(SES.AttendanceStatus='ABS', CONCAT('<span id=\"span', SES.StudentID, '\"><img src=\"$image_path/$LAYOUT_SKIN/smartcard/icon_absent.gif\" width=\"20\" height=\"20\" title=\"$i_Discipline_Absent\"></span> <select name=\"select', SES.StudentID, '\" id=\"select',SES.StudentID,'\" onChange=\"javascript:changeStatus(', SES.StudentID, ');\"><option value=\"\"></option><option value=\"PRE\">$i_Discipline_Present</option><option value=\"ABS\" selected>$i_Discipline_Absent</option></select>'), CONCAT('<span id=\"span', SES.StudentID, '\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"></span> <select name=\"select', SES.StudentID, '\" id=\"select',SES.StudentID,'\" onChange=\"javascript:changeStatus(', SES.StudentID, ');\"><option value=\"\" selected></option><option value=\"PRE\">$i_Discipline_Present</option><option value=\"ABS\">$i_Discipline_Absent</option></select>'))) AS AttendanceStatus, CONCAT('<input name=\"textAttendanceRemark', SES.StudentID, '\" id=\"textAttendanceRemark',SES.StudentID,'\" type=\"text\" class=\"textbox\" style=\"background:#ffffff\" value=\"', REPLACE(IFNULL(SES.AttendanceRemark, ''), '\"', '&quot;') , '\">') AS AttendanceRemark, SES.StudentID 
				FROM DISCIPLINE_DETENTION_STUDENT_SESSION SES 
				LEFT OUTER JOIN INTRANET_USER USR ON (SES.StudentID = USR.UserID)
				LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID)
				LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) 
				WHERE SES.DetentionID = '$did' AND yc.AcademicYearID=$yearID";

$li->field_array = array("Class", "Student", "Reason", "AttendanceStatus", "AttendanceRemark");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = $i_Discipline_Records;
$li->column_array = array(14,14,14,14,14);
$li->IsColOff = "eDisciplineDetentionTakeAttendance";
//$li->AllFormsData = $i_Discipline_All_Forms;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width='2%' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='10%' nowrap>".$li->column($pos++, $i_Discipline_Class)."</td>\n";
$li->column_list .= "<td width='25%'>".$li->column($pos++, $i_Discipline_Student)."</td>\n";
$li->column_list .= "<td width='28%'>".$li->column($pos++, $i_Discipline_Detention_Reason)."</td>\n";
$li->column_list .= "<td width='20%' nowrap>".$li->column($pos++, $i_Discipline_Status)."</td>\n";
$li->column_list .= "<td width='15%' nowrap>".$li->column($pos++, $i_Discipline_Remark)."</td>\n";

$linterface->LAYOUT_START();


$TempList = $ldiscipline->getDetentionListByDetentionID($did);
list($TmpDetentionID,$TmpDetentionDate,$TmpDetentionDayName,$TmpStartTime,$TmpEndTime,$TmpPeriod,$TmpLocation,$TmpForm,$TmpPIC,$TmpVacancy,$TmpAssigned,$TmpAvailable) = $TempList[0];
if ($TmpForm == $TempAllForms) $TmpForm = $this->AllFormsData;
//$SessionString = substr($TmpDetentionDate,2)." (".substr($TmpDetentionDayName,0,3).") | ".$TmpPeriod." | ".intranet_htmlspecialchars($TmpLocation)." | ".$TmpForm." | ".$TmpPIC." | ".$TmpAssigned;
$SessionString =$ldiscipline->displayDetentionSessionString($did);
$TempList = $ldiscipline->getDetentionLastUpdatedByDetentionID($did);
$TmpAttendanceTakenBy = $TempList[0];
$TmpAttendanceTakenDate = $TempList[1];
if ($TmpAttendanceTakenBy <> "" && $TmpAttendanceTakenDate <> "") {
	$TempList = $ldiscipline->getUserNameByID($TmpAttendanceTakenBy);
	$TmpStudentName = $TempList[0];
	$LastUpdatedString = $TmpAttendanceTakenDate." (".$TmpStudentName.")";
}

$TempList = $ldiscipline->getStudentIDByDetentionID($did);
for($i=0;$i<sizeof($TempList);$i++) {
	$StudentID[] = $TempList[$i];
}
?>

<script language="javascript">
	// Grab reasons
	function getReasons(pos) {
		var jArrayTemp = new Array();
		var obj2 = document.getElementById("select"+pos);
		if (obj2.selectedIndex == 2) {
			return jArrayWordsAbsence;
		} else {
			return jArrayTemp;
		}
	}

	function changeStatus(studentID) {
		var newStatus = document.getElementById("select"+studentID).value;
		if (newStatus == "PRE") {
			document.getElementById("span"+studentID).innerHTML = "<img src=\"<?="$image_path/$LAYOUT_SKIN"?>/smartcard/icon_present.gif\" width=\"20\" height=\"20\" title=\"<?=$i_Discipline_Attended?>\">";
			document.getElementById("tr"+studentID).className = "row_approved";
			document.getElementById("textAttendanceRemark"+studentID).disabled = true;
			document.getElementById("textAttendanceRemark"+studentID).value = "";
			document.getElementById("textAttendanceRemark"+studentID).style.background= "#dfdfdf";
			document.getElementById("poslink"+studentID).style.display = "none";
		}	else if (newStatus == "ABS"){
			document.getElementById("span"+studentID).innerHTML = "<img src=\"<?="$image_path/$LAYOUT_SKIN"?>/smartcard/icon_absent.gif\" width=\"20\" height=\"20\" title=\"<?=$i_Discipline_Absent?>\">";
			document.getElementById("tr"+studentID).className = "row_suspend";
			document.getElementById("textAttendanceRemark"+studentID).disabled = false;
			document.getElementById("textAttendanceRemark"+studentID).style.background= "#ffffff";
			document.getElementById("poslink"+studentID).style.display = "inline";
		}	else {
			document.getElementById("span"+studentID).innerHTML = "<img src=\"<?="$image_path/$LAYOUT_SKIN"?>/10x10.gif\" width=\"20\" height=\"20\" align=\"absmiddle\">";
			document.getElementById("tr"+studentID).className = "row_waiting";
			document.getElementById("textAttendanceRemark"+studentID).disabled = true;
			document.getElementById("textAttendanceRemark"+studentID).value = "";
			document.getElementById("textAttendanceRemark"+studentID).style.background= "#dfdfdf";
			document.getElementById("poslink"+studentID).style.display = "none";
		}
	}

	function absToPre() {
		var obj = document.form1;
		var len = obj.elements.length;
		var studentID;
		for(var i=0; i<len; i++) {
			if (obj.elements[i].name.substr(0,6)=="select") {
				studentID = obj.elements[i].name.substr(6)
				objSelect = obj.elements[i];
				if (objSelect.selectedIndex != 1) {
					objSelect.selectedIndex = 1;
				}
				changeStatus(studentID);
			}
		}
	}

	function updateStyle() {
		var obj = document.form1;
		var len = obj.elements.length;
		var studentID;
		for(var i=0; i<len; i++) {
			if (obj.elements[i].name.substr(0,6)=="select") {
				studentID = obj.elements[i].name.substr(6)
				changeStatus(studentID);
			}
		}
	}

	function checkForm(){
		var obj = document.form1;
		var len = obj.elements.length;
		for(var i=0; i<len; i++) {
			if (obj.elements[i].name.substr(0,6)=="select") {
				objSelect = obj.elements[i];
				if (objSelect.selectedIndex == 0) {
					alert('<?=$i_alert_pleaseselect.$i_Discipline_Status?>');
					return false;
				}
			}
		}
		if(confirm("<?=$i_Discipline_Take_Attendance_Alert?>")){
			var obj = document.form1;
			obj.action = "take_update.php";
			obj.method = "post";
			obj.submit();
		} else {
			return false;
		}
	}
</script>

<br />
<form name="form1" method="post">
<table width="97%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="navigation"><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?></td>
	</tr>
</table>
<table width="97%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="right">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right"><?=$SysMsg?>&nbsp;</td>
				</tr>
				<tr>
					<td height="28" align="right" valign="bottom">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left">&nbsp;</td>
							</tr>
						</table>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><?=$i_Discipline_Session?>: <?=$SessionString?>
								</td>
								<td align="right" valign="bottom">
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="21">
												<img src="<?="$image_path/$LAYOUT_SKIN"?>/table_tool_01.gif" width="21" height="23"></td>
											<td background="<?="$image_path/$LAYOUT_SKIN"?>/table_tool_02.gif">
												<table border="0" cellspacing="0" cellpadding="2">
													<tr>
														<!--<td nowrap>
															<a href="#" class="tabletool">
																<img src="<?="$image_path/$LAYOUT_SKIN"?>/icon_photo.gif" width="12" height="12" border="0" align="absmiddle"><?=$i_Discipline_Display_Photos?></a></td>
														<td><img src="<?="$image_path/$LAYOUT_SKIN"?>/10x10.gif" width="5"></td>-->
														<td nowrap>
															<a href="#" class="tabletool" onClick="javascript:absToPre();">
																<img src="<?="$image_path/$LAYOUT_SKIN"?>/icon_approve.gif" width="12" height="12" border="0" align="absmiddle"><?=$i_Discipline_Set_Absent_To_Present?></a></td>
													</tr>
												</table>
											</td>
											<td width="6"><img src="<?="$image_path/$LAYOUT_SKIN"?>/table_tool_03.gif" width="6" height="23"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
<?=$li->display();?>
		</td>
	</tr>
	<tr>
		<td height="1" class="dotline">
			<img src="<?="$image_path/$LAYOUT_SKIN"?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td align="right">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="left" class="tabletextremark"><?=$i_Discipline_Last_Updated?>: <?=$LastUpdatedString?></td>
				</tr>
				<tr>
					<td height="50" align="center" class="tabletextremark">
<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "return checkForm()", "btnSubmit")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='calendar.php'")?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" name="did" id="did" value="<?=$did?>" />
<input type="hidden" name="studentid" id="studentid" value="<?=implode(",", $StudentID)?>" />
<input type="hidden" name="field" id="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="order" id="order" value="<?php echo $li->order; ?>"/>
<input type="hidden" name="pageNo" id="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
</form>
<br />
<script language="javascript">
	updateStyle();
</script>
<?
print $linterface->FOCUS_ON_LOAD("form1.btnSubmit");
$linterface->LAYOUT_STOP();
?>
