<?php
// Modifying by: 

#############################
#
#	Date:	2017-07-10	Bill
#			- General deploy Repeat by Weekdays function
#			- Exclude Holidays ($sys_custom['eDiscipline']['DetentionAllowRepeatWeekdays'] = true)
#
#	Date:	2015-04-20	Bill
#			add detention type field and repeat by weekdays function [2014-1216-1347-28164 - Detention customization]
#
#	Date:	2013-10-25	YatWoon
#			Add back $cmonth variable for back to previous calender month [Case#2013-1016-1535-14073]
#
#############################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$ldiscipline->CONTROL_ACCESS("Discipline-SETTINGS-Detention-Access");

$CurrentPage = "Management_Detention";
$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$linterface = new interface_html();

$lclass = new libclass();
$ClassLvlArr = $lclass->getLevelArray();
$lteaching = new libteaching();

$TAGS_OBJ[] = array($eDiscipline['Detention_Arrangement'], "../", 0);
$TAGS_OBJ[] = array($eDiscipline['Session_Management'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/management/detention/settings/calendar.php", 1);

$PAGE_NAVIGATION[] = array($i_Discipline_Session_List, "list.php");
$PAGE_NAVIGATION[] = array($i_Discipline_New_Sessions, "");

if ($cpage == "cal") {
	$CancelBtnScript = "window.location='calendar.php?cmonth=$cmonth'";
	$ActionPage = "new_update.php?cpage=cal";
}
else if ($cpage == "list") {
	$CancelBtnScript = "window.location='list.php'";
	$ActionPage = "new_update.php?cpage=list";
}

//$SchoolYear = getCurrentAcademicYear();
$yearID = Get_Current_Academic_Year_ID();
$SchoolYear = $ldiscipline->getAcademicYearNameByYearID($yearID);

$AcademicStart = date("Y-m-d");
$AcademicEnd   = date("Y-m-d", getEndOfAcademicYear('', $yearID));

// [2017-07-10] after General Deploy Repeat by Weekdays function, exclude holidays for Creative Secondary School ($sys_custom['eDiscipline']['DetentionAllowRepeatWeekdays'] = true)
// [2014-1216-1347-28164] get public and school holiday list
//$acyear = " and EventDate >= '".substr(getStartDateOfAcademicYear($yearID),0,10)."' and EventDate <= '".substr(getEndDateOfAcademicYear($yearID),0,10)."'";
$holidayList = array();
if($sys_custom['eDiscipline']['DetentionAllowRepeatWeekdays']) {
	$acyear = " AND EventDate >= '$AcademicStart' AND EventDate <= '$AcademicEnd'";
	$sql = "SELECT EventDate FROM INTRANET_EVENT WHERE RecordStatus = '1' AND RecordType IN ('3', '4') $acyear ORDER BY EventDate";
	$holidayList = $ldiscipline->returnVector($sql);
}

/*
$semester_mode = getSemesterMode();
if($semester_mode==1)	# Manual Update
{
	$select_sem = getSelectSemester("name=semester", $semester);	
	$select_sem_html = "
		<tr>
			<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\">
				<span class=\"tabletext\">{$i_SettingsSemester} <span class=\"tabletextrequire\">*</span></span>
			</td>
			<td valign=\"top\">{$select_sem}</td>
		</tr>
	";
}
else
{
	$select_sem_html = "<input type='hidden' name='semester' value=''>";
}
*/
$select_sem_html = "<input type='hidden' name='semester' value=''>";

for ($i=0; $i<=23; $i++) {
	if ($i < 10) {
		$HourArray[] = array("0".$i,"0".$i);
	}
	else {
		$HourArray[] = array($i,$i);
	}
}
for ($i=0; $i<=55; $i+=5) {
	if ($i < 10) {
		$MinArray[] = array("0".$i,"0".$i);
	}
	else {
		$MinArray[] = array($i,$i);
	}
}

$StartHour = getSelectByArray($HourArray,"name='StartHour'","",0,1,"",2);
$StartMin = getSelectByArray($MinArray,"name='StartMin'","",0,1,"",2);
$EndHour = getSelectByArray($HourArray,"name='EndHour'","",0,1,"",2);
$EndMin = getSelectByArray($MinArray,"name='EndMin'","",0,1,"",2);

// [2014-1216-1347-28164] show repeated by weekdays td
$repeat_td = "";
$repeat_td .= '<td style="padding:5px; background-color:#dbedff" valign="top" height="50%">';
$repeat_td .= '<div style="background-color:#f7f7f7">'."\n";
$repeat_td .= '<table border=0 width="520">'."\n";
$repeat_td .= '<col width=110>'."\n";
$repeat_td .= '<tr>'."\n";
$repeat_td .= '<td>'.$Lang['General']['StartDate'].'</td>'."\n";
$repeat_td .= '<td>'."\n";
$repeat_td .= $linterface->Get_Date_Picker("StartDate", $StartDate)."\n";
$repeat_td .= '<span class="tabletextremark">('.str_replace('<!--targetDate-->', $AcademicStart, $Lang['eDiscipline']['WarningMsgArr']['StartDateInvalid']).')</span>';
$repeat_td .= $linterface->Get_Form_Warning_Msg('invalidDateRangeWarningDiv', $Lang['eDiscipline']['WarningMsgArr']['DateRangeInvalid'], 'warningDiv');
$repeat_td .= $linterface->Get_Form_Warning_Msg('invalidStartDateWarningDiv', str_replace('<!--targetDate-->', $AcademicStart, $Lang['eDiscipline']['WarningMsgArr']['StartDateInvalid']), 'warningDiv');
$repeat_td .= '</td>'."\n";
$repeat_td .= '</tr>'."\n";
$repeat_td .= '<tr>'."\n";
$repeat_td .= '<td>'.$Lang['General']['EndDate'].'</td>'."\n";
$repeat_td .= '<td>'."\n";
$repeat_td .= $linterface->Get_Date_Picker("EndDate", $EndDate)."\n";
$repeat_td .= '<span class="tabletextremark">('.str_replace('<!--targetDate-->', $AcademicEnd, $Lang['eDiscipline']['WarningMsgArr']['EndDateInvalid']).')</span>';
$repeat_td .= $linterface->Get_Form_Warning_Msg('invalidEndDateWarningDiv', str_replace('<!--targetDate-->', $AcademicEnd, $Lang['eDiscipline']['WarningMsgArr']['EndDateInvalid']), 'warningDiv');
$repeat_td .= '</td>'."\n";
$repeat_td .= '</tr>'."\n";
$repeat_td .= '<tr valign="top">'."\n";
$repeat_td .= '<td>'.$Lang['eDiscipline']['OnEvery'].'</td>'."\n";
$repeat_td .= '<td>'."\n";
$repeat_td .= '<div>'."\n";
for($i=1 ;$i<6; $i++) {
	$repeat_td .= $linterface->Get_Checkbox("WeekDay$i", "SelectedWeekDay[]", $i, false, 'weekdayChk', '', $Onclick='', $Disabled='');
	$repeat_td .= $Lang['eDiscipline']['RepeatDatePeriod']['ShortWeekday'][$i]."&nbsp;";
}
$repeat_td .= '</div>'."\n";
$repeat_td .= $linterface->Get_Form_Warning_Msg('selectWeekdayWarningDiv', $Lang['eDiscipline']['WarningMsgArr']['SelectWeekday'], 'warningDiv');						
$repeat_td .= '</td>'."\n";
$repeat_td .= '</tr>'."\n";
$repeat_td .= '<tr valign="top">'."\n";
$repeat_td .= '<td>&nbsp;</td>'."\n";
$repeat_td .= '<td>'."\n";
$repeat_td .= '<div>'."\n";
$repeat_td .= $linterface->GET_SMALL_BTN($Lang['Btn']['Add'], 'button', 'addPeriodicDate();');
$repeat_td .= '</div>'."\n";
$repeat_td .= '</td>'."\n";
$repeat_td .= '</tr>'."\n";
$repeat_td .= '</table>'."\n";
$repeat_td .= '</div>'."\n";
$repeat_td .= '</td>';

// [2014-1216-1347-28164] session type selection
if($sys_custom['eDiscipline']['CreativeSchDetentionCust']){
	$sessiontype_select = "
	<select id='sessiontype' name='sessiontype'>
		<option value='0'> -- $button_select -- </option>
		<option value='1'>".$Lang['eDiscipline']['DetentionTypes']['LunchTime']."</option>
		<option value='2'>".$Lang['eDiscipline']['DetentionTypes']['AfterSchool']."</option>
		<option value='3'>".$Lang['eDiscipline']['DetentionTypes']['Suspension']."</option>
	</SELECT>";
}

$ClassLvlChk = "<input type=\"checkbox\" id=\"classlvlall\" name=\"classlvl\" onClick=\"(this.checked) ? setChecked(1, document.FormNew, 'classlvl[]') : setChecked(0, document.FormNew, 'classlvl[]');\"><label for=\"classlvlall\">".$i_Discipline_All_Forms."</label> ";
for ($i = 0; $i < sizeof($ClassLvlArr); $i++) {
	$checked = "";
	$ClassLvlChk .= "<input type=\"checkbox\" $checked id=\"classlvl{$i}\" name=\"classlvl[]\" onClick=\"if(!this.checked){document.FormNew.classlvlall.checked=false;}\" value=\"".$ClassLvlArr[$i][0]."\"><label for=\"classlvl{$i}\">".$ClassLvlArr[$i][1]."</label> ";
}

for($i = 0; $i < 40; $i++) $space .= "&nbsp;";

$linterface->LAYOUT_START();
?>

<script type="text/javascript">
	var holiday = new Array();
	<?php for($holCount=0; $holCount<count($holidayList); $holCount++){
		echo "holiday[$holCount] = '".substr($holidayList[$holCount], 0, 10)."';";
	}
	?>
	
	function formatTableDate() {
		var targetTable = "TableDate";
		if(document.getElementById('repeatWeekdays1').checked){
			targetTable = "TableDate2";
		}
		
		if(document.getElementById(targetTable).rows.length>1) {
			for(i=1; i<=document.getElementById(targetTable).rows.length-1; i++) {
				var x=document.getElementById(targetTable).rows[i].cells;
				x[0].innerHTML=i;
				x[2].innerHTML='<a href="javascript:deleteDate('+i+')"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_delete_b.gif" border="0" align="absmiddle" alt="<?=$i_Discipline_Delete?>" /></a>';
				var y=document.getElementById(targetTable).rows[i];
				if(i%2==1) {
					y.className='tablerow1';
				}
				else {
					y.className='tablerow2';
				}
			}
		}
	}

	function checkWeedend(parDate) {
		var tmpYear = parDate.substr(0,4);
		var tmpMonth = parDate.substr(5,2);
		var tmpDay = parDate.substr(8,2);
		
		var tmpDate = new Date();
		tmpDate.setYear(tmpYear);
		tmpDate.setMonth(tmpMonth-1,tmpDay);
		
		<? if($ldiscipline->Detention_Sat) { ?>
			if((tmpDate.getDay()==0)) {
		<? }
		else { ?>
			if((tmpDate.getDay()==0)||(tmpDate.getDay()==6)) {
		<? } ?>
			alert('<?=$i_alert_pleaseselect.$i_Discipline_Weekday?>');
			return false;
		}
		else {
			return true;
		}
	}

	function checkPastDate(parDate) {
		var tmpYear = parDate.substr(0,4);
		var tmpMonth = parDate.substr(5,2);
		var tmpDay = parDate.substr(8,2);

		var today=new Date();
		var year=today.getFullYear();
		var month=today.getMonth()+1;
		if (month<10) month="0"+month;
		var day=today.getDate();
		if (day<10) day="0"+day;

		if(tmpYear+''+tmpMonth+''+tmpDay<year+''+month+''+day) {
			alert('<?=$Lang['eDiscipline']['DateCannotInPast']?>');
			return false;
		}
		else {
			return true;
		};
	}

	function insertDate() {
		var targetTable = "TableDate";
		if(document.getElementById('repeatWeekdays1').checked){
			targetTable = "TableDate2";
		}
		
		if((checkWeedend(document.getElementById('SelectedDate').value))&&(checkPastDate(document.getElementById('SelectedDate').value))){
			var x=document.getElementById(targetTable).insertRow(document.getElementById(targetTable).rows.length);
			var a=x.insertCell(0);
			var b=x.insertCell(1);
			var c=x.insertCell(2);
			b.innerHTML=document.getElementById('SelectedDate').value;
			formatTableDate();
		}
	}

	function deleteDate(deleteRowIndex) {
		var targetTable = "TableDate";
		if(document.getElementById('repeatWeekdays1').checked){
			targetTable = "TableDate2";
		}
		
		document.getElementById(targetTable).deleteRow(deleteRowIndex);
		formatTableDate();
	}

	function deleteAllDate() {
		if(document.getElementById('TableDate').rows.length>1) {
			for(i=document.getElementById('TableDate').rows.length-1;i>=1;i--) {
				document.getElementById('TableDate').deleteRow(i);
			}
		}
		if(document.getElementById('TableDate2').rows.length>1) {
			for(i=document.getElementById('TableDate2').rows.length-1; i>=1; i--) {
				document.getElementById('TableDate2').deleteRow(i);
			}
		}
		if(document.getElementById('repeatWeekdays1').checked){
			document.getElementById('repeatWeekdays0').checked = true;
			document.getElementById('RepeatDaysTable').style.display='none';
			document.getElementById('SingleSelectTable').style.display = '';
		}
	}

	function deleteAllTeacher() {
		obj = document.getElementById('SelectTeacher[]');
		checkOptionClear(obj);
		checkOptionAdd(obj, "<? for($i = 0; $i < 40; $i++) echo " "; ?>", "");
	}
	
	function getDateTextByDateObject(dateObj) {
		return dateObj.getFullYear() + '-' + str_pad((dateObj.getMonth() + 1), 2) + '-' + str_pad(dateObj.getDate(), 2);;
	}
	
	function addPeriodicDate(){
		var startDate = $('input#StartDate').val();
		var endDate = $('input#EndDate').val();
		var canAddDate = true;
		
		// check if the date input is valid
		if(startDate=='' || endDate==''){
			canAddDate = false;
			return;
		}
		
		// check if the date range is valid
		if (startDate > endDate) {
			$('div#invalidDateRangeWarningDiv').show();
			canAddDate = false;
			return;
		}
		else {
			$('div#invalidDateRangeWarningDiv').hide();
		}
		
		// check if the date is within valid period
		if (startDate < '<?=$AcademicStart?>') {
			$('div#invalidStartDateWarningDiv').show();
			canAddDate = false;
			return;
		}
		else {
			$('div#invalidStartDateWarningDiv').hide();
		}
		if (endDate > '<?=$AcademicEnd?>') {
			$('div#invalidEndDateWarningDiv').show();
			canAddDate = false;
			return;
		}
		else {
			$('div#invalidEndDateWarningDiv').hide();
		}
		
		// check if selected any weekday
		if ($('input.weekdayChk:checked').length==0) {
			$('div#selectWeekdayWarningDiv').show();
			canAddDate = false;
			return;
		}
		else {
			$('div#selectWeekdayWarningDiv').hide();
		}
		
		if (canAddDate) {
			var startDatePiece = startDate.split('-');
			var startYear = startDatePiece[0];
			var startMonth = startDatePiece[1] - 1;
			var startDay = startDatePiece[2];
			
			// get date which fulfill the requirements
			var dateObj = new Date(startYear, startMonth, startDay);
			var currentDate = getDateTextByDateObject(dateObj);
			var targetDateAry = new Array();
			while (currentDate <= endDate) {
				// add date if current date is not public or school holiday
				if(jQuery.inArray(currentDate, holiday) === -1){
					var _day = dateObj.getDay();
					if ($('input#WeekDay' + _day).attr('checked')) {
						targetDateAry.push(getDateTextByDateObject(dateObj));
					}
				}
				dateObj.setDate(dateObj.getDate() + 1);
				currentDate = getDateTextByDateObject(dateObj);
			}
			
			// Add the dates to the table
			var numOfDate = targetDateAry.length;
			var i;
			// display alert message
			if(numOfDate == 0){
				alert("<?=$Lang['eDiscipline']['WarningMsgArr']['NoMatchDays']?>");
			}
			for (i=0; i<numOfDate; i++) {
				var _dateText = targetDateAry[i];
				addDateRowToTable(_dateText);
			}
		}
	}

	function addDateRowToTable(dateText) {
		var x=document.getElementById('TableDate2').insertRow(document.getElementById('TableDate2').rows.length);
		var a=x.insertCell(0);
		var b=x.insertCell(1);
		var c=x.insertCell(2);
		b.innerHTML=dateText;
		formatTableDate();
	}
	
	function checkForm() {
		/*
		if (document.getElementById('TableDate').rows.length<=1) {
			alert('<?=$i_alert_pleaseselect.$i_Discipline_Date?>');
			return false;
		} else if ((document.getElementById('StartHour').value+''+document.getElementById('StartMin').value)>=(document.getElementById('EndHour').value+''+document.getElementById('EndMin').value)) {
			alert('<?=$i_Discipline_StartEnd_Time_Alert?>');
			return false;
		} else if (document.getElementById('TextLocation').value=='') {
			alert('<?=$i_alert_pleasefillin.$i_Discipline_Location?>');
			return false;
		} else if ((document.getElementById('TextVacancy').value+' '!=Math.round(document.getElementById('TextVacancy').value)+' ') || (!(document.getElementById('TextVacancy').value>0))) {
			alert('<?=$i_Discipline_Invalid_Vacancy?>');
			return false;
		}
		*/
		
		var f = document.FormNew;
		var targetTable = "TableDate";
		if(document.getElementById('repeatWeekdays1').checked){
			targetTable = "TableDate2";
		}
		
		if (document.getElementById(targetTable).rows.length<=1) {
			alert('<?=$i_alert_pleaseselect.$i_Discipline_Date?>');
			return false;
		} 
		if ((f.StartHour.value+''+f.StartMin.value)>=(f.EndHour.value+''+f.EndMin.value)) {
			alert('<?=$i_Discipline_StartEnd_Time_Alert?>');
			return false;
		} 
		
		<?php if($sys_custom['eDiscipline']['CreativeSchDetentionCust']){ ?>
			if(document.getElementById('sessiontype').value == 0){
				alert('<?=$Lang['eDiscipline']['WarningMsgArr']['SelectDetentionTypes']?>');
				return false;
			}
		<?php } ?>
		
		if (f.TextLocation.value=='') {
			alert('<?=$i_alert_pleasefillin.$i_Discipline_Location?>');
			return false;
		} 
		if ((f.TextVacancy.value+' '!=Math.round(f.TextVacancy.value)+' ') || (!(f.TextVacancy.value>0))) {
			alert('<?=$i_Discipline_Invalid_Vacancy?>');
			return false;
		}
		
		var CheckLvl = 0;
		for (i=0; i< <?=sizeof($ClassLvlArr)?>; i++)
		{
			var classLevelChkBox = document.getElementById('classlvl'+i);
			if (classLevelChkBox.checked)
			{
				CheckLvl = 1;
			}
		}

		if (CheckLvl==0) {
			alert('<?=$i_alert_pleaseselect.$i_Discipline_Applicable_Form?>');
			return false;
		}

		obj = document.getElementById('SelectTeacher[]');
		checkOption(obj);
		if (obj.length==0) {
			checkOptionAdd(obj, "<? for($i = 0; $i < 40; $i++) echo " "; ?>", "");
			alert("<?=$i_alert_pleaseselect.$i_Discipline_Duty_Teacher?>");
			return false;
		}
		checkOptionAll(obj);

		var TmpAllSelectedDate = new Array();
		var x;
		for(i=1; i<=document.getElementById(targetTable).rows.length-1; i++) {
			x=document.getElementById(targetTable).rows[i].cells;
			TmpAllSelectedDate[i-1] = x[1].innerHTML;
		}
		document.getElementById('AllSelectedDate').value = TmpAllSelectedDate.join();
		return true;
	}
</script>

<br />
<form name="FormNew" method="POST" action="<?=$ActionPage?>">

<table width="98%" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td class="navigation"><?= $linterface->GET_NAVIGATION4($PAGE_NAVIGATION); ?></td>
	</tr>
</table>

<table width="86%" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap class="formfieldtitle"><?=$i_Discipline_School_Year?></td>
					<td valign="top"><?=$SchoolYear?></td>
				</tr>
				<?=$select_sem_html?>
				
				<tr valign="top">
					<td valign="top" nowrap class="formfieldtitle"><span class="tabletext"> <?=$Lang['eDiscipline']['RepeatDatePeriod']['Title']?> </span></td>
					<td>
						<input type="radio" id="repeatWeekdays0" name="repeatWeekdays" value="0" onclick="document.getElementById('RepeatDaysTable').style.display='none'; document.getElementById('SingleSelectTable').style.display = '';" checked><label for="repeatWeekdays0"><?=$Lang['General']['No']?></label>
						<input type="radio" id="repeatWeekdays1" name="repeatWeekdays" value="1" onclick=";document.getElementById('SingleSelectTable').style.display='none'; document.getElementById('RepeatDaysTable').style.display = '';"><label for="repeatWeekdays1"><?=$Lang['General']['Yes']?></label>
					</td>
				</tr>
				
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Discipline_Date?> <span class="tabletextrequire">*</span></span>
					</td>
					<td>
						<table width="30%" border="0" cellpadding="4" cellspacing="0" id="SingleSelectTable">
							<tr>
								<td valign="top" style="border:1px solid #CCCCCC">
									<table id="TableDate" width="100%" border="0" cellspacing="0" cellpadding="4">
										<tr class="tabletop">
											<td>#</td>
											<td nowrap><?=$i_Discipline_Date_Selected?></td>
											<td>&nbsp;</td>
										</tr>
									</table>
								</td>
								<td valign="top"><?= $linterface->GET_CALENDAR("FormNew", "SelectedDate", "insertDate();", false)?></td>
							</tr>
						</table>
						<table border="0" cellpadding="4" cellspacing="0" id="RepeatDaysTable" style="display:none">
							<tr><?= $repeat_td ?></tr>
							<tr><td valign="top" style="border:1px solid #CCCCCC">
									<table id="TableDate2" width="100%" border="0" cellspacing="0" cellpadding="4">
										<tr class="tabletop">
											<td>#</td>
											<td nowrap><?=$i_Discipline_Date_Selected?></td>
											<td>&nbsp;</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<br />
					</td>
				</tr>

				<tr valign="top">
					<td valign="top" nowrap class="formfieldtitle"><span class="tabletext"><?=$i_Discipline_Time?> <span class="tabletextrequire">*</span></span></td>
					<td><?=$i_From?> <?=$StartHour?> : <?=$StartMin?> <?=$i_To?> <?=$EndHour?> : <?=$EndMin?></td>
				</tr>
				
				<?php if($sys_custom['eDiscipline']['CreativeSchDetentionCust']){ ?>
					<tr valign='top'>
						<td valign="top" nowrap class="formfieldtitle"> <?=$Lang['eDiscipline']['DetentionTypes']['Title']?> <span class="tabletextrequire">*</span></td>
						<td><?=$sessiontype_select?></td>
					</tr>
				<?php } ?>

				<tr>
					<td valign="top" nowrap class="formfieldtitle"><?=$i_Discipline_Applicable_Form?> <span class="tabletextrequire">*</span></td>
					<td valign="top"><?=$ClassLvlChk?></td>
				</tr>
				<tr>
					<td valign="top" nowrap class="formfieldtitle"><?=$i_Discipline_Location?> <span class="tabletextrequire">*</span></td>
					<td valign="top"><input name="TextLocation" class="textboxtext" maxLength="255"></td>
				</tr>
				<tr>
					<td valign="top" nowrap class="formfieldtitle"><?=$i_Discipline_Duty_Teacher?> <span class="tabletextrequire">*</span></td>
					<td>
						<table>
							<tr>
								<td>
									<select name="SelectTeacher[]" id="SelectTeacher[]" size=6 multiple></select>
								</td>
								<td valign="bottom">
									<?=$linterface->GET_BTN($i_Discipline_Select, "button","newWindow('../../choose_pic.php?fieldname=SelectTeacher[]',9)");?><br />
									<?=$linterface->GET_BTN($i_Discipline_Remove, "button","checkOptionRemove(document.FormNew.elements['SelectTeacher[]'])");?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap class="formfieldtitle"><?=$i_Discipline_Vacancy?> <span class="tabletextrequire">*</span></td>
					<td valign="top"><input name="TextVacancy" class="textboxnum" maxLength="4"></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td align="left" class="tabletextremark"><?=$i_general_required_field?></td>
				</tr>
				<tr>
					<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="center">
						<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "return checkForm();")?>&nbsp;
						<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "deleteAllDate();deleteAllTeacher();")?>&nbsp;
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "$CancelBtnScript")?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" id="SelectedDate" name="SelectedDate">
<input type="hidden" id="AllSelectedDate" name="AllSelectedDate">
<input type="hidden" id="cmonth" name="cmonth" value="<?=$cmonth?>">
</form>
<br />

<?
print $linterface->FOCUS_ON_LOAD("FormNew.StartHour");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>