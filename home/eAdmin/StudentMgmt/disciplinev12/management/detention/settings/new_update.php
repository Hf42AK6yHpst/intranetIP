<?php
// Modifying by: 

#############################
#
#	Date:	2015-04-20	Bill
#			insert detention session type [2014-1216-1347-28164 - Detention customization]
#
#	Date:	2013-10-25	YatWoon
#			Add back $cmonth variable for back to previous calender month [Case#2013-1016-1535-14073]
#			
#############################


$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$ldiscipline->CONTROL_ACCESS("Discipline-SETTINGS-Detention-Access");

$DetentionDate = explode(",", $_POST["AllSelectedDate"]);
// [2014-1216-1347-28164] fixed: import unique detention date
$DetentionDate = array_unique((array)$DetentionDate);
$DetentionDateStr = implode("','", $DetentionDate);
$SchoolYear = getCurrentAcademicYear();
$semester_mode = getSemesterMode();
//if($semester_mode==2) $semester = retrieveSemester($DetentionDate);	
$StartTime = $_POST["StartHour"].":".$_POST["StartMin"].":00";
$EndTime = $_POST["EndHour"].":".$_POST["EndMin"].":00";
$Location = trim($_POST["TextLocation"]);
$Vacancy = $_POST["TextVacancy"];
$ClassLevelID = $_POST["classlvl"];
$PICID = $_POST["SelectTeacher"];
// 2014-1216-1347-28164 - Detention Session Type
if($sys_custom['eDiscipline']['CreativeSchDetentionCust']){
	$type = $_POST["sessiontype"];
}

$SuccessFlag = true;
$AvailableFlag = true;
$FutureFlag = true;
$PICAvailable = true;

for ($j=0; $j<sizeof($DetentionDate); $j++) {
	//if($semester_mode==2) $semester = retrieveSemester($DetentionDate[$j]);	
	$semester = retrieveSemester($DetentionDate[$j]);	
	
	// Check for past session
	$Today = date("Y-m-d");
	$NowTime = date("H:i:s");
	if ($Today > $DetentionDate[$j] || ($Today == $DetentionDate[$j] && $NowTime > $StartTime)) {
		$FutureFlag = false;
	} else {

		// Check for overlapping of date, time and location
		$sql = "SELECT DetentionDate, StartTime, EndTime, Location FROM DISCIPLINE_DETENTION_SESSION WHERE DetentionDate IN ('$DetentionDateStr') ORDER BY DetentionDate DESC, StartTime, EndTime, Location";
		$result = $ldiscipline->returnArray($sql, 4);
		$AllAvailableFlag = true;
		$PassChecking = false;

		if (sizeof($result) == 0) {
			$PassChecking = true;
		} else {
			for ($i=0; $i<sizeof($result); $i++) {
				list($ExistDetentionDate, $ExistStartTime, $ExistEndTime, $ExistLocation) = $result[$i];

				if ($ExistDetentionDate == $DetentionDate[$j] && strtoupper(trim($ExistLocation)) == strtoupper(trim($Location)) && (($StartTime >= $ExistStartTime && $StartTime < $ExistEndTime) || ($EndTime > $ExistStartTime && $EndTime <= $ExistEndTime))) {
					$AvailableFlag = false;
					$AllAvailableFlag = false;
					break;
				} else {
					if (($i == sizeof($result) - 1) && ($AllAvailableFlag)) {
						$PassChecking = true;
					}
				}
			}
		}
		if($PassChecking==true) {
			# check for overlapping of PIC in same time-slot
			$sql = "SELECT COUNT(*) FROM DISCIPLINE_DETENTION_SESSION_PIC SES_PIC LEFT OUTER JOIN DISCIPLINE_DETENTION_SESSION SES ON (SES_PIC.DetentionID=SES.DetentionID) WHERE SES.DetentionDate='".$DetentionDate[$j]."' AND (('$StartTime'>SES.StartTime AND '$StartTime'<SES.EndTime) OR ('$EndTime'>SES.StartTime AND '$EndTime'<SES.EndTime)) AND SES_PIC.UserID IN (".implode(',',$PICID).")";
			
			$result = $ldiscipline->returnVector($sql);
			if($result[0]!=0) {
				$PassChecking = false;	
				$PICAvailable = false;
			}
		}
		
		if ($PassChecking) {
			// Passed
			$DetentionArr = array();
			$DetentionArr['Year'] = "'$SchoolYear'";
			$DetentionArr['Semester'] = "'$semester'";
			$DetentionArr['DetentionDate'] = "'$DetentionDate[$j]'";
			$DetentionArr['StartTime'] = "'$StartTime'";
			$DetentionArr['EndTime'] = "'$EndTime'";
			$DetentionArr['Location'] = "'$Location'";
			$DetentionArr['Vacancy'] = "'$Vacancy'";
			// 2014-1216-1347-28164 - Detention Session Type
			if($sys_custom['eDiscipline']['CreativeSchDetentionCust']){
				$DetentionArr['SessionType'] = "'$type'";
			}
			$disciplineRecordID = $ldiscipline->insertDetentionSession($DetentionArr);
			if ($disciplineRecordID=="") $SuccessFlag = false;


			for ($k=0; $k<sizeof($ClassLevelID); $k++) {
				$DetentionArr = array();
				$DetentionArr['DetentionID'] = $disciplineRecordID;
				$DetentionArr['ClassLevelID'] = $ClassLevelID[$k];
				$disciplineRecordID1 = $ldiscipline->insertDetentionClassLevel($DetentionArr);
				if ($disciplineRecordID1=="") $SuccessFlag = false;
			}


			$PICIDUnique = array();
			for ($l=0; $l<sizeof($PICID); $l++) {
				if (!in_array($PICID[$l], $PICIDUnique)) {
					$PICIDUnique[] = $PICID[$l];
				}
			}
			for ($m=0; $m<sizeof($PICIDUnique); $m++) {
				$DetentionArr = array();
				$DetentionArr['DetentionID'] = $disciplineRecordID;
				$DetentionArr['UserID'] = $PICIDUnique[$m];
				$disciplineRecordID2 = $ldiscipline->insertDetentionPIC($DetentionArr);
				if ($disciplineRecordID2=="") $SuccessFlag = false;
			}
		}
	}
}


if (!($AvailableFlag && $FutureFlag)) {
	$SysMsg = "add_past_overlapped";
} else if(!$PICAvailable) {
	$SysMsg2 = 11;
} else if ($SuccessFlag) {
	$SysMsg = "add";
} else {
	$SysMsg = "add_failed";
}


if ($cpage == "cal") {
	$ReturnPage = "calendar.php?msg=$SysMsg&msg2=$SysMsg2&cmonth=$cmonth";
} else if ($cpage == "list") {
	$ReturnPage = "list.php?msg=$SysMsg&msg2=$SysMsg2";
}

intranet_closedb();
header("Location: $ReturnPage");
?>
