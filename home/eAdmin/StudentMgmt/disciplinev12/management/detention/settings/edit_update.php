<?php
// Modifying by: 

############ Change Log Start #############
#
#	Date	:	2015-04-20	Bill
#				update detention session type [2014-1216-1347-28164 - Detention customization]
#
#	Date	:	2013-10-25 (Yatwoon)
#				Add back $cmonth variable for back to previous calender month [Case#2013-1016-1535-14073]
#
#	Date	:	2011-04-14 (Henry Chow)
#	Detail 	:	allow to edit today's detention session even start time is passed (but cannot edit the Time)
#
############ Change Log End #############

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$ldiscipline->CONTROL_ACCESS("Discipline-SETTINGS-Detention-Access");

$DetentionID = $_POST["DetentionID"];
$DetentionDate = $_POST["DetentionDate"];
/*
$semester_mode = getSemesterMode();
if($semester_mode==2) $semester = retrieveSemester($DetentionDate);
*/
$semester = retrieveSemester($DetentionDate);



$SuccessFlag = true;
$AvailableFlag = true;
$FutureFlag = true;
$AssignedFormExistFlag = true;
$AssignedStudentCountFlag = true;

// Check for Vacancy.  Must be larger or equal to the assigned student count
$AssignedStudentCount = $ldiscipline->getAssignedStudentCountByDetentionID($DetentionID);

if($AssignedStudentCount[0] == '0' && $disabled=="")
{
	$StartTime = $_POST["StartHour"].":".$_POST["StartMin"].":00";
	$EndTime = $_POST["EndHour"].":".$_POST["EndMin"].":00";
	$ClassLevelID = $_POST["classlvl"];
}
$Location = $_POST["TextLocation"];
$Vacancy = $_POST["TextVacancy"];
$PICID = $_POST["SelectTeacher"];
// 2014-1216-1347-28164 - Detention Session Type
if($sys_custom['eDiscipline']['CreativeSchDetentionCust']){
	$type = $_POST["sessiontype"];
}

if ($Vacancy < $AssignedStudentCount[0]) {
	$AssignedStudentCountFlag = false;
}


// Check for overlapping of date, time and location

if($AssignedStudentCount[0] == '0' && $disabled=="")
{
	$sql = "SELECT StartTime, EndTime, Location FROM DISCIPLINE_DETENTION_SESSION WHERE DetentionDate = '$DetentionDate' AND DetentionID <> '$DetentionID'";
	$result = $ldiscipline->returnArray($sql, 3);
	for ($i=0; $i<sizeof($result); $i++) {
		list($OldStartTime, $OldEndTime, $OldLocation) = $result[$i];
		if ($OldLocation == $Location && (($StartTime >= $OldStartTime && $StartTime < $OldEndTime) || ($EndTime > $OldStartTime && $EndTime <= $OldEndTime))) {
			$AvailableFlag = false;
			break;
		}
	}

	// Check for past session
	$Today = date("Y-m-d");
	$NowTime = date("H:i:s");
	
	if ($Today == $DetentionDate && $NowTime > $StartTime) {
		$FutureFlag = false;
	}
	
	// Check for forms assigned
	$resultForm = $ldiscipline->getAssignedFormByDetentionID($DetentionID);
	
	//die;
	for ($i=0; $i<sizeof($resultForm); $i++) {
		$tmpForm = $resultForm[$i];
		$Match = false;
		for ($j=0; $j<sizeof($ClassLevelID); $j++) {
			if ($tmpForm == $ClassLevelID[$j]) {
				$Match = true;
				break;
			}
		}
		if ($Match == false) {
			$AssignedFormExistFlag = false;
			break;
		}
	}
}

if ($AvailableFlag && $FutureFlag && $AssignedFormExistFlag && $AssignedStudentCountFlag) {
	
	if($AssignedStudentCount[0] == '0' && $disabled=="")
	{
		$DetentionArr['Semester'] = "'$semester'";
		$DetentionArr['StartTime'] = "'$StartTime'";
		$DetentionArr['EndTime'] = "'$EndTime'";
	}
	$DetentionArr['Location'] = "'$Location'";
	$DetentionArr['Vacancy'] = "'$Vacancy'";
	// 2014-1216-1347-28164 - Detention Session Type
	if($sys_custom['eDiscipline']['CreativeSchDetentionCust']){
		$DetentionArr['SessionType'] = "'$type'";
	}
	$ldiscipline->updateDetentionSessionByDetentionID($DetentionArr, $DetentionID);

	if($AssignedStudentCount[0] == '0' && $disabled=="")
	{
		$ldiscipline->deleteDetentionClassLevelByDetentionID($DetentionID);
		for ($i=0; $i<sizeof($ClassLevelID); $i++) {
			$DetentionArr = array();
			$DetentionArr['DetentionID'] = $DetentionID;
			$DetentionArr['ClassLevelID'] = $ClassLevelID[$i];
			$disciplineRecordID1 = $ldiscipline->insertDetentionClassLevel($DetentionArr);
		}
		if ($disciplineRecordID1=="") $SuccessFlag = false;
	}


	$ldiscipline->deleteDetentionPICByDetentionID($DetentionID);
	$PICIDUnique = array();
	for ($k=0; $k<sizeof($PICID); $k++) {
		if (!in_array($PICID[$k], $PICIDUnique)) {
			$PICIDUnique[] = $PICID[$k];
		}
	}
	for ($i=0; $i<sizeof($PICIDUnique); $i++) {
		$DetentionArr = array();
		$DetentionArr['DetentionID'] = $DetentionID;
		$DetentionArr['UserID'] = $PICIDUnique[$i];
		$disciplineRecordID2 = $ldiscipline->insertDetentionPIC($DetentionArr);
	}
	if ($disciplineRecordID2=="") $SuccessFlag = false;
}

if ($AvailableFlag && $FutureFlag && $AssignedFormExistFlag && $AssignedStudentCountFlag) {
	if ($SuccessFlag) {
		$SysMsg = "update";
	} else {
		$SysMsg = "update_failed";
	}
} else if (!$AssignedStudentCountFlag) {
	$SysMsg = "update_incompatible_vacancy";
} else if (!$AvailableFlag) {
	$SysMsg = "update_overlapped";
} else if (!$FutureFlag) {
	$SysMsg = "update_past";
} else if (!$AssignedFormExistFlag) {
	$SysMsg = "update_incompatible_form";
}

if ($cpage == "cal") {
	$ReturnPage = "calendar.php?cmonth=$cmonth&msg=$SysMsg&$back_par";
} else if ($cpage == "list") {
	$ReturnPage = "list.php?msg=$SysMsg&$back_par";
}

intranet_closedb();
header("Location: $ReturnPage");
?>