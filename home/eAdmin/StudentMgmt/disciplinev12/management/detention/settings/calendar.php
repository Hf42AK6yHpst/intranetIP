<?php
// Modifying by: yat

######################################## Change Log #################################################
#
#	Date	:	2013-10-25	YatWoon
#				Add back $cmonth variable for back to previous calender month [Case#2013-1016-1535-14073]
#
#	Date	: 	2010-05-07 YatWoon
#				According to the setting $PIC_SelectionDefaultOwn to check PIC default selected value
// 
//	Date 	:	20100118 (Ronald)
//	Details :	Now will use getSessionPeriodByMonth() to get all the detention sessions using in the selected year & month
//
############################################ End ###################################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

$ldiscipline->CONTROL_ACCESS("Discipline-SETTINGS-Detention-Access");

$CurrentPage = "Management_Detention";

$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($eDiscipline['Detention_Arrangement'], "../", 0);
$TAGS_OBJ[] = array($eDiscipline['Session_Management'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/management/detention/settings/calendar.php", 1);

if($msg2==11) {
	$xmsg2 = $Lang['eDiscipline']['PIC_Not_Available'];	
}

$SysMsg = $linterface->GET_SYS_MSG($msg, $xmsg2);

if ($cmonth == "") $cmonth = date("Ym");
$CurrentYear = substr($cmonth, 0, 4);
$CurrentMonth = substr($cmonth, 4, 2);
$NoOfDays = date("t", mktime(0,0,0,$CurrentMonth,1,$CurrentYear));
$CurrentDay = date("Y-m-d");
$CurrentTime = date("H:i", time());

$menuBar = $linterface->GET_LNK_NEW("new.php?cpage=cal&cmonth=$cmonth", $button_new,"","","",0);

// Previous month and next month
if ($CurrentMonth == 1) {
	$PreviousStr = ($CurrentYear-1)."12";
	$NextStr = $CurrentYear.str_pad(($CurrentMonth+1), 2, "0", STR_PAD_LEFT);
} else if ($CurrentMonth == 12) {
	$PreviousStr = $CurrentYear.($CurrentMonth-1);
	$NextStr = ($CurrentYear+1)."01";
} else {
	$PreviousStr = $CurrentYear.str_pad(($CurrentMonth-1), 2, "0", STR_PAD_LEFT);
	$NextStr = $CurrentYear.str_pad(($CurrentMonth+1), 2, "0", STR_PAD_LEFT);
}

// Empty box on first week
if (date("w", mktime(0,0,0,$CurrentMonth,1,$CurrentYear)) == 0) {	// sun
	$FirstWeekday = 2;
	$PreEmptyBox = 0;
} else if (date("w", mktime(0,0,0,$CurrentMonth,1,$CurrentYear)) == 1) {	// mon
	$FirstWeekday = 1;
	$PreEmptyBox = 0;
} else if (date("w", mktime(0,0,0,$CurrentMonth,1,$CurrentYear)) == 2) {	// tue
	$FirstWeekday = 1;
	$PreEmptyBox = 1;
} else if (date("w", mktime(0,0,0,$CurrentMonth,1,$CurrentYear)) == 3) {	// wed
	$FirstWeekday = 1;
	$PreEmptyBox = 2;
} else if (date("w", mktime(0,0,0,$CurrentMonth,1,$CurrentYear)) == 4) {	// thu
	$FirstWeekday = 1;
	$PreEmptyBox = 3;
} else if (date("w", mktime(0,0,0,$CurrentMonth,1,$CurrentYear)) == 5) {	// fri
	$FirstWeekday = 1;
	$PreEmptyBox = 4;
} else if (date("w", mktime(0,0,0,$CurrentMonth,1,$CurrentYear)) == 6) {	// sat
	$FirstWeekday = 3;
	$PreEmptyBox = $ldiscipline->Detention_Sat ? 1 : 0;
};

// Empty box on last week
if (date("w", mktime(0,0,0,$CurrentMonth,$NoOfDays,$CurrentYear)) == 0) {	// sun
	$PostEmptyBox = 0;
} else if (date("w", mktime(0,0,0,$CurrentMonth,$NoOfDays,$CurrentYear)) == 1) {	// mon
	$PostEmptyBox = 4;
} else if (date("w", mktime(0,0,0,$CurrentMonth,$NoOfDays,$CurrentYear)) == 2) {	// tue
	$PostEmptyBox = 3;
} else if (date("w", mktime(0,0,0,$CurrentMonth,$NoOfDays,$CurrentYear)) == 3) {	// wed
	$PostEmptyBox = 2;
} else if (date("w", mktime(0,0,0,$CurrentMonth,$NoOfDays,$CurrentYear)) == 4) {	// thu
	$PostEmptyBox = 1;
} else if (date("w", mktime(0,0,0,$CurrentMonth,$NoOfDays,$CurrentYear)) == 5) {	// fri
	$PostEmptyBox = 0;
} else if (date("w", mktime(0,0,0,$CurrentMonth,$NoOfDays,$CurrentYear)) == 6) {	// sat
	$PostEmptyBox = 0;
};

// Get list of available DetentionID and list of full DetentionID
$AvailableDetentionID = $ldiscipline->getDetentionArr("Available");
$FullDetentionID = $ldiscipline->getDetentionArr("Full");

// Get Distinct Period
//$AllPeriod = $ldiscipline->getSessionPeriod();
$AllPeriod = $ldiscipline->getSessionPeriodByMonth($CurrentYear,$CurrentMonth);

// Get Distinct PIC
$AllPIC = $ldiscipline->getSessionPIC();

// Get list of DetentionID with selected PIC
if (($targetPIC != "All_Duty") && ($targetPIC != "")) {
	$result = $ldiscipline->getDetentionIDByPIC($targetPIC);
	for ($i=0; $i<sizeof($result); $i++) {
		list($PICDetentionID[]) = $result[$i];
	}
}

$conds = "";
if ($targetStatus == "Available") {
	$conds .= " AND (DetentionDate > '$CurrentDay' OR (DetentionDate = '$CurrentDay' AND StartTime > '$CurrentTime:00'))";
	$conds .= " AND DetentionID IN (".implode(', ', $AvailableDetentionID).")";
} else if ($targetStatus == "Full") {
	$conds .= " AND (DetentionDate > '$CurrentDay' OR (DetentionDate = '$CurrentDay' AND StartTime > '$CurrentTime:00'))";
	$conds .= " AND DetentionID IN (".implode(', ', $FullDetentionID).")";
} else if ($targetStatus == "Finished") {
	$conds .= " AND (DetentionDate < '$CurrentDay' OR (DetentionDate = '$CurrentDay' AND StartTime < '$CurrentTime:00'))";
}

if (($targetPeriod != "All_Time") && ($targetPeriod != "")) {
	$conds .= " AND CONCAT(SUBSTRING(StartTime,1,5), '-', SUBSTRING(EndTime,1,5)) = '$targetPeriod'";
}

if (($targetPIC != "All_Duty") && ($targetPIC != "")) {
	$conds .= " AND DetentionID IN (".implode(', ', $PICDetentionID).")";
}

$sql = "SELECT DetentionID, DetentionDate, StartTime, EndTime, Location, Vacancy
				FROM DISCIPLINE_DETENTION_SESSION WHERE MONTH(DetentionDate) = $CurrentMonth AND YEAR(DetentionDate) = $CurrentYear
				$conds
				ORDER BY DetentionDate, StartTime, EndTime, Location";
$result = $ldiscipline->returnArray($sql, 6);
for ($i=0; $i<sizeof($result); $i++) {
	list($DetentionIDRec[], $DetentionDateRec[], $StartTimeRec[], $EndTimeRec[], $LocationRec[], $VacancyRec[]) = $result[$i];
}

// Get the count of assigned students for current month
$result = $ldiscipline->getAssignedStudentCountByYearMonth($CurrentYear, $CurrentMonth);
for ($i=0; $i<sizeof($result); $i++) {
	list($DetentionIDStudentRec[], $StudentIDCountRec[]) = $result[$i];
}

// Get the names of PIC for current month
$result = $ldiscipline->getDetentionPICByYearMonth($CurrentYear, $CurrentMonth);
for ($i=0; $i<sizeof($result); $i++) {
	list($DetentionIDPICRec[], $PICRec[], $NameRec[]) = $result[$i];
}

for ($i=0; $i<sizeof($DetentionIDRec); $i++) {
	$DisplayDetentionID[$DetentionDateRec[$i]][] = $DetentionIDRec[$i];
	$DisplayPeriod[$DetentionDateRec[$i]][] = substr($StartTimeRec[$i],0,5)."-".substr($EndTimeRec[$i],0,5);
	$DisplayLocation[$DetentionDateRec[$i]][] = intranet_htmlspecialchars($LocationRec[$i]);
	$DisplayVacancy[$DetentionDateRec[$i]][] = $VacancyRec[$i];
	$TmpPIC = array();
	for ($j=0; $j<sizeof($DetentionIDPICRec); $j++) {
		if ($DetentionIDRec[$i] == $DetentionIDPICRec[$j]) {
			$TmpPIC[] = $NameRec[$j];
		}
	}
	$DisplayPIC[$DetentionDateRec[$i]][] = implode(", ", $TmpPIC);
	$TmpCount = 0;
	for ($k=0; $k<sizeof($DetentionIDStudentRec); $k++) {
		if ($DetentionIDRec[$i] == $DetentionIDStudentRec[$k]) {
			$TmpCount = $StudentIDCountRec[$k];
		}
	}
	$DisplayAssigned[$DetentionDateRec[$i]][] = $TmpCount;
	$resultAttendanceCount = $ldiscipline->getAttendanceCountByDetentionID($DetentionIDRec[$i], "PRE");
	if ($resultAttendanceCount <> "--" && $resultAttendanceCount < $TmpCount) $resultAttendanceCount = "<span class=\"Warningtitletext\">".$resultAttendanceCount."</span>";
	$DisplayAttended[$DetentionDateRec[$i]][] = $resultAttendanceCount;
}


# filters
$select_status = "<SELECT name=\"targetStatus\" onChange=\"this.form.submit()\">\n";
$select_status .= "<OPTION value='All_Status' ".($targetStatus=="All_Status"?"SELECTED":"").">$i_Discipline_Detention_All_Status</OPTION>\n";
$select_status .= "<OPTION value='Available' ".($targetStatus=="Available"?"SELECTED":"").">$i_Discipline_Available</OPTION>\n";
$select_status .= "<OPTION value='Full' ".($targetStatus=="Full"?"SELECTED":"").">$i_Discipline_Full</OPTION>\n";
$select_status .= "<OPTION value='Finished' ".($targetStatus=="Finished"?"SELECTED":"").">$i_Discipline_Finished</OPTION>\n";
$select_status .= "</SELECT>\n";

$select_period = "<SELECT name=\"targetPeriod\" onChange=\"this.form.submit()\">\n";
$select_period .= "<OPTION value='All_Time' ".($targetPeriod=="All_Time"?"SELECTED":"").">$i_Discipline_Detention_All_Time_Slot</OPTION>\n";
for ($i=0; $i<sizeof($AllPeriod); $i++) {
	list($TempPeriod) = $AllPeriod[$i];
	$select_period .= "<OPTION value='$TempPeriod' ".($targetPeriod==$TempPeriod?"SELECTED":"").">$TempPeriod</OPTION>\n";
}
$select_period .= "</SELECT>\n";

if($ldiscipline->PIC_SelectionDefaultOwn)
{
	if(sizeof($_POST)==0 && sizeof($_GET)==0 && !isset($_POST['targetPIC'])) {		# default display own PIC records
		$flag = 0;
		for($i=0; $i<sizeof($AllPIC); $i++) {
			if($AllPIC[$i][0]==$UserID) {
				$targetPIC = $UserID;		
				$flag = 1;
				break;
			}
		}
	}
}

$select_pic = "<SELECT name=\"targetPIC\" onChange=\"this.form.submit()\">\n";
$select_pic .= "<OPTION value='All_Duty' ".($targetPIC=="All_Duty"?"SELECTED":"").">$i_Discipline_Detention_All_Teachers</OPTION>\n";
for ($i=0; $i<sizeof($AllPIC); $i++) {
	list($TempUserID, $TempPIC) = $AllPIC[$i];
	$select_pic .= "<OPTION value='$TempUserID' ".($targetPIC==$TempUserID?"SELECTED":"").">$TempPIC</OPTION>\n";
}
$select_pic .= "</SELECT>\n";

$searchbar = "$select_status $select_period $select_pic";

$linterface->LAYOUT_START();
?>
<br />
<form name="FormCalendar" method="POST" action="calendar.php">

<script type="text/javascript">
	function deleteSessionAlert(did) {
		if (document.getElementById('assign'+did).innerHTML>0){
			alert("<?=$i_Discipline_Assigned_Session_Cannot_Be_Deleted?>");
			return false;
		} else {
			if(confirm("<?=$i_Discipline_Delete_Session_Alert?>")){
				return true;
			} else {
				return false;
			}
		}
	}

	function changeMonth(targetMonth){
		obj = document.getElementById('cmonth');
		obj.value = targetMonth;
		document.FormCalendar.submit();
	}
</script>

<table width="97%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td><?= $menuBar ?></td>
				</tr>
			</table>
		</td>
		<td align="right"><?= $SysMsg ?>&nbsp;</td>
	</tr>
</table>

<table width="97%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="right">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td height="28" align="right" valign="bottom" class="tabletextremark">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr></tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td height="30">&nbsp;</td>
					<td align="right" valign="middle" class="thumb_list"><span><?=$i_Discipline_Calendar?></span> | <a href="list.php"><?=$i_Discipline_Session?></a></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<table width="97%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td><?= $searchbar ?></td>
				</tr>
			</table>
		</td>
		<td align="right">
			<span class="icalendar_title">
				<a href="javascript:changeMonth('<?=$PreviousStr?>');"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icalendar/icon_prev_off.gif" width="21" height="21" border="0" align="absmiddle" title="<?=$i_Discipline_Previous?>"></a>
<?=date("F", mktime(0,0,0,$CurrentMonth,1,$CurrentYear))?> <?=date("Y", mktime(0,0,0,$CurrentMonth,1,$CurrentYear))?>
				<a href="javascript:changeMonth('<?=$NextStr?>');"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icalendar/icon_next_off.gif" width="21" height="21" border="0" align="absmiddle" title="<?=$i_Discipline_Next?>"></a>
			</span>
		</td>
	</tr>
</table>


<table width="97%" border="0" cellpadding="3" cellspacing="1" bgcolor="#D2D2D2">
	<tr>
		<td width="16%" align="center" class="icalendar_weektitle"><?=$i_DayType0[1]?></td>
		<td width="16%" align="center" class="icalendar_weektitle"><?=$i_DayType0[2]?></td>
		<td width="16%" align="center" class="icalendar_weektitle"><?=$i_DayType0[3]?></td>
		<td width="16%" align="center" class="icalendar_weektitle"><?=$i_DayType0[4]?></td>
		<td width="16%" align="center" class="icalendar_weektitle"><?=$i_DayType0[5]?></td>
		<? if($ldiscipline->Detention_Sat) {?>
		<td width="16%" align="center" class="icalendar_weektitle"><?=$i_DayType0[6]?></td>
		<? } ?>
	</tr>
	<tr height="140">
<?
	for ($i=0; $i<$PreEmptyBox; $i++) {
?>
		<td valign="top" bgcolor="#FFFFFF">&nbsp;</td>
<?
	}
	$FirstWeekFlag = true;
	for ($j=$FirstWeekday; $j<=$NoOfDays; $j++) {
		if ((date("w", mktime(0,0,0,$CurrentMonth,$j,$CurrentYear)) == 1) && !$FirstWeekFlag) {
?>
	<tr height="140">
<?
		}

		if($ldiscipline->Detention_Sat)
			$if_condition = (date("w", mktime(0,0,0,$CurrentMonth,$j,$CurrentYear)) != 0);
		else
			$if_condition = (date("w", mktime(0,0,0,$CurrentMonth,$j,$CurrentYear)) != 0) && (date("w", mktime(0,0,0,$CurrentMonth,$j,$CurrentYear)) != 6);
		if ($if_condition) 
		{
?>
		<td valign="top" bgcolor="#FFFFFF"<?=($CurrentYear.$CurrentMonth.$j==date("Ymj"))?" style=\"background-color:#FCFFD7\"":""?>><strong<?=($CurrentYear.$CurrentMonth.$j==date("Ymj"))?" style=\"color:#FFFFFF; background-color:#D97200; padding:2px \"":""?>><?=$j?></strong><br>
<?
			$TmpDate = $CurrentYear."-".$CurrentMonth."-".str_pad($j, 2, "0", STR_PAD_LEFT);
			$TmpArray = $DisplayPeriod[$TmpDate];
			if (is_array($TmpArray)) {
				foreach ($TmpArray as $TmpKey => $TmpValue) {
					//if (($TmpDate < $CurrentDay) || (($TmpDate == $CurrentDay) && (substr($DisplayPeriod[$TmpDate][$TmpKey],0,5) < $CurrentTime))) {
					if (($TmpDate < $CurrentDay)) {
						$TableClass = "detent_room_past";
						$EditableFlag = false;
						$DeleteFlag = false;
					} else if ($DisplayAssigned[$TmpDate][$TmpKey] > 0) {
						if ($DisplayVacancy[$TmpDate][$TmpKey] <= $DisplayAssigned[$TmpDate][$TmpKey]) {
							$TableClass = "detent_room_full";
						}else{
							$TableClass = "detent_room_vacancy";
						}
						$EditableFlag = true;
						$DeleteFlag = false;
					} else if ($DisplayVacancy[$TmpDate][$TmpKey] <= $DisplayAssigned[$TmpDate][$TmpKey]) {
						$TableClass = "detent_room_full";						
						$EditableFlag = true;
						$DeleteFlag = true;
					} else {
						$TableClass = "detent_room_vacancy";
						$EditableFlag = true;
						$DeleteFlag = true;
					}

					if ($EditableFlag) {
						$ImgSrcEclass = $image_path."/".$LAYOUT_SKIN."/ediscipline/icon_eclass.gif";
						$ImgSrcTeacher = $image_path."/".$LAYOUT_SKIN."/ediscipline/icon_teacher.gif";
						$ImgSrcTime = $image_path."/".$LAYOUT_SKIN."/ediscipline/icon_time.gif";
						$ImgSrcVacancy = $image_path."/".$LAYOUT_SKIN."/ediscipline/icon_vancany.gif";
						$ImgSrcAttend = $image_path."/".$LAYOUT_SKIN."/ediscipline/icon_attend.gif";
					} else {
						$ImgSrcEclass = $image_path."/".$LAYOUT_SKIN."/ediscipline/icon_eclass_g.gif";
						$ImgSrcTeacher = $image_path."/".$LAYOUT_SKIN."/ediscipline/icon_teacher_g.gif";
						$ImgSrcTime = $image_path."/".$LAYOUT_SKIN."/ediscipline/icon_time_g.gif";
						$ImgSrcVacancy = $image_path."/".$LAYOUT_SKIN."/ediscipline/icon_vancany_g.gif";
						$ImgSrcAttend = $image_path."/".$LAYOUT_SKIN."/ediscipline/icon_attend_g.gif";
					}
					$ImgSrcTakeAttend = $image_path."/".$LAYOUT_SKIN."/ediscipline/icon_takeattendance.gif";
?>
			<table width="100%" border="0" cellpadding="2" cellspacing="0" class="<?=$TableClass?>">
<?
					if (($EditableFlag)||($DeleteFlag)){
?>
				<tr>
					<td class="tablebottom" width="20"><br></td>
					<td align="right" class="tablebottom">
						<? if($DeleteFlag) { ?>
						<a href="delete_update.php?cmonth=<?=$cmonth?>&did=<?=$DisplayDetentionID[$TmpDate][$TmpKey]?>" class="tabletool" onClick="return deleteSessionAlert(<?=$DisplayDetentionID[$TmpDate][$TmpKey]?>);">
							<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle" title="<?=$button_delete?>"></a>
						<? } ?>
						<? if($EditableFlag) { ?>
						<a href="edit.php?did=<?=$DisplayDetentionID[$TmpDate][$TmpKey]?>&cpage=cal&cmonth=<?=$cmonth?>&targetStatus=<?=$targetStatus?>&targetPeriod=<?=$targetPeriod?>&targetPIC=<?=$targetPIC?>" class="tabletool">
							<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle" title="<?=$button_edit?>"></a>
						<? } ?>
					</td>
				</tr>
<?
					}
?>
				<tr>
					<td valign="top" nowrap width="20">
						<img src="<?=$ImgSrcEclass?>" width="20" height="20" border="0" align="absmiddle" title="<?=$i_Discipline_Location?>"></td>
					<td nowrap>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tbclip">
							<tr>
								<td nowrap><?=$DisplayLocation[$TmpDate][$TmpKey]?></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap width="20">
						<img src="<?=$ImgSrcTeacher?>" width="20" height="20" border="0" align="absmiddle" title="<?=$i_Discipline_Duty_Teacher?>"></td>
					<td nowrap>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tbclip">
							<tr>
								<td nowrap><?=$DisplayPIC[$TmpDate][$TmpKey]?></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap width="20">
						<img src="<?=$ImgSrcTime?>" width="20" height="20" border="0" align="absmiddle" title="<?=$i_Discipline_Time?>"></td>
					<td nowrap>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tbclip">
							<tr>
								<td><?=$DisplayPeriod[$TmpDate][$TmpKey]?></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" valign="top" nowrap>
						<table width="100%" border="0" cellpadding="1" cellspacing="0">
							<tr>
								<td width="16%">
									<img src="<?=$ImgSrcVacancy?>" width="20" height="20" border="0" align="absmiddle" title="<?=$i_Discipline_Vacancy?>"></td>
								<td width="16%"><?=$DisplayVacancy[$TmpDate][$TmpKey]?></td>
								<td width="16%">
									<img src="<?=$ImgSrcAttend?>" width="20" height="20" border="0" align="absmiddle" title="<?=$i_Discipline_Assigned?>"></td>
								<td width="16%" id="assign<?=$DisplayDetentionID[$TmpDate][$TmpKey]?>" ><?=$DisplayAssigned[$TmpDate][$TmpKey]?></td>
								<td width="16%"><img src="<?=$ImgSrcTakeAttend?>" width="20" height="20" border="0" align="absmiddle" title="<?=$i_Discipline_Attended?>"></td>
								<td width="16%" id="attend<?=$DisplayDetentionID[$TmpDate][$TmpKey]?>"><?=$DisplayAttended[$TmpDate][$TmpKey]?></td>
							</tr>
						</table></td>
				</tr>
				<!-- 
<?
					$TakeAttendanceFlag = $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-TakeAttendance");
					if ($TakeAttendanceFlag) {
?>											<tr>
								<td valign="top" nowrap>&nbsp;</td>
								<td nowrap><a href="take_attendance.php?did=<?=$DisplayDetentionID[$TmpDate][$TmpKey]?>" class="contenttool"><?=$i_Discipline_Take_Attendance?></a></td>
							</tr>
<?
					}
?>
-->
			</table>
<?
				}
			}
?>
		</td>
<?
		}
		if (date("w", mktime(0,0,0,$CurrentMonth,$j,$CurrentYear)) == ($ldiscipline->Detention_Sat ? 6 : 5)) {
			$FirstWeekFlag = false;
?>
	</tr>
<?
		}
	}
	for ($i=0; $i<$PostEmptyBox; $i++) {
?>
		<td valign="top" bgcolor="#FFFFFF">&nbsp;</td>
<?
		if ($i==$PostEmptyBox-1) {
?>
	</tr>
<?
		}
	}
?>

</table>
<input type="hidden" id="cmonth" name="cmonth" value="<?=$cmonth?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>