<?php
// Modifying by: 

##################################################################
#
# 	Date:	2017-01-09	Bill	[2016-0808-1743-20240]
#			Cust: Handling for RecordID from Calendar 
#
##################################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
if(!($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-RearrangeStudent")))
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}
else
{
$SuccessFlag = true;
$FormAllowedFlag = true;
$StudentNotInSessionFlag = true;

// [2016-0808-1743-20240] Special Handling - RecordID received from calendar view is StudentSessionID 
if($sys_custom['eDiscipline']['BWWTC_CalendarWithReason'] && $isAssignFromCalendar)
{
	if (is_array($RecordID) && $RecordID[0] <> "")
	{
		for ($i=0; $i<sizeof($RecordID); $i++)
		{
			// Get StudentID using StudentSessionID
			$sql = "SELECT StudentID FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE RecordID = '".$RecordID[$i]."' ORDER BY StudentID";
			$StudentID = $ldiscipline->returnVector($sql);
			$StudentID = $StudentID[0];
			
			if (!($ldiscipline->checkFormAllowed($StudentID, $DetentionID))) {
				$FormAllowedFlag = false;
			}
			if ($ldiscipline->checkStudentInSession($StudentID, $DetentionID)) {
				$StudentNotInSessionFlag = false;
			}
		}
	}
	
	// Check if student can be added to detention session
	if ($FormAllowedFlag && $StudentNotInSessionFlag && is_array($RecordID) && $RecordID[0] <> "") {
		for ($i=0; $i<sizeof($RecordID); $i++)
		{
			// Get StudentID using StudentSessionID
			$sql = "SELECT StudentID FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE RecordID = '".$RecordID[$i]."' ORDER BY StudentID";
			$StudentID = $ldiscipline->returnVector($sql);
			$StudentID = $StudentID[0];
			
			// Assign to detention session
			$sql = "UPDATE DISCIPLINE_DETENTION_STUDENT_SESSION SET DetentionID = '$DetentionID', ArrangedBy = '".$_SESSION['UserID']."', ArrangedDate = NOW(), DateModified = NOW(), isAutoAssign='0' ";
			$sql .= " WHERE RecordID = '".$RecordID[$i]."' AND StudentID = '".$StudentID."' AND DetentionID IS NULL AND RecordStatus = 1 LIMIT 1";
			$ldiscipline->db_db_query($sql);

			$sql = "SELECT RecordID FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE RecordID = '".$RecordID[$i]."' AND StudentID = '".$StudentID."' AND DetentionID = '$DetentionID' AND RecordStatus = 1";
			$result = $ldiscipline->returnVector($sql);
			if (!$result) $SuccessFlag = false;
		}
	}
}
else
{
	if (is_array($RecordID) && $RecordID[0] <> "") {
		for ($i=0; $i<sizeof($RecordID); $i++) {
			$StudentID = $RecordID[$i];
			if (!($ldiscipline->checkFormAllowed($StudentID, $DetentionID))) {
				$FormAllowedFlag = false;
			}
			if ($ldiscipline->checkStudentInSession($StudentID, $DetentionID)) {
				$StudentNotInSessionFlag = false;
			}
		}
	}
	
	if ($FormAllowedFlag && $StudentNotInSessionFlag && is_array($RecordID) && $RecordID[0] <> "") {
		for ($i=0; $i<sizeof($RecordID); $i++) {
			$StudentID = $RecordID[$i];
	
			$DetentionArr['StudentID'] = $StudentID;
			$DetentionArr['DetentionID'] = $DetentionID;
			
			$result = $ldiscipline->updateDetention($DetentionArr);
	
			if (!$result) $SuccessFlag = false;
		}
	}
}

if ($FormAllowedFlag && $StudentNotInSessionFlag) {
	if ($SuccessFlag) {
		$SysMsg = "update";
	}
	else {
		$SysMsg = "update_failed";
	}
}
else if ($FormAllowedFlag) {
	$SysMsg = "update_already_in_session";
}
else {
	$SysMsg = "update_inapplicable_form";
}

intranet_closedb();
$ReturnPage = "calendar.php";
header("Location: $ReturnPage?msg=$SysMsg");
?>
<?
}
?>