<?php
// Modifying by: anna

###### Change Log [Start] #############
#	Date	L	2018-01-16 Anna
#				added send push message
#
#	Date	:	2015-09-24	Bill	[2015-0924-1046-51071]
#				change table column name to Attendance Status & Attendance Remark
#
#	Date	:	2014-08-06	(YatWoon)	[Case#D64929]
#				fixed: faield to display content, page corrupted.  use $ldiscipline->AllFormsData instead of $this->AllFormsData
#				deploy: ip.2.5.5.8.1
#
#	Date	:	2011-03-21 (Henry Chow)
#				revised the sorting of "Class" column
#
#	Date	:	2010-06-18	YatWoon
#				Add "add punishment for absent student" link
#
#	Date:	2020-06-03 [YatWoon]
#			add "Craeted by" column
#
#	Date	:	2010-05-27 YatWoon
#				Add status "select all" option
#
#	Date	:	2010-05-10	YatWoon
#				Add PIC column
#				Add "add misconduct for absent student" link
#
###### Change Log [End] #############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Detention-TakeAttendance");

$CurrentPage = "Management_Detention";

$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($eDiscipline['Detention_Arrangement'], "", 1);
$TAGS_OBJ[] = array($eDiscipline['Session_Management'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/management/detention/settings/calendar.php", 0);
$PAGE_NAVIGATION[] = array($i_Discipline_Calendar, "calendar.php");
$PAGE_NAVIGATION[] = array($i_Discipline_Take_Attendance, "");

$SysMsg = $linterface->GET_SYS_MSG("$msg");


if (!isset($field)) $field = 0;		// default: sort by class
if (!isset($order)) $order = 1;		// default: asc order

$pageSizeChangeEnabled = true;

if ($page_size_change == 1) {
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
	$ck_page_size = $numPerPage;
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;

$li = new libdbtable2007($field, $order, $pageNo);

$name_field = getNameFieldByLang("USR.");
$clsName = ($intranet_session_language=="en") ? "yc.ClassTitleEN" : "yc.ClassTitleB5";

$yearID = Get_Current_Academic_Year_ID();
if($plugin['eClassApp']){
$PusheMessageCheckBox  = ",CONCAT('<input id=pushMessageID',SES.StudentID,' 
									name=pushMessageID',SES.StudentID,'    
									type=checkbox 
									value=', SES.StudentID ,'>
						') as CheckBox";
}
else{
	$PusheMessageCheckBox = "";
}
/*
$sql = "SELECT CONCAT(USR.ClassName, '-', USR.ClassNumber) AS Class, $name_field AS Student, SES.Reason, IF(SES.AttendanceStatus='PRE', CONCAT('<span id=\"span', SES.StudentID, '\"><img src=\"$image_path/$LAYOUT_SKIN/smartcard/icon_present.gif\" width=\"20\" height=\"20\" title=\"$i_Discipline_Attended\"></span> <select name=\"select', SES.StudentID, '\" onChange=\"javascript:changeStatus(', SES.StudentID, ');\"><option value=\"\"></option><option value=\"PRE\" selected>$i_Discipline_Present</option><option value=\"ABS\">$i_Discipline_Absent</option></select>'), IF(SES.AttendanceStatus='ABS', CONCAT('<span id=\"span', SES.StudentID, '\"><img src=\"$image_path/$LAYOUT_SKIN/smartcard/icon_absent.gif\" width=\"20\" height=\"20\" title=\"$i_Discipline_Absent\"></span> <select name=\"select', SES.StudentID, '\" onChange=\"javascript:changeStatus(', SES.StudentID, ');\"><option value=\"\"></option><option value=\"PRE\">$i_Discipline_Present</option><option value=\"ABS\" selected>$i_Discipline_Absent</option></select>'), CONCAT('<span id=\"span', SES.StudentID, '\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"></span> <select name=\"select', SES.StudentID, '\" onChange=\"javascript:changeStatus(', SES.StudentID, ');\"><option value=\"\" selected></option><option value=\"PRE\">$i_Discipline_Present</option><option value=\"ABS\">$i_Discipline_Absent</option></select>'))) AS AttendanceStatus, CONCAT('<input name=\"textAttendanceRemark', SES.StudentID, '\" type=\"text\" class=\"textbox\" style=\"background:#ffffff\" value=\"', REPLACE(IFNULL(SES.AttendanceRemark, ''), '\"', '&quot;') , '\">') AS AttendanceRemark, SES.StudentID
				FROM DISCIPLINE_DETENTION_STUDENT_SESSION SES, INTRANET_USER USR
				WHERE SES.StudentID = USR.UserID
				AND SES.DetentionID = '$did'";
*/
$sql = "SELECT 
			CONCAT($clsName, '-', ycu.ClassNumber) AS Class, 
			$name_field AS Student, 
			SES.Reason, 
			SES.PICID,
			SES.CreatedBy,
			IF(SES.AttendanceStatus='PRE', CONCAT('<span id=\"span', SES.StudentID, '\"><img src=\"$image_path/$LAYOUT_SKIN/smartcard/icon_present.gif\" width=\"20\" height=\"20\" title=\"$i_Discipline_Attended\"></span> <select name=\"select', SES.StudentID, '\" id=\"select',SES.StudentID,'\" onChange=\"javascript:changeStatus(', SES.StudentID, ');\"><option value=\"\"></option><option value=\"PRE\" selected>$i_Discipline_Present</option><option value=\"ABS\">$i_Discipline_Absent</option></select>'), IF(SES.AttendanceStatus='ABS', CONCAT('<span id=\"span', SES.StudentID, '\"><img src=\"$image_path/$LAYOUT_SKIN/smartcard/icon_absent.gif\" width=\"20\" height=\"20\" title=\"$i_Discipline_Absent\"></span> <select name=\"select', SES.StudentID, '\" id=\"select',SES.StudentID,'\" onChange=\"javascript:changeStatus(', SES.StudentID, ');\"><option value=\"\"></option><option value=\"PRE\">$i_Discipline_Present</option><option value=\"ABS\" selected>$i_Discipline_Absent</option></select>'), CONCAT('<span id=\"span', SES.StudentID, '\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"></span> <select name=\"select', SES.StudentID, '\" id=\"select',SES.StudentID,'\" onChange=\"javascript:changeStatus(', SES.StudentID, ');\"><option value=\"\" selected></option><option value=\"PRE\">$i_Discipline_Present</option><option value=\"ABS\">$i_Discipline_Absent</option></select>'))) AS AttendanceStatus, 
			CONCAT('<input name=\"textAttendanceRemark', SES.StudentID, '\" id=\"textAttendanceRemark',SES.StudentID,'\" type=\"text\" class=\"textbox\" style=\"background:#ffffff\" value=\"', REPLACE(IFNULL(SES.AttendanceRemark, ''), '\"', '&quot;') , '\">') AS AttendanceRemark, 
			
			SES.StudentID,  
			SES.Remark,  
			SES.RecordID
$PusheMessageCheckBox
		FROM DISCIPLINE_DETENTION_STUDENT_SESSION SES
				LEFT OUTER JOIN INTRANET_USER USR ON (SES.StudentID = USR.UserID)
				LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID)
				LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)
				WHERE SES.DetentionID = '$did' AND SES.RecordStatus=1 AND yc.AcademicYearID=$yearID";
		
$li->field_array = array("concat(TRIM(SUBSTRING_INDEX(Class, '-', 1)), IF(INSTR(Class, '-') = 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(Class, '-', -1)), 10, '0')))", "Student", "Reason", "AttendanceStatus", "AttendanceRemark");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+3;
$li->title = $i_Discipline_Records;
$li->column_array = array(14,14,14,14,14);
$li->IsColOff = "eDisciplineDetentionTakeAttendance";
//$li->AllFormsData = $i_Discipline_All_Forms;


// TABLE COLUMN
$set_all_selection = "
	<select name=\"select_all\" id=\"select_all\">
	<option value=\"\" selected=\"\">
	<option value=\"PRE\">$i_Discipline_Present</option>
	<option value=\"ABS\">$i_Discipline_Absent</option>
	</select>
	<a href=\"javascript:changeAllStatus();\"><img src=\"". $image_path ."/". $LAYOUT_SKIN ."/icon_assign.gif\" border=\"0\"></a>	
";

$pos = 0;
$li->column_list .= "<td width='2%' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='10%' nowrap valign='top'>".$li->column($pos++, $i_Discipline_Class)."</td>\n";
$li->column_list .= "<td width='25%' valign='top'>".$li->column($pos++, $i_Discipline_Student)."</td>\n";
$li->column_list .= "<td width='28%' valign='top'>".$li->column($pos++, $i_Discipline_Detention_Reason)."</td>\n";
$li->column_list .= "<td width='20%' valign='top'>". $i_Discipline_PIC ."</td>\n";
$li->column_list .= "<td width='20%' valign='top'>". $Lang['eDiscipline']['RecordCreatedBy'] ."</td>\n";
$li->column_list .= "<td width='20%' nowrap valign='top'>".$li->column($pos++, $Lang['eDiscipline']['Detention']['AttendanceStatus'])."<br>".$set_all_selection."</td>\n";
$li->column_list .= "<td width='15%' nowrap valign='top'>".$li->column($pos++, $Lang['eDiscipline']['Detention']['AttendanceRemark'])."</td>\n";
// $li->column_list .= "<td width='3%' nowrap valign='top'>".$Lang['AppNotifyMessage']['PushMessage']."</td>\n";
if($plugin['eClassApp']){
	$li->column_list .= "<td width='15%' nowrap valign='top'>". $Lang['AppNotifyMessage']['PushMessage']."</td>\n";
}
$linterface->LAYOUT_START();

$TempList = $ldiscipline->getDetentionListByDetentionID($did);

list($TmpDetentionID,$TmpDetentionDate,$TmpDetentionDayName,$TmpStartTime,$TmpEndTime,$TmpPeriod,$TmpLocation,$TmpForm,$TmpPIC,$TmpVacancy,$TmpAssigned,$TmpAvailable) = $TempList[0];

if ($TmpForm == $TempAllForms) $TmpForm = $ldiscipline->AllFormsData;

$SessionString =$ldiscipline->displayDetentionSessionString($did);

$TempList = $ldiscipline->getDetentionLastUpdatedByDetentionID($did);
$TmpAttendanceTakenBy = $TempList[0];
$TmpAttendanceTakenDate = $TempList[1];
if ($TmpAttendanceTakenBy <> "" && $TmpAttendanceTakenDate <> "") {
	$TempList = $ldiscipline->getUserNameByID($TmpAttendanceTakenBy);
	$TmpStudentName = $TempList[0];
	$LastUpdatedString = $TmpAttendanceTakenDate." (".$TmpStudentName.")";
}

$TempList = $ldiscipline->getStudentIDByDetentionID($did);
for($i=0;$i<sizeof($TempList);$i++) {
	$StudentID[] = $TempList[$i];
}
$AbsentStudentID = implode(",",$ldiscipline->returnDetentionSessionAbsentStudentList($did));
?>

<script language="javascript">
	// Grab reasons
	function getReasons(pos) {
		var jArrayTemp = new Array();
		var obj2 = document.getElementById("select"+pos);
		if (obj2.selectedIndex == 2) {
			return jArrayWordsAbsence;
		} else {
			return jArrayTemp;
		}
	}

	function changeStatus(studentID) {
		var newStatus = document.getElementById("select"+studentID).value;
		if(!isNaN(studentID)) {
			if (newStatus == "PRE") {
				document.getElementById("span"+studentID).innerHTML = "<img src=\"<?="$image_path/$LAYOUT_SKIN"?>/smartcard/icon_present.gif\" width=\"20\" height=\"20\" title=\"<?=$i_Discipline_Attended?>\">";
				document.getElementById("tr"+studentID).className = "row_approved";
				document.getElementById("textAttendanceRemark"+studentID).disabled = false;
				document.getElementById("textAttendanceRemark"+studentID).style.background= "#ffffff";
				document.getElementById("poslink"+studentID).style.display = "none";
			}	else if (newStatus == "ABS" || newStatus=="PRE"){
				document.getElementById("span"+studentID).innerHTML = "<img src=\"<?="$image_path/$LAYOUT_SKIN"?>/smartcard/icon_absent.gif\" width=\"20\" height=\"20\" title=\"<?=$i_Discipline_Absent?>\">";
				document.getElementById("tr"+studentID).className = "row_suspend";
				document.getElementById("textAttendanceRemark"+studentID).disabled = false;
				document.getElementById("textAttendanceRemark"+studentID).style.background= "#ffffff";
				document.getElementById("poslink"+studentID).style.display = "inline";
				
			}	else {
				document.getElementById("span"+studentID).innerHTML = "<img src=\"<?="$image_path/$LAYOUT_SKIN"?>/10x10.gif\" width=\"20\" height=\"20\" align=\"absmiddle\">";
				document.getElementById("tr"+studentID).className = "row_waiting";
				document.getElementById("textAttendanceRemark"+studentID).disabled = true;
				document.getElementById("textAttendanceRemark"+studentID).value = "";
				document.getElementById("textAttendanceRemark"+studentID).style.background= "#dfdfdf";
				document.getElementById("poslink"+studentID).style.display = "none";
			}

			if(newStatus == "ABS"){
				document.getElementById("pushMessageID"+studentID).disabled = false;
			}else{
				document.getElementById("pushMessageID"+studentID).disabled = true;		
			}
		}
	}
	
	function changeAllStatus()
	{
		var obj = document.form1;
		
		for(count=0;count<obj.length;count++)
		{
			if(obj[count].type == "select-one")
			{
				if(obj[count].name.substring(0, 6)=="select" && obj.elements[count].name!="select_all")
				{
					obj[count].selectedIndex = obj.select_all.selectedIndex;
					studentID = obj.elements[count].name.substr(6);
					changeStatus(studentID);
				}
			}	
		}
	}
	

	function absToPre() {
		var obj = document.form1;
		var len = obj.elements.length;
		var studentID;
		for(var i=0; i<len; i++) {
			if (obj.elements[i].name.substr(0,6)=="select") {
				studentID = obj.elements[i].name.substr(6)
				objSelect = obj.elements[i];
				if (objSelect.selectedIndex != 1) {
					objSelect.selectedIndex = 1;
				}
				changeStatus(studentID);
			}
		}
	}

	function updateStyle() {
		var obj = document.form1;
		var len = obj.elements.length;
		var studentID;
		for(var i=0; i<len; i++) {
			if (obj.elements[i].name.substr(0,6)=="select" && obj.elements[i].name!="select_all") {
				studentID = obj.elements[i].name.substr(6);
				changeStatus(studentID);
			}
		}
	}

	function checkForm(){
		var obj = document.form1;
		var len = obj.elements.length;
		for(var i=0; i<len; i++) {
			if (obj.elements[i].name.substr(0,6)=="select" && obj.elements[i].name!="select_all") {
				objSelect = obj.elements[i];
				if (objSelect.selectedIndex == 0) {
					alert('<?=$i_alert_pleaseselect.$i_Discipline_Status?>');
					return false;
				}
			}
		}
		if(confirm("<?=$i_Discipline_Take_Attendance_Alert?>")){
			var obj = document.form1;
			obj.action = "take_update.php";
			obj.method = "post";
			obj.submit();
		} else {
			return false;
		}
	}
	
	function setClickID(id) {
		document.getElementById('clickID').value = id;	
	}
	
	var ClickID = '';
	var callback_show_ref_list = {
		success: function ( o )
	    {
		    var tmp_str = o.responseText;
		    document.getElementById('ref_list').innerHTML = tmp_str;
		    DisplayPosition();
		}
	}
	
	function show_ref_list(type,click)
	{
		ClickID = click;
		document.getElementById('task').value = type;
		obj = document.form1;
		YAHOO.util.Connect.setForm(obj);
		var path = "ajax_content.php";
	    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_show_ref_list);
	}
	
	function DisplayPosition()
	{
	  document.getElementById('ref_list').style.left= (getPosition(document.getElementById(ClickID),'offsetLeft') +10)+'px';
	  document.getElementById('ref_list').style.top=getPosition(document.getElementById(ClickID),'offsetTop') + 'px';
	  document.getElementById('ref_list').style.visibility='visible';
	}
	
	function getPosition(obj, direction)
	{
		var objStr = "obj";
		var pos_value = 0;
		while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
		{
			pos_value += eval(objStr + "." + direction);
			objStr += ".offsetParent";
		}
	
		return pos_value;
	}
	
	function Hide_Window(pos){
	  document.getElementById(pos).style.visibility='hidden';
	  
	}
	
	function addMisconduct()
	{
		window.location='../goodconduct_misconduct/new2.php?student=<?=$AbsentStudentID?>';
	}
	
	function addPunishment()
	{
		window.location='../award_punishment/new2.php?student=<?=$AbsentStudentID?>';
	}
</script>

<br />
<div id="ref_list" style='position:absolute; height:150px; z-index:1; visibility: hidden;'></div>

<form name="form1" method="post">
<table width="97%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="navigation"><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?></td>
	</tr>
</table>
<table width="97%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="right">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right"><?=$SysMsg?>&nbsp;</td>
				</tr>
				<tr>
					<td height="28" align="right" valign="bottom">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left">&nbsp;</td>
							</tr>
						</table>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><?=$i_Discipline_Session?>: <?=$SessionString?>
								</td>
							</tr>
							<tr>
								<td align="right" valign="bottom">
								
								<? if(!empty($StudentID)) { ?>
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="21">
												<img src="<?="$image_path/$LAYOUT_SKIN"?>/table_tool_01.gif" width="21" height="23"></td>
											<td background="<?="$image_path/$LAYOUT_SKIN"?>/table_tool_02.gif">
												<table border="0" cellspacing="0" cellpadding="2">
													<tr>
														<!--<td nowrap> 
															<a href="#" class="tabletool">
																<img src="<?="$image_path/$LAYOUT_SKIN"?>/icon_photo.gif" width="12" height="12" border="0" align="absmiddle"><?=$i_Discipline_Display_Photos?></a></td>
														<td><img src="<?="$image_path/$LAYOUT_SKIN"?>/10x10.gif" width="5"></td>-->
														<? if(trim($AbsentStudentID)) {?>
														<td nowrap="">
																<a class="tabletool" href="#" onClick="javascript:addMisconduct();">
																	<img width="12" height="12" border="0" align="absmiddle" src="<?="$image_path/$LAYOUT_SKIN"?>/ediscipline/icon_misconduct.gif"/> <?=$Lang['eDiscipline']['AddMisconductForAbsent']?></a>
															</td>
														<td><img width="5" src="<?="$image_path/$LAYOUT_SKIN"?>/10x10.gif"/></td>
														<td nowrap="">
																<a class="tabletool" href="#" onClick="javascript:addPunishment();">
																	<img width="12" height="12" border="0" align="absmiddle" src="<?="$image_path/$LAYOUT_SKIN"?>/ediscipline/icon_demerit.gif"/> <?=$Lang['eDiscipline']['AddPunishmentForAbsent']?></a>
															</td>
														<td><img width="5" src="<?="$image_path/$LAYOUT_SKIN"?>/10x10.gif"/></td>
														<? } ?>
														<td nowrap>
															<a href="#" class="tabletool" onClick="javascript:absToPre();">
																<img src="<?="$image_path/$LAYOUT_SKIN"?>/icon_approve.gif" width="12" height="12" border="0" align="absmiddle"> <?=$i_Discipline_Set_Absent_To_Present?></a></td>
													</tr>
												</table>
											</td>
											<td width="6"><img src="<?="$image_path/$LAYOUT_SKIN"?>/table_tool_03.gif" width="6" height="23"></td>
										</tr>
									</table>
								<? } ?>
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
<?=$li->display();?>
		</td>
	</tr>
	<tr>
		<td height="1" class="dotline">
			<img src="<?="$image_path/$LAYOUT_SKIN"?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<? if(!empty($StudentID)) { ?>
	<tr>
		<td align="right">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="left" class="tabletextremark"><?=$i_Discipline_Last_Updated?>: <?=$LastUpdatedString?></td>
				</tr>
				<tr>
					<td height="50" align="center" class="tabletextremark">
<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "return checkForm()", "btnSubmit")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='calendar.php'")?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<? } ?>
</table>


<input type="hidden" name="task" id="task" value="" />
<input type="hidden" name="clickID" id="clickID" value="" />
<input type="hidden" name="did" id="did" value="<?=$did?>" />
<input type="hidden" name="studentid" id="studentid" value="<?=implode(",", $StudentID)?>" />
<input type="hidden" name="field" id="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="order" id="order" value="<?php echo $li->order; ?>"/>
<input type="hidden" name="pageNo" id="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
</form>
<br />
<script language="javascript">
	updateStyle();
</script>
<?
print $linterface->FOCUS_ON_LOAD("form1.btnSubmit");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
