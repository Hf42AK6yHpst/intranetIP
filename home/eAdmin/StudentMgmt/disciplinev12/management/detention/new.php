<?php
// Modifying by:

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Detention-New");

$CurrentPage = "Management_Detention";

$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($eDiscipline['Detention_Arrangement'], "", 1);
$TAGS_OBJ[] = array($eDiscipline['Session_Management'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/management/detention/settings/calendar.php", 0);

$PAGE_NAVIGATION[] = array($i_Discipline_Detention_List, "list.php");
$PAGE_NAVIGATION[] = array($i_Discipline_New_Student_Records, "");

$STEPS_OBJ[] = array($i_Discipline_Select_Student, 1);
$STEPS_OBJ[] = array($i_Discipline_Add_Record, 0);

$NoticeTemplateAva = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO("",1);
if (!$lnotice->disabled && !empty($NoticeTemplateAva))
	$STEPS_OBJ[] = array($i_Discipline_Enotice_Setting, 0);
else
	$STEPS_OBJ[] = array($button_finish, 0);

$student = $_POST['student'];
$flag = $_POST['flag'];
$targetClass = $_POST['targetClass'];
$targetNum = $_POST['targetNum'];
$student_login = $_POST['student_login'];

# Class selection
if ($flag != 2)
{
	$targetClass = "";
}
$lc = new libclass();
$select_class = $lc->getSelectClass("name='targetClass' onChange='changeClass(this.form)'", $targetClass);

if ($flag==2 && $targetClass != "")
{
	# Get Class Number
	$target_class_name = $targetClass;
	$sql = "SELECT DISTINCT ClassNumber, CONCAT(ClassNumber, ' (', ".getNamefieldByLang().", ')') FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (1) AND ClassName = '$target_class_name' ORDER BY ClassNumber";
	$classnum = $lc->returnArray($sql, 2);
	$select_classnum = $linterface->GET_SELECTION_BOX($classnum, "name='targetNum' ","",$targetNum);
	$select_classnum .= $linterface->GET_BTN($button_add, "button", "javascript:addByClassNum()");
}

if ($flag == 1 && $student_login != '')		# Login
{
	$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (1) AND (UserLogin = '$student_login' OR CardID = '$student_login')";
	$temp = $lc->returnVector($sql);
	$target_new_id = $temp[0];
	if ($target_new_id != "")
	{
		$student[] = $target_new_id;
	}
}
else if ($flag == 2 && $target_class_name != '' && $targetNum != '')
{
	$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND ClassName = '$target_class_name' AND ClassNumber = '$targetNum'";
	$temp = $lc->returnVector($sql);
	$target_new_id = $temp[0];
	if ($target_new_id != "")
	{
		$student[] = $target_new_id;
	}
}

# Last selection
if (is_array($student) && sizeof($student)>0)
{
	$list = implode(",", $student);
	$namefield = getNameFieldWithClassNumberByLang();
	# sort by class and class number
	$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE RecordType = 2 AND UserID IN ($list) ORDER BY ClassName ASC, ClassNumber ASC";
	$array_students = $lc->returnArray($sql,2);
}

$student_selected = $linterface->GET_SELECTION_BOX($array_students, "name='student[]' ID='student[]' class='select_studentlist' size='15' multiple='multiple'", "");
$button_remove_html = $linterface->GET_BTN($button_remove, "button", "javascript:checkOptionRemove(document.form1.elements['student[]'])");

$linterface->LAYOUT_START();

### start for searching ###
#get required data

$year = Get_Current_Academic_Year_ID();

$data_ary = array();
/*
$sql = "SELECT ClassID,ClassName FROM INTRANET_CLASS WHERE RecordStatus = 1 ORDER BY ClassName";
$result = $lc->returnArray($sql,2);
*/
$orderBy = ($intranet_session_language=="en") ? " ORDER BY ClassTitleEN" : " ORDER BY ClassTitleB5";
$sql = "SELECT YearClassID, ClassTitleEN, ClassTitleB5 FROM YEAR_CLASS WHERE AcademicYearID=$year $orderBy";
$result = $ldiscipline->returnArray($sql,3);

for ($i = 0; $i < sizeof($result); $i++)
{
	list($this_classid, $classnameEN, $classnameB5) = $result[$i];
	$this_classname = Get_Lang_Selection($classnameB5, $classnameEN);

	$name_field = getNameFieldByLang();
	//$sql1 = "SELECT UserID, $name_field, ClassNumber,UserLogin FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1 AND ClassName = '".$this_classname."' ORDER BY ClassNumber";
	$sql1 = "SELECT USR.UserID, $name_field, ycu.ClassNumber, USR.UserLogin FROM INTRANET_USER USR 
				LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID)
				WHERE USR.RecordType = 2 AND USR.RecordStatus = 1 AND ycu.YearClassID=$this_classid ORDER BY ycu.ClassNumber";
	
	$result1 = $lc->returnArray($sql1,4);
	for ($j = 0; $j < sizeof($result1); $j++)
	{
		list($this_userid, $this_stu_name, $this_class_number, $this_userlogin) = $result1[$j];
		$data_ary[] = array($this_userid, $this_classname, $this_class_number, $this_stu_name, $this_userlogin);
	}
}

if(!empty($data_ary))
{
	#define yui array (Search by input format )
	for($i=0;$i<sizeof($data_ary);$i++)
	{
		list($this_userid, $this_classname, $this_class_number, $this_stu_name, $this_userlogin) = $data_ary[$i];

		$temp_str = $this_classname . $this_class_number. " ". $this_stu_name;
		if($this_class_number)
			$temp_str2 = $this_stu_name . " (". $this_classname ."-". $this_class_number .")";
		else
			$temp_str2 = $this_stu_name;

		//($i == 0) ? $liList = "<li class=\"\" style=\"display: none;\">". $temp_str ."</li>\n" : $liList = "<li style=\"display: none;\">". $temp_str ."</li>\n";
		$liArr .= "[\"". $temp_str ."\", \"". $temp_str2 ."\", \"". $this_userid ."\"]";
		//($i == (sizeof($result)-1)) ? $liArr .= "" : $liArr .= ",\n";
		$liArr .= ",\n";
	}

	foreach ($data_ary as $key => $row)
	{
		$field1[$key] = $row[0];	//user id
		$field2[$key] = $row[1];	//class name
		$field3[$key] = $row[2];	//class number
		$field4[$key] = $row[3];	//stu name
		$field5[$key] = $row[4];	//login id
	}
	array_multisort($field5, SORT_ASC, $data_ary);

	#define yui array (Search by login id )
	for($i=0;$i<sizeof($data_ary);$i++)
	{
		list($this_userid, $this_classname, $this_class_number, $this_stu_name, $this_userlogin) = $data_ary[$i];

		if($this_class_number)
			$temp_str2 = $this_stu_name . " (". $this_classname ."-". $this_class_number .")";
		else
			$temp_str2 = $this_stu_name;

		$liArr2 .= "[\"". $this_userlogin ."\", \"". $temp_str2 ."\", \"". $this_userid ."\"]";
		($i == (sizeof($data_ary)-1)) ? $liArr2 .= "" : $liArr2 .= ",\n";
	}
	### end for searching
}
?>

<link type="text/css" rel="stylesheet" href="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/dpSyntaxHighlighter.css">

<style type="text/css">
	#statesmod {position:relative;}
	#statesautocomplete {position:relative;width:22em;margin-bottom:1em;}/* set width of widget here*/
	#statesautocomplete {z-index:9000} /* for IE z-index of absolute divs inside relative divs issue */
	#statesinput {_position:absolute;width:100%;height:1.4em;z-index:0;} /* abs for ie quirks */
	#statescontainer, #statescontainerCC, #statescontainerBCC {position:absolute;top:0.3em;width:100%}
	#statescontainer .yui-ac-content, #statescontainerCC .yui-ac-content, #statescontainerBCC .yui-ac-content {position:absolute;width:100%;border:1px solid #404040;background:#eeeeee;overflow:hidden;z-index:9050;}
	#statescontainer .yui-ac-shadow, #statescontainerCC .yui-ac-shadow, #statescontainerBCC .yui-ac-shadow {position:absolute;margin:.3em;width:100%;background:#a0a0a0;z-index:9049;}
	#statescontainer ul, #statescontainerCC ul, #statescontainerBCC ul {padding:5px 0;width:100%;}
	#statescontainer li, #statescontainerCC li, #statescontainerBCC li {padding:0 5px;cursor:default;white-space:nowrap;}
	#statescontainer li.yui-ac-highlight, #statescontainerCC li.yui-ac-highlight, #statescontainerBCC li.yui-ac-highlight {background:#bbbbbb;}
	#statescontainer li.yui-ac-prehighlight, #statescontainerCC li.yui-ac-prehighlight, #statescontainerBCC li.yui-ac-prehighlight {background:#FFFFFF;}


	#statesmod div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,form,fieldset,input,textarea,p,blockquote{margin:0;padding:0;}
	#statesmod table{border-collapse:collapse;border-spacing:0;}
	#statesmod fieldset,img{border:0;}
	#statesmod address,caption,cite,code,dfn,em,strong,th,var{font-style:normal;font-weight:normal;}
	#statesmod ol,ul {list-style:none;}
	#statesmod caption,th {text-align:left;}
	#statesmod h1,h2,h3,h4,h5,h6{font-size:100%;font-weight:normal;}
	#statesmod q:before,q:after{content:'';}
	#statesmod abbr,acronym {border:0;}

	#statesmod {font:13px arial,helvetica,clean,sans-serif;*font-size:small;*font:x-small;}
</style>

<script language="javascript">
function addByLogin()
{
	obj = document.form1;
	obj.flag.value = 1;
	generalFormSubmitCheck(obj);
}
function changeClass(obj)
{
	obj.flag.value = 2;
	if (obj.targetNum != undefined)
		obj.targetNum.selectedIndex = 0;
	generalFormSubmitCheck(obj);
}
function addByClassNum()
{
	obj = document.form1;
	obj.flag.value = 2;
	generalFormSubmitCheck(obj);
}
function finishSelection()
{
	obj = document.form1;
	obj.action = 'new2.php';
	checkOptionAll(obj.elements["student[]"]);
	obj.submit();
	return true;
}
function generalFormSubmitCheck(obj)
{
	checkOptionAll(obj.elements["student[]"]);
	obj.submit();
}
function formSubmit(obj)
{
	if (obj.flag.value == 0)
	{
		obj.flag.value = 1;
		generalFormSubmitCheck(obj);
		return true;
	}
	else
	{
		return finishSelection();
	}
}
function checkForm()
{
	obj = document.form1;

	if (obj.flag.value==1)
	{
		return addByLogin();
	}

	if(obj.elements["student[]"].length != 0)
		return formSubmit(obj);
	else
	{
		alert('<?=$i_Discipline_System_alert_PleaseSelectStudent?>');
		return false;
	}
}

</script>

<form name="form1" method="POST" onsubmit="return checkForm();" enctype="multipart/form-data" >

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td class="navigation"><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?><br /><br /><br /><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
</tr>
<tr>
	<td>
		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr>
			<td class="tabletext" width="40%"><?=$i_general_choose_student?></td>
			<td class="tabletext" ><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
			<td class="tabletext" width="60%"><?=$i_general_selected_students; ?></td>
		</tr>
		<tr>
			<td class="tablerow2" valign="top">
				<table width="100%" border="0" cellpadding="3" cellspacing="0">
				<tr>
					<td class="tabletext"><?=$i_general_from_class_group?></td>
				</tr>
				<tr>
					<td class="tabletext"><?= $linterface->GET_BTN($button_select, "button", "javascript:newWindow('../choose_student.php?fieldname=student[]', 9)")?></td>
				</tr>
				<tr>
					<td class="tabletext"><i><?=$i_general_or?></i></td>
				</tr>
				<tr>
					<td class="tabletext"><?=$i_general_search_by_inputformat?><br />
					<div id="statesautocomplete">
						<input type="text" class="tabletext" name="search1" ID="search1">
						<div id="statescontainer" style=" left:142px; top:0px;">
							<div style="display: none; width: 199px; height: 0px;" class="yui-ac-content">
								<div style="display: none;" class="yui-ac-hd"></div>
								<div class="yui-ac-bd"></div>
								<div style="display: none;" class="yui-ac-ft"></div>
							</div>
							<div style="width: 0pt; height: 0pt;" class="yui-ac-shadow"></div>
						</div>
					</div>
					</td>
				</tr>
				<tr>
					<td class="tabletext"><i><?=$i_general_or?></i></td>
				</tr>
				<tr>
					<td class="tabletext"><?=$i_general_search_by_loginid?><br />
					<div id="statesautocomplete">
						<input type="text" class="tabletext" name="search2" ID="search2">
						<div id="statescontainerCC" style=" left:142px; top:0px;">
							<div style="display: none; width: 199px; height: 0px;" class="yui-ac-content">
								<div style="display: none;" class="yui-ac-hd"></div>
								<div class="yui-ac-bd"></div>
								<div style="display: none;" class="yui-ac-ft"></div>
							</div>
							<div style="width: 0pt; height: 0pt;" class="yui-ac-shadow"></div>
						</div>
					</div>
					</td>
				</tr>
				</table>
			</td>
			<td class="tabletext" ><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
			<td align="left" valign="top">
				<table width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td align="left"><?= $student_selected ?></td>
				</tr>
				<tr>
					<td align="right"><?=$button_remove_html?></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center">
<?= $linterface->GET_ACTION_BTN($button_continue, "submit", "this.form.flag.value=3")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='./'")?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>

<br />
<input type="hidden" name="flag" value="0" />
</form>
<?
print $linterface->FOCUS_ON_LOAD("form1.search1");
$linterface->LAYOUT_STOP();
?>

<!-- Libary begins -->
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/yahoo.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/dom.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/event-debug.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/animation.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/autocomplete-debug-ip20.js"></script>
<!-- Library ends -->

<!-- In-memory JS array begins-->
<script type="text/javascript">
var statesArray = [
	<?= $liArr?>
];

var loginidArray = [
	<?= $liArr2?>
];

var delimArray = [
	";"
];
</script>
<!-- In-memory JS array ends-->


<script type="text/javascript">
YAHOO.example.ACJSArray = function() {
	var oACDS, oAutoComp;
	return {
		init: function() {

			// Instantiate first JS Array DataSource
			oACDS = new YAHOO.widget.DS_JSArray(statesArray);

			// Instantiate first AutoComplete
			oAutoComp = new YAHOO.widget.AutoComplete('search1','student[]', 'statescontainer', oACDS);
			oAutoComp.queryDelay = 0;
			oAutoComp.prehighlightClassName = "yui-ac-prehighlight";
			oAutoComp.useShadow = true;
			oAutoComp.minQueryLength = 0;

			oACDS2 = new YAHOO.widget.DS_JSArray(loginidArray);
			oAutoComp2 = new YAHOO.widget.AutoComplete('search2','student[]', 'statescontainerCC', oACDS2);
			oAutoComp2.queryDelay = 0;
			oAutoComp2.prehighlightClassName = "yui-ac-prehighlight";
			oAutoComp2.useShadow = true;
			oAutoComp2.minQueryLength = 0;
		},

		validateForm: function() {
			// Validate form inputs here
			return false;
		}
	};
}();

YAHOO.util.Event.addListener(this,'load',YAHOO.example.ACJSArray.init);
</script>

<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/dpSyntaxHighlighter.js"></script>
<script type="text/javascript">
dp.SyntaxHighlighter.HighlightAll('code');
</script>