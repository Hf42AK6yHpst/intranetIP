<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html("popup.html");
$ldiscipline = new libdisciplinev12();

$stdInfo = $ldiscipline->getStudentNameByID($uid);

list($tempID, $tempName, $tempClsName, $tempClsNo) = $stdInfo[0];
$stdName = "$tempName ($tempClsName-$tempClsNo)";

//$APCategory = $ldiscipline->getAPCategoryInfo(-1);
$sql = "SELECT TagID, TagName FROM DISCIPLINE_ITEM_TAG_SETTING ORDER BY TagID";
$TagAry = $ldiscipline->returnArray($sql, 2);

$sql = "SELECT conductString, UserID FROM DISCIPLINE_MS_STUDENT_CONDUCT WHERE Year='$year' AND Semester='$semester' AND StudentID=$uid";
$data = $ldiscipline->returnArray($sql, 2);

for($i=0; $i<sizeof($data); $i++) {
	$teacherID = $data[$i][1];
	$tempGrade = split(",",$data[$i][0]);
	for($j=0; $j<sizeof($tempGrade); $j++) {
		$g = $tempGrade[$j];
		$line = split(":",$g);
		list ($catid, $gradeNew) = $line;
		if($catid!="" && $uid!="")	
			$conductGrade[$teacherID][$catid] = $gradeNew;
	}
}

$MODULE_OBJ['title'] = $iDiscipline['OriginalResult'];

# Start layout
$linterface->LAYOUT_START();

?>
<table width="100%" cellpadding="4" cellspacing="0" border="0">
	<tr>
		<td colspan="3">
			<table width="100%" cellpadding="4" cellspacing="0" border="0">
				<tr>
					<td width="40%" class="formfieldtitle"><?=$i_Discipline_System_Conduct_School_Year?></td>
					<td width="60%" class="tablerow1"><?=getCurrentAcademicYear()?></td>
				</tr>
				<tr>
					<td class="formfieldtitle"><?=$i_Discipline_System_Conduct_Semester?></td>
					<td class="tablerow1"><?=$semester?></td>
				</tr>
				<tr>
					<td class="formfieldtitle"><?=$i_Discipline_Student?></td>
					<td class="tablerow1"><?=$stdName?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<table width="100%" cellpadding="4" cellspacing="0" border="0">
				<tr>
					<td class='tabletext' colspan="4"></td>
					<td class='tabletext' colspan="4"></td>
				</tr>
				<tr class="tablebluetop">
					<td class='tabletoplink'>&nbsp;</td>
		<?
		for($i=0; $i<sizeof($TagAry); $i++) {
			echo "<td class='tablebluetop tabletoplink'>".intranet_htmlspecialchars($TagAry[$i]['TagName'])."</td>";	
		}
		?>
				</tr>
	<?
	$stdInfo = $ldiscipline->getStudentNameByID($uid);
	$yearID = Get_Current_Academic_Year_ID();
	$name_field = getNameFieldByLang('USR.');
	$sql = "SELECT USR.UserID, $name_field as name FROM INTRANET_USER USR LEFT OUTER JOIN 
				YEAR_CLASS_TEACHER CLSTCH ON (USR.UserID=CLSTCH.UserID) LEFT OUTER JOIN 
				SUBJECT_TERM_CLASS_TEACHER SUBTCH ON (USR.UserID=SUBTCH.UserID) LEFT OUTER JOIN 
				SUBJECT_TERM_CLASS_YEAR_RELATION stcyr ON (stcyr.SubjectGroupID=SUBTCH.SubjectGroupID) LEFT OUTER JOIN
				YEAR_CLASS CLS ON (CLS.YearClassID=CLSTCH.YearClassID AND CLS.AcademicYearID=$yearID) LEFT OUTER JOIN
				YEAR y ON (CLS.YearID=y.YearID OR stcyr.YearID=y.YearID)
			WHERE 
				(CLS.ClassTitleB5='".$stdInfo[0]['ClassName']."' OR CLS.ClassTitleEN='".$stdInfo[0]['ClassName']."') AND 
				USR.RecordType=1
			GROUP BY USR.UserID
			ORDER BY name
			";
			
	$data = $ldiscipline->returnArray($sql, 2);

	for($i=0; $i<sizeof($data); $i++) {
		$css = ($i%2==0) ? 1 : 2;
		echo "<tr class='tablebluerow$css'><td class='tabletext'>".$data[$i][1]."</td>";
		for($j=0; $j<sizeof($TagAry); $j++) {
			echo "<td class='tabletext'>".$conductGrade[$data[$i][0]][$TagAry[$j]['TagID']]."</td>";	
		}
		echo "</tr>";	
	}
	?>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3" align="center"><?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();")?></td>
	</tr>
</table>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
