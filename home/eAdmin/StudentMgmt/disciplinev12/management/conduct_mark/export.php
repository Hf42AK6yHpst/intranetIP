<?php

#################### Change Log #####################
## 
##	Date	:	2017-10-13 (Bill)	[2017-1004-0907-59206]
##	Details :	Support Total Gain & Total Deduct column including Manual Adjustment	($sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns_IncludeAdjust'])
##
##	Date 	: 	20100211 (Henry)
##	Details :	Modified the query by using left outer join instead of inner join when joining DISCIPLINE_STUDENT_CONDUCT_BALANCE
## 
##	Date 	: 	20100202 (Ronald)
##	Details :	Modified the query by using from left outer join to inner join when joining INTRANET_USER
##
#####################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
 
# Temp assign memory of this page
ini_set("memory_limit", "150M"); 

$ldiscipline = new libdisciplinev12();
$lexport = new libexporttext();

$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Conduct_Mark-View");

$ExportArr = array();

$extraCriteria .= ($ClassID=='') ? "" : " AND ycu.YearClassID=$ClassID";
$extraCriteria .= ($searchStr=='')? "" : " AND (iu.EnglishName like '%".trim($searchStr)."%' or iu.ChineseName like '%".trim($searchStr)."%' )";

$conds_year = ($AcademicYearID=='0')? "": " AND (con_bal.AcademicYearID='$AcademicYearID')";
$conds_semester = ($YearTermID=='0' || $YearTermID=='')? "": " AND (con_bal.IsAnnual = '1')";

$clsName = ($intranet_session_language=="en") ? "yc.ClassTitleEN" : "yc.ClassTitleB5";

### Note: Changed to use from left outer join to inner join ###
$sql = "SELECT 
			$clsName as ClassName,
			".getNameFieldByLang("iu.")." as UserName,
			1 as Current_Mark,
			ROUND(SUM(con_adj.AdjustMark)) as AdjustMark,
			1 as Adjusted_Mark,
			1 as GradeChar,
			DATE_FORMAT(MAX(con_adj.DateInput), '%Y-%m-%d') as LastUpdate,
			iu.UserID,
			'$Year' as Year,
			'".$Semester."'as Semester,
			1 as DateInput,
			COUNT(con_adj.AdjustMark),
			ycu.ClassNumber
		FROM INTRANET_USER iu
		LEFT OUTER JOIN DISCIPLINE_STUDENT_CONDUCT_BALANCE con_bal on (iu.UserID = con_bal.StudentID $conds_semester $conds_year)
		LEFT OUTER JOIN DISCIPLINE_CONDUCT_ADJUSTMENT con_adj on (iu.UserID = con_adj.studentID)
		LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID = iu.UserID)
		LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID = ycu.YearClassID)
		WHERE
			1 $extraCriteria AND iu.RecordType=2 AND iu.RecordStatus IN (0, 1, 2) AND 
			yc.YearClassID != 0 AND yc.AcademicYearID = $AcademicYearID
		GROUP BY iu.UserID
		ORDER BY $clsName, ycu.ClassNumber ";
$row = $ldiscipline->returnArray($sql,9);

# Get Conduct Base Mark
$BaseConductScore = $ldiscipline->getConductMarkRule('baseMark');

# Year Term Condition
$YearTermCond = " AND YearTermID IN ('') ";
if($ldiscipline->retriveConductMarkCalculationMethod() == 0) {
	// do nothing
}
else {
	$TermStartDate = getStartDateOfAcademicYear($AcademicYearID, $YearTermID);
	$sql = "SELECT YearTermID FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '".$AcademicYearID."' AND TermStart <= '$TermStartDate' ORDER BY TermStart DESC";
	$result = $ldiscipline->returnVector($sql);
	if(sizeof($result)>0) {
		$targetTermID = implode(",", $result);
		$YearTermCond = " AND YearTermID IN ($targetTermID) ";
	}
}

# Build CSV data
for($i=0; $i<sizeof($row); $i++)
{
    if ($YearTermID=='' || $YearTermID==0)
    {
         $cond = ' and IsAnnual = 1';
         $cond2 = '';
    }
    else
    {
	     $cond = ' and YearTermID = \''.$YearTermID.'\'';
	     $cond2 = ' AND YearTermID = \''.$YearTermID.'\'';
    }
    
    # Get Student Conduct Score Balance & Grade
    $sql = "SELECT ConductScore, GradeChar, Semester
				FROM DISCIPLINE_STUDENT_CONDUCT_BALANCE 
			WHERE StudentID = ".$row[$i]['UserID']." AND AcademicYearID = '".$AcademicYearID."' $cond";
    $tmpArr = $ldiscipline->returnArray($sql,1);
    if(sizeof($tmpArr)>0)
    {
 		$ConductScore = $tmpArr[0]['ConductScore'];
 		$Grade = $tmpArr[0]['GradeChar'];
 		/*
	    $sql = "SELECT SUM(ConductScoreChange) FROM DISCIPLINE_MERIT_RECORD 
					WHERE StudentID = ".$row[$i]['UserID']." AND AcademicYearID = ".$AcademicYearID." AND RecordStatus = ".DISCIPLINE_STATUS_APPROVED." AND ReleaseStatus = ".DISCIPLINE_STATUS_RELEASED." $cond";
	    $tmpArr2 = $ldiscipline->returnVector($sql);
	    $sum = $tmpArr2[0];
	    */
	}
	else
	{
		$ConductScore = $BaseConductScore;
		$Grade = "";
	}
	
	if($ldiscipline->retriveConductMarkCalculationMethod() == 0)
	{
		# Get Student Gain / Deduct Conduct Mark
		if($sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns'])
		{
			# Total Gain
	    	$tmpSql1 = "SELECT SUM(ConductScoreChange) FROM DISCIPLINE_MERIT_RECORD WHERE MeritType=1 AND ReleaseStatus=1 AND RecordStatus=1 AND StudentID=".$row[$i]['UserID']." AND AcademicYearID='".$AcademicYearID."' $cond2";
	    	$tmpResult = $ldiscipline->returnVector($tmpSql1);	
	    	$totalGain = $tmpResult[0];
	    	
	    	# Total Deduct
	    	$tmpSql1 = "SELECT SUM(ConductScoreChange) FROM DISCIPLINE_MERIT_RECORD WHERE MeritType=-1 AND ReleaseStatus=1 AND RecordStatus=1 AND StudentID=".$row[$i]['UserID']." AND AcademicYearID='".$AcademicYearID."' $cond2";
	    	$tmpResult = $ldiscipline->returnVector($tmpSql1);	
	    	$totalDeduct = $tmpResult[0];
												
			// [2016-1207-1221-39240]
			# Update Total Gain / Deduct with Manual Adjustment
			if($sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns_IncludeAdjust'])
			{
				# Total Adjustment Gain
				$tmpSql1 = "SELECT SUM(AdjustMark) FROM DISCIPLINE_CONDUCT_ADJUSTMENT WHERE StudentID = ".$row[$i]['UserID']." AND AcademicYearID = '".$AcademicYearID."' $cond AND AdjustMark > 0";
				$tmpResult = $ldiscipline->returnVector($tmpSql1);
				$adjustTotalGain = $tmpResult[0];
				$totalGain += $adjustTotalGain;
				
				# Total Adjustment Deduct
				$tmpSql2 = "SELECT SUM(AdjustMark) FROM DISCIPLINE_CONDUCT_ADJUSTMENT WHERE StudentID = ".$row[$i]['UserID']." AND AcademicYearID = '".$AcademicYearID."' $cond AND AdjustMark < 0";
				$tmpResult = $ldiscipline->returnVector($tmpSql2);
				$adjustTotalDeduct = $tmpResult[0];
				$totalDeduct += $adjustTotalDeduct;
			}
	    }
	    
	    # Get Student Manual Adjustment
		$sql = "SELECT AdjustMark FROM DISCIPLINE_CONDUCT_ADJUSTMENT 
				WHERE StudentID = ".$row[$i]['UserID']." AND AcademicYearID = '".$AcademicYearID."' $cond";
		$tmpArr = $ldiscipline->returnArray($sql);
	}
	else
	{
		# Get Student Gain / Deduct Conduct Mark
		if($sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns']) 
		{ 
			# Total Gain
	    	$tmpSql1 = "SELECT SUM(ConductScoreChange) FROM DISCIPLINE_MERIT_RECORD WHERE MeritType=1 and ReleaseStatus=1 AND RecordStatus=1 AND StudentID=".$row[$i]['UserID']." AND AcademicYearID='".$AcademicYearID."'";
	    	$tmpResult = $ldiscipline->returnVector($tmpSql1);	
	    	$totalGain = $tmpResult[0];
	    	
	    	# Total Deduct
	    	$tmpSql1 = "SELECT SUM(ConductScoreChange) FROM DISCIPLINE_MERIT_RECORD WHERE MeritType=-1 and ReleaseStatus=1 AND RecordStatus=1 AND StudentID=".$row[$i]['UserID']." AND AcademicYearID='".$AcademicYearID."'";
	    	$tmpResult = $ldiscipline->returnVector($tmpSql1);	
	    	$totalDeduct = $tmpResult[0];
			
			// [2016-1207-1221-39240]
			# Update Total Gain / Deduct with Manual Adjustment
			if($sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns_IncludeAdjust'])
			{
				# Total Adjustment Gain
				$tmpSql1 = "SELECT SUM(AdjustMark) FROM DISCIPLINE_CONDUCT_ADJUSTMENT WHERE StudentID = ".$row[$i]['UserID']." AND AcademicYearID = '".$AcademicYearID."' $YearTermCond AND AdjustMark > 0";
				$tmpResult = $ldiscipline->returnVector($tmpSql1);
				$adjustTotalGain = $tmpResult[0];
				$totalGain += $adjustTotalGain;
				
				# Total Adjustment Deduct
				$tmpSql2 = "SELECT SUM(AdjustMark) FROM DISCIPLINE_CONDUCT_ADJUSTMENT WHERE StudentID = ".$row[$i]['UserID']." AND AcademicYearID = '".$AcademicYearID."' $YearTermCond AND AdjustMark < 0";
				$tmpResult = $ldiscipline->returnVector($tmpSql2);
				$adjustTotalDeduct = $tmpResult[0];
				$totalDeduct += $adjustTotalDeduct;
			}
	    }
		
	    # Get Student Manual Adjustment
		$sql = "SELECT AdjustMark FROM DISCIPLINE_CONDUCT_ADJUSTMENT 
					WHERE StudentID = ".$row[$i]['UserID']." AND AcademicYearID = '".$AcademicYearID."' $YearTermCond";
		$tmpArr = $ldiscipline->returnArray($sql);
	}
	
	# Display - Conduct Score Balance
	if(sizeof($tmpArr)>0)
	{
		$score = 0;
		for($y=0; $y<sizeof($tmpArr); $y++)
		{
			$score += $tmpArr[$y]['AdjustMark'];
		}
    }
	else
	{
		$score = '--';	
	}
	
	# Display - Conduct Score Grade
	if($Grade=='')
	{
		$Grade = '--'; 
	}
	
	# Display - Last Update
	if(sizeof($tmpArr)==0 || $row[$i]['AdjustMark']=='--'|| trim($row[$i]['AdjustMark']==''))
	{
		$LastUpdate='--';
	}
	else
	{
		$LastUpdate = $ldiscipline->convertDatetoDays($row[$i]['LastUpdate']);
    }
    
 	$a = 0;
 	$ExportArr[$i][$a] = $row[$i]['ClassName'];	$a++;			// Class Name
 	$ExportArr[$i][$a] = $row[$i]['ClassNumber'];	$a++;		// Class Number
	$ExportArr[$i][$a] = $row[$i]['UserName'];	$a++;			// Student Name
	$ExportArr[$i][$a] = $ConductScore;	$a++;					// Conduct Score
	
	if($sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns']) {
		$ExportArr[$i][$a] = $totalGain ? $totalGain : '-';	$a++; 		// Total Gain
		$ExportArr[$i][$a] = $totalDeduct ? $totalDeduct : '-';	$a++; 	// Total Deduct
	}
	
	$ExportArr[$i][$a] = $score; $a++;							// Adjustment
	$ExportArr[$i][$a] = $ConductScore + $score; $a++; 			// Adjusted Conduct Mark
	$ExportArr[$i][$a] = $Grade; $a++; 							// Conduct Grade
	$ExportArr[$i][$a] = $LastUpdate; $a++; 					// Last Update
}

# CSV Columns
$exportColumn = array($i_ClassName, $i_ClassNumber, $i_general_name, $Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['SystemGenerated']);
if($sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns']) { 
	$exportColumn = array_merge($exportColumn, array($Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['TotalGain'], $Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['TotalDeduct']));
}
$exportColumn = array_merge($exportColumn, array($Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['ManuallyAdjusted'], $Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['ConsolidatedMark'], $i_Discipline_System_Report_ClassReport_Grade,$i_Discipline_System_Discipline_Conduct_Last_Updated));

$export_content .= $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

# Output the file to user browser
$filename = 'conduct_mark.csv';
$lexport->EXPORT_FILE($filename, $export_content);

?>