<?php
// Modifying by:
/*
 * Change Log:
 * Date 2020-11-24 Bill  pass AcademicYearID to getTermIDByTermName() to ensure return correct YearTermID   [IP30 DM#1014]
 * Date 2019-05-02 Bill  Hidden Fields > Change 'UniqueID' to 'UniqueIndex' to prevent IntegerSafe()    [EJ DM#1200]
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");

intranet_auth();
intranet_opendb();

/*
if(!is_array($RecordID) || sizeof($RecordID)==0) {
	header("Location: index.php");	
	exit();
}
*/

$linterface = new interface_html();
$ldisciplinev12 = new libdisciplinev12();

$ldisciplinev12->CONTROL_ACCESS("Discipline-MGMT-Conduct_Mark-Adjust");

//$yearID = $ldisciplinev12->getAcademicYearIDByYearName($Year);
$yearID = $Year;

# Get semester data from file, prepare semester conversation from number to text. ie: 1 -> First semester
if(!is_numeric($Semester))
{
    // [IP30 DM#1014]
    //$SemesterNum = $ldisciplinev12->getTermIDByTermName($Semester);
	$SemesterNum = $ldisciplinev12->getTermIDByTermName($Semester, $yearID);
}
else
{
	$SemesterNum = $Semester;	
}
if($Semester=='' || $Semester=='IsAnnual' || $Semester==0)  // IS Annual
{
	$SemesterNum = 'IsAnnual';
}

if(is_array($RecordID) && sizeof($RecordID) > 0)
{
	$js = "var tb_num = new Array(".sizeof($RecordID).");\n";
	$js .= "var select_num = new Array(".sizeof($RecordID).");\n";
	for($a=0;$a<sizeof($RecordID);$a++)
	{
		$js .= "tb_num[$a] = \"tb_".$RecordID[$a]."\";\n";
		$js .= "select_num[$a] = \"select_".$RecordID[$a]."\";\n";
		
		$tmpArr = explode(",",$RecordID[$a]);
		list($StudentArr[$a]['UserID'], $StudentArr[$a]['Year'], $StudentArr[$a]['Semester']) = $tmpArr;
		$IDArr[] = $StudentArr[$a]['UserID'];
		$HiddenValue .= "<input type=\"hidden\" name=\"UniqueIndex[]\" id=\"UniqueIndex[]\" value=\"".$RecordID[$a]."\" >";
	}
	
	for($x=0; $x<sizeof($StudentArr); $x++)
	{
		/*if($x!=0)
		{
			$cond .=" or ";
		}
		$cond .= "  (iu.UserID = ".$StudentArr[$x]['UserID']." ) ";	*/
		
		
		$StudentIDArr[] = $StudentArr[$x]['UserID'];
	}
	
	$cond .= " AND iu.UserID IN (".implode(",",$StudentIDArr).")";
	$SemesterStr = $Semester;
}
else if($StudentID!='')
{
	$IDs = $StudentID;
	if($SemesterStr=='')$SemesterStr = $ldisciplinev12->convertSemesterNumToString($Semester);
	
	$js = "var tb_num = new Array(1);\n";
	$js .= "var select_num = new Array(1);\n";
	$js.= "tb_num[0] = \"tb_".$StudentID.",$Year,$SemesterNum\";\n";
	$js.= "select_num[0] = \"select_".$StudentID.",$Year,$SemesterNum\";\n";
	
	$HiddenValue .= "<input type=\"hidden\" name=\"UniqueIndex[]\" id=\"UniqueIndex[]\" value=\"".$StudentID.",$Year,$SemesterNum\" >";
	//$cond = " con_bal.StudentID = ".$StudentID." and con_bal.Semester = '".$SemesterStr."' and con_bal.Year = '".$Year."'";
	$cond = " and iu.UserID = ".$StudentID;
}
//$cond .=" and Semester = '$SemesterStr' and Year = '$Year'";

# Prepare reason array for reason selection box
$lf = new libwordtemplates();
$word_array = $lf->word_array;
$merit_wording = $lf->getWordListMerit();
$demerit_wording = $lf->getWordListDemerit();
for($a=0; $a<sizeof($merit_wording); $a++)
{
	$reasonArr[] = array($merit_wording[$a],$merit_wording[$a]);	
}
for($a=0; $a<sizeof($demerit_wording); $a++)
{
	$reasonArr[] = array($demerit_wording[$a],$demerit_wording[$a]);	
}

$reasonTB = "<input type=\"textbox\" id=\"select_',iu.UserID,',$Year,$SemesterNum\" name=\"select_',iu.UserID,',$Year,$SemesterNum\">";

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

/*if($Semester=='')
	{
		//$con_on_statement = "on (con_bal.StudentID = con_adj.StudentID and con_bal.Year = con_adj.Year and con_bal.IsAnnual = con_adj.IsAnnual)";
		$con_on_statement = "on (iu.UserID = con_adj.StudentID and if (con_bal.StudentID is not null,con_bal.Year = con_adj.Year,1) and
		if (con_bal.StudentID is not null, con_bal.IsAnnual = con_adj.IsAnnual,1)";
		$merit_on_statement = "on (merit_rec.StudentID = con_bal.StudentID and merit_rec.Year = con_bal.Year)";
	}
	else
	{
		//$con_on_statement = "on (con_bal.StudentID = con_adj.StudentID and con_bal.Year = con_adj.Year and con_bal.Semester = con_adj.Semester)";
		$con_on_statement = "on (iu.UserID = con_adj.StudentID and if (con_bal.StudentID is not null, con_bal.Year = con_adj.Year ,1) and 
		if (con_bal.StudentID is not null, con_bal.Semester = con_adj.Semester,1))";
		$merit_on_statement = "on (merit_rec.StudentID = con_bal.StudentID and merit_rec.Year = con_bal.Year and merit_rec.Semester = con_bal.Semester)";
	}
*/
$sql = "SELECT 
			CONCAT(".Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN').", '-', ycu.ClassNumber) as ClassNameNum, 
			".getNameFieldByLang("iu.")." as UserName,
			1 as Current_Mark,
			ROUND(SUM(con_adj.AdjustMark)) as AdjustMark,
			CONCAT('<input name=\"tb_', iu.UserID, ',$Year,$SemesterNum\" id=\"tb_', iu.UserID, ',$Year,$SemesterNum\" type=\"text\" class=\"formtextbox\" size=\"3\">'),
			1 as Adjusted_Mark,
			CONCAT('$reasonTB'),
            iu.UserID,
			'$Year' as AcademicYearID,
			'$SemesterStr' as  YearTermID,
			COUNT(con_adj.AdjustMark) as count_adjmark
		FROM INTRANET_USER iu
			LEFT JOIN DISCIPLINE_STUDENT_CONDUCT_BALANCE con_bal ON (iu.UserID = con_bal.StudentID $conds_semester $conds_year)
			LEFT OUTER JOIN DISCIPLINE_CONDUCT_ADJUSTMENT con_adj ON (iu.UserID = con_adj.studentID)
			LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=iu.UserID)
			LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)
		WHERE 
            1 $cond AND 
            iu.RecordType=2 AND iu.RecordStatus=1 AND 
            yc.YearClassID != 0 AND yc.AcademicYearID = '$yearID' GROUP BY iu.UserID";

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("CONCAT(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', 1)), IF(INSTR(ClassNameNum, '-') = 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', -1)), 10, '0')))", "UserName", "ConductScore", "AdjustMark", "AdjustedMark", "UserName", "UserName");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
$li->column_array = array(0,0,0);
$li->wrap_array = array(0,0,0);
$li->IsColOff = "eDisciplineConductMarkAdjust";
//echo $li->built_sql();

$pos = 0;
$li->column_list .= "<td width='1' class='tablegreentop tabletoplink'>#</td>\n";
$li->column_list .= "<td width='' class='tablegreentop tabletoplink'>".$i_general_class."</td>\n";
$li->column_list .= "<td width='25%' class='tablegreentop tabletoplink'>".$i_general_name."</td>\n";
//$li->column_list .= "<td width='' class='tablegreentop tabletoplink'>".$i_Discipline_System_Conduct_Mark_Pre_Adjustment."</td>\n";
//$li->column_list .= "<td width='' class='tablegreentop tabletoplink'>".$i_Discipline_System_Conduct_Mark_Acc_Adjustment."</td>\n";
$li->column_list .= "<td width='' class='tablegreentop tabletoplink'>".$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['SystemGenerated']."</td>\n";
$li->column_list .= "<td width='' class='tablegreentop tabletoplink'>".$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['ManuallyAdjusted']."</td>\n";
$li->column_list .= "<td width='' class='tablegreentop tabletoplink'>".$i_Discipline_System_Discipline_Conduct_This_Adjustment."</td>\n";
//$li->column_list .= "<td width='' class='tablegreentop tabletoplink'>".$i_Discipline_System_Conduct_Mark_Current_Conduct_Mark."</td>\n";
$li->column_list .= "<td width='' class='tablegreentop tabletoplink'>".$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['ConsolidatedMark']."</td>\n";
$li->column_list .= "<td width='' class='tablegreentop tabletoplink'>".$i_Discipline_Reason."</td>\n";

# Top menu highlight setting
$CurrentPage = "Management_ConductMark";
$TAGS_OBJ[] = array($i_Discipline_System_Access_Right_Conduct_Mark);

$PAGE_NAVIGATION[] = array($i_Discipline_System_Conduct_Mark_Student_List, "index.php");
$PAGE_NAVIGATION[] = array($i_Discipline_System_Conduct_Mark_Adjust_Conduct_Mark);

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldisciplinev12->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
<?=$js?>

var cordX = "";
var cordY = "";

function change_adjust(mark)
{
	for(var i=0; i<tb_num.length; i++)
	{
		document.getElementById(tb_num[i]).value=mark;
	}	
}

function change_reason(reason)
{
	for(var i=0; i<select_num.length; i++)
	{
		//if(document.getElementById(select_num[i]).value=='')
		//{
			document.getElementById(select_num[i]).value=reason;
		//}
	}
}

<!--
function removeCat(obj,element,page){
	var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
    if(countChecked(obj,element)==0) {
        alert(globalAlertMsg2);
    }
    else {
 	   if(confirm(alertConfirmRemove)){
            obj.action=page;
            obj.method="post";
            obj.submit();
        }
    }
}
//-->

document.onmousedown = function(){
var e = arguments[0] || event;
//window.status = 'X: ' + e.clientX + '\nY: ' + e.clientY

cordX = e.clientX;
cordY = e.clientY;
}

function Show_Merit_Window(StudentID,Year,Semester)
{
    obj = document.form1;
    obj.targetDivID.value = StudentID+Year+Semester;
    obj.ajaxStudentID.value = StudentID;
    obj.ajaxYear.value = Year;
    obj.ajaxSemester.value = Semester;
    obj.ajaxType.value = 'Merit';
    clickDivID = 'merit_'+StudentID+Year+Semester;
    targetDivID = StudentID+Year+Semester;
    YAHOO.util.Connect.setForm(obj);
    
    var path;
    path = "ajax_conduct.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback2);
}

function Show_Adj_Window(StudentID,Year,Semester)
{
    obj = document.form1;
    obj.targetDivID.value = StudentID+Year+Semester;
    obj.ajaxStudentID.value = StudentID;
    obj.ajaxYear.value = Year;
    obj.ajaxSemester.value = Semester;
    obj.ajaxType.value = 'Adjust';
    clickDivID = 'adj_'+StudentID+Year+Semester;
    targetDivID = StudentID+Year+Semester;
    YAHOO.util.Connect.setForm(obj);
    
    var path;
    path = "ajax_conduct.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback2);
}

var callback2 = {
    success: function ( o )
    {
		DisplayDefDetail(o.responseText);
	}
}

function DisplayDefDetail(text){
	document.getElementById('div_form').innerHTML=text;
	DisplayPosition();
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}
	
	return pos_value;
}

function DisplayPosition(){
	document.getElementById(targetDivID).style.left=getPosition(document.getElementById(clickDivID),'offsetLeft') +10;
  	document.getElementById(targetDivID).style.top=getPosition(document.getElementById(clickDivID),'offsetTop') +10;
  	document.getElementById(targetDivID).style.visibility='visible';
}

function Hide_Window(pos){
  document.getElementById(pos).style.visibility='hidden';
}

function checkForm(obj) 
{
	for(var a=0; a<tb_num.length;a++)
	{
		if(!check_text(document.getElementById(tb_num[a]),'<?=$i_Discipline_System_Discipline_Conduct_JS_alert?>'))
		{
			return false
		}
	}
	
	var boxes = document.getElementsByTagName("input"); 
	for(var i = 0; i < boxes.length; i++) 
	{
		if(boxes[i].type == 'text' && boxes[i].name!='master_adjust' && boxes[i].name.substr(0,3)=='tb_') 
		{
			<? if($sys_custom['eDiscipline']['ConductMark1DecimalPlace']) {?>
    			if(isNaN(boxes[i].value))
    			{
    				alert("<?=$Lang['eDiscipline']['AlertMsgNumericInput']?>");
    				return false;
    			}
			<? } else { ?>
    			if(!isInteger(boxes[i].value))
    			{
    				alert("<?=$i_Discipline_System_Discipline_Conduct_Mark_Score_Integer_JS_alert?>");
    				return false;
    			}
			<? } ?>
		}
	}
	
	return true
}

function isInteger(sText)
{
	var ValidChars = "0123456789-";
	var Char;
	
	sText = sText.toLowerCase();
	for (i = 0; i < sText.length; i++)
    {
        Char = sText.charAt(i);
        if (ValidChars.indexOf(Char) == -1)
        {
	        return false;
        }
    }
    return true;
}

function confirmDeleteAdjust(RecordID, StudentID, Year, Semester) {
	Remove_Adjust_Window(RecordID, StudentID, Year, Semester);
	//Show_Adj_Window(StudentID,Year,Semester);
}

function Remove_Adjust_Window(RecordID, StudentID, Year, Semester)
{
	if(confirm("<?=$i_Discipline_System_alert_remove_warning_level?>"))
	{
		clickDivID = 'adj_'+StudentID+Year+Semester;
		
		$.post(
			"ajax_conduct.php", 
			{ 
				Action: "Edit",
				DivID: encodeURIComponent('adj_'+StudentID+Year+Semester),
				targetDivID: StudentID+Year+Semester,
				ajaxStudentID: StudentID,
				ajaxYear: Year,
				ajaxSemester: Semester,
				ajaxType: 'Remove_Adjust',
				ajaxRecordID: RecordID
			},
			function(ReturnData)
			{
				//alert(ReturnData);
				if(ReturnData==1) {
					self.location.reload();
				} else {
					Get_Return_Message("<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>")
				}
			}
		);
	}
}
</script>

<br />
<form name="form1" method="post" action="adjust_update.php" onSubmit="return checkForm(form1)">

			<table width="98%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr><td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
						<td align="right"><?= $linterface->GET_SYS_MSG($xmsg) ?></td></tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="10px"></td>
				</tr>
				<tr><td>
				<tr>
					<td><?= $li->display() ?>
    					<div id="div_form"></div>
    					<?=$adjDivHTML?>
    					<?=$meritHTML?>
					</td>
				</tr>
				<tr>
					<td height="10px"></td>
				</tr>
				<tr>
					<td colspan="2" class="dotline"><img src="<?=$image_path;?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td height="10px"></td>
				</tr>
				<tr><td colspan="2" align="center">
					<? echo $linterface->GET_ACTION_BTN($button_submit, "submit"); ?>
					<? echo $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\""); ?>
					<? echo $linterface->GET_ACTION_BTN($button_back, "button", "","back"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" onClick=\"history.back()\""); ?>
				</td></tr>
				</table><br>
				
				<?=$HiddenValue?>
				<input type="hidden" name="targetDivID" id="targetDivID" />
				<input type="hidden" name="ajaxStudentID" id="ajaxStudentID" />
				<input type="hidden" name="ajaxYear" id="ajaxYear" />
				<input type="hidden" name="ajaxSemester" id="ajaxSemester" />
				<input type="hidden" name="ajaxType" id="ajaxType" />
				<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
				<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
				<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
				<input type="hidden" name="page_size_change" value="" />
				<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
				<input type="hidden" name="searchStr" id="searchStr" value="<?=$searchStr?>" />
				<input type="hidden" name="Year" id="Year" value="<?=$Year?>" />
				<input type="hidden" name="YearID" id="YearID" value="<?=$yearID?>" />
				<input type="hidden" name="Semester" id="Semester" value="<?=$Semester?>" />
				<input type="hidden" name="SemesterStr" id="SemesterStr" value="<?=$SemesterStr?>" />
				<input type="hidden" name="ClassID" id="ClassID" value="<?=$ClassID?>" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>