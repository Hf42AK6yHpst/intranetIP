<?php
// using:
/*
 * 2019-05-13 (Bill)  : Prevent SQL Injection
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

// if without the sys_cust, redirect
if(!$sys_custom['eDiscipline']['WSCSS_Conduct_Grade']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit;	
}

$linterface = new interface_html();
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Conduct_Mark-Adjust");

### Handle SQL Injection + XSS [START]
//$Year = intranet_htmlspecialchars($Year);
//$Semester = intranet_htmlspecialchars($Semester);
$AcademicYearID = IntegerSafe($Year);
$YearTermID = IntegerSafe($Semester);

$targetClass = IntegerSafe($targetClass, $Separater="::");
if(strpos($targetClass, '0::') !== false) {
    $targetClass = str_replace('0::', '::', $targetClass);
}
### Handle SQL Injection + XSS [END]

// load stored "Year-Grade" Setting
$gradeSettingAry = $ldiscipline->ReturnYearGradeSetting();

// load released-approved Award records of students on selected AcademicYearID and YearTermID
$studentMeritAry = $ldiscipline->ReturnStudentMeritRecord($AcademicYearID, $YearTermID);	
	
// load conduct score deducted in "Manual Adjustment"
$manualAdjustScore = $ldiscipline->ReturnStudentManualAdjustScore($AcademicYearID, $YearTermID);	
	
# Get all students of selected form / class
$array_students = $ldiscipline->storeStudent("0", $targetClass, $AcademicYearID); 

// Get YearID of students
$StudentYearID = $ldiscipline->GetYearIdOfStudent($array_students, $AcademicYearID);

# when generate whole year
if($IsAnnual)
{
	if($AcademicYearID != '')
	{	# when generate whole year of a selected year
		$student_data = $ldiscipline->retrieveAllSemesterConductBalanceGradeForGeneration($array_students,$AcademicYearID);
	}
	else
	{
		# when generate all year, whole year	
		$student_data = $ldiscipline->retrieveAllSemesterAllYearConductBalanceGradeForGeneration($array_students,$AcademicYearID);
	}
	
	for ($i=0; $i<sizeof($student_data); $i++)
	{
		$student_id = $student_data[$i]['StudentID'];
		$student_data_annual[$student_id][] = $student_data[$i];
	}	
	
	if(is_array($student_data_annual))
	{
		foreach($student_data_annual as $Key=>$Value)
		{
			$total_score = '';
			$ratio = '';
			$total_ratio ='';
			
			# Calculate the conduct score for each semester by multiplying them to the ratio
			for($a=0;$a<sizeof($Value);$a++)
			{
				list($t_student_id, $t_conduct_year ,$t_conduct_semester,$t_conduct_score, $t_grade_char) = $Value[$a];
				$ratio = $ldiscipline->getSemesterRatio('', '', $t_conduct_year,$t_conduct_semester);
				
				if($ratio != 0)
				{
					$t_conduct_score *= $ratio;
				
					# Add all conduct score to total 
					$total_score[$t_conduct_year][]= $t_conduct_score;
					$total_ratio += $ratio;
				}
			}
			
			if(is_array($total_score))
			{
				foreach($total_score as $Key=>$Value)
				{
					# get this student old annual grade
					$t_grade_char_array = $ldiscipline->retrieveStudentConductGrade($t_student_id, $Key,'',1);
					$t_grade_char = $t_grade_char_array[$t_student_id];
					
					# add up the conduct score in all semesters
					$sum=0;
					for($a=0;$a<sizeof($Value);$a++)
					{
						$sum+=$Value[$a];	
					}
					
					# Get the average of the total score by dividing the number of semester
					$pre_adj_total_score_avg = round($sum / $total_ratio);
                    
					# get this student annual conduct score
					$tmp_studentid[0] = $t_student_id;
					$ldiscipline_annual_record = $ldiscipline->retrieveAnnualConductBalanceGradeForGeneration($tmp_studentid,$Key);
					$ldiscipline_annual_record_ConductScore = $ldiscipline_annual_record[0]['ConductScore'];
					
					# add up semester conduct score and annual conduct score
					$total_score_avg = $pre_adj_total_score_avg + $ldiscipline_annual_record_ConductScore;
					
					# get the grade
					$new_grade_char = $ldiscipline->getGradeFromScore($total_score_avg,$Key,$t_conduct_semester);
			        
        			// if ($t_grade_char != $new_grade_char)
					{
						$sql = "UPDATE DISCIPLINE_STUDENT_CONDUCT_BALANCE
						SET GradeChar = '$new_grade_char', ConductScore = '$pre_adj_total_score_avg'
						WHERE StudentID = '$t_student_id'
						AND AcademicYearID = '$Key' AND IsAnnual=1";
						$ldiscipline->db_db_query($sql);
					}
				}
			}
		}
	}
}
else
{
	if($AcademicYearID != '')
	{
		if($ldiscipline->retriveConductMarkCalculationMethod() == 1)
		{
			$sql = "SELECT YearTermID FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '".$AcademicYearID."' ORDER BY YearTermID DESC";
			$MaxYearTermID = $ldiscipline->returnVector($sql);
			if($MaxYearTermID[0] == $YearTermID)		
			{
				// if the select YearTermID is the Final Year Term, udpate / create a ConductMarkBalance record
				for($i=0; $i<sizeof($array_students); $i++)
				{
					$sql = "SELECT COUNT(*) FROM DISCIPLINE_STUDENT_CONDUCT_BALANCE WHERE AcademicYearID = '".$AcademicYearID."' AND StudentID = '".$array_students[$i]."' AND IsAnnual = 1";
					$IsAnnualExist = $ldiscipline->returnVector($sql);
					
					// Get the Conduct mark for the final year term
					$sql = "SELECT ConductScore FROM DISCIPLINE_STUDENT_CONDUCT_BALANCE WHERE AcademicYearID = '".$AcademicYearID."' AND StudentID = '".$array_students[$i]."' AND YearTermID = '".$YearTermID."'";
					$LatestCondcutMark = $ldiscipline->returnVector($sql);
					
					if($IsAnnualExist[0] == 0)
					{
						$sql = "INSERT INTO DISCIPLINE_STUDENT_CONDUCT_BALANCE(StudentID, IsAnnual, ConductScore, AcademicYearID, YearTermID) VALUES ('".$array_students[$i]."',1,'".$LatestCondcutMark[0]."','$AcademicYearID','0')";
						$ldiscipline->db_db_query($sql);
					}
					else
					{
						$sql = "UPDATE DISCIPLINE_STUDENT_CONDUCT_BALANCE SET ConductScore = '".$LatestCondcutMark[0]."' WHERE StudentID = '".$array_students[$i]."' AND AcademicYearID = '".$AcademicYearID."' AND IsAnnual = 1";
						$ldiscipline->db_db_query($sql);
					}
				}
			}
		}
		
		$student_data = $ldiscipline->retrieveSemesterConductBalanceGradeForGeneration($array_students,$AcademicYearID,$YearTermID);
	}
	else
	{
		$student_data = $ldiscipline->retrieveSemesterAllYearConductBalanceGradeForGeneration($array_students,$YearTermID);
	}
	
	for ($i=0; $i<sizeof($student_data); $i++)
	{
		list($t_student_id, $t_conduct_year, $t_conduct_semester,$t_conduct_score, $t_grade_char) = $student_data[$i];
		//$new_grade_char = $ldiscipline->getGradeFromScore($t_conduct_score,$t_conduct_year);
		$new_grade_char = $ldiscipline->Get_WSCSS_Grade_From_Year_Grade_Setting($t_student_id, $t_conduct_score);
		//echo $t_student_id.'/'.$new_grade_char.'<br>';
		
		if($ldiscipline->retriveConductMarkCalculationMethod() == 1)
		{
			$sql = "UPDATE DISCIPLINE_STUDENT_CONDUCT_BALANCE
					SET GradeChar = '$new_grade_char'
					WHERE StudentID = '$t_student_id'
					AND AcademicYearID = '$t_conduct_year' AND YearTermID= '$t_conduct_semester'";
			$ldiscipline->db_db_query($sql);
			
			$sql = "SELECT YearTermID FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = $t_conduct_year ORDER BY TermStart DESC";
			$LastTermID = $ldiscipline->returnVector($sql);
			if($LastTermID[0] == $t_conduct_semester)
			{
				$sql = "UPDATE DISCIPLINE_STUDENT_CONDUCT_BALANCE
						SET GradeChar = '$new_grade_char'
						WHERE StudentID = '$t_student_id'
						AND AcademicYearID = '$t_conduct_year' AND IsAnnual = 1";
				$ldiscipline->db_db_query($sql);
			}
		}
		else
		{
			if ($t_grade_char != $new_grade_char)
			{
				$sql = "UPDATE DISCIPLINE_STUDENT_CONDUCT_BALANCE
				SET GradeChar = '$new_grade_char'
				WHERE StudentID = $t_student_id
				AND AcademicYearID = '$t_conduct_year' AND YearTermID= '$t_conduct_semester'";
				$ldiscipline->db_db_query($sql);
			}
		}
	}
}

# Update generation time
if($YearTermID == '0' || $YearTermID=='')
{
	$redirect_semester = '';
	$gen_semester = "Annual";
}
else
{
	$gen_semester = $YearTermID;
	$redirect_semester = $YearTermID;	
}

if($AcademicYearID == '0')
{
	$gen_year = "Year";
}
else
{
	$gen_year = $AcademicYearID;	
}
$ldiscipline->updateGenerateActionRecord($gen_year,$gen_semester);

header("Location: index.php?xmsg=grade_generated&Year=$AcademicYearID&Semester=$redirect_semester");

intranet_closedb();
?>