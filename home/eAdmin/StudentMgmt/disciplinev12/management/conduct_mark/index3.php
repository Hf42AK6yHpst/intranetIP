<?php 
# using:
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldisciplinev12 = new libdisciplinev12();

$ldisciplinev12->CONTROL_ACCESS("Discipline-MGMT-Conduct_Mark-View");

# prepare year array for generating year selection box
$sql = "select distinct Year from DISCIPLINE_STUDENT_CONDUCT_BALANCE where trim(Year)!='' and Year is not null";
$tmpYearArr = $ldisciplinev12->returnArray($sql);
if($Year=='')
{
	$Year = $tmpYearArr[0][0];
}
if(is_array($tmpYearArr))
{
	$yearArr[] = array('0',$i_Attendance_AllYear);
	foreach($tmpYearArr as $Key=>$Value)
	{
		$yearArr[]= array($Value[0],$Value[0]);
	}
}
if($Year=='')
{
	$Year = getCurrentAcademicYear();
}

# prepare semester array for generating semester selection box
$semester_data = getSemesters();
$number = sizeof($semester_data);
$semArr[] = array('0',$ec_iPortfolio['whole_year']);
for ($i=0; $i<$number; $i++)
{
	$linedata[]= split("::",$semester_data[$i]);
	$semArr[] = array($linedata[$i][0],$linedata[$i][0]);
}
if($Semester=='')
{
	$Semester = getCurrentSemester();
}

if($Semester=='')
{
	$Semester = $semArr[0][0];
}
# prepare class array for generating class selection box
$sql = "select distinct ClassName from INTRANET_CLASS";
$tmpClassArr = $ldisciplinev12->returnArray($sql);
if(is_array($tmpClassArr))
{
	$classArr[] = array('0',$i_general_all_classes);
	foreach($tmpClassArr as $Key=>$Value)
	{
		$classArr[]= array($Value[0],$Value[0]);
	}
}
if($ClassName=='')
{
	$ClassName = $classArr[1][0];
	
}

/*if($Semester=='' || $Semester==0)
{
	$SemesterNum = 0;
}
else*/

$SemesterNum = $ldisciplinev12->convertSemesterStringToNum($Semester);
if($SemesterNum=='')$SemesterNum=0;

#Get initial score
$sql = "SELECT ConductScore FROM DISCIPLINE_CONDUCT_SETTING WHERE RecordType=0";
$temp = $ldisciplinev12->returnVector($sql);
$initial_score = $temp[0];

$yearSelection = $linterface->GET_SELECTION_BOX($yearArr, 'id="Year", name="Year" onChange="document.form1.submit()"', "", $Year);
$semSelection = $linterface->GET_SELECTION_BOX($semArr, 'id="Semester", name="Semester" onChange="document.form1.submit()"', "", $Semester);
$classSelection = $linterface->GET_SELECTION_BOX($classArr, 'id="ClassName", name="ClassName" onChange="document.form1.submit()"', "", $ClassName);
$toolbar = "<table width=\"98%\" cellspacing=\"0\" cellpadding=\"0\"><tr><td>".$linterface->GET_LNK_IMPORT("import.php")."</td>";
$toolbar .= "<td>".$linterface->GET_LNK_EXPORT("export.php")."</td>";
$toolbar .="<td>".$linterface->GET_LNK_GENERATE("generate.php")."</td>";
$toolbar.="</tr></table>";
$searchTB = "<input type=\"textbox\" class=\"formtextbox\" id=\"searchStr\" name=\"searchStr\" value=\"$searchStr\">".toolBarSpacer().$linterface->GET_BTN($button_find,'button','document.form1.submit()');

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

$onStatement = " or con_adj.Year = '$Year'";
$onStatement .= " or con_adj.Semester = '$Semester'";

$extraCriteria .= ($ClassName=='0')? "" : " AND iu.ClassName = '$ClassName' ";
$extraCriteria .= ($searchStr=='')? "" : " AND (iu.EnglishName like '%".trim($searchStr)."%' or iu.ChineseName like '%".trim($searchStr)."%' )";
$sql_bal_semester = $ldisciplinev12->convertSemesterStringToNumSql('con_bal');
$sql_adj_semester = $ldisciplinev12->convertSemesterStringToNumSql('con_adj');

$BaseMark = $ldisciplinev12->getConductMarkRule('baseMark');

$sql = "select 
		concat(ClassName,'-',ClassNumber) as ClassNameNum, 
		concat('<a href=\"adjust.php?StudentID=',iu.UserID,'&Year=$Year&ClassName=',iu.ClassName,'&Semester=$SemesterNum&SemesterStr=$Semester\" class=\"tablelink\">
		',".getNameFieldByLang("iu.").",'</a>') as UserName,
		
		if(merit_rec.StudentID is not null,
			concat('<a href=\"javascript:Show_Merit_Window(\'',iu.UserID,'\',\'',con_adj.Year,'\',\'',
			".$sql_adj_semester.",'\')\" class=\"tablelink\"><span id=\"merit_',con_adj.StudentID,con_adj.Year,".$sql_adj_semester.",'\">',con_bal.ConductScore,'</span></a>'),
		if(con_bal.StudentID is not null,con_bal.ConductScore,'$BaseMark')),
		
		sum(con_adj.AdjustMark),
			 
		
		if(con_bal.StudentID is not null,
			con_bal.ConductScore+round(sum(con_adj.AdjustMark)),
			'$BaseMark'+round(sum(con_adj.AdjustMark))) as Adjusted_Mark,
		
		if(con_bal.StudentID is not null,con_bal.GradeChar,'--') as GradeChar,
		DATE_FORMAT(max(con_adj.DateInput),'%Y-%m-%d') as LastUpdate,
		CONCAT('<input type=\"checkbox\" name=\"RecordID[]\" value=\"',iu.UserID,',$Year,$SemesterNum\">'),
		if(con_bal.StudentID is not null,con_bal.ConductScore,'$BaseMark') as Current_Mark,
		iu.UserID, '$Year' as Year, '".$Semester."'as Semester,con_adj.DateInput as DateInput,sum(con_adj.AdjustMark) SortAdjustMark,count(merit_rec.StudentID)
		
		
		from INTRANET_USER iu
		left join DISCIPLINE_STUDENT_CONDUCT_BALANCE con_bal on (iu.UserID = con_bal.StudentID and con_bal.year='$Year' and con_bal.semester='$Semester')
		left join DISCIPLINE_CONDUCT_ADJUSTMENT con_adj 
		on (con_adj.StudentID = iu.UserID and con_adj.Year = con_bal.Year and con_adj.Semester = con_bal.Semester)
		left join DISCIPLINE_MERIT_RECORD merit_rec on (merit_rec.StudentID = iu.UserId and merit_rec.Year = con_bal.Year and merit_rec.Semester = con_bal.Semester)
		where 1 $extraCriteria and ((con_bal.Year = '$Year' and con_bal.semester='$Semester' ) or (con_bal.StudentID is null))
		group by iu.UserID";
		
// left outer join DISCIPLINE_STUDENT_CONDUCT_BALANCE con_bal on (con_bal.StudentID = iu.UserID)		
// and con_bal.Year ='$Year' 
// 		and con_bal.Semester='$Semester'
				
//group by con_bal.Year, con_bal.Semester, con_bal.StudentID
// debug_r($sql);

$SettingName = ($Semester == '0')? "conduct_mark_gen_".$Year."_Annual": "conduct_mark_gen_".$Year."_$Semester";

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("ClassNameNum","iu.EnglishName", "Current_Mark", "SortAdjustMark","Adjusted_Mark","GradeChar","DateInput");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0,0);
$li->wrap_array = array(0,0,0);
//$li->IsColOff = 2;
$li->IsColOff = 'eDisciplineConductMarkView';

$pos = 0;
$li->column_list .= "<td width='1%'class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='5%'>".$li->column($pos++, $i_general_class)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_general_name)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_Discipline_System_Conduct_Mark_Current_Conduct_Mark)."</td>\n";
$li->column_list .= "<td width='5%'>".$li->column($pos++, $i_Discipline_System_Discipline_Conduct_Mark_Adjustment)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_Discipline_System_Discipline_Conduct_Mark_Adjusted_Conduct_Mark)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_Discipline_System_Report_ClassReport_Grade)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_Discipline_System_Discipline_Conduct_Last_Updated)."</td>\n";
$li->column_list .= "<td width='5%'>".$li->check("RecordID[]")."</td>\n";

# Get last grade generation
$sqlgen = "select DATE_FORMAT(SettingValue,'%Y-%m-%d' ) as LastGen from DISCIPLINE_GENERAL_SETTING
		   where SettingName = '$SettingName' order by SettingValue desc limit 1";
$LastGen = $ldisciplinev12->returnVector($sqlgen);
if(sizeof($LastGen)==0)
{
	$LastGen[0] = '--';
}

# Top menu highlight setting
$CurrentPage = "Management_ConductMark";

$TAGS_OBJ[] = array($i_Discipline_System_Access_Right_Conduct_Mark);

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldisciplinev12->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

?>
<script language="javascript">

var targetDivID = "";
var clickDivID = "";


function adjust_record(StudentID,Year,Semester)
{
	//checkPost(document.form1, 'adjust.php?StudentID='+StudentID+'&Year='+Year+'&Semester='+Semester);
	window.location='adjust.php?StudentID='+StudentID+'&Year='+Year+'&Semester='+Semester;
}
function adjust_selected_record()
{
	if(check_checkbox(document.form1,'RecordID[]'))
	{
		checkPost(document.form1,'adjust.php');
	}
	else
	{
		alert('<?=$i_Discipline_System_Notice_Warning_Please_Select?>');	
	}
}


function Show_Merit_Window(StudentID,Year,Semester){

    obj = document.form1;
    obj.targetDivID.value = StudentID+Year+Semester;
    obj.ajaxStudentID.value = StudentID;
    obj.ajaxYear.value = Year;
    obj.ajaxSemester.value = Semester;
    obj.ajaxType.value = 'Merit';
    clickDivID = 'merit_'+StudentID+Year+Semester;
    targetDivID = StudentID+Year+Semester;
    YAHOO.util.Connect.setForm(obj);	
    var path;
    path = "ajax_conduct.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback2);
    
}
function Show_Adj_Window(StudentID,Year,Semester){

    obj = document.form1;
    obj.targetDivID.value = StudentID+Year+Semester;
    obj.ajaxStudentID.value = StudentID;
    obj.ajaxYear.value = Year;
    obj.ajaxSemester.value = Semester;
    obj.ajaxType.value = 'Adjust';
    clickDivID = 'adj_'+StudentID+Year+Semester;
    targetDivID = StudentID+Year+Semester;
    YAHOO.util.Connect.setForm(obj);	
    var path;
    path = "ajax_conduct.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback2);
    
}

var callback2 = {
    success: function ( o )
    {       
    	
		  DisplayDefDetail(o.responseText);
	}
}
function DisplayDefDetail(text){
	document.getElementById('div_form').innerHTML=text;
	DisplayPosition();
  
}
function getPosition(obj, direction)
{
	
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}
function DisplayPosition(){
	
  document.getElementById(targetDivID).style.left=getPosition(document.getElementById(clickDivID),'offsetLeft') +10;
  document.getElementById(targetDivID).style.top=getPosition(document.getElementById(clickDivID),'offsetTop') +10;
  document.getElementById(targetDivID).style.visibility='visible';
}

function Hide_Window(pos){
  document.getElementById(pos).style.visibility='hidden';
}
</script>
<br />
<form name="form1" method="get" action="">

			<table width="98%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr><td colspan="2" align="right"><?= $linterface->GET_SYS_MSG($xmsg) ?></td></tr>
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="70%"><?=$toolbar ?></td>
											<td width="30%"></td>
										</tr>
									</table>
								</td>
								<td align="right"><?=$searchTB ?></td>
							</tr>
							<tr><td><?=$yearSelection?><?=$semSelection?><?=$classSelection?></td>
								<td height="28" align="right" valign="bottom" >
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="right" valign="bottom">
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
														<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
															<table border="0" cellspacing="0" cellpadding="2">
																<tr>
																	<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
																	<td nowrap="nowrap"><a href="javascript:adjust_selected_record()" class="tabletool" title="<?= $i_Discipline_System_Access_Right_Adjust ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_approve.gif" width="12" height="12" border="0" align="absmiddle"> <?=$i_Discipline_System_Access_Right_Adjust ?></a>
																	</td>
																</tr>
															</table>
														</td>
														<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><?= $li->display() ?>
					<div id="div_form"></div>
					</td>
				</tr>
				<tr><td class="tabletextremark">^<?=$i_Discipline_System_Discipline_Conduct_Last_Generated?> : <?=$LastGen[0]?></td></tr>
				</table><br>
				
				<input type="hidden" name="targetDivID" id="targetDivID" />
				<input type="hidden" name="ajaxStudentID" id="ajaxStudentID" />
				<input type="hidden" name="ajaxYear" id="ajaxYear" />
				<input type="hidden" name="ajaxSemester" id="ajaxSemester" />
				<input type="hidden" name="ajaxType" id="ajaxType" />
				<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
				<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
				<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
				<input type="hidden" name="page_size_change" value="" />
				<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

</form>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>