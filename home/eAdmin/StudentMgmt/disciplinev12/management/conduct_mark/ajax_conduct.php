<?
// using : 

/*************************** Change Log ***************************/
##
##	2017-10-13 (Bill)   : ($ajaxType=='Merit') display Manual Adjustment Table in Merit Layer 	($sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns_IncludeAdjust'])	[2016-1207-1221-39240]
##
##	2017-10-04 (Bill)   : ($ajaxType=='Merit') display Conduct Score Change - Gain / Deduct only	($sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns'])		[2016-0930-1728-34206]
##											   display Merit Records with Conduct Score Change only 	($sys_custom['eDiscipline']['ConductMark_DisplayScoreChangeMeritOnly'])		[2016-1207-1221-39240]
##
##	2016-09-29 (Bill)   : ($ajaxType=='Merit') display conduct score change related to misconduct [2016-0808-1526-52240]
##
##	2013-06-21 (Carlos) : ($ajaxType='Merit') display overflowed merit item conduct score change with mask symbol
##
##	Date	:	20091208 (Ronald)
##	Details	:	in (ajaxSemester=="Annual") condition, I added a checking to check if now using accumlate method or not.
##				if using accumlate method, then no need to show the ratio table
##
/************************ End of Change Log ************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldisciplinev12 = new libdisciplinev12();

if($sys_custom['eDiscipline']['yy3']){
	include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");
	$ldiscipline_ui = new libdisciplinev12_ui_cust();
}

## Get the conduct mark calculation method
$ConductMarkCalculationMethod = $ldisciplinev12->retriveConductMarkCalculationMethod();

if($task=='check_gen_time')
{
	include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
	
	$OriYearTermID = $ajaxSemester;
	if($ajaxSemester == '0' || $ajaxSemester=='')
	{
		$ajaxSemester = "Annual";
	}
		
		$SettingName = "conduct_mark_gen_".$ajaxYear."_".$ajaxSemester;
		$x = $ldisciplinev12->getLastConductGradeGenDate($SettingName);
		
		if ($x == '')
		{
			$Obj_AcademicYear = new academic_year($ajaxYear);
			$AcademicYearTitleEn = $Obj_AcademicYear->YearNameEN;
			
			$Obj_YearTerm = new academic_year_term($OriYearTermID);
			$YearTermTitleEn = $Obj_YearTerm->YearTermNameEN;
			
			$SettingName = "conduct_mark_gen_".$AcademicYearTitleEn."_".$YearTermTitleEn;
			$x = $ldisciplinev12->getLastConductGradeGenDate($SettingName);
		}
		
		$sql = "select Ratio, AcademicYearID, YearTermID from DISCIPLINE_SEMESTER_RATIO where AcademicYearID ='$ajaxYear' and RecordType='C'";
		$ResultArr = $ldisciplinev12->returnArray($sql);	
		
		if($ajaxSemester=="Annual")
		{
			if($ConductMarkCalculationMethod == 0)
			{
				## only show the ratio table if calculate by term (no accumlate) ##	
				$x .= '<table border="0" cellspacing="0" cellpadding="3" class="tablerow2">';																
				$x .= '<tr><td>
							<fieldset class="form_sub_option" style="margin:0px;">
								<table border="0" cellspacing="0" cellpadding="3">
								<tr><td colspan="2" class="tablebluebottom"><strong>'.$i_Discipline_System_Discipline_Conduct_Mark_Weighting.'</strong></td></tr>';
				
				if(sizeof($ResultArr)>0)
				{
					$x .= '		<tr><td><u>'.$i_Discipline_System_Discipline_Conduct_Mark_Weight_Semester.'</u></td>
								<td align="center"><u>'.$i_Discipline_System_Discipline_Conduct_Mark_Weight.'</u></td></tr>';
					
					for($a=0;$a<sizeof($ResultArr);$a++)
					{
						$thisYearTermID = $ResultArr[$a]['YearTermID'];
						$Obj_YearTerm = new academic_year_term($thisYearTermID);
						$thisTermDisplay = $Obj_YearTerm->Get_Year_Term_Name();
						
						$x .= '	<tr><td>'.$thisTermDisplay.'</td><td align="center">'.$ResultArr[$a]['Ratio'].'</td></tr>';	
						$sum += $ResultArr[$a]['Ratio'];
					}
					
					$x.='		<tr><td align="right" class="tabletextremark" style="border-top:1px solid #CCCCCC">'.$i_Discipline_System_Discipline_Conduct_Mark_Weight_Total.'</td>
									<td align="center" class="tabletextremark" style="border-top:1px solid #CCCCCC">'.$sum.'</td>
								</tr>';
				}
				else
				{
					$x .= '		<tr><td align="center" colspan="2">'.$i_Discipline_System_Discipline_Conduct_Mark_No_Ratio_Setting.'</td></tr>';	
				}
				$x .= '</table>';
			}
		}
		
		echo $x;
}
else if ($task == 'reload_term_selection')
{
	$semester_data = getSemesters($AcademicYearID);
	$number = sizeof($semester_data);
	$semArr[] = array('0',$ec_iPortfolio['whole_year']);
	
	if($number > 0)
	{
		foreach($semester_data as $id=>$name)
		{
			$semArr[] = array($id, $name);
		}
	}
	$semSelection = $linterface->GET_SELECTION_BOX($semArr, 'id="Semester", name="Semester" onChange="change_field(\'ajaxSemester\',this.value)"', "", "");
	
	echo $semSelection;
}
else
{
	//if($ajaxSemester=='IsAnnual')
	/*
	$yearID = $ldisciplinev12->getAcademicYearIDByYearName($Year);
	$semester_data = getSemesters($yearID);
	$number = sizeof($semester_data);
	foreach($semester_data as $id=>$name) {
		$semArr[] = $name;	
	}
	
	$SemesterStr = $semArr[$ajaxSemester];
	
	
	$cond = " cond_adj.StudentID = $ajaxStudentID and cond_adj.Year = '$ajaxYear' ";
	if($semArr[$ajaxSemester]=='')
	{
		$cond.=" and cond_adj.Isannual=1";
	}
	else
	{
		$cond.=" and cond_adj.Semester = '$semArr[$ajaxSemester]'";
	}
	*/
	//$yearID = $Year;
	
	$yearID = ($ajaxYear!="") ? $ajaxYear : $Year;
	$YearTermID = $ajaxSemester;
	
	$cond = " cond_adj.StudentID = $ajaxStudentID and cond_adj.AcademicYearID = '$yearID' ";
	if($YearTermID=='' || $YearTermID==0)
	{
		$cond.=" and cond_adj.Isannual=1";
	}
	else
	{
		$cond.=" and cond_adj.YearTermID = '$YearTermID'";
	}
	
	if($ajaxType=='Remove_Adjust') {
		if($ajaxSemester=="") {
			$conds = " AND IsAnnual=1";
		}
		else {
			$conds = " AND YearTermID='$ajaxSemester'";
		}
		
		$sql = "DELETE FROM DISCIPLINE_CONDUCT_ADJUSTMENT WHERE RecordID=$ajaxRecordID AND StudentID=$ajaxStudentID AND AcademicYearID=$ajaxYear $conds";
		$result = $ldisciplinev12->db_db_query($sql);
		if($result)
			echo 1;
	}
	
	if($ajaxType=='Adjust')
	{
		$sql = "select cond_adj.Reason, ".getNameFieldByLang("iu.")." as PIC, cond_adj.AdjustMark, 
			DATE_FORMAT(cond_adj.DateInput,'%Y-%m-%d') as DateInput, cond_adj.RecordID, cond_adj.StudentID
			from DISCIPLINE_CONDUCT_ADJUSTMENT as cond_adj left join INTRANET_USER as iu on iu.UserID = cond_adj.PICID
			where  $cond ORDER BY cond_adj.DateModified DESC";
		$resultArr = $ldisciplinev12->returnArray($sql);	
	
		$data = '<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">';
		$data .='	<tr class="tabletop">
						<td align="center" valign="middle">#</td>
						<td>'.$i_Discipline_System_Discipline_Conduct_Mark_Adjustment.'</td>
						<td align="left">'.$i_Discipline_PIC.'</td>
						<td>'.$eDiscipline["AdjustmentDate"].'</td>
						<td align="left">'.$i_Discipline_Reason.'</td>';
		if($ldisciplinev12->CHECK_ACCESS("Discipline-MGMT-Conduct_Mark-Adjust")) {
			$data .=	'<td>&nbsp</td>';
		}									
		$data .= 	'</tr>';
		
		if(sizeof($resultArr)>0)
		{
			for($a=0;$a<sizeof($resultArr);$a++)
			{
				$data .= '	<tr class="tablerow1">
								 <td>'.($a+1).'</td>
								 <td>'.$resultArr[$a]['AdjustMark'].'</td>
								 <td>'.$resultArr[$a]['PIC'].'</td>
								 <td>'.$resultArr[$a]['DateInput'].'</td>
								 <td>'.$resultArr[$a]['Reason'].'</td>';
				if($ldisciplinev12->CHECK_ACCESS("Discipline-MGMT-Conduct_Mark-Adjust")) 
					$data .= '	<td><div class="table_row_tool"><a href="javascript:confirmDeleteAdjust(\''.$resultArr[$a]['RecordID'].'\',\''.$resultArr[$a]['StudentID'].'\',\''.$Year.'\',\''.$Semester.'\')" class="delete_dim">&nbsp;</a></div></td>';
				$data .= '	</tr>';
			}
		}
		else{
			//$data .= "<tr><td colspan='5' align='center'>".$i_no_record_exists_msg."</td></tr>";
			$data .= "<tr><td colspan='5' align='center'>".$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['NoRecordAtThisTerm']."</td></tr>";
		}
	}
	else if($ajaxType=='Merit')
	{
		// [2016-0930-1728-34206]
		# Condition - Records with target Merit Type
		$targetMeritType = "";
		if($sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns'] && trim($ajaxGainDeduct) != "") {
			if($ajaxGainDeduct=="G") {
				$targetMeritType = "1";
			}
			else if($ajaxGainDeduct=="D") {
				$targetMeritType = "-1";
			}
		}
		
		// [2016-1207-1221-39240]
		# Condition - Skip no Score Change Merit / Demerit
		$IsSkipZeroScoreMerit = 0;
		if($sys_custom['eDiscipline']['ConductMark_DisplayScoreChangeMeritOnly']) {
			$IsSkipZeroScoreMerit = 1;
		}
		
		# Get Student Merit Records
		//$StudentMeritArr = $ldisciplinev12->retrieveStudentAllMeritRecordByYearSemester($ajaxStudentID, '', '', $yearID, $YearTermID);
		$StudentMeritArr = $ldisciplinev12->retrieveStudentAllMeritRecordByYearSemester($ajaxStudentID, '', '', $yearID, $YearTermID, "conduct", $IsSkipZeroScoreMerit, $targetMeritType);
		
		# Get Student Conduct Records
		$withGMrecords = false;
		if($sys_custom['eDiscipline']['GMConductMark']) {
			$StudentGMArr = $ldisciplinev12->retrieveStudentACCUConductScoreChangeRecord($ajaxStudentID, $yearID, $YearTermID);
			$withGMrecords = count((array)$StudentGMArr) > 0;
		}
		
		# Merit Record Table
		$data = '<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">';
		if(!$sys_custom['eDiscipline']['GMConductMark'] || ($sys_custom['eDiscipline']['GMConductMark'] && sizeof($StudentMeritArr) > 0))
		{
			// Table Header if required
			if($sys_custom['eDiscipline']['GMConductMark'] && $withGMrecords) {
				$data .= '<tr class="tablerow3"><td colspan="5">'.$eDiscipline['Award_and_Punishment'].'</td></tr>';
			}
			if ($sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns'] && $sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns_IncludeAdjust'] && trim($ajaxGainDeduct) != "") {
				$data .= '<tr class="tablerow3"><td colspan="5">'.$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['SystemGenerated'].'</td></tr>';
			}
			
			// Table Columns
			$data .= '<tr class="tabletop">
						<td align="center" valign="middle">#</td>
						<td align="left">'.$i_Discipline_Reason.'</td>
						<td align="left">'.$i_Discipline_PIC.'</td>
						<td>'.$i_Discipline_System_Discipline_Conduct_Mark_Adjustment.'</td>
						<td>'.$Lang['eDiscipline']['EventDate'].'</td>
					</tr>';
			
			// Table Content
			for($a=0;$a<sizeof($StudentMeritArr);$a++)
			{
				/*
				$To = $StudentMeritArr[$a]['ToScore'];
				$From = $StudentMeritArr[$a]['FromScore'];
				$AdjustedScore = $To-$From;
				*/
				//$AdjustedScore = $StudentMeritArr[$a]['ConductScoreChange'];
				
				$data .= '<tr class="tablerow1">
							 <td>'.($a+1).'</td>
							 <td>'.$StudentMeritArr[$a]['Remark'].'</td>
							 <td>'.$StudentMeritArr[$a]['PIC'].'</td>';
				if($sys_custom['eDiscipline']['yy3'] && 
					($StudentMeritArr[$a]['OverflowConductMark']=='1' || $StudentMeritArr[$a]['OverflowMeritItemScore']=='1' || $StudentMeritArr[$a]['OverflowTagScore']=='1') && $StudentMeritArr[$a]['ScoreChange']==0) 
				{
					$data .= '<td>'.$ldiscipline_ui->displayMaskedMark($StudentMeritArr[$a]['ScoreChange']).'</td>';
				}
				else
				{
					$data .= '<td>'.$StudentMeritArr[$a]['ScoreChange'].'</td>';
				}
				$data .= '	<td>'.$StudentMeritArr[$a]['RecordDate'].'</td>
						</tr>';
			}
		}
		
		// [2016-0808-1526-52240]
		# Conduct Records Table
		if($sys_custom['eDiscipline']['GMConductMark'] && $withGMrecords)
		{
			// Seperator if required
			if(($sys_custom['eDiscipline']['GMConductMark'] && $withGMrecords && sizeof($StudentMeritArr) > 0)) {
				$data .='<tr class="tablerow1"><td colspan="5">&nbsp;</td></tr>';
			}
			
			// Table Header & Columns
			$data .='<tr class="tablerow3"><td colspan="5">'.$eDiscipline['Good_Conduct_and_Misconduct'].'</td></tr>
				 	 <tr class="tabletop">
						<td align="center" valign="middle">#</td>
						<td align="left">'.$i_Discipline_Reason.'</td>
						<td align="left">'.$i_Discipline_PIC.'</td>
						<td>'.$i_Discipline_System_Discipline_Conduct_Mark_Adjustment.'</td>
						<td>'.$Lang['eDiscipline']['EventDate'].'</td>
					</tr>';
			
			// Table Content
			for($a=0;$a<sizeof($StudentGMArr);$a++)
			{
				$data .='<tr class="tablerow1">
						 	<td>'.($a+1).'</td>
						 	<td>'.$StudentGMArr[$a]['RecordName'].'</td>
						 	<td>'.$StudentGMArr[$a]['PICName'].'</td>
							<td>'.$StudentGMArr[$a]['GMConductScoreChange'].'</td>
							<td>'.$StudentGMArr[$a]['RecordDate'].'</td>
						</tr>';
			}
		}
		
		// [2016-0930-1728-34206]
		# Manual Adjustment Table
		if($sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns'] && $sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns_IncludeAdjust'] && trim($ajaxGainDeduct) != "")
		{
			# Condition - Adjustment Type
			$targetAdjustCond = "";
			if($ajaxGainDeduct=="G") {
				$targetAdjustCond = " AND cond_adj.AdjustMark > 0 ";
			}
			else if($ajaxGainDeduct=="D") {
				$targetAdjustCond = " AND cond_adj.AdjustMark < 0 ";
			}
			
			# Get Student Adjustment Records
			$sql = "SELECT
						cond_adj.Reason, ".getNameFieldByLang("iu.")." as PIC, cond_adj.AdjustMark,
						DATE_FORMAT(cond_adj.DateInput,'%Y-%m-%d') as DateInput, cond_adj.RecordID, cond_adj.StudentID
					FROM DISCIPLINE_CONDUCT_ADJUSTMENT as cond_adj 
						LEFT JOIN INTRANET_USER as iu on (iu.UserID = cond_adj.PICID)
					WHERE 
						$cond $targetAdjustCond 
					ORDER BY cond_adj.DateModified DESC";
			$resultArr = $ldisciplinev12->returnArray($sql);
			if(sizeof($resultArr) > 0 && $targetAdjustCond != "")
			{
				# Check Adjustment access right
				$showDeleteConductCol = $ldisciplinev12->CHECK_ACCESS("Discipline-MGMT-Conduct_Mark-Adjust");
				
				// Seperator if required
				if(sizeof($StudentMeritArr) > 0) {
					$data .= '</table>';
					$data .= '<br/>';
				}
				// Reset if required
				else {
					$data = '';
				}
				
				// Table Header & Columns
				$data .= '<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">';
				$data .= '	<tr class="tablerow3"><td colspan="'.($showDeleteCol? 6 : 5).'">'.	$i_Discipline_System_Manual_Adjustment.'</td></tr>';
				$data .= '	<tr class="tabletop">
								<td align="center" valign="middle">#</td>
								<td align="left">'.$i_Discipline_Reason.'</td>
								<td align="left">'.$i_Discipline_PIC.'</td>
								<td>'.$i_Discipline_System_Discipline_Conduct_Mark_Adjustment.'</td>
								<td>'.$eDiscipline["AdjustmentDate"].'</td>';
				if($showDeleteConductCol) {
					$data .='	<td>&nbsp</td>';
				}									
				$data .= 	'</tr>';
				
				// Table Content
				for($a=0;$a<sizeof($resultArr);$a++) {
					$data .= '	<tr class="tablerow1">
									 <td>'.($a+1).'</td>
									 <td>'.$resultArr[$a]['Reason'].'</td>
									 <td>'.$resultArr[$a]['PIC'].'</td>
									 <td>'.$resultArr[$a]['AdjustMark'].'</td>
									 <td>'.$resultArr[$a]['DateInput'].'</td>';
					if($showDeleteConductCol) {
						$data .= '	 <td><div class="table_row_tool"><a href="javascript:confirmDeleteAdjust(\''.$resultArr[$a]['RecordID'].'\',\''.$resultArr[$a]['StudentID'].'\',\''.$Year.'\',\''.$Semester.'\')" class="delete_dim">&nbsp;</a></div></td>';
					}
					$data .= '	</tr>';
				}
			}
		}
	}
	
	if($ajaxType!='Remove_Adjust')
		$data .= '	</table>';
	
	$div_deb = '<div id="'.$targetDivID.'" style="position:absolute; width:380px; z-index:1; ">
		  		<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td height="19"><table width="100%" border="0" cellspacing="0" cellpadding="0">
						 	<tr>
								<td width="5" height="19"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_01.gif" width="5" height="19"></td>
								<td height="19" valign="middle" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_02.gif">&nbsp;</td>
								<td width="19" height="19"><a href="javascript:Hide_Window(\''.$targetDivID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_close_off.gif" name="pre_close1" width="19" height="19" border="0" id="pre_close1"></a></td>
							</tr>
						</table></td>
					</tr>
					<tr>
						<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
						  	<tr>
								<td width="5" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif" width="5" height="19"></td>
								<td align="left" bgcolor="#FFFFF7"><table width="100%" border="0" cellspacing="0" cellpadding="0">
							  		<tr><td height="150" align="left" valign="top">
											'.$data.'
									</td></tr>
									<tr>
								    	<td><br><table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
								      		<tr>
									       		<td height="1" class="dotline"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>
								      		</tr>
								     	</table></td>
									</tr>
		                 		</table></td>
								<td width="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif" width="6" height="6"></td>
						  	</tr>
						  	<tr>
								<td width="5" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_07.gif" width="5" height="6"></td>
								<td height="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif" width="5" height="6"></td>
								<td width="6" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_09.gif" width="6" height="6"></td>
						  	</tr>
						</table></td>
				  	</tr>
				</table></div>';
	
	if($ajaxType!='Remove_Adjust')
		echo $div_deb;
}
?>