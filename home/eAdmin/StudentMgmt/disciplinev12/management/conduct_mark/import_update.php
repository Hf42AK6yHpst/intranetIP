<?php
// Modifying by:

############ Change Log Start #############################
##
##  Date    :   2018-03-15 (Bill)   [DM#3378]
##              fixed: cannot import whole year records correctly
##
##	Date	:	2018-02-07 (Bill)	[2017-1206-1547-27236]
##				Display User Login
##
##	Date	:	2016-04-18 (Bill)	[DM#2953]
##				Add parms passing to index.php and import.php
##
#####################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Conduct_Mark-Adjust");

# Get Import data
$sql = "SELECT * FROM temp_conduct_score_import";
$recordArr = $ldiscipline->returnArray($sql);

### List out the Import Result
$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
$x .= "<tr>";
	$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_general_class</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_identity_student</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['General']['UserLogin']."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_general_Year</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_SettingsSemester</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_System_Discipline_Conduct_Mark_Adjustment</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Profile_PersonInCharge</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_Reason</td>";
$x .= "</tr>";

for($a=0; $a<sizeof($recordArr); $a++)
{
	$x .= "<tr class=\"tablebluerow".($a % 2 + 1)."\">";
		$x .= "<td class=\"tabletext\">".($a + 1)."</td>";
		$x .= "<td class=\"tabletext\">". $recordArr[$a]['ClassName']."-".$recordArr[$a]['ClassNumber']."</td>";
		$x .= "<td class=\"tabletext\">". $recordArr[$a]['Student'] ."</td>";
		$x .= "<td class=\"tabletext\">". $recordArr[$a]['UserLogin'] ."</td>";
		$x .= "<td class=\"tabletext\">". $recordArr[$a]['Year'] ."</td>";
		$x .= "<td class=\"tabletext\">". $recordArr[$a]['Semester'] ."</td>";
		$x .= "<td class=\"tabletext\">". $recordArr[$a]['AdjustMark'] ."</td>";
		$x .= "<td class=\"tabletext\">". $recordArr[$a]['Teacher'] ."</td>";
		$x .= "<td class=\"tabletext\">". $recordArr[$a]['Reason'] ."</td>";
	$x .= "</tr>";
	
	$AcademicYearID = $ldiscipline->getAcademicYearIDByYearName($recordArr[$a]['Year']);
	
	// [DM#3378] use IsAnnual instead of Semester
	//if(strtolower($recordArr[$a]['Semester']) == 'Whole Year')
	if($recordArr[$a]['IsAnnual'])
	{
		$fields = "(StudentID,AcademicYearID,YearTermID,IsAnnual,AdjustMark,Reason,PICID,DateInput,DateModified)";
		$values = "('".$recordArr[$a]['StudentID']."','".$AcademicYearID."',0,1,'".$recordArr[$a]['AdjustMark']."','".$recordArr[$a]['Reason']."','".$recordArr[$a]['PICID']."',now(),now())";
	}
	else
	{
		$YearTermID = $ldiscipline->getTermIDByTermName($recordArr[$a]['Semester'], $AcademicYearID);
	
		$fields = "(StudentID,AcademicYearID,YearTermID,AdjustMark,Reason,PICID,DateInput,DateModified)";
		$values = "('".$recordArr[$a]['StudentID']."','".$AcademicYearID."','".$YearTermID."','".$recordArr[$a]['AdjustMark']."','".$recordArr[$a]['Reason']."','".$recordArr[$a]['PICID']."',now(),now())";
	}
	
	$sql = "INSERT INTO DISCIPLINE_CONDUCT_ADJUSTMENT $fields values $values";
	if($ldiscipline->db_db_query($sql))
	{
		$import_success++;	
	}
}

if($import_success > 0)
{
	$xmsg2 = "<font color=green>".($import_success)." ".$iDiscipline['Reord_Import_Successfully']."</font>";
}
else
{
	$xmsg = "import_failed";
}

$x .= "</table>";

$linterface = new interface_html();
$import_button = $linterface->GET_ACTION_BTN($button_finish, "button","self.location='index.php?Semester=$RedirectYearTermID&ClassID=$RedirectClassID'")."&nbsp;".$linterface->GET_ACTION_BTN($iDiscipline['ImportOtherRecords'], "button", "window.location='import.php?RedirectYearTermID=$RedirectYearTermID&RedirectClassID=$RedirectClassID'");

# Menu Highlight
$CurrentPage = "Management_ConductMark";
$CurrentPageArr['eDisciplinev12'] = 1;

# Left Menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Tag
$TAGS_OBJ[] = array($i_Discipline_System_Access_Right_Conduct_Mark);

# Step
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

$linterface->LAYOUT_START();
?>

<form name="form1" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
</tr>
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td><?= $x ?></td>
</tr>
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
				<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>