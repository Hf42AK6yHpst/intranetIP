<?php
# modifying by : 

############ Change Log Start #############################
##
##	Date	:	2017-10-04 (Bill)	[2017-1004-0907-59206]
##				added js function Show_GD_Merit_Window(), to support pop-up layer for Total Gain & Total Deduct column
##
##	Date	:	2016-04-18 (Bill)	[DM#2953]
##				Add parms passing to import.php and generate.php
##
##	Date	:	2015-07-17 (Evan)
##				Update style of search bar
##
##	Date	:	2013-07-17 (Carlos)
##				Added remark of "*Down Graded" for yy3 customization
##
##	Date 	: 	20100211 (Henry)
##	Details :	Modified the query by using left outer join in stead of inner join when joining DISCIPLINE_STUDENT_CONDUCT_BALANCE
## 
##	Date 	: 	20100202 (Ronald)
##	Details :	Modified the query by using from left outer join to inner join when joining INTRANET_USER
##
#####################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
//include_once($PATH_WRT_ROOT."includes/libaccessright.php");

# Change "num per page"
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# Preserve Table View
if ($ck_approval_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_approval_page_number", $pageNo, 0, "", "", 0);
	$ck_approval_page_number = $pageNo;
}
else if (!isset($pageNo) && $ck_approval_page_number!="")
{
	$pageNo = $ck_approval_page_number;
}
if ($ck_approval_page_order!=$order && $order!="")
{
	setcookie("ck_approval_page_order", $order, 0, "", "", 0);
	$ck_approval_page_order = $order;
}
else if (!isset($order) && $ck_approval_page_order!="")
{
	$order = $ck_approval_page_order;
}
if ($ck_approval_page_field!=$field && $field!="")
{
	setcookie("ck_approval_page_field", $field, 0, "", "", 0);
	$ck_approval_page_field = $field;
}
else if (!isset($field) || $ck_approval_page_field == "")
{
	$field = $sortEventDate;
}

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldisciplinev12 = new libdisciplinev12();

$ldisciplinev12->CONTROL_ACCESS("Discipline-MGMT-Conduct_Mark-View");

## Get Conduct Mark Calculation Method
$ConductMarkCalculationMethod = $ldisciplinev12->retriveConductMarkCalculationMethod();

# Prepare array for Year Selection
$yearInfo = GetAllAcademicYearInfo();
$yearArr = array();
for($i=0; $i<sizeof($yearInfo); $i++) {
	list($year_id, $yearEN, $yearB5) = $yearInfo[$i];
	$yearArr[$i][] = $year_id;
	$yearArr[$i][] = Get_Lang_Selection($yearB5, $yearEN);
}

# Target Year
if($Year=='')
{
	$Year = Get_Current_Academic_Year_ID();
}
$AcademicYearID = $Year;

# Prepare array for Semester Selection
$semester_data = getSemesters($AcademicYearID);
$number = sizeof($semester_data);
if(($ConductMarkCalculationMethod == 0) || $special_feature['eDis_hidden_semester_in_conduct_mark']){
	$semArr[] = array('',$ec_iPortfolio['whole_year']);
}
if(!$special_feature['eDis_hidden_semester_in_conduct_mark']) 
{
	foreach($semester_data as $id => $name) {
		$semArr[] = array($id, $name);
	}
}

# Target Semester & Year Term
if(!isset($Semester) && !$special_feature['eDis_hidden_semester_in_conduct_mark'])
{
	$Semester = getCurrentSemesterID();
}
if(!isset($Semester) && !$special_feature['eDis_hidden_semester_in_conduct_mark'])
{
	$Semester = $semArr[0][0];
}
$YearTermID = $Semester;
if($YearTermID=='' && $YearTermID!='0') 
	$YearTermID = 0;

# Year Selection
$current = ($Year == Get_Current_Academic_Year_ID()) ? 1 : 0;
if($current) {
	$yearOption = $Lang['eDiscipline']['CurrentYear']." | <a href='javascript:goSubmit(".Get_Previous_Academic_Year_ID().")'>".$Lang['eDiscipline']['PreviousYear']."</a>";	
	$yearSelection = "<input type='hidden' name='Year' id='Year' value='$Year'>";
}
else {
	$yearOption = "<a href='index.php'>".$Lang['eDiscipline']['CurrentYear']."</a> | ".$Lang['eDiscipline']['PreviousYear'];
	$yearSelection = $linterface->GET_SELECTION_BOX($yearArr, 'id="Year", name="Year" onChange="if(this.selectedIndex>0) {document.form1.Semester.value=\'\';document.form1.ClassID.value=\'\';document.form1.submit();}"', $Lang['eDiscipline']['FieldTitle']['SelectSchoolYear'], $Year);
}

# Semester Selection
$semSelection = $linterface->GET_SELECTION_BOX($semArr, 'id="Semester" name="Semester" onChange="document.form1.submit();"', "", $Semester);

# Get Initial Score
$sql = "SELECT ConductScore FROM DISCIPLINE_CONDUCT_SETTING WHERE RecordType=0";
$temp = $ldisciplinev12->returnVector($sql);
$initial_score = $temp[0];

# Target Class - default the first class
if(empty($ClassID))
{
	$orderBy = " ORDER BY Sequence";
	$sql = "SELECT YearClassID, ClassTitleEN, ClassTitleB5 FROM YEAR_CLASS WHERE AcademicYearID=$AcademicYearID $orderBy limit 1";
	$result = $ldisciplinev12->returnArray($sql);
	$ClassID = $result[0]['YearClassID'];
}
$ClassID = $ClassID==-1 ? "" : $ClassID;

# Class Selection
$classSelection = $ldisciplinev12->getSelectClass("name=\"ClassID\" id=\"ClassID\" onChange=\"document.form1.submit()\"", $ClassID, $i_Discipline_System_Award_Punishment_All_Classes, 1, -1, $Year);

$RedirectClassID = $ClassID? $ClassID : "-1";

$searchStr = trim($_GET['searchStr']);

$toolbar = "<table cellspacing=\"0\" cellpadding=\"0\"><tr>";
if($current) {
	$toolbar .= "<td>".$linterface->GET_LNK_IMPORT("import.php?RedirectYearTermID=$YearTermID&RedirectClassID=$RedirectClassID","","","","",0)."</td>";
}
$toolbar .= "<td>".$linterface->GET_LNK_EXPORT("export.php?AcademicYearID=$AcademicYearID&YearTermID=$YearTermID&ClassID=$ClassID&searchStr=".stripslashes(intranet_htmlspecialchars($searchStr))."","","","","",0)."</td>";
if($current) {
	$toolbar .="<td>".$linterface->GET_LNK_GENERATE("generate.php?RedirectClassID=$RedirectClassID","","","","",0)."</td>";
}
$toolbar.="</tr></table>";

$searchTB = "<input type=\"textbox\" class=\"formtextbox\" id=\"searchStr\" name=\"searchStr\" value=\"".stripslashes(intranet_htmlspecialchars($searchStr))."\">";

# DB Table Settings
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

# SQL Condition
$onStatement = " or con_adj.AcademicYearID = '$AcademicYearID'";
$onStatement .= " or con_adj.YearTermID = '$YearTermID'";

$extraCriteria .= ($ClassID=='') ? "" : " AND ycu.YearClassID=$ClassID";
$extraCriteria .= ($searchStr=='')? "" : " AND (iu.EnglishName like '%".$ldisciplinev12->Get_Safe_Sql_Like_Query(stripslashes($searchStr))."%' or iu.ChineseName like '%".$ldisciplinev12->Get_Safe_Sql_Like_Query(stripslashes($searchStr))."%' )";

$conds_year = ($AcademicYearID=='0')? "": " AND (con_bal.AcademicYearID='$AcademicYearID')";
$conds_semester = ($YearTermID=='0' || $YearTermID=='')? "": " AND (con_bal.IsAnnual = '1')";

if ($YearTermID=='0' || $YearTermID=='')
	$YearTermIDStr = 'IsAnnual';
else
	$YearTermIDStr = $YearTermID;

$sql_adj_semester = $ldisciplinev12->convertSemesterStringToNumSql('con_adj', 1);
$sql_adj_semester = $ldisciplinev12->convertSemesterStringToNumSql('con_adj', 1);

### Note: Changed to use from left outer join to inner join ###
$sql = "select 
		concat(".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN').",'-',ycu.ClassNumber) as ClassNameNum, 
		concat('<a href=\"adjust.php?StudentID=',iu.UserID,'&Year=$Year&ClassName=',iu.ClassName,'&ClassID=',yc.YearClassID,'&Semester=$YearTermID&SemesterStr=$Semester\" class=\"tablelink\">',".getNameFieldByLang("iu.").",'</a>') as UserName,
		1 as Current_Mark,";
if($sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns'])
{
	$sql .= "1 as AddMark,
			1 as DeductMark, ";	
}		
$sql .= "
		round(sum(con_adj.AdjustMark)) as AdjustMark,
		1 as Adjusted_Mark,
		1 as GradeChar,
		DATE_FORMAT(max(con_adj.DateInput),'%Y-%m-%d') as LastUpdate,
		if($current,(CONCAT('<input type=\"checkbox\" name=\"RecordID[]\" value=\"',iu.UserID,',$AcademicYearID,$YearTermIDStr\">')), '&nbsp;'),
		iu.UserID, 
		'$AcademicYearID' as AcademicYearID, 
		'".$YearTermID."' as YearTermID,
		1 as DateInput,
		count(con_adj.AdjustMark),
		".getNameFieldByLang("iu.")." as UserName2

		from INTRANET_USER iu
		LEFT OUTER join DISCIPLINE_STUDENT_CONDUCT_BALANCE con_bal on (iu.UserID = con_bal.StudentID $conds_semester $conds_year)
		left outer join DISCIPLINE_CONDUCT_ADJUSTMENT con_adj on (iu.UserID = con_adj.studentID)
		LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=iu.UserID)
		LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)
		where 1 $extraCriteria and iu.RecordType=2 AND iu.RecordStatus IN (1) AND yc.YearClassID != 0 AND yc.AcademicYearID=$AcademicYearID
		group by iu.UserID";
$SettingName = ($Semester == '')? "conduct_mark_gen_".$Year."_Annual": "conduct_mark_gen_".$Year."_$Semester";

if (!isset($order)) $order = 1;

# Init DB Table
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("concat(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', 1)), IF(INSTR(ClassNameNum, '-') = 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', -1)), 10, '0')))","iu.EnglishName", "Current_Mark");
if($sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns']) {
	$li->field_array = array_merge($li->field_array, array("TotalGain","TotalDeduct"));	
}
$li->field_array = array_merge($li->field_array, array("AdjustMark","Adjusted_Mark","GradeChar","DateInput"));
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0,0);
$li->wrap_array = array(0,0,0);
//$li->IsColOff = 2;
$li->IsColOff = 'eDisciplineConductMarkView';

# DB Table Header
$pos = 0;
$li->column_list .= "<td width='1'class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='20%'>".$li->column($pos++, $i_general_class)."</td>\n";
$li->column_list .= "<td width='20%'>".$li->column($pos++, $i_general_name)."</td>\n";
$li->column_list .= "<td width='10%'>".$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['SystemGenerated']."</td>\n";
if($sys_custom['eDiscipline']['ConductMark_DisplayAddDeductColumns']) {
	$li->column_list .= "<td width='5%'>".$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['TotalGain']."</td>\n";
	$li->column_list .= "<td width='5%'>".$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['TotalDeduct']."</td>\n";		
}
$li->column_list .= "<td width='10%'>".$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['ManuallyAdjusted']."</td>\n";
$li->column_list .= "<td width='15%'>".$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['ConsolidatedMark']."</td>\n";
$li->column_list .= "<td width='10%'>$i_Discipline_System_Report_ClassReport_Grade</td>\n";
$li->column_list .= "<td width='15%'>$i_Discipline_System_Discipline_Conduct_Last_Updated</td>\n";
$li->column_list .= "<td width='1'>".($current ? $li->check("RecordID[]") : "&nbsp;")."</td>\n";

# Get Last Grade Generation
$sqlgen = "select DATE_FORMAT(SettingValue,'%Y-%m-%d' ) as LastGen from DISCIPLINE_GENERAL_SETTING
		   where SettingName = '$SettingName' order by SettingValue desc limit 1";
$LastGen = $ldisciplinev12->returnVector($sqlgen);
if(sizeof($LastGen)==0)
{
	$LastGen[0] = '--';
}

# Top Menu
$CurrentPage = "Management_ConductMark";
$CurrentPageArr['eDisciplinev12'] = 1;

$TAGS_OBJ[] = array($i_Discipline_System_Access_Right_Conduct_Mark);

# Left Menu
$MODULE_OBJ = $ldisciplinev12->GET_MODULE_OBJ_ARR();

# Start Layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
var targetDivID = "";
var clickDivID = "";

function adjust_record(StudentID,Year,Semester)
{
	//checkPost(document.form1, 'adjust.php?StudentID='+StudentID+'&Year='+Year+'&Semester='+Semester);
	window.location='adjust.php?StudentID='+StudentID+'&Year='+Year+'&Semester='+Semester;
}

function adjust_selected_record()
{
	if(check_checkbox(document.form1,'RecordID[]'))
	{
		checkPost(document.form1,'adjust.php');
	}
	else
	{
		alert('<?=$i_Discipline_System_Notice_Warning_Please_Select?>');	
	}
}

function Show_Merit_Window(StudentID,Year,Semester)
{
    obj = document.form1;
    obj.targetDivID.value = StudentID+Year+Semester;
    obj.ajaxStudentID.value = StudentID;
    obj.ajaxYear.value = Year;
    obj.ajaxSemester.value = Semester;
    obj.ajaxType.value = 'Merit';
    obj.ajaxGainDeduct.value = '';
    
    clickDivID = 'merit_'+StudentID+Year+Semester;
    targetDivID = StudentID+Year+Semester;
    YAHOO.util.Connect.setForm(obj);
    
    var path;
    path = "ajax_conduct.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback2);
}

function Show_GD_Merit_Window(StudentID,Year,Semester,Type)
{
    obj = document.form1;
    obj.targetDivID.value = StudentID+Year+Semester+Type;
    obj.ajaxStudentID.value = StudentID;
    obj.ajaxYear.value = Year;
    obj.ajaxSemester.value = Semester;
    obj.ajaxType.value = 'Merit';
    obj.ajaxGainDeduct.value = Type;
    
    clickDivID = 'merit_gd_'+StudentID+Year+Semester+Type;
    targetDivID = StudentID+Year+Semester+Type;
    YAHOO.util.Connect.setForm(obj);
    
    var path;
    path = "ajax_conduct.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback2);
}

function Show_Adj_Window(StudentID,Year,Semester)
{
    obj = document.form1;
    obj.targetDivID.value = StudentID+Year+Semester;
    obj.ajaxStudentID.value = StudentID;
    obj.ajaxYear.value = Year;
    obj.ajaxSemester.value = Semester;
    obj.ajaxType.value = 'Adjust';
    obj.ajaxGainDeduct.value = '';
    
    clickDivID = 'adj_'+StudentID+Year+Semester;
    targetDivID = StudentID+Year+Semester;
    YAHOO.util.Connect.setForm(obj);
    
    var path;
    path = "ajax_conduct.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback2);
}

var callback2 = {
    success: function ( o )
    {
		DisplayDefDetail(o.responseText);
	}
}

function DisplayDefDetail(text)
{
	document.getElementById('div_form').innerHTML=text;
	DisplayPosition();
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}
	return pos_value;
}

function DisplayPosition()
{
	var posleft = getPosition(document.getElementById(clickDivID),'offsetLeft') +10;
	var postop = getPosition(document.getElementById(clickDivID),'offsetTop') +10;
	
  	//document.getElementById(targetDivID).style.left=getPosition(document.getElementById(clickDivID),'offsetLeft') +10;
  	//document.getElementById(targetDivID).style.top=getPosition(document.getElementById(clickDivID),'offsetTop') +10;
  	document.getElementById(targetDivID).style.left= posleft+"px";
  	document.getElementById(targetDivID).style.top= postop+"px";
  	document.getElementById(targetDivID).style.visibility='visible';
}

function Hide_Window(pos)
{
	document.getElementById(pos).style.visibility='hidden';
}

function confirmDeleteAdjust(RecordID, StudentID, Year, Semester)
{
	Remove_Adjust_Window(RecordID, StudentID, Year, Semester);
	//Show_Adj_Window(StudentID,Year,Semester);
}

function Remove_Adjust_Window(RecordID, StudentID, Year, Semester)
{
	if(confirm("<?=$i_Discipline_System_alert_remove_warning_level?>"))
	{
		clickDivID = 'adj_'+StudentID+Year+Semester;
		
		$.post(
			"ajax_conduct.php", 
			{
				Action: "Edit",
				DivID: encodeURIComponent('adj_'+StudentID+Year+Semester),
				targetDivID: StudentID+Year+Semester,
				ajaxStudentID: StudentID,
				ajaxYear: Year,
				ajaxSemester: Semester,
				ajaxType: 'Remove_Adjust',
				ajaxRecordID: RecordID
			},
			function(ReturnData)
			{
				//alert(ReturnData);
				if(ReturnData==1) {
					self.location.reload();
				}
				else {
					Get_Return_Message("<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>")
				}
			}
		);
	}
}

function goSubmit(year)
{
	$('#Year').val(year);
	
	document.form1.Semester.value='';
	document.form1.ClassID.value=''
	document.form1.submit();	
}
</script>

<br />
<form name="form1" method="get" action="">
			<table width="98%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr><td colspan="2" align="right"><?= $linterface->GET_SYS_MSG($xmsg) ?></td></tr>
							<tr>
								<td valign="top">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="70%"><?=$toolbar ?></td>
											<td width="30%"></td>
										</tr>
									</table>
								</td>
								<td align="right">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="right"><div class="Conntent_search"><?=$searchTB ?></div></td>
										</tr>
										<tr>
											<td align="right"><?=$yearOption?></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr class="table-action-bar"><td><?=$yearSelection?><?=$semSelection?><?=$classSelection?></td>
								<td height="28" align="right" valign="bottom" >
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="right" valign="bottom">
											<?
											//debug_pr($_SESSION);
											if($ldisciplinev12->CHECK_ACCESS("Discipline-MGMT-Conduct_Mark-Adjust") && $current) {?>
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
														<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
															<table border="0" cellspacing="0" cellpadding="2">
																<tr>
																	<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
																	<td nowrap="nowrap"><a href="javascript:adjust_selected_record()" class="tabletool" title="<?= $i_Discipline_System_Access_Right_Adjust ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_approve.gif" width="12" height="12" border="0" align="absmiddle"> <?=$i_Discipline_System_Access_Right_Adjust ?></a>
																	</td>
																</tr>
															</table>
														</td>
														<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
													</tr>
												</table>
											<? } ?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><?= $li->display() ?>
					<div id="div_form"></div>
					</td>
				</tr>
				<tr><td class="tabletextremark">^<?=$i_Discipline_System_Discipline_Conduct_Last_Generated?> : <?=$LastGen[0]?>
					<?=$sys_custom['eDiscipline']['yy3']?'<br /><span style="color:red">'.$Lang['eDiscipline']['Remark_DownGraded'].'</span>':''?></td></tr>
			</table><br>
			
			<input type="hidden" name="targetDivID" id="targetDivID" />
			<input type="hidden" name="ajaxStudentID" id="ajaxStudentID" />
			<input type="hidden" name="ajaxYear" id="ajaxYear" />
			<input type="hidden" name="ajaxSemester" id="ajaxSemester" />
			<input type="hidden" name="ajaxType" id="ajaxType" />
			<input type="hidden" name="ajaxGainDeduct" id="ajaxGainDeduct" />
			<input type="hidden" name="ajaxRecordID" id="ajaxRecordID" />
			<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
			<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
			<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
			<input type="hidden" name="page_size_change" value="" />
			<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>