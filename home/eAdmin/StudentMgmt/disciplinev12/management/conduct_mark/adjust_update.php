<?php 
// Using:

###### Change Log [Start] ######
#
#	Date    :	2019-05-13 (Bill)
#               Prevent SQL Injection
#
#   Date    :   2019-05-02 (Bill)
#               Hidden Fields > Change 'UniqueID' to 'UniqueIndex' to prevent IntegerSafe()    [EJ DM#1200]
#
####### Change Log [End] #######

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();

$ldisciplinev12 = new libdisciplinev12();
$ldisciplinev12->CONTROL_ACCESS("Discipline-MGMT-Conduct_Mark-Adjust");

$IsAllowedtoAdjust = $ldisciplinev12->CHECK_ACCESS("Discipline-MGMT-Conduct_Mark-Adjust");
if($IsAllowedtoAdjust)
{
	if(is_array($_POST))
	{
		foreach($_POST as $Key => $Value)
		{
			if($Key=='UniqueIndex')
			{
				for($ct=0; $ct<sizeof($UniqueIndex);$ct++)
				{
				    $Student = explode(",", $UniqueIndex[$ct]);
					list($StudentArr[$ct]['StudentID'], $StudentArr[$ct]['Year'], $StudentArr[$ct]['Semester']) = $Student;
					
					$StudentArr[$ct]['StudentID'] = IntegerSafe($StudentArr[$ct]['StudentID']);
					$StudentArr[$ct]['Year'] = IntegerSafe($StudentArr[$ct]['Year']);
					if($StudentArr[$ct]['Semester']=='IsAnnual' || $StudentArr[$ct]['Semester']==0)
					{
						$StudentArr[$ct]['Semester'] = '';	
						$StudentArr[$ct]['IsAnnual'] = 1;
						$Semester = '';
					}
					else
					{
					    $StudentArr[$ct]['Semester'] = IntegerSafe($StudentArr[$ct]['Semester']);
						$StudentArr[$ct]['IsAnnual'] = '';
					}
					
					$StudentArr[$ct]['AdjustMark'] = IntegerSafe($_POST['tb_'.$UniqueIndex[$ct]]);
					$StudentArr[$ct]['Reason'] = intranet_htmlspecialchars($_POST['select_'.$UniqueIndex[$ct]]);
				}
			}
		}
	}
	
	/*
	$semester_data = getSemesters();
	$number = sizeof($semester_data);
	if($number>0)
	{
		/*
		for ($i=0; $i<$number; $i++)
		{
			$linedata[]= split("::",$semester_data[$i]);
			$semArr[] = $linedata[$i][0];
		}
		*/
		/*
		foreach($semester_data as $key=>$val) 
		{
			$semArr[] = $val;
		}
		
	}
	*/
	
	### Handle SQL Injection + XSS [START]
	//$AcademicYearID = Get_Current_Academic_Year_ID();
	//$YearTermID = getCurrentSemesterID();
	$AcademicYearID = IntegerSafe($YearID);
	$YearTermID = IntegerSafe($Semester);
	$SemesterStr = IntegerSafe($SemesterStr);
	### Handle SQL Injection + XSS [END]
	
	for($a=0;$a<sizeof($StudentArr);$a++)
	{
		/*
		$sql1 = "SELECT ConductScore FROM DISCIPLINE_STUDENT_CONDUCT_BALANCE WHERE StudentID = '".$StudentArr[$a]['StudentID']."' AND YearTermID = '$YearTermID'";
		$ConductMarkBalance = $ldisciplinev12->returnVector($sql1);
		$FromScore = ($ConductMarkBalance[0]==0) ? 0 : $ConductMarkBalance[0];
		*/
		//$fields = "(StudentID, Year, Semester, IsAnnual, AdjustMark, Reason, PICID, DateInput, DateModified)";
		/*$values = "(".$StudentArr[$a]['StudentID'].", '".$StudentArr[$a]['Year']."', '".$semArr[$StudentArr[$a]['Semester']]."',
					'".$StudentArr[$a]['IsAnnual']."',".$StudentArr[$a]['AdjustMark'].", '".$StudentArr[$a]['Reason']."', $UserID,now(),now() )";*/
		
		$fields = "(StudentID, AcademicYearID, YearTermID, IsAnnual, AdjustMark, Reason, PICID, DateInput, DateModified)";
		$values = "('".$StudentArr[$a]['StudentID']."', '".$StudentArr[$a]['Year']."', '".$SemesterStr."',
					'".$StudentArr[$a]['IsAnnual']."', '".$StudentArr[$a]['AdjustMark']."', '".$StudentArr[$a]['Reason']."', '$UserID', NOW(), NOW())";
		$sql = "INSERT INTO DISCIPLINE_CONDUCT_ADJUSTMENT $fields VALUES $values";
		if($ldisciplinev12->db_db_query($sql))
		{
			$result = 'add';
			
			$current_conduct_info = $ldisciplinev12->retrieveConductBalanceGrade(array($StudentArr[$a]['StudentID']), "", "", "", $AcademicYearID, $YearTermID);
			$FromScore = $current_conduct_info[0]['ConductScore'];
			$ldisciplinev12->CHECK_AND_SEND_WARNING_RULE_LETTER($StudentArr[$a]['StudentID'], "", "", $FromScore, $AcademicYearID, $YearTermID, "", $manuallyAdjust=1);	
		}
		else
		{
			$result = 'failed';
		}
	}
}

$ClassName = intranet_htmlspecialchars($ClassName);
$classID = $ldisciplinev12->getClassIDByClassName($ClassName);

$ReturnPage = "index.php?Year=$Year&Semester=$SemesterStr&ClassID=$ClassID&searchStr=$searchStr&xmsg=add";

intranet_closedb();
header("Location: $ReturnPage");
?>