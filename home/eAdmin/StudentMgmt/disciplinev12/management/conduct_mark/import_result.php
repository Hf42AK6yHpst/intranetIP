<?
// Modifying by:

############ Change Log Start #############################
##
##  Date    :   2018-03-15 (Bill)   [DM#3378]
##              fixed 1. no checking for term name
##                    2. cannot import whole year records correctly
##
##	Date	:	2018-02-07 (Bill)	[2017-1206-1547-27236]
##				Support User Login Checking
##
##	Date	:	2016-04-18 (Bill)	[DM#2953]
##				Add parms passing to import.php and import_update.php
##
#####################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();

$limport = new libimporttext();
$lo = new libfilesystem();

$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));
if($ext != ".CSV" && $ext != ".TXT")
{
	header("location: import.php?xmsg=import_failed");
	exit();
}

$data = $limport->GET_IMPORT_TXT($csvfile);
if(is_array($data))
{
	$col_name = array_shift($data);
}

$format_wrong = false;
$file_format = array('Class Name','Class Number','User Login','Year','Semester','Adjustment','PIC','Reason');
for($i=0; $i<sizeof($file_format); $i++)
{
	if ($col_name[$i] != $file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}

if($format_wrong)
{
	header("location: import.php?xmsg=wrong_header");
	exit();
}

$ldiscipline = new libdisciplinev12();
$lu = new libuser();

$linterface = new interface_html();

# Menu Highlight
$CurrentPage = "Management_ConductMark";
$CurrentPageArr['eDisciplinev12'] = 1;

# Left Menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Tag
$TAGS_OBJ[] = array($i_Discipline_System_Access_Right_Conduct_Mark);

# Step
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$linterface->LAYOUT_START();

### List out the Import Result
$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
$x .= "<tr>";
	$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_general_class</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_identity_student</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['General']['UserLogin']."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_general_Year</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_SettingsSemester</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_System_Discipline_Conduct_Mark_Adjustment</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Profile_PersonInCharge</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_Reason</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$iDiscipline['AccumulativePunishment_Import_Failed_Reason']."</td>";
$x .= "</tr>";

//$teacherList = $lu->returnUsersType(1);

### Remove old data by drop temp table
$sql = "DROP TABLE temp_conduct_score_import";
$ldiscipline->db_db_query($sql);

### Rebuild table for temp data storage
$sql = "CREATE TABLE temp_conduct_score_import
		(
			 ClassName varchar(10),
			 ClassNumber int(3),
			 UserLogin varchar(20),
			 StudentID int(11),
			 Year varchar(100),
			 Semester varchar(100),
			 IsAnnual int(11),
			 AdjustMark int(11),
			 Reason varchar(100),
			 PICID int(11),
			 Teacher varchar(255),
			 Student varchar (255)
	    ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
$ldiscipline->db_db_query($sql);

/*
$semester_data = split("\n",get_file_content("$intranet_root/file/semester.txt"));
for ($i=0; $i<sizeof($semester_data); $i++)
{
	$target_sem = $semester_data[$i];
	$line = split("::",$target_sem);
	list ($name,$current,$fr,$to) = $line;
	$SemesterArr[] = $name;
}
*/
//$currentYear = getCurrentAcademicYear();

// [DM#3378] Re-accumulate Conduct Mark : Every Term > allow Whole Year adjustment import
$isTermCalculation = $ldiscipline->retriveConductMarkCalculationMethod() == 0;
$wholeYearInputStrAry = array('whole year', '全年');

$error_occured = 0;
for($i=0; $i<sizeof($data); $i++)
{
	$error = array();
	$PicIDArr = array();
	
	### Get Data
	list($ClassName, $ClassNumber, $UserLogin, $Year, $Semester, $Adjustment, $PIC, $Reason) = $data[$i];
	
	# School Year
	if(trim($Year) == "")
	{
		$error['No_Year'] = $iDiscipline['Award_Punishment_Missing_Year'];
	}
	else
	{
// 		$sql = "SELECT COUNT(*) FROM DISCIPLINE_STUDENT_CONDUCT_BALANCE WHERE Year = '$Year'";
// 		$tmpYear = $ldiscipline->returnVector($sql);
// 		if(!$tmpYear[0])
// 		{
			/*
			if($Year!=$currentYear)
			{
				$current_format_len = strlen($currentYear);
				$import_format_len = strlen($Year);
				# compare the length of current year. (Not the year value)
				if($current_format_len!=$import_format_len)
				{
					$error['No_Year'] = $iDiscipline['Award_Punishment_No_Year'];	
				}
			}
			*/
            // [DM#3378] always get yearid for term checking
			$AcademicYearID = $ldiscipline->getAcademicYearIDByYearName($Year);
			if ($AcademicYearID == "")
			{
				$error['No_Year'] = $iDiscipline['Award_Punishment_No_Year'];
			}
// 		}
	}
	
	# Semester
	$isAnnual = false;
	if(trim($Semester) == "")
	{
		$error['No_Semester'] = $iDiscipline['Award_Punishment_Missing_Semester'];	
	}
	else
	{
		//if ($AcademicYearID != "" && strtolower($Semester) != "whole year")
		if ($AcademicYearID != "")
		{
		    $isWholeYearInput = in_array(strtolower($Semester), $wholeYearInputStrAry);
		    if(!$isTermCalculation || !$isWholeYearInput)
		    {
		        // [DM#3378] Check term name correct or not
    			$YearTermID = $ldiscipline->getTermIDByTermName($Semester, $AcademicYearID);
    			if($YearTermID == "")
    			{
    				//$error['Wrong_Semester'] = "No such semester";
    			    $error['Wrong_Semester'] = $iDiscipline['Case_Student_Import_No_Semester'];
    			}
		    }
		    // [DM#3378] Check if target whole year
		    else if ($isTermCalculation && $isWholeYearInput)
	        {
	            $isAnnual = true;
	        }
		}
	}
	
	### Checking - Valid Class & Class Number / User Login (Student found)
	$StudentID1 = "";
	if($ClassName != "" && $ClassNumber != "") {
		$StudentID1 = $lu->returnUserID($ClassName, $ClassNumber, 2, "0,1,2");
	}
	$StudentID2 = "";
	if($UserLogin != "") {
		$StudentID2 = $lu->getUserInfoByUserLogin($UserLogin, 2);
		$StudentID2 = $StudentID2[0]["UserID"];
	}
	
	// [2017-1206-1547-27236]
	$StudentID = $StudentID1 ? $StudentID1 : $StudentID2;
	if($StudentID)
	{
		$lu = new libuser($StudentID);
		$StudentName = $intranet_session_language=="en" ? $lu->EnglishName : $lu->ChineseName;
		$StudentUserLogin = $lu->UserLogin;
		$ClassInfo = $lu->ClassName."-".$lu->ClassNumber;
		
		$ClassName = $lu->ClassName;
		$ClassNumber = $lu->ClassNumber;
		$UserLogin = $lu->UserLogin;
	}
	else
	{
		$StudentName = "";
		$StudentUserLogin = $UserLogin;
		$ClassInfo = $ClassName."-".$ClassNumber;
	}
	
	### Checking - Valid PIC
	$PicName = array();
	$PicArr = explode(",", $PIC);
	for($ct=0; $ct<sizeof($PicArr); $ct++)
	{
		$sql = "SELECT UserID as PICID, ".getNamefieldByLang()." as Name FROM INTRANET_USER WHERE UserLogin = '".$PicArr[$ct]."' AND RecordType = 1";
		$tmpResult = $lu->returnArray($sql);
		
		$PicID = $tmpResult[0]['PICID'];
		if(trim($PicID) == "")
		{
			$error['PIC'] = $iDiscipline['Good_Conduct_No_Staff'];
		}
		else
		{
			$PicIDArr[] = $PicID;
			$PicName[] = $tmpResult[0]['Name'];
		}
	}
	
	# Adjustment
	if(!is_numeric($Adjustment))
	{
		$error['Adjustment'] = $i_ServiceMgmt_System_Warning_Numeric;
	}
	else
	{
		$IsFloat = (strpos($Adjustment, "."));
		if($IsFloat)
		{
			$error['Adjustment'] = $iDiscipline['Conduct_Mark_Import_Adjustment_OutRange'];
		}
	}
	
	//$PICIDs = implode(",",$PicIDArr);
	//$status = $StudentID ? "&nbsp;":$iDiscipline['student_not_found'];
	if(trim($StudentID) == "")
	{
		//$error++;
		$error['No_Student'] = $iDiscipline['Good_Conduct_No_Student'];
	}
	
	$css = (sizeof($error) == 0) ? "tabletext" : "red";
	
	$x .= "<tr class=\"tablebluerow".($i % 2 + 1)."\">";
		$x .= "<td class=\"$css\">".($i + 1)."</td>";
		$x .= "<td class=\"$css\">". $ClassInfo ."</td>";
		$x .= "<td class=\"$css\">". $StudentName ."</td>";
		$x .= "<td class=\"$css\">". $StudentUserLogin ."</td>";
		$x .= "<td class=\"$css\">". $Year ."</td>";
		$x .= "<td class=\"$css\">". $Semester ."</td>";
		$x .= "<td class=\"$css\">". $Adjustment ."</td>";
		$x .= "<td class=\"$css\">". implode(",", $PicName) ."</td>";
		$x .= "<td class=\"$css\">". $Reason ."</td>";
		$x .= "<td class=\"$css\">";
		if(sizeof($error) > 0)
		{
			foreach($error as $Key => $Value)
			{
				$x .= $Value."<br/>";
			}
		}
		else
		{
			$x .= "--";
		}
		$x .= "</td>";
	$x .= "</tr>";
	
	//if($Semester == "")
	if($isAnnual)
	{
		$sql_fields = " (ClassName,ClassNumber,UserLogin,StudentID,Year,Semester,IsAnnual,AdjustMark,Reason,PICID,Teacher,Student)";
		$sql_values = " ('$ClassName',$ClassNumber,'$UserLogin','$StudentID','$Year','$Semester','1','$Adjustment','$Reason','".implode(',', $PicIDArr)."','".implode(",", $PicName)."','".addslashes($StudentName)."')";
	}
	else
	{
		$sql_fields = " (ClassName,ClassNumber,UserLogin,StudentID,Year,Semester,AdjustMark,Reason,PICID,Teacher,Student)";
		$sql_values = " ('$ClassName',$ClassNumber,'$UserLogin','$StudentID','$Year','$Semester','$Adjustment','$Reason','".implode(',', $PicIDArr)."','".implode(",", $PicName)."','".addslashes($StudentName)."')";
	}
	
	$sql = "INSERT INTO temp_conduct_score_import $sql_fields VALUES $sql_values ";
	$ldiscipline->db_db_query($sql);
	
	if($error)
	{
		$error_occured++;
	}	
}
$x .= "</table>";

if($error_occured)
{
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import.php?RedirectYearTermID=$RedirectYearTermID&RedirectClassID=$RedirectClassID'");
}
else
{
	$import_button = $linterface->GET_ACTION_BTN($button_import, "submit")." &nbsp;".$linterface->GET_ACTION_BTN($button_back, "button", "window.location='import.php?RedirectYearTermID=$RedirectYearTermID&RedirectClassID=$RedirectClassID'");
}
?>

<br />
<form name="form1" method="post" action="import_update.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center">
		<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2"><?=$x?></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>

<input type="hidden" name="RedirectYearTermID" id="RedirectYearTermID" value="<?php echo $RedirectYearTermID; ?>" />
<input type="hidden" name="RedirectClassID" id="RedirectClassID" value="<?php echo $RedirectClassID; ?>" />
</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>