<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
# preserve table view
if ($ck_approval_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_approval_page_number", $pageNo, 0, "", "", 0);
	$ck_approval_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_approval_page_number!="")
{
	$pageNo = $ck_approval_page_number;
}

if ($ck_approval_page_order!=$order && $order!="")
{
	setcookie("ck_approval_page_order", $order, 0, "", "", 0);
	$ck_approval_page_order = $order;
} else if (!isset($order) && $ck_approval_page_order!="")
{
	$order = $ck_approval_page_order;
}

if ($ck_approval_page_field!=$field && $field!="")
{
	setcookie("ck_approval_page_field", $field, 0, "", "", 0);
	$ck_approval_page_field = $field;
} else if (!isset($field) || $ck_approval_page_field == "")
{
	$field = $ck_right_page_field;
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!isset($RecordType) || $RecordType=="")
	$RecordType = 0;

# Form select menu
# (all forms)
$allForm = $ldiscipline->getAllForm();
$formAry = array_merge(array(array("0",$i_Discipline_All_Forms)), $allForm);
$form = (isset($form) && $form!="") ? $form : $allForm[0][0];
$formSelect = getSelectByArray($formAry, ' name="form" id="form" onChange="document.form1.submit();"', $form, 0, 1);
	
# Record Type Selection Menu
$typeSelection = "<SELECT name='RecordType' id='RecordType' onChange='document.form1.submit()'>";
$typeSelection .= "<OPTION value='0'";
$typeSelection .= ($RecordType==0) ? " selected" : "";
$typeSelection .= ">$i_general_all {$Lang['eDiscipline']['MarkingType']}</OPTION>";
$typeSelection .= "<OPTION value='1'";
$typeSelection .= ($RecordType==1) ? " selected" : "";
$typeSelection .= ">".$Lang['eDiscipline']['Marking_NA']."</OPTION>";
$typeSelection .= "<OPTION value='2'";
$typeSelection .= ($RecordType==2) ? " selected" : "";
$typeSelection .= ">".$Lang['eDiscipline']['Marking_OriginalGrade']."</OPTION>";
$typeSelection .= "</SEELCT>";

# Condition
$conds = ($RecordType!=0) ? " AND ABL.RecordType=$RecordType" : "";

$conds .= (isset($form) && $form!="" && $form!=0) ? " AND ABL.YearID=$form" : "";

# SQL Statement
$namefield = getNameFieldWithLoginByLang("USR.");
$sql = "SELECT
			y.YearName,
			$namefield as name, 
			CASE (ABL.RecordType)
				WHEN 1 THEN '".$Lang['eDiscipline']['Marking_NA']."'
				WHEN 2 THEN '".$Lang['eDiscipline']['Marking_OriginalGrade']."'
			ELSE 'Error' END as RecordType,
			CONCAT('<input type=\"checkbox\" name=\"RecordID[]\" value=\"',ABL.RecordID,'\">')
		FROM
			DISCIPLINE_MS_STUDENT_CONDUCT_ABSENTLIST ABL LEFT OUTER JOIN
			INTRANET_USER USR ON (USR.UserID=ABL.UserID) LEFT OUTER JOIN
			YEAR y ON (y.YearID=ABL.YearID)
		WHERE
			ABL.DateInput IS NOT NULL
			$conds
		";

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("YearName", "name", "RecordType");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0,0);
$li->wrap_array = array(0,0,0);
$li->IsColOff = 2;

$pos = 0;
$li->column_list .= "<td width='1%'class='tabletopnolink'>#</td>\n";
$li->column_list .= "<td width='20%'>".$li->column($pos++, "Form")."</td>\n";
$li->column_list .= "<td width='40%'>".$li->column($pos++, $i_Teaching_TeacherName)."</td>\n";
$li->column_list .= "<td width='40%'>".$li->column($pos++, $Lang['eDiscipline']['MarkingType'])."</td>\n";
$li->column_list .= "<td width='1%'>".$li->check("RecordID[]")."</td>\n";

$toolbar = "<table width=\"80%\" cellspacing=\"0\" cellpadding=\"0\">";
$toolbar .= "<tr><td>".$linterface->GET_LNK_NEW("absent_new.php?form=$form","","","","",0)."</td>";
$toolbar.="</tr></table>";

$TAGS_OBJ[] = array($iDiscipline['ConductGradeAssessment'], "/home/eAdmin/StudentMgmt/disciplinev12/management/conduct_grade/index.php", 0);
$TAGS_OBJ[] = array($iDiscipline['ConductGradeMeeting'], "/home/eAdmin/StudentMgmt/disciplinev12/management/conduct_grade/meeting/index.php", 0);
$TAGS_OBJ[] = array($Lang['eDiscipline']['AbsentInConductGradeMeeting'], "/home/eAdmin/StudentMgmt/disciplinev12/management/conduct_grade/meeting/absentList.php", 1);

$PAGE_NAVIGATION[] = array($Lang['eDiscipline']['AbsentList'], "");

# menu highlight setting
$CurrentPage = "Management_ConductGrade";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

if($xmsg=="add") $msg = $i_con_msg_add;
else if ($xmsg=="add_partial_failed") $msg = $Lang['eDiscipline']['SomeRecordsCannotAdd'];
else if($xmsg=="delete") $msg = $i_con_msg_delete;
else if($xmsg=="delete_failed") $msg = $i_con_msg_no_delete_acc_record;
else if($xmsg=="update_failed") $msg = $i_con_msg_update_failed;

?>
<script language="javascript">
<!--
function deleteRecord() {
	//var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
	if(check_checkbox(document.form1,'RecordID[]')) {
		checkRemove(document.form1,'RecordID[]','absent_remove_update.php')
	} else {
		alert('<?=$i_general_please_select." ".$iDiscipline['teacher']?>');	
	}
}

function checkAccessEdit()
{
	if(countChecked(document.form1,'RecordID[]')==1)
	{
		checkPost(document.form1,'absent_edit.php');
	}
	else
	{
		alert('<?=$i_general_please_select." ".$iDiscipline['teacher']?>');	
	}
}
//-->
</script>
<form name="form1" method="POST" action="">
<br />
<table width="100%" border="0" cellspacing="5" cellpadding="0">
	<tr height="30">
		<td width="60%"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td>
		<td align="right" width="40%"><?= $linterface->GET_SYS_MSG("",$msg) ?></td>
	</tr>
</table>
<table align="center" width="95%" border="0" cellpadding="0" cellspacing="0" >
	<tr><td colspan="2"><?=$toolbar?></td></tr>
	<tr valign="top" height="30" class="table-action-bar">
		<td class="tabletext" valign="bottom"><?=$formSelect?><?=$typeSelection?></td>
		<td valign="bottom" align="right">
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="right" valign="bottom">
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
														<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
															<table border="0" cellspacing="0" cellpadding="2">
																<tr>
																	<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
																	<td nowrap><a href="javascript:checkAccessEdit()" class="tabletool" title="<?= $button_edit ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_edit ?></a></td>
																	<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
																	<td nowrap="nowrap"><a href="javascript:deleteRecord()" class="tabletool" title="<?= $button_delete?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_delete?></a></td>
																</tr>
															</table>
														</td>
														<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
		</td>
	</tr>
	<tr valign="top">
		<td class="tabletext" colspan="2"><?=$li->display();?>
		</td>
	</tr>
</table>
<br />
<input type="hidden" name="pageNo" id="pageNo" value="<?php echo $li->pageNo; ?>"/>
<input type="hidden" name="order" id="order" value="<?php echo $li->order; ?>"/>
<input type="hidden" name="field" id="field" value="<?php echo $li->field; ?>"/>
<input type="hidden" name="page_size_change" id="page_size_change" value=""/>
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>"/>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
