<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!is_array($teacherID) || sizeof($teacherID)==0) {
	header("Location: absentList.php");	
	exit();
}

$sql = "SELECT UserID FROM DISCIPLINE_MS_STUDENT_CONDUCT_ABSENTLIST WHERE YearID=$form";
$list = $ldiscipline->returnVector($sql);

$result = true;

foreach($teacherID as $t) {
	if(!in_array($t, $list)) {
		$sql = "INSERT INTO DISCIPLINE_MS_STUDENT_CONDUCT_ABSENTLIST SET YearID=$form, UserID=$t, RecordType=$markingType, DateInput=NOW(), DateModified=NOW()";
		$ldiscipline->db_db_query($sql);
	} else {
		$result = false;	
	}
}
intranet_closedb();

$msg = ($result) ? "add" : "add_partial_failed";
/*
echo $sql.'<br>';
echo $msg;
*/
header("Location: absentList.php?RecordType=$RecordType&form=$form&xmsg=$msg");
?>
