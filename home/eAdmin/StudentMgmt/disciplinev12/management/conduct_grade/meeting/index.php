<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if($yearID=="")
	$yearID = Get_Current_Academic_Year_ID();

if($ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$meetingStudentList = $ldiscipline->getConductMeetingStudentListByAdmin($yearID, $semester);
} else {
	$meetingStudentList = $ldiscipline->getConductMeetingStudentList($yearID, $semester);
}

$classArray = array();
for($i=0; $i<sizeof($meetingStudentList); $i++) {
	
	if(!isset($classArray[$meetingStudentList[$i]['LevelName']]) || !in_array($meetingStudentList[$i]['ClassName'], $classArray[$meetingStudentList[$i]['LevelName']])) {
		$classArray[$meetingStudentList[$i]['LevelName']][] = $meetingStudentList[$i]['ClassName'];
	}
	
	$list[$meetingStudentList[$i]['LevelName']][] = $meetingStudentList[$i]['ClassName'];

}

$allForm = $ldiscipline->getAllForm($yearID);

# Semester Menu
$semester_data = getSemesters($yearID);
$selectSemesterMenu = "<select name=\"semester\" id=\"semester\">";
$selectSemesterMenu .= "<option value='#'>-- ".$button_select." --</option>";
foreach($semester_data as $key=>$val) {
	$selectSemesterMenu .= "<option value=\"".$key."\"";
	$selectSemesterMenu .= ($semester==$key) ? " selected" : "";
	$selectSemesterMenu .= ">".$val."</option>";
}
$selectSemesterMenu .= "</select>";


# Form Menu
$selectFormMenu = "<select name='form' id='form' onChange='changeClass(this.value)'>";
$selectFormMenu .= "<option value='0' selected>-- ".$i_general_all." --</option>";
for($i=0; $i<sizeof($allForm); $i++) {
	list($lvlid, $lvlName) = $allForm[$i];
	$lvlName = str_replace(" ","__",$lvlName);
	$lvlName = str_replace(".","_",$lvlName);
	$selectFormMenu .= "<option value='$lvlName'";
	$selectFormMenu .= ($form==$lvlName) ? " selected" : "";
	$lvlName = str_replace("__"," ",$lvlName);
	$lvlName = str_replace("_",".",$lvlName);
	$selectFormMenu .= ">$lvlName</option>";	
}
$selectFormMenu .= "</select>";


# class selection menu
$selectClassMenu = "<select name='class' id='class1'>";
$selectClassMenu .= "<option value='0'>-- $i_general_all --</option>";
$selectClassMenu .= "</select>";

# status selection menu
$selectStatusMenu = "<select name=\"isAdjust\" id=\"isAdjust\" onClick=\"document.getElementById('statusType1').checked=true\">";
$selectStatusMenu .= "<option value='all'";
$selectStatusMenu .= ($isAdjust=="all") ? " selected" : "";
$selectStatusMenu .= ">-- $i_general_all --</option>";
$selectStatusMenu .= "<option value='0'";
$selectStatusMenu .= ($isAdjust=="0") ? " selected" : "";
$selectStatusMenu .= ">".$iDiscipline['WaitingForReview']."</option>";
$selectStatusMenu .= "<option value='1'";
$selectStatusMenu .= ($isAdjust=="1") ? " selected" : "";
$selectStatusMenu .= ">".$iDiscipline['Revised']."</option>";
$selectStatusMenu .= "<option value='2'";
$selectStatusMenu .= ($isAdjust=="2") ? " selected" : "";
$selectStatusMenu .= ">".$Lang['eDiscipline']['SpecialCaseRevised']."</option>";
$selectStatusMenu .= "</select>";

$TAGS_OBJ[] = array($iDiscipline['ConductGradeAssessment'], "/home/eAdmin/StudentMgmt/disciplinev12/management/conduct_grade/index.php", 0);
$TAGS_OBJ[] = array($iDiscipline['ConductGradeMeeting'], "/home/eAdmin/StudentMgmt/disciplinev12/management/conduct_grade/meeting/index.php", 1);
$TAGS_OBJ[] = array($Lang['eDiscipline']['AbsentInConductGradeMeeting'], "/home/eAdmin/StudentMgmt/disciplinev12/management/conduct_grade/meeting/absentList.php", 0);

# menu highlight setting
$CurrentPage = "Management_ConductGrade";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

?>
<script language="javascript">
<!--
function showResult(semester, form, cls) {
	
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 

	document.getElementById("spanStudent").innerHTML = "<?=$i_general_loading?>";
	var url = "";
	
	url = "get_live.php?year="+document.getElementById('year').value;
	url += "&semester=" + document.getElementById('semester').value;
	url += "&form=" + document.getElementById('form').value;
	url += "&class=" + cls;
	url += "&isAdjust=" + document.getElementById('isAdjust').value;
	url += "&statusType=" + (document.getElementById('statusType1').checked ? document.getElementById('statusType1').value : document.getElementById('statusType2').value);

	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById("spanStudent").innerHTML = xmlHttp.responseText;
		document.getElementById("spanStudent").style.border = "0px solid #A5ACB2";
	} 
}

function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

function checkForm(obj) {
	if(obj.semester.value == "#") {
		alert("<?=$i_general_please_select." ".$i_Discipline_System_Conduct_Semester?>");	
		return false;
	}
	showResult(document.getElementById('semester').value, document.getElementById('form').value, document.getElementById('class1').value);
}

function goSubmit(id) {
	document.getElementById('uid').value = id;
	document.form1.submit();	
}
<?

for($i=0; $i<sizeof($allForm); $i++) {
	list($lvlid, $lvlName) = $allForm[$i];
	$lvlName = str_replace(" ","__",$lvlName);
	$lvlName = str_replace(".","_",$lvlName);
	echo "var list_".$lvlName." = new Array();\n";	
}
?>

function changeClass(val) {
	
	var item_select = document.getElementById('class1');
	
	while (item_select.options.length > 0)
	{
		item_select.options[0] = null;
	}

	if(val==0) {
		item_select.options[0] = new Option("-- <?=$i_general_all?> --",0);	
	} 
	<?
	for($i=0; $i<sizeof($allForm); $i++) {
		list($lvlid, $lvlName) = $allForm[$i];
		$lvlName = str_replace(" ","__",$lvlName);
		$lvlName = str_replace(".","_",$lvlName);
		?>
		else if(val=='<?=$lvlName?>') {
			item_select.options[0] = new Option("-- <?=$i_general_all?> --",0);
			<?
				$lvlAry[0] = $lvlid;
				$m = 1;
				$classIDAry = $ldiscipline->getClassIDByLevelID($lvlAry, $yearID);
				foreach($classIDAry as $clsID) {
					$clsName = $ldiscipline->getClassNameByClassID($clsID);
					echo "item_select.options[$m] = new Option('$clsName','$clsName');\n";
					$m++;
				}
			?>
			
		}
	<?
	}
	
	?>

}

function ChangeLockStatus(status) {
	if(countChecked(document.form1,"studentid[]")==0) {
		alert(globalAlertMsg2);	
		return false;
	} else {
		document.form1.lockStatus.value = status;
		document.form1.viewList.value = 1;
		document.form1.action = "changeLockStatus_update.php";
		document.form1.submit();
	}
}
//-->
</script>
<form name="form1" method="POST" action="reAssessment.php">
<br />
<table align="center" width="95%" border="0" cellpadding="5" cellspacing="0" >
	<tr valign="top">
		<td class="tabletext" align="center">
			<table width="100%" cellpadding="4" cellspacing="0" border="0">
				<tr>
					<td width="20%" class="formfieldtitle"><?=$i_Discipline_System_Conduct_School_Year?></td>
					<td width="80%" class="tablerow1"><?=getCurrentAcademicYear()?></td>
				</tr>
				<tr>
					<td width="20%" class="formfieldtitle"><?=$i_Discipline_System_Conduct_Semester?></td>
					<td width="80%" class="tablerow1"><?=$selectSemesterMenu?></td>
				</tr>
				<tr>
					<td width="20%" class="formfieldtitle"><?=$i_Discipline_Form?></td>
					<td width="80%" class="tablerow1"><?=$selectFormMenu?></td>
				</tr>
				<tr>
					<td width="20%" class="formfieldtitle"><?=$i_general_class?></td>
					<td width="80%" class="tablerow1"><?=$selectClassMenu?></td>
				</tr>
				<tr>
					<td width="20%" class="formfieldtitle"><?=$i_general_status?></td>
					<td width="80%" class="tablerow1">
						<input type="radio" name="statusType" id="statusType1" value="0" <? if($statusType=="" || $statusType==0) echo " checked";?>><?=$selectStatusMenu?><br>
						<input type="radio" name="statusType" id="statusType2" value="1" <? if($statusType==1) echo " checked";?>> <label for="statusType2"><?=$i_general_all?></label>
					</td>
				</tr>
				<tr>
					<td width="20%">&nbsp;</td>
					<td width="80%" class="tablerow1"><?= $linterface->GET_ACTION_BTN($button_submit, "button", "return checkForm(document.form1);")?></td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">
						<span id="spanStudent"></span>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br />
<input type="hidden" name="year" id="year" value="<?=Get_Current_Academic_Year_ID()?>">
<input type="hidden" name="uid" id="uid" value="<?=$uid?>">
<input type="hidden" name="lockStatus" id="lockStatus" value="">
<input type="hidden" name="backurl" id="backurl" value="index.php">

<input type="hidden" name="viewList" id="viewList" value="<?=$viewList?>">
</form>
<script language="javascript">
<!--
if(document.getElementById('viewList').value==1) {
	changeClass("<?=$form?>");
	showResult(document.getElementById('semester').value, document.getElementById('form').value, "<?=$class?>");	
	var item = document.form1.elements["class"];
	var len = item.options.length;
	
	for(i=0; i<len; i++) {
		if(item.options[i].value=='<?=$class?>')
			item.selectedIndex = i;
	}
	
}
//-->
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
