<?php

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(isset($studentid) && is_array($studentid))	# trigger LOCK action from index page
	$uid = $studentid;
	
$ldiscipline->updateLockStatusOfFinalGrade($year, $semester, $uid, $lockStatus);

$backurl = ($backurl!="") ? $backurl : "reAssessment.php";
intranet_closedb();
?>
<body onLoad="document.form1.submit();">
<form name="form1" method="post" action="<?=$backurl?>">
	<input type="hidden" name="year" id="year" value="<?=$year?>">
	<input type="hidden" name="semester" id="semester" value="<?=$semester?>">
	<input type="hidden" name="form" id="form" value="<?=$form?>">
	<input type="hidden" name="class" id="class" value="<?=$class?>">
	<input type="hidden" name="statusType" id="statusType" value="<?=$statusType?>">
	<input type="hidden" name="isAdjust" id="isAdjust" value="<?=$isAdjust?>">
	<input type="hidden" name="viewList" id="viewList" value="<?=$viewList?>">
	<input type="hidden" name="uid" id="uid" value="<?=$uid?>">
	<input type="hidden" name="tagid" id="tagid" value="<?=$tagid?>">
	<input type="hidden" name="submitFlag" id="submitFlag" value="YES">
</form>
<script language="javascript">
<!--
	document.form1.submit();
//-->
</script>
</body>