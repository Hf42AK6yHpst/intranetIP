<?php

#############################################
#
#	Date:	2016-07-11	Bill
#			change split() to explode(), for PHP 5.4
#
#############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html("popup4.html");
$ldiscipline = new libdisciplinev12();

$linterface->LAYOUT_START();

$conductGrade = array();

//$APCategory = $ldiscipline->getAPCategoryInfo(-1);
$sql = "SELECT TagID, TagName FROM DISCIPLINE_ITEM_TAG_SETTING ORDER BY TagID";
$APTagID = $ldiscipline->returnArray($sql, 2);

$sql = "SELECT conductString, UserID FROM DISCIPLINE_MS_STUDENT_CONDUCT WHERE AcademicYearID='$year' AND YearTermID='$semester' AND StudentID=$uid";
$data = $ldiscipline->returnArray($sql, 2);

for($i=0; $i<sizeof($data); $i++) {
	$teacherID = $data[$i][1];
	$tempGrade = explode(",",$data[$i][0]);
	for($j=0; $j<sizeof($tempGrade); $j++) {
		$g = $tempGrade[$j];
		$line = explode(":",$g);
		list ($catid, $gradeNew) = $line;
		if($catid!="" && $uid!="")	
			$conductGrade[$teacherID][$catid] = $gradeNew;
	}
}

?>

<table width="100%" cellpadding="3" cellspacing="0" border="0">
	<tr>
		<td class='tablebluetop tabletoplink'>&nbsp;</td>
		<?
		for($i=0; $i<sizeof($APTagID); $i++) {
			echo "<td class='tablebluetop tabletopnolink'>".intranet_htmlspecialchars($APTagID[$i]['TagName'])."</td>";	
		}
		?>
	</tr>
	<?
	
	$subjectGroupTeacherIDs = $ldiscipline->getSubjectGroupTeacherList($year, $semester, $uid);
	$classTeacherIDs = $ldiscipline->getClassTeacherByStudentID($uid, $year);
	$stdInfo = $ldiscipline->getStudentNameByID($uid);
	

	//$yearID = Get_Current_Academic_Year_ID();
	$yearID = $year;
	$name_field = getNameFieldByLang('USR.');
	
	$conds = (sizeof($subjectGroupTeacherIDs)>0 && ($ldiscipline->displayOtherTeacherGradeInConductGrade || $ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || in_array($_SESSION['UserID'],$classTeacherIDs))) ? " OR (STCT.UserID IN (".implode(',',$subjectGroupTeacherIDs)."))" : "";
	$sql = "SELECT 
				USR.UserID, $name_field as name 
			FROM 
				INTRANET_USER USR LEFT OUTER JOIN 
				YEAR_CLASS_TEACHER CLSTCH ON (USR.UserID=CLSTCH.UserID) LEFT OUTER JOIN 
				YEAR_CLASS CLS ON (CLS.YearClassID=CLSTCH.YearClassID AND CLS.AcademicYearID=$yearID) LEFT OUTER JOIN
				SUBJECT_TERM_CLASS_TEACHER STCT ON (STCT.UserID=USR.UserID) LEFT OUTER JOIN
				SUBJECT_TERM_CLASS_USER STCU ON (STCT.SubjectGroupID=STCU.SubjectGroupID)
				
			WHERE 
				((CLS.ClassTitleEN='".$stdInfo[0]['ClassName']."' OR CLS.ClassTitleB5='".$stdInfo[0]['ClassName']."') $conds) AND 
				USR.RecordType=1 AND
				(CLSTCH.UserID!=".$_SESSION['UserID']." OR STCT.UserID!=".$_SESSION['UserID'].")
				
			GROUP BY USR.UserID
			ORDER BY name
			";
		
	
	$data = $ldiscipline->returnArray($sql, 2);

	
	for($i=0; $i<sizeof($data); $i++) {
		$css = ($i%2==0) ? 1 : 2;
		echo "<tr class='tablebluerow$css'><td class='tabletext'>";
		if(in_array($data[$i][0], $classTeacherIDs)) 
			echo "<span class='tabletextrequire'>*</span>";
		echo $data[$i][1]."</td>";
		for($j=0; $j<sizeof($APTagID); $j++) {
			echo "<td class='tabletext'>".$conductGrade[$data[$i][0]][$APTagID[$j]['TagID']]."</td>";	
		}
		echo "</tr>";	
	}
	echo "<tr><td colspan='2' class='tabletextremark'><span class='tabletextrequire'>*</span>$i_Teaching_ClassTeacher</td></tr>";
	?>
</table>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();

?>