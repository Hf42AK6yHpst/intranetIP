<?php
# modifying by : henry chow

$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdiscipline.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libclass.php");
$lclass = new libclass();
$linterface = new interface_html("popup.html");

$select_class = $lclass->getSelectClass("name=\"targetClass\" onChange=\"this.form.action='';this.form.submit()\"",$targetClass);
    
$select_students = $lclass->getStudentSelectByClass($targetClass,"size=\"21\" multiple=\"multiple\" name=\"targetID[]\"");

$permitted[] = 2;

$li = new libgrouping();

if($CatID < 0){
     unset($ChooseGroupID);
     $ChooseGroupID[0] = 0-$CatID;
}
$lgroupcat = new libgroupcategory();
$cats = $lgroupcat->returnAllCat();

$x1  = ($CatID!=0 && $CatID > 0) ? "<select name='CatID' onChange='checkOptionNone(this.form.elements[\"ChooseGroupID[]\"]);this.form.submit()'>\n" : "<select name='CatID' onChange='this.form.submit()'>\n";
$x1 .= "<option value='0'></option>\n";
for ($i=0; $i<sizeof($cats); $i++)
{
     list($id,$name) = $cats[$i];
     if ($id!=0)
     {
         $x1 .= "<option value='$id' ".(($CatID==$id)?"SELECTED":"").">$name</option>\n";
     }
}

$x1 .= "<option value='0'></option>\n";
$x1 .= "</select>";

if($CatID!=0 && $CatID > 0) {
     $row = $li->returnCategoryGroups($CatID);
     $x2  = "<select name='ChooseGroupID[]' size='5' multiple>\n";
     for($i=0; $i<sizeof($row); $i++){
          $GroupCatID = $row[$i][0];
          $GroupCatName = $row[$i][1];
          $x2 .= "<option value='$GroupCatID'";
          for($j=0; $j<sizeof($ChooseGroupID); $j++){
          $x2 .= ($GroupCatID == $ChooseGroupID[$j]) ? " SELECTED" : "";
          }
          $x2 .= ">$GroupCatName</option>\n";
     }
     $x2 .= "</select>\n";
}

if(isset($ChooseGroupID)) {
     $row = $li->returnGroupUsersInIdentity($ChooseGroupID, $permitted);
     $x3  = "<select name='ChooseUserID[]' size='5' multiple>\n";
     for($i=0; $i<sizeof($row); $i++)
     $x3 .= "<option value='".$row[$i][0]."'>".$row[$i][1]."</option>\n";
     $x3 .= "</select>\n";
}
    
################################################################
$MODULE_OBJ['title'] = $iDiscipline['select_students'];
$linterface->LAYOUT_START();
?>

<script language="javascript">
/*
function AddOptions(obj){
     par = opener.window;
     parObj = opener.window.document.form1.elements["<?=$fieldname; ?>"];

     checkOption(obj);
     par.checkOption(parObj);
     i = obj.selectedIndex;

     while(i!=-1)
        {
         addtext = obj.options[i].text;

          par.checkOptionAdd(parObj, addtext, obj.options[i].value);
          obj.options[i] = null;
          i = obj.selectedIndex;
     }
     par.checkOptionAdd(parObj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
     par.form1.flag.value = 0;
     //par.generalFormSubmitCheck(par.form1);
}
*/
function AddOptions(obj){
     par = opener.window;
     parObj = opener.window.document.form1.elements["<?php echo $fieldname; ?>"];
     x = (obj.name == "ChooseGroupID[]") ? "G" : "U";
     checkOption(obj);
     par.checkOption(parObj);
     i = obj.selectedIndex;
     while(i!=-1){
          par.checkOptionAdd(parObj, obj.options[i].text, x + obj.options[i].value);
          obj.options[i] = null;
          i = obj.selectedIndex;
     }
     //par.checkOptionAdd(parObj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
}
function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}

</script>

<form name="form1" action="" method="post">
<table border="0" cellpadding="0" cellspacing="0" align="center" width="90%">
<tr>
<td align="center">

<table width="95%" border="0" cellpadding="5" cellspacing="1">
<tr>
	<td>
	<table width="100%" cellpadding="0" cellspacing="0" border="0">

			

					<tr>
			<td valign="top" nowrap="nowrap" width="30%" class='formfieldtitle'>
			<span class="tabletext"><?=$i_frontpage_campusmail_select_category?></span>
			</td>
			<td >
			<?=$x1?>
			</td>
		</tr>
                <?php if($CatID!=0 && $CatID > 0) { ?>
                <tr>
			<td valign="top" nowrap="nowrap" width="30%" class='formfieldtitle'>
			<span class="tabletext"><?=$i_frontpage_campusmail_select_group?></span>
			</td>
			<td >
                        
                        <table border='0' cellspacing='1' cellpadding='1'>
        		<tr>
                        	<td><?=$x2?></td>
                        	<td>
                                        <?= $linterface->GET_BTN($button_add, "submit","checkOption(this.form.elements['ChooseGroupID[]']);AddOptions(this.form.elements['ChooseGroupID[]'])") . "<br>"; ?>
                                        <?= $linterface->GET_BTN($i_frontpage_campusmail_expand, "submit","checkOption(this.form.elements['ChooseGroupID[]'])") . "<br>"; ?>
                                        <?= $linterface->GET_BTN($button_select_all, "submit","SelectAll(this.form.elements['ChooseGroupID[]']); return false;") . "<br>"; ?>
				</td>
			</tr>
                        </table>
                                                                
			</td>
		</tr>
                <?php } ?>
                
                <?php if(isset($ChooseGroupID)) { ?>
                <tr>
			<td valign="top" nowrap="nowrap" width="30%" class='formfieldtitle'>
			<span class="tabletext"><?=$i_frontpage_campusmail_select_user?></span>
			</td>
			<td >
                        
                        <table border='0' cellspacing='1' cellpadding='1'>
        		<tr>
                        	<td><?=$x3?></td>
                        	<td>
                                        <?= $linterface->GET_BTN($button_add, "submit","checkOption(this.form.elements['ChooseUserID[]']);AddOptions(this.form.elements['ChooseUserID[]'])") . "<br>"; ?>
					<?= $linterface->GET_BTN($button_select_all, "submit","SelectAll(this.form.elements['ChooseUserID[]']); return false;") . "<br>"; ?>
				</td>
			</tr>
                        </table>
                                                                
			</td>
		</tr>
                <?php } ?>

	</table>
</tr>

</table>


</td>
</tr>

<tr><td>

<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr>
	<td align="center"><?= $linterface->GET_ACTION_BTN($button_close, "button", "javascript:self.close()")?>
	</td>
</tr>
</table>
<br/>

</td></tr>

</table>
<input type="hidden" name="fieldname" value="<?php echo $fieldname; ?>" />
<input type="hidden" name="ppl_type" value="<?php echo $ppl_type; ?>" />
<input type="hidden" name="type" value="<?php echo $type; ?>" />
<input type="hidden" name="EnrolEventID" value="<?php echo $EnrolEventID; ?>" />

</form>

<script language="javascript">
if (document.form1.elements['targetID[]'])
	SelectAll(document.form1.elements['targetID[]']);
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>