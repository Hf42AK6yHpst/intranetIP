<?php 
// Using : yat

################################
#
#	Date:	2012-08-27	YatWoon
#			fix incorrect wording display at header
#
################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldisciplinev12 = new libdisciplinev12();
$ldisciplinev12->CONTROL_ACCESS("Discipline-MGMT-Conduct_Mark-Adjust");


# prepare year array for generating year selection box
$yearInfo = GetAllAcademicYearInfo();
$yearArr = array();
for($i=0; $i<sizeof($yearInfo); $i++) {
	list($year_id, $yearEN, $yearB5) = $yearInfo[$i];
	$yearArr[$i][] = $year_id;
	$yearArr[$i][] = Get_Lang_Selection($yearB5, $yearEN);
}

$current_year = Get_Current_Academic_Year_ID();
if(trim($Year)=='')
{
	$Year = $current_year;
}
$yearSelection = "<select id=\"Year\", name=\"Year\" onChange=\"change_field('ajaxYear',this.value); js_Reload_Term_Selection(this.value);\">";
for($a=0;$a<sizeof($yearArr);$a++)
{
	$default = ($yearArr[$a][0]==$current_year)?"selected":"";
	$yearSelection .="<option value=\"".$yearArr[$a][0]."\" $default>".$yearArr[$a][1]."</option>";
}
$yearSelection .= "</select>";

$yearID = $Year;

# get semester data from file, prepare semester conversation from number to text. ie: 1 -> First semester
$semester_data = getSemesters($yearID);

# Study Score calculation method
$StudyScoreCalculationMethod = $ldisciplinev12->retriveStudyScoreCalculationMethod();
if($StudyScoreCalculationMethod==0) {	// calculate by term
	$semArr[] = array('0',$ec_iPortfolio['whole_year']);
}

$number = sizeof($semester_data);

if($number>0)
{	
	foreach($semester_data as $id=>$name)
	{
		$semArr[] = array($id, $name);
	}
}
$semSelection = $linterface->GET_SELECTION_BOX($semArr, 'id="Semester", name="Semester" onChange="change_field(\'ajaxSemester\',this.value)"', "", "");

# Top menu highlight setting
$CurrentPage = "Management_StudyScore";

$TAGS_OBJ[] = array($i_Discipline_System_Subscore1);

$PAGE_NAVIGATION[] = array($eDiscipline["RecordList"], "index.php");
$PAGE_NAVIGATION[] = array($Lang['eDiscipline']['ComputeStudyScoreGrade']);

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldisciplinev12->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

?>
<script language="javascript">

var callback2 = {
    success: function ( o )
    {       
		document.getElementById('LastGenDiv').innerHTML = o.responseText;
	}
}

function change_field(field,parValue)
{
	document.getElementById(field).value = parValue;
	document.getElementById('task').value = 'check_gen_time';
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);	
    var path;
    path = "ajax_subscore.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback2);
}

function js_Reload_Term_Selection(jsAcademicYearID)
{
	$('#termSelectionDiv').load(
		"ajax_subscore.php", 
		{ 
			task: 'reload_term_selection',
			AcademicYearID: jsAcademicYearID
		},
		function(ReturnData)
		{
			change_field('ajaxSemester', 0);
		}
	);
}
</script>
<br />
<form name="form1" method="post" action="generate_update.php">

			<table width="98%" border="0" cellspacing="0" cellpadding="1">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr><td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
						<td align="right"><?= $linterface->GET_SYS_MSG($xmsg) ?></td></tr>
						</table>
					</td>
				</tr>
				
				
				<tr>
					<td align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr><td align="center">
								<fieldset class="instruction_box">
										<legend class="instruction_title"><?=$i_Discipline_System_Discipline_Conduct_Mark_Generation_Instruction1?></legend>
										<?=$i_Discipline_System_Discipline_Conduct_Mark_Generation_Instruction2?><br>
										<span class="tabletextrequire"><?=$i_Discipline_System_Discipline_Conduct_Mark_Generation_Instruction3?></span>
								</fieldset>
					</td></tr>
					</table>
					</td>
				</tr>
				<tr>
					<td height="10px"></td>
				</tr>
				<tr>
					<td align="center">
						<table width="80%" border="0" cellspacing="5" cellpadding="5" align="center">
							<tr valign="top">
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Profile_Year?></td>
								<td width="80%"><?=$yearSelection?></td>
							</tr>
							<tr valign="top">
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Profile_Semester?></td>
								<td width="80%" class="tabletextremark"><div id="termSelectionDiv"><?=$semSelection?></div></td>
							</tr>
							<tr valign="top">
								<td>&nbsp;</td>
								<td width="80%" class="tabletextremark"><?=$i_Discipline_System_Discipline_Conduct_Last_Generated?> : <span id="LastGenDiv"><?=$LastGen[0]?></div></td>
							</tr>
							<tr><td colspan="2"><span class="tabletextremark"><?=$i_general_required_field?></td></tr>
						</table>
					</td>
				</tr>
				
				<tr>
					<td colspan="2" class="dotline"><img src="<?=$image_path;?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td height="10px"></td>
				</tr>
				<tr><td colspan="2" align="center">
					<? echo $linterface->GET_ACTION_BTN($button_submit, "submit"); ?>
					<? echo $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\""); ?>
					<? echo $linterface->GET_ACTION_BTN($button_back, "button", "","back"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" onClick=\"history.back()\""); ?>
				</td></tr>
				</table><br>
				<?=$HiddenValue?>
				<input type="hidden" name="ajaxYear" id="ajaxYear" />
				<input type="hidden" name="task" id="task" />
				<input type="hidden" name="ajaxSemester" id="ajaxSemester" />
				<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
				<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
				<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
				<input type="hidden" name="page_size_change" value="" />
				<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
<?php
//if($Year=='' && $Semester=='')
{
	?>
<script>
change_field('ajaxYear','<?=$Year?>');
change_field('ajaxSemester','<?=$semArr[0][0]?>');
</script>
<?
}
?>
</form>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>