<?php

#############################################
#
#	Date	:	2019-05-13  (Bill)
#	            Prevent SQL Injection
#
#############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldisciplinev12 = new libdisciplinev12();
$ldisciplinev12->CONTROL_ACCESS("Discipline-MGMT-Conduct_Mark-Adjust");

### Handle SQL Injection + XSS [START]
$AcademicYearID = IntegerSafe($Year);
$YearTermID = IntegerSafe($Semester);

if($YearTermID=='0')
{
	$IsAnnual = 1;
	$parSemester = null;
	$conds .= " AND IsAnnual = 1";
}
else
{
	$parSemester = $YearTermID;
	//$conds .= " AND Semester = '$YearTermID'";
	$conds .= " AND YearTermID = '$YearTermID'";
}
### Handle SQL Injection + XSS [END]

# Get all students
$sql = "select distinct UserID from INTRANET_USER where RecordType=2 and RecordStatus=1 ";
$StudentsArr = $ldisciplinev12->returnArray($sql);
for($a=0; $a<sizeof($StudentsArr); $a++)
{
	$array_students[] = $StudentsArr[$a]['UserID'];
}

# Generate whole year
if($IsAnnual)
{
	if($AcademicYearID != '')
	{	# when generate whole year of a selected year
		$student_data = $ldisciplinev12->retrieveAllSemesterStudyScoreBalanceGradeForGeneration($array_students, $AcademicYearID);
	}
	else
	{
		# when generate all year, whole year	
		$student_data = $ldisciplinev12->retrieveAllSemesterAllYearStudyScoreBalanceGradeForGeneration($array_students, $AcademicYearID);
	}
	for ($i=0; $i<sizeof($student_data); $i++)
	{
		$student_id = $student_data[$i]['StudentID'];
		$student_data_annual[$student_id][] = $student_data[$i];
	}
	
	if(is_array($student_data_annual))
	{
		foreach($student_data_annual as $Key=>$Value)
		{
			$total_score = '';
			$ratio = '';
			$total_ratio = '';
			
			# Calculate the conduct score for each semester by multiplying them to the ratio
			for($a=0; $a<sizeof($Value); $a++)
			{
				list($t_student_id, $t_conduct_year ,$t_conduct_semester,$t_study_score, $t_grade_char) = $Value[$a];
				$ratio = $ldisciplinev12->getSemesterRatio('', '', $t_conduct_year, $t_conduct_semester, 'S');
// 				$ratio = 1;
				
				if($ratio != 0)
				{
					$t_study_score *= $ratio;
				    
					# Add all conduct score to total 
					$total_score[$t_conduct_year][] = $t_study_score;
					$total_ratio += $ratio;
				}
			}
			//echo $total_ratio;
			//echo $total_score.'/'.$ratio;
			
			if(is_array($total_score))
			{
				foreach($total_score as $Key=>$Value)
				{
					# Get this student old annual grade
					$t_grade_char_array = $ldisciplinev12->retrieveStudentStudyScoreGrade($t_student_id, $Key,'',1);
					$t_grade_char = $t_grade_char_array[$t_student_id];
					
					# Add up the conduct score in all semesters
					$sum = 0;
					for($a=0;$a<sizeof($Value);$a++)
					{
						$sum+=$Value[$a];	
					}
					
					# Get the average of the total score by dividing the number of semester
					$pre_adj_total_score_avg = round($sum / $total_ratio);
					//echo $sum.'/'.$total_ratio.'<br>';
                    
					# Get this student annual conduct score
					$tmp_studentid[0] = $t_student_id;
					$ldisciplinev12_annual_record = $ldisciplinev12->retrieveAnnualStudyScoreBalanceGradeForGeneration($tmp_studentid, $Key);
					$ldisciplinev12_annual_record_StudyScore = $ldisciplinev12_annual_record[0]['ConductScore'];
					
					# Add up semester conduct score and annual conduct score
					$total_score_avg = $pre_adj_total_score_avg + $ldisciplinev12_annual_record_StudyScore;
					
					# Get the grade
					$new_grade_char = $ldisciplinev12->getStudyScoreGradeFromScore($total_score_avg,$Key,$t_conduct_semester);
        			// if ($t_grade_char != $new_grade_char)
					{
						$sql = "UPDATE DISCIPLINE_STUDENT_SUBSCORE_BALANCE
						SET GradeChar = '$new_grade_char', SubScore = '$pre_adj_total_score_avg'
						WHERE StudentID = '$t_student_id'
						AND AcademicYearID = '$Key' AND IsAnnual=1";
						$ldisciplinev12->db_db_query($sql);
					}
					//echo $sql.'<br>';
				}
			}
		}
	}
	//exit;
}
else
{
	if($AcademicYearID != '')
	{
		if($ldisciplinev12->retriveStudyScoreCalculationMethod() == 1)
		{
			$sql = "SELECT YearTermID FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '".$AcademicYearID."' ORDER BY YearTermID DESC";
			$MaxYearTermID = $ldisciplinev12->returnVector($sql);
			
			if($MaxYearTermID[0] == $YearTermID)		
			{
				# if the select YearTermID is the Final Year Term, udpate / create subscoreBalance record
				for($i=0; $i<sizeof($array_students); $i++)
				{
					$sql = "SELECT COUNT(*) FROM DISCIPLINE_STUDENT_SUBSCORE_BALANCE WHERE AcademicYearID = '".$AcademicYearID."' AND StudentID = '".$array_students[$i]."' AND IsAnnual = 1";
					$IsAnnualExist = $ldisciplinev12->returnVector($sql);
					
					// Get the Conduct mark for the final year term
					$sql = "SELECT SubScore FROM DISCIPLINE_STUDENT_SUBSCORE_BALANCE WHERE AcademicYearID = '".$AcademicYearID."' AND StudentID = '".$array_students[$i]."' AND YearTermID = '".$YearTermID."'";
					$LatestStudyScoreMark = $ldisciplinev12->returnVector($sql);
					
					if($IsAnnualExist[0] == 0)
					{
						$sql = "INSERT INTO DISCIPLINE_STUDENT_SUBSCORE_BALANCE(StudentID, IsAnnual, SubScore, AcademicYearID, YearTermID) VALUES ('".$array_students[$i]."',1,'".$LatestStudyScoreMark[0]."','$AcademicYearID','0')";
						$ldisciplinev12->db_db_query($sql);
					}
					else
					{
						$sql = "UPDATE DISCIPLINE_STUDENT_SUBSCORE_BALANCE SET SubScore = '".$LatestStudyScoreMark[0]."' WHERE StudentID = '".$array_students[$i]."' AND AcademicYearID = '".$AcademicYearID."' AND IsAnnual = 1";
						$ldisciplinev12->db_db_query($sql);
					}
				}
			}
		}
		
		$student_data = $ldisciplinev12->retrieveSemesterStudyScoreBalanceGradeForGeneration($array_students,$AcademicYearID,$YearTermID);
	}
	else
	{
		$student_data = $ldisciplinev12->retrieveSemesterAllYearStudyScoreBalanceGradeForGeneration($array_students,$YearTermID);
	}
	
	for ($i=0; $i<sizeof($student_data); $i++)
	{
		list($t_student_id, $t_conduct_year, $t_conduct_semester,$t_conduct_score, $t_grade_char) = $student_data[$i];
		$new_grade_char = $ldisciplinev12->getStudyScoreGradeFromScore($t_conduct_score,$t_conduct_year);
		
		if($ldisciplinev12->retriveStudyScoreCalculationMethod() == 1)
		{
			$sql = "UPDATE DISCIPLINE_STUDENT_SUBSCORE_BALANCE
					SET GradeChar = '$new_grade_char'
					WHERE StudentID = '$t_student_id'
					AND AcademicYearID = '$t_conduct_year' AND YearTermID= '$t_conduct_semester'";
			$ldisciplinev12->db_db_query($sql);
			
			$sql = "SELECT YearTermID FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '$t_conduct_year' ORDER BY TermStart DESC";
			$LastTermID = $ldisciplinev12->returnVector($sql);
			if($LastTermID[0] == $t_conduct_semester)
			{
				$sql = "UPDATE DISCIPLINE_STUDENT_SUBSCORE_BALANCE
						SET GradeChar = '$new_grade_char'
						WHERE StudentID = '$t_student_id'
						AND AcademicYearID = '$t_conduct_year' AND IsAnnual = 1";
				$ldisciplinev12->db_db_query($sql);
			}
		}
		else
		{
			if ($t_grade_char != $new_grade_char)
			{
				$sql = "UPDATE DISCIPLINE_STUDENT_SUBSCORE_BALANCE
				SET GradeChar = '$new_grade_char'
				WHERE StudentID = '$t_student_id'
				AND AcademicYearID = '$t_conduct_year' AND YearTermID= '$t_conduct_semester'";
				$ldisciplinev12->db_db_query($sql);
			}
		}
	}
}

# Update generation time
if($YearTermID == '0' || $YearTermID=='')
{
	$redirect_semester = '';
	$gen_semester = "Annual";
}
else
{
	$gen_semester = $YearTermID;
	$redirect_semester = $YearTermID;	
}

if($AcademicYearID == '0')
{
	$gen_year = "Year";
}
else
{
	$gen_year = $AcademicYearID;	
}
$ldisciplinev12->updateGenerateActionRecord($gen_year, $gen_semester, "study_score_gen_");

header("Location: index.php?xmsg=grade_generated&Year=$AcademicYearID&Semester=$redirect_semester");

intranet_closedb();
?>