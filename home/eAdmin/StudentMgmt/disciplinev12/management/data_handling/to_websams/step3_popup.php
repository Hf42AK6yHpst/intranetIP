<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();
$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$MODULE_OBJ['title'] = $eDiscipline['Websams_Transition'];
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr><td class="tabletext" align="center">&nbsp;</td></tr>
		<tr><td class="tabletext" align="center">&nbsp;</td></tr>
		<tr><td class="tabletext" align="center"><?=$eDiscipline['Websams_Transition_Step3_PopUpWarning']?></td></tr>
	</table>
	
	<table width="95%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td height="1" class="dotline"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="2">
					<tr><td class="tabletext" align="center">
						<?= $linterface->GET_ACTION_BTN($eDiscipline['StartDataTransfer'], "button", "window.opener.TriggerFormSubmit()") ?>
					</td></tr>
				</table>
			</td>
		</tr>
	</table>

	
	<br />
	
	
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
