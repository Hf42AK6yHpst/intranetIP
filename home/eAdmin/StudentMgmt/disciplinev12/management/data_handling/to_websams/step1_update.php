<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");


intranet_auth();
intranet_opendb();
$ldiscipline = new libdisciplinev12();
$limport = new libimporttext();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$AgreedDisclaimer = $_POST['AgreedDisclaimer'];
if ($AgreedDisclaimer != 1) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT($Lang['eDiscipline']['NotYetAgreeDisclaimer'], 'disclaimer.php');
}

$currentDateTime = date("Y-m-d H:i:s");
$currentDateTimeUnixTimestamp = strtotime($currentDateTime);
$fs = new libfilesystem();

if(!empty($userfile)) {
	# open folder if not exist
	$folder_prefix = $ldiscipline->WebSAMS_FilesPath;
	if (!file_exists($folder_prefix))
		$fs->folder_new($folder_prefix);
		
	# Save the URL to a text file
	$url_file = $ldiscipline->WebSAMS_FilesPath."/url.txt";
	$fs->file_write($url, $url_file);
	
	# CSV File Handling
	$loc = $userfile;
	$filename = $userfile_name;
	$success = 0;
	
	if($filename!="") {
		$ext = $fs->file_ext($filename);
		if($ext==".csv") {
			if ($loc!="none" && file_exists($loc)) {
				//$data = $fs->file_read_csv($loc);
				$data = $limport->GET_IMPORT_TXT($loc);
				
				$data = array_remove_empty($data);
				
				if(!empty($data) && count($data) > 1) {
					
					list($title1, $title2) = $data[0];
					$title1 = strtolower($title1);
					$title2 = strtolower($title2);
					
					if ($title1=="stuid" && $title2=="regno")
					{
						$numWebSAMSRegNoInCSV = count($data) - 1;
						
						# File name format: timestamp_UserID.csv
						$filename = $currentDateTimeUnixTimestamp."_".$_SESSION['UserID'].substr($filename, strpos($filename, "."));
						
						$success = $fs->file_copy($loc, stripslashes($folder_prefix."/".$filename));
						
						if (!$success)
							$Result = "Result=uploadFailed";
					}
					else
					{
						$success = 0;
						$Result = "Result=wrongHeader";
					}
				}
				else
				{
					$success = 0;
					$Result = "Result=CsvNoData";
				}
			}
		}
		else
		{
			$success = 0;
			$Result = "Result=wrongExtension";
		}
	}
	
	if (!$success) {
		$URL = "step1.php?".$Result;
		header("Location: $URL");
	}
}

if(!empty($teacherfile)) {
	# open folder if not exist
	$folder_prefix = $ldiscipline->WebSAMS_FilesPath;
	if (!file_exists($folder_prefix))
		$fs->folder_new($folder_prefix);
		
	# CSV File Handling
	$teacher_loc = $teacherfile;
	$teacher_filename = $teacherfile_name;
	$success = 0;
	
	if($teacher_filename!="") {
		$ext = $fs->file_ext($teacher_filename);
		if($ext==".csv") {
			if ($teacher_loc!="none" && file_exists($teacher_loc)) {
				//$data = $fs->file_read_csv($teacher_loc);
				$data = $limport->GET_IMPORT_TXT($teacher_loc);
				$data = array_remove_empty($data);
				
				if(!empty($data) && count($data) > 1) {
					
					list($title1, $title2) = $data[0];
					$title1 = strtolower($title1);
					$title2 = strtolower($title2);
					
					if ($title1=="eclass_teacherid" && $title2=="websams_teacherid")
					{
						# File name format: timestamp_UserID.csv
						$teacher_filename = $currentDateTimeUnixTimestamp."_".$_SESSION['UserID']."_teacher_id".substr($teacher_filename, strpos($teacher_filename, "."));
						
						$success = $fs->file_copy($teacher_loc, stripslashes($folder_prefix."/".$teacher_filename));
						
						if (!$success)
							$Result = "Result=uploadFailed";
					}
					else
					{
						$success = 0;
						$Result = "Result=wrongHeader";
					}
				}
				else
				{
					$success = 0;
					$Result = "Result=CsvNoData";
				}
			}
		}
		else
		{
			$success = 0;
			$Result = "Result=wrongExtension";
		}
	}
	
	if (!$success) {
		$URL = "step1.php?".$Result;
		header("Location: $URL");
	}
}


intranet_closedb();
?>

	<? if ($success) { ?>
	<html>
		<body>
			<form name="form1" id="form1" method="post" action="step2.php">
				<input type="hidden" name="url" value="<?=$url?>" />
				<input type="hidden" name="filename" value="<?=$filename?>" />
				<input type="hidden" name="teacher_filename" value="<?=$teacher_filename?>" />
				<input type="hidden" name="numWebSAMSRegNoInCSV" value="<?=$numWebSAMSRegNoInCSV?>" />
				<input type="hidden" id="AgreedDisclaimer" name="AgreedDisclaimer" value="<?=$AgreedDisclaimer?>" />
			</form>
		
			<script language="javascript">
				document.getElementById("form1").submit();
			</script>
		</body>
	</html>
	<? } ?>
