<?php

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

# Get the Merit Record of current acadermic year which is not transferred to WebSAMS
# Set the DateOfTransferToWebSAMS of the record to now
# Construct a WebSAMS form using the data
# Return the form

# initialization
$ldiscipline = new libdisciplinev12();
$filename = $_POST['filename'];
$AcademicYearID = $_POST['AcademicYearID'];
$numWebSAMSRegNoInCSV = $_POST['numWebSAMSRegNoInCSV'];
$RecordTypeStr = $_POST['RecordTypeStr'];
$chkPrintInd = $_POST['chkPrintInd'];
$chkSLPReadInd = $_POST['chkSLPReadInd'];
$isLast = $_POST['isLast'];
$transferCount = $_POST['transferCount'];

$WebSAMS_StuID_MappingArr = unserialize(rawurldecode($WebSAMS_StuID_MappingArr));
$WebSAMS_eClassStuID_MappingArr = unserialize(rawurldecode($WebSAMS_eClassStuID_MappingArr));
$WebSAMS_TeacherID_MappingArr = unserialize(rawurldecode($WebSAMS_TeacherID_MappingArr));

$returnString = "";

$RecordTypeArr = explode(",", $RecordTypeStr);

if (in_array("Award", $RecordTypeArr) && in_array("Punishment", $RecordTypeArr))
{
	$RecordType = "";
}
else if (in_array("Award", $RecordTypeArr))
{
	$RecordType = "Award";
}
else if (in_array("Punishment", $RecordTypeArr))
{
	$RecordType = "Punishment";
}

// for testing in 52001
//$DisciplineRecordArr = $ldiscipline->Get_Info_of_AP_Records($RecordType, DISCIPLINE_STATUS_APPROVED, $HasTransferredToWebSAMS=0, $AcademicYearID, $fields="*", $studentID=102006, $limit=1);
$DisciplineRecordArr = $ldiscipline->Get_Info_of_AP_Records($RecordType, DISCIPLINE_STATUS_APPROVED, $HasTransferredToWebSAMS=0, $AcademicYearID, $fields="*", $WebSAMS_eClassStuID_MappingArr, $limit=1);

$thisRecordID = $DisciplineRecordArr[0]['RecordID'];
$thisStudentID = $DisciplineRecordArr[0]['StudentID'];
$thisMeritType = $DisciplineRecordArr[0]['MeritType'];
$thisProfileMeritType = $DisciplineRecordArr[0]['ProfileMeritType'];
$thisMeritCount = $DisciplineRecordArr[0]['ProfileMeritCount'];
$thisRecordDate = $DisciplineRecordArr[0]['RecordDate'];
$thisRemark = $DisciplineRecordArr[0]['Remark'];

# Staff Code and Name
$thisPICID_List = $DisciplineRecordArr[0]['PICID'];
$thisPICID_Arr = explode(',', $thisPICID_List);
$this_eClass_PICID = $thisPICID_Arr[0];		// Record the first PIC only

$this_WebSAMS_PICID = $WebSAMS_TeacherID_MappingArr[$this_eClass_PICID];
$name_field = getNameFieldByLang();
$sql = "SELECT $name_field FROM INTRANET_USER WHERE UserID = '$this_eClass_PICID'";
$StaffNameArr = $ldiscipline->returnVector($sql);
$thisStaffName = $StaffNameArr[0];

$success = $ldiscipline->Set_HasTransferredToWebSAMS($thisRecordID, 1);

# Map WebSAMSRegNo in IP to StuID in WebSAMS
$sql = "SELECT WebSAMSRegNo FROM INTRANET_USER WHERE UserID = '$thisStudentID'";
$resultArr = $ldiscipline->returnVector($sql);
$intranet_WebSAMSRegNo = str_replace("#", "", $resultArr[0]);
$WebSAMS_StuID = $WebSAMS_StuID_MappingArr[$intranet_WebSAMSRegNo];

if (!$success)
{
	$returnString = "invalid";
	$reason = "Cannot Set HasTransferredToWebSAMS to 1";
}
else if ($intranet_WebSAMSRegNo == "")
{
	$returnString = "invalid";
	$reason = "No WebSAMS number in IP";
}
else if ($WebSAMS_StuID=="")
{
	$returnString = "invalid";
	$reason = "No Mapped STUID in WebSAMS";
}
else
{
	# Build info array to contruct WebSAMS form
	$FormDataArr["stuID"] = $WebSAMS_StuID;
	$FormDataArr["anpDate"] = $ldiscipline->convertDateFormat($thisRecordDate);
	$FormDataArr["chkPrintInd"] = ($chkPrintInd=="on")? "checked" : "";
	$FormDataArr["chkSLPReadInd"] = ($chkSLPReadInd=="on")? "checked" : "";
	$FormDataArr["remarks"] = $thisRemark;
	$FormDataArr["descriptionStr"] = $ldiscipline->Get_Record_Description($thisRecordID);
	$FormDataArr["descriptionCode"] = $ldiscipline->Get_Item_Code_By_RecordID($thisRecordID);
	$FormDataArr["staffCode"] = $this_WebSAMS_PICID;
	$FormDataArr["staffName"] = $thisStaffName;
	
	# Set Merit type and numbers
	for ($i=1; $i<=5; $i++)
	{
		$level = abs($thisProfileMeritType);
		
		if ($level == $i)
		{
			$FormDataArr["lvl".$i] = $ldiscipline->TrimDeMeritNumber_00($thisMeritCount);
		}
		else
		{
			$FormDataArr["lvl".$i] = 0;
		}
	}
	
	if ($thisMeritType == 1)
	{
		# Award
		//$FormDataArr["prefix"] = "�\�\�\ 111";		
		//$FormDataArr["position"] = "�\�\�\ 111";
		//$FormDataArr["slpRemarks"] = "�\�\�\ 111";
		//$FormDataArr["awardFrom"] = "1";
		//$FormDataArr["awardFromText"] = "�դ�";
		//$FormDataArr["awardCat"] = "1";
		//$FormDataArr["awardCatText"] = "�ǳN";
		//$FormDataArr["staffCode"] = "1";
		//$FormDataArr["staffName"] = "������";
		$returnString = $ldiscipline->Generate_Websams_Merit_Form("websams_form", $FormDataArr);
		$ldiscipline->Update_Transfer_DateTime_To_WebSAMS($thisRecordID);
	}
	else if ($thisMeritType == -1)
	{
		# Punishment
		//$FormDataArr["actionTaken"] = "01";
		//$FormDataArr["actionTakenText"] = "�d��";
		//$FormDataArr["fromDate"] = "11/02/2009";
		//$FormDataArr["toDate"] = "12/02/2009";
		//$FormDataArr["includeInClass"] = "checked";		# "checked" or ""
		//$FormDataArr["staffCode"] = "01";
		//$FormDataArr["staffName"] = "������";
		$returnString = $ldiscipline->Generate_Websams_Demerit_Form("websams_form", $FormDataArr);
		$ldiscipline->Update_Transfer_DateTime_To_WebSAMS($thisRecordID);
	}
	else
	{
		$returnString = "invaild";
		$reason = "Wrong Merit Type. Merit Type = ".$thisMeritType;
	}
}


# Log Process of data transfer to WebSAMS
$fs = new libfilesystem();
$logFileName = substr($filename, 0, strlen($filename)-4).".txt";
$logFilePath = $ldiscipline->WebSAMS_FilesPath."/$logFileName";

# open folder if not exist
$folder_prefix = $ldiscipline->WebSAMS_FilesPath;
if (!file_exists($folder_prefix))
	$fs->folder_new($folder_prefix);

$existingRecords = get_file_content($logFilePath);

if ($returnString == "invalid")
{
	$msg = $transferCount."\t".$thisRecordID."\t\t".$thisStudentID."\t\t".$reason."\n";
	
}
else
{
	$msg = $transferCount."\t".$thisRecordID."\t\t".$thisStudentID."\t\t"."Successful\n";
}
$fs->file_write($existingRecords.$msg, $logFilePath);

if ($isLast)
{
	# Reset all HasTransferredToWebSAMS to 0
	/////$ldiscipline->Reset_All_HasTransferredToWebSAMS($acadermicYear);
}


//echo convert2unicode($returnString, 1);
echo $returnString;
intranet_closedb();

?>