<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}


$ExportHeaderArr = array();
$ExportContentArr = array();
$lexport = new libexporttext();

# Header
$ExportHeaderArr[] = "eClass_TeacherID";
$ExportHeaderArr[] = "English Name";
$ExportHeaderArr[] = "Chinese Name";

# Content
$sql = "SELECT
				UserID, EnglishName, ChineseName
		FROM
				INTRANET_USER
		WHERE
				RecordType = 1
			AND
				Teaching = 1
		ORDER BY
				EnglishName
		";
$TeacherInfoArr = $ldiscipline->returnArray($sql);
$numOfTeacher = count($TeacherInfoArr);

for ($i=0; $i<$numOfTeacher; $i++)
{
	$thisUserID = $TeacherInfoArr[$i]['UserID'];
	$thisEnglishName = $TeacherInfoArr[$i]['EnglishName'];
	$thisChineseName = $TeacherInfoArr[$i]['ChineseName'];
	
	$j_counter = 0;
	$ExportContentArr[$i][$j_counter++] = $thisUserID;
	$ExportContentArr[$i][$j_counter++] = $thisEnglishName;
	$ExportContentArr[$i][$j_counter++] = $thisChineseName;
}

// Title of the grandmarksheet
$filename = "eclass_teacher_id.csv";

$export_content = $lexport->GET_EXPORT_TXT($ExportContentArr, $ExportHeaderArr, "", "\r\n", "", 0, "11");

// Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);
	

intranet_closedb();
?>
