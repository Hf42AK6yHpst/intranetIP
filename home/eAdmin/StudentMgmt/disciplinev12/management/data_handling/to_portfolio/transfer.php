<?php
// Using: 

####### Change log [Start] #######
#
#	Date:	2017-11-06	(Bill)	[2017-0403-1552-04240]
#			Create file to support Data Transfer (to iPortfolio)
#
####### Change log [End] #######

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

# Check access right
$ldiscipline = new libdisciplinev12();
if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || !$sys_custom['eDiscipline']['HKUGA_Sync_eDis']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$linterface = new interface_html();

# Current Page
$CurrentPage = "Management_DataHandling_ToPortfolio";
$CurrentPageArr['eDisciplinev12'] = 1;

$TAGS_OBJ[] = array($Lang['eDiscipline']['DataHandling_ToPortfolio'], "", 1);
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Step Information
$STEPS_OBJ[] = array($Lang['eDiscipline']['DataHandling_ToPortfolioStepArr'][0], 0);
$STEPS_OBJ[] = array($Lang['eDiscipline']['DataHandling_ToPortfolioStepArr'][1], 1);

# Init
$yearAry = array();
$semesterAry = array();

# Condition
if($rankTarget == "form"){
	$yearIdCond = " AND y.YearID IN ('".implode("','", (array)$rankTargetDetail)."') ";
}
else if($rankTarget == "class"){
	$classIdCond = " AND yc.YearClassID IN ('".implode("','", (array)$rankTargetDetail)."') ";
}
else if($rankTarget == "student"){
	$classIdCond = " AND yc.YearClassID IN ('".implode("','", (array)$rankTargetDetail)."') ";
	$studentIdCond = " AND ycu.UserID IN ('".implode("','", (array)$studentID)."')";
}

# Fields
$YearClassNameField = Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN");
$StudentNameField = getNameFieldByLang("iu.");

# Student Record [Start]
$StudentRecordCount = 0;
if($StudentRecord==1)
{
	# Target Student Record
	$sql = "SELECT
				r.*,
				$YearClassNameField as ClassName,
				ycu.ClassNumber,
				DATE_FORMAT(r.RecordDate,'%Y-%m-%d') as AccuDate,
				dci.Name as Reason
			FROM
				DISCIPLINE_ACCU_RECORD as r
				LEFT JOIN INTRANET_USER as iu ON (r.StudentID = iu.UserID)
				LEFT JOIN INTRANET_ARCHIVE_USER as au ON (r.StudentID = au.UserID)
				LEFT JOIN YEAR_CLASS_USER as ycu ON (r.StudentID = ycu.UserID OR au.UserID = ycu.UserID)
				LEFT JOIN YEAR_CLASS as yc ON (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '$selectYear') 
				LEFT JOIN YEAR as y ON (y.YearID = yc.YearID)
				LEFT JOIN DISCIPLINE_ACCU_CATEGORY_ITEM as dci ON (r.ItemID = dci.ItemID AND dci.RecordStatus = 1)
			WHERE
				r.RecordStatus = '1' AND 
				r.AcademicYearID = '".$selectYear."' ".(!in_array($selectSemester, array("", "0"))?" AND r.YearTermID = '".$selectSemester."' " : "")." AND 
				yc.AcademicYearID = '".$selectYear."' AND (r.RecordDate BETWEEN '".$textFromDate." 00:00:00' AND '".$textToDate." 23:59:59')  
				$yearIdCond $classIdCond $studentIdCond 
			GROUP BY
				r.RecordID
			ORDER BY
				y.Sequence, yc.Sequence, ycu.ClassNumber+0, r.RecordDate";
	$records = $ldiscipline->returnResultSet($sql);
	$StudentRecordCount = count($records);
	if($StudentRecordCount > 0)
	{
		# Remove Target Records in iPortfolio
		$recordIDAry = Get_Array_By_Key((array)$records, "RecordID");
		$deleteSql = "DELETE FROM $eclass_db.STUDENT_RECORD_STUDENT WHERE AccuRecordID IN ('".implode("', '", $recordIDAry)."')";
		$ldiscipline->db_db_query($deleteSql);
		
		# Remove Deleted Records in iPortfolio
		$sql = "SELECT
					r.RecordID
				FROM
					$eclass_db.STUDENT_RECORD_STUDENT as r
					LEFT JOIN INTRANET_USER as iu ON (r.UserID = iu.UserID)
					LEFT JOIN INTRANET_ARCHIVE_USER as au ON (r.UserID = au.UserID)
					LEFT JOIN YEAR_CLASS_USER as ycu ON (ycu.UserID = r.UserID OR au.UserID = ycu.UserID) 
					LEFT JOIN YEAR_CLASS as yc ON (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '$selectYear') 
					LEFT JOIN YEAR as y ON (y.YearID = yc.YearID)
				WHERE
					r.RecordStatus = '1' AND 
					r.AcademicYearID = '".$selectYear."' ".(!in_array($selectSemester, array("", "0"))?" AND r.YearTermID = '".$selectSemester."' " : "")." AND 
					yc.AcademicYearID = '".$selectYear."' AND (r.AccuDate BETWEEN '".$textFromDate." 00:00:00' AND '".$textToDate." 23:59:59')  
					$yearIdCond $classIdCond $studentIdCond 
				GROUP BY
					r.RecordID";
		$recordIDAry2 = $ldiscipline->returnVector($sql);
		$deleteSql2 = "DELETE FROM $eclass_db.STUDENT_RECORD_STUDENT WHERE RecordID IN ('".implode("', '", $recordIDAry2)."')";
		$ldiscipline->db_db_query($deleteSql2);
		
		# Tranfer Records to iPortfolio
		$fields = array("AccuRecordID", "UserID", "AcademicYearID", "Year", "YearTermID", "Semester", "ClassName", "ClassNumber", "AccuDate", "Reason", "PersonInCharge", "RecordType", "RecordStatus", "Remark", "ApplyHOY");
		$sql = "INSERT INTO $eclass_db.STUDENT_RECORD_STUDENT (";
		foreach($fields as $field)	$sql .= $field .", ";
		$sql .= "InputDate, ModifiedDate) VALUES ";
		
		// loop Records
		$values = array();
		foreach($records as $this_record)
		{
			$updateSql = "";
			foreach($fields as $field)
			{
				if($field == "AccuRecordID") {
					$field = "RecordID";
				}
				else if($field == "UserID") {
					$field = "StudentID";
				}
				else if($field == "Year") {
					if(empty($yearAry[$this_record["AcademicYearID"]])) {
						$yearAry[$this_record["AcademicYearID"]] = $ldiscipline->getAcademicYearNameByYearID($this_record["AcademicYearID"]);
					}
					$this_record[$field] = $yearAry[$this_record["AcademicYearID"]];
				}
				else if ($field == "Semester"){
					if(empty($semesterAry[$this_record["YearTermID"]])) {
						$RecordSemester = getAcademicYearAndYearTermByDate($this_record["RecordDate"]);
						$semesterAry[$this_record["YearTermID"]] = $RecordSemester[1];
					}
					$this_record[$field] = $semesterAry[$this_record["YearTermID"]];
				}
				else if($field=="PersonInCharge") {
					$field = "PICID";
				}
				$updateSql .= "'". intranet_htmlspecialchars($this_record[$field]) ."', ";
			}
			$updateSql .= "NOW(), NOW()";
			$values[] = $updateSql;
		}
		
		$delim = "";
		foreach($values as $thisValueSql) {
			$sql .= $delim." ($thisValueSql) ";
			$delim = ", ";
		}
		
		if($ldiscipline->db_db_query($sql)) {
			$resultStr = str_replace("<!--count-->", $StudentRecordCount, $Lang['eDiscipline']['DataHandling_ToPortfolioSuccess']['Student']); 
		}
	}
}
# Student Record [End]

# Pastoral Record [Start]
$PastoralRecordCount = 0;
if($PastoralRecord==1)
{
	# Target Pastoral Record
	$sql = "SELECT
				r.*,
				$YearClassNameField as ClassName,
				ycu.ClassNumber
			FROM
				DISCIPLINE_MERIT_RECORD as r
				LEFT JOIN INTRANET_USER as iu ON (r.StudentID = iu.UserID)
				LEFT JOIN INTRANET_ARCHIVE_USER as au ON (au.UserID = r.StudentID)
				LEFT JOIN YEAR_CLASS_USER as ycu ON (ycu.UserID = r.StudentID OR au.UserID = ycu.UserID)
				LEFT JOIN YEAR_CLASS as yc ON (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '$selectYear')
				LEFT JOIN YEAR as y ON (y.YearID = yc.YearID)
			WHERE
				r.RecordStatus = '1' AND 
				r.AcademicYearID = '$selectYear' ".(!in_array($selectSemester, array("", "0"))?" AND r.YearTermID = '$selectSemester' " : "")." AND 
				yc.AcademicYearID = '".$selectYear."' AND (r.RecordDate BETWEEN '$textFromDate 00:00:00' AND '$textToDate 23:59:59')  
				$yearIdCond $classIdCond $studentIdCond 
			GROUP BY
				r.RecordID
			ORDER BY
				y.Sequence, yc.Sequence, ycu.ClassNumber+0, r.RecordDate";
	$records = $ldiscipline->returnResultSet($sql);
	$PastoralRecordCount = count($records);
	if($PastoralRecordCount > 0)
	{
		# Remove Target Records in iPortfolio
		$recordIDAry = Get_Array_By_Key((array)$records, "RecordID");
		$deleteSql = "DELETE FROM $eclass_db.PASTORAL_RECORD_STUDENT WHERE MeritRecordID IN ('".implode("', '", $recordIDAry)."')";
		$ldiscipline->db_db_query($deleteSql);
		
		# Remove Deleted Records in iPortfolio
		$sql = "SELECT
					r.RecordID
				FROM
					$eclass_db.PASTORAL_RECORD_STUDENT as r
					LEFT JOIN INTRANET_USER as iu ON (r.UserID = iu.UserID)
					LEFT JOIN INTRANET_ARCHIVE_USER as au ON (r.UserID = au.UserID)
					LEFT JOIN YEAR_CLASS_USER as ycu ON (ycu.UserID = r.UserID OR au.UserID = ycu.UserID) 
					LEFT JOIN YEAR_CLASS as yc ON (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '$selectYear') 
					LEFT JOIN YEAR as y ON (y.YearID = yc.YearID)
				WHERE
					r.RecordStatus = '1' AND 
					r.AcademicYearID = '$selectYear' ".(!in_array($selectSemester, array("", "0"))?" AND r.YearTermID = '$selectSemester' " : "")." AND 
					yc.AcademicYearID = '".$selectYear."' AND (r.MeritDate BETWEEN '$textFromDate 00:00:00' AND '$textToDate 23:59:59')  
					$yearIdCond $classIdCond $studentIdCond 
				GROUP BY
					r.RecordID";
		$recordIDAry2 = $ldiscipline->returnVector($sql);
		$deleteSql2 = "DELETE FROM $eclass_db.PASTORAL_RECORD_STUDENT WHERE RecordID IN ('".implode("', '", $recordIDAry2)."')";
		$ldiscipline->db_db_query($deleteSql2);
		
		# Tranfer Records to iPortfolio
		$fields = array("MeritRecordID", "UserID", "AcademicYearID", "Year", "YearTermID", "Semester", "ClassName", "ClassNumber", "MeritDate", "Reason", "PersonInCharge", "RecordType", "RecordStatus", "Remark");
		$sql = "INSERT INTO $eclass_db.PASTORAL_RECORD_STUDENT (";
		foreach($fields as $field)	$sql .= $field .", ";
		$sql .= "InputDate, ModifiedDate) VALUES ";
		
		// loop Records
		$values = array();
		foreach($records as $this_record)
		{
			$updateSql = "";
			foreach($fields as $field)
			{
				if($field=="MeritRecordID") {
					$field = "RecordID";
				}
				else if($field=="UserID") {
					$field = "StudentID";
				}
				else if($field == "Year") {
					if(empty($yearAry[$this_record["AcademicYearID"]])) {
						$yearAry[$this_record["AcademicYearID"]] = $ldiscipline->getAcademicYearNameByYearID($this_record["AcademicYearID"]);
					}
					$this_record[$field] = $yearAry[$this_record["AcademicYearID"]];
				}
				else if ($field == "Semester"){
					if(empty($semesterAry[$this_record["YearTermID"]])) {
						$RecordSemester = getAcademicYearAndYearTermByDate($this_record["RecordDate"]);
						$semesterAry[$this_record["YearTermID"]] = $RecordSemester[1];
					}
					$this_record[$field] = $semesterAry[$this_record["YearTermID"]];
				}
				else if($field=="MeritDate") {
					$field = "RecordDate";
				}
				else if($field=="Reason") {
					$field = "ItemText";
				}
				else if($field=="PersonInCharge") {
					$field = "PICID";
				}
				else if($field=="RecordType") {
					$field = "MeritType";
				}
				$updateSql .= "'". intranet_htmlspecialchars($this_record[$field]) ."', ";
			}
			$updateSql .= "NOW(), NOW()";
			$values[] = $updateSql;
		}
		
		$delim = "";
		foreach($values as $thisValueSql) {
			$sql .= $delim." ($thisValueSql) ";
			$delim = ", ";
		}
		
		if($ldiscipline->db_db_query($sql)) {
			if(!empty($resultStr)) $resultStr.= " & ";
			$resultStr .= str_replace("<!--count-->", $PastoralRecordCount, $Lang['eDiscipline']['DataHandling_ToPortfolioSuccess']['Pastoral']); 
		}
	}
}
# Pastoral Record [End]

# Import Result Summary
if(!empty($resultStr)) {
	$resultStr = $Lang['eDiscipline']['DataHandling_ToPortfolioSuccess']['Success'].$resultStr;
}

$linterface->LAYOUT_START();
?>

<script language="javascript">
function js_Go_Transfer_Other_Records()
{
	window.location = 'index.php';
}
</script>
		
<br />
<form id="form1" name="form1" method="POST">
	<div class="table_board">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr><td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td align=center><?=$resultStr?></td></tr>
		</table>
	</div>
	
	<br style="clear:both;" />
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$linterface->GET_ACTION_BTN($Lang['eEnrolment']['Transfer_to_SP']['Button']['TransferOtherRecords'], "button", "js_Go_Transfer_Other_Records();", "submitBtn")?>
		<p class="spacer"></p>
	</div> 
</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>