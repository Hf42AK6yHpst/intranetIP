<?php
// Editing by 
/*
 * 2014-06-11 (Carlos): Created for customization $sys_custom['eDiscipline']['PooiToMiddleSchool']
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_cust.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12_cust();

if(
	!($ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || 
	$ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-View") || 
	$ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-View") || 
	$ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-View") || !$sys_custom['eDiscipline']['PooiToMiddleSchool'])
) 
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$linterface = new interface_html();
$ldiscipline_ui = new libdisciplinev12_ui_cust();

$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID(); 

$template_types = array();
$template_types[] = array('','--'.$Lang['General']['PleaseSelect'].'--');
$template_types[] = array('1',$Lang['eDiscipline']['EmailNotifictionCust']['RecordTypeArr'][1]);
$template_types[] = array('2',$Lang['eDiscipline']['EmailNotifictionCust']['RecordTypeArr'][2]);
$template_types[] = array('3',$Lang['eDiscipline']['EmailNotifictionCust']['RecordTypeArr'][3]);
$template_types[] = array('4',$Lang['eDiscipline']['EmailNotifictionCust']['RecordTypeArr'][4]);
$template_types[] = array('5',$Lang['eDiscipline']['EmailNotifictionCust']['RecordTypeArr'][5]);
$template_type_selection = $linterface->GET_SELECTION_BOX($template_types, 'name="TemplateType" id="TemplateType"  ', '', '');


$send_status_selection = $linterface->Get_Radio_Button("SendStatusY", "SendStatus", '1', 0, "", $Lang['eDiscipline']['EmailNotifictionCust']['Sent'], "", 0).'&nbsp;'.$linterface->Get_Radio_Button("SendStatusN", "SendStatus", '0', 1, "", $Lang['eDiscipline']['EmailNotifictionCust']['NotSent'], "", 0);


$CurrentPageArr['eDisciplinev12'] = 1;
$CurrentPage = "Management_EmailNotification";

### Title ###
$TAGS_OBJ[] = array($Lang['eDiscipline']['EmailNotification']);
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

?>
<script type="text/javascript" language="JavaScript">
var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image();?>';

function showSpan(span) {
	document.getElementById(span).style.display="inline";
	if(span == 'spanStudent') {
		$('#selectAllBtn01').hide();
		var temp = document.getElementById('studentID[]');
		
		while(temp.options.length>0) {
			temp.options[0] = null;	
		}
	} 
}

function hideSpan(span) {
	document.getElementById(span).style.display="none";
	if(span=='spanStudent') {
		$('#selectAllBtn01').show();
	}
}

function showResult(str, choice)
{
	if (str.length==0)
	{ 
		document.getElementById("rankTargetDetail").innerHTML = "";
		document.getElementById("rankTargetDetail").style.border = "0px";
		return
	}
	
	var yearClassId = '';
	if(str == 'student2ndLayer'){
		var options = $('select#rankTargetDetail\\[\\] option:selected');
		var delim = '';
		for(var i=0;i<options.length;i++){
			yearClassId += delim + options.get(i).value;
			delim = ',';
		}
	}
	
	var url = "../../reports/ajaxGetLive.php";
	url = url + "?target=" + str + "&rankTargetDetail=" + choice;
	if(str == 'form'){
		url += "&fieldId=rankTargetDetail[]&fieldName=rankTargetDetail[]";
	}else if(str == 'class'){
		url += "&fieldId=rankTargetDetail[]&fieldName=rankTargetDetail[]";
	}else if(str == 'student'){
		url += "&fieldId=rankTargetDetail[]&fieldName=rankTargetDetail[]&onchange="+encodeURIComponent("showRankTargetResult('student2ndLayer','');");
	}else if(str == 'student2ndLayer'){
		url += "&fieldId=rankTargetDetail[]&fieldName=rankTargetDetail[]&studentFieldId=studentID[]&studentFieldName=studentID[]&YearClassID="+yearClassId;
	}
	url += "&academicYearId=<?=$AcademicYearID?>";
	
	$.get(
		url,
		{},
		function(responseText){
			var showIn = "rankTargetDetail";
			if(str == 'student2ndLayer'){
				showIn = "spanStudent";	
			}
			$('#'+showIn).html(responseText);
			document.getElementById(showIn).style.border = "0px solid #A5ACB2";
			
			if(str == 'student'){
				showRankTargetResult('student2ndLayer','');
			}
		}
	);
}

function showRankTargetResult(val, choice) {
	showResult(val,choice);
	if (val=='student2ndLayer') {
		showSpan('spanStudent');
	} else {
		hideSpan('spanStudent');
	}
}

function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function SelectAllItem(obj, flag) {
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = flag;
	}
}

function SendEmail()
{
	var recordIdObjs = $('input[name="RecordID[]"]:checked');
	var recordIdAry = [];
	for(var i=0;i<recordIdObjs.length;i++){
		recordIdAry.push(recordIdObjs.get(i).value);
	}
	
	if(recordIdAry.length == 0){
		alert('<?=$Lang['General']['JS_warning']['SelectAtLeastOneRecord']?>');
		return;
	}
	
	if(confirm('<?=$Lang['eDiscipline']['EmailNotifictionCust']['ConfirmSendEmail']?>'))
	{
		Block_Document();
		$.post(
			'ajax_task.php',
			{
				'task': 'send_email',
				'TemplateType': $('input#hiddenTemplateType').val(),
				'SendStatus': $('input#hiddenSendStatus').val(),
				'RecordID[]': recordIdAry
			},
			function(data){
				if(data == '1'){
					Get_Return_Message('<?=$Lang['eDiscipline']['EmailNotifictionCust']['EmailsSentSuccess']?>');
					submitForm();
				}else{
					Get_Return_Message('<?=$Lang['eDiscipline']['EmailNotifictionCust']['EmailsSentUnsuccess']?>');
					UnBlock_Document();
				}
			}
		);
	}
}

function PreviewEmail()
{
	var formObj = document.getElementById('MainForm');
	var recordIdObjs = $('input[name="RecordID[]"]:checked');
	var recordIdAry = [];
	for(var i=0;i<recordIdObjs.length;i++){
		recordIdAry.push(recordIdObjs.get(i).value);
	}
	
	if(recordIdAry.length == 0){
		alert('<?=$Lang['General']['JS_warning']['SelectAtLeastOneRecord']?>');
		return;
	}
	
	formObj.target = "_blank";
	formObj.action = "preview.php";
	formObj.submit();
	formObj.target = "";
	formObj.action = "";
	
}

function submitForm()
{
	var valid = true;
	var form_obj = $('#form1');
	var rank_target = $('#rankTarget').val();
	var template_type = $('#TemplateType').val();
	
	if(rank_target == 'form' || rank_target == 'class'){
		if($('select#rankTargetDetail\\[\\] option:selected').length == 0){
			valid = false;
			$('#TargetWarnDiv span').html('<?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?>');
			$('#TargetWarnDiv').show();
		}else{
			$('#TargetWarnDiv').hide();
		}
	}else if(rank_target == 'student'){
		if($('select#studentID\\[\\] option:selected').length == 0){
			valid = false;
			$('#TargetWarnDiv span').html('<?=$i_alert_pleaseselect.$Lang['Btn']['Student']?>');
			$('#TargetWarnDiv').show();
		}else{
			$('#TargetWarnDiv').hide();
		}
	}
	
	if(template_type == ''){
		valid = false;
		$('#TemplateTypeWarnDiv span').html('<?=$Lang['eDiscipline']['EmailNotifictionCust']['RecordTypeWarning']?>');
		$('#TemplateTypeWarnDiv').show();
	}else{
		$('#TemplateTypeWarnDiv').hide();
	}
	
	if(!valid) return;
	
	Block_Document();
	$.post(
		'ajax_task.php?task=get_record_list',
		$('#form1').serialize(),
		function(data){
			$('#TableDiv').html(data);
			UnBlock_Document();
		}
	);
}

$(document).ready(function(){
	showRankTargetResult($("#rankTarget").val(), '');
});
</script>
<br />
<form id="form1" name="form1" action="" method="post" onsubmit="return false;">
<table class="form_table_v30" border="0" cellspacing="0" cellpadding="5" align="center">
	<!-- Target -->
	<tr valign="top">
		<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["eEnrolment"]["ActivityParticipationReport"]["Target"]?></td>
		<td>
			<table class="inside_form_table">
				<tr>
					<td valign="top">
						<select name="rankTarget" id="rankTarget" onChange="showRankTargetResult(this.value,'')">
							<option value="form" <? if($rankTarget=="form" || !isset($rankTarget) || $rankTarget=="") { echo "selected";} ?>><?=$i_Discipline_Form?></option>
							<option value="class" <? if($rankTarget=="class") { echo "selected";} ?>><?=$i_Discipline_Class?></option>
							<option value="student" <? if($rankTarget=="student") { echo "selected";} ?>><?=$i_UserStudentName?></option>
						</select>
					</td>
					<td valign="top" nowrap>
						<!-- Form / Class //-->
						<span id='rankTargetDetail'>
						<select name="rankTargetDetail[]" id="rankTargetDetail[]" size="5"></select>
						</span>
						<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('rankTargetDetail[]'));return false;", "selectAllBtn01")?>
							
						<!-- Student //-->
						<span id='spanStudent' style='<? if($rankTarget=="student") { echo "display:inline;";} else { echo "display:none;";} ?>'>
							<select name="studentID[]" multiple size="5" id="studentID[]"></select> <?= $linterface->GET_BTN($button_select_all, "button", "SelectAllItem(document.getElementById('studentID[]'), true);return false;")?>
						</span>
						
					</td>
				</tr>
				<tr><td colspan="3" class="tabletextremark">(<?=$Lang["eEnrolment"]["ActivityParticipationReport"]["PressCtrlKey"]?>)</td></tr>
			</table>
			<span id='div_Target_err_msg'></span>
			<?=$linterface->Get_Form_Warning_Msg("TargetWarnDiv","")?>
		</td>
	</tr>
	
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eDiscipline']['EmailNotifictionCust']['RecordType']?></td>
		<td width="70%"class="tabletext">
			<?=$template_type_selection?>
			<?=$linterface->Get_Form_Warning_Msg("TemplateTypeWarnDiv","")?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eDiscipline']['EmailNotifictionCust']['SendStatus']?></td>
		<td width="70%"class="tabletext">
			<?=$send_status_selection?>
		</td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td align="left" class="tabletextremark"><?=$i_general_required_field?></td></tr>
</table>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_submit, "button", "submitForm();") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" onclick=\"window.location.href='index.php';\" ") ?>
		</td>
	</tr>
</table>
</form>
<br />
<form name="MainForm" id="MainForm" action="" method="post">
<div id="TableDiv"></div>
</form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>