<?php
// Editing by 
/*
 * 2015-06-01 (Carlos): Modified to cater more than one class teachers
 * 2014-06-11 (Carlos): Created for customization $sys_custom['eDiscipline']['PooiToMiddleSchool']
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_cust.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12_cust();

if(
	!($ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || 
	$ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-View") || 
	$ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-View") || 
	$ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-View") || !$sys_custom['eDiscipline']['PooiToMiddleSchool'])
) 
{
	//$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit;
}

$ldiscipline_ui = new libdisciplinev12_ui_cust();
$fcm = new form_class_manage();

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");

$TemplateType = $_REQUEST['hiddenTemplateType'];
$SendStatus = $_REQUEST['hiddenSendStatus'];

$options = array('TemplateType'=>$TemplateType,'SendStatus'=>$SendStatus);
$RecordID = $_REQUEST['RecordID'];
$RecordID_count = count($RecordID);
$recordIdAry = array();
for($i=0;$i<$RecordID_count;$i++){
	$ary = explode(",",$RecordID[$i]);
	$recordIdAry = array_merge($recordIdAry,$ary);
}
$options['RecordID'] = $recordIdAry;

$records = $ldiscipline->getMisconductAndPunishmentNotificationRecords($options);
$record_count = count($records);

$x = '';
$x .= '<table width="96%" align="center" class="print_hide" border="0">
			<tbody>
				<tr>
					<td align="right">'.$ldiscipline_ui->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","printBtn").'</td>
				</tr>
			</tbody>
		</table>'."\n";
for($i=0;$i<$record_count;$i++){
	$year_class_id = $fcm->Get_Student_Studying_Class($records[$i]['UserID']);
	$year_class = new year_class($year_class_id);
	$year_class->Get_Class_Teacher_List(false);
	$class_teacher_list = $year_class->ClassTeacherList;
	$class_teacher_name_ary = Get_Array_By_Key($class_teacher_list, 'TeacherName');
	$class_teacher_id_ary = Get_Array_By_Key($class_teacher_list, 'UserID');
	//$class_teacher_name = $class_teacher_list[0]['TeacherName'];
	//$class_teacher_id = $class_teacher_list[0]['UserID'];
	$class_teacher_name = implode(", ", $class_teacher_name_ary);
	$records[$i]['ClassTeacherName'] = $class_teacher_name;
	
	$x .= $ldiscipline_ui->getEmailNotificationDiv($TemplateType, $records[$i],$i!=($record_count-1));
}

echo $x;

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");

intranet_closedb();
?>