<?
# Using: YW

########## Change Log ###############
#
#	Date	:	2020-10-08 (YatWoon) [Case#2020-1007-1112-42170]
#				change PIC selection (use common_choose) (replace "U")
#
#   Date    :   2019-05-14  Bill    [2019-0509-1450-25066]
#               Cast $GMCount to float
#
#   Date    :   2019-05-01 (Bill)
#               prevent SQL Injection + Cross-site Scripting
#
#	Date	:	2017-10-31 (Bill)	[2017-0403-1552-54240]
#				Update "Details refer to HOY" ($sys_custom['eDiscipline']['HOY_Access_GM'])
#
#	Date	:	2016-09-29 (Bill)	[2016-0808-1526-52240]
#				Update ConductScoreChange for misconduct ($sys_custom['eDiscipline']['GMConductMark'])
#
#	Date	:	2014-05-28 (Carlos) - $sys_custom['eDiscipline']['PooiToMiddleSchool'] update ScoreChange for misconduct record
#
#	Date	:	2010-06-10 (Henry)
#	Detail 	:	add attachment to record
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$RecordID = IntegerSafe($RecordID);

$SchoolYear = IntegerSafe($SchoolYear);
if($semester != '' && $semester != 'WholeYear') {
    $semester = IntegerSafe($semester);
}
$targetClass = cleanCrossSiteScriptingCode($targetClass);

$RecordType = IntegerSafe($RecordType);
$approved = IntegerSafe($approved);
$rejected = IntegerSafe($rejected);
$waived = IntegerSafe($waived);
$fromPPC = IntegerSafe($fromPPC);
$newleaf = IntegerSafe($newleaf);
$pic = IntegerSafe($pic);

foreach($PIC as $this_key_2 => $thisPICID) {
			$thisPICID = str_replace("U", "", $thisPICID);
            $PIC[$this_key_2] = IntegerSafe($thisPICID);
        }

$StudentID = IntegerSafe($StudentID);
$meritItemID = IntegerSafe($meritItemID);
$PIC = IntegerSafe($PIC);

$ActionID = IntegerSafe($ActionID);
if(isset($GMCount)) {
    $GMCount = (float)($GMCount);
}
$resetFollowup = IntegerSafe($resetFollowup);

$FolderLocation = cleanCrossSiteScriptingCode($FolderLocation);
### Handle SQL Injection + XSS [END]

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

$isExist = array();
$fail = 0;

# Not allow same student with same item in a same day
if($ldiscipline->NotAllowSameItemInSameDay)
{
	$dataInfo = $ldiscipline->RETRIEVE_CONDUCT_INFO($RecordID);
	$thisCatID = $dataInfo[0]['CategoryID'];					# Original CategoryID
	//$thisItemID = $meritItemID;								# New ItemID
	$thisRecordDate = substr($dataInfo[0]['RecordDate'],0,10);	# Original Record Date
	$StudentID = $dataInfo[0]['StudentID'];						# Original StudentID
	$isExist[$StudentID] = $ldiscipline->checkSameGMItemInSameDay($StudentID, $thisCatID, $meritItemID, $thisRecordDate, $RecordID);
	if($isExist[$StudentID]==1) $fail = 1;
}

$AttachmentLoc = substr($FolderLocation,0,-1);	

//debug_pr($_POST); exit;
if($fail == 0)
{
	# Check access right
	if(!($ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-EditAll") || (($ldiscipline->isConductPIC($RecordID) || $ldiscipline->isConductOwn($RecordID)) && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-EditOwn")))) {
			$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	}
	if(!isset($Attachment) || sizeof($Attachment)==0) $AttachmentLoc = "";
	if(!isset($GMCount) || $GMCount=="") $GMCount = 1;
	
	if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
	    $ScoreChange = IntegerSafe($ScoreChange);
		$OtherDataAry = array('ScoreChange'=>$ScoreChange);
		
		$result = $ldiscipline->updateGoodConductMisconductRecord($RecordID, $meritItemID, $remark, $PIC, $AttachmentLoc, $ActionID, $resetFollowup, $GMCount, $OtherDataAry);
	}
	// [2016-0808-1526-52240] added Conduct Score
	else if($sys_custom['eDiscipline']['GMConductMark']){
	    $GMConductScoreChange = IntegerSafe($GMConductScoreChange);
		$OtherDataAry = array('GMConductScoreChange' => ($GMConductScoreChange * -1));
		
		$result = $ldiscipline->updateGoodConductMisconductRecord($RecordID, $meritItemID, $remark, $PIC, $AttachmentLoc, $ActionID, $resetFollowup, $GMCount, $OtherDataAry);
	}
	// [2017-0403-1552-54240] "Details refer to HOY" Handling
	else if ($sys_custom['eDiscipline']['HOY_Access_GM']){
	    $ApplyHOY = IntegerSafe($ApplyHOY);
	    $SkipRemarkUpdate = IntegerSafe($SkipRemarkUpdate);
		$OtherDataAry = array('ApplyHOY' => $ApplyHOY, 'SkipRemarkUpdate' => $SkipRemarkUpdate);
		
		$result = $ldiscipline->updateGoodConductMisconductRecord($RecordID, $meritItemID, $remark, $PIC, $AttachmentLoc, $ActionID, $resetFollowup, $GMCount, $OtherDataAry);
	}
	else{
		$result = $ldiscipline->updateGoodConductMisconductRecord($RecordID, $meritItemID, $remark, $PIC, $AttachmentLoc, $ActionID, $resetFollowup, $GMCount);
	}
	intranet_closedb();
}

$par = "SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&RecordType=$RecordType&approved=$approved&rejected=$rejected&waived=$waived&fromPPC=$fromPPC&newleaf=$newleaf&pic=$pic";

if($fail==1) { ?>
	<form name="form1" method="POST" action="edit.php">
		<input type="hidden" name="RecordID[]" id="RecordID[]" value="<?=$RecordID?>">
		<input type="hidden" name="StudentID[]" id="StudentID[]" value="<?=$StudentID?>">
		<input type="hidden" name="duplicate" id="duplicate" value="1">
	</form>
	<script language="javascript">
		document.form1.submit();
	</script>
<?	
}
else if($result){
	header("Location: index.php?msg=update&$par");
	exit();
}
else{
	header("Location: index.php?msg=update_failed&$par");
	exit();
}
?>