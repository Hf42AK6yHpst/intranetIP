<?
// Modifying by: 

###################################
#
#	Date	:	2017-10-30	Bill	[2016-0726-1631-34073]
#				fixed: perform record date checking when Setting : Allow to input discipline records within the last ... day(s) is empty
#
#	Date	:	2015-02-13 (Bill) [2015-0123-1536-08071]
#				fixed: correctly import main subjects which have component subjects if using Item Code start with "SUBJ"
#
#	Date	:	2014-06-26 (Carlos)
#				$sys_custom['eDiscipline']['PooiToMiddleSchool'] - no need to check period settings for misconduct
#				2014-05-30 (Carlos)
#				$sys_custom['eDiscipline']['PooiToMiddleSchool'] - added [Score] data column
#
#	Date	:	2013-09-26 (YatWoon)
#	Detail	:	add checking for $ldiscipline->OnlyAdminSelectPIC [Case#2013-0903-0925-36071]
#
#	Date	:	2013-02-06	YatWoon
#				update wording "Receive" to "Times"
#
###################################

$PATH_WRT_ROOT="../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();

$limport = new libimporttext();
$lo = new libfilesystem();

$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));
if($ext != ".CSV" && $ext != ".TXT")
{
	header("location: import.php?xmsg=import_failed");
	exit();
}

$data = $limport->GET_IMPORT_TXT($csvfile);
if(is_array($data))
{
	$col_name = array_shift($data);
}
/*
if($sys_custom['eDiscipline_GM_Times']) {
	$file_format = array('Class Name','Class Number','Event Date','Item Code','Times','PIC','Remark');
} else {
	$file_format = array('Class Name','Class Number','Event Date','Item Code','PIC','Remark');
}
*/

$file_format = array('Class Name','Class Number','Event Date','Item Code');
if($sys_custom['eDiscipline_GM_Times']) {
	$file_format = array_merge($file_format,array('Times'));
}
if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
	$file_format = array_merge($file_format,array('Score'));
}
$file_format = array_merge($file_format,array('PIC','Remark'));

$format_wrong = false;
for($i=0; $i<sizeof($file_format); $i++)
{
	if ($col_name[$i]!=$file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}
if($format_wrong)
{
	header("location: import.php?xmsg=wrong_header");
	exit();
}

$ldiscipline = new libdisciplinev12();
$lu = new libuser();

$linterface = new interface_html();

# Menu Highlight
$CurrentPage = "Management_GoodConductMisconduct";
$CurrentPageArr['eDisciplinev12'] = 1;

# Left Menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Tag Information
$TAGS_OBJ[] = array($eDiscipline['Good_Conduct_and_Misconduct']);

# Step Information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$linterface->LAYOUT_START();

### List out the import result
$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
$x .= "<tr>";
$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_general_class</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_identity_student</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eDiscipline']['EventDate']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_SettingsSemester</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$iDiscipline['RecordType']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_System_general_record</td>";
if($sys_custom['eDiscipline_GM_Times'])
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eDiscipline']['ReceiveTimes']."</td>";
if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eDiscipline']['Score']."</td>";
}
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_PIC</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_System_general_remark</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$iDiscipline['AccumulativePunishment_Import_Failed_Reason']."</td>";
$x .= "</tr>";

$teacherList = $lu->returnUsersType(1);

$sql = "create table IF NOT EXISTS temp_goodconduct_import
		(
			 ClassName varchar(10),
			 ClassNumber int(3),
			 CategoryID int(11),
			 Category varchar(255),
			 ConductCat varchar(255),
			 ConductItem varchar(255),
			 ItemID int(11),
			 RecordDate date,
			 StudentID int(11),
			 Remark text,
			 PICID varchar(255),
			 RecordType int(11),
			 RecordStatus int(11),
			 CountNo int(11),
			 Year varchar(50),
			 Semester varchar(50),
			 Teacher text,
			 StudentName varchar(255),
			 ItemName varchar(255)
	    ) ENGINE=InnoDB DEFAULT CHARSET=utf8";	    
$ldiscipline->db_db_query($sql);

$fields = mysql_list_fields($intranet_db, 'temp_goodconduct_import');
$columns = mysql_num_fields($fields);
for ($i = 0; $i < $columns; $i++) { $field_array[] = mysql_field_name($fields, $i); }
if (!in_array('UserID', $field_array)) {
	$result = mysql_query('ALTER TABLE temp_goodconduct_import ADD UserID int(11)');
}
if (!in_array('GMCount', $field_array)) {
	$result = mysql_query('ALTER TABLE temp_goodconduct_import ADD GMCount float(8,2) default 1');
}
if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
	$sql_add_score = "ALTER TABLE temp_goodconduct_import ADD COLUMN ScoreChange int(8) DEFAULT 0";
	$ldiscipline->db_db_query($sql_add_score);
}

# Delete the temp data in temp table 
$sql = "delete from temp_goodconduct_import where UserID=".$_SESSION["UserID"];
$ldiscipline->db_db_query($sql) or die(mysql_error());

$error_occured = 0;
for($i=0; $i<sizeof($data); $i++)
{
	$error = array();
	$PicIDArr = array();
	$PicName = array();
	$RecordType = ''; 
	$ItemCategoryID = '';
	$ItemID = '';
	$ItemName = '';
	
	### Get Data
	/*
	if($sys_custom['eDiscipline_GM_Times']) {
		list($ClassName,$ClassNumber,$RecordDate,$CondItem,$GMCount,$PIC,$Remark) = $data[$i];
	} else {
		list($ClassName,$ClassNumber,$RecordDate,$CondItem,$PIC,$Remark) = $data[$i];	
		$GMCount = 1;
	}
	*/
	$col = 0;
	$ClassName = $data[$i][$col];
	$col++;
	$ClassNumber = $data[$i][$col];
	$col++;
	$RecordDate = $data[$i][$col];
	$col++;
	$CondItem = $data[$i][$col];
	$col++;
	if($sys_custom['eDiscipline_GM_Times']){
		$GMCount = $data[$i][$col];
		$col++;
	}
	else{
		$GMCount = 1;
	}
	if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
		$Score = $data[$i][$col];
		$col++;
	}
	else{
		$Score = 0;
	}
	$PIC = $data[$i][$col];
	$col++;
	$Remark = $data[$i][$col];
	$col++;
	$CondItem = intranet_htmlspecialchars($CondItem);
	
	# Record Date
	if(trim($RecordDate)=='')
	{
		$error['RecordDate']=$iDiscipline['Award_Punishment_Missing_RecordDate'];	
	}	
	else
	{
			$oriRecordDate = $RecordDate;
			$RecordDate = getDefaultDateFormat($RecordDate);
			
			$date_format = false;
			$date_format = $ldiscipline->checkDateFormat($RecordDate);
			if(!$date_format)
			{
				$error['RecordDate'] = $iDiscipline['Invalid_Date_Format'];			
			}
			else
			{
				$NumOfMaxRecordDays = $ldiscipline->MaxRecordDayBeforeAllow;
				if($NumOfMaxRecordDays > 0) {
					$EnableMaxRecordDaysChecking = 1;
					$DateMinLimit = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d")-$NumOfMaxRecordDays, date("Y")));
					$DateMaxLimit = date("Y-m-d");
				}
				else{
					$EnableMaxRecordDaysChecking = 0;
				}
	
				if($EnableMaxRecordDaysChecking == 1)
				{
					if($DateMinLimit > $RecordDate) {
						$error['RecordDate'] = $eDiscipline['Import']['Error']['RecordDateIsNotAllow'];
					}
					else {
						if($RecordDate > $DateMaxLimit) {
							$error['RecordDate'] = $eDiscipline['Import']['Error']['RecordDateIsNotAllow'];
						}
					}
				}
				else
				{
					$TodayDate = date("Y-m-d");
					if($RecordDate > $TodayDate) {
						$error['RecordDate'] = $eDiscipline['Import']['Error']['RecordDateIsNotAllow'];
					}
				}
			}
	}
	
	$Remark = intranet_htmlspecialchars(addslashes($Remark));
	
	if(trim($CondItem)=='')
	{
		$error['CondItem']=$iDiscipline['Good_Conduct_Missing_Conduct_Item'];	
	}
	else
	{
		# Compare with good Category Item
		$good_items = $ldiscipline->RETRIEVE_CONDUCT_ITEM_GROUPBY_CAT(1);
		for($a=0;$a<sizeof($good_items);$a++)
		{
			//if($good_items[$a]['ItemCode'] == ($CondItem))
			if($good_items[$a][3] == ($CondItem))
			{
				$ItemCategoryID = $good_items[$a][0];
				$ItemID = $good_items[$a][1];
				//$ItemName = $good_items[$a]['Name'];
				$ItemName = $good_items[$a][2];
				$RecordType=1;
			}
		}
		
		# Compare with bad Category Item
		$bad_items = $ldiscipline->RETRIEVE_CONDUCT_ITEM_GROUPBY_CAT(-1);
		for($a=0;$a<sizeof($bad_items);$a++)
		{			
			//if($bad_items[$a]['ItemCode'] == ($CondItem))
			if($bad_items[$a][3] == ($CondItem))
			{
				$ItemCategoryID = $bad_items[$a][0];
				$ItemID = $bad_items[$a][1];
				//$ItemName = $bad_items[$a]['Name'];
				$ItemName = $bad_items[$a][2];
				$RecordType=-1;
			}
		}
		
		if($ItemCategoryID=='' || $ItemID=='' || $RecordType=='')
		{
			# Compare with homework subject
			$use_intranet_homework = $ldiscipline->accumulativeUseIntranetSubjectList();
			if($use_intranet_homework)
			{
				//$sql = "SELECT SubjectID, CONCAT('SUBJ',SubjectID) as ItemCode, SubjectName FROM INTRANET_SUBJECT WHERE RecordStatus = 1 ";
				$sql = "SELECT RecordID as SubjectID, CONCAT('SUBJ',CodeID) as ItemCode, ".Get_Lang_Selection("CH_DES", "EN_DES")." as SubjectName FROM ASSESSMENT_SUBJECT";
				
				// only allow main and active subject [2015-0123-1536-08071]
				$sql.= " WHERE RecordStatus = 1 and (CMP_CODEID is NULL or CMP_CODEID = '') ";
				$homework_list = $ldiscipline->returnArray($sql);	
				for($a=0;$a<sizeof($homework_list);$a++)
				{
					if($homework_list[$a]['ItemCode'] == $CondItem)
					{
						$ItemCategoryID = "2";
						$ItemID = $homework_list[$a]['SubjectID'];
						$ItemName = $homework_list[$a]['SubjectName'];
						$RecordType=-1;
					}
				}
			}
		}
		
		if($RecordType!=1 && $RecordType!=-1) {
			$error['CondItem']=$iDiscipline['Good_Conduct_Missing_Conduct_Item'];	
		}
	}
	
	//if(trim($CondCategory)!='' && trim($CondItem)!='')
	if(trim($CondItem)!='' && !$error['RecordDate'])
	{
		if($ItemID == '' || $ItemID=='' || $RecordType=='' )
		{
			$error['ConductItem'] = $iDiscipline['Good_Conduct_No_Conduct_Item'];
		}
		
		if($ItemCategoryID!='')
		{
			### Check the caetgory is in use or not
			$sql = "select RecordStatus from DISCIPLINE_ACCU_CATEGORY where CategoryID=$ItemCategoryID";
			$recordstatus = $ldiscipline->returnVector($sql);
			if($recordstatus[0]==DISCIPLINE_CONDUCT_CATEGORY_DRAFT)
			{
				$error['ConductCategory']=$Lang['eDiscipline']['InvalidConductCategory'];
			}
			else
			{
				### Check the record date in one of period
				$period_info = $ldiscipline->RETRIEVE_TARGET_CATEGORY_PERIOD_SETTING($RecordDate,$RecordType,$ItemCategoryID);
				if(sizeof($period_info) || ($sys_custom['eDiscipline']['PooiToMiddleSchool'] && $RecordType==-1)) # have period setting or PooiTo cust can skip period settings for misconduct
				{
					
				}
				else 
				{
					$error['period']=$iDiscipline['Good_Conduct_No_Period_Setting'];
				}
			}
		}
	}
	
	### Checking - Valid class & class number (student found)
	//$StudentID = $lu->returnUserID($ClassName, $ClassNumber, 2, '0,1,2');
	$classID = $ldiscipline->getClassIDByClassName($ClassName, Get_Current_Academic_Year_ID());
	$StudentID = $lu->returnUserIDByClassIDClassNumber($classID, $ClassNumber);
	if(trim($StudentID)=="")
	{
		$error['No_Student'] = $iDiscipline['Good_Conduct_No_Student'];
	}
	
	$lu = new libuser($StudentID);
	$ClassInfo = $ClassName."-".$ClassNumber;
	$StudentName = ($intranet_session_language=='en')?$lu->EnglishName:$lu->ChineseName;
	
	# School Year
	//$Year = GET_ACADEMIC_YEAR_WITH_FORMAT($RecordDate);
	$year_info = getAcademicYearInfoAndTermInfoByDate($RecordDate);
	$Year = $year_info[1];
	$Semester =  $year_info[3];
	if(trim($Semester)=='')
	{
		$error['No_Semester'] = $eDiscipline['SemesterSettingError'];		
	}	
	
	if($GMCount=="") {
		$error['GMCount'] = $Lang['eDiscipline']['Import_GM_Error'][1];	
	}
	else {
		$defaultGmCountAry = $ldiscipline->Default_GM_Count();
		if(!in_array($GMCount, $defaultGmCountAry)) {
			$error['GMCount'] = $Lang['eDiscipline']['Import_GM_Error'][2];	
		}
	}
	
	if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
		if($Score == '' || !is_numeric($Score) || $Score < 0){
			$error['Score'] = $Lang['eDiscipline']['Import_GM_Error'][3];
		}
		$Score = abs($Score);
	}
	
	### Checking - Valid PICs
	$PicArr = explode(",",$PIC);
	for($ct=0; $ct<sizeof($PicArr); $ct++)
	{
		$sql = "select UserID as PICID, ".getNamefieldByLang()." as Name from INTRANET_USER where UserLogin = '".$PicArr[$ct]."' and RecordType = 1";
		$tmpResult = $lu->returnArray($sql);
		$PicID = $tmpResult[0]['PICID'];
		
		if($ldiscipline->OnlyAdminSelectPIC)
		{
			if($PicID != $UserID) {
				$error['PIC'] = $Lang['eDiscipline']['OnlyAdminSelectPIC'];
			}
		}
		else if(trim($PicID)=="")
		{
			$error['PIC']=$iDiscipline['Good_Conduct_No_Staff'];
		}
		
		$PicIDArr[] = $PicID;
		$PicName[] = $tmpResult[0]['Name'];
	}
	
	# (START) Check whether same item already added to student on same day (if $ldiscipline->NotAllowSameItemInSameDay = 1)
	if($ldiscipline->NotAllowSameItemInSameDay) {	# Not allow same student with same item in a same day
		$isExist = array();
		$hiddenField = "";
		$success = 1;
	
		$thisCatID = $ItemCategoryID;
		$thisItemID = $ItemID;
		$thisRecordDate = $RecordDate;
		$isExist[$StudentID] = $ldiscipline->checkSameGMItemInSameDay($StudentID, $thisCatID, $thisItemID, $thisRecordDate);
		if($isExist[$StudentID]==1)
			$error['duplicateOnSameDay'] = $Lang['eDiscipline']['ItemAlreadyAdded'];
	}
	# (END) Check whether same item already added to student on same day (if $ldiscipline->NotAllowSameItemInSameDay = 1)
	
	$Category = (($RecordType==1)? $i_Discipline_GoodConduct: (($RecordType==-1) ? $i_Discipline_Misconduct : ""));
	$css = (sizeof($error)==0) ? "tabletext":"red";
	
	$x .= "<tr class=\"tablebluerow".($i%2+1)."\">";
	$x .= "<td class=\"$css\">".($i+1)."</td>";
	$x .= "<td class=\"$css\">". $ClassInfo ."</td>";
	$x .= "<td class=\"$css\">". $StudentName ."</td>";
	$x .= "<td class=\"$css\">". $oriRecordDate ."</td>";
	$x .= "<td class=\"$css\">". $Semester ."</td>";
	$x .= "<td class=\"$css\">". $Category ."</td>";
	$x .= "<td class=\"$css\">". $ItemName ."</td>";
	if($sys_custom['eDiscipline_GM_Times']){
		$x .= "<td class=\"$css\">". $GMCount."</td>";
	}
	if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
		$x .= "<td class=\"$css\">". $Lang['eDiscipline']['Deduct']."&nbsp;". $Score."</td>";
	}
	$x .= "<td class=\"$css\">". implode(",",$PicName) ."</td>";
	$x .= "<td class=\"$css\">". stripslashes($Remark)."</td>";
	$x .= "<td class=\"$css\">";
	if(sizeof($error)>0)
	{
		foreach($error as $Key=>$Value)
		{
			$x .= $Value.'<br/>';
		}	
	}
	else
	{
		$x .= "--";
	}
	$x .= "</td>";
	$x .= "</tr>";
	
	$sql_fields = "(ClassName, ClassNumber,CategoryID,Category,ConductCat,ConductItem, ItemID, RecordDate, StudentID, Remark, PICID, RecordStatus,  RecordType, Year, Semester, CountNo,Teacher,StudentName,ItemName, UserID, GMCount ".($sys_custom['eDiscipline']['PooiToMiddleSchool']?",ScoreChange":"").")";
	$sql_values = "('$ClassName',$ClassNumber,'$ItemCategoryID','$Category','".addslashes($CondCategory)."','".addslashes($CondItem)."','$ItemID','$RecordDate','$StudentID','".addslashes($Remark)."', '".implode(',',$PicIDArr)."', 1,  $RecordType, '$Year', '$Semester',0,'".implode(",",$PicName)."','$StudentName','".intranet_htmlspecialchars(addslashes($ItemName))."', '". $_SESSION["UserID"] ."', '$GMCount' ".($sys_custom['eDiscipline']['PooiToMiddleSchool']?",'$Score'":"").")";
	
	$sql = "insert into temp_goodconduct_import $sql_fields values $sql_values ";
	$ldiscipline->db_db_query($sql);
	
	if($error)
	{
		$error_occured++;
	}
}
$x .= "</table>";

if($error_occured)
{
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import.php'");
}
else
{	
	$import_button = $linterface->GET_ACTION_BTN($button_import, "submit")." &nbsp;".$linterface->GET_ACTION_BTN($button_back, "button", "window.location='import.php'");	
}
?>

<br />
<form name="form1" method="post" action="import_update.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center">
		<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2"><?=$x?></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
				<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>