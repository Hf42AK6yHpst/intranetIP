<?php
# using: 

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libucc.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-NewLeaf");
if(!$ldiscipline->use_newleaf) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$RecordIDAry = array();

if(is_array($RecordID))
	$RecordIDAry = $RecordID;
else if($RecordID)
	$RecordIDAry[] = $RecordID;
	
if(empty($RecordIDAry))	header("Location: index.php");

$linterface = new interface_html();

# menu highlight setting
$CurrentPage = "Management_GoodConductMisconduct";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eDiscipline['Good_Conduct_and_Misconduct']);

# navigation bar
//$PAGE_NAVIGATION[] = array($eDiscipline['RecordList'], "index.php?SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&num_per_page=$num_per_page&pageNo=$pageNo&order=$order&field=$field&numPerPage=$numPerPage&MeritType=$MeritType&s=$s&waitApproval=$waitApproval&approved=$approved&rejected=$rejected&released=$released&waived=$waived");
$PAGE_NAVIGATION[] = array($eDiscipline['RecordList'], "javascript:click_cancel()");
$PAGE_NAVIGATION[] = array($eDiscipline["NewLeaf_Rehabilitation_Record"], "");

# build html part
$i = 0;
$imgPath = "{$image_path}/{$LAYOUT_SKIN}";
$use_intranet_homework = $ldiscipline->accumulativeUseIntranetSubjectList();

$recID = array();

foreach($RecordIDAry as $GMRecordID)
{
	$temp = "";
	$reference = "";
	$datainfo = $ldiscipline->RETRIEVE_CONDUCT_INFO($GMRecordID);
	$data = $datainfo[0];
	
	$StudentID = $data['StudentID'];
	$remark = $data['Remark'];
	$ItemID = $data['ItemID'];
	$lu = new libuser($StudentID);
	$thisClassNumber = $lu->ClassNumber;
	$thisClassName = $lu->ClassName.($thisClassNumber ? "-".$thisClassNumber : "");
	
	$namefield = getNameFieldWithLoginByLang();
	$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE RecordType = 1 AND UserID IN (". $data['PICID'] .") AND RecordStatus = 1 ORDER BY $namefield";
	$array_PIC = $ldiscipline->returnArray($sql);
	$PICAry = array();
	foreach($array_PIC as $k=>$d)
		$PICAry[] = $d[1];
	$PICName = empty($PICAry) ? "---" : implode("<br>",$PICAry);
	
	# if the record is waived, cannot wavie again  
	$isWavied = $data['RecordStatus'] == DISCIPLINE_STATUS_WAIVED ? 1 : 0;
	
	# if the record is rejected, cannot wavie again
	$isRejected = $data['RecordStatus'] == DISCIPLINE_STATUS_REJECTED ? 1 : 0;
	
	$actionAry = array();
	
	if($ldiscipline->HAS_DETENTION_FROM_GM($GMRecordID))
	{
		$actionAry[] = "<a href='javascript:;' onClick=\"changeClickID({$GMRecordID});moveObject('show_detail{$GMRecordID}', true);showResult({$GMRecordID},'detention');MM_showHideLayers('show_detail{$GMRecordID}','','show');\">". $eDiscipline['Detention'] ."</a><div id='show_detail{$GMRecordID}' style='position:absolute; width:280px; height:180px; z-index:1;'></div>";
		$y .= "document.onload=moveObject('show_detail{$GMRecordID}', true);\n";
		$jsDetention .= ($jsDetention!='') ? ','.$GMRecordID : $GMRecordID;
	}
	if($y != "") {
		$showY = "<script languahe='javascript'>window.onload = pageOnLoad;function pageOnLoad() {".$y."}</script>";
	}
	
	if($data['NoticeID'] != 0)
	{
		$lc = new libucc($data['NoticeID']);
		$notice_info = $lc->getNoticeDetails();
		$notice_url = $notice_info['url'];
		$actionAry[] = "<a href='javascript:;' onClick=\"newWindow('{$notice_url}',1)\">". $eDiscipline["SendNotice"] ."</a>";
	}
	if(sizeof($actionAry)==0) $actionAry[] = "---";
	
	if($sys_custom['eDisciplinev12_Munsang_NewLeafScheme']) {
		# do nothing
	} else {
		$waivePassedDay = $ldiscipline->countWaivePassedDate($data['CategoryID'], $data['RecordDate'], $StudentID, $GMRecordID);
	}
	
	$result = $ldiscipline->newLeafWaiveCountTotal($data['CategoryID'], $data['RecordDate'], $StudentID);
	list($newLeafWaiveDay, $newLeafWaiveCountTotal) = $result;
	
	$newLeafWaiveCountUsed = $ldiscipline->newLeafWaiveCountUsed($data['CategoryID'], $data['RecordDate'], $StudentID);
	
	$newLeafWaiveCountRemain = !isset($leafRemain[$ItemID][$StudentID]) ? ($newLeafWaiveCountTotal - $newLeafWaiveCountUsed) : $leafRemain[$ItemID][$StudentID];
		
	if($newLeafWaiveDay!="" && $newLeafWaiveCountTotal!="") {
		
		if(!isset($leafRemain[$ItemID][$StudentID])) {
			$leafRemain[$ItemID][$StudentID] = $newLeafWaiveCountRemain - 1;
		} else {
			$leafRemain[$ItemID][$StudentID] = $leafRemain[$ItemID][$StudentID] - 1;
		}
	}
	//if($data['CategoryID']==3)
	//	echo $newLeafWaiveCountTotal.'/'.$newLeafWaiveCountUsed.'/'.$leafRemain[$ItemID][$StudentID].'<br>';
	
	if($sys_custom['eDisciplinev12_Munsang_NewLeafScheme']) {
		$i++;	
	} else {
		if(!$isWavied && !$isRejected && $waivePassedDay>=0 && $waivePassedDay!='---' && $data['RecordType']!=1 && !($newLeafWaiveCountTotal!="" && isset($leafRemain[$ItemID][$StudentID]) && $leafRemain[$ItemID][$StudentID]<0) && $newLeafWaiveDay!="")	$i++;
	}
	
	
	$sql = "SELECT";
	if($use_intranet_homework) {
		//$sql .= " IF(a.CategoryID = 2, CONCAT(c.Name,' - ',e.SubjectName), IF(d.Name IS NULL, c.Name, CONCAT(c.Name,' - ',d.Name))) as itemName ";
		$sql .= " IF(a.CategoryID = 2, CONCAT(c.Name,' - ',".Get_Lang_Selection('CH_DES','EN_DES')."), IF(d.Name IS NULL, c.Name, CONCAT(c.Name,' - ',d.Name))) as itemName ";
	} else {
		$sql .= " CONCAT(c.Name,' - ',d.Name) as itemName ";
	}
	$sql .= "FROM 
				DISCIPLINE_ACCU_RECORD as a LEFT OUTER JOIN
				DISCIPLINE_ACCU_CATEGORY as c ON a.CategoryID = c.CategoryID LEFT OUTER JOIN
				DISCIPLINE_ACCU_CATEGORY_ITEM as d ON (a.ItemID = d.ItemID)
			";
	if($use_intranet_homework) {
		//$sql .= " LEFT OUTER JOIN INTRANET_SUBJECT as e ON (a.ItemID = e.SubjectID)";
		$sql .= " LEFT OUTER JOIN ASSESSMENT_SUBJECT AS e ON (a.ItemID = e.RecordID)";
	}
	$sql .=	" WHERE a.DateInput IS NOT NULL AND a.RecordID=".$data['RecordID'];
	$itemName = $ldiscipline->returnVector($sql);
	$ItemText = $itemName[0];
	
	$pendingImg = "<img src={$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif width=20 height=20 align=absmiddle title=\"$i_Discipline_System_Award_Punishment_Pending\" border=0>";
	$rejectImg = "<img src={$image_path}/{$LAYOUT_SKIN}/icon_reject_l.gif width=20 height=20 align=absmiddle title=\"$i_Discipline_System_Award_Punishment_Rejected\" border=0>";
	$releasedImg = "<img src={$image_path}/{$LAYOUT_SKIN}/icon_to_student.gif width=20 height=20 align=absmiddle title=\"$i_Discipline_Released_To_Student\" border=0>";
	$approvedImg = "<img src={$image_path}/{$LAYOUT_SKIN}/icon_approve_b.gif width=20 height=20 align=absmiddle title=\"$i_Discipline_System_Award_Punishment_Approved\" border=0>";
	$waivedImg = "<img src={$image_path}/{$LAYOUT_SKIN}/icon_waived.gif width=20 height=20 align=absmiddle title=\"$i_Discipline_System_Award_Punishment_Waived\" border=0>";
	$remarksImg = "<img src={$image_path}/{$LAYOUT_SKIN}/icon_remark.gif width=20 height=20 align=absmiddle title=\"".$iDiscipline['Remarks']."\" border=0>";
	
	$status = ($data['RecordStatus']==DISCIPLINE_STATUS_PENDING) ? $pendingImg : "";
	$status .= ($data['ReleaseStatus']==DISCIPLINE_STATUS_RELEASED) ? $releasedImg : "";
	$status .= ($data['RecordStatus']==DISCIPLINE_STATUS_REJECTED) ? $rejectImg : "";
	$status .= ($data['RecordStatus']==DISCIPLINE_STATUS_APPROVED) ? $approvedImg : "";
	if($data['RecordStatus']==DISCIPLINE_STATUS_WAIVED) {
		$status .= "<a href='javascript:;' onClick=\"changeClickID({$data['RecordID']});moveObject('show_waive{$data['RecordID']}', true);showResult({$data['RecordID']},'waived');MM_showHideLayers('show_waive".$data['RecordID']."','','show');\">".$waivedImg."</a><div onload='changeClickID({$data['RecordID']});moveObject('show_waive{$data['RecordID']}', true);' id='show_waive".$data['RecordID']."' style='position:absolute; width:280px; height:180px; z-index:1;'></div>";
		$jsWaive .= ($jsWaive!='') ? ','.$GMRecordID : $GMRecordID;
	}
	
	$waived_css = ($isWavied || $isRejected || $waivePassedDay<0 || $waivePassedDay=='---' || $data['RecordType']==1 || ($newLeafWaiveCountTotal!="" && isset($leafRemain[$ItemID][$StudentID]) && $leafRemain[$ItemID][$StudentID]<0) || $newLeafWaiveDay=="") ? "record_waived" : " ";

	# table content 
	$row .= "<tr class='row_approved $waived_css'>";
	if($sys_custom['eDisciplinev12_Munsang_NewLeafScheme']) {
		$row .= "<td valign='top'>$i</td>";
	} else {
		$row .= "<td valign='top' >". (($isWavied || $isRejected || $waivePassedDay<0 || $waivePassedDay=='---' || $data['RecordType']==1 || ($newLeafWaiveCountTotal!="" && isset($leafRemain[$ItemID][$StudentID]) && $leafRemain[$ItemID][$StudentID]<0) || $newLeafWaiveDay=="") ? "&nbsp;" : $i) ."&nbsp;</td>";
	}
	$row .= "<td valign='top'>". $thisClassName ."&nbsp;</td>";
	$row .= "<td valign='top'>". $lu->UserName()."&nbsp;</td>";
	$row .= "<td valign='top'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/". ($data['RecordType']==1? "icon_merit":"icon_demerit").".gif' width='20' height='20' title='".($data['MeritType']==1?$i_Merit_Award:$i_Merit_Punishment)."'>&nbsp;</td>";
	//$row .= "<td valign='top'>". $data['ProfileMeritCount'] ." ". $ldiscipline->RETURN_MERIT_NAME($data['ProfileMeritType']) ."&nbsp;</td>";
	$row .= "<td valign='top'>". $ItemText;
	if($remark != '') {
		$row .= "&nbsp;";
		$row .= "<a href='javascript:;' onClick=\"changeClickID({$GMRecordID}); moveObject('show_remark".$GMRecordID."', true); 
							showResult({$GMRecordID},'record_remark'); MM_showHideLayers('show_remark".$GMRecordID."','','show');\">";
			$row .= $remarksImg;
		$row .= "</a>";
		$row .= "<div onload='changeClickID({$GMRecordID}); moveObject(\'show_remark".$GMRecordID."}\', true);'
							id='show_remark".$GMRecordID."' style='position:absolute; width:320px; height:150px; z-index:1;'></div>";
							
		$jsRemarkAry .= ($jsRemarkAry != '') ? ",".$GMRecordID : $GMRecordID;
	}
	$row .= "</td>";
	$row .= "<td valign='top'>". substr($data['RecordDate'],0,10) ."&nbsp;</td>";
	$row .= "<td valign='top'>". $PICName ."&nbsp;</td>";
	$row .= "<td align='left' valign='top'>". implode("<br>",$actionAry) ."&nbsp;</td>";
	//$row .= "<td valign='top'>".$waivePassedDay."&nbsp;</td>";
	$row .= "<td valign='top'>".$status."&nbsp;</td>";
	
	if($sys_custom['eDisciplinev12_Munsang_NewLeafScheme']) {
		$row .= "<td valign='top' nowrap>&nbsp;</td>";
		$recID[] = $GMRecordID;
	} else {
		if($isWavied)
			$row .= "<td valign='top'>". $eDiscipline["RecordIsWavied"] ."&nbsp;</td>";
		else if($isRejected)
			$row .= "<td valign='top'>". $eDiscipline["RecordIsRejected"] ."&nbsp;</td>";	
		else if($data['RecordType']==1)
			$row .= "<td valign='top'>".$eDiscipline["RecordisGoodConduct"]."&nbsp;</td>";	
		else if($newLeafWaiveDay=="")
			$row .= "<td valign='top'>".$eDiscipline["RecordNoNewLeafSetting"]."&nbsp;</td>";	
		else if($waivePassedDay<0)
			$row .= "<td valign='top'>". $eDiscipline["RecordNotPassWaiveDate"] ."&nbsp;</td>";	
		else if($waivePassedDay=="---")
			$row .= "<td valign='top'>".$eDiscipline["RecordHappenAgain"]."</td>";	
		else if($newLeafWaiveCountTotal!="" && isset($leafRemain[$ItemID][$StudentID]) && $leafRemain[$ItemID][$StudentID]<0)
			$row .= "<td valign='top'>".$eDiscipline["RecordNotEnoughNewLeaf"]."&nbsp;</td>";	
		else {
			$row .= "<td valign='top' nowrap>&nbsp;</td>";
			$recID[] = $GMRecordID;
		}
	}
	$row .= "</tr>";
}

$jsContent .= ($jsRemarkAry != '') ? "var jsRemark=[{$jsRemarkAry}];\n" : "var jsRemark=[];\n";
$jsContent .= ($jsDetention != '') ? "var jsDetention=[{$jsDetention}];\n" : "var jsDetention=[];\n";
$jsContent .= ($jsWaive != '') ? "var jsWaive=[{$jsWaive}];\n" : "var jsWaive=[];\n";

# Start layout
$linterface->LAYOUT_START();
echo "<script language='javascript'>$jsContent</script>";
?>

<script language="javascript">
<!--
function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) 
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}

function apply_all()
{
	<? foreach($RecordIDAry as $GMRecordID)	{ ?>
		document.form1.reason_<?=$GMRecordID?>.value=document.form1.top_reason.value;
	<? }?>
}
function click_cancel()
{
	form1.action = "index.php";
	form1.submit();
	/*
	<?if($back_page) {?>
		window.location= "<?=$back_page?>?id=<?=$RecordIDAry[0]?>";
	<? } else {?>
		window.location="index.php?SchoolYear=<?=$SchoolYear?>&semester=<?=$semester?>&targetClass=<?=$targetClass?>&num_per_page=<?=$num_per_page?>&pageNo=<?=$pageNo?>&order=<?=$order?>&field=<?=$field?>&page_size_change=<?=$page_size_change?>&numPerPage=<?=$numPerPage?>&MeritType=<?=$MeritType?>&s=<?=$s?>&clickID=<?=$clickID?>&waitApproval=<?=$waitApproval?>&approved=<?=$approved?>&rejected=<?=$rejected?>&released=<?=$released?>&waived=<?=$waived?>&pic=<?=$pic?>";
	<? } ?>
	*/
}


//-->
</script>
<script language="javascript">
<!--

var xmlHttp

function showResult(str,flag)
{
		
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 

	var url = "";
		
	url = "get_live.php?flag="+flag;
	url = url + "&RecordID=" + str
	url = url + "&sid=" + Math.random()
	
	if(flag=='detention') {
		xmlHttp.onreadystatechange = stateChanged 
	} else if(flag=='record_remark') {
		xmlHttp.onreadystatechange = stateChanged3 
	} else if(flag=='waived') {
		xmlHttp.onreadystatechange = stateChanged2 
	}
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_detail"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_detail"+id).style.border = "0px solid #A5ACB2";
	} 
}

function stateChanged2() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_waive"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_waive"+id).style.border = "0px solid #A5ACB2";
	} 
}

function stateChanged3() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_remark"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_remark"+id).style.border = "0px solid #A5ACB2";
	} 
}

function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}
/*********************************/
function getObject( obj ) {

	if ( document.getElementById ) {
		obj = document.getElementById( obj );
	
	} else if ( document.all ) {
		obj = document.all.item( obj );
	
	} else {
		obj = null;
	}
	
	return obj;
}

function moveObject( obj, e ) {
	var tempX = 0;
	var tempY = 0;
	var offset = 5;
	var objHolder = obj;
	
	obj = getObject( obj );
	if (obj==null) {return;}
	
	if (document.all) {
		tempX = event.clientX + document.body.scrollLeft;
		tempY = event.clientY + document.body.scrollTop;
	} else {
		tempX = e.pageX;
		tempY = e.pageY;
	}
	
	if (tempX < 0){tempX = 0} else {tempX = tempX - 300}
	if (tempY < 0){tempY = 0} else {tempY = tempY}
	
	obj.style.top  = (tempY + offset) + 'px';
	obj.style.left = (tempX + offset) + 'px';
	
	displayObject( objHolder, true );
}

function displayObject( obj, show ) {

	obj = getObject( obj );
	if (obj==null) return;
	
	obj.style.display = show ? 'block' : 'none';
	obj.style.visibility = show ? 'visible' : 'hidden';
}

function changeClickID(id) {
	document.form1.clickID.value = id;
	hideAllOtherLayer();
}

function hideAllOtherLayer() {
	var layer = "";
	
	if(jsDetention.length != 0) {
		for(i=0;i<jsDetention.length;i++) {
			layer = "show_detail" + jsDetention[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	if(jsWaive.length != 0) {
		for(i=0;i<jsWaive.length;i++) {
			layer = "show_waive" + jsWaive[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	if(jsRemark.length != 0) {
		for(i=0;i<jsRemark.length;i++) {
			layer = "show_remark" + jsRemark[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
}

//-->
</script>
<?=$showY?>
<form name="form1" method="POST" action="newleaf_update2.php">

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td class="navigation">
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
		</table>
	</td>
</tr>

<tr>
	<td align="center">
		<table width="90%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="form_sep_title"><i>- <?=$eDiscipline["SelectedRecord"]?> -</i></td>
		</tr>
		<tr>
			<td align="center">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr class="tabletop">
					<td align="center">#</td>
					<td><?=$i_general_class?></td>
					<td><?=$i_general_name?></td>
					<td>&nbsp;</td>
					<!--<td><?=$i_Discipline_Reason2?></td>//-->
					<td><?=$eDiscipline["Category_Item"]?></td>
					<td><?=$Lang['eDiscipline']['EventDate']?></td>
					<td><?=$i_Discipline_PIC?></td>
					<td><?=$eDiscipline["Action"]?></td>
					<!--<td><?=$i_Discipline_System_GoodConduct_Misconduct_Waive_Date_Count?></td>//-->
					<td><?=$i_Discipline_System_Discipline_Status?></td>
					<td><?/*=$eDiscipline["WaiveReason"]*/?>&nbsp;</td>
				</tr>
<!--				
				<tr class="tablebottom">
					<td width="15" align="center">&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td width="100">&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td align="center">&nbsp;</td>
					<td><table width="100%" border="0" cellpadding="0" cellspacing="2" class="layer_table">
						<tr>
							<td><input name="top_reason" type="text" class="textboxtext" value="" ></td>
							<td width="25" align="center"><a href="#" onClick="javascript:apply_all();"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_assign.gif" width="12" height="12" border="0"></a></td>
						</tr>
					</table></td>
				</tr>
//-->			
				<?=$row?>
		
		
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center">
				<? if($i) { ?>
				<?= $linterface->GET_ACTION_BTN($button_continue, "submit")?>&nbsp;
				<? } ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "click_cancel();")?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>

<br />
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr>
	<td align="center">
		<?= $ldiscipline->showWarningMsg($i_Discipline_System_Conduct_Instruction, $eDiscipline['NewLeaf_Notes']) ?>
	</td>
</tr>
</table>


<? foreach($recID as $d) { ?>
	<input type="hidden" name="RecordID[]" value="<?=$d?>" />	
<? } ?>

<input type="hidden" name="SchoolYear" value="<?=$SchoolYear?>" />
<input type="hidden" name="semester" value="<?=$semester?>" />
<input type="hidden" name="targetClass" value="<?=$targetClass?>" />
<input type="hidden" name="approved" value="<?=$approved?>" />
<input type="hidden" name="rejected" value="<?=$rejected?>" />
<input type="hidden" name="waived" value="<?=$waived?>" />
<input type="hidden" name="num_per_page" value="<?=$num_per_page?>" />
<input type="hidden" name="pageNo" value="<?=$pageNo?>" />
<input type="hidden" name="order" value="<?=$order?>" />
<input type="hidden" name="field" value="<?=$field?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$numPerPage?>" />
<input type="hidden" name="MeritType" value="<?=$MeritType?>" />
<input type="hidden" name="RecordType" value="<?=$RecordType?>" />
<input type="hidden" name="s" value="<?=$s?>" />
<input type="hidden" name="pic" value="<?=$pic?>" />
<input type="hidden" name="clickID" value="<?=$clickID?>" />
<input type="hidden" name="back_page" value="<?=$back_page?>" />
<input type="hidden" name="newleaf" value="<?=$newleaf?>" />
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();

?>