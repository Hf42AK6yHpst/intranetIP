<?php
// Modifying by: 

########## Change Log ###############
#
#   Date    :   2019-05-01 (Bill)
#               prevent SQL Injection + Cross-site Scripting
#
#	Date	:	2017-03-02	(Bill)	[2016-0823-1255-04240]
#				default uncheck checkbox "Send email to notify parents"	($sys_custom['eDiscipline']['DefaultUncheckSendEmailCheckbox'])
#
#	Date	:	2015-04-30 (Bill)	[2014-1216-1347-28164]
#				add option to send push message to parent
#				disable finish button after click
#
#	Date	:	2010-06-10 (Henry)
#	Detail 	:	select default eNotice Template
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

if(empty($RecordID))	header("Location: index.php");

$ldiscipline = new libdisciplinev12();

$linterface = new interface_html();

# Menu highlight
$CurrentPage = "Management_GoodConductMisconduct";
$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Data
$RecordID = IntegerSafe($RecordID);
$datainfo = $ldiscipline->RETRIEVE_CONDUCT_INFO($RecordID);
$data = $datainfo[0];
$student = $data['StudentID'];
$cid = $data['CategoryID'];

# Default eNotice template info
$gmCatInfo = $ldiscipline->getGMCategoryInfoByCategoryID($cid);
$TemplateID = $gmCatInfo['TemplateID'];
$TemplateAry = $ldiscipline->retrieveTemplateDetails($TemplateID);
$TemplateCategoryID = $TemplateAry[0]['CategoryID'];

$catTmpArr = $ldiscipline->TemplateCategory();

// [2014-1216-1347-28164]
$canSendPushMsg = false;
if($plugin['eClassApp'] && $sys_custom['eDiscipline']['SendDetentionPushMessage'] && $isMsg){
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	$luser = new libuser();
	$canSendPushMsg = $data['RecordType']==-1 && $luser->getParentUsingParentApp($student, true);
}

# Tag
$TAGS_OBJ[] = array($eDiscipline['Award_and_Punishment']);
// [2014-1216-1347-28164] change navigation name
$PAGE_NAVIGATION[] = array(($canSendPushMsg? $Lang['eDiscipline']['DetentionMgmt']['SendPushMessage'] : $eDiscipline["SendNotice"]), "");

if(is_array($catTmpArr))
{
	$catArr[0] = array("0","-- $button_select --");
	foreach($catTmpArr as $Key=>$Value)
	{
		# check the Template Catgory has template or not
		$NoticeTemplateAvaTemp = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO("",1, $Key);
		if(!empty($NoticeTemplateAvaTemp)) {
			$catArr[] = array($Key,$Value);
	    }
	}
}

                $catSelection = $linterface->GET_SELECTION_BOX($catArr, 'id="CategoryID" name="CategoryID" onChange="changeCat(this.value)"', "", $TemplateCategoryID);
                
				$resultStudentName = $ldiscipline->getStudentNameByID($student);
				list($TmpUserID, $TmpName, $TmpClassName, $TmpClassNumber) = $resultStudentName[0];
                
				$TableNotice .= "<table align=\"center\" width=\"90%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">\n";
				$TableNotice .= "<tr>\n";
				$TableNotice .= "<td colspan=\"2\" valign=\"top\" nowrap>\n";
				$TableNotice .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
				$TableNotice .= "<tr>\n";
				$TableNotice .= "<td><img src=\"$image_path/$LAYOUT_SKIN/ediscipline/icon_stu_ind.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"> <span class=\"sectiontitle\">$TmpClassName-$TmpClassNumber $TmpName</span></td>\n";
				$TableNotice .= "<td align=\"right\"><span class=\"sectiontitle\">\n";
				$TableNotice .= $linterface->GET_BTN($button_view_template, "button", "javascript:previewNotice();");
				$TableNotice .= "</span></td>\n";
				$TableNotice .= "</tr>\n";
				$TableNotice .= "</table>\n";
				$TableNotice .= "</td>\n";
				$TableNotice .= "</tr>\n";
				$TableNotice .= "<tr valign=\"top\" class=\"tablerow1\">\n";
				$TableNotice .= "<td width=\"20%\" nowrap class=\"formfieldtitle\"><span class=\"tabletext\">$i_Discipline_Usage_Template <span class=\"tabletextrequire\">*</span></span></td>\n";
				$TableNotice .= "<td>\n";
				$TableNotice .= "<input type=\"hidden\" id=\"StudentID\" name=\"StudentID\" value=\"".$TmpUserID."\" />";
				$TableNotice .= "<input type=\"hidden\" id=\"StudentName\" name=\"StudentName\" value=\"".$TmpName."\" />";
				$TableNotice .= $catSelection." <select name=\"SelectNotice\" id=\"SelectNotice\"><option value=\"0\">-- $button_select --</option></select>\n";
				$TableNotice .= "</td>\n";
				$TableNotice .= "</tr>\n";
				$TableNotice .= "<tr class=\"tablerow1\">\n";
				$TableNotice .= "<td valign=\"top\" nowrap class=\"formfieldtitle\">$i_Discipline_Additional_Info</td>\n";
				$TableNotice .= "<td valign=\"top\">".$linterface->GET_TEXTAREA("TextAdditionalInfo", "")."</td>";
				$TableNotice .= "</tr>\n";
				
				// [2014-1216-1347-28164] display checkbox for eClass app notification
//				if($canSendPushMsg && $data['RecordType']==-1 && $luser->getParentUsingParentApp($student, true)){
//				if($canSendPushMsg){
//					$TableNotice .= "<tr class=\"tablerow1\">\n";
//					$TableNotice .= "<td valign=\"top\" nowrap>&nbsp;</td>\n";
//					$TableNotice .= "<td valign=\"top\"><input type='checkbox' name='eClassAppNotify' id='eClassAppNotify' value='1' checked><label for='eClassAppNotify'>".$Lang['eDiscipline']['DetentionMgmt']['SendPushMessage']."</label></td>";
//					$TableNotice .= "</tr>\n";
//				} else {
				if(!$canSendPushMsg){
					$TableNotice .= "<tr class=\"tablerow1\">\n";
					$TableNotice .= "<td valign=\"top\" nowrap>&nbsp;</td>\n";
					$TableNotice .= "<td valign=\"top\"><input type='checkbox' name='emailNotify' id='emailNotify' value='1' ".($sys_custom['eDiscipline']['DefaultUncheckSendEmailCheckbox']? "" : "checked")."><label for='emailNotify'>".$Lang['eDiscipline']['EmailNotifyParent']."</label></td>";
					$TableNotice .= "</tr>\n";
				}
				$TableNotice .= "</table>\n";

		for ($i=0; $i<sizeof($catArr); $i++) {
			$result = $ldiscipline->getDetentionENoticeByCategoryID("DISCIPLINE", $catArr[$i][0]);
			for ($j=0; $j<=sizeof($result); $j++) {
				if ($j==0) {
					$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]."Value = new Array(".(sizeof($result)+1).");\n";
					$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]." = new Array(".(sizeof($result)+1).");\n";
					$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."Value[$j] = \"0\"\n";
					$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."[$j] = \"-- $button_select --\"\n";
				} else {
					$tempTemplate = $result[$j-1][1];
					$tempTemplate=str_replace("&lt;", "<", $tempTemplate);
			        $tempTemplate=str_replace("&gt;", ">", $tempTemplate);
			        $tempTemplate=str_replace("&#039;","'",$tempTemplate);
			        $tempTemplate=str_replace("&amp;", "&", $tempTemplate);
        			//$tempTemplate=str_replace("&quot;", "\"", $tempTemplate);
        
					$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."Value[$j] = \"".$result[$j-1][0]."\"\n";
					$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."[$j] = \"".$tempTemplate."\"\n";
				}
			}
		}

# Start layout
$linterface->LAYOUT_START();
?>

<!--<script type="text/javascript" src = "<?=$PATH_WRT_ROOT?>templates/2007a/js/jslb_ajax.js" charset = "utf-8"></script>-->

<script language="Javascript">
<!--
<?=$jTemplateString?>

function applyToAll(studentCount){
	for(var i=1;i<=studentCount;i++){
		document.getElementById('DetentionCount'+i).value = document.getElementById('DetentionCount0').value;
		updateSessionCell(i);
		for(var j=1;j<=document.getElementById('DetentionCount0').value;j++){
			document.getElementById('SelectSession'+i+'_'+j).value = document.getElementById('SelectSession0_'+j).value;
			if (document.getElementById('SelectSession'+i+'_'+j).value != document.getElementById('SelectSession0_'+j).value){
				document.getElementById('SelectSession'+i+'_'+j).value = 'AUTO';
			}
		}
		document.getElementById('TextRemark'+i).value = document.getElementById('TextRemark0').value;
	}
}

function checkForm()
{
	if(document.getElementById('SelectNotice').value==0) {
		document.getElementById('submit_btn').disabled  = false; 
		document.getElementById('submit_btn').className  = "formbutton_v30 print_hide"; 
		alert("<?=$i_alert_pleaseselect." ".$i_Discipline_Template?>");
		return false;
	} else {
		document.getElementById('submit_flag').value='YES';
		document.form1.submit();
	}
}

function jsBack()
{
	document.form1.id.value = <?=$RecordID?>;
	document.form1.action="edit.php";
	document.form1.submit();
}

function changeCat(cat){
	var x = document.getElementById("SelectNotice");
	if(x)
	{
		var listLength = x.length;
	}
	for (var i=listLength-1; i>=0; i--) {
		x.remove(i);
	}
	
	try {
		var tmpCatLength = eval("jArrayTemplate"+cat).length;
	} catch (err) {
		var tmpCatLength = 0;
	}
	
	for (var j=0; j<tmpCatLength; j++) {
		var y = document.createElement('option');
		y.text = eval("jArrayTemplate"+cat)[j];
		tempText = eval("jArrayTemplate"+cat)[j];
		y.value = eval("jArrayTemplate"+cat+"Value")[j];
		tempValue = eval("jArrayTemplate"+cat+"Value")[j];
		try {
			x.add(y,null); // standards compliant
		} catch(ex) {
			x.add(y); // IE only
		}
	}
}

function itemSelected(itemID) {
	var obj = document.form1;
	var item_length = obj.SelectNotice.length;
	for(var i=0; i<item_length; i++) {
		if(obj.elements['SelectNotice'].options[i].value=='<?=$TemplateID?>') {
			obj.elements['SelectNotice'].selectedIndex = i;
			//alert(i);
			break;	
		}
	}
}

function previewNotice(){
	var templateID = document.getElementById("SelectNotice").value;
	if(templateID==0) {
		alert("<?=$i_alert_pleaseselect." ".$i_Discipline_Template?>");
	} else {
		var studentName = '';
		var additionInfo = document.getElementById("TextAdditionalInfo").value;
		newWindow("preview_notice.php?tid="+templateID+"&pv_studentname="+studentName+"&pv_additionalinfo="+additionInfo);
	}
}
//-->
</script>

<form name="form1" method="POST" action="notice_new_update.php">

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td class="navigation">
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
		</table>
	</td>
</tr>

<tr>
	<td>
		<br />
		<?=$TableNotice?>
		<table align="center" width="95%" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td valign="top" nowrap="nowrap">
					<span class="tabletextremark"><?=$i_general_required_field?></span>
				</td>
				<td width="80%">&nbsp;</td>
			</tr>
		</table>
	</td>
</tr>

<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_continue, "button", "this.disabled=true; this.className='formbutton_disable_v30 print_hide'; return checkForm();", "submit_btn")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "jsBack()")?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>

<br />

<input type="hidden" name="RecordID" id="RecordID" value="<?=$RecordID?>" />
<input type="hidden" name="submit_flag" id="submit_flag" value="NO" />
<input type="hidden" id="id" name="id" />

<input type="hidden" name="isMsg" id="isMsg" value="<?=$canSendPushMsg?>" />

</form>

<?
print $linterface->FOCUS_ON_LOAD("form1.DetentionCount0");

if($TemplateCategoryID!="0" && $TemplateCategoryID!="") {
	echo "<script language='javascript'>changeCat('$TemplateCategoryID');itemSelected('$TemplateID')</script>";
}	

$linterface->LAYOUT_STOP();
intranet_closedb();

if ($FailFlag) {
?>
<script language="javascript">
<?=$jInitialForm?>
</script>
<?
}
?>