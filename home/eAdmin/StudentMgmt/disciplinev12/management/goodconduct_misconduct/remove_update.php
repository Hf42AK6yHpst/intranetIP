<?php
# modify by :

########## Change Log ###############
#
#	Date	:	2010-06-10 (Henry)
#	Detail 	:	add attachment to record
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");

intranet_auth();
intranet_opendb();

# Check Access Right
$ldiscipline = new libdisciplinev12();
if(!($ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-DeleteAll") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-DeleteOwn")))
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$RecordIDAry = array();
if(is_array($RecordID))
	$RecordIDAry = $RecordID;
else
	$RecordIDAry[] = $RecordID;
	
$all_delete = 1;
foreach($RecordIDAry as $ConductRecordID)
{
	if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-DeleteAll") || (($ldiscipline->isConductPIC($ConductRecordID) || $ldiscipline->isConductOwn($ConductRecordID)) && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-DeleteOwn")))
	{
		$sql = "SELECT Attachment FROM DISCIPLINE_ACCU_RECORD where RecordID=$ConductRecordID";
		$attachmentLoc = $ldiscipline->returnVector($sql);
		//echo $attachmentLoc[0]; exit;
		if($attachmentLoc[0] != "") {
			$path = "$file_path/file/disciplinev12/goodconduct_misconduct/".$attachmentLoc[0];
			$ldiscipline->deleteDirectory($path);	
			//echo $path;
		}
		$this_delete = $ldiscipline->DELETE_CONDUCT_RECORD($ConductRecordID);
		$all_delete = $this_delete ? $all_delete : 0;
	}
	else
	{
		$all_delete = 0;
	}
}

$msg = $all_delete ? "delete":"no_delete_acc_record";
header("Location: index.php?msg=$msg&approved=$approved&rejected=$rejected&waived=$waived&SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&RecordType=$RecordType&s=$s&pageNo=$pageNo&order=$order&field=$field&page_size=$numPerPage&RecordType=$RecordType&s=$s&pic=$pic");

intranet_closedb();
?>