<?php
// Modifying by: yat

######################################################
#
#	Date:	2014-09-15	YatWoon
#			Fixed: failed to display PIC and GM reason [Case#P67831]
#			
######################################################

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");

intranet_auth();
intranet_opendb();

if(empty($RecordID))	header("Location: index.php");

$ldiscipline = new libdisciplinev12();

# Check access right
if(!((($ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-EditAll") || ($ldiscipline->isConductPIC($RecordID) && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-EditOwn"))) && ($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-New") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-EditAll") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-EditOwn"))) && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-RearrangeStudent")))
{
		$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
		exit();
}


$linterface = new interface_html();

# menu highlight setting
$CurrentPage = "Management_GoodConductMisconduct";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eDiscipline['Good_Conduct_and_Misconduct']);

# navigation bar
if ($DetentionAction == "Add") {
	$PAGE_NAVIGATION[] = array($eDiscipline["AddDetention"], "");
} else if ($DetentionAction == "Edit") {
	$PAGE_NAVIGATION[] = array($eDiscipline["EditDetention"], "");
}

# retrieve data
$datainfo = $ldiscipline->RETRIEVE_CONDUCT_INFO($RecordID);
$data = $datainfo[0];

$student = array($data['StudentID']);

# build html part

		if ($_POST["submit_flag"] == "YES") {

			// Get submitted data
			$submitStudent = $student;
			$submitDetentionCount0 = $_POST["DetentionCount0"];
			
			for ($k=1; $k<=$submitDetentionCount0; $k++) {
				${"submitSelectSession0_".$k} = $_POST["SelectSession0_".$k];
			}
			$submitTextRemark0 = $_POST["TextRemark0"];
			for ($l=1; $l<=sizeof($submitStudent); $l++) {
				${"submitDetentionCount".$l} = $_POST["DetentionCount".$l];
				for ($m=1; $m<=${"submitDetentionCount".$l}; $m++) {
					${"submitSelectSession".$l."_".$m} = $_POST["SelectSession".$l."_".$m];
					if (${"submitSelectSession".$l."_".$m} <> "AUTO" && ${"submitSelectSession".$l."_".$m} <> "LATER") {
						$selectedSessionForOneStudent[$l][] = ${"submitSelectSession".$l."_".$m};
						$allSelectedSession[] = ${"submitSelectSession".$l."_".$m};
					}
				}
				${"submitTextRemark".$l} = $_POST["TextRemark".$l];
			}


			// Check past session and available session
			for ($l=1; $l<=sizeof($submitStudent); $l++) {
				$assignedSessionForEachStudent = array();
				for ($m=1; $m<=${"submitDetentionCount".$l}; $m++) {
					if (${"submitSelectSession".$l."_".$m} == "AUTO") {
						$result = $ldiscipline->getDetentionList("FUTURE", "AVAILABLE", "THIS_FORM", $submitStudent[$l-1], "ASC");
						$TmpDetentionID = "";
						if (is_array($allSelectedSession)) {
							$TmpSessionArrayCount = array_count_values($allSelectedSession);
						}
						$AutoSuccess = false;
						
						$currentTime = date('Y-m-d H;i:s');
						
						for ($i=0; $i<sizeof($result); $i++) {
							list($TmpDetentionID,$TmpDetentionDate,$TmpDetentionDayName,$TmpStartTime,$TmpEndTime,$TmpPeriod,$TmpLocation,$TmpForm,$TmpPIC,$TmpVacancy,$TmpAssigned,$TmpAvailable) = $result[$i];
							
							$detentionSession = $TmpDetentionDate." ".$TmpStartTime;
							
							if($currentTime<$detentionSession) {
								if (!(is_array($selectedSessionForOneStudent[$l]) && in_array($TmpDetentionID, $selectedSessionForOneStudent[$l])) && ($TmpSessionArrayCount[$TmpDetentionID] < $TmpAvailable) && !($ldiscipline->checkStudentInSession($submitStudent[$l-1], $TmpDetentionID))) {
									$AutoSuccess = true;
									break;
								}
							}
						}
						if ($TmpDetentionID == "" || $AutoSuccess == false) {
							$AvailableFlag[$l][$m] = "NO";
						} else {
							$AvailableFlag[$l][$m] = "YES";
							$selectedSessionForOneStudent[$l][] = $TmpDetentionID;
							$allSelectedSession[] = $TmpDetentionID;
							$AutoDetentionID[$l][$m] = $TmpDetentionID;
						}
					} else if (${"submitSelectSession".$l."_".$m} != "LATER") {
						$StudentInSessionFlag[$l][$m] = ($ldiscipline->checkStudentInSession($submitStudent[$l-1], ${"submitSelectSession".$l."_".$m}, $ExistingDetentionIDList))?"YES":"NO";
						$PastFlag[$l][$m] = ($ldiscipline->checkPastByDetentionID(${"submitSelectSession".$l."_".$m}))?"YES":"NO";
						if (is_array($assignedSession)) {		// array for sessions that are not assigned by AUTO
							$TmpAssignedSessionArrayCount = array_count_values($assignedSession);
						}
						if (in_array(${"submitSelectSession".$l."_".$m}, $assignedSessionForEachStudent)) {
							$StudentInSessionFlag[$l][$m] = "YES";
						}
						$FullFlag[$l][$m] = ($ldiscipline->checkFullByDetentionID(${"submitSelectSession".$l."_".$m}, $TmpAssignedSessionArrayCount[${"submitSelectSession".$l."_".$m}] + 1))?"YES":"NO";
						if ($StudentInSessionFlag[$l][$m] == "NO" && $PastFlag[$l][$m] == "NO" && $FullFlag[$l][$m] == "NO") {
							$assignedSession[] = ${"submitSelectSession".$l."_".$m};
							$assignedSessionForEachStudent[] = ${"submitSelectSession".$l."_".$m};
						}
					}
				}
			}

			$FailFlag = false;

			for ($l=1; $l<=sizeof($submitStudent); $l++) {
				for ($m=1; $m<=${"submitDetentionCount".$l}; $m++) {
				$allAvailableFlag[] = $AvailableFlag[$l][$m];
				$allStudentInSessionFlag[] = $StudentInSessionFlag[$l][$m];
				$allPastFlag[] = $PastFlag[$l][$m];
				$allFullFlag[] = $FullFlag[$l][$m];
				}
			}
			if (!((is_array($allAvailableFlag) && in_array("NO", $allAvailableFlag)) || (is_array($allStudentInSessionFlag) && in_array("YES", $allStudentInSessionFlag)) || (is_array($allPastFlag) && in_array("YES", $allPastFlag)) || (is_array($allFullFlag) && in_array("YES", $allFullFlag)))) {
				$allResult = true;
				$ldiscipline->deleteDetentionByGMID($RecordID);
				for ($l=1; $l<=sizeof($submitStudent); $l++) {
					for ($m=1; $m<=${"submitDetentionCount".$l}; $m++) {
						$DetentionArr['StudentID'] = "'".$submitStudent[$l-1]."'";
						
						if (${"submitSelectSession".$l."_".$m}=="AUTO") {
							$DetentionArr['DetentionID'] = "'".$AutoDetentionID[$l][$m]."'";
						} else if (${"submitSelectSession".$l."_".$m}=="LATER") {
							$DetentionArr['DetentionID'] = "NULL";
						} else {
							$DetentionArr['DetentionID'] = "'".${"submitSelectSession".$l."_".$m}."'";
						}
						//$DetentionArr['DetentionID'] = "'".((${"submitSelectSession".$l."_".$m}=="AUTO")?$AutoDetentionID[$l][$m]:${"submitSelectSession".$l."_".$m})."'";
						
						$DetentionArr['GMID'] = "'".$RecordID."'";
						$DetentionArr['Remark'] = "'".${"submitTextRemark".$l}."'";
						
						$thisReason = intranet_undo_htmlspecialchars(stripslashes($DetentionReason));
						$thisReason = str_replace("'", "\'", $thisReason);
						$DetentionArr['Reason'] = "'".$thisReason."'";
						$DetentionArr['RequestedBy'] = "'".$UserID."'";
						$DetentionArr['ArrangedBy'] = "'".$UserID."'";
						$DetentionArr['ArrangedDate'] = 'NOW()';
						$DetentionArr['PICID'] = "'".$UserID."'";
						
						$result = $ldiscipline->insertDetention($DetentionArr);
						
						$allResult = $allResult && $result;
					}
				}
				if ($allResult) {
					$SysMsg = "update";
				} else {
					$SysMsg = "update_failed";
				}
				$RedirectPage = "edit.php?RecordID=$RecordID&msg=$SysMsg";
				intranet_closedb();
				header("Location: $RedirectPage");

			} else {
				$FailFlag = true;
			}
		}		// end of submit_flag = "YES"

		function getSelectSessionString($parIndex="", $parSessionNumber="", $parUserID="", $parDetentionID="") {
			global $ldiscipline, $i_Discipline_Auto_Assign_Session, $i_Discipline_All_Forms, $i_Discipline_Put_Into_Unassign_List;
			
			$TmpAllForms = $ldiscipline->getAllFormString();
			if ($parUserID == "") {
				$result = $ldiscipline->getDetentionList("FUTURE", "AVAILABLE", "ALL_FORMS", "", "ASC");
			} else {
				$result = $ldiscipline->getDetentionList("FUTURE", "AVAILABLE", "THIS_FORM", $parUserID, "ASC");
			}
			$SelectSession = "<table><tr><td>";
			$SelectSession .= "<select name=\"SelectSession".$parIndex."_".$parSessionNumber."\" id=\"SelectSession".$parIndex."_".$parSessionNumber."\" class=\"formtextbox\">";
			$SelectSession .= "<option class=\"row_avaliable\" value=\"AUTO\">".$i_Discipline_Auto_Assign_Session."</option>";
			
			if ($parDetentionID=="LATER")
			{
				$selectedLater = "selected";
			}
			else
			{
				$selectedLater = "";
			}
			
			$SelectSession .= "<option class=\"row_avaliable\" value=\"LATER\" ".$selectedLater.">".$i_Discipline_Put_Into_Unassign_List."</option>";
			$SelectSession .= "<optgroup label=\"Date | Time | Location | Form | PIC | Vacancy\">";

			for ($i=0; $i<sizeof($result); $i++) {
				list($TmpDetentionID[$i],$TmpDetentionDate[$i],$TmpDetentionDayName[$i],$TmpStartTime[$i],$TmpEndTime[$i],$TmpPeriod[$i],$TmpLocation[$i],$TmpForm[$i],$TmpPIC[$i],$TmpVacancy[$i],$TmpAssigned[$i],$TmpAvailable[$i]) = $result[$i];

				if ($parDetentionID == $TmpDetentionID[$i])
				{
					$selected = "selected";
				}
				else
				{
					$selected = "";
				}
				
				if ($TmpForm[$i] == $TmpAllForms) $TmpForm[$i] = $i_Discipline_All_Forms;
				$SelectSession .= "<option class=\"row_avaliable\" value=\"".$TmpDetentionID[$i]."\" $selected >".substr($TmpDetentionDate[$i],2)."(".substr($TmpDetentionDayName[$i],0,3).") | ".$TmpPeriod[$i]." | ".$TmpLocation[$i]." | ".$TmpForm[$i]." | ".$TmpPIC[$i]." | ".$TmpAvailable[$i]."</option>";
			}

			$SelectSession .= "</optgroup>";
			$SelectSession .= "</select>";
			$SelectSession .= "</td><td>";
			$SelectSession .= "<span id=\"SpanSysMsg".$parIndex."_".$parSessionNumber."\"></span>";
			$SelectSession .= "</td></tr></table>";

			return $SelectSession;
		}
		$DetentionInfoArr = $ldiscipline->getDetentionListByGMID($RecordID);
		if (count($DetentionInfoArr) == 0)
			$detentionCount = 1;
		else
			$detentionCount = count($DetentionInfoArr);
			
		$session_times = $ldiscipline->DetentinoSession_MAX ? $ldiscipline->DetentinoSession_MAX : 10;
		for ($i = 1; $i <= $session_times; $i++) 
		{
			if ($i == $detentionCount)
			{
				$selected = "selected";
			}
			else
			{
				$selected = "";
			}
			$OptionForDetentionCount .= "<option value=\"$i\" ".$selected.">$i</option>";
		}
		
		//debug_r($DetentionInfoArr);

		// Loop for individual student
		$TableIndStudent = "";
		$ExistingDetentionIDArr = array();
		for ($i=0; $i<sizeof($student); $i++) {
			$resultStudentName = $ldiscipline->getStudentNameByID($student[$i]);
			list($TmpUserID, $TmpName, $TmpClassName, $TmpClassNumber) = $resultStudentName[0];
			$TableIndStudent .= "<br />";
			$TableIndStudent .= "<table align=\"center\" width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
			$TableIndStudent .= "<tr>";
			$TableIndStudent .= "<td colspan=\"2\" valign=\"top\" nowrap=\"nowrap\">".($i+1).".";
			$TableIndStudent .= "<img src=\"".$image_path."/".$LAYOUT_SKIN."/ediscipline/icon_stu_ind.gif\" width=\"20\" height=\"20\" align=\"absmiddle\">";
			$TableIndStudent .= "<span class=\"sectiontitle\">".$TmpClassName."-".$TmpClassNumber." ".$TmpName."</span>";
			$TableIndStudent .= "</td>";
			$TableIndStudent .= "</tr>";
			$TableIndStudent .= "<tr valign=\"top\" class=\"tablerow1\">";
			$TableIndStudent .= "<td nowrap=\"nowrap\" class=\"formfieldtitle\">";
			$TableIndStudent .= "<span class=\"tabletext\">".$i_Discipline_Detention_Times." <span class=\"tabletextrequire\">*</span></span>";
			$TableIndStudent .= "</td>";
			$TableIndStudent .= "<td width=\"80%\">";
			$TableIndStudent .= "<input type=\"hidden\" id=\"HiddenDetentionCount".($i+1)."\" name=\"HiddenDetentionCount".($i+1)."\" value=\"".$detentionCount."\" />";
			$TableIndStudent .= "<select name=\"DetentionCount".($i+1)."\" id=\"DetentionCount".($i+1)."\" onChange=\"javascript:updateSessionCell(".($i+1).");\">".$OptionForDetentionCount."</select>";
			$TableIndStudent .= "</td>";
			$TableIndStudent .= "</tr>";
			
			# Session Selection
			$TableIndStudent .= "<tr class=\"tablerow1\">";
			$TableIndStudent .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\">".$i_Discipline_Session." <span class=\"tabletextrequire\">*</span></td>";
			$TableIndStudent .= "<td valign=\"top\">";
			
			$TableIndStudent .= "<table name=\"TableSession".($i+1)."\" id=\"TableSession".($i+1)."\" width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
			if (count($DetentionInfoArr) == 0)
			{
				# Build selection if 
				$TableIndStudent .= "<tr><td width=\"15\" nowrap>1.</td><td nowrap>".getSelectSessionString($i+1, $i+1, $TmpUserID, "LATER")."</td></tr>";
				
				$thisResaon = $ldiscipline->RETURN_CONDUCT_REASON($data['ItemID'], $data['CategoryID']);
			}
			else
			{
				for ($j=0; $j<count($DetentionInfoArr); $j++)
				{
					$thisDetentionID = $DetentionInfoArr[$j][0];
					if ($thisDetentionID=="") 
						$thisTempDetentionID = "LATER";
					else
						$thisTempDetentionID = $thisDetentionID;
					$TableIndStudent .= "<tr><td width=\"15\" nowrap>".($i+$j+1)."</td><td nowrap>".getSelectSessionString($i+1, $j+1, $TmpUserID, $thisTempDetentionID)."</td></tr>";
					
					$ExistingDetentionIDArr[] = $thisDetentionID;
				}
				$thisResaon = intranet_htmlspecialchars($DetentionInfoArr[0][13]);
			}
			$TableIndStudent .= "</table>";
				
			$TableIndStudent .= "</td>";
			$TableIndStudent .= "</tr>";
			$TableIndStudent .= "<tr class=\"tablerow1\">";
			$TableIndStudent .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\">".$i_Discipline_Remark."</td>";
			$TableIndStudent .= "<td valign=\"top\">".$linterface->GET_TEXTAREA("TextRemark".($i+1), $DetentionInfoArr[0][12])."</td>";
			$TableIndStudent .= "</tr>";
			if ($i != sizeof($student)-1) {
				$TableIndStudent .= "<tr>";
				$TableIndStudent .= "<td height=\"1\" colspan=\"2\" class=\"dotline\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"1\"></td>";
				$TableIndStudent .= "</tr>";
			}
			$TableIndStudent .= "</table>";
			
			
			$TableIndStudent .= "<input type=\"hidden\" id=\"DetentionReason\" name=\"DetentionReason\" value=\"".$thisResaon."\" />";
		}
		$ExistingDetentionIDList = implode(", ", $ExistingDetentionIDArr);

		// Preset detention session (for javascript)
		$jSessionString = "var SelectSessionArr=new Array(".(sizeof($student)+1).");\n";
		for ($k=0; $k<=sizeof($student); $k++) {
			$jSessionString .= "SelectSessionArr[".$k."]=new Array(10);\n";
		}
		for ($i=0; $i<=sizeof($student); $i++) {
			for ($j=0; $j<10; $j++) {
				$jSessionString .= "SelectSessionArr[".$i."][".$j."]='".str_replace("'", "&#039;", getSelectSessionString($i, $j+1, ($i==0)?"":$student[$i-1], "LATER"))."';\n";
			}
		}

		// Initial form after submit fails (for javascript)
		if ($FailFlag) {
			$SessionNAMsg = str_replace("\n", "", str_replace("'", "\"", $linterface->GET_SYS_MSG("session_not_available")));
			$SessionPastMsg = str_replace("\n", "", str_replace("'", "\"", $linterface->GET_SYS_MSG("session_past")));
			$SessionFullMsg = str_replace("\n", "", str_replace("'", "\"", $linterface->GET_SYS_MSG("session_full")));
			$SessionAssignedBeforeMsg = str_replace("\n", "", str_replace("'", "\"", $linterface->GET_SYS_MSG("session_assigned_before")));

			$jInitialForm = "";
			for ($l=1; $l<=sizeof($submitStudent); $l++) {
				$jInitialForm .= "document.getElementById('DetentionCount".$l."').value='".${"submitDetentionCount".$l}."';\n";
				$jsDetentionCount = (count($DetentionInfoArr)==0)? 1 : count($DetentionInfoArr);
				$jInitialForm .= "document.getElementById('HiddenDetentionCount".$l."').value='".$jsDetentionCount."';\n";
				$jInitialForm .= "updateSessionCell(".$l.");\n";
				for ($m=1; $m<=${"submitDetentionCount".$l}; $m++) {
					$jInitialForm .= "document.getElementById('SelectSession".$l."_".$m."').value='".${"submitSelectSession".$l."_".$m}."';\n";
					if ($AvailableFlag[$l][$m] == "NO") {
						$jInitialForm .= "document.getElementById('SpanSysMsg".$l."_".$m."').innerHTML='".$SessionNAMsg."';\n";
					} else if ($FullFlag[$l][$m] == "YES") {
						$jInitialForm .= "document.getElementById('SpanSysMsg".$l."_".$m."').innerHTML='".$SessionFullMsg."';\n";
					} else if ($PastFlag[$l][$m] == "YES") {
						$jInitialForm .= "document.getElementById('SpanSysMsg".$l."_".$m."').innerHTML='".$SessionPastMsg."';\n";
					} else if ($StudentInSessionFlag[$l][$m] == "YES") {
						$jInitialForm .= "document.getElementById('SpanSysMsg".$l."_".$m."').innerHTML='".$SessionAssignedBeforeMsg."';\n";
					}
				}
				$jInitialForm .= "document.getElementById('TextRemark".$l."').value='".${"submitTextRemark".$l}."';\n";
			}
		}

# Start layout
$linterface->LAYOUT_START();

?>

<!--<script type="text/javascript" src = "<?=$PATH_WRT_ROOT?>templates/2007a/js/jslb_ajax.js" charset = "utf-8"></script>-->

<script language="Javascript">
<!--
<?=$jSessionString?>

function updateSessionCell(studentIndex){
	if(studentIndex==undefined) studentIndex='0';

	var oldCount=parseInt(document.getElementById('HiddenDetentionCount'+studentIndex).value);
	var newCount=parseInt(document.getElementById('DetentionCount'+studentIndex).value);
	
	if(oldCount<newCount){
		var newRow, newCell;
		for(var i=oldCount;i<newCount;i++){
			newRow=document.getElementById('TableSession'+studentIndex).insertRow(document.getElementById('TableSession'+studentIndex).rows.length);
			newCell0=newRow.insertCell(0);
			newCell1=newRow.insertCell(1);
			newCell0.innerHTML=(i+1)+'.';
			newCell1.setAttribute("noWrap", true);
			newCell1.innerHTML=SelectSessionArr[studentIndex][i];
		}
		document.getElementById('HiddenDetentionCount'+studentIndex).value = newCount;
	} else if(oldCount>newCount){
		for(var i=oldCount;i>newCount;i--){
			document.getElementById('TableSession'+studentIndex).deleteRow(i-1);
		}
		document.getElementById('HiddenDetentionCount'+studentIndex).value = newCount;
	}
}

function applyToAll(studentCount){
	for(var i=1;i<=studentCount;i++){
		document.getElementById('DetentionCount'+i).value = document.getElementById('DetentionCount0').value;
		updateSessionCell(i);
		for(var j=1;j<=document.getElementById('DetentionCount0').value;j++){
			document.getElementById('SelectSession'+i+'_'+j).value = document.getElementById('SelectSession0_'+j).value;
			if (document.getElementById('SelectSession'+i+'_'+j).value != document.getElementById('SelectSession0_'+j).value){
				document.getElementById('SelectSession'+i+'_'+j).value = 'AUTO';
			}
		}
		document.getElementById('TextRemark'+i).value = document.getElementById('TextRemark0').value;
	}
}

function checkForm()
{
	document.getElementById('submit_flag').value='YES';
	return true;
}

function jsBack()
{
	document.form1.id.value = <?=$RecordID?>;
	document.form1.action="edit.php";
	document.form1.submit();
}

//-->
</script>

<form name="form1" method="POST" action="edit_detention.php">

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td class="navigation">
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
		</table>
	</td>
</tr>

<tr>
	<td>
		<br />
		<? if(sizeof($student)>1) {?>
		<table align="center" width="95%" border="0" cellpadding="5" cellspacing="0" class="tablerow2">
			<tr>
				<td colspan="2" valign="top" nowrap="nowrap">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>
								<img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_section.gif" width="20" height="20" align="absmiddle">
								<span class="sectiontitle"><?=$eDiscipline["GlobalSettingsToAllStudent"]?></span>
							</td>
							<td align="right">
								<span class="sectiontitle">
<?=$linterface->GET_BTN($i_Discipline_Apply_To_All, "button", "javascript:applyToAll(".sizeof($student).");")?>
								</span>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr valign="top">
				<td nowrap="nowrap" class="formfieldtitle">
					<span class="tabletext"><?=$i_Discipline_Detention_Times?> <span class="tabletextrequire">*</span></span>
				</td>
				<td width="80%">
					<input type="hidden" id="HiddenDetentionCount0" name="HiddenDetentionCount0" value="<?=$jsDetentionCount?>" />
					<select name="DetentionCount0" id="DetentionCount0" onChange="javascript:updateSessionCell();"><?=$OptionForDetentionCount?></select>
				</td>
			</tr>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_Session?> <span class="tabletextrequire">*</span></td>
				<td valign="top"><table name="TableSession0" id="TableSession0" width="100%" border="0" cellpadding="5" cellspacing="0"><tr><td width="15" nowrap>1.</td><td nowrap><?=getSelectSessionString(0, 1, "")?></td></tr></table></td>
			</tr>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_Remark?></td>
				<td valign="top"><?=$linterface->GET_TEXTAREA("TextRemark0", "")?></td>
			</tr>
		</table>
		<? } ?>
<?=$TableIndStudent?>
		<table align="center" width="95%" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td valign="top" nowrap="nowrap">
					<span class="tabletextremark"><?=$i_general_required_field?></span>
				</td>
				<td width="80%">&nbsp;</td>
			</tr>
		</table>
	</td>
</tr>

<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_continue, "submit", "return checkForm();")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "jsBack()")?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>

<br />

<input type="hidden" name="RecordID" id="RecordID" value="<?=$RecordID?>" />
<input type="hidden" name="submit_flag" id="submit_flag" value="NO" />
<!--<input type="hidden" name="student" value="<?=implode(",",$student)?>" />-->
<input type="hidden" id="id" name="id" />
<input type="hidden" id="ExistingDetentionIDList" name="ExistingDetentionIDList" value="<?=$ExistingDetentionIDList?>" />



</form>

<?
print $linterface->FOCUS_ON_LOAD("form1.DetentionCount0");

$linterface->LAYOUT_STOP();
intranet_closedb();

?>
<?
if ($FailFlag) {
?>
<script language="javascript">
<?=$jInitialForm?>
</script>
<?
}
?>
