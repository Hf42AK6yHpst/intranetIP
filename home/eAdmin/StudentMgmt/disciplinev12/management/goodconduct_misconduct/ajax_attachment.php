<?php
// modify by : 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lu = new libfilesystem();
/*
if(!($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Goodconduct_Misconduct-EditAll") || (($ldiscipline->isCasePIC($RecordID) || $ldiscipline->isCaseOwn($RecordID)) && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditOwn"))))
{
		$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}
*/

if($task=="remove") {
	$name = substr($fieldname,0,-2);
	$Attachment = ${$name};
	//echo $Attachment;
	//debug_pr($_POST);
	//exit;
	if(sizeof($Attachment)>0)
		$Attachment = array_unique($Attachment);
		
	if($fieldname=="Attachment[]") {
		//echo $FolderLocation.'<br>';
		$FolderLocation = substr($FolderLocation, 0, -1);	
		//echo $FolderLocation.'<br>';
	}
	//debug_pr($_POST);
	//echo $FolderLocation;
	
	$path = "$file_path/file/disciplinev12/goodconduct_misconduct/".$FolderLocation."/";
	for($a=0;$a<sizeof($Attachment);$a++)
	{
		$file = $path.$Attachment[$a];
		$file = stripslashes($file);
		$lu->lfs_remove($file);
	}
	$attachment_list = $ldiscipline->getAttachmentArray($path, '');
	//debug_pr($attachment_list);
	if(sizeof($attachment_list)==0) {
		$ldiscipline->deleteDirectory($path);	
	}

}

else if($task=="copyAll") {
	//$Attachment = ${$fidlename};
	if(sizeof($Attachment)>0)
		$Attachment = array_unique($Attachment);
	
	//debug_pr($Attachment);
	
	foreach($student as $StudentID) {
		
		$path = "$file_path/file/disciplinev12/goodconduct_misconduct/".$sessionTime."_".$StudentID."tmp";
		
		$ldiscipline->deleteDirectory($path);
		
		
		if (sizeof($Attachment)>0 && !is_dir($path))
		{
		    //$path = $path."tmp";
			$lu->folder_new($path);
			chmod($path, 0777);
		}

		for($a=0;$a<sizeof($Attachment);$a++) {
			$file = "$file_path/file/disciplinev12/goodconduct_misconduct/".$sessionTime."tmp/".$Attachment[$a];
			$newfile = "$file_path/file/disciplinev12/goodconduct_misconduct/".$sessionTime."_".$StudentID."tmp/".$Attachment[$a];
			
			if (!copy($file, $newfile)) {
				//echo "failed to copy $file...\n";
			} else {
				//echo "Copied!";	
			}
		}
	}	
}


intranet_closedb();
?>


