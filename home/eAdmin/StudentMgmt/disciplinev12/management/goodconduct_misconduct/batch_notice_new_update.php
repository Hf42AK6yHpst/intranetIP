<?php
// Modifying by: 

########## Change Log ###############
#
#	Date	:	2020-11-13 (Bill)	[2020-1009-1753-46096]
# 				delay notice start date & end date		($sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'])
#
#	Date	:	2017-10-11	(Bill)	[2017-1011-1143-48235]
#				Suspend Notice if not approved conduct
#
#	Date	:	2017-03-02	(Bill)	[2016-0823-1255-04240]
#				default not distribute notice to parent	($sys_custom['eDiscipline']['NotReleaseNotice'])
#
#	Date	:	2015-05-04 (Bill)
#				Create file [2014-1216-1347-28164]
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libucc.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$lnotice = new libnotice();

$lc = new libucc();

for($i=0; $i<$record_number; $i++)
{
	# Data from $_POST
	$record_id = $_POST["RecordID".($i+1)];
	$student_id = $_POST["StudentID".($i+1)];
	$cateogory_id = $_POST["CategoryID".($i+1)];
	$notice_id = $_POST["SelectNotice".($i+1)];
	$additional_info = $_POST["TextAdditionalInfo".($i+1)];
	$emailNotify = $_POST["emailNotify".$i];
	$with_template = $_POST["withNoticeTemplate".($i+1)];
	
	# Skip if eNotice set template before
	if($with_template == 1)
		continue;
	
	$template_info = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO($notice_id);
	if(sizeof($template_info) && isset($notice_id) && $notice_id!="")
	{
		$SpecialData = array();
		$SpecialData['ConductRecordID'] = $record_id;
		if($sys_custom['eDisciplinev12_eNoticeTemplate_AccuNumber'])
			$SpecialData['AccuNumber'] = $ldiscipline->RETURN_ACCUMULATED_CONDUCT_NUMBER_FOR_ENOTICE_TEMPLATE($student_id, $record_id, $cateogory_id);
	
		$includeReferenceNo = 1;
		$template_data = $ldiscipline->TEMPLATE_VARIABLE_CONVERSION($cateogory_id, $student_id, $SpecialData, $additional_info, $includeReferenceNo);
		$UserName = $ldiscipline->getUserNameByID($student_id);

		$Module = $ldiscipline->Module;
		$NoticeNumber = time();
		$TemplateID = $notice_id;
		$Variable = $template_data;
		$IssueUserID = $UserID;
		$IssueUserName = $UserName[0];
		$TargetRecipientType = "U";
		$RecipientID = array($student_id);
		$RecordType = NOTICE_TO_SOME_STUDENTS;
		
		$NoticeID = -1;
		if (!$lnotice->disabled)
		{
			$ReturnArr = $lc->setNoticeParameter($Module, $NoticeNumber, $TemplateID, $Variable, $IssueUserID, $IssueUserName, $TargetRecipientType, $RecipientID, $RecordType);

            // [2020-1009-1753-46096] delay notice start date & end date
            if (isset($sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay']) && $sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'] > 0)
            {
                $delayedHrs = $sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'];

                $DateStart = date('Y-m-d H:i:s', strtotime("+".$delayedHrs." hours"));
                $lc->setDateStart($DateStart);

                if ($lc->defaultDisNumDays > 0) {
                    $DateEndTs = time() + ($lc->defaultDisNumDays * 24 * 60 * 60) + ($delayedHrs * 60 * 60);
                    $DateEnd = date('Y-m-d', $DateEndTs).' 23:59:59';
                    $lc->setDateEnd($DateEnd);
                }
            }

            if(!isset($emailNotify) || $emailNotify=="") $emailNotify = 0;
			$NoticeID = $lc->sendNotice($emailNotify);
		}
		
		if($NoticeID != -1)
		{
			# Update Conduct Record
			$sql = "UPDATE DISCIPLINE_ACCU_RECORD SET NoticeID=$NoticeID WHERE RecordID=$record_id";
			$ldiscipline->db_db_query($sql);
			
			// [2016-0823-1255-04240]
			if($sys_custom['eDiscipline']['NotReleaseNotice'])
			{
				$lnotice->changeNoticeStatus($NoticeID, SUSPENDED_NOTICE_RECORD);
			}
			// [2017-1011-1143-48235] Suspend Notice if not approved conduct
			else
			{
				$RecordInfo = $ldiscipline->RETRIEVE_CONDUCT_INFO($record_id);
				$RecordStatus = $RecordInfo[0]["RecordStatus"];
				if($RecordStatus != DISCIPLINE_STATUS_APPROVED) {
					$lnotice->changeNoticeStatus($NoticeID, SUSPENDED_NOTICE_RECORD);
				}
			}
		}
	}
}

intranet_closedb();

$url = "index.php?field=$field&order=$order&pageNo=$pageNo&numPerPage=$numPerPage&num_per_page=$num_per_page&page_size_change=$page_size_change&SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&pic=$pic&s=$s&RecordType=$MeritType2&approved=$approved&waived=$waived&rejected=$rejected&clickID=$clickID&fromPPC=$fromPPC&passedActionDueDate=$passedActionDueDate";
header("Location: $url");
?>