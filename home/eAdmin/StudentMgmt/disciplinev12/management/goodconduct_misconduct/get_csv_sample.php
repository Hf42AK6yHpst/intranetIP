<?php
// Editing by 
// Please use UTF-8 萬國碼 
/*
 * 2014-05-29 : Created to dynamically generating the sample csv. 
 *				*** good_conduct_unicode.csv, good_conduct_with_times_unicode.csv are not used anymore. *** 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$lexport = new libexporttext();

$isKIS = $_SESSION["platform"]=="KIS";

### download csv / page title
$other_fields = "";
if($sys_custom['eDiscipline_GM_Times'])
{
	$other_fields .= "_with_times";
}
if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
	$other_fields .= "_with_score";
}
$other_fields .= "_unicode";

$rows = array();

$sample_file = "good_conduct$other_fields.csv"; 


$exportColumn = array("Class Name","Class Number","Event Date","Item Code");
if($sys_custom['eDiscipline_GM_Times']){
 	$exportColumn = array_merge($exportColumn,array("Times"));
}
if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
	$exportColumn = array_merge($exportColumn,array("Score"));
}
$exportColumn = array_merge($exportColumn,array("PIC","Remark"));

$rows = array();
$rows[0] = array("1A","12","2009-03-20","ITEM113");
$rows[1] = array("2A","2","2009-03-20","ITEM113");
if($sys_custom['eDiscipline_GM_Times']){
	$rows[0] = array_merge($rows[0],array("0.5"));
	$rows[1] = array_merge($rows[1],array("1"));
}
if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
	$rows[0] = array_merge($rows[0],array("1"));
	$rows[1] = array_merge($rows[1],array("2"));
}
$rows[0] = array_merge($rows[0],array("Teacher01","Abcdefg"));
$rows[1] = array_merge($rows[1],array("Teacher01",""));

$export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn);	
$filename = $sample_file;
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>