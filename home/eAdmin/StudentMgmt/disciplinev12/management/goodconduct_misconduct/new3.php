<?php
// Modifying by: 

########## Change Log ###############
#
#   Date    :   2019-05-01  Bill
#               prevent Cross-site Scripting
#
#	Date	:	2017-11-21	(Bill)	[2017-1101-1454-59170]
#				modify coding that generate detention session list to improve performance
#				apply temp array to reduce query number
#
#	Date	:	2017-03-02	(Bill)	[2016-0823-1255-04240]
#				default uncheck checkbox "Send email to notify parents"	($sys_custom['eDiscipline']['DefaultUncheckSendEmailCheckbox'])
#
#	Date	:	2016-12-05	(Bill)	[2016-1129-1154-50236]
#				fixed: checkbox of Send Notice moved when selected Detention checkbox, set inline style to remove float setting
#
#	Date	:	2016-04-21 (Bill)	[2016-0405-1012-27236]
#				modified changeCat(), fix js error result in cannot submit AP records
#
#	Date	:	2015-04-30 (Bill)	[2014-1216-1347-28164]
#				add option to send push message to notify parent
#				fixed: disable finish button after click
#
#	Date	:	2015-04-10 (Bill)	[2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073]
#				auto check "Detention" and/or "Send notice via eNotice" when $DefaultDetention is true and/or $DefaultSendNotice is true
#
#	Date	:	2014-12-23 (Bill)
#				add system message after insert conduct records to DB
#
#	Date	:	2014-09-18 (YatWoon)
#				midding add id for "DetentionCount0" [Case#P67811]
#				deploy: IPv10.1
#
#	Date	:	2014-05-28 (Carlos)
#				$sys_custom['eDiscipline']['PooiToMiddleSchool'] - display score cahnge field
#
#	Date	:	2013-03-08 (YatWoon)
#				add studentGMRecordList() parameter, customization for 2013-0122-1029-01156, display "current term semester total record"
#				($sys_custom['eDiscipline_DisplayCurrentSemesterTotalRecord']) 
#
#	Date	:	2010-08-26 (Henry Chow)
#	Detail 	:	add Reference number (Munsang cust)
#
#	Date	:	2010-06-10 (Henry)
#	Detail 	:	select default eNotice Template
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."/includes/libnotice.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$student = $_POST['student'];
if(!is_array($student)) {
    $student = explode(",", $student);
}
if(is_array($student) && sizeof($student) > 0) {
    foreach($student as $thisKey => $thisStudentID) {
        $student[$thisKey] = IntegerSafe($thisStudentID);
    }
    ${"GMID_".$thisStudentID} = IntegerSafe(${"GMID_".$thisStudentID});
    
    if(!intranet_validateDate(${"RecordDate_".$thisStudentID})) {
        ${"RecordDate_".$thisStudentID} = date('Y-m-d');
    }
    
    ${"record_type_".$thisStudentID} = IntegerSafe(${"record_type_".$thisStudentID});
    ${"CatID_".$thisStudentID} = IntegerSafe(${"CatID_".$thisStudentID});
    ${"ItemID_".$thisStudentID} = IntegerSafe(${"ItemID_".$thisStudentID});
    ${"semester_".$thisStudentID} = cleanCrossSiteScriptingCode(${"semester_".$thisStudentID});
}

if($detentionFlag != 'YES' && $detentionFlag != 'NO' && $detentionFlag != '') {
    $detentionFlag = '';
}
if($noticeFlag != 'YES' && $noticeFlag != 'NO' && $noticeFlag != '') {
    $noticeFlag = '';
}
### Handle SQL Injection + XSS [END]

$ldiscipline = new libdisciplinev12();
$lnotice = new libnotice();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-New");

$linterface = new interface_html();

// [2014-1216-1347-28164]
$canSendPushMsg = false;
if($plugin['eClassApp'] && $sys_custom['eDiscipline']['SendDetentionPushMessage']){
	$luser = new libuser();
	$canSendPushMsg = true;
}

# Menu highlight setting
$CurrentPage = "Management_GoodConductMisconduct";
$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Tag information
$TAGS_OBJ[] = array($eDiscipline['Good_Conduct_and_Misconduct']);

# Step information
$STEPS_OBJ[] = array($eDiscipline["SelectStudents"], 0);
$STEPS_OBJ[] = array($eDiscipline["AddRecordToStudents"], 0);
$STEPS_OBJ[] = array($eDiscipline["SelectActions"], 1);

# Navigation bar
$PAGE_NAVIGATION[] = array($eDiscipline['RecordList'], "index.php");
$PAGE_NAVIGATION[] = array($eDiscipline["NewRecord"], "");

# Get data from preious page
// $student = $_POST['student'];
if(empty($student))	header("Location: new1.php");

# Build html part
$selected_student_table = "<table width='100%' border='0' cellspacing='0' cellpadding='5'>";
$selected_student_table .= "<tr class='tabletop' width=''>";
$selected_student_table .= "<td class=\"tabletoplink\" width='15%'>{$i_general_class}</td>";
$selected_student_table .= "<td class=\"tabletoplink\" width='10%'>{$i_general_name}</td>";
$selected_student_table .= "<td class=\"tabletoplink\" width='1'>&nbsp;</td>";
$selected_student_table .= "<td class=\"tabletoplink\" width='40%'>".$eDiscipline["Category_Item"]."</td>";
if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
	$selected_student_table .= "<td class=\"tabletoplink\" width='10%'>".$Lang['eDiscipline']['Score']."</td>";
}
if($sys_custom['eDiscipline_Display_Times_Of_Current_GM_When_Add']) {
	$selected_student_table .= "<td class=\"tabletoplink\" width='13%'>".$Lang['eDiscipline']['ThisGmTimes']."</td>";	
}
if($sys_custom['eDiscipline_GM_Times']) {
	$selected_student_table .= "<td class=\"tabletoplink\" width='13%'>".$Lang['eDiscipline']['TotalTimes']."</td>";	
}
else {
	$selected_student_table .= "<td class=\"tabletoplink\" width='13%'>". ($sys_custom['eDiscipline_DisplayCurrentSemesterTotalRecord'] ? $Lang['eDiscipline']['CurrentSemesterTotalRecord']:$Lang['eDiscipline']['CurrentYearTotalRecord']) ."</td>";
}

if($sys_custom['eDisciplinev12_add_referenceNo'])	
	$selected_student_table .= "<td class=\"tabletoplink\" width='13%'>".$iDiscipline['ReferenceNo']."</td>";
if($sys_custom['eDisciplinev12_display_grouped_record'])
	$selected_student_table .= "<td class=\"tabletoplink\" width='13%'>".$Lang['eDiscipline']['RecordGenerated']."</td>";

$selected_student_table .= "</tr>";
// if(!is_array($student))
// {
// 	$student = explode(",",$student);
// }

$gmCatInfo = $ldiscipline->getGMCategoryInfoByCategoryID($CatID);
$TemplateID = $gmCatInfo['TemplateID'];	
$TemplateAry = $ldiscipline->retrieveTemplateDetails($TemplateID);
$defaultTemplateID = $TemplateAry[0]['CategoryID'];
if($defaultTemplateID!="0" && $defaultTemplateID!="")
	$js .= "changeCat('$defaultTemplateID', 0);itemSelected('$TemplateID', 0, '$TemplateID');\n";
	
// [2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073] - get auto select detention and eNotice option value
$gmCatDefaultOption = $ldiscipline->getGMCategoryInfoByCategoryID($CatID);
$DefaultDetention = $gmCatDefaultOption['AutoSelectDetention'];
$DefaultSendNotice = $gmCatDefaultOption['AutoSelecteNotice'];

$i = 1;
foreach($student as $k=>$StudentID)
{
	$thisCatID = ${"CatID_".$StudentID};
	$thisItemID = ${"ItemID_".$StudentID};
	
	$gmCatInfo = $ldiscipline->getGMCategoryInfoByCategoryID($thisCatID);
	$TemplateID = $gmCatInfo['TemplateID'];	
	$TemplateAry = $ldiscipline->retrieveTemplateDetails($TemplateID);
	$TemplateCategoryID[$StudentID] = $TemplateAry[0]['CategoryID'];
	$thisRecordID = ${"GMID_".$StudentID};
	
	// [2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073] - get auto select detention and eNotice option value
	$DefaultDetention = $gmCatInfo['AutoSelectDetention'] || $DefaultDetention;
	$DefaultSendNotice = $gmCatInfo['AutoSelecteNotice'] || $DefaultSendNotice;

	$thisGmRecord = $ldiscipline->RETRIEVE_CONDUCT_INFO($thisRecordID);
	$thisRecordDate = $thisGmRecord[0]['RecordDate'];
	$data = $ldiscipline->studentGMRecordList($StudentID, $thisCatID, $thisItemID, $thisRecordDate);
	
	$css = ($i%2==1) ? 1 : 2;
	
	for($m=0; $m<sizeof($data); $m++) {
		list($class, $name, $img, $cat, $total, $remark) = $data[$m];
		
		$selected_student_table .= "<tr class='tablerow{$css}'>";	
		$selected_student_table .= "<td>$class</td>";
		$selected_student_table .= "<td>$name</td>";
		$selected_student_table .= "<td>$img</td>";
		$selected_student_table .= "<td>$cat</td>";
		if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
			$selected_student_table .= "<td>".(-1*$data[$m]['ScoreChange'])."</td>";
		}
		if($sys_custom['eDiscipline_Display_Times_Of_Current_GM_When_Add']) {
			$selected_student_table .= "<td>".(floor($thisGmRecord[0]['GMCount'])==$thisGmRecord[0]['GMCount'] ? floor($thisGmRecord[0]['GMCount']) : substr($thisGmRecord[0]['GMCount'],0,3))."</td>";
		}
		$selected_student_table .= "<td>".(floor($total)==$total ? floor($total) : number_format($total,1))."</td>";
		
		if($sys_custom['eDisciplinev12_add_referenceNo']) {
			$ref_id = $ldiscipline->RETURN_REFERNCE_NUMBER("DISCIPLINE_ACCU_RECORD", $thisRecordID);
			$selected_student_table .= "<td>$ref_id</td>";
		}
		if($sys_custom['eDisciplinev12_display_grouped_record']) {
			$ap_record = $ldiscipline->RETURN_UPGRADED_AP_RECORD($thisRecordID);
			$selected_student_table .= "<td>$ap_record</td>";
		}
		$selected_student_table .= "</tr>";
	}	
	
	if($TemplateCategoryID[$StudentID]!="0" && $TemplateCategoryID[$StudentID]!="") {
		$js .= "changeCat('{$TemplateCategoryID[$StudentID]}', $i);itemSelected('$TemplateID', $i, '$TemplateID');\n";
	}
	
	$i++;
}
$selected_student_table .= "</table>";

for($a=0;$a<sizeof($student);$a++)
{
	$ID = $student[$a];
	$StudentArr[$ID]['RecordType'] = ${"record_type_".$ID};
	$StudentArr[$ID]['CatID'] = ${"CatID_".$ID};
	$StudentArr[$ID]['ItemID'] = ${"ItemID_".$ID};
	$StudentArr[$ID]['RecordDate'] = ${"RecordDate_".$ID};
	$StudentArr[$ID]['Remark'] = ${"remark_".$ID};
	$StudentArr[$ID]['Remark'] = intranet_htmlspecialchars($StudentArr[$ID]['Remark']);
	
	# School Year
	$school_year = GET_ACADEMIC_YEAR_WITH_FORMAT($StudentArr[$ID]['RecordDate']);
	
	# Semester
	if($semester_mode==2)
		$StudentArr[$ID]['Semester'] = retrieveSemester($StudentArr[$ID]['RecordDate']);
	else
		$StudentArr[$ID]['Semester'] = ${"semester_".$ID};	
}

########## For Detention (Begin) ##########

		if ($_POST["submit_flag"] == "YES")
		{
			if ($detentionFlag == "YES")
			{
				// Get submitted data
				$submitStudent = $student;
				$submitDetentionCount0 = $_POST["DetentionCount0"];
				$submitDetentionCount0 = IntegerSafe($submitDetentionCount0);
				
				for ($k=1; $k<=$submitDetentionCount0; $k++) {
				    ${"submitSelectSession0_".$k} = $_POST["SelectSession0_".$k];
				    if (${"submitSelectSession0_".$k} <> "AUTO" && ${"submitSelectSession0_".$k} <> "LATER") {
				        ${"submitSelectSession0_".$k} = IntegerSafe(${"submitSelectSession0_".$k});
				    }
				}
				$submitTextRemark0 = $_POST["TextRemark0"];
				
				for ($l=1; $l<=sizeof($submitStudent); $l++) {
				    ${"submitDetentionCount".$l} = $_POST["DetentionCount".$l];
				    ${"submitDetentionCount".$l} = IntegerSafe(${"submitDetentionCount".$l});
				    
					for ($m=1; $m<=${"submitDetentionCount".$l}; $m++) {
						${"submitSelectSession".$l."_".$m} = $_POST["SelectSession".$l."_".$m];
						if (${"submitSelectSession".$l."_".$m} <> "AUTO" && ${"submitSelectSession".$l."_".$m} <> "LATER") {
						    ${"submitSelectSession".$l."_".$m} = IntegerSafe(${"submitSelectSession".$l."_".$m});
						    
							$selectedSessionForOneStudent[$l][] = ${"submitSelectSession".$l."_".$m};
							$allSelectedSession[] = ${"submitSelectSession".$l."_".$m};
						}
					}
					${"submitTextRemark".$l} = $_POST["TextRemark".$l];
				}
				
				// Check past session and available session
				for ($l=1; $l<=sizeof($submitStudent); $l++) {
					$assignedSessionForEachStudent = array();
	
					for ($m=1; $m<=${"submitDetentionCount".$l}; $m++) {
						if (${"submitSelectSession".$l."_".$m} == "AUTO") {
							$result = $ldiscipline->getDetentionList("FUTURE", "AVAILABLE", "THIS_FORM", $submitStudent[$l-1], "ASC");
							$TmpDetentionID = "";
							if (is_array($allSelectedSession)) {
								$TmpSessionArrayCount = array_count_values($allSelectedSession);
							}
							$AutoSuccess = false;
							$currentTime = date('Y-m-d H:i:s');
							
							for ($i=0; $i<sizeof($result); $i++) {
								list($TmpDetentionID,$TmpDetentionDate,$TmpDetentionDayName,$TmpStartTime,$TmpEndTime,$TmpPeriod,$TmpLocation,$TmpForm,$TmpPIC,$TmpVacancy,$TmpAssigned,$TmpAvailable) = $result[$i];
								
							$detentionSession = $TmpDetentionDate." ".$TmpStartTime;
								if($currentTime<$detentionSession) {
									if (!(is_array($selectedSessionForOneStudent[$l]) && in_array($TmpDetentionID, $selectedSessionForOneStudent[$l])) && ($TmpSessionArrayCount[$TmpDetentionID] < $TmpAvailable) && !($ldiscipline->checkStudentInSession($submitStudent[$l-1], $TmpDetentionID))) {
										$AutoSuccess = true;
										break;
									}
								}
							}
							
							if ($TmpDetentionID == "" || $AutoSuccess == false) {
								$AvailableFlag[$l][$m] = "NO";
							} else {
								$AvailableFlag[$l][$m] = "YES";
								$selectedSessionForOneStudent[$l][] = $TmpDetentionID;
								$allSelectedSession[] = $TmpDetentionID;
								$AutoDetentionID[$l][$m] = $TmpDetentionID;
							}
						} else if (${"submitSelectSession".$l."_".$m} != "LATER") {
							$StudentInSessionFlag[$l][$m] = ($ldiscipline->checkStudentInSession($submitStudent[$l-1], ${"submitSelectSession".$l."_".$m}))?"YES":"NO";
							$PastFlag[$l][$m] = ($ldiscipline->checkPastByDetentionID(${"submitSelectSession".$l."_".$m}))?"YES":"NO";
							if (is_array($assignedSession)) {		// array for sessions that are not assigned by AUTO
								$TmpAssignedSessionArrayCount = array_count_values($assignedSession);
							}
							if (in_array(${"submitSelectSession".$l."_".$m}, $assignedSessionForEachStudent)) {
								$StudentInSessionFlag[$l][$m] = "YES";
							}
							$FullFlag[$l][$m] = ($ldiscipline->checkFullByDetentionID(${"submitSelectSession".$l."_".$m}, $TmpAssignedSessionArrayCount[${"submitSelectSession".$l."_".$m}] + 1))?"YES":"NO";
							if ($StudentInSessionFlag[$l][$m] == "NO" && $PastFlag[$l][$m] == "NO" && $FullFlag[$l][$m] == "NO") {
								$assignedSession[] = ${"submitSelectSession".$l."_".$m};
								$assignedSessionForEachStudent[] = ${"submitSelectSession".$l."_".$m};
							}
						}
					}
				}

				$FailFlag = false;
	
				for ($l=1; $l<=sizeof($submitStudent); $l++) {
					for ($m=1; $m<=${"submitDetentionCount".$l}; $m++) {
    					$allAvailableFlag[] = $AvailableFlag[$l][$m];
    					$allStudentInSessionFlag[] = $StudentInSessionFlag[$l][$m];
    					$allPastFlag[] = $PastFlag[$l][$m];
    					$allFullFlag[] = $FullFlag[$l][$m];
					}
				}
				if (!((is_array($allAvailableFlag) && in_array("NO", $allAvailableFlag)) || (is_array($allStudentInSessionFlag) && in_array("YES", $allStudentInSessionFlag)) || (is_array($allPastFlag) && in_array("YES", $allPastFlag)) || (is_array($allFullFlag) && in_array("YES", $allFullFlag)))) {
					for ($l=1; $l<=sizeof($submitStudent); $l++) {
						for ($m=1; $m<=${"submitDetentionCount".$l}; $m++) {
							// StudentID
							$DetentionArr[] = $submitStudent[$l-1];
							// DetentionID
							if (${"submitSelectSession".$l."_".$m}=="AUTO") {
								$DetentionArr[] = $AutoDetentionID[$l][$m];
							} else if (${"submitSelectSession".$l."_".$m}=="LATER") {
								$DetentionArr[] = "";
							} else {
								$DetentionArr[] = ${"submitSelectSession".$l."_".$m};
							}
							// Remark
							$DetentionArr[] = ${"submitTextRemark".$l};
						}
					}
				
?>
<body onLoad="document.form2.submit();">
<form name="form2" method="post" action="new3_update.php">

<!-- Step 1 data //-->

<!-- Step 2 data //-->
<? foreach($_POST as $k=>$d) { 
	if($k=='student')
	{
		if(is_array($_POST['student']))
		{
		    $student = IntegerSafe($student);
			for($ct=0; $ct<sizeof($student); $ct++) {
				echo "<input type=\"hidden\" name=\"student[]\" value=\"$student[$ct]\" />\n";
			}
		}
		else
		{
		    $student = IntegerSafe($_POST['student']);
		    echo "<input type=\"hidden\" name=\"student[]\" value=\"".$student."\" />\n";
		}
	}
	else
	{
	    if($d != '') $d = intranet_htmlspecialchars($d);
	    
	    if(substr($k,0,20) == "HiddenDetentionCount" || substr($k,0,14) == "DetentionCount" || substr($k,0,9) == "StudentID" || substr($k,0,10) == "CategoryID" || substr($k,0,12) == "SelectNotice" ||
	        substr($k,0,4) == "GMID" || substr($k,0,11) == "record_type" || substr($k,0,5) == "CatID" || substr($k,0,6) == "ItemID" || $k == "formValid" || $k == "action_detention") {
            $d = IntegerSafe($d);
        }
        else if(substr($k,0,8) == "semester" || $k == "msg" || $k == "layername") {
            $d = cleanCrossSiteScriptingCode($d);
        }
        else if(substr($k,0,10) == "RecordDate") {
            if(!intranet_validateDate($d)) {
                $d = '';
            }
        }
        else if (substr($k,0,13) == "SelectSession") {
            if ($d <> "AUTO" && $d <> "LATER") {
                $d = IntegerSafe($d);
            }
        }
        else if($k == "submit_flag" || $k == "detentionFlag" || $k == "noticeFlag") {
            if($d != 'YES' && $d != 'NO' && $d != '') {
                $d = '';
            }
        }
        
		echo "<input type=\"hidden\" name=\"$k\" value=\"$d\" />";
	}
} ?>

<!-- Step 3 data //-->
<input type="hidden" name="submit_flag" id="submit_flag" value="YES" />

<? foreach($DetentionArr as $k=>$d) { 
	if($d!='') $d = intranet_htmlspecialchars($d);?>
	<input type="hidden" name="DetentionArr[]" value="<?=$d?>" />
<? } ?>
<input type="hidden" name="detentionFlag" id="detentionFlag" value="<?=$detentionFlag?>" />
<input type="hidden" name="noticeFlag" id="noticeFlag" value="<?=$noticeFlag?>" />

</form>
</body>

<?
				}
				else {
					$FailFlag = true;
	
				}

			}
			else {		// $detentionFlag != "YES"
?>
<body onLoad="document.form3.submit();">
<form name="form3" method="post" action="new3_update.php">

<?
foreach($_POST as $k=>$d) {
	if($k=='student')
	{
	    $student = IntegerSafe($student);
		for($ct=0; $ct<sizeof($student); $ct++) {
			echo "<input type=\"hidden\" name=\"student[]\" value=\"$student[$ct]\" />\n";
		}
	}
	else
	{
	    if($d!='') $d = intranet_htmlspecialchars($d);
    	
    	if(substr($k,0,20) == "HiddenDetentionCount" || substr($k,0,14) == "DetentionCount" || substr($k,0,9) == "StudentID" || substr($k,0,10) == "CategoryID" || substr($k,0,12) == "SelectNotice" || 
    	    substr($k,0,4) == "GMID" || substr($k,0,11) == "record_type" || substr($k,0,5) == "CatID" || substr($k,0,6) == "ItemID" || $k == "formValid") {
	        $d = IntegerSafe($d);
	    }
	    else if(substr($k,0,8) == "semester" || $k == "msg" || $k == "layername") {
	        $d = cleanCrossSiteScriptingCode($d);
	    }
	    else if(substr($k,0,10) == "RecordDate") {
	        if(!intranet_validateDate($d)) {
	            $d = '';
	        }
	    }
        else if (substr($k,0,13) == "SelectSession") {
            if ($d <> "AUTO" && $d <> "LATER") {
                $d = IntegerSafe($d);
            }
        }
        else if($k == "submit_flag" || $k == "detentionFlag" || $k == "noticeFlag") {
            if($d != 'YES' && $d != 'NO' && $d != '') {
                $d = '';
            }
        }
    ?>
	<input type="hidden" name="<?=$k?>" value="<?=$d?>" />
<? }
} ?>
<!-- Step 3 data //-->

<input type="hidden" name="submit_flag" id="submit_flag" value="YES" />
<input type="hidden" name="detentionFlag" id="detentionFlag" value="<?=$detentionFlag?>" />
<input type="hidden" name="noticeFlag" id="noticeFlag" value="<?=$noticeFlag?>" />

</form>
</body>
<?
			}
		}		// end of submit_flag = "YES"
		
		$session_times = $ldiscipline->DetentinoSession_MAX ? $ldiscipline->DetentinoSession_MAX : 10;
		for ($i=1; $i<=$session_times; $i++) $OptionForDetentionCount .= "<option value=\"$i\">$i</option>";
		
		// [2017-1101-1454-59170] move out from getSelectSessionString() - to solve the performance issue
		// Preset String
		$label = $ldiscipline->displayDetentionSessionDefaultString();
		$TmpAllForms = $ldiscipline->getAllFormString();
		
		// [2017-1101-1454-59170] modified - to solve the performance issue
//		function getSelectSessionString($parIndex="", $parSessionNumber="", $parUserID="") {
		function getSelectSessionString($parIndex="", $parSessionNumber="", $resultCount="", $row_avaliable_option="")
		{
			global $ldiscipline, $i_Discipline_Auto_Assign_Session, $i_Discipline_Put_Into_Unassign_List, $i_Discipline_All_Forms;
			global $i_Discipline_Date, $i_Discipline_Time, $i_Discipline_Location, $i_Discipline_Form, $i_Discipline_Vacancy, $i_Discipline_PIC, $label, $TmpAllForms;
			
			// [2017-1101-1454-59170] commented - to solve the performance issue
//			$TmpAllForms = $ldiscipline->getAllFormString();
//			if ($parUserID == "") {
//				$result = $ldiscipline->getDetentionList("FUTURE", "AVAILABLE", "ALL_FORMS", "", "ASC");
//			} else {
//				$result = $ldiscipline->getDetentionList("FUTURE", "AVAILABLE", "THIS_FORM", $parUserID, "ASC");
//			}
			
			$SelectSession = "<table><tr><td>";
			$SelectSession .= "<select name=\"SelectSession".$parIndex."_".$parSessionNumber."\" id=\"SelectSession".$parIndex."_".$parSessionNumber."\" class=\"formtextbox\">";
			
			// Hide auto assign option if no available session
			$assignLater = "";
			if ($resultCount > 0) {
				$SelectSession .= "<option class=\"row_avaliable\" value=\"AUTO\" selected>".$i_Discipline_Auto_Assign_Session."</option>";
			}
			else {
				$assignLater = " selected";
			}
			$SelectSession .= "<option class=\"row_avaliable\" value=\"LATER\" $assignLater>".$i_Discipline_Put_Into_Unassign_List."</option>";
			
			// [2017-1101-1454-59170] commented - to solve the performance issue
//			$label = $ldiscipline->displayDetentionSessionDefaultString();
			
			$SelectSession .= "<optgroup label=\"$label\">";
			
			//  [2017-1101-1454-59170] commented - to solve the performance issue
//			for ($i=0; $i<sizeof($result); $i++) {
//				list($TmpDetentionID[$i],$TmpDetentionDate[$i],$TmpDetentionDayName[$i],$TmpStartTime[$i],$TmpEndTime[$i],$TmpPeriod[$i],$TmpLocation[$i],$TmpForm[$i],$TmpPIC[$i],$TmpVacancy[$i],$TmpAssigned[$i],$TmpAvailable[$i]) = $result[$i];
//
//				if ($TmpForm[$i] == $TmpAllForms) $TmpForm[$i] = $i_Discipline_All_Forms;
//				$SessionStr = $ldiscipline->displayDetentionSessionString($TmpDetentionID[$i]);
//				$SelectSession .= "<option class=\"row_avaliable\" value=\"".$TmpDetentionID[$i]."\">".$SessionStr."</option>";
//			}
			$SelectSession .= $row_avaliable_option;
			
			$SelectSession .= "</optgroup>";
			$SelectSession .= "</select>";
			$SelectSession .= "<div id=\"show_result_".$parIndex."_".$parSessionNumber."\" style=\"position:absolute; width:200px; height:20px; z-index:0;\"></div>";
			$SelectSession .= "</td><td>";
			$SelectSession .= "<span id=\"SpanSysMsg".$parIndex."_".$parSessionNumber."\"></span>";
			$SelectSession .= "</td></tr></table>";
			
			return $SelectSession;
		}
		
		// [2017-1101-1454-59170] move out from getSelectSessionString() - to solve the performance issue
		$detentionSessionAry = array();
		$detentionSessionCountAry = array();
		$detentionSessionStringAry = array();
		
		// get detention session - apply to All Student
		$detentionList = $ldiscipline->getDetentionList("FUTURE", "AVAILABLE", "ALL_FORMS", "", "ASC");
		
		// loop Sessions
		$detentionSessionStr = "";
		$detentionSessionNum = count($detentionList);
		for ($d=0; $d<$detentionSessionNum; $d++)
		{
			$currentDetentionID = $detentionList[$d][0];
			$returnStr = $ldiscipline->displayDetentionSessionString($currentDetentionID);
			$detentionSessionStringAry[$currentDetentionID] = $returnStr;
			
			$detentionSessionStr .= "<option class=\"row_avaliable\" value=\"".$currentDetentionID."\">$returnStr</option>";
		}
		
		// Data for Apply All Detention Settings
		$detentionSessionAry[0] = $detentionSessionStr;
		$detentionSessionCountAry[0] = $detentionSessionNum;

		// Loop for individual student
		$TableIndStudent = "";
		$DetentionRecordCount = 0;
		$i = 0;
		foreach($StudentArr as $Key => $Value)
		{
			if($Value['RecordType']==-1)
			{
				//  [2017-1101-1454-59170] move out from getSelectSessionString() - to solve the performance issue
				// get detention session - for each Student
				$detentionList = $ldiscipline->getDetentionList("FUTURE", "AVAILABLE", "THIS_FORM", $Key, "ASC");
				
				// loop Sessions
				$detentionSessionStr = "";
				$detentionSessionNum = count($detentionList);
				for ($d=0; $d<$detentionSessionNum; $d++)
				{
					$currentDetentionID = $detentionList[$d][0];
					
					// [2017-1003-1124-05235] apply temp array
					if(isset($detentionSessionStringAry[$currentDetentionID])) {
						$returnStr = $detentionSessionStringAry[$currentDetentionID];
					}
					else {
						$returnStr = $ldiscipline->displayDetentionSessionString($currentDetentionID);
						$detentionSessionStringAry[$currentDetentionID] = $returnStr;
					}
					
					$detentionSessionStr .= "<option class=\"row_avaliable\" value=\"".$currentDetentionID."\">$returnStr</option>";
				}
				
				// Data for Student Detention Settings
				$detentionSessionAry[$i+1] = $detentionSessionStr;
				$detentionSessionCountAry[$i+1] = $detentionSessionNum;
				
				$resultStudentName = $ldiscipline->getStudentNameByID($Key);
				list($TmpUserID, $TmpName, $TmpClassName, $TmpClassNumber) = $resultStudentName[0];
				
				$TableIndStudent .= "<br />";
				$TableIndStudent .= "<table align=\"center\" width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
				$TableIndStudent .= "<tr>";
				$TableIndStudent .= "<td colspan=\"2\" valign=\"top\" nowrap=\"nowrap\">".($i+1).".";
				$TableIndStudent .= "<img src=\"".$image_path."/".$LAYOUT_SKIN."/ediscipline/icon_stu_ind.gif\" width=\"20\" height=\"20\" align=\"absmiddle\">";
				$TableIndStudent .= "<span class=\"sectiontitle\">".$TmpClassName."-".$TmpClassNumber." ".$TmpName."</span>";
				$TableIndStudent .= "</td>";
				$TableIndStudent .= "</tr>";
				$TableIndStudent .= "<tr valign=\"top\" class=\"tablerow1\">";
				$TableIndStudent .= "<td nowrap=\"nowrap\" class=\"formfieldtitle\">";
				$TableIndStudent .= "<span class=\"tabletext\">".$i_Discipline_Detention_Times." <span class=\"tabletextrequire\">*</span></span>";
				$TableIndStudent .= "</td>";
				$TableIndStudent .= "<td width=\"80%\">";
				$TableIndStudent .= "<input type=\"hidden\" id=\"HiddenDetentionCount".($i+1)."\" name=\"HiddenDetentionCount".($i+1)."\" value=\"1\" />";
				$TableIndStudent .= "<select id=\"DetentionCount".($i+1)."\" name=\"DetentionCount".($i+1)."\" onChange=\"javascript:updateSessionCell(".($i+1).");\">".$OptionForDetentionCount."</select>";
				$TableIndStudent .= "</td>";
				$TableIndStudent .= "</tr>";
				$TableIndStudent .= "<tr class=\"tablerow1\">";
				$TableIndStudent .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\">".$i_Discipline_Session." <span class=\"tabletextrequire\">*</span></td>";
				// [2017-1101-1454-59170] change input parameter to getSelectSessionString() - to solve the performance issue
//				$TableIndStudent .= "<td valign=\"top\"><table name=\"TableSession".($i+1)."\" id=\"TableSession".($i+1)."\" width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\"><tr><td width=\"15\" nowrap>1.</td><td nowrap>".getSelectSessionString($i+1, 1, $TmpUserID)."</td></tr></table></td>";
				$TableIndStudent .= "<td valign=\"top\"><table name=\"TableSession".($i+1)."\" id=\"TableSession".($i+1)."\" width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\"><tr><td width=\"15\" nowrap>1.</td><td nowrap>".getSelectSessionString($i+1, 1, $detentionSessionNum, $detentionSessionStr)."</td></tr></table></td>";
				$TableIndStudent .= "</tr>";
				$TableIndStudent .= "<tr class=\"tablerow1\">";
				$TableIndStudent .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\">".$i_Discipline_Remark."</td>";
				$TableIndStudent .= "<td valign=\"top\">".$linterface->GET_TEXTAREA("TextRemark".($i+1), "")."</td>";
				$TableIndStudent .= "</tr>";
				if ($i != sizeof($student)-1) {
					$TableIndStudent .= "<tr>";
					$TableIndStudent .= "<td height=\"1\" colspan=\"2\" class=\"dotline\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"1\"></td>";
					$TableIndStudent .= "</tr>";
				}
				$TableIndStudent .= "</table>";
				
				$DetentionRecordCount++;
				$i++;
			}
		}
		
		// Preset detention session (for javascript)
		$jSessionString = "var SelectSessionArr=new Array(".(($DetentionRecordCount)+1).");\n";
		$jSessionString .= "SelectSessionArr[0]=new Array(10);\n";
		$k = 1;
		foreach($StudentArr as $Key => $Value)
		{
			if($Value['RecordType']==-1)
			{
				$jSessionString .= "SelectSessionArr[".$k."]=new Array(10);\n";
				$k++;
			}
		}
		
		$k = 1;
		for ($j=0; $j<$session_times; $j++) 
		{
			// [2017-1101-1454-59170] change input parameter to getSelectSessionString() - to solve the performance issue
			//$jSessionString .= "SelectSessionArr[0][".$j."]='".str_replace("'", "&#039;", getSelectSessionString(0, $j+1, ""))."';\n";
			$jSessionString .= "SelectSessionArr[0][".$j."]='".str_replace("'", "&#039;", getSelectSessionString(0, $j+1, $detentionSessionCountAry[0], $detentionSessionAry[0]))."';\n";
		}
		foreach($StudentArr as $Key => $Value)
		{
			if($Value['RecordType']==-1)
			{
				for ($j=0; $j<$session_times; $j++) 
				{
					// [2017-1101-1454-59170] change input parameter to getSelectSessionString() - to solve the performance issue
//					$jSessionString .= "SelectSessionArr[".$k."][".$j."]='".str_replace("'", "&#039;", getSelectSessionString($k, $j+1, ($k==0)?"":$student[$i-1]))."';\n";
//					$jSessionString .= "SelectSessionArr[".$k."][".$j."]='".str_replace("'", "&#039;", getSelectSessionString($k, $j+1, $student[$j]))."';\n";
					$jSessionString .= "SelectSessionArr[".$k."][".$j."]='".str_replace("'", "&#039;", getSelectSessionString($k, $j+1, $detentionSessionCountAry[$k], $detentionSessionAry[$k]))."';\n";
				}
				$k++;
			}
		}

		// Initial form after submit fails (for javascript)
		if ($FailFlag)
		{
			$SessionNAMsg = str_replace("\n", "", str_replace("'", "\"", $linterface->GET_SYS_MSG("session_not_available")));
			$SessionPastMsg = str_replace("\n", "", str_replace("'", "\"", $linterface->GET_SYS_MSG("session_past")));
			$SessionFullMsg = str_replace("\n", "", str_replace("'", "\"", $linterface->GET_SYS_MSG("session_full")));
			$SessionAssignedBeforeMsg = str_replace("\n", "", str_replace("'", "\"", $linterface->GET_SYS_MSG("session_assigned_before")));

			$jInitialForm = "";
			
			for ($l=1; $l<=sizeof($submitStudent); $l++) {
				$jInitialForm .= "if(document.getElementById('DetentionCount".$l."')) document.getElementById('DetentionCount".$l."').value='".${"submitDetentionCount".$l}."';\n";
				$jInitialForm .= "if(document.getElementById('HiddenDetentionCount".$l."')) document.getElementById('HiddenDetentionCount".$l."').value='1';\n";
				$jInitialForm .= "updateSessionCell(".$l.");\n";
				for ($m=1; $m<=${"submitDetentionCount".$l}; $m++) {
					$jInitialForm .= "if(document.getElementById('SelectSession".$l."_".$m."')) document.getElementById('SelectSession".$l."_".$m."').value='".${"submitSelectSession".$l."_".$m}."';\n";
					if ($AvailableFlag[$l][$m] == "NO") {
						$jInitialForm .= "if(document.getElementById('SpanSysMsg".$l."_".$m."')) document.getElementById('SpanSysMsg".$l."_".$m."').innerHTML='".$SessionNAMsg."';\n";
					}
					else if ($FullFlag[$l][$m] == "YES") {
						$jInitialForm .= "if(document.getElementById('SpanSysMsg".$l."_".$m."')) document.getElementById('SpanSysMsg".$l."_".$m."').innerHTML='".$SessionFullMsg."';\n";
					}
					else if ($PastFlag[$l][$m] == "YES") {
						$jInitialForm .= "if(document.getElementById('SpanSysMsg".$l."_".$m."')) document.getElementById('SpanSysMsg".$l."_".$m."').innerHTML='".$SessionPastMsg."';\n";
					}
					else if ($StudentInSessionFlag[$l][$m] == "YES") {
						$jInitialForm .= "if(document.getElementById('SpanSysMsg".$l."_".$m."')) document.getElementById('SpanSysMsg".$l."_".$m."').innerHTML='".$SessionAssignedBeforeMsg."';\n";
					}
				}
				$jInitialForm .= "if(document.getElementById('TextRemark".$l."')) document.getElementById('TextRemark".$l."').value='".${"submitTextRemark".$l}."';\n";
			}
		}

########## For Detention (End) ##########

########## For eNotice (Begin) ##########
# check template is ava
$NoticeTemplateAva = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO("",1);

if (!$lnotice->disabled && !empty($NoticeTemplateAva))
	{
		// [2014-1216-1347-28164]
		//$enoticeCheckboxName = $canSendPushMsg ? $Lang['eDiscipline']['DetentionMgmt']['SendNotification'] : $i_Discipline_Send_Notice_Via_ENotice;
		$enoticeCheckboxName = $i_Discipline_Send_Notice_Via_ENotice;
		$enotice_checkbox = '<input name="action_notice" type="checkbox" id="action_notice" value="1" onClick="javascript:if(this.checked){document.getElementById(\'noticeFlag\').value=\'YES\';showDiv(\'divNotice\');}else{document.getElementById(\'noticeFlag\').value=\'NO\';hideDiv(\'divNotice\');}" /><label for="action_notice">'.$enoticeCheckboxName.'</label><br>';
		$catTmpArr = $ldiscipline->TemplateCategory();
		if(is_array($catTmpArr))
		{
			$catArr[0] = array("0","-- $button_select --");
			foreach($catTmpArr as $Key=>$Value)
			{
				# check the Template Catgory has template or not
				$NoticeTemplateAvaTemp = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO("",1, $Key);
				if(!empty($NoticeTemplateAvaTemp))
					$catArr[] = array($Key,$Key);
			}
		}

		for ($i=0; $i<=sizeof($student); $i++) {
			
			${"catSelection".$i} = $linterface->GET_SELECTION_BOX($catArr, 'id="CategoryID'.$i.'", name="CategoryID'.$i.'" onChange="changeCat(this.value, '.$i.')"', "", (($i==0) ? $defaultTemplateID : $TemplateCategoryID[$student[$i-1]]));
			if ($i > 0) {
				$resultStudentName = $ldiscipline->getStudentNameByID($student[$i-1]);
				list($TmpUserID, $TmpName, $TmpClassName, $TmpClassNumber) = $resultStudentName[0];
				// [2014-1216-1347-28164]
				$parentCanReceiveMsg = $canSendPushMsg? ${"record_type_".$student[$i-1]}==-1 && $luser->getParentUsingParentApp($student[$i-1], true) : false;
				$TableNotice .= "<table align=\"center\" width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">\n";
				$TableNotice .= "<tr>\n";
				$TableNotice .= "<td colspan=\"2\" valign=\"top\" nowrap>\n";
				$TableNotice .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
				$TableNotice .= "<tr>\n";
				$TableNotice .= "<td>$i. <img src=\"$image_path/$LAYOUT_SKIN/ediscipline/icon_stu_ind.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"> <span class=\"sectiontitle\">$TmpClassName-$TmpClassNumber $TmpName</span></td>\n";
				$TableNotice .= "<td align=\"right\"><span class=\"sectiontitle\">\n";
				$TableNotice .= $linterface->GET_BTN($button_view_template, "button", "javascript:previewNotice($i);");
				$TableNotice .= "</span></td>\n";
				$TableNotice .= "</tr>\n";
				$TableNotice .= "</table>\n";
				$TableNotice .= "</td>\n";
				$TableNotice .= "</tr>\n";
				$TableNotice .= "<tr valign=\"top\" class=\"tablerow1\">\n";
				$TableNotice .= "<td width=\"20%\" nowrap class=\"formfieldtitle\"><span class=\"tabletext\">$i_Discipline_Usage_Template <span class=\"tabletextrequire\">*</span></span></td>\n";
				$TableNotice .= "<td>\n";
				$TableNotice .= "<input type=\"hidden\" id=\"StudentID$i\" name=\"StudentID$i\" value=\"".$TmpUserID."\" />";
				$TableNotice .= "<input type=\"hidden\" id=\"StudentName$i\" name=\"StudentName$i\" value=\"".$TmpName."\" />";
				$TableNotice .= ${"catSelection".$i}." <select name=\"SelectNotice$i\" id=\"SelectNotice$i\"><option value=\"0\">-- $button_select --</option></select>\n";
				$TableNotice .= "</td>\n";
				$TableNotice .= "</tr>\n";
				$TableNotice .= "<tr class=\"tablerow1\">\n";
				$TableNotice .= "<td valign=\"top\" nowrap class=\"formfieldtitle\">$i_Discipline_Additional_Info</td>\n";
				$TableNotice .= "<td valign=\"top\"><textarea name=\"TextAdditionalInfo$i\" id=\"TextAdditionalInfo$i\" rows=\"2\" wrap=\"virtual\" class=\"textboxtext\"></textarea></td>\n";
				$TableNotice .= "</tr>\n";
				$TableNotice .= "</tr>\n";
				
				// [2014-1216-1347-28164] display checkbox to send push message if enable eClass App and student's parents using eClass App
				if($parentCanReceiveMsg){
					$TableNotice .= "<tr class=\"tablerow1\">\n";
					$TableNotice .= "<td valign=\"top\">&nbsp;</td>\n";
					$TableNotice .= "<td valign=\"top\"><input type='checkbox' name=\"eClassAppNotify$i\" id=\"eClassAppNotify$i\" value='1' checked><label for=\"eClassAppNotify$i\"'>".$Lang['eDiscipline']['Detention']['SendNotification']."</label></td>\n";
					$TableNotice .= "</tr>\n";
				}
				
				$TableNotice .= "<tr class=\"tablerow1\">\n";
				$TableNotice .= "<td valign=\"top\">&nbsp;</td>\n";
				// [2014-1216-1347-28164] checkbox not checked if enable eClass App and student's parents using eClass App
//				$TableNotice .= "<td valign=\"top\"><input type='checkbox' name=\"emailNotify$i\" id=\"emailNotify$i\" value='1' checked><label for=\"emailNotify$i\"'>".$Lang['eDiscipline']['EmailNotifyParent']."</label></td>\n";
				$TableNotice .= "<td valign=\"top\"><input type='checkbox' name=\"emailNotify$i\" id=\"emailNotify$i\" value='1' ".($parentCanReceiveMsg || $sys_custom['eDiscipline']['DefaultUncheckSendEmailCheckbox']? "" : "checked")."><label for=\"emailNotify$i\"'>".$Lang['eDiscipline']['EmailNotifyParent']."</label></td>\n";
				$TableNotice .= "</tr>\n";
				if ($i != sizeof($student)) {
					$TableNotice .= "<tr>\n";
					$TableNotice .= "<td height=\"1\" colspan=\"2\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\"></td>\n";
					$TableNotice .= "</tr>\n";
				}
				$TableNotice .= "</table>\n";
			}
		}


		// Preset template items (for javascript)
		for ($i=0; $i<sizeof($catArr); $i++) {
			$result = $ldiscipline->getDetentionENoticeByCategoryID("DISCIPLINE", $catArr[$i][0]);
			for ($j=0; $j<=sizeof($result); $j++) {
				if ($j==0) {
					$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]."Value = new Array(".(sizeof($result)+1).");\n";
					$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]." = new Array(".(sizeof($result)+1).");\n";
					$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."Value[$j] = \"0\"\n";
					$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."[$j] = \"-- $button_select --\"\n";
					//$js .= "obj.options[$j] = new Option('-- $button_select --',0);\n";
				} else {
					$tempTemplate = $result[$j-1][1];
					$tempTemplate=str_replace("&lt;", "<", $tempTemplate);
			        $tempTemplate=str_replace("&gt;", ">", $tempTemplate);
			        $tempTemplate=str_replace("&#039;","'",$tempTemplate);
			        $tempTemplate=str_replace("&amp;", "&", $tempTemplate);
        			//$tempTemplate=str_replace("&quot;", "\"", $tempTemplate);
        
					$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."Value[$j] = \"".$result[$j-1][0]."\"\n";
					$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."[$j] = \"".$tempTemplate."\"\n";
					//$js .= "obj.options[$j] = new Option('$tempTemplate',".$result[$j-1][0].");\n";
				}
			}
		}
	}
	if ($lnotice->disabled)
	{
		//$enotice_checkbox = $eDiscipline["EnoticeDisabledMsg"];	
		$enotice_checkbox = '<input name="action_notice" type="checkbox" id="action_notice" disabled /><label for="action_notice">'.$eDiscipline["SendNoticeWhenReleased"].'</label><br><br>('.$eDiscipline["EnoticeDisabledMsg"].')<br>';
	}

########## For eNotice (End) ##########


if (!($submit_flag == "YES" && $FailFlag == false))
{
$DetentionRecordCount=0;

foreach($_POST as $Key=>$Value)
{
	if(substr($Key,0,11)=='record_type' && $Key!='record_type' && $Value=='-1')
	{
		$DetentionRecordCount++;	
	}
}

# Start layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
<!--
<?=$jSessionString?>
<?=$jTemplateString?>

function viewCurrentRecord(id)
{
         newWindow('viewcurrent.php?StudentID='+id, 8);
}

function showDiv(div)
{
	document.getElementById(div).style.display="inline";
}

function hideDiv(div)
{
	document.getElementById(div).style.display="none";
}

function updateSessionCell(studentIndex){
	if(studentIndex==undefined) studentIndex='0';

	if(document.getElementById('HiddenDetentionCount'+studentIndex))
	{
		var oldCount=parseInt(document.getElementById('HiddenDetentionCount'+studentIndex).value);
		var newCount=parseInt(document.getElementById('DetentionCount'+studentIndex).value);
	}

	if(oldCount<newCount){
		
		var newRow, newCell;
		for(var i=oldCount;i<newCount;i++){
			newRow=document.getElementById('TableSession'+studentIndex).insertRow(document.getElementById('TableSession'+studentIndex).rows.length);
			newCell0=newRow.insertCell(0);
			newCell1=newRow.insertCell(1);
			newCell0.innerHTML=(i+1)+'.';
			newCell1.setAttribute("noWrap", true);
			newCell1.innerHTML=SelectSessionArr[studentIndex][i];
			//newCell1.innerHTML += studentIndex;
			
		}
		document.getElementById('HiddenDetentionCount'+studentIndex).value = newCount;
	} else if(oldCount>newCount){
		for(var i=oldCount;i>newCount;i--){
			document.getElementById('TableSession'+studentIndex).deleteRow(i-1);
		}
		document.getElementById('HiddenDetentionCount'+studentIndex).value = newCount;
	}
	
//	document.getElementById('DetentionCount'+studentIndex).value = newCount;
}

function applyToAll(studentCount){
	for(var i=1;i<=studentCount;i++){

		document.getElementById('DetentionCount'+i).value = document.getElementById('DetentionCount0').value;
		
		updateSessionCell(i);
		
		for(var j=1;j<=document.getElementById('DetentionCount0').value;j++){
			
			document.getElementById('SelectSession'+i+'_'+j).value = document.getElementById('SelectSession0_'+j).value;
			if (document.getElementById('SelectSession'+i+'_'+j).value != document.getElementById('SelectSession0_'+j).value){
				document.getElementById('SelectSession'+i+'_'+j).value = 'AUTO';
			}
		}
		document.getElementById('TextRemark'+i).value = document.getElementById('TextRemark0').value;
	}
}

function changeCat(cat, selectIdx){

	var x=document.getElementById("SelectNotice"+selectIdx);

	if(x)
	{
		var listLength = x.length;
	}
	for (var i=listLength-1; i>=0; i--) {
		x.remove(i);
	}
	try {
		var tmpCatLength = eval("jArrayTemplate"+cat).length;
	} catch (err) {
		var tmpCatLength = 0;
		
		// add empty option to prevent js error when submit form
		if((listLength-1)>=0){
			var y = document.createElement('option');
			y.text = "-- <?=$button_select?> --";
			y.value = "0";
			try {
				x.add(y,null); // standards compliant
			} catch(ex) {
				x.add(y); // IE only
			}
		}
	}

	for (var j=0; j<tmpCatLength; j++) {
		var y = document.createElement('option');
		y.text = eval("jArrayTemplate"+cat)[j];
		y.value = eval("jArrayTemplate"+cat+"Value")[j];

		try {
			x.add(y,null); // standards compliant
		} catch(ex) {
			x.add(y); // IE only
		}
	}

	/****************
	obj = x;
	<?
	
	for ($i=0; $i<sizeof($catArr); $i++) {
		
		$result = $ldiscipline->getDetentionENoticeByCategoryID("DISCIPLINE", $catArr[$i][0]);?>
		
		if(cat=="<?=$catArr[$i][0]?>") {
			<?
			for ($j=0; $j<=sizeof($result); $j++) { 
				if($result[$j][0]==0) {
					echo "obj.options[0] = new Option('-- $button_select --',0);\n";	
				} else {
					$tempTemplate = $result[$j-1][1];
					echo "obj.options[$j] = new Option('$tempTemplate',".$result[$j-1][0].");\n";
				}
			}
			
		?> } <?
	}
	?>
	****************/
}

function itemSelected(itemID, selectIdx, templateID) {
	var obj = document.form1;
	if(templateID!="") {
		var item_length = eval("obj.SelectNotice"+selectIdx+".length");
		for(var i=0; i<item_length; i++) {
			if(obj.elements['SelectNotice'+selectIdx].options[i].value==templateID) {
				obj.elements['SelectNotice'+selectIdx].selectedIndex = i;
				//alert(i);
				break;	
			}
		}
	}
}

function previewNotice(studentIdx){
	var templateID = document.getElementById("SelectNotice"+studentIdx).value;
	var studentName = '';
	if(studentIdx!=0) {
		studentName = document.getElementById("StudentName"+studentIdx).value;
	}
	var additionInfo = document.getElementById("TextAdditionalInfo"+studentIdx).value;
	newWindow("../award_punishment/preview_notice.php?tid="+templateID+"&pv_studentname="+studentName+"&pv_additionalinfo="+additionInfo);
}

function applyNoticeToAll(studentCount){
	for(var i=1;i<=studentCount;i++){
		changeCat(document.getElementById('CategoryID0').value, i);
		document.getElementById('CategoryID'+i).value = document.getElementById('CategoryID0').value;
		document.getElementById('SelectNotice'+i).value = document.getElementById('SelectNotice0').value;
		document.getElementById('TextAdditionalInfo'+i).value = document.getElementById('TextAdditionalInfo0').value;
		<?php if($canSendPushMsg){ ?>
			if(document.getElementById('eClassAppNotify'+i)){
				document.getElementById('eClassAppNotify'+i).checked = document.getElementById('eClassAppNotify0').checked;
			}
		<?php } ?>
		document.getElementById('emailNotify'+i).checked = document.getElementById('emailNotify0').checked;
	}
}

function checkForm()
{
	/*
	if(document.getElementById('formValid').value == 0 || document.getElementById('formValid').value == "") {
		return false;	
	}
	*/

	<? if (!$lnotice->disabled && !empty($NoticeTemplateAva)) {?>	
	if (document.getElementById('action_notice').checked) {
		for(var i=1;i<=<?=sizeof($student)?>;i++){
			if (document.getElementById('SelectNotice'+i).value=='0') {
				document.getElementById('submit_btn').disabled = false; 
				document.getElementById('submit_btn').className = "formbutton_v30 print_hide"; 
				alert('<?=$i_alert_pleaseselect.$i_Discipline_Template?>');
				return false;
			}
		}
	}
	<? } ?>
	
	if(document.getElementById('action_detention')==undefined || document.getElementById('action_detention').checked==false || document.getElementById('formValid').value==1) {
		document.getElementById('submit_flag').value='YES';
		document.form1.submit();
	} else {
		document.getElementById('submit_btn').disabled = false; 
		document.getElementById('submit_btn').className = "formbutton_v30 print_hide";
	} 
	return true;
}

function showMsg(msg) {
	
	if(document.getElementById('action_detention')!=undefined && document.getElementById('action_detention').checked==true) {
		var temp;
		var warningMsg = "";
		document.getElementById('formValid').value = 1;
		
		var msgSplit = msg.split(")\n");
		for(i=0;i<msgSplit.length-1;i++) {
			var layer = msgSplit[i].substring(1);
			temp = layer.split(",");
			
			if(temp[1] != 0) {
				document.getElementById('formValid').value = 0;
			}
	
			layername = document.getElementById(temp[0]);
			//layername = eval(temp[0]);
			
	
			switch(temp[1]) {
				case "0" : warningMsg = ""; break;
				case "1" : warningMsg = "<?=substr($i_con_msg_session_not_available,0,-1) ?>"; break;
				case "2" : warningMsg = "<?=substr($i_con_msg_session_full,0,-1) ?>"; break;
				case "3" : warningMsg = "<?=substr($i_con_msg_session_past,0,-1) ?>"; break;
				case "4" : warningMsg = "<?=substr($i_con_msg_session_assigned_before,0,-1) ?>"; break;
				default : warningMsg = ""; break;
			}
	
			layername.innerHTML = warningMsg;
	
		}
	}
	checkForm();
	
}
//-->
</script>
<script type="text/javascript" language="javascript">
var http_request = false;

function makeRequest(url, parameters) {

	http_request = GetXmlHttpObject()
	
	if (!http_request) {
		alert('Cannot create XMLHTTP instance');
		document.getElementById('submit_btn').disabled = false; 
		document.getElementById('submit_btn').className = "formbutton_v30 print_hide"; 
		return false;
	}
	http_request.onreadystatechange = alertContents;
	http_request.open('GET', url + parameters, true);
	http_request.send(null);
}

function alertContents() {
	//layername = document.getElementById('layername').value;
	//if (http_request.readyState == 4 && http_request.status == 200) {
	if(http_request.readyState==4 || http_request.readyState=="complete") {
		//document.getElementById(layername).innerHTML = http_request.responseText;  
		//document.getElementById(layername).style.border = "0px solid #A5ACB2";          
		showMsg(http_request.responseText);
	}
}

function get(obj, layername) {
	if(layername != undefined)
		document.getElementById('layername').value = layername;
	var getstr = "?";

	for (i=0; i<obj.elements.length; i++) {
		if (obj.elements[i].tagName == "SELECT") {
			var sel = obj.elements[i];
			getstr += sel.name + "=" + sel.options[sel.selectedIndex].value + "&";
		}
		if (obj.elements[i].tagName == "INPUT") {
			var inTemp = obj.elements[i];
			getstr += inTemp.name + "=" + inTemp.value + "&";
		}
		if (obj.elements[i].tagName == "TEXTAREA") {
			var inTemp = obj.elements[i];
			getstr += inTemp.name + "=" + inTemp.value + "&";
		}
	}
	makeRequest('validate.php', getstr);
}

function GetXmlHttpObject()
{
	var http_request = null;
	try {
		// Firefox, Opera 8.0+, Safari
		http_request = new XMLHttpRequest();
	} catch (e) {
		// Internet Explorer
		try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			http_request = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return http_request;
}
</script>
<form name="form1" method="POST" action="new3.php">

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td class="navigation">
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
		</table>
	</td>
</tr>
<tr>
	<td align="center"><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
</tr>

<tr>
	<td>
		<table align="center" width="90%" border="0" cellpadding="5" cellspacing="0">
		<tr colspan="2" valign="top" align="right">
			<td>&nbsp;</td><td><?=$linterface->GET_SYS_MSG($msg)?></td>
		</tr>
		<tr>
			<td colspan="2" valign="top" nowrap="nowrap"><?=$i_general_students_selected?></td>
		<tr>
		<tr>
			<td colspan="2" valign="top"><?=$selected_student_table?></td>
		</tr>
		<tr>
			<td colspan="2" valign="middle" nowrap="nowrap" class="form_sep_title"><i> - <?=$eDiscipline["Action"]?> -</i></td>
		</tr>
		<tr>
			<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"></td>
			<td>

<? 
if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-New") && $DetentionRecordCount>0) {?>
				<input name="action_detention" type="checkbox" id="action_detention" value="1" onClick="javascript:if(this.checked){document.getElementById('detentionFlag').value='YES';showDiv('divDetention');}else{document.getElementById('detentionFlag').value='NO';hideDiv('divDetention');}" /><label for="action_detention"><?=$eDiscipline['Detention']?></label><br>
<div id="divDetention" style="display:none">
	<fieldset class="form_sub_option" style="float:none">
	<? if(sizeof($student)>1) {?>
		<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0" class="tablerow2">
			<tr>
				<td colspan="2" valign="top" nowrap="nowrap">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>
								<img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_section.gif" width="20" height="20" align="absmiddle">
								<span class="sectiontitle"><?=$eDiscipline["GlobalSettingsToAllStudent"]?></span>
							</td>
							<td align="right">
								<span class="sectiontitle">
<?=$linterface->GET_BTN($i_Discipline_Apply_To_All, "button", "javascript:applyToAll(".($DetentionRecordCount).");")?>
								</span>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr valign="top">
				<td nowrap="nowrap" class="formfieldtitle">
					<span class="tabletext"><?=$i_Discipline_Detention_Times?> <span class="tabletextrequire">*</span></span>
				</td>
				<td width="80%">
					<input type="hidden" id="HiddenDetentionCount0" name="HiddenDetentionCount0" value="1" />
					<select name="DetentionCount0" id="DetentionCount0" onChange="javascript:updateSessionCell();"><?=$OptionForDetentionCount?></select>
				</td>
			</tr>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_Session?> <span class="tabletextrequire">*</span></td>
				<td valign="top"><table name="TableSession0" id="TableSession0" width="100%" border="0" cellpadding="5" cellspacing="0"><tr><td width="15" nowrap>1.</td><td nowrap><?=getSelectSessionString(0, 1, $detentionSessionCountAry[0], $detentionSessionAry[0])?><!--<?=getSelectSessionString(0, 1, "")?>--></td></tr></table></td>
			</tr>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_UserRemark?></td>
				<td valign="top"><?=$linterface->GET_TEXTAREA("TextRemark0", "")?></td>
			</tr>
		</table>
		<? } ?>
<?=$TableIndStudent?>

<span name="myspan" id="myspan"></span>
	</fieldset>
</div>
<? } ?>

<?=$enotice_checkbox?>

<div id="divNotice" style="display:none">
	<fieldset class="form_sub_option">
	<? if(sizeof($student)>1) {?>
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0" class="tablerow2">
					<tr>
						<td colspan="2" valign="top" nowrap="nowrap">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td><img src="<?="$image_path/$LAYOUT_SKIN"?>/icon_section.gif" width="20" height="20" align="absmiddle"><span class="sectiontitle"><?=$eDiscipline["GlobalSettingsToAllStudent"]?></span></td>
									<td align="right"><span class="sectiontitle">
<?=$linterface->GET_BTN($i_Discipline_Apply_To_All, "button", "javascript:applyNoticeToAll(".sizeof($student).");")?>&nbsp;
<?=$linterface->GET_BTN($button_view_template, "button", "javascript:previewNotice(0);")?>
									</span></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr valign="top">
						<td width="20%" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Discipline_Template?> <span class="tabletextrequire">*</span></span></td>
						<td>
<?=$catSelection0?> <select name="SelectNotice0" id="SelectNotice0"><option value="0"><?="-- $button_select --"?></option></select>
						</td>
					</tr>
					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_Additional_Info?></td>
						<td valign="top"><textarea name="TextAdditionalInfo0" id="TextAdditionalInfo0" rows="2" wrap="virtual" class="textboxtext"></textarea></td>
					</tr>
					
					<?php if($canSendPushMsg){ ?>
						<tr>
							<td valign="top" nowrap="nowrap">&nbsp;</td>
							<td valign="top"><input type='checkbox' name='eClassAppNotify0' id='eClassAppNotify0' value='1' checked><label for='eClassAppNotify0'><?=$Lang['eDiscipline']['Detention']['SendNotification']?></label></td>
						</tr>
					<?php } ?>
					
					<tr>
						<td valign="top" nowrap="nowrap">&nbsp;</td>
						<td valign="top"><input type='checkbox' name='emailNotify0' id='emailNotify0' value='1' checked><label for='emailNotify0'><?=$Lang['eDiscipline']['EmailNotifyParent']?></label></td>
					</tr>
				</table>
				<br />
				<? } ?>
<?=$TableNotice?>
	</fieldset>
</div>

			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_finish, "button", "this.disabled=true; this.className='formbutton_disable_v30 print_hide'; get(this.form, 'show_result_".$parIndex."_".$parSessionNumber."');","submit_btn")?>&nbsp;
				
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>

<br />

<!-- Step 1 data //-->
<? foreach($_POST as $k=>$d) { 
	if($k=='student')
	{
		if (is_array($d))
		{
		    $d = IntegerSafe($d);
			for($a=0; $a<sizeof($d); $a++) {
				echo "<input type=\"hidden\" name=\"student[]\" value=\"".$d[$a]."\" />";
			}
		}
		else
		{
		    $tmpArr = (explode(",", $d));
		    $tmpArr = IntegerSafe($tmpArr);
			for($a=0; $a<sizeof($tmpArr); $a++) {
				echo "<input type=\"hidden\" name=\"student[]\" value=\"".$tmpArr[$a]."\" />";
			}
		}
	}
	else
	{
	    if(substr($k,0,14) != "DetentionCount" && substr($k,0,13) != "SelectSession" && substr($k,0,10) != "TextRemark"
	        && substr($k,0,12) != "SelectNotice" && substr($k,0,18) != "TextAdditionalInfo" && substr($k,0,13) != "detentionFlag" && substr($k,0,10) != "noticeFlag")
	    {
	        if($d != '') $d = intranet_htmlspecialchars($d);
	        
	        if(substr($k,0,4) == "GMID" || substr($k,0,11) == "record_type" || substr($k,0,5) == "CatID" || substr($k,0,6) == "ItemID") {
	            $d = IntegerSafe($d);
	        }
	        else if(substr($k,0,8) == "semester") {
	            $d = cleanCrossSiteScriptingCode($d);
	        }
	        else if(substr($k,0,10) == "RecordDate") {
	            if(!intranet_validateDate($d)) {
	                $d = '';
	            }
	        }
		?>
		<input type="hidden" name="<?=$k?>" id="<?=$k?>" value="<?=$d?>" />
<? 		} 
	}
}?>

<!-- Step 2 data //-->

<!-- Step 3 data //-->
<input type="hidden" name="submit_flag" id="submit_flag" value="YES" />

<input type="hidden" name="detentionFlag" id="detentionFlag" value="<?=$detentionFlag?>" />
<input type="hidden" name="noticeFlag" id="noticeFlag" value="<?=$noticeFlag?>" />
<input type="hidden" name="layername" id="layername" value="" />
<input type="hidden" name="formValid" id="formValid" value="1" />


</form>
<?
if ($FailFlag) {
?>
<script language="javascript">
<?=$jInitialForm?>
	<? if ($detentionFlag == "YES") { ?>
document.getElementById('action_detention').checked=true;
showDiv('divDetention');
	<? } ?>
	<? if ($noticeFlag == "YES") { ?>
document.getElementById('action_notice').checked=true;
showDiv('divNotice');
		<? for ($j=0; $j<=sizeof($student); $j++) { ?>
changeCat("<?=${"CategoryID".$j}?>", <?=$j?>);
if(document.getElementById('CategoryID<?=$j?>')) document.getElementById('CategoryID<?=$j?>').value = "<?=${"CategoryID".$j}?>";
if(document.getElementById('SelectNotice<?=$j?>')) document.getElementById('SelectNotice<?=$j?>').value = "<?=${"SelectNotice".$j}?>";
if(document.getElementById('TextAdditionalInfo<?=$j?>')) document.getElementById('TextAdditionalInfo<?=$j?>').value = "<?=${"TextAdditionalInfo".$j}?>";
		<? } ?>
	<? } ?>
	
</script>
<? }
else { ?>
<script language="javascript">
	<? if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-New") && $DetentionRecordCount>0 && $DefaultDetention){ ?>
		if(document.form1.action_detention){
			document.getElementById('action_detention').checked = true;
			document.getElementById('detentionFlag').value = 'YES';
			showDiv('divDetention');
		}
	<? } ?>
	<? if(!$lnotice->disabled && !empty($NoticeTemplateAva) && $DefaultSendNotice) {?>
		if(document.form1.action_notice){
			document.getElementById('action_notice').checked = true;
			document.getElementById('noticeFlag').value = 'YES';
			showDiv('divNotice');
		}
	<? } ?>
</script>
<? }
echo "<script language='javascript'>$js</script>";
$linterface->LAYOUT_STOP();

}		// End of !($submit_flag == "YES" && $FailFlag == false)

intranet_closedb();
?>