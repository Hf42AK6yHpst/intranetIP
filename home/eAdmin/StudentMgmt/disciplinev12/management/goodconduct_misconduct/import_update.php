<?php
// Modifying by: 
/*
 * 2017-01-09	Bill	[2016-0808-1743-20240]
 * 		- Auto insert unassigned detention for Not Submitted / Late misconduct	($sys_custom['eDiscipline']['BWWTC_AutoCreateDetention'])
 * 2015-10-26	(Bill)	[2015-0611-1642-26164]			[REMOVED]
 * 		Change logic for CWC Probation Scheme
 * 		- Approve probation records if upgrade new punishment records
 * 2014-05-30 (Carlos): $sys_custom['eDiscipline']['PooiToMiddleSchool'] - added [Score] data column
 */

$PATH_WRT_ROOT="../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-New");

$sql = "select * from temp_goodconduct_import where UserID=". $_SESSION["UserID"];
$recordArr = $ldiscipline->returnArray($sql);

### List out the import result
$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
$x .= "<tr>";
$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_general_class</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_identity_student</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eDiscipline']['EventDate']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_SettingsSemester</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$iDiscipline['RecordType']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_System_general_record</td>";
if($sys_custom['eDiscipline_GM_Times']) 
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eDiscipline']['ReceiveTimes']."</td>";
if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eDiscipline']['Score']."</td>";
}
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_PIC</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_System_general_remark</td>";
//$x .= "<td class=\"tablebluetop tabletopnolink\">".$iDiscipline['AccumulativePunishment_Import_Failed_Reason']."</td>";
$x .= "</tr>";

for($a=0;$a<sizeof($recordArr);$a++)
{	
	$x .= "<tr class=\"tablebluerow".($a%2+1)."\">";
	$x .= "<td class=\"tabletext\">".($a+1)."</td>";
	$x .= "<td class=\"tabletext\">". $recordArr[$a]['ClassName']."-".$recordArr[$a]['ClassNumber']."</td>";
	$x .= "<td class=\"tabletext\">". $recordArr[$a]['StudentName'] ."</td>";
	$x .= "<td class=\"tabletext\">". $recordArr[$a]['RecordDate'] ."</td>";
	$x .= "<td class=\"tabletext\">". $recordArr[$a]['Semester'] ."</td>";
	$x .= "<td class=\"tabletext\">". $recordArr[$a]['Category'] ."</td>";
	$x .= "<td class=\"tabletext\">". $recordArr[$a]['ItemName'] ."</td>";
	if($sys_custom['eDiscipline_GM_Times']) 
		$x .= "<td class=\"tabletext\">". $recordArr[$a]['GMCount']."</td>";
	if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
		$x .= "<td class=\"tabletext\">". $recordArr[$a]['ScoreChange']."</td>";
	}
	$x .= "<td class=\"tabletext\">". $recordArr[$a]['Teacher'] ."</td>";
	$x .= "<td class=\"tabletext\">". stripslashes($recordArr[$a]['Remark']) ."</td>";
	$x .= "</tr>";
	
	$semInfo = getAcademicYearAndYearTermByDate($recordArr[$a]['RecordDate']);
	
	$dataAry = array();
	$dataAry['CategoryID'] = $recordArr[$a]['CategoryID'];
	$dataAry['ItemID'] = $recordArr[$a]['ItemID'];
	$dataAry['RecordDate'] = $recordArr[$a]['RecordDate'];
	$dataAry['StudentID'] = $recordArr[$a]['StudentID'];
	$dataAry['Remark'] = addslashes($recordArr[$a]['Remark']);
	$dataAry['PICID'] = $recordArr[$a]['PICID'];
	$dataAry['RecordType'] = $recordArr[$a]['RecordType'];
	$dataAry['Year'] = $recordArr[$a]['Year'];
	$dataAry['Semester'] = $recordArr[$a]['Semester'];
	$dataAry['AcademicYearID'] = $ldiscipline->getAcademicYearIDByYearName($recordArr[$a]['Year']);
	$dataAry['YearTermID'] = $semInfo[0];
	$dataAry['GMCount'] = $sys_custom['eDiscipline_GM_Times'] ? $recordArr[$a]['GMCount'] : 1;
	if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
		$dataAry['ScoreChange'] = $recordArr[$a]['ScoreChange'];
	}
	$accu_record_id = $ldiscipline->INSERT_MISCONDUCT_RECORD($dataAry);

	if($accu_record_id)
	{
		# do the grouping
		// [2015-0611-1642-26164]
//		if($sys_custom['eDiscipline']['CSCProbation']){
//			// approve probation record if update to punishment records
//			$ldiscipline->REGROUP_CONDUCT_RECORDS($accu_record_id, "", 1);
//		}
//		// Normal Case
//		else{
//			$ldiscipline->REGROUP_CONDUCT_RECORDS($accu_record_id);	
//		}
		$ldiscipline->REGROUP_CONDUCT_RECORDS($accu_record_id);	
		
		// [2016-0808-1743-20240] Insert detention (Not Submitted / Late Misconduct)
		if($sys_custom['eDiscipline']['BWWTC_AutoCreateDetention'] && ($recordArr[$a]['CategoryID'] == $sys_custom['eDiscipline']['BWWTC_WithoutSubmCatID'] || $recordArr[$a]['CategoryID'] == $sys_custom['eDiscipline']['BWWTC_LateCatID']))
		{
			$ldiscipline->INSERT_LATE_HW_DETNETION_RECORD($accu_record_id);
		}
		$xmsg2= "<font color=green>".sizeof($recordArr)." ".$iDiscipline['Reord_Import_Successfully']."</font>";
	}
	else {
		$xmsg="import_failed";		
	}
}
$x .= "</table>";

$linterface = new interface_html();

$import_button = $linterface->GET_ACTION_BTN($button_finish, "button","self.location='index.php'")."&nbsp;".$linterface->GET_ACTION_BTN($iDiscipline['ImportOtherRecords'], "button", "window.location='import.php'");

# menu highlight setting
$CurrentPage = "Management_GoodConductMisconduct";
$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eDiscipline['Good_Conduct_and_Misconduct']);

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

$linterface->LAYOUT_START();
?>

<form name="form1" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg,$xmsg2);?></td>
	</tr>
	<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td><?= $x ?></td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
			<tr>
				<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			</tr>
			<tr>
				<td align="center" colspan="2">
					<?=$import_button?>
				</td>
			</tr>
			</table>
		</td>
</tr>
</table>
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>