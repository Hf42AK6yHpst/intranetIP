<?php
// Modifying by: 
/*
 * 2018-11-05 (Bill): [Remark] column not compulsory    [2018-1102-1608-02073]
 * 2015-07-06 (Shan): update UI standard
 * 2014-05-30 (Carlos): $sys_custom['eDiscipline']['PooiToMiddleSchool'] - added [Score] data column
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-New");

$linterface = new interface_html();
$lsp = new libstudentprofile();

# Menu highlight
$CurrentPage = "Management_GoodConductMisconduct";
$CurrentPageArr['eDisciplinev12'] = 1;

# Left Menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Tag
$TAGS_OBJ[] = array($eDiscipline['Good_Conduct_and_Misconduct']);

# Navigation
$navigationAry[] = array($eDiscipline['Good_Conduct_and_Misconduct'], 'index.php');
$navigationAry[] = array($Lang['Btn']['Import']);
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($navigationAry);

# Step
$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$csvFile = ($sys_custom['eDiscipline_GM_Times']) ? "good_conduct_with_times.csv" : "good_conduct.csv";

$layer_html = "<div id=\"ref_list\" style='position:absolute; width:280px; height:180px; z-index:1; visibility: hidden;'></div>";

$subcode_ref .= $Lang['eDiscipline']['Good_Conduct_and_Misconduct_Import_Instruct_Note_1_3'];
$subcode_ref .= "<strong>&lt;<a href=\"javascript:show_ref_list('SubjCode','sc_click')\" class=\"tablelink\">".$Lang['eDiscipline']['Good_Conduct_and_Misconduct_Import_Instruct_Note_1_4']."</a>&gt;</strong><span id=\"sc_click\">&nbsp;</span>";
/*
$format_str = $layer_html;
$format_str .= "<p><span id=\"GM__to\"><strong>".$iDiscipline['Import_Instruct_Main']."</strong></span></p>";
$format_str .= "<div class=\"p\"><br/><ol type=\"a\"><li>".$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_a_1']."
				<strong>&lt;<a class=\"tablelink\" href=\"". GET_CSV($csvFile) ."\" target=\"_blank\">".$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_a_2']."</a>&gt;</strong>".
				$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_a_3']."</li>";
$format_str .= "<li>".$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_b_1']."</span><br/>";
$format_str .= "<img src=\"".$image_path."/".$LAYOUT_SKIN."/discipline/gm_import_illus_$intranet_session_language.png\"/><br/><div class=\"note\">";
$format_str .= "<b>".$iDiscipline['Import_Instruct_Note']."</b><br/>";
$format_str .= "<ul><li>".$Lang['eDiscipline']['Good_Conduct_and_Misconduct_Import_Instruct_Note_1_1'];
$format_str .= "<strong>&lt;<a href=\"javascript:show_ref_list('ItemCode','ic_click')\" class=\"tablelink\">".$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_Note_1_2']."</a>&gt;</strong><span id=\"ic_click\">&nbsp;</span>";
if($intranet_session_language!='en')
$format_str .= $iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_Note_1_2_1'];
$format_str .= $subcode_ref;
if($intranet_session_language!='en')
	$format_str .= $Lang['eDiscipline']['Good_Conduct_and_Misconduct_Import_Instruct_Note_1_5'];


$format_str .= "</li>";
$format_str .= "<li><span id=\"GM__date\">".$Lang['eDiscipline']['Good_Conduct_and_Misconduct_Import_Instruct_Note_2_1']."</span></li></ul></div></li><br/>";
$format_str .= "<li><span id=\"GM__savee\">".$Lang['eDiscipline']['Good_Conduct_and_Misconduct_Import_Instruct_c_1']."</span></li></ol></div>";

*/
//********************
//$format_str .= "<a class=\"tablelink\" href=\"". GET_CSV($csvFile) ."\">[". $i_general_clickheredownloadsample ."]</a><br>";
//$format_str .= "<a class=\"tablelink\" href=\"get_csv_sample.php\">[". $i_general_clickheredownloadsample ."]</a><br>";
	
//$format_str .= "<br>";
	
# column description
//$subcode_ref .= "<strong>&lt;<a href=\"javascript:show_ref_list('SubjCode','sc_click')\" class=\"tablelink\">".$Lang['eDiscipline']['Good_Conduct_and_Misconduct_Import_Instruct_Note_1_4']."</a>&gt;</strong><span id=\"sc_click\">&nbsp;</span>";

//
//	$compulsoryColumn = array(0,1,2,3,4,5);
//	$FileDescription = "";
//	$delim = "";
//	
//	//$ColumnTextAry = ($sys_custom['eDiscipline_GM_Times']) ? $Lang['eDiscipline']['Import_GM_Column_With_Times'] : $Lang['eDiscipline']['Import_GM_Column'];
//	$ColumnTextAry = $Lang['eDiscipline']['Import_GM_Column'];
//	
//	for($i=0; $i<count($ColumnTextAry); $i++) {
//		$FileDescription .= $delim.$Lang['General']['ImportColumn']." ".($i+1)." : ";
//		$FileDescription .= (in_array($i, $compulsoryColumn)) ? "<span class='tabletextrequire'>*</span>" : "";
//		$FileDescription .= $ColumnTextAry[$i];
//		if($Lang['eDiscipline']['Import_GM_Reference'][$i]!="") {
//			$FileDescription .= " <a href=\"javascript:show_ref_list('".$Lang['eDiscipline']['Import_GM_Reference'][$i][1]."','".$Lang['eDiscipline']['Import_GM_Reference'][$i][2]."')\">[".$Lang['eDiscipline']['Import_GM_Reference'][$i][0]."]</a><span id=\"".$Lang['eDiscipline']['Import_GM_Reference'][$i][2]."\">&nbsp;</span>";	
//		}
//		if($Lang['eDiscipline']['Import_GM_Remark'][$i]!="") {
//			$FileDescription .= " <span class='tabletextremark'>(".$Lang['eDiscipline']['Import_GM_Remark'][$i].")</span>";	
//		}
//		$delim = "<br>";
//	}


$layer_html = "<div id=\"ref_list\"  class=\"selectbox_layer\"'></div>";

### Data column
$DataColumnTitleArr = array($i_ClassName,$Lang['General']['ClassNumber'], $i_EventDate,$i_Discipline_System_ItemCode,$i_Discipline_PIC, $i_Discipline_Remark);

// [2018-1102-1608-02073]
//$DataColumnPropertyArr = array(1,1,1,1,1,1);
$DataColumnPropertyArr = array(1,1,1,1,1,0);

$DataColumnRemarksArr = array();
$DataColumnRemarksArr[2] = '<span class="tabletextremark">&nbsp;'.$Lang['eDiscipline']['Import_GM_Remark'][2].'</span>';
//$DataColumnRemarksArr[3] = '<span class="tabletextremark">&nbsp;'.$Lang['eDiscipline']['Import_GM_Remark'][2].'</span>';
$DataColumnRemarksArr[3] =  "<a href=\"javascript:show_ref_list('ItemCode','ic_click')\" class=\"tablelink\">[".$iDiscipline['Award_Punishment_Import_Instruct_Note_3_2']."]</a><span id=\"ic_click\">&nbsp;</span>";
$DataColumnRemarksArr[4] = '<span class="tabletextremark">('.$Lang['SysMgr']['SubjectClassMapping']['ImportCSVField']['6'].')</span>';
$DataColumn = $linterface->Get_Import_Page_Column_Display($DataColumnTitleArr, $DataColumnPropertyArr, $DataColumnRemarksArr);

$format_str .= $DataColumn;
$format_str .= $FileDescription;

//********************

$linterface->LAYOUT_START();
?>

<script language="JavaScript">
var ClickID = '';
var callback_show_ref_list = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('ref_list').innerHTML = tmp_str;
	    DisplayPosition();
	}
}

function show_ref_list(type,click)
{
	ClickID = click;
	document.getElementById('task').value = type;

	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	
	var path = "ajax_gm.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_show_ref_list);
}

function Hide_Window(pos)
{
	document.getElementById(pos).style.visibility='hidden';
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function DisplayPosition()
{
	var posleft = getPosition(document.getElementById(ClickID),'offsetLeft') ;
	var postop = getPosition(document.getElementById(ClickID),'offsetTop');
	var divwidth = document.getElementById('ref_list').clientWidth;
	
	$('#ref_list').removeAttr('style');
	if(posleft+divwidth > document.body.clientWidth){
		document.getElementById('ref_list').style.right= "1px";
	}
	else{
		document.getElementById('ref_list').style.left= posleft+"px";
	}
	
	document.getElementById('ref_list').style.top= postop+"px";
	document.getElementById('ref_list').style.visibility='visible';
}
</script>

<br />
<form name="form1" method="post" action="import_result.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
	</tr>
	<tr>
		<td><?=$htmlAry['navigation']?></td>
	</tr>
	<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td>
			<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr>
		  		<td>
    				<table width="90%" class="form_table_v30">
    				<tr>
    					<td width="100%" valign="top" nowrap="nowrap" class="field_title" ><?=$linterface->RequiredSymbol().$Lang['General']['SourceFile']." <span class='tabletextremark'>".$Lang['General']['CSVFileFormat']."</span>"?>: </td>
    			  		<td class="tabletext"><input class="file" type="file" name="csvfile"></td>
    			  	</tr>
    				<tr>
    					<td width="100%" valign="top" nowrap="nowrap" class="field_title"><?= $Lang['General']['CSVSample'] ?></td>
    					<td valign="top" nowrap="nowrap" colspan='2' class="tabletext">
    						<a id="SampleCsvLink" class="tablelink" href="<?=GET_CSV($csvFile)?>" target="_blank">
    						<img src='<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/icon_files/xls.gif' border='0' align='absmiddle' hspace='3'>
    						<?=$i_general_clickheredownloadsample?>
						</td>
    				</tr>
    				<tr>
    					<td width="35%" valign="top" nowrap="nowrap" class="field_title" ><?= $i_general_Format ?></td>	
    		  			<td class="tabletext"><?=$format_str?></td>
    		  		</tr>
					</table>
		 		</td>
			</tr>
		    
		<!--<tr>
		<td colspan="2">
		
			<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr><td><table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
						<tr><td class="tabletext" align="left" width="30%"><?=$Lang['General']['SourceFile']." <span class='tabletextremark'>".$Lang['General']['CSVFileFormat']."</span>"?>: </td><td class="tabletext" width="85%"><input class="file" type="file" name="csvfile"></td></tr>
						<tr><td class="tabletext" align="left" colspan="2"><?=$Lang['eDiscipline']['ImportSourceFileNote']?></td></tr>
						</table>
					</td>	
				</tr>
				<tr>
					<td class="tabletext">
						<?=$format_str?>
					</td>
				</tr>
			</table>
			
			<br>
    		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $i_select_file ?>
					</td>
					<td class="tabletext"><input class="file" type="file" name="csvfile"><br>
					<div id="ref_list" style='position:absolute; width:280px; height:180px; z-index:1; visibility: hidden;'></div>					
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_general_Format ?></td>
					<td class="tabletext"><?=$format_str?><div id="RefDiv" style="visibility:hidden;"></div></td>
				</tr>
			</table>
		</td>
		</tr>-->
		
    		<tr>
    			<td class="tabletext"><?= $linterface->MandatoryField();?> </td>
    		</tr>
    		<tr>
    			<td>
    			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
    				<tr>
    					<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    				</tr>
    			</table>
    		 	</td>
    		</tr>
    		<tr>
        	  	<td align="center" colspan="2">
        			<?= $linterface->GET_ACTION_BTN($button_continue, "submit") ?>&nbsp;
        			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='index.php'") ?>
        		</td>
        	 </tr>
    		</table>
		</td>
	</tr>
</table>

<?=$layer_html?>
<input type="hidden" id="task" name="task"/>
</form>
<br />

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>