<?php
// Modifying by: 

########## Change Log ###############
#
#	Date	:	2020-11-13 (Bill)	[2020-1009-1753-46096]
# 				delay notice start date & end date		($sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'])
#
#	Date	:	2017-10-11	(Bill)	[2017-1011-1143-48235]
#				Suspend Notice if not approved conduct
#
#	Date	:	2017-03-02	(Bill)	[2016-0823-1255-04240]
#				default not distribute notice to parent	($sys_custom['eDiscipline']['NotReleaseNotice'])
#
#	Date	:	2015-04-30 (Bill)	[2014-1216-1347-28164]
#				send out notification if $eClassAppNotify = 1
#				update DB after send out push message
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");

intranet_auth();
intranet_opendb();

$lnotice = new libnotice();
$ldiscipline = new libdisciplinev12();
	
$template_info = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO($SelectNotice);
if(sizeof($template_info) && isset($SelectNotice) && $SelectNotice!="")
{
	$SpecialData = array();
	$SpecialData['ConductRecordID'] = $RecordID;
	if($sys_custom['eDisciplinev12_eNoticeTemplate_AccuNumber'])
		$SpecialData['AccuNumber'] = $ldiscipline->RETURN_ACCUMULATED_CONDUCT_NUMBER_FOR_ENOTICE_TEMPLATE($StudentID, $GMIDArr[$StudentID], $CatArr[$StudentID]);
	
	$includeReferenceNo = 1;
	$template_data = $ldiscipline->TEMPLATE_VARIABLE_CONVERSION($CategoryID, $StudentID, $SpecialData, $TextAdditionalInfo, $includeReferenceNo);
	$UserName = $ldiscipline->getUserNameByID($StudentID);
	
	$Module = $ldiscipline->Module;
	$NoticeNumber = time();
	$TemplateID = $SelectNotice;
	$Variable = $template_data;
	$IssueUserID = $UserID;
	$IssueUserName = $UserName[0];
	$TargetRecipientType = "U";
	$RecipientID = array($StudentID);
	$RecordType = NOTICE_TO_SOME_STUDENTS;
	
	// [2014-1216-1347-28164] Send eNotice
	if(!$isMsg)
	{
		if (!$lnotice->disabled)
		{
			include_once($PATH_WRT_ROOT."includes/libucc.php");
			$lc = new libucc();
			$ReturnArr = $lc->setNoticeParameter($Module, $NoticeNumber, $TemplateID, $Variable, $IssueUserID, $IssueUserName, $TargetRecipientType, $RecipientID, $RecordType);

            // [2020-1009-1753-46096] delay notice start date & end date
            if (isset($sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay']) && $sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'] > 0)
            {
                $delayedHrs = $sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'];

                $DateStart = date('Y-m-d H:i:s', strtotime("+".$delayedHrs." hours"));
                $lc->setDateStart($DateStart);

                if ($lc->defaultDisNumDays > 0) {
                    $DateEndTs = time() + ($lc->defaultDisNumDays * 24 * 60 * 60) + ($delayedHrs * 60 * 60);
                    $DateEnd = date('Y-m-d', $DateEndTs).' 23:59:59';
                    $lc->setDateEnd($DateEnd);
                }
            }

            if(!isset($emailNotify) || $emailNotify=="") $emailNotify = 0;
			$NoticeID = $lc->sendNotice($emailNotify);
		}
		
		if($NoticeID != -1)
		{
			# Update Conduct Record
			$sql = "UPDATE DISCIPLINE_ACCU_RECORD SET NoticeID=$NoticeID WHERE RecordID=$RecordID";
			$ldiscipline->db_db_query($sql);
			
			// [2016-0823-1255-04240]
			if($sys_custom['eDiscipline']['NotReleaseNotice'])
			{
				$lnotice->changeNoticeStatus($NoticeID, SUSPENDED_NOTICE_RECORD);
			}
			// [2017-1011-1143-48235] Suspend Notice if not approved conduct
			else
			{
				$RecordInfo = $ldiscipline->RETRIEVE_CONDUCT_INFO($RecordID);
				$RecordStatus = $RecordInfo[0]["RecordStatus"];
				if($RecordStatus != DISCIPLINE_STATUS_APPROVED) {
					$lnotice->changeNoticeStatus($NoticeID, SUSPENDED_NOTICE_RECORD);
				}
			}
		}
	}
	// [2014-1216-1347-28164] eClass App - Send Notification
	else
	{
		if($plugin['eClassApp'] && $sys_custom['eDiscipline']['SendDetentionPushMessage'])
		{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
			include_once($PATH_WRT_ROOT."includes/libucc.php");
			
			$luser = new libuser();
			$leClassApp = new libeClassApp();
			
			$lc = new libucc();
			$ReturnArr = $lc->setNoticeParameter($Module, $NoticeNumber, $TemplateID, $Variable, $IssueUserID, $IssueUserName, $TargetRecipientType, $RecipientID, $RecordType);
			
			// Structure data for Sending Message
			$lc->restructureData();
		
			// Get ParentID and StudentID Mapping
			$sql = "SELECT 
							ip.ParentID, ip.StudentID
					from 
							INTRANET_PARENTRELATION AS ip 
							Inner Join INTRANET_USER AS iu ON (ip.ParentID=iu.UserID) 
					WHERE 
							ip.StudentID IN ('".implode("','", (array)$RecipientID)."') 
							AND iu.RecordStatus = 1";
			$parentStudentAssoAry = BuildMultiKeyAssoc($lnotice->returnResultSet($sql), 'ParentID', array('StudentID'), $SingleValue=1, $BuildNumericArray=1);
			
			// Push Message Data
			$messageInfoAry = array();
			$messageInfoAry[0]['relatedUserIdAssoAry'] = $parentStudentAssoAry;
			$messageContent = removeHTMLtags($lc->Description);
			$messageContent = intranet_undo_htmlspecialchars($messageContent);
			$messageContent = str_replace("&nbsp;", "", $messageContent);
			$notifyMessageId = $leClassApp->sendPushMessage($messageInfoAry, $lc->Subject, $messageContent);
			
			// [2014-1216-1347-28164] Update DB if $notifyMessageId valid
			if($notifyMessageId){
				$sql = "UPDATE DISCIPLINE_ACCU_RECORD SET PushMessageID='$notifyMessageId' WHERE RecordID='$RecordID'";
				$ldiscipline->db_db_query($sql);
			}
		}
	}
}

intranet_closedb();

//$url = "edit.php?id=$RecordID&msg=update&num_per_page=$num_per_page&pageNo=$pageNo&order=$order&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&s=$s&clickID=$clickID&fromPPC=$fromPPC&passedActionDueDate=$passedActionDueDate&SchoolYear2=$SchoolYear2&semester2=$semester2&targetClass2=$targetClass2&MeritType2=$MeritType2";
$url = "index.php?msg=update";

header("Location: $url");
?>