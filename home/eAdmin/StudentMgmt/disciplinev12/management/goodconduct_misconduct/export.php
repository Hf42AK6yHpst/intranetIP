<?php
// Editing by Bill

############ Change Log Start ############################
#
#	Date	:	2017-11-01	(Bill)	[2017-0403-1552-54240]
#				Hide Remarks if not HOY Group Member	($sys_custom['eDiscipline']['HideAllGMReport'])
#
#	Date	:	2017-10-31 (Bill)	[2017-0403-1552-54240]
#				Support HKUGA Cust - only allow HOY and Class Teacher to view Conduct Records ($sys_custom['eDiscipline']['HOY_Access_GM'])
#
#	Date	:	2017-03-15 (Anna)	[2017-0309-1027-13206]
#				added user login column ($sys_custom['eDiscipline']['ExportRecordWithUserLogin'])			
#
#	Date	:	2016-04-18 (Bill)	[DM#2973]
#				allow export name of deleted teacher account
#
#	Date	:	2016-04-11 (Bill)
#				Replace deprecated split() by explode() for PHP 5.4
#
#	Date	:	2014-09-19 (YatWoon)
#				Fixed: incorrect search result if same ItemID but diff CategoryID (homework and others) > $check_cat [Case#P68212]
#				deploy: IPv10.1
#
# 	Date	:	2014-05-29 Carlos
#				$sys_custom['eDiscipline']['PooiToMiddleSchool'] - add Score change data column
#
#	Date	:	2010-09-24 Henry Chow
#				handle "Form" in targetClass
#
############ Change Log End ##############################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$lexport = new libexporttext();

$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-View");

$ExportArr = array();

ini_set("memory_limit", "800M"); 

$ArrayUpperLimit = 10000;

// replace split() by explode() - for PHP 5.4
//list($start, $end) = split('-',$SchoolYear);
list($start, $end) = explode('-',$SchoolYear);

# School Year Condition #
if($SchoolYear != '' && $SchoolYear != 0) {
	$conds .= " AND a.AcademicYearID = $SchoolYear";
}

if($semester != '' && $semester != 'WholeYear') {
	$conds .= " AND a.YearTermID = $semester";
}

if ($targetClass != '' && $targetClass!="0")
{
    //$conds .= " AND yc.YearClassID = $targetClass";
    if(is_numeric($targetClass)) {
    	$sql = "SELECT YearClassID FROM YEAR_CLASS WHERE YearID='$targetClass'";
    	$temp = $ldiscipline->returnVector($sql);
    	$conds .= (sizeof($temp)>0) ? " AND yc.YearClassID IN (".implode(',', $temp).")" : "";
	}
    else {
    	$conds .= " AND yc.YearClassID=".substr($targetClass,2);
    }
}

if ($RecordType != '' && $RecordType != 0) {
    $conds .= " AND a.RecordType = $RecordType";
}
else {
	$RecordType = 0;	
}

if($pic != "") {
	$conds .= " AND concat(',',a.PICID,',') LIKE '%,$pic,%'";
}

if($sDate!="" && $eDate!="") {
	$conds .= " AND a.RecordDate BETWEEN '$sDate' AND '$eDate'";
}

// [2017-0403-1552-54240] HKUGA Handling
// 1. HOY Members: View all GM
// 2. Class Teacher: View students' GM only
if($sys_custom['eDiscipline']['HOY_Access_GM'] && !$ldiscipline->IS_HOY_GROUP_MEMBER())
{
	$ClassStudentAry = $ldiscipline->getStudentListByClassTeacherID("", $UserID, $currentYearID);
	$ClassStudentAry = array_unique($ClassStudentAry);
	if(sizeof($ClassStudentAry) > 0) {
		$ClassStudentStr = implode("', '", $ClassStudentAry);
	}
	else { 
		$ClassStudentStr = "";
	}
	$conds .= " AND a.StudentID IN ('".$ClassStudentStr."') ";
}

$conds2 = "";

# Waiting for Approval #
if($waitApproval == 1) {
	$waitApprovalChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.RecordStatus = ".DISCIPLINE_STATUS_PENDING;	
}

# Approved #  	// released record also be approved
if($approved == 1) {
	$approvedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.RecordStatus = ".DISCIPLINE_STATUS_APPROVED;
}

# Rejected #
if($rejected == 1) {
	$rejectedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.RecordStatus = ".DISCIPLINE_STATUS_REJECTED;	
}

# Waived #
if($waived == 1) {
	$waivedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.RecordStatus = ".DISCIPLINE_STATUS_WAIVED;	
}

$conds2 .= ($conds2 != "") ? ")" : "";

$filename = "good_conduct_misconduct.csv";	

$currentYearInfo = Get_Current_Academic_Year_ID();

# SQL Statement
$student_namefield = getNamefieldByLang("b.");
$PICID_namefield = getNameFieldWithLoginByLang("d.");
$clsName = ($intranet_session_language=="en") ? "yc.ClassTitleEN" : "yc.ClassTitleB5";
$sbjName = ($intranet_session_language=="en") ? "SUB.EN_DES" : "SUB.CH_DES";

$use_intranet_homework = $ldiscipline->accumulativeUseIntranetSubjectList();

if($fromPPC==1)
{
	$result = $ldiscipline->retrieveGMRecordFromPPC();
	$GMRecordIDFromPPC = implode(',', $result);
	
		$sql = "SELECT 
					$clsName as Class, 
					$student_namefield as std_name,
					IF(a.RecordType=1,'$i_Discipline_GoodConduct','$i_Discipline_Misconduct') as conductImg,";
		if($use_intranet_homework){
			$sql .= " IF(a.CategoryID = 2, CONCAT(c.Name,' - ',$sbjName), IF(d.Name IS NULL, c.Name, CONCAT(c.Name,' - ',d.Name))) as itemName,";
		}
		else {
			$sql .= " IF(d.Name IS NULL, c.Name, CONCAT(c.Name,' - ',d.Name)) as itemName,";
		}
		$sql .= "	
					LEFT(a.RecordDate,10) as RecordDate,
					PICID,
					f.CategoryID as WaivedDate,
					LEFT(a.DateModified,10) as DateModified,
					CONCAT(
						IF(a.RecordStatus=".DISCIPLINE_STATUS_PENDING.",'$i_status_pending',''),
						IF(a.RecordStatus=".DISCIPLINE_STATUS_REJECTED.",'$i_Discipline_System_Award_Punishment_Rejected',''),
						IF(a.RecordStatus=".DISCIPLINE_STATUS_APPROVED.",'".$eEnrollment['approved']."',''),
						IF(a.RecordStatus=".DISCIPLINE_STATUS_WAIVED.",'$i_Discipline_System_Award_Punishment_Waived','')
					) as status,
					CONCAT('<input type=checkbox name=RecordID[] value=', a.RecordID ,'>'),
					a.RecordStatus,
					a.RecordID,
					b.UserID,
					ycu.ClassNumber,
					a.Remark,
					a.GMCount ";
		if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
			$sql .= ",IF(a.ScoreChange=0,'".$Lang['General']['EmptySymbol']."',a.RecordType*a.ScoreChange) as ScoreChange ";
		}
		if($sys_custom['eDiscipline']['HOY_Access_GM']){
			$sql .= ",a.ApplyHOY as isApplyHOY ";
		}
		$sql .= "	,b.UserLogin as stu_login
				FROM DISCIPLINE_ACCU_RECORD as a
					INNER JOIN INTRANET_USER as b ON (a.StudentID = b.UserID)
					LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY as c ON (a.CategoryID = c.CategoryID)
					LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY_ITEM as d ON (a.ItemID = d.ItemID)
					LEFT OUTER JOIN DISCIPLINE_NEW_LEAF_SCHEME as f ON (f.CategoryID = a.CategoryID)
					LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY_PERIOD as g ON (a.CategoryID=g.CategoryID)
					LEFT OUTER JOIN DISCIPLINE_ACCU_PERIOD as h ON (f.PeriodID=h.PeriodID)
					LEFT OUTER JOIN YEAR_CLASS_USER as ycu ON (b.UserID=ycu.UserID)
					LEFT OUTER JOIN YEAR_CLASS as yc ON (ycu.YearClassID=yc.YearClassID)
				";
	if($use_intranet_homework){
		//$sql .= " LEFT OUTER JOIN INTRANET_SUBJECT as e ON (a.ItemID = e.SubjectID)";
		$sql .= " LEFT OUTER JOIN ASSESSMENT_SUBJECT as SUB ON (a.ItemID = SUB.RecordID)";
	}
	
	$sql .=	($GMRecordIDFromPPC) ? " WHERE a.RecordID IN ($GMRecordIDFromPPC)" : " WHERE a.RecordDate='9999-99-99'";
	$sql .= " AND yc.AcademicYearID=$currentYearInfo AND b.RecordStatus IN (0,1,2)";
	$sql .= $conds;
	$sql .= ($conds2 != "") ? " AND ".$conds2 : "";
	$sql .= " GROUP BY a.RecordID";
}
else if($newleaf==1)
{
		$result = $ldiscipline->GMWaitForNewLeafCount();
		$waitForNewLeafID = implode(',', $result);
		
		$sql = "SELECT 
					$clsName as Class, 
					$student_namefield as std_name,
					IF(a.RecordType=1,'$i_Discipline_GoodConduct','$i_Discipline_Misconduct') as conductImg,";
		if($use_intranet_homework){
			$sql .= " IF(a.CategoryID = 2, CONCAT(c.Name,' - ',$sbjName), IF(d.Name IS NULL, c.Name, CONCAT(c.Name,' - ',d.Name))) as itemName,";
		}
		else {
			$sql .= " IF(d.Name IS NULL, c.Name, CONCAT(c.Name,' - ',d.Name)) as itemName,";
		}
		$sql .= "	
					LEFT(a.RecordDate,10) as RecordDate,
					PICID,
					f.CategoryID as WaivedDate,
					LEFT(a.DateModified,10) as DateModified,
					CONCAT(
						IF(a.RecordStatus=".DISCIPLINE_STATUS_PENDING.",'$i_status_pending',''),
						IF(a.RecordStatus=".DISCIPLINE_STATUS_REJECTED.",'$i_Discipline_System_Award_Punishment_Rejected',''),
						IF(a.RecordStatus=".DISCIPLINE_STATUS_APPROVED.",'".$eEnrollment['approved']."',''),
						IF(a.RecordStatus=".DISCIPLINE_STATUS_WAIVED.",'$i_Discipline_System_Award_Punishment_Waived','')
					) as status,
					CONCAT('<input type=checkbox name=RecordID[] value=', a.RecordID ,'>'),
					a.RecordStatus,
					a.RecordID,
					b.UserID,
					ycu.ClassNumber,
					a.Remark,
					LEFT(DATE_ADD(a.RecordDate, INTERVAL f.WaiveDay DAY),10) as waivePassDate,
					a.GMCount ";
		if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
			$sql .= ",IF(a.ScoreChange=0,'".$Lang['General']['EmptySymbol']."',a.RecordType*a.ScoreChange) as ScoreChange ";
		}
		if($sys_custom['eDiscipline']['HOY_Access_GM']){
			$sql .= ",a.ApplyHOY as isApplyHOY ";
		}
		$sql .= "	,b.UserLogin as stu_login
				FROM DISCIPLINE_ACCU_RECORD as a
					INNER JOIN INTRANET_USER as b ON (a.StudentID = b.UserID)
					LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY as c ON (a.CategoryID = c.CategoryID)
					LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY_ITEM as d ON (a.ItemID = d.ItemID)
					LEFT OUTER JOIN DISCIPLINE_NEW_LEAF_SCHEME as f ON (f.CategoryID = a.CategoryID)
					LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY_PERIOD as g ON (a.CategoryID=g.CategoryID)
					LEFT OUTER JOIN DISCIPLINE_ACCU_PERIOD as h ON (f.PeriodID=h.PeriodID)
					LEFT OUTER JOIN YEAR_CLASS_USER as ycu ON (b.UserID=ycu.UserID)
					LEFT OUTER JOIN YEAR_CLASS as yc ON (ycu.YearClassID=yc.YearClassID)
				";
	if($use_intranet_homework){
		//$sql .= " LEFT OUTER JOIN INTRANET_SUBJECT as e ON (a.ItemID = e.SubjectID)";
		$sql .= " LEFT OUTER JOIN ASSESSMENT_SUBJECT as SUB ON (a.ItemID = SUB.RecordID)";
	}
	
	$sql .=	($waitForNewLeafID) ? " WHERE a.RecordID IN ($waitForNewLeafID)" : " WHERE a.RecordDate='9999-99-99'";
	$sql .= " AND yc.AcademicYearID=$currentYearInfo AND b.RecordStatus IN (0,1,2)";
	$sql .= $conds;
	$sql .= ($conds2 != "") ? " AND ".$conds2 : "";

	//$sql .= " GROUP BY a.RecordID ORDER BY ClassName, ClassNumber";
	$sql .= " GROUP BY a.RecordID ORDER BY $clsName, ycu.ClassNumber";
}
else
{
	/*
	if($s != "") {		# filter by Searching
		$s = addslashes(trim($s));
		
		$sql = "SELECT ";
		//				CONCAT($clsName,' - ',ycu.ClassNumber) as ClassNameNum,
		$sql .= " $clsName, ";
		$sql .= "
						$student_namefield,
						IF(a.RecordType=1,'$i_Discipline_GoodConduct','$i_Discipline_Misconduct') as conductImg,";
		if($use_intranet_homework){
			$sql .= " IF(a.CategoryID = 2, CONCAT(c.Name,' - ',$sbjName), IF(d.Name IS NULL, c.Name, CONCAT(c.Name,' - ',d.Name))) as itemName,";
		} else {
			$sql .= " IF(d.Name IS NULL, c.Name, CONCAT(c.Name,' - ',d.Name)) as itemName,";
		}
		$sql .= "	LEFT(a.RecordDate,10) as RecordDate,
						PICID,
						f.CategoryID as WaivedDate,
						LEFT(a.DateModified,10) as DateModified,
						CONCAT(
							IF(a.RecordStatus=".DISCIPLINE_STATUS_PENDING.",'$i_status_pending',''),
							IF(a.RecordStatus=".DISCIPLINE_STATUS_REJECTED.",'$i_Discipline_System_Award_Punishment_Rejected',''),
							IF(a.RecordStatus=".DISCIPLINE_STATUS_APPROVED.",'".$eEnrollment['approved']."',''),
							IF(a.RecordStatus=".DISCIPLINE_STATUS_WAIVED.",'$i_Discipline_System_Award_Punishment_Waived','')
						) as status,
						CONCAT('<input type=checkbox name=RecordID[] value=', a.RecordID ,'>'),
						a.RecordStatus,
						a.RecordID,
						b.UserID,
						ycu.ClassNumber
		
					FROM DISCIPLINE_ACCU_RECORD as a
						LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
						LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY as c ON a.CategoryID = c.CategoryID
						LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY_ITEM as d ON (a.ItemID = d.ItemID)
						LEFT OUTER JOIN DISCIPLINE_NEW_LEAF_SCHEME as f ON (c.CategoryID = f.CategoryID)
						LEFT OUTER JOIN YEAR_CLASS_USER as ycu ON (b.UserID=ycu.UserID)
						LEFT OUTER JOIN YEAR_CLASS as yc ON (ycu.YearClassID=yc.YearClassID)
						";
		if($use_intranet_homework){
			//$sql .= " LEFT OUTER JOIN INTRANET_SUBJECT as e ON (a.ItemID = e.SubjectID) ";
			$sql .= " LEFT OUTER JOIN ASSESSMENT_SUBJECT as SUB ON (a.ItemID = SUB.RecordID)";
		}
		$sql .=	"WHERE a.DateInput IS NOT NULL AND yc.AcademicYearID=$currentYearInfo
							AND (
								$clsName like '%$s%'
								OR ycu.ClassNumber like '%$s%'
								OR $student_namefield like '%$s%'
								OR c.Name like '%$s%'
								OR d.Name like '%$s%'
								OR a.RecordDate like '%$s%'
							)
		
	}
	else {				# filter by option";*/
		$sql = "SELECT 
					$clsName as Class,
					$student_namefield as std_name,
					IF(a.RecordType=1,'$i_Discipline_GoodConduct','$i_Discipline_Misconduct') as conductImg,";
		if($use_intranet_homework){
			$sql .= " IF(a.CategoryID = 2, CONCAT(c.Name,' - ',$sbjName), IF(d.Name IS NULL, c.Name, CONCAT(c.Name,' - ',d.Name))) as itemName,";
			$check_cat = " and a.CategoryID !=2 ";
		}
		else {
			$sql .= " IF(d.Name IS NULL, c.Name, CONCAT(c.Name,' - ',d.Name)) as itemName,";
			$check_cat = "";
		}
		$sql .= "
					LEFT(a.RecordDate,10) as RecordDate,
					PICID,
					f.CategoryID as WaivedDate,
					LEFT(a.DateModified,10) as DateModified,
					CONCAT(
						IF(a.RecordStatus=".DISCIPLINE_STATUS_PENDING.",'$i_status_pending',''),
						IF(a.RecordStatus=".DISCIPLINE_STATUS_REJECTED.",'$i_Discipline_System_Award_Punishment_Rejected',''),
						IF(a.RecordStatus=".DISCIPLINE_STATUS_APPROVED.",'".$eEnrollment['approved']."',''),
						IF(a.RecordStatus=".DISCIPLINE_STATUS_WAIVED.",'$i_Discipline_System_Award_Punishment_Waived','')
					) as status,
					CONCAT('<input type=checkbox name=RecordID[] value=', a.RecordID ,'>'),
					a.RecordStatus,
					a.RecordID,
					b.UserID,
					ycu.ClassNumber,
					a.Remark,
					a.GMCount ";
		if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
			$sql .= ",IF(a.ScoreChange=0,'".$Lang['General']['EmptySymbol']."',a.RecordType*a.ScoreChange) as ScoreChange ";
		}
		if($sys_custom['eDiscipline']['HOY_Access_GM']){
			$sql .= ",a.ApplyHOY as isApplyHOY ";
		}
		$sql .= "	,b.UserLogin as stu_login
				FROM DISCIPLINE_ACCU_RECORD as a
					INNER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
					LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY as c ON a.CategoryID = c.CategoryID
					LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY_ITEM as d ON (a.ItemID = d.ItemID $check_cat)
					LEFT OUTER JOIN DISCIPLINE_NEW_LEAF_SCHEME as f ON (c.CategoryID = f.CategoryID)
					LEFT OUTER JOIN YEAR_CLASS_USER as ycu ON (b.UserID=ycu.UserID)
					LEFT OUTER JOIN YEAR_CLASS as yc ON (ycu.YearClassID=yc.YearClassID)
					";
		if($use_intranet_homework){
			//$sql .= " LEFT OUTER JOIN INTRANET_SUBJECT as e ON (a.ItemID = e.SubjectID) ";
			$sql .= " LEFT OUTER JOIN ASSESSMENT_SUBJECT as SUB ON (a.ItemID = SUB.RecordID)";
		}
		$sql .=	"WHERE a.DateInput IS NOT NULL AND yc.AcademicYearID=$currentYearInfo AND b.RecordStatus IN (0,1,2)
					$conds
							AND (
								$clsName like '%$s%'
								OR ycu.ClassNumber like '%$s%'
								OR $student_namefield like '%$s%'
								OR c.Name like '%$s%'
								OR d.Name like '%$s%'
								OR a.RecordDate like '%$s%'
							)					
			";
	$sql .= ($conds2 != "") ? " AND ".$conds2 : "";
	$sql .= " GROUP BY a.RecordID";
	
	//$sql .= " order by $clsName, ycu.ClassNumber";
	$sql .= " ORDER BY a.RecordDate DESC, a.RecordID DESC";
}

$row = $ldiscipline->returnArray($sql,14);
$noOfRecord = count($row);

// Create data array for export
//$thisMax = $noOfRecord < $ArrayUpperLimit ? $noOfRecord : $ArrayUpperLimit;
$noOfLoop = ceil($noOfRecord / $ArrayUpperLimit);
for($a=0; $a<$noOfLoop; $a++)
{
	$thisMin = $a * $ArrayUpperLimit;
	$thisMax = $noOfRecord<$thisMin + $ArrayUpperLimit ? $noOfRecord : $thisMin + $ArrayUpperLimit;
	//echo $thisMin.'/'.$thisMax.'<br>'; continue;
	
	for($i=$thisMin; $i<$thisMax; $i++)
	{
		//list($thisClass, $thisStdName, $thisConductImg, $thisItemName, $thisRecordDate, $thisPic, $thisWaivedDate, $thisDateModified, $thisStatus, $thisCheckbox, $thisRS, $thisRecordID, $thisUserID, $thisClassNo, $thisRemark, $thisGmCount) = $row[$i];
		
		$m = 0;
		$ExportArr[$i][$m] = $row[$i]['Class'];					// Class
		$m++;
		$ExportArr[$i][$m] = $row[$i]['ClassNumber'];			// Class Number
		$m++;
		$ExportArr[$i][$m] = $row[$i]['std_name'];				// Student Name
		$m++;
		if($sys_custom['eDiscipline']['ExportRecordWithUserLogin']){
			$ExportArr[$i][$m] = $row[$i]['stu_login'];			// Student User Login
			$m++;
		}
		$ExportArr[$i][$m] = $row[$i]['conductImg'];			// Record Type
		$m++;
		$ExportArr[$i][$m] = $row[$i]['GMCount'];				// Receive Times
		$m++;
		$ExportArr[$i][$m] = $row[$i]['itemName'];				// Item Name
		$m++;
		if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
			$ExportArr[$i][$m] = $row[$i]['ScoreChange']; 		// Score
			$m++;
		}
		$ExportArr[$i][$m] = $row[$i]['Remark'];				// Remark
		if($sys_custom['eDiscipline']['HOY_Access_GM'] && $row[$i]['isApplyHOY'] && !$ldiscipline->IS_HOY_GROUP_MEMBER()) {
			$ExportArr[$i][$m] = $Lang['eDiscipline']['DetailsReferToHOY'];
		}
		$m++;
		$ExportArr[$i][$m] = $row[$i]['RecordDate'];			// Event Date
		$m++;
		
		$name_field = getNameFieldByLang();						// PIC
		if($row[$i][5] != '') {
			//$ary = split(',', $row[$i][5]);
			$pic = $row[$i][5];
	
			$sql2 = "SELECT $name_field FROM INTRANET_USER WHERE UserID IN ($pic)";
			$temp = $ldiscipline->returnArray($sql2,1);
	
			// [DM#2973] INTRANET_ARCHIVE_USER
			$sql2 = "SELECT CONCAT('*', $name_field) FROM INTRANET_ARCHIVE_USER WHERE UserID IN ($pic)";
			$temp2 = $ldiscipline->returnArray($sql2,1);
			
			$temp = array_merge($temp, $temp2);
			if(sizeof($temp)!=0) {
		    	for($k=0; $k<sizeof($temp); $k++) {
		        	$ExportArr[$i][$m] .= $temp[$k][0];
		        	$ExportArr[$i][$m] .= ($k != (sizeof($temp)-1)) ? ", " : "";
		    	}
			}
			else {
				$ExportArr[$i][$m] .= "---";
			}
		}
		else {
			$ExportArr[$i][$m] .= "---";	
		}
		$m++;
		
		if($newleaf==1) {
			if($row[$i][6] != '') {								// Waive day
		    	$catid = $row[$i][6];
				$ExportArr[$i][$m] = $ldiscipline->countWaivePassedDate($catid, $row[$i][4], $row[$i][12], $row[$i][11]);
			}
			else {
				$ExportArr[$i][$m] = "---";
			}
			$m++;
		}
		
		$ExportArr[$i][$m] = $row[$i][7]; 						// Last Update
		$m++;
		$ExportArr[$i][$m] = $row[$i][8]; 						// Status
		$m++;
	}
}

if(sizeof($row)==0) {
	$ExportArr[0][0] = $i_no_record_exists_msg;	
}

// Define Column Title
/*
if($newleaf==1) {
	$exportColumn = array($i_ClassName, $i_ClassNumber, $i_general_name, $i_Merit_Type, $Lang['eDiscipline']['ReceiveTimes'], $iDiscipline['Accumulative_Category_Item'], $iDiscipline['Remark'], $Lang['eDiscipline']['EventDate'], $i_Profile_PersonInCharge, $i_Discipline_System_GoodConduct_Misconduct_Waive_Date_Count, $i_Discipline_Last_Updated, $i_Discipline_System_Discipline_Status);	
} else {
	$exportColumn = array($i_ClassName, $i_ClassNumber, $i_general_name, $i_Merit_Type, $Lang['eDiscipline']['ReceiveTimes'], $iDiscipline['Accumulative_Category_Item'], $iDiscipline['Remark'], $Lang['eDiscipline']['EventDate'], $i_Profile_PersonInCharge, $i_Discipline_Last_Updated, $i_Discipline_System_Discipline_Status);
}
*/
$exportColumn = array($i_ClassName, $i_ClassNumber, $i_general_name);
if($sys_custom['eDiscipline']['ExportRecordWithUserLogin']){
	$exportColumn = array_merge($exportColumn, array($Lang['eDiscipline']['UserLogin']));
}
$exportColumn = array_merge($exportColumn,array($i_Merit_Type, $Lang['eDiscipline']['ReceiveTimes'], $iDiscipline['Accumulative_Category_Item']));
if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
	$exportColumn = array_merge($exportColumn,array($Lang['eDiscipline']['Score']));
}
$exportColumn = array_merge($exportColumn,array($iDiscipline['Remark'], $Lang['eDiscipline']['EventDate'], $i_Profile_PersonInCharge));
if($newleaf==1) {
	$exportColumn = array_merge($exportColumn,array($i_Discipline_System_GoodConduct_Misconduct_Waive_Date_Count));
}
$exportColumn = array_merge($exportColumn,array($i_Discipline_Last_Updated, $i_Discipline_System_Discipline_Status));

$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

// Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);
?>