<?php
# using: yat
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();


$ldiscipline = new libdisciplinev12();
$lexport = new libexporttext();

$ExportArr = array();

list($start, $end) = split('-',$SchoolYear);
# School Year Condition #

if($SchoolYear != '' && $SchoolYear != 0) {
	$conds .= " AND a.AcademicYearID = $SchoolYear";
}

if($semester != '' && $semester != 'WholeYear') {
	$conds .= " AND a.YearTermID = $semester";
}

if ($targetClass != '')
{
    $conds .= " AND yc.YearClassID = $targetClass";
}

if ($RecordType != '' && $RecordType != 0)
{
    $conds .= " AND a.RecordType = $RecordType";
} else {
	$RecordType = 0;	
}

$conds2 = "";

# Waiting for Approval #
if($waitApproval == 1) {
	$waitApprovalChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.RecordStatus = ".DISCIPLINE_STATUS_PENDING;	
}

# Approved #  // released record also be approved
if($approved == 1) {
	$approvedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.RecordStatus = ".DISCIPLINE_STATUS_APPROVED;
}

# Rejected #
if($rejected == 1) {
	$rejectedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.RecordStatus = ".DISCIPLINE_STATUS_REJECTED;	
}

# Waived #
if($waived == 1) {
	$waivedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.RecordStatus = ".DISCIPLINE_STATUS_WAIVED;	
}

$conds2 .= ($conds2 != "") ? ")" : "";

$filename = "good_conduct_misconduct_wsc.csv";	

# SQL Statement
$clsName = ($intranet_session_language=="en") ? "yc.ClassTitleEN" : "yc.ClassTitleB5";
$sbjName = ($intranet_session_language=="en") ? "SUB.EN_DES" : "SUB.CH_DES";


		$sql = "SELECT
				b.ClassName,
				b.ClassNumber,";
// 				b.EnglishName,
// 				b.UserID,";
// 		$sql .= " CONCAT(c.Name,' - ',d.Name) as itemName,";
	$sql .= "	
					LEFT(a.RecordDate,10) as RecordDate,
					d.ItemCode,
					p.UserLogin,
					a.Remark
				FROM DISCIPLINE_ACCU_RECORD as a
					LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
					LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY as c ON a.CategoryID = c.CategoryID
					LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY_ITEM as d ON (a.ItemID = d.ItemID)
					LEFT OUTER JOIN INTRANET_USER as p ON a.PICID = p.UserID
					";
	$sql .=	"WHERE a.DateInput IS NOT NULL
					$conds
			";
	$sql .= ($conds2 != "") ? " AND ".$conds2 : "";
	$sql .= " GROUP BY a.RecordID";
	$sql .= " order by a.RecordDate desc";
// 	$sql .= " order by $clsName, ycu.ClassNumber";
		
	
// debug_pr($sql);
//echo $sql;

$row = $ldiscipline->returnArray($sql,14);
// debug_pr($row);

// exit;

// Create data array for export
for($i=0; $i<sizeof($row); $i++){
	$m = 0;
	$ExportArr[$i][$m] = $row[$i][0];		//Class Name
	$m++;
	$ExportArr[$i][$m] = $row[$i][1];		//Class Number
	$m++;
	$ExportArr[$i][$m] = $row[$i][2];		//Record Date
	$m++;
	$ExportArr[$i][$m] = $row[$i][3];		//Item Code				
	$m++;
	$ExportArr[$i][$m] = $row[$i][4];		//PIC		
	$m++;
	$ExportArr[$i][$m] = $row[$i][5];		//Remark		
	$m++;
	
}


//define column title
$exportColumn = array("Class Name", "Class Number", "Event Date", "Item Code", "PIC", "Remark");

$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

// Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);

?>
