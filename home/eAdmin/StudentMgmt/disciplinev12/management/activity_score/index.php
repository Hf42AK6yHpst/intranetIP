<?php 
# using: 

#############################################
#
#	Date    :	2019-05-13 (Bill)
#               Prevent SQL Injection + Cross-Site Scripting
#
#	Date	:	2017-10-04	(Bill)
#				Fixed: Whole Year > Score Change Layer show selected student punishment only
#
#	Date	:	2017-03-17	(Bill)	[2016-1207-1221-39240]
#				create File
#
#############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

# Change "num per page"
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# Preserve table view
if ($ck_approval_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_approval_page_number", $pageNo, 0, "", "", 0);
	$ck_approval_page_number = $pageNo;
}
else if (!isset($pageNo) && $ck_approval_page_number!="")
{
	$pageNo = $ck_approval_page_number;
}
if ($ck_approval_page_order!=$order && $order!="")
{
	setcookie("ck_approval_page_order", $order, 0, "", "", 0);
	$ck_approval_page_order = $order;
}
else if (!isset($order) && $ck_approval_page_order!="")
{
	$order = $ck_approval_page_order;
}
if ($ck_approval_page_field!=$field && $field!="")
{
	setcookie("ck_approval_page_field", $field, 0, "", "", 0);
	$ck_approval_page_field = $field;
}
else if (!isset($field) || $ck_approval_page_field == "")
{
	$field = $sortEventDate;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

### Handle SQL Injection + XSS [START]
$Year = IntegerSafe($Year);
$Semester = IntegerSafe($Semester);
$ClassID = IntegerSafe($ClassID);
### Handle SQL Injection + XSS [END]

$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Conduct_Mark-View");

# Year Info for Year Selection
$yearInfo = GetAllAcademicYearInfo();
$yearArr = array();
for($i=0; $i<sizeof($yearInfo); $i++) {
	list($year_id, $yearEN, $yearB5) = $yearInfo[$i];
	$yearArr[$i][] = $year_id;
	$yearArr[$i][] = Get_Lang_Selection($yearB5, $yearEN);
}

# Target Year
if($Year == "")
{
	$Year = Get_Current_Academic_Year_ID();
}
$AcademicYearID = $Year;

# Activity Score Calculation Method
$ActivityScoreCalculationMethod = $ldiscipline->retriveActivityScoreCalculationMethod();

# Semester Info for Semester Selection
$semester_data = getSemesters($AcademicYearID);
$number = sizeof($semester_data);
if($ActivityScoreCalculationMethod==0) {
	// Calculate by Term
	$semArr[] = array("", $ec_iPortfolio['whole_year']);
}
foreach($semester_data as $id => $name) {
	$semArr[] = array($id, $name);
}

# Target Semester
$YearTermID = $Semester;
if($YearTermID == "" && $YearTermID != "0") {
	$YearTermID = 0;
}

# Year & Semester Selection
$yearSelection = $linterface->GET_SELECTION_BOX($yearArr, "id='Year' name='Year' onChange='if(this.selectedIndex>0) { document.form1.Semester.value=\"\"; document.form1.ClassID.value=\"\"; document.form1.submit(); }'", $Lang['eDiscipline']['FieldTitle']['SelectSchoolYear'], $Year);
$semSelection = $linterface->GET_SELECTION_BOX($semArr, "id='Semester' name='Semester' onChange='document.form1.submit();'", "", $Semester);

# Set default to the first class
if(empty($ClassID))
{
	$orderBy = " ORDER BY Sequence";
	$sql = "SELECT YearClassID, ClassTitleEN, ClassTitleB5 FROM YEAR_CLASS WHERE AcademicYearID = '$AcademicYearID' $orderBy LIMIT 1";
	$result = $ldiscipline->returnArray($sql);
	$ClassID = $result[0]['YearClassID'];
}
$ClassID = $ClassID==-1 ? "" : $ClassID;
$classSelection = $ldiscipline->getSelectClass("name=\"ClassID\" onChange=\"document.form1.submit()\"", $ClassID, $i_Discipline_System_Award_Punishment_All_Classes, 1, -1);

$toolbar .= "<div class='Conntent_tool'>";
$toolbar .= $linterface->GET_LNK_EXPORT("export.php?AcademicYearID=$AcademicYearID&YearTermID=$YearTermID&ClassID=$ClassID&searchStr=$searchStr", "", "", "", "", 0);
$toolbar .= $linterface->GET_LNK_GENERATE("generate.php", $Lang['eDiscipline']['ComputeActivityScoreGrade'], "", "", "", 0);
$toolbar .= "</div>";

$searchTB = "<input type=\"textbox\" class=\"formtextbox\" id=\"searchStr\" name=\"searchStr\" value=\"$searchStr\">&nbsp;".toolBarSpacer().$linterface->GET_BTN($button_find, "button", "document.form1.submit()");

# DB Table Settings
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

$extraCriteria .= ($ClassID=="") ? "" : " AND ycu.YearClassID = '$ClassID'";
$extraCriteria .= ($searchStr=="")? "" : " AND (iu.EnglishName LIKE '%".trim($searchStr)."%' or iu.ChineseName LIKE '%".trim($searchStr)."%' )";

$conds_year = ($AcademicYearID=="0")? "": " AND (act_bal.AcademicYearID = '$AcademicYearID')";
$conds_semester = ($YearTermID=="0" || $YearTermID=="")? " AND (act_bal.IsAnnual = '1')" : " AND (act_bal.YearTermID = '$YearTermID')";
$conds_semester2 = ($YearTermID=="0" || $YearTermID=="")? " " : " AND (m.YearTermID = '$YearTermID')";

if ($YearTermID=="0" || $YearTermID=="") {
	$YearTermIDStr = "IsAnnual";
}
else {
	$YearTermIDStr = $YearTermID;
}

$sql_adj_semester = $ldiscipline->convertSemesterStringToNumSql("act_bal", 1);
$sql_adj_semester = $ldiscipline->convertSemesterStringToNumSql("act_bal", 1);

$baseMark = $ldiscipline->getActivityScoreRule("baseMark");

$studentIDs = $ldiscipline->storeStudent("0", "::".$ClassID);
$isAnnual = ($YearTermID=="" || $YearTermID=="0") ? 1 : 0;
$Subscore2Arr = $ldiscipline->retrieveStudentActivityScore($studentIDs, $AcademicYearID, $YearTermID, $isAnnual);

$sql = "SELECT 
			CONCAT(".Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN").", '-', ycu.ClassNumber) as ClassNameNum, 
			".getNameFieldByLang("iu.")." as UserName,
			IF(act_bal.SubScore IS NULL, '$baseMark', CONCAT('a', act_bal.SubScore)) as ActScore,
			IFNULL(act_bal.GradeChar, '--') as GradeChar,
			IFNULL(act_bal.DateModified, '--') as DateModified,
			iu.UserID as StudentID,
			count(m.RecordID) as m_count,
			'".$AcademicYearID."' as AcademicYearID, 
			'".$YearTermID."' as YearTermID
		FROM 
			INTRANET_USER iu 
			LEFT OUTER JOIN DISCIPLINE_STUDENT_ACTSCORE_BALANCE act_bal ON (iu.UserID = act_bal.StudentID $conds_semester $conds_year)
			INNER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID = iu.UserID)
			INNER JOIN YEAR_CLASS yc ON (yc.YearClassID = ycu.YearClassID)
			LEFT JOIN DISCIPLINE_MERIT_RECORD as m on (m.StudentID = iu.UserID and m.SubScore2Change!=0 and m.RecordStatus = 1 and m.ReleaseStatus = 1 and m.AcademicYearID = '$AcademicYearID' $conds_semester2)
		WHERE
			 1 $extraCriteria AND iu.RecordType = 2 AND iu.RecordStatus IN (1) AND yc.YearClassID != 0 AND yc.AcademicYearID = '$AcademicYearID'
		GROUP BY iu.UserID";

# Init DB Table
if (!isset($order)) $order = 1;
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("CONCAT(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', 1)), IF(INSTR(ClassNameNum, '-') = 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', -1)), 10, '0')))", "iu.EnglishName", "SubScore", "GradeChar","DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0, 0, 0, 0, 0, 0);
$li->wrap_array = array(0, 0, 0, 0, 0, 0);
$li->IsColOff = 2;

# DB Table Header
$pos = 0;
$li->column_list .= "<th width='1%'>#</th>\n";
$li->column_list .= "<th width='20%'>".$li->column_IP25($pos++, $i_general_class)."</th>\n";
$li->column_list .= "<th width='30%'>".$li->column_IP25($pos++, $i_general_name)."</th>\n";
$li->column_list .= "<th width='15%'>".$Lang['eDiscipline']['ActivityScore']."</th>\n";
$li->column_list .= "<th width='15%'>".$Lang['eDiscipline']['ActivityScoreGrade']."</th>\n";
$li->column_list .= "<th width='20%'>$i_Discipline_System_Discipline_Conduct_Last_Updated</th>\n";

# Get last grade generation
$SettingName = ($Semester == "")? "activity_score_gen_".$Year."_Annual": "activity_score_gen_".$Year."_$Semester";
$sqlgen = "SELECT DATE_FORMAT(SettingValue, '%Y-%m-%d') as LastGen FROM DISCIPLINE_GENERAL_SETTING
		   	WHERE SettingName = '$SettingName' 
			ORDER BY SettingValue DESC LIMIT 1";
$LastGen = $ldiscipline->returnVector($sqlgen);
if(sizeof($LastGen)==0)
{
	$LastGen[0] = '--';
}

# Top Menu
$CurrentPage = "Management_ActivityScore";
$CurrentPageArr['eDisciplinev12'] = 1;

$TAGS_OBJ[] = array($Lang['eDiscipline']['ActivityScore']);

# Left Menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start Layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
var targetDivID = "";
var clickDivID = "";

function adjust_record(StudentID,Year,Semester)
{
	//checkPost(document.form1, 'adjust.php?StudentID='+StudentID+'&Year='+Year+'&Semester='+Semester);
	window.location='adjust.php?StudentID='+StudentID+'&Year='+Year+'&Semester='+Semester;
}

function Show_Merit_Window(StudentID,Year,Semester){
    obj = document.form1;
    obj.targetDivID.value = StudentID+Year+Semester;
    obj.ajaxStudentID.value = StudentID;
    obj.ajaxYear.value = Year;
    obj.ajaxSemester.value = Semester;
    obj.ajaxType.value = 'Merit';
    
    clickDivID = 'merit_'+StudentID+Year+Semester;
    targetDivID = StudentID+Year+Semester;
    YAHOO.util.Connect.setForm(obj);
    
    var path;
    path = "ajax_actscore.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback2);
}

var callback2 = {
    success: function ( o )
    {
		DisplayDefDetail(o.responseText);
	}
}

function DisplayDefDetail(text){
	document.getElementById('div_form').innerHTML=text;
	DisplayPosition();
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}
	return pos_value;
}

function DisplayPosition(){
	var posleft = getPosition(document.getElementById(clickDivID),'offsetLeft') +10;
	var postop = getPosition(document.getElementById(clickDivID),'offsetTop') +10;
	//document.getElementById(targetDivID).style.left=getPosition(document.getElementById(clickDivID),'offsetLeft') +10;
	//document.getElementById(targetDivID).style.top=getPosition(document.getElementById(clickDivID),'offsetTop') +10;
	document.getElementById(targetDivID).style.left= posleft+"px";
	document.getElementById(targetDivID).style.top= postop+"px";
	document.getElementById(targetDivID).style.visibility='visible';
}

function Hide_Window(pos){
	document.getElementById(pos).style.visibility='hidden';
}

function confirmDeleteAdjust(RecordID, StudentID, Year, Semester) {
	Remove_Adjust_Window(RecordID, StudentID, Year, Semester);
	//Show_Adj_Window(StudentID,Year,Semester);
}

function Remove_Adjust_Window(RecordID, StudentID, Year, Semester)
{
	if(confirm("<?=$i_Discipline_System_alert_remove_warning_level?>")) {
		clickDivID = 'adj_'+StudentID+Year+Semester;
		
		$.post(
			"ajax_conduct.php",
			{
				Action: "Edit",
				DivID: encodeURIComponent('adj_'+StudentID+Year+Semester),
				targetDivID: StudentID+Year+Semester,
				ajaxStudentID: StudentID,
				ajaxYear: Year,
				ajaxSemester: Semester,
				ajaxType: 'Remove_Adjust',
				ajaxRecordID: RecordID
			},
			function(ReturnData)
			{
				//alert(ReturnData);
				if(ReturnData==1) {
					self.location.reload();
				}
				else {
					Get_Return_Message("<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>")
				}
			}
		);
	}
}

function goSubmit(year) {
	$('#Year').val(year);
	document.form1.Semester.value='';
	document.form1.ClassID.value=''
	document.form1.submit();	
}
</script>

<br />
<form name="form1" method="get" action="">
			<table width="98%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr><td colspan="2" align="right"><?= $linterface->GET_SYS_MSG($xmsg) ?></td></tr>
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="70%"><?=$toolbar ?></td>
											<td width="30%"></td>
										</tr>
									</table>
								</td>
								<td align="right"><?=$searchTB?></td>
							</tr>
							<tr>
								<td><?=$yearSelection?><?=$semSelection?><?=$classSelection?>
							</tr>
						</table>
						<br style="clear:both"></td>
					</td>
				</tr>
				<tr>
					<td><?= $li->displayFormat_Activity_Score_Management($Subscore2Arr) ?>
					<div id="div_form"></div>
					</td>
				</tr>
				<tr><td class="tabletextremark">^<?=$i_Discipline_System_Discipline_Conduct_Last_Generated?> : <?=$LastGen[0]?></td></tr>
			</table>
			<br>
			
			<input type="hidden" name="targetDivID" id="targetDivID" />
			<input type="hidden" name="ajaxStudentID" id="ajaxStudentID" />
			<input type="hidden" name="ajaxYear" id="ajaxYear" />
			<input type="hidden" name="ajaxSemester" id="ajaxSemester" />
			<input type="hidden" name="ajaxType" id="ajaxType" />
			<input type="hidden" name="ajaxRecordID" id="ajaxRecordID" />
			<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
			<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
			<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
			<input type="hidden" name="page_size_change" value="" />
			<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>