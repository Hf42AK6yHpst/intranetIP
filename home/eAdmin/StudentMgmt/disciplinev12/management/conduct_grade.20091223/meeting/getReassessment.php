<?php

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

//header("Content-Type:text/html;charset=BIG5");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

# remark table
$remarkTable = $ldiscipline->getConductGradeRemarkTable();

$semiGradeString = $ldiscipline->getConductString("DISCIPLINE_MS_STUDENT_CONDUCT_SEMI", $year, $semester, $studentid);

$finalGrade = $ldiscipline->getFinalGradeString($year, $semester, $uid);

# get grade
$gradeInfo = split("\n",get_file_content("$intranet_root/home/eAdmin/StudentMgmt/disciplinev12/management/conduct_grade/gradeFile.txt"));
for($i=0; $i<sizeof($gradeInfo); $i++) {
	list($tempGrade, $comment, $mark) = split(":", $gradeInfo[$i]);	
	$grade[] = $tempGrade;				
}

# get category name & $category ID
//$catname = $ldiscipline->getAwardPunishCategoryName($catid);

$sql = "SELECT TagName FROM DISCIPLINE_ITEM_TAG_SETTING WHERE TagID=$tagid";
$result = $ldiscipline->returnVector($sql, 1);
$tagname = $result[0];

# check the best possible conduct grade
$start = "A";

if($tagid == 2) {		# Diligence
	$conds = ($form!=5 && $form!=7) ? " AND Semester='$semester'" : "";
	$sql = "SELECT COUNT(*) FROM DISCIPLINE_ACCU_RECORD WHERE Year='$year' $conds AND CategoryID=2 AND StudentID=$uid AND RecordStatus=1";
	$result = $ldiscipline->returnVector($sql);
	
	if($result[0]>=8) $start = "B";

} else if($tagid == 1) {

	$sql = "SELECT MR.ProfileMeritType, SUM(MR.ProfileMeritCount) as ProfileMeritCount FROM DISCIPLINE_MERIT_RECORD MR
			LEFT OUTER JOIN DISCIPLINE_MERIT_ITEM MI ON (MI.ItemID=MR.ItemID)
			WHERE MR.Year='$year' AND MR.Semester='$semester' and MR.StudentID=$uid AND MR.MeritType=-1 AND MR.RecordStatus=1 group by MR.MeritType, MR.ProfileMeritType";	
	$result = $ldiscipline->returnArray($sql, 2);

	for($k=0; $k<sizeof($result); $k++) {
		$tmp = $result[$k];
		switch($tmp['ProfileMeritType']) {
			case -2: 	IF($tmp['ProfileMeritCount']>=1) $start = "D";
						break; 
			case -1:	IF($tmp['ProfileMeritCount']>=5) $start = "D";
						ELSE IF($tmp['ProfileMeritCount']>=3) $start = "C-";
						ELSE IF($tmp['ProfileMeritCount']>=2) $start = "C+";
			default :	break;
		}
	}
}

$stdInfo = $ldiscipline->getStudentNameByID($uid);

$yearID = Get_Current_Academic_Year_ID();
$name_field = getNameFieldByLang('USR.');
$sql = "SELECT USR.UserID, $name_field as name FROM INTRANET_USER USR LEFT OUTER JOIN 
			YEAR_CLASS_TEACHER CLSTCH ON (USR.UserID=CLSTCH.UserID) LEFT OUTER JOIN 
			SUBJECT_TERM_CLASS_TEACHER SUBTCH ON (USR.UserID=SUBTCH.UserID) LEFT OUTER JOIN 
				SUBJECT_TERM_CLASS_YEAR_RELATION stcyr ON (stcyr.SubjectGroupID=SUBTCH.SubjectGroupID) LEFT OUTER JOIN
				YEAR_CLASS CLS ON (CLS.YearClassID=CLSTCH.YearClassID AND CLS.AcademicYearID=$yearID) LEFT OUTER JOIN
				YEAR y ON (CLS.YearID=y.YearID OR stcyr.YearID=y.YearID)
		WHERE 
			(CLS.ClassTitleB5='".$stdInfo[0]['ClassName']."' OR CLS.ClassTitleEN='".$stdInfo[0]['ClassName']."') AND 
			USR.RecordType=1
		GROUP BY USR.UserID
		ORDER BY name
		";
$data = $ldiscipline->returnArray($sql, 2);

$totalTeacher = sizeof($data);

$semiConductMarkedTeacherAry = $ldiscipline->getsemiConductMarkedTeacher($year, $semester, $uid);


$table = "<table width='100%' cellpadding='4' cellspacing='0' border='0'>";
$table .= "<tr class='tablebluetop'><td class='tabletopnolink' width='40%'>$tagname</td>";
foreach($grade as $val) {
	$table .= "<td class='tabletopnolink' width='10%' align='center'>$val</td>";	
}
$table .= "</tr>";

for($i=0; $i<sizeof($data); $i++) {
	$teacherID[$i] = $data[$i][0];
	
	$css = ($i%2==0) ? 1 : 2;
	$table .= "<tr class='tablebluerow$css'><td class='tabletext'>".$data[$i][1]."</td>";
	$show = 0;
	foreach($grade as $val) {		# each grade
		if($val==$start) $show = 1;
		$table .= "<td class='tabletopnolink' align='center'>";
		if($show==1) {
			$table .= "<input type='radio' name='radio_".$data[$i][0]."' value='$val'";
			$table .= ($semiConductMarkedTeacherAry[$teacherID[$i]][$tagid]==$val) ? " checked" : "";
			$table .= ">";
		} else {
			$table .= "&nbsp;";	
		}
		$table .= "</td>";
	}
	$table .= "</tr>";	
}
$table .= "	
			<tr class='tablebluetop'>
				<td colspan='8' class='tabletopnolink'>$finalGrade</td>
			</tr>
			<tr>
				<td colspan='8' align='right'>
					".$linterface->GET_BTN($button_view." ".$iDiscipline['OriginalResult'], "button", "showOriginal()")."
				</td>
			</tr>
			<tr>
				<td colspan='8'>
					".$ldiscipline->showWarningMsg($i_UserRemark, $remarkTable)."
				</td>
			</tr>
			<tr>
				<td colspan='8'>
					".$linterface->GET_ACTION_BTN($button_save, "submit", "")."&nbsp;
					".$linterface->GET_ACTION_BTN($button_reset, "reset", "")."&nbsp;
					".$linterface->GET_ACTION_BTN($button_back, "button", "goBack()")."
				</td>
			</tr>";
$table .= "</table>";
$table .= "<input type='hidden' name='teacherID' id='teacherID' value='".implode(',', $teacherID)."'>";
$table .= "<input type='hidden' name='totalTeacher' id='totalTeacher' value='$totalTeacher'>";

echo $table;

intranet_closedb();

?>