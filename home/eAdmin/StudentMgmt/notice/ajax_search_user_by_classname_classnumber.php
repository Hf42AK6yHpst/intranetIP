<?php
// using:

// ################# Change Log [Start] ##############
// Date: 2020-05-04  Bill    [2020-0407-1445-44292]
// - support class teacher audience limit checking + UI update ($sys_custom['eNotice']['ClassTeacherSelectOwnClassStudentOnly'])
// Date: 2019-10-15 Bill    [DM#3678 & DM#3679]
// - support include suspended users
// Date: 2019-07-05 Bill    [2019-0412-1159-01235]
// - support POST request
// Date: 2015-06-26 Omas
// - created this page for autocompleter
// ################# Change Log [End] ################

$PATH_WRT_ROOT = "../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libnotice.php");

intranet_auth();
intranet_opendb();

// Get data
$YearID = $_REQUEST['YearID'];
// $SearchValue = stripslashes(urldecode($_REQUEST['q']));
$SelectedUserIDList = $_REQUEST['SelectedUserIDList'];
$SelectedUserIDList = str_replace('U', '', $SelectedUserIDList);
$SelectedUserIDListArr = explode(',', $SelectedUserIDList);
$SelectedUserIDsStr = implode("','", $SelectedUserIDListArr);

$libdb = new libdb();

// [2020-0407-1445-44292]
$lnotice = new libnotice();
$isAudienceLimited = $lnotice->isClassTeacherAudienceTargetLimited();

$currAcademicYearID = Get_Current_Academic_Year_ID();

$includeSuspended = $_REQUEST['IncludeSuspenedAccount'];
if($includeSuspended) {
    $user_status_cond = " OR iu.RecordStatus = 0 ";
}

if (isset($_REQUEST["searchfrom"]) && $_REQUEST["searchfrom"] == "textarea")
{
    $request_q = isset($_POST['q'])? $_POST['q'] : $_GET['q'];

    // $textarea_q = explode("||", $_GET['q']);
    $textarea_q = explode("||", $request_q);
    if (count($textarea_q) > 0) {
        $removeChar = array(
            "\t",
            " "
        );
        //$removeChar = array("\t");
        foreach ($textarea_q as $kk => $vv) {
            $textarea_q[$kk] = trim(str_replace($removeChar, "", $vv));
        }
    }
    $textarea_q = array_unique($textarea_q);

    $returnString = '';
    if (count($textarea_q) > 0)
    {
        foreach ($textarea_q as $kk => $val) {
            $SearchValue = trim($libdb->Get_Safe_Sql_Like_Query(strtolower((urldecode($val)))));
            if (!empty($SearchValue))
            {
                if ($_REQUEST["nt_userType"] == "pic")
                {
                    $sql = "
                    SELECT
					    UserID, 
					    UserLogin,
						" . getNameFieldByLang() . " as Name,
						ClassName,
						ClassNumber
					FROM
						INTRANET_USER AS iu
					WHERE
						(RecordStatus = 1 $user_status_cond)
						AND RecordType = 1
						AND UserID NOT IN ('" . $SelectedUserIDsStr . "')
						AND UserLogin LIKE '" . $SearchValue . "'
					ORDER BY ClassName ASC, ClassNumber ASC
					LIMIT 1";
                }
                else
                {
                    // [2020-0407-1445-44292] filter - only teaching classes only
                    $extra_cond = "";
                    if($isAudienceLimited)
                    {
                        $teaching_class_list = $lnotice->getClassTeacherCurrentTeachingClass();
                        $teaching_class_ids = Get_Array_By_Key($teaching_class_list, 'ClassID');

                        $extra_cond .= " AND yc.YearClassID IN ('".implode("', '", (array)$teaching_class_ids)."') ";
                    }

                    $sql = "
                    SELECT
					    iu.UserID,
					    iu.UserLogin,
						" . getNameFieldByLang() . " as Name,
						iu.ClassName,
						iu.ClassNumber
					FROM
						INTRANET_USER AS iu
                        LEFT JOIN YEAR_CLASS_USER AS ycu ON (ycu.UserID = iu.UserID)
                        LEFT JOIN YEAR_CLASS AS yc ON (ycu.YearClassID = yc.YearClassID AND AcademicYearID = '" . $currAcademicYearID . "')
					WHERE
						(iu.RecordStatus = 1 $user_status_cond)
						AND iu.RecordType = 2
						AND iu.UserID NOT IN ('" . $SelectedUserIDsStr . "')
						AND (
							CONCAT(LOWER(iu.ClassName), iu.classNumber) LIKE '" . $SearchValue . "'
							OR CONCAT(LOWER(yc.ClassTitleEN), ycu.classNumber) LIKE '" . $SearchValue . "'
							OR CONCAT(LOWER(yc.ClassTitleB5), ycu.classNumber) LIKE '" . $SearchValue . "'
							OR iu.UserLogin LIKE '" . $SearchValue . "'
						)
						$extra_cond
					GROUP BY iu.UserID 
					ORDER BY iu.ClassName ASC, iu.ClassNumber ASC
					LIMIT 1";
                }
                
                $result = $libdb->returnResultSet($sql);
                if (count($result) > 0)
                {
                    foreach ((array)$result as $_StudentInfoArr) {
                        $_UserID = $_StudentInfoArr['UserID'];
                        $_Name = $_StudentInfoArr['Name'];
                        $_ClassName = $_StudentInfoArr['ClassName'];
                        $_ClassNumber = $_StudentInfoArr['ClassNumber'];
                        $UserLogin = $_StudentInfoArr['UserLogin'];
                        if (empty($_ClassName) || $_REQUEST["nt_userType"] == "pic") {
                            $_thisUserNameWithClassAndClassNumber = $_Name . '';
                        }
                        else {
                            $_thisUserNameWithClassAndClassNumber = $_Name . ' (' . $_ClassName . '-' . $_ClassNumber . ')';
                        }
                        
                        $returnString .= $_thisUserNameWithClassAndClassNumber . '||U' . $_UserID . "||" . $val . "\n";
                    }
                }
                else
                {
                    $returnString .= '||NOT_FOUND||' . $val . "\n";
                }
            }
        }
    }
}
else
{
    $sql = "SELECT
				iu.UserID,
				" . getNameFieldByLang() . " as Name,
				iu.ClassName,
				iu.ClassNumber
			FROM
				INTRANET_USER AS iu
                LEFT JOIN YEAR_CLASS_USER AS ycu ON (ycu.UserID = iu.UserID)
                LEFT JOIN YEAR_CLASS AS yc ON (ycu.YearClassID = yc.YearClassID AND AcademicYearID = '" . $currAcademicYearID . "')";
    $sql .= " WHERE (iu.RecordStatus = 1 $user_status_cond) AND iu.RecordType = 2 AND iu.USERID NOT IN ('" . $SelectedUserIDsStr . "') AND (";
    
    $pos = strpos($_REQUEST['q'], " ");
    if ($pos !== false) {
        $tmp = explode(" ", $libdb->Get_Safe_Sql_Like_Query((urldecode($_REQUEST['q']))));
        $SearchValues[0] = strtolower(trim($tmp[0]));
        $SearchValues[1] = trim(str_replace($tmp[0], "", $libdb->Get_Safe_Sql_Like_Query((urldecode($_REQUEST['q'])))));
    } else {
        $SearchValue = trim($libdb->Get_Safe_Sql_Like_Query(strtolower((urldecode($_REQUEST['q'])))));
    }

    if ($SearchValues > 1) {
        $sql .= " (CONCAT(LOWER(iu.ClassName), iu.classNumber) LIKE '%" . $SearchValues[0] . "%'
                    OR CONCAT(LOWER(yc.ClassTitleEN), ycu.classNumber) LIKE '%" . $SearchValues[0] . "%'
                    OR CONCAT(LOWER(yc.ClassTitleB5), ycu.classNumber) LIKE '%" . $SearchValues[0] . "%'
		)
		AND (iu.ChineseName LIKE '%" . $SearchValues[1] . "%'
		        OR iu.EnglishName LIKE '%" . $SearchValues[1] . "%' )";
    }
    else {
        $sql .= " CONCAT(LOWER(iu.ClassName), iu.classNumber) LIKE '%" . $SearchValue . "%'
                    OR CONCAT(LOWER(yc.ClassTitleEN), ycu.classNumber) LIKE '%" . $SearchValue . "%'
                    OR CONCAT(LOWER(yc.ClassTitleB5), ycu.classNumber) LIKE '%" . $SearchValue . "%'
                    OR iu.ChineseName LIKE '%" . $SearchValue . "%'
                    OR iu.EnglishName LIKE '%" . $SearchValue . "%'";
    }
    $sql .= " ) ";

    // [2020-0407-1445-44292] filter - only teaching classes only
    $extra_cond = "";
    if($isAudienceLimited)
    {
        $teaching_class_list = $lnotice->getClassTeacherCurrentTeachingClass();
        $teaching_class_ids = Get_Array_By_Key($teaching_class_list, 'ClassID');

        $sql .= " AND yc.YearClassID IN ('".implode("', '", (array)$teaching_class_ids)."') ";
    }

    $sql .= "GROUP BY iu.UserID ORDER BY iu.ClassName ASC, iu.ClassNumber ASC LIMIT 10";
    $result = $libdb->returnResultSet($sql);

    $returnString = '';
    foreach ((array) $result as $_StudentInfoArr) {
        $_UserID = $_StudentInfoArr['UserID'];
        $_Name = $_StudentInfoArr['Name'];
        $_ClassName = $_StudentInfoArr['ClassName'];
        $_ClassNumber = $_StudentInfoArr['ClassNumber'];
        if (empty($_ClassName)) {
            $_thisUserNameWithClassAndClassNumber = $_Name . '';
        } else {
            $_thisUserNameWithClassAndClassNumber = $_Name . ' (' . $_ClassName . '-' . $_ClassNumber . ')';
        }
        
        $returnString .= $_thisUserNameWithClassAndClassNumber . '|' . '|' . 'U' . $_UserID . "\n";
    }
}

intranet_closedb();
echo $returnString;
?>