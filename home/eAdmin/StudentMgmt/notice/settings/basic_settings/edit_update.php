<?php
# using: 

################# Change Log [Start] ############
#
#   Date:   2020-09-07 [Bill]   [2020-0604-1821-16170]
#           move setting - 'Allow eNotice Admin to sign payment notice for parents.' under payment notice tab
#
#	Date:	2019-11-29  (Philips) [2019-1126-1210-58206]
#			added defaultDisNumDays
#
#	Date:	2019-11-11	(Philips) [2019-1031-1227-12235]
#			added Access Group Can see all notice content
#
#	Date:	2019-09-30	(Philips) [2019-0704-1449-17235]
#			add sendEmail
#
#   Date:   2019-06-05  (Bill)
#           add CSRF token checking
#           support form post action
#
#	Date:	2016-11-21	Tiffany	
#			add general setting: teacherCanSeeAllNotice 
#
#	Date:	2016-10-19	(Bill)	[DM#3101]
#			pass xmsg = 1 to index.php instead of whole wording to prevent security and encoding problem
#
# 	Date:	2016-09-23	(Villa)
#			add the variable of $sendPushMsg ->add to module setting - "default using eclass App to notify"
#
#	Date:	2016-05-18	Kenneth
#			fix of error ($approvalUserID)
#
#	Date:	2016-03-22	Kenneth	
#			add general setting: needApproval 
# 
#	Date:	2015-09-18	Bill
#			update setting "Allow eNotice admin to sign payment notice for parents." [2015-0917-1053-13066]
# 
#	Date:	2015-04-14	Bill
#			update setting "isPICAllowReply" [2015-0323-1602-46073]
#			update setting "isAllowClassTeacherSendeNoticeMessage" [2015-0303-1237-15073]
# 
#	Date:	2014-09-30	Bill
#			add option "staffview" to allow staff to view all students' replay
#
#	Date:	2014-04-15	YatWoon
#			add option "ClassTeacherCanAccessDisciplineNotice"
#
#	Date:	2014-03-03	YatWoon
#			hide "Allow all parents/students to view all notices."
#
#	Date:	2011-02-24	YatWoon
#			add option "Max. Reply Slip Option"
#
#	Date:	2010-06-02	YatWoon
#			add option allow re-sign the notice or not ($lnotice->NotAllowReSign)
#
################# Change Log [End] ############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if(!$plugin['notice'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

if(!verifyCsrfToken($_POST['csrf_token'], $_POST['csrf_token_key'])){
    header('Location: /');
    exit;
}
intranet_auth();
intranet_opendb();

$lnotice = new libnotice();
$lgeneralsettings = new libgeneralsettings();
$libdb = new libdb();

$data = array();
$data['disabled'] = $disabled;
$data['normalAccessGroupID'] = $normalAccessGroupID;
$data['fullAccessGroupID'] = $fullAccessGroupID;
if($plugin['Disciplinev12']) {
	$data['DisciplineGroupID'] = $DisciplineGroupID;
}
$data['isClassTeacherEditDisabled'] = $isClassTeacherEditDisabled;
// [2020-0604-1821-16170] moved under payment notice
// [2015-0917-1053-13066]
//$data['isAllowAdminToSignPaymentNotice'] = $isAllowAdminToSignPaymentNotice;
// [2015-0323-1602-46073]
$data['isPICAllowReply'] = $isPICAllowReply;
$data['isAllAllowed'] = $isAllAllowed;
// [2015-0303-1237-15073]
$data['isAllowClassTeacherSendeNoticeMessage'] = $isAllowClassTeacherSendeNoticeMessage;
$data['defaultNumDays'] = $defaultNumDays;
//$data['showAllEnabled'] = $showAllEnabled;
$data['isLateSignAllow'] = $isLateSignAllow;
$data['NotAllowReSign'] = $NotAllowReSign;
$data['MaxReplySlipOption'] = $MaxReplySlipOption;
$data['ClassTeacherCanAccessDisciplineNotice'] = $ClassTeacherCanAccessDisciplineNotice;
$data['staffview'] = $staffview;
$data['needApproval'] = $needApproval;
$data['sendPushMsg'] = $sendPushMsg;
$data['sendEmail'] = $sendEmail;
$data['teacherCanSeeAllNotice'] = $teacherCanSeeAllNotice;
$data['accessGroupCanSeeAllNotice'] = $accessGroupCanSeeAllNotice;
$data['defaultDisNumDays'] = $defaultDisNumDays;
// $approvalUserID = $_GET['approvalUserID'];
$approvalUserID = $_POST['approvalUserID'];

# store in DB
$lgeneralsettings->Save_General_Setting($lnotice->ModuleName, $data);

# set $this->
$lnotice->disabled = $disabled;			
$lnotice->fullAccessGroupID = $fullAccessGroupID;		
$lnotice->normalAccessGroupID = $normalAccessGroupID;		
if($plugin['Disciplinev12']) {
	$lnotice->DisciplineGroupID = $DisciplineGroupID;		
}
$lnotice->isClassTeacherEditDisabled = $isClassTeacherEditDisabled;
// [2020-0604-1821-16170] moved under payment notice
// [2015-0917-1053-13066]
//$lnotice->isAllowAdminToSignPaymentNotice = $isAllowAdminToSignPaymentNotice;
// [2015-0323-1602-46073]
$lnotice->isPICAllowReply = $isPICAllowReply;
$lnotice->isAllAllowed = $isAllAllowed;		
// [2015-0303-1237-15073]
$lnotice->isAllowClassTeacherSendeNoticeMessage = $isAllowClassTeacherSendeNoticeMessage;
$lnotice->defaultNumDays = $defaultNumDays;		
//$lnotice->showAllEnabled = $showAllEnabled;		
$lnotice->isLateSignAllow = $isLateSignAllow;		
$lnotice->NotAllowReSign = $NotAllowReSign;	
$lnotice->MaxReplySlipOption = $MaxReplySlipOption;
$lnotice->ClassTeacherCanAccessDisciplineNotice = $ClassTeacherCanAccessDisciplineNotice;
$lnotice->staffview = $staffview;
$lnotice->sendPushMsg= $sendPushMsg;
$lnotice->teacherCanSeeAllNotice= $teacherCanSeeAllNotice;
$lnotice->accessGroupCanSeeAllNotice = $accessGroupCanSeeAllNotice;
$lnotice->defaultDisNumDays = $defaultDisNumDays;

# Update Session
foreach($data as $name=>$val) {
	$_SESSION["SSV_PRIVILEGE"]["notice"][$name] = $val;
}

######## Update approvalUserID ############
// Replace 'U' to ''
$numOfUser = count($approvalUserID);
for($i=0; $i<$numOfUser; $i++) {
	$approvalUserID[$i] = str_replace('U', '', $approvalUserID[$i]);
}
// [2020-0604-1821-16170]
//$lnotice->updateApprovalUserInfo((array)$approvalUserID);
$lnotice->updateApprovalUserInfo((array)$approvalUserID, NOTICE_SETTING_TYPE_SCHOOL);

intranet_closedb();
//header("Location: index.php?xmsg=".$Lang['eNotice']['SettingsUpdateSuccess']);
header("Location: index.php?xmsg=1");
?>