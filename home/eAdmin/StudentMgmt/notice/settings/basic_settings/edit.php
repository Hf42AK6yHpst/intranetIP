<?php
# using: 

################# Change Log [Start] ############
# 	Date:	2013-09-11	Carlos
#			hide [Discipline Notice Batch Print Group] for KIS
# 
#	Date:	2010-08-01	YatWoon
#			change to IP25 standard
#
#	Date:	2010-06-02	YatWoon
#			add option allow re-sign the notice or not ($lnotice->NotAllowReSign)
#
################# Change Log [End] ############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

if(!$plugin['notice'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPageArr['eAdminNotice'] = 1;
$CurrentPage = "PageNoticeSettings_BasicSettings";
$lnotice = new libnotice();
$lgroup = new libgroup();

$PAGE_NAVIGATION[] = array($button_edit);

# Left menu 
$TAGS_OBJ[] = array($Lang['eNotice']['Settings']);
$MODULE_OBJ = $lnotice->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();


$adminGroups = $lgroup->returnGroupsByCategory(1, Get_Current_Academic_Year_ID());
$normalGroupSelect = getSelectByArray($adminGroups,"name=normalAccessGroupID",$lnotice->normalAccessGroupID,2,0);
$fullGroupSelect = getSelectByArray($adminGroups,"name=fullAccessGroupID",$lnotice->fullAccessGroupID,2,0);
$DisciplineGroupSelect = getSelectByArray($adminGroups,"name=DisciplineGroupID",$lnotice->DisciplineGroupID,2,0);

# build select option object
for($i=1;$i<=30;$i++)
	$data[] = array($i, $i);
$defaultNumDays_selection = getSelectByArray($data, "name='defaultNumDays'", $lnotice->defaultNumDays , 0, 1);	
?>

<?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>
  
<form name="form1" method="get" action="edit_update.php">
<table class="form_table_v30">

		<tr valign='top'>
			<td class="field_title" nowrap><?=$Lang['eNotice']['DisableNotice']?></td>
			<td>
			<input type="radio" name="disabled" value="1" <?=$lnotice->disabled ? "checked":"" ?> id="disabled1"> <label for="disabled1"><?=$i_general_yes?></label> 
			<input type="radio" name="disabled" value="0" <?=$lnotice->disabled ? "":"checked" ?> id="disabled0"> <label for="disabled0"><?=$i_general_no?></label>
			</td>
		</tr>
		
		<tr valign='top'>
			<td class="field_title" nowrap><?=$Lang['eNotice']['NormalControlGroup']?> </td>
			<td><?=$normalGroupSelect?> (<?=$Lang['eNotice']['AdminGroupOnly']?>)</td>
		</tr>
		
		<tr valign='top'>
			<td class="field_title" nowrap><?=$Lang['eNotice']['FullControlGroup']?></td>
			<td><?=$fullGroupSelect?> (<?=$Lang['eNotice']['AdminGroupOnly']?>)</td>
		</tr>
		
		<? if($plugin['Disciplinev12'] && $_SESSION["platform"]!="KIS") {?>
		<tr valign='top'>
			<td class="field_title" nowrap><?=$Lang['eNotice']['DisciplineGroup']?></td>
			<td><?=$DisciplineGroupSelect?> (<?=$Lang['eNotice']['AdminGroupOnly']?>)</td>
		</tr> 
		<? } ?>
		
		<tr valign='top'>
			<td class="field_title" nowrap><?=$Lang['eNotice']['DisableClassTeacher']?></td>
			<td>
			<input type="radio" name="isClassTeacherEditDisabled" value="1" <?=$lnotice->isClassTeacherEditDisabled ? "checked":"" ?> id="isClassTeacherEditDisabled1"> <label for="isClassTeacherEditDisabled1"><?=$i_general_yes?></label> 
			<input type="radio" name="isClassTeacherEditDisabled" value="0" <?=$lnotice->isClassTeacherEditDisabled ? "":"checked" ?> id="isClassTeacherEditDisabled0"> <label for="isClassTeacherEditDisabled0"><?=$i_general_no?></label>
			</td>
		</tr>
		
		<tr valign='top'>
			<td class="field_title" nowrap><?=$Lang['eNotice']['AllHaveRight']?></td>
			<td>
			<input type="radio" name="isAllAllowed" value="1" <?=$lnotice->isAllAllowed ? "checked":"" ?> id="isAllAllowed1"> <label for="isAllAllowed1"><?=$i_general_yes?></label> 
			<input type="radio" name="isAllAllowed" value="0" <?=$lnotice->isAllAllowed ? "":"checked" ?> id="isAllAllowed0"> <label for="isAllAllowed0"><?=$i_general_no?></label>
			</td>
		</tr>
		
		<tr valign='top'>
			<td class="field_title" nowrap><?=$Lang['eNotice']['DefaultNumDays']?></td>
			<td><?=$defaultNumDays_selection?></td>
		</tr>
		
		<tr valign='top'>
			<td class="field_title" nowrap><?=$Lang['eNotice']['ParentStudentCanViewAll']?></td>
			<td>
			<input type="radio" name="showAllEnabled" value="1" <?=$lnotice->showAllEnabled ? "checked":"" ?> id="showAllEnabled1"> <label for="showAllEnabled1"><?=$i_general_yes?></label> 
			<input type="radio" name="showAllEnabled" value="0" <?=$lnotice->showAllEnabled ? "":"checked" ?> id="showAllEnabled0"> <label for="showAllEnabled0"><?=$i_general_no?></label>
			</td>
		</tr>
		
		<tr valign='top'>
			<td class="field_title" nowrap><?=$i_Circular_Settings_AllowLateSign?></td>
			<td>
			<input type="radio" name="isLateSignAllow" value="1" <?=$lnotice->isLateSignAllow ? "checked":"" ?> id="isLateSignAllow1"> <label for="isLateSignAllow1"><?=$i_general_yes?></label> 
			<input type="radio" name="isLateSignAllow" value="0" <?=$lnotice->isLateSignAllow ? "":"checked" ?> id="isLateSignAllow0"> <label for="isLateSignAllow0"><?=$i_general_no?></label>
			</td>
		</tr>
		
		<tr valign='top'>
			<td class="field_title" nowrap><?=$Lang['eNotice']['NotAllowReSign']?></td>
			<td>
			<input type="radio" name="NotAllowReSign" value="1" <?=$lnotice->NotAllowReSign ? "checked":"" ?> id="NotAllowReSign1"> <label for="NotAllowReSign1"><?=$i_general_yes?></label> 
			<input type="radio" name="NotAllowReSign" value="0" <?=$lnotice->NotAllowReSign ? "":"checked" ?> id="NotAllowReSign0"> <label for="NotAllowReSign0"><?=$i_general_no?></label>
			</td>
		</tr>
		
</table>                        

<div class="edit_bottom">
	<p class="spacer"></p>
	<?= $linterface->GET_ACTION_BTN($button_update, "submit") ?>
	<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back()","cancelbtn") ?>
	<p class="spacer"></p>
</div>


</form>


<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>