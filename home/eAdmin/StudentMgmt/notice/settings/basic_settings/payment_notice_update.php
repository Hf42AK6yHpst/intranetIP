<?php
# using:

################# Change Log [Start] ############
#
#   Date:   2020-09-07 [Bill]   [2020-0604-1821-16170]
#           Create file
#
################# Change Log [End] ############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if(!$plugin['notice'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"])
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

if(!verifyCsrfToken($_POST['csrf_token'], $_POST['csrf_token_key'])){
    header('Location: /');
    exit;
}

intranet_auth();
intranet_opendb();

$lnotice = new libnotice();
$lgeneralsettings = new libgeneralsettings();
$libdb = new libdb();

$data = array();
//$data['payment_disabled'] = $payment_disabled;
$data['payment_fullAccessGroupID'] = $payment_fullAccessGroupID;
$data['payment_normalAccessGroupID'] = $payment_normalAccessGroupID;
/*
if($plugin['Disciplinev12']) {
    $data['payment_DisciplineGroupID'] = $payment_DisciplineGroupID;
}
*/
//$data['payment_isClassTeacherEditDisabled'] = $payment_isClassTeacherEditDisabled;
$data['isAllowAdminToSignPaymentNotice'] = $isAllowAdminToSignPaymentNotice;
//$data['payment_isPICAllowReply'] = $payment_isPICAllowReply;
$data['payment_isAllAllowed'] = $payment_isAllAllowed;
$data['payment_isAllowClassTeacherSendeNoticeMessage'] = $payment_isAllowClassTeacherSendeNoticeMessage;
$data['payment_defaultNumDays'] = $payment_defaultNumDays;
//$data['payment_showAllEnabled'] = $payment_showAllEnabled;
//$data['payment_isLateSignAllow'] = $payment_isLateSignAllow;
//$data['payment_NotAllowReSign'] = $payment_NotAllowReSign;
$data['payment_MaxReplySlipOption'] = $payment_MaxReplySlipOption;
//$data['payment_ClassTeacherCanAccessDisciplineNotice'] = $payment_ClassTeacherCanAccessDisciplineNotice;
$data['payment_staffview'] = $payment_staffview;
$data['payment_needApproval'] = $payment_needApproval;
$data['payment_sendPushMsg'] = $payment_sendPushMsg;
$data['payment_sendEmail'] = $payment_sendEmail;
$data['payment_teacherCanSeeAllNotice'] = $payment_teacherCanSeeAllNotice;
$data['payment_accessGroupCanSeeAllNotice'] = $payment_accessGroupCanSeeAllNotice;
//$data['payment_defaultDisNumDays'] = $payment_defaultDisNumDays;
// $approvalUserID = $_GET['approvalUserID'];
$approvalUserID = $_POST['approvalUserID'];

# store in DB
$lgeneralsettings->Save_General_Setting($lnotice->ModuleName, $data);

# set $this->
//$lnotice->payment_disabled = $payment_disabled;
$lnotice->payment_fullAccessGroupID = $payment_fullAccessGroupID;
$lnotice->payment_normalAccessGroupID = $payment_normalAccessGroupID;
/*
if($plugin['Disciplinev12']) {
    $lnotice->payment_DisciplineGroupID = payment_DisciplineGroupID;
}
*/
//$lnotice->payment_isClassTeacherEditDisabled = $payment_isClassTeacherEditDisabled;
$lnotice->isAllowAdminToSignPaymentNotice = $isAllowAdminToSignPaymentNotice;
//$lnotice->payment_isPICAllowReply = $payment_isPICAllowReply;
$lnotice->payment_isAllAllowed = $payment_isAllAllowed;
$lnotice->payment_isAllowClassTeacherSendeNoticeMessage = $payment_isAllowClassTeacherSendeNoticeMessage;
$lnotice->payment_defaultNumDays = $payment_defaultNumDays;
//$lnotice->payment_showAllEnabled = $payment_showAllEnabled;
//$lnotice->payment_isLateSignAllow = $payment_isLateSignAllow;
//$lnotice->payment_NotAllowReSign = $payment_NotAllowReSign;
$lnotice->payment_MaxReplySlipOption = $payment_MaxReplySlipOption;
//$lnotice->payment_ClassTeacherCanAccessDisciplineNotice = $payment_ClassTeacherCanAccessDisciplineNotice;
$lnotice->payment_staffview = $payment_staffview;
$lnotice->payment_needApproval = $payment_needApproval;
$lnotice->payment_sendPushMsg = $payment_sendPushMsg;
$lnotice->payment_sendEmail = $payment_sendEmail;
$lnotice->payment_teacherCanSeeAllNotice = $payment_teacherCanSeeAllNotice;
$lnotice->payment_accessGroupCanSeeAllNotice = $payment_accessGroupCanSeeAllNotice;
//$lnotice->payment_defaultDisNumDays = $payment_defaultDisNumDays;

# Update Session
foreach($data as $name=>$val) {
    $_SESSION["SSV_PRIVILEGE"]["notice"][$name] = $val;
}

######## Update approvalUserID ############
// Replace 'U' to ''
$numOfUser = count($approvalUserID);
for($i=0; $i<$numOfUser; $i++) {
    $approvalUserID[$i] = str_replace('U', '', $approvalUserID[$i]);
}
$lnotice->updateApprovalUserInfo((array)$approvalUserID, NOTICE_SETTING_TYPE_PAYMENT);

intranet_closedb();
//header("Location: index.php?xmsg=".$Lang['eNotice']['SettingsUpdateSuccess']);
header("Location: payment_notice.php?xmsg=1");
?>