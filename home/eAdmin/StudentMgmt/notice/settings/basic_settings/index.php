<?php
# using: 

################# Change Log [Start] ############
#
#   Date:   2020-09-07 [Bill]   [2020-0604-1821-16170]
#           added new tab for payment notice
#           move setting - 'Allow eNotice Admin to sign payment notice for parents.' under payment notice tab
#
#   Date:   2020-08-03  (Bill)  [2020-0604-1048-34235]
#           Set default no. of days for returning notice    ($sys_custom['eNotice']['MaxDefaultNumDaysCount'])
#           (including discipline notice - $sys_custom['eNotice']['MaxDefaultNumDaysCount_eDis'])
#
#	Date:	2019-12-03	(Philips) [2019-1126-1210-58206]
#			First time defaultDisNumDays as same to defaultNumDays
#
#	Date:	2019-11-29  (Philips) [2019-1126-1210-58206]
#			added defaultDisNumDays
#
#	Date:	2019-11-11	(Philips) [2019-1031-1227-12235]
#			added Access Group Can see all notice content
#
#	Date:	2019-09-30	(Philips) [2019-0704-1449-17235]
#			add sendEmail
#
#	Date:	2019-08-02  (Carlos)
#			added payment notice remark to [Allow admin to sign payment notice for parent] setting. 
#
#   Date:   2019-06-05  (Bill)
#           add CSRF token in html form
#           change form action to POST
#
#	Date:	2016-11-21	(Tiffany)
#			add to module setting - "Teacher Can See All Notice"
#
#	Date:	2016-10-19	(Bill)	[DM#3101]
#			display system message when $xmsg value is 1 
#
# 	Date:	2016-09-23	(Villa)
#			add to module setting - "default using eclass App to notify"
#
#	Date:	2016-05-18	Kenneth	
#			needApproval label fix 
#
#	Date:	2016-03-22	Kenneth	
#			add general setting: needApproval 
# 
#	Date:	2015-10-28	Bill
#			hide option "Allow class teacher to send push message to parents" if client without eClass App [2015-1028-1200-02206]
#
#	Date:	2015-09-18	Bill
#			add option "Allow eNotice admin to sign payment notice for parents." [2015-0917-1053-13066]
#
#	Date:	2015-09-07	Roy
#			change allow late sign wordings to $Lang['eNotice']['AllowLateSign']
#
#	Date:	2015-04-14	Bill
#			add option "Allow PIC to edit replies for parents." [2015-0323-1602-46073]
#			add option "Allow class teacher to send push message to parents." [2015-0303-1237-15073]
#
#	Date:	2014-09-30	Bill
#			add option "staffview" to allow staff to view all students' replay
#
#	Date:	2014-04-15	YatWoon
#			add option "ClassTeacherCanAccessDisciplineNotice"
#
#	Date"	2014-03-03	YatWoon
#			hide "Allow all parents/students to view all notices."
#
# 	Date:	2013-09-11	Carlos
#			hide [Discipline Notice Batch Print Group] for KIS
#
#	Date:	2011-09-28	YatWoon
#			display academic year for the group 
#
#	Date:	2011-02-24	YatWoon
#			add option "Max. Reply Slip Option"
#
#	Date:	2010-08-01	YatWoon
#			change to IP25 standard
#
#	Date:	2010-06-02	YatWoon
#			add option allow re-sign the notice or not ($lnotice->NotAllowReSign)
#
################# Change Log [End] ############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

if(!$plugin['notice'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();

$CurrentPageArr['eAdminNotice'] = 1;
$CurrentPage = "PageNoticeSettings_BasicSettings";

$lnotice = new libnotice();
$lgroup = new libgroup();
$luser = new libuser();
$lpayment = new libpayment();

# Left menu
// [2020-0604-1821-16170]
//$TAGS_OBJ[] = array($Lang['eNotice']['Settings']);
$TAGS_OBJ[] = array($i_Notice_ElectronicNotice2, '', 1);
$TAGS_OBJ[] = array($Lang['eNotice']['PaymentNotice'], "payment_notice.php", 0);
$MODULE_OBJ = $lnotice->GET_MODULE_OBJ_ARR();

# Start layout
$xmsg = $xmsg==1? $Lang['eNotice']['SettingsUpdateSuccess'] : "";
$linterface->LAYOUT_START($xmsg);

$adminGroups = $lgroup->returnGroupsByCategory(1, Get_Current_Academic_Year_ID());
$normalGroupSelect = getSelectByArray($adminGroups,"name=normalAccessGroupID",$lnotice->normalAccessGroupID,2,0);
$fullGroupSelect = getSelectByArray($adminGroups,"name=fullAccessGroupID",$lnotice->fullAccessGroupID,2,0);
$DisciplineGroupSelect = getSelectByArray($adminGroups,"name=DisciplineGroupID",$lnotice->DisciplineGroupID,2,0);

$NormalControlGroupInfo = $lgroup->returnGroup($lnotice->normalAccessGroupID);
$NormalControlGroupInfo_year = getAcademicYearByAcademicYearID($NormalControlGroupInfo[0]['AcademicYearID'], $intranet_session_language);
$NormalControlGroupTitle = ($intranet_session_language=="en" ? $NormalControlGroupInfo[0]['Title'] : $NormalControlGroupInfo[0]['TitleChinese']) . " <span class='tabletextremark'>(". $NormalControlGroupInfo_year .")</span>";

$FullControlGroupInfo = $lgroup->returnGroup($lnotice->fullAccessGroupID);
$FullControlGroupInfo_year = getAcademicYearByAcademicYearID($FullControlGroupInfo[0]['AcademicYearID'], $intranet_session_language);
$FullControlGroupTitle = ($intranet_session_language=="en" ? $FullControlGroupInfo[0]['Title'] : $FullControlGroupInfo[0]['TitleChinese']) . " <span class='tabletextremark'>(". $FullControlGroupInfo_year .")</span>";

if($plugin['Disciplinev12'])
{
	$DisciplineGroupInfo = $lgroup->returnGroup($lnotice->DisciplineGroupID);
	$DisciplineGroupInfo_year = getAcademicYearByAcademicYearID($DisciplineGroupInfo[0]['AcademicYearID'], $intranet_session_language);
	$DisciplineGroupTitle = ($intranet_session_language=="en" ? $DisciplineGroupInfo[0]['Title'] : $DisciplineGroupInfo[0]['TitleChinese']) . " <span class='tabletextremark'>(". $DisciplineGroupInfo_year .")</span>";
}

// [2020-0604-1048-34235] set default no. of days for returning notice
// for($i=1; $i<=30; $i++)
$maxDefaultNumDays = $sys_custom['eNotice']['MaxDefaultNumDaysCount'] ? $sys_custom['eNotice']['MaxDefaultNumDaysCount'] : 30;
for($i=1; $i<=$maxDefaultNumDays; $i++) {
	$data[] = array($i, $i);
}
$defaultNumDays_selection = getSelectByArray($data, "name='defaultNumDays'", $lnotice->defaultNumDays , 0, 1);

// [2020-0604-1048-34235] set default no. of days for returning discipline notice
$maxDefaultNumDays_eDis = $sys_custom['eNotice']['MaxDefaultNumDaysCount_eDis'] ? $sys_custom['eNotice']['MaxDefaultNumDaysCount_eDis'] : 30;
for($i=1; $i<=$maxDefaultNumDays_eDis; $i++) {
    $data2[] = array($i, $i);
}
$defaultDisNumDays_selection = getSelectByArray($data2, "name='defaultDisNumDays'", ($lnotice->defaultDisNumDays ? $lnotice->defaultDisNumDays : $lnotice->defaultNumDays), 0, 1);

for($i=3; $i<=100; $i++) {
	$data1[] = array($i, $i);
}
$MaxReplySlipOption_selection = getSelectByArray($data1, "name='MaxReplySlipOption'", $lnotice->MaxReplySlipOption , 0, 1);

// Get Approval User Info
// [2020-0604-1821-16170]
//$approvalUserInfoAry = $lnotice->getApprovalUserInfo();
$approvalUserInfoAry = $lnotice->getApprovalUserInfo(NOTICE_SETTING_TYPE_SCHOOL);
$approvalUserStringForShow = '';
foreach($approvalUserInfoAry as $i => $approvalUserInfo){
	if($i != 0) {
		$approvalUserStringForShow .= '<br>';
	}
	$approvalUserStringForShow .= Get_Lang_Selection($approvalUserInfo['ChineseName'],$approvalUserInfo['EnglishName']);
}

// Get Approval User Selection Box
$approvalUserSelectionBox = '';
$approvalUserIDArray = Get_Array_By_Key($approvalUserInfoAry,'UserID');
$approvalUserSelectionBox = "<select name='approvalUserID[]' id='approvalUserID' size='6' multiple='multiple'>";
for($i = 0; $i < sizeof($approvalUserIDArray); $i++) {
	$approvalUserSelectionBox .= "<option value=\"".$approvalUserIDArray[$i]."\">".$luser->getNameWithClassNumber($approvalUserIDArray[$i], $IncludeArchivedUser=1)."</option>";
}
$approvalUserSelectionBox .= "</select>";
?>

<? include_once($PATH_WRT_ROOT."home/eClassApp/common/pushMessageTemplate/js_AppMessageReminder.php") ?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/html_editor/fckeditor_cust.js" ></script>
<script language="javascript">
<!--
$( document ).ready(function() {
	<?php if(!$lnotice->needApproval){ ?>
	 	disbleApprovalGroupSeletor(0);
	<?php } ?>
});

function EditInfo()
{
	$('#EditBtn').attr('style', 'visibility: hidden'); 
	$('.Edit_Hide').attr('style', 'display: none');
	$('.Edit_Show').attr('style', '');
	
	$('#UpdateBtn').attr('style', '');
	$('#cancelbtn').attr('style', '');
}		

function ResetInfo()
{
	document.form1.reset();
	
	$('#EditBtn').attr('style', '');
	$('.Edit_Hide').attr('style', '');
	$('.Edit_Show').attr('style', 'display: none');
	
	$('#UpdateBtn').attr('style', 'display: none');
	$('#cancelbtn').attr('style', 'display: none');
}

function goSubmit(obj){
	disbleApprovalGroupSeletor(1);
	checkOptionAll(obj.elements["approvalUserID[]"]);
	return true;
}

function chooseRadioNeedApproval(obj){
	var needApproval = obj.value;
	disbleApprovalGroupSeletor(needApproval);
}

function disbleApprovalGroupSeletor(enable){
	if(enable==1){
		$('#approvalUserID').removeAttr('disabled');
		$('#approvalUserDiv').css('display', 'inline-block');
	}else if(enable==0){
		$('#approvalUserID').attr('disabled','disabled');
		$('#approvalUserDiv').hide();
	}
}
//-->
function canSeeAll(){
	if($('#teacherCanSeeAllNotice1').attr('checked')){
		$('#accessGroupCanSeeAllNotice1').attr('disabled', true);
		$('#accessGroupCanSeeAllNotice0').attr('disabled', true);
	} else {
		$('#accessGroupCanSeeAllNotice1').removeAttr('disabled');
		$('#accessGroupCanSeeAllNotice0').removeAttr('disabled');
	}
}
</script>

<form name="form1" method="post" action="edit_update.php">

<div class="table_board">
	<div class="table_row_tool row_content_tool">
		<?= $linterface->GET_SMALL_BTN($button_edit, "button","javascript:EditInfo();","EditBtn")?>
	</div>
	<p class="spacer"></p>
	<div class="form_sub_title_v30"><em>- <span class="field_title"> <?=$Lang['eNotice']['SysProperties']['General']?> </span>-</em>
		<p class="spacer"></p>
	</div>
	
	<table class="form_table_v30">
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eNotice']['DisableNotice']?></td>
				<td> 
    				<span class="Edit_Hide"><?=$lnotice->disabled ? $i_general_yes:$i_general_no?></span>
    				<span class="Edit_Show" style="display:none">
        				<input type="radio" name="disabled" value="1" <?=$lnotice->disabled ? "checked":"" ?> id="disabled1"> <label for="disabled1"><?=$i_general_yes?></label> 
        				<input type="radio" name="disabled" value="0" <?=$lnotice->disabled ? "":"checked" ?> id="disabled0"> <label for="disabled0"><?=$i_general_no?></label>
    				</span>
				</td>
			</tr>
			
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eNotice']['NormalControlGroup']?> </td>
				<td>
    				<span class="Edit_Hide"><?=$NormalControlGroupTitle ? $NormalControlGroupTitle : $i_general_NotSet ?></span>
    				<span class="Edit_Show" style="display:none"><?=$normalGroupSelect?> (<?=$Lang['eNotice']['AdminGroupOnly']?>)</span>
    			</td>
			</tr>
			
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eNotice']['FullControlGroup']?></td>
				<td>
    				<span class="Edit_Hide"><?=$FullControlGroupTitle ? $FullControlGroupTitle : $i_general_NotSet ?></span>
    				<span class="Edit_Show" style="display:none"><?=$fullGroupSelect?> (<?=$Lang['eNotice']['AdminGroupOnly']?>)</span>
    			</td>
			</tr>
<?php if ($sys_custom['eNotice']['HideParenteNotice']) { ?>
			<input type="hidden" name="isClassTeacherEditDisabled" value="0" id="isClassTeacherEditDisabled0">
<?php } else { ?>
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eNotice']['DisableClassTeacher']?></td>
				<td>
    				<span class="Edit_Hide"><?=$lnotice->isClassTeacherEditDisabled ? $i_general_yes:$i_general_no?></span>
    				<span class="Edit_Show" style="display:none">
        				<input type="radio" name="isClassTeacherEditDisabled" value="1" <?=$lnotice->isClassTeacherEditDisabled ? "checked":"" ?> id="isClassTeacherEditDisabled1"> <label for="isClassTeacherEditDisabled1"><?=$i_general_yes?></label> 
        				<input type="radio" name="isClassTeacherEditDisabled" value="0" <?=$lnotice->isClassTeacherEditDisabled ? "":"checked" ?> id="isClassTeacherEditDisabled0"> <label for="isClassTeacherEditDisabled0"><?=$i_general_no?></label>
    				</span>
				</td>
			</tr>
<?php } ?>

<!--
<?php if ($sys_custom['eNotice']['HideParenteNotice']) { ?>
			<input type="hidden" name="isAllowAdminToSignPaymentNotice" value="0" id="isAllowAdminToSignPaymentNotice0">
<?php } else { ?>
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eNotice']['AllowAdminToSignPaymentNotice'].($lpayment->isEWalletDirectPayEnabled()?'<br />('.$Lang['eNotice']['PaymentNoticeAdminSignForParentSettingRemark'].')':'')?></td>
				<td>
    				<span class="Edit_Hide"><?=$lnotice->isAllowAdminToSignPaymentNotice ? $i_general_yes:$i_general_no?></span>
    				<span class="Edit_Show" style="display:none">
        				<input type="radio" name="isAllowAdminToSignPaymentNotice" value="1" <?=$lnotice->isAllowAdminToSignPaymentNotice ? "checked":"" ?> id="isAllowAdminToSignPaymentNotice1"> <label for="isAllowAdminToSignPaymentNotice1"><?=$i_general_yes?></label> 
        				<input type="radio" name="isAllowAdminToSignPaymentNotice" value="0" <?=$lnotice->isAllowAdminToSignPaymentNotice ? "":"checked" ?> id="isAllowAdminToSignPaymentNotice0"> <label for="isAllowAdminToSignPaymentNotice0"><?=$i_general_no?></label>
					</span>
				</td>
			</tr>
<?php } ?>
-->

<?php if ($sys_custom['eNotice']['HideParenteNotice']) { ?>
			<input type="hidden" name="isPICAllowReply" value="0" id="isPICAllowReply0">
<?php } else { ?>
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eNotice']['PICAllowReply']?></td>
				<td>
    				<span class="Edit_Hide"><?=$lnotice->isPICAllowReply ? $i_general_yes:$i_general_no?></span>
    				<span class="Edit_Show" style="display:none">
        				<input type="radio" name="isPICAllowReply" value="1" <?=$lnotice->isPICAllowReply ? "checked":"" ?> id="isPICAllowReply1"> <label for="isPICAllowReply1"><?=$i_general_yes?></label> 
        				<input type="radio" name="isPICAllowReply" value="0" <?=$lnotice->isPICAllowReply ? "":"checked" ?> id="isPICAllowReply0"> <label for="isPICAllowReply0"><?=$i_general_no?></label>
					</span>
				</td>
			</tr>
<?php } ?>
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eNotice']['AllHaveRight']?></td>
				<td>
    				<span class="Edit_Hide"><?=$lnotice->isAllAllowed ? $i_general_yes:$i_general_no?></span>
    				<span class="Edit_Show" style="display:none">
        				<input type="radio" name="isAllAllowed" value="1" <?=$lnotice->isAllAllowed ? "checked":"" ?> id="isAllAllowed1"> <label for="isAllAllowed1"><?=$i_general_yes?></label> 
        				<input type="radio" name="isAllAllowed" value="0" <?=$lnotice->isAllAllowed ? "":"checked" ?> id="isAllAllowed0"> <label for="isAllAllowed0"><?=$i_general_no?></label>
    				</span>
				</td>
			</tr>
			
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eNotice']['AllStaffAllow']?></td>
				<td> 
    				<span class="Edit_Hide"><?=$lnotice->staffview ? $i_general_yes:$i_general_no?></span>
    				<span class="Edit_Show" style="display:none">
        				<input type="radio" name="staffview" value="1" <?=$lnotice->staffview ? "checked":"" ?> id="staffview1"> <label for="staffview1"><?=$i_general_yes?></label> 
        				<input type="radio" name="staffview" value="0" <?=$lnotice->staffview ? "":"checked" ?> id="staffview0"> <label for="staffview0"><?=$i_general_no?></label>
    				</span>
				</td>
			</tr>
			
			<?php if($plugin['eClassApp']){ ?>
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eNotice']['AllowClassTeacherSendeNoticeMessage']?></td>
				<td>
    				<span class="Edit_Hide"><?=$lnotice->isAllowClassTeacherSendeNoticeMessage ? $i_general_yes:$i_general_no?></span>
    				<span class="Edit_Show" style="display:none">
        				<input type="radio" name="isAllowClassTeacherSendeNoticeMessage" value="1" <?=$lnotice->isAllowClassTeacherSendeNoticeMessage ? "checked":"" ?> id="isAllowClassTeacherSendeNoticeMessage1"> <label for="isAllowClassTeacherSendeNoticeMessage1"><?=$i_general_yes?></label> 
        				<input type="radio" name="isAllowClassTeacherSendeNoticeMessage" value="0" <?=$lnotice->isAllowClassTeacherSendeNoticeMessage ? "":"checked" ?> id="isAllowClassTeacherSendeNoticeMessage0"> <label for="isAllowClassTeacherSendeNoticeMessage0"><?=$i_general_no?></label>
    				</span>
				</td>
			</tr>
			<?php } ?>
			
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eNotice']['DefaultNumDays']?></td>
				<td>
    				<span class="Edit_Hide"><?=$lnotice->defaultNumDays?></span>
    				<span class="Edit_Show" style="display:none"><?=$defaultNumDays_selection?></span>
				</td>
			</tr>
			
			<? /* ?>
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eNotice']['ParentStudentCanViewAll']?></td>
				<td>
				<span class="Edit_Hide"><?=$lnotice->showAllEnabled ? $i_general_yes:$i_general_no?></span>
				<span class="Edit_Show" style="display:none">
				<input type="radio" name="showAllEnabled" value="1" <?=$lnotice->showAllEnabled ? "checked":"" ?> id="showAllEnabled1"> <label for="showAllEnabled1"><?=$i_general_yes?></label> 
				<input type="radio" name="showAllEnabled" value="0" <?=$lnotice->showAllEnabled ? "":"checked" ?> id="showAllEnabled0"> <label for="showAllEnabled0"><?=$i_general_no?></label>
				</span></td>
			</tr>
			<? */ ?>
			
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eNotice']['AllowLateSign']?></td>
				<td>
    				<span class="Edit_Hide"><?=$lnotice->isLateSignAllow ? $i_general_yes:$i_general_no?></span>
    				<span class="Edit_Show" style="display:none">
        				<input type="radio" name="isLateSignAllow" value="1" <?=$lnotice->isLateSignAllow ? "checked":"" ?> id="isLateSignAllow1"> <label for="isLateSignAllow1"><?=$i_general_yes?></label> 
        				<input type="radio" name="isLateSignAllow" value="0" <?=$lnotice->isLateSignAllow ? "":"checked" ?> id="isLateSignAllow0"> <label for="isLateSignAllow0"><?=$i_general_no?></label>
    				</span>
				</td>
			</tr>
			
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eNotice']['NotAllowReSign']?></td>
				<td>
    				<span class="Edit_Hide"><?=$lnotice->NotAllowReSign ? $i_general_yes:$i_general_no?></span>
    				<span class="Edit_Show" style="display:none">
        				<input type="radio" name="NotAllowReSign" value="1" <?=$lnotice->NotAllowReSign ? "checked":"" ?> id="NotAllowReSign1"> <label for="NotAllowReSign1"><?=$i_general_yes?></label> 
        				<input type="radio" name="NotAllowReSign" value="0" <?=$lnotice->NotAllowReSign ? "":"checked" ?> id="NotAllowReSign0"> <label for="NotAllowReSign0"><?=$i_general_no?></label>
    				</span>
				</td>
			</tr>
			
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eNotice']['MaxNumberReplySlipOption']?></td>
				<td>
    				<span class="Edit_Hide"><?=$lnotice->MaxReplySlipOption?></span>
    				<span class="Edit_Show" style="display:none"><?=$MaxReplySlipOption_selection?></span>
				</td>
			</tr>
			
			<tr valign='top'>
				<td class="field_title" nowrap><?= $eComm['NeedApproval'] ?></td>
				<td><span class="Edit_Hide">
					<?= $lnotice->needApproval ? $i_general_yes:$i_general_no?>
					<div>
						<table class="common_table_list_v30">
							<tr>
								<td class="field_title" style="width:30%">
									<?=  $Lang['eNotice']['ApprovalUser'] ?> <br> <?= $Lang['eNotice']['AdminGroupHaveApprovalRight'] ?>
								</td>
								<td>
									<?= $approvalUserStringForShow ?>
								</td>
							</tr>
						</table>
					</div>				
				</span>
				<span class="Edit_Show" style="display:none">
					<input type="radio" name="needApproval" value="1" <?= $lnotice->needApproval ? "checked":"" ?> id="needApprovalY" onclick="chooseRadioNeedApproval(this);"> <label for="needApprovalY"><?=$i_general_yes?></label> 
					<input type="radio" name="needApproval"  value="0" <?= $lnotice->needApproval? "":"checked" ?> id="needApprovalN" onclick="chooseRadioNeedApproval(this);"> <label for="needApprovalN"><?=$i_general_no?></label>
					<div>
						<table class="common_table_list_v30">
							<tr>
								<td class="field_title" style="width:30%">
									<?=  $Lang['eNotice']['ApprovalUser'] ?>
								</td>
								<td>
									<?= $approvalUserSelectionBox ?>
									<div style="display:inline-block" id="approvalUserDiv">
									<?= $linterface->GET_SMALL_BTN($button_select, "button", "javascript:newWindow('/home/common_choose/index.php?page_title=SelectMembers&permitted_type=1&fieldname=approvalUserID[]&excluded_type=4', 9)")?><br>
									<?= $linterface->GET_SMALL_BTN($button_remove, "button", "javascript:checkOptionRemove(document.form1.elements['approvalUserID[]']);");?>
									</div>
								</td>
							</tr>
						</table>
					</div>
				</span></td>
			</tr>
<!-- villa 2016-09-23 -->
			<?php if($plugin['eClassApp']){?>
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eClassApp']['DefaultUserNotifyUsingApp']?></td>
				<td> 
    				<span class="Edit_Hide"><?=$lnotice->sendPushMsg ? $Lang['General']['Yes']:$Lang['General']['No']?></span>
    				<span class="Edit_Show" style="display:none">
        				<input type="radio" id= "sendPushMsg_yes" name="sendPushMsg" value="1" <?=$lnotice->sendPushMsg ? "checked":"" ?>> <label for="sendPushMsg_yes"><?=$Lang['General']['Yes']?></label> 
        				<input type="radio" id= "sendPushMsg_no" name="sendPushMsg" value="0" <?=$lnotice->sendPushMsg ? "":"checked" ?>> <label for="sendPushMsg_no"><?=$Lang['General']['No']?></label>
					</span>
				</td>
			</tr>
			<?php }?>
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eNotice']['DefaultUserNotifyUsingEmail']?></td>
				<td> 
    				<span class="Edit_Hide"><?=$lnotice->sendEmail ? $Lang['General']['Yes']:$Lang['General']['No']?></span>
    				<span class="Edit_Show" style="display:none">
        				<input type="radio" id= "sendEmail_yes" name="sendEmail" value="1" <?=$lnotice->sendEmail? "checked":"" ?>> <label for="sendEmail_yes"><?=$Lang['General']['Yes']?></label> 
        				<input type="radio" id= "sendEmail_no" name="sendEmail" value="0" <?=$lnotice->sendEmail? "":"checked" ?>> <label for="sendEmail_no"><?=$Lang['General']['No']?></label>
					</span>
				</td>
			</tr>
	</table>
	
	<? if($plugin['Disciplinev12'] && $_SESSION["platform"] != "KIS") {?>
	<p class="spacer"></p>
	<div class="form_sub_title_v30"><em>- <span class="field_title"> <?=$Lang['Header']['Menu']['eDiscipline']?> </span>-</em>
		<p class="spacer"></p>
	</div>
	<table class="form_table_v30">
	
	<tr valign='top'>
		<td class="field_title" nowrap><?=$Lang['eNotice']['DisciplineGroup']?></td>
		<td>
    		<span class="Edit_Hide"><?=$DisciplineGroupTitle ? $DisciplineGroupTitle : $i_general_NotSet ?></span>
    		<span class="Edit_Show" style="display:none"><?=$DisciplineGroupSelect?> (<?=$Lang['eNotice']['AdminGroupOnly']?>)</span>
		</td>
	</tr> 
	
	<tr valign='top'>
		<td class="field_title" nowrap><?=$Lang['eNotice']['DefaultNumDays']?></td>
		<td>
    		<span class="Edit_Hide"><?=($lnotice->defaultDisNumDays ? $lnotice->defaultDisNumDays : $lnotice->defaultNumDays)?></span>
    		<span class="Edit_Show" style="display:none"><?=$defaultDisNumDays_selection?></span>
		</td>
	</tr>
	
	<tr valign='top'>
		<td class="field_title" nowrap><?=$Lang['eNotice']['AllowClassTeacerAccessDisciplineNotice']?></td>
		<td>
			<span class="Edit_Hide"><?=$lnotice->ClassTeacherCanAccessDisciplineNotice ? $i_general_yes:$i_general_no?></span>
			<span class="Edit_Show" style="display:none">
    			<input type="radio" name="ClassTeacherCanAccessDisciplineNotice" value="1" <?=$lnotice->ClassTeacherCanAccessDisciplineNotice ? "checked":"" ?> id="ClassTeacherCanAccessDisciplineNotice1"> <label for="ClassTeacherCanAccessDisciplineNotice1"><?=$i_general_yes?></label> 
    			<input type="radio" name="ClassTeacherCanAccessDisciplineNotice" value="0" <?=$lnotice->ClassTeacherCanAccessDisciplineNotice ? "":"checked" ?> id="ClassTeacherCanAccessDisciplineNotice0"> <label for="ClassTeacherCanAccessDisciplineNotice0"><?=$i_general_no?></label>
			</span>
		</td>
	</tr> 
	
	</table>
	<? } ?>
	
	<?php if($plugin['eClassTeacherApp']){?>
	<div class="form_sub_title_v30"><em>- <span class="field_title"> <?=$Lang['eClassApp']['TeacherApp']?> </span>-</em>
		<p class="spacer"></p>
	</div>
	
	<table class="form_table_v30">
	<tr valign='top'>
		<td class="field_title" nowrap><?=$Lang['eNotice']['TeacherCanSeeAllNotice']?></td>
		<td>
			<span class="Edit_Hide"><?=$lnotice->teacherCanSeeAllNotice ? $i_general_yes:$i_general_no?></span>
			<span class="Edit_Show" style="display:none">
    			<input type="radio" name="teacherCanSeeAllNotice" value="1" <?=$lnotice->teacherCanSeeAllNotice ? "checked":"" ?> id="teacherCanSeeAllNotice1" onclick='canSeeAll()'> <label for="teacherCanSeeAllNotice1"><?=$i_general_yes?></label> 
    			<input type="radio" name="teacherCanSeeAllNotice" value="0" <?=$lnotice->teacherCanSeeAllNotice ? "":"checked" ?> id="teacherCanSeeAllNotice0" onclick='canSeeAll()'> <label for="teacherCanSeeAllNotice0"><?=$i_general_no?></label>
			</span>
		</td>
	</tr>
	<tr valign='top'>
		<td class="field_title" nowrap><?=$Lang['eNotice']['AccessGroupCanSeeAllNotice']?></td>
		<td>
			<span class="Edit_Hide"><?=$lnotice->accessGroupCanSeeAllNotice ? $i_general_yes:$i_general_no?></span>
			<span class="Edit_Show" style="display:none">
    			<input type="radio" name="accessGroupCanSeeAllNotice" value="1" <?=$lnotice->accessGroupCanSeeAllNotice ? "checked":"" ?> id="accessGroupCanSeeAllNotice1"> <label for="accessGroupCanSeeAllNotice1"><?=$i_general_yes?></label> 
    			<input type="radio" name="accessGroupCanSeeAllNotice" value="0" <?=$lnotice->accessGroupCanSeeAllNotice ? "":"checked" ?> id="accessGroupCanSeeAllNotice0"> <label for="accessGroupCanSeeAllNotice0"><?=$i_general_no?></label>
			</span>
		</td>
	</tr>
	</table>
	<? } ?>
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($button_update, "submit","return goSubmit(document.form1);","UpdateBtn", "style='display: none'") ?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "ResetInfo();","cancelbtn", "style='display: none'") ?>
		<p class="spacer"></p>
	</div>
</div>

<?php echo csrfTokenHtml(generateCsrfToken()); ?>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>