<?php

# using by  anna
/*
 * 	
 */

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");


intranet_auth();
intranet_opendb();
$lnotice = new libnotice();
$lgeneralsettings = new libgeneralsettings();

if (! $plugin['notice'] || ! $_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || !$plugin['eClassApp']) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}


if( empty( $_POST['notify'] ) ) {
	header("Location: index.php");	
}


$result[] =$lgeneralsettings-> Save_General_Setting($lnotice->ModuleName,$_POST['notify']);

if($_POST['notify2']){	
	$result[] = $lgeneralsettings-> Save_General_Setting($lnotice->ModuleName,$_POST['notify2']);
}

$xmsg = in_array(false,$result) ? 'UpdateUnsuccess' : 'UpdateSuccess';
 	
intranet_closedb();
header("Location: index.php?xmsg=$xmsg");
?>
