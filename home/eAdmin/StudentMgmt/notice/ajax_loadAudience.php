<?php
# using: 

################################
#
#   Date:   2020-05-04  Bill    [2020-0407-1445-44292]
#           - support class teacher audience limit checking + UI update ($sys_custom['eNotice']['ClassTeacherSelectOwnClassStudentOnly'])
#
#	Date:	2017-06-13	Bill	[2017-0524-1550-19235]
#			Improved: Hide Class Level if without any classes
#
#	Date:	2014-06-26	Omas
#			Improved: Type 4 return autocomplete
#
#	Date:	2014-05-05	YatWoon
#			Fixed: incorrect U/G ID from audience, use function returnIndividualTypeNames2() instead of returnIndividualTypeNames()
#
#	Date:	2011-12-14	YatWoon
#			add UserRecordStatus parameter to common_choose, allow select suspended student
#
#	Date:	2010-12-20	YatWoon
#			change student selection to "/home/common_choose/index.php"
#
################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");

intranet_auth();
intranet_opendb();

$lnotice = new libnotice();
// $lnotice->returnRecord($templateID);

$lclass = new libclass();

// [2020-0407-1445-44292]
$isAudienceLimited = $lnotice->isClassTeacherAudienceTargetLimited();

$nextSelect = "";
if ($type == 1)         # Whole School
{
//     	$nextSelect = "";
//     	$type_selection = "$i_Notice_RecipientTypeAllStudents";
}
else if ($type == 2)	# Some Levels
{
	$lvls = $lclass->getLevelArray();

	$templateLvl = explode(",",$RecipientID);
	
 	for ($i=0; $i<sizeof($lvls); $i++)
 	{
		list($id, $name) = $lvls[$i];
		
		// [2017-0524-1550-19235] Skip if Class Level without any classes
		$formLevelClassList = $lclass->returnClassListByLevel($id);
		if(empty($formLevelClassList)) {
			continue;
        }
		
		if (isset($templateLvl) && sizeof($templateLvl) != 0) {
			$str = (in_array($id,$templateLvl) ? "CHECKED" : "");
        }
        else {
			$str = "";
        }
		$nextSelect .= "<input type='checkbox' name='target[]' value='$id' $str id='target_$i'><label for='target_$i'>$name</label> \n";
 	}
 
	$nextSelect .= "</td></tr>\n";
}
else if ($type == 3)	# Some Classes
{
	$templateClass = explode(",", $RecipientID);

 	$classes = $lclass->getClassList();

    // [2020-0407-1445-44292] filter not teaching classes
    if($isAudienceLimited)
    {
        $teaching_class_list = $lnotice->getClassTeacherCurrentTeachingClass();
        $teaching_class_ids = Get_Array_By_Key($teaching_class_list, 'ClassID');

        $class_size = sizeof($classes);
        for ($i=0; $i<$class_size; $i++)
        {
            list($classid, $name, $lvlID) = $classes[$i];
            if(!in_array($classid, $teaching_class_ids)) {
                unset($classes[$i]);
            }
        }
        $classes = array_values($classes);
    }

 	$prevID = $classes[0][2];
    for ($i=0; $i<sizeof($classes); $i++)
 	{
		list($classid, $name, $lvlID) = $classes[$i];
		if (isset($templateClass) && sizeof($templateClass) != 0) {
			$str = (in_array($classid, $templateClass)? "CHECKED" : "");
        }
        else {
		    $str = "";
        }
		
		if ($lvlID == $prevID) {
			$nextSelect .= " ";
		}
		else {
			$nextSelect .= "<br>\n";
			$prevID = $lvlID;
		}
		
		$nextSelect .= "<input type='checkbox' name='target[]' value='$classid' $str id='target_$i'><label for='target_$i'>$name</label> ";
 	}
}
else if ($type == 4)	# Select Audience
{
		include_once($PATH_WRT_ROOT."includes/libinterface.php");
		include_once($PATH_WRT_ROOT."includes/libuser.php");
		
		$linterface = new interface_html();
		$lu = new libuser();
				
		$option = "";
		$recipient_name_ary = array();
		if($RecipientID)
		{
 			$student_ary = explode(",",$RecipientID);
			//$recipient_name_ary = $lnotice->returnIndividualTypeNames($RecipientID);
			$recipient_name_ary = $lnotice->returnIndividualTypeNames2($RecipientID);
			
//			for($i=0; $i<sizeof($recipient_name_ary); $i++)
//			{
//				//$option .= "<option value='". $student_ary[$i] ."'>". $recipient_name_ary[$i] ."</option>";
//				$option .= "<option value='". $recipient_name_ary[$i]['tempID'] ."'>". $recipient_name_ary[$i]['tempName'] ."</option>";
//			}
		}
		else
		{
//			for($i = 0; $i < 40; $i++) $space .= "&nbsp;";
//			$option = "<option>$space</option>";
		}

		### Choose Member Btn
        $common_choose_parm = 'fieldname=target[]&page_title=SelectAudience&permitted_type=2&DisplayGroupCategory=1&UserRecordStatus=0,1';
        if($isAudienceLimited) {
            // [2020-0407-1445-44292] add 2 params passed to 'home/common_choose/index.php' to show teaching class students only
            $common_choose_parm = 'fieldname=target[]&page_title=SelectAudience&permitted_type=2&DisplayGroupCategory=0&UserRecordStatus=0,1&filterClassTeacherClass=1&filterOwnClassesOnly=1';
        }
        // $btn_ChooseMember = $linterface->GET_SMALL_BTN($Lang['Btn']['Select'], "button", "newWindow('/home/common_choose/index.php?fieldname=target[]&page_title=SelectAudience&permitted_type=2&DisplayGroupCategory=1&UserRecordStatus=0,1',16)");
		$btn_ChooseMember = $linterface->GET_SMALL_BTN($Lang['Btn']['Select'], "button", "newWindow('/home/common_choose/index.php?".$common_choose_parm."',16)");
		
		### Auto-complete ClassName ClassNumber StudentName search
		$UserClassNameClassNumberInput = '';
		$UserClassNameClassNumberInput .= '<div style="float:left;">';
			$UserClassNameClassNumberInput .= '<input type="text" id="UserClassNameClassNumberSearchTb" name="UserClassNameClassNumberSearchTb" value="" />';
		$UserClassNameClassNumberInput .= '</div>';
		
		### ClassName ClassNumber StudentName Textarea input
		$UserClassNameClassNumberTextarea = '';
		$UserClassNameClassNumberTextarea .= '<div style="float:left;" class="student_textarea">';
		$UserClassNameClassNumberTextarea .= '<textarea style="height:100px;" id="UserClassNameClassNumberAreaSearchTb" name="UserClassNameClassNumberAreaSearchTb"></textarea><span id="textarea_studentmsg"></span><br><br>';
		$UserClassNameClassNumberTextarea .= $linterface->GET_SMALL_BTN($Lang['AppNotifyMessage']['searchAndInsertStudent'], "button" , "");
		$UserClassNameClassNumberTextarea .= '</div>';
		
		### Auto-complete login search
		$UserLoginInput = '';
		$UserLoginInput .= '<div style="float:left;">';
			$UserLoginInput .= '<input type="text" id="UserLoginSearchTb" name="UserLoginSearchTb" value="" />';
		$UserLoginInput .= '</div>';
		
		### Student Selection Box & Remove all Btn
		$MemberSelectionBox = $linterface->GET_SELECTION_BOX($recipient_name_ary, "name='target[]' id='target[]' class='select_studentlist' size='15' multiple='multiple'", "");
		$btn_RemoveSelected = $linterface->GET_SMALL_BTN($Lang['Btn']['RemoveSelected'], "button", "js_Remove_Selected_Student();");

		$nextSelect .= "<table>";
		$nextSelect .= '<tr>
									<td class="tablerow2" valign="top">
										<table width="100%" border="0" cellpadding="3" cellspacing="0">
										<tr>
											<td class="tabletext">'.$i_general_from_class_group.'</td>
										</tr>
										<tr>
											<td class="tabletext">'.$btn_ChooseMember.'</td>
										</tr>
										<tr>
											<td class="tabletext"><i>'.$Lang['General']['Or'].'</i></td>
										</tr>
										<tr>
											<td class="tabletext">
												'.$i_general_search_by_inputformat.'
												<br />
												'.$UserClassNameClassNumberInput.'
											</td>
										</tr>
										</table>
									</td>
									<td class="tabletext" ><img src="'. $image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>
									<td class="tablerow2">
										<table width="100%" border="0" cellpadding="3" cellspacing="0">
										<tr>
											<td><i>'.$Lang['General']['Or'].'</i></td>
										</tr>
										<tr>
											<td class="tabletext">
												'.$Lang['AppNotifyMessage']['msgSearchAndInsertInfo'].'
												<br />
												'.$UserClassNameClassNumberTextarea.'
											</td>
										</tr>
										</table>
									</td>
									<td class="tabletext" ><img src="'. $image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>
									<td align="left" valign="top">
										<table width="100%" border="0" cellpadding="5" cellspacing="0">
										<tr>
											<td align="left">
												'. $MemberSelectionBox .'
												'.$btn_RemoveSelected.'
											</td>
										</tr>
										<tr>
											<td>
												'.$linterface->Get_Thickbox_Warning_Msg_Div("MemberSelectionWarningDiv", '* '.$Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['SelectionContainsMember']).'
											</td>
										</tr>
										</table>
									</td>
								</tr>';
		$nextSelect .= "</table>";
		
//		$nextSelect .= "<table border='0' cellspacing='1' cellpadding='1'>";
//		$nextSelect .= "<tr>";
//		$nextSelect .= "<td>";
//		
//		$nextSelect .= "<select name='target[]' size='4' multiple>$option</select>";
//		$nextSelect .= "</td>";
//		$nextSelect .= "<td>";
//		//$nextSelect .= $linterface->GET_BTN($i_frontpage_campusmail_choose." old", "button","newWindow('choose/index.php?fieldname=target[]',9)") . "<br>";
//		$nextSelect .= $linterface->GET_BTN($i_frontpage_campusmail_choose, "button","newWindow('/home/common_choose/index.php?fieldname=target[]&page_title=SelectAudience&permitted_type=2&DisplayGroupCategory=1&UserRecordStatus=0,1',16)") . "<br>";
//		$nextSelect .= $linterface->GET_BTN($i_frontpage_campusmail_remove, "button","checkOptionRemove(document.form1.elements['target[]'])") . "<br>";
//		$nextSelect .= "</td>";
//		$nextSelect .= "</tr>";
//		$nextSelect .= "</table>";
	
	}

intranet_closedb();

echo $nextSelect;
	
?>