<?php
//Using : 

/* Modification log:
 *	
 * 2019-06-06 [Bill]
 *   - add CSRF token checking
 *   
 * 2015-07-20 [Ivan] (ip.2.5.6.7.1)
 * 	- Added sms confirmation page logic
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

if(!verifyCsrfToken($_POST['csrf_token'], $_POST['csrf_token_key'], $do_not_remove_token_data=true)){
    echo "<font color='red'>".$Lang['AppNotifyMessage']['eNotice']['send_failed']."</font>";
    exit;
}

intranet_auth();
intranet_opendb();

################################
# Pass in:
#	$NoticeID
#   $type (1: view own class)
################################
$NoticeID = IntegerSafe($NoticeID);

$MODULE_OBJ['title'] = $Lang['AppNotifyMessage']['SMS'];
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$lnotice = new libnotice($NoticeID);
$lu = new libuser($UserID);

if (!$lu->isTeacherStaff()) {
     include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT($i_general_no_access_right);
		exit;
}

### Warning
$WarningBox = $linterface->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Note'].'</font>', $Lang['SMS']['RemarkChargeForSMS'], $others="");

### SMS content
$NoticeNumber = "".$lnotice->NoticeNumber;
$NoticeTitle = "".$lnotice->Title;
$NoticeDeadline = "".$lnotice->DateEnd;
$MessageTitle_sms = str_replace("[NoticeNumber]", $NoticeNumber, $Lang['AppNotifyMessage']['eNotice']['Title']['SMS']);
$MessageContent_sms = str_replace("[NoticeNumber]", $NoticeNumber, $Lang['AppNotifyMessage']['eNotice']['Alert']['SMS']);
$MessageContent_sms = str_replace("[NoticeTitle]", $NoticeTitle, $MessageContent_sms);
$MessageContent_sms = str_replace("[NoticeDeadline]", $NoticeDeadline, $MessageContent_sms);
$SMSContent = $MessageTitle_sms."\n".$MessageContent_sms;

### Buttons
$back_button = $linterface->GET_ACTION_BTN($button_cancel, "button", "goBack();");
$send_button = $linterface->GET_ACTION_BTN($button_send, "button", "goSubmit();");

if ($intranet_session_language == "en") {
    $langStr = "English";
}
else {
    $langStr = "Chinese";
}

if ($fromPage == 'tableview') {
	$targetPhp = '../../../eService/notice/tableview.php';
}
else if ($fromPage == 'result') {
	$targetPhp = 'result.php';
}

?>
<script language="javascript" src="/templates/sms.js"></script>
<script language="javascript" type="text/javascript">
$(document).ready(function() {
	countit(document.form1, 0, '<?=$langStr?>');
});

function goBack() {
	$('form#form1').attr('action', '<?=$targetPhp?>').submit();
}

function goSubmit() {
	var canSubmit = true;
	if(document.form1.Message.value.length == 0){
		canSubmit = false;
	}
	
	countit(document.form1, 0, '<?=$langStr?>');
	
	if (canSubmit) {
		$('div#actionBtnDiv').hide();
		$('div#sendingSmsDiv').show();
		
		$('input#directSend').val(1);
		$('form#form1').attr('action', '<?=$targetPhp?>').submit();
	}
}
</script>

<br />
<form name="form1" id="form1" method="post">
<?=$WarningBox?>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?=$i_SMS_MessageContent?>
					</td>
					<td class="tabletext" width="70%">
						<TEXTAREA NAME="Message" id="Message" COLS="50" ROWS="4" WRAP="virtual" onKeyUp="countit(this.form,0,'<?=$langStr?>');" onpaste="setTimeout(function() { countit(document.form1,0,'<?=$langStr?>'); }, 10)" ondrop="setTimeout(function() { countit(document.form1,0,'<?=$langStr?>'); }, 10);"><?=$SMSContent?></TEXTAREA><FONT>&nbsp;</FONT><INPUT TYPE=TEXT NAME="Size" VALUE="160" SIZE="3" MAXLENGTH="3">
						<span class="tabletextremark">
			  			  <br /><?=$Lang['General']['Caution']?>
						  <li><?=$Lang['SMS']['LimitationArr'][0]['Content']?></li>
						  <li><?=$Lang['SMS']['LimitationArr'][1]['Content']?></li>
						  <li><?=$Lang['SMS']['LimitationArr'][2]['Content']?></li>
						</span>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
			  	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			  </tr>
				<tr>
				  <td align="center" colspan="2">
				  		<div id="actionBtnDiv">
							<?= $send_button."&nbsp;".$back_button ?>
						</div>
						<div id="sendingSmsDiv" class="tabletextrequire" style="display:none;">
							<?=$Lang['General']['Submitting...']?>
						</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" name="NoticeID" id="NoticeID" value="<?=$NoticeID?>">
<input type="hidden" name="class" id="class" value="<?=$class?>">
<input type="hidden" name="all" value="<?=$all?>">
<input type="hidden" name="type" value="<?=$type?>">

<input type="hidden" name="chinesechar" id="chinesechar" VALUE="">
<input type="hidden" name="Textsize" id="Textsize" VALUE="">
<input type="hidden" name="directSend" id="directSend" VALUE="">
<input type="hidden" name="sendMethod" id="sendMethod" VALUE="<?=$sendMethod?>">

</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>