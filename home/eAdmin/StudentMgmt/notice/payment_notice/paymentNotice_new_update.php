<?php
# using: 

######################
#
# 	Date:   2020-09-07 (Bill)    [2020-0604-1821-16170]
#			access right checking > replace hasIssueRight() by hasPaymentNoticeIssueRight()
#
#   Date:   2020-06-29  Ray
#           Added multi payment gateway
#
#	Date:	2019-10-14  Philips
#			Added $pushmessagenotify_PIC, $pushmessagenotify_ClassTeachers, $emailnotify_Students, $emailnotify_PIC, $emailnotify_ClassTeachers
#
#   Date:   2019-09-19  Bill    [2019-0917-1522-01206]
#           skip student status checking when send push message to parents
#
#	Date:	2019-04-08  Carlos
#			Improved reply slip with new php library eNoticePaymentReplySlip.php
#
#   Date:   2019-01-25 Bill     [2019-0118-1125-41206]
#           Improved: Description - support text input from FCKEditor
#
#	Date:	2018-10-04 Bill
#			get noticeAttFolder from $_POST 
#
#	Date:	2018-09-28 Bill		[2018-0809-0935-01276]
#			Support New Payment Question Type - Must Pay
#
#	Date:	2018-08-21 Carlos 
#			$sys_custom['ePayment']['Alipay'] - add selection option to set Alipay merchant account for payment item.
#
#	Date:	2016-11-24 (Bill)	[2016-1124-1120-39066]
#			fixed cannot update notice if reply slip contain single quote
#
#	Date:	2016-09-26	Villa
#			- send push msg in specific time
#
#	Date:	2016-07-11	Kenneth
#			- add approval logic
#
#	Date:	2016-06-06	Kenneth
#			- add '23:59:59' into DateEnd
#
#	Date:	2016-02-17	Bill	[2016-0217-1403-50207]
#			fixed: release push message even if notice cannot created
#			- redirect to paymentNotice.php with error message if $NoticeID is empty
#
#	Date:	2015-12-11	Bill
#			- replace session_unregister() by session_unregister_intranet() for PHP 5.4
#
#	Date:	2015-05-11 Omas	[Case #J73013]
#			- enabled send module mail new footer
#
#	Date:	2015-04-14 	Bill	[2015-0323-1602-46073]
#			- import UserID of PICs to INTRANET_NOTICE_PIC
#
#	Date:	2014-10-30 Ivan > ip.2.5.5.10.1
#			Fixed: special character handling in reply slip
#
#	Date:	2014-10-14 Roy	> ip.2.5.5.10.1
#			Improved: allow send push message to parent
#
#	Date:	2014-06-23 YatWoon	> ip.2.5.5.8.1
#			Improved: Revised system return message method [Case#Q63447]
#
#	Date:	2014-04-01 Carlos
#			Modified template would not pre-create payment item records	
#
#	Date:	2014-03-19 Carlos
#			Fixed reply slip question #PaymentItemID# replacement problem for template questions and new questions
#
#	Date:	2012-06-20 YatWoon
#			Fixed: If edit the reply slip again, the question with "enter" will become "<br>"
#
#	Date:	2011-03-25	YatWoon
#			change email notification subject & content data 
#
#	Date:	2011-02-16	YatWoon
#			Add option "Display question number"
#
######################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libemail.php");
include_once($PATH_WRT_ROOT."includes/libsendmail.php");
// include_once($PATH_WRT_ROOT."lang/email.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once($PATH_WRT_ROOT."includes/eNoticePaymentReplySlip.php");

intranet_auth();
intranet_opendb();

$lnotice = new libnotice();
$lf = new libfilesystem();
$lpayment = new libpayment();
$libeClassApp = new libeClassApp();
$eNoticePaymentReplySlip = new eNoticePaymentReplySlip();

// [2020-0604-1821-16170]
//if ($lnotice->disabled || !$lnotice->hasIssueRight())
if ($lnotice->disabled || !$lnotice->hasPaymentNoticeIssueRight())
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$NoticeNumber = intranet_htmlspecialchars(trim($NoticeNumber));
$Title = intranet_htmlspecialchars(trim($Title));

// [2019-0118-1125-41206] handle Description input from FCKEditor
if ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod")
{
    if ($Description==strip_tags($Description))
    {
        $Description = nl2br($Description);
    }
}
$Description = intranet_htmlspecialchars(trim($Description));
// $qStr = intranet_htmlspecialchars(trim($qStr));

// [2016-1124-1120-39066] for 1st update SQL for INTRANET_NOTICE - keep slashes in Question
$original_qStr = trim($qStr);

// [2016-1124-1120-39066] for create / edit payment items and 2nd update SQL for INTRANET_NOTICE - remove slashes in Question for futhur handling 
$qStr = standardizeFormPostValue($qStr);

$pushMsgNoticeNumber = standardizeFormPostValue($_POST['NoticeNumber']);
$pushMsgTitle = standardizeFormPostValue($_POST['Title']);

$moduleName = 'Payment';
$AllFieldsReq = $AllFieldsReq==1?1:0;
$DisplayQuestionNumber = $DisplayQuestionNumber==1 ? 1 : 0;

$luser = new libuser($UserID);

$AttachmentStr = (sizeof($Attachment)==0) ? "" : implode(",", $Attachment);
$AttachmentStr = stripslashes($AttachmentStr);

$noticeAttFolderName = $_POST['noticeAttFolder'];
$path = "$file_path/file/notice/".$noticeAttFolderName;

$lu = new libfilesystem();
$command ="mv ".OsCommandSafe($path)."tmp ".OsCommandSafe($path);


exec($command);

		if($bug_tracing['notice_attachment_log']){
             $temp_log_filepath = "$file_path/file/log_notice_attachment.txt";
             $temp_time = date("Y-m-d H:i:s");
             $temp_user = $UserID;
             $temp_page = 'notice_new_update.php';
             $temp_action=$command;
             $temp_content = get_file_content($temp_log_filepath);
             $temp_logentry = "\"$temp_time\",\"$temp_user\",\"$temp_page\",\"$temp_action\"\n";
             $temp_content .= $temp_logentry;
             write_file_content($temp_content, $temp_log_filepath);
		 }

$lo = new libfiletable("", $path, 0, 0, "");
$files = $lo->files;
while (list($key, $value) = each($files)) {
     if(!strstr($AttachmentStr,$files[$key][0])){
	     	if($bug_tracing['notice_attachment_log']){
             $temp_log_filepath = "$file_path/file/log_notice_attachment.txt";
             $temp_time = date("Y-m-d H:i:s");
             $temp_user = $UserID;
             $temp_page = 'notice_new_update.php';
             $temp_action="remove file:".$path."/".$files[$key][0];
             $temp_content = get_file_content($temp_log_filepath);
             $temp_logentry = "\"$temp_time\",\"$temp_user\",\"$temp_page\",\"$temp_action\"\n";
             $temp_content .= $temp_logentry;
             write_file_content($temp_content, $temp_log_filepath);
		 }
          $lu->file_remove($path."/".$files[$key][0]);
     }
}
$IsAttachment = (isset($Attachment)) ? 1 : 0;
$Attachment = $noticeAttFolderName;

if ($IsAttachment)
{
}
else
{
    if ($noticeAttFolderName != ""){
	    if($bug_tracing['notice_attachment_log']){
             $temp_log_filepath = "$file_path/file/log_notice_attachment.txt";
             $temp_time = date("Y-m-d H:i:s");
             $temp_user = $UserID;
             $temp_page = 'notice_new_update.php';
             $temp_action="remove path:".$path;
             $temp_content = get_file_content($temp_log_filepath);
             $temp_logentry = "\"$temp_time\",\"$temp_user\",\"$temp_page\",\"$temp_action\"\n";
             $temp_content .= $temp_logentry;
             write_file_content($temp_content, $temp_log_filepath);
             $temp_cmd = "mv ".OsCommandSafe($path)." ".OsCommandSafe($path)."_bak";
        	 exec($temp_cmd);

		 }else{
        	 $lu->lfs_remove($path);
         }
    }
}

if (isset($target) && sizeof($target)!=0)
    $targetID = implode(",",$target);
    
# Store to DB
$IssueUserName = $luser->getNameForRecord();
$DateEnd = $DateEnd.' '.$DateEndTime;
$DateStart = $DateStart.' '.$DateStartTime;

$fieldname = "Module,NoticeNumber,Title,Description,DateStart,DateEnd,IssueUserID,IssueUserName";
$fieldname .= ",RecipientID,Question,Attachment,RecordType,RecordStatus,DebitMethod,DateInput,DateModified,AllFieldsReq,DisplayQuestionNumber,isPaymentNoticeApplyEditor";
$values = "'$moduleName','$NoticeNumber','$Title','$Description','$DateStart','$DateEnd','$UserID','$IssueUserName'";
$values .= ",'$targetID','$original_qStr','$Attachment','$type','$sus_status','$debit_method',now(),now(),'$AllFieldsReq','$DisplayQuestionNumber','1'";
if($sus_status==1){
	$fieldname .= ',ApprovedBy,ApprovedTime ';
	$values .= ",'$UserID',now() ";
}

$extraKeyValues = array();
$use_merchant_account = $lpayment->useEWalletMerchantAccount();

$multi_marchant_account_ids = array();
if($use_merchant_account) {
	if ($sys_custom['ePayment']['MultiPaymentGateway']) {
		$last_MerchantAccountID_value = '';
		$service_provider_list = $lpayment->getPaymentServiceProviderMapping(false, true);
		foreach ($service_provider_list as $k => $temp) {
			$MerchantAccountID_value = ${'MerchantAccountID_' . $k};
			if ($MerchantAccountID_value == '') {
				continue;
			}
			$last_MerchantAccountID_value = $MerchantAccountID_value;
			$multi_marchant_account_ids[] = $MerchantAccountID_value;
		}
		$extraKeyValues = array('MerchantAccountID'=>$last_MerchantAccountID_value);
		$fieldname .= ",MerchantAccountID";
		$values .= ",".(($last_MerchantAccountID_value == '') ? 'NULL' : "'$last_MerchantAccountID_value'")."";
	} else {
		if($_POST['MerchantAccountID'] != ''){
			$extraKeyValues = array('MerchantAccountID'=>$_POST['MerchantAccountID']);
			$fieldname .= ",MerchantAccountID";
			$values .= ",'".$_POST['MerchantAccountID']."'";
		}
	}
}

$sql = "INSERT INTO INTRANET_NOTICE ($fieldname) VALUES ($values)";
$lnotice->db_db_query($sql);
$NoticeID = $lnotice->db_insert_id();

// [2016-0217-1403-50207] redirect to paymentNotice.php if $NoticeID is empty
if(empty($NoticeID)){
	header("Location: paymentNotice.php?xmsg=AddUnsuccess&pageNo=$pageNo&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&noticeType=$noticeType&status=$status&year=$year&month=$month");
    intranet_closedb();
	exit();
}


if($use_merchant_account) {
	if ($sys_custom['ePayment']['MultiPaymentGateway']) {
		foreach ($multi_marchant_account_ids as $MerchantAccountID_value) {
			$sql = "INSERT INTO INTRANET_NOTICE_PAYMENT_GATEWAY
						(NoticeID, MerchantAccountID, DateInput, InputBy)
						VALUES
						($NoticeID, '$MerchantAccountID_value', now(), '" . $_SESSION['UserID'] . "')
						";
			$Result['CreateNoticePaymentGateway' . $MerchantAccountID_value] = $lnotice->db_db_query($sql);
		}
	}
}

# Add PICs to DB
if(isset($target_PIC) && sizeof($target_PIC) != 0)
{
	$fieldname = "NoticeID,PICUserID,InputBy,DateInput,ModifiedBy,DateModified";
	$values = "";
	
	$delim = "";
	$target_PIC = array_unique($target_PIC);
	for($i=0; $i<sizeof($target_PIC); $i++) {
		$this_PIC = $target_PIC[$i];
		$this_PIC = str_replace("&#160;", "", $this_PIC);
		$this_PIC = str_replace("U", "", $this_PIC);
		if(trim($this_PIC) != "") {
			$values .= "$delim ('$NoticeID','$this_PIC','$UserID',now(),'$UserID',now())";
			$delim = ",";
		}
	}
	
	$sql = "INSERT INTO INTRANET_NOTICE_PIC ($fieldname) VALUES $values";
	$lnotice->db_db_query($sql);
}

if($moduleName == "Payment")
{
	$is_template = $sus_status == TEMPLATE_RECORD;
	/*
	//$arrPaymentItemDetails = $lnotice->returnPaymentItemAndCategoryID($qStr);
	$arrPaymentItemDetails = $lnotice->returnPaymentItemDetails($qStr); // 2014-03-19 use returnPaymentItemDetails() with PaymentItemID instead of returnPaymentItemAndCategoryID()
	$notice_array = $lnotice->splitQuestion($qStr);
	//debug_pr($arrPaymentItemDetails); 
	//exit;
	if(sizeof($arrPaymentItemDetails)>0)
	{
		foreach($arrPaymentItemDetails as $key=>$details)
		{
			$payment_type = $notice_array[$key-1][0];
			switch($payment_type)
			{
			    case 1:
			    case 12:
						$PaymentItemName = $details['PaymentItem'];
						$PaymentCategroyID = $details['PaymentCategory'];
						$NonPayItem = $details['NonPayItem'];	
						//echo $NonPayItem.'/'; exit;
						
						if($is_template) {
							$str_PaymentID = "#PAYMENTITEMID#1";
						} else if(!$NonPayItem) {
							### Create Payment Item ###
							$payment_item_id = $lpayment->Create_Notice_Payment_Item($PaymentItemName, $DateStart, $DateEnd, $NoticeID, $PaymentCategroyID, $extraKeyValues);
						
							$delim_itemid = "#PAYMENTITEMID#";
							$str_PaymentID = $delim_itemid.$payment_item_id;
						} else {
							$str_PaymentID = "#PAYMENTITEMID#1";
						}
						
						### In here , replace the #PAYMENTITEMID# in the original $qStr, then update the record in INTRANET_NOTICE 
						$qSeparator = "#QUE#";
						$strDetails = explode($qSeparator,$qStr);
						if(strpos($strDetails[$key],"#PAYMENTITEMID#1||")!==false) {       // New question
							$temp_str = str_replace("#PAYMENTITEMID#1",$str_PaymentID,$strDetails[$key]);
						} else {      // Template question with PaymentItemID
							$temp_str = str_replace("#PAYMENTITEMID#".$details['PaymentItemID'],$str_PaymentID,$strDetails[$key]);
						}
						$temp_str = $qSeparator.$temp_str;
						$arrFinalStr[] = $temp_str;
						break;
				case 2:
						$PaymentItemName = $details['PaymentItem'];
						$PaymentCategroyID = $details['PaymentCategory'];
						$NonPayItem = $details['NonPayItem'];	
						
						if($is_template){
							$str_PaymentID = "#PAYMENTITEMID#1";
						}else if(!$NonPayItem) {
							### Create Payment Item ###
							$payment_item_id = $lpayment->Create_Notice_Payment_Item($PaymentItemName, $DateStart, $DateEnd, $NoticeID, $PaymentCategroyID, $extraKeyValues);
				
							$delim_itemid = "#PAYMENTITEMID#";
							$str_PaymentID = $delim_itemid.$payment_item_id;
						} else {
							$str_PaymentID = "#PAYMENTITEMID#1";
						}
						### In here , replace the #PAYMENTITEMID# in the original $qStr, then update the record in INTRANET_NOTICE 
						$qSeparator = "#QUE#";
						$strDetails = explode($qSeparator,$qStr);
						if(strpos($strDetails[$key],"#PAYMENTITEMID#1||")!==false){ // New question
							$temp_str = str_replace("#PAYMENTITEMID#1",$str_PaymentID,$strDetails[$key]);
						}else{ // Template question PaymentItemID
							$temp_str = str_replace("#PAYMENTITEMID#".$details['PaymentItemID'],$str_PaymentID,$strDetails[$key]);
						}
						$temp_str = $qSeparator.$temp_str;
						$arrFinalStr[] = $temp_str;
						break;
				case 3:
						$PaymentItemNameArray = $details['PaymentItem'];
						$PaymentCategroyIDArray = $details['PaymentCategory'];
						
						if($is_template){
							$strPaymentItemID = "#PAYMENTITEMID#1";
						}else{
							for($i=0; $i<sizeof($PaymentItemNameArray); $i++){
								$PaymentItemName = $PaymentItemNameArray[$i];
								$PaymentCategroyID = $PaymentCategroyIDArray[$i];
								
								### Create Payment Item ###
								$arr_payment_item_id[$key][] = $lpayment->Create_Notice_Payment_Item($PaymentItemName, $DateStart, $DateEnd, $NoticeID, $PaymentCategroyID, $extraKeyValues);
							}
	
							//$strPaymentItemID = implode("#PAYMENTITEMID#",$arr_payment_item_id[$key]);
							$strPaymentItemID = implode(",",$arr_payment_item_id[$key]);
							$strPaymentItemID = "#PAYMENTITEMID#".$strPaymentItemID;
						}
						
						### In here , replace the #PAYMENTITEMID# in the original $qStr, then update the record in INTRANET_NOTICE 
						$qSeparator = "#QUE#";
						$strDetails = explode($qSeparator,$qStr);
						if(strpos($strDetails[$key],"#PAYMENTITEMID#1||")!==false){ // New question
							$temp_str = str_replace("#PAYMENTITEMID#1",$strPaymentItemID,$strDetails[$key]);
						}else if(strpos($strDetails[$key],"#PAYMENTITEMID#".implode("#PAYMENTITEMID#",$details['PaymentItemID'])."||")!==false){
							$temp_str = str_replace("#PAYMENTITEMID#".implode("#PAYMENTITEMID#",$details['PaymentItemID'])."||",$strPaymentItemID."||",$strDetails[$key]);
						}
						else{ // Template question with PaymentItemID
							$temp_str = str_replace("#PAYMENTITEMID#".implode(",",$details['PaymentItemID']),$strPaymentItemID,$strDetails[$key]);
						}
						$temp_str = $qSeparator.$temp_str;
						$arrFinalStr[] = $temp_str;
						break;
				case 4:
				case 5:
				case 6:
				case 7:
				case 8:
				case 9:
						$PaymentItemNameArray = $details['PaymentItem'];
						$PaymentCategroyIDArray = $details['PaymentCategory'];
						
						if($is_template){
							$strPaymentItemID = "#PAYMENTITEMID#1";
						}else{
							for($i=0; $i<sizeof($PaymentItemNameArray); $i++){
								$PaymentItemName = $PaymentItemNameArray[$i];
								$PaymentCategroyID = $PaymentCategroyIDArray[$i];
								
								### Create Payment Item ###
								$arr_payment_item_id[$key][] = $lpayment->Create_Notice_Payment_Item($PaymentItemName, $DateStart, $DateEnd, $NoticeID, $PaymentCategroyID, $extraKeyValues);
							}
							
							$strPaymentItemID = implode("#PAYMENTITEMID#",$arr_payment_item_id[$key]);
							$strPaymentItemID = "#PAYMENTITEMID#".$strPaymentItemID;
						}
						
						### In here , replace the #PAYMENTITEMID# in the original $qStr, then update the record in INTRANET_NOTICE 
						$qSeparator = "#QUE#";
						$strDetails = explode($qSeparator,$qStr);
						if(strpos($strDetails[$key],"#PAYMENTITEMID#1||")!==false){ // New question
							$temp_str = str_replace("#PAYMENTITEMID#1",$strPaymentItemID,$strDetails[$key]);
						}else if(strpos($strDetails[$key],"#PAYMENTITEMID#".implode(",",$details['PaymentItemID'])."||")!==false){
							$temp_str = str_replace("#PAYMENTITEMID#".implode(",",$details['PaymentItemID'])."||",$strPaymentItemID."||",$strDetails[$key]);
						}else{ // Template question with PaymentItemID
							$temp_str = str_replace("#PAYMENTITEMID#".implode("#PAYMENTITEMID#",$details['PaymentItemID']),$strPaymentItemID,$strDetails[$key]);
						}
						$temp_str = $qSeparator.$temp_str;
						$arrFinalStr[] = $temp_str;
						break;
				case 10:
					$qSeparator = "#QUE#";
					$arrFinalStr[] = $qSeparator.$details['ItemName'];
					break;
			}
		}
		if(sizeof($arrFinalStr)>0){
			$final_str = implode("",$arrFinalStr);
			
			$sql = "UPDATE INTRANET_NOTICE SET Question = '".$lnotice->Get_Safe_Sql_Query($final_str)."' WHERE NoticeID = '$NoticeID'";
			$lnotice->db_db_query($sql);
		}
	}
	*/
	
	$questions = $eNoticePaymentReplySlip->parseQuestionString($qStr);
	if(!$is_template){
		// create payment items if not created yet
		$questions = $eNoticePaymentReplySlip->preparePaymentItemsForQuestions($questions, $NoticeID, $DateStart, $DateEnd, $extraKeyValues);
		if($use_merchant_account) {
			if ($sys_custom['ePayment']['MultiPaymentGateway']) {
				$sql_values = array();
				$payment_ids_array = array();
				foreach ($questions as $temp) {
					if(isset($temp['Options']) && is_array($temp['Options'])) {
						foreach($temp['Options'] as $option_temp) {
							if(isset($option_temp['PaymentItemID']) && $option_temp['PaymentItemID'] != '') {
								$payment_ids_array[] = $option_temp['PaymentItemID'];
							}
						}
					} else if (isset($temp['PaymentItemID']) && $temp['PaymentItemID'] != '') {
						$payment_ids_array[] = $temp['PaymentItemID'];
					}
				}

				$payment_ids_array = array_unique($payment_ids_array);
				if (count($payment_ids_array) > 0) {
					foreach($payment_ids_array as $temp) {
						foreach ($multi_marchant_account_ids as $MerchantAccountID_value) {
							$sql_values[] = "('" . $temp . "', '$MerchantAccountID_value', now(), '" . $_SESSION['UserID'] . "')";
						}
					}
					if(count($sql_values) > 0) {
						$sql = "INSERT INTO PAYMENT_PAYMENT_ITEM_PAYMENT_GATEWAY
						(ItemID, MerchantAccountID, DateInput, InputBy)
						VALUES " . implode(" , ", $sql_values);
						$Result['CreateItemPaymentGateway'] = $lnotice->db_db_query($sql);
					}
				}
			}
		}
	}else{
		// clear the payment items for template
		$questions = $eNoticePaymentReplySlip->removePaymentItemsForQuestion($questions);
	}
	$final_qstr = $eNoticePaymentReplySlip->constructQuestionString($questions);
	$sql = "UPDATE INTRANET_NOTICE SET Question = '".$lnotice->Get_Safe_Sql_Query($final_qstr)."' WHERE NoticeID = '$NoticeID'";
	$lnotice->db_db_query($sql);
}

// [2019-0118-1125-41206]
## Update description if user upload image with Flash upload (fck) [Start]
$lnotice->updateNoticeFlashUploadPath($NoticeID, $Description);
## Update description if user upload image with Flash upload (fck) [End]	

$username_field = getNameFieldWithClassNumberEng("");

if ($type==1)         # Whole School
{
    /*
    $sql = "INSERT IGNORE INTO INTRANET_NOTICE_REPLY (NoticeID,StudentID,StudentName,RecordStatus,DateInput,DateModified)
            SELECT $NoticeID,UserID,$username_field,0,now(),now() FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1";
            $lnotice->db_db_query($sql);
            */
            $username_field = getNameFieldWithClassNumberEng("iu.");
            $sql = "select 
						ycu.UserID, $username_field
					from
						YEAR as y 
						INNER JOIN YEAR_CLASS as yc on (yc.YearID = y.YearID and yc.AcademicYearID = ". Get_Current_Academic_Year_ID() .")
						INNER JOIN YEAR_CLASS_USER as ycu on (ycu.YearClassID = yc.YearClassID)
						INNER JOIN INTRANET_USER as iu on (iu.UserID = ycu.UserID)
            		";
            $actual_target_users = $lnotice->returnArray($sql);
            
     if (sizeof($actual_target_users)!=0)
     {
         $delimiter = "";
         $values = "";
         for ($i=0; $i<sizeof($actual_target_users); $i++)
         {
              list($uid,$name,$usertype) = $actual_target_users[$i];
              $name = intranet_htmlspecialchars($name);
              $values .= "$delimiter ('$NoticeID','$uid','$name',0,now(),now())";
              $delimiter = ",";
         }
         $sql = "INSERT IGNORE INTO INTRANET_NOTICE_REPLY (NoticeID,StudentID,StudentName,RecordStatus,DateInput,DateModified)
                 VALUES $values";
		$lnotice->db_db_query($sql);
     }
    
}
else if ($type==2)    # Some levels only
{
     # Grab the class name for the levels
     $list = implode(",",$target);
     //$sql = "SELECT ClassName FROM INTRANET_CLASS WHERE ClassLevelID IN ($list)";
     $sql = "
	     		SELECT 
	     			yc.ClassTitleEN 
	     		FROM 
	     			YEAR as y 
	     			INNER JOIN YEAR_CLASS as yc on (yc.YearID = y.YearID and yc.AcademicYearID = ". Get_Current_Academic_Year_ID() .")
	     		WHERE 
	     			y.YearID IN ($list)
     		";
     $classes = $lnotice->returnVector($sql);
     $classList = "'".implode("','",$classes)."'";
     $sql = "INSERT IGNORE INTO INTRANET_NOTICE_REPLY (NoticeID,StudentID,StudentName,RecordStatus,DateInput,DateModified)
             SELECT $NoticeID,UserID,$username_field,0,now(),now() FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1 AND ClassName IN ($classList)";
     $lnotice->db_db_query($sql);
}
else if ($type==3)    # Some classes only
{
     # Grab the class name for the classes
     $list = implode(",",$target);
     //$sql = "SELECT ClassName FROM INTRANET_CLASS WHERE ClassID IN ($list)";
     $sql = "SELECT ClassTitleEN FROM YEAR_CLASS WHERE YearClassID IN ($list)";
     $classes = $lnotice->returnVector($sql);
     $classList = "'".implode("','",$classes)."'";
     if($classList)
     {
     	$sql = "INSERT IGNORE INTO INTRANET_NOTICE_REPLY (NoticeID,StudentID,StudentName,RecordStatus,DateInput,DateModified)
             SELECT $NoticeID,UserID,$username_field,0,now(),now() FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1 AND ClassName IN ($classList)";
     	$lnotice->db_db_query($sql);
 	}
}
else if ($type == 4)  # Some only
{
	
     $actual_target_users = $lnotice->returnTargetUserIDArray($targetID);
     if (sizeof($actual_target_users)!=0)
     {
         $delimiter = "";
         $values = "";
         for ($i=0; $i<sizeof($actual_target_users); $i++)
         {
              list($uid,$name,$usertype) = $actual_target_users[$i];
              $values .= "$delimiter ('$NoticeID','$uid','$name',0,now(),now())";
              $delimiter = ",";
         }
         $sql = "INSERT IGNORE INTO INTRANET_NOTICE_REPLY (NoticeID,StudentID,StudentName,RecordStatus,DateInput,DateModified)
                 VALUES $values";
         $lnotice->db_db_query($sql);
     }
}

# Send email notification
if ($sus_status==1 && (($emailnotify==1) || ($emailnotify_Students==1) || ($emailnotify_PIC==1) || ($emailnotify_ClassTeachers==1) || ($pushmessagenotify == 1) || ($pushmessagenotify_Students == 1) || ($pushmessagenotify_PIC == 1) || ($pushmessagenotify_ClassTeachers == 1)))
{
    $lwebmail = new libwebmail();
	
    $sql = "SELECT DISTINCT StudentID FROM INTRANET_NOTICE_REPLY WHERE NoticeID = '$NoticeID'";
    $students = $lnotice->returnVector($sql);
    
    $student_list = implode(",",$students);
    $sql = "SELECT DISTINCT b.UserID
                  FROM INTRANET_PARENTRELATION as a
                       LEFT OUTER JOIN INTRANET_USER as b ON a.ParentID = b.UserID
                  WHERE a.StudentID IN ($student_list)
           ";
    $parent = $lnotice->returnVector($sql);
    
    if(($emailnotify==1) && ($emailnotify_Students==1))
    {
    	$ToArray = array_merge($students,$parent);
    }
    else if($emailnotify==1)
    {
    	$ToArray = $parent;  
    }
    else
    {
    	$ToArray = $students;
    }
    
    if($emailnotify_PIC == 1 && sizeof($target_PIC) != 0) {
    	$PICs = array();
    	$target_PIC = array_unique($target_PIC);
    	foreach($target_PIC as $tPICID) {
    		$tPICid = $tPICID;
    		$tPICid = str_replace("&#160;", "", $tPICid);
    		$tPICid = str_replace("U", "", $tPICid);
    		
    		$PICs[] = $tPICid;
    	}
    	$ToArray = array_merge($ToArray, $PICs);
    }
    
    if($emailnotify_ClassTeachers == 1) {
    	require_once ($PATH_WRT_ROOT."includes/libclass.php");
    	$libclass = new libclass();
    	
    	$teacherArr = array();
    	foreach ($students as $student) {
    		$class = $libclass->returnCurrentClass($student);
    		$teachers = $libclass->returnClassTeacherID($class[0]['ClassName']);
    		foreach ($teachers as $teacher) {
    			array_push($teacherArr, $teacher['UserID']);
    		}
    	}
    	$teacherArr = array_unique($teacherArr);
    	
    	//debug_pr($teacherArr);
    	$ToArray = array_merge($ToArray, $teacherArr);
    }
    
    if ($emailnotify==1 || $emailnotify_Students==1 || ($emailnotify_PIC == 1 && sizeof($target_PIC) != 0) || ($emailnotify_ClassTeachers == 1)) {
	    list($email_subject, $email_body) = $lnotice->returnEmailNotificationData($DateStart,$DateEnd,$pushMsgTitle);
	    $lwebmail->sendModuleMail($ToArray,$email_subject,$email_body,1,'','User',true);
    }
    
    # send push message
    if ($pushmessagenotify == 1 || $pushmessagenotify_Students == 1 || $pushmessagenotify_PIC == 1 || $pushmessagenotify_ClassTeachers == 1) {
		$isPublic = "N";
		$appType = $eclassAppConfig['appType']['Parent'];
		$appType_s = $eclassAppConfig['appType']['Student'];
		$appType_t = $eclassAppConfig['appType']['Teacher'];
		if($sendTimeMode=='now'){
			$sendTimeString='';
		}

		
		//Logic if sendTimeMode == 'now',
		//	check is issue day is in future
		//		-> yes:  sendTimeMode -> 'scheduled' and sendTimeString -> issueDate
// 		$now = date('Y-m-d h:i:s');
// 		if($sendTimeMode=='now'){
// 			if(strtotime($now)<strtotime($DateStart)){
// 				$sendTimeMode = 'scheduled';
// 				$sendTimeString = $DateStart;
// 			}
// 		}
		
		$individualMessageInfoAry = array();
		$i = 0;
		list($pushmessage_subject, $pushmessage_body) = $lnotice->returnPushMessageData($DateStart,$DateEnd,$pushMsgTitle,$pushMsgNoticeNumber);
    
//     	if (!empty($students)) {
            // [2019-0917-1522-01206] skip student status checking
			//$parentStudentAssoAry = BuildMultiKeyAssoc($luser->getParentStudentMappingInfo($students), 'ParentID', $IncludedDBField=array('StudentID'), $SingleValue=1, $BuildNumericArray=1);
//             $parentStudentAssoAry = BuildMultiKeyAssoc($luser->getParentStudentMappingInfo($students, '', $skipStuStatusCheck=true), 'ParentID', $IncludedDBField=array('StudentID'), $SingleValue=1, $BuildNumericArray=1);
// 		} else {
// 			$parentStudentAssoAry = array();
// 		}
//     	$individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $parentStudentAssoAry;
	    			
		//$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $NoticeID);
// 		if ($libeClassApp->isEnabledSendBulkPushMessageInBatches()) {
// 			$notifyMessageId = $libeClassApp->sendPushMessageByBatch($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $NoticeID);
// 	    }
// 	    else {
// 	    	$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $NoticeID);
// 	    }
		if($pushmessagenotify == 1)
		{
			if (!empty($students)) {
				// [2019-0917-1522-01206] skip student status checking
				//$parentStudentAssoAry = BuildMultiKeyAssoc($luser->getParentStudentMappingInfo($students), 'ParentID', $IncludedDBField=array('StudentID'), $SingleValue=1, $BuildNumericArray=1);
				$parentStudentAssoAry = BuildMultiKeyAssoc($luser->getParentStudentMappingInfo($students, '', $skipStuStatusCheck=true), 'ParentID', $IncludedDBField=array('StudentID'), $SingleValue=1, $BuildNumericArray=1);
			} else {
				$parentStudentAssoAry = array();
			}
			$individualMessageInfoAry = array();
			$individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $parentStudentAssoAry;
			
			if ($libeClassApp->isEnabledSendBulkPushMessageInBatches()) {
				$notifyMessageId = $libeClassApp->sendPushMessageByBatch($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $NoticeID);
			} else {
				$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $NoticeID);
			}
		}
		if($pushmessagenotify_Students == 1)
		{
			$StudentAssoAry = array();
			foreach ($students as $studentId) {
				$_targetStudentId = $libeClassApp->getDemoSiteUserId($studentId);
				// link the message to be related to oneself
				$StudentAssoAry[$studentId] = array($_targetStudentId);
			}
			$individualMessageInfoAry = array();
			$individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $StudentAssoAry;
			
			if ($libeClassApp->isEnabledSendBulkPushMessageInBatches()) {
				$notifyMessageId = $libeClassApp->sendPushMessageByBatch($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType_s, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $NoticeID);
			} else {
				$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType_s, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $NoticeID);
			}
		}
		
		// 2018-05-18 Added by Philips
		// [2018-1008-1030-09073] Handle both PIC and Class Teachers - push msg notification
		if($pushmessagenotify_PIC == 1 || $pushmessagenotify_ClassTeachers == 1)
		{
			$teacherAssoAry = array();
			if($pushmessagenotify_PIC == 1)
			{
				if(isset($target_PIC) && sizeof($target_PIC) != 0) {
					$target_PIC = array_unique($target_PIC);
					foreach($target_PIC as $tPICID) {
						$tPICid = $tPICID;
						$tPICid = str_replace("&#160;", "", $tPICid);
						$tPICid = str_replace("U", "", $tPICid);
						
						//$PICAssoAry[$tPICid] = array($tPICid);
						$teacherAssoAry[$tPICid] = array($tPICid);
					}
				}
			}
			if($pushmessagenotify_ClassTeachers == 1)
			{
				require_once($PATH_WRT_ROOT."includes/libclass.php");
				$libclass = new libclass();
				
				$teacherArr = array();
				foreach ($students as $student) {
					$class = $libclass->returnCurrentClass($student);
					$teachers = $libclass->returnClassTeacherID($class[0]['ClassName']);
					foreach ($teachers as $teacher) {
						array_push($teacherArr, $teacher['UserID']);
					}
				}
				$teacherArr = array_unique($teacherArr);
				foreach ($teacherArr as $tid) {
					$teacherAssoAry[$tid] = array($tid);
				}
			}
			$individualMessageInfoAry = array();
			$individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $teacherAssoAry;
			
			if ($libeClassApp->isEnabledSendBulkPushMessageInBatches()) {
				//$notifyMessageId = $libeClassApp->sendPushMessageByBatch($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus = 1, $appType_t, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $NoticeID);
				$notifyMessageId = $libeClassApp->sendPushMessageByBatch($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType_t, $sendTimeMode, $sendTimeString, '', '', 'eNoticeS', $NoticeID);
			} else {
				//$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus = 1, $appType_t, $sendTimeMode, $sendTimeString, '', '', 'eNotice', $NoticeID);
				$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType_t, $sendTimeMode, $sendTimeString, '', '', 'eNoticeS', $NoticeID);
			}
		} else
		{
			// cancel scheduled push message
			$libeClassApp->overrideExistingScheduledPushMessageFromModule("eNotice", $NoticeID, $newNotifyMessageId='');
			$libeClassApp->overrideExistingScheduledPushMessageFromModule("eNoticeS", $NoticeID, $newNotifyMessageId='');
		}
    }
}else if($sus_status==4){
	//Save the choices -> need approval Only
	$sql = "UPDATE INTRANET_NOTICE 
			SET pushmessagenotify = '$pushmessagenotify',
				pushmessagenotify_Students = '$pushmessagenotify_Students',
                pushmessagenotify_PIC = '$pushmessagenotify_PIC',
                pushmessagenotify_ClassTeachers = '$pushmessagenotify_ClassTeachers',
				pushmessagenotifyMode = '$sendTimeMode',
				pushmessagenotifyTime = '$sendTimeString',
				emailnotify = '$emailnotify',
				emailnotify_Students = '$emailnotify_Students',
                emailnotify_PIC = '$emailnotify_PIC',
                emailnotify_ClassTeachers = '$emailnotify_ClassTeachers'
			WHERE NoticeID = $NoticeID ";
	$lnotice->db_db_query($sql);		
}

// for PHP 5.4, replace session_unregister()
//session_unregister("noticeAttFolder");
session_unregister_intranet("noticeAttFolder");

if($sus_status>=4){
	$additional_location = "&pendingApproval=1";
}

intranet_closedb();
//header("Location: paymentNotice.php?xmsg=add&pageNo=$pageNo&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&noticeType=$noticeType&status=$status&year=$year&month=$month");
header("Location: paymentNotice.php?xmsg=AddSuccess&pageNo=$pageNo&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&noticeType=$noticeType&status=$status&year=$year&month=$month".$additional_location);

?>