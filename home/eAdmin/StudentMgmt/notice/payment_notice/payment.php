<?php
//Modifying by: yat
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if (!$plugin['notice'] || $_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["notice"]["hasIssueRight"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"]))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"])
	{
		header("location: ./settings/basic_settings/");	
		exit;
	}
	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

include_once($PATH_WRT_ROOT."lang/email.php");

intranet_auth();
intranet_opendb();

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$CurrentPageArr['eAdminNotice'] = 1;
$CurrentPage = "PageNotice";
$lnotice = new libnotice();
$lclass = new libclass();

$linterface         = new interface_html();
$CurrentPage        = "PageNotice";

if ($status != 2 && $status != 3) $status = 1;

if ($lnotice->hasIssueRight())
{
    $AddBtn         = "<a href=\"javascript:newNotice()\" class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_new . "</a>";
    
    $status_select = "<SELECT name='status' onChange=this.form.submit()>\n";
    $status_select.= "<OPTION value=1 ".($status==1? "SELECTED":"").">$i_Notice_StatusPublished</OPTION>\n";
    $status_select.= "<OPTION value=2 ".($status==2? "SELECTED":"").">$i_Notice_StatusSuspended</OPTION>\n";
    $status_select.= "<OPTION value=3 ".($status==3? "SELECTED":"").">$i_Notice_StatusTemplate</OPTION>\n";
    $status_select.= "</SELECT>&nbsp;\n";
}

# TABLE INFO
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
if($field=="") $field = 0;
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("a.DateStart");
$li->fieldorder2 = ", a.NoticeID desc";

## select Year
$filterbar = "";
$currentyear = date('Y');
for ($i=$currentyear-7; $i<$currentyear+1; $i++)
{
     $array_year[] = $i;
}
$firstname = $i_status_all . ($intranet_session_language=="en"?" ":"") . $i_general_Years;
$select_year = getSelectByValueDiffName($array_year,$array_year,"name='year' onChange='document.form1.submit();'",$year,1,0, $firstname) . "&nbsp;";

## select Month
for ($i=1; $i<=12; $i++)
{
     $array_month[] = $i;
}
$firstname = $i_status_all . ($intranet_session_language=="en"?" ":"") . $i_general_Months;
$select_month = getSelectByValueDiffName($array_month,$array_month,"name='month' onChange='document.form1.submit();'",$month,1,0,$firstname) . "&nbsp;";

$viewRight = $lnotice->hasNormalRight();
$fullRight = $lnotice->hasFullRight();
$hasIssueRight = $lnotice->hasIssueRight();

$keyword = convertKeyword($keyword);

if($_SESSION['UserType']==USERTYPE_STAFF)
{
        ## select of All Notices / Issued Notice
        $noticeTypeSelect = "";
        $noticeTypeSelect = "<SELECT name='noticeType' onChange='this.form.submit()'>\n";
        $noticeTypeSelect.= "<OPTION value=0 ".($noticeType==0? "SELECTED":"").">$i_Notice_AllNotice</OPTION>\n";
        if ($lnotice->hasIssueRight())
        $noticeTypeSelect.= "<OPTION value=1 ".($noticeType==1? "SELECTED":"").">$i_Notice_MyNotice</OPTION>\n";
        $noticeTypeSelect.= "</SELECT>&nbsp;\n";
}

$x = "";
        if ($_SESSION['UserType']==USERTYPE_STAFF)
        {
                if($noticeType)	# Issued Notice
                {
                        $li->sql = $lnotice->returnMyNotice($year,$month,$status,1, $keyword);
                        $li->no_col = 8;
                        $li->IsColOff = "eNoticedisplayMyNotice";

                        // TABLE COLUMN
                        $li->column_list .= "<td class='tabletop tabletopnolink'>".$i_Notice_DateStart."</td>\n";
                        $li->column_list .= "<td class='tabletop tabletopnolink'>".$i_Notice_NoticeNumber."</td>\n";
                        $li->column_list .= "<td class='tabletop tabletopnolink'>&nbsp;</td>\n";
                        $li->column_list .= "<td class='tabletop tabletopnolink'>".$i_Notice_Title."</td>\n";
                        $li->column_list .= "<td class='tabletop tabletopnolink'>".$i_Notice_Type."</td>\n";
                        $li->column_list .= "<td class='tabletop tabletopnolink'>".$i_Notice_Signed."/".$i_Notice_Total."</td>\n";
                        $li->column_list .= "<td class='tabletop tabletopnolink'>".$i_Notice_RecipientType."</td>\n";

                        if($fullRight || $hasIssueRight)
                        $li->column_list .= "<td width=1 class='tabletoplink'>".$li->check("NoticeIDArr[]")."</td>\n";
                }
                else		# All Notices
                {
                    $class = $lclass->returnHeadingClass($UserID);
					$isClassTeacher = ($class!="");

                    $li->sql = $lnotice->returnNoticeListTeacherView($status,$year,$month,1, $keyword);
                    $li->no_col = 7;
                    $li->IsColOff = "displayTeacherView";

						$tabletop_css = "tabletop";
					    // TABLE COLUMN
						$li->column_list .= "<td class='". $tabletop_css ." tabletopnolink'>".$i_Notice_DateStart."</td>\n";
						$li->column_list .= "<td class='". $tabletop_css ." tabletopnolink'>".$i_Notice_NoticeNumber."</td>\n";
						$li->column_list .= "<td class='". $tabletop_css ." tabletopnolink'>&nbsp;</td>\n";
						$li->column_list .= "<td class='". $tabletop_css ." tabletopnolink'>".$i_Notice_Title."</td>\n";
						$li->column_list .= "<td class='". $tabletop_css ." tabletopnolink'>".$i_Notice_Issuer."</td>\n";
						
						if ($isClassTeacher && $status == 1)
						{
                            $li->column_list .= "<td class='". $tabletop_css ." tabletopnolink'>".$i_Notice_ViewOwnClass."</td>\n";
                            $li->no_col++;
						}
						$li->column_list .= "<td class='". $tabletop_css ." tabletopnolink'>".$i_Notice_RecipientType."</td>\n";
					
                    if ($viewRight && $status == 1)
                    {
                            $li->column_list .= "<td class='". $tabletop_css ." tabletopnolink'>".$i_Notice_Signed."/".$i_Notice_Total."</td>\n";
                            $li->no_col++;
                    }
                    
                    $li->column_list .= "<td width=1 class='tabletoplink'>".$li->check("NoticeIDArr[]")."</td>\n";
                }
       }
        else if ($_SESSION['UserType']==USERTYPE_PARENT)
        {
                $noticeType = $noticeType == "" ? "0" : $noticeType;
             switch($noticeType)
             {
                case 0:                        ## Current
                        # Grab children ids
                         $sql = "SELECT StudentID FROM INTRANET_PARENTRELATION WHERE ParentID = '$UserID'";
                         $children = $lnotice->returnVector($sql);
                         if (sizeof($children)==0)
                         {
                                 header ("Location: /");
                                                     intranet_closedb();
                                                     exit();
                             }

                         $child_list = implode(",",$children);
                         $name_field = getNameFieldWithClassNumberByLang("b.");
                         $name_field2 = getNameFieldWithLoginByLang("d.");
                         $conds = "";
                         if ($year != "")
                         {
                             $conds .= " AND DATE_FORMAT(a.DateStart,'%Y')='$year'";
                         }
                         if ($month != "")
                         {
                             $conds .= " AND DATE_FORMAT(a.DateStart,'%c')='$month'";
                         }
                         $sql = "SELECT a.NoticeID,DATE_FORMAT(a.DateStart,'%Y-%m-%d'),
                                        DATE_FORMAT(a.DateEnd,'%Y-%m-%d'),if(a.IsModule=1,'--',a.NoticeNumber),a.Title,c.StudentID,
                                        IF(b.UserID IS NULL,CONCAT('<I>',c.StudentName,'</I>'),$name_field),
                                        a.RecordType, c.RecordType, c.RecordStatus,
                                        IF(d.UserID IS NULL,CONCAT('<I>',c.SignerName,'</I>'),$name_field2),
                                        c.DateModified
                                 FROM INTRANET_NOTICE_REPLY as c
                                      LEFT OUTER JOIN INTRANET_NOTICE as a ON a.NoticeID = c.NoticeID
                                      LEFT OUTER JOIN INTRANET_USER as b ON b.UserID = c.StudentID
                                      LEFT OUTER JOIN INTRANET_USER as d ON d.UserID = c.SignerID
                                 WHERE ('%$keyword%' IS NOT NULL) AND c.StudentID IN ($child_list) AND a.RecordStatus = 1
								 AND ((a.NoticeNumber like '%$keyword%') OR (a.Title like '%$keyword%'))
                                       AND a.DateStart <= CURDATE() AND a.DateEnd >= CURDATE()
                                       $conds ";

                        $li->sql = $sql;
                        $li->no_col = 8;
                        $li->IsColOff = "displayParentView";

                        // TABLE COLUMN
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_DateStart."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_DateEnd."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_NoticeNumber."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_Title."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_StudentName."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_RecipientType."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_SignerNoColon."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_SignedAt."</td>\n";

                        break;
                case 1:                        ## All
                        $li->sql = $lnotice->returnAllNotice($year,$month, 1, $keyword);

                        $li->no_col = 4;
                        $li->IsColOff = "displayAllNotice";

                        // TABLE COLUMN
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_DateStart."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_NoticeNumber."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_Title."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_RecipientType."</td>\n";

                        break;
                case 2:                        ## History
                        # Grab children ids
                         $sql = "SELECT StudentID FROM INTRANET_PARENTRELATION WHERE ParentID = '$UserID'";
                         $children = $lnotice->returnVector($sql);
                         if (sizeof($children)==0) return "";

                         $child_list = implode(",",$children);
                         $name_field = getNameFieldWithClassNumberByLang("b.");
                         $name_field2 = getNameFieldWithLoginByLang("d.");
                         $conds = "";
                         if ($year != "")
                         {
                             $conds .= " AND DATE_FORMAT(a.DateStart,'%Y')='$year'";
                         }
                         if ($month != "")
                         {
                             $conds .= " AND DATE_FORMAT(a.DateStart,'%c')='$month'";
                         }
                         $sql = "SELECT a.NoticeID,DATE_FORMAT(a.DateStart,'%Y-%m-%d'),
                                        DATE_FORMAT(a.DateEnd,'%Y-%m-%d'),if(a.IsModule=1,'--',a.NoticeNumber),a.Title,c.StudentID,
                                        IF(b.UserID IS NULL,CONCAT('<I>',c.StudentName,'</I>'),$name_field),
                                        a.RecordType, c.RecordType, c.RecordStatus,
                                        IF(d.UserID IS NULL,CONCAT('<I>',c.SignerName,'</I>'),$name_field2),
                                        c.DateModified
                                 FROM INTRANET_NOTICE_REPLY as c
                                      LEFT OUTER JOIN INTRANET_NOTICE as a ON c.NoticeID = a.NoticeID
                                      LEFT OUTER JOIN INTRANET_USER as b ON b.UserID = c.StudentID
                                      LEFT OUTER JOIN INTRANET_USER as d ON d.UserID = c.SignerID
                                 WHERE ('%$keyword%' IS NOT NULL) AND c.StudentID IN ($child_list)
                                        AND a.RecordStatus = 1
										AND ((a.NoticeNumber like '%$keyword%') OR (a.Title like '%$keyword%'))
                                       AND a.DateEnd < CURDATE()
                                       $conds ";

                        $li->sql = $sql;
                        $li->no_col = 8;
                        $li->IsColOff = "displayParentHistory";

                        // TABLE COLUMN
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_DateStart."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_DateEnd."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_NoticeNumber."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_Title."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_StudentName."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_RecipientType."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_SignerNoColon."</td>\n";
                        $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_SignedAt."</td>\n";

                        break;
             }


             ## select of All Notices / Issued Notice
                $noticeTypeSelect = "";
                $noticeTypeSelect = "<SELECT name='noticeType' onChange='this.form.submit()'>\n";
                $noticeTypeSelect.= "<OPTION value=0 ".($noticeType==0? "SELECTED":"").">$i_Notice_ElectronicNotice_Current</OPTION>\n";
                if ($lnotice->showAllEnabled)
                        $noticeTypeSelect.= "<OPTION value=1 ".($noticeType==1? "SELECTED":"").">$i_Notice_ElectronicNotice_All</OPTION>\n";
                $noticeTypeSelect.= "<OPTION value=2 ".($noticeType==2? "SELECTED":"").">$i_Notice_ElectronicNotice_History</OPTION>\n";
                $noticeTypeSelect.= "</SELECT>&nbsp;\n";
        }
        else if ($_SESSION['UserType']==USERTYPE_STUDENT)
        {
                switch($noticeType)
                {
                        case 0:                ## Current
                                $conds = "";
                                 if ($year != "")
                                 {
                                     $conds .= " AND DATE_FORMAT(a.DateStart,'%Y')='$year'";
                                 }
                                 if ($month != "")
                                 {
                                     $conds .= " AND DATE_FORMAT(a.DateStart,'%c')='$month'";
                                 }

                                 $name_field = getNameFieldWithClassNumberByLang("b.");
                                 $sql = "SELECT a.NoticeID,DATE_FORMAT(a.DateStart,'%Y-%m-%d'),
                                                DATE_FORMAT(a.DateEnd,'%Y-%m-%d'),if(a.IsModule=1,'--',a.NoticeNumber),a.Title,
                                                a.RecordType, c.RecordType, c.RecordStatus
                                         FROM INTRANET_NOTICE_REPLY as c
                                              LEFT OUTER JOIN INTRANET_NOTICE as a ON c.NoticeID = a.NoticeID
                                              LEFT OUTER JOIN INTRANET_USER as b ON b.UserID = c.StudentID
                                         WHERE ('%$keyword%' IS NOT NULL) AND c.StudentID = '$UserID' AND a.RecordStatus = 1
									           AND ((a.NoticeNumber like '%$keyword%') OR (a.Title like '%$keyword%'))
                                               AND a.DateStart <= CURDATE() AND a.DateEnd >= CURDATE() $conds ";

                                $li->sql = $sql;
                                $li->no_col = 5;
                                $li->IsColOff = "displayStudentView";

                                // TABLE COLUMN
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_DateStart."</td>\n";
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_DateEnd."</td>\n";
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_NoticeNumber."</td>\n";
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_Title."</td>\n";
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_RecipientType."</td>\n";

                                break;
                        case 1:                ## All
                                //$x .= $lnotice->displayAllNotice($year,$month);
                                $li->sql = $lnotice->returnAllNotice($year,$month, 1, $keyword);
                                $li->no_col = 4;
                                $li->IsColOff = "displayAllNotice";

                                // TABLE COLUMN
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_DateStart."</td>\n";
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_NoticeNumber."</td>\n";
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_Title."</td>\n";
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_RecipientType."</td>\n";

                                break;
                        case 2:                ## History
                                $conds = "";
                                 if ($year != "")
                                 {
                                     $conds .= " AND DATE_FORMAT(a.DateStart,'%Y')='$year'";
                                 }
                                 if ($month != "")
                                 {
                                     $conds .= " AND DATE_FORMAT(a.DateStart,'%c')='$month'";
                                 }

                                 $name_field = getNameFieldWithClassNumberByLang("b.");
                                 $sql = "SELECT a.NoticeID,DATE_FORMAT(a.DateStart,'%Y-%m-%d'),
                                        DATE_FORMAT(a.DateEnd,'%Y-%m-%d'),if(a.IsModule=1,'--',a.NoticeNumber),a.Title,
                                        a.RecordType, c.RecordType, c.RecordStatus,
                                        IF(b.UserID IS NULL,CONCAT('<I>',c.SignerName,'</I>'),$name_field),
                                        c.DateModified
                                 FROM INTRANET_NOTICE_REPLY as c
                                      LEFT OUTER JOIN INTRANET_NOTICE as a ON c.NoticeID = a.NoticeID
                                      LEFT OUTER JOIN INTRANET_USER as b ON c.SignerID = b.UserID
                                 WHERE ('%$keyword%' IS NOT NULL) AND c.StudentID = '$UserID' AND a.DateEnd < CURDATE() $conds 
								 AND ((a.NoticeNumber like '%$keyword%') OR (a.Title like '%$keyword%'))
								 AND a.RecordStatus=1 ";

                                 $li->sql = $sql;
                                $li->no_col = 7;
                                $li->IsColOff = "displayStudentHistory";

                                // TABLE COLUMN
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_DateStart."</td>\n";
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_DateEnd."</td>\n";
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_NoticeNumber."</td>\n";
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_Title."</td>\n";
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_RecipientType."</td>\n";
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_SignerNoColon."</td>\n";
                                $li->column_list .= "<td class='tablebluetop tabletopnolink'>".$i_Notice_SignedAt."</td>\n";

                                break;
                }


             ## select of All Notices / Issued Notice
                $noticeTypeSelect = "";
                $noticeTypeSelect = "<SELECT name='noticeType' onChange='this.form.submit()'>\n";
                $noticeTypeSelect.= "<OPTION value=0 ".($noticeType==0? "SELECTED":"").">$i_Notice_ElectronicNotice_Current</OPTION>\n";
                if ($lnotice->showAllEnabled)
                        $noticeTypeSelect.= "<OPTION value=1 ".($noticeType==1? "SELECTED":"").">$i_Notice_ElectronicNotice_All</OPTION>\n";
                $noticeTypeSelect.= "<OPTION value=2 ".($noticeType==2? "SELECTED":"").">$i_Notice_ElectronicNotice_History</OPTION>\n";
                $noticeTypeSelect.= "</SELECT>&nbsp;\n";

        }
        else $x .= "";

        
### Button
$delBtn         = "<a href=\"javascript:checkRemoveThis(document.form1,'NoticeIDArr[]','remove_update.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_delete . "</a>";
//$editBtn         = "<a href=\"javascript:checkEdit(document.form1,'NoticeIDArr[]','edit.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";
//$edit_link        = "edit.php";

$searchTag 	= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><td>";
$searchTag 	.= "&nbsp;<input type=\"text\" name=\"keyword\" class=\"formtextbox\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\"></td>";
$searchTag 	.= "<td>".$linterface->GET_BTN($button_find, "submit", "","submit3"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."</td>";
$searchTag 	.= "</td></table>";


### Title ###

$TAGS_OBJ[] = array($Lang['eNotice']['PaymentNotice']);
$MODULE_OBJ = $lnotice->GET_MODULE_OBJ_ARR();

if(!$_SESSION['UserType']==USERTYPE_STAFF)
{
        $CurrentPageArr['eOffice']                         = 0;
        $CurrentPageArr['eServiceeOffice']         = 1;
}


$linterface->LAYOUT_START();

?>
<SCRIPT LANGUAGE=Javascript>
function viewNotice(id)
{
       newWindow('/home/eService/notice/sign.php?NoticeID='+id,10);
}
function viewResult(id)
{
         newWindow('./result.php?NoticeID='+id,10);
}
function viewNoticeClass(id)
{
         newWindow('/home/eService/notice/tableview.php?type=1&NoticeID='+id,10);
}

/*
function sign(id,studentID)
{
         newWindow('./sign.php?NoticeID='+id+'&StudentID='+studentID,10);
}
function viewReply(id,studentID)
{
         newWindow('./view.php?NoticeID='+id+'&StudentID='+studentID,10);
}

*/

function checkRemoveThis(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm("<?=$i_Notice_RemovalWarning?>")){
                obj.action=page;
                obj.method="POST";
                obj.submit();
                }
        }
}
function editNotice(nid)
{
         with(document.form1)
         {
                NoticeID.value = nid;
                action = "edit.php";
                submit();
         }
}

function newNotice()
{
         with(document.form1)
         {
                action = "new.php";
                submit();
         }
}

</SCRIPT>

<br />
<form name="form1" method="get" action="index.php">





<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">


                                <tr>
										<td class="tabletext">
                                        	<table width="97%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
														<td class="tabletext" nowrap><?=$select_status?></td>
														<td width="100%" align="right" class="tabletext"><?=$searchTag?></td>
                                                    </tr>
                                                  </table>
                                        </td>
                                </tr>	

<tr>
        <td align="center">
                <table width="96%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                        <td align="left" class="tabletext">
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
								
                                        <td>
                                                <table border="0" cellspacing="0" cellpadding="2">
                                                    <tr>
                                                              <td><p><?=$AddBtn?></p></td>
                                                    </tr>
                                                  </table>
                                        </td>
										
										
                                        <td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
                                </tr>
								
			
		
                                <tr>
                                        <td align="left" valign="bottom"><?=$noticeTypeSelect?><?=$status_select?><?=$select_year?><?=$select_month?></td>
                                        <td align="right" valign="bottom" height="28">
                                        <? if($_SESSION['UserType']==USERTYPE_STAFF and (($fullRight || $hasIssueRight))) {?>
                                                <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                        <td width="21"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_01.gif" width="21" height="23" /></td>
                                                        <td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_02.gif">
                                                                <table border="0" cellspacing="0" cellpadding="2">
                                                                <tr>
                                                                        <td nowrap><?=$delBtn?></td>
                                                                </tr>
                                                                </table>
                                                        </td>
                                                        <td width="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_03.gif" width="6" height="23" /></td>
                                                </tr>
                                                </table>
                                                <? } ?>
                                        </td>
                                </tr>
                                <tr>
                                        <td colspan="2">
                                                <?=$li->display();?>
                                        </td>
                                </tr>


                                </table>
                        </td>
                </tr>
                </table>
        </td>
</tr>
</table>
<br />


<input type="hidden" name="NoticeID" value="" />
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

</form>


<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>

