<?php
// Editing by 
/*
 * 2019-11-12 Carlos: Improved subsidy effective period checking with notice start date and end date.
 * 2019-05-10 Carlos: Created for getting target students for compose reply slip.
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$lnotice = new libnotice();
$lclass = new libclass();
$libjson = new JSON_obj();

$type = IntegerSafe($_POST['type']);
$return_selection = $_POST['return_selection'] == 1;

$subsidy_conds = "";
$notice_start_date = '';
$notice_end_date = '';
if(isset($_POST['NoticeID'])){
	$notice_id = IntegerSafe($_POST['NoticeID']);
	$lnotice = new libnotice($notice_id);
	$notice_start_date = $lnotice->DateStart;
	$notice_end_date = $lnotice->DateEnd;
	//$subsidy_conds .= " AND ((s.EffectiveStartDate IS NULL AND s.EffectiveEndDate IS NULL) OR ('".$lnotice->Get_Safe_Sql_Query($notice_start_date)."' BETWEEN s.EffectiveStartDate AND s.EffectiveEndDate) ) ";
}
if(isset($_POST['DateStart']) && intranet_validateDate($_POST['DateStart'])){
	$notice_start_date = $_POST['DateStart'];
	//$subsidy_conds .= " AND ((s.EffectiveStartDate IS NULL AND s.EffectiveEndDate IS NULL) OR ('".$lnotice->Get_Safe_Sql_Query($_POST['DateStart'])."' BETWEEN s.EffectiveStartDate AND s.EffectiveEndDate) ) ";
}
if(isset($_POST['DateEnd']) && intranet_validateDate($_POST['DateEnd'])){
	$notice_end_date = $_POST['DateEnd'];
}

if($notice_start_date != '' && $notice_end_date != '')
{
	$subsidy_conds .= " AND (";
	$subsidy_conds .= " (s.EffectiveStartDate IS NULL AND s.EffectiveEndDate IS NULL) ";
	$subsidy_conds .= " OR IF(s.EffectiveEndDate IS NULL,
							  '$notice_start_date' >= s.EffectiveStartDate,
							  '$notice_start_date' BETWEEN s.EffectiveStartDate AND s.EffectiveEndDate 
							)
						OR IF(s.EffectiveEndDate IS NULL,
							  '$notice_end_date' >= s.EffectiveStartDate,
							  '$notice_end_date' BETWEEN s.EffectiveStartDate AND s.EffectiveEndDate 
							) 
						OR (s.EffectiveStartDate BETWEEN '$notice_start_date' AND '$notice_end_date')
						OR IF(s.EffectiveEndDate IS NOT NULL,s.EffectiveEndDate BETWEEN '$notice_start_date' AND '$notice_end_date',0) ";
			$subsidy_conds .= ") ";
}

$students = array();
if(isset($_POST['NoticeID'])){
	$notice_id = IntegerSafe($_POST['NoticeID']);
	//$username_field = getNameFieldWithClassNumberEng("u.");
	$username_field = getNameFieldWithClassNumberByLang("u.");
	$sql = "SELECT DISTINCT u.UserID,$username_field as StudentName,u.EnglishName,u.ChineseName,u.ClassName,u.ClassNumber,u.UserLogin,i.IdentityID,IFNULL(i.IdentityName,'') as IdentityName  
			FROM INTRANET_NOTICE_REPLY as r 
			INNER JOIN INTRANET_USER as u ON u.UserID=r.StudentID 
			LEFT JOIN PAYMENT_SUBSIDY_IDENTITY_STUDENT as s ON s.StudentID=r.StudentID $subsidy_conds 
			LEFT JOIN PAYMENT_SUBSIDY_IDENTITY as i ON i.IdentityID=s.IdentityID AND i.RecordStatus='1' 
			WHERE r.NoticeID='$notice_id' AND u.RecordType = 2 AND u.RecordStatus = 1 
			ORDER BY u.ClassName,u.ClassNumber";
    $students = $lnotice->returnResultSet($sql);
	
}else if ($type==1)         # Whole School
{
    $username_field = getNameFieldWithClassNumberByLang("iu.");
    $sql = "select 
				ycu.UserID, $username_field as StudentName, iu.EnglishName, iu.ChineseName, iu.ClassName, iu.ClassNumber, iu.UserLogin, i.IdentityID,IFNULL(i.IdentityName,'') as IdentityName  
			from
				YEAR as y 
				INNER JOIN YEAR_CLASS as yc on (yc.YearID = y.YearID and yc.AcademicYearID = ". Get_Current_Academic_Year_ID() .")
				INNER JOIN YEAR_CLASS_USER as ycu on (ycu.YearClassID = yc.YearClassID)
				INNER JOIN INTRANET_USER as iu on (iu.UserID = ycu.UserID) 
				LEFT JOIN PAYMENT_SUBSIDY_IDENTITY_STUDENT as s ON s.StudentID=iu.UserID $subsidy_conds
				LEFT JOIN PAYMENT_SUBSIDY_IDENTITY as i ON i.IdentityID=s.IdentityID AND i.RecordStatus='1' 
			where iu.RecordStatus=1 and iu.RecordType=2 
			order by iu.ClassName,iu.ClassNumber+0
    		";
    $students = $lnotice->returnResultSet($sql);
}
else if ($type==2)    # Some levels only
{
	$username_field = getNameFieldWithClassNumberByLang("u.");
     # Grab the class name for the levels
     $sql = "
	     		SELECT 
	     			yc.ClassTitleEN 
	     		FROM 
	     			YEAR as y 
	     			INNER JOIN YEAR_CLASS as yc on (yc.YearID = y.YearID and yc.AcademicYearID = ". Get_Current_Academic_Year_ID() .")
	     		WHERE 
	     			y.YearID IN ('".implode("','",$lnotice->Get_Safe_Sql_Query($target))."')
     		";
     $classes = $lnotice->returnVector($sql);
     $sql = "SELECT u.UserID,$username_field as StudentName,u.EnglishName,u.ChineseName,u.ClassName,u.ClassNumber,u.UserLogin,i.IdentityID,IFNULL(i.IdentityName,'') as IdentityName 
			FROM INTRANET_USER as u 
			LEFT JOIN PAYMENT_SUBSIDY_IDENTITY_STUDENT as s ON s.StudentID=u.UserID $subsidy_conds
			LEFT JOIN PAYMENT_SUBSIDY_IDENTITY as i ON i.IdentityID=s.IdentityID AND i.RecordStatus='1' 
			WHERE u.RecordType = 2 AND u.RecordStatus = 1 AND u.ClassName IN ('".implode("','",$lnotice->Get_Safe_Sql_Query($classes))."') 
			ORDER BY u.ClassName,u.ClassNumber+0";
     $students = $lnotice->returnResultSet($sql);
}
else if ($type==3)    # Some classes only
{
	 $username_field = getNameFieldWithClassNumberByLang("u.");
     # Grab the class name for the classes
     $sql = "SELECT ClassTitleEN FROM YEAR_CLASS WHERE YearClassID IN ('".implode("','",$lnotice->Get_Safe_Sql_Query($target))."')";
     $classes = $lnotice->returnVector($sql);
     $sql = "SELECT u.UserID,$username_field as StudentName,u.EnglishName,u.ChineseName,u.ClassName,u.ClassNumber,u.UserLogin,i.IdentityID,IFNULL(i.IdentityName,'') as IdentityName
			FROM INTRANET_USER as u 
			LEFT JOIN PAYMENT_SUBSIDY_IDENTITY_STUDENT as s ON s.StudentID=u.UserID $subsidy_conds 
			LEFT JOIN PAYMENT_SUBSIDY_IDENTITY as i ON i.IdentityID=s.IdentityID AND i.RecordStatus='1' 
			WHERE u.RecordType = 2 AND u.RecordStatus = 1 AND u.ClassName IN ('".implode("','",$lnotice->Get_Safe_Sql_Query($classes))."') 
			ORDER BY u.ClassName,u.ClassNumber+0";
     $students = $lnotice->returnResultSet($sql);
}
else if ($type == 4)  # Some only
{
	 $target = cleanCrossSiteScriptingCode($target);
	 for($i=0;$i<count($target);$i++){
		if(substr($target[$i],0,1) != 'U'){
			$target[$i] = 'U'.$target[$i];
		}
	 }
   	 $targetID = implode(",",$target);
     $actual_target_users = $lnotice->returnTargetUserIDArray($targetID);
     if (sizeof($actual_target_users)>0)
     {
     	$username_field = getNameFieldWithClassNumberByLang("u.");
         $student_ids = Get_Array_By_Key($actual_target_users,'UserID');
         $sql = "SELECT u.UserID,$username_field as StudentName,u.EnglishName,u.ChineseName,u.ClassName,u.ClassNumber,u.UserLogin,i.IdentityID,IFNULL(i.IdentityName,'') as IdentityName 
				 FROM INTRANET_USER as u 
				LEFT JOIN PAYMENT_SUBSIDY_IDENTITY_STUDENT as s ON s.StudentID=u.UserID $subsidy_conds
				LEFT JOIN PAYMENT_SUBSIDY_IDENTITY as i ON i.IdentityID=s.IdentityID AND i.RecordStatus='1' 
				WHERE u.RecordType = 2 AND u.RecordStatus = 1 AND u.UserID IN (".implode(",",$student_ids).") 
				ORDER BY u.ClassName,u.ClassNumber";
     	 $students = $lnotice->returnResultSet($sql);
     }
}

intranet_closedb();

if($return_selection){
	$field_name = isset($_POST['field_name'])? cleanCrossSiteScriptingCode($_POST['field_name']) : 'TargetID';
	$first_row_empty = isset($_POST['first_row_empty']) && $_POST['first_row_empty']==1? true:false;
	$selection_data = array();
	if($first_row_empty){
		$selection_data[] = array('',$Lang['General']['EmptySymbol'],'');
	}
	for($i=0;$i<count($students);$i++){
		$selection_data[] = array($students[$i]['UserID'],$students[$i]['StudentName'].($students[$i]['IdentityName']!=''?"(".$students[$i]['IdentityName'].")":""),$students[$i]['IdentityID']);
	}
	//$html = getSelectByArray($selection_data, ' id="'.$field_name.'" name="'.$field_name.'" ', $selected="", $all=0, $noFirst=1, $FirstTitle="", $ParQuoteValue=1);
	$html = '<select id="'.$field_name.'" name="'.$field_name.'">'."\n";
	for($i=0;$i<count($selection_data);$i++){
		$html .= '<option value="'.$selection_data[$i][0].'" data-identityid="'.$selection_data[$i][2].'">'.intranet_htmlspecialchars($selection_data[$i][1]).'</option>'."\n";
	}
	$html.= '</select>'."\n";
	echo $html;
}else{
	header("Content-Type: application/json;charset=".returnCharset());
	echo $libjson->encode($students);
}

?>