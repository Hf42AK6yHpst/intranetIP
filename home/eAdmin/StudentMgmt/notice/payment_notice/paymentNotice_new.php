<?php
# Using: Bill
/*
 * if upload file to client site before v5.1, need to check regional case [2020-0407-1445-44292]
 */

################################
#
#   Date:   2020-10-30  Ray
#           add quota
#
# 	Date:   2020-09-24  YatWoon    [2020-0921-1147-02066]
#			add flag checking $sys_custom['eNotice']['HideCurrentNoticeList']
#
# 	Date:   2020-09-07  Bill    [2020-0604-1821-16170]
#			load settings from payment notice
#
#   Date:   2020-08-11  Bill    [2020-0803-0925-40235]
#           add remarks next to checkbox - "All questions are required to be answered"
#
#   Date:   2020-06-29 Ray
#           - add multi payment gateway
#
#   Date:   2020-05-04  Bill    [2020-0407-1445-44292]
#           - support class teacher audience limit checking + UI update ($sys_custom['eNotice']['ClassTeacherSelectOwnClassStudentOnly'])
#           - add param js function Init_User_Selection_Menu() in '/home/common_choose/user_selection_plugin_js.php'
#
#   Date:   2020-02-17 Bill
#           Replace php inline coding (if('<?=$plugin['eClassApp']>'=='1') { ... }) in js
#
#	Date:	2019-10-17	Philips
#			Modified $emailChecked
#
#	Date:	2019-10-15	Philips [2019-1004-1641-10235]
#			Modified syncIssueTimeToPushMsg(), use #DateStart_hour and #DateStart_min to sync pushMsg send time
#
#	Date:	2019-10-14  Philips
#			Added $pushmessagenotify_PIC, $pushmessagenotify_ClassTeachers, $emailnotify_Students, $emailnotify_PIC, $emailnotify_ClassTeachers
#
#   Date:   2019-09-23  Bill    [2019-0923-1111-44235]
#           To set iframe scrollable
#
#	Date:	2019-02-11 Carlos
#			Hide Debit Method choice if use TNG/AlipayHK to direct pay the notice items. 
#
#   Date:   2019-01-25 Bill     [2019-0118-1125-41206]
#           Improved: Description - replace textarea by FCKEditor
#
#   Date:   2018-11-08 Bill     [2018-1107-1014-05054]
#           Fixed: cannot set correct recipient if search by autocomplete input field
#
#	Date:	2018-10-05 Isaac
#           Integrated User_Selection_Menu plugin for PIC and target
#
#   Date:   2018-09-19 Philips
#           Add hidden input field : noticeAttFolder to avoid using session
#
#	Date:	2018-08-21 Carlos 
#			$sys_custom['ePayment']['Alipay'] - add selection option to set Alipay merchant account for payment item.
#
#   Date:   2018-03-26 Bill    [2018-0221-1225-19206]
#           Sort notice templates by notice number and notice title
#
#	Date:	2018-01-22	Carlos
#           $sys_custom['ePayment']['TNG'] do not display debit method, set default method to [Debit Directly].
#
#	Date:	2017-09-07	Bill	[2017-0906-1332-14066]
#			Improved: Disable submit button after submit the form
#
#	Date:	2017-06-13	Bill	[2017-0524-1550-19235]
#			Improved: Hide Class Level if without any classes
#
#	Date:	2016-09-26	(Villa)
#			Modified checkform - check if the schedule time is in the past or 30days behind the issue day
#
#	Date:	2016-09-26	(Villa)
#			Add sending mode to pushMsg-send in specific time
#
# 	Date:	2016-09-23	(Villa)
#			Add to module setting - "default using eclass App to notify"
#
#	Date:	2016-09-23	Villa
#			Added Push Message Setting box
#
#	Date:	2016-07-11	Kenneth
#			Add approval logic
#
#	Date:	2015-12-11	Bill
#			Replace session_register() by session_register_intranet() for PHP 5.4
#
#	Date:	2015-11-10	Kenneth		[2015-1020-1555-27066]
#			Allow auto count eNotice deadline for future eNotice (JS)
#
#	Date:	2015-10-08	Bill	[DM#2915]
#			Ensure run js function Init_JQuery_AutoComplete() when Audience is Applicable students, prevent js error
#
#	Date:	2015-06-26 	Omas
#			Added autocompleter for type 4
#
#	Date:	2015-06-09	Shan
#			Update UI standard
#
#	Date:	2015-04-14	Bill	[2015-0323-1602-46073]
#			Add field to select PICs of payment notice
#
#	Date:	2014-10-14 (Roy)
#			Add push message option, and modify JavaScript
#
#	Date:	2014-03-25	YatWoon
#			Add wordings [Case#P59398]
#
#	Date:	2011-02-16	YatWoon
#			Add option "Display question number"
#
#	Date:	2010-12-20	YatWoon
#			Change student selection to "/home/common_choose/index.php"
#
################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

// [2020-0604-1821-16170]
//if (!$plugin['notice'] || $_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["notice"]["hasIssueRight"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"]))
if (!$plugin['notice'] || $_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["notice"]["hasPaymentNoticeIssueRight"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PaymentNotice";
$CurrentPageArr['eAdminNotice'] = 1;

$lnotice = new libnotice();
$lform = new libform();
$li = new libfilesystem();
$lpayment = new libpayment();

// [2020-0407-1445-44292]
$isAudienceLimited = $lnotice->isClassTeacherAudienceTargetLimited();

$use_merchant_account = $lpayment->useEWalletMerchantAccount();
$use_direct_pay = $lpayment->isEWalletDirectPayEnabled();

# Attachment Folder
# create a new folder to avoid more than 1 notice using the same folder
//session_register("noticeAttFolder");
$noticeAttFolder = session_id().".".time();
// for PHP 5.4, replace session_register()
session_register_intranet("noticeAttFolder", $noticeAttFolder);

$path = "$file_path/file/notice/";
if (!is_dir($path))
{
	if($bug_tracing['notice_attachment_log']){
         $temp_log_filepath = "$file_path/file/log_notice_attachment.txt";
         $temp_time = date("Y-m-d H:i:s");
         $temp_user = $UserID;
         $temp_page = 'notice_new.php';
         $temp_action="create folder:".$path;
         $temp_content = get_file_content($temp_log_filepath);
         $temp_logentry = "\"$temp_time\",\"$temp_user\",\"$temp_page\",\"$temp_action\"\n";
         $temp_content .= $temp_logentry;
         write_file_content($temp_content, $temp_log_filepath);
	 }
     $li->folder_new($path);
}

$path = "$file_path/file/notice/$noticeAttFolder"."tmp";
if (!is_dir($path))
{
	if($bug_tracing['notice_attachment_log']){
         $temp_log_filepath = "$file_path/file/log_notice_attachment.txt";
         $temp_time = date("Y-m-d H:i:s");
         $temp_user = $UserID;
         $temp_page = 'notice_new.php';
         $temp_action="create folder:".$path;
         $temp_content = get_file_content($temp_log_filepath);
         $temp_logentry = "\"$temp_time\",\"$temp_user\",\"$temp_page\",\"$temp_action\"\n";
         $temp_content .= $temp_logentry;
         write_file_content($temp_content, $temp_log_filepath);
	 }
     $li->folder_new($path);
}
$lo = new libfiletable("", $path, 0, 0, "");
$files = $lo->files;

if ($templateID == "")
{
    //$notice_templates = $lnotice->returnTemplates("Payment");
    $notice_templates = $lnotice->returnTemplates("Payment", $orderByTitle=true);
    $NAoption = array(-1, $i_Notice_NotUseTemplate);
    if (sizeof($notice_templates) != 0)
    {
        array_unshift($notice_templates, $NAoption);
    }
    else
    {
        $notice_templates = array($NAoption);
    }
    $template_selection = getSelectByArray($notice_templates,"name='templateID' onChange=\"this.form.submit()\"");
}
else
{
    if ($templateID != -1)
    {
        $lnotice->returnRecord($templateID);
        $template_selection = $lnotice->NoticeNumber." - ". $lnotice->Title;
    }
    else
    {
        $template_selection = "$i_Notice_NotUseTemplate";
    }
    $template_selection .= " <input type='hidden' name='templateID' value='$templateID'>";

    if ($type < 1 || $type > 4) $type = "";
    if ($type == "")
    {
        $type = $lnotice->RecordType;
        $type_selected = false;
    }
    else
    {
        $type_selected = true;
    }

    // [2020-0407-1445-44292] limited to 2 audience types only
    if($isAudienceLimited)
    {
        $type_selection = "
        <SELECT name='type' onChange='this.form.submit()'>
            <OPTION value='' > -- $button_select -- </OPTION>
            <OPTION value='3' $check3>$i_Notice_RecipientTypeClass</OPTION>
            <OPTION value='4' $check4>$i_Notice_RecipientTypeIndividual</OPTION>
        </SELECT>";
    }
    else
    {
        $type_selection = "
        <SELECT name='type' onChange='this.form.submit()'>
            <OPTION value='' > -- $button_select -- </OPTION>
            <OPTION value='1' $check1>$i_Notice_RecipientTypeAllStudents</OPTION>
            <OPTION value='2' $check2>$i_Notice_RecipientTypeLevel</OPTION>
            <OPTION value='3' $check3>$i_Notice_RecipientTypeClass</OPTION>
            <OPTION value='4' $check4>$i_Notice_RecipientTypeIndividual</OPTION>
        </SELECT>";
    }
}

if (!$type_selected)
{
	$x = "<tr valign='top'>";
	$x .= "<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'>$i_Notice_FromTemplate</span></td>";
    $x .= "<td class='tabletext'>$template_selection</td>";
	$x .= "</tr>";

	if ($templateID!="") 
	{
		$x .= "<tr valign='top'>";
		$x .= "<td width='30%' valign='top' nowrap class='field_title'><span class='tabletext'>$i_Notice_RecipientType</span></td>";
		$x .= "<td class='tabletext'>$type_selection</td>";
		$x .= "</tr>";
	}
}
else
{
		$lclass = new libclass();
		if ($type == 1)       # Whole School
		{
        	$nextSelect = "";
        	$type_selection = "$i_Notice_RecipientTypeAllStudents";
    	}
    	else if ($type == 2)	# Some Levels
    	{
			$lvls = $lclass->getLevelArray();
            
			$nextSelect = "<tr valign='top'>";
        	$nextSelect .= "<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'>$i_Notice_RecipientLevel</span> <span class='tabletextrequire'>*</span></td>";
            $nextSelect .= "<td class='tabletext'>";
         	
         	if ($type == $lnotice->RecordType)
         	{
             	$templateLvl = explode(",",$lnotice->RecipientID);
         	}
         
         	for ($i=0; $i<sizeof($lvls); $i++)
         	{
				list($id, $name) = $lvls[$i];
				
				// [2017-0524-1550-19235] Skip if Class Level without any classes
				$formLevelClassList = $lclass->returnClassListByLevel($id);
				if(empty($formLevelClassList)) {
					continue;
                }

				if (isset($templateLvl) && sizeof($templateLvl) != 0) {
					$str = (in_array($id, $templateLvl) ? "CHECKED" : "");
                } else {
					$str = "";
                }
				
				$nextSelect .= "<input type='checkbox' name='target[]' value='$id' $str id='target_$i'><label for='target_$i'>$name</label> \n";
         	}
			
			$nextSelect .= "</td></tr>\n";
         	$type_selection = "$i_Notice_RecipientTypeLevel";
    	}
    	else if ($type == 3)	#Some Classes
    	{
			if ($type == $lnotice->RecordType)
         	{
            	$templateClass = explode(",", $lnotice->RecipientID);
         	}
         	$classes = $lclass->getClassList();

			// [2020-0407-1445-44292] filter not teaching classes
            if($isAudienceLimited)
            {
                $teaching_class_list = $lnotice->getClassTeacherCurrentTeachingClass();
                $teaching_class_ids = Get_Array_By_Key($teaching_class_list, 'ClassID');

                $class_size = sizeof($classes);
                for ($i=0; $i<$class_size; $i++)
                {
                    list($classid, $name, $lvlID) = $classes[$i];
                    if(!in_array($classid, $teaching_class_ids)) {
                        unset($classes[$i]);
                    }
                }
                $classes = array_values($classes);
            }

         	$prevID = $classes[0][2];
			
            $nextSelect = "<tr valign='top'>";
        	$nextSelect .= "<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'>$i_Notice_RecipientClass</span> <span class='tabletextrequire'>*</span></td>";
            $nextSelect .= "<td class='tabletext'>";
         	
            for ($i=0; $i<sizeof($classes); $i++)
         	{
              		list($classid, $name, $lvlID) = $classes[$i];
              		if (isset($templateClass) && sizeof($templateClass) != 0) {
                  		$str = (in_array($classid, $templateClass) ? "CHECKED" : "");
                    } else {
              			$str = "";
                    }
              		
                    if ($lvlID == $prevID)
              		{
                  		$nextSelect .= " ";
              		}
              		else
              		{
                  		$nextSelect .= "<br>\n";
                  		$prevID = $lvlID;
              		}
              		
                    $nextSelect .= "<input type='checkbox' name='target[]' value='$classid' $str id='target_$i'><label for='target_$i'>$name</label> ";
         	}
         	
         	$nextSelect .= "</td></tr>\n";
         	$type_selection = $i_Notice_RecipientTypeClass;
		}
    	else	## type==4
    	{
//          $nextSelect = "<tr valign='top'>";
//        		$nextSelect .= "<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'>$i_Notice_RecipientIndividual</span> <span class='tabletextrequire'>*</span></td>";
//              $nextSelect .= "<td class='tabletext'>";
//                
//          $nextSelect .= "<table border='0' cellspacing='1' cellpadding='1'>";
//        	$nextSelect .= "<tr>";
//        	$nextSelect .= "<td>";
//        	for($i = 0; $i < 40; $i++) $space .= "&nbsp;";
//                $nextSelect .= "<select name='target[]' size='4' multiple><option>$space</option></select>";
//        	$nextSelect .= "</td>";
//        	$nextSelect .= "<td>";
//                //$nextSelect .= $linterface->GET_BTN($i_frontpage_campusmail_choose, "button","newWindow('../choose/index.php?fieldname=target[]',9)") . "<br>";
//                $nextSelect .= $linterface->GET_BTN($i_frontpage_campusmail_choose, "button","newWindow('/home/common_choose/index.php?fieldname=target[]&page_title=SelectAudience&permitted_type=2&DisplayGroupCategory=1',16)") . "<br>";
//                $nextSelect .= $linterface->GET_BTN($i_frontpage_campusmail_remove, "button","checkOptionRemove(document.form1.elements['target[]'])") . "<br>";
//        	$nextSelect .= "</td>";
//        	$nextSelect .= "</tr>";
//        	$nextSelect .= "</table>";
//                
//         	$nextSelect .= "</td></tr>\n";

		### Choose Member Btn
		$btn_ChooseMember = $linterface->GET_SMALL_BTN($Lang['Btn']['Select'], "button", "newWindow('/home/common_choose/index.php?fieldname=target[]&page_title=SelectAudience&permitted_type=2&DisplayGroupCategory=1&UserRecordStatus=0,1',16)");
		
		### Auto-complete ClassName ClassNumber StudentName search
		$UserClassNameClassNumberInput = '';
		$UserClassNameClassNumberInput .= '<div style="float:left;">';
			$UserClassNameClassNumberInput .= '<input type="text" id="UserClassNameClassNumberSearchTb" name="UserClassNameClassNumberSearchTb" value="" />';
		$UserClassNameClassNumberInput .= '</div>';
		
		### Auto-complete login search
		$UserLoginInput = '';
		$UserLoginInput .= '<div style="float:left;">';
			$UserLoginInput .= '<input type="text" id="UserLoginSearchTb" name="UserLoginSearchTb" value="" />';
		$UserLoginInput .= '</div>';
		
		### Student Selection Box & Remove all Btn
		$MemberSelectionBox = $linterface->GET_SELECTION_BOX($recipient_name_ary, "name='target[]' id='target[]' class='select_studentlist' size='15' multiple='multiple'", "");
		$btn_RemoveSelected = $linterface->GET_SMALL_BTN($Lang['Btn']['RemoveSelected'], "button", "js_Remove_Selected_Student();");
		
		$nextSelect = "<tr valign='top'>";
        $nextSelect .= "<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'>$i_Notice_RecipientIndividual</span> <span class='tabletextrequire'>*</span></td>";
        $nextSelect .= "<td class='tabletext'>";
			$nextSelect .= "<table>";
			$nextSelect .= '<tr>
                                <td class="tablerow2" valign="top">
                                    <div id="divSelectStudent">'.'</div>
                                </td>
                            </tr>';
			$nexSelect .= "</table>";
			$nextSelect .= "</td>";
        	$nextSelect .= "</tr>";
        	$nextSelect .= "</table>";
			$nextSelect .= "</td></tr>\n";
         	$type_selection = $i_Notice_RecipientTypeIndividual;
    	}
    
    	$type_selection .= " <input type='hidden' name='type' value='$type'>";

    	$now = time();
    	// [2020-0604-1821-16170]
    	//$defaultEnd = $now + ($lnotice->defaultNumDays*24*3600);
        $defaultEnd = $now + ($lnotice->payment_defaultNumDays*24*3600);
    	$start = date('Y-m-d H:i:s', $now);
    	$end = date('Y-m-d H:i:s', $defaultEnd);
    	
    	// reference: http://stackoverflow.com/questions/2480637/round-minute-down-to-nearest-quarter-hour
    	$rounded_seconds = ceil(time() / (15 * 60)) * (15 * 60);
    	$nextTimeslotHour = date('H', $rounded_seconds);
    	$nextTimeslotMinute = date('i', $rounded_seconds);
    	if ($sendTimeString == "") {
    		$selectionBoxDate = date('Y-m-d', $nowTime);
    		$selectionBoxHour = $nextTimeslotHour;
    		$selectionBoxMinute = $nextTimeslotMinute;
    	}
    	else {
    		$selectionBoxDate = date('Y-m-d', $sendTime);
    		$selectionBoxHour = date('H', $sendTime);
    		$selectionBoxMinute = date('i', $sendTime);
    	}

    	if ($start == "") {
    		$startDate = date('Y-m-d', $now);
    		$startDateHour = $nextTimeslotHour;
    		$startDateMin = $nextTimeslotMinute;
    	}
    	else {
    		$startDate = date('Y-m-d', strtotime($start));
    		$startDateHour = date('H', strtotime($start));
    		$startDateMin = date('i', strtotime($start));
    	}
    	if ($end == "") {
    		$endDate = date('Y-m-d', $defaultEnd);
    		$endDateHour = $nextTimeslotHour;
    		$endDateMin = $nextTimeslotMinute;
    	}
    	else {
    		$endDate = date('Y-m-d', strtotime($end));
    		$endDateHour = '23';
    		$endDateMin = '59';
    	}

    	if ($templateID == -1)
    	{
			// do nothing
    	}
    	else
    	{
            $Title = $lnotice->Title;
            $NoticeNumber = $lnotice->NoticeNumber;
            $Description = $lnotice->Description;
            $Question = $lform->getConvertedString($lnotice->Question);
            $DisplayQuestionNumber = $lnotice->DisplayQuestionNumber;
            
            // [2019-0118-1125-41206] handle previous description content
            if(strtoupper($lnotice->Module) == "PAYMENT" && !$lnotice->isPaymentNoticeApplyEditor) {
                $Description = nl2br($Description);
            }
    	}
    	
	$x = "<tr valign='top'>";
		$x .= "<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'>$i_Notice_FromTemplate</span></td>";
        $x .= "<td class='tabletext'>$template_selection</td>";
	$x .= "</tr>";
	
	$x .= "<tr valign='top'>";
		$x .= "<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'>$i_Notice_RecipientType</span></td>";
		$x .= "<td class='tabletext'>$type_selection</td>";
	$x .= "</tr>";
	
	$x .= $nextSelect;
	
	// [2015-0323-1602-46073] PIC field
	$x .= "<tr valign='top'>";
		$x .= "<td nowrap class='field_title'>$i_Profile_PersonInCharge</td>";
		$x .= "<td><table border='0' cellpadding='0' cellspacing='0'>";
			$x .= "<tr>";
// 				$x .= "<td>".$linterface->GET_SELECTION_BOX($PICUser, "id='target_PIC' name='target_PIC[]' size='6' multiple", "")."</td>";
// 				$x .= "<td valign='bottom'>";
// 					$x .= $linterface->GET_BTN($Lang['Btn']['Select'], "button", "newWindow('/home/common_choose/index.php?fieldname=target_PIC[]&permitted_type=1&DisplayGroupCategory=1&DisplayInternalRecipientGroup=1&Disable_AddGroup_Button=1', 16)");
// 					$x .= "<br>";
// 					$x .= $linterface->GET_BTN($Lang['Btn']['Remove'], "button", "checkOptionRemove(document.form1.elements['target_PIC[]'])");
// 				$x .= "</td>";
			    $x .= "<td class='tablerow2' valign='top'>";
			        $x .= "<div id='divSelectPIC'>"."</div>";
			    $x .= "</td>";
			$x .= "</tr>";
		$x .= "</table></td>";
	$x .= "</tr>";
	
	$x .= "<tr valign='top'>";
		$x .= "<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'>$i_Notice_NoticeNumber</span> <span class='tabletextrequire'>*</span></td>";
		$x .= "<td class='tabletext'><input type='text' name='NoticeNumber' maxlength='50' class='textboxnum'> ";
		if(!$sys_custom['eNotice']['HideCurrentNoticeList'])
		{
			$x .= "&nbsp;<a href=javascript:newWindow('currentlist.php?moduleName=Payment',1) class='tablelink'>[$i_Notice_CurrentList]</a>";
		}
		$x .="</td>";
	$x .= "</tr>";
	
	$x .= "<tr valign='top'>";
		$x .= "<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'>$i_Notice_Title</span> <span class='tabletextrequire'>*</span></td>";
		$x .= "<td class='tabletext'><input type='text' name='Title' maxlength='255' value='$Title' class='textboxtext'></td>";
	$x .= "</tr>";
	
	$x .= "<tr valign='top'>";
		$x .= "<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'>$i_Notice_DateStart</span> <span class='tabletextrequire'>*</span></td>";
		$x .= "<td class='tabletext'>".$linterface->GET_DATE_PICKER("DateStart",$startDate).$linterface->Get_Time_Selection_Box('DateStart_hour', 'hour', $startDateHour, $others_tab='').':'.$linterface->Get_Time_Selection_Box('DateStart_min', 'min', $startDateMin, $others_tab='')."
							".$linterface->GET_HIDDEN_INPUT('DateStartTime', 'DateStartTime', $startDateHour.':'.$startDateMin.':00')."</td>";
	$x .= "</tr>";
	
	$x .= "<tr valign='top'>";
		$x .= "<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'>$i_Notice_DateEnd</span> <span class='tabletextrequire'>*</span></td>";
		$x .= "<td class='tabletext'> ". $linterface->GET_DATE_PICKER("DateEnd",$endDate).$linterface->Get_Time_Selection_Box('DateEnd_hour', 'hour', $endDateHour, $others_tab='').':'.$linterface->Get_Time_Selection_Box('DateEnd_min', 'min', $endDateMin, $others_tab='')."
							".$linterface->GET_HIDDEN_INPUT('DateEndTime', 'DateEndTime', $endDateHour.':'.$endDateMin.':00')."</td>";
	$x .= "</tr>";
	
	// [2019-0118-1125-41206] apply FCKEditor for text input
	include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
	$objHtmlEditor = new FCKeditor("Description", "100%", "320", "", "Basic2_withInsertImageFlash", htmlspecialchars_decode($Description), $is_scrollable=true);
	$objHtmlEditor->Config['FlashImageInsertPath'] = $li->returnFlashImageInsertPath($cfg['fck_image']['eNotice'], $id);
	
	$x .= "<tr valign='top'>";
		$x .= "<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'>$i_Notice_Description</span></td>";
		//$x .= "<td class='tabletext'>". $linterface->GET_TEXTAREA("Description", $Description)."</td>";
		$x .= "<td>".$objHtmlEditor->CreateHtml()."</td>";
	$x .= "</tr>";
	
	$x .= "<tr valign='top'>";
        $x .= "<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'>$i_Notice_Attachment</span></td>";
        $x .= "<td class='tabletext'>";
        $x .= "<table border='0' cellspacing='1' cellpadding='1'>";
            $x .= "<tr>";
            $x .= "<td>";
            $x .= "<select name='Attachment[]' size='4' multiple><option>";
                for($i = 0; $i < 40; $i++) {
                    $x .= "&nbsp;";
                }
                $x .= "</option></select>";
            $x .= "</td>";
            $x .= "<td>";
                $x .= $linterface->GET_BTN($i_frontpage_campusmail_attach, "button","newWindow('attach.php?folder=$noticeAttFolder',2)") . "<br>";
                $x .= $linterface->GET_BTN($i_frontpage_campusmail_remove, "button","checkOptionRemove(document.form1.elements['Attachment[]'])") . "<br>";
            $x .= "</td>";
            $x .= "</tr>";
        $x .= "</table>";
        $x .= "</td>";
	$x .= "</tr>";
	
	$x .= "<tr valign='top'>";
		$x .= "<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'>$i_Notice_ReplySlip</span></td>";
		$x .= "<td class='tabletext'>";
			$x .= $linterface->GET_BTN($button_edit, "button","newWindow('paymentNotice_editform.php',1)") ."&nbsp;";
			$x .= $linterface->GET_BTN($button_preview, "button","newWindow('paymentNotice_preview.php?preview_with_subsidy=1',10)");
			$x .= "<br><input name=\"AllFieldsReq\" type=\"checkbox\" value=\"1\" id=\"AllFieldsReq\" " . ($lnotice->AllFieldsReq ? "checked":"") . "> <label for=\"AllFieldsReq\">". $i_Survey_AllRequire2Fill ."</label>";
            // [2020-0803-0925-40235]
			$x .= "<br/><span class=\"tabletextremark\">(".$i_ResourceRemark.": ".$Lang['eNotice']['PaymentNotice_ReminderAfterEditMustSubmit'].")</span>";
	        $x .= "<br><input name='DisplayQuestionNumber' type='checkbox' value='1' id='DisplayQuestionNumber' " . ($DisplayQuestionNumber ? "checked":"") . "> <label for='DisplayQuestionNumber'>". $Lang['eNotice']['DisplayQuestionNumber'] ."</label>";
	$x .= "</td></tr>";
	
	$x .= "<tr valign='top'>";
        $x .= "<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'>".$Lang['eNotice']['Options']."</span></td>";
        $x .= "<td class='tabletext'>";
        $x .= "<table border='0' cellpadding='1' cellspacing='1' width='100%'>";
            $x .= "<tr><td>";

    // [2020-0604-1821-16170]
    //if($lnotice->needApproval){
    if($lnotice->payment_needApproval)
    {
        // [2020-0604-1821-16170]
		// if UserID in Array -> this user is in Approval Group
        //if($lnotice->hasApprovalRight()){
        if($lnotice->hasPaymentNoticeApprovalRight()){
			$needApprove = false;
		}
		else{
			if($lnotice->RecordStatus == 1){
				$needApprove = false;
			}
			else{
				$needApprove = true;
			}
		}
	}
    // [2020-0604-1821-16170]
	//$check = $lnotice->sendPushMsg? "checked":"";
    $check = $lnotice->payment_sendPushMsg? "checked":"";
	$scheduledChecked = true; //in new.php $scheduledChecked always is the default tick
	$htmlAry['sendTimeNowRadio'] = $linterface->Get_Radio_Button('sendTimeRadio_now', 'sendTimeMode', 'now', $nowChecked, $Class="", $Lang['AppNotifyMessage']['Now'], "clickedSendTimeRadio('now');" );
	$htmlAry['sendTimeScheduledRadio'] = $linterface->Get_Radio_Button('sendTimeRadio_scheduled', 'sendTimeMode', 'scheduled', $scheduledChecked, $Class="", $Lang['AppNotifyMessage']['SpecificTime'], "clickedSendTimeRadio('scheduled');" );
	
	$nowTime = time();
	$selectionBoxDate = date('Y-m-d', $nowTime);
	$selectionBoxHour = $nextTimeslotHour;
	$selectionBoxMinute = $nextTimeslotMinute;
	
	$y = '';
	$y .= "&nbsp;&nbsp;";
	$y .= $linterface->GET_DATE_PICKER('sendTime_date', $selectionBoxDate);
	$y .= $linterface->Get_Time_Selection_Box('sendTime_hour', 'hour', $selectionBoxHour);
	$y .= " : ";
	$y .= $linterface->Get_Time_Selection_Box('sendTime_minute', 'min', $selectionBoxMinute, $others_tab='', $interval=15);
	$y .= "&nbsp;&nbsp;";
	$y .= $linterface->GET_SMALL_BTN($Lang['AppNotifyMessage']['SyncWithIssueDate'],'button','javascript:syncIssueTimeToPushMsg()');
	$y .= "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id='div_push_message_date_err_msg'></span>";
	$y .= "<input type='hidden' id='sendTimeString' name='sendTimeString' value='' />";
	$htmlAry['sendTimeDateTimeDisplay'] = $y;
	
	$x .= "<input type='radio' name='sus_status' value='".(($needApprove)?4:1)."' id='status1' checked onClick='js_show_email_notification(this.value);'><label for='status1'>". (($needApprove)?$Lang['eNotice']['NeedApproval']:$Lang['eNotice']['ApproveAndIssue'])."</label><br />";
// 	$x .= "<span style='margin-left:20px; ".($plugin['eClassApp'] ? "" : "display: none;")."'>

// 			<input type='checkbox' name='pushmessagenotify' value='1' id='pushmessagenotify'> <label for='pushmessagenotify'>". $Lang['eNotice']['NotifyParentsByPushMessage']."</label><br>
// 			</span>";
	
	if($plugin['eClassApp'])
	{
    // 	$x .= "<span style='margin-left:20px; ".($plugin['eClassApp'] ? "" : "display: none;")."'>";
        $x .= "<span style='margin-left:20px;'>";
        $x .= "<input type='checkbox' name='pushmessagenotify' value='1' id='pushmessagenotify'  onclick='checkedSendPushMessage();' $check> <label for='pushmessagenotify'>". $Lang['eNotice']['NotifyParentsByPushMessage']."</label><br/>
               </span>";
        $x .= "<span style='margin-left:20px;'>";
        $x .= "<input type='checkbox' name='pushmessagenotify_Students' value='1' id='pushmessagenotify_Students'  onclick='checkedSendPushMessage();' $check> <label for='pushmessagenotify_Students'>". $Lang['eNotice']['NotifyStudentsByPushMessage']."</label><br/>
               </span>";
        $x .= "<span style='margin-left:20px;'>";
        $x .= "<input type='checkbox' name='pushmessagenotify_PIC' value='1' id='pushmessagenotify_PIC'  onclick='checkedSendPushMessage();' $check> <label for='pushmessagenotify_PIC'>". $Lang['eNotice']['NotifyPICByPushMessage']."</label><br/>
               </span>";
        $x .= "<span style='margin-left:20px;'>";
        $x .= "<input type='checkbox' name='pushmessagenotify_ClassTeachers' value='1' id='pushmessagenotify_ClassTeachers'  onclick='checkedSendPushMessage();' $check> <label for='pushmessagenotify_ClassTeachers'>". $Lang['eNotice']['NotifyClassTeachersByPushMessage']."</label><br/>
               </span>";
        $x .= "<div id = 'PushMessageSetting'>".$linterface->Get_Warning_Message_Box($Lang['eClassApps']['PushMessage']." ".$Lang['ePost']['Setting'],
                                                                                "<div id='sendTimeSettingDiv'>
                                                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$htmlAry['sendTimeNowRadio']."
                                                                                            <br />
                                                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$htmlAry['sendTimeScheduledRadio']."
                                                                                            <br />
                                                                                            <div id='specificSendTimeDiv' style='".($scheduledChecked ? "" : "display: none;")."'>
                                                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$htmlAry['sendTimeDateTimeDisplay']."
                                                                                            </div>
                                                                                        </div></div>");
	    $x .= "<hr />";
	}
	// [2020-0604-1821-16170]
	//$emailChecked = $lnotice->sendEmail ? 'checked' : '';
    $emailChecked = $lnotice->payment_sendEmail ? 'checked' : '';

	$x .= "<span style='margin-left:20px;'>";
	$x .= "<input type='checkbox' name='emailnotify' value='1' id='emailnotify' $emailChecked> <label for='emailnotify'>". $Lang['eNotice']['NotifyParentsByEmail']."</label><br>";
	$x .= "</span>";
	$x .= "<span style='margin-left:20px;'>";
	$x .= "<input type='checkbox' name='emailnotify_Students' value='1' id='emailnotify_Students' $emailChecked> <label for='emailnotify_Students'>". $Lang['eNotice']['NotifyStudentsByEmail']."</label><br/>";
	$x .= "</span>";
	$x .= "<span style='margin-left:20px;'>";
	$x .= "<input type='checkbox' name='emailnotify_PIC' value='1' id='emailnotify_PIC' $emailChecked> <label for='emailnotify_PIC'>". $Lang['eNotice']['NotifyPICByEmail']."</label><br/>";
	$x .= "</span>";
	$x .= "<span style='margin-left:20px;'>";
	$x .= "<input type='checkbox' name='emailnotify_ClassTeachers' value='1' id='emailnotify_ClassTeachers' $emailChecked> <label for='emailnotify_ClassTeachers'>". $Lang['eNotice']['NotifyClassTeachersByEmail']."</label><br/>";
	$x .= "</span>";
	$x .= "<br><span style='margin-left:20px;'>(". $Lang['eNotice']['NotifyEmailRemark']  .")</span>";
	$x .= "</td>";
	$x .= "</tr>";
	$x .= "<tr>";
	$x .= "<td>";
	$x .= "<input type='radio' name='sus_status' value='2' id='status2' onClick='js_show_email_notification(this.value);'><label for='status2'>". $Lang['eNotice']['NotToBeDistributed']."</label><br />";
	$x .= "</td>";
	$x .= "</tr>";
	$x .= "<tr>";
	$x .= "<td>";
	$x .= "<input type='radio' name='sus_status' value='3' id='status3' onClick='js_show_email_notification(this.value);'><label for='status3'>". $i_Notice_StatusTemplate."</label><br />". $i_Notice_TemplateNotes."<br />";
	$x .= "</td>";
	$x .= "</tr>";
        $x .= "</table>";
        $x .= "</td>";
	$x .= "</tr>";
    //$x .= "<input type='radio' name='sus_status' value='1' id='status1' checked><label for='status1'>$i_Notice_StatusPublished</label><br />";
	//$x .= "<input type='radio' name='sus_status' value='2' id='status2'><label for='status2'>$i_Notice_StatusSuspended</label><br />";
	//$x .= "<input type='radio' name='sus_status' value='3' id='status3'><label for='status3'>$i_Notice_StatusTemplate</label><br />$i_Notice_TemplateNotes<br />";
    //$x .= "</td>";
	//$x .= "</tr>";
    /*
	$x .= "<tr valign='top'>";
	$x .= "<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'>&nbsp;</span></td>";
	$x .= "<td class='tabletext'>";
	$x .= "<input type='checkbox' name='emailnotify' value='1' id='emailnotify'> <label for='emailnotify'>$i_Notice_SendEmailToParent</label><br>";
	$x .= "<input type='checkbox' name='emailnotify_Students' value='1' id='emailnotify_Students'> <label for='emailnotify_Students'>".$Lang['eNotice']['SendEmailNotificationToStudent']."</label>";
	$x .= "</td>";
    $x .= "</tr>";
    */
    
    $x .= "<tr valign=\"top\"".($use_direct_pay?" style=\"display:none;\" ":"").">";
        $x .= "<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'>".$eEnrollment['debit_method']."</span></td>";
        $x .= "<td class='tabletext'>";
            $x .= "<input type='radio' name='debit_method' value='1' id='debit_method1' checked ><label for='debit_method1'>".$eEnrollment['debit_directly']."</label>&nbsp;";
            $x .= "<input type='radio' name='debit_method' value='2' id='debit_method2' ><label for='debit_method2'>".$eEnrollment['debit_later']."</label>";
        $x .= "</td>";
	$x .= "</tr>";


    if ($use_merchant_account) {
		if($sys_custom['ePayment']['MultiPaymentGateway']) {
			$x .= '<tr valign="top">
				<td width="30%" valign="top" nowrap="nowrap" class="field_title">
    				<span class="tabletext">'.$Lang['ePayment']['ServiceProviderAndSchoolAccount'].'</span>
    			</td>
    			<td class="tabletext" width="70%">
    			<span id="div_merchant_err_msg"></span>
    			';

			$x .= '<table>';
			$service_provider_list = $lpayment->getPaymentServiceProviderMapping(false,true);
			foreach($service_provider_list as $k=>$temp) {
				$merchants = $lpayment->getMerchantAccounts(array('ServiceProvider'=>$k,'RecordStatus'=>1,'order_by_accountname'=>true));
				$merchant_selection_data = array();
				for($i=0;$i<count($merchants);$i++){
					$merchant_selection_data[] = array($merchants[$i]['AccountID'],$merchants[$i]['AccountName']);
				}
				$x .= '<tr>';
				$x .= '<td>'.$temp.'</td>';
				$x .= '<td>';
				$x .= $linterface->GET_SELECTION_BOX($merchant_selection_data, ' id="MerchantAccountID_'.$k.'" name="MerchantAccountID_'.$k.'" onchange="onMerchantChange(this,\''.$k.'\')"', $Lang['General']['EmptySymbol'], '');
				foreach($merchant_selection_data as $merchant_data) {
					$year = date('Y');
					$month = date('m');
					$sql = "SELECT * FROM PAYMENT_MERCHANT_ACCOUNT_QUOTA WHERE Year='$year' AND Month='$month' AND AccountID='".$merchant_data[0]."'";
					$rs_quota = $lpayment->returnArray($sql);
					if(count($rs_quota)>0) {
						$remain_quota = $rs_quota[0]['Quota'] - $rs_quota[0]['UsedQuota'];
						if($remain_quota < 0) {
							$remain_quota = 0;
						}
						$quota_str = str_replace('{COUNT}', $remain_quota, $Lang['ePayment']['CurrentRemainQuota']);
						$x .= ' <span class="quota_' . $k . ' quota_id_' . $merchant_data[0] . '" style="display: none;">(' . $quota_str . ')</span>';
					}
				}
				$x .= '</td>';
				$x .= '</tr>';
			}
			$x .= '</table>';
			$x .= '	</td>
			</tr>';
		} else {
			$merchants = $lpayment->getMerchantAccounts(array('RecordStatus' => 1, 'order_by_accountname' => true));
			$merchant_selection_data = array();
			for ($i = 0; $i < count($merchants); $i++) {
				$merchant_selection_data[] = array($merchants[$i]['AccountID'], $merchants[$i]['AccountName']);
			}

			$x .= '<tr valign="top">
                <td width="30%" valign="top" nowrap="nowrap" class="field_title">
                    <span class="tabletext">' . $Lang['ePayment']['ServiceProviderAndSchoolAccount'] . '</span>
                </td>
                <td class="tabletext" width="70%">
                    ' . $linterface->GET_SELECTION_BOX($merchant_selection_data, ' id="MerchantAccountID" name="MerchantAccountID" ', $Lang['General']['EmptySymbol'], count($merchants) > 0 ? $merchants[0]['AccountID'] : '') . '
                </td>
            </tr>';
		}
    }
}

### Title ###
$TAGS_OBJ[] = array($Lang['eNotice']['PaymentNotice']);
$MODULE_OBJ = $lnotice->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($i_Notice_New);

echo $linterface->Include_AutoComplete_JS_CSS();
?>

<?php include_once ($PATH_WRT_ROOT . "home/common_choose/user_selection_plugin_js.php");?>
<script language="javascript">
var AutoCompleteObj_ClassNameClassNumber;
var AutoCompleteObj_UserLogin;

function onMerchantChange(obj, sp) {
    var id = $(obj).val();
    $(".quota_"+sp).hide();
    $(".quota_id_"+id).show();
}

$(document).ready( function() {
	var divId ='divSelectStudent'
	var targetUserTypeStudentAry = [2]; 
	var StudentSelectedFieldName = 'target[]'; 
	var StudentSelectedFieldId = 'target[]'; 
	var selectedUsersData = "";
    var extraParmData = "";
	<?php if($isAudienceLimited) { ?>
	    extraParmData = ['filterClassTeacherClass', 'filterOwnClassesOnly'];
    <?php } ?>
    Init_User_Selection_Menu(divId, targetUserTypeStudentAry, StudentSelectedFieldName, StudentSelectedFieldId, selectedUsersData, '', extraParmData);

	var divId ='divSelectPIC'
	var targetUserTypePICAry = [1]; 
	var PICSelectedFieldName = 'target_PIC[]'; 
	var PICSelectedFieldId = 'target_PIC'; 
	var selectedUsersData = "";
	Init_User_Selection_Menu(divId,targetUserTypePICAry,PICSelectedFieldName, PICSelectedFieldId,selectedUsersData);
	
	<?php if($plugin['eClassApp']) { ?>
        <?php // if($lnotice->sendPushMsg) { ?>
        <?php if($lnotice->payment_sendPushMsg) { ?>
			$('div#PushMessageSetting').show();
		<?php } else { ?>
			$('div#PushMessageSetting').hide();
		<?php } ?>
	<?php } ?>

	<?php if ($_GET["type"] == 4) { ?>
		// initialize jQuery Auto Complete plugin
		Init_JQuery_AutoComplete('UserClassNameClassNumberSearchTb', 'UserLoginSearchTb');
		$('input#UserClassNameClassNumberSearchTb').focus();
	<?php } ?>
	
	<?php if($_GET["type"]) { ?>
		var initStartDate = getMillisecond(getCurrentDate());
		var endDate = getMillisecond($("input#DateEnd").val());
		var diff = endDate - initStartDate;
		var newStartDate;

		$(document).click( function(){
			newStartDate = getMillisecond(getCurrentDate());
			if(isNaN(newStartDate)){
				return;
			}

			if(initStartDate != newStartDate){
				var newDateEnd = new Date(newStartDate+diff);
				jQuery.datepick._selectDay('#DateEnd',newDateEnd,this);
				initStartDate = newStartDate;
			}
		});
	<?php } ?>
});

function getMillisecond(dateString){
	var millisecond = Date.parse(dateString);
	if(isNaN(millisecond)){
		dateString = dateString.replace(/-/g, '/');
		millisecond = Date.parse(dateString);
	}

	return Date.parse(dateString);
}

function getCurrentDate(){
	var date = $("#DateStart").val();
	return date;
}

// function Init_JQuery_AutoComplete(InputID_ClassNameClassNumber, InputID_UserLogin) {
// 	AutoCompleteObj_ClassNameClassNumber = $("input#" + InputID_ClassNameClassNumber).autocomplete(
// 		"../ajax_search_user_by_classname_classnumber.php",
// 		{
// 			onItemSelect: function(li) {
// 				Add_Selected_User(li.extra[1], li.selectValue, InputID_ClassNameClassNumber);
// 			},
// 			formatItem: function(row) {
// 				return row[0];
// 			},
// 			maxItemsToShow: 100,
// 			minChars: 1,
// 			delay: 0,
// 			width: 200
// 		}
// 	);
	
// 	Update_Auto_Complete_Extra_Para();
// }

// function Add_Selected_User(UserID, UserName, InputID) {
// 	var UserID = UserID || "";
// 	var UserName = UserName || "";
// 	var UserSelected = document.getElementById('target[]');

// 	UserSelected.options[UserSelected.length] = new Option(UserName, UserID);
	
// 	Update_Auto_Complete_Extra_Para();
	
// 	// reset and refocus the textbox
// 	$('input#' + InputID).val('').focus();
// }

function Init_JQuery_AutoComplete(Input_Search_Field, Target_User_Type, Target_Select_Field_Id) {
	if(Target_User_Type == 3){
		var ajaxUrl="<?=$PATH_WRT_ROOT;?>home/common_choose/ajax_search_parent.php";
	}
	else if(Target_User_Type == 2){
		var ajaxUrl="../ajax_search_user_by_classname_classnumber.php";
	}
	else if(Target_User_Type == 1){
		var ajaxUrl="<?=$PATH_WRT_ROOT;?>home/common_choose/ajax_search_user.php";
	}
	
	var AutoCompleteObj = $("input#" + Input_Search_Field).autocomplete(
        ajaxUrl,
		{
			onItemSelect: function(li) {
				Add_Selected_User(li.extra[1], li.selectValue, Input_Search_Field, Target_Select_Field_Id);
			},
			formatItem: function(row) {
				return row[0];
			},
			maxItemsToShow: 100,
			minChars: 1,
			delay: 0,
			width: 200,
			
		}
	);

	if(Target_User_Type == 3){
		AutoCompleteObj_parent=AutoCompleteObj;
	}
	else if(Target_User_Type == 2){
		AutoCompleteObj_student = AutoCompleteObj;
	}
	else if(Target_User_Type == 1){
		AutoCompleteObj_staff = AutoCompleteObj;
	}
	Update_Auto_Complete_Extra_Para(Target_Select_Field_Id);
}

// function Update_Auto_Complete_Extra_Para(){
// 	checkOptionAll(document.getElementById('form1').elements["target[]"]);
// 	ExtraPara = new Array();
// 	ExtraPara['SelectedUserIDList'] = Get_Selection_Value('target[]', 'Array', true);
// 	AutoCompleteObj_ClassNameClassNumber[0].autocompleter.setExtraParams(ExtraPara);
// }

// function js_Remove_Selected_Student(){
// 	checkOptionRemove(document.getElementById('target[]'));
// 	Update_Auto_Complete_Extra_Para();
// }

function compareDate(s1,s2)
{
    y1 = parseInt(s1.substring(0,4),10);
    y2 = parseInt(s2.substring(0,4),10);
    m1 = parseInt(s1.substring(5,7),10);
    m2 = parseInt(s2.substring(5,7),10);
    d1 = parseInt(s1.substring(8,10),10);
    d2 = parseInt(s2.substring(8,10),10);

    if (y1 > y2)
    {
        return 1;
    }
    else if (y1 < y2)
    {
        return -1;
    }
    else if (m1 > m2)
    {
        return 1;
    }
    else if (m1 < m2)
    {
        return -1;
    }
    else if (d1 > d2)
    {
        return 1;
    }
    else if (d1 < d2)
    {
        return -1;
    }
    return 0;
}

function checkform2(obj) {
	$('#submit2').attr('disabled', true);
	var canSubmit = checkform(obj);
	if (canSubmit) {
		obj.action = 'paymentNotice_new_update.php';
		obj.method = 'post';
		obj.submit();
	}
	else {
		$('#submit2').attr('disabled', false);
	}
}

function checkform(obj){
	checkOption(obj.elements['Attachment[]']);
     
	<?php if ($type == 2) { ?>
		if(!countChecked(obj, "target[]")){ alert("<?=$i_alert_pleaseselect?><?=$i_Notice_RecipientLevel?>"); return false; }
    <? } ?>
    
    <?php if ($type == 3) { ?>
		if(!countChecked(obj, "target[]")){ alert("<?=$i_alert_pleaseselect?><?=$i_Notice_RecipientClass?>"); return false; }
    <? } ?>
    
    <?php if ($type == 4) {?>
		if(obj.elements["target[]"].length == 0){ alert("<?php echo $i_frontpage_campusmail_choose; ?>"); return false; }
		checkOptionAll(obj.elements["target[]"]);
    <? } ?>
     
	if(obj.elements["target_PIC[]"].length != 0)
	{
 		 checkOptionAll(obj.elements["target_PIC[]"]);
	}
    
    if(!check_text(obj.NoticeNumber, "<?php echo $i_alert_pleasefillin.$i_Notice_NoticeNumber; ?>.")) return false;
    if(!check_text(obj.Title, "<?php echo $i_alert_pleasefillin.$i_Notice_Title; ?>.")) return false;
     
    if(!check_date(obj.DateStart,"<?php echo $i_invalid_date; ?>")) return false;
    if(!check_date(obj.DateEnd,"<?php echo $i_invalid_date; ?>")) return false;
    
    if(compareDate(obj.DateStart.value, obj.DateEnd.value) > 0) {alert ("<?php echo $i_Homework_new_duedate_wrong; ?>"); return false;}
    if(compareDate('<?=date('Y-m-d')?>', obj.DateStart.value) > 0) {alert ("<?php echo $i_Homework_new_startdate_wrong; ?>"); return false;}
	
   	// assign time into hidden field
 	var dateStartTime = str_pad($('#DateStart_hour').val(),2)+':'+str_pad($('#DateStart_min').val(),2)+':00';
 	$('#DateStartTime').val(dateStartTime);
 	var dateEndTime = str_pad($('#DateEnd_hour').val(),2)+':'+str_pad($('#DateEnd_min').val(),2)+':59';
 	$('#DateEndTime').val(dateEndTime);
	
 	// check is type radio is checked
	var sus_status = $('input[name=sus_status]:checked').val();
	if(sus_status > 0){
		//do nothing
	}
	else{
		alert('Please select type of the notice');
		return false;
	}
	
	// 26-9-2016 villa add scheduleString to be scheduletime if it is selected  
	// check if the schedule day is in the past
	var scheduleString = '';
	if ($('input#sendTimeRadio_scheduled').attr('checked')) {
	    scheduleString = $('input#sendTime_date').val() + ' ' + str_pad($('select#sendTime_hour').val(),2) + ':' + str_pad($('select#sendTime_minute').val(),2) + ':00';
	}
	$('input#sendTimeString').val(scheduleString);
	
	var dateNow = new Date();
	var dateNowString = dateNow.getFullYear() + '-' + str_pad((dateNow.getMonth()+1),2) + '-' + str_pad(dateNow.getDate(),2) + ' ' + str_pad(dateNow.getHours(),2) + ':' + str_pad(dateNow.getMinutes(),2) + ':' + str_pad(dateNow.getSeconds(),2);
    if($('#pushmessagenotify').attr('checked')){    	
		if ($('input#sendTimeRadio_scheduled').attr('checked') && scheduleString <= dateNowString) {
			document.getElementById('div_push_message_date_err_msg').innerHTML = '<font color="red"><?=$Lang['AppNotifyMessage']['WarningMsg']['SendTimeInvalid']?></font>';
			$('input#sendTime_date').focus();
			return false;
		}     
    } 
	//check if the schedule day is greater than 31days after the issue day 
	if($('#pushmessagenotify').attr('checked')){
		var maxDate = new Date();
		maxDate.setDate(dateNow.getDate() + 31);
		var maxDateString = maxDate.getFullYear() + '-' + str_pad((maxDate.getMonth()+1),2) + '-' + str_pad(maxDate.getDate(),2) + ' ' + str_pad(maxDate.getHours(),2) + ':' + str_pad(maxDate.getMinutes(),2) + ':' + str_pad(maxDate.getSeconds(),2);
		if ($('input#sendTimeRadio_scheduled').attr('checked')&& scheduleString > maxDateString) {
			$('input#sendTime_date').focus();
			document.getElementById('div_push_message_date_err_msg').innerHTML = '<font color="red"><?=$Lang['AppNotifyMessage']['WarningMsg']['PleaseSelectDateWithinRange']?></font>';
			return false;
		}
	}

	<?php if($sys_custom['ePayment']['MultiPaymentGateway']) { ?>

    var have_merchantid = false;
    $("select[id^=MerchantAccountID_]").each(function(e) {
        if($(this).val() != '') {
            have_merchantid = true;
        }
    });

    if(have_merchantid == false) {
        if(confirm('<?=$Lang['ePayment']['SetupMerchant']?>') == false) {
            return false;
        }
    }
	<?php }?>

	checkOptionAll(obj.elements["Attachment[]"]);
	return true;
}

function clickCancel()
{
	with(document.form1)
    {
    	action = "paymentNotice.php";
        submit();
    }
}

function js_show_email_notification(val)
{
	if(val == 1 || val == 4)
	{
		$('#pushmessagenotify').removeAttr('disabled');
		$('#pushmessagenotify_Students').removeAttr('disabled');
		$('#pushmessagenotify_PIC').removeAttr('disabled');
		$('#pushmessagenotify_ClassTeachers').removeAttr('disabled');
		$('#emailnotify').removeAttr('disabled');
		$('#emailnotify_Students').removeAttr('disabled');
		$('#emailnotify_PIC').removeAttr('disabled');
		$('#emailnotify_ClassTeachers').removeAttr('disabled');
		var parentCheck = document.getElementById("pushmessagenotify");
		var studnetCheck = document.getElementById("pushmessagenotify_Students");
		var picCheck = document.getElementById("pushmessagenotify_PIC");
		var classTeacherCheck = document.getElementById("pushmessagenotify_ClassTeachers");
		if((parentCheck && parentCheck.check == true) || (studnetCheck && studnetCheck.check == true) || (picCheck && picCheck.check == true) || (classTeacherCheck && classTeacherCheck.check == true)) {
			$('div#sendTimeSettingDiv').show();
			$('div#PushMessageSetting').show();
		}
	}
	else
    {
		$('#pushmessagenotify').removeAttr('checked');
		$('#pushmessagenotify_Students').removeAttr('checked');
		$('#pushmessagenotify_PIC').removeAttr('checked');
		$('#pushmessagenotify_ClassTeachers').removeAttr('checked');
		$('#emailnotify').removeAttr('checked');
		$('#emailnotify_Students').removeAttr('checked');
		$('#emailnotify_PIC').removeAttr('checked');
		$('#emailnotify_ClassTeachers').removeAttr('checked');
		
		$('#pushmessagenotify').attr('disabled','true');
		$('#pushmessagenotify_Students').attr('disabled','true');
		$('#pushmessagenotify_PIC').attr('disabled','true');
		$('#pushmessagenotify_ClassTeachers').attr('disabled','true');
		$('#emailnotify').attr('disabled','true');
		$('#emailnotify_Students').attr('disabled','true');
		$('#emailnotify_PIC').attr('disabled','true');
		$('#emailnotify_ClassTeachers').attr('disabled','true');
		
        $('div#sendTimeSettingDiv').hide();
		$('div#PushMessageSetting').hide();
	}
}

<?php if($plugin['eClassApp']){?>
function checkedSendPushMessage(){
	if($('#pushmessagenotify').attr('checked') || $('#pushmessagenotify_Students').attr('checked') || $('#pushmessagenotify_PIC').attr('checked') || $('#pushmessagenotify_ClassTeachers').attr('checked')) {
		$('div#sendTimeSettingDiv').show();
		$('div#PushMessageSetting').show();
	}
	else{
		$('div#sendTimeSettingDiv').hide();
		$('div#PushMessageSetting').hide();
	}
}

function clickedSendTimeRadio(targetType) {
	if (targetType == 'now') {
		$('div#specificSendTimeDiv').hide();
	}
	else {
		$('div#specificSendTimeDiv').show();
	}
}

function syncIssueTimeToPushMsg(){
	var issueDate = $('#DateStart').val();
//	var issueHour = '<?= $startDateHour?>';
//	var issueMin = '<?= $startDateMin?>';
	var issueHour = parseInt($('#DateStart_hour option:selected').val());
	var issueMin = parseInt($('#DateStart_min option:selected').val());
				
	if(issueMin == 0){
		issueMin = 00;
	}
	else if(issueMin <= 15){
		issueMin = 15;
	}
	else if(issueMin <= 30){
		issueMin = 30;
	}
	else if(issueMin <= 45){
		issueMin = 45;
	}
	else{
		issueMin = 00;
		issueHour++
	}
	
	$('#sendTime_date').val(issueDate);;
	$('#sendTime_hour').val(issueHour);
	$('#sendTime_minute').val(issueMin);
}
<?php } ?>

</script>

<br />   
<form name="form1" id="form1" method="get" action="paymentNotice_new.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" >
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>

<tr>
	<td colspan="2">
        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center" class="form_table_v30">
            <tr>
                <td><br />
                <table align="center" width="30%" border="0" cellpadding="5" cellspacing="0"  class="form_table_v30">
                    <?=$x?>
                </table>
                </td>
            </tr>
        </table>
	</td>
</tr>

<?php if ($type_selected) { ?>
<tr>
	<td colspan="2">        
        <table width="99%" border="0" cellspacing="0" cellpadding="5" align="center">
        <tr>
            <td align="left" class="tabletextremark"><?=$i_general_required_field2?></td>
        </tr>
        <tr>
            <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        </tr>
        <tr>
            <td align="center">
                <?//= $linterface->GET_ACTION_BTN($button_submit, "submit", "checkOption(this.form.elements['Attachment[]']); ". ($type==4 ? "checkOption(this.form.elements['target[]']);" :"") ."this.form.action='paymentNotice_new_update.php'; this.form.method='post'; return checkform(this.form);","submit2") ?>
                <?= $linterface->GET_ACTION_BTN($button_submit, "button", "checkOption(this.form.elements['Attachment[]']); ". ($type==4 ? "checkOption(this.form.elements['target[]']);" : "") ."checkform2(this.form);", "submit2") ?>
                <?= $linterface->GET_ACTION_BTN($button_reset, "reset") ?>
                <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "clickCancel();","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
            </td>
        </tr>
        </table>
<?php } else { ?>
<tr>
	<td colspan="2">        
        <table width="99%" border="0" cellspacing="0" cellpadding="5" align="center" >
        <tr>
            <td><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        </tr>
        <tr>
            <td align="center" class="field_title">
            <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "clickCancel();","cancelbtn") ?>
        </td>
        </tr>
        </table>
<?php } ?>
	</td>
</tr>
</table>
<br />

<input type="hidden" name="qStr" value="<?=$Question?>">
<input type="hidden" name="aStr" value="">
<input type="hidden" name="noticeAttFolder" value="<?=$noticeAttFolder?>" />
<input type="hidden" name="pageNo" value="<?=$pageNo?>" />
<input type="hidden" name="field" value="<?=$field?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$numPerPage?>" />

<input type="hidden" name="noticeType" value="<?=$noticeType?>" />
<input type="hidden" name="status" value="<?=$status?>" />
<input type="hidden" name="year" value="<?=$year?>" />
<input type="hidden" name="month" value="<?=$month?>" />
</form>

<?php if ($type_selected) { ?>
    <script language="JavaScript1.2">
        obj = document.form1.elements["Attachment[]"];
        checkOptionClear(obj);
        <?php
        	while (list($key, $value) = each($files)) {
				echo "checkOptionAdd(obj, \"".$files[$key][0]." (".ceil($files[$key][1]/1000)."Kb".")\", \"".$files[$key][0]."\");\n";
            }
		?>
        checkOptionAdd(obj, "<?php for($i = 0; $i < 40; $i++) { echo " "; } ?>", "");

        <?php if ($templateID != -1 && $lnotice->RecordType == 4) { ?>
	        obj = document.form1.elements["target[]"];
	        checkOptionClear(obj);
	        checkOptionAdd(obj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
        <? } ?>
    </script>
<?php } ?>

<?
intranet_closedb();

if (!$type_selected)
{
	if ($templateID == "") {
		print $linterface->FOCUS_ON_LOAD("form1.templateID");
    }
	else {
        print $linterface->FOCUS_ON_LOAD("form1.type");
    }
}        
else
{
	print $linterface->FOCUS_ON_LOAD("form1.NoticeNumber");
}

$linterface->LAYOUT_STOP();
?>