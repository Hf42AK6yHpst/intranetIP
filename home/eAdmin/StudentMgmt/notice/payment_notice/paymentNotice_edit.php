<?php
// Using : Bill
/*
 * if upload file to client site before v5.1, need to check regional case [2020-0407-1445-44292]
 */

######################
#
#   Date:   2020-11-05  Ray
#           not allow change to template
#
#   Date:   2020-10-30  Ray
#           Add quota
#
#   Date:   2020-10-23  Ray
#           Add remove unsigned student
#
#   Date:   2020-09-17  Bill
#           fixed notification checkbox handling when modify notice status
#
# 	Date:   2020-09-07  Bill    [2020-0604-1821-16170]
#			updated access right checking
#
#   Date:   2020-08-11  Bill    [2020-0803-0925-40235]
#           add remarks next to checkbox - "All questions are required to be answered"
#           disable checkbox if not allow edit reply slip content
#
#   Date:   2020-07-03  Bill    [2020-0604-1717-36170]
#           Handle back button if is copied notice
#
#   Date:   2020-06-29  ray
#           - add multi payment gateway
#
#   Date:   2020-05-04  Bill    [2020-0407-1445-44292]
#           - support class teacher audience limit checking + UI update ($sys_custom['eNotice']['ClassTeacherSelectOwnClassStudentOnly'])
#           - add param js function Init_User_Selection_Menu() in '/home/common_choose/user_selection_plugin_js.php'
#           - added js function Init_JQuery_AutoComplete()
#
#   Date:   2020-02-17 Bill
#           Replace php inline coding (if('<?=$plugin['eClassApp']>'=='1') { ... }) in js
#
#	Date:	2019-12-27  Philips [2019-1210-1655-09276]
#			Fixed: Hide edit NoticeReply Button when SignedCount > 0
#
#	Date:	2019-10-14  Philips
#			Added $pushmessagenotify_PIC, $pushmessagenotify_ClassTeachers, $emailnotify_Students, $emailnotify_PIC, $emailnotify_ClassTeachers
#
#   Date:   2019-09-23  Bill    [2019-0923-1111-44235]
#           To set iframe scrollable
#
#	Date:	2019-08-02 Carlos
#			Do not allow to change merchant account if anyone signed the notice.
#
#	Date:	2019-03-28 Carlos
#			Improved to allow add more target students.
#
#	Date:	2019-02-11 Carlos
#			Hide Debit Method choice if use TNG/AlipayHK to direct pay the notice items.
#
#   Date:   2019-01-25 Bill     [2019-0118-1125-41206]
#           Improved: Description - replace textarea by FCKEditor
#
#	Date:	2018-10-05 Isaac
#           Integrated User_Selection_Menu plugin for PIC
#
#   Date:   2018-10-04 Bill
#           Add hidden input field : noticeAttFolder to avoid using session
#
#	Date:	2018-08-21 Carlos 
#			$sys_custom['ePayment']['Alipay'] - add selection option to set Alipay merchant account for payment item.
#
#	Date:	2018-01-22	Carlos
#           $sys_custom['ePayment']['TNG'] do not display debit method, set default method to [Debit Directly].
#
#	Date:	2017-10-03	Bill	[2017-0926-1454-11066]
#			Added remarks for signed payment notice in status - Not to be distributed
#
#	Date:	2017-09-19	Bill
#			Fixed session register for $noticeAttFolder
#
#	Date:	2017-09-07	Bill	[2017-0906-1332-14066]
#			Improved: Disable submit button after submit the form
#
#	Date:	2016-11-18	(Bill)	[2016-1118-1145-57207]
#			Added logic to generate folder name if attachment field of notice is empty
#
#	Date:	2016-09-26	(Villa)
#			Modified checkform - check if the schedule time is in the past or 30days behind the issue day
#
# 	Date:	2016-09-23	(Villa)
#			Add to module setting - "default using eclass App to notify"
#
#	Date:	2016-09-23	Villa
#			Added Push Message Setting box
#
#	Date:	2016-09-21	Bill	[2016-0915-1432-52206]
#			Hide Not to be distributed and Template Only when Approval User approve or reject notice which is Waiting for Approval
#
#	Date:	2016-07-15	Bill
#			Fixed cannot update attachments
#
#	Date:	2016-07-11	Kenneth
#			Add approval logic
#
#	Date:	2016-06-06	Kenneth
#			Format time as Y-m-d
#
#	Date:	2015-12-11	Bill
#			Replace session_register() by session_register_intranet() for PHP 5.4
#			Replace session_is_registered() by session_is_registered_intranet() for PHP 5.4
#
#	Date:	2015-09-15	Bill
#			Removed php error msg due to implode() in hidden input field - CurrentItemID
#
#	Date:	2015-06-09	Shan
#			Update UI standard
#
#	Date:	2015-04-14	Bill	[2015-0323-1602-46073]
#			Add field to edit PICs of eNotice
#
#	Date:	2014-10-30 Ivan > ip.2.5.5.10.1
#			Fixed: special character handling in reply slip
#
#	Date:	2014-10-14 (Roy)
#			Add push message option, and modify JavaScript
#
#	Date:	2014-03-27	Carlos
#			Hide template type option if the notice is non-template type and already has signed records
#
#	Date:	2014-03-25	YatWoon
#			Add wordings [Case#P59398]
#
#	Date:	2012-06-20 YatWoon
#			Fixed: If edit the reply slip again, the question with "enter" will become "<br>"
#
#	Date:	2011-02-16	YatWoon
#			Add option "Display question number"
#
######################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lnotice = new libnotice($NoticeID);
$lpayment = new libpayment();

// [2020-0407-1445-44292]
$isAudienceLimited = $lnotice->isClassTeacherAudienceTargetLimited();

$CurrentPageArr['eAdminNotice'] = 1;

if($lnotice->Module == 'Payment') {
	$CurrentPage = "PaymentNotice";
}
else {
	$CurrentPage = "PageNotice";
}

$lform = new libform();

// [2020-0604-1821-16170]
//if (!$plugin['notice'] || $_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["notice"]["hasIssueRight"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"]))
if (!$plugin['notice'] || $_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["notice"]["hasPaymentNoticeIssueRight"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$use_merchant_account = $lpayment->useEWalletMerchantAccount();
$use_direct_pay = $lpayment->isEWalletDirectPayEnabled();

# Attachment Folder
// for PHP 5.4, replace session_is_registered() and session_register()
//if(!session_is_registered("noticeAttFolder") || $noticeAttFolder==""){
//     session_register("noticeAttFolder");
//}

// [2016-1118-1145-57207] Generate folder name if empty
$noticeAttFolder = $lnotice->Attachment;
if(trim($noticeAttFolder) == "") {
	$noticeAttFolder = session_id().".".time();
}

// Update Session if noticeAttFolder not set
//if(!session_is_registered_intranet("noticeAttFolder")){
//	$noticeAttFolder = $noticeAttFolder? $noticeAttFolder : "";
//	session_register_intranet("noticeAttFolder", $noticeAttFolder);
//}
session_register_intranet("noticeAttFolder", $noticeAttFolder);

$li = new libfilesystem();
$path = "$file_path/file/notice/$noticeAttFolder";
if(!empty($noticeAttFolder))
{
	$tmp_path = $path."tmp";
	$li->folder_remove_recursive($tmp_path);
	$li->folder_new($tmp_path);
	$li->folder_content_copy($path, $tmp_path);
}

$lo = new libfiletable("", $tmp_path, 0, 0, "");
$files = $lo->files;

$type = $lnotice->RecordType;
$lclass = new libclass();
$recipientNames = $lnotice->returnRecipientNames();
$type_selection = $recipientNames[0];
if ($recipientNames[1] == "")
{
    $nextSelect = "";
}
else
{
    switch($type)
    {
       case 2: $cellTitle = $i_Notice_RecipientLevel; break;
       case 3: $cellTitle = $i_Notice_RecipientClass; break;
       case 4: $cellTitle = $i_Notice_RecipientIndividual; break;
    }
    $nextSelect = "<tr valign='top'>";
    $nextSelect .= "<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'>$cellTitle</span></td>";
    $nextSelect .= "<td class='tabletext'>";

    if($type == 4) {
		$namefield = getNameFieldWithClassNumberByLang("u.");
		$sql = "SELECT $namefield as name, r.RecordStatus, r.StudentID FROM INTRANET_NOTICE_REPLY as r INNER JOIN INTRANET_USER as u ON u.UserID=r.StudentID WHERE r.NoticeID='".$lnotice->NoticeID."' ORDER BY u.ClassName, u.ClassNumber+0";
		$result = $lnotice->returnArray($sql);
		$signed_username = array();
		$unsigned_users = array();
        foreach($result as $temp) {
            if($temp['RecordStatus'] == '' || $temp['RecordStatus'] == '0') {
				$unsigned_users[] = array($temp['StudentID'], $temp['name']);
				//$unsigned_users[] = array($temp['StudentID'].'1', $temp['name'].'2');
			} else {
				$signed_username[] = $temp['name'];
			}
		}

		$nextSelect .= $Lang['eNotice']['SignedNotices'].'<br/>';
		$nextSelect .= implode(', ', $signed_username);
		$nextSelect .= '<br/><br/>';

		$nextSelect .= $Lang['eNotice']['NotyetSignedNotices'] .'<br/>';
		$nextSelect .= '<table cellspacing="0" cellpadding="5" border="0">
                        <tbody>
                            <tr>
                                <td style="border-bottom: 0px none;" align="left">
                                    <select name="remove_target[]" id="remove_target" class="select_userList" size="5" multiple="multiple">';
		foreach($unsigned_users as $temp) {
			$nextSelect .= '<option value="'.$temp[0].'">'.$temp[1].'</option>';
		}

        $nextSelect .= '             </select>
                                     <div id="to_remove_target"></div>                                     
                                </td>
                                <td style="border-bottom: 0px none;" valign="top"><input type="button" class="formsmallbutton " onclick="js_AddSelectedStudent(\'remove_target\',\'to_remove_target\');" value="'.$Lang['Btn']['RemoveSelected'].'" onmouseover="this.className=\'formsmallbuttonon \'" onmouseout="this.className=\'formsmallbutton \'"> 
                                </td>
                            </tr>
                        </tbody>
                    </table>';

	} else {
		$nextSelect .= $recipientNames[1];
	}

	$nextSelect .= "</td></tr>\n";
}

$start = $lnotice->DateStart;
$start = date("Y-m-d H:i:s",strtotime($start));
$startDate = date("Y-m-d",strtotime($start));
$startDateHour = date("H",strtotime($start));
$startDateMin = date("i",strtotime($start));

$end = $lnotice->DateEnd;
$end = date("Y-m-d H:i:s",strtotime($end));
$endDate = date("Y-m-d",strtotime($end));
$endDateHour = date("H",strtotime($end));
$endDateMin = date("i",strtotime($end));
$Title = $lnotice->Title;
$NoticeNumber = $lnotice->NoticeNumber;
$Description = $lnotice->Description;
$Question = $lnotice->Question;

// $Question = str_replace("<br>","\r\n",$Question);
// $Question = intranet_htmlspecialchars($Question);
$Question = str_replace("<br>","\r\n",$Question);
$Question = intranet_htmlspecialchars($Question);

$status = $lnotice->RecordStatus;
$DebitMethod = $lnotice->DebitMethod;
$AllFieldsReq = $lnotice->AllFieldsReq;
$DisplayQuestionNumber = $lnotice->DisplayQuestionNumber;

// [2015-0323-1602-46073] - Get PICs of payment notice
$PICUser = $lnotice->returnNoticePICNames();

$changeable_to_template = true;
if(in_array($lnotice->RecordStatus,array(1,2))){
	$total_signed_data = $lnotice->returnNoticeSignedCount($NoticeID);
	$changeable_to_template = $total_signed_data[$NoticeID] == 0;
}

### Title ###
$TAGS_OBJ[] = array($Lang['eNotice']['PaymentNotice']);
$MODULE_OBJ = $lnotice->GET_MODULE_OBJ_ARR();

// 2015-6-3
$sql = "SELECT NotifyDateTime, SendTimeMode FROM INTRANET_APP_NOTIFY_MESSAGE_MODULE_RELATION as ianmmr
          INNER JOIN INTRANET_APP_NOTIFY_MESSAGE as ianm ON ianmmr.NotifyMessageID = ianm.NotifyMessageID
        WHERE ianmmr.ModuleName = 'eNotice' AND ianmmr.ModuleRecordID = '$NoticeID' AND ianm.RecordStatus = '1' ORDER BY ianm.NotifyMessageID DESC LIMIT 1";

$resultAry = $lnotice->returnResultSet($sql);
if (count($resultAry) > 0) {
	$sendTimeMode = $resultAry[0]["SendTimeMode"];
	$sendTimeString = $resultAry[0]["NotifyDateTime"];
}
else {
	$sendTimeMode = "";
	$sendTimeString = "";
}

$nowTime = strtotime("now");
if ($sendTimeMode == "scheduled" && $sendTimeString != "") {
	$sendTime = strtotime($sendTimeString);
	if ($sendTime > $nowTime) {
		$passScheduled = true;
		$scheduledBoxChecked = 'checked';
	}
	else {
		$sendTimeString = "";
		$passScheduled = false;
	}
}

// Add students
$is_target_type_individual_students = $lnotice->RecordType == 4;
if($is_target_type_individual_students)
{
	### Choose Member Btn
	$btn_ChooseMember = $linterface->GET_SMALL_BTN($Lang['Btn']['Select'], "button", "newWindow('/home/common_choose/index.php?fieldname=target[]&page_title=SelectAudience&permitted_type=2&DisplayGroupCategory=1&UserRecordStatus=0,1',16)");
	
	### Auto-complete ClassName ClassNumber StudentName search
	$UserClassNameClassNumberInput = '';
	$UserClassNameClassNumberInput .= '<div style="float:left;">';
		$UserClassNameClassNumberInput .= '<input type="text" id="UserClassNameClassNumberSearchTb" name="UserClassNameClassNumberSearchTb" value="" />';
	$UserClassNameClassNumberInput .= '</div>';
	
	### Auto-complete login search
	$UserLoginInput = '';
	$UserLoginInput .= '<div style="float:left;">';
		$UserLoginInput .= '<input type="text" id="UserLoginSearchTb" name="UserLoginSearchTb" value="" />';
	$UserLoginInput .= '</div>';
	
	### Student Selection Box & Remove all Btn
	$MemberSelectionBox = $linterface->GET_SELECTION_BOX($recipient_name_ary, "name='target[]' id='target[]' class='select_studentlist' size='15' multiple='multiple'", "");
	$btn_RemoveSelected = $linterface->GET_SMALL_BTN($Lang['Btn']['RemoveSelected'], "button", "js_Remove_Selected_Student();");
	
	$addTargetSelect = "<tr valign=\"top\">";
	$addTargetSelect .= "<td width=\"30%\" valign=\"top\" nowrap=\"nowrap\" class=\"field_title\"><span class=\"tabletext\">".$Lang['eNotice']['AddStudents']."</span></td>";
	$addTargetSelect .= "<td class=\"tabletext\">";
	$addTargetSelect .= "<table>";
	$addTargetSelect .= '<tr>
                            <td class="tablerow2" valign="top">
                                <div id="divSelectStudent">'.'</div>
                            </td>
						</tr>';
	$addTargetSelect .= "</table>";
	$addTargetSelect .= "</td>";
	$addTargetSelect .= "</tr>";
}

$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($i_Notice_Edit);

// debug_pr($sendTimeMode);
?>

<?php include_once ($PATH_WRT_ROOT . "home/common_choose/user_selection_plugin_js.php");?>
<script language="javascript">
function js_AddSelectedStudent(remove_target, to_remove_target) {
    var values = $('#'+remove_target).val();
    for(var i=0;i<values.length;i++) {
        var str = '<input type="hidden" name="to_remove_target[]" value="'+values[i]+'">';
        $("#to_remove_target").append(str);
    }
    if (document.getElementById(remove_target)) {
        checkOptionRemove(document.getElementById(remove_target));
    }
}

$(document).ready( function() {
	// 26-9-2016 Villa hide the setting div 
	<?php if($plugin['eClassApp']) { ?>
        <?php if(!$passScheduled) { ?>
			$('div#PushMessageSetting').hide();
			$('input#sendNewSelect').hide();
			$('label#sendNewSelect').hide();
		<?php } else { ?>
			$('div#PushMessageSetting').show();
			$('#sendTimeSettingDiv').show();
		<?php } ?>
		checkedSendPushMessage();
	<?php } ?>

	var divId ='divSelectPIC';
    var targetUserTypeAry = [1];
    var userSelectedFieldName = 'target_PIC[]';
    var userSelectedFieldId = 'target_PIC';
    var selectedUsersData = <?php echo $jsonObj->encode($PICUser)?>;
    Init_User_Selection_Menu(divId, targetUserTypeAry, userSelectedFieldName, userSelectedFieldId, selectedUsersData);

<?php if($is_target_type_individual_students){ ?>			
	var divId ='divSelectStudent';
	var targetUserTypeStudentAry = [2]; 
	var StudentSelectedFieldName = 'target[]'; 
	var StudentSelectedFieldId = 'target[]'; 
	var selectedUsersData = "";
    var extraParmData = "";
    <?php if($isAudienceLimited) { ?>
        extraParmData = ['filterClassTeacherClass', 'filterOwnClassesOnly'];
    <?php } ?>
	Init_User_Selection_Menu(divId, targetUserTypeStudentAry, StudentSelectedFieldName, StudentSelectedFieldId, selectedUsersData, '', extraParmData);
<?php } ?>

    $("[name^='MerchantAccountID']").each(function() {
        $(this).trigger("change");
    })

});

function Init_JQuery_AutoComplete(Input_Search_Field, Target_User_Type, Target_Select_Field_Id) {
    if(Target_User_Type == 3){
        var ajaxUrl="<?=$PATH_WRT_ROOT;?>home/common_choose/ajax_search_parent.php";
    }
    else if(Target_User_Type == 2){
        var ajaxUrl="../ajax_search_user_by_classname_classnumber.php";
    }
    else if(Target_User_Type == 1){
        var ajaxUrl="<?=$PATH_WRT_ROOT;?>home/common_choose/ajax_search_user.php";
    }

    var AutoCompleteObj = $("input#" + Input_Search_Field).autocomplete(
        ajaxUrl,
        {
            onItemSelect: function(li) {
                Add_Selected_User(li.extra[1], li.selectValue, Input_Search_Field, Target_Select_Field_Id);
            },
            formatItem: function(row) {
                return row[0];
            },
            maxItemsToShow: 100,
            minChars: 1,
            delay: 0,
            width: 200,

        }
    );

    if(Target_User_Type == 3){
        AutoCompleteObj_parent=AutoCompleteObj;
    }
    else if(Target_User_Type == 2){
        AutoCompleteObj_student = AutoCompleteObj;
    }
    else if(Target_User_Type == 1){
        AutoCompleteObj_staff = AutoCompleteObj;
    }
    Update_Auto_Complete_Extra_Para(Target_Select_Field_Id);
}

function compareDate(s1,s2)
{
    y1 = parseInt(s1.substring(0,4),10);
    y2 = parseInt(s2.substring(0,4),10);
    m1 = parseInt(s1.substring(5,7),10);
    m2 = parseInt(s2.substring(5,7),10);
    d1 = parseInt(s1.substring(8,10),10);
    d2 = parseInt(s2.substring(8,10),10);

    if (y1 > y2)
    {
        return 1;
    }
    else if (y1 < y2)
    {
        return -1;
    }
    else if (m1 > m2)
    {
        return 1;
    }
    else if (m1 < m2)
    {
        return -1;
    }
    else if (d1 > d2)
    {
        return 1;
    }
    else if (d1 < d2)
    {
        return -1;
    }
    return 0;
}

function checkform2(obj)
{
	$('#submit2').attr('disabled', true);
	var canSubmit = checkform(obj);
	if (canSubmit) {
		obj.submit();
	}
	else {
		$('#submit2').attr('disabled', false);
	}
}

function checkform(obj)
{
    checkOption(obj.elements['Attachment[]']);
    
    if(!check_text(obj.NoticeNumber, "<?php echo $i_alert_pleasefillin.$i_Notice_NoticeNumber; ?>.")) return false;
    if(!check_text(obj.Title, "<?php echo $i_alert_pleasefillin.$i_Notice_Title; ?>.")) return false;
    
    //if (!check_date(obj.DateStart,"<?php echo $i_invalid_date; ?>")) return false;
    if (!check_date(obj.DateEnd,"<?php echo $i_invalid_date; ?>")) return false;
	if (compareDate(obj.DateStart.value, obj.DateEnd.value) > 0) { alert ("<?php echo $i_Notice_Alert_DateInvalid; ?>"); return false; }
    
    if(obj.elements["target[]"] && obj.elements["target[]"].length != 0)
	{
	 	checkOptionAll(obj.elements["target[]"]);
	}
    
	if(obj.elements["target_PIC[]"].length != 0)
	{
	 	checkOptionAll(obj.elements["target_PIC[]"]);
	}
	
	// assign time into hidden field
 	var dateStartTime = str_pad($('#DateStart_hour').val(),2)+':'+str_pad($('#DateStart_min').val(),2)+':00';
 	$('#DateStartTime').val(dateStartTime);
 	var dateEndTime = str_pad($('#DateEnd_hour').val(),2)+':'+str_pad($('#DateEnd_min').val(),2)+':59';
 	$('#DateEndTime').val(dateEndTime);
	
	//check is type radio is checked
	var sus_status = $('input[name=sus_status]:checked').val();
	if(sus_status > 0){
		//do nothing
	}
	else{
		alert('Please select type of the notice');
		return false;
	}
	
	//check if the schedule day is in the past
	var scheduleString = '';
	if ($('input#sendTimeRadio_scheduled').attr('checked')) {
	    scheduleString = $('input#sendTime_date').val() + ' ' + str_pad($('select#sendTime_hour').val(),2) + ':' + str_pad($('select#sendTime_minute').val(),2) + ':00';
	}
	$('input#sendTimeString').val(scheduleString);
	 
	var dateNow = new Date();
	var dateNowString = dateNow.getFullYear() + '-' + str_pad((dateNow.getMonth()+1),2) + '-' + str_pad(dateNow.getDate(),2) + ' ' + str_pad(dateNow.getHours(),2) + ':' + str_pad(dateNow.getMinutes(),2) + ':' + str_pad(dateNow.getSeconds(),2);
    if($('#pushmessagenotify').attr('checked')){    	
		if ($('input#sendTimeRadio_scheduled').attr('checked') && scheduleString <= dateNowString) {
			document.getElementById('div_push_message_date_err_msg').innerHTML = '<font color="red"><?=$Lang['AppNotifyMessage']['WarningMsg']['SendTimeInvalid']?></font>';
			$('input#sendTime_date').focus();
			return false;
		}     
    } 
	//check if the schedule day is greater than 31days after the issue day 
	if($('#pushmessagenotify').attr('checked')){
		var maxDate = new Date();
		maxDate.setDate(dateNow.getDate() + 31);
		var maxDateString = maxDate.getFullYear() + '-' + str_pad((maxDate.getMonth()+1),2) + '-' + str_pad(maxDate.getDate(),2) + ' ' + str_pad(maxDate.getHours(),2) + ':' + str_pad(maxDate.getMinutes(),2) + ':' + str_pad(maxDate.getSeconds(),2);
		if ($('input#sendTimeRadio_scheduled').attr('checked')&& scheduleString > maxDateString) {
			$('input#sendTime_date').focus();
			document.getElementById('div_push_message_date_err_msg').innerHTML = '<font color="red"><?=$Lang['AppNotifyMessage']['WarningMsg']['PleaseSelectDateWithinRange']?></font>';
			return false;
		}
	}	
<?php if($sys_custom['ePayment']['MultiPaymentGateway']) { ?>

    var have_merchantid = false;
    $("[id^=MerchantAccountID_]").each(function(e) {
        if($(this).val() != '') {
            have_merchantid = true;
        }
    });

    if(have_merchantid == false) {
       if(confirm('<?=$Lang['ePayment']['SetupMerchant']?>') == false) {
           return false;
       }
    }
<?php }?>

    checkOptionAll(obj.elements["Attachment[]"]);
    
    obj.action = 'paymentNotice_edit_update.php';
    obj.method = 'POST';
    return true;
}

<?php if($plugin['eClassApp']){ ?>
	function checkedSendPushMessage(){
		if($('#pushmessagenotify').attr('checked') || $('#pushmessagenotify_Students').attr('checked') || $('#pushmessagenotify_PIC').attr('checked') || $('#pushmessagenotify_ClassTeachers').attr('checked')) {
			$('div#sendTimeSettingDiv').show();
			$('div#PushMessageSetting').show();
		}
		else{
			$('div#sendTimeSettingDiv').hide();
			$('div#PushMessageSetting').hide();
		}
	}
	
	function clickedSendTimeRadio(targetType) {
		if (targetType == 'now') {
			$('div#specificSendTimeDiv').hide();
		}
		else {
			$('div#specificSendTimeDiv').show();
		}
	}
<?php } ?>


function onMerchantChange(obj, sp) {
    var id = $(obj).val();
    $(".quota_"+sp).hide();
    $(".quota_id_"+id).show();
}

</script>

<br />

<form name="form1" method="get" action="">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>
<tr>
	<td colspan="2">
        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
                <td><br />
            <table align="center" width="100%" border="0" cellpadding="5" cellspacing="0" class="form_table_v30">
            <tr valign='top'>
                <td width='30%' valign='top' nowrap='nowrap' class="field_title"><span class='tabletext'><?=$i_Notice_RecipientType?></span></td>
                <td class='tabletext'><?=$type_selection?></td>
            </tr>

            <?=$nextSelect?>

            <?php if($is_target_type_individual_students){
                echo $addTargetSelect;
            }
            ?>

            <!-- Select PICs of eNotice -->
            <tr valign='top'>
                <td nowrap class="field_title"><?=$i_Profile_PersonInCharge?></td>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class='tablerow2' valign='top'>
                                <div id='divSelectPIC'></div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr valign='top'>
                <td width='30%' valign='top' nowrap='nowrap' class="field_title"><span class='tabletext'><?=$i_Notice_NoticeNumber?></span> <span class='tabletextrequire'>*</span></td>
                <td class='tabletext'><input type='text' name='NoticeNumber' maxlength='50' value='<?=$NoticeNumber?>' class='textboxnum'></td>
            </tr>

            <tr valign='top'>
                <td width='30%' valign='top' nowrap='nowrap' class="field_title"><span class='tabletext'><?=$i_Notice_Title?></span> <span class='tabletextrequire'>*</span></td>
                <td class='tabletext'><input type='text' name='Title' maxlength='255' value='<?=$Title?>' class='textboxtext'></td>
            </tr>

            <tr valign='top'>
                <td width='30%' valign='top' nowrap='nowrap' class="field_title"'><span class='tabletext'><?=$i_Notice_DateStart?></span> <span class='tabletextrequire'>*</span></td>
                <td class='tabletext'>
                    <?
                    if($lnotice->RecordStatus == 1) {
                        echo $start."<input type='hidden' name=DateStart id=DateStart value=\"$startDate\">";
                    }
                    else {
                        echo $linterface->GET_DATE_PICKER("DateStart",$startDate).$linterface->Get_Time_Selection_Box('DateStart_hour', 'hour', $startDateHour, $others_tab='').':'.$linterface->Get_Time_Selection_Box('DateStart_min', 'min', $startDateMin, $others_tab='')."
                            ".$linterface->GET_HIDDEN_INPUT('DateStartTime', 'DateStartTime', $startDateHour.':'.$startDateMin.':00');
                    }
                    ?>
                </td>
            </tr>

            <tr valign='top'>
                <td width='30%' valign='top' nowrap='nowrap' class="field_title"><span class='tabletext'><?=$i_Notice_DateEnd?></span> <span class='tabletextrequire'>*</span></td>
                <td class='tabletext'><?=$linterface->GET_DATE_PICKER("DateEnd",$endDate).$linterface->Get_Time_Selection_Box('DateEnd_hour', 'hour', $endDateHour, $others_tab='').':'.$linterface->Get_Time_Selection_Box('DateEnd_min', 'min', $endDateMin, $others_tab='')."
                    ".$linterface->GET_HIDDEN_INPUT('DateEndTime', 'DateEndTime', $endDateHour.':'.$endDateMin.':00')?></td>
            </tr>

            <tr valign='top'>
                <td width='30%' valign='top' nowrap='nowrap'class="field_title"><span class='tabletext'><?=$i_Notice_Description?></span></td>
                <td class='tabletext'>
                    <?
                    if($lnotice->RecordStatus == 1) {
                        // [2019-0118-1125-41206] display text content from FCKEditor
                        if(strtoupper($lnotice->Module) == "PAYMENT" && $lnotice->isPaymentNoticeApplyEditor) {
                            echo htmlspecialchars_decode($Description);
                        }
                        else {
                            echo nl2br($Description);
                        }
                    }
                    else {
                        // [2019-0118-1125-41206] handle previous description content
                        if(strtoupper($lnotice->Module) == "PAYMENT" && !$lnotice->isPaymentNoticeApplyEditor) {
                            $Description = nl2br($Description);
                        }

                        // [2019-0118-1125-41206] apply FCKEditor for text input
                        include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
                        $objHtmlEditor = new FCKeditor("Description", "100%", "320", "", "Basic2_withInsertImageFlash", htmlspecialchars_decode($Description), $is_scrollable=true);
                        $objHtmlEditor->Config['FlashImageInsertPath'] = $li->returnFlashImageInsertPath($cfg['fck_image']['eNotice'], $id);

                        echo $objHtmlEditor->CreateHtml();
                    }
                    ?>
                </td>
            </tr>

            <tr>
                <td width='30%' valign='top' nowrap='nowrap' class="field_title"><span class='tabletext'><?=$i_Notice_Attachment?></span></td>
                <td>
                    <table border="0" cellspacing="1" cellpadding="1">
                    <tr>
                        <td>
                            <select name="Attachment[]" size="4" multiple><option><?php for($i = 0; $i < 40; $i++) echo "&nbsp;"; ?></option></select>
                        </td>
                        <td>
                            <?
                            if($lnotice->RecordStatus != 1) {
                                echo $linterface->GET_BTN($i_frontpage_campusmail_attach, "button","newWindow('attach.php?folder=$noticeAttFolder',2)")."<br>".
                                        $linterface->GET_BTN($i_frontpage_campusmail_remove, "button","checkOptionRemove(document.form1.elements['Attachment[]'])")."<br>";
                            }
                            ?>
                        </td>
                    </tr>
                    </table>
                </td>
            </tr>

            <tr valign='top'>
                <td width='30%' valign='top' nowrap='nowrap' class="field_title"><span class='tabletext'><?=$i_Notice_ReplySlip?></span></td>
                <td class='tabletext'>
                    <?php if($lnotice->RecordStatus != 1) {
                        $arrSignedCount = $lnotice->returnNoticeSignedCount($NoticeID);
// 						$SignedCount = $arrSignedCount[$NoticeID][0];
                        $SignedCount = $arrSignedCount[$NoticeID];
                        if($SignedCount == 0 || $SignedCount == "") {
                            echo $linterface->GET_BTN($button_edit, "button","newWindow('paymentNotice_editform.php',1)");
                        }
                    }
                    ?>
                    <?=$linterface->GET_BTN($button_preview, "button","newWindow('paymentNotice_preview.php?preview_with_subsidy=1',10)");?>
                    <?php if($lnotice->RecordStatus != 1 && (($SignedCount == 0 || $SignedCount == ""))) { ?>
                        <br><input name="AllFieldsReq" type="checkbox" value="1" id="AllFieldsReq" <?=($AllFieldsReq ? "checked":"")?> > <label for="AllFieldsReq"><?=$i_Survey_AllRequire2Fill?></label>
                        <br><span class="tabletextremark">(<?=$i_ResourceRemark?>: <?=$Lang['eNotice']['PaymentNotice_ReminderAfterEditMustSubmit']?>)</span>
                    <?php } else { ?>
                        <br><input type="checkbox" value="1" <?=($AllFieldsReq ? "checked" : "")?> disabled > <label for="AllFieldsReq"><?=$i_Survey_AllRequire2Fill?></label>
                        <input name="AllFieldsReq" type="hidden" value="<?=($AllFieldsReq ? "1": "0")?>" id="AllFieldsReq" >
                    <?php } ?>
                    <br><input name='DisplayQuestionNumber' type='checkbox' value='1' id='DisplayQuestionNumber' <?=($DisplayQuestionNumber ? "checked":"")?> > <label for='DisplayQuestionNumber'><?=$Lang['eNotice']['DisplayQuestionNumber']?></label>

                    <?php if($lnotice->RecordStatus != 1 && !$changeable_to_template && ($SignedCount == 0 || $SignedCount == "")) {
                        echo "<br/><font color='red'>".$Lang['eNotice']['ChangePaymentReplyRemark']."</font>";
                    }
                    ?>
                </td>
            </tr>

            <!--
            <tr>
                <td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_Notice_Type?></span></td>
                <td class='tabletext'>
                    <input type="radio" name="sus_status" value="1" id="status1" <?=($lnotice->RecordStatus==1? "CHECKED":"")?>><label for='status1'><?=$i_Notice_StatusPublished?></label><br />
                    <input type="radio" name="sus_status" value="2" id="status2" <?=($lnotice->RecordStatus==2? "CHECKED":"")?>><label for='status2'><?=$i_Notice_StatusSuspended?></label><br />
                    <input type="radio" name="sus_status" value="3" id="status3" <?=($lnotice->RecordStatus==3? "CHECKED":"")?>><label for='status3'><?=$i_Notice_StatusTemplate?></label><br />
                    <?=$i_Notice_TemplateNotes?>
                </td>
            </tr>

            <tr valign='top'>
                <td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>&nbsp;</span></td>
                <td class='tabletext'>
                    <input type='checkbox' name='emailnotify' value='1' id='emailnotify'> <label for='emailnotify'><?=$i_Notice_SendEmailToParent?></label><br>
                    <input type='checkbox' name='emailnotify_Students' value='1' id='emailnotify_Students'> <label for='emailnotify_Students'><?=$Lang['eNotice']['SendEmailNotificationToStudent'];?></label>
                </td>
            </tr>
            -->

            <tr valign='top'>
                <td width='30%' valign='top' nowrap='nowrap' class="field_title"><?=$Lang['eNotice']['Options']?></td>
                <td>
                    <table border='0' cellpadding='1' cellspacing='1' width='100%'>
                        <tr>
                            <td>
                                <?php
                                // [2020-0604-1821-16170]
                                //if($lnotice->needApproval){
                                if($lnotice->payment_needApproval)
                                {
                                    // [2020-0604-1821-16170]
                                    // if UserID in Array -> this user is in Approval Group
                                    //if($lnotice->hasApprovalRight()){
                                    if($lnotice->hasPaymentNoticeApprovalRight()){
                                        $needApprove = false;
                                    }
                                    else{
                                        if($lnotice->RecordStatus == 1){
                                            $needApprove = false;
                                        }
                                        else{
                                            $needApprove = true;
                                        }
                                    }
                                }

                                // Get temp saved email pushmsg choices
                                if($NoticeID > 0){
                                    if($lnotice->RecordStatus == 4){
                                        // Get temp saved email and push msg choices
                                        // $sql = "SELECT pushmessagenotify, pushmessagenotifyMode, pushmessagenotifyTime, emailnotify, emailnotify_Students FROM INTRANET_NOTICE WHERE NoticeID = '$NoticeID'";
                                        // $emailPushMsgChoicesArray = $this->returnResultSet($sql);
                                        // $emailPushMsgChoicesArray = $emailPushMsgChoicesArray[0];
                                        $emailPushMsgChoicesArray = array(
                                                'pushmessagenotify'=>$lnotice->pushmessagenotify,
                                                'pushmessagenotify_Students'=>$lnotice->pushmessagenotify_Students,
                                                'pushmessagenotify_PIC'=>$lnotice->pushmessagenotify_PIC,
                                                'pushmessagenotify_ClassTeachers'=>$lnotice->pushmessagenotify_ClassTeachers,
                                                'pushmessagenotifyMode'=>$lnotice->pushmessagenotifyMode,
                                                'pushmessagenotifyTime'=>$lnotice->pushmessagenotifyTime,
                                                'emailnotify'=>$lnotice->emailnotify,
                                                'emailnotify_Students'=>$lnotice->emailnotify_Students,
                                                'emailnotify_PIC'=>$lnotice->emailnotify_PIC,
                                                'emailnotify_ClassTeachers'=>$lnotice->emailnotify_ClassTeachers
                                        );
                                    }
                                }

                                ######################
                                #	Assign back temp saved choices
                                ######################
                                $pushmessagenotifyChecked ='';
                                $scheduledChecked = true;
                                #check if there is a schedule will be sending later	26-9-2016 Villa

                                if($lnotice->RecordStatus == 4){
                                    if($emailPushMsgChoicesArray['pushmessagenotify'] || $emailPushMsgChoicesArray['pushmessagenotify_Students'] || $emailPushMsgChoicesArray['pushmessagenotify_PIC'] || $emailPushMsgChoicesArray['pushmessagenotify_ClassTeachers']){
// 											$pushmessagenotifyChecked = "checked";
                                        if($emailPushMsgChoicesArray['pushmessagenotify']){
                                            $scheduledMessageChecked = "checked";
                                        }
                                        if($emailPushMsgChoicesArray['pushmessagenotify_Students']){
                                            $scheduledStudentsMessageChecked = "checked";
                                        }
                                        if($emailPushMsgChoicesArray['pushmessagenotify_PIC']){
                                            $scheduledPICMessageChecked = "checked";
                                        }
                                        if($emailPushMsgChoicesArray['pushmessagenotify_ClassTeachers']){
                                            $scheduledClassTeachersMessageChecked = "checked";
                                        }
                                        $scheduledMessageChecked = "checked";
                                        if($emailPushMsgChoicesArray['pushmessagenotifyMode'] == 'now'){
                                            $pushmessagenotifyChecked = "checked";
                                            $nowChecked = true;
                                            $scheduledChecked = false;
                                        }
                                        else if($emailPushMsgChoicesArray['pushmessagenotifyMode'] == 'scheduled'){
                                            $scheduledChecked = true;
                                            $nowChecked = false;
                                            $sendTimeString = $emailPushMsgChoicesArray['pushmessagenotifyTime'];
                                            $sendTime = strtotime($sendTimeString);
                                        }
                                    }
                                    if($emailPushMsgChoicesArray['emailnotify']){
                                        $emailNotifyParentChecked = "checked";
                                    }
                                    if($emailPushMsgChoicesArray['emailnotify_Students']){
                                        $emailNotifyStudentChecked = "checked";
                                    }
                                    if($emailPushMsgChoicesArray['emailnotify_PIC']){
                                        $emailNotifyPICChecked = "checked";
                                    }
                                    if($emailPushMsgChoicesArray['emailnotify_ClassTeachers']){
                                        $emailNotifyClassTeachersChecked = "checked";
                                    }
                                }
                                ?>
                                <?php $htmlAry['sendTimeNowRadio'] = $linterface->Get_Radio_Button('sendTimeRadio_now', 'sendTimeMode', 'now', $nowChecked, $Class="", $Lang['AppNotifyMessage']['Now'], "clickedSendTimeRadio('now');" );
                                $htmlAry['sendTimeScheduledRadio'] = $linterface->Get_Radio_Button('sendTimeRadio_scheduled', 'sendTimeMode', 'scheduled', $scheduledChecked, $Class="", $Lang['AppNotifyMessage']['SpecificTime'], "clickedSendTimeRadio('scheduled');" );

                                //2016-9-26 Villa get back the schedule time
                                $rounded_seconds = ceil(time() / (15 * 60)) * (15 * 60);
                                $nextTimeslotHour = date('H', $rounded_seconds);
                                $nextTimeslotMinute = date('i', $rounded_seconds);
                                $nowTime = time();
                                $sendTime = strtotime($sendTimeString);
                                if ($sendTimeString == "") {
                                    $selectionBoxDate = date('Y-m-d', $nowTime);
                                    $selectionBoxHour = $nextTimeslotHour;
                                    $selectionBoxMinute = $nextTimeslotMinute;
                                }
                                else if($passScheduled == true) {
                                    $selectionBoxDate = date('Y-m-d', $sendTime);
                                    $selectionBoxHour = date('H', $sendTime);
                                    $selectionBoxMinute = date('i', $sendTime);
                                }

                                $y = '';
                                $y .= "&nbsp;&nbsp;";
                                $y .= $linterface->GET_DATE_PICKER('sendTime_date', $selectionBoxDate);
                                $y .= $linterface->Get_Time_Selection_Box('sendTime_hour', 'hour', $selectionBoxHour);
                                $y .= " : ";
                                $y .= $linterface->Get_Time_Selection_Box('sendTime_minute', 'min', $selectionBoxMinute, $others_tab='', $interval=15);
                                $y .= "&nbsp;&nbsp;";
                                $y .= $linterface->GET_SMALL_BTN($Lang['AppNotifyMessage']['SyncWithIssueDate'],'button','javascript:syncIssueTimeToPushMsg()');
                                $y .= "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id='div_push_message_date_err_msg'></span>";
                                $y .= "<input type='hidden' id='sendTimeString' name='sendTimeString' value='' />";
                                $htmlAry['sendTimeDateTimeDisplay'] = $y;
                                ?>

                                <input type='radio' name='sus_status' value='<?= (($needApprove)?4:1) ?>' id='status1' <?echo ($lnotice->RecordStatus==1||$lnotice->RecordStatus==4 ||($lnotice->RecordStatus==5&&!$lnotice->hasPaymentNoticeApprovalRight())? "checked":"")?> onClick='js_show_email_notification(this.value);'><label for='status1'><?=(($needApprove)?$Lang['eNotice']['NeedApproval']:$Lang['eNotice']['ApproveAndIssue'])?></label><br />
                                <?php if($plugin['eClassApp']){ ?>
                                    <span style='margin-left:20px; <?=($plugin['eClassApp'] ? "" : "display: none;")?>'>
                                        <input type='checkbox' name='pushmessagenotify' value='1' id='pushmessagenotify' <?=$scheduledMessageChecked?> onclick='checkedSendPushMessage();'> <label for='pushmessagenotify'><?=$Lang['eNotice']['NotifyParentsByPushMessage']?></label><br>
                                    </span>
                                    <span style='margin-left:20px;'>
                                        <input type='checkbox' name='pushmessagenotify_Students' value='1' id='pushmessagenotify_Students'  onclick='checkedSendPushMessage();' <?=$scheduledStudentsMessageChecked?>> <label for='pushmessagenotify_Students'><?=$Lang['eNotice']['NotifyStudentsByPushMessage']?></label><br/>
                                    </span>
                                    <span style='margin-left:20px;'>
                                        <input type='checkbox' name='pushmessagenotify_PIC' value='1' id='pushmessagenotify_PIC'  onclick='checkedSendPushMessage();' <?=$scheduledPICMessageChecked?>> <label for='pushmessagenotify_PIC'><?=$Lang['eNotice']['NotifyPICByPushMessage']?></label><br/>
                                    </span>
                                    <span style='margin-left:20px;'>
                                        <input type='checkbox' name='pushmessagenotify_ClassTeachers' value='1' id='pushmessagenotify_ClassTeachers'  onclick='checkedSendPushMessage();' <?=$scheduledClassTeachersMessageChecked?>> <label for='pushmessagenotify_ClassTeachers'><?=$Lang['eNotice']['NotifyClassTeachersByPushMessage']?></label><br/>
                                    </span>

                                    <div id='PushMessageSetting'><?= $linterface->Get_Warning_Message_Box($Lang['eClassApps']['PushMessage']." ".$Lang['ePost']['Setting'],
                                                "<div id='sendTimeSettingDiv'>
                                                &nbsp;&nbsp;&nbsp;&nbsp;    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$htmlAry['sendTimeNowRadio']."
                                                            <br />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$htmlAry['sendTimeScheduledRadio']."
                                                            <br />
                                                            <div id='specificSendTimeDiv' style='".($scheduledChecked ? "" : "display: none;")."'>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$htmlAry['sendTimeDateTimeDisplay']."
                                                            </div>
                                                        </div></div>")?>
                                <hr />
                                <?php } ?>

                                <span style='margin-left:20px;'>
                                    <input type='checkbox' name='emailnotify' value='1' id='emailnotify' <?echo ($lnotice->RecordStatus==1||$lnotice->RecordStatus==4 ? "":"disabled")?> <?=$emailNotifyParentChecked ?> > <label for='emailnotify'><?=$Lang['eNotice']['NotifyParentsByEmail']?></label><br>
                                </span>
                                <span style='margin-left:20px;'>
                                    <input type='checkbox' name='emailnotify_Students' value='1' id='emailnotify_Students' <?echo ($lnotice->RecordStatus==1||$lnotice->RecordStatus==4 ? "":"disabled")?>  <?= $emailNotifyStudentChecked?>> <label for='emailnotify_Students'><?=$Lang['eNotice']['NotifyStudentsByEmail']?></label><br/>
                                </span>
                                <span style='margin-left:20px;'>
                                    <input type='checkbox' name='emailnotify_PIC' value='1' id='emailnotify_PIC' <?echo ($lnotice->RecordStatus==1||$lnotice->RecordStatus==4 ? "":"disabled")?>  <?= $emailNotifyPICChecked?>> <label for='emailnotify_PIC'><?=$Lang['eNotice']['NotifyPICByEmail']?></label><br/>
                                </span>
                                <span style='margin-left:20px;'>
                                    <input type='checkbox' name='emailnotify_ClassTeachers' value='1' id='emailnotify_ClassTeachers' <?echo ($lnotice->RecordStatus==1||$lnotice->RecordStatus==4 ? "":"disabled")?>  <?= $emailNotifyClassTeachersChecked?>> <label for='emailnotify_ClassTeachers'><?=$Lang['eNotice']['NotifyClassTeachersByEmail']?></label><br/>
                                </span>
                                <br><span style='margin-left:20px;'>(<?=$Lang['eNotice']['NotifyEmailRemark']?>)</span>
                            </td>
                        </tr>

                        <? if(!($lnotice->payment_needApproval && !$needApprove && $lnotice->RecordStatus>=4)){ ?>
                            <tr>
                                <td>
                                    <input type='radio' name='sus_status' value='2' id='status2' <?echo ($lnotice->RecordStatus==2 ? "checked":"")?> onClick='js_show_email_notification(this.value);'><label for='status2'><?=$Lang['eNotice']['NotToBeDistributed']?></label><br />
                                </td>
                            </tr>
                            <? if($changeable_to_template && $lnotice->RecordStatus == 3){ ?>
                                <tr>
                                    <td>
                                        <input type='radio' name='sus_status' value='3' id='status3' <?echo ($lnotice->RecordStatus==3 ? "checked":"")?> onClick='js_show_email_notification(this.value);'><label for='status3'><?=$i_Notice_StatusTemplate?></label><br /><?=$i_Notice_TemplateNotes?><br />
                                    </td>
                                </tr>
                            <? } ?>
                        <? } ?>

                        <? if($lnotice->payment_needApproval && !$needApprove && $lnotice->RecordStatus >= 4){
                            $x .= "<tr><td>";
                            $x .= "<p><input type='radio' name='sus_status' value='5' id='status5'  ". ($lnotice->RecordStatus==5 ? "checked":"") ." onClick='js_show_email_notification(this.value);'><label for='status5'>". $Lang['eNotice']['Reject']."</label> <br />";

                            //Need Approval is "ON" in System Settings && 'I' can Approve eNotice (have e-Notice Approval Right)
                            $x .= '<p>
                                <label for="approvalComment">'.$Lang['eNotice']['RejectedComment'].': </label><br>
                                <textarea rows="4" cols="50" id="approvalComment" name="approvalComment" >'.$lnotice->ApprovalComment.'</textarea>';
                            $x .= "</td></tr>";
                        } ?>
                        <?= $x ?>
                    </table>
                </td>
            </tr>

            <tr valign='top'<?=($use_direct_pay?" style=\"display:none;\" ":"")?>>
                <td width='30%' valign='top' nowrap='nowrap' class="field_title"><span class='tabletext'><?=$eEnrollment['debit_method']?></span></td>
                <td class='tabletext'>
                    <input type='radio' name='debit_method' value='1' id='debit_method1' <? if($DebitMethod==1) echo " checked";?> ><label for='debit_method1'><?=$eEnrollment['debit_directly']?></label>&nbsp;
                    <input type='radio' name='debit_method' value='2' id='debit_method2' <? if($DebitMethod==2) echo " checked";?> ><label for='debit_method2'><?=$eEnrollment['debit_later']?></label>&nbsp;
                </td>
            </tr>

            <?php

            if ($use_merchant_account) {
                if($sys_custom['ePayment']['MultiPaymentGateway']) {
                    $payment_merchant_account_ids = $lpayment->returnPaymentNoticeMultiMerchantAccountID($NoticeID);


                    $x = '<tr valign="top">
                                <td width="30%" valign="top" nowrap="nowrap" class="field_title">
                                    <span class="tabletext">' . $Lang['ePayment']['ServiceProviderAndSchoolAccount'] . '</span>
                                </td>
                                <td class="tabletext" width="70%">
                                <span id="div_merchant_err_msg"></span>
                                <table>';


                    $payment_merchant_account_ids = array_unique($payment_merchant_account_ids);
                    $service_provider_list = $lpayment->getPaymentServiceProviderMapping(false, true);
                    foreach ($service_provider_list as $k => $temp) {
                        $merchants = $lpayment->getMerchantAccounts(array('ServiceProvider' => $k, 'RecordStatus' => 1, 'order_by_accountname' => true));
                        $merchant_selection_data = array();
                        $selected_merchant_id = '';
                        $selected_merchant_account_name = '';
                        for ($i = 0; $i < count($merchants); $i++) {
                            $merchant_selection_data[] = array($merchants[$i]['AccountID'], $merchants[$i]['AccountName']);
                            if (in_array($merchants[$i]['AccountID'], $payment_merchant_account_ids)) {
                                $selected_merchant_id = $merchants[$i]['AccountID'];
                                $selected_merchant_account_name = $merchants[$i]['AccountName'];
                            }
                        }
                        $x .= '<tr>';
                        $x .= '<td>' . $temp . '</td>';
                        $x .= '<td>';
						if ($selected_merchant_account_name == '') {
							$selected_merchant_account_name = $Lang['General']['EmptySymbol'];
						}
                        if ($changeable_to_template) {
                            $x .= $linterface->GET_SELECTION_BOX($merchant_selection_data, ' id="MerchantAccountID_' . $k . '" name="MerchantAccountID_' . $k . '" onchange="onMerchantChange(this,\''.$k.'\')"', $Lang['General']['EmptySymbol'], $selected_merchant_id);
                        } else {
                            $x .= $selected_merchant_account_name . '<input type="hidden" id="MerchantAccountID_' . $k . '" name="MerchantAccountID_' . $k . '" value="' . $selected_merchant_id . '" onchange="onMerchantChange(this,\''.$k.'\')"/>';
                        }

						foreach($merchant_selection_data as $merchant_data) {
							$year = date('Y');
							$month = date('m');
							$sql = "SELECT * FROM PAYMENT_MERCHANT_ACCOUNT_QUOTA WHERE Year='$year' AND Month='$month' AND AccountID='".$merchant_data[0]."'";
							$rs_quota = $lpayment->returnArray($sql);
							if(count($rs_quota)>0) {
								$remain_quota = $rs_quota[0]['Quota'] - $rs_quota[0]['UsedQuota'];
								if($remain_quota < 0) {
									$remain_quota = 0;
                        }
								$quota_str = str_replace('{COUNT}', $remain_quota, $Lang['ePayment']['CurrentRemainQuota']);
								$x .= ' <span class="quota_' . $k . ' quota_id_' . $merchant_data[0] . '" style="display: none;">(' . $quota_str . ')</span>';
							}
						}

                        $x .= '</td>';
                        $x .= '</tr>';
                    }

                    $x .= '		</table>
                                </td>
                            </tr>';
                    echo $x;
                } else {
                    $sql = "SELECT * FROM INTRANET_NOTICE WHERE NoticeID = '$NoticeID'";
                    $notice_record = $lnotice->returnResultSet($sql);
                    $selected_merchant_account_id = $notice_record[0]['MerchantAccountID'];

                    $selected_merchant_account_name = '';
                    $merchants = $lpayment->getMerchantAccounts(array('RecordStatus' => 1, 'order_by_accountname' => true));
                    $merchant_selection_data = array();
                    for ($i = 0; $i < count($merchants); $i++) {
                        $merchant_selection_data[] = array($merchants[$i]['AccountID'], $merchants[$i]['AccountName']);
                        if ($selected_merchant_account_id != '' && $selected_merchant_account_id == $merchants[$i]['AccountID']) {
                            $selected_merchant_account_name = $merchants[$i]['AccountName'];
                        }
                    }
                    if ($selected_merchant_account_name == '') {
                        $selected_merchant_account_name = $Lang['General']['EmptySymbol'];
                    }
                    $merchant_account_selection = $linterface->GET_SELECTION_BOX($merchant_selection_data, ' id="MerchantAccountID" name="MerchantAccountID" ', $Lang['General']['EmptySymbol'], $selected_merchant_account_id);
                    if (!$changeable_to_template) {
                        $merchant_account_selection = $selected_merchant_account_name . '<input type="hidden" id="MerchantAccountID" name="MerchantAccountID" value="' . $selected_merchant_account_id . '" />';
                    }

                    $x = '<tr valign="top">
                            <td width="30%" valign="top" nowrap="nowrap" class="field_title">
                                <span class="tabletext">' . $Lang['ePayment']['ServiceProviderAndSchoolAccount'] . '</span>
                            </td>
                            <td class="tabletext" width="70%">
                                ' . $merchant_account_selection . '
                            </td>
                        </tr>';
                    echo $x;
                }
            }

				?>
				</table>
			</td>
            </tr>
        </table>
	</td>
</tr>

<tr>
	<td colspan="2">
        <table width="93%" border="0" cellspacing="0" cellpadding="5" align="center">
            <tr>
                <td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
            </tr>
            <tr>
                <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
            </tr>
            <tr>
                <td align="center">
                    <?//= $linterface->GET_ACTION_BTN("$button_save / $button_send", "submit", "return checkform(this.form);","submit2") ?>
                    <?= $linterface->GET_ACTION_BTN("$button_save / $button_send", "button", "checkform2(this.form);", "submit2") ?>
                    <?= $linterface->GET_ACTION_BTN($button_reset, "reset") ?>
                    <?php if ($copied == 1) { ?>
                        <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='paymentNotice.php';","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
                    <?php } else { ?>
                        <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back();","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
                    <?php } ?>
                </td>
            </tr>
        </table>
	</td>
</tr>

</table>                        
<br />

<input type="hidden" name="qStr" value="<?=$Question?>">
<input type="hidden" name="aStr" value="">
<input type="hidden" name="NoticeID" value="<?=$NoticeID?>">
<input type="hidden" name="noticeAttFolder" value="<?=$noticeAttFolder?>" />

<input type="hidden" name="pageNo" value="<?=$pageNo?>" />
<input type="hidden" name="field" value="<?=$field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$numPerPage?>" />

<input type="hidden" name="noticeType" value="<?=$noticeType?>" />
<input type="hidden" name="status" value="<?=$status?>" />
<input type="hidden" name="year" value="<?=$year?>" />
<input type="hidden" name="month" value="<?=$month?>" />
<input type="hidden" name="backUrl" value="paymentNotice" />

<input type="hidden" name="CurrentItemID" value="<?=implode(",",(array)$arrCurrentItemID);?>">

</form>

<script language="JavaScript1.2">
obj = document.form1.elements["Attachment[]"];
checkOptionClear(obj);
<?php while (list($key, $value) = each($files)) echo "checkOptionAdd(obj, \"".$files[$key][0]." (".ceil($files[$key][1]/1000)."Kb".")\", \"".$files[$key][0]."\");\n";  ?>
checkOptionAdd(obj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");

function js_show_email_notification(val)
{
	if(val == 1 || val == 4)
	{
		<?php if($plugin['eClassApp']){ ?>
			$('#pushmessagenotify').removeAttr('disabled');
            $('#pushmessagenotify_Students').removeAttr('disabled');
            $('#pushmessagenotify_PIC').removeAttr('disabled');
            $('#pushmessagenotify_ClassTeachers').removeAttr('disabled');
		<?php }?>
		$('#emailnotify').removeAttr('disabled');
		$('#emailnotify_Students').removeAttr('disabled');
        $('#emailnotify_PIC').removeAttr('disabled');
        $('#emailnotify_ClassTeachers').removeAttr('disabled');
	}
	else
    {
		<?php if($plugin['eClassApp']){ ?>
			$('#pushmessagenotify').removeAttr('checked');
            $('#pushmessagenotify_Students').removeAttr('checked');
            $('#pushmessagenotify_PIC').removeAttr('checked');
            $('#pushmessagenotify_ClassTeachers').removeAttr('checked');
			$('#pushmessagenotify').attr('disabled','true');
            $('#pushmessagenotify_Students').attr('disabled','true');
            $('#pushmessagenotify_PIC').attr('disabled','true');
            $('#pushmessagenotify_ClassTeachers').attr('disabled','true');
		<?php } ?>
		$('#emailnotify').removeAttr('checked');
		$('#emailnotify_Students').removeAttr('checked');
        $('#emailnotify_PIC').removeAttr('checked');
        $('#emailnotify_ClassTeachers').removeAttr('checked');
		$('#emailnotify').attr('disabled','true');
		$('#emailnotify_Students').attr('disabled','true');
        $('#emailnotify_PIC').attr('disabled','true');
        $('#emailnotify_ClassTeachers').attr('disabled','true');

        $('div#sendTimeSettingDiv').hide();
        $('div#PushMessageSetting').hide();
	}
}

function syncIssueTimeToPushMsg(){
	var issueDate = $('#DateStart').val();
	var issueHour = '<?= $startDateHour?>';
	var issueMin = '<?= $startDateMin?>';
			
	if(issueMin == 0){
		issueMin = 00;
	}
	else if(issueMin <= 15){
		issueMin = 15;
	}
	else if(issueMin <= 30){
		issueMin = 30;
	}
	else if(issueMin <= 45){
		issueMin = 45;
	}
	else{
		issueMin = 00;
		issueHour++
	}
	
	$('#sendTime_date').val(issueDate);;
	$('#sendTime_hour').val(issueHour);
	$('#sendTime_minute').val(issueMin);
}
</script>

<?php
intranet_closedb();

print $linterface->FOCUS_ON_LOAD("form1.NoticeNumber");
$linterface->LAYOUT_STOP();
?>