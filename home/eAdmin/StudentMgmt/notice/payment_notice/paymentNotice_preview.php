<?php
//using : 

######################
#	Date:	2019-05-15  Carlos
#			Added preview by subsidy options. 
#
#	Date:	2019-04-08  Carlos
#			Improved reply slip with new php library eNoticePaymentReplySlip.php
#
#	Date:	2011-02-16	YatWoon
#			Add option "Display question number"
#
######################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/eNoticePaymentReplySlip.php");

intranet_auth();
intranet_opendb();

$lform = new libform();
$lpayment = new libpayment();
$eNoticePaymentReplySlip = new eNoticePaymentReplySlip();

$preview_with_subsidy = $_GET['preview_with_subsidy'] == 1;

$MODULE_OBJ['title'] = $i_Notice_ReplySlip;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();
?>
<script type="text/javascript" language="javascript" src="/templates/forms/eNoticePayment_replyslip.js?t=<?=time()?>"></script>
<script type="text/javascript" language="javascript" src="/templates/jquery/jquery.tablednd_0_5.js"></script>
<br />
<p>
<?php 
if($preview_with_subsidy){
	$subsidy_identity_records = $lpayment->getSubsidyIdentity(array());
	$subsidy_identity_data = array();
	$subsidy_identity_data[] = array('',$Lang['General']['EmptySymbol']);
	for($i=0;$i<count($subsidy_identity_records);$i++){
		$subsidy_identity_data[] = array($subsidy_identity_records[$i]['IdentityID'],$subsidy_identity_records[$i]['IdentityName']);
	}
	$subsidy_identity_selection = $linterface->GET_SELECTION_BOX($subsidy_identity_data, ' id="SubsidyIdentityID" name="SubsidyIdentityID" ', $ParDefault='', $ParSelected="", $CheckType=false);
	
	$subsidy_type_not_apply_rb = $linterface->Get_Radio_Button("SubsidyType0", "SubsidyType", "0", $isChecked=1, $Class=" subsidy-type ", $Display=$Lang['General']['No'], $Onclick="",$isDisabled=0,$isIndented=0,$specialLabel="");
	$subsidy_type_by_identity_rb = $linterface->Get_Radio_Button("SubsidyType1", "SubsidyType", "1", $isChecked=0, $Class=" subsidy-type ", $Lang['eNotice']['PreviewByIdentity'], $Onclick="",$isDisabled=0,$isIndented=0,$specialLabel="");
	$subsidy_type_by_student_rb = $linterface->Get_Radio_Button("SubsidyType2", "SubsidyType", "2", $isChecked=0, $Class=" subsidy-type ", $Display=$Lang['eNotice']['PreviewByStudent'], $Onclick="",$isDisabled=0,$isIndented=0,$specialLabel="");
?>
<table align="center" width="80%" border="0" cellpadding="5" cellspacing="0" class="form_table_v30" style="width:80%;">
<tbody>
	<tr valign="top">
		<td align="left" valign="top" class="field_title" rowspan="2"><?php echo $Lang['eNotice']['PreviewWithSubsidySettings']?></td>
		<td valign="top" class="tabletext"><?php echo $subsidy_type_not_apply_rb.'<br />'.$subsidy_type_by_identity_rb.'<br />'.$subsidy_type_by_student_rb?></td>
	</tr>
	<tr id="SubsidySelectionContainer" valign="top" style="display:none;">
		<td valign="top" class="tabletext">
			<div id="SubsidyIdentitySelectionContainer" style="display:none;"><?php echo $subsidy_identity_selection?></div>
			<div id="SubsidyStudentSelectionContainer" style="display:none;"><?php echo $linterface->Get_Ajax_Loading_Image();?></div>
		</td>
	</tr>
</tbody>	
</table>
<?php 
} 
?>
<table width="80%" align="center" border="0">
<form name="ansForm" method="post" action="update.php">
	<input type="hidden" name="qStr" value="">
    <input type="hidden" name="aStr" value="">
</form>



<tr>
	<td align="left">
		<div id="ReplySlipContainer"><?php echo $linterface->Get_Ajax_Loading_Image();?></div>
<script language="Javascript">
$(document).ready(function(){
<?php if($preview_with_subsidy){ ?>
	function get_target_students(callback)
	{
		var post_data = {};
		post_data['return_selection'] = 1;
		post_data['field_name'] = 'SubsidyStudentID';
		post_data['first_row_empty'] = 1;
		if(window.opener.document.form1['NoticeID']){
			post_data['NoticeID'] = window.opener.document.form1['NoticeID'].value;
		}
		if(window.opener.document.form1['DateStart']){
			post_data['DateStart'] = window.opener.document.form1['DateStart'].value;
		}
		post_data['type'] = window.opener.document.form1['type']? window.opener.document.form1.type.value : '';
		post_data['target[]'] = [];
		var target_selection = window.opener.document.form1['target[]'];
		if(target_selection){
			if(!target_selection.options){
				for(var i=0;i<target_selection.length;i++){
					if(target_selection[i].checked){
						post_data['target[]'].push(target_selection[i].value);
					}
				}
			}else{
				for(var i=0;i<target_selection.options.length;i++){
					if(target_selection.options[i].value != ''){
						post_data['target[]'].push(target_selection.options[i].value);
					}
				}
			}
		}
		
		$.post('ajax_get_target_students.php',post_data,function(returnData){
			$('#SubsidyStudentSelectionContainer').html(returnData);
			callback();
		});
	}
<?php } ?>

	var options = {
		"QuestionString":window.opener.document.form1.qStr.value,
		"AnswerString":"",
		"PaymentCategoryAry":[],
		"TemplatesAry":[],
		"DisplayQuestionNumber":window.opener.document.form1.DisplayQuestionNumber.checked?1:0,
		"SubsidyStudentID":'',
		"SubsidyIdentityID":'',
		<?php
		$words = $eNoticePaymentReplySlip->getRequiredWords();
		foreach($words as $key => $value){
			echo '"'.$key.'":"'.$value.'",'."\n";
		}
		?>
		"Debug":false
	};
	var sheet = new Answersheet(options);
	//sheet.renderPreviewPanel('ReplySlipContainer');
	sheet.renderFillInPanel('ReplySlipContainer');
	
<?php if($preview_with_subsidy){ ?>
	function applySubsidyTypeAndRerender()
	{
		var selected_type = $('.subsidy-type:checked').val();
		if(selected_type == '1'){
			var subsidy_identity_id = $('#SubsidyIdentityID').val();
			sheet.setOption('SubsidyIdentityID',subsidy_identity_id);
			sheet.setOption('SubsidyStudentID','');
		}else if(selected_type == '2'){
			var target_student_id = $('#SubsidyStudentID').val();
			var student_identity_id = $('#SubsidyStudentID option:checked').attr('data-identityid')?$('#SubsidyStudentID option:checked').attr('data-identityid'):'';
			sheet.setOption('SubsidyIdentityID',student_identity_id);
			sheet.setOption('SubsidyStudentID',target_student_id);
		}else{
			sheet.setOption('SubsidyStudentID','');
			sheet.setOption('SubsidyIdentityID','');
		}
		sheet.renderFillInPanel('ReplySlipContainer');
	}

	get_target_students(function(){
		$('#SubsidyStudentID').bind('change',function(){
			applySubsidyTypeAndRerender();
		});
	});
	
	$('.subsidy-type').bind('click',function(){
		var selected_value = $(this).val();
		if(selected_value == '1'){
			$('#SubsidyIdentitySelectionContainer').show();
			$('#SubsidyStudentSelectionContainer').hide();
			$('#SubsidySelectionContainer').show();
		}else if(selected_value == '2'){
			$('#SubsidyIdentitySelectionContainer').hide();
			$('#SubsidyStudentSelectionContainer').show();
			$('#SubsidySelectionContainer').show();
		}else{
			$('#SubsidySelectionContainer').hide();
			$('#SubsidyIdentitySelectionContainer').hide();
			$('#SubsidyStudentSelectionContainer').hide();
		}
		applySubsidyTypeAndRerender();
	});
	
	$('#SubsidyIdentityID').bind('change',function(){
		applySubsidyTypeAndRerender();
	});
<?php } ?>	
});
</script>
	</td>
</tr>

<tr>
	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
</tr>

<tr>
	<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_close, "button", "window.close(); return false;","cancelbtn") ?>
	</td>
</tr>
</table>
<br />

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>