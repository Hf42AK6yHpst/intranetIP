<?php
# Using :

############ Change Log [Start] ############
#
#   Date:   2020-09-17  Bill
#           add logic to copy target notice PIC & payment gateway settings
#
# 	Date:   2020-09-07  Bill    [2020-0604-1821-16170]
#			updated access right checking
#
#   Date:   2020-07-03  Bill    [2020-0604-1717-36170]
#           Create file
#
############ Change Log [End] ############

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

// [2020-0604-1821-16170]
//if (!$plugin['notice'] || $_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["notice"]["hasIssueRight"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"]))
if (!$plugin['notice'] || $_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["notice"]["hasPaymentNoticeIssueRight"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"]))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

intranet_auth();
intranet_opendb();

$li = new libdb();
$luser = new libuser($UserID);

$copy_from = $NoticeIDArr[0];
$lnotice = new libnotice($copy_from);

$lf = new libfilesystem();

## Copy data from target notice
// NoticeNumber, Title, Description, DateStart, DateEnd, Question, RecordType, IsModule, ReplySlipContent, AllFieldsReq, DisplayQuestionNumber, ContentType, MergeFormat, MergeType, MergeFile
$sql = "INSERT INTO INTRANET_NOTICE 
            (NoticeNumber, Title, Description, DateStart, DateEnd, Question, IsModule, Module, ReplySlipContent, DebitMethod, AllFieldsReq, DisplayQuestionNumber, ContentType, MergeFormat, MergeType, MergeFile, MerchantAccountID, isPaymentNoticeApplyEditor)
		SELECT 
            NoticeNumber, Title, Description, DateStart, DateEnd, Question, IsModule, Module, ReplySlipContent, DebitMethod, AllFieldsReq, DisplayQuestionNumber, ContentType, MergeFormat, MergeType, MergeFile, MerchantAccountID, isPaymentNoticeApplyEditor 
		FROM 
			INTRANET_NOTICE 
		WHERE 
			NoticeID = '$copy_from'";
$li->db_db_query($sql) or die(mysql_error());
$NoticeID = $li->db_insert_id();

## Update description if target notice has embedded images uploaded with flash upload (fck)
$lnotice->updateNoticeFlashUploadPath($NoticeID, $lnotice->Description,'copy');

## Remove all existing payment items in question
$sql = "SELECT Question FROM INTRANET_NOTICE WHERE NoticeID = '$NoticeID'";
$question = $lnotice->returnArray($sql);
$question = $question[0]['Question'];
if($question != '') {
    $question = preg_replace('/\|\|#PAYMENTITEMID#(.*?)\|\|/', '||#PAYMENTITEMID#0||', $question);
}

## Generate attachment folder
$noticeAttFolder = session_id().".".time();
session_register_intranet("noticeAttFolder", $noticeAttFolder);
$path = "$file_path/file/notice/$noticeAttFolder";
if (!is_dir($path))
{
    $lf->folder_new($path);
}

## Copy attachment and PowerVoice files from target notice
if(!empty($lnotice->Attachment))
{
    $old_path = "$file_path/file/notice/".$lnotice->Attachment;
    $lf->folder_content_copy($old_path, $path);
}

## Get issue user name
$IssueUserName = $luser->getNameForRecord();

## Update notice
$sql = "UPDATE INTRANET_NOTICE 
            SET Question = '".$lnotice->Get_Safe_Sql_Query($question)."', IssueUserID = '$UserID', IssueUserName = '$IssueUserName', Attachment = '$noticeAttFolder', RecordStatus = '2', DateInput = NOW(), DateModified = NOW() 
        WHERE NoticeID = '$NoticeID'";
$li->db_db_query($sql);

## Copy PICs from target notice
$sql = "INSERT INTO INTRANET_NOTICE_PIC 
			(NoticeID, PICUserID, InputBy, DateInput, ModifiedBy, DateModified)
		SELECT 
			'$NoticeID' as NoticeID, PICUserID, '$UserID' as InputBy, NOW(), '$UserID' as ModifiedBy, NOW() 
		FROM 
			INTRANET_NOTICE_PIC 
		WHERE 
			NoticeID = '$copy_from'";
$li->db_db_query($sql);

## Copy payment gateway from target notice
$sql = "INSERT INTO INTRANET_NOTICE_PAYMENT_GATEWAY 
			(NoticeID, MerchantAccountID, DateInput, InputBy)
		SELECT 
			'$NoticeID' as NoticeID, n_pay_g.MerchantAccountID, NOW(), '$UserID' as InputBy
		FROM 
			INTRANET_NOTICE_PAYMENT_GATEWAY n_pay_g
			INNER JOIN PAYMENT_MERCHANT_ACCOUNT m_acct ON (n_pay_g.MerchantAccountID = m_acct.AccountID AND m_acct.RecordStatus = 1)
		WHERE 
			n_pay_g.NoticeID = '$copy_from'";
$li->db_db_query($sql);

intranet_closedb();
header("Location: paymentNotice_copy_edit.php?NoticeID=$NoticeID&isCopied=1");
?>