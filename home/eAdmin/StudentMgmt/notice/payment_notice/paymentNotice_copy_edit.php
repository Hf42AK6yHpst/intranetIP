<?php
// Using : Bill

######################
#
#   Date:   2020-07-03  Bill    [2020-0604-1717-36170]
#           Create file - for audience selection
#
######################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lnotice = new libnotice($NoticeID);
$lpayment = new libpayment();

// [2020-0407-1445-44292]
$isAudienceLimited = $lnotice->isClassTeacherAudienceTargetLimited();

$CurrentPageArr['eAdminNotice'] = 1;
if($lnotice->Module == 'Payment') {
    $CurrentPage = "PaymentNotice";
}
else {
    $CurrentPage = "PageNotice";
}

$lform = new libform();

if (!$plugin['notice'] || $_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"] || !($_SESSION["SSV_PRIVILEGE"]["notice"]["hasIssueRight"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"]))
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$type = $lnotice->RecordType;
$isCopiedPaymentNotice = $_GET['isCopied'] && $type == '';

// Update notice audience
if($isCopiedPaymentNotice && $_POST['isSubmit'])
{
    if (isset($target) && sizeof($target) != 0) {
        $targetID = implode(",", (array)$target);
    }
    $sql = "UPDATE INTRANET_NOTICE SET RecordType = '".$_POST['type']."', RecipientID = '$targetID' WHERE NoticeID = {$NoticeID}";
    $lnotice->db_db_query($sql);

    $username_field = getNameFieldWithClassNumberEng("");
    if ($_POST['type'] == 1)         # Whole School
    {
        $username_field = getNameFieldWithClassNumberEng("iu.");
        $sql = "SELECT 
						ycu.UserID, $username_field
                FROM
						YEAR as y 
						INNER JOIN YEAR_CLASS as yc ON (yc.YearID = y.YearID AND yc.AcademicYearID = ". Get_Current_Academic_Year_ID() .")
						INNER JOIN YEAR_CLASS_USER as ycu ON (ycu.YearClassID = yc.YearClassID)
						INNER JOIN INTRANET_USER as iu ON (iu.UserID = ycu.UserID)
            		";
        $actual_target_users = $lnotice->returnArray($sql);

        if (sizeof($actual_target_users) != 0)
        {
            $delimiter = "";
            $values = "";
            for ($i=0; $i<sizeof($actual_target_users); $i++)
            {
                list($uid,$name,$usertype) = $actual_target_users[$i];
                $name = intranet_htmlspecialchars($name);
                $values .= "$delimiter ('$NoticeID','$uid','$name',0,now(),now())";
                $delimiter = ",";
            }

            $sql = "INSERT IGNORE INTO INTRANET_NOTICE_REPLY (NoticeID,StudentID,StudentName,RecordStatus,DateInput,DateModified)
                    VALUES $values";
            $lnotice->db_db_query($sql);
        }
    }
    else if ($_POST['type'] == 2)    # Some levels only
    {
        # Grab the class name for the levels
        $list = implode(",",$target);
        $sql = "SELECT 
	     			    yc.ClassTitleEN 
	     		FROM 
	     			    YEAR as y 
	     			    INNER JOIN YEAR_CLASS as yc on (yc.YearID = y.YearID AND yc.AcademicYearID = ". Get_Current_Academic_Year_ID() .")
	     		WHERE 
	     			    y.YearID IN ($list)
     		";
        $classes = $lnotice->returnVector($sql);
        $classList = "'".implode("','", $classes)."'";

        $sql = "INSERT IGNORE INTO INTRANET_NOTICE_REPLY (NoticeID,StudentID,StudentName,RecordStatus,DateInput,DateModified)
                SELECT $NoticeID,UserID,$username_field,0,now(),now() FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1 AND ClassName IN ($classList)";
        $lnotice->db_db_query($sql);
    }
    else if ($_POST['type'] == 3)    # Some classes only
    {
        # Grab the class name for the classes
        $list = implode(",",$target);
        $sql = "SELECT ClassTitleEN FROM YEAR_CLASS WHERE YearClassID IN ($list)";
        $classes = $lnotice->returnVector($sql);
        $classList = "'".implode("','",$classes)."'";
        if($classList)
        {
            $sql = "INSERT IGNORE INTO INTRANET_NOTICE_REPLY (NoticeID,StudentID,StudentName,RecordStatus,DateInput,DateModified)
                    SELECT $NoticeID,UserID,$username_field,0,now(),now() FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1 AND ClassName IN ($classList)";
            $lnotice->db_db_query($sql);
        }
    }
    else if ($_POST['type'] == 4)  # Some individuals only
    {
        $actual_target_users = $lnotice->returnTargetUserIDArray($targetID);
        if (sizeof($actual_target_users) != 0)
        {
            $delimiter = "";
            $values = "";
            for ($i=0; $i<sizeof($actual_target_users); $i++)
            {
                list($uid,$name,$usertype) = $actual_target_users[$i];
                $values .= "$delimiter ('$NoticeID','$uid','$name',0,now(),now())";
                $delimiter = ",";
            }

            $sql = "INSERT IGNORE INTO INTRANET_NOTICE_REPLY (NoticeID,StudentID,StudentName,RecordStatus,DateInput,DateModified)
                    VALUES $values";
            $lnotice->db_db_query($sql);
        }
    }

    header("Location: paymentNotice_edit.php?NoticeID=$NoticeID&copied=1");
    die();
}
// Notice audience selection
else if($isCopiedPaymentNotice)
{
    $type = $_GET['type'];
    if ($type < 1 || $type > 4) {
        $type = "";
    }
    if ($type == "")
    {
        $type_selected = false;
    }
    else
    {
        $type_selected = true;
    }

    // [2020-0407-1445-44292] limited to 2 audience types only
    if($isAudienceLimited)
    {
        $type_selection = "
        <SELECT name='type' onChange='this.form.submit()'>
            <OPTION value='' > -- $button_select -- </OPTION>
            <OPTION value='3' $check3>$i_Notice_RecipientTypeClass</OPTION>
            <OPTION value='4' $check4>$i_Notice_RecipientTypeIndividual</OPTION>
        </SELECT>";
    }
    else
    {
        $type_selection = "
        <SELECT name='type' onChange='this.form.submit()'>
            <OPTION value='' > -- $button_select -- </OPTION>
            <OPTION value='1' $check1>$i_Notice_RecipientTypeAllStudents</OPTION>
            <OPTION value='2' $check2>$i_Notice_RecipientTypeLevel</OPTION>
            <OPTION value='3' $check3>$i_Notice_RecipientTypeClass</OPTION>
            <OPTION value='4' $check4>$i_Notice_RecipientTypeIndividual</OPTION>
        </SELECT>";
    }
}
// Redirect as already set notice audience
else
{
    header("Location: paymentNotice_edit.php?NoticeID=$NoticeID");
    die();
}

// Notice Recipient Type selection
if (!$type_selected)
{
    $x .= "<tr valign='top'>";
        $x .= "<td width='30%' valign='top' nowrap class='field_title'><span class='tabletext'>$i_Notice_RecipientType</span></td>";
        $x .= "<td class='tabletext'>$type_selection</td>";
    $x .= "</tr>";
}
// Notice Recipient selection
else
{
    $lclass = new libclass();

    if ($type == 1)                 # Whole School
    {
        $nextSelect = "";
        $type_selection = "$i_Notice_RecipientTypeAllStudents";
    }
    else if ($type == 2)	        # Some levels only
    {
        $lvls = $lclass->getLevelArray();

        $nextSelect = "<tr valign='top'>";
            $nextSelect .= "<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'>$i_Notice_RecipientLevel</span> <span class='tabletextrequire'>*</span></td>";
            $nextSelect .= "<td class='tabletext'>";

        if ($type == $lnotice->RecordType)
        {
            $templateLvl = explode(",",$lnotice->RecipientID);
        }

        for ($i=0; $i<sizeof($lvls); $i++)
        {
            list($id, $name) = $lvls[$i];

            // [2017-0524-1550-19235] Skip if Class Level without any classes
            $formLevelClassList = $lclass->returnClassListByLevel($id);
            if(empty($formLevelClassList)) {
                continue;
            }

            if (isset($templateLvl) && sizeof($templateLvl) != 0) {
                $str = (in_array($id, $templateLvl) ? "CHECKED" : "");
            } else {
                $str = "";
            }

            $nextSelect .= "<input type='checkbox' name='target[]' value='$id' $str id='target_$i'><label for='target_$i'>$name</label> \n";
        }

        $nextSelect .= "</td></tr>\n";
        $type_selection = "$i_Notice_RecipientTypeLevel";
    }
    else if ($type == 3)	        # Some classes only
    {
        if ($type == $lnotice->RecordType)
        {
            $templateClass = explode(",", $lnotice->RecipientID);
        }
        $classes = $lclass->getClassList();

        // [2020-0407-1445-44292] filter not teaching classes
        if($isAudienceLimited)
        {
            $teaching_class_list = $lnotice->getClassTeacherCurrentTeachingClass();
            $teaching_class_ids = Get_Array_By_Key($teaching_class_list, 'ClassID');

            $class_size = sizeof($classes);
            for ($i=0; $i<$class_size; $i++)
            {
                list($classid, $name, $lvlID) = $classes[$i];
                if(!in_array($classid, $teaching_class_ids)) {
                    unset($classes[$i]);
                }
            }
            $classes = array_values($classes);
        }

        $prevID = $classes[0][2];

        $nextSelect = "<tr valign='top'>";
            $nextSelect .= "<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'>$i_Notice_RecipientClass</span> <span class='tabletextrequire'>*</span></td>";
            $nextSelect .= "<td class='tabletext'>";

        for ($i=0; $i<sizeof($classes); $i++)
        {
            list($classid, $name, $lvlID) = $classes[$i];
            if (isset($templateClass) && sizeof($templateClass) != 0) {
                $str = (in_array($classid, $templateClass) ? "CHECKED" : "");
            } else {
                $str = "";
            }

            if ($lvlID == $prevID)
            {
                $nextSelect .= " ";
            }
            else
            {
                $nextSelect .= "<br>\n";
                $prevID = $lvlID;
            }

            $nextSelect .= "<input type='checkbox' name='target[]' value='$classid' $str id='target_$i'><label for='target_$i'>$name</label> ";
        }

        $nextSelect .= "</td></tr>\n";
        $type_selection = $i_Notice_RecipientTypeClass;
    }
    else	                        # Some individuals only
    {
        ### Choose Member Btn
        $btn_ChooseMember = $linterface->GET_SMALL_BTN($Lang['Btn']['Select'], "button", "newWindow('/home/common_choose/index.php?fieldname=target[]&page_title=SelectAudience&permitted_type=2&DisplayGroupCategory=1&UserRecordStatus=0,1',16)");

        ### Auto-complete ClassName ClassNumber StudentName search
        $UserClassNameClassNumberInput = '';
        $UserClassNameClassNumberInput .= '<div style="float:left;">';
        $UserClassNameClassNumberInput .= '<input type="text" id="UserClassNameClassNumberSearchTb" name="UserClassNameClassNumberSearchTb" value="" />';
        $UserClassNameClassNumberInput .= '</div>';

        ### Auto-complete login search
        $UserLoginInput = '';
        $UserLoginInput .= '<div style="float:left;">';
        $UserLoginInput .= '<input type="text" id="UserLoginSearchTb" name="UserLoginSearchTb" value="" />';
        $UserLoginInput .= '</div>';

        ### Student Selection Box & Remove all Btn
        $MemberSelectionBox = $linterface->GET_SELECTION_BOX($recipient_name_ary, "name='target[]' id='target[]' class='select_studentlist' size='15' multiple='multiple'", "");
        $btn_RemoveSelected = $linterface->GET_SMALL_BTN($Lang['Btn']['RemoveSelected'], "button", "js_Remove_Selected_Student();");

        $nextSelect = "<tr valign='top'>";
            $nextSelect .= "<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'>$i_Notice_RecipientIndividual</span> <span class='tabletextrequire'>*</span></td>";
            $nextSelect .= "<td class='tabletext'>";
                $nextSelect .= "<table>";
                    $nextSelect .= '<tr>
                                        <td class="tablerow2" valign="top">
                                            <div id="divSelectStudent">'.'</div>
                                        </td>
                                    </tr>';
                $nexSelect .= "</table>";
            $nextSelect .= "</td>";
        $nextSelect .= "</tr>";

        $nextSelect .= "</table>";
        $nextSelect .= "</td></tr>\n";

        $type_selection = $i_Notice_RecipientTypeIndividual;
    }

    $type_selection .= " <input type='hidden' name='type' value='$type'>";

    $x .= "<tr valign='top'>";
        $x .= "<td width='30%' valign='top' nowrap='nowrap' class='field_title'><span class='tabletext'>$i_Notice_RecipientType</span></td>";
        $x .= "<td class='tabletext'>$type_selection</td>";
    $x .= "</tr>";

    $x .= $nextSelect;
}

### Title ###
$TAGS_OBJ[] = array($Lang['eNotice']['PaymentNotice']);
$MODULE_OBJ = $lnotice->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($i_Notice_Edit);

echo $linterface->Include_AutoComplete_JS_CSS();
?>

<?php include_once ($PATH_WRT_ROOT . "home/common_choose/user_selection_plugin_js.php");?>
<script language="javascript">
    var AutoCompleteObj_ClassNameClassNumber;
    var AutoCompleteObj_UserLogin;

    $(document).ready( function() {
        var divId ='divSelectStudent'
        var targetUserTypeStudentAry = [2];
        var StudentSelectedFieldName = 'target[]';
        var StudentSelectedFieldId = 'target[]';
        var selectedUsersData = "";
        var extraParmData = "";
        <?php if($isAudienceLimited) { ?>
            extraParmData = ['filterClassTeacherClass', 'filterOwnClassesOnly'];
        <?php } ?>
        Init_User_Selection_Menu(divId, targetUserTypeStudentAry, StudentSelectedFieldName, StudentSelectedFieldId, selectedUsersData, '', extraParmData);

        <?php if ($_GET["type"] == 4) { ?>
            // initialize jQuery Auto Complete plugin
            Init_JQuery_AutoComplete('UserClassNameClassNumberSearchTb', 'UserLoginSearchTb');
            $('input#UserClassNameClassNumberSearchTb').focus();
        <?php } ?>
    });

    function Init_JQuery_AutoComplete(Input_Search_Field, Target_User_Type, Target_Select_Field_Id) {
        if(Target_User_Type == 3){
            var ajaxUrl="<?=$PATH_WRT_ROOT;?>home/common_choose/ajax_search_parent.php";
        }
        else if(Target_User_Type == 2){
            var ajaxUrl="../ajax_search_user_by_classname_classnumber.php";
        }
        else if(Target_User_Type == 1){
            var ajaxUrl="<?=$PATH_WRT_ROOT;?>home/common_choose/ajax_search_user.php";
        }

        var AutoCompleteObj = $("input#" + Input_Search_Field).autocomplete(
            ajaxUrl,
            {
                onItemSelect: function(li) {
                    Add_Selected_User(li.extra[1], li.selectValue, Input_Search_Field, Target_Select_Field_Id);
                },
                formatItem: function(row) {
                    return row[0];
                },
                maxItemsToShow: 100,
                minChars: 1,
                delay: 0,
                width: 200,

            }
        );

        if(Target_User_Type == 3){
            AutoCompleteObj_parent = AutoCompleteObj;
        }
        else if(Target_User_Type == 2){
            AutoCompleteObj_student = AutoCompleteObj;
        }
        else if(Target_User_Type == 1){
            AutoCompleteObj_staff = AutoCompleteObj;
        }
        Update_Auto_Complete_Extra_Para(Target_Select_Field_Id);
    }

    function checkform2(obj) {
        $('#submit2').attr('disabled', true);
        var canSubmit = checkform(obj);
        if (canSubmit) {
            obj.method = 'post';
            obj.submit();
        }
        else {
            $('#submit2').attr('disabled', false);
        }
    }

    function checkform(obj){
        <?php if ($type == 2) { ?>
            if(!countChecked(obj, "target[]")){ alert("<?=$i_alert_pleaseselect?><?=$i_Notice_RecipientLevel?>"); return false; }
        <? } ?>

        <?php if ($type == 3) { ?>
            if(!countChecked(obj, "target[]")){ alert("<?=$i_alert_pleaseselect?><?=$i_Notice_RecipientClass?>"); return false; }
        <? } ?>

        <?php if ($type == 4) {?>
        if(obj.elements["target[]"].length == 0){ alert("<?php echo $i_frontpage_campusmail_choose; ?>"); return false; }
            checkOptionAll(obj.elements["target[]"]);
        <? } ?>

        return true;
    }

    function clickCancel()
    {
        with(document.form1)
        {
            <?php if ($type_selected) { ?>
                history.back();
            <?php } else { ?>
                window.location = 'paymentNotice.php';
            <?php } ?>
        }
    }
</script>

<br />
<form name="form1" id="form1" method="get" action="">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" >
    <tr>
        <td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
        <td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
    </tr>
    <tr>
        <td colspan="2">
            <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center" class="form_table_v30">
                <tr>
                    <td>
                        <br />
                        <table align="center" width="30%" border="0" cellpadding="5" cellspacing="0"  class="form_table_v30">
                            <?=$x?>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table width="99%" border="0" cellspacing="0" cellpadding="5" align="center">
            <?php if ($type_selected) { ?>
                <tr>
                    <td align="left" class="tabletextremark"><?=$i_general_required_field2?></td>
                </tr>
                <tr>
                    <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
                    <td align="center">
                        <?= $linterface->GET_ACTION_BTN($button_continue, "button", ($type==4 ? "checkOption(this.form.elements['target[]']);" : "") ."checkform2(this.form);", "submit2") ?>
                        <?= $linterface->GET_ACTION_BTN($button_reset, "reset") ?>
                        <?= $linterface->GET_ACTION_BTN($button_back, "button", "clickCancel();","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
                    </td>
                </tr>
            <?php } else { ?>
                <tr>
                    <td colspan="2">
                        <table width="99%" border="0" cellspacing="0" cellpadding="5" align="center" >
                            <tr>
                                <td><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                            </tr>
                            <tr>
                                <td align="center" class="field_title">
                                    <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "clickCancel();","cancelbtn") ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            <?php } ?>
            </table>
        </td>
    </tr>
</table>
<br />

<input type="hidden" name="NoticeID" value="<?=$NoticeID?>" />
<input type="hidden" name="isCopied" value="<?=$isCopied?>"/>
<?php if ($type_selected) { ?>
    <input type="hidden" name="isSubmit" value="1"/>
<?php } ?>
</form>

<?
intranet_closedb();

print $linterface->FOCUS_ON_LOAD("form1.type");

$linterface->LAYOUT_STOP();
?>