<?php
# using:

########################################
#
#	Date:	2020-08-07	Bill	[2020-0807-1403-48235]
#			fixed display problem of highlighted auto fill-in text - remove some css style & htmlspecialchars_decode()
#
#	Date:	2017-07-14	Bill	[2016-0719-1134-29206]
#			fixed cannot preview updated CSV data when edit notice
#
#	Date:	2016-07-19	Bill	[2016-0719-1134-29206]
#			display current student order when target audience is Applicable students
#
#	Date:	2015-10-28	Bill	[2015-0416-1040-06164]
#			support merge notice content
#			- copy from /MassMailing/new_step2.php
#
########################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

# Create a new interface instance
$linterface = new interface_html();

# Create a Notice Object
$lnotice = new libnotice($NoticeID);

// get targets if refresh
if($previewOthers){
	$target = explode(",",$target);
}
// get content and CSV if refresh
if($previewOthers){
	// Notice Content
	$Description = intranet_undo_htmlspecialchars($Description);

	// Merge CSV Path
	$PreviewCSVPath = getDecryptedText($PreviewCSVPath);
	if($PreviewCSVPath!=""){
		$FileCSV = $PreviewCSVPath;
		$mergedes = $PreviewCSVPath;
	}
}
// save CSV into temp folder for further preview
else if($changeCSV && $PreviewCSVPath==""){
	$lf = new libfilesystem();

	// get file
	$mergefile = stripslashes($FileCSV_Name);
	$mergeloc = $FileCSV;
	if ($mergeloc == "none" || $mergeloc==""){
		// do nothing
	}
	else
	{
		if (!strpos($mergefile,".csv"==0)){
			$mergepath = "$file_path/file/mergenotice";
			$lf->folder_new($mergepath);
				
			$mergepath .= "/temp";
			$lf->folder_new($mergepath);
				
			$mergepath .= "/".$UserID;
			$lf->folder_new($mergepath);
				
			// copy file to destination
			$mergedes = "$mergepath/$mergefile";
			$lf->lfs_copy($mergeloc, $mergedes);
		}
	}
}

# Get Audience
$new_audience = array();
if ($ContentType == "2") {
	$original_audience = array();
	$preview = true;
	
	$lnotice = new libnotice($NoticeID);
	
	// [2016-0719-1134-29206] Existing Notice + Use old CSV
	if ($NoticeID > 0 && !$changeCSV) {
		$new_audience = $lnotice->getNoticeCSVUserInfo();
	}
	// Use new CSV
	else {
		// (Preview - 1st student) - Get CSV through file upload path
		if (!empty($_REQUEST["FileCSV_Name"])) {
			$new_audience = $lnotice->getNoticeCSVUserInfo($preview, $UserID . "/" . $_REQUEST["FileCSV_Name"]);
		}
		// (Preview - Other students) - Get CSV through temp folder (CSV already stored when Preview - 1st student)
		else {
			$new_audience = $lnotice->getNoticeCSVUserInfo($preview, $UserID . "/" . basename($FileCSV));
		}
	}
}
else {
	if ($type==1)         # Whole School
	{
		$sql = "select 
					ycu.UserID
				from
					YEAR as y 
					INNER JOIN YEAR_CLASS as yc on (yc.YearID = y.YearID and yc.AcademicYearID = ". Get_Current_Academic_Year_ID() .")
					INNER JOIN YEAR_CLASS_USER as ycu on (ycu.YearClassID = yc.YearClassID)
					INNER JOIN INTRANET_USER as iu on (iu.UserID = ycu.UserID)
				WHERE iu.RecordType = 2 AND iu.RecordStatus in (0,1)
					Order By yc.ClassTitleEN, ycu.ClassNumber+0
	    		";
	     $new_audience = $lnotice->returnVector($sql);
	}
	else if ($type==2)    # Some levels only
	{
		# Grab the class name for the levels
		$list = implode(",",(array)$target);
		$sql = "
		SELECT 
			yc.ClassTitleEN 
		FROM 
			YEAR as y 
			INNER JOIN YEAR_CLASS as yc on (yc.YearID = y.YearID and yc.AcademicYearID = ". Get_Current_Academic_Year_ID() .")
		WHERE 
			y.YearID IN ($list)
			Order By yc.ClassTitleEN
		";
		$classes = $lnotice->returnVector($sql);
		$classList = "'".implode("','",(array)$classes)."'";
		
		if(!empty($classList))
		{
			# new audience
			$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus in (0,1) AND ClassName IN ($classList) Order By ClassName, ClassNumber+0";
			$new_audience = $lnotice->returnVector($sql);
		}
	}
	else if ($type==3)    # Some classes only
	{
	     # Grab the class name for the classes
	     $list = implode(",",(array)$target);
	     $sql = "SELECT ClassTitleEN FROM YEAR_CLASS WHERE YearClassID IN ($list) Order By ClassTitleEN";
	     $classes = $lnotice->returnVector($sql);
	     $classList = "'".implode("','",(array)$classes)."'";
	     
	     if(!empty($classList))
	     {
		     # new audience
		     $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus in (0,1) AND ClassName IN ($classList) Order By ClassName, ClassNumber+0";
		     $new_audience = $lnotice->returnVector($sql);
	     }
	}
	else if ($type == 4)  # Some only
	{
		if(isset($target) && sizeof($target)!=0)
			$targetID = implode(",",$target);
		
		// [2016-0719-1134-29206] add Class Name and Class Number Sorting
		$actual_target_users = $lnotice->returnTargetUserIDArray($targetID, "0,1", true);
		if (sizeof($actual_target_users)!=0)
		{
			for ($i=0; $i<sizeof($actual_target_users); $i++)
			{
				$new_audience[] = $actual_target_users[$i]['UserID'];
			}
		}
	}
}

// Create Audience drop down list
$PreviewChoice = array();

$luser = new libuser('', '', $new_audience);
for($i=0; $i<count($new_audience); $i++){
	$luser->LoadUserData($new_audience[$i]);
	$studentDisplay = Get_Lang_Selection($luser->ChineseName, $luser->EnglishName)." ( ".$luser->ClassName." - ".$luser->ClassNumber." )";
	$PreviewChoice[] = array($new_audience[$i], $studentDisplay);
}
if(!$targetStudentID){
	$targetStudentID = $PreviewChoice[0][0];
}
$PreviewSelections = $linterface->GET_SELECTION_BOX($PreviewChoice, " onChange='document.form2.submit()' name='targetStudentID' id='targetStudentID' ", "", $targetStudentID);


// handle extra slashes
$Description = stripslashes($Description);

# Title
$TAGS_OBJ[] = array($Lang['eNotice']['SchoolNotice']);
$MODULE_OBJ = $lnotice->GET_MODULE_OBJ_ARR();

# Page
$CurrentPageArr['eAdminNotice'] = 1;
$CurrentPage = "PageNotice";
$PAGE_NAVIGATION[] = array("Preview");

//$linterface->LAYOUT_START();
?>
<link href="/templates/2009a/css/content_25.css" rel="stylesheet" type="text/css">
<link href="/templates/2009a/css/content_30.css" rel="stylesheet" type="text/css">
<style>
/*
* {
    font-size: 12px;
    font-family: Verdana, Arial, Helvetica, sans-serif, 新細明體, 細明體_HKSCS, mingliu;
}
*/
body {
	background-color: #FFFFFF;
}
</style>
<br>
<form name="form2" id="form2" method="post" action="preview_merge_csv.php" enctype="multipart/form-data" >
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr><td width="100%" height="350px" valign="top">
	<div style="padding:8px; height:350px; overflow:auto" width="100%" >
		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td colspan="3"><div class="table_board"><span class="sectiontitle_v30"><?=$Lang['Btn']['Preview'] ." ". $PreviewSelections ?><br/><br/></span></div></td></tr>
		<tr>
			<td nowrap="nowrap">&nbsp; &nbsp;</td>
			<td width="95%" valign="top" style="border:dotted #555555 1px;">
				<div id="PreviewMerged"><?=$lnotice->previewNoticeContent($targetStudentID, ($changeCSV ? $FileCSV : ""), $Description, $ContentType, $MergeFormat, $MergeType)?></div>
			</td>
			<td nowrap="nowrap">&nbsp; &nbsp;</td>
		</tr>
		<tr><td colspan="3">&nbsp; &nbsp;</td></tr>
		<tr>
			<td colspan="3"><p><?=$Lang['AccountMgmt']['Remarks']?> : <span style='background:#88FF88;'> &nbsp &nbsp;&nbsp;</span> <?=$Lang['MassMailing']['PreviewMergedData']?> &nbsp; &nbsp; &nbsp; <span style='background:#FF6666;'> &nbsp &nbsp;&nbsp;</span> <?=$Lang['MassMailing']['PreviewMissingData']?></p></td>
		</tr>
		</table>
	</div>
</td></tr>
</table>
<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center" style="border-top: 1px dashed #CCCCCC;">
	<tr>
		<td align="center" colspan="3">
			<input type="button" class="formbutton" onclick="parent.tb_remove()" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$Lang['eBooking']['eService']['Cancel']?>" />
		</td>
	</tr>
</table>

<input type="hidden" name="NoticeID" value="<?=$NoticeID?>" />
<input type="hidden" name="type" value="<?=$type?>" />
<input type="hidden" name="target" value="<?=implode(",",(array)$target)?>" />

<input type="hidden" name="Description" value="<?=intranet_htmlspecialchars($Description)?>" />

<input type="hidden" name="ContentType" value="<?=$ContentType?>" />
<input type="hidden" name="MergeFormat" value="<?=$MergeFormat?>" />
<input type="hidden" name="MergeType" value="<?=$MergeType?>" />
<input type="hidden" name="changeCSV" value="<?=$changeCSV?>" />
<input type="hidden" name="PreviewCSVPath" value="<?=getEncryptedText($mergedes)?>" />

<input type="hidden" name="previewOthers" value="1" />

</form>

<?php
intranet_closedb();
//$linterface->LAYOUT_STOP();

// For performance tunning info
//echo '<script language="JavaScript" src="/templates/jquery/jquery-1.3.2.min.js"></script>';
//debug_pr('$runTime = '.$runTime.'s');
//debug_pr('convert_size(memory_get_usage()) = '.convert_size(memory_get_usage()));
//debug_pr('convert_size(memory_get_usage(true)) = '.convert_size(memory_get_usage(true)));
//debug_pr('Query Count = '.$GLOBALS[debug][db_query_count]);
////$lreportcard->db_show_debug_log();
//$lreportcard->db_show_debug_log_by_query_number(1);
//die();
?>