<?php
# using: Bill

################## Change Log [Start] ##############
#
#	Date:	2016-09-09	Bill
#			Only show released notice
#
#	Date:	2016-07-14 (Bill)
#			fixed incorrect record count due to Group By in sql
#
#	Date:	2016-06-06	Kenneth
#			edit start / end date for eNotice 'Approval' Update
#
#	Date:	2016-04-20	Bill	[2016-0405-1450-49066]
#			allow Advanced Control Group to access Notice Summary
#
#	Date:	2014-11-28	Omas
#			Improve : add new column WebSAM# and not-signed notice, add btn & thickbox for send push message to eClass App
#
#	Date:	2014-11-14	Bill
#			Fixed: only display count of "Distributed" notice result [Case#P71405]
#
################## Change Log [End] ##############

//### set cookies
//
//if ($page_size_change == 1)
//{
//    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
//    $ck_page_size = $numPerPage;
//}
//# preserve table view
//if ($ck_notice_summary_page_number!=$pageNo && $pageNo!="")
//{
//	setcookie("ck_notice_summary_page_number", $pageNo, 0, "", "", 0);
//	$ck_notice_summary_page_number = $pageNo;
//} else if (!isset($pageNo) && $ck_notice_summary_page_number!="")
//{
//	$pageNo = $ck_notice_summary_page_number;
//}
//
//if ($ck_notice_summary_page_order!=$order && $order!="")
//{
//	setcookie("ck_notice_summary_page_order", $order, 0, "", "", 0);
//	$ck_notice_summary_page_order = $order;
//} else if (!isset($order) && $ck_notice_summary_page_order!="")
//{
//	$order = $ck_notice_summary_page_order;
//}
//
//if ($ck_notice_summary_page_field!=$field && $field!="")
//{
//	setcookie("ck_notice_summary_page_field", $field, 0, "", "", 0);
//	$ck_notice_summary_page_field = $field;
//} else if (!isset($field) && $ck_notice_summary_page_field!="")
//{
//	$field = $ck_notice_summary_page_field;
//}comment by omas 2014/12/12

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

// [2016-0405-1450-49066]
//if (!$plugin['notice'] || $_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"] || !$_SESSION['SSV_USER_ACCESS']['eAdmin-eNotice'])
if (!$plugin['notice'] || $_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_template.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$lnotice = new libnotice();
$libAppTemplate = new libeClassApp_template();

// [2016-0405-1450-49066] allow access - eNotice Admin and Advanced Control Group member
if (!$lnotice->hasFullRight())
{	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

# Init libeClassApp_template
$libAppTemplate->setModule('eNotice');
$libAppTemplate->setSection('NoticeSummaryReport');

$CurrentPage	= "PageNoticeReports_NoticeSummary";
$CurrentPageArr['eAdminNotice'] = 1;

# db table info
$order = ($order == '') ? 1 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 1 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 50 : $numPerPage;


$li = new libdbtable2007($field, $order, $pageNo);
if ($field == 1){
	$li->fieldorder2 = " , u.ClassNumber asc ";
}
else{
	$li->fieldorder2 = " ,u.ClassName asc, u.ClassNumber asc  ";
}


### Title ###
$TAGS_OBJ[] = array($Lang['eNotice']['NoticeSummary']);
$MODULE_OBJ = $lnotice->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if(empty($target)){
$target = explode(",",$target_str);
}
$target_str = implode(",",$target);

$thisAudience = $lnotice->returnRecipientNames2($type, $target_str);

if ($type==2)    # Some levels only
{
     # Grab the class name for the levels
     $list = implode(",",$target);
     $sql = "
	     		SELECT 
	     			yc.ClassTitleEN 
	     		FROM 
	     			YEAR as y 
	     			INNER JOIN YEAR_CLASS as yc on (yc.YearID = y.YearID and yc.AcademicYearID = ". Get_Current_Academic_Year_ID() .")
	     		WHERE 
	     			y.YearID IN ($list)
     		";
     $classes = $lnotice->returnVector($sql);
     $classList = "'".implode("','",$classes)."'";
     if($classList)
     {
	     $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus in (0,1) AND ClassName IN ($classList)";
	     $student_list = $lnotice->returnVector($sql);
     }
}
else if ($type==3)    # Some classes only
{
     # Grab the class name for the classes
     $list = implode(",",$target);
     $sql = "SELECT ClassTitleEN FROM YEAR_CLASS WHERE YearClassID IN ($list)";
     $classes = $lnotice->returnVector($sql);
     $classList = "'".implode("','",$classes)."'";
     if($classList)
     {
     	$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus in (0,1) AND ClassName IN ($classList)";
     	$student_list = $lnotice->returnVector($sql);
 	}
}
else if ($type == 4)  # Some only
{
	$targetID = implode(",",$target);
	$actual_target_users = $lnotice->returnTargetUserIDArray($targetID, "0,1");
	if (sizeof($actual_target_users)!=0)
	{
		for ($i=0; $i<sizeof($actual_target_users); $i++)
		{
			$student_list[] = $actual_target_users[$i]['UserID'];
		}
	}
}

if(!empty($student_list))
{
	$student_str = implode(",",$student_list);
}
else
{
	$student_str = "-1";
}
$student_namefield = getNamefieldByLang("u.");
$clsName = ($intranet_session_language=="en") ? "g.ClassTitleEN" : "g.ClassTitleB5";

$sign_count = ($sign_status==1)? "count(a.SignerID >0) as signed_no," : "";
$sign_status_cond = ($sign_status==2) ? " having (count(*) - count(a.SignerID >0))>0 " : "";

##SQL for data table
$li->db_db_query('SET SESSION group_concat_max_len = 1000000');
$DateStart = $DateStart.' '.'00:00:00';
$DateEnd = $DateEnd. ' '.'23:59:59';
$sql = "Select 
			u.WebSAMSRegNo as RegNo,
			$clsName as class_name, 
			u.ClassNumber,
			$student_namefield as student_name,
			$sign_count
			count(*) - count(a.SignerID >0) as unsign_no,
			GROUP_CONCAT( 
						IF(a.SignerID IS NULL,
							CONCAT(
							'<a href=\"javascript:void(0);\" onclick=\"viewNotice(', b.NoticeID, ',', u.UserID, ');\">',
								b.NoticeNumber,
							'</a>'),
							NULL
						  )
			ORDER BY b.NoticeNumber ASC
			SEPARATOR ', ') as link
			
		from 
			INTRANET_NOTICE_REPLY as a
			left join INTRANET_NOTICE as b on b.NoticeID = a.NoticeID 
			left join INTRANET_USER as u on u.UserID = a.StudentID 
			left join YEAR_CLASS_USER as f ON (f.UserID=u.UserID)
			left join YEAR_CLASS as g ON (f.YearClassID=g.YearClassID)
		where 
			g.AcademicYearID=". Get_Current_Academic_Year_ID() ." and 
			a.StudentID in ($student_str) and  
			b.RecordStatus = 1 and
			not(b.DateEnd<'$DateStart' or b.DateStart>'$DateEnd') and
			b.DateStart <= now()
		group by 
			a.StudentID  
		$sign_status_cond
";
$li->sql = $sql;

if($sign_status==1)
{
	$li->field_array = array("RegNo","class_name","ClassNumber","student_name","signed_no","unsign_no");
	$li->no_col = 8;
	$cell_width = "10%";
}
else
{
	$li->field_array = array("RegNo","class_name","ClassNumber","student_name","unsign_no");
	$li->no_col = 7;
	$cell_width = "15%";
}

$li->IsColOff = "IP25_table";

// fixed incorrect record count due to Group By in sql
$li->count_mode = 1;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<th width='1' class='num_check'>#</th>\n";
$li->column_list .= "<th width='$cell_width' >".$li->column($pos++, "WebSAMS No.")."</th>\n";
$li->column_list .= "<th width='$cell_width' >".$li->column($pos++, $i_ClassName)."</th>\n";
$li->column_list .= "<th width='$cell_width' >".$li->column($pos++, $i_ClassNumber)."</th>\n";
$li->column_list .= "<th width='$cell_width' >".$li->column($pos++, $i_UserStudentName)."</th>\n";

if($sign_status==1)
{
	$li->column_list .= "<th width='15%' >".$li->column($pos++, $i_Notice_Signed)."</th>\n";
}
else{
	//do nth
}
$li->column_list .= "<th width='15%' >".$li->column($pos++, $i_Notice_Unsigned)."</th>\n";
$li->column_list .= "<th width='30%' >".$Lang['eNotice']['NotSignedNoticeNum']."</th>\n";

$PrintBtn = "<a href=\"javascript:print_details()\" class='print'>" . $Lang['eNotice']['PrintDetails'] . "</a>";

if ($plugin['eClassApp']) {
	$hiddenFieldAry = array ();
	$hiddenFieldAry[] = array('StudentList' , rawurlencode(serialize($student_list)) );
	$hiddenFieldAry[] = array('DateStart' , $DateStart );
	$hiddenFieldAry[] = array('DateEnd' , $DateEnd );
	$thickbox = $libAppTemplate->getPushMessageThickBox($hiddenFieldAry);
}
?>

<script type="text/javascript" src= "<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css" type="text/css" media="screen" />
<? include_once($PATH_WRT_ROOT."home/eClassApp/common/pushMessageTemplate/js_AppMessageReminder.php") ?>
<script language="javascript">
<!--
function print_details()
{
	newWindow('print_details.php?DateStart=<?=$DateStart?>&DateEnd=<?=$DateEnd?>&type=<?=$type?>&target_str=<?=$target_str?>&sign_status=<?=$sign_status?>',10);
}
//-->

$(document).ready( function() {
	AppMessageReminder.initInsertAtTextarea();
	AppMessageReminder.TargetAjaxScript = 'send_reminder_update.php';
});

function goURL(url){
	document.form1.action = url;
	document.form1.submit();
}

function viewNotice(parNoticeId, parUserId) {
	var url = '<?=$PATH_WRT_ROOT?>home/eService/notice/sign.php?NoticeID=' + parNoticeId + '&StudentID=' +parUserId +  '&Hide=1';
	$('form#form1').attr('target', 'popupWindow').attr('action', url).submit();
	$('form#form1').attr('target', '_self').attr('action', '');
}

</script>
<div class="content_top_tool">
	<div class="Conntent_tool"><?=$PrintBtn?></div>
	<br style="clear:both" />
</div>

<table class="form_table_v30">
	<tr valign='top'>
		<td class='field_title'><?=$iDiscipline['Period']?></td>
		<td><?=$DateStart?> <?=$iDiscipline['Period_End']?> <?=$DateEnd?></td>
	</tr>
	
	<tr valign='top'>
		<td class='field_title'><?=$iDiscipline['RankingTarget']?></td>
		<td><?=$thisAudience['0']?><br><?=$thisAudience['1']?></td>
	</tr>
	
	<tr>
		<td class='field_title'><?=$i_Notice_SignStatus?></td>
		<td><?= $sign_status==1 ? $Lang['eNotice']['Signed_Unsigned'] : $Lang['eNotice']['UnsignedOnly'] ?></td>
	</tr>
</table>

<form name="form1" id="form1" method="POST" action="" >
	
	<div>
		<?=$li->display();?>
	</div>
	
	<div class="edit_bottom_v30">
		<?= $thickbox['Button']?>
		
		<?= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='index.php'","backbtn") ?>

	</div>
	
	<p><center><span id='PreviewStatus'></span></center></p>
	
	<input type="hidden" name="target_str" value="<?=$target_str?>">
	<input type="hidden" name="type" value="<?=$type?>">
	<input type="hidden" name="DateStart" value="<?=$DateStart?>">
	<input type="hidden" name="DateEnd" value="<?=$DateEnd?>">
	<input type="hidden" name="sign_status" value="<?=$sign_status?>">
	<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
	<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
	<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
	<input type="hidden" name="page_size_change" id="page_size_change" value="" />
	<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />

</form>

<?=$thickbox['Content']?>

<?

intranet_closedb();
$linterface->LAYOUT_STOP();
?>
