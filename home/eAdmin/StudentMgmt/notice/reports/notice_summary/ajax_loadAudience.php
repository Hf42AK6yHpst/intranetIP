<?
# using: yat

################################
#
#
################################


$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");

$lnotice = new libnotice();

intranet_auth();
intranet_opendb();

$lclass = new libclass();

$nextSelect = "";

if ($type==1)       # Whole School
{
//     	$nextSelect = "";
//     	$type_selection = "$i_Notice_RecipientTypeAllStudents";
}
else if ($type==2)	# Some Levels
{
	$lvls = $lclass->getLevelArray();
        
	$templateLvl = explode(",",$RecipientID);
	
 	for ($i=0; $i<sizeof($lvls); $i++)
 	{
		list($id,$name) = $lvls[$i];
		if (isset($templateLvl) && sizeof($templateLvl)!=0)
			$str = (in_array($id,$templateLvl)? "CHECKED":"");
		else 
			$str = "";
		
		$nextSelect .= "<input type='checkbox' name='target[]' value='$id' $str id='target_$i'><label for='target_$i'>$name</label> \n";
 	}
 
	$nextSelect .= "</td></tr>\n";
}
else if ($type==3)	#Some Classes
{
	$templateClass = explode(",",$RecipientID);
	
 	$classes = $lclass->getClassList();
 	$prevID = $classes[0][2];

    for ($i=0; $i<sizeof($classes); $i++)
 	{
		list($classid,$name,$lvlID) = $classes[$i];
		if (isset($templateClass) && sizeof($templateClass)!=0)
			$str = (in_array($classid,$templateClass)? "CHECKED":"");
		else $str = "";
		
		if ($lvlID == $prevID)
		{
			$nextSelect .= " ";
		}
		else
		{
			$nextSelect .= "<br>\n";
			$prevID = $lvlID;
		}
		
		$nextSelect .= "<input type='checkbox' name='target[]' value='$classid' $str id='target_$i'><label for='target_$i'>$name</label> ";
 	}
}
	else if ($type==4)	## type==4
	{
		include_once($PATH_WRT_ROOT."includes/libinterface.php");
		include_once($PATH_WRT_ROOT."includes/libuser.php");
		
		$linterface 	= new interface_html();
		$lu = new libuser();
				
		$option = "";	
		if($RecipientID)
		{
					
 			$student_ary = explode(",",$RecipientID);
			$recipient_name_ary = $lnotice->returnIndividualTypeNames($RecipientID);

			for($i=0; $i<sizeof($recipient_name_ary); $i++)
			{
				$option .= "<option value='". $student_ary[$i] ."'>". $recipient_name_ary[$i] ."</option>";
			}
		}
		else
		{
			for($i = 0; $i < 40; $i++) $space .= "&nbsp;";
			$option = "<option>$space</option>";
		}
		
		$nextSelect .= "<table border='0' cellspacing='1' cellpadding='1'>";
		$nextSelect .= "<tr>";
		$nextSelect .= "<td>";
		
		$nextSelect .= "<select name='target[]' size='4' multiple>$option</select>";
		$nextSelect .= "</td>";
		$nextSelect .= "<td>";
		$nextSelect .= $linterface->GET_BTN($i_frontpage_campusmail_choose, "button","newWindow('/home/common_choose/index.php?fieldname=target[]&page_title=SelectAudience&permitted_type=2&DisplayGroupCategory=1&UserRecordStatus=0,1',16)") . "<br>";
		$nextSelect .= $linterface->GET_BTN($i_frontpage_campusmail_remove, "button","checkOptionRemove(document.form1.elements['target[]'])") . "<br>";
		$nextSelect .= "</td>";
		$nextSelect .= "</tr>";
		$nextSelect .= "</table>";
	}

intranet_closedb();

echo $nextSelect;
	
?>