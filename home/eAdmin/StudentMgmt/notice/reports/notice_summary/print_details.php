<?php
# modifying by : 

############################################
#	Date:	2019-11-06	Philips []
#			added notice number column
#
#   Date:   2019-05-30  Bill    [2019-0328-1537-48235]
#           hide students if without matched notice
#
#	Date:	2016-09-09	Bill
#			Only show released notice
#
#	Date:	2016-04-20	Bill	[2016-0405-1450-49066]
#			allow Advanced Control Group to access Notice Summary
#
#	Date:	2015-12-10	Omas
#			Improved: add sorting to Classname classnumber [#K90310]
#
#	Date:	2014-09-19	YatWoon
#			Fixed: only display "Distributed" notice result [Case#P68392]
#			Deploy: IPv10.1
#		
############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();

// [2016-0405-1450-49066]
//if (!$plugin['notice'] || $_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"] || !$_SESSION['SSV_USER_ACCESS']['eAdmin-eNotice'])
if (!$plugin['notice'] || $_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
$lnotice = new libnotice();

// [2016-0405-1450-49066] allow access - eNotice Admin and Advanced Control Group member
if (!$lnotice->hasFullRight())
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$target = !empty($target_str)? explode(",", $target_str) : $target;
$target = recursiveIntegerSafeHandling($target);

### Retrieve student list
$student_list = array();
if ($type==2)       # Some levels only
{
     # Grab the class name for the levels
     $list = implode(",", $target);
     $sql = "	SELECT 
	     			yc.ClassTitleEN 
	     		FROM 
	     			YEAR as y 
	     			INNER JOIN YEAR_CLASS as yc ON (yc.YearID = y.YearID AND yc.AcademicYearID = '". Get_Current_Academic_Year_ID() ."')
	     		WHERE 
	     			y.YearID IN ($list)
     		";
     $classes = $lnotice->returnVector($sql);
     $classList = "'".implode("','", $classes)."'";
     if($classList)
     {
	     $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (0,1) AND ClassName IN ($classList) ORDER BY ClassName ASC, ClassNumber ASC";
	     $student_list = $lnotice->returnVector($sql);
     }
}
else if ($type==3)  # Some classes only
{
     # Grab the class name for the classes
     $list = implode(",", $target);
     $sql = "SELECT ClassTitleEN FROM YEAR_CLASS WHERE YearClassID IN ($list)";
     $classes = $lnotice->returnVector($sql);
     $classList = "'".implode("','",$classes)."'";
     if($classList)
     {
     	$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (0,1) AND ClassName IN ($classList) ORDER BY ClassName ASC, ClassNumber ASC";
     	$student_list = $lnotice->returnVector($sql);
 	}
}
else if ($type == 4)    # Some only
{
    $targetID = implode(",", $target);
	$actual_target_users = $lnotice->returnTargetUserIDArray($targetID, "0,1");
	if (sizeof($actual_target_users) != 0)
	{
		for ($i=0; $i<sizeof($actual_target_users); $i++)
		{
			$student_list[] = $actual_target_users[$i]['UserID'];
		}
	}
}

$AcademicYearID = Get_Current_Academic_Year_ID();
$sign_status_cond = ($sign_status==2)? " AND (a.SignerID IS NULL OR a.SignerID = 0) " : "";

$data_ary = array();
foreach((array)$student_list as $k => $student_id)
{
	$lu = new libuser($student_id);
	$stu_info = $lu->getStudentInfoByAcademicYear($AcademicYearID);
	
	$data_ary[$student_id]['ClassName'] = $intranet_session_language=="en"? $stu_info[0]['ClassNameEn'] : $stu_info[0]['ClassNameCh'];
	$data_ary[$student_id]['ClassNumber'] = $stu_info[0]['ClassNumber'];
	$data_ary[$student_id]['Name'] = $lu->UserName(0);
	
	# Student Notice
	$sql = "SELECT 
				b.Title,
				LEFT(b.DateStart, 10) as DateStart, 
				LEFT(b.DateEnd, 10) as DateEnd, 
			 	IF(a.SignerID > 0 , '".$i_Notice_Signed."', '".$i_Notice_Unsigned."') as sign_status,
				b.NoticeNumber as NoticeNumber
			FROM 
				INTRANET_NOTICE_REPLY as a
				LEFT JOIN INTRANET_NOTICE as b ON b.NoticeID = a.NoticeID 
			WHERE 
				a.StudentID = '$student_id' AND 
				b.RecordStatus = 1 AND 
				NOT(b.DateEnd < '$DateStart' OR b.DateStart > '$DateEnd') AND
				b.DateStart <= now()
				$sign_status_cond
			ORDER BY
				b.DateStart
		";
	$result = $lnotice->returnArray($sql);
	$data_ary[$student_id]['NoticeInfo'] = $result;
	
	// [2019-0328-1537-48235] hide students if without matched notice
	if(empty($result)) {
	    unset($data_ary[$student_id]);
	}
}
?>

<table width='100%' align='center' class='print_hide' border=0>
<tr>
	<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
</tr>
</table>

<? foreach((array)$data_ary as $this_student_id => $this_data) { ?>
	<div align="center"><font size=3><b><?=$Lang['eNotice']['NoticeSummary']?></b></font></div>
	<div class="table_board">
	<table class="form_table_v30">
	<tr valign='top'>
		<td class='field_title'><?=$i_UserStudentName?></td>
		<td><?=$this_data['Name'];?> (<?=$this_data['ClassName'];?> - <?=$this_data['ClassNumber'];?>)</td>
	</tr>
	<tr valign='top'>
		<td class='field_title'><?=$iDiscipline['Period']?></td>
		<td><?=$DateStart?> <?=$iDiscipline['Period_End']?> <?=$DateEnd?></td>
	</tr>
	</table>
	
	<table class="common_table_list">
	<tr>
		<th width='1' class='num_check'>#</th>
		<th width='15%'><?=$i_Notice_NoticeNumber?></th>
		<th width='45%'><?=$i_Notice_Title?></th>
		<th width='15%'><?=$i_Notice_DateStart?></th>
		<th width='15%'><?=$i_Notice_DateEnd?></th>
		<th width='10%'><?=$i_Notice_SignStatus?></th>
	</tr>
	
	<? 
	$ni = 1;
	foreach($this_data['NoticeInfo'] as $nd => $this_notice) {?>
    	<tr valign='top'>
    		<td><?=$ni?></td>
    		<td><?=$this_notice['NoticeNumber']?>
    		<td><?=$this_notice['Title']?></td>
    		<td><?=$this_notice['DateStart']?></td>
    		<td><?=$this_notice['DateEnd']?></td>
    		<td><?=$this_notice['sign_status']?></td>
    	</tr>
	<? 
	   $ni++;
	} ?>
	</table>
	</div>
	<div style='page-break-after:always;'>&nbsp;</div>
<? } ?>

<?
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>