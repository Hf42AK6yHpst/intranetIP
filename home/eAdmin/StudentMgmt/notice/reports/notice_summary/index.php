<?php
# using: 

################## Change Log [Start] ##############
#
#	Date:	2016-04-20	Bill	[2016-0405-1450-49066]
#			allow Advanced Control Group to access Notice Summary
#
#	Date:	2014-11-28	Omas
#			Improve : add select all btn for class/form level
#
################## Change Log [End] ##############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

// [2016-0405-1450-49066]
//if (!$plugin['notice'] || $_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"] || !$_SESSION['SSV_USER_ACCESS']['eAdmin-eNotice'])
if (!$plugin['notice'] || $_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageNoticeReports_NoticeSummary";
$CurrentPageArr['eAdminNotice'] = 1;
$lnotice = new libnotice();

// [2016-0405-1450-49066] allow access - eNotice Admin and Advanced Control Group member
if (!$lnotice->hasFullRight())
{	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

//$lform = new libform();

//<OPTION value='1'>$i_Notice_RecipientTypeAllStudents</OPTION>
$type_selection = "
<SELECT name='type' onChange='selectAudience()'>
<OPTION value='' > -- $button_select -- </OPTION>
<OPTION value='2'>$i_Notice_RecipientTypeLevel</OPTION>
<OPTION value='3'>$i_Notice_RecipientTypeClass</OPTION>
<OPTION value='4'>$i_Notice_RecipientTypeIndividual</OPTION>
</SELECT>";

### Title ###
$TAGS_OBJ[] = array($Lang['eNotice']['NoticeSummary']);
$MODULE_OBJ = $lnotice->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
//$PAGE_NAVIGATION[] = array($i_Notice_New);

//if (!empty($DateStart) && !empty($DateEnd)){
//	$start_date = $DateStart;
//	$end_date = $DateEnd;
//}
//else{
	$start_date = date("Y-m-d",getStartOfAcademicYear());
	$end_date = date("Y-m-d",getEndOfAcademicYear());
//}


?>

<script language="javascript">
<!--
function selectAudience()
{
	var tid = document.form1.type.value;
	document.getElementById('div_audience_err_msg').innerHTML = "";
	
	$('#div_audience').load(
		'ajax_loadAudience.php', 
		{type: tid}, 
		function (data){
			if (tid == 2 || tid == 3){
				$('div#div_selectAll').css('display', 'block');	
			}
			else if (tid == 4){
				$('div#div_selectAll').css('display', 'none');	
			}
		
		}
	);
}

function selectAllTarget(){
	if ($('input#alltarget').attr('checked')){
		$("input[name='target[]']").each(function() {
        	$(this).attr("checked", true);
     	});
	}
	else{
		$("input[name='target[]']").each(function() {
        	$(this).attr("checked", false);
     	});
	}	
}

function reset_innerHtml()
{
 	document.getElementById('div_audience_err_msg').innerHTML = "";
}

function checkform(obj)
{
	var error_no = 0;
	var focus_field = "";
	
	//// Reset div innerHtml
	reset_innerHtml();
	
	if(obj.type.value=="") 
	{
		document.getElementById('div_audience_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect?><?=$Lang['eNotice']['Audience']?></font>';
		error_no++;
		focus_field = "type";
	}
		
	if(obj.type.value==2) 
	{
		if(!countChecked(obj, "target[]"))
		{ 
			document.getElementById('div_audience_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect?><?=$i_Notice_RecipientLevel?></font>';
			error_no++;
			focus_field = "type";
		}
	}

	if(obj.type.value==3) 
	{
		if(!countChecked(obj, "target[]"))
		{ 
			document.getElementById('div_audience_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect?><?=$i_Notice_RecipientClass?></font>';
			error_no++;
			focus_field = "type";
		}
	}

    if(obj.type.value==4) 
    {
	    checkOptionAll(obj.elements["target[]"]);
	    
		if(obj.elements["target[]"].length==0)
		{ 
			document.getElementById('div_audience_err_msg').innerHTML = '<font color="red"><?=$i_frontpage_campusmail_choose?></font>';
			error_no++;
			focus_field = "type";
		}
	}
	if (compareDate(obj.DateStart.value, obj.DateEnd.value) > 0) 
	{
		document.getElementById('div_DateEnd_err_msg').innerHTML = '<font color="red"><?=$i_Homework_new_duedate_wrong?></font>';
		error_no++;
		if(focus_field=="")	focus_field = "DateEnd";
	}
	
	if(error_no>0)
	{
		eval("obj." + focus_field +".focus();");
		return false;
	}
	
}
//-->
</script>

<form name="form1" method="post" action="result.php">
<table class="form_table_v30">

<tr valign='top'>
	<td class='field_title'><span class='tabletextrequire'>*</span><?=$iDiscipline['Period']?></td>
	<td>
		<?=$iDiscipline['Period_Start']?> <?=$linterface->GET_DATE_PICKER("DateStart",$start_date)?>
		<?=$iDiscipline['Period_End']?> <?=$linterface->GET_DATE_PICKER("DateEnd",$end_date)?>
		<br><span id='div_DateEnd_err_msg'></span>
	</td>
</tr>


<tr valign='top'>
	<td class='field_title'><span class='tabletextrequire'>*</span><?=$iDiscipline['RankingTarget']?></td>
	<td><?=$type_selection?><br><span id="div_audience_err_msg"></span>
	<div id="div_selectAll" style="display:none"><input type='checkbox' name='alltarget' value='1' id='alltarget' onchange='selectAllTarget();'><label for='alltarget'><?=$Lang['Btn']['All']?></label></div>
	<div id="div_audience"></div>
	</td>
</tr>

<tr>
	<td class='field_title'><?=$i_Notice_SignStatus?></td>
	<td><?=$linterface->Get_Radio_Button("sign_status1", "sign_status", 1, $isChecked=1,"", $Lang['eNotice']['Signed_Unsigned'])?>
		<?=$linterface->Get_Radio_Button("sign_status2", "sign_status", 2, $isChecked=0,"", $Lang['eNotice']['UnsignedOnly'])?>
	</td>
</tr>
</table>

<?=$linterface->MandatoryField();?>
<div class="edit_bottom_v30">
	<?= $linterface->GET_ACTION_BTN($button_submit, "submit", ($type==4 ? "checkOption(this.form.elements['target[]']);" :"") ."return checkform(this.form);","submit2") ?>
	<?= $linterface->GET_ACTION_BTN($button_reset, "reset") ?>
	<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","cancelbtn") ?>
</div>	


</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
