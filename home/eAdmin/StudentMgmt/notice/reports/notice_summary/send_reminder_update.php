<?php
//using by: 

##################################
#
#	Date:	2016-09-09	Bill
#			Only show released notice
#			fixed: incorrect Not Signed Notice Number
#
#	Date:	2014-11-28	Omas
#			Created this file - for ajax sending push message to eClass App
#
##################################

@set_time_limit(21600);
@ini_set('max_input_time', 21600);
@ini_set('memory_limit', -1);

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");

intranet_auth();
intranet_opendb();

$luser = new libuser();
$libeClassApp = new libeClassApp();
$ldbsms 	= new libsmsv2();
$lnotice = new libnotice();

# DataReady
$StudentList = unserialize(rawurldecode($_POST['StudentList']));
$DateStart = $_POST['DateStart'];
$DateEnd = $_POST['DateEnd'];
$messageTitle = standardizeFormPostValue($_POST['PushMessageTitle']);
$messageContent = standardizeFormPostValue($_POST['MessageContent']);

## Check not signed > 0
$student_list = unserialize(rawurldecode($target_student));
$AcademicYearID = Get_Current_Academic_Year_ID();
$studentWithParentUsingAppAry = $luser->getStudentWithParentUsingParentApp();

##select student with not signed notice
$sql = "
			Select 
					a.StudentID as StudentID,
					count(*) - count(a.SignerID >0) as unsign_no,
					GROUP_CONCAT(
								IF(a.SignerID IS NULL, b.NoticeNumber, NULL)
									ORDER BY b.NoticeNumber ASC SEPARATOR ', '
					) as NoticeNum
			from 
					INTRANET_NOTICE_REPLY as a
					left join INTRANET_NOTICE as b on b.NoticeID = a.NoticeID 
					left join INTRANET_USER as u on u.UserID = a.StudentID 
					left join YEAR_CLASS_USER as f ON (f.UserID=u.UserID)
					left join YEAR_CLASS as g ON (f.YearClassID=g.YearClassID)
			where 
					g.AcademicYearID='".$AcademicYearID."' and 
					a.StudentID in (".implode(',',$StudentList).") and  
					b.RecordStatus = 1 and
					not(b.DateEnd<'".$DateStart."' or b.DateStart>'".$DateEnd."') and
					b.DateStart <= now()
			group by 
					a.StudentID  
			having 
					(count(*) - count(a.SignerID >0))>0
		";

$sendTargetAry = $lnotice->ReturnResultSet($sql);
$sendTargetNum = count($sendTargetAry);
//$sendTargetIdAry = array();
//for ($i = 0; $i < $sendTargetNum; $i++){
//	$sendTargetIdAry[$i] = $sendTargetAry[$i]['StudentID'];
//}
$sendTargetIdAry = Get_Array_By_Key($sendTargetAry, 'StudentID');


## Select students whose parent using App
$TargetUsers = $sendTargetIdAry;
$numOfUser = count($TargetUsers);
$lu = new libuser('', '', $TargetUsers);

for ($i = 0; $i < $numOfUser; $i++) {
	$studentId = $TargetUsers[$i];
	//$lu = new libuser($studentId);
	
	$lu->loadUserData($studentId);
	
	$appParentIdAry = $lu->getParentUsingParentApp($studentId);
	if (in_array($studentId, $studentWithParentUsingAppAry)) {
		$studentIds[] = $studentId;
	}

}

$isPublic = "N";
$parentStudentAssoAry = BuildMultiKeyAssoc($luser->getParentStudentMappingInfo($studentIds), 'ParentID', $IncludedDBField=array('StudentID'), $SingleValue=1, $BuildNumericArray=1);

## Build Data of unsigned notice for string replace
$TargetMessageDataAry = $sendTargetAry;
$TargetMessageDataAssoAry = BuildMultiKeyAssoc($TargetMessageDataAry, 'StudentID');

## Build Data for send message
$appType = $eclassAppConfig['appType']['Parent'];
$sendTimeMode = "";
$sendTimeString = "";
$individualMessageInfoAry = array();
$i = 0;
if(!empty($studentIds)){
	foreach ($studentIds as $studentId) {
		$appParentIdAry = $luser->getParentUsingParentApp($studentId);
	
		$_individualMessageInfoAry = array();
		foreach ($appParentIdAry as $parentId) {
			$_individualMessageInfoAry['relatedUserIdAssoAry'][$parentId] = (array)$studentId;
		}
		$_individualMessageInfoAry['messageTitle'] = $messageTitle;
		$_individualMessageInfoAry['messageContent'] = $ldbsms->replace_content($studentId, $messageContent, '');
	
		$_individualMessageInfoAry['messageContent'] = str_replace('($Notice_Num)',$TargetMessageDataAssoAry[$studentId]['NoticeNum'],$_individualMessageInfoAry['messageContent']);
		$_individualMessageInfoAry['messageContent'] = str_replace('($Num_Of_Unsigned_Notice)',$TargetMessageDataAssoAry[$studentId]['unsign_no'],$_individualMessageInfoAry['messageContent']);
	
		$individualMessageInfoAry[$i] = $_individualMessageInfoAry;
	
		$i++;
	}
}

$NumOfRecepient = $i;
## send message
$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $messageTitle, 'MULTIPLE MESSAGES', $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString);

//$msg = ($notifyMessageId > 0)? $Lang['AppNotifyMessage']['ReturnMsg']['SendSuccess']:$Lang['AppNotifyMessage']['ReturnMsg']['SendFail'];

$sent =  ($notifyMessageId > 0) ? 1 : 0;
$statisticsAry = $libeClassApp->getPushMessageStatistics($notifyMessageId);
$statisticsAry = $statisticsAry[$notifyMessageId];

if($sent == 1){
	echo "<font color='#DD5555'>".str_replace("[SentTotal]", $statisticsAry['numOfSendSuccess'], $Lang['AppNotifyMessage']['eNotice']['send_result']). "</font>";
}
else{
	$ErrorMsg = $Lang['AppNotifyMessage']['eNotice']['send_failed'];
	if ($statisticsAry['numOfRecepient'] == 0) {
		$ErrorMsg .= $Lang['AppNotifyMessage']['eNotice']['send_result_no_parent'];
	}
	echo "<font color='red'>".$ErrorMsg."</font> <font color='grey'>(".$statisticsAry['numOfRecepient']."-".$statisticsAry['numOfSendSuccess'].")</font>";
}
intranet_closedb();


?>