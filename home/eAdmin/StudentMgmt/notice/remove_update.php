<?php
# using: Bill

########################################
#
#   Date:   2020-04-16  Bill    [2020-0310-1051-05235]
#           Special notice > access checking for teacher    ($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin'])
#
#	Date:   2019-04-30  Bill
#           - prevent SQL Injection + Command Injection
#
#   Date:   2018-10-10  Bill    [2018-1008-1030-09073]
#           - for push message notification to PIC & Class Teacher
#           fixed: cannot delete old scheduled notification for PIC / Class Teacher
#
#	Date:	2015-10-28	Bill	[2015-0416-1040-06164]
#			support merge notice content
#
#	Date:	2015-06-08 Roy	> ej.5.0.5.6.1
#			use overrideExistingScheduledPushMessageFromModule() to remove not yet sent scheduled push message
#
#	Date:	2014-06-23 YatWoon	> ip.2.5.5.8.1
#			Improved: Revised system return message method [Case#Q63447]
#
########################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");

intranet_auth();
intranet_opendb();

$lu = new libuser($UserID);
$libeClassApp = new libeClassApp();

$NoticeID = $NoticeIDArr;
$NoticeID = IntegerSafe($NoticeID);
for($i=0; $i<sizeof($NoticeID); $i++)
{
        $thisNoticeID = $NoticeID[$i];
        $lnotice = new libnotice($thisNoticeID);

        // [2020-0310-1051-05235] Special notice > access checking for teacher
        $allowTeacherAccess = true;
        if($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin'] && $lnotice->SpecialNotice)
        {
            if($lu->isTeacherStaff()) {
                $allowTeacherAccess = $_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $lnotice->isNoticePIC($thisNoticeID);
            }
        }
        
        $lf = new libfilesystem();

        //if ($lnotice->disabled || !$lnotice->hasRemoveRight())
        if ($lnotice->disabled || !$lnotice->hasRemoveRight() || !$allowTeacherAccess)
        {
			include_once($PATH_WRT_ROOT."includes/libaccessright.php");
			$laccessright = new libaccessright();
			$laccessright->NO_ACCESS_RIGHT_REDIRECT();
			exit;
        }
        $NoticeAttachment = OsCommandSafe($lnotice->Attachment);
        
        $sql = "DELETE FROM INTRANET_NOTICE_REPLY WHERE NoticeID = '$thisNoticeID'";
        $lnotice->db_db_query($sql);
        
        # Grab attachment
        // $path = "$file_path/file/notice/".$lnotice->Attachment;
        $path = "$file_path/file/notice/".$NoticeAttachment;
        if (trim($lnotice->Attachment) != "" && is_dir($path))
        {
    		  if($bug_tracing['notice_attachment_log']) {
                 $temp_log_filepath = "$file_path/file/log_notice_attachment.txt";
                 $temp_time = date("Y-m-d H:i:s");
                 $temp_user = $UserID;
                 $temp_page = 'notice_remove_update.php';
                 $temp_action="remove path:".$path;
                 $temp_content = get_file_content($temp_log_filepath);
                 $temp_logentry = "\"$temp_time\",\"$temp_user\",\"$temp_page\",\"$temp_action\"\n";
                 $temp_content .= $temp_logentry;
                 write_file_content($temp_content, $temp_log_filepath);
                 $temp_cmd = "mv $path ".$path."_bak";
            	 exec($temp_cmd);
    		 }
    		 else {
        		$lf->lfs_remove($path);
        	 }
        }
        
        // [2015-0416-1040-06164] Grab merge CSV file
        $merge_path = "$file_path/file/mergenotice/".ceil($thisNoticeID/10000)."/".$thisNoticeID;
        if(is_dir($merge_path) && trim($lnotice->MergeFile)!= "")
        {
        	$merge_path .= "/".$lnotice->MergeFile;
        	$lf->lfs_remove($merge_path);
        }
        
        # remove scheduled push message
        $libeClassApp->overrideExistingScheduledPushMessageFromModule("eNotice", $thisNoticeID, $newNotifyMessageId='');
        $libeClassApp->overrideExistingScheduledPushMessageFromModule("eNoticeS", $thisNoticeID, $newNotifyMessageId='');       // [2018-1008-1030-09073]
        
        # Add deletion log
        include_once($PATH_WRT_ROOT."includes/liblog.php");
		$lg = new liblog();
		$sql = "select * FROM INTRANET_NOTICE WHERE NoticeID = '$thisNoticeID'";
		$result = $lnotice->returnArray($sql);
		foreach($result as $k => $d)
		{
			$this_id = $thisNoticeID;
			
			$RecordDetailAry = array();
			$RecordDetailAry['DateStart'] = substr($d['DateStart'],0,10);
			$RecordDetailAry['DateEnd'] = substr($d['DateEnd'],0,10);
			$RecordDetailAry['IssueUserID'] = $d['IssueUserID'];
			$RecordDetailAry['IssueUserName'] = $d['IssueUserName'];
			$RecordDetailAry['NoticeNumber'] = $d['NoticeNumber'];
			$RecordDetailAry['Title'] = $d['Title'];
			$RecordDetailAry['NoticeType'] = "Parent Notice";
			
			$lg->INSERT_LOG('eNotice', 'eNotice', $RecordDetailAry, 'INTRANET_NOTICE', $thisNoticeID);
		}
		
        $sql = "DELETE FROM INTRANET_NOTICE WHERE NoticeID = '$thisNoticeID'";
        $lnotice->db_db_query($sql);
}

intranet_closedb();

if($backUrl=="paymentNotice") {
	$location = "paymentNotice.php";
}
else {
	$location = "index.php";
	if(intval($status) >= 4) {
		$addCond = "&pendingApproval=1";
	}
}

//header("Location: $location?noticeType=$noticeType&status=$status&year=$year&month=$month&xmsg=". $Lang['General']['ReturnMessage']['DeleteSuccess']);
header("Location: $location?noticeType=$noticeType&status=$status&year=$year&month=$month&xmsg=DeleteSuccess$addCond");
?>