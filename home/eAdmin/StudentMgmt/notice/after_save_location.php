<?php
# using: Bill

########################################
#
#   Date:   2020-11-10  Bill    [EJ DM#1444]
#           redirect to paymentNotice.php for payment notice
#
########################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

$location = '';
if($_GET['noticeType']=='S'){
	$location .= 'student_notice/';
} else {
	// do nothing
}

// [EJ DM#1444]
if ($_GET['noticeType'] == 'PY') {
    $location .= 'payment_notice/paymentNotice.php';
} else {
    $location .= 'index.php';
}

if($_GET['status'] >= 4) {
	$_GET['pendingApproval'] = 1;
}

unset($_GET['noticeType']);
$count = 0;
$numOfGet = count($_GET);
foreach((array)$_GET as $_key => $_value){
	if($count==0){
		$param = '?';
	}
	$param .= $_key.'='.$_value;
	
	$count++;
	if($count!=$numOfGet){
		$param .= '&';
	}
}
//$location .= 'index.php'.$param;
$location .= $param;

header("Location: $location");
?>