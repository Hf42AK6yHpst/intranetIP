<?php
// Using : Bill

#######################################
#
#   Date:   2020-09-23  Ray
#           add classlist filter
#
#   Date:   2020-09-22  Bill    [2020-0922-1006-24206]
#           fixed: cannot display whole notice title if attachment file name too lang - break by chars
#
#   Date:   2020-08-25  Bill    [2020-0821-1028-21206]
#           fixed cannot access urls with 'https://' due to jquery mobile event listeners
#
#   Date:   2020-04-15 (Bill)   [2020-0310-1051-05235]
#           hide notice details ($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin'] && $_GET['showTitleOnly'] == 1)
#
#   Date:   2019-09-05  Bill    [2019-0902-1654-50207]
#           fixed cannot access urls with '#' due to jquery mobile event listeners
#
#	Date:	2019-05-10 (Bill)
#           - prevent SQL Injection
#           - apply intranet_auth()
#
#   Date:   2019-03-06  Bill
#           for auth checking - get token from returnSimpleTokenByDateTime()     ($special_feature['download_attachment_add_auth_checking'])
#
#######################################

$PATH_WRT_ROOT = "../../../../../";
$PATH_WRT_ROOT_NEW = "../../../../..";

//set language
@session_start();

include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();

$parLang = isset($_GET['parLang']) ? $_GET['parLang'] : '';
$_SESSION['intranet_hardcode_lang'] = $leClassApp_init->getPageLang($parLang);
//$_SESSION['UserID'] = $CurUserID;

//switch (strtolower($parLang)) {
//	case 'en':
//		$intranet_hardcode_lang = 'en';
//		break;
//	default:
//		$intranet_hardcode_lang = 'b5';
//}

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_hardcode_lang.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.".$_SESSION['intranet_session_language'].".php");

### Handle SQL Injection + XSS [START]
$NoticeID = IntegerSafe($NoticeID);
$CurUserID = IntegerSafe($CurUserID);
### Handle SQL Injection + XSS [END]

$UserID = $CurUserID;
$_SESSION['UserID'] = $CurUserID;
$studens_list = "";

$delimiter = "###";
$keyStr = $special_feature['ParentApp_PrivateKey'].$delimiter.$NoticeID.$delimiter.$CurUserID;
if($token != md5($keyStr)) {
	include_once($PATH_WRT_ROOT."lang/lang.en.php");
	echo $i_general_no_access_right	;
	exit;
}

# Generate token for auth checking
$dl_auth_token = '';
if($special_feature['download_attachment_add_auth_checking'])
{
    $dl_auth_token = returnSimpleTokenByDateTime();
    
//     unset($_SESSION['dlToken']);
//    
//     $dl_auth_token = md5($keyStr.$delimiter.time());
}

intranet_auth();
intranet_opendb();

$lnotice = new libnotice($NoticeID);
$lu = new libuser($UserID);

$linterface = new interface_html("popup6.html");

$sql = "select TargetType from INTRANET_NOTICE where NoticeID ='".$NoticeID."'";
$result = $lnotice->returnArray($sql);

$TargetType = trim($result[0][0]);
if($TargetType == 'P') {
	$Target = $Lang['eClassApp']['eNoticeS']['Parent-signedNotice'];
} else if($TargetType == 'S') {
	$Target = $Lang['eClassApp']['eNoticeS']['Student-signedNotice'];
}

# modified by Kelvin Ho 2008-11-12 for ucc
if($lnotice->Module == '')
{
    $attachment = $lnotice->displayAttachment($targetBlank=true, $dl_auth_token);
}
else
{
	if(strtoupper($lnotice->Module) == "PAYMENT") {
	    $attachment = $lnotice->displayAttachment($targetBlank=true, $dl_auth_token);
	} else {
		$attachment = $lnotice->displayModuleAttachment();
	}
}

// [2016-0728-1454-09073] use getNoticeContent() to get display content
// $this_Description = $lnotice->Description;
$this_Description = $lnotice->getNoticeContent();

//$sql1 = "SELECT
//		    ycu.YearClassID, yc.ClassTitleEN , yc.ClassTitleB5
//        FROM 
//		    INTRANET_NOTICE_REPLY as a  
//			Inner JOIN YEAR_CLASS_USER as ycu ON a.StudentID = ycu.UserID
//			Inner JOIN YEAR_CLASS yc on ycu.YearClassID = yc.YearClassID
//		    Inner JOIN YEAR_CLASS_TEACHER yct ON yc.YearClassID=yct.YearClassID and yct.UserID='".$CurUserID."' 		    		
//	    WHERE 
//		    a.NoticeID ='".$NoticeID."'
//		    AND yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
//		GROUP BY ycu.YearClassID";
		
//$sql3 = "select * from INTRANET_NOTICE_PIC as p where p.NoticeID='".$NoticeID."' and p.PICUserID='".$CurUserID."'";
//$result3 = $lnotice->returnArray($sql3);

$hideStudentList = "";
if($type == 3) {
	$hideStudentList='style="display: none;"';
}
if($type == 0)
{
    $sql1 = "SELECT
                 ycu.YearClassID, yc.ClassTitleEN , yc.ClassTitleB5
            FROM 
                INTRANET_NOTICE_REPLY as a  
                INNER JOIN YEAR_CLASS_USER as ycu ON a.StudentID = ycu.UserID
                INNER JOIN YEAR_CLASS yc ON ycu.YearClassID = yc.YearClassID
                INNER JOIN YEAR y ON y.YearID = yc.YearID			
            WHERE 
                a.NoticeID = '".$NoticeID."' AND yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
            GROUP BY ycu.YearClassID
            ORDER BY y.Sequence, yc.Sequence ";
}
else if($type == 1)
{
    $sql1 = "SELECT
			    ycu.YearClassID, yc.ClassTitleEN , yc.ClassTitleB5
	        FROM
			    INTRANET_NOTICE_REPLY as a
			    INNER JOIN YEAR_CLASS_USER as ycu ON a.StudentID = ycu.UserID
			    INNER JOIN YEAR_CLASS yc ON ycu.YearClassID = yc.YearClassID
			    INNER JOIN YEAR y ON y.YearID = yc.YearID		
		    WHERE
		    	a.NoticeID = '".$NoticeID."' AND yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
		    GROUP BY ycu.YearClassID
		    ORDER BY y.Sequence, yc.Sequence ";
}
else if($type == 2)
{
	$sql1 = "SELECT
                ycu.YearClassID, yc.ClassTitleEN , yc.ClassTitleB5
            FROM 
                INTRANET_NOTICE_REPLY as a  
                INNER JOIN YEAR_CLASS_USER as ycu ON a.StudentID = ycu.UserID
                INNER JOIN YEAR_CLASS yc ON ycu.YearClassID = yc.YearClassID
                INNER JOIN YEAR y ON y.YearID = yc.YearID			
                INNER JOIN YEAR_CLASS_TEACHER yct ON yc.YearClassID = yct.YearClassID AND yct.UserID = '".$CurUserID."' 		    		
            WHERE 
                a.NoticeID = '".$NoticeID."' AND yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
            GROUP BY ycu.YearClassID
            ORDER BY y.Sequence, yc.Sequence";
}
$result1 = $lnotice->returnArray($sql1);

if(count($result1) != 0) {
	$studens_list .="<ul data-role='listview' data-inset='true' data-icon='false' >";
}

$classlist_array = array();
for($i=0; $i<count($result1); $i++)
{
    $classid_name = 'class_'.$result1[$i][0];
// 	if($intranet_hardcode_lang=="b5"){
// 		$className = $result1[$i][2];
// 	}else{
// 		$className = $result1[$i][1];
// 	}
	$className = Get_Lang_Selection($result1[$i][2], $result1[$i][1]);
	
	$classlist_array[] = array($result1[$i][0], $className);

	$studens_list .="<li data-role='list-divider' class='$classid_name'>".$className."</li>";
//	$sql2 = "SELECT
//			    iu.UserID, iu.EnglishName, iu.ChineseName,a.RecordStatus, iu.ClassNumber
//	        FROM
//			    INTRANET_NOTICE_REPLY as a
//			    Inner JOIN INTRANET_USER as iu ON a.StudentID = iu.UserID
//			    Inner JOIN YEAR_CLASS_USER as ycu ON iu.UserID = ycu.UserID
//				Inner JOIN YEAR_CLASS yc on ycu.YearClassID = yc.YearClassID
//			    Inner JOIN YEAR_CLASS_TEACHER yct ON yc.YearClassID=yct.YearClassID and yct.UserID='".$CurUserID."'	    
//		    WHERE
//			    a.NoticeID ='".$NoticeID."' and  ycu.YearClassID = '".$result1[$i][0]."'
//			ORDER BY 
//			    iu.ClassNumber ASC";
	if($type == 0)
	{
        $sql2 = "SELECT
                    iu.UserID, iu.EnglishName, iu.ChineseName,a.RecordStatus, iu.ClassNumber
                FROM
                    INTRANET_NOTICE_REPLY as a
                    INNER JOIN INTRANET_USER as iu ON a.StudentID = iu.UserID
                    INNER JOIN YEAR_CLASS_USER as ycu ON iu.UserID = ycu.UserID
                    INNER JOIN YEAR_CLASS yc ON ycu.YearClassID = yc.YearClassID
                WHERE
                    a.NoticeID = '".$NoticeID."' AND ycu.YearClassID = '".$result1[$i][0]."'
                ORDER BY 
                    iu.ClassNumber ASC";
	}
	else if($type == 1)
	{
		$sql2 = "SELECT
			        iu.UserID, iu.EnglishName, iu.ChineseName,a.RecordStatus, iu.ClassNumber
                FROM
                    INTRANET_NOTICE_REPLY as a
                    INNER JOIN INTRANET_USER as iu ON a.StudentID = iu.UserID
                    INNER JOIN YEAR_CLASS_USER as ycu ON iu.UserID = ycu.UserID
                    INNER JOIN YEAR_CLASS yc ON ycu.YearClassID = yc.YearClassID	    
                WHERE
                    a.NoticeID = '".$NoticeID."' AND ycu.YearClassID = '".$result1[$i][0]."'
                ORDER BY 
                    iu.ClassNumber ASC";
	}
	else if($type == 2)
	{
		$sql2 = "SELECT
                    iu.UserID, iu.EnglishName, iu.ChineseName,a.RecordStatus, iu.ClassNumber
                FROM
                    INTRANET_NOTICE_REPLY as a
                    INNER JOIN INTRANET_USER as iu ON a.StudentID = iu.UserID
                    INNER JOIN YEAR_CLASS_USER as ycu ON iu.UserID = ycu.UserID
                    INNER JOIN YEAR_CLASS yc ON ycu.YearClassID = yc.YearClassID
                    INNER JOIN YEAR_CLASS_TEACHER yct ON yc.YearClassID = yct.YearClassID AND yct.UserID = '".$CurUserID."'			    
                WHERE
                    a.NoticeID = '".$NoticeID."' AND ycu.YearClassID = '".$result1[$i][0]."'
                ORDER BY 
                    iu.ClassNumber ASC";
	}
	$result2 = $lnotice->returnArray($sql2);
	
	for($j=0; $j<count($result2); $j++)
	{
// 		if($intranet_hardcode_lang=="b5"){
// 			$studentName = $result2[$j][2];
// 		}else{
// 			$studentName = $result2[$j][1];
// 		}
		$studentName = Get_Lang_Selection($result2[$j][2], $result2[$j][1]);
		
		$photo = $lu->GET_OFFICIAL_PHOTO_BY_USER_ID($result2[$j][0]);
		if($result2[$j][3] == 0)
		{
			$studens_list .= "<li class='unsigned $classid_name'><a href='javascript:show_notice_content(".$result2[$j][0].")' style='padding-top:5px;padding-bottom:5px;'>
					            <table width='100%'>
					            <tr>
					                <td width='40px'><img src='".$photo[1]."'  style='width:32px;height:40px;' /></td>
					         		<td width='20px' align='center'>".$result2[$j][4]."</td>
					            	<td><span style='font-size:20px;font-weight:bold;color:red;padding-left:10px;'>".$studentName."</span></td>
                                    <td></td>
                                </tr>
                                </table>
                            </a></li>";
		}
		else if($result2[$j][3] == 2)
		{
			$studens_list .= "<li class='signed $classid_name'><a href='javascript:show_notice_content(".$result2[$j][0].")' style='padding-top:5px;padding-bottom:5px;'>
					            <table width='100%'>
					            <tr>
					                <td width='40px'><img src='".$photo[1]."'  style='width:32px;height:40px;' /></td>
					         		<td width='20px' align='center'>".$result2[$j][4]."</td>
					            	<td><span style='font-size:20px;font-weight:bold;padding-left: 10px;'>".$studentName."</span></td>
                                    <td align='right'><img src='".$PATH_WRT_ROOT_NEW.$image_path."/".$LAYOUT_SKIN."/eClassApp/teacherApp/eNotice_S/teacher_tick.png'  style='width:30px;height:30px;padding-right:10px' /></td>
                                </tr>
                                </table>
                            </a></li>";
		}
	}
}
if(count($result1) != 0) {
	$studens_list .="</ul>";
}

# Store token to session
// if($special_feature['download_attachment_add_auth_checking'] && $dl_auth_token != '')
// {
//     $_SESSION['dlToken'] = $dl_auth_token;
// }

$linterface->LAYOUT_START();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.11.1.min.js"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.js"></script>
<script>
	jQuery(document).ready( function() {

	});
	
    function show_notice_content(StudentID){
    	var div = document.getElementById("div1");
        var input = document.createElement("input");
        input.type = "hidden";
        input.id = "StudentID";
        input.name = "StudentID";
        input.value = StudentID;
        div.appendChild(input);
        
        document.form1.action = "notice_content.php";
        document.form1.submit();
    }
    
    function select(){
        var selected_class_value = document.getElementById("select_class").value;
        var class_select = '';
        if(selected_class_value == 'all') {
            $("li[class^='class_'],li[class*=' class_']").show();
        } else {
            class_select = ".class_"+selected_class_value;
            $("li[class^='class_'],li[class*=' class_']").hide();
            $(class_select+".ui-li-divider").show();
        }

    	var selected = document.getElementById("select_students");
    	var selectedID = selected.selectedIndex;
    	if(selectedID == 0){
        	$(".signed"+class_select).show();
        	$(".unsigned"+class_select).show();
        }
    	else if(selectedID == 1){
           	$(".signed"+class_select).show();
        	$(".unsigned"+class_select).hide();
    	}
    	else if(selectedID == 2){
           	$(".signed"+class_select).hide();
        	$(".unsigned"+class_select).show();
    	}
    }
</script>

<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.css">
<style type="text/css">
.tabletext {
	font-size: 16px;
}
.tabletext2 {
	font-size: 16px;
}
.tabletextremark {
	font-size: 16px;
}
</style>

<script>
    $(function(){
        $('div#descriptionDiv a.ui-link').filter(function() {
            return ($(this).attr('href').indexOf('https://') != -1 || $(this).attr('href').indexOf('#') != -1);
        })
        .attr('rel', 'external');
    });
</script>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no" />
</head>

	<body>
    <?php if($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin'] && $_GET['showTitleOnly'] == 1) { ?>
        <div data-role="page" id="body" style = "background-color:#FFFFFF">
            <table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr valign='top'>
                                <td bgcolor='#E7E7E7' style="padding:10px;">
                                    <span class='tabletext'>
                                        <h1><?=(($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin'] && $lnotice->SpecialNotice) ? "<span style='color:red'>* </span>" : "")?><?=$lnotice->Title?></h1>
                                    </span>
                                </td>
                            </tr>
                            <tr valign='top'>
                                <td bgcolor='#FFFFFF' style="padding:10px;font-size:18px;" >
                                    <table border="0" cellpadding="0" cellspacing="0" style="width:100%; font-size:18px;">
                                        <tr>
                                            <td style="vertical-align:top; font-size:18px;">
                                                <?=$lnotice->NoticeNumber?>
                                            </td>
                                            <td style="width:130px; text-align:right; vertical-align:bottom; font-size:18px;">
                                                <img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_date.png" align="absmiddle" style="width:25px; height:25px;" />
                                                <span style="vertical-align:bottom;"><?=$lnotice->DateStart?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align:top; font-size:18px;">(<?=$Target?>)</td>
                                            <td style="text-align:right; vertical-align:bottom; font-size:18px;">
                                                <img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_deadline.png" align="absmiddle" style="width:25px; height:25px;" />
                                                <span style="vertical-align:bottom;"><font color="red"><?=$lnotice->DateEnd?></font></span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    <?php } else { ?>
        <div data-role="page" id="body" style = "background-color:#FFFFFF">
            <form id="form1" name="form1" method="get" >
                <table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr valign='top'>
                                    <td bgcolor='#E7E7E7' style="padding:10px;">
                                        <span class='tabletext'>
                                            <h1><?=(($sys_custom['eNotice']['SpecialNoticeForPICAndAdmin'] && $lnotice->SpecialNotice) ? "<span style='color:red'>* </span>" : "")?><?=$lnotice->Title?></h1>
                                        </span>
                                    </td>
                                </tr>
                                <tr valign='top'>
                                    <td bgcolor='#FFFFFF' style="padding:10px;font-size:18px;" >
                                        <table border="0" cellpadding="0" cellspacing="0" style="width:100%; font-size:18px;">
                                            <tr>
                                                <td style="vertical-align:top; font-size:18px;">
                                                    <?=$lnotice->NoticeNumber?>
                                                </td>
                                                <td style="width:130px; text-align:right; vertical-align:bottom; font-size:18px;">
                                                    <img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_date.png" align="absmiddle" style="width:25px; height:25px;" />
                                                    <span style="vertical-align:bottom;"><?=$lnotice->DateStart?></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align:top; font-size:18px;">(<?=$Target?>)</td>
                                                <td style="text-align:right; vertical-align:bottom; font-size:18px;">
                                                    <img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_deadline.png" align="absmiddle" style="width:25px; height:25px;" />
                                                    <span style="vertical-align:bottom;"><font color="red"><?=$lnotice->DateEnd?></font></span>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <div data-role="collapsible" data-collapsed="true" style="font-size:18px">
                    <h1><?=$Lang['General']['Misc']?></h1>
                    <div class="ui-grid-a">
                        <div id="descriptionDiv" class="ui-block-a" style="width:100%; overflow-x: auto; overflow-y: hidden;">
                            <span><strong><?=$i_Notice_Description?></strong></span>
                            <br>
                            <span><?=str_replace("&nbsp;&nbsp;", "&nbsp; ", htmlspecialchars_decode($this_Description))?></span>
                        </div>
                        <? if ($attachment != "") { ?>
                            <div class="ui-block-a" style="width:100%; word-break:break-all;">
                                <br>
                                <span><strong><?=$i_Notice_Attachment?></strong></span>
                                <br>
                                <span><?=$attachment?></span>
                            </div>
                        <? } ?>
                    </div>
                </div>

                <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor='#E7E7E7' <?=$hideStudentList?> >
                    <tr valign='top'>
                        <td bgcolor='#E7E7E7' style='padding-top:10px;padding-bottom:10px;padding-left:15px;' align="left">
                            <span style="font-size: 20px;"><?=$Lang['eClassApp']['eNoticeS']['SignResults']?></span>
                        </td>
                        <td width="100px" align= "right" style="padding-right:0.5em;">
                            <select name="select_class" id="select_class" data-mini="true" onchange="select()">
                                <option value="all" id="all"><?=$Lang['SysMgr']['FormClassMapping']['All']['Class']?></option>
                                <?php foreach($classlist_array as $temp) { ?>
                                <option value="<?=$temp[0]?>"><?=$temp[1]?></option>
                                <?php } ?>
                            </select>
                        </td>
                        <td width="100px" align= "right" style="padding-right:0.5em;">
                            <select name="select_students" id="select_students" data-mini="true" onchange="select()">
                                <option value="all" id="all"><?=$Lang['eClassApp']['eNoticeS']['All']?></option>
                                <option value="signed" id="signed"><?=$Lang['eClassApp']['eNoticeS']['Signed']?></option>
                                <option value="unsigned" id="unsigned"><?=$Lang['eClassApp']['eNoticeS']['Unsigned']?></option>
                            </select>
                        </td>
                    </tr>
                </table>

                <div <?=$hideStudentList?>>
                    <?=$studens_list?>
                </div>

                <input type="hidden" id="NoticeID" name="NoticeID" value="<?=$NoticeID?>">
                <input type="hidden" id="CurUserID" name="CurUserID" value="<?=$CurUserID?>">
                <input type="hidden" id="token" name="token" value="<?=$token?>">
                <input type="hidden" id="parLang" name="parLang" value="<?=$parLang?>">
                <input type="hidden" id="type" name="type" value="<?=$type?>">

                <div id="div1"></div>
            </form>
        </div>
    <?php } ?>
	</body>
</html>

<?php
intranet_closedb();
?>