<?php
function generate_radar_highchart_diagram($StudentID, $SubjectArray, $info, $sb, $lang){
 $subjectDisplay = array(
     'Abbr' => 'ABBR',
     'Desc' => 'DES',
     'ShortForm' => 'SNAME'
 );
 $thisSubjectInfo = $info;
 $thisLang = ($lang=='en') ? 'EN_' : 'CH_';
 $SubjectArr = array();
 $SubjectArr['id'] = $StudentID;
 $SubjectArr['name'] = array();
 $SubjectArr['average'] = array();
 foreach($SubjectArray as $subjectID => $average){
     array_push($SubjectArr['name'], $thisSubjectInfo[$subjectID][$thisLang.$subjectDisplay[$sb]]);
     array_push($SubjectArr['average'], $average);
 }
 return $SubjectArr;
}
?>