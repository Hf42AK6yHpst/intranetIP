<?php
// using: 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Statistics_RubricsReport";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_module.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
//include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");

//$lreportcard = new libreportcardrubrics_custom();
$lreportcard = new libreportcardrubrics();
$lreportcard_module = new libreportcardrubrics_module();
$lreportcard_topic = new libreportcardrubrics_topic();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$lreportcard_ui = new libreportcardrubrics_ui();
$linterface = new interface_html();

### prepare tmp folder
$fs = new libfilesystem();
$folder_prefix = $intranet_root."/file/reportcard_rubrics/tmp_chart/";

### Clear the tmp png folder
if (file_exists($folder_prefix))
	$fs->folder_remove_recursive($folder_prefix);

### Create the tmp png folder if not exist
//if (!file_exists($folder_prefix))
//{
	$fs->folder_new($folder_prefix);
	chmod($folder_prefix, 0777);
//}

### Print Button
$PrintBtn = $linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();", "submit2");

### Gen Flash Chart
$chartWidth = "800";
$chartHeight = "600";
$StudentMarksheetScoreArr = $lreportcard->Get_Student_Marksheet_Score($SubjectIDArr, $StudentIDArr);

$numOfModule = count($ModuleID);
$numOfSubject = count($SubjectIDArr);

foreach($StudentIDArr as $StudentID)
{
	$StudentInvolvedRubrics = array();
	
	$RubricsCount = array();
	$StudentStudyTopicInfoArr = $lreportcard_topic->Get_Student_Study_Topic_Info($ModuleID, '', '', $StudentID);

	$SubjectRubricsTotal = array();
	for ($i=0; $i<$numOfModule; $i++)
	{
		$thisModuleID = $ModuleID[$i];
		
		for ($j=0; $j<$numOfSubject; $j++)
		{
			$thisSubjectID = $SubjectIDArr[$j];
			$StudentInvolvedSubject[$thisSubjectID] =  1;
			# A-Za-z
			$SubjectCode[$thisSubjectID] = chr(65+$j>90?97+($j%26):65+$j);
			
			# Count the Student Studied Topic only
			foreach((array)$StudentStudyTopicInfoArr[$StudentID][$thisModuleID][$thisSubjectID] as $thisTopicID => $thisInfoArr)
			{
				# Record the Mark Count of each Rubrics Items
				$thisRubricsItemID = $StudentMarksheetScoreArr[$StudentID][$thisSubjectID][$thisTopicID][$thisModuleID]['RubricsItemID'];
				if(empty($thisRubricsItemID)) continue; // skip empty mark  
				
				$RubricsCount[$thisRubricsItemID][$thisSubjectID]++;
				$SubjectRubricsTotal[$thisSubjectID]++;
				$SubjectMarkDisplayArr[$StudentID]["RubricsCount"][$thisRubricsItemID][$thisSubjectID]++;
				$StudentInvolvedRubrics[$thisRubricsItemID] = 1;
			}
		}
	}
	
	foreach($RubricsCount as $RubricsItemID => $SubjectRubricsCount)
	{
		foreach($SubjectRubricsCount as $SubjectID => $RubricsCount)
		{
			$SubjectMarkDisplayArr[$StudentID]["RubricsPercent"][$RubricsItemID][$SubjectID] = $RubricsCount/$SubjectRubricsTotal[$SubjectID]*100;
		}
	}
	
	$SubjectMarkDisplayArr[$StudentID]["InvolvedRubricsItems"] = array_keys($StudentInvolvedRubrics);
	$SubjectMarkDisplayArr[$StudentID]["InvolvedSubject"] = array_keys($StudentInvolvedSubject);
}

$FlashChartArr = $lreportcard_ui->Get_Statistics_RubricsReport_Flash_Chart($SubjectMarkDisplayArr, $SubjectDisplay, $YearID,$SubjectCode);
$StatisticTable = $lreportcard_ui->Get_Statistics_RubricsReport_Statistic_Table($SubjectMarkDisplayArr, $SubjectDisplay,$SubjectCode);

$k=0;
foreach((array)$FlashChartArr as $StudentID => $FlashChart)
{
	$chartArr[] = $FlashChart;
	$FlashDiv .= '<table cellpadding=2 cellspacing=2 align=center>';
		$FlashDiv .= '<tr><td><div id="div'.$k.'parent"><div id="div'.$k.'"></div></div></td></tr>';
		$FlashDiv .= '<tr><td><div id="StatTable'.$k.'" style="text-align:center">'.$StatisticTable[$StudentID].'</div></td></tr>';
	$FlashDiv .= '</table><br><br>';
	$k++;
}
//echo $lreportcard_ui->Include_JS_CSS(array('rubrics_css'));
//echo $lreportcard_ui->Include_Template_CSS();
include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/print_header.php");
?>

<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic_201301_in_use/js/json/json2.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic_201301_in_use/js/swfobject.js"></script>	
<script type="text/javascript">

	function ofc_ready(){
		//alert('ofc_ready');
	}

	function open_flash_chart_data(id){
		return JSON.stringify(dataArr[id]);
	}

	function findSWF(movieName) {
	  if (navigator.appName.indexOf("Microsoft")!= -1) {
		return window[movieName];
	  } else {
		return document[movieName];
	  }
	}

	/*
	function jsonLoaded(id){
		//alert("json loaded and id: "+id);
	}	

	function onAutoReturnImgBinary(bmpBytStr){
		//alert("image binary string is automatically returned: "+bmpBytStr);
	}
	*/

	function onAutoPostImgBinary(id){
		//alert("image binary string is automatically posted and flash id: "+id);
		var swfDIV = document.getElementById("div"+id+"parent");
		//var swfDIV = document.getElementById("img"+id);
		//alert("swfDIV: "+swfDIV+" / width: "+swfDIV.width+" / height: "+swfDIV.height);
		while(swfDIV.hasChildNodes()){
			swfDIV.removeChild(swfDIV.firstChild);
		}
		var img = document.createElement("img");
		img.setAttribute("src", "<?=$intranet_httppath?>/file/reportcard_rubrics/tmp_chart/tmp_chart_png_"+id+".png");
		img.setAttribute("width", "<?=$chartWidth?>");
		img.setAttribute("height", "<?=$chartHeight?>");
		swfDIV.appendChild(img);
	}
	
	var chartNum = <?=count($chartArr)?>;
//	alert("chartNum: "+chartNum);
	
	var dataArr = new Array();
	<?php
		foreach( (array)$chartArr as $chartID => $chartJSON ) {
			echo "dataArr[ ".$chartID." ] = ".$chartJSON->toPrettyString()."\n";
		}
	?>
	
//	alert("dataArr length: "+dataArr.length);

</script>
	
<script type="text/javascript">
	var chartNum = <?=count($chartArr)?>;
	for(var k=0; k<chartNum; k++){
		var flashvars = {id:k, postImgUrl:"../uploadFlashImage.php"};
		//var flashvars = {id:k};
		swfobject.embedSWF("<?=$PATH_WRT_ROOT?>includes/flashchart_basic_201301_in_use/open-flash-chart.swf", "div"+k, "<?=$chartWidth?>", "<?=$chartHeight?>", "9.0.0", "", flashvars);
	}//end for
</script>

<style type='text/css' media='print'>
	.print_hide {display:none;}
</style>
<style type='text/css'>
	.report_table {border-collapse:collapse;}
</style>
<table border="0" cellpadding="4" width="95%" cellspacing="0" class="print_hide">
	<tr><td align="right"><span><?=$PrintBtn?></span></td></tr>
</table>

<div style="text-align:center;vertical-align:top;width:100%">
	<?=$FlashDiv?>
</div>
<table border="0" cellpadding="4" width="95%" cellspacing="0" class="print_hide">
	<tr><td align="right"><span><?=$PrintBtn?></span></td></tr>
</table>

<?
intranet_closedb();
?>