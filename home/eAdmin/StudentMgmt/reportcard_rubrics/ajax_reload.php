<?php
// Using:
/*
 *  Date :  2019-07-29 (Bill)
 *          - added first title config for subject group selection
 *
 *  Date :  2019-03-21 (Bill)     [2018-1002-1146-08277]
 *          - added new parms for Module Selection
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
$lreportcard = new libreportcardrubrics_custom();
$lreportcard_ui = new libreportcardrubrics_ui();


# Get data
$Action = stripslashes($_REQUEST['Action']);

if ($Action == "Subject_Selection")
{
	$YearTermID = $_REQUEST['YearTermID'];
	$ModuleID = $_REQUEST['ModuleID'];
	$SubjectID = $_REQUEST['SubjectID'];
	$SelectionID = $_REQUEST['SelectionID'];
	$OnChange = $_REQUEST['OnChange'];
	$CheckModuleSubject = $_REQUEST['CheckModuleSubject'];
	$NoFirst = $_REQUEST['NoFirst'];
	$FirstTitle = stripslashes(trim($_REQUEST['FirstTitle']));
	$AccessibleSubjectOnly = $_REQUEST['AccessibleSubjectOnly'];
	
	$NoFirst = ($NoFirst == '')? 1 : $NoFirst;
	
	include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
	include_once($PATH_WRT_ROOT.'includes/subject_class_mapping_ui.php');
	
	if ($CheckModuleSubject == 1)
	{
		if ($ModuleID == '')
		{
			include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_module.php");
			$lreportcard_module = new libreportcardrubrics_module();
			$ModuleInfoArr = $lreportcard_module->Get_Module_Info($YearTermID);
			$ModuleIDArr = Get_Array_By_Key($ModuleInfoArr, 'ModuleID');
		}
		else
		{
			$ModuleIDArr = array($ModuleID);
		}
		$ModuleSubjectArr = $lreportcard->Get_Module_Subject($ModuleIDArr, $ReturnAsso=0);
		$SubjectIDArr = array_unique(Get_Array_By_Key($ModuleSubjectArr, 'SubjectID'));
	}
	else
	{
		$libSubject = new subject();
		$SubjectArr = $libSubject->Get_Subject_List();
		$SubjectIDArr = Get_Array_By_Key($SubjectArr, 'RecordID');
	}
		
	
	if ($AccessibleSubjectOnly)
	{
		$AccessibleSubjectArr = $lreportcard->Get_User_Accessible_Subject($YearTermID);
		$AccessibleSubjectIDArr = Get_Array_By_Key($AccessibleSubjectArr, 'SubjectID');
	}
	else
	{
		if (!isset($libSubject))
			$libSubject = new subject();
		$SubjectArr = $libSubject->Get_Subject_List();
		$AccessibleSubjectIDArr = Get_Array_By_Key($SubjectArr, 'RecordID');
	}
	
	$DisplaySubjectID = array_values(array_intersect($SubjectIDArr, $AccessibleSubjectIDArr));
	
	$scm_ui = new subject_class_mapping_ui();
	echo $scm_ui->Get_Subject_Selection($SelectionID, $SubjectID, $OnChange, $NoFirst, $FirstTitle, $YearTermID, $OnFocus='', $FilterSubjectWithoutSG=0, $IsMultiple=0, $DisplaySubjectID);
}
else if ($Action == "Subject_Group_Selection")
{
	$YearTermID = $_REQUEST['YearTermID'];
	$SubjectID = $_REQUEST['SubjectID'];
	$SelectionID = $_REQUEST['SelectionID'];
	$OnChange = stripslashes($_REQUEST['OnChange']);
	$DisplayTermInfo = stripslashes($_REQUEST['DisplayTermInfo']);
	$SubjectGroupID = $_REQUEST['SubjectGroupID'];
	$IsMultiple = $_REQUEST['IsMultiple'];
	$NoFirst = $_REQUEST['NoFirst'];
    $FirstTitle = stripslashes(trim($_REQUEST['FirstTitle']));
	
	$CurrentPage = $lreportcard->Get_Current_Page_Session();
	$IsAdmin = $lreportcard->Is_Admin_User();
	$CanAccessPage = $lreportcard->Has_Page_Access_Right($CurrentPage);
	$TeachingOnly = ($IsAdmin || $CanAccessPage)? false : true;
	
	include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
	include_once($PATH_WRT_ROOT.'includes/subject_class_mapping_ui.php');
	$scm_ui = new subject_class_mapping_ui();
	
	echo $scm_ui->Get_Subject_Group_Selection($SubjectID, $SelectionID, $SubjectGroupID, $OnChange, $YearTermID, $IsMultiple, $lreportcard->AcademicYearID, $NoFirst, $DisplayTermInfo, $TeachingOnly, $FirstTitle);
}
else if ($Action == "Subject_Group_Student_Selection")
{
	$SubjectGroupID = $_REQUEST['SubjectGroupID'];
	$SelectionID = $_REQUEST['SelectionID'];
	$OnChange = stripslashes($_REQUEST['OnChange']);
	$IsAll = stripslashes($_REQUEST['IsAll']);
	$NoFirst = stripslashes($_REQUEST['NoFirst']);
	$StudentID = stripslashes($_REQUEST['StudentID']);
	
	include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
	include_once($PATH_WRT_ROOT.'includes/subject_class_mapping_ui.php');
	$scm_ui = new subject_class_mapping_ui();
	
	echo $scm_ui->Get_Subject_Group_Student_Selection($SelectionID, $SubjectGroupID, $StudentID, $OnChange, $IsMultiple=0, $IsAll, $NoFirst);
}
else if ($Action == "Module_Selection")
{
	$YearTermID = $_REQUEST['YearTermID'];
	$SelectionID = $_REQUEST['SelectionID'];
	$OnChange = stripslashes($_REQUEST['OnChange']);
	$IsAll = stripslashes($_REQUEST['IsAll']);
	$NoFirst = stripslashes($_REQUEST['NoFirst']);
	$ModuleID = stripslashes($_REQUEST['ModuleID']);
	$IsMultiple = $_REQUEST['IsMultiple'];
	$AcademicYearID = $_REQUEST['AcademicYearID'];
	$CheckIsWithinDateRange = $_REQUEST['CheckIsWithinDateRange'];
	
	echo $lreportcard_ui->Get_Module_Selection($SelectionID, $YearTermID, $ModuleID, $OnChange, $Class='', $IsAll, $NoFirst, $IsMultiple, $AcademicYearID, $CheckIsWithinDateRange);
}
//else if ($Action == "Class_Selection")	// moved to $Action == "Accessible_Class_Selection"
//{
//	include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
//	include_once($PATH_WRT_ROOT.'includes/form_class_manage_ui.php');
//	$fcm_ui = new form_class_manage_ui();
//	
//	$YearID = $_REQUEST['YearID'];
//	$SelectionID = $_REQUEST['SelectionID'];
//	$OnChange = $_REQUEST['OnChange'];
//	$IsMultiple = stripslashes($_REQUEST['IsMultiple']);
//	$NoFirst = stripslashes($_REQUEST['NoFirst']);
//	$isAll = empty($isAll)? 0 : $isAll;
//	
//	echo $fcm_ui->Get_Class_Selection($lreportcard->AcademicYearID, $YearID, $SelectionID, $SelectedYearClassID='', $OnChange, $NoFirst, $IsMultiple, $isAll);
//}
else if ($Action == "Class_Student_Selection")
{
//	include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
//	include_once($PATH_WRT_ROOT.'includes/form_class_manage_ui.php');
//	$fcm_ui = new form_class_manage_ui();
			
	$YearClassIDList = $_REQUEST['YearClassIDList'];
	$SubjectGroupIDList = $_REQUEST['SubjectGroupIDList'];
	$ExtraInfoList = $_REQUEST['ExtraInfoList'];
	$SelectionID = $_REQUEST['SelectionID'];
	$IsMultiple = stripslashes($_REQUEST['IsMultiple']);
	$NoFirst = stripslashes($_REQUEST['NoFirst']);
	
	if (trim($YearClassIDList) == '')
		$YearClassIDArr = '';
	else
		$YearClassIDArr = explode(',', $YearClassIDList);
		
	if (trim($SubjectGroupIDList) == '')
		$SubjectGroupIDArr = '';
	else
		$SubjectGroupIDArr = explode(',', $SubjectGroupIDList);
		
	if(trim($ExtraInfoList)!='')
		$ExtraInfoArr  = explode(',', $ExtraInfoList);
	
	$lreportcard_ui = new libreportcardrubrics_ui();
	echo $lreportcard_ui->Get_Student_Selection($SelectionID, $YearClassIDArr, $SelectedStudentID='', $Onchange='', $NoFirst, $IsMultiple, $isAll=0, $ExtraInfoArr, $SubjectGroupIDArr);
//	echo $fcm_ui->Get_Student_Selection($SelectionID, $YearClassIDArr, $SelectedStudentID='', $Onchange='', $NoFirst, $IsMultiple, $isAll=0);
}
else if ($Action == "LoadSubCategorySelection")
{
	$lreportcard_ui = new libreportcardrubrics_ui();
	
	$EcaCategoryID = $_REQUEST['EcaCategoryID'];
	$SubCategorySelection = $lreportcard_ui->Get_ECA_SubCategory_Selection("EcaSubCategoryID",$EcaCategoryID,$EcaSubCategoryID=0,$Onchange='js_Reload_Item_Table();', $noFirst=0, $isAll=1);
	
	echo $SubCategorySelection;
} 
else if ($Action == "Class_Selection" || $Action == "Accessible_Class_Selection")
{
	global $Lang;
	
	if ($Action == "Class_Selection")
	{
		$ClassLevelID = $_REQUEST['YearID'];
		$SelectionID = $_REQUEST['SelectionID'];
		$OnChange = $_REQUEST['OnChange'];
		$IsMultiple = stripslashes($_REQUEST['IsMultiple']);
		$noFirst = stripslashes($_REQUEST['NoFirst']);
		$isAll = empty($isAll)? 0 : $isAll;
		$ClassID = '';
		
		$ClassLevelID = explode(",",$ClassLevelID);
	}
	else if ($Action == "Accessible_Class_Selection")
	{
		$ClassID =  $_REQUEST['ClassID']?$_REQUEST['ClassID']:'';
		$noFirst =  $_REQUEST['noFirst']?$_REQUEST['noFirst']:0;
		$isAll =  trim($_REQUEST['isAll'])!=''?$_REQUEST['isAll']:1;
		$firstTitle = $isAll==1?'- '.$Lang['eRC_Rubrics']['GeneralArr']['AllClass'].' -':'';
		$firstTitle =  $_REQUEST['firstTitle']?stripslashes($_REQUEST['firstTitle']):$firstTitle;
		$ClassLevelID =  $_REQUEST['ClassLevelID']?$_REQUEST['ClassLevelID']:'';
		$OnChange =  $_REQUEST['OnChange']?$_REQUEST['OnChange']:'';
		$SelectionID = "ClassID";
		$IsMultiple = 0;
	}
	
	$lreportcard_ui = new libreportcardrubrics_ui();
	$ClassSelection = $lreportcard_ui->Get_Class_Selection($SelectionID, $ClassID, $OnChange, $noFirst, $isAll, $firstTitle, $ClassLevelID, $TeachingClassOnly=1, $IsMultiple);
	echo $ClassSelection;
}
else if ($Action == "Reload_Topic_Selection")
{
	$SubjectID = $_POST['SubjectID'];
	$Level = $_POST['Level'];
	$PreviousLevelTopicID = $_POST['PreviousLevelTopicID'];
	$SelectionID = stripslashes($_POST['SelectionID']);
	$OnChange = stripslashes($_POST['OnChange']);
	$TopicID = $_POST['TopicID'];
	$IsMultiple = $_POST['IsMultiple'];
	$all = $_POST['all'];
	$noFirst = $_POST['noFirst'];
	$hasChooseOption = $_POST['hasChooseOption'];
	$SubjectIDArr = explode(",",$SubjectID);
	$lreportcard_ui = new libreportcardrubrics_ui();
	echo $lreportcard_ui->Get_Topic_Selection($SubjectIDArr, $Level, $PreviousLevelTopicID, $SelectionID, $TopicID, $OnChange, $all, $noFirst, $IsMultiple,$hasChooseOption);
}
else if ($Action == "Topic_Remarks_Layer")
{
	$TopicID = $_REQUEST['TopicID'];
	
	$lreportcard_ui = new libreportcardrubrics_ui();
	echo $lreportcard_ui->Get_Topic_Remarks_Layer($TopicID);
}
else if ($Action == "Load_Learning_Detail_Option")
{
	$SubjectIDArr = explode(",",$_POST['SubjectIDArr']);
	
	$lreportcard_ui = new libreportcardrubrics_ui();
	echo $lreportcard_ui->Get_Reports_Grand_Marksheet_Detail_Option($SubjectIDArr);
}
else if ($Action == "Form_Selection")
{
	//($ID_Name, $SelectedYearID='', $Onchange='', $noFirst=0, $isAll=0, $firstTitle='', $TeachingFormOnly=0, $isMultiple=0, $FormStageID='')
	//$FormStageID = array($_REQUEST['FormStageID']);
	$SelectionID = $_REQUEST['SelectionID'];
	$SelectedYearID = $_REQUEST['SelectedYearID'];
	$OnChange = $_REQUEST['OnChange'];
	$NoFirst = $_REQUEST['NoFirst'];
	$isAll = $_REQUEST['isAll'];
	$FirstTitle = stripslashes(trim($_REQUEST['FirstTitle']));
	$TeachingFormOnly = $_REQUEST['TeachingFormOnly'];
	$IsMultiple =  $_REQUEST['IsMultiple'];
	
	$FormStageIDArr = explode(",", $_REQUEST['FormStageID']);
	
	$lreportcard_ui = new libreportcardrubrics_ui();
	echo $lreportcard_ui->Get_Form_Selection($SelectionID, $SelectedYearID, $OnChange, $NoFirst, $isAll, $FirstTitle, $TeachingFormOnly, $IsMultiple, $FormStageIDArr);
}
else if ($Action == "Term_Selection")
{
	include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
	$libSCM_ui = new subject_class_mapping_ui();
	
	$SelectionID = $_REQUEST['SelectionID'];
	$AcademicYearID = $_REQUEST['AcademicYearID'];
	$YearTermID = $_REQUEST['YearTermID'];
	$OnChange = trim(urldecode(stripslashes($_REQUEST['OnChange'])));
	$NoFirst = $_REQUEST['NoFirst'];
	$DisplayAll = $_REQUEST['DisplayAll'];
	$FirstTitle = trim(urldecode(stripslashes($_REQUEST['FirstTitle'])));
	$IsMultiple = $_REQUEST['IsMultiple'];
	$ReportID = $_REQUEST['ReportID'];
	
	$IncludeYearTermIDArr = '';
	if ($ReportID != '')
	{
		$ReportModuleInfoArr = $lreportcard->GetReportTemplateModuleInfo($ReportID);
		$IncludeYearTermIDArr = array_values(array_unique(Get_Array_By_Key($ReportModuleInfoArr, 'YearTermID')));
		
		// YearTermID = 0 means Whole Year => Get all Terms
		if (in_array(0, $IncludeYearTermIDArr)) {
			$IncludeYearTermIDArr = '';
		}
	}
	
	$AcademicYearID = ($AcademicYearID == '')? $lreportcard->AcademicYearID : $AcademicYearID;
	echo $libSCM_ui->Get_Term_Selection($SelectionID, $AcademicYearID, $YearTermID, $OnChange, $NoFirst, $NoPastTerm=0, $DisplayAll, $FirstTitle, $PastTermOnly=0, $CompareYearTermID='', $IsMultiple, $IncludeYearTermIDArr);
}


intranet_closedb();
?>