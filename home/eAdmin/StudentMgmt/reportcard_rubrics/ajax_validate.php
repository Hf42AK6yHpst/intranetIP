<?php
// using :
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
$lreportcard = new libreportcardrubrics_custom();

# Get data
$Action = stripslashes($_REQUEST['Action']);

if ($Action == "ReportCard_Type")
{
	$ReportID = $_REQUEST['ReportID'];
	$ReportInfoArr = $lreportcard->GetReportTemplateInfo($ReportID);
	
	echo $ReportInfoArr[0]['ReportType'];
}

intranet_closedb();
?>