<?php
// Using:
/*
 *  Date :  2019-03-21 (Bill)     [2018-1002-1146-08277]
 *          - Add Import Mode
 */

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Management_MarksheetRevision";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

# Get data
$action = stripslashes($_REQUEST['action']);
if ($action == 'Import_Student_Marksheet')
{
    include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
    include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_module.php");
    include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_rubrics.php");
    $lreportcard_topic = new libreportcardrubrics_topic();
    $lreportcard_module = new libreportcardrubrics_module();
    $lreportcard_rubrics = new libreportcardrubrics_rubrics();
    
    $lreportcard_topic->Start_Trans();
    
    ### Include JS libraries
    $jsInclude = '';
    $jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
    $jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
    $jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
    echo $jsInclude;
    
    ### Get import temp data
    $TEMP_IMPORT_TABLE = $lreportcard_topic->Get_Table_Name('TEMP_IMPORT_MARKSHEET_SCORE');
    $sql = "SELECT * FROM $TEMP_IMPORT_TABLE WHERE ImportUserID = '".$_SESSION['UserID']."'";
    $ImportDataArr = $lreportcard_topic->returnArray($sql);
    $numOfImportData = count($ImportDataArr);
    
    # Get data
    $SubjectID = $_REQUEST['SubjectID'];
    $RubricsID = $ImportDataArr[0]['RubricsID'];
    $StudentSubjectLevelArr = BuildMultiKeyAssoc($ImportDataArr, array('StudentID', 'ModuleID', 'TopicID'), 'RubricsItemID', 1, 0);
    $StudentSubjectLevelRemarkArr = BuildMultiKeyAssoc($ImportDataArr, array('StudentID', 'ModuleID', 'TopicID'), 'Remarks', 1, 0);
    foreach((array)$StudentSubjectLevelRemarkArr as $_studentID => $SubjectLevelRemarkArr) {
        foreach((array)$SubjectLevelRemarkArr as $_moduleID => $LevelRemarksArr) {
            foreach((array)$LevelRemarksArr as $_topicID => $_LevelRemarks) {
                $StudentSubjectLevelRemarkArr[$_studentID][$_moduleID][$_topicID] = addslashes($_LevelRemarks);
            }
        }
    }
    
    ### Insert Marksheet
    foreach((array)$StudentSubjectLevelArr as $StudentID => $SubjectLevelArr) {
        $SubjectLevelRemarkArr = $StudentSubjectLevelRemarkArr[$StudentID];
        $Success = $lreportcard_topic->Save_Student_Marksheet_Score($SubjectID, $StudentID, $RubricsID, $SubjectLevelArr, $SubjectLevelRemarkArr);
    }
    
    ### Update Processing Display
    $thisJSUpdate = '';
    $thisJSUpdate .= '<script language="javascript">'."\n";
        $thisJSUpdate .= 'window.parent.document.getElementById("BlockUI_Processed_Number_Span").innerHTML = "'.$csvDataSize.'";';
    $thisJSUpdate .= '</script>'."\n";
    echo $thisJSUpdate;
    
    ### Delete temp data
    $success['DeleteTempData'] = $lreportcard_topic->Delete_Import_Temp_Marksheet();
    
    ### Roll Back data if import failed
    if (in_multi_array(false, $success))
    {
        $lreportcard_topic->RollBack_Trans();
        $ImportStatus = $Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ReturnMsgArr']['ImportFail'];
    }
    else
    {
        $lreportcard_topic->Commit_Trans();
        $ImportStatus = $Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ReturnMsgArr']['ImportSuccess'];
    }
    
    ### Update Processing Display
    $thisJSUpdate = '';
    $thisJSUpdate .= '<script language="javascript">'."\n";
        $thisJSUpdate .= 'window.parent.document.getElementById("ImportStatusSpan").innerHTML = "'.$ImportStatus.'";';
        $thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
    $thisJSUpdate .= '</script>'."\n";
    echo $thisJSUpdate;
}
else 
{
    # Get data
    $SubjectID = $_REQUEST['SubjectID'];
    $StudentID = $_REQUEST['StudentID'];
    $RubricsID = $_REQUEST['RubricsID'];
    $SubjectLevelArr = $_REQUEST['SubjectLevelArr'];		// $SubjectLevelArr[$ModuleID][$TopicID] = $RubricsItemID;
    $SubjectLevelRemarkArr = $_REQUEST['SubjectLevelRemarkArr'];
    
    $Success = $lreportcard->Save_Student_Marksheet_Score($SubjectID, $StudentID, $RubricsID, $SubjectLevelArr, $SubjectLevelRemarkArr);
    echo ($Success)? '1' : '0';
}

intranet_closedb();
?>