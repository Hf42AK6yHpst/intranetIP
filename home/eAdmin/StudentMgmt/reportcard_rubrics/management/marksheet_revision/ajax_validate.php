<?php
// Using:
/*
 *  Date :  2019-07-29 (Bill)
 *          - Updated logic for new csv format
 *
 *  Date :  2019-03-21 (Bill)     [2018-1002-1146-08277]
 *          - Create file
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_module.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_rubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");

$lreportcard_topic = new libreportcardrubrics_topic();
$lreportcard_module = new libreportcardrubrics_module();
$lreportcard_rubrics = new libreportcardrubrics_rubrics();

# Get action
$action = stripslashes($_REQUEST['action']);
if ($action == 'Import_Student_Marksheet')
{
    include_once($PATH_WRT_ROOT.'includes/libimporttext.php');
    include_once($PATH_WRT_ROOT."includes/libexporttext.php");
    include_once($PATH_WRT_ROOT.'includes/libuser.php');
	$limport = new libimporttext();
	$lexport = new libexporttext();
	$luser = new libuser();

	### Get CSV Data
	$targetFilePath = stripslashes($_REQUEST['targetFilePath']);
	$csvData = $limport->GET_IMPORT_TXT($targetFilePath);
	
	### Get CSV Header
	$csvHeaderArr = array_shift($csvData);
    //$csvStudentHeaderArr = array_shift($csvData);
    //$csvHeader2Arr = array_shift($csvData);
    $csvDataSize = count($csvData);

	### Get Default Cols
	$defaultMarksheet_ColumnArr = $lreportcard_topic->Get_Student_Marksheet_Import_Export_Column_Title_Arr();
    //$csvHeaderSize = count($defaultMarksheet_ColumnArr[0]);
    $csvHeaderSize = count($defaultMarksheet_ColumnArr);
	
	### Delete temp data first
	$SuccessArr['DeleteOldTempData'] = $lreportcard_topic->Delete_Import_Temp_Marksheet();
	
	//### Get Class Student
	//$StudentArr = $lreportcard_topic->GET_STUDENT_BY_CLASS($_GET['ClassID'], $ParStudentIDList="", $isShowBothLangs=0, $withClassNumber=0, $isShowStyle=0, $ReturnAsso=1);
	//$StudentIDArr = array_keys($StudentArr);

    ### Get Year Term
    if($_GET['YearTermID'] == '') {
        $YearTermInfo = getSemesters($lreportcard_topic->Get_Active_AcademicYearID());
        $YearTermIDArr = array_keys((array)$YearTermInfo);
    } else {
        $YearTermIDArr = array($_GET['YearTermID']);
    }

    ### Get Subject Group
    include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
    $scm = new subject_class_mapping();
    $SubjectGroupInfo = $scm->Get_Subject_Group_List($YearTermIDArr, $_GET['SubjectID'], $returnAssociate=0, $TeachingOnly);
    if($_GET['SubjectGroupID'] == '') {
        $SubjectGroupIDArr = Get_Array_By_Key($SubjectGroupInfo, 'SubjectGroupID');
    } else {
        $SubjectGroupIDArr = array($_GET['SubjectGroupID']);
    }
    $SubjectGroupAssoc = BuildMultiKeyAssoc($SubjectGroupInfo, 'SubjectGroupID');
    $SubjectGroupCodeAssoc = BuildMultiKeyAssoc($SubjectGroupInfo, 'ClassCode', 'SubjectGroupID');
    $SubjectGroupCodeArr = array_keys((array)$SubjectGroupCodeAssoc);

    ### Get Subject Group Student
    $StudentIDArr = $scm->Get_Subject_Group_Student_List($SubjectGroupIDArr);
    $StudentIDArr = Get_Array_By_Key($StudentIDArr, 'UserID');

    //$SubjectGroupStudentAssoc = array();
    $SubjectGroupCodeStudentAssoc = array();
    foreach($SubjectGroupIDArr as $thisSubjectGroupID)
    {
        $thisSubjectGroupCode = $SubjectGroupAssoc[$thisSubjectGroupID]['ClassCode'];

        $thisSubjectGroupObj = new subject_term_class($thisSubjectGroupID);
        $thisSubjectGroupStudentArr = $thisSubjectGroupObj->Get_Subject_Group_Student_List($OrderByStudentName=0, $SortingOrder='', $WithStyle=0);
        $thisSubjectGroupStudentIDArr = Get_Array_By_Key($thisSubjectGroupStudentArr, 'UserID');

        //$SubjectGroupStudentAssoc[$thisSubjectGroupID] = $thisSubjectGroupStudentIDArr;
        $SubjectGroupCodeStudentAssoc[$thisSubjectGroupCode] = $thisSubjectGroupStudentIDArr;
    }

    ### Get Subject Topics Info
	$TopicInfoArr = $lreportcard_topic->Get_Topic_Info($_GET['SubjectID']);
	$TopicTreeArr = $lreportcard_topic->Get_Student_Study_Topic_Tree($StudentIDArr, $_GET['SubjectID'], $_GET['ModuleID']);
	$TopicInfoLevelArr = BuildMultiKeyAssoc($TopicInfoArr, array("Level", "TopicCode"), "TopicID", 1, 0);
	
	### Get Module Info
	$ModuleInfoArr = $lreportcard_module->Get_Module_Info($_GET['YearTermID'], $_GET['ModuleID']);
	$numOfModule = count($ModuleInfoArr);
	
	### Get Student Study Topic Info
	// $StudentStudyTopicInfoArr[$StudentID][$ModuleID][$SubjectID][$TopicID] = InfoArr
	$StudentStudyTopicInfoArr = $lreportcard_topic->Get_Student_Study_Topic_Info($_GET['ModuleID'], $_GET['SubjectID'], $TopicID='', $StudentIDArr);
	$StudyingStudentIDArr = array_keys($StudentStudyTopicInfoArr);

	### Get Subject Rubrics Info
	$StudentClassInfoArr = $lreportcard_topic->Get_Student_Class_Info($StudentIDArr);
	$YearID = $StudentClassInfoArr[0]['YearID'];
	
	# Get Subject Rubrics Scheme
	// $SubjectRubricsInfoArr[$YearID][$SubjectID][$RubricsID][$RubricsItemID] = RubricsItemInfoArr
	$SubjectRubricsInfoArr = $lreportcard_topic->Get_Subject_Rubrics_Mapping($_GET['SubjectID'], $YearID);
	if (is_array($SubjectRubricsInfoArr[$YearID])) {
	    $tmpArrKey = array_keys((array)$SubjectRubricsInfoArr[$YearID][$_GET['SubjectID']]);
	} else {
	    $tmpArrKey = array_keys((array)$SubjectRubricsInfoArr[0][$_GET['SubjectID']]);
	}
	$RubricsID = $tmpArrKey[0];
	if ($RubricsID != '') {
	    $RubricsInfoArr = $lreportcard_rubrics->Get_Rubics_Info($RubricsID);
	}
	
	### Get Subject Rubrics Item Info
	$SubjectRubricsItemInfoArr = $SubjectRubricsInfoArr[$YearID][$_GET['SubjectID']][$RubricsID];
	$SubjectRubricsItemLevelArr = array();
	foreach($SubjectRubricsItemInfoArr as $thisItemLevelInfo) {
	    $SubjectRubricsItemLevelArr[trim($thisItemLevelInfo['LevelNameEn'])] = $thisItemLevelInfo['RubricsItemID'];
	}
	
	### Get Student Subject Score
	// $StudentSubjectMarksheetScoreArr[$StudentID][$SubjectID][$TopicID][$ModuleID] = InfoArr
	$StudentSubjectMarksheetScoreArr = $lreportcard_topic->Get_Student_Marksheet_Score($_GET['SubjectID'], $StudentIDArr);
	
	### Include JS libraries
	$jsInclude = '';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
	echo $jsInclude;
	
	### Var Init.
	$NumOfSuccessRow = 0;
	$NumOfErrorRow = 0;
	$ErrorRowInfoArr = array();
	$InsertValueArr = array();
	$RowNumber = 2;

    ### loop student
    //$csvStudentIDArr = array();
    //for($j=$csvHeaderSize; $j<count($csvStudentHeaderArr); $j++)
    //{
    //    # Warning if no such student
    //    $csvStudentUserLogin = trim($csvStudentHeaderArr[$j]);
    //    $csvStudentInfo = $luser->returnUser("", $csvStudentUserLogin, $UserIDsArray=null);
    //    if ($csvStudentUserLogin == '' || empty($csvStudentInfo))
    //    {
    //        $csvColumnName = numberToLetter(($j+1), $uppercase=true);
    //        (array)$ErrorRowInfoArr[$RowNumber][$csvColumnName]['ErrorMsgArr'][] = '- '.$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['NoStudentWarning'];
    //    }
    //    $csvStudentIDArr[$csvStudentUserLogin] = $csvStudentInfo[0]['UserID'];
    //
    //    # Warning if student not studying subject
    //    if($csvStudentInfo[0]['UserID'] > 0 && !in_array($csvStudentInfo[0]['UserID'], $StudyingStudentIDArr))
    //    {
    //        $csvColumnName = numberToLetter(($j+1), $uppercase=true);
    //        (array)$ErrorRowInfoArr[$RowNumber][$csvColumnName]['ErrorMsgArr'][] = '- '.$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['StudentNotStudyingSubject'];
    //    }
    //
    //    $j++;
    //}
    //$RowNumber++;
    //$RowNumber++;


    ### loop CSV data
	for ($i=0; $i<$csvDataSize; $i++)
	{
		$thisColumnCounter = 0;
        $j = 1;

		$thisSubjectGroupCode = trim($csvData[$i][$thisColumnCounter++]);
        $thisColumnCounter++;

        $thisStudentUserLogin = trim($csvData[$i][$thisColumnCounter++]);
        $thisColumnCounter++;

		$thisTopicCode = trim($csvData[$i][$thisColumnCounter++]);
		$thisColumnCounter++;
		
		$thisObjectiveCode = trim($csvData[$i][$thisColumnCounter++]);
		$thisColumnCounter++;
		
		$thisDetailsCode = trim($csvData[$i][$thisColumnCounter++]);
		$thisColumnCounter++;

		$thisLevelInput = trim($csvData[$i][$thisColumnCounter++]);
        $thisLevelDescription = trim($csvData[$i][$thisColumnCounter++]);

		# Warning if no such subject group
        //if ($thisSubjectGroupCode == '' || !in_array($thisSubjectGroupCode, (array)$SubjectGroupCodeArr))
        $isInvalidSubjectGroup = $thisSubjectGroupCode == '' || !in_array($thisSubjectGroupCode, (array)$SubjectGroupCodeArr);
        if($isInvalidSubjectGroup)
        {
            $csvColumnName = numberToLetter($j, $uppercase=true);
            (array)$ErrorRowInfoArr[$RowNumber][$csvColumnName]['ErrorMsgArr'][] = '- '.$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['NoSubjectGroupWarning'];
        }
        $j = $j + 2;

        # Warning if no such student
        $csvStudentInfo = $luser->returnUser("", $thisStudentUserLogin, $UserIDsArray=null);
        if ($thisStudentUserLogin == '' || empty($csvStudentInfo))
        {
            $csvColumnName = numberToLetter($j, $uppercase=true);
            (array)$ErrorRowInfoArr[$RowNumber][$csvColumnName]['ErrorMsgArr'][] = '- '.$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['NoStudentWarning'];
        }
        # Warning if student not studying subject
        else if($csvStudentInfo[0]['UserID'] > 0 && !in_array($csvStudentInfo[0]['UserID'], $StudyingStudentIDArr))
        {
            $csvColumnName = numberToLetter($j, $uppercase=true);
            (array)$ErrorRowInfoArr[$RowNumber][$csvColumnName]['ErrorMsgArr'][] = '- '.$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['StudentNotStudyingSubject'];
        }
        # Warning if student not in subject group
        else if($csvStudentInfo[0]['UserID'] > 0 && !in_array($csvStudentInfo[0]['UserID'], (array)$SubjectGroupCodeStudentAssoc[$thisSubjectGroupCode]))
        {
            $csvColumnName = numberToLetter($j, $uppercase=true);
            (array)$ErrorRowInfoArr[$RowNumber][$csvColumnName]['ErrorMsgArr'][] = '- '.$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['StudentNotInSubjectGroup'];
        }
        $csvStudentIDArr[$thisStudentUserLogin] = $csvStudentInfo[0]['UserID'];
        $j = $j + 2;

		# Warning if no such Learning Topic
		if ($thisTopicCode == '' || !isset($TopicInfoLevelArr[1][$thisTopicCode]) || !isset($TopicTreeArr[$TopicInfoLevelArr[1][$thisTopicCode]]))
		{
		    $csvColumnName = numberToLetter($j, $uppercase=true);
		    (array)$ErrorRowInfoArr[$RowNumber][$csvColumnName]['ErrorMsgArr'][] = '- '.$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['NoLearningTopicWarning'];
		}
		$Level1_TopicID = $TopicInfoLevelArr[1][$thisTopicCode];
        $j = $j + 2;
		
		# Warning if no such Learning Objective
		if ($Level1_TopicID && ($thisObjectiveCode == '' || !isset($TopicInfoLevelArr[2][$thisObjectiveCode]) || !isset($TopicTreeArr[$Level1_TopicID][$TopicInfoLevelArr[2][$thisObjectiveCode]])))
		{
		    $csvColumnName = numberToLetter($j, $uppercase=true);
		    (array)$ErrorRowInfoArr[$RowNumber][$csvColumnName]['ErrorMsgArr'][] = '- '.$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['NoLearningObjectiveWarning'];
		}
		$Level2_TopicID = $TopicInfoLevelArr[2][$thisObjectiveCode];
        $j = $j + 2;
		
		# Warning if no such Learning Details
		if ($Level1_TopicID && $Level2_TopicID && ($thisDetailsCode == '' || !isset($TopicInfoLevelArr[3][$thisDetailsCode]) || !isset($TopicTreeArr[$Level1_TopicID][$Level2_TopicID][$TopicInfoLevelArr[3][$thisDetailsCode]])))
		{
		    $csvColumnName = numberToLetter($j, $uppercase=true);
		    (array)$ErrorRowInfoArr[$RowNumber][$csvColumnName]['ErrorMsgArr'][] = '- '.$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['NoLearningDetailsWarning'];
		}
		$Level3_TopicID = $TopicInfoLevelArr[3][$thisDetailsCode];
        $j = $j + 2;
		
		# Warning if invalid Level / Description input
        if($Level3_TopicID && isset($csvStudentIDArr[$thisStudentUserLogin]))
        {
            // Student not studying Learning Details
            $thisStudentID = $csvStudentIDArr[$thisStudentUserLogin];
            if(!isset($StudentStudyTopicInfoArr[$thisStudentID][$_GET['ModuleID']][$_GET['SubjectID']][$Level3_TopicID]))
            {
                # Warning if not input '--'
                if ($thisLevelInput != '--')
                {
                    $csvColumnName = numberToLetter($j, $uppercase=true);
                    (array)$ErrorRowInfoArr[$RowNumber][$csvColumnName]['ErrorMsgArr'][] = '- '.$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['StudentNotStudyLearningDetails'];
                }
                else if ($thisLevelDescription != '--')
                {
                    $csvColumnName = numberToLetter(++$j, $uppercase=true);
                    (array)$ErrorRowInfoArr[$RowNumber][$csvColumnName]['ErrorMsgArr'][] = '- '.$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['StudentNotStudyLearningDetails'];
                }
            }
            // Student studying Learning Details
            else
            {
                // with Rubrics level input
                if($thisLevelInput != '')
                {
                    # Warning if invalid level
                    if(!isset($SubjectRubricsItemLevelArr[$thisLevelInput]))
                    {
                        $csvColumnName = numberToLetter($j, $uppercase=true);
                        (array)$ErrorRowInfoArr[$RowNumber][$csvColumnName]['ErrorMsgArr'][] = '- '.$Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['WarningArr']['InvalidDetailsLevel'];
                    }
                    // Prepare value insert to temp table
                    else
                    {
                        $InsertValueArr[] = " ('".$_SESSION['UserID']."', '".$RowNumber."', '".$thisStudentID."', '".$_GET['ModuleID']."', '".$_GET['SubjectID']."', '".$Level3_TopicID."',
                                                '".$RubricsID."', '".$SubjectRubricsItemLevelArr[$thisLevelInput]."', '".$thisLevelInput."', '".$lreportcard_topic->Get_Safe_Sql_Query($thisLevelDescription)."', NOW()) ";
                    }
                }
                // without Rubrics level input
                else
                {
                    $InsertValueArr[] = " ('".$_SESSION['UserID']."', '".$RowNumber."', '".$thisStudentID."', '".$_GET['ModuleID']."', '".$_GET['SubjectID']."', '".$Level3_TopicID."',
                                                '".$RubricsID."', '0', '0', '".$lreportcard_topic->Get_Safe_Sql_Query($thisLevelDescription)."', NOW()) ";
                }
            }
		}
		
		### Update result count
		if(empty($ErrorRowInfoArr[$RowNumber])) {
			$NumOfSuccessRow++;
		} else {
			$NumOfErrorRow++;
		}
		
		### Update Processing Display
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.document.getElementById("SuccessCountDiv").innerHTML = "'.$NumOfSuccessRow.'";';
			$thisJSUpdate .= 'window.parent.document.getElementById("FailCountDiv").innerHTML = "'.$NumOfErrorRow.'";';
			$thisJSUpdate .= 'window.parent.document.getElementById("BlockUI_Processed_Number_Span").innerHTML = "'.($RowNumber - 2).'";';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
		
		$RowNumber++;
	}

	### Insert temp records into the temp table
	if (count($InsertValueArr) > 0)
	{
		$InsertList = implode(',', $InsertValueArr);
		
		$TEMP_IMPORT_ECA = $lreportcard_topic->Get_Table_Name('TEMP_IMPORT_MARKSHEET_SCORE');
		$sql = "INSERT INTO $TEMP_IMPORT_ECA
					(ImportUserID, RowNumber, StudentID, ModuleID, SubjectID, TopicID, RubricsID, RubricsItemID, Level, Remarks, DateInput)
                VALUES
                    $InsertList ";
        $SuccessArr['InsertTempRecords'] = $lreportcard_topic->db_db_query($sql);
	}
	
	### Display Error Result Table
	$numOfErrorRow = count($ErrorRowInfoArr);
	if($numOfErrorRow > 0)
	{
		### Update Processing Display
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.document.getElementById("BlockUI_Span").innerHTML = "'.$Lang['SysMgr']['SubjectClassMapping']['GeneratingErrorsInfo'].'...";';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
		
		### Build Error Info Table
		$x .= '<table width="100%" border="0" cellpadding="3" cellspacing="0">';
			$x .= '<tr>';
				$x .= '<td class="tablebluetop tabletopnolink" width="8%">'.$Lang['General']['ImportArr']['Row'].'</td>';
				$x .= '<td class="tablebluetop tabletopnolink" width="10%">'.$Lang['General']['ImportArr']['Column'].'</td>';
				$x .= '<td class="tablebluetop tabletopnolink">'.$Lang['General']['Remark'].'</td>';
			$x .= '</tr>';
			
		$i = 0;
        foreach((array)$ErrorRowInfoArr as $thisRowNumber => $thisErrorRowInfo)
        {
            $row_size = count($thisErrorRowInfo);
			$css_i = ($i % 2) ? "2" : "";
			
			$is_first_col = true;
			foreach((array)$thisErrorRowInfo as $thisColumnName => $thisErrorRowColumnInfo)
			{
			    $x .= '<tr style="vertical-align:top">';
				    if($is_first_col) {
				        $x .= '<td class="tablebluerow'.$css_i.'" rowspan="'.$row_size.'">'.$thisRowNumber.'</td>';
				    }
				    $x .= '<td class="tablebluerow'.$css_i.'">'.$thisColumnName.'</td>';
				    $x .= '<td class="tablebluerow'.$css_i.'">'.implode('<br />', (array)$thisErrorRowColumnInfo['ErrorMsgArr']).'</td>';
			    $x .= '</tr>';
			    
			    $is_first_col = false;
			}
			$i++;
		}
		
		$x .= "</table>";
	}
    
	$ErrorCountDisplay = ($NumOfErrorRow > 0) ? "<font color=\"red\">".$NumOfErrorRow."</font>" : $NumOfErrorRow;
	
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= 'window.parent.document.getElementById("ErrorTableDiv").innerHTML = \''.$x.'\';';
		$thisJSUpdate .= 'window.parent.document.getElementById("SuccessCountDiv").innerHTML = \''.$NumOfSuccessRow.'\';';
		$thisJSUpdate .= 'window.parent.document.getElementById("FailCountDiv").innerHTML = \''.$ErrorCountDisplay.'\';';
		
		if ($NumOfErrorRow == 0) 
		{
			$thisJSUpdate .= '$("input#ContinueBtn", window.parent.document).removeClass("formbutton_disable").addClass("formbutton").attr("disabled", "");';
		}
		
		$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
}

intranet_closedb();
?>