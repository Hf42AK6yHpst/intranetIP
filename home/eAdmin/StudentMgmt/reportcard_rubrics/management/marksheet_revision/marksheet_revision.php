<?php
// Using:
/*
 *  Date :  2019-03-21 (Bill)     [2018-1002-1146-08277]
 *          - added JS function - js_Go_Import()
 */

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Management_MarksheetRevision";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$lreportcard_ui = new libreportcardrubrics_ui();
$linterface = new interface_html();

############## Interface Start ##############
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# Tag
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['MenuTitle']);
$linterface->LAYOUT_START();

//$Select_LearningTopic = Get_Selection_First_Title($Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['LevelArr'][1]['Select']);
//$All_LearningTopic = Get_Selection_First_Title($Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][1]['All']);
//
//$All_LearningObj =Get_Selection_First_Title($Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][2]['All']);

echo $lreportcard_ui->Include_JS_CSS();
echo $lreportcard_ui->Get_Management_MarksheetRevision_UI();
?>

<script language="javascript">
var jsCurYearTermID = '';
var jsCurModuleID = '';
var jsCurSubjectID = '';
var jsCurSubjectGroupID = '';
var jsCurStudentID = '';
var jsScreenWidth = parseInt(Get_Screen_Width());
var jsTableWidth = jsScreenWidth - 250;
var jsMaxTopicLevel = <?=$eRC_Rubrics_ConfigArr['MaxLevel']?>;

// The variables for copy and paste are defined in the php function Get_Management_MarksheetRevision_Table()
var arrCookies = new Array();
arrCookies[arrCookies.length] = "SubjectID";

$(document).ready( function() {	
	<? if($clearCoo) { ?>
		for(i=0; i<arrCookies.length; i++)
		{
			var obj = arrCookies[i];
			//alert('obj = ' + obj);
			$.cookies.del(obj);
		}
	<? } ?>
	
	jsCurYearTermID = $('select#YearTermID').val();
	js_Reload_Module_Selection();
});

function js_Changed_Term_Selection(jsYearTermID)
{
	jsCurYearTermID = jsYearTermID;
	js_Reload_Module_Selection();
}

function js_Changed_Module_Selection(jsModuleID)
{
	jsCurModuleID = jsModuleID;
	js_Reload_Subject_Selection();
}

function js_Changed_Subject_Selection(jsSubjectID)
{
	jsCurSubjectID = jsSubjectID;
	if (jsCurSubjectID == '')
	{
		$('div#SubjectGroupSelectionDiv').html('');
		$('div#StudentSelectionDiv').html('');
		$('div#StudentMarksheetDiv').html('');
	}
	else
	{
		js_Reload_Subject_Group_Selection();
		js_Reload_Topic_Selection(jsLevel=1, '', '', 0);
	}
}

function js_Changed_SubjectGroup_Selection(jsSubjectGroupID)
{
	jsCurSubjectGroupID = jsSubjectGroupID;
	
	var TopicID = getSelectVal(1);
	var ReloadStr = 'reload';
	if(TopicID=='')
	{
		ReloadStr = '';
	}
	js_Reload_Subject_Group_Student_Selection(ReloadStr);
}

function js_Changed_Student_Selection(jsStudentID)
{
	jsCurStudentID = jsStudentID;
	
	var TopicVal = getSelectVal(1);
	if(TopicVal!='')
	{
		js_Reload_Student_Marksheet_Table();
	}
}

function js_Reload_Module_Selection(ParReload)
{
	$('div#ModuleSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{
			Action: 'Module_Selection',
			async: false,
			YearTermID: jsCurYearTermID,
			SelectionID: 'ModuleID',
			OnChange: 'js_Changed_Module_Selection(this.value)',
			IsAll: 1,
			ModuleID: jsCurModuleID
		},
		function(ReturnData)
		{
			jsCurModuleID = $('select#ModuleID').val();
			js_Reload_Subject_Selection(ParReload);
		}
	);
}

function js_Reload_Subject_Selection(ParReload)
{
	$('div#SubjectSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{ 
			Action: 'Subject_Selection',
			async: false,
			YearTermID: jsCurYearTermID,
			ModuleID: jsCurModuleID,
			SelectionID: 'SubjectID',
			OnChange: 'js_Changed_Subject_Selection(this.value)',
			SubjectID: jsCurSubjectID,
			CheckModuleSubject: 1,
			FirstTitle: '<?=$Lang['SysMgr']['SubjectClassMapping']['Select']['Subject']?>',
			NoFirst: 0,
			AccessibleSubjectOnly: 1
		},
		function(ReturnData)
		{
			jsCurSubjectID = $('select#SubjectID').val();
			if (jsCurSubjectID == '') {
				$('div.LevelSelectionDiv').html('');
				$('div#SubjectGroupSelectionDiv').html('');
				$('div#StudentSelectionDiv').html('');
				$('div#StudentMarksheetDiv').html('');
			}
			else {	
				js_Reload_Subject_Group_Selection(ParReload);
				js_Reload_Topic_Selection(jsLevel=1, '', '', 0);
			}
		}
	);
}

function js_Reload_Subject_Group_Selection(ParReload)
{
	var jsDisplayTermInfo = (jsCurYearTermID=='')? 1 : 0;
	
	$('div#SubjectGroupSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{ 
			Action: 'Subject_Group_Selection',
			async: false,
			SubjectID: jsCurSubjectID,
			YearTermID: jsCurYearTermID,
			SelectionID: 'SubjectGroupID',
			OnChange: 'js_Changed_SubjectGroup_Selection(this.value)',
			DisplayTermInfo: jsDisplayTermInfo,
			SubjectGroupID: jsCurSubjectGroupID
		},
		function(ReturnData)
		{
			// use the second value as the default selection if no specified
			if ($('select#SubjectGroupID').val() == '') {
				$('select#SubjectGroupID option:nth-child(2)').attr('selected', true);
			}
			jsCurSubjectGroupID = $('select#SubjectGroupID').val();
			js_Reload_Subject_Group_Student_Selection(ParReload);
		}
	);
}

function js_Reload_Subject_Group_Student_Selection(ParReload)
{
	$('div#StudentSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{ 
			Action: 'Subject_Group_Student_Selection',
			async: false,
			SubjectGroupID: jsCurSubjectGroupID,
			SelectionID: 'StudentID',
			OnChange: 'js_Changed_Student_Selection(this.value)',
			IsAll: 0,
			NoFirst: 1,
			StudentID: jsCurStudentID
		},
		function(ReturnData)
		{
			jsCurStudentID = $('select#StudentID').val();
			
			if(ParReload=='reload')
			{
				js_Reload_Student_Marksheet_Table();
			}
		}
	);
}

function js_Reload_Student_Marksheet_Table()
{
	jsCurModuleID = $('select#ModuleID').val();
	jsCurSubjectID = $('select#SubjectID').val();
	if ($('select#SubjectGroupID').val() == '')
	{
		$('select#SubjectGroupID option:nth-child(1)').attr('selected', true);
	}			
	jsCurSubjectGroupID = $('select#SubjectGroupID').val();					
    jsCurStudentID = $('select#StudentID').val();
    
	if (jsCurStudentID == '' || jsCurStudentID == null)
	{
		$('div#StudentMarksheetDiv').html('');
	}
	else
	{
		var FilterText = getFilterText();
						
		$('div#StudentMarksheetDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
			"ajax_reload.php", 
			{ 
				Action: 'Student_Marksheet_Table',
				async: false,
				YearTermID: jsCurYearTermID,
				ModuleID: jsCurModuleID,
				SubjectID: jsCurSubjectID,
				StudentID: jsCurStudentID,
				FilterTopicInfoText: FilterText
			},
			function(ReturnData)
			{
				// do not use super table if the settings are not completed
				if (document.getElementById('Btn_Save') != null)
				{
					js_Init_SuperTable();
				}
			}
		);
	}
}

function js_Save_Marksheet()
{
	Block_Element('StudentMarksheetDiv');
	
	var jsSubmitString = Get_Form_Values(document.getElementById('form1'));
	$.ajax({  
		type: "POST",  
		url: "ajax_update.php",
		async: false,
		data: jsSubmitString,  
		success: function(data) {
			if (data=='1')
			{
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>');
				Scroll_To_Top();
				
				js_Reload_Student_Marksheet_Table();
			}
			else
			{
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>');
				Scroll_To_Top();
			}
			
			UnBlock_Element('StudentMarksheetDiv');
		} 
	});  
	return false;  
}

function js_Apply_Mark_To_All_Topic(jsModuleID)
{
	var jsCounter = 0;
	$('select.GlobalModule_' + jsModuleID).each(function(){
		jsCounter++;
		
		if (jsCounter == 2)	// SuperTable will clone table and therefore there will be more than 1 apply to all selection
			$('select.ChkModuleID_' + jsModuleID + ':enabled').val($(this).val());
	});
}

function js_Update_GlobalModule_Selection(jsID, jsValue)
{
	//alert($('select#' + jsID).val());
}

function js_Init_SuperTable()
{
    $("table#ContentTable").toSuperTable({
    	width: jsTableWidth + "px", 
    	height: "500px", 
    	fixedCols: '<?=$eRC_Rubrics_ConfigArr['MaxLevel']?>',
    	headerRows :3,
    	onFinish: function(){ fixSuperTableHeight('ContentTable', 400);  $(".sSky").css("font-size","0.9em");}
	});
}

function js_Changed_Topic_Selection(jsLevel, jsPreviousLevelTopicID, jsTargetTopicID, jsReloadTable)
{
	var jsOriginalLevel = jsLevel - 1;
	js_Reload_Topic_Selection(jsLevel, jsPreviousLevelTopicID, jsTargetTopicID, jsReloadTable);
}

function js_Reload_Topic_Selection(jsLevel, jsPreviousLevelTopicID, jsTargetTopicID, jsReloadTable)
{
	if (jsTargetTopicID == null) {
		jsTargetTopicID = '';
	}
	if (jsReloadTable == null) {
		jsReloadTable = 1;
	}
	
	// Hide the all Next Level Topic Selection
	var i;
	for (i=jsLevel; i<jsMaxTopicLevel; i++)
	{
		$('div#Level' + i + '_SelectionDiv').html('');
	}
	
	if (jsLevel < jsMaxTopicLevel)
	{
		if (jsLevel==1 || (jsLevel > 1 && jsPreviousLevelTopicID != ''))
		{
			js_Reload_Individual_Topic_Selection(jsLevel, jsPreviousLevelTopicID, jsTargetTopicID);
		}
		else
		{
			$('#StudentMarksheetDiv').html('');
		}
	}
	
	if (jsReloadTable==1)
	{
		js_Reload_Student_Marksheet_Table();
	}
}

function js_Reload_Individual_Topic_Selection(jsLevel, jsPreviousLevelTopicID, jsTargetTopicID)
{
	var FilterText = getFilterText();
	var TopicVal ='';
	
	var jsNextLevel = parseInt(jsLevel) + 1;
	
	var AllTitle = '';
	var SelectTitle ='';
	var jsReloadTableAfterOnChange = 1;
	
	if(jsLevel==1)
	{
		jsReloadTableAfterOnChange = 0;
	}
	
	$('div#Level' + jsLevel + '_SelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{
			Action: 'Reload_Topic_Selection',
			async: false,
			SubjectID: jsCurSubjectID,
			Level: jsLevel,
			noFirst: '1',
			hasChooseOption: true,
			PreviousLevelTopicID: jsPreviousLevelTopicID,
			SelectionID: 'Level' + jsLevel + '_TopicID',
			OnChange: 'js_Changed_Topic_Selection(' + jsNextLevel + ', this.value, \'\', ' + jsReloadTableAfterOnChange + ');',
			TopicID: jsTargetTopicID
		},
		function(ReturnData)
		{	
			FilterText = getFilterText();
			TopicVal = getSelectVal(1);
			   			
			if(TopicVal!='')
			{
				js_Reload_Student_Marksheet_Table();
			} 	
			else
			{
				$('div#StudentMarksheetDiv').html('');
			}
			
			if(jsPreviousLevelTopicID=='ALL')
			{
				$('div#Level'+jsLevel+'_SelectionDiv').html('');
			}
			
			//	Blind_Cookies_To_Object();
		}
	);
	
	if(jsPreviousLevelTopicID=='ALL')
	{
		$('div#Level'+jsLevel+'_SelectionDiv').html('');
	}
}

function getFilterText()
{	
	var i = 1;
	var seperator = '';
	var FilterText = '';
	
	for(i=1; i<=jsMaxTopicLevel;i++)
	{
		FilterText += seperator+getSelectVal(i);
		seperator = ":_:";
	}
	
	return FilterText;
}

function getSelectVal(ParLevel)
{
	var Val = $('select#Level'+ParLevel+'_TopicID').val();			
	if($('select#Level'+ParLevel+'_TopicID').val()==null)
	{
		Val='';
	}
	return Val;
}

var jsPreviousClickedObjId = '';
function showShortCutPanel(objID,thisModuleID,thisTopicID) { 
	var shortCutPanelId = 'DescDivID';
	
	if (jsPreviousClickedObjId != objID || $('div#' + shortCutPanelId).css('visibility') == 'hidden') {
		// if clicked another icon or not visible => load the layer
		var shortCutPanel = document.getElementById(shortCutPanelId);
		var jsOffsetLeft, jsOffsetTop;	
		
		jsOffsetLeft = -20;
		var ua = $.browser;
		if ( ua.mozilla ) {
			// jsOffsetLeft = 550;
		}
		jsOffsetTop = 20;
		
		var jsCounter = 0;
		var posleft='';
		var postop ='';
		
// 		var panelWidth = document.getElementById(shortCutPanelId).style.width;
// 		jsOffsetLeft = parseInt(panelWidth);
		
		$('.' + objID).each(function(){		// id and class is the same for the object 
			jsCounter++;
			
			if (jsCounter == 2) {	// SuperTable will clone table and therefore there will be more than 1 apply to all selection
				 posleft = $(this).offset().left - jsOffsetLeft;
				 postop = $(this).offset().top + jsOffsetTop;
				 return;
			}
		});
		
		document.getElementById(shortCutPanelId).style.left = posleft + "px";
		document.getElementById(shortCutPanelId).style.top = postop + "px";
		
		MM_showHideLayers(shortCutPanelId,'','show'); 
		$('div#DescDivContent').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
			"ajax_reload.php",  
			{ 
				async: false,
	  			Action: "Comment_Bank",
	  			TopicID: thisTopicID,
	  			ModuleID: thisModuleID
			},
			function(returnString)
			{
				if(returnString=='') {
					$('div#' + shortCutPanelId).html('<?=$Lang['General']['EmptySymbol']?>');
				}	
			}
		);
	}
	else {
		// if visible => hide the layer
		MM_showHideLayers(shortCutPanelId,'','hide');
	}
	
	jsPreviousClickedObjId = objID;
}

function js_InsertCommentToTextBox(ParModuleID,ParTopicID,ParCommentID)
{
	$.post(
  			"ajax_reload.php",
  			{
  				async: false,
  				Action: "GetCommentByID",
  				CommentID: ParCommentID
  			},
  			function(result)
  			{
  				$("input#SubjectLevelRemarkID_"+ParModuleID+"_"+ParTopicID).val(result).focus();
//   			$("input.SubjectLevelRemarkID_"+ParModuleID+"_"+ParTopicID).each(function(){
// 					$(this).val(result).focus();
// 				});
  				
  				MM_showHideLayers('DescDivID','','hide'); 
  			});
}

function js_Go_Import()
{
	document.form1.action = 'import.php';
	document.form1.submit();
}
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>