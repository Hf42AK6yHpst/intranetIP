<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");


intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	
	# Temp
	//$ReportCardCustomSchoolName = "st_stephen_college";
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	$linterface = new interface_html();
	$fs = new libfilesystem();
	
	
	$filename = $YearTermID."_".$YearID."_".$YearClassID."_unicode.csv";

	$folder_prefix = $intranet_root."/file/reportcard2008/".$lreportcard->schoolYear;
	$tmp_folder_prefix = $folder_prefix."/tmp";
	
	if (!file_exists($folder_prefix))
		$fs->folder_new($folder_prefix);
	$folder_prefix .= "/".$UploadType;
	$fs->folder_new($folder_prefix);	

	$success['move'] = $fs->lfs_move($filepath, stripslashes($folder_prefix."/".$filename));
	
	$success['remove'] = $fs->lfs_remove($tmp_folder_prefix);
	
	$SuccessMsg = !$success['move']?$eReportCard['ImportOtherInfoFail']:$eReportCard['ImportOtherInfoSuccess'];
	
	# BackBtn
	$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='index.php?UploadType=$UploadType'","back"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
	
	### Title ###
	# Step Obj 
	$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
	$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
	$STEPS_OBJ[] = array($i_general_imported_result, 1);
	
	# tag information
	$TAGS_OBJ =array();
	$TAGS_OBJ = $lreportcard->getOtherInfoTabObjArr($UploadType);

	# page navigation (leave the array empty if no need)
	$PAGE_NAVIGATION[] = array($button_upload, "");
	
	$CurrentPage = "Management_OtherInfo";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	$linterface->LAYOUT_START();  
	
	?>
	<br>
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
		</tr>
		<tr>
			<td class='tabletext' align='center'><br><?=$SuccessMsg?><br></td>
		</tr>
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td colspan="2" align="center"><?=$BackBtn?></td>
		</tr>
	</table>
	
	<?
	$linterface->LAYOUT_STOP();

}  else {
	echo "You have no priviledge to access this page.";
}

?>