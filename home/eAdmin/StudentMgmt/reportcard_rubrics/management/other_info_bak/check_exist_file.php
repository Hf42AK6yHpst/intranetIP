<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
intranet_auth();
if ($plugin['ReportCard2008']) {
	if (isset($UploadType) && isset($YearID) && isset($YearClassID) && isset($AcademicYear) && isset($YearTermID)) {
		$folder_prefix = $intranet_root."/file/reportcard2008/".$AcademicYear."/".$UploadType;
		$filename = $YearTermID."_".$YearID."_".$YearClassID."_unicode.csv";
		$file = $folder_prefix."/".$filename;
		if (file_exists($file))
			echo 0;
		else
			echo 1;
	} else {
	 echo -1;
	}
} else {
	echo -1;
}
?>