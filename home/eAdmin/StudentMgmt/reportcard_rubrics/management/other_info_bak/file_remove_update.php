<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

intranet_auth();
intranet_opendb();

if (!isset($CsvFileName) || $CsvFileName=="" || !isset($UploadType) || $UploadType=="")
	header("Location: index.php?Result=delete_failed");

$lreportcard = new libreportcard();

# updated on 3 Dec 2008 by Ivan
# do not hardcode the school year
//$schoolYear = $lreportcard->schoolYear = "2007";
#######################################################
function removeFiles($ParAll, $ParFileName="") {
	//global $fs, $intranet_root, $RecordType, $admin_root_path, $UploadType, $schoolYear;
	global $fs, $intranet_root, $RecordType, $admin_root_path, $UploadType, $lreportcard;
	
	//$schoolYear = substr($ParFileName,0,4);
	$TargetFolderPath = $intranet_root."/file/reportcard2008/".$lreportcard->schoolYear."/";
	$TargetFolderPath = $TargetFolderPath.$UploadType;
	
	if (file_exists($TargetFolderPath)) {
		$handle = opendir($TargetFolderPath);
		if($ParAll==1) {
			while (($file = readdir($handle))!==false) {
				if($file!="." && $file!="..")
				{
					$filepath = $TargetFolderPath."/".$file;
					$success = $fs->file_remove($filepath);
				}
			}
		} else {
			$filepath = $TargetFolderPath."/".$ParFileName;
			if(file_exists($filepath)) {
				$success = $fs->file_remove($filepath);
			}
		}
	}
	return $success;
}
#######################################################
$fs = new libfilesystem();

$success = array();
for($i=0; $i<sizeof($CsvFileName); $i++) {
	$success[] = removeFiles(0, $CsvFileName[$i]);
}

intranet_closedb();

$Result = (!in_array(false, $success)) ? "delete" : "delete_failed";
header("Location: index.php?Result=$Result&UploadType=$UploadType");
?>