<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!isset($UploadType) || $UploadType=="")
	header("Location: index.php");

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	include_once($PATH_WRT_ROOT."includes/libreportcard2008_ui.php");
	
	# Temp
	//$ReportCardCustomSchoolName = "st_stephen_college";
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	$lreportcard_ui = new libreportcard_ui();
	
	if (!in_array($UploadType, $lreportcard->getOtherInfoType()))
		header("Location: index.php");
	
	# Step Obj 
	$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
	$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
	$STEPS_OBJ[] = array($i_general_imported_result, 0);
	
	$CurrentPage = "Management_OtherInfo";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	
	include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
	$ObjYear = new academic_year($lreportcard->schoolYearID);
	$yearDisplay = $ObjYear->Get_Academic_Year_Name();
	
	### Semester Selection
	if ($YearTermID == '')
	{
		$semester_data = getSemesters($lreportcard->schoolYearID, 0);
		$curSemInfoArr = getCurrentAcademicYearAndYearTerm();
		$YearTermID = $curSemInfoArr['YearTermID'];
	}
	$semesterSelect = $lreportcard_ui->Get_Term_Selection('YearTermID', $lreportcard->schoolYearID, $YearTermID, $OnChange='', $NoFirst=1, $WithWholeYear=1);
	
	### Form Selection
	$Onchange = "js_Reload_Class_Selection(this.value);";
	$checkClassTeacher = ($ck_ReportCard_UserType=="ADMIN")? 0 : 1;
	$formSelect = $lreportcard_ui->Get_Form_Selection('YearID', $YearID, $Onchange, $noFirst=1, $isAll=0, $hasTemplateOnly=0, $checkClassTeacher);
	
	
	$SampleCSV = "get_sample_csv.php?UploadType=".$UploadType;
	
	if ($lreportcard->hasAccessRight())
    {
         $linterface = new interface_html();

################################################################
		
		# generate table
		$UploadTableRow = "";
		$UploadTableRow .= "<tr>
								<td valgin='top' class='tabletext formfieldtitle' width='30%'>".$i_Profile_Year."</td>
								<td valgin='top' class='tabletext'>".$yearDisplay."</td>
							</tr>";
		$UploadTableRow .= "<tr>
								<td valgin='top' class='tabletext formfieldtitle' width='30%'>".$i_Profile_Semester."</td>
								<td valgin='top'>".$semesterSelect."</td>
							</tr>";
		$UploadTableRow .= "<tr>
								<td valgin='top' class='tabletext formfieldtitle' width='30%'>".$eReportCard['Form']."</td>
								<td valgin='top'>".$formSelect."</td>
							</tr>";
		$UploadTableRow .= "<tr>
								<td valgin='top' class='tabletext formfieldtitle' width='30%'>".$i_ClassName."</td>
								<td valgin='top'><div id='ClassSelectionDiv'></div></td>
							</tr>";
		$UploadTableRow .= "<tr>
								<td valgin='top' class='tabletext formfieldtitle' width='30%'>".$eReportCard['File']."</td>
								<td valgin='top'><input class=\"textboxtext\" type='file' name='userfile' size=\"25\"  maxlength=\"255\">
							";
		
		if($g_encoding_unicode) {
			$UploadTableRow .= "<span class='tabletextremark'>$i_import_utf_type</span>";
		}
		
		$UploadTableRow .= "</tr>";
		$UploadTableRow .= "<tr>
								<td nowrap='nowrap' colspan='2'>
									<a target='_blank' href='".$SampleCSV."' class='tablelink'>
										<img src='".$PATH_WRT_ROOT."/images/{$LAYOUT_SKIN}/icon_files/xls.gif' border='0' align='absmiddle' hspace='3'>".
										$i_general_clickheredownloadsample."
									</a>
								</td>
							</tr>";
		
		$ButtonHTML = $linterface->GET_ACTION_BTN($button_submit, "button", "javascript:checkForm();", "btnSubmit");
		$ButtonHTML .= "&nbsp;".$linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back();");

################################################################
		${"link".$UploadType} = 1;
		
		# tag information
		$TAGS_OBJ =array();
		$TAGS_OBJ = $lreportcard->getOtherInfoTabObjArr($UploadType);

		# page navigation (leave the array empty if no need)
		$PAGE_NAVIGATION[] = array($button_upload, "");

		$linterface->LAYOUT_START();
		
		if (isset($WrongRow) && $WrongRow != "") {
			$WrongRowArr = explode(" ", $WrongRow);
			$WrongRowMsg = implode(", ", $WrongRowArr);
			$SysMsg = sprintf($i_con_msg_wrong_row, $WrongRowMsg);
			$SysMsg = $linterface->GET_SYS_MSG("", $SysMsg);
		} else {
			$SysMsg = $linterface->GET_SYS_MSG($Result);
		}
?>
<script language="javascript">
$(document).ready( function() {	
	js_Reload_Class_Selection($('select#YearID').val(), '<?=$YearClassID?>');
});

function js_Reload_Class_Selection(jsYearID, jsYearClassID)
{
	if (jsYearClassID==null)
		jsYearClassID ='';
		
	$('div#ClassSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{
			Action: 'Class_Selection',
			YearID: jsYearID,
			YearClassID: jsYearClassID,
			SelectionID: 'YearClassID',
			CheckClassTeacher: 1,
			IsAll: 1
		},
		function(ReturnData)
		{
			
		}
	);
}

function trim(str) {
	return str.replace(/^\s+|\s+$/g,"");
}


function checkForm() {
	obj = document.form1;
	if (trim(obj.elements["userfile"].value) == "") {
		 alert('<?=$eReportCard['AlertSelectFile']?>');
		 obj.elements["userfile"].focus();
		 return false;
	}
	
	var jsYearTermID = $('select#YearTermID').val();
	var jsYearID = $('select#YearID').val();
	var jsYearClassID = $('select#YearClassID').val();
	
	if (jsYearClassID == '')
		jsYearClassID = 0;
		
	$.post(
		"check_exist_file.php", 
		{ 
			UploadType: '<?=$UploadType?>',
			AcademicYear: '<?=$lreportcard->schoolYear?>',
			YearTermID: jsYearTermID,
			YearID: jsYearID,
			YearClassID: jsYearClassID
		},
		function(ReturnData)
		{
			if (ReturnData != '1')
			{
				if (confirm('<?=$eReportCard['AlertExistFile']?>'))
				{
					document.getElementById('form1').submit();
				}
				else
				{
					return false;
				}
			}
			else
			{
				document.getElementById('form1').submit();
			}
		}
	);
}
</script>

<form id="form1" name="form1" enctype="multipart/form-data" action="file_upload_confirm.php" method="post">
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr><td align="right" colspan="<?=$col_size?>"><?= $SysMsg ?></td></tr>
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td>
		<table width="90%" border="0" cellpadding="4" cellspacing="1" align="center">
			<?=$UploadTableRow?>
		</table>
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center"><?=$ButtonHTML?></td>
		</tr>
	</table>
	</td>
</tr>
</table>
<input type="hidden" name="UploadType" value="<?=$UploadType?>" />
</form>
<?
        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
