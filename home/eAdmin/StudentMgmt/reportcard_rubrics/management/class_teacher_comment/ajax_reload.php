<?php
// using ivan
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");

$lreportcard_ui = new libreportcardrubrics_ui();

switch ($Action)
{
	
	case "ReloadCommentTable":
		$ReportID = stripslashes($_REQUEST['ReportID']);
		$arrCookies[] = array("ck_settings_class_teacher_comment_report_id", "ReportID");
		updateGetCookies($arrCookies);		
		echo $lreportcard_ui->Get_Management_Class_Teacher_Comment_Table($ReportID);
	break;
	
	case "AutoComplete":
		include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_comment.php");
		$lreportcard_comment = new libreportcardrubrics_comment();
		
		$q = trim(stripslashes($_REQUEST['q']));
		$SubjectID = stripslashes($_REQUEST['SubjectID']);
		$result = $lreportcard_comment->Get_Comment_Bank_Comment($q,$SubjectID);
		foreach((array)$result as $matches)
		{
			list($code,$CommentEn,$CommentCh) = $matches;
			if($CommentEn)
			{
				$CommentEn = str_replace("\n",'<br>',$CommentEn);
				$val = $CommentEn;
				$display = $code." ".$CommentEn;
				
				$row .= $val."|".$display."\n"; 
			}
			if($CommentCh)
			{
				$CommentCh = str_replace("\n",'<br>',$CommentCh);
				$val = $CommentCh;
				$display = $code." ".$CommentCh;
				
				$row .= $val."|".$display."\n";
			}
				
		}
		
		echo $row;
	break;
	
	case "ReloadClassSelection":
		$ClassLevelID = stripslashes($_REQUEST['ClassLevelID']);
		$ClassSelection = $lreportcard_ui->Get_Class_Selection("ClassID", $ClassID, '', $noFirst=0, $isAll=1, $firstTitle='',$ClassLevelID, $TeachingClassOnly=1);

		echo $ClassSelection;
	break;
	
	case "Comment_Table":
		$Keyword = stripslashes($_REQUEST['Keyword']);
		echo $lreportcard_ui->Get_Management_ClassTeacherComment_CommentView_Table($Keyword);
	break;
	
}
intranet_closedb();
?>