<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Management_ClassTeacherComment";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_comment.php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$MODULE_OBJ['title'] = $iDiscipline['select_students'];
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$permitted[] = 2;
$academicYearID = Get_Current_Academic_Year_ID();

$li = new libgrouping();

if($CatID < 0){
     unset($ChooseGroupID);
     $ChooseGroupID[0] = 0-$CatID;
}
$lgroupcat = new libgroupcategory();
$cats = $lgroupcat->returnAllCat();

$x1  = ($CatID!=0 && $CatID > 0) ? "<select name='CatID' id='CatID' onChange='checkOptionNone(this.form.elements[\"ChooseGroupID[]\"]);this.form.submit()'>\n" : "<select name='CatID' id='CatID' onChange='this.form.submit()'>\n";
$x1 .= "<option value='0'></option>\n";
for ($i=0; $i<sizeof($cats); $i++)
{
     list($id,$name) = $cats[$i];
     if ($id!=0)
     {
         $x1 .= "<option value='$id' ".(($CatID==$id)?"SELECTED":"").">$name</option>\n";
     }
}

$x1 .= "<option value='0'></option>\n";
$x1 .= "</select>";

if($CatID!=0 && $CatID > 0) {
	$row = $li->returnCategoryGroups($CatID);
    
    if ($CatID==3) // Classes Category
    {
    	// display the classes of specific form only
    	$FormObj = new Year($YearID);
    	$CheckClassTeacher = ($ck_ReportCard_Rubrics_UserType=="ADMIN")? 0 : 1;
    	$YearClassInfoArr = $FormObj->Get_All_Classes($CheckClassTeacher);
    	$YearClassGroupIDArr = Get_Array_By_Key($YearClassInfoArr, 'GroupID');
    }
    
    $x2  = "<select name='ChooseGroupID[]' size='7' multiple>\n";
     for($i=0; $i<sizeof($row); $i++){
	    $GroupCatID = $row[$i][0];
        $GroupCatName = $row[$i][1];
        
        // display the classes of specific form only
        if ($CatID==3 && in_array($GroupCatID, $YearClassGroupIDArr)==false)
        	continue;
     	
     	 $x2 .= "<option value='$GroupCatID'";
          for($j=0; $j<sizeof($ChooseGroupID); $j++){
          $x2 .= ($GroupCatID == $ChooseGroupID[$j]) ? " SELECTED" : "";
          }
          $x2 .= ">$GroupCatName</option>\n";
     }
//      $x2 .= "<option>";
//      for($i = 0; $i < 40; $i++)
//      $x2 .= "&nbsp;";
//      $x2 .= "</option>\n";
     $x2 .= "</select>\n";
}

if(isset($ChooseGroupID)) {
	$OwnClassStudentOnly = ($lreportcard->IS_ADMIN_USER($_SESSION['UserID']))? 0 : 1;
	$row = $li->returnGroupUsersInIdentity($ChooseGroupID, $permitted, $YearID, $OwnClassStudentOnly);		
	
	 $x3  = "<select name='ChooseUserID[]' size='15' multiple>\n";
     for($i=0; $i<sizeof($row); $i++)
     $x3 .= "<option value='".$row[$i][0]."'>".$row[$i][1]."</option>\n";
//      $x3 .= "<option>";
//      for($i = 0; $i < 40; $i++)
//      $x3 .= "&nbsp;";
//      $x3 .= "</option>\n";
     $x3 .= "</select>\n";
}

$step1 = getStepIcons(3,1,$i_SelectMemberSteps);
$step2 = getStepIcons(3,2,$i_SelectMemberSteps);
$step3 = getStepIcons(3,3,$i_SelectMemberSteps);
?>

<script language="javascript">
function AddOptions(obj){
     par = opener.window;
     parObj = opener.window.document.form1.elements["<?php echo $fieldname; ?>"];
     x = (obj.name == "ChooseGroupID[]") ? ((document.getElementById('CatID').value==3) ? "C" : "G") : "U";
     checkOption(obj);
     par.checkOption(parObj);
     i = obj.selectedIndex;
     while(i!=-1){
          par.checkOptionAdd(parObj, obj.options[i].text, x + obj.options[i].value);
          obj.options[i] = null;
          i = obj.selectedIndex;
     }
     par.Update_Auto_Complete_Extra_Para();
     //par.checkOptionAdd(parObj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
}

function SelectAll(obj)
{
     for (i=0; i<obj.length; i++)
     {
          obj.options[i].selected = true;
     }
}
</script>

<form name="form1" action="choose_student_popup.php" method="get">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
			<br />
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td>
						<table width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr>
								<td valign="top" nowrap="nowrap" width="30%" class='formfieldtitle'>
									<span class="tabletext"><?=$i_frontpage_campusmail_select_category?></span>
								</td>
								<td >
									<?=$x1?>
								</td>
							</tr>
			                <?php if($CatID!=0 && $CatID > 0) { ?>
				                <tr>
									<td valign="top" nowrap="nowrap" width="30%" class='formfieldtitle'>
									<span class="tabletext"><?=$i_frontpage_campusmail_select_group?></span>
									</td>
									<td >
						                <table border='0' cellspacing='1' cellpadding='1'>
					    					<tr>
					                        	<td><?=$x2?></td>
					                        	<td valign="bottom">
													<?= $linterface->GET_SMALL_BTN($button_add, "button","checkOption(this.form.elements['ChooseGroupID[]']);AddOptions(this.form.elements['ChooseGroupID[]'])") . "<br>"; ?>
					                                <?= $linterface->GET_SMALL_BTN($i_frontpage_campusmail_expand, "submit","checkOption(this.form.elements['ChooseGroupID[]'])") . "<br>"; ?>
					                                <?= $linterface->GET_SMALL_BTN($button_select_all, "submit","SelectAll(this.form.elements['ChooseGroupID[]']); return false;") . "<br>"; ?>
												</td>
											</tr>
					                    </table>                                               
									</td>
								</tr>
			                <?php } ?>
			                
			                <?php if(isset($ChooseGroupID)) { ?>
				                <tr>
									<td valign="top" nowrap="nowrap" width="30%" class='formfieldtitle'>
										<span class="tabletext"><?=$i_frontpage_campusmail_select_user?></span>
									</td>
									<td >
						                <table border='0' cellspacing='1' cellpadding='1'>
					    					<tr>
					                        	<td><?=$x3?></td>
					                        	<td valign="bottom">
					                            	<?= $linterface->GET_SMALL_BTN($button_add, "button","checkOption(this.form.elements['ChooseUserID[]']);AddOptions(this.form.elements['ChooseUserID[]'])") . "<br>"; ?>
													<?= $linterface->GET_SMALL_BTN($button_select_all, "submit","SelectAll(this.form.elements['ChooseUserID[]']); return false;") . "<br>"; ?>
												</td>
											</tr>
					                    </table>                                        
									</td>
								</tr>
			                <?php } ?>
				
				                
			                <tr>
			                	<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			                </tr>
			                <tr>
								<td align="center" colspan="2">
									<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();") ?>
								</td>
							</tr>
				    	</table>
			    	</td>
				</tr>
		    </table>
		</td>
	</tr>
</table>        

<input type="hidden" name="fieldname" value="<?= $fieldname ?>">
<input type="hidden" name="YearID" value="<?= $YearID ?>">
</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();

?>