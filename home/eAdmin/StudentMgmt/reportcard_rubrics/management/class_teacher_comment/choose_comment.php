<?php
/***********************************************
 * 	20100112 Marcus:
 * 		- fixed comment selection box
 * *********************************************/
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Management_ClassTeacherComment";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_comment.php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$lreportcard_comment = new libreportcardrubrics_comment();

$MODULE_OBJ['title'] = $Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['SelectComment'];
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$CommentArr = $lreportcard_comment->Get_Comment_Bank_Comment('');

$CommentChiArr = Get_Array_By_Key($CommentArr,"CommentCh");
$CommentEngArr = Get_Array_By_Key($CommentArr,"CommentEn");

# prepare Options of selection box (multiline comment are separated into different options with same value)
$idx=0;	
foreach	((array)$CommentEngArr as $thisComment)
{
	$tmpOption = explode("\n",$thisComment);
	foreach((array)$tmpOption as $thisOptionPiece)
	{
		if ($eRCCommentSetting['ShowCompleteSentence'])
			$thisOptionPieceShort = trim($thisOptionPiece);
		else
			$thisOptionPieceShort = trim(mb_substr($thisOptionPiece, 0, 30)).(mb_strlen($thisOptionPiece)>30?'...':'');
		$tmpCommentEngOptions[] = array($idx,$thisOptionPieceShort);
	}
	$jsOption = str_replace(chr(13), ' ', $thisComment); // replace carriage return
	$jsOption = str_replace("\n","\\n",$jsOption); // replace new line
	$jsOption = str_replace('"','\\"',$jsOption); // addslashes to double quote
	$jsEngObjPieces[] = '"'.$jsOption.'"';
	$idx++; 
}
$idx=0;	
foreach	((array)$CommentChiArr as $thisComment)
{
	$tmpOption = explode("\n",$thisComment);
	foreach((array)$tmpOption as $thisOptionPiece)
	{
		if ($eRCCommentSetting['ShowCompleteSentence'])
			$thisOptionPieceShort = trim($thisOptionPiece);
		else
			$thisOptionPieceShort = trim(mb_substr($thisOptionPiece, 0, 30)).(mb_strlen($thisOptionPiece)>30?'...':'');
		$tmpCommentOptions[] = array($idx,$thisOptionPieceShort);
	}
	$jsOption = str_replace(chr(13), ' ', $thisComment); // replace carriage return
	$jsOption = str_replace("\n","\\n",$jsOption); // replace new line
	$jsOption = str_replace('"','\\"',$jsOption); // addslashes to double quote
	$jsObjPieces[] =  '"'.$jsOption.'"';
	$idx++;
}
$select_comment = $linterface->GET_SELECTION_BOX($tmpCommentOptions, 'name="Comment_b5[]" id="Comment_b5" size="21" multiple="multiple" onchange="" style="display:none; width:180px"', '');
$select_comment_eng = $linterface->GET_SELECTION_BOX($tmpCommentEngOptions, 'name="Comment_en[]" id="Comment_en" size="21" multiple="multiple" onchange="" style="display:block; width:180px"', '');

# build jsObj for return comment by value 
$jsObjectStr = "new Array(".implode(",",(array)$jsObjPieces).")";
$jsEngObjectStr = "new Array(".implode(",",(array)$jsEngObjPieces).")";
		
?>

<script language="javascript">
var chiComment = <?=$jsObjectStr?>;
var engComment = <?=$jsEngObjectStr?>;

function isExists(obj2,val){
	for(j=0;j<obj2.options.length;j++) {
		if(obj2.options[j].value==val) return true;
	}
	return false;
}
function AddOptions(){
	
	var jObj = returnActiveDropDownListObj();
	options = jObj.children("option:selected"); //get selected options
	var lang = $("[name=Language]:checked").val(); 
	if(lang=='eng')
		var comment = engComment;
	else
		var comment = chiComment;
     par = opener.window;

     parObj = $(par.window.document).find("textarea#<?=$fieldname?>");
     
	 var optionAdded = new Array();
	 
	 lastadded = '';
     jQuery.each(options,function(idx,optionObj){
     	if($(optionObj).val()!=lastadded) //if the comment have not been added in this round 
     	{
     		addtext = '';
//     		if (Trim(parObj.value) != '')
//     			addtext += "\n";
     		addtext += Trim(comment[$(optionObj).val()]); 
			var tmp = $(parObj).val();
			$(parObj).val(tmp+addtext+"\n");
			lastadded = $(optionObj).val();	
		}
     });
    // options.remove();
}

function SelectAll() {
	
	var jObj = returnActiveDropDownListObj();
	jObj.children().attr("selected",true);
}

function jsChangeLang() {
	var lang_eng = document.getElementById("lang_eng");
	var lang_chi = document.getElementById("lang_chi");
	var CommentSelectCell = document.getElementById("CommentSelectCell");
	
	if(lang_eng.checked) 
	{
		$("#Comment_b5").hide();
		$("#Comment_en").show();
	}
	else
	{
		$("#Comment_b5").show();
		$("#Comment_en").hide();
	}
}


function returnActiveDropDownListObj(){
	var lang = $("[name=Language]:checked").val();
	if(lang=='eng')
		jObj = $("#Comment_en")
	else
		jObj = $("#Comment_b5")
	return jObj;
}

$().ready(function(){
	jsChangeLang();
	$("#Comment_en").change(SelectOptions);
	$("#Comment_b5").change(SelectOptions);
});

function SelectOptions(evt)
{
	//loop each selected options to found if there are any other options with the same value with it
	//if so, also select those options (as they are the same multiple line comment )
	var jObj = returnActiveDropDownListObj();
	jQuery.each(jObj.children("option:selected"),function(idx,selectedOption){
		 thisVal = $(selectedOption).val(); 
		 jObj.children("[value="+thisVal+"]").attr("selected",true);
     })
}
</script>
<br />
<form name="form1" action="index.php" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class='tabletext'>
						<?=$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Language']?>:
					</td>
					<td width="80%" class='tabletext'>
						<input type="radio" name="Language" id="lang_eng" value="eng" onclick="jsChangeLang()" <?=(!isset($Language) || $Language=="eng") ? "checked" : ""?> />
						<label for="lang_eng"><?=$i_general_english?></label> 
						<input type="radio" name="Language" id="lang_chi" value="chi" onclick="jsChangeLang()" <?=($Language=="chi") ? "checked" : ""?> />
						<label for="lang_chi"><?=$i_general_chinese?></label>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class='tabletext'>
						<?=$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Comment']?>:
					</td>
					<td width="80%">
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td id="CommentSelectCell"><?= $select_comment_eng ?><?= $select_comment?></td>
								<td valign="bottom">
									<table width="100%" border="0" cellspacing="0" cellpadding="6">
										<tr> 
											<td align="left"> 
												<?= $linterface->GET_SMALL_BTN($button_add, "button", "AddOptions()")?>
											</td>
										</tr>
										<tr> 
											<td align="left"> 
												<?= $linterface->GET_SMALL_BTN($button_select_all, "button", "SelectAll(); return false;")?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
                	<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
				<tr>
					<td align="center" colspan="2">
						<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="fieldname" value="<?php echo $fieldname; ?>" />
</form>
</br />
<?
		intranet_closedb();
        $linterface->LAYOUT_STOP();

?>