<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Management_ClassTeacherComment";

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_comment.php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$linterface = new interface_html();
$lreportcard_ui = new libreportcardrubrics_ui();

$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
       
# tag information
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Student'], "index.php", 0);
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Comment'], "index_comment.php", 1);

$linterface->LAYOUT_START();
	
$Keyword = stripslashes($_POST['Keyword']);
$CommentIDArr = $_POST['CommentIDArr'];

echo $lreportcard_ui->Include_JS_CSS();
echo $lreportcard_ui->Get_Management_ClassTeacherComment_CommentView_ChooseStudent_UI($CommentIDArr, $Keyword);
?>

<script language="javascript">

var AutoCompleteObj_ClassNameClassNumber;
var AutoCompleteObj_UserLogin;
var jsCurYearID;
$(document).ready( function() {
	
	// initialize jQuery Auto Complete plugin
	Init_JQuery_AutoComplete('UserClassNameClassNumberSearchTb', 'UserLoginSearchTb');

	// initialize the report selection
	jsCurYearID = $('select#YearID').val();
	js_Reload_Selection(jsCurYearID);
	
	$('input#UserLoginSearchTb').focus();
});

function Init_JQuery_AutoComplete(InputID_ClassNameClassNumber, InputID_UserLogin) {
	
	AutoCompleteObj_ClassNameClassNumber = $("input#" + InputID_ClassNameClassNumber).autocomplete(
		"ajax_search_user_by_classname_classnumber.php",
		{  			
			onItemSelect: function(li) {
				Add_Selected_User(li.extra[1], li.selectValue, InputID_ClassNameClassNumber);
			},
			formatItem: function(row) {
				return row[0];
			},
			maxItemsToShow: 5,
			minChars: 1,
			delay: 0,
			width: 200
		}
	);
	
	AutoCompleteObj_UserLogin = $("input#" + InputID_UserLogin).autocomplete(
		"ajax_search_user_by_login.php",
		{  			
			onItemSelect: function(li) {
				Add_Selected_User(li.extra[1], li.selectValue, InputID_UserLogin);
			},
			formatItem: function(row) {
				return row[0];
			},
			maxItemsToShow: 5,
			minChars: 1,
			delay: 0,
			width: 200
		}
	);
	
	Update_Auto_Complete_Extra_Para();
}

function Add_Selected_User(UserID, UserName, InputID) {
	var UserID = UserID || "";
	var UserName = UserName || "";
	var UserSelected = document.getElementById('SelectedUserIDArr[]');

	UserSelected.options[UserSelected.length] = new Option(UserName, UserID);
	
	Update_Auto_Complete_Extra_Para();
	
	// reset and refocus the textbox
	$('input#' + InputID).val('').focus();
}

function Update_Auto_Complete_Extra_Para() {
	ExtraPara = new Array();
	ExtraPara['SelectedUserIDList'] = Get_Selection_Value('SelectedUserIDArr[]', 'Array', true);
	ExtraPara['YearID'] = $('select#YearID').val();
	
	AutoCompleteObj_ClassNameClassNumber[0].autocompleter.setExtraParams(ExtraPara);
	AutoCompleteObj_UserLogin[0].autocompleter.setExtraParams(ExtraPara);
}

function js_Reload_Selection(jsYearID)
{
	if (Get_Selection_Value('SelectedUserIDArr[]', 'Array', true) != '')
	{
		if (confirm("<?=$Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['jsConfirmArr']['SelectedStudentWillBeRemoved']?>"))
		{
			// remove selected students
			checkOptionAll(document.getElementById('SelectedUserIDArr[]'));
			checkOptionRemove(document.getElementById('SelectedUserIDArr[]'));
		}
		else
		{
			$('select#YearID').val(jsCurYearID);
			return false;
		}
	}
	
	jsCurYearID = jsYearID;
	
	Update_Auto_Complete_Extra_Para();
	

}

function js_Select_Student_Pop_up()
{
	var jsYearID = $('select#YearID').val();
	newWindow('choose_student_popup.php?YearID=' + jsYearID + '&fieldname=SelectedUserIDArr[]', 9);
}

function js_Remove_Selected_Student()
{
	checkOptionRemove(document.getElementById('SelectedUserIDArr[]'));
	Update_Auto_Complete_Extra_Para();
}

function js_Go_Back_To_Comment_List()
{
	var ObjForm = document.getElementById('form1');
	ObjForm.action = 'index_comment.php';
	ObjForm.submit();
}

function js_Add_Comment_To_Student()
{
	checkOptionAll(document.getElementById("SelectedUserIDArr[]"));
	
	var objForm = document.getElementById('form1');
	objForm.action = 'add_comment_to_student.php';
	objForm.submit();
}
</script>


<?
$linterface->LAYOUT_STOP();
intranet_closedb();

?>