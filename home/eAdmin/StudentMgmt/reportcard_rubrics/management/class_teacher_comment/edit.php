<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Management_ClassTeacherComment";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_comment.php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);


$linterface = new interface_html();
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
       
# tag information
if ($sys_custom['eRC']['Management']['ClassTeacherComment']['AddFromComment'])
{
	$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Student'], "index.php", 1);
	$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Comment'], "index_comment.php", 0);
}
else
{
	$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['MenuTitle'], "", 0);
}

$linterface->LAYOUT_START();

$lreportcard_ui = new libreportcardrubrics_ui();

echo $lreportcard_ui->Include_JS_CSS();
echo $lreportcard_ui->Get_Management_Class_Teacher_Comment_Edit_UI($ReportID,$ClassID);

?>

<script>

// jquery autocomplete function 

var AutoCompleteObj;
function Init_JQuery_AutoComplete(InputID,UserID,UserType) {
	AutoCompleteObj = $("#"+InputID).autocomplete("ajax_reload.php",
		{  			
			onItemSelect: function (row) {
				
				var ReturnVal = $("#"+InputID).val();
				ReturnVal = ReturnVal.replace("<br>","\r\n");
				var StudentID = InputID.replace("search_","");
				
				// manual append (jquery append doesn't work properly in textarea)
				var OriValue = $("#comment"+StudentID).val();
				$("#comment"+StudentID).val(OriValue+ReturnVal+"\r\n");
				
				$("#"+InputID).val("");
			},
			formatItem: function(row) {
				return row[1] ;
			},
			maxItemsToShow: 20,
			minChars: 1,
			delay: 0,
			autoFill: false,
			width: 200

		}
	);
	
	Update_Auto_Complete_Extra_Para();
}

function Update_Auto_Complete_Extra_Para() {
	var ExtraPara = new Array();
	ExtraPara['Action'] = "AutoComplete";
	AutoCompleteObj[0].autocompleter.setExtraParams(ExtraPara);
}

function selectItem(li) {

		if (!li) {
			li = document.createElement("li");
			li.extra = [];
			li.selectValue = "";
		}
		var v = $.trim(li.selectValue ? li.selectValue : li.innerHTML);
		input.lastSelected = v;
		prev = v;
		$results.html("");
		$input.val(v);
		hideResultsNow();
		if (options.onItemSelect) setTimeout(function() { options.onItemSelect(li) }, 1);
};

$().ready(function(){
	$("input.SearchComment").each(function(){
		Init_JQuery_AutoComplete($(this).attr("id"));
	});
});


// Return false when ESC button is pressed, use to prevent RESET form
function noEsc() {
	return !(window.event && window.event.keyCode == 27);
}

//function checkOptionAdd(parObj, addText) {
//	if (parObj.value != "")
//	{
//		//parObj.value += " ";
//	}
//	parObj.value += addText;
//}

function limitText(limitField, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	}
}

var numb = '0123456789';
var lwr = 'abcdefghijklmnopqrstuvwxyz';
var upr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

function isValid(parm,val) {
	if (parm == "") return true;
	for (i=0; i<parm.length; i++) {
		if (val.indexOf(parm.charAt(i),0) == -1) return false;
	}
	return true;
}

function isNum(parm) {return isValid(parm,numb);}
function isAlpha(parm) {return isValid(parm,lwr+upr);}
function isAlphanum(parm) {return isValid(parm,lwr+upr+numb);}
</script>
<br/>
<?
       $linterface->LAYOUT_STOP();
       intranet_closedb();

?>