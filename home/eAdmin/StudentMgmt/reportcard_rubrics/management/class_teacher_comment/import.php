<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Management_ClassTeacherComment";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_comment.php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);


$linterface = new interface_html();
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
       
# tag information
if ($sys_custom['eRC']['Management']['ClassTeacherComment']['AddFromComment'])
{
	$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Student'], "index.php", 1);
	$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Comment'], "index_comment.php", 0);
}
else
{
	$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['MenuTitle'], "", 0);
}

$ReturnMsg = $Lang['General']['ReturnMessage'][$msg];

$linterface->LAYOUT_START($ReturnMsg);
$lreportcard_ui = new libreportcardrubrics_ui();

echo $lreportcard_ui->Get_Management_Class_Teacher_Comment_Import_UI($ReportID,$ClassID, $ClassLevelID);

?>


<script type="text/JavaScript" language="JavaScript">
function resetForm() {
	document.form1.reset();
	changePicClass()
	return true;
}

function checkForm() {
	obj = document.form1;
	if (obj.elements["userfile"].value.Trim() == "") {
		 alert('<?=$Lang['eRC_Rubrics']['GeneralArr']['jsWarningArr']['AlertSelectFile']?>');
		 obj.elements["userfile"].focus();
		 return false;
	}
	
	return true;
}

function js_Changed_Form_Selection(ClassLevelID)
{
	$("div#ClassSelectionDiv").html('<?=$lreportcard_ui->Get_Ajax_Loading_Image()?>');
	$.post(
		"ajax_reload.php",
		{
			ClassLevelID: ClassLevelID,
			Action: "ReloadClassSelection"
		},
		function(ReturnData)
		{
			$("div#ClassSelectionDiv").html(ReturnData);
		}
	);
}

function js_Export()
{
	var ClassID = $("select#ClassID").val();
	var ReportID = $("input#ReportID").val();
	var ClassLevelID = $("select#ClassLevelID").val();
	
	
	var param = "?is_template=1&ReportID="+ReportID;
	if(ClassID.Trim() != '')
		param += "&ClassID="+ClassID;
	else
		param += "&ClassLevelID="+ClassLevelID;
	
	window.location.href="export.php"+param;	
}
</script>
<br />

<?
	print $linterface->FOCUS_ON_LOAD("form1.userfile");
  	$linterface->LAYOUT_STOP();
?>
