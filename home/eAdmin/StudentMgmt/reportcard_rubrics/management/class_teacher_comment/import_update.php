<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Management_ClassTeacherComment";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_comment.php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard_comment = new libreportcardrubrics_comment();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$linterface = new interface_html();

$ReportID = $_REQUEST['ReportID'];
$ClassLevelID = $_REQUEST['ClassLevelID'];
$ClassID = ($_REQUEST['ClassID']=='')? 0 : $_REQUEST['ClassID'];
$ImportData = unserialize(stripslashes($ImportData));

$Success = $lreportcard_comment->Replace_Comment($ReportID, $ImportData, 0);

if($Success)
	$SuccessMsg = $Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportCommentSuccess'];
else
	$SuccessMsg = $Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportCommentFail'];

### Title ###
# tag information
if ($sys_custom['eRC']['Management']['ClassTeacherComment']['AddFromComment'])
{
	$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Student'], "index.php", 1);
	$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Comment'], "index_comment.php", 0);
}
else
{
	$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['MenuTitle'], "", 0);
}

$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='edit.php?ReportID=$ReportID&ClassID=$ClassID'","back");

# page navigation
$ReportTemplateInfo = $lreportcard->GetReportTemplateInfo($ReportID);
$ReportTitle = Get_Lang_Selection($ReportTemplateInfo[0]['ReportTitleCh'],$ReportTemplateInfo[0]['ReportTitleEn']);

$PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['MenuTitle'], "index.php?ReportID=$ReportID&ClassID=$ClassID"); 
$PAGE_NAVIGATION[] = array($ReportTitle, "edit.php?ReportID=$ReportID&ClassID=$ClassID"); 
$PAGE_NAVIGATION[] = array($Lang['Btn']['Import'], "");

$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();  

?>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td colspan="2"><?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?><br><br></td>
	</tr>
	<tr>
		<td colspan="2"><?= $linterface->GET_IMPORT_STEPS(3) ?></td>
	</tr>
	<tr>
		<td class='tabletext' align='center'><br><?=$SuccessMsg?><br></td>
	</tr>
	<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<tr>
		<td colspan="2" align="center"><?=$BackBtn?></td>
	</tr>
</table>

<?
$linterface->LAYOUT_STOP();


?>