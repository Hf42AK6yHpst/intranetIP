<?php
// Using:
/*
 *  Date :  2019-07-29 (Bill)
 *          - Updated logic for new csv format
 *
 *  Date :  2019-03-21 (Bill)     [2018-1002-1146-08277]
 *          - Create file
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_eca.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");

$lreportcard_eca = new libreportcardrubrics_eca();

# Get action
$action = stripslashes($_REQUEST['action']);
if ($action == 'Import_Student_ECA')
{
    include_once($PATH_WRT_ROOT.'includes/libimporttext.php');
    include_once($PATH_WRT_ROOT."includes/libexporttext.php");
    include_once($PATH_WRT_ROOT.'includes/libuser.php');
	$limport = new libimporttext();
	$lexport = new libexporttext();
	$luser = new libuser();
	
	### Get CSV Data
	$targetFilePath = stripslashes($_REQUEST['targetFilePath']);
	$csvData = $limport->GET_IMPORT_TXT($targetFilePath);
	
	### Get CSV Header
	$csvHeaderArr = array_shift($csvData);
	$csvHeader2Arr = array_shift($csvData);
	//$csvStudentHeaderArr = array_shift($csvData);
	$csvDataSize = count($csvData);
	
	### Get Default Cols
	$defaultECA_ColumnArr = $lreportcard_eca->Get_Student_ECA_Import_Export_Column_Title_Arr();
	$csvHeaderSize = count($defaultECA_ColumnArr[0]);
	
	### Delete temp data first
	$SuccessArr['DeleteOldTempData'] = $lreportcard_eca->Delete_Import_Temp_Data();
	
	### Get ECA Category & Item Data
	//$ECA_CatInfoArr = BuildMultiKeyAssoc($lreportcard_eca->Get_ECA_Category_Info(), "EcaCategoryCode");
	//$ECA_SubCatInfoArr = BuildMultiKeyAssoc($lreportcard_eca->Get_ECA_SubCategory_Info(), array("EcaCategoryID", "EcaSubCategoryCode"));
	//$ECA_CatItemInfoArr = BuildMultiKeyAssoc($lreportcard_eca->Get_ECA_Category_Item_Info(), array("EcaCategoryID", "EcaSubCategoryID", "EcaItemCode"));
    $ECA_CatItemCodeAssoc = BuildMultiKeyAssoc($lreportcard_eca->Get_ECA_Category_Item_Info(), 'EcaItemCode', 'EcaItemID', 1, 0);
	
	### Include JS libraries
	$jsInclude = '';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
	echo $jsInclude;
	
	### Var Init.
	$NumOfSuccessRow = 0;
	$NumOfErrorRow = 0;
	$ErrorRowInfoArr = array();
	$InsertValueArr = array();
	$RowNumber = 3;
	
	### loop student
	//$csvStudentIDArr = array();
	//for($j=$csvHeaderSize; $j<count($csvStudentHeaderArr); $j++)
	//{
	//    # Warning if no such student
	//    $csvStudentUserLogin = trim($csvStudentHeaderArr[$j]);
	//    $csvStudentInfo = $luser->returnUser("", $csvStudentUserLogin, $UserIDsArray=null);
	//    if ($csvStudentUserLogin == '' || empty($csvStudentInfo))
	//    {
	//        $csvColumnName = numberToLetter(($j+1), $uppercase=true);
	//        (array)$ErrorRowInfoArr[$RowNumber][$csvColumnName]['ErrorMsgArr'][] = '- '.$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['WarningArr']['NoStudentWarning'];
	//    }
	//
	//    $csvStudentIDArr[$csvStudentUserLogin] = $csvStudentInfo[0]['UserID'];
	//}
	//$RowNumber++;

    ### loop ECA Item Code
    $csvItemCodeArr = array();
    for($j=$csvHeaderSize; $j<count($csvHeaderArr); $j++)
    {
        $csvItemCode = trim($csvHeaderArr[$j]);
        if ($csvItemCode != '' && isset($ECA_CatItemCodeAssoc[$csvItemCode]) && $ECA_CatItemCodeAssoc[$csvItemCode] != '') {
            $csvItemCodeArr[$j] = $ECA_CatItemCodeAssoc[$csvItemCode];
        }
    }
	
	### loop CSV data
    $csvStudentIDArr = array();
	for ($i=0; $i<$csvDataSize; $i++)
	{
		$thisColumnCounter = 0;
        $j = 3;
		
		$thisClassName = trim($csvData[$i][$thisColumnCounter++]);
		$thisClassNumber = trim($csvData[$i][$thisColumnCounter++]);
		$thisUserLogin = trim($csvData[$i][$thisColumnCounter++]);
        $thisStudentName = trim($csvData[$i][$thisColumnCounter++]);

        ### Warning if no such student
        $thisStudentInfo = $luser->returnUser("", $thisUserLogin, $UserIDsArray=null);
        $isInvalidStudent = $thisUserLogin == '' || empty($thisStudentInfo);
        if ($isInvalidStudent)
        {
            $csvColumnName = numberToLetter($j, $uppercase=true);
            (array)$ErrorRowInfoArr[$RowNumber][$csvColumnName]['ErrorMsgArr'][] = '- '.$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['WarningArr']['NoStudentWarning'];
        }
        $j = $j + 2;
		
		### Warning if any invalid input
        if(!$isInvalidStudent)
        {
            $thisStudentID = $thisStudentInfo[0]['UserID'];
            for($k=$csvHeaderSize; $k<count($csvHeaderArr); $k++)
            {
                $ECA_InputVal = trim($csvData[$i][$k]);
                $isInvalidECAInput = $ECA_InputVal != '' && $ECA_InputVal != 'Y';
                if($isInvalidECAInput)
                {
                    $csvColumnName = numberToLetter($j, $uppercase=true);
                    (array)$ErrorRowInfoArr[$RowNumber][$csvColumnName]['ErrorMsgArr'][] = '- '.$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['WarningArr']['InvalidInputWarning'];
                }
                $j++;

                // Prepare value insert to temp table
                $ECA_CatItemID = $csvItemCodeArr[$k];
                if(!$isInvalidECAInput && $ECA_CatItemID && $thisStudentID)
                {
                    $is_selected = $ECA_InputVal == 'Y';
                    $InsertValueArr[] = " ('".$_SESSION['UserID']."', '".$RowNumber."', '".$thisStudentID."', '".$ECA_CatItemID."', '".$is_selected."', NOW()) ";
                }
            }
        }
		
		### Update result count
		if(empty($ErrorRowInfoArr[$RowNumber])) {
			$NumOfSuccessRow++;
		} else {
			$NumOfErrorRow++;
		}
		
		### Update Processing Display
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.document.getElementById("SuccessCountDiv").innerHTML = "'.$NumOfSuccessRow.'";';
			$thisJSUpdate .= 'window.parent.document.getElementById("FailCountDiv").innerHTML = "'.$NumOfErrorRow.'";';
			$thisJSUpdate .= 'window.parent.document.getElementById("BlockUI_Processed_Number_Span").innerHTML = "'.($RowNumber - 2).'";';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
		
		$RowNumber++;
	}
	
	### Insert temp records into the temp table
	if (count($InsertValueArr) > 0)
	{
		$InsertList = implode(',', $InsertValueArr);

		$TEMP_IMPORT_ECA = $lreportcard_eca->Get_Table_Name('TEMP_IMPORT_STUDENT_ECA');
		$sql = "INSERT INTO $TEMP_IMPORT_ECA
					(ImportUserID, RowNumber, StudentID, ItemID, isSelected, DateInput)
                VALUES
                    $InsertList ";
        $SuccessArr['InsertTempRecords'] = $lreportcard_eca->db_db_query($sql);
	}
	
	### Display Error Result Table
	$numOfErrorRow = count($ErrorRowInfoArr);
	if($numOfErrorRow > 0)
	{
		### Update Processing Display
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.document.getElementById("BlockUI_Span").innerHTML = "'.$Lang['SysMgr']['SubjectClassMapping']['GeneratingErrorsInfo'].'...";';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
		
		### Build Error Info Table
		$x .= '<table width="100%" border="0" cellpadding="3" cellspacing="0">';
			$x .= '<tr>';
				$x .= '<td class="tablebluetop tabletopnolink" width="8%">'.$Lang['General']['ImportArr']['Row'].'</td>';
				$x .= '<td class="tablebluetop tabletopnolink" width="10%">'.$Lang['General']['ImportArr']['Column'].'</td>';
				$x .= '<td class="tablebluetop tabletopnolink">'.$Lang['General']['Remark'].'</td>';
			$x .= '</tr>';
			
		$i = 0;
		foreach((array)$ErrorRowInfoArr as $thisRowNumber => $thisErrorRowInfo)
        {
            $row_size = count($thisErrorRowInfo);
			$css_i = ($i % 2) ? "2" : "";
			
			$is_first_col = true;
			foreach((array)$thisErrorRowInfo as $thisColumnName => $thisErrorRowColumnInfo)
			{
			    $x .= '<tr style="vertical-align:top">';
				    if($is_first_col) {
				        $x .= '<td class="tablebluerow'.$css_i.'" rowspan="'.$row_size.'">'.$thisRowNumber.'</td>';
				    }
				    $x .= '<td class="tablebluerow'.$css_i.'">'.$thisColumnName.'</td>';
				    $x .= '<td class="tablebluerow'.$css_i.'">'.implode('<br />', (array)$thisErrorRowColumnInfo['ErrorMsgArr']).'</td>';
			    $x .= '</tr>';
			    
			    $is_first_col = false;
			}
			$i++;
		}
		
		$x .= "</table>";
	}
    
	$ErrorCountDisplay = ($NumOfErrorRow > 0) ? "<font color=\"red\">".$NumOfErrorRow."</font>" : $NumOfErrorRow;
	
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= 'window.parent.document.getElementById("ErrorTableDiv").innerHTML = \''.$x.'\';';
		$thisJSUpdate .= 'window.parent.document.getElementById("SuccessCountDiv").innerHTML = \''.$NumOfSuccessRow.'\';';
		$thisJSUpdate .= 'window.parent.document.getElementById("FailCountDiv").innerHTML = \''.$ErrorCountDisplay.'\';';
		
		if ($NumOfErrorRow == 0) 
		{
			$thisJSUpdate .= '$("input#ContinueBtn", window.parent.document).removeClass("formbutton_disable").addClass("formbutton").attr("disabled", "");';
		}
		
		$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
}

intranet_closedb();
?>