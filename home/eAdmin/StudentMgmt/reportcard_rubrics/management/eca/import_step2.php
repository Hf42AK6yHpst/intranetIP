<?php
// Using:
/*
 *  Date :  2019-07-29 (Bill)
 *          - Updated logic for new csv format
 *
 *  Date :  2019-03-21 (Bill)     [2018-1002-1146-08277]
 *          - Create file
 */

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Management_ECAService";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_eca.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$lreportcard_eca = new libreportcardrubrics_eca();

$linterface = new interface_html();
$lreportcard_ui = new libreportcardrubrics_ui();

$limport = new libimporttext();
$lo = new libfilesystem();

$backPara = "ReportID=".$ReportID."&ClassLevelID=".$ClassLevelID;

# Check file format
$name = $_FILES['userfile']['name'];
$ext = strtoupper($lo->file_ext($name));
if(!($ext == ".CSV" || $ext == ".TXT"))
{
    intranet_closedb();
    header("location: import.php?".$backPara."&ReturnMsgKey=WrongFileFormat");
    exit();
}

# Move CSV file to temp folder
$folder_prefix = $intranet_root."/file/import_temp/reportcard_rubrics/eca_info";
if (!file_exists($folder_prefix)) {
    $lo->folder_new($folder_prefix);
}
$targetFileName = date('Ymd_His').'_'.$_SESSION['UserID'].$ext;
$targetFilePath = stripslashes($folder_prefix."/".$targetFileName);
$SuccessArr['MoveCsvFileToTempFolder'] = $lo->lfs_move($userfile, $targetFilePath);

# ECA Category Item
$ECA_ItemCodeArr = Get_Array_By_Key($lreportcard_eca->Get_ECA_Category_Item_Info(), 'EcaItemCode');
$ECA_ItemCodeArr = array_unique(array_filter(Array_Trim((array)$ECA_ItemCodeArr)));

# Get Default Cols
$defaultECA_ColumnArr = $lreportcard_eca->Get_Student_ECA_Import_Export_Column_Title_Arr();
// $numOfDefaultCsvHeader = count($DefaultCsvHeaderArr);
// $ColumnPropertyArr = $lreportcard_topic->Get_Topic_Import_Export_Column_Property($forGetCsvContent=1);

# Get CSV header & data
$csvData = $limport->GET_IMPORT_TXT($targetFilePath);
$csvHeaderArr = array_shift($csvData);
$csvHeader2Arr = array_shift($csvData);
//$csvStudentHeaderArr = array_shift($csvData);
$csvDataSize = count($csvData);

# Check 2 CSV header
$csvHeaderWrong = false;
$DefaultECA_HeaderArr = $defaultECA_ColumnArr[0];
for($i=0; $i<count($DefaultECA_HeaderArr); $i++)
{
    if ($csvHeaderArr[$i] != $DefaultECA_HeaderArr[$i]) {
        $csvHeaderWrong = true;
        break;
    }
}
if(!$csvHeaderWrong)
{
    for($i=count($DefaultECA_HeaderArr); $i<count($csvHeaderArr); $i++)
    {
        if (!in_array($csvHeaderArr[$i], (array)$ECA_ItemCodeArr)) {
            $csvHeaderWrong = true;
            break;
        }
    }
}
$DefaultECA_Header2Arr = $defaultECA_ColumnArr[1];
for($i=0; $i<count($DefaultECA_Header2Arr); $i++)
{
    if ($csvHeader2Arr[$i] != $DefaultECA_Header2Arr[$i]) {
        $csvHeaderWrong = true;
        break;
    }
}
//$DefaultECA_Header3Arr = $defaultECA_ColumnArr[2];
//for($i=0; $i<count($DefaultECA_Header3Arr); $i++)
//{
//    if ($csvStudentHeaderArr[$i] != $DefaultECA_Header3Arr[$i]) {
//        $csvHeaderWrong = true;
//        break;
//    }
//}

if($csvHeaderWrong || $csvDataSize == 0)
{
    $ReturnMsgKey = $csvHeaderWrong? 'WrongCSVHeader' : 'CSVFileNoData';
    intranet_closedb();
    
    header("location: import.php?".$backPara."&ReturnMsgKey=".$ReturnMsgKey);
    exit();
}

# Thickbox loading message
$ProcessingMsg = '';
$ProcessingMsg .= '<span id="BlockUI_Span">';
    $ProcessingMsg .= str_replace('<!--NumOfRecords-->', '<span id="BlockUI_Processed_Number_Span">0</span> / '.$csvDataSize, $Lang['General']['ImportArr']['RecordsValidated']);
$ProcessingMsg .= '</span>';

# Tag
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['MenuTitle']);

$ReturnMsg = $Lang['General']['ReturnMessage'][$msg];
$linterface->LAYOUT_START($ReturnMsg);

echo $lreportcard_ui->Get_Management_ECA_Import_Step2_UI($ReportID, $ClassLevelID, $targetFilePath, $csvDataSize);
?>

<script language="javascript">
$('document').ready(function () {
	Block_Document('<?=$ProcessingMsg?>');
});

function js_Go_Back()
{
	window.location = 'import.php?<?=$backPara?>';
}

function js_Cancel()
{
	window.location = 'eca.php?<?=$backPara?>';
}

function js_Continue()
{
	$('form#form1').submit();
}
</script>
<br />

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>