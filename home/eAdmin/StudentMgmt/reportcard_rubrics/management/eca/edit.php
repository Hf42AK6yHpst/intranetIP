<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Management_ECAService";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

############## Interface Start ##############
$linterface = new interface_html();
$lreportcard_ui = new libreportcardrubrics_ui();

$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['MenuTitle']);
$linterface->LAYOUT_START();


$lreportcard_ui = new libreportcardrubrics_ui();

echo $lreportcard_ui->Include_JS_CSS();
echo $lreportcard_ui->Get_Management_ECA_Service_Edit_UI($ReportID,$ClassID);

?>

<script>
var BlockElementAlready = false;
function js_Reload_Item_Table()
{
	if(CheckBoxChanged && !confirm("<?=$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['jsConfirmMsg']['ChangesWillBeDiscard']?>"))
		return false;
	
	var ReportID = $("input#ReportID").val();
	var ClassID = $("input#ClassID").val();
	var EcaCategoryID = $("select#EcaCategoryID").val();
	var EcaSubCategoryID = $("select#EcaSubCategoryID").val();
	
	if(!BlockElementAlready)
		Block_Element("EcaTableDiv");
	$.post(
		"ajax_reload.php",
		{
			Action: "LoadItemTable",
			ReportID : ReportID,
			ClassID : ClassID,
			EcaCategoryID:EcaCategoryID,
			EcaSubCategoryID:EcaSubCategoryID
		},
		function(ReturnData)
		{
			$("div#EcaTableDiv").html(ReturnData);
			initSuperTable();
			UnBlock_Element();
			CheckBoxChanged = false;
			BlockElementAlready = false;
		}
	);
	
}

function js_Reload_Cat_Selection()
{
	if(CheckBoxChanged && !confirm("<?=$Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['jsConfirmMsg']['ChangesWillBeDiscard']?>"))
		return false;
	else
		CheckBoxChanged = false;
	
	Block_Element("EcaTableDiv");
	BlockElementAlready = true;
	var EcaCategoryID = $("select#EcaCategoryID").val();
	$.post
	(
		"../../ajax_reload.php",
		{
			Action:"LoadSubCategorySelection",
			EcaCategoryID: EcaCategoryID
		},
		function (ReturnData)
		{
			$("span#SubCategorySelectionSpan").html(ReturnData);
			js_Reload_Item_Table();
		}
	)
}

function CheckAll(obj,ItemID)
{
//	var isChecked = $("input#CheckAll_ItemChk_"+ItemID).attr("checked");
	var isChecked = $(obj).attr("checked");
	$("input.ItemChk_"+ItemID).attr("checked",isChecked);
	CheckBoxChanged = true;
}

var CheckBoxChanged = false;
function UncheckAll(ItemID)
{
	var isAllChecked = $("input.ItemChk_"+ItemID).not(":checked").length==0?true:false;
	$("input#CheckAll_ItemChk_"+ItemID).attr("checked",isAllChecked)
	CheckBoxChanged = true;
}

var jsScreenWidth = parseInt(Get_Screen_Width());
var jsTableWidth = jsScreenWidth - 250;

function initSuperTable()
{
    $("#EcaEditTable").toSuperTable(
    	{ 
    		width: jsTableWidth + "px", 
    		height: "500px", 
    		fixedCols: 2,
    		headerRows :4, 
    		onFinish: function(){fixSuperTableHeight("EcaEditTable",397); }
    	} 
    );
}

$().ready(function(){
	js_Reload_Cat_Selection();
	CheckBoxChanged = false;
})


	
</script>
<br/>
<?
       $linterface->LAYOUT_STOP();
       intranet_closedb();

?>