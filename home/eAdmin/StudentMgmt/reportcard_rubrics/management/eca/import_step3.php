<?php
// Using:
/*
 *  Date :  2019-03-21 (Bill)     [2018-1002-1146-08277]
 *          - Create file
 */

@SET_TIME_LIMIT(21600);

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Management_ECAService";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_eca.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$lreportcard_eca = new libreportcardrubrics_eca();

$linterface = new interface_html();
$lreportcard_ui = new libreportcardrubrics_ui();

$backPara = "ReportID=".$ReportID."&ClassLevelID=".$ClassLevelID;

### Thickbox loading message
$ProcessingMsg = '';
$ProcessingMsg .= '<span id="BlockUI_Span">';
	$ProcessingMsg .= str_replace('<!--NumOfRecords-->', '<span id="BlockUI_Processed_Number_Span">0</span> / '.$numOfCsvData, $Lang['General']['ImportArr']['RecordsProcessed']);
$ProcessingMsg .= '</span>';

# Tag
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['MenuTitle']);
$linterface->LAYOUT_START();

echo $lreportcard_ui->Get_Management_ECA_Import_Step3_UI($ReportID, $ClassLevelID="");
?>

<script language="javascript">
$('document').ready(function () {
	Block_Document('<?=$ProcessingMsg?>');
});

function js_Back_To_ECA()
{
	window.location = 'eca.php?<?=$backPara?>';
}

function js_Back_To_Import_Step1()
{
	window.location = 'import.php?<?=$backPara?>';
}
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>