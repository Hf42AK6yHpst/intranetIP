<?php
# using: 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Management_OtherInfo";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$linterface = new interface_html();
$lreportcard_ui = new libreportcardrubrics_ui();

$ReportID = $_REQUEST['ReportID'];
$ClassLevelID = $_REQUEST['ClassLevelID'];
$ClassID = ($_REQUEST['ClassID']=='')? 0 : $_REQUEST['ClassID'];
$CategoryID = $_REQUEST['CategoryID'];
$ImportData = unserialize(stripslashes($ImportData));

$success["ReplaceOtherInfo"] = $lreportcard->Replace_Student_OtherInfo_Record($ReportID,$CategoryID,$ClassLevelID,$ClassID,$ImportData); 

$SuccessMsg = !$success["ReplaceOtherInfo"]?$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportOtherInfoFail']:$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['ImportOtherInfoSuccess'];

# BackBtn
$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='index.php?CategoryID=$CategoryID'","back"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");

### Title ###
# tag information
$TAGS_OBJ =array();
$TAGS_OBJ = $lreportcard_ui->getOtherInfoTabObjArr($CategoryID);

# page navigation (leave the array empty if no need)
$PAGE_NAVIGATION[] = array($button_upload, "");

$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();  

?>
<br>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td colspan="2"><?= $linterface->GET_IMPORT_STEPS(3) ?></td>
	</tr>
	<tr>
		<td class='tabletext' align='center'><br><?=$SuccessMsg?><br></td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<div class="edit_bottom">
				<?=$BackBtn?>
			</div>
		</td>
	</tr>
</table>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>