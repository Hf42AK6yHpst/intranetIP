<?php
# using: 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Management_OtherInfo";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

list($ExportHeader,$ExportData) = $lreportcard->Get_Student_OtherInfo_Export_Data($ReportID,$CategoryID,$ClassLevelID,$ClassID);

$filename = $lreportcard->Get_OtherInfo_Export_Filename($ReportID,$CategoryID,$ClassLevelID,$ClassID);

include_once($PATH_WRT_ROOT."includes/libexporttext.php");
$lexport = new libexporttext();
$exportContent = $lexport->GET_EXPORT_TXT($ExportData, $ExportHeader);

$lexport->EXPORT_FILE($filename, $exportContent);
	
intranet_closedb();		
?>