<?php
# using: 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Management_OtherInfo";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");


$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$linterface = new interface_html();
$lreportcard_ui = new libreportcardrubrics_ui();

$CategoryID = $_REQUEST['CategoryID'];
$ReportID = $_REQUEST['ReportID'];
$ClassLevelID = $_REQUEST['ClassLevelID'];
$ClassID = ($_REQUEST['ClassID']=='')? 0 : $_REQUEST['ClassID'];

$fs = new libfilesystem();
$limport = new libimporttext();

$UpdateFail = false;

// check right
$TeachingClassInfoArr = $lreportcard->Get_User_Accessible_Class($ClassLevelID);
$TeachingYearClassIDArr = Get_Array_By_Key($TeachingClassInfoArr, 'ClassID');
if(!in_array($ClassID, $TeachingYearClassIDArr) && $ClassID!=0)
	$UpdateFail = true;

// check $userfile is empty 
if(!$UpdateFail && !empty($userfile)) 
{
	$loc = $userfile;
	$filename = $userfile_name;
}
else
	$UpdateFail = true;

# check if file name is empty  
if(!$UpdateFail && trim($filename)!='')
	$ext = $fs->file_ext($filename);
else 
	$UpdateFail = true;

# check if file ext valid and file exist
if(!$UpdateFail && ($ext==".csv" || $ext==".txt")&& $loc!="none" && file_exists($loc)) 
	$data = $limport->GET_IMPORT_TXT($loc);
else
	$UpdateFail = true;

# check if data is empty 
if(!$UpdateFail && !$data)
	$UpdateFail = true;

if($UpdateFail)
{
	header("Location: upload.php?msg=WrongFileFormat&ClassID=$ClassID&ClassLevelID=$ClassLevelID&CategoryID=$CategoryID"); 
		exit;
}

$success = 0;	
	
$fieldName = $lreportcard->Get_OtherInfo_CSV_Header($CategoryID);
$header_row = array_shift($data);
$wrong_format = $fs->CHECK_CSV_FILE_FORMAT($header_row, $fieldName);

// Validate the data according to the setting of config file
// Empty fields will be accepted
if($wrong_format)
{
	header("Location: upload.php?msg=WrongCSVHeader&CategoryID=$CategoryID");
	exit;
}

# Get a list of WebSAMSRegNo of all student in the class for validating REGNO
if ($ClassID=='')
	$StudentList = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($ClassLevelID);
else
	$StudentList = $lreportcard->GET_STUDENT_BY_CLASS($ClassID);
	
# $ClassNameNumberAssoc[$ClassName][$ClassNumber] = $UserID
$ClassNameNumberAssoc = BuildMultiKeyAssoc($StudentList,array("ClassTitleEn","ClassNumber"),"UserID",1);

# $UserLoginAssoc[$UserLogin] = $UserID
$UserLoginAssoc = BuildMultiKeyAssoc($StudentList,array("UserLogin"),"UserID",1);

# $WebSamsAssoc[$WebSAMSRegNo] = $UserID
$WebSamsAssoc = BuildMultiKeyAssoc($StudentList,array("WebSAMSRegNo"),"UserID",1);

# prepare $OtherInfoItemInfo
$OtherInfoItemInfo = $lreportcard->Get_OtherInfo_Item_Info($CategoryID);
$ItemNameInfo = BuildMultiKeyAssoc($OtherInfoItemInfo,"ItemNameEn",array("ItemID","ItemCode","ItemNameEn","RecordType","Length")); 

$wrong_row = array();

# skip $data[0] as $data[0] should be the chinese title
for($i=1; $i<sizeof($data); $i++) 
{
	list($ClassName, $ClassNumber, $UserLogin, $WebSamsRegNo, $StudentName) = $data[$i];
	
	if(trim($ClassName)!='' xor trim($ClassNumber)!='')
	{
		if(trim($ClassName)=='')
		{
			$WarnMsg[$i][] = $Lang['eRC_Rubrics']['GeneralArr']['ImportWarningArr']['EmptyClassName'];
			$WarnCss[$i][0] = true;
		}
		else if(trim($ClassNumber)=='')
		{
			$WarnMsg[$i][] = $Lang['eRC_Rubrics']['GeneralArr']['ImportWarningArr']['EmptyClassNumber'];
			$WarnCss[$i][1] = true;
		}
	}
	else if(trim($ClassName)!='' || trim($ClassNumber)!='')
	{
		
		if(!$thisStudentID = $ClassNameNumberAssoc[$ClassName][$ClassNumber])
		{
			$WarnMsg[$i][] = $Lang['eRC_Rubrics']['GeneralArr']['ImportWarningArr']['WrongClassNameNumber'];
			$WarnCss[$i][0] = true;
			$WarnCss[$i][1] = true;
		}
	}
	else if(trim($UserLogin)!='')
	{
		if(!$thisStudentID = $UserLoginAssoc[$UserLogin])
		{
			$WarnMsg[$i][] = $Lang['eRC_Rubrics']['GeneralArr']['ImportWarningArr']['WrongUserLogin'];
			$WarnCss[$i][2] = true;											
		}
	}
	else if(trim($WebSamsRegNo)!='')
	{
		if(!$thisStudentID = $WebSamsAssoc[$WebSamsRegNo])
		{
			$WarnMsg[$i][] = $Lang['eRC_Rubrics']['GeneralArr']['ImportWarningArr']['WrongWebSamsRegNo'];
			$WarnCss[$i][3] = true;											
		}
		
	}
	else
	{
		$WarnMsg[$i][] = $Lang['eRC_Rubrics']['GeneralArr']['ImportWarningArr']['EmptyStudentData'];
		$WarnCss[$i][0] = true;
		$WarnCss[$i][1] = true;
		$WarnCss[$i][2] = true;
		$WarnCss[$i][3] = true;											
	}
	
	$WrongDataFormat = false;
	$InfoValue = array();
	for($j=5; $j<sizeof($header_row); $j++)
	{
		$field = $header_row[$j];
		$thisItemInfo = $ItemNameInfo[$field];
		$thisWrongFormat = false;
		
		if ($data[$i][$j] != "") 
		{
			$DataLength = (int)$thisItemInfo["Length"];
			if (!empty($DataLength) && strlen($data[$i][$j]) > $DataLength) {
				$thisWrongFormat = true;
				$WarnCss[$i][$j] = true;
			}
			
			$DataType = $thisItemInfo["RecordType"];
			if (strtoupper($DataType) == OTHERINFO_DATA_TYPE_NUM && !is_numeric($data[$i][$j])) {
				$thisWrongFormat = true;
				$WarnCss[$i][$j] = true;
			}
			
			$gradeRegEx = '/^[a-zA-Z][+|-]?$/';
			if (strtoupper($DataType) == OTHERINFO_DATA_TYPE_GRADE && !preg_match($gradeRegEx, $data[$i][$j])) {
				$thisWrongFormat = true;
				$WarnCss[$i][$j] = true;
			}

			if($WrongDataFormat||$thisWrongFormat) 
				$WrongDataFormat = $thisWrongFormat;
			else
				$ImportData[$thisStudentID][$thisItemInfo["ItemID"]][] = $data[$i][$j];
		}
	}
	if($WrongDataFormat)
		$WarnMsg[$i][] = $Lang['eRC_Rubrics']['GeneralArr']['ImportWarningArr']['WrongDataFormat']; 
	
}

$NoOfFail = count($WarnMsg);
$NoOfSuccess = sizeof($data)-1-$NoOfFail;

if(count($WarnMsg)>0)
{
	# build Table 
	$display .= '<table width="95%" border="0" cellpadding="5" cellspacing="0">'."\n";
		$display .= '<tr>'."\n";
			$display .= '<td class="tablebluetop tabletopnolink" width="1%">Row#</td>'."\n";
			$display .= '<td class="tablebluetop tabletopnolink">'.$i_ClassName.'</td>'."\n";
			$display .= '<td class="tablebluetop tabletopnolink">'.$i_ClassNumber.'</td>'."\n";
			$display .= '<td class="tablebluetop tabletopnolink">'.$i_UserLogin.'</td>'."\n";				
			$display .= '<td class="tablebluetop tabletopnolink">'.$Lang['StudentRegistry']['WebSAMSRegNo'].'</td>'."\n";
			for($i=4;$i<sizeof($fieldName);$i++)
			{
				$display .= '<td class="tablebluetop tabletopnolink">'.$fieldName[$i].'</td>'."\n";
			}
			$display .= '<td class="tablebluetop tabletopnolink" width="30%">'.$Lang['General']['Remark'].'</td>'."\n";
		$display .= '</tr>'."\n";
	
		foreach($WarnMsg as $rowno => $Msg)
		{
			$rowcss = " class='".($errctr%2==0? "tablebluerow2":"tablebluerow1")."' ";
			
			$display .= '<tr '.$rowcss.'>'."\n";
				$display .= '<td class="tabletext">'.($rowno+2).'</td>'."\n";
				for($j=0; $j<sizeof($data[$rowno]); $j++)
				{
					$css = $WarnCss[$rowno][$j]?"red":"";
					$value = $WarnCss[$rowno][$j]&&empty($data[$rowno][$j])?"***":$data[$rowno][$j];
					$display .= '<td class="tabletext '.$css.'">'.$value.'</td>'."\n";
				}
				$display .= '<td class="tabletext">'.implode("<br>",$Msg).'</td>'."\n";
			$display .= '</tr>'."\n";
			
			$errctr++;
		}
	$display .= '</table>'."\n";
}

$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "javascript:history.back()","back");
$NextBtn = $linterface->GET_ACTION_BTN($button_import, "submit", "","submit2");

$Btn .= empty($WarnMsg)?$NextBtn."&nbsp;":"";
$Btn .= $BackBtn;
					
# tag information
$TAGS_OBJ =array();
$TAGS_OBJ = $lreportcard_ui->getOtherInfoTabObjArr($CategoryID);

# page navigation (leave the array empty if no need)
$PAGE_NAVIGATION[] = array($button_upload, "");

$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
							
?>
<form id="form1" name="form1" action="file_upload_update.php" method="post">
<br />
<table id="html_body_frame" width="96%" border="0" cellspacing="0" cellpadding="5">
<tr><td align="right" colspan="<?=$col_size?>"><?= $SysMsg ?></td></tr>
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td><?= $linterface->GET_IMPORT_STEPS(2) ?></td>
</tr>
<tr>
	<td>
		<table width="30%">
			<tr>
				<td class='formfieldtitle'><?=$Lang['General']['SuccessfulRecord']?></td>
				<td class='tabletext'><?=$NoOfSuccess?></td>
			</tr>
			<tr>
				<td class='formfieldtitle'><?=$Lang['General']['FailureRecord']?></td>
				<td class='tabletext <?=$NoOfFail>0?"red":""?>'><?=$NoOfFail?></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<?=$display?>
	</td>
</tr>
<tr>
	<td><table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center"><?=$Btn?></td>
		</tr>
	</table>
	</td>
</tr>
</table>
<input type="hidden" name="ClassLevelID" value="<?=$ClassLevelID?>" />
<input type="hidden" name="ReportID" value="<?=$ReportID?>" />
<input type="hidden" name="ClassID" value="<?=$ClassID?>" />
<input type="hidden" name="CategoryID" value="<?=$CategoryID?>" />
<input type="hidden" name="ImportData" value="<?=htmlspecialchars(serialize($ImportData))?>" />

</form>
<br><br>
<?
$linterface->LAYOUT_STOP();

?>