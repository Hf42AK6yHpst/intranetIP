<?php
// using :
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Management_Schedule";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_module.php");
$lreportcard_module = new libreportcardrubrics_module();

$lreportcard_module->Has_Access_Right($PageRight, $CurrentPage);

# Get data
$Action = stripslashes($_REQUEST['Action']);
if ($Action == "Update_Module_Schedule")
{
	$ModuleID = stripslashes($_REQUEST['ModuleID']);
	$MarksheetSubmissionStartDate = stripslashes($_REQUEST['MarksheetSubmissionStartDate']);
	$MarksheetSubmissionEndDate = stripslashes($_REQUEST['MarksheetSubmissionEndDate']);
	
	$DataArr = array();
	$DataArr['MarksheetSubmissionStartDate'] = $MarksheetSubmissionStartDate;
	$DataArr['MarksheetSubmissionEndDate'] = $MarksheetSubmissionEndDate;
	echo $lreportcard_module->UpdateModule($ModuleID, $DataArr, $UpdateLastModified=0);
}

intranet_closedb();
?>