<?php
// Using :

##############################################
#
#	Date:   2019-05-02    Bill
#           - prevent Command Injection
#
##############################################

function TRUNCATE_TABLE($db, $tables) {
	global $lreportcard;
	$success = array();
	for($i=0; $i<sizeof($tables); $i++) {
		$sql = "TRUNCATE TABLE $db.".$tables[$i];
		$success[] = $lreportcard->db_db_query($sql);
	}
	return !in_array(false, $success);
}

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");

$CurrentPage = "Management_DataHandling";
$NoLangWordings = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

if (isset($transitionAction) && $transitionAction != "") {
	if ($transitionAction == "other") {
		if (!isset($OtherYear) || !is_numeric($OtherYear)) {
			header("Location:transition.php");
		}
	}
	else if($transitionAction != "other" && !is_numeric($transitionAction)) {
	    header("Location:transition.php");
	}
} else {
	header("Location:transition.php");
}


$logFunctionality = 'Data Transition';

# Case 1: change active database
if ($transitionAction == "other") {
	$originalYear = $lreportcard->Get_Active_AcademicYearID();
	
	$lreportcard->Update_Active_AcademicYearID($OtherYear);
	$successArr['Update_ActiveYear'] = 1;
	
	$logContent = 'From Year '.$originalYear.' to Year '.$OtherYear;
	$lreportcard->Insert_General_Log($logFunctionality, $logContent);
} 
else 
{ 
	$lreportcard->Start_Trans();
	
	$originalYear = $lreportcard->Get_Active_AcademicYearID();
	
	# Case 2: create new database and tables
	$activeDBName = $lreportcard->DBName;
	
	# Step 1: Create the new database
	$newDBName = $intranet_db."_DB_REPORT_CARD_RUBRICS_".$transitionAction;
	$sql = "CREATE DATABASE $newDBName";
	$successArr['Create_Database'] = $lreportcard->db_db_query($sql);
	
	if ($successArr['Create_Database']) {
		# Step 2: Execute external commands (mysqldump & mysql) as an easy way to duplicate the entire database
		$dumpCommand = "mysqldump -u $intranet_db_user --password=$intranet_db_pass $activeDBName | mysql -u $intranet_db_user --password=$intranet_db_pass $newDBName";
		exec($dumpCommand);
		
		# Step 3: Truncate all data from these tables, other are preserved so that they don't have to set again
		$tablesToTruncate = array(
								"RC_MARKSHEET_SCORE",
								"RC_MARKSHEET_SCORE_LOG",
								"RC_OTHER_INFO_STUDENT_RECORD",
								"RC_STUDENT_ECA",
								"RC_STUDENT_STUDY_TOPIC",
								"RC_STUDENT_STUDY_TOPIC_BACKUP",
								"RC_TEACHER_COMMENT"
							);
		$successArr['Truncate_Table'] = TRUNCATE_TABLE($newDBName, $tablesToTruncate);
		
		# Reset Start Date and End Date of all Modules
		$sql = "Update $newDBName.RC_MODULE Set StartDate = Null, EndDate = Null, MarksheetSubmissionStartDate = Null, MarksheetSubmissionEndDate = Null";
		$successArr['Reset_Module_Dates'] = $lreportcard->db_db_query($sql);
		
		# Step 5: Set the active database to the newly created and modfied database
		$successArr['Update_ActiveYear'] = $lreportcard->Update_Active_AcademicYearID($transitionAction);
		
		# Step 6: Map the New Term ID
		$OldAcademicYearID = $lreportcard->AcademicYearID;
		
		// Get New AcademicYearID
		$new_lreportcard = new libreportcardrubrics_custom();
		$NewAcademicYearID = $new_lreportcard->AcademicYearID;
		
		// Build the old term and new term mapping
		include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
		$OldYear_Obj = new academic_year($OldAcademicYearID);
		$OldYearTermArr = $OldYear_Obj->Get_Term_List(0);
		$numOfTerm = count($OldYearTermArr);
		
		$NewYear_Obj = new academic_year($NewAcademicYearID);
		$NewYearTermArr = $NewYear_Obj->Get_Term_List(0);
		
		for ($i=0; $i<$numOfTerm; $i++)
		{
			$thisOldYearTermID = $OldYearTermArr[$i]['YearTermID'];
			$thisNewYearTermID = $NewYearTermArr[$i]['YearTermID'];
			
			$sql = "UPDATE $newDBName.RC_MODULE SET YearTermID = '$thisNewYearTermID' WHERE YearTermID = '$thisOldYearTermID' ";
			$successArr['Reset_Module_Term_Mapping_'.$i] = $lreportcard->db_db_query($sql);
		}
	}
	
	$newYear = $new_lreportcard->Get_Active_AcademicYearID();
	$logContent = 'From Year '.$originalYear.' to Year '.$newYear.' (NEW)';
	$lreportcard->Insert_General_Log($logFunctionality, $logContent);
}

if (in_array(false, $successArr))
{
	$lreportcard->RollBack_Trans();
	$ReturnMsgKey = 'Failed';
}
else
{
	$lreportcard->Commit_Trans();
	$ReturnMsgKey = 'Success';
}
intranet_closedb();

header("Location:transition.php?ReturnMsgKey=$ReturnMsgKey");
?>