<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_AdminGroup";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");


### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_settings_admin_group_member_list_page_size", "numPerPage");
$arrCookies[] = array("ck_settings_admin_group_member_list_page_number", "pageNo");
$arrCookies[] = array("ck_settings_admin_group_member_list_page_order", "order");
$arrCookies[] = array("ck_settings_admin_group_member_list_page_field", "field");
$arrCookies[] = array("ck_settings_admin_group_member_list_page_keyword", "Keyword_Member_List");

$Keyword_Member_List = (isset($_POST['Keyword_Member_List']))? $_POST['Keyword_Member_List'] : $_COOKIE['Keyword_Member_List'];

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
	$ck_settings_admin_group_member_list_page_size = '';
	$Keyword_Member_List = '';
}
else 
	updateGetCookies($arrCookies);
	

### Check Access Right
$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

############## Interface Start ##############
$linterface = new interface_html();
$lreportcard_ui = new libreportcardrubrics_ui();

$AdminGroupID = $_REQUEST['AdminGroupID'];

$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['MenuTitle']);

#return msg
$ReturnMsgKey = $_REQUEST['ReturnMsgKey'];
$ReturnMsg = $Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['ReturnMessage'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

//echo $lreportcard_ui->Include_JS_CSS(array('cookies'));
echo $lreportcard_ui->Get_Setting_Admin_Group_Member_List_UI($AdminGroupID, $pageNo, $order, $field, $Keyword_Member_List);

?>
<br><br>

<script language="javascript">
$(document).ready( function() {	
	
});

function js_Back_To_Admin_Group_List()
{
	window.location = 'admin_group.php';
}
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>