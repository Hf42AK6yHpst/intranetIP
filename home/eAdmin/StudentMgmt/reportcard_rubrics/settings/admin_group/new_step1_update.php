<?php
// using :
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_AdminGroup";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
$lreportcard = new libreportcardrubrics();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

# Get data
$AdminGroupID = $_POST['AdminGroupID'];
$AccessiableMenuArr = $_POST['AccessiableMenuArr'];

$DataArr = array();
$DataArr['AdminGroupCode'] = trim(urldecode(stripslashes($_POST['AdminGroupCode'])));
$DataArr['AdminGroupNameEn'] = trim(urldecode(stripslashes($_POST['AdminGroupNameEn'])));
$DataArr['AdminGroupNameCh'] = trim(urldecode(stripslashes($_POST['AdminGroupNameCh'])));


$FromNew = 0;
if ($AdminGroupID == '')
{
	# New Admin Group
	$AdminGroupID = $lreportcard->Add_Admin_Group($DataArr);
	
	if ($AdminGroupID == 0)
	{
		// failed to add Admin Group
		$ReturnMsgKey = 'AddFailed';
		$NextPage = 'admin_group.php';
	}
	else
	{
		// Go to Step 2 to add member into the group
		$ReturnMsgKey = 'AddSuccess';
		$NextPage = 'new_step2.php';
		$FromNew = 1;
	}
}
else
{
	# Edit Admin group
	$Success = $lreportcard->Update_Admin_Group($AdminGroupID, $DataArr);
	$ReturnMsgKey = ($Success)? 'EditSuccess' : 'EditFailed';
	$NextPage = 'admin_group.php';
}

### Admin Group Access Right
if ($AdminGroupID > 0)
{
	$Success = $lreportcard->Update_Admin_Group_Access_Right($AdminGroupID, $AccessiableMenuArr);
}
	
intranet_closedb();

$para = "AdminGroupID=$AdminGroupID&ReturnMsgKey=$ReturnMsgKey&FromNew=$FromNew";
header("Location: $NextPage?$para");
?>