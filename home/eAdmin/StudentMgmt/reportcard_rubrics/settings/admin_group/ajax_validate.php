<?php
// using :
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
$lreportcard = new libreportcardrubrics();

# Get data
$Action = stripslashes($_REQUEST['Action']);

if ($Action == 'AdminGroupCode')
{
	$AdminGroupID = $_POST['AdminGroupID'];
	$AdminGroupCode = trim(urldecode(stripslashes($_POST['AdminGroupCode'])));
	$isValid = $lreportcard->Is_Admin_Group_Code_Valid($AdminGroupCode, $AdminGroupID);
	 
	echo ($isValid)? '1' : '0'; 
}
else if ($Action == 'Validate_Selected_Member')
{
	$AdminGroupID = $_POST['AdminGroupID'];
	$SelectedUserList = stripslashes($_REQUEST['SelectedUserList']);
	echo $lreportcard->Get_Invalid_Member_Selection($AdminGroupID, $SelectedUserList); 
}

intranet_closedb();
?>