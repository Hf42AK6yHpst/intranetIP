<?php
// using :
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_AdminGroup";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
$lreportcard = new libreportcardrubrics();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

# Get data
$AdminGroupID = $_POST['AdminGroupID'];
$UserIDArr = $_POST['UserIDArr'];
$success = $lreportcard->Delete_Admin_Group_Member($AdminGroupID, $UserIDArr);

$ReturnMsgKey = ($success)? "DeleteMemberSuccess" : "DeleteMemberFailed";

intranet_closedb();

header("Location: member_list.php?AdminGroupID=$AdminGroupID&ReturnMsgKey=".$ReturnMsgKey);
?>