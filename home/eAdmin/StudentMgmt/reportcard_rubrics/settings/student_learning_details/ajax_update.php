<?php
// using :
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_StudentLearningDetails";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

# Get data
$SubjectID = $_REQUEST['SubjectID'];
$SubjectGroupID = $_REQUEST['SubjectGroupID'];
$StudentID = $_REQUEST['StudentID'];
$StudyArr = $_REQUEST['StudyArr'];		// $StudyArr[$ModuleID][$TopicID] = 1;
$ModuleIDList = $_REQUEST['ModuleIDList'];
$TopicIDList = $_REQUEST['TopicIDList'];

$ModuleIDArr = explode(',', $ModuleIDList);
$TopicIDArr = explode(',', $TopicIDList);

$Success = $lreportcard->Save_Student_Study_Topic($SubjectID, $StudyArr, $SubjectGroupID, $StudentID, $ModuleIDArr,$TopicIDArr);
echo ($Success)? '1' : '0';

intranet_closedb();
?>