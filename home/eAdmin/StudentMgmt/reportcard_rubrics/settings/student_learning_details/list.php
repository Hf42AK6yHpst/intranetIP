<?php
// using: Connie
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_StudentLearningDetails";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard_ui = new libreportcardrubrics_ui();
$linterface = new interface_html();

$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

############## Interface Start ##############
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['SettingsArr']['StudentLearningDetailsArr']['MenuTitle']);
$linterface->LAYOUT_START();

echo $lreportcard_ui->Include_JS_CSS();
echo $lreportcard_ui->Get_Settings_StudentLearningDetails_UI();
?>

<script language="javascript">
var jsCurYearTermID = '';
var jsCurModuleID = '';
var jsCurSubjectID = '';
var jsCurSubjectGroupID = '';
var jsCurStudentID = '';
var jsMaxTopicLevel = <?=$eRC_Rubrics_ConfigArr['MaxLevel']?>;

$(document).ready( function() {	
	
	<? if($clearCoo) { ?>
		for(i=0; i<arrCookies.length; i++)
		{
			var obj = arrCookies[i];
			//alert('obj = ' + obj);
			$.cookies.del(obj);
		}
	<? } ?>
	
	jsCurYearTermID = $('select#YearTermID').val();
	js_Reload_Module_Selection();
});

function js_Changed_Term_Selection(jsYearTermID)
{
	jsCurYearTermID = jsYearTermID;
	js_Reload_Module_Selection();
}

function js_Changed_Module_Selection(jsModuleID)
{
	jsCurModuleID = jsModuleID;
	js_Reload_Subject_Selection();
}

function js_Changed_Subject_Selection(jsSubjectID)
{
	jsCurSubjectID = jsSubjectID;
	
	if (jsCurSubjectID == '')
	{
		$('div#SubjectGroupSelectionDiv').html('');
		$('div#StudentSelectionDiv').html('');
		$('div#StudentLearningDetailsDiv').html('');
	}
	else
	{
		js_Reload_Subject_Group_Selection();
		js_Reload_Topic_Selection(jsLevel=1, '', '', 0);
	}
}

function js_Changed_SubjectGroup_Selection(jsSubjectGroupID)
{
	jsCurSubjectGroupID = jsSubjectGroupID;

	var TopicID = getTopicFilterSelectVal(1);
	var ReloadStr = 'reload';
	if(TopicID=='')
	{
		ReloadStr='';
	}
	js_Reload_Subject_Group_Student_Selection(ReloadStr);
}

function js_Changed_Student_Selection(jsStudentID)
{
	jsCurStudentID = jsStudentID;
	
	if (jsCurStudentID == '')
	{
		$('div#SaveSG_WarningDiv').show();
		
		var TopicVal = getTopicFilterSelectVal(1);	
		if(TopicVal!='')
		{
			js_Reload_Student_Learning_Details_Table();
		}
	}		
	else
	{	
		var TopicVal = getTopicFilterSelectVal(1);		
		if(TopicVal!='')
		{
			$('div#SaveSG_WarningDiv').hide();
			js_Reload_Student_Learning_Details_Table();
		}	
	}	
}

function js_Reload_Module_Selection(ParReload)
{
	$('div#ModuleSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{ 
			Action: 'Module_Selection',
			YearTermID: jsCurYearTermID,
			SelectionID: 'ModuleID',
			OnChange: 'js_Changed_Module_Selection(this.value)',
			IsAll: 1,
			ModuleID: jsCurModuleID
		},
		function(ReturnData)
		{
			jsCurModuleID = $('select#ModuleID').val();
			js_Reload_Subject_Selection(ParReload);
		}
	);
}

function js_Reload_Subject_Selection(ParReload)
{
	$('div#SubjectSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{ 
			Action: 'Subject_Selection',
			YearTermID: jsCurYearTermID,
			ModuleID: jsCurModuleID,
			SelectionID: 'SubjectID',
			OnChange: 'js_Changed_Subject_Selection(this.value)',
			SubjectID: jsCurSubjectID,
			CheckModuleSubject: 1,
			FirstTitle: '<?=$Lang['SysMgr']['SubjectClassMapping']['Select']['Subject']?>',
			NoFirst: 0
		},
		function(ReturnData)
		{
			jsCurSubjectID = $('select#SubjectID').val();
			
//			if (jsCurSubjectID != '')
//			{
//				js_Reload_Subject_Group_Selection(ParReload);
//				js_Reload_Topic_Selection(jsLevel=1, '', '', 0);
//			}

			if (jsCurSubjectID == '') {
				$('div.LevelSelectionDiv').html('');
				$('div#SubjectGroupSelectionDiv').html('');
				$('div#StudentSelectionDiv').html('');
				$('div#StudentLearningDetailsDiv').html('');
			}
			else {	
				js_Reload_Subject_Group_Selection(ParReload);
				js_Reload_Topic_Selection(jsLevel=1, '', '', 0);
			}
		}
	);
}

function js_Reload_Subject_Group_Selection(ParReload)
{
	var jsDisplayTermInfo = (jsCurYearTermID=='')? 1 : 0;
	
	$('div#SubjectGroupSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{ 
			Action: 'Subject_Group_Selection',
			SubjectID: jsCurSubjectID,
			YearTermID: jsCurYearTermID,
			SelectionID: 'SubjectGroupID',
			OnChange: 'js_Changed_SubjectGroup_Selection(this.value)',
			DisplayTermInfo: jsDisplayTermInfo,
			SubjectGroupID: jsCurSubjectGroupID
		},
		function(ReturnData)
		{
			// use the second value as the default selection if no specified
			if ($('select#SubjectGroupID').val() == '')
				$('select#SubjectGroupID option:nth-child(2)').attr('selected', true);
			
			jsCurSubjectGroupID = $('select#SubjectGroupID').val();
			js_Reload_Subject_Group_Student_Selection(ParReload);
		}
	);
}

function js_Reload_Subject_Group_Student_Selection(ParReload)
{
	if (jsCurSubjectGroupID == '')
	{
		$('div#StudentSelectionDiv').html('');
		$('div#StudentLearningDetailsDiv').html('');
		$('div#SaveSG_WarningDiv').hide();
	}
	else
	{
		$('div#StudentSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
			"../../ajax_reload.php", 
			{ 
				Action: 'Subject_Group_Student_Selection',
				SubjectGroupID: jsCurSubjectGroupID,
				SelectionID: 'StudentID',
				OnChange: 'js_Changed_Student_Selection(this.value)',
				IsAll: 1,
				NoFirst: 0,
				StudentID: jsCurStudentID
			},
			function(ReturnData)
			{
				jsCurStudentID = $('select#StudentID').val();

				$('div#SaveSG_WarningDiv').show();			
				if(ParReload=='reload')
				{
					js_Reload_Student_Learning_Details_Table();
				}
			}
		);
	}
}

function js_Reload_Student_Learning_Details_Table()
{
	set_jsID();
	
	var NoStudent=0;
	if($('select#StudentID option').length<=1)
	{
		NoStudent=1;
	}

	$('div#StudentLearningDetailsDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{ 
			Action: 'Student_Learning_Details_Table',
			YearTermID: jsCurYearTermID,
			SubjectID: jsCurSubjectID,
			SubjectGroupID: jsCurSubjectGroupID,
			StudentID: jsCurStudentID,
			ModuleID: jsCurModuleID,
			FilterTopicInfoText: js_Get_Filtering_Info_Text(),
			noStudent: NoStudent
		},
		function(ReturnData)
		{
			
		}
	);
}

function js_Clicked_Checkbox(jsModuleID, jsTopicID, jsChecked)
{
	if (jsChecked == true)
		$('td#Cell_' + jsModuleID + '_' + jsTopicID).removeClass('not_check_cell').removeClass('special_cell');
	else
	{
		$('td#Cell_' + jsModuleID + '_' + jsTopicID).addClass('not_check_cell').removeClass('special_cell');
		$('input#ModuleAllChk_' + jsModuleID).attr('checked', jsChecked);
		$('input#TopicAllChk_' + jsTopicID).attr('checked', jsChecked);
		$('input#ModuleAllChk').attr('checked', jsChecked);
	}
}

function js_Select_All_Topic_And_Module(jsChecked)
{
	var jsTempID = '';
	var jsTempIDArr;
	var jsTempModuleID = '';
	var jsTempTopicID = '';
	
	$('input.ChkStudy').each( function() {
		$(this).attr('checked', jsChecked);
		
		jsTempID = $(this).attr('id');		// id = "Study_ModuleID_TopicID"
		jsTempIDArr = jsTempID.split('_');
		jsTempModuleID = jsTempIDArr[1];
		jsTempTopicID = jsTempIDArr[2];
		
		if (jsChecked == true)
			$('td#Cell_' + jsTempModuleID + '_' + jsTempTopicID).removeClass('not_check_cell').removeClass('special_cell');
		else
			$('td#Cell_' + jsTempModuleID + '_' + jsTempTopicID).addClass('not_check_cell').removeClass('special_cell');
	})
	
	$('input.ModuleAllChk').attr('checked', jsChecked);
	$('input.TopicAllChk').attr('checked', jsChecked);
}

function js_Select_All_Topic_Of_Module(jsModuleID, jsChecked)
{
	var jsTempID = '';
	var jsTempIDArr;
	var jsTempTopicID = '';
	
	$('input.ChkModuleID_' + jsModuleID).each( function() {
		jsTempID = $(this).attr('id');		// id = "Study_ModuleID_TopicID"
		jsTempIDArr = jsTempID.split('_');
		jsTempTopicID = jsTempIDArr[2];
		
		$(this).attr('checked', jsChecked);
		js_Clicked_Checkbox(jsModuleID, jsTempTopicID, jsChecked);
	})
}

function js_Select_All_Module_Of_Topic(jsTopicID, jsChecked)
{
	var jsTempID = '';
	var jsTempIDArr;
	var jsTempModuleID = '';
	
	$('input.ChkTopicID_' + jsTopicID).each( function() {
		jsTempID = $(this).attr('id');		// id = "Study_ModuleID_TopicID"
		jsTempIDArr = jsTempID.split('_');
		jsTempModuleID = jsTempIDArr[1];
		
		$(this).attr('checked', jsChecked);
		js_Clicked_Checkbox(jsTempModuleID, jsTopicID, jsChecked);
	})
}

function js_Save_Study_Learning_Details()
{
	if (jsCurStudentID == '')
	{
		if (!confirm('<?=$Lang['eRC_Rubrics']['SettingsArr']['StudentLearningDetailsArr']['jsWarningArr']['RecordOverwriteIfSaveForSG']?>'))
			return false;
	}
	
	var jsSubmitString = Get_Form_Values(document.getElementById('form1'));

	$.ajax({  
		type: "POST",  
		url: "ajax_update.php",
		data: jsSubmitString,  
		success: function(data) {
			
			if (data=='1')
			{
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>');
				Scroll_To_Top();
				js_Reload_Student_Learning_Details_Table();
			}
			else
			{
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>');
				Scroll_To_Top();
			}
		} 
	});  
	return false;
}

function js_Changed_Reload_Topic_Selection(jsLevel, jsPreviousLevelTopicID, jsTargetTopicID, jsReloadTable)
{
	var jsOriginalLevel = jsLevel - 1;

	js_Reload_Topic_Selection(jsLevel, jsPreviousLevelTopicID, jsTargetTopicID, jsReloadTable);
}

function js_Reload_Topic_Selection(jsLevel, jsPreviousLevelTopicID, jsTargetTopicID, jsReloadTable)
{
	set_jsID();
	
	if (jsTargetTopicID == null)
		jsTargetTopicID = '';
		
	if (jsReloadTable == null)
		jsReloadTable = 1;
		
	// Hide the all Next Level Topic Selection
	var i;
	for (i=jsLevel; i<jsMaxTopicLevel; i++)
	{
		$('div#Level' + i + '_SelectionDiv').html('');
	}

	if (jsLevel < jsMaxTopicLevel)
	{
		if (jsLevel==1 || (jsLevel > 1 && jsPreviousLevelTopicID != ''))
		{
			js_Reload_Individual_Topic_Selection(jsLevel, jsPreviousLevelTopicID, jsTargetTopicID);
		}
		else
		{
			$('#StudentLearningDetailsDiv').html('');
		}
	}
	
	if (jsReloadTable==1)
	{
		js_Reload_Student_Learning_Details_Table();
	}
		
}


function js_Reload_Individual_Topic_Selection(jsLevel, jsPreviousLevelTopicID, jsTargetTopicID)
{	
	set_jsID();
	var TopicVal = getTopicFilterSelectVal(1);				
	var jsNextLevel = parseInt(jsLevel) + 1;
	
	var AllTitle = '';
	var SelectTitle ='';
	var jsReloadTableAfterOnChange = 1;
	
	if(jsLevel==1)
	{
		jsReloadTableAfterOnChange = 0;
	}


	$('div#Level' + jsLevel + '_SelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{
			Action: 'Reload_Topic_Selection',
			async: false,
			SubjectID: jsCurSubjectID,
			Level: jsLevel,
			noFirst: '1',
			hasChooseOption: true,
			PreviousLevelTopicID: jsPreviousLevelTopicID,
			SelectionID: 'Level' + jsLevel + '_TopicID',
			OnChange: 'js_Changed_Reload_Topic_Selection(' + jsNextLevel + ', this.value, \'\', ' + jsReloadTableAfterOnChange + ');',
			TopicID: jsTargetTopicID
		},
		function(ReturnData)
		{	  
			TopicVal = getTopicFilterSelectVal(1);	
			if(TopicVal!='')
			{
				js_Reload_Student_Learning_Details_Table();
			}
			else
			{
				$('div#StudentLearningDetailsDiv').html('');
			}  
			if(jsPreviousLevelTopicID=='ALL')
			{
				$('div#Level'+jsLevel+'_SelectionDiv').html('');
			}
		}
	);
	
	if(jsPreviousLevelTopicID=='ALL')
	{
		$('div#Level'+jsLevel+'_SelectionDiv').html('');
	}

}

function set_jsID()
{
	jsCurModuleID = $('select#ModuleID').val();
	jsCurSubjectID = $('select#SubjectID').val();
	if ($('select#SubjectGroupID').val() == '')
	{
		$('select#SubjectGroupID option:nth-child(1)').attr('selected', true);
	}			
	jsCurSubjectGroupID = $('select#SubjectGroupID').val();					
    jsCurStudentID = $('select#StudentID').val();
}

function getTopicFilterSelectVal(ParLevel)
{
	var Val = $('select#Level'+ParLevel+'_TopicID').val();			
	if (Val==null || Val=='undefined') {
		Val = '';
	}
	return Val;
}

</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>