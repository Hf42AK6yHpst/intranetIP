<?php
// using :
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_ECA";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_eca.php");
$lreportcard_eca = new libreportcardrubrics_eca();
$lreportcard_eca->Has_Access_Right($PageRight, $CurrentPage);


# Get data
$Action = stripslashes($_REQUEST['Action']);

if ($Action == 'SaveCategoryInfo')
{
	$EcaCategoryCode = trim(stripslashes($_REQUEST['EcaCategoryCode']));
	$EcaCategoryNameEn = trim(stripslashes($_REQUEST['EcaCategoryNameEn']));
	$EcaCategoryNameCh = trim(stripslashes($_REQUEST['EcaCategoryNameCh']));
	$DisplayOrder = stripslashes($_REQUEST['DisplayOrder']);
	
		$success["InsertCategory"] = $lreportcard_eca->InsertCategory($EcaCategoryCode,$EcaCategoryNameEn,$EcaCategoryNameCh,$DisplayOrder);
		if($success["InsertCategory"])
			echo $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['InsertCategorySuccess'];
		else
			echo $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['InsertCategoryFail'];
	
}
else if($Action == 'EcaCategoryEditableUpdate')
{
	$id = stripslashes($_REQUEST['id']);
	$value = trim(stripslashes($_REQUEST['value']));
	$postData = explode("_",$id);
	$FieldName = $postData[0];
	$EcaCategoryID = $postData[1];
	
	$EcaCategoryInfo[$FieldName] = $value;
	if($FieldName == "EcaCategoryCode" )
	{
		if(!valid_code($value))
		{
			echo 'fail|=msg=|0|=|'.$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['CodeMustBeginWithLetter'];
			exit;
		}
		else if(!$lreportcard_eca->Is_ECA_Category_Code_Valid($value,$EcaCategoryID))
		{
			echo 'fail|=msg=|'.$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['ECACodeNotAvailible'];
			exit;
		}


	}
	$success["UpdateCategory"] = $lreportcard_eca->UpdateCategory($EcaCategoryID,$EcaCategoryInfo);
	
	if($success["UpdateCategory"])
		echo $value.'|=msg=|'.$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['UpdateCategorySuccess'];
	else
		echo 'fail|=msg=|'.$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['UpdateCategoryFail'];
}
else if($Action == 'DeleteEcaCategory')
{
	$EcaCategoryID = stripslashes($_REQUEST['EcaCategoryID']);
	$UpdateInfo['RecordStatus'] = 0;
	
	$SubCategoryInfo = $lreportcard_eca->Get_ECA_SubCategory_Info($EcaCategoryID);
	$SubCatIDArr = Get_Array_By_Key($SubCategoryInfo,"EcaSubCategoryID");
	
	$lreportcard_eca->Start_Trans();
	
	$success["UpdateCategory"] = $lreportcard_eca->UpdateCategory($EcaCategoryID,$UpdateInfo);
	$success["UpdateSubCategory"] = $lreportcard_eca->UpdateSubCategory($EcaCategoryID,$UpdateInfo,1);
	if(!empty($SubCatIDArr))
		$success["UpdateItem"] = $lreportcard_eca->UpdateItem($SubCatIDArr,$UpdateInfo,1);
	
	if(!in_array(false,$success))
	{
		$lreportcard_eca->Commit_Trans();
		echo $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['DeleteCategorySuccess'];
	}
	else
	{
		$lreportcard_eca->RollBack_Trans();
		echo $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['DeleteCategoryFail'];
	}
}
else if($Action == 'UpdateCategoryDisplayOrder')
{
	$EcaCategoryIDArr = $_REQUEST['EcaCategoryID'];
	
	$lreportcard_eca->Start_Trans();
	foreach($EcaCategoryIDArr as $order => $EcaCategoryID)
	{
		$EcaCategoryInfo['DisplayOrder'] = $order+1;
		$success[] = $lreportcard_eca->UpdateCategory($EcaCategoryID,$EcaCategoryInfo);
	}
	
	if(in_array(false,$success))
	{
		$lreportcard_eca->RollBack_Trans();
		echo $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['UpdateCategoryFail'];
	}
	else
	{
		$lreportcard_eca->Commit_Trans();
		echo $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['UpdateCategorySuccess'];
	}
	
}
else if ($Action == 'SaveSubCategoryInfo')
{
	$EcaCategoryID = stripslashes($_REQUEST['EcaCategoryID']);
	$EcaSubCategoryID = stripslashes($_REQUEST['EcaSubCategoryID']);
	$EcaSubCategoryCode = trim(stripslashes($_REQUEST['EcaCategoryCode']));
	$EcaSubCategoryNameEn = trim(stripslashes($_REQUEST['EcaCategoryNameEn']));
	$EcaSubCategoryNameCh = trim(stripslashes($_REQUEST['EcaCategoryNameCh']));
	$DisplayOrder = stripslashes($_REQUEST['DisplayOrder']);
	
	if(trim($EcaSubCategoryID)=='') # insert category
	{
		$success["InsertSubCategory"] = $lreportcard_eca->InsertSubCategory($EcaCategoryID,$EcaSubCategoryCode,$EcaSubCategoryNameEn,$EcaSubCategoryNameCh,$DisplayOrder);
		if($success["InsertSubCategory"])
			echo $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['InsertSubCategorySuccess'];
		else
			echo $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['InsertSubCategoryFail'];
	}
	else # update category
	{
		$EcaSubCategoryInfoArr['EcaSubCategoryCode'] = $EcaSubCategoryCode;
		$EcaSubCategoryInfoArr['EcaSubCategoryNameEn'] = $EcaSubCategoryNameEn;
		$EcaSubCategoryInfoArr['EcaSubCategoryNameCh'] = $EcaSubCategoryNameCh;
		
		$success["UpdateSubCategory"] = $lreportcard_eca->UpdateSubCategory($EcaSubCategoryID,$EcaSubCategoryInfoArr);
		if($success["UpdateSubCategory"])
			echo $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['UpdateSubCategorySuccess'];
		else
			echo $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['UpdateSubCategoryFail'];

	}
	
}
else if($Action == 'DeleteEcaSubCategory')
{
	$EcaSubCategoryID = stripslashes($_REQUEST['EcaSubCategoryID']);
	$UpdateInfo['RecordStatus'] = 0;
	
	$lreportcard_eca->Start_Trans();
	
	$success["UpdateSubCategory"] = $lreportcard_eca->UpdateSubCategory($EcaSubCategoryID,$UpdateInfo);
	$success["UpdateItem"] = $lreportcard_eca->UpdateItem($EcaSubCategoryID,$UpdateInfo,1);
	
	if(!in_array(false,$success))
	{
		$lreportcard_eca->Commit_Trans();
		echo $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['DeleteSubCategorySuccess'];
	}
	else
	{
		$lreportcard_eca->RollBack_Trans();
		echo $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['DeleteSubCategoryFail'];
	}
}
else if($Action == 'UpdateSubCategoryDisplayOrder')
{
	$EcaSubCategoryIDArr = $_REQUEST['EcaSubCategoryID'];
	
	$lreportcard_eca->Start_Trans();
	foreach($EcaSubCategoryIDArr as $order => $EcaSubCategoryID)
	{
		$EcaSubCategoryInfo['DisplayOrder'] = $order+1;
		$success[] = $lreportcard_eca->UpdateSubCategory($EcaSubCategoryID,$EcaSubCategoryInfo);
	}
	
	if(in_array(false,$success))
	{
		$lreportcard_eca->RollBack_Trans();
		echo $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['UpdateSubCategoryFail'];
	}
	else
	{
		$lreportcard_eca->Commit_Trans();
		echo $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['UpdateSubCategorySuccess'];
	}
	
}
else if ($Action == 'SaveItemInfo')
{
	$EcaSubCategoryID = stripslashes($_REQUEST['TBEcaSubCategoryID']);
	$EcaItemID = stripslashes($_REQUEST['TBEcaItemID']);
	$EcaItemCode = trim(stripslashes($_REQUEST['EcaItemCode']));
	$EcaItemNameEn = trim(stripslashes($_REQUEST['EcaItemNameEn']));
	$EcaItemNameCh = trim(stripslashes($_REQUEST['EcaItemNameCh']));
	$DisplayOrder = stripslashes($_REQUEST['DisplayOrder']);
	
	if(trim($EcaItemID)=='') # insert category
	{
		$success["InsertItem"] = $lreportcard_eca->InsertItem($EcaSubCategoryID,$EcaItemCode,$EcaItemNameEn,$EcaItemNameCh,$DisplayOrder);
		if($success["InsertItem"])
			echo $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['InsertItemSuccess'];
		else
			echo $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['InsertItemFail'];
	}
	else # update category
	{
		$EcaItemInfoArr['EcaItemCode'] = $EcaItemCode;
		$EcaItemInfoArr['EcaItemNameEn'] = $EcaItemNameEn;
		$EcaItemInfoArr['EcaItemNameCh'] = $EcaItemNameCh;
		
		$success["UpdateItem"] = $lreportcard_eca->UpdateItem($EcaItemID,$EcaItemInfoArr);
		if($success["UpdateItem"])
			echo $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['UpdateItemSuccess'];
		else
			echo $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['UpdateItemFail'];

	}
	
}
else if($Action == 'DeleteEcaItem')
{
	$EcaItemID = stripslashes($_REQUEST['EcaItemID']);
	$EcaSubCategoryInfo['RecordStatus'] = 0;
	
	$success["UpdateItem"] = $lreportcard_eca->UpdateItem($EcaItemID,$EcaSubCategoryInfo);
	
	if($success["UpdateItem"])
		echo $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['DeleteItemSuccess'];
	else
		echo $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['DeleteItemFail'];
}
else if($Action == 'UpdateItemDisplayOrder')
{
	$EcaItemIDArr = $_REQUEST['EcaItemID'];
	
	$lreportcard_eca->Start_Trans();
	foreach($EcaItemIDArr as $order => $EcaItemID)
	{
		$EcaItemInfo['DisplayOrder'] = $order+1;
		$success[] = $lreportcard_eca->UpdateItem($EcaItemID,$EcaItemInfo);
	}
	
	if(in_array(false,$success))
	{
		$lreportcard_eca->RollBack_Trans();
		echo $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['UpdateItemFail'];
	}
	else
	{
		$lreportcard_eca->Commit_Trans();
		echo $Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsReturnMsgArr']['UpdateItemSuccess'];
	}
	
}

intranet_closedb();
?>