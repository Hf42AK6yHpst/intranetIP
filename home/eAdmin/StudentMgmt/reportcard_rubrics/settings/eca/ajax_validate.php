<?php
// using :
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_eca.php");
$lreportcard_eca = new libreportcardrubrics_eca();


# Get data
$Action = stripslashes($_REQUEST['Action']);

if ($Action == 'ValidateEcaCategoryCode')
{
	$EcaCategoryID = stripslashes($_REQUEST['EcaCategoryID']);
	$EcaCategoryCode = stripslashes($_REQUEST['EcaCategoryCode']);

	$Valid = $lreportcard_eca->Is_ECA_Category_Code_Valid($EcaCategoryCode, $EcaCategoryID);
	echo ($Valid)? '1' : '0';
}
else if ($Action == 'ValidateEcaSubCategoryCode')
{
	$EcaSubCategoryID = stripslashes($_REQUEST['EcaSubCategoryID']);
	$EcaSubCategoryCode = stripslashes($_REQUEST['EcaSubCategoryCode']);

	$Valid = $lreportcard_eca->Is_ECA_SubCategory_Code_Valid($EcaSubCategoryCode, $EcaSubCategoryID);
	echo ($Valid)? '1' : '0';
}
else if($Action == 'ValidateEcaItemCode')
{
	$EcaItemID = stripslashes($_REQUEST['EcaItemID']);
	$EcaItemCode = stripslashes($_REQUEST['EcaItemCode']);

	$Valid = $lreportcard_eca->Is_ECA_Item_Code_Valid($EcaItemCode, $EcaItemID);
	echo ($Valid)? '1' : '0';
	
}





intranet_closedb();
?>