<?php
// using :
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");

$lreportcard_ui = new libreportcardrubrics_ui();

//debug_pr($_REQUEST);

# Get data
$Action = stripslashes($_REQUEST['Action']);

switch ($Action)
{
	case "LoadECACategoryTable":
		echo $lreportcard_ui->Get_Setting_ECA_Category_Table();
	break;
	
	case "LoadECACategoryEditLayer":
		echo $lreportcard_ui->Get_Setting_ECA_Category_Edit_Layer();
	break;
	
	case "LoadSubCategoryEditLayer":
		$EcaCategoryID = $_REQUEST['EcaCategoryID'];
		$EcaSubCategoryID = $_REQUEST['EcaSubCategoryID'];

		echo $lreportcard_ui->Get_Setting_ECA_SubCategory_Edit_Layer($EcaCategoryID, $EcaSubCategoryID);
	break; 
	
	case "LoadItemTable":
	
		$EcaCategoryID = $_REQUEST['EcaCategoryID'];
		$EcaSubCategoryID = $_REQUEST['EcaSubCategoryID'];

		echo $lreportcard_ui->Get_Setting_ECA_Item_Table($EcaCategoryID, $EcaSubCategoryID);
	break; 
	
	case "LoadItemEditLayer":
		$EcaItemID = $_REQUEST['EcaItemID'];
		$EcaSubCategoryID = $_REQUEST['EcaSubCategoryID'];

		echo $lreportcard_ui->Get_Setting_ECA_Item_Edit_Layer($EcaSubCategoryID,$EcaItemID);
	break;
	
//	case "LoadSubCategorySelection":
//		$EcaCategoryID = $_REQUEST['EcaCategoryID'];
//		
//		$SubCategorySelection = $lreportcard_ui->Get_ECA_SubCategory_Selection("EcaSubCategoryID",$EcaCategoryID,$EcaSubCategoryID=0,$Onchange='js_Reload_Item_Table();', $noFirst=0, $isAll=1);
//		
//		echo $SubCategorySelection;
//	break;
}

intranet_closedb();
?>