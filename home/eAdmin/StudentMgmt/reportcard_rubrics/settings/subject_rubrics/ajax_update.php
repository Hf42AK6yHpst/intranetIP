<?php
// using :
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_SubjectRubrics";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_rubrics.php");
$lreportcard_rubrics = new libreportcardrubrics_rubrics();
$lreportcard_rubrics->Has_Access_Right($PageRight, $CurrentPage);


# Get data
$Action = stripslashes($_REQUEST['Action']);

if ($Action == 'SaveRubricInfo')
{
	$RubricsID = stripslashes($_REQUEST['RubricsID']);
	$SaveAs = stripslashes($_REQUEST['SaveAs']);
	
	$RubricsDataArr["RubricsCode"] = stripslashes($_REQUEST['RubricsCode']);
	$RubricsDataArr["RubricsEn"] = stripslashes($_REQUEST['RubricsEn']);
	$RubricsDataArr["RubricsCh"] = stripslashes($_REQUEST['RubricsCh']);
	
	$RubricsItem = $_REQUEST['RubricsItem'];
	$RubricsItemDesc = $_REQUEST['RubricsItemDesc'];
	
	if(trim($RubricsID) == '' || trim($SaveAs) == 1) // insert new rubrics
	{
		$lreportcard_rubrics->Start_Trans();
		
		$NewRubricsID = $lreportcard_rubrics->InsertRubrics($RubricsDataArr);
		
		foreach($RubricsItem as $key => $RubricsLevel)
		{
			$RubricsItemInfo[$key]["LevelNameEn"] = $RubricsLevel;
			$RubricsItemInfo[$key]["LevelNameCh"] = $RubricsLevel;
			$RubricsItemInfo[$key]["DescriptionEn"] = $RubricsItemDesc[$key];
			$RubricsItemInfo[$key]["DescriptionCh"] = $RubricsItemDesc[$key];
			$RubricsItemInfo[$key]["Level"] = $key+1;
		}
		
		$Success = $lreportcard_rubrics->InsertRubricsItem($NewRubricsID,$RubricsItemInfo);
		
		if(!$Success)
		{
			$lreportcard_rubrics->RollBack_Trans();
			echo $Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsReturnMsgArr']['InsertFail'];
		}
		else
		{
			$lreportcard_rubrics->Commit_Trans();
			$RubricsName = $RubricsDataArr["RubricsEn"];
			$RubricsCode = $RubricsDataArr["RubricsCode"];
			
			echo $Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsReturnMsgArr']['InsertSuccess']."|==|Insert|==|$NewRubricsID|==|$RubricsName|==|$RubricsCode";
		}
		
	}
	else // edit rubrics
	{
		# Get existing item list
		$RubricsInfoArr = $lreportcard_rubrics->Get_Rubics_Item_Info($RubricsID);
		$CurrRubricsItemIDList = Get_Array_By_Key($RubricsInfoArr,"RubricsItemID");
		
		$lreportcard_rubrics->Start_Trans();
		
		$Success["UpdateRubrics"][] = $lreportcard_rubrics->UpdateRubrics($RubricsID, $RubricsDataArr);

		foreach($RubricsItem as $key => $RubricsLevel)
		{
			$RubricsItemInfo[$key]["LevelNameEn"] = $RubricsLevel;
			$RubricsItemInfo[$key]["LevelNameCh"] = $RubricsLevel;
			$RubricsItemInfo[$key]["DescriptionEn"] = $RubricsItemDesc[$key];
			$RubricsItemInfo[$key]["DescriptionCh"] = $RubricsItemDesc[$key];
			$RubricsItemInfo[$key]["Level"] = $key+1;
			$thisRubricsItemID = $RubricsItemID[$key];
			
			if(trim($thisRubricsItemID)=='new')// newly added item
			{
				$InsertRubricsItemInfo[] = $RubricsItemInfo[$key];
			}
			else  // update existing rubrics item 
			{
				$Success["UpdateRubricsItem"][] = $lreportcard_rubrics->UpdateRubricsItem($thisRubricsItemID,$RubricsItemInfo[$key]);
			}
			
			# prepare array of ids which do not need to be disabled
			$ActiveRubricsIDItemList[] = $thisRubricsItemID;	
		}
		
		# Insert Rubrics items in bulk
		if(count($InsertRubricsItemInfo)>0)
			$Success["InsertRubricsItem"][] = $lreportcard_rubrics->InsertRubricsItem($RubricsID,$InsertRubricsItemInfo);
		
		# Update Record Status of deleted rubrics items
		$DisableRubricsItemIDList = array_diff($CurrRubricsItemIDList,$ActiveRubricsIDItemList);
		
		if(count($DisableRubricsItemIDList)>0)
			$Success["DeleteRubricsItem"][] = $lreportcard_rubrics->DeleteRubricsItem($DisableRubricsItemIDList);
		
		if(in_array(false,(array)$Success["UpdateRubrics"])||in_array(false,(array)$Success["UpdateRubricsItem"])||in_array(false,(array)$Success["InsertRubricsItem"])||in_array(false,(array)$Success["DeleteRubricsItem"]))
		{
			$lreportcard_rubrics->RollBack_Trans();
			echo $Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsReturnMsgArr']['UpdateFail'];
		}
		else
		{
			$lreportcard_rubrics->Commit_Trans();
			$RubricsName = $RubricsDataArr["RubricsEn"];
			$RubricsCode = $RubricsDataArr["RubricsCode"];
			
			echo $Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsReturnMsgArr']['UpdateSuccess']."|==|Update|==|$RubricsID|==|$RubricsName|==|$RubricsCode";
		}
	}
}
else if($Action == "DeleteRubrics")
{
	$RubricsID = stripslashes($_REQUEST['RubricsID']);
	
	$Success = $lreportcard_rubrics->DeleteRubrics($RubricsID);	
	if($Success)
	{
		echo $Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsReturnMsgArr']['DeleteSuccess'];
	}
	else
	{
		echo $Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsReturnMsgArr']['DeleteFail'];
	}
} 
else if ($Action == "SaveRubricSubjectMap")
{
	$YearID = stripslashes($_REQUEST['YearID']);
	
	$lreportcard_rubrics->Start_Trans();
	
	$RubricsSubjectMap = $lreportcard_rubrics->Get_Rubrics_Subject_Mapping($YearID);
	foreach((array)$RubricsScheme as $SubjectID => $RubricsID)
	{
		$CurrRubricsID = $RubricsSubjectMap[$YearID][$SubjectID];
		if($CurrRubricsID == $RubricsID)
			continue;
		else if(!empty($CurrRubricsID))
			$Success[$SubjectID] = $lreportcard_rubrics->UpdateSubjectRubricsMap($YearID,$SubjectID,$RubricsID);
		else
			$Success[$SubjectID] = $lreportcard_rubrics->InsertSubjectRubricsMap($YearID,$SubjectID,$RubricsID);
	}
	
	if(in_array(false,(array)$Success))
	{
		echo $Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsReturnMsgArr']['SaveFail'];
		$lreportcard_rubrics->RollBack_Trans();
	}
	else
	{
		echo $Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsReturnMsgArr']['SaveSuccess'];
		$lreportcard_rubrics->Commit_Trans();
	}
	
}

intranet_closedb();
?>