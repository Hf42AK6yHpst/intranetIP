<?php
// using :
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");

$lreportcard_ui = new libreportcardrubrics_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);

switch ($Action)
{
	case "LoadRubricsSettingTBLayer":
		$RubricsID = $_REQUEST['RubricsID'];

		echo $lreportcard_ui->Get_Setting_Rubrics_Edit_Layer($RubricsID);
	break; 
	
	case "loadRubricsSettingTable":
		$YearID = $_REQUEST['YearID'];
	
		echo $lreportcard_ui->Get_Setting_Rubrics_Table($YearID);
	break;
}

intranet_closedb();
?>