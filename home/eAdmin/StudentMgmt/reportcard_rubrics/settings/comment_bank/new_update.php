<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_CommentBank";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_comment.php");

### Check Access Right
$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$lreportcard_comment = new libreportcardrubrics_comment();

$CommentCode = trim(stripslashes($_REQUEST['CommentCode']));
$CommentEn = trim(stripslashes($_REQUEST['CommentEn']));
$CommentCh = trim(stripslashes($_REQUEST['CommentCh']));
$TopicID = trim(stripslashes($_REQUEST['TopicID']));

if(valid_code($CommentCode))
	$Success = $lreportcard_comment->Insert_Comment_Bank_Comment($CommentCode,$CommentEn,$CommentCh, $TopicID); 
else
	$Success = false;
	
$msg = $Success?"AddSuccess":"AddUnsuccess";

intranet_closedb();

header("Location:index.php?msg=$msg");
?>

