<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_CommentBank";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_comment.php");

$commentType = ($_GET['commentType'])? $_GET['commentType'] : 'class';

$lreportcard = new libreportcardrubrics_custom();
$lreportcard_ui = new libreportcardrubrics_ui();
$linterface = new interface_html();

$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ = $lreportcard_ui->Get_Settings_CommentBank_Tab($commentType);

$ReturnMsg = $Lang['General']['ReturnMessage'][$msg]; 
$linterface->LAYOUT_START($ReturnMsg);

echo $lreportcard_ui->Get_Setting_CommentBank_Import_UI($commentType);
?>
<script type="text/JavaScript" language="JavaScript">
function checkForm() {
	obj = document.form1;
	if (obj.elements["userfile"].value.Trim() == "") {
		 alert('<?=$Lang['eRC_Rubrics']['GeneralArr']['jsWarningArr']['AlertSelectFile']?>');
		 obj.elements["userfile"].focus();
		 return false;
	}
	
	return true;
}
</script>
<br />

<?
	print $linterface->FOCUS_ON_LOAD("form1.userfile");
  	$linterface->LAYOUT_STOP();
?>
