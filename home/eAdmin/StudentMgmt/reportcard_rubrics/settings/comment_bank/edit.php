<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_CommentBank";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");

$CommentID = $_REQUEST['CommentID'];
$commentType = $_REQUEST['commentType'];


### Check Access Right
$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$linterface = new interface_html();
$lreportcard_ui = new libreportcardrubrics_ui();

# tag information
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ = $lreportcard_ui->Get_Settings_CommentBank_Tab($commentType);


$linterface->LAYOUT_START();

echo $lreportcard_ui->Get_Settings_CommentBank_New_Edit_UI($CommentID, $commentType); 
?>
<br/>
<script>

var jsCurSubjectID = '';
var jsMaxTopicLevel = <?=$eRC_Rubrics_ConfigArr['MaxLevel']?>;

var jsFirstLoad = true;

var CodeValid = true;
var WarnMsg = new Array();
WarnMsg["CommentCode"] = "<?=$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['jsWarningArr']['Empty']['CommentCode']?>";
WarnMsg["CommentEn"] = "<?=$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['jsWarningArr']['Empty']['CommentEn']?>";
WarnMsg["CommentCh"] = "<?=$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['jsWarningArr']['Empty']['CommentCh']?>";


$().ready(function(){
	<? if($clearCoo) { ?>
		for(i=0; i<arrCookies.length; i++)
		{
			var obj = arrCookies[i];
			//alert('obj = ' + obj);
			$.cookies.del(obj);
		}
	<? } ?>
	//aj_CheckCode();	
	
	js_Reload_Subject_Selection('reload');
	
})


function aj_CheckCode()
{
	var val = $("#CommentCode").val();

	var CommentID = $("#CommentID").val(); 
	if(val.Trim()=='')
	{
		$("#WarnCommentCode").html(WarnMsg["CommentCode"]);
		CodeValid = false;
		return false;
	}	
	if(!valid_code(val))
	{
		$("#WarnCommentCode").html("<?=$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['jsWarningArr']['WrongCodeFormat']?>");
		CodeValid = false;
		return false;
	}
			
	$.post(
		"ajax_validate.php",
		{
			Action:"ValidateCommentCode",
			CommentCode:val,
			CommentID:CommentID	
		},
		function(ReturnData)
		{
			if(ReturnData==0)
			{
				$("#WarnCommentCode").html("<?=$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['jsWarningArr']['CommentCodeNotAvailible']?>");
				CodeValid = false;
			}
			else
			{
				$("#WarnCommentCode").html("");		
				CodeValid = true;		
			}
		}
	);
	
}

function checkForm()
{
	var FormValid = CodeValid;
	
	$(".WarnRequired").html('');
	$(".required").each(function(){
		FormValid = (check_text_30(this, WarnMsg[$(this).attr("id")], "Warn"+$(this).attr("id"))&& FormValid);
	});
	
	aj_CheckCode();

	//$('#Level"+jsMaxTopicLevel+"_TopicID")
	
	FormValid = checkTopicID();
	
	if (FormValid) {
		document.form1.submit();
	}
}

function checkTopicID()
{
	for (i=1; i<=jsMaxTopicLevel; i++)
	{
		var thisTopicID = $('select#Level' + i + '_TopicID').val();
		
		
		if(thisTopicID=='' || thisTopicID===undefined || thisTopicID==null)
		{
			alert('<?=$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['WarningTopic'] ?>');
			return false;
		}
		
	}
	
	return true;
}


function js_Reload_Subject_Selection(ParReload)
{
    
	$('div#SubjectSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{ 
			Action: 'Subject_Selection',
			SelectionID: 'SubjectID',
			OnChange: 'js_Changed_Subject_Selection(this.value)',
			SubjectID: jsCurSubjectID,
			FirstTitle: '<?=$Lang['SysMgr']['SubjectClassMapping']['Select']['Subject']?>',
			NoFirst: 0
		},
		function(ReturnData)
		{
			var CommentID = $("#CommentID").val(); 
			var SubjectID = $("#TopicPath_SubjectID").val();  
			if(CommentID.Trim()=='')   // it is new record
			{
				
			}
			else if (jsFirstLoad==true)
			{				
			//	alert(SubjectID);
			//	alert($("select#SubjectID optgroup option[value='"+SubjectID+"']").text());

				$("select#SubjectID optgroup option[value='"+SubjectID+"']").attr("selected","selected");
				js_Changed_Subject_Selection(SubjectID);
			
			}
		
		}
	);
}

function js_Changed_Subject_Selection(jsSubjectID)
{
	jsCurSubjectID = jsSubjectID;
	
	if (jsCurSubjectID == '')
	{
		var i=1;
		for (i=1; i<=jsMaxTopicLevel; i++)
		{
			$('div#Level' + i + '_SelectionDiv').html('');
		}
	 
	}
	else
	{
		js_Reload_Topic_Selection(jsLevel=1, '', '', 0);
	}
}

function js_Changed_Reload_Topic_Selection(jsLevel, jsPreviousLevelTopicID, jsTargetTopicID, jsReloadTable)
{
	var jsOriginalLevel = jsLevel - 1;

	js_Reload_Topic_Selection(jsLevel, jsPreviousLevelTopicID, jsTargetTopicID, jsReloadTable);
}

function js_Reload_Topic_Selection(jsLevel, jsPreviousLevelTopicID, jsTargetTopicID, jsReloadTable)
{
	set_jsID();
	
	if (jsTargetTopicID == null)
		jsTargetTopicID = '';
		
	if (jsReloadTable == null)
		jsReloadTable = 1;
		
	// Hide the all Next Level Topic Selection
	var i;
	for (i=jsLevel; i<=jsMaxTopicLevel; i++)
	{
		$('div#Level' + i + '_SelectionDiv').html('');
	}

	if (jsLevel <= jsMaxTopicLevel)
	{
		if (jsLevel==1 || (jsLevel > 1 && jsPreviousLevelTopicID != ''))
		{
			js_Reload_Individual_Topic_Selection(jsLevel, jsPreviousLevelTopicID, jsTargetTopicID);
		}
		else
		{
		}
	}
		
}


function js_Reload_Individual_Topic_Selection(jsLevel, jsPreviousLevelTopicID, jsTargetTopicID)
{	
	set_jsID();
	var TopicVal = getSelectVal(1);				
		
	var jsNextLevel = parseInt(jsLevel) + 1;
	
	var AllTitle = '';
	var SelectTitle ='';
	var jsReloadTableAfterOnChange = 1;
	
	if(jsLevel==1)
	{
		jsReloadTableAfterOnChange = 0;
	}

	$('div#Level' + jsLevel + '_SelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{
			Action: 'Reload_Topic_Selection',
			async: false,
			SubjectID: jsCurSubjectID,
			Level: jsLevel,
			noFirst: '1',
			hasChooseOption: 0,
			indicator: 'CommentBank',
			PreviousLevelTopicID: jsPreviousLevelTopicID,
			SelectionID: 'Level' + jsLevel + '_TopicID',
			OnChange: 'js_Changed_Reload_Topic_Selection(' + jsNextLevel + ', this.value, \'\', ' + jsReloadTableAfterOnChange + ');',
			TopicID: jsTargetTopicID
		},
		function(ReturnData)
		{	 		 
			TopicVal = getSelectVal(1);	

			if(jsPreviousLevelTopicID=='ALL')
			{
				$('div#Level'+jsLevel+'_SelectionDiv').html('');
			}
						
			var CommentID = $("#CommentID").val(); 
			var thisTopicID = $("#TopicPath_TopicID_Level"+jsLevel).val();  
			if(CommentID.Trim()=='')   // it is new record
			{
				
			}
			else if (jsFirstLoad==true)
			{
				$("select#Level"+jsLevel+"_TopicID option[value='"+thisTopicID+"']").attr("selected","selected");
				
				//alert($("select#Level"+jsLevel+"_TopicID option[value='"+thisTopicID+"']").text());
							
				js_Reload_Individual_Topic_Selection(jsNextLevel, thisTopicID, '');
			}
			
			if(jsNextLevel >jsMaxTopicLevel )
			{
				jsFirstLoad=false;  // it is not first load
			}
			
		}
	);
	
	if(jsPreviousLevelTopicID=='ALL')
	{
		$('div#Level'+jsLevel+'_SelectionDiv').html('');
	}

}


function set_jsID()
{
	jsCurSubjectID = $('select#SubjectID').val();
}

function getSelectVal(ParLevel)
{
	var Val = $('select#Level'+ParLevel+'_TopicID').val();			
	if($('select#Level'+ParLevel+'_TopicID').val()==null)
	{
		Val='';
	}
	return Val;
}

</script>
<?
		echo $linterface->FOCUS_ON_LOAD("form1.CommentCode");
        $linterface->LAYOUT_STOP();
		intranet_closedb();
?>