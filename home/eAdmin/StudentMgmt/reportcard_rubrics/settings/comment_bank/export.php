<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_CommentBank";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_comment.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$lreportcard_comment = new libreportcardrubrics_comment();
$lreportcard_topic = new libreportcardrubrics_topic();


$commentType = ($_GET['commentType'])? $_GET['commentType'] : $_POST['commentType'];
$commentType = ($commentType=='')? 'class' : $commentType;
$forImportSample = (isset($_GET['forImportSample']))? $_GET['forImportSample'] : $_POST['forImportSample'];
$SubjectID = (isset($_GET['SubjectID']))? $_GET['SubjectID'] : $_POST['SubjectID'];
$FilterTopicString = (isset($_GET['FilterTopicString']))? $_GET['FilterTopicString'] : $_POST['FilterTopicString'];
$Keyword = (isset($_GET['Keyword']))? $_GET['Keyword'] : $_POST['Keyword'];


# Import Header
$ImportHeader = $Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']; 
$ColHeader = array();
$ColHeader[] =	$ImportHeader['CommentCode']['En'];
$ColHeader[] =	$ImportHeader['CommentCh']['En'];
$ColHeader[] =	$ImportHeader['CommentEn']['En'];
if($commentType=='subject') {
	$ColHeader[] = $ImportHeader['SubjectCode']['En'];
	$ColHeader[] = $ImportHeader['SubjectName']['En'];
	
	for ($i=1; $i<=$lreportcard->MaxTopicLevel; $i++) {
		$ColHeader[] = $Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['TopicCode'][$i]['En'];
		$ColHeader[] = $Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['TopicName'][$i]['En'];
	}
}

$ColHeader2 = array();
$ColHeader2[] =	$ImportHeader['CommentCode']['Ch'];
$ColHeader2[] =	$ImportHeader['CommentCh']['Ch'];
$ColHeader2[] =	$ImportHeader['CommentEn']['Ch'];
if($commentType=='subject') {
	$ColHeader2[] = $ImportHeader['SubjectCode']['Ch'];
	$ColHeader2[] = $ImportHeader['SubjectName']['Ch'];
	
	for ($i=1; $i<=$lreportcard->MaxTopicLevel; $i++) {
		$ColHeader2[] = $Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['TopicCode'][$i]['Ch'];
		$ColHeader2[] = $Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['TopicName'][$i]['Ch'];
	}
}
$ColHeader2[] =	$Lang['General']['ImportArr']['SecondRowWarn'];

# chinese header
$rowCounter = 0;
$ExportArr[$rowCounter++] = $ColHeader2;

//$CommentArr = $lreportcard_comment->Get_Comment_Bank_Comment('',$SubjectID=0);
$sql = trim(urldecode(stripslashes($_POST['ExportSQL'])));
if ($commentType == 'class') {
	$CommentArr = $lreportcard_comment->returnArray($sql);
}
else if ($commentType == 'subject') {
	if ($forImportSample == 1) {
		$IncludeTopicIDArr = $lreportcard_topic->Get_TopicID_By_Filtering_Text($FilterTopicString, $SubjectID);
		$CommentArr = $lreportcard_comment->Get_Comment_Bank_Comment($Keyword, $IncludeTopicIDArr);
		$TopicTreeInfoArr = $lreportcard_topic->Get_TopicPathArr_by_LastTopicID(Get_Array_By_Key($CommentArr, 'TopicID'));
	}
	else {
		$CommentArr = $lreportcard_comment->returnArray($sql);
	}
}
$numOfComment = count($CommentArr);

 
//foreach($CommentArr as $thisComment)
//{
//	list($CommentCode, $CommenrEn, $CommentCh,$TopicID) = $thisComment;
//	
//	if($commentType=='subject')
//	{
//		$LevelTopicIDArr = $lreportcard_topic->Get_TopicPathArr_by_LastTopicID($TopicID ,$Par_basedOnLang=true);
//				
//		
//		$display_SubjectPath = implode(' > ',(array)$_pathArr);
//	
//		$ExportArr[$rowCounter][] = $CommentCode;
//		$ExportArr[$rowCounter][] = $CommentCh;
//		$ExportArr[$rowCounter][] = $CommenrEn;
//		$ExportArr[$rowCounter][] = $LevelTopicIDArr['Subject']['SubjectCode'];
//		$ExportArr[$rowCounter][] = $LevelTopicIDArr['Subject']['SubjectName'];
//		for ($i=1; $i<=$lreportcard->MaxTopicLevel; $i++) {
//			$ExportArr[$rowCounter][] = $LevelTopicIDArr['Level'][$i]['TopicCode'];
//			$ExportArr[$rowCounter][] = $LevelTopicIDArr['Level'][$i]['TopicName'];
//		}
//		$rowCounter++;
//	}
//	else if($commentType=='class')
//	{
//		$ExportArr[] = array($CommentCode, $CommentCh, $CommenrEn);
//	}
//	
//}
for ($i=0; $i<$numOfComment; $i++) {
	$thisCommentID = $CommentArr[$i]['CommentID'];
	$thisCommentCode = $CommentArr[$i]['CommentCode'];
	$thisCommentEn = $CommentArr[$i]['CommentEn'];
	$thisCommentCh = $CommentArr[$i]['CommentCh'];
	$thisTopicID = $CommentArr[$i]['TopicID'];
	
	if (is_array($IncludeCommentIDArray) && !in_array($thisCommentID, $IncludeCommentIDArray)) {
		// filter records according to the ui filtering
		continue;
	}
	
	$ExportArr[$rowCounter][] = $thisCommentCode;
	$ExportArr[$rowCounter][] = $thisCommentCh;
	$ExportArr[$rowCounter][] = $thisCommentEn;
	
	if($commentType=='subject') {
		$thisLevelTopicIDArr = $LevelTopicIDArr[$thisTopicID];
		//$display_SubjectPath = implode(' > ',(array)$_pathArr);
		
		$ExportArr[$rowCounter][] = $TopicTreeInfoArr[$thisTopicID]['Subject']['SubjectCode'];
		$ExportArr[$rowCounter][] = $TopicTreeInfoArr[$thisTopicID]['Subject']['SubjectName'];
		for ($j=1; $j<=$lreportcard->MaxTopicLevel; $j++) {
			$ExportArr[$rowCounter][] = $TopicTreeInfoArr[$thisTopicID]['Level'][$j]['TopicCode'];
			$ExportArr[$rowCounter][] = $TopicTreeInfoArr[$thisTopicID]['Level'][$j]['TopicName'];
		}
	}
	
	$rowCounter++;
}

$lexport = new libexporttext();
$export_content = $lexport->GET_EXPORT_TXT($ExportArr,$ColHeader,"", "\r\n", "", 0, "11");

// Output the file to user browser
if($commentType=='subject') {
	$filename = ($forImportSample == 1)? "import_subject_detatils_comment_sample.csv" : "subject_detatils_comment.csv";
}
else if($commentType=='class') {
	$filename = "class_teacher_comment.csv";
}

intranet_closedb();
$filename = str_replace(' ', '%20', $filename);
$lexport->EXPORT_FILE($filename, $export_content);

?>