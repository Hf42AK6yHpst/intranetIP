<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_CommentBank";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");

### Check Access Right
$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$linterface = new interface_html();
$linterface_ui = new libreportcardrubrics_ui();

# tag information
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

$TAGS_OBJ = array();
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ClassTeacherComment'], "index.php?CommentType=class", ($CommentType=="class" || !isset($CommentType))?1:0);

$linterface->LAYOUT_START();

echo $linterface_ui->Get_Settings_CommentBank_New_Edit_UI($CommentID); 
?>
<br/>
<script>
var CodeValid = false;
var WarnMsg = new Array();
WarnMsg["CommentCode"] = "<?=$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['jsWarningArr']['Empty']['CommentCode']?>";
WarnMsg["CommentEn"] = "<?=$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['jsWarningArr']['Empty']['CommentEn']?>";
WarnMsg["CommentCh"] = "<?=$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['jsWarningArr']['Empty']['CommentCh']?>";

function aj_CheckCode(val)
{
	var val = $("#CommentCode").val(); 
	if(val.Trim()=='')
	{
		$("#WarnCommentCode").html(WarnMsg["CommentCode"]);
		CodeValid = false;
		return false;
	}	
	
	if(!valid_code(val))
	{
		$("#WarnCommentCode").html("<?=$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['jsWarningArr']['WrongCodeFormat']?>");
		CodeValid = false;
		return false;
	}
		
	$.post(
		"ajax_validate.php",
		{
			Action:"ValidateCommentCode",
			CommentCode:val	
		},
		function(ReturnData)
		{
			if(ReturnData==0)
			{
				$("#WarnCommentCode").html("<?=$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['jsWarningArr']['CommentCodeNotAvailible']?>");
				CodeValid = false;
			}
			else
			{
				$("#WarnCommentCode").html("");		
				CodeValid = true;		
			}
		}
	);
	
}

function checkForm()
{
	var FormValid = CodeValid;
	
	$(".WarnRequired").html('');
	$(".required").each(function(){
		FormValid = (check_text_30(this, WarnMsg[$(this).attr("id")], "Warn"+$(this).attr("id"))&& FormValid);
	});
	
	return FormValid;
}

$().ready(function(){
	//aj_CheckCode();
})
</script>
<?
		echo $linterface->FOCUS_ON_LOAD("form1.CommentCode");
        $linterface->LAYOUT_STOP();
		intranet_closedb();
?>