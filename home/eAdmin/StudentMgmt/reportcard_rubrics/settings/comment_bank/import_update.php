<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_CommentBank";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_comment.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");

### Check Access Right
$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$commentType = $_POST['commentType'];

$linterface = new interface_html();
$lreportcard_comment = new libreportcardrubrics_comment();
$lreportcard_topic = new libreportcardrubrics_topic();
$lreportcard_ui = new libreportcardrubrics_ui();

$ImportData = unserialize(stripslashes($ImportData));

$ExistCommentList = $lreportcard_comment->Get_Comment_Bank_Comment('', 0, '', $returnSubjectComment='');
$CodeIDMap = BuildMultiKeyAssoc($ExistCommentList,"CommentCode","CommentID",1);

if($commentType=='subject') {
	$TopicInfoArr = $lreportcard_topic->Get_Topic_Info('', '', $lreportcard->MaxTopicLevel, '', '', '', $ReturnAsso=0);
	$TopicAssoArr = BuildMultiKeyAssoc($TopicInfoArr, 'TopicCode');
	unset($TopicInfoArr);
}


$lreportcard_comment->Start_Trans();
//foreach((array)$ImportData as $Row)
$numImportData = count($ImportData);
for ($i=0; $i<$numImportData; $i++)
{
	$thisColumnCount = 0;
	$thisCommentCode = trim($ImportData[$i][$thisColumnCount++]);
	$thisCommentCh = trim($ImportData[$i][$thisColumnCount++]);
	$thisCommentEn = trim($ImportData[$i][$thisColumnCount++]);
	$thisSubjectCode = trim($ImportData[$i][$thisColumnCount++]);
	$thisSubjectName = trim($ImportData[$i][$thisColumnCount++]);
	
	$thisTopicInfoArr = array();
	$thisLastTopicCodeIndex = 0;
	for ($j=1; $j<=$lreportcard->MaxTopicLevel; $j++) {
		$thisTopicInfoArr[$j]['TopicCode'] = trim($ImportData[$i][$thisColumnCount++]);
		$thisTopicInfoArr[$j]['TopicName'] = trim($ImportData[$i][$thisColumnCount++]);
	}
	
	$thisCommentID = $CodeIDMap[$thisCommentCode];
	if($thisCommentID) {
		$thisCommentData = array();
		$thisCommentData['CommentCh'] = $thisCommentCh;
		$thisCommentData['CommentEn'] = $thisCommentEn;
		$Success['Update'][] = $lreportcard_comment->Update_Comment_Bank_Comment($thisCommentID, $thisCommentData);
	}
	else {
		if($commentType=='subject') {
			$thisLastLevelTopicCode = $thisTopicInfoArr[$lreportcard->MaxTopicLevel]['TopicCode'];
			$thisTopicID = $TopicAssoArr[$thisLastLevelTopicCode]['TopicID'];
		}
		else {
			$thisTopicID = 0;
		}
		$Success['Insert'][] = $lreportcard_comment->Insert_Comment_Bank_Comment($thisCommentCode,$thisCommentEn,$thisCommentCh, $thisTopicID);
	}	
}


if(in_array(false,(array)$Success['Update']) || in_array(false,(array)$Success['Insert']) )
{
	$lreportcard_comment->RollBack_Trans();
	$SuccessMsg = $Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportCommentFail'];
}
else
{
	$lreportcard_comment->Commit_Trans();
	$NoOfImported = count($Success['Update']) + count($Success['Insert']);
	$SuccessMsg = $NoOfImported." ".$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportCommentSuccess'];
}

$targetFile = ($commentType=='subject')? 'index_subject.php' : 'index.php';  
$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='".$targetFile."' ", "back");

### Title ###
# tag information
$TAGS_OBJ = $lreportcard_ui->Get_Settings_CommentBank_Tab($commentType);

# page navigation (leave the array empty if no need)
if ($commentType == 'class') {
	$PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ClassTeacherComment'], "index.php");
}
else if ($commentType == 'subject') {
	$PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['SubjectDetailsComment'], "index_subject.php");
}
$PAGE_NAVIGATION[] = array($Lang['Btn']['Import'], "");

$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();  

?>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td colspan="2"><?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?><br><br></td>
	</tr>
	<tr>
		<td colspan="2"><?= $linterface->GET_IMPORT_STEPS(3) ?></td>
	</tr>
	<tr>
		<td class='tabletext' align='center'><br><?=$SuccessMsg?><br></td>
	</tr>
	<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<tr>
		<td colspan="2" align="center"><?=$BackBtn?></td>
	</tr>
</table>

<?
intranet_closedb();

$linterface->LAYOUT_STOP();
?>