<?php
// using ivan
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_comment.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");

$lreportcard_comment = new libreportcardrubrics_comment();
switch ($Action)
{
	case "ValidateCommentCode":
		$CommentCode = stripslashes($_REQUEST['CommentCode']);
		$CommentID = stripslashes($_REQUEST['CommentID']);
		echo $lreportcard_comment->Validate_Comment_Code($CommentCode,$CommentID)?1:0;
	break;
	
}
intranet_closedb();
?>