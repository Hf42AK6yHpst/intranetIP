<?php
// using :
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_ReportCardTemplate";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

# Get data
$ReportID = $_REQUEST['ReportID'];
$ReportTitleEn = trim(stripslashes($_REQUEST['ReportTitleEn']));
$ReportTitleCh = trim(stripslashes($_REQUEST['ReportTitleCh'])); 

$Success["UpdateTemplate"] = $lreportcard->UpdateReportTemplate($ReportID, $ReportTitleEn, $ReportTitleCh);

if(in_array(false,$Success))
{
	$msg = 'UpdateUnsuccess'; 
}
else
{
	$msg = 'UpdateSuccess'; 	
}

intranet_closedb();

header("location: reportcard_template.php?msg=$msg");
?>