<?php

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_ReportCardTemplate";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");


### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_settings_report_template_page_size", "numPerPage");
$arrCookies[] = array("ck_settings_report_template_page_number", "pageNo");
$arrCookies[] = array("ck_report_template_page_order", "order");
$arrCookies[] = array("ck_report_template_page_field", "field");	

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
	$ck_settings_report_template_page_size = '';
}
else 
	updateGetCookies($arrCookies);


### Check Access Right
$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

############## Interface Start ##############
$linterface = new interface_html();
$lreportcard_ui = new libreportcardrubrics_ui();

$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['ReportCardTemplate']);

#return msg
$ReturnMsg = $Lang['General']['ReturnMessage'][$msg];
$linterface->LAYOUT_START($ReturnMsg);

//echo $lreportcard_ui->Include_JS_CSS();
echo $lreportcard_ui->Get_Setting_Report_Template_UI($field,$order,$pageNo);

?>
<br><br>
<script>
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();

?>