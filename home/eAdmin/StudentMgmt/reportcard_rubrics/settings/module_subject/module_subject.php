<?php
// using: Ivan
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_ModuleSubject";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard_ui = new libreportcardrubrics_ui();
$linterface = new interface_html();

$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

############## Interface Start ##############

$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['SettingsArr']['ModuleSubjectArr']['MenuTitle']);
$linterface->LAYOUT_START();

echo $lreportcard_ui->Include_JS_CSS();
echo $lreportcard_ui->Get_Settings_ModuleSubject_UI();
?>

<script language="javascript">
var jsScreenWidth = parseInt(Get_Screen_Width());
var jsTableWidth = jsScreenWidth - 250;

$(document).ready( function() {	
	//js_Init_SuperTable();
});

function js_Select_All_Module_And_Subject(jsChecked)
{
	$('input.ModuleAllChk').attr('checked', jsChecked);
	$('input.SubjectAllChk').attr('checked', jsChecked);
	$('input.ModuleSubjectChk').attr('checked', jsChecked);
}

function js_Select_All_Subject_Of_Module(jsModuleID, jsChecked)
{
	$('input.ModuleSubjectChk_ModuleID_' + jsModuleID).attr('checked', jsChecked);
	
	if (jsChecked == false)
	{
		$('input#ApplyAllChk').attr('checked', false);
		$('input.SubjectAllChk').attr('checked', false);
	}
}

function js_Select_All_Module_Of_Subject(jsSubjectID, jsChecked)
{
	$('input.ModuleSubjectChk_SubjectID_' + jsSubjectID).attr('checked', jsChecked);
	
	if (jsChecked == false)
	{
		$('input#ApplyAllChk').attr('checked', false);
		$('input.ModuleAllChk').attr('checked', false);
	}
}

function js_Uncheck_Select_All(jsModuleID, jsSubjectID, jsChecked)
{
	if (jsChecked == false)
	{
		$('input#ApplyAllChk').attr('checked', false);
		$('input#ModuleAllChk_' + jsModuleID).attr('checked', false);
		$('input#SubjectAllChk_' + jsSubjectID).attr('checked', false);
	}
}

function js_Save_Module_Subject()
{
	var jsSubmitString = Get_Form_Values(document.getElementById('form1'));
	$.ajax({  
		type: "POST",  
		url: "ajax_update.php",
		data: jsSubmitString,  
		success: function(data) {
			
			if (data=='1')
			{
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>');
			}
			else
			{
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>');
			}
			
			Scroll_To_Top();
		} 
	});  
	return false;
}

function js_Init_SuperTable()
{
    $("table#ContentTable").toSuperTable({
    	width: jsTableWidth + "px", 
    	height: "500px", 
    	fixedCols: 2,
    	headerRows: 3,
    	onFinish: function(){ fixSuperTableHeight('ContentTable', 397); }
	});
}
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>