<?php
// using :
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
$lreportcard_topic = new libreportcardrubrics_topic();


# Get data
$Action = stripslashes($_REQUEST['Action']);

if ($Action == 'Topic_Code')
{
	$InputValue = stripslashes(trim($_REQUEST['InputValue']));
	$ExcludeTopicID = $_REQUEST['ExcludeTopicID'];
	
	$Valid = $lreportcard_topic->Is_Topic_Code_Valid($InputValue, $ExcludeTopicID);
	echo ($Valid)? '1' : '0';
}
else if ($Action == 'Import_Topic')
{
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	include_once($PATH_WRT_ROOT.'includes/libimporttext.php');
	include_once($PATH_WRT_ROOT.'includes/subject_class_mapping.php');
	include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
	
	$lexport = new libexporttext();
	$limport = new libimporttext();
	$libSubject = new subject();
	$libYear = new Year();
	
	$TargetFilePath = stripslashes($_REQUEST['TargetFilePath']);
	
	### Delete the temp data first
	$SuccessArr['DeleteOldTempData'] = $lreportcard_topic->Delete_Import_Temp_Data();
				
			
	### Get Subject Data
	$SubjectInfoArr = $libSubject->Get_Subject_List($returnAssociate=0, $withLearningCategory=0);
	$SubjectCodeAssoArr = BuildMultiKeyAssoc($SubjectInfoArr, 'CODEID');
		
	
	### Get Topic Data
	$TopicInfoArr = $lreportcard_topic->Get_Topic_Info($SubjectID='', $TopicID='', $Level='', $TopicCode='', $PreviousLevelTopicID='', $ParentTopicID='', $ReturnAsso=0);
	$TopicCodeAssoArr = BuildMultiKeyAssoc($TopicInfoArr, 'TopicCode');
	
	### Get Form Data
	$FormInfoArr = $libYear->Get_All_Year_List();
	$FormNameIDAssoArr = BuildMultiKeyAssoc($FormInfoArr, 'YearName', array('YearID'), $SingleValue=1, $BuildNumericArray=0); 
	$FormInfoAssoArr = BuildMultiKeyAssoc($FormInfoArr, 'YearID');
	
	
	### Get Csv data
	$DefaultCsvHeaderArr = $lreportcard_topic->Get_Topic_Import_Export_Column_Title_Arr();
	$ColumnPropertyArr = $lreportcard_topic->Get_Topic_Import_Export_Column_Property($forGetCsvContent=0);
	$DefaultCsvHeaderArr = $lexport->GET_EXPORT_HEADER_COLUMN($DefaultCsvHeaderArr, $ColumnPropertyArr);
	$ColumnPropertyArr = $lreportcard_topic->Get_Topic_Import_Export_Column_Property($forGetCsvContent=1);
		
	$CsvDataArr = $limport->GET_IMPORT_TXT_WITH_REFERENCE($TargetFilePath, "", "", $DefaultCsvHeaderArr, $ColumnPropertyArr);
	$CsvHeaderArr = array_shift($CsvDataArr);
	$numOfCsvData = count($CsvDataArr);
	
	
	### Include js libraries
	$jsInclude = '';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
	echo $jsInclude;
	
	
	### Variable Initialization
	$MaxTopicLevel = $lreportcard_topic->MaxTopicLevel;
	$NumOfSuccessRow = 0;
	$NumOfErrorRow = 0;
	$ErrorRowInfoArr = array();		//$ErrorRowInfoArr[$RowNumber][ErrorField] = array(ErrorMsg)
	$InsertValueArr = array();
	$RowNumber = 3;
	
	
	### Loop each csv data
	$NewTopicCodeArr = array();		// $NewTopicCodeArr[$thisTopicCode] = $thisPreviousLevelTopicCode;
	for ($i=0; $i<$numOfCsvData; $i++)
	{
		$thisColumnCounter = 0;
		
		$thisSubjectCode = $lreportcard_topic->Get_Safe_Sql_Query(trim($CsvDataArr[$i][$thisColumnCounter++]));
		$thisSubjectNameEn = $lreportcard_topic->Get_Safe_Sql_Query(trim($CsvDataArr[$i][$thisColumnCounter++]));
		$thisSubjectNameCh = $lreportcard_topic->Get_Safe_Sql_Query(trim($CsvDataArr[$i][$thisColumnCounter++]));
		$thisSubjectID = $SubjectCodeAssoArr[$thisSubjectCode]['RecordID'];
		
		
		# Warning if no such Subject
		if ($thisSubjectID == '')
		{
			$ErrorRowInfoArr[$RowNumber]['SubjectCode'] = 1;
			$ErrorRowInfoArr[$RowNumber]['SubjectNameEn'] = 1;
			$ErrorRowInfoArr[$RowNumber]['SubjectNameCh'] = 1;
			(array)$ErrorRowInfoArr[$RowNumber]['ErrorMsgArr'][] = '- '.$Lang['SysMgr']['SubjectClassMapping']['NoSubjectWarning'];
		}
		
		
		### Read Topic Data
		for ($thisLevel=1; $thisLevel<=$MaxTopicLevel; $thisLevel++)
		{
			${'thisTopicCode_'.$thisLevel} = $lreportcard_topic->Get_Safe_Sql_Query(trim($CsvDataArr[$i][$thisColumnCounter++]));
			${'thisTopicNameEn_'.$thisLevel} = $lreportcard_topic->Get_Safe_Sql_Query(trim($CsvDataArr[$i][$thisColumnCounter++]));
			${'thisTopicNameCh_'.$thisLevel} = $lreportcard_topic->Get_Safe_Sql_Query(trim($CsvDataArr[$i][$thisColumnCounter++]));
			
			${'thisRemarks_'.$thisLevel} = '';
			if ($thisLevel == 1)
				${'thisRemarks_'.$thisLevel} = $lreportcard_topic->Get_Safe_Sql_Query(trim($CsvDataArr[$i][$thisColumnCounter++]));
				
			
			### Map the TopicID if the TopiceCode exist
			$thisTopicInfoArr = $TopicCodeAssoArr[${'thisTopicCode_'.$thisLevel}];
			$thisTopicID = $thisTopicInfoArr['TopicID'];
			${'thisTopicID_'.$thisLevel} = $thisTopicID;
			
			# Warning if the Topic exist but not belongs to this Subject
			$thisOriginalTopicSubjectID = $thisTopicInfoArr['SubjectID'];
			if ($thisTopicID != '' && $thisSubjectID != '' && $thisOriginalTopicSubjectID != $thisSubjectID)
			{
				$thisTopicLevelTitle = $lreportcard_topic->Get_Topic_Level_Name($thisLevel);
				$thisWarning = $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportWarningArr']['TopicNotBelongsToThisSubject'];
				$thisWarning = str_replace('<!--TopicLevel-->', $thisTopicLevelTitle, $thisWarning);
				
				$ErrorRowInfoArr[$RowNumber]['TopicCode_'.$thisLevel] = 1;
				$ErrorRowInfoArr[$RowNumber]['TopicNameEn_'.$thisLevel] = 1;
				$ErrorRowInfoArr[$RowNumber]['TopicNameCh_'.$thisLevel] = 1;
				(array)$ErrorRowInfoArr[$RowNumber]['ErrorMsgArr'][] = '- '.$thisWarning;
			}
			
			# Warning if the Topic is not exist and is duplicated in other csv data
			// $thisTopicID == '' means the Topic is not exist in the system
			// isset($NewTopicCodeArr[${'thisTopicCode_'.$thisLevel}]) means the Topic Code has appeared in other csv data
			// $NewTopicCodeArr[${'thisTopicCode_'.$thisLevel}] != ${'thisTopicCode_'.($thisLevel - 1)} means the TopicCode is not linked to the same Topic in the previous Level
			if ($thisTopicID == '' && isset($NewTopicCodeArr[${'thisTopicCode_'.$thisLevel}]) && $NewTopicCodeArr[${'thisTopicCode_'.$thisLevel}] != ${'thisTopicCode_'.($thisLevel - 1)})
			{
				$thisTopicLevelTitle = $lreportcard_topic->Get_Topic_Level_Name($thisLevel);
				$thisWarning = $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportWarningArr']['TopicDuplicatedIfAddTopic'];
				$thisWarning = str_replace('<!--TopicLevel-->', $thisTopicLevelTitle, $thisWarning);
				
				$ErrorRowInfoArr[$RowNumber]['TopicCode_'.$thisLevel] = 1;
				$ErrorRowInfoArr[$RowNumber]['TopicNameEn_'.$thisLevel] = 1;
				$ErrorRowInfoArr[$RowNumber]['TopicNameCh_'.$thisLevel] = 1;
				(array)$ErrorRowInfoArr[$RowNumber]['ErrorMsgArr'][] = '- '.$thisWarning;
			}
			$NewTopicCodeArr[${'thisTopicCode_'.$thisLevel}] = ${'thisTopicCode_'.($thisLevel - 1)};
			
			# Warning if the Topic is not belongs to this Topic Level
			$thisOriginalTopicLevel = $thisTopicInfoArr['Level'];
			if ($thisTopicID != '' && $thisLevel != $thisOriginalTopicLevel)
			{
				$thisTopicLevelTitle = $lreportcard_topic->Get_Topic_Level_Name($thisLevel);
				$thisWarning = $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportWarningArr']['TopicNotBelongsToThisLevel'];
				$thisWarning = str_replace('<!--TopicCode-->', ${'thisTopicCode_'.$thisLevel}, $thisWarning);
				$thisWarning = str_replace('<!--TopicLevel-->', $thisTopicLevelTitle, $thisWarning);
				
				$ErrorRowInfoArr[$RowNumber]['TopicCode_'.$thisLevel] = 1;
				$ErrorRowInfoArr[$RowNumber]['TopicNameEn_'.$thisLevel] = 1;
				$ErrorRowInfoArr[$RowNumber]['TopicNameCh_'.$thisLevel] = 1;
				(array)$ErrorRowInfoArr[$RowNumber]['ErrorMsgArr'][] = '- '.$thisWarning;
			}
			
			# Warning if the Topic is not linked to the input previous Topic
			if ($thisLevel > 1)
			{
				$thisCsvPreviousLevelTopicID = ${'thisTopicID_'.($thisLevel-1)};
				$thisPreviousLevelTopicID = $thisTopicInfoArr['PreviousLevelTopicID'];
				
				if ($thisTopicID != '' && $thisPreviousLevelTopicID != '' && $thisCsvPreviousLevelTopicID != $thisPreviousLevelTopicID)
				{
					$thisTopicLevelTitle = $lreportcard_topic->Get_Topic_Level_Name($thisLevel);
					$thisPreviousTopicLevelTitle = $lreportcard_topic->Get_Topic_Level_Name($thisLevel - 1);
					
					$thisWarning = $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportWarningArr']['WrongLinkageOfExistingTopic'];
					$thisWarning = str_replace('<!--TopicLevel-->', $thisTopicLevelTitle, $thisWarning);
					$thisWarning = str_replace('<!--PreviousTopicLevel-->', $thisPreviousTopicLevelTitle, $thisWarning);
					
					$ErrorRowInfoArr[$RowNumber]['TopicCode_'.$thisLevel] = 1;
					$ErrorRowInfoArr[$RowNumber]['TopicNameEn_'.$thisLevel] = 1;
					$ErrorRowInfoArr[$RowNumber]['TopicNameCh_'.$thisLevel] = 1;
					(array)$ErrorRowInfoArr[$RowNumber]['ErrorMsgArr'][] = '- '.$thisWarning;
				}
			}
		}	// End Loop Topic Data
		
		# Warning if the Form is not existing
		$thisApplicableFormList = $lreportcard_topic->Get_Safe_Sql_Query(trim($CsvDataArr[$i][$thisColumnCounter++]));
		$thisApplicableFormNameArr = explode($Lang['General']['ImportContentSeparator'], $thisApplicableFormList);
		$thisNumOfApplicableForm = count($thisApplicableFormNameArr);
		
		if ($thisNumOfApplicableForm == 0)
		{
			$ErrorRowInfoArr[$RowNumber]['ApplicableForm'] = 1;
			(array)$ErrorRowInfoArr[$RowNumber]['ErrorMsgArr'][] = '- '.$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportWarningArr']['NoApplicableForm'];
		}
		
		$thisApplicableFormIDArr = array();
		for ($j=0; $j<$thisNumOfApplicableForm; $j++)
		{
			$thisYearName = $thisApplicableFormNameArr[$j];
			$thisYearID = $FormNameIDAssoArr[$thisYearName];
			
			# Warning if no such Form
			if ($thisYearID == '')
			{
				$ErrorRowInfoArr[$RowNumber]['ApplicableForm'] = 1;
				(array)$ErrorRowInfoArr[$RowNumber]['ErrorMsgArr'][] = '- '.$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportWarningArr']['NoSuchForm'];
			}
			else
			{
				# Warning if the Form is not linked to any stage
				$thisFormStageID = $FormInfoAssoArr[$thisYearID]['FormStageID'];
				if ($thisFormStageID == '')
				{
					$ErrorRowInfoArr[$RowNumber]['ApplicableForm'] = 1;
					(array)$ErrorRowInfoArr[$RowNumber]['ErrorMsgArr'][] = '- '.$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportWarningArr']['FormNotLinkedToAnyStage'];
				}
				else
				{
					$thisApplicableFormIDArr[] = $thisYearID;
				}
			}
		}
		$thisApplicableFormIDList = implode(',', $thisApplicableFormIDArr);
		$thisApplicableFormNameList = $lreportcard_topic->Get_Safe_Sql_Query($thisApplicableFormList);
		
		### Prepare Insert Value to insert the temp data into the temp table
		$thisInsertSql = '';
		$thisInsertSql .= " 	(
									'".$_SESSION['UserID']."', '".$RowNumber."', '".$thisSubjectID."', '".$thisSubjectCode."', '".$thisSubjectNameEn."' , '".$thisSubjectNameCh."', ";
									for ($thisLevel=1; $thisLevel<=$MaxTopicLevel; $thisLevel++)
									{
										$thisInsertSql .= " '".${'thisTopicID_'.$thisLevel}."', '".${'thisTopicCode_'.$thisLevel}."', '".${'thisTopicNameEn_'.$thisLevel}."', '".${'thisTopicNameCh_'.$thisLevel}."', "; 
										
										if ($thisLevel == 1)
											$thisInsertSql .= " '".${'thisRemarks_'.$thisLevel}."', ";
									}
		$thisInsertSql .= "			'".$thisApplicableFormIDList."', '".$thisApplicableFormNameList."', now()
								)";
		$InsertValueArr[] = $thisInsertSql;
		
		
		### Count success / failed records	
		if(empty($ErrorRowInfoArr[$RowNumber]))
			$NumOfSuccessRow++;
		else
			$NumOfErrorRow++;
			
			
		### Update Processing Display
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.document.getElementById("SuccessCountDiv").innerHTML = "'.$NumOfSuccessRow.'";';
			$thisJSUpdate .= 'window.parent.document.getElementById("FailCountDiv").innerHTML = "'.$NumOfErrorRow.'";';
			$thisJSUpdate .= 'window.parent.document.getElementById("BlockUI_Processed_Number_Span").innerHTML = "'.($RowNumber - 2).'";';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
		
		
		$RowNumber++;
	}
	
	
	### Insert the temp records into the temp table
	if (count($InsertValueArr) > 0)
	{
		
		$InsertList = implode(',', $InsertValueArr);
		
		$TEMP_IMPORT_TOPIC = $lreportcard_topic->Get_Table_Name('TEMP_IMPORT_TOPIC');
		$sql = '';
		$sql .= "Insert Into $TEMP_IMPORT_TOPIC
					(
						ImportUserID, RowNumber, SubjectID, SubjectCode, SubjectNameEn, SubjectNameCh,
				";
						for ($thisLevel=1; $thisLevel<=$MaxTopicLevel; $thisLevel++)
						{
							$sql .= " TopicID_$thisLevel, TopicCode_$thisLevel, TopicNameEn_$thisLevel, TopicNameCh_$thisLevel, "; 
							
							if ($thisLevel == 1)
								$sql .= " TopicRemarks_$thisLevel, ";
						}
		$sql .= "		ApplicableFormIDList, ApplicableFormNameList, DateInput
					)
				Values
					$InsertList
				";
	}
	$SuccessArr['InsertTempRecords'] = $lreportcard_topic->db_db_query($sql);
	
	
	### Display Error Result Table if there are any
	$numOfErrorRow = count($ErrorRowInfoArr);
	if($numOfErrorRow > 0)
	{
		### Update Processing Display
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.document.getElementById("BlockUI_Span").innerHTML = "'.$Lang['SysMgr']['SubjectClassMapping']['GeneratingErrorsInfo'].'...";';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
		
		
		### Get data of the error rows
		$ErrorRowArr = array_keys($ErrorRowInfoArr);
		$ErrorRowList = implode(',', $ErrorRowArr);
		$sql = "Select * From $TEMP_IMPORT_TOPIC Where RowNumber in ($ErrorRowList) And ImportUserID = '".$_SESSION['UserID']."'";
		$ErrorRowDataArr = $lreportcard_topic->returnArray($sql);
			
			
		### Build Error Info Table
		$CsvHeaderArr = Get_Lang_Selection($DefaultCsvHeaderArr[1], $DefaultCsvHeaderArr[0]);
		$numOfCsvHeader = count($CsvHeaderArr);
		$x .= '<table width="100%" border="0" cellpadding="3" cellspacing="0">';
			$x .= '<tr>';
				$x .= '<td class="tablebluetop tabletopnolink" width="10">'.$Lang['General']['ImportArr']['Row'].'</td>';
				
				for ($i=0; $i<$numOfCsvHeader; $i++)
				{
					$thisDisplay = $CsvHeaderArr[$i];
					
					$x .= '<td class="tablebluetop tabletopnolink">'.$CsvHeaderArr[$i].'</td>';
				}
				
				$x .= '<td class="tablebluetop tabletopnolink">'.$Lang['General']['Remark'].'</td>';
			$x .= '</tr>';
			
			$i=0;
			for ($i=0; $i<$numOfErrorRow; $i++)
			{
				$thisRowNumber = $ErrorRowDataArr[$i]['RowNumber'];
				$thisSubjectCode = $ErrorRowDataArr[$i]['SubjectCode'];
				$thisSubjectNameEn = $ErrorRowDataArr[$i]['SubjectNameEn'];
				$thisSubjectNameCh = $ErrorRowDataArr[$i]['SubjectNameCh'];
				
				$thisErrorInfoArr = $ErrorRowInfoArr[$thisRowNumber];
				$thisSubjectCode = ($thisErrorInfoArr['SubjectCode']==1)? '<font color="red">'.$thisSubjectCode.'</font>' : $thisSubjectCode;
				$thisSubjectNameEn = ($thisErrorInfoArr['SubjectNameEn']==1)? '<font color="red">'.$thisSubjectNameEn.'</font>' : $thisSubjectNameEn;
				$thisSubjectNameCh = ($thisErrorInfoArr['SubjectNameCh']==1)? '<font color="red">'.$thisSubjectNameCh.'</font>' : $thisSubjectNameCh;
				
				$thisApplicableFormNameList = $ErrorRowDataArr[$i]['ApplicableFormNameList'];
				$thisApplicableFormNameList = ($thisApplicableFormNameList == '')? '<font color="red">***</font>' : $thisApplicableFormNameList;
				$thisApplicableFormNameList = ($thisErrorInfoArr['ApplicableForm']==1)? '<font color="red">'.$thisApplicableFormNameList.'</font>' : $thisApplicableFormNameList;
								
				$css_i = ($i % 2) ? "2" : "";
				$x .= '<tr style="vertical-align:top">';
				
					### Subject Info
					$x .= '<td class="tablebluerow'.$css_i.'">'.$thisRowNumber.'</td>';
					$x .= '<td class="tablebluerow'.$css_i.'">'.$thisSubjectCode.'</td>';
					$x .= '<td class="tablebluerow'.$css_i.'">'.$thisSubjectNameEn.'</td>';
					$x .= '<td class="tablebluerow'.$css_i.'">'.$thisSubjectNameCh.'</td>';
				
					### Topics Info
					for ($thisLevel=1; $thisLevel<=$MaxTopicLevel; $thisLevel++)
					{
						$thisTopicCode = $ErrorRowDataArr[$i]['TopicCode_'.$thisLevel];
						$thisTopicNameEn = $ErrorRowDataArr[$i]['TopicNameEn_'.$thisLevel];
						$thisTopicNameCh = $ErrorRowDataArr[$i]['TopicNameCh_'.$thisLevel];
						
						$thisTopicCode = ($thisErrorInfoArr['TopicCode_'.$thisLevel]==1)? '<font color="red">'.$thisTopicCode.'</font>' : $thisTopicCode;
						$thisTopicNameEn = ($thisErrorInfoArr['TopicNameEn_'.$thisLevel]==1)? '<font color="red">'.$thisTopicNameEn.'</font>' : $thisTopicNameEn;
						$thisTopicNameCh = ($thisErrorInfoArr['TopicNameCh_'.$thisLevel]==1)? '<font color="red">'.$thisTopicNameCh.'</font>' : $thisTopicNameCh;
						
						$x .= '<td class="tablebluerow'.$css_i.'">'.$thisTopicCode.'</td>';
						$x .= '<td class="tablebluerow'.$css_i.'">'.$thisTopicNameEn.'</td>';
						$x .= '<td class="tablebluerow'.$css_i.'">'.$thisTopicNameCh.'</td>';
						
						### Display Topic Remarks for Level 1 only
						if ($thisLevel == 1)
						{
							$thisTopicRemarks = $ErrorRowDataArr[$i]['TopicRemarks_'.$thisLevel];
							$thisTopicRemarks = ($thisErrorInfoArr['TopicRemarks_'.$thisLevel]==1)? '<font color="red">'.$thisTopicRemarks.'</font>' : $thisTopicRemarks;
							
							$x .= '<td class="tablebluerow'.$css_i.'">'.$thisTopicRemarks.'</td>';
						}
					}
					
					### Applicable Form
					$x .= '<td class="tablebluerow'.$css_i.'">'.$thisApplicableFormNameList.'</td>';
					
					### Error Remarks
					$thisErrorDisplayArr = $thisErrorInfoArr['ErrorMsgArr'];
					$thisErrorDisplayText = implode('<br />', (array)$thisErrorDisplayArr);
					$x .= '<td class="tablebluerow'.$css_i.'">'.$thisErrorDisplayText.'</td>';
					
				$x .= '</tr>';
			}
			
		$x .= "</table>";
	}
	
	
	$ErrorCountDisplay = ($NumOfErrorRow > 0) ? "<font color=\"red\">".$NumOfErrorRow."</font>" : $NumOfErrorRow;
	
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= 'window.parent.document.getElementById("ErrorTableDiv").innerHTML = \''.$x.'\';';
		$thisJSUpdate .= 'window.parent.document.getElementById("SuccessCountDiv").innerHTML = \''.$NumOfSuccessRow.'\';';
		$thisJSUpdate .= 'window.parent.document.getElementById("FailCountDiv").innerHTML = \''.$ErrorCountDisplay.'\';';
		
		if ($NumOfErrorRow == 0) 
		{
			$thisJSUpdate .= '$("input#ContinueBtn", window.parent.document).removeClass("formbutton_disable").addClass("formbutton").attr("disabled", "");';
		}
		
		$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
}

intranet_closedb();
?>