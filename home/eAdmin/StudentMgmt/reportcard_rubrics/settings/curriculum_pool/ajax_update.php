<?php
// using :
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_CurriculumPool";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
$lreportcard_topic = new libreportcardrubrics_topic();
$lreportcard_topic->Has_Access_Right($PageRight, $CurrentPage);

# Get data
$Action = stripslashes($_REQUEST['Action']);
if ($Action == 'Add_Topic')
{
	$DataArr = array();
	$DataArr['SubjectID'] = $_REQUEST['SubjectID'];
	$DataArr['Level'] = $_REQUEST['Level'];
	$DataArr['TopicCode'] = stripslashes(trim($_REQUEST['Code']));
	$DataArr['TopicNameEn'] = stripslashes(trim($_REQUEST['NameEn']));
	$DataArr['TopicNameCh'] = stripslashes(trim($_REQUEST['NameCh']));
	$DataArr['Remarks'] = stripslashes(trim($_REQUEST['Remarks']));
	$DataArr['ParentTopicID'] = $_REQUEST['ParentTopicID'];
	$DataArr['PreviousLevelTopicID'] = $_REQUEST['PreviousLevelTopicID'];
	$DataArr['RecordStatus'] = 1;
	$DataArr['DisplayOrder'] = $lreportcard_topic->Get_Topic_Max_Display_Order($DataArr['SubjectID'], $DataArr['Level']) + 1;
	
	$YearID = $_REQUEST['YearID'];
	$YearID = ($YearID == -1)? '' : $YearID;
	
	$InsertedTopicID = $lreportcard_topic->Add_Topic($DataArr, $YearID);
	echo ($InsertedTopicID > 0)? '1' : '0';
}
else if ($Action == 'Reorder_Topic')
{
	$DisplayOrderString = $_REQUEST['DisplayOrderString'];
	$DisplayOrderTopicIDArr = explode(',', $DisplayOrderString);
	$numOfTopic = count($DisplayOrderTopicIDArr);
	$DisplayOrderCount = 1;
	
	$SuccessArr = array();
	for ($i=0; $i<$numOfTopic; $i++)
	{
		$thisTopicID = $DisplayOrderTopicIDArr[$i];
		
		if ($thisTopicID == '' || $thisTopicID == 0)
			continue;
			
		$DataArr = array();
		$DataArr['DisplayOrder'] = $DisplayOrderCount++;
		$SuccessArr[$thisTopicID] = $lreportcard_topic->Update_Topic($thisTopicID, $DataArr, $UpdateLastModified=0);
	}
	
	if (in_array(false, $SuccessArr))
		echo '0';
	else
		echo '1';
}
else if ($Action == 'Update_Topic_Field')
{
	$TopicID = $_REQUEST['TopicID'];
	$DBNameField = $_REQUEST['DBNameField'];
	$Value = stripslashes(trim($_REQUEST['Value']));
	
	$DataArr = array();
	$DataArr[$DBNameField] = $Value;
	
	$Success = $lreportcard_topic->Update_Topic($TopicID, $DataArr);
	echo ($Success)? '1' : '0';
}
else if ($Action == 'Delete_Topic')
{
	$TopicID = $_REQUEST['TopicID'];
	$Success = $lreportcard_topic->Delete_Topic($TopicID, $DeleteRelated=1);
	echo ($Success)? '1' : '0';
}
else if ($Action == 'Import_Topic')
{
	$lreportcard_topic->Start_Trans();
	$MaxTopicLevel = $lreportcard_topic->MaxTopicLevel;
	
	$jsInclude = '';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
	echo $jsInclude;
	
	
	### Get import temp data
	$TEMP_IMPORT_TOPIC = $lreportcard_topic->Get_Table_Name('TEMP_IMPORT_TOPIC');
	$sql = "Select * From $TEMP_IMPORT_TOPIC Where ImportUserID = '".$_SESSION['UserID']."'";
	$ImportDataArr = $lreportcard_topic->returnArray($sql);
	$numOfImportData = count($ImportDataArr);
	
	$SuccessArr = array();
	$ProcessedCodeIDAssoArr = array();
	for ($i=0; $i<$numOfImportData; $i++)
	{
		$thisSubjectID = $ImportDataArr[$i]['SubjectID'];
		$thisApplicableFormIDList = $ImportDataArr[$i]['ApplicableFormIDList'];
		
		### Get Import Topic Info
		$thisParentTopicID = 0;
		$thisPreviousTopicID = 0;
		for ($thisLevel=1; $thisLevel<=$MaxTopicLevel; $thisLevel++)
		{
			$thisTopicID = $ImportDataArr[$i]['TopicID_'.$thisLevel];
			$thisTopicCode = $ImportDataArr[$i]['TopicCode_'.$thisLevel];
			$thisTopicNameEn = $ImportDataArr[$i]['TopicNameEn_'.$thisLevel];
			$thisTopicNameCh = $ImportDataArr[$i]['TopicNameCh_'.$thisLevel];
			$thisRemarks = $ImportDataArr[$i]['TopicRemarks_'.$thisLevel];
			
			# For newly added Topic, find the TopicID in the Processed Topic Array instead of the Temp DB as the Temp DB does not has the TopicID data
			if ($thisTopicID == 0)
				$thisTopicID = $ProcessedCodeIDAssoArr[$thisTopicCode];
			
			if ($thisLevel == 1 && $thisTopicID > 0)
			{
				$thisParentTopicID = $thisTopicID;
				$thisPreviousLevelTopicID = $thisTopicID;
			}			
			
			### Skip Process Topic in different rows
			if (!isset($ProcessedCodeIDAssoArr[$thisTopicCode]))
			{
				if ($thisTopicID == 0)
				{
					# Insert New Topic
					$DataArr = array();
					$DataArr['SubjectID'] = $thisSubjectID;
					$DataArr['Level'] = $thisLevel;
					$DataArr['TopicCode'] = $thisTopicCode;
					$DataArr['TopicNameEn'] = $thisTopicNameEn;
					$DataArr['TopicNameCh'] = $thisTopicNameCh;
					$DataArr['ParentTopicID'] = $thisParentTopicID;
					$DataArr['PreviousLevelTopicID'] = $thisPreviousLevelTopicID;
					$DataArr['RecordStatus'] = 1;
					$DataArr['DisplayOrder'] = $lreportcard_topic->Get_Topic_Max_Display_Order($thisSubjectID, $thisLevel) + 1;
					
					if ($thisLevel == 1)
						$DataArr['Remarks'] = $thisRemarks;
					
					$thisTopicID = $lreportcard_topic->Add_Topic($DataArr);
					$SuccessArr['AddTopic'][$thisTopicID] = ($thisTopicID==0)? false : true;
				}
				else
				{
					# Edit Topic Info
					$DataArr = array();
					$DataArr['TopicNameEn'] = $thisTopicNameEn;
					$DataArr['TopicNameCh'] = $thisTopicNameCh;
					if ($thisLevel == 1)
						$DataArr['Remarks'] = $thisRemarks;
					
					$SuccessArr['UpdateTopic'][$thisTopicID] = $lreportcard_topic->Update_Topic($thisTopicID, $DataArr);
				}
				
				### Update Applicable Form
				if ($thisLevel == $MaxTopicLevel)
				{
					$thisApplicableFormIDArr = explode(',', $thisApplicableFormIDList);
					$SuccessArr['UpdateTopicFormMapping'][$thisTopicID] = $lreportcard_topic->Save_Topic_Form_Mapping($thisTopicID, $thisApplicableFormIDArr);
				}
			}
			
			# Record the mapping to skip further updating of the Topic
			$ProcessedCodeIDAssoArr[$thisTopicCode] = $thisTopicID;
			
			# Record the TopicID for the children Topic use
			if ($thisLevel == 1)
				$thisParentTopicID = $thisTopicID;
			$thisPreviousLevelTopicID = $thisTopicID;
		}
		
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.document.getElementById("BlockUI_Processed_Number_Span").innerHTML = "'.($i + 1).'";';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
	}
	
	
	### Delete the temp data first
	$SuccessArr['DeleteTempData'] = $lreportcard_topic->Delete_Import_Temp_Data();
	
	### Roll Back data if import failed
	if (in_multi_array(false, $SuccessArr))
	{
		$lreportcard_topic->RollBack_Trans();
		$ImportStatus = $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportCurriculumFailed'];
	}
	else
	{
		$lreportcard_topic->Commit_Trans();
		$ImportStatus = $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['ImportCurriculumSuccess'];
	}
	
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= 'window.parent.document.getElementById("ImportStatusSpan").innerHTML = "'.$ImportStatus.'";';
		$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
}
else if ($Action == 'Save_Topic_Form_Mapping')
{
	$TopicID = stripslashes(trim($_REQUEST['TopicID']));
	$YearIDList = stripslashes(trim($_REQUEST['YearIDList']));
	$YearIDArr = explode(',', $YearIDList);
	
	$Success = $lreportcard_topic->Save_Topic_Form_Mapping($TopicID, $YearIDArr);
	echo ($Success)? 1 : 0;
}

intranet_closedb();
?>