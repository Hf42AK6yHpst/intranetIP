<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_CurriculumPool";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

$lexport = new libexporttext();
$libSubject = new subject();
$lreportcard = new libreportcardrubrics_custom();
$lreportcard_topic = new libreportcardrubrics_topic();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);


$SubjectID = $_POST['SubjectID'];
if ($SubjectID == '' || $ExportAllSubject)
{
	$SubjectInfoArr = $libSubject->Get_Subject_List();
	$SubjectIDArr = Get_Array_By_Key($SubjectInfoArr, 'RecordID');
}
else if (!is_array($SubjectID) && $SubjectID != '')
{
	$SubjectIDArr = array($SubjectID);
}
else if (is_array($SubjectID))
{
	$SubjectIDArr = $SubjectID;
}


$FilterTopicInfoText = $_POST['FilterTopicInfoText'];	// $FilterTopicInfoText = 'Level,TopicID::Level,TopicID::'
$FilterLevelTopicArr = $lreportcard_topic->Get_Topic_FilterArr_From_FilterText($FilterTopicInfoText);


$ForImportSample = $_POST['ForImportSample'];
$ExportAllSubject = $_POST['ExportAllSubject'];



### File name
if ($ForImportSample == 1)
{
	$filename = "import_topic_info_sample.csv";
}
else
{
	if ($ExportAllSubject == 0)
	{
		$SubjectObj = new subject($SubjectID);
		$SubjectName = $SubjectObj->Get_Subject_Desc();
		$SubjectName = str_replace(' ', '_', $SubjectName);
		$filename = "topic_info_".$SubjectName.".csv";
	}
	else if ($ExportAllSubject == 1)
	{
		$filename = "topic_info.csv";
	}
	
}


### Column Title (2 dimensions array, 1st row is english, 2nd row is chinese)
$ColumnPropertyArr = $lreportcard_topic->Get_Topic_Import_Export_Column_Property();
$ColumnTitleArr = $lreportcard_topic->Get_Topic_Import_Export_Column_Title_Arr();
$ExportColumnTitleArr = $lexport->GET_EXPORT_HEADER_COLUMN($ColumnTitleArr, $ColumnPropertyArr);


### Csv Data
$SubjectInfoArr = $libSubject->Get_Subject_List($returnAssociate=1, $withLearningCategory=0);
$TopicInfoArr = $lreportcard_topic->Get_Topic_Info($SubjectIDArr);
$TopicTreeArr = $lreportcard_topic->Get_Topic_Tree($SubjectIDArr);
$IncludeNoTopicSubject = ($ForImportSample)? true : false;
$ExportDataArr = $lreportcard_topic->Get_Export_Content_Arr($SubjectIDArr, $SubjectInfoArr, $TopicTreeArr, $TopicInfoArr, $FilterLevelTopicArr, $IncludeNoTopicSubject);


$export_content = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportDataArr, $ExportColumnTitleArr);

intranet_closedb();

# Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);
?>