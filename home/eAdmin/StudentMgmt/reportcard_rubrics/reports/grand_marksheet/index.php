<?php
// using:
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Reports_GrandMarksheet";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$lreportcard_ui = new libreportcardrubrics_ui();
$linterface = new interface_html();


############## Interface Start ##############
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['MenuTitle']);
$linterface->LAYOUT_START();

echo $lreportcard_ui->Include_JS_CSS();
echo $lreportcard_ui->Get_Reports_Grand_Marksheet_Index_UI();

?>
<script>
function CheckAll(checked,ClassName)
{
	$("input."+ClassName).attr("checked",checked);
}

function UnCheck(checked,CheckAllID)
{
	if(!checked)
		$("input#"+CheckAllID).attr("checked","");
}

function showOption(table_id)
{
	$("#spanShowOption"+table_id).hide();
	$("#spanHideOption"+table_id).show();
	$("#"+table_id).show();
}

function hideOption(table_id)
{
	$("#spanShowOption"+table_id).show();
	$("#spanHideOption"+table_id).hide();
	$("#"+table_id).hide();
	
}

function jsSelectAllCheckbox(checked, className)
{
	$("."+className).attr("checked",checked);
}

function jsCheckUnSelectAll(checked, className)
{
	if(!checked)
		$("#"+className).attr("checked",checked);
}

function ToggleDetailFilter()
{
	if($(".TopicDetailDisplay:checked").val()==1)
	{
		js_Changed_Subject_Selection()
	}
	else
	{
		$('#TopicSelectionTD').html("").hide();
	}
}

function js_Changed_Form_Selection(jsYearID)
{
	js_Reload_Class_Selection();
}

function js_Changed_Subject_Selection()
{
	var SubjectIDArrValue = $("select#SubjectIDArr\\[\\]").val();
	if(SubjectIDArrValue == null)
		var SubjectList = '';
	else
		var SubjectList = $("select#SubjectIDArr\\[\\]").val().toString();
	
	$('td#TopicSelectionTD').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{
			Action: 'Load_Learning_Detail_Option',
			SubjectIDArr : SubjectList
		},
		function(ReturnData)
		{
			
		}
	).show();
	
}

function js_Reload_Class_Selection()
{
	var jsYearID = $('select#YearID').val();
	
	$('span#ClassSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{
			Action: 'Class_Selection',
			YearID: jsYearID,
			SelectionID: 'YearClassIDArr[]',
			OnChange: 'ToggleDetailFilter();',
			IsMultiple: 1,
			NoFirst: 1
		},
		function(ReturnData)
		{
			js_Select_All('YearClassIDArr[]', 1);
		}
	);
}

function js_Select_All_Year_Class(jsObjectID)
{
	js_Select_All(jsObjectID);
}

function js_Check_Form()
{
	var jsValid = true;
	
	// Check selected Subject
	if ($('select#SubjectIDArr\\[\\]').val() == null)
	{
		$('div#SelectSubjectWarningDiv').show();
		$('select#SubjectIDArr\\[\\]').focus();
		jsValid = false;
	}
	else
	{
		$('div#SelectSubjectWarningDiv').hide();
	}
	
	// Check selected Student Info
	if ($('input.StudentDataDisplayChk:checked').length==0)
	{
		$('div#SelectStudentInfoWarningDiv').show();
		$('input.StudentDataDisplayChk').focus();
		jsValid = false;
	}
	else
	{
		$('div#SelectStudentInfoWarningDiv').hide();
	}
	
	// Check selected Class
	if ($('select#YearClassIDArr\\[\\]').val() == null)
	{
		$('div#SelectClassWarningDiv').show();
		$('select#YearClassIDArr\\[\\]').focus();
		jsValid = false;
	}
	else
	{
		$('div#SelectClassWarningDiv').hide();
	}
	
	// Check selected Module
	if ($('select#ModuleModuleID').val() == null)
	{
		$('div#ModuleIDWarningDiv').show();
		$('select#ModuleModuleID').focus();
		jsValid = false;
	}
	else
	{
		$('div#ModuleIDWarningDiv').hide();
	}	

	// Check selected Topic Detail
	if (!$("input#TopicDisplay_All").attr("checked") && $('input.TopicDetailOption:checked').length==0)
	{
		$('div#SelectLearningDetailDiv').show();
		$('table#SelectLearningDetail').focus();
		jsValid = false;
	}
	else
	{
		$('div#SelectLearningDetailDiv').hide();
	}		
	
	if (jsValid == true)
		document.getElementById('form1').submit();
}

$().ready(function (){
	js_Changed_Form_Selection();
	js_Select_All_With_OptGroup("SubjectIDArr[]", 1);
	ToggleDetailFilter();
	js_Select_All('ModuleModuleID',1);
	initSortable();
});

function initSortable()
{
	$("ul.sortable").sortable({
		placeholder: 'placeholder'
	});
	$("ul.sortable").disableSelection();
}
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>