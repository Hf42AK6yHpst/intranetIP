<?php
// using: Ivan
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Reports_ClassAverageRubricsCount";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_rubrics.php");

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");

$lreportcard = new libreportcardrubrics();
$lreportcard_rubrics = new libreportcardrubrics_rubrics();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$lreportcard_ui = new libreportcardrubrics_ui();

include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/print_header.php");
echo $lreportcard_ui->Include_JS_CSS(array('rubrics_css'));

### Print Button
$PrintBtn = $lreportcard_ui->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();", "submit2");

$TargetLevel = 1;
$Statistic = $lreportcard->Get_Topic_Mark_Statistics($StudentIDArr,$ModuleID,$SubjectIDArr, $TargetLevel);

$SubjectInfo = $lreportcard->Get_Subject_Info();
switch($SubjectDisplay)
{
	case "Abbr": $subfix = "ABBR"; break;
	case "Desc": $subfix = "DES"; break;
	case "ShortForm": $subfix = "SNAME"; break;
}	
$SubjectTitle = Get_Lang_Selection("CH_","EN_").$subfix;
$SubjectTitleArr = BuildMultiKeyAssoc($SubjectInfo,"SubjectID",$SubjectTitle,1);

$StudentInfoArr = $lreportcard->Get_Student_Class_Info($StudentIDArr);
$ClassTitleLang = Get_Lang_Selection("ClassTitleB5","ClassTitleEN");
foreach($StudentInfoArr as $StudentInfo)
{
	$StudentInfoArr[$StudentInfo["UserID"]] =  $StudentInfo;
	$ClassStudentCountArr[$StudentInfo[$ClassTitleLang]]++;
}

$SubjectRubricsArr = $lreportcard_rubrics->Get_Subject_RubricsItem_Mapping();
$SubjectRubricsArr = $SubjectRubricsArr[0]; 

foreach($SubjectRubricsArr as $thisSubjectID => $RubricsItem)
{
	sortByColumn2($RubricsItem,"Level",1,1);
	$MaxLevelItem =  array_shift($RubricsItem);
	$MaxLevelItemArr[$thisSubjectID] = $MaxLevelItem;
}

foreach((array)$StudentIDArr as $StudentID) // loop all selected student
{
	
	$thisClassTitle = $StudentInfoArr[$StudentID][$ClassTitleLang];
	$SubjectTopicArr = $Statistic[$StudentID];
	
	foreach((array)$SubjectTitleArr as $thisSubjectID => $thisSubjectTitle) // loop 
	{
		if(!in_array($thisSubjectID, $SubjectIDArr))
			continue;
		
		$TopicInfoArr = $SubjectTopicArr[$thisSubjectID];
		$thisSubjectMaxLevelItem = $MaxLevelItemArr[$thisSubjectID];
		$SubjectMaxLevel[$thisSubjectTitle] = $thisSubjectMaxLevelItem['Level'];

		if(!isset($SubjectRubricsCount[$thisSubjectTitle][$thisClassTitle]))
			$SubjectRubricsCount[$thisSubjectTitle][$thisClassTitle] = 0;
		
		foreach((array)$TopicInfoArr as $thisTopicID => $TopicInfo)
		{
			foreach((array)$TopicInfo['MarkInfoArr'] as $RubricsItemID => $RubricsCount)
			{
				if($RubricsItemID == $thisSubjectMaxLevelItem['RubricsItemID'])
				{
					$SubjectRubricsCount[$thisSubjectTitle][$thisClassTitle] += $RubricsCount['Count'];
				}
			}
		}
	}
}

$ClassRubricsCountReport = $lreportcard_ui->Get_Reports_Class_Rubrics_Count_Report($SubjectRubricsCount, $SubjectMaxLevel,$ClassStudentCountArr);

?>
<table border="0" cellpadding="4" width="95%" cellspacing="0" class="print_hide">
	<tr><td align="right"><span><?=$PrintBtn?></span></td></tr>
</table>
<table border="0" cellpadding="4" width="95%" cellspacing="0">
	<tr><td><?=$ClassRubricsCountReport?></td></tr>
</table>
<table border="0" cellpadding="4" width="95%" cellspacing="0" class="print_hide">
	<tr><td align="right"><span><?=$PrintBtn?></span></td></tr>
</table>

<?
intranet_closedb();
?>