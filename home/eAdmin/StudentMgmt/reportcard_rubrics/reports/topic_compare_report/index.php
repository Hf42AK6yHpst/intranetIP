<?php
// using:
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Reports_TopicCompareReport";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard_ui = new libreportcardrubrics_ui();
$linterface = new interface_html();

$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

############## Interface Start ##############
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['ReportsArr']['TopicCompareReportArr']['MenuTitle']);
$linterface->LAYOUT_START();

echo $lreportcard_ui->Include_JS_CSS();
echo $lreportcard_ui->Get_Topic_Comparison_Report_Setting_UI();

?>
<script>
function js_Changed_Form_Stage()
{
	js_Reload_Form_Selection();
}

function js_Changed_Form_Selection()
{
	js_Reload_Class_Selection();
}

function js_Changed_Class_Selection()
{
	js_Reload_Student_Selection();
}

function js_Changed_Subject_Selection()
{
	js_Reload_Topic_Selection()
}

function js_Changed_Student_Subject_Group_Selection() {
	js_Reload_Student_Selection();
}

function js_Changed_Student_Year_Term_Selection() {
	js_Reload_Student_Subject_Group_Selection();
}

function js_Reload_Form_Selection()
{
	var FormStageIDStr ;
	if($("#FormStageIDArr\\[\\]").val())
		FormStageIDStr = $("#FormStageIDArr\\[\\]").val().toString()
	else
		FormStageIDStr = ''
	
	$('span#YearSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{
			Action: 'Form_Selection',
			FormStageID: FormStageIDStr,
			SelectionID: 'YearIDArr[]',
			OnChange: 'js_Changed_Form_Selection();',
			IsMultiple: 1,
			NoFirst: 1
		},
		function(ReturnData)
		{
			js_Select_All('YearIDArr[]', 1);
			js_Reload_Class_Selection();
		}
	);	
}

function js_Reload_Class_Selection()
{
	if($('select#YearIDArr\\[\\]').val())
		var jsYearID = $('select#YearIDArr\\[\\]').val().toString();
	else
		var jsYearID = '';
	
	$('span#ClassSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{
			Action: 'Class_Selection',
			YearID: jsYearID,
			SelectionID: 'YearClassIDArr[]',
			OnChange: 'js_Changed_Class_Selection();',
			IsMultiple: 1,
			NoFirst: 1
		},
		function(ReturnData)
		{
			js_Select_All('YearClassIDArr[]', 1);
			js_Reload_Student_Selection();
		}
	);
}

/*
function js_Reload_Student_Selection()
{
	console.log("js_Reload_Student_Selection");
	
	var jsYearClassIDValue = $('select#YearClassIDArr\\[\\]').val();
	var jsYearClassIDList;
	if (jsYearClassIDValue == null)
		jsYearClassIDList = '';
	else
		jsYearClassIDList = jsYearClassIDValue.toString();
	
	var jsExtraInfoValue = $('select#ExtraItemID\\[\\]').val();
	var jsExtraInfoList;
	if (jsExtraInfoValue == null)
		jsExtraInfoList = '';
	else
		jsExtraInfoList = jsExtraInfoValue.toString();
	
	$('span#StudentSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{
			Action: 'Class_Student_Selection',
			YearClassIDList: jsYearClassIDList,
			ExtraInfoList : jsExtraInfoList,
			SelectionID: 'StudentIDArr[]',
			IsMultiple: 1,
			NoFirst: 1
		},
		function(ReturnData)
		{
			js_Select_All('StudentIDArr[]', 1);
		}
	);
}
*/

function js_Reload_Topic_Selection()
{
	var SubjectIDStr ;
	if($("#SubjectIDArr\\[\\]").val())
		SubjectIDStr = $("#SubjectIDArr\\[\\]").val().toString()
	else
		SubjectIDStr = ''
	
	$('span#TopicSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{
			Action: 'Reload_Topic_Selection',
			SubjectID: SubjectIDStr,
			SelectionID: 'TopicIDArr[]',
			OnChange: '',
			IsMultiple: 1,
			noFirst: 1,
			all: 0,
			Level: 3 
		},
		function(ReturnData)
		{
			js_Select_All('TopicIDArr[]', 1);
		}
	);	
}
function js_Select_All_Form_Stage(jsObjectID)
{
	js_Select_All(jsObjectID);
	js_Reload_Form_Selection();
}

function js_Select_All_Year(jsObjectID)
{
	js_Select_All(jsObjectID);
	js_Reload_Class_Selection();
}

function js_Select_All_Year_Class(jsObjectID)
{
	js_Select_All_With_OptGroup(jsObjectID);
	js_Reload_Student_Selection();
}

function js_Select_All_Extra_Info()
{
	js_Select_All_With_OptGroup("ExtraItemID[]");
	js_Reload_Student_Selection();
}

function js_Select_All_Subject(jsObjectID)
{
	js_Select_All_With_OptGroup(jsObjectID,1);
	js_Reload_Topic_Selection();
}

function js_Changed_AcademicYear_Selection(jsType)
{
	js_Reload_Term_Selection(jsType);
}

function js_Changed_Term_Selection(jsType)
{
	js_Reload_Module_Selection(jsType)
}

function js_Changed_Student_Subject_Selection() {
	js_Reload_Student_Subject_Group_Selection();
}

function js_Reload_Term_Selection(jsType)
{
	var jsAcademicYearID = $("#"+jsType+"AcademicYearID").val();
//	eval('var jsAcademicYearID = jsCur' + jsType + 'AcademicYearID;');
//	eval('var jsYearTermID = jsCur' + jsType + 'YearTermID;');
	var jsSelectionID = jsType + "YearTermID";
	
	$('div#' + jsType + 'YearTermSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{
			Action: 'Term_Selection',
			AcademicYearID: jsAcademicYearID,
			
			SelectionID: jsSelectionID,
			OnChange: 'js_Changed_Term_Selection(\''+ jsType +'\');',
			NoFirst: 0,
			DisplayAll: 1,
			FirstTitle: '<?=$Lang['General']['WholeYear']?>'
		},
		function(ReturnData)
		{
			js_Reload_Module_Selection(jsType);
		}
	);
}

function js_Reload_Module_Selection(jsType)
{
	var jsAcademicYearID = $("select#"+jsType+"AcademicYearID").val();
	var jsYearTermID = $('select#'+jsType+'YearTermID').val();
	var jsSelectionID = jsType+'ModuleID[]';

	$('div#' + jsType + 'ModuleSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{
			Action: 'Module_Selection',
			YearTermID: jsYearTermID,
			AcademicYearID :jsAcademicYearID,
			SelectionID: jsSelectionID,
			IsAll: 0,
			NoFirst: 1,
			IsMultiple:1
		},
		function(ReturnData)
		{
			js_Select_All(jsType+'ModuleID[]');
		}
	);
	
}

function js_Changed_Student_Selection_Source(jsTargetSource, jsInit)
{
	if (jsTargetSource == 'Class') {
		$('tr.Student_FormClassTr').show();
		$('tr.Student_SubjectGroupTr').hide();
	}
	else if (jsTargetSource == 'SubjectGroup') {
		$('tr.Student_FormClassTr').hide();
		$('tr.Student_SubjectGroupTr').show();
	}
	
	if(jsInit!=1)
	{
		js_Reload_Student_Selection();
	}
}

//Subject Group Selection Functions
function js_Reload_Student_Year_Term_Selection(js_Init)
{
	
	$('span#StudentYearTermSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{
			Action: 'Term_Selection',
			SelectionID: 'StudentYearTermIDArr[]',
			OnChange: 'js_Changed_Student_Year_Term_Selection();',
			IsMultiple: 1,
			NoFirst: 1
		},
		function(ReturnData)
		{
			js_Select_All('StudentYearTermIDArr[]', 1);
			js_Reload_Student_Subject_Group_Selection(js_Init);
		}
	);
}

function js_Reload_Student_Subject_Group_Selection(js_Init)
{
	var jsSubjectIDList = Get_Selection_Value('StudentSubjectIDArr[]', 'String');
	var jsYearTermIDList = Get_Selection_Value('StudentYearTermIDArr[]', 'String');
	
	$('span#StudentSubjectGroupSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{
			Action: 'Subject_Group_Selection',
			SubjectID: jsSubjectIDList,
			YearTermID: jsYearTermIDList,
			SelectionID: 'StudentSubjectGroupIDArr[]',
			OnChange: 'js_Changed_Student_Subject_Group_Selection();',
			IsMultiple: 1,
			NoFirst: 1,
			DisplayTermInfo: 1
		},
		function(ReturnData)
		{
			// Load preset if initialize and sepecfied the settings
			js_Select_All('StudentSubjectGroupIDArr[]', 1);
		
			if(!js_Init)
				js_Reload_Student_Selection();
			
		}
	);
}


function js_Reload_Student_Selection()
{
	var jsSelectStudentSource = $("input[name='SelectStudentFrom']:checked").val();
	var jsExtraInfoList = Get_Selection_Value('ExtraItemIDArr[]', 'String');
	
	var jsYearClassIDList = '';
	var jsSubjectGroupIDList = '';
	if (jsSelectStudentSource == 'Class') {
		jsYearClassIDList = Get_Selection_Value('YearClassIDArr[]', 'String');
	}
	else if (jsSelectStudentSource == 'SubjectGroup') {
		jsSubjectGroupIDList = Get_Selection_Value('StudentSubjectGroupIDArr[]', 'String');
	}
					
	$('span#StudentSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{
			Action: 'Class_Student_Selection',
			YearClassIDList: jsYearClassIDList,
			SubjectGroupIDList: jsSubjectGroupIDList,
			ExtraInfoList: jsExtraInfoList,
			SelectionID: 'StudentIDArr[]',
			IsMultiple: 1,
			NoFirst: 1
		},
		function(ReturnData)
		{
			js_Select_All('StudentIDArr[]', 1);
		}
	);
}

function js_Check_Form()
{
	var jsValid = true;
	var jsSelectStudentSource = $("input[name='SelectStudentFrom']:checked").val();
	
//	if (jsSelectStudentSource == 'Class') {
//		
//		// Check selected FormStage
//		if ($('select#FormStageIDArr\\[\\]').val() == null)
//		{
//			$('div#SelectFormStageWarningDiv').show();
//			$('select#FormStageIDArr\\[\\]').focus();
//			jsValid = false;
//		}
//		else
//		{
//			$('div#SelectFormStageWarningDiv').hide();
//		}
//		
//		// Check selected Form
//		if ($('select#YearIDArr\\[\\]').val() == null)
//		{
//			$('div#SelectFormWarningDiv').show();
//			$('select#YearIDArr\\[\\]').focus();
//			jsValid = false;
//		}
//		else
//		{
//			$('div#SelectFormWarningDiv').hide();
//		}
//		
//		// Check selected Class
//		if ($('select#YearClassIDArr\\[\\]').val() == null)
//		{
//			$('div#SelectClassWarningDiv').show();
//			$('select#YearClassIDArr\\[\\]').focus();
//			jsValid = false;
//		}
//		else
//		{
//			$('div#SelectClassWarningDiv').hide();
//		}
//	}
//	else
//	{
//		
//	}
	
	// Check selected Student
	if ($('select#StudentIDArr\\[\\]').val() == null)
	{
		$('div#SelectStudentWarningDiv').show();
		$('select#StudentIDArr\\[\\]').focus();
		jsValid = false;
	}
	else
	{
		$('div#SelectStudentWarningDiv').hide();
	}
	
	// Check selected Subject
	if ($('select#SubjectIDArr\\[\\]').val() == null)
	{
		$('div#SelectSubjectWarningDiv').show();
		$('select#SubjectIDArr\\[\\]').focus();
		jsValid = false;
	}
	else
	{
		$('div#SelectSubjectWarningDiv').hide();
	}
	
	// Check selected Topic
	if ($('select#TopicIDArr\\[\\]').val() == null)
	{
		$('div#SelectTopicWarningDiv').show();
		$('select#TopicIDArr\\[\\]').focus();
		jsValid = false;
	}
	else
	{
		$('div#SelectTopicWarningDiv').hide();
	}

	// Check selected Module
	if ($('select#FirstModuleID\\[\\]').val() == null)
	{
		$('div#FirstModuleIDWarningDiv').show();
		$('select#FirstModuleID\\[\\]').focus();
		jsValid = false;
	}
	else
	{
		$('div#FirstModuleIDWarningDiv').hide();
	}
	
	if ($('select#SecondModuleID\\[\\]').val() == null)
	{
		$('div#SecondModuleIDWarningDiv').show();
		$('select#SecondModuleID\\[\\]').focus();
		jsValid = false;
	}
	else
	{
		$('div#SecondModuleIDWarningDiv').hide();
	}		
	
	if (jsValid == true)
		document.getElementById('form1').submit();
}

function js_Hide_Option_Table(jsTableNumber)
{
	var jsTableID = "OptionTable_" + jsTableNumber;
	var jsShowSpanID = "spanShowOption_" + jsTableNumber;
	var jsHideSpanID = "spanHideOption_" + jsTableNumber;
	
	$('table#' + jsTableID).hide();
	$('span#' + jsShowSpanID).show();
	$('span#' + jsHideSpanID).hide();
}

function js_Show_Option_Table(jsTableNumber)
{
	var jsTableID = "OptionTable_" + jsTableNumber;
	var jsShowSpanID = "spanShowOption_" + jsTableNumber;
	var jsHideSpanID = "spanHideOption_" + jsTableNumber;
	
	$('table#' + jsTableID).show();
	$('span#' + jsShowSpanID).hide();
	$('span#' + jsHideSpanID).show();
}

$().ready(function (){
//	js_Changed_Form_Selection();
	js_Changed_Student_Selection_Source('Class',1)
	js_Select_All_With_OptGroup("ExtraItemID[]",1);
	
	js_Reload_Student_Year_Term_Selection(1)
	js_Select_All("StudentSubjectIDArr[]", 1)
	
	js_Select_All_Form_Stage('FormStageIDArr[]')
	
	js_Select_All_With_OptGroup("SubjectIDArr[]", 1);
	js_Reload_Topic_Selection();
	js_Changed_AcademicYear_Selection('First')
	js_Changed_AcademicYear_Selection('Second')
});
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>