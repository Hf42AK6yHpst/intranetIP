<?php
// using: Ivan
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Reports_ClassAverageRubricsCount";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_rubrics.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_module.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");

$lreportcard = new libreportcardrubrics();
$lreportcard_topic = new libreportcardrubrics_topic();
$lreportcard_module = new libreportcardrubrics_module();
$lreportcard_rubrics = new libreportcardrubrics_rubrics();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$lreportcard_ui = new libreportcardrubrics_ui();

include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/print_header.php");
echo $lreportcard_ui->Include_JS_CSS(array('rubrics_css'));

### Print Button
$PrintBtn = $lreportcard_ui->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();", "submit2");

$ActiveYearTopicInfoArr = $lreportcard_topic->Get_Topic_Info($SubjectIDArr,$TopicIDArr,'','','','',0);
$TopicCodeArr = Get_Array_By_Key($ActiveYearTopicInfoArr, "TopicCode"); // use active year topic code to map topic of other academic year
$ActiveYearTopicInfoAssoc = BuildMultiKeyAssoc($ActiveYearTopicInfoArr, array("SubjectID","TopicCode"));

//$TargetLevel = 1;

# First Report Data
$lreportcard->libreportcardrubrics($FirstAcademicYearID);

$lreportcard_topic->libreportcardrubrics_topic($FirstAcademicYearID);
$FirstReportTopicInfo =  $lreportcard_topic->Get_Topic_Info('','','',$TopicCodeArr);

$lreportcard_module->libreportcardrubrics_module($FirstAcademicYearID);
$FirstModuleInfo = $lreportcard_module->Get_Module_Info('',$FirstModuleID);

$DataArr[0]['TopicInfoArr'] = BuildMultiKeyAssoc($FirstReportTopicInfo,"TopicCode");
$DataArr[0]['AcademicYearID'] = $FirstAcademicYearID;
$DataArr[0]['ModuleInfo'] = $FirstModuleInfo;
$DataArr[0]['MarkArr'] = $lreportcard->Get_Student_Marksheet_Score($SubjectIDArr, $StudentIDArr, $FirstModuleID);

# Second Report Data
$lreportcard->libreportcardrubrics($SecondAcademicYearID);

$lreportcard_topic->libreportcardrubrics_topic($SecondAcademicYearID);
$SecondReportTopicInfo =  $lreportcard_topic->Get_Topic_Info('','','',$TopicCodeArr);

$lreportcard_module->libreportcardrubrics_module($SecondAcademicYearID);
$SecondModuleInfo = $lreportcard_module->Get_Module_Info('',$SecondModuleID);

$DataArr[1]['TopicInfoArr'] = BuildMultiKeyAssoc($SecondReportTopicInfo,"TopicCode");
$DataArr[1]['AcademicYearID'] = $SecondAcademicYearID;
$DataArr[1]['ModuleInfo'] = $SecondModuleInfo;
$DataArr[1]['MarkArr'] = $lreportcard->Get_Student_Marksheet_Score($SubjectIDArr, $StudentIDArr, $SecondModuleID);	

$SubjectInfo = $lreportcard->Get_Subject_Info();
switch($SubjectDisplay)
{
	case "Abbr": $subfix = "ABBR"; break;
	case "Desc": $subfix = "DES"; break;
	case "ShortForm": $subfix = "SNAME"; break;
}	
$SubjectTitle = Get_Lang_Selection("CH_","EN_").$subfix;
$SubjectTitleArr = BuildMultiKeyAssoc($SubjectInfo,"SubjectID",$SubjectTitle,1);

$StudentInfoArr = $lreportcard->Get_Student_Class_Info($StudentIDArr);
$StudentInfoAssoc = BuildMultiKeyAssoc($StudentInfoArr,"UserID");

$ClassTitleLang = Get_Lang_Selection("ClassTitleB5","ClassTitleEN");
$TopicTitleLang = Get_Lang_Selection("TopicNameCh","TopicNameEn");
$ModuleTitleLang = Get_Lang_Selection("ModuleNameCh","ModuleNameEn");

foreach($SubjectIDArr as $thisSubjectID)
{
	if(count($ActiveYearTopicInfoAssoc[$thisSubjectID])==0)
		continue;
	
	$thisTopicInfoArr = $ActiveYearTopicInfoAssoc[$thisSubjectID];
	foreach((array)$thisTopicInfoArr as $ActiveYearTopicCode => $thisTopicInfo)
	{
		$x .= '<div>';
			$x .= '<span class="compare_report_title">'.$Lang['eRC_Rubrics']['GeneralArr']['Subject'].':</span>'.$SubjectTitleArr[$thisSubjectID]."\n";
			$x .= '<span class="compare_report_title">'.$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][1]['Title'].':</span>'.$thisTopicInfo[$TopicTitleLang]."\n";
		$x .= '</div>';
		$x .= '<table class="report_table" border=1>'."\n";
			$x .= '<thead>'."\n";
				$x .= '<tr>'."\n";
					$x .= '<th>'.$Lang['eRC_Rubrics']['GeneralArr']['ClassName'].'</th>'."\n";
					$x .= '<th>'.$Lang['eRC_Rubrics']['GeneralArr']['ClassNumber'].'</th>'."\n";
					$x .= '<th>'.$Lang['eRC_Rubrics']['GeneralArr']['Name'].'</th>'."\n";
					for($i=0;$i<2;$i++)
					{
						$AYName = getAcademicYearByAcademicYearID($DataArr[$i]['AcademicYearID'],'');
						foreach($DataArr[$i]['ModuleInfo'] as $ModuleInfo)
						{
							$x .= '<th>'.$AYName."<br>".$ModuleInfo[$ModuleTitleLang].'</th>'."\n";
						}
					}
				$x .= '</tr>'."\n";
			$x .= '</thead>'."\n";
			$x .= '<tbody>'."\n";
			
				foreach($StudentInfoAssoc as $thisStudentID => $thisStudentInfo)
				{
					$x .= '<tr>'."\n";
						$x .= '<td>'.$thisStudentInfo[$ClassTitleLang].'</td>'."\n";
						$x .= '<td>'.$thisStudentInfo['ClassNumber'].'</td>'."\n";
						$x .= '<td>'.$thisStudentInfo['StudentName'].'</td>'."\n";		
					
						for($i=0;$i<2;$i++)
						{
							$SelectedYearTopicID = $DataArr[$i]['TopicInfoArr'][$ActiveYearTopicCode]['TopicID'];
							
							$thisMarkArr = $DataArr[$i]['MarkArr'];
	//						
							foreach($DataArr[$i]['ModuleInfo'] as $ModuleInfo)
							{
								$thisModuleID = $ModuleInfo['ModuleID'];
								$thisMarkArr[$thisStudentID][$thisSubjectID][$SelectedYearTopicID][$thisModuleID]['Level'];
								
								$x .= '<td>&nbsp;'.$thisMarkArr[$thisStudentID][$thisSubjectID][$SelectedYearTopicID][$thisModuleID]['Level'].'</td>'."\n";
							}
						}
					$x .= '</tr>'."\n";
				}
			$x .= '</tbody>'."\n";
		$x .= '</table>'."\n";
		$x .= '<br>'."\n";

	}
}

?>
<table border="0" cellpadding="4" width="95%" cellspacing="0" class="print_hide">
	<tr><td align="right"><span><?=$PrintBtn?></span></td></tr>
</table>
<table border="0" cellpadding="4" width="95%" cellspacing="0">
	<tr><td><?=$x?></td></tr>
</table>
<table border="0" cellpadding="4" width="95%" cellspacing="0" class="print_hide">
	<tr><td align="right"><span><?=$PrintBtn?></span></td></tr>
</table>

<?
intranet_closedb();
?>