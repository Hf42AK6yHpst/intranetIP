<?php
// using :
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
$lreportcard = new libreportcardrubrics_custom();
$lreportcard_ui = new libreportcardrubrics_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);

if ($Action == "Settings_Layer")
{
	$ReturnString = $lreportcard_ui->Get_Reports_ReportGeneration_Settings_Layer();
}

echo $ReturnString;

intranet_closedb();
?>