<?php
/*
 *  2019-05-22 Cameron
 *      - security fix: apply getDecryptedText to file and path
 */
	if (!$_GET["file"]) {
		header ("Location: /");
		exit();
	}
	
	$filename = basename(getDecryptedText($_GET["file"]));
	
	$ref = @$HTTP_REFERER;
	$url_format = parse_url($ref);
	
	if (!isset($path) || $path=='') {
//		$path = $PATH_WRT_ROOT.str_replace(basename($url_format["path"]), "", $url_format["path"]).$filename;
		$path = $intranet_root."/".str_replace(basename($url_format["path"]), "", $url_format["path"]).$filename;
	}
	else {
	    $path = getDecryptedText($_GET["path"]);
	    $path = $intranet_root."/".$path.$filename;
	}




	$parts = explode(".", $filename);
	if (is_array($parts) && count($parts) > 1)
	    $extension = strtoupper(end($parts));
	
	# Only allow plain text files on the server to be read
	$allow_ext = array("CSV", "TXT", "LOG","csv","txt","log","DAT","dat");
	
	$handle = @fopen($path,"r");

	if ($handle && in_array($extension, $allow_ext)) {
		$data = fread($handle, filesize($path));
		
		header('Pragma: public');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	
		header('Content-type: application/octet-stream');
		header('Content-Length: '.strlen($data));
	
		header('Content-Disposition: attachment; filename="'.$filename.'";');
	
		print $data;
	} else {

		header ("Location: /");
		exit();
	}
?>