<?php
///home/web/eclass40/intranetIP25/includes/libUploadFiles.php
include_once($PATH_WRT_ROOT."/includes/libUploadFiles.php");
$sid = trim($sid);
$record_date = trim($record_date);

//debug_r($_POST);

/////////////////////////
// IMPORTANCE VARIABLE //
/////////////////////////
// store the HTML event mapping , ie map n1 (html) to DB table MEDICAL_STUDENT_LOG.RecordID,
// eg $inputIdMapWithDBId['n1'] = 1234;  // 1234 is the table MEDICAL_STUDENT_LOG.RecordID , IT IS USED FOR SAVE PART AND DOCUMENT
$inputIdMapWithDBId = array(); 

foreach($event as $key=>$details)
{

		$objStudentLogPartsMapper = null;
		$_recordId = '';

		$objStudendLog = new StudentLog();
		if(strpos($key,'n') === 0 )
		{
			//new record , do nothing
		}else
		{
			if($key > 0 && is_numeric(intval($key))){
				//assign the student log recordid for update
				$objStudendLog->setRecordID($key);
			}else
			{
				//not a valid key value , skip this record
				continue; 			
			}
		}

		$objStudendLog->setBehaviourOne($details['level1']);
		$objStudendLog->setBehaviourTwo($details['level2']);
		$objStudendLog->setRemarks($details['remark']);
		$objStudendLog->setRecordTime($record_date.' '.$details['record_hour'].':'.$details['record_min']);					
		$objStudendLog->setDuration($details['duration_min'].':'.$details['duration_sec']);
		$objStudendLog->setPIC($details['pic']);
		$objStudendLog->setInputBy($medical_currentUserId);
		$objStudendLog->setModifyBy($medical_currentUserId);
		$objStudendLog->setUserID($sid);

		$objLogMapper = new StudentLogMapper();
		$objLogMapper->save($objStudendLog);
		
		$_logRecordId = $objStudendLog->getRecordID();

		if($_logRecordId > 0)
		{
			//save this $_logRecordId to the array $inputIdMapWithDBId for upload document
			$inputIdMapWithDBId[$key] = $_logRecordId;
			
			$objStudentLogPartsMapper = new StudentLogPartsMapper($_logRecordId);

			for($i=0,$iMax = count($details['parts']['level3']);$i < $iMax; $i++)
			{
				
				$objStudentLogParts = new StudentLogParts();
				$objStudentLogParts->setLevel3ID($details['parts']['level3'][$i]);
				$objStudentLogParts->setLevel4ID($details['parts']['level4'][$i]);

				$objStudentLogParts->setDateInput('now()');
				$objStudentLogParts->setInputBy($medical_currentUserId);
				$objStudentLogParts->setDateModified('now()');
				$objStudentLogParts->setModifyBy($medical_currentUserId);

				$objStudentLogPartsMapper->addParts($objStudentLogParts);
			}
			$result = $objStudentLogPartsMapper->save();
		}
}
//return;

/////////////////////////////////////////
//handle the file upload Data Structure//
/////////////////////////////////////////
foreach($_FILES as $_file=>$fileDetails){
//	debug_r($fileDetails);
	foreach($fileDetails['name'] as $_elementName=>$_details){
		$uploadFileList[$_elementName]['name'] = $_details['uploadFile'];
	}
	$_details = null;

	foreach($fileDetails['type'] as $_elementName=>$_details){
		$uploadFileList[$_elementName]['type'] = $_details['uploadFile'];
	}
	$_details = null;

	foreach($fileDetails['tmp_name'] as $_elementName=>$_details){
		$uploadFileList[$_elementName]['tmp_name']= $_details['uploadFile'];
	}
	$_details = null;

	foreach($fileDetails['error'] as $_elementName=>$_details){
		$uploadFileList[$_elementName]['error'] = $_details['uploadFile'];
	}
	$_details = null;


	foreach($fileDetails['size'] as $_elementName=>$_details){
		$uploadFileList[$_elementName]['size'] = $_details['uploadFile'];
	}
	$_details = null;

}

$uploadFile_root = $medical_cfg['uploadFile_root'];
if(!file_exists($uploadFile_root)){
	mkdir($uploadFile_root,0777);
}

$uploadResultAry = array(); //STORE THE UPLOAD DOCUMENT RESULT
foreach ($uploadFileList as $_uploadInputName=>$_details)
{
	//must be '==='
	if(strpos($_uploadInputName,'n' === 0))
	{
		//start with "n" , it is a new record
	}
	//$uploadResultAry = NULL;
	for($i = 0,$iMax = count($_details['name']); $i < $iMax; $i++)
	{
		if(trim($_details['tmp_name'][$i]) == '')
		{//without upload item , skip to next one
			continue; 
		}
		$objUploadFiles = new libUploadFiles($uploadFile_root);

		$objUploadFiles->setName($_details['name'][$i]);
		$objUploadFiles->setFileType($_details['type'][$i]);
		$objUploadFiles->setTmp_Name($_details['tmp_name'][$i]);
		$objUploadFiles->setError($_details['error'][$i]);
		$objUploadFiles->setSize($_details['size'][$i]);
		$uploadResultAry[$_uploadInputName][] = $objUploadFiles->upload();
	}
}


//SAVE TO DB
foreach ($uploadResultAry as $_uploadInputName => $_uploadResult)
{
	$_uploadFailed = 0; //RECORD FOR EACH _uploadInputName

	if($inputIdMapWithDBId[$_uploadInputName] == '')
	{
		$errMsg = 'Support a value must be find in array \$inputIdMapWithDBId but failed. Value :['.$_uploadInputName.'] Array Value '.print_r($rawArray,true).'"' ." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
		alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');

		continue;
	}
	else
	{
		for($i = 0,$iMax = count($_uploadResult);$i < $iMax; $i++)
		{
			if(!$_uploadResult[$i]['uploadSuccess'])
			{
				++$_uploadFailed;
				$errMsg = 'Error in upload document. ERROR value :['.print_r($_uploadResult[$i],true).']' ." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
				alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');

				//upload failed , skip to save to DB
				continue; 
			}

			$objDocument = new StudentLogDocument();

			$objDocument->setStudentLogID($inputIdMapWithDBId[$_uploadInputName]);
			$objDocument->setRenameFile($_uploadResult[$i]['renameFile']);
			$objDocument->setOrgFileName($_uploadResult[$i]['name']);
			$objDocument->setFileType($_uploadResult[$i]['type']);
			$objDocument->setFileSize($_uploadResult[$i]['size']);
			$objDocument->setFolderPath($_uploadResult[$i]['destFolder']);
			$objDocument->setDateInput('now()');
			$objDocument->setInputBy($medical_currentUserId);
			$objDocument->save();	
		}
	}
}

//debug_r($uploadFileList);
?>