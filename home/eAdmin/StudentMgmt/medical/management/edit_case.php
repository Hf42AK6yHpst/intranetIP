<?php
// using:
/*
 * 2018-12-27 Cameron
 * - not allow to change student in edit mode
 *      
 * 2018-03-20 Cameron
 * - add group selection [case #F121742]
 *
 * 2017-06-30 Cameron
 * - create this file
 */
if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'CASE_MANAGEMENT') || ! $plugin['medical_module']['discipline']) {
    header("location: /");
    exit();
}

if (is_array($CaseID)) {
    $caseID = (count($CaseID) == 1) ? $CaseID[0] : '';
} else {
    $caseID = $CaseID;
}

if (! $caseID) {
    header("Location: ?t=management.case_list");
}

$rs_case = $objMedical->getCases($caseID);

if (count($rs_case) == 1) {
    $rs_case = current($rs_case);
}

$CurrentPage = "ManagementCase";
$CurrentPageName = $Lang['medical']['menu']['case'];
$TAGS_OBJ[] = array(
    $Lang['medical']['menu']['case'],
    "",
    0
);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();

$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$PAGE_NAVIGATION[] = array(
    $Lang['medical']['menu']['case'],
    "?t=management.case_list"
);
$PAGE_NAVIGATION[] = array(
    $Lang['Btn']['Edit'],
    ""
);

$lclass = new libclass();

$rs_case['CasePIC'] = $objMedical->getCasePIC($caseID);

$rs_class_name = $objMedical->getClassByStudentID($rs_case['StudentID']);

if (! empty($rs_class_name)) {
//     $classSelection = $lclass->getSelectClass("name='ClassName' id='ClassName' ", $rs_class_name);
//     $studentSelection = $objMedical->getStudentNameListWClassNumberByClassName($rs_class_name);
//     $studentSelection = getSelectByArray($studentSelection, "name='StudentID' id='StudentID'", $rs_case['StudentID']);
//     $groupSelection = $objMedical->getGroupSelection("GroupID", "", "style=\"display:none\"", "-- " . $Lang['Btn']['Select'] . " --");
    $studentInfo = $objMedical->getStudentNameListWClassNumberByClassName($rs_class_name,array($rs_case['StudentID']));
    $studentInfo = count($studentInfo) ? $studentInfo[0]['StudentName'] : '';
}

$relevantEvents = $objMedical->getCaseEventList($caseID);
$meetingMinutes = $objMedical->getCaseMinuteList($caseID);

$linterface->LAYOUT_START($returnMsg);

$form_action = '?t=management.edit_case_save';

include_once ('templates/case.tmpl.php');

if ($use_plupload) {
    include_once ('plupload_script.php');
}

$linterface->LAYOUT_STOP();
?>