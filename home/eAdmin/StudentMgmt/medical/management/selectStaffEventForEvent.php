<?php 
/*
 *  2019-01-04 Cameron
 * - create this file
 * 
 */

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='STAFFEVENT_VIEW') || !$plugin['medical_module']['staffEvents']){
    header("Location: /");
    exit();
}

$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);
$AcademicYearID = Get_Current_Academic_Year_ID();

// ############ start Filters

// Event Type Filter
$staffEventTypeAry = $objMedical->getStaffEventType();
$staffEventTypeFilter = getSelectByArray($staffEventTypeAry, "name='StaffEventTypeID' id='StaffEventTypeID' onChange='changeStaffEventType();'", $StaffEventTypeID, 0, 0, $Lang['medical']['event']['AllCategory']);

// Event Type Lev2 Filter
$eventTypeLev2Ary = array();
$eventTypeLev2Filter = getSelectByArray($eventTypeLev2Ary, "name='StaffEventTypeLev2ID' id='StaffEventTypeLev2ID' onChange='getStaffEvents();' style='display:none'", $EventTypeLev2ID, 0, 0, $Lang['medical']['staffEvent']['AllSubcategory']);

$buildingAry = $objMedical->getInventoryBuildingArray();
$buildingFilter = getSelectByArray($buildingAry, "Name='SrhLocationBuildingID' ID='SrhLocationBuildingID' onChange='changeSrhBuilding();'", $SrhLocationBuildingID, 0, 0, $Lang['SysMgr']['Location']['All']['Building']);

$levelAry = array();
$levelFilter = getSelectByArray($levelAry, "Name='SrhLocationLevelID' ID='SrhLocationLevelID' onChange='changeSrhBuildingLevel();' style='display:none'", $SrhLocationLevelID, 0, 0, $Lang['SysMgr']['Location']['All']['Floor']);

$locationAry = array();
$locationFilter = getSelectByArray($locationAry, "Name='SrhLocationID' ID='SrhLocationID' onChange='getStaffEvents();' style='display:none'", $SrhLocationID, 0, 0, $Lang['SysMgr']['Location']['All']['Room']);

$staffFilter= $objMedical->getTeacherOptionsByType($StaffID);

$teacherAry = $objMedical->getTeacher();
$reporterFilter = getSelectByArray($teacherAry, "name='StaffEventReporter' onChange='getStaffEvents();'", $EventReporter, 0, 0, $Lang['medical']['case']['AllReporter']);

$StaffEventDateStart = $StaffEventDateStart ? $StaffEventDateStart : substr(getStartDateOfAcademicYear($AcademicYearID), 0, 10);
$StaffEventDateEnd = $StaffEventDateEnd ? $StaffEventDateEnd : substr(getEndDateOfAcademicYear($AcademicYearID), 0, 10);

// ############ end Filters

?>
<script>
$(document).ready(function(){
	
	$('#StaffEventDate').click(function(){
		if ($(this).is(':checked')){
			$('#spanRequest').css('display','inline');
		}
		else {
			$('#spanRequest').css('display','none');
		}
	});
	
	$('#keyword').keyup(function(event) {				
	  	if (event.which == 13) {			
			getStaffEvents();		
	  	}  			
	});				
	
	$('#keyword').keypress(function(e){			
	    if ( e.which == 13 ) {			
			e.preventDefault();	
		    return false;		
	    }			
	});			

	$('#StaffID').change(function(){
		getStaffEvents();
	});
	
	getStaffEvents();
		
});

function getStaffEvents() {
	$.ajax({
		dataType: "json",
		type: "POST",
		url: '?t=management.ajax.ajax&ma=1&action=getStaffEvents',
		data : $('#form2').serialize(),		  
		success: showStaffEvents,
		error: show_ajax_error
	});
}

function showStaffEvents(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#StaffEventList').html(ajaxReturn.html);
		if (parseInt(ajaxReturn.html2) > 0) {
			$('#SelectBtn').css('display','');
		}
		else {
			$('#SelectBtn').css('display','none');
		}
	}
}	

function changeStaffEventType() 
{
	if ($('#StaffEventTypeID').val() == '') {
		$('#StaffEventTypeLev2ID').val('');
		$('#StaffEventTypeLev2ID option:not(:first)').remove();
		$('#StaffEventTypeLev2ID').css('display','none');
	}
	$.ajax({
		dataType: "json",
		type: "POST",
		url: '?t=management.ajax.ajax&ma=1',
		data : {
			'action': 'getStaffEventTypeLev2Selection',
			'onChange': 'getStaffEvents()',
			'FirstTitle': 'all',
			'StaffEventTypeID': $('#StaffEventTypeID').val()
		},		  
		success: update_staff_event_type_select,
		error: show_ajax_error
	});
}

function update_staff_event_type_select(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		if ($('#StaffEventTypeID').val() != '') {
			$('#StaffEventTypeLev2ID').replaceWith(ajaxReturn.html);
		}
		getStaffEvents();
	}
}	

function changeSrhBuilding() {
	if ($('#SrhLocationBuildingID').val() == '') {
		$('#SrhLocationLevelID').val('');
		$('#SrhLocationID').val('');
		$('#SrhLocationLevelID option:not(:first)').remove();
		$('#SrhLocationID option:not(:first)').remove();
		$('#SrhLocationLevelID').css('display','none');
		$('#SrhLocationID').css('display','none');
	}

	$.ajax({
		dataType: "json",
		type: "POST",
		url: '?t=management.ajax.ajax&ma=1',
		data : {
			'action': 'getBuildingLevelForStaffEventSelection',
			'FirstTitle': 'all',
			'SrhLocationBuildingID': $('#SrhLocationBuildingID').val()
		},		  
		success: update_building_level_list_in_staff_event_select,
		error: show_ajax_error
	});
}

function update_building_level_list_in_staff_event_select(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		if ($('#SrhLocationBuildingID').val() != '') {
			$('#SrhLocationLevelID').replaceWith(ajaxReturn.html);
			$('#SrhLocationID').val('');
			$('#SrhLocationID option:not(:first)').remove();
			$('#SrhLocationID').css('display','none');
		}
		getStaffEvents();
	}
}	

function changeSrhBuildingLevel() {
	if ($('#SrhLocationLevelID').val() == '') {
		$('#SrhLocationID').val('');
		$('#SrhLocationID option:not(:first)').remove();
		$('#SrhLocationID').css('display','none');
	}
	
	$.ajax({
		dataType: "json",
		type: "POST",
		url: '?t=management.ajax.ajax&ma=1',
		data : {
			'action': 'getLocationForStaffEventSelection',
			'onChange': 'getStaffEvents()',
			'FirstTitle': 'all',
			'SrhLocationLevelID': $('#SrhLocationLevelID').val()
		},		  
		success: update_location_list_in_staff_event_select,
		error: show_ajax_error
	});
}

function update_location_list_in_staff_event_select(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		if ($('#SrhLocationLevelID').val() != '') {
			$('#SrhLocationID').replaceWith(ajaxReturn.html);
		}
		getStaffEvents();
	}
}	

function show_ajax_error() 
{
	alert('<?=$Lang['medical']['general']['error']['ajax']?>');
}

function checkDuplicateSelectEvent() {
	var ret = true;
	$(":input[name^='StaffEventID']:checked").each(function(){
		var thisStaffEventID = $(this).val();
		$(":input[name^='SelStaffEventID']").each(function(){
			if ($(this).val() == thisStaffEventID) {
				ret = false;
			}	
		})
	});
	return ret;	
}

function selectStaffEvents() {
    if(countChecked(document.form2,"StaffEventID[]")==0) {
    	alert(globalAlertMsg2);
    }
    else if (!checkDuplicateSelectEvent()) {
    	alert('<?=$Lang['medical']['case']['Warning']['DuplicateEvent']?>');
    } 
    else{
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '?t=management.ajax.ajax&ma=1&action=getSelectedStaffEventsForStudentEvent',
			data : $('#form2').serialize(),
			success: returnStaffEvents,
			error: show_ajax_error
		});

    	js_Hide_ThickBox();
    }
}

function removeStaffEvent(staffEventID) {
	$('#staff_event_row_'+staffEventID).remove();
}

function returnStaffEvents(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#relevantStaffEventTable').css('display','');
		$('#relevantStaffEventTable').append(ajaxReturn.html);
	}
}	

</script>

<form name="form2" id="form2" method="POST">
	<div class="content_top_tool">

		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="20%"><?=$toolbar ?></td>
				<td width="50%" align="center">&nbsp;</td>
				<td width="30%" style="float: right;">
					<div class="content_top_tool" style="float: right;">
						<?=$htmlAry['searchBox']?>	
						<br style="clear: both" />
					</div>
				</td>
			</tr>
		</table>

	</div>

	<div class="table_board">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="bottom">
					<div class="table_filter">
					<?=$staffFilter?>
					<?=$staffEventTypeFilter?>
					<?php echo $eventTypeLev2Filter;?>
					<?=$buildingFilter?>
					<?=$levelFilter?>
					<?=$locationFilter?>
					<?=$reporterFilter?>
				</div> <br style="clear: both" />
					<div>
						<input type="checkbox" name="StaffEventDate" id="StaffEventDate" value="1"
							<?=$StaffEventDate==1 ? "checked" : ""?>><label for="StaffEventDate"><?=$Lang['medical']['filter']['FilterDateRange']?></label>
						<div id="spanRequest" style="position:relative;display:<?=$StaffEventDate==1 ? "inline" : "none"?>">
							<label for="StaffEventDateStart"><?=$i_From?></label> : <?=$linterface->GET_DATE_PICKER("StaffEventDateStart",$StaffEventDateStart)?>
					 <?=getTimeSel('StaffEventTimeStart', ($StaffEventTimeStartHour?$StaffEventTimeStartHour:0), ($StaffEventTimeStartMin?$StaffEventTimeStartMin:0), "", $minuteStep=1)?>
					 <label for="StaffEventDateEnd"><?=$i_To ?></label> <?=$linterface->GET_DATE_PICKER("StaffEventDateEnd",$StaffEventDateEnd)?>
					 <?=getTimeSel('StaffEventTimeEnd', ($StaffEventTimeEndHour?$StaffEventTimeEndHour:23), ($StaffEventTimeEndMin?$StaffEventTimeEndMin:59), "", $minuteStep=1)?>
					 <?=$linterface->GET_ACTION_BTN($Lang['Btn']['View'], 'button',$ParOnClick="getStaffEvents();", $ParName="submit1")?>
						</div>
					</div>

				</td>
				<td valign="bottom">
					<div class="common_table_tool" id="SelectBtn"
						style="width: 50px; display: none">
						<a href="javascript:selectStaffEvents()" class="tool_other"><?=$Lang['Btn']['Select'] ?></a>
					</div>
				</td>
			</tr>
		</table>
	</div>

	<div class="table_board">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="bottom" id="StaffEventList"></td>
			</tr>
		</table>
	</div>

</form>
