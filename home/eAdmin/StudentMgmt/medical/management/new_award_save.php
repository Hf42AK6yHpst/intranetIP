<?php
// using:
/*
 * 2018-02-27 Cameron
 * - add InputBy and DateInput
 *
 * 2017-10-13 Cameron
 * - use $plugin['medical_module']['awardscheme'] to control awards scheme
 *
 * 2017-07-06 Cameron
 * - create this file
 */
if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'AWARDSCHEME_MANAGEMENT') || ! $plugin['medical_module']['awardscheme']) {
    header("Location: /");
    exit();
}

$ldb = new libdb();
$result = array();

$dataAry['SchemeName'] = standardizeFormPostValue($SchemeName);
$dataAry['StartDate'] = $StartDate;
$dataAry['EndDate'] = $EndDate;
$dataAry['GroupID'] = implode(",", (array) $GroupID);
$dataAry['Remark'] = standardizeFormPostValue($Remark);
$dataAry['LastModifiedBy'] = $_SESSION['UserID'];
$dataAry['InputBy'] = $_SESSION['UserID'];
$dataAry['DateInput'] = 'now()';
$sql = $objMedical->INSERT2TABLE('MEDICAL_AWARD_SCHEME', $dataAry, array(), false);

$ldb->Start_Trans();
// # step 1: add to award scheme
$res = $ldb->db_db_query($sql);
$result[] = $res;
if ($res) {
    $SchemeID = $ldb->db_insert_id();
    
    // # step 2: add scheme PIC
    foreach ((array) $TeacherID as $id) {
        if ($id) {
            unset($dataAry);
            $id = str_replace('U', '', $id);
            $dataAry['SchemeID'] = $SchemeID;
            $dataAry['TeacherID'] = $id;
            $sql = $objMedical->INSERT2TABLE('MEDICAL_AWARD_SCHEME_PIC', $dataAry, array(), false, false, false); // no DateModified field
            $result[] = $ldb->db_db_query($sql);
        }
    }
    
    // # step 3: Scheme Target
    for ($i = 0, $iMax = count($target_name_n); $i < $iMax; $i ++) {
        unset($dataAry);
        $dataAry['SchemeID'] = $SchemeID;
        $dataAry['Target'] = standardizeFormPostValue($target_name_n[$i]);
        $group = array();
        for ($j = 0, $jMax = count($GroupID); $j < $jMax; $j ++) {
            if ($target_group_n[$GroupID[$j]][$i + 1]) {
                $group[] = $GroupID[$j];
            }
        }
        $dataAry['GroupID'] = implode(",", (array) $group);
        $dataAry['LastModifiedBy'] = $_SESSION['UserID'];
        $sql = $objMedical->INSERT2TABLE('MEDICAL_AWARD_SCHEME_TARGET', $dataAry, array(), false);
        $result[] = $ldb->db_db_query($sql);
        unset($group);
    }
}

if (! in_array(false, $result)) {
    $ldb->Commit_Trans();
    $returnMsgKey = 'AddSuccess';
} else {
    $ldb->RollBack_Trans();
    $returnMsgKey = 'AddUnsuccess';
}

header("location: ?t=management.award_list&returnMsgKey=" . $returnMsgKey);

?>