<?php
// using:
/*
 * 2019-01-04 Cameron
 * - add $staffEventViewRight
 *  
 * 2018-04-27 Cameron
 * - apply stripslashes to EventTypeName
 *
 * 2018-04-03 Cameron
 * - show EventTypeNotSetup message if there's no event type when click add event button
 *
 * 2018-03-23 Cameron
 * - set isCase
 *
 * 2017-06-12 Cameron
 * - create this file
 */
if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'EVENT_MANAGEMENT') || ! $plugin['medical_module']['discipline']) {
    header("Location: /");
    exit();
}
$CurrentPage = "ManagementEvent";
$CurrentPageName = $Lang['medical']['menu']['event'];
$TAGS_OBJ[] = array(
    $Lang['medical']['menu']['event'],
    "",
    0
);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();

$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$PAGE_NAVIGATION[] = array(
    $Lang['medical']['menu']['event'],
    "?t=management.event_list"
);
$PAGE_NAVIGATION[] = array(
    $Lang['Btn']['New'],
    ""
);

$eventTypeAry = $objMedical->getEventType();
if (count($eventTypeAry) == 0) {
    header("Location: ?t=management.event_list&returnMsgKey=EventTypeNotSetup");
    exit();
}
else {
    foreach ((array) $eventTypeAry as $_idx => $_eventType) {
        $eventTypeAry[$_idx]['EventTypeName'] = stripslashes($_eventType['EventTypeName']);
        $eventTypeAry[$_idx][1] = $eventTypeAry[$_idx]['EventTypeName']; // 2nd column
    }
}

$eventTypeSelection = getSelectByArray($eventTypeAry, 'Name="EventTypeID" ID="EventTypeID"', $selected = "");

$buildingAry = $objMedical->getInventoryBuildingArray();
$buildingSelection = getSelectByArray($buildingAry, 'Name="LocationBuildingID" ID="LocationBuildingID"', $selected = "");

$staffEventViewRight = $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'STAFFEVENT_VIEW');

$isCase = false;

// # handle attachment(s)
$ret = $objMedical->handleTempUploadFolder();
$tempFolderPath = $ret['TempFolderPath'];
$use_plupload = $objMedical->is_use_plupload();

$linterface->LAYOUT_START($returnMsg);

$plupload = array();
$plupload['use_plupload'] = $use_plupload;
if ($use_plupload) {
    $plupload['pluploadButtonId'] = 'UploadButton';
    $plupload['pluploadDropTargetId'] = 'DropFileArea';
    $plupload['pluploadFileListDivId'] = 'FileListDiv';
    $plupload['pluploadContainerId'] = 'pluploadDiv';
}

$form_action = '?t=management.new_event_save';

include_once ('templates/event.tmpl.php');

if ($use_plupload) {
    include_once ('plupload_script.php');
}

$linterface->LAYOUT_STOP();
?>