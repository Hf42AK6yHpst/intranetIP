<?php
// using:
/*
 * 2019-07-05 Cameron
 * - fix: don't specify as RecordDate for hyperlink column because if need to use the pure date value to sort rather than including html tag
 *
 * 2019-06-13 Philips
 * - create this file
 */
if (! $sys_custom['medical']['Handover'] || !($objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'HANDOVER_MANAGEMENT'))) {
    header("Location: /");
    exit();
}
include_once($PATH_WRT_ROOT. 'includes/libdbtable.php');
include_once($PATH_WRT_ROOT. 'includes/libdbtable2007a.php');
if (isset($ck_page_size) && $ck_page_size != "")
    $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$pageNo = ($pageNo == "") ? 1 : $pageNo;
$order = ($order == "") ? 0 : $order; // default descending
$field = ($field == "") ? 0 : $field;
$li = new libdbtable2007($field, $order, $pageNo);
$firstField = 'iu.'.Get_Lang_Selection('ChineseName','EnglishName');
$secondField = 'iu.'.Get_Lang_Selection('EnglishName','ChineseName');
$firstTitle = 'iu.'.Get_Lang_Selection('TitleChinese', 'TitleEnglish');
$secondTitle = 'iu.'.Get_Lang_Selection('TitleEnglish', 'TitleChinese');
// $Lang['medical']['revisit']['unstatedDate']
$sql = "SELECT 
            CONCAT('<a href=\'?t=management.handover&RecordID=', mh.RecordID, '\'>', DATE_FORMAT(mh.RecordDate, '%Y-%m-%d'), '</a>'),
            mhi.ItemName AS RecordItem,
            IF( mhp.RecordID IS NULL, '', GROUP_CONCAT(IF($firstField IS NULL OR $firstField = '', CONCAT({$secondField},IF({$secondTitle} IS NULL OR {$secondTitle} = '', '', CONCAT(' ', {$secondTitle}))), CONCAT({$firstField},IF({$firstTitle} IS NULL OR {$firstTitle} = '', '', CONCAT(' ', {$firstTitle})))))) AS PIC,
            CONCAT('<input type=\'checkbox\' id=\'RecordID\' name=\'RecordID[]\' value=\'',mh.RecordID, '\' />')
        FROM 
            MEDICAL_HANDOVER mh
            INNER JOIN MEDICAL_HANDOVER_ITEM mhi
                ON mh.RecordItemID = mhi.ItemID
            LEFT OUTER JOIN MEDICAL_HANDOVER_PIC mhp
                ON mh.RecordID = mhp.RecordID
            LEFT OUTER JOIN INTRANET_USER iu
                ON mhp.UserID = iu.UserID
        WHERE 1
            AND mh.isDeleted = '0'
        GROUP BY
                mh.RecordID
";
// debug_pr($sql);die();

$li->field_array = array(
    "RecordDate",
    "RecordItem",
    "PIC"
);
$li->sql = $sql;
$li->no_col = sizeof($li->field_array) + 2;
$li->title = $Lang['medical']['handover']['record'];
$li->column_array = array(
    0,
    0,
    0
);
$li->wrap_array = array(
    0,
    0,
    0
);
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='33%'>" . $li->column($pos ++, $Lang['General']['Date']) . "</th>";
$li->column_list .= "<th width='33%'>" . $li->column($pos ++, $Lang['medical']['handover']['item']) . "</th>";
$li->column_list .= "<th width='33%'>" . $li->column($pos ++, $Lang['medical']['handover']['pic']) . "</th>";
$li->column_list .= "<th width='1'>" . $li->check("RecordID[]") . "</th>\n";

$toolbar = $linterface->GET_LNK_NEW("?t=management.handover", $button_new, "", "", "", 0);

$CurrentPage = "ManagementHandover";
$CurrentPageName = $Lang['medical']['menu']['handover'];
$TAGS_OBJ[] = array(
    $Lang['medical']['menu']['handover'],
    "",
    0
);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();

if ($returnMsgKey == 'DeleteNonSelfRecord') {
    $returnMsg = $Lang['medical']['general']['ReturnMessage']['NotAllowDeletingNonSelfRecord'];
} else {
    $returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
}
$linterface->LAYOUT_START($returnMsg);

include_once ('templates/handover_list.tmpl.php');
$linterface->LAYOUT_STOP();

?>