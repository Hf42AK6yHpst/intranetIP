<?php
// using:
/*
 * 2019-01-04 Cameron
 * - add staff event related to student event
 * 
 * 2018-03-29 Cameron
 * - add event student log
 *
 * 2018-03-23 Cameron
 * - add event case
 *
 * 2018-03-19 Cameron
 * - add AffectedNonAccountHolder to event
 *
 * 2017-06-22 Cameron
 * - create this file
 */
if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'EVENT_MANAGEMENT') || ! $plugin['medical_module']['discipline']) {
    header("Location: /");
    exit();
}

$ldb = new libdb();
$result = array();

// $eventBefore = $objMedical->getEvents($EventID); // get event before update
// if (count($eventBefore) == 1) {
// $eventBefore = current($eventBefore);
// } else {
// header("Location: /");
// exit();
// }

$dataAry['EventDate'] = $EventDate;
$dataAry['StartTime'] = $StartTimeHour . ":" . $StartTimeMin;
$dataAry['EndTime'] = $EndTimeHour . ":" . $EndTimeMin;
$dataAry['EventTypeID'] = $EventTypeID;
$dataAry['LocationBuildingID'] = $LocationBuildingID;
$dataAry['LocationLevelID'] = $LocationLevelID;
$dataAry['LocationID'] = $LocationID;
$dataAry['Summary'] = standardizeFormPostValue($Summary);
$dataAry['Cause'] = standardizeFormPostValue($Cause);
$dataAry['AffectedNonAccountHolder'] = standardizeFormPostValue($AffectedNonAccountHolder);
// $dataAry['IsCase'] = $IsCase;
// $dataAry['CaseID'] = $IsCase ? $CaseID : '0';
$dataAry['LastModifiedBy'] = $_SESSION['UserID'];
$sql = $objMedical->UPDATE2TABLE('MEDICAL_EVENT', $dataAry, array(
    'EventID' => $EventID
), false);

$ldb->Start_Trans();
// # step 1: update to event
$result[] = $ldb->db_db_query($sql);

// # step 2: delete event student that's not in updated list
if ($StudentID) {
    $sql = "DELETE FROM MEDICAL_EVENT_STUDENT WHERE EventID='" . $EventID . "' AND StudentID NOT IN ('" . implode("','", (array) $StudentID) . "')";
    $result[] = $ldb->db_db_query($sql);
}

// # step 3: get present student list after delete in step 1
$present_student = $objMedical->getEventStudent($EventID);
$present_student = Get_Array_By_Key($present_student, 'UserID');

foreach ((array) $StudentID as $id) {
    if (! in_array($id, (array) $present_student)) { // add event student
        unset($dataAry);
        $dataAry['EventID'] = $EventID;
        $dataAry['StudentID'] = $id;
        $sql = $objMedical->INSERT2TABLE('MEDICAL_EVENT_STUDENT', $dataAry, array(), false, true, false); // no DateModified field, insert ignore
        $result[] = $ldb->db_db_query($sql);
    }
}
unset($present_student);

// # step 4: delete event affected person that's not in updated list
if ($AffectedPersonID) {
    $sql = "DELETE FROM MEDICAL_EVENT_AFFECTED_PERSON WHERE EventID='" . $EventID . "' AND AffectedPersonID NOT IN ('" . implode("','", (array) $AffectedPersonID) . "')";
    $result[] = $ldb->db_db_query($sql);
}

// # step 5: get present affected person list after delete in step 4
$present_affected_person = $objMedical->getEventAffectedPerson($EventID);
$present_affected_person = Get_Array_By_Key($present_affected_person, 'UserID');

// # step 6: add event affected person
foreach ((array) $AffectedPersonID as $id) {
    if (! in_array($id, (array) $present_affected_person)) { // add event affected person
        unset($dataAry);
        $dataAry['EventID'] = $EventID;
        $dataAry['AffectedPersonID'] = $id;
        $sql = $objMedical->INSERT2TABLE('MEDICAL_EVENT_AFFECTED_PERSON', $dataAry, array(), false, true, false); // no DateModified field, insert ignore
        $result[] = $ldb->db_db_query($sql);
    }
}
unset($present_affected_person);

// # step 7: add attachments
$uploadResult = $objMedical->handleUploadFile($TargetFolder, $EventID, $table = 'event');
if ($uploadResult != 'AddSuccess') {
    $result[] = false;
}

// # step 8: add event case
$current_case = $objMedical->getEventCase($EventID);
$current_case = Get_Array_By_Key($current_case, 'CaseID');
foreach ((array) $SelCaseID as $_caseID) {
    unset($dataAry);
    $dataAry['CaseID'] = $_caseID;
    $dataAry['EventID'] = $EventID;
    if (! in_array($_caseID, (array) $current_case)) {
        $sql = $objMedical->INSERT2TABLE('MEDICAL_CASE_EVENT', $dataAry, array(), false, false, false); // no DateModified field
        $result[] = $ldb->db_db_query($sql);
    }
}

// # step 9: add event student log
$currentStudentLogAry = $objMedical->getEventStudentLog($EventID);
$currentStudentLogAry = Get_Array_By_Key($currentStudentLogAry, 'StudentLogID');
foreach ((array) $selStudentLogID as $_studentLogID) {
    unset($dataAry);
    $dataAry['StudentLogID'] = $_studentLogID;
    $dataAry['EventID'] = $EventID;
    if (! in_array($_studentLogID, (array) $currentStudentLogAry)) {
        $sql = $objMedical->INSERT2TABLE('MEDICAL_EVENT_STUDENT_LOG', $dataAry, array(), false, false, false); // no DateModified field
        $result[] = $ldb->db_db_query($sql);
    }
}

// # step 10: add staff event related to student event
$currentStaffEventAry = $objMedical->getStaffEventStudentEvent('',$EventID);
$currentStaffEventAry = Get_Array_By_Key($currentStaffEventAry, 'StaffEventID');

foreach ((array) $SelStaffEventID as $_staffEventID) {
    unset($dataAry);
    $dataAry['EventID'] = $EventID;
    $dataAry['StaffEventID'] = $_staffEventID;
    if (! in_array($_staffEventID, (array) $currentStaffEventAry)) {
        $sql = $objMedical->INSERT2TABLE('MEDICAL_EVENT_STAFF_EVENT', $dataAry, array(), false, false, false); // no DateModified field
        $result['AddStudentEventRelStaffEvent_'.$_staffEventID] = $ldb->db_db_query($sql);
    }
}

if (! in_array(false, $result)) {
    $ldb->Commit_Trans();
    $returnMsgKey = 'UpdateSuccess';
} else {
    $ldb->RollBack_Trans();
    if ($uploadResult == 'FileSizeExceedLimit') {
        $returnMsgKey = 'FileSizeExceedLimit';
    } else {
        $returnMsgKey = 'UpdateUnsuccess';
    }
}

header("location: ?t=management.event_list&returnMsgKey=" . $returnMsgKey);

?>