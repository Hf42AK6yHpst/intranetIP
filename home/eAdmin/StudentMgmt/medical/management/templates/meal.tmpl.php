<?php 
// using:
?>
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tbody>
			<?php foreach( (array)$menuOption as $menuItem): ?>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $menuItem['Name']; ?></td>
				<td class="tabletext" width="70%"><?php echo $menuItem['Detail']; ?></td>
			</tr>
			<?php endforeach; ?>
			<tr>
				<td class="dotline" colspan="2">
					<img src="<?php echo $image_path."/".$LAYOUT_SKIN.'/10x10.gif'; ?>" width="10" height="1" />
				</td>	
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td align="center" colspan="2">
					<input type="button" id="Search" name="Search" class="formbutton_v30 print_hide " value="<?php echo $Lang['medical']['meal']['button']['search']; ?>">
				</td>	
			</tr>
		</tfoot>
	</table>
	
	<div id="viewResult">
	</div>
<script>
$(document).ready(function(){
	$('#Search').click(function(){
//		console.log('Event search activiated.');
		$.ajax({
			url : "?t=management.ajax.getMealList",
			type : "POST",
//			data : "date="+$('#date').val()+"&classID="+$('#classID').val()+"&sleep="+$('#sleep').val()+"&gender="+$('#gender').val(),
			data : {
				'date':$('#date').val(),
				'timePeriod':$('input[name="timePeriod"]:checked').val(),
				'classID': $('#classID').val(),
				'groupID': $('#groupID').val(),
				'sleep': $('#sleep').val(),
				'gender': $('#gender').val()
			},
			success : function(msg) {
				$('#viewResult').html(msg);
				$('.viewArea').each(function(){
					$(this).next().next().html($(this).html());
				});
			}
		});	
	});
});
</script>