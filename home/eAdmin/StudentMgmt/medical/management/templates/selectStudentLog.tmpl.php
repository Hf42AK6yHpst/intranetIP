<?php
/*
 * 2018-03-28 Cameron
 * - create this file
 */
$ldb = new libdb();

$currentAcademicYearID = Get_Current_Academic_Year_ID();
$classCond = '';
$cond = '';

$studentLogClassName = standardizeFormPostValue($_POST['studentLogClassName']);
$studentLogGroupID = $_POST['studentLogGroupID'];
$studentLogStudentID = $_POST['studentLogStudentID'];
$studentLogLev1Type = $_POST['studentLogLev1Type'];
$studentLogLev2Item = $_POST['studentLogLev2Item'];
$studentLogDate = $_POST['studentLogDate'];
$studentLogDateStart = $_POST['studentLogDateStart'];
$studentLogDateEnd = $_POST['studentLogDateEnd'];
$keyword = standardizeFormPostValue($_POST['keyword']);

if ($junior_mck) {
    $classNameField = "u.ClassName";
    $classNameEng = $classNameField;
    $joinClass = '';
} else {
    $classNameField = Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN");
    $classNameEng = "yc.ClassTitleEN";
    $joinClass = " INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=u.UserID
					INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='" . $currentAcademicYearID. "' ";
}

if ($studentLogGroupID) {
    $joinClass .= "	INNER JOIN INTRANET_USERGROUP g ON g.UserID=u.UserID AND g.GroupID='" . $studentLogGroupID. "'";
}

if ($studentLogClassName) {
    $cond .= " AND s.ClassName LIKE '%" . $ldb->Get_Safe_Sql_Like_Query($studentLogClassName) . "%'";
}

if ($studentLogStudentID) {
    $cond .= " AND e.UserID='" . $studentLogStudentID. "'";
}

if ($studentLogLev1Type) {
    $cond .= " AND e.BehaviourOne='" . $studentLogLev1Type. "'";
}

if ($studentLogLev2Item) {
    $cond .= " AND e.BehaviourTwo='" . $studentLogLev2Item. "'";
}


if ($studentLogDate) {
    if ($studentLogDateStart) {
        $cond .= " AND e.RecordTime>='" . $studentLogDateStart. " 00:00:00'";
    }
    
    if ($studentLogDateEnd) {
        $cond .= " AND e.RecordTime<='" . $studentLogDateEnd. " 23:59:59'";
    }
}

$studentName = getNameFieldByLang2('u.');


if ($keyword != "") {
    $kw = $ldb->Get_Safe_Sql_Like_Query($keyword);
    $cond .= " AND (s.Student LIKE '%$kw%' OR lev1.Lev1Name LIKE '%$kw%' OR lev2.Lev2Name LIKE '%$kw%')";
    unset($kw);
}

$sql = "SELECT 
    				DATE_FORMAT(e.RecordTime,'%Y-%m-%d&nbsp;&nbsp;&nbsp;%H:%i') AS RecordTime,
                    s.Student,
                    lev1.Lev1Name,
                    lev2.Lev2Name,
                    e.Remarks,
                    DATE_FORMAT(e.DateModified, '%Y-%m-%d&nbsp;&nbsp;&nbsp;%H:%i') AS DateModified,
    				CONCAT('<input type=\'checkbox\' name=\'studentLogID[]\' id=\'studentLogID[]\' value=\'', e.`RecordID`,'\'>') AS ChkStudentLogID
		FROM
    				MEDICAL_STUDENT_LOG e
		INNER JOIN (
				SELECT 	u.UserID,
                        $classNameEng as ClassName,
                        $studentName AS Student
				FROM 
						INTRANET_USER u
				" . $joinClass . "
			) AS s ON s.UserID=e.UserID
		LEFT JOIN 
                    MEDICAL_STUDENT_LOG_LEV1 lev1 ON lev1.Lev1ID=e.BehaviourOne
		LEFT JOIN 
                    MEDICAL_STUDENT_LOG_LEV2 lev2 ON lev2.Lev2ID=e.BehaviourTwo
		LEFT JOIN 
                    INTRANET_USER r ON r.UserID=e.ModifyBy
		WHERE 1 " . $cond . " ORDER BY e.DateModified DESC";
if (empty($cond)) {
    $sql .= " LIMIT 10";
}


$rs = $ldb->returnResultSet($sql);


$x .= '	<table class="common_table_list">
			<th width="1" class="tabletoplink">#</th>
			<th width="20%">' . $Lang['medical']['studentLog']['eventDate']. '</th>
			<th width="20%">' . $Lang['medical']['event']['tableHeader']['StudentName'] . '</th>
			<th width="12%">' . $Lang['medical']['studentLog']['studentLog']. '</th>
			<th width="10%">' .  $Lang['medical']['studentLog']['case']. '</th>
			<th width="25%">' .  $Lang['medical']['report_meal']['tableHeader']['remarks']. '</th>
			<th width="12%">' . $Lang['medical']['sleep']['tableHeader']['LastUpdated']. '</th>
			<th width="1"><input type="checkbox" name="checkmaster" onclick="(this.checked)?setChecked(1,this.form,\'studentLogID[]\'):setChecked(0,this.form,\'studentLogID[]\')"></th>
		';

if (count($rs)) {
    $i = 1;
    foreach ((array) $rs as $r) {
        $x .= '<tr>';
        $x .= '<td class="tableContent">' . $i . '</td>';
        $x .= '<td class="tabletext tablerow">' . $r['RecordTime'] . '</td>';
        $x .= '<td class="tabletext tablerow">' . $r['Student'] . '</td>';
        $x .= '<td class="tabletext tablerow">' . stripslashes(empty($r['Lev1Name']) ? '-' : $r['Lev1Name']) . '</td>';
        $x .= '<td class="tabletext tablerow">' . stripslashes(empty($r['Lev2Name']) ? '-' : $r['Lev2Name']) . '</td>';
        $x .= '<td class="tabletext tablerow">' . nl2br(stripslashes(empty($r['Remarks']) ? '-' : $r['Remarks'])) . '</td>';
        $x .= '<td class="tabletext tablerow">' . $r['DateModified'] . '</td>';
        $x .= '<td class="tabletext tablerow">' . $r['ChkStudentLogID'] . '</td>';
        $x .= '</tr>';
        $i ++;
    }
    $y = $i - 1;
} else {
    $x .= '<tr><td colspan="8" align="center">' . $i_no_search_result_msg . '</td></tr>';
    $y = 0;
}
$x .= '</table>';

?>