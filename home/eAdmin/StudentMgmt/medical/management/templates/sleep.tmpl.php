<?php 
// using: Pun
?>
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tbody>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $menuOption[0]['Name']; ?></td>
				<td class="tabletext" width="70%"><?php echo $menuOption[0]['Detail'] . $Lang['medical']['sleep']['toNextDay']; ?><span id='nextDay'></span></td>
			</tr>
			
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $menuOption[1]['Name']; ?></td>
				<td class="tabletext" width="70%"><?php echo $menuOption[1]['Detail']; ?></td>
			</tr>
			
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $menuOption[2]['Name']; ?></td>
				<td class="tabletext" width="70%"><?php echo $menuOption[2]['Detail']; ?></td>
			</tr>
			
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $menuOption[3]['Name']; ?></td>
				<td class="tabletext" width="70%"><?php echo $menuOption[3]['Detail']; ?></td>
			</tr>
			
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $menuOption[4]['Name']; ?></td>
				<td class="tabletext" width="70%"><?php echo $menuOption[4]['Detail']; ?></td>
			</tr>
			
			
			<tr>
				<td class="dotline" colspan="2">
					<img src="<?php echo $image_path."/".$LAYOUT_SKIN.'/10x10.gif'; ?>" width="10" height="1" />
				</td>	
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td align="center" colspan="2">
					<input type="button" id="Search" name="Search" class="formbutton_v30 print_hide " value="<?php echo $Lang['medical']['meal']['button']['search']; ?>">
					
					<div class="Conntent_tool" style="float:right">
						<div class="btn_option" id="ImportDiv">
						  <a id="btn_import" class="import" href="?t=management.studentSleepImport1"><?=$Lang['Btn']['Import']?></a>
						</div>
					</div>
				</td>	
			</tr>
		</tfoot>
	</table>
	
	<div id="viewResult">
		<?php
		if($date !=''){
			include_once('management/ajax/getSleepList.php');
		}
		?>
	</div>
<script>
$(document).ready(function(){
	$('#Search').click(function(){
//		console.log('Event search activiated.');
		$.ajax({
			url : "?t=management.ajax.getSleepList",
			type : "POST",
//			data : "date="+$('#date').val()+"&classID="+$('#classID').val()+"&sleep="+$('#sleep').val()+"&gender="+$('#gender').val(),
			data : {
				'date':$('#date').val(),
				'classID': $('#classID').val(),
				'groupID': $('#groupID').val(),
				'sleep': $('#sleep').val(),
				'gender': $('#gender').val()
			},
			success : function(msg) {
				$('#viewResult').html(msg);
				$('.viewArea').each(function(){
					$(this).next().next().html($(this).html());
				});
			}
		});	
	});
});

function changeNextDateDisplay(){
	var today = $('#date').val();
	var tomorrow = new Date();
	var parts = today.split('-');
	tomorrow.setFullYear(parts[0], parts[1]-1, parts[2]);
	tomorrow.setDate(tomorrow.getDate() + 1);

	var tomorrowDay = tomorrow.getDate();
	var tomorrowMonth = tomorrow.getMonth()+1;
	var tomorrowYear = tomorrow.getFullYear();
	if(tomorrowDay<10){
		tomorrowDay = '0' + tomorrowDay;
	}
	if(tomorrowMonth<10){
		tomorrowMonth = '0' + tomorrowMonth;
	}
	$('#nextDay').html(tomorrowYear+'-'+tomorrowMonth+'-'+tomorrowDay);
}
changeNextDateDisplay();
</script>