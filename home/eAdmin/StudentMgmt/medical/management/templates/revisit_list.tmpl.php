<?
/*
 * Log
 *
 * 2018-03-28 Cameron
 * - change button 'Submit' to 'View' in search toobar
 *
 * 2018-03-21 Cameron
 * - fix: javascript error in NotAllowToDeleteNonSelfRecord
 *
 * 2018-02-27 Cameron
 * - allow to delete self record only for non-super-admin user [case #F135176]
 *
 * 2017-09-01 Cameron
 * - create this file
 */
?>

<script>
function checkGet2(obj,url){
    obj.action=url;
    obj.submit();
}

$(document).ready(function(){
	$('#RevisitDate').click(function(){
		if ($(this).is(':checked')){
			$('#spanRequest').css('display','inline');
			$('#AcademicYearID').val('');
		}
		else {
			$('#spanRequest').css('display','none');
		}
	});

	$('#deleteButton').click(function(){
		var isPass = true;
		if ($(":input[name='RevisitID\[\]']:checked").length == 0) {
			alert(globalAlertMsg2);
			isPass = false;
		}
		else {
    		$(":input[name='RevisitID\[\]']:checked").each(function(){
    			if ($(this).attr('data-deleteAllow') == 0) {
    				alert("<?php echo $Lang['medical']['general']['Warning']['NotAllowToDeleteNonSelfRecord'];?>");	// please use " here as English version contains '
    				isPass = false;
    				return false;
    			}
    		});
		}
		
		if (isPass && confirm(globalAlertMsg3)){
			form1.action='?t=management.remove_revisit';                
            form1.method='POST';
            form1.submit();
		}
	});	
});

function changeAcademicYearFilter() {
	if ($('#AcademicYearID').val() != '' ) {
		$('#RevisitDate').attr('checked', false);
		$('#spanRequest').css('display','none');
	}
	$('#form1').submit();
}

</script>

<form name="form1" id="form1" method="POST"
	action="?t=management.revisit_list">
	<div class="content_top_tool">

		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="20%"><?=$toolbar ?></td>
				<td width="50%" align="center">&nbsp;</td>
				<td width="30%" style="float: right;">
					<div class="content_top_tool" style="float: right;">
						<?=$htmlAry['searchBox']?>     
						<br style="clear: both" />
					</div>
				</td>
			</tr>
		</table>

	</div>

	<div class="table_board">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr class="table-action-bar">
				<td valign="bottom">
					<div class="table_filter">
					<?=$yearFilter?>
					<?=$classFilter ?>
					<?=$groupFilter ?>
					<?=$studentFilter?>
					<?=$hospitalFilter?>
					<?=$divisionFilter?>
				</div> <br style="clear: both" />
					<div>
						<input type="checkbox" name="RevisitDate" id="RevisitDate"
							value="1" <?=$RevisitDate==1 ? "checked" : ""?>><label
							for="RevisitDate"><?=$Lang['medical']['filter']['FilterDateRange']?></label>
						<div id="spanRequest" style="position:relative;display:<?=$RevisitDate==1 ? "inline" : "none"?>">
							<label for="RevisitDateStart"><?=$i_From?></label> : <?=$linterface->GET_DATE_PICKER("RevisitDateStart",$RevisitDateStart)?>
					<label for="RevisitDateEnd"><?=$i_To ?></label> <?=$linterface->GET_DATE_PICKER("RevisitDateEnd",$RevisitDateEnd)?>
					<?=$linterface->GET_ACTION_BTN($Lang['Btn']['View'], 'submit',$ParOnClick="", $ParName="submit1")?>
						</div>
					</div>

				</td>
				<td valign="bottom">
					<div class="common_table_tool" style="width: 150px;">
						<a
							href="javascript:checkEdit(document.form1,'RevisitID[]','?t=management.edit_revisit')"
							class="tool_edit"><?=$button_edit ?></a> <a href="#"
							id='deleteButton' class="tool_delete"><?= $button_delete ?></a>
					</div>
				</td>
			</tr>
		</table>
	</div>

	<div class="table_board">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="bottom">

				<?= $li->display() ?>
 
			</td>
			</tr>
		</table>
	</div>

	<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
	<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
	<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
	<input type="hidden" name="page_size_change" value="" /> <input
		type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

</form>
<?
?>