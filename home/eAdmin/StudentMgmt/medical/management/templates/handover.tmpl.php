<?php 
/*
  * Using:
  *
  * 2019-07-24 Cameron
  *     - show Groups in PIC selection
  *
  * 2019-07-12 Caemron
  *     - change $Lang['Btn']['RemoveSelected'] to $Lang['Btn']['Remove']
  *
  * 2019-07-08 Cameron
  *     - prompt confirm message before delete attachment
  *
  * 2019-07-05 Cameron
  *     - add handover student [case #P150622]
  *
 *  2019-06-20 Cameron
 *      - add mandatory field checking (except time) before submit
 */

### Choose Member Btn
$btn_ChooseMember = $linterface->GET_SMALL_BTN($Lang['Btn']['Select'], "button", "newWindow('/home/common_choose/index.php?fieldname=target[]&page_title=SelectStudent&permitted_type=2&DisplayGroupCategory=1&Disable_AddGroup_Button=1&filterMedicalGroup=1&UserRecordStatus=0,1',16)");

### Auto-complete ClassName ClassNumber StudentName search
$UserClassNameClassNumberInput = '';
$UserClassNameClassNumberInput .= '<div style="float:left;">';
$UserClassNameClassNumberInput .= '<input type="text" id="UserClassNameClassNumberSearchTb" name="UserClassNameClassNumberSearchTb" value="" />';
$UserClassNameClassNumberInput .= '</div>';

### ClassName ClassNumber StudentName Textarea input
$UserClassNameClassNumberTextarea = '';
$UserClassNameClassNumberTextarea .= '<div style="float:left;" class="student_textarea">';
$UserClassNameClassNumberTextarea .= '<textarea style="height:100px;" id="UserClassNameClassNumberAreaSearchTb" name="UserClassNameClassNumberAreaSearchTb"></textarea><span id="textarea_studentmsg"></span><br><br>';
$UserClassNameClassNumberTextarea .= $linterface->GET_SMALL_BTN($Lang['AppNotifyMessage']['searchAndInsertStudent'], "button" , "");
$UserClassNameClassNumberTextarea .= '</div>';

### Auto-complete login search
//$UserLoginInput = '';
//$UserLoginInput .= '<div style="float:left;">';
//$UserLoginInput .= '<input type="text" id="UserLoginSearchTb" name="UserLoginSearchTb" value="" />';
//$UserLoginInput .= '</div>';

### Student Selection Box & Remove all Btn
$MemberSelectionBox = $linterface->GET_SELECTION_BOX($studentAry, "name='target[]' id='target' class='select_studentlist' size='15' multiple='multiple'", "");
$btn_RemoveSelected = $linterface->GET_SMALL_BTN($Lang['Btn']['Remove'], "button", "js_Remove_Selected_Student();");

$nextSelect = "";
$nextSelect .= "<table>";
$nextSelect .= '<tr>
                        <td class="tablerow2" valign="top">
                            <table width="100%" border="0" cellpadding="3" cellspacing="0">
                            <tr>
                                <td class="tabletext">'.$i_general_from_class_group.'</td>
                            </tr>
                            <tr>
                                <td class="tabletext">'.$btn_ChooseMember.'</td>
                            </tr>
                            <tr>
                                <td class="tabletext"><i>'.$Lang['General']['Or'].'</i></td>
                            </tr>
                            <tr>
                                <td class="tabletext">
                                    '.$Lang['AppNotifyMessage']['msgSearchAndInsertInfo'].'
                                    <br />
                                    '.$UserClassNameClassNumberTextarea.'
                                </td>
                            </tr>
                            </table>
                        </td>
                        <td class="tabletext" ><img src="'. $image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>
                        <td align="left" valign="top">
                            <table width="100%" border="0" cellpadding="5" cellspacing="0">
                            <tr>
                                <td align="left">
                                    '. $MemberSelectionBox .'
                                    '.$btn_RemoveSelected.'
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    '.$linterface->Get_Thickbox_Warning_Msg_Div("MemberSelectionWarningDiv", '* '.$Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['SelectionContainsMember']).'
                                </td>
                            </tr>
                            </table>
                        </td>
                    </tr>';
$nextSelect .= "</table>";

?>
<form name="form1" id='form1' action="?t=management.handoverSave" method="post" enctype="multipart/form-data">
	<?php echo $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>
	<div class="form_table">
		<table class="form_table_v30">
			<tr>
				<td class="field_title"><span class="tabletextrequire">*</span><?php echo $Lang['General']['Date']?></td>
				<td><?php echo $linterface->GET_DATE_PICKER("recordDate", $rt_Date);?></td>
			</tr>
			<tr>
				<td class="field_title"><span class="tabletextrequire">*</span><?php echo $Lang['General']['Time']?></td>
				<td><?php echo $linterface->Get_Time_Selection_Box("recordTime_Hour", "hour", $rt_Hour) . ' : ' . $linterface->Get_Time_Selection_Box("recordTime_Minute", "min", $rt_Mintue);?></td>
			</tr>
			<tr>
				<td class="field_title"><span class="tabletextrequire">*</span><?php echo $Lang['medical']['handover']['item']?></td>
				<td><?php echo $linterface->GET_SELECTION_BOX($itemArr, "id='recordItem' name='recordItem'", $Lang['General']['PleaseSelect'], $record['RecordItemID']);?></td>
			</tr>

            <tr valign='top'>
                <td class="field_title"><?php echo $Lang['medical']['handover']['studentName'];?></td>
                <td><?php echo $nextSelect;?></td>
            </tr>

			<tr>
				<td class="field_title"><?php echo $Lang['medical']['handover']['attachment']?></td>
				<td>
					<?php echo $existedAttachment;?>
					<input type='file' name="attachment[]" id="attachment_0" />
					<br />
					<button type="button" id="btnAddFile" onclick="addAttachment()"> + </button>
				</td>
			</tr>
			<tr>
				<td class="field_title"><?php echo $Lang['General']['Remark'];?></td>
				<td><?php echo $linterface->GET_TEXTAREA("remark", $record['Remark'])?></td>
			</tr>
			<tr>
				<td class="field_title"><span class="tabletextrequire">*</span><?php echo $Lang['medical']['handover']['pic'];?></td>
				<td>
					<table border='0' cellpadding='0' cellspacing='0'>
    					<tr>
    						<td class='tablerow2'>
        						<?php echo $Lang['AppNotifyMessage']['msgSearchAndInsertPICInfo']?>
        						<br />
        						<?php echo $UserPICTextarea ?>
    						</td>
    						<td class='tabletext' ><img src='<?php echo $image_path . '/' . $LAYOUT_SKIN; ?>/10x10.gif' width='10' height='1'></td>
    						<td><?php echo $linterface->GET_SELECTION_BOX($PICUser, "id='target_PIC' name='target_PIC[]' size='6' multiple", ""); ?></td>
    						<td valign='bottom'><?php echo $linterface->GET_BTN($Lang['Btn']['Select'], "button", "newWindow('/home/common_choose/index.php?fieldname=target_PIC[]&permitted_type=1&Disable_AddGroup_Button=1&DisplayGroupCategory=1', 16)") . "<br>" . $linterface->GET_BTN($Lang['Btn']['Remove'], "button", "checkOptionRemove(document.form1.elements['target_PIC[]'])");?></td>
    					</tr>
					</table>
				</td>
			</tr>
		</table>
		<?=$linterface->MandatoryField();?>
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?php echo $btnSubmit;?>
			<?php echo $btnBack;?>
			<p class="spacer"></p>
		</div>
	</div>
	<input type='hidden' name='recordID' value='<?php echo $recordID;?>' />
	<input type='hidden' name='action' value='<?php echo $action;?>' />
</form>
<?php
    echo $linterface->Include_AutoComplete_JS_CSS();
    echo $linterface->Include_JS_CSS();
?>
<script type="text/javascript">
    var AutoCompleteObj_ClassNameClassNumber;
	var attachmentCount = 1;
	function addAttachment(){
		var addBtn = $('#btnAddFile');
		addBtn.before('<input type="file" name="attachment[]" id="attachment_'+attachmentCount+'" />');
		addBtn.before('<br />');
		attachmentCount++;
	}
	function checkForm(){
		$('#target_PIC option').attr('selected',true);
		
		if($('#recordItem').val() == '') {
			alert('<?php echo $Lang['medical']['revisit']['SelectAtLeastOneItem'];?>');
			$('#recordItem').focus();
			return false;
		}
		else if (!check_date($('#recordDate')[0],"<?=$Lang['General']['InvalidDateFormat']?>")) {
	 		$('#recordDate').focus();
	 		return false;
	 	}
		else if (!$('#target_PIC').val()) {
			alert('<?php echo $Lang['medical']['handover']['noPICSelected'];?>');
	 		$('#target_PIC').focus();
	 		return false;
		}
		
		$('#form1').submit();
	}
	function deleteAttachment(id){
        var conf = confirm('<?=$Lang['General']['__Warning']['AreYouSureYouWouldLikeToDeleteThisFile']?>');
        if (conf) {
            $.post('?t=management.ajax.ajax_remove_handover_attachment', {"attachmentID": id}, function (result) {
                if (result)
                    $('#exAttachment_' + id).remove();
            });
        }
	}

    // function Init_JQuery_AutoComplete(InputID_ClassNameClassNumber) {
    //
    //     AutoCompleteObj_ClassNameClassNumber = $("input#" + InputID_ClassNameClassNumber).autocomplete(
    //         "?t=management.ajax.ajax_search_user_by_classname_classnumber",
    //         {
    //             onItemSelect: function(li) {
    //                 Add_Selected_User(li.extra[1], li.selectValue, InputID_ClassNameClassNumber);
    //             },
    //             formatItem: function(row) {
    //                 return row[0];
    //             },
    //             maxItemsToShow: 100,
    //             minChars: 1,
    //             delay: 0,
    //             width: 200
    //         }
    //     );
    //
    //     Update_Auto_Complete_Extra_Para();
    // }

    // function Add_Selected_User(UserID, UserName, InputID) {
    //     var UserID = UserID || "";
    //     var UserName = UserName || "";
    //
    //     var UserSelected = document.getElementById('target[]');
    //     UserSelected.options[UserSelected.length] = new Option(UserName, UserID);
    //
    //     Update_Auto_Complete_Extra_Para();
    //
    //     // reset and refocus the textbox
    //     $('input#' + InputID).val('').focus();
    // }

    // function Update_Auto_Complete_Extra_Para(){
    //     checkOptionAll(document.getElementById('form1').elements["target[]"]);
    //     ExtraPara = new Array();
    //     ExtraPara['SelectedUserIDList'] = Get_Selection_Value('target[]', 'Array', true);
    //     AutoCompleteObj_ClassNameClassNumber[0].autocompleter.setExtraParams(ExtraPara);
    // }

    function js_Remove_Selected_Student(){
        checkOptionRemove(document.getElementById('target'));
//        Update_Auto_Complete_Extra_Para();
    }

	var eFormAppJS = {
			vars: {
				ajaxURL: "?t=management.ajax.ajax_search_user_by_classname_classnumber",
				xhr: "",
				selectedUserContainer: "UserClassNameClassNumberSearchTb"
			},
			listener: {
				insertPICFromTextarea: function(e) {
					var textarea = $('div.pic_textarea textarea').eq(0);

					if (textarea.val() == "") {
						textarea.focus();
					} else {
						eFormAppJS.func.ajaxFromTextArea(textarea, "pic");
					}
				},
				insertFromTextarea: function(e) {
					var textarea = $('div.student_textarea textarea').eq(0);
					if (textarea.val() == "") {
						textarea.focus();
					} else {
						eFormAppJS.func.ajaxFromTextArea(textarea, "student");
					}
					e.preventDefault();
				}
			},
			func: {
				ajaxFromTextArea: function(textarea, contentType) {
					var targetSelectID = "target[]";
					var textareaMsgObj = '#textarea_studentmsg';
					switch (contentType) {
						case "pic":
							textareaMsgObj = '#textarea_msg';
							targetSelectID = "target_PIC";
							break;
					}
					if (textarea.val() == "") {
						textarea.focus();
						$(textareaMsgObj).css({"color":"#f00"}).html('<br>' + '<?php
		    
		    echo $Lang['eNotice']['WrongUserLogin'];
		    ?>');
					} else {
						var xhr = eFormAppJS.vars.xhr;
						if (xhr != "") {
							xhr.abort();
						}
						var textareaVal = textarea.val();
						var getTargetListVal = "";
						if (contentType == "pic") {
							$('select#' + targetSelectID + ' option').each(function(ndex, obj) {
								if (getTargetListVal != "") {
									getTargetListVal += ",";
								}
								getTargetListVal += $(this).val();
							});
						} else { 
							$('select[name="' + targetSelectID + '"] option').each(function(ndex, obj) {
								if (getTargetListVal != "") {
									getTargetListVal += ",";
								}
								getTargetListVal += $(this).val();
							});
						} 

						eFormAppJS.vars.xhr = $.ajax({
							url: eFormAppJS.vars.ajaxURL,
							type: 'get',
							data: { "q": textareaVal.split("\n").join("||"), "searchfrom":"textarea", "nt_userType" : contentType, "SelectedUserIDList" : getTargetListVal },
							error: function() {
								textarea.focus();
							},
							success: function(data) {
								var lineRec = data.split("\n");
								if (lineRec.length > 0) {
									var UserSelected = "";
									if (contentType == "pic") {
										UserSelected = $('#' + targetSelectID);
									} else {
										UserSelected = $('select[name="' + targetSelectID + '"]').eq(0);
									}
									// console.log(UserSelected);
									var afterTextarea = "";
									$.each(lineRec, function(index, data) {
										if (data != "") {
											var tmpData = data.split("||");
											if (typeof tmpData[0] != "undefined" && typeof tmpData[1] != "undefined" && tmpData[1] != "NOT_FOUND") {  
												// Add_Selected_User(tmpData[1], tmpData[0], eFormAppJS.vars.selectedUserContainer);
												// UserSelected.options[UserSelected.length] = new Option(tmpData[0], tmpData[1]);
												UserSelected.append($("<option></option>")
									                    .attr("value", tmpData[1])
									                    .text(tmpData[0])); 
												
											} else {
												if (afterTextarea != "") {
													afterTextarea += "\n";
												}
												afterTextarea += tmpData[2];									
											}
										}
									});
									if (afterTextarea != "") {
										textarea.val(afterTextarea);
										$(textareaMsgObj).css({"color":"#f00"}).html('<br>' + '<?php
		        
		        echo $Lang['eNotice']['WrongUserLogin'];
		        ?>');
									} else {
										textarea.val("");
										$(textareaMsgObj).html('');
									}
									checkOptionAll(document.getElementById('form1').elements[targetSelectID]);
								} else {
									textarea.focus();
									$(textareaMsgObj).css({"color":"#f00"}).html('<br>' + '<?php
		    
		    echo $Lang['eNotice']['WrongUserLogin'];
		    ?>');
								}
							}
						});
					}
				},
				init: function() {
					$('.form_table_v30 tr td tr td').css({"border-bottom":"0px"});

					$('div.student_textarea input[type="button"]').unbind('click', eFormAppJS.listener.insertFromTextarea);
					$('div.pic_textarea input[type="button"]').unbind('click', eFormAppJS.listener.insertPICFromTextarea);
					
					$('div.student_textarea input[type="button"]').bind('click', eFormAppJS.listener.insertFromTextarea);
					$('div.pic_textarea input[type="button"]').bind('click', eFormAppJS.listener.insertPICFromTextarea);
				}
			}
		};
	$(document).ready(function(){
        // initialize jQuery Auto Complete plugin
        eFormAppJS.func.init();
//        Init_JQuery_AutoComplete('UserClassNameClassNumberSearchTb');

		$('#form1').submit(function(){
			$('#target_PIC option[value=""]').remove();
			$('#target_PIC option').attr('selected','selected');
            $('#target option[value=""]').remove();
            $('#target option').attr('selected','selected');
		});
	});
</script>