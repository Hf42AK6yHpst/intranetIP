<?
/*
 * 	Log
 * 
 * 	2017-06-23 Cameron
 * 		- create this file
 */
 
	echo $linterface->Include_Thickbox_JS_CSS();
?>

<style>
.error_msg_hide{display:none;}
.error_msg_show{display:'block';color: #FF0000; font-weight: bold;}
#TB_window a {
    color: #2286C5 !important;
}
#TB_window a:hover {
    color: #ff0000 !important;
}
</style>

<script language="javascript">
$().ready( function(){
	$("#submitBtn_tb").click(function(){
		var empty = true;
		var error = 0;
		$("input.actionBtn").attr("disabled", "disabled");
		$("#form2 textarea").each(function(){
			if ($.trim($(this).val())!="") {
				empty = false;
			}
		});
		
<? if ($objMedical->is_use_plupload()): ?>	
	if(!jsIsFileUploadCompleted()) {
		error++;
		$('#FileWarnDiv_event_followup').html('<?=$Lang['medical']['upload']['jsWaitAllFilesUploaded']?>').show();
		setTimeout(hideFileWarningTimerHandler_thickbox,5000);
	}
<? else: ?>
		$('#FileName').val(document.getElementById('File').value);
<? endif;?>
		
		if (empty) {
			alert('<?=$Lang['medical']['event']['Warning']['InputFollowup']?>');
			$("input.actionBtn").attr("disabled", "");
			return;
		}
		else if (error) {
			$("input.actionBtn").attr("disabled", "");
			return;
		}
		else {
			$.ajax({
				dataType: "json",
				type: "POST",
				url: "?t=management.ajax.ajax&m=1&action=<?=$action?>",
				data : $("#form2").serialize(),		  
				success: <?=(($action=='newFollowupSave') ? 'add_followup_list' : 'update_followup_list')?>,
				error: show_ajax_error
			});
			
		}
		js_Hide_ThickBox();
	});			
});


function add_followup_list(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#FollowupList').append(ajaxReturn.html);
	}
	$("input.actionBtn").attr("disabled", "");
}	

function update_followup_list(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#ef_<?=$followupID?>').replaceWith(ajaxReturn.html);
	}
	$("input.actionBtn").attr("disabled", "");
}	

function hideFileWarningTimerHandler_thickbox() {
	$('#FileWarnDiv_event_followup').hide();
}


</script>

<form name="form2" id="form2" method="post" enctype="multipart/form-data">
	<table align="center" class="form_table_v30">
		<tr valign="top">
			<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['event']['Handling']?></td>
			<td class="tabletext"><?=$linterface->GET_TEXTAREA("Handling", $rs_followup['Handling'], 80, 10)?></td>
		</tr>

		<tr valign="top">
			<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['event']['StudentResponse']?></td>
			<td class="tabletext"><?=$linterface->GET_TEXTAREA("StudentResponse", $rs_followup['StudentResponse'], 80, 10)?></td>
		</tr>

		<tr valign="top">
			<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['event']['FollowupAction']?></td>
			<td class="tabletext"><?=$linterface->GET_TEXTAREA("FollowupAction", $rs_followup['FollowupAction'], 80, 10)?></td>
		</tr>

		<tr valign="top">
			<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['event']['Other']?></td>
			<td class="tabletext"><?=$linterface->GET_TEXTAREA("Other", $rs_followup['Other'], 80, 10)?></td>
		</tr>

		<!-- upload files -->
		<?=$objMedical->getAttachmentLayout('event_followup', $rs_followup['FollowupID'], $mode='edit', $plupload)?>

	</table>
	
<div id="editBottomDiv" class="edit_bottom_v30">
	<p class="spacer"></p>
		<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "", "submitBtn_tb", "", "0", "", "actionBtn")?>
		<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", "cancelBtn_tb", "", "0", "", "actionBtn")?>
	<p class="spacer"></p>
</div>
<?=$linterface->GET_HIDDEN_INPUT('EventID', 'EventID', $eventID)?>
<?=$linterface->GET_HIDDEN_INPUT('FollowupID', 'FollowupID', $followupID)?>

<? if($use_plupload):?>
	<input type="hidden" name="TargetFolder" id="TargetFolder" value="<?=$tempFolderPath?>">
<? else: ?>
	<input type="hidden" name="FileName" id="FileName" value="">
<? endif; ?>
	
</form>

