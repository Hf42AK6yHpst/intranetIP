<?php
/*
 * 
 * 2019-01-04 Cameron
 */
$ldb = new libdb();

$AcademicYearID = Get_Current_Academic_Year_ID();
$cond = '';

if ($StaffID) {
    $cond .= " AND s.UserID='" . $StaffID . "'";
}

if ($StaffEventTypeID) {
    $cond .= " AND e.EventTypeID='" . $StaffEventTypeID. "'";
}

if ($StaffEventTypeLev2ID) {
    $cond .= " AND e.EventTypeLev2ID='" . $StaffEventTypeLev2ID. "'";
}

if ($SrhLocationBuildingID) {
    $cond .= " AND e.LocationBuildingID='" . $SrhLocationBuildingID . "'";
}

if ($SrhLocationLevelID) {
    $cond .= " AND e.LocationLevelID='" . $SrhLocationLevelID . "'";
}

if ($SrhLocationID) {
    $cond .= " AND e.LocationID='" . $SrhLocationID . "'";
}

if ($StaffEventReporter) {
    $cond .= " AND e.LastModifiedBy='" . $StaffEventReporter. "'";
}

if ($StaffEventDate) {
    if ($StaffEventDateStart) {
        $cond .= " AND CONCAT(e.EventDate,' ',e.StartTime)>='" . $StaffEventDateStart . " " . $StaffEventTimeStartHour . ":" . $StaffEventTimeStartMin . ":00'";
    }
    
    if ($StaffEventDateEnd) {
        $cond .= " AND CONCAT(e.EventDate,' ',e.EndTime)<='" . $StaffEventDateEnd . " " . $StaffEventTimeEndHour . ":" . $StaffEventTimeEndMin . ":00'";
    }
}

$locationField = Get_Lang_Selection("NameChi", "NameEng");
$staffName = getNameFieldByLang('s.');

$keyword = standardizeFormPostValue($_POST['keyword']);
if ($keyword != "") {
    $kw = $ldb->Get_Safe_Sql_Like_Query($keyword);
    $cond .= " AND {$staffName} LIKE '%$kw%'";
    unset($kw);
}

$sql = "SELECT 
				e.EventDate,
				" . $staffName. " AS StaffName,
				DATE_FORMAT(e.StartTime,'%k:%i') as StartTime,
                CASE 
                    WHEN e.EventTypeID=0 AND e.EventTypeLev2ID=0 THEN '-'
                    WHEN e.EventTypeID=0 AND e.EventTypeLev2ID>0 THEN et2.EventTypeLev2Name
                    WHEN e.EventTypeID>0 AND e.EventTypeLev2ID=0 THEN et.EventTypeName
                    ELSE CONCAT(et.EventTypeName,' > ', et2.EventTypeLev2Name)
                END AS EventType,
				CASE 
					WHEN e.LocationBuildingID=0 AND e.LocationLevelID=0 AND e.LocationID=0 THEN '-'
					WHEN e.LocationBuildingID=0 AND e.LocationLevelID=0 AND e.LocationID<>0 THEN loc." . $locationField . "
					WHEN e.LocationBuildingID=0 AND e.LocationLevelID<>0 AND e.LocationID=0 THEN lvl." . $locationField . "
					WHEN e.LocationBuildingID=0 AND e.LocationLevelID<>0 AND e.LocationID<>0 THEN CONCAT(lvl." . $locationField . ",' > ',loc." . $locationField . ")
					WHEN e.LocationBuildingID<>0 AND e.LocationLevelID=0 AND e.LocationID=0 THEN b." . $locationField . "
					WHEN e.LocationBuildingID<>0 AND e.LocationLevelID=0 AND e.LocationID<>0 THEN CONCAT(b." . $locationField . ",' > ',loc." . $locationField . ")
					WHEN e.LocationBuildingID<>0 AND e.LocationLevelID<>0 AND e.LocationID=0 THEN CONCAT(b." . $locationField . ",' > ',lvl." . $locationField . ")
					ELSE CONCAT(b." . $locationField . ",' > ',lvl." . $locationField . ",' > ',loc." . $locationField . ")
				END as Location,
				e.Remark,
                e.DateModified,
				CONCAT('<input type=\'checkbox\' name=\'StaffEventID[]\' id=\'StaffEventID[]\' value=\'', e.`StaffEventID`,'\'>') as ChkEventID
		FROM
				MEDICAL_STAFF_EVENT e
		INNER JOIN 
				INTRANET_USER s ON s.UserID=e.StaffID
		LEFT JOIN MEDICAL_STAFF_EVENT_TYPE et ON et.EventTypeID=e.EventTypeID
        LEFT JOIN MEDICAL_STAFF_EVENT_TYPE_LEV2 et2 ON et2.EventTypeLev2ID=e.EventTypeLev2ID
		LEFT JOIN INVENTORY_LOCATION_BUILDING b ON b.BuildingID=e.LocationBuildingID
		LEFT JOIN INVENTORY_LOCATION_LEVEL lvl ON lvl.LocationLevelID=e.LocationLevelID
		LEFT JOIN INVENTORY_LOCATION loc ON loc.LocationID=e.LocationID
		WHERE 1 " . $cond . " ORDER BY e.EventDate DESC";
if (empty($cond)) {
    $sql .= " LIMIT 10";
}
$rs = $ldb->returnResultSet($sql);
// debug_pr($sql);
// debug_pr($rs);
$x .= '	<table class="common_table_list">
			<th width="1" class="tabletoplink">#</th>
			<th width="10%">' . $Lang['medical']['staffEvent']['tableHeader']['EventDate']. '</th>
			<th width="15%">' . $Lang['medical']['staffEvent']['tableHeader']['StaffName']. '</th>
			<th width="10%">' . $Lang['medical']['staffEvent']['tableHeader']['StartTime']. '</th>
			<th width="10%">' . $Lang['medical']['staffEvent']['tableHeader']['EventType']. '</th>
			<th width="15%">' . $Lang['medical']['staffEvent']['tableHeader']['Place']. '</th>
			<th width="20%">' . $Lang['medical']['staffEvent']['remark']. '</th>
			<th width="10%">' . $Lang['medical']['general']['dateModified']. '</th>
			<th width="1"><input type="checkbox" name="checkmaster" onclick="(this.checked)?setChecked(1,this.form,\'StaffEventID[]\'):setChecked(0,this.form,\'StaffEventID[]\')"></th>
		';

if (count($rs)) {
    $i = 1;
    foreach ((array) $rs as $r) {
        $x .= '<tr>';
        $x .= '<td class="tableContent">' . $i . '</td>';
        $x .= '<td class="tabletext tablerow">' . $r['EventDate'] . '</td>';
        $x .= '<td class="tabletext tablerow">' . $r['StaffName'] . '</td>';
        $x .= '<td class="tabletext tablerow">' . $r['StartTime'] . '</td>';
        $x .= '<td class="tabletext tablerow">' . stripslashes($r['EventType']) . '</td>';
        $x .= '<td class="tabletext tablerow">' . $r['Location'] . '</td>';        
        $x .= '<td class="tabletext tablerow">' . nl2br($r['Remark']) . '</td>';
        $x .= '<td class="tabletext tablerow">' . $r['DateModified'] . '</td>';
        $x .= '<td class="tabletext tablerow">' . $r['ChkEventID'] . '</td>';
        $x .= '</tr>';
        $i ++;
    }
    $y = $i - 1;
} else {
    $x .= '<tr><td colspan="9" align="center">' . $i_no_search_result_msg . '</td></tr>';
    $y = 0;
}
$x .= '</table>';

?>