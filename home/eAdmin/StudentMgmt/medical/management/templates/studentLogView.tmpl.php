<?php 
/*
 *  2018-03-29 Cameron
 *      - create this file
 */

$objlibMedical = new libMedical();
$defaultRemark = $objlibMedical->getDefaultRemarksArray($medical_cfg['general']['module']['studentlog']);

echo <<< REMARKS_INIT
	<script>
		var jRemarksArr=$defaultRemark;
	</script>
REMARKS_INIT;
?>
<style>

.eventClass td{
	vertical-align:top;
}
a{
	color:#2286C5;
	cursor:pointer;
}
.errorMsg{
	color:red;
}

.table_row_tool{
	float:none;
	display:inline-block;
	color:#2286C5 !important;
	cursor:pointer !important;	
}
.delete_event_div{
	float:right !important;
	display:block;
}
legend {
	color:gray;
	font-style:italic;
	font-size: 16px;
}
.defaultRemarkSpan img{
	vertical-align: top;
}

</style>

<table width="98%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tbody>
		<tr>
			<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
		</tr>
		<tr>
    		<td>
    			<table align="center" class="form_table_v30">
            		<tr valign="top">
            			<td width="20%" valign="top" nowrap="nowrap"
            				class="field_title"><?php echo $Lang['medical']['studentLog']['eventDate'];?></td>
            			<td class="tabletext"><?php echo $eventDate;?></td>
            		</tr>
            
            		<tr valign="top">
            			<td width="20%" valign="top" nowrap="nowrap"
            				class="field_title"><?php echo $Lang['Header']['Menu']['Class'];?></td>
            			<td class="tabletext"><?php echo $studentInfo['ClassName'] ?></td>
            		</tr>
            
            		<tr valign="top">
            			<td width="20%" valign="top" nowrap="nowrap"
            				class="field_title"><?php echo $Lang['Identity']['Student'];?></td>
            			<td class="tabletext"><?php echo $studentInfo['Name'].'('.$studentInfo['ClassNumber'].')' ?></td>
            		</tr>
    			</table>
    		</td>
		</tr>

		<tr valign="top">
			<td>
				<?php $objMedical->getEventLogInputUI($Id=$studentLogObj->getRecordID(), $linterface, $studentLogObj, -1, $isReadOnly=true);?>	
			</td>									
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<td height="1" class="dotline"><img
				src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10"
				height="1"></td>
		</tr>
		<tr>
			<td align="right">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="center"><span>
							<?= $linterface->GET_ACTION_BTN($button_close, "button", "js_Hide_ThickBox();","btnClose", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>									
						</span></td>
					</tr>
				</table>
			</td>
		</tr>
   	</tfoot>
   	
</table>

