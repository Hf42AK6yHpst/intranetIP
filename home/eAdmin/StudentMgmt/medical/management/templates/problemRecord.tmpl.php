<?php 
// using:
?>
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tbody>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['problemRecord']['searchMenu']['date']; ?></td>
				<td class="tabletext" width="70%"><?=$dateSelectionHTML?></td>
			</tr>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['problemRecord']['searchMenu']['type']; ?></td>
				<td class="tabletext" width="70%"><?=$typeSelectionHTML?></td>
			</tr>

			<tr>
				<td class="dotline" colspan="2">
					<img src="<?php echo $image_path."/".$LAYOUT_SKIN.'/10x10.gif'; ?>" width="10" height="1" />
				</td>	
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td align="center" colspan="2">
					<input type="button" id="Search" name="Search" class="formbutton_v30 print_hide " value="<?php echo $Lang['medical']['meal']['button']['search']; ?>">
				</td>	
			</tr>
		</tfoot>
	</table>
	
	<div id="viewResult">
	</div>
<script>
$(document).ready(function(){
	$.datepick.setDefaults({ maxDate: 0 }); // Cannot select the future date
	
	$('#Search').click(function(){
		if(!checkDateValid($('#startDate').val(),$('#endDate').val())){
			alert("<?php echo $Lang['medical']['report_bowel']['wrongDatePeriod']?>");
			return false;
		}
		$.ajax({
			url : "?t=management.ajax.getProblemRecord"+$('#type').val(),
			type : "POST",
			data : {
				'startdate':$('#startDate').val(),
				'enddate':$('#endDate').val()
			},
			success : function(msg) {
				$('#viewResult').html(msg);
				$('.viewArea').each(function(){
					$(this).next().next().html($(this).html());
				});
			}
		});	
	});

	function checkDateValid(start,end){
		// .replace() is for IE7 
		var _start = new Date(start.replace(/-/g,'/'));
		var _end = new Date(end.replace(/-/g,'/'));
		return start <= end;
	}
	<?php if($type != ''){ ?>
		$("#Search").click();
	<?php } ?>
});
</script>