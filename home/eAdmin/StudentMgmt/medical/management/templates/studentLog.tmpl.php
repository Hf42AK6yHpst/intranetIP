<?php 
// using: 
?>

<div class="table_board">
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tbody>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['meal']['searchMenu']['date']; ?></td>
			<td class="tabletext" width="70%"><?php echo  $linterface->GET_DATE_PICKER($Name="r_date",$DefaultValue=$date,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum"); ?></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['meal']['searchMenu']['form']; ?></td>
			<td class="tabletext" width="70%"><?php echo $objFCM_UI->Get_Class_Selection($AcademicYearID = Get_Current_Academic_Year_ID(), $YearID='', $ID_Name='classID', $SelectedYearClassID=$classID, $Onchange='', $noFirst=0, $isMultiple=0, $isAll=1, $TeachingOnly=0); ?></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['meal']['searchMenu']['group']; ?></td>
			<td class="tabletext" width="70%"><?php echo $objMedical->getGroupSelection($name='groupID',$defaultValue=$_GET['groupID']); ?></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['meal']['searchMenu']['sleep']; ?></td>
			<td class="tabletext" width="70%"><?php echo getSelectionBox($id="sleep", $name="sleep", $valueArray['sleep'], $valueSelected=$_GET['sleep'], $class=""); ?></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['report_meal']['searchMenu']['gender']; ?></td>
			<td class="tabletext" width="70%"><?php echo getSelectionBox($id="gender", $name="gender", $valueArray['gender'], $valueSelected=$gender, $class=""); ?></td>
		</tr>
		<tr>
			<td class="dotline" colspan="2">
				<img src="<?php echo $image_path."/".$LAYOUT_SKIN.'/10x10.gif'; ?>" width="10" height="1" />
			</td>	
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<td align="center" colspan="2">
				<input type="button" id="Search" name="Search" class="formbutton_v30 print_hide " value="<?php echo $Lang['medical']['meal']['button']['search']; ?>">
			
				<div class="Conntent_tool" style="float:right">
					<div class="btn_option" id="ImportDiv">
					  <a id="btn_import" class="import" href="?t=management.studentLogImport1"><?=$Lang['Btn']['Import']?></a>
					</div>
				</div>
			</td>	
		</tr>
	</tfoot>
</table>
</div>
<div id="viewResult">
<?php
	if($date !=''){
		include_once('management/ajax/getStudentLogList.php');
	}
?>
</div>
<script>
$(document).ready(function(){
	$('#Search').click(function(){
//		console.log('Event search activiated.');
//		console.log("date="+$('#date').val()+"&classID="+$('#classID').val()+"&sleep="+$('#sleep').val()+"&gender="+$('#gender').val());
		$.ajax({
			url : "?t=management.ajax.getStudentLogList",
			type : "POST",
//			data : "date="+$('#r_date').val()+"&classID="+$('#classID').val()+"&sleep="+$('#sleep').val()+"&gender="+$('#gender').val(),
			data : {
				'date':$('#r_date').val(),
				'classID': $('#classID').val(),
				'groupID': $('#groupID').val(),
				'sleep': $('#sleep').val(),
				'gender': $('#gender').val()
			},
			success : function(msg) {
				$('#viewResult').html(msg);
			}
		});	
	});
});
</script>