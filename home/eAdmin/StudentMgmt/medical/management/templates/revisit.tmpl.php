<?
/*
 * 	Log
 * 
 *  2019-06-20 Caemron
 *      - apply intranet_htmlspecialchars() to Hospital and Division field
 *      
 *  2018-12-27 Cameron
 *      - not allow to change student in edit mode
 *      
 * 	2017-10-13 Cameron
 * 		- support search student by group 
 */
?>
<style>
.error_msg_hide{display:none;}
.error_msg_show{display:'block';color: #FF0000; font-weight: bold;}
</style>

<script src="/templates/jquery/jquery.inputselect.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.inputselect.css" rel="stylesheet" type="text/css">

<script src="/templates/jquery/jquery.autocomplete.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.autocomplete.css" rel="stylesheet" type="text/css">

<script type="text/javascript">

//var hasDrugInit = [];		// for initialize inputselect in drug
var nrDrugRows = <?=$nrDrugs > 0 ? $nrDrugs : 0?>;
var isLoading = true;
var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image() ?>';

$(document).ready(function(){

	$('#ClassName').change(function(){
		changeClassName();
	});

	$('#GroupID').change(function(){
		changeGroup();
	});

	$('#ByMethod').change(function(){
		if ($('#ByMethod').val() == 'ByClass') {
			$('#GroupID').css('display','none');
			$('#ClassName').css('display','');
			changeClassName();			
		}
		else {
			$('#GroupID').css('display','');
			$('#ClassName').css('display','none');
			changeGroup();
		}
	});

	$('#btnSubmit').click(function(e){
	    e.preventDefault();
	    $('input.actionBtn').attr('disabled', 'disabled');
		
		if (checkForm()) {
			$('#form1').submit();
		}
		else {
			$('input.actionBtn').attr('disabled', '');	
		}
	});
	
	$("input.inputselect").inputselect({
		image_path: '/images/2009a/', 
		ajax_script: '?t=management.ajax.ajax_get_selection_list&ma=1', 
		js_lang_alert: {'no_records' : '<?=$Lang['General']['NoRecordAtThisMoment']?>' }
	});

	// autocomplete for inputselect fields
	$('.inputselect').each(function(){
		var this_id = $(this).attr('id');
		if($(this).length > 0){
			$(this).autocomplete(
		      "?t=management.ajax.ajax_get_target_suggestion&ma=1",
		      {
		  			delay:3,
		  			minChars:1,
		  			matchContains:1,
		  			extraParams: {'from':this_id},
		  			autoFill:false,
		  			overflow_y: 'auto',
		  			overflow_x: 'hidden',
		  			maxHeight: '200px'
		  		}
		    );
		}
	});
	
	$("input:radio[name='IsChangedDrug']").change(function(){
		var disabled = '';
		if ($("input:radio[name='IsChangedDrug']:checked").val() == '1') {
			$('#AddDrugDiv').css("display","");
			$('#DrugTable').css("display","");
			disabled = '';
			$('#DrugDeleteColTitle').css("display","");
		}
		else {
			$('#AddDrugDiv').css("display","none");
			<? if (!$revisitID):?>
				$('#DrugTable').css("display","none");
			<? endif;?>
			disabled = 'disabled';
			$('#DrugDeleteColTitle').css("display","none");
		}

	<? if ($revisitID):?>		
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '?t=management.ajax.ajax',
			data : {
				'action': 'getDrugTable',
				'RevisitID': '<?=$revisitID?>',
				'disabled': disabled
			},		  
			success: update_drug_table,
			error: show_ajax_error
		});
		
	<? endif;?>
	});
	
});

function checkForm() {
	var error = 0;
	var focusField = '';
	hideError();
	<?php if($sys_custom['medical']['MedicalReport']){?>
	if($('#RevisitDate').val() != ''){
	<?php }?>
     	if (!check_date($('#RevisitDate')[0],"<?=($Lang['General']['InvalidDateFormat'] . "(".$Lang['medical']['revisit']['RevisitDate'].")")?>")) {
     		error++;
     		if (focusField == '') focusField = 'RevisitDate';
     	}
    <?php if($sys_custom['medical']['MedicalReport']){?>
        if($('#RevisitDate').val() < $('#VisitDate').val() && error == 0){
    		alert('<?php echo $Lang['medical']['revisit']['RevisitDate'] . ' ' . $Lang['medical']['revisit']['CannotEarlier'] . ' ' . $Lang['medical']['revisit']['VisitDate']; ?>');
    		error++;
    		if (focusField == '') focusField = 'RevisitDate';
    	}
	}
	<?php }?>
	<?php if($sys_custom['medical']['MedicalReport']){?>
 	if (!check_date($('#VisitDate')[0],"<?=($Lang['General']['InvalidDateFormat'] . "(".$Lang['medical']['revisit']['VisitDate'].")")?>")) {
 		error++;
 		if (focusField == '') focusField = 'VisitDate';
 	}
 	if($('#CuringStatus').val() == ''){
 	 	alert('<?php echo $Lang['medical']['revisit']['Warning']['CuringStatusEmpty'] ?>');
 	 	error++;
 	 	if (focusField == '') focusField = 'CuringStatus';
 	}
 	<?php }?>
	
	if ($('#StudentID').val() == '') {
 		error++;
 		if (focusField == '') focusField = 'StudentID';
 		$('#ErrStudentID').addClass('error_msg_show').removeClass('error_msg_hide');
	}
	
	if ($("input:radio[name='IsChangedDrug']:checked").val() == '1') {
		$(":input[id^='drug_name_']").each(function(){
			if ($(this).val() == '') {
				error++;
				var this_id = ($(this).attr('id'));
				if (focusField == '') focusField = this_id;
				this_id = this_id.replace('drug_name_','');
				$('#ErrDrugName_'+this_id).addClass('error_msg_show').removeClass('error_msg_hide');
			}
		});
	
		$(":input[id^='dosage_']").each(function(){
			if (($(this).val() != '') && (isNaN($(this).val()))) {
				error++;
				var this_id = ($(this).attr('id'));
				if (focusField == '') focusField = this_id;
				this_id = this_id.replace('dosage_','');
				$('#ErrDosage_'+this_id).addClass('error_msg_show').removeClass('error_msg_hide');
				
			}
			else if ($(this).val() != '') {
				var this_id = ($(this).attr('id'));
				this_id = this_id.replace('dosage_','');
				if ($(":input[name^='drug_unit["+this_id+"]']:checked").length == 0) {
					error++;
					if (focusField == '') focusField = 'drug_unit_'+this_id+'_mg';
					$('#ErrUnit_'+this_id).addClass('error_msg_show').removeClass('error_msg_hide');
				}
			}
		});
	}
	
	if (error > 0) {
		if (focusField != '') {
			$('#' + focusField).focus();
		}
		return false
	}
	else {
		$("#DrugTable tr:not(:first)").each(function(){
			console.log(this.rowIndex);
			$(this).find("[type=radio]").attr('name', 'drug_unit['+(this.rowIndex-1)+']');
		})
						
		return true;
	}
}

function changeClassName() {
    isLoading = true;
	$('#StudentNameSpan').html(loadingImg);
	
	$.ajax({
		dataType: "json",
		type: "POST",
		url: '?t=management.ajax.ajax',
		data : {
			'action': 'getStudentNameByClass',
			'ClassName': $('#ClassName').val(),
			'onChange': 'showLastDrug()'
		},		  
		success: update_student_list,
		error: show_ajax_error
	});
}

function changeGroup() {
    isLoading = true;
	$('#StudentNameSpan').html(loadingImg);
	
	$.ajax({
		dataType: "json",
		type: "POST",
		url: '?t=management.ajax.ajax',
		data : {
			'action': 'getStudentNameByGroupID',
			'GroupID': $('#GroupID').val(),
			'onChange': 'showLastDrug()'
		},		  
		success: update_student_list,
		error: show_ajax_error
	});
}

function update_student_list(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#StudentNameSpan').html(ajaxReturn.html);
		isLoading = false;
	}
}

function update_last_drug(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#LastDrugRow').css("display","");
		$('#LastDrugContent').html(ajaxReturn.html);
	}
}

function update_drug_table(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#DrugTable tr:not(:first)').remove();
		$('#DrugTable').append(ajaxReturn.html);
		nrDrugRows = $("#DrugTable tr[id^='drug_row_']").length;
//		hasDrugInit = [];	// reset
		for (var i=0; i<nrDrugRows;i++) {
			InitInputSelect(i);
		}
	}
}

function show_ajax_error() {
	alert('<?=$Lang['medical']['general']['error']['ajax']?>');
}

function hideError() {
	$('.error_msg_show').each(function() {
		$(this).addClass('error_msg_hide').removeClass('error_msg_show');
	});	
}

function showLastDrug() {
	if ($('#StudentID').val()) {
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '?t=management.ajax.ajax',
			data : {
				'action': 'getLastDrugTable',
				'StudentID': $('#StudentID').val()
			},		  
			success: update_last_drug,
			error: show_ajax_error
		});
	}
	else {
		$('#LastDrugRow').css("display","none");
	}		
}

function add_drug() {
//	var rowID = $("#DrugTable tr[id^='drug_row_']").length > 0 ? $("#DrugTable tr[id^='drug_row_']").length : 0;
	var rowID = nrDrugRows;
	$.ajax({
		dataType: "json",
		async: false,			
		type: "POST",
		url: '?t=management.ajax.ajax&ma=1&action=getNewDrug&RowID='+rowID,
		data : {},		  
		success: function(ajaxReturn){
			if (ajaxReturn != null && ajaxReturn.success){
				$('#DrugTable').append(ajaxReturn.html);
				
				InitInputSelect(rowID);
								
				$('#drug_name_'+rowID).autocomplete(
			      "?t=management.ajax.ajax_get_target_suggestion&ma=1&from=Drug&q=",
			      {
			  			delay:3,
			  			minChars:1,
			  			matchContains:1,
			  			autoFill:false,
			  			overflow_y: 'auto',
			  			overflow_x: 'hidden',
			  			maxHeight: '200px',
			  			width:'200px'
			  		}
			    );
			    
			    nrDrugRows++;
			}
		},
		error: show_ajax_error
	});
}

function InitInputSelect(rowID){
	$(":input[id^='drug_name_']").each(function(){
		var this_id = $(this).attr('id');
		this_id = this_id.replace('drug_name_','');
//		if (typeof hasDrugInit[this_id] === 'undefined' || hasDrugInit[this_id] === null) {
		if (this_id == rowID) {

			$('#drug_name_'+this_id).inputselect({
				image_path: '/images/2009a/', 
				ajax_script: '?t=management.ajax.ajax_get_selection_list&ma=1', 
				js_lang_alert: {'no_records' : '<?=$Lang['General']['NoRecordAtThisMoment']?>' }
			});
//			hasDrugInit[this_id] = true;
		}
	});	
}

function remove_drug(rowID) {
	$('#drug_row_'+rowID).remove();
/*	
	var nr_rows = $("#DrugTable tr[id^='drug_row_']").length;
	if ((nr_rows > 1) && (rowID != nr_rows-1)) {	// not last row
		$('#drug_row_'+rowID).remove();
		var startIdx = parseInt(rowID) + 1;
		for (var i=startIdx; i<nr_rows; i++) {
			var j = i - 1;
			$('#drug_row_'+i + ' span.table_row_tool').html('<a class="delete" title="<?=$Lang['medical']['revisit']['RemoveDrug']?>" onClick="remove_drug(\''+j+'\')"></a>');
			var name = $('#drug_row_'+i + ' :radio[name$=\['+i+'\]]').attr('name');
			var idx = name.lastIndexOf('[');
			if (idx != -1) {
				var new_name = name.substr(0,idx);
				new_name = new_name + '['+ j +']';
				$('#drug_row_'+i + ' :radio[name$=\['+i+'\]]').attr('name', new_name);
			
				$('#drug_row_'+i).attr('id','drug_row_'+j);
				$('#ErrDrugName_'+i).attr('id','ErrDrugName_'+j);
				$('#ErrDosage_'+i).attr('id','ErrDosage_'+j);
				$('#ErrUnit_'+i).attr('id','ErrUnit_'+j);
				$('#drug_name_'+i).attr('id','drug_name_'+j);
				$('#drug_name_'+i+'_inputselect_div').attr('id','drug_name_'+j+'_inputselect_div');
				$('#drug_name_'+i+'_selection_list').attr('id','drug_name_'+j+'_selection_list');
				$('#drug_name_'+i+'_arrow').attr('id','drug_name_'+j+'_arrow');
				$('#drug_name_'+i+'_link').attr('id','drug_name_'+j+'_link');
				$('#dosage_'+i).attr('id','dosage_'+j);
				$('#drug_unit_'+i+'_mg').attr('id','drug_unit_'+j+'_mg');
				$('#drug_unit_'+i+'_ml').attr('id','drug_unit_'+j+'_ml');
				$('#drug_unit_'+i+'_prn').attr('id','drug_unit_'+j+'_prn');
				$('#label_unit_'+i+'_mg').attr('for','drug_unit_'+j+'_mg');
				$('#label_unit_'+i+'_ml').attr('for','drug_unit_'+j+'_ml');
				$('#label_unit_'+i+'_prn').attr('for','drug_unit_'+j+'_prn');
				$('#label_unit_'+i+'_mg').attr('id','label_unit_'+j+'_mg');
				$('#label_unit_'+i+'_ml').attr('id','label_unit_'+j+'_ml');
				$('#label_unit_'+i+'_prn').attr('id','label_unit_'+j+'_prn');
				
			}
		}	
	}
	else {	// last row
		$('#drug_row_'+rowID).remove();
	}
	
	hasDrugInit = [];	// reset
	nr_rows = $("#DrugTable tr[id^='drug_row_']").length;	// update new number of rows
	for (var i=0; i < nr_rows; i++) {
		hasDrugInit[i] = true;
	}
*/		
}


</script>

<form name="form1" id="form1" method="post" action="<?=$form_action?>" enctype="multipart/form-data">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" class="form_table_v30">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['Identity']['Student']?>
								<?php if (!$revisitID):?>
									<span class="tabletextrequire">*</span>
								<?php endif;?>
								</td>
								<td class="tabletext">
								<?php if ($revisitID):?>
									<?php echo $studentInfo;?>
									<input type="hidden" name="StudentID" value="<?php echo $rs_revisit['StudentID'];?>">
								<?php else:?>
									<span>
									<select name="ByMethod" id="ByMethod">
										<option value="ByClass"><?=$Lang['Header']['Menu']['Class']?></option>
										<option value="ByGroup"><?=$Lang['medical']['meal']['searchMenu']['group']?></option>
									</select>
									</span>&nbsp;<span id="ByMethodSpan"><?=$classSelection?><?=$groupSelection?></span> &nbsp;
									<span><?=$Lang['medical']['revisit']['StudentName']?></span>&nbsp;<span id="StudentNameSpan"><?=$studentSelection?></span>
									<span class="error_msg_hide" id="ErrStudentID"><?=$Lang['General']['JS_warning']['SelectStudent']?></span>
								<?php endif;?>
								</td>
							</tr>
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?php echo $Lang['medical']['revisit']['RevisitDate']; if(!$sys_custom['medical']['MedicalReport']){ ?><span class="tabletextrequire">*</span><?php }?></td>
								<td class="tabletext"><?=$linterface->GET_DATE_PICKER("RevisitDate",((empty($rs_revisit['RevisitDate']) || $rs_revisit['RevisitDate'] == '0000-00-00')?'':$rs_revisit['RevisitDate']),$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1)?></td>
								</td>
							</tr>

							<tr valign="top" id="LastDrugRow" style="display:<?=$revisitID?'':'none'?>">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['revisit']['LastChangedDrug']?></td>
								<td id="LastDrugContent"><?=$rs_revisit['LastDrugTable']?></td>
								</td>
							</tr>

							<tr valign="top">
								<td colspan="2"><?=$Lang['medical']['revisit']['ThisRevisitInfo']?></td>
							</tr>
							
							<!-- New Customized Columns -->
							<?php if($sys_custom['medical']['MedicalReport']){?>
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['revisit']['VisitDate']?>
								<td class="tabletext"><?=$linterface->GET_DATE_PICKER("VisitDate",((empty($rs_revisit['VisitDate']) || $rs_revisit['VisitDate'] == '0000-00-00')?'':$rs_revisit['VisitDate']),$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0)?></td>
								</td>
							</tr>
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['revisit']['AccidentInjure']?></td>
								<td class="tabletext"><?=$linterface->Get_Checkbox("AccidentInjure", "AccidentInjure", '1', $rs_revisit['AccidentInjure'])?></td>
							</tr>
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['revisit']['DischargedAdmittion']?></td>
								<td class="tabletext"><?=$linterface->Get_Checkbox("DischargedAdmittion", "DischargedAdmittion", '1', $rs_revisit['DischargedAdmittion'])?></td>
							</tr>
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['revisit']['Diagnosis']?></td>
								<td class="tabletext"><?=$linterface->GET_TEXTAREA("Diagnosis", $rs_revisit['Diagnosis'], 80, 10)?></td>
							</tr>
							<?php }?>
							<!-- End of New Customized Columns -->
							
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['revisit']['IsChangedDrug']?></td>
								<td class="tabletext">
									<input type="radio" name="IsChangedDrug" id="IsChangedDrugYes" value="1"><label for="IsChangedDrugYes"><?=$Lang['General']['Yes']?></label>								
									<input type="radio" name="IsChangedDrug" id="IsChangedDrugNo" value="0" checked><label for="IsChangedDrugNo"><?=$Lang['General']['No']?></label>
									
									<div id="RevisitDrugDiv">
										<table id="DrugTable" class="common_table_list" style="display:<?=($revisitID?'':'none')?>">
											<tr id="drug_title" class="tabletop">
												<th width="55%"><?=$Lang['medical']['revisit']['Drug']?></th>
												<th width="40%"><?=$Lang['medical']['revisit']['Dosage']?></th>
												<th width="5%" id="DrugDeleteColTitle" style="display:none"></th>
											</tr>
											<?=$rs_revisit['DrugTable']?>
										</table>									
									</div>
									<div id="AddDrugDiv" style="display:none">
										<span class="table_row_tool"><a class="newBtn add" onclick="add_drug()" title="<?=$Lang['medical']['revisit']['AddDrug']?>"></a></span>
									</div>
									
								</td>
							</tr>
							
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['revisit']['CuringStatus']?><?php if($sys_custom['medical']['MedicalReport']){ ?><span class="tabletextrequire">*</span><?php }?> </td></td>
								<td class="tabletext"><?=$linterface->GET_TEXTAREA("CuringStatus", $rs_revisit['CuringStatus'], 80, 10)?></td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['revisit']['Hospital']?></td>
								<td><input id="Hospital" name="Hospital" type="text" class="textboxtext inputselect" value="<?=intranet_htmlspecialchars($rs_revisit['Hospital'])?>"/></td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['revisit']['Division']?></td>
								<td><input id="Division" name="Division" type="text" class="textboxtext inputselect" value="<?=intranet_htmlspecialchars($rs_revisit['Division'])?>"/></td>
							</tr>

							<tr>
								<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
								<td width="80%">&nbsp;</td>
							</tr>
							
						</table>
						
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<span>
									<?= $linterface->GET_ACTION_BTN($button_submit, "button","","btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_reset, "reset","","btnReset", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='?t=management.revisit_list'","btnCancel", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
									</span></td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<input type=hidden id="RevisitID" name="RevisitID" value="<?=$revisitID?>">

</form>
