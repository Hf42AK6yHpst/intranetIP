<?php
/*
 * 2018-12-14 Cameron
 * - create this file
 */
echo $linterface->Include_Thickbox_JS_CSS();
echo '<script language="JavaScript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery-1.8.3.min.js"></script>';
?>

<style>
.error_msg_hide {
	display: none;
}

.error_msg_show {
	display: 'block';
	color: #FF0000;
	font-weight: bold;
}

.followup_list {
	margin: 5px;
	padding: 0.2em;
	border: 1px solid #c6cc42;
	color: #222222;
}

.followup_list_header {
	background: #c6cc42 url(<?=$PATH_WRT_ROOT?>images/space.gif) repeat-x
		scroll 50% 50%;
	border: 1px solid #c6cc42;
	color: #000000;
	font-weight: bold;
	padding: 3px;
}
</style>

<script type="text/javascript">
var isLoading = true;
var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image() ?>';


$(document).ready(function(){

	$(document).on('change', '#TeacherType', function(){	
		isLoading = true;
		$('#StaffIDSpan').html(loadingImg);
		
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '?t=management.ajax.ajax&ma=1',
			data : {
				'action': 'getStaffList',
				'teacherType': $(this).val()
			},		  
			success: updateStaffIDList,
			error: show_ajax_error
		});
	});
	
	
 	$('#StaffEventTypeID').change(function(){
 		hideError();
		if ($('#StaffEventTypeID').val() == '') {
			$('#StaffEventTypeLev2ID').val('');
			$('#StaffEventTypeLev2Span').html('');
		}
		else {
    		isLoading = true;
    		$('#StaffEventTypeLev2Span').html(loadingImg);
     	 	
    		$.ajax({
    			dataType: "json",
    			type: "POST",
    			url: '?t=management.ajax.ajax&ma=1',
    			data : {
    				'action': 'getStaffEventTypeLev2Selection',
    				'StaffEventTypeID':$('#StaffEventTypeID').val()
    			},		  
    			success: updateStaffEventTypeLev2Selection,
    			error: show_ajax_error
    		});
		}
 	});

 	
	$('#StaffEventLocationBuildingID').change(function(){
		if ($('#StaffEventLocationBuildingID').val() == '') {
			$('#StaffEventLocationLevelID').val('');
			$('#StaffEventLocationID').val('');
			$('#BuildingLevelSpan').html('');
			$('#LocationSpan').html('');
		}
		else {
			$.ajax({
				dataType: "json",
				type: "POST",
				url: '?t=management.ajax.ajax&ma=1',
				data : {
					'action': 'getStaffEventBuildingLevel',
					'StaffEventLocationBuildingID': $('#StaffEventLocationBuildingID').val()
				},		  
				success: update_building_level,
				error: show_ajax_error
			});
		}
	});

	$('#btnSubmit').click(function(e){
	    e.preventDefault();
	    $('input.actionBtn').attr('disabled', 'disabled');
		
		if (checkForm()) {
			$('#form1').submit();
		}
		else {
			$('input.actionBtn').prop('disabled', false);	
		}
	});


	$('#addStudentEventBtn').click(function(){
		tb_show('<?=$Lang['medical']['staffEvent']['SelectEvent']?>','?t=management.selectEventForStaffEvent&height=500&width=800');
	});

});

function checkForm() 
{
	var error = 0;
	var focusField = '';
	hideError();
 	if (!check_date($('#StaffEventDate')[0],"<?=$Lang['General']['InvalidDateFormat']?>")) {
 		error++;
 		if (focusField == '') focusField = 'StaffEventDate';
 	}
 	
	if($('#StaffID').val()=='') {    
 		error++;
 		if (focusField == '') focusField = 'StaffID';
 		$('#ErrStaffID').addClass('error_msg_show').removeClass('error_msg_hide');
 	}
	if($('#StaffEventTypeID').val()=='')  {    
 		error++;
 		if (focusField == '') focusField = 'StaffEventTypeID';
 		$('#ErrStaffEventType').addClass('error_msg_show').removeClass('error_msg_hide');
 	}
 	
 	if ((($('#EndTimeHour').val() != '00') || ($('#EndTimeMin').val() != '00')) && (($('#EndTimeHour').val()+''+$('#EndTimeMin').val()) < ($('#StartTimeHour').val()+''+$('#StartTimeMin').val()))) {
 		error++;
 		if (focusField == '') focusField = 'EndTimeHour';
 		$('#ErrEndTime').addClass('error_msg_show').removeClass('error_msg_hide');
 	}

	if (($('#InjuryLeave').val() != '') && (isNaN($('#InjuryLeave').val()))) {
		error++;
		if (focusField == '') focusField = 'InjuryLeave';
		$('#ErrInjuryLeave').addClass('error_msg_show').removeClass('error_msg_hide');
	}

	if (($('#SickLeave').val() != '') && (isNaN($('#SickLeave').val()))) {
		error++;
		if (focusField == '') focusField = 'SickLeave';
		$('#ErrSickLeave').addClass('error_msg_show').removeClass('error_msg_hide');
	}
	
<? if ($objMedical->is_use_plupload()): ?>	
	if(!jsIsFileUploadCompleted()) {
		error++;
		$('#FileWarnDiv_event').html('<?=$Lang['medical']['upload']['jsWaitAllFilesUploaded']?>').show();
		setTimeout(hideFileWarningTimerHandler,5000);
	}
<? else: ?>
		$('#FileName').val(document.getElementById('File').value);
<? endif;?>

	if (error > 0) {
		if (focusField != '') {
			$('#' + focusField).focus();
		}
		return false
	}
	else {		
		return true;
	}
}

function hideError() 
{
	$('.error_msg_show').each(function() {
		$(this).addClass('error_msg_hide').removeClass('error_msg_show');
	});	
}

function changeStaffEventBuildingLevel() 
{
	if ($('#StaffEventLocationLevelID').val() == '') {
		$('#StaffEventLocationID').val('');
		$('#LocationSpan').html('');
	}
	else {
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '?t=management.ajax.ajax&ma=1',
			data : {
				'action': 'getStaffEventLocation',
				'StaffEventLocationLevelID': $('#StaffEventLocationLevelID').val()
			},		  
			success: update_location,
			error: show_ajax_error
		});
	}
	
}

function update_building_level(ajaxReturn) 
{
	if (ajaxReturn != null && ajaxReturn.success){
		$('#BuildingLevelSpan').html(ajaxReturn.html);
		$('#StaffEventLocationID').val('');
		$('#LocationSpan').html('');
	}
}	

function update_location(ajaxReturn) 
{
	if (ajaxReturn != null && ajaxReturn.success){
		$('#LocationSpan').html(ajaxReturn.html);
	}
}	

function show_ajax_error() 
{
	alert('<?=$Lang['medical']['general']['error']['ajax']?>');
}

function hideFileWarningTimerHandler() 
{
	$('#FileWarnDiv_event').hide();
}

function FileDeleteSubmit(table, fileID) 
{
	conf = confirm('<?=$Lang['General']['__Warning']['AreYouSureYouWouldLikeToDeleteThisFile']?>');
	if (conf) {
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '?t=management.ajax.ajax',
			data : {
				'action': 'removeAttachment',
				'table': table,
				'FileID': fileID
			},		  
			success: function(ajaxReturn){
				if (ajaxReturn != null && ajaxReturn.success){
					$('#current_attachment_' + table + '_edit').replaceWith(ajaxReturn.html);
				}
				
			},
			error: show_ajax_error
		});
	}
}

function updateStaffIDList(ajaxReturn) 
{
	if (ajaxReturn != null && ajaxReturn.success){
		$('#StaffIDSpan').html(ajaxReturn.html);
		isLoading = false;
	}
}

function updateStaffEventTypeLev2Selection(ajaxReturn) 
{
	if (ajaxReturn != null && ajaxReturn.success){
		$('#StaffEventTypeLev2Span').html(ajaxReturn.html);
		isLoading = false;
	}
}

function view_event(eventID) {
	tb_show('<?=$Lang['medical']['case']['ViewEvent']?>','?t=management.event_view&EventID='+eventID+'&ViewMode=1&height=500&width=800');
}

function remove_student_event_of_staff_event(staffEventID, studentEventID)
{
	conf = confirm('<?=$Lang['medical']['staffEvent']['Warning']['ConfirmDeleteStudentEventRelStaff']?>');
	if (conf) {
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '?t=management.ajax.ajax',
			data : {
				'action': 'removeStudentEventOfStaffEvent',
				'StudentEventID': studentEventID,
				'StaffEventID': staffEventID
			},		  
			success: function(ajaxReturn){
				if (ajaxReturn != null && ajaxReturn.success){
					$('#student_event_row_'+studentEventID).remove();
					if ($('#relevantStudentEventTable tr').length == 1) {
						$('#relevantStudentEventTable').css("display","none");
					}
				}
			},
			error: show_ajax_error
		});
	}
}

</script>

<form name="form1" id="form1" method="post" action="<?=$form_action?>"
	enctype="multipart/form-data">
	<table width="98%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td align="center">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
					</tr>
				</table>
				<table width="100%" border="0" cellpadding="5" cellspacing="0">
					<tr>
						<td>
							<table align="center" class="form_table_v30">
								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['Identity']['Staff']?>
									<?php if (!$staffEventAry['StaffEventID']): ?>
										<span class="tabletextrequire">*</span>
									<?php endif;?>
									</td>
									<td class="tabletext">
									<?php if ($staffEventAry['StaffEventID']): ?>
										<?php echo $staffEventAry['StaffName'];?>
										<input type="hidden" name="TeacherType" value="<?php echo $staffEventAry['Teaching'];?>">
										<input type="hidden" name="StaffID" value="<?php echo $staffEventAry['StaffID'];?>">
									<?php else:?>
										<?php echo $objMedical->getStaffSelection($staffEventAry['Teaching'], $staffEventAry['StaffID']);?>
										<span class="error_msg_hide" id="ErrStaffID"><?=$Lang['medical']['staffEvent']['Warning']['SelectStaff']?></span>
									<?php endif;?>	
									</td>
								</tr>
								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['staffEvent']['EventDate']?><span
										class="tabletextrequire">*</span></td>
									<td class="tabletext"><?=$linterface->GET_DATE_PICKER("StaffEventDate",(empty($staffEventAry['EventDate'])?date('Y-m-d'):$staffEventAry['EventDate']))?></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['staffEvent']['StartTime']?>
									</td>
									<td class="tabletext"><?=getTimeSel('StartTime', empty($staffEventAry['StartTime'])?0:substr($staffEventAry['StartTime'],0,2), empty($staffEventAry['StartTime'])?0:substr($staffEventAry['StartTime'],3,2), "", $minuteStep=1)?></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['staffEvent']['EndTime']?>
									</td>
									<td class="tabletext"><?=getTimeSel('EndTime', empty($staffEventAry['EndTime'])?0:substr($staffEventAry['EndTime'],0,2), empty($staffEventAry['EndTime'])?0:substr($staffEventAry['EndTime'],3,2), "", $minuteStep=1)?>
									<span class="error_msg_hide" id="ErrEndTime"><?=$Lang['medical']['event']['Warning']['StartTimeGtEndTime']?></span></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['staffEvent']['EventType']?><span
										class="tabletextrequire">*</span></td>
									<td class="tabletext"><?=$staffEventTypeSelection?><span
										id="StaffEventTypeLev2Span"><?=$staffEventTypeLev2Selection?></span><span
										class="error_msg_hide" id="ErrStaffEventType"><?=$Lang['medical']['staffEvent']['Warning']['SelectEventType']?></span></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['staffEvent']['Place']?></td>
									<td class="tabletext"><?=$buildingSelection?>&nbsp;<span
										id="BuildingLevelSpan"><?=$levelSelection?></span>&nbsp;<span
										id="LocationSpan"><?=$locationSelection?></span></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['staffEvent']['HasReportedInjury']?></td>
									<td class="tabletext"><input type="checkbox" name="IsReportedInjury" id="IsReportedInjury" value="1" <?php if ($staffEventAry['IsReportedInjury']) echo 'checked';?>>
										<label for="IsReportedInjury"><?php echo $Lang['General']['Yes'];?></label>
									</td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['staffEvent']['DaysOfInjuryLeave']?></td>
									<td class="tabletext"><input type="text" name="InjuryLeave" id="InjuryLeave" value="<?php echo $staffEventAry['InjuryLeave'];?>" class="textboxnum" maxlength="8">
										<span class="error_msg_hide" id="ErrInjuryLeave"><?=$Lang['medical']['staffEvent']['Warning']['PleaseInputNumeric']?></span>										
									</td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['staffEvent']['DaysOfSickLeave']?></td>
									<td class="tabletext"><input type="text" name="SickLeave" id="SickLeave" value="<?php echo $staffEventAry['SickLeave'];?>" class="textboxnum" maxlength="8">
										<span class="error_msg_hide" id="ErrSickLeave"><?=$Lang['medical']['staffEvent']['Warning']['PleaseInputNumeric']?></span>										
									</td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['staffEvent']['remark']?></td>
									<td class="tabletext"><?=$linterface->GET_TEXTAREA("Remark", $staffEventAry['Remark'], 80, 10)?></td>
								</tr>


<?php if ($plugin['medical_module']['discipline']) :?>
								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['staffEvent']['RelevantEvent']?></td>
									<td class="tabletext">
										<table width="100%" id="relevantStudentEventTable" class="common_table_list" style="display:<?=$relevantStudentEvents ? '':'none'?>">
											<tr>
												<th width="15%"><?=$Lang['medical']['event']['EventDate']?></th>
												<th width="10%"><?=$Lang['medical']['event']['tableHeader']['StartTime']?></th>
												<th width="15%"><?=$Lang['medical']['staffEvent']['StudentName']?></th>
												<th width="15%"><?=$Lang['medical']['event']['EventType']?></th>
												<th width="44%"><?=$Lang['medical']['event']['Summary']?></th>
												<th>&nbsp;</th>
											</tr>
											<?=$relevantStudentEvents?>
										</table>

										<div>
											<span class="table_row_tool"><a class="newBtn add" id="addStudentEventBtn" title="<?=$Lang['medical']['staffEvent']['LinkRelevantEvent']?>"></a></span>
										</div>

									</td>
								</tr>
<?php endif;?>								
								<!-- upload files -->
							<?=$objMedical->getAttachmentLayout('staff_event', $staffEventAry['StaffEventID'], $mode='edit', $plupload)?>
							
						</table>

							<table align="center" class="form_table_v30">
								<tr>
									<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
									<td width="80%">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td height="1" class="dotline"><img
							src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10"
							height="1"></td>
					</tr>
					<tr>
						<td align="right">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td align="center"><span>
									<?= $linterface->GET_ACTION_BTN($button_submit, "button","","btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_reset, "reset","","btnReset", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='?t=management.staffEventList'","btnCancel", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
									</span></td>
								</tr>
							</table>
						</td>
					</tr>
				</table> <br>
			</td>
		</tr>
	</table>
	<input type=hidden id="StaffEventID" name="StaffEventID" value="<?=$staffEventAry['StaffEventID']?>">

<? if($use_plupload):?>
	<input type="hidden" name="TargetFolder" id="TargetFolder"
		value="<?=$tempFolderPath?>">
<? else: ?>
	<input type="hidden" name="FileName" id="FileName" value="">
<? endif; ?>

</form>
