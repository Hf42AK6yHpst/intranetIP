<?
	echo $linterface->Include_Thickbox_JS_CSS();
?>

<script type="text/javascript">

function edit_performance(schemeID,groupID) {
//	tb_show('<?=$Lang['medical']['award']['EditPerformance']?>','?t=management.edit_performance&SchemeID='+schemeID+'&GroupID='+groupID+'&height=500&width=800');
	load_dyn_size_thickbox_ip('<?=$Lang['medical']['award']['EditPerformance']?>', 'onloadPerformanceThickbox('+schemeID+','+groupID+');', inlineID='', defaultHeight=450, defaultWidth=800);
}

function onloadPerformanceThickbox(schemeID,groupID) {
	$('div#TB_ajaxContent').load(
		"?t=management.edit_performance", 
		{ 
			SchemeID: schemeID,
			GroupID: groupID
		},
		function(ReturnData) {
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	);
}

function Show_Edit_Background(Obj) {
	Obj.style.backgroundImage = "url(<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/icon_edit_b.gif)";
	Obj.style.backgroundPosition = "center right";
	Obj.style.backgroundRepeat = "no-repeat";

	if (Obj.parentNode.parentNode.width == '') {
		Obj.parentNode.parentNode.width = parseInt(Obj.parentNode.parentNode.clientWidth) + 20;
	}
	else {
		Obj.parentNode.parentNode.width = parseInt(Obj.parentNode.parentNode.width) + 20;
	}
	 
}

function Hide_Edit_Background(Obj) {
	Obj.style.backgroundImage = "";
	Obj.style.backgroundPosition = "";
	Obj.style.backgroundRepeat = "";
	
	if (Obj.parentNode.parentNode.width != '') {
		Obj.parentNode.parentNode.width = parseInt(Obj.parentNode.parentNode.width) - 20;
	}
	else {
		Obj.parentNode.parentNode.width = parseInt(Obj.parentNode.parentNode.clientWidth) - 20;
	}
}

</script>

<form name="form1" id="form1" method="post">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" class="form_table_v30">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['award']['SchemeName']?></td>
								<td class="tabletext"><?=intranet_htmlspecialchars($rs_scheme['SchemeName'])?></td>
							</tr>
						
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['General']['StartDate']?></td>
								<td class="tabletext"><?=($rs_scheme['StartDate'] && $rs_scheme['StartDate'] != '0000-00-00') ? $rs_scheme['StartDate'] : '-'?></td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['General']['EndDate']?></td>
								<td class="tabletext"><?=($rs_scheme['EndDate'] && $rs_scheme['EndDate'] != '0000-00-00') ? $rs_scheme['EndDate'] : '-'?></td>
							</tr>
							
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['award']['GroupPIC']?></td>
								<td class="tabletext"><?=$schemePICInfo?></td>
							</tr>
							
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['award']['ClassOrientedSuggestion']?></td>
								<td class="tabletext">
									<table width="100%" id="SchemeTargetTable" class="common_table_list" style="display:<?=$schemeTargets ? '':'none'?>">
										<?=$schemeTargets?>
									</table>
								</td>
							</tr>
							
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['General']['Remark']?></td>
								<td class="tabletext"><?=$rs_scheme['Remark'] ? nl2br($rs_scheme['Remark']) : '-'?></td>
							</tr>

						</table>
						
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<span>
										<?= $linterface->GET_ACTION_BTN($button_edit, "button","window.location='?t=management.edit_award&SchemeID=$schemeID'","btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
										<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='?t=management.award_list'","btnCancel", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
									</span></td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<input type=hidden id="SchemeID" name="SchemeID" value="<?=$schemeID?>">
</form>
