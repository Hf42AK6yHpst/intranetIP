<?
/*
 * Log
 *
 * 2018-03-28 Cameron
 * - change button 'Submit' to 'View' in search toobar
 * 
 * 2018-03-21 Cameron
 * - fix: javascript error in NotAllowToDeleteNonSelfRecord
 *
 * 2018-03-20 Cameron
 * - add student group filter [case #121742]
 * - fix: should set EventStudent to all if ClassName or GroupID is set to all
 *
 * 2018-02-27 Cameron
 * - allow to delete self record only for non-super-admin user [case #F135176]
 *
 * 2018-01-31 Cameron
 * - only allow super admin to delete record [case #F135093]
 *
 * 2017-06-21 Cameron
 * - create this file
 */
?>

<script>
function checkGet2(obj,url){
    obj.action=url;
    obj.submit();
}

$(document).ready(function(){
	$('#EventDate').click(function(){
		if ($(this).is(':checked')){
			$('#spanRequest').css('display','inline');
		}
		else {
			$('#spanRequest').css('display','none');
		}
	});

	$('#deleteButton').click(function(){
		var isPass = true;
		if ($(":input[name='EventID\[\]']:checked").length == 0) {
			alert(globalAlertMsg2);
			isPass = false;
		}
		else {
    		$(":input[name='EventID\[\]']:checked").each(function(){
    			if ($(this).attr('data-deleteAllow') == 0) {
    				alert("<?php echo $Lang['medical']['general']['Warning']['NotAllowToDeleteNonSelfRecord'];?>");	// please use " here as English version contains '
    				isPass = false;
    				return false;
    			}
    		});
		}
		
		if (isPass && confirm(globalAlertMsg3)){
			form1.action='?t=management.remove_event';                
            form1.method='POST';
            form1.submit();
		}
	});	

	$('#ClassName').change(function(){
		if ($(this).val() == '') {
			$('#EventStudent').val('');
		}
		$('#form1').submit();
	});

	$('#GroupID').change(function(){
		if ($(this).val() == '') {
			$('#EventStudent').val('');
		}
		$('#form1').submit();
	});
	
});

function changeBuilding() {
	$('#LocationLevelID').val('');
	$('#LocationID').val('');
	$('#form1').submit();
}

function changeBuildingLevel() {
	$('#LocationID').val('');
	$('#form1').submit();
}

</script>

<form name="form1" id="form1" method="POST"
	action="?t=management.event_list">
	<div class="content_top_tool">

		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="20%"><?=$toolbar ?></td>
				<td width="50%" align="center">&nbsp;</td>
				<td width="30%" style="float: right;">
					<div class="content_top_tool" style="float: right;">
						<?=$htmlAry['searchBox']?>	
						<br style="clear: both" />
					</div>
				</td>
			</tr>
		</table>

	</div>

	<div class="table_board">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr class="table-action-bar">
				<td valign="bottom">
					<div class="table_filter">
					<?=$classFilter ?>
					<?=$groupFilter ?>
					<?=$studentFilter?>
					<?=$eventTypeFilter?>
					<?=$buildingFilter?>
					<?=$levelFilter?>
					<?=$locationFilter?>
					<?=$affectedPersonFilter?>
					<?=$reporterFilter?>
				</div> <br style="clear: both" />
					<div>
						<input type="checkbox" name="EventDate" id="EventDate" value="1"
							<?=$EventDate==1 ? "checked" : ""?>><label for="EventDate"><?=$Lang['medical']['filter']['FilterDateRange']?></label>
						<div id="spanRequest" style="position:relative;display:<?=$EventDate==1 ? "inline" : "none"?>">
							<label for="EventDateStart"><?=$i_From?></label> : <?=$linterface->GET_DATE_PICKER("EventDateStart",$EventDateStart)?>
					 <?=getTimeSel('TimeStart', ($TimeStartHour?$TimeStartHour:0), ($TimeStartMin?$TimeStartMin:0), "", $minuteStep=1)?>
					<label for="EventDateEnd"><?=$i_To ?></label> <?=$linterface->GET_DATE_PICKER("EventDateEnd",$EventDateEnd)?>
					 <?=getTimeSel('TimeEnd', ($TimeEndHour?$TimeEndHour:23), ($TimeEndMin?$TimeEndMin:59), "", $minuteStep=1)?></label>
					 <?=$linterface->GET_ACTION_BTN($Lang['Btn']['View'], 'submit',$ParOnClick="", $ParName="submit1")?>
						</div>
					</div>

				</td>
				<td valign="bottom">
					<div class="common_table_tool" style="width:<?=(($sys_custom['medical']['Event']['OnlyAllowSuperAdminDelete'] && $objMedical->isSuperAdmin($_SESSION['UserID'])) || !$sys_custom['medical']['Event']['OnlyAllowSuperAdminDelete']) ? "150px;": "50px;"?>">
						<a
							href="javascript:checkEdit(document.form1,'EventID[]','?t=management.edit_event')"
							class="tool_edit"><?=$button_edit ?></a>
				<? if (($sys_custom['medical']['Event']['OnlyAllowSuperAdminDelete'] && $objMedical->isSuperAdmin($_SESSION['UserID'])) || !$sys_custom['medical']['Event']['OnlyAllowSuperAdminDelete']):?>					
					<a href="#" id='deleteButton' class="tool_delete"><?= $button_delete ?></a>
				<? endif;?>
				</div>
				</td>
			</tr>
		</table>
	</div>

	<div class="table_board">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="bottom">

				<?= $li->display() ?>
 
			</td>
			</tr>
		</table>
	</div>

	<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
	<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
	<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
	<input type="hidden" name="page_size_change" value="" /> <input
		type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

</form>
