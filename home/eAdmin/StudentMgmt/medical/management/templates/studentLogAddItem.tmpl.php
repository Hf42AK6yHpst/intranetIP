<?php
// using:
/*
 * 2018-03-29 [Cameron]
 * - add link to relevant events
 *
 * 2018-01-11 [Cameron]
 * - fix: do not change change hasSave flag to 0 if selecct element has 'readonly' class
 *
 * 2017-11-24 [Cameron]
 * - Hide / Show SaveAll button depends on the appearance of SaveIndividual button
 *
 * 2017-11-22 [Cameron]
 * - change fileMaxSize from 3M to 6M in isFileInCorrectSize() [case #P122386]
 *
 * 2017-09-18 [Cameron]
 * - move "Add Event" button to top of "Event Information" [case #P120778]
 *
 * 2015-12-04 [Cameron]
 * - remove pass by reference in getEventLogInputUI() to support php 5.4
 *
 * 2015-01-22 [Cameron] Use flag $plugin['medical_module']['AlwaysShowBodyPart'] to control whether to show body parts.
 * Note: Body Part is not mandatory if this flag is true, otherwise, it's mandatory if Lev2Name=$medical_cfg['studentLog']['level2']['specialStatus']
 */
$objlibMedical = new libMedical();
$defaultRemark = $objlibMedical->getDefaultRemarksArray($medical_cfg['general']['module']['studentlog']);

echo <<< REMARKS_INIT
	<script>
//		var jRemarksCount=$i;
		var jRemarksArr=$defaultRemark;
	</script>
REMARKS_INIT;
?>
<style>
.eventClass td {
	vertical-align: top;
}

a {
	color: #2286C5;
	cursor: pointer;
}

.errorMsg {
	color: red;
}

.table_row_tool {
	float: none;
	display: inline-block;
	color: #2286C5 !important;
	cursor: pointer !important;
}

.delete_event_div {
	float: right !important;
	display: block;
}

legend {
	color: gray;
	font-style: italic;
	font-size: 16px;
}

.defaultRemarkSpan img {
	vertical-align: top;
}
</style>

<table width="95%" border="0" cellpadding="5" cellspacing="0"
	align="center">
	<colgroup>
		<col style="width: 20%" />
		<col style="width: 80%" />
	</colgroup>
	<tbody>
		<tr>
			<td><?php echo $Lang['medical']['studentLog']['eventDate'];?></td>
			<td><?php echo $date; ?></td>
		</tr>
		<tr>
			<td><?php echo $Lang['Header']['Menu']['Class'];?></td>
			<td><?php echo $studentInfo['ClassName'] ?></td>
		</tr>
		<tr>
			<td><?php echo $Lang['Identity']['Student'];?></td>
			<td><?php echo $studentInfo['Name'].'('.$studentInfo['ClassNumber'].')' ?></td>
		</tr>
		<tr>
			<td colspan="2"><span class="table_row_tool addEvent"><a
					class="add newBtn" id="addEventImage"
					title="<?php echo $Lang['medical']['studentLog']['addEvent']; ?>"></a></span><a
				id="addEvent"><?php echo $Lang['medical']['studentLog']['addEvent']; ?></a>
				<!--<hr style="width:80%;" align="left"/>--> <br /> <span
				class="form_sep_title" style="font-size: 15px"> <i>
					<?php echo $Lang['medical']['studentLog']['eventInfo'];?>
					</i>
			</span> <br />

				<table width="100%" border="0" cellpadding="5" cellspacing="0"
					align="center">
					<colgroup>
						<col style="width: 70%" />
						<col style="width: 30%" />
					</colgroup>
					<tbody>
						<tr>
							<td>
								<div id="viewResult">
									<?php
        $countStudentLogObjAry = count($studentLogObjAry);
        foreach ((array) $studentLogObjAry as $index => $studentLogObj) {
            $objMedical->getEventLogInputUI($Id = $studentLogObj->getRecordID(), $linterface, $studentLogObj, $countStudentLogObjAry - $index);
            $eventIDArray[] = $studentLogObj->getRecordID();
        }
        ?>	
								</div>
							</td>
							<td></td>
						</tr>
					</tbody>
				</table></td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<td align="center" colspan="9" class="dotline"></td>
		</tr>
		<tr>
			<td align="center" colspan="9">
				<form id="form1" action="index.php?t=management.studentLog">
					<input type="hidden" id="t" name="t" value="management.studentLog" />
					<input type="hidden" id="sid" name="sid" value="<?php echo $sid?>" />
					<input type="hidden" id="date" name="date"
						value="<?php echo $date?>" /> <input type="hidden" id="gender"
						name="gender" value="<?php echo $gender?>" /> <input type="hidden"
						id="classID" name="classID" value="<?php echo $classID?>" /> <input
						type="hidden" id="sleep" name="sleep" value="<?php echo $sleep?>" />
					<input type="hidden" id="groupID" name="groupID"
						value="<?php echo $groupID?>" /> <input type="hidden"
						id="eventIDArray" name="eventIDArray"
						value="<?php echo implode(',',$eventIDArray); ?>" /> <input
						type="button" id="SaveAll" name="SaveAll"
						class="formbutton_v30 print_hide " style="display: none;"
						value="<?php echo $Lang['medical']['studentLog']['button']['saveAll']; ?>">
					<input type="button" id="Cancel" name="Cancel"
						class="formbutton_v30 print_hide "
						value="<?php echo $Lang['medical']['studentLog']['button']['back']; ?>">
				</form>
			</td>
		</tr>
	</tfoot>
</table>

<?php echo $linterface->Include_Thickbox_JS_CSS();?>
<script type="text/javascript"
	src="<?=$PATH_WRT_ROOT?>/templates/jquery/jquery.blockUI.js"></script>
<script type="text/javascript"
	src="<?=$PATH_WRT_ROOT?>/templates/jquery/jquery.form-2.60.js"></script>
<script>
$(document).ready(function(){
	// Add Buttons
	// Delete Buttons
	
	var count = 1; // a count for new items
	var deleteFile = 0;
	var deleteEvent = 0;

	<?php
if ($recordID != NULL) {
    echo "window.location.href='#event_$recordID';";
}
?>

	$('#Cancel').click(function(){
		$('#form1').submit();
	});
	
	// Add Buttons Start
	$(document).on('click','.addAttachment',function(){
		var form = $(this).closest('form');
		var Id = form.find('input[name="recordID"]').val();	
		
		var numberOfAttachment = $('.event\\['+Id+'\\]\\[fileUploaded\\]\\[\\]').length;
		var returnStr ='';
		if( numberOfAttachment <6){
			returnStr += '<span><img src="<?php echo $image_path?>/icon/attachment_blue.gif" border=0 /><input type="file" id="event['+Id+'][fileUploaded][]" class="event['+Id+'][fileUploaded][]" name="event['+Id+'][fileUploaded][]" /></span>';
			returnStr += '<span class="table_row_tool"><a class="deleteAttachmentBtn delete" title="<?php echo $Lang['Btn']['Delete']?>"></a></span>' ;
			returnStr += '<br />';
			
			$(this).before(returnStr);
		}
		else{
			alert('<?php echo $Lang['medical']['general']['Hint']['MaxAttachment'] ?>');
		}
	});
	$(document).on('click','.addBodyParts',function(){
		var obj = $(this).parent().find('.bodyPartsField');
		var form = $(this).closest('form');
		var Id = form.find('input[name="recordID"]').val();	

		$.ajax({
			url : "?t=management.ajax.getStudentLogLevelList",
			type : "POST",
			data : '&action=3'+'&Id='+Id,
			success : function(msg) {
				obj.append(msg);
			}
		});	

	});
	$('#addEvent').hover(
		function(){
		$('#addEventImage').css('background-position','-20px -40px');
		},
        function () {
			$('#addEventImage').css('background-position','0px -40px');
        }
	);
	$('#addEvent, .newBtn').click(function(){
		$.ajax({
			url : "?t=management.ajax.getStudentLogLevelList",
			type : "POST",
			data : 'Id=n'+count+'&action=newField',
			success : function(msg) {
				$('#viewResult').prepend(msg);
				$('html, body').animate({scrollTop:250}, 'slow');

				count++;

				$('#viewResult').find('.level2:first').change();
				
				//  show / hide SavelAll button
				refreshSaveAllButton();
			}
		});	

		// Hide all opened default-remarks-dialog
		$('div > a[onclick*="setDivVisible"]').click();
	});
	// Add Buttons End
	
	// Delete Buttons Start
	$(document).on('click','.deleteEventBtn',function(){
		var isDeleted = confirm('<?php echo $Lang['medical']['studentLog_general']['deleteEvent']; ?>');
		if(!isDeleted){
			return;
		}

		var form = $(this).closest('form');
		if(form.find('input[name="recordID"]').val().indexOf('n')>-1){
			form.parent().remove();
			
			//  show / hide SavelAll button
			refreshSaveAllButton();
		}else{
			$.get('index.php',{
				t: 'management.ajax.saveStudentLogIndividual',
				action: 'delete',
				recordID: form.find('input[name="recordID"]').val()
			},function(data){
				if(data == '<?php echo $Lang['General']['ReturnMessage']['UpdateSuccess'];?>'){
					form.parent().remove();
					
					//  show / hide SavelAll button
					refreshSaveAllButton();
				}
//				console.log(data);
			});
		}

	});

	$(document).on('click','.deletePartsBtn',function(){
		$(this).parent().prev().remove();
		$(this).parent().prev().remove();
		$(this).parent().next().remove();
		$(this).parent().remove();
	});

	$(document).on('click','.deleteSavedAttachmentBtn',function(){
		var isDeleted = confirm('<?php echo $Lang['medical']['report_general']['deleteFile'] ?>');
		if(!isDeleted){
			return;
		}
		
		var form = $(this).closest('form');
		form.find('.hasSave').val('0');
		var attachmentId = $(this).attr('data-attachmentId');
		var Id = form.find('input[name="recordID"]').val();	
		$('#event\\['+Id+'\\]\\[isfileUploadedPresent\\]\\['+attachmentId+'\\]').val(0);
		$(this).parent().next().remove();
		$(this).parent().prev().remove();
		$(this).parent().remove();
	});

	$(document).on('click','.deleteAttachmentBtn',function(){
		var isDeleted = confirm('<?php echo $Lang['medical']['report_general']['deleteFile'] ?>');
		if(!isDeleted){
			return;
		}
		
		var form = $(this).closest('form');
		form.find('.hasSave').val('0');
		$(this).parent().next().remove();
		$(this).parent().prev().remove();
		$(this).parent().remove();
	});
	// Delete Buttons End
		
	<?php if($_POST['action']=='new'): ?>
		$('#addEvent').click();
	<?php endif;?>

	// Level List on ajax Start
	$(document).on('change','.level1',function(){
		var form = $(this).closest('form');
		$objList = $(this).next();
		$.ajax({
			url : "?t=management.ajax.getStudentLogLevelList",
			type : "POST",
			data : 'data='+$(this).val()+'&action=2',
			success : function(msg) {
				$objList.html(msg);
				form.find('.level2').change();
			}
		});	
	});
	$(document).on('change','.level2',function(){
		var form = $(this).closest('form');
		if(($(this).find('option:selected').html()=='<?php echo $medical_cfg['studentLog']['level2']['specialStatus']?>') || (<?=intval($plugin['medical_module']['AlwaysShowBodyPart'])?> > 0)){
			form.find('.bodyPartsDiv').show();			
		}
		else{
			form.find('.bodyPartsDiv').hide();
		}
	});

	$(document).on('change','.level3',function(){
		var objList = $(this).next();
		var Id = $(this).attr('data-Id');
		$.ajax({
			url : "?t=management.ajax.getStudentLogLevelList",
			type : "POST",
			data : 'data='+$(this).val()+'&action=4'+'&Id='+Id,
			success : function(msg) {
				objList.html(msg);
//				console.log(msg);
			}
		});	
	});
	// Level List on ajax End
	
	
	// Record the form has been edit or not Start
	$(document).on('change','form input, select, textarea',function(){
		if (!$(this).hasClass('readonly')) {
			var form = $(this).closest('form');
			form.find('.hasSave').val('0');
		}
	});
	$(document).on('click','form .tbclip a, .formsubbutton',function(){
//	$('form').find('.tbclip').find('a').live('click',function(){
		var form = $(this).closest('form');
		form.find('.hasSave').val('0');
	});
	// Record the form has been edit or not End
	
	$(document).on('click','.SaveIndividual',function(){
		var isValid = true;
		var form = $(this).closest('form');
	
		// Check Form Start
		form.find('.level2').each(function(){
			if($(this).val()==""){
				alert('<?=$Lang['medical']['studentLog']['addItemNoLev2']?>');
				isValid = false;
				window.location.href='#'+form.attr('id');
				return false;
			}
		});
		if(!isValid){
			return false;
		}
		
		form.find('.errorMsg').remove();
		if(!checkEmptyLevel4(form)){
			window.location.href='#'+form.attr('id');
			return false;
		}
		// Check Form End
		
		return isFileInCorrectSize(form,submitForm); // Check the file size and submit form
	});
	
	function checkEmptyLevel4(form){
		var noError=true;
		var checkSpecialCase = false;
		form.find('.level2').each(function(){
			if(($(this).find('option:selected').html()=='<?php echo $medical_cfg['studentLog']['level2']['specialStatus']?>') && (<?=intval($plugin['medical_module']['AlwaysShowBodyPart'])?> == 0)){
				checkSpecialCase = true;
			}
		});
		
		if(checkSpecialCase){
			noError = false;
			form.find('.level4').each(function(){
				if($(this).is(':checked')){
					noError = true;
				}
			});
			if(!noError){				
				alert('<?php echo $Lang['medical']['general']['AlertMessage']['NoLevel4SelectedError'];?>');
				return false;
			}
		}
		
		return noError;
	}
	
	function isFileInCorrectSize(form,submitFun)
	{
		var isFieldValid = true;
		var fileMaxSize = 6 * 1024 * 1024;
		
		if(window.navigator.userAgent.indexOf("MSIE")>0){ // IE
			var item,i;
			form.ajaxSubmit({
				url: 'index.php',
				data: {t: 'management.ajax.checkFileSize',fileMaxSize:fileMaxSize},
				success: function(data){
					if(data == '1'){
						$('.select_studentlist > option').attr('selected','selected');
						submitForm(form);
					}else{
						var failDataArr = $.trim(data).split(" ");
						for(var i=0;i<failDataArr.length;i++){
							var itemName = failDataArr[i].substring(0,2);
							var index = failDataArr[i].substring(3);
							diaplsyFileSizeError(
								$("input:file[name='event["+itemName+"][fileUploaded][]']")[index]
							);
						}
						isFieldValid = false;
					}
				}
			});
		}else{
			form.find('input[type="file"]').each(function(index,element){
				var tempObj1 = $('#errormsg');
				
				if ( $(this).val() !='')
				{
					if(this.files[0].size > fileMaxSize){
						diaplsyFileSizeError(element);
						isFieldValid = false;
					}
				}
		    });
			if(isFieldValid){
				$('.select_studentlist > option').attr('selected','selected');
				submitFun(form);
			}else{
				window.location.href='#'+form.attr('id');
			}
	    }
	    return isFieldValid;
	}
	function diaplsyFileSizeError(element){
		if(!$(element).prev().hasClass("errorMsg")){
			var errorMsg = '<span class="errorMsg alert_text"><br /><?php echo $Lang['medical']['general']['AlertMessage']['MaxAttachmentError'];?><br /></span>';
			$(element).before(errorMsg);
		}
	}
	function submitForm(form){
		var saveSuccessString = '<?php echo $Lang['General']['ReturnMessage']['UpdateSuccess'];?>';
		saveSuccessString = saveSuccessString.substring(saveSuccessString.lastIndexOf('|=|')+3);
		if(form.find('.hasSave').val() == 1){
			form.find('.saveMsg > td > span').html(saveSuccessString);
		}else{
			form.block();
			form.ajaxSubmit({
				url: 'index.php',
				data: {
					t: 'management.ajax.saveStudentLogIndividual',
					sid: '<?php echo $sid?>',
					date: '<?php echo $date?>'
				},
				success: function(data){
					var recordSpan = form.parent();
					recordSpan.html(data);
					recordSpan.find('.saveMsg > td > span').html(saveSuccessString);
				}
			});
		}
		setTimeout(function(){
			$('.saveMsg > td > span').html('');
		},3000);
	}

	
	$('#SaveAll').click(function(){
		var isSaveAll = confirm('<?php echo $Lang['medical']['general']['SaveAllConfirm'];?>');
		if(!isSaveAll){
			return;
		}
//$('body').block();
		var saveAllForm = $('#form1');
		var countSubmitForm = $('form[id^="event"]').length;
		$('form[id^="event"]').each(function(index){
			var isValid = true;
			var form = $(this);
		
			// Check Form Start
			form.find('.level2').each(function(){
				if($(this).val()==""){
					alert('<?=$Lang['medical']['studentLog']['addItemNoLev2']?>');
					isValid = false;
					window.location.href='#'+form.attr('id');
					return false;
				}
			});
			if(!isValid){
				window.location.href='#'+$(this).attr('id');
				return false;
			}
			
			form.find('.errorMsg').remove();
			if(!checkEmptyLevel4(form)){
				window.location.href='#'+form.attr('id');
				return false;
			}
			// Check Form End
			var submitSuccessful = isFileInCorrectSize(form,submitForm); // Check the file size and submit form 
			if(submitSuccessful){
				if(index == countSubmitForm-1){
					setTimeout(function(){
						saveAllForm.submit();
					},1000); // Give server a time for server to save data.
				}
			}else{
				window.location.href='#'+form.attr('id');
				return false; 
			}
		});
	});


	$(document).on('click','.addEventBtn',function(){
		var form = $(this).closest('form');
		var Id = form.find('input[name="recordID"]').val();	
		tb_show('<?=$Lang['medical']['case']['SelectEvent']?>','?t=management.selectEventForStudentLog&EventStudent='+$('#sid').val()+'&FormId='+Id+'&FromStudentLog=1&height=500&width=800');
	});

	$(document).on('click','.viewEvent',function(){
		var eventID = $(this).attr('data-EventID');
		tb_show('<?=$Lang['medical']['case']['ViewEvent']?>','?t=management.event_view&EventID='+eventID+'&ViewMode=1&height=500&width=800');
	});

	$(document).on('click','.removeEvent',function(){
		var form = $(this).closest('form');
		form.find('.hasSave').val('0');
		var eventID = $(this).attr('data-EventID');
		$(this).parent().parent().parent().remove();
	});
	
	//  show / hide SavelAll button
	refreshSaveAllButton();
	
});

function refreshSaveAllButton() {
	var numberOfSaveButton=0;
	$('.SaveIndividual').each(function(){
		numberOfSaveButton++;
	});

	if (numberOfSaveButton > 0) {
		$('#SaveAll').css('display','');
	}
	else {
		$('#SaveAll').css('display','none');
	}
}

function show_ajax_error() 
{
	alert('<?=$Lang['medical']['general']['error']['ajax']?>');
}

</script>