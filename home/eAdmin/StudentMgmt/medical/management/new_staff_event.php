<?php
// using:
/*
 * 2018-12-14 Cameron
 * - create this file
 */
if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'STAFFEVENT_ADD') || ! $plugin['medical_module']['staffEvents']) {
    header("Location: /");
    exit();
}
$CurrentPage = "ManagementStaffEvent";
$CurrentPageName = $Lang['medical']['menu']['staffEvent'];
$TAGS_OBJ[] = array(
    $Lang['medical']['menu']['staffEvent'],
    "",
    0
);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();

$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$PAGE_NAVIGATION[] = array(
    $Lang['medical']['menu']['staffEvent'],
    "?t=management.staffEventList"
);
$PAGE_NAVIGATION[] = array(
    $Lang['Btn']['New'],
    ""
);


$staffEventTypeAry = $objMedical->getStaffEventType();

if (count($staffEventTypeAry) == 0) {
    header("Location: ?t=management.staffEventList&returnMsgKey=EventTypeNotSetup");
    exit();
}

$staffEventTypeSelection = getSelectByArray($staffEventTypeAry, 'Name="StaffEventTypeID" ID="StaffEventTypeID"', $selected = "");
$staffEventTypeLev2Selection = '';

$buildingAry = $objMedical->getInventoryBuildingArray();
$buildingSelection = getSelectByArray($buildingAry, 'Name="StaffEventLocationBuildingID" ID="StaffEventLocationBuildingID"', $selected = "");

$relevantStudentEvents = '';
$staffEventAry = array();
$staffEventAry['Teaching'] = 1;     // default teaching

// # handle attachment(s)
$ret = $objMedical->handleTempUploadFolder();
$tempFolderPath = $ret['TempFolderPath'];
$use_plupload = $objMedical->is_use_plupload();

$linterface->LAYOUT_START($returnMsg);

$plupload = array();
$plupload['use_plupload'] = $use_plupload;
if ($use_plupload) {
    $plupload['pluploadButtonId'] = 'UploadButton';
    $plupload['pluploadDropTargetId'] = 'DropFileArea';
    $plupload['pluploadFileListDivId'] = 'FileListDiv';
    $plupload['pluploadContainerId'] = 'pluploadDiv';
}

$form_action = '?t=management.new_staff_event_save';

include_once ('templates/staff_event.tmpl.php');

if ($use_plupload) {
    include_once ('plupload_script.php');
}

$linterface->LAYOUT_STOP();
?>