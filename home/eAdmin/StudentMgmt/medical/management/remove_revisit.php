<?php
// using:
/*
 * 2018-12-27 Cameron
 * - fix sql in checking self record
 * 
 * 2018-02-27 Cameron
 * - allow to delete self record only for non-super-admin user [case #F135176]
 *
 * 2018-02-01 Cameron
 * - Log delete record for trace purpose
 *
 * 2017-09-01 Cameron
 * - create this file
 */
if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'REVISIT_MANAGEMENT') || ! $plugin['medical_module']['revisit']) {
    header("Location: /");
    exit();
}

$ldb = new libdb();
$lfs = new libfilesystem();
$result = array();
$dataAry = array();
$isRequiredCheckSelfRevisit = $objMedical->isRequiredCheckSelfRevisit();
$returnMsgKey = '';

if (count($RevisitID) > 0) {
    $isAllowedToDelete = true;
    if ($isRequiredCheckSelfRevisit) {
        $sql = "SELECT RevisitID FROM MEDICAL_REVISIT WHERE RevisitID IN ('" . implode("','", $RevisitID) . "') AND InputBy<>'" . $_SESSION['UserID'] . "'";
        $checkResult = $ldb->returnResultSet($sql);
        if (count($checkResult)) {
            $isAllowedToDelete = false;
            $returnMsgKey = 'DeleteNonSelfRecord';
        }
    }
    
    if ($isAllowedToDelete) {
        $ldb->Start_Trans();
        
        // log delete record
        foreach ((array) $RevisitID as $revisitID) {
            $sql = "SELECT CONCAT(Drug,'^^',Dosage,'^*',Unit) AS DrugInfo FROM MEDICAL_REVISIT_DRUG WHERE RevisitID='{$revisitID}'";
            $rs = $ldb->returnResultSet($sql);
            if (count($rs)) {
                $drugInfoAry = BuildMultiKeyAssoc($rs, "DrugInfo", array(
                    "DrugInfo"
                ), $SingleValue = 1);
                $drugInfos = implode('^~', $drugInfoAry);
            } else {
                $drugInfos = '';
            }
            
            $revisitAry = $objMedical->getRevisit($revisitID);
            $revisit = current($revisitAry);
            
            unset($dataAry);
            $dataAry['TableName'] = 'MEDICAL_REVISIT';
            $dataAry['RecordID'] = $revisitID;
            $dataAry['StartDate'] = $revisit['RevisitDate'];
            $dataAry['Summary'] = $revisit['CuringStatus'];
            $dataAry['Detail'] = $drugInfos;
            $dataAry['StudentID'] = $revisit['StudentID'];
            $dataAry['OtherRef'] = $revisit['Hospital'] . ',' . $revisit['Division'];
            $dataAry['LastModifiedBy'] = $_SESSION['UserID'];
            $sql = $objMedical->INSERT2TABLE('MEDICAL_DELETE_LOG', $dataAry, $condition = array(), $run = false);
            $result[] = $ldb->db_db_query($sql);
        }
        
        // delete revisit
        $sql = "DELETE FROM MEDICAL_REVISIT WHERE RevisitID IN ('" . implode("','", $RevisitID) . "')";
        $result[] = $ldb->db_db_query($sql);
        
        $sql = "DELETE FROM MEDICAL_REVISIT_DRUG WHERE RevisitID IN ('" . implode("','", $RevisitID) . "')";
        $result[] = $ldb->db_db_query($sql);
        
        if (! in_array(false, $result)) {
            $ldb->Commit_Trans();
            $returnMsgKey = 'DeleteSuccess';
        } else {
            $ldb->RollBack_Trans();
            $returnMsgKey = 'DeleteUnsuccess';
        }
    }
}

header("location: ?t=management.revisit_list&returnMsgKey=" . $returnMsgKey);

?>