<?php
// using:
/*
 * 	2017-07-06 Cameron
 * 		- create this file
 */

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='AWARDSCHEME_MANAGEMENT') || !$plugin['medical_module']['discipline']){
	header("location: /");
	exit();
}

if (is_array($SchemeID)) {
	$schemeID = (count($SchemeID)==1) ? $SchemeID[0] : '';
}
else {
	$schemeID = $SchemeID;
}

if (!$schemeID) {	
	header("Location: ?t=management.scheme_list");
}

$rs_scheme = $objMedical->getSchemes($schemeID);
if (count($rs_scheme) == 1) {	
	$rs_scheme = current($rs_scheme);
}
else {
	header("Location: ?t=management.scheme_list");
}

$CurrentPage = "ManagementAward";
$CurrentPageName = $Lang['medical']['menu']['award'];
$TAGS_OBJ[] = array($Lang['medical']['award']['tab']['Scheme'] , "?t=management.award_list", 1);
$TAGS_OBJ[] = array($Lang['medical']['award']['tab']['Performance'] , "?t=management.student_list_of_performance", 0);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();		

$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$PAGE_NAVIGATION[] = array($Lang['medical']['menu']['award'], "?t=management.award_list");
$PAGE_NAVIGATION[] = array($Lang['Btn']['View'], "");

$rs_scheme['SchemePIC'] = $objMedical->getSchemePIC($schemeID);
$schemePICInfo = $objMedical->getUserNameList($rs_scheme['SchemePIC']);
$schemeTargets = $objMedical->getSchemeTargetList($schemeID, 'view');


$linterface->LAYOUT_START($returnMsg);

include_once('templates/award_view.tmpl.php');

$linterface->LAYOUT_STOP();
?>