<?php
// using:
/*
 * 2018-03-28 Cameron
 * - change button 'Submit' to 'View' in search toobar
 *
 * 2018-03-21 Cameron
 * - add groupID filter
 *
 * 2017-07-04 Cameron
 * - create this file
 */
if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'EVENT_MANAGEMENT') || ! $plugin['medical_module']['discipline']) {
    header("Location: /");
    exit();
}

$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);

// ############ start Filters
$currentAcademicYearID = Get_Current_Academic_Year_ID();

// Acadermic Year Filter
$yearFilter = getSelectAcademicYear("AcademicYearID", 'onChange="changeAcademicYearFilter();"', 0, 0, $AcademicYearID);

// Class Filter
$lclass = new libclass();
$classFilter = $lclass->getSelectClass("name='SrhClassName' id='SrhClassName' onChange='changeClass();'", $ClassName, "", $Lang['medical']['event']['AllClass'], $currentAcademicYearID);

// Group Filter
$groupFilter = $objMedical->getGroupSelection("SrhGroupID", $SrhGroupID, "onChange='changeGroup();'", $Lang['medical']['event']['AllGroup']);

foreach ((array) $Lang['medical']['case']['status'] as $k => $v) {
    $statusAry[] = array(
        $k,
        $v
    );
}
$statusFilter = getSelectByArray($statusAry, "name='CaseStatus' onChange='getCases();'", $CaseStatus, 0, 0, $Lang['medical']['case']['AllStatus']);

if (! empty($ClassName)) {
    if ($junior_mck) {
        $studentAry = $lclass->getStudentNameListWClassNumberByClassName($ClassName);
    } else {
        $studentAry = $objMedical->getStudentNameListWClassNumberByClassName($ClassName);
    }
} else {
    $studentAry = $objMedical->getAllStudents($currentAcademicYearID);
}
$studentFilter = getSelectByArray($studentAry, "name='CaseStudent' id='CaseStudent' onChange='getCases();'", $CaseStudent, 0, 0, $Lang['medical']['event']['AllStudent']);

$teacherAry = $objMedical->getTeacher();
$reporterFilter = getSelectByArray($teacherAry, "name='CaseReporter' onChange='getCases();'", $CaseReporter, 0, 0, $Lang['medical']['case']['AllReporter']);

$casePICAry = $teacherAry;
$casePICFilter = getSelectByArray($casePICAry, "name='CasePIC' onChange='getCases();'", $CasePIC, 0, 0, $Lang['medical']['case']['AllPIC']);

$CaseDateStart = $CaseDateStart ? $CaseDateStart : substr(getStartDateOfAcademicYear($currentAcademicYearID), 0, 10);
$CaseDateEnd = $CaseDateEnd ? $CaseDateEnd : substr(getEndDateOfAcademicYear($currentAcademicYearID), 0, 10);

// ############ end Filters

?>

<script>
$(document).ready(function(){
	$('#CaseDate').click(function(){
		if ($(this).is(':checked')){
			$('#spanRequest').css('display','inline');
			$('#AcademicYearID').val('');
		}
		else {
			$('#spanRequest').css('display','none');
		}
	});	
	
	$('#keyword').keyup(function(event) {				
	  	if (event.which == 13) {			
			getCases();		
	  	}  			
	});				
	
	$('#keyword').keypress(function(e){			
	    if ( e.which == 13 ) {			
			e.preventDefault();	
		    return false;		
	    }			
	});			
	
	getCases();	
	
});

function getCases() {
	$.ajax({
		dataType: "json",
		type: "POST",
		url: '?t=management.ajax.ajax&ma=1&action=getCases',
		data : $('#form2').serialize(),		  
		success: showCases,
		error: show_ajax_error
	});
}

function showCases(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#CaseList').html(ajaxReturn.html);
		if (parseInt(ajaxReturn.html2) > 0) {
			$('#SelectBtn').css('display','');
		}
		else {
			$('#SelectBtn').css('display','none');
		}
	}
}	

function selectCase() {
    if(countChecked(document.form2,"CaseID[]")==0) {
    	alert(globalAlertMsg2);
    }
    else{
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '?t=management.ajax.ajax&ma=1&action=getSelectedCase',
			data : $('#form2').serialize(),
			success: returnCase,
			error: show_ajax_error
		});

    	js_Hide_ThickBox();
    }
}

function returnCase(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#RelevantCaseTable').css('display','');
		$('#RelevantCaseTable').append(ajaxReturn.html);
//		$('#CaseNoDiv').html(ajaxReturn.html);
		$('#ErrCase').addClass('error_msg_hide').removeClass('error_msg_show');
	}
}	

function changeClass() {
	$.ajax({
		dataType: "json",
		type: "POST",
		url: '?t=management.ajax.ajax&ma=1',
		data : {
			'action': 'getStudentNameByClassForEvent',
			'ClassName': $('#SrhClassName').val(),
			'GroupID': $('#SrhGroupID').val()
		},		  
		success: update_student_list_in_case_select,
		error: show_ajax_error
	});
}

function changeGroup()
{
	$.ajax({
		dataType: "json",
		type: "POST",
		url: '?t=management.ajax.ajax&ma=1',
		data : {
			'action': 'getStudentNameByGroupIDForEvent',
			'GroupID': $('#SrhGroupID').val(),
			'ClassName': $('#SrhClassName').val()
		},		  
		success: update_student_list_in_case_select,
		error: show_ajax_error
	});
}

function update_student_list_in_case_select(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#CaseStudent').replaceWith(ajaxReturn.html);
		getCases();
	}
}	

function changeAcademicYearFilter() {
	if ($('#AcademicYearID').val() != '' ) {
		$('#CaseDate').attr('checked', false);
		$('#spanRequest').css('display','none');
	}
	getCases();
}

function show_ajax_error() 
{
	alert('<?=$Lang['medical']['general']['error']['ajax']?>');
}

</script>

<form name="form2" id="form2" method="POST">
	<div class="content_top_tool">

		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="20%"><?=$toolbar ?></td>
				<td width="50%" align="center">&nbsp;</td>
				<td width="30%" style="float: right;">
					<div class="content_top_tool" style="float: right;">
						<?=$htmlAry['searchBox']?>     
						<br style="clear: both" />
					</div>
				</td>
			</tr>
		</table>

	</div>

	<div class="table_board">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="bottom">
					<div class="table_filter">
					<?=$yearFilter?>
					<?=$classFilter ?>
					<?=$groupFilter ?>
					<?=$studentFilter?>
					<?=$statusFilter?>
					<?=$casePICFilter?>
					<?=$reporterFilter?>
				</div> <br style="clear: both" />
					<div>
						<input type="checkbox" name="CaseDate" id="CaseDate" value="1"
							<?=$CaseDate==1 ? "checked" : ""?>><label for="CaseDate"><?=$Lang['medical']['filter']['FilterDateRange']?></label>
						<div id="spanRequest" style="position:relative;display:<?=$CaseDate==1 ? "inline" : "none"?>">
							<label for="CaseDateStart"><?=$i_From?></label> : <?=$linterface->GET_DATE_PICKER("CaseDateStart",$CaseDateStart)?>
					<label for="CaseDateEnd"><?=$i_To ?></label> <?=$linterface->GET_DATE_PICKER("CaseDateEnd",$CaseDateEnd)?>
					<?=$linterface->GET_ACTION_BTN($Lang['Btn']['View'], 'button',$ParOnClick="getCases();", $ParName="submit1")?>
						</div>
					</div>

				</td>
				<td valign="bottom">
					<div class="common_table_tool" id="SelectBtn"
						style="width: 50px; display: none">
						<a href="javascript:selectCase()" class="tool_other"><?=$Lang['Btn']['Select'] ?></a>
					</div>
				</td>

			</tr>
		</table>
	</div>

	<div class="table_board">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="bottom" id="CaseList"></td>
			</tr>
		</table>
	</div>

</form>

