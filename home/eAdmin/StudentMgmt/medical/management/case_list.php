<?php
// using:
/*
 * 2018-12-27 Cameron
 * - show error message when deleting non-self record
 *  
 * 2018-06-05 Cameron
 * - show created by person [case #F133828]
 * - don't show title for people column (use getNameFieldByLang2 instead of getNameFieldByLang) 
 * 
 * 2018-03-20 Cameron
 * - add group filter [case #F121742]
 *
 * 2018-02-27 Cameron
 * - add data-deleteAllow attribute to checkbox item for checking if it's allowed to delete [case #F135176]
 * - show DateModified and LastModifiedBy
 * 
 * 2018-01-23 Cameron
 * - change default order to descending by date
 *
 * 2017-11-27 Cameron
 * - fix: cookie to store AcademicYearID
 *
 * 2017-10-13 Cameron
 * - fix: set AcademicYearID to current academic year if it's not specify
 *
 * 2017-07-03 Cameron
 * - create this file
 */
if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'CASE_MANAGEMENT') || ! $plugin['medical_module']['discipline']) {
    header("Location: /");
    exit();
}

$arrCookies = array();
$arrCookies[] = array(
    "ck_page_size",
    "numPerPage"
);
$arrCookies[] = array(
    "ck_right_page_number",
    "pageNo"
);
$arrCookies[] = array(
    "ck_right_page_order",
    "order"
);
$arrCookies[] = array(
    "ck_right_page_field",
    "field"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_CaseAcademicYearID",
    "AcademicYearID"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_CaseClassName",
    "ClassName"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_CaseGroupID",
    "GroupID"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_CaseStudent",
    "CaseStudent"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_CaseType",
    "CaseType"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_CasePIC",
    "CasePIC"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_CaseReporter",
    "CaseReporter"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_CaseDateStart",
    "CaseDateStart"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_CaseDateEnd",
    "CaseDateEnd"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_CaseKeyword",
    "keyword"
);

if (isset($clearCoo) && $clearCoo == 1) {
    clearCookies($arrCookies);
} else {
    updateGetCookies($arrCookies);
}

if (isset($ck_page_size) && $ck_page_size != "")
    $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$pageNo = ($pageNo == "") ? 1 : $pageNo;
$order = ($order == "") ? 0 : $order; // default descending
$field = ($field == "") ? 0 : $field;
$li = new libdbtable2007($field, $order, $pageNo);

$currentAcademicYearID = Get_Current_Academic_Year_ID();

$classCond = '';
$cond = '';

if ($junior_mck) {
    $classNameField = "u.ClassName";
    $classNumber = "u.ClassNumber";
    $classNameEng = $classNameField;
    $joinClass = '';
} else {
    $classNameField = Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN");
    $classNumber = "ycu.ClassNumber";
    $classNameEng = "yc.ClassTitleEN";
    $joinClass = " INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=u.UserID
					INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='" . $currentAcademicYearID . "' ";
}

if ($GroupID) {
    $joinClass .= "	INNER JOIN INTRANET_USERGROUP g ON g.UserID=u.UserID AND g.GroupID='" . $GroupID . "'";
}

if ($ClassName) {
    $cond .= " AND s.ClassName='" . $li->Get_Safe_Sql_Query($ClassName) . "'";
}

if ($CaseStudent) {
    $cond .= " AND c.StudentID='" . $li->Get_Safe_Sql_Query($CaseStudent) . "'";
}

switch ($CaseStatus) {
    case 'Closed':
        $statusVal = '1';
        break;
    case 'Processing':
        $statusVal = '0';
        break;
    default:
        $statusVal = '';
}
if ($statusVal != '') {
    $cond .= " AND c.IsClosed='" . $statusVal . "'";
}

if ($CasePIC) {
    $cond .= " AND a.CasePICID LIKE '%" . $li->Get_Safe_Sql_Like_Query('^' . $CasePIC . '~') . "%'";
    $casePICJoin = " INNER JOIN";
} else {
    $casePICJoin = " LEFT JOIN";
}

if ($CaseReporter) {
    $cond .= " AND c.LastModifiedBy='" . $CaseReporter . "'";
}

if ($CaseDate) {
    if ($CaseDateStart) {
        $cond .= " AND c.StartDate>='" . $CaseDateStart . "'";
    }
    
    if ($CaseDateEnd) {
        $cond .= " AND c.EndDate<='" . $CaseDateEnd . "'";
    }
    $AcademicYearID = 'null';
} else {
    if (! $AcademicYearID) {
        $AcademicYearID = $currentAcademicYearID;
    }
    $startDate = getStartDateOfAcademicYear($AcademicYearID);
    $endDate = getEndDateOfAcademicYear($AcademicYearID);
    $cond .= " AND c.StartDate>='" . $startDate . "' AND c.StartDate<='" . $endDate . "'";
}

$student_name = getNameFieldByLang2('u.');
$casePIC_name = getNameFieldByLang2('u.');
$createdBy = getNameFieldByLang2('cb.');
$updatedBy = getNameFieldByLang2('ub.');

$keyword = $_POST['keyword'] ? standardizeFormPostValue($_POST['keyword']) : standardizeFormGetValue($keyword);
if ($keyword != "") {
    $kw = $li->Get_Safe_Sql_Like_Query($keyword);
    $cond .= " AND (s.Student LIKE '%$kw%' OR a.CasePICName LIKE '%$kw%')";
    unset($kw);
}

$isRequiredCheckSelfCase = $objMedical->isRequiredCheckSelfCase();
$checkSelfDeleteStr = "IF(c.InputBy=" . $_SESSION['UserID'] . ",1,0)";
$dataDeleteAllowValue = $isRequiredCheckSelfCase ? $checkSelfDeleteStr : '1';

$sql = "SELECT 
				CONCAT('<a href=\"?t=management.case_view&CaseID=',c.`CaseID`,'\">',c.CaseNo,'</a>'),
				c.StartDate,
				CONCAT(s.Student,' (',s.ClassName,'-',s.ClassNumber,')') AS Student,
				c.Summary,
				IF (c.IsClosed=1,'" . $Lang['medical']['case']['status']['Closed'] . "','" . $Lang['medical']['case']['status']['Processing'] . "') AS Status,
				a.CasePICName,
				{$createdBy} AS CreatedBy,
                {$updatedBy} AS UpdatedBy,
                c.DateModified,
				CONCAT('<input type=\'checkbox\' name=\'CaseID[]\' data-deleteAllow=\'',$dataDeleteAllowValue,'\' id=\'CaseID[]\' value=\'', c.`CaseID`,'\'>')
		FROM
				MEDICAL_CASE c
		INNER JOIN (
				SELECT 	u.UserID, 
						$classNameEng as ClassName, 
						$classNumber as ClassNumber,
						$student_name as Student
				FROM 
						INTRANET_USER u
				" . $joinClass . "
			) AS s ON s.UserID=c.StudentID
		" . $casePICJoin . " (
				SELECT 	CaseID, 
						GROUP_CONCAT(DISTINCT CONCAT('^',u.UserID,'~') ORDER BY u.UserID SEPARATOR ',') as CasePICID,
						GROUP_CONCAT(DISTINCT " . $casePIC_name . " ORDER BY " . $casePIC_name . " SEPARATOR ',<br>') AS CasePICName
				FROM 
						MEDICAL_CASE_PIC p
				INNER JOIN 
						INTRANET_USER u ON u.UserID=p.TeacherID
				GROUP BY CaseID
			) AS a ON a.CaseID=c.CaseID
		LEFT JOIN INTRANET_USER cb ON cb.UserID=c.InputBy
        LEFT JOIN INTRANET_USER ub ON ub.UserID=c.LastModifiedBy
		WHERE 1 " . $cond;
// debug_r($sql);

$li->field_array = array(
    "CaseNo",
    "StartDate",
    "Student",
    "Summary",
    "Status",
    "CasePICName",
    "CreatedBy",
    "UpdatedBy",
    "DateModified"
);
$li->sql = $sql;
$li->no_col = sizeof($li->field_array) + 2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0
);
$li->wrap_array = array(
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0
);
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='12%' >" . $li->column($pos ++, $Lang['medical']['case']['tableHeader']['CaseNo']) . "</th>\n";
$li->column_list .= "<th width='9%' >" . $li->column($pos ++, $Lang['medical']['case']['tableHeader']['StartDate']) . "</th>\n";
$li->column_list .= "<th width='12%' >" . $li->column($pos ++, $Lang['medical']['case']['tableHeader']['StudentName']) . "</th>\n";
$li->column_list .= "<th width='15%' >" . $li->column($pos ++, $Lang['medical']['case']['tableHeader']['Summary']) . "</th>\n";
$li->column_list .= "<th width='8%' >" . $li->column($pos ++, $Lang['medical']['case']['tableHeader']['Status']) . "</th>\n";
$li->column_list .= "<th width='10%' >" . $li->column($pos ++, $Lang['medical']['case']['tableHeader']['CasePIC']) . "</th>\n";
$li->column_list .= "<th width='10%' >" . $li->column($pos ++, $Lang['medical']['general']['inputBy']) . "</th>\n";
$li->column_list .= "<th width='10%' >" . $li->column($pos ++, $Lang['medical']['general']['lastModifiedBy']) . "</th>\n";
$li->column_list .= "<th width='10%' >" . $li->column($pos ++, $Lang['medical']['general']['dateModified']) . "</th>\n";
$li->column_list .= "<th width='1'>" . $li->check("CaseID[]") . "</th>\n";

$toolbar = $linterface->GET_LNK_NEW("javascript:checkGet2(document.form1,'?t=management.new_case')", $button_new, "", "", "", 0);

$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);

// ############ start Filters
// Acadermic Year Filter
$yearFilter = getSelectAcademicYear("AcademicYearID", 'onChange="changeAcademicYearFilter();"', 0, 0, $AcademicYearID);

// Class Filter
$lclass = new libclass();
$classFilter = $lclass->getSelectClass("name='ClassName' onChange='this.form.submit();'", $ClassName, "", $Lang['medical']['event']['AllClass'], $currentAcademicYearID);

// Group Filter
$groupFilter = $objMedical->getGroupSelection("GroupID", $GroupID, "", $Lang['medical']['event']['AllGroup']);

foreach ((array) $Lang['medical']['case']['status'] as $k => $v) {
    $statusAry[] = array(
        $k,
        $v
    );
}
$statusFilter = getSelectByArray($statusAry, "name='CaseStatus' onChange='this.form.submit();'", $CaseStatus, 0, 0, $Lang['medical']['case']['AllStatus']);

if (! empty($ClassName)) {
    if ($junior_mck) {
        $studentAry = $lclass->getStudentNameListWClassNumberByClassName($ClassName);
    } else {
        $studentAry = $objMedical->getStudentNameListWClassNumberByClassName($ClassName);
    }
} else {
    $studentAry = $objMedical->getAllStudents($currentAcademicYearID);
}
$studentFilter = getSelectByArray($studentAry, "name='CaseStudent' onChange='this.form.submit();'", $CaseStudent, 0, 0, $Lang['medical']['event']['AllStudent']);

$teacherAry = $objMedical->getTeacher();
$reporterFilter = getSelectByArray($teacherAry, "name='CaseReporter' onChange='this.form.submit();'", $CaseReporter, 0, 0, $Lang['medical']['case']['AllReporter']);

$casePICAry = $teacherAry;
$casePICFilter = getSelectByArray($casePICAry, "name='CasePIC' onChange='this.form.submit();'", $CasePIC, 0, 0, $Lang['medical']['case']['AllPIC']);

$CaseDateStart = $CaseDateStart ? $CaseDateStart : substr(getStartDateOfAcademicYear($currentAcademicYearID), 0, 10);
$CaseDateEnd = $CaseDateEnd ? $CaseDateEnd : substr(getEndDateOfAcademicYear($currentAcademicYearID), 0, 10);

// ############ end Filters

$CurrentPage = "ManagementCase";
$CurrentPageName = $Lang['medical']['menu']['case'];
$TAGS_OBJ[] = array(
    $Lang['medical']['menu']['case'],
    "",
    0
);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();

if ($returnMsgKey == 'DeleteNonSelfRecord') {
    $returnMsg = $Lang['medical']['general']['ReturnMessage']['NotAllowDeletingNonSelfRecord'];
}else {
    $returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
}
$linterface->LAYOUT_START($returnMsg);

include_once ('templates/case_list.tmpl.php');
$linterface->LAYOUT_STOP();

?>