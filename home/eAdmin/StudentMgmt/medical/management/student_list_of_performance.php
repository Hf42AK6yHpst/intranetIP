<?php
// using:
/*
 * 2018-04-09 Cameron
 * - add group filter
 * 
 * 2018-03-28 Cameron
 * - change button 'Submit' to 'View' in search toobar
 *
 * 2017-10-13 Cameron
 * - use $plugin['medical_module']['awardscheme'] to control awards scheme
 *
 * 2017-07-14 Cameron
 * - create this file
 */
if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'AWARDSCHEME_MANAGEMENT') || ! $plugin['medical_module']['awardscheme']) {
    header("Location: /");
    exit();
}

$arrCookies = array();
$arrCookies[] = array(
    "ck_page_size",
    "numPerPage"
);
$arrCookies[] = array(
    "ck_right_page_number",
    "pageNo"
);
$arrCookies[] = array(
    "ck_right_page_order",
    "order"
);
$arrCookies[] = array(
    "ck_right_page_field",
    "field"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_ClassName",
    "ClassName"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_SchemeStudent",
    "SchemeStudent"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_SchemeDateStart",
    "SchemeDateStart"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_SchemeDateEnd",
    "SchemeDateEnd"
);

if (isset($clearCoo) && $clearCoo == 1) {
    clearCookies($arrCookies);
} else {
    updateGetCookies($arrCookies);
}

if (isset($ck_page_size) && $ck_page_size != "")
    $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$pageNo = ($pageNo == "") ? 1 : $pageNo;
$order = ($order == "") ? 1 : $order;
$field = ($field == "") ? 0 : $field;
$li = new libdbtable2007($field, $order, $pageNo);

$currentAcademicYearID = Get_Current_Academic_Year_ID();
$today = date('Y-m-d');

$cond = '';

if ($junior_mck) {
    $classNameField = "u.ClassName";
    $classNumber = "u.ClassNumber";
    $classNameEng = $classNameField;
    $joinClass = '';
} else {
    $classNameField = Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN");
    $classNumber = "ycu.ClassNumber";
    $classNameEng = "yc.ClassTitleEN";
    $joinClass = " INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=u.UserID
					INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='" . $currentAcademicYearID . "' ";
}

if ($GroupID) {
    $joinClass .= "	INNER JOIN INTRANET_USERGROUP g ON g.UserID=u.UserID AND g.GroupID='" . $GroupID . "'";
}

if ($ClassName) {
    $cond .= " AND s.ClassName='" . $li->Get_Safe_Sql_Query($ClassName) . "'";
}

if ($SchemeStudent) {
    $cond .= " AND s.StudentID='" . $li->Get_Safe_Sql_Query($SchemeStudent) . "'";
}

if ($SchemeDate) {
    if ($SchemeDateStart) {
        $cond .= " AND sc.StartDate>='" . $SchemeDateStart . "'";
    }
    
    if ($SchemeDateEnd) {
        $cond .= " AND sc.EndDate<='" . $SchemeDateEnd . "'";
    }
}

$student_name = getNameFieldByLang('u.');

$keyword = standardizeFormPostValue($_POST['keyword']);
if ($keyword != "") {
    $kw = $li->Get_Safe_Sql_Like_Query($keyword);
    $cond .= " AND (sc.SchemeName LIKE '%$kw%' OR t.Target LIKE '%$kw%' OR s.Student LIKE '%$kw%')";
    unset($kw);
}

$sql = "SELECT 
				CONCAT('<a href=\"?t=management.performance_view&StudentID=',s.`StudentID`,'\">',CONCAT(s.Student,' (',s.ClassName,'-',s.ClassNumber,')'),'</a>'),
				a.SchemeName
		FROM   		(
						SELECT 	u.UserID as StudentID, 
								$classNameEng as ClassName, 
								$classNumber as ClassNumber,
								$student_name as Student
						FROM 
								INTRANET_USER u
						" . $joinClass . "
					) AS s
		INNER JOIN 	(
						SELECT 	t.StudentID, 
								GROUP_CONCAT(DISTINCT s.SchemeName ORDER BY s.SchemeName SEPARATOR ',') as SchemeName
						FROM 
								MEDICAL_AWARD_SCHEME_STUDENT_TARGET t
						INNER JOIN 
								MEDICAL_AWARD_SCHEME s ON s.SchemeID=t.SchemeID
						GROUP BY t.StudentID
					) AS a ON a.StudentID=s.StudentID
		LEFT JOIN
					MEDICAL_AWARD_SCHEME_STUDENT_TARGET t ON t.StudentID=s.StudentID
		LEFT JOIN
					MEDICAL_AWARD_SCHEME sc ON sc.SchemeID=t.SchemeID
		WHERE 		1 " . $cond . " GROUP BY s.StudentID";
// debug_r($sql);

$li->field_array = array(
    "s.Student",
    "SchemeName"
);
$li->sql = $sql;
$li->no_col = sizeof($li->field_array) + 1;
$li->title = $eDiscipline["Record"];
$li->column_array = array(
    0,
    0
);
$li->wrap_array = array(
    0,
    0
);
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='48%' >" . $li->column($pos ++, $Lang['medical']['award']['performance']['tableHeader']['StudentName']) . "</th>\n";
$li->column_list .= "<th width='48%' >" . $li->column($pos ++, $Lang['medical']['award']['tab']['Scheme']) . "</th>\n";

$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);

// ############ start Filters

// Class Filter
$lclass = new libclass();
$classFilter = $lclass->getSelectClass("name='ClassName' onChange='this.form.submit();'", $ClassName, "", $Lang['medical']['event']['AllClass'], $currentAcademicYearID);

// Group Filter
$groupFilter = $objMedical->getGroupSelection("GroupID", $GroupID, "onChange='this.form.submit();'", $Lang['medical']['event']['AllGroup']);

if (! empty($ClassName)) {
    if ($junior_mck) {
        $studentAry = $lclass->getStudentNameListWClassNumberByClassName($ClassName);
    } else {
        $studentAry = $objMedical->getStudentNameListWClassNumberByClassName($ClassName);
    }
} else {
    $studentAry = $objMedical->getAllStudents($currentAcademicYearID);
}
$studentFilter = getSelectByArray($studentAry, "name='SchemeStudent' onChange='this.form.submit();'", $SchemeStudent, 0, 0, $Lang['medical']['event']['AllStudent']);

$SchemeDateStart = $SchemeDateStart ? $SchemeDateStart : substr(getStartDateOfAcademicYear($currentAcademicYearID), 0, 10);
$SchemeDateEnd = $SchemeDateEnd ? $SchemeDateEnd : substr(getEndDateOfAcademicYear($currentAcademicYearID), 0, 10);

// ############ end Filters

$CurrentPage = "ManagementAward";
$CurrentPageName = $Lang['medical']['menu']['award'];
$TAGS_OBJ[] = array(
    $Lang['medical']['award']['tab']['Scheme'],
    "?t=management.award_list",
    0
);
$TAGS_OBJ[] = array(
    $Lang['medical']['award']['tab']['Performance'],
    "?t=management.student_list_of_performance",
    1
);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();

$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);
?>
<script>

$(document).ready(function(){
	$('#SchemeDate').click(function(){
		if ($(this).is(':checked')){
			$('#spanRequest').css('display','inline');
		}
		else {
			$('#spanRequest').css('display','none');
		}
	});	
});

</script>

<form name="form1" id="form1" method="POST"
	action="?t=management.student_list_of_performance">
	<div class="content_top_tool">

		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="20%"><?=$toolbar ?></td>
				<td width="50%" align="center">&nbsp;</td>
				<td width="30%" style="float: right;">
					<div class="content_top_tool" style="float: right;">
						<?=$htmlAry['searchBox']?>     
						<br style="clear: both" />
					</div>
				</td>
			</tr>
		</table>

	</div>

	<div class="table_board">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="bottom">
					<div class="table_filter">
					<?=$classFilter ?>
					<?=$groupFilter ?>
					<?=$studentFilter?>					
				</div> <br style="clear: both" />
					<div>
						<input type="checkbox" name="SchemeDate" id="SchemeDate" value="1"
							<?=$SchemeDate==1 ? "checked" : ""?>><label for="SchemeDate"><?=$Lang['medical']['filter']['FilterDateRange']?></label>
						<div id="spanRequest" style="position:relative;display:<?=$SchemeDate==1 ? "inline" : "none"?>">
							<label for="SchemeDateStart"><?=$i_From?></label> : <?=$linterface->GET_DATE_PICKER("SchemeDateStart",$SchemeDateStart)?>
					<label for="SchemeDateEnd"><?=$i_To ?></label> <?=$linterface->GET_DATE_PICKER("SchemeDateEnd",$SchemeDateEnd)?>
					 <?=$linterface->GET_ACTION_BTN($Lang['Btn']['View'], 'submit',$ParOnClick="", $ParName="submit1")?>
					</div>
					</div>
				</td>
			</tr>
		</table>
	</div>

	<div class="table_board">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="bottom">

				<?= $li->display() ?>
 
			</td>
			</tr>
		</table>
	</div>

	<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
	<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
	<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
	<input type="hidden" name="page_size_change" value="" /> <input
		type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

</form>
<?
$linterface->LAYOUT_STOP();
?>