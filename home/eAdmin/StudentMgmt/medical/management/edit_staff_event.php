<?php
// using:
/*
 * 2018-12-21 Cameron
 * - create this file
 */
if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'STAFFEVENT_EDIT') || ! $plugin['medical_module']['staffEvents']) {
    header("Location: /");
    exit();
}

if (is_array($StaffEventID)) {
    $StaffEventID = (count($StaffEventID) == 1) ? $StaffEventID[0] : '';
} else {
    $StaffEventID = $StaffEventID;
}

if (! $StaffEventID) {
    header("Location: ?t=management.staffEventList");
}

$staffEventAry = $objMedical->getStaffEvents($StaffEventID);
if (count($staffEventAry) == 1) {
    $staffEventAry = current($staffEventAry);
} else {
    header("Location: ?t=management.staffEventList");
}

$CurrentPage = "ManagementStaffEvent";
$CurrentPageName = $Lang['medical']['menu']['staffEvent'];
$TAGS_OBJ[] = array(
    $Lang['medical']['menu']['staffEvent'],
    "",
    0
);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();

$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$PAGE_NAVIGATION[] = array(
    $Lang['medical']['menu']['staffEvent'],
    "?t=management.staffEventList"
);
$PAGE_NAVIGATION[] = array(
    $Lang['Btn']['Edit'],
    ""
);

$staffEventTypeAry = $objMedical->getStaffEventType();
if (count($staffEventTypeAry) == 0) {
    header("Location: ?t=management.staffEventList&returnMsgKey=EventTypeNotSetup");
    exit();
}
$staffEventTypeSelection = getSelectByArray($staffEventTypeAry, 'Name="StaffEventTypeID" ID="StaffEventTypeID"', $staffEventAry['EventTypeID']);

$staffEventTypeLev2Ary = $objMedical->getStaffEventTypeLev2('',$staffEventAry['EventTypeID'], $idAndNameOnly=true);
$staffEventTypeLev2Selection = getSelectByArray($staffEventTypeLev2Ary, 'Name="StaffEventTypeLev2ID" ID="StaffEventTypeLev2ID"', $staffEventAry['EventTypeLev2ID']);

$buildingAry = $objMedical->getInventoryBuildingArray();
$buildingSelection = getSelectByArray($buildingAry, 'Name="StaffEventLocationBuildingID" ID="StaffEventLocationBuildingID"', $staffEventAry['LocationBuildingID']);

if ($staffEventAry['LocationBuildingID']) {
    $levelAry = $objMedical->getInventoryLevelArray($staffEventAry['LocationBuildingID']);
    $levelSelection = getSelectByArray($levelAry, "Name='StaffEventLocationLevelID' ID='StaffEventLocationLevelID' onChange='changeStaffEventBuildingLevel()'", $staffEventAry['LocationLevelID']);
}

if ($staffEventAry['LocationLevelID']) {
    $locationAry = $objMedical->getInventoryLocationArray($staffEventAry['LocationLevelID']);
    $locationSelection = getSelectByArray($locationAry, "Name='StaffEventLocationID' ID='StaffEventLocationID'", $staffEventAry['LocationID']);
}


$relevantStudentEvents = $objMedical->getStudentEventListOfStaffEvent($StaffEventID);

// # handle attachment(s)
$ret = $objMedical->handleTempUploadFolder();
$tempFolderPath = $ret['TempFolderPath'];
$use_plupload = $objMedical->is_use_plupload();

$linterface->LAYOUT_START($returnMsg);

$plupload = array();
$plupload['use_plupload'] = $use_plupload;
if ($use_plupload) {
    $plupload['pluploadButtonId'] = 'UploadButton';
    $plupload['pluploadDropTargetId'] = 'DropFileArea';
    $plupload['pluploadFileListDivId'] = 'FileListDiv';
    $plupload['pluploadContainerId'] = 'pluploadDiv';
}

$form_action = '?t=management.edit_staff_event_save';

include_once ('templates/staff_event.tmpl.php');

if ($use_plupload) {
    include_once ('plupload_script.php');
}

$linterface->LAYOUT_STOP();
?>