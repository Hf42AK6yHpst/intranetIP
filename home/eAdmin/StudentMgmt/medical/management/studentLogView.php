<?php
// using:
/*
 * 	2018-03-29 Cameron
 * 		- create this file
 */

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='STUDENTLOG_MANAGEMENT') || !$plugin['medical_module']['studentLog']){
	header("location: /");
	exit();
}

$studentLogID = $_GET['studentLogID'];

if (is_array($studentLogID)) {
	$studentLogID = (count($studentLogID)==1) ? $studentLogID[0] : '';
}
else {
	$studentLogID = $studentLogID;
}

if (!$studentLogID) {	
	header("Location: ?t=management.studentLog");
}

$rsStudentLog = $objMedical->getStudentLogInfo($studentLogID);
if (count($rsStudentLog) == 1) {	
	$rsStudentLog = current($rsStudentLog);
}
else {
	header("Location: ?t=management.studentLog");
}


$CurrentPage = "ManagementStudentLog";
$CurrentPageName = $Lang['medical']['menu']['studentLog'];
$TAGS_OBJ[] = array($Lang['medical']['menu']['studentLog'], "", 0);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();		

$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$PAGE_NAVIGATION[] = array($Lang['medical']['menu']['studentLog'], $ViewMode ? "" : "?t=management.studentLog");
$PAGE_NAVIGATION[] = array($Lang['Btn']['View'], "");

$studentLogObj = new StudentLog($studentLogID);
$userID = $studentLogObj->getUserID();
$eventDate = $studentLogObj->getRecordTime();
if ($eventDate != '' && $eventDate != '0000-00-00 00:00:00') {
    $eventDate = substr($eventDate,0,10);
}
else {
    $eventDate = '--';
}

# Student Info
$studentInfo = $objMedical->getForStudentLog($userID);

include_once('templates/studentLogView.tmpl.php');

?>