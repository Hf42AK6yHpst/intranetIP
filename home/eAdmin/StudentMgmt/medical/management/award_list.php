<?php
// using:
/*
 * 2018-12-27 Cameron
 * - show error message when deleting non-self record
 *  
 * 2018-06-05 Cameron
 * - show created by person [case #F133828]
 * - don't show title for people column (use getNameFieldByLang2 instead of getNameFieldByLang) 
 * 
 * 2018-02-27 Cameron
 * - add data-deleteAllow attribute to checkbox item for checking if it's allowed to delete [case #F135176]
 * - show DateModified and LastModifiedBy
 * 
 * 2018-01-23 Cameron
 * - change default order to descending by date
 *
 * 2017-11-28 Cameron
 * - fix search filter cookie
 *
 * 2017-10-13 Cameron
 * - use $plugin['medical_module']['awardscheme'] to control awards scheme
 *
 * 2017-06-14 Cameron
 * - create this file
 */
if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'AWARDSCHEME_MANAGEMENT') || ! $plugin['medical_module']['awardscheme']) {
    header("Location: /");
    exit();
}

$arrCookies = array();
$arrCookies[] = array(
    "ck_page_size",
    "numPerPage"
);
$arrCookies[] = array(
    "ck_right_page_number",
    "pageNo"
);
$arrCookies[] = array(
    "ck_right_page_order",
    "order"
);
$arrCookies[] = array(
    "ck_right_page_field",
    "field"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_SchemeStatus",
    "SchemeStatus"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_SchemePIC",
    "SchemePIC"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_SchemeDate",
    "SchemeDate"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_SchemeDateStart",
    "SchemeDateStart"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_SchemeDateEnd",
    "SchemeDateEnd"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_SchemeKeyword",
    "keyword"
);

if (isset($clearCoo) && $clearCoo == 1) {
    clearCookies($arrCookies);
} else {
    updateGetCookies($arrCookies);
}

if (isset($ck_page_size) && $ck_page_size != "")
    $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$pageNo = ($pageNo == "") ? 1 : $pageNo;
$order = ($order == "") ? 0 : $order; // default descending
$field = ($field == "") ? 0 : $field;
$li = new libdbtable2007($field, $order, $pageNo);

$currentAcademicYearID = Get_Current_Academic_Year_ID();
$today = date('Y-m-d');

switch ($SchemeStatus) {
    case 'Completed':
        $cond .= " AND s.EndDate<'" . $today . "'";
        break;
    case 'Processing':
        $cond .= " AND s.StartDate<='" . $today . "' AND s.EndDate>'" . $today . "'";
        break;
}

if ($SchemePIC) {
    $cond .= " AND a.SchemePICID LIKE '%" . $li->Get_Safe_Sql_Like_Query('^' . $SchemePIC . '~') . "%'";
    $schemePICJoin = " INNER JOIN";
} else {
    $schemePICJoin = " LEFT JOIN";
}

if ($SchemeDate) {
    if ($SchemeDateStart) {
        $cond .= " AND s.StartDate>='" . $SchemeDateStart . "'";
    }
    
    if ($SchemeDateEnd) {
        $cond .= " AND s.EndDate<='" . $SchemeDateEnd . "'";
    }
}

$schemePIC_name = getNameFieldByLang2('u.');
$createdBy = getNameFieldByLang2('cb.');
$updatedBy = getNameFieldByLang2('ub.');

$keyword = $_POST['keyword'] ? standardizeFormPostValue($_POST['keyword']) : standardizeFormGetValue($keyword);
if ($keyword != "") {
    $kw = $li->Get_Safe_Sql_Like_Query($keyword);
    $cond .= " AND (s.SchemeName LIKE '%$kw%' OR a.SchemePICName LIKE '%$kw%')";
    unset($kw);
}

$isRequiredCheckSelfAwardScheme = $objMedical->isRequiredCheckSelfAwardScheme();
$checkSelfDeleteStr = "IF(s.InputBy=" . $_SESSION['UserID'] . ",1,0)";
$dataDeleteAllowValue = $isRequiredCheckSelfAwardScheme ? $checkSelfDeleteStr : '1';

$sql = "SELECT 
				s.StartDate,
				s.EndDate,
				CONCAT('<a href=\"?t=management.award_view&SchemeID=',s.`SchemeID`,'\">',s.SchemeName,'</a>'),
				CASE WHEN s.StartDate<='" . $today . "' AND s.EndDate>'" . $today . "' THEN '" . $Lang['medical']['award']['status']['Processing'] . "'
					 WHEN s.EndDate<'" . $today . "' THEN '" . $Lang['medical']['award']['status']['Completed'] . "'
					 ELSE '-' 
				END as Status,
				a.SchemePICName,
				IF(s.Remark='' OR s.Remark is null, '-',s.Remark) as Remark,
				{$createdBy} AS CreatedBy,
                {$updatedBy} AS UpdatedBy,
                s.DateModified,
				CONCAT('<input type=\'checkbox\' name=\'SchemeID[]\' id=\'SchemeID[]\' data-deleteAllow=\'',$dataDeleteAllowValue,'\' value=\'', s.`SchemeID`,'\'>')
		FROM
				MEDICAL_AWARD_SCHEME s
		" . $schemePICJoin . " (
				SELECT 	SchemeID, 
						GROUP_CONCAT(DISTINCT CONCAT('^',u.UserID,'~') ORDER BY u.UserID SEPARATOR ',') as SchemePICID,
						GROUP_CONCAT(DISTINCT " . $schemePIC_name . " ORDER BY " . $schemePIC_name . " SEPARATOR ',<br>') AS SchemePICName
				FROM 
						MEDICAL_AWARD_SCHEME_PIC p
				INNER JOIN 
						INTRANET_USER u ON u.UserID=p.TeacherID
				GROUP BY SchemeID
			) AS a ON a.SchemeID=s.SchemeID
		LEFT JOIN INTRANET_USER cb ON cb.UserID=s.InputBy
        LEFT JOIN INTRANET_USER ub ON ub.UserID=s.LastModifiedBy
		WHERE 1 " . $cond;
// debug_r($sql);

$li->field_array = array(
    "StartDate",
    "EndDate",
    "SchemeName",
    "Status",
    "SchemePICName",
    "Remark",
    "CreatedBy",
    "UpdatedBy",
    "DateModified"
);
$li->sql = $sql;
$li->no_col = sizeof($li->field_array) + 2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0
);
$li->wrap_array = array(
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0
);
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='10%' >" . $li->column($pos ++, $Lang['General']['StartDate']) . "</th>\n";
$li->column_list .= "<th width='10%' >" . $li->column($pos ++, $Lang['General']['EndDate']) . "</th>\n";
$li->column_list .= "<th width='15%' >" . $li->column($pos ++, $Lang['medical']['award']['SchemeName']) . "</th>\n";
$li->column_list .= "<th width='8%' >" . $li->column($pos ++, $Lang['General']['Status2']) . "</th>\n";
$li->column_list .= "<th width='13%' >" . $li->column($pos ++, $Lang['medical']['award']['GroupPIC']) . "</th>\n";
$li->column_list .= "<th width='13%' >" . $li->column($pos ++, $Lang['General']['Remark']) . "</th>\n";
$li->column_list .= "<th width='10%' >" . $li->column($pos ++, $Lang['medical']['general']['inputBy']) . "</th>\n";
$li->column_list .= "<th width='10%' >" . $li->column($pos ++, $Lang['medical']['general']['lastModifiedBy']) . "</th>\n";
$li->column_list .= "<th width='10%' >" . $li->column($pos ++, $Lang['medical']['general']['dateModified']) . "</th>\n";
$li->column_list .= "<th width='1'>" . $li->check("SchemeID[]") . "</th>\n";

$toolbar = $linterface->GET_LNK_NEW("javascript:checkGet2(document.form1,'?t=management.new_award')", $button_new, "", "", "", 0);

$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);

// ############ start Filters

foreach ((array) $Lang['medical']['award']['status'] as $k => $v) {
    $statusAry[] = array(
        $k,
        $v
    );
}
$statusFilter = getSelectByArray($statusAry, "name='SchemeStatus' onChange='this.form.submit();'", $SchemeStatus, 0, 0, $Lang['medical']['award']['AllStatus']);

$teacherAry = $objMedical->getTeacher();
$schemePICFilter = getSelectByArray($teacherAry, "name='SchemePIC' onChange='this.form.submit();'", $SchemePIC, 0, 0, $Lang['medical']['award']['AllGroupPIC']);

$SchemeDateStart = $SchemeDateStart ? $SchemeDateStart : substr(getStartDateOfAcademicYear($currentAcademicYearID), 0, 10);
$SchemeDateEnd = $SchemeDateEnd ? $SchemeDateEnd : substr(getEndDateOfAcademicYear($currentAcademicYearID), 0, 10);

// ############ end Filters

$CurrentPage = "ManagementAward";
$CurrentPageName = $Lang['medical']['menu']['award'];
// $TAGS_OBJ[] = array($Lang['medical']['menu']['award'], "", $curTab=0);
$TAGS_OBJ[] = array(
    $Lang['medical']['award']['tab']['Scheme'],
    "?t=management.award_list",
    1
);
$TAGS_OBJ[] = array(
    $Lang['medical']['award']['tab']['Performance'],
    "?t=management.student_list_of_performance",
    0
);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();

if ($returnMsgKey == 'DeleteNonSelfRecord') {
    $returnMsg = $Lang['medical']['general']['ReturnMessage']['NotAllowDeletingNonSelfRecord'];
}else {
    $returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
}
$linterface->LAYOUT_START($returnMsg);

include_once ('templates/award_list.tmpl.php');
$linterface->LAYOUT_STOP();
?>