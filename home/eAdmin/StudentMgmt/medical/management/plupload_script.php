<?php
// Editing by 
/*
 * 2016-06-16 (Cameron): Copy from digital archive. Included in new.php
 */
/*
 * Variables required: 
 * $pluploadButtonId - button id for triggering file selection dialog
 * $pluploadDropTargetId - droppable element id for drag and drop files
 * $pluploadFileListDivId - container element id for displaying uploaded file name, file size and upload status
 * $tempFolderPath - temp folder name (not the whole folder path, because it is visible in js coding) for storing uploaded files
 */
include_once($intranet_root."/includes/libdb.php");
include_once($intranet_root."/includes/libfilesystem.php");
include_once($intranet_root."/includes/libinterface.php");
include_once($intranet_root."/includes/cust/medical/medicalConfig.inc.php");

$lfs = new libfilesystem();
$linterface = new interface_html();

$MaxFileSize = $medical_cfg['general']['MaxFileSize']; // MB
$MaxFileSizeInBytes = $MaxFileSize * 1024 * 1024; // bytes

//$PATH_WRT_ROOT = "../../../../../"; 
echo $linterface->Include_Plupload_JS_CSS();
?>
<style type="text/css">
.DropFileArea
{
	width:100%;
	height:30px;
	line-height:30px;
	text-align:center;
	vertical-align:middle;
	border:2px dashed #CCCCCC;
}

.DropFileAreaDragOver 
{
	width:100%;
	height:30px;
	line-height:30px;
	text-align:center;
	vertical-align:middle;
	border: 2px dashed #00FF55;
}
</style>
<script type="text/javascript" language="JavaScript">
var TargetFolder = '<?=$tempFolderPath?>';
var MaxFileSizeInBytes = <?=$MaxFileSizeInBytes?>;

function jsDeleteTempFile(fileId)
{
	var filename = $('div#'+fileId + ' span.file_name').html();
	
	$.post(
		'?t=management.plupload_remove_file&ma=1',
		{
			'TargetFolder':TargetFolder,
			'FileName':encodeURIComponent(filename)
		},
		function(data){
			$('div#'+fileId).remove();
		}
	);
}

function jsDeleteTempFileByName(filename)
{
	$.post(
		'?t=management.plupload_remove_file&ma=1',
		{
			'TargetFolder':TargetFolder,
			'FileName':encodeURIComponent(filename)
		},
		function(data){
			
		}
	);
}

function jsIsFileUploadCompleted()
{
	var totalSelected = uploader.files.length;
	var totalDone = 0;
	for(var i=0;i<uploader.files.length;i++) {
		if(uploader.files[i].status == plupload.DONE) {
			totalDone++;
		}
	}
	
	return totalDone == totalSelected;
}

function jsNumberOfFileUploaded()
{
	var dv = $('#<?=$plupload['pluploadFileListDivId']?> div');
	return dv.length;
}

var uploader = new plupload.Uploader({
			        runtimes : 'html5,flash,silverlight,html4,gears,browserplus',
			        autostart : true,
			        browse_button : '<?=$plupload['pluploadButtonId']?>',
			        drop_element : '<?=$plupload['pluploadDropTargetId']?>',
			        container : '<?=$plupload['pluploadContainerId']?>',
					flash_swf_url : '<?=$PATH_WRT_ROOT?>templates/jquery/plupload/plupload.flash.swf',
					silverlight_xap_url : '<?=$PATH_WRT_ROOT?>templates/jquery/plupload/plupload.silverlight.xap',
					url : '?t=management.plupload_process&TargetFolder=<?=$tempFolderPath?>',
			<? if(is_numeric($MaxFileSize) && $MaxFileSize > 0) { ?>		
					max_file_size : '<?=$MaxFileSize?>mb',
			<? } ?>
					chunk_size : '1mb',
					unique_names :false
			    });

uploader.bind('Init', function(up, params) {
    if (uploader.features.dragdrop) {
		var target = document.getElementById("<?=$plupload['pluploadDropTargetId']?>");
	      
	    target.ondragover = function(event) {
	        event.dataTransfer.dropEffect = "copy";
	        this.className = "DropFileAreaDragOver";
	    };
	      
	    target.ondragenter = function() {
	        this.className = "DropFileAreaDragOver";
	    };
	      
	    target.ondragleave = function() {
	        this.className = "DropFileArea";
	    };
	      
	    target.ondrop = function(event) {
	    	event.preventDefault();
	        this.className = "DropFileArea";
	    };
	      
	    $('#<?=$plupload['pluploadDropTargetId']?>').show();
      	
    }else{
    	$('#<?=$plupload['pluploadDropTargetId']?>').hide();
    }
});

uploader.init();

uploader.bind('FilesAdded', function(up, files) {
    $.each(files, function(i, file) {
    	var ok = true;
    	if(MaxFileSizeInBytes > 0 && file.size > MaxFileSizeInBytes) {
    		ok = false;
    	}
    	if(ok) {    		
	        $('#<?=$plupload['pluploadFileListDivId']?>').append(
	            '<div id="' + file.id + '">' +
	            '<span class="file_name">' + file.name + '</span> (' + plupload.formatSize(file.size) + ') <b></b>' +
	        	'</div>'
	        );
    	}else{
    		jsDeleteTempFileByName(file.name);
    		up.removeFile(file);
    	}
    });
	
    up.refresh(); // Reposition Flash/Silverlight
    up.start();
});

uploader.bind('UploadProgress', function(up, file) {
    $('#' + file.id + " b").html(file.percent + "%");
});

uploader.bind('UploadComplete', function(up, files) {
	$.each(files, function(i, file) {
		if( $('#' + file.id + ' a').length == 0) {
			$('#' + file.id).append('<a href="javascript:void(0);" onclick="jsDeleteTempFile(\''+file.id+'\');"> [ <?=$Lang['Btn']['Delete']?> ]</a>');
		}
	});
});

</script>
<?
// reset path
//$PATH_WRT_ROOT = "../../../../";
?>