<?php
// using:
/*
 * 2018-04-09 Cameron
 * - fix: should pass parameter array to array_diff
 *
 * 2017-10-13 Cameron
 * - use $plugin['medical_module']['awardscheme'] to control awards scheme
 *
 * 2017-07-06 Cameron
 * - create this file
 */
if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'AWARDSCHEME_MANAGEMENT') || ! $plugin['medical_module']['awardscheme']) {
    header("Location: /");
    exit();
}

$ldb = new libdb();
$result = array();
$group = array();
$originalGroupIDAry = array();

$originalScheme = $objMedical->getSchemes($SchemeID);
if (count($originalScheme) == 1) {
    $originalScheme = current($originalScheme);
    $oldShemeDate = $objMedical->getSchemeDateAry($originalScheme['StartDate'], $originalScheme['EndDate']);
    $newShemeDate = $objMedical->getSchemeDateAry($StartDate, $EndDate);
    $date_to_delete = array_diff($oldShemeDate, $newShemeDate);
    $ret = $objMedical->checkSchemeDateRangeChange($SchemeID, $StartDate, $EndDate);
    $update_performance = $ret[1];
    
    $originalGroupIDAry = explode(',', $originalScheme['GroupID']);
}
$grouIDtoAddAry = array_diff((array) $GroupID, $originalGroupIDAry); // GroupID to add
$grouIDtoDelAry = array_diff($originalGroupIDAry, (array) $GroupID); // GroupID to remove
$performanceExist = $objMedical->checkPerformanceExistByGroupID($SchemeID, $grouIDtoDelAry);
if ($performanceExist) {
    $result[] = false;
} else {
    $dataAry['SchemeName'] = standardizeFormPostValue($SchemeName);
    $dataAry['StartDate'] = $StartDate;
    $dataAry['EndDate'] = $EndDate;
    $dataAry['GroupID'] = implode(",", (array) $GroupID);
    $dataAry['Remark'] = standardizeFormPostValue($Remark);
    $dataAry['LastModifiedBy'] = $_SESSION['UserID'];
    $sql = $objMedical->UPDATE2TABLE('MEDICAL_AWARD_SCHEME', $dataAry, array(
        'SchemeID' => $SchemeID
    ), false);
    
    $ldb->Start_Trans();
    // # step 1: update to award scheme
    $result[] = $ldb->db_db_query($sql);
    
    $teacherID = array();
    if ($TeacherID) {
        foreach ((array) $TeacherID as $id) {
            if ($id) {
                $teacherID[] = str_replace('U', '', $id);
            }
        }
        
        if ($teacherID) {
            // # step 2: delete award scheme PIC that's not in updated list
            $sql = "DELETE FROM MEDICAL_AWARD_SCHEME_PIC WHERE SchemeID='" . $SchemeID . "' AND TeacherID NOT IN ('" . implode("','", (array) $teacherID) . "')";
            $result[] = $ldb->db_db_query($sql);
        }
    }
    
    // # step 3: get present scheme PIC list after delete in step 2
    $present_scheme_PIC = $objMedical->getSchemePIC($SchemeID);
    $present_scheme_PIC = Get_Array_By_Key($present_scheme_PIC, 'UserID');
    
    foreach ((array) $teacherID as $id) {
        if (! in_array($id, (array) $present_scheme_PIC)) { // add scheme PIC
            unset($dataAry);
            $dataAry['SchemeID'] = $SchemeID;
            $dataAry['TeacherID'] = $id;
            $sql = $objMedical->INSERT2TABLE('MEDICAL_AWARD_SCHEME_PIC', $dataAry, array(), false, false, false); // no DateModified field
            $result[] = $ldb->db_db_query($sql);
        }
    }
    unset($present_scheme_PIC);
    
    // # step 4: update scheme target
    if (count($TargetID)) {
        
        foreach ((array) $TargetID as $k => $id) {
            unset($dataAry);
            $dataAry['SchemeID'] = $SchemeID;
            $dataAry['Target'] = standardizeFormPostValue($target_name_e[$k]);
            unset($group);
            
            for ($j = 0, $jMax = count($GroupID); $j < $jMax; $j ++) {
                if ($target_group_e[$GroupID[$j]][$id]) {
                    $group[] = $GroupID[$j];
                }
            }
            $dataAry['GroupID'] = implode(",", (array) $group);
            $dataAry['LastModifiedBy'] = $_SESSION['UserID'];
            
            $sql = $objMedical->UPDATE2TABLE('MEDICAL_AWARD_SCHEME_TARGET', $dataAry, array(
                'TargetID' => $id
            ), false);
            
            $result[] = $ldb->db_db_query($sql);
        }
    }
    
    // # step 5: add new scheme target
    for ($i = 0, $iMax = count($target_name_n); $i < $iMax; $i ++) {
        unset($dataAry);
        $dataAry['SchemeID'] = $SchemeID;
        $dataAry['Target'] = standardizeFormPostValue($target_name_n[$i]);
        $group = array();
        for ($j = 0, $jMax = count($GroupID); $j < $jMax; $j ++) {
            if ($target_group_n[$GroupID[$j]][$i + 1]) {
                $group[] = $GroupID[$j];
            }
        }
        $dataAry['GroupID'] = implode(",", (array) $group);
        $dataAry['LastModifiedBy'] = $_SESSION['UserID'];
        $sql = $objMedical->INSERT2TABLE('MEDICAL_AWARD_SCHEME_TARGET', $dataAry, array(), false);
        $result[] = $ldb->db_db_query($sql);
        unset($group);
    }
    
    // # step 6: update performance (delete record in the list)
    if ($update_performance) {
        
        $nr_date_to_delete = count($date_to_delete);
        
        if ($nr_date_to_delete) {
            $rs = $objMedical->getPerformanceByDate($SchemeID, $date_to_delete);
            $nrRec = count($rs);
            
            if ($nrRec) {
                for ($i = 0; $i < $nrRec; $i ++) {
                    $perf = '';
                    $performance = explode("^~", $rs[$i]['Performance']);
                    foreach ((array) $performance as $val) {
                        list ($date, $score) = explode(",", $val);
                        if (! in_array($date, $date_to_delete)) {
                            $perf .= ($perf == '') ? '' : '^~'; // column record splitter
                            $perf .= ($date) ? $date : '';
                            $perf .= ','; // date & score splitter
                            $perf .= ($score === '') ? '' : $score;
                        }
                    }
                    
                    unset($dataAry);
                    $dataAry['Performance'] = $perf;
                    $dataAry['LastModifiedBy'] = $_SESSION['UserID'];
                    $sql = $objMedical->UPDATE2TABLE('MEDICAL_AWARD_SCHEME_STUDENT_TARGET', $dataAry, array(
                        'StudentTargetID' => $rs[$i]['StudentTargetID']
                    ), false);
                    $result[] = $ldb->db_db_query($sql);
                }
            }
        }
    }
}

if (! in_array(false, $result)) {
    $ldb->Commit_Trans();
    $returnMsgKey = 'UpdateSuccess';
} else {
    $ldb->RollBack_Trans();
    $returnMsgKey = 'UpdateUnsuccess';
}

header("location: ?t=management.award_list&returnMsgKey=" . $returnMsgKey);

?>