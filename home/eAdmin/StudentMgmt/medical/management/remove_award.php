<?php
// using:
/*
 * 2018-12-27 Cameron
 * - fix sql in checking self record
 *  
 * 2018-02-27 Cameron
 * - allow to delete self record only for non-super-admin user [case #F135176]
 *
 * 2018-02-01 Cameron
 * - Log delete record for trace purpose
 *
 * 2017-10-13 Cameron
 * - use $plugin['medical_module']['awardscheme'] to control awards scheme
 *
 * 2017-07-06 Cameron
 * - create this file
 */
if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'AWARDSCHEME_MANAGEMENT') || ! $plugin['medical_module']['awardscheme']) {
    header("Location: /");
    exit();
}

$ldb = new libdb();
$lfs = new libfilesystem();
$result = array();
$dataAry = array();
$isRequiredCheckSelfAwardScheme = $objMedical->isRequiredCheckSelfAwardScheme();
$returnMsgKey = '';

if (count($SchemeID) > 0) {
    $isAllowedToDelete = true;
    if ($isRequiredCheckSelfAwardScheme) {
        $sql = "SELECT SchemeID FROM MEDICAL_AWARD_SCHEME WHERE SchemeID IN ('" . implode("','", $SchemeID) . "') AND InputBy<>'" . $_SESSION['UserID'] . "'";
        $checkResult = $ldb->returnResultSet($sql);
        if (count($checkResult)) {
            $isAllowedToDelete = false;
            $returnMsgKey = 'DeleteNonSelfRecord';
        }
    }
    
    if ($isAllowedToDelete) {
        $ldb->Start_Trans();
        
        // log delete record
        foreach ((array) $SchemeID as $schemeID) {
            $sql = "SELECT CONCAT(Target,'^^',GroupID) AS TargetGroup FROM MEDICAL_AWARD_SCHEME_TARGET WHERE SchemeID='{$schemeID}'";
            $rs = $ldb->returnResultSet($sql);
            if (count($rs)) {
                $targetGroupAry = BuildMultiKeyAssoc($rs, "TargetGroup", array(
                    "TargetGroup"
                ), $SingleValue = 1);
                $targetGroups = implode('^~', $targetGroupAry);
            } else {
                $targetGroups = '';
            }
            
            $sql = "SELECT TeacherID FROM MEDICAL_AWARD_SCHEME_PIC WHERE SchemeID='{$schemeID}'";
            $rs = $ldb->returnResultSet($sql);
            if (count($rs)) {
                $teacherIDAry = BuildMultiKeyAssoc($rs, "TeacherID", array(
                    "TeacherID"
                ), $SingleValue = 1);
                $teacherIDs = implode(',', $teacherIDAry);
            } else {
                $teacherIDs = '';
            }
            
            $schemeAry = $objMedical->getSchemes($schemeID);
            $scheme = current($schemeAry);
            
            unset($dataAry);
            $dataAry['TableName'] = 'MEDICAL_AWARD_SCHEME';
            $dataAry['RecordID'] = $schemeID;
            $dataAry['StartDate'] = $scheme['StartDate'];
            $dataAry['EndDate'] = $scheme['EndDate'];
            $dataAry['RefID'] = $scheme['GroupID'];
            $dataAry['Summary'] = $scheme['SchemeName'];
            $dataAry['Detail'] = $targetGroups;
            $dataAry['OtherRef'] = $teacherIDs;
            $dataAry['LastModifiedBy'] = $_SESSION['UserID'];
            $sql = $objMedical->INSERT2TABLE('MEDICAL_DELETE_LOG', $dataAry, $condition = array(), $run = false);
            
            $result[] = $ldb->db_db_query($sql);
            
            $studentTargetAry = $objMedical->getSchemeStudentTarget($schemeID);
            
            if (count($studentTargetAry)) {
                foreach ((array) $studentTargetAry as $studentTarget) {
                    unset($dataAry);
                    $dataAry['TableName'] = 'MEDICAL_AWARD_SCHEME_STUDENT_TARGET';
                    $dataAry['RecordID'] = $studentTarget['StudentTargetID'];
                    $dataAry['RefID'] = $schemeID;
                    $dataAry['Summary'] = $studentTarget['Target'];
                    $dataAry['Detail'] = $studentTarget['Performance'];
                    $dataAry['StudentID'] = $studentTarget['StudentID'];
                    $dataAry['OtherRef'] = $studentTarget['GroupID'];
                    $dataAry['LastModifiedBy'] = $_SESSION['UserID'];
                    $sql = $objMedical->INSERT2TABLE('MEDICAL_DELETE_LOG', $dataAry, $condition = array(), $run = false);
                    
                    $result[] = $ldb->db_db_query($sql);
                }
            }
        }
        
        // delete award scheme
        $sql = "DELETE FROM MEDICAL_AWARD_SCHEME WHERE SchemeID IN ('" . implode("','", $SchemeID) . "')";
        $result[] = $ldb->db_db_query($sql);
        
        $sql = "DELETE FROM MEDICAL_AWARD_SCHEME_TARGET WHERE SchemeID IN ('" . implode("','", $SchemeID) . "')";
        $result[] = $ldb->db_db_query($sql);
        
        $sql = "DELETE FROM MEDICAL_AWARD_SCHEME_PIC WHERE SchemeID IN ('" . implode("','", $SchemeID) . "')";
        $result[] = $ldb->db_db_query($sql);
        
        $sql = "DELETE FROM MEDICAL_AWARD_SCHEME_STUDENT_TARGET WHERE SchemeID IN ('" . implode("','", $SchemeID) . "')";
        $result[] = $ldb->db_db_query($sql);
        
        if (! in_array(false, $result)) {
            $ldb->Commit_Trans();
            $returnMsgKey = 'DeleteSuccess';
        } else {
            $ldb->RollBack_Trans();
            $returnMsgKey = 'DeleteUnsuccess';
        }
    }
}

header("location: ?t=management.award_list&returnMsgKey=" . $returnMsgKey);

?>