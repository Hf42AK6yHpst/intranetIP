<?php
// using:
/*
 * 	2017-07-25 Cameron
 * 		- add filter: gender	[case#P120778]
 */ 

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='MEAL_MANAGEMENT') || !$plugin['medical_module']['meal']){
	header("Location: /");
	exit();
}
$CurrentPage = "ManagementMeal";
$TAGS_OBJ[] = array($Lang['medical']['menu']['meal'], "", 0);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();		
//$MODULE_OBJ['title'] = $Lang['medical']['menu']['meal'];

$objFCM_UI = new form_class_manage_ui;
$menuOption = array();
getSearchMenu($menuOption, $linterface, $objFCM_UI);

function getSearchMenu(&$menuOption, $linterface, $objFCM_UI){
	global $Lang,$objMedical;
	
//	$menuOptionCount = count($Lang['medical']['meal']['searchMenu']);
	$i=0;
	
	# Handle those cases with fixed option
	foreach($Lang['medical']['meal']['searchMenu'] as $key=>$menuItem){
		$itemHTML ='';
		$menuOption[$i]['Name'] = $menuItem;
		if($key == 'timePeriod'){ // Time period change to radio button
			for ($j = 0, $jMax = count($Lang['medical']['meal']['search']['timePeriodOption']); $j < $jMax; $j++) {
				$checked = '';
				if($j == 0){
					$checked = 'checked';
				}
				$itemHTML .= '<input type="radio" id="'.$key.$j.'" name="timePeriod" value="'.($j+1).'" '.$checked.'/>';
				$itemHTML .= '<label for="'.$key.$j.'">'.$Lang['medical']['meal']['search']['timePeriodOption'][$j].'</label>&nbsp;&nbsp;';
			}
		}else{
			
			if(is_array($Lang['medical']['meal']['search'][$key.'Option'])){
				$itemHTML .="<select name='{$key}' id='{$key}'>";		
		
				$j=1; //set key value
				foreach( $Lang['medical']['meal']['search'][$key.'Option'] as $itemName){
					$selected = '';
					if( $j == $_POST[$key]){
						$selected = 'selected';
					}
					$itemHTML .= "<option value='{$j}' {$selected}>{$itemName}</option>";
					++$j;
				}
				$itemHTML .='</select>';
			}		
		}
		$menuOption[$i]['Detail'] = $itemHTML;
		++$i;
	}
	
	# For Date Picker
	$menuOption[0]['Detail'] = $linterface->GET_DATE_PICKER($Name="date",$DefaultValue="",$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum");
	
	# For Class List
	$menuOption[1]['Detail'] = 	$objFCM_UI->Get_Class_Selection($AcademicYearID = Get_Current_Academic_Year_ID(), $YearID='', $ID_Name='classID', $SelectedYearClassID='', $Onchange='', $noFirst=0, $isMultiple=0, $isAll=1, $TeachingOnly=0);
	
	# For Group List
	$menuOption[2]['Detail'] = $objMedical->getGroupSelection($name='groupID');
	
	# Gender
//	$genderIdx = count($menuOption);
//	$menuOption[$genderIdx]['Name'] = $Lang['medical']['report_meal']['searchMenu']['gender'];
	$genderArray =array();
	$genderArray[] = array('M',$Lang['medical']['report_meal']['search']['genderOption']['M']);
	$genderArray[] = array('F',$Lang['medical']['report_meal']['search']['genderOption']['F']);
	$menuOption[4]['Detail'] = getSelectByArray($genderArray, 'name="gender" id="gender"', $selected="", $all=0, $noFirst=0, $FirstTitle=$Lang['medical']['report_meal']['search']['genderOption']['all'], $ParQuoteValue=1);
	
}


$linterface->LAYOUT_START($Msg);
//debug_r("<----".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n");
include_once('templates/meal.tmpl.php');
$linterface->LAYOUT_STOP();
?>