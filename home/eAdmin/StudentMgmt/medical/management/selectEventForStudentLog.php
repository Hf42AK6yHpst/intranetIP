<?php 
/*
 *  2018-04-03 Cameron
 * - create this file
 * 
 */

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='STUDENTLOG_MANAGEMENT') || !$plugin['medical_module']['studentLog']){
    header("Location: /");
    exit();
}

$formId = $_GET['FormId'];

include ($intranet_root . '/home/eAdmin/StudentMgmt/medical/management/select_event.php');
?>
<script>
function checkDuplicateSelectEvent() {
	var ret = true;
	$(":input[name^='EventID']:checked").each(function(){
		var thisEventID = $(this).val();
		$(":input[name^='event\\[<?php echo $formId;?>\\]\\[event\\]\\[\\]']").each(function(){
			if ($(this).val() == thisEventID) {
				ret = false;
			}	
		})
	});
	return ret;	
}

function selectEvents() {
    if(countChecked(document.form2,"EventID[]")==0) {
    	alert(globalAlertMsg2);
    }
    else if (!checkDuplicateSelectEvent()) {
    	alert('<?=$Lang['medical']['case']['Warning']['DuplicateEvent']?>');
    } 
    else{
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '?t=management.ajax.ajax&ma=1&action=getSelectedEventsForStudentLog&FormId=<?php echo $formId;?>',
			data : $('#form2').serialize(),
			success: returnEvents,
			error: show_ajax_error
		});

    	js_Hide_ThickBox();
    }
}

function returnEvents(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		var table = $('.eventTable<?php echo $formId?>');
		table.css('display','');
		table.append(ajaxReturn.html);
		var form = table.closest('form');
		form.find('.hasSave').val('0');
	}
}	

</script>