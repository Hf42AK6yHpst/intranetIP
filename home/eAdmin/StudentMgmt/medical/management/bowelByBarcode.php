<?php
// using: 
/*
 * Note: Please use UTF-8 for editing
 *
 * 2019-06-21 Cameron
 * - input student barcode first, then bowel status barcode [case #P162222]
 * - place newly added record on top of the table
 * 
 * 2019-05-28 Cameron
 * - modify the operation process to: [P161843]
 * (1) cursor focus on student barcode after scan bowel condition barcode,
 * (2) clear all barcodes after adding record 
 * 
 * 2019-05-27 Cameron
 * - change bowelStatus input method from radio button to barcode input [case #P160784]
 * 
 * 2018-06-13 Cameron
 * - use radio button instead of drop down list for bowel status [case #P132488]  
 * 
 * 2018-05-28 Cameron
 * - show checking result (invalid barcode, attendance status and duplicate record) in addBowelByBarcode
 *  
 * 2018-05-09 Cameron
 * - create this file
 */
if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'BOWEL_MANAGEMENT') || ! $plugin['medical_module']['bowel']) {
    header("Location: /");
    exit();
}
$CurrentPage = "ManagementBowel";

$TAGS_OBJ[] = array(
    $Lang['medical']['bowel']['tab']['Bowel'],
    "?t=management.bowel",
    0
);
$TAGS_OBJ[] = array(
    $Lang['medical']['bowel']['tab']['Barcode'],
    "?t=management.bowelByBarcode",
    1
);

//$PAGE_NAVIGATION[] = array($Lang['medical']['bowel']['tab']['Barcode'], "?t=management.bowelByBarcode");
$PAGE_NAVIGATION[] = array($Lang['Btn']['New'], "");

$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();

$objBowelStatus = new bowelStatus();


### Generate Legend HTML Start
// $legendHTML ='';
// $legendHTML .='<ul class="legend" style="list-style-type: none; padding-left:6px;">';

// $bowelDetail = $objBowelStatus->getActiveStatus(' StatusCode ASC ');

// $defaultBowelColorID = '';
// foreach( (array)$bowelDetail as $bowelItem){
//     if($bowelItem['IsDefault']) {
//         $checked = ' checked';
//         $defaultBowelColorID = $bowelItem['StatusID'];
//     }
//     else {
//         $checked = '';
//     }
//     $legendHTML .='<li ><label><input type="radio" name="bowelStatus" id="bowelStatus_'.$bowelItem['StatusID'].'" value="'.$bowelItem['StatusID'].'" '.$checked.'> <span class="colorBoxStyle" style="background-color:'.$bowelItem['Color'].';">&nbsp;&nbsp;&nbsp;</span>&nbsp;'. stripslashes($bowelItem['StatusName']).'</label></li>';
// }
// $legendHTML .='</ul>';
### Generate Legend HTML End


$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey] ? $Lang['General']['ReturnMessage'][$returnMsgKey] : '';
$linterface->LAYOUT_START($returnMsg);

#### Get default remarks ####
$objlibMedical = new libMedical();
$defaultRemark = $objlibMedical->getDefaultRemarksArray($medical_cfg['general']['module']['bowel']);
//////////////////////////////// Presentation Layer //////////////////////////////////////
echo <<< REMARKS_INIT
	<script>
		var jRemarksArr=$defaultRemark;
	</script>
REMARKS_INIT;
?>

<style>
.legend li{
    margin: 5px;
}
.colorBoxStyle{
	display: inline-block;
	border: 1px solid #c0c0c0;
	width:17px;
	height:17px;
}
.colorBoxStyle2{
	display: inline-block;
	border: 1px solid #c0c0c0;
	width:12px;
	height:12px;
}
.defaultRemarkSpan{
	margin-left:5px;
}
.barcode {
    width: 165px;
}
#remarks {
  width: 216px;
  height: 80px;
  vertical-align: top;
}
</style>

<form name="form1" id="form1" method="post" action="">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" class="form_table_v30">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['bowel']['StudentBarCode']?><span class="tabletextrequire">*</span></td>
								<td class="tabletext">
									<input type="text" name="barcode" id="barcode" class="barcode" value="">
									<input name="btnBarcode" id='btnBarcode' type="button"	class="formsubbutton" value="&crarr; <?php echo $Lang['medical']['bowel']['enter'];?>" />
									<input type="hidden" name="studentBarcode" id="studentBarcode" value="">
									<div id="divStudentBarcode"></div>
								</td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['bowel']['tableHeader']['Remarks']?></td>
								<td class="tabletext">
									<textarea name="remarks" id="remarks" onkeyup="resizeTextArea(this);"></textarea>
									<span class="defaultRemarkSpan"><?php echo GetPresetText("jRemarksArr", "1", "remarks", "", "", "", "1", "15");  ?></span>
								</td>
							</tr>
						
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['bowel']['BowelStatusBarCode']?><span class="tabletextrequire">*</span></td>
								<td class="tabletext"><?php // echo $legendHTML;?>
									<input type="text" name="bowelStatusBarcode" id="bowelStatusBarcode" class="barcode" value="">
									<input name="btnBowelStatus" id='btnBowelStatus' type="button"	class="formsubbutton" value="&crarr; <?php echo $Lang['medical']['bowel']['enter'];?>" />
									<input type="hidden" name="bowelStatus" id="bowelStatus" value="">
									<div id="divBowelStatus"></div>
								</td>
							</tr>

							<tr>
								<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
								<td width="80%">&nbsp;</td>
							</tr>
							
						</table>
						
					</td>
				</tr>
			</table>
			
    		<table id='bowel_table' class="common_table_list">
            	<colgroup>
            		<col style="width:1%"/>
            		<col style="width:6%"/>
            		<col style="width:6%"/>
            		<col style="width:6%"/>
            		<col style="width:8%"/>
            		<col style="width:15%"/>
            		<col style="width:12%"/>
            		<col style="width:17%"/>
            		<col style="width:10%"/>
            		<col style="width:15%"/>
            		<col style="width:4%"/>
            	</colgroup>
            
            	<thead>
            		<tr class="tabletop">
            			<th class="tabletoplink" nowrap><?=$Lang['medical']['bowel']['tableHeader']['Number'] ?></th>
            			<th class="tabletoplink" nowrap><?=$Lang['medical']['bowel']['tableHeader']['ClassName'] ?></th>
            			<th class="tabletoplink" nowrap><?=$Lang['medical']['bowel']['tableHeader']['StudentName'] ?></th>
            			<th class="tabletoplink" nowrap><?=$Lang['medical']['bowel']['tableHeader']['StayOrNot'] ?></th>
            			<th class="tabletoplink" nowrap><?=$Lang['medical']['bowel']['tableHeader']['AttendanceStatus'] ?></th>
            			<th class="tabletoplink" nowrap><?=$Lang['medical']['bowel']['tableHeader']['BowelCondition'] ?></th>
            			<th class="tabletoplink" nowrap><?=$Lang['medical']['bowel']['tableHeader']['BowelTime'] ?></th>
            			<th class="tabletoplink" nowrap><?=$Lang['medical']['bowel']['tableHeader']['Remarks'] ?></th>
            			<th class="tabletoplink" nowrap><?=$Lang['medical']['bowel']['tableHeader']['LastPersonConfirmed'] ?></th>
            			<th class="tabletoplink" nowrap><?=$Lang['medical']['bowel']['tableHeader']['LastUpdated'] ?></th>
            			<th class="tabletoplink" nowrap><?=$Lang['medical']['bowel']['tableHeader']['del'] ?></th>
            		</tr>
            	</thead>
    		</table>
    		
		</td>
	</tr>
</table>

</form>
<script>

$('.defaultRemarkSpan > a').live('click',function(){
	$('iframe#lyrShim').hide();
});

$(document).ready(function(){
	$('#barcode').focus();

	$('#btnBarcode').click( function(e){
		e.preventDefault();	
    	$('#barcode').val($('#barcode').val().replace(/^\s+|^　+|\s+$|　+$/gm,''));
		if ($('#barcode').val()) {
        	$.ajax({
        		dataType: "json",
        		type: "POST",
        		url: '?t=management.ajax.ajax&ma=1',
        		data : {
        			'action': 'getStudentBarcode',
        			'barcode': $('#barcode').val()
        		},		  
        		success: showStudentBarcode,
        		error: show_ajax_error
        	});
    	}
	});

	$('#barcode').keypress(function(e) {
		var keyCode = e.keyCode || e.which;
		if (e.keyCode == 13) {
			e.preventDefault();
		}  
	});
	
	$('#barcode').keyup(function(e) {
		var keyCode = e.keyCode || e.which;
		if (e.keyCode == 13) {
			$('#btnBarcode').click();
		}
	});

	$('#btnBowelStatus').click( function(e){
		e.preventDefault();	
    	$('#bowelStatusBarcode').val($('#bowelStatusBarcode').val().replace(/^\s+|^　+|\s+$|　+$/gm,''));
		if ($('#bowelStatusBarcode').val()) {
			if ($('#studentBarcode').val() == '') {
				alert('<?php echo $Lang['medical']['bowel']['warning']['InputStudentBarcode'];?>');
			}		
 			else {	
            	$.ajax({
            		dataType: "json",
            		type: "POST",
            		url: '?t=management.ajax.ajax&ma=1',
            		data : {
            			'action': 'checkAddBowel',
            			'studentBarcode': $('#studentBarcode').val(),
            			'bowelStatusBarcode': $('#bowelStatusBarcode').val()
            		},		  
            		success: addBowelByBarcode,
            		error: show_ajax_error
            	});
 			}
    	}
	});

	$('#bowelStatusBarcode').keypress(function(e) {
		var keyCode = e.keyCode || e.which;
		if (e.keyCode == 13) {
			e.preventDefault();
		}  
	});
	
	$('#bowelStatusBarcode').keyup(function(e) {
		var keyCode = e.keyCode || e.which;
		if (e.keyCode == 13) {
			$('#btnBowelStatus').click();
		}
		else if (e.keyCode == 46 || e.keyCode == 8) {	// Delete / Backspace
			if ($('#bowelStatusBarcode').val() == '') {
    			$('#bowelStatus').val('');
    			$('#divBowelStatus').html('');
			}
		}
	});
	
	$('.deleteBtn').die('click').live('click', function(e){
		e.preventDefault();
		var date = '<?php echo date('Y-m-d');?>';
		var userID = $(this).attr('data-sid');
		var recordID = $(this).attr('data-recordID');
		var deleteRow = $(this).closest('tr');
		
		var isDeleted = confirm('<?php echo $Lang['medical']['report_general']['deleteEvent'] ?>');
		if(!isDeleted){
			return;
		}
		else {
	    	$.ajax({
	    		dataType: "json",
	    		type: "POST",
	    		url: '?t=management.ajax.ajax&ma=1',
	    		data : {
	    			'action': 'deleteBowel',
	    			'recordID': recordID,
	    			'recordDate': date
	    		},		  
	    		success: function(ajaxReturn) {
	    			if (ajaxReturn != null && ajaxReturn.success){
						deleteRow.remove();
	    				updateRowNo();
	    			}
	    		},
	    		error: show_ajax_error
	    	});
		}
	});
	
});

function addBowelByBarcode(ajaxReturn)
{
	if (ajaxReturn != null && ajaxReturn.success){
		if (ajaxReturn.msg == 'studentBarcodeNotFound') {
			alert('<?php echo $Lang['medical']['bowel']['studentBarcodeNotFound'];?>');
		}
		else if (ajaxReturn.msg == 'bowelBarcodeNotFound') {
			alert('<?php echo $Lang['medical']['bowel']['bowelStatusBarcodeNotFound'];?>');
		}		
		else if (ajaxReturn.msg == 'BowelLogExist') {
			alert('<?php echo $Lang['medical']['bowel']['duplicateRecord'];?>');
		}
		else if (ajaxReturn.msg == 'absent') {
			alert('<?php echo $Lang['medical']['bowel']['AttendanceAbsentAlert'];?>');
		}		
		else {
	    	$.ajax({
	    		dataType: "json",
	    		type: "POST",
	    		url: '?t=management.ajax.ajax&ma=1',
	    		data : {
	    			'action': 'addBowelByBarcode',
	    			'studentBarcode': $('#studentBarcode').val(),
	    			'bowelStatus': ajaxReturn.BowelStatusID,
	    			//'bowelStatus': $("input:radio[name='bowelStatus']:checked").val(),
	    			'remarks':$('#remarks').val()
	    		},		  
	    		success: updateBowelList,
	    		error: show_ajax_error
	    	});
		}
	}
}

function updateBowelList(ajaxReturn)
{
	if (ajaxReturn != null && ajaxReturn.success){
		table = $('#bowel_table');
		$row = $(ajaxReturn.html);
		$('#bowel_table tr:first').after($row);
		updateRowNo();
		$('#remarks').val('');
		$('#barcode').val('');
		$('#studentBarcode').val('');
		$('#divStudentBarcode').html('');
		$('#bowelStatus').val('');
		$('#divBowelStatus').html('');
		$('#bowelStatusBarcode').val('');
		$('#barcode').focus();
<?php //if ($defaultBowelColorID):?>		
//		$('#bowelStatus_<?php //echo $defaultBowelColorID;?>').attr('checked',true);
<?php //endif;?>		
	}
	else if (ajaxReturn.msg == 'InvalidBarcode') {
		alert('<?php echo $Lang['medical']['bowel']['InvalidBarCode']; ?>');
	}
}

function updateRowNo() {
	var i = 1;	
	if ($('#bowel_table tr').length) { 
		i = $('#bowel_table tr').length - 1;
	}
	$('#bowel_table tr span.row_id').each(function(){
		$(this).html(i--);
	});
}

function showStudentBarcode(ajaxReturn)
{
	if (ajaxReturn != null && ajaxReturn.success){
		if (ajaxReturn.msg == 'barcodeNotFound') {
			alert('<?php echo $Lang['medical']['bowel']['studentBarcodeNotFound'];?>');
			$('#studentBarcode').val('');
			$('#divStudentBarcode').html('');
			$('#barcode').focus();
		}
		else {
    		$('#studentBarcode').val(ajaxReturn.Barcode);
    		$('#divStudentBarcode').html(ajaxReturn.Barcode);
    		$('#bowelStatusBarcode').focus();
		}
	}
}

function show_ajax_error() 
{
	alert('<?=$Lang['medical']['general']['error']['ajax']?>');
}

</script>
<?php 
$linterface->LAYOUT_STOP();
?>