<?php
// using:
/*
 * 2018-12-19 Cameron
 * - fix: should empty and hide LocationID when change BuildingID (in update_building_level_list_in_event_select)
 * 
 * 2018-04-27 Cameron
 * - apply stripslashes to $eventTypeAry in option list
 *
 * 2018-03-28 Cameron
 * - change button 'Submit' to 'View' in search toobar
 *
 * 2018-03-21 Cameron
 * - add groupID filter
 *
 * 2017-06-12 Cameron
 * - create this file
 */

$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);
$AcademicYearID = Get_Current_Academic_Year_ID();

// ############ start Filters

// Class Filter
$lclass = new libclass();
$classFilter = $lclass->getSelectClass("name='SrhClassName' id='SrhClassName' onChange='changeClass();'", $ClassName, "", $Lang['medical']['event']['AllClass'], $AcademicYearID);

// Group Filter
$groupFilter = $objMedical->getGroupSelection("SrhGroupID", $SrhGroupID, "onChange='changeGroup();'", $Lang['medical']['event']['AllGroup']);

// Event Type Filter
$eventTypeAry = $objMedical->getEventType();
foreach ((array) $eventTypeAry as $_idx => $_eventType) {
    $eventTypeAry[$_idx]['EventTypeName'] = stripslashes($_eventType['EventTypeName']);
    $eventTypeAry[$_idx][1] = $eventTypeAry[$_idx]['EventTypeName']; // 2nd column
}
$eventTypeFilter = getSelectByArray($eventTypeAry, "name='EventType' onChange='getEvents();'", $EventType, 0, 0, $Lang['medical']['event']['AllCategory']);

$buildingAry = $objMedical->getInventoryBuildingArray();
$buildingFilter = getSelectByArray($buildingAry, "Name='LocationBuildingID' ID='LocationBuildingID' onChange='changeBuilding();'", $LocationBuildingID, 0, 0, $Lang['SysMgr']['Location']['All']['Building']);

$levelAry = array();
$levelFilter = getSelectByArray($levelAry, "Name='LocationLevelID' ID='LocationLevelID' onChange='changeLevel();' style='display:none'", $LocationLevelID, 0, 0, $Lang['SysMgr']['Location']['All']['Floor']);

$locationAry = array();
$locationFilter = getSelectByArray($locationAry, "Name='LocationID' ID='LocationID' onChange='getEvents();' style='display:none'", $LocationID, 0, 0, $Lang['SysMgr']['Location']['All']['Room']);

if (! empty($ClassName)) {
    if ($junior_mck) {
        $studentAry = $lclass->getStudentNameListWClassNumberByClassName($ClassName);
    } else {
        $studentAry = $objMedical->getStudentNameListWClassNumberByClassName($ClassName);
    }
} else {
    $studentAry = $objMedical->getAllStudents($AcademicYearID);
}
$studentFilter = getSelectByArray($studentAry, "name='EventStudent' id='EventStudent' onChange='getEvents();'", $EventStudent, 0, 0, $Lang['medical']['event']['AllStudent']);

$teacherAry = $objMedical->getTeacher();
$reporterFilter = getSelectByArray($teacherAry, "name='EventReporter' onChange='getEvents();'", $EventReporter, 0, 0, $Lang['medical']['event']['AllTeacher']);

$affectedPersonAry = array_merge($studentAry, $teacherAry);
$affectedPersonFilter = getSelectByArray($affectedPersonAry, "name='EventAffectedPerson' onChange='getEvents();'", $EventAffectedPerson, 0, 0, $Lang['medical']['event']['AllAffectedPerson']);

$EventDateStart = $EventDateStart ? $EventDateStart : substr(getStartDateOfAcademicYear($AcademicYearID), 0, 10);
$EventDateEnd = $EventDateEnd ? $EventDateEnd : substr(getEndDateOfAcademicYear($AcademicYearID), 0, 10);

// ############ end Filters

?>

<script>

$(document).ready(function(){
	
	$('#EventDate').click(function(){
		if ($(this).is(':checked')){
			$('#spanRequest').css('display','inline');
		}
		else {
			$('#spanRequest').css('display','none');
		}
	});
	
	$('#keyword').keyup(function(event) {				
	  	if (event.which == 13) {			
			getEvents();		
	  	}  			
	});				
	
	$('#keyword').keypress(function(e){			
	    if ( e.which == 13 ) {			
			e.preventDefault();	
		    return false;		
	    }			
	});			

	getEvents();
		
});

function getEvents() {

	$.ajax({
		dataType: "json",
		type: "POST",
		url: '?t=management.ajax.ajax&ma=1&action=getEvents',
		data : $('#form2').serialize(),		  
		success: showEvents,
		error: show_ajax_error
	});
}

function showEvents(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#EventList').html(ajaxReturn.html);
		if (parseInt(ajaxReturn.html2) > 0) {
			$('#SelectBtn').css('display','');
		}
		else {
			$('#SelectBtn').css('display','none');
		}
	}
}	

function changeClass() {
	$.ajax({
		dataType: "json",
		type: "POST",
		url: '?t=management.ajax.ajax&ma=1',
		data : {
			'action': 'getStudentNameByClassForCase',
			'ClassName': $('#SrhClassName').val(),
			'GroupID': $('#SrhGroupID').val()
		},		  
		success: update_student_list_in_event_select,
		error: show_ajax_error
	});
}

function changeGroup()
{
	$.ajax({
		dataType: "json",
		type: "POST",
		url: '?t=management.ajax.ajax&ma=1',
		data : {
			'action': 'getStudentNameByGroupIDForCase',
			'GroupID': $('#SrhGroupID').val(),
			'ClassName': $('#SrhClassName').val()
		},		  
		success: update_student_list_in_event_select,
		error: show_ajax_error
	});
}

function update_student_list_in_event_select(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#EventStudent').replaceWith(ajaxReturn.html);
		getEvents();
	}
}	
	
function changeBuilding() {
	if ($('#LocationBuildingID').val() == '') {
		$('#LocationLevelID').val('');
		$('#LocationID').val('');
		$('#LocationLevelID option:not(:first)').remove();
		$('#LocationID option:not(:first)').remove();
		$('#LocationLevelID').css('display','none');
		$('#LocationID').css('display','none');
	}

	$.ajax({
		dataType: "json",
		type: "POST",
		url: '?t=management.ajax.ajax&ma=1',
		data : {
			'action': 'getBuildingLevel',
			'FirstTitle': 'all',
			'LocationBuildingID': $('#LocationBuildingID').val()
		},		  
		success: update_building_level_list_in_event_select,
		error: show_ajax_error
	});
}

function update_building_level_list_in_event_select(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		if ($('#LocationBuildingID').val() != '') {
			$('#LocationLevelID').replaceWith(ajaxReturn.html);
			$('#LocationID').val('');
			$('#LocationID option:not(:first)').remove();
			$('#LocationID').css('display','none');
		}
		getEvents();
	}
}	

function changeLevel() {
	if ($('#LocationLevelID').val() == '') {
		$('#LocationID').val('');
		$('#LocationID option:not(:first)').remove();
		$('#LocationID').css('display','none');
	}
	
	$.ajax({
		dataType: "json",
		type: "POST",
		url: '?t=management.ajax.ajax&ma=1',
		data : {
			'action': 'getLocation',
			'onChange': 'getEvents()',
			'FirstTitle': 'all',
			'LocationLevelID': $('#LocationLevelID').val()
		},		  
		success: update_location_list_in_event_select,
		error: show_ajax_error
	});
}

function update_location_list_in_event_select(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		if ($('#LocationLevelID').val() != '') {
			$('#LocationID').replaceWith(ajaxReturn.html);
		}
		getEvents();
	}
}	

function show_ajax_error() 
{
	alert('<?=$Lang['medical']['general']['error']['ajax']?>');
}

</script>

<form name="form2" id="form2" method="POST">
	<div class="content_top_tool">

		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="20%"><?=$toolbar ?></td>
				<td width="50%" align="center">&nbsp;</td>
				<td width="30%" style="float: right;">
					<div class="content_top_tool" style="float: right;">
						<?=$htmlAry['searchBox']?>	
						<br style="clear: both" />
					</div>
				</td>
			</tr>
		</table>

	</div>

	<div class="table_board">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="bottom">
					<div class="table_filter">
					<?=$classFilter ?>
					<?=$groupFilter ?>
					<?=$studentFilter?>
					<?=$eventTypeFilter?>
					<?=$buildingFilter?>
					<?=$levelFilter?>
					<?=$locationFilter?>
					<?=$affectedPersonFilter?>
					<?=$reporterFilter?>
				</div> <br style="clear: both" />
					<div>
						<input type="checkbox" name="EventDate" id="EventDate" value="1"
							<?=$EventDate==1 ? "checked" : ""?>><label for="EventDate"><?=$Lang['medical']['filter']['FilterDateRange']?></label>
						<div id="spanRequest" style="position:relative;display:<?=$EventDate==1 ? "inline" : "none"?>">
							<label for="EventDateStart"><?=$i_From?></label> : <?=$linterface->GET_DATE_PICKER("EventDateStart",$EventDateStart)?>
					 <?=getTimeSel('TimeStart', ($TimeStartHour?$TimeStartHour:0), ($TimeStartMin?$TimeStartMin:0), "", $minuteStep=1)?>
					 <label for="EventDateEnd"><?=$i_To ?></label> <?=$linterface->GET_DATE_PICKER("EventDateEnd",$EventDateEnd)?>
					 <?=getTimeSel('TimeEnd', ($TimeEndHour?$TimeEndHour:23), ($TimeEndMin?$TimeEndMin:59), "", $minuteStep=1)?>
					 <?=$linterface->GET_ACTION_BTN($Lang['Btn']['View'], 'button',$ParOnClick="getEvents();", $ParName="submit1")?>
						</div>
					</div>

				</td>
				<td valign="bottom">
					<div class="common_table_tool" id="SelectBtn"
						style="width: 50px; display: none">
						<a href="javascript:selectEvents()" class="tool_other"><?=$Lang['Btn']['Select'] ?></a>
					</div>
				</td>
			</tr>
		</table>
	</div>

	<div class="table_board">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="bottom" id="EventList"></td>
			</tr>
		</table>
	</div>

</form>
