<?php
// using:
/*
 * 	2017-06-23 Cameron
 * 		- create this file
 */

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='EVENT_MANAGEMENT') || !$plugin['medical_module']['discipline']){
	header("Location: /");
	exit();
}

if (is_array($FollowupID)) {
	$followupID = (count($FollowupID)==1) ? $FollowupID[0] : '';
}
else {
	$followupID = $FollowupID;
}

if (!$followupID) {	
	header("Location: ?t=management.event_list");
}

$rs_followup = $objMedical->getEventFollowup('',$followupID);
if (count($rs_followup) == 1) {	
	$rs_followup = current($rs_followup);
}


## handle attachment(s)
$ret = $objMedical->handleTempUploadFolder();
$tempFolderPath = $ret['TempFolderPath'];
$use_plupload = $objMedical->is_use_plupload();

$plupload = array();
$plupload['use_plupload'] = $use_plupload;
if($use_plupload) {
	$plupload['pluploadButtonId'] = 'UploadButton2';
	$plupload['pluploadDropTargetId'] = 'DropFileArea2';
	$plupload['pluploadFileListDivId'] = 'FileListDiv2';
	$plupload['pluploadContainerId'] = 'pluploadDiv2';
}

$action = 'editFollowupSave';
$eventID = $EventID;

include_once('templates/followup.tmpl.php');

if($use_plupload) {
	include_once('plupload_script.php');
}

?>