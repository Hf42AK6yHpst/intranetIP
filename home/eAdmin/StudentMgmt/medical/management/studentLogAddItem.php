<?php
// using: Adam
//http://192.168.0.146:31002/home/eAdmin/StudentMgmt/medical/?t=management.studentLogAddItem&action=new&sid=3070&date=2013-12-23

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='STUDENTLOG_MANAGEMENT') || !$plugin['medical_module']['studentLog']){
	header("Location: /");
	exit();
}
$CurrentPage = "ManagementStudentLog";
$TAGS_OBJ[] = array($Lang['medical']['menu']['studentLog'], "", 0);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();		
//$MODULE_OBJ['title'] = $Lang['medical']['menu']['meal'];

$objFCM_UI = new form_class_manage_ui;
$menuOption = array();

$sid = trim($sid); //Student UserID for particular student 
$date = trim($date);

////////////////////////// Object Data ////////////////////////////////////////////////

# Student Info
$studentInfo = $objMedical->getForStudentLog($userID= $sid);

# Object Data
$objLogMapper = new StudentLogMapper();
$studentLogObjAry = $objLogMapper->findByStudentId($sid,$date,$parEndDate = null,$orderBy = 'recordtime Desc');


// An already existed eventID List in hidden form
// This function needs to wait for the db
$eventIDArray = array();
//$eventIDArray = getEventIDListByUserIDAndDate($userID, $date);

$linterface->LAYOUT_START($Msg);
echo '<script language="JavaScript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.8.3.min.js"></script>';
//debug_r("<----".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n");
include_once('templates/studentLogAddItem.tmpl.php');
$linterface->LAYOUT_STOP();
?>