<?php
//using: Adam
global $medical_cfg;
$recordID = $_GET['RecordID'];
$uploadFile_root = $medical_cfg['uploadFile_root'];

$fileDetail = $objMedical->getFileDetailInfo($recordID);
if(empty($fileDetail)){
	echo "Incorrect ID";	
	return;
	
}

switch($_SESSION['UserType']){
	case USERTYPE_PARENT:
		$sid = $objMedical->getFileDetailUserID($recordID);
		$validPermission = $objMedical->isChildOfTheParent($_SESSION['UserID'],$sid);
		break;
	case USERTYPE_STUDENT:
		$sid = $objMedical->getFileDetailUserID($recordID);
		if($sid == $_SESSION['UserID']){
			$validPermission = true;
		}
		break;
	case USERTYPE_STAFF:
		$validPermission = true;
		break;
	default:
		$validPermission = false;
		break;
}
if(!$validPermission){
	echo "Permission denied";	
	exit();
}


$fileName = $fileDetail['OrgFileName'];
$fileType = $fileDetail['FileType'];
$folderPath = $fileDetail['FolderPath'];
$filePath = $uploadFile_root.'/'.$folderPath.'/'.$fileDetail['RenameFile'];

if( empty($filePath) || empty($fileName) ){
	echo "Empty File Path/ Name";	
	return;
}


if(!file_exists($filePath)){
	echo "File does not exist";	
	return;	
}
$fs = new libfilesystem();
$FileContent = $fs->file_read($filePath);
$FileType = $fs->getMIMEType($FileContent);
Output_To_Browser($FileContent, $fileName, $FileType);

# Dislay generated content to html, such as attachment
function Output_To_Browser($content, $filename,$content_type="")
{
	$ua = $_SERVER["HTTP_USER_AGENT"];
	
	if($content_type=="" || $content_type=="application/zip") {	# to handle IE download corrupted zip file problem
		$content_type = "application/octet-stream";
	}
	
	while (@ob_end_clean());
	header("Pragma: public");
	header("Expires: 0"); # set expiration time
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-type: $content_type");
	header("Content-Length: ".strlen($content));
	
	# Handle UTF-8 FileName with IE And FF
	$encoded_filename = urlencode($filename);
	$encoded_filename = str_replace("+", "%20", $encoded_filename);
	if (preg_match("/MSIE/", $ua)) {
		header('Content-Disposition: attachment; filename="' . $encoded_filename . '"');
	} else  {
		header('Content-Disposition: attachment; filename="' . $filename . '"');
	} 
	
	echo $content;
}
?>