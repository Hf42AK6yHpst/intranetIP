<?php
/*
 * 	Modified: cameron
 * 	Log
 * 	Date:
 * #######################
 * 	!!!!! This file not used now, functions have been moved to libMedical and libStudentLogImport 	
 * 
 */
 
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimport.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");

//$TargetFilePath = $_POST["TargetFilePath"];
function getClassNameClassNumber(){ // to libmedical.php
//function _getClassNameClassNumber(){ 
//function _getClassNameClassNumber(){
	$objDB = new libdb();
	$sql = 'SELECT ClassName, ClassNumber FROM INTRANET_USER where RecordType = 2 And RecordStatus = 1';
	$rs = $objDB->returnResultSet($sql);
	return $rs;
}

function checkUserExistInSystem($dbResult,$classname, $classnum) // to libmedical.php
//function _checkUserExistInSystem($dbResult,$classname, $classnum) 
{
	for($i = 0, $iMax = count($dbResult);$i < $iMax;$i++)
	{
		if($dbResult[i]["ClassName"] == $classname && $dbResult[i]["ClassNumber"] == $classnum)
		{
			return true;
		}
	}
	return false;
}

function getUserIDByClassNameAndNumber($className,$classNumber) // to libmedical.php
{
	$objDB = new libdb();

	if($className == '' || $classNumber == ''){
		return false;
	}

	$sql = "SELECT UserID FROM INTRANET_USER where RecordType = 2 And RecordStatus = 1 AND ClassName='$className' AND ClassNumber='$classNumber'";
	$rs = $objDB->returnResultSet($sql);
	if (count($rs) == 1)
	{
		return $rs[0]["UserID"];
	}
	else
	{
		return false;
	}
}

function getLev1IDByCode($code) // to libmedical.php
{
	$objDB = new libdb();
	$sql = "SELECT Lev1ID FROM MEDICAL_STUDENT_LOG_LEV1 WHERE Lev1Code='$code'";
	$rs = $objDB->returnResultSet($sql);
	if (count($rs) == 1)
	{
		return $rs[0]["Lev1ID"];
	}
	else
	{
		return false;
	}	
}

function getLev2IDByCode($code) // to libmedical.php
{
	$objDB = new libdb();
	$sql = "SELECT Lev2ID FROM MEDICAL_STUDENT_LOG_LEV2 WHERE Lev2Code='$code'";
	$rs = $objDB->returnResultSet($sql);
	if (count($rs) == 1)
	{
		return $rs[0]["Lev2ID"];
	}
	else
	{
		return false;
	}	
}

function getLev3IDByCode($code) // to libmedical.php
{
	$objDB = new libdb();
	$sql = "SELECT Lev3ID FROM MEDICAL_STUDENT_LOG_LEV3 WHERE Lev3Code='$code'";
	$rs = $objDB->returnResultSet($sql);
	if (count($rs) == 1)
	{
		return $rs[0]["Lev3ID"];
	}
	else
	{
		return false;
	}	
}

function getLev4IDByCode($code)  // to libmedical.php
{
	$objDB = new libdb();
	$sql = "SELECT Lev4ID FROM MEDICAL_STUDENT_LOG_LEV4 WHERE Lev4Code='$code'";
	$rs = $objDB->returnResultSet($sql);
	if (count($rs) == 1)
	{
		return $rs[0]["Lev4ID"];
	}
	else
	{
		return false;
	}	
}


///////////////////////////////
//////////////new file here
///////////////////////////////

if (!defined("LIBSTUDENTLOGIMPORT_DEFINED"))                     // Preprocessor directive
{
	define("LIBSTUDENTLOGIMPORT_DEFINED", true);
	class libStudentLogImport
	{

		function ReadStudentLogImportData($TargetFilePath)
		{
			if (file_exists($TargetFilePath))
			{
		//		$libfs = new libfilesystem();
				$libimport = new libimporttext();
				
				$DefaultCsvHeaderArr= array('ClassName','ClassNumber','Date','Time','Behaviour1','Behaviour2','Parts1','Parts2','Duration','Remarks','PIC');
				$ColumnPropertyArr = array(1,1,0,1,1,1,1,1,1,1,1,1);	// 0 - Ref Field, not for actual import
				
				$CsvData = $libimport->GET_IMPORT_TXT_WITH_REFERENCE($TargetFilePath, "", "", $DefaultCsvHeaderArr, $ColumnPropertyArr);

//print "CsvData:<br>";
//debug_r($CsvData);
				$CsvHeaderArr = array_shift($CsvData);
//debug_r($CsvHeaderArr);
				$numOfCsvData = count($CsvData);
				$CsvHeaderWrong = false;
				for($i=0; $i<count($DefaultCsvHeaderArr); $i++) {
					if ($CsvHeaderArr[$i] != $DefaultCsvHeaderArr[$i]) {
						$CsvHeaderWrong = true;
						break;
					}
				}

				$ret = array();		
				if ($CsvHeaderWrong)
					$ret["HeaderError"] = $CsvHeaderWrong;
				$ret["NumOfData"] 	= $numOfCsvData;
		//		if($CsvHeaderWrong || $numOfCsvData==0)
		//		{
		//			$ReturnMsgKey = ($CsvHeaderWrong)? 'WrongCSVHeader' : 'CSVFileNoData';
		//			intranet_closedb();
		//			header('location: '.$ReturnPath.'&ReturnMsgKey='.$ReturnMsgKey);
		//			exit();
		//		}
						
				$ret["Data"] = $this->ValidateStudentLogImportData($CsvData);
				return $ret;		
			}
			else
			{
				return false;
			}
		}

		function ValidateStudentLogImportData($CsvData)
		{
			$objMedical = new libMedical();		
			$ret = array();
			for($i = 0,$iMax = count($CsvData);$i < $iMax; $i++)
			{
				$classname 		= $CsvData[$i][0]; // column 0=> ClassName
				$classnum 		= $CsvData[$i][1]; // column 1=> ClassNumber
				$date 			= $CsvData[$i][2]; // column 2=> Date
				$time 			= $CsvData[$i][3]; // column 3=> Time
				$behaviour1 	= $CsvData[$i][4]; // column 4=> Behaviour1
				$behaviour2		= $CsvData[$i][5]; // column 5=> Behaviour2
				$parts1 		= $CsvData[$i][6]; // column 6=> Parts1
				$parts2 		= $CsvData[$i][7]; // column 7=> Parts2
				$duration 		= $CsvData[$i][8]; // column 8=> Duration
				$remarks 		= $CsvData[$i][9]; // column 9=> Remarks
				$pic 			= $CsvData[$i][10]; // column 10=> PIC
				
				// Validate process here
				//$pass = ValidateProcess();
				$classRs = $objMedical->getClassNameClassNumber();
				
				$error = array();
				
				//////////////////
				//check name exist
				//////////////////
				$nameExist = false;
				for($j = 0, $jMax = count($classRs); $j < $jMax; $j++)
				{
					if($classRs[$j]['ClassName'] == $classname){
						$nameExist = true;
						break;
					}
				}
				if(!$nameExist)
				{
					array_unshift($error, 'Invalid Classname');
				}
			
				////////////////
				//check classnum 
				////////////////
				if($classnum == '' || !$objMedical->getUserIDByClassNameAndNumber($classname, $classnum))
				{
					array_unshift($error, 'Invalid Classnum');
				}
			
				////////////////
				//check date 
				////////////////
				$date = str_replace('-', '/', $date);
				sscanf($date, '%d/%d/%d', $date_y, $date_m, $date_d);
				if(checkdate($date_m, $date_d, $date_y))
				{
					array_unshift($error, 'Invalid Date');
				}
				
				////////////////
				//check time 
				////////////////
				if($time == '')
				{
					array_unshift($error, 'Invalid Time');
				}
				
				////////////////
				//check Behaviour1 
				////////////////
				if($behaviour1 == '')
				{
					array_unshift($error, 'Empty Behaviour1');
				}
				
				////////////////
				//check Behaviour2 
				////////////////
				if($behaviour2 == '')
				{
					array_unshift($error, 'Empty Behaviour2');
				}
				
				////////////////
				//check Parts1 
				////////////////
				if($parts1 == '')
				{
					array_unshift($error, 'Empty Parts1');
				}
				
				////////////////
				//check Parts2
				////////////////
				/*if($parts2 == '')
				{
					array_unshift($error, 'Invalid Parts2');
				}*/
				
				////////////////
				//check Duration
				////////////////
				if($duration == '')
				{
					array_unshift($error, 'Invalid Duration');
				}
				
				////////////////
				//check Remarks
				////////////////
				if($remarks == '')
				{
					array_unshift($error, 'Empty Remarks');
				}
				
				////////////////
				//check PIC
				////////////////
				if($pic == '')
				{
					array_unshift($error, 'Empty PIC');
				}
				
				$pass = true;
				$ret[$i]["pass"] 					= $pass;
				$ret[$i]["reason"] 					= $error;
				$ret[$i]["rawData"]["ClassName"] 	= $classname;
				$ret[$i]["rawData"]["ClassNum"]		= $classnum;
				$ret[$i]["rawData"]["Date"] 		= $date;
				$ret[$i]["rawData"]["Time"] 		= $time;
				$ret[$i]["rawData"]["Behaviour1"] 	= $behaviour1;
				$ret[$i]["rawData"]["Behaviour2"] 	= $behaviour2;
				$ret[$i]["rawData"]["Parts1"] 		= $parts1;
				$ret[$i]["rawData"]["Parts2"] 		= $parts2;
				$ret[$i]["rawData"]["Duration"] 	= $duration;
				$ret[$i]["rawData"]["Remarks"] 		= $remarks;
				$ret[$i]["rawData"]["Pic"] 			= $pic;
			}
//print "ret:<br>";
//debug_r($ret);
			return $ret;	
		}

//		function ValidateStudentLogImportData($CsvData)
//		{
//			$ret = array();
//			for($i = 0,$iMax = count($CsvData);$i < $iMax; $i++)
//			{
//				$_classname 	= $CsvData[$i][0]; // column 0=> ClassName
//				$_classnum 		= $CsvData[$i][1]; // column 1=> ClassNumber
//				$_date 			= $CsvData[$i][2]; // column 2=> Date
//				$_time 			= $CsvData[$i][3]; // column 3=> Time
//				$_behaviour1 	= $CsvData[$i][4]; // column 4=> Behaviour1
//				$_behaviour2	= $CsvData[$i][5]; // column 5=> Behaviour2
//				$_parts1 		= $CsvData[$i][6]; // column 6=> Parts1
//				$_parts2 		= $CsvData[$i][7]; // column 7=> Parts2
//				$_duration 		= $CsvData[$i][8]; // column 8=> Duration
//				$_remarks 		= $CsvData[$i][9]; // column 9=> Remarks
//				$_pic 			= $CsvData[$i][10]; // column 10=> PIC
//				
//				// Validate process here
//				//$pass = ValidateProcess();
//				$classRs = _getClassNameClassNumber();
//		echo 'ClassNameNum:';
//		debug_r($classRs);
//				
//				$error = array();
//				
//				//////////////////
//				//check name exist
//				//////////////////
//				if($_classname == '')
//				{
//					array_unshift($error, 'Invalid Classname');
//				}
//				
//			
//				////////////////
//				//check classnum 
//				////////////////
//				if($_classnum == '' || _checkUserExistInSystem($classRs, $_classname, $_classnum))
//				{
//					array_unshift($error, 'Invalid Classnum');
//				}
//				
//				////////////////
//				//check time 
//				////////////////
//				if($_time == '')
//				{
//					array_unshift($error, 'Invalid Time');
//				}
//				
//				////////////////
//				//check Behaviour1 
//				////////////////
//				if($_behaviour1 == '')
//				{
//					array_unshift($error, 'Empty Behaviour1');
//				}
//				
//				////////////////
//				//check Behaviour2 
//				////////////////
//				if($_behaviour2 == '')
//				{
//					array_unshift($error, 'Empty Behaviour2');
//				}
//				
//				////////////////
//				//check Parts1 
//				////////////////
//				if($_parts1 == '')
//				{
//					array_unshift($error, 'Empty Parts1');
//				}
//				
//				////////////////
//				//check Parts2
//				////////////////
//				/*if($_parts2 == '')
//				{
//					array_unshift($error, 'Invalid Parts2');
//				}*/
//				
//				////////////////
//				//check Duration
//				////////////////
//				if($_duration == '')
//				{
//					array_unshift($error, 'Invalid Duration');
//				}
//				
//				////////////////
//				//check Remarks
//				////////////////
//				if($_remarks == '')
//				{
//					array_unshift($error, 'Empty Remarks');
//				}
//				
//				////////////////
//				//check PIC
//				////////////////
//				if($_pic == '')
//				{
//					array_unshift($error, 'Empty PIC');
//				}
//				
//				$pass = true;
//				$ret[$i]["pass"] 					= $pass;
//				$ret[$i]["reason"] 					= $error;
//				$ret[$i]["rawData"]["ClassName"] 	= $_classname;
//				$ret[$i]["rawData"]["ClassNum"]		= $_classnum;
//				$ret[$i]["rawData"]["Date"] 		= $_date;
//				$ret[$i]["rawData"]["Time"] 		= $_time;
//				$ret[$i]["rawData"]["Behaviour1"] 	= $_behaviour1;
//				$ret[$i]["rawData"]["Behaviour2"] 	= $_behaviour2;
//				$ret[$i]["rawData"]["Parts1"] 		= $_parts1;
//				$ret[$i]["rawData"]["Parts2"] 		= $_parts2;
//				$ret[$i]["rawData"]["Duration"] 	= $_duration;
//				$ret[$i]["rawData"]["Remarks"] 		= $_remarks;
//				$ret[$i]["rawData"]["Pic"] 			= $_pic;
//			}
//			return $ret;	
//		}
	}//close clase
}
?>