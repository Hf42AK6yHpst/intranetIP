<?php
// using:
/*
 * 2019-01-04 Cameron
 * - set $relevantStaffEvent
 * 
 * 2018-04-27 Cameron
 * - apply stripslashes to EventTypeName
 *
 * 2018-03-29 Cameron
 * - call getEventStudentLogList() to show relevant student log list
 *
 * 2018-03-23 Cameron
 * - call getEventCaseList() to show relevant case list
 *
 * 2017-06-22 Cameron
 * - create this file
 */
if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'EVENT_MANAGEMENT') || ! $plugin['medical_module']['discipline']) {
    header("location: /");
    exit();
}

if (is_array($EventID)) {
    $eventID = (count($EventID) == 1) ? $EventID[0] : '';
} else {
    $eventID = $EventID;
}

if (! $eventID) {
    header("Location: ?t=management.event_list");
}

$rs_event = $objMedical->getEvents($eventID);
if (count($rs_event) == 1) {
    $rs_event = current($rs_event);
}

$CurrentPage = "ManagementEvent";
$CurrentPageName = $Lang['medical']['menu']['event'];
$TAGS_OBJ[] = array(
    $Lang['medical']['menu']['event'],
    "",
    0
);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();

$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$PAGE_NAVIGATION[] = array(
    $Lang['medical']['menu']['event'],
    "?t=management.event_list"
);
$PAGE_NAVIGATION[] = array(
    $Lang['Btn']['Edit'],
    ""
);

$eventTypeAry = $objMedical->getEventType();
if (count($eventTypeAry)) {
    foreach ((array) $eventTypeAry as $_idx => $_eventType) {
        $eventTypeAry[$_idx]['EventTypeName'] = stripslashes($_eventType['EventTypeName']);
        $eventTypeAry[$_idx][1] = $eventTypeAry[$_idx]['EventTypeName']; // 2nd column
    }
}
$eventTypeSelection = getSelectByArray($eventTypeAry, 'Name="EventTypeID" ID="EventTypeID"', $rs_event['EventTypeID']);

$buildingAry = $objMedical->getInventoryBuildingArray();
$buildingSelection = getSelectByArray($buildingAry, 'Name="LocationBuildingID" ID="LocationBuildingID"', $rs_event['LocationBuildingID']);

if ($rs_event['LocationBuildingID']) {
    $levelAry = $objMedical->getInventoryLevelArray($rs_event['LocationBuildingID']);
    $levelSelection = getSelectByArray($levelAry, "Name='LocationLevelID' ID='LocationLevelID' onChange='changeLevel()'", $rs_event['LocationLevelID']);
}

if ($rs_event['LocationLevelID']) {
    $locationAry = $objMedical->getInventoryLocationArray($rs_event['LocationLevelID']);
    $locationSelection = getSelectByArray($locationAry, "Name='LocationID' ID='LocationID'", $rs_event['LocationID']);
}

$rs_event['Student'] = $objMedical->getEventStudent($eventID);
$rs_event['AffectedPerson'] = $objMedical->getEventAffectedPerson($eventID);
$rs_event['Attachment'] = $objMedical->getEventAttachment($eventID);

// $refCase = $objMedical->getCaseRef4Event($rs_event['CaseID'],$viewMode='',$returnID=1);
$relevantCases = $objMedical->getEventCaseList($eventID);
$relevantStudentLogs = $objMedical->getEventStudentLogList($eventID);

$staffEventViewRight = $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'STAFFEVENT_VIEW');
if ($staffEventViewRight) {
    $relevantStaffEvent = $objMedical->getStaffEventListOfStudentEvent($eventID);
}
else {
    $relevantStaffEvent = '';
}

$isCase = $relevantCases == '' ? false : true;

$followup_list = $objMedical->getEventFollowupList($eventID);

// # handle attachment(s)
$ret = $objMedical->handleTempUploadFolder();
$tempFolderPath = $ret['TempFolderPath'];
$use_plupload = $objMedical->is_use_plupload();

$linterface->LAYOUT_START($returnMsg);

$plupload = array();
$plupload['use_plupload'] = $use_plupload;
if ($use_plupload) {
    $plupload['pluploadButtonId'] = 'UploadButton';
    $plupload['pluploadDropTargetId'] = 'DropFileArea';
    $plupload['pluploadFileListDivId'] = 'FileListDiv';
    $plupload['pluploadContainerId'] = 'pluploadDiv';
}
$form_action = '?t=management.edit_event_save';

include_once ('templates/event.tmpl.php');

if ($use_plupload) {
    include_once ('plupload_script.php');
}

$linterface->LAYOUT_STOP();
?>