<?php
// using:
/*
 * 2018-03-23 Cameron
 * - remove IsCase and CaseID in MEDICAL_EVENT table
 *
 * 2017-07-03 Cameron
 * - create this file
 */
if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'CASE_MANAGEMENT') || ! $plugin['medical_module']['discipline']) {
    header("Location: /");
    exit();
}

$ldb = new libdb();
$result = array();

$dataAry['StudentID'] = $StudentID;
$dataAry['StartDate'] = $StartDate;
$dataAry['EndDate'] = $EndDate;
$dataAry['Summary'] = standardizeFormPostValue($Summary);
$dataAry['Detail'] = standardizeFormPostValue($Detail);
$dataAry['IsClosed'] = $IsClosed;
$dataAry['ClosedRemark'] = standardizeFormPostValue($ClosedRemark);
$dataAry['LastModifiedBy'] = $_SESSION['UserID'];
$sql = $objMedical->UPDATE2TABLE('MEDICAL_CASE', $dataAry, array(
    'CaseID' => $CaseID
), false);

$ldb->Start_Trans();
// # step 1: update to case
$result[] = $ldb->db_db_query($sql);

// # step 2: delete case PIC that's not in updated list
if ($TeacherID) {
    $sql = "DELETE FROM MEDICAL_CASE_PIC WHERE CaseID='" . $CaseID . "' AND TeacherID NOT IN ('" . implode("','", (array) $TeacherID) . "')";
    $result[] = $ldb->db_db_query($sql);
}

// # step 3: get present case PIC list after delete in step 2
$present_case_PIC = $objMedical->getCasePIC($CaseID);
$present_case_PIC = Get_Array_By_Key($present_case_PIC, 'UserID');

foreach ((array) $TeacherID as $id) {
    if (! in_array($id, (array) $present_case_PIC)) { // add case PIC
        unset($dataAry);
        $dataAry['CaseID'] = $CaseID;
        $dataAry['TeacherID'] = $id;
        $sql = $objMedical->INSERT2TABLE('MEDICAL_CASE_PIC', $dataAry, array(), false, true, false); // no DateModified field, insert ignore
        $result[] = $ldb->db_db_query($sql);
    }
}
unset($present_case_PIC);

// # step 4: add case event
$current_event = $objMedical->getCaseEvent($CaseID);
$current_event = Get_Array_By_Key($current_event, 'EventID');

foreach ((array) $SelEventID as $id) {
    unset($dataAry);
    $dataAry['CaseID'] = $CaseID;
    $dataAry['EventID'] = $id;
    if (! in_array($id, (array) $current_event)) {
        $sql = $objMedical->INSERT2TABLE('MEDICAL_CASE_EVENT', $dataAry, array(), false, false, false); // no DateModified field
        $result[] = $ldb->db_db_query($sql);
        
        // // update CaseID in event
        // $sql = "UPDATE MEDICAL_EVENT SET IsCase=1, CaseID='".$CaseID."' WHERE EventID='".$id."'";
        // $result[] = $ldb->db_db_query($sql);
    }
}

if (! in_array(false, $result)) {
    $ldb->Commit_Trans();
    $returnMsgKey = 'UpdateSuccess';
} else {
    $ldb->RollBack_Trans();
    $returnMsgKey = 'UpdateUnsuccess';
}

header("location: ?t=management.case_list&returnMsgKey=" . $returnMsgKey);

?>