<?php
// using:

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='SLEEP_MANAGEMENT') || !$plugin['medical_module']['sleep']){
	header("Location: /");
	exit();
}
$CurrentPage = "ManagementStudentSleep";
$TAGS_OBJ[] = array($Lang['medical']['menu']['studentSleep'], "", 0);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();		
//$MODULE_OBJ['title'] = $Lang['medical']['menu']['meal'];

if($_POST['action']=='deleteRecord'){
//	$result = $objMedical->deleteSleepRecords($_POST['recordID']);
	if($result){
		$Msg =$Lang['General']['ReturnMessage']['DeleteSuccess'];
	}
	else{
		$Msg =$Lang['General']['ReturnMessage']['DeleteUnsuccess'];
	}
}


$objFCM_UI = new form_class_manage_ui;
$menuOption = array();
getSearchMenu($menuOption, $linterface, $objFCM_UI);

function getSearchMenu(&$menuOption, $linterface, $objFCM_UI){
	global $Lang,$objMedical;
	
//	$menuOptionCount = count($Lang['medical']['meal']['searchMenu']);
	$i=0;
	
	# Handle those cases with fixed option
	foreach($Lang['medical']['sleep']['searchMenu'] as $key=>$menuItem){
		$menuOption[$i]['Name'] = $menuItem;
		
		if(is_array($Lang['medical']['sleep']['search'][$key.'Option'])){
			$itemHTML ='';
			$itemHTML .="<select name='{$key}' id='{$key}'>";		
	
			$j=1; //set key value
			foreach( $Lang['medical']['sleep']['search'][$key.'Option'] as $itemName){
				$selected = '';
				if( $j == $_POST[$key]){
					$selected = 'selected';
				}
				$itemHTML .= "<option value='{$j}' {$selected}>{$itemName}</option>";
				++$j;
			}
			$itemHTML .='</select>';
			
			$menuOption[$i]['Detail'] = $itemHTML;
				
		}
		++$i;
	}
	
	# For Date Picker
	$menuOption[0]['Detail'] = $linterface->GET_DATE_PICKER($Name="date",$DefaultValue="",$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeNextDateDisplay()",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum");
	
	# For Class List
	$menuOption[1]['Detail'] = 	$objFCM_UI->Get_Class_Selection($AcademicYearID = Get_Current_Academic_Year_ID(), $YearID='', $ID_Name='classID', $SelectedYearClassID='', $Onchange='', $noFirst=0, $isMultiple=0, $isAll=1, $TeachingOnly=0);
	
	# For Group List
	$menuOption[2]['Detail'] = $objMedical->getGroupSelection($name='groupID');
}


$linterface->LAYOUT_START($Msg);
//debug_r("<----".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n");
include_once('templates/sleep.tmpl.php');
$linterface->LAYOUT_STOP();
?>