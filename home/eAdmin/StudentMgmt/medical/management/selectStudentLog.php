<?php
// using:
/*
 * 2018-03-28 Cameron
 * - create this file
 */
if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'STUDENTLOG_MANAGEMENT') || ! $plugin['medical_module']['studentLog']) {
    header("Location: /");
    exit();
}

$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);
$currentAcademicYearID = Get_Current_Academic_Year_ID();

// ############ start Filters

if ($_GET['FromEvent']) {
    $studentLogClassName = urldecode($_GET['ClassName']);
    $studentIDAry = explode(',',$_GET['StudentID']);
    if(count($studentIDAry) > 0 && $studentIDAry[0] > 0) {
        $studentLogStudentID = $studentIDAry[0];        // use first to get result
    }
}
else {
    $studentLogClassName = intranet_htmlspecialchars($_POST['studentLogClassName']);
    $studentLogStudentID = $_POST['studentLogStudentID'];
}

$studentLogGroupID = $_POST['studentLogGroupID'];
$studentLogLev1Type = $_POST['studentLogLev1Type'];
$studentLogLev2Item = $_POST['studentLogLev2Item'];
$studentLogDate = $_POST['studentLogDate'];
$studentLogDateStart = $_POST['studentLogDateStart'];
$studentLogDateEnd = $_POST['studentLogDateEnd'];

// Class Filter
$lclass = new libclass();
$classFilter = $lclass->getSelectClass("name='studentLogClassName' id='studentLogClassName'", $studentLogClassName, "", $Lang['medical']['event']['AllClass'], $currentAcademicYearID);

// Group Filter
$groupFilter = $objMedical->getGroupSelection("studentLogGroupID", $studentLogGroupID, "", $Lang['medical']['event']['AllGroup']);

if (! empty($studentLogClassName) && ! empty($studentLogGroupID)) {
    $includeUserIdAry = $objMedical->getMedicalGroupStudent(array(
        $studentLogGroupID
    ));
    $includeUserIdAry = BuildMultiKeyAssoc($includeUserIdAry, 'UserID', array(
        'UserID'
    ), $SingleValue = 1);
    if ($junior_mck) {
        $studentAry = $lclass->getStudentNameListWClassNumberByClassName($studentLogClassName, $recordstatus = '', $includeUserIdAry);
    } else {
        $studentAry = $objMedical->getStudentNameListWClassNumberByClassName($studentLogClassName, $includeUserIdAry);
    }
} else if (! empty($studentLogClassName) && empty($studentLogGroupID)) {
    if ($junior_mck) {
        $studentAry = $lclass->getStudentNameListWClassNumberByClassName($studentLogClassName);
    } else {
        $studentAry = $objMedical->getStudentNameListWClassNumberByClassName($studentLogClassName);
    }
} else if (empty($studentLogClassName) && ! empty($studentLogGroupID)) {
    $studentAry = $objMedical->getStudentNameListWClassNumberByGroupID($studentLogGroupID);
} else {
    $studentAry = $objMedical->getAllStudents($currentAcademicYearID);
}

$studentFilter = getSelectByArray($studentAry, "name='studentLogStudentID' id='studentLogStudentID'", $studentLogStudentID, 0, 0, $Lang['medical']['event']['AllStudent']);

$lev1List = $objMedical->getStudentLogAllActiveLev1();
$lev1Ary = array();
foreach ((array) $lev1List as $_lev1) {
    $lev1Ary[$_lev1['Lev1ID']] = $_lev1['Lev1Name'];
}
$lev1Filter = getSelectByAssoArray($lev1Ary, 'name="studentLogLev1Type" id="studentLogLev1Type"', $studentLogLev1Type); // student log behavior level 1 type

$lev2List = $objMedical->getStudentLogAllActiveLev2();
$lev2Ary = array();
foreach ((array) $lev2List as $_lev2) {
    if ($_lev2['Lev1ID'] == $studentLogLev1Type) {
        $lev2Ary[$_lev2['Lev2ID']] = $_lev2['Lev2Name'];
    }
}
if (empty($lev2Ary)) {
//    $lev2Filter = '<select name="studentLogLev2Item" id="studentLogLev2Item"></select>';
    $lev2Filter = '';
} else {
    $lev2Filter = getSelectByAssoArray($lev2Ary, 'name="studentLogLev2Item" id="studentLogLev2Item"', $studentLogLev2Item);
}

$studentLogDateStart = $studentLogDateStart ? $studentLogDateStart : substr(getStartDateOfAcademicYear($currentAcademicYearID), 0, 10);
$studentLogDateEnd = $studentLogDateEnd ? $studentLogDateEnd : substr(getEndDateOfAcademicYear($currentAcademicYearID), 0, 10);

// ############ end Filters

echo '<script language="JavaScript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.8.3.min.js"></script>';

?>

<script>

$(document).ready(function(){
	$('#studentLogDate').click(function(){
		if ($(this).is(':checked')){
			$('#spanRequest').css('display','inline');
		}
		else {
			$('#spanRequest').css('display','none');
		}
	});
	
	$('#keyword').keyup(function(event) {				
	  	if (event.which == 13) {			
			getStudentLog();		
	  	}  			
	});				
	
	$('#keyword').keypress(function(e){			
	    if ( e.which == 13 ) {			
			e.preventDefault();	
		    return false;		
	    }			
	});			

	$('#studentLogClassName').change(function(){
		changeStudentLogClass();
	});

	$('#studentLogGroupID').change(function(){
		changeStudentLogGroup();
	});

	$('#studentLogStudentID').change(function(){
		getStudentLog();
	});
	
	$('#studentLogLev1Type').change(function(){
		if($(this).val() == ''){
			$('#studentLogLev2Span').html('');
			getStudentLog();
		}else{
			$.post('/home/eAdmin/StudentMgmt/medical/?t=management.ajax.getStudentLogLevelList',{
				'action': '2',
				'data': $(this).val()
			},function(res){
				$('#studentLogLev2Span').html('<select name="studentLogLev2Item" id="studentLogLev2Item"><option value="" selected=""> -- <?=$button_select?> -- </option>'+res+'</select>');
				$("#studentLogLev2Item").val($("#studentLogLev2Item option:first").val());
				$("#studentLogLev2Item").width($("#studentLogLev2Item").width()); // IE is suck!
				getStudentLog();
			});
		}
	});

	$(document).on('change', '#studentLogLev2Item', function(){
		getStudentLog();
	});

	$('#studentLogBtn').click(function(){
		getStudentLog();
	});
	
	getStudentLog();	
});

function getStudentLog() {
	$.ajax({
		dataType: "json",
		type: "POST",
		url: '?t=management.ajax.ajax&ma=1&action=getStudentLog',
		data : $('#studentLogForm').serialize(),		  
		success: showStudentLogs,
		error: show_ajax_error
	});
}

function showStudentLogs(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#studentLogList').html(ajaxReturn.html);
		if (parseInt(ajaxReturn.html2) > 0) {
			$('#selectBtnDiv').css('display','');
		}
		else {
			$('#selectBtnDiv').css('display','none');
		}
	}
}	

function checkDuplicateSelectStudentLog() {
	var ret = true;
	$(":input[name^='studentLogID']:checked").each(function(){
		var thisStudentLogID = $(this).val();
		$(":input[name^='selStudentLogID']").each(function(){
			if ($(this).val() == thisStudentLogID) {
				ret = false;
			}	
		})
	});

	return ret;	
}

function selectStudentLogs() {
    if(countChecked(document.studentLogForm,"studentLogID[]")==0) {
    	alert(globalAlertMsg2);
    }
    else if (!checkDuplicateSelectStudentLog()) {
    	alert('<?php echo $Lang['medical']['staffEvent']['warning']['duplicateStudentLog'];?>');
    } 
    else{
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '?t=management.ajax.ajax&ma=1&action=getSelectedStudentLogs',
			data : $('#studentLogForm').serialize(),
			success: returnStudentLogs,
			error: show_ajax_error
		});

    	js_Hide_ThickBox();
    }
}

function returnStudentLogs(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#relevantStudentLogTable').css('display','');
		$('#relevantStudentLogTable').append(ajaxReturn.html);
	}
}	

function removeStudentLog(studentLogID) {
	$('#studentLogRow'+studentLogID).remove();
}

function changeStudentLogClass() {
	$.ajax({
		dataType: "json",
		type: "POST",
		url: '?t=management.ajax.ajax&ma=1',
		data : {
			'action': 'getStudentNameByClassForStudentLog',
			'className': $('#studentLogClassName').val(),
			'groupID': $('#studentLogGroupID').val()
		},		  
		success: updateStudentListInStudentLogSelect,
		error: show_ajax_error
	});
}

function changeStudentLogGroup()
{
	$.ajax({
		dataType: "json",
		type: "POST",
		url: '?t=management.ajax.ajax&ma=1',
		data : {
			'action': 'getStudentNameByGroupIDForStudentLog',
			'groupID': $('#studentLogGroupID').val(),
			'className': $('#studentLogClassName').val()
		},		  
		success: updateStudentListInStudentLogSelect,
		error: show_ajax_error
	});
}

function updateStudentListInStudentLogSelect(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#studentLogStudentID').replaceWith(ajaxReturn.html);
		getStudentLog();
	}
}	
	
function show_ajax_error() 
{
	alert('<?=$Lang['medical']['general']['error']['ajax']?>');
}

</script>

<form name="studentLogForm" id="studentLogForm" method="POST">
	<div class="content_top_tool">

		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="20%"><?=$toolbar ?></td>
				<td width="50%" align="center">&nbsp;</td>
				<td width="30%" style="float: right;">
					<div class="content_top_tool" style="float: right;">
						<?=$htmlAry['searchBox']?>	
						<br style="clear: both" />
					</div>
				</td>
			</tr>
		</table>

	</div>

	<div class="table_board">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="bottom">
					<div class="table_filter">
    					<?=$classFilter ?>
    					<?=$groupFilter ?>
    					<?=$studentFilter?>
    					<?=$lev1Filter?>
    					<span id="studentLogLev2Span"><?=$lev2Filter?></span>
					</div> <br style="clear: both" />
					<div>
						<input type="checkbox" name="studentLogDate" id="studentLogDate"
							value="1" <?=$studentLogDate==1 ? "checked" : ""?>> <label
							for="studentLogDate"><?=$Lang['medical']['filter']['FilterDateRange']?></label>
						<div id="spanRequest" style="position:relative;display:<?=$studentLogDate==1 ? "inline" : "none"?>">
							<label for="studentLogDateStart"><?=$i_From?></label> : <?=$linterface->GET_DATE_PICKER("studentLogDateStart",$studentLogDateStart)?>
					 		<label for="studentLogDateEnd"><?=$i_To ?></label> <?=$linterface->GET_DATE_PICKER("studentLogDateEnd",$studentLogDateEnd)?>
					 		<?=$linterface->GET_ACTION_BTN($Lang['Btn']['View'], 'button',$ParOnClick="", $ParName="studentLogBtn")?>
						</div>
					</div>

				</td>
				<td valign="bottom">
					<div class="common_table_tool" id="selectBtnDiv"
						style="width: 50px; display: none">
						<a href="javascript:selectStudentLogs()" class="tool_other"><?=$Lang['Btn']['Select'] ?></a>
					</div>
				</td>
			</tr>
		</table>
	</div>

	<div class="table_board">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="bottom" id="studentLogList"></td>
			</tr>
		</table>
	</div>

</form>
