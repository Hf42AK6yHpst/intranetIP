<?php 
/*
 *  2018-12-19 Cameron
 * - create this file
 * 
 */

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='STAFFEVENT_VIEW') || !$plugin['medical_module']['staffEvents']){
    header("Location: /");
    exit();
}


include ($intranet_root . '/home/eAdmin/StudentMgmt/medical/management/select_event.php');
?>
<script>
function checkDuplicateSelectEvent() {
	var ret = true;
	$(":input[name^='EventID']:checked").each(function(){
		var thisEventID = $(this).val();
		$(":input[name^='SelEventID']").each(function(){
			if ($(this).val() == thisEventID) {
				ret = false;
			}	
		})
	});
	return ret;	
}

function selectEvents() {
    if(countChecked(document.form2,"EventID[]")==0) {
    	alert(globalAlertMsg2);
    }
    else if (!checkDuplicateSelectEvent()) {
    	alert('<?=$Lang['medical']['case']['Warning']['DuplicateEvent']?>');
    } 
    else{
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '?t=management.ajax.ajax&ma=1&action=getSelectedEventsForStaffEvent',
			data : $('#form2').serialize(),
			success: returnEvents,
			error: show_ajax_error
		});

    	js_Hide_ThickBox();
    }
}

function removeEvent(eventID) {
	$('#event_row_'+eventID).remove();
}

function returnEvents(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#relevantStudentEventTable').css('display','');
		$('#relevantStudentEventTable').append(ajaxReturn.html);
	}
}	

</script>