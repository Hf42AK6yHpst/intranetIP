<?php 
/*
 *  2018-05-29 Cameron
 *      - apply stripslashes to Name and Code field
 */

$objDB = new libdb();

if($fieldID == 3){
	$sql = "SELECT
		StatusName, 
		StatusCode 
	FROM 
		MEDICAL_SLEEP_STATUS 
	WHERE 
		RecordStatus=1 
	AND 
		DeletedFlag=0
	AND
		StatusCode <> ''";
}else if($fieldID == 4){
	$sql = "SELECT 
		MSS.StatusName,
		MSSR.ReasonName, 
		MSSR.ReasonCode
	FROM 
		MEDICAL_SLEEP_STATUS_REASON MSSR
	INNER JOIN
		MEDICAL_SLEEP_STATUS MSS
	ON
		MSSR.StatusID = MSS.StatusID
	WHERE 
		MSSR.RecordStatus=1 
	AND 
		MSSR.DeletedFlag=0
	AND
		MSSR.ReasonCode <> ''
	ORDER BY
		MSS.StatusID, MSSR.ReasonID";
}else{
	echo 'Wrong FieldID.';
	exit;
}

$rs = $objDB->returnResultSet($sql);

$ui = new common_ui();



@ob_start();
?>

<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">
	<tr class="tabletop">
		<td align="center" valign="middle">#</td>
		<?php if($fieldID == 3){ ?>
			<td><?=$Lang['medical']['general']['tableHeader']['Category']?></td>
			<td><?=$Lang['General']['Code']?></td>
		<?php }else if($fieldID == 4){ ?>
			<td><?=$Lang['medical']['general']['tableHeader']['Category']?></td>
			<td><?=$Lang['medical']['studentLog']['tableHeader']['Item']?></td>
			<td><?=$Lang['General']['Code']?></td>
		<?php } ?>
	</tr>
	<?php foreach ($rs as $index=>$r) { ?>
		<?php if($fieldID == 3){ ?>
			<tr class="tablerow1">
				<td><?=($index+1)?></td>
				<td><?=stripslashes($r['StatusName'])?></td>
				<td><?=stripslashes($r['StatusCode'])?></td>
			</tr>
		<?php }else if($fieldID == 4){ ?>
			<tr class="tablerow1">
				<td><?=($index+1)?></td>
				<td><?=stripslashes($r['StatusName'])?></td>
				<td><?=stripslashes($r['ReasonName'])?></td>
				<td><?=stripslashes($r['ReasonCode'])?></td>
			</tr>
		<?php } ?>
	<?php } ?>
</table>

<?php
$data = @ob_get_contents();
@ob_end_clean();


echo $ui->generatePopUpBox($data);

?>