<?php 

//Using: 
/*
 *  2016-01-05 [Pun]
 *  	- fixed cannot save student log if the log has attachment 
 *  		due to PHP5.4 different behaviour with $_POST and $_FILES 
 * 	2015-12-04 [Cameron]
 * 		- remove pass by reference in getEventLogInputUI() to support php 5.4
 */

switch($action){
	case 'edit':
		
		#### Merge $_POST and $_FILES START ####
		$event = $_POST['event'];
		foreach ((array)$_FILES['event']['tmp_name'] as $k=>$f){
			$event[$k] = array_merge($event[$k], $f);
		}
		#### Merge $_POST and $_FILES END ####
		
		include_once($PATH_WRT_ROOT."/home/eAdmin/StudentMgmt/medical/management/studentLogAddItemSave.php");
		if($Msg == $Lang['General']['ReturnMessage']['UpdateSuccess']){
			$objStudentLog = new StudentLog($_logRecordId);
			echo $objMedical->getEventLogInputUI($_logRecordId,$linterface,$objStudentLog);
		}else{
			echo $Msg;
		}
		break;
	case 'delete':
		$eventIDArray = $recordID;
		include_once($PATH_WRT_ROOT."/home/eAdmin/StudentMgmt/medical/management/studentLogAddItemSave.php");
		echo $Msg;
		break;
	default:
		exit;
}


?>