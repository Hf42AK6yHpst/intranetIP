<?php 
/*
 *  2018-05-29 Cameron
 *      - apply stripslashes to Name and Code field
 */

$objDB = new libdb();

if($fieldID == 1){
	$sql = "SELECT
		StatusName, 
		BarCode 
	FROM 
		MEDICAL_BOWEL_STATUS 
	WHERE 
		RecordStatus=1 
	AND 
		DeletedFlag=0
	AND
		BarCode <> ''";
}else{
	echo 'Wrong FieldID.';
	exit;
}
$rs = $objDB->returnResultSet($sql);

$ui = new common_ui();



@ob_start();
?>

<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">
	<tr class="tabletop">
		<td align="center" valign="middle">#</td>
		<?php if($fieldID == 1){ ?>
			<td><?=$Lang['medical']['general']['tableHeader']['Category']?></td>
			<td><?=$Lang['medical']['bowel_setting']['BarCode']?></td>
		<?php } ?>
	</tr>
	<?php foreach ((array)$rs as $index=>$r) { ?>
		<?php if($fieldID == 1){ ?>
			<tr class="tablerow1">
				<td><?=($index+1)?></td>
				<td><?=stripslashes($r['StatusName'])?></td>
				<td><?=stripslashes($r['BarCode'])?></td>
			</tr>
		<?php } ?>
	<?php } ?>
</table>

<?php
$data = @ob_get_contents();
@ob_end_clean();


echo $ui->generatePopUpBox($data);

?>