<?php 
/*
 *  2018-05-29 Cameron
 *      - apply stripslashes to Name and Code field
 */
$objDB = new libdb();

if($fieldID == 4){ // $fieldID => Column. Eg. Column 5 : *Log Type Code
	$sql = "SELECT
		Lev1Name, 
		Lev1Code 
	FROM 
		MEDICAL_STUDENT_LOG_LEV1 
	WHERE 
		RecordStatus=1 
	AND 
		DeletedFlag=0
	AND
		Lev1Code <> ''";
}else if($fieldID == 5){
	$sql = "SELECT 
		MSLL1.Lev1Name,
		MSLL2.Lev2Name, 
		MSLL2.Lev2Code 
	FROM 
		MEDICAL_STUDENT_LOG_LEV2 MSLL2
	INNER JOIN
		MEDICAL_STUDENT_LOG_LEV1 MSLL1
	ON
		MSLL2.Lev1ID = MSLL1.Lev1ID
	WHERE 
		MSLL2.RecordStatus=1 
	AND 
		MSLL2.DeletedFlag=0
	AND
		MSLL2.Lev2Code <> ''
	ORDER BY
		MSLL1.Lev1ID, MSLL2.Lev2Code";
}else if($fieldID == 6){
	$sql = "SELECT 
		Lev3Name,
		Lev4Name,
		Lev4Code
	FROM 
		MEDICAL_STUDENT_LOG_LEV4 MSLL4
	INNER JOIN
		MEDICAL_STUDENT_LOG_LEV3 MSLL3
	ON
		MSLL3.Lev3ID = MSLL4.Lev3ID
	WHERE 
		MSLL4.RecordStatus=1 
	AND 
		MSLL4.DeletedFlag=0
	AND
		MSLL4.Lev4Code <> ''
	ORDER BY
		MSLL3.Lev3ID, MSLL4.Lev4Code";
}else{
	echo 'Wrong FieldID.';
	exit;
}
$rs = $objDB->returnResultSet($sql);

$ui = new common_ui();



@ob_start();
?>

<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">
	<tr class="tabletop">
		<td align="center" valign="middle">#</td>
		<?php if($fieldID == 4){ ?>
			<td><?=$Lang['medical']['general']['tableHeader']['Category']?></td>
			<td><?=$Lang['General']['Code']?></td>
		<?php }else if($fieldID == 5){ ?>
			<td><?=$Lang['medical']['general']['tableHeader']['Category']?></td>
			<td><?=$Lang['medical']['studentLog']['tableHeader']['Item']?></td>
			<td><?=$Lang['General']['Code']?></td>
		<?php }else if($fieldID == 6){ ?>
			<td><?=$Lang['medical']['studentLog']['bodyParts']?></td>
			<td><?=$Lang['medical']['studentlog_setting']['convulsion']['syndrome']?></td>
			<td><?=$Lang['General']['Code']?></td>
		<?php } ?>
	</tr>
	<?php foreach ($rs as $index=>$r) { ?>
		<?php if($fieldID == 4){ ?>
			<tr class="tablerow1">
				<td><?=($index+1)?></td>
				<td><?=stripslashes($r['Lev1Name'])?></td>
				<td><?=stripslashes($r['Lev1Code'])?></td>
			</tr>
		<?php }else if($fieldID == 5){ ?>
			<tr class="tablerow1">
				<td><?=($index+1)?></td>
				<td><?=stripslashes($r['Lev1Name'])?></td>
				<td><?=stripslashes($r['Lev2Name'])?></td>
				<td><?=stripslashes($r['Lev2Code'])?></td>
			</tr>
		<?php }else if($fieldID == 6){ ?>
			<tr class="tablerow1">
				<td><?=($index+1)?></td>
				<td><?=stripslashes($r['Lev3Name'])?></td>
				<td><?=stripslashes($r['Lev4Name'])?></td>
				<td><?=stripslashes($r['Lev4Code'])?></td>
			</tr>
		<?php } ?>
	<?php } ?>
</table>
<?php
$data = @ob_get_contents();
@ob_end_clean();


echo $ui->generatePopUpBox($data);

/*
$data = <<<HTML
	<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">
		<tr class="tabletop">
			<td align="center" valign="middle">#</td>
HTML;
if($fieldID == 4){
	$data = <<<HTML
		<td>{$Lang['medical']['general']['tableHeader']['Category']}</td>
		<td>{$Lang['General']['Code']}</td>
HTML;
}else if($fieldID == 5){
	$data = <<<HTML
		<td>{$Lang['medical']['general']['tableHeader']['Category']}</td>
		<td>{$Lang['medical']['studentLog']['tableHeader']['Item']}</td>
		<td>{$Lang['General']['Code']}</td>
HTML;
}else if($fieldID == 6){
	$data = <<<HTML
		<td>{$Lang['medical']['studentLog']['bodyParts']}</td>
		<td>{$Lang['medical']['studentlog_setting']['convulsion']['syndrome']}</td>
		<td>{$Lang['General']['Code']}</td>
HTML;
}
	$data .= '</tr>';
foreach ($rs as $index=>$r) {
	$i = $index+1;
	if($fieldID == 4){
		$data .= <<<HTML
		<tr class="tablerow1">
			<td>{$i}</td>
			<td>{$r['Lev1Name']}</td>
			<td>{$r['Lev1Code']}</td>
		</tr>
HTML;
	}else if($fieldID == 5){
		$data .= <<<HTML
		<tr class="tablerow1">
			<td>{$i}</td>
			<td>{$r['Lev1Name']}</td>
			<td>{$r['Lev2Name']}</td>
			<td>{$r['Lev2Code']}</td>
		</tr>
HTML;
	}else if($fieldID == 6){
		$data .= <<<HTML
		<tr class="tablerow1">
			<td>{$i}</td>
			<td>{$r['LEV_NAME']}</td>
			<td>{$r['LEV_CODE']}</td>
		</tr>
HTML;
	}
}
$data .= <<<HTML
	</table>
HTML;
	echo $ui->generatePopUpBox($data);
*/
?>