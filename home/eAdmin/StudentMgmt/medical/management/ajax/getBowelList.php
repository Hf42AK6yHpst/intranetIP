<?php
// using:
/*
 * 
 *  2020-05-25 [Cameron]
 *      - fix bug: click newBtn "Input By" and "Last Updated By" cannot move to next line [case #E185635]
 *
 *  2018-06-05 [Cameron]
 *      - show created by person [case #F133828]
 *      - change removeRecord()
 * 
 *  2018-05-11 Cameron
 *      - fix: add function on bowelIDSelection change
 *      
 * 	2017-07-27 Cameron
 * 		- add argument $presetRowHeight=15 to GetPresetText() [case #P120778]
 * 		- use div instead of span for showing remarks and AttendanceStatusExplain to avoid overlap problem
 * 		- set row size to show all remark context when edit
 * 		- auto adjust the textarea height when edit remark
 *
 * 	2017-07-25 [Cameron]
 * 		- add argument $append=1 to GetPresetText()
 * 
 * 	2017-07-17 [Cameron]
 * 		- not need to apply stripslashes to remark field
 */
$objBowelStatus = new bowelStatus();
$objlibMedical = new libMedical();
$resultList = array();
$studentDetailList = array();


//////////////// INPUT CHECKING /////////
if(!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $_POST['date'])){
	echo 'Date Format Error! Please select with format YYYY-MM-DD';
	return;
}

### Generate Legend HTML Start
$legendHTML ='';
$legendHTML .='<ul class="legend" style="list-style-type: none;">';

$bowelDetail = $objBowelStatus->getActiveStatus(' StatusCode ASC ');
$i=0;
foreach( (array)$bowelDetail as $bowelItem){
	$legendHTML .='<li><span class="colorBoxStyle" style="background-color:'.$bowelItem['Color'].';">&nbsp;&nbsp;&nbsp;</span>&nbsp;'. stripslashes($bowelItem['StatusName']).'</li>&nbsp;&nbsp;';
	if(++$i%8 == 0){
		$legendHTML .='<br /><br />';
	}
}
if(libMedicalShowInput::attendanceDependence('bowel')){
	$legendHTML .= '<li><span class="colorBoxStyle" style="background-color:lightgray;">&nbsp;&nbsp;&nbsp;</span>&nbsp;'. $Lang['medical']['sleep']['tableHeaderOption']['AttendanceOption']['Absent'] . '</li>';
}
$legendHTML .='</ul>';
### Generate Legend HTML End

### Generate Result Table Start
$resultList = $objlibMedical->getBowelDailyLogList(
				$_POST['date'], $_POST['gender'], 
				$_POST['sleep'], $_POST['classID'] );
//debug_r($resultList);exit();
$groupStudentList = $objMedical->getMedicalGroupStudent($_POST['groupID']);

$groupResult = array();
foreach($resultList as $rs)
{
	if($_POST['groupID'] != ''){
		if(in_array(array('UserID'=>$rs['UserID']),$groupStudentList)){
			$groupResult[$rs['UserID']][] = $rs;
		}
	}else{
		$groupResult[$rs['UserID']][] = $rs;
	}
}

//debug_r($groupResult);

$todayDate = date( 'Y-m-d' );
$nonFutureDate = strtotime($_POST['date']) <= strtotime($todayDate);

$index = 1;
$i=1;
foreach( $groupResult as $rs){
//	if(!$plugin['attendancestudent']){
	if(!libMedicalShowInput::attendanceDependence('bowel')){
		$rs[0]['Present']=1; // Always present if the school have not buy attendance module
	}
	$indexHTML = '<span>'.$index.'</span>';
	
	$userID = $rs[0]['UserID'];
	
	if($rs[0]['RecordID'] !=''){
		$indexHTML .= '<input name="bowelRecord['.$userID.'][RecordID][]" type="hidden" value="'.$rs[0]['RecordID'].'">';
	}
	$studentDetailList[$i][] = $indexHTML;
	
	if($rs[0]['Present'] == 1 || libMedicalShowInput::absentAllowedInput('bowel')){
		$addBtnHTML = '<span class="table_row_tool">';
		$addBtnHTML .= '<a class="newBtn add" data-sid="'.$userID.'"  title="'.$Lang['Btn']['Add'].'"></a>';
		$addBtnHTML .= '<br /></span>';
		$studentDetailList[$i][] = $addBtnHTML;
	}else{
		$studentDetailList[$i][] = '';
	}
	
	$studentDetailList[$i][] = $rs[0]['ClassName'];
	$studentDetailList[$i][] = $rs[0]['Name'];
	$studentDetailList[$i][] = (($rs[0]['Stay'])?$Lang['medical']['meal']['search']['sleepOption'][1]:$Lang['medical']['meal']['search']['sleepOption'][2]);
	
	if(!libMedicalShowInput::attendanceDependence('bowel')){
		$studentDetailList[$i][] = '<span>--</span>';
	}else if($rs[0]['Present'] == 1){
		$studentDetailList[$i][] = $Lang['medical']['meal']['tableHeaderOption']['AttendanceOption']['Present'];
	}else{
		$studentDetailList[$i][] = $Lang['medical']['meal']['tableHeaderOption']['AttendanceOption']['Absent'];
	}
	if($rs[0]['RecordID'] !='' && $nonFutureDate){
		
		for($recordI=0,$recordIMax=count($rs);$recordI<$recordIMax;$recordI++){
			if($recordI>0){
				$studentDetailList[$i][] = '<input name="bowelRecord['.$userID.'][RecordID][]" type="hidden" value="'.$rs[$recordI]['RecordID'].'">';
				$studentDetailList[$i][] = '';
				$studentDetailList[$i][] = '';
				$studentDetailList[$i][] = '';
				$studentDetailList[$i][] = ($rs[0]['Present'] == 1)?'':'<span style="display:none">ABSENT</span>';
				$studentDetailList[$i][] = '';
			}
			
			$studentDetailList[$i][] = $objBowelStatus->getHTMLSelection($htmlName = 'bowelRecord['.$userID.'][bowelID][]',$userSelectedValue = ($rs[$recordI]['BowelID'] !='')?$rs[$recordI]['BowelID']:'','class="bowelIDSelection"');
			$studentDetailList[$i][] = createTimeSelectionBoxHTML($userID,$rs[$recordI]['RecordTime']);
			
			$remarksHTML = '';
			if($rs[$recordI]['Remarks'] !=''){
//				$remarksHTML .='<span class="viewArea">'.stripslashes(nl2br($rs[$recordI]['Remarks'])).'<br /></span>';
				$remarksHTML .='<div class="viewArea">'.nl2br($rs[$recordI]['Remarks']).'<br /></div>';
				$rows = 'rows="'.((int)substr_count($rs[$recordI]['Remarks'],"\n") + 2).'"';
			}
			else {
				$rows = '';
			}
			$remarksHTML .= '<a class="remarksBtn tablelink" ><div>'.$Lang['medical']['meal']['tableContent'][0].'</div></a>';
			$remarksHTML .= '<textarea name="bowelRecord['.$userID.'][remarks][]" id="bowelRecord['.$userID.'][remarks]['.$i.']" '.$rows.' onkeyup="resizeTextArea(this);" style="display:none; overflow:hidden;">';
			$remarksHTML .= $rs[$recordI]['Remarks'];//nl2br($rs[$recordI]['Remarks']);
			$remarksHTML .= '</textarea>';
			$remarksHTML .= '<span class="defaultRemarkSpan" style="display:none;">'.GetPresetText("jRemarksArr", $i, "bowelRecord[$userID][remarks][$i]", "", "", "", "1", "15").'</span>';
			$studentDetailList[$i][] = $remarksHTML;
			
			$studentDetailList[$i][] = '<span>'.(($rs[$recordI]['CreatedBy'] !='')?$rs[$recordI]['CreatedBy']:'-').'</span>';
			$studentDetailList[$i][] = '<span>'.(($rs[$recordI]['ModifyBy'] !='')?$rs[$recordI]['ModifyBy']:'-').'</span>';
			$studentDetailList[$i][] = '<span>'.(($rs[$recordI]['DateModified'] !='')?date( 'Y-m-d H:i', strtotime($rs[$recordI]['DateModified'])):'-').'</span>';
			
			$delBtnHTML = '<span class="table_row_tool">';
			$delBtnHTML .= '<a class="deleteBtn delete" title="'.$Lang['Btn']['Delete'].'" data-recordID="'.$rs[$recordI]['RecordID'].' " data-sid="'.$userID.'"  ></a>';
			$delBtnHTML .= '</span>';
			$studentDetailList[$i][] = $delBtnHTML;
			
			++$i;
		}
	}else{
//		$studentDetailList[$i][] = $Lang['medical']['meal']['tableHeaderOption']['AttendanceOption']['Absent'];
		$studentDetailList[$i][] = '<span>-</span>';
		$studentDetailList[$i][] = '<span>-</span>';
		$studentDetailList[$i][] = '<span>-</span>';
		$studentDetailList[$i][] = '<span>-</span>';
		$studentDetailList[$i][] = '<span>-</span>';
		$studentDetailList[$i][] = '<span>-</span>';
		$studentDetailList[$i][] = '';
	}
		
	++$i;
	++$index;
}
### Generate Result Table End

/**
 * To generate time dropdown field
 * @param (SQL)datetime $time
 * @return HTML field of bowelTime.
 */
function createTimeSelectionBoxHTML($userID, $time='1900-00-00 00:00:00'){
	
	$html = "<span style='white-space:nowrap;'><select name='bowelRecord[$userID][bowelTime][hour][]' id='bowelRecord[$userID][bowelTime][hour][]' class='bowelTimeHourSelection'>";
	$time_HH = date( 'H', strtotime($time));
	$time_MM = date( 'i', strtotime($time));
//	$time_MM = ( (int)($time_MM/5) ) *5;
	for($hh=0,$hhMax=24;$hh<$hhMax;$hh++){
		if($hh == $time_HH){
			$isSelected = ' SELECTED ';
		}else{
			$isSelected = '';
		}
		$html .= "<option value='".sprintf("%02d", $hh)."'$isSelected>".sprintf("%02d", $hh)."</option>";
	}
	$html .= "</select><span>:</span><select name='bowelRecord[$userID][bowelTime][minute][]' id='bowelRecord[$userID][bowelTime][minute][]' class='bowelTimeMinuteSelection'>";

	$objMedical = new libMedical();
	$timeFormat = $objMedical->getBowelTimeInterval();
//$timeFormat = 30;
	for($i=0;$i<59;$i+=$timeFormat){
		$min = $i;
		$max = $i + $timeFormat - 1;
		$value = $max;
		$minDisplay = str_pad($min, 2, '0', STR_PAD_LEFT);
		$maxDisplay = str_pad($max, 2, '0', STR_PAD_LEFT);
		$html .= "<option value='{$value}' " . (($time_MM >= $min && $time_MM <= $max)? 'SELECTED' : '') . ">{$minDisplay}-{$maxDisplay}</option>";
		}
	$html .= '</select></span>';
	return $html;
}

#### Get default remarks ####
$defaultRemark = $objlibMedical->getDefaultRemarksArray($medical_cfg['general']['module']['bowel']);
//////////////////////////////// Presentation Layer //////////////////////////////////////
echo <<< REMARKS_INIT
	<script>
		var jRemarksCount=$i;
		var jRemarksArr=$defaultRemark;
	</script>
REMARKS_INIT;
?>
<style>
.viewArea{
	-ms-word-break: break-all;
	-webkit-hyphens: auto;
	-moz-hyphens: auto;
	
	word-break: break-all;
	word-break: break-word;
	hyphens: auto;
}
//.tabletoplink{
//	font-weight: bold !important;
//}
.legend li{
	display: inline;
}
.colorBoxStyle{
	display: inline-block;
	border: 1px solid #c0c0c0;
	width:17px;
	height:17px;
}
.disabledField td{
	background-color: lightgray !important;
}
.tablelink{
	cursor:pointer;
}
//.tabletoplink{
//	font-weight: bold !important;
//}
#bowelForm table td, #bowelForm table td * {
    vertical-align: top;
}
.defaultRemarkSpan{
	margin-left:5px;
}
</style>

<?php
	echo '<div id="bowelIDTemplate" style="display:none">'.$objBowelStatus->getHTMLSelection('','','class="bowelIDSelection"').'</div>';
	echo '<div id="recordTimeTemplate" style="display:none">'.createTimeSelectionBoxHTML('', date('Y-m-d H:i:s')).'</div>';
		
//	$remarksHTML = '<div id="remarksTemplate">';
	$remarksHTML = '<div id="remarksTemplate" style="display:none">';
	$remarksHTML .= '<a class="remarksBtn tablelink" ><div>'.$Lang['medical']['meal']['tableContent'][0].'</div></a>';
	$remarksHTML .= '<textarea onkeyup="resizeTextArea(this);" style="display:none; overflow:hidden;"></textarea>';
	$remarksHTML .= '<span class="defaultRemarkSpan" style="display:none"></span>';
	$remarksHTML .= '</div>'; 
	echo $remarksHTML;
?>
<br />
<?php echo $legendHTML;?>

<form method="post" action="?t=management.bowelSave" name="bowelForm" id="bowelForm">
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center" >
	<colgroup>
		<col style="width:1%"/>
		<col style="width:1%"/>
		<col style="width:7%"/>
		<col style="width:5%"/>
		<col style="width:5%"/>
		<col style="width:8%"/>
		<col style="width:14%"/>
		<col style="width:12%"/>
		<col style="width:16%"/>
		<col style="width:10%"/>
		<col style="width:10%"/>
		<col style="width:10%"/>
		<col style="width:1%"/>
	</colgroup>

	<thead>
		<tr class="tabletop">
			<th class="tabletoplink" nowrap><?=$Lang['medical']['bowel']['tableHeader']['Number'] ?></th>
			<th class="tabletoplink" nowrap><?=$Lang['medical']['bowel']['tableHeader']['Add'] ?></th>
			<th class="tabletoplink" nowrap><?=$Lang['medical']['bowel']['tableHeader']['ClassName'] ?></th>
			<th class="tabletoplink" nowrap><?=$Lang['medical']['bowel']['tableHeader']['StudentName'] ?></th>
			<th class="tabletoplink" nowrap><?=$Lang['medical']['bowel']['tableHeader']['StayOrNot'] ?></th>
			<th class="tabletoplink" nowrap><?=$Lang['medical']['bowel']['tableHeader']['AttendanceStatus'] ?></th>
			<th class="tabletoplink" nowrap><?=$Lang['medical']['bowel']['tableHeader']['BowelCondition'] ?></th>
			<th class="tabletoplink" nowrap><?=$Lang['medical']['bowel']['tableHeader']['BowelTime'] ?></th>
			<th class="tabletoplink" nowrap><?=$Lang['medical']['bowel']['tableHeader']['Remarks'] ?></th>
			<th class="tabletoplink" nowrap><?=$Lang['medical']['general']['inputBy']?></th>
			<th class="tabletoplink" nowrap><?=$Lang['medical']['bowel']['tableHeader']['LastPersonConfirmed'] ?></th>
			<th class="tabletoplink" nowrap><?=$Lang['medical']['bowel']['tableHeader']['LastUpdated'] ?></th>
			<th class="tabletoplink" nowrap><?=$Lang['medical']['bowel']['tableHeader']['del'] ?></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach( $studentDetailList as $studentDetail): 
				$disabled ='';
				if( $studentDetail[5] == ($Lang['medical']['sleep']['tableHeaderOption']['AttendanceOption']['Absent']) || $studentDetail[4] == '<span style="display:none">ABSENT</span>'){
					$disabled ='class="disabledField"';
				}
		?>
			<tr <?php echo $disabled; ?> class="row_approved">
				<?php foreach( $studentDetail as $studentDetailItem): ?>
					<td valign="top"><?php echo $studentDetailItem; ?></td>
				<?php endforeach; ?>
			</tr>
		<?php endforeach; ?>
		<?php if(count($studentDetailList)==0): ?>
		<tr>
			<td colspan="13"  class="tableContent" align="center" >
					<?php echo $Lang['General']['NoRecordFound']; ?>
			</td>
		</tr>
		<?php endif; ?>
	</tbody>
	<tfoot>
			<tr>
				<td align="center" colspan="13" class="dotline" style="text-align:left">
					<div><?=$Lang['medical']['bowel']['AttendanceStatusExplain']?></div><br />
					<?php if(!libMedicalShowInput::absentAllowedInput('bowel')){ ?>
						<div><?=$Lang['medical']['bowel']['AttendanceInputExplain']?></div>
					<?php } ?>
				</td>
			</tr>
		<?php if(count($studentDetailList)>0): ?>
		<tr>
			<td align="center" colspan="13">
				<!--<input type="submit" id="Save" name="Save" class="formbutton_v30 print_hide " value="<?php echo $Lang['medical']['meal']['button']['save']; ?>"> -->
				<input type="hidden" id="deleteReacordList" name="deleteReacordList" value="">
				<input type="button" id="Save" name="Save" class="formbutton_v30 print_hide " value="<?php echo $Lang['medical']['meal']['button']['save']; ?>">
				<input type="button" id="Reset" name="Reset" class="formbutton_v30 print_hide " value="<?php echo $Lang['medical']['meal']['button']['reset']; ?>">
				<!--<input type="button" id="Cancel" name="Cancel" class="formbutton_v30 print_hide " value="<?php echo $Lang['medical']['meal']['button']['cancel']; ?>">-->
			</td>	
		</tr>
		<?php endif; ?>
	</tfoot>

</table>
<input type="hidden" name="date" value="<?php echo $_POST['date'] ?>" />
</form>

<script>
	$('.bowelIDSelection').live('click', function(){
		$(this).prev().css('background-color', $(this).find(":selected").attr('data-color'));
	}).click();

	$('.bowelIDSelection').live('change', function(){
		$('.bowelIDSelection').click();
	});
	
	$('.remarksBtn').live('click', function(){
		$(this).hide();
		$(this).prev().hide();
		$(this).next().show();
		$(this).next().next().show();

		$(this).next().focus();
		var psconsole = $(this).next();
    	psconsole.scrollTop(
        psconsole[0].scrollHeight - psconsole.height()
    	);
	});

	$('.deleteBtn').die('click').live('click', function(){
		var date = '<?=$_POST['date']?>';
		var userID = $(this).attr('data-sid');
		var recordID = $(this).attr('data-recordID');
		var trDOM = $(this).closest('tr');
		
		var isDeleted = confirm('<?php echo $Lang['medical']['report_general']['deleteEvent'] ?>');
		if(!isDeleted){
			return;
		}
		
		var nextTrDom = trDOM.next();
		trDOM.find('td:eq(0) > input').remove();
		
		if(recordID == ''){ // new record (not in DB)
			if(trDOM.find('td:eq(0) > span').length > 0){
				removeRecord(trDOM);
			}else{
				trDOM.remove();
			}
		}else{ // old record (in DB)
			$("#deleteReacordList").val($("#deleteReacordList").val()+','+recordID);
			removeRecord(trDOM);
			/*
			$.post('?t=management.ajax.deleteBowelRecord',{RecordID: recordID, Date: date}, function(responseResult){
				if(responseResult){ // Delete successful
					msg ='<?php echo $Lang['General']['ReturnMessage']['UpdateSuccess'];?>';
					removeRecord(trDOM);
				}else{
					msg = '<?php echo $Lang['General']['ReturnMessage']['UpdateUnsuccess'];?>';
				}
				Get_Return_Message(msg, 'system_message_box');
				$("html, body").animate({ scrollTop: 0 }, 0);
			});
			*/
		}
	});

    function removeRecord(recordTrDOM){
    	var nextTrDom = recordTrDOM.next();
    	recordTrDOM.find('td:eq(0) > input').remove();
    	if(nextTrDom.length > 0){		// next row exist
    		if(nextTrDom.find('td:eq(0) > span').length == 0){	// Replace this line by next record, next row is not the first one of a student
    			recordTrDOM.find('td:eq(0)').append(nextTrDom.find('td:eq(0) > *'));
    			for(var i=6;i<13;i++){
    				recordTrDOM.find('td:eq('+i+') > *').remove();
    				recordTrDOM.find('td:eq('+i+')').append(nextTrDom.find('td:eq('+i+') > *'));
    			}
    			nextTrDom.remove();
    		}else{			// next row is the first one of a student
    			if(recordTrDOM.find('td:eq(0) > span').length == 0){	// record to remove is the last one among multiple of a student
    				recordTrDOM.remove();
    			}else{		// record to remove is the only one of a student
    				for(var i=6;i<13;i++){
    					recordTrDOM.find('td:eq('+i+') > *').remove();
    
    					recordTrDOM.find('td:eq('+i+')').html('<span>-</span>');
    				}
    				recordTrDOM.find('td:eq(12) > *').remove();
    			}
    		}
    	}else{			// next row not exist
    		if(recordTrDOM.find('td:eq(0) > span').length == 0){		// record to remove is the last one among multiple of a student
    			recordTrDOM.remove();
    		}else{		// record to remove is the only one of a student
    			for(var i=6;i<13;i++){
    				recordTrDOM.find('td:eq('+i+') > *').remove();
    
    				recordTrDOM.find('td:eq('+i+')').html('<span>-</span>');
    			}
    			recordTrDOM.find('td:eq(12) > *').remove();
    		}
    	}
    }


	$('.newBtn').click(function(){
		var userID = $(this).attr('data-sid');
		var trDOM = $(this).closest('tr');

		if(trDOM.find('td:eq(0) > input').length > 0){
			trDOM.after('<tr class="'+trDOM.attr('class')+'"><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>');
			var trNextDOM = trDOM.next();
			trNextDOM.find('td:eq(0)').append(trDOM.find('td:eq(0) > input'));
			for(i=6;i<=12;i++){
				trNextDOM.find('> td:eq('+i+')').append(trDOM.find('td:eq('+i+') > *'));
			}
		}else{
			for(i=6;i<=12;i++){
				trDOM.find('td:eq('+i+') > *').remove();
			}
		}

		var bowelIDName = 'bowelRecord['+userID+'][bowelID][]';
		var bowelTimeHourName = 'bowelRecord['+userID+'][bowelTime][hour][]';
		var bowelTimeMinuteName = 'bowelRecord['+userID+'][bowelTime][minute][]';
		var remarksName = 'bowelRecord['+userID+'][remarks][]';
		
		setElementName($('#bowelIDTemplate').find('select'),bowelIDName);
		setElementName($('#recordTimeTemplate').find('select:eq(0)'),bowelTimeHourName);
		setElementName($('#recordTimeTemplate').find('select:eq(1)'),bowelTimeMinuteName);
		setElementName($('#remarksTemplate').find('textarea'),remarksName);
		/*
			It doesn't work with IE7 >_<
			$('#bowelIDTemplate').find('select').attr('name',bowelIDName);
			$('#recordTimeTemplate').find('select:eq(0)').attr('name',bowelTimeHourName);	
			$('#recordTimeTemplate').find('select:eq(1)').attr('name',bowelTimeMinuteName);		
			$('#remarksTemplate').find('textarea').attr('name',remarksName);		
		*/

		

		trDOM.find('td:eq(0)').append('<input name="bowelRecord['+userID+'][RecordID][]" type="hidden" value="">');
		trDOM.find('td:eq(6)').append($('#bowelIDTemplate').html());
		trDOM.find('td:eq(7)').append($('#recordTimeTemplate').html());
		trDOM.find('td:eq(8)').append($('#remarksTemplate').html());
		trDOM.find('td:eq(9)').append('<span>-</span>');
		trDOM.find('td:eq(10)').append('<span>-</span>');
		trDOM.find('td:eq(11)').append('<span>-</span>');
		trDOM.find('td:eq(12)').append('<span class="table_row_tool"><a class="deleteBtn delete" title="<?=$Lang["Btn"]["Delete"]?>" data-recordID="" data-sid="'+userID+'"  ></a></span>');
		
		if(trDOM.parent().find('input[name="bowelRecord['+userID+'][RecordID][]"]').length == <?=$medical_cfg['general']['bowel']['maximumRecordCount']?>){
			trDOM.parent().find('a.newBtn[data-sid="'+userID+'"]').hide();
		}


		var remarksID = 'bowelRecord['+userID+'][remarks][' + jRemarksCount + ']';
		trDOM.find('td:eq(8)').find('textarea').attr('id',remarksID);
		$.ajax({
			url : "?t=management.ajax.getRemarksDefaultList",
			type : "GET",
			data : {
				jArrName: 'jRemarksArr',
				btnID: jRemarksCount,
				targetName: remarksID,
				jOtherFunction: '',
				divIDPrefix: ''
			},
			success : function(result) {
				$('textarea[id="' + remarksID + '"]').next().html(result);
			}
		});
		jRemarksCount++;

		// Hide all opened default-remarks-dialog
		$('div > a[onclick*="setDivVisible"]').click();
	});
/**
 * Specially for IE7, you know it...
 * Reference: http://stackoverflow.com/questions/2094618/changing-name-attr-of-cloned-input-element-in-jquery-doesnt-work-in-ie6-7
 */
function setElementName(elems, name) {
	if ($.browser.msie === true){
		$(elems).each(function() {
			this.mergeAttributes(document.createElement("<input name='" + name + "'/>"), false);
		});
	} else {
		$(elems).attr('name', name);
	}
}

	$('.defaultRemarkSpan > a').live('click',function(){
		$('iframe#lyrShim').hide();
	});
	
	
	$('#Reset').click(function(){
		$('#bowelForm')[0].reset();
		$('.bowelStatusSelection').change();
	});
	$('#Save').click(function(){
		if(checkDuplicate()){
			$.ajax({
				url : "?t=management.bowelSave",
				type : "POST",
				data : $('#bowelForm').serialize(),
				success : function(result) {
					if(result){
						msg ='<?php echo $Lang['General']['ReturnMessage']['UpdateSuccess'];?>';
					}
					else{
						msg = '<?php echo $Lang['General']['ReturnMessage']['UpdateUnsuccess'];?>';
					}
					Get_Return_Message(msg, 'system_message_box');
					$("html, body").animate({ scrollTop: 0 }, 0);
					$('#Search').click();
				}
			});
		}else{
			alert('<?=$Lang['medical']['bowel']['import']['duplicateRecordBelow']?>');
		}
	});

	function checkDuplicate(){
		var noDuplicate = true;
		var dataSet = new Array();
		$('#bowelForm > table > tbody > tr').each(function(index, element){
			var data = '' + $(element).find('a.deleteBtn').attr('data-sid') + '_'
						+ $(element).find('select[name$="][bowelID][]"]').val() + '_'
						+ $(element).find('select[name$="][bowelTime][hour][]"]').val() + '_'
						+ $(element).find('select[name$="][bowelTime][minute][]"]').val();
			if(dataSet.indexOf(data) != -1 && data.indexOf('undefined') == -1){
				noDuplicate = false;
				$(element).css('background-color','red');
			}else{
				$(element).css('background-color','transparent');
			}
			dataSet.push(data);
		});
		return noDuplicate;
	}

</script>