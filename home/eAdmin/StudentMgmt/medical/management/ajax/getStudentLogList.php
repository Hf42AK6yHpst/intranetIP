<?php
// using: 
/*
 * 2019-01-04 [Pun] [157819]
 * - Added AllowAdminEditDeleteRecordOnly cust
 * 
 * 2018-06-05 [Cameron]
 * - show created by person [case #F133828]
 * 
 * 2018-04-27 [Cameron]
 * - apply stripslashes to level1 and level2
 *
 * 2017-11-24 [Cameron]
 * - don't show delete button for user who is not (superadmin or record creator or the last update person)
 *
 * 2017-09-18 [Cameron]
 * - apply stripslashes to remark field
 *
 * 2017-07-27 [Cameron]
 * - apply nl2br to remarks field
 */

// ////////////// INPUT CHECKING /////////
if (! preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $date)) {
    echo 'Date Format Error! Please select with format YYYY-MM-DD';
    return;
}

// ## Generate Legend HTML Start
$legendHTML = '';
if (libMedicalShowInput::attendanceDependence('studentLog')) {
    $legendHTML .= '<ul class="legend" style="list-style-type: none;">';
    $legendHTML .= '<li><span class="colorBoxStyle" style="background-color:lightgray;">&nbsp;&nbsp;&nbsp;</span>&nbsp;' . $Lang['medical']['sleep']['tableHeaderOption']['AttendanceOption']['Absent'] . '</li>';
    $legendHTML .= '</ul>';
}
// ## Generate Legend HTML End

// ## Generate Result Table Start
$groupStudentList = $objMedical->getMedicalGroupStudent($groupID);
$allStudentDetailList = $objMedical->getStudentLogList($date, $gender, $sleep, $classID);
$studentDetailList = array();
if ($groupID != '') {
    for ($i = 0, $count = count($allStudentDetailList); $i < $count; ++ $i) {
        if (in_array(array(
            'UserID' => $allStudentDetailList[$i]['UserID']
        ), $groupStudentList)) {
            $studentDetailList[] = $allStudentDetailList[$i];
        }
    }
} else {
    $studentDetailList = $allStudentDetailList;
}

/*
 * for($i=0, $count = count($studentDetailList); $i<$count; ++$i){
 *
 * if($_POST['groupID'] != ''){
 * if(in_array(array('UserID'=>$studentDetailList[$i]['UserID']),$groupStudentList)){
 *
 * }
 * }else{
 * $groupResult[$rs['UserID']][] = $rs;
 * }
 *
 * if($studentDetailList[$i]['RecordID']!=''){
 * $recordIDList[] = $studentDetailList[$i]['RecordID'];
 * }
 * }
 */
// Array Style

/*
 * No need to display body parts now
 * $studentBodyPartsList = array();
 * debug_r($recordIDList);
 * $studentBodyPartsList = $objMedical->getStudentLogBodyPartsList($recordIDList);
 */

// ## Generate Result Table End

echo $linterface->Include_Thickbox_JS_CSS();

function nullReplacer($data, $replaceString = '-')
{
    return ($data == '' ? $replaceString : $data);
}
// ////////////////////////////// Presentation Layer //////////////////////////////////////
?>
<style>
.table_row_tool, .tablelink {
	float: none;
	display: inline-block;
}

.viewArea {
	-ms-word-break: break-all;
	-webkit-hyphens: auto;
	-moz-hyphens: auto;
	word-break: break-all;
	word-break: break-word;
	hyphens: auto;
}

//
.tabletoplink { //
	font-weight: bold !important;
	//
}

.legend li {
	display: inline;
}

.colorBoxStyle {
	display: inline-block;
	border: 1px solid #c0c0c0;
	width: 17px;
	height: 17px;
}

.recordedField td {
	background-color: #DEF8FF !important;
}

.disabledField td {
	background-color: lightgray !important;
}

a {
	cursor: pointer;
}

//
.tabletoplink { //
	font-weight: bold !important;
	//
}

.rowColor1, .rowColor1 {
	color: black;
}

.rowColor2, .rowColor2 {
	color: blue;
}

.disabledField * {
	color: black;
}

#studentLogForm table td, #studentLogForm table td * {
	line-height: 15px;
	vertical-align: top;
}
</style>
<br />
<?php echo $legendHTML;?>

<form method="post" action="index.php" name="studentLogForm"
	id="studentLogForm">
	<table width="95%" border="0" cellpadding="5" cellspacing="0"
		align="center">
		<colgroup>
			<col style="width: 1%" />
			<col style="width: 3%" />
			<col style="width: 5%" />
			<col style="width: 6%" />
			<col style="width: 5%" />
			<col style="width: 7%" />

			<col style="width: 6%" />
			<col style="width: 9%" />
			<col style="width: 11%" />
			<col style="width: 15%" />
			<col style="width: 10%" />
			<col style="width: 10%" />
			<col style="width: 10%" />
			<col style="width: 1%" />
		</colgroup>
		<thead>
			<tr class="tabletop">
				<th class="tabletoplink">#</th>
				<th class="tabletoplink"></th>
				<th class="tabletoplink"><?php echo $Lang['Header']['Menu']['Class']; ?></th>
				<th class="tabletoplink"><?php echo $Lang['medical']['meal']['tableHeader']['StudentName']; ?></th>
				<th class="tabletoplink"><?php echo $Lang['medical']['meal']['searchMenu']['sleep']; ?></th>
				<th class="tabletoplink"><?php echo $Lang['medical']['meal']['tableHeader']['AttendanceStatus']; ?></th>

				<th class="tabletoplink"><?php echo $Lang['medical']['report_studentlog']['tableHeader']['time']; ?></th>
				<th class="tabletoplink"><?php echo $Lang['medical']['studentLog']['studentLog']; ?></th>
				<th class="tabletoplink"><?php echo $Lang['medical']['studentLog']['case']; ?></th>
				<th class="tabletoplink"><?php echo $Lang['medical']['report_meal']['tableHeader']['remarks']; ?></th>
				<!-- th class="tabletoplink"><?php echo $Lang['medical']['studentLog']['bodyParts']; ?></th-->
				<th class="tabletoplink"><?php echo $Lang['medical']['general']['inputBy']; ?></th>
				<th class="tabletoplink"><?php echo $Lang['medical']['meal']['tableHeader']['LastPersonConfirmed']; ?></th>

				<th class="tabletoplink"><?php echo $Lang['medical']['meal']['tableHeader']['LastUpdated']; ?></th>
				<th class="tabletoplink">&nbsp;</th>
			</tr>
		</thead>
		<tbody>
		<?php
$rowCount = 0;
for ($i = 0, $count = count($studentDetailList); $i < $count; ++ $i) {
    if (! libMedicalShowInput::attendanceDependence('studentLog')) {
        $studentDetailList[$i]['Present'] = 1; // Always present if the school have not buy attendance module
    }
    $isFirstRecordOfStudent = true;
    if ($studentDetailList[$i]['UserID'] == $studentDetailList[$i - 1]['UserID']) {
        $isFirstRecordOfStudent = false;
    }
    $classStyle = 'row_approved';
    if ($studentDetailList[$i]['Present'] != '1') {
        $classStyle .= ' disabledField';
    } elseif ($studentDetailList[$i]['RecordID'] != '') {
        // $classStyle .=' recordedField';
    }
    
    echo "<tr class='$classStyle'>";
    
    // ## Display student info START ###
    if ($isFirstRecordOfStudent) {
        // Sequence Number
        echo '<td>' . (++ $rowCount) . '</td>';
        
        // New Button
        echo '<td>';
        if ($studentDetailList[$i]['Present'] == '1' || libMedicalShowInput::absentAllowedInput('studentLog')) {
            echo '<span class="table_row_tool">';
            echo '<a class="newBtn add" data-sid="' . $studentDetailList[$i]['UserID'] . '"  title="' . $Lang['Btn']['Add'] . '"></a>';
            echo '</span>';
        } else {
            echo '&nbsp;';
        }
        echo '</td>';
        
        // Class Name
        echo '<td>' . $studentDetailList[$i]['ClassName'] . '</td>';
        
        // Student Name
        echo '<td>';
        if ($studentDetailList[$i]['RecordID'] != '' && ($studentDetailList[$i]['Present'] == '1' || libMedicalShowInput::absentAllowedInput('studentLog'))) {
            echo '<a class="editBtn"  data-sid="' . $studentDetailList[$i]['UserID'] . '">' . $studentDetailList[$i]['Name'] . '</a>';
        } else {
            echo $studentDetailList[$i]['Name'];
        }
        echo '</td>';
        
        // Stay/ Not Stay
        echo '<td>';
        echo ($studentDetailList[$i]['Stay'] == '1') ? $Lang['medical']['meal']['search']['sleepOption'][1] : $Lang['medical']['meal']['search']['sleepOption'][2];
        echo '</td>';
        
        // Attendance
        echo '<td>';
        if (! libMedicalShowInput::attendanceDependence('studentLog')) {
            echo '<span>--</span>';
        } else if ($studentDetailList[$i]['Present'] == '1') {
            echo $Lang['medical']['meal']['tableHeaderOption']['AttendanceOption']['Present'];
        } else {
            echo $Lang['medical']['meal']['tableHeaderOption']['AttendanceOption']['Absent'];
        }
        echo '</td>';
    } else {
        echo '<td>&nbsp</td>';
        echo '<td>&nbsp</td>';
        echo '<td>&nbsp</td>';
        echo '<td>&nbsp</td>';
        echo '<td>&nbsp</td>';
        echo '<td>&nbsp</td>';
    }
    // ## Display student info END ###
    
    // ## Set data variable START ###
    if ($studentDetailList[$i]['Lev1Name'] != '') {
        $level1 = '<a class="editBtn"  data-sid="' . $studentDetailList[$i]['UserID'] . '" data-rid="' . $studentDetailList[$i]['RecordID'] . '" href="#">' . $studentDetailList[$i]['Lev1Name'] . '</a>';
    } else {
        $level1 = '-';
    }
    if ($studentDetailList[$i]['Lev2Name'] != '') {
        $level2 = '<a class="editBtn"  data-sid="' . $studentDetailList[$i]['UserID'] . '" data-rid="' . $studentDetailList[$i]['RecordID'] . '" href="#">' . $studentDetailList[$i]['Lev2Name'] . '</a>';
    } else {
        $level2 = '-';
    }
    
    $remarks = nullReplacer(nl2br(stripslashes($studentDetailList[$i]['Remarks'])));
    
    $createdBy = nullReplacer($studentDetailList[$i]['CreatedBy']);
    $lastEdit = nullReplacer($studentDetailList[$i]['ConfirmedName']);
    
    if (strtotime($studentDetailList[$i]['RecordTime']) != '') {
        $recordTimeAndAttachment = date('H:i', strtotime($studentDetailList[$i]['RecordTime']));
        if ($objMedical->hasAttachment($studentDetailList[$i]['RecordID'])) {
            $recordTimeAndAttachment .= "<a id='fixedSizeThickboxLink' class='tablelink fixedSizeThickboxLink' href='javascript:void(0);' data-Id='{$studentDetailList[$i]['RecordID']}' title='" . $Lang['SysMgr']['Homework']['Attachment'] . "' ><img src='{$image_path}/icon/attachment_blue.gif' border=0 /></a> ";
        }
    } else {
        $recordTimeAndAttachment = '-';
    }
    
    if (strtotime($studentDetailList[$i]['DateModified']) != '') {
        $dateModify = date('Y-m-d H:i', strtotime($studentDetailList[$i]['DateModified']));
    } else {
        $dateModify = '-';
    }
    
    if ($studentDetailList[$i]['RecordID'] != '') {
        if(
            ! $objMedical->isSuperAdmin($_SESSION['UserID']) &&
            (
                (
                    $sys_custom['medical']['StudentLog']['AllowEditDeleteOwnRecordOnly'] &&
                    ($studentDetailList[$i]['InputBy'] != $_SESSION['UserID']) && 
                    ($studentDetailList[$i]['ModifyBy'] != $_SESSION['UserID'])
                ) || (
                    $sys_custom['medical']['StudentLog']['AllowAdminEditDeleteRecordOnly']
                )
            )
        ){
            $deleteButton = '&nbsp;';
        } else {
            $deleteButton = '<a style="margin: -2px 0 -3px 0" class="deleteBtn delete" title="' . $Lang['Btn']['Delete'] . '" data-recordID="' . $studentDetailList[$i]['RecordID'] . '"></a>';
        }
    } else {
        $deleteButton = '&nbsp;';
    }
    // ## Set data variable END ###
    
    // Record Time & Attachment
    echo '<td>';
    echo '<span class="rowColor' . (1) . '">' . $recordTimeAndAttachment . '</span>';
    echo '</td>';
    
    // Level 1
    echo '<td>';
    echo '<span class="rowColor' . (1) . '">' . stripslashes($level1) . '</span>';
    echo '</td>';
    
    // Level 2
    echo '<td>';
    echo '<span class="rowColor' . (1) . '">' . stripslashes($level2) . '</span>';
    echo '</td>';
    
    // Remarks
    echo '<td>';
    echo '<span class="rowColor' . (1) . '">' . $remarks . '</span>';
    echo '</td>';
    
    // Input By
    echo '<td>';
    echo '<span class="rowColor' . (1) . '">' . $createdBy . '</span>';
    echo '</td>';

    // Last edit
    echo '<td>';
    echo '<span class="rowColor' . (1) . '">' . $lastEdit . '</span>';
    echo '</td>';
    
    // Date Modified
    echo '<td>';
    echo '<span class="rowColor' . (1) . '">' . $dateModify . '</span>';
    echo '</td>';
    
    // Delete Button
    echo '<td>';
    echo '<span class="table_row_tool">' . $deleteButton . '</span>';
    echo '</td>';
    
    echo '</tr>';
}
?>
		
		<?php 
/*
       * // debug_r($studentDetailList);
       * $rowCount=0;
       * for($i=0, $count = count($studentDetailList); $i<$count; ++$i){
       * $rowHasShowStudentInfo = false;
       *
       * if($studentDetailList[$i]['UserID'] == $studentDetailList[$i-1]['UserID']){
       * continue;
       * }
       * $classStyle ='row_approved';
       * if($studentDetailList[$i]['Present']!='1'){
       * $classStyle .=' disabledField';
       * }
       * elseif($studentDetailList[$i]['RecordID']!=''){
       * $classStyle .=' recordedField';
       * }
       *
       * # Sequence Number
       * echo "<tr class='$classStyle'>";
       * echo '<td>'.(++$rowCount).'</td>';
       *
       * # New Button
       * echo '<td>';
       * echo '<span class="table_row_tool">';
       *
       * if($studentDetailList[$i]['Present']=='1'){
       * echo '<a class="newBtn add" data-sid="'.$studentDetailList[$i]['UserID'].'" title="'.$Lang['Btn']['Add'].'"></a>';
       * }
       *
       *
       * echo '<br />';
       *
       * echo '</span>';
       *
       * echo '</td>';
       *
       * # Class Name
       * echo '<td>'.$studentDetailList[$i]['ClassName'].'</td>';
       *
       * # Student Name
       * echo '<td>';
       * if($studentDetailList[$i]['RecordID']!='' && $studentDetailList[$i]['Present']=='1'){
       * echo '<a class="editBtn" data-sid="'.$studentDetailList[$i]['UserID'].'">'.$studentDetailList[$i]['Name'].'</a>';
       * }
       * else{
       * echo $studentDetailList[$i]['Name'];
       * }
       * echo '</td>';
       *
       * # Stay/ Not Stay
       * echo '<td>'.($studentDetailList[$i]['Stay']=='1'
       * ?
       * $Lang['medical']['meal']['search']['sleepOption'][1]:
       * $Lang['medical']['meal']['search']['sleepOption'][2]
       * ).'</td>';
       *
       * # Attendance
       * echo '<td>'.($studentDetailList[$i]['Present']=='1'?
       * $Lang['medical']['meal']['tableHeaderOption']['AttendanceOption']['Present']:
       * $Lang['medical']['meal']['tableHeaderOption']['AttendanceOption']['Absent']
       * ).'</td>';
       *
       * # Record Time & Attachment
       * echo '<td>';
       *
       * $row=0;
       * if(strtotime($studentDetailList[$i]['RecordTime']) !=''){
       * $j=$i;
       * do{
       * echo '<span class="rowColor'.($row++%2+1).'">'.date( 'H:i', strtotime($studentDetailList[$j]['RecordTime'])).'</span>';
       * $hasAttachment=$objMedical->hasAttachment($studentDetailList[$j]['RecordID']);
       *
       * if($hasAttachment){
       * echo "<a id='fixedSizeThickboxLink' class='tablelink fixedSizeThickboxLink' href='javascript:void(0);' data-Id='{$studentDetailList[$j]['RecordID']}' title='".$Lang['SysMgr']['Homework']['Attachment']."' ><img src='{$image_path}/icon/attachment.gif' border=0 /></a> ";
       * }
       * echo '<br />';
       * }while( $studentDetailList[$i]['UserID'] == $studentDetailList[++$j]['UserID']);
       * }
       * else{
       * echo '<span class="rowColor'.($row++%2+1).'">-</span>';
       * }
       * echo '</td>';
       *
       * # Level 1
       * echo '<td>';
       * $row=0;
       * $string = nullReplacer( $string = $studentDetailList[$i]['Lev1Name']);
       * echo '<span class="rowColor'.($row++%2+1).'"><a class="editBtn" data-sid="'.$studentDetailList[$i]['UserID'].'" data-rid="'.$studentDetailList[$i]['RecordID'].'" href="#">'.$string.'</a></span><br />';
       * $j=$i;
       * while( $studentDetailList[$i]['UserID'] == $studentDetailList[++$j]['UserID']){
       * echo '<span class="rowColor'.($row++%2+1).'"><a class="editBtn" data-sid="'.$studentDetailList[$i]['UserID'].'" data-rid="'.$studentDetailList[$j]['RecordID'].'" href="#">'.$studentDetailList[$j]['Lev1Name'].'</a></span>';
       * echo '<br />';
       * }
       *
       *
       * echo '</td>';
       *
       * # Level 2
       * echo '<td>';
       * $row=0;
       * $string = nullReplacer($studentDetailList[$i]['Lev2Name']);
       * echo '<span class="rowColor'.($row%2+1).'"><a class="editBtn" data-sid="'.$studentDetailList[$i]['UserID'].'" data-rid="'.$studentDetailList[$i]['RecordID'].'" href="#">'.$string.'</a></span><br />';
       * $row++;
       * $j=$i;
       * while( $studentDetailList[$i]['UserID'] == $studentDetailList[++$j]['UserID']){
       * echo '<span class="rowColor'.($row++%2+1).'"><a class="editBtn" data-sid="'.$studentDetailList[$i]['UserID'].'" data-rid="'.$studentDetailList[$j]['RecordID'].'" href="#">'.$studentDetailList[$j]['Lev2Name'].'</a></span><br />';
       * }
       * echo '</td>';
       *
       *
       * /* Change to remarks
       * # Level 3 & 4
       * echo '<td>';
       * // if(strtotime($studentDetailList[$i]['RecordTime']) !=''){
       * $j=$i;
       * do{
       * $bodyPartsList = array();
       * foreach( (array)$studentBodyPartsList[$studentDetailList[$j]['RecordID']] as $lev3Name=>$lev4NameList){
       * $tempString = '';
       * $tempString .= "{$lev3Name} (";
       * $tempString .= implode(", ", (array)$lev4NameList);
       * $tempString .= ")";
       *
       * $bodyPartsList[] = $tempString;
       * }
       * if( count($bodyPartsList) > 0 ){
       * echo implode(", ", $bodyPartsList);
       * }
       * else{
       * echo '-';
       * }
       * echo '<br />';
       * }while( $studentDetailList[$i]['UserID'] == $studentDetailList[++$j]['UserID']);
       * // }
       * // else{
       * // echo '--';
       * // }
       *
       * echo '</td>';
       *
       *
       * # Remarks
       * echo '<td>';
       * $row=0;
       * echo '<span class="rowColor'.($row++%2+1).'">'.nullReplacer( $string = $studentDetailList[$i]['Remarks']).'</span><br />';
       * $j=$i;
       * while( $studentDetailList[$i]['UserID'] == $studentDetailList[++$j]['UserID']){
       * echo '<span class="rowColor'.($row++%2+1).'">'.$studentDetailList[$j]['Remarks'].'</span><br />';
       * }
       * echo '</td>';
       *
       * # Date Input
       * echo '<td>';
       * $row=0;
       * echo '<span class="rowColor'.($row++%2+1).'">'.nullReplacer( $string = $studentDetailList[$i]['ConfirmedName']).'</span><br />';
       * $j=$i;
       * while( $studentDetailList[$i]['UserID'] == $studentDetailList[++$j]['UserID']){
       * echo '<span class="rowColor'.($row++%2+1).'">'.$studentDetailList[$j]['ConfirmedName'].'</span><br />';
       * }
       * echo '</td>';
       *
       * # Date Modified
       * echo '<td>';
       * $time ='';
       * $row=0;
       * if(strtotime($studentDetailList[$i]['DateModified']) !=''){
       * echo '<span class="rowColor'.($row++%2+1).'">'.nullReplacer( $string = date( 'Y-m-d H:i', strtotime($studentDetailList[$i]['DateModified']))).'</span>';
       *
       * /*if($studentDetailList[$i]['RecordID']!=''){
       * echo '<span class="table_row_tool">';
       * echo '<a class="deleteBtn delete" title="'.$Lang['Btn']['Delete'].'" data-recordID="'.$studentDetailList[$i]['RecordID'].'"></a>';
       * echo '</span>';
       * }
       *
       * }
       * else{
       * echo '<span class="rowColor'.($row++%2+1).'">-</span>';
       * }
       * echo '<br />';
       *
       * $j=$i;
       * while( $studentDetailList[$i]['UserID'] == $studentDetailList[++$j]['UserID']){
       * echo '<span class="rowColor'.($row++%2+1).'">'.date( 'Y-m-d H:i', strtotime($studentDetailList[$j]['DateModified'])).'</span>';
       *
       * /*if($studentDetailList[$j]['RecordID']!=''){
       * echo '<span class="table_row_tool">';
       * echo ' <a class="deleteBtn delete" title="'.$Lang['Btn']['Delete'].'" data-recordID="'.$studentDetailList[$j]['RecordID'].'"></a>';
       * echo '</span>';
       * }
       * echo '<br />';
       *
       *
       * }
       *
       * echo '</td>';
       *
       * echo '<td>';
       * $j=$i;
       * if($studentDetailList[$i]['RecordID']!=''){
       * echo '<span class="table_row_tool">';
       * echo '<a style="margin: -2px 0 -3px 0" class="deleteBtn delete" title="'.$Lang['Btn']['Delete'].'" data-recordID="'.$studentDetailList[$i]['RecordID'].'"></a>';
       * echo '</span>';
       * }
       * while( $studentDetailList[$i]['UserID'] == $studentDetailList[++$j]['UserID']){
       * if($studentDetailList[$j]['RecordID']!=''){
       * echo '<span class="table_row_tool">';
       * echo ' <a style="margin: -2px 0 -3px 0" class="deleteBtn delete" title="'.$Lang['Btn']['Delete'].'" data-recordID="'.$studentDetailList[$j]['RecordID'].'"></a>';
       * echo '</span>';
       * }
       * }
       * echo '</td>';
       *
       * echo '</tr>';
       *
       * }
       */
?>
		<?php if(count($studentDetailList)==0): ?>
		<tr>
				<td colspan="14" class="tableContent" align="center">
					<?php echo $Lang['General']['NoRecordFound']; ?>
			</td>
			</tr>
		<?php endif; ?>
	</tbody>
		<tfoot>
			<tr>
				<td align="center" colspan="14" class="dotline"
					style="text-align: left">
					<div><?=$Lang['medical']['studentLog']['AttendanceStatusExplain']?></div>
					<br />
				<?php if(!libMedicalShowInput::absentAllowedInput('studentLog')){ ?>
					<div><?=$Lang['medical']['studentLog']['AttendanceInputExplain']?></div>
				<?php } ?>
			</td>
			</tr>
		</tfoot>
	</table>
	<input type="hidden" name="t" id="t" value="" /> <input type="hidden"
		name="action" id="action" value="" /> <input type="hidden"
		name="recordID" id="recordID" value="" /> <input type="hidden"
		name="Msg" id="Msg" value="" /> <input type="hidden" name="sid"
		id="sid" value="" />
	<!-- Student UserID for particular student -->
	<input type="hidden" name="date" id="date" value="<?php echo $date ?>" />
	<input type="hidden" id="gender" name="gender"
		value="<?php echo $gender?>" /> <input type="hidden" id="classID"
		name="classID" value="<?php echo $classID?>" /> <input type="hidden"
		id="sleep" name="sleep" value="<?php echo $sleep?>" /> <input
		type="hidden" id="groupID" name="groupID"
		value="<?php echo $groupID?>" />

</form>

<script>
var Id;
$(document).ready(function(){
	$('.newBtn').click(function(){
		var userID = $(this).attr('data-sid');
		var recordID = $(this).attr('data-rid');
		$('#t').val('management.studentLogAddItem');
		$('#sid').val(userID);
		$('#recordID').val(recordID);
		$('#action').val('new');
		$('#Msg').val('');
		$('#studentLogForm').submit();
	});
	$('.deleteBtn').click(function(){
		var isDeleted = confirm('<?php echo $Lang['medical']['report_general']['deleteEvent'] ?>');
		if(!isDeleted){
				return;
		}
		var recordID = $(this).attr('data-recordID');
		$('#t').val('management.studentLog');
		$('#recordID').val(recordID);
		$('#action').val('deleteRecord');
		$('#studentLogForm').submit();
	});
	$('.editBtn').click(function(){
		var userID = $(this).attr('data-sid');
		var recordID = $(this).attr('data-rid');
		$('#t').val('management.studentLogAddItem');
		$('#Msg').val('');
		$('#sid').val(userID);
		$('#recordID').val(recordID);
		$('#studentLogForm').submit();
	});
		
	$('a.fixedSizeThickboxLink').click( function() {
		Id = $(this).attr('data-Id');
		load_dyn_size_thickbox_ip('<?php echo $Lang['medical']['report_general']['attachmentTitle']?>', 'onloadThickBox();', inlineID='', defaultHeight=250, defaultWidth=400);
	});	
});


function onloadThickBox() {
	$('div#TB_ajaxContent').load(
		"?t=management.ajax.getStudentLogAttachmentList",{ 
			'Id': Id
		},
		function(ReturnData) {
		}
	);
}
</script>