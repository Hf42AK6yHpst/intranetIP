<?php
// using: 
/*
 *  2018-06-05 [Cameron]
 *      - show created by person [case #F133828]
 * 
 *  2018-05-28 Cameron
 *      - fix: apply stripslashes to $sleepItem['StatusName'] so that it can escape back slashes
 *  
 * 	2017-09-20 Cameron
 * 		- fix bug: should enclosed by <div> for ModifyByName so that existing row can shift one row below when add new item
 * 		- add flag (presentInputNotShowDefault('sleep')) to control whether to show default items on loading the page for present student. [case #F126577]    
 * 
 * 	2017-07-27 Cameron
 * 		- add argument $presetRowHeight=15 to GetPresetText() [case #P120778]
 * 		- use div instead of span for showing remarks and AttendanceStatusExplain to avoid overlap problem
 * 		- set row size to show all remark context when edit
 * 		- auto adjust the textarea height when edit remark
 *
 * 	2017-07-25 [Cameron]
 * 		- add argument $append=1 to GetPresetText()
 */
$objSleepLev1 = new studentSleepLev1();
$objSleepLev2 = new studentSleepLev2();
$objlibMedical = new libMedical();
$resultList = array();
$studentDetailList = array();

$sleep = $_POST['sleep'];//

//////////////// INPUT CHECKING /////////
if(!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $_POST['date'])){
	echo 'Date Format Error! Please select with format YYYY-MM-DD';
	return;
}


### Generate Legend HTML Start
$legendHTML ='';
$legendHTML .='<ul class="legend" style="list-style-type: none;">';

$sleepDetailLev1 = $objSleepLev1->getActiveStatus(' StatusCode ASC ');
$i=0;
foreach( (array)$sleepDetailLev1 as $sleepItem){
	$legendHTML .='<li><span class="colorBoxStyle" style="background-color:'.$sleepItem['Color'].';">&nbsp;&nbsp;&nbsp;</span>&nbsp;'.stripslashes($sleepItem['StatusName']).'</li>&nbsp;&nbsp;';
	if(++$i%6 == 0){
		$legendHTML .='<br /><br />';
	}
}
if(libMedicalShowInput::attendanceDependence('sleep')){
	$legendHTML .= '<li><span class="colorBoxStyle" style="background-color:lightgray;">&nbsp;&nbsp;&nbsp;</span>&nbsp;'. $Lang['medical']['sleep']['tableHeaderOption']['AttendanceOption']['Absent'] . '</li>';
}
$legendHTML .='</ul>';
### Generate Legend HTML End

### Generate Result Table Start
$resultList = $objlibMedical->getSleepLogList(
				$_POST['date'], $_POST['gender'], 
				$_POST['sleep'], $_POST['classID'] );
//debug_r($resultList);
$groupStudentList = $objMedical->getMedicalGroupStudent($_POST['groupID']);
$groupResult = array();
foreach($resultList as $rs)
{
	if($_POST['groupID'] != ''){
		if(in_array(array('UserID'=>$rs['UserID']),$groupStudentList)){
			$groupResult[$rs['UserID']][] = $rs;
		}
	}else{
		$groupResult[$rs['UserID']][] = $rs;
	}
}
//debug_pr($groupResult);

$index = 1;
$i=1;
foreach( $groupResult as $rs){
	if(!libMedicalShowInput::attendanceDependence('sleep')){
		$rs[0]['Present']=1; // Always present if the school have not buy attendance module
	}
	$indexHTML = '<span>'.$index.'</span>';
	$userID = $rs[0]['UserID'];
	if(($rs[0]['Present'] == 1 && !libMedicalShowInput::presentInputNotShowDefault('sleep')) || $rs[0]['RecordID'] != NULL || (libMedicalShowInput::absentAllowedInput('sleep') && libMedicalShowInput::absentAllowedInputShowDefault('sleep')) ){
		$indexHTML .= '<input name="sleepRecord['.$userID.'][RecordID][]" type="hidden" value="'.$rs[0]['RecordID'].'">';
	}
	$studentDetailList[$i][] = $indexHTML;
	
	if($rs[0]['Present'] == 1 || libMedicalShowInput::absentAllowedInput('sleep')){
		$addBtnHTML = '<span class="table_row_tool">';
		$addBtnHTML .= '<a class="newBtn add" data-sid="'.$userID.'"  title="'.$Lang['Btn']['Add'].'"></a>';
		$addBtnHTML .= '<br /></span>';
		$studentDetailList[$i][] = $addBtnHTML;
	}else{
		$studentDetailList[$i][] = '';
	}
	
	$studentDetailList[$i][] = $rs[0]['ClassName'];
	$studentDetailList[$i][] = $rs[0]['Name'];
	$studentDetailList[$i][] = (($rs[0]['Stay'])?$Lang['medical']['meal']['search']['sleepOption'][1]:$Lang['medical']['meal']['search']['sleepOption'][2]);
		
	if(!libMedicalShowInput::attendanceDependence('sleep')){
		$studentDetailList[$i][] = '<span>--</span>';
	}else if($rs[0]['Present'] == 1){
		$studentDetailList[$i][] = $Lang['medical']['sleep']['tableHeaderOption']['AttendanceOption']['Present'];
	}else{
		$studentDetailList[$i][] = $Lang['medical']['sleep']['tableHeaderOption']['AttendanceOption']['Absent'];
	}
	
	if($rs[0]['Present'] == 1 || libMedicalShowInput::absentAllowedInput('sleep')){
//		$sleepStatusHTML = ''; 
		if($rs[0]['RecordID'] != NULL)
		{
			for($recordI=0,$recordIMax=count($rs);$recordI<$recordIMax;$recordI++)
			{
				if($recordI>0){
					$studentDetailList[$i][] = '<input name="sleepRecord['.$userID.'][RecordID][]" type="hidden" value="'.$rs[$recordI]['RecordID'].'">';
					$studentDetailList[$i][] = '';
					$studentDetailList[$i][] = '';
					$studentDetailList[$i][] = '';
					$studentDetailList[$i][] = ($rs[0]['Present'] == 1)?'':'<span style="display:none">ABSENT</span>';
					$studentDetailList[$i][] = '';
				}
			
				
				$studentDetailList[$i][] = $objSleepLev1->getHTMLSelection($htmlName = 'sleepRecord['.$userID.'][sleepStatus][]',$userSelectedValue = ($rs[$recordI]['SleepID'] !='')?$rs[$recordI]['SleepID']:'','class="sleepStatusSelection"');
				$studentDetailList[$i][] = $objSleepLev2->getHTMLSelection('sleepRecord['.$userID.'][sleepReason][]',($rs[$recordI]['ReasonID'] !='')?$rs[$recordI]['ReasonID']:'',$rs[$recordI]['SleepID'],'class="sleepReasonSelection"');
				
				$frequenceHTML = '<select name="sleepRecord['.$userID.'][frequence][]" id="sleepRecord['.$userID.'][frequence]">';
				for($j=0;$j<$medical_cfg['general']['student_sleep']['frequency'];$j++){
					$selected = '';
					if($rs[$recordI]['Frequency'] == $j+1)
					{
						$selected = ' SELECTED ';
					}
					$frequenceHTML .= '<option value="' . ($j+1) . '"'.$selected.'>' . ($j+1) . '</option>';
				}
				$frequenceHTML .= '</select>';
				$studentDetailList[$i][] = $frequenceHTML;
				
				$remarksHTML = '';
				if($rs[$recordI]['Remarks'] !='')
				{
					$remarksHTML .='<div class="viewArea">'.stripslashes(nl2br($rs[$recordI]['Remarks'])).'<br /></div>';
					$rows = 'rows="'.((int)substr_count($rs[$recordI]['Remarks'],"\n") + 2).'"';
				}
				else {
					$rows = '';
				}
				$remarksHTML .= '<a class="remarksBtn tablelink" ><div>'.$Lang['medical']['meal']['tableContent'][0].'</div></a>';
				$remarksHTML .= '<textarea name="sleepRecord['.$userID.'][remarks][]" id="sleepRecord['.$userID.'][remarks]['.$i.']" '.$rows.' onkeyup="resizeTextArea(this);" style="display:none; overflow:hidden;">';
				$remarksHTML .= $rs[$recordI]['Remarks'];
				$remarksHTML .= '</textarea>';
					
				$remarksHTML .= '<span class="defaultRemarkSpan" style="display:none;">'.GetPresetText("jRemarksArr", $i, "sleepRecord[{$userID}][remarks][$i]", "", "", "", "1", "15").'</span>';
				
				$studentDetailList[$i][] = $remarksHTML;
				
				$studentDetailList[$i][] = '<div>'.(($rs[$recordI]['CreatedBy'] !='')?$rs[$recordI]['CreatedBy']:'-').'</div>';
				$studentDetailList[$i][] = '<div>'.(($rs[$recordI]['ModifyByName'] !='')?$rs[$recordI]['ModifyByName']:'-').'</div>';	// use div instead of span to avoid overlap if it's too long
				$studentDetailList[$i][] = '<span>'.(($rs[$recordI]['DateModified'] !='')?date( 'Y-m-d H:i', strtotime($rs[$recordI]['DateModified'])):'-').'</span>';
				
				$delBtnHTML = '<span class="table_row_tool">';
				$delBtnHTML .= '<a class="deleteBtn delete" title="'.$Lang['Btn']['Delete'].'" data-recordID="'.$rs[$recordI]['RecordID'].'" data-sid="'.$userID.'"  ></a>';
				$delBtnHTML .= '</span>';
				$studentDetailList[$i][] = $delBtnHTML;
				
				++$i;
			}
		}
		else
		{ // No record found, show default record for present student.
			if(($rs[0]['Present'] == 1 && !libMedicalShowInput::presentInputNotShowDefault('sleep')) || (libMedicalShowInput::absentAllowedInput('sleep') && libMedicalShowInput::absentAllowedInputShowDefault('sleep')) ){
				$studentDetailList[$i][] = $objSleepLev1->getHTMLSelection($htmlName = 'sleepRecord['.$userID.'][sleepStatus][]',$userSelectedValue = ($rs[$recordI]['SleepID'] !='')?$rs[$recordI]['SleepID']:'','class="sleepStatusSelection"');
				$studentDetailList[$i][] = $objSleepLev2->getHTMLSelection('sleepRecord['.$userID.'][sleepReason][]',($rs[$recordI]['ReasonID'] !='')?$rs[$recordI]['ReasonID']:'',$rs[$recordI]['SleepID'],'class="sleepReasonSelection"');
				
				$frequenceHTML = '<select name="sleepRecord['.$userID.'][frequence][]" id="sleepRecord['.$userID.'][frequence]">';
				for($j=0;$j<$medical_cfg['general']['student_sleep']['frequency'];$j++){
					$frequenceHTML .= '<option value="' . ($j+1) . '">' . ($j+1) . '</option>';
				}
				$frequenceHTML .= '</select>';
				$studentDetailList[$i][] = $frequenceHTML;
				
				$remarksHTML = '';
				$remarksHTML .= '<a class="remarksBtn tablelink" ><div>'.$Lang['medical']['meal']['tableContent'][0].'</div></a>';
				$remarksHTML .= '<textarea name="sleepRecord['.$userID.'][remarks][]" id="sleepRecord['.$userID.'][remarks]['.$i.']" onkeyup="resizeTextArea(this);" style="display:none; overflow:hidden;">';
				$remarksHTML .= '</textarea>';	
					$remarksHTML .= '<span class="defaultRemarkSpan" style="display:none;">'.GetPresetText("jRemarksArr", $i, "sleepRecord[{$userID}][remarks][$i]", "", "", "", "1", "15").'</span>';
				$studentDetailList[$i][] = $remarksHTML;
					
				$studentDetailList[$i][] = '<span>-</span>';
				$studentDetailList[$i][] = '<span>-</span>';
				$studentDetailList[$i][] = '<span>-</span>';
				
				$delBtnHTML = '<span class="table_row_tool">';
				$delBtnHTML .= '<a class="deleteBtn delete" title="'.$Lang['Btn']['Delete'].'" data-recordID="" data-sid="'.$userID.'"  ></a>';
				$delBtnHTML .= '</span>';
				$studentDetailList[$i][] = $delBtnHTML;
			}else{
				$studentDetailList[$i][] = '<span>-</span>';
				$studentDetailList[$i][] = '<span>-</span>';
				$studentDetailList[$i][] = '<span>-</span>';
				$studentDetailList[$i][] = '<span>-</span>';
				$studentDetailList[$i][] = '<span>-</span>';
				$studentDetailList[$i][] = '<span>-</span>';
				$studentDetailList[$i][] = '<span>-</span>';
				$studentDetailList[$i][] = '';
			}
		}
	}else{ // Absent
		if($rs[0]['RecordID'] == NULL){
			$studentDetailList[$i][] = '<span>-</span>';
			$studentDetailList[$i][] = '<span>-</span>';
			$studentDetailList[$i][] = '<span>-</span>';
			$studentDetailList[$i][] = '<span>-</span>';
			$studentDetailList[$i][] = '<span>-</span>';
			$studentDetailList[$i][] = '<span>-</span>';
			$studentDetailList[$i][] = '<span>-</span>';
			$studentDetailList[$i][] = '';
		}else{
			for($recordI=0,$recordIMax=count($rs);$recordI<$recordIMax;$recordI++){
				if($recordI>0){
					$studentDetailList[$i][] = '<input name="sleepRecord['.$userID.'][RecordID][]" type="hidden" value="'.$rs[$recordI]['RecordID'].'">';
					$studentDetailList[$i][] = '';
					$studentDetailList[$i][] = '';
					$studentDetailList[$i][] = '';
					$studentDetailList[$i][] = '<span style="display:none">ABSENT</span>';
					$studentDetailList[$i][] = '';
				}
				$status = $objSleepLev1->getAllStatus(' recordstatus = 1 and DeletedFlag = 0 and statusID = '.$rs[$recordI]['SleepID']);
				$colorBox = '<font color="red">**</font><span style="white-space:nowrap;"><span class="colorBoxStyle"style="background-color: '.$status[0]['Color'].'">&nbsp;&nbsp;&nbsp;</span>&nbsp;';
				$hiddenInput = '<input name="sleepRecord['.$userID.'][sleepStatus][]" type="hidden" value="'.$rs[$recordI]['SleepID'].'">';
				$studentDetailList[$i][] = $colorBox . $status[0]['StatusName'] . $hiddenInput;
				
				$status = $objSleepLev2->getAllStatus(' recordstatus = 1 and DeletedFlag = 0 and ReasonID = '.$rs[$recordI]['ReasonID']);
				$hiddenInput = '<input name="sleepRecord['.$userID.'][sleepReason][]" type="hidden" value="'.$rs[$recordI]['ReasonID'].'">';
				$studentDetailList[$i][] = $status[0]['ReasonName'] . $hiddenInput;
				
				$hiddenInput = '<input name="sleepRecord['.$userID.'][frequence][]" type="hidden" value="'.$rs[$recordI]['Frequency'].'">';
				$studentDetailList[$i][] = $rs[$recordI]['Frequency'] . $hiddenInput;
				
				$remarksHTML = '';
				if($rs[$recordI]['Remarks'] !=''){
					$remarksHTML .='<div class="viewArea">'.stripslashes(nl2br($rs[$recordI]['Remarks'])).'<br /></div>';
					$rows = 'rows="'.((int)substr_count($rs[$recordI]['Remarks'],"\n") + 2).'"';
				}else{
					$remarksHTML = '-';
					$rows = '';
				}
				$remarksHTML .= '<textarea name="sleepRecord['.$userID.'][remarks][]" id="sleepRecord['.$userID.'][remarks]['.$i.']" '.$rows.' onkeyup="resizeTextArea(this);" style="display:none; overflow:hidden;">';
				$remarksHTML .= $rs[$recordI]['Remarks'];
				$remarksHTML .= '</textarea>';	
				$studentDetailList[$i][] = $remarksHTML;
					
				$studentDetailList[$i][] = '<div>'.($rs[$recordI]['CreatedBy'] !='')?$rs[$recordI]['CreatedBy']:'-'.'</div>';
				$studentDetailList[$i][] = '<div>'.($rs[$recordI]['ModifyByName'] !='')?$rs[$recordI]['ModifyByName']:'-'.'</div>';
				$studentDetailList[$i][] = '<span>'.(($rs[$recordI]['DateModified'] !='')?date( 'Y-m-d H:i', strtotime($rs[$recordI]['DateModified'])):'-').'</span>';
				
				$delBtnHTML = '<span class="table_row_tool">';
				$delBtnHTML .= '<a class="deleteBtn delete" title="'.$Lang['Btn']['Delete'].'" data-recordID="'.$rs[$recordI]['RecordID'].'" data-sid="'.$userID.'"  ></a>';
				$delBtnHTML .= '</span>';
				$studentDetailList[$i][] = $delBtnHTML;
				
				++$i;
			}
		}
	}
		
	++$i;
	++$index;
}
### Generate Result Table End

#### Get default remarks ####
$defaultRemark = $objlibMedical->getDefaultRemarksArray($medical_cfg['general']['module']['sleep']);
//////////////////////////////// Presentation Layer //////////////////////////////////////
echo <<< REMARKS_INIT
	<script>
		var jRemarksCount=$i;
		var jRemarksArr=$defaultRemark;
	</script>
REMARKS_INIT;
?>
<style>
.legend li{
	display: inline;
}
.colorBoxStyle{
	display: inline-block;
	border: 1px solid #c0c0c0;
	width:17px;
	height:17px;
}
.disabledField td{
	background-color: lightgray !important;
}
.tablelink{
	cursor:pointer;
}
#sleepForm table td, #sleepForm table td * {
    vertical-align: top;
}
.defaultRemarkSpan{
	margin-left:5px;
}
</style>
<?php
	echo '<div id="sleepRecordTemplate" style="display:none">'.$objSleepLev1->getHTMLSelection('','','class="sleepStatusSelection"').'</div>';
//debug_r($lev1Rs);
//exit();	
//	echo '<span style="display:none"></span>';

	$lev1Rs = $objSleepLev1->getActiveStatus();
	for($i=0,$iMax=count($lev1Rs);$i<$iMax;$i++){
		echo '<div id="sleepReasonTemplate_'.$lev1Rs[$i]['StatusID'].'" style="display:none">'.$objSleepLev2->getHTMLSelection('','',$lev1Rs[$i]['StatusID'],'class="sleepReasonSelection"').'</div>';
	}
	
	$frequenceHTML = '<div id="sleepFrequenceTemplate" style="display:none"><select>';
	for($j=0;$j<$medical_cfg['general']['student_sleep']['frequency'];$j++){
		$frequenceHTML .= '<option value="' . ($j+1) . '">' . ($j+1) . '</option>';
	}
	$frequenceHTML .= '</select></div>';
	echo $frequenceHTML;
	
	$remarksHTML = '<div id="sleepRemarksTemplate" style="display:none"><a class="remarksBtn tablelink" ><div>'.$Lang['medical']['meal']['tableContent'][0].'</div></a>';
	$remarksHTML .= '<textarea onkeyup="resizeTextArea(this);" style="display:none; overflow:hidden;">';
	$remarksHTML .= $rs['Remarks'];
	$remarksHTML .= '</textarea>';
	$remarksHTML .= '<span class="defaultRemarkSpan" style="display:none"></span>';
	$remarksHTML .= '</div>'; 
	echo $remarksHTML;
	
	
?>

<br />
<?php echo $legendHTML;?>

<form method="post" action="?t=management.sleepSave" name="sleepForm" id="sleepForm">
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<colgroup>
		<col style="width:1%"/>
		<col style="width:1%"/>
		<col style="width:7%"/>
		<col style="width:5%"/>
		<col style="width:6%"/>
		<col style="width:7%"/>
		
		<col style="width:16%"/>
		<col style="width:14%"/>
		<col style="width:5%"/>
		<col style="width:11%"/>
		<col style="width:8%"/>
		<col style="width:8%"/>
		<col style="width:10%"/>
		<col style="width:1%"/>
	</colgroup>
	<thead>
		<tr class="tabletop">
			<?php foreach( $Lang['medical']['sleep']['tableHeader'] as $tableHeader): ?>
				<th class="tabletoplink" nowrap><?php echo $tableHeader; ?></th>
			<?php endforeach; ?>
		</tr>		
	</thead>
	<tbody>
		<?php 
		if(count($studentDetailList)>0){
			foreach( $studentDetailList as $index=>$studentDetail): 
				$disabled ='';
//				if( strlen($studentDetail[12]) <1 ){
				if( $studentDetail[5] == ($Lang['medical']['sleep']['tableHeaderOption']['AttendanceOption']['Absent']) || $studentDetail[4] == '<span style="display:none">ABSENT</span>'){
					$disabled ='class="disabledField"';
				}
		?>
			<tr <?php echo $disabled; ?> class="row_approved">
				<?php foreach( $studentDetail as $studentDetailItem): ?>
					<td valign="top"><?php echo $studentDetailItem; ?></td>
				<?php endforeach; ?>
			</tr>
		<?php endforeach; 
		}else{?>
		<tr><td colspan="<?=count($Lang['medical']['sleep']['tableHeader'])?>" align="center"><?=$Lang['SysMgr']['Homework']['NoRecord']?></td></tr>
		<?php } ?>
	</tbody>
	<tfoot>
		<?php if(!libMedicalShowInput::absentAllowedInput('sleep')){ ?>
		<tr>
			<td align="center" colspan="14" class="dotline" style="text-align:left">
				<div><?=$Lang['medical']['sleep']['AttendanceStatusExplain']?></div>
			</td>
		</tr>
		<?php } ?>
		<tr>
			<td align="center" colspan="14">
				<!--<input type="submit" id="Save" name="Save" class="formbutton_v30 print_hide " value="<?php echo $Lang['medical']['meal']['button']['save']; ?>"> -->
				<input type="hidden" id="deleteReacordList" name="deleteReacordList" value="">
				<input type="button" id="Save" name="Save" class="formbutton_v30 print_hide " value="<?php echo $Lang['medical']['meal']['button']['save']; ?>">
				<input type="button" id="Reset" name="Reset" class="formbutton_v30 print_hide " value="<?php echo $Lang['medical']['meal']['button']['reset']; ?>">
				<!--<input type="button" id="Cancel" name="Cancel" class="formbutton_v30 print_hide " value="<?php echo $Lang['medical']['meal']['button']['cancel']; ?>">-->
			</td>	
		</tr>
	</tfoot>
</table>
<input type="hidden" name="date" value="<?php echo $_POST['date'] ?>" />
</form>
<style>
.viewArea{
	-ms-word-break: break-all;
	-webkit-hyphens: auto;
	-moz-hyphens: auto;
	
	word-break: break-all;
	word-break: break-word;
	hyphens: auto;
}
</style>
<script>
	function updateReasonSelection(){
		$('.sleepStatusSelection').change(function(){
			$(this).prev().css('background-color', $(this).find(":selected").attr('data-color'));

			var nextTd = $(this).closest('td').next();
			var nextSelect = nextTd.find('select');
			var nextSelectName = nextSelect.attr('name');
			var selectedValue = nextSelect.val();
			setElementName($('div[id^="sleepReasonTemplate"]').find('select'),nextSelectName);

			nextTd.empty();
			nextTd.append($('#sleepReasonTemplate_'+$(this).val()).html());
			nextTd.find('select > option[value='+selectedValue+']').attr('selected', 'selected');

			if($(this).hasClass('disableSelection')){
				nextTd.find('select').attr('disabled','disabled');
			}

			setElementName($('div[id^="sleepReasonTemplate"]').find('select'),'');
		});
	}
	updateReasonSelection();
	// Set Default Color	
	$('#sleepForm').find('.sleepStatusSelection').change();
	
	$('.remarksBtn').live('click', function(){
		$(this).hide();
		$(this).prev().hide();
		$(this).next().show();
		$(this).next().next().show();

		$(this).next().focus();
		$(this).next().text($(this).next().val());
		var psconsole = $(this).next();
		psconsole.scrollTop(
			psconsole[0].scrollHeight - psconsole.height()
		);
	});

	$('.deleteBtn').die('click').live('click', function(){
		var userID = $(this).attr('data-sid');
		var recordID = $(this).attr('data-recordID');
		var trDOM = $(this).closest('tr');
		
		var isDeleted = confirm('<?php echo $Lang['medical']['report_general']['deleteEvent'] ?>');
		if(!isDeleted){
			return;
		}
		
		var nextTrDom = trDOM.next();
		
		if(recordID == ''){ // new record (not in DB)
			if(trDOM.find('td:eq(0) > span').length > 0){
				removeRecord(trDOM);
			}else{
				trDOM.remove();
			}
		}else{ // old record (in DB)
			$("#deleteReacordList").val($("#deleteReacordList").val()+','+recordID);
			removeRecord(trDOM);
			/*
			$.post('?t=management.ajax.deleteSleepRecord',{RecordID: recordID}, function(responseResult){
				if(responseResult){ // Delete successful
					msg ='<?php echo $Lang['General']['ReturnMessage']['UpdateSuccess'];?>';
					removeRecord(trDOM);
				}else{
					msg = '<?php echo $Lang['General']['ReturnMessage']['UpdateUnsuccess'];?>';
				}
				Get_Return_Message(msg, 'system_message_box');
				$("html, body").animate({ scrollTop: 0 }, 0);
			});
			*/
		}
		
		trDOM.parent().find('a[data-sid="'+userID+'"]').show();
	});

	function removeRecord(recordTrDOM){
		var nextTrDom = recordTrDOM.next();
		recordTrDOM.find('td:eq(0) > input').remove();
		if(nextTrDom.length > 0){
			if(nextTrDom.find('td:eq(0) > span').length == 0){// Replace this line by next record
				recordTrDOM.find('td:eq(0)').append(nextTrDom.find('td:eq(0) > *'));
				for(var i=6;i<14;i++){
					recordTrDOM.find('td:eq('+i+') > *').remove();
					recordTrDOM.find('td:eq('+i+')').append(nextTrDom.find('td:eq('+i+') > *'));
				}
				nextTrDom.remove();
			}else{
				if(recordTrDOM.find('td:eq(0) > span').length == 0){
					recordTrDOM.remove();
				}else{
					for(var i=6;i<14;i++){
						recordTrDOM.find('td:eq('+i+') > *').remove();
	
						recordTrDOM.find('td:eq('+i+')').html('<span>-</span>');
					}
					recordTrDOM.find('td:eq(13) > *').remove();
				}
			}
		}else{
			if(recordTrDOM.find('td:eq(0) > span').length == 0){
				recordTrDOM.remove();
			}else{
				for(var i=6;i<14;i++){
					recordTrDOM.find('td:eq('+i+') > *').remove();

					recordTrDOM.find('td:eq('+i+')').html('<span>-</span>');
				}
				recordTrDOM.find('td:eq(13) > *').remove();
			}
		}
	}

	$('.newBtn').click(function(){
		var userID = $(this).attr('data-sid');
		var trDOM = $(this).closest('tr');

		if(trDOM.find('td:eq(0) > input').length > 0){
			trDOM.after('<tr class="'+trDOM.attr('class')+'"><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>');
			var trNextDOM = trDOM.next();
			trNextDOM.find('td:eq(0)').append(trDOM.find('td:eq(0) > input'));
			for(i=6;i<=13;i++){
				trNextDOM.find('> td:eq('+i+')').append(trDOM.find('td:eq('+i+') > *'));
//if (i == 10) {				
//				console.log(trNextDOM.find('> td:eq('+i+')').html());
//				console.log(trDOM.find('td:eq('+i+')').html());
//}
			}
		}else{
			for(i=6;i<=13;i++){
				trDOM.find('td:eq('+i+') > *').remove();
			}
		}

		var sleepStatusName = 'sleepRecord['+userID+'][sleepStatus][]';
		var sleepReasonName = 'sleepRecord['+userID+'][sleepReason][]';
		var frequenceName = 'sleepRecord['+userID+'][frequence][]';
		var remarksName = 'sleepRecord['+userID+'][remarks][]';

		setElementName($('#sleepRecordTemplate').find('select'),sleepStatusName);
		setElementName($('div[id^=sleepReasonTemplate]').find('select'),sleepReasonName);
		setElementName($('#sleepFrequenceTemplate').find('select'),frequenceName);
		setElementName($('#sleepRemarksTemplate').find('textarea'),remarksName);
		/*
			It doesn't work with IE7 >_<
			$('#sleepRecordTemplate').find('select').attr('name',sleepStatusName);
			$('#sleepReasonTemplate').find('select').attr('name',sleepReasonName);		
			$('#sleepFrequenceTemplate').find('select').attr('name',frequenceName);		
			$('#sleepRemarksTemplate').find('textarea').attr('name',remarksName);
		*/

		trDOM.find('td:eq(0)').append('<input name="sleepRecord['+userID+'][RecordID][]" type="hidden" value="">');
		trDOM.find('td:eq(6)').append($('#sleepRecordTemplate').html());
		trDOM.find('td:eq(7)').append( $('div[id^=sleepReasonTemplate]:first').html() );
		trDOM.find('td:eq(8)').append($('#sleepFrequenceTemplate').html());
		trDOM.find('td:eq(9)').append($('#sleepRemarksTemplate').html());
		trDOM.find('td:eq(10)').append('<span>-</span>');
		trDOM.find('td:eq(11)').append('<span>-</span>');
		trDOM.find('td:eq(12)').append('<span>-</span>');
		trDOM.find('td:eq(13)').append('<span class="table_row_tool"><a class="deleteBtn delete" title="<?=$Lang["Btn"]["Delete"]?>" data-recordID="" data-sid="'+userID+'"  ></a></span>');
		
		updateReasonSelection();
		$('#sleepForm').find('.sleepStatusSelection').change();
		if(trDOM.parent().find('input[name="sleepRecord['+userID+'][RecordID][]"]').length == <?=$medical_cfg['general']['student_sleep']['maximumRecordCount']?>){
			trDOM.parent().find('a.newBtn[data-sid="'+userID+'"]').hide();
		}

		var remarksID = 'sleepRecord['+userID+'][remarks][' + jRemarksCount + ']';
		trDOM.find('td:eq(9)').find('textarea').attr('id',remarksID);
		$.ajax({
			url : "?t=management.ajax.getRemarksDefaultList",
			type : "GET",
			data : {
				jArrName: 'jRemarksArr',
				btnID: jRemarksCount,
				targetName: remarksID,
				jOtherFunction: '',
				divIDPrefix: ''
			},
			success : function(result) {
				$('textarea[id="' + remarksID + '"]').next().html(result);
			}
		});
		jRemarksCount++;

		// Hide all opened default-remarks-dialog
		$('div > a[onclick*="setDivVisible"]').click();
	});
	
/**
 * Specially for IE7, you know it...
 * Reference: http://stackoverflow.com/questions/2094618/changing-name-attr-of-cloned-input-element-in-jquery-doesnt-work-in-ie6-7
 */
function setElementName(elems, name) {
	if ($.browser.msie === true){
		$(elems).each(function() {
			this.mergeAttributes(document.createElement("<input name='" + name + "'/>"), false);
		});
	} else {
		$(elems).attr('name', name);
	}
}

	$('.defaultRemarkSpan > a').live('click',function(){
		$('iframe#lyrShim').hide();
	});
	
	$('#Reset').click(function(){
		$('#Search').click();
//		$('#sleepForm')[0].reset();
//		$('.sleepStatusSelection').change();
	});
	$('#Save').click(function(){
		/*var isValid = true;
		$('.sleepReasonSelection').each(function(){
			if($(this).val()==""){
				alert('<?=$Lang['medical']['studentLog']['addItemNoLev2']?>');
				isValid = false;
				return false;
			}
		});
		if(!isValid){
			return false;
		}*/
		$.ajax({
			url : "?t=management.sleepSave",
			type : "POST",
			data : $('#sleepForm').serialize(),
			success : function(result) {
				if(result){
					msg ='<?php echo $Lang['General']['ReturnMessage']['UpdateSuccess'];?>';
				}
				else{
					msg = '<?php echo $Lang['General']['ReturnMessage']['UpdateUnsuccess'];?>';
				}
				Get_Return_Message(msg, 'system_message_box');
				$("html, body").animate({ scrollTop: 0 }, 0);
				$('#Search').click();
			}
		});	
		
	});

	/**
	 * Checking whether the record of this student is the last record or not.
	 * @param jqueryObj The jQuery of the <tr> tag of the record.
	 *
	 * @return true if this is the last record.
	 */
	function isLastRecord(jQueryObj){
		return  (jQueryObj.next().length == 0) || (jQueryObj.next().find('td:first > span').length != 0) ;
	}
</script>