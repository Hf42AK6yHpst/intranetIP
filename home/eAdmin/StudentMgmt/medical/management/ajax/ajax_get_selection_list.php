<?
/*
 * 	Log
 * 
 * 	Description: output json format data
 *
 * 	2017-08-30 [Cameron] create this file
 */

$characterset = 'utf-8';

header('Content-Type: text/html; charset='.$characterset);

$ljson = new JSON_obj();

$action = $_GET['action'] ? $_GET['action'] : $_POST['action'];
//$remove_dummy_chars = ($junior_mck) ? false : true;	// whether to remove new line, carriage return, tab and back slash
$data = array();
$ldb = new libdb();
$result = array();

if ((strlen($action) > 14) && substr($action,0,14) == 'get_drug_name_') {
	$sql = "select distinct Drug from MEDICAL_REVISIT_DRUG order by Drug";
	$result = $ldb->returnResultSet($sql);
	
	if(!empty($result)){
		foreach ((array)$result as $row){
//			$val = remove_dummy_chars_for_json($row['Drug']);
			$val = $row['Drug'];
			$data[] = array('value' => $val, 'title' => $val);
		}
	}
}
else {
	switch($action) {
	
		case 'get_Hospital':
			$sql = "select distinct Hospital from MEDICAL_REVISIT order by Hospital";
			$result = $ldb->returnResultSet($sql);
			
			if(!empty($result)){
				foreach ((array)$result as $row){
//					$val = remove_dummy_chars_for_json($row['Hospital']);
					$val = $row['Hospital'];
					$data[] = array('value' => $val, 'title' => $val);
				}
			}
		break;
		
		case 'get_Division':
			$sql = "select distinct Division from MEDICAL_REVISIT order by Division";
			$result = $ldb->returnResultSet($sql);
			
			if(!empty($result)){
				foreach ((array)$result as $row){
//					$val = remove_dummy_chars_for_json($row['Division']);
					$val = $row['Division'];
					$data[] = array('value' => $val, 'title' => $val);
				}
			}
		break;
		
	}
}

echo $ljson->encode($data);

?>