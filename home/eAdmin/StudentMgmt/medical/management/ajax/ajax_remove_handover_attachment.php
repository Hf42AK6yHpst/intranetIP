<?php 
/*
  *     2019-07-08 Cameron
  *         - should also delete physical file
  */
if (!$sys_custom['medical']['Handover'] || !($objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'HANDOVER_MANAGEMENT'))) {
    header("Location: /");
    exit();
}

$result = array();
$path = "$intranet_root/file";

//include_once($PATH_WRT_ROOT.'includes/libdb.php');        // already included in home/eAdmin/StudentMgmt/medical/index.php
$li = new libdb();
$lf = new libfilesystem();
$attachmentID = $_POST['attachmentID'];
if($attachmentID != ''){
    $attachmentInfo = $objMedical->getHandoverAttachment($attachmentID);

    if (count($attachmentInfo)) {
        $attachmentLink = $attachmentInfo[0]['AttachmentLink'];
        $file = $path . "/" . $attachmentLink;
        $result['removeFile'] = $lf->file_remove($file);
    }
exit;
    $table = "MEDICAL_HANDOVER_ATTACHMENT";
    $cond =  "AttachmentID = '$attachmentID'";
    $sql = "DELETE FROM $table WHERE $cond";
    $result['deleteFileFromDB'] = $li->db_db_query($sql);

    if (!in_array(false, $result)) {
        echo true;
    }
    else {
        echo false;
    }
} else {
    echo '';
}
?>