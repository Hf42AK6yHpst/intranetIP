<?php
// using:
/*
 * 2019-08-07 Cameron
 * - fix: should set $CurrentPage to highlight current item (Handover) in left menu
 *
 * 2019-07-08 Cameron
 * - retrieve handover student [case #P150622]
 *
 * 2019-06-13 Philips
 * - create this file
 */
if (!$sys_custom['medical']['Handover'] || !($objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'HANDOVER_MANAGEMENT'))) {
    header("Location: /");
    exit();
}
include_once($PATH_WRT_ROOT.'includes/libdb.php');
$TAGS_OBJ[] = array($Lang['medical']['handover']['record'], "", 0);
$PAGE_NAVIGATION[] = array($Lang['medical']['handover']['record'], '');

$li = new libdb();

$btnSubmit = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], 'button',$ParOnClick="checkForm()", $ParName="btnSubmit");
$btnBack = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], 'button',$ParOnClick="window.location.href='?t=management.handover_list'", $ParName="btnBack");

$recordID = $_GET['RecordID'];
$attachments = array();
if($recordID != ''){
    $record = $objMedical->getHandoverRecord($recordID);
    $record = $record[0];
    $attachments = $objMedical->getHandoverRecordAttachment($recordID);
    $action = 'edit';
    $rt = explode(' ', $record['RecordDate']);
    $rt_Date = $rt[0];
    $rt_Time = explode(':', $rt[1]);
    $rt_Hour = $rt_Time[0];
    $rt_Minute = $rt_Time[1];
} else {
    $action = 'new';
}

$items = $objMedical->getHandoverItemList();
$itemArr = array();
if(sizeof($items) > 0){
    foreach($items as $item){
        $itemArr[] = array($item['ItemID'], $item['ItemName']);
    }
}
# PIC Selection
$PICUser = array();

# student selection
$studentAry = array();

if($recordID != ''){
    $pics = $objMedical->getHandoverRecordPIC($recordID);
//     debug_pr($pics);die();
    foreach($pics as $pic){
        $PICUser[] = array($pic['UserID'], $pic['UserName']);
    }

    $handoverStudentAry = $objMedical->getHandoverStudent($recordID);
    foreach ((array)$handoverStudentAry as $_handoverStudentAry) {
        $studentAry[] = array($_handoverStudentAry['UserID'], $_handoverStudentAry['UserName']);
    }
}

# UserLogin
$UserPICTextarea = '';
$UserPICTextarea .= '<div style="float:left;" class="pic_textarea">';
$UserPICTextarea .= '<textarea style="height:100px;" id="PICAreaSearchTb" name="PICAreaSearchTb"></textarea><span id="textarea_msg"></span><br><br>';
$UserPICTextarea .= $linterface->GET_SMALL_BTN($Lang['AppNotifyMessage']['searchAndInsertPIC'], "button", "");
$UserPICTextarea .= '</div>';

# Attachment Links
$existedAttachment = "";
foreach($attachments as $attach){
    $existedAttachment .= "<div id='exAttachment_{$attach['AttachmentID']}'>";
    $url = $file_path. "/file/" . $attach['AttachmentLink'];
    $filelink = $PATH_WRT_ROOT . '/home/download_attachment.php?target_e=' . getEncryptedText($url);
    $filename = substr($attach['AttachmentLink'], strrpos($attach['AttachmentLink'], '/', -1) + 1);
    $existedAttachment .= "<a href='$filelink'>$filename</a>";
    $existedAttachment .= "&nbsp;<a href='javascript:deleteAttachment(\"{$attach['AttachmentID']}\");'>({$Lang['Btn']['Delete']})</a>";
    $existedAttachment .= "<br />";
    $existedAttachment .= "</div>";
}

$CurrentPage = "ManagementHandover";
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

include_once('templates/handover.tmpl.php');

$linterface->LAYOUT_STOP();
?>