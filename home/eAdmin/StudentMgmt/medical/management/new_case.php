<?php
// using:
/*
 * 2018-03-20 Cameron
 * - add group selection [case #F121742]
 *
 * 2017-06-13 Cameron
 * - create this file
 */
if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = ($popup ? 'EVENT_MANAGEMENT' : 'CASE_MANAGEMENT')) || ! $plugin['medical_module']['discipline']) {
    header("Location: /");
    exit();
}
$CurrentPage = "ManagementCase";
$CurrentPageName = $Lang['medical']['menu']['case'];
$TAGS_OBJ[] = array(
    $Lang['medical']['menu']['case'],
    "",
    0
);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();

$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$PAGE_NAVIGATION[] = array(
    $Lang['medical']['menu']['case'],
    ($popup ? "" : "?t=management.case_list")
);
$PAGE_NAVIGATION[] = array(
    $Lang['Btn']['New'],
    ""
);

$lclass = new libclass();
$classSelection = $lclass->getSelectClass("name='ClassName' id='ClassName' ");
$studentSelection = getSelectByArray(array(), "name='StudentID' id='StudentID'");

$groupSelection = $objMedical->getGroupSelection("GroupID", "", "style=\"display:none\"", "-- " . $Lang['Btn']['Select'] . " --");

$form_action = "?t=management.new_case_save";
if ($NoHeader) {
    include_once ('templates/case.tmpl.php');
} else {
    $linterface->LAYOUT_START($returnMsg);
    include_once ('templates/case.tmpl.php');
    $linterface->LAYOUT_STOP();
}
?>