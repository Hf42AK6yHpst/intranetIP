<?php 
// using:
/*
 * 2019-07-05 Cameron
 * - add handover student [case #P150622]
 * - fix: use transaction to commit/rollback lots of actions
 * - fix: return success/fail result
 *
 * 2019-06-13 Philips
 * - create this file
 */
if (!$sys_custom['medical']['Handover'] || !($objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'HANDOVER_MANAGEMENT'))) {
    header("Location: /");
    exit();
}
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT.'includes/libdb.php');
$li = new libdb();
$lf = new libfilesystem();
global $intranet_root;

$path = "$intranet_root/file";

$table = "MEDICAL_HANDOVER";

$action = $_POST['action'];

$recordDate = $_POST['recordDate'];
$hour = $_POST['recordTime_Hour'];
$minute = $_POST['recordTime_Minute'];

$recordDate .= ' ' . $hour . ':' . $minute . ':00';

$recordItem = $_POST['recordItem'];
$remark = $_POST['remark'];
$recordID = $_POST['recordID'];
$picArr = $_POST['target_PIC'] ? $_POST['target_PIC'] : array(); // With U before UserID
$picArr = array_unique($picArr);
foreach($picArr as $k => $v){
    $picArr[$k] = str_replace('U', '', $v);
}

$studentIDArr = $_POST['target'] ? $_POST['target'] : array(); // With U before UserID
$studentIDArr = array_unique($studentIDArr);
foreach($studentIDArr as $k => $v){
    $studentIDArr[$k] = str_replace('U', '', $v);
}

$result = array();

$li->Start_Trans();

switch($action){
    case 'new':
        // Add New Record First
        $cols = "RecordDate, RecordItemID, Remark, InputBy, DateInput";
        $values = "'$recordDate', '$recordItem', '$remark', '{$_SESSION['UserID']}', NOW()";
        
        $sql = "INSERT INTO $table ($cols) VALUES ($values)";
        $result['addHandover'] = $li->db_db_query($sql);
        $recordID = $li->db_insert_id();
        
        if(sizeof($picArr) > 0){
            $cols = "RecordID, UserID";
            $values = "";
            foreach($picArr as $pic){
                $values .= "('{$recordID}', '{$pic}'),";
            }
            $values = substr($values, 0, -1);
            $table = "MEDICAL_HANDOVER_PIC";
            $sql = "INSERT INTO $table ($cols) VALUES $values";
            $result['addPic'] = $li->db_db_query($sql);
        }

        if(sizeof($studentIDArr) > 0){
            $cols = "RecordID, StudentID";
            $values = "";
            foreach($studentIDArr as $_studentID){
                $values .= "('{$recordID}', '{$_studentID}'),";
            }
            $values = substr($values, 0, -1);
            $table = "MEDICAL_HANDOVER_STUDENT";
            $sql = "INSERT INTO $table ($cols) VALUES $values";
            $result['addHandoverStudent'] = $li->db_db_query($sql);
        }
        
        $DirArr = Array (
            'medical',
            'handoverReport',
            "Report".$recordID,
            $_SESSION['iSession_LoginSessionID']."_".time());
        //check if the directory exists and create it
        $chkdir = $path."/";
        foreach ($DirArr as $dir) {
            $chkdir .= $dir."/";
            if (!is_dir($chkdir))
                {
                    //create dir
                    mkdir($chkdir, 0777);
                }
        }
        $DBPath = array();
        for($i=0; $i < sizeof($_FILES['attachment']); $i++){
            if(is_file($_FILES['attachment']['tmp_name'][$i])){
                //Set file Path in DB
                $DBPath[$i] = $chkdir.$_FILES['attachment']['name'][$i];
                move_uploaded_file($_FILES['attachment']['tmp_name'][$i], $DBPath[$i]);
                $DBPath[$i] = str_replace(($intranet_root."/file/"), "", $DBPath[$i]);
            }
        }
        if (count($DBPath)) {
            $cols = "RecordID, AttachmentLink";
            $table = "MEDICAL_HANDOVER_ATTACHMENT";
            $values = "";
            foreach ($DBPath as $p) {
                $values .= "('$recordID', '$p'),";
            }
            if ($values != "") {
                $values = substr($values, 0, -1);
            }
            $sql = "INSERT INTO $table ($cols) VALUES $values";
            $result['addAttachment'] = $li->db_db_query($sql);
        }

        $successMsg = "AddSuccess";
        $failMsg = "AddUnsuccess";
        
        break;
        
    case 'edit':
        $values = "RecordDate = '{$recordDate}',
                   RecordItemID = '{$recordItem}',
                   Remark = '{$remark}',
                   ModifiedBy = '{$_SESSION['UserID']}',
                   DateModified = NOW()";
        $cond = "RecordID = '{$recordID}'";
        $sql = "UPDATE $table SET $values WHERE $cond";
        $result['updateHandover'] = $li->db_db_query($sql);

        // PIC update
        $oriPICArr = $objMedical->getHandoverRecordPIC($recordID);
        $oriPICArr = Get_Array_By_Key($oriPICArr, 'UserID');
        
        $delArr = array_diff($oriPICArr, $picArr);
        $delArr = array_unique($delArr);
//         debug_pr($delArr);
        $insertArr = array_diff($picArr, $oriPICArr);
        $insertArr = array_unique($insertArr);
//         debug_pr($insertArr);die();
        $table = "MEDICAL_HANDOVER_PIC";
        if(sizeof($delArr) > 0){
            $sql = "DELETE FROM $table WHERE UserID IN ('" . implode("','", $delArr) . "')";
            $result['deletePIC'] = $li->db_db_query($sql);
        }
        if(sizeof($insertArr) > 0){
            $cols = "RecordID, UserID";
            $values = "";
            foreach($insertArr as $pic){
                $values .= "('{$recordID}', '{$pic}'),";
            }
            $values = substr($values, 0, -1);
            $sql = "INSERT INTO $table ($cols) VALUES $values";
            $result['addPIC'] = $li->db_db_query($sql);
        }
        // end of PIC update

        // StudentID update
        $oriStudentIDArr = $objMedical->getHandoverStudent($recordID);
        $oriStudentIDArr = Get_Array_By_Key($oriStudentIDArr, 'UserID');

        $delArr = array_diff($oriStudentIDArr, $studentIDArr);
        $delArr = array_unique($delArr);
//         debug_pr($delArr);
        $insertArr = array_diff($studentIDArr, $oriStudentIDArr);
        $insertArr = array_unique($insertArr);
//         debug_pr($insertArr);die();
        $table = "MEDICAL_HANDOVER_STUDENT";
        if(sizeof($delArr) > 0){
            $sql = "DELETE FROM $table WHERE StudentID IN ('" . implode("','", $delArr) . "')";
            $result['deleteStudentID'] = $li->db_db_query($sql);
        }
        if(sizeof($insertArr) > 0){
            $cols = "RecordID, StudentID";
            $values = "";
            foreach($insertArr as $_studentID){
                $values .= "('{$recordID}', '{$_studentID}'),";
            }
            $values = substr($values, 0, -1);
            $sql = "INSERT INTO $table ($cols) VALUES $values";
            $result['addStudentID'] = $li->db_db_query($sql);
        }
        // end of StudentID update
    
        $DirArr = Array (
            'medical',
            'handoverReport',
            "Report".$recordID,
            $_SESSION['iSession_LoginSessionID']."_".time());
        //check if the directory exists and create it
        $chkdir = $path."/";
        foreach ($DirArr as $dir) {
            $chkdir .= $dir."/";
            if (!is_dir($chkdir))
            {
                //create dir
                mkdir($chkdir, 0777);
            }
        }
        $DBPath = array();
        for($i=0; $i < sizeof($_FILES['attachment']); $i++){
            if(is_file($_FILES['attachment']['tmp_name'][$i])){
                //Set file Path in DB
                $DBPath[$i] = $chkdir.$_FILES['attachment']['name'][$i];
                move_uploaded_file($_FILES['attachment']['tmp_name'][$i], $DBPath[$i]);
                $DBPath[$i] = str_replace(($intranet_root."/file/"), "", $DBPath[$i]);
                
            }
        }
        if (count($DBPath)) {
            $cols = "RecordID, AttachmentLink";
            $table = "MEDICAL_HANDOVER_ATTACHMENT";
            $values = "";
            foreach ($DBPath as $p) {
                $values .= "('$recordID', '$p'),";
            }
            if ($values != "") {
                $values = substr($values, 0, -1);
            }
            $sql = "INSERT INTO $table ($cols) VALUES $values";
            $result['addAttachment'] = $li->db_db_query($sql);
        }

        $successMsg = "UpdateSuccess";
        $failMsg = "UpdateUnsuccess";
        
        break;

    case 'delete':
        $idAry = $_POST['RecordID'];
        if (count($idAry)) {

            $attachmentAry = $objMedical->getHandoverRecordAttachment($idAry);
            foreach((array)$attachmentAry as $_attachmentAry) {
                $_attachmentID = $_attachmentAry['AttachmentID'];
                $_attachmentLink = $_attachmentAry['AttachmentLink'];
                $file = $path . "/" . $_attachmentLink;
                $result['removeFile_'.$_attachmentID] = $lf->file_remove($file);
            }
            $sql = "DELETE FROM MEDICAL_HANDOVER_ATTACHMENT WHERE RecordID IN ('" . implode("','", (array)$idAry) . "')";
            $result['deleteHandoverAttachment'] = $li->db_db_query($sql);

            $sql = "DELETE FROM MEDICAL_HANDOVER_PIC WHERE RecordID IN ('" . implode("','", (array)$idAry) . "')";
            $result['deleteHandoverPIC'] = $li->db_db_query($sql);

            $sql = "DELETE FROM MEDICAL_HANDOVER_STUDENT WHERE RecordID IN ('" . implode("','", (array)$idAry) . "')";
            $result['deleteHandoverStudent'] = $li->db_db_query($sql);

            $table = "MEDICAL_HANDOVER";
            $sql = "UPDATE $table SET isDeleted = '1' WHERE RecordID IN ('" . implode("','", $idAry) . "')";
            $result['deleteHandover'] = $li->db_db_query($sql);
        }

        $successMsg = "DeleteSuccess";
        $failMsg = "DeleteUnsuccess";
        
        break;
}

if (!in_array(false,$result)) {
    $li->Commit_Trans();
    $returnMsgKey = $successMsg;
}
else {
    $li->RollBack_Trans();
    $returnMsgKey = $failMsg;
}

header('location:?t=management.handover_list&returnMsgKey='.$returnMsgKey);
?>