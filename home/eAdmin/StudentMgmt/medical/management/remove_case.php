<?php
// using:
/*
 * 2018-12-27 Cameron
 * - fix sql in checking self record
 * 
 * 2018-03-29 Cameron
 * - cancel IsCase field
 * 
 * 2018-02-27 Cameron
 * - allow to delete self record only for non-super-admin user [case #F135176]
 *
 * 2018-02-01 Cameron
 * - Log delete record for trace purpose
 *
 * 2017-07-03 Cameron
 * - create this file
 */
if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'CASE_MANAGEMENT') || ! $plugin['medical_module']['discipline']) {
    header("Location: /");
    exit();
}

$ldb = new libdb();
$lfs = new libfilesystem();
$result = array();
$dataAry = array();
$isRequiredCheckSelfCase = $objMedical->isRequiredCheckSelfCase();
$returnMsgKey = '';

if (count($CaseID) > 0) {
    $isAllowedToDelete = true;
    if ($isRequiredCheckSelfCase) {
        $sql = "SELECT CaseID FROM MEDICAL_CASE WHERE CaseID IN ('" . implode("','", $CaseID) . "') AND InputBy<>'" . $_SESSION['UserID'] . "'";
        $checkResult = $ldb->returnResultSet($sql);
        if (count($checkResult)) {
            $isAllowedToDelete = false;
            $returnMsgKey = 'DeleteNonSelfRecord';
        }
    }
    
    if ($isAllowedToDelete) {
        
        $ldb->Start_Trans();
        
        // log delete record
        foreach ((array) $CaseID as $caseID) {
            $sql = "SELECT TeacherID FROM MEDICAL_CASE_PIC WHERE CaseID='{$caseID}'";
            $rs = $ldb->returnResultSet($sql);
            if (count($rs)) {
                $teacherIDAry = BuildMultiKeyAssoc($rs, "TeacherID", array(
                    "TeacherID"
                ), $SingleValue = 1);
                $teacherIDs = implode(',', $teacherIDAry);
            } else {
                $teacherIDs = '';
            }
            
            $caseAry = $objMedical->getCases($caseID);
            $case = current($caseAry);
            
            unset($dataAry);
            $dataAry['TableName'] = 'MEDICAL_CASE';
            $dataAry['RecordID'] = $caseID;
            $dataAry['StartDate'] = $case['StartDate'];
            $dataAry['EndDate'] = $case['EndDate'];
            $dataAry['RefID'] = $case['CaseNo'];
            $dataAry['Summary'] = $case['Summary'];
            $dataAry['Detail'] = $case['Detail'];
            $dataAry['StudentID'] = $case['StudentID'];
            $dataAry['OtherRef'] = $teacherIDs;
            $dataAry['LastModifiedBy'] = $_SESSION['UserID'];
            $sql = $objMedical->INSERT2TABLE('MEDICAL_DELETE_LOG', $dataAry, $condition = array(), $run = false);
            $result[] = $ldb->db_db_query($sql);
        }
        
        // delete case
        $sql = "DELETE FROM MEDICAL_CASE WHERE CaseID IN ('" . implode("','", $CaseID) . "')";
        $result[] = $ldb->db_db_query($sql);
        
        $sql = "DELETE FROM MEDICAL_CASE_EVENT WHERE CaseID IN ('" . implode("','", $CaseID) . "')";
        $result[] = $ldb->db_db_query($sql);
        
        $sql = "DELETE FROM MEDICAL_CASE_PIC WHERE CaseID IN ('" . implode("','", $CaseID) . "')";
        $result[] = $ldb->db_db_query($sql);
        
        $caseMinute = $objMedical->getCaseMinute($CaseID);
        
        $sql = "DELETE FROM MEDICAL_CASE_MINUTE WHERE CaseID IN ('" . implode("','", $CaseID) . "')";
        $result[] = $ldb->db_db_query($sql);
        
        for ($i = 0, $iMax = count($caseMinute); $i < $iMax; $i ++) {
            $minuteID = $caseMinute[$i]['MinuteID'];
            $attachment = $objMedical->getCaseMinuteAttachment($minuteID);
            if (count($attachment)) {
                foreach ((array) $attachment as $rs) {
                    $FileID = $rs['FileID'];
                    $FolderPath = $rs['FolderPath'];
                    $FileHashName = $rs['FileHashName'];
                    $file = $file_path . "/file/medical/edisc/" . $FolderPath . "/" . $FileHashName;
                    $lfs = new libfilesystem();
                    $result[] = $lfs->file_remove($file);
                    $result[] = $objMedical->deleteAttachmentByID('MEDICAL_CASE_MINUTE_ATTACHMENT', $FileID);
                }
            }
        }
        
        if (! in_array(false, $result)) {
            $ldb->Commit_Trans();
            $returnMsgKey = 'DeleteSuccess';
        } else {
            $ldb->RollBack_Trans();
            $returnMsgKey = 'DeleteUnsuccess';
        }
    }
}

header("location: ?t=management.case_list&returnMsgKey=" . $returnMsgKey);

?>