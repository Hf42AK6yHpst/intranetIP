<?php
// using:
/*
 * 	2017-10-13 Cameron
 * 		- add greoupSelection filter
 * 
 * 	2017-08-30 Cameron
 * 		- create this file
 */

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='REVISIT_MANAGEMENT') || !$plugin['medical_module']['revisit']){
	header("Location: /");
	exit();
}
$CurrentPage = "ManagementRevisit";
$CurrentPageName = $Lang['medical']['menu']['revisit'];
$TAGS_OBJ[] = array($Lang['medical']['menu']['revisit'], "", 0);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();		

$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$PAGE_NAVIGATION[] = array($Lang['medical']['menu']['revisit'], ($popup ? "" :"?t=management.revisit_list"));
$PAGE_NAVIGATION[] = array($Lang['Btn']['New'], "");

$lclass = new libclass();
$classSelection = $lclass->getSelectClass("name='ClassName' id='ClassName' ");
$groupSelection = $objMedical->getGroupSelection("GroupID","","style=\"display:none\"","-- ".$Lang['Btn']['Select']." --");
$studentSelection = getSelectByArray(array(),"name='StudentID' id='StudentID' onchange='showLastDrug()'");

$rs_revisit = array();
$nrDrugs = 0;
$form_action = "?t=management.new_revisit_save";

$linterface->LAYOUT_START($returnMsg);
include_once('templates/revisit.tmpl.php');
$linterface->LAYOUT_STOP();

?>