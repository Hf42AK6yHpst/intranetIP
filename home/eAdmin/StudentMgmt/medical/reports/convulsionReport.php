<?php
//update by : 
/*
 * 	Log
 * 
 *  Date:   2020-02-14 [Cameron]
 *          - fix: check if $objMedical is defined or not to avoid direct access this page via url
 *          - check report access right to avoid privilege violation
 *      
 * 	Date:	2013-12-30 [Cameron] 
 */
//if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='CONVULSION_REPORT')){
//	header("Location: /");
//	exit();
//}

if (!isset($objMedical) || (!$plugin['medical_module']['studentLog']) || !($objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'STUDENTLOG_REPORT'))) {
    header("Location: /");
    exit();
}

$CurrentPage = "ReportConvulsion";
//$TAGS_OBJ[] = array($Lang['medical']['studentLog']['report_convulsion']['reportName'], "", 0);

$curTab = "report1";
$TAGS_OBJ[] = array($Lang['medical']['menu']['studentLog'], '?t=reports.convulsionReport2', $curTab=='report2');
$TAGS_OBJ[] = array($Lang['medical']['studentLog']['report_convulsion']['reportName'], 'javascript: void(0);', $curTab=='report1');

$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();

$objDB = new libdb();

$r_startDate = trim($_POST['r_startDate']);
$r_endDate = trim($_POST['r_endDate']);

if( $r_startDate =='' || $r_endDate == ''){
	list($r_startDate, $r_endDate) = getPeriodOfAcademicYear($academicYearID = Get_Current_Academic_Year_ID(), $yearTermID='');
	$r_startDate = date('Y-m-d');	// Use today as default
	$r_endDate = date('Y-m-d');
}

//////////////// INPUT CHECKING /////////
if(!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $r_startDate)){
	echo 'Start Report Date Format Error! Please select with format YYYY-MM-DD';
	return;
}

if(!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $r_endDate)){
	echo 'Start Report Date Format Error! Please select with format YYYY-MM-DD';
	return;
}
//////////////// INPUT CHECKING /////////
$objFCM_UI = new form_class_manage_ui;

$valueArray['gender'][1]=$Lang['medical']['report_meal']['search']['genderOption']['all'];
$valueArray['gender'][2]=$Lang['medical']['report_meal']['search']['genderOption']['M'];
$valueArray['gender'][3]=$Lang['medical']['report_meal']['search']['genderOption']['F'];
$gender = getSelectionBox($id="gender", $name="gender", $valueArray['gender'], $valueSelected="", $class="");

$displayMethodHTML = '<input type="checkbox" name="displayEmptyData" value="1" id="displayEmptyData"><label for="displayEmptyData">' . $Lang['medical']['general']['displayMethod']['displayEmptyStudent'] . '</label>';

$startDate = $linterface->GET_DATE_PICKER($Name="r_startDate",$DefaultValue=$startDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum");
$endDate = $linterface->GET_DATE_PICKER($Name="r_endDate",$DefaultValue=$endDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum");


////////////////////////////////// Logical Function Helper /////////////////////////////////////////////////
function getSelectionBox($id, $name, $valueArray, $valueSelected="", $class=""){
	$returnStr = '';
	$returnStr .= "<select class='{$class}' id='{$id}' name='{$name}'>";
	foreach( (array)$valueArray as $key=>$valueItem){
			$selected ='';
			if( $key === $valueSelected){
				$selected ='selected';
			}
			$returnStr .= "<option $selected value='{$key}'>{$valueItem}</option>";
	}
	$returnStr .= "</select>";
	
	return $returnStr;
	
}

	# Handle those cases with fixed options
	
	/////////////////////
	//filter for sleep Start
	/////////////////////
	$sleepFilter = '<select name="sleep2" id="sleep2">';
	$sleepFilter .= '<option value="">'.$medical_cfg['general']['all'].'</option>';

	foreach($medical_cfg['sleep_status'] as $key => $value)
	{
		$sleepFilter .= '<option value= "'.$value['value'].'">'.$value['lang'].'</option>';		
	}
	$sleepFilter .= '</select>'."\n";

	$groupFilter = $objMedical->getGroupSelection($name='groupID');
	/////////////////////
	//filter for sleep End
	/////////////////////
	
	# For Class List
	$classHTML = '';
	$classHTML .= $objFCM_UI->Get_Class_Selection($AcademicYearID = Get_Current_Academic_Year_ID(), $YearID='', $ID_Name='classID[]', $SelectedYearClassID='', $Onchange='', $noFirst=1, $isMultiple=1, $isAll=0, $TeachingOnly=0);
	$classHTML .= '<input type="button"  name="SelectAll_Class" id="SelectAll_Class" class="formbutton_v30 print_hide selectAll" value="'.$Lang['Btn']['SelectAll'].'">';
	$classHTML .= '<span class="form_sep_title">';
	$classHTML .= $Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'];
	$classHTML .= '</span>';
	
	# For Checkbox List Start
	// Select All
	$checkboxHTML  = '<table border=0 cellspacing=0 cellpadding=0>';
	$checkboxHTML .= '<tr><td colspan=2><input type ="checkbox" id="checkAllBodyParts"/><label for="checkAllBodyParts">'.$medical_cfg['general']['report']['search']['itemCheckbox']['all'].'</label></td></tr>'."\n\r";
//	$checkboxHTML .= '<br/>';
	$rsLev3 = $objMedical->getLev3List();	// Note!!! need to parse Lev2ID to indicate convulsion
	
	if (count($rsLev3)>0)
	{
		$i = 0;
		foreach($rsLev3 as $k => $v)
		{
			$lev3ID 	= $v["Lev3ID"];
			$lev3Name 	= $v["Lev3Name"];
				
			$checkboxHTML .= '<tr><td width=80><input type ="checkbox" class="bodyParts" name="bodyParts[]" value="'.$lev3ID.'" id="bodyParts_'.$i.'"/><label for="bodyParts_'.$i.'">'.$lev3Name.'&nbsp;:</label></td>'."\n\r";
			$checkboxHTML .= '<td>';
			$rsLev4 = $objMedical->getLev4List($lev3ID);
			if (count($rsLev4)>0)
			{
				$j = 0;
				foreach($rsLev4 as $k2 => $v2)
				{
					$lev4ID 	= $v2["Lev4ID"];
					$lev4Name 	= $v2["Lev4Name"];
					$checkboxHTML .= '<input type ="checkbox" class="syndrome" name="syndrome[]" value="'.$lev4ID.'" id="syndrome_'.$i.'_'.$j.'"/><label for="syndrome_'.$i.'_'.$j.'">'.$lev4Name.'</label>'."\n\r";
					if(++$j%6==0){
						$checkboxHTML .= '<br />'."\n\r";
					}					
				}
			}
			$checkboxHTML .= '</td></tr>';
			$i++;			
		}		
	}
	$checkboxHTML .= '</table>';
		
	// For Remarks
	$checkboxHTML .= '<br/>';
//	$checkboxHTML .= '<br/>';
	$checkboxHTML .= '<input name="r_withRemarks" type ="checkbox" id="r_withRemarks" value="1"/><label for="r_withRemarks">'.$medical_cfg['general']['report']['search']['itemCheckbox']['remarks'].'</label>'."\n\r";
	

$linterface->LAYOUT_START($Msg);
?>
<style>
.tablelink{
	cursor:pointer;
}
</style>
<form id="reportForm" action="index.php" name="form" target="_blank" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
			<table  border="0" cellspacing="0" cellpadding="5" width="100%">
				<?php /*foreach( (array)$menuOption as $menuItem): ?>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $menuItem['Name']; ?></td>
					<td class="tabletext" width="70%"><?php echo $menuItem['Detail']; ?></td>
				</tr>
				<?php endforeach; */?>
				
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['meal']['searchMenu']['date']; ?></td>
					<td class="tabletext" width="70%"><?php echo $startDate.$Lang['StaffAttendance']['To'].'&nbsp;' .$endDate; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['meal']['searchMenu']['form']; ?></td>
					<td class="tabletext" width="70%"><?php echo $classHTML; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['meal']['searchMenu']['group']; ?></td>
					<td class="tabletext" width="70%"><?php echo $groupFilter; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['meal']['searchMenu']['sleep']; ?></td>
					<td class="tabletext" width="70%"><?php echo $sleepFilter; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['report_meal']['searchMenu']['gender']; ?></td>
					<td class="tabletext" width="70%"><?php echo $gender; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['report_meal']['searchMenu']['studentName']; ?></td>
					<td class="tabletext" width="70%"><span id="studentListArea"></span></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['report_meal']['searchMenu']['item']; ?></td>
					<td class="tabletext" width="70%"><?php echo $checkboxHTML; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['general']['displayMethod']['title']; ?></td>
					<td class="tabletext" width="70%"><?php echo $displayMethodHTML; ?></td>
				</tr>

				<tr>
					<td height="1" class="dotline" colspan="2"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td height="1" align="center" colspan="2"><?= $linterface->GET_ACTION_BTN($Lang['medical']['meal']['button']['search'], 'button',$ParOnClick="", $ParName="r_search")?>&nbsp;</td>
				</tr>
			</table>
			<div id="viewResult">
			</div>
		</td>
	</tr>
</table>
</form>

<script>
$(document).ready(function(){
	$('#checkAllBodyParts').click(function(){
		$('.bodyParts').attr('checked', $(this).attr('checked'));
		checkSyndrome();
	});

	$('.bodyParts').each(function(){
		$(this).click(function(){
			checkSyndrome();
		});
	});

	$('.syndrome').each(function(){
		$(this).click(function(){
			checkBodyParts(this);
		});
	});
			
	$('#classID\\[\\], #gender, #sleep2, #groupID').change(function(){
		$.ajax({
			url : "?t=reports.ajax.getStudentList",
			type : "POST",
			data : $('#reportForm').serialize(),
			success : function(msg) {
				$('#studentListArea').html(msg);
			}
		});	
	});
	$('.selectAll').click(function(){
		$(this).prev().children().attr("selected","selected");
		$('#classID\\[\\]').change();
	});
	
	$('#classID\\[\\]').change();
	$('#r_search').click(function(){
		if($('#studentList\\[\\]').val() == null){
			alert("<?php echo $Lang['medical']['report_bowel']['noStudentSelected']?>");
			return false;
		}
		if($('input[name="bodyParts\\[\\]"]:checked').val() == undefined){
			alert("<?php echo $Lang['medical']['report_bowel']['noReasonSelected']?>");
			return false;
		}
		
		if(!checkDateValid($('#r_startDate').val(),$('#r_endDate').val())){
			alert("<?php echo $Lang['medical']['report_bowel']['wrongDatePeriod']?>");
			return false;
		}
		
		$.ajax({
			//reports/ajax/getConvulsionReport.php
			url : "?t=reports.ajax.getConvulsionReport",
			type : "POST",
			data : $('#reportForm').serialize(),
			success : function(msg) {
				$('#viewResult').html(msg);
			}
		});	
	});

});
function checkDateValid(start,end){
	// .replace() is for IE7 
	var _start = new Date(start.replace(/-/g,'/'));
	var _end = new Date(end.replace(/-/g,'/'));
	return start <= end;
}

function checkSyndrome() {
	var lev3ID = "";
	var lev3Index = 0;
	$('.bodyParts').each(function(){
		lev3ID = this.id;
		lev3Index = lev3ID.substr(10);	// bodyParts_$i
		$('[id^="syndrome_'+lev3Index+'_"]').attr('checked', $(this).attr('checked'));
	});
}

function checkBodyParts(obj) {
	var lev4ID = "";
	var lev4Str = "";
	var lev3Index = 0;
	lev4ID = obj.id;
	lev4Str = lev4ID.substr(9);	// syndrome_$i
	lev3Index = lev4Str.substr(0,lev4Str.indexOf("_"));
	$('[id^="bodyParts_'+lev3Index+'"]').attr('checked', $(obj).attr('checked'));
}

</script>
<?
$linterface->LAYOUT_STOP();
?>