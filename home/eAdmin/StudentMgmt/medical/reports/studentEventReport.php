<?php
/*
 *  Using:
 *  
 * 	Log
 * 	
 *  2020-02-14 Cameron
 *      - fix: check if $objMedical is defined or not to avoid direct access this page via url
 * 
 *  2019-01-02 Cameron
 *      - create this file
 */

if (!isset($objMedical) || ! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'STUDENTEVENT_REPORT') || ! $plugin['medical_module']['discipline']) {
    header("Location: /");
    exit();
}

$CurrentPage = "ReportStudentEvent";
$CurrentPageName = $Lang['medical']['menu']['report_student_event'];
$TAGS_OBJ[] = array($Lang['medical']['menu']['report_student_event'], "", 0);

$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();

//$objDB = new libdb();

$defaultStartDate = date('Y-m-d', strtotime("-1 week"));	// 1 week before
$defaultEndDate = date('Y-m-d');                            // Use today as default

$valueArray['gender'][1]=$Lang['medical']['report_meal']['search']['genderOption']['all'];
$valueArray['gender'][2]=$Lang['medical']['report_meal']['search']['genderOption']['M'];
$valueArray['gender'][3]=$Lang['medical']['report_meal']['search']['genderOption']['F'];

$objFCM_UI = new form_class_manage_ui;
$menuOption = array();

// date range filter
$startDate = $linterface->GET_DATE_PICKER($Name="startDate",$defaultStartDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum");
$endDate = $linterface->GET_DATE_PICKER($Name="endDate",$defaultEndDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum");


# Level 2 Checkbox List
$eventTypeHTML = $objMedical->getAllStudentEventTypes();

$gender = getSelectionBox($id="gender", $name="gender", $valueArray['gender'], $valueSelected="", $class="");

$displayMethodHTML = '';
$displayMethodHTML .= $Lang['medical']['general']['displayMethod']['orderBy']['title'] . '<input type="radio" name="displayOrder" id="displayOrderDesc" value="desc" checked/><label for="displayOrderDesc">' . $Lang['medical']['general']['displayMethod']['orderBy']['desc'] . '</label>';
$displayMethodHTML .= '&nbsp;<input type="radio" name="displayOrder" id="displayOrderAsc" value="asc" /><label for="displayOrderAsc">' . $Lang['medical']['general']['displayMethod']['orderBy']['asc'] . '</label>';
$displayMethodHTML .= '<br /><br />';
//$displayMethodHTML .= '<input type="checkbox" name="displayEmptyData" value="1" id="displayEmptyData"><label for="displayEmptyData">' . $Lang['medical']['general']['displayMethod']['displayEmptyDate'] . '</label>';


////////////////////////////////// Logical Function Helper /////////////////////////////////////////////////

function getSelectionBox($id, $name, $valueArray, $valueSelected="", $class=""){
	$returnStr = '';
	$returnStr .= "<select class='{$class}' id='{$id}' name='{$name}'>";
	foreach( (array)$valueArray as $key=>$valueItem){
			$selected ='';
			if( $key === $valueSelected){
				$selected ='selected';
			}
			$returnStr .= "<option $selected value='{$key}'>{$valueItem}</option>";
	}
	$returnStr .= "</select>";
	
	return $returnStr;
	
}

	# Handle those cases with fixed options
	
	/////////////////////
	//filter for sleep Start
	/////////////////////
	$sleepFilter = '<select name="sleep2" id="sleep2">';
	$sleepFilter .= '<option value="">'.$medical_cfg['general']['all'].'</option>';

	foreach($medical_cfg['sleep_status'] as $key => $value)
	{
		$sleepFilter .= '<option value= "'.$value['value'].'">'.$value['lang'].'</option>';		
	}
	$sleepFilter .= '</select>'."\n";

	$groupFilter = $objMedical->getGroupSelection($name='groupID');
	/////////////////////
	//filter for sleep End
	/////////////////////
	
// 	# For Date Picker
// 	$menuOption[0]['Detail'] = $date;

	# For Class List
	$classHTML = '';
	$classHTML .= $objFCM_UI->Get_Class_Selection($AcademicYearID = Get_Current_Academic_Year_ID(), $YearID='', $ID_Name='classID[]', $SelectedYearClassID='', $Onchange='', $noFirst=1, $isMultiple=1, $isAll=0, $TeachingOnly=0);
	$classHTML .= '<input type="button"  name="SelectAll_Class" id="SelectAll_Class" class="formbutton_v30 print_hide selectAll" value="'.$Lang['Btn']['SelectAll'].'">';
	$classHTML .= '<span class="form_sep_title">';
	$classHTML .= $Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'];
	$classHTML .= '</span>';
	
	
$linterface->LAYOUT_START($Msg);
?>
<style>
.tablelink{
	cursor:pointer;
}
.leve3Header{
	vertical-align: top;
}
</style>
<form id="reportForm" action="index.php" name="form" target="_blank" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
			<table  border="0" cellspacing="0" cellpadding="5" width="100%">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['General']['Date']; ?></td>
					<td class="tabletext" width="70%"><?php echo $startDate.$Lang['General']['To'].'&nbsp;' .$endDate; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['meal']['searchMenu']['form']; ?></td>
					<td class="tabletext" width="70%"><?php echo $classHTML; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['meal']['searchMenu']['group']; ?></td>
					<td class="tabletext" width="70%"><?php echo $groupFilter; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['meal']['searchMenu']['sleep']; ?></td>
					<td class="tabletext" width="70%"><?php echo $sleepFilter; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['report_meal']['searchMenu']['gender']; ?></td>
					<td class="tabletext" width="70%"><?php echo $gender; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['report_meal']['searchMenu']['studentName']; ?></td>
					<td class="tabletext" width="70%"><span id="studentListArea"></span></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['report_meal']['searchMenu']['item']; ?></td>
					<td class="tabletext" width="70%"><?php echo $eventTypeHTML; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['general']['displayMethod']['title']; ?></td>
					<td class="tabletext" width="70%"><?php echo $displayMethodHTML; ?></td>
				</tr>
				
				<tr>
					<td height="1" class="dotline" colspan="2"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td height="1" align="center" colspan="2"><?= $linterface->GET_ACTION_BTN($Lang['medical']['meal']['button']['search'], 'button',$ParOnClick="", $ParName="r_search")?>&nbsp;</td>
				</tr>
			</table>
			<div id="viewResult">
			</div>
		</td>
	</tr>
</table>
</form>
<script>
$(document).ready(function(){
	$('#checkAllStudentEventType').click(function(){
		$('.level1').attr('checked', $(this).attr('checked'));
	});
			
	$('#classID\\[\\], #gender, #sleep2, #groupID').change(function(){
		$.ajax({
			url : "?t=reports.ajax.getStudentList",
			type : "POST",
			data : $('#reportForm').serialize(),
			success : function(msg) {
				$('#studentListArea').html(msg);
			}
		});	
	});
	$('.selectAll').click(function(){
		$(this).prev().children().attr("selected","selected");
		$('#classID\\[\\]').change();
	});
	
	$('#classID\\[\\]').change();
	$('#r_search').click(function(){
		if($('#studentList\\[\\]').val() == null){
			alert("<?php echo $Lang['medical']['report_bowel']['noStudentSelected']?>");
			return false;
		}
		
		if(!checkDateValid($('#startDate').val(),$('#endDate').val())){
			alert("<?php echo $Lang['medical']['report_bowel']['wrongDatePeriod']?>");
			return false;
		}
				
		$.ajax({
			url : "?t=reports.ajax.getStudentEventReport",
			type : "POST",
			data : $('#reportForm').serialize(),
			success : function(msg) {
				$('#viewResult').html(msg);
			}
		});	
	});
});
function checkDateValid(start,end){
	// .replace() is for IE7 
	var _start = new Date(start.replace(/-/g,'/'));
	var _end = new Date(end.replace(/-/g,'/'));
	return start <= end;
}

</script>
<?
$linterface->LAYOUT_STOP();
?>