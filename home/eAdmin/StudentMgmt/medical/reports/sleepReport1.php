<?php
//update by : 
/*
 * 	Log
 * 
 *  Date:   2020-02-14 [Cameron]
 *          - fix: check if $objMedical is defined or not to avoid direct access this page via url
 *          - check report access right to avoid privilege violation
 * 
 *  Date:   2018-05-28 [Cameron]
 *          - fix: apply stripslashes to lev1Name and lev2Name so that it can escape back slashes
 *  
 * 	Date: 	2017-07-27 [Cameron]
 * 			- change $plugin['medical_module']['discipline'] to $sys_custom['medical']['swapBowelAndSleep']
 * 
 * 	Date:	2017-07-17 Cameron
 * 			- hide Bowel Report for $plugin['medical_module']['discipline'] = true. [case #F116540]
 * 
 * 	Date:	2014-01-03 
 */
//if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='CONVULSION_REPORT')){
//	header("Location: /");
//	exit();
//}

if (!isset($objMedical) || (!$plugin['medical_module']['sleep']) || !($objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'SLEEP_REPORT'))) {
    header("Location: /");
    exit();
}

$CurrentPage = "ReportSleep";

$curTab = "report1";
$TAGS_OBJ[] = array($Lang['medical']['report_studentSleep']['report1'], 'javascript: void(0);', $curTab=='report1');
if (!$sys_custom['medical']['swapBowelAndSleep']) {
	$TAGS_OBJ[] = array($Lang['medical']['report_studentSleep']['report2'], '?t=reports.sleepReport2', $curTab=='report2');
	$TAGS_OBJ[] = array($Lang['medical']['report_studentSleep']['report3'], '?t=reports.sleepReport3', $curTab=='report3');
}

$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();

$objDB = new libdb();

$startDate = trim($_POST['startDate']);
$endDate = trim($_POST['endDate']);

//////////////// INPUT CHECKING Start /////////

if(!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $startDate) && $startDate !=''){
	echo 'Report Start Date Format Error! Please select with format YYYY-MM-DD';
	return;
}
if(!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $endDate) && $endDate !=''){
	echo 'Report End Date Format Error! Please select with format YYYY-MM-DD';
	return;
}
//////////////// INPUT CHECKING End /////////

$valueArray['gender'][1]=$Lang['medical']['report_meal']['search']['genderOption']['all'];
$valueArray['gender'][2]=$Lang['medical']['report_meal']['search']['genderOption']['M'];
$valueArray['gender'][3]=$Lang['medical']['report_meal']['search']['genderOption']['F'];

$objFCM_UI = new form_class_manage_ui;
$menuOption = array();

# For Date Picker
if( $startDate =='' || $endDate == ''){
	list($r_startDate, $r_endDate) = getPeriodOfAcademicYear($academicYearID = Get_Current_Academic_Year_ID(), $yearTermID='');
	$startDate = date('Y-m-d');	// Use today as default
	$endDate = date('Y-m-d');
}

$startDate = $linterface->GET_DATE_PICKER($Name="startDate",$DefaultValue=$startDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum");
$endDate = $linterface->GET_DATE_PICKER($Name="endDate",$DefaultValue=$endDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum");

# Level 2 Checkbox List
$recordTypeHMTL = getSleepLogList($objMedical);

//$classList = $objFCM_UI->Get_Class_Selection($AcademicYearID = Get_Current_Academic_Year_ID(), $YearID='', $ID_Name='classID', $SelectedYearClassID='', $Onchange='', $noFirst=0, $isMultiple=0, $isAll=1, $TeachingOnly=0);
//$sleep = getSelectionBox($id="sleep", $name="sleep", $valueArray['sleep'], $valueSelected="", $class="");
$gender = getSelectionBox($id="gender", $name="gender", $valueArray['gender'], $valueSelected="", $class="");

////////////////////////////////// Logical Function Helper /////////////////////////////////////////////////
function getSleepLogList(&$objMedical){
	global $medical_cfg,$Lang;
	
	$objStudentLogLev1 = new studentSleepLev1();
	$rsLev1 = $objStudentLogLev1->getAllStatus($whereCriteria = " DeletedFlag = 0 ", $orderCriteria = 'StatusCode');	// Note!!! need to parse Lev2ID to indicate convulsion

	$checkboxHTML  = '<table border=0 cellspacing=0 cellpadding=0>';
	$checkboxHTML .= '<tr><td colspan=2><input type ="checkbox" id="checkAllsleepID"/><label for="checkAllsleepID">'.$medical_cfg['general']['report']['search']['itemCheckbox']['all'].'</label></td></tr>'."\n\r";
		$i = 0;
		foreach($rsLev1 as $k => $v)
		{
			$lev1ID 	= $v["StatusID"];
			$lev1Name 	= $v["StatusName"];
				
			$objStudentLogLev2 = new studentSleepLev2();
			$rsLev2 = $objStudentLogLev2->getAllStatus($whereCriteria = " DeletedFlag = 0  And StatusID = '{$v['StatusID']}'", $orderCriteria = 'ReasonCode');
				$checkboxHTML .= '<tr><td width="150px" class="leve3Header"><input type ="checkbox" class="level1 sub_item1" name="level1[]" data-Lev1ID ="'.$v["StatusID"].'" value="'.$lev1ID.'" id="level1_'.$v["StatusID"].'"/><label for="level1_'.$v["StatusID"].'">'.stripslashes($lev1Name).'&nbsp;:</label></td>'."\n\r";
				$checkboxHTML .= '<td>';
				
				$j = 0;
				foreach($rsLev2 as $k2 => $v2)
				{
					$lev2ID 	= $v2["ReasonID"];
					$lev2Name 	= $v2["ReasonName"];
					$checkboxHTML .= '<input type ="checkbox" class="level2 sub_item2 level2_'.$v["StatusID"].'" name="reasonID[]" value="'.$lev2ID.'" data-Lev1ID ="'.$v["StatusID"].'" id="level2'.$i.'_'.$j.'"/><label for="level2'.$i.'_'.$j.'">'.stripslashes($lev2Name).'</label>'."\n\r";
					if(++$j%6==0){
						$checkboxHTML .= '<br />'."\n\r";
					}					
				}
				$checkboxHTML .= '</td></tr>';
			$i++;			
		}
	
	// For Remarks
	$checkboxHTML .= '<tr><td>&nbsp;</td></tr>';
	$checkboxHTML .= '<tr><td>';
	$checkboxHTML .= '<input name="r_withRemarks" type ="checkbox" id="r_withRemarks" value="1"/><label for="r_withRemarks">'.$Lang['medical']['report_meal']['search']['itemCheckbox']['remarks'].'</label>'."\n\r";
	$checkboxHTML .= '</td><tr/>';
		
	$checkboxHTML .= '</table>';
		
	return	$checkboxHTML;
}

function getSelectionBox($id, $name, $valueArray, $valueSelected="", $class=""){
	$returnStr = '';
	$returnStr .= "<select class='{$class}' id='{$id}' name='{$name}'>";
	foreach( (array)$valueArray as $key=>$valueItem){
			$selected ='';
			if( $key === $valueSelected){
				$selected ='selected';
			}
			$returnStr .= "<option $selected value='{$key}'>{$valueItem}</option>";
	}
	$returnStr .= "</select>";
	
	return $returnStr;
	
}

	# Handle those cases with fixed options
	
	/////////////////////
	//filter for sleep Start
	/////////////////////
	$sleepFilter = '<select name="sleep2" id="sleep2">';
	$sleepFilter .= '<option value="">'.$medical_cfg['general']['all'].'</option>';

	foreach($medical_cfg['sleep_status'] as $key => $value)
	{
		$sleepFilter .= '<option value= "'.$value['value'].'">'.$value['lang'].'</option>';		
	}
	$sleepFilter .= '</select>'."\n";

	$groupFilter = $objMedical->getGroupSelection($name='groupID');
	/////////////////////
	//filter for sleep End
	/////////////////////

	# For Class List
	$classHTML = '';
	$classHTML .= $objFCM_UI->Get_Class_Selection($AcademicYearID = Get_Current_Academic_Year_ID(), $YearID='', $ID_Name='classID[]', $SelectedYearClassID='', $Onchange='', $noFirst=1, $isMultiple=1, $isAll=0, $TeachingOnly=0);
	$classHTML .= '<input type="button"  name="SelectAll_Class" id="SelectAll_Class" class="formbutton_v30 print_hide selectAll" value="'.$Lang['Btn']['SelectAll'].'">';
	$classHTML .= '<span class="form_sep_title">';
	$classHTML .= $Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'];
	$classHTML .= '</span>';

$home_header_no_EmulateIE7 = true;
$linterface->LAYOUT_START($Msg);
?>
<style>
.tablelink{
	cursor:pointer;
}
.leve3Header{
	vertical-align: top;
}
</style>
<form id="reportForm" action="index.php" name="form" target="_blank" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
			<table  border="0" cellspacing="0" cellpadding="5" width="100%">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['meal']['searchMenu']['date']; ?></td>
					<td class="tabletext" width="70%"><?php echo $startDate.$Lang['StaffAttendance']['To'].'&nbsp;' .$endDate; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['meal']['searchMenu']['form']; ?></td>
					<td class="tabletext" width="70%"><?php echo $classHTML; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['meal']['searchMenu']['group']; ?></td>
					<td class="tabletext" width="70%"><?php echo $groupFilter; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['meal']['searchMenu']['sleep']; ?></td>
					<td class="tabletext" width="70%"><?php echo $sleepFilter; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['report_meal']['searchMenu']['gender']; ?></td>
					<td class="tabletext" width="70%"><?php echo $gender; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['report_meal']['searchMenu']['studentName']; ?></td>
					<td class="tabletext" width="70%"><span id="studentListArea"></span></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['report_meal']['searchMenu']['item']; ?></td>
					<td class="tabletext" width="70%"><?php echo $recordTypeHMTL; ?></td>
				</tr>
				
				<tr>
					<td height="1" class="dotline" colspan="2"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td height="1" align="center" colspan="2"><?= $linterface->GET_ACTION_BTN($Lang['medical']['meal']['button']['search'], 'button',$ParOnClick="", $ParName="r_search")?>&nbsp;</td>
				</tr>
			</table>
			<div id="viewResult">
			</div>
		</td>
	</tr>
</table>
</form>
<script>
$(document).ready(function(){
	$('#checkAllsleepID').click(function(){
		$('.level1').attr('checked', $(this).attr('checked'));
		$('.level2').attr('checked', $(this).attr('checked'));
	});
	$('.sub_item2, .sub_item1').click(function(){
		if(!$(this).attr('checked')){
			$('#checkAllBodyParts').attr('checked', $(this).attr('checked'));
		}
	});
	$('.sub_item1').click(function(){
		var Level1ID = $(this).attr('data-Lev1ID');
		$('.level2_'+Level1ID).attr('checked', $(this).attr('checked'));
	});
	$('.sub_item2').click(function(){
		var Level1ID = $(this).attr('data-Lev1ID');
		$('#level1_'+Level1ID).attr('checked', $(this).attr('checked'));
	});
	/*$('#checkAllsleepID').click(function(){
		$('.sleepID').attr('checked', $(this).attr('checked'));
		$('.reasonID').attr('checked', $(this).attr('checked'));
	});

	$('.sleepID, .reasonID').click(function(){
		if($(this).attr('checked') != true){
			$('#checkAllsleepID').attr('checked', $(this).attr('checked'));
		}
	});*/
			
	$('#classID\\[\\], #gender, #sleep2, #groupID').change(function(){
		$.ajax({
			url : "?t=reports.ajax.getStudentList",
			type : "POST",
			data : $('#reportForm').serialize(),
			success : function(msg) {
				$('#studentListArea').html(msg);
			}
		});	
	});

	
	$('.selectAll').click(function(){
		$(this).prev().children().attr("selected","selected");
		$('#classID\\[\\]').change();
	});
	
	$('#classID\\[\\]').change();
	$('#r_search').click(function(){
		if($('#studentList\\[\\]').val() == null){
			alert("<?php echo $Lang['medical']['report_bowel']['noStudentSelected']?>");
			return false;
		}
		
		if(!checkDateValid($('#startDate').val(),$('#endDate').val())){
			alert("<?php echo $Lang['medical']['report_bowel']['wrongDatePeriod']?>");
			return false;
		}
		
		$.ajax({
			//reports/ajax/getSleepReport1.php
			url : "?t=reports.ajax.getSleepReport1",
			type : "POST",
			data : $('#reportForm').serialize(),
			success : function(msg) {
				$('#viewResult').html(msg);
			}
		});	
	});
});
function checkDateValid(start,end){
	// .replace() is for IE7 
	var _start = new Date(start.replace(/-/g,'/'));
	var _end = new Date(end.replace(/-/g,'/'));
	return start <= end;
}

</script>
<?
$linterface->LAYOUT_STOP();
?>