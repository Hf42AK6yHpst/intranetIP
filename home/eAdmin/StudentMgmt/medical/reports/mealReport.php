<?php
//update by : 
/*
 * 	Log
 * 
 *  2020-02-14 [Cameron]
 *      - fix: check if $objMedical is defined or not to avoid direct access this page via url
 *
 *  2018-05-28 [Cameron]
 *      - fix: apply stripslashes to StatusName so that it can escape back slashes
 *  
 * 	2013-12-17 [Cameron] use today for $r_startDate and $r_endDate by default
 */

if (!isset($objMedical) || (!$plugin['medical_module']['meal']) || !($objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'MEAL_REPORT'))) {
	header("Location: /");
	exit();
}

$CurrentPage = "ReportMeal";

$curTab = "report1";
$TAGS_OBJ[] = array($Lang['medical']['report_meal']['report2'], '?t=reports.mealReport2', $curTab=='report2');
$TAGS_OBJ[] = array($Lang['medical']['report_meal']['report1'], 'javascript: void(0);', $curTab=='report1');


//$TAGS_OBJ[] = array($Lang['medical']['menu']['report_meal'], "", 0);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();

//if( empty($page) ){
//	$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ToDoList'], "index.php?t=report.mealReport", 1);
//	$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['History'], "history.php?t=report.mealReport&page=history", 0);
//}
//else{
//	$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['ToDoList'], "index.php?t=report.mealReport", 0);
//	$TAGS_OBJ[] = array($Lang['SysMgr']['Homework']['History'], "history.php?t=report.mealReport&page=history", 1);
//	
//}
$objDB = new libdb();


$r_startDate = trim($_POST['r_startDate']);
$r_endDate = trim($_POST['r_endDate']);

if( $r_startDate =='' || $r_endDate == ''){
	list($r_startDate, $r_endDate) = getPeriodOfAcademicYear($academicYearID = Get_Current_Academic_Year_ID(), $yearTermID='');
//	$r_startDate = date('Y-m-d', strtotime($r_startDate));
//	$r_endDate = date('Y-m-d', strtotime($r_endDate));
	$r_startDate = date('Y-m-d');	// Use today as default
	$r_endDate = date('Y-m-d');
}

//////////////// INPUT CHECKING /////////
if(!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $r_startDate)){
	echo 'Start Report Date Format Error! Please select with format YYYY-MM-DD';
	return;
}

if(!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $r_endDate)){
	echo 'Start Report Date Format Error! Please select with format YYYY-MM-DD';
	return;
}
//////////////// INPUT CHECKING /////////

$objFCM_UI = new form_class_manage_ui;

$valueArray['gender'][1]=$Lang['medical']['report_meal']['search']['genderOption']['all'];
$valueArray['gender'][2]=$Lang['medical']['report_meal']['search']['genderOption']['M'];
$valueArray['gender'][3]=$Lang['medical']['report_meal']['search']['genderOption']['F'];

$valueArray['timePeriodOption'][1]=$Lang['medical']['report_meal']['search']['timePeriodOption'][0];
$valueArray['timePeriodOption'][2]=$Lang['medical']['report_meal']['search']['timePeriodOption'][1];
$valueArray['timePeriodOption'][3]=$Lang['medical']['report_meal']['search']['timePeriodOption'][2];
$valueArray['timePeriodOption'][4]=$Lang['medical']['report_meal']['search']['timePeriodOption'][3];
////////////////////////////////// Logical Function Helper /////////////////////////////////////////////////

	# For Date
	$startDate = $linterface->GET_DATE_PICKER($Name="r_startDate",$DefaultValue=$startDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum");
	$endDate = $linterface->GET_DATE_PICKER($Name="r_endDate",$DefaultValue=$endDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum");

	# For Class List
	$classHTML = '';
	$classHTML .= $objFCM_UI->Get_Class_Selection($AcademicYearID = Get_Current_Academic_Year_ID(), $YearID='', $ID_Name='classID[]', $SelectedYearClassID='', $Onchange='', $noFirst=1, $isMultiple=1, $isAll=0, $TeachingOnly=0);
	$classHTML .= '<input type="button"  name="SelectAll_Class" id="SelectAll_Class" class="formbutton_v30 print_hide selectAll" value="'.$Lang['Btn']['SelectAll'].'">';
	$classHTML .= '<span class="form_sep_title">';
	$classHTML .= $Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'];
	$classHTML .= '</span>';
	
	# For Group
	$groupFilter = $objMedical->getGroupSelection($name='groupID');
	
	# For Sleep
	$sleepFilter = '<select name="sleep2" id="sleep2">';
	$sleepFilter .= '<option value="">'.$Lang['medical']['General']['All'].'</option>';

	foreach($medical_cfg['sleep_status'] as $key => $value)
	{
		$sleepFilter .= '<option value= "'.$value['value'].'">'.$value['lang'].'</option>';		
	}
	$sleepFilter .= '</select>'."\n";
	
	# For Gender
	$gender = getSelectionBox('gender','gender',$valueArray['gender']);

	# For Time Period
	$timePeriod = getSelectionBox('timePeriod','timePeriod',$valueArray['timePeriodOption']);
	
	# For Report item
	$objStatus = new mealStatus();
	$rsStatusList = array();
	$rsStatusList = $objStatus->getActiveStatus($whereCriteria = 'recordstatus = 1');
	$i=0;
	// Select All
	$checkboxHTML .= '<input type ="checkbox" id="checkAllStatus"/><label for="checkAllStatus">'.$Lang['medical']['report_meal']['search']['itemCheckbox']['all'].'</label>'."\n\r";
	$checkboxHTML .= '<br/>';
	foreach ($rsStatusList as $rsStatus){
		$checkboxHTML .= '<input type ="checkbox" class="statusItem" name="r_status[]" value="'.$rsStatus['StatusID'].'" id="r_status_'.$i.'"/><label for="r_status_'.$i.'">'.stripslashes($rsStatus['StatusName']).'</label>'."\n\r";
		if(++$i%5==0){
			$checkboxHTML .= '<br />'."\n\r";
		}
	}
	// For Remarks
	$checkboxHTML .= '<br/>';
	$checkboxHTML .= '<br/>';
	$checkboxHTML .= '<input name="r_withRemarks" type ="checkbox" id="r_withRemarks" value="1"/><label for="r_withRemarks">'.$Lang['medical']['report_meal']['search']['itemCheckbox']['remarks'].'</label>'."\n\r";
	
	# For Report Type
	$reportTypeHTML = '';
	$reportTypeHTML .= '<input type ="radio" class="reportType" name="reportType" value="r" id="reportType_R" checked/><label for="reportType_R">'.$Lang['medical']['report_meal']['search']['reportTypeSelect']['mealReport'].'</label>'."\n\r";	
	$reportTypeHTML .= '&nbsp;';
	$reportTypeHTML .= '<input type ="radio" class="reportType" name="reportType" value="s" id="reportType_S"/><label for="reportType_S">'.$Lang['medical']['report_meal']['search']['reportTypeSelect']['mealStatistics'].'</label>'."\n\r";

function getSelectionBox($id, $name, $valueArray, $valueSelected="", $class=""){
	$returnStr = '';
	$returnStr .= "<select class='{$class}' id='{$id}' name='{$name}'>";
	foreach( (array)$valueArray as $key=>$valueItem){
			$selected ='';
			if( $key === $valueSelected){
				$selected ='selected';
			}
			$returnStr .= "<option $selected value='{$key}'>{$valueItem}</option>";
	}
	$returnStr .= "</select>";
	
	return $returnStr;
	
}

$linterface->LAYOUT_START($Msg);
?>
<style>
.tablelink{
	cursor:pointer;
}
</style>
<form id="reportForm" action="index.php" name="form" target="_blank" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
			<table  border="0" cellspacing="0" cellpadding="5" width="100%">
				<?php /*foreach( (array)$menuOption as $menuItem): ?>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $menuItem['Name']; ?></td>
					<td class="tabletext" width="70%"><?php echo $menuItem['Detail']; ?></td>
				</tr>
				<?php endforeach; */?>
				
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['meal']['searchMenu']['date']; ?></td>
					<td class="tabletext" width="70%"><?php echo $startDate.$Lang['StaffAttendance']['To'].'&nbsp;' .$endDate; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['meal']['searchMenu']['form']; ?></td>
					<td class="tabletext" width="70%"><?php echo $classHTML; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['meal']['searchMenu']['group']; ?></td>
					<td class="tabletext" width="70%"><?php echo $groupFilter; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['meal']['searchMenu']['sleep']; ?></td>
					<td class="tabletext" width="70%"><?php echo $sleepFilter; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['report_meal']['searchMenu']['gender']; ?></td>
					<td class="tabletext" width="70%"><?php echo $gender; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['report_meal']['searchMenu']['timePeriod']; ?></td>
					<td class="tabletext" width="70%"><?php echo $timePeriod; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['report_meal']['searchMenu']['studentName']; ?></td>
					<td class="tabletext" width="70%"><span id="studentListArea"></span></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['report_meal']['searchMenu']['item']; ?></td>
					<td class="tabletext" width="70%"><?php echo $checkboxHTML; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['report_meal']['searchMenu']['reportType']; ?></td>
					<td class="tabletext" width="70%"><?php echo $reportTypeHTML; ?></td>
				</tr>
				
				
				<tr>
					<td height="1" class="dotline" colspan="2"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td height="1" align="center" colspan="2"><?= $linterface->GET_ACTION_BTN($Lang['medical']['meal']['button']['search'], 'button',$ParOnClick="", $ParName="r_search")?>&nbsp;</td>
				</tr>
			</table>
			<div id="viewResult">
			</div>
		</td>
	</tr>
</table>
</form>
<script>
$(document).ready(function(){
	$('#checkAllStatus').click(function(){
		$('.statusItem').attr('checked', $(this).attr('checked'));
	});
	$('.statusItem').click(function(){
		if($(this).attr('checked')==''){
			$('#checkAllStatus').attr('checked', '');
		}
	});

//	$('#classID\\[\\], #gender, #sleep').change(function(){
	$('#classID\\[\\], #gender, #sleep2, #groupID').change(function(){
//		console.log('Form Changed.');
		$.ajax({
			url : "?t=reports.ajax.getStudentList",
			type : "POST",
			data : $('#reportForm').serialize(),
			success : function(msg) {
				$('#studentListArea').html(msg);
			}
		});	
	});
	$('.selectAll').click(function(){
		$(this).prev().children().attr("selected","selected");
		$('#classID\\[\\]').change();
	});
	
	$('#classID\\[\\]').change();
	$('#r_search').click(function(){
		if($('#studentList\\[\\]').val() == null){
			alert("<?php echo $Lang['medical']['report_bowel']['noStudentSelected']?>");
			return false;
		}
		if($('input[name="r_status\\[\\]"]:checked').val() == undefined){
			alert("<?php echo $Lang['medical']['report_bowel']['noReasonSelected']?>");
			return false;
		}
		
		if(!checkDateValid($('#r_startDate').val(),$('#r_endDate').val())){
			alert("<?php echo $Lang['medical']['report_bowel']['wrongDatePeriod']?>");
			return false;
		}
		
//		console.log('Event search activiated.');
		$.ajax({
			//reports/ajax/getMealReport.php
			url : "?t=reports.ajax.getMealReport",
			type : "POST",
			data : $('#reportForm').serialize(),
			success : function(msg) {
				$('#viewResult').html(msg);
			}
		});	
	});
});

function checkDateValid(start,end){
	// .replace() is for IE7 
	var _start = new Date(start.replace(/-/g,'/'));
	var _end = new Date(end.replace(/-/g,'/'));
	return start <= end;
}
</script>
<?
$linterface->LAYOUT_STOP();
?>