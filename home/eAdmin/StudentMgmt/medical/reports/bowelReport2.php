<?php
//editing by :   
/*
 * 	Log
 * 
 *  Date:   2020-02-14 Cameron
 *          - fix: check if $objMedical is defined or not to avoid direct access this page via url
 *          - check report access right to avoid privilege violation
 *      
 * 	Date:	2014-01-03 
 */
//if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='CONVULSION_REPORT')){
//	header("Location: /");
//	exit();
//}

if (!isset($objMedical) || (!$plugin['medical_module']['bowel']) || !($objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'BOWEL_REPORT'))) {
    header("Location: /");
    exit();
}

$CurrentPage = "ReportBowel";

$curTab = "report2";
$TAGS_OBJ[] = array($Lang['medical']['report_studentBowel']['report2'], 'javascript: void(0);', $curTab=='report2');
$TAGS_OBJ[] = array($Lang['medical']['report_studentBowel']['report1'], '?t=reports.bowelReport1', $curTab=='report1');


$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();


$objDB = new libdb();

$startDate = trim($_POST['startDate']);
$endDate = trim($_POST['endDate']);

//////////////// INPUT CHECKING Start /////////

if(!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $startDate) && $startDate !=''){
	echo 'Report Start Date Format Error! Please select with format YYYY-MM-DD';
	return;
}
if(!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $endDate) && $endDate !=''){
	echo 'Report End Date Format Error! Please select with format YYYY-MM-DD';
	return;
}
//////////////// INPUT CHECKING End /////////

$valueArray['gender'][1]=$Lang['medical']['report_meal']['search']['genderOption']['all'];
$valueArray['gender'][2]=$Lang['medical']['report_meal']['search']['genderOption']['M'];
$valueArray['gender'][3]=$Lang['medical']['report_meal']['search']['genderOption']['F'];


//Report option




$objFCM_UI = new form_class_manage_ui;
$menuOption = array();

# For Date Picker
if( $startDate =='' || $endDate == ''){
	list($r_startDate, $r_endDate) = getPeriodOfAcademicYear($academicYearID = Get_Current_Academic_Year_ID(), $yearTermID='');
	$startDate = date('Y-m-d');	// Use today as default
	$endDate = date('Y-m-d');
}

$startDate = $linterface->GET_DATE_PICKER($Name="startDate",$DefaultValue=$startDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum");
$endDate = $linterface->GET_DATE_PICKER($Name="endDate",$DefaultValue=$endDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum");

# Level 2 Checkbox List
$recordTypeHMTL = getBowelLogList($objMedical);

//$classList = $objFCM_UI->Get_Class_Selection($AcademicYearID = Get_Current_Academic_Year_ID(), $YearID='', $ID_Name='classID', $SelectedYearClassID='', $Onchange='', $noFirst=0, $isMultiple=0, $isAll=1, $TeachingOnly=0);
//$sleep = getSelectionBox($id="sleep", $name="sleep", $valueArray['sleep'], $valueSelected="", $class="");
$gender = getSelectionBox($id="gender", $name="gender", $valueArray['gender'], $valueSelected="", $class="");

////////////////////////////////// Logical Function Helper /////////////////////////////////////////////////
function getBowelLogList(&$objMedical){
	global $medical_cfg,$Lang;
	
	# For Checkbox List Start
	// Select All
	$checkboxHTML  = '<table border=0 cellspacing=0 cellpadding=0 style="width:100%">';
	$checkboxHTML .= '<tr><td colspan=2><input type ="checkbox" id="checkAllsleepID"/><label for="checkAllsleepID">'.$medical_cfg['general']['report']['search']['itemCheckbox']['all'].'</label></td></tr>'."\n\r";
//	$checkboxHTML .= '<br/>';

	$bowelStatus = new bowelStatus();
	$bowelStatusList = $bowelStatus->getAllStatus($whereCriteria = 'DeletedFlag = 0',$orderCriteria = 'StatusCode');
	if (count($bowelStatusList)>0)
	{
		$i = 0;
		$checkboxHTML .= '<tr>';
		foreach($bowelStatusList as $k => $v)
		{
			$statusID 	= $v["StatusID"];
			$statusName = stripslashes($v["StatusName"]);
			
			if($i%3==0){
				$checkboxHTML .= '</tr><tr>';
			}
				
			$checkboxHTML .= '<td style="width:100px"><input type ="checkbox" class="reasonID" name="reasonID[]" value="'.$statusID.'" id="reasonID'.$i.'"/><label for="reasonID'.$i.'">'.$statusName.'&nbsp;</label></td>'."\n\r";
			$checkboxHTML .= '</td>';
			$i++;			
		}
		
		
		$checkboxHTML .='</tr>';		
	}
	
	// For Remarks
	$checkboxHTML .= '<tr><td>&nbsp;</td></tr>';
	$checkboxHTML .= '<tr><td>';
	$checkboxHTML .= '<input name="r_withRemarks" type ="checkbox" id="r_withRemarks" value="1"/><label for="r_withRemarks">'.$Lang['medical']['report_meal']['search']['itemCheckbox']['remarks'].'</label>'."\n\r";
	$checkboxHTML .= '</td><tr/>';
	
	$checkboxHTML .= '</table>';
		
	return	$checkboxHTML;
}

function getSelectionBox($id, $name, $valueArray, $valueSelected="", $class=""){
	$returnStr = '';
	$returnStr .= "<select class='{$class}' id='{$id}' name='{$name}'>";
	foreach( (array)$valueArray as $key=>$valueItem){
			$selected ='';
			if( $key === $valueSelected){
				$selected ='selected';
			}
			$returnStr .= "<option $selected value='{$key}'>{$valueItem}</option>";
	}
	$returnStr .= "</select>";
	
	return $returnStr;
	
}

function Get_Input_RadioButton($id,$name,$value,$Checked=false,$OtherMember="") {
		$Checked = ($Checked)? 'checked="checked"':'';
		$x = '<input type="radio" name="'.$name.'" id="'.$id.'" value="'.$value.'" '.$Checked.' '.$OtherMember.'/>'.$value;
	return $x;
}

	# Handle those cases with fixed options
	
	/////////////////////
	//filter for sleep Start
	/////////////////////
	$sleepFilter = '<select name="sleep2" id="sleep2">';
	$sleepFilter .= '<option value="">'.$medical_cfg['general']['all'].'</option>';

	foreach($medical_cfg['sleep_status'] as $key => $value)
	{
		$sleepFilter .= '<option value= "'.$value['value'].'">'.$value['lang'].'</option>';		
	}
	$sleepFilter .= '</select>'."\n";

	$groupFilter = $objMedical->getGroupSelection($name='groupID');
	/////////////////////
	//filter for sleep End
	/////////////////////

	# For Class List
	$classHTML = '';
	$classHTML .= $objFCM_UI->Get_Class_Selection($AcademicYearID = Get_Current_Academic_Year_ID(), $YearID='', $ID_Name='classID[]', $SelectedYearClassID='', $Onchange='', $noFirst=1, $isMultiple=1, $isAll=0, $TeachingOnly=0);
	$classHTML .= '<input type="button"  name="SelectAll_Class" id="SelectAll_Class" class="formbutton_v30 print_hide selectAll" value="'.$Lang['Btn']['SelectAll'].'">';
	$classHTML .= '<span class="form_sep_title">';
	$classHTML .= $Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'];
	$classHTML .= '</span>';
	
//$home_header_no_EmulateIE7 = true;	
$linterface->LAYOUT_START($Msg);
?>
<style>
.tablelink{
	cursor:pointer;
}
.leve3Header{
	vertical-align: top;
}
</style>
<form id="reportForm" action="index.php" name="form" target="_blank" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
			<table  border="0" cellspacing="0" cellpadding="5" width="1000px">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['meal']['searchMenu']['date']; ?></td>
					<td class="tabletext" width="70%"><?php echo $startDate.$Lang['StaffAttendance']['To'].'&nbsp;' .$endDate; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['meal']['searchMenu']['form']; ?></td>
					<td class="tabletext" width="70%"><?php echo $classHTML; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['meal']['searchMenu']['group']; ?></td>
					<td class="tabletext" width="70%"><?php echo $groupFilter; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['meal']['searchMenu']['sleep']; ?></td>
					<td class="tabletext" width="70%"><?php echo $sleepFilter; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['report_meal']['searchMenu']['gender']; ?></td>
					<td class="tabletext" width="70%"><?php echo $gender; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['report_meal']['searchMenu']['studentName']; ?></td>
					<td class="tabletext" width="70%"><span id="studentListArea"></span></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['report_meal']['searchMenu']['item']; ?></td>
					<td class="tabletext" width="70%"><?php echo $recordTypeHMTL; ?></td>
				</tr>
				
				<tr>
					<td height="1" class="dotline" colspan="2"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td height="1" align="center" colspan="2"><?= $linterface->GET_ACTION_BTN($Lang['medical']['meal']['button']['search'], 'button',$ParOnClick="", $ParName="r_search")?>&nbsp;</td>
				</tr>
			</table>
			<div id="viewResult">
			</div>
		</td>
	</tr>
</table>
</form>
<script>
$(document).ready(function(){
	$('#checkAllsleepID').click(function(){
		$('.sleepID').attr('checked', $(this).attr('checked'));
		$('.reasonID').attr('checked', $(this).attr('checked'));
	});

	$('.sleepID, .reasonID').click(function(){
		if($(this).attr('checked') !='checked'){
			$('#checkAllsleepID').attr('checked', '');
		}
	});
	

	$('#classID\\[\\], #gender, #sleep2, #category1, #category2, #groupID').change(function(){
		$.ajax({
			url : "?t=reports.ajax.getStudentList",
			type : "POST",
			data : $('#reportForm').serialize(),
			success : function(msg) {
				$('#studentListArea').html(msg);
			}
		});	
	});
	$('.selectAll').click(function(){
		$(this).prev().children().attr("selected","selected");
		$('#classID\\[\\]').change();
	});
	
	$('#classID\\[\\]').change();
	$('#r_search').click(function(){
		if($('#studentList\\[\\]').val() == null){
			alert("<?php echo $Lang['medical']['report_bowel']['noStudentSelected']?>");
			return false;
		}
		if($('input[name="reasonID\\[\\]"]:checked').val() == undefined){
			alert("<?php echo $Lang['medical']['report_bowel']['noReasonSelected']?>");
			return false;
		}
		
		if(!checkDateValid($('#startDate').val(),$('#endDate').val())){
			alert("<?php echo $Lang['medical']['report_bowel']['wrongDatePeriod']?>");
			return false;
		}
		
		$.ajax({
			//reports/ajax/getBowelReport2.php
			url : "?t=reports.ajax.getBowelReport2",
			type : "POST",
			data : $('#reportForm').serialize(),
			success : function(msg) {
				$('#viewResult').html(msg);
			}
		});	
	});
	
	function checkDateValid(start,end){
		// .replace() is for IE7 
		var _start = new Date(start.replace(/-/g,'/'));
		var _end = new Date(end.replace(/-/g,'/'));
		return start <= end;
	}
//	var category = null;
//	$("input[name='category']").click(function() {
//	    category = this.value;
//	    if (category =='individual'){
//	     	$("#studentList\\[\\]").removeAttr("Multiple");
//	     	$('#studentList\\[\\]').next().next().hide()
//	     	$('#studentList\\[\\]').next().hide();
//	     	
//	    }else
//	    {
//			$('#studentList\\[\\]').attr("Multiple"," ");	  	
//			$('#studentList\\[\\]').next().next().show()
//			$('#studentList\\[\\]').next().show()
//	    }
//	});	

});

</script>
<?
$linterface->LAYOUT_STOP();
?>