<?php
// using:
/*
 * 2019-07-08 Cameron
 * - break item list into 6 items per row
 * - add filter by PIC [case #P150622]
 * - set $CurrentPage to hightlight current page
 *
 * 2019-06-13 Philips
 * - create this file
 */
if (!$sys_custom['medical']['Handover'] || !($objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'HANDOVER_MANAGEMENT'))) {
    header("Location: /");
    exit();
}
include_once($PATH_WRT_ROOT.'includes/libdb.php');
$CurrentPage = "ReportHandover";
$TAGS_OBJ[] = array($Lang['medical']['menu']['report_handover'], "", 0);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();
$objDB = new libdb();
$r_startDate = trim($_POST['r_startDate']);
$r_endDate = trim($_POST['r_endDate']);

if( $r_startDate =='' || $r_endDate == ''){
    list($r_startDate, $r_endDate) = getPeriodOfAcademicYear($academicYearID = Get_Current_Academic_Year_ID(), $yearTermID='');
    $r_startDate = date('Y-m-d');	// Use today as default
    $r_endDate = date('Y-m-d');
}

$startDate = $linterface->GET_DATE_PICKER($Name="r_startDate",$DefaultValue=$startDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum");
$endDate = $linterface->GET_DATE_PICKER($Name="r_endDate",$DefaultValue=$endDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum");

$checkboxHTML .= '<input type ="checkbox" id="checkAllItem"/><label for="checkAllItem">'.$Lang['medical']['General']['All'].'</label>'."\n\r";
$itemAry = $objMedical->getHandoverItemList();
if(sizeof($itemAry) > 0){
    $checkboxHTML .= "<br />";
    $i = 0;
    foreach($itemAry as $item){
        $checkboxHTML .= "<label><input type='checkbox' id='Item_{$item['ItemID']}' class='handoverItem' name='Item[{$item['ItemID']}]' value='1' /> {$item['ItemName']}</label>";
        if (++$i%6 == 0) {
            $checkboxHTML .= "<br />";
        }
    }
}

$handoverPicHTML .= '<input type ="checkbox" id="checkAllPic"/><label for="checkAllPic">'.$Lang['medical']['General']['All'].'</label>'."\n\r";
$picAry = $objMedical->getHandoverPicList();
if(sizeof($picAry) > 0){
    $handoverPicHTML .= "<br />";
    $i = 0;
    foreach($picAry as $_picAry){
        $handoverPicHTML .= "<label><input type='checkbox' id='Pic_{$_picAry['UserID']}' class='handoverPic' name='Pic[{$_picAry['UserID']}]' value='1' /> {$_picAry['UserName']}</label>";
        if (++$i%6 == 0) {
            $handoverPicHTML .= "<br />";
        }
    }
}
    
$displayMethodHTML = '';
$displayMethodHTML .= $Lang['medical']['general']['displayMethod']['orderBy']['title'] . '<input type="radio" name="displayOrder" id="displayOrderDesc" value="desc" checked/><label for="displayOrderDesc">' . $Lang['medical']['general']['displayMethod']['orderBy']['desc'] . '</label>';
$displayMethodHTML .= '&nbsp;<input type="radio" name="displayOrder" id="displayOrderAsc" value="asc" /><label for="displayOrderAsc">' . $Lang['medical']['general']['displayMethod']['orderBy']['asc'] . '</label>';

// debug_pr($itemAry);die();
$linterface->LAYOUT_START($Msg);
?>
<form id="reportForm" action="?t=reports.ajax.getHandoverReport" name="form" target="_blank" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
			<table  border="0" cellspacing="0" cellpadding="5" width="100%">				
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['meal']['searchMenu']['date']; ?></td>
					<td class="tabletext" width="70%"><?php echo $startDate.$Lang['StaffAttendance']['To'].'&nbsp;' .$endDate; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['handover']['item']; ?></td>
					<td class="tabletext" width="70%"><?php echo $checkboxHTML; ?></td>
				</tr>

                <tr>
                    <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['handover']['pic']; ?></td>
                    <td class="tabletext" width="70%"><?php echo $handoverPicHTML; ?></td>
                </tr>

				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['general']['displayMethod']['title']; ?></td>
					<td class="tabletext" width="70%"><?php echo $displayMethodHTML; ?></td>
				</tr>
				<tr>
					<td height="1" class="dotline" colspan="2"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td height="1" align="center" colspan="2"><?= $linterface->GET_ACTION_BTN($Lang['Btn']['View'], 'button',$ParOnClick="getReport()", $ParName="r_search")?>&nbsp;</td>
				</tr>
			</table>
			<input type="hidden" name="action" id="action" value="" />
			<div id="viewResult">
			</div>
		</td>
	</tr>
</table>
</form>
<script type="text/javascript">
$(document).ready(function(){
	$('#checkAllItem').click(function(){
		$('.handoverItem').attr('checked', $(this).attr('checked'));
	});
	$('.handoverItem').click(function(){
		if($(this).attr('checked')==''){
			$('#checkAllItem').attr('checked', '');
		}
	});

    $('#checkAllPic').click(function(){
        $('.handoverPic').attr('checked', $(this).attr('checked'));
    });
    $('.handoverPic').click(function(){
        if($(this).attr('checked')==''){
            $('#checkAllPic').attr('checked', '');
        }
    });

    // default check all pic
    $('#checkAllPic').attr('checked', true);
    $('.handoverPic').attr('checked', true);
});
function getReport(){
	var error = 0;
	$('#action').val('');
	if($('.handoverItem:checked').length < 1){
		alert('<?php echo $Lang['medical']['revisit']['SelectAtLeastOneItem'];?>');
		error++;
	}
	if(error == 0){
		$.post('?t=reports.ajax.getHandoverReport', $('#reportForm').serialize(), function(data){
				$('#viewResult').html(data);
		});
	}
}
function printReport(){
	$('#action').val('print');
	$('#reportForm').submit();
}
</script>
<?php
$linterface->LAYOUT_STOP();
?>