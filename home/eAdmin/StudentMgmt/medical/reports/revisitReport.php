<?php
//update by : 
/*
 * 	Log 
 */
if (!$sys_custom['medical']['MedicalReport'] || !$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'REVISIT_REPORT') || ! $plugin['medical_module']['revisit']) {
    header("Location: /");
    exit();
}
include_once($PATH_WRT_ROOT. 'includes/libclass.php');
$lclass = new libclass();
$CurrentPage = "ReportRevisit";
//$TAGS_OBJ[] = array($Lang['medical']['studentLog']['report_convulsion']['reportName'], "", 0);
$TAGS_OBJ[] = array(
    $Lang['medical']['menu']['report_revisit'],
    "",
    0
);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();

$objDB = new libdb();

$r_startDate = trim($_POST['r_startDate']);
$r_endDate = trim($_POST['r_endDate']);

if( $r_startDate =='' || $r_endDate == ''){
	list($r_startDate, $r_endDate) = getPeriodOfAcademicYear($academicYearID = Get_Current_Academic_Year_ID(), $yearTermID='');
	$r_startDate = date('Y-m-d');	// Use today as default
	$r_endDate = date('Y-m-d');
}

//////////////// INPUT CHECKING /////////
if(!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $r_startDate)){
	echo 'Start Report Date Format Error! Please select with format YYYY-MM-DD';
	return;
}

if(!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $r_endDate)){
	echo 'Start Report Date Format Error! Please select with format YYYY-MM-DD';
	return;
}
//////////////// INPUT CHECKING /////////
$objFCM_UI = new form_class_manage_ui;

$startDate = $linterface->GET_DATE_PICKER($Name="r_startDate",$DefaultValue=$startDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum");
$endDate = $linterface->GET_DATE_PICKER($Name="r_endDate",$DefaultValue=$endDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum");

$valueArray['gender'][1]=$Lang['medical']['report_meal']['search']['genderOption']['all'];
$valueArray['gender'][2]=$Lang['medical']['report_meal']['search']['genderOption']['M'];
$valueArray['gender'][3]=$Lang['medical']['report_meal']['search']['genderOption']['F'];


$yearFilter = getSelectAcademicYear("AcademicYearID", '', 0, 0, $AcademicYearID);

# For Class List
$classHTML = '';
// $classHTML .= $objFCM_UI->Get_Class_Selection($AcademicYearID = Get_Current_Academic_Year_ID(), $YearID='', $ID_Name='classID[]', $SelectedYearClassID='', $Onchange='', $noFirst=1, $isMultiple=1, $isAll=0, $TeachingOnly=0);
$classHTML .= $lclass->getSelectClassID("name='classID[]' id='classID[]' onChange='' multiple size='10' ", "", "");
$classHTML .= '<input type="button"  name="SelectAll_Class" id="SelectAll_Class" class="formbutton_v30 print_hide selectAll" value="'.$Lang['Btn']['SelectAll'].'">';
$classHTML .= '<span class="form_sep_title">';
$classHTML .= $Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'];
$classHTML .= '</span>';
$groupFilter = $objMedical->getGroupSelection($name='groupID');

# For Sleep
$sleepFilter = '<select name="sleep2" id="sleep2">';
$sleepFilter .= '<option value="">'.$Lang['medical']['General']['All'].'</option>';

foreach($medical_cfg['sleep_status'] as $key => $value)
{
    $sleepFilter .= '<option value= "'.$value['value'].'">'.$value['lang'].'</option>';
}
$sleepFilter .= '</select>'."\n";

# For Gender
$gender = getSelectionBox('gender','gender',$valueArray['gender']);

// Select All
$checkboxHTML .= '<input type ="checkbox" id="checkAllStatus"/><label for="checkAllStatus">'.$Lang['medical']['General']['All'].'</label>'."\n\r";
$checkboxHTML .= '<br/>';
$checkboxHTML .= '<input type="checkbox" id="Status_Normal" name="Status[Normal]" class="statusItem" value="1" /><label for="Status_Normal">' . $Lang['medical']['revisit']['StatusNormal'] . '</label>';
$checkboxHTML .= '<input type="checkbox" id="Status_AccidentInjure" name="Status[AccidentInjure]" class="statusItem" value="1" /><label for="Status_AccidentInjure">'. $Lang['medical']['revisit']['AccidentInjure'] . '</label>';
$checkboxHTML .= '<input type="checkbox" id="Status_DischargedAdmittion" name="Status[DischargedAdmittion]" class="statusItem" value="1" /><label for="Status_DischargedAdmittion">'. $Lang['medical']['revisit']['DischargedAdmittion'] . '</label>';

$displayMethodHTML = '';
$displayMethodHTML .= $Lang['medical']['general']['displayMethod']['orderBy']['title'] . '<input type="radio" name="displayOrder" id="displayOrderDesc" value="desc" checked/><label for="displayOrderDesc">' . $Lang['medical']['general']['displayMethod']['orderBy']['desc'] . '</label>';
$displayMethodHTML .= '&nbsp;<input type="radio" name="displayOrder" id="displayOrderAsc" value="asc" /><label for="displayOrderAsc">' . $Lang['medical']['general']['displayMethod']['orderBy']['asc'] . '</label>';

////////////////////////////////// Logical Function Helper /////////////////////////////////////////////////
function getSelectionBox($id, $name, $valueArray, $valueSelected="", $class=""){
	$returnStr = '';
	$returnStr .= "<select class='{$class}' id='{$id}' name='{$name}'>";
	foreach( (array)$valueArray as $key=>$valueItem){
			$selected ='';
			if( $key === $valueSelected){
				$selected ='selected';
			}
			$returnStr .= "<option $selected value='{$key}'>{$valueItem}</option>";
	}
	$returnStr .= "</select>";
	
	return $returnStr;
	
}

$linterface->LAYOUT_START($Msg);
?>
<style>
.tablelink{
	cursor:pointer;
}
</style>
<form id="reportForm" action="?t=reports.ajax.getRevisitReport" name="form" target="_blank" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
			<table  border="0" cellspacing="0" cellpadding="5" width="100%">				
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['revisit']['VisitDate']; ?></td>
					<td class="tabletext" width="70%"><?php echo $startDate.$Lang['StaffAttendance']['To'].'&nbsp;' .$endDate; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['meal']['searchMenu']['form']; ?></td>
					<td class="tabletext" width="70%"><?php echo $classHTML; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['meal']['searchMenu']['group']; ?></td>
					<td class="tabletext" width="70%"><?php echo $groupFilter; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['meal']['searchMenu']['sleep']; ?></td>
					<td class="tabletext" width="70%"><?php echo $sleepFilter; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['report_meal']['searchMenu']['gender']; ?></td>
					<td class="tabletext" width="70%"><?php echo $gender; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['report_meal']['searchMenu']['studentName']; ?></td>
					<td class="tabletext" width="70%"><span id="studentListArea"></span></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['report_meal']['searchMenu']['item']; ?></td>
					<td class="tabletext" width="70%"><?php echo $checkboxHTML; ?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['general']['displayMethod']['title']; ?></td>
					<td class="tabletext" width="70%"><?php echo $displayMethodHTML; ?></td>
				</tr>
				<tr>
					<td height="1" class="dotline" colspan="2"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td height="1" align="center" colspan="2"><?= $linterface->GET_ACTION_BTN($Lang['Btn']['View'], 'button',$ParOnClick="getReport()", $ParName="r_search")?>&nbsp;</td>
				</tr>
			</table>
			<input type="hidden" name="action" id="action" value="" />
			<div id="viewResult">
			</div>
		</td>
	</tr>
</table>
</form>

<script>
function refreshStudentList(){
	$.ajax({
		url : "?t=reports.ajax.getStudentList",
		type : "POST",
		data : $('#reportForm').serialize(),
		success : function(msg) {
			$('#studentListArea').html(msg);
		}
	});
}
$(document).ready(function(){
	$('#classID\\[\\]').change(function(){
		refreshStudentList();
	});
	$('#groupID').change(function(){
		refreshStudentList();
	});
	$('#sleep2').change(function(){
		refreshStudentList();
	});
	$('#gender').change(function(){
		refreshStudentList();
	});
	$('.selectAll').click(function(){
		$(this).prev().children().children().attr("selected","selected");
		$('#classID\\[\\]').change();
	});
	
	$('#classID\\[\\]').change();
	$('#checkAllStatus').click(function(){
		$('.statusItem').attr('checked', $(this).attr('checked'));
	});
	$('.statusItem').click(function(){
		if($(this).attr('checked')==''){
			$('#checkAllStatus').attr('checked', '');
		}
	});
});
function printReport(){
	$('#action').val('print');
	$('#reportForm').submit();
}
function getReport(){
	var error = 0;
	$('#action').val('');
	if($('.statusItem:checked').length < 1){
		alert('<?php echo $Lang['medical']['revisit']['SelectAtLeastOneItem'];?>');
		error++;
	}
	if($('#studentList\\[\\]').val() == null){
		alert("<?php echo $Lang['medical']['report_bowel']['noStudentSelected']?>");
		error++;
	}
	if(error == 0){
		$.post('?t=reports.ajax.getRevisitReport', $('#reportForm').serialize(), function(data){
				$('#viewResult').html(data);
		});
	}
}
</script>
<?
$linterface->LAYOUT_STOP();
?>