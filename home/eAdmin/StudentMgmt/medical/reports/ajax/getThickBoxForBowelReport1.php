<?php

//using: Pun

list($startTime, $endTime) = explode('|', $period);
$studentMedicalInfo = $objMedical->getBowelReportData($reasonIDThickBox,$studentList,$startDate,$endDate, $sleep='', $report = 1, $fromThickBox = true, $startTime.':00', $endTime.':00');

$studentInfo = $objMedical->getStudentList($studentList, $sleep2, $genderNum='', $showFullDetail = true);

$bowelStatusObj = new bowelStatus($reasonIDThickBox);

//$bowelStatusName = $bowelStatusObj->getStatusName();

//debug_r($studentMedicalInfo);
//debug_r($StudentList);

$dispGender = "";
switch($_POST["gender"])
{
	case 2:
		$dispGender = $Lang['medical']['report_general']['search']['genderOption']['M'];
		break;
	case 3:
		$dispGender = $Lang['medical']['report_general']['search']['genderOption']['F'];
		break;		
}
	
if ($_POST["sleep2"] == "0")
{
	$dispSleepType = $Lang['medical']['report_meal']['sleepOption']['StayOut'];
}
else if ($_POST["sleep2"] == "1")
{
	$dispSleepType = $Lang['medical']['report_meal']['sleepOption']['StayIn'];
}
else
{
	$dispSleepType = "";	
}
 
$space = ($intranet_session_language == "en" ) ? "&nbsp;" : "";
$disStudentType = $dispGender . $space . $dispSleepType;

//$disStudentType = $objUser->UserNameLang();
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");

?>

<br />
<div style="width:100%;border-bottom: 2px black solid;font-weight:bold;">
	<?php echo stripslashes($bowelStatusObj->getStatusName()).$Lang['SysMgr']['Homework']['Reports'] ?> : <?php echo $disStudentType ?> ( <?php echo $startDate.' '.$Lang['StaffAttendance']['To'].' '.$endDate ?> )
</div>
<br />
<table class="common_table_list_v30">
	<colgroup>
		<col style="width: 20%"/>
		<col style="width: 20%"/>
		<col style="width: 20%"/>
<!--		<col style="width: 10%"/> -->
		<col style="width: 30%"/>
	</colgroup>
	<thread>
		<th><?php echo $Lang['medical']['meal']['tableHeader']['StudentName']?></th>
		<th><?php echo $Lang['medical']['report_meal']['searchMenu']['date']?></th>
		<th><?php echo $Lang['medical']['studentLog']['time']?></th>
<!--		<th><?php echo $Lang['medical']['sleep']['tableHeader']['Frequency']?></th> -->
		<th><?php echo $Lang['medical']['sleep']['tableHeader']['Remarks']?></th>
	</thread>
	<?php if(count($studentList)>0): ?>

		<?php 
		foreach($studentInfo as $student):
		$userID = $student['UserID']; 

		if(empty($studentMedicalInfo[$userID])){
			continue; // Only display students that have record.
		}
		
		echo '<tr>';
			
//			foreach( (array)$studentMedicalInfo[$userID] as $eventList){
//				$rowspanUser += count($eventList);
//			}
//			if($rowspanUser <=0){
//				$rowspanUser = 1;
//			}

			echo "<td >{$student['Name']}</td>";
			$skipName = false;
			
			if($studentMedicalInfo[$userID]>0){
				ksort($studentMedicalInfo[$userID]);
				foreach( (array)$studentMedicalInfo[$userID] as $date => $eventList){
					
					
					if($skipName){
						echo '<tr>';
						echo '<td></td>';
					}
					
					
					echo '<td>'.$date.'</td>';
					$skipDate = false;
					
					$skipName = true;

					foreach( (array)$eventList as $event){
						if($skipDate){
							echo '<tr>';
							echo '<td></td>';
							echo '<td></td>';
						}
						$_time = date ("H:i", strtotime($date));
						$_firstDateColumn = true;
						echo '<td>'.$event['Time'].'</td>';
						echo '<td>'.$event['Remarks'].'</td>';
						$skipDate = true;
						echo '</tr>';
					}
				}
			}
			else{
				echo'<td></td>';
				echo'<td></td>';
				echo'<td></td>';
				echo'</tr>';
			}	
					/*
					echo '<td>'.$date.'</td>';
					echo '<td>'.$eventList.'</td>';
					echo '<td>'..'</td>';
					if(!$firstDateColumn){
						echo '</tr>';
						echo '<tr>';
					}
					
					$rowspanDate = 0;
					$tempList = array();
					foreach( (array)$eventList as $event){
						$tempList[$event['Time']] = $event['Remarks'];
						$rowspanDate += count($event);
					}
					ksort($tempList);

					$eventList = $tempList;
					
					
					if($rowspanDate<=0){ 
						$rowspanDate = 1;
					}
					echo "<td rowspan='{$rowspanDate}'>{$date}</td>";
					
					$firstEventColumn =true;
					
					if(!$firstEventColumn){
						echo '</tr>';
						echo '<tr>';
					}
					
					foreach( (array)$eventList as $key=>$val){

	
	
						$time = date ("H:i", strtotime($key));
						echo "<td>{$time}</td>";
						echo "<td>{$val}</td>";
						
						$firstEventColumn =false;
					}
					
					$firstDateColumn = false;
					*/
				
			
			

		endforeach; ?>
	<?php else: ?>
		<tr><td colspan="5" align="center"  style="height:auto;">沒有相關資料。</td></tr>
	<?php endif; ?>
</table>
<div id="editBottomDiv" class="edit_bottom_v30">
	<p class="spacer"></p>
	<?php echo $htmlAry['cancelBtn']; ?>
</div>
