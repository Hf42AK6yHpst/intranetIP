<?php 
// using: 
/*
 * 	Log
 *  Date:   2019-06-03 Philips
 *      - add access right checking to print and export
 * 
 * 	Date:	2017-09-04 Cameron 
 * 		- add parameter LastDrug to getConvulsionReport2
 * 
 * 	Date:	2014-01-03 
 */

include_once($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/medical/reports/reportFunction.php");

//////////////// INPUT CHECKING /////////
if(!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $startDate) && $startDate !=''){
	echo 'Report Start Date Format Error! Please select with format YYYY-MM-DD';
	return;
}
if(!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $endDate) && $endDate !=''){
	echo 'Report End Date Format Error! Please select with format YYYY-MM-DD';
	return;
}
$dateRangeArr = dateRange( $startDate, $endDate, $step = '+1 day', $format = 'Y-m-d' );
if($_POST['displayOrder'] == 'desc'){
	$dateRangeArr = array_reverse($dateRangeArr);
}

$reportName= 'convulsionReport';

$dispGender = "";
switch($_POST["gender"])
{
	case 2:
		$dispGender = $Lang['medical']['report_general']['search']['genderOption']['M'];
		break;
	case 3:
		$dispGender = $Lang['medical']['report_general']['search']['genderOption']['F'];
		break;		
}
	
if ($_POST["sleep2"] == "0")
{
	$dispSleepType = $Lang['medical']['report_meal']['sleepOption']['StayOut'];
}
else if ($_POST["sleep2"] == "1")
{
	$dispSleepType = $Lang['medical']['report_meal']['sleepOption']['StayIn'];
}
else
{
	$dispSleepType = "";	
}
 
$space = ($intranet_session_language == "en" ) ? "&nbsp;" : "";
$disStudentType = $dispGender . $space . $dispSleepType;

switch($action){
	case 'print':
	    if($sys_custom['medical']['accessRight']['ViewAll'] && !($objMedical->isSuperAdmin($_SESSION['UserID'])|| $objMedical->checkAccessRight($_SESSION['UserID'], "STUDENTLOG_REPORTPRINT"))){
	        header("Location: /");
	        exit();
	    }
		include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
		
		$printButton = $linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit2");
		$printHTML = <<<HTML
		<style>
		@media print
		{    
		    .print_hide, .print_hide *
		    {
		        display: none !important;
		    }
			table { page-break-after:always !important}
			tr    { page-break-inside:avoid !important; page-break-after:auto !important}
			td    { page-break-inside:avoid !important; page-break-after:auto !important}		    
		}
		</style>
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		</td></tr>
		<tr><td>
		<div class="print_hide" align="right">
		<br />
		$printButton
		<br />
		<br />
		</div>
HTML;
		echo $printHTML;

		$hasRecord = false;
		foreach($dateRangeArr as $d){
			$detailList = $objMedical->getStudentLogReport2($d, $studentList, $level2, $LastDrug);	
			if(!empty($detailList) || $_POST['displayEmptyData']){
				$resultHeader = 
				'<div style="width:100%;border-bottom: 2px black solid;font-weight:bold;">'.
				$Lang['medical']['studentLog']['report_convulsion']['reportName2']. ':&nbsp;' . 
				$disStudentType.
				'&nbsp;(&nbsp;'.$d.'&nbsp;)'.
				'</div><br /><br />';
				echo $resultHeader;
				
				echo getConvulsionReport2($detailList, $outputFormat = 'html', $LastDrug);	
				echo '<hr/><br/>';	
				$hasRecord = true;
			}
		}
		if(!$hasRecord){
			echo "<div style='text-align: center'>{$Lang['SysMgr']['Homework']['NoRecord']}</div>";
		}

		$printHTML = <<<HTML
		</td></tr>
		</table>
		</body>
		</html>
HTML;
		echo $printHTML;
	break;
	
	case 'export':
	    if($sys_custom['medical']['accessRight']['ViewAll'] && !($objMedical->isSuperAdmin($_SESSION['UserID'])|| $objMedical->checkAccessRight($_SESSION['UserID'], "STUDENTLOG_REPORTEXPORT"))){
	        header("Location: /");
	        exit();
	    }
		include_once($PATH_WRT_ROOT."includes/libexporttext.php");
		$lexport = new libexporttext();
		
		$hasRecord = false;
		$exportContent = '';
		foreach($dateRangeArr as $d){
			$detailList = $objMedical->getStudentLogReport2($d, $studentList, $level2, $LastDrug);
			if(!empty($detailList) || $_POST['displayEmptyData']){
				$exportContent .= $d . "\r\n";
				$exportContent .= getConvulsionReport2($detailList, $outputFormat = 'csv',$LastDrug);
				$exportContent .= "\r\n";
				$exportContent .= "\r\n";
				$hasRecord = true;
			}
		}
		if(!$hasRecord){
			$exportContent = $Lang['SysMgr']['Homework']['NoRecord'];
		}
		$lexport->EXPORT_FILE('export.csv', $exportContent);
		exit();
		break;	
	default:
		//////////////// INPUT CHECKING /////////
		echo $linterface->Include_Thickbox_JS_CSS();
		echo '<div class="Conntent_tool">';
		if(!$sys_custom['medical']['accessRight']['ViewAll'] || ($objMedical->isSuperAdmin($_SESSION['UserID'])|| $objMedical->checkAccessRight($_SESSION['UserID'], "STUDENTLOG_REPORTPRINT")))
		  echo '<a class="print tablelink" name="printBtn" id="printBtn" style="float:none;display:inline;">'.$Lang['Btn']['Print'].'</a>&nbsp;';
	    if(!$sys_custom['medical']['accessRight']['ViewAll'] ||($objMedical->isSuperAdmin($_SESSION['UserID'])|| $objMedical->checkAccessRight($_SESSION['UserID'], "STUDENTLOG_REPORTEXPORT")))
		  echo '<a class="export tablelink" name="exportBtn" id="exportBtn" style="float:none;display:inline;">'.$Lang['Btn']['Export'].'</a>';
		echo '</div>';

		echo "<br/><br/><br/>";
		
		$hasRecord = false;
		foreach($dateRangeArr as $d){
			$detailList = $objMedical->getStudentLogReport2($d, $studentList, $level2, $LastDrug);
			if(!empty($detailList) || $_POST['displayEmptyData']){
				$resultHeader = 
				'<div style="width:100%;border-bottom: 2px black solid;font-weight:bold;">'.
				$Lang['medical']['studentLog']['report_convulsion']['reportName2']. ':&nbsp;' . 
				$disStudentType.
				'&nbsp;(&nbsp;'.$d.'&nbsp;)'.
				'</div><br /><br />';
				echo $resultHeader;
				
				echo getConvulsionReport2($detailList, $outputFormat = 'html',$LastDrug);	
				echo '<hr/><br/>';	
				$hasRecord = true;
			}
		}	
		if(!$hasRecord){
			echo "<div style='text-align: center'>{$Lang['SysMgr']['Homework']['NoRecord']}</div>";
		}
		
		$script = <<<HTML
		<script>
		$('#exportBtn').click(function(){
			$('#reportForm').attr('action',"?t=reports.ajax.getConvulsionReport2&action=export");
			$('#reportForm').submit();
		});
		$('#printBtn').click(function(){
			$('#reportForm').attr('action',"?t=reports.ajax.getConvulsionReport2&action=print");
			$('#reportForm').submit();		
		});
		
		$('a.fixedSizeThickboxLink').click( function() {
			Id = $(this).attr('data-Id');
			load_dyn_size_thickbox_ip('{$Lang['medical']['report_general']['attachmentTitle']}', 'onloadThickBox();', inlineID='', defaultHeight=250, defaultWidth=400);
		});	
		function onloadThickBox() {
			$('div#TB_ajaxContent').load(
				"?t=management.ajax.getStudentLogAttachmentList",{ 
					'Id': Id
				},
				function(ReturnData) {
				}
			);
		}
		</script>
HTML;
		echo $script;
	break;
}


function dateRange( $first, $last, $step = '+1 day', $format = 'Y/m/d' ) {
	$dates = array();
	$current = strtotime( $first );
	$last = strtotime( $last );
	while( $current <= $last ) {

		$dates[] = date( $format, $current );
		$current = strtotime( $step, $current );
	}
	return $dates;
}
?>