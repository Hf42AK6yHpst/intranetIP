<?php
// using: 
/*
 * 	Log
 *  Date:   20190708 Cameron
 *      - add filter by PIC [case #P150622]
 *
 *  Date:   20190617 Philips
 */

//////////////// INPUT CHECKING /////////
if(!$sys_custom['medical']['Handover'] || !$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'HANDOVER_REPORT')){
    header("Location: /");
    exit();
}
$reportName= 'handoverReport';
include_once($PATH_WRT_ROOT."/includes/libdb.php");
global $i_no_record_exists_msg;
$li = new libdb();

$r_startDate = trim($_POST['r_startDate']);
$r_endDate = trim($_POST['r_endDate']);
$itemAry = $_POST['Item'];
$itemAry = array_keys((array)$itemAry);
$picAry = $_POST['Pic'];
$picAry = array_keys((array)$picAry);

$displayOrder = $_POST['displayOrder'];

$firstField = 'iu.'.Get_Lang_Selection('ChineseName','EnglishName');
$secondField = 'iu.'.Get_Lang_Selection('EnglishName','ChineseName');
$firstTitle = 'iu.'.Get_Lang_Selection('TitleChinese', 'TitleEnglish');
$secondTitle = 'iu.'.Get_Lang_Selection('TitleEnglish', 'TitleChinese');
$cols = "mh.RecordDate,
         mh.Remark,
         mhi.ItemName, 
         IF( mhp.RecordID IS NULL, '', GROUP_CONCAT(IF($firstField IS NULL OR $firstField = '', CONCAT({$secondField},IF({$secondTitle} IS NULL OR {$secondTitle} = '', '', CONCAT(' ', {$secondTitle}))), CONCAT({$firstField},IF({$firstTitle} IS NULL OR {$firstTitle} = '', '', CONCAT(' ', {$firstTitle})))))) AS PIC,
         mh.RecordID
";
$table = "MEDICAL_HANDOVER mh";
$joinTable = "INNER JOIN MEDICAL_HANDOVER_ITEM mhi
                ON mh.RecordItemID = mhi.ItemID
            LEFT OUTER JOIN MEDICAL_HANDOVER_PIC mhp
                ON mh.RecordID = mhp.RecordID
            LEFT OUTER JOIN INTRANET_USER iu
                ON mhp.UserID = iu.UserID
            LEFT OUTER JOIN MEDICAL_HANDOVER_ATTACHMENT mha
                ON mh.RecordID = mha.RecordID";
$conds = "1
          AND mh.isDeleted = '0'
          AND (DATE_FORMAT(mh.RecordDate, '%Y-%m-%d') >= '{$r_startDate}' AND DATE_FORMAT(mh.RecordDate, '%Y-%m-%d') <= '{$r_endDate}')
          AND mh.RecordItemID IN (' ". implode("','", $itemAry) ."')
";
if (count($picAry)) {
    $conds .= " AND mhp.UserID IN ('".implode("','",(array)$picAry)."') ";
}
$group = "mh.RecordID";
$order="mh.RecordDate {$displayOrder}";

$sql = "SELECT 
            $cols
        FROM
            $table
            $joinTable
        WHERE
            $conds
        GROUP BY
            $group
        ORDER BY
            $order
";
// debug_pr($sql);die();
$recordAry = $li->returnArray($sql);
switch($action){
    case 'print':
        $style = <<<style
<style>
@media print
{
    .print_hide, .print_hide *
    {
        display: none !important;
    }
	table { page-break-after:always !important}
	tr    { page-break-inside:avoid !important; page-break-after:auto !important}
	td    { page-break-inside:avoid !important; page-break-after:auto !important}
}
table.reportTable {
    width: 100%;
    border-collapse: collapse;
}
table.reportTable th, table.reportTable tr, table.reportTable td{
    border: 1px black solid;
    padding: 5px;
    vertical-align: top;
}
</style>
style;
        echo $style;
        $printButton = $linterface->GET_BTN($Lang['Btn']['Print'], "button", "window.print();","submit2");
        $printHTML = <<<HTML
    <div class="print_hide" align="right">
    <br />
    $printButton
    <br />
    <br />
    </div>
    
HTML;
        echo $printHTML;
        
        if(sizeof($recordAry) > 0){
            foreach($recordAry as $record){
                $recordID = $record['RecordID'];
                $attachments = $objMedical->getHandoverRecordAttachment($recordID);
                $r_Date = date('Y-m-d', strtotime($record['RecordDate']));
                $r_Time = date('H:i', strtotime($record['RecordDate']));
                echo "<h3 style='text-align:center' width=100% align=center>{$Lang['medical']['menu']['report_handover']}</h3>";
                $tableHeaderHTML = <<<TABLE
<table class="reportTable">
    <col width="20%">
    <col width="80">
TABLE;
                echo $tableHeaderHTML;
                $tableBody = "<tbody>";
                    $tableBody .= "<tr>";
                        $tableBody .= "<td>{$Lang['General']['Date']}</td>";
                        $tableBody .= "<td>{$r_Date}</td>";
                    $tableBody .= "</tr>";
                    $tableBody .= "<tr>";
                        $tableBody .= "<td>{$Lang['General']['Time']}</td>";
                        $tableBody .= "<td>{$r_Time}</td>";
                    $tableBody .= "</tr>";
                    $tableBody .= "<tr>";
                        $tableBody .= "<td>{$Lang['medical']['handover']['item']}</td>";
                        $tableBody .= "<td>{$record['ItemName']}</td>";
                    $tableBody .= "</tr>";
//                     $tableBody .= "<tr>";
//                         $tableBody .= "<td>{$Lang['medical']['handover']['attachment']}</td>";
//                         $tableBody .= "<td>";
//                         foreach($attachments as $attach){
//                             $url = $file_path. "/file/" . $attach['AttachmentLink'];
//                             $filename = substr($attach['AttachmentLink'], strrpos($attach['AttachmentLink'], '/', -1) + 1);
//                             $tableBody .= $filename . '<br/>';
//                         }
//                         $tableBody .= "</td>";
//                     $tableBody .= "</tr>";
                    $tableBody .= "<tr>";
                        $tableBody .= "<td>{$Lang['General']['Remark']}</td>";
                        $tableBody .= "<td>".nl2br($record['Remark'])."</td>";
                    $tableBody .= "</tr>";
                    $tableBody .= "<tr>";
                        $tableBody .= "<td>{$Lang['medical']['handover']['pic']}</td>";
                        $tableBody .= "<td>" . str_replace(',','<br />',$record['PIC']) . "</td>";
                    $tableBody .= "</tr>";
                
                /*// Format 2
                $tableBody .= '<tr>';
                    $tableBody .= "<td>{$r_Date}</td>";
                    $tableBody .= "<td>{$r_Time}</td>";
                    $tableBody .= "<td>{$record['ItemName']}</td>";
                    $tableBody .= "<td>{$record['AttachmentCount']}</td>";
                $tableBody .= '</tr>';
                $tableBody .= '<tr>';
                    $tableBody .= "<td colspan='2'>";
                        $tableBody .= $Lang['General']['Remark'] . ': ' . $record['Remark'];
                    $tableBdoy .= "</td>";
                    $tableBody .= "<td colspan='2'>";
                        $tableBody .= $Lang['medical']['handover']['pic'] . ': ' . $record['PIC'];
                    $tableBody .= "</td>";
                    $tableBody .= '</tr>';*/
                $tablebody .= "</tbody>";
                echo $tableBody;
                echo "</table>";
            }
        } else {
            /*
            $tableBody .= "<tr>";
            $tableBody .= "<td colspan='4'>$i_no_record_exists_msg</td>";
            $tableBody .= "</tr>";*/
        }
        break;
    default:
//         debug_pr($recordAry);die();
        echo '<div class="Conntent_tool">';
            echo '<a href="#" class="print tablelink" name="printBtn" id="printBtn" onclick="printReport()">'.$Lang['Btn']['Print'].'</a>&nbsp;';
        echo '</div>';
        $resultHeader =
        '<br /><br /><br /><div style="width:100%;border-bottom: 2px black solid;font-weight:bold;">'.
        $Lang['medical']['report_meal']['search']['timePeriodOption'][$timePeriodHeader].
        $Lang['medical']['menu']['report_handover'].': '.
            '(&nbsp;'.$r_startDate.'&nbsp;'.$Lang['StaffAttendance']['To'].'&nbsp;'.$r_endDate.'&nbsp;)'.
            '</div><br /><br />';
        echo $resultHeader;
        if(sizeof($recordAry) > 0){
            foreach($recordAry as $record){
                $recordID = $record['RecordID'];
                $attachments = $objMedical->getHandoverRecordAttachment($recordID);
                $r_Date = date('Y-m-d', strtotime($record['RecordDate']));
                $r_Time = date('H:i', strtotime($record['RecordDate']));
                echo "<h3 style='text-align:center' width=100% align=center>{$Lang['medical']['menu']['report_handover']}</h3>";
                $tableHeaderHTML = <<<TABLE
<table class="common_table_list_v30">
    <col width="20%">
    <col width="80%">
TABLE;
                echo $tableHeaderHTML;
                $tableBody = "<tbody>";
                    $tableBody .= "<tr>";
                        $tableBody .= "<td>{$Lang['General']['Date']}</td>";
                        $tableBody .= "<td>{$r_Date}</td>";
                    $tableBody .= "</tr>";
                    $tableBody .= "<tr>";
                        $tableBody .= "<td>{$Lang['General']['Time']}</td>";
                        $tableBody .= "<td>{$r_Time}</td>";
                    $tableBody .= "</tr>";
                    $tableBody .= "<tr>";
                        $tableBody .= "<td>{$Lang['medical']['handover']['item']}</td>";
                        $tableBody .= "<td>{$record['ItemName']}</td>";
                    $tableBody .= "</tr>";
//                     $tableBody .= "<tr>";
//                         $tableBody .= "<td>{$Lang['medical']['handover']['attachment']}</td>";
//                         $tableBody .= "<td>";
//                         foreach($attachments as $attach){
//                             $url = $file_path. "/file/" . $attach['AttachmentLink'];
//                             $filename = substr($attach['AttachmentLink'], strrpos($attach['AttachmentLink'], '/', -1) + 1);
//                             $tableBody .= $filename . '<br/>';
//                         }
//                         $tableBody .= "</td>";
//                     $tableBody .= "</tr>";
                    $tableBody .= "<tr>";
                        $tableBody .= "<td>{$Lang['General']['Remark']}</td>";
                        $tableBody .= "<td>".nl2br($record['Remark'])."</td>";
                    $tableBody .= "</tr>";
                    $tableBody .= "<tr>";
                        $tableBody .= "<td>{$Lang['medical']['handover']['pic']}</td>";
                        $tableBody .= "<td>" . str_replace(',','<br />',$record['PIC']) . "</td>";
                    $tableBody .= "</tr>";
                $tablebody .= "</tbody>";
                echo $tableBody;
                echo '</table>';
                /*// Format 2
                $tableBody .= '<tr>';
                    $tableBody .= "<td>{$r_Date}</td>";
                    $tableBody .= "<td>{$r_Time}</td>";
                    $tableBody .= "<td>{$record['ItemName']}</td>";
                    $tableBody .= "<td>{$record['AttachmentCount']}</td>";
                $tableBody .= '</tr>';
                $tableBody .= '<tr>';
                    $tableBody .= "<td colspan='2'>";
                        $tableBody .= $Lang['General']['Remark'] . ': ' . $record['Remark'];
                    $tableBdoy .= "</td>";
                    $tableBody .= "<td colspan='2'>";
                        $tableBody .= $Lang['medical']['handover']['pic'] . ': ' . $record['PIC'];
                    $tableBody .= "</td>";
                $tableBody .= '</tr>';
                */
            }
        } else {
            echo $i_no_record_exists_msg;
        }
        
        break;
}