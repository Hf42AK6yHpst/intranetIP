<?php 
// using: Adam
/*
 * 	Log
 * 	Date:	2014-01-03 
 */

include_once($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/medical/reports/reportFunction.php");

//////////////// INPUT CHECKING /////////
if(!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $startDate)){
	echo 'Report Start Date Format Error! Please select with format YYYY-MM-DD';
	return;
}
if(!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $endDate)){
	echo 'Report End Date Format Error! Please select with format YYYY-MM-DD';
	return;
}

$reportName= 'sleepReport';

$userID = implode($studentList);
$sleepStudentData = $objMedical->getSleepReportData($reasonID='', $userID, $startDate,$endDate, $getReasonName=true, $getReport3Data=true);
$objUser = new libuser($userID);
$disStudentType = $objUser->UserNameLang();

switch($action){
	case 'print':
	    if($sys_custom['medical']['accessRight']['PrintAndExport'] && !($objMedical->isSuperAdmin($_SESSION['UserID'])|| $objMedical->checkAccessRight($_SESSION['UserID'], "SLEEP_REPORTPRINT"))){
	        header("Location: /");
	        exit();
	    }
		include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
		
		$printButton = $linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit2");
		$printHTML = <<<HTML
		<style>
		@media print
		{    
		    .print_hide, .print_hide *
		    {
		        display: none !important;
		    }
			table { page-break-after:always !important}
			tr    { page-break-inside:avoid !important; page-break-after:auto !important}
			td    { page-break-inside:avoid !important; page-break-after:auto !important}		    
		}
		</style>
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		</td></tr>
		<tr><td>
		<div class="print_hide" align="right">
		<br />
		$printButton
		</div>
		<br />
		<br />
HTML;
		echo $printHTML;
		$resultHeader = 
		'<div style="width:100%;border-bottom: 2px black solid;font-weight:bold;">'.
		$Lang['medical']['report_studentSleep']['report3']. ':&nbsp;' . 
		$disStudentType.
		'&nbsp;(&nbsp;'.$startDate.' '.$Lang['StaffAttendance']['To'].' '.$endDate.'&nbsp;)'.
		'</div><br /><br />';

		echo $resultHeader;

		echo $objMedical->getSleepReport3($sleepStudentData, $userID, $startDate, $endDate, $outputFormat = 'html');

		$printHTML = <<<HTML
		</td></tr>
		</table>
		</body>
		</html>
HTML;
		echo $printHTML;
	break;
	
	case 'export':
	    if($sys_custom['medical']['accessRight']['PrintAndExport'] && !($objMedical->isSuperAdmin($_SESSION['UserID'])|| $objMedical->checkAccessRight($_SESSION['UserID'], "SLEEP_REPORTEXPORT"))){
	        header("Location: /");
	        exit();
	    }
		echo $objMedical->getSleepReport3($sleepStudentData, $userID, $startDate, $endDate, $outputFormat = 'csv');
	exit();
	break;	
	default:
		//////////////// INPUT CHECKING /////////
		echo '<div class="Conntent_tool">';
		if(!$sys_custom['medical']['accessRight']['PrintAndExport'] || ($objMedical->isSuperAdmin($_SESSION['UserID'])|| $objMedical->checkAccessRight($_SESSION['UserID'], "SLEEP_REPORTPRINT")))
		  echo '<a class="print tablelink" name="printBtn" id="printBtn" style="float:none;display:inline;">'.$Lang['Btn']['Print'].'</a>&nbsp;';
		  if(!$sys_custom['medical']['accessRight']['PrintAndExport'] ||($objMedical->isSuperAdmin($_SESSION['UserID'])|| $objMedical->checkAccessRight($_SESSION['UserID'], "SLEEP_REPORTEXPORT")))
		  echo '<a class="export tablelink" name="exportBtn" id="exportBtn" style="float:none;display:inline;">'.$Lang['Btn']['Export'].'</a>';
		echo '</div>';

		echo "<br/><br/><br/>";
		$resultHeader = 
		'<div style="width:100%;border-bottom: 2px black solid;font-weight:bold;">'.
		$Lang['medical']['report_studentSleep']['report3']. ':&nbsp;' . 
		$disStudentType.
		'&nbsp;(&nbsp;'.$startDate.' '.$Lang['StaffAttendance']['To'].' '.$endDate.'&nbsp;)'.
		'</div><br /><br />';

		echo $resultHeader;

		echo $objMedical->getSleepReport3($sleepStudentData, $userID, $startDate, $endDate, $outputFormat = 'html');
		echo $linterface->Include_Thickbox_JS_CSS();
		$script = <<<HTML
		<script>
		$('#exportBtn').click(function(){
				$('#reportForm').attr('action',"?t=reports.ajax.getSleepReport3&action=export");
				$('#reportForm').submit();
			});
		$('#printBtn').click(function(){
				$('#reportForm').attr('action',"?t=reports.ajax.getSleepReport3&action=print");
				$('#reportForm').submit();
		});
		$('a.dateDetail').click(function(){
				selectedDate = $(this).attr('data-date');
				load_dyn_size_thickbox_ip("{$Lang['medical']['bowel']['tableHeader']['Detail']}", 'onloadThickBox();', inlineID='', defaultHeight=400, defaultWidth=800);
							
		});

		function onloadThickBox() {
			$('div#TB_ajaxContent').load(
				"?t=reports.ajax.getThickBoxForSleepReport3",{ 
					'userID': {$userID},
					'date': selectedDate
				},
				function(ReturnData) {
				}
			);
		}
		</script>
HTML;
		echo $script;
	break;
}
?>