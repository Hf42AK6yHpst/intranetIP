<?php 
// using: 
/*
 * 	Log
 * 	Date:	2013-12-30 [Cameron] 
 */

include_once($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/medical/reports/reportFunction.php");

//////////////// INPUT CHECKING /////////
if(!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $r_startDate)){
	echo 'Start Report Date Format Error! Please select with format YYYY-MM-DD';
	return;
}

if(!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $r_endDate)){
	echo 'Start Report Date Format Error! Please select with format YYYY-MM-DD';
	return;
}

$reportName= 'convulsionReport';

switch($action){
	case 'print':
		
	case 'print_format2':
	    if($sys_custom['medical']['accessRight']['PrintAndExport'] && !($objMedical->isSuperAdmin($_SESSION['UserID'])|| $objMedical->checkAccessRight($_SESSION['UserID'], "STUDENTLOG_REPORTPRINT"))){
	        header("Location: /");
	        exit();
	    }
		include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
		
		$printButton = $linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit2");

		$printHTML = <<<HTML

		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td>
		<div class="print_hide" align="right">
		<br />
		$printButton
		<br />
		<br />
		</div>
		
HTML;
		echo $printHTML;

//		$resultHeader = 
//		'<div style="width:100%;border-bottom: 2px black solid;font-weight:bold;">'.		
//		$Lang['medical']['studentLog']['report_convulsion']['reportName'].': '.
//// Student Name		
//		'(&nbsp;'.$r_startDate.'&nbsp;'.$Lang['StaffAttendance']['To'].'&nbsp;'.$r_endDate.'&nbsp;)'.
//		'</div><br /><br />';
//		$resultHeader ='<br />';

//		echo $resultHeader;

		// $config_school_code == '250473' , school code of sunnyside
		$printingWithHeader = ($action == 'print_format2' && $config_school_code == '250473') ? 1: 0;//
		$rowPerPage = $medical_cfg['report']['convlusion']['rowPerPage'];
		echo getConvulsionReport($_POST['r_startDate'],$_POST['r_endDate'],
						$_POST['studentList'], $_POST['bodyParts'], $_POST['syndrome'],
						$outputFormat = 'html',$_POST['r_withRemarks'],$printingWithHeader, $_POST['displayEmptyData'],$rowPerPage);
		$printHTML = <<<HTML
		</td></tr>
		</table>
		</body>
		</html>
HTML;
		echo $printHTML;
	break;
	case 'export':
	    if($sys_custom['medical']['accessRight']['PrintAndExport'] && !($objMedical->isSuperAdmin($_SESSION['UserID'])|| $objMedical->checkAccessRight($_SESSION['UserID'], "STUDENTLOG_REPORTEXPORT"))){
	        header("Location: /");
	        exit();
	    }
		getConvulsionReport($_POST['r_startDate'],$_POST['r_endDate'],
						$_POST['studentList'], $_POST['bodyParts'], $_POST['syndrome'],
//						$gender, $sleepOvernight,
						$outputFormat = 'csv',$_POST['r_withRemarks'],$printingWithHeader = 0, $_POST['displayEmptyData']);
	exit();
	break;	
	default:
		//////////////// INPUT CHECKING /////////
		echo '<div class="Conntent_tool">';
		if(!$sys_custom['medical']['accessRight']['PrintAndExport'] || ($objMedical->isSuperAdmin($_SESSION['UserID'])|| $objMedical->checkAccessRight($_SESSION['UserID'], "STUDENTLOG_REPORTPRINT")))
		  echo '<a class="print tablelink" name="printBtn" id="printBtn" style="float:none;display:inline;">'.$Lang['Btn']['Print'].'</a>&nbsp;';
	    if(!$sys_custom['medical']['accessRight']['PrintAndExport'] || ($objMedical->isSuperAdmin($_SESSION['UserID'])|| $objMedical->checkAccessRight($_SESSION['UserID'], "STUDENTLOG_REPORTEXPORT")))
		  echo '<a class="export tablelink" name="exportBtn" id="exportBtn" style="float:none;display:inline;">'.$Lang['Btn']['Export'].'</a>';
		echo '</div>';
//				$resultHeader = 
//		'<br /><br /><br /><div style="width:100%;border-bottom: 2px black solid;font-weight:bold;">'.
//		$Lang['medical']['studentLog']['report_convulsion']['reportName'].': '.
//// Student Name		
//		'(&nbsp;'.$r_startDate.'&nbsp;'.$Lang['StaffAttendance']['To'].'&nbsp;'.$r_endDate.'&nbsp;)'.
//		'</div><br /><br />';
				$resultHeader = '<br /><br /><br />';

		echo $resultHeader;

		echo getConvulsionReport($_POST['r_startDate'],$_POST['r_endDate'],
						$_POST['studentList'], $_POST['bodyParts'], $_POST['syndrome'], 
//						$gender, $sleepOvernight,
						$outputFormat = 'html',$_POST['r_withRemarks'],$printingWithHeader = 0, $_POST['displayEmptyData']);
		$script = <<<HTML
		<script>
		$('#exportBtn').click(function(){
			$('#reportForm').attr('action',"?t=reports.ajax.getConvulsionReport&action=export");
			$('#reportForm').submit();
		});
		$('#printBtn').click(function(){
			$('#reportForm').attr('action',"?t=reports.ajax.getConvulsionReport&action=print_format2");
			$('#reportForm').submit();		
		});
		</script>
HTML;
		echo $script;
	break;
}
?>