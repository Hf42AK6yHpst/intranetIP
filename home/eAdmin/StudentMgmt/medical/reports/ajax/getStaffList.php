<?php
// using:
/*
 *  2018-12-31 Cameron
 *      - create this file
 */
global $Lang;

if ($_POST['teachingStaff'] && $_POST['nonTeachingStaff']) {
    $staffType = '-1';  // all staff 
}
else if ($_POST['teachingStaff']) {
    $staffType = '1';
}
else if ($_POST['nonTeachingStaff']) {
    $staffType = '0';
}
else {
    $emptyStaff = true;
}

if ($emptyStaff) {
    $resultHTML = '';
}
else {
    $resultHTML = $objMedical->getMultipleSelectStaff($staffType);
}

echo $resultHTML;
?>