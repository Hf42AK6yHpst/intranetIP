<?php
/*
 *  2018-05-29 Cameron
 *      - fix: apply stripslashes to status in remarks
 */
//using: 
//debug_r($r_studentId);
$thickBoxStudentId = $r_studentId;
$thickBoxDate = $r_date;
$thickBoxID = $r_ID;


$objMealDailyLog = new mealDailyLog($thickBoxDate,$thickBoxID);


$statusRs = $objMedical->getAllMealStatus();
$statusArr = array();
foreach($statusRs as $r){
	$statusArr[$r['StatusID']] = $r['StatusName']; 
}

$objUser = new libuser($r_studentId);
$disStudentType = $objUser->UserNameLang();
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$resultHeader = 
		'<div style="width:100%;border-bottom: 2px black solid;font-weight:bold;">'.
		$Lang['medical']['report_meal']['report2'].':&nbsp;' . 
		$disStudentType.
		'&nbsp;(&nbsp;'.$thickBoxDate.'&nbsp;)'.
		'</div>';

$resultHTML = <<<HTML
	<br />
	{$resultHeader}
	<br />
	<form id="thickboxForm" name="thickboxForm">
	<table class="common_table_list_v30">
	<colgroup>
		<col style="width: 20%"/>
		<col style="width: 20%;min-width:80px"/>
		<col style="width: 60%"/>
	</colgroup>
		<thread>
			<th style="min-width:50px">{$Lang['medical']['report_meal']['searchMenu']['timePeriod']}</th>
			<th style="min-width:80px">{$Lang['medical']['meal']['tableHeader']['EatingCondition']}</th>
			<th style="">{$Lang['medical']['meal']['tableHeader']['Remarks']}</th>
		</thread>
HTML;
echo $resultHTML;
	
            $status = stripslashes($statusArr[$objMealDailyLog->getMealStatus()]);
            $remarks = stripslashes($objMealDailyLog->getRemarks());

			$resultHTML = <<<HTML
				<tr>
					<td style="min-width:50px">{$Lang['medical']['report_meal']['search']['timePeriodOption'][$objMealDailyLog->getPeriodStatus()]}</td>
					<td style="min-width:80px">{$status}</td>
					<td style="">{$remarks}&nbsp;</td>
				</tr>
HTML;
			echo $resultHTML;		
			
$resultHTML = <<<HTML
	</table>
	</form>
HTML;
echo $resultHTML;
?>
<div id="editBottomDiv" class="edit_bottom_v30">
	<p class="spacer"></p>
	<?php echo $htmlAry['cancelBtn']; ?>
</div>