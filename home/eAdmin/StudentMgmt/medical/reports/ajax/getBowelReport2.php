<?php 
// using: 
/*
 * 	Log
 * 	Date:	2014-01-03 
 */

include_once($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/medical/reports/reportFunction.php");

//////////////// INPUT CHECKING /////////
if(!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $startDate)){
	echo 'Report Start Date Format Error! Please select with format YYYY-MM-DD';
	return;
}
if(!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $endDate)){
	echo 'Report End Date Format Error! Please select with format YYYY-MM-DD';
	return;
}
$reportName= 'bowelReport';

$dispGender = "";
//$reasonIDList = $objMedical->getSleepLevel2List($reasonID); 
$bowelStudentData = $objMedical->getBowelReportData($reasonID, $studentList, $startDate,$endDate,$sleep2, $report=2);

$StudentList = $objMedical->getStudentList($studentList, $sleep2, $genderNum='', $showFullDetail = true);

$str_reasonID = implode(',',$reasonID);

// how many days ? 
//$startDate = str_replace("-","",$_POST['startDate']);
//$endDate = str_replace("-","",$_POST['endDate']);
$date =  date("Y-m-d", strtotime($startDate));
$final_date = date("Y-m-d", strtotime($endDate));
$dates = array();
$dates[] = date("Y-m-d", strtotime($date));
while($date < $final_date){
   $date = date("Y-m-d", strtotime($date . " +1 day"));
   $dates[] = $date;
}
//for($i=0;$i<count($dates);$i++){
//	$dates[$i] = str_replace("-","",$dates[$i]);
////	$dates[$i] = 	
//} 
//debug_r($dates);

//debug_r($bowelStudentData);
$dispGender = "";
switch($_POST["gender"])
{
	case 2:
		$dispGender = $Lang['medical']['report_general']['search']['genderOption']['M'];
		break;
	case 3:
		$dispGender = $Lang['medical']['report_general']['search']['genderOption']['F'];
		break;		
}
	
if ($_POST["sleep2"] == "0")
{
	$dispSleepType = $Lang['medical']['report_meal']['sleepOption']['StayOut'];
}
else if ($_POST["sleep2"] == "1")
{
	$dispSleepType = $Lang['medical']['report_meal']['sleepOption']['StayIn'];
}
else
{
	$dispSleepType = "";	
} 
$space = ($intranet_session_language == "en" ) ? "&nbsp;" : "";
$disStudentType = $dispGender . $space . $dispSleepType;


//$bowelStudentData;
//$dates;
//$StudentList

switch($action){
	case 'print':
	    if($sys_custom['medical']['accessRight']['PrintAndExport'] && !($objMedical->isSuperAdmin($_SESSION['UserID'])|| $objMedical->checkAccessRight($_SESSION['UserID'], "BOWEL_REPORTPRINT"))){
        header("Location: /");
        exit();
    }
	$StyleForTable = <<<HTML
	<style>
	 .bowelReport{
		min-width:70px !important;
		text-align:center !important;
	 }
	 </style>
HTML;
echo $StyleForTable; 
		include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
		
		$printButton = $linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit2");
		$printHTML = <<<HTML
		<style>
		@media print
		{    
		    .print_hide, .print_hide *
		    {
		        display: none !important;
		    }
			table { page-break-after:always !important}
			tr    { page-break-inside:avoid !important; page-break-after:auto !important}
			td    { page-break-inside:avoid !important; page-break-after:auto !important}		    
		}
		</style>
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		</td></tr>
		<tr><td>
		<div class="print_hide" align="right">
		<br />
		$printButton
		</div>
		<br />
		<br />
HTML;
		echo $printHTML;

		
		$resultHeader = 
		'<div style="width:100%;border-bottom: 2px black solid;font-weight:bold;">'.
		$Lang['medical']['report_studentBowel']['report2']. ':&nbsp;' . 
		$disStudentType.
		'&nbsp;(&nbsp;'.$startDate.' '.$Lang['StaffAttendance']['To'].' '.$endDate.' )'.
		'</div><br /><br />';

		echo $resultHeader;
//$bowelStudentData;
//$dates;
//$StudentList
	echo '<table class="common_table_list_v30">';
echo '<thead><tr class="tabletop">';
echo '<th class="bowelReport">'.$Lang['medical']['sleep']['searchMenu']['form'].'</th><th class="bowelReport">'.$Lang['medical']['bowel']['importRemarksTable']['studentName'].'</th><th class="bowelReport">'.$Lang['medical']['sleep']['searchMenu']['sleep'].'</th>';
$dayHeader = array();
foreach($dates as $date){
	$weekday = date("w", strtotime($date));
	$day = (int)trim($date[8].$date[9]);
	$month = (int)trim($date[5].$date[6]);
	$dayHeader[] = $day.'/'.$month.'<br/>'.$Lang['General']['DayType4'][$weekday];	
} 
foreach($dayHeader as $dayCol ){
	echo '<th class="bowelReport">'.$dayCol.'</th>';
}
if($r_withRemarks == 1){
	echo '<th class="bowelReport">'.$Lang['medical']['report_meal']['search']['itemCheckbox']['remarks'].'</th>';
}
echo '</tr>';
foreach($StudentList as $studentId=>$details)
{
	/*
	 array(4) {
  ["Name"]=>
  string(14) "九甲_學生_2"
  ["UserID"]=>
  string(4) "1972"
  ["stayOverNight"]=>
  NULL
  ["ClassName"]=>
  string(6) "九甲"
}
*/
	$isStay ='';
	if($details['stayOverNight'] == 1 )
	{
	   $isStay = 1; 
	} 
	else 
	{
		$isStay = 2;
	}
	$Row = '<tr><td class="bowelReport">'.$details['ClassName'].'</td><td class="bowelReport">'.$details['Name'].'</td><td class="bowelReport">'.$Lang['medical']['sleep']['search']['sleepOption'][$isStay];
	//get the all the bowl detail for a userid
	$bowlDetails =$bowelStudentData[$details['UserID']];
	//debug_r($bowelStudentData); 
	$_str = '';
//	debug_r($dates);
//	debug_r($bowlDetails);
//	foreach ($date1s as $y => $item){
		//debug_r($y);

	for($i = 0,$iMax = count($dates);$i < $iMax; $i++){
//		debug_r($dates[$i]);	
			$_str = '';
		$_BowlDetailsOnThatDate =$bowlDetails[$dates[$i]];
//		debug_r($_BowlDetailsOnThatDate);
		if(is_array($_BowlDetailsOnThatDate )){
			$bowlRs = array();
			ksort($_BowlDetailsOnThatDate); 
			foreach($_BowlDetailsOnThatDate as $bowlChar=>$bowlid){
				if($bowlid > 1){
					$bowlRs[] = $bowlid.$bowlChar;
				}else 
				{
					$bowlRs[] = $bowlChar;
				}
//				$_str = implode(',',array_keys($_BowlDetailsOnThatDate));
				$_str = implode('+',$bowlRs);
			}
		}
		//$dates[$i] <- 2014-01-15, etc
		
//		$Row .= '<td class="bowelReport">'.print_r($_BowlDetailsOnThatDate,true).'&nbsp</td>';
		$Row .= '<td class="bowelReport">'.$_str.'&nbsp</td>';
		
		
		/*		
		debug_r($bowlDetails[$dates[$i]]);
		$_BowlDetailsOnThatDate = $bowlDetails[$dates[$i]];
		if(is_array($_BowlDetailsOnThatDate )){
			$_str = implode(',',$_BowlDetailsOnThatDate);
		}
		echo $dates[$i].'==== ['.$_str.']<br/>';
		*/
	}
		
		if($r_withRemarks == 1){
			$remarksDetails = $bowelStudentData['Remarks'][$details['UserID']];
			$Row .= '<td class="bowelReport" style="text-align:left !important">';
			if($remarksDetails != NULL){
				ksort($remarksDetails);
				foreach($remarksDetails as $date=>$remarks){
					$displayDate = date("j/n", strtotime($date));
					foreach($remarks as $remark){
						if($remark != ''){
							$Row .= "$displayDate: $remark<br/>";
						}
					}
				}
			}else{
				$Row .= '&nbsp;';
			}
			$Row .= '</td>';
		}

	echo $Row.'</tr>';
}
echo '</table>';
			

		$printHTML = <<<HTML
		</td></tr>
		</table>
		</body>
		</html>
HTML;
		echo $printHTML;
	break;
	
	case 'export':
	    if($sys_custom['medical']['accessRight']['PrintAndExport'] && !($objMedical->isSuperAdmin($_SESSION['UserID'])|| $objMedical->checkAccessRight($_SESSION['UserID'], "BOWEL_REPORTEXPORT"))){
	        header("Location: /");
	        exit();
	    }
		//		echo $objMedical->getSleepReport($sleepStudentData, $reasonIDList, $outputFormat = 'csv');
	# should place on lib
		global $PATH_WRT_ROOT;
				include_once($PATH_WRT_ROOT."includes/libexporttext.php");
				$lexport = new libexporttext();
				$exportColumn = array();
								
				$header[] = $Lang['medical']['sleep']['searchMenu']['form'];
				$header[] = $Lang['medical']['bowel']['importRemarksTable']['studentName'];
				$header[] = $Lang['medical']['sleep']['searchMenu']['sleep'];
				$dayHeader = array();
					foreach($dates as $date){
						$weekday = date("w", strtotime($date));
						$day = (int)trim($date[8].$date[9]);
						$month = (int)trim($date[5].$date[6]);
						$dayHeader[] = $day.'/'.$month.$Lang['General']['DayType4'][$weekday];	
					} 
					foreach($dayHeader as $dayCol ){
						$header[] = $dayCol;
					}		
					
					if($r_withRemarks == 1){
						$header[] = $Lang['medical']['report_meal']['search']['itemCheckbox']['remarks'];
					}		
					
				$exportColumn = $header;

				
				$RowDetails = array();
				foreach($StudentList as $studentId=>$details)
				{
					/*
					 array(4) {
				  ["Name"]=>
				  string(14) "九甲_學生_2"
				  ["UserID"]=>
				  string(4) "1972"
				  ["stayOverNight"]=>
				  NULL
				  ["ClassName"]=>
				  string(6) "九甲"
				}
				*/
					$isStay ='';
					if($details['stayOverNight'] == 1 )
					{
					   $isStay = 1; 
					} 
					else 
					{
						$isStay = 2;
					}
					$Row[] = $details['ClassName'];
					$Row[] = $details['Name'];
					$Row[] = $Lang['medical']['sleep']['search']['sleepOption'][$isStay];
					
					//get the all the bowl detail for a userid
					$bowlDetails =$bowelStudentData[$details['UserID']];
					//debug_r($bowelStudentData); 
					$_str = '';
				//	debug_r($dates);
				//	debug_r($bowlDetails);
				//	foreach ($date1s as $y => $item){
						//debug_r($y);
				
					for($i = 0,$iMax = count($dates);$i < $iMax; $i++){
				//		debug_r($dates[$i]);	
							$_str = '';
						$_BowlDetailsOnThatDate =$bowlDetails[$dates[$i]];
				//		debug_r($_BowlDetailsOnThatDate);
						if(is_array($_BowlDetailsOnThatDate )){
							$bowlRs = array();
							foreach($_BowlDetailsOnThatDate as $bowlChar=>$bowlid){
								if($bowlid > 1){
									$bowlRs[] = $bowlid.$bowlChar;
								}else 
								{
									$bowlRs[] = $bowlChar;
								}
				//				$_str = implode(',',array_keys($_BowlDetailsOnThatDate));
								$_str = implode('+',$bowlRs);
							}
						}
						//$dates[$i] <- 2014-01-15, etc
						
						$Row[] = $_str;
							
						
					}
				
					if($r_withRemarks == 1){
						$remarksRow = "";
						$remarksDetails = $bowelStudentData['Remarks'][$details['UserID']];
						if($remarksDetails != NULL){
							ksort($remarksDetails);
							foreach($remarksDetails as $date=>$remarks){
								$displayDate = date("j/n", strtotime($date));
								foreach($remarks as $remark){
									if($remark != ''){
										$remarksRow .= "$displayDate: $remark\n";
									}
								}
							}
						}
						$Row[] = trim($remarksRow);
					}	
					
					$RowDetails[] = $Row;
					unset($Row);	
				}

//				debug_r($RowDetails);
				$exportContent = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($RowDetails, $exportColumn, "\t", "\r\n", "\t", 0, "11", $includeLineBreak=1);
				$lexport->EXPORT_FILE('export.csv', $exportContent);
	exit();
	break;	
	default:
	$StyleForTable = <<<HTML
	<style>
	 .bowelReport{
		min-width:70px !important;
		text-align:center !important;
	 }
	 </style>
HTML;
echo $StyleForTable; 
		//////////////// INPUT CHECKING /////////

echo '<div class="Conntent_tool">';
if(!$sys_custom['medical']['accessRight']['PrintAndExport'] || ($objMedical->isSuperAdmin($_SESSION['UserID'])|| $objMedical->checkAccessRight($_SESSION['UserID'], "BOWEL_REPORTPRINT")))
    echo '<a class="print tablelink" name="printBtn" id="printBtn">'.$Lang['Btn']['Print'].'</a>';
if(!$sys_custom['medical']['accessRight']['PrintAndExport'] || ($objMedical->isSuperAdmin($_SESSION['UserID'])|| $objMedical->checkAccessRight($_SESSION['UserID'], "BOWEL_REPORTEXPORT")))
	echo '<a class="export tablelink" name="exportBtn" id="exportBtn">'.$Lang['Btn']['Export'].'</a>';
	echo '</div><br /><br/><br/>';
	
	$resultHeader = 
	'<div style="width:100%;border-bottom: 2px black solid;font-weight:bold;">'.
	$Lang['medical']['report_studentBowel']['report2']. ':&nbsp;' . 
	$disStudentType.
	'&nbsp;(&nbsp;'.$startDate.' '.$Lang['StaffAttendance']['To'].' '.$endDate.' )'.
	'</div>';

	echo $resultHeader;

//		echo $objMedical->getSleepReport($sleepStudentData, $reasonIDList, $outputFormat = 'html');


	echo "<br/><br/><br/>";



		
	echo '<table class="common_table_list_v30">';
echo '<thead><tr class="tabletop">';
echo '<th class="bowelReport">'.$Lang['medical']['sleep']['searchMenu']['form'].'</th><th class="bowelReport">'.$Lang['medical']['bowel']['importRemarksTable']['studentName'].'</th><th class="bowelReport">'.$Lang['medical']['sleep']['searchMenu']['sleep'].'</th>';
$dayHeader = array();
foreach($dates as $date){
	$weekday = date("w", strtotime($date));
	$day = (int)trim($date[8].$date[9]);
	$month = (int)trim($date[5].$date[6]);
	$dayHeader[] = $day.'/'.$month.'<br/>'.$Lang['General']['DayType4'][$weekday];		
} 
foreach($dayHeader as $dayCol ){
	echo '<th class="bowelReport">'.$dayCol.'</th>';
}

if($r_withRemarks == 1){
	echo '<th class="bowelReport">'.$Lang['medical']['report_meal']['search']['itemCheckbox']['remarks'].'</th>';
}

echo '</tr>';
foreach($StudentList as $studentId=>$details)
{
	/*
	 array(4) {
  ["Name"]=>
  string(14) "九甲_學生2"
  ["UserID"]=>
  string(4) "1972"
  ["stayOverNight"]=>
  NULL
  ["ClassName"]=>
  string(6) "九甲"
}
*/
//error_log("details -->".print_r($details,true)."<----".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/aaa.txt");
	$isStay ='';
	if($details['stayOverNight'] == 1 )
	{
	   $isStay = 1; 
	} 
	else 
	{
		$isStay = 2;
	}
	$Row = '<tr><td class="bowelReport">'.$details['ClassName'].'</td><td class="bowelReport">'.$details['Name'].'</td><td class="bowelReport">'.$Lang['medical']['sleep']['search']['sleepOption'][$isStay].'</td>';
	//get the all the bowl detail for a userid
	$bowlDetails =$bowelStudentData[$details['UserID']];
	//debug_r($bowelStudentData); 
	$_str = '';
//	debug_r($dates);
//	debug_r($bowlDetails);
//	foreach ($date1s as $y => $item){
		//debug_r($y);

	for($i = 0,$iMax = count($dates);$i < $iMax; $i++){
//		debug_r($dates[$i]);	
			$_str = '';
		$_BowlDetailsOnThatDate =$bowlDetails[$dates[$i]];
//		debug_r($_BowlDetailsOnThatDate);
		if(is_array($_BowlDetailsOnThatDate )){
			$bowlRs = array();
				ksort($_BowlDetailsOnThatDate); 
			foreach($_BowlDetailsOnThatDate as $bowlChar=>$bowlid){
				if($bowlid > 1){
					$bowlRs[] = $bowlid.$bowlChar;
				}else 
				{
					$bowlRs[] = $bowlChar;
				}
//				$_str = implode(',',array_keys($_BowlDetailsOnThatDate));
				$_str = implode('+',$bowlRs);
			}
		}
		//$dates[$i] <- 2014-01-15, etc
		
		$Row .= '<td class="bowelReport"><a href=\'#\' data-date="'.$dates[$i].'" data-reasonlist="'.$str_reasonID.'" data-studentid="'.$details['UserID'].'" class="dateDetail">'.$_str.'&nbsp;</a></td>';
		
		
		/*		
		debug_r($bowlDetails[$dates[$i]]);
		$_BowlDetailsOnThatDate = $bowlDetails[$dates[$i]];
		if(is_array($_BowlDetailsOnThatDate )){
			$_str = implode(',',$_BowlDetailsOnThatDate);
		}
		echo $dates[$i].'==== ['.$_str.']<br/>';
		*/
	}

	if($r_withRemarks == 1){
		$remarksDetails = $bowelStudentData['Remarks'][$details['UserID']];//[$dates[$i]];
		$Row .= '<td class="bowelReport" style="text-align:left !important">';
		if($remarksDetails != NULL){
			ksort($remarksDetails);
			foreach($remarksDetails as $date=>$remarks){
				$displayDate = date("j/n", strtotime($date));
				foreach($remarks as $remark){
					if($remark != ''){
						$Row .= "$displayDate: $remark<br/>";
					}
				}
			}
		}else{
			$Row .= '&nbsp;';
		}
		$Row .= '</td>';
	}

	echo $Row.'</tr>';
}
echo '</table>';
			
			echo $linterface->Include_Thickbox_JS_CSS();		
		$script = <<<HTML
		<script>
		$('#exportBtn').click(function(){
			$('#reportForm').attr('action',"?t=reports.ajax.getBowelReport2&action=export");
			$('#reportForm').submit();
		});
		$('#printBtn').click(function(){
			$('#reportForm').attr('action',"?t=reports.ajax.getBowelReport2&action=print");
			$('#reportForm').submit();		
		});
		
		$('a.dateDetail').click(function(){
				
				selectedDate = $(this).attr('data-date');
				selectedReasonList = $(this).attr('data-reasonlist');
				selectedStudentId = $(this).attr('data-studentid');
				load_dyn_size_thickbox_ip("{$Lang['medical']['bowel']['tableHeader']['Detail']}", 'onloadThickBox();', inlineID='', defaultHeight=250, defaultWidth=800);
		});
		function onloadThickBox() {
//			alert(parDate + ' == '+ parReasonList + ' == '+parStudentId);
			$('div#TB_ajaxContent').load(
				"?t=reports.ajax.getThickBoxForBowelReport2",{ 
					'r_studentId': selectedStudentId,
					'r_date': selectedDate,
					'r_reasonList': selectedReasonList
				},
				function(ReturnData) {
					
				}
			);
		}
		
		
		
		</script>
HTML;
		echo $script;
	break;
}
?>