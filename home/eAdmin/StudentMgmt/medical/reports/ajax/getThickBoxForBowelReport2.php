<?php

//using: 
//debug_r($r_studentId);
$thickBoxDate = $r_date;

$r_reasonList = explode(",",$r_reasonList);
//error_log("_REQUEST -->".print_r($_REQUEST,true)."<----".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/aaa.txt");
$studentDayInfo = $objMedical->getBowelReportData($r_reasonList,$r_studentId,$thickBoxDate,$thickBoxDate, $sleep='', $report = 2, $fromThickBox=true);
//list($year,$month,$day) = explode('-', $date);
//$month = str_pad($month, 2, "0", STR_PAD_LEFT);
$objUser = new libuser($r_studentId);
$disStudentType = $objUser->UserNameLang();
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$resultHeader = 
		'<div style="width:100%;border-bottom: 2px black solid;font-weight:bold;">'.
	$Lang['medical']['report_studentBowel']['report2'].':&nbsp;' . 
		$disStudentType.
		'&nbsp;(&nbsp;'.$thickBoxDate.'&nbsp;)'.
		'</div>';

$resultHTML = <<<HTML
	<br />
	{$resultHeader}
	<br />
	<form id="thickboxForm" name="thickboxForm">
	<table class="common_table_list_v30">
	<colgroup>
		<col style="width: 20%"/>
		<col style="width: 20%;min-width:80px"/>
		<col style="width: 60%"/>
	</colgroup>
		<thread>
			<th style="min-width:50px">{$Lang['medical']['studentLog']['time']}</th>
			<th style="min-width:80px">{$Lang['medical']['bowel']['tableHeader']['BowelCondition']}</th>
			<th style="">{$Lang['medical']['sleep']['tableHeader']['Remarks']}</th>
		</thread>
HTML;
echo $resultHTML;
	
	
	function sortByTime ($a,$b){
		return strcmp($a['Time'], $b['Time']);
	}
	usort($studentDayInfo[$r_studentId],"sortByTime");
	if(count($studentDayInfo)>0){
		foreach($studentDayInfo[$r_studentId] as $bowelRecord){
			$statusName = stripslashes($bowelRecord['StatusName']);
			$resultHTML = <<<HTML
				<tr>
					<td style="min-width:50px">{$bowelRecord['Time']}</td>
					<td style="min-width:80px">{$statusName}</td>
					<td style="">{$bowelRecord['Remarks']}&nbsp;</td>
				</tr>
HTML;
			echo $resultHTML;		
		}
	}
	else{
		$resultHTML = <<<HTML
		<tr><td colspan="4" align="center"  style="height:auto;">沒有相關資料。</td></tr>
HTML;
		echo $resultHTML;
		
	}	
$resultHTML = <<<HTML
	</table>
	</form>
HTML;
echo $resultHTML;
?>
<div id="editBottomDiv" class="edit_bottom_v30">
	<p class="spacer"></p>
	<?php echo $htmlAry['cancelBtn']; ?>
</div>