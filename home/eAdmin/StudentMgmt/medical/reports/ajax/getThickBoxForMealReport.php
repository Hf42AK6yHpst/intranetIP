<?php

//using:
/*
 *  2018-05-29 [Cameron]
 *      - fix: apply stripslashes to status
 */
//debug_r($r_studentId);
$thickBoxStudentId = $r_studentId;
$thickBoxStartDate = $r_startDate;
$thickBoxEndDate = $r_endDate;
$thickBoxStatusId = $r_statusId;
$thickBoxTimePeriod = $r_timePeriod;

$mealData = getMealReport2ThickboxData($thickBoxStartDate, $thickBoxEndDate, $thickBoxStudentId, $thickBoxStatusId, $thickBoxTimePeriod);

$data = array();
foreach ($mealData as $year => $d1) {
	foreach ($d1 as $month => $d2){
		foreach ($d2 as $day => $d3){
			foreach ($d3 as $period => $remarks){
				$data[] = array(
					'Date' => $year.'-'.$month.'-'.$day,
					'Period' => $period,
					'Remarks' => stripslashes($remarks)
				);
			}
		}
	}
}

$countData = count($data);


$statusRs = $objMedical->getAllMealStatus();
$statusArr = array();
foreach($statusRs as $r){
	$statusArr[$r['StatusID']] = $r['StatusName']; 
}

$objUser = new libuser($r_studentId);
$disStudentType = $objUser->UserNameLang();
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$resultHeader = 
		'<div style="width:100%;border-bottom: 2px black solid;font-weight:bold;">'.
		$Lang['medical']['report_meal']['search']['reportTypeSelect']['mealReport'].':&nbsp;'.
		$disStudentType.
		'&nbsp;(&nbsp;'.$thickBoxStartDate.' '.$Lang['StaffAttendance']['To'].' '.$thickBoxEndDate.'&nbsp;)'.
		'</div>';

$status = stripslashes($statusArr[$thickBoxStatusId]);
$resultHTML = <<<HTML
	<br />
	{$resultHeader}
	<br />
    {$Lang['medical']['meal_setting']['tableHeader']['Status']}: {$status} ({$Lang['medical']['general']['report']['total']} {$countData} {$Lang['medical']['report_sleep']['tableContent']['frequency']})
	<br style="clear:both"/>
	<form id="thickboxForm" name="thickboxForm">
	<table class="common_table_list_v30">
	<colgroup>
		<col style="width: 20%"/>
		<col style="width: 20%;min-width:80px"/>
		<col style="width: 60%"/>
	</colgroup>
		<thread>
			<th>{$Lang['medical']['report_meal']['searchMenu']['date']}</th>
			<th>{$Lang['medical']['report_meal']['searchMenu']['timePeriod']}</th>
			<th>{$Lang['medical']['meal']['tableHeader']['Remarks']}</th>
		</thread>
HTML;
echo $resultHTML;
	
	foreach ($data as $d){
			$resultHTML = <<<HTML
				<tr>
					<td>{$d['Date']}</td>
					<td>{$Lang['medical']['report_meal']['search']['timePeriodOption'][$d['Period']]}</td>
					<td style="">{$d['Remarks']}&nbsp;</td>
				</tr>
HTML;
			echo $resultHTML;	
	}	
			
$resultHTML = <<<HTML
	</table>
	</form>
HTML;
echo $resultHTML;
?>
<div id="editBottomDiv" class="edit_bottom_v30">
	<p class="spacer"></p>
	<?php echo $htmlAry['cancelBtn']; ?>
</div>