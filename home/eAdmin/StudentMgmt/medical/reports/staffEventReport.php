<?php
//update by :  
/*
 * 	Log
 * 	
 *  2020-02-14 Cameron
 *          - fix: check if $objMedical is defined or not to avoid direct access this page via url
 * 
 *  2018-12-31 Cameron
 *      - create this file
 */

if (!isset($objMedical) || ! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'STAFFEVENT_REPORT') || ! $plugin['medical_module']['staffEvents']) {
    header("Location: /");
    exit();
}

$CurrentPage = "ReportStaffEvent";
$CurrentPageName = $Lang['medical']['menu']['report_staff_event'];
$TAGS_OBJ[] = array($Lang['medical']['menu']['report_staff_event'], "", 0);

$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();

//$objDB = new libdb();
//$menuOption = array();

$defaultStartDate = date('Y-m-d', strtotime("-1 week"));	// 1 week before
$defaultEndDate = date('Y-m-d');                            // Use today as default

// date range filter
$startDate = $linterface->GET_DATE_PICKER($Name="startDate",$defaultStartDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum");
$endDate = $linterface->GET_DATE_PICKER($Name="endDate",$defaultEndDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum");

// staff name filter
$allStaff = $objMedical->getMultipleSelectStaff();

$staffNameHTML  = '<input type="button"  name="selectAllStaff" id="selectAllStaff" class="formbutton_v30 print_hide selectAll" value="'.$Lang['Btn']['SelectAll'].'">';
$staffNameHTML .= '<span class="form_sep_title">';
$staffNameHTML .= $Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'];
$staffNameHTML .= '</span>';

// staff event type filter
$staffEventTypeHTML = $objMedical->getAllStaffEventTypes();


$displayMethodHTML = $Lang['medical']['general']['displayMethod']['orderBy']['title'] . '<input type="radio" name="displayOrder" id="displayOrderDesc" value="desc" checked/><label for="displayOrderDesc">' . $Lang['medical']['general']['displayMethod']['orderBy']['desc'] . '</label>';
$displayMethodHTML .= '&nbsp;<input type="radio" name="displayOrder" id="displayOrderAsc" value="asc" /><label for="displayOrderAsc">' . $Lang['medical']['general']['displayMethod']['orderBy']['asc'] . '</label>';


$linterface->LAYOUT_START();
?>
<script language="JavaScript" src="<?php echo $PATH_WRT_ROOT;?>templates/jquery/jquery-1.8.3.min.js"></script>
<style>
.tablelink{
	cursor:pointer;
}
.staffEventTypeTr{
	vertical-align: top;
}
</style>
<form id="reportForm" action="" name="form" target="_blank" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
			<table  border="0" cellspacing="0" cellpadding="5" width="100%">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['General']['Date']; ?></td>
					<td class="tabletext" width="70%"><?php echo $startDate.$Lang['General']['To'].'&nbsp;' .$endDate; ?></td>
				</tr>

				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['reportStaffEvent']['StaffType']; ?></td>
					<td class="tabletext" width="70%">
						<input type="checkbox" class="staffType" name="teachingStaff" id="teachingStaff" value="1" checked><label for="teachingStaff"><?php echo $Lang['Identity']['TeachingStaff'];?></label>
						<input type="checkbox" class="staffType" name="nonTeachingStaff" id="nonTeachingStaff" value="1" checked><label for="nonTeachingStaff"><?php echo $Lang['Identity']['NonTeachingStaff'];?></label>
					</td>
				</tr>
				
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['Identity']['Staff']; ?></td>
					<td class="tabletext" width="70%">
						<span id="staffListArea">
							<?php echo $allStaff; ?>
						</span>
						<?php echo $staffNameHTML;?>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['staffEvent']['EventType']; ?></td>
					<td class="tabletext" width="70%">
						<?php echo $staffEventTypeHTML; ?>
					</td>
				</tr>
				
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['general']['displayMethod']['title']; ?></td>
					<td class="tabletext" width="70%"><?php echo $displayMethodHTML; ?></td>
				</tr>
				
				<tr>
					<td height="1" class="dotline" colspan="2"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td height="1" align="center" colspan="2"><?= $linterface->GET_ACTION_BTN($Lang['medical']['meal']['button']['search'], 'button',$ParOnClick="", $ParName="r_search")?>&nbsp;</td>
				</tr>
			</table>
			<div id="viewResult">
			</div>
		</td>
	</tr>
</table>
</form>
<script>
$(document).ready(function(){
	$('#checkAllStaffEventType').click(function(){
		$('.level1').attr('checked', $(this).prop('checked'));
		$('.level2').attr('checked', $(this).prop('checked'));
	});
	$('.sub_item2, .sub_item1').click(function(){
		if(!$(this).prop('checked')){
			$('#checkAllStaffEventType').prop('checked', $(this).prop('checked'));
		}
	});
	$('.sub_item1').click(function(){
		var Level1ID = $(this).attr('data-Lev1ID');
		$('.level2_'+Level1ID).prop('checked', $(this).prop('checked'));
	});
	$('.sub_item2').click(function(){
		var Level1ID = $(this).attr('data-Lev1ID');
		$('#level1_'+Level1ID).prop('checked', $(this).prop('checked'));
	});
	
	$('.staffType').click(function(){
		if (!$('#teachingStaff').is(':checked') && !$('#nonTeachingStaff').is(':checked')) {
			$(this).attr("checked", !($(this).attr("checked")));
			alert("<?php echo $Lang['medical']['reportStaffEvent']['noStaffTypeSelected'];?>");
		}
		else {	
    		$.ajax({
    			url : "?t=reports.ajax.getStaffList",
    			type : "POST",
    			data : {
    				'teachingStaff': $('#teachingStaff').is(':checked') ? '1' : '0',
    				'nonTeachingStaff': $('#nonTeachingStaff').is(':checked') ? '1' : '0'
    			},
    			success : function(msg) {
    				$('#staffListArea').html(msg);
    			}
    		});
		}	
	});

	$(document).on('click','#selectAllStaff',function(){
		$('#staffID option').attr('selected',true);
	});
	
	$('#r_search').click(function(){
		if($('#staffID').val() == null){
			alert("<?php echo $Lang['medical']['reportStaffEvent']['noStaffSelected']?>");
			return false;
		}
		
		if(!checkDateValid($('#startDate').val(),$('#endDate').val())){
			alert("<?php echo $Lang['medical']['report_bowel']['wrongDatePeriod']?>");
			return false;
		}
				
		$.ajax({
			url : "?t=reports.ajax.getStaffEventReport",
			type : "POST",
			data : $('#reportForm').serialize(),
			success : function(msg) {
				$('#viewResult').html(msg);
			}
		});	
	});
});
function checkDateValid(start,end){
	// .replace() is for IE7 
	var _start = new Date(start.replace(/-/g,'/'));
	var _end = new Date(end.replace(/-/g,'/'));
	return start <= end;
}

</script>
<?
$linterface->LAYOUT_STOP();
?>