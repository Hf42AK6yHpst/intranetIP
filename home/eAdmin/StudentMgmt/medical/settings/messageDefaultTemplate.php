<?php
/*
 * 	using: 
 * 	Log
 * 	Date:
 */
//http://192.168.0.146:31002/home/common_choose/index.php?fieldname=student[]&page_title=SelectMembers&permitted_type=1
 
if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='NOTICE_SETTINGS')){
	header("Location: /");
	exit();
}
if(!$plugin['medical_module']['message']){
	header("Location: /");
	exit();
}

$CurrentPage = "SettingsNotice";

$TAGS_OBJ[] = array($Lang['medical']['menu']['settings_noticeRemarks'], "", 0);
$PAGE_NAVIGATION[] = array($Lang['medical']['menu']['settings_accessRight'], "?t=settings.accessRight");
$PAGE_NAVIGATION[] = array($i_Discipline_System_Group_Right_Navigation_New_Group, "");

$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();	


$objDB = new libdb();


$conds = "";
$sql = "SELECT
			Title AS Title, 
			Message AS Message, 
			RecordStatus AS RecordStatus,
			CONCAT('<input type=\'checkbox\' name=\'defaultID[]\' id=\'defaultID[]\' value=', DefaultID ,'>') as checkbox
		FROM 
			MEDICAL_DEFAULT_MAIL
		WHERE
			1=1
			$conds ";
$rs = $objDB->returnResultSet($sql);


#### Display Message START ####
switch($ShowMsg){
	case 'active|success':
		$Msg = $Lang['medical']['message_setting']['ReturnMessage']['Activate']['Success'];
		break;
	case 'active|fail':
		$Msg = $Lang['medical']['message_setting']['ReturnMessage']['Activate']['Failed'];
		break;
	case 'suspend|success':
		$Msg = $Lang['medical']['message_setting']['ReturnMessage']['Suspend']['Success'];
		break;
	case 'suspend|fail':
		$Msg = $Lang['medical']['message_setting']['ReturnMessage']['Suspend']['Failed'];
		break;
	case 'delete|success':
		$Msg = $Lang['medical']['message_setting']['ReturnMessage']['Delete']['Success'];
		break;
	case 'delete|fail':
		$Msg = $Lang['medical']['message_setting']['ReturnMessage']['Delete']['Failed'];
		break;
	case 'new|success':
		$Msg = $Lang['medical']['message_setting']['ReturnMessage']['Add']['Success'];
		break;
	case 'new|fail':
		$Msg = $Lang['medical']['message_setting']['ReturnMessage']['Add']['Failed'];
		break;
	case 'edit|success':
		$Msg = $Lang['medical']['message_setting']['ReturnMessage']['Edit']['Success'];
		break;
	case 'edit|fail':
		$Msg = $Lang['medical']['message_setting']['ReturnMessage']['Edit']['Failed'];
		break;
	default:
		$Msg = '';
}
#### Display Message END ####

$linterface->LAYOUT_START($Msg);

$c_ui = new common_ui();
$js_css = $c_ui->Include_JS_CSS();
echo $js_css;
?>
<style type="text/css">
<?php
include_once($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/medical/css/medical.css");
?>
</style>
<form method="get" name="form1" id="form1" action="index.php">

<div id="ContentTableDiv">

<div id="viewResult">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<colgroup>
			<col style="width:70%">
			<col style="width:30%">
		</colgroup>
		<thead>
			<tr>
				<td valign="bottom" colspan="2">
					<div class="Conntent_tool"><br style="clear:both">		
						<a href="javascript:js_handle_message_setting('new');" class="add_dim" title="<?=$Lang['medical']['meal_setting']['button']['new_category']?>">&nbsp;<?=$Lang['Btn']['New']?></a>
					</div>												
				</td>
            </tr>
            <tr class="table-action-bar">
				<td valign="bottom" colspan="2">
					<div class="common_table_tool">						
						<a href="javascript:js_handle_message_setting('activate');" class="tool_approve"><?=$Lang['Status']['Activate']?></a>						
						<a href="javascript:js_handle_message_setting('suspend');" class="tool_reject"><?=$Lang['Status']['Suspend']?></a>						
						<a href="javascript:js_handle_message_setting('edit');"><?=$Lang['Btn']['Edit']?></a>
						<a href="javascript:js_handle_message_setting('delete');" class="tool_delete"><?php echo $Lang['Btn']['Delete']; ?></a>
					</div>
				
				</td>
			</tr>
		</thead>
	</table>	
	<table width="95%" border="0" cellspacing="0" cellpadding="0" class="common_table_list">
		<colgroup>
			<col style="width:4%">
			<col style="width:40%">
			<col style="width:45%">
			<col style="width:10%">
			<col style="width:1%">
		</colgroup>
		<thead>
			<tr class="tabletop">
				<th class="tabletoplink">#</th>
				<th class="tabletoplink"><?php echo $Lang['medical']['message']['setting']['title']; ?></th>
				<th class="tabletoplink"><?php echo $Lang['medical']['message']['setting']['message']; ?></th>
				<th class="tabletoplink"><?php echo $Lang['medical']['message']['setting']['recordStatus']; ?></th>
 				<th class="tabletoplink"><input type="checkbox" id="checkAll"></th>
			</tr>
		</thead>
		<tbody>
		<?php
			if (count($rs)>0)
			{			
				$i=0;
				foreach($rs as $r): ?>
				<tr>
					<td><?php echo ++$i;?></td>
					<td><?php echo $r['Title']; ?></td>
					<td><?php echo $r['Message']; ?></td>
					<td><?php echo ($r['RecordStatus']=='1')? $Lang['medical']['message']['setting']['StatusOption']['InUse'] : $Lang['medical']['message']['setting']['StatusOption']['Suspend'] ; ?></td>
					<td><?php echo $r['checkbox']; ?></td>
				</tr>
		<?php endforeach;
			}
			else
			{?>
				<tr>
					<td class=tableContent align=center colspan="6"><br><?=$Lang['SysMgr']['Homework']['NoRecord']?><br><br></td>
				</tr>
		<?php
			}?>
		</tbody>
	</table>
<?php
	if (count($mealSetting)>0)
	{			
		print ' <span class="form_sep_title">'.
					$Lang['medical']['meal_setting']['tableSorting'].
				'</span>';
	}
?>			
	
</div>



</div>
<input type="hidden" name="t" value="settings.messageDefaultTemplateEdit">
<input type="hidden" id="Msg" name="Msg" value="">
<input type="hidden" id="action" name="action" value="new">
</form>


<?php
$linterface->LAYOUT_STOP();
?>
<script>
$('#checkAll').click(function(){
	$('input[name^="defaultID"]').attr('checked',$(this).attr('checked'));
});

function js_handle_message_setting(action){
	$('#action').val(action);
	switch(action){
	case 'new':
		break;
	case 'activate':
	case 'suspend':
	case 'delete':
		if($('input[name^="defaultID"]:checked').length == 0){
			alert(globalAlertMsg2);	// check at least one item	
			return;
		}
		break;
	case 'edit':
		if($('input[name^="defaultID"]:checked').length != 1){
			alert(globalAlertMsg1);	// select only one
			return;
		}
		break;
	default:
		alert('error in js');
		return;
	}
	$('#form1').submit();
}
</script>
