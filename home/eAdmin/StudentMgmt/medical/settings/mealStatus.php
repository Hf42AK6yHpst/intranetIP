<?php
/*
 * 	using: 
 * 	Log
 * 	Date:
 */
//http://192.168.0.146:31002/home/common_choose/index.php?fieldname=student[]&page_title=SelectMembers&permitted_type=1
 
if( (!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='MEAL_SETTINGS')) || (!$plugin['medical_module']['meal']) ){
	header("Location: /");
	exit();
}

$CurrentPage = "SettingsMealStatus";

$TAGS_OBJ[] = array($Lang['medical']['menu']['settings_meal'], "", 0);
$PAGE_NAVIGATION[] = array($Lang['medical']['menu']['settings_accessRight'], "?t=settings.accessRight");
$PAGE_NAVIGATION[] = array($i_Discipline_System_Group_Right_Navigation_New_Group, "");

$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();		
$linterface->LAYOUT_START($Msg);

$c_ui = new common_ui();
$js_css = $c_ui->Include_JS_CSS();
echo $js_css;
?>
<style type="text/css">
<?php
include_once($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/medical/css/medical.css");
?>
</style>
<form method="post" name="form1" action="index.php">
<!--Status Name : <input type="text" name="r_name">-->
<input type="hidden" name="t" value="settings.mealStatusSave">
<?php
//	echo $linterface->GET_ACTION_BTN($button_submit, "submit");
?>
<div id="ContentTableDiv">
<?php
	include_once("mealStatusList.php");
?>
</div>
</form>

<?php
$linterface->LAYOUT_STOP();
?>
<script>
//var curStatusID = "";
var colorInitFlag = 0; //control the color picking init color or not


function js_show_meal_setting_add_edit_layer(action) {
	var pass = true;
	var selMealSettingID="";
	switch(action)
	{
		case "add":
			break;
		case "edit":
			if($('input[name="MealSettingID\[\]"]:checked').length == 0)
			{				
				alert(globalAlertMsg1);	// select only one
			    pass = false;
			}
			else if($('input[name="MealSettingID\[\]"]:checked').length > 1)
			{
				alert(globalAlertMsg1);	// select only one
			    pass = false;
			}		    
		    else
		    {		    	 
		    	selMealSettingID = $('input[name="MealSettingID\[\]"]:checked').val();
		    }
			break;
	}
	if (pass)
	{
		Hide_Return_Message();		
		js_Show_ThickBox('<?=$Lang['Btn']['Edit']?>',350, 650);
		$.post(
			"index.php?t=settings.ajax.ajax_get_meal_setting_add_edit_layer", 
			{ 
				MealSettingID: selMealSettingID
			},
			function(ReturnData)
			{
				$('#TB_ajaxContent').html(ReturnData);
			}
		);
	}
}

function Check_MealSetting_Form() {
	var StatusID 		= Trim($('#StatusID').val());
	var StatusName 		= Trim($('#StatusName').val());
	var StatusCode 		= Trim($('#StatusCode').val());
	var StatusColor 	= Trim($('#StatusColor').val());
	var RecordStatus	= $('input[name=RecordStatus]:checked','#MealSettingForm').val();
	var IsDefault 		= 0;
	if ($('#IsDefault').is(":checked"))
		IsDefault 		= Trim($('#IsDefault').val());
		
	if (isMandatoryTextPass() && isEmptySelection()) {

		Block_Thickbox();
		
		// Add or Edit
		var action = '';
		var actionScript = '?t=settings.mealStatusSave';
		if (StatusID == '')
		{			
			action = "new";
		}
		else
		{			
			action = "update";
		}
		
		var checkScript = '?t=settings.ajax.ajax_check_meal_duplicate';
		$.post(
			checkScript,
			{
				StatusID: StatusID,
				StatusName: StatusName,
				StatusCode: StatusCode
			},
			function(ReturnData)
			{
				if(ReturnData == 'pass')
				{
					$.post(
						actionScript, 
						{ 
							StatusName: StatusName,				
							StatusCode: StatusCode,
							StatusColor: StatusColor,
							RecordStatus: RecordStatus,
							IsDefault: IsDefault,
							StatusID: StatusID
						 },
						function(ReturnData)
						{
			//				js_show_meal_setting_add_edit_layer(StatusID);
							js_Reload_Content_Table();
							
							// Show system messages
							js_Show_System_Message(action, ReturnData);
							
							UnBlock_Thickbox();
							js_Hide_ThickBox();
							
							//$('#debugArea').html('aaa ' + ReturnData);
						}
					);
				}else{
					var errorStr = '';
					UnBlock_Thickbox();
					if(ReturnData & 1)
					{
						errorStr += '<?=$Lang['medical']['general']['duplicateCategory']?>\n'
					}
					if(ReturnData & 2)
					{
						errorStr += '<?=$Lang['medical']['general']['duplicateCode']?>\n'
					}
					alert(errorStr);
				}
			});
		
//		Save_MealSetting(mealSettingID);
	}
}
	
// Reload Content Table
function js_Reload_Content_Table()
{
	$('#ContentTableDiv').load(		
		"?t=settings.ajax.ajax_reload_meal_setting",
		function(ReturnData)
		{
			initThickBox();
		}
	);
}

// Load system message
function js_Show_System_Message(Action, Status)
{
	var returnMessage = '';
	
	switch(Action)
	{
		case "activate":
			returnMessage = (Status)? '<?=$Lang['medical']['meal_setting']['ReturnMessage']['Activate']['Success']?>' : '<?=$Lang['medical']['meal_setting']['ReturnMessage']['Activate']['Failed']?>';
			break;
		case "suspend":
			returnMessage = (Status)? '<?=$Lang['medical']['meal_setting']['ReturnMessage']['Suspend']['Success']?>' : '<?=$Lang['medical']['meal_setting']['ReturnMessage']['Suspend']['Failed']?>';
			break;
		case "new":
			returnMessage = (Status)? '<?=$Lang['medical']['meal_setting']['ReturnMessage']['Add']['Success']?>' : '<?=$Lang['medical']['meal_setting']['ReturnMessage']['Add']['Failed']?>';
			break;
		case "update":
			returnMessage = (Status)? '<?=$Lang['medical']['meal_setting']['ReturnMessage']['Edit']['Success']?>' : '<?=$Lang['medical']['meal_setting']['ReturnMessage']['Edit']['Failed']?>';
			break;
		case "delete":
			if (Status == 1) {			
				returnMessage = '<?=$Lang['medical']['meal_setting']['ReturnMessage']['Delete']['Success']?>';
			}
			else if (Status == "-1") {
				returnMessage = '<?=$Lang['medical']['meal_setting']['ReturnMessage']['Delete']['FailedWithReason']?>';
			}
			else {
				returnMessage = '<?=$Lang['medical']['meal_setting']['ReturnMessage']['Delete']['Failed']?>';
			}
							
//			returnMessage = (Status)? '<?=$Lang['medical']['meal_setting']['ReturnMessage']['Delete']['Success']?>' : '<?=$Lang['medical']['meal_setting']['ReturnMessage']['Delete']['Failed']?>';
			break;
		
	}
	Get_Return_Message(returnMessage);
}
	
function isMandatoryTextPass(){
	var isFieldValid = true;
	var msg = "";
//	$('.textbox').each(function(){
//		if ($(this).val().trim() == "") {
//			isFieldValid= false;
//			var errorMsg = '<span class="errorMsg alert_text"><?=$Lang['medical']['meal_setting']['AlertMessage']['FillText']?><br /></span>';
//			$(this).after(errorMsg);
//		}
//	});

	if ($.trim($('#StatusName').val()) == "") {		
		isFieldValid= false;		
		msg = "<?=$Lang['medical']['meal_setting']['Name']?>";
	}

	if ($.trim($('#StatusCode').val()) == "") {		
		isFieldValid= false;
		if (msg != "") {
			msg += ",";
		}		
		msg += "<?=$Lang['medical']['meal_setting']['Code']?>";
	}
	
	if (isFieldValid == false) {
		alert("<?=$Lang['medical']['meal_setting']['AlertMessage']['FillText']?>" + ": " + msg);		
	}
	
	return isFieldValid;
}

function isEmptySelection(){
	var isFieldValid = true;
	$('.radiobutton').each(function(){	
		var name = $(this).attr('name');
		if(!$('input[name="'+name+'"]').is(':checked')){
			isFieldValid = false;
//			var errorMsg = '<span class="errorMsg alert_text"><br /><?=$Lang['medical']['meal_setting']['AlertMessage']['SelectOption']?><br /></span>';
//			if(!$(this).parent().next().hasClass('errorMsg')){
//				$(this).parent().after(errorMsg);
//			}			
		}
	});
	
	if (isFieldValid == false) {	
		alert("<?=$Lang['medical']['meal_setting']['AlertMessage']['SelectOption'].": " .$Lang['medical']['meal_setting']['Status']?>");
	}
	return isFieldValid;
}

/*
function Save_MealSetting(mealSettingID) {
	$('#MealSettingForm').attr('action','index.php?t=settings.mealStatusSave');
	$('#MealSettingForm').submit();
}
*/

function js_handle_meal_setting(task)
{
	if($('input[name="MealSettingID\[\]"]:checked').length == 0)
	{
		alert(globalAlertMsg2);	// check at least one item	
	}
    else
    {
    	var cnf;
    	
    	switch(task)
    	{
    		case "activate":
    			cnf = confirm("<?=$Lang['medical']['meal_setting']['AlertMessage']['Activate']?>");
    			break;
    		case "delete":
    			cnf = confirm(globalAlertMsg3);
    			break;
    		case "suspend":
    			cnf = confirm(globalAlertMsg5);
    			break;
    	}
    	if (cnf)
    	{
		    var ids = [];
		    $('input[name="MealSettingID\[\]"]:checked').each(function() {
		        ids.push($(this).val());	        
		    });
		    ids = ids.join(",");
//	    	alert(ids);
			Hide_Return_Message();		
			$.post(
				"index.php?t=settings.ajax.ajax_handle_meal_setting", 
				{ 
					MealSettingID: ids,
					Task: task
				},
				function(ReturnData)
				{
//					if (task == "delete")
//					{
						js_Show_System_Message(task, ReturnData);
//					}
					js_Reload_Content_Table();
				}
			);
    	}
    }
}


$('#mealSettingsCheckAll').click(function(){
	$('.panelSelection').attr('checked', $(this).attr('checked'));
});


</script>
