<?php
//Using: 
//error_log("\n\nGetPost -->".print_r($_POST,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");
$lev1ID 		= trim($_POST['Lev1ID']);
if ($lev1ID)
{
	$lev2ID 		= trim($_POST['Lev2ID']);
	$lev2Name 		= trim( htmlentities($_POST['Lev2Name'], ENT_QUOTES, 'UTF-8'));
	$lev2Code 		= trim( htmlentities($_POST['Lev2Code'], ENT_QUOTES, 'UTF-8'));
	$recordStatus 	= trim(isset($_POST['RecordStatus'])?$_POST['RecordStatus']:0);
	$isDefault 		= trim(isset($_POST['IsDefault'])?$_POST['IsDefault']:0);

	$objDb = new libdb();
	$objDb->Start_Trans();
//error_log("\n\new db --><---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");	
	$objStudentLogLev2 = new studentLogLev2($lev2ID);
	$objStudentLogLev2->setLev2Name($lev2Name);
	$objStudentLogLev2->setLev2Code($lev2Code);
	$objStudentLogLev2->setLev1ID($lev1ID);
	$objStudentLogLev2->setRecordStatus($recordStatus);
	$objStudentLogLev2->setIsDefault($isDefault);
	
	if ($lev2ID)
	{
		$objStudentLogLev2->setLastModifiedBy($_SESSION['UserID']);
	}
	else
	{
		$objStudentLogLev2->setInputBy($_SESSION['UserID']);
	}
//error_log("\n\nbefore save -->".print_r($objStudentLogLev2,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");	
	$result = $objStudentLogLev2->save();
	
	if (!$result)
	{
		$objDb->RollBack_Trans();
	}
	$itemQty = $objStudentLogLev2->sumItemQty($lev1ID);
	if ($itemQty !== false)	// zero is allowed
	{
//		$sql = "UPDATE MEDICAL_STUDENT_LOG_LEV1 SET ItemQty=".$itemQty." WHERE Lev1ID=".$lev1ID;
////error_log("\n\nupdate sql -->".print_r($sql,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");		
//		$result2 = $objDb->db_db_query($sql);
		$objStudentLogLev1 = new studentLogLev1($lev1ID);
		$result2 = $objStudentLogLev1->updateItemQty($itemQty);
		if (!$result2)
		{
			$objDb->RollBack_Trans();
		}
	}
	
	$objDb->Commit_Trans();
	
	echo $result2;
}
else
{
	echo false;	
}
?>