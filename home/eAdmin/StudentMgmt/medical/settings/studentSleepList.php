<?php
/*
 * 	using: 
 */

$CurrentPage = "SettingsStudentSleep";

$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();	

$objSLog = new studentSleepLev1();
$filter = "DeletedFlag<>1";	// don't show marked deleted records
$orderBy = "StatusCode";
$studentSleepLev1 = $objSLog->getAllStatus($filter,$orderBy,$showDefault=true);
//debug_r($studentSleepLev1);	

if (!isset($c_ui))
{
	$c_ui = new common_ui();	
}

?>

<div id="viewResult">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<colgroup>
			<col style="width:70%">
			<col style="width:30%">
		</colgroup>
		<thead>
			<tr>
				<td valign="bottom" colspan="2">
					<div class="Conntent_tool"><br style="clear:both">				
<?php						
	$x = $linterface->Get_Thickbox_Link($c_ui->getThickBoxHeight(), $c_ui->getThickBoxWidth(), "add_dim", 
			$Lang['medical']['general']['button']['new_category'], "js_show_studentsleep_setting_add_edit_layer('add'); return false;",$InlineID="FakeLayer","&nbsp;".$Lang['Btn']['New']);
	echo $x;
?>	
					</div>												
				</td>
            </tr>
            <tr class="table-action-bar">
				<td valign="bottom" colspan="2">
					<div class="common_table_tool">						
						<a href="javascript:js_handle_studentsleep_setting('activate');" class="tool_approve"><?=$Lang['Status']['Activate']?></a>						
						<a href="javascript:js_handle_studentsleep_setting('suspend');" class="tool_reject"><?=$Lang['Status']['Suspend']?></a>						
						<a href="javascript:js_show_studentsleep_setting_add_edit_layer('edit');"><?=$Lang['Btn']['Edit']?></a>
						<a href="javascript:js_handle_studentsleep_setting('delete');" class="tool_delete"><?php echo $Lang['Btn']['Delete']; ?></a>
					</div>
				
				</td>
			</tr>
		</thead>
	</table>	
	<table width="95%" border="0" cellspacing="0" cellpadding="0" class="common_table_list">
		<colgroup>
			<col style="width:4%">
			<col style="width:30%">
			<col style="width:14%">
			<col style="width:18%">
			<col style="width:11%">
			<col style="width:7%">
			<col style="width:16%">
		</colgroup>
		<thead>
			<tr class="tabletop">
				<th class="tabletoplink">#</th>
				<th class="tabletoplink"><?php echo $Lang['medical']['general']['tableHeader']['Category']; ?></th>
				<th class="tabletoplink"><?php echo $Lang['General']['Code']; ?></th>
				<th class="tabletoplink"><?php echo $Lang['medical']['studentsleep_setting']['tableHeader']['ItemQty']; ?></th>
				<th class="tabletoplink"><?php echo $Lang['General']['Status2']; ?></th>	
				<th class="tabletoplink"><?php echo $Lang['medical']['meal_setting']['tableHeader']['Default']; ?></th>		
				<th class="tabletoplink"><?php echo $Lang['medical']['sleep_setting']['tableHeader']['DefaultLev2']; ?></th>				
 				<th class="tabletoplink"><input type="checkbox" name="studentSleepSettingsCheckAll" id="studentSleepSettingsCheckAll"></th>
			</tr>
		</thead>
		<tbody>
		<?php
			if (count($studentSleepLev1)>0)
			{			
				$i=0;
				foreach( (array)$studentSleepLev1 as $rs): ?>

				<tr>
					<td><?php echo ++$i;?></td>
					<td><?php echo '<div class="colorBoxStyle" style="background-color:'. stripslashes($rs['Color']) .'; float: left;">&nbsp;</div>&nbsp;'; ?><a href="index.php?t=settings.studentSleepLev2&StatusID=<?=$rs['StatusID']?>"><?php echo ($rs['StatusName'] ? stripslashes($rs['StatusName']) : "--"); ?></a></td>
					<td><?php echo ($rs['StatusCode'] ? stripslashes($rs['StatusCode']) : "--"); ?></td>
					<td><?php echo (($rs['ItemQty']>=0) ? stripslashes($rs['ItemQty']) : "--"); ?></td>
					<td><?php echo ($rs['RecordStatus'] ? $Lang['medical']['general']['StatusOption']['InUse'] : $Lang['medical']['general']['StatusOption']['Suspend']); ?></td>
					<td><?php echo ($rs['IsDefault'] ? $linterface->Get_Tick_Image() : "&nbsp;"); ?></td>
					<td><?php echo ($rs['DefaultReasonName'] ? $rs['DefaultReasonName'] : "&nbsp;"); ?></td>
					<td><input type="checkbox" name="StudentSleepStatusID[]" id="StudentSleepStatusID_<?=$i?>" value=<?php echo $rs['StatusID']; ?> class="panelSelection"></td>
				</tr>
		<?php endforeach;
			}
			else
			{?>
				<tr>
					<td class=tableContent align=center colspan="6"><br><?=$Lang['SysMgr']['Homework']['NoRecord']?><br><br></td>
				</tr>
		<?php
			}?>
		</tbody>
	</table>
<?php
	if (count($studentSleepLev1)>0)
	{			
		print ' <span class="form_sep_title">'.
					$Lang['medical']['general']['tableSorting'].
					'<br />'.
					$Lang['medical']['report_sleep']['reminder1'].
					
				'</span>';
	}
?>			

</div>
