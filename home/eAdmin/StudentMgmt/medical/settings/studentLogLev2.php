<?php
/*
 * 	using: 	
 * 	Log
 * 	Date:
 * 
 *  2018-05-29 [Cameron]
 *      - fix: apply stripslashes to Lev1Name
 */


if( (!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='STUDENTLOG_SETTINGS')) || (!$plugin['medical_module']['studentLog']) ){
	header("Location: /");
	exit();
}

$lev1ID = $_REQUEST["Lev1ID"];
if (!$lev1ID)
{
	$errMsg = 'Null StudentLogLev1ID!'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
  	alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
	
	header("Location: /");
	exit();
}
else
{
	$objLev1 = new studentLogLev1($lev1ID);
	$lev1Name = stripslashes($objLev1->getLev1Name());
}


$CurrentPage = "SettingsStudentLog";


$TAGS_OBJ[] = array($Lang['medical']['menu']['settings_studentlog'], "", 0);
$PAGE_NAVIGATION[] = array('<span class="tablelink">'.$Lang['medical']['menu']['settings_studentlog'].'</span>', "?t=settings.studentLog");
$PAGE_NAVIGATION[] = array($lev1Name, "");
//debug_r($PAGE_NAVIGATION);

$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();		
$linterface->LAYOUT_START($Msg);

$c_ui = new common_ui();
$js_css = $c_ui->Include_JS_CSS();
echo $js_css;
?>
<style type="text/css">
<?php
include_once($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/medical/css/medical.css");
?>
</style>
<form method="post" name="form1" action="index.php">
<input type="hidden" name="t" value="settings.studentLogLev2Save">
	<table width="95%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
		</tr>
	</table>									

<div id="ContentTableDiv">
<?php
	include_once("studentLogLev2List.php");
?>
</div>
</form>

<?php
$linterface->LAYOUT_STOP();
?>
<script>

function js_show_studentlog_setting_add_edit_layer(action) {
	var pass = true;
	var selStudentLogLev2ID="";
	switch(action)
	{
		case "add":
			break;
		case "edit":
			if($('input[name="StudentLogLev2ID\[\]"]:checked').length == 0)
			{				
				alert(globalAlertMsg1);	// select only one
			    pass = false;
			}
			else if($('input[name="StudentLogLev2ID\[\]"]:checked').length > 1)
			{
				alert(globalAlertMsg1);	// select only one
			    pass = false;
			}		    
		    else
		    {		    	 
		    	selStudentLogLev2ID = $('input[name="StudentLogLev2ID\[\]"]:checked').val();
		    }
			break;
	}
	if (pass)
	{
		Hide_Return_Message();		
		js_Show_ThickBox('<?=$Lang['Btn']['Edit']?>',350, 650);
		$.post(
			"index.php?t=settings.ajax.ajax_get_studentloglev2_setting_add_edit_layer", 
			{ 
				StudentLogLev2ID: selStudentLogLev2ID,
				Lev1ID: <?=$lev1ID?>
			},
			function(ReturnData)
			{
				$('#TB_ajaxContent').html(ReturnData);
			}
		);
	}
}

function Check_StudentLogSetting_Form() {
	var Lev2ID 			= Trim($('#Lev2ID').val());
	var Lev2Name 		= Trim($('#Lev2Name').val());
	var Lev2Code 		= Trim($('#Lev2Code').val());
	var Lev1ID 			= <?=$lev1ID?>;
	var RecordStatus	= $('input[name=RecordStatus]:checked','#StudentLogLev2Form').val();
	var IsDefault 		= 0;
	if ($('#IsDefault').is(":checked"))
		IsDefault 		= Trim($('#IsDefault').val());

	if (isMandatoryTextPass() && isEmptySelection()) {

		Block_Thickbox();
		
		// Add or Edit
		var action = '';
		var actionScript = '?t=settings.studentLogLev2Save';
		if (Lev2ID == '')
		{			
			action = "new";
		}
		else
		{			
			action = "update";
		}


		var checkScript = '?t=settings.ajax.ajax_check_studentloglev_duplicate_NameCode';
		$.post(
			checkScript,
			{
				StudentLogLev: '2',
				StudentLogLevID: Lev2ID,
				StudentLogLevName: Lev2Name,				
				StudentLogLevCode: Lev2Code,
				StudentLogLev1ID: Lev1ID
			},
			function(ReturnData)
			{
				if(ReturnData == 'pass')
				{
					$.post(
						actionScript, 
						{ 
							Lev2Name: Lev2Name,				
							Lev2Code: Lev2Code,
							RecordStatus: RecordStatus,
							IsDefault: IsDefault,
							Lev2ID: Lev2ID,
							Lev1ID: Lev1ID
						 },
						function(ReturnData)
						{
							js_Reload_Content_Table();
							js_Show_System_Message(action, ReturnData);
							UnBlock_Thickbox();
							js_Hide_ThickBox();
						}
					);
				}
				else if(ReturnData == '1')
				{
					UnBlock_Thickbox();
					alert('<?=$Lang['medical']['general']['duplicateCategory']?>');
				}
				else if(ReturnData == '2')
				{
					UnBlock_Thickbox();
					alert('<?=$Lang['medical']['general']['duplicateCode']?>');
				}
				else
				{
//alert(ReturnData);
					UnBlock_Thickbox();
					alert('<?=$Lang['medical']['general']['duplicateCategory'] . '\n' . $Lang['medical']['general']['duplicateCode']?>');
				}
			}
		);
	}
}
	
// Reload Content Table
function js_Reload_Content_Table()
{
	$('#ContentTableDiv').load(		
		"?t=settings.ajax.ajax_reload_studentloglev2_setting&Lev1ID=<?=$lev1ID?>",
		function(ReturnData)
		{
			initThickBox();
		}
	);
}

// Load system message
function js_Show_System_Message(Action, Status)
{
	var returnMessage = '';
	
	switch(Action)
	{
		case "activate":
			returnMessage = (Status)? '<?=$Lang['medical']['studentlog_setting']['ReturnMessage']['Activate']['Success']?>' : '<?=$Lang['medical']['studentlog_setting']['ReturnMessage']['Activate']['Failed']?>';
			break;
		case "suspend":
			returnMessage = (Status)? '<?=$Lang['medical']['studentlog_setting']['ReturnMessage']['Suspend']['Success']?>' : '<?=$Lang['medical']['studentlog_setting']['ReturnMessage']['Suspend']['Failed']?>';
			break;
		case "new":
			returnMessage = (Status)? '<?=$Lang['General']['ReturnMessage']['AddSuccess']?>' : '<?=$Lang['General']['ReturnMessage']['AddUnsuccess']?>';
			break;
		case "update":
			returnMessage = (Status)? '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>' : '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
			break;
		case "delete":
			if (Status == 1) {			
				returnMessage = '<?=$Lang['General']['ReturnMessage']['DeleteSuccess']?>';
			}
			else if (Status == "-1") {
//				returnMessage = '<?=$Lang['medical']['studentlog_setting']['ReturnMessage']['Delete']['FailedWithReason']?>';
				returnMessage = '<?=$Lang['medical']['general']['ReturnMessage']['Delete']['FailedWithReason']?>';
			}
			else {
				returnMessage = '<?=$Lang['General']['ReturnMessage']['DeleteUnsuccess']?>';
			}
			break;
		
	}
	Get_Return_Message(returnMessage);
}
	
function isMandatoryTextPass(){
	var isFieldValid = true;
	var msg = "";
	if ($.trim($('#Lev2Name').val()) == "") {
		isFieldValid= false;		
		msg = "<?=$Lang['General']['Name']?>";
	}
	
	if (isFieldValid == false) {
		alert("<?=$Lang['medical']['general']['AlertMessage']['FillText']?>" + ": " + msg);		
	}
	
	return isFieldValid;
}

function isEmptySelection(){
	var isFieldValid = true;
	$('.radiobutton').each(function(){	
		var name = $(this).attr('name');
		if(!$('input[name="'+name+'"]').is(':checked')){
			isFieldValid = false;
		}
	});
	
	if (isFieldValid == false) {	
		alert("<?=$Lang['medical']['general']['AlertMessage']['SelectOption'].": " .$Lang['General']['Status2']?>");
	}
	return isFieldValid;
}


function js_handle_studentlog_setting(task)
{
	if($('input[name="StudentLogLev2ID\[\]"]:checked').length == 0)
	{
		alert(globalAlertMsg2);	// check at least one item	
	}
    else
    {
    	var cnf;
    	switch(task)
    	{
    		case "activate":
    			cnf = confirm("<?=$Lang['medical']['general']['AlertMessage']['Activate']?>");
    			break;
    		case "delete":
    			cnf = confirm(globalAlertMsg3);
    			break;
    		case "suspend":
    			cnf = confirm(globalAlertMsg5);
    			break;
    	}
    	if (cnf)
    	{
		    var ids = [];
		    $('input[name="StudentLogLev2ID\[\]"]:checked').each(function() {
		        ids.push($(this).val());	        
		    });
		    ids = ids.join(",");

			Hide_Return_Message();		
			$.post(
				"index.php?t=settings.ajax.ajax_handle_studentloglev2_setting", 
				{ 
					StudentLogLev2ID: ids,
					Task: task
				},
				function(ReturnData)
				{
					js_Show_System_Message(task, ReturnData);
					js_Reload_Content_Table();
				}
			);
    	}
    }
}


$('#studentLogSettingsCheckAll').live('click',function(){
	$('.panelSelection').attr('checked', $(this).attr('checked'));
});


</script>
