<?php 
if( !$sys_custom['medical']['Handover'] || !($objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'HANDOVER_SETTINGS')) ){
    header("Location: /");
    exit();
}
include_once($PATH_WRT_ROOT."includes/libdb.php");

$idAry = $_POST['ItemID'];
if(!is_array($idAry)){
    $itemID = $idAry;
}
$itemName = trim(intranet_htmlspecialchars($_POST['itemName']));
$itemCode = trim(intranet_htmlspecialchars($_POST['itemCode']));
$itemStatus = $_POST['itemStatus'];
$li = new libdb();
$table = "MEDICAL_HANDOVER_ITEM";
switch($action){
    case 'new':
        $cols = "ItemName, ItemCode, ItemStatus, InputBy, DateInput";
        $sql = "INSERT INTO $table ($cols) VALUES ('$itemName', '$itemCode', '$itemStatus', '{$_SESSION['UserID']}', NOW())";
        break;
    case 'edit':
        $sql = "UPDATE $table SET
                    ItemName = '$itemName',
                    ItemCode = '$itemCode',
                    ItemStatus = '$itemStatus'
                WHERE
                    ItemID = '$itemID'";
        break;
    case 'activate':
        $sql = "UPDATE $table SET ItemStatus = '1', ModifiedBy = '{$_SESSION['USERID']}', DateModified = NOW()  WHERE ItemID IN ('" . implode("','", $idAry) . "')";
        break;
    case 'suspend':
        $sql = "UPDATE $table SET ItemStatus = '0', ModifiedBy = '{$_SESSION['USERID']}', DateModified = NOW() WHERE ItemID IN ('" . implode("','", $idAry) . "')";
        break;
    case 'delete':
        $sql = "UPDATE $table SET isDeleted = '1', ModifiedBy = '{$_SESSION['USERID']}', DateModified = NOW() WHERE ItemID IN ('" . implode("','", $idAry) . "')";
        break;
}
// debug_pr($sql);die();
$result = $li->db_db_query($sql);
header('location:?t=settings.handoverList');
?>