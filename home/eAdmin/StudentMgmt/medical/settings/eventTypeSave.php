<?php
/*
 * 	Log
 * 	
 * 	2017-06-15 Cameron
 * 		- create this file
 */
$eventTypeID 	= trim($_POST['EventTypeID']);
$eventTypeName 	= trim( htmlentities($_POST['EventTypeName'], ENT_QUOTES, 'UTF-8'));
$recordStatus 	= trim(isset($_POST['RecordStatus'])?$_POST['RecordStatus']:0);

$objEt = new eventType($eventTypeID);

$objEt->setEventTypeName($eventTypeName);
$objEt->setRecordStatus($recordStatus);
if ($eventTypeID)
{
	$objEt->setLastModifiedBy($_SESSION['UserID']);
}
else
{
	$objEt->setInputBy($_SESSION['UserID']);
}

$result = $objEt->save();
echo $result;
?>