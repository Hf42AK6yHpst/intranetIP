<?php
/*
 * Log
 *
 * 2018-03-27 Cameron
 * - create this file
 */
$eventTypeID = trim($_POST['EventTypeID']);
$eventTypeLev2ID = trim($_POST['EventTypeLev2ID']);
//$eventTypeLev2Name= trim(htmlentities($_POST['EventTypeLev2Name'], ENT_QUOTES, 'UTF-8'));
$eventTypeLev2Name= standardizeFormPostValue(trim($_POST['EventTypeLev2Name']));
$recordStatus = trim(isset($_POST['RecordStatus']) ? $_POST['RecordStatus'] : 0);
$isDefault = trim(isset($_POST['IsDefault']) ? $_POST['IsDefault'] : 0);

$objEt = new StaffEventLev2($eventTypeLev2ID);

$objEt->setEventTypeLev2Name($eventTypeLev2Name);
$objEt->setEventTypeID($eventTypeID);
$objEt->setRecordStatus($recordStatus);
$objEt->setIsDefault($isDefault);
if ($eventTypeID) {
    $objEt->setLastModifiedBy($_SESSION['UserID']);
} else {
    $objEt->setInputBy($_SESSION['UserID']);
}

$result = $objEt->save();
echo $result;
?>