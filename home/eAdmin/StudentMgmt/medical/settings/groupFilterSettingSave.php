<?php 
// Using:

include_once($PATH_WRT_ROOT."includes/libgroup.php");

if(!$objMedical->isSuperAdmin($_SESSION['UserID'])){
	header("Location: /");
	exit();
}

$objDB = new libdb();
$result = array();

#### SQL Safe START ####
$selectedCategory = IntegerSafe($selectedCategory);
for ($i = 0, $iMax = count($deletedCategoryName); $i < $iMax; $i++) {
	$deletedCategoryName[$i] = trim( htmlentities( $deletedCategoryName[$i] , ENT_QUOTES, 'UTF-8'));
}
#### SQL Safe END ####


#### Get All Category START ####
$objGroup = new libgroup();
$allCategoryList = $objGroup->returnAllCategory();
#### Get All Category END ####


#### Delete START ####
foreach ((array)$deletedCategoryName as $categoryName) {
	$sql = "DELETE FROM MEDICAL_GROUP_NAME_FILTER WHERE CategoryName='{$categoryName}'";
	$result[] = $objDB->db_db_query($sql);
}
#### Delete END ####


#### Save START ####
foreach ((array)$selectedCategory as $categoryID) {
	if($categoryID == 0){
		continue; // Modified category name
	}
	$categoryName = $allCategoryList[$categoryID];
	
	
	$sql = "SELECT COUNT(*) FROM MEDICAL_GROUP_NAME_FILTER WHERE CategoryName='{$categoryName}'";
	$rs = $objDB->returnResultSet($sql);
	
	if($rs[0]['COUNT(*)'] == 0){
		$sql = "INSERT INTO MEDICAL_GROUP_NAME_FILTER (
			CategoryName,
			InputBy,
			DateInput
		) VALUES (
			'{$categoryName}',
			'{$_SESSION['UserID']}',
			NOW()
		)
		";
		
		$result[] = $objDB->db_db_query($sql);
	}
}
#### Save END ####


#### Redirect START ####
if(in_array( false, $result)){
	$Msg = $Lang['medical']['group_filter_setting']['ReturnMessage']['Update']['Failed'];
}else{
	$Msg = $Lang['medical']['group_filter_setting']['ReturnMessage']['Update']['Success'];
}
header("Location: ?t=settings.groupFilterSetting&Msg={$Msg}");
exit;
?>