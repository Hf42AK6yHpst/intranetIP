<?php
/*
 * 	using: 
 * 	Log
 * 	Date:
 * 
 *  2018-05-29 [Cameron]
 *      - fix: apply stripslashes to Lev1Name
 *      
 * 	2017-09-19 [Cameron] 
 * 		- add label for radio button for easy choose item
 */
?> 
	<div id="debugArea"></div>
	<div class="edit_pop_board edit_pop_board_reorder">
<?php	$linterface->Get_Thickbox_Return_Message_Layer();?>
		<div class="edit_pop_board_write">
			<form id="StudentLog<?=$level?>Form" name="StudentLog<?=$level?>Form" method="post">
				<table class="form_table">
					
					<?php if($level == 'Lev2') { ?>
					<!-- Lev1 Name -->
					<tr>
						<td><?=$Lang['medical']['report_studentlog2']['tableHeader']['type']?></td>
						<td>
							<?php echo stripslashes($Lev1Name);?>
						</td>
					</tr>
					<?php } ?>
					
					<!-- Category Name -->
					<tr>
						<td><?=$Lang['General']['Name']?><span class="tabletextrequire">*</span></td>
						<td>
							<input id="<?=$level?>Name" name="<?=$level?>Name" type="text" class="textbox" value="<?=$levName?>" />
							<?php echo $linterface->Get_Thickbox_Warning_Msg_Div("NameWarningDiv");?>
						</td>
					</tr>

					<!-- Code -->
					<tr>
						<td><?=$Lang['General']['Code']?></td>
						<td>
							<input id="<?=$level?>Code" name="<?=$level?>Code" type="text" class="textbox" value="<?=$levCode?>" />
							<?php echo $linterface->Get_Thickbox_Warning_Msg_Div("CodeWarningDiv");?>
						</td>
					</tr>

					<!-- Status -->
					<tr>
						<td><?=$Lang['General']['Status2']?><span class="tabletextrequire">*</span></td>
						<td>
							<input type="radio" value="<?=$medical_cfg['general']['status']['InUse']['value']?>" name="RecordStatus" id='RecordStatusInUse' class="radiobutton" <?php echo $recordStatus == $medical_cfg['general']['status']['InUse']['value'] ? " checked=\"checked\"" : "";?>> <label for="RecordStatusInUse"><?=$medical_cfg['general']['status']['InUse']['lang']?></label>
							<input type="radio" value="<?=$medical_cfg['general']['status']['Suspend']['value']?>" name="RecordStatus" id='RecordStatusSuspend' class="radiobutton" <?php echo $recordStatus == $medical_cfg['general']['status']['Suspend']['value'] ? " checked=\"checked\"" : "";?>> <label for="RecordStatusSuspend"><?=$medical_cfg['general']['status']['Suspend']['lang']?></label>
							<?php echo $linterface->Get_Thickbox_Warning_Msg_Div("StatusWarningDiv");?>
						</td>							
					</tr>

		<?php
			// IsDefault apply to Level 2 only
			if ($level == "Lev2")
			{
		?>		
					<!-- Set as default -->
					<tr>
						<td><?=$Lang['medical']['general']['Default']?></td>
						<td>						
							<input id="IsDefault" name="IsDefault" type="checkbox" class="checkbox" value="1" <? echo $disableIsDefault; echo ( ($isDefault == "1") ? " checked" : "");?>/>
								<?php echo $linterface->Get_Thickbox_Warning_Msg_Div("IsDefaultWarningDiv");?>
						</td>
					</tr>

				<!-- Show remind message -->
				<?php
				if (!$enableIsDefault)
				{
				?>
					<tr>
						<td></td>
						<td style="float:left">						
							<?=$Lang['medical']['general']['RemindMessage']['DefaultHasSet']?>
						</td>
						<td></td>
					</tr>
				<?php
				}
			}				
				?>
					
<?php
//debug_r($recordStatus);
//debug_r($medical_cfg['general']['status']['Suspend']['value']);
?>					
				</table>
				<input type="hidden" id="<?=$level?>ID" name="<?=$level?>ID" value="<?=$levID?>">
				
				<table align="center" width="100%" border="0" cellpadding="2" cellspacing="0">
					<tr>
						<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
						<td width="80%">&nbsp;</td>
					</tr>
				</table>				
			</form>
		</div>
		
		<div class="edit_bottom">
			<p class="spacer"></p>
				<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "button", $onclick="Check_StudentLogSetting_Form()", $id="Btn_Save")?>&nbsp;
				<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", $onclick="js_Hide_ThickBox()", $id="Btn_Cancel")?>
			<p class="spacer"></p>
		</div>
	</div>
