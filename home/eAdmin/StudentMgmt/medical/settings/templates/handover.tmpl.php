<form name="form1" id='form1' action="?t=settings.handoverSave" method="post">
	
	<?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>
	
	<div class="form_table">
		<table class="form_table_v30">
			<tr>
				<td class="field_title"><span class="tabletextrequire">*</span><?php echo $Lang['medical']['handover']['itemName']?></td>
				<td><input type='text' name='itemName' id='itemName' value="<?php echo $item['ItemName']?>" required /></td>
			</tr>
			<tr>
				<td class="field_title"><?php echo $Lang['medical']['handover']['itemCode']?></td>
				<td><input type='text' name='itemCode' id='itemCode' value="<?php echo $item['ItemCode']?>" /></td>
				<?php // echo "<td><input type='text' name='itemCode' id='itemCode' value={$item['ItemCode']}/></td>"?>
			</tr>
			<tr>
				<td class="field_title"><span class="tabletextrequire">*</span><?php echo $Lang['medical']['handover']['itemStatus']?></td>
				<td>
					<input type='radio' name='itemStatus' id='itemStatus_activate' value='1' <?php echo ($checked_activate ? 'checked' : '');?> required /> <label for='itemStatus_activate'><?php echo $Lang['medical']['message']['setting']['StatusOption']['InUse'];?></label>
					<input type='radio' name='itemStatus' id='itemStatus_suspend' value='0' <?php echo ($checked_suspend ? 'checked' : '');?> required /> <label for='itemStatus_suspend'><?php echo $Lang['medical']['message']['setting']['StatusOption']['Suspend'];?></label>
				</td>
			</tr>
		</table>
		<?=$linterface->MandatoryField();?>
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?php echo $btnSubmit;?>
			<?php echo $btnBack;?>
			<p class="spacer"></p>
		</div>
	</div>
	<input type='hidden' name='ItemID' value='<?php echo $itemID;?>' />
	<input type='hidden' name='action' value='<?php echo $action;?>' />
	<input type='hidden' name='field' id='chkfield' />
</form>

<script type='text/javascript'>
$(document).ready(function(){
})
function checkForm(){
	if($('#itemName').val() == ''){
		alert('<?php echo $Lang['General']['PleaseFillIn'] . $Lang['medical']['handover']['itemName'];?>');
		return false;
	} else {
		$('#chkfield').val('Name');
		$.post('?t=settings.ajax.ajax_check_handover_duplicate', $('#form1').serialize(), function(data){
			if(data == 'duplicate'){
				alert('<?php echo $Lang['medical']['handover']['nameInUse'];?>');
				return false;
			} else {
				if($('input[name="itemStatus"]:checked').length != 1){
					alert('<?php echo $Lang['General']['PleaseSelect'] . $Lang['medical']['handover']['itemStatus'];?>');
					return false;
				}
				
				if($('#itemCode').val() != ''){
					$('#chkfield').val('Code');
					$.post('?t=settings.ajax.ajax_check_handover_duplicate', $('#form1').serialize(), function(data){
						if(data == 'duplicate'){
							alert('<?php echo $Lang['General']['JS_warning']['CodeIsInUse'];?>');
							return false;
						} else {
							$('#form1').submit();
						}
					});
				} else {
					$('#form1').submit();
				}
			}
		});
	}
}
</script>