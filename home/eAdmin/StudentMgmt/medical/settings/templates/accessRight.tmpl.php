<?php 
// using: 
/*
 *  2018-12-27 Cameron
 *      - add script to handle STAFFEVENT_DELETE right [case #F142033]
 */
?>

<form name="form1" action="?t=settings.accessRightSave" method="POST" onsubmit="return checkForm();">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="95%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>									
			<table width="88%" border="0" cellpadding="5" cellspacing="0" border="0">
				<tr>
					<td>
						<table width="100%" cellpadding="5" cellspacing="0" border="0">
							<colgroup>
								<col style="width:30%"/>
								<col style="width:70%"/>
							</colgroup>
							<tr>
								<td></td>
								<td align="right"><?=$linterface->GET_SYS_MSG('',$xmsg2) ?></td>
							</tr>
						</table>
						
						<!-- start  content-->
						<table width="100%" cellpadding="5" cellspacing="0" border="0" class="form_table_v30">
							<colgroup>
								<col style="width:30%"/>
								<col style="width:40%"/>
								<col style="width:30%"/>
							</colgroup>
							<tr>
								<td class="field_title"><?php echo $Lang['medical']['accessRight']['selectTitle']['setUserGroup']; ?><span class="tabletextrequire">*</span></td>
								<td colspan="2" align="right" >
									<input type="text" value="<?php echo stripslashes($acessRightGroupDetail['GroupTitle'])?>" name="r_name" id="r_name" maxlength="50">
								</td>
							</tr>
							<tr>
								<td class="field_title"><?php echo $Lang['medical']['accessRight']['selectTitle']['setUsers']; ?></td>
								<td>
									<span style="float:left;">
										<?php echo $user_selected; ?>
									</span>
									<?php echo $linterface->GET_BTN($button_select, "button", "javascript:newWindow('/home/common_choose/index.php?fieldname=r_group_user[]&page_title=SelectMembers&permitted_type=1&excluded_type=4', 16)");
										echo '<br/>';
										echo $button_remove_html;
										echo '<br style="clear:both;" / >';
										echo '<span class="form_sep_title">'.
												$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].
											'</span>';
									?>
								</td>
							</tr>

							<tr><td colspan="3" class="form_sep_title"><br/><i><?php echo $Lang['medical']['accessRight']['header']['option'];?></i>&nbsp;</td></tr>
							<?php
							echo $h;
							?>
						</table>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr>
								<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field ?></td>
								<td width="80%">&nbsp;</td>
							</tr>
						</table>
						<!-- end of content-->
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="center">
						<table width="88%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<!--<input type="hidden" name="t" value="settings.accessRightSave"> -->
									<input type="hidden" name="r_gid" value="<?php echo $groupID;?>">
									<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_reset, "reset","javascript:resetForm()")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='?t=settings.accessRightList'")?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
</form>
<script>
function resetForm(){
}

function checkForm()
{
	if($('#r_name').val() ==''){
		alert('請輸入用戶類別。');
		return false;	
	}
	
    obj = document.form1;
		
    if(obj.elements["r_group_user[]"].length != 0) {
    	checkOptionAll(obj.elements["r_group_user[]"]);
    	return true;
	}
    else{
        alert('請加入最少一位用戶。');
        return false;
    }
}

$(document).ready(function(){
<?php if ($plugin['medical_module']['staffEvents']):?>	
    $('#accessRight__STAFFEVENT_DELETE').click(function(){
    	if ($(this).is(':checked')) {
    		$('#DelSelf__STAFFEVENT').attr('checked', true);
    	}
    	else {
    		$('#DelSelf__STAFFEVENT').attr('checked', false);
    		$('#DelAll__STAFFEVENT').attr('checked', false);
    	}
    });

    $('input:radio[name="Delete__STAFFEVENT"]').click(function(){
    	$('#accessRight__STAFFEVENT_DELETE').attr('checked', true);
    });

    $('#accessRight__STAFFEVENT_VIEW').click(function(){
    	if (!$(this).is(':checked')) {
    		$('input:checkbox[id^="accessRight__STAFFEVENT_"][id!="accessRight__STAFFEVENT_SETTINGS"]').attr('checked', false);
    		$('input:checkbox[id^="accessRight__STAFFEVENT_"][id!="accessRight__STAFFEVENT_SETTINGS"][id!="accessRight__STAFFEVENT_VIEW"]').attr('disabled', true);
    		$('input:radio[name="Delete__STAFFEVENT"]').attr('checked', false);
    		$('input:radio[name="Delete__STAFFEVENT"]').attr('disabled', true);
    	}
    	else {    		
    		$('input:checkbox[id^="accessRight__STAFFEVENT_"][id!="accessRight__STAFFEVENT_SETTINGS"][id!="accessRight__STAFFEVENT_VIEW"]').attr('disabled', false);    		
    		$('input:radio[name="Delete__STAFFEVENT"]').attr('disabled', false);
    	}
    });
    
<?php endif;?>
	let reportsArr = ['accessRight__MEAL_REPORT', 'accessRight__BOWEL_REPORT', 'accessRight__STUDENTLOG_REPORT', 'accessRight__SLEEP_REPORT'];
    $.each(reportsArr, function(index, value){
    	let id = value;
    	if($("#" + id).is(':checked')){
    		$("#" + id + "PRINT, " + "#" + id + "EXPORT").removeAttr('disabled');
    	} else {
    		$("#" + id + "PRINT, " + "#" + id + "EXPORT").attr('disabled', true);
    	}
    });
	$('#accessRight__MEAL_REPORT, #accessRight__BOWEL_REPORT, #accessRight__STUDENTLOG_REPORT, #accessRight__SLEEP_REPORT').click(function(){
		let id = $(this).attr('id');
		if($(this).is(':checked')){
			$("#" + id + "PRINT, " + "#" + id + "EXPORT").removeAttr('disabled');
		} else {
			$("#" + id + "PRINT, " + "#" + id + "EXPORT").attr('disabled', true);
		}
	});
});

</script>