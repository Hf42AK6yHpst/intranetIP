<?php
//Using: 
//debug_r($_POST);
//error_log("\n\nGetPost -->".print_r($_POST,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");

$statusID 		= trim($_POST['StatusID']);
$statusName 	= trim( htmlentities($_POST['StatusName'], ENT_QUOTES, 'UTF-8'));
$statusColor 	= trim( htmlentities($_POST['StatusColor'], ENT_QUOTES, 'UTF-8'));
$statusCode 	= trim( htmlentities($_POST['StatusCode'], ENT_QUOTES, 'UTF-8'));
$barCode 		= trim( htmlentities($_POST['StatusBarCode'], ENT_QUOTES, 'UTF-8'));
$recordStatus 	= trim(isset($_POST['RecordStatus'])?$_POST['RecordStatus']:0);
$isDefault 		= trim(isset($_POST['IsDefault'])?$_POST['IsDefault']:0);

$objBowelStatus = new bowelStatus($statusID);

$objBowelStatus->setStatusName($statusName);
$objBowelStatus->setStatusCode($statusCode);
$objBowelStatus->setBarCode($barCode);
$objBowelStatus->setColor($statusColor);
$objBowelStatus->setRecordStatus($recordStatus);
$objBowelStatus->setIsDefault($isDefault);
if ($statusID)
{
	$objBowelStatus->setLastModifiedBy($_SESSION['UserID']);
}
else
{
	$objBowelStatus->setInputBy($_SESSION['UserID']);
}

$result = $objBowelStatus->save();
//debug_r($objBowelStatus);
//debug_r($result);
echo $result;
?>