<?php 
// Using: 

include_once($PATH_WRT_ROOT."includes/libgroup.php");


if(!$objMedical->isSuperAdmin($_SESSION['UserID'])){
	header("Location: /");
	exit();
}


$CurrentPage = "SettingsGroupFilter";

$TAGS_OBJ[] = array($Lang['medical']['menu']['settings_groupFilter'], "", 0);
//$PAGE_NAVIGATION[] = array($Lang['medical']['menu']['settings_groupFilter'], "?t=settings.groupFilterSetting");
$PAGE_NAVIGATION[] = array($i_Discipline_System_Group_Right_Navigation_New_Group, "");

$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();	


$objGroup = new libgroup();

/////////////// Get Medical Group START ///////////////
$groupArray = $objMedical->getMedicalGroupCategory();

$selectedGroup = array();
$selectedGroupID = array();
$selectedGroupCategoryID = array();
foreach ($groupArray as $group) {
	$name = $group['CategoryName'];
	if($group['GroupCategoryID'] == null){
		$name = '* ' . $name;
	}
	$selectedGroup[] = array(
		'value' => $group['GroupCategoryID'], 
		'name' => $name
	);
	$selectedGroupCategoryID[] = $group['GroupCategoryID'];
}
/////////////// Get Medical Group END ///////////////

/////////////// Get All Group START ///////////////
$selectedGroupIdList = trim(implode(',', $selectedGroupCategoryID), ',');
if($selectedGroupIdList == ''){
	$filterSQL = '';
}else{
	$filterSQL = " WHERE GroupCategoryID NOT IN ({$selectedGroupIdList})";
}
$allCategory = $objGroup->returnAllCategory($filterSQL);

$deselectedGroup = array();
foreach ($allCategory as $categoryID => $categoryName) {
	$deselectedGroup[] = array(
		'value' => $categoryID, 
		'name' => $categoryName
	);
}
/////////////// Get All Group END ///////////////


$settings = array(
	'deselectedHeader' => $Lang['medical']['group_filter_setting']['deselectedCategory'],
	'deselectedSelectionName' => 'deselectedCategory',
	'deselectedList' => $deselectedGroup,
	'selectedHeader' => $Lang['medical']['group_filter_setting']['selectedCategory'],
	'selectedSelectionName' => 'selectedCategory',
	'selectedList' => $selectedGroup
);


$linterface->LAYOUT_START($Msg);
?>


<form method="post" name="form1" id="form1" action="?t=settings.groupFilterSettingSave">
<div id="ContentTableDiv">

	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tbody>
			<!--tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['group_filter_setting']['category'] ?></td>
			</tr>
			<tr>
				<td class="tabletext" width="80%">
					<?=$linterface->generateUserSelection($settings);?>
					<?=$Lang['medical']['group_filter_setting']['hint'];?>
				</td>
			</tr-->
			<tr><td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td></tr>
			<tr>
				<td>
					<?=$linterface->generateUserSelection($settings);?>
					<?=$Lang['medical']['group_filter_setting']['hint'];?>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td align="center" colspan="2" class="dotline" style="text-align:left"></td>
			</tr>
			<tr>
				<td align="center" colspan="2">
					<?php
						echo $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "submit");
					?>
						<!--input type="button" id="Cancel" class="formbutton_v30 print_hide " value="<?php echo $Lang['medical']['studentLog']['button']['back']; ?>"-->
				</td>	
			</tr>
		</tfoot>
	</table>

</div>
</form>

<script type="text/javascript">
	$('#AddAll').click(function(){
		$('#selectedCategory').append( $('#deselectedCategory').find('option') );
	});
	$('#Add').click(function(){
		$('#selectedCategory').append( $('#deselectedCategory').find('option:selected') );
	});
	
	$('#Remove').click(function(){
		$('#selectedCategory > option:selected').each(function(){
			var deletedName;
			if($(this).val() == ''){
				deletedName = $(this).text().substring(2);
			}else{
				deletedName = $(this).text();
			}
			$('#form1').append('<input type="hidden" name="deletedCategoryName[]" value="'+deletedName+'" />');
			$(this).remove();
		});
		updateDeselectedStudentList();
	});
	$('#RemoveAll').click(function(){
		$('#selectedCategory > option').each(function(){
			var deletedName;
			if($(this).val() == ''){
				deletedName = $(this).text().substring(2);
			}else{
				deletedName = $(this).text();
			}
			$('#form1').append('<input type="hidden" name="deletedCategoryName[]" value="'+deletedName+'" />');
			$(this).remove();
		});
		updateDeselectedStudentList();
	});
	function updateDeselectedStudentList(){
		var selectedCategoryList = [];
		$('#selectedCategory > option').each(function(){
			selectedCategoryList.push( $(this).val() );
		});
		$.get('?t=settings.ajax.ajax_get_avaliable_group_category', {
			'selectedCategory[]': selectedCategoryList
		},function(res){
			$('#deselectedCategoryList').html(res);
		});
	}

	$('#form1').submit(function(){
		$('#selectedCategory > option').attr('selected', 'selected');
	});
</script>
<?php
$linterface->LAYOUT_STOP();
?>