<?php
/*
 * 	using: 
 * 	Log
 * 	Date:
 */
//http://192.168.0.146:31002/home/common_choose/index.php?fieldname=student[]&page_title=SelectMembers&permitted_type=1
 
if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='NOTICE_SETTINGS')){
	header("Location: /");
	exit();
}
if(!$plugin['medical_module']['message']){
	header("Location: /");
	exit();
}


$CurrentPage = "SettingsNotice";

$TAGS_OBJ[] = array($Lang['medical']['menu']['settings_noticeRemarks'], "", 0);
$PAGE_NAVIGATION[] = array($Lang['medical']['menu']['settings_accessRight'], "?t=settings.accessRight");
$PAGE_NAVIGATION[] = array($i_Discipline_System_Group_Right_Navigation_New_Group, "");

$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();	

$objDB = new libdb();


$defaultID = IntegerSafe($defaultID,'');
if(is_array($defaultID)){
	$defaultIdList = implode(',',$defaultID);
}

switch($action){
	case 'activate':
		$sql = "UPDATE MEDICAL_DEFAULT_MAIL SET RecordStatus='1' WHERE DefaultID IN ({$defaultIdList})";
		$result = $objDB->db_db_query($sql);
		if($result){
			$showMsg = 'active|success';
//			$Msg = $Lang['medical']['message_setting']['ReturnMessage']['Activate']['Success'];
		}else{
			$showMsg = 'active|fail';
//			$Msg = $Lang['medical']['message_setting']['ReturnMessage']['Activate']['Failed'];
		}
		header("Location: index.php?t=settings.messageDefaultTemplate&ShowMsg={$showMsg}");
		exit();
		break;
	case 'suspend':
		$sql = "UPDATE MEDICAL_DEFAULT_MAIL SET RecordStatus='0' WHERE DefaultID IN ({$defaultIdList})";
		$result = $objDB->db_db_query($sql);
		if($result){
			$showMsg = 'suspend|success';
//			$Msg = $Lang['medical']['message_setting']['ReturnMessage']['Suspend']['Success'];
		}else{
			$showMsg = 'suspend|fail';
//			$Msg = $Lang['medical']['message_setting']['ReturnMessage']['Suspend']['Failed'];
		}
		header("Location: index.php?t=settings.messageDefaultTemplate&ShowMsg={$showMsg}");
		exit();
		break;
	case 'delete':
		$sql = "DELETE FROM MEDICAL_DEFAULT_MAIL WHERE DefaultID IN ({$defaultIdList})";
		$result = $objDB->db_db_query($sql);
		if($result){
			$showMsg = 'delete|success';
//			$Msg = $Lang['medical']['message_setting']['ReturnMessage']['Delete']['Success'];
		}else{
			$showMsg = 'delete|fail';
//			$Msg = $Lang['medical']['message_setting']['ReturnMessage']['Delete']['Failed'];
		}
		header("Location: index.php?t=settings.messageDefaultTemplate&ShowMsg={$showMsg}");
		exit();
		break;
	case 'new':
			// Display input form below
			$action = 'new_save';
		break;
	case 'edit':
		// Display input form below
		$sql = "SELECT Title, Message FROM MEDICAL_DEFAULT_MAIL WHERE DefaultID IN ({$defaultIdList})";
		$rs = $objDB->returnResultSet($sql);
		$title = $rs[0]['Title'];
		$message = $rs[0]['Message'];
		$action = 'edit_save';
		break;
	case 'new_save':
		$_safeTitle = trim( htmlentities( $title , ENT_QUOTES, 'UTF-8'));
		$sql = "INSERT INTO MEDICAL_DEFAULT_MAIL (Title, Message, RecordStatus, DateInput, InputBy, DateModified, ModifyBy)
			VALUES ('{$_safeTitle}', '{$Message}', '1', now(), '{$_SESSION['UserID']}', now(), '{$_SESSION['UserID']}')";
		$result = $objDB->db_db_query($sql);
		if($result){
			$showMsg = 'new|success';
//			$Msg = $Lang['medical']['message_setting']['ReturnMessage']['Add']['Success'];
		}else{
			$showMsg = 'new|fail';
//			$Msg = $Lang['medical']['message_setting']['ReturnMessage']['Add']['Failed'];
		}
		header("Location: index.php?t=settings.messageDefaultTemplate&ShowMsg={$showMsg}");
		exit();
		break;
	case 'edit_save':
		$_safeTitle = trim( htmlentities( $title , ENT_QUOTES, 'UTF-8'));
		$sql = "UPDATE MEDICAL_DEFAULT_MAIL SET Title='{$_safeTitle}', Message='{$Message}', DateModified=now(), ModifyBy='{$_SESSION['UserID']}' WHERE DefaultID IN ({$defaultIdList})";
		$result = $objDB->db_db_query($sql);
		if($result){
			$showMsg = 'edit|success';
//			$Msg = $Lang['medical']['message_setting']['ReturnMessage']['Edit']['Success'];
		}else{
			$showMsg = 'edit|fail';
//			$Msg = $Lang['medical']['message_setting']['ReturnMessage']['Edit']['Failed'];
		}
		header("Location: index.php?t=settings.messageDefaultTemplate&ShowMsg={$showMsg}");
		exit();
		break;
	default:
		echo 'Error: Action = ' . $action;
		exit;
}


#### HTML Editor START ####
include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
$objHtmlEditor = new FCKeditor ( 'Message' , "100%", "320", "", "CustMedicalMessageSet", htmlspecialchars_decode($message));
$htmlEditor = $objHtmlEditor->CreateHtml();
$home_header_no_EmulateIE7 = false; // FCK Editor does not support IE11, but it does support IE7 @口@
#### HTML Editor END ####


$linterface->LAYOUT_START($Msg);

$c_ui = new common_ui();
$js_css = $c_ui->Include_JS_CSS();
echo $js_css;
?>

<style type="text/css">
<?php
include_once($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/medical/css/medical.css");
?>
textarea{
	min-width: 50%;
	min-height: 200px;
}
</style>
<form method="post" name="form1" id="form1" action="?t=settings.messageDefaultTemplateEdit">
<div id="ContentTableDiv">

	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tbody>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['message']['setting']['title'] ?></td>
				<td class="tabletext" width="80%"><input name="title" style="width:50%" value="<?=$title?>"/></td>
			</tr>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['message']['setting']['message'] ?><!--font style="color:red;">*</font--></td>
				<td class="tabletext" width="80%">
					<?= $htmlEditor?>
				</td>
			</tr>
			<tr>
				<td class="dotline" colspan="2">
					<!--<span style="vertical-align:top;"><font style="color:red;">*</font><?=$Lang['medical']['default_remarks_setting']['defaultRemarksHint']?></span>
					<img src="<?php echo $image_path."/".$LAYOUT_SKIN.'/10x10.gif'; ?>" width="10" height="1" />-->
				</td>	
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td align="center" colspan="2">
					<?php
						echo $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "submit");
					?>
						<input type="button" id="Cancel" class="formbutton_v30 print_hide " value="<?php echo $Lang['medical']['studentLog']['button']['back']; ?>">
				</td>	
			</tr>
		</tfoot>
	</table>
	<input type='hidden' name='action' value='<?=$action?>' />
	
	<?php if(count($defaultID) > 0) {
		foreach($defaultID as $dID) { 
	?>
		<input type='hidden' name='defaultID[]' value='<?=$dID?>' />
	<?php } } ?>
</div>
</form>

<?php
$linterface->LAYOUT_STOP();
?>
<script>
	$('#Cancel').click(function(){
		window.location.href = 'index.php?t=settings.messageDefaultTemplate';
	});
</script>