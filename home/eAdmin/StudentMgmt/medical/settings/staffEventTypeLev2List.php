<?php
/*
 * Log
 *
 * 2018-03-27 Cameron
 * - create this file
 */
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();

$objEt = new StaffEventLev2();
$filter = "DeletedFlag<>1"; // don't show marked deleted records
$eventTypeID = $_GET['EventTypeID'];
if ($eventTypeID){
    $filter .= " AND EventTypeID='".$eventTypeID."'";
}
$orderBy = "EventTypeLev2Name";
$eventTypeSetting = $objEt->getAllEventType($filter, $orderBy);

if (! isset($c_ui)) {
    $c_ui = new common_ui();
}

?>

<div id="viewResult">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<colgroup>
			<col style="width: 70%">
			<col style="width: 30%">
		</colgroup>
		<thead>
			<tr>
				<td valign="bottom" colspan="2">
					<div class="Conntent_tool">
						<br style="clear: both">				
<?php
$x = $linterface->Get_Thickbox_Link($c_ui->getThickBoxHeight(), $c_ui->getThickBoxWidth(), "add_dim", $Lang['medical']['general']['button']['new_category'], "jsShowStaffEventTypeLev2SettingAddEditLayer('add'); return false;", $InlineID = "FakeLayer", "&nbsp;" . $Lang['Btn']['New']);
echo $x;
?>	
					</div>
				</td>
            </tr>
            <tr class="table-action-bar">
				<td valign="bottom" colspan="2">
					<div class="common_table_tool">
						<a href="javascript:jsHandleEventTypeLev2Setting('activate');"
							class="tool_approve"><?=$Lang['Status']['Activate']?></a> <a
							href="javascript:jsHandleEventTypeLev2Setting('suspend');"
							class="tool_reject"><?=$Lang['Status']['Suspend']?></a> <a
							href="javascript:jsShowStaffEventTypeLev2SettingAddEditLayer('edit');"><?=$Lang['Btn']['Edit']?></a>
						<a href="javascript:jsHandleEventTypeLev2Setting('delete');"
							class="tool_delete"><?=$Lang['Btn']['Delete']; ?></a>
					</div>

				</td>
			</tr>
		</thead>
	</table>
	<table width="95%" border="0" cellspacing="0" cellpadding="0"
		class="common_table_list">
		<colgroup>
			<col style="width: 4%">
			<col style="width: 40%">
			<col style="width: 20%">
			<col style="width: 20%">
			<col style="width: 16%">
		</colgroup>
		<thead>
			<tr class="tabletop">
				<th class="tabletoplink">#</th>
				<th class="tabletoplink"><?=$Lang['medical']['general']['tableHeader']['Subcategory']?></th>
				<th class="tabletoplink"><?=$Lang['medical']['message']['setting']['recordStatus']?></th>
				<th class="tabletoplink"><?php echo $Lang['medical']['general']['tableHeader']['Default']; ?></th>
				<th class="tabletoplink"><input type="checkbox"
					name="eventTypeSettingsCheckAll" id="eventTypeSettingsCheckAll"
					onClick="(this.checked)?setChecked(1,this.form,'EventTypeLev2ID[]'):setChecked(0,this.form,'EventTypeLev2ID[]')"></th>
			</tr>
		</thead>
		<tbody>
<?php if (count($eventTypeSetting) > 0) :?>
	<?php $i = 0;?>
	<?php foreach ((array) $eventTypeSetting as $rs) :?>
			<tr>
				<td><?php echo ++$i;?></td>
				<td><?php echo ($rs['EventTypeLev2Name'] ? $rs['EventTypeLev2Name'] : "--"); ?></td>
				<td><?php echo ($rs['RecordStatus'] ? $Lang['medical']['message']['setting']['StatusOption']['InUse'] : $Lang['medical']['message']['setting']['StatusOption']['Suspend']); ?></td>
				<td><?php echo ($rs['IsDefault'] ? $linterface->Get_Tick_Image() : "&nbsp;"); ?></td>
				<td><input type="checkbox" name="EventTypeLev2ID[]"
					value=<?php echo $rs['EventTypeLev2ID']; ?> class="panelSelection"></td>
			</tr>
	<?php endforeach;?>
<?php else:?>
			<tr>
				<td class=tableContent align=center colspan="6"><br><?=$Lang['General']['NoRecordAtThisMoment']?><br>
					<br></td>
			</tr>
<?php endif;?>
		</tbody>
	</table>

</div>
