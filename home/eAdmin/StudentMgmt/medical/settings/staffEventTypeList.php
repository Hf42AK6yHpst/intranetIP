<?php
/*
 * Log
 *
 * 2018-03-26 Cameron
 * - create this file
 */
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();

$objEt = new StaffEventLev1();
$filter = "DeletedFlag<>1"; // don't show marked deleted records
$orderBy = "EventTypeName";
$eventTypeSetting = $objEt->getAllEventType($filter, $orderBy);

if (! isset($c_ui)) {
    $c_ui = new common_ui();
}

?>

<div id="viewResult">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<colgroup>
			<col style="width: 70%">
			<col style="width: 30%">
		</colgroup>
		<thead>
			<tr>
				<td valign="bottom" colspan="2">
					<div class="Conntent_tool">
						<br style="clear: both">				
<?php
$x = $linterface->Get_Thickbox_Link($c_ui->getThickBoxHeight(), $c_ui->getThickBoxWidth(), "add_dim", $Lang['medical']['general']['button']['new_category'], "js_show_event_type_setting_add_edit_layer('add'); return false;", $InlineID = "FakeLayer", "&nbsp;" . $Lang['Btn']['New']);
echo $x;
?>	
					</div>
				</td>
            </tr>
            <tr class="table-action-bar">
				<td valign="bottom" colspan="2">
					<div class="common_table_tool">
						<a href="javascript:js_handle_event_type_setting('activate');"
							class="tool_approve"><?=$Lang['Status']['Activate']?></a> <a
							href="javascript:js_handle_event_type_setting('suspend');"
							class="tool_reject"><?=$Lang['Status']['Suspend']?></a> <a
							href="javascript:js_show_event_type_setting_add_edit_layer('edit');"><?=$Lang['Btn']['Edit']?></a>
						<a href="javascript:js_handle_event_type_setting('delete');"
							class="tool_delete"><?=$Lang['Btn']['Delete']; ?></a>
					</div>

				</td>
			</tr>
		</thead>
	</table>
	<table width="95%" border="0" cellspacing="0" cellpadding="0"
		class="common_table_list">
		<colgroup>
			<col style="width: 4%">
			<col style="width: 40%">
			<col style="width: 15%">
			<col style="width: 16%">
			<col style="width: 15%">
			<col style="width: 10%">
		</colgroup>
		<thead>
			<tr class="tabletop">
				<th class="tabletoplink">#</th>
				<th class="tabletoplink"><?=$Lang['medical']['general']['tableHeader']['Category']?></th>
				<th class="tabletoplink"><?php echo $Lang['medical']['staffEvent']['setting']['subcategoryQty']; ?></th>
				<th class="tabletoplink"><?=$Lang['medical']['message']['setting']['recordStatus']?></th>
				<th class="tabletoplink"><?php echo $Lang['medical']['general']['tableHeader']['Default']; ?></th>
				<th class="tabletoplink"><input type="checkbox"
					name="eventTypeSettingsCheckAll" id="eventTypeSettingsCheckAll"
					onClick="(this.checked)?setChecked(1,this.form,'EventTypeSettingID[]'):setChecked(0,this.form,'EventTypeSettingID[]')"></th>
			</tr>
		</thead>
		<tbody>
<?php if (count($eventTypeSetting)>0):?>
	<?php $i=0;?>			
	<?php foreach( (array)$eventTypeSetting as $rs): ?>
			<tr>
				<td><?php echo ++$i;?></td>
				<td><?php echo '<a href="index.php?t=settings.staffEventTypeLev2&EventTypeID='.$rs['EventTypeID'].'">'.($rs['EventTypeName'] ? $rs['EventTypeName'] : "--").'</a>'; ?></td>
				<td><?php echo (($rs['NrSubCategory']>=0) ? stripslashes($rs['NrSubCategory']) : "--"); ?></td>
				<td><?php echo ($rs['RecordStatus'] ? $Lang['medical']['message']['setting']['StatusOption']['InUse'] : $Lang['medical']['message']['setting']['StatusOption']['Suspend']); ?></td>
				<td><?php echo ($rs['IsDefault'] ? $linterface->Get_Tick_Image() : "&nbsp;"); ?></td>
				<td><input type="checkbox" name="EventTypeSettingID[]"
					value=<?php echo $rs['EventTypeID']; ?> class="panelSelection"></td>
			</tr>
	<?php endforeach;?>

<?php else:?>

			<tr>
				<td class=tableContent align=center colspan="6"><br><?=$Lang['General']['NoRecordAtThisMoment']?><br>
				<br></td>
			</tr>
<?php endif;?>
		</tbody>
	</table>

</div>
