<?php
/*
 * 	using: 
 * 	Log
 * 	Date:
 */
//http://192.168.0.146:31002/home/common_choose/index.php?fieldname=student[]&page_title=SelectMembers&permitted_type=1
 
if(
	!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='MEAL_SETTINGS') &&
	!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='BOWEL_SETTINGS') &&
	!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='STUDENTLOG_SETTINGS') &&
	!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='SLEEP_SETTINGS')
){
	header("Location: /");
	exit();
}

$CurrentPage = "SettingsDefaultRemarks";

$TAGS_OBJ[] = array($Lang['medical']['menu']['settings_defaultRemarks'], "", 0);
$PAGE_NAVIGATION[] = array($Lang['medical']['menu']['settings_accessRight'], "?t=settings.accessRight");
$PAGE_NAVIGATION[] = array($i_Discipline_System_Group_Right_Navigation_New_Group, "");

$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();	

$objDB = new libdb();


#### init START ####
if(empty($module)){// Default selection
	if($plugin['medical_module']['meal'] && $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='MEAL_SETTINGS')){
		$module = 'meal'; 
	}else if($plugin['medical_module']['bowel'] && $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='BOWEL_SETTINGS')){
		$module = 'bowel'; 
	}else if($plugin['medical_module']['studentLog'] && $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='STUDENTLOG_SETTINGS')){
		$module = 'studentlog'; 
	}else if($plugin['medical_module']['sleep'] && $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='SLEEP_SETTINGS')){
		$module = 'sleep'; 
	}else{
		$module = '';
	}
}
#### init END ####

#### Save Remarks START ####
if($action == 'update'){
	if(!empty($module)){
		// Clean the remarks for empty line
//	debug_r($remarks);	
		$remarks = htmlspecialchars($remarks,ENT_QUOTES,'UTF-8');
		$remarks = explode("\n",$remarks);
		for($i=0,$iMax=count($remarks);$i<$iMax;$i++){
			$remarks[$i] = trim($remarks[$i]);
		}
		$remarks = implode("\n",array_filter($remarks));
//	debug_r($remarks);	
		$sql = "SELECT COUNT(*) AS hasVal FROM MEDICAL_DEFAULT_REMARKS WHERE RemarksType='{$module}'";
		$rs = $objDB->returnResultSet($sql);
		if($rs[0]['hasVal'] > 0){
			$sql = "UPDATE MEDICAL_DEFAULT_REMARKS SET Remarks='{$remarks}', DateModified=now(), LastModifiedBy='{$_SESSION['UserID']}' WHERE RemarksType='{$module}'";
		}else{
			$sql = "INSERT INTO MEDICAL_DEFAULT_REMARKS (RemarksType, Remarks, DateInput, InputBy, DateModified, LastModifiedBy)
				VALUES
					('{$module}', '{$remarks}', now(), '{$_SESSION['UserID']}', now(), '{$_SESSION['UserID']}')";
		}
		$result = $objDB->db_db_query($sql);
		if($result){
			$Msg = $Lang['medical']['default_remarks_setting']['ReturnMessage']['Update']['Success'];
		}else{
			$Msg = $Lang['medical']['default_remarks_setting']['ReturnMessage']['Update']['Failed'];
		}
	}else{
		$Msg = $Lang['medical']['default_remarks_setting']['ReturnMessage']['Update']['Failed'];
	}
}
#### Save Remarks END ####

#### Get Module Selection START ####
$moduleSelection = "<select id='module' name='module'>";
foreach($medical_cfg['general']['module'] as $mod){
	$selected = '';
	if( ($mod=='meal') && ($plugin['medical_module']['meal']) && ($objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='MEAL_SETTINGS')) ){
		if($module == $mod){
			$selected = 'selected';
		}
		$moduleSelection .= "<option value='$mod' $selected>{$Lang['medical']['menu']['meal']}</option>";
	}else if( ($mod=='bowel') && ($plugin['medical_module']['bowel']) && ($objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='BOWEL_SETTINGS')) ){
		if($module == $mod){
			$selected = 'selected';
		}
		$moduleSelection .= "<option value='$mod' $selected>{$Lang['medical']['menu']['bowel']}</option>";
	}else if( ($mod=='studentlog') && ($plugin['medical_module']['studentLog']) && ($objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='STUDENTLOG_SETTINGS')) ){
		if($module == $mod){
			$selected = 'selected';
		}
		$moduleSelection .= "<option value='$mod' $selected>{$Lang['medical']['menu']['studentLog']}</option>";
	}else if( ($mod=='sleep') && ($plugin['medical_module']['sleep']) && ($objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='SLEEP_SETTINGS')) ){
		if($module == $mod){
			$selected = 'selected';
		}
		$moduleSelection .= "<option value='$mod' $selected>{$Lang['medical']['menu']['studentSleep']}</option>";
	}
}
$moduleSelection .= '</select>';
#### Get Module Selection END ####

#### Get Remarks START ####
$sql = "SELECT Remarks FROM MEDICAL_DEFAULT_REMARKS WHERE RemarksType='{$module}'";
$rs = $objDB->returnResultSet($sql);
$remarks = $rs[0]['Remarks'];
#### Get Remarks END ####


$linterface->LAYOUT_START($Msg);

$c_ui = new common_ui();
$js_css = $c_ui->Include_JS_CSS();
echo $js_css;
?>


<style type="text/css">
<?php
include_once($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/medical/css/medical.css");
?>
textarea{
	min-width: 50%;
	min-height: 200px;
}
</style>
<form method="post" name="form1" id="form1" action="?t=settings.defaultRemarks">
<div id="ContentTableDiv">

	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tbody>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['default_remarks_setting']['module'] ?></td>
				<td class="tabletext" width="80%"><?=$moduleSelection?></td>
			</tr>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['default_remarks_setting']['defaultRemarks'] ?><font style="color:red;">*</font></td>
				<td class="tabletext" width="80%">
					<textarea id="remarks" name="remarks"><?= $remarks?></textarea>
				</td>
			</tr>
			<tr>
				<td class="dotline" colspan="2">
					<span style="vertical-align:top;"><font style="color:red;">*</font><?=$Lang['medical']['default_remarks_setting']['defaultRemarksHint']?></span>
					<img src="<?php echo $image_path."/".$LAYOUT_SKIN.'/10x10.gif'; ?>" width="10" height="1" />
				</td>	
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td align="center" colspan="2">
					<?php
						echo $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "submit");
					?>
				</td>	
			</tr>
		</tfoot>
	</table>
	<input type='hidden' id='action' name='action' value='update' />
</div>
</form>

<?php
$linterface->LAYOUT_STOP();
?>
<script>
	$('#module').change(function(){
		$('#action').val('change');
		$('#form1').submit();
	});
</script>
