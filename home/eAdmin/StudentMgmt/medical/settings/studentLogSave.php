<?php

//error_log("\n\nGetPost -->".print_r($_POST,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");

$lev1ID 		= trim($_POST['Lev1ID']);
$lev1Name 		= trim( htmlentities($_POST['Lev1Name'], ENT_QUOTES, 'UTF-8'));
$lev1Code 		= trim( htmlentities($_POST['Lev1Code'], ENT_QUOTES, 'UTF-8'));
$recordStatus 	= trim(isset($_POST['RecordStatus'])?$_POST['RecordStatus']:0);

$objStudentLogLev1 = new studentLogLev1($lev1ID);
$objStudentLogLev1->setLev1Name($lev1Name);
$objStudentLogLev1->setLev1Code($lev1Code);
$objStudentLogLev1->setRecordStatus($recordStatus);

if ($lev1ID)
{
	$objStudentLogLev1->setLastModifiedBy($_SESSION['UserID']);
}
else
{
	$objStudentLogLev1->setInputBy($_SESSION['UserID']);
}

$result = $objStudentLogLev1->save();
echo $result;
?>