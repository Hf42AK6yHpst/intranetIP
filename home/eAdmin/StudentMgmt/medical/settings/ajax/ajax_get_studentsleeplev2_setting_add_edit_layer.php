<?php
// using: 


intranet_opendb();

$StudentSleepReasonID = stripslashes($_REQUEST['StudentSleepReasonID']);
$StatusID = stripslashes($_REQUEST['StatusID']);

$objLev1 = new studentSleepLev1($StatusID);
$StatusName = $objLev1->getStatusName();

//debug_r($_REQUEST);
$enableIsDefault = false;
if ($StudentSleepReasonID)
{
	$sLog = new studentSleepLev2($StudentSleepReasonID);
	$reasonID 		= $sLog->getReasonID();
	$reasonName 	= str_replace('"','&quot;',stripslashes($sLog->getReasonName()));
	$reasonCode 	= str_replace('"','&quot;',stripslashes($sLog->getReasonCode()));
//	$itemQty 		= $sLog->getItemQty();
	$isDefault 		= $sLog->getIsDefault();	
	$recordStatus 	= $sLog->getRecordStatus();

	if ($isDefault)	// Current Record is default
	{
		$enableIsDefault = true;
	}	
}
else
{
	$sLog = new studentSleepLev2();
	$reasonID 		= "";
	$reasonName 	= "";
	$reasonCode 	= "";
//	$itemQty 		= 0;
	$isDefault 		= 0;		// Not default
	$recordStatus 	= "-1";		// Status not set	
}

if (!$sLog->checkIsDefault($StatusID))		// All are not default at present
{
	$enableIsDefault = true;		
}
$disableIsDefault = $enableIsDefault ? "" : "disabled";

$level 		= "Lev2";	// Name & ID prefix
$levID 		= $reasonID;
$levName	= $reasonName;
$levCode 	= $reasonCode;


## Interface Start
include_once($PATH_WRT_ROOT."/home/eAdmin/StudentMgmt/medical/settings/templates/studentSleep.tmpl.php");
## Interface End

intranet_closedb();
?>
