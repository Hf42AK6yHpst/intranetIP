<?php
// using: 

$StatusID = trim(htmlentities($_POST['StatusID'], ENT_QUOTES, 'UTF-8'));
$StatusName = trim(htmlentities($_POST['StatusName'], ENT_QUOTES, 'UTF-8'));
$StatusCode = trim(htmlentities($_POST['StatusCode'], ENT_QUOTES, 'UTF-8'));

//debug_r($_POST);
$result = '';
$objMealStatus = new mealStatus($StatusID);
$result = $objMealStatus->recordDuplicateChecking($StatusName,$StatusCode,$StatusID);


// if($result == 0)	: pass
// if($result & 1)	: name duplicate
// if($result & 2)	: code duplicate
if($result == 0)
{
	echo 'pass';
}
else
{
	echo $result;
}
?>