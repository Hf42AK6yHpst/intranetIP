<?php
// using
/*
 * 2018-03-27 Cameron
 * - create this file
 */
intranet_opendb();

$eventTypeLev2ID = stripslashes($_POST['EventTypeLev2ID']);
$Task = stripslashes($_POST['Task']);

$pass = true;
$ret = 1;

$objDb = new libdb();
if ($eventTypeLev2ID) {
    switch ($Task) {
        case "activate":
            $updatedField = "RecordStatus=1,LastModifiedBy=" . $_SESSION['UserID'] . ",DateModified=NOW()";
            break;
        
        case "delete":
            $sql = "SELECT COUNT(*) AS NumRecord FROM MEDICAL_STAFF_EVENT WHERE EventTypeLev2ID='$eventTypeLev2ID'";
            
            $rs = $objDb->returnResultSet($sql);
            $isUsed = $rs[0]['NumRecord'];
            if ($isUsed) {
                $pass = false;
            }
            $updatedField = "DeletedFlag=1,DeletedBy=" . $_SESSION['UserID'] . ",DeletedDate=NOW()";
            break;
        
        case "suspend":
            $updatedField = "RecordStatus=0,LastModifiedBy=" . $_SESSION['UserID'] . ",DateModified=NOW()";
            break;
    }
    
    if ($pass) {
        $sql = "UPDATE MEDICAL_STAFF_EVENT_TYPE_LEV2 SET $updatedField WHERE EventTypeLev2ID IN (" . $eventTypeLev2ID . ")";
        $ret = $objDb->db_db_query($sql);
    } else {
        $ret = - 1;
    }
}

intranet_closedb();
echo $ret; // return to refresh page
?>
