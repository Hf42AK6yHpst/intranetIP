<?php
// using:
/*
 * Log
 *
 * 2018-03-27 Cameron
 * - create this file
 */
$eventTypeLev2ID = trim($_POST['EventTypeLev2ID']);
//$eventTypeLev2Name= trim(htmlentities($_POST['EventTypeLev2Name'], ENT_QUOTES, 'UTF-8'));
$eventTypeLev2Name= standardizeFormPostValue(trim($_POST['EventTypeLev2Name']));
$eventTypeID= trim($_POST['EventTypeID']);

$result = '';
$objEt = new StaffEventLev2($eventTypeLev2ID);
$result = $objEt->recordDuplicateChecking($eventTypeLev2Name, $eventTypeID, $eventTypeLev2ID);

if ($result == 0) {
    echo 'pass';
} else {
    echo $result;
}

?>