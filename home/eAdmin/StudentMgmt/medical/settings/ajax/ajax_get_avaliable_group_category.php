<?php 
// Using: 

include_once($PATH_WRT_ROOT."includes/libgroup.php");

$objGroup = new libgroup();

#### SQL Safe START ####
$selectedCategory = IntegerSafe($selectedCategory);
#### SQL Safe END ####

/////////////// Get All Group START ///////////////
if($selectedCategory == ''){
	$filterSQL = '';
}else{
	$selectedGroupIdList = trim(implode(',', $selectedCategory), ',');
	$filterSQL = " WHERE GroupCategoryID NOT IN ({$selectedGroupIdList})";
}
$allCategory = $objGroup->returnAllCategory($filterSQL);

$deselectedGroup = array();
foreach ($allCategory as $categoryID => $categoryName) {
	$deselectedGroup[] = array(
		'value' => $categoryID, 
		'name' => $categoryName
	);
}
/////////////// Get All Group END ///////////////


?>
<select name="deselectedCategory[]" id="deselectedCategory" size="10" style="width:99%" multiple="true">
<?php foreach ($deselectedGroup as $group) { ?>
	<option value="<?=$group['value']?>"><?=$group['name']?></option>
<?php } ?>
</select>