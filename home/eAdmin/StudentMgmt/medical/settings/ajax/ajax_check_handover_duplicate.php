<?php 
if(!$sys_custom['medical']['Handover'] || !($objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'HANDOVER_SETTINGS'))){
    header('location:/');
    exit();
}
$itemName = trim($_POST['itemName']);
$itemID = trim($_POST['ItemID']);
$itemCode = trim($_POST['itemCode']);
$field = trim($_POST['field']);
switch($field){
    case 'Name':
        $item = $objMedical->getHandoverItemByName($itemName);
        if(sizeof($item) == 0){
            echo 'pass';
        } else {
            $item = $item[0];
            if($item['ItemID'] == $itemID){
                echo 'pass';
            } else {
                echo 'duplicate';
            }
        }
        break;
    case 'Code':
        $item = $objMedical->getHandoverItemByCode($itemCode);
        if(sizeof($item) == 0){
            echo 'pass';
        } else {
            $item = $item[0];
            if($item['ItemID'] == $itemID){
                echo 'pass';
            } else {
                echo 'duplicate';
            }
        }
        break;
}
?>