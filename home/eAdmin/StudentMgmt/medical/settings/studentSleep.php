<?php
/*
 * 	using: 
 * 	Log 
 * 	Date:
 */

 
if( (!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='SLEEP_SETTINGS')) || (!$plugin['medical_module']['sleep']) ){
	header("Location: /");
	exit();
}

$CurrentPage = "SettingsStudentSleep";


$TAGS_OBJ[] = array($Lang['medical']['menu']['settings_studentsleep'], "", 0);
$PAGE_NAVIGATION[] = array($Lang['medical']['menu']['settings_studentsleep'], "?t=settings.studentSleep");
//$PAGE_NAVIGATION[] = array($i_Discipline_System_Group_Right_Navigation_New_Group, "");

$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();		
$linterface->LAYOUT_START($Msg);

$c_ui = new common_ui();
$js_css = $c_ui->Include_JS_CSS();
echo $js_css;
?>
<style type="text/css">
<?php
include_once($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/medical/css/medical.css");
?>
</style>
<form method="post" name="form1" action="index.php">
<input type="hidden" name="t" value="settings.studentSleepSave">
<div id="ContentTableDiv">
<?php
	include_once("studentSleepList.php");
?>
</div>
</form>

<?php
$linterface->LAYOUT_STOP();
?>
<script>

function js_show_studentsleep_setting_add_edit_layer(action) {
	var pass = true;
	var selStudentSleepStatusID="";
	switch(action)
	{
		case "add":
			break;
		case "edit":
			if($('input[name="StudentSleepStatusID\[\]"]:checked').length == 0)
			{				
				alert(globalAlertMsg1);	// select only one
			    pass = false;
			}
			else if($('input[name="StudentSleepStatusID\[\]"]:checked').length > 1)
			{
				alert(globalAlertMsg1);	// select only one
			    pass = false;
			}		    
		    else
		    {		    	 
		    	selStudentSleepStatusID = $('input[name="StudentSleepStatusID\[\]"]:checked').val();
		    }
			break;
	}
	if (pass)
	{
		Hide_Return_Message();		
		js_Show_ThickBox('<?=$Lang['Btn']['Edit']?>',350, 650);
		$.post(
			"index.php?t=settings.ajax.ajax_get_studentsleep_setting_add_edit_layer", 
			{ 
				StudentSleepStatusID: selStudentSleepStatusID
			},
			function(ReturnData)
			{
				$('#TB_ajaxContent').html(ReturnData);
			}
		);
	}
}

function Check_StudentSleepSetting_Form() {
	var StatusID 		= Trim($('#Lev1ID').val());
	var StatusName 		= Trim($('#Lev1Name').val());
	var StatusCode 		= Trim($('#Lev1Code').val());
	var StatusColor 	= Trim($('#StatusColor').val());
	var RecordStatus	= $('input[name=RecordStatus]:checked','#StudentSleepLev1Form').val();
	var IsDefault 		= 0;
	if ($('#IsDefault').is(":checked"))
		IsDefault 		= Trim($('#IsDefault').val());
		
	if (isMandatoryTextPass() && isEmptySelection()) {

		Block_Thickbox();
		
		// Add or Edit
		var action = '';
		var actionScript = '?t=settings.studentSleepSave';
		if (StatusID == '')
		{			
			action = "new";
		}
		else
		{			
			action = "update";
		}

		var checkScript = '?t=settings.ajax.ajax_check_studentsleeplev_duplicate_NameCode';
		$.post(
			checkScript,
			{
				StudentSleepLev: '1',
				StudentSleepLevID: StatusID,
				StudentSleepLevName: StatusName,				
				StudentSleepLevCode: StatusCode
			},
			function(ReturnData)
			{
				if(ReturnData == 'pass')
				{
					$.post(
						actionScript, 
						{ 
							StatusName: StatusName,				
							StatusCode: StatusCode,
							RecordStatus: RecordStatus,
							StatusID: StatusID,
							IsDefault: IsDefault,
							StatusColor: StatusColor
						 },
						function(ReturnData)
						{
							js_Reload_Content_Table();
							js_Show_System_Message(action, ReturnData);
							UnBlock_Thickbox();
							js_Hide_ThickBox();
						}
					);
				}
				else if(ReturnData == '1')
				{
					UnBlock_Thickbox();
					alert('<?=$Lang['medical']['general']['duplicateCategory']?>');
				}
				else if(ReturnData == '2')
				{
					UnBlock_Thickbox();
					alert('<?=$Lang['medical']['general']['duplicateCode']?>');
				}
				else
				{
					UnBlock_Thickbox();
					alert('<?=$Lang['medical']['general']['duplicateCategory'] . '\n' . $Lang['medical']['general']['duplicateCode']?>');
				}
			}
		);
	}
}
	
// Reload Content Table
function js_Reload_Content_Table()
{
	$('#ContentTableDiv').load(		
		"?t=settings.ajax.ajax_reload_studentsleep_setting",
		function(ReturnData)
		{
			initThickBox();
		}
	);
}

// Load system message
function js_Show_System_Message(Action, Status)
{
	var returnMessage = '';
	
	switch(Action)
	{
		case "activate":
			returnMessage = (Status)? '<?=$Lang['medical']['studentlog_setting']['ReturnMessage']['Activate']['Success']?>' : '<?=$Lang['medical']['studentlog_setting']['ReturnMessage']['Activate']['Failed']?>';
			break;
		case "suspend":
			returnMessage = (Status)? '<?=$Lang['medical']['studentlog_setting']['ReturnMessage']['Suspend']['Success']?>' : '<?=$Lang['medical']['studentlog_setting']['ReturnMessage']['Suspend']['Failed']?>';
			break;
		case "new":
			returnMessage = (Status)? '<?=$Lang['General']['ReturnMessage']['AddSuccess']?>' : '<?=$Lang['General']['ReturnMessage']['AddUnsuccess']?>';
			break;
		case "update":
			returnMessage = (Status)? '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>' : '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
			break;
		case "delete":
			if (Status == 1) {			
				returnMessage = '<?=$Lang['General']['ReturnMessage']['DeleteSuccess']?>';
			}
			else if (Status == "-1") {
				returnMessage = '<?=$Lang['medical']['studentlog_setting']['ReturnMessage']['Delete']['FailedWithReason']?>';
			}
			else {
				returnMessage = '<?=$Lang['General']['ReturnMessage']['DeleteUnsuccess']?>';
			}
			break;
		
	}
	Get_Return_Message(returnMessage);
}
	
function isMandatoryTextPass(){
	var isFieldValid = true;
	var msg = "";
	if ($.trim($('#Lev1Name').val()) == "") {
		isFieldValid= false;		
		msg = "<?=$Lang['General']['Name']?>";
	}
	
	if (isFieldValid == false) {
		alert("<?=$Lang['medical']['general']['AlertMessage']['FillText']?>" + ": " + msg);		
	}
	
	return isFieldValid;
}

function isEmptySelection(){
	var isFieldValid = true;
	$('.radiobutton').each(function(){	
		var name = $(this).attr('name');
		if(!$('input[name="'+name+'"]').is(':checked')){
			isFieldValid = false;
		}
	});
	
	if (isFieldValid == false) {	
		alert("<?=$Lang['medical']['general']['AlertMessage']['SelectOption'].": " .$Lang['General']['Status2']?>");
	}
	return isFieldValid;
}


function js_handle_studentsleep_setting(task)
{
	if($('input[name="StudentSleepStatusID\[\]"]:checked').length == 0)
	{
		alert(globalAlertMsg2);	// check at least one item	
	}
    else
    {
    	var cnf;
    	
    	switch(task)
    	{
    		case "activate":
    			cnf = confirm("<?=$Lang['medical']['general']['AlertMessage']['Activate']?>");
    			break;
    		case "delete":
    			cnf = confirm(globalAlertMsg3);
    			break;
    		case "suspend":
    			cnf = confirm(globalAlertMsg5);
    			break;
    	}
    	if (cnf)
    	{
		    var ids = [];

		    var deleteSuccess = true;
		    $('input[name="StudentSleepStatusID\[\]"]:checked').each(function() {
				if(task == "delete"){
					if($(this).closest('td').prev().prev().prev().html()>0){
						alert('<?=$Lang['medical']['general']['deleteCategoryContainsItem']?>');
						deleteSuccess = false;
						return;
					}
				}
			    
		        ids.push($(this).val());
		    });
		    
		    $('input[name="StudentSleepStatusID\[\]"]:checked').each(function() {
		        ids.push($(this).val());	        
		    });
		    ids = ids.join(",");

			Hide_Return_Message();	
			if(deleteSuccess){		
				$.post(
					"index.php?t=settings.ajax.ajax_handle_studentsleep_setting", 
					{ 
						StudentSleepStatusID: ids,
						Task: task
					},
					function(ReturnData)
					{
						js_Show_System_Message(task, ReturnData);
						js_Reload_Content_Table();
					}
				);
			}else{
				js_Show_System_Message(task, "");
			}
    	}
    }
}


$('#studentSleepSettingsCheckAll').live('click',function(){
	$('.panelSelection').attr('checked', $(this).attr('checked'));
});


</script>
