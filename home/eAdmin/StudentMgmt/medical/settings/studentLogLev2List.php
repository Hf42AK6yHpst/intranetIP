<?php
/*
 * 	using:
 * 	Date:	2015-01-22 [Cameron] Add $plugin['medical_module']['AlwaysShowBodyPart'] to control showing body part list or not 
 */

$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();	
$lev1ID = $_REQUEST["Lev1ID"];
if ($lev1ID)
{
	$objSLog = new studentLogLev2();
	$filter = "Lev1ID=".$lev1ID." AND DeletedFlag<>1";	// don't show marked deleted records
	$orderBy = "Lev2Code";
	$studentLogLev2 = $objSLog->getAllStatus($filter,$orderBy);
	//debug_r($studentLogLev2);	
	
	if (!isset($c_ui))
	{
		$c_ui = new common_ui();	
	}

?>

<div id="viewResult">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<colgroup>
			<col style="width:70%">
			<col style="width:30%">
		</colgroup>
		<thead>
			<tr>
				<td valign="bottom" colspan="2">
					<div class="Conntent_tool"><br style="clear:both">				
<?php						
	$x = $linterface->Get_Thickbox_Link($c_ui->getThickBoxHeight(), $c_ui->getThickBoxWidth(), "add_dim", 
			$Lang['medical']['general']['button']['new_category'], "js_show_studentlog_setting_add_edit_layer('add'); return false;",$InlineID="FakeLayer","&nbsp;".$Lang['Btn']['New']);
	echo $x;
?>	
					</div>												
				</td>
            </tr>
            <tr class="table-action-bar">
				<td valign="bottom" colspan="2">
					<div class="common_table_tool">						
						<a href="javascript:js_handle_studentlog_setting('activate');" class="tool_approve"><?=$Lang['Status']['Activate']?></a>						
						<a href="javascript:js_handle_studentlog_setting('suspend');" class="tool_reject"><?=$Lang['Status']['Suspend']?></a>						
						<a href="javascript:js_show_studentlog_setting_add_edit_layer('edit');"><?=$Lang['Btn']['Edit']?></a>
						<a href="javascript:js_handle_studentlog_setting('delete');" class="tool_delete"><?php echo $Lang['Btn']['Delete']; ?></a>
					</div>
				
				</td>
			</tr>
		</thead>
	</table>	
	<table width="95%" border="0" cellspacing="0" cellpadding="0" class="common_table_list">
		<colgroup>
			<col style="width:4%">
			<col style="width:30%">
			<col style="width:20%">			
			<col style="width:15%">
			<col style="width:15%">
			<col style="width:16%">
		</colgroup>
		<thead>
			<tr class="tabletop">
				<th class="tabletoplink">#</th>
				<th class="tabletoplink"><?php echo $Lang['medical']['studentLog']['tableHeader']['Item']; ?></th>
				<th class="tabletoplink"><?php echo $Lang['General']['Code']; ?></th>
				<th class="tabletoplink"><?php echo $Lang['General']['Status2']; ?></th>
				<th class="tabletoplink"><?php echo $Lang['medical']['general']['tableHeader']['Default']; ?></th>				
 				<th class="tabletoplink"><input type="checkbox" name="studentLogSettingsCheckAll" id="studentLogSettingsCheckAll"></th>
			</tr>
		</thead>
		<tbody>
		<?php
			if (count($studentLogLev2)>0)
			{			
				$i=0;
				foreach( (array)$studentLogLev2 as $rs): ?>
				<tr>
					<td><?php echo ++$i;?></td>
<!--					<td><a href="javascript:js_edit('<?="StudentLogLev2ID_".$i?>');"><?php echo ($rs['Lev2Name'] ? stripslashes($rs['Lev2Name']) : "--"); ?></a></td>-->
					<td><?php echo ($rs['Lev2Name'] ? stripslashes($rs['Lev2Name']) : "--"); ?></td>
					<td><?php echo ($rs['Lev2Code'] ? stripslashes($rs['Lev2Code']) : "--"); ?></td>
					<td><?php echo ($rs['RecordStatus'] ? $Lang['medical']['general']['StatusOption']['InUse'] : $Lang['medical']['general']['StatusOption']['Suspend']); ?></td>
					<td><?php echo ($rs['IsDefault'] ? $linterface->Get_Tick_Image() : "&nbsp;"); ?></td>
					<td><input type="checkbox" name="StudentLogLev2ID[]" id="StudentLogLev2ID_<?=$i?>" value=<?php echo $rs['Lev2ID']; ?> class="panelSelection"></td>
				</tr>
		<?php endforeach;
			}
			else
			{?>
				<tr>
					<td class=tableContent align=center colspan="6"><br><?=$Lang['SysMgr']['Homework']['NoRecord']?><br><br></td>
				</tr>
		<?php
			}?>
		</tbody>
	</table>
<?php
	if (count($studentLogLev2)>0)
	{			
		print ' <span class="form_sep_title">'.
					$Lang['medical']['general']['tableSorting'].
				'</span>';
				
		$medical = new libMedical();
		$lev3BodyParts = $medical->getStudentLogPartsSettings();
		//debug_r($lev3BodyParts);
		
		$containsLev3BodyParts=false;
		foreach($studentLogLev2 as $rs)
		{
//			if($rs['Lev2Name'] == $medical_cfg['studentLog']['level2']['specialStatus'])
			if (($plugin['medical_module']['AlwaysShowBodyPart'] == true) || ($rs['Lev2Name'] == $medical_cfg['studentLog']['level2']['specialStatus']))
			{
				$containsLev3BodyParts = true;
				break;
			}
		}
		if ($containsLev3BodyParts && count($lev3BodyParts)>0)
		{			
?>		
	<h4><?=$Lang['medical']['studentlog_setting']['convulsionRemark']?></h4>
	<table border="0" cellspacing="0" cellpadding="0" align="left" class="common_table_list" style="width:30%;">	
		<colgroup>	
			<col style="width:20%">
			<col style="width:25%">	
			<col style="width:25%">			
		</colgroup>
		<thead>
			<tr class="tabletop">
				<th class="tabletoplink"><?php echo $Lang['medical']['studentLog']['bodyParts']; ?></th>
				<th class="tabletoplink"><?php echo $Lang['medical']['studentlog_setting']['convulsion']['syndrome']; ?></th>
				<th class="tabletoplink"><?php echo $Lang['General']['Code']; ?></th>
			</tr>
		</thead>
		<tbody>
		
		<?php			
			foreach( (array)$lev3BodyParts as $rs3): ?>
			<tr>
				<td><?php echo ($rs3["details"]["name"] ? stripslashes($rs3["details"]["name"]) : "--"); ?></td>					
				<td><?php 
					$child = $rs3["child"] ? $rs3["child"] : array();
//debug_r($child);	
					$i = 1;				
					foreach((array)$child as $k=>$v)
					{
						echo $i++ .". ". $v["name"] . "<br>";
					}
					if (count($child) == 0) 
						echo "--"
					?>
				</td>
				<td><?php 
					$child = $rs3["child"] ? $rs3["child"] : array();
//debug_r($child);	
					$i = 1;				
					foreach((array)$child as $k=>$v)
					{
						echo $v["code"] . "<br>";
					}
					if (count($child) == 0) 
						echo "--"
					?>
				</td>
			</tr>
		<?php endforeach; ?>
		
		
		
		<?php	/*	
			foreach( (array)$lev3BodyParts as $rs3): ?>
			<tr>
				<td><?php echo ($rs3["details"]["name"] ? stripslashes($rs3["details"]["name"]) : "--"); ?></td>					
				<td><?php 
					$child = $rs3["child"] ? $rs3["child"] : array();
//debug_r($child);	
					$i = 1;				
					foreach((array)$child as $k=>$v)
					{
						echo $i++ .". ". $v["name"] . " (" . $v["code"] . ")<br>";
					}
					if (count($child) == 0) 
						echo "--"
					?>
				</td>
			</tr>
		<?php endforeach; 
		*/?>
		
					
		<?php }?>
		</tbody>
	</table>	
<?php				
	}
?>			

</div>
<?php
}
else	// Lev1ID not exist
{
	// do nothing
}
?>