<?php
/*
 * 	using: 
 */

$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();	
$statusID = $_REQUEST["StatusID"];
if ($statusID)
{
	$objSLog = new studentSleepLev2();
	$filter = "StatusID=".$statusID." AND DeletedFlag<>1";	// don't show marked deleted records
	$orderBy = "ReasonCode";
	$studentSleepLev2 = $objSLog->getAllStatus($filter,$orderBy);
	//debug_r($studentSleepLev2);	
	
	if (!isset($c_ui))
	{
		$c_ui = new common_ui();	
	}

?>

<div id="viewResult">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<colgroup>
			<col style="width:70%">
			<col style="width:30%">
		</colgroup>
		<thead>
			<tr>
				<td valign="bottom" colspan="2">
					<div class="Conntent_tool"><br style="clear:both">				
<?php						
	$x = $linterface->Get_Thickbox_Link($c_ui->getThickBoxHeight(), $c_ui->getThickBoxWidth(), "add_dim", 
			$Lang['medical']['general']['button']['new_category'], "js_show_studentsleep_setting_add_edit_layer('add'); return false;",$InlineID="FakeLayer","&nbsp;".$Lang['Btn']['New']);
	echo $x;
?>	
					</div>												
				</td>
            </tr>
            <tr class="table-action-bar">
				<td valign="bottom" colspan="2">
					<div class="common_table_tool">						
						<a href="javascript:js_handle_studentsleep_setting('activate');" class="tool_approve"><?=$Lang['Status']['Activate']?></a>						
						<a href="javascript:js_handle_studentsleep_setting('suspend');" class="tool_reject"><?=$Lang['Status']['Suspend']?></a>						
						<a href="javascript:js_show_studentsleep_setting_add_edit_layer('edit');"><?=$Lang['Btn']['Edit']?></a>
						<a href="javascript:js_handle_studentsleep_setting('delete');" class="tool_delete"><?php echo $Lang['Btn']['Delete']; ?></a>
					</div>
				
				</td>
			</tr>
		</thead>
	</table>	
	<table width="95%" border="0" cellspacing="0" cellpadding="0" class="common_table_list">
		<colgroup>
			<col style="width:4%">
			<col style="width:30%">
			<col style="width:20%">			
			<col style="width:15%">
			<col style="width:15%">
			<col style="width:16%">
		</colgroup>
		<thead>
			<tr class="tabletop">
				<th class="tabletoplink">#</th>
				<th class="tabletoplink"><?php echo $Lang['medical']['studentSleep']['tableHeader']['Reason'] ?></th>
				<th class="tabletoplink"><?php echo $Lang['General']['Code']; ?></th>
				<th class="tabletoplink"><?php echo $Lang['General']['Status2']; ?></th>
				<th class="tabletoplink"><?php echo $Lang['medical']['general']['tableHeader']['Default']; ?></th>				
 				<th class="tabletoplink"><input type="checkbox" name="studentSleepSettingsCheckAll" id="studentSleepSettingsCheckAll"></th>
			</tr>
		</thead>
		<tbody>
		<?php
			if (count($studentSleepLev2)>0)
			{			
				$i=0;
				foreach( (array)$studentSleepLev2 as $rs): ?>
				<tr>
					<td><?php echo ++$i;?></td>
<!--					<td><a href="javascript:js_edit('<?="StudentSleepReasonID_".$i?>');"><?php echo ($rs['ReasonName'] ? stripslashes($rs['ReasonName']) : "--"); ?></a></td>-->
					<td><?php echo ($rs['ReasonName'] ? stripslashes($rs['ReasonName']) : "--"); ?></td>
					<td><?php echo ($rs['ReasonCode'] ? stripslashes($rs['ReasonCode']) : "--"); ?></td>
					<td><?php echo ($rs['RecordStatus'] ? $Lang['medical']['general']['StatusOption']['InUse'] : $Lang['medical']['general']['StatusOption']['Suspend']); ?></td>
					<td><?php echo ($rs['IsDefault'] ? $linterface->Get_Tick_Image() : "&nbsp;"); ?></td>
					<td><input type="checkbox" name="StudentSleepReasonID[]" id="StudentSleepReasonID_<?=$i?>" value=<?php echo $rs['ReasonID']; ?> class="panelSelection"></td>
				</tr>
		<?php endforeach;
			}
			else
			{?>
				<tr>
					<td class=tableContent align=center colspan="6"><br><?=$Lang['SysMgr']['Homework']['NoRecord']?><br><br></td>
				</tr>
		<?php
			}?>
		</tbody>
	</table>
<?php
	if (count($studentSleepLev2)>0)
	{			
		print ' <span class="form_sep_title">'.
					$Lang['medical']['general']['tableSorting'].
					'<br />'.
					$Lang['medical']['report_sleep']['reminder2'].
				'</span>';
	}
?>			

</div>

<?php
}
else	// StatusID not exist
{
	// do nothing
}
?>