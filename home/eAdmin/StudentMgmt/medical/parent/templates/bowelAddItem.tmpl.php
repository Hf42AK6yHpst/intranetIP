<?php 
// using: 
/*
 * 	Log
 */

$disStuentSleepType = $studentInfo["stayOverNight"] ? $Lang['medical']['general']['sleepOption']['StayIn']:$Lang['medical']['general']['sleepOption']['StayOut'];
//$attendance = $objMedical->getAttendanceStatusOfUser($sid,$date);
//$disAttendance = $attendance ? $Lang['medical']['bowel']['parent']['AttendanceOption']['Present']: $Lang['medical']['bowel']['parent']['AttendanceOption']['Absent'];

?>
<style>
.eventClass td{
	vertical-align:top;
}
a{
	color:#2286C5;
	cursor:pointer;
}
.errorMsg{
	color:red;
}

.table_row_tool{
	float:none;
	display:inline-block;
	color:#2286C5 !important;
	cursor:pointer !important;	
}
.delete_event_div{
	float:right !important;
	display:block;
}
legend {
	color:gray;
	font-style:italic;
	font-size: 16px;
}
</style>
<form method="post" action="index.php" name="form1" id="form1">
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<colgroup>
			<col style="width:20%"/>
			<col style="width:80%"/>
		</colgroup>
		<tbody>
		
			<tr>
				<td><?php echo $Lang['medical']['general']['form'];?></td>
				<td><?php echo $studentInfo['ClassName'] ?></td>
			</tr>
			<tr>
				<td><?php echo $Lang['medical']['general']['student'];?></td>
				<td><?php echo $studentInfo['Name'].' ('.$studentInfo['ClassNumber'].')' ?></td>
			</tr>
			<tr>
				<td><?php echo $Lang['medical']['general']['sleepType'];?></td>
				<td><?php echo $disStuentSleepType ?></td>
			</tr>
			<tr>
				<td><?php echo $Lang['medical']['bowel']['parent']['bowelDate'];?></td>
				<td><?php echo  $linterface->GET_DATE_PICKER($Name="r_date",$DefaultValue=$date,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="ChangeBowelDetail();",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum"); ?>
				&nbsp;<span id="AttendanceStatus">
<?php
	include_once('parent/ajax/getAttendance.php');
?>								
					  </span>
				</td>
			</tr>			
						
			<tr>
			<td colspan="2">
				<br />
				<span class="form_sep_title" style="font-size: 15px">
					<i>	-
					<?php echo $Lang['medical']['bowel']['parent']['bowelInfo'];?>
						-
					</i>
				</span>
				<br />
				<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
					<colgroup>
						<col style="width:70%"/>
						<col style="width:30%"/>
					</colgroup>
					<tbody>
						<tr>
							<td>
								<div id="viewResult">
<?php
	include_once('parent/ajax/getBowelEvent.php');
?>								
								</div>
							</td>
							<td></td>
						</tr>
					</tbody>
				</table>
				<hr style="width:80%;" align="left"/>
				<span class="table_row_tool addEvent"><a class="add newBtn" id="addEventImage" title="<?php echo $Lang['medical']['bowel']['parent']['new']; ?>"></a></span><a id="addEvent"><?php echo $Lang['medical']['bowel']['parent']['new']; ?></a>										
			</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td align="center" colspan="9" class="dotline"></td>
			</tr>			
			<tr>
				<td align="center" colspan="9">
					<input type="hidden" id="t" name="t" value="" />
					<input type="hidden" id="sid" name="sid" value="<?php echo $sid;?>" />
					<input type="hidden" id="Student" name="Student" value="<?php echo $_REQUEST["FilterStudent"];;?>" />
					<input type="hidden" id="SelYear" name="SelYear" value="<?php echo $_REQUEST["FilterYear"];?>" />
					<input type="hidden" id="SelMonth" name="SelMonth" value="<?php echo $_REQUEST["FilterMonth"];?>" />
					<input type="button" id="Save" name="Save" class="formbutton_v30 print_hide " value="<?php echo $Lang['medical']['meal']['button']['save']; ?>">					
					<input type="button" id="Cancel" name="Cancel" class="formbutton_v30 print_hide " value="<?php echo $Lang['medical']['meal']['button']['cancel']; ?>">
				</td>	
			</tr>
		</tfoot>
	</table>

</form>
<script>

function ChangeBowelDetail() {
//	alert("change");
	$.ajax({
		url : "?t=parent.ajax.getAttendance",			
		type : "POST",			
		data : "sid="+$('#sid').val()+"&date="+$('#r_date').val(),
		success : function(msg) {
			$('#AttendanceStatus').html(msg);
			GetBowelEvent();			
		}
	});
}

function GetBowelEvent() {
	$.ajax({
		url : "?t=parent.ajax.getBowelEvent",			
		type : "POST",			
		data : "sid="+$('#sid').val()+"&date="+$('#r_date').val(),
		success : function(msg) {
			$('#viewResult').html(msg);				
		}
	});	
}


// Check if there's duplicate reocrd by: BowelStatusID + Hour + Minute
function CheckDuplicate() {
	var ret = true;

	$('.ClsBowel').each(function() {
		if ($(this).attr('disabled') == false)
		{
			bowelID = $(this).attr('id');
			hourID = bowelID.replace('[bowelID]','[time_hour]');
			escHourID = hourID.replace(/\[/g,'\\[');
			escHourID = escHourID.replace(/\]/g,'\\]');			
			minuteID = bowelID.replace('[bowelID]','[time_min]');
			escMinuteID = minuteID.replace(/\[/g,'\\[');
			escMinuteID = escMinuteID.replace(/\]/g,'\\]');

			cBowelVal = $(this).val();
			
			cHourVal = $('#'+escHourID + ' option:selected').val();
			cMinuteVal = $('#'+escMinuteID + ' option:selected').val();

			$('.ClsBowel').each(function() {
				if ($(this).attr('id') != bowelID)
				{
					pBowelID = $(this).attr('id');
					
					pHourID = pBowelID.replace('[bowelID]','[time_hour]');
					pMinuteID = pBowelID.replace('[bowelID]','[time_min]');

					pEscHourID = pHourID.replace(/\[/g,'\\[');
					pEscHourID = pEscHourID.replace(/\]/g,'\\]');
					pEscMinuteID = pMinuteID.replace(/\[/g,'\\[');
					pEscMinuteID = pEscMinuteID.replace(/\]/g,'\\]');

					pBowelVal = $(this).val();
					
					pHourVal = $('#'+pEscHourID + ' option:selected').val();
					pMinuteVal = $('#'+pEscMinuteID + ' option:selected').val();
								
					if ((pBowelVal == cBowelVal) && (pHourVal == cHourVal) && (pMinuteVal == cMinuteVal))
					{
						ret = false;
					}
				}
				
			});
		}		
		
	});	
	
	return ret;
}

$(document).ready(function(){
	// Add Buttons
	// Delete Buttons
	
	var count = 1; // a count for new items
	var deleteFile = 0;
	var deleteEvent = 0;
	
	$('#Cancel').click(function(){
		$('#t').val('parent.bowelRec');
		$('#form1').submit();
	});
	$('#Save').click(function(){
		
		$('.errorMsg').remove();
		
		if (!CheckDuplicate())
		{
			alert('<?php echo $Lang['medical']['bowel']['parent']['duplicateRecord']; ?>');
			return;
		}
		
		if(deleteEvent){
			var isDeleted = confirm('<?php echo $Lang['medical']['report_general']['deleteEvent'] ?>');
			if(!isDeleted){
				return;
			}
		}
		
		$('#t').val('parent.bowelAddItemSave');
		$('#form1').submit();
	});
	
	
	// Add Buttons Start
	$('#addEvent').hover(
		function(){
		$('#addEventImage').css('background-position','-20px -40px');
		},
        function () {
			$('#addEventImage').css('background-position','0px -40px');
        }
	);
	$('#addEvent, .newBtn').click(function(){		
		$.ajax({
			url : "?t=parent.ajax.getNewBowelEvent",
			type : "POST",
			data : 'Id=n'+count,
			success : function(msg) {
				$('#dummySpan').after(msg);
				$('html, body').animate({scrollTop:250}, 'slow');
				count++;
			}
		});	
	});
	// Add Buttons End
	
		
	$('.deleteEventBtn').live('click', function(){
		$(this).parent().parent().parent().parent().parent().parent().next().remove();
		$(this).parent().parent().parent().parent().parent().parent().next().remove();
		$(this).parent().parent().parent().parent().parent().parent().remove();
		deleteEvent = 1;
	});
	// Delete Buttons End

	<?php if($_POST['action']=='new'): ?>
		$('#addEvent').click();
	<?php endif;?>

});

</script>