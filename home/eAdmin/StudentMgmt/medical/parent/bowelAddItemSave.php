<?php
// using:
/*
 * 	Log:
 * 	Date:	2014-02-27 [Cameron] Need to check $sid to avoid saving wrong data if the student is not the corresponding child of login parent
 */

if (!$objMedical->getParentViewAccess('bowel')){
	header("Location: /");
	exit();
}

$sid = trim($_POST['sid']);

$validChild = $objMedical->isChildOfTheParent($_SESSION['UserID'],$sid);
if (!$validChild)		// Not child of the parent, redirect to home directory
{
	header("Location: /");
	exit();
}

$r_date = trim($_POST['r_date']);
$date = trim($_POST['date']);
$selYear = "";
$selMonth = "";

$result = array();

if (($sid == "") || ($date == "") || ($r_date > date("Y-m-d")) )
{
	$result[] = 0;	// error:
}
else
{
	// For redirect back to list view
	$selYear = date("Y",strtotime($date));
	$selMonth = date("m",strtotime($date));
	
	$eventIDArray = explode(',',$eventIDArray);
	$deleteEventIDArray = $eventIDArray;

	// 1. Get $deleteEventIDArray
	foreach( (array)$event as $key=>$details)
	{
		if(strpos($key,'n') === 0 )
		{
			//new record , do nothing
		}else
		{
			if($key > 0 && is_numeric(intval($key))){
				$deleteEventIDArray = array_diff((array)$deleteEventIDArray, (array)$key);
			}else
			{
				//not a valid key value , skip this record
				continue; 			
			}
		}	
	}

	// 2. Delete Records
	if(!empty($deleteEventIDArray) && count($deleteEventIDArray)>0) {
		if (implode(",",$deleteEventIDArray) != "")
		{
			$result[] = $objMedical->deleteBowelLogRecords($deleteEventIDArray, $date);
		}
	}
	
	// 3. Add/Update records
	foreach( (array)$event as $key=>$details)
	{
		if(strpos($key,'n') === 0 )
		{
			//new record
			$objBowelLog = new BowelLog($r_date);
		}else
		{
			if($key > 0 && is_numeric(intval($key))){
				//assign the bowel log recordid for update
				$objBowelLog = new BowelLog($r_date,$key);
				//$objBowelLog->setRecordID($key);
	
//				$eventIDArray = array_diff((array)$eventIDArray, (array)$key);
			}else
			{
				//not a valid key value , skip this record
				continue; 			
			}
		}	
		
		$objBowelLog->setUserID($sid);
		$objBowelLog->setRecordTime($r_date.' '.$details['time_hour'].':'.$details['time_min']);
		$objBowelLog->setRemarks(trim( htmlentities($details['remarks'], ENT_QUOTES, 'UTF-8')));
		$objBowelLog->setBowelID($details['bowelID']);
		$objBowelLog->setInputBy($medical_currentUserId);
		$objBowelLog->setModifiedBy($medical_currentUserId);
		$result[] = $objBowelLog->save();
		
	}
	
}

if(in_array(0, $result)){
	$Msg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}
else{
	$Msg = $Lang['General']['ReturnMessage']['UpdateSuccess'];	
}

header('Location: ?t=parent.bowelRec&Msg='.$Msg.'&SelYear='.$selYear.'&SelMonth='.$selMonth.'&Student='.$sid);

?>