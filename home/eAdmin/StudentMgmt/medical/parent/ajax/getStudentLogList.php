<?php
/*
 * 	using:
 * 
 * 	Date:	2014-02-27 [Cameron] Need to check $sid to avoid getting wrong data if the student is not the corresponding child of login parent
 */



//print "FilterStudent=" . $_POST["FilterStudent"] ."<br>";
//print "FilterYear=" . $_POST["FilterYear"] ."<br>";
//print "FilterMonth=" . $_POST["FilterMonth"] ."<br>";
$filterStudent = ($_POST["FilterStudent"])? $_POST["FilterStudent"] : $selectedStudent;
$filterYear = ($_POST["FilterYear"])? $_POST["FilterYear"] : $defaultYear;
$filterMonth = ($_POST["FilterMonth"])? $_POST["FilterMonth"] : $defaultMonth; 
 
$validChild = $objMedical->isChildOfTheParent($_SESSION['UserID'],$filterStudent);
if (!$validChild)		// Not child of the parent, redirect to home directory
{
	exit();
}
 
//print "student: $filterStudent, $filterYear, $filterMonth<br>"; 
$studentDetailList = $objMedical->getStudentLogListByParent($filterStudent, $filterYear, $filterMonth);
//debug_r($studentDetailList);

$objStudentLogLev1 = new studentLogLev1();
$lev1Detail = $objStudentLogLev1->getActiveStatus();
$lev1List = array();
foreach ($lev1Detail as $lev1) {
	$lev1List[$lev1['Lev1ID']] = $lev1['Lev1Name'];
}
$objStudentLogLev2 = new studentLogLev2();
$lev2Detail = $objStudentLogLev2->getActiveStatus();
$lev2List = array();
foreach ($lev2Detail as $lev2) {
	$lev2List[$lev2['Lev2ID']] = $lev2['Lev2Name'];
}

?>


<table border="0" cellpadding="5" cellspacing="0" align="center" class="common_table_list" style="width: 95%;">
	<colgroup>
		<col style="width:3%"/>
		<col style="width:10%"/>
		<col style="width:10%"/>
		<col style="width:10%"/>
		<col style="width:10%"/>
		<col style="width:10%"/>
		<col style="width:20%"/>
		<col style="width:12%"/>
		<col style="width:15%"/>
	</colgroup>
	<thead>
		<tr class="tabletop">
			<?php foreach( $Lang['medical']['studentLog']['parent']['tableHeader'] as $tableHeader): ?>
				<th class="tabletoplink"><?php echo $tableHeader; ?></th>
			<?php endforeach; ?>
		</tr>		
	</thead>
	<tbody>
<?php
	if (($studentDetailList == false) || count($studentDetailList)==0)
	{
?>
		<tr>
			<td colspan="8"  class="tableContent" align="center" >
			<?=$Lang['General']['NoRecordFound']?>
			</td>
		</tr>
<?php 	
	}
	else
	{
		$i = 1;
		foreach ($studentDetailList as $date=>$details) {
			$countDayRecord = count($studentDetailList[$date]);
			$data = array_shift(array_values($details));
			$attendance = ($data[0]['Attendance']==1)? $Lang['medical']['bowel']['parent']['AttendanceOption']['Present']:$Lang['medical']['bowel']['parent']['AttendanceOption']['StayAtHome'];
?>
			<tr>
				<td rowspan="<?=$countDayRecord?>"><?=($i++)?></td>
				<td rowspan="<?=$countDayRecord?>"><?=$date?></td>
				<td rowspan="<?=$countDayRecord?>"><?=$attendance?></td>
<?php 
				$row = 0;
				foreach ($details as $time=>$record) {
					if($row++){
						echo '<tr>';	
					}
?>
					<td><a class="editBtn" id="editRecord" data-date="<?=$date?>"><?=$time?></a></td>
					<td>
						<?php foreach ($record as $r) { ?>
							<?=$lev1List[$r['BehaviourOne']]?><br />
						<?php } ?>
					</td>
					<td>
						<?php foreach ($record as $r) { ?>
							<?=$lev2List[$r['BehaviourTwo']]?><br />
						<?php } ?>
					</td>
					<td>
						<?php foreach ($record as $r) { ?>
							<?= ($r['Remarks'] == '')? '-' : nl2br(stripslashes($r['Remarks'])) ?><br />
						<?php } ?>
					</td>
					<td>
						<?php foreach ($record as $r) { ?>
							<?= $r['ModifyBy'] ?><br />
						<?php } ?>
					</td>
					<td>
						<?php foreach ($record as $r) { ?>
							<?= $r['DateModified'] ?><br />
						<?php } ?>
					</td>
<?php 
					echo '</tr>';
				}
?>
			</tr>
<?php 
		}
	}
				
?>
	</tbody>

</table>

