<?php
// using: Adam
// http://192.168.0.146:31002/home/eAdmin/StudentMgmt/medical/?t=test.chart

echo date('Y-m-d',strtotime('13-03-2011 99:52'));

$nameOfGraph = 'Name of Line Graph';
$headerList = array('a','b','c','d');

$row[0]['dataName'] = 'testing Data';
$row[0]['data'] = array(1,2.4,1.33,4);

$row[1]['dataName'] = 'testing Data2';
$row[1]['data'] = array(2,3,4,5);
$statusColorList = array('#AA3456');
//"Select * From MEDICAL_HOWEL_STATUS Where StatusID In ('".implode("','", (array)$statusID)."')";
//$rs = $objDB->returnDataSet($sql);
?>
<meta http-equiv="X-UA-Compatible" content="IE=8" /> 
<link href="<?php echo $PATH_WRT_ROOT?>home/library_sys/reports/css/visualize.css" type="text/css" rel="stylesheet" />
<link href="<?php echo $PATH_WRT_ROOT?>home/library_sys/reports/css/visualize-light.css" type="text/css" rel="stylesheet" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script>
<!--[if IE]><script type="text/javascript" src="<?php echo $PATH_WRT_ROOT?>home/library_sys/reports/js/excanvas.js"></script><![endif]-->
<script type="text/javascript" src="<?php echo $PATH_WRT_ROOT?>home/library_sys/reports/js/visualize.jQuery.js"></script>	

<span id="y-axis-Label" style="float:left;z-index:99;">Y Axis Label</span>	
<div id="chartArea">
	<table id="medical_line_graph">
		<caption><?php echo $nameOfGraph ?></caption>
		<thead>
			<tr>
				<td></td>
				<?php foreach($headerList as $header): ?>
					<th scope="col"><?php echo $header ?></th>
				<?php endforeach; ?>
				
			</tr>
		</thead>
		<tbody>
			<?php foreach((array)$row as $rowDetail): ?>
			<tr>
				<th scope="row"><?php echo $rowDetail['dataName'];?></th>
				<?php foreach( (array)$rowDetail['data'] as $rowItem): ?>
					<td><?php echo $rowItem; ?></td>
				<?php endforeach; ?>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</div>


<script>
	$(document).ready( function() {
		var line_graph_colors = ['<?php echo implode("','",(array)$statusColorList)?>'];
		$('table').visualize({type: 'line', width: '660px', height : '200px', lineWeight:'2',colors : line_graph_colors});
//		$('table').visualize({type: 'pie', height: '300px', width: '420px'});
	});
</script>

