<?php
// using: 

if( (!$plugin['medical']) || ( $_SESSION["UserType"] != USERTYPE_STAFF ) || (!$plugin['medical_module']['message']) ){
	header("Location: /");
	exit();
}


include_once($PATH_WRT_ROOT."includes/cust/medical/libMedicalMessage.php");

$objMedicalMessage = new MedicalMessage();

$userIDList = array();
foreach( (array)$_POST['r_group_user'] as $userID){
	$userIDList[] = ltrim($userID, 'U');
}


$result = $objMedicalMessage->sendMessage($sender=$_SESSION['UserID'], $receiverArr=$userIDList, $message=$_POST['Description']);

header("Location:?t=message.readSendMessage" );
?>