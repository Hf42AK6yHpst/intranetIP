<?php
// using: 

if( (!$plugin['medical']) || ( $_SESSION["UserType"] != USERTYPE_STAFF ) || (!$plugin['medical_module']['message']) ){
	header("Location: /");
	exit();
}


$CurrentPage = "RemainderRead";
$TAGS_OBJ[] = array($Lang['medical']['menu']['readMessage'], "", 0);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();


include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");


$objDB = new libdb();

$arrCookies[] = array("ck_receive_message_page_size", "numPerPage");
$arrCookies[] = array("ck_receive_message_page_no", "pageNo");
$arrCookies[] = array("ck_receive_message_page_order", "order");
$arrCookies[] = array("ck_receive_message_page_field", "field");
$arrCookies[] = array("ck_receive_message_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1){
	clearCookies($arrCookies);
}else{
	updateGetCookies($arrCookies);
}
if ($page_size_change == 1){
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
if (isset($ck_page_size) && $ck_page_size != "") {
	$page_size = $ck_page_size;
}
$pageSizeChangeEnabled = true;

if(empty($order) && empty($field)){ // default sort by datetime
	$order = 0;
	$field = 2;
}

$li = new libdbtable2007($field, $order, $pageNo);

$senderNameField = getNameFieldByLang2('iu.');
$conds = " AND mmr.Receiver = '{$_SESSION['UserID']}'";
$conds .= " AND mmr.Message LIKE '%$keyword%'";
$sql = "SELECT
			{$senderNameField} AS Sender, 
			mmr.Message AS Message, 
			mmr.ReceiveTime AS ReceiveTime,
			CONCAT('<input type=\'checkbox\' name=\'receiveID[]\' id=\'receiveID[]\' value=', ReceiveID ,'>') as checkbox,
			mmr.ReceiveID AS ReceiveID
		FROM 
			MEDICAL_MESSAGE_RECEIVE mmr
		INNER JOIN
			INTRANET_USER iu
		ON
			mmr.Sender = iu.UserID
		WHERE
			1=1
			$conds ";				
$li->sql = $sql;
$li->field_array = array("Sender", "Message", "ReceiveTime");
$li->no_col = sizeof($li->field_array)+2;
//$li->IsColOff = "UserMgmtAlumniAccount";
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th class='num_check'>#</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang['medical']['message']['sender'])."</th>\n";
$li->column_list .= "<th width='70%' >".$li->column($pos++, $Lang['medical']['message']['content'])."</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang['medical']['message']['date'])."</th>\n";
$li->column_list .= "<th width='1'>".$li->check("receiveID[]")."</th>\n";

#### Set has read START ####
$sql = $li->built_sql();
$result = $objDB->returnResultSet($sql);
if(count($result)>0){
	$receiveIdList = '';
	foreach($result as $r){
		$receiveIdList .= $r['ReceiveID'] . ',';
	}
	$receiveIdList = trim($receiveIdList,',');
	$sql = "UPDATE MEDICAL_MESSAGE_RECEIVE SET HasRead='1' WHERE ReceiveID IN ({$receiveIdList})";
	$objDB->db_db_query($sql);
}
#### Set has read END ####


$linterface->LAYOUT_START($Msg);
?>


<form name="form1" id="form1" method="post" action="">

<div class="content_top_tool" style="float:left;">
	<?=$linterface->Get_Search_Box_Div("keyword", $keyword)?>
<br style="clear:both" />
</div>

<div class="table_board">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td valign="bottom">
		<div class="common_table_tool">
			<a id="deleteMail" href="javascript:void(0)" class="tool_delete"><?=$button_delete?></a>
		</div>
	</td>
</tr>
</table>

<?= $li->display()?>
	
<input type="hidden" name="uid" id="uid" value="" />
<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />

<input type="hidden" name="deleteAction" id="deleteAction" value="receivedMail" />
<input type="hidden" name="Msg" id="Msg" value="" />
</form>


<script>
	$('#deleteMail').click(function(){
		if($('input[id^="receiveID"]:checked').length == 0){
			alert(globalAlertMsg1);
			return;
		}
		if(!confirm(globalAlertMsg3)){
			return;
		}
		
		$.get('index.php?t=message.ajax.ajaxDeleteMessage',$( "#form1" ).serialize(),function(data){
			$('#Msg').val(data);
			$( "#form1" ).submit();
		});
	});
</script>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>