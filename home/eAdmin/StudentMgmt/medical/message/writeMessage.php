<?php
// using: 

if( (!$plugin['medical']) || ( !$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='SEND_NOTICE') ) || (!$plugin['medical_module']['message']) ){
	header("Location: /");
	exit();
}


$CurrentPage = "RemainderWrite";
$TAGS_OBJ[] = array($Lang['medical']['menu']['writeMessage'], "", 0);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();		
//$MODULE_OBJ['title'] = $Lang['medical']['menu']['meal'];

$objDB = new libdb();


#### Receiver Selection box START ####
$user_selected = $linterface->GET_SELECTION_BOX($array_student, "name='r_group_user[]' id='r_group_user[]' class='select_studentlist' size='7' style='width:400px' multiple='multiple'", "");
$button_remove_html = $linterface->GET_BTN($button_remove, "button", "javascript:checkOptionRemove(document.getElementById('r_group_user[]'))");
#### Receiver Selection box END ####

#### Message Template START ####
$sql = "SELECT DefaultID, Title FROM MEDICAL_DEFAULT_MAIL WHERE RecordStatus = '1'";
$rs = $objDB->returnResultSet($sql);
$messageTemplateHTML = '<select id="messageTemplate" style="width:50%;margin-bottom:5px;">';
$messageTemplateHTML .= "<option value=''>{$Lang['medical']['message']['defaultHint']}</option>";
foreach($rs as $r){
	$messageTemplateHTML .= "<option value='{$r['DefaultID']}'>{$r['Title']}</option>";
}
$messageTemplateHTML .= '</select>';
#### Message Templete END ####

#### HTML Editor START ####
include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
$objHtmlEditor = new FCKeditor ( 'Description' , "100%", "320", "", "CustMedicalMessageSet", htmlspecialchars_decode($Description));
$htmlEditor = $objHtmlEditor->CreateHtml();
$home_header_no_EmulateIE7 = false; // FCK Editor does not support IE11, but it does support IE7 @口@
#### HTML Editor END ####

#### Get filter list for select user START #### 
$filterList = $objMedical->getAccessRightUserIdList($cfg_medical['ACCESS_RIGHT']['NOTICE']['RECEIVE']['RIGHT']);
#### Get filter list for select user END ####

$linterface->LAYOUT_START($Msg);
$c_ui = new common_ui();
$js_css = $c_ui->Include_JS_CSS();
echo $js_css;
?>


<style>
textarea{
	min-width: 50%;
	min-height: 200px;
}

</style>
<form name="form1" method="post" action="?t=message.sendMessage" onsubmit="return checkForm();">
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tbody>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['message']['receiver'] ?></td>
			<td>
				<span style="float:left;">
					<?php echo $user_selected; ?>
				</span>
				<?php 
					echo $linterface->GET_BTN($button_select, "button", "javascript: $('#thickboxTest').click();addFilter();");
					echo '<br/>';
					echo $button_remove_html;
					echo '<br/>';
					echo '<a style="display:none;" id="thickboxTest" class="thickbox" href="/home/common_choose/index.php?fieldname=r_group_user[]&page_title=SelectMembers&permitted_type=1&excluded_type=4&from_thickbox=true&from_medical_write_message=true&KeepThis=true&TB_iframe=true&height=500&width=600">test</a>';
					echo '<br style="clear:both;" / >';
					echo '<span class="form_sep_title">'.
							$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].
						'</span>';
				?>
			</td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?php echo $Lang['medical']['message']['sendContent']; ?></td>
			<td class="tabletext" width="80%">
				<?php echo $messageTemplateHTML?><br/>
				<?php echo $htmlEditor?>
			</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<td align="center" colspan="2">
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_reset, "reset","javascript:resetForm()")?>&nbsp;
			</td>	
		</tr>
	</tfoot>
</table>
</form>


<script>
function addFilter(){
	var filter = [
		<?php echo implode($filterList,',');?>
	];
	$('body').find('#TB_window iframe').load(function() {
		var form = $(this).contents().find('form');
		$.each(filter,function(key,value){
			form.append('<input type="hidden" name="filterUser[]" value="'+value+'" />');
		});
	});
}

function checkForm()
{
    obj = document.form1;

    if(obj.elements["r_group_user[]"].length == 0) {
        alert('<?=$Lang['medical']['message']['AlertMessage']['FillReceiver']?>');
        return false;
    }

    /* There is no way to check empty
    if($.trim($('#Description').val()) == '') {
        alert('<?=$Lang['medical']['message']['AlertMessage']['FillMessage']?>');
        return false;
    }
    */

    checkOptionAll(obj.elements["r_group_user[]"]);
    return true;
}

$('#messageTemplate').change(function(){
	var DefaultID = $(this).val();
	if(DefaultID != ''){
		$.get('index.php?t=message.ajax.ajaxGetWriteMessageTemplate',{
			DefaultID: DefaultID
		},function(data){
			if(data != ''){
				appendEditorContent(data);
			}
		});
	}
});

//dynamically change HTML editor
function appendEditorContent(value)
{
	var Editor1 = FCKeditorAPI.GetInstance('Description');
	var oldContent = Editor1.GetHTML();
	Editor1.SetHTML(oldContent + ' ' + value);
}
</script>
<?php 
$linterface->LAYOUT_STOP();
?>