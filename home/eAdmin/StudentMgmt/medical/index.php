<?php
// using:
/*
 * 2018-03-26 Cameron
 * include libStaffEventLev1.php and libStaffEventLev2.php
 *
 * 2017-08-29 Cameron
 * place intranet_opendb() before libMedical()
 *
 * 2017-06-12 Cameron
 * add include libdbtable.php and libdbtable2007a.php
 */
// http://192.168.0.146:31002/home/eAdmin/StudentMgmt/medical/?t=settings.index
$PATH_WRT_ROOT = "../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "lang/cust/medical_lang." . $intranet_session_language . ".php");
include_once ($PATH_WRT_ROOT . "includes/cust/medical/medicalConfig.inc.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/cust/common_ui.php");
include_once ($PATH_WRT_ROOT . "includes/cust/common_function.php");
include_once ($PATH_WRT_ROOT . "includes/cust/medical/libMedical.php");
include_once ($PATH_WRT_ROOT . "includes/cust/medical/libMedicalShowInput.php");
include_once ($PATH_WRT_ROOT . "includes/cust/medical/libMealStatus.php");
include_once ($PATH_WRT_ROOT . "includes/cust/medical/libBowelLog.php");
include_once ($PATH_WRT_ROOT . "includes/cust/medical/libBowelStatus.php");
include_once ($PATH_WRT_ROOT . "includes/cust/medical/libStudentLog.php");
include_once ($PATH_WRT_ROOT . "includes/cust/medical/libStudentLogLev1.php");
include_once ($PATH_WRT_ROOT . "includes/cust/medical/libStudentLogLev2.php");
include_once ($PATH_WRT_ROOT . "includes/cust/medical/libStudentSleepLev1.php");
include_once ($PATH_WRT_ROOT . "includes/cust/medical/libStudentSleepLev2.php");
include_once ($PATH_WRT_ROOT . "includes/cust/medical/libStudentSleepLog.php");
include_once ($PATH_WRT_ROOT . "includes/cust/medical/libMealDailyLog.php");
include_once ($PATH_WRT_ROOT . "includes/cust/medical/libStudentLogDocument.php");
include_once ($PATH_WRT_ROOT . "includes/cust/medical/libEventType.php");
include_once ($PATH_WRT_ROOT . "includes/cust/medical/libStaffEventLev1.php");
include_once ($PATH_WRT_ROOT . "includes/cust/medical/libStaffEventLev2.php");
include_once ($PATH_WRT_ROOT . "includes/form_class_manage_ui.php");
include_once ($PATH_WRT_ROOT . "includes/form_class_manage.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "includes/libclass.php");
include_once ($PATH_WRT_ROOT . "includes/json.php");

intranet_auth();
if (! $plugin['medical']) {
    header("Location: /");
    exit();
}
$ma = trim($ma); // module ajax suppose , 1 => is an ajax module call or 'empty' (default)
$t = trim($t);
if ($t == '') {}

// This cust module don't allow IE7 to open
$home_header_no_EmulateIE7 = true;

// Prevent searching parent directories, like 'task=../../index'.
$t = str_replace("../", "", $t);

$t = str_replace('.', '/', $t);

$mod_script = $t . '.php';
//
intranet_opendb();
$objMedical = new libMedical();

$CurrentPageArr['medical'] = 1;

$medical_currentUserId = $_SESSION['UserID'];

// access right check
// checkAccessRight($_SESSION['UserID'],'');

$templateLayout = $popup ? 'popup.html' : 'default.html';
$linterface = new interface_html($templateLayout);
$Msg = urldecode($Msg);
if (file_exists($mod_script)) {
    include ($mod_script);
} else {
    echo 'file not found<br/>';
}
intranet_closedb();
exit();
?>