<?php 


$objDB = new libdb();

######## SQL Safe START ########
$picID = IntegerSafe($picID, ',');
######## SQL Safe END ########



######## Get Data START ########
$picList = $picID;
if($picList != ''){
	$nameField = getNameFieldByLang2("IU.")." As Name";
	$sql = "SELECT {$nameField} FROM INTRANET_USER IU WHERE UserID IN ({$picList})";
	$rs = $objDB->returnResultSet($sql);
}else{
	$rs = array();
}
######## Get Data END ########


$ui = new common_ui();



@ob_start();
?>

<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">
	<tr class="tabletop">
		<td align="center" valign="middle">#</td>
		<td><?=$Lang['medical']['general']['pic']?></td>
	</tr>
	<?php foreach ($rs as $index=>$r) { ?>
		<tr class="tablerow1">
			<td><?=($index+1)?></td>
			<td><?=$r['Name']?></td>
		</tr>
	<?php } ?>
</table>
<?php
$data = @ob_get_contents();
@ob_end_clean();


echo $ui->generatePopUpBox($data, '140px', '200px');
?>

