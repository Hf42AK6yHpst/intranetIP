<?php
/*
 * Using:
 *
 * 2018-04-04 Cameron
 * - hide filter options only if both student log list and event list are shown in portal
 *
 * 2018-03-28 Cameron
 * - add filter by Group
 *
 * 2017-12-14 Cameron
 * - add form class filter [case #P132008]
 */
include_once ($PATH_WRT_ROOT . "lang/cust/medical_lang." . $intranet_session_language . ".php");
include_once ($PATH_WRT_ROOT . "includes/form_class_manage_ui.php");

$medicalPath = $PATH_WRT_ROOT . '/home/eAdmin/StudentMgmt/medical';
if ($plugin['medical_module']['homepageHint']['studentLog']) {
    $includeNoticePath = $medicalPath . '/homepage/studentLogNotice.php';
}

if ((empty($medicalNoticeStartDate)) || (! preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $medicalNoticeStartDate))) {
    $medicalNoticeStartDate = date('Y-m-d', getStartOfAcademicYear());
}
if ((empty($medicalNoticeEndDate)) || (! preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $medicalNoticeEndDate))) {
    $medicalNoticeEndDate = date('Y-m-d');
}
$startDate = $linterface->GET_DATE_PICKER($Name = "medicalNoticeStartDate", $DefaultValue = $medicalNoticeStartDate, $OtherMember = "", $DateFormat = "yy-mm-dd", $MaskFunction = "", $ExtWarningLayerID = "", $ExtWarningLayerContainer = "", $OnDatePickSelectedFunction = "", $ID = "", $SkipIncludeJS = 0, $CanEmptyField = 0, $Disable = false, $cssClass = "textboxnum");
$endDate = $linterface->GET_DATE_PICKER($Name = "medicalNoticeEndDate", $DefaultValue = $medicalNoticeEndDate, $OtherMember = "", $DateFormat = "yy-mm-dd", $MaskFunction = "", $ExtWarningLayerID = "", $ExtWarningLayerContainer = "", $OnDatePickSelectedFunction = "", $ID = "", $SkipIncludeJS = 0, $CanEmptyField = 0, $Disable = false, $cssClass = "textboxnum");

if ($_POST['StudentLogByMethod'] == 'ByGroup') {
    $selectedByClass = '';
    $selectedByGroup = 'selected';
    $classStyle = 'style="display:none"';
    $groupStyle = '';
} else {
    $selectedByClass = 'selected';
    $selectedByGroup = '';
    $classStyle = '';
    $groupStyle = 'style="display:none"';
}

$objFCM_UI = new form_class_manage_ui();
$formList = $objFCM_UI->Get_Class_Selection($AcademicYearID = Get_Current_Academic_Year_ID(), $YearID = '', $ID_Name = 'classID', $SelectedYearClassID = $classID, $Onchange = '', $noFirst = 0, $isMultiple = 0, $isAll = 1, $TeachingOnly = 0, $firstTitle = '', $style = $classStyle);

// Group Filter
$groupFilter = $objMedical->getGroupSelection("groupID", $groupID, $groupStyle, $Lang['medical']['event']['AllGroup']);

$lev1List = $objMedical->getStudentLogAllActiveLev1();
$lev1Array = array();
foreach ($lev1List as $lev1) {
    $lev1Array[$lev1['Lev1ID']] = $lev1['Lev1Name'];
}
$filterLev1HTML = getSelectByAssoArray($lev1Array, 'name="medicalNoticeTypeFilter" id="medicalNoticeTypeFilter"', $medicalNoticeTypeFilter);

$lev2List = $objMedical->getStudentLogAllActiveLev2();
$lev2Array = array();
foreach ($lev2List as $lev2) {
    if ($lev2['Lev1ID'] == $medicalNoticeTypeFilter) {
        $lev2Array[$lev2['Lev2ID']] = $lev2['Lev2Name'];
    }
}
if (empty($lev2Array)) {
    $filterLev2HTML = '<select name="medicalNoticeItemFilter" id="medicalNoticeItemFilter"></select>';
} else {
    $filterLev2HTML = getSelectByAssoArray($lev2Array, 'name="medicalNoticeItemFilter" id="medicalNoticeItemFilter"', $medicalNoticeItemFilter);
}

?>

<tr>
	<td>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td height="24">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="8"><img
								src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_01.gif"
								width="8" height="24"></td>
							<td
								background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_02.gif">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td class="indexnewstitle"><?=$Lang['medical']['menu']['studentLog']?></td>
										<td align="right">&nbsp;</td>
									</tr>
								</table>
							</td>
							<td width="8"><img
								src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_03.gif"
								width="8" height="24"></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%" height="100%" border="0" cellpadding="0"
			cellspacing="0">
			<tr>
				<td width="5"
					background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_07.gif"><img
					src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_07.gif"
					width="5" height="5"></td>
				<td valign="top" bgcolor="#FFFFFF" height="100%">
<?php if ($plugin['medical_module']['homepageHint']['studentLog'] && $plugin['medical_module']['homepageHint']['event']):?>
	<?php $filterStyle = 'none'; ?>				
    				<span id="studentLogSpanShowOption" class="spanShowOption"
					style="display: '';"> <a href="#" id="showStudentLogFilter"><?php echo $Lang['Btn']['ShowOption'];?></a>
				</span> <span id="studentLogSpanHideOption" class="spanHideOption"
					style="display: none;"> <a href="#" id="hideStudentLogFilter"><?php echo $Lang['Btn']['HideOption'];?></a>
				</span>
<?php else:?>
	<?php $filterStyle = '';?>    				
<?php endif;?>				
					<form name="formMedicalNotice" id="formMedicalNotice" method="POST"
						action="">
						<div style="margin-top: 3px;display:<?php echo $filterStyle;?>;" id="medicalStudentLogFilter">
						<?=$Lang['medical']['meal']['searchMenu']['date']?>:&nbsp;
						<?=$startDate?>
						<?=$Lang['StaffAttendance']['To']?>&nbsp;
						<?=$endDate?>
						<span> <select name="StudentLogByMethod" id="StudentLogByMethod">
									<option value="ByClass" <?php echo $selectedByClass;?>><?php echo $Lang['Header']['Menu']['Class'];?></option>
									<option value="ByGroup" <?php echo $selectedByGroup;?>><?php echo $Lang['medical']['meal']['searchMenu']['group'];?></option>
							</select>
							</span> <span id="StudentLogByMethodSpan">
						<?=$formList .$groupFilter?>
						</span> <br />
						<?=$Lang['medical']['studentLog']['studentLog']?>:&nbsp;
						<?=$filterLev1HTML?>
						<?=$Lang['medical']['studentLog']['case']?>:&nbsp;
						<?=$filterLev2HTML?>
						<?= $linterface->GET_ACTION_BTN($Lang['Btn']['View'], 'button',$ParOnClick="", $ParName="r_search")?>&nbsp;
					</div>

						<div id="medicalNoticeResult">
						<?php include($includeNoticePath); ?>
					</div>
					</form>
				</td>
				<td width="5"
					background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_09.gif"><img
					src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_09.gif"
					width="5" height="5"></td>
			</tr>
			<tr>
				<td width="5" height="6"><img
					src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_10.gif"
					width="5" height="6"></td>
				<td height="6"
					background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_11.gif"><img
					src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_11.gif"
					width="5" height="6"></td>
				<td width="5" height="6"><img
					src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_12.gif"
					width="5" height="6"></td>
			</tr>
		</table>
	</td>
</tr>

<script>
function checkDateValid(start,end){
	// .replace() is for IE7 
	var _start = new Date(start.replace(/-/g,'/'));
	var _end = new Date(end.replace(/-/g,'/'));
	return start <= end;
}

$('#medicalNoticeTypeFilter').change(function(){
	if($(this).val() == ''){
		$('#medicalNoticeItemFilter').html('');
	}else{
		$.post('/home/eAdmin/StudentMgmt/medical/?t=management.ajax.getStudentLogLevelList',{
			'action': '2',
			'data': $(this).val()
		},function(res){
			$('#medicalNoticeItemFilter').html('<option value="" selected=""> -- <?=$button_select?> -- </option>'+res);
			$("#medicalNoticeItemFilter").val($("#medicalNoticeItemFilter option:first").val());
			$("#medicalNoticeItemFilter").width($("#medicalNoticeItemFilter").width()); // IE is suck!
		});
	}
});

$(document).ready(function(){
	$('#StudentLogByMethod').change(function(){
		if ($('#StudentLogByMethod').val() == 'ByClass') {
			$('#groupID').css('display','none');
			$('#classID').css('display','');
			$('#groupID').val('');
			getStudentLogList();
		}
		else {
			$('#groupID').css('display','');
			$('#classID').css('display','none');
			$('#classID').val('');
			getStudentLogList();
		}
	});

 	$('#classID, #groupID').change(function(e){
 		e.preventDefault();
 		getStudentLogList();
 	});

 	$('#r_search').click(function(e){
 		e.preventDefault();
 		getStudentLogList();
 	});

 	$('#showStudentLogFilter').click(function(e){
 		e.preventDefault();
 		$('#studentLogSpanShowOption').css('display','none');
 		$('#studentLogSpanHideOption').css('display','');
 		$('#medicalStudentLogFilter').css('display','');
 	});

 	$('#hideStudentLogFilter').click(function(e){
 		e.preventDefault();
 		$('#studentLogSpanShowOption').css('display','');
 		$('#studentLogSpanHideOption').css('display','none');
 		$('#medicalStudentLogFilter').css('display','none');
 	});
});


function getStudentLogList()
{
	if(!checkDateValid($('#medicalNoticeStartDate').val(),$('#medicalNoticeEndDate').val())){
		alert("<?php echo $Lang['medical']['report_bowel']['wrongDatePeriod']?>");
		return false;
	}
	else {	
        $.ajax({
        	dataType: "json",
        	type: "POST",
        	url: '/home/eAdmin/StudentMgmt/medical/?t=homepage.ajax.getStudentLogList&ma=1',
        	data : $('#formMedicalNotice').serialize(),		  
        	success: showStudentLogs,
        	error: show_ajax_error
        });
	}
}

function showStudentLogs(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#medicalNoticeResult').html(ajaxReturn.html);
	}
}	

function show_ajax_error() 
{
	alert('<?=$Lang['medical']['general']['error']['ajax']?>');
}

</script>