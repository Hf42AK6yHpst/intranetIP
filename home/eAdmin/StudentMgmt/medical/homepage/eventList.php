<?php
/*
 * 2018-06-05 Cameron
 * - show created by person [case #F133828]
 * - don't show title for people column (use getNameFieldByLang2 instead of getNameFieldByLang) 
 * 
 * 2018-04-27 Cameron
 * - set 2 to column_array for EventType to strip slashes
 *
 * 2018-04-11 Cameron
 * - fix sorting in EventDate column
 *
 * 2018-04-04 Cameron
 * - create this file
 */
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

$pageNo = ($_POST['pageNo'] == '') ? 1 : $_POST['pageNo'];
$order = ($_POST['order'] == '') ? 0 : $_POST['order']; // default descending
$field = ($_POST['field'] == '') ? 0 : $_POST['field'];
$page_size = ($_POST['numPerPage'] == '') ? 5 : $_POST['numPerPage'];

$li = new libdbtable2007($field, $order, $pageNo);

$AcademicYearID = Get_Current_Academic_Year_ID();
$classCond = '';
$cond = '';

if ($junior_mck) {
    $classNameField = "u.ClassName";
    $classNameEng = $classNameField;
    $joinClass = '';
} else {
    $classNameField = Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN");
    $classNameEng = "yc.ClassTitleEN";
    $joinClass = " INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=u.UserID
					INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='" . $AcademicYearID . "' ";
}

if ($EventGroupID) {
    $joinClass .= "	INNER JOIN INTRANET_USERGROUP g ON g.UserID=u.UserID AND g.GroupID='" . $EventGroupID . "'";
}

if ($ClassName) {
    $cond .= " AND s.ClassName LIKE '%" . $li->Get_Safe_Sql_Like_Query($ClassName) . "%'";
}

if ($EventStudent) {
    $cond .= " AND s.StudentID LIKE '%" . $li->Get_Safe_Sql_Like_Query('^' . $EventStudent . '~') . "%'";
}

if ($EventType) {
    $cond .= " AND e.EventTypeID='" . $EventType . "'";
}

if ($LocationBuildingID) {
    $cond .= " AND e.LocationBuildingID='" . $LocationBuildingID . "'";
}

if ($LocationLevelID) {
    $cond .= " AND e.LocationLevelID='" . $LocationLevelID . "'";
}

if ($LocationID) {
    $cond .= " AND e.LocationID='" . $LocationID . "'";
}

if ($EventAffectedPerson) {
    $cond .= " AND a.AffectedPersonID LIKE '%" . $li->Get_Safe_Sql_Like_Query('^' . $EventAffectedPerson . '~') . "%'";
    $affectedPersonJoin = " INNER JOIN";
} else {
    $affectedPersonJoin = " LEFT JOIN";
}

if ($EventReporter) {
    $cond .= " AND e.LastModifiedBy='" . $EventReporter . "'";
}

if ($EventDate) {
    if ($EventDateStart) {
        $cond .= " AND CONCAT(e.EventDate,' ',e.StartTime)>='" . $EventDateStart . " " . $TimeStartHour . ":" . $TimeStartMin . ":00'";
    }
    
    if ($EventDateEnd) {
        $cond .= " AND CONCAT(e.EventDate,' ',e.EndTime)<='" . $EventDateEnd . " " . $TimeEndHour . ":" . $TimeEndMin . ":00'";
    }
}

$locationField = Get_Lang_Selection("NameChi", "NameEng");
$student_name = getNameFieldByLang('u.');
$affected_name = getNameFieldByLang('u.');
$createdBy = getNameFieldByLang2('cb.');
$updatedBy = getNameFieldByLang2('ub.');

// note 1st column sort by EventDate
$sql = "SELECT
                CONCAT('<a href=\"/home/eAdmin/StudentMgmt/medical/?t=management.event_view&EventID=',e.`EventID`,'\">',e.EventDate,'&nbsp;&nbsp;&nbsp;', DATE_FORMAT(e.StartTime, '%H:%i'),'</a>'),
                s.Student,
                et.EventTypeName,
                CASE
                    WHEN e.LocationBuildingID=0 AND e.LocationLevelID=0 AND e.LocationID=0 THEN '-'
                    WHEN e.LocationBuildingID=0 AND e.LocationLevelID=0 AND e.LocationID<>0 THEN loc." . $locationField . "
					WHEN e.LocationBuildingID=0 AND e.LocationLevelID<>0 AND e.LocationID=0 THEN lvl." . $locationField . "
					WHEN e.LocationBuildingID=0 AND e.LocationLevelID<>0 AND e.LocationID<>0 THEN CONCAT(lvl." . $locationField . ",' > ',loc." . $locationField . ")
					WHEN e.LocationBuildingID<>0 AND e.LocationLevelID=0 AND e.LocationID=0 THEN b." . $locationField . "
					WHEN e.LocationBuildingID<>0 AND e.LocationLevelID=0 AND e.LocationID<>0 THEN CONCAT(b." . $locationField . ",' > ',loc." . $locationField . ")
					WHEN e.LocationBuildingID<>0 AND e.LocationLevelID<>0 AND e.LocationID=0 THEN CONCAT(b." . $locationField . ",' > ',lvl." . $locationField . ")
					ELSE CONCAT(b." . $locationField . ",' > ',lvl." . $locationField . ",' > ',loc." . $locationField . ")
			    END as Location,
				a.AffectedPerson,
				" . $createdBy. " as CreatedBy,
                " . $updatedBy. " as UpdatedBy,
				DATE_FORMAT(e.DateModified, '%Y-%m-%d&nbsp;&nbsp;&nbsp;%H:%i') AS DateModified,
                CONCAT(e.EventDate, e.StartTime) AS EventDate
        FROM
				MEDICAL_EVENT e
				INNER JOIN (
				SELECT 	EventID,
				GROUP_CONCAT(DISTINCT CONCAT('^',u.UserID,'~') ORDER BY u.UserID SEPARATOR ',') as StudentID,
				GROUP_CONCAT(DISTINCT CONCAT('^',$classNameEng,'~') ORDER BY $classNameEng SEPARATOR ',') as ClassName,
				GROUP_CONCAT(DISTINCT " . $student_name . " ORDER BY u.ClassName, u.ClassNumber SEPARATOR ',<br>') as Student
				FROM
						MEDICAL_EVENT_STUDENT s
				INNER JOIN
						INTRANET_USER u ON u.UserID=s.StudentID
				" . $joinClass . "
				LEFT JOIN
						INTRANET_USER_PERSONAL_SETTINGS p ON p.UserID=u.UserID
				GROUP BY s.EventID
			) AS s ON s.EventID=e.EventID
		LEFT JOIN MEDICAL_EVENT_TYPE et ON et.EventTypeID=e.EventTypeID
		LEFT JOIN INVENTORY_LOCATION_BUILDING b ON b.BuildingID=e.LocationBuildingID
		LEFT JOIN INVENTORY_LOCATION_LEVEL lvl ON lvl.LocationLevelID=e.LocationLevelID
		LEFT JOIN INVENTORY_LOCATION loc ON loc.LocationID=e.LocationID
		" . $affectedPersonJoin . " (
				SELECT 	EventID,
						GROUP_CONCAT(DISTINCT CONCAT('^',u.UserID,'~') ORDER BY u.UserID SEPARATOR ',') as AffectedPersonID,
						GROUP_CONCAT(DISTINCT " . $affected_name . " ORDER BY " . $affected_name . " SEPARATOR ',<br>') AS AffectedPerson
				FROM
						MEDICAL_EVENT_AFFECTED_PERSON p
				INNER JOIN
						INTRANET_USER u ON u.UserID=p.AffectedPersonID
				GROUP BY EventID
			) AS a ON a.EventID=e.EventID
		LEFT JOIN INTRANET_USER cb ON cb.UserID=e.InputBy
        LEFT JOIN INTRANET_USER ub ON ub.UserID=e.LastModifiedBy
		WHERE 1 " . $cond;
// debug_r($sql);

$li->field_array = array(
    "EventDate",
    "Student",
    "EventTypeName",
    "Location",
    "AffectedPerson",
    "CreatedBy",
    "UpdatedBy",
    "DateModified"
);
$li->sql = $sql;
$li->no_col = sizeof($li->field_array) + 1;
$li->title = $eDiscipline["Record"];
$li->column_array = array(
    0,
    0,
    2,
    0,
    0,
    0,
    0,
    0
);
$li->wrap_array = array(
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0
);
$li->IsColOff = "list4ajax";
$li->form_name = 'medicalEventForm'; // overwrite form1

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='14%' >" . $li->column_4ajax($pos ++, $Lang['medical']['event']['tableHeader']['EventDate']) . "</th>\n";
$li->column_list .= "<th width='12%' >" . $li->column_4ajax($pos ++, $Lang['medical']['event']['tableHeader']['StudentName']) . "</th>\n";
$li->column_list .= "<th width='12%' >" . $li->column_4ajax($pos ++, $Lang['medical']['event']['tableHeader']['EventType']) . "</th>\n";
$li->column_list .= "<th width='12%' >" . $li->column_4ajax($pos ++, $Lang['medical']['event']['tableHeader']['Place']) . "</th>\n";
$li->column_list .= "<th width='12%' >" . $li->column_4ajax($pos ++, $Lang['medical']['event']['tableHeader']['AffectedPerson']) . "</th>\n";
$li->column_list .= "<th width='12%' >" . $li->column_4ajax($pos ++, $Lang['medical']['event']['tableHeader']['ReportPerson']) . "</th>\n";
$li->column_list .= "<th width='12%' >" . $li->column_4ajax($pos ++, $Lang['medical']['general']['lastModifiedBy']) . "</th>\n";
$li->column_list .= "<th width='13%' >" . $li->column_4ajax($pos ++, $Lang['medical']['general']['dateModified']) . "</th>\n";

?>

<div class="table_board">
	<?php echo $li->display('100%'); ?>
</div>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="page_size_change" value="" />

<script>
$(document).ready(function(){

 	$('#medicalEventResult .tabletoplink').click(function(e){
 		e.preventDefault();
		var form = $(this).closest('form');
		form.find(":hidden[name='order']").val($(this).attr('data-order'));
		form.find(":hidden[name='field']").val($(this).attr('data-fieldIndex'));
		getEventList();
 	});

 	$('#medicalEventResult .tablebottomlink').click(function(e){
 		e.preventDefault();
		var form = $(this).closest('form');
		form.find(":hidden[name='pageNo']").val($(this).attr('data-targetPage'));
		getEventList();
 	});

 	$('#medicalEventResult .pageSelection').change(function(e){
 		e.preventDefault();
		var form = $(this).closest('form');
		form.find(":hidden[name='pageNo']").val($(this).val());
		getEventList();
 	});

 	$("#medicalEventResult :input[name='num_per_page']").change(function(e){
 		e.preventDefault();
		var form = $(this).closest('form');
		form.find(":hidden[name='numPerPage']").val($(this).val());
		getEventList();
 	});
 	
	$.datepick.setDefaults({ maxDate: 0 }); // Cannot select the future date
});

</script>
