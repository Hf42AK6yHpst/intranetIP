<?php
# using: 

#############################################
#	Date:   2016-06-16 (Cara)
#			add event group name at csv export file
#
#	Date:	2014-10-27	YatWoon [Case#T70207]
#			Add "Group", "Gender" and "Athlete Number"
#			Deploy: ip.2.5.5.10.1
#############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lexport = new libexporttext();
$lsports = new libsports();
$lsports->authSportsSystem();

$houseID = $_POST['houseID'];
$HouseDetails = $lsports->getHouseDetail($houseID);

$ExportArr = array();
$DataAry = array();

# House info
$HouseName = $intranet_session_language=="en" ? $HouseDetails['EnglishName'] : $HouseDetails['ChineseName'];

$Students = $lsports->returnStudentListByHouse2($houseID);
$maxEvent = 0;
$i = 0;
foreach($Students as $k=>$d)
{
	list($thisUserID, $thisStudentName, $thisClassName, $thisClassNumber) = $d;
	
	$student_group_id = $lsports->retrieveAgeGroupByStudentID($thisUserID);
	
	$student_group = $lsports->retrieveAgeGroupName($student_group_id);
	$student_gender = $lsports->retrieveAgeGroupDetail($student_group_id);
	$athleticNum = $lsports->returnStudentAthleticNum($thisUserID);			
	
	$DataAry[$i][] = $thisClassName;
	$DataAry[$i][] = $thisClassNumber;
	$DataAry[$i][] = $student_group;
	$DataAry[$i][] = $student_gender["Gender"];
	$DataAry[$i][] = $thisStudentName;
	$DataAry[$i][] = $athleticNum;
	
	# enrolled event
	$sql = "SELECT EventGroupID FROM SPORTS_STUDENT_ENROL_EVENT WHERE StudentID = '$thisUserID'";
	$enroledID = $lsports->returnArray($sql);
	$maxEvent = sizeof($enroledID) > $maxEvent ? sizeof($enroledID) : $maxEvent;
	foreach($enroledID as $k1=>$d1)
	{
		$thisEvent = $lsports->retrieveEventAndGroupNameByEventGroupID($d1['EventGroupID']);

		$eventGroupName = '';
 		$groupID = $thisEvent['GroupID'];
        if($groupID == '-2')
        {
         	$eventGroupName = $i_Sports_Event_Girls_Open;
        }
        else if($groupID == '-1')
        {
             $eventGroupName = $i_Sports_Event_Boys_Open;
        }
         else if($groupID == '-4')
        {
             $eventGroupName = $i_Sports_Event_Mixed_Open;
        }
 		$DataAry[$i][] = $eventGroupName.$thisEvent[0];
	}
	$i++;
}

$ExportArr[] = array($i_Sports_House . ":". $HouseName);

$ExportArr[] = "";

$ExportRowHeader = array();
$ExportRowHeader[] = $i_general_class;
$ExportRowHeader[] = $i_ClassNumber;
$ExportRowHeader[] = $i_Sports_field_Group;
$ExportRowHeader[] = $i_Sports_field_Gender;
$ExportRowHeader[] = $i_Sports_Participant;
$ExportRowHeader[] = $i_Sports_menu_Settings_Participant;
for($i=1;$i<=$maxEvent;$i++)
{
	$ExportRowHeader[] = $i_Sports_menu_Settings_TrackFieldName . " " . $i;
}
$ExportArr[] = $ExportRowHeader;
for($i=0;$i<sizeof($DataAry);$i++)
	$ExportArr[] = $DataAry[$i];


$filename = "HouseEnrol.csv";
$export_content = $lexport->GET_EXPORT_TXT($ExportArr, "", "\t", "\r\n", "\t", 0, "11");
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>