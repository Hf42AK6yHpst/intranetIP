<?
##### MODIFICATION # LOG ##########
# Using : Philips
#
#   Date:   2018-07-11 Philips
#           Create File( all_arrangement.php )
#
#
###################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageAllArrangement";

$lsports = new libsports();
$lsports->authSportsSystem();
$li = new libdb();
$TAGS_OBJ[] = array($Lang['Sports']['Report']['AllArrangement'], "all_arrangement.php", 0);
$TAGS_OBJ[] = array($Lang['Sports']['Report']['GameArrangement'], "game_arrangement.php", 1);

$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

/*
// Initialization
$CurrentAcademicYear = Get_Current_Academic_Year_ID();

$AgeGroupInfoArr = $lsports->retrieveAgeGroupInfo();
//AgeGroupID, GradeChar, EnglishName, ChineseName, Gender, GroupCode,
//DOBUpLimit, DOBLowLimit, DisplayOrder
$AgeGroupArr = array();
$AgeIdArr = array();
foreach($AgeGroupInfoArr as $k => $agi){
    $AgeIdArr[$agi['DisplayOrder']-1] = 'CONCAT('.
                                        'SUM(IF( h.AgeGroupID = '.
                                        $agi['AgeGroupID'].
                                        ',f.StudentID IS NOT NULL,0)), '.
                                        '"/",'.
                                        'SUM(IF( h.AgeGroupID = '.
                                        $agi['AgeGroupID'].
                                        ',e.StudentID IS NOT NULL, 0))'.
                                        ') as AgeGroup'.$k;
    $AgeGroupArr[$agi['DisplayOrder']-1] = array(
        "id" => $agi['AgeGroupID'],
        "name" => $agi[Get_Lang_Selection('ChineseName', 'EnglishName')]
    );  
}
$AgeIdArr = implode(',', $AgeIdArr);

$tableFields = "a.ClassTitleB5, $AgeIdArr, CONCAT(COUNT(f.StudentID),'/', COUNT(e.StudentID)) as ClassTotal";
$tableSql = "YEAR_CLASS as a
              INNER JOIN YEAR_CLASS_USER as b ON a.YearClassID = b.YearClassID
              INNER JOIN INTRANET_USER as d ON b.UserID = d.UserID
              LEFT OUTER JOIN SPORTS_STUDENT_ENROL_EVENT as e ON d.UserID = e.StudentID
              LEFT OUTER JOIN SPORTS_LANE_ARRANGEMENT as f ON d.UserID = f.StudentID AND e.EventGroupID = f.EventGroupID AND f.RoundType = 1
              LEFT OUTER JOIN SPORTS_EVENTGROUP g ON e.EventGroupID = g.EventGroupID
              LEFT OUTER JOIN SPORTS_AGE_GROUP h ON g.GroupID = h.AgeGroupID";
$userCondition = "d.RecordStatus IN (0,1,2) 
			  AND d.RecordType = 2
			  AND (d.ClassName <> '' AND d.ClassNumber <> ''  AND d.ClassName IS NOT NULL  AND d.ClassNumber IS NOT NULL )";
$yearCondition = "AND a.AcademicYearID = '$CurrentAcademicYear'";
$groupBySql = "GROUP BY a.YearClassID";
$sql = "SELECT $tableFields
        FROM  
              $tableSql
        WHERE
              $userCondition
			  $yearCondition
			  $groupoBySql
        ";
// Initialization
if($field=="") $field = 0;
$li = new libdbtable2007($field, 1, $pageNo);
$li->field_array = array("a.YearClassID");
$li->sql = $sql;
$li->no_col = 3 + sizeof($AgeGroupArr);
$li->IsColOff = "eSportSettings";

// TABLE COLUMN
$pos = 0;

$li->column_list .= "<td class='tabletop tabletoplink'>Class Name</td>\n";
foreach($AgeGroupArr as $aga){
    $li->column_list .= "<td class='tabletop tabletoplink'>$aga[name]</td>\n";
}
$li->column_list .= "<td class='tabletop tabletoplink'>Total</td>\n";*/

$AgeGroupOrder = $lsports->returnAgeGroupOrderArr();
$AgeIdArr = $AgeGroupOrder['idArr'];
$AgeGroupArr = $AgeGroupOrder['groupArr'];

$column_list = "<th class='tabletop tabletoplink'>".$Lang['Sports']['Report']['Events']."</th>\n";
foreach($AgeGroupArr as $aga){
    $column_list .= "<th class='tabletop tabletoplink'>$aga[name]</th>\n";
}
$column_list .= "<th class='tabletop tabletoplink'>".$Lang['Sports']['Report']['ClassTotal']."</th>\n";

$result = $lsports->returnAllEventArrangement();
//$result = $li->returnArray($sql);
$linterface->LAYOUT_START();
//debug_pr($sql);
//debug_pr($AgeGroupArr);
//debug_pr($result);
?>
<style>
</style>
<script type="text/javascript"></script>
<form id="form1" method="GET" action="">
	<table class="common_table_list_v30 view_table_list_v30">
		<thead>
		 <tr>
		  <?=$column_list?>
		 </tr>
		</thead>
		<tbody>
		 <?php 
		 foreach($result as $rs){?>
		 <tr>
		  <td><?=$rs['Title']?></td>
		  <?php for($i=0; $i<sizeof($AgeGroupArr);$i++){?>
		    <td><?=$rs['AgeGroup'.$i]?></td>  
		  <?}?>
		  <td><?=$rs['EventTotal']?></td>
		 </tr>
		 <?}?>
		</tbody>
	</table>
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($button_print, "button", "window.open('game_arrangement_print.php', '_blank')")?>
		<p class="spacer"></p>
	</div>
</form>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>