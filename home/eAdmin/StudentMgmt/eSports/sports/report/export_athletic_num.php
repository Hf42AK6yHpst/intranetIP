<?php
#using: Philips
/*
 * 2019-09-18 (Philips) [2019-0503-1912-35164] : Add Display settings
 * 2016-06-10 (Cara)  : change data table's CSS
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageReport_ExportRecord";

$lclass = new libclass();
$lsports = new libsports();
$lsports->authSportsSystem();

# Create Age Group Selection List
$age_groups = $lsports->retrieveAgeGroupIDNames();
$select_age_group = getSelectByArray($age_groups,"name=ageGroup id=ageGroup","$ageGroup",1,0);

# Create Class Selection List
$all_class[0] = -3;
$all_class[1] = $i_general_all_classes;
$classList[] = $all_class;
 
$classes = $lclass->getClassList();
foreach($classes as $i => $arr)
	$classList[$i+1] = $arr;

$class_select = getSelectByArray($classList,"name=className id=className","$className",0,1);

# Create House Selection List
$all_house[0] = -3;
$all_house[1] = $i_general_all_houses;
$houses[] =$all_house;
 
$housesList = $lsports->retrieveHouseSelectionInfo();
foreach($housesList as $i=>$arr)
	$houses[$i+1] = $arr;

$select_house = getSelectByArray($houses,"name=house id=house","$house",0,1);

$day_select = "<SELECT name=raceDay>";
$day_select .= "<OPTION value=1>1</OPTION>";
$day_select .= "<OPTION value=2>2</OPTION>";
$day_select .= "<OPTION value=3>3</OPTION>";
$day_select .= "<OPTION value=0>".$i_Sports_All."</OPTION>";
$day_select .= "</SELECT>";

# Group Type defaults to "Class"
$groupType = 1;

### Title ###
$TAGS_OBJ[] = array($i_Sports_menu_Report_ExportRaceArrangement,"export_arrangement.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Report_ExportRaceResult,"export_result.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Report_ExportAthleticNumber,"export_athletic_num.php", 1);
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();


#### font family selection
// $ff_select = "<select name='font_family'>";
$ff_select = "<option value='Arial'>Arial</option>";
$ff_select .= "<option value='Times New Roman'>Times New Roman</option>";
$ff_select .= "<option value='Verdana'>Verdana</option>";
$ff_select .= "<option value='Georgia'>Georgia</option>";
// $ff_select .= "</select>";

// MKSS custom
$htmlCheckBoxName = 'displayItem[]';
?>
<script language="JavaScript" type="text/javascript">
<!--
function toggle(x)
{
	if(x==1) {
		document.getElementById('className').style.display="block";
		document.getElementById('house').style.display="none";
	} else {
		document.getElementById('className').style.display="none";
		document.getElementById('house').style.display="block";
	}
}
//-->
</script>

<form name="form1" method="post" >

<table class="form_table_v30">
<tr>
	<td class="field_title"><span class="tabletext"><?=$i_Sports_Race_Day?></span></td>
	<td><?=$day_select?></td>
</tr>
<?php if(!$sys_custom['eSports']['KaoYipReports']){?>
<tr>
	<td class="field_title"><span class="tabletext"><?=$eComm['By']?></span></td>
	<td>
		<input type="radio" name="groupType" value="1"<?=($groupType==1)?" checked":""?> id="groupTypeClass" onClick="toggle(this.value);"><label for="groupTypeClass"><?=$i_ClassName?></label>&nbsp;
		<input type="radio" name="groupType" value="2"<?=($groupType==2)?" checked":""?> id="groupTypeHouse" onClick="toggle(this.value);"><label for="groupTypeHouse"><?=$i_House?></label>
		
		<br>
		<?=$class_select?><?=$select_house?>
	</td>
</tr>
<tr>
	<td class="field_title"><span class="tabletext"><?=$i_Sports_field_Group?></span></td>
	<td><?=$select_age_group?></td>
</tr>
<?php }?>
<tr>
	<td class="field_title"><span class="tabletext"><?=$Lang['eSports']['FontSettings']?></span></td>
	<td>
	
	<table class="inside_form_table">
	<tr>
		<td><?=$Lang['eSports']['AthleteInformation']?></td>
		<td><select name="student_font_family"><?=$ff_select?></select> <input name="student_font_size" type="text" value="20" size="3" maxlength="3">px</td>
	</tr>
	
	<tr>
		<td><?=$Lang['eSports']['AthleteNumber']?></td>
		<td><select name="athlete_font_family"><?=$ff_select?></select> <input name="athlete_font_size" type="text" value="120" size="3" maxlength="3">px</td>
	</tr>
	
	<tr>
		<td><?=$Lang['eSports']['EnroledEvent']?></td>
		<td><select name="event_font_family"><?=$ff_select?></select> <input name="event_font_size" type="text" value="20" size="3" maxlength="3">px</td>
	</tr>
	</table>
	</td>
</tr>
<?php //if($sys_custom['eSports']['SportDay_AthleteNumberPrintout_MKSS']){?>
<tr>
	<td class="field_title"><span class="tabletext"><?=$Lang['eSports']['DisplaySettings']?></span></td>
	<td>
		<input type="checkbox" id="displayItem_all" checked/><label for="displayItem_all"><?=$Lang['Btn']['SelectAll']?></label>
		<br/>
		&nbsp;&nbsp;<input type="checkbox" id="displayItem_Class" name="<?=$htmlCheckBoxName?>" value="Class" checked/><label for="displayItem_Class"><?=$Lang['General']['Class']?></label>
		<br/>
		&nbsp;&nbsp;<input type="checkbox" id="displayItem_ClassNo" name="<?=$htmlCheckBoxName?>" value="ClassNo" checked/><label for="displayItem_ClassNo"><?=$Lang['General']['ClassNumber']?></label>
		<br/>
		&nbsp;&nbsp;<input type="checkbox" id="displayItem_Group" name="<?=$htmlCheckBoxName?>" value="Group" checked/><label for="displayItem_Group"><?=$i_Sports_Group?></label>
		<br/>
		&nbsp;&nbsp;<input type="checkbox" id="displayItem_Line" name="<?=$htmlCheckBoxName?>" value="Line" checked/><label for="displayItem_Line"><?=$Lang['eSports']['Lane'].'/'.$Lang['eSports']['Order']?></label>
	</td>
</tr>
<?php //}?>
</table>

<div class="edit_bottom_v30">
<p class="spacer"></p>  
<?= $linterface->GET_ACTION_BTN($button_preview, "button", "Preview()","submit2") ?> 
<?= $linterface->GET_ACTION_BTN($button_export, "button", "Export()","submit2") ?>
<p class="spacer"></p>
</div>

</form>

<script language="JavaScript" type="text/javascript">
function Preview()
{
	document.form1.action='athletic_num_export_process.php'; 
	document.form1.target = '_blank';
	document.form1.submit();
}

function Export()
{
	document.form1.action='athletic_num_export.php'; 
	document.form1.submit();
}

$(document).ready(function(){
	$('#displayItem_all').click(function(){
		if($(this).attr('checked')){
			$('input[name="<?php echo $htmlCheckBoxName;?>"]').attr('checked', true);
		} else {
			$('input[name="<?php echo $htmlCheckBoxName;?>"]').removeAttr('checked');
		}
	});
	$('input[name="<?php echo $htmlCheckBoxName;?>"]').click(function(){
		$('#displayItem_all').removeAttr('checked');
	});
});
<!--
toggle(<?=$groupType?>);
//-->
</script>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>