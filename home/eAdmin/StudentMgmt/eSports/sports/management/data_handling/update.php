<?php
// Using :

####### Change log [Start] #######
#
#   Date:   2020-02-07  (Bill)
#           Create file
#
####### Change log [End] #######

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/iPortfolioConfig.inc.php");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lsports = new libsports();

if(!$lsports->isAdminUser($_SESSION['UserID']) || !$sys_custom['eSports']['SyncResultToPortfolio']) {
    No_Access_Right_Pop_Up();
    die();
}

# Menu
$CurrentPage = "PageManagement_SyncResultToPortfolio";

# Title
$TAGS_OBJ[] = array($Lang['Sports']['Management']['SyncResultToPortfolio']);
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

# Get all Age Groups
$ageGroups = array();
$allGroupsTemp = $lsports->retrieveAgeGroupIDNames();
foreach($allGroupsTemp as $k => $d) {
    $ageGroups[] = $d['AgeGroupID'];
}
$ageGroups[] = -1;
$ageGroups[] = -2;
$ageGroups[] = -4;

# Get all Events
$eventNames = $lsports->retrieveTrackFieldEventName('b5');
$eventNames = BuildMultiKeyAssoc($eventNames, 'EventID', 'EventName', 1, 0);

# Get all Rankings
$rankings = array('第一名', '第二名', '第三名');

// loop Age Groups
$dataArr = array();
for($i=0; $i<sizeof($ageGroups); $i++)
{
    $ageGroupID = $ageGroups[$i];

    // Age Group Name
    if($ageGroupID == -1) {
        $GroupName = '男子公開組';
    }
    else if($ageGroupID == -2) {
        $GroupName = '女子公開組';
    }
    else if($ageGroupID == -4) {
        $GroupName = '混合公開組';
    }
    else {
        $GropuInfo = $lsports->retrieveAgeGroupDetail($ageGroupID);
        $GroupName = $GropuInfo['ChineseName'];
    }

    // loop event groups
    $EventAry = $lsports->retrieveEventIDByAgeGroup($ageGroupID);
    foreach($EventAry as $k => $event)
    {
        list($EventID, $EventName, $EventType) = $event;
        $EventName = $eventNames[$EventID];

        // skip for class & house relay
        if($EventType == 3 || $EventType==4) {
            continue;
        }

        $result = $lsports->retrieveTFRanking($EventID, $ageGroupID, $finalOnly=true);
        foreach($result as $data)
        {
            // only transfer for 1st - 3rd ranking
            $rank = isset($data['Rank']) ? $data['Rank'] : '';
            if($rank != '' && $rank > 0 && $rank <= 3)
            {
                $sid = $data['StudentID'];
                $RankName = $rankings[($rank - 1)];

                $lu = new libuser($sid);
                $dataArr[] = array($sid, $lu->ClassName, $lu->ClassNumber, $GroupName.$EventName.$RankName);
            }
        }
    }
}

# Static data
$SchoolName = GET_SCHOOL_NAME();
$AcademicYearID = IntegerSafe($targetYearID);
$sql = "SELECT YearNameEN FROM ACADEMIC_YEAR WHERE AcademicYearID = '$AcademicYearID'";
$AcademicYearName = $lsports->returnVector($sql);
$AcademicYearName = $AcademicYearName[0];

# [REMOVE] old sports awards
$deleteSql = "DELETE FROM $eclass_db.AWARD_STUDENT WHERE AcademicYearID = '$AcademicYearID' AND ComeFrom = '".$ipf_cfg["DB_AWARD_STUDENT_ComeFrom"]["sports"]."' ";
$lsports->db_db_query($deleteSql);

# [INSERT INTO] current awards
$fields = array("UserID", "AcademicYearID", "Year", "Semester", "IsAnnual", "ClassName", "ClassNumber", "AwardName", "Organization", "RecordType", "RecordStatus", "ComeFrom");
$sql = "INSERT INTO $eclass_db.AWARD_STUDENT (";
foreach($fields as $field) {
    $sql .= $field .", ";
}
$sql .= "InputDate, ModifiedDate) VALUES ";

// loop Records
$values = array();
foreach($dataArr as $this_record)
{
    $updateSql = "";
    foreach($fields as $field)
    {
        $value = '';
        if($field == "UserID") {
            $value = $this_record[0];
        }
        else if($field == "AcademicYearID") {
            $value = $AcademicYearID;
        }
        else if($field == "Year") {
            $value = $AcademicYearName;
        }
        else if ($field == "IsAnnual"){
            $value = 1;
        }
        else if($field == "ClassName") {
            $value = $this_record[1];
        }
        else if($field == "ClassNumber") {
            $value = $this_record[2];
        }
        else if($field == "AwardName") {
            $value = $this_record[3];
        }
        else if($field == "Organization") {
            $value = $SchoolName;
        }
        else if($field == "RecordType") {
            $value = 1;
        }
        else if($field == "RecordStatus") {
            $value = 2;
        }
        else if($field == "ComeFrom") {
            $value = $ipf_cfg["DB_AWARD_STUDENT_ComeFrom"]["sports"];
        }
        $updateSql .= "'". intranet_htmlspecialchars($value) ."', ";
    }
    $updateSql .= "NOW(), NOW()";
    $values[] = $updateSql;
}

$delim = "";
foreach($values as $thisValueSql) {
    $sql .= $delim." ($thisValueSql) ";
    $delim = ", ";
}

$result = $lsports->db_db_query($sql);
if($result)
{
    $resultStr = str_replace("<!--count-->", count($values), $Lang['eDiscipline']['DataHandling_ToPortfolioSuccess']['Student']);

    // log sync records
    $sql = " INSERT INTO SPORTS_SYNC_DATA_LOG (AcademicYearID, IsOldRecord, InputBy, DateInput) VALUES ('$AcademicYearID', '0', '$UserID', NOW()) ";
    $lsports->db_db_query($sql);
}

# Import Result Summary
if(!empty($resultStr)) {
    $resultStr = $Lang['eDiscipline']['DataHandling_ToPortfolioSuccess']['Success'].$resultStr;
}

$linterface->LAYOUT_START();
?>

<script language="javascript">
function js_Go_Transfer_Other_Records()
{
    window.location = 'to_portfolio.php';
}
</script>

<br />
<form id="form1" name="form1" method="POST">
    <div class="table_board">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr><td align=center><?=$resultStr?></td></tr>
        </table>
    </div>

    <br style="clear:both;" />
    <div class="edit_bottom_v30">
        <p class="spacer"></p>
        <?=$linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "js_Go_Transfer_Other_Records();", "submitBtn")?>
        <p class="spacer"></p>
    </div>
</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>