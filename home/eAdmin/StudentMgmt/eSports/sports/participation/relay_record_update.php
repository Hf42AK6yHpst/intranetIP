<?php
//using: 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();
$lsports->authSportsSystem();

$eventType = 3;
$arrangeDetail = $lsports->returnHRLaneArrangeDetailByEventGroupID($eventGroupID);
$ExtInfo = $lsports->retrieveEventGroupExtInfo($eventGroupID, $eventType);

#clear newrecord
$table="SPORTS_EVENTGROUP_EXT_RELAY";
$updateFields = "NewRecordMin = NULL, NewRecordSec = NULL, NewRecordMs = NULL ";
$orderType = "ASC";

$sql = "	UPDATE $table 
				SET $updateFields
			WHERE
				EventGroupID = $eventGroupID";
$lsports->db_db_query($sql);

$record = ($ExtInfo["RecordMin"]*60*1000) + ($ExtInfo["RecordSec"]*1000) + $ExtInfo["RecordMs"];
$check_record = $record;

$standard = ($ExtInfo["StandardMin"]*60*1000) + ($ExtInfo["StandardSec"]*1000) + $ExtInfo["StandardMs"];
//modified by marcus 18/6

for($i=0; $i<sizeof($arrangeDetail); $i++)
{
	list($hname, $order, $hid, $h_color) = $arrangeDetail[$i];
	$house_arr[$order][0]=$hid;
	$house_arr[$order][1]=$hname;
	$house_arr[$order][2]=$h_color;
	
	$result_min = (${"result_min_".$order}=="")?0:${"result_min_".$order};
	$result_sec = (${"result_sec_".$order}=="")?0:${"result_sec_".$order};
	$result_ms = (${"result_ms_".$order}=="")?0:${"result_ms_".$order};

	$result_min = (strlen($result_min)==1) ? "0".$result_min : $result_min;
	$result_sec = (strlen($result_sec)==1) ? "0".$result_sec : $result_sec;
	$result_ms = (strlen($result_ms)==1) ? "0".$result_ms : $result_ms;

	$result_record = ($result_min*60*1000) + ($result_sec*1000) + $result_ms;

	$resultArr[$order]["min"] = $result_min;
	$resultArr[$order]["sec"] = $result_sec;
	$resultArr[$order]["ms"] = $result_ms;
	
	$resultArr[$order]["other"] = ${"other_".$order};

	if($result_record != 0 && $resultArr[$order]["other"]!=4)
		$Ranking[$order] = $result_record;
		
}


if(sizeof($Ranking)==0)
{
	header ("Location:relay_record_detail.php?eventGroupID=$eventGroupID");
}

asort($Ranking);
$ranked_arrange_order = array_keys($Ranking);
$rank=0; 

foreach($Ranking as $order => $result_record)
{
	list($hid,$hname,$h_color)=$house_arr[$order];
	$result_min = $resultArr[$order]["min"];
	$result_sec = $resultArr[$order]["sec"];
	$result_ms = $resultArr[$order]["ms"];
	
	$other = $resultArr[$order]["other"];

	if($other==0)
	{
		# Retrieve the updated new record for compare
		$sql = "SELECT NewRecordMin, NewRecordSec, NewRecordMs FROM SPORTS_EVENTGROUP_EXT_RELAY Where EventGroupID = '$eventGroupID'";
		$newRecord = $lsports->returnArray($sql, 3);

		$new = ($newRecord[0][0]*60*1000) + ($newRecord[0][1]*1000) + $newRecord[0][2];
		$check_record = ($new==0) ? $check_record : $new;
			
		if($result_record<$check_record)
		{
			$status = 4;

			# Update broken record time
			$sql = "UPDATE SPORTS_EVENTGROUP_EXT_RELAY SET NewRecordMin = '$result_min', NewRecordSec = '$result_sec', NewRecordMs = '$result_ms', NewRecordHolderName = '$hname', NewRecordHouseID = '$hid' WHERE EventGroupID = '$eventGroupID'";
			$lsports->db_db_query($sql);
		}
		else if($result_record<=$standard)
			$status = 3;
		else
			$status = 2;
	}
	else if($other==1) //break record
	{
		$status = 4;
	}
	else if($other==2) //qualified
	{
		$status = 3;
	}
	else if($other==3)// not qualified
	{
		$status = 2;
	}


	$resultArr[$order]["status"] = $status;
	$rank++;
	if($last_order!=""&&$Ranking[$last_order]==$result_record) //check if record same as neighbour
		$resultArr[$order]["rank"]=$last_rank;
	else
	{
		$resultArr[$order]["rank"]=$rank;
		$last_rank=$rank;
	}
	$last_order=$order;
	
}
# Retrieve score standard 
$sql = "SELECT ScoreStandardID FROM SPORTS_EVENTGROUP WHERE EventGroupID = '$eventGroupID'";
$temp = $lsports->returnVector($sql);
$ScoreStandardDetail = $lsports->retrieveScoreStandardDetail($temp[0]);


foreach($resultArr as $order => $result_record)
{
	//assign rank
	$result_record["rank"]= $resultArr[$order]["rank"]?$resultArr[$order]["rank"]:"NULL";
	
	
	# Calculate score by ranking
	if($ScoreStandardDetail[$result_record["rank"]]!="")
		$score = $ScoreStandardDetail[$result_record["rank"]];
	else
		$score = 0;

	# Calculate score by status
	if($result_record["status"]==3)
		$score = $score + $ScoreStandardDetail[10];
	else if($result_record["status"]==4)
		$score = $score + $ScoreStandardDetail[9]+$ScoreStandardDetail[10];

	$result_record["score"] = $score;
	if($result_record["status"]=="")
	{
		$result_record["status"] = ($result_record["other"]==4) ? 5 : 1;
	}
	
	$values = "ResultMin = ".$result_record["min"].", ";
	$values .= "ResultSec = ".$result_record["sec"].", ";
	$values .= "ResultMs = ".$result_record["ms"].", ";
	$values .= "RecordStatus = ".$result_record["status"].", ";
	$values .= "Rank = ".$result_record["rank"].", ";
	$values .= "Score = ".$result_record["score"];

	$sql = "UPDATE SPORTS_HOUSE_RELAY_LANE_ARRANGEMENT SET $values WHERE EventGroupID = '$eventGroupID' AND ArrangeOrder = '$order'";
	$lsports->db_db_query($sql);
}

//modified by marcus end

##########
# status
# 1 - absent
# 2 - Unsuitable
# 3 - Qualified
# 4 - Record Broken
# 5 - Foul
####################

intranet_closedb();
header ("Location:relay_record_detail.php?eventGroupID=$eventGroupID&xmsg=update");
?>