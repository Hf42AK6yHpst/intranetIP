<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();
$lsports->authSportsSystem();

$lexport = new libexporttext();

$content = $_POST['exportContent'];
$fname = $_POST['exportFileName'];

$content = str_replace('&quot;','"',$content);

$filename = $fname.".csv";


// if (!$g_encoding_unicode) {
// 	$export_content = trim(stripslashes($content));	
// 	
// 	//Output the file to user browser
// 	output2browser($export_content, $filename);
// } else {
	$export_content = trim(stripslashes($content), "\r\n ");
	$Temp = explode("\n", $export_content);
	
	for ($i = 0; $i < sizeof($Temp); $i++) 
	{
		$RawDataPieces = explode(",", trim($Temp[$i]));
		
		$DataPieces = array();
		
		for ($j=0; $j<sizeof($RawDataPieces); $j++) 
		{
			$tmpDataPiece = $RawDataPieces[$j];
			$StrLength = strlen($tmpDataPiece);
			$Str = trim($tmpDataPiece);
			
			if(!empty($RawDataPieces[$j]) && $RawDataPieces[$j][0] == '"')
			{
				if($RawDataPieces[$j][$StrLength-1]!='"')
				{
					
					//look for combine end
					for($k=$j+1; $k<sizeof($RawDataPieces);$k++)
					{
						$len = strlen($RawDataPieces[$k]);
						$tmpDataPiece .= ','.$RawDataPieces[$k];
						$j++;	
						if($len > 0 && $RawDataPieces[$k][$len-1] == '"')
						{
							break;
						}
					}
 				}
 				$tmpDataPiece = substr($tmpDataPiece, 1, strlen($tmpDataPiece)-2);
			}
			
			$result[$i][] = $tmpDataPiece;
		}
		
	}


$exportColumn = "";
$Delimiter = "";
$LineBreak="\r\n";
$ColumnDefDelimiter="";
$DataSize = 5;
$Quoted="11";

	$export_content = $lexport->GET_EXPORT_TXT($result, $ColumnDef, $Delimiter, $LineBreak, $ColumnDefDelimiter, $DataSize,$Quoted);
	
	$lexport->EXPORT_FILE($filename, $export_content);

// }


intranet_closedb();
?>