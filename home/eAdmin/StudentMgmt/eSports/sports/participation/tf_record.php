<?php
# using: 

############################################
#	Date:	2019-11-20  Philips [2019-1024-1748-37066]
#			Replace $i_Sports_menu_Report_EventRanking by $Lang['eSports']['IndividualEventRanking']
#
#   Date:   2019-04-11  Bill    [2019-0301-1144-56289]
#           Change Tab - Redirect to cust Group Relay page    ($sys_custom['eSports']['KaoYipRelaySettings'])
#
#	Date:	2017-02-27	Bill	[2016-0627-1013-19066]
#			Hide Tab Group Champion		($sys_custom['eSports']['PuiChi_MultipleGroup'])
#
#	Date:	2012-06-26	YatWoon	(??? <- not included in deploy first)
#			improved: check the event is completed with "sizeof($Ranking)" and "RecordStatus" (function returnEventIsCompleted())
#
############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "PageRaceResult";

$lsports = new libsports();
$lsports->authSportsSystem();

$AgeGroups = $lsports->retrieveAgeGroupIDNames();	# retrieve all age group ids and names

# Add Boys Open and Girls Open 
$openBoy[0] = '-1';
$openBoy[1] = $i_Sports_Event_Boys_Open;
$openGirl[0] = '-2';
$openGirl[1] = $i_Sports_Event_Girls_Open;
$openMixed[0] = '-4';
$openMixed[1] = $i_Sports_Event_Mixed_Open;
$AgeGroups[] = $openBoy;
$AgeGroups[] = $openGirl;
$AgeGroups[] = $openMixed;

$TREvents = $lsports->retrieveTrackFieldEventName();		# retrieve all track and field eventgroup ids and names
$numberOfLane = $lsports->numberOfLanes;

# Create a new array to store the track and field results
for($i=0; $i<sizeof($TREvents); $i++)
{
	list($event_id, $event_name) = $TREvents[$i];

	for($j=0; $j<sizeof($AgeGroups); $j++)
	{
		list($group_id, $group_name) = $AgeGroups[$j];
		
		$typeIDs = array(1, 2);
		
		# Retrieve event group id
		$eventGroupInfo = $lsports->retrieveEventGroupID($event_id, $group_id, $typeIDs);
		$eventGroupID = $eventGroupInfo[0];
		$eventType = $eventGroupInfo[1];
		
		if($eventGroupID != "")
		{
			# Retrieve Number of Participants
			$arrangedCount = $lsports->retrieveTFLaneArrangedEventCount($eventGroupID);

			$TrackFieldResult[$event_id][$group_id]["eg_id"] = $eventGroupID;
			$TrackFieldResult[$event_id][$group_id]["event_type"] = $eventType;
			$TrackFieldResult[$event_id][$group_id]["arranged"] = $arrangedCount;
		}
	}
}

# Age Group
$ageGroup = "";
$ageGroup .= "<tr>";
$ageGroup .= "<td width='100'>&nbsp;</td>";
for($i=0; $i<sizeof($AgeGroups); $i++)
	$ageGroup .= "<td class='tabletop tabletoplink' align='center'>".$AgeGroups[$i][1]."</td>";
$ageGroup .= "</tr>";

# List
$list = "";
for($j=0; $j<sizeof($TREvents); $j++)
{
	list($event_id, $event_name) = $TREvents[$j];
	
	$list .= "<tr class='tablerow".($j%2?"2":"1")."'>";
	$list .= "<td class='tablelist'>".$event_name."</td>";
	
	for($k=0; $k<sizeof($AgeGroups); $k++)
	{
		$group_id = $AgeGroups[$k][0];
		
		if(sizeof($TrackFieldResult[$event_id][$group_id]) != 0)
		{
			$eventGroupID = $TrackFieldResult[$event_id][$group_id]["eg_id"];
			$event_type = $TrackFieldResult[$event_id][$group_id]["event_type"];
			$arranged = $TrackFieldResult[$event_id][$group_id]["arranged"];
			$Ranking = $lsports->retrieveTFRanking($event_id, $group_id);
			
			// added by marcus 
			$indicator="<font style='color:red'>*</font>"; //non complete event indicator
			
			# Check whether Event has been set up
			$is_setup = $lsports->checkExtInfoExist($event_type, $eventGroupID);

			if($is_setup == 1)
			{				
				if($arranged != 0)
				{
					$tr_display = ((sizeof($Ranking)==0)? $indicator : "")."<a class='tablelink' href='input_tf_record.php?eventGroupID=$eventGroupID'>".$arranged."</a>";
				}
				else
				{
					$tr_display = "0";
				}
				$list .= "<td class='tabletext' align='center'>".$tr_display."</td>";
			}
			else
				$list .= "<td class='tabletext' align='center'>- -</td>";
		}
		else
			$list .= "<td>&nbsp;</td>";
	}
	$list .= "</tr>";
}

# Title
$house_relay_name = $lsports->retrieveEventTypeNameByID(3);
$class_relay_name = $lsports->retrieveEventTypeNameByID(4);

# Tag
$TAGS_OBJ[] = array($i_Sports_menu_Participation_TrackField, "../participation/tf_record.php", 1);
$TAGS_OBJ[] = array($house_relay_name, "../participation/relay_record.php", 0);
// [2019-0301-1144-56289]
if($sys_custom['eSports']['KaoYipRelaySettings']) {
    $TAGS_OBJ[] = array($class_relay_name, "../participation/class_relay_group_record.php", 0);
} else {
    $TAGS_OBJ[] = array($class_relay_name, "../participation/class_relay_record.php", 0);
}
$TAGS_OBJ[] = array($Lang['eSports']['IndividualEventRanking'], "../report/event_rank.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Report_HouseGroupScore, "../report/house_group.php", 0);
// [2016-0627-1013-19066]
if(!$sys_custom['eSports']['PuiChi_MultipleGroup'])
	$TAGS_OBJ[] = array($i_Sports_menu_Report_GroupChampion, "../report/group_champ.php", 0);
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center"><br />
		<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<table width='100%' border='0' cellspacing='0' cellpadding='4'>
						<?=$ageGroup?>
						<?=$list?>
						</table>
					</td>
				</tr>
				<tr>
					<td height='1' class='dotline'><img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif' width='10' height='1'></td>
				</tr>
				<tr>
					<td class="tabletextremark">(<?=$i_Sports_Explain?>: - - <?=$i_Sports_Item_Without_Setting_Meaning?> , <?=$indicator?> <?=$i_Sports_Not_Complete_Events_Meaning?>)</td>
				</tr>
				</table>
			</td>
		</tr>
		<tr>
		<td align="center">
			<div>
				<form name="form_export" action="tf_record_export_all.php" style='display:inline-block;'>
				<? echo $linterface->GET_ACTION_BTN($button_export, "submit"); ?>
				</form>
				<?php if($sys_custom['eSports']['SportDay_TrackFieldResultExport_MKSS']){?>
					<p class='spacer' style='display:inline-block;'></p>
					<? echo $linterface->GET_ACTION_BTN($Lang['eSports']['Sports']['Export_TrackFieldResult_MKSS'], "button", "window.open('tf_record_export_all_mkss.php');"); ?>
				<?php }?>
			</div>
		</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>