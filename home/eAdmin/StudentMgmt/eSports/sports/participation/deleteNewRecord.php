<?php
# using: Bill

############################################
#
#   Date:   2019-04-15  Bill    [2019-0301-1144-56289]
#           Redirect to cust Group Relay page    ($sys_custom['eSports']['KaoYipRelaySettings'])
#
############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();
$lsports->authSportsSystem();

if($eventType==1)
{
	$sql = "UPDATE SPORTS_EVENTGROUP_EXT_TRACK SET NewRecordMin = NULL, NewRecordSec = NULL, NewRecordMs = NULL, NewRecordHolderName = NULL, NewRecordHolderUserID = NULL, NewRecordHouseID = NULL WHERE EventGroupID = '$eventGroupID'";
	$lsports->db_db_query($sql);
}
else if($eventType==2)
{
	$sql = "UPDATE SPORTS_EVENTGROUP_EXT_FIELD SET NewRecordMetre = NULL, NewRecordHolderName = NULL, NewRecordHolderUserID = NULL, NewRecordHouseID = NULL WHERE EventGroupID = '$eventGroupID'";
	$lsports->db_db_query($sql);
}
else if($eventType==3 || $eventType==4)
{
	$sql = "UPDATE SPORTS_EVENTGROUP_EXT_RELAY SET NewRecordMin = NULL, NewRecordSec = NULL, NewRecordMs = NULL, NewRecordHolderName = NULL, NewRecordHouseID = NULL WHERE EventGroupID = '$eventGroupID'";
	$lsports->db_db_query($sql);
}

intranet_closedb();
if($eventType==3) {
	header ("Location:relay_record_detail.php?eventGroupID=$eventGroupID&msg=1");
}
else if($eventType==4) {
    // [2019-0301-1144-56289]
    if($sys_custom['eSports']['KaoYipRelaySettings']) {
        header ("Location:class_relay_group_record_detail.php?eventGroupID=$eventGroupID&msg=1");
    }
    else {
	   header ("Location:class_relay_record_detail.php?eventGroupID=$eventGroupID&msg=1");
    }
}
else {
	header ("Location:input_tf_record.php?eventGroupID=$eventGroupID&xmsg=New_Record_Delete");
}
?>