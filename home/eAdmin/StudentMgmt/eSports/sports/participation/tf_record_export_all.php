<?php
# using: yat

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();
$lsports->authSportsSystem();
$lexport = new libexporttext();

$AgeGroups = $lsports->retrieveAgeGroupIDNames();	# retrieve all age group ids and names
# Add Boys Open and Girls Open 
$openBoy[0] = '-1';
$openBoy[1] = $i_Sports_Event_Boys_Open;
$openGirl[0] = '-2';
$openGirl[1] = $i_Sports_Event_Girls_Open;
$openMixed[0] = '-4';
$openMixed[1] = $i_Sports_Event_Mixed_Open;
$AgeGroups[] = $openBoy;
$AgeGroups[] = $openGirl;
$AgeGroups[] = $openMixed;

$ExportArr = array();
$exportColumn = array();
$exportColumn[] = "";
foreach($AgeGroups as $k=>$d)
{
	$exportColumn[] = $d[1];
}

$TREvents = $lsports->retrieveTrackFieldEventName();		# retrieve all track and field eventgroup ids and names

# Create a new array to store the track and field results
for($i=0; $i<sizeof($TREvents); $i++)
{
	list($event_id, $event_name) = $TREvents[$i];

	for($j=0; $j<sizeof($AgeGroups); $j++)
	{
		list($group_id, $group_name) = $AgeGroups[$j];
		
		$typeIDs = array(1, 2);
		# Retrieve event group id
		$eventGroupInfo= $lsports->retrieveEventGroupID($event_id, $group_id, $typeIDs);	

		$eventGroupID = $eventGroupInfo[0];
		$eventType = $eventGroupInfo[1];
		
		#Check whether the Event has been set up or finished
		$Ranking = $lsports->retrieveTFRanking($event_id, $group_id);
		$finished = sizeof($Ranking)>0;
		$is_setup = $lsports->checkExtInfoExist($eventType, $eventGroupID);
		
		
		if($eventGroupID != "")
		{
			# Retrieve Number of Participants
			$arrangedCount = $lsports->retrieveTFLaneArrangedEventCount($eventGroupID);
			$arrangedisplay = $is_setup?($finished?"":"*").$arrangedCount:"- -";
			$TrackFieldResult[$event_id][$group_id]["arranged"] = $arrangedisplay;
		}
	}
}

# Build the export array
for($j=0; $j<sizeof($TREvents); $j++)
{
	$ExportRow = array();
	
	list($event_id, $event_name) = $TREvents[$j];
	
	$ExportRow[] = $event_name;
	
	for($k=0; $k<sizeof($AgeGroups); $k++)
	{
		$group_id = $AgeGroups[$k][0];
		
		if(sizeof($TrackFieldResult[$event_id][$group_id]) != 0)
		{
			$arranged = $TrackFieldResult[$event_id][$group_id]["arranged"];
			$ExportRow[] = $arranged;
		}
		else
		{
			$ExportRow[] = "";
		}
	}
	
	$ExportArr[] = $ExportRow;
}
$ExportRow = array($i_Sports_Explain.": - - ".$i_Sports_Item_Without_Setting_Meaning." * ".$i_Sports_Not_Complete_Events_Meaning);
$ExportArr[] = $ExportRow;

$filename = "TrackFieldRecord.csv";
$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");
$lexport->EXPORT_FILE($filename, $export_content);
intranet_closedb();
?>