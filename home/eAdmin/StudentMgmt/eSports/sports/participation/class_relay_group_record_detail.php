<?php
# using: 

############################################
#	Date:	2019-11-20  Philips [2019-1024-1748-37066]
#			Replace $i_Sports_menu_Report_EventRanking by $Lang['eSports']['IndividualEventRanking']
#
#   Date:   2019-04-11  Bill    [2019-0301-1144-56289]
#           Create File
#           Copy logic from input_tf_record.php
#
############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "PageRaceResult";

$lsports = new libsports();
$lsports->authSportsSystem();

$export_content = "\n";

# No of arranged class groups
$classGroupCount = $lsports->retrieveClassRelayLaneArrangementByRound($eventGroupID, ROUND_TYPE_FIRSTROUND);
$GroupEventInfo = $lsports->Get_Group_Event_By_EventGroupID($eventGroupID);
list($GroupID, $EventID) = $GroupEventInfo[0];
if($GroupID > 0)
{
    $EventGroupInfo = $lsports->retrieveEventAndGroupNameByEventGroupID($eventGroupID);
    $eventName = $EventGroupInfo[0];
    $groupName = $EventGroupInfo[1];
    $eventType = $EventGroupInfo[2];
}
else
{
    $EventInfo = $lsports->Get_Event_Info($EventID);
    list($eventType, $eventName) = $EventInfo[0];
    $groupName = $lsports->Get_Open_Event_Name($GroupID);
}

# Get Event Group Extra Settings
$ExtInfo = $lsports->retrieveEventGroupExtInfo($eventGroupID, $eventType);
$numberOfLane = $lsports->numberOfLanes;

$deleteLink = "deleteNewRecord.php?eventGroupID=$eventGroupID&eventType=$eventType";
// if($eventType == EVENT_TYPE_TRACK)
{
    // New Record
    if($ExtInfo["NewRecordMin"] != NULL && $ExtInfo["NewRecordSec"] != NULL && $ExtInfo["NewRecordMs"] != NULL)
    {
        $NewRecordArr = array($ExtInfo["NewRecordMin"], $ExtInfo["NewRecordSec"], $ExtInfo["NewRecordMs"]);
        $new_record_result = $lsports->Format_TimerArr($NewRecordArr);
        $new_record = $new_record_result."&nbsp;&nbsp;&nbsp;".$linterface->GET_SMALL_BTN($button_remove, "button", "click_delete();","submit2");
    }
    else
    {
        $new_record = "&nbsp;";
        $new_record_result = "";
    }
    
    // Record
    $RecordArr = array($ExtInfo["RecordMin"], $ExtInfo["RecordSec"], $ExtInfo["RecordMs"]);
    $record = $lsports->Format_TimerArr($RecordArr);
    
    // Standard Record
    $StandardArr = array($ExtInfo["StandardMin"], $ExtInfo["StandardSec"], $ExtInfo["StandardMs"]);
    $standard = $lsports->Format_TimerArr($StandardArr);
    
//  $ResultArr = $lsports->Get_Track_Result($eventGroupID);
}
/* 
else
{
    # Check if it is high jump event
    $isHighJump = $lsports->Is_High_Jump($eventGroupID);
    
    # Check if consider all 6 trials
    $IsAllTrialIncluded = $lsports->Is_All_Trial_Included($eventGroupID);
    $needToUpdateFinalRound = false;
    
    if($ExtInfo["NewRecordMetre"]!=NULL)
    {
        $new_record = $ExtInfo["NewRecordMetre"];
        $new_record_result = $new_record;
        $new_record .= "&nbsp;&nbsp;&nbsp;".$linterface->GET_SMALL_BTN($button_remove, "button", "click_delete();", "submit2");
    }
    else
    {
        $new_record = "&nbsp;";
        $new_record_result = "";
    }
    
    $record = $ExtInfo["RecordMetre"];
    $standard = $ExtInfo["StandardMetre"];
    
    $ResultArr = $lsports->Get_Field_Result($eventGroupID);
}
 
$ResultArr = $lsports->returnClassRelayLaneArrangeDetailByEventGroupID($eventGroupID);
$ResultArr = BuildMultiKeyAssoc($ResultArr, array("ClassGroupID", "RoundType"));
 */

# Build Export Content
$export_content .= $i_Sports_Item.",".$eventName."\n";
$export_content .= $i_Sports_field_Group.",".$groupName."\n";
$export_content .= $i_Sports_Record.",".$record."\n";
$export_content .= $i_Sports_New_Record.",".$new_record_result."\n";
$export_content .= $i_Sports_Standard_Record.",".$standard."\n";
$export_content .= "\n";
$export_event_content = $export_content;

if($ExtInfo["FinalRoundReq"] == 1) {
    $finalReq = 1;
} else {
    $finalReq = 0;
}

$rows_per_table = 0;

######################################################################
################# Loop Round Table  ##################################
######################################################################

$totalHeatNumber = $lsports->returnClassRelayHeatNumberByEventGroupID($eventGroupID);

$RoundTypeArr = array(ROUND_TYPE_FIRSTROUND, ROUND_TYPE_FINALROUND);
foreach((array)$RoundTypeArr as $roundType)
{
    $export_content = '';
    $gen_next_btn = '';
    
    $directFinal = 0;
    $arrangeDetail1 = array();
    $arrange1 = $lsports->returnClassRelayLaneArrangeDetailByEventGroupID($eventGroupID, $roundType);
    if(empty($arrange1)) continue;
    
    $involvedRoundArr[] = $roundType;
    if($roundType == ROUND_TYPE_FIRSTROUND)
    {
// 		if($eventType==EVENT_TYPE_TRACK && $total_heat_num==1 && $participantCount>1)
//      {
// 			$directFinal = 1;
// 			$ExtInfo["SecondRoundReq"]=0;
// 			$ExtInfo["FinalRoundReq"]=0;
// 		}
        
        if($totalHeatNumber == 1 && $classGroupCount > 1)
        {
            $directFinal = 1;
// 			$ExtInfo["SecondRoundReq"] = 0;
// 			$ExtInfo["FinalRoundReq"] = 0;
        }
        
        if($ExtInfo["SecondRoundReq"] == 1)	  		# even only have 1 total heat num, still need to according to the usering setting
        {
            /* 
            $directFinal = 0;
            $gen_next_round_lnk = "class_relay_gen_next_round.php?eventGroupID=".$eventGroupID."&eventType=".$eventType."&currRound=".$roundType."&roundType=2";
            $gen_next_btn = $linterface->GET_BTN($i_Sports_Record_Generate_Second_Round, "button", "window.location='".$gen_next_round_lnk."'","but2");
             */
        }
        else if($ExtInfo["FinalRoundReq"] == 1)     # even only have 1 total heat num, still need to according to the usering setting
        {
            $directFinal = 0;
            $gen_next_round_lnk = "class_relay_gen_next_round.php?eventGroupID=".$eventGroupID."&eventType=".$eventType."&currRound=".$roundType."&roundType=0";
            $gen_next_btn = $linterface->GET_BTN($i_Sports_Record_Generate_Final_Round, "button", "window.location='".$gen_next_round_lnk."'","but2");
        }
    }
    else if($roundType == ROUND_TYPE_SECONDROUND)
    {
        /* 
        if($ExtInfo["SecondRoundReq"] == 0)
            continue;
        
        if($ExtInfo["FinalRoundReq"] == 1&&sizeof($arrange1) != 0)
        {
            $gen_next_round_lnk = "gen_next_round.php?eventGroupID=".$eventGroupID."&eventType=".$eventType."&currRound=".$roundType."&roundType=0";
            $gen_next_btn .= $linterface->GET_BTN($i_Sports_Record_Generate_Final_Round, "button", "window.location='".$gen_next_round_lnk."'","but2");
        }
        */
    }
    else if($roundType == ROUND_TYPE_FINALROUND)
    {
        if($ExtInfo["FinalRoundReq"] == 0) {
            continue;
        }
    }
    
    for($i=0; $i<sizeof($arrange1); $i++)
    {
        list($heat, $order, $cgid, $tmp_rank, $tmp_score, $tmp_trackresult, $result_min1, $result_sec1, $result_ms1, $result_status, $cid, $c_name, $c_cname, $c_ename, $cg_title, $absent_reason) = $arrange1[$i];
        
        /* 
		# Get House name of Student
		$house1 = $lsports->retrieveStudentHouseInfo($sid);
		
        # Retrieve result if exist
        if($eventType == EVENT_TYPE_TRACK)
        {
//			$result= $lsports->Get_Track_Result($eventGroupID,$sid,$roundType);
            list($result_min1, $result_sec1, $result_ms1, $s_status1, $reason, $score, $rank) = $ResultArr[$sid][$roundType];
        }
        else
        {
//			$result= $lsports->Get_Field_Result($eventGroupID,$sid,$roundType);
            list($s_record1, $s_status1, $s_rank1, $reason, $trial1, $trial2, $trial3, $score, $rank) = $ResultArr[$sid][$roundType];
            
            // [2017-0405-1457-30236] Get First Round Best Attempts
            if($roundType == ROUND_TYPE_FINALROUND && $IsAllTrialIncluded)
            {
                $arrangeDetail1[$heat][$order]["trial"]["first_round"] = "";
                $first_round_record = $ResultArr[$sid][ROUND_TYPE_FIRSTROUND]["ResultMetre"];
                if(!empty($first_round_record))
                {
                    $arrangeDetail1[$heat][$order]["trial"]["first_round"] = $first_round_record;
                    
                    // Display Warning Message - *Please update Final Round Result
                    list($final_round_status, $final_round_attend) = $lsports->Get_Attend_And_Status($s_status1);
                    if($final_round_attend == 1 && ($first_round_record > $s_record1)) {
                        $s_record1 = $first_round_record;
                        $needToUpdateFinalRound = true;
                    }
                }
            }
        }
         */
        
        $arrangeDetail1[$heat][$order]["cgid"] = $cgid;
        $arrangeDetail1[$heat][$order]["cid"] = $cid;
        $arrangeDetail1[$heat][$order]["cg_title"] = $cg_title;
        $arrangeDetail1[$heat][$order]["c_name"] = $c_name;
        $arrangeDetail1[$heat][$order]["min"] = $result_min1;
        $arrangeDetail1[$heat][$order]["sec"] = $result_sec1;
        $arrangeDetail1[$heat][$order]["ms"] = $result_ms1;
        $arrangeDetail1[$heat][$order]["rank"] = $tmp_rank;
        $arrangeDetail1[$heat][$order]["score"] = $tmp_score;
        $arrangeDetail1[$heat][$order]["status"] = $result_status;
        $arrangeDetail1[$heat][$order]["reason"] = $absent_reason;
    }
    
    $tableContent1 = "";
    if(sizeof($arrangeDetail1) != 0)
    {
        foreach((array)$arrangeDetail1 as $heat => $value)
        {
            $RoundTables .= "<form name='form_".$roundType."_".$heat."' id='form_".$roundType."_".$heat."' action='class_relay_group_record_update.php' method='post' onSubmit='return checkForm(".$roundType.",".$heat.")' changed='false'>";
            $RoundTables .= "<table width='90%' border='0' cellspacing='0' cellpadding='4'>";
            
            # Round Title & Table row class
            switch($roundType)
            {
                case ROUND_TYPE_FIRSTROUND:
                    $RoundTitle = '';
                    $RoundTitle .= $lsports->Get_Lang_Heat($heat);
                    $RoundTitle .= $ExtInfo["FinalRoundReq"] == 1? " (".$i_Sports_First_Round.")" : "";
                    $table_row_class = "tabletop";
                    break;
                case ROUND_TYPE_SECONDROUND:
                    /* 
                    $RoundTitle = $lsports->Get_Lang_Heat($heat) ." (".$i_Sports_Second_Round .")";
                    $table_row_class = "tablegreentop";
                     */
                    break;
                case ROUND_TYPE_FINALROUND:
                    $RoundTitle = $i_Sports_Final_Round;
                    $table_row_class = "tablebluetop";
                    break;
            }
            
            /* 
            // [2017-0405-1457-30236]
            if($needToUpdateFinalRound) {
                $RoundTables .= "<tr><td colspan='6' align='right'>".$linterface->GET_SYS_MSG("", " *".$Lang['eSports']['WarningArr']['PleaseUpdateFinalResult'])."</td></tr>";
            }
             */
            
            $RoundTables .= "<tr><td colspan='5' align='left'>". $linterface->GET_NAVIGATION2($RoundTitle) ."</td></tr>";
            $export_content .= $RoundTitle ."\n";
            
            # The following if statement seems have logic problem
//          $GroupPersonNum = $ExtInfo['FirstRoundGroupCount'];
            
            $RoundTables .= "<tr>";
                $RoundTables .= "<td class='$table_row_class tabletopnolink' width='10%'>". $i_Sports_Line ."</td>";
                $RoundTables .= "<td class='$table_row_class tabletopnolink' width='25%'>". $i_general_class ."</td>";
                $RoundTables .= "<td class='$table_row_class tabletopnolink' width='25%'>". $i_Sports_Present."/".$i_Sports_Absent ."</td>";
                $RoundTables .= "<td class='$table_row_class tabletopnolink' width='25%'>". $i_Sports_Record ." (min : sec : ms)</td>";
                $RoundTables .= "<td class='$table_row_class tabletopnolink' width='15%'>". $i_Sports_Other ."</td>";
            $RoundTables .= "</tr>";
            
            $export_content .= $i_Sports_Line .",";
//          $export_content .= $i_Sports_Participant .",";
//          $export_content .= $Lang['General']['EnglishName'] .",".$Lang['General']['ChineseName'] .",";
            $export_content .= $i_general_class .",";
//          $export_content .= $i_Sports_House .",";
            $export_content .= $i_Sports_Present."/".$i_Sports_Absent.",";
//          $export_content .= $i_Sports_field_Rank .",";
            $export_content .= $i_Sports_Record .",";
            /* 
            if($eventType == EVENT_TYPE_FIELD && !$isHighJump)
            {
                for($try = 1; $try<=3; $try++)
                    $export_content .= $lsports->Get_Lang_Trial($try) .",";
            }
             */
//          $export_content .= $i_Sports_field_Score .",";
            $export_content .= $i_Sports_Other ."\n";
            
            # Retrieve the maxinum key
            if(sizeof($value) != 0)
            {
                $key = array_keys($value);
                rsort($key);
                $maxPos = $key[0];
            }
            else
            {
                $maxPos = 0;
            }
            
            /* 
            $thisEventStudentList = Get_Array_By_Key(array_values($value), "sid");
            $lu = new libuser("","",$thisEventStudentList);
             */
            
            for($i=1; $i<=$maxPos; $i++)
            {
                $orderShow = $lsports->Get_Lang_Line($i);
                
                $detail = $value[$i];
                if(sizeof($detail) != 0)
                {
                    /*
                    $lu->LoadUserData($detail["sid"]);

                    $color = ($detail["color_code"] == "") ? "FFFFFF" : $detail["color_code"];
                    */
                    list($status, $attend) = $lsports->Get_Attend_And_Status($detail["status"]);
                    
                    $tableContent1 .= "<tr class='tablegreenrow". (($i + 1) % 2 + 1)."'>";
                    
                    # Line Number
                    $tableContent1 .= "<td class='tabletext'>".$orderShow."</td>";
                    $export_content .= $orderShow .",";
                    
                    /* 
                    # Student Name and House
                    $tableContent1 .= "<td class='tabletext'>".$lsports->house_flag2($color, $detail["student_name"])."</td>";
                    $export_content .= '"'.$detail["student_name"] .'",';
                    $export_content .= '"'.$lu->EnglishName .'",';
                    $export_content .= '"'.$lu->ChineseName .'",';
                    $export_content .= '"'.$lu->ClassName .'",';
                    $export_content .= $detail["house_name"] .',';
                     */
                    
                    # Class Group Name
                    $classGroupNameDisplay = '';
                    $classGroupNameDisplay .= $detail['c_name'];
                    $classGroupNameDisplay .= $detail['cg_title']==''? '' : ' ('.$detail['cg_title'].')' ;
                    $tableContent1 .= "<td>".$classGroupNameDisplay."</td>";
                    $export_content .= $classGroupNameDisplay.",";
                    
                    # Present / Absent Selection
                    /* 
                    if($eventType==EVENT_TYPE_FIELD && $isHighJump)
                        $checkTie = " checkTieResult($roundType,$heat) ";
                     */
                    $AttendSelection = $lsports->Get_Present_Absent_Selection($heat, $i, $attend, $detail['reason'], "doValidation2($roundType, $heat);");
                    $tableContent1 .= "<td>$AttendSelection</td>";
                    switch($attend)
                    {
                        case 1: $export_content .= $i_Sports_Present; break;
                        case 2: $export_content .= $i_Sports_Absent; break;
                        case 3: $export_content .= $i_Sports_Absent." - ".$Lang['eSports']['Waive']."(".$detail['reason'].")"; break;
                        default: $export_content .= "";
                    }
                    $export_content .= ",";
                    
                    /* 
                    # Rank
                    $export_content .= $detail['rank'].",";
                    $tableContent1 .= "<td>".$detail['rank']."</td>";
                     */
                    
                    # Result Display
                    $result_disabled = $attend==2 || $attend==3? " disabled " : "";
//                  if($eventType == EVENT_TYPE_TRACK)
                    {
                        $tableContent1 .= "<td class='tabletext' nowrap>";
                            $tableContent1 .= "<input type='text' onChange='doValidation2($roundType, $heat)' name='result_min_".$heat."_".$i."' maxlength='3' size='3' value='".($attend != 1? "" : $detail['min'])."' class='tabletext' $result_disabled> ' ";
                            $tableContent1 .= "<input type='text' onChange='doValidation2($roundType, $heat)' name='result_sec_".$heat."_".$i."' maxlength='2' size='3' value='".($attend != 1? "" : $detail['sec'])."' class='tabletext' $result_disabled> '' ";
                            $tableContent1 .= "<input type='text' onChange='doValidation2($roundType, $heat)' name='result_ms_".$heat."_".$i."' maxlength='2' size='3' value='".($attend != 1? "" : $detail['ms'])."' class='tabletext' $result_disabled>";
                        $tableContent1 .= "</td>";
                        if($attend != 2 && ($detail['min'] || $detail['sec'] || $detail['ms'] )) {    # 2 = absent
                            $export_content .= $detail['min'] ."'".$detail['sec']."''".$detail['ms'].",";
                        } else {
                            $export_content .= ",";
                        }
                    }
                    /* 
                    else
                    {
                        # Tie Breaker Selection
                        if($isHighJump)
                        {
                            $tableContent1 .= "<td><input onChange='checkTieResult($roundType,$heat);doValidation2($roundType,$heat)' type='text' name='record_".$heat."_".$i."' size='10' value='".($attend!=1?"":$detail['record'] )."' $result_disabled>";
                            
                            $tieResult = $lsports->Get_Tie_Break_Result($eventGroupID, $roundType, $detail["record"]);
                            $optionArr = array();
                            $optionArr[] = array(0, $i_Sports_Tie_Breaker);
                            if(count($tieResult) > 1)
                            {
// 								for($rankCount=0;$rankCount<count($tieResult);$rankCount++) {
// 									list($oriResult,$oriRank) = $tieResult[$rankCount];
// 									$optionArr[] = array($oriRank,$i_Sports_field_Rank." ".$oriRank);
// 								}
                                
                                // [2019-0117-1351-39207] rank options - updated logic
                                $rankBase = $tieResult[0]['Rank'];
                                for($rankCount=0; $rankCount<count($tieResult); $rankCount++) {
                                    list($oriResult, $oriRank) = $tieResult[$rankCount];
                                    $oriRank = $rankBase + $rankCount;
                                    
                                    $optionArr[] = array($oriRank,$i_Sports_field_Rank." ".$oriRank);
                                }
                                $disabled = "";
                            }
                            else
                            {
                                $disabled = " disabled ";
                            }
                            $onChange = " onchange='doValidation2($roundType,$heat)' ";
                            $TieBreakSelection = getSelectByArray($optionArr, " name='tie_breaker_".$heat."_".$i."' $disabled $onChange",$detail["rank"],0,1,$i_Sports_Tie_Breaker);
                            
                            $tableContent1 .= $TieBreakSelection;
                            if($attend!=2) {	# 2 = absent
                                $export_content .= $detail['record'].",";
                            } else {
                                $export_content .= ",";
                            }
                            
                            $CheckTieOnStatusSelection = " checkTieResult($roundType,$heat); ";
                        }
                        else
                        {
                            $tableContent1 .= "<td width='25%'>";
                            $tableContent1 .= "<table>";
                            for($try = 1; $try<=3; $try++) {
                                $tableContent1 .= "<tr><td>".$lsports->Get_Lang_Trial($try)."</td><td><input onKeyUp='UpdateBestTrial($roundType,$heat,$i); doValidation2($roundType,$heat)' type='text' class='record_trial_".$i." record_".$heat."_".$i."' name='record_trial_".$i."[]' size='10' value='".($attend!=1?"":$detail["trial"][$try])."' $result_disabled></td></tr>";
                            }
                            if($roundType == ROUND_TYPE_FINALROUND && $IsAllTrialIncluded) {
                                $tableContent1 .= "<tr><td>".$Lang['eSports']['Round1BestTrial']."</td><td><input readonly type='text' class='record_".$i."_first_round record_".$heat."_".$i."' id='record_trial_".$roundType."_".$i."_1st_round' size='10' value='".($detail["trial"]["first_round"]? $detail["trial"]["first_round"] : "")."' $result_disabled></td></tr>";
                            }
                            
                            $tableContent1 .= "<tr><td>".$Lang['eSports']['BestTrial']."</td><td><input readonly type='text' class='record_".$i." record_".$heat."_".$i."' name='record_".$heat."_".$i."' size='10' value='".($attend!=1?0:$detail['record'])."' $result_disabled></td></tr>";
                            $tableContent1 .= "</table>";
                            $tableContent1 .= "</td>";
                            if($attend!=2) {     # 2 = absent
                                for($try = 1; $try<=3; $try++) {
                                    $export_content .= $detail["trial"][$try].",";
                                }
                                $export_content .= $detail['record'].",";
                            } else {
                                $export_content .= ",";
                            }
                        }
                    }
                    
                    if($attend!=2 && ($detail['min'] || $detail['sec'] || $detail['ms'])) {	# 2=absent
                        $export_content .= $detail['min'] ."'".$detail['sec']."''".$detail['ms'].",";
                    }
                    else {
                        $export_content .= ",";
                    }
                    $export_content .= $detail["score"].",";
                     */
                    
                    # Record Status Selection
                    $RecordStatusSelect = $lsports->Get_Record_Status_Selection("other_".$heat."_".$i, $status, "doValidation($roundType, $heat); $CheckTieOnStatusSelection");
                    $tableContent1 .= "<td>".$RecordStatusSelect."</td>";
                    
                    switch($status)
                    {
                        case 0: $export_content .= "--"; 					break;
                        case 1: $export_content .= $i_Sports_RecordBroken; 	break;
                        case 2: $export_content .= $i_Sports_Qualified; 	break;
                        case 3: $export_content .= $i_Sports_Unsuitable; 	break;
                        case 4: $export_content .= $i_Sports_Foul; 			break;
                        default: $i_Sports_Unsuitable;
                    }
                    $tableContent1 .="</tr>";
                }
                else
                {
                    $tableContent1 .= "<tr class='tablegreenrow". (($i + 1) % 2 + 1)."'>";
                        $tableContent1 .= "<td class='tabletext'>".$orderShow."</td>";
                        $tableContent1 .= "<td colspan='4'>&nbsp;</td>";
                    $tableContent1 .= "</tr>";
                }
                $rows_per_table++;
                
                $export_content .= "\n";
            }
            $RoundTables .= $tableContent1;
            unset($tableContent1);
            
            $RoundTables .= "</td></tr>";
            
            $RoundTables .= "<tr><td colspan='6' class='dotline'><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='10' height='1' /></td></tr>";
            $RoundTables .= "<tr>";
                $RoundTables .= "<td align='center' colspan='6' >";
                    $RoundTables .= $linterface->GET_ACTION_BTN($button_submit, "submit", "","submit2") . "&nbsp;";
                    $RoundTables .= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset") . "&nbsp;";
                    $RoundTables .= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='tf_record.php'");
                $RoundTables .= "</td>";
            $RoundTables .= "</tr>";
            
            $RoundTables .= "</table><br>";
            
            $RoundTables .= "<input type='hidden' name='eventGroupID' value='".$eventGroupID."'>";
            $RoundTables .= "<input type='hidden' name='roundType' value='".$roundType."'>";
            $RoundTables .= "<input type='hidden' name='eventType' value='".$eventType."'>";
            $RoundTables .= "<input type='hidden' name='currHeat' value='".$heat."'>";
            $RoundTables .= "<input type='hidden' name='finalReq' value='".$finalReq."'>";
            $RoundTables .= "<input type='hidden' name='total_heat_num' value='".$totalHeatNumber."'>";
            $RoundTables .= "<input type='hidden' name='rows_per_table' value='".$rows_per_table."'>";
//             $RoundTables .= "<input type='hidden' name='isHighJump' value='".$isHighJump."'>";
            $RoundTables .= "<input type='hidden' name='directFinal' value='".$directFinal."'>";
            $RoundTables .= "</form>";
            
            $rows_per_table = 0;
            $export_content .= "\n\n";
            
            $export_content_arr[$roundType] = $export_content;
        }
    }
    
    # Gen Button at the end of first round
    if($gen_next_btn)
    {
        $RoundTables .= "<table width='90%' border='0' cellspacing='0' cellpadding='4'>";
        $RoundTables .= "<tr>";
            $RoundTables .= "<td colspan='6' align='right'>";
                $RoundTables .= $gen_next_btn;
            $RoundTables .= "</td>";
        $RoundTables .= "</tr>";
        $RoundTables .= "</table>";
    }
}

######################################################################
#################  Loop Round Table - End ############################
######################################################################

$printBtnLayer .= '<div class="btn_option_layer print_layer">'."\n";
$printBtnLayer .= '<form action="../report/result_export_process.php" method="get" target="_blank" onsubmit="return checkLayerOption(\'print\')">'."\n";
$printBtnLayer .= '<table class="form_table" style="width:200px">'."\n";
$printBtnLayer .= '<col class="field_title">'."\n";
$printBtnLayer .= '<tbody>'."\n";
foreach((array)$involvedRoundArr as $thisRoundType)
{
    $printBtnLayer .= '<tr>'."\n";
        $printBtnLayer .= '<td class="field_title" nowrap>'.$Lang['eSports']['RoundTitle'][$thisRoundType].'</td>'."\n";
        $printBtnLayer .= '<td>'."\n";
            $printBtnLayer .= $linterface->Get_Checkbox($eventGroupID."_".$thisRoundType, "EventGroupID[]", $eventGroupID."_".$thisRoundType, 1)."\n";
        $printBtnLayer .= '</td>'."\n";
    $printBtnLayer .= '</tr>'."\n";
}
    $printBtnLayer .= '<tr>'."\n";
        $printBtnLayer .= '<td colspan="2">'."\n";
        $printBtnLayer .= '<div class="edit_bottom">'."\n";
            $printBtnLayer .= '<input type="submit" value="'.$Lang['Btn']['Print'].'" class="formsmallbutton" >'."\n";
            $printBtnLayer .= '<input type="button" value="'.$Lang['Btn']['Cancel'].'" class="formsmallbutton" onclick="js_ShowHide_Layer(\'print\');" ></div>'."\n";
        $printBtnLayer .= '</td>'."\n";
    $printBtnLayer .= '</tr>'."\n";
$printBtnLayer .= '</tbody>'."\n";
$printBtnLayer .= '</table>'."\n";

$printBtnLayer .= '<input type="hidden" value="1" name="isResult">'."\n";
$printBtnLayer .= '</form>'."\n";
$printBtnLayer .= '</div>'."\n";
$printBtn = '<div class="btn_option">'.$linterface->Get_Content_Tool_v30("print", "javascript:void(0)", "", "", "onclick='js_ShowHide_Layer(\"print\")' ").'<br style="clear: both;">'.$printBtnLayer.'</div>';

$exportBtnLayer .= '<div class="btn_option_layer export_layer">'."\n";
$exportBtnLayer .= '<form action="../report/result_export_process.php" method="get">'."\n";
$exportBtnLayer .= '<table class="form_table" style="width:200px">'."\n";
$exportBtnLayer .= '<col class="field_title">'."\n";
$exportBtnLayer .= '<tbody>'."\n";
foreach((array)$involvedRoundArr as $thisRoundType)
{
    $exportBtnLayer .= '<tr>'."\n";
        $exportBtnLayer .= '<td class="field_title" nowrap>'.$Lang['eSports']['RoundTitle'][$thisRoundType].'</td>'."\n";
        $exportBtnLayer .= '<td>'."\n";
            $exportBtnLayer .= $linterface->Get_Checkbox("export_".$thisRoundType, "EventGroupID[]", $eventGroupID."_".$thisRoundType, 1)."\n";
        $exportBtnLayer .= '</td>'."\n";
    $exportBtnLayer .= '</tr>'."\n";
}
$exportBtnLayer .= '<tr>'."\n";
    $exportBtnLayer .= '<td colspan="2">'."\n";
    $exportBtnLayer .= '<div class="edit_bottom">'."\n";
        $exportBtnLayer .= '<input type="button" value="'.$Lang['Btn']['Export'].'" class="formsmallbutton" onclick="js_Export();  ">'."\n";
        $exportBtnLayer .= '<input type="button" value="'.$Lang['Btn']['Cancel'].'" class="formsmallbutton" onclick="js_ShowHide_Layer(\'export\');"></div>'."\n";
    $exportBtnLayer .= '</td>'."\n";
$exportBtnLayer .= '</tr>'."\n";
$exportBtnLayer .= '</tbody>'."\n";
$exportBtnLayer .= '</table>'."\n";

$exportBtnLayer .= '<input type="hidden" value="1" name="isResult">'."\n";
$exportBtnLayer .= '</form>'."\n";
$exportBtnLayer .= '</div>'."\n";
$exportBtn = '<div class="btn_option">'.$linterface->Get_Content_Tool_v30("export", "javascript:void(0)", "", "", "onclick='js_ShowHide_Layer(\"export\")' ").'<br style="clear: both;">'.$exportBtnLayer.'</div>';

# Title
$house_relay_name = $lsports->retrieveEventTypeNameByID(3);
$class_relay_name = $lsports->retrieveEventTypeNameByID(4);

# Tag
$TAGS_OBJ[] = array($i_Sports_menu_Participation_TrackField, "../participation/tf_record.php", 0);
$TAGS_OBJ[] = array($house_relay_name, "../participation/relay_record.php", 0);
$TAGS_OBJ[] = array($class_relay_name, "../participation/class_relay_group_record.php", 1);
$TAGS_OBJ[] = array($Lang['eSports']['IndividualEventRanking'], "../report/event_rank.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Report_HouseGroupScore, "../report/house_group.php", 0);
if(!$sys_custom['eSports']['PuiChi_MultipleGroup']) {
    $TAGS_OBJ[] = array($i_Sports_menu_Report_GroupChampion, "../report/group_champ.php", 0);
}

$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($button_edit.($intranet_session_language=="en"?" ":""). $i_Sports_menu_Participation_TrackField, "");

$linterface->LAYOUT_START();
?>

<script language="javascript">
	function js_ShowHide_Layer(type)
	{
		if($("."+type+"_layer").css("visibility")!="visible")
		{
			$(".btn_option_layer").css("visibility","hidden");
			$(".print").removeClass('parent_btn');
			$(".export").removeClass('parent_btn');
			
			$("div."+type+"_layer").find("input").attr("checked","checked");
			
			$("."+type).addClass('parent_btn');
			$("."+type+"_layer").css("visibility","visible");
		}
		else
		{
			$("."+type).removeClass('parent_btn');
			$("."+type+"_layer").css("visibility","hidden");
		}
	}
	
	function js_Export()
	{
		if(!checkLayerOption('export')) {
			return false;
		}
		
		$("#exportContent").val($("#exportEventContent").val()); 
		if($("#export_1").attr("checked")) {
			$("#exportContent").val($("#exportContent").val()+$("#exportContent_1").val());
		}
		if($("#export_2").attr("checked")) {
			$("#exportContent").val($("#exportContent").val()+$("#exportContent_2").val());
		}
		if($("#export_0").attr("checked")) {
			$("#exportContent").val($("#exportContent").val()+$("#exportContent_0").val());
		}
		
		document.form_export.submit();
	}
	
	function checkLayerOption(type)
	{
		if($("div."+type+"_layer").find("input:checked").length==0)
		{
			alert("<?=$Lang['eSports']['WarningArr']['PleaseSelectRound']?>");
			return false;
		}
		js_ShowHide_Layer(type)
		
		return true;
	}

// 	// all functions modified by marcus 22/7/2009 get formObj by eval(formStr) not work in firefox, replaced by jQuery
// 	function checkTieResult(roundType, heat){
// 		count_largest = 0;
// 		count_equal = 1;
// 		//formObj = eval('form_'+roundType+'_'+heat);
// 		formStr = 'form_'+roundType+'_'+heat;
// 		jObj = $("[name="+formStr+"]");
// 		formObj = jObj.get()[0];
//		
// 		r = parseInt(formObj.rows_per_table.value);
// 		if(r<=0) return;
// 		resetTieBreakerSelection(formObj,heat);
//		
// 		for(i=1; i<=r; i++){
// 			//tie_break_obj_i=jObj.find("[name=tie_breaker_"+heat+"_"+i+"]").get()[0];
//			
// 			tie = false;
// 			o = eval('formObj.record_'+heat+'_'+i);
//
// 			otherObj = eval('formObj.other_'+heat+'_'+i);
// 			if($(otherObj).val()==4)
// 				continue;
//
// 			if(o==null || o.disabled) continue;
// 			num1 = o.value;
// 			if(num1==''||isNaN(num1)){
// 				o.value=""; 
// 				continue;
// 			}
// 			num1= parseFloat(num1);
// 			attendObj_i = eval('formObj.attend_'+heat+'_'+i);
//
// 			for(j=1;j<=r;j++){
// 				//tie_break_obj_j=jObj.find("[name=tie_breaker_"+heat+"_"+j+"]").get()[0];
//
// 				if(i==j)continue;
// 				otherObj = eval('formObj.other_'+heat+'_'+j);
// 				if($(otherObj).val()==4)
// 					continue;
//				
// 				o2 = eval('formObj.record_'+heat+'_'+j);
//				
// 				if(o2==null || o2.disabled)continue;
// 				num2 = o2.value;
// 				attendObj_j = eval('formObj.attend_'+heat+'_'+j);
//				
// 				if(num2=='' || isNaN(num2)){ 
// 					o2.value="";
// 					continue;
// 				}
// 				num2 = parseFloat(num2);
// 				if(num2>num1) count_largest++;
// 				if(num2==num1 && num1>0){
// 					//if(attendObj_i !=null && attendObj_j!=null&&attendObj_i[0].checked==true && attendObj_j[0].checked==true){
// 						eval('formObj.tie_breaker_'+heat+'_'+i+'.disabled=false');
// 						eval('formObj.tie_breaker_'+heat+'_'+j+'.disabled=false');
//						
// 						tie = true;
// 						count_equal++;
// 					//}
// 				}
// 			}
// 			if(tie==false){
// 					o3 = eval('formObj.tie_breaker_'+heat+'_'+i);
//					
// 					if(o3!=null){
// 						o3.options[0].selected=true;
// 						o3.disabled=true;
// 					}
// 			}
//
// 				// keep the old selected index
// 				selectedIndex = eval('formObj.tie_breaker_'+heat+'_'+i+'.selectedIndex');
// 				selectedVal = eval('formObj.tie_breaker_'+heat+'_'+i+'.options['+selectedIndex+'].value');
//				
// 				// remove the existing tie-breaker selection list
// 				len = eval('formObj.tie_breaker_'+heat+'_'+i+'.options.length');
//
// 				for(k=len-1; k>0; k--){
// 					eval('formObj.tie_breaker_'+heat+'_'+i+'.options['+k+']=null');
// 					len = eval('formObj.tie_breaker_'+heat+'_'+i+'.options.length');
// 				}
// 				// create new tie-breaker selection list
// 				for(k=1; k<=count_equal; k++){
// 					len = eval('formObj.tie_breaker_'+heat+'_'+i+'.options.length');
//					eval('formObj.tie_breaker_'+heat+'_'+i+'.options['+(len)+']=new Option("<?=$i_Sports_Tie_Breaker_Rank?> '+(k+count_largest)+'",'+(k+count_largest)+',false,false)');
//					
// 					if(k+count_largest==parseInt(selectedVal)){
// 						eval('formObj.tie_breaker_'+heat+'_'+i+'.options['+(len)+'].selected=true');
// 					}
// 				}
// 			count_largest = 0;
// 			count_equal = 1;
// 		}
// 	}
//	
// 	function resetTieBreakerSelection(formObj, heat){
// 		r = parseInt(formObj.rows_per_table.value);
// 		if(r<=0)return;
// 		for(i=1;i<=r;i++){
// 			obj = eval('formObj.record_'+heat+'_'+i);
// 			if(obj==null)return;
// 			attendObj = eval('formObj.attend_'+heat+'_'+i);
//			
// 			num = obj.value;
// 			if(num==''||isNaN(num)){
// 				eval('formObj.record_'+heat+'_'+i+'.value=""');
// 				eval('formObj.tie_breaker_'+heat+'_'+i+'.options[0].selected=true');
// 				if(attendObj != undefined)
// 					eval('formObj.attend_'+heat+'_'+i+'[0].checked=false');
// 			}
// 			else if(num>0){
// 				if(attendObj != undefined)
// 					eval('formObj.attend_'+heat+'_'+i+'[0].checked=true');
// 			}
// 			eval('formObj.tie_breaker_'+heat+'_'+i+'.disabled=true');
// 		}
// 	}
	
	function checkForm(roundType, heat)
	{
		//f = eval('form_'+roundType+'_'+heat);
		f = $("[name=form_"+roundType+"_"+heat+"]").get()[0] ;

		// check if the attend reason is empty
		var attend_reason_empty = false;		
		$(f).find("input.attend_reason:visible").each(function(){
			if($(this).val().Trim()=='')
			{
				alert("<?=$Lang['eSports']['InputWaiveReason']?>");
				$(this).focus();
				attend_reason_empty = true;
				return false;
			}
		});
		if(attend_reason_empty) {
			return false;
		}
		
// 		// bypass it if it is NOT High Jump
// 		if(f.isHighJump.value!=1)return true;
// 		// below is commented by Yuen, to be verified by Yat Woon
// 		//if(f.directFinal.value==1)return false;
//		
// 		r = parseInt(f.rows_per_table.value);
// 		str = 'f.tie_breaker_'+heat+'_';
// 		str2 = 'f.record_'+heat+'_';
// 		str3 = 'f.attend_'+heat+'_';
// 		str4 = 'f.other_'+heat+'_';
//		
// 		for(i=1; i<=r; i++){
// 			record_i = eval(str2+i+'.value');
// 			attend_i = eval(str3+i);
// 			other_i =  eval(str4+i);
//			
// 			if($(other_i).val()==4) continue;
// 			if(record_i==''||isNaN(record_i)) {
// 				continue;
// 			} else {
// 				record_i = parseFloat(record_i);
// 			}
//
// 			// check current line result > selected 1st option?
// 			var need_to_check_1st_rank = false;
// 			var selected_1st_rank = false;
// 			s_o = eval(str+i);
// 			if(s_o.disabled==false && s_o.selectedIndex==1) {
// 				selected_1st_rank = true;
// 			}
//			
// 			for(j=1; j<=r; j++){
// 				if(j==i)continue;
//				
// 				record_j = eval(str2+j+'.value');
// 				attend_j = eval(str3+j);
// 				other_j  = eval(str4+j);
//				
// 				if($(other_j).val()==4) continue;
// 				if(record_j==''||isNaN(record_j)||record_j==0) {
// 					continue;
// 				} else {
// 					record_j=parseFloat(record_j);
// 				}
//				
// 				if(record_j==record_i && (!attend_i || (attend_i[0].checked && attend_j[0].checked))) {
// 					s = eval(str+i);
// 					s1 = eval(str+j);
// 					if(s.selectedIndex==s1.selectedIndex || (s.disabled==false && s.selectedIndex==0) || (s1.disabled==false && s1.selectedIndexdisabled==false)) {
// 						//if(s.selectedIndex==s1.selectedIndex && s.selectedIndex>0)
//						//	alert(s.options[s.selectedIndex].text+' <?=$i_Sports_Warn_Rank_Assigned_To_Others?>');
//						//else alert('<?=$i_Sports_Warn_Please_Select_Rank?>');
// 						//if(s.disabled==false)
// 						//	s.focus();
// 						//return false;
//						
// 						if(s.selectedIndex==s1.selectedIndex && s.selectedIndex>0) {
// 							// do nothing
// 						} else {
//							alert('<?=$i_Sports_Warn_Please_Select_Rank?>');
//     						if(s.disabled==false) {
//     							s.focus();
//     						}
//     						return false;
// 						}
// 					}
//
// 					// check related lines result > selected 1st option?
// 					need_to_check_1st_rank = true;
// 					if(selected_1st_rank==false && s1.disabled==false && s1.selectedIndex==1) {
// 						selected_1st_rank = true;
// 					}
// 				}
// 			}
//
// 			// no 1st option selected > not allow submit
// 			if(s_o.disabled==false && need_to_check_1st_rank && selected_1st_rank==false) {
//				alert('<?=$Lang['eSports']['PleaseSelectAtLeastOneRank']?>\''+s_o.options[1].text+'\'');
// 				s_o.focus();
//				
// 				return false;
// 			}
// 		}
		
		return true;
	}
	
	function doValidation(roundType, heat){
		strForm		= 'form_'+roundType+'_'+heat;
		strRecord 	= 'formObj.record_'+heat+'_';
		strMin 		= 'formObj.result_min_'+heat+'_';
		strSec 		= 'formObj.result_sec_'+heat+'_';
		strMs 		= 'formObj.result_ms_'+heat+'_';
		strOther 	= 'formObj.other_'+heat+'_';
		strAttend 	= 'formObj.attend_'+heat+'_';

		jObj =  $("[name="+strForm+"]");
		formObj = jObj.get()[0];

// 		eventType = parseInt(formObj.eventType.value);
// 		IsJump = parseInt(formObj.isHighJump.value);
//		
// 		if(eventType!=1 && eventType!=2) return;

		r = parseInt(formObj.rows_per_table.value);
		for(i=1; i<=r; i++)
		{
			otherObj = eval(strOther+i);
			if(otherObj==null) { 
				continue;
			}
			selectedValue = otherObj.options[otherObj.selectedIndex].value;

// 			if(eventType==2 && selectedValue==4 && IsJump){
// 				eval(strRecord+i+'.value=""');
// 			}
		}
	}
	
	var formchanged = false;
	var test = new Array();
	function doValidation2(roundType, heat){
		strForm		= 'form_'+roundType+'_'+heat;
		strRecord 	= 'formObj.record_'+heat+'_';
		strMin 		= 'formObj.result_min_'+heat+'_';
		strSec 		= 'formObj.result_sec_'+heat+'_';
		strMs 		= 'formObj.result_ms_'+heat+'_';
		strOther 	= 'formObj.other_'+heat+'_';
		strAttend 	= 'formObj.attend_'+heat+'_';
		
		jObj =  $("[name="+strForm+"]");
		formObj = jObj.get()[0];

// 		eventType = parseInt(formObj.eventType.value);
//
// 		if(eventType!=1 && eventType!=2) return;
			
		r = parseInt(formObj.rows_per_table.value);
		for(i=1; i<=r; i++)
		{
			isNum = false;
			attendObj= eval(strAttend+i);

// 			if(eventType==1){
    			minObj = eval(strMin+i);
    			secObj = eval(strSec+i);
    			msObj = eval(strMs+i);
				
    			if(minObj==null) continue;
    			valMin = parseInt(minObj.value);
    			valSec = parseInt(secObj.value);
    			valMs  = parseInt(msObj.value);
    			if(!isNaN(valMin) || !isNaN(valSec) || !isNaN(valMs) || valMin>0 || valSec>0 || valMs>0 || (!valMin&&!valSec&&!valMs)) {
    				isNum = true;
    			}
// 			}
// 			else if(eventType==2){
// 				recordObj = eval(strRecord+i);
// 				if(recordObj==null) continue;
// 				valRecord = parseFloat(recordObj.value);
// 				if(!isNaN(valRecord)||valRecord>0||valRecord)
// 					isNum = true;
// 			}

			if(isNum)
			{
				otherObj = eval(strOther+i);
				valOther = otherObj.options[otherObj.selectedIndex].value;
				
				if(attendObj!=null)
				{
					attendObj[0].checked=true;
				}

				// reset SELECT BOX "other" when form first modified
				if(formchanged!=true && (valOther==1 || valOther==2 || valOther==3))
				{
					otherObj.options[0].selected =true;
				}
			}
			else
			{
				if(attendObj != null) {
					attendObj[0].checked = false;
				}
			}
		}

		// indicate form changed
		formchanged = true;
	}
    
    function click_delete()
    {
    	if(confirm("<?=$i_Discipline_System_alert_remove_warning_level?>"))
        {
    		window.location='<?=$deleteLink?>';
        }
    }
	
    function AttendUpdate(heat, line, obj)
    {
    	strAttend = "attend_"+heat+"_"+line;
    	strReason = "attend_"+heat+"_"+line+"_reason";
    	thisForm = obj.form;
   
    	strRecord 	= 'record_'+heat+'_'+line;
    	strMin 		= 'result_min_'+heat+'_'+line;
    	strSec 		= 'result_sec_'+heat+'_'+line;
    	strMs 		= 'result_ms_'+heat+'_'+line;
    	strTieBreak = 'tie_breaker_'+heat+'_'+line;
    	
//     	IsJump = parseInt(thisForm.isHighJump.value);
		
    	if($(thisForm).find("[name='"+strAttend+"']").val()==3) {	// waived absent => show reason textbox
    		$(thisForm).find("[name='"+strReason+"']").show().attr("disabled","");
    	} else {
    		$(thisForm).find("[name='"+strReason+"']").hide().attr("disabled","disabled");
    	}
    	
    	if($(thisForm).find("[name='"+strAttend+"']").val()==2 || $(thisForm).find("[name='"+strAttend+"']").val()==3) {  // Absent => disabled result
    		var disabled = "disabled";
    	} else {
    		var disabled = "";
    	}
    	// field
//     	if(IsJump)
//     		$(thisForm).find("[name='"+strRecord+"']").attr("disabled",disabled)
//     	else
			$(thisForm).find("."+strRecord).attr("disabled",disabled)
    	$(thisForm).find("[name='"+strTieBreak+"']").attr("disabled",disabled)
    	
    	//track
    	$(thisForm).find("[name='"+strMin+"']").attr("disabled",disabled)
    	$(thisForm).find("[name='"+strSec+"']").attr("disabled",disabled)
    	$(thisForm).find("[name='"+strMs+"']").attr("disabled",disabled)
    }
	
//    function UpdateBestTrial(roundType,heat,line)
//    {
//     	var jQ_formObj = $("#form_"+roundType+"_"+heat);
//     	var max = 0;
//     	jQ_formObj.find("input.record_trial_"+line).each(function(){
//     		var thisVal = $(this).val();
//     		if(!isNaN(thisVal)&&parseFloat(thisVal)>parseFloat(max))
//     			max = thisVal;
//     	});
//   	
//     	jQ_formObj.find("input.record_"+line).val(max)
//    	
//    	<? if ($IsAllTrialIncluded) { ?>
//     		if($('input#record_trial_'+roundType+'_'+line+'_1st_round')) {
//     			var thisVal = $('input#record_trial_'+roundType+'_'+line+'_1st_round').val();
//     			if(!isNaN(thisVal) && parseFloat(thisVal) > parseFloat(max)) {
//     				max = thisVal;
//     			}
//     		}
//    	<? } ?>
//     }
</script>

<br />   
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<!--
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align="right" valign="bottom">
		<?=$linterface->GET_ACTION_BTN($button_print, "button", "window.open('../report/result_export_process.php?EventGroupID=$eventGroupID&isResult=1')");?>&nbsp;&nbsp;
		<?=$linterface->GET_ACTION_BTN($button_export, "button", "document.form_export.submit();");?>&nbsp;&nbsp;
		<?=$linterface->GET_ACTION_BTN($button_back, "button", "window.location='tf_record.php'");?>&nbsp;&nbsp;
		</td>
</tr>
<tr>
	<td colspan="2">
		<div class="content_top_tool">
			<div class="Conntent_tool">
				<?=$printBtn?>
				<?=$exportBtn?>
			</div>
		</div>
	</td>
</tr>
-->
<tr>
    <td>
		<div class="content_top_tool">
			<div class="Conntent_tool">
				<?=$printBtn?>
				<?=$exportBtn?>
			</div>
		</div>
	</td>
    <td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($xmsg,$xmsg2);?></td>
</tr>
<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td>
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                
                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Item?> </span></td>
					<td class="tabletext"><?=$eventName?></td>
				</tr>
                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_field_Group?> </span></td>
					<td class="tabletext"><?=$groupName?></td>
				</tr>
                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Record?> </span></td>
					<td class="tabletext"><?=$record?></td>
				</tr>
                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_New_Record?> </span></td>
					<td class="tabletext"><?=$new_record?></td>
				</tr>
                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Standard_Record?> </span></td>
					<td class="tabletext"><?=$standard?></td>
				</tr>
				</table>
					</td>
                </tr>
                </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                </table>                                
	</td>
</tr>
</table>                        

<?=$RoundTables?>
<br />

<form name="form_export" action='tf_record_export.php' method="post">
    <input type="hidden" name="exportFileName" value="event-result">
    <input type="hidden" id="exportContent" name="exportContent">
    <input type="hidden" id="exportEventContent" name="exportEventContent" value="<?=htmlspecialchars($export_event_content)?>">
    <input type="hidden" id="exportContent_1" name="exportContent_1" value="<?=htmlspecialchars($export_content_arr[1])?>">
    <input type="hidden" id="exportContent_2" name="exportContent_2" value="<?=htmlspecialchars($export_content_arr[2])?>">
    <input type="hidden" id="exportContent_0" name="exportContent_0" value="<?=htmlspecialchars($export_content_arr[0])?>">   
</form>

<script>
<?=$js_gen_options?>
</script>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>