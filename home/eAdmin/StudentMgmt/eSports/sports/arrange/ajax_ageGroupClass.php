<?php
# using:

#################################################
#
#   Date:   2018-08-10 Philips  [2018-0628-1634-01096]
#           Create file
#
#################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$str = '';
if($classID != '')
{
    $lsports = new libsports();
    $result = $lsports->retrieveClassStudent($classID, $ageGroupID, $gender);
    
    // Get Student in Class Group from other Age Groups
    $excludeClassGroupArr = $lsports->retrieveClassRelayClassGroup('', '', $classID, '', '', $excludeAgeGroupID=$ageGroupID);
    $excludeClassGroupIDArr = Get_Array_By_Key((array)$excludeClassGroupArr, 'ClassRelayGroupID');
    if(!empty($excludeClassGroupIDArr)) {
        $excludeClassGroupStudentArr = $lsports->retrieveClassRelayGroupStudent($excludeClassGroupIDArr, '', true);
        $excludeClassGroupStudentIDArr = Get_Array_By_Key((array)$excludeClassGroupStudentArr, 'UserID');
    }
    
    $ary = array();
    foreach((array)$result as $rs)
    {
        // skip those students
        if(!empty($excludeClassGroupStudentIDArr) && in_array($rs['UserID'], (array)$excludeClassGroupStudentIDArr)) {
            continue;
        }
        
        if(!in_array($rs['UserID'], (array)$ary)) {
            $str .= '<option sort="'.$rs['ClassNumber'].'" value="'.$rs['UserID'].'">'.$rs['sname'].' ('.$rs['ClassNumber'].')</option>'."\n";
            $ary[] = $rs['UserID'];
        }
    }
}

echo $str;

intranet_closedb();
?>