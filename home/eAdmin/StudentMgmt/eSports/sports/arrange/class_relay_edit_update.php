<?php
# using:

#################################################
#
#   Date:   2018-09-26 Bill  [2018-0628-1634-01096]
#           support Class Group for relay
#
#   Date:   2018-09-04 Bill  [2018-0628-1634-01096]
#           Allow Kao Yip Class Teacher to update relay ($sys_custom['eSports']['KaoYipRelaySettings'])
#
#   Date:   2018-08-10 Philips  [2018-0628-1634-01096]
#           support adding class students ($sys_custom['eSports']['KaoYipRelaySettings'])
#
#################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();

// [2018-0628-1634-01096] Kao Yip Class Teacher
// if($lsports->isYearClassTeacher()) {
//     // do nothing
// } else {
$lsports->authSportsSystem();

$eventGroupID = $_POST['eventGroupID'];

$exist_flag = 0;
for($i=1; $i<=$lsports->numberOfLanes; $i++)
{
	${"ClassID".$i} = $_POST['ClassID'.$i];
	if($sys_custom['eSports']['KaoYipRelaySettings'])
	{
	    // skip for Kao Yip Class Teacher - as cannot edit non teaching class
// 	    if($lsports->isYearClassTeacher() && !isset($_POST['ClassID'.$i])) {
// 	        continue;
// 	    }
//         ${"StudentID".$i} = $_POST['Student_Selected'.$i];

	    $cgid = $_POST['ClassGroupID'.$i];
	    if($cgid != '') {
	        $thisClassGroup = $lsports->retrieveClassRelayClassGroup($cgid);
	        ${"ClassID".$i} = $thisClassGroup[0]['YearClassID'];
	    }
	}
	
	$cid = ${"ClassID".$i};
	if($cid != "")	# update / insert 
	{
		# Check whether the record exist
		$sql = "SELECT ClassID FROM SPORTS_CLASS_RELAY_LANE_ARRANGEMENT WHERE EventGroupID = '$eventGroupID' AND ArrangeOrder = '$i'";
		$temp = $lsports->returnVector($sql);
		$exist_ClassID = $temp[0];
		if($exist_ClassID != "")		# Update Record
		{
			if($exist_ClassID != $cid)
			{
				$sql = "UPDATE SPORTS_CLASS_RELAY_LANE_ARRANGEMENT 
							SET ClassID = '$cid', ClassGroupID = '$cgid', DateModified = now()
							WHERE EventGroupID = '$eventGroupID' AND ArrangeOrder = '$i'";
				$lsports->db_db_query($sql);
			}
			
// 			if($sys_custom['eSports']['KaoYipRelaySettings']){
//                 $lsports->updateClassRelayLaneMapping($eventGroupID, $i, ${"StudentID".$i});
// 			}
		}
		else		# Add Record
		{
			$sql = "INSERT INTO SPORTS_CLASS_RELAY_LANE_ARRANGEMENT 
						(EventGroupID, ClassID, ClassGroupID, ArrangeOrder, DateModified)
						VALUES ('$eventGroupID', '$cid', '$cgid', '$i', now())";
			$lsports->db_db_query($sql);
			
// 			if($sys_custom['eSports']['KaoYipRelaySettings']){
//                 $lsports->updateClassRelayLaneMapping($eventGroupID, $i, ${"StudentID".$i});
// 			}
		}
	}
	else	# delete 
	{	
		$sql = "DELETE FROM SPORTS_CLASS_RELAY_LANE_ARRANGEMENT WHERE EventGroupID='$eventGroupID' and ArrangeOrder=$i";
		$lsports->db_db_query($sql);
		
// 		if($sys_custom['eSports']['KaoYipRelaySettings']){
//             $lsports->deleteClassRelayLaneMapping($eventGroupID, $i);
// 		}
	}
	
	/*
	if($cid != "")	# update / insert 
	{
		# Check whether the record exist
		$sql = "SELECT COUNT(*) FROM SPORTS_CLASS_RELAY_LANE_ARRANGEMENT WHERE EventGroupID = '$eventGroupID' AND ClassID = '$cid'";
		$temp = $lsports->returnVector($sql);
		$exist_flag = $temp[0];
		
		if($exist_flag != 0)		# Update Record
		{
			$sql = "UPDATE SPORTS_CLASS_RELAY_LANE_ARRANGEMENT 
						SET ArrangeOrder = '$i', DateModified = now()
						WHERE EventGroupID = '$eventGroupID' AND ClassID = '$cid'";
			$lsports->db_db_query($sql);

		}
		else		# Add Record
		{
			$sql = "INSERT INTO SPORTS_CLASS_RELAY_LANE_ARRANGEMENT 
						(EventGroupID, ClassID, ArrangeOrder, DateModified)
						VALUES ('$eventGroupID', '$cid', '$i', now())";
			$lsports->db_db_query($sql);
		}
	}
	*/
}
intranet_closedb();
header("Location: schedule_class_relay.php?xmsg2=update");
?>