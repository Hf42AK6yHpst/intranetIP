<?php
# using:

#################################################
#
#   Date:   2019-04-10 Bill     [2019-0301-1144-56289]
#           Create file
#
#################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();

# Delete Record
$sql = "DELETE FROM SPORTS_CLASS_RELAY_CLASS_GROUP WHERE ClassRelayGroupID = '$class_group_id'";
$lsports->db_db_query($sql);

$lsports->updateClassRelayGroupStudent($class_group_id, $event_group_id, '');

echo 1;

intranet_closedb();
?>