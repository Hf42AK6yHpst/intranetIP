<?php
# using:

#################################################
#
#   Date:   2018-09-26  (Bill)  [2018-0628-1634-01096]
#           Create file
#
#################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$str = '';
if($_GET['classGroupID'] != '')
{
    $lsports = new libsports();
    $result = $lsports->retrieveClassRelayGroupStudent($_GET['classGroupID'], true);
    foreach($result as $rs) {
        $str .= '<option sort="'.$rs['ClassNumber'].'" value="'.$rs['UserID'].'">'.$rs['stu_name'].' ('.$rs['ClassNumber'].')</option>'."\n";
    }
    
}
echo $str;

intranet_closedb();
?>