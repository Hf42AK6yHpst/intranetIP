<?php
# using:

#################################################
#
#   Date:   2019-04-11  Bill    [2018-0628-1634-01096]
#           Create File
#
#################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

if(!$sys_custom['eSports']['KaoYipRelaySettings']) {
    echo "You have no priviledge to access this page.";
    exit();
}

$linterface = new interface_html();
$CurrentPage = "PageArrangement_Schedule";

$lsports = new libsports();
$lclass = new libclass();

// [2018-0628-1634-01096] Kao Yip Class Teacher [removed]
// if($lsports->isYearClassTeacher()) {
//     // do nothing
// } else {
$lsports->authSportsSystem();

# Retrieve Event ID and Name
$CREventName = $lsports->retrieveClassRelayNameByCondition(" AND a.EventGroupID = '$eventGroupID' ");
list($eventID, $groupID, $eventName) = $CREventName[0];
$groupName = $lsports->retrieveAgeGroupName($groupID);
/* 
# Retrieve Event Age Group
$groupID = $lsports->retrieveAgeGroupID($eventGroupID);
$groupID = $groupID[0]['GroupID'];
*/

$numberOfLane = $lsports->numberOfLanes;
$finalLineCount = $numberOfLane;

# Retrieve Event Group Info
$CREventExtInfo = $lsports->retrieveEventGroupExtInfo($eventGroupID, EVENT_TYPE_CLASSRELAY);
if($CREventExtInfo["FinalRoundReq"] == 1 && $CREventExtInfo["FinalRoundNum"] > 0) {
    $finalLineCount = $CREventExtInfo["FinalRoundNum"];
}

# Retrieve Event Class Group
$ClassGroupArr = $lsports->retrieveClassRelayClassGroup('', $groupID, '', '', $eventGroupID);
$totalGroupCount = count($ClassGroupArr);
$arrangedGroupCount = 0;

# Retrieve Event Heat
$heatNumber = ceil($totalGroupCount / $finalLineCount);
$heatNumber = $heatNumber? $heatNumber : 1;

# Retrieve Class Relay Lane Arrangement
// if($heatNumber == 1) {
//     $class_arrange = $lsports->retrieveClassRelayLaneArrangementByRound($eventGroupID, ROUND_TYPE_FINALROUND);
// } else {
    $class_arrange = $lsports->retrieveClassRelayLaneArrangementByRound($eventGroupID, ROUND_TYPE_FIRSTROUND);
// }

# Table Header
$form_content = "";
$form_content .= "<tr>";
    $form_content .= "<td>&nbsp;</td>";
    for($i=1; $i<=$numberOfLane; $i++) {
        $form_content .= "<td class='tablebluetop tabletopnolink'>".$lsports->Get_Lang_Line($i)."</td>";
    }
$form_content .= "</tr>";

// loop Heats
for($h=1; $h<=$heatNumber; $h++)
{
    $form_content .= "<tr height='40'>";
        $form_content .= "<td width='50' rowspan='$rowspan' class='tablebluelist'>".$lsports->Get_Lang_Heat($h)."</td>";
    
    // loop Lines
    for($i=1; $i<=$numberOfLane; $i++) 
    {
    	# Retrieve current lane position
    	$select_class = "";
    	$select_class_group = "";
    	for($j=0; $j<sizeof($class_arrange); $j++)
    	{
    	    $heat = $class_arrange[$j]['Heat'];
    	    $order = $class_arrange[$j]['ArrangeOrder'];
    	    if($heat == $h && $order == $i)
    		{
    		    $select_class = $class_arrange[$j]['ClassID'];
    		    $select_class_group = $class_arrange[$j]['ClassGroupID'];
    		    $arrangedGroupCount++;
    		    
    			break;
    		}
    	}
    	
    	$select_class_group_name = '';
    	if($select_class_group != '') {
    	    $ClassGroupInfo = $lsports->retrieveClassRelayClassGroup($select_class_group);
    	    $ClassGroupInfo = $ClassGroupInfo[0];
    	    
    	    $thisClassName = $lclass->getClassNameByLang($ClassGroupInfo['YearClassID']);
    	    $thisGroupTitle = $ClassGroupInfo['GroupTitle'] == ''? '' : '<br/>('.$ClassGroupInfo['GroupTitle'].')';
    	    $select_class_group_name = $thisClassName.$thisGroupTitle;
    	}
    	
    	# Class Group Selection
    	$class_group_select_tag = " name='groupHeatLine[$h][$i]' id='groupHeatLine_".$h."_".$i."' ";
    	$class_group_select = $lsports->getClassRelayGroupSelection($eventGroupID, $groupID, '', $selectedRelayGroupID=$select_class_group, $class_group_select_tag);
    	
    	$form_content .= "<td>";
        	$form_content .= "<span class='Edit_Hide'>".$select_class_group_name."</span>";
        	$form_content .= "<span class='Edit_Show' style='display: none'>".$class_group_select."</span>";
    	$form_content .= "</td>";
    }
    $form_content .= "</tr>";
}

$house_relay_name = $lsports->retrieveEventTypeNameByID(3);
$class_relay_name = $lsports->retrieveEventTypeNameByID(4);

### Title ###
// if($lsports->isYearClassTeacher()) {
//     $TAGS_OBJ[] = array($class_relay_name);
// } else {
$TAGS_OBJ[] = array($i_Sports_menu_Settings_CommonTrackFieldEvent,"schedule.php", 0);
$TAGS_OBJ[] = array($house_relay_name,"schedule_relay.php", 0);
$TAGS_OBJ[] = array($class_relay_name,"schedule_class_relay.php", 1);
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

$PAGE_NAVIGATION[] = array($button_edit.($intranet_session_language=="en"?" ":"").$class_relay_name);

$linterface->LAYOUT_START();
?>

<br />
<form name="form1" action="class_relay_group_edit_update.php" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'>&nbsp;</td>
</tr>
<tr>
	<td colspan="2">
        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
        	<td>
        		<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
            		<tr valign="top">
            			<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_field_Group?></span></td>
            			<td class="tabletext"><?=$groupName?></td>
            		</tr>
            		<tr valign="top">
            			<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Item?></span></td>
            			<td class="tabletext"><?=$eventName?></td>
            		</tr>
                    <tr valign="top">
            			<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Enroled_Student_Count?></span></td>
            			<td class="tabletext"><?=$totalGroupCount?></td>
            		</tr>
                    <tr valign="top">
            			<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Arranged_Student_Count?></span></td>
            			<td class="tabletext"><?=$arrangedGroupCount?></td>
            		</tr>
        		</table>
			</td>
        </tr>
        </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
        <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
            <tr>
            	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
            </tr>
        </table>                                
	</td>
</tr>
</table>	

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION2($i_Sports_Arrange_Lane) ?></td>
	<td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>
<tr>
	<td colspan="2">
        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr> 
    	<td>
			<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                <?=$form_content?>
			</table>
			<div style='display:none;' class='warnMsgDiv' id='warnDiv'>
                <span class='tabletextrequire'><?=$Lang['eSports']['Sports']['ClassRelay']['ClassGroupAlreadyInAnotherLine'] ?></span>
            </div>
		</td>
    </tr>
        </table>
	</td>
</tr>
<tr>
	<td colspan="2">
        <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
            <tr>
            	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
            </tr>
            <tr>
    			<td align="center">
                    <span class='Edit_Hide'>
        				<?= $linterface->GET_ACTION_BTN($button_edit, "button", "show_edit_view()", "edit2") ?>
                    </span>
    				<span class='Edit_Show' style='display: none'>
        				<?= $linterface->GET_ACTION_BTN($button_submit, "button", "handle_submit()", "submit2") ?>
                        <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "hide_edit_view()", "cancel2") ?>
                    </span>
    			</td>
    		</tr>
        </table>
	</td>
</tr>
</table>
<br />

<input type="hidden" name="eventGroupID" value="<?=$eventGroupID?>">
</form>

<script type="text/javascript">
function show_edit_view()
{
	$('.Edit_Show').show();
	$('.Edit_Hide').hide();
}		

function hide_edit_view()
{
	document.form1.reset();

	$('.Edit_Hide').show();
	$('.Edit_Show').hide();
}

function handle_submit()
{
	$('.warnMsgDiv').hide();

	var valid = true;
	var selectedGroupArr = [];
	var selectGroupObj = $('select[id^=groupHeatLine_]');	
	for(var i=0; i<selectGroupObj.length; i++) {
		var classGroupID = selectGroupObj[i].value;
		if(classGroupID != '') {
			if(selectedGroupArr.indexOf(classGroupID) != -1) {
				valid = false;
				selectGroupObj[i].focus();
				$('#warnDiv').show();
				break;
			} else {
				selectedGroupArr.push(classGroupID);
			}
		}
	}
	
	if(valid) {
		document.form1.submit();
	} else {
		return false;
	}
}

//     function getStudent(num) {
//     	var ele = $('select[name="ClassGroupID' + num + '"]');
//     	var studForm = $('select#Student_Selected' + num);
    	
//     	if(ele.val() != '') {
//         	$.ajax({
//         		url: 'ajax_get_group_student.php',
//         		method: 'post',
//         		data: {
//       			"relayType": "<?=$relayType?>",
//        			"ageGroupID": "<?=$groupID?>",
//         			"classGroupID": ele.val()
//         		},
//         		success: function(res){
//         			studForm.empty().append(res);
//         		}
//         	});
//     	} else {
//     		studForm.empty();
//     	}
//     }

<?php
/* 
    $script_content = "";
    for($i=1; $i<=$numberOfLane; $i++) {
        $script_content .= "getStudent($i);";
    }
    echo $script_content;
 */
?>
</script>

<style>
body{
    min-width: 1080px;
}
td.tableContent select, td.tableContent2 select {
    vertical-align: top;
}
select.stud_select {
    width: 150px;
}
</style>

<?
intranet_closedb();

print $linterface->FOCUS_ON_LOAD("form1.ClassGroupID1");
$linterface->LAYOUT_STOP();
?>