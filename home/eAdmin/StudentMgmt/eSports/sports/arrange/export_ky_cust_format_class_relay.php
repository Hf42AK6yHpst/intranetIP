<?php
# using:

###########################################
#
#	Date:	2019-05-20 Bill [2018-1019-1043-03066]
#			Create File
#
###########################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

function convertToChineseNumber($number)
{
    if($number <= 0) {
        return '';
    }
    
    $digits_arr = array('一', '二', '三', '四', '五', '六', '七', '八', '九');
    $tens = '十';
    
    $str = '';
    if($number >= 10) {
        $tens_digit = floor($number / 10);
        if($tens_digit > 1) {
            $str .= $digits_arr[($tens_digit - 1)];
        }
        $str .= $tens;
    }
    
    $units_digit = $number % 10;
    $str .= $digits_arr[($units_digit - 1)];
    
    return $str;
}

$lsports = new libsports();
$lsports->authSportsSystem();

$numberOfLane = $lsports->numberOfLanes;
$current_year_id = Get_Current_Academic_Year_ID();

$lexport = new libexporttext();

/* 
# Retrieve all age group ids and names
$age_groups = $lsports->retrieveAgeGroupIDNames();

# Add Boys / Girls / Mixed Open
$openBoy[0] = '-1';
$openBoy[1] = $i_Sports_Event_Boys_Open;
$openGirl[0] = '-2';
$openGirl[1] = $i_Sports_Event_Girls_Open;
$openMixed[0] = '-4';
$openMixed[1] = $i_Sports_Event_Mixed_Open;
$age_groups[] = $openBoy;
$age_groups[] = $openGirl;
$age_groups[] = $openMixed;

# Retrieve all round types
//$round_types = array(ROUND_TYPE_FIRSTROUND, ROUND_TYPE_SECONDROUND, ROUND_TYPE_FINALROUND);
$round_types = array(ROUND_TYPE_FIRSTROUND);

# Retrieve all event types
$type_ids = array(EVENT_TYPE_TRACK, EVENT_TYPE_FIELD);
 */

$utf_content = array();
$student_info_ary = array();

# CSV Header
$export_header[] = array("組別", "賽事名稱", "學生號碼布編碼", "班級", "姓名", "道次", "組號 ");

# Retrieve all class relay events
$events = $lsports->retrieveClassRelayName('b5');
for($i=0; $i<sizeof($events); $i++)
{
    # Retrieve event info
    list($event_id, $group_id, $event_name) = $events[$i];
    
    // exclude Open Event first
    if($group_id < 0) {
        continue;
    }
	
    # Retrieve age group info
    $group_info = $lsports->retrieveAgeGroupDetail($group_id);
    $group_code = $group_info['GroupCode'];
    $group_name = $group_info['ChineseName'];
    
    # Retrieve event group info
    $event_group_info = $lsports->retrieveEventGroupID($event_id, $group_id, EVENT_TYPE_CLASSRELAY);
    $event_group_id = $event_group_info['EventGroupID'];
    
    # Retrieve class relay groups
    $class_groups = $lsports->retrieveClassRelayClassGroup('', '', '', '', $event_group_id);
    $class_groups_assoc = BuildMultiKeyAssoc($class_groups, 'ClassRelayGroupID');
    
    # Retrieve class relay line arrangement
    $class_relay_arrange = $lsports->retrieveClassRelayLaneArrangementByRound($event_group_id, ROUND_TYPE_FIRSTROUND);
    if(sizeof($class_relay_arrange) != 0)
    {
        # Retrieve event group setting
        $finalLineCount = $numberOfLane;
        $event_ext_info = $lsports->retrieveEventGroupExtInfo($event_group_id, EVENT_TYPE_CLASSRELAY);
        if($event_ext_info["FinalRoundReq"] == 1 && $event_ext_info["FinalRoundNum"] > 0) {
            $finalLineCount = $event_ext_info["FinalRoundNum"];
        }
        
        # Retrieve event heat number
        $heat_number = ceil(count($class_groups) / $finalLineCount);
        $heat_number = $heat_number? $heat_number : 1;
        
        // loop Heats
        for($h=1; $h<=$heat_number; $h++)
        {
            for($k=0; $k<sizeof($class_relay_arrange); $k++)
            {
                $class_id = $class_relay_arrange[$k]['ClassID'];
                $class_group_id = $class_relay_arrange[$k]['ClassGroupID'];
                $order = $class_relay_arrange[$k]['ArrangeOrder'];
                $heat = $class_relay_arrange[$k]['Heat'];
                $heatNumber = convertToChineseNumber($heat);
                
                if($heat == $h)
                {
                    $class_name_ch = '';
                    $student_name_arr = array();
                    $athlete_num_arr = array();
                    
                    # Retrieve class group students
                    $class_group_student = $lsports->retrieveClassRelayGroupStudent($class_group_id, $event_group_id);
                    if(sizeof($class_group_student) == 0)
                    {
                        continue;
                    }
                    foreach((array)$class_group_student as $this_student)
                    {
                        # Retrieve student info
                        $student_id = $this_student['UserID'];
                        if(!isset($student_info_ary[$student_id]))
                        {
                            $lu = new libuser($student_id);
                            
                            // Student name
                            $student_name_ch = $lu->ChineseName;
                            
                            // Class name
                            $student_info = $lu->getStudentInfoByAcademicYear($current_year_id);
                            $class_name_ch = $student_info[0]['ClassNameCh'];
                            
                            // Athletic number
                            $athlete_num = $lsports->returnStudentAthleticNum($student_id);
                            // $athlete_num = $athlete_num? $athlete_num : '--';
                            
                            # Store student into
                            $student_info_ary[$student_id] = array($student_name_ch, $class_name_ch, $athlete_num);
                        }
                        list($student_name_ch, $class_name_ch, $athlete_num) = $student_info_ary[$student_id];
                        
                        $this_row = array();
                        $this_row[] = "$group_code";
                        $this_row[] = "$event_name";
                        $this_row[] = "$athlete_num";
                        $this_row[] = "$class_name_ch";
                        $this_row[] = "$student_name_ch";
                        $this_row[] = "$order";
                        $this_row[] = "第"."$heatNumber"."組";
                        $utf_content[] = $this_row;
                        
//                         $student_name_arr[] = $student_name_ch;
//                         $athlete_num_arr[] = $athlete_num;
                    }
//                     $student_name_display = implode(',', (array)$student_name_arr);
//                     $athlete_num_display = implode(',', (array)$athlete_num_arr);
//                    
//                     $this_row = array();
//                     $this_row[] = $group_code;
//                     $this_row[] = $event_name;
//                     $this_row[] = $athlete_num_display;
//                     $this_row[] = $class_name_ch;
//                     $this_row[] = $student_name_display;
//                     $this_row[] = $order;
//                     $this_row[] = '第'.convertToChineseNumber($heat).'組';
//                     $utf_content[] = $this_row;
                    
                    unset($class_groups_assoc[$class_group_id]);
                }
            }
        }
    }
    
    # Retrieve class group without line arrangement
    if(sizeof($class_groups_assoc) != 0)
    {
        foreach((array)$class_groups_assoc as $this_class_group)
        {
            $class_id = $this_class_group['ClassID'];
            $class_group_id = $this_class_group['ClassRelayGroupID'];
            
            $class_name_ch = '';
            $student_name_arr = array();
            $athlete_num_arr = array();
            
            # Retrieve class group students
            $class_group_student = $lsports->retrieveClassRelayGroupStudent($class_group_id, $event_group_id);
            if(sizeof($class_group_student) == 0)
            {
                continue;
            }
            foreach((array)$class_group_student as $this_student)
            {
                # Retrieve student info
                $student_id = $this_student['UserID'];
                if(!isset($student_info_ary[$student_id]))
                {
                    $lu = new libuser($student_id);
                    
                    // Student name
                    $student_name_ch = $lu->ChineseName;
                    
                    // Class name
                    $student_info = $lu->getStudentInfoByAcademicYear($current_year_id);
                    $class_name_ch = $student_info[0]['ClassNameCh'];
                    
                    // Athletic number
                    $athlete_num = $lsports->returnStudentAthleticNum($student_id);
                    // $athlete_num = $athlete_num? $athlete_num : '--';
                    
                    # Store student into
                    $student_info_ary[$student_id] = array($student_name_ch, $class_name_ch, $athlete_num);
                }
                
                list($student_name_ch, $class_name_ch, $athlete_num) = $student_info_ary[$student_id];
                
                $this_row = array();
                $this_row[] = "$group_code";
                $this_row[] = "$event_name";
                $this_row[] = "$athlete_num";
                $this_row[] = "$class_name_ch";
                $this_row[] = "$student_name_ch";
                $this_row[] = '';
                $this_row[] = '';
                $utf_content[] = $this_row;
                
//                 $student_name_arr[] = $student_name_ch;
//                 $athlete_num_arr[] = $athlete_num;
            }
//             $student_name_display = implode(',', (array)$student_name_arr);
//             $athlete_num_display = implode(',', (array)$athlete_num_arr);
            
//             $this_row = array();
//             $this_row[] = $group_code;
//             $this_row[] = $event_name;
//             $this_row[] = $athlete_num_display;
//             $this_row[] = $class_name_ch;
//             $this_row[] = $student_name_display;
//             $this_row[] = '';
//             $this_row[] = '';
//             $utf_content[] = $this_row;
        }
    }
}

$filename = "class_relay_export.csv";
$export_text = $lexport->GET_EXPORT_TXT($utf_content, $export_header);
$lexport->EXPORT_FILE($filename,$export_text); 

intranet_closedb();
?>