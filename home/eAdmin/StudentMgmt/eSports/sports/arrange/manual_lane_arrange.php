<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# Get House info
$lsports = new libsports();
$lsports->authSportsSystem();

$changeGroupNum = $_POST['changeGroupNum'];
$eventGroupID = $_POST['eventGroupID'];

for($i=0; $i<$changeGroupNum; $i++)
{
	$group1 = ${"group1_".$i};
	$group2 = ${"group2_".$i};
	$pos1 = ${"lane1_".$i};
	$pos2 = ${"lane2_".$i};

	if($group1 != "" && $group2 != "" && $pos1 != "" && $pos2 != "")
	{
		# Retrieve Student Numbers 
		$student1 = $lsports->returnStudentIDFromLaneArrange($group1, $pos1, $eventGroupID);
		$student2 = $lsports->returnStudentIDFromLaneArrange($group2, $pos2, $eventGroupID);

		if($student1 != "")
		{
			# Change the position of the two students
			$sql = "UPDATE SPORTS_LANE_ARRANGEMENT SET Heat = '$group2', ArrangeOrder = '$pos2'
						WHERE StudentID = '$student1' AND EventGroupID = '$eventGroupID' AND RoundType = '$roundType'";
			$lsports->db_db_query($sql);
		}
		if($student2 != "" && $student1 != "")
		{
			$sql = "UPDATE SPORTS_LANE_ARRANGEMENT SET Heat = '$group1', ArrangeOrder = '$pos1'
						WHERE StudentID = '$student2' AND EventGroupID = '$eventGroupID' AND RoundType = '$roundType'";
			$lsports->db_db_query($sql);
		}
	}
}

intranet_closedb();
header ("Location:tf_lane_arrange_detail.php?eventGroupID=$eventGroupID&eventType=$eventType&total=$total&arranged=$arranged&eventName=$eventName&xmsg=update");

?>