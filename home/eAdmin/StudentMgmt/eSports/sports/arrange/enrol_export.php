<?
# using: Bill

####################################################
#
#	Date:	2017-02-27	Bill	[2016-0627-1013-19066]
#			Improved: support student in multiple age groups	($sys_custom['eSports']['PuiChi_MultipleGroup'])
#
#	Date:	2014-10-22	YatWoon [Case#A67796] [ip.2.5.5.10.1]
#			Improved: display data according to the ui language
#
####################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();
$lexport = new libexporttext();

$result = array();

# CSV Header
$header_ary = array("student name (Eng)", "student name (Chi)", "House", "Class", "Class Number", "Sex", "Grade", "Event1", "Event2", "Event3");
$result[] = $header_ary;

# Age Group Info
$AgeGroupInfo = BuildMultiKeyAssoc((array)$lsports->retrieveAgeGroupInfo(), "AgeGroupID");

// loop Students
$students = $lsports->retrieveStudentInfo($engName, $chiName, $theClass, $ageGroup, $house, " d.Sequence, a.ClassName, a.ClassNumber ");
foreach($students as $key => $thisStudent)
{
	# Student Info
	list($tmp_UserID, $tmp_EnName, $tmp_ChiName, $tmp_EnClass, $tmp_ChiClass, $tmp_ClassNumber, $Gender) = $thisStudent;
    
 	# Retrieve Age Group
 	$AgeGroupIDAry = $lsports->retrieveAgeGroupByStudentID($tmp_UserID, $sys_custom['eSports']['PuiChi_MultipleGroup']);
 	$AgeGroupIDAry = (array)$AgeGroupIDAry;
 	
 	// [2016-0627-1013-19066] loop Age Groups
 	foreach($AgeGroupIDAry as $AgeGroupID)
	{
	    $tmpResult = array();
		
		# Student Name
		$tmpResult[] = $tmp_EnName;
		$tmpResult[] = $tmp_ChiName;
		
		# House
		$tmpResult[] = $lsports->retrieveStudentHouse($tmp_UserID);
		
		# Class
		$tmpResult[] = $tmp_EnClass;
		
		# Class Number
		$tmpResult[] = $tmp_ClassNumber;
	 	
	 	# Gender
	 	if($Gender=="M")
	 		$Gender = "男";
	 	else if($Gender=="F")
			$Gender = "女";
	 	$tmpResult[] = $Gender;
	 	
	 	# Age Group
		$tmpResult[] = $intranet_session_language=="en" ? $AgeGroupInfo[$AgeGroupID]["EnglishName"] : $AgeGroupInfo[$AgeGroupID]["ChineseName"];
		
		# Retrieve Enroled Events
		if($sys_custom['eSports']['PuiChi_MultipleGroup'])
			$tmpEvents = $lsports->retrieveStudentEnroledEvent($tmp_UserID, 1, 0, $AgeGroupID);
		else
			$tmpEvents = $lsports->retrieveStudentEnroledEvent($tmp_UserID, 1);
		
		# Event
	    for($i=0; $i<3; $i++)
	    {
			$tmpResult[] = $intranet_session_language=="en" ? $tmpEvents[$i][0] : $tmpEvents[$i][1];
	  	}
	 	
	 	$result[] = $tmpResult;
	}
}

# Build CSV Content
$utf_content = "";
foreach($result as $k=>$d)
{
	$utf_content .= implode("\t", $d);
	$utf_content .= "\r\n";
}

# Export CSV File
$filename = "sport_student_enrolment_list.csv";
$lexport->EXPORT_FILE($filename, $utf_content);

intranet_closedb();
?>