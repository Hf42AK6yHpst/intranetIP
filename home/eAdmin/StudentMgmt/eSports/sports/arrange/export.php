<?php
# using: 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lclass = new libclass();
$lsports = new libsports();
$lsports->authSportsSystem();

$lexport = new libexporttext();

$utf_content .= "Class Name\t";
$utf_content .= "Class Number\t";
$utf_content .= "Event Code\t";
$utf_content .= "\r\n";


if($importTarget=="form")
	$enroldetail=$lsports->returnEnrolmentRecordByYearID($FormSelection);
else
	$enroldetail=$lsports->returnEnrolmentRecordByClassID($ClassSelection);

foreach($enroldetail as $record)
{
	list($EventCode,$ClassNumber,$ClassTitleEN) = $record;
	$utf_content .= $EventCode."\t";
	$utf_content .= $ClassNumber."\t";
	$utf_content .= $ClassTitleEN."\t";
	$utf_content .= "\r\n";
}
	



$filename = "sport_day_enrolment_list.csv";

$lexport->EXPORT_FILE($filename, $utf_content);

intranet_closedb();
?>