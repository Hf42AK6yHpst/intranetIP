<?php
# using:

#################################################
#
#	Date:	2019-05-20 Bill  [2018-1019-1043-03066]
#			add cust "Export for customized format"
#
#   Date:   2019-04-09 Bill  [2019-0301-1144-56289]
#           Updated Class Group Display ($sys_custom['eSports']['KaoYipRelaySettings'])
#
#   Date:   2018-10-29 Bill  [2018-0628-1634-01096]
#           Display Class Group ($sys_custom['eSports']['KaoYipReports'])
#
#   Date:   2018-09-04 Bill  [2018-0628-1634-01096]
#           Allow Kao Yip Class Teacher to input relay ($sys_custom['eSports']['KaoYipReports'])
#
#################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "PageArrangement_Schedule";

$lsports = new libsports();
$lclass = new libclass();

// [2018-0628-1634-01096] Kao Yip Class Teacher
if($lsports->isYearClassTeacher()) {
    // do nothing
} else {
    $lsports->authSportsSystem();
}

### Retrieve class relay event group ids and names
$CREvents = $lsports->retrieveClassRelayName();

$house_relay_name = $lsports->retrieveEventTypeNameByID(3);
$class_relay_name = $lsports->retrieveEventTypeNameByID(4);
$numberOfLane = $lsports->numberOfLanes;

### Number of lane for class relay
$lane_number = "<tr>";
$lane_number .= "<td>&nbsp;</td>";
if($sys_custom['eSports']['KaoYipRelaySettings']) {
    $lane_number .= "<td>&nbsp;</td>";
}
for($i=1; $i<=$numberOfLane; $i++) {
	$lane_number .= "<td class='tabletop tabletopnolink' align='center'>".$i_Sports_The.$i.$i_Sports_Line."</td>";
}
$lane_number .= "<td class='tabletop tabletopnolink'>&nbsp;</td>";
$lane_number .= "</tr>";

### Class relay content
$class_content = "";
for($j=0; $j<sizeof($CREvents); $j++)
{
	list($event_id, $group_id, $event_name) = $CREvents[$j];
	$type_id = 4;

	// if(true || $group_id > 0)
	// {
		$group_name = $lsports->retrieveAgeGroupName($group_id);	
		
		$eventGroupInfo = $lsports->retrieveEventGroupID($event_id, $group_id, $type_id);
		$eventGroupID = $eventGroupInfo[0];
		
		# Replace the space in the string
		$tempName = str_replace(" ", ",", $event_name);
 		$temp_group_name = str_replace("'", "\'", $group_name);
		
		if(!$sys_custom['eSports']['KaoYipRelaySettings'])
		{
		    // $edit_link = "class_relay_edit.php?eventGroupID=".$eventGroupID."&groupName=".$temp_group_name."&eventName=".$tempName;
		    $edit_link = "class_relay_edit.php?eventGroupID=".urlencode($eventGroupID)."&groupName=".urlencode($temp_group_name)."&eventName=".urlencode($tempName);
		    
		    $class_content .= "<tr class='tablerow". ($j % 2? '2' : '1') ."'>";
                $class_content .= "<td class='tablelist'>(".$group_name.") ".$event_name."</td>";
		    
    		$class_arrange = $lsports->retrieveClassRelayLaneArrangement($eventGroupID);
    		if(sizeof($class_arrange) != 0)
    		{
    			$cr_display = "";
    			for($m=1; $m<=$numberOfLane; $m++)
    			{
    				$flag = 0;
    				for($k=0; $k<sizeof($class_arrange); $k++)
    				{
    					list($class_id, $order) = $class_arrange[$k];
    					if($order == $m)
    					{
    						$ClassInfo = $lclass->getClassName($class_id);
    						/* 
    						// [2018-0628-1634-01096] [removed]
    						if($sys_custom['eSports']['KaoYipReports']) {
    						    $ClassGroupInfo = $lsports->retrieveClassRelayClassGroup($class_arrange[$k]['ClassGroupID']);
    						    $ClassGroupInfo = '<br/>('.$ClassGroupInfo[0]['GroupTitle'].')';
    						}
                             */
    						
    						$cr_display .= "<td align='center'>".$ClassInfo."</td>";
    						$flag = 1;
    						
    						break;
    					}
    				}
                    
    				if($flag != 1) {
    					$cr_display .= "<td>&nbsp;</td>";
    				}
    			}
    			$class_content .= $cr_display;
    		}
    		else
    		{
    		    $class_content .= "<td colspan=".$numberOfLane.">&nbsp;</td>";
    		}
    		
                $class_content .= "<td align='center'>". $linterface->GET_BTN($button_edit, "button", "window.location='". addslashes($edit_link) ."'","submit2") ."</td>";
    		$class_content .= "</tr>";
		}
		else
		{
		    // $edit_link = "class_relay_edit.php?eventGroupID=".$eventGroupID."&groupName=".$temp_group_name."&eventName=".$tempName;
		    $edit_link = "class_relay_group_edit.php?eventGroupID=".urlencode($eventGroupID)."&groupName=".urlencode($temp_group_name)."&eventName=".urlencode($tempName);
		    
		    // Get Event Group Setting
		    $finalLineCount = $numberOfLane;
		    $CRExtInfo = $lsports->retrieveEventGroupExtInfo($eventGroupID, $type_id);
		    if($CRExtInfo["FinalRoundReq"] == 1 && $CRExtInfo["FinalRoundNum"] > 0) {
		        $finalLineCount = $CRExtInfo["FinalRoundNum"];
		    }
            
		    // Get Class Relay Class Group
		    $ClassGroupArr = $lsports->retrieveClassRelayClassGroup('', '', '', '', $eventGroupID);
		    $heatNumber = ceil(count($ClassGroupArr) / $finalLineCount);
		    $heatNumber = $heatNumber? $heatNumber : 1;
		    
		    // Get Class Relay Lane Arrangement
		    $class_arrange = $lsports->retrieveClassRelayLaneArrangementByRound($eventGroupID, ROUND_TYPE_FIRSTROUND);
// 		    $class_arrange_final = $lsports->retrieveClassRelayLaneArrangementByRound($eventGroupID, ROUND_TYPE_FINALROUND);
		    
		    $class_content .= "<tr class='tablerow". ($j % 2? '2' : '1') ."'>";
                $class_content .= "<td class='tablelist' rowspan='$heatNumber'>(".$group_name.") ".$event_name."</td>";
            
            // loop Heats
            $isEventHeatFirstLine = true;
		    for($h=1; $h<=$heatNumber; $h++)
		    {
		        if(!$isEventHeatFirstLine)
		        {
		            $class_content .= "</tr>";
		            
		            $class_content .= "<tr class='tablerow". ($j % 2? '2' : '1') ."'>";
		        }
		        
		        if($heatNumber > 1) {
		            $class_content .= "<td class='tablelist'>".$lsports->Get_Lang_Heat($h)."</td>";
		        }
		        else {
		            $class_content .= "<td class='tablelist'>&nbsp;</td>";
		        }
		        
		        if(sizeof($class_arrange) != 0)
    		    {
    		        $cr_display = "";
    		        for($m=1; $m<=$numberOfLane; $m++)
    		        {
    		            # Class Group in First Round
    		            $flag = 0;
    		            for($k=0; $k<sizeof($class_arrange); $k++)
    		            {
    		                $class_id = $class_arrange[$k]['ClassID'];
    		                $heat = $class_arrange[$k]['Heat'];
    		                $order = $class_arrange[$k]['ArrangeOrder'];
    		                if($heat == $h && $order == $m)
    		                {
    		                    $ClassName = $lclass->getClassName($class_id);
    		                    
    	                        $ClassGroupInfo = $lsports->retrieveClassRelayClassGroup($class_arrange[$k]['ClassGroupID']);
    	                        $ClassGroupTitle = $ClassGroupInfo[0]['GroupTitle'] == ''? '' : '<br/>('.$ClassGroupInfo[0]['GroupTitle'].')';
    		                    
    	                        $cr_display .= "<td align='center'>".$ClassName.$ClassGroupTitle."</td>";
    		                    $flag = 1;
    		                    
    		                    break;
    		                }
    		            }
    		            /* 
    		            # Class Group in Final (if no First Round)
    		            if($flag == 0)
    		            {
        		            for($k=0; $k<sizeof($class_arrange_final); $k++)
        		            {
        		                $class_id = $class_arrange_final[$k]['ClassID'];
        		                $heat = $class_arrange_final[$k]['Heat'];
        		                $order = $class_arrange_final[$k]['ArrangeOrder'];
        		                if($heat == $h && $order == $m)
        		                {
        		                    $ClassName = $lclass->getClassName($class_id);
        		                    
        		                    $ClassGroupInfo = $lsports->retrieveClassRelayClassGroup($class_arrange_final[$k]['ClassGroupID']);
        		                    $ClassGroupTitle = $ClassGroupInfo[0]['GroupTitle'] == ''? '' : '<br/>('.$ClassGroupInfo[0]['GroupTitle'].')';
        		                    
        		                    $cr_display .= "<td align='center'>".$ClassName.$ClassGroupTitle."</td>";
        		                    $flag = 1;
        		                    
        		                    break;
        		                }
        		            }
    		            }
    		             */
    		            
    		            if($flag != 1) {
    		                $cr_display .= "<td>&nbsp;</td>";
    		            }
    		        }
    		        $class_content .= $cr_display;
    		    }
    		    else
    		    {
    		        $class_content .= "<td colspan=".$numberOfLane.">&nbsp;</td>";
    		    }
    		    
    		    if($isEventHeatFirstLine) {
                    $class_content .= "<td align='center' rowspan='$heatNumber'>". $linterface->GET_BTN($button_edit, "button", "window.location='". addslashes($edit_link) ."'","submit2") ."</td>";
    		    }
    		    $isEventHeatFirstLine = false;
		    }
		    $class_content .= "</tr>";
		}
	//}
}

### Title ###
if($lsports->isYearClassTeacher()) {
    $TAGS_OBJ[] = array($class_relay_name);
} else {
    $TAGS_OBJ[] = array($i_Sports_menu_Settings_CommonTrackFieldEvent,"schedule.php", 0);
    $TAGS_OBJ[] = array($house_relay_name,"schedule_relay.php", 0);
    $TAGS_OBJ[] = array($class_relay_name,"schedule_class_relay.php", 1);
}
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<? if($sys_custom['eSports']['KaoYipReports']) { ?>
    <div class="Conntent_tool">
		<?=$linterface->GET_LNK_EXPORT_IP25("export_ky_cust_format_class_relay.php", $Lang['eSports']['ExportForCustExportFormat'])?>
    </div>
    <p class="spacer"></p>
<? } ?>

<!------------- Start of the Class Relay Table --------------------------------------//-->
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>&nbsp;</td>
    <td align="right"><?=$linterface->GET_SYS_MSG($xmsg2);?></td>
</tr>
<tr>
	<td align="center" colspan="2">
    	<table width="96%" border="0" cellspacing="0" cellpadding="0">
        	<tr>
            	<td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="4">
                        <?=$lane_number?>
                        <?=$class_content?>
                	</table>
                </td>
			</tr>
            <tr>
    			<td height="1" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    		</tr>
		</table>
	</td>
</tr>
</table>

<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>