<?php
# using: Bill

####################################################
#
#	2017-02-27	Bill	[2016-0627-1013-19066]
#	- support student in multiple age groups	($sys_custom['eSports']['PuiChi_MultipleGroup'])
#
#	2013-09-03	Roy
#	- add trackCount and fieldCount
#
#	2012-06-08	YatWoon
#	- add event quota checking (double check)
#
####################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();

$trackCount = $_REQUEST["tc"];
$fieldCount = $_REQUEST["fc"];

# [2016-0627-1013-19066] Defined Age Group from enrol_update.php -> Get related record_id for data handling
$cond = '';
if($sys_custom['eSports']['PuiChi_MultipleGroup'] && !empty($AgeGroupID) && $AgeGroupID > 0){
	$sql = "SELECT ssee.RecordID, ssee.EventGroupID FROM SPORTS_STUDENT_ENROL_EVENT ssee INNER JOIN SPORTS_EVENTGROUP seg ON (ssee.EventGroupID = seg.EventGroupID) WHERE ssee.StudentID = '$StudentID' AND (seg.GroupID = '$AgeGroupID' OR seg.GroupID < 0)";
	$targetRecords = $lsports->returnArray($sql, 1);
	$targetRecordIDs = Get_Array_By_Key($targetRecords, "RecordID");
	$targetEventGroupID = Get_Array_By_Key($targetRecords, "EventGroupID");
	
	$cond .= " AND RecordID IN ('".implode("','", (array)$targetRecordIDs)."')";
	$cond2 .= " AND EventGroupID IN ('".implode("','", (array)$targetEventGroupID)."')";
	
	$key = "&AgeGroup=$AgeGroupID";
}

# Clear old records in SPORTS_STUDENT_ENROL_EVENT
$sql = "DELETE FROM SPORTS_STUDENT_ENROL_EVENT WHERE StudentID = '$StudentID' $cond";
$lsports->db_db_query($sql);

# Insert Restrict Quota Event Enrolment Record(s)
$count = 0;
$face_full_quota = 0;
$typeArr = explode(",", $types);
for($i=0; $i<sizeof($typeArr); $i++)
{
	$t = $typeArr[$i];
	$tempArr = ${RQ_EventGroup.$t};
	$de = "";
	$values = "";
	${totalCount.$t} = 0;
	
	// loop Event Groups
	for($j=0; $j<sizeof($tempArr); $j++)
	{
		$egid = $tempArr[$j];
		
		# Check if Event is full or not
        $cur_enroled = $lsports->returnEventEnroledNo($egid, $StudentID);
        $eventQuota = $lsports->returnEventQuota($egid);
        if($cur_enroled < $eventQuota || !$eventQuota)	
        {
// 			$values .= $de."(".$StudentID.", ".$egid.", now(), $UserID)";
			$values = "(".$StudentID.", ".$egid.", now(), $UserID)";
// 			$de = ",";
	
			# Get all enroled EventGroupID
			$allEvents[$count] = $egid;
			$count++;
			
			$fields = "(StudentID, EventGroupID, DateModified, InputBy)";
			$sql = "INSERT INTO SPORTS_STUDENT_ENROL_EVENT $fields VALUES $values";
			$lsports->db_db_query($sql);
// 			${totalCount.$t} = sizeof($tempArr);
			${totalCount.$t}++;
		}
		else
		{
			$face_full_quota = 1;
		}
	}
}
// $totalCount1 = sizeof($EventGroup1);
// $totalCount2 = sizeof($EventGroup2);

####################################################

# Insert Unrestrict Quota Event Enrolment Record(s)
$de = "";
$values = "";
for($i=0; $i<sizeof($UQ_EventGroup); $i++)
{
	$egid = $UQ_EventGroup[$i];
	
	# Check if Event is full or not
    $cur_enroled = $lsports->returnEventEnroledNo($egid, $StudentID);
    $eventQuota = $lsports->returnEventQuota($egid);
    if($cur_enroled < $eventQuota || !$eventQuota)	
	{
//		$values .= $de."(".$StudentID.", ".$egid.", now(), $UserID)";
//		$de = ",";
		$values = "(".$StudentID.", ".$egid.", now(), $UserID)";
		
		$fields = "(StudentID, EventGroupID, DateModified, InputBy)";
		$sql = "INSERT INTO SPORTS_STUDENT_ENROL_EVENT $fields VALUES $values";
		$lsports->db_db_query($sql);
	}
	else
	{
		$face_full_quota = 1;
	}
}

####################################################

# Check if record is existed or not
$sql = "SELECT COUNT(*) FROM SPORTS_STUDENT_ENROL_INFO WHERE StudentID = '$StudentID'";
$exist_info = $lsports->returnVector($sql);
if($exist_info[0] == 0) 
{
	# Insert new records into SPORTS_STUDENT_ENROL_INFO
	$fields1 = "(StudentID, TrackEnrolCount, FieldEnrolCount, DateModified)";
	$values1 = "(".$StudentID.", ".$trackCount.", ".$fieldCount.", now())";
	$sql = "INSERT INTO SPORTS_STUDENT_ENROL_INFO $fields1 VALUES $values1";
	$lsports->db_db_query($sql);
}
else
{
	// #Retrieve Amount of Events that the Student Have Enroled
	// $sql = "SELECT TrackEnrolCount, FieldEnrolCount FROM SPORTS_STUDENT_ENROL_INFO WHERE StudentID = '$StudentID'";
	// $temp = $lsports->returnArray($sql, 2);
	// if(sizeof($temp) != 0)
	// {
		// $trackCount = $temp[0][0];
		// $fieldCount = $temp[0][1];
	// }
	// else
	// {
		// $trackCount = 0;
		// $fieldCount = 0;
	// }

	# Update existing records in SPORTS_STUDENT_ENROL_INFO
	$sql = "UPDATE SPORTS_STUDENT_ENROL_INFO SET TrackEnrolCount = '$trackCount', FieldEnrolCount = '$fieldCount', DateModified = now() WHERE StudentID = '$StudentID'";
	$lsports->db_db_query($sql);
}

# Clear old records in SPORTS_LANE_ARRANGEMENT
if(sizeof($allEvents) != 0)
{
	$enroledID = (is_array($allEvents)) ? implode($allEvents, ",") : $allEvents;
	$sql = "DELETE FROM SPORTS_LANE_ARRANGEMENT WHERE StudentID = '$StudentID' AND EventGroupID NOT IN ($enroledID) $cond2";
}
else
{
	$sql = "DELETE FROM SPORTS_LANE_ARRANGEMENT WHERE StudentID = '$StudentID' $cond2";
}
$lsports->db_db_query($sql);

intranet_closedb();

if($face_full_quota)
	$xmsg = "UpdatePartiallySuccess";
else
	$xmsg = "UpdateSuccess";
header ("Location: detail.php?StudentID=$StudentID&xmsg=$xmsg$key");
?>