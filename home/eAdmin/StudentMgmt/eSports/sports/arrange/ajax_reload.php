<?php
# using:

#################################################
#
#	Date:	2020-03-20 Philips  [2020-0117-1205-31207]
#			Enlarge Student selection box
#
#   Date:   2019-04-10 Bill     [2019-0301-1144-56289]
#           Create file
#
#################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();
$lclass = new libclass();

# Get Teaching Class
if(!$lsports->isAdminOrHelper($UserID)) {
    $yearClassIDArr = $lsports->teachingYearClassList();
    if(empty($yearClassIDArr)) {
        $yearClassIDArr[] = -1;
    }
}

$row_id = $event_group_id.'_'.$group_row;
$row_name = '['.$event_group_id.']['.$group_row.']';

# 2020-03-20 (Philips) - Default student selection box size
$selectSize = 'size="16"';

# Class Selection
$select_class_tag = "name='ClassID".$row_name."' id='ClassID_".$row_id."' class='class_select' onchange='get_class_stud(\"$row_id\", \"$row_name\", \"\")'";
$select_class = $lclass->getSelectClassID($select_class_tag, '', 1, '', '', $yearClassIDArr);

# Student Selection
$student_select = "<select class='stud_select' name='Student_Select".$row_name."' id='Student_Select_".$row_id."' multiple='yes' $selectSize></select>";
$push = "<button type='button' onclick='stud_push(\"$row_id\")'>>></button>";
$pop = "<button type='button' onclick='stud_pop(\"$row_id\")'><<</button>";
$student_selected = "<select class='stud_select' name='Student_Selected".$row_name."[]' id='Student_Selected_".$row_id."' multiple='yes' $selectSize></select>";

$student_select_div_1 = $student_select.
                        "<div class='push_pop' style='display: inline'>".$push."</div>";
$student_select_div_2 = "<div class='push_pop' style='display: inline'>".$pop."</div>".
                        $student_selected.
                        "<div style='display:none;' class='warnMsgDiv' id='warnDiv_".$row_id."'>
                            <span class='tabletextrequire'>".$Lang['eSports']['Sports']['ClassRelay']['EnrolLimitAlert']."!</span>
                        </div>
                        <div style='display:none;' class='warnMsgDiv' id='warnClassDiv_".$row_id."'>
                            <span class='tabletextrequire'>".$Lang['eSports']['Sports']['ClassRelay']['ClassIsAlreadySelected']."!</span>
                        </div>
                        <div style='display:none;' class='warnMsgDiv' id='warnStuDiv_".$row_id."'>
                            <span class='tabletextrequire'>".$Lang['eSports']['Sports']['ClassRelay']['StudentAlreadyInAnotherLine']."!</span>
                        </div>";

$form_content .= "<tr id='group_row_".$row_id."'>";
    $form_content .= "<td valign='top' nowrap='nowrap' class='tableContent$css'>&nbsp;</td>";
    $form_content .= "<td valign='top' class='tableContent$css'>".$select_class."<br/></td>";
    $form_content .= "<td class='tableContent$css'>".$student_select_div_1."</td>";
    $form_content .= "<td class='tableContent$css'>".$student_select_div_2."</td>";
    $form_content .= "<td class='tableContent$css' valign='top' style='text-align: right'>";
        $form_content .= "<span class='table_row_tool row_content_tool'>";
        $form_content .= "<a onclick='delete_class_group(this, \"$event_group_id\", \"$group_row\")' title='delete' class='delete' href='javascript:void(0);'></a>";
        $form_content .= "</span>";
    $form_content.= "</td>";
$form_content .= "</tr>";

echo $form_content;

intranet_closedb();
?>