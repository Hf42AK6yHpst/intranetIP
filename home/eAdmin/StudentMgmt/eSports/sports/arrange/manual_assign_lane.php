<?php
# using: 

############################################
#
#	Date:	2016-03-16	Bill	[2016-0301-1022-11066]
#			skip line checking for field event
#
############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# Get House info
$lsports = new libsports();
$lsports->authSportsSystem();

$lsports->Start_Trans();
$success = array();

// [2016-0301-1022-11066]
$skipLaneChecking = $eventType==2;

foreach($ManualAssignStudent as $thisStudentID)
{
	$thisGroup = $group[$thisStudentID];
	$thisLane = $lane[$thisStudentID];
	
	// [2016-0301-1022-11066]
	// if(trim($thisGroup)=='' || trim($thisLane)=='')
	if(trim($thisGroup)=='' || (!$skipLaneChecking && trim($thisLane)==''))
		continue;

	// $success[] = $lsports->Assign_Student_To_EventGroup($thisStudentID,$eventGroupID,$thisGroup,$thisLane,$roundType);
	$success[] = $lsports->Assign_Student_To_EventGroup($thisStudentID,$eventGroupID,$thisGroup,$thisLane,$roundType,$skipLaneChecking);
}

if(in_array(false,$success)|| count($success)==0)
{
	$lsports->RollBack_Trans();
	$msg = "update_failed";
}
else
{
	$lsports->Commit_Trans();
	$msg = "update";
}
intranet_closedb();
header ("Location:tf_lane_arrange_detail.php?eventGroupID=$eventGroupID&xmsg=$msg");

?>