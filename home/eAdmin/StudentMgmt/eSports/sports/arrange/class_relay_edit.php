<?php
# using:

#################################################
#
#   Date:   2019-04-11  Bill    [2019-0301-1144-56289]
#           Redirect to cust Group Relay page    ($sys_custom['eSports']['KaoYipRelaySettings'])
#
#   Date:   2018-10-29 Bill  [2018-0628-1634-01096]
#           support Class Group filtering (1st round - not selected by other heats / Final - top 10 Class Group)
#
#   Date:   2018-09-26 Bill  [2018-0628-1634-01096]
#           support Class Group selection
#
#   Date:   2018-09-04 Bill  [2018-0628-1634-01096]
#           Allow Kao Yip Class Teacher to input relay ($sys_custom['eSports']['KaoYipRelaySettings'])  [removed]
#
#   Date:   2018-08-10 Philips  [2018-0628-1634-01096]
#           support adding class students ($sys_custom['eSports']['KaoYipRelaySettings'])
#
#################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "PageArrangement_Schedule";

$lsports = new libsports();
$lclass = new libclass();

// [2018-0628-1634-01096] Kao Yip Class Teacher [removed]
// if($lsports->isYearClassTeacher()) {
//     // do nothing
// } else {
$lsports->authSportsSystem();

$eventName = str_replace(",", " ", $eventName);

// [2019-0301-1144-56289] redirect to cust page
if($sys_custom['eSports']['KaoYipRelaySettings'])
{
    header("Location: class_relay_group_edit.php?eventGroupID=".$eventGroupID."&groupName=".$groupName."&eventName=".$eventName);
    exit;
}

$numberOfLane = $lsports->numberOfLanes;

// # get the selection list of houses
// $houses = $lsports->retrieveHouseSelectionInfo();

# retrieve the class relay lane arrangement info
$class_arrange = $lsports->retrieveClassRelayLaneArrangement($eventGroupID);

/* 
// [2018-0628-1634-01096]
if($sys_custom['eSports']['KaoYipRelaySettings'])
{
    # Get Age Group
    $ageGroupID = $lsports->retrieveAgeGroupID($eventGroupID);
    $ageGroupID = $ageGroupID[0]['GroupID'];
    
    # Get Event
    $eventInfo = $lsports->retrieveEventGroupDetail($eventGroupID);
    $eventInfo = $lsports->retrieveTrackFieldDetail($eventInfo['EventID']);
    $relayType = $eventInfo['RelayEventType'];
    $relayRound = $eventInfo['RelayRoundType'];
    
//     $eventNameEn = $eventInfo['EnglishName'];
//     $eventNameEn = str_replace(' x ', 'x', $eventNameEn);
    
//     # Get Event Relay Type
//     $relayEventTypeAry = $lsports->retrieveClassRelayType();
//     foreach($relayEventTypeAry as $thisRelayType) {
//         if(stripos($eventNameEn, $thisRelayType['RelayType']) !== false) {
//             $relayType = $thisRelayType['RelayType'];
//             break;
//         }
//     }
}
 */

# form content
$form_content = "";
for($i=1; $i<=$numberOfLane; $i++) 
{
	# Retrieve the current lane position
	$select_class = "";
	$select_class_group = "";
	for($j=0; $j<sizeof($class_arrange); $j++)
	{
		list($class_id, $order, $class_group_id) = $class_arrange[$j];
		if($order == $i)
		{
			$select_class = $class_id;
			$select_class_group = $class_group_id;
			break;
		}
	}
	//$houses_select = getSelectByArray($houses, "name='houseID$i'", $select_house, 0, 0);
	$class_select = $lclass->getSelectClassID("name='ClassID$i'", $select_class, 1);
	
	/* 
	$student_select_div = '';
	if($sys_custom['eSports']['KaoYipRelaySettings'])
	{
	    $class_group_select = $lsports->getClassRelayGroupSelection($relayType, $ageGroupID, $selectedRelayGroupID=$select_class_group, $selectID='ClassGroupID'.$i, $onchange="getStudent($i)", $relayRound, $eventGroupID);
	    $class_select = $class_group_select;
	    
    	$student_selected = "<select class='stud_select' name='Student_Selected".$i."[]' id='Student_Selected$i' multiple='yes' disabled></select>
                                <div style='display:none;' class='warnMsgDiv' id='warnStuDiv$i'>
                                    <span class='tabletextrequire'>".$Lang['eSports']['Sports']['ClassRelay']['ClassGroupAlreadyInAnotherLine']."!</span>
                                </div>";
    	$student_select_div = $student_selected;
	}
	 */
	
	$css = $i % 2? "" : "2";
	$form_content .= "<tr>";
        $form_content .= "<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>".$i_Sports_The.$i.$i_Sports_Line."</span></td>";
	    $form_content .= "<td width='70%' class='tableContent$css'>".$class_select.$student_select_div."</td>";
	$form_content .= "</tr>";
}

$house_relay_name = $lsports->retrieveEventTypeNameByID(3);
$class_relay_name = $lsports->retrieveEventTypeNameByID(4);

### Title ###
// if($lsports->isYearClassTeacher()) {
//     $TAGS_OBJ[] = array($class_relay_name);
// } else {
$TAGS_OBJ[] = array($i_Sports_menu_Settings_CommonTrackFieldEvent,"schedule.php", 0);
$TAGS_OBJ[] = array($house_relay_name,"schedule_relay.php", 0);
$TAGS_OBJ[] = array($class_relay_name,"schedule_class_relay.php", 1);
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

$PAGE_NAVIGATION[] = array($button_edit.($intranet_session_language=="en"?" ":"").$class_relay_name);

$linterface->LAYOUT_START();
?>

<br />   
<form name="form1" action="class_relay_edit_update.php" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td>
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Item?></span></td>
					<td class="tabletext"><?=stripslashes($eventName)?></td>
				</tr>
                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_field_Group?></span></td>
					<td class="tabletext"><?=stripslashes($groupName)?></td>
				</tr>
                <?=$form_content?>
    			</table>
    				</td>
                </tr>
                </table>
	</td>
</tr>
<tr>
	<td colspan="2">
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
					<td align="center">
						<?= $linterface->GET_ACTION_BTN($button_submit, "button", "handle_submit()", "submit2") ?>
                        <?= $linterface->GET_ACTION_BTN($button_reset, "reset", "", "reset2") ?>
                        <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='schedule_class_relay.php'", "cancel2") ?>
					</td>
				</tr>
                </table>
	</td>
</tr>
</table>
<br />

<input type="hidden" name="eventGroupID" value="<?=$eventGroupID?>">
</form>

<?php if(!$sys_custom['eSports']['KaoYipRelaySettings']) { ?>
<script type="text/javascript">
    function handle_submit(){
		document.form1.submit();
    }
</script>
<?php } else { ?>
<script type="text/javascript">
    function handle_submit(){
    	$('.warnMsgDiv').hide();
    	
//     	var alert = [];
    	var valid = true;
    	var selectedGroupArr = [];
    	for(var i=1; i<=<?=$lsports->numberOfLanes?>; i++) {
    		var classGroupID = $('select[name="ClassGroupID' + i + '"]').val();
    		if(classGroupID != '') {
				if(selectedGroupArr.indexOf(classGroupID) != -1) {
    				valid = false;
    				$('#warnStuDiv' + i).show();
				} else {
					selectedGroupArr.push(classGroupID);
				}
    		}
    	}
    	
    	if(valid) {
    		document.form1.submit();
    	} else {
    		return false;
    	}
    }
    
    function getStudent(num) {
    	var ele = $('select[name="ClassGroupID' + num + '"]');
    	var studForm = $('select#Student_Selected' + num);
    	
    	if(ele.val() != '') {
        	$.ajax({
        		url: 'ajax_get_group_student.php',
        		method: 'post',
        		data: {
        			"relayType": "<?=$relayType?>",
        			"ageGroupID": "<?=$ageGroupID?>",
        			"classGroupID": ele.val()
        		},
        		success: function(res){
        			studForm.empty().append(res);
        		}
        	});
    	} else {
    		studForm.empty();
    	}
    }
    
<?php
    $script_content = "";
    for($i=1; $i<=$numberOfLane; $i++) {
        $script_content .= "getStudent($i);";
    }
    echo $script_content;
?>
</script>

<style>
    body{
        min-width: 1080px;
    }
    td.tableContent select, td.tableContent2 select {
        vertical-align: top;
    }
    select.stud_select {
        width: 150px;
    }
</style>
<?php } ?>

<?
intranet_closedb();

print $linterface->FOCUS_ON_LOAD("form1.ClassGroupID1");
$linterface->LAYOUT_STOP();
?>