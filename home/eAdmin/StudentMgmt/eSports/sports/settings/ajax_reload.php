<?php
# using:

#################################################
#
#   Date:   2019-04-15 Bill     [2019-0301-1144-56289]
#           Create file
#
#################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();

$db_field = $intranet_session_language=="en"? "EnglishName" : "ChineseName";
$sql = "SELECT EventID, $db_field, EventType FROM SPORTS_EVENT WHERE EventType = '$event_type' ORDER BY EventType, DisplayOrder";
$events = $lsports->returnArray($sql,3);
$select_events = getSelectByArray($events, "id='EventID' name='EventID' onChange='check_event_type();'", $event_id, 0, 1);

echo $select_events;

intranet_closedb();
?>