<?php
# Using: 

#################################################
#	Date:	2019-09-19 Philips	[]
#			$sys_custom['eSports']['SportDay_EventGroupSchedule_MKSS'] For event group type 3, 4, added FinalRoundDay 
#
#   Date:   2019-04-09 Bill     [2019-0301-1144-56289]
#           Kao Yip group relay settings update     ($sys_custom['eSports']['KaoYipRelaySettings'])
#
#   Date:   2018-10-11  Bill    [2018-1010-1718-03170]
#           fixed: cannot update Person Per Group if set Ext Info - 1st time [$is_setup != 1]
#
#	Date:	2012-06-08	YatWoon
#			add "Event Quota"
#
#################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();
$lsports->authSportsSystem();

$EventGroupID = $_POST['EventGroupID'];
$EventTypeID = $_POST['EventTypeID'];

$AgeGroupID = $_POST['AgeGroupID'];

$onlineEnrol = $_POST['onlineEnrol'];
$Quota = $_POST['Quota'];
$countHouse = $_POST['countHouse'];
$countClass = $_POST['countClass'];
$countIndividual = $_POST['countIndividual'];

$scoreStandard = $_POST['scoreStandard'];
$RecordHouseID = $_POST['house'];
$RecordHolderName = $_POST['record_holder'];
$RecordYear = $_POST['record_year'];

# For Track and Relay Event Only
$RecordMin = $_POST['record_min'];
$RecordSec = $_POST['record_sec'];
$RecordMs = $_POST['record_ms'];
$StandardMin = $_POST['standard_min'];
$StandardSec = $_POST['standard_sec'];
$StandardMs = $_POST['standard_ms'];

# For Field Event Only
$RecordMetre = $_POST['record_metre'];
$StandardMetre = $_POST['standard_metre'];

$FirstRoundType = $_POST['FirstRoundType'];     # 0- Number of Personal Per Group   1 - Number of Group
$personNum1 = $_POST['personNum1'];
$groupNum1 = $_POST['groupNum1'];
$FirstRoundRandom = $_POST['FirstRoundRandom']; # 1- Random     0/NULL - Non Random
$FirstRoundDay = $_POST['race_day1'];
$SecondRoundReq = $_POST['SecondRoundReq'];     # 1 - Second Round Required     0 - Does Not Required
$personNum2 = $_POST['personNum2'];
$groupNum2 = $_POST['groupNum2'];
$SecondRoundDay = $_POST['race_day2'];
$FinalRoundReq = $_POST['FinalRoundReq'];       # 1 - Final Round Required      0 - Does Not Required
$personNum3 = $_POST['personNum3'];
$FinalRoundDay = $_POST['race_day3'];
$FirstRoundGroupCount = ($FirstRoundType == 0) ? $personNum1 : $groupNum1;

$eventQuota = $_POST['eventQuota'] ? $_POST['eventQuota'] : 0;

$sql = "UPDATE SPORTS_EVENTGROUP SET IsOnlineEnrol = '$onlineEnrol',
                                CountPersonalQuota = '$Quota',
                                CountHouseScore = '$countHouse',
                                CountClassScore = '$countClass',
								CountIndividualScore = '$countIndividual',
								ScoreStandardID = '$scoreStandard',
								EventCode = '$event_code',
								EventQuota = '$eventQuota',
                                DateModified = now()
                            WHERE EventGroupID = $EventGroupID";
$lsports->db_db_query($sql);

# Check whether the Ext Info of this Event Enrolment has been existed
$is_setup = $lsports->checkExtInfoExist($EventTypeID, $EventGroupID);

if($EventTypeID == '1')		# Track Event
{
	if($is_setup == 1)
	{
		$valuefields = "RecordHolderName = '$RecordHolderName', ";
		$valuefields .= "RecordMin = '$RecordMin', ";
		$valuefields .= "RecordSec = '$RecordSec', ";
		$valuefields .= "RecordMs = '$RecordMs', ";
		$valuefields .= "RecordYear = '$RecordYear', ";
		$valuefields .= "RecordHouseID = '$RecordHouseID', ";
		$valuefields .= "StandardMin = '$StandardMin', ";
		$valuefields .= "StandardSec = '$StandardSec', ";
		$valuefields .= "StandardMs = '$StandardMs', ";
		$valuefields .= "FirstRoundType = '$FirstRoundType', ";
		$valuefields .= "FirstRoundGroupCount = '$FirstRoundGroupCount', ";
		$valuefields .= "FirstRoundRandom = '$FirstRoundRandom', ";
		$valuefields .= "FirstRoundDay = '$FirstRoundDay', ";
		$valuefields .= "SecondRoundReq = '$SecondRoundReq', ";

		if($SecondRoundReq == 1)
		{
			$valuefields .= "SecondRoundLanes = '$personNum2', ";
			$valuefields .= "SecondRoundGroups = '$groupNum2', ";
			$valuefields .= "SecondRoundDay = '$SecondRoundDay', ";
		}
		else
		{
			$valuefields .= "SecondRoundLanes = ' ', ";
			$valuefields .= "SecondRoundGroups = ' ', ";
			$valuefields .= "SecondRoundDay = ' ', ";
		}
		if($FinalRoundReq == 1)
		{
			$valuefields .= "FinalRoundNum = '$personNum3', ";
			$valuefields .= "FinalRoundDay = '$FinalRoundDay', ";
		}
		else
		{
			$valuefields .= "FinalRoundNum = ' ', ";
			$valuefields .= "FinalRoundDay = ' ', ";
		}
		$valuefields .= "FinalRoundReq = '$FinalRoundReq'";

		$sql = "UPDATE SPORTS_EVENTGROUP_EXT_TRACK SET $valuefields WHERE EventGroupID = '$EventGroupID'";
		$lsports->db_db_query($sql);
	}
	else
	{
		$fields = "(EventGroupID, RecordHolderName, RecordMin, RecordSec, RecordMs, RecordYear, RecordHouseID, StandardMin, StandardSec, StandardMs, FirstRoundType, FirstRoundGroupCount, FirstRoundRandom, FirstRoundDay, SecondRoundReq, SecondRoundLanes, SecondRoundGroups, SecondRoundDay, FinalRoundNum, FinalRoundDay, FinalRoundReq)";

		$values = "('$EventGroupID', ";
		$values .= "'$RecordHolderName', ";
		$values .= "'$RecordMin', ";
		$values .= "'$RecordSec', ";
		$values .= "'$RecordMs', ";
		$values .= "'$RecordYear', ";
		$values .= "'$RecordHouseID', ";
		$values .= "'$StandardMin', ";
		$values .= "'$StandardSec', ";
		$values .= "'$StandardMs', ";
		$values .= "'$FirstRoundType', ";
		$values .= "'$FirstRoundGroupCount', ";
		$values .= "'$FirstRoundRandom', ";
		$values .= "'$FirstRoundDay', ";
		$values .= "'$SecondRoundReq', ";
		if($SecondRoundReq == 1)
		{
		    // [2018-1010-1718-03170]
			//$values .= "'$SecondRoundLanes', ";
		    $values .= "'$personNum2', ";
			$values .= "'$SecondRoundGroups', ";
			$values .= "'$SecondRoundDay', ";
		}
		else
		{
			$values .= "' ', ";
			$values .= "' ', ";
			$values .= "' ', ";
		}
		if($FinalRoundReq == 1)
		{
		    // [2018-1010-1718-03170]
			//$values .= "'$FinalRoundNum', ";
		    $values .= "'$personNum3', ";
			$values .= "'$FinalRoundDay', ";
		}
		else
		{
			$values .= "' ', ";
			$values .= "' ', ";
		}
		$values .= "'$FinalRoundReq')";
		
		$sql = "INSERT INTO SPORTS_EVENTGROUP_EXT_TRACK $fields VALUES $values";
		$lsports->db_db_query($sql);
	}
}
else if($EventTypeID == '2')		# Field Event
{
	if($is_setup == 1)
	{
		$valuefields = "RecordHolderName = '$RecordHolderName', ";
		$valuefields .= "RecordMetre = '$RecordMetre', ";
		$valuefields .= "RecordYear = '$RecordYear', ";
		$valuefields .= "RecordHouseID = '$RecordHouseID', ";
		$valuefields .= "StandardMetre = '$StandardMetre', ";
		$valuefields .= "FirstRoundType = '$FirstRoundType', ";
		$valuefields .= "FirstRoundGroupCount = '$FirstRoundGroupCount', ";
		$valuefields .= "FirstRoundRandom = '$FirstRoundRandom', ";
		$valuefields .= "FirstRoundDay = '$FirstRoundDay', ";

		if($FinalRoundReq == 1)
		{
			$valuefields .= "FinalRoundNum = '$personNum3', ";
			$valuefields .= "FinalRoundDay = '$FinalRoundDay', ";
		}
		else
		{
			$valuefields .= "FinalRoundNum = ' ', ";
			$valuefields .= "FinalRoundDay = ' ', ";
		}
		$valuefields .= "FinalRoundReq = '$FinalRoundReq'";
		
		$sql = "UPDATE SPORTS_EVENTGROUP_EXT_FIELD SET $valuefields WHERE EventGroupID = '$EventGroupID'";
		$lsports->db_db_query($sql);
	}
	else
	{
		$fields = "(EventGroupID, RecordHolderName, RecordMetre, RecordYear, RecordHouseID, StandardMetre, FirstRoundType, FirstRoundGroupCount, FirstRoundRandom, FirstRoundDay, FinalRoundNum, FinalRoundDay, FinalRoundReq)";

		$values = "('$EventGroupID', ";
		$values .= "'$RecordHolderName', ";
		$values .= "'$RecordMetre', ";
		$values .= "'$RecordYear', ";
		$values .= "'$RecordHouseID', ";
		$values .= "'$StandardMetre', ";
		$values .= "'$FirstRoundType', ";
		$values .= "'$FirstRoundGroupCount', ";
		$values .= "'$FirstRoundRandom', ";
		$values .= "'$FirstRoundDay', ";

		if($FinalRoundReq == 1)
		{
		    // [2018-1010-1718-03170]
		    //$values .= "'$FinalRoundNum', ";
		    $values .= "'$personNum3', ";
			$values .= "'$FinalRoundDay', ";
		}
		else
		{
			$values .= "' ', ";
			$values .= "' ', ";
		}
		$values .= "'$FinalRoundReq')";
		
		$sql = "INSERT INTO SPORTS_EVENTGROUP_EXT_FIELD $fields VALUES $values";
		$lsports->db_db_query($sql);
	}
}
else if($EventTypeID == '3' || $EventTypeID == '4')		# Relay Event
{
	if($is_setup == 1)
	{
		$valuefields = "RecordHolderName = '$RecordHolderName', ";
		$valuefields .= "RecordMin = '$RecordMin', ";
		$valuefields .= "RecordSec = '$RecordSec', ";
		$valuefields .= "RecordMs = '$RecordMs', ";
		$valuefields .= "RecordYear = '$RecordYear', ";
		$valuefields .= "RecordHouseID = '$RecordHouseID', ";
		$valuefields .= "StandardMin = '$StandardMin', ";
		$valuefields .= "StandardSec = '$StandardSec', ";
		$valuefields .= "StandardMs = '$StandardMs'";
		
		// [2019-0301-1144-56289]
		if($sys_custom['eSports']['KaoYipRelaySettings'] && $EventTypeID == '4')
		{
		    if($FinalRoundReq == 1) {
		        $valuefields .= ", FinalRoundNum = '$personNum3'";
		    } else {
		        $valuefields .= ", FinalRoundNum = ' '";
		    }
		    $valuefields .= ", FinalRoundReq = '$FinalRoundReq'";
		}
		
		if($sys_custom['eSports']['SportDay_EventGroupSchedule_MKSS']){
			$valuefields .= ", FinalRoundDay = '$FinalRoundDay'";
		}
        
		$sql = "UPDATE SPORTS_EVENTGROUP_EXT_RELAY SET $valuefields WHERE EventGroupID = '$EventGroupID'";
		$lsports->db_db_query($sql);
	}
	else
	{
		$fields = "(EventGroupID, RecordHolderName, RecordMin, RecordSec, RecordMs, RecordYear, RecordHouseID, StandardMin, StandardSec, StandardMs";

		$values = "('$EventGroupID', ";
		$values .= "'$RecordHolderName', ";
		$values .= "'$RecordMin', ";
		$values .= "'$RecordSec', ";
		$values .= "'$RecordMs', ";
		$values .= "'$RecordYear', ";
		$values .= "'$RecordHouseID', ";
		$values .= "'$StandardMin', ";
		$values .= "'$StandardSec', ";
		$values .= "'$StandardMs'";
		
		// [2019-0301-1144-56289]
		if($sys_custom['eSports']['KaoYipRelaySettings'] && $EventTypeID == '4')
		{
		    $fields .= ", FinalRoundNum, FinalRoundReq ";
		    
		    if($FinalRoundReq == 1) {
		        $values .= ", '$personNum3'";
		    } else {
		        $values .= ", ' '";
		    }
		    $values .= ", '$FinalRoundReq'";
		}
		
		$fields .= ")";
        $values .= ")";
		
		$sql = "INSERT INTO SPORTS_EVENTGROUP_EXT_RELAY $fields VALUES $values";
		$lsports->db_db_query($sql);
	}
}

intranet_closedb();
header("Location: tf_event.php?eventID=".$eventID."&AgeGroupID=".$AgeGroupID."&xmsg=update");
?>