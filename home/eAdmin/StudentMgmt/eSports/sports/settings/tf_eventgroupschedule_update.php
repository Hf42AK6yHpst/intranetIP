<?php
# Using:

#################################################
#
#   Date:   2019-09-19 Philips     [2019-0301-1144-56289]
#               ($sys_custom['eSports']['KaoYipRelaySettings'])
#
#################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();
$lsports->authSportsSystem();

$Action = $_POST['Action'];
if($Action == 'init'){
	$result = $lsports->initEventGroupSchedule();
} else if($Action == 'edit'){
	$scheduleCodeAry = $_POST['EventScheduleCode'];
	$updateAry = array();
	foreach($scheduleCodeAry as $scheduleID => $sc){
		$eventGroupSchedule = array();
		$eventGroupSchedule['ScheduleID'] = $scheduleID;
		$eventGroupSchedule['EventScheduleCode'] = $sc;
		$eventGroupSchedule['EventTime'] = $_POST['EventTime_'.$scheduleID.'_hour'].':'.$_POST['EventTime_'.$scheduleID.'_min'];
		
		$updateAry[] = $lsports->updateEventGroupSchedule($eventGroupSchedule);
	}
}

intranet_closedb();
header("Location: tf_eventgroupschedule.php?xmsg=update");
?>