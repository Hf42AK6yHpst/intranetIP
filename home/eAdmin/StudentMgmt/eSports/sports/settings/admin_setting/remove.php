<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");

if(!$plugin['Sports'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSportsAdmin"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

intranet_opendb();

$lsports = new libsports();

if(!empty($AdminID))
{
	$lsports->removeAdminUser($AdminID);
	$msg = "delete";
}
else
	$msg = "delete_failed";

intranet_closedb();
header("Location: index.php?msg=$msg");
?>