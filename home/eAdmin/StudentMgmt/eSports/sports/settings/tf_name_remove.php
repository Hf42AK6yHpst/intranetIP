<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();
$lsports->authSportsSystem();

$list = implode(",",$EventID);

$sql = "DELETE FROM SPORTS_EVENT WHERE EventID IN ($list)";
$lsports->db_db_query($sql);

intranet_closedb();
header("Location: tf_name.php?xmsg=delete");
?>