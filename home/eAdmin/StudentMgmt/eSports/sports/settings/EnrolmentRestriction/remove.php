<?php
#####################################################
#####################################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if(!$plugin['Sports'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSportsAdmin"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libsports.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();
$student_str = implode(",",$StudentID);
$sql = "delete from SPORTS_ENROL_RESTRICTION_LIST where UserID in (". $student_str.")";
$lsports->db_db_query($sql);

intranet_closedb();
header("Location: index.php?xmsg=DeleteSuccess");
?>