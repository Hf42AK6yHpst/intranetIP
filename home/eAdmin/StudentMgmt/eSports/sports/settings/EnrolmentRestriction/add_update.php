<?php
#####################################################
#####################################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
// include_once($PATH_WRT_ROOT."includes/libclass.php");

if(!$plugin['Sports'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSportsAdmin"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libsports.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();
// $lclass = new libclass();

# student list
$permitted[] = 2;
$student = returnSelectedStudentList($target, $permitted);
if(empty($student))
{
	header("Location: index.php?xmsg=AddUnsuccess");
	exit;
}

$student_str = implode(",",$student);
$reason = intranet_htmlspecialchars(trim($reason));
# remove old record
$sql = "delete from SPORTS_ENROL_RESTRICTION_LIST where UserID in (". $student_str.")";
$lsports->db_db_query($sql);

$val = array();
# insert new record
foreach($student as $k=>$sid)
	$val[] = "($sid, '$reason', now(), $UserID)";
$sql = "insert into SPORTS_ENROL_RESTRICTION_LIST (UserID, Reason, DateInput, InputBy) values " . implode(",",$val);
$lsports->db_db_query($sql);

intranet_closedb();
header("Location: index.php?xmsg=AddSuccess");
?>