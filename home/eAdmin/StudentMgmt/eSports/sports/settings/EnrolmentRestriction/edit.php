<?php
# using: yat

#####################################################
#####################################################
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if(!$plugin['Sports'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSportsAdmin"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

$CurrentPage = "PageAdminSettings";

$linterface = new interface_html();
$lsports = new libsports();

$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit']);

# Left menu 
$TAGS_OBJ[] = array($Lang['eSports']['EnrolmentRestriction']);
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
$sid = $StudentID[0];
$name_field = getNameFieldByLang("b.");
$sql = "
	SELECT 
		$name_field as NameField, 
		Reason
	FROM 
		SPORTS_ENROL_RESTRICTION_LIST as a
		LEFT JOIN INTRANET_USER as b ON a.UserID = b.UserID
	WHERE 
		a.UserID=$sid
";
$tmp = $lsports->returnArray($sql);
list($NameField,$this_reason) = $tmp[0];

?>

<br />   
<form name="form1" method="get" action="edit_update.php" onSubmit="return checkform();">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$i_UserStudentName?></td>
			<td class='tabletext'><?=$NameField?></td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$i_Discipline_Reason?></td>
			<td class='tabletext'>
			<input type="text" name="reason" size="50" maxlength="100" value="<?=$this_reason?>">
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">        
        <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
        <tr>
        	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        </tr>
        <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit") ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back()","cancelbtn") ?>
			</td>
		</tr>
        </table>                                
	</td>
</tr>

</table>                        
<br />

<input type="hidden" name="sid" value="<?=$sid?>">
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>