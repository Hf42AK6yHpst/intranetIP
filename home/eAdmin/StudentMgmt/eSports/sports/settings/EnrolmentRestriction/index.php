<?php
# using: yat

#####################################################################
#	Customization for Sana Rosa [Case#2013-0424-1211-29096]
#####################################################################
		
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();
if(!$plugin['Sports'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSportsAdmin"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_settings_admin_setting_page_size", "numPerPage");
$arrCookies[] = array("ck_settings_admin_setting_page_number", "pageNo");
$arrCookies[] = array("ck_settings_admin_setting_page_order", "order");
$arrCookies[] = array("ck_settings_admin_setting_page_field", "field");

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
	$ck_settings_admin_setting_page_size = '';
}
else 
	updateGetCookies($arrCookies);

include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");


$linterface 	= new interface_html();
$CurrentPage	= "PageEnrolmentRestriction";
$lsports = new libsports();





$name_field = getNameFieldByLang("u.");
$sql = "
	SELECT 
		u.ClassName,
		u.ClassNumber,
		$name_field as student_name,
		r.Reason,
		r.DateInput,
		CONCAT('<input type=\"checkbox\" name=\"StudentID[]\" value=\"', r.UserID ,'\">')
	FROM 
		SPORTS_ENROL_RESTRICTION_LIST as r
		left join INTRANET_USER as u ON u.UserID = r.UserID
";


# TABLE INFO
if($field=="") $field = 0;
if($order=="") $order =1;

$li = new libdbtable2007($field, $order, $pageNo);
$li->page_size = $numPerPage?$numPerPage:20;
$li->field_array = array("ClassName", "ClassNumber", "student_name", "Reason", "DateInput");
$li->fieldorder2 = ", ClassNumber asc";
$li->sql = $sql;
$li->no_col = 7;
$li->IsColOff = "IP25_table";

// TABLE COLUMN
$li->column_list .= "<th class='tabletop' width='1'>#</th>\n";
$li->column_list .= "<th class='tabletop' width='5%'>". $li->column_IP25(0,$i_ClassName) ."</th>\n";
$li->column_list .= "<th class='tabletop' width='5%'>". $i_ClassNumber ."</th>\n";
$li->column_list .= "<th class='tabletop' width='20%'>". $li->column_IP25(2,$i_UserStudentName) ."</th>\n";
$li->column_list .= "<th class='tabletop' width='50%'>". $li->column_IP25(3,$i_Discipline_Reason) ."</th>\n";
$li->column_list .= "<th class='tabletop' width='20%'>". $li->column_IP25(4,$Lang['General']['RecordDate']) ."</th>\n";
$li->column_list .= "<th class='tabletop' width='1'>".$li->check("StudentID[]")."</th>\n";

### Button
$AddBtn = $linterface->GET_LNK_ADD("add.php",$button_new,"","","",0);
$delBtn = $linterface->GET_LNK_REMOVE("javascript:checkRemove(document.form1,'StudentID[]','remove.php')");
$editBtn = $linterface->GET_LNK_EDIT("javascript:checkEdit(document.form1,'StudentID[]','edit.php')","","","","",0);
//$editBtn 	= "<a href=\"javascript:checkEdit(document.form1,'ID[]','agegroup_edit.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";

### Title ###
$TAGS_OBJ[] = array($Lang['eSports']['EnrolmentRestriction']);
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>

<form name="form1" method="get">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><?=$AddBtn?></td>
				</tr>
				<tr>
					<td align="right" valign="bottom" height="28">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
								<td>
									<div class="common_table_tool">
								        <?=$editBtn?>
								        <?=$delBtn?>
									</div>		
								</td>		
							</tr>
						</table>
					</td>
				</tr>
                                <tr>
                                	<td colspan="2">
						<?php
							echo $li->display($li->no_col);
						?>
					</td>
				</tr>
				</table>
			</td>
		</tr>
                </table>
 

<input type="hidden" name="pageNo" value="<?=$li->pageNo?>" />
<input type="hidden" name="order" value="<?=$li->order?>" />
<input type="hidden" name="field" value="<?=$li->field?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>
 
<?

intranet_closedb();
$linterface->LAYOUT_STOP();
?>