<?php
# using:

#################################################
#
#   Date:   2018-10-19 Bill     [2018-1019-0953-47066]
#           remove athlete number if students not enrolled
#
#   Date:   2018-09-24 Bill     [2018-0628-1634-01096]
#           improved Athletes Number generation ($sys_custom['eSports']['OrderAthleteNumberByType'])
#
#   Date:   2018-08-10 Philips  [2018-0628-1634-01096]
#           support special format of Athletes Number ($sys_custom['eSports']['KaoYipReports'])
#
#################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT."includes/global.php");
include_once ($PATH_WRT_ROOT."includes/libdb.php");
include_once ($PATH_WRT_ROOT."includes/libsports.php");
include_once ($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();
$lsports->authSportsSystem();

// Select of students list
// $sql = "SELECT UserID, UserID FROM INTRANET_USER WHERE RecordType = '2' AND RecordStatus IN (0,1,2)";
// $students = $lsports->returnArray($sql,2);
// $a_students = build_assoc_array($students,$empty_name);
if ($sys_custom['eSports']['KaoYipReports'])
{
    $index = 1;
    $students = $lsports->Get_Enroled_Participant_List2();
    foreach ($students as $t_student) {
        $sid = $t_student[0];
        $athlnum = $index . "";
        while (strlen($athlnum) < 4) {
            $athlnum = '0' . $athlnum;
        }
        
        // Check whether student record exist in SPORTS_STUDENT_ENROL_INFO
        $sql = "SELECT COUNT(*) FROM SPORTS_STUDENT_ENROL_INFO WHERE StudentID = '$sid'";
        $exist_flag = $lsports->returnVector($sql);
        if ($exist_flag[0] == 1)    // Update Athletic Number if record exist
        {
            $sql = "UPDATE SPORTS_STUDENT_ENROL_INFO SET AthleticNum = '$athlnum', DateModified = now() WHERE StudentID = '$sid'";
            $lsports->db_db_query($sql);
        }
        else    // Insert record is record not exist
        {
            $sql = "INSERT INTO SPORTS_STUDENT_ENROL_INFO (StudentID, AthleticNum, TrackEnrolCount, FieldEnrolCount, DateModified) VALUES ('$sid', '$athlnum', 0, 0, now())";
            $lsports->db_db_query($sql);
        }
        $index++;
    }
}
else
{
    $students = $lsports->Get_Enroled_Participant_List();
    if(count((array)$students) > 0)
    {
        // [2018-1019-0953-47066] remove athlete number if not enrolled
        $studentIDArr = Get_Array_By_Key($students, 'UserID');
        $sql = "UPDATE SPORTS_STUDENT_ENROL_INFO SET AthleticNum = NULL, DateModified = NOW() WHERE StudentID NOT IN ('".implode("', '", $studentIDArr)."')";
        $lsports->db_db_query($sql);
    }
    
    $numbertype = $lsports->numberGenerationType;
    if ($numbertype == 1) {
        $cond1_1 = $lsports->numberGenRule1;
        $cond1_2 = $lsports->numberGenRule2;
        $autonum_len = $lsports->numberGenAutoLength;
        
        // define the initial generation number
        $autoNumber = 1;
    }
    else {
        $cond2_1 = $lsports->numberGenRule1;
        $cond2_2 = $lsports->numberGenRule2;
        $cond2_3 = $lsports->numberGenRule3;
    }
    $autoNumberAry = array();
    
    if ($numbertype == 1)
    {
        for ($i = 1; $i < 3; $i++) {
            $conds = ${"cond1_" . $i};
            switch ($conds) {
                case (1):
                    $HouseCodeNeed = true;
                    $orderArr[] = 1;
                    break;
                case (2):
                    $DisplayOrderNeed = true;
                    $orderArr[] = 2;
                    break;
                case (3):
                    $GroupCodeNeed = true;
                    $orderArr[] = 3;
                    break;
                default:
                    $NoSelected = true;
            }
        }
        
        foreach ($students as $t_student) {
            $sid = $t_student[0];
            
            if ($HouseCodeNeed == true) {
                $num1 = $lsports->retrieveHouseCode($sid);
            }
            if ($DisplayOrderNeed == true) {
                $num2 = $lsports->retrieveHouseDisplayOrder($sid);
            }
            if ($GroupCodeNeed == true) {
                $num3 = $lsports->retrieveGroupCode($sid);
            }
            
            $athlnum = "";
            for ($i = 0; $i < sizeof($orderArr); $i++) {
                $type = $orderArr[$i];
                $athlnum .= ${"num" . $type};
                $athlnum .= " ";
            }
            
            // [2018-0920-1057-26066] Get athlete number by type
            if($sys_custom['eSports']['OrderAthleteNumberByType']) {
                $athlNumType = $athlnum;
                if(!isset($autoNumberAry[$athlNumType])) {
                    $autoNumberAry[$athlNumType] = 1;
                }
                $autoNumber = $autoNumberAry[$athlNumType];
            }
            
            if ($autonum_len == 2) {
                $a_num = (strlen($autoNumber) == 1) ? "0" . $autoNumber : $autoNumber;
            } else if ($autonum_len == 3) {
                if (strlen($autoNumber) == 1) {
                    $a_num = "00" . $autoNumber;
                } else if (strlen($autoNumber) == 2) {
                    $a_num = "0" . $autoNumber;
                } else {
                    $a_num = $autoNumber;
                }
            }
            $athlnum .= $a_num;
            $autoNumber++;
            
            // [2018-0920-1057-26066]
            if($sys_custom['eSports']['OrderAthleteNumberByType']) {
                $autoNumberAry[$athlNumType] = $autoNumber;
            }
            
            // Check whether student record exist in SPORTS_STUDENT_ENROL_INFO
            $sql = "SELECT COUNT(*) FROM SPORTS_STUDENT_ENROL_INFO WHERE StudentID = '$sid'";
            $exist_flag = $lsports->returnVector($sql);
            
            if ($exist_flag[0] == 1)    // Update Athletic Number if record exist
            {
                $sql = "UPDATE SPORTS_STUDENT_ENROL_INFO SET AthleticNum = '$athlnum', DateModified = now() WHERE StudentID = '$sid'";
                $lsports->db_db_query($sql);
            }
            else        // Insert record is record not exist
            {
                $sql = "INSERT INTO SPORTS_STUDENT_ENROL_INFO (StudentID, AthleticNum, TrackEnrolCount, FieldEnrolCount, DateModified) VALUES ('$sid', '$athlnum', 0, 0, now())";
                $lsports->db_db_query($sql);
            }
        }
    }
    else
    {
        for ($i = 1; $i < 7; $i ++) {
            $conds = ${"cond2_" . $i};
            
            switch ($conds) {
                
                case (1):
                    $HouseCodeNeed = true;
                    $orderArr[] = 1;
                    break;
                case (2):
                    $DisplayOrderNeed = true;
                    $orderArr[] = 2;
                    break;
                case (3):
                    $GroupCodeNeed = true;
                    $orderArr[] = 3;
                    break;
                case (4):
                    $ClassNameNeed = true;
                    $orderArr[] = 4;
                    break;
                case (5):
                    $ClassNumberNeed = true;
                    $orderArr[] = 5;
                    break;
                case (6):
                    $ClassNameNumNeed = true;
                    $orderArr[] = 6;
                    break;
                default:
                    $NoSelected = true;
            }
        }
        
        foreach ($students as $t_student) {
            $sid = $t_student[0];
            
            if ($HouseCodeNeed == true) {
                $num1 = $lsports->retrieveHouseCode($sid);
            }
            
            if ($DisplayOrderNeed == true) {
                $num2 = $lsports->retrieveHouseDisplayOrder($sid);
            }
            
            if ($GroupCodeNeed == true) {
                $num3 = $lsports->retrieveGroupCode($sid);
            }
            
            if ($ClassNameNeed == true) {
                $num4 = $lsports->retrieveClassName($sid);
            }
            
            if ($ClassNumberNeed == true) {
                $num5 = $lsports->retrieveClassNumber($sid);
            }
            
            if ($ClassNameNumNeed == true) {
                $num6 = $lsports->retrieveClassNameNumber($sid);
            }
            
            $athlnum = "";
            for ($i = 0; $i < sizeof($orderArr); $i ++) {
                $type = $orderArr[$i];
                $athlnum .= ${"num" . $type};
                $athlnum .= " ";
            }
            
            // Check whether student record exist in SPORTS_STUDENT_ENROL_INFO
            $sql = "SELECT COUNT(*) FROM SPORTS_STUDENT_ENROL_INFO WHERE StudentID = '$sid'";
            $exist_flag = $lsports->returnVector($sql);
            
            if ($exist_flag[0] == 1) // Update Athletic Number if record exist
            {
                $sql = "UPDATE SPORTS_STUDENT_ENROL_INFO SET AthleticNum = '$athlnum', DateModified = now() WHERE StudentID = '$sid'";
                $lsports->db_db_query($sql);
            } else // Insert record is record not exist
            {
                $sql = "INSERT INTO SPORTS_STUDENT_ENROL_INFO (StudentID, AthleticNum, TrackEnrolCount, FieldEnrolCount, DateModified) VALUES ('$sid', '$athlnum', 0, 0, now())";
                $lsports->db_db_query($sql);
            }
        }
    }
}

intranet_closedb();
header("Location: participant_gen.php?xmsg=update");
?>