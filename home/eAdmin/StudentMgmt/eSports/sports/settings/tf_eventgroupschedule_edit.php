<?php
//# using: 

/********************************************************
 *  Modification Log
 *  Date: 2019-09-25 Philips [2019-0503-1912-35164]
 ********************************************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

$keyword = standardizeFormPostValue($_GET['keyword']);

if ($page_size_change == 1) {
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "PageItemSettings";

$lsports = new libsports();
$lsports->authSportsSystem();

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

// $eventType = $eventType? $eventType : $eventTypes[0]['EventTypeID'];

// TABLE COLUMN
// $select_event_type = $i_Sports_field_Event_Type." : ".getSelectByArray($eventTypes, "id='eventType' name='eventType' onChange='this.form.submit()'", $eventType, 0, 0, $i_status_all);
// $select_event = $i_Sports_Item." : ".getSelectByArray($eventList, "name=eventID onChange=this.form.submit()", $eventID, 0, 1);
// $select_group = $i_Sports_field_Group." : ".getSelectByArray($groups, "name=AgeGroupID onChange=this.form.submit()", $AgeGroupID, 0, 1);

### Button
### Title ###
$TAGS_OBJ[] = array($i_Sports_menu_Settings_TrackFieldName,"tf_name.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_TrackFieldEvent,"tf_event.php", 0);
$TAGS_OBJ[] = array($Lang['eSports']['Settings']['EventGroupSchedule'],"tf_eventgroupschedule.php", 1);
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

// $initBtn = "<a href=\"javascript:initEventGroupSchedule()\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_remove . "</a>";
// $editBtn = "<a href=\"tf_eventgroupschedule_edit.php\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";

### GET DATA FROM SQL


# Retrive Event Name and Type
$db_event_name = $intranet_session_language=="en"? "se.EnglishName" : "se.ChineseName";
$db_type_name = $intranet_session_language=="en"? "se.EnglishName" : "se.ChineseName";

$cols = "segs.ScheduleID, $db_event_name as Name, seg.GroupID, segs.EventRound,
			CASE se.EventType
			WHEN 1 THEN (
				CASE segs.EventRound
				WHEN 1 THEN (d_track.FirstRoundDay)
				WHEN 2 THEN (d_track.SecondRoundDay)
				WHEN 0 THEN (d_track.FinalRoundDay)
				END
			)
			WHEN 2 THEN (
				CASE segs.EventRound
				WHEN 1 THEN (d_field.FirstRoundDay)
				WHEN 0 THEN (d_field.FinalRoundDay)
				END
			)
			WHEN 3 THEN (
				d_relay.FinalRoundDay
			)
			WHEN 4 THEN (
				d_relay.FinalRoundDay
			)
			END AS RoundDay,
			segs.EventScheduleCode,
			IF(segs.EventTime IS NOT NULL, DATE_FORMAT(segs.EventTime, '%H:%i'), '') AS EventTime,
			IF(seg.GroupID < 0, (seg.GroupID * -1) + 100, sag.DisplayOrder) AS AgeGroupID,
			IF(segs.EventRound = 0, segs.EventRound + 30, segs.EventRound) AS RoundID
";
$tables = "SPORTS_EVENTGROUP_SCHEDULE segs ";
$tables .= "INNER JOIN SPORTS_EVENTGROUP seg
			ON segs.EventGroupID = seg.EventGroupID ";
$tables .= "INNER JOIN SPORTS_EVENT se
			ON seg.EventID = se.EventID ";
$tables .= "LEFT OUTER JOIN SPORTS_AGE_GROUP sag
			ON sag.AgeGroupID = seg.GroupID ";
$tables .= "LEFT OUTER JOIN SPORTS_EVENTGROUP_EXT_TRACK d_track
			ON seg.EventGroupID = d_track.EventGroupID ";
$tables .= "LEFT OUTER JOIN SPORTS_EVENTGROUP_EXT_FIELD d_field
			ON seg.EventGroupID = d_field.EventGroupID ";
$tables .= "LEFT OUTER JOIN SPORTS_EVENTGROUP_EXT_RELAY d_relay
			ON seg.EventGroupID = d_relay.EventGroupID ";
$conds = " AND segs.isDeleted = '0'";
$order = "se.EventType asc ";
$order .= ",se.DisplayOrder asc";
$order .= ",AgeGroupID asc";
$order .= ",RoundDay asc";
$order .= ",RoundID asc";

$sql = "SELECT
$cols
FROM
$tables
WHERE
1 $conds
ORDER BY
$order
";
$eventGroupScheduleAry = $lsports->returnArray($sql);
// debug_pr($sql);
// debug_pr($eventGroupScheduleAry);
// die();

$tableBody = "";
$count = 1;
foreach($eventGroupScheduleAry as $egs){
	$tableBody .= "<tr class='dataRow row_drafted" . (fmod($count,2)==1 ? '1' : '2') . "'>";
	$tableBody .= "<td>$count</td>";
	$tableBody .= "<td>$egs[Name]</td>";
	$tableBody .= "<td>".$lsports->retrieveAgeGroupName($egs['GroupID'])."</td>";
	switch($egs['EventRound']){
		case '1':
			$roundName = $i_Sports_First_Round;
			break;
		case '2':
			$roundName = $i_Sports_Second_Round;
			break;
		case '0':
			$roundName = $i_Sports_Final_Round;
			break;
	}
	$tableBody .= "<td>$roundName</td>";
	$tableBody .= "<td>$egs[RoundDay]</td>";
	$tableBody .= "<td>". "<input type='text' name='EventScheduleCode[$egs[ScheduleID]]' id='$egs[ScheduleID]' value='$egs[EventScheduleCode]' />" ."</td>";
	$tableBody .= "<td>" . $linterface->Get_Time_Selection('EventTime_'.$egs['ScheduleID'], $egs['EventTime'],'',1). "</td>";
	$tableBody .= "</tr>";
	$count++;
}

$linterface->LAYOUT_START();
?>

<br />
<form name="form1" method="post" action="tf_eventgroupschedule_update.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
	<td align="center">
      <table width="96%" border="0" cellspacing="0" cellpadding="0">
      	<tr>
			<td align="left" class="tabletext">
                 <table width="100%" border="0" cellspacing="0" cellpadding="0">
                 	<tr>
                         <td colspan="2">
                 			<table width='100%' class="common_table_list_v30 edit_table_list_v30" border='0' cellspacing='0' cellpadding='4' />
	                 			<thead>
		                 			<tr class="tabletop">
		                 				<th class="tabletop" width='1'>#</th>
		                 				<th class="tabletop" width='30%'><?php echo $i_Sports_field_Event_Name;?></th>
		                 				<th class="tabletop" width='20%'><?php echo $i_Sports_field_GradeName;?></th>
		                 				<th class="tabletop tabletoplink"><?php echo $Lang['eSports']['Settings']['EventRound'];?></th>
		                 				<th class="tabletop tabletoplink"><?php echo $i_Sports_Race_Day?></th>
		                 				<th class="tabletop tabletoplink"><?php echo $Lang['eSports']['Settings']['EventScheduleCode']?></th>
		                 				<th class="tabletop tabletoplink"><?php echo $Lang['eSports']['Settings']['EventTime']?></th>
		                 			</tr>
	                 			</thead>
	                 			<tbody>
	                 				<?php echo $tableBody?>
	                 			</tbody>
                 			</table>
                 		</td>
                 	</tr>
                 	<tr height="30px">
	                	<td>&nbsp;</td>
	                </tr>
                 	<tr>
	        			<td align="center" colspan="2">
	        				<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
	        				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='tf_eventgroupschedule.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
	        			</td>
					</tr>
                 </table>
			</td>
		</tr>
      </table>
     </td>
</tr>
</table>
<br />
<input type="hidden" name="Action" value="edit" />
</form>
<script type="text/javascript">
	function initEventGroupSchedule(){
		document.form1.Action.value = "init";
		document.form1.submit();
	}
</script>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>