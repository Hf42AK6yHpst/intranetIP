<?php
# using: 

###########################
#
#	2019-05-14  Bill
#   - prevent SQL Injection
#
#	2017-09-14	Bill	[2017-0906-1708-05236]
# 	-  Support Enrolment Restriction
#
#	2016-10-12	Bill	[2016-0927-1445-23073]
#	- pass $skipOnlineEnrolChecking = 1 to allow admin to do the amendment for events with Online Enrol : false
#
#	2013-09-03	Roy
#	- POST hidden input tc to update page
#
#	2012-09-21	YatWoon
#	- fixed: data retreive for "maxTrack"
#
#	2012-06-13	YatWoon
#	- add event quota checking
#
###########################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

# Get Student Info (Group)
# Get Event available for Enrolment
# Show Enrolment details
$names = $lswimminggala->retrieveEventTypeName();
$txt_track = $names[0];
$txt_field = $names[1];

# Retrieve Athletic Number
$StudentID = IntegerSafe($StudentID);
$sql = "SELECT AthleticNum FROM SWIMMINGGALA_STUDENT_ENROL_INFO WHERE StudentID = '$StudentID'";
$athleticNum = $lswimminggala->returnArray($sql,1);

# Retrieve Student Info
$lu = new libuser($StudentID);
$s_name = $lu->UserNameLang();
$s_class = $lu->ClassName;
$s_num = $lu->ClassNumber;
$gender = $lu->Gender;
$birth = $lu->DateOfBirth;

# Retrieve House Info
$house_namefield = ($intranet_session_language=="en") ? "c.EnglishName" : "c.ChineseName";
$sql = "SELECT $house_namefield, c.ColorCode FROM INTRANET_USERGROUP as a LEFT OUTER JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID, INTRANET_HOUSE as c WHERE b.GroupID = c.GroupID AND a.UserID = '$StudentID' And b.AcademicYearID = '".Get_Current_Academic_Year_ID()."' And b.RecordType = '4'";
$temp = $lswimminggala->returnArray($sql,2);
if(sizeof($temp) > 0)
{
	$house_name	= $temp[0][0];
	$house_color = $temp[0][1];
}

# Student without Birthday
if($birth=="0000-00-00 00:00:00")
{
        $have_DOB = 0;
        $targetGroupID = "";
        $targetGroupName = "N/A";
}
# Get Student related Age Group
else
{
        $have_DOB = 1;
        $birthArr = explode(" ", $birth);
        $birth = trim($birthArr[0]);

        # Retrieve Group Info
		$namefield = ($intranet_session_language == "b5") ? "ChineseName" : "EnglishName";
        $sql = "SELECT AgeGroupID, $namefield, DOBLowLimit, DOBUpLimit FROM SWIMMINGGALA_AGE_GROUP WHERE Gender = '$gender'";
        $groups = $lswimminggala->returnArray($sql,4);
		
        # Check which Age Group Student belongs to
        for($i=0; $i<sizeof($groups); $i++)
        {
                list($gid, $gname, $lowDOB, $upDOB) = $groups[$i];
                if($upDOB == NULL)
                {
                        if($birth <= $lowDOB)
                        {
                                $targetGroupID = $gid;
                                $targetGroupName = $gname;
                                break;
                        }
                }
                else if($lowDOB == NULL)
                {
                        if($birth >= $upDOB)
                        {
                                $targetGroupID = $gid;
                                $targetGroupName = $gname;
                                break;
                        }
                }
                else
                {
                        if(($birth <= $lowDOB) && ($birth >= $upDOB))
                        {
                                $targetGroupID = $gid;
                                $targetGroupName = $gname;
                                break;
                        }
                }
        }

        # Retrieve Enroled EventGroup IDs
        $sql = "SELECT EventGroupID FROM SWIMMINGGALA_STUDENT_ENROL_EVENT WHERE StudentID = '$StudentID'";
        $enroledID = $lswimminggala->returnArray($sql, 1);
}

# Retrieve Enrollment Rules
# 1 - Track
# modified by Marcus 20090917 - AgeGroup Enrol Count Enhancement Follow up
//$rules = $lswimminggala->retrieveEnrolmentRules();
//list($maxTrack, $details) = $rules[0];
$AgeGroupInfo = $lswimminggala->returnStudentAgeGroup($StudentID);
$maxTrack = $AgeGroupInfo['EnrolMaxTrack']+0;

if($maxTrack == 0)
{
	$rules = $lswimminggala->retrieveEnrolmentRules();
//  $maxTrack = $rules[0];
	list($maxTrack, $details) = $rules[0];
}

$MODULE_OBJ['title'] = $i_Sports_Participant.($intranet_session_language=="en"?" ":"").$i_Sports_Detail;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>

<SCRIPT LANGUAGE=Javascript>
var maxTrack = "<?=$maxTrack?>";
var track = '<?=$txt_track?>';
</SCRIPT>

<?php
if($have_DOB==1)
{
        # Retrieve Restrict Quota Track and Field Events that can be Enrolled
        $RQ_eventgroup = $lswimminggala->retrieveRestrictQuotaEventGroupInfo($targetGroupID, $StudentID, $skipOnlineEnrolChecking=1);

        # Retrieve Unrestrict Quota Track and Field Events that can be Enrolled
        $UQ_eventgroup = $lswimminggala->retrieveUnrestrictQuotaEventGroupInfo($targetGroupID, $StudentID, $skipOnlineEnrolChecking=1);
}
$navigation = $i_frontpage_separator.$i_Sports_Enrolment;

# [2017-0906-1708-05236] Check if Student is allowed for enrol or not
$sql = "SELECT Reason FROM SWIMMINGGALA_ENROL_RESTRICTION_LIST WHERE UserID = '$StudentID'";
$restrict_reason = $lswimminggala->returnVector($sql);
$can_apply = $restrict_reason[0]? 0 : 1;
?>

<SCRIPT LANGUAGE=Javascript>
function checkform(obj)
{
        		var tc = countChecked(obj, "RQ_EventGroup1[]");
                var totalCount = parseInt(tc);

                if(tc > maxTrack)
                {
                        alert('<?=$i_Sports_Enrolment_Warn_MaxEvent?> ' + maxTrack + ' <?=$Lang['eSports']['per']?>' + track + '<?=$Lang['eSports']['events']?>');
                        return false;
                }
                else
						document.getElementById("tc").value = tc;
                        return true;
}
</SCRIPT>

<table width="90%" border="0" cellpadding="5" cellspacing="0">
<tr> 
	<td><table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
<tr>
<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_UserStudentName?></td>
<td width="80%" valign="top" class="tabletext"><?=$s_name?></td>
</tr>
<tr>
<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_ClassName?></td>
<td valign="top" class="tabletext"><?=$s_class?></td>
</tr>
<tr>
<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_UserClassNumber?></td>
<td valign="top" class="tabletext"><?=$s_num?></td>
</tr>
<tr>
<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Sports_field_Group?></td>
<td valign="top" class="tabletext"><?=$targetGroupName?></td>
</tr>
<tr>
<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Sports_Participant_Number?></td>
<td valign="top" class="tabletext"><?=$athleticNum[0][0]?></td>
</tr>
<tr>
<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?="$i_Sports_Enrolment_MaxEvent ($txt_track)"?></td>
<td valign="top" class="tabletext"><?=$maxTrack?></td>
</tr>
<tr>
<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_House?></td>
<td valign="top" class="tabletext"><?=$lswimminggala->house_flag2($house_color, $house_name);?></td>
</tr>
	</table></td>
</tr>
<tr> 
	<td height="1" class="dotline"><img src="/images/2007a/10x10.gif" width="10" height="1"></td>
</tr>
</table>

<!----------------- List //----------------->      
<form name="form1" method="post" action="detail_update.php" enctype="multipart/form-data" ONSUBMIT="return checkform(this)">
	<table width="90%" border="0" cellpadding="5" cellspacing="0">
     
<?php
         if($have_DOB==1 && $can_apply)
         {
			 	###################### Restrict Quota Events #######################
                 $currType = "";
                 $displayHTML = "";
                 for($i=0; $i<sizeof($RQ_eventgroup); $i++)
                 {
                        list($eg_id, $event_id, $event_name, $type_id, $type_name, $group_id, $event_quota) = $RQ_eventgroup[$i];

                        $ext_exist = $lswimminggala->checkExtInfoExist($type_id, $eg_id);
                        if($ext_exist == 1)
                        {
                                if($currType != $type_id)
                                {
                                         $displayHTML .= "<tr class='tabletop'>";
                                         $displayHTML .= "<td width='25'>&nbsp;</td>";
                                         $displayHTML .= "<td><span class='tabletopnolink'>".$type_name."</span> </td>";
                                         $displayHTML .= "<td>&nbsp;</td>";
                                         $displayHTML .= "</tr>";

                                         $currType = $type_id;
                                         $typeArr[] = $type_id;
                                }
								
                                $checked = "";
                                $enroled_hint = "";
                                for($j=0; $j<sizeof($enroledID); $j++)
                                {
                                         if($eg_id == $enroledID[$j][0])
                                         {
                                                 $checked = "CHECKED";
                                                 $enroled_hint = "<font color=red> ($i_Sports_Enrolled)</font>";
                                         }
                                }

                                if($group_id == '-2')
                                {
                                        $event_name = $i_Sports_Event_Girls_Open."&nbsp;".$event_name;
                                }
                                else if($group_id == '-1')
                                {
                                        $event_name = $i_Sports_Event_Boys_Open."&nbsp;".$event_name;
                                }
                                else if($group_id == '-4')
                                {
                                        $event_name = $i_Sports_Event_Mixed_Open."&nbsp;".$event_name;
                                }

                                $css = ($i%2?"":"2");
                                
                                # Check if Event is full or not
                                $cur_enroled = $lswimminggala->returnEventEnroledNo($eg_id);
                                $enroled_hint .= ($event_quota && $cur_enroled>=$event_quota) ? " <font color=red> (". $Lang['eSports']['QuotaFull'] .")</font>" : "";
                                $this_disabled = ($event_quota && $cur_enroled>=$event_quota && !$checked) ? " disabled" : "";
                                
                                $displayHTML .= "<tr class='tablerow". ( ($i)%2 + 1 )."'>";
                                $displayHTML .= "<td><input type='checkbox' ". $this_disabled ." name='RQ_EventGroup".$type_id."[]' value='".$eg_id."' ". $checked ." id='RQ_EventGroup_". $i ."'></td>";
                                $displayHTML .= "<td height='34' class='tabletext'> <label for='RQ_EventGroup_". $i ."'>".$event_name."</label> </td>";
                                $displayHTML .= "<td class='tabletext'><span class='tabletextrequire'>". $enroled_hint ."</span></td>";
                                $displayHTML .= "</tr>";
                        }
                 }
                 
                 if(is_array($typeArr))
                        $types = implode($typeArr, ",");
                 else
                        $types = $typeArr;
				
                 if($displayHTML == "")
                 	$displayHTML .= "<tr><td align='center' colspan='3' class='tabletext'>".$i_no_record_exists_msg."</td></tr>";

       			############################## Unrestrict Quota Events ##############################
                $recordExist = 0;
                $displayHTML2 = "<tr><td colspan='3' class='tabletext'>".$i_Sports_Event_UnrestrictQuota_Event."</td></tr>";
				
                for($i=0; $i<sizeof($UQ_eventgroup); $i++)
                {
                        list($eg_id, $event_name, $type_id, $group_id, $event_quota) = $UQ_eventgroup[$i];

                        $ext_exist = $lswimminggala->checkExtInfoExist($type_id, $eg_id);
                        if($ext_exist == 1)
                        {
                                $recordExist = 1;
                                //$css = ($i%2?"":"2");

                                $checked = "";
                                $enroled_hint = "";
                                for($j=0; $j<sizeof($enroledID); $j++)
                                {
                                        if($eg_id == $enroledID[$j][0])
                                        {
                                                $checked = "CHECKED";
                                                $enroled_hint = "<font color=red> ($i_Sports_Enrolled)</font>";
                                        }
                                }

                                if($group_id == '-2')
                                {
                                        $event_name = $i_Sports_Event_Girls_Open.$event_name;
                                }
                                else if($group_id == '-1')
                                {
                                        $event_name = $i_Sports_Event_Boys_Open.$event_name;
                                }
                                else if($group_id == '-4')
                                {
                                        $event_name = $i_Sports_Event_Mixed_Open.$event_name;
                                }
                                
								# Check if Event is full or not
                                $cur_enroled = $lswimminggala->returnEventEnroledNo($eg_id);
                                $enroled_hint .= ($event_quota && $cur_enroled>=$event_quota) ? " <font color=red> (". $Lang['eSports']['QuotaFull'] .")</font>" : "";
                                $this_disabled = ($event_quota && $cur_enroled>=$event_quota && !$checked) ? " disabled" : "";

                                $displayHTML2 .= "<tr class='tablerow". ( ($i)%2 + 1 )."'>";
                                $displayHTML2 .= "<td><input type='checkbox' ". $this_disabled ." name='UQ_EventGroup[]' value='".$eg_id."' ". $checked ." id='UQ_EventGroup_". $i ."'></td>";
                                $displayHTML2 .= "<td height='34' class='tabletext'> <label for='UQ_EventGroup_". $i."'>".$event_name."</label> </td>";
                                $displayHTML2 .= "<td class='tabletext'><span class='tabletextrequire'>". $enroled_hint ."</span></td>";
                                $displayHTML2 .= "</tr>";
                        }
                }
				
                if($recordExist!=0)
                {
                        $displayHTML = $displayHTML.$displayHTML2;
                }
				
                echo $displayHTML;
         }
         else
         {
		         if(!$can_apply)
		         {
			         echo "<tr><td align='center' colspan='3' class='tabletext'>".$Lang['eSports']['NotAllowForEnrolment'] . $restrict_reason[0] ."</td></tr>";
		         }
		         else
		         {
	                 echo "<tr><td align='center' colspan='3' class='tabletext'>".$i_Sports_No_DOB."</td></tr>";
	             }
         }
?>
    </table>
    
	<table width="95%" border="0" cellpadding="5" cellspacing="0">    
    	<tr>
        	<td colspan="3">        
                <table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
        			<td align="center">
        				<?= $linterface->GET_ACTION_BTN($button_save, "submit", "", "submit2", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
                    	<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "", "reset2", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
                        <?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();", "close_btn") ?>
        			</td>
        		</tr>
                </table>
        	</td>
        </tr>
    </table>

<input type="hidden" name="types" value="<?=$types?>">
<input type="hidden" name="StudentID" value="<?=$StudentID?>">
<input type="hidden" id="tc" name="tc" value="" />

</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>