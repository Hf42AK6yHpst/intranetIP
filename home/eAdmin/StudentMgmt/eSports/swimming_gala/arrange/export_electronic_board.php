<?php
# using: yat

###############################
#
#	Date:	2014-10-22	YatWoon	[Case#A70038] [ip.2.5.5.10.1]
#			cust "Export for electronic board format".	
#			
###############################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();
$lexport = new libexporttext();


$AgeGroups = $lswimminggala->retrieveAgeGroupIDNames();	# retrieve all age group ids and names
# Add Boys Open and Girls Open 
$openBoy[0] = '-1';
$openBoy[1] = $i_Sports_Event_Boys_Open;
$openGirl[0] = '-2';
$openGirl[1] = $i_Sports_Event_Girls_Open;
$openMixed[0] = '-4';
$openMixed[1] = $i_Sports_Event_Mixed_Open;
$AgeGroups[] = $openBoy;
$AgeGroups[] = $openGirl;
$AgeGroups[] = $openMixed;

$TREvents = $lswimminggala->retrieveTrackFieldEventName();		# retrieve all track and field eventgroup ids and names
// debug_pr($TREvents);
// $house_relay_name = $lswimminggala->retrieveEventTypeNameByID(3);
// $class_relay_name = $lswimminggala->retrieveEventTypeNameByID(4);
// $numberOfLane = $lswimminggala->numberOfLanes;


$export_header[] = array("event_num", "event_code", "event_name", "idno", "class", "no", "ename", "cname", "sex", "house", "grade", "totheats", "heat_num", "lane_no", "position", "Round");
$utf_content = array();

$roundTypeAry= array(1,2,0); 	
$typeIDs = array(1);	

# Create a new array to store the track and field results
for($i=0; $i<sizeof($TREvents); $i++)
{
	list($event_id, $event_name) = $TREvents[$i];
	for($j=0; $j<sizeof($AgeGroups); $j++)
	{
		list($group_id, $group_name) = $AgeGroups[$j];
		$break = 0;
		# Retrieve event group id
		$eventGroupInfo= $lswimminggala->retrieveEventGroupID($event_id, $group_id, $typeIDs);
		if(!empty($eventGroupInfo))
		{
			$eventGroupID = $eventGroupInfo[0];
			$EventGroupInfoDetails = $lswimminggala->retrieveEventGroupDetail($eventGroupID);
			$total_heat_num = $lswimminggala->returnHeatNumberByEventGroupID($eventGroupID);
			
			for($r=0;$r<3;$r++)
			{
				$roundType = $roundTypeAry[$r];
				$LaneArrange = $lswimminggala->retrieveTFLaneArrangeDetail($eventGroupID, $roundType);
				if(!empty($LaneArrange))
				{
					$break = 1;
					foreach($LaneArrange as $k=>$result)
					{
						$this_row = array();
						$lu = new libuser($result['StudentID']);
						$house = $lswimminggala->retrieveStudentHouseInfo($result['StudentID']);
						
						$this_row[] = "";
						$this_row[] = $EventGroupInfoDetails['EventCode'];
						$this_row[] = $group_name . " " .$event_name;
						$this_row[] = $lu->UserLogin;
						$this_row[] = $lu->ClassName;
						$this_row[] = $lu->ClassNumber;
						$this_row[] = $lu->EnglishName ." (".$lu->ClassName ."-".$lu->ClassNumber.")";
						$this_row[] = "";	# $lu->ChineseName;
						$this_row[] = "";	# gender
						$this_row[] = $house[0]['HouseCode'];
						$this_row[] = "";	# grade
						$this_row[] = $total_heat_num;
						$this_row[] = $result['Heat'];
						$this_row[] = $result['ArrangeOrder'];
						$this_row[] = "";
						$this_row[] = $roundType;
						
						$utf_content[] = $this_row;
					}
				}
			}
		}
		
		if($break)
			$utf_content[] = array("");
	}
	
}

// debug_pr($utf_content);
// exit;

$filename = "lane-arrangement.csv";
$export_text = $lexport->GET_EXPORT_TXT($utf_content,$export_header);
$lexport->EXPORT_FILE($filename,$export_text); 

intranet_closedb();
?>
