<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

$eventGroupID = $_POST['eventGroupID'];


$exist_flag = 0;

for($i=1; $i<=$lswimminggala->numberOfLanes; $i++)
{
	${"ClassID".$i} = $_POST['ClassID'.$i];
	
	$cid = ${"ClassID".$i};
	
	if($cid != "")	# update / insert 
	{
		# Check whether the record exist
		$sql = "SELECT ClassID FROM SWIMMINGGALA_CLASS_RELAY_LANE_ARRANGEMENT WHERE EventGroupID = '$eventGroupID' AND ArrangeOrder = '$i'";
		$temp = $lswimminggala->returnVector($sql);
		$exist_ClassID = $temp[0];
		
		if($exist_ClassID != "")		# Update Record
		{
			if($exist_ClassID != $cid)
			{
				$sql = "UPDATE SWIMMINGGALA_CLASS_RELAY_LANE_ARRANGEMENT 
							SET ClassID = '$cid', DateModified = now()
							WHERE EventGroupID = '$eventGroupID' AND ArrangeOrder = '$i'";
				$lswimminggala->db_db_query($sql);
			}
		}
		else		# Add Record
		{
			$sql = "INSERT INTO SWIMMINGGALA_CLASS_RELAY_LANE_ARRANGEMENT 
						(EventGroupID, ClassID, ArrangeOrder, DateModified)
						VALUES ('$eventGroupID', '$cid', '$i', now())";
			$lswimminggala->db_db_query($sql);
		}
	}
	else	# delete 
	{	
		$sql = "DELETE FROM SWIMMINGGALA_CLASS_RELAY_LANE_ARRANGEMENT WHERE EventGroupID='$eventGroupID' and ArrangeOrder=$i";
		$lswimminggala->db_db_query($sql);
	}
	
	
	
	/*
	if($cid != "")	# update / insert 
	{
		# Check whether the record exist
		$sql = "SELECT COUNT(*) FROM SWIMMINGGALA_CLASS_RELAY_LANE_ARRANGEMENT WHERE EventGroupID = '$eventGroupID' AND ClassID = '$cid'";
		$temp = $lswimminggala->returnVector($sql);
		$exist_flag = $temp[0];
		
		if($exist_flag != 0)		# Update Record
		{
			$sql = "UPDATE SWIMMINGGALA_CLASS_RELAY_LANE_ARRANGEMENT 
						SET ArrangeOrder = '$i', DateModified = now()
						WHERE EventGroupID = '$eventGroupID' AND ClassID = '$cid'";
			$lswimminggala->db_db_query($sql);

		}
		else		# Add Record
		{
			$sql = "INSERT INTO SWIMMINGGALA_CLASS_RELAY_LANE_ARRANGEMENT 
						(EventGroupID, ClassID, ArrangeOrder, DateModified)
						VALUES ('$eventGroupID', '$cid', '$i', now())";
			$lswimminggala->db_db_query($sql);
		}
	}
	*/
	
}

intranet_closedb();
header("Location: schedule_class_relay.php?xmsg2=update");
?>