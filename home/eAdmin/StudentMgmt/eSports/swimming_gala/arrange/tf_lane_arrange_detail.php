<?php
# using: 

/*****************************************************************
 * 	Modification log
 * 	2016-05-30 Bill:	[2016-0527-1154-29206]
 * 		- correct wordings
 * ****************************************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$lsports = new libswimminggala();
$lsports->authSportsSystem();
$house_relay_name = $lsports->retrieveEventTypeNameByID(3);
$class_relay_name = $lsports->retrieveEventTypeNameByID(4);

$linterface 	= new interface_html();
$CurrentPage	= "PageArrangement_Schedule";

$EventGroupInfo = $lsports->retrieveEventGroupDetail($eventGroupID);

$roundType = 1;
$LaneArrange = $lsports->retrieveTFLaneArrangeDetail($eventGroupID, $roundType);
$GroupName = $lsports->returnAgeGroupName($EventGroupInfo['GroupID']);

$EnrolArrangeNum = $lsports->Get_EventGroup_Enroled_Arranged_Number($eventGroupID);
list($total, $arranged) = $EnrolArrangeNum[0];

$EventInfo = $lsports->returnEventInfo($EventGroupInfo['EventID']);
$eventName = $EventInfo['EventName'];
$eventType = $EventInfo['EventType'];

# Create a new array for lane arrangement
for($i=0; $i<sizeof($LaneArrange); $i++)
{
	list($s_id, $s_name, $s_class, $heat, $order) = $LaneArrange[$i];
	$arrangeArr[$heat][$order]["id"] = $s_id;
        
	$arrangeArr[$heat][$order]["info"] = "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	$arrangeArr[$heat][$order]["info"] .= "<tr>";
        $arrangeArr[$heat][$order]["info"] .= "<td align='left' class='tabletext'> ". $s_name ." </td>";
        $arrangeArr[$heat][$order]["info"] .= "</tr>";
        if($s_class) {
        $arrangeArr[$heat][$order]["info"] .= "<tr>";
	$arrangeArr[$heat][$order]["info"] .= "<td align='left' class='tabletext'>(".$s_class.")</td>";
        $arrangeArr[$heat][$order]["info"] .= "</tr>";
        }
	$arrangeArr[$heat][$order]["info"] .= "</table>";
                
}
# Get the FirstRoundGroupCount (in order to check if �u�D�ƥ� is selected)
if($eventType == 1)		# If this is Track event
{
	$sql = "SELECT FirstRoundGroupCount,FirstRoundType FROM SWIMMINGGALA_EVENTGROUP_EXT_TRACK WHERE EventGroupID = '$eventGroupID'";
	$FirstRoundNum = $lsports->returnVector($sql);
	$sql = "SELECT FirstRoundType FROM SWIMMINGGALA_EVENTGROUP_EXT_TRACK WHERE EventGroupID = '$eventGroupID'";
	$FirstRoundType=$lsports->returnVector($sql);
}

# Lane Arrangement
$laneNumFlag = 0;
$numberOfLane = $lsports->numberOfLanes;
$resultDisplay = "";
if($FirstRoundNum[0] == 0 || sizeof($FirstRoundNum) != 0)
{
	$laneNumFlag = 1;
	$resultDisplay .= "<tr>";
	$resultDisplay .= "<td>&nbsp;</td>";
	for($i=1; $i<=$numberOfLane; $i++)
	{
		$lineTitle = $eventType==1?$lsports->Get_Lang_Line($i):$i;
		$resultDisplay .= "<td class='tablebluetop tabletopnolink'>".$lineTitle."</td>";
	}
	$resultDisplay .= "</tr>";
}
else
{
	$resultDisplay .= "<tr><td colspan='".($numberOfLane+1)."' >&nbsp;</td></tr>";
}

# retrieve the maxinum key of the arrangeArr array
if(is_array($arrangeArr))
{
	$keys1 = array_keys($arrangeArr);	
	rsort($keys1);
	$maxGroup = $keys1[0];
}
else
	$maxGroup = 0;

for($i=1; $i<=$maxGroup; $i++)
{
	$tempGroup = $arrangeArr[$i];
	# Retrieve the maxinum key of the tempGroup array
	if(sizeof($tempGroup) != 0)
	{
		$keys2 = array_keys($tempGroup);
		rsort($keys2);
		$maxPos = $keys2[0];
	}
	else
		$maxPos = 0;

	$rowspan = ceil(sizeof($tempGroup)/$numberOfLane);
	$resultDisplay .= "<tr height='40'>";
	$resultDisplay .= "<td width='50' rowspan='$rowspan' class='tablebluelist'>".$lsports->Get_Lang_Heat($i)."</td>";
	
	$sid_arr = Get_Array_By_Key(array_values((array)$tempGroup),"id");
	$stu_info = BuildMultiKeyAssoc($lsports->Get_Student_Basic_Info($sid_arr),"UserID");
	
	for($k=1; $k<=$maxPos; $k++)
	{
		$key = $k;
		
		$sid = $tempGroup[$key]["id"];
		$info = $tempGroup[$key]["info"];

		# Retrieve House Color Code of the Student
//		$House = $lsports->retrieveStudentHouseInfo($sid);
		$thisStudentInfo = $stu_info[$sid]; 
		$thisHouseInfo = $lsports->retrieveHouseInfo($thisStudentInfo['HouseID']);
                
                # Student info 
                $stu_into_table = "";
		$stu_into_table .= "<table width='100%' border='0' cellspacing='0' cellpadding='3'>";
                $stu_into_table .= "<tr>";
                $stu_into_table .= "<td align='center' valign='top'>";
                $stu_into_table .= $lsports->house_flag($thisHouseInfo['ColorCode']);
                $stu_into_table .= "</td>";
                $stu_into_table .= "<td width='100%'>". $info ."</td>";
                $stu_into_table .= "</tr>";
                $stu_into_table .= "</table>";
                
		# Display the Student Info
		if($key > $numberOfLane && ($key%$numberOfLane) == 1)
		{
			$resultDisplay .= "</tr>";
			$resultDisplay .= "<tr height='40'>";
			//$resultDisplay .= "<td width='50'>&nbsp;</td>";
			$resultDisplay .= "<td>".$stu_into_table."</td>";
		}
		else
		{
			$resultDisplay .= "<td>".$stu_into_table."</td>";
		}
	}
	if($maxPos < $numberOfLane)
	{
		$diff = $numberOfLane - $maxPos;
		for($j=0; $j<$diff; $j++)
			$resultDisplay .= "<td>&nbsp;</td>";
	}
	$resultDisplay .= "</tr>";
}

# Change Position
if($FirstRoundNum[0] == 0 || sizeof($FirstRoundNum) != 0)
{
	$changeGroupNum = floor($numberOfLane/2);
	$change_position_tag = "";
	for($i=0; $i<$changeGroupNum; $i++)
	{
		$lane_select1 = "<SELECT name='lane1_$i'>";
		$lane_select1 .= "<OPTION value=''>-</OPTION>";
		for($j=1; $j<=$numberOfLane; $j++)
		{
			$lane_select1 .= "<OPTION value='$j'>$j</OPTION>";
		}
		$lane_select1 .= "</SELECT>";
		
		$group_select1 = "<SELECT name='group1_$i'>";
		$group_select1 .= "<OPTION value=''>-</OPTION>";
		for($k=1; $k<=$maxGroup; $k++)
		{
			$group_select1 .= "<OPTION value='$k'>$k</OPTION>";
		}
		$group_select1 .= "</SELECT>";

		$lane_select2 = "<SELECT name='lane2_$i'>";
		$lane_select2 .= "<OPTION value=''>-</OPTION>";
		for($j=1; $j<=$numberOfLane; $j++)
		{
			$lane_select2 .= "<OPTION value='$j'>$j</OPTION>";
		}
		$lane_select2 .= "</SELECT>";
		
		$group_select2 = "<SELECT name='group2_$i'>";
		$group_select2 .= "<OPTION value=''>-</OPTION>";
		for($k=1; $k<=$maxGroup+1; $k++)
		{
			$group_select2 .= "<OPTION value='$k'>$k</OPTION>";
		}
		$group_select2 .= "</SELECT>";

		
		$change_position_tag .= "<tr class='tablerow". ( ($i)%2 + 1 )."'>";
		$change_position_tag .= "<td>".$group_select1." - ".$lane_select1."</td>";
		$change_position_tag .= "<td>".$group_select2." - ".$lane_select2."</td>";
		$change_position_tag .= "</tr>";
	}
} 

# Manual Assign Lane
$UnassignedStudent = $lsports->Get_Event_UnAssigned_Student($eventGroupID);
if(count($UnassignedStudent)>0)
{
	$UnassignedStudentIDArr = Get_Array_By_Key($UnassignedStudent,"StudentID");
	$UnassignedStudentInfo = $lsports->Get_Student_Basic_Info($UnassignedStudentIDArr);
	
	$mal .= '<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">'."\n";
		$mal .= '<tr>'."\n";
			$mal .= '<td class="tabletop tabletoplink">'.$Lang['eSports']['Athlete'].'</td>'."\n";
			$mal .= '<td class="tabletop tabletoplink">'.$i_Sports_To.' ('.$i_Sports_Group.' - '.$i_Sports_Line.')</td>'."\n";
		$mal .= '</tr>'."\n";
	foreach($UnassignedStudentInfo as $thisStudentInfo)
	{
		$thisHouseInfo =  $lsports->retrieveHouseInfo($thisStudentInfo['HouseID']);
		
		$lane_select2 = "<SELECT name='lane[".$thisStudentInfo['UserID']."]' class='manual_assign_".$thisStudentInfo['UserID']."'>";
		$lane_select2 .= "<OPTION value=''>-</OPTION>";
		for($j=1; $j<=$numberOfLane; $j++)
		{
			$lane_select2 .= "<OPTION value='$j'>$j</OPTION>";
		}
		$lane_select2 .= "</SELECT>";
		
		$group_select2 = "<SELECT name='group[".$thisStudentInfo['UserID']."]' class='manual_assign_".$thisStudentInfo['UserID']."'>";
		$group_select2 .= "<OPTION value=''>-</OPTION>";
		for($k=1; $k<=$maxGroup+1; $k++)
		{
			$group_select2 .= "<OPTION value='$k'>$k</OPTION>";
		}
		$group_select2 .= "</SELECT>";
		
		$mal .= '<tr>'."\n";
			$mal .= '<td class="tabletext">'.$lsports->house_flag2($thisHouseInfo['ColorCode'],$thisStudentInfo['StudentName']).'</td>'."\n";
			$mal .= '<td class="tabletext">'.$group_select2.' - '.$lane_select2.'</td>'."\n";
		$mal .= '</tr>'."\n";
		
		$HiddenField .= '<input type="hidden" name="ManualAssignStudent[]" value="'.$thisStudentInfo['UserID'].'">'."\n";	
	}
	$mal .= '</table>'."\n";
}

### Title ###
$TAGS_OBJ[] = array($Lang['eSports']['IndividualEvent'],"schedule.php", 1);
$TAGS_OBJ[] = array($house_relay_name,"schedule_relay.php", 0);
$TAGS_OBJ[] = array($class_relay_name,"schedule_class_relay.php", 0);
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

?>
<script language="javascript">
<!--
function click_auto_arrange()
{
	if(confirm("<?=$Lang['eSports']['AutoArrangeWarning']?>"))
	{
		document.form1.action='auto_lane_arrange2.php';
		document.form1.submit();	
	}
}
//-->
</script>
<br />   

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION2($i_Sports_Event_Detail) ?></td>
	<td align='right'>&nbsp;</td>
</tr>
<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td>
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_field_Group?></span></td>
					<td class="tabletext"><?=$GroupName?></td>
				</tr>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Item?></span></td>
					<td class="tabletext"><?=$eventName?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Enroled_Student_Count?></span></td>
					<td class="tabletext"><?=$total?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Arranged_Student_Count?></span></td>
					<td class="tabletext"><?=$arranged?></td>
				</tr>
                                
			</table>
			</td>
                </tr>
                </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                </table>                                
	</td>
</tr>
</table>               



<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION2($i_Sports_Arrange_Lane) ?></td>
	<td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>
<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td>
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                                <?=$resultDisplay?>
				</table>
			</td>
                </tr>
                </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <? if($lsports->isAdminUser($UserID) && !$lsports->HiddenAutoArrangeButton) {?>
                <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($i_Sports_Re_Auto_Arrange_Lane, "button", "click_auto_arrange();","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			</td>
		</tr>
		<? } ?>
                </table>                                
	</td>
</tr>
</table>          

<br />
<form name="form1" action="manual_lane_arrange.php" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION2($i_Sports_Arrange_Change_Postion) ?></td>
	<td align='right'>&nbsp;</td>
</tr>
<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td>
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
                                	<td class='tabletop tabletoplink'><?=$i_Sports_From?> (<?=$i_Sports_Group?> - <?=$i_Sports_Line?>)</td>
                                        <td class='tabletop tabletoplink'><?=$i_Sports_To?> (<?=$i_Sports_Group?> - <?=$i_Sports_Line?>)</td>
				</tr>
                                <?=$change_position_tag?>
				</table>
			</td>
                </tr>
                </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "","submit2") ?>
                                <?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2") ?>
                                <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='schedule.php'","cancel2") ?>
			</td>
		</tr>
                </table>                                
	</td>
</tr>
</table>
<br />

<input type="hidden" name="changeGroupNum" value="<?=$changeGroupNum?>">
<input type="hidden" name="eventGroupID" value="<?=$eventGroupID?>">
<input type="hidden" name="eventType" value="<?=$eventType?>">
<input type="hidden" name="roundType" value="<?=$roundType?>">
<input type="hidden" name="total" value="<?=$total?>">
<input type="hidden" name="arranged" value="<?=$arranged?>">
<input type="hidden" name="eventName" value="<?=$eventName?>">
<input type="hidden" name="AgeGroup" value="<?=$AgeGroup?>">
</form>

<?if(count($UnassignedStudent)>0){?>
<!-- Manual Assign Lane -->
<form name="form2" action="manual_assign_lane.php" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION2($Lang['eSports']['ManualLaneArrangement']) ?></td>
		<td align='right'>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
					<td>
						<?=$mal?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">        
			<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
				<tr>
					<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
				<tr>
					<td align="center">
						<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "","submit3") ?>
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='schedule.php'","cancel3") ?>
					</td>
				</tr>
			</table>                                
		</td>
	</tr>
</table>
<br />

<input type="hidden" name="eventGroupID" value="<?=$eventGroupID?>">
<input type="hidden" name="roundType" value="<?=$roundType?>">
<?=$HiddenField?>
</form>
<?
}


intranet_closedb();
$linterface->LAYOUT_STOP();
?>