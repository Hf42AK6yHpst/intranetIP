<?php
# editing by:
/***************************************************************************
*	modification log:
*	2015-10-15	Bill	[2015-1014-1158-16066]
*	- fixed: insert record to SWIMMINGGALA_STUDENT_ENROL_INFO rather than SPORTS_STUDENT_ENROL_INFO
*	2013-09-03	Roy
*	- add insert record to table SPORTS_STUDENT_ENROL_INFO
***************************************************************************/
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

$linterface 	= new interface_html();

### get Import Data
for($i=0;$i<count($ImportRawData);$i++)
{
	$ImportData[$i]	= explode(":_Separator_:",$ImportData[$i]);
	$RawData[$i] 		= explode(":_Separator_:",$ImportRawData[$i]);
}
$TargetClass	= explode(":_Separator_:",$ImportTargetClasses);

## select all student of target Class / Form
$TargetClassStr = count($TargetClass)>1?implode("','",$TargetClass):array_shift($TargetClass);
$sql = "SELECT UserID FROM INTRANET_USER WHERE ClassName IN ('$TargetClassStr')";
$TargetClassStudentList = $lswimminggala->returnVector($sql);

## delete all enrolment record of target class student
$TargetClassStudentListStr = count($TargetClassStudentList)>1?implode(",",$TargetClassStudentList):array_shift($TargetClassStudentList);
$sql = "DELETE FROM SWIMMINGGALA_STUDENT_ENROL_EVENT WHERE StudentID IN ($TargetClassStudentListStr)";
$lswimminggala->db_db_query($sql) or die (mysql_error);

## count events
$trackCount = 0;
## insert import data
foreach($ImportData as $key => $Record)
{
	list($StudentID,$EventGroupID)=$Record;
	list($ClassName,$ClassNumber,$EventCode)=$RawData[$key];
	$eventGroupInfoArr = $lswimminggala->Get_EventGroup_Info('', $EventGroupID);
	$eventID = $eventGroupInfoArr[0]['EventID'];
	$eventInfoArr = $lswimminggala->Get_Event_Info($eventID);
	$eventType = $eventInfoArr[0]['EventType'];
	if ($eventType == 1) $trackCount++;
	
	$sql = "	INSERT INTO 
					SWIMMINGGALA_STUDENT_ENROL_EVENT 
					(
						`StudentID`,
						`EventGroupID`,
						`DateModified`
					)
				VALUES
					(
						'$StudentID',
						'$EventGroupID',
						NOW()
					)";
	$SuccessArr[] = $lswimminggala->db_db_query($sql);
	
	#Check Record Exist Or Not
	// [2015-1014-1158-16066] change to SWIMMINGGALA_STUDENT_ENROL_INFO
	//$sql = "SELECT COUNT(*) FROM SPORTS_STUDENT_ENROL_INFO WHERE StudentID = '$StudentID'";
	$sql = "SELECT COUNT(*) FROM SWIMMINGGALA_STUDENT_ENROL_INFO WHERE StudentID = '$StudentID'";
	$exist_info = $lswimminggala->returnVector($sql);

	if($exist_info[0] == 0) 
	{
		#Insert New Record To SPORTS_STUDENT_ENROL_INFO
		$fields1 = "(StudentID, TrackEnrolCount, DateModified)";
		$values1 = "(".$StudentID.", ".$trackCount.", now())";
		// [2015-1014-1158-16066] change to SWIMMINGGALA_STUDENT_ENROL_INFO
		//$sql = "INSERT INTO SPORTS_STUDENT_ENROL_INFO $fields1 VALUES $values1";
		$sql = "INSERT INTO SWIMMINGGALA_STUDENT_ENROL_INFO $fields1 VALUES $values1";
		$SuccessArr[] = $lswimminggala->db_db_query($sql);
	}
	else
	{
		#Update Existing Record in SPORTS_STUDENT_ENROL_INFO
		// [2015-1014-1158-16066] change to SWIMMINGGALA_STUDENT_ENROL_INFO
		//$sql = "UPDATE SPORTS_STUDENT_ENROL_INFO SET TrackEnrolCount = '$trackCount', DateModified = now() WHERE StudentID = '$StudentID'";
		$sql = "UPDATE SWIMMINGGALA_STUDENT_ENROL_INFO SET TrackEnrolCount = '$trackCount', DateModified = now() WHERE StudentID = '$StudentID'";
		$SuccessArr[] = $lswimminggala->db_db_query($sql);
	}

	#Build Table Content
	$rowcss = " class='".(($key)%2==0? "tablebluerow2":"tablebluerow1")."' ";
	$insertStatus = !in_array(0, $SuccessArr)?$i_con_msg_import_success:$i_con_msg_import_failed2;
	$failcss = !in_array(0, $SuccessArr)?"":" class='red' ";
	
	$Confirmtable .= "	<tr $rowcss>";
	$Confirmtable .= "		<td class='tabletext'>".($key+1)."</td>";
	$Confirmtable .= "		<td class='tabletext'>$ClassName</td>";
	$Confirmtable .= "		<td class='tabletext'>$ClassNumber</td>";
	$Confirmtable .= "		<td class='tabletext'>$EventCode</td>";
	$Confirmtable .= "		<td $failcss>$insertStatus</td>"; 
	$Confirmtable .= "	</tr>";	
}



### Title ###
$TitleTitle1 = "<span class='contenttitle'>". $i_Sports_menu_Arrangement_EnrolmentUpdate ."</span>";
$TitleTitle = "<table width='100%' height='25' cellpadding='0' cellspacing='0' ><tr><td style=\"vertical-align: bottom;\" >".$TitleTitle1."</td></tr></table>";
$CurrentPage	= "PageArrangement_EnrolmentUpdate";

$TAGS_OBJ[] = array($TitleTitle, "", 0);    
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);
$MODULE_OBJ = $lswimminggala->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>
<br>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td colspan="2"><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td clospan="2" align="center">
			<table width="95%" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td class='tablebluetop tabletopnolink'>#</td>
				<td class='tablebluetop tabletopnolink'><?=$Lang['eSports']['csv']['ClassTitle']?></td>
				<td class='tablebluetop tabletopnolink'><?=$Lang['eSports']['csv']['ClassNumber']?></td>
				<td class='tablebluetop tabletopnolink'><?=$Lang['eSports']['csv']['EventCode']?></td>
				<td class='tablebluetop tabletopnolink'><?=$Lang['eSports']['ImportStatus']?></td>
			</tr>
			<?=$Confirmtable?>
			</table>
		</td>
	</tr>
</table>
<br>
<br>


<?
$linterface->LAYOUT_STOP();
?>
