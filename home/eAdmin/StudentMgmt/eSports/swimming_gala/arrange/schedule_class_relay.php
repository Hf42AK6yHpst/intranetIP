<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageArrangement_Schedule";

$lswimminggala = new libswimminggala();
$lclass = new libclass();

$lswimminggala->authSportsSystem();

$CREvents = $lswimminggala->retrieveClassRelayName();			# retrieve class relay eventgroup ids and names

$house_relay_name = $lswimminggala->retrieveEventTypeNameByID(3);
$class_relay_name = $lswimminggala->retrieveEventTypeNameByID(4);
$numberOfLane = $lswimminggala->numberOfLanes;

### Number of Lane for Class relay
$lane_number = "<tr>";
$lane_number .= "<td>&nbsp;</td>";
for($i=1; $i<=$numberOfLane; $i++)
	$lane_number .= "<td class='tabletop tabletopnolink' align='center'>".$i_Sports_The.$i.$i_Sports_Line."</td>";
$lane_number .= "<td class='tabletop tabletopnolink'>&nbsp;</td>";
$lane_number .= "</tr>";

### Class relay content
$class_content = "";
for($j=0; $j<sizeof($CREvents); $j++)
{
	list($event_id, $group_id, $event_name) = $CREvents[$j];
	$typeIDs = 4;

	//if(true || $group_id > 0)
	//{
		$group_name = $lswimminggala->retrieveAgeGroupName($group_id);	
		$eventGroupInfo= $lswimminggala->retrieveEventGroupID($event_id, $group_id, $typeIDs);
		$eventGroupID = $eventGroupInfo[0];

		$class_arrange = $lswimminggala->retrieveClassRelayLaneArrangement($eventGroupID);

		# Replace the space in the string
		$tempName = str_replace(" ", ",", $event_name);
 		$temp_group_name = str_replace("'", "\'", $group_name);
		
		$edit_link = "class_relay_edit.php?eventGroupID=".urlencode($eventGroupID)."&groupName=".urlencode($temp_group_name)."&eventName=".urlencode($tempName);
			
		$class_content .= "<tr class='tablerow". ($j%2?"2":"1") ."'>";
		$class_content .= "<td class='tablelist'>(".$group_name.") ".$event_name."</td>";
		
		if(sizeof($class_arrange) != 0)
		{		
			$cr_display = "";
			for($m=1; $m<=$numberOfLane; $m++)
			{
				$flag = 0;
				for($k=0; $k<sizeof($class_arrange); $k++)
				{
					list($class_id, $order) = $class_arrange[$k];

					if($order == $m)
					{
						$ClassInfo = $lclass->getClassName($class_id);

						$cr_display .= "<td align='center'>".$ClassInfo ."</td>";
						$flag = 1;
						break;
					}
				}

				if($flag != 1)
					$cr_display .= "<td>&nbsp;</td>";
			}
			$class_content .= $cr_display;
		}
		else 
		{
			$class_content .= "<td colspan=".$numberOfLane.">&nbsp;</td>";
		}
                
		$class_content .= "<td align='center'>". $linterface->GET_BTN($button_edit, "button", "window.location='". addslashes($edit_link) ."'","submit2") ."</td>";
		$class_content .= "</tr>";
	//}
}


### Title ###
$TAGS_OBJ[] = array($Lang['eSports']['IndividualEvent'],"schedule.php", 0);
$TAGS_OBJ[] = array($house_relay_name,"schedule_relay.php", 0);
$TAGS_OBJ[] = array($class_relay_name,"schedule_class_relay.php", 1);
$MODULE_OBJ = $lswimminggala->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>


<!-- Start of the Class Relay Table //-->
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>&nbsp;</td>
        <td align="right"><?=$linterface->GET_SYS_MSG($xmsg2);?></td>
</tr>
<tr>
	<td align="center" colspan="2">
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
                             
		<tr>
                	<td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="4">
                                <?=$lane_number?>
                                <?=$class_content?>
                        	</table>
                        </td>
		</tr>
                <tr>
			<td height="1" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		</table>
	</td>
</tr>
</table>

<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
