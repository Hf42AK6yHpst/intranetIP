<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageReport_ExportRecord";

$lclass = new libclass();
$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

# Create Age Group Selection List
$age_groups = $lswimminggala->retrieveAgeGroupIDNames();
$select_age_group = getSelectByArray($age_groups,"name='ageGroup' id='ageGroup'","$ageGroup",1,0);

# Create Class Selection List
$all_class[0] = -3;
$all_class[1] = $i_general_all_classes;
$classList[] = $all_class;
 
$classes = $lclass->getClassList();
foreach($classes as $i => $arr)
	$classList[$i+1] = $arr;

$class_select = getSelectByArray($classList,"name='className' id='className'","$className",0,1);

# Create House Selection List
$all_house[0] = -3;
$all_house[1] = $i_general_all_houses;
$houses[] =$all_house;
 
$housesList = $lswimminggala->retrieveHouseSelectionInfo();
foreach($housesList as $i=>$arr)
	$houses[$i+1] = $arr;

$select_house = getSelectByArray($houses,"name='house' id='house'","$house",0,1);

$day_select = "<SELECT name=raceDay>";
$day_select .= "<OPTION value=1>1</OPTION>";
$day_select .= "<OPTION value=2>2</OPTION>";
$day_select .= "<OPTION value=3>3</OPTION>";
$day_select .= "<OPTION value=0>".$i_Sports_All."</OPTION>";
$day_select .= "</SELECT>";

# Group Type defaults to "Class"
$groupType = 1;

### Title ###
$TAGS_OBJ[] = array($i_Sports_menu_Report_ExportRaceArrangement,"export_arrangement.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Report_ExportRaceResult,"export_result.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Report_ExportAthleticNumber,"export_athletic_num.php", 1);
$MODULE_OBJ = $lswimminggala->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>
<script language="JavaScript" type="text/javascript">
<!--
function toggle(x)
{
	if(x==1) {
		document.getElementById('className').style.display="block";
		document.getElementById('house').style.display="none";
	} else {
		document.getElementById('className').style.display="none";
		document.getElementById('house').style.display="block";
	}
}
//-->
</script>

<br />
<form name="form1" method="post" action="athletic_num_export_process.php" target="_blank">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td><br />
			<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_Race_Day?></span></td>
					<td><?=$day_select?></td>
				</tr>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" ><span class="tabletext"><?=$eComm['By']?></span></td>
					<td class="tabletext">
						<input type="radio" name="groupType" value="1"<?=($groupType==1)?" checked":""?> id="groupTypeClass" onClick="toggle(this.value);"><label for="groupTypeClass"><?=$i_ClassName?></label>&nbsp;
						<input type="radio" name="groupType" value="2"<?=($groupType==2)?" checked":""?> id="groupTypeHouse" onClick="toggle(this.value);"><label for="groupTypeHouse"><?=$i_House?></label>
					</td>
				</tr>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">&nbsp;</span></td>
					<td><?=$class_select?><?=$select_house?></td>
				</tr>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_field_Group?></span></td>
					<td><?=$select_age_group?></td>
				</tr>
			</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2">
		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center">
			<?= $linterface->GET_ACTION_BTN($button_preview, "button", "Preview()","submit2") ?>
			<img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" />
			<?= $linterface->GET_ACTION_BTN($button_export, "button", "Export()","submit2") ?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<br />
</form>

<script language="JavaScript" type="text/javascript">
function Preview()
{
	document.form1.action='athletic_num_export_process.php'; 
	document.form1.target = '_blank';
	document.form1.submit();
}

function Export()
{
	document.form1.action='athletic_num_export.php'; 
	document.form1.submit();
}
<!--
toggle(<?=$groupType?>);
//-->
</script>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>