<?php

/* *******************************************
 * Modification log
 * 
 *  2017-10-26	Carlos
 * 	- Get house name with libsports.php retrieveStudentHouseInfo() instead of retrieveStudentHouse().
 * 
 *	2011-06-14	YatWoon
 *	- Improved: Display event for user selection [Case#2011-0106-1041-15073]
 * 
 * 	20101103 Marcus:
 * 		add column "EventCode" to the export function 
 * 	20091209 Marcus:
 * 		add column "Result" to the export function 
 * 
 * *******************************************/

# using: Carlos
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lsports = new libswimminggala();
$lexport = new libexporttext();

$targetEventID = array();
if(!empty($targetEventIDstr))	$targetEventID = explode(",",$targetEventIDstr);

$allGroupsTemp = $lsports->retrieveAgeGroupIDNames();
$allGroups = array();
foreach($allGroupsTemp as $k => $d)
	$allGroups[] = $d['AgeGroupID'];

$allGroups[] = -1;
$allGroups[] = -2;
$allGroups[] = -4;

$ageGroups = (isset($ageGroup) && $ageGroup) ? array($ageGroup) : $allGroups;

//$export_content = "";
//$export_content .= $i_Sports_field_Group."\t".$i_Sports_menu_Settings_TrackFieldEvent."\t".$i_UserStudentName."\t".$i_UserClassName."\t".$i_UserClassNumber."\t".$i_Sports_House."\t".$i_Sports_field_Rank."\t".$i_Sports_field_Score."\n";
$export_header_ary = array($i_Sports_field_Group,$i_Sports_menu_Settings_TrackFieldEvent,$Lang['eSports']['EventCode'], $i_UserStudentName,$i_UserClassName,$i_UserClassNumber,$i_Sports_House,$i_Sports_field_Rank,$i_Sports_field_Score,$i_Sports_field_Result);

$export_content_ary = array();
for($i=0;$i<sizeof($ageGroups);$i++)
{

	$ageGroup = $ageGroups[$i];
	
	$EventAry = $lsports->retrieveEventIDByAgeGroup($ageGroup);
	$GropuInfo = $lsports->retrieveAgeGroupDetail($ageGroup);
	$EventGroupInfo = $lsports->Get_EventGroup_Info();
	$EventCodeArr = BuildMultiKeyAssoc($EventGroupInfo, array("EventID","GroupID"),"EventCode",1);
	
	$GroupName = $intranet_session_language=="en"? $GropuInfo['EnglishName'] : $GropuInfo['ChineseName'];
	
	$GroupName = $ageGroup == -1 ? $i_Sports_Event_Boys_Open : $GroupName;
	$GroupName = $ageGroup == -2 ? $i_Sports_Event_Girls_Open : $GroupName;
	$GroupName = $ageGroup == -4 ? $i_Sports_Event_Mixed_Open : $GroupName;

	foreach($EventAry as $k=>$event)
	{
		list($EventID, $EventName, $EventType) = $event;
		
		if  ((!empty($targetEventID) && in_array($EventID, $targetEventID)) || empty($targetEventID))
		{
			$EventCode = $EventCodeArr[$EventID][$ageGroup];
			
			if($EventType == 3 || $EventType==4)    continue;
	
			switch($EventType)
			{
				case 3:	
					// no need to show house relay score (useless)
					//$result = $lsports->retrieveHRResult($EventID, $ageGroup);
					break;
				case 4:
					// no need to show class relay score (useless)
					//$result = $lsports->retrieveCRResult($EventID, $ageGroup);
					break;
				default:
					$result = $lsports->retrieveTFResult($EventID, $ageGroup);
					break;
			}
	
			foreach($result as $sid=>$data)
			{
				$rank = "";
				$score = 0;
				
				# get round 1
				$rank = $data[1]['Rank'];
				$score = $data[1]['Score'];
					
				# get round 2 (if 20 exists) [Second Record]
				$rank = $data[2]['Rank'] ? $data[2]['Rank'] : $rank;
				//$score = $data[2]['Score'] ? $data[2]['Score'] : $score;
				$score += $data[2]['Score'];
				
				# get round 0  (if 0 exists) [Final Round]
				$rank = $data[0]['Rank'] ? $data[0]['Rank'] : $rank;
				//$score = $data[0]['Score'] ? $data[0]['Score'] : $score;
				$score += $data[0]['Score'];
				
				for($j=0; $j<3; $j++)
				{
	//				if(!empty($data[$j]['Rank']))
	//				{
						$metre = $data[$j]['ResultMetre'];
						$min = $data[$j]['ResultMin'];
						$sec = $data[$j]['ResultSec'];
						$ms = $data[$j]['ResultMs'];
						if(!empty($metre) || (!empty($min)||!empty($sec)||!empty($ms)))
						break;
						
	//				}
				}
				
				if($metre)
					$event_result = $metre ."m";
				else if($min || $sec || $ms)
					$event_result = $lsports->Format_TimerArr(array($min,$sec,$ms));
				else
					$event_result = "";
				
				$lu = new libuser($sid);
				$student_name = $lu->UserNameLang();
				$class_name = $lu->ClassName;
				$class_number = $lu->ClassNumber;
				//$housename = $lsports->retrieveStudentHouse($sid);
				$house_info = $lsports->retrieveStudentHouseInfo($sid);
				$housename = $house_info[0][0];
				
				if($lu->UserNameLang())
				{
					//$export_content .= $GroupName."\t".$EventName."\t".$student_name."\t".$class_name."\t".$class_number."\t".$housename."\t".$rank."\t".$score."\n";
					$export_content_ary[] = array($GroupName,$EventName, $EventCode,$student_name,$class_name,$class_number,$housename,$rank,$score,$event_result);
				}
			}
		}
	}
}
intranet_closedb();
	$filename = "event_result.csv";
//debug(count($export_content_ary));
// debug_pr($export_content_ary);
//debug_pr($export_header_ary);
//echo $export_content;
//die;
	$export_content = $lexport->GET_EXPORT_TXT($export_content_ary, $export_header_ary);	
	//echo $export_content;
	//die;
// 		debug_pr($export_content);
	$lexport->EXPORT_FILE($filename, $export_content);

?>