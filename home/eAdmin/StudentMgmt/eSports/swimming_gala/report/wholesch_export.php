<?php
# using: yat

#############################################
#	Date:	2010-11-15	YatWoon
#			Add "No. of Students Enrolled"
#			update export function  
#############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();
$lexport = new libexporttext();

# Retrieve Student Number and Enrolment Number of Different ClassLevel
$studentCount = $lswimminggala->retrieveClassLevelStudentCount();
$enrolCount = $lswimminggala->retrieveClassLevelNumberOfEnrolment();
$studentEnrolCount = $lswimminggala->retrieveClassLevelNumberOfStudentEnrolment();

$studentTotal = 0;
$enrolTotal = 0;
for($i=0; $i<sizeof($studentCount); $i++)
{
	$studentTotal = $studentTotal + $studentCount[$i][0];
	$enrolTotal = $enrolTotal + $enrolCount[$i][0];
}
# Calculate the overall average number of enrolment
$averageCount = $enrolTotal/$studentTotal;
$overall_averageCount = round($averageCount, 2);
$sortname 	= $sortname=="" ? "level_name" : $sortname;
$sortorder	= $sortorder=="" ? 0 : $sortorder;

#Export Content
//overall stat
$export_ary = array();

$export_ary[] = array($i_Sports_menu_Report_WholeSchool);
$export_ary[] = array();
$export_ary[] = array($i_Sports_Report_Total_Student_Count, $studentTotal);
$export_ary[] = array($i_Sports_Report_Enrolment_Count, $enrolTotal);
$export_ary[] = array($i_Sports_Report_Average_Enrolment_Count, $overall_averageCount);
$export_ary[] = array();

$export_ary[] = array($i_ClassLevel,$i_Sports_Report_Total_Student_Count,$i_Sports_Report_Enrolment_Count,$Lang['eSports']['NoOfStudentsEnrolled'],$i_Sports_Report_Average_Enrolment_Count);

## push the data into multi-array
$data = array();
$enrolCountAry = array(); 
foreach($enrolCount as $formEnrolCount)
{
	$enrolCountAry[$formEnrolCount[1]] = $formEnrolCount;
}
$studentenrolCountAry = array(); 
foreach($studentEnrolCount as $formEnrolCount)
{
	$studentenrolCountAry[$formEnrolCount[1]] = $formEnrolCount;
}
for($i=0; $i<sizeof($studentCount); $i++)
{
	list($s_count, $s_level, $level_name) = $studentCount[$i];
	list($e_count, $e_level) = $enrolCountAry[$s_level];
	list($se_count, $se_level) = $studentenrolCountAry[$s_level];

		# Calculate the average number of enrolment of this classlevel
		$averageCount = !empty($s_count)?$e_count/$s_count:0;
		$averageCount = round($averageCount, 2);

                $data[$i]['level_name']		= $level_name;
                $data[$i]['s_count'] 		= !empty($s_count)?$s_count:0;
                $data[$i]['e_count'] 		= !empty($e_count)?$e_count:0;
                $data[$i]['se_count'] 		= !empty($se_count)?$se_count:0;
                $data[$i]['averageCount'] 	= !empty($averageCount)?$averageCount:0;
}
foreach ($data as $key => $row) {
    $level_name_ary[$key]	= $row['level_name'];
    $s_count_ary[$key] 		= $row['s_count'];
    $e_count_ary[$key] 		= $row['e_count'];
    $se_count_ary[$key] 		= $row['se_count'];
    $averageCount_ary[$key] 	= $row['averageCount'];
}

array_multisort( ${$sortname."_ary"}, $sortorder==1?SORT_DESC:SORT_ASC, $data);

$display = "";
for($i=0; $i<sizeof($data); $i++)
{
		$export_ary[] = array($data[$i]['level_name'],$data[$i]['s_count'],$data[$i]['e_count'],$data[$i]['se_count'],$data[$i]['averageCount']);
		
}
### Content End

$utf_content = "";
foreach($export_ary as $k=>$d)
{
	$utf_content .= implode("\t", $d);
	$utf_content .= "\r\n";
}

intranet_closedb();

$filename = "wholesch.csv";
$lexport->EXPORT_FILE($filename, $utf_content); 
?>