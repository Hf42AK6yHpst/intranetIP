<?
# using: 
/*
 * 2017-10-26 (Carlos): Get house name with libsports.php retrieveStudentHouseInfo() instead of retrieveStudentHouse().
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lsports = new libswimminggala();
$lexport = new libexporttext();


$result = array();

# header
$header_ary = array("student name (Eng)", "student name (Chi)","House", "Event Participated", "School Year");
$result[] = $header_ary;

$students = $lsports->retrieveStudentInfo($engName, $chiName, $theClass, $ageGroup, $house, " d.Sequence, a.ClassName, a.ClassNumber ");

# age group Info
$AgeGroupInfo = BuildMultiKeyAssoc((array)$lsports->retrieveAgeGroupInfo(), "AgeGroupID");

# Current Year
$CurrentYear = getCurrentAcademicYear();

foreach($students as $key=>$thisStudent)
{
	list($tmp_UserID, $tmp_EnName, $tmp_ChiName, $tmp_EnClass, $tmp_ChiClass, $tmp_ClassNumber, $Gender) = $thisStudent;
    
    $participateEvent = $lsports->retrieveStudentEnroledEvent($tmp_UserID, $excludeOpenEvent = 0, $presentOnly=1);
    //$tmp_house = $lsports->retrieveStudentHouse($tmp_UserID);
    $house_info = $lsports->retrieveStudentHouseInfo($tmp_UserID);
	$tmp_house = $house_info[0][0];
    
    foreach((array) $participateEvent as $EventInfo)
    {
    	list($engEvent, $chiEvent) = $EventInfo;
    	
	    $tmpResult = array();
    	

    	#Eng Student Name
    	$tmpResult[] = $tmp_EnName;

    	#Chi Student Name
    	$tmpResult[] = $tmp_ChiName;

    	# House
    	$tmpResult[] = $tmp_house;
    	
    	#Event 
    	$tmpResult[] = $engEvent;
    	
    	# School Year
    	$tmpResult[] = $CurrentYear;
    	
    	$result[] = $tmpResult;
    }
   
}

$utf_content = "";
foreach($result as $k=>$d)
{
	$utf_content .= implode("\t", $d);
	$utf_content .= "\r\n";
}

$filename = "swimminggala_student_paticipation_cetificate.csv";
$lexport->EXPORT_FILE($filename, $utf_content);

intranet_closedb();
?>
