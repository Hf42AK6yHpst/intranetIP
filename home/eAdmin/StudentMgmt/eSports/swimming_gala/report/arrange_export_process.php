<?php
# using: 

######################################
#
#	Date:	2012-11-21	YatWoon
#			display house code before student name [Case#2012-1114-0921-46071]
#
######################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lclass = new libclass();
$lsports = new libswimminggala();
$lsports->authSportsSystem();

$lexport = new libexporttext();

$openGroupList = $_POST["openGroupList"];
$openGroupArr = explode(",", $openGroupList);

$numberOfLane = $lsports->numberOfLanes;
for($i=1; $i<=$numberOfLane; $i++)
{
	$laneField .= ",";
	$laneField .= $lsports->Get_Lang_Line($i);	
	$utf_laneField .= "\t".$lsports->Get_Lang_Line($i);
	
	$HRlaneField .= $lsports->Get_Lang_Line($i).",";
	$utf_HRlaneField .= $lsports->Get_Lang_Line($i)."\t";
	
	$CRlaneField .= $lsports->Get_Lang_Line($i).",";
	$utf_CRlaneField .= $lsports->Get_Lang_Line($i)."\t";
}
$laneField .= "\n";
$HRlaneField .= "\n";
$CRlaneField .= "\n";

$utf_laneField .= "\r\n";
$utf_HRlaneField .= "\r\n";
$utf_CRlaneField .= "\r\n";

$content1 = "";

for($i=0; $i<sizeof($EventGroupID); $i++)
{
	$tf_flag =0;
	$hr_flag = 0;
	$cr_flag = 0;
	$eventGroupID = $EventGroupID[$i];
	
	$isOpen = 0;
	for($j=0; $j<sizeof($openGroupArr); $j++)
	{
		if($openGroupArr[$j] == $eventGroupID)
		{
			$isOpen = 1;
			break;
		}
	}

	if($isOpen == 0)
	{                  
		$eventGroupName = $lsports->retrieveEventAndGroupNameByEventGroupID($eventGroupID);
		$content1 .= $eventGroupName[0]." (".$eventGroupName[1].")\n";
		$utf_content .= $eventGroupName[0]." (".$eventGroupName[1].")\t\r\n";
		$eventType = $eventGroupName[2];
	}
	else
	{
        $db_field = ($intranet_session_language=="en"?"b.EnglishName":"b.ChineseName");

        $sql = "SELECT $db_field, a.GroupID, b.EventType
                     FROM SWIMMINGGALA_EVENTGROUP as a,
                     SWIMMINGGALA_EVENT as b
                     WHERE a.EventGroupID = '$eventGroupID'
                     AND b.EventID = a.EventID
					";

		$temp = $lsports->returnArray($sql,3); 
		list($eventName, $groupID, $event_type) = $temp[0];
		
		if($groupID == '-1')
			$groupName = $i_Sports_Event_Boys_Open;
		else if($groupID == '-2')
			$groupName = $i_Sports_Event_Girls_Open;
		else if($groupID == '-4')
			$groupName = $i_Sports_Event_Mixed_Open;

		$content1 .= $eventName." (".$groupName.")\n";
		$utf_content .= $eventName." (".$groupName.")\t\r\n";
		$eventType = $event_type;
	}

	if($eventType == 1 || $eventType == 2)
	{
		$tf_flag = 1;
		$result = $lsports->returnTFLaneArrangeDetailByEventGroupID($eventGroupID, 0, 1);
		
	}
	else if($eventType == 3)
	{
		$hr_flag = 1;
		$result = $lsports->returnHRLaneArrangeDetailByEventGroupID($eventGroupID);
	}
	else if($eventType == 4)
	{
		$cr_flag = 1;
		$result = $lsports->returnCRLaneArrangeDetailByEventGroupID($eventGroupID);
	}
	else
		$result = "";

		
	# Check wether it choose the number of lane as the number of people per group
	$firstRoundCount = $lsports->returnFirstRoundGroupCount($eventGroupID, $eventType);

	if($eventType == 1 || $hr_flag == 1 || $cr_flag == 1)
	{
		if($hr_flag==1)
		{
			$content1 .= $HRlaneField;
			$utf_content .= $utf_HRlaneField;
		}
		else if($cr_flag==1)
		{
			$content1 .= $CRlaneField;
			$utf_content .= $utf_CRlaneField;
		}
		else
		{
			$content1 .= $laneField;
			$utf_content .= $utf_laneField;
		}
	}


	if(sizeof($result) != 0)
	{
		#	Create a associate array of Event 
		for($j=0; $j<sizeof($result); $j++)
		{
			if($tf_flag == 1)
			{
					list($heat, $order, $sname, $sid) = $result[$j];

					# get the house name of the student
					$house = $lsports->retrieveStudentHouseInfo($sid);
					
					#get stu info  added by marcus 19/6
					$classNum=$lsports->returnStudentClassNum($sid);
					$className=$lsports->returnStudentClassName($sid);
					$athleticNum=$lsports->returnStudentAthleticNum($sid);

					$hcode = $house[0][3] ? "(". $house[0][3] .") " : "";
	
					//$resultArr[$heat][$order] = $athleticNum." \n".$sname." (".$className." - ".$classNum.")";
// 					$resultArr[$heat][$order] = '"'.$athleticNum . chr(10) . $sname. chr(10) .'('.$className.' - '.$classNum.')"';
					$resultArr[$heat][$order] = '"'.$hcode.$athleticNum . chr(10) . $sname. chr(10) .'('.$className.' - '.$classNum.')"';
					
			}
			else if($hr_flag == 1)
			{
				list($name, $order, $house_id, $color_code) = $result[$j];
				$resultArr[1][$order] = $name;
			}
			else if($cr_flag == 1)
			{
				list($name, $order, $class_id) = $result[$j];
				$resultArr[1][$order] = $name;
			}
			else
				$resultArr = "";
		}

		$maxGroup = 0;

		if(sizeof($resultArr) != 0)
		{
			$keys1 = array_keys($resultArr);
			rsort($keys1);
			$maxGroup = $keys1[0];
		}
		else
			$maxGroup = 0;

		for($j=1; $j<=$maxGroup; $j++)
		{
			if($hr_flag!=1 && $cr_flag!=1) {
				$content1 .= $lsports->Get_Lang_Heat($j).",";
				$utf_content .= $lsports->Get_Lang_Heat($j)."\t";
			}
			
			$tempGroup = $resultArr[$j];
			if(sizeof($tempGroup) != 0)
			{
				$keys2 = array_keys($tempGroup);
				rsort($keys2);
				$maxPos = $keys2[0];
			}
			else
				$maxPos = 0;

			for($k=1; $k<=$maxPos; $k++)
			{
				if($k>$numberOfLane && $k%$numberOfLane == 1)
				{
					$content1 .= "\n";
					$content1 .= ",";
					$content1 .= $tempGroup[$k];
					
					$utf_content .= "\r\n";
					$utf_content .= "\t";
					$utf_content .= $tempGroup[$k];					
				}
				else
				{
					$content1 .= $tempGroup[$k];
					$utf_content .= $tempGroup[$k];
				}

				if($k != $maxPos)
				{
					$content1 .= ",";
					$utf_content .= "\t";
				}
				else
				{
					$content1 .= "\n";
					$utf_content .= "\r\n";
				}
			}
		}
	}
	$content1 .= "\n";
	$utf_content .= "\r\n";
	unset($resultArr);
}

$filename = "lane-arrangement.csv";

//if (!$g_encoding_unicode) {
	#$export_content .= stripslashes($content1);
	//$export_content = $content1;	
	
	// Output the file to user browser
	//output2browser($export_content, $filename);
//} else {
	#$export_content .= stripslashes($content1);
	
	// Output the file to user browser
	$lexport->EXPORT_FILE($filename, $utf_content);
//}



intranet_closedb();
?>
