<?php
# using: yat
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageRaceResult";

$lclass = new libclass();
$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

$AgeGroups = $lswimminggala->retrieveAgeGroupInfo();	# retrieve all age group ids and names
$HouseInfo = $lswimminggala->retrieveHouseInfo();		# retrieve all houses info

for($i=0; $i<sizeof($AgeGroups); $i++)
{
	$groupID = $AgeGroups[$i][0];
	$groupName = $intranet_session_language=="en"?$AgeGroups[$i][2]:$AgeGroups[$i][3];
	$groupGender = $AgeGroups[$i][4];
	
	if($groupGender=="M")
	{
		$MGroup[] = $groupID;
		$table1GroupsHeader .= "<td class='tablebluetop tabletopnolink' align='center'>".$groupName."</td>";
	}
	else if($groupGender=="F")
	{
		$FGroup[] = $groupID;
		$table2GroupsHeader .= "<td class='tablebluetop tabletopnolink' align='center'>".$groupName."</td>";
	}
}


if($sys_custom['eSports_count_open_event_in_grade_score']!==true )
{
	# add open group
	$MGroup[] = '-1';
	$FGroup[] = '-2';
	
	$table1GroupsHeader .= "<td class='tablebluetop tabletopnolink' align='center'>".$i_Sports_Event_Boys_Open."</td>";
	$table2GroupsHeader .= "<td class='tablebluetop tabletopnolink' align='center'>".$i_Sports_Event_Girls_Open."</td>";
}
else
{
	$includeOpenEventScore = 1;
}

$HouseScoreSumArr = $lswimminggala->retrieveHouseScoreSumArr("","",$includeOpenEventScore);

### Table 1 (Boy)
$table1 = "";
$table1 .= "<tr>";
$table1 .= "<td>&nbsp;</td>";
$table1 .= $table1GroupsHeader;
//$table1 .= "<td class='tablebluetop tabletoplink' align='center'>".$i_Sports_Event_Boys_Open."</td>";
$table1 .= "<td class='tablebluetop tabletoplink' align='center'>".$i_Sports_field_Total_Score."</td>";
$table1 .= "</tr>";

//$MGroup[] = '-1';

$table_content = "";
$gender = "M";
for($i=0; $i<sizeof($HouseInfo); $i++)
{
	$houseID = $HouseInfo[$i][0];
	$houseName = ($intranet_session_language=="en") ? $HouseInfo[$i][2] : $HouseInfo[$i][3];
	$colorCode = $HouseInfo[$i][4];
	$maleHouseTotal = 0;

        $table_content .= "<tr class='tablebluerow".($i%2?"2":"1")."'>";
	$table_content .= "<td class='tablebluelist tabletext' align='center'>".$lswimminggala->house_flag2($colorCode,$houseName)."</td>";
	
	for($k=0; $k<sizeof($MGroup); $k++)
	{
		$ageGroupID = $MGroup[$k];

		$score = round($HouseScoreSumArr[$houseID][$ageGroupID],1);
//		$score = $lswimminggala->retrieveHouseScoreSum($houseID, $ageGroupID);

		$table_content .= "<td class='tabletext' align='center'>".$score."</td>";
		$maleHouseTotal = $maleHouseTotal + $score;

	}

	$table_content .= "<td class='tabletext' align='center'>".$maleHouseTotal."</td>";
	$table_content .= "</tr>";
	
	$allMaleTotal[$houseID] = $maleHouseTotal;
}
$table1 .= $table_content;
### Table 1 (Boy) - End

### Table 2 (Girl) 
$table2 = "";
$table2 .= "<tr>";
$table2 .= "<td>&nbsp;</td>";
$table2 .=$table2GroupsHeader;
//for($i=0; $i<sizeof($AgeGroups); $i++)
//{
//	$groupID = $AgeGroups[$i][0];
//	$groupName = $intranet_session_language=="en"?$AgeGroups[$i][2]:$AgeGroups[$i][3];
//	$groupGender = $AgeGroups[$i][4];
//	
//	if($groupGender=="F")
//	{
//		$FGroup[] = $groupID;
//		$table2 .= "<td class='tablebluetop tabletoplink' align='center'>".$groupName."</td>";
//	}
//}
//$table2 .= "<td class='tablebluetop tabletoplink' align='center'>".$i_Sports_Event_Girls_Open."</td>";
$table2 .= "<td class='tablebluetop tabletoplink' align='center'>".$i_Sports_field_Total_Score."</td>";
$table2 .= "</tr>";
//$FGroup[] = '-2';
$table_content = "";
$gender = "F";
for($i=0; $i<sizeof($HouseInfo); $i++)
{
	$houseID = $HouseInfo[$i][0];
	$houseName = ($intranet_session_language=="en") ? $HouseInfo[$i][2] : $HouseInfo[$i][3];
	$colorCode = $HouseInfo[$i][4];
	$femaleHouseTotal = 0;
	
        $table_content .= "<tr class='tablebluerow".($i%2?"2":"1")."'>";
        $table_content .= "<td class='tablebluelist tabletext' align='center'>".$lswimminggala->house_flag2($colorCode,$houseName)."</td>";

	for($k=0; $k<sizeof($FGroup); $k++)
	{
		$ageGroupID = $FGroup[$k];

		$score = round($HouseScoreSumArr[$houseID][$ageGroupID],1);
//		$score = $lswimminggala->retrieveHouseScoreSum($houseID, $ageGroupID);

		$table_content .= "<td class='tabletext' align='center'>".$score."</td>";
		$femaleHouseTotal = $femaleHouseTotal + $score;
	}
	$table_content .= "<td class='tabletext' align='center'>".$femaleHouseTotal."</td>";
	$table_content .= "</tr>";
	
	$allFemaleTotal[$houseID] = $femaleHouseTotal;
}
$table2 .= $table_content;
### Table 2 (Girl) - End

### Table 3 (Mixed) 
if($sys_custom['eSports_count_open_event_in_grade_score']!==true )
{
	$table3 = "";
	$table3 .= "<tr>";
	$table3 .= "<td>&nbsp;</td>";
	
	$table3 .= "<td class='tablebluetop tabletoplink' align='center'>".$i_Sports_field_Total_Score."</td>";
	$table3 .= "</tr>";
	
	$MGroup[] = '-4';
	$table_content = "";
	for($i=0; $i<sizeof($HouseInfo); $i++)
	{
		$houseID = $HouseInfo[$i][0];
		$houseName = ($intranet_session_language=="en") ? $HouseInfo[$i][2] : $HouseInfo[$i][3];
		$colorCode = $HouseInfo[$i][4];
		
	  $table_content .= "<tr class='tablebluerow".($i%2?"2":"1")."'>";
	  $table_content .= "<td class='tablebluelist tabletext' align='center'>".$lswimminggala->house_flag2($colorCode,$houseName)."</td>";
	
		$mixedTotal = $lswimminggala->retrieveHouseScoreSum($houseID, -4);
	
		$table_content .= "<td class='tabletext' align='center'>".$mixedTotal."</td>";
		$table_content .= "</tr>";
		
		$allMixedTotal[$houseID] = $mixedTotal;
	}
	$table3 .= $table_content;
}
### Table 3 (Mixed) - End

### Table 4 (Total)
$table4 = ""; 
for($i=0; $i<sizeof($HouseInfo); $i++)
{
	$houseName = ($intranet_session_language=="en") ? $HouseInfo[$i][2] : $HouseInfo[$i][3];
	$table4 .= "<td class='tablebluetop tabletopnolink' align='center'>".$houseName."</td>";
}
$table4 .= "</tr>";
$table4 .= "<tr>";
for($i=0; $i<sizeof($HouseInfo); $i++)
{
	$houseID = $HouseInfo[$i][0];
	$colorCode = $HouseInfo[$i][4];
	$total = $allMaleTotal[$houseID] + $allFemaleTotal[$houseID] + $allMixedTotal[$houseID];

	$table4 .= "<td class='tabletext' align='center'>".$lswimminggala->house_flag2($colorCode, $total)."</td>";
}
$table4 .= "</tr>";
### Table 4 (Total) - End

### Title ###
$house_relay_name = $lswimminggala->retrieveEventTypeNameByID(3);
$class_relay_name = $lswimminggala->retrieveEventTypeNameByID(4);

$TAGS_OBJ[] = array($Lang['eSports']['Individual'],"../participation/tf_record.php", 0);
$TAGS_OBJ[] = array($house_relay_name,"../participation/relay_record.php", 0);
$TAGS_OBJ[] = array($class_relay_name,"../participation/class_relay_record.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Report_EventRanking,"../report/event_rank.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Report_HouseGroupScore,"../report/house_group.php", 1);
$TAGS_OBJ[] = array($i_Sports_menu_Report_GroupChampion,"../report/group_champ.php", 0);
$MODULE_OBJ = $lswimminggala->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>
<script language="javascript">
<!--
function click_print()
{
	newWindow('house_group_print.php',10);
}
function click_export()
{
	document.form1.submit();
}
//-->
</script>

<?=$linterface->GET_LNK_PRINT_IP25("javascript:click_print()");?>
<?=$linterface->GET_LNK_EXPORT_IP25("javascript:click_export()");?>

<form name="form1" action="house_group_export.php"></form>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>&nbsp;&nbsp;&nbsp;<?= $linterface->GET_NAVIGATION2($i_Sports_Event_Boys) ?></td>
</tr>
<tr>
	<td align="center">
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
                              	<table width="100%" border="0" cellspacing="0" cellpadding="4">
				<?=$table1?>
				</table>
			</td>
		</tr>
                <tr>
                	<td height='1' class='dotline'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1'></td>
                </tr>
                </table>
        </td>
</tr>
<tr>
	<td><br />&nbsp;&nbsp;&nbsp;<?= $linterface->GET_NAVIGATION2($i_Sports_Event_Girls) ?></td>
</tr>
<tr>
	<td align="center">
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
                              	<table width="100%" border="0" cellspacing="0" cellpadding="4">
				<?=$table2?>
				</table>
			</td>
		</tr>
                <tr>
                	<td height='1' class='dotline'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1'></td>
                </tr>
                </table>
        </td>
</tr>
<tr>
	<td><br />&nbsp;&nbsp;&nbsp;<?= $linterface->GET_NAVIGATION2($i_Sports_Event_Mixed_Open) ?></td>
</tr>
<tr>
	<td align="center">
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
                              	<table width="100%" border="0" cellspacing="0" cellpadding="4">
				<?=$table3?>
				</table>
			</td>
		</tr>
                <tr>
                	<td height='1' class='dotline'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1'></td>
                </tr>
                </table>
        </td>
</tr>
<tr>
	<td><br />&nbsp;&nbsp;&nbsp;<?= $linterface->GET_NAVIGATION2($i_Sports_field_Total_Score) ?></td>
</tr>
<tr>
	<td align="center">
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
                              	<table width="100%" border="0" cellspacing="0" cellpadding="4">
				<?=$table4?>
				</table>
			</td>
		</tr>
                <tr>
                	<td height='1' class='dotline'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1'></td>
                </tr>
                </table>
        </td>
</tr>
</table>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
