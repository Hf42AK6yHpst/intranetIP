<?php
# using: 

########## Change Log [Start] ############
#
#	Date:	2017-11-03 (Bill)
#			fixed: class relay score not correct (always the same)
#
########## Change Log [End] ############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");

intranet_auth();
intranet_opendb();

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

# Get Record & Standard Record
$eventType = 4;
$ExtInfo = $lswimminggala->retrieveEventGroupExtInfo($eventGroupID, $eventType);
$record = ($ExtInfo["RecordMin"]*60*100) + ($ExtInfo["RecordSec"]*100) + $ExtInfo["RecordMs"];
$check_record = $record;
$standard = ($ExtInfo["StandardMin"]*60*100) + ($ExtInfo["StandardSec"]*100) + $ExtInfo["StandardMs"];

# Clear New Record
$table = "SWIMMINGGALA_EVENTGROUP_EXT_RELAY";
$updateFields = "NewRecordMin = NULL, NewRecordSec = NULL, NewRecordMs = NULL ";
$orderType = "ASC";
$sql = "	UPDATE $table 
				SET $updateFields
			WHERE
				EventGroupID = $eventGroupID";
$lswimminggala->db_db_query($sql);

$arrangeDetail = $lswimminggala->returnCRLaneArrangeDetailByEventGroupID($eventGroupID);
for($i=0; $i<sizeof($arrangeDetail); $i++)
{
	list($cname_en,$cname_b5, $order, $cid) = $arrangeDetail[$i];
	$cname = Get_Lang_Selection($cname_en,$cname_b5);
	
	$result_min = (${"result_min_".$order}=="")?0:${"result_min_".$order};
	$result_sec = (${"result_sec_".$order}=="")?0:${"result_sec_".$order};
	$result_ms = (${"result_ms_".$order}=="")?0:${"result_ms_".$order};
	
	$result_min = (strlen($result_min)==1) ? "0".$result_min : $result_min;
	$result_sec = (strlen($result_sec)==1) ? "0".$result_sec : $result_sec;
	$result_ms = (strlen($result_ms)==1) ? "0".$result_ms : $result_ms;
	
	$result_record = ($result_min*60*100) + ($result_sec*100) + $result_ms;
	
	$classname[$order] = $cname;
	$resultArr[$order]["min"] = $result_min;
	$resultArr[$order]["sec"] = $result_sec;
	$resultArr[$order]["ms"] = $result_ms;
	
	$resultArr[$order]["other"] = ${"other_".$order};
	
	if($result_record != 0 && $resultArr[$order]["other"]!=4) {
		$Ranking[$order] = $result_record;
	}
}

if(sizeof($Ranking)==0)
{
	header ("Location:class_relay_record_detail.php?eventGroupID=$eventGroupID");
}

asort($Ranking);
$ranked_cid = array_keys($Ranking);

$rank = 0;
foreach($Ranking as $order => $result_record)
{
	$result_min = $resultArr[$order]["min"];
	$result_sec = $resultArr[$order]["sec"];
	$result_ms = $resultArr[$order]["ms"];
	$cname = $classname[$order];
	
	$other = $resultArr[$order]["other"];
	if($other!=3 && $other!=4)
	{
		# Retrieve Updated New Record for Compare
		$sql = "SELECT NewRecordMin, NewRecordSec, NewRecordMs FROM SWIMMINGGALA_EVENTGROUP_EXT_RELAY Where EventGroupID = '$eventGroupID'";
		$newRecord = $lswimminggala->returnArray($sql, 3);
		$new = ($newRecord[0][0]*60*100) + ($newRecord[0][1]*100) + $newRecord[0][2];
		$check_record = ($new==0) ? $check_record : $new;
		if($result_record < $check_record)
		{
			$status = 4;

			# Update Broken Record Time
			$sql = "UPDATE SWIMMINGGALA_EVENTGROUP_EXT_RELAY SET NewRecordMin = '$result_min', NewRecordSec = '$result_sec', NewRecordMs = '$result_ms', NewRecordHolderName = '$cname' WHERE EventGroupID = '$eventGroupID'";
			$lswimminggala->db_db_query($sql);
		}
		else if($result_record <= $standard)
		{
			$status = 3;
		}
		else
		{
			$status = 2;
		}
	}
	else if($other==1)
	{
		$status = 4;
	}
	else if($other==2)
	{
		$status = 3;
	}
	else if($other==3)
	{
		$status = 2;
	}
	
	$resultArr[$order]["status"] = $status;
	
	// add by marcus 17/6
	$rank++;
	if($last_cid!="" && $Ranking[$last_cid]==$result_record) 	// Check if record same as neighbour
	{
		$resultArr[$order]["rank"] = $last_rank;
	}
	else
	{
		$resultArr[$order]["rank"] = $rank;
		$last_rank = $rank;
	}
	$last_cid = $order;
}

# Retrieve Score Standard 
$sql = "SELECT ScoreStandardID FROM SWIMMINGGALA_EVENTGROUP WHERE EventGroupID = '$eventGroupID'";
$temp = $lswimminggala->returnVector($sql);
$ScoreStandardDetail = $lswimminggala->retrieveScoreStandardDetail($temp[0]);

foreach($resultArr as $order => $result_record)
{
	/* modified by marcus 17/6
	for($i=0; $i<sizeof($ranked_cid); $i++)
	{
		if($order == $ranked_cid[$i])
		{
			$rank = $i+1;
			$result_record["rank"] = $rank;
			break;
		}
	}

	$result_record["rank"] = ($result_record["rank"]=="") ? "NULL" : $result_record["rank"];
	*/
	
	// Assign Rank
	$result_record["rank"] = $resultArr[$order]["rank"];

	# Calculate Score by Ranking
	if($ScoreStandardDetail[$result_record["rank"]]!="")
		$score = $ScoreStandardDetail[$result_record["rank"]];
	else
		$score = 0;
	
	# Calculate Score by Status
	if($result_record["status"]==3)
		$score = $score + $ScoreStandardDetail[10];
	else if($result_record["status"]==4)
		$score = $score + $ScoreStandardDetail[9];
	
	$result_record["score"] = $score;
	if($result_record["status"]=="")
	{
		$result_record["status"] = ($result_record["other"]==4) ? 5 : 1;
	}
	
	$values = "ResultMin = ".$result_record["min"].", ";
	$values .= "ResultSec = ".$result_record["sec"].", ";
	$values .= "ResultMs = ".$result_record["ms"].", ";
	$values .= "RecordStatus = ".$result_record["status"].", ";
	$values .= "Rank = ".$result_record["rank"].", ";
	$values .= "Score = ".$result_record["score"];

	$sql = "UPDATE SWIMMINGGALA_CLASS_RELAY_LANE_ARRANGEMENT SET $values WHERE EventGroupID = '$eventGroupID' AND ArrangeOrder = '$order'";
	$lswimminggala->db_db_query($sql);
}

##########
# status
# 1 - absent
# 2 - Unsuitable
# 3 - Qualified
# 4 - Record Broken
# 5 - Foul
####################

intranet_closedb();
header ("Location:class_relay_record_detail.php?eventGroupID=$eventGroupID&xmsg=update");
?>