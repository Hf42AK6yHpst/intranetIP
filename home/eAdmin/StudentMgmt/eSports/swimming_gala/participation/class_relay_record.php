<?php
# using: 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageRaceResult";

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

$CREvents = $lswimminggala->retrieveClassRelayName();		# retrieve class relay eventgroup ids and names
//$AgeGroups = $lswimminggala->retrieveAgeGroupIDNames();	# retrieve all age group ids and names
# Add Boys Open and Girls Open 
$openBoy[0] = '-1';
$openBoy[1] = $i_Sports_Event_Boys_Open;
$openGirl[0] = '-2';
$openGirl[1] = $i_Sports_Event_Girls_Open;
$openMixed[0] = '-4';
$openMixed[1] = $i_Sports_Event_Mixed_Open;
$AgeGroups[] = $openBoy;
$AgeGroups[] = $openGirl;
$AgeGroups[] = $openMixed;

for($i=0; $i<sizeof($AgeGroups); $i++)
{
	list($gid, $gname) = $AgeGroups[$i];
	$groupArr[$gid] = $gname;
}

### age group
$ageGroup = "";
$ageGroup .= "<tr>";
$ageGroup .= "<td width='100'>&nbsp;</td>";
for($i=0; $i<sizeof($AgeGroups); $i++)
	$ageGroup .= "<td class='tabletop tabletoplink' align='center'>".$AgeGroups[$i][1]."</td>";
$ageGroup .= "</tr>";

//tablelist
### list
$typeIDs = array(4);
$currEvent = "";
$list = "";
$tr = 0;
for($j=0; $j<sizeof($CREvents); $j++)
{
	list($event_id, $group_id, $event_name) = $CREvents[$j];
        if($event_id==$currEvent)	continue;
        $currEvent = $event_id;
        
        $list .= "<tr class='tablerow".($tr%2?"2":"1")."'>";
	$list .= "<td class='tablelist'>". $event_name."</td>";
        for($k=0; $k<sizeof($AgeGroups); $k++)
	{
		$group_id = $AgeGroups[$k][0];
		
                $eventGroupInfo= $lswimminggala->retrieveEventGroupID($event_id, $group_id, $typeIDs);
                
                if(sizeof($eventGroupInfo) !=0)
                {
                	$egid 		= $eventGroupInfo[0];
                    $arranged 	= $lswimminggala->returnCRLaneArrangedFlag($egid);
					if($arranged>0)
					{
						$editBtn 	= "<a href=\"class_relay_record_detail.php?eventGroupID=$egid\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /></a>";
                        $list .= "<td align='center' class='tabletext'>$editBtn</td>";
					}
					else
						$list .= "<td>&nbsp;</td>";
		} else { 
			$list .= "<td>&nbsp;</td>";
		}
	}
        
        $list .= "</tr>";
	$tr++;
}

$house_relay_name = $lswimminggala->retrieveEventTypeNameByID(3);
$class_relay_name = $lswimminggala->retrieveEventTypeNameByID(4);

### Title ###
$TAGS_OBJ[] = array($Lang['eSports']['Individual'],"../participation/tf_record.php", 0);
$TAGS_OBJ[] = array($house_relay_name,"../participation/relay_record.php", 0);
$TAGS_OBJ[] = array($class_relay_name,"../participation/class_relay_record.php", 1);
$TAGS_OBJ[] = array($i_Sports_menu_Report_EventRanking,"../report/event_rank.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Report_HouseGroupScore,"../report/house_group.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Report_GroupChampion,"../report/group_champ.php", 0);
$MODULE_OBJ = $lswimminggala->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<br />

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center"><br />
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
                              	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                	<td>
                                                <table width='100%' border='0' cellspacing='0' cellpadding='4'>
                                                <?=$ageGroup?>
                                                <?=$list?>
						</table>
					</td>
				</tr>
                                <tr>
                                	<td height='1' class='dotline'><img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif' width='10' height='1'></td>
                                </tr>
				</table>
			</td>
		</tr>
                </table>
        </td>
</tr>
</table>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
