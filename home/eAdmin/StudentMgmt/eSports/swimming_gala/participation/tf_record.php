<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageRaceResult";

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

$AgeGroups = $lswimminggala->retrieveAgeGroupIDNames();	# retrieve all age group ids and names
# Add Boys Open and Girls Open 
$openBoy[0] = '-1';
$openBoy[1] = $i_Sports_Event_Boys_Open;
$openGirl[0] = '-2';
$openGirl[1] = $i_Sports_Event_Girls_Open;
$openMixed[0] = '-4';
$openMixed[1] = $i_Sports_Event_Mixed_Open;
$AgeGroups[] = $openBoy;
$AgeGroups[] = $openGirl;
$AgeGroups[] = $openMixed;

$TREvents = $lswimminggala->retrieveTrackFieldEventName();		# retrieve all track and field eventgroup ids and names
$numberOfLane = $lswimminggala->numberOfLanes;

# Create a new array to store the track and field results
for($i=0; $i<sizeof($TREvents); $i++)
{
	list($event_id, $event_name) = $TREvents[$i];

	for($j=0; $j<sizeof($AgeGroups); $j++)
	{
		list($group_id, $group_name) = $AgeGroups[$j];
		
		$typeIDs = array(1, 2);
		# Retrieve event group id
		$eventGroupInfo= $lswimminggala->retrieveEventGroupID($event_id, $group_id, $typeIDs);	

		$eventGroupID = $eventGroupInfo[0];
		$eventType = $eventGroupInfo[1];
		
		if($eventGroupID != "")
		{
			# Retrieve Number of Participants
			$arrangedCount = $lswimminggala->retrieveTFLaneArrangedEventCount($eventGroupID);

			$TrackFieldResult[$event_id][$group_id]["eg_id"] = $eventGroupID;
			$TrackFieldResult[$event_id][$group_id]["event_type"] = $eventType;
			$TrackFieldResult[$event_id][$group_id]["arranged"] = $arrangedCount;
		}
	}
}

### age group
$ageGroup = "";
$ageGroup .= "<tr>";
$ageGroup .= "<td width='100'>&nbsp;</td>";
for($i=0; $i<sizeof($AgeGroups); $i++)
	$ageGroup .= "<td class='tabletop tabletoplink' align='center'>".$AgeGroups[$i][1]."</td>";
$ageGroup .= "</tr>";

//tablelist
### list
$list = "";
for($j=0; $j<sizeof($TREvents); $j++)
{
	list($event_id, $event_name) = $TREvents[$j];
	
	$list .= "<tr class='tablerow".($j%2?"2":"1")."'>";
	$list .= "<td class='tablelist'>".$event_name."</td>";
	
	for($k=0; $k<sizeof($AgeGroups); $k++)
	{
		$group_id = $AgeGroups[$k][0];
		
		if(sizeof($TrackFieldResult[$event_id][$group_id]) != 0)
		{
			$eventGroupID = $TrackFieldResult[$event_id][$group_id]["eg_id"];
			$event_type = $TrackFieldResult[$event_id][$group_id]["event_type"];
			$arranged = $TrackFieldResult[$event_id][$group_id]["arranged"];
			$Ranking = $lswimminggala->retrieveTFRanking($event_id, $group_id);
			//added by marcus 
			$indicator="<font style='color:red'>*</font>"; //non complete event indicator
			
			#Check whether the Event has been set up
			$is_setup = $lswimminggala->checkExtInfoExist($event_type, $eventGroupID);

			if($is_setup == 1)
			{				
				if($arranged != 0)
				{
					
					$tr_display = ((sizeof($Ranking)==0)?$indicator:"")."<a class='tablelink' href='input_tf_record.php?eventGroupID=$eventGroupID'>".$arranged."</a>";
				}
				else
				{
					$tr_display = "0";
				}
				$list .= "<td class='tabletext' align='center'>".$tr_display."</td>";
			}
			else
				$list .= "<td class='tabletext' align='center'>- -</td>";
		}
		else
			$list .= "<td>&nbsp;</td>";
	}
	$list .= "</tr>";
}

# Export Btn 
if($sys_custom['eSports_participant_certificate'] === true)
	$ExportCertBtn 	= $linterface->GET_ACTION_BTN($Lang['eSports']['ExportParticipateCertificate'], "button","click_export('enrol_export.php');");


### Title ###
$house_relay_name = $lswimminggala->retrieveEventTypeNameByID(3);
$class_relay_name = $lswimminggala->retrieveEventTypeNameByID(4);

$TAGS_OBJ[] = array($Lang['eSports']['Individual'],"../participation/tf_record.php", 1);
$TAGS_OBJ[] = array($house_relay_name,"../participation/relay_record.php", 0);
$TAGS_OBJ[] = array($class_relay_name,"../participation/class_relay_record.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Report_EventRanking,"../report/event_rank.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Report_HouseGroupScore,"../report/house_group.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Report_GroupChampion,"../report/group_champ.php", 0);
$MODULE_OBJ = $lswimminggala->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<br />

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center"><br />
        	<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
                              	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                	<td>
                                                <table width='100%' border='0' cellspacing='0' cellpadding='4'>
                                                <?=$ageGroup?>
                                                <?=$list?>
						</table>
					</td>
				</tr>
                                <tr>
                                	<td height='1' class='dotline'><img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif' width='10' height='1'></td>
                                </tr>
                                <tr>
                                	<td class="tabletextremark">(<?=$i_Sports_Explain?>: - - <?=$i_Sports_Item_Without_Setting_Meaning?> , <?=$indicator?> <?=$i_Sports_Not_Complete_Events_Meaning?>)</td>
				</tr>
				</table>
			</td>
		</tr>
                </table>
        </td>
</tr>
</table>
<form name="form2" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align='center'> <?=$ExportCertBtn?></td>
</tr>
</table>
<input type="hidden" name="exportFileName" value="event">
<input type="hidden" name="exportContent" value="<?=$export_content?>">
</form>
<br />

<SCRIPT>
function click_export(action)
{
	var obj = document.form2;
	obj.action=action;
	obj.submit();
	
}
</SCRIPT>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
