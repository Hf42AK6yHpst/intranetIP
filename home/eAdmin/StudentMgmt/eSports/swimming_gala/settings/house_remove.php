<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

# Get House info
$list = implode(",",$HouseID);
if($list!=""){
	$sql = "DELETE FROM INTRANET_HOUSE WHERE HouseID IN ($list)";
	$lswimminggala->db_db_query($sql);
}

intranet_closedb();
header("Location: house.php?xmsg=delete");
?>

