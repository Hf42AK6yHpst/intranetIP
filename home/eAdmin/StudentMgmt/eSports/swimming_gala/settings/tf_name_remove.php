<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");

intranet_auth();
intranet_opendb();

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

$list = implode(",",$EventID);

$sql = "DELETE FROM SWIMMINGGALA_EVENT WHERE EventID IN ($list)";
$lswimminggala->db_db_query($sql);

intranet_closedb();
header("Location: tf_name.php?xmsg=delete");
?>