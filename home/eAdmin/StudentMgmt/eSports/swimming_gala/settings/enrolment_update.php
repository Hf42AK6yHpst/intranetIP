<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

$date_start = $_POST['date_start'];
$date_end = $_POST['date_end'];
$num_track = $_POST['num_track'];
$detail = intranet_htmlspecialchars($_POST['detail']);

$lswimminggala->enrolDateStart = $date_start;
$lswimminggala->enrolDateEnd = $date_end;
$lswimminggala->enrolMaxTrack = $num_track;
$lswimminggala->enrolDetails = $detail;
$lswimminggala->saveSettings();

intranet_closedb();
header("Location: enrolment.php?xmsg=update");
?>