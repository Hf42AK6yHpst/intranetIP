<?php

//using :marcus
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

# Get House info
$lswimminggala 	= new libswimminggala();
$lswimminggala->authSportsSystem();
$linterface 	= new interface_html();
$CurrentPage	= "PageGeneralSettings";

$GroupID = (is_array($GroupID)? $GroupID[0]:$GroupID);

//$house_detail = $lsports->getHouseDetail($HouseID);
$house_detail = $lswimminggala->getHouseDetailByGroupID($GroupID);
$new = empty($house_detail)?1:0;
list ($linkGroup, $engName, $chiName, $dOrder, $dColor, $dCode) = $house_detail;

$sql = "SELECT Title FROM INTRANET_GROUP WHERE RecordType = 4 AND GroupID=$GroupID";
$groups = $lswimminggala->returnVector($sql);

$sql = "SELECT COUNT(*) FROM INTRANET_GROUP WHERE RecordType = 4";
$temp = $lswimminggala->returnVector($sql);
$count = $temp[0];
$select_order = "<SELECT name=houseOrder>\n";
for ($i=1; $i<=$count; $i++)
{
     $select_order .= "<OPTION value='$i' ".($dOrder==$i?"SELECTED":"").">$i</OPTION>\n";
}
$select_order .= "</SELECT>\n";




### Title ###
$TAGS_OBJ[] = array($i_Sports_menu_Settings_House,"house.php", 1);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_Score,"score.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_Lane,"lane.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_LaneSet,"laneset.php", 0);
$TAGS_OBJ[] = array($i_Sports_menu_Settings_ParticipantFormat,"participant.php", 0);
$TAGS_OBJ[] = array($Lang['eSports']['Arrangement_Schedule_Setting'],"arrangement_schedule_setting.php", 0);
$MODULE_OBJ = $lswimminggala->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_edit.($intranet_session_language=="en"?" ":""). $i_Sports_menu_Settings_House, "");

?>

<SCRIPT LANGUAGE="Javascript">
<!--
function checkForm(obj)
{
	if(!check_text(obj.engName, "<?=$i_alert_pleasefillin.$i_House_name ."(".$i_general_english.")"?>")) return false;
	if(!check_text(obj.chiName, "<?=$i_alert_pleasefillin.$i_House_name ."(".$i_general_chinese.")"?>")) return false;
	if(!check_text(obj.houseCode, "<?=$i_alert_pleasefillin.$i_House_HouseCode?>")) return false;
	
	if(<?=$plugin["Sports"]?1:0?>)
		if(!confirm("<?=$i_Sports_Edit_House_Alert?>"))
			return false;
}
//-->
</SCRIPT>

<br />   
<form name="form1" method="post" action="house_edit_update.php" onSubmit="return checkForm(this);">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG("",$xmsg);?></td>
</tr>
<tr>
	<td colspan="2">
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                	<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_House_GroupLink?> </span></td>
					<td><?=$groups[0]?></td>
				</tr>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?="$i_House_name ($i_general_english)"?> <span class='tabletextrequire'>*</span></span></td>
					<td><input name="engName" type="text" class="textboxtext" maxlength="255" value="<?=$engName?>"/></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?="$i_House_name ($i_general_chinese)"?> <span class='tabletextrequire'>*</span></span></td>
					<td><input name="chiName" type="text" class="textboxtext" maxlength="255" value="<?=$chiName?>"/></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_general_DisplayOrder?> </span></td>
					<td><?=$select_order?></td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_House_ColorCode?> </span></td>
					<td>
                                        <table>
                                	<tr>
                                	<td><input type="text" name="colorCode" value="<?=$dColor?>" class="textboxnum" value="<?=$colorCode?>"></td>
                                	<td id="colorpreview" name="colorpreview" width="20" bgcolor="#<?=$dColor?>" onClick="javascript:newWindow('select_color.php?rgb=<?=$colorCode?>&formname=form1&colorpreview=colorpreview&rgbinput=colorCode', 11)" style="CURSOR: hand">&nbsp;</td>
                                	<td class="tablelink">
                                        <?= $linterface->GET_BTN($button_select, "button", "javascript:newWindow('select_color.php?rgb=$dColor&formname=form1&colorpreview=colorpreview&rgbinput=colorCode', 11)","selectbtn") ?>
                                        </td>
                                	</tr>
                                	</table>
                                        </td>
				</tr>
                                <tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?="$i_House_HouseCode"?> <span class='tabletextrequire'>*</span></span></td>
					<td><input name="houseCode" type="text" class="textboxtext" maxlength="255" value="<?=$dCode?>" /></td>
				</tr>
			</table>
			</td>
                </tr>
                </table>
	</td>
</tr>
<tr>
	<td colspan="2">        
                <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
		</tr>
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_save, "submit", "","submit2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
                                <?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='house.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			</td>
		</tr>
                </table>                                
	</td>
</tr>
</table>                        
<br />

<input type="hidden" name="GroupID" value="<?=$GroupID?>" />
<input type="hidden" name="new" value="<?=$new?>"/>
</form>

<?
intranet_closedb();
print $linterface->FOCUS_ON_LOAD("form1.engName");
$linterface->LAYOUT_STOP();
?>

