<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

$list = implode(",",$ID);
if($list!=""){
	$sql = "DELETE FROM SWIMMINGGALA_AGE_GROUP WHERE AgeGroupID IN ($list)";
	$lswimminggala->db_db_query($sql);
}

intranet_closedb();
header("Location: agegroup.php?xmsg=delete");
?>