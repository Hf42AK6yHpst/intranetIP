<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lsports = new libswimminggala();
$lsports->authSportsSystem();

# Get House info
$new = $_POST['new'];
$GroupID = $_POST['GroupID'];
$engName = $_POST['engName'];
$chiName = $_POST['chiName'];
$houseOrder = $_POST['houseOrder'];
$houseGroup = $_POST['houseGroup'];
$colorCode = $_POST['colorCode'];
$houseCode = $_POST['houseCode'];

//modified by marcus 17/7/2009
if($new)
{
	$sql = "	INSERT INTO INTRANET_HOUSE 
					(
						GroupID, 
						EnglishName, 
						ChineseName, 
						DisplayOrder, 
						ColorCode, 
						HouseCode
					)
				VALUES
					(
						'$GroupID',
						'$engName',
						'$chiName',
						'$houseOrder',
						'$colorCode',
						'$houseCode'
					)
					";
}
else
{
	$sql = "UPDATE INTRANET_HOUSE SET EnglishName = '$engName'
               , ChineseName = '$chiName'
               , DisplayOrder = '$houseOrder'
               , ColorCode = '$colorCode'
               , HouseCode = '$houseCode'
               WHERE GroupID = '$GroupID'";
}

$lsports->db_db_query($sql);
/*if($lsports->db_affected_rows()>0)
	$xmsg = "update";
else
	$xmsg = "update_failed";
*/
intranet_closedb();
header("Location: house.php?xmsg=update");
?>