<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

$list = implode(",",$EventGroupID);

$sql = "DELETE FROM SWIMMINGGALA_EVENTGROUP WHERE EventGroupID IN ($list)";
$lswimminggala->db_db_query($sql);

intranet_closedb();
header("Location: tf_event.php?AgeGroupID=".$AgeGroupID."&eventID=".$eventID."&xmsg=delete");
?>