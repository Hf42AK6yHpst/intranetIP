<?php
# using: yat

#################################################
#
#	Date:	2012-06-08	YatWoon
#			add "Event Quota"
#
#################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

$EventGroupID = $_POST['EventGroupID'];
$EventTypeID = $_POST['EventTypeID'];

$AgeGroupID = $_POST['AgeGroupID'];

$onlineEnrol = $_POST['onlineEnrol'];
$Quota = $_POST['Quota'];
$countHouse = $_POST['countHouse'];
$countClass = $_POST['countClass'];
$countIndividual = $_POST['countIndividual'];
$scoreStandard = $_POST['scoreStandard'];
$RecordHouseID = $_POST['house'];
$RecordHolderName = $_POST['record_holder'];
$RecordYear = $_POST['record_year'];

#For Track and Relay Event Only
$RecordMin = $_POST['record_min'];
$RecordSec = $_POST['record_sec'];
$RecordMs = $_POST['record_ms'];
$StandardMin = $_POST['standard_min'];
$StandardSec = $_POST['standard_sec'];
$StandardMs = $_POST['standard_ms'];


$FirstRoundType = $_POST['FirstRoundType'];	# 0- Number of Personal Per Group; 1 - Number of Group
$personNum1 = $_POST['personNum1'];
$groupNum1 = $_POST['groupNum1'];
$FirstRoundRandom = $_POST['FirstRoundRandom'];	 # 1- Random; 0/NULL - Non Random
$FirstRoundDay = $_POST['race_day1'];
$SecondRoundReq = $_POST['SecondRoundReq'];	# 1 - Second Round Required; 0 - Does Not Required
$personNum2 = $_POST['personNum2'];
$groupNum2 = $_POST['groupNum2'];
$SecondRoundDay = $_POST['race_day2'];
$FinalRoundReq = $_POST['FinalRoundReq'];	# 1 - Final Round Required; 0 - Does Not Required
$personNum3 = $_POST['personNum3'];
$FinalRoundDay = $_POST['race_day3'];

$FirstRoundGroupCount = ($FirstRoundType == 0) ? $personNum1 : $groupNum1;

$eventQuota = $_POST['eventQuota'] ? $_POST['eventQuota'] : 0;

$sql = "UPDATE SWIMMINGGALA_EVENTGROUP SET IsOnlineEnrol = '$onlineEnrol',
                                CountPersonalQuota = '$Quota',
                                CountHouseScore = '$countHouse',
                                CountClassScore = '$countClass',
								CountIndividualScore = '$countIndividual',
								ScoreStandardID = '$scoreStandard',
								EventCode = '$event_code',
								EventQuota = '$eventQuota',
                                DateModified = now()
                            WHERE EventGroupID = $EventGroupID";
$lswimminggala->db_db_query($sql);

#Check whether the Ext Info of this Event Enrolment has been existed
$is_setup = $lswimminggala->checkExtInfoExist($EventTypeID, $EventGroupID);

if($EventTypeID == '1')		# Track Event
{
	if($is_setup == 1)
	{
		$valuefields = "RecordHolderName = '$RecordHolderName', ";
		$valuefields .= "RecordMin = '$RecordMin', ";
		$valuefields .= "RecordSec = '$RecordSec', ";
		$valuefields .= "RecordMs = '$RecordMs', ";
		$valuefields .= "RecordYear = '$RecordYear', ";
		$valuefields .= "RecordHouseID = '$RecordHouseID', ";
		$valuefields .= "StandardMin = '$StandardMin', ";
		$valuefields .= "StandardSec = '$StandardSec', ";
		$valuefields .= "StandardMs = '$StandardMs', ";
		$valuefields .= "FirstRoundType = '$FirstRoundType', ";
		$valuefields .= "FirstRoundGroupCount = '$FirstRoundGroupCount', ";
		$valuefields .= "FirstRoundRandom = '$FirstRoundRandom', ";
		$valuefields .= "FirstRoundDay = '$FirstRoundDay', ";
		$valuefields .= "SecondRoundReq = '$SecondRoundReq', ";

		if($SecondRoundReq == 1)
		{
			$valuefields .= "SecondRoundLanes = '$personNum2', ";
			$valuefields .= "SecondRoundGroups = '$groupNum2', ";
			$valuefields .= "SecondRoundDay = '$SecondRoundDay', ";
		}
		else
		{
			$valuefields .= "SecondRoundLanes = ' ', ";
			$valuefields .= "SecondRoundGroups = ' ', ";
			$valuefields .= "SecondRoundDay = ' ', ";
		}
		if($FinalRoundReq == 1)
		{
			$valuefields .= "FinalRoundNum = '$personNum3', ";
			$valuefields .= "FinalRoundDay = '$FinalRoundDay', ";
		}
		else
		{
			$valuefields .= "FinalRoundNum = ' ', ";
			$valuefields .= "FinalRoundDay = ' ', ";
		}
		$valuefields .= "FinalRoundReq = '$FinalRoundReq'";

		$sql = "UPDATE SWIMMINGGALA_EVENTGROUP_EXT_TRACK SET $valuefields WHERE EventGroupID = '$EventGroupID'";
		$lswimminggala->db_db_query($sql);
	}
	else
	{
		$fields = "(EventGroupID, RecordHolderName, RecordMin, RecordSec, RecordMs, RecordYear, RecordHouseID, StandardMin, StandardSec, StandardMs, FirstRoundType, FirstRoundGroupCount, FirstRoundRandom, FirstRoundDay, SecondRoundReq, SecondRoundLanes, SecondRoundGroups, SecondRoundDay, FinalRoundNum, FinalRoundDay, FinalRoundReq)";

		$values = "('$EventGroupID', ";
		$values .= "'$RecordHolderName', ";
		$values .= "'$RecordMin', ";
		$values .= "'$RecordSec', ";
		$values .= "'$RecordMs', ";
		$values .= "'$RecordYear', ";
		$values .= "'$RecordHouseID', ";
		$values .= "'$StandardMin', ";
		$values .= "'$StandardSec', ";
		$values .= "'$StandardMs', ";
		$values .= "'$FirstRoundType', ";
		$values .= "'$FirstRoundGroupCount', ";
		$values .= "'$FirstRoundRandom', ";
		$values .= "'$FirstRoundDay', ";
		$values .= "'$SecondRoundReq', ";
		if($SecondRoundReq == 1)
		{
			$values .= "'$SecondRoundLanes', ";
			$values .= "'$SecondRoundGroups', ";
			$values .= "'$SecondRoundDay', ";
		}
		else
		{
			$values .= "' ', ";
			$values .= "' ', ";
			$values .= "' ', ";
		}
		if($FinalRoundReq == 1)
		{
			$values .= "'$FinalRoundNum', ";
			$values .= "'$FinalRoundDay', ";
		}
		else
		{
			$values .= "' ', ";
			$values .= "' ', ";
		}
		$values .= "'$FinalRoundReq')";
		
		$sql = "INSERT INTO SWIMMINGGALA_EVENTGROUP_EXT_TRACK $fields VALUES $values";
		$lswimminggala->db_db_query($sql);
	}
}
else if($EventTypeID == '3' || $EventTypeID=='4')		# Relay Event
{
	if($is_setup == 1)
	{
		$valuefields = "RecordHolderName = '$RecordHolderName', ";
		$valuefields .= "RecordMin = '$RecordMin', ";
		$valuefields .= "RecordSec = '$RecordSec', ";
		$valuefields .= "RecordMs = '$RecordMs', ";
		$valuefields .= "RecordYear = '$RecordYear', ";
		$valuefields .= "RecordHouseID = '$RecordHouseID', ";
		$valuefields .= "StandardMin = '$StandardMin', ";
		$valuefields .= "StandardSec = '$StandardSec', ";
		$valuefields .= "StandardMs = '$StandardMs'";

		$sql = "UPDATE SWIMMINGGALA_EVENTGROUP_EXT_RELAY SET $valuefields WHERE EventGroupID = '$EventGroupID'";
		$lswimminggala->db_db_query($sql);
	}
	else
	{
		$fields = "(EventGroupID, RecordHolderName, RecordMin, RecordSec, RecordMs, RecordYear, RecordHouseID, StandardMin, StandardSec, StandardMs)";

		$values = "('$EventGroupID', ";
		$values .= "'$RecordHolderName', ";
		$values .= "'$RecordMin', ";
		$values .= "'$RecordSec', ";
		$values .= "'$RecordMs', ";
		$values .= "'$RecordYear', ";
		$values .= "'$RecordHouseID', ";
		$values .= "'$StandardMin', ";
		$values .= "'$StandardSec', ";
		$values .= "'$StandardMs')";
		
		$sql = "INSERT INTO SWIMMINGGALA_EVENTGROUP_EXT_RELAY $fields VALUES $values";
		$lswimminggala->db_db_query($sql);

	}
}
intranet_closedb();
header("Location: tf_event.php?eventID=".$eventID."&AgeGroupID=".$AgeGroupID."&xmsg=update");
?>