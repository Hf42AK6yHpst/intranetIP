<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

# Get House info
$numbertype = $_POST['numbertype'];

$cond1_1 = $_POST['cond1_1'];
$cond1_2 = $_POST['cond1_2'];
$autonum_len = $_POST['autonum_len'];

$cond2_1 = $_POST['cond2_1'];
$cond2_2 = $_POST['cond2_2'];
$cond2_3 = $_POST['cond2_3'];

$lswimminggala->numberGenerationType = $numbertype;
if ($numbertype==1)
{
    $lswimminggala->numberGenRule1 = $cond1_1;
    $lswimminggala->numberGenRule2 = $cond1_2;
    $lswimminggala->numberGenAutoLength = $autonum_len;
}
else
{
    $lswimminggala->numberGenRule1 = $cond2_1;
    $lswimminggala->numberGenRule2 = $cond2_2;
    $lswimminggala->numberGenRule3 = $cond2_3;
}

$lswimminggala->saveSettings();

intranet_closedb();
header("Location: participant.php?xmsg=update");
?>