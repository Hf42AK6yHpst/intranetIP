<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

 # Select of students list
//$sql = "SELECT UserID, UserID FROM INTRANET_USER WHERE RecordType = '2' AND RecordStatus IN (0,1,2)";
//$students = $lswimminggala->returnArray($sql,2);
//$a_students = build_assoc_array($students,$empty_name);
$students = $lswimminggala->Get_Enroled_Participant_List();

$numbertype = $lswimminggala->numberGenerationType;

if ($numbertype==1)
{
    $cond1_1 = $lswimminggala->numberGenRule1;
    $cond1_2 = $lswimminggala->numberGenRule2;
    $autonum_len = $lswimminggala->numberGenAutoLength;
	
	# define the initial generation number
	$autoNumber = 1;
}
else
{
    $cond2_1 = $lswimminggala->numberGenRule1;
    $cond2_2 =  $lswimminggala->numberGenRule2;
    $cond2_3 = $lswimminggala->numberGenRule3;
}

if($numbertype == 1)
{
		for($i=1; $i<3; $i++)
		{
			$conds = ${"cond1_".$i};
			
			switch($conds){

				case(1):
					$HouseCodeNeed = true;	
					$orderArr[] = 1;
					break;
				case(2):
					$DisplayOrderNeed = true;
					$orderArr[] = 2;
					break;
				case(3):
					$GroupCodeNeed = true;
					$orderArr[] = 3;
					break;
				default:
					$NoSelected = true;
			}
			
		}
		
	
		foreach($students as $t_student)
		{
			$sid = $t_student[0];
			
			if($HouseCodeNeed == true)
			{
				$num1 = $lswimminggala->retrieveHouseCode($sid);
			}

			if($DisplayOrderNeed == true)
			{
				$num2 = $lswimminggala->retrieveHouseDisplayOrder($sid);
			}

			if($GroupCodeNeed == true)
			{
				$num3 = $lswimminggala->retrieveGroupCode($sid);
			}
			
				$athlnum = "";
				for($i=0; $i<sizeof($orderArr); $i++)
				{
					$type = $orderArr[$i];
					$athlnum .=  ${"num".$type};
					$athlnum .= " ";
				}
				
				if($autonum_len == 2)
				{
					$a_num = (strlen($autoNumber) == 1) ? "0".$autoNumber : $autoNumber;
				}
				else if($autonum_len == 3)
				{
					if(strlen($autoNumber) == 1)
					{
						$a_num = "00".$autoNumber;
					}
					else if(strlen($autoNumber) == 2)
					{
						$a_num = "0".$autoNumber;
					}
					else
						$a_num = $autoNumber;
				}
				$athlnum .= $a_num;	
				$autoNumber++;
				
				# Check whether student record exist in SWIMMINGGALA_STUDENT_ENROL_INFO
				$sql = "SELECT COUNT(*) FROM SWIMMINGGALA_STUDENT_ENROL_INFO WHERE StudentID = '$sid'";
				$exist_flag = $lswimminggala->returnVector($sql);

				if($exist_flag[0] == 1)		# Update Athletic Number if record exist
				{
					$sql = "UPDATE SWIMMINGGALA_STUDENT_ENROL_INFO SET AthleticNum = '$athlnum', DateModified = now() WHERE StudentID = '$sid'";
					$lswimminggala->db_db_query($sql);
				}
				else		# Insert record is record not exist
				{
					$sql = "INSERT INTO SWIMMINGGALA_STUDENT_ENROL_INFO (StudentID, AthleticNum, TrackEnrolCount, DateModified) VALUES ('$sid', '$athlnum', 0, now())";
					$lswimminggala->db_db_query($sql);
				}
		}
}
else 
{
		for($i=1; $i<7; $i++)
		{
			$conds = ${"cond2_".$i};

			switch($conds){

				case(1):
					$HouseCodeNeed = true;	
					$orderArr[] = 1;
					break;
				case(2):
					$DisplayOrderNeed = true;
					$orderArr[] = 2;
					break;
				case(3):
					$GroupCodeNeed = true;
					$orderArr[] = 3;
					break;
				case(4):
					$ClassNameNeed = true;
					$orderArr[] = 4;
					break;
				case(5):
					$ClassNumberNeed = true;
					$orderArr[] =5;
					break;
				case(6):
					$ClassNameNumNeed = true;
					$orderArr[] = 6;
					break;
				default:
					$NoSelected = true;
			}
		}

		foreach($students as $t_student)
		{
			$sid = $t_student[0];
			
			if($HouseCodeNeed == true)
			{
				$num1 = $lswimminggala->retrieveHouseCode($sid);
			}

			if($DisplayOrderNeed == true)
			{				
				$num2 = $lswimminggala->retrieveHouseDisplayOrder($sid);
			}

			if($GroupCodeNeed == true)
			{
				$num3 = $lswimminggala->retrieveGroupCode($sid);
			}

			if($ClassNameNeed == true)
			{
				$num4 = $lswimminggala->retrieveClassName($sid);
			}

			if($ClassNumberNeed == true)
			{
				$num5 = $lswimminggala->retrieveClassNumber($sid);
			}

			if($ClassNameNumNeed == true)
			{
				$num6 = $lswimminggala->retrieveClassNameNumber($sid);
			}
				
			$athlnum = "";
			for($i=0; $i<sizeof($orderArr); $i++)
			{
				$type = $orderArr[$i];
				$athlnum .=  ${"num".$type};
				$athlnum .= " ";
			}

				# Check whether student record exist in SWIMMINGGALA_STUDENT_ENROL_INFO
				$sql = "SELECT COUNT(*) FROM SWIMMINGGALA_STUDENT_ENROL_INFO WHERE StudentID = '$sid'";
				$exist_flag = $lswimminggala->returnVector($sql);

				if($exist_flag[0] == 1)		# Update Athletic Number if record exist
				{
					$sql = "UPDATE SWIMMINGGALA_STUDENT_ENROL_INFO SET AthleticNum = '$athlnum', DateModified = now() WHERE StudentID = '$sid'";
					$lswimminggala->db_db_query($sql);
				}
				else		# Insert record is record not exist
				{
					$sql = "INSERT INTO SWIMMINGGALA_STUDENT_ENROL_INFO (StudentID, AthleticNum, TrackEnrolCount, DateModified) VALUES ('$sid', '$athlnum', 0, now())";
					$lswimminggala->db_db_query($sql);
				}
		}
}
intranet_closedb();
header("Location: participant_gen.php?xmsg=update");
?>