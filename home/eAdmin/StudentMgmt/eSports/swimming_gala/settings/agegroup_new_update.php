<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libswimminggala.php");

intranet_auth();
intranet_opendb();

$lswimminggala = new libswimminggala();
$lswimminggala->authSportsSystem();

# Get House info
$grade = $_POST['grade'];
$engName = $_POST['engName'];
$chiName = $_POST['chiName'];
$gender = $_POST['gender'];
$gradeCode = $_POST['gradeCode'];
$dobUp = ($_POST['dobUp']==""? 'null':"'".$_POST['dobUp']."'");
$dobLow = ($_POST['dobLow']==""? 'null':"'".$_POST['dobLow']."'");
$num_track = $_POST['num_track'];
$displayOrder = ($_POST['displayOrder']==""? 'null':"'".$_POST['displayOrder']."'");

$sql = "INSERT INTO SWIMMINGGALA_AGE_GROUP (
                    GradeChar, EnglishName, ChineseName, Gender, GroupCode, DOBUpLimit,
                    DOBLowLimit ,DisplayOrder, DateInput, DateModified,
					EnrolMinTrack, EnrolMaxTrack) VALUES
                    ('$grade','$engName','$chiName','$gender','$gradeCode',$dobUp,$dobLow,$displayOrder,
                    now(), now(),NULL,$num_track)";
$lswimminggala->db_db_query($sql);

intranet_closedb();
header("Location: agegroup.php?xmsg=add");
?>