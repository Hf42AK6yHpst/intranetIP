<?php
/*
 * 	Log
 * 	Date:	2018-03-12 Anna
 *          Create this file
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/SFOC/libmedallist.php");


include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");

intranet_auth();
intranet_opendb();

$libUser = new libuser($UserID);	
$linterface = new interface_html();
$libmedallist = new libmedallist();
	
	
// if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
// 		header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	
	$CurrentPage = "PageMgtMedalList";
	$CurrentPageArr['MedalList'] = 1;
	
	$MODULE_OBJ = $libmedallist->GET_MODULE_OBJ_ARR();
	
// 	if ($libmedallist->hasAccessRight($_SESSION['UserID'])) {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $TAGS_OBJ[] = array($MedalListMenu['medallist'], "", 1);
        $linterface->LAYOUT_START();

		if (is_array($RecordID)) {
		    $RecordID= $RecordID[0];
		}
		    
 		$MedalListArr = $libmedallist->getMedalListInfoByID($RecordID);
		$RecordID = $MedalListArr['RecordID'];
		$Year =  $MedalListArr['Year'];
		$LocationChi=  $MedalListArr['LocationChi'];
		$LocationEng =  $MedalListArr['LocationEng'];
		$GameID =  $MedalListArr['GameID'];
		$EventID=  $MedalListArr['EventID'];
		$SportID=  $MedalListArr['SportID'];
		$NameOfAthleteChi=  $MedalListArr['NameChi'];
		$NameOfAthleteEng=  $MedalListArr['NameEng'];
		$WebsiteCode  = $MedalListArr['WebsiteCode'];
		$Medal = $MedalListArr['Medal'];
		
		# page navigation (leave the array empty if no need)
		if ($RecordID != "") {
		    $PAGE_NAVIGATION[] = array($button_edit." ".$MedalListMenu['game'], "");
			$button_title = $button_save;
		} else {
		    $PAGE_NAVIGATION[] = array($button_new." ".$MedalListMenu['game'], "");
			$button_title = $button_submit;
		}
		
$GamesList = $libmedallist->getGamesList();
$GameSelectionBox =  returnSelection($GamesList,$GameID, "GameID", "id='GameID'");


$EventsNameAry= $libmedallist->getEventList($EventID);
$EventsName = $EventsNameAry[0][1];
// $EventSelectionBox =  returnSelection($EventsList, $EventID, "EventID", "id='EventID'");
					
$SportsNameAry = $libmedallist->getSportsList($SportID);
$SportsName = $SportsNameAry[0][1];
// $SportSelectionBox =  returnSelection($SportsList, $SportID, "SportID", "id='SportID'");

$MedalList = array();
$MedalList[] = array(1,Get_Lang_Selection('金牌','Gold'));
$MedalList[] = array(2,Get_Lang_Selection('銀牌','Silver'));
$MedalList[] = array(3,Get_Lang_Selection('銅牌','Bronze'));
$MedalSelectionBox =  returnSelection($MedalList, $Medal, "MedalID", "id='MedalID'");

?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js" type="text/javascript" charset="utf-8"></script>
<script src="/templates/jquery/jquery.inputselect.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.inputselect.css" rel="stylesheet" type="text/css">

<!-- <script src="./TextExt.js" type="text/javascript" charset="utf-8"></script> -->

<script src="/templates/jquery/jquery.autocomplete.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.autocomplete.css" rel="stylesheet" type="text/css">

<script language="javascript">
$(document).ready( function() {	
// 	js_Init_DND_Table();
	$('.inputselect').each(function(){
		var this_id = $(this).attr('id');
		if($(this).length > 0){
			$(this).autocomplete(
		      "ajax_get_sport_suggestion.php",
		      {
		  			delay:3,
		  			minChars:1,
		  			matchContains:1,
		  			extraParams: {'field':this_id},
		  			autoFill:false,
		  			overflow_y: 'auto',
		  			overflow_x: 'hidden',
		  			maxHeight: '200px'
		  		}
		    );
		}
	});
});

function FormSubmitCheck(obj)
{
	if(!check_text(obj.Year, "<?php echo $i_alert_pleasefillin.$eEnrollmentMenu['cat_title']; ?>.")) 
		return false;
	if(!check_text(obj.LocationChi, "<?php echo $i_alert_pleasefillin.$eEnrollmentMenu['cat_title']; ?>.")) 
		return false;
	if(!check_text(obj.LocationEng, "<?php echo $i_alert_pleasefillin.$eEnrollmentMenu['cat_title']; ?>.")) 
		return false;
	if(!check_text(obj.NameOfAthleteChi, "<?php echo $i_alert_pleasefillin.$eEnrollmentMenu['cat_title']; ?>.")) 
		return false;
	if(!check_text(obj.NameOfAthleteEng, "<?php echo $i_alert_pleasefillin.$eEnrollmentMenu['cat_title']; ?>.")) 
		return false;

	obj.submit();
}
</SCRIPT>
<form name="form1" action="Record_update.php" method="POST" enctype="multipart/form-data">
<br/>

<table width="100%" border="0" cellspacing="4" cellpadding="4">
<tr><td>
	<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
</td></tr>
<tr><td>

<table class="form_table_v30" >
	<tr>
		<td class="field_title"><?=$linterface->RequiredSymbol()?><?= $Lang['SFOC']['MedalList']['Year']?></td>
		<td><input size="10"  maxlength="4" type="number" id="Year" name="Year"  value="<?= htmlspecialchars($Year)?>" ></td>
	</tr>
	<tr>
		<td class="field_title"><?=$linterface->RequiredSymbol()?><?= $Lang['SFOC']['MedalList']['LocationChi']?></td>
		<td><input type="text" id="LocationChi"  size="40" name="LocationChi"  value="<?= htmlspecialchars($LocationChi)?>"></td>
	</tr>
	<tr>
		<td class="field_title"><?=$linterface->RequiredSymbol()?><?= $Lang['SFOC']['MedalList']['LocationEng']?></td>
		<td><input type="text" id="LocationEng"  size="40" name="LocationEng"  value="<?= htmlspecialchars($LocationEng)?>"></td>
	</tr>
	<tr>
		<td class="field_title"><?=$linterface->RequiredSymbol()?><?= $Lang['SFOC']['MedalList']['Game']?></td>
		<td><?php echo $GameSelectionBox;?></td>
	</tr>
									
	<tr>	
		<td class="field_title_short"><?=$linterface->RequiredSymbol()?>
			<span class="field_title"><?= $Lang['SFOC']['MedalList']['Event']?>
			</span>
		</td>
		<td>
			<input id="Event" name="Event" type="text" class="textboxtext inputselect" maxlength="64"  size="40" value="<?php echo $EventsName;?>"/>
		</td>
	</tr>
	<tr>	
		<td class="field_title"><?=$linterface->RequiredSymbol()?><?=$Lang['SFOC']['MedalList']['Sport']?></td>
		
		<td><input id="Sport" name="Sport" type="text" class="textboxtext inputselect"
									maxlength="128" size="40"  value="<?php echo $SportsName;?>"/></td>
	</tr>
	<tr>
		<td class="field_title"><?=$linterface->RequiredSymbol()?><?=$Lang['SFOC']['MedalList']['NameOfAthleteChi']?></td>
		<td><input type="text" id="NameOfAthleteChi" name="NameOfAthleteChi" size="40" value="<?= htmlspecialchars($NameOfAthleteChi)?>" ></td>
	</tr>
	<tr>
		<td class="field_title"><?=$linterface->RequiredSymbol()?><?=$Lang['SFOC']['MedalList']['NameOfAthleteEng']?></td>
		<td><input type="text" id="NameOfAthleteEng" name="NameOfAthleteEng" size="40" value="<?= htmlspecialchars($NameOfAthleteEng)?>" ></td>
	</tr>
	<tr>
		<td class="field_title"><?=$linterface->RequiredSymbol()?><?= $Lang['SFOC']['MedalList']['Medal']?></td>
		<td><?php echo $MedalSelectionBox;?></td>
	</tr>
	
	<tr>
		<td class="field_title"><?=$Lang['SFOC']['MedalList']['WebsiteCode']?></td>
		<td><input type="text" id="WebsiteCode" name="WebsiteCode" size="40" value="<?= htmlspecialchars($WebsiteCode)?>" ></td>
	</tr>

</table>

</td></tr>

</table>
<table width="98%" border="0" cellspacing="0" cellpadding="1" align="center">
<tr><td align="left" class="tabletextremark"><?=$i_general_required_field?></td></tr>
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">
<div style="padding-top: 5px">
<?= $linterface->GET_ACTION_BTN($button_title, "button", "javascript: FormSubmitCheck(document.form1);")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='index.php'")?>
</div>
</td></tr>
</table>
<br/>
<input type="hidden" name="RecordID" id="RecordID" value="<?= $RecordID?>" />

</form>
<?
$linterface->FOCUS_ON_LOAD("form1.GameChiName");
$linterface->LAYOUT_STOP();
//     }
 //   else
 //   {
    ?>

