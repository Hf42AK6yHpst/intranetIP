<?php


$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/SFOC/libmedallist.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libmedallist = new libmedallist();
// $libmedallist->hasAccessRight($_SESSION['UserID'], 'Admin');

$CurrentPage = "PageMgtMedalList";
$CurrentPageArr['MedalList'] = 1;

$MODULE_OBJ = $libmedallist->GET_MODULE_OBJ_ARR();


$TAGS_OBJ[] = array($MedalListMenu['medallist'], "", 1);
$returnMsg = $Lang['General']['ReturnMessage'][$xmsg];
$linterface->LAYOUT_START($returnMsg);
echo $linterface->Include_JS_CSS();

$ActionBtnArr = array();
$ActionBtnArr[] = array('edit', 'javascript:checkEdit(document.form1,\'RecordID[]\',\'Record_new.php\')');
$ActionBtnArr[] = array('delete', 'javascript:checkRemove2(document.form1,\'RecordID[]\',\'goDeleteRecord();\')');


$menuBar = '<div class="content_top_tool">'."\n";
    $menuBar .= '<div class="Conntent_tool">'."\n";
        $menuBar .= $linterface->Get_Content_Tool_v30("new","javascript:js_Go_New_Record();")."\n";
        $menuBar .= $linterface->Get_Content_Tool_v30("import","javascript:js_Go_Import_Record();")."\n";
    $menuBar .= '</div>'."\n";
$menuBar .= '</div>'."\n";

$toolsBar= $linterface->Get_DBTable_Action_Button_IP25($ActionBtnArr);

$MedalListRecord = $libmedallist->getMedalListRecord();
$numOfRecords = count($MedalListRecord);
if ($field == "")
    $field = 0;

if ($order == "" || !isset ($order)) {
    $order = ($order == 0) ? 1 : 0;
}

$li = new libdbtable2007($field, $order, $pageNo);


$keyword = ($_GET['keyword'] != '') ? $_GET['keyword'] : '';

$searchTag = "<input name=\"keyword\" id=\"keyword\" type=\"text\" value=\"" . intranet_htmlspecialchars(stripslashes($keyword)) . "\" onkeyup=\"Check_Go_Search(event);\"/>";

$LocationTitle = Get_Lang_Selection('smlr.LocationChi','smlr.LocationEng');
$GameName = Get_Lang_Selection('sgs.GameChiName','sgs.GameEngName');
$EventName = Get_Lang_Selection('ses.EventChiName','ses.EventEngName');
$SportName = Get_Lang_Selection('sst.SportChiName','sst.SportEngName');
$AthleteName= Get_Lang_Selection('smlr.NameChi','smlr.NameEng');
// $MedalList[] = array(1,Get_Lang_Selection('金牌','Gold'));
// $MedalList[] = array(2,Get_Lang_Selection('銀牌','Silver'));
// $MedalList[] = array(3,Get_Lang_Selection('銅牌','Bronze'));
$MedalList[1] = Get_Lang_Selection('金牌','Gold');
$MedalList[2] = Get_Lang_Selection('銀牌','Silver');
$MedalList[3] = Get_Lang_Selection('銅牌','Bronze');

$year_conds = " smlr.Year LIKE '%" . $libmedallist->Get_Safe_Sql_Like_Query(stripslashes($keyword)) . "%'";
$location_conds = " OR smlr.LocationChi LIKE '%" . $libmedallist->Get_Safe_Sql_Like_Query(stripslashes($keyword)) . "%'
                OR smlr.LocationEng LIKE '%" . $libmedallist->Get_Safe_Sql_Like_Query(stripslashes($keyword)) . "%'";
$game_conds = " OR sgs.GameChiName LIKE '%" . $libmedallist->Get_Safe_Sql_Like_Query(stripslashes($keyword)) . "%'
                OR sgs.GameEngName LIKE '%" . $libmedallist->Get_Safe_Sql_Like_Query(stripslashes($keyword)) . "%'";
$event_conds = " OR ses.EventChiName LIKE '%" . $libmedallist->Get_Safe_Sql_Like_Query(stripslashes($keyword)) . "%'
                OR ses.EventEngName LIKE '%" . $libmedallist->Get_Safe_Sql_Like_Query(stripslashes($keyword)) . "%'";
$sport_conds = " OR sst.SportChiName LIKE '%" . $libmedallist->Get_Safe_Sql_Like_Query(stripslashes($keyword)) . "%'
                OR sst.SportEngName LIKE '%" . $libmedallist->Get_Safe_Sql_Like_Query(stripslashes($keyword)) . "%'";
$name_conds = " OR smlr.NameChi LIKE '%" . $libmedallist->Get_Safe_Sql_Like_Query(stripslashes($keyword)) . "%'
                OR smlr.NameEng LIKE '%" . $libmedallist->Get_Safe_Sql_Like_Query(stripslashes($keyword)) . "%'";
$conds = $year_conds.$location_conds.$game_conds.$event_conds.$sport_conds.$name_conds;


// smlr.LocationChi,sgs.GameChiName,ses.EventChiName,sst.SportChiName,smlr.NameChi,
$sql = "
                SELECT 
                    smlr.Year,
                    $LocationTitle as LocationName,
                    $GameName as GameName,
                    $EventName as EventName,
                    $SportName as SportName,
                    $AthleteName as AthleteName,
                    CASE 
                        WHEN smlr.Medal=1 THEN '".$MedalList[1]."'
                        WHEN smlr.Medal=2 THEN '".$MedalList[2]."'
                        WHEN smlr.Medal=3 THEN '".$MedalList[3]."'
                    END AS Medal,
                    smlr.WebsiteCode,
                    CONCAT('<input type=\"checkbox\" name=\"RecordID[]\" value=\"', smlr.RecordID ,'\">') As CheckBox,
                    smlr.RecordID                  
                FROM  SFOC_MEDAL_LIST_RECORD AS smlr               
                    INNER JOIN SFOC_GAMES_SETTING AS sgs ON (smlr.GameID = sgs.GameID)
                    INNER JOIN SFOC_EVENTS_SETTING AS ses ON (smlr.EventID = ses.EventID)
                    INNER JOIN SFOC_SPORTS_TYPE AS sst ON (smlr.SportID = sst.SportID)
                WHERE  $conds
";

                    
// $li->field_array = array (
//     "Year",
//     "LocationChi",
//     "GameChiName",
//     "EventChiName",
//     "SportChiName",
//     "NameChi"
// );

$li->field_array = array (
    "Year",
    "LocationChi",
    "GameName",
    "EventName",
    "SportName",
    "AthleteName","Medal","WebsiteCode","CheckBox"
);
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1; #+2個2係唔洗SORT的數量
// $li->title = $eDiscipline["Record"];
$li->column_array = array(0,0,0); # col style
$li->wrap_array = array(0,0,0); # dbtable displayCell => case
$li->IsColOff =  "IP25_table";

$pos = 0;
$li->column_list .= "<th class='num_check'>#</th>\n";
$li->column_list .= "<th>".$li->column_IP25($pos++,  $Lang['SFOC']['MedalList']['Year'])."</th>\n";
$li->column_list .= "<th>".$li->column_IP25($pos++,  $Lang['SFOC']['MedalList']['Location'])."</th>\n";
$li->column_list .= "<th>".$li->column_IP25($pos++,  $Lang['SFOC']['MedalList']['Game'])."</th>\n";
$li->column_list .= "<th>".$li->column_IP25($pos++,  $Lang['SFOC']['MedalList']['Event'])."</th>\n";
$li->column_list .= "<th>".$li->column_IP25($pos++,  $Lang['SFOC']['MedalList']['Sport'])."</th>\n";
$li->column_list .= "<th>".$li->column_IP25($pos++,  $Lang['SFOC']['MedalList']['NameOfAthlete'])."</th>\n";
$li->column_list .= "<th>".$Lang['SFOC']['MedalList']['Medal']."</th>\n";
$li->column_list .= "<th>".$Lang['SFOC']['MedalList']['WebsiteCode']."</th>\n";
// $li->column_list .= "<th class='num_check'><input type=\"checkbox\" name=\"checkmaster\" onclick=\"(this.checked)?setChecked(1,this.form,'RecordID[]'):setChecked(0,this.form,'RecordID[]')\" name=\"checkmaster\"></th>\n";
$li->column_list .="<th width=1 class=\"num_check\">" . $li->check("RecordID[]") . "</th>\n";

// "<th width=1 class=\"num_check\">" . $li->check("RecordID[]") . "</th>\n";





// $x = '';
// $x .= '<br />'."\n";
//     $x .= '<form id="form1" name="form1" method="get" action="category2.php">'."\n";
//         $x .= '<div class="table_board">'."\n";
//             $x .= '<table id="html_body_frame" width="100%">'."\n";
//                 $x .= '<tr>'."\n";
//                     $x .= '<td>'."\n";
//                         $x .= '<div class="content_top_tool">'."\n";
//                             $x .= '<div class="Conntent_tool">'."\n";
//                              $x .= $linterface->Get_Content_Tool_v30("new","javascript:js_Go_New_Record();")."\n";
//                              $x .= $linterface->Get_Content_Tool_v30("import","javascript:js_Go_Import_Record();")."\n";
//                              $x .= '</div>'."\n";
//                         $x .= '</div>'."\n";
//                     $x .= '</td>'."\n";
//                 $x .= '</tr>'."\n";
//                $x .= '<tr>'."\n";
//                     $x .= '<td>'."\n";
//                          $x .= $linterface->Get_DBTable_Action_Button_IP25($ActionBtnArr);
//                          $x .= '<table id="ContentTable" class="common_table_list_v30">'."\n";                             
//                              $x .= '<thead>'."\n";
//                                 $x .= '<tr>'."\n"; 
//                                     $x .= '<th style="width:2%;">#</th>'."\n";
//                                     $x .= '<th style="width:11%;">'.$Lang['SFOC']['MedalList']['Year'].'</th>'."\n";
//                                     $x .= '<th style="width:17%;">'.$Lang['SFOC']['MedalList']['Location'].'</th>'."\n";
//                                     $x .= '<th style="width:17%;">'.$Lang['SFOC']['MedalList']['Game'].'</th>'."\n";
//                                     $x .= '<th style="width:17%;">'.$Lang['SFOC']['MedalList']['Event'].'</th>'."\n";
//                                     $x .= '<th style="width:17%;">'.$Lang['SFOC']['MedalList']['Sport'].'</th>'."\n";
//                                     $x .= '<th style="width:17%;">'.$Lang['SFOC']['MedalList']['NameOfAthlete'].'</th>'."\n";
//                                     $x .= '<th style="width:2%;"><input type="checkbox" onclick="(this.checked)?setChecked(1,this.form,\'RecordID[]\'):setChecked(0,this.form,\'RecordID[]\')" name="checkmaster"></th>'."\n";
//                                 $x .= '<tr>'."\n";
//                              $x .= '</thead>'."\n";
                                
//                                 $x .= '<tbody>'."\n";
//                                 if ($numOfRecords== 0) {
//                                         $x .= '<tr><td colspan="100%" style="text-align:center;">'.	$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
//                                     }
//                                     else {
//                                         for ($i=0; $i<$numOfRecords; $i++) {
//                                             $thisRecordID= $MedalListRecord[$i]['RecordID'];
//                                             $thisYear = $MedalListRecord[$i]['Year'];
//                                             $thisLocationName= $MedalListRecord[$i]['LocationName'];
//                                             $thisGameName= $MedalListRecord[$i]['GameName'];
//                                             $thisEventName= $MedalListRecord[$i]['EventName'];
//                                             $thisSportName= $MedalListRecord[$i]['SportName']; 
//                                             $thisAthleteName= $MedalListRecord[$i]['AthleteName'];             
                                            
//                                             $x .= '<tr id="tr_'.$thisRecordID.'">'."\n";
//                                             $x .= '<td><span class="rowNumSpan">'.($i + 1).'</td>'."\n";
//                                             $x .= '<td>'.$thisYear.'</td>'."\n";
//                                             $x .= '<td>'.$thisLocationName.'</td>'."\n";
//                                             $x .= '<td>'.$thisGameName.'</td>'."\n";
//                                             $x .= '<td>'.$thisEventName.'</td>'."\n";
//                                             $x .= '<td>'.$thisSportName.'</td>'."\n";
//                                             $x .= '<td>'.$thisAthleteName.'</td>'."\n";
                                          
//                                             $x .= '<td>'."\n";
//                                             $x .= '<input type="checkbox" id="RecordChk" class="RecordChk" name="RecordID[]" value="'.$thisRecordID.'">'."\n";
//                                             $x .= '</td>'."\n";
//                                             $x .= '</tr>'."\n";
//                                         }
//                                     }
//                                $x .= '</tbody>'."\n";
//                         $x .= '</table>'."\n";
//                     $x .= '</td>'."\n";
//                 $x .= '</tr>'."\n";
//             $x .= '</table>'."\n";
//         $x .= '</div>'."\n";
//     $x .= '</form>'."\n";
// $x .= '<br />'."\n";
?>

<script language="javascript">
$(document).ready( function() {	
// 	js_Init_DND_Table();

});

function js_Go_New_Record() {
	window.location = 'Record_new.php';
}
function js_Go_Import_Record(){
	window.location = 'import.php';
}
	
//Delete Game
function goDeleteRecord() {
	var jsSelectedRecordIDArr = new Array();
	$('input.RecordChk:checked').each( function() {
		jsSelectedRecordIDArr[jsSelectedRecordIDArr.length] = $(this).val();
	});
	var jsSelectedRecordIDList = jsSelectedRecordIDArr.join(',');


	$('form#form1').attr('action', 'Record_remove.php').submit();

}

function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		document.form1.submit();
	else
		return false;
}

</script>

<form id="form1" name="form1" method="get" action="index.php">
<div class="content_top_tool">
	<div class="Conntent_tool"><?=$menuBar?></div>
	<div class="Conntent_search"><?=$searchTag?></div>
<br style="clear:both" />
</div>

<div class="table_board">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td valign="bottom"><?=$toolsBar?></td>
</tr>
</table>
<?php echo $li->display();?>
</div>

<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />

</form>

<?php 
// echo $x;

$linterface->LAYOUT_STOP();
intranet_closedb();
?>