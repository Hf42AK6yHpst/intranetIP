<?php
/*
 * 	Log:
 * 	Date:	2018-03-12 Anna
 *          Created this file
 * 
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/SFOC/libmedallist.php");


intranet_auth();
intranet_opendb();

$libmedallist = new libmedallist();
// $fileupload = false;

// if($_FILES["SportIconUpload"]){
//     $imageDir = 'upload/';
//     $dateStr = Date('YmdHis');
//     $filename = $dateStr . '_' . basename($_FILES["SportIconUpload"]["name"]);
//     $filepath = $imageDir . $filename;
//     $imageFileType = strtolower(pathinfo($filename,PATHINFO_EXTENSION));
//    // $check = getimagesize($_FILES["SportIconUpload"]["tmp_name"]);
//     $check = $_FILES["SportIconUpload"]["tmp_name"] == ''? false: getimagesize($_FILES["SportIconUpload"]["tmp_name"]);
    
//     if($check !== false){
//         // Do upload
//         move_uploaded_file($_FILES["SportIconUpload"]["tmp_name"], $filepath);
//         $fileupload = true;
//     } else {
//         // Not ok
//     }
// }


$SportArr['SportChiName'] = $SportChiName;
$SportArr['SportEngName'] = $SportEngName;
$SportArr['IntroductionChi'] = $SportChiIntro;
$SportArr['IntroductionEng'] = $SportEngIntro;
// if($fileupload){
//     $SportArr['CustomIconPath'] = $filename;
// }
if ($SportID== "") {
	
	$successAry['add'] = $libmedallist->addSport($SportArr);
	
} else {
    $SportArr['SportID'] = $SportID;
    $successAry['edit'] = $libmedallist->updateSport($SportArr);
}

if (in_array(false, $successAry)) {
//	$libenroll->RollBack_Trans();
	$ReturnMsgKey = 'UpdateUnsuccess'; 
}
else {
//	$libenroll->Commit_Trans();
	$ReturnMsgKey = 'UpdateSuccess';
}

intranet_closedb();
header("Location: sport.php?ReturnMsgKey=$ReturnMsgKey");
?>