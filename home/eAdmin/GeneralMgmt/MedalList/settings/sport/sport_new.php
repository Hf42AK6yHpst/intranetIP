<?php
/*
 * 	Log
 * 	Date:	2018-03-12 Anna
 *          Created this file
 *  
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/SFOC/libmedallist.php");


include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");

intranet_auth();
intranet_opendb();

$libUser = new libuser($UserID);	
$linterface = new interface_html();
$libmedallist = new libmedallist();
	
	
// if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
// 		header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	
	$CurrentPage = "PageSysSettingSportSettings";
	$CurrentPageArr['MedalList'] = 1;
	
	$MODULE_OBJ = $libmedallist->GET_MODULE_OBJ_ARR();
	
// 	if ($libmedallist->hasAccessRight($_SESSION['UserID'])) {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $TAGS_OBJ[] = array($MedalListMenu['sport'], "", 1);

        $linterface->LAYOUT_START();
		if (is_array($SportID)) {
		    $SportID= $SportID[0];
		}
		        
		$fieldname = "SportID, SportChiName, SportEngName, IntroductionChi, IntroductionEng";
		$SportArr = $libmedallist->getSport($SportID, $fieldname);
		if(count($SportArr) > 0){
		    $SportArr = $SportArr[0];
		}
		$SportID = $SportArr['SportID'];
		$SportEngName= $SportArr['SportEngName']; 
		$SportChiName= $SportArr['SportChiName'];
		$IntroductionChi = $SportArr['IntroductionChi'];
		$IntroductionEng = $SportArr['IntroductionEng'];
// 		$CustomIconPath = $SportArr['CustomIconPath'];
		
		# page navigation (leave the array empty if no need)
		if ($SportID != "") {
		    $PAGE_NAVIGATION[] = array($button_edit." ".$MedalListMenu['sport'], "");
			$button_title = $button_save;
		} else {
		    $PAGE_NAVIGATION[] = array($button_new." ".$MedalListMenu['sport'], "");
			$button_title = $button_submit;
		}

?>
<style>
textarea{
min-height: 100px;
}
img.imgIcon{
    height: 150px;
    width: 150px;
}
</style>
<script language="javascript">
function FormSubmitCheck(obj)
{
	if(!check_text(obj.SportChiName, "<?php echo $i_alert_pleasefillin.$Lang['SFOC']['Settings']['SportChi']; ?>.")) return false;
	if(!check_text(obj.SportEngName, "<?php echo $i_alert_pleasefillin.$Lang['SFOC']['Settings']['SportEng']; ?>.")) return false;
	if(!check_text(obj.SportChiIntro, "<?php echo $i_alert_pleasefillin.$Lang['SFOC']['Settings']['SportIntroChi']; ?>.")) return false;
	if(!check_text(obj.SportEngIntro, "<?php echo $i_alert_pleasefillin.$Lang['SFOC']['Settings']['SportIntroEng']; ?>.")) return false;
<?
//if ($sys_custom['eEnrolment']['CategoryType']) {
//	print "if(!check_text(obj.CategoryTypeID, \"". $i_alert_pleasefillin.$Lang['eEnrolment']['CategoryType']. "\")) return false;\n";
//}
?>
	obj.submit();
}
</SCRIPT>
<form name="form1" action="sport_update.php" method="POST" enctype="multipart/form-data">
<br/>

<table width="100%" border="0" cellspacing="4" cellpadding="4">
<tr><td>
	<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
</td></tr>
<tr><td>

<table class="form_table_v30">
	<tr>
		<td class="field_title"><?=$linterface->RequiredSymbol()?><?= $Lang['SFOC']['Settings']['SportChi']?></td>
		<td><input type="text" id="SportChiName" name="SportChiName" value="<?= htmlspecialchars($SportChiName)?>" class="textboxtext"></td>
	</tr>
	<tr>
		<td class="field_title"><?=$linterface->RequiredSymbol()?><?= $Lang['SFOC']['Settings']['SportEng']?></td>
		<td><input type="text" id="SportEngName" name="SportEngName" value="<?= htmlspecialchars($SportEngName)?>" class="textboxtext"></td>
	</tr>
	<tr>
		<td class="field_title"><?=$linterface->RequiredSymbol()?><?= $Lang['SFOC']['Settings']['SportIntroChi']?></td>
		<td><textarea id="SportChiIntro" name="SportChiIntro" class="textboxtext"><?= htmlspecialchars($IntroductionChi)?></textarea></td>
	</tr>
	<tr>
		<td class="field_title"><?=$linterface->RequiredSymbol()?><?= $Lang['SFOC']['Settings']['SportIntroEng']?></td>
		<td><textarea id="SportEngIntro" name="SportEngIntro" class="textboxtext"><?= htmlspecialchars($IntroductionEng)?></textarea></td>
	</tr>
</table>

</td></tr>

</table>
<table width="98%" border="0" cellspacing="0" cellpadding="1" align="center">
<tr><td align="left" class="tabletextremark"><?=$i_general_required_field?></td></tr>
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">
<div style="padding-top: 5px">
<?= $linterface->GET_ACTION_BTN($button_title, "button", "javascript: FormSubmitCheck(document.form1);")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='sport.php'")?>
</div>
</td></tr>
</table>
<br/>
<input type="hidden" name="SportID" id="SportID" value="<?= $SportID?>" />

</form>
<?= $linterface->FOCUS_ON_LOAD("form1.SportChiName") ?>
<?
  $linterface->LAYOUT_STOP();
?>

