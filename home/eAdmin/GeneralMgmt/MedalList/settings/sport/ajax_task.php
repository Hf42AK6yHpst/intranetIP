<?php 
################## Change Log [Start] #################
#   using  

#
################## Change Log [End] ###################

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT.'includes/libimporttext.php');
include_once($PATH_WRT_ROOT."includes/SFOC/libmedallist.php");

intranet_auth();
intranet_opendb();


if($task == 'validateRecordImport'){
	//$webSAMSCheckingArr = array_filter(Get_Array_By_Key($data,'2'));	
	### Include js libraries
	$jsInclude = '';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
	echo $jsInclude;
	
	
	$limport = new libimporttext();
	$libmedallist = new libmedallist();
	
	$targetFilePath = standardizeFormPostValue($_GET['targetFilePath']);
	
	### Get Data from the csv file
	$csvData = $limport->GET_IMPORT_TXT($targetFilePath);
	$csvColName = array_shift($csvData);
	$numOfData = count($csvData);

	$errorCount = 0;
	$successCount = 0;
	##verify imported data
	for ($i=0; $i<$numOfData; $i++) {
	    
	    $_sportChi = safeString($csvData[$i][0]); 
	    $_sportEng = safeString($csvData[$i][1]);
	    $_sportIntroChi= safeString($csvData[$i][2]);
	    $_sportIntroEng= safeString($csvData[$i][3]);
	   
		        
		// find blank space
		 if($_sportEng=="" || $_sportChi==""){
		     $errorArr[$i][] = $Lang['SFOC']['MedalList']['emptySport'];
		 }
		 
		 		 
		 $sportConds = "AND SportChiName = '".$_sportChi."' AND SportEngName ='".$_sportEng."'";
		 $sportID  = $libmedallist->getSportIDByCondition($sportConds);

		 if($sportID !=""){
		     $errorArr[$i][] =  $Lang['SFOC']['MedalList']['MappingSport'];
		 }
			
 		 ## Insert Data in Temp Table
		 $TempValueArr[$i] = '("'.($i+1).'","'.$_sportChi.'","'.$_sportEng.'","'.$_sportIntroChi.'","'.$_sportIntroEng.'","'.$_SESSION['UserID'] .'",now())';
		//debug_pr($TempValueArr);
		### Update Processing Display
		Flush_Screen_Display(1);
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= '$("span#BlockUISpan", window.parent.document).html("'.($i + 1).'");';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
	}	

	for ($i=0;$i<sizeof($csvData);$i++){
	    if ($errorArr[$i]==""){
	        $successCount++;
	    }
	}
	$x = '';
	if (sizeof($errorArr)>0)
    {
        $x .= '<table class="common_table_list_v30 view_table_list_v30">';
 //       $x .= "<tr><td>".$ec_guide['import_error_row']."</td><td>".$ec_guide['import_error_reason']."</td><td>".$ec_guide['import_error_detail']."</td></tr>\n";        
        $x .= '<tr>';
            $x .= '<th>#</th>';
            $x .= '<th>'.$Lang['SFOC']['Settings']['SportChi'].'</th>';
            $x .= '<th>'.$Lang['SFOC']['Settings']['SportEng'].'</th>';
            $x .= '<th>'.$Lang['SFOC']['Settings']['SportIntroChi'].'</th>';
            $x .= '<th>'.$Lang['SFOC']['Settings']['SportIntroEng'].'</th>';
            $x .= '<th>'.$Lang['SFOC']['MedalList']['Remark'].'</th>';
        $x .= '</tr>';
        $x .= '<tbody>';

        foreach($errorArr as $i=>$ErrorAry)
    	{
    	    $_sportChi = safeString($csvData[$i][0]);
    	    $_sportEng = safeString($csvData[$i][1]);
    	    $_sportIntroChi= safeString($csvData[$i][2]);
    	    $_sportIntroEng= safeString($csvData[$i][3]);
    	    
    	    $x .= '<tr><td class="tabletext">'.($i+1).'</td>';
    	    $x .='<td class="tabletext">'.$_sportChi.'</td>';
    	    $x .='<td class="tabletext">'.$_sportEng.'</td>';
    	    $x .='<td class="tabletext">'.$_sportIntroChi.'</td>';
    	    $x .='<td class="tabletext">'.$_sportIntroEng.'</td>';
        	$x .='<td class="tabletext">'.implode('<br>',$ErrorAry).'</td></tr>';
    	}
    	$x .= '</tbody>';
    	$x .= '</table>';
    	$htmlAry['errorTbl'] = $x;  
    }
	
    # counting number of incorrect data
    $errorCount=count($errorArr);	

	### Display Record Error Table	//TODO
 	if($errorCount == 0){	    
 	    $libmedallist->insertSportRecordTemp($TempValueArr);
    }
		 
  	$errorCountDisplay = ($errorCount > 0) ? "<font color=\"red\">".$errorCount."</font>" : $errorCount;
	
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
	$thisJSUpdate .= 'window.parent.document.getElementById("ErrorTableDiv").innerHTML = \''.$htmlAry['errorTbl'].'\';';
	$thisJSUpdate .= 'window.parent.document.getElementById("SuccessCountDiv").innerHTML = \''.$successCount.'\';';
	$thisJSUpdate .= 'window.parent.document.getElementById("FailCountDiv").innerHTML = \''.$errorCountDisplay.'\';';
	
	if ($errorCount == 0) {
		$thisJSUpdate .= '$("input#ImportBtn", window.parent.document).removeClass("formbutton_disable").addClass("formbutton");';
		$thisJSUpdate .= '$("input#ImportBtn", window.parent.document).attr("disabled","");';
	}
		
	$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
}
intranet_closedb();

function safeString($str){
    $rs = str_replace('\'', '\\\'', $str);
    $rs = str_replace('\"', '\\\"', $rs);
    return $rs;
}
?>