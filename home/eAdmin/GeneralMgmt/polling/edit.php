<?php
# using: Bill

########## Change Log [Start] ############
#
#   Date:   2019-05-17 (Bill)
#           Change hidden input field name from "OriginalGroupID" to "OriginalGroupCode"
#
#	Date:	2016-05-12 (Bill)
#			use session_is_registered_intranet(), session_register_intranet() for PHP 5.4
#
#	Date:	2012-07-16 (YatWoon)
#			update download_attachment.php with encrypt logic
#
#	Date:	2011-04-13	YatWoon
#			add choice to Z 
#
#	Date:	2010-04-27 [YatWoon]
#			can update past polling (but only can update due date)
#
########## Change Log [End] ############

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpolling.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "ePolling";

// $lpolling = new libpolling();
$PollingID[0] = IntegerSafe($PollingID[0]);
$li = new libpolling($PollingID[0]);
$lf = new libfilesystem();

$hasAccessRight = $_SESSION["SSV_USER_ACCESS"]["eAdmin-ePolling"];
if(!$hasAccessRight)
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$now = time();
$today = mktime(0,0,0,date("m",$now),date("d",$now),date("Y",$now));
$start = strtotime($li->DateStart);

## special_announce_public_allowed
$lgroup = new libgrouping();

### Title ###
$top_menu_mode = 1;
$TAGS_OBJ[] = array($i_adminmenu_im_polling,"");
$MODULE_OBJ = $li->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_edit.($intranet_session_language=="en"?" ":""). $i_adminmenu_im_polling, "");

# Prepare UI
$addButton = "<a id=\"addItemButton\" href=\"javascript:void(0)\" onclick=\"addNewItem()\" class=\"contenttool\" style=\"float:right;margin-top:5px;margin-right:20px\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_more_option . "</a>";
$delBtnEmpty = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" style=\"margin:3px; opacity:0;filter:alpha(opacity=0); cursor: default\" />";
$delBtn	= "<a href=\"javascript:void(0)\" onclick=\"removeItem(this)\"class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" style=\"margin:3px\" title=\"".$button_delete."\"  /></a>";

$prefix = "Answer";
// $suffix = Array('A','B','C','D','E','F','G','H','I','J');
$suffix = array();
for($i=65; $i<=90; $i++)
{
	$suffix[] = chr($i);
}

$allAnswer = Array();
$lastSuff = "";
for ($i = 0; $i < 26; $i ++){
	$fullName = $prefix.$suffix[$i];
	if (!empty($li->$fullName)){
		$allAnswer[$suffix[$i]] = $li->$fullName;
	}
}
$noAnswer = count($allAnswer); 

# Attachment Folder
//if(!session_is_registered("pollingAttFolder") || $pollingAttFolder==""){
//     session_register("pollingAttFolder");
//}
$pollingAttFolder = $li->Attachment;
//if(!session_is_registered_intranet("pollingAttFolder")){
$pollingAttFolder = $pollingAttFolder? $pollingAttFolder : "";
session_register_intranet("pollingAttFolder", $pollingAttFolder);
//}

if(empty($pollingAttFolder))
{
	$pollingAttFolder = session_id().".".time();
	$path = "$file_path/file/polling/";
	if (!is_dir($path))
	{
	     $lf->folder_new($path);
	}
	$path = "$file_path/file/polling/$pollingAttFolder";
	if (!is_dir($path))
	{
	     $lf->folder_new($path);
	}
}

$path = "$file_path/file/polling/$pollingAttFolder";
$lo = new libfiletable("", $path, 0, 0, "");
$files = $lo->files;

$preisAnonymous = $ia==""? $li->RecordType : $ia;
$TargetGroup = $li->returnTargetGroupsArray($PollingID[0]);
$TargetGroupIDArr = Get_Array_By_Key($TargetGroup,'GroupID');

$editable = $li->isPollingEditable($PollingID[0]);
?>

<script src="/templates/jquery/jquery-1.3.2.min.js"></script>
<script language="javascript">
function checkOption1(obj){
	for(i=0; i<obj.length; i++){
		if(!parseInt(obj.options[i].value)){
				obj.options[i] = null;
		}
	}
}

function checkform(obj){
	<? if($start > $today) {?>
	     if(!check_text(obj.DateStart, "<?php echo $i_alert_pleasefillin.$i_PollingDateStart; ?>.")) return false;
	     if(!check_date(obj.DateStart, "<?php echo $i_invalid_date; ?>.")) return false;
	     if(!check_text(obj.DateEnd, "<?php echo $i_alert_pleasefillin.$i_PollingDateEnd; ?>.")) return false;
	     if(!check_date(obj.DateEnd, "<?php echo $i_invalid_date; ?>.")) return false;
	     if(!check_text(obj.Question, "<?php echo $i_alert_pleasefillin.$i_PollingQuestion; ?>.")) return false;
	     if(!check_text(obj.AnswerA, "<?php echo $i_alert_pleasefillin.$i_PollingAnswerA; ?>")) return false;
	     if(!check_text(obj.AnswerB, "<?php echo $i_alert_pleasefillin.$i_PollingAnswerB; ?>")) return false;
	     
	     if (compareDate(obj.DateStart.value,obj.DateEnd.value)>0) {
	         alert ("<?php echo $i_Homework_new_duedate_wrong; ?>");
	         return false;
	     }
		 startdate = (obj.DateStart.value).split('-');
		 releasedate = (obj.DateRelease.value).split('-');
	     if (Date.UTC(startdate[0],startdate[1],startdate[2]) > Date.UTC(releasedate[0],releasedate[1],releasedate[2])){
			alert("<?=$i_PollingWrontRelease?>");
			return false;
		 }
		 
		checkOption(obj.elements['Attachment[]']);
		checkOptionAll(obj.elements["Attachment[]"]);
		
		<!--? if($editable) {?-->
        var PublicDisplay = document.getElementById("publicdisplay");
		var Group = document.getElementById("GroupID");
		if(PublicDisplay.checked == false)
		{
			if(Group.length==0){
				alert ("<?=$Lang['SysMgr']['SchoolNews']['WarnSelectGroup']?>");
				return false;
			}
		}
		
		checkOptionAll(obj.elements["GroupID[]"]);
		//checkOption1(document.getElementById("GroupID[]"));
		checkOption1(document.getElementById("GroupID"));
		return true;
	<? } else { ?>
	    if(!check_text(obj.DateEnd, "<?php echo $i_alert_pleasefillin.$i_PollingDateEnd; ?>.")) return false;
		if(!check_date(obj.DateEnd, "<?php echo $i_invalid_date; ?>.")) return false;
		if (compareDate("<?=$li->DateStart?>",obj.DateEnd.value)>0) {
	         alert ("<?php echo $i_Homework_new_duedate_wrong; ?>");
	         return false;
	    }
		if (compareDate("<?=date("Y-m-d")?>",obj.DateEnd.value)>0) {
	         alert ("<?php echo $Lang['Polling']['InvalidDueDate']; ?>");
	         return false;
	    }
	    if(Trim(obj.DateRelease.value)!="") {
		     if(!check_date(obj.DateRelease, "<?php echo $i_invalid_date; ?>.")) return false;
		     if (compareDate("<?=$li->DateStart?>",obj.DateRelease.value)>0)
		     {
		         alert ("<?=$Lang['Polling']['InvalidReleaseDate']?>");
		         return false;
		     }
	    }
	    
	    <!--? if($editable) {?-->
        var PublicDisplay = document.getElementById("publicdisplay");
		var Group = document.getElementById("GroupID");
		if(PublicDisplay.checked == false)
		{
			if(Group.length==0){
				alert ("<?php echo $Lang['Polling']['WarnSelectGroup']?>");
				return false;
			}
		}
		
		checkOptionAll(obj.elements["GroupID[]"]);
		// checkOption1(document.getElementById("GroupID[]"));
		checkOption1(document.getElementById("GroupID"));
	    return true;
    <? } ?>
}

function removeAllOption(from, to)
{
	checkOption1(from);
	checkOption1(to);
	for(i=0; i<from.length; i++){
		to.options[to.length] = new Option(from.options[i].text, from.options[i].value, false, false);
	}
	checkOptionClear(from);
	text1 = "";
	for(i=0; i<20; i++) 
		text1 += " ";
	checkOptionAdd(from, text1, "")
}

function checkalldate(t1, t2)
{
        today = new Date();
        smonth = today.getMonth()+1;
        sdate = today.getDate();
        if (smonth < 10)
        {
                smonth = "0"+smonth;
        }
        if (sdate < 10)
        {
                sdate = "0"+sdate;
        }
        stoday = today.getFullYear() + '-' + smonth + '-' + sdate;
        if (compareDate(t1, stoday) < 0) {alert("<?php echo $i_PollingWrontStart; ?>"); return false;}
        if (compareDate(t2, t1) < 0) {alert ("<?php echo $i_PollingWrongEnd; ?>"); return false;}

        return true;
}

function groupSelection(){
	var obj = document.getElementById("publicdisplay");
	var group = document.getElementById("groupSelectionBox");
	
	if(obj.checked == false)
	{
		group.style.display = "block";
		checkOption1(document.getElementById("GroupID"));
	}
	else
	{
		removeAllOption(document.getElementById("GroupID"), document.getElementById("AvailableGroupID"));
		group.style.display = "none";
	}
}

// var qName = new Array('<?=$i_PollingAnswerA?>','<?=$i_PollingAnswerB?>','<?=$i_PollingAnswerC?>','<?=$i_PollingAnswerD?>','<?=$i_PollingAnswerE?>','<?=$i_PollingAnswerF?>','<?=$i_PollingAnswerG?>','<?=$i_PollingAnswerH?>','<?=$i_PollingAnswerI?>','<?=$i_PollingAnswerJ?>');
var qName = new Array(
<? 
    $demi = "";
    for($i=65; $i<=90; $i++)
	{
		echo $demi . "'". $Lang['Polling']['Choice'] . " " . chr($i) ."'";	
		$demi = ",";
	}
?>
);
// var fName = new Array("AnswerA","AnswerB","AnswerC","AnswerD","AnswerE","AnswerF","AnswerG","AnswerH","AnswerI","AnswerJ");
var fName = new Array(
<? 
    $demi = "";
    for($i=65; $i<=90; $i++)
	{
		echo $demi . "'Answer". chr($i) ."'";	
		$demi = ",";
	}
?>
);

function addNewItem(){
	noItem = document.getElementById("noItems").value;
	if (parseInt(noItem) == 26)
		return;
	document.getElementById("noItems").value =  parseInt(noItem) + 1;
	
	$('a#addItemButton').before('<div id="items-'+ (parseInt(noItem) + 1)+'" class="itemBox" style="opacity:0;filter:alpha(opacity=0)"><span class="itemInput"><input style="margin:2px" type="text" name="'+fName[(parseInt(noItem))]+'" class="itemInput" maxLength="255"/><?=$delBtn?></span><span class="qText">'+qName[parseInt(noItem)]+'</span></div>');
	itemid = 'div#items-'+(parseInt(noItem)+1);
	if (parseInt(noItem) == 25){
		$("#addItemButton").remove();
	}
	$(itemid).animate({opacity: "1"}, 1000);
}

$(document).ready(function(){
	document.getElementById("noItems").value = '<?=$noAnswer?>';
})

function removeItem(obj){
	if ($('div:animated').get().length > 0)
		return;
	noItem = document.getElementById("noItems").value;
	parentItem = $((obj.parentNode.parentNode));
	nextAllItem = parentItem.nextAll().filter('div');	
	nextAllItem.each(function(){
		pieces = (this.id).split("-");
		this.id = "items-"+(parseInt(pieces[1])-1);
		inChild = $(this).children('span.itemInput').find('input').get();
		inChild[0].name = fName[(parseInt(pieces[1])-2)];
		textChild = $(this).children('span.qText').get();;
		textChild[0].innerHTML = qName[(parseInt(pieces[1])-2)];
		
	});
	noItem = document.getElementById("noItems").value;
	document.getElementById("noItems").value =  parseInt(noItem) - 1;
	parentItem.animate({opacity: "0"}, "fast", function(){
		$(this).remove();
		if ($("a#addItemButton").length == 0)
			$lastBlock = $('td#itemField').append('<?=$addButton?>');
	});
}
</script>

<style type="text/css">
div.itemBox{
	background-color: white; 
	width:100%; 
	border-bottom-color: #eeeeee;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	height: 32px;
}

input.itemInput{
	width: <?php echo (strstr($_SERVER['HTTP_USER_AGENT'],"MSIE")?'630':'635'); ?>px; 
	margin: 5px;
	margin-right: 1px;
	font-family:Verdana,Arial,Helvetica,sans-serif,新細明體,mingliu;
	font-size: 12px;
} 

span.itemInput{
	float:right;
	clear:both;
}

span.qText{
	vertical-align: bottom; 
	line-height:30px; 
	text-align:center;
	font-family:Verdana,Arial,Helvetica,sans-serif,新細明體,mingliu;
	font-size: 12px;
}
#groupSelectionBox table { width:auto!important; }
.form_table_v30 tr td tr td { border-bottom:0px; }
#groupSelectionBox table td { padding:5px; }
#GroupID { min-width:120px; }
#groupSelectionBox table td.field_title { width:30%; min-width:150px; }
</style>

<br />   
<form name="form1" action="edit_update.php" method="post" onSubmit="return checkform(this);">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>

<tr>
	<td colspan="2">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
        	<td><br />
				<table align="center" class='form_table_v30'>
				<? if ($start <= $today) {?>
					<tr valign="top">
						<td width="30%" valign="top" nowrap="nowrap" class="field_title"><span class="tabletext"><?=$i_PollingDateStart?> <span class='tabletextrequire'>*</span></span></td>
						<td><input name="DateStart" type="text" class="textboxnum" maxlength="10" value="<?php echo $li->DateStart; ?>"/><?=$linterface->GET_CALENDAR("form1","DateStart");?></td>
					<td width="2%"></td>
					</tr>
	                                <tr valign="top">
						<td width="30%" valign="top" nowrap="nowrap" class="field_title"><span class="tabletext"><?=$i_PollingDateEnd?> <span class='tabletextrequire'>*</span></span></td>
						<td><input name="DateEnd" type="text" class="textboxnum" maxlength="10" value="<?php echo $li->DateEnd; ?>"/><?=$linterface->GET_CALENDAR("form1","DateEnd");?></td>
					<td></td>
					</tr>
	                                <tr valign="top">
						<td width="30%" valign="top" nowrap="nowrap" class="field_title"><span class="tabletext"><?=$i_PollingDateRelease?> </span></td>
						<td><input name="DateRelease" type="text" class="textboxnum" maxlength="10" value="<?php echo $li->DateRelease; ?>"/><?=$linterface->GET_CALENDAR("form1","DateRelease");?></td>
					<td></td>
					</tr>
	                <tr valign="top">
						<td width="30%" valign="top" nowrap="nowrap" class="field_title"><span class="tabletext"><?=$i_PollingQuestion?> <span class='tabletextrequire'>*</span></span></td>
						<td><input name="Question" type="text" class="textboxtext" maxlength="255" value="<?php echo $li->Question; ?>"/></td>
    					<td></td>
    				</tr>
    				<tr valign="top">
    					<td  class="field_title" style="border-bottom-width: 0px"><?php echo $Lang['Polling']['Answer']; ?> <span class='tabletextrequire'>*</span></td>
    					<td id="itemField" colspan='2' style="border-bottom-width: 0px">
							<input id="noItems" type="hidden" value="<?=$noAnswer?>" />
							<?php
								$i = 0;
								$output ='';
								foreach($allAnswer as $key => $answer){									
									$fullName = $prefix.$suffix[$i];
									$pollingAnswer = 'i_PollingAnswer'.$suffix[$i];
									$output .= '<div id="items-'.($i+1).'" class="itemBox">
								<span class="itemInput">
								<input style="margin:2px" type="text" name="'.$fullName.'"  class="itemInput" maxLength="255" value="'.$answer.'" />'.($suffix[$i]=="A" || $suffix[$i]=="B"? $delBtnEmpty: $delBtn).'</span>
								<span class="qText">'.$Lang['Polling']['Choice'].$suffix[$i].($suffix[$i]=="A" || $suffix[$i]=="B" ? '<span class="tabletextrequire">*</span>':"").'</span>
								</div>';
									$i++;
								}
                                echo $output.$addButton;
							?>
    					</td>
    				</tr>
                    <tr valign="top">
						<td width="30%" valign="top" nowrap="nowrap" class="field_title"><span class="tabletext"><?=$i_PollingReference?> </span></td>
						<td><input name="Reference" type="text" class="textboxtext" maxlength="255" value="<?php echo (($li->Reference=="") ? "http://" : $li->Reference); ?>"/></td>
					</tr>
					
					<tr valign='top'>
						<td class='field_title'><?=$i_Notice_Attachment?></td>
						<td>
							<table border='0' cellspacing='1' cellpadding='1'>
							<tr>
    							<td>
        							<select name='Attachment[]' size='4' multiple> 
        								<option><? for($i = 0; $i < 40; $i++) { $x .="&nbsp;"; } ?></option>
        							</select>
    							</td>
    							<td>
        							<?=$linterface->GET_BTN($i_frontpage_campusmail_attach, "button","newWindow('attach.php?folder=$pollingAttFolder',2)")?><br>
        							<?=$linterface->GET_BTN($i_frontpage_campusmail_remove, "button","checkOptionRemove(document.form1.elements['Attachment[]'])")?><br>
    							</td>
							</tr>
							</table>
						</td>
					</tr>
				<? } else { ?>
					<tr valign="top">
						<td width="30%" valign="top" nowrap="nowrap" class="field_title"><span class="tabletext"><?=$i_PollingDateStart?></span></td>
						<td><?php echo $li->DateStart; ?></td>
					</tr>
					<tr valign="top">
						<td width="30%" valign="top" nowrap="nowrap" class="field_title"><span class="tabletext"><?=$i_PollingDateEnd?></span> <span class='tabletextrequire'>*</span></td>
						<td><input name="DateEnd" type="text" class="textboxnum" maxlength="10" value="<?php echo $li->DateEnd; ?>"/><?=$linterface->GET_CALENDAR("form1","DateEnd");?></td>
					</tr>
					<tr valign="top">
						<td width="30%" valign="top" nowrap="nowrap" class="field_title"><span class="tabletext"><?=$i_PollingDateRelease?></span></td>
						<td><input name="DateRelease" type="text" class="textboxnum" maxlength="10" value="<?php echo $li->DateRelease; ?>"/><?=$linterface->GET_CALENDAR("form1","DateRelease");?></td>
					</tr>
					<tr valign="top">
						<td width="30%" valign="top" nowrap="nowrap" class="field_title"><span class="tabletext"><?=$i_PollingQuestion?></span></td>
						<td><?php echo $li->Question; ?><?php if (!$editable && $preisAnonymous==1 && $sys_custom['Polling_Anonymous']) { echo " (" . trim($Lang['Polling']['Anonymous']) . ")"; }?></td>
					</tr>
					<tr valign="top">
						<td width="30%" valign="top" nowrap="nowrap" class="field_title"><span class="tabletext"><?php echo $Lang["Polling"]["Answer"]; ?></span></td>
						<td><input id="noItems" type="hidden" value="<?=$noAnswer?>" />
    						<table class='form_table_v30'>
    						<? 
    							$i = 0;
    							foreach($allAnswer as $key => $answer)
    							{
                                    // $pollingAnswer = 'i_PollingAnswer'.$suffix[$i];
    								$pollingAnswer = $Lang['Polling']['Choice'] . $key;
    								$x .= "<tr valign='top'>";
    								$x .= "<td width='30%' valign='top' nowrap='nowrap' class='field_title'>". $pollingAnswer ."</td>";
    								$x .= "<td>". $answer ."</td>";
    								$x .= "</tr>";
    								$i++;
    							}
    							echo $x;
							?>
    						</table></td>
					</tr>
					<? if($li->Reference) {?>
    					<tr valign="top">
    						<td width="30%" valign="top" nowrap="nowrap" class="field_title"><span class="tabletext"><?=$i_PollingReference?> </span></td>
    						<td><?=$li->Reference?></td>
    					</tr>
					<? } ?>
					
					<? if(!empty($files)) {?>
    					<tr valign='top'>
    						<td class='field_title'><?=$i_Notice_Attachment?></td>
    						<td>
        						<?
        						foreach($files as $k=>$d) {
        							$this_path = $path . "/".$d[0];
                                    // $this_path = rawurlencode($this_path);
        							echo "<img src=$image_path/$LAYOUT_SKIN/index/whatnews/icon_attachment.gif hspace=2 vspace=2 border=0 align=absmiddle><a href='/home/download_attachment.php?target_e=". getEncryptedText($this_path) ."' class='indexpoplink'>$d[0]</a><br>";
        						 }
        						 ?>
    						</td>
    					</tr>
					<? } ?>
				<? } ?>
				<?php if ($sys_custom['Polling_User_Group_Option']) { ?>
    				<tr>
    					<td class='field_title'><?=$Lang['Polling']['Target']?></td>
    					<td align="left" valign="top">
    						<input type=hidden name="UpdatePastPolling" value="1">
    						<?php if ($editable && $sys_custom['Polling_Anonymous']) {?>
        						<span>
        							<input type="checkbox" name="isAnonymous" value="1" id="isAnonymous" <?=($preisAnonymous==1?"CHECKED":"");?>> <label for="isAnonymous"><?php echo $Lang['Polling']['Anonymous'] . " (" . $Lang['Polling']['Anonymous_Description'] . ")"; ?></label>
        						</span><br>
    						<?php } ?>
    						<input type="checkbox" name="publicdisplay" id="publicdisplay" onClick="groupSelection();" value="1" <?=(sizeof($TargetGroup)>0?"":"CHECKED")?> <?=($editable?'':'disabled')?>> <label for="publicdisplay"><?=$Lang['Polling']['WholeSchool']?></label>
    						<?php if ($sys_custom['Polling_User_Group_Option']) { ?>
        						<!--? if($editable) {?-->
        						<!-- Groups -->
        						<div id="groupSelectionBox" style="display:none;"><br>
        							<table width="90%" border="0" cellpadding="5" cellspacing="0" >
        								<tr>
        									<td valign="top" class='field_title' >
        										<?=$i_admintitle_group?>
        									</td>
        									<td >
        										<?php echo $lgroup->displayPollingGroupsFrontend($PollingID[0], "GroupID[]", 0, 1)?>
        									</td>
        								</tr>
        								<tr>
        									<td colspan="2"><font color="red"># <?php echo $Lang['Polling']['EditRemarks']; ?></font></td>
        								</tr>
        							</table>
        						</div>
    						<?php } ?>
    					</td>
    				</tr>
				<?php } ?>
				</table>
			</td>
        </tr>
        </table>
	</td>
</tr>

<tr>
	<td colspan="2">        
        <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
        <tr>
        	<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
		</tr>
        <tr>
        	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        </tr>
        <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_save, "submit", "", "submit2") ?>
        		<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2") ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back();","cancelbtn") ?>
			</td>
		</tr>
        </table>
	</td>
</tr>
</table>

<input type="hidden" name="PollingID" value="<?php echo $li->PollingID; ?>">
<input type="hidden" name="OriginalGroupCode" value="<?=urlencode(serialize($TargetGroupIDArr))?>">
</form>

<? if($start > $today) {?>
<script language="JavaScript1.2">
obj = document.form1.elements["Attachment[]"];
checkOptionClear(obj);
<?php while (list($key, $value) = each($files)) echo "checkOptionAdd(obj, \"".$files[$key][0]." (".ceil($files[$key][1]/1000)."Kb".")\", \"".$files[$key][0]."\");\n";  ?>
checkOptionAdd(obj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
</script>
<? } ?>

<script language="javascript">
<!--? if($editable) {?-->
groupSelection();
<!--? } ?-->
</script>

<?php
intranet_closedb();
if ($start > $today)
	print $linterface->FOCUS_ON_LOAD("form1.DateStart");
else
	print $linterface->FOCUS_ON_LOAD("form1.DateEnd");
$linterface->LAYOUT_STOP();
?>