<?php
# yat

#############################
#
#	Date:	2010-12-29	YatWoon
#			IP25 standard
#
#############################


if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpolling.php");
intranet_auth();
intranet_opendb();

$CurrentPage	= "ePolling";
$linterface 	= new interface_html();

## Check access right
$lpolling	= new libpolling();
$hasAccessRight =  $_SESSION["SSV_USER_ACCESS"]["eAdmin-ePolling"];
if(!$hasAccessRight)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	 $laccessright = new libaccessright();
	 $laccessright->NO_ACCESS_RIGHT_REDIRECT();
	 exit; 
}

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
if($field=="") $field = 1;
switch ($field){
     case 0: $field = 0; break;
     case 1: $field = 1; break;
     case 2: $field = 2; break;
     case 3: $field = 3; break;
     default: $field = 0; break;
}
$defectChr = Array("%","_");
$replaceChr = Array("\%","\_");
$keyword = str_replace($defectChr,$replaceChr,$keyword);
$sql  = "SELECT
               CONCAT('<nobr>', DATE_FORMAT(DateStart, '%Y-%m-%d'), '</nobr>'),
			   CONCAT('<nobr>', DATE_FORMAT(DateEnd, '%Y-%m-%d'), '</nobr>'),
               CONCAT('<a class=\'tablelink\' href=\'view.php?PollingID=', PollingID, '\'><img src=\'{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\' border=\'0\'></a>'),
			   Case When
			    DateStart <= now() THEN Question
			   ELSE
               CONCAT('<a class=\'tablelink\' href=\'edit.php?PollingID[]=', PollingID, '\'>', Question, '</a>')END,
               DateModified,
               CONCAT('<input type=checkbox name=PollingID[] value=', PollingID ,'>')
          FROM
               INTRANET_POLLING
          WHERE
               (Question like '%$keyword%')";

# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("DateStart", "DateEnd", "Question", "DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+3;
$li->title = $i_admintitle_polling;
$li->IsColOff = "IP25_table";

// TABLE COLUMN
$li->column_list .= "<th class='num_check'>#</th>\n";
$li->column_list .= "<th>".$li->column(0, $i_PollingDateStart)."</th>\n";
$li->column_list .= "<th>".$li->column(1, $i_PollingDateEnd)."</th>\n";
$li->column_list .= "<th>". $Lang['Polling']['ViewPoll'] ."</th>\n";
$li->column_list .= "<th>".$li->column(2, $i_PollingQuestion)."</th>\n";
$li->column_list .= "<th>".$li->column(3, $i_PollingDateModified)."</th>\n";
$li->column_list .= "<th>".$li->check("PollingID[]")."</th>\n";

### Button
$AddBtn 	= "<a href=\"javascript:checkNew('new.php')\" class='new'> " . $button_new . "</a>";
$delBtn 	= "<a href=\"javascript:checkRemove(document.form1,'PollingID[]','remove.php')\" class=\"tool_delete\">" . $button_delete . "</a>";
$editBtn 	= "<a href=\"javascript:checkEdit(document.form1,'PollingID[]','edit.php')\" class=\"tool_edit\">" . $button_edit . "</a>";
$searchTag = "<input name=\"keyword\" id=\"keyword\" type=\"text\" value=\"". stripslashes($keyword) ."\" onkeyup=\"Click_SearchBox(event, document.form1);\"/>";

### Title ###
$TAGS_OBJ[] = array($i_adminmenu_im_polling,"");
$MODULE_OBJ = $lpolling->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);   
?>

<form name="form1" method="get">


<div class="content_top_tool">
	<div class="Conntent_tool"><?=$AddBtn?></div>
	<div class="Conntent_search"><?=$searchTag?></div>
<br style="clear:both" />
</div>


<!--Table Content -->
<div class="table_board">
    
<!-- Filter & Indication  and table tool-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td valign="bottom">
	 <div class="common_table_tool">
	 <?=$editBtn?><?=$delBtn?> 
	 </div>                                 
	</td>
</tr>
</table>

<?=$li->display();?>

</div>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">

</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
