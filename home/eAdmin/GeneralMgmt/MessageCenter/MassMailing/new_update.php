<?php
# using: 

#################################
#	Date:	2011-06-23	Yuen
#			first version
#################################

	$PATH_WRT_ROOT = "../../../../../";
	
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/libimporttext.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	
	# iMail Plus enabled, assigned user only
	# to be changed
	if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-EmailMerge"])
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}

	$RecipientStudent = ($RecipientsTo=="STUDENT") ? "1" : "0";
	$RecipientParent = ($RecipientsTo=="PARENT") ? "1" : "0";
	$RecipientAddress = ($RecipientsTo=="ADDRESS") ? "1" : "0";
	$RecipientLoginID = ($RecipientsTo=="LOGINID") ? "1" : "0";
	
	intranet_auth();
	intranet_opendb();
	
	# Create a new interface instance
	$limc = new libmessagecenter();
	$libimport = new libimporttext();
	
	$Errors = array();
	
	# prepare the CSV data into Header and Data Array
	if (file_exists($FileCSV))
	{
		$csv_data = $libimport->GET_IMPORT_TXT($FileCSV);
		
		$csv_serialized = serialize($csv_data);
		
		//debug(strlen($csv_serialized));
		//die();
		//debug(strlen($csv_serialized), strlen($csv_serialized)/1024/1024, 1024*1024);
		
		// save to database
	} else
	{
		$Errors[] = "NoCSV";
	}
	
	
	if ($BodyFrom==2)
	{
		$HTML_template = get_file_content($FileHTML);
		//$htmlsize_org = strlen($HTML_template);
		if (trim($HTML_template)=="")
		{
			$Errors[] = "NoHTML";
		}
	} else
	{
		$HTML_template = stripslashes($MailHTMLContents);
	}
	
	if (isset($MassMailingID) && $MassMailingID>0)
	{
		# update
		$sql = "UPDATE INTRANET_MASS_MAILING " .
				"SET EmailTitle='{$EmailTitle}', ShowContentIn='{$ShowContent}', EmailBody='{$EmailBody}', ";
		$sql .= " CSVFormat='".$CSVFormat."', ";
		$sql .= " BodyFrom='".$BodyFrom."', ";
		
		/*
		if (trim($csv_serialized)!="")
		{
			$sql .= " CSVFile='".addslashes($csv_serialized)."', ";
		}
		*/
		if (trim($HTML_template)!="")
		{
			$sql .= " HTMLFile='".addslashes($HTML_template)."', ";
		}
		$sql .= "RecipientStudent='$RecipientStudent', RecipientParent='$RecipientParent', RecipientAddress='$RecipientAddress', RecipientLoginID='$RecipientLoginID', SendAsAdmin='$SendAsAdmin', " .
				"ModifyDate=now(), ModifiedBy='{$UserID}' " .
				"WHERE MassMailingID='{$MassMailingID}'";
		$result = $limc->db_db_query($sql);

		$msg = ($result)? "UpdateSuccess" : "UpdateUnsuccess"; 
	} elseif (sizeof($Errors)==0) 
	{
		# save to database
		$sql = "INSERT INTO INTRANET_MASS_MAILING" .
				"(EmailTitle, ShowContentIn, EmailBody, CSVFormat, BodyFrom, HTMLFile, RecipientStudent, RecipientParent, RecipientAddress, RecipientLoginID, SendAsAdmin, RecordStatus, InputBy, InputDate)" .
				"values" .
				"('$EmailTitle', '$ShowContent', '$EmailBody', '$CSVFormat', '$BodyFrom', '".addslashes($HTML_template)."', '$RecipientStudent', '$RecipientParent', '$RecipientAddress', '$RecipientLoginID', '$SendAsAdmin', '0', '".$UserID."', now())";

		$result = $limc->db_db_query($sql);
		
		$MassMailingID = $limc->db_insert_id();
		$msg = "AddSuccess";
	}

	$lfs = new libfilesystem();
	
	# store attachments
	if (file_exists($EmailAttachment))
	{
		# unzip it into a folder
		$attachment_folder = $intranet_root."/file/mailmerge_attachment";
		$lfs->folder_new($attachment_folder);

		$attachment_folder .= "/att".$MassMailingID;
		$lfs->folder_new($attachment_folder);
		if (substr(strtoupper($_FILES["EmailAttachment"]["name"]), -4)==".ZIP")
		{
			$lfs->file_unzip($EmailAttachment, $attachment_folder);
			# move files out from subfolder
	
			$SubFiles = $lfs->return_folderlist($attachment_folder);
			for ($fi=0; $fi<sizeof($SubFiles); $fi++)
			{
				$lfs->lfs_move($SubFiles[$fi], $attachment_folder);
			}
		} else
		{
			$lfs->lfs_move($EmailAttachment, $attachment_folder."/".$_FILES["EmailAttachment"]["name"]);
		}
	}
	
	# insert CSV data using concat in order to avoid 1MB memory limit
	if (trim($csv_serialized)!="")
	{
		$MaxStrings = (1024 * 1024);
		
			
		# get the previous file to remove
		$CSVFileObj = $limc->GetCSVFile($MassMailingID);
		$csv_file_path = $CSVFileObj["filepath"];
		if ($csv_file_path!="")
			$lfs->file_remove($intranet_root.$csv_file_path);

		# find previous file, remove it if exists
		if (strlen($csv_serialized)>=$MaxStrings)
		{
			# store in file
			$file_folder = $intranet_root."/file/mailmergecsv";			
			$lfs->folder_new($file_folder);
			$file_name = $MassMailingID."m".time().".dat";
			$lfs->file_write($csv_serialized, $file_folder."/".$file_name);
			$Filepathmark = "LOADFILE:::";
			$sql = "UPDATE INTRANET_MASS_MAILING SET CSVFile = '".addslashes($Filepathmark."/file/mailmergecsv/".$file_name)."' WHERE MassMailingID='{$MassMailingID}'";
			$result = $limc->db_db_query($sql);
		} else
		{
			$sql = "UPDATE INTRANET_MASS_MAILING SET CSVFile = '".addslashes($csv_serialized)."' WHERE MassMailingID='{$MassMailingID}'";
			$result = $limc->db_db_query($sql);
		}
	}

	
	
	intranet_closedb();
	
	if ($MassMailingID<>"" && $MassMailingID>0)
	{
		header("location: new_step2.php?msg=".$msg."&MassMailingID=".$MassMailingID);
	} elseif (sizeof($Errors)>0)
	{
		$err = implode(",", $Errors);
		header("location: new_step2.php?msg=AddUnsuccess&err=".$err);
	} else
	{
		header("location: index.php?msg=AddUnsuccess");
	}
	
?>