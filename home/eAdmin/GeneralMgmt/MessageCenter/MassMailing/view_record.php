<?php
# using: 

#################################
#	Date:	2012-07-24	Yuen
#			support print all records with option of showing sender, recipient
#
#	Date:	2011-10-17	Yuen
#			support multiple data in multiple rows for a person
#
#	Date:	2011-06-23	Yuen
#			first version
#################################

	$PATH_WRT_ROOT = "../../../../../";
	
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");

	# iMail Plus enabled, assigned user only
	# to be changed
	if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-EmailMerge"])
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}

	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
	
	intranet_auth();
	intranet_opendb();
	
	# Create a new interface instance
	$linterface = new interface_html("popup_no_layout.html");
	
	
	$limc = new libmessagecenter();
	
	
	# for EJ only!
	$limc->CaterBigForAJAX();
	
	
	
	$sql = "SELECT * " .
			"FROM INTRANET_MASS_MAILING_LOG " .
			"WHERE MassMailingID='{$MassMailingID}' " .
			"ORDER BY MassMailingLogID ASC ";
	$rows = $limc->returnArray($sql);

	for ($i=0; $i<sizeof($rows); $i++)
	{
		$rowObj = $rows[$i];
		$ClassName = $rowObj["Class"];
		$ClassNumber = $rowObj["ClassNumber"];
		$ClassInfo = ($ClassName!="" && $ClassNumber!="") ? "(".$ClassName."-".str_pad($ClassNumber, 2, "0", STR_PAD_LEFT).") " : "";
		$ParentIcon = ($rowObj["IsParent"]) ? "(".$Lang['Identity']['Parent'].") " : "";
		$SelectionArr[] = array($rowObj["MassMailingLogID"], $ClassInfo.$ParentIcon.$rowObj["RecipientEmail"]." [".$rowObj["InputDate"]."]");
		
		if ($rowObj["MassMailingLogID"]==$MassMailingLogID && isset($rows[$i+1]) && $rows[$i+1]["MassMailingLogID"]>0)
		{
			$NextMassMailingLogID = $rows[$i+1]["MassMailingLogID"];
		}
		if ($rowObj["MassMailingLogID"]==$MassMailingLogID && isset($rows[$i-1]) && $rows[$i-1]["MassMailingLogID"]>0)
		{
			$PreMassMailingLogID = $rows[$i-1]["MassMailingLogID"];
		}
	}
	if ($PreMassMailingLogID>0 && $PreMassMailingLogID!="")
	{
		$button_previous_shown = $linterface->GET_BTN(" &lt;&lt;", "button", "self.location='view_record.php?MassMailingID={$MassMailingID}&MassMailingLogID={$PreMassMailingLogID}'", "ViewPrevious", "title='".$Lang['MassMailing']['PreviousRecord']."'");
	} else
	{
		$button_previous_shown = $linterface->GET_BTN(" &lt;&lt;", "button", "", "ViewPrevious", "disabled='disabled' title='".$Lang['MassMailing']['PreviousRecord']."'");
	}
	//$button_next_shown = ($NextMassMailingLogID>0 && $NextMassMailingLogID!="") ? $linterface->GET_BTN($Lang['MassMailing']['NextRecord'], "button", "self.location='view_record.php?MassMailingID={$MassMailingID}&MassMailingLogID={$NextMassMailingLogID}'") : "";
	if ($NextMassMailingLogID>0 && $NextMassMailingLogID!="")
	{
		$button_next_shown = $linterface->GET_BTN("&gt;&gt; ", "button", "self.location='view_record.php?MassMailingID={$MassMailingID}&MassMailingLogID={$NextMassMailingLogID}'", "ViewNext", "title='".$Lang['MassMailing']['NextRecord']."'");
	} else
	{
		$button_next_shown = $linterface->GET_BTN("&gt;&gt; ", "button", "", "ViewNext", "disabled='disabled' title='".$Lang['MassMailing']['NextRecord']."'");
	}
	$select_records = $button_previous_shown . " " . $linterface->GET_SELECTION_BOX($SelectionArr, " id='MassMailingLogID' name='MassMailingLogID' onChange=\"this.form.submit()\" ", "", $MassMailingLogID) . $button_next_shown;
	
	$EmailBody = "";
	if ($PrintAll)
	{
		$sql = "SELECT * " .
			"FROM INTRANET_MASS_MAILING " .
			"WHERE MassMailingID='{$MassMailingID}' " ;
		$MassMailObj = $limc->returnArray($sql);
		$MailTitle = $MassMailObj[0]["EmailTitle"];
		for ($i=0; $i<sizeof($rows); $i++)
		{
			$rowObj = $rows[$i];
			if ($EmailBody!="")
			{
				$EmailBody .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="print_hide"><tr><td style="border-bottom:dashed orange 2px;">&nbsp;</td></tr></table>';
			}
			if ($rowObj['RecipientEmail']!="" && $rowObj['RecipientEmail']!="[CAMPUSMAIL]")
			{
				$ThisRecipient = $rowObj['RecipientEmail'];
				$ThisSender = $rowObj['SenderEmail'];
			} else
			{
				if ($rowObj['Class']!="" && $rowObj['ClassNumber']!="")
				{
					$ThisRecipient = $rowObj['Class'] . "-" . $rowObj['ClassNumber'];
				} else
				{
					$ThisRecipient = $rowObj['RecipientName'] ;
				}
				
				if ($rowObj['IsParent'])
				{
					$ThisRecipient .=  " (".$Lang['Identity']['Parent'] .")";
				}
				
				$ThisRecipient .= " " . $rowObj['RecipientEmail'];
				//debug_r($rowObj);
			}
			$EmailBody .= "\n";
			$Page_Break = ($i<sizeof($rows)-1) ? "style=\"page-break-after:always;\"" : "";
			$EmailBody .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" '.$Page_Break.' ><tr><td>';
			$EmailBody .= '<span class="Edit_Hide"><table border="0" cellpadding="5" cellspacing="0">';
			$EmailBody .= '<tr><td nowrap="nowrap">'.$Lang['Gamma']['Subject'].' :&nbsp;</td><td nowrap="nowrap"><b>'.$MailTitle.'</b></td></tr>';
			if ($ThisSender!="")
			{
				$EmailBody .= '<tr><td nowrap="nowrap">'.$Lang['Gamma']['Sender'].' :&nbsp;</td><td nowrap="nowrap">'.$ThisSender.'</td></tr>';	
			}
			$EmailBody .= '<tr><td nowrap="nowrap">'.$Lang['Gamma']['Recipient'].' :&nbsp;</td><td nowrap="nowrap">'.$ThisRecipient.'</td></tr>';
			$EmailBody .= '<tr><td nowrap="nowrap">'.$Lang['General']['Time'].' :&nbsp;</td><td nowrap="nowrap">'.$rowObj['InputDate'].'</td></tr>';
			$EmailBody .= '</table><hr /></span>';
			$EmailBody .= $rowObj['EmailBody'];
			$EmailBody .= $limc->CheckEmailAttachment($MassMailingID, $rowObj['Attachment']);
			$EmailBody .= '</td></tr></table>';
			$EmailBody .= "\n";
		}
	} else
	{
		# get the selected one only
		$sql = "SELECT * " .
				"FROM INTRANET_MASS_MAILING_LOG " .
				"WHERE MassMailingID='{$MassMailingID}' AND MassMailingLogID='{$MassMailingLogID}'";
		$rows = $limc->returnArray($sql);
		
		$EmailBody = $rows[0]['EmailBody'];
		$EmailBody .= $limc->CheckEmailAttachment($MassMailingID, $rows[0]['Attachment']);
	}
	
	
	
	$system_title = "eClass";
	
	# Start layout
	$linterface->LAYOUT_START();

	
	
	$print_all = $linterface->GET_BTN($Lang['Btn']['PrintAll'], "button", "jsPrintAll()", "PrintAll", "");
	$print_now = $linterface->GET_BTN($Lang['Btn']['Print'], "button", "window.print()", "PrintNow", "");
?>

<style type='text/css'>

html, body { margin: 0px; padding: 0px; } 


</style>


<script language="JavaScript">
function jsPrintAll()
{
	newWindow("view_record.php?MassMailingID=<?=$MassMailingID?>&PrintAll=1", 37);
}

function jsDisplayEmails(isShow)
{
	if (isShow)
	{
		$('.Edit_Hide').attr('style', '');
	} else
	{
		$('.Edit_Hide').attr('style', 'display:none');
	}
}
</script>

<form name="form1" id="form1" action="view_record.php" >
<table width="90%" border="0" cellpadding="5" cellspacing="0">
<?php if (!$PrintAll) { ?>
<tr class="print_hide">
	<td bgcolor='#DDDDDD' height='30' style="border-bottom: 2px #BBBBBB solid;"><?=$select_records?></td>
	<td bgcolor='#DDDDDD' height='30' style="border-bottom: 2px #BBBBBB solid;" align="right"><?=$print_all?></td>
</tr>
<?php } ?>
<tr class="print_hide">
	<td bgcolor='#DDDDDD' height='30' style="border-bottom: 2px #BBBBBB solid;" colspan="2" align="center"><input type="checkbox" onChange="jsDisplayEmails(this.checked)" checked="checked" id="ToShowInfo" /> <label for="ToShowInfo"><?= $Lang['MassMailing']['ShowInfo'] ?></label>
		<?=$print_now?></td>
</tr>
<tr>
	<td colspan="2" style="vertical-align:top;"><?=$EmailBody?></td>
</tr>
</table>
<input type="hidden" name="MassMailingID" value="<?=$MassMailingID?>" />
</form>
<?

	$linterface->LAYOUT_STOP();
	
	intranet_closedb();
?>