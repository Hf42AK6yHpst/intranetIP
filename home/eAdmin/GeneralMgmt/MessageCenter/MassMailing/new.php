<?php
# using: 

#################################
#	Date:	2017-03-07	Carlos
#			Always provide the [CSV by email address] option. Should be no dependency on campus mail or not. 
#
#	Date:	2013-11-05	Yuen
#			1. for campusmail case, there is no system admin account. thus, the sender is the person who create the mail merge record.
#			2. for campusmail case, webmail option is hide now
#
#	Date:	2012-03-19	Yuen
#			support online editing for mail contents
#
#	Date:	2011-10-17	Yuen
#			support multiple data in multiple rows for a person
#
#	Date:	2011-06-23	Yuen
#			first version
#################################

	$PATH_WRT_ROOT = "../../../../../";
	
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");

	# iMail Plus enabled, assigned user only
	# to be changed
	if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-EmailMerge"])
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}

	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
	include_once($PATH_WRT_ROOT."includes/libwebmail.php");
	
	intranet_auth();
	intranet_opendb();
	
	$liwm = new libwebmail();
	
	if ($plugin['imail_gamma'])
	{
	} elseif ($liwm->isExternalMailAvaliable())
	{		
	} else
	{
		# campusmail$sql = "SELECT UserID, ClassName, ClassNumber FROM INTRANET_USER where RecordType=2 AND RecordStatus=1 AND ClassName<>'' AND ClassNumber<>''";
		$UseCampusMail = true;
	}
	//debug($UseCampusMail);
	# Create a new interface instance
	$linterface = new interface_html();
	$limc = new libmessagecenter();
	
	# Page title
	$CurrentPageArr['eAdmineMassMailing'] = 1;
	$CurrentPage = "PageMassMailing";
// 	$PAGE_TITLE = $Lang['eSurvey']['SurveyList'];
	$TAGS_OBJ[] = array($Lang['Header']['Menu']['MassMailing']);
	$MODULE_OBJ = $limc->GET_MODULE_OBJ_ARR();
	
	$PAGE_NAVIGATION[] = array($Lang['Header']['Menu']['MassMailing'], "javascript:history.back();");
	$PAGE_NAVIGATION[] = (isset($MassMailingID) && $MassMailingID>0) ? array($Lang['MassMailing']['Edit']) : array($Lang['MassMailing']['New'] . " (".$Lang['MassMailing']['NewStep1'].")");
	
	$DisplayCSVUpload = "";
	$DisplayHTMLUpload = "";
	if (isset($MassMailingID) && $MassMailingID>0)
	{	
		# load from database
		$sql = "SELECT * FROM INTRANET_MASS_MAILING WHERE MassMailingID='$MassMailingID'";
		$rows = $limc->returnArray($sql);
		
		if (sizeof($rows)==0)
		{
			$MassMailingID = "";
		}
		
		$MassMailingObj = $rows[0];
		
		$EmailTitle = str_replace('"', "&quot;", $MassMailingObj["EmailTitle"]);
		$ShowContent = $MassMailingObj["ShowContentIn"];

		$EmailBody = $MassMailingObj["EmailBody"];
		$CSVFormat = $MassMailingObj["CSVFormat"];
		$RecipientStudent = $MassMailingObj["RecipientStudent"];
		$RecipientParent = $MassMailingObj["RecipientParent"];
		$RecipientAddress = $MassMailingObj["RecipientAddress"];
		$RecipientLoginID = $MassMailingObj["RecipientLoginID"];
		$SendAsAdmin = $MassMailingObj["SendAsAdmin"];
		$BodyFrom = ($MassMailingObj["BodyFrom"]=="") ? 2 : $MassMailingObj["BodyFrom"];
		$encrypted_MassMailingID = getEncryptedText($MassMailingID);
		
		if ($BodyFrom==1)
		{
			$MailHTMLContents = $MassMailingObj["HTMLFile"];
		}

		if (strlen($MassMailingObj["CSVFile"])>1)
		{
			//$CSV_download = "<a href='download.php?file=CSV&MassMailingID={$MassMailingID}'>".$Lang['Button']['Download']."</a> | <a href='javascript:jsChangeCSV()' >".$Lang['MassMailing']['BtnChange']."</a>";
			$CSV_download = "<a href='download.php?file=CSV&MassMailingID_e={$encrypted_MassMailingID}'>".$Lang['Button']['Download']."</a> | <a href='javascript:jsChangeCSV()' >".$Lang['MassMailing']['BtnChange']."</a>";
			$DisplayCSVUpload = "none";		
			
		}
		if (strlen($MassMailingObj["HTMLFile"])>1 && $BodyFrom==2)
		{
			//$HTML_download = "<a href='download.php?file=HTML&MassMailingID={$MassMailingID}'>".$Lang['Button']['Download']."</a> | <a href='javascript:jsChangeHTML()' >".$Lang['MassMailing']['BtnChange']."</a>";
			$DisplayHTMLUpload = "none";	
			$HTML_download = "<a href='download.php?file=HTML&MassMailingID_e={$encrypted_MassMailingID}'>".$Lang['Button']['Download']."</a> | <a href='javascript:jsChangeHTML()' >".$Lang['MassMailing']['BtnChange']."</a>";		
		}
		
		$EmailAttachment = $limc->GetAttachmentSummary($MassMailingID, true, "");
		$UploadMoreBtn = "";
		if ($EmailAttachment!="")
		{
			$DisplayAttachmentsUpload = "none";
			$UploadMoreBtn = " | <a href='javascript:jsUploadMore()' >".$Lang['MassMailing']['AttachmentsUploadMore']."</a>";
		} else
		{
			
		}
	}
	
	if ($ShowContent==2)
	{
		# as attachment
		 $ShowContentChecked2 = "checked='checked'";
	} else
	{
		# in mail body
		$ShowContentChecked1 = "checked='checked'";
	}
	
	if ($CSVFormat==2)
	{
		 $CSVFormat2Checked = "checked='checked'";
		 $CSV_sample1_display = "style='display:none'";
		 $RecipientAddressDisplay = "style='display:none'";
	} else
	{
		 $CSVFormat1Checked = "checked='checked'";
		 $CSV_sample2_display = "style='display:none'";		
	}
	
	if ($BodyFrom==2)
	{
		 $MailBody2Checked = "checked='checked'";
		 $Mail_online_edit_display = "style='display:none'";
	} else
	{
		 $MailBody1Checked = "checked='checked'";
		 $Mail_HTML_upload_display = "style='display:none'";		
	}
	
	
	
	if (isset($SendAsAdmin) && $SendAsAdmin==0)
	{
		# yourself
		 $SendAsAdminCheck0 = "checked='checked'";
	} else
	{
		# as sys admin
		$SendAsAdminCheck1 = "checked='checked'";
	}
	
	$RecipientStudentCheck = (isset($RecipientStudent) && !$RecipientStudent) ? "" : "checked='checked'";
	$RecipientParentCheck = ($RecipientParent) ? "checked='checked'" : "";
	$RecipientAddressCheck = ($RecipientAddress) ? "checked='checked'" : "";
	$RecipientLoginIDCheck = ($RecipientLoginID) ? "checked='checked'" : "";
	
	
	
	# Start layout
	$linterface->LAYOUT_START();
	
	$sample_csv_file1 = GET_CSV("sample_data.csv", '', false);
	$sample_csv_file1_userlogin = GET_CSV("sample_data_userlogin.csv", '', false);
	$sample_csv_file1_email = GET_CSV("sample_data_format1_email.csv", '', false);
	$sample_csv_file2 = GET_CSV("sample_data_format2.csv", '', false);
	$sample_csv_file2_email = GET_CSV("sample_data_format2_email.csv", '', false);
	$sample_csv_file2_userlogin = GET_CSV("sample_data_format2_userlogin.csv", '', false);
	$sample_template_file = GET_CSV("sample_template.htm", '', false);
	
?>




<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script> 
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.blockUI.js"></script> 
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.tablednd_0_5.js"></script> 
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.cookies.2.2.0.js"></script> 

<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css" type="text/css" media="screen" /> 

<script language="javascript">

function checkform(obj)
{

        if(Trim(obj.EmailTitle.value)=="")
        {
                alert("<?=$Lang['MassMailing']['ProblemNoSubject']?>");
                obj.EmailTitle.focus();
                return false;
		}
		
}

function jsChangeCSV()
{
	$('#UploadCSV').show();
}


function jsChangeHTML()
{
	$('#UploadHTML').show();
}

function ShowCSVFormat(FormatNo)
{
	if (FormatNo==1)
	{
		//$('#RecipientAddressDiv').show();
		$('#CSV_Sample2').hide();
		$('#CSV_Sample1').show();
	} else
	{
		$('input[name=RecipientsTo]').get(0).checked = true;
		//$('#RecipientAddressDiv').hide();
		$('#CSV_Sample1').hide();
		$('#CSV_Sample2').show();
	}
}


function ShowMailBody(FormatNo)
{
	if (FormatNo==1)
	{
		$('#Mail_HTML_Upload').hide();
		$('#Mail_Online_Edit').show();
	} else
	{
		$('#Mail_Online_Edit').hide();
		$('#Mail_HTML_Upload').show();
	}
}

function jsUploadMore(FormatNo)
{
	$('#UploadAttachments').show();
}




function viewAttachments()
{
	var mailID = "<?=$MassMailingID?>";
	//alert(mailID);
}

function jsUpdateAttachmentTotal(AttachmentTotal)
{
	$('#AttachmentSizeInfo').html(AttachmentTotal);
}
</script>

<form name="form1" action="new_update.php" method="post" enctype="multipart/form-data" onSubmit="return checkform(this);">
	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($msg, $xmsg);?></td>
	</tr>
	
		<tr>
			<td class="navigation">
				<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr><td><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?></td></tr>
				</table>
			</td>
		</tr>
	</table>
	
	<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
		
		<!-- Title -->
		<tr>
		   <td class="tabletext formfieldtitle" width="20%" valign="top"  nowrap='nowrap'><?=$Lang['Gamma']['Subject']?> <span class="tabletextrequire">*</span></td>
			<td align="left" valign="top">
			  <input type="text" class="textboxtext" name="EmailTitle" value="<?=$EmailTitle?>" size="100%" />
			</td>
		</tr>
		
		
		
		<!-- show in  -->
		<!--
		<tr>
			<td class="tabletext formfieldtitle" valign="top"  nowrap='nowrap'><?=$Lang['MassMailing']['ShowContent']?> <span class="tabletextrequire">*</span></td>
			<td align="left" valign="top">
			  <input type="radio" name="ShowContent" id="ShowContent1" onClick="$('#'+'EmailBodyTr').hide();" value="1" <?=$ShowContentChecked1?> /> <label for="ShowContent1"><?=$Lang['MassMailing']['InMailBody']?></label> &nbsp;
			 <input type="radio" name="ShowContent" id="ShowContent2" disabled="disabled" onClick="$('#'+'EmailBodyTr').show();" value="2" <?=$ShowContentChecked2?> /> <label for="ShowContent2"><?=$Lang['MassMailing']['AsAttachment']?></label>
			</td>
		</tr>
		-->
		
		
		<!-- Description -->
		<tr style="display:none" id="EmailBodyTr">
			<td class="tabletext formfieldtitle" valign="top"  nowrap='nowrap'><?=$Lang['Gamma']['Message']?></td>
			<td align="left" valign="top"><?=$linterface->GET_TEXTAREA("EmailBody", $EmailBody, "86",10)?></td>
		</tr>
		
		
		<!-- CSV -->
		<tr>
			<td class="tabletext formfieldtitle" valign="top"  nowrap='nowrap'><?=$Lang['MassMailing']['CSVFile']?> <span class="tabletextrequire">*</span></td>
			<td align="left" valign="top">
			<input type="radio" name="CSVFormat" id="CSVFormat1" onClick="ShowCSVFormat(1)" value="1" <?=$CSVFormat1Checked?> /> <label for="CSVFormat1"><?=$Lang['MassMailing']['CSVFormat1']?></label> &nbsp;
			<input type="radio" name="CSVFormat" id="CSVFormat2" onClick="ShowCSVFormat(2)" value="2" <?=$CSVFormat2Checked?> /> <label for="CSVFormat2"><?=$Lang['MassMailing']['CSVFormat2']?></label>
			 <br /><?=$CSV_download?>
			 
			<div id="UploadCSV" style="display:<?=$DisplayCSVUpload?>">
				<input type="file" name="FileCSV" size="60" />
				<div id="CSV_Sample1" <?=$CSV_sample1_display?>><a class='tablelink' href="<?=$sample_csv_file1?>"> <?=$Lang['General']['ClickHereToDownloadSample'].$Lang['MassMailing']['CSV_CLASS_NO'] ?></a>

						<br /><a class='tablelink' href="<?=$sample_csv_file1_email?>"> <?=$Lang['General']['ClickHereToDownloadSample'].$Lang['MassMailing']['CSV_Email']?></a>

						<br /><a class='tablelink' href="<?=$sample_csv_file1_userlogin?>"> <?=$Lang['General']['ClickHereToDownloadSample'].$Lang['MassMailing']['UserLogin']?></a>
				</div>
				<div id="CSV_Sample2" <?=$CSV_sample2_display?>><a class='tablelink' href="<?=$sample_csv_file2?>"> <?=$Lang['General']['ClickHereToDownloadSample'].$Lang['MassMailing']['CSV_CLASS_NO']?></a>

						<br /><a class='tablelink' href="<?=$sample_csv_file2_email?>"> <?=$Lang['General']['ClickHereToDownloadSample'].$Lang['MassMailing']['CSV_Email']?></a>

						<br /><a class='tablelink' href="<?=$sample_csv_file2_userlogin?>"> <?=$Lang['General']['ClickHereToDownloadSample'].$Lang['MassMailing']['UserLogin']?></a>
				</div>
			</div>
			</td>
		</tr>
		
		<!-- Attachments
		list the files for view, remove (delete all)		
		upload more files
		 -->
		<tr style="display:" id="EmailAttachmentTr">
			<td class="tabletext formfieldtitle" valign="top"  nowrap='nowrap'><?=$Lang['MassMailing']['AttachmentsZIP']?></td>
			<td align="left" valign="top">
				<?= $EmailAttachment  . $UploadMoreBtn?>
			<div id="UploadAttachments" style="display:<?=$DisplayAttachmentsUpload?>">
				<input type="file" name="EmailAttachment" size="60" />
				<br /><span class='tabletextremark'>* <?=$Lang['MassMailing']['Attachments_remark']?></span>
			</div>
			</td>
		</tr>
		
		
		
		<!-- HTML -->
		<tr>
			<td class="tabletext formfieldtitle" valign="top"  nowrap='nowrap'><?=$Lang['MassMailing']['MailBody']?> <span class="tabletextrequire">*</span></td>
			<td align="left" valign="top">
			<input type="radio" name="BodyFrom" id="BodyFrom1" onClick="ShowMailBody(1)" value="1" <?=$MailBody1Checked?> /> <label for="MailBody1"><?=$Lang['MassMailing']['HTMLContents']?></label> &nbsp;
			<input type="radio" name="BodyFrom" id="BodyFrom2" onClick="ShowMailBody(2)" value="2" <?=$MailBody2Checked?> /> <label for="MailBody2"><?=$Lang['MassMailing']['HTMLFile']?> (UTF-8)</label> <br />
 
			<div id="Mail_HTML_Upload" <?=$Mail_HTML_upload_display?>>
				<?=$HTML_download?>
				<div id="UploadHTML" style="display:<?=$DisplayHTMLUpload?>">
					<input type="file" name="FileHTML" size="60" />
					<br /><a class='tablelink' href="sample_template.htm" target="sample"> <?=$Lang['General']['ClickHereToDownloadSample']?></a>
				</div>
			</div>
			<div id="Mail_Online_Edit" <?=$Mail_online_edit_display?>>
				<?php
				include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
				$objHtmlEditor = new FCKeditor ( 'MailHTMLContents' , "100%", "500", "", "", intranet_undo_htmlspecialchars($MailHTMLContents));
				$objHtmlEditor->Create();
				?>
				<br /><span class='tabletextremark'>* <?=$Lang['MassMailing']['HTMLContents_Code_remark']?></span>
			</div>
			</td>
		</tr>
		
		
		<tr>
			<td class="tabletext formfieldtitle" valign="top" nowrap='nowrap'><?=$Lang['Gamma']['Recipient']?> <span class="tabletextrequire">*</span></td>
			<td align="left" valign="top">
			  <div><input type="radio" name="RecipientsTo" id="RecipientStudent" value="STUDENT" <?=$RecipientStudentCheck?> /> <label for="RecipientStudent"><?=$Lang['MassMailing']['ToStudent']?></label> &nbsp;</div>
			  <div><input type="radio" name="RecipientsTo" id="RecipientParent" value="PARENT" <?=$RecipientParentCheck?> /> <label for="RecipientParent"><?=$Lang['MassMailing']['ToParent']?></label> &nbsp;</div>

			  <div id="RecipientAddressDiv" ><input type="radio" name="RecipientsTo" id="RecipientAddress" value="ADDRESS" <?=$RecipientAddressCheck?> /> <label for="RecipientAddress"><?=$Lang['MassMailing']['GivenEmails']?></label></div>

			  <div><input type="radio" name="RecipientsTo" id="RecipientLoginID" value="LOGINID" <?=$RecipientLoginIDCheck?> /> <label for="RecipientLoginID"><?=$Lang['MassMailing']['GivenUserLoginID']?></label></div>
			</td>
		</tr>
		
		
		<?php if ($plugin['webmail'] || $plugin['imail_gamma'] || $sys_custom['EmailMergeAllowSelectSender']) { ?>
		<tr>
			<td class="tabletext formfieldtitle" valign="top"  nowrap='nowrap'><?=$Lang['Gamma']['SendersMailAddress']?> <span class="tabletextrequire">*</span></td>
			<td align="left" valign="top">
			  <input type="radio" name="SendAsAdmin" id="SendAsAdmin1" value="1"  <?=$SendAsAdminCheck1?> /> <label for="SendAsAdmin1"><?=$Lang['MassMailing']['SysAdmin']?> (<?=get_webmaster()?>)</label> <br />
			  <input type="radio" name="SendAsAdmin" id="SendAsAdmin0" value="0" <?=$SendAsAdminCheck0?> /> <label for="SendAsAdmin0"><?=$Lang['MassMailing']['Yourself']?> (<?=$limc->getSenderEmail(0, $UserID);?>) </label>
			</td>
		</tr>
		<?php } else { ?>
		<input type="hidden" name="SendAsAdmin" id="SendAsAdmin" value="0" />
		<?php } ?>
	</table>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td>
				<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>
					<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
					<tr>
						<td align="center">
							<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit")?>&nbsp;
							<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "javascript:history.back()")?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

<input type="hidden" name="ShowContent" id="ShowContent" value="1" />

<?php
if (isset($MassMailingID) && $MassMailingID>0)
{
	echo "<input type='hidden' name='MassMailingID' value='{$MassMailingID}' />";
}
?>

</form>
<?=	$linterface->FOCUS_ON_LOAD("form1.EmailTitle") ?>
<?

	$linterface->LAYOUT_STOP();
	
	intranet_closedb();
?>