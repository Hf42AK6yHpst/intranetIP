<?php

# using: 

#################################
#	Date:	2016-02-03	Kenneth
#			modify sendMassMail() - add param $_rowNumber, to log current row number to db
#
#	Date:	2015-12-11	Omas
#			added show user Standard display name if they didn't set in imail
#
#	Date:	2011-06-23	Yuen
#			first version
#################################



	# for testing
	//$SetAsCampusMail = true;
	

	$PATH_WRT_ROOT = "../../../../../";
	
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/libemail.php");
	include_once($PATH_WRT_ROOT."includes/libsendmail.php");
	
	# iMail Plus enabled, assigned user only
	# to be changed
	if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-EmailMerge"])
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}

	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
	include_once($PATH_WRT_ROOT."includes/libwebmail.php");
	
	intranet_auth();
	intranet_opendb();
	
	# Create a new interface instance
	$limc = new libmessagecenter();
	$libemail = new libsendmail();
	$liwm = new libwebmail();
	
	
	# for EJ only!
	$limc->CaterBigForAJAX();
	
	
	# 10 emails per job!!!
	//$EmailInterval = 10; 
	
	$sendSingleMail = $_GET['sendSingleMail'];

	
	$HTML_template = "";
	if ($MassMailingID>0)
	{		
		
		# load from database
		$sql = "SELECT * FROM INTRANET_MASS_MAILING WHERE MassMailingID='$MassMailingID'";
		$rows = $limc->returnArray($sql,null,1);
		$MassMailingObj = $rows[0];
		$EmailTitle = $MassMailingObj["EmailTitle"];
		$ShowContent = $MassMailingObj["ShowContentIn"];
		$EmailBody = $MassMailingObj["EmailBody"];
		$RecipientStudent = $MassMailingObj["RecipientStudent"];
		$RecipientParent = $MassMailingObj["RecipientParent"];
		$RecipientAddress = $MassMailingObj["RecipientAddress"];
		$RecipientLoginID = $MassMailingObj["RecipientLoginID"];
		$SendAsAdmin = $MassMailingObj["SendAsAdmin"];
		$EmailTitle = $MassMailingObj["EmailTitle"];
		
		$CSVFormat = $MassMailingObj["CSVFormat"];
		$CSVFileObj = $limc->GetCSVFile($MassMailingID, $MassMailingObj["CSVFile"]);
		$csv_data = $CSVFileObj["data"];
		

		
		$RowHeader = $csv_data[0];
		
		for ($i=0; $i<sizeof($RowHeader); $i++)
		{
			// ClassNo = ClassNumber
			if (strtoupper(trim($RowHeader[$i]))=="CLASSNO")
			{
				$HeaderArr[] = "CLASSNUMBER";
			} else
			{
				$HeaderArr[] = strtoupper(trim($RowHeader[$i]));
			}
		}
		
		for ($i=1; $i<sizeof($csv_data); $i++)
		{
			$RowArr = $csv_data[$i];
			
			for ($j=0; $j<sizeof($RowArr); $j++)
			{
				$CSVDataArr[$i-1][$HeaderArr[$j]] = $RowArr[$j];
			}
		}
		
		# Format 2
		if ($CSVFormat==2)
		{
			//consolidate data from multiple rows
			// TYPE, Award_Event or CONTENT
			
			$row_index = 0;
			for ($i=0; $i<sizeof($CSVDataArr); $i++)
			{	
				if ($RecipientAddress)
				{
					$iClass = $CSVDataArr[$i]['EMAIL'];
					$iClassNo = "-";
				} elseif ($RecipientLoginID)
				{
					$iClass = $CSVDataArr[$i]['USERLOGIN'];
					$iClassNo = "-";
				} else
				{
					$iClass = $CSVDataArr[$i]['CLASS'];
					$iClassNo = $CSVDataArr[$i]['CLASSNUMBER'];					
				}
				//debug($iClass, $iClassNo);
				$iType = $CSVDataArr[$i]['TYPE'];
				$iContent = $CSVDataArr[$i]['CONTENT'];
				if ($sys_custom["ttca_mailmerge"])
				{
					if ($CSVDataArr[$i]['AWARD_EVENT']!="" && $CSVDataArr[$i]['AWARD_EVENT']!="-" && $CSVDataArr[$i]['AWARD_EVENT']!="--" && $CSVDataArr[$i]['AWARD_EVENT']!="---")
					{
						$iContent = $CSVDataArr[$i]['AWARD_EVENT']; 
					} elseif ($CSVDataArr[$i]['PUNISHMENT_EVENT']!="" && $CSVDataArr[$i]['PUNISHMENT_EVENT']!="-" && $CSVDataArr[$i]['PUNISHMENT_EVENT']!="--" && $CSVDataArr[$i]['PUNISHMENT_EVENT']!="---")
					{
						$iContent = $CSVDataArr[$i]['PUNISHMENT_EVENT'];
					}
				} 
				//$iContent = ($CSVDataArr[$i]['AWARD_EVENT']!="") ? $CSVDataArr[$i]['AWARD_EVENT'] : $CSVDataArr[$i]['CONTENT'];
				$DataAsso[$iClass][$iClassNo][$iType] .= ($DataAsso[$iClass][$iClassNo][$iType]!="") ? "<br />".$iContent : $iContent;
				if ($sys_custom["ttca_mailmerge"])
				{
					$DataAsso[$iClass][$iClassNo]["NAME"] = "({$iClass}".str_pad($iClassNo, 2, "0", STR_PAD_LEFT).") ".$CSVDataArr[$i]['CHNAME'];
				} else
				{
					if ($CSVDataArr[$i]['NAME']!="")
					{
						$DataAsso[$iClass][$iClassNo]["NAME"] = $CSVDataArr[$i]['NAME'];
					}
					if ($CSVDataArr[$i]['STUDENTNAME']!="")
					{
						$DataAsso[$iClass][$iClassNo]["STUDENTNAME"] = $CSVDataArr[$i]['STUDENTNAME'];
					}					
				}
			}
			
			if (is_array($DataAsso) && sizeof($DataAsso)>0)
			{
				unset($CSVDataArr);
				unset($HeaderArr);	
				$Arr_index = 0;
				foreach($DataAsso as $iClass=>$DataStudents)
				{
					if ($Arr_index==0)
					{
						if ($RecipientAddress)
						{
							$HeaderArr[] = "EMAIL";
						} elseif ($RecipientLoginID)
						{
							$HeaderArr[] = "USERLOGIN";
						} else
						{
							$HeaderArr[] = "CLASS";		
						}
						
					}
					if (is_array($DataStudents) && sizeof($DataStudents)>0)
					{
						foreach($DataStudents as $iClassNo=>$DataOneStudent)
						{
							if ($Arr_index==0 && !$RecipientAddress && !$RecipientLoginID)
							{
								$HeaderArr[] = "CLASSNUMBER";
								$HeaderArr[] = "CLASSNO";
							}
							
							if ($RecipientAddress)
							{
								$CSVDataArr[$Arr_index]["EMAIL"] = $iClass;
							} elseif ($RecipientLoginID)
							{
								$CSVDataArr[$Arr_index]["USERLOGIN"] = $iClass;
							} else
							{
								$CSVDataArr[$Arr_index]["CLASS"] = $iClass;
								$CSVDataArr[$Arr_index]["CLASSNUMBER"] = $iClassNo;
							}
							if (is_array($DataOneStudent) && sizeof($DataOneStudent)>0)
							{
								foreach($DataOneStudent as $iType=>$iContent)
								{
									$CSVDataArr[$Arr_index][$iType] = $iContent;
									if (!in_array($iType, $HeaderArr))
									{
										$HeaderArr[] = $iType;
									}
								}
							}
							$Arr_index++;
						}
					}
				}
				if ($sys_custom["ttca_mailmerge"])
				{
					if (!in_array("NAME", $HeaderArr))
						$HeaderArr[] = "NAME";
					if (!in_array("D_ASS", $HeaderArr))
						$HeaderArr[] = "D_ASS";
					if (!in_array("PLA", $HeaderArr))
						$HeaderArr[] = "PLA";
					if (!in_array("WLA", $HeaderArr))
						$HeaderArr[] = "WLA";
					if (!in_array("MLA", $HeaderArr))
						$HeaderArr[] = "MLA";
					if (!in_array("DLA", $HeaderArr))
						$HeaderArr[] = "DLA";
					if (!in_array("P", $HeaderArr))
						$HeaderArr[] = "P";
					if (!in_array("W", $HeaderArr))
						$HeaderArr[] = "W";
					if (!in_array("W", $HeaderArr))
						$HeaderArr[] = "M";
					if (!in_array("W", $HeaderArr))
						$HeaderArr[] = "D";
				}
			}
		}
		
		$HTML_template = $MassMailingObj["HTMLFile"];

	}
	
	if($sendSingleMail){
		$rowNumber = $_GET['sendSingleRowNumber'];
		$tempArray = $CSVDataArr;
		$CSVDataArr= array();
		$CSVDataArr[0] = $tempArray[$rowNumber-1];
	}
	
	if (sizeof($csv_data)>1000)
	{
		$EmailInterval = 50;
	} elseif (sizeof($csv_data)>500)
	{
		$EmailInterval = 20;
	} elseif (sizeof($csv_data)>100)
	{
		$EmailInterval = 10;
	} else
	{
		$EmailInterval = 5;
	}
	
	if ($ProgressIndex=="" || $ProgressIndex<0)
	{
		$ProgressIndex = 0;
		
	}
	
	$ProgressIndexInstant = 0;
	# parse the tags using regular expression to remove unnecessary tag inside [=..=]
	$HTML_template = $limc->ParseTemplateTags($HTML_template,$HeaderArr);

	$EmailSender = $limc->getSenderEmail($SendAsAdmin, $UserID);
	
	$EmailSenderName = "";
	$headers_preset = "";
	if (!$SendAsAdmin && $plugin['imail_gamma'])
	{
		include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
		$imap_gamma = new imap_gamma();
		$Preference = $imap_gamma->Get_Preference($ImapEmailAddress);
		$ReplyAddress = $Preference["ReplyEmail"];
		$DisplayName = $Preference["DisplayName"];
		if (!empty($ReplyAddress))
		{
			$headers_preset = "Reply-To: $ReplyAddress";
		}
		if (!empty($DisplayName))
		{
			$EmailSenderName = $DisplayName;
		}
		else{
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$luser = new libuser($UserID);
			$EmailSenderName = $luser->StandardDisplayName;
			unset($luser);
		}		
	}
	else if($SendAsAdmin){
		$EmailSenderName = $Lang['General']['SystemAdmin'];
	}
	
	# get emails
	if ($RecipientLoginID)
	{		
		# get emails from iMail plus, iMail 1.2, alternative emails
		if (!$SetAsCampusMail && $plugin['imail_gamma'])
		{
			$sql = "SELECT UserID, ImapUserEmail, UserLogin, EnglishName, ClassName, ClassNumber FROM INTRANET_USER where RecordStatus=1";
		} elseif (!$SetAsCampusMail && $liwm->isExternalMailAvaliable())
		{
			$sql = "SELECT a.UserID, a.UserLogin, a.EnglishName, a.ClassName, a.ClassNumber, b.ACL FROM INTRANET_USER AS a INNER JOIN INTRANET_SYSTEM_ACCESS AS b ON (a.UserID = b.UserID) WHERE RecordStatus='1' order by a.UserLogin";
		} else
		{
			# campusmail
			$sql = "SELECT UserID, UserLogin, EnglishName, ClassName, ClassNumber FROM INTRANET_USER where RecordStatus=1";
			$UseCampusMail = true;
		}
		$rows = $limc->returnArray($sql,null,1);
		
		# prepare array with emails
		for ($i=0; $i<sizeof($rows); $i++)
		{
			# user email for iMail 1.2
			if (!$plugin['imail_gamma'] && $rows[$i]["UserLogin"]!="")
			{
				if ($rows[$i]["ACL"]==1 || $rows[$i]["ACL"]==3)
				{
					$rows[$i]["ImapUserEmail"] = $rows[$i]["UserLogin"]."@".$liwm->mailaddr_domain;
				}
			}
			$CurUser = $rows[$i];
			if ($CurUser["UserLogin"]!="")
			{
				$AllUsers[$CurUser["UserLogin"]] = ($UseCampusMail) ? array("UserID"=>$CurUser["UserID"], "ImapUserEmail"=>"[CAMPUSMAIL]", "UserName"=>$CurUser["EnglishName"], "ClassName"=>$CurUser["ClassName"], "ClassNumber"=>$CurUser["ClassNumber"]): array("UserID"=>$CurUser["UserID"], "ImapUserEmail"=>$CurUser["ImapUserEmail"], "UserName"=>$CurUser["EnglishName"], "ClassName"=>$CurUser["ClassName"], "ClassNumber"=>$CurUser["ClassNumber"]);
			}
		}
		
	} else
	{
		if (!$SetAsCampusMail && $plugin['imail_gamma'])
		{
			$sql = "SELECT UserID, ImapUserEmail, ClassName, ClassNumber, EnglishName FROM INTRANET_USER where RecordType=2 AND RecordStatus=1 AND ClassName<>'' AND ClassNumber<>''";
		} elseif (!$SetAsCampusMail && $liwm->isExternalMailAvaliable())
		{
			$sql = "SELECT a.UserID, a.UserLogin, a.ClassName, a.ClassNumber, a.EnglishName, b.ACL FROM INTRANET_USER AS a INNER JOIN INTRANET_SYSTEM_ACCESS AS b ON (a.UserID = b.UserID) WHERE  a.RecordType='2' AND RecordStatus='1' AND a.ClassName<>'' AND a.ClassNumber<>''";
		} else
		{
			# campusmail
			$sql = "SELECT UserID, ClassName, ClassNumber, EnglishName FROM INTRANET_USER where RecordType=2 AND RecordStatus=1 AND ClassName<>'' AND ClassNumber<>''";
			$UseCampusMail = true;
		}
		$rows = $limc->returnArray($sql,null,1);
		
		for ($i=0; $i<sizeof($rows); $i++)
		{
			# user email for iMail 1.2
			if (!$plugin['imail_gamma'] && $rows[$i]["UserLogin"]!="")
			{
				if ($rows[$i]["ACL"]==1 || $rows[$i]["ACL"]==3)
				{
					$rows[$i]["ImapUserEmail"] = $rows[$i]["UserLogin"]."@".$liwm->mailaddr_domain;
				}
			}
			$CurUser = $rows[$i];
			if ($CurUser["ClassName"]!="" && $CurUser["ClassNumber"]!="")
			{
				$StudentsArr[$CurUser["ClassName"]][$CurUser["ClassNumber"]] = ($UseCampusMail) ? array("UserID"=>$CurUser["UserID"], "ImapUserEmail"=>"[CAMPUSMAIL]", "UserName"=>$CurUser["EnglishName"]): array("UserID"=>$CurUser["UserID"], "ImapUserEmail"=>$CurUser["ImapUserEmail"], "UserName"=>$CurUser["EnglishName"]);
				$StudentsArrByID[$CurUser["UserID"]] = array("ClassName"=>$CurUser["ClassName"], "ClassNumber"=>$CurUser["ClassNumber"]);
			}
		}
	}
	//debug(sizeof($rows),sizeof($StudentsArr));
	//debug_r($StudentsArr);
	
	if (!$RecipientAddress)
	{
		$limc->ChangeEmailMethod($UseCampusMail);
	}
	
	# check emails for students
	$CountStudents = 0;
	$CountAddress = 0;
	if (true)
	{
		for ($i=0; $i<sizeof($CSVDataArr); $i++)
		{
			$ToEmail = $CSVDataArr[$i]["EMAIL"];
			$UserLogin = $CSVDataArr[$i]["USERLOGIN"];
			$StdClass = (is_array($AllUsers) && sizeof($AllUsers)>0) ? $AllUsers[$UserLogin]["ClassName"]: $CSVDataArr[$i]["CLASS"];
			$StdClassNumber = (is_array($AllUsers) && sizeof($AllUsers)>0) ? $AllUsers[$UserLogin]["ClassNumber"]: $CSVDataArr[$i]["CLASSNUMBER"];
			$EmailAttachment = $CSVDataArr[$i]["EMAILATTACHMENT"];
			$UserName =  (is_array($AllUsers) && sizeof($AllUsers)>0) ? $AllUsers[$UserLogin]["UserName"]: $StudentsArr[$StdClass][(int)$StdClassNumber]["UserName"];
			$StudentID = (is_array($AllUsers) && sizeof($AllUsers)>0) ? $AllUsers[$UserLogin]["UserID"]: $StudentsArr[$StdClass][(int)$StdClassNumber]["UserID"];

			if (trim($ToEmail)=="")
			{
				$ToEmail = (is_array($AllUsers) && sizeof($AllUsers)>0) ? trim($AllUsers[$UserLogin]["ImapUserEmail"]): trim($StudentsArr[$StdClass][(int)$StdClassNumber]["ImapUserEmail"]);
			}

			if ($RecipientStudent || $RecipientAddress || $RecipientLoginID )
			{		
				
				if ($ToEmail=="")
				{
					$ErrorNoEmailStudent[] =  "[row ".($i+2)."] ".$StdClass . " - " . $StdClassNumber;
				} else
				{
					# send & log
					
					if ($ProgressIndexInstant>=$ProgressIndex && $ProgressIndexInstant<$ProgressIndex+$EmailInterval)
					{
						if (!$SendingNow)
						{
							$limc->updateMassMailStatus($MassMailingID, "IN_PROGRESS");
							$SendingNow = true;
						}
						$EmailAttachmentSendNow = "";
						if ($EmailAttachment!="")
						{
							$attachment_file = $intranet_root."/file/mailmerge_attachment/att".$MassMailingID."/".$EmailAttachment;
							if (file_exists($attachment_file))
							{
								$EmailAttachmentSendNow = $attachment_file;
							}							
						}
						if($sendSingleMail){
							$_rowNumber = 'NULL';
						}else{
							$_rowNumber = $ProgressIndexInstant+1;
						}
						
						//write_file_content($ToEmail, $intranet_root."/file/mailmerge_attachment/sendlog1.txt");
						$limc->sendMassMail($MassMailingID, $EmailTitle, $HTML_template, $HeaderArr, $CSVDataArr[$i], $EmailSender, $ShowContent, $ToEmail, $StudentID, $UserName, $StdClass, $StdClassNumber, 0, $EmailAttachmentSendNow, $headers_preset, $EmailSenderName,$_rowNumber);
						
					}
					
					$ProgressIndexInstant ++;
					if ($RecipientAddress)
						$CountAddress ++;
					else
						$CountStudents ++;
				}
			}
			
			# for mapping to parents
			if ($StudentsArr[$StdClass][(int)$StdClassNumber]["UserID"]!="" && $StudentsArr[$StdClass][(int)$StdClassNumber]["UserID"]>0)
			{
				$StudentIDs[] = $StudentsArr[$StdClass][(int)$StdClassNumber]["UserID"];	
			}
		}
	}

	# check emails for parents
	if ($RecipientParent)
	{
		if (sizeof($StudentIDs)>0)
		{
			$StdIDsStr = implode(",", $StudentIDs);
			if (!$SetAsCampusMail && $plugin['imail_gamma'])
			{
				$sql = "SELECT ip.ParentID, ip.StudentID, iu.ImapUserEmail, iu.EnglishName FROM INTRANET_PARENTRELATION AS ip, INTRANET_USER AS iu where ip.StudentID IN ($StdIDsStr) AND ip.ParentID=iu.UserID AND iu.RecordType=3 AND iu.RecordStatus=1 ";
			} elseif (!$SetAsCampusMail && $liwm->isExternalMailAvaliable())
			{
				$sql = "SELECT DISTINCT ip.ParentID, ip.StudentID, iu.UserLogin, iu.EnglishName FROM INTRANET_PARENTRELATION AS ip, INTRANET_USER AS iu, INTRANET_SYSTEM_ACCESS AS b where ip.StudentID IN ($StdIDsStr) AND ip.ParentID=b.UserID AND ip.ParentID=iu.UserID AND iu.RecordType=3 AND b.ACL IN (1,3) AND iu.RecordStatus=1  ";
			} else
			{
				# campusmail
				$sql = "SELECT ip.ParentID, ip.StudentID, iu.EnglishName FROM INTRANET_PARENTRELATION AS ip, INTRANET_USER AS iu where ip.StudentID IN ($StdIDsStr) AND ip.ParentID=iu.UserID AND iu.RecordType=3 AND iu.RecordStatus=1 ";
				$UseCampusMail = true;
			}
			$rows = $limc->returnArray($sql,null,1);

	
			for ($i=0; $i<sizeof($rows); $i++)
			{
				# user email for iMail 1.2
				if (!$plugin['imail_gamma'] && $rows[$i]["UserLogin"]!="")
				{
					$rows[$i]["ImapUserEmail"] = $rows[$i]["UserLogin"]."@".$liwm->mailaddr_domain;
				}
				$CurParent = $rows[$i];				
				$StdClass = $StudentsArrByID[$CurParent["StudentID"]]["ClassName"];
				$StdClassNumber = $StudentsArrByID[$CurParent["StudentID"]]["ClassNumber"];
				$ParentsArr[$StdClass][(int)$StdClassNumber] = ($UseCampusMail) ? array("UserID"=>$CurParent["ParentID"], "ImapUserEmail"=>"[CAMPUSMAIL]", "UserName"=>$CurUser["EnglishName"]):  array("UserID"=>$CurParent["ParentID"], "ImapUserEmail"=>$CurParent["ImapUserEmail"], "UserName"=>$CurUser["EnglishName"]);
			}
		}
		
		# check parent existence by students
		$CountParents = 0;
		for ($i=0; $i<sizeof($CSVDataArr); $i++)
		{
			$StdClass = $CSVDataArr[$i]["CLASS"];
			$StdClassNumber = $CSVDataArr[$i]["CLASSNUMBER"];
			$ToEmail = trim($ParentsArr[$StdClass][(int)$StdClassNumber]["ImapUserEmail"]);
			$ParentID = $ParentsArr[$StdClass][(int)$StdClassNumber]["UserID"];
			$EmailAttachment = $CSVDataArr[$i]["EMAILATTACHMENT"];
			$UserName =  $ParentsArr[$StdClass][(int)$StdClassNumber]["UserName"];
			if ($ToEmail!="")
			{
				if ($ProgressIndexInstant>=$ProgressIndex && $ProgressIndexInstant<$ProgressIndex+$EmailInterval)
				{
					if (!$SendingNow)
					{
						$limc->updateMassMailStatus($MassMailingID, "IN_PROGRESS");
						$SendingNow = true;
					}
					
					$EmailAttachmentSendNow = "";
					if ($EmailAttachment!="")
					{
						$attachment_file = $intranet_root."/file/mailmerge_attachment/att".$MassMailingID."/".$EmailAttachment;
						if (file_exists($attachment_file))
						{
							$EmailAttachmentSendNow = $attachment_file;
						}							
					}
					$_rowNumber = $ProgressIndexInstant+1;
					# send & log
					$limc->sendMassMail($MassMailingID, $EmailTitle, $HTML_template, $HeaderArr, $CSVDataArr[$i], $EmailSender, $ShowContent, $ToEmail, $ParentID, $UserName, $StdClass, $StdClassNumber, 1, $EmailAttachmentSendNow, $headers_preset, $EmailSenderName,$_rowNumber);
				}
				
				$ProgressIndexInstant ++;
				$CountParents ++;
			} elseif (is_array($ParentsArr[$StdClass][(int)$StdClassNumber]) && isset($ParentsArr[$StdClass][(int)$StdClassNumber]))
			{
				$ErrorNoEmailParent[] = "[row ".($i+2)."] ".$StdClass . " - " . $StdClassNumber;
			} else
			{
				$ErrorNoParent[] =  "[row ".($i+2)."] ".$StdClass . " - " . $StdClassNumber;
			}
		}
		//debug($CountParents." parents are okay:");
		//debug_r($ParentsArr);
	}
	
	
	$RecipientTotal = $CountStudents + $CountParents + $CountAddress;
	
	
	$ProgressIndex += $EmailInterval;
	
	if ($ProgressIndex>$RecipientTotal)
	{
		$ProgressIndex = $RecipientTotal;
	}
	
	# mark as completed
	if ($CountStudents>0 || $CountParents>0 || $CountAddress>0)
	{
		if ($ProgressIndex>=$RecipientTotal)
		{
			$limc->updateMassMailStatus($MassMailingID, "COMPLETE");
			$ProgressStatus = "COMPLETE";
		} else
		{
			$ProgressStatus = "ToContinue";
		}
	} else
	{
		# as no recipient
		$ProgressStatus = "COMPLETE";
	}
	
	echo "$ProgressStatus|$RecipientTotal|$ProgressIndex";
	intranet_closedb();
?>

