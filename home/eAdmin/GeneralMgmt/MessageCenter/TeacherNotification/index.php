<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
    	
intranet_auth();
intranet_opendb();

$appType = $eclassAppConfig['appType']['Teacher'];

$limc = new libmessagecenter();
$limc->checkNotifyMessageAccessRight($appType);

intranet_closedb();

header("Location: ../ParentNotification/index.php?appType=".$appType);
?>