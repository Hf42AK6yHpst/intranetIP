<?php
# using: 

#################################
#   Date:   2019-05-13 Anna
#           Added single quot for sql
#
#	Date:	2011-06-23	Yuen
#			first version
#################################

	$PATH_WRT_ROOT = "../../../../../";
	
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");

	# iMail Plus enabled, assigned user only
	# to be changed
	if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-SMS"])
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}

	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
	include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
	
	intranet_auth();
	intranet_opendb();
	
	# Create a new interface instance
	$linterface = new interface_html();
	$limc = new libmessagecenter();
	
	# Page title
	$CurrentPage = "SMSTemplate";

	$TAGS_OBJ[] = array($Lang['SMS']['MessageMgmt']);
	$MODULE_OBJ = $limc->GET_MODULE_OBJ_ARR();
	
	$TemplateID = (is_array($TemplateID)? $TemplateID[0]:$TemplateID);
	
	
	$PAGE_NAVIGATION[] = array($Lang['SMS']['MessageMgmt'], "javascript:history.back();");
	$PAGE_NAVIGATION[] = ($TemplateID>0) ? array($Lang['Btn']['Edit']) : array($Lang['Btn']['New']);
	
	$TemplateType = $TemplateType ? $TemplateType : 1;
	

	$lsms = new libsmsv2();
	
	
	if ($TemplateID>0)
	{
		$selection = ($TemplateType==1)? $i_SMS_NormalTemplate : $i_SMS_SystemTemplate;
		$selection .= "<input type='hidden' name='TemplateType' value='{$TemplateType}' />";
		
		if($TemplateType==1)
		{
			$sql = "SELECT Title, Content FROM INTRANET_SMS_TEMPLATE WHERE TemplateID = '$TemplateID'";
		} else
		{
			$status = $lsms->returnTemplateStatus($TemplateID);

			$StatusActiveChecked = ($status) ? "checked='checked'" : "";
			$StatusDisabledChecked = ($status) ? "" : "checked='checked'";
			
			$sql = "SELECT TemplateCode, Content FROM INTRANET_SMS2_SYSTEM_MESSAGE_TEMPLATE WHERE TemplateID = '$TemplateID'";
		}	
		$result = $lsms->returnArray($sql,2);
		list($Title, $Content) = $result[0];

	} elseif($lsms->returnNonExistsTemplateAry(1))
	{
		$selection 	= $lsms->getTemplateTypeSelect("name=TemplateType onChange=chTemplate(document.form1)",$TemplateType);
	} else {
		$selection 	= $lsms->getTemplateTypeSelect("name=TemplateType onChange=chTemplate(document.form1)",$TemplateType, 1);
	}	

	# Start layout
	$linterface->LAYOUT_START();
	
	if ($StatusActiveChecked=="" && $StatusDisabledChecked=="")
	{
		$StatusActiveChecked = "checked='checked'";
	}
	
	if ($intranet_session_language == "en")
	{
	    $langStr = "English";
	}
	else
	{
	    $langStr = "Chinese";
	}
?>


<SCRIPT LANGUAGE=Javascript src="/templates/sms.js"></SCRIPT>
<script language="javascript">
function checkform(obj){
    <? if($TemplateType==1) {?>
	if(!check_text(obj.Title, "<?php echo $i_alert_pleasefillin.$i_SMS_TemplateName; ?>.")) return false;
	<? } else {?>
	if(!check_text(obj.TemplateCode, "<?php echo $i_alert_pleasefillin.$i_SMS_TemplateCode; ?>.")) return false;
	<? } ?>
     if(!check_text(obj.Content, "<?php echo $i_alert_pleasefillin.$i_SMS_TemplateContent; ?>.")) return false;
}

function chTemplate(obj)
{
	obj.action = "template_new.php";
	obj.submit();
}

function addTag(obj)
{
	tag = "($" + obj.Tag.value + ")";
	obj.Content.focus();
	obj.Content.value = obj.Content.value + tag;
}
</SCRIPT>


	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($msg, $xmsg);?></td>
	</tr>
	
		<tr>
			<td class="navigation">
				<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr><td><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?></td></tr>
				</table>
			</td>
		</tr>
	</table>
	
	<form name="form1" action='template_new_update.php' method='POST' ONSUBMIT="return checkform(this)">
	
	<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
		
		
		<!-- Title -->
		<tr>
		   <td class="tabletext formfieldtitle" width="30%" valign="top"  nowrap='nowrap'><?=$i_SMS_Personalized_Template_Type?> <span class="tabletextrequire">*</span></td>
			<td align="left" valign="top">
			  <?= $selection ?>
			</td>
		</tr>
		
<? if($TemplateType==1) {?>
		<tr>
		   <td class="tabletext formfieldtitle" width="30%" valign="top"  nowrap='nowrap'><?=$i_SMS_TemplateName?> <span class="tabletextrequire">*</span></td>
			<td align="left" valign="top">
			  <input class=text type=text name=Title size="50" maxlength="100" value="<?=$Title?>" />
			</td>
		</tr>
		
<? } else {?>
		<tr>
		   <td class="tabletext formfieldtitle" width="30%" valign="top"  nowrap='nowrap'><?=$i_SMS_Personalized_Template_SendCondition?> <span class="tabletextrequire">*</span></td>
			<td align="left" valign="top">
			  <?= ($TemplateID>0) ? $lsms->returnTemplateName($Title) : $lsms->returnNonExistsTemplateAry();?>
			</td>
		</tr>
<? } ?>	

		<tr>
		   <td class="tabletext formfieldtitle" width="30%" valign="top"  nowrap='nowrap'><?=$i_SMS_TemplateContent?> <span class="tabletextrequire">*</span></td>
			<td align="left" valign="top">
			  <!--<TEXTAREA NAME="Content" onFocus="startTimer('<?=$langStr?>', this.form);" onChange="countit(this.form,0,'<?=$langStr?>');" onBlur="stopTimer();" COLS="50" ROWS="4" WRAP="virtual"><?=$Content?></TEXTAREA><FONT>&nbsp;</FONT><INPUT TYPE=TEXT NAME="Size" VALUE="160" SIZE="3" MAXLENGTH="3">-->
			  <TEXTAREA NAME="Content" COLS="50" ROWS="4" WRAP="virtual"><?=$Content?></TEXTAREA>
			  <span class="tabletextremark">
			  <br /><?=$Lang['General']['Caution']?>
			  <li><?=$Lang['SMS']['LimitationArr'][0]['Content']?></li>
			  <li><?=$Lang['SMS']['LimitationArr'][1]['Content']?></li>
			  <li><?=$Lang['SMS']['LimitationArr'][2]['Content']?></li>
			  </span>
			  
<?
## select the tag code
if($TemplateType==2) {?>
	<br /><span class='extraInfo'><?=$i_SMS_Personalized_Template_Instruction?></span>
	<?= $i_SMS_Personalized_Msg_Tags ?>: <?=$lsms->returnTagAry()?><?= $linterface->GET_BTN($Lang['Btn']['Add'], "button", "addTag(document.form1)") ?>
<? } ?>

			</td>
		</tr>
		
<? if($TemplateType==2) {?>
		<tr>
		   <td class="tabletext formfieldtitle" width="30%" valign="top"  nowrap='nowrap'><?=$i_general_status?> <span class="tabletextrequire">*</span></td>
			<td align="left" valign="top">
			  <input type="radio" name="RecordStatus" value="1" id="RecordStatus1" <?=$StatusActiveChecked?> > <label for="RecordStatus1"><?=$i_general_active?></label> &nbsp;
			<input type="radio" name="RecordStatus" value="0" id="RecordStatus0" <?=$StatusDisabledChecked?> > <label for="RecordStatus0"><?=$i_general_inactive?></label>
			</td>
		</tr>
		
<? } ?>

		
		
	</table>	

	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center" >
		<tr>
			<td>
				<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>
					<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
					<tr>
						<td align="center">
							<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
							<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='template.php?TemplateType=$TemplateType'")?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
	<INPUT TYPE=HIDDEN NAME='options' VALUE='0'>
	
	<INPUT TYPE=HIDDEN NAME='chinesechar' VALUE=''>
	
	<INPUT TYPE=HIDDEN NAME='Textsize' VALUE=''>
	
	<input type="hidden" name="TemplateID" value="<?=$TemplateID?>" />
	
	</form>
	


<?

	$linterface->LAYOUT_STOP();
	
	intranet_closedb();
?>