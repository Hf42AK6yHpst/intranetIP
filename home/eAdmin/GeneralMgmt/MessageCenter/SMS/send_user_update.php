<?php

# using: yuen


###################### Change Log Start ########################
#	Date: 	2012-11-30 yuen 
#			parsed phone number to remove unnecessary " " and "-"
#
###################### Change Log End ########################


$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");

intranet_auth();
intranet_opendb();

# to be changed
	if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-SMS"])
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}


if(sizeof($Recipient)<=0 &&sizeof($usertype)<=0 ){
    header("Location: index.php?xmsg=SMSsubmittedfailed");
    exit();
	
}

$libsms = new libsmsv2();

$message = str_replace("\n", "", trim($Message));
$sms_message = intranet_htmlspecialchars(trim($message));

if(sizeof($Recipient)>0){
	$Recipient = array_unique($Recipient);
	$Recipient = array_values($Recipient);
	$RecipientID = implode(",", $Recipient);
}
#$li = new libcampusmail();
$actual_recipient_array = $libsms->returnRecipientUserIDArray($RecipientID);

$namefield = $libsms->getSMSIntranetUserNameField("");

if(sizeof($usertype)>0){
	$usertype_list = implode(",",$usertype);
	if($usertype_list==2)
		$cond = " AND RecordStatus IN (0,1,2)";
	// $sql="SELECT UserID FROM INTRANET_USER WHERE RecordType IN ($usertype_list) AND TRIM(MobileTelNo) <> '' AND MobileTelNo IS NOT NULL ";
	$sql="SELECT UserID FROM INTRANET_USER WHERE RecordType IN ($usertype_list) $cond ";
	$temp = $libsms->returnVector($sql);
}

if (sizeof($actual_recipient_array)==0 && sizeof($temp)<=0)
{
    header("Location: index.php?xmsg=SMSsubmittedfailed");
    exit();
}

if ($scdate != "" && $sctime != "")
{
    $time_send = "$scdate $sctime";
}
else $time_send = "";


# Grab mobile numbers and UserIDs
//$delim = "";
//$mobileList = "";
$mobileList=array();
for ($i=0; $i<sizeof($actual_recipient_array); $i++)
{
     //$mobileList .= "$delim".$actual_recipient_array[$i][0];
     //$delim = ",";
     $mobileList[] = $actual_recipient_array[$i][0];
}
for($i=0;$i<sizeof($temp);$i++){
	if(!in_array($temp[$i],$mobileList)){
		$mobileList[] = $temp[$i];
	}
}

$mobile_list = implode(",",$mobileList);

$sql = "
		SELECT 
			UserID, 
			TRIM(MobileTelNo), 
			ClassName,
			ClassNumber,
			$namefield 
		FROM 
			INTRANET_USER 
		WHERE 
			UserID IN ($mobile_list) 
		ORDER BY 
			ClassName,
			ClassNumber,
			EnglishName
	";
$users = $libsms->returnArray($sql,5);
if (sizeof($users)==0)
{
    header("Location: index.php?xmsg=SMSsubmittedfailed");
    exit();
}

$error_list=array();
$valid_list=array();
for($i=0;$i<sizeof($users);$i++){
	list($userid,$mobile,$classname,$classnumber,$name,) = $users[$i];
	
	if ($classnumber==0)
	{
		$classnumber = "";
	}
	$mobile = $libsms->parsePhoneNumber($mobile);
	if($libsms->isValidPhoneNumber($mobile)){
		$recipientData[] = array($mobile,$userid,addslashes($name),"");
		$valid_list[] = array($userid,$mobile,$name,$classname,$classnumber);
	}else{
		$reason =$i_SMS_Error_NovalidPhone;
		$error_list[] = array($userid,$mobile,$name,$classname,$classnumber,$reason);
	}
}


if(sizeof($recipientData)>0 && $send==1){
	$targetType = 3;
	$picType = 2;
	$adminPIC =$PHP_AUTH_USER;
	$userPIC = $UserID;
	$frModule= "";
	$deliveryTime = $time_send;
	$isIndividualMessage=false;
	$sms_message = $message; #### Use original text - not htmlspecialchars processed
	$returnCode = $libsms->sendSMS($sms_message,$recipientData, $targetType, $picType, $adminPIC, $userPIC,"",$deliveryTime,$isIndividualMessage);
	$isSent = 1;
}


# ----------- valid list-------- #
if (sizeof($valid_list)>0)
{
	$valid_table = "<div class=\"table_board\"><span class=\"sectiontitle_v30\"> {$i_SMS_Notice_Send_To_Users}</span><br />";
	$valid_table.= "<table class=\"common_table_list view_table_list\">";
	$valid_table.= "<thead><tr><th width='1'>&nbsp;</th><th width=30%>$i_UserName</th><th width=20%>$i_ClassName</th><th width=20%>$i_ClassNumber</th><th  width=25%>$i_SMS_MobileNumber</th></tr></thead>";
	for($i=0;$i<sizeof($valid_list);$i++)
	{
		list($userid,$mobile,$name,$classname,$classnumber)=$valid_list[$i];
	    $css = $i%2==0?"row_avaliable":"";
	    $valid_table.="<tr class='$css'><td>".($i+1)."</td><td>".($name==""?"-":$name)."</td><td width=15%>".($classname==""?"--":$classname)."</td><Td width=5%>".($classnumber==""?"--":$classnumber)."</td><td>".($mobile==""?"-":$mobile)."</td></tr>";	
	}
	
	$valid_table.="</table>";
	$valid_table.="</div>";
}

# ----------- error list -------------- #
if (sizeof($error_list)>0)
{
	$invalid_table = "<div class=\"table_board\"><span class=\"sectiontitle_v30\"> <font color='red'>{$i_SMS_Warning_Cannot_Send_To_Users}</font></span><br />";
	$invalid_table.= "<table class=\"common_table_list view_table_list\">";
	$invalid_table.= "<thead><tr><th width=25%>$i_UserName</th><th width=15%>$i_ClassName</th><th width=15%>$i_ClassNumber</th><th  width=15%>$i_SMS_MobileNumber</th><th width=30%>$i_Attendance_Reason</th></tr></thead>";
	for($i=0;$i<sizeof($error_list);$i++)
	{
	    list($sid,$mobile,$name,$classname,$classnumber,$reason)=$error_list[$i];
	    $css = $i%2==0?"row_suspend":"";
	    $invalid_table.="<tr class='$css'><td>".($name==""?"-":$name)."</td><td>".($classname==""?"--":$classname)."</td><Td>".($classnumber==""?"--":$classnumber)."</td><td>".($mobile==""?"-":$mobile)."</td><td>".($reason==""?"-":$reason)."</td></tr>";
	}
	$invalid_table.="</table>";
	$invalid_table.="</div>";
}


if($isSent && ($returnCode > 0 && $returnCode==true))
{
	intranet_closedb();
	
	header("Location: index.php?xmsg=SMSsubmitted");
}
else
{
	
	# Create a new interface instance
	$linterface = new interface_html();
	$limc = new libmessagecenter();
	
	# Page title
	$CurrentPage = "PageSMS";

	$TAGS_OBJ[] = array($Lang['SMS']['MessageMgmt']);
	$MODULE_OBJ = $limc->GET_MODULE_OBJ_ARR();
	
	$PAGE_NAVIGATION[] = array($Lang['SMS']['MessageMgmt'], "index.php");
	$PAGE_NAVIGATION[] = array($Lang['MassMailing']['New']. " (".$Lang['MassMailing']['NewStep2'].")");
	
	
	# Start layout
	$linterface->LAYOUT_START();
	
	$xmsg = '';
	if ($isSent && $returnCode < 0)
		$xmsg = $libsms->Get_SMS_Status_Display($returnCode);
	
	if ($isSent)
    	$btn_to_send = $Lang['SMS']['ReSend'];
    else
    	$btn_to_send = $Lang['SMS']['Send'];
	
	?>
	<script language='javascript'>
		function sendSMS(){
			
<?php if ($sys_custom["ThisSiteIsForDemo"]) {?>
			alert("Sorry, this action is not allowed in trial site!");
			return false;
<?php } ?>

			obj = document.form1;
			if(document.form1==null) return;
			
			sendObj = obj.send;
			if(sendObj==null) return;
			sendObj.value = 1;
			
			$('div#actionBtnDiv').hide();
			$('div#sendingSmsDiv').show();
			
			document.form1.submit();
			
		}
	</script>
	<form name=form1 action='send_user_update.php' method=post>
	<input type=hidden name="scdate" value="<?=$scdate?>">
	<input type=hidden name="sctime" value="<?=$sctime?>">
	<input type=hidden name="Message" value="<?=intranet_htmlspecialchars(stripslashes($sms_message))?>">

	<?php 
		for($i=0;$i<sizeof($Recipient);$i++)
			echo "<input type=hidden name='Recipient[]' value='".$Recipient[$i]."'>\n";
		for($i=0;$i<sizeof($usertype);$i++)
			echo "<input type=hidden name='usertype[]' value='".$usertype[$i]."'>\n";

	?>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="navigation">
				<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr><td><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?></td></tr>
				</table>
			</td>
		</tr>
		<tr>
			<td><table width="90%" border="0" cellspacing="5" cellpadding="15" align="center">	
				<tr>
					<td nowrap='nowrap'><?=$Lang['SMS']['MessageToBeSent']?></td><td bgcolor='#DEEAFF' style='border: 1px dotted #4466AA;' width='95%'><?=stripslashes($sms_message)?></td>
				</tr>
				</table>
			</td>
		</tr>
	</table>
	
	
	<input type="hidden" name="send" value="">
	
	<table width="90%" border="0" cellspacing="5" cellpadding="5" align="center">	
	<tr>
		<td>
		<?=$invalid_table?>
		<Br />
		<?=$valid_table?>
		</td>
	</tr>
	</table>
	
	
	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td>
				<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
					<tr>
						<td align="center">
							<div id="actionBtnDiv">
								<?php if (sizeof($valid_list)>0) {?>
									<?= $linterface->GET_ACTION_BTN($btn_to_send, "button", "sendSMS()")?>&nbsp;
								<?php } ?>
								<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "self.location='index.php'")?>
							</div>
							<div id="sendingSmsDiv" class="tabletextrequire" style="display:none;">
								<?=$Lang['SMS']['SendingSms']?>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

    </form>
    <?php
    intranet_closedb();
	$linterface->LAYOUT_STOP();

}
                    

?>