<?php //
/*************
 *  2013-03-20	(Ivan) [2012-0928-1438-14066]
 * 				- added logic to show scheduled sms send time
 *	2012-11-07	(Rita)
 * 			  	- add Status Filter
 * 
 *************/
 
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
intranet_opendb();


if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-SMS"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$CurrentPage = "PageSMS";

$linterface = new interface_html();
$limc = new libmessagecenter();


### Refresh SMS status
$libsms = new libsmsv2();
$isRefresh = $_GET['isRefresh'];
if ($isRefresh)
{
	$libsms->Refresh_SMS_Status($SourceID);
	$xmsg = "UpdateSuccess";
}
	

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
if ($field=="") $field = 4;
if ($order=="") $order = 0;
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        case 4: $field = 4; break;
        case 5: $field = 5; break;
        default: $field = 5; break;
}
$order = ($order == 1) ? 1 : 0;
if($filter == "") $filter = 1;
$filter = ($filter == 1) ? 1 : 0;

$namefield = getNameFieldByLang("b.");

$StatusField = '';
if (($sms_vendor == 'CTM') || ($sms_vendor == 'TACTUS'))
{
	$StatusField = ', '.$libsms->Get_SMS_Status_Field('a').' as RecordStatusWord';
}

if ($sms_vendor == 'TACTUS'){
	$StatusOptionArr = $_REQUEST['StatusOption'];
	
	$numOfStatusOptionArr = count($StatusOptionArr);
	if($numOfStatusOptionArr>0){
		$StatusOption_Cond = "AND 
							 IF (a.ReferenceID Is Null,'4',a.RecordStatus)
							 IN ('".implode("','", (array)$StatusOptionArr)."')";						 
	}else{
		$StatusOption_Cond = '';
	}
}

if ($sms_vendor != "TACTUS") {
	$scheduleDateTimeField = " , If (ssm.ScheduleDateTime is null OR ssm.ScheduleDateTime = '0000-00-00 00:00:00', '".$Lang['General']['EmptySymbol']."', ssm.ScheduleDateTime) as ScheduleDateTime ";
}


$sql = "
		SELECT
			a.RecipientPhoneNumber,  
			a.RecipientName,
			$namefield as RelatedUserName,  
			a.Message,                             
			a.DateInput
			$StatusField
			$scheduleDateTimeField
		FROM INTRANET_SMS2_MESSAGE_RECORD as a
			Inner Join INTRANET_SMS2_SOURCE_MESSAGE as ssm On (a.SourceMessageID = ssm.SourceID)
            LEFT OUTER JOIN INTRANET_USER as b ON a.RelatedUserID = b.UserID
		WHERE
			SourceMessageID = $SourceID
			and
				(
               a.RecipientPhoneNumber 	LIKE '%$keyword%' or
               a.RecipientName 			LIKE '%$keyword%' or
               $namefield		 		LIKE '%$keyword%' or
               a.Message 				LIKE '%$keyword%' 
               )   
			$StatusOption_Cond    
";
        
# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
//if (($sms_vendor == 'CTM') || ($sms_vendor == 'TACTUS'))
//	$li->field_array = array("a.RecipientPhoneNumber","a.RecipientName","RelatedUserName","a.Message","a.DateInput","a.RecordStatus");
//else
//	$li->field_array = array("a.RecipientPhoneNumber","a.RecipientName","RelatedUserName","a.Message","a.DateInput");
	
$li->field_array = array("a.RecipientPhoneNumber","a.RecipientName","RelatedUserName","a.Message","a.DateInput");
if ($sms_vendor != "TACTUS") {
	$li->field_array[] = 'ScheduleDateTime';
}
if ($sms_vendor == 'CTM' || $sms_vendor == 'TACTUS') {
	$li->field_array[] = "a.RecordStatus";
}

	
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
$li->column_array = array(5,0,0,0);
$li->wrap_array = array(25,0,0,0);
$li->IsColOff = 'IP25_table';

// TABLE COLUMN
$col = 0;
$li->column_list .= "<th width=1 >#</th>\n";
$li->column_list .= "<th width=5% >".$li->column($col++, $i_SMS_MobileNumber)."</th>\n";
$li->column_list .= "<th width=15% >".$li->column($col++, $i_SMS_Recipient2)."</th>\n";
$li->column_list .= "<th width=15% >".$li->column($col++, $i_SMS_RelatedUser)."</th>\n";
$li->column_list .= "<th width=35% >".$li->column($col++, $i_SMS_MessageContent)."</th>\n";
//$li->column_list .= "<th nowrap='nowrap' >".$li->column($col++, $i_SMS_TargetSendTime)."</th>\n";
$li->column_list .= "<th nowrap='nowrap' >".$li->column($col++, $Lang['SMS']['SmsCreationTime'])."</th>\n";
if ($sms_vendor != "TACTUS") {
	$li->column_list .= "<th nowrap='nowrap' >".$li->column($col++, $Lang['SMS']['ScheduleDateTime'])."</th>\n";
}
if ($sms_vendor == 'CTM' || $sms_vendor == 'TACTUS') {
	$li->column_list .= "<th width=10% >".$li->column($col++, $i_general_status)."</th>\n";
}


$toolbar = $linterface->GET_LNK_EXPORT("javascript:checkPost(document.form1,'detail_export.php?SourceID=".$SourceID."')",$button_export,"","","",0);

$toolbar .= "<div class='Conntent_tool'><a href='javascript:refresh();' class='refresh_status'>". $i_SMS['RefreshAllStatus'] ."</a></div>\n";

$searchbar  = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\" />\n";

$sentSql = "SELECT COUNT(*) FROM INTRANET_SMS_LOG WHERE RecordStatus = 2 OR RecordStatus = 0 OR RecordStatus IS NULL";
$result = $li->returnVector($sentSql);
$sent = $result[0]+0;

$quota = get_file_content("$intranet_root/admin/sms/limit.txt");
$removed = get_file_content("$intranet_root/admin/sms/removed.txt");
$removed += 0;
$sent += $removed;

$left = $quota - $sent;

$left = ($left<0? "<font color=red>$left</font>":"<font color=green>$left</font>");

$infobar = "$i_SMS_MessageSentSuccessfully: $sent<br>$i_SMS_MessageQuotaLeft: $left";

### Title ###

$TAGS_OBJ[] = array($Lang['SMS']['MessageMgmt']);
$MODULE_OBJ = $limc->GET_MODULE_OBJ_ARR();

$PAGE_NAVIGATION[] = array($Lang['SMS']['MessageMgmt'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['SMS']['ViewLog']);

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

# Status Filter
if ($sms_vendor == 'TACTUS')
{		
	$SourceID = $_GET['SourceID'] ;

	$StatusField = $libsms->Get_SMS_Status_Field('ismr');
	$StatusFieldSQL = " IF (a.RecipientCount > 1 And (ismr.ReferenceID is Not Null Or ismr.ReferenceID != ''), 
							'".$i_SMS['MultipleStatus']."', 
							$StatusField) 
						as RecordStatus ";
	
	$sql = "SELECT $StatusFieldSQL
			FROM 
				INTRANET_SMS2_SOURCE_MESSAGE as a
        	INNER JOIN
				INTRANET_SMS2_MESSAGE_RECORD as ismr
				ON (a.SourceID = ismr.SourceMessageID)
     		LEFT OUTER JOIN 
     			INTRANET_USER as b 
     			ON (a.UserPIC = b.UserID)
			WHERE 
				SourceMessageID='".$SourceID."'
			GROUP BY 
				a.SourceID
			";
			  
	$statusResultArr = $libsms->returnArray($sql);
	
	$statusOptionArr = $_REQUEST['StatusOption'];
	
	if($statusResultArr[0]['RecordStatus'] == $i_SMS['MultipleStatus']){
		$statusFilter = $libsms->getSMSStatusMultiSelection(false, $statusOptionArr);	
	}
	
}

?>

<script language="javascript">
<!--
	function view_detail(sid)
	{
		with(document.form1)
		{
			SourceID.value = sid;
			submit();		
		}
	}
	
	function refresh()
	{
		document.form1.isRefresh.value = 1;
		document.form1.submit();
	}
//-->


<? if ($sms_vendor == 'TACTUS')
{	
?>

	function checkboxCheck(flag) {
		
		if(flag==true){		
			$('#Pending').attr('checked', true);
			$('#Sending').attr('checked', true);
			$('#Failed').attr('checked', true);
			$('#Delivered').attr('checked', true);	
			$('#FailToSendSMS').attr('checked', true);			
		}else{
			$('#Pending').attr('checked', false);
			$('#Sending').attr('checked', false);
			$('#Failed').attr('checked', false);
			$('#Delivered').attr('checked', false);
			$('#FailToSendSMS').attr('checked', false);		
		}
	
	}
<?}?>

</script>

<form name="form1" id="form1" method="get" action="detail.php?SourceID='<?=$SourceID?>'" >

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td class="navigation">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
			<tr><td><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?></td></tr>
			</table>
		</td>
	</tr>
</table>
<br />


<div class="content_top_tool">
	<?=$toolbar?>
	<div class="Conntent_search"><?=$searchbar?></div>     
	<br style="clear:both" />
</div>

<?=$statusFilter;?>

<?=$li->display();?>


<br />



<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
<input type="hidden" name="SourceID" value="<?=$SourceID?>">
<input type="hidden" name="isRefresh" value="">



</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>