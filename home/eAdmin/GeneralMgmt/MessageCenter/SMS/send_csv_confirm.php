<?php


# using: yuen


###################### Change Log Start ########################
#	Date: 	2012-11-30 yuen 
#			parsed phone number to remove unnecessary " " and "-"
#
###################### Change Log End ########################


$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

# to be changed
	if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-SMS"])
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}

	# Create a new interface instance
	$linterface = new interface_html();
	$limc = new libmessagecenter();


$libsms = new libsmsv2();

if ($scdate != "" && $sctime != "")
{
    $time_send = "$scdate $sctime";
}
else $time_send = "";


$sql = "CREATE TABLE IF NOT EXISTS TEMPSTORE_SMS2_FILE_USER (
 Mobile varchar(255),
 Message varchar(255),
 UserID int(9)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";
$libsms->db_db_query($sql);



if (!$isSent)
{
	$limport = new libimporttext();
	$filepath = $userfile;
	$filename = $userfile_name;
	$data = $limport->GET_IMPORT_TXT($filepath);
	array_shift($data);
	
	$sql = "DELETE FROM TEMPSTORE_SMS2_FILE_USER where UserID='$UserID'";
	$libsms->db_db_query($sql);
}
else
{
	$sql = "Select Mobile, Message From TEMPSTORE_SMS2_FILE_USER where UserID='$UserID' ";
	$data = $libsms->returnArray($sql);
	
	$xmsg = '';
	if ($isSent && $returnCode < 0)
		$xmsg = $libsms->Get_SMS_Status_Display($returnCode);
}




$invalid_data = array();


$values = "";
$delim = "";
for ($i=0; $i<sizeof($data); $i++)
{
     list($mobile, $sms_msg) = $data[$i];
	$mobile = $libsms->parsePhoneNumber($mobile);
     if(!$libsms->isValidPhoneNumber($mobile) || $mobile == ''){
	     $invalid_data[$mobile]['Message'] = $sms_msg;
	     $invalid_data[$mobile]['Reason'] = $i_SMS_Error_NovalidPhone;
	     continue;
	  }
#     $sms_msg = intranet_htmlspecialchars($sms_msg);
     $mobile = trim(addslashes($mobile));
     $db_sms_msg = trim(addslashes($sms_msg));
     $values .= "$delim ('$mobile','$db_sms_msg','$UserID')";
     $delim = ",";
}


if (!$isSent)
{
	$sql = "INSERT INTO TEMPSTORE_SMS2_FILE_USER (Mobile, Message, UserID) VALUES $values";
	$libsms->db_db_query($sql);
}


### Send / Re-try button
if ($isSent)
	$btn_to_send = $Lang['SMS']['ReSend'];
else
	$btn_to_send = $Lang['SMS']['Send'];
	
### Get system message if sending sms failed
$xmsg = '';
if ($isSent && $returnCode < 0)
	$xmsg = $libsms->Get_SMS_Status_Display($returnCode);


## --------- Valid List ------------ ### 
$sql=" SELECT a.Mobile, a.Message FROM TEMPSTORE_SMS2_FILE_USER AS a WHERE UserID='{$UserID}'";

$temp = $libsms->returnArray($sql,2);

$hasValidNumber=sizeof($temp)>0?true:false;
if ($hasValidNumber)
{
	$MsgTotalCounted = 0;
	$valid_table = "<div class=\"table_board\"><span class=\"sectiontitle_v30\"> {$i_SMS_Notice_Send_To_Users}</span><br />";
	$valid_table.= "<table class=\"common_table_list view_table_list\">";
	//$valid_table.= "<thead><tr><th width='20%'>$i_SMS_MobileNumber</th><th width='60%'>$i_SMS_MessageContent</th><th width='20%'>".$Lang['SMS']['MessageNo']."</th></tr></thead>";
	$valid_table.= "<thead><tr><th width='10%'>$i_SMS_MobileNumber</th><th width='50%'>$i_SMS_MessageContent</th><th width='40%'>".$Lang['SMS']['MessageCutted']."</th></tr></thead>";
	for($i=0;$i<sizeof($temp);$i++)
	{
		list($mobile,$msg) = $temp[$i];
		$css = $i%2==0?"row_avaliable":"";
		/* not in used, not to support long message due to billing issue
		if (utf8_strlen($msg,2)!=strlen($msg))
		{
			# Chinese
			$MsgNoCounted = ceil(utf8_strlen($msg)/69);
		} else
		{
			# Pure English
			$MsgNoCounted = ceil(strlen($msg)/160);
		}
		$MsgTotalCounted += $MsgNoCounted;
		*/
		$msg_ok = sms_substr($msg);
		$msg_cut = str_replace($msg_ok, "", $msg);
		if (trim($msg_cut)=="")
		{
			$msg_cut = "&nbsp;";
		}
		//$valid_table .= "<tr class='$css'><td>".($mobile==""?"-":$mobile)."</td><td>".($msg==""?"-":$msg)."</td><td>".$MsgNoCounted."</td></tr>";
		$valid_table .= "<tr class='$css'><td>".($mobile==""?"-":$mobile)."</td><td>".($msg_ok==""?"-":$msg_ok)."</td><td><font color='orange'>".$msg_cut."</font></td></tr>";
	}
	//$valid_table .= "<tr class='row_suspend'><td colspan='2' align='right'>".$Lang['SMS']['MessageTotal']."</td><td>".$MsgTotalCounted."</td></tr>";
	$valid_table.="</table>";
	$valid_table.="</div>";
	$valid_table.="<p>".$Lang['SMS']['MessageCutNotes']."</p>";
}

## ---------- invalid list ---- #####
if (sizeof($invalid_data)>0)
{
	$invalid_table = "<div class=\"table_board\"><span class=\"sectiontitle_v30\"> <font color='red'>{$i_SMS_Warning_Cannot_Send_To_Users}</font></span><br />";
	$invalid_table.= "<table class=\"common_table_list view_table_list\">";
	$invalid_table.= "<thead><tr><th width=15%>$i_SMS_MobileNumber</th><th width=50%>$i_SMS_MessageContent</th><th width=35%>$i_Attendance_Reason</th></tr></thead>";
	$i=0;
	foreach($invalid_data as $mobile=>$details)
	{
		if($mobile=="") continue;
		$msg  = $details['Message'];
		$reason= $details['Reason'];
		 $css = $i%2==0?"row_suspend":"";
		$invalid_table .= "<tr class='$css'><td>".($mobile==""?"-":$mobile)."</td><td>".($msg==""?"-":$msg)."</td><td>".($reason==""?"-":$reason)."</td></tr>";
		$i++;
	}
	$invalid_table.="</table>";
	$invalid_table.="</div>";
}
	
	# Page title
	$CurrentPage = "PageSMS";

	$TAGS_OBJ[] = array($Lang['SMS']['MessageMgmt']);
	$MODULE_OBJ = $limc->GET_MODULE_OBJ_ARR();
	
	$PAGE_NAVIGATION[] = array($Lang['SMS']['MessageMgmt'], "index.php");
	$PAGE_NAVIGATION[] = array($Lang['MassMailing']['New']. " (".$Lang['MassMailing']['NewStep2'].")");
	
	
	# Start layout
	$linterface->LAYOUT_START($xmsg);
		
?>

<script language='javascript'>
		function sendSMS(){
			
<?php if ($sys_custom["ThisSiteIsForDemo"]) {?>
			alert("Sorry, this action is not allowed in trial site!");
			return false;
<?php } ?>

			$('div#actionBtnDiv').hide();
			$('div#sendingSmsDiv').show();

			document.form1.submit();
			
		}
	</script>


		<form name=form1 action='send_csv_update.php' method=post>
		
		
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="navigation">
				<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr><td><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?></td></tr>
				</table>
			</td>
		</tr>
	</table>
	
	
	<table width="90%" border="0" cellspacing="5" cellpadding="5" align="center">	
	<tr>
		<td>
		<?=$invalid_table?>
		<Br />
		<?=$valid_table?>
		</td>
	</tr>
	</table>
	
	
	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td>
				<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
					<tr>
						<td align="center">
							<div id="actionBtnDiv">
								<?php if($hasValidNumber) { ?>
									<?= $linterface->GET_ACTION_BTN($btn_to_send, "button", "sendSMS()")?>&nbsp;
								<?php } ?>
								<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "self.location='index.php'")?>
							</div>
							<div id="sendingSmsDiv" class="tabletextrequire" style="display:none;">
								<?=$Lang['SMS']['SendingSms']?>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>


	<input type="hidden" name="time_send" value="<?=$time_send?>" />
	</form>


<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
                    

?>