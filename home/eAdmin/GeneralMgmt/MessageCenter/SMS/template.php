<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
intranet_opendb();


if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-SMS"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$CurrentPage = "SMSTemplate";

$linterface = new interface_html();
$limc = new libmessagecenter();
$lsms = new libsmsv2();



# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
if ($field=="") $field = 0;
if ($order=="") $order = 1;
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        default: $field = 0; break;
}
$order = ($order == 1) ? 1 : 0;
$TemplateType = $TemplateType ? $TemplateType : 1;


if($TemplateType==1)
{
	$sql = "SELECT
              CONCAT('<a href=\"template_new.php?TemplateType=1&TemplateID=',TemplateID,'\" >',Title,'</a>'), Content, DateModified,
              CONCAT('<input type=checkbox name=TemplateID[] value=', TemplateID ,'>')
        FROM INTRANET_SMS_TEMPLATE
        WHERE (
               Title LIKE '%$keyword%' OR
               Content LIKE '%$keyword%'
               )
        ";
        
	$field_array 	= array("Title", "Content","DateModified");
	$col0 			= $i_SMS_TemplateName;
}
else
{
	$TemplateAry = $lsms->TemplateAry();
	
	$t = "CASE TemplateCode ";
	$ava_template = "";
	foreach ($TemplateAry as $tempAry) 
	{
		$ava_template .= ":".$tempAry[0].":";
		$t .= " WHEN '{$tempAry[0]}' THEN '{$tempAry[1]}' ";
	}
	$t .= " ELSE '' END";
	
	$sql = "SELECT
              CONCAT('<a href=\"template_new.php?TemplateType=2&TemplateID=',TemplateID,'\" >', $t ,'</a>'), 
              Content, 
              DateModified,
              case RecordStatus when 1 then '". $i_general_active."' else '". $i_general_inactive."' end,
              CONCAT('<input type=checkbox name=TemplateID[] value=', TemplateID ,'>')
        FROM INTRANET_SMS2_SYSTEM_MESSAGE_TEMPLATE
        WHERE (
               TemplateCode LIKE '%$keyword%' OR
               Content LIKE '%$keyword%'
               )
          		and '".$ava_template."' like concat('%:',TemplateCode,':%')           
        ";	
        
	$field_array 	= array("TemplateCode", "Content","DateModified","RecordStatus");
	$col0 			= $i_SMS_Personalized_Template_SendCondition;
}

# TABLE INFO

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = $field_array;
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,5,0);
$li->wrap_array = array(0,25,0);
$li->IsColOff = 'IP25_table';

// TABLE COLUMN
$li->column_list .= "<th width=1 class=tableTitle>#</th>\n";
if($TemplateType==2)
{
	$li->column_list .= "<th width=20% class=tableTitle>".$li->column(0, $col0)."</th>\n";
	$li->column_list .= "<th width=45% class=tableTitle>".$li->column(1, $i_SMS_TemplateContent)."</th>\n";
	$li->column_list .= "<th width=23% class=tableTitle>".$li->column(2, $i_LastModified)."</th>\n";
	$li->column_list .= "<th width=12% class=tableTitle>".$li->column(3, $i_general_status)."</th>\n";
} else
{
	$li->column_list .= "<th width=25% class=tableTitle>".$li->column(0, $col0)."</th>\n";
	$li->column_list .= "<th width=50% class=tableTitle>".$li->column(1, $i_SMS_TemplateContent)."</th>\n";
	$li->column_list .= "<th width=25% class=tableTitle>".$li->column(2, $i_LastModified)."</th>\n";
	
}
$li->column_list .= "<th width=1 >".$li->check("TemplateID[]")."</th>\n";



//$toolbar  .= "<br><a class=iconLink href=javascript:checkPost(document.form1,'export.php')>".exportIcon()."$button_export</a>".toolBarSpacer();
if($lsms->returnNonExistsTemplateAry(1) or $TemplateType==1)
{
	$toolbar = $linterface->GET_LNK_ADD("template_new.php",$button_new,"","","",0);	
}

$searchbar  = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";

if($plugin['sms'] and $bl_sms_version >= 2)
	$selection 	= $lsms->getTemplateTypeSelect("name='TemplateType' onChange='this.form.submit()'",$TemplateType);

$functionbar  = "<a href=\"javascript:checkEdit(document.form1,'TemplateID[]','template_new.php')\" class=\"tool_edit\">{$button_edit}</a>";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'TemplateID[]','template_remove.php')\" class=\"tool_delete\">{$button_delete}</a>&nbsp;";


### Title ###

$TAGS_OBJ[] = array($Lang['SMS']['MessageTemplates']);
$MODULE_OBJ = $limc->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>


<form name="form1" id="form1" method="get" action="template.php" onSubmit="return checkForm()">

<div class="content_top_tool">
	<?=$toolbar?>
	<div class="Conntent_search"><?=$searchbar?></div>     
	<br style="clear:both" />
</div>

<?php if ($Instruction!="") {?>
<center>
<fieldset class="instruction_box">
	<legend><?=$Lang['Header']['Menu']['SMS']?></legend>
	<?= $Instruction ?>
</fieldset>
</center>
<?php } ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td valign="bottom">
		<?=$selection?>
	</td>
	<td valign="bottom">
		<div class="common_table_tool">	
		<?= $functionbar ?>
		</div>
	</td>
</tr>
</table>
<?=$li->display();?>


<br />



<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
<input type="hidden" name="SourceID" value="">
<input type="hidden" name="isRefresh" value="">



</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>