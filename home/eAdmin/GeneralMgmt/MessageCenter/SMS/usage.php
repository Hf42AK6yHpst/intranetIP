<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
intranet_opendb();


if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-SMS"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$CurrentPage = "SMSUsage";

$linterface = new interface_html();
$limc = new libmessagecenter();


if(!$year)	$year = date("Y");

$sql = "
		SELECT
			Month,
			MessageCount
		FROM INTRANET_SMS2_MONTHLY_REPORT 
        WHERE 	
        	Year = $year
";
$result = $limc->returnArray($sql,2);

//save the data in array
$data[] = array();
for($i=0;$i<sizeof($result);$i++)
{
	list($month, $count) = $result[$i];
	$data[$month] = $count;
}


$x = "<div class='table_board'><table width='90%' align='center' class='common_table_list view_table_list'>\n";
$x .= "<thead><th width='33%' style='text-align:center' >". $i_general_Month."</td>\n";
$x .= "<th width='33%' style='text-align:center'>".$i_SMS_MessageCount."</td>\n";
$x .= "<th width='33%' style='text-align:center'>".$i_general_refresh."</td>\n</thead>";
        
for($i=1;$i<=12;$i++)
{
	$css = ($i%2) ? "" : "class='row_waiting'";
	
	$monthDifference = getMonthDifferenceBetweenDates($year.'-'.$i.'-01', date('Y-m-d'));
	if ($monthDifference >= 6) {
		$refreshBtn = $Lang['General']['EmptySymbol'];
	}
	else {
		$refreshBtn = "<a href='javascript:refresh($i);' class='refresh_status' title='{$i_general_refresh}'>&nbsp;</a>";
	}
	
	$x .= "<tr {$css} >";
	$x .= "<td align='center' style='vertical-align:middle'>". $i."</td>\n";
	$x .= "<td align='center' style='vertical-align:middle'>". ($data[$i]+0) ."</td>\n";
	$x .= "<td><table width='100%' cellpadding='0' cellspacing='0'><tr><td width='46%' style='border:0px white solid;'>&nbsp;</td><td nowrap='nowrap' style='border:0px white solid;'><div class='Conntent_tool'>".$refreshBtn."</div></td><td width='46%' style='border:0px white solid;'>&nbsp;</td></tr></table></td>\n";
	$x .= "</tr>";
}

$x .= "</table></div>\n";

$sql = "select min(year) from INTRANET_SMS2_MONTHLY_REPORT";
$result = $limc->returnArray($sql,1);
$start_year = $result[0][0];
$start_year = $start_year == "" ? date("Y") : $start_year;

$select_year = "";
$select_year .= "<select name=\"year\" onChange=\"window.location='?year='+this.value\">";
for($i=date("Y");$i>=$start_year;$i--)
		$select_year .= "<option value='$i' ". ($i==$year? "selected":"") .">$i</option>";
$select_year .= "</select>";

$functionbar = "<table width='100%' border=0 cellpadding=5 cellspacing=0 align=center>\n";
$functionbar .= "<tr>\n";
$functionbar .= "<td nowrap='nowrap'>$select_year &nbsp; &nbsp;</td>\n";
$functionbar .= "<td width='95%'><div class='Conntent_tool'><a href='javascript:refresh(0);' class='refresh_status'>". $i_general_refresh . ($intranet_session_language=="en"?" ":"") . $i_Homework_AllRecords ."</a></div></td>\n";
$functionbar .= "</tr>\n";
$functionbar .= "</table>\n";


$remarksTable = $linterface->Get_Warning_Message_Box($Lang['General']['Remark'], $Lang['SMS']['UsageReportRemarks']);


### Title ###

$TAGS_OBJ[] = array($Lang['SMS']['UsageReport']);
$MODULE_OBJ = $limc->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>

<script language="javascript">
<!--
	function refresh(month)
	{
		window.location='usage_refresh.php?year=<?=$year?>&month='+month;
	}
//-->
</script>

<form name="form1" id="form1" method="get" action="index.php" onSubmit="return checkForm()">


<?= $functionbar ?>
<?= $remarksTable ?>
<?= $x ?>



</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>