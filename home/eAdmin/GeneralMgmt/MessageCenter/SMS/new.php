<?php
# using: 

#################################
#
#	Date:	2015-11-17	Ivan [P88986] [ip.2.5.7.1.1]
#			- added message length checking when doing form checking now
#
#	Date:	2014-10-22	Bill
#			- added ondrop and onpaste to Message Textarea
#	
#	Date:	2013-09-12	Roy
#			- added parameter filterAppParentOption to 'ChooseUser' button link in 'divUSERS'
#
#	Date:	2013-03-20	Ivan [2012-0928-1438-14066]
#			- added scheduled send date time JS validation
#
#	Date:	2011-06-23	Yuen
#			first version
#################################

	$PATH_WRT_ROOT = "../../../../../";
	
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");

	# iMail Plus enabled, assigned user only
	# to be changed
	if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-SMS"])
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}

	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
	include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
	
	intranet_auth();
	intranet_opendb();
	
	# Create a new interface instance
	$linterface = new interface_html();
	$limc = new libmessagecenter();
	
	# Page title
	$CurrentPage = "PageSMS";

	$TAGS_OBJ[] = array($Lang['SMS']['MessageMgmt']);
	$MODULE_OBJ = $limc->GET_MODULE_OBJ_ARR();
	
	$PAGE_NAVIGATION[] = array($Lang['SMS']['MessageMgmt'], "javascript:history.back();");
	$PAGE_NAVIGATION[] = array($Lang['MassMailing']['New'] . " (".$Lang['MassMailing']['NewStep1'].")");
	
	$MethodArr[] = array("USERS", $Lang['SMS']['SendToIntranetUser']);
	$MethodArr[] = array("GUARDIAN", $Lang['SMS']['SendToGuardian']);
	$MethodArr[] = array("MOBILE", $Lang['SMS']['SendToMobileNumber']);
	$MethodArr[] = array("CSV", $Lang['SMS']['SendByCSV']);
	
	$SelectMethod = $linterface->GET_SELECTION_BOX($MethodArr, " onChange='displayForm(this.value)' ", $Lang['SMS']['SendToSelect'] , "");
	$SelectMethodUsers = $linterface->GET_SELECTION_BOX($MethodArr, " id='MethodSelected' name='MethodSelected' onChange='displayForm(this.value)' ", $Lang['SMS']['SendToSelect'] , "USERS");
	
	# Start layout
	$linterface->LAYOUT_START();
	
	$libsms = new libsmsv2();

	$sql = "SELECT Content, Title FROM INTRANET_SMS_TEMPLATE ORDER BY Title";
	$templates = $libsms->returnArray($sql,2);
	// [DM#581] added word count js function if select SMS templates
	$countJS = $NoLimit? "" : "setTimeout(function() { countit(document.form1,0,'<?=$langStr?>'); }, 10);";
	$select_template = $linterface->GET_SELECTION_BOX($templates, " id='MethodSelected' name='MethodSelected' onChange=\"this.form.Message.value=this.value; this.form.Message.focus(); $countJS\" ", "-- $button_select $i_SMS_MessageTemplate --", "");


	if ($intranet_session_language == "en")
	{
	    $langStr = "English";
	}
	else
	{
	    $langStr = "Chinese";
	}
?>


<SCRIPT LANGUAGE=Javascript src="/templates/sms.js"></SCRIPT>
<script language="javascript">

var PreMethod = "";
var CurMethod = "";

function displayForm(FormName)
{
	// hide previous
	if (CurMethod!="")
		$('#div'+CurMethod).hide();
	
	//$('#mainForm').show();
	
	// show current	
	$('#div'+FormName).show();
	
	if (FormName=="CSV")
		$('#divMESSAGE').hide();
	else
		$('#divMESSAGE').show();
	$('#divSTARTDATE').show();
	$('#divSTARTTIME').show();
	$('#divSTARTTIMENOTE').show();
	$('#divNOTES').show();
	$('#divBUTTONS').show();
	
	// updates
	PreMethod = CurMethod;
	CurMethod = FormName;
}


function checkform(obj)
{	
	if (CurMethod=="USERS")
	{
         if(obj.elements["Recipient[]"].length==0 && !obj.elements["usertype[]"][0].checked && !obj.elements["usertype[]"][1].checked && !obj.elements["usertype[]"][2].checked)
         { 
	         alert("<?php echo "$button_select $i_SMS_Recipient"; ?>"); return false; 
	     }
	}
	
	if (CurMethod=="GUARDIAN")
	{
         if(obj.elements["RecipientG[]"].length==0)
         { 
	         alert("<?php echo "$button_select $i_SMS_Recipient"; ?>"); return false; 
	     }
	}
	
	if (CurMethod=="MOBILE")
	{
		if (!check_text(obj.recipient,'<?=$i_SMS_AlertRecipient?>')) return false;
	}
	
	if (CurMethod!="CSV")
	{
	 if (!check_text(obj.Message,'<?=$i_SMS_AlertMessageContent?>')) return false;
	}
	 
//	if(obj.Message.value.length > 160){
//		countit(obj,0,'<?=$langStr?>');
//		return false;
//	}
	var originalMsg = obj.Message.value;
	countit(obj,0,'<?=$langStr?>');
	if (originalMsg != obj.Message.value) {
		return false;
	}
	 
	 <? if ($sms_vendor != "TACTUS") { ?>
	     if (obj.scdate.value != '')
	     {
	         if (!check_date(obj.scdate,'<?=$i_invalid_date?>')) return false;
	     }
	     if (obj.sctime.value != '')
	     {
	         if (!check_time(obj.sctime, '<?=$i_SMS_InvalidTime?>')) return false;
	     }
	     
	     if (obj.scdate.value != '' && obj.sctime.value != '') {
	     	var curTs = new Date().getTime();
	     	
	     	var scheduledDateText = obj.scdate.value;
	     	var scheduledDateAry = scheduledDateText.split('-');
	     	var scheduledYear = scheduledDateAry[0];
	     	var scheduledMonth = scheduledDateAry[1] - 1;
	     	var scheduledDate = scheduledDateAry[2];
	     	
	     	var scheduledTime = obj.sctime.value;
	     	var scheduledTimeAry = scheduledTime.split(':');
	     	var scheduledHour = scheduledTimeAry[0];
	     	var scheduledMin = scheduledTimeAry[1];
	     	var scheduledSec = scheduledTimeAry[2];
	     	
			var scheduledDateObj = new Date(scheduledYear, scheduledMonth, scheduledDate, scheduledHour, scheduledMin, scheduledSec);
			var scheduledDateTs = scheduledDateObj.getTime();
			
			if (scheduledDateTs <= curTs) {
				// scheduled date has past already
				alert('<?=$Lang['SMS']['ScheduledDateIsPast']?>');
				return false;
			}
	     }
	 <? } ?>
	 
	if (CurMethod=="USERS")
	{
         checkOptionAll(obj.elements["Recipient[]"]);
         obj.action = "send_user_update.php";
	} else if (CurMethod=="GUARDIAN")
	{
         checkOptionAll(obj.elements["RecipientG[]"]);
         obj.action = "send_guardian_update.php";
	} else if (CurMethod=="MOBILE")
	{
         obj.action = "send_mobile_update.php";
	} else if (CurMethod=="CSV")
	{
         obj.action = "send_csv_confirm.php";
	}
}
</SCRIPT>


	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($msg, $xmsg);?></td>
	</tr>
	
		<tr>
			<td class="navigation">
				<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr><td><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?></td></tr>
				</table>
			</td>
		</tr>
	</table>
	
	<form name="form1" action='send_user_update.php' method='POST' enctype="multipart/form-data" ONSUBMIT="return checkform(this)">
	
	<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
		
		
		<!-- Title -->
		<tr>
		   <td class="tabletext formfieldtitle" width="30%" valign="top"  nowrap='nowrap'><?=$Lang['SMS']['SendTo']?> <span class="tabletextrequire">*</span></td>
			<td align="left" valign="top">
			  <?= $SelectMethod ?>
			</td>
		</tr>
		
		
		<tr id="divUSERS"  style="display:none" >
		   <td class="tabletext formfieldtitle" width="30%" valign="top"  nowrap='nowrap'><?=$i_SMS_Recipient?> <span class="tabletextrequire">*</span></td>
			<td align="left" valign="top">
				<table border="0">
				<tr>
					<td><select name="Recipient[]" size=4 multiple></select></td>
					<td width="50%"">
					<?= $linterface->GET_BTN($Lang['General']['ChooseUser'], "button", "newWindow('../../../../common_choose/index.php?fieldname=Recipient[]&page_title=SelectAudience&permitted_type=1,2,3&DisplayGroupCategory=1&OpenerFormName=form1&filterNotAppParentOption=1',3)") ?>
					<p>
					<?= $linterface->GET_BTN($Lang['Btn']['RemoveSelected'], "button", "checkOptionRemove(document.form1.elements['Recipient[]'])") ?>
					</p>
					</td>
				</tr>
				</table>
				<input type=checkbox name="usertype[]" value=1><?="$i_status_all $i_identity_teachstaff"?>&nbsp;
				<input type=checkbox name="usertype[]" value=2><?="$i_status_all $i_identity_student"?>&nbsp;
				<input type=checkbox name="usertype[]" value=3><?="$i_status_all $i_identity_parent"?>

			</td>
		</tr>
		
		
		<tr id="divGUARDIAN"  style="display:none" >
		   <td class="tabletext formfieldtitle" width="30%" valign="top"  nowrap='nowrap'><?=$i_SMS_Recipient?> <span class="tabletextrequire">*</span></td>
			<td align="left" valign="top">
				<table border="0">
				<tr>
					<td><select name="RecipientG[]" size=4 multiple></select></td>
					<td width="50%"">
					<?= $linterface->GET_BTN($Lang['General']['ChooseUser'], "button", "newWindow('select_guardian.php?fieldname=RecipientG[]&page_title=SelectAudience&OpenerFormName=form1',3)") ?>
					<p>
					<?= $linterface->GET_BTN($Lang['Btn']['RemoveSelected'], "button", "checkOptionRemove(document.form1.elements['RecipientG[]'])") ?>
					</p>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		
		<tr id="divMOBILE" style="display:none">
		   <td class="tabletext formfieldtitle" width="30%" valign="top"  nowrap='nowrap'><?=$i_SMS_Recipient?> <span class="tabletextrequire">*</span></td>
			<td align="left" valign="top">
				<TEXTAREA name=recipient rows=5 cols=50></TEXTAREA><br /><font color=green><?=$i_SMS_SeparateRecipient?></font>
			</td>
		</tr>
		
		<tr id="divCSV" style="display:none">
		   <td class="tabletext formfieldtitle" width="30%" valign="top"  nowrap='nowrap'><?=$i_SMS_UploadFile?> <span class="tabletextrequire">*</span></td>
			<td align="left" valign="top">
				<input type=file name="userfile" size="40" />
				<br /><p>
<?=$i_SMS_FileDescription?>
<br>
<p><a class="functionlink_new" href="<?=GET_CSV("sms_sample.csv")?>"><?=$i_general_clickheredownloadsample?></a>
			</td>
		</tr>
		
		<tr id="divMESSAGE"  style="display:none" >
		   <td class="tabletext formfieldtitle" width="30%" valign="top"  nowrap='nowrap'><?=$i_SMS_MessageContent?> <span class="tabletextrequire">*</span></td>
			<td align="left" valign="top">
			  <?=$select_template?><br />
<?php
if ($NoLimit)
{
?>
			<TEXTAREA NAME="Message" id="Message" COLS="50" ROWS="4" WRAP="virtual"></TEXTAREA><FONT>&nbsp;</FONT><INPUT TYPE=TEXT NAME="Size" VALUE="160" SIZE="3" MAXLENGTH="3">
<?php
} else
{
?>
			<TEXTAREA NAME="Message" id="Message" COLS="50" ROWS="4" WRAP="virtual" onKeyUp="countit(this.form,0,'<?=$langStr?>');" onpaste="setTimeout(function() { countit(document.form1,0,'<?=$langStr?>'); }, 10)" ondrop="setTimeout(function() { countit(document.form1,0,'<?=$langStr?>'); }, 10);"></TEXTAREA><FONT>&nbsp;</FONT><INPUT TYPE=TEXT NAME="Size" VALUE="160" SIZE="3" MAXLENGTH="3">
<?php
}
?>
			</td>
		</tr>
		
		<? if ($sms_vendor != "TACTUS") { ?>
			
			
		<tr id="divSTARTDATE"  style="display:none">
		   <td class="tabletext formfieldtitle" width="30%" valign="top"  nowrap='nowrap'><?=$i_SMS_SendDate?></td>
			<td align="left" valign="top">
			  <input type=text size=10 maxlength=10 name=scdate> (YYYY-MM-DD)
			</td>
		</tr>
		
		
		<tr id="divSTARTTIME"  style="display:none">
		   <td class="tabletext formfieldtitle" width="30%" valign="top"  nowrap='nowrap'><?=$i_SMS_SendTime?></td>
			<td align="left" valign="top">
			  <input type=text size=10 maxlength=10 name=sctime> (HH:mm:ss)
			</td>
		</tr>
		
		
		<tr id="divSTARTTIMENOTE"  style="display:none">
		   <td class="tabletext " width="30%" valign="top"  nowrap='nowrap'>&nbsp;</td>
			<td align="left" valign="top">
			  <font color=green><?=$i_SMS_NoteSendTime?></font>
			</td>
		</tr>
		<? } ?>	

		<tr id="divNOTES"  style="display:none">
		   <td class="tabletext formfieldtitle" width="30%" valign="top"  nowrap='nowrap'>&nbsp;</td>
			<td align="left" valign="top">
			  <?=$libsms->Get_SMS_Limitation_Note_Table()?>
			</td>
		</tr>	
		
	</table>	

	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center" id="divBUTTONS"  style="display:none">
		<tr>
			<td>
				<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>
					<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
					<tr>
						<td align="center">
							<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit")?>&nbsp;
							<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "javascript:history.back()")?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
	<INPUT TYPE=HIDDEN NAME='options' VALUE='0'>
	
	<INPUT TYPE=HIDDEN NAME='chinesechar' VALUE=''>
	
	<INPUT TYPE=HIDDEN NAME='Textsize' VALUE=''>
	</form>
	


<?

	$linterface->LAYOUT_STOP();
	
	intranet_closedb();
?>