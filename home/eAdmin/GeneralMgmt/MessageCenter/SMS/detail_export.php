<?php
/*
 * 2013-03-20	(Ivan) [2012-0928-1438-14066]
 * 				- added logic to show scheduled sms send time
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php"); 
include_once($PATH_WRT_ROOT."includes/libexporttext.php");


if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-SMS"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

intranet_opendb();

$libsms = new libsmsv2();
$lexport = new libexporttext();

$namefield = getNameFieldByLang("b.");

$StatusField = '';
if ($sms_vendor == 'CTM' || $sms_vendor == 'TACTUS')
{
	$StatusField = ', '.$libsms->Get_SMS_Status_Field('a', 0).' as RecordStatus';
}

if ($sms_vendor != "TACTUS") {
	$scheduleDateTimeField = " , If (ssm.ScheduleDateTime is null OR ssm.ScheduleDateTime = '0000-00-00 00:00:00', '".$Lang['General']['EmptySymbol']."', ssm.ScheduleDateTime) as ScheduleDateTime ";
}

$sql = "
		SELECT
			a.RecipientPhoneNumber,  
			a.RecipientName,
			$namefield as RelatedUserName,  
			a.Message,                             
			a.DateInput     
			$StatusField
			$scheduleDateTimeField
		FROM INTRANET_SMS2_MESSAGE_RECORD as a
			Inner Join INTRANET_SMS2_SOURCE_MESSAGE as ssm On (a.SourceMessageID = ssm.SourceID)
            LEFT OUTER JOIN INTRANET_USER as b ON a.RelatedUserID = b.UserID
		WHERE
			SourceMessageID = $SourceID
			and
				(
               a.RecipientPhoneNumber 	LIKE '%$keyword%' or
               a.RecipientName 			LIKE '%$keyword%' or
               $namefield		 		LIKE '%$keyword%' or
               a.Message 				LIKE '%$keyword%' 
               )       
";
        
$li = new libdb();
$result = $li->returnArray($sql);
/*
$csv = "Mobile,Receiver Name,Related User, Message,Real Transmission Time\n";

for ($i=0; $i<sizeof($result); $i++)
{
     $delim = "";
     for ($j=0; $j<sizeof($result[$i]); $j++)
     {
          $csv .= "$delim\"".$result[$i][$j]."\"";
          $delim = ",";
     }
     $csv .= "\n";
}

$filename = "file/export/smslog-".session_id()."-".time().".csv";
$export_filepath = "$intranet_root/$filename";
write_file_content($csv,$export_filepath);
*/

$exportColumn = array("Mobile", "Receiver Name", "Related User", "Message", "Creation Time");
if ($sms_vendor != "TACTUS") {
	$exportColumn[] = 'Scheduled Send Time';
}
if ($sms_vendor == 'CTM' || $sms_vendor == 'TACTUS')
	$exportColumn[] = "Status";

$numOfSMS = count($result);
$exportContent = array();
for ($i=0; $i<$numOfSMS; $i++)
{
	$exportContent[$i][] = $result[$i]['RecipientPhoneNumber'];
	$exportContent[$i][] = $result[$i]['RecipientName'];
	$exportContent[$i][] = $result[$i]['RelatedUserName'];
	$exportContent[$i][] = $result[$i]['Message'];
	$exportContent[$i][] = $result[$i]['DateInput'];
	
	if ($sms_vendor != "TACTUS") {
		$exportContent[$i][] = $result[$i]['ScheduleDateTime'];
	}
	
	if ($sms_vendor == 'CTM' || $sms_vendor == 'TACTUS') {
		$exportContent[$i][] = $result[$i]['RecordStatus'];
	}
}
$export_content = $lexport->GET_EXPORT_TXT($exportContent, $exportColumn);


$filename = "file/export/smslog-".session_id()."-".time().".csv";
$lexport->EXPORT_FILE($filename, $export_content);


//header("Location: $intranet_httppath/$filename");
intranet_closedb();
?>