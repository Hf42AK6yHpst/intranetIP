<?
//Modifying by: 
#######################################
## this page used in:
## /home/eAdmin/GeneralMgmt/payment/sms_notification/index.php
## /home/eAdmin/StudentMgmt/attendance/dailyoperation/absence/show.php
## /home/eAdmin/StudentMgmt/attendance/dailyoperation/early/show.php
## /home/eAdmin/StudentMgmt/attendance/dailyoperation/late/showlate.php
## /home/eAdmin/GeneralMgmt/MessageCenter/ParentNotification/new.php
## /home/eAdmin/StudentMgmt/notice/result.php
## /home/eService/notice/tableview.php
#######################################
##	Date:	2015-07-28 Omas
##			Add export mobile number for teacher
##
##	Date:	2015-07-21 Shan
##			Add task "byClassName"
##
##	Date:	2015-06-05	Omas
##			Create this page
#######################################
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$lexport = new libexporttext();
$luser = new libuser();
$lclass = new libclass();
 
/* 
 * 2015-06-04 Omas
 * Pass In $StudentIDArr Or Create a new task to retrieve StudentIDArr 
 */

if(!$_GET['teacher']){
	$exportHeaderAry['En'] = array("Class Name","Class No.","Student Name","Parent Name","Contact No.");
	$exportHeaderAry['Ch'] = array("班別","班號","學生姓名","家長姓名","聯絡電話");
	if($task == 'byClassName'){		
		$stu_ary = array(); 
		$stu_ary =$lclass->getStudentIDByClassName($ClassID);  
		$StudentIDArr = array_unique ($stu_ary);  
	}
	else if($task == 'byNoticeID'){
		
		include_once($PATH_WRT_ROOT."includes/libnotice.php");
		$lnotice = new libnotice($NoticeID);
		$data = $lnotice->returnTableViewAll();
		$numData = count($data);
		
	
		
		if ($numData>0){
			for ($i=0; $i<$numData; $i++){
				if ($lnotice->RecordType == 4){
					list($sid,$name,$classNo,$type,$status,$answer,$signer,$last,$class_name, $signer_id,$websamsRegNo,$signerAuthCode) = $data[$i];
			 	}
			 	else{
				 	list($sid,$name,$classNo,$type,$status,$answer,$signer,$last,$class_name, $signer_id,$websamsRegNo,$signerAuthCode) = $data[$i];
			 	}
			 	if ($status != 2){
	                $unsigned_StudentIDArr[] = $sid;
	            }
			}
			$StudentIDArr = $unsigned_StudentIDArr;	 
		}
	}
	else{
		$StudentIDArr = $_POST['StudentIDArr'];
		if(empty($StudentIDArr)){
			$StudentIDArr = $_GET['StudentIDArr'];
		}
	}
	
	if(!empty($StudentIDArr)){
		if($StudentIDArr == 'all'){
			// do nth
		}
		else if(is_array($StudentIDArr)){
			// do nth
		}
		else{
			$StudentIDArr = unserialize(rawurldecode($StudentIDArr));
		}
			
		// Get Student's Parent Info, in dont have parents will search in guardian
		if($StudentIDArr == 'all'){
			$result  = $luser->getParentGuardianInfoAry(array(),0,1);
		}
		else{
			$result  = $luser->getParentGuardianInfoAry((array)$StudentIDArr);
			
		}
	}
	if(count($result>0)){
		$row = 0;
		foreach((array)$result as $_InfoAry){
			$exportDataAry[$row][] = $_InfoAry['ClassName'];
			$exportDataAry[$row][] = $_InfoAry['ClassNumber'];
			$exportDataAry[$row][] = $_InfoAry['StudentName'];
			$exportDataAry[$row][] = $_InfoAry['ParentName'];
			$exportDataAry[$row][] = $_InfoAry['ContactNumber'];
			$row++;
		}
	}
	$filename = "non_app_parent_mobile_number.csv";
}
else{
	$exportHeaderAry['En'] = array("Teacher Name","Contact No.");
	$exportHeaderAry['Ch'] = array("教職員姓名","聯絡電話");
	
	$teacherWithAppIDs = array_unique($luser->getTeacherWithTeacherUsingTeacherApp());
	$teacherNonAppIDs = Get_Array_By_Key( $luser->returnUsersType2('1','-1','',$teacherWithAppIDs) , 'UserID' );
	$userInfoObj = new libuser('','',$teacherNonAppIDs);
	
	$row = 0;
	foreach((array)$teacherNonAppIDs as $uid){
		$userInfoObj->LoadUserData($uid);
		
		$exportDataAry[$row][] = $userInfoObj->StandardDisplayName;
		$exportDataAry[$row][] = $userInfoObj->MobileTelNo;
		$row++;
	}
	
	$filename = "non_app_teacher_mobile_number.csv";
}
$export_header = $lexport->GET_EXPORT_HEADER_COLUMN($exportHeaderAry,$colProperty);
$export_content = $lexport->GET_EXPORT_TXT($exportDataAry, $export_header);

intranet_closedb();
$lexport->EXPORT_FILE($filename, $export_content);
?>