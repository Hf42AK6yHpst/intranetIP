<?php
//Modifying by:

############ Change Log Start ###############
#	Date:	2020-07-31 (Ray)
#			added filter sent from myself
#
#	Date:	2019-10-22 (Philips)
#			modified query
#
#	Date:	2019-09-24 (Philips)
#			added imageFail msg for incorrect attachment format
#
#   Date:   2018-05-23 (Philips)
#           added Filter for sended by System and Teachers
#
#   Date:	2018-03-14 (Isaac) [DM#3390]
#           added intranet_htmlspecialchars() to $keyword
#
#   Date:	2017-01-17 (Isaac)
#	        Improved: added EnglishName & ChineseName to keyword search's sql
#                     added Get_Safe_Sql_Like_Query to keyword search's sql
#
# Date:		2016-10-06	Villa[K104806]
#			update the msg from the $msg from update.php
#
# Date:		2016-05-20	Kenneth [ip.2.5.7.7.1]
#			Message with userID = 0 => marked as 'System Generated'
#
# Date:		2016-03-11	Ivan [ip.2.5.7.4.1]
#			added student app logic
#
# Date:		2015-08-04 Omas: [ip.2.5.6.7.1]
#			- changed lang of filter if usertype is teacher
#
# Date:		2015-05-08 Ivan: [ip.2.5.6.5.1]
#			- add logic for settings "Allow non-admin users to delete own push message records"
#
# Date:		2014-10-23 Roy: [ip.2.5.5.10.1]
#			- show message sent by user only if user is not admin but have right to send push message
#
############ Change Log End ###############

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");


intranet_auth();
intranet_opendb();

$limc = new libmessagecenter();
$leClassApp = new libeClassApp();

$appType = ($_POST['appType'])? $_POST['appType'] : $_GET['appType'];
//$appType = ($appType=='')? $eclassAppConfig['appType']['Parent'] : $appType;
if ($appType == '') {
	if ($leClassApp->isSchoolInLicense($eclassAppConfig['appType']['Parent'])) {
		$appType = $eclassAppConfig['appType']['Parent'];
	}
	else if ($leClassApp->isSchoolInLicense($eclassAppConfig['appType']['Teacher'])) {
		$appType = $eclassAppConfig['appType']['Teacher'];
	}
}


//if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ParentAppNotify"])
//{
//	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
//	$laccessright = new libaccessright();
//	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
//	exit;
//}

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$eClassAppSettingsObj = $leClassApp->getAppSettingsObj();
$nonAdminUserCanDeleteOwnPushMessageRecord = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['nonAdminUserCanDeleteOwnPushMessageRecord']);

if ($appType == $eclassAppConfig['appType']['Parent']) {
	$CurrentPage = "PageParentNotification";
	$conds_AppType = " And (m.AppType = '".$appType."' OR m.AppType = '' OR m.AppType is null) ";
	$TAGS_OBJ[] = array($Lang['Header']['Menu']['ParentAppsNotify']);
	
	$publicLang = $Lang['AppNotifyMessage']['Public'];
	
	if ($nonAdminUserCanDeleteOwnPushMessageRecord) {
		$canDelete = true;
	}
	else {
		$canDelete = ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassApp"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-ParentAppNotify"])? true : false;
	}
	
	$isAppTypeMessageCenterAdmin = ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ParentAppNotify"])? true : false;
}
else if ($appType == $eclassAppConfig['appType']['Teacher']) {
	$CurrentPage = "PageTeacherNotification";
	$conds_AppType = " And m.AppType = '".$appType."' ";
	$TAGS_OBJ[] = array($Lang['Header']['Menu']['TeacherAppsNotify']);
	
	$publicLang = $Lang['AppNotifyMessage']['Public(Teacher)'];
	
	if ($nonAdminUserCanDeleteOwnPushMessageRecord) {
		$canDelete = true;
	}
	else {
		$canDelete = ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassTeacherApp"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-TeacherAppNotify"])? true : false;
	}
	
	$isAppTypeMessageCenterAdmin = ($_SESSION["SSV_USER_ACCESS"]["eAdmin-TeacherAppNotify"])? true : false;
}
else if ($appType == $eclassAppConfig['appType']['Student']) {
	$CurrentPage = "PageStudentNotification";
	$conds_AppType = " And m.AppType = '".$appType."' ";
	$TAGS_OBJ[] = array($Lang['Header']['Menu']['StudentAppsNotify']);
	
	$publicLang = $Lang['AppNotifyMessage']['Public(Student)'];
	
	if ($nonAdminUserCanDeleteOwnPushMessageRecord) {
		$canDelete = true;
	}
	else {
		$canDelete = ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassStudentApp"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAppNotify"])? true : false;
	}
	
	$isAppTypeMessageCenterAdmin = ($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAppNotify"])? true : false;
}

if($sys_custom['PowerClass']){
	$TAGS_OBJ = array();
	$curTab = $appType;
	$TAGS_OBJ[] = array($Lang['Header']['Menu']['ParentAppsNotify'],'index.php?appType='.$eclassAppConfig['appType']['Parent'],$curTab==$eclassAppConfig['appType']['Parent']);
	$TAGS_OBJ[] = array($Lang['Header']['Menu']['StudentAppsNotify'],'index.php?appType='.$eclassAppConfig['appType']['Student'],$curTab==$eclassAppConfig['appType']['Student']);
	$TAGS_OBJ[] = array($Lang['Header']['Menu']['TeacherAppsNotify'],'index.php?appType='.$eclassAppConfig['appType']['Teacher'],$curTab==$eclassAppConfig['appType']['Teacher']);
}

//if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ParentAppNotify"]) {
if ($isAppTypeMessageCenterAdmin) {
	// if user is eClass parent app admin, display all messages sent
	$conds_sortSender = "And 1";
} else if ($_SESSION["SSV_PRIVILEGE"]["MessageCenter"]["allowSendPushMessage"]) {
	// if user is not eClass parent app admin, show message sent by himself only
	if ($_SESSION["USER_BASIC_INFO"]["is_class_teacher"]) {
		$conds_sortSender = "And m.NotifyUserID = ".$UserID;
	}
	if ($_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_club_pic"]) {
		$conds_sortSender = "And m.NotifyUserID = ".$UserID;
		
	}
	if ($_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_activity_pic"]) {
		$conds_sortSender = "And m.NotifyUserID = ".$UserID;
		
	}
}

$linterface = new interface_html();
$limc->checkNotifyMessageAccessRight($appType);

# TABLE INFO
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == "") ? 0 : $order;
$field = ($field == "") ? 3 : $field;


$li = new libdbtable2007($field, $order, $pageNo);

if ($canDelete) {
	$checkboxSql = ",
            CONCAT('<input type=\"checkbox\" name=\"NotifyMessageID[]\" value=\"', m.NotifyMessageID ,'\">') ";
}


if($filter != ""){
	$conds_other.= " AND m.IsPublic = '$filter' ";
}
// Added By Philips 2018-05-23
if(!isset($filter_sender)){
	$filter_sender = 'T';
}
if($filter_sender != ""){
	if($filter_sender == 'T'){
		$conds_other .= "AND b.Teaching IN ('1', '0', 'S')"; // Teachers
	}
}
// Added By Philips 2018-05-23
if($keyword != ""){
	$keyword_safe = $li->Get_Safe_Sql_Query(intranet_htmlspecialchars($keyword));
	$conds_other.= " AND (m.NotifyDateTime LIKE '%".$keyword_safe."%'
				OR m.MessageTitle LIKE '%".$keyword_safe."%'
				OR m.MessageContent LIKE '%".$keyword_safe."%'
				OR m.NotifyFor LIKE '%".$keyword_safe."%'
				OR m.NotifyUser LIKE '%".$keyword_safe."%'
				OR b.EnglishName like '%$keyword_safe%'
				OR b.ChineseName like '%$keyword_safe%')";
	$conds_keyword = " AND (m.NotifyDateTime LIKE '%".$keyword_safe."%'
				OR m.MessageTitle LIKE '%".$keyword_safe."%'
				OR m.MessageContent LIKE '%".$keyword_safe."%'
				OR m.NotifyFor LIKE '%".$keyword_safe."%'
				OR m.NotifyUser LIKE '%".$keyword_safe."%') ";
}

$user_field = getNameFieldByLang("b.");

### GET User Name Array
$userAry = array();
if($filter_sender != 'S'){
	if($filter_sender == 'M') {
        $cond = " b.UserID='".$_SESSION['UserID']."'";
	} else {
		$cond = " 1 $conds_other";
	}
	$sql = "SELECT b.UserID, $user_field AS Name
		FROM INTRANET_USER b 
		INNER JOIN INTRANET_APP_NOTIFY_MESSAGE m ON m.NotifyUserID = b.UserID 
		WHERE $cond
		GROUP BY b.UserID";
	$userAry = $leClassApp->returnArray($sql);
	$userIDAry = Get_Array_By_Key($userAry, 'UserID');
	$userAry = BuildMultiKeyAssoc($userAry, 'UserID');
	if($filter_sender!='T' && $filter_sender != 'M'){
		$userIDAry[] = '0';
	}
	$conds_user = "AND m.NotifyUserID IN ('" . implode("','", $userIDAry) . "') ";
	if(count($userIDAry) > 0) {
		$conds_keyword = " AND (m.NotifyDateTime LIKE '%".$keyword_safe."%'
				OR m.MessageTitle LIKE '%".$keyword_safe."%'
				OR m.MessageContent LIKE '%".$keyword_safe."%'
				OR m.NotifyFor LIKE '%".$keyword_safe."%'
				OR m.NotifyUser LIKE '%".$keyword_safe."%'
				OR m.NotifyUserID IN ('" . implode("','", $userIDAry) . "')
				) ";
	}
} else {
	$conds_user.= "AND m.NotifyUserID = '0'"; // System
}

$sql = "SELECT
            CONCAT('<a class=\"tableContentLink\" href=\"javascript:void(0);\" onclick=\"goViewMessageDetails(', m.NotifyMessageID, ');\" title=\"".$Lang['AppNotifyMessage']['ViewDetailedRecord']."\">', m.MessageTitle, '</a>') as MessageTitle,
			IF(m.IsPublic = 'Y','".$publicLang."',COUNT(t.NotifyMessageTargetID)) as TargetUserCount, 
			IF(m.NotifyUserID = '0', '".$Lang['General']['SystemGenerated']."', CONCAT('<--USER_', m.NotifyUserID  ,'-->') ) AS NotifyUser,
            m.DateInput,
			m.NotifyDateTime
			$checkboxSql
		FROM INTRANET_APP_NOTIFY_MESSAGE as m  
		INNER JOIN INTRANET_APP_NOTIFY_MESSAGE_TARGET as t ON t.NotifyMessageID = m.NotifyMessageID
		WHERE m.RecordStatus = 1 
				$conds_AppType
				$conds_sortSender
				$conds_user
				$conds_keyword
			";
//IF($user_field IS NULL,'".$Lang['General']['SystemGenerated']."',$user_field) AS NotifyUser,
$sql .=" GROUP BY m.NotifyMessageID ";
$li->sql = $sql;
$li->field_array = array("m.MessageTitle","TargetUserCount","NotifyUser", "m.DateInput", "m.NotifyDateTime");
//$li->no_col = sizeof($li->field_array)+2;
$li->no_col = sizeof($li->field_array)+1;
if ($canDelete) {
	$li->no_col++;
}
$li->IsColOff = "IP25_table";
$li->column_array = array(0,0,0,0);
$li->wrap_array = array(0,0,0,0);

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<th class=num_check>#</th>\n";
$li->column_list .= "<th width=35% >".$li->column($col++, $Lang['AppNotifyMessage']['Title'])."</th>\n";
//$li->column_list .= "<th width=20% >".$li->column($col++, $Lang['AppNotifyMessage']['Public'])."</th>\n";
$li->column_list .= "<th width=10% >".$li->column($col++, $Lang['AppNotifyMessage']['NumOfUsers'])."</th>\n";
$li->column_list .= "<th width=20% >".$li->column($col++, $Lang['General']['CreatedBy'])."</th>\n";
$li->column_list .= "<th width=15% >".$li->column($col++, $Lang['IP_FILE']['InputDate'])."</th>\n";
$li->column_list .= "<th width=15% >".$li->column($col++, $Lang['AppNotifyMessage']['SendTime'])."</th>\n";
if ($canDelete) {
	$li->column_list .= "<th width=1 >".$li->check("NotifyMessageID[]")."</th>\n";
}

$toolbar = $linterface->GET_LNK_ADD("javascript:void(0);",$button_new,"goNewMessage();","","",0);

if($appType==$eclassAppConfig['appType']['Teacher']){
	$public_arr[] = array("Y", $Lang['AppNotifyMessage']['ToPublic(Teacher)']);
	$public_arr[] = array("N", $Lang['AppNotifyMessage']['ToNonPublic(Teacher)']);
}
else if ($appType==$eclassAppConfig['appType']['Student']) {
	$public_arr[] = array("Y", $Lang['AppNotifyMessage']['ToPublic(Student)']);
	$public_arr[] = array("N", $Lang['AppNotifyMessage']['ToNonPublic(Student)']);
}
else{
	$public_arr[] = array("Y", $Lang['AppNotifyMessage']['ToPublic']);
	$public_arr[] = array("N", $Lang['AppNotifyMessage']['ToNonPublic']);
}
$filter_menu = $linterface->GET_SELECTION_BOX($public_arr, "name='filter' id='filter' onChange='this.form.submit()'", $Lang['AppNotifyMessage']['AllRecords'], $filter);
// Added By Philips 
$sender_arr[] = array("T", $Lang['AppNotifyMessage']['FromTeacher']);
$sender_arr[] = array("M", $Lang['AppNotifyMessage']['FromMyself']);
$sender_arr[] = array("S", $Lang['AppNotifyMessage']['FromSystem']);
$sender_menu = $linterface->GET_SELECTION_BOX($sender_arr, "name='filter_sender' id='filter_sender' onChange='this.form.submit()'", $Lang['AppNotifyMessage']['AllSenders'], $filter_sender);
// Added By Philips
### Title ###
$MODULE_OBJ = $limc->GET_MODULE_OBJ_ARR();
//Villa [K104806] 2016-10-06 Avoid Display problem of B5 in the hyper link

if($msg=='success'){
	$msg = $Lang['AppNotifyMessage']['ReturnMsg']['SendSuccess'];
}elseif($msg=='fail'){
	$msg = $Lang['AppNotifyMessage']['ReturnMsg']['SendFail'];
}elseif($msg=='imageFail'){
	$msg = $Lang['General']['ReturnMessage']['ImageUploadFail'];
}else{
	$msg = '';
}
$linterface->LAYOUT_START($msg);

## search
$searchTag = "<input name=\"keyword\" id=\"keyword\" type=\"text\" value=\"".stripslashes($keyword)."\" onkeyup=\"Check_Go_Search(event);\" />";

## replace userID tag by user name field
$resultTable = $li->display();
foreach($userAry as $uid => $detail){
	$resultTable = str_replace('<--USER_'.$uid.'-->', $detail['Name'], $resultTable);
}
?>

<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script> 
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.blockUI.js"></script> 
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.tablednd_0_5.js"></script> 
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.cookies.2.2.0.js"></script> 

<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css" type="text/css" media="screen" /> 

<script language="javascript">
<!--

function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		document.form1.submit();
	else
		return false;
}

function goNewMessage() {
	$('form#form1').attr('method', 'post').attr('action', 'new.php').submit();
}

function goViewMessageDetails(targetNotifyMessageId) {
	$('input#targetNotifyMessageID').val(targetNotifyMessageId);
	$('form#form1').attr('method', 'post').attr('action', 'view_detail.php').submit();
}
-->
</script>

<form name="form1" id="form1" method="get" action="index.php" onSubmit="return checkForm()">

<div class="content_top_tool">
	<?=$toolbar?>
	<div class="Conntent_search"><?=$searchTag?></div>     
	<br style="clear:both" />
</div>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr class="common_table_tool">
	<td valign="bottom">
        <div class="table_filter">
            <?=$filter_menu?>
            <?=$sender_menu?>
        </div>
	</td>
	<td valign="bottom">
		<? if ($canDelete) { ?>
		<div>
		<a href="javascript:checkRemove(document.form1,'NotifyMessageID[]','delete.php')" class="tool_delete"><?=$Lang['Btn']['Delete']?></a>
		</div>
		<? } ?>
	</td>
</tr>
</table>
<?=$resultTable?>


<br />
<input type="hidden" name="completeFlag" id="completeFlag" value="<?=$completeFlag?>" />
<input type="hidden" name="requestFlag" id="requestFlag" value="<?=$requestFlag?>" />
<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="appType" id="appType" value="<?=$appType?>" />
<input type="hidden" name="targetNotifyMessageID" id="targetNotifyMessageID" value="" />

</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>