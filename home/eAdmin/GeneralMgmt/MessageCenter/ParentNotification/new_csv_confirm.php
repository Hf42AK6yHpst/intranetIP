<?php
# using: 
###################### Change Log Start ########################
#   Date:   2020-09-22 Ray
#           student haven't login app not in receipient list
#
#   Date:   2020-03-12 Ray
#           added messageContent length check
#
#	Date:	2016-03-11 Ivan [ip.2.5.7.4.1]
#			added student app logic
#
#	Date:	2015-09-01 Ivan [A83733] [ip.2.5.6.10.1.0]
#			Modified: Fixed to support one student linked with multiple parent accounts
#
#	Date: 	2013-05-10 yuen 
#			first version
#
###################### Change Log End ########################


$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");

intranet_auth();
intranet_opendb();

# to be changed
//if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ParentAppNotify"])
//{
//	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
//	$laccessright = new libaccessright();
//	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
//	exit;
//}

# Create a new interface instance
$linterface = new interface_html();
$limc = new libmessagecenter();
$li = new libdb();
$luser = new libuser();
$libimport = new libimporttext();

$appType = $_POST['appType'];
$sendTimeMode = standardizeFormPostValue($_POST['sendTimeMode']);
$sendTimeString = standardizeFormPostValue($_POST['sendTimeString']);
$limc->checkNotifyMessageAccessRight($appType);

if ($appType == $eclassAppConfig['appType']['Parent']) {
	$CurrentPage = "PageParentNotification";
	$TAGS_OBJ[] = array($Lang['Header']['Menu']['ParentAppsNotify']);
	$PAGE_NAVIGATION[] = array($Lang['Header']['Menu']['ParentAppsNotify'], "index.php?appType=$appType");
	$validTableTitleLang = $Lang['AppNotifyMessage']['valid_table_title'];
}
else if ($appType == $eclassAppConfig['appType']['Teacher']) {
	$CurrentPage = "PageTeacherNotification";
	$TAGS_OBJ[] = array($Lang['Header']['Menu']['TeacherAppsNotify']);
	$PAGE_NAVIGATION[] = array($Lang['Header']['Menu']['TeacherAppsNotify'], "index.php?appType=$appType");
	$validTableTitleLang = $Lang['AppNotifyMessage']['valid_table_title_teacher'];
}
else if ($appType == $eclassAppConfig['appType']['Student']) {
	$CurrentPage = "PageStudentNotification";
	$TAGS_OBJ[] = array($Lang['Header']['Menu']['StudentAppsNotify']);
	$PAGE_NAVIGATION[] = array($Lang['Header']['Menu']['StudentAppsNotify'], "index.php?appType=$appType");
	$validTableTitleLang = $Lang['AppNotifyMessage']['valid_table_title_student'];
}


//	$sql = "CREATE TABLE IF NOT EXISTS TEMPSTORE_PUSHMESSAGE_FILE_USER (
//	 UserLogin varchar(255),
//	 ParentID int(11),
//	 MessageTitle varchar(255),
//	 Message text,
//	 UserID int(11)
//	) ENGINE=InnoDB DEFAULT CHARSET=utf8";
//	$li->db_db_query($sql);

$sql = "DELETE FROM TEMPSTORE_PUSHMESSAGE_FILE_USER where UserID='$UserID'";
$li->db_db_query($sql);


if ($FileCSV=="")
{
	header("location: new.php?msg=".$Lang['General']['ReturnMessage']['CSVUploadFail'].'&appType='.$appType);
	die();	
} elseif  (file_exists($FileCSV))
{
    $csv_data = $libimport->GET_IMPORT_TXT($FileCSV, $incluedEmptyRow=0, $lineBreakReplacement='<!--linebreak-->');
	$csv_header = $csv_data[0];
	
	$validHeader = true;
	if ($appType == $eclassAppConfig['appType']['Parent'] || $appType == $eclassAppConfig['appType']['Student']) {
		if (strtoupper($csv_header[0])!=strtoupper("Class") || strtoupper($csv_header[1])!=strtoupper("ClassNumber") || strtoupper($csv_header[2])!=strtoupper("MessageTitle") || strtoupper($csv_header[3])!=strtoupper("MessageContent")) {
			$validHeader = false;
		}
	}
	else {
		// added "UserLogin" case for DHL
		//if (strtoupper($csv_header[0])!=strtoupper("Teacher UserLogin") || strtoupper($csv_header[1])!=strtoupper("MessageTitle") || strtoupper($csv_header[2])!=strtoupper("MessageContent")) {
		if ((strtoupper($csv_header[0])!=strtoupper("Teacher UserLogin") && strtoupper($csv_header[0])!=strtoupper("UserLogin")) || strtoupper($csv_header[1])!=strtoupper("MessageTitle") || strtoupper($csv_header[2])!=strtoupper("MessageContent")) {
			$validHeader = false;
		}
	}
	
	if (!$validHeader) {
		# wrong header
		header("location: new.php?msg=".$Lang['General']['ReturnMessage']['WrongFileFormat'].'&appType='.$appType);
		die();
	}
	
	// get user mapping
	if ($appType == $eclassAppConfig['appType']['Parent']) {
		# load all students with class and classnumber
		$sql = "SELECT UserID, ClassName, ClassNumber FROM INTRANET_USER where RecordType=2 AND RecordStatus=1 AND ClassName<>'' AND ClassNumber<>'' order by ClassName, ClassNumber";
		$rows_student = $li->returnResultSet($sql);
		for ($i=0; $i<sizeof($rows_student); $i++)
		{
			$row_className = trim($rows_student[$i]["ClassName"]);
			$row_classNumber = (int) trim($rows_student[$i]["ClassNumber"]);
			if ($row_className!="" && $row_classNumber!="")
			{
				$AllStudents[$row_className][$row_classNumber] = $rows_student[$i]["UserID"];
			}  			
		}
	
		# load all parents according to class and classnumber
		$sql = "SELECT DISTINCT ip.ParentID, ip.StudentID, iu.UserLogin FROM INTRANET_PARENTRELATION AS ip, INTRANET_USER AS iu " .
				" where ip.ParentID=iu.UserID AND iu.RecordType=3 AND iu.RecordStatus=1  ";
		$rows_parent = $li->returnResultSet($sql);
		for ($i=0; $i<sizeof($rows_parent); $i++)
		{
//			$AllStudentParents[$rows_parent[$i]["StudentID"]]["UserLogin"] = $rows_parent[$i]["UserLogin"];
//			$AllStudentParents[$rows_parent[$i]["StudentID"]]["ParentID"] = $rows_parent[$i]["ParentID"];
			
			$_studentId = $rows_parent[$i]["StudentID"];
			
			$_tmpAry = array();
			$_tmpAry['UserLogin'] = $rows_parent[$i]["UserLogin"];
			$_tmpAry['ParentID'] = $rows_parent[$i]["ParentID"];
			
			$AllStudentParents[$_studentId][] = $_tmpAry;
		}
		
		$parentWithAppStudentIdAry = $luser->getStudentWithParentUsingParentApp();
		$parentWithAppAndWithNoPushMessageStudentIdAry = $luser->getStudentWithParentUsingParentApp($includeNoPushMessageUser=true);
	}
	else if ($appType == $eclassAppConfig['appType']['Teacher']) {
		$teacherIdAry = $luser->getTeacherWithTeacherUsingTeacherApp(true);
		$numOfTeacher = count($teacherIdAry);
		
		$teacherMappingAssoAry = array();
		$userObj = new libuser('', '', $teacherIdAry);
		for ($i=0; $i<$numOfTeacher; $i++) {
			$_teacherId = $teacherIdAry[$i];
			
			$userObj->LoadUserData($_teacherId);
			$_userLogin = $userObj->UserLogin;
			
			$teacherMappingAssoAry[$_userLogin] = $_teacherId;
		}
	}
	else if ($appType == $eclassAppConfig['appType']['Student']) {
		# load all students with class and classnumber
		$sql = "SELECT UserID, ClassName, ClassNumber FROM INTRANET_USER where RecordType=2 AND RecordStatus=1 AND ClassName<>'' AND ClassNumber<>'' order by ClassName, ClassNumber";
		$rows_student = $li->returnResultSet($sql);
		for ($i=0; $i<sizeof($rows_student); $i++)
		{
			$row_className = trim($rows_student[$i]["ClassName"]);
			$row_classNumber = (int) trim($rows_student[$i]["ClassNumber"]);
			if ($row_className!="" && $row_classNumber!="")
			{
				$AllStudents[$row_className][$row_classNumber] = $rows_student[$i]["UserID"];
			}  			
		}

		$studentWithAppStudentIdAry = $luser->getStudentWithStudentUsingStudentApp();
		$studentWithAppAndWithNoPushMessageStudentIdAry = $luser->getStudentWithStudentUsingStudentApp($includeNoPushMessageUser=true);
	}
	
	
	$validate_count = 0;
	$studentMsgAssoAry = array();
	$teacherMsgAssoAry = array();
	# validate the data
	for ($i=1; $i<sizeof($csv_data); $i++)
	{
		$csv_row = $csv_data[$i];
		$_tmpParentAry = array();
		
		$_colCount = 0;
		if ($appType == $eclassAppConfig['appType']['Parent'] || $appType == $eclassAppConfig['appType']['Student']) {
			$csv_row_className = trim($csv_row[$_colCount++]);
			$csv_row_classNumber = (int) trim($csv_row[$_colCount++]);
			$_studentId = $AllStudents[$csv_row_className][$csv_row_classNumber];
		}
		else if ($appType == $eclassAppConfig['appType']['Teacher']) {
			$csv_row_userLogin = trim($csv_row[$_colCount++]);
		}
		$csv_row_messageTitle = trim($csv_row[$_colCount++]);
		$csv_row_messageContent = trim($csv_row[$_colCount++]);
		$csv_row_messageContentHtml = str_replace("<!--linebreak-->", "<br>", $csv_row_messageContent);
		$csv_row_messageContentSql = str_replace("<!--linebreak-->", "\n", $csv_row_messageContent);
		
		
		$ErrorFound = "";
		if (($appType == $eclassAppConfig['appType']['Parent'] || $appType == $eclassAppConfig['appType']['Student']) && ($csv_row_className=="" || $csv_row_classNumber==""))
		{
			$ErrorFound = $Lang['AppNotifyMessage']['Error']['invalidate_class'];
		} 
		elseif (($appType == $eclassAppConfig['appType']['Parent'] || $appType == $eclassAppConfig['appType']['Student']) && $AllStudents[$csv_row_className][$csv_row_classNumber]=="")
		{
			$ErrorFound = $Lang['AppNotifyMessage']['Error']['no_student'];
		} 
		//elseif ($appType == $eclassAppConfig['appType']['Parent'] && $AllStudentParents[$AllStudents[$csv_row_className][$csv_row_classNumber]]["UserLogin"]=="")
		elseif ($appType == $eclassAppConfig['appType']['Parent'] && !isset($AllStudentParents[$AllStudents[$csv_row_className][$csv_row_classNumber]]))
		{
			$ErrorFound = $Lang['AppNotifyMessage']['Error']['no_related_parent'];
		} 
		elseif ($appType == $eclassAppConfig['appType']['Parent'] && $_studentId > 0 && !in_array($_studentId, (array)$parentWithAppStudentIdAry))
		{
			//$ErrorFound = $Lang['AppNotifyMessage']['Error']['parent_did_not_login_eClassApp'];
			if (in_array($_studentId, (array)$parentWithAppAndWithNoPushMessageStudentIdAry)) {
				$ErrorFound = $Lang['AppNotifyMessage']['Error']['parent_did_not_reg_push_message'];
			}
			else {
				$ErrorFound = $Lang['AppNotifyMessage']['Error']['parent_did_not_login_eClassApp'];
			}
		}

        elseif ($appType == $eclassAppConfig['appType']['Student'] && $_studentId > 0 && !in_array($_studentId, (array)$studentWithAppStudentIdAry))
		{
			if (in_array($_studentId, (array)$studentWithAppAndWithNoPushMessageStudentIdAry)) {
				$ErrorFound = $Lang['AppNotifyMessage']['Error']['student_did_not_reg_push_message'];
			}
			else {
				$ErrorFound = $Lang['AppNotifyMessage']['Error']['student_did_not_login_eClassApp'];
			}
		}

// 		elseif ($appType == $eclassAppConfig['appType']['Parent'] && $_studentId > 0 && !in_array($_studentId, (array)$parentWithAppStudentIdAry) && !in_array($_studentId, (array)$parentWithAppAndWithNoPushMessageStudentIdAry))
// 		{
// 			$ErrorFound = $Lang['AppNotifyMessage']['Error']['parent_did_not_login_eClassApp'];
// 		} 
		elseif ($appType == $eclassAppConfig['appType']['Teacher'] && $teacherMappingAssoAry[$csv_row_userLogin]=="")
		{
			$ErrorFound = $Lang['AppNotifyMessage']['Error']['no_teacher'];
		} 
		elseif (trim($csv_row_messageTitle)=="")
		{
			$ErrorFound = $Lang['AppNotifyMessage']['Error']['no_title'];
		} 
		elseif (trim($csv_row_messageContent)=="")
		{
			$ErrorFound = $Lang['AppNotifyMessage']['Error']['no_content'];
		}
		else if ($appType == $eclassAppConfig['appType']['Parent'] && isset($studentMsgAssoAry[$csv_row_className][$csv_row_classNumber][$csv_row_messageTitle][$csv_row_messageContent])) 
		{
			$ErrorFound = $Lang['AppNotifyMessage']['Error']['duplicated_msg'];
		}
		else if ($appType == $eclassAppConfig['appType']['Teacher'] && isset($teacherMsgAssoAry[$csv_row_userLogin][$csv_row_messageTitle][$csv_row_messageContent])) 
		{
			$ErrorFound = $Lang['AppNotifyMessage']['Error']['duplicated_msg'];
		} else if(mb_strlen($csv_row_messageContent,"UTF-8") > 500) {
			$ErrorFound = $Lang['MessageCenter']['ContentMaximumRemarks'];
		}

		if ($ErrorFound!="")
		{
			$invalid_table .= "<tr>";
				$invalid_table .= "<td>".($i+1)."</td>";
				if ($appType == $eclassAppConfig['appType']['Parent'] || $appType == $eclassAppConfig['appType']['Student']) {
					$invalid_table .= "<td>".$csv_row_className."</td>";
					$invalid_table .= "<td>".$csv_row_classNumber."</td>";
				}
				else if ($appType == $eclassAppConfig['appType']['Teacher']) {
					$invalid_table .= "<td>".$csv_row_userLogin."</td>";
				}
				$invalid_table .= "<td>".$csv_row_messageTitle."</td>";
				$invalid_table .= "<td>".$csv_row_messageContentHtml."</td>";
				$invalid_table .= "<td><font color='red'>".$ErrorFound."</font></td>";
			$invalid_table .= "</tr>\n";
			
		} else
		{
			# valid data is inserted to database
			$_tmpParentAry = array();
			if ($appType == $eclassAppConfig['appType']['Parent'] || $appType == $eclassAppConfig['appType']['Student']) {
				$studentMsgAssoAry[$csv_row_className][$csv_row_classNumber][$csv_row_messageTitle][$csv_row_messageContent] = true;
//				$_userLogin = $AllStudentParents[$AllStudents[$csv_row_className][$csv_row_classNumber]]["UserLogin"];
//				$_parentId = $AllStudentParents[$AllStudents[$csv_row_className][$csv_row_classNumber]]["ParentID"];
//				$_tmpParentAry = $AllStudentParents[$AllStudents[$csv_row_className][$csv_row_classNumber]];
//				$_relatedUserId = $AllStudents[$csv_row_className][$csv_row_classNumber];
				
				if ($appType == $eclassAppConfig['appType']['Parent']) {
					$_tmpParentAry = $AllStudentParents[$AllStudents[$csv_row_className][$csv_row_classNumber]];
				}
				else {
					$_tmpParentAry[] = array("ParentID" => $AllStudents[$csv_row_className][$csv_row_classNumber]);
				}
				$_relatedUserId = $AllStudents[$csv_row_className][$csv_row_classNumber];
			}
			else if ($appType == $eclassAppConfig['appType']['Teacher']) {
				$teacherMsgAssoAry[$csv_row_userLogin][$csv_row_messageTitle][$csv_row_messageContent] = true;
				
//				$_userLogin = $csv_row_userLogin;
//				$_parentId = $teacherMappingAssoAry[$csv_row_userLogin];
				$_tmpParentAry[] = array("UserLogin" => $csv_row_userLogin, "ParentID" => $teacherMappingAssoAry[$csv_row_userLogin]);

				$_relatedUserId = $teacherMappingAssoAry[$csv_row_userLogin];
			}
			$validate_count++;
			
			$valid_table .= "<tr>";
				$valid_table .= "<td>".$validate_count."</td>";
				if ($appType == $eclassAppConfig['appType']['Parent'] || $appType == $eclassAppConfig['appType']['Student']) {
					$valid_table .= "<td>".$csv_row_className."</td>";
					$valid_table .= "<td>".$csv_row_classNumber."</td>";
				}
				else if ($appType == $eclassAppConfig['appType']['Teacher']) {
					$valid_table .= "<td>".$csv_row_userLogin."</td>";
				}
				$valid_table .= "<td>".$csv_row_messageTitle."</td>";
				$valid_table .= "<td>".$csv_row_messageContentHtml."</td>";
			$valid_table .= "</tr>\n";
			
			$numOfParent = count($_tmpParentAry);
			for ($j=0; $j<$numOfParent; $j++) {
				$__userLogin = $_tmpParentAry[$j]['UserLogin']; 
				$__parentId = $_tmpParentAry[$j]['ParentID'];
				
				$sql = "INSERT INTO TEMPSTORE_PUSHMESSAGE_FILE_USER (UserLogin, ParentID, MessageTitle, Message, UserID, RelatedUserID) " .
						"VALUES ('".$luser->Get_Safe_Sql_Query($__userLogin)."', " .
								"'".$luser->Get_Safe_Sql_Query($__parentId)."', " .
								"'".$luser->Get_Safe_Sql_Query($csv_row_messageTitle)."', " .
								"'".$luser->Get_Safe_Sql_Query($csv_row_messageContentSql)."', " .
								"'".$_SESSION['UserID']."', " .
								"'".$luser->Get_Safe_Sql_Query($_relatedUserId)."')";
				$li->db_db_query($sql);
			}
		}	
	}
	if ($invalid_table!="")
	{
//		$invalid_table = "<div class=\"table_board\"><span class=\"sectiontitle_v30\"> <font color='red'>{$i_SMS_Warning_Cannot_Send_To_Users}</font></span><br />"
//							. "<table class=\"common_table_list view_table_list\">"
//							. "<thead><tr><th width=5%>".$Lang['AppNotifyMessage']['CSV_ROW']."</th><th width=10%>".$Lang['SysMgr']['RoleManagement']['Class']."</th><th width=10%>".$Lang['SysMgr']['RoleManagement']['ClassNumber']."</th><th width=15%>".$Lang['AppNotifyMessage']['Title']."</th><th width=40%>".$Lang['AppNotifyMessage']['Description']."</th><th width=20%>$i_Attendance_Reason</th></tr></thead>"
//							. $invalid_table."</table>";
		$invalid_div = "";
		$invalid_div .= "<div class=\"table_board\"><span class=\"sectiontitle_v30\"> <font color='red'>{$i_SMS_Warning_Cannot_Send_To_Users}</font></span><br />";
			$invalid_div .= "<table class=\"common_table_list view_table_list\">";
				$invalid_div .= "<thead>";
					$invalid_div .= "<tr>";
						$invalid_div .= "<th width=5%>".$Lang['AppNotifyMessage']['CSV_ROW']."</th>";
						if ($appType == $eclassAppConfig['appType']['Parent'] || $appType == $eclassAppConfig['appType']['Student']) {
							$invalid_div .= "<th width=10%>".$Lang['SysMgr']['RoleManagement']['Class']."</th>";
							$invalid_div .= "<th width=10%>".$Lang['SysMgr']['RoleManagement']['ClassNumber']."</th>";
						}
						else if ($appType == $eclassAppConfig['appType']['Teacher']) {
							$invalid_div .= "<th width=20%>".$Lang['General']['UserLogin']."</th>";
						}
						$invalid_div .= "<th width=15%>".$Lang['AppNotifyMessage']['Title']."</th>";
						$invalid_div .= "<th width=40%>".$Lang['AppNotifyMessage']['Description']."</th>";
						$invalid_div .= "<th width=20%>$i_Attendance_Reason</th>";
					$invalid_div .= "</tr>";
				$invalid_div .= "</thead>";
				$invalid_div .= $invalid_table;
			$invalid_div .= "</table>";
		$invalid_div .= "</div>";
	}
	
	
	if ($valid_table!="")
	{
//		$valid_table = "<div class=\"table_board\"><span class=\"sectiontitle_v30\"> ".$Lang['AppNotifyMessage']['valid_table_title'] ."</span><br />"
//						. "<table class=\"common_table_list view_table_list\">"
//						. "<thead><tr><th width=5%>#</th><th width=10%>".$Lang['SysMgr']['RoleManagement']['Class']."</th><th width=10%>".$Lang['SysMgr']['RoleManagement']['ClassNumber']."</th><th width=15%>".$Lang['AppNotifyMessage']['Title']."</th><th width=60%>".$Lang['AppNotifyMessage']['Description']."</th></tr></thead>"
//						. $valid_table."</table>";
						
		$valid_div = "";
		$valid_div .= "<div class=\"table_board\"><span class=\"sectiontitle_v30\"> ".$validTableTitleLang ."</span><br />";
			$valid_div .= "<table class=\"common_table_list view_table_list\">";
				$valid_div .= "<thead>";
					$valid_div .= "<tr>";
						$valid_div .= "<th width=5%>#</th>";
						if ($appType == $eclassAppConfig['appType']['Parent'] || $appType == $eclassAppConfig['appType']['Student']) {
							$valid_div .= "<th width=10%>".$Lang['SysMgr']['RoleManagement']['Class']."</th>";
							$valid_div .= "<th width=10%>".$Lang['SysMgr']['RoleManagement']['ClassNumber']."</th>";
						}
						else if ($appType == $eclassAppConfig['appType']['Teacher']) {
							$valid_div .= "<th width=20%>".$Lang['General']['UserLogin']."</th>";
						}
						$valid_div .= "<th width=15%>".$Lang['AppNotifyMessage']['Title']."</th>";
						$valid_div .= "<th width=60%>".$Lang['AppNotifyMessage']['Description']."</th>";
					$valid_div .= "</tr>";
				$valid_div .= "</thead>";
			
				$valid_div .= $valid_table;
			$valid_div .= "</table>";
		$valid_div .= "</div>";
	}
	//debug_r($csv_data);
}

# Page title
//$CurrentPage = "PageParentNotification";

//$MODULE_OBJ = $limc->GET_MODULE_OBJ_ARR();


//$TAGS_OBJ[] = array($Lang['Header']['Menu']['ParentAppsNotify']);
$MODULE_OBJ = $limc->GET_MODULE_OBJ_ARR();

//$PAGE_NAVIGATION[] = array($Lang['Header']['Menu']['ParentAppsNotify'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['MassMailing']['New'], "new.php?appType=$appType");
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['ConfirmImportRecord']);


# Start layout
$linterface->LAYOUT_START($xmsg);


if ($plugin['eClassApp'] || $plugin['eClassTeacherApp']) {
	$updateScript = 'new_update_eClassApp.php';
}
else {
	$updateScript = 'new_update.php';
}
		
?>

<script language='javascript'>
		function sendSMS(){

			$('div#actionBtnDiv').hide();
			$('div#sendingSmsDiv').show();

			document.form1.submit();
		}
	</script>


<form name=form1 action='<?=$updateScript?>' method='post'>
		
		
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="navigation">
				<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr><td><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?></td></tr>
				</table>
			</td>
		</tr>
	</table>
	
	
	<table width="90%" border="0" cellspacing="5" cellpadding="5" align="center">	
	<tr>
		<td>
		<?=$invalid_div?>
		<br />
		<?=$valid_div?>
		</td>
	</tr>
	</table>
	
	
	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td>
				<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
					<tr>
						<td align="center">
							<div id="actionBtnDiv">
								<?php //if($invalid_table=="" && $valid_table!="") { 
										if($valid_table!="") {
								?>
									<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Send'], "button", "sendSMS()")?>&nbsp;
								<?php } ?>
								<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "self.location='new.php?appType=$appType'")?>
								<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "self.location='index.php?appType=$appType'")?>
							</div>
							<div id="sendingSmsDiv" class="tabletextrequire" style="display:none;">
								<?=$Lang['AppNotifyMessage']['sending']?>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<input type="hidden" name="MsgTitle" value="<?=str_replace("\"", '&quot;', stripslashes($MsgTitle))?>" />
	<input type="hidden" name="IsCSV" value="1" />
	<input type="hidden" name="appType" id="appType" value="<?=$appType?>" />
	<input type="hidden" name="sendTimeMode" id="sendTimeMode" value="<?=$sendTimeMode?>" />
	<input type="hidden" name="sendTimeString" id="sendTimeString" value="<?=$sendTimeString?>" />
</form>


<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
                    

?>