<?php
# using:

@set_time_limit(21600);
@ini_set('max_input_time', 21600);
@ini_set('memory_limit', -1);

### Change log:
#
#	Date:	2020-02-12  (Bill)  [2020-0212-1115-29235]
#			support images in jpeg format
#
#	Date:	2020-01-09  (Philips) [DM#3729]
#			added parentStatusCheck in getParentStudentMappingInfo()
#
#	Date:	2019-09-24 	(Philips) [2019-0917-1549-28207]
#			added attach image file checking
#
# 	Date:	2017-09-22	(Ivan) [ip.2.5.8.10.1]
# 			changed to send message by batches now
# 
#	Date:	2016-10-06 Villa[K104806]
#			Return the msg in the header with the form of english in stead of b5
#
#	Date:	2016-03-11	Ivan [ip.2.5.7.4.1]
#			added student app logic
#
#	Date:	2016-02-04	Kenneth
#			add liblog.php -> INSERT_LOG() to take log of the selected class/group/target
#
#	Date:	2015-07-23	(Roy)
#			add $receivePushMessageWhileLoggedOutStatus for receiving push message while logged out feature in teacher app
#			Deploy: ip.2.5.6.8.1.0
#
###

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
include_once($PATH_WRT_ROOT."includes/liblog.php");

intranet_auth();
intranet_opendb();

$messageTitle = standardizeFormPostValue($_POST['MsgTitle']);
$isCSV = standardizeFormPostValue($_POST['IsCSV']);
$messageContent = standardizeFormPostValue($_POST['Message']);
$isPublic = standardizeFormPostValue(cleanHtmlJavascript($_POST['IsPublic']));
$appType = standardizeFormPostValue($_POST['appType']);
$recipientAry = $_POST['Recipient'];
$sendTimeMode = standardizeFormPostValue(cleanCrossSiteScriptingCode($_POST['sendTimeMode']));
$sendTimeString = standardizeFormPostValue(cleanCrossSiteScriptingCode($_POST['sendTimeString']));
$selectedClassAry = standardizeFormPostValue(cleanCrossSiteScriptingCode($_POST['selectedClassAry']));
$selectedGroupAry = standardizeFormPostValue(cleanCrossSiteScriptingCode($_POST['selectedGroupAry']));
$selectedTargetAry = standardizeFormPostValue(cleanCrossSiteScriptingCode($_POST['selectedTargetAry']));

$messageTitle = str_replace(array('<', '>', 'javascript:', '"'),'',$messageTitle);
$messageContent =  str_replace(array('<', '>', 'javascript:', '"'),'',$messageContent);

$luser = new libuser();
$lmc = new libmessagecenter();
$libeClassApp = new libeClassApp();
$liblog = new liblog();

$lmc->checkNotifyMessageAccessRight($appType);

### Check image file format
include_once ($PATH_WRT_ROOT."includes/libfilesystem.php");
$lfs = new libfilesystem();
$filename = $_FILES["attachedImage"]["name"];
$tmpfilepath = $_FILES["attachedImage"]["tmp_name"];
$fileext = $lfs->file_ext($filename);
// [2020-0212-1115-29235]
// if($_FILES["attachedImage"]["name"] && !(strtolower($fileext) == ".jpg" || strtolower($fileext) == ".gif" || strtolower($fileext) == ".png")){
if($_FILES["attachedImage"]["name"] && !(strtolower($fileext) == ".jpg" || strtolower($fileext) == ".jpeg" || strtolower($fileext) == ".gif" || strtolower($fileext) == ".png")){
	header('Location: index.php?appType='.$appType.'&msg=imageFail');
	die();
}

### Consolidate message data into same array for different mode
$individualMessageInfoAry = array();
if ($isCSV)
{
	// Multiple message
	$messageContent = 'MULTIPLE MESSAGES';
	
	$pushMessageAry = $lmc->getPushMessageFromImportTempTable();
	$numOfMessage = count($pushMessageAry);
	for ($i=0; $i<$numOfMessage; $i++) {
		$_parentId = $pushMessageAry[$i]['ParentID'];
		$_messageTitle = $pushMessageAry[$i]['MessageTitle'];
		$_messageContent = $pushMessageAry[$i]['Message'];
		$_relatedToUserId = $pushMessageAry[$i]['RelatedUserID'];
		
		$_individualMessageInfoAry = array();
		$_individualMessageInfoAry['relatedUserIdAssoAry'][$_parentId] = array($_relatedToUserId);
		$_individualMessageInfoAry['messageTitle'] = $_messageTitle;
		$_individualMessageInfoAry['messageContent'] = $_messageContent;
		$individualMessageInfoAry[$i] = $_individualMessageInfoAry;
	}
}
else
{
	// Single message
	$parentStudentAssoAry = array();
	if ($appType == $eclassAppConfig['appType']['Parent']) {
		if ($isPublic == 'Y') {
			$recipientUserIdAry = '';
		}
		else {
			$recipientUserIdAry = Get_User_Array_From_Common_Choose($recipientAry, array(USERTYPE_STUDENT));
		}
		$parentStudentAssoAry = BuildMultiKeyAssoc($luser->getParentStudentMappingInfo($recipientUserIdAry, '', false, $parentStatusCheck=true), 'ParentID', $IncludedDBField=array('StudentID'), $SingleValue=1, $BuildNumericArray=1);
	}
	else if ($appType == $eclassAppConfig['appType']['Teacher']) {
		if ($isPublic == 'Y') {
			$eClassAppSettingsObj = $libeClassApp->getAppSettingsObj();
			$receivePushMessageWhileLoggedOut = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['TeacherApp']['canReceivePushMessageWhileLoggedOut']);
			$receivePushMessageWhileLoggedOutStatus = ($receivePushMessageWhileLoggedOut == 1) ? true : false;
			$teacherUserIdAry = $luser->getTeacherWithTeacherUsingTeacherApp(true, $receivePushMessageWhileLoggedOutStatus);
		}
		else {
			$teacherUserIdAry = Get_User_Array_From_Common_Choose($recipientAry, array(USERTYPE_STAFF));
		}
		
		$numOfTeacher = count($teacherUserIdAry);
		for ($i=0; $i<$numOfTeacher; $i++) {
			$_teacherId = $teacherUserIdAry[$i];
			
			$_targetTeacherId = $libeClassApp->getDemoSiteUserId($_teacherId);
			
			// link the message to be related to oneself
			$parentStudentAssoAry[$_teacherId] = array($_targetTeacherId);
		}
	}
	else if ($appType == $eclassAppConfig['appType']['Student']) {
		if ($isPublic == 'Y') {
			$studentUserIdAry = $luser->getStudentWithStudentUsingStudentApp(true);
		}
		else {
			$studentUserIdAry = Get_User_Array_From_Common_Choose($recipientAry, array(USERTYPE_STUDENT));
		}
		
		$numOfStudent = count($studentUserIdAry);
		for ($i=0; $i<$numOfStudent; $i++) {
			$_studentId = $studentUserIdAry[$i];
			$_targetStudentId = $libeClassApp->getDemoSiteUserId($_studentId);
			
			// link the message to be related to oneself
			$parentStudentAssoAry[$_studentId] = array($_targetStudentId);
		}
	}
	
	$individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $parentStudentAssoAry;
}

### Send message
$sendByBatches = $libeClassApp->isEnabledSendBulkPushMessageInBatches();
if ($sys_custom['eClassApp']['sendPushMessageImmediately'][$appType]) {
	$sendByBatches = false;
}
if ($sendByBatches) {
	$notifyMessageId = $libeClassApp->sendPushMessageByBatch($individualMessageInfoAry, $messageTitle, $messageContent, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', '', '', $_FILES['attachedImage']);
}
else {
	$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $messageTitle, $messageContent, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', '', '', false, $_FILES['attachedImage']);
}

// Villa [K104806]	2016-10-06 Avoid Display problem of B5 in the hyper link
$msg = ($notifyMessageId > 0)? 'success':'fail';
// $msg = ($notifyMessageId > 0)? $Lang['AppNotifyMessage']['ReturnMsg']['SendSuccess']:$Lang['AppNotifyMessage']['ReturnMsg']['SendFail'];

if($msg == 'success')
{
	$Module = 'Message Center';
	$Section = 'Push Notification';
	
	$RecordDetail = 'Message ID: '.$notifyMessageId.'; Target ID selected: '.$selectedTargetAry.'; Class selected: '.$selectedClassAry .'; Group ID selected: '.$selectedGroupAry ;
	$liblog->INSERT_LOG($Module, $Section, $RecordDetail, $TableName='', $RecodID='');
}
	
intranet_closedb();

// For performance tunning info
//echo '<script language="JavaScript" src="/templates/jquery/jquery-1.3.2.min.js"></script>';
//debug_pr('convert_size(memory_get_usage(true)) = '.convert_size(memory_get_usage(true)));
//debug_pr('Query Count = '.$GLOBALS[debug][db_query_count]);
//$libeClassApp->db_show_debug_log();
//$libeClassApp->db_show_debug_log_by_query_number(1);
//die();

header("Location: ./index.php?appType=$appType&msg=$msg");
?>