<?php
/*
 * Change Log:
 * Date 2020-07-28 Ray add display returnMessage
 * Date 2016-10-27 Villa [E107437] fix  Wrong header when export in message center
 * Date 2016-10-12	Villa [L105447]
 * Open the file - export csv
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
	

intranet_auth();
intranet_opendb();

# Create a new interface instance
$limc = new libmessagecenter();
$leClassApp = new libeClassApp();


$appType = ($_POST['appType'])? $_POST['appType'] : $appType;
$messageStatus = isset($_POST['messageStatus'])? $_POST['messageStatus'] : -1;
$returnMsgKey = ($_POST['returnMsgKey'])? $_POST['returnMsgKey'] : $_GET['returnMsgKey'];


$limc->checkNotifyMessageAccessRight($appType);

$NotifyMessageID = ($_POST['targetNotifyMessageID'])? $_POST['targetNotifyMessageID'] : $NotifyMessageID;


if($NotifyMessageID == ''){
	intranet_closedb();
	header("Location: index.php?filter=$filter&order=$order&field=$field&appType=$appType");
	exit;
}


$li = new libdb();

$name_field = getNameFieldByLang("iu.");
$sql = "SELECT
DATE_FORMAT(m.NotifyDateTime, '%Y-%m-%d %H:%i:%s') as NotifyDateTime,m.MessageTitle,m.MessageContent,m.NotifyFor,
IF(m.NotifyUserID is not null AND m.NotifyUserID > 0, $name_field, m.NotifyUser) as NotifyUser,
m.IsPublic,t.TargetID as TargetID
FROM INTRANET_APP_NOTIFY_MESSAGE as m
LEFT JOIN INTRANET_APP_NOTIFY_MESSAGE_TARGET as t ON m.NotifyMessageID = t.NotifyMessageID
LEFT JOIN INTRANET_USER as iu ON (m.NotifyUserID = iu.UserID)
WHERE m.NotifyMessageID = '$NotifyMessageID' ";
$temp_result = $li->returnArray($sql);
$NotifyMessage = $temp_result[0];


if ($NotifyMessage['MessageContent']=="MULTIPLE MESSAGES")
{
	# multiple messages case
	$sql = "SELECT * FROM INTRANET_APP_NOTIFY_MESSAGE_TARGET WHERE NotifyMessageID = '$NotifyMessageID' ORDER BY TargetID";
	$log_rows = $li->returnResultSet($sql);
	for ($i=0; $i<sizeof($log_rows); $i++)
	{
		$log_obj = $log_rows[$i];
		$MessageLogByParent[$log_obj["TargetID"]] =  array("MessageTitle"=>$log_obj["MessageTitle"], "MessageContent"=>$log_obj["MessageContent"]);
		$MessageLogByMessageTargetID[$log_obj["NotifyMessageTargetID"]] =  array("MessageTitle"=>$log_obj["MessageTitle"], "MessageContent"=>$log_obj["MessageContent"]);
	}
}

$title[]='#';
if ($appType == $eclassAppConfig['appType']['Parent']) {
	$title[]= $Lang['General']['Parent'];
	$title[]= $Lang['General']['Children'];

}
else if ($appType == $eclassAppConfig['appType']['Teacher']) {
	if ($sys_custom['DHL']) {
		$title[] = $Lang['DHL']['Company'];
		$title[] = $Lang['DHL']['Division'];
		$title[] = $Lang['DHL']['Department'];
	}
	$title[] = $i_identity_teachstaff;
}
else if ($appType == $eclassAppConfig['appType']['Student']) {
	$title[] = $Lang['General']['Class'];
	$title[] = $Lang['General']['ClassNumber'];
	$title[] = $Lang['Identity']['Student'];
}
$title[] = $Lang['General']['Status'];
if (is_array($MessageLogByMessageTargetID) && count($MessageLogByMessageTargetID)>0) {
	$title[] = $Lang['AppNotifyMessage']['Description'];
}

$title[] = $Lang['AppNotifyMessage']['AppLastLoginTime'];
$exportContentAry[] = $title;

foreach ($temp_result as $result_row) {
	$target_ids[] = $result_row['TargetID'];
}
$target_ids = implode(',', $target_ids);
// $target_ids = $NotifyMessage['TargetIDS'];


$messageTargetAry = $limc->getPushMessageTargetInfo($NotifyMessageID, $orderByName, $orderByClassNameClassNumber);
$messageRelatedToAry = $limc->getPushMessageRelatedToInfo($NotifyMessageID);
$messageRelatedToAssoAry = BuildMultiKeyAssoc($messageRelatedToAry, array('NotifyMessageTargetID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
$messageReferenceAssoAry = BuildMultiKeyAssoc($limc->getPushMessageReferenceInfo($NotifyMessageID), array('NotifyMessageTargetID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);

if ($target_ids!=NULL)
{
	

	if (count($messageRelatedToAssoAry) > 0) {
		### new logic: record the push message related to student in DB

		// status selection
		$statusSelection = $limc->getPushMessageStatusSelection('messageStatusSel', 'messageStatus', $messageStatus, "changedStatusSelection(this.value);", $NotifyMessageID);

		// pre-load user data
		$allUserIdAry = array_values(array_unique(array_merge(Get_Array_By_Key($messageTargetAry, 'TargetID'), Get_Array_By_Key($messageRelatedToAry, 'UserID'))));
		$objUser = new libuser('', '', $allUserIdAry);
		
		if ($sys_custom['DHL']) {
			include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
			$ldhl = new libdhl();
			
			$filterMap = array();
			$filterMap['user_id'] = $allUserIdAry;
			$filterMap['GetOrganizationInfo'] = 1;
			$filterMap['GetOrganizationField'] = 1;
			$userCompanyAssoAry = BuildMultiKeyAssoc($ldhl->getUserRecords($filterMap), 'UserID');
		}

		// get user last app login info
		$appLoginAssoAry = BuildMultiKeyAssoc($leClassApp->getUserLoginAppInfo($allUserIdAry), 'UserID', 'LastLoginTime');

		$numOfTarget = count($messageTargetAry);
		$numOfDisplayRow = 0;
		for ($i=0; $i<$numOfTarget; $i++) {
			$_messageTargetId = $messageTargetAry[$i]['NotifyMessageTargetID'];
			$_targetUserId = $messageTargetAry[$i]['TargetID'];

			// get target user info
			$objUser->LoadUserData($_targetUserId);
			$_targetUserName = Get_Lang_Selection($objUser->ChineseName, $objUser->EnglishName);
			if ($appType == $eclassAppConfig['appType']['Student']) {
				$_classNameDisplay = ($objUser->ClassName)? $objUser->ClassName : $Lang['General']['EmptySymbol'];
				$_classNumberDisplay = ($objUser->ClassNumber)? $objUser->ClassNumber : $Lang['General']['EmptySymbol'];
			}
			
			// get related user info
			$_relatedToAry = $messageRelatedToAssoAry[$_messageTargetId];
			$_numOfRelatedUser = count((array)$_relatedToAry);
			$_relatedUserDisplayAry = array();
			for ($j=0; $j<$_numOfRelatedUser; $j++) {
				$__relatedUserId = $_relatedToAry[$j]['UserID'];
					
				$objUser->LoadUserData($__relatedUserId);
				$__relatedUserDisplay = '';
				$__relatedUserDisplay .= Get_Lang_Selection($objUser->ChineseName, $objUser->EnglishName);
				if ($objUser->ClassName != '' && $objUser->ClassNumber != '') {
					$__relatedUserDisplay .= ' ('.$objUser->ClassName.' - '.$objUser->ClassNumber.')';
				}
				$_relatedUserDisplayAry[] = $__relatedUserDisplay;
			}
			$_relatedUserDisplay = implode('<br>', $_relatedUserDisplayAry);

			// get message status
			$_messageReferenceAry = $messageReferenceAssoAry[$_messageTargetId];
			//if (is_array($_messageReferenceAry)) {
			$_messageReferenceAry = $messageReferenceAssoAry[$_messageTargetId];
			$_messageStatusAry = Get_Array_By_Key($_messageReferenceAry, 'MessageStatus');
			$_messageErrorCodeAry = Get_Array_By_Key($_messageReferenceAry, 'ErrorCode');
			$_messageReturnMessageAry = Get_Array_By_Key($_messageReferenceAry, 'ReturnMessage');
			$_messageReturnMessageAry = array_unique($_messageReturnMessageAry);
			$_messageReturnMessageAry = array_filter($_messageReturnMessageAry, 'strlen');

			$_messageStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendSuccess'];
			$_filterStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendSuccess'];
			if (in_array($eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['userHasRead'], (array)$_messageStatusAry)) {
				$_messageStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['userHasRead'];
				$_filterStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['userHasRead'];
			}
			else if (in_array($eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendSuccess'], (array)$_messageStatusAry)) {
				// do nth
			}
			else if (in_array($eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['waitingToSend'], (array)$_messageStatusAry)) {
				$_messageStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['waitingToSend'];
				$_filterStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['waitingToSend'];
			}
			else {
				$_isAllFailed = true;
				$_numOfMessage = count($_messageStatusAry);
				for ($j=0; $j<$_numOfMessage; $j++) {
					$__status = $_messageStatusAry[$j];
						
					if ($__status != $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendFailed']) {
						$_isAllFailed = false;
						break;
					}
				}

				if ($_isAllFailed) {
					if (in_array($eclassAppConfig['errorCode']['pushMessage']['cannotConnectToServiceProvider'], $_messageErrorCodeAry) || in_array($eclassAppConfig['errorCode']['pushMessage']['cannotConnectToCentralServer'], $_messageErrorCodeAry)) {
						$_messageStatus = $eclassAppConfig['errorCode']['pushMessage']['cannotConnectToServiceProvider'];
					}
					else {
						$_messageStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendFailed'];
					}
					$_filterStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['sendFailed'];
				}
				else if (in_array($eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['noRegisteredDevice'], (array)$_messageStatusAry)) {
					$_messageStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['noRegisteredDevice'];
					$_filterStatus = $eclassAppConfig['INTRANET_APP_NOTIFY_MESSAGE_REFERENCE']['MessageStatus']['noRegisteredDevice'];
				}
			}
				
			if ($messageStatus != -1 && $_filterStatus != $messageStatus) {
				continue;
			}
			$_messageStatusDisplay = $limc->getPushMessageStatusDisplay($_messageStatus);
			if(count($_messageReturnMessageAry) > 0) {
				//$_messageStatusDisplay .= ' ('.implode(' ', $_messageReturnMessageAry).')';
			}

			//}

			// app last login time
			$_appLastLoginTime = $appLoginAssoAry[$_targetUserId]['LastLoginTime'];
			$_appLastLoginTime = ($_appLastLoginTime)? $_appLastLoginTime : $Lang['General']['EmptySymbol'];


// 			$recipient_html .= '<tr>'."\n";
// 			$recipient_html .= '<td>'.($numOfDisplayRow + 1).'</td>'."\n";
			$content[] = ($numOfDisplayRow + 1);
			if ($appType == $eclassAppConfig['appType']['Student']) {
// 				$recipient_html .= '<td>'.$_classNameDisplay.'</td>'."\n";
// 				$recipient_html .= '<td>'.$_classNumberDisplay.'</td>'."\n";
				$content[] = $_classNameDisplay;
				$content[] = $_classNumberDisplay;
			}
// 			$recipient_html .= '<td>'.$_targetUserName.'</td>'."\n";
			if ($sys_custom['DHL']) {
				$_company = $userCompanyAssoAry[$_targetUserId]['CompanyCode'];
				$_division = $userCompanyAssoAry[$_targetUserId]['DivisionCode'];
				$_department = $userCompanyAssoAry[$_targetUserId]['DepartmentCode'];
				
				$content[] = $_company;
				$content[] = $_division;
				$content[] = $_department;
			}
			$content[]=$_targetUserName;
			if ($appType == $eclassAppConfig['appType']['Parent']) {
// 				$recipient_html .= '<td>'.$_relatedUserDisplay.'</td>'."\n";
				$_relatedUserDisplay = str_replace('<br>', ',', $_relatedUserDisplay);
				$content[] = $_relatedUserDisplay;
			}
			//if (is_array($_messageReferenceAry)) {
// 			$recipient_html .= '<td>'.$_messageStatusDisplay.'</td>'."\n";
			$content[] = $_messageStatusDisplay;
			//}
				
			if (isset($MessageLogByMessageTargetID[$_messageTargetId])) {
				$_messageTitle = $MessageLogByMessageTargetID[$_messageTargetId]['MessageTitle'];
				$_messageContent = $MessageLogByMessageTargetID[$_messageTargetId]['MessageContent'];
// 				$recipient_html .= '<td><b>'.$_messageTitle.'</b><br />'.nl2br($_messageContent).'</td>'."\n";
				$msg = str_replace('<br />','', $_messageTitle.nl2br($_messageContent));
				$content[] = htmlspecialchars($msg);
				
			}
				
			$content[] = $_appLastLoginTime;
			$exportContentAry[] = $content;

			$numOfDisplayRow++;
			$content='';
		}
			
		if ($numOfDisplayRow == 0) {
			$content[] = $Lang['General']['NoRecordAtThisMoment'];
		}
	}
}

$filename = "export.csv";


include_once($PATH_WRT_ROOT."includes/libexporttext.php");

$lexport = new libexporttext();
$export_content = $lexport->GET_EXPORT_TXT($exportContentAry, array());
intranet_closedb();

// Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);

?>