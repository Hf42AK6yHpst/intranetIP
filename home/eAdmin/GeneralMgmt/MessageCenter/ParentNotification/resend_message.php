<?php
# using: 
/*
 * 
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");

intranet_auth();
intranet_opendb();


$NotifyMessageID = IntegerSafe(standardizeFormPostValue($_POST['NotifyMessageID']));
$appType = standardizeFormPostValue($_POST['appType']);
$messageTargetIdAry = $_POST['messageTargetIdAry'];


$lmc = new libmessagecenter();
$libeClassApp = new libeClassApp();

$lmc->checkNotifyMessageAccessRight($appType);

$success = $libeClassApp->resendPushMessage($NotifyMessageID, $messageTargetIdAry);

intranet_closedb();

if ($success) {
	$returnMsgKey = 'SendSuccess';
}
else {
	$returnMsgKey = 'SendFail';
}
header("Location: ./view_detail.php?appType=$appType&NotifyMessageID=$NotifyMessageID&returnMsgKey=$returnMsgKey");
?>