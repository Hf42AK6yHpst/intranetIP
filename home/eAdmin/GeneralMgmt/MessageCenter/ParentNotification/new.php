<?php
# using: 

#################################
#	Date:	2019-12-17 Philips [2018-1010-1341-08235]
#			Added filterECAActivity to commonChoose of Apptype = S
#
#	Date:	2019-11-14 Tiffany [Z174638]
#			change back for fix the problem by use default json
#
#	Date:	2019-11-04 Tiffany [Z174638]
#			change the push message limit to 150 for iphone can't get if the message is too long in php 5.4 site
#
#	Date:	2019-10-22 Philips [2019-1018-1442-51073]
#			modify isPublic display condition from isAdmin || !DHL into isAdmin && !DHL
#
#	Date:	2019-09-24 Philips [2019-0917-1549-28207]
#			added reminder for attach image
#			hide and disable attach image row when using CSV
#
#   Date:   2019-09-05 Tiffany
#           set send pushmessage schedule days to 90 by setting $sys_custom['eClassApp']['sendPushmessageWithin90Days']
#
#   Date:   2019-05-24 Ronald
#           uncomment image field for upload iamge
#
#   Date:   2019-02-28 Ronald
#           comment out image field
#
#   Date:   2018-12-18 Anna
#           $sys_custom['DHL'] - added SelectGroupOnly for common choose 
#
#	Date:	2017-04-27  Carlos
#			$sys_custom['DHL'] Use thickbox for choosing target for DHL. (Cancelled on 2017-08-03)
#
#	Date:	2017-02-13	Roy [ip.2.5.8.3.1]
#			add includeNoPushMessageUser=1 to common choose for teacher & student app, allow send message to user who is not registered 
#
#	Date:	2016-10-07	Omas [ip.2.5.7.10.1]
#			fix $recipientRemarks for student app 
#
#	Date:	2016-07-06	Ivan [ip.2.5.7.7.1]
#			added logic for $sys_custom['MessageCenter']['OnlySendByUI'] and $sys_custom['MessageCenter']['OnlySendToPublic']
#
#	Date:	2016-06-07	Ivan [M93934] [ip.2.5.7.7.1]
#			changed $sys_custom['MessageCenter']['DefaultSendToSpecificUser'] cust logic to general deploy
#
#	Date:	2016-06-03	Ivan [M93934] [ip.2.5.7.7.1]
#			added $sys_custom['MessageCenter']['DefaultSendToSpecificUser'] cust logic to default select send message to specific users
#
#	Date:	2016-05-18	Ivan [L95978] [ip.2.5.7.7.1]
#			allow common choose to show user without push message registered
#
#	Date:	2016-03-11	Ivan [ip.2.5.7.4.1]
#			added student app logic
#
#	Date:	2016-02-04	Kenneth
#			Add getSeclectedTargetLogInfo() / getSeclectedGroupLogInfo() / getSeclectedClassLogInfo() and param 'logToDB=1' to log selected target/class/group into db 'MODULE_RECORD_DELETE_LOG'
#
#	Date:	2016-01-26	Kenneth
#			Add number of user are selected besides seletion box, add js: commonChooseFollowUpAction()
#
#	Date:	2015-07-27	Omas
#			Add Message template, updated UI style
#	Date:	2015-06-05	Omas
#			Add Export Mobile Button
#	Date:	2015-05-11 Ivan [DM#2868] [ip.2.5.6.5.1]
#			fixed non-admin user can send message without selecting recipient
#
#	Date:	2014-12-10 Roy
#			use 'allowSendPushMessage_classTeacher' and 'allowSendPushMessage_clubAndActivityPIC' session variable to control common choose parameter
#
#	Date:	2014-10-24 Roy
#			allow class teacher / club pic / activity pic to send push message
#
#	Date:	2014-03-14 Ivan
#			disable checking of flag $sys_custom["ThisSiteIsForDemo"]
#
# 	Date:	2011-12-28  Carlos
#			added paremeter &filterAppParent=1 to common_choose/index.php
#
#	Date:	2011-06-23	Yuen
#			first version
#################################

$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

# iMail Plus enabled, assigned user only
# to be changed
//if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ParentAppNotify"])
//{
//	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
//	$laccessright = new libaccessright();
//	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
//	exit;
//}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_template.php");

intranet_auth();
intranet_opendb();

# Create a new interface instance
$linterface = new interface_html();
$limc = new libmessagecenter();
$libAppTemplate = new libeClassApp_template();

$Module = 'MessageCenter';
# Init libeClassApp_template
$libAppTemplate->setModule($Module);
//$libAppTemplate->setSection($Section);

$appType = ($_POST['appType'])? $_POST['appType'] : $_GET['appType'];
$limc->checkNotifyMessageAccessRight($appType);

# Page title
if ($appType == $eclassAppConfig['appType']['Parent']) {
	$CurrentPage = "PageParentNotification";
	$TAGS_OBJ[] = array($Lang['Header']['Menu']['ParentAppsNotify']);
	$PAGE_NAVIGATION[] = array($Lang['Header']['Menu']['ParentAppsNotify'], 'index.php?appType='.$eclassAppConfig['appType']['Parent']);
	$recipientRemarks = $Lang['AppNotifyMessage']['UsingParentAppParentOnly'];
	$publicLang = $Lang['AppNotifyMessage']['Public'];
	$nonPublicLang = $Lang['AppNotifyMessage']['NonPublic'];
	$sampleCsv = 'push_message_sample.csv';
	
	$commonChooseLink = '../../../../common_choose/index.php?fieldname=Recipient[]&page_title=SelectRecipients&permitted_type=2&excluded_type=4&filterAppStudent=1&DisplayGroupCategory=1&Disable_AddGroup_Button=1&filterECAActivity=1&custFollowUpJs=1&logToDB=1&includeNoPushMessageUser=1';
	
	$isAdminUser = false;
	if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ParentAppNotify"]) {
		$isAdminUser = true;
	}
	else {
		if ($_SESSION["SSV_PRIVILEGE"]["MessageCenter"]["allowSendPushMessage_classTeacher"]) {
			$commonChooseLink .= '&filterClassTeacherClass=1';
		}
		if ($_SESSION["SSV_PRIVILEGE"]["MessageCenter"]["allowSendPushMessage_clubAndActivityPIC"]) {
			$commonChooseLink .= '&filterClubAndEventInCharge=1';
		}
	}

	if (!$sys_custom['DHL']) {
		$exportMobileBtn = $linterface->GET_BTN($Lang['MessageCenter']['ExportMobile'], "button", "goExportMobile()");
	}
	$display_Remarks = $Lang['MessageCenter']['ExportMobileRemarks'];
}
else if ($appType == $eclassAppConfig['appType']['Teacher']) {
	$CurrentPage = "PageTeacherNotification";
	$TAGS_OBJ[] = array($Lang['Header']['Menu']['TeacherAppsNotify']);
	$PAGE_NAVIGATION[] = array($Lang['Header']['Menu']['TeacherAppsNotify'], 'index.php?appType='.$eclassAppConfig['appType']['Teacher']);
	$recipientRemarks = $Lang['AppNotifyMessage']['UsingTeacherAppTeacherOnly'];
	$publicLang = $Lang['AppNotifyMessage']['Public(Teacher)'];
	$nonPublicLang = $Lang['AppNotifyMessage']['NonPublic(Teacher)'];
	$sampleCsv = 'push_message_teacher_sample.csv';
	
	$commonChooseLink = '../../../../common_choose/index.php?fieldname=Recipient[]&page_title=SelectRecipients&permitted_type=1&excluded_type=4&filterAppTeacher=1&DisplayGroupCategory=1&Disable_AddGroup_Button=1&custFollowUpJs=1&logToDB=1&includeNoPushMessageUser=1';
	
	$isAdminUser = false;
	if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-TeacherAppNotify"]) {
		$isAdminUser = true;
	}
	
	if($sys_custom['DHL']){
	    $commonChooseLink .='&SelectGroupOnly=1&DHLSelectGroup=1&isAdmin='.$isAdminUser;
	}
	
	if (!$sys_custom['DHL']) {
		$exportMobileBtn = $linterface->GET_BTN($Lang['MessageCenter']['ExportMobile'], "button", "goExportMobile('teacher')");
	}
	$display_Remarks = $Lang['MessageCenter']['ExportMobileRemarks_T'];
}
else if ($appType == $eclassAppConfig['appType']['Student']) {
	$CurrentPage = "PageStudentNotification";
	$TAGS_OBJ[] = array($Lang['Header']['Menu']['StudentAppsNotify']);
	$PAGE_NAVIGATION[] = array($Lang['Header']['Menu']['StudentAppsNotify'], 'index.php?appType='.$eclassAppConfig['appType']['Student']);
// 	$recipientRemarks = $Lang['AppNotifyMessage']['UsingStudentAppTeacherOnly'];
	$recipientRemarks = $Lang['AppNotifyMessage']['UsingStudentAppStudentOnly'];
	$publicLang = $Lang['AppNotifyMessage']['Public(Student)'];
	$nonPublicLang = $Lang['AppNotifyMessage']['NonPublic(Student)'];
	$sampleCsv = 'push_message_sample.csv';
	
	$commonChooseLink = '../../../../common_choose/index.php?fieldname=Recipient[]&page_title=SelectRecipients&permitted_type=2&excluded_type=4&filterAppStudentByStudentApp=1&DisplayGroupCategory=1&Disable_AddGroup_Button=1&custFollowUpJs=1&logToDB=1&includeNoPushMessageUser=1';
	# 20191217 Philips - Add ECA option
	$commonChooseLink .= "&filterECAActivity=1";
	
	$isAdminUser = false;
	if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAppNotify"]) {
		$isAdminUser = true;
	}
}
if($sys_custom['PowerClass']){
	$TAGS_OBJ = array();
	$curTab = $appType;
	$TAGS_OBJ[] = array($Lang['Header']['Menu']['ParentAppsNotify'],'new.php?appType='.$eclassAppConfig['appType']['Parent'],$curTab==$eclassAppConfig['appType']['Parent']);
	$TAGS_OBJ[] = array($Lang['Header']['Menu']['StudentAppsNotify'],'new.php?appType='.$eclassAppConfig['appType']['Student'],$curTab==$eclassAppConfig['appType']['Student']);
	$TAGS_OBJ[] = array($Lang['Header']['Menu']['TeacherAppsNotify'],'new.php?appType='.$eclassAppConfig['appType']['Teacher'],$curTab==$eclassAppConfig['appType']['Teacher']);
}
$MODULE_OBJ = $limc->GET_MODULE_OBJ_ARR();

$PAGE_NAVIGATION[] = (isset($MassMailingID) && $MassMailingID>0) ? array($Lang['MassMailing']['Edit']) : array($Lang['MassMailing']['New']);

	
# Start layout
$linterface->LAYOUT_START($msg);

if ($plugin['eClassApp'] || $plugin['eClassTeacherApp']) {
	$updateScript = 'new_update_eClassApp.php';
}
else {
	$updateScript = 'new_update.php';
}

$htmlAry['sendTimeNowRadio'] = $linterface->Get_Radio_Button('sendTimeRadio_now', 'sendTimeMode', $eclassAppConfig['pushMessageSendMode']['now'], $isChecked=true, $Class="", $Lang['AppNotifyMessage']['Now'], "clickedSendTimeRadio(this.value);");
$htmlAry['sendTimeScheduledRadio'] = $linterface->Get_Radio_Button('sendTimeRadio_scheduled', 'sendTimeMode', $eclassAppConfig['pushMessageSendMode']['scheduled'], $isChecked=0, $Class="", $Lang['AppNotifyMessage']['SpecificTime'], "clickedSendTimeRadio(this.value);");


// reference: http://stackoverflow.com/questions/2480637/round-minute-down-to-nearest-quarter-hour
$rounded_seconds = ceil(time() / (15 * 60)) * (15 * 60);
$nextTimeslotHour = date('H', $rounded_seconds);
$nextTimeslotMinute = date('i', $rounded_seconds);

$x = '';
$x .= $linterface->GET_DATE_PICKER('sendTime_date');
$x .= $linterface->Get_Time_Selection_Box('sendTime_hour', 'hour', $nextTimeslotHour);
$x .= " : ";
$x .= $linterface->Get_Time_Selection_Box('sendTime_minute', 'min', $nextTimeslotMinute, $others_tab='', $interval=15);
$htmlAry['sendTimeDateTimeDisplay'] = $x;

//if ($sys_custom['MessageCenter']['DefaultSendToSpecificUser']) {
	$isPublicY_checked = '';
	$isPublicN_checked = ' checked="checked" ';
//}
//else {
//	$isPublicY_checked = ' checked="checked" ';
//	$isPublicN_checked = '';
//}


?>
<? include_once($PATH_WRT_ROOT."home/eClassApp/common/pushMessageTemplate/js_AppMessageReminder.php") ?>
<?=$linterface->Include_JS_CSS()?>
<script language="javascript">
$(document).ready( function() {
	AppMessageReminder.initInsertAtTextarea();
	AppMessageReminder.targetMessageTitleID = 'MsgTitle';
	AppMessageReminder.targetMessageContentID = 'Message';
	AppMessageReminder.MsgModule = '<?=$Module?>';
	
	<?php
	//if ($isAdminUser && $sys_custom['MessageCenter']['DefaultSendToSpecificUser']) {
	if ($isAdminUser) {
	?>
	$('input#IsPublicN').click();
	<?php } ?>
});

function goExportMobile(type){
	type = type || '';
	
	if(type=='teacher'){
		window.location = '../export_mobile.php?teacher=1';
	}
	else{
		window.location = '../export_mobile.php?StudentIDArr=all';
	}
}

function checkform(obj)
{
	
//    <?php if ($sys_custom["ThisSiteIsForDemo"]) {?>
//	alert("Sorry, this action is not allowed in trial site!");
//	return false;
//	<?php } ?>
		
    if (!check_text(obj.MsgTitle,'<?=$Lang['AppNotifyMessage']['WarningMsg']['RequestInputTitle']?>')) return false;
    
    var scheduleString = '';
    if ($('input#sendTimeRadio_scheduled').attr('checked')) {
    	scheduleString = $('input#sendTime_date').val() + ' ' + str_pad($('select#sendTime_hour').val(),2) + ':' + str_pad($('select#sendTime_minute').val(),2) + ':00';
    }
    $('input#sendTimeString').val(scheduleString);
    
    var minuteValid = false;
	if (str_pad($('select#sendTime_minute').val(),2) == '00' || str_pad($('select#sendTime_minute').val(),2) == '15' || str_pad($('select#sendTime_minute').val(),2) == '30' || str_pad($('select#sendTime_minute').val(),2) == '45') {
		minuteValid = true;
	}	
    
    if ($('input[name=IsCSV]:checked').val()=="1")
    {
    	obj.action = "new_csv_confirm.php";
    } else
    {
	    if (!check_text(obj.Message,'<?=$Lang['AppNotifyMessage']['WarningMsg']['RequestInputDescription']?>')) return false;
	    
	    var recipients = obj.elements["Recipient[]"];
	    var recipient_cnt = 0;
//	    if (<?=(($isAdminUser) ? 'true' : 'false')?>) {
//		    if (obj.IsPublic[1].checked)
//		    {
//			    for(i=0;i<recipients.length;++i)
//			    {
//			    	if(recipients.options[i].value == '' || recipients.options[i].value == 0) continue;
//			    	recipient_cnt++;
//			    }
//			    if(recipient_cnt==0)
//			    {
//					alert("<?=$Lang['AppNotifyMessage']['WarningMsg']['RequestSelectRecipients']?>");
//					return false; 
//				}
//			}
//	    }
		
		var needCheckRecipient = true;
		if (<?=(($isAdminUser) ? 'true' : 'false')?> && obj.IsPublic[0].checked) {
			needCheckRecipient = false;
		}
		if (needCheckRecipient) {
			for(i=0;i<recipients.length;++i) {
		    	if(recipients.options[i].value == '' || recipients.options[i].value == 0) continue;
		    	recipient_cnt++;
		    }
		    if(recipient_cnt==0) {
				alert("<?=$Lang['AppNotifyMessage']['WarningMsg']['RequestSelectRecipients']?>");
				return false; 
			}
		}

		
		// if scheduled message => check time is past or not
		var dateNow = new Date();
		var dateNowString = dateNow.getFullYear() + '-' + str_pad((dateNow.getMonth()+1),2) + '-' + str_pad(dateNow.getDate(),2) + ' ' + str_pad(dateNow.getHours(),2) + ':' + str_pad(dateNow.getMinutes(),2) + ':' + str_pad(dateNow.getSeconds(),2);
		if ($('input#sendTimeRadio_scheduled').attr('checked') && minuteValid && scheduleString <= dateNowString) {
			alert('<?=$Lang['AppNotifyMessage']['WarningMsg']['SendTimeInvalid']?>');
			return false;
		}

        <?php if ($sys_custom['eClassApp']['sendPushmessageWithin90Days']) {?>

            // check time is within 90 days
            var maxDate = new Date();
            maxDate.setDate(dateNow.getDate() + 91);
            var maxDateString = maxDate.getFullYear() + '-' + str_pad((maxDate.getMonth()+1),2) + '-' + str_pad(maxDate.getDate(),2) + ' ' + str_pad(maxDate.getHours(),2) + ':' + str_pad(maxDate.getMinutes(),2) + ':' + str_pad(maxDate.getSeconds(),2);
            if ($('input#sendTimeRadio_scheduled').attr('checked') && minuteValid && scheduleString > maxDateString) {
                alert('<?=$Lang['AppNotifyMessage']['WarningMsg']['PleaseSelectDateWithinRange90']?>');
                $('input#sendTime_date').focus();
                return false;
            }
        <?php }else{ ?>
            // check time is within 30 days
            var maxDate = new Date();
            maxDate.setDate(dateNow.getDate() + 31);
            var maxDateString = maxDate.getFullYear() + '-' + str_pad((maxDate.getMonth()+1),2) + '-' + str_pad(maxDate.getDate(),2) + ' ' + str_pad(maxDate.getHours(),2) + ':' + str_pad(maxDate.getMinutes(),2) + ':' + str_pad(maxDate.getSeconds(),2);
            if ($('input#sendTimeRadio_scheduled').attr('checked') && minuteValid && scheduleString > maxDateString) {
                alert('<?=$Lang['AppNotifyMessage']['WarningMsg']['PleaseSelectDateWithinRange']?>');
                $('input#sendTime_date').focus();
                return false;
            }
        <?php } ?>

	    checkOptionAll(obj.elements["Recipient[]"]);
	    
	    $('div#buttonDiv').hide();
	    $('div#sendingDiv').show();
	}
}


function ShowHideUserSelection()
{
	
	if ($('input[name=IsCSV]:checked').val()=="1")
	{
		$('#'+'RecipientRow').hide();
		$('#Recipients').attr('rowspan',1);
		$('#'+'MessageRow').hide();
		$('#'+'PublicRow').hide();
		$('#'+'CSVUploadRow').show();
	} else
	{
	
		$('#'+'CSVUploadRow').hide();
		$('#'+'MessageRow').show();
		$('#'+'PublicRow').show();
		if ($('input[name=IsPublic]:checked').val()=="Y")
		{
			$('#'+'RecipientRow').hide();
			$('#Recipients').attr('rowspan',1);
		} else
		{
			$('#'+'RecipientRow').show();
			$('#Recipients').attr('rowspan',2);
		}
	}
}

function clickedSendTimeRadio(targetType) {
	if (targetType == 'now') {
		$('div#specificSendTimeDiv').hide();
	}
	else {
		$('div#specificSendTimeDiv').show();
	}
}

function goBack() {
	window.location = "index.php?appType=<?=$appType?>";
}

function commonChooseFollowUpAction(){
	
	checkOptionAll(document.getElementById('form1').elements["Recipient[]"]);
	
	var n = $('#userSelector option').size();
	var lastChild = $('#userSelector option:last-child');
	if(lastChild.val()==''){
		n=n-1;
	}
	
	$('#numOfUser').html(n);
	$('#outerspan').show();
}
function getSeclectedClassLogInfo(className){
	var existingValueStr = $('#selectedClassAry').val();
	var newValuesStr = '';
	if(className==''){
		return;
	}
	if(existingValueStr==''||!existingValueStr||typeof(className)=='undefined'){
		$('#selectedClassAry').val(className);
	}else{
		array = existingValueStr.split(',');
		//check existance of className;
		for(i=0;i<array.length;i++){
			if(array[i]==className){
				return;
			}
		}
		newValuesStr = existingValueStr+','+className;
		$('#selectedClassAry').val(newValuesStr);
	}
}
function getSeclectedGroupLogInfo(groupIDArray){

	var existingValueStr = $('#selectedGroupAry').val();
	var newValuesStr = '';
	if(groupIDArray==''||typeof(groupIDArray)=='undefined'){
		return;
	}
	if(existingValueStr==''||!existingValueStr){
		$('#selectedGroupAry').val(groupIDArray);
	}else{
		$('#selectedGroupAry').val(existingValueStr+','+groupIDArray);
	}
}
function getSeclectedTargetLogInfo(targetCatID){
	var existingValueStr = $('#selectedTargetAry').val();
	var newValuesStr = '';
	if(targetCatID==''){
		return;
	}
	if(existingValueStr==''||!existingValueStr||typeof(targetCatID)=='undefined'){
		$('#selectedTargetAry').val(targetCatID);
	}else{
		array = existingValueStr.split(',');
		//check existance of className;
		for(i=0;i<array.length;i++){
			if(array[i]==targetCatID){
				return;
			}
		}
		newValuesStr = existingValueStr+','+targetCatID;
		$('#selectedTargetAry').val(newValuesStr);
	}
}
</script>
<br />
<form id="form1" name="form1" action="<?=$updateScript?>" method="post" enctype="multipart/form-data" onSubmit="return checkform(this);">
	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	
		<tr>
			<td class="navigation">
				<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr><td><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?></td></tr>
				</table>
			</td>
		</tr>
	</table>
	<br />
	
	<table class="form_table_v30" width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
		
		<? if ($isAdminUser) { ?>
			<?php if ($sys_custom['MessageCenter']['OnlySendByUI']) { ?>
				<input type="hidden" name="IsCSV" id="IsCSVNo" value="0" />
			<?php } else { ?>
				<tr>
				   	<td class="field_title" width="30%" valign="top"  nowrap='nowrap'><span class="tabletextrequire">*</span><?=$Lang['AppNotifyMessage']['MethodMessage']?></td>
					<td align="left" valign="top">
						<input type="radio" name="IsCSV" id="IsCSVNo" onClick="ShowHideUserSelection();$('#TemplateRow').show();$('#attachedImageFile').removeAttr('disabled');$('#attachedImageRow').show();" value="0" checked="checked" /> <label for="IsCSVNo"><?=$Lang['AppNotifyMessage']['MethodSameMessage']?></label> &nbsp;
				  		<input type="radio" name="IsCSV" id="IsCSVYes" onClick="ShowHideUserSelection();$('#TemplateRow').hide();$('#attachedImageFile').attr('disabled',true);$('#attachedImageRow').hide();" value="1"  /> <label for="IsCSVYes"><?=$Lang['AppNotifyMessage']['MethodDifferentMessage']?></label>
				  	</td>
				</tr>
			<?php }?>
		<? } ?>
		<tr id="TemplateRow">
			<td class="field_title"><?=$Lang['SMS']['MessageTemplates']?></td>
			<td>
				<?=$libAppTemplate->getMessageTemplateSelectionBox()?>
				<span id="AjaxStatus"></span>
			</td>
		</tr>
		<!-- Title -->
		<tr>
		   <td class="field_title" width="30%" valign="top"  nowrap='nowrap'><span class="tabletextrequire">*</span><?=$Lang['AppNotifyMessage']['Title']?></td>
			<td align="left" valign="top">
			  <input type="text" class="textboxtext" id="MsgTitle" name="MsgTitle" value="<?=$MsgTitle?>" size="100%" maxlength="128" />
			</td>
		</tr>
		<!-- Title -->
		
		<tr id="MessageRow">
		   <td class="field_title" width="30%" valign="top"  nowrap='nowrap'><span class="tabletextrequire">*</span><?=$Lang['AppNotifyMessage']['Description']?><br/>
               <!-- change it from 500 to 150 charactor for php 5.4 site can't show problem, change back for use default json can solve-->
               <span class="tabletextremark">(<?=$Lang['MessageCenter']['ContentMaximumRemarks']?>)</span></td>
            <td align="left" valign="top">
			  <?= $linterface->GET_TEXTAREA("Message", $Message, "100%",5,"","","maxlength=\"500\""); ?>
			</td>
		</tr>
		
		<!-- show in  -->
			<tr id="PublicRow">
			<td class="field_title" valign="top"  nowrap='nowrap' id="Recipients">
				<span class="tabletextrequire">*</span><?=$Lang['AppNotifyMessage']['Recipients']?>
				<br/>
				<span class="tabletextremark">(<?=$recipientRemarks?>)</span>
			</td>
		<? if ($isAdminUser) { ?>
			<?php if ($sys_custom['MessageCenter']['OnlySendToPublic']) { ?>
				<input type="hidden" name="IsPublic" id="IsPublicY" value="Y" />
			<?php } else { ?>

					<td align="left" valign="top">
					  <input type="radio" name="IsPublic" id="IsPublicY" onClick="$('#'+'RecipientRow').hide();$('#Recipients').attr('rowspan',1);" value="Y" <?=$isPublicY_checked?> /> <label for="IsPublicY"><?=$publicLang?></label> &nbsp;
					  <input type="radio" name="IsPublic" id="IsPublicN" onClick="$('#'+'RecipientRow').show();$('#Recipients').attr('rowspan',2);" value="N" <?=$isPublicN_checked?> /> <label for="IsPublicN"><?=$nonPublicLang?></label>
					  &nbsp;&nbsp;
					  <?=$exportMobileBtn?>
					</td>
				</tr>
			<?php } ?>
		<? } else { ?>
			<input type="hidden" name="IsPublic" id="IsPublicN" value="N" />
		<? } ?>
		
		 <? if ($isAdminUser) { ?><tr id="RecipientRow" style="display:none"> <? } ?>
			
			<td><table border="0">
				
					<td><select id="userSelector" name="Recipient[]" size="10" multiple><option><?php for($i = 0; $i < 40; $i++) echo "&nbsp;"; ?></option></select></td>
					<td width="50%"">
					<?php 
					if ($plugin['eClassApp'] || $plugin['eClassTeacherApp']) {
						// select student 
						//if($sys_custom['DHL']){
						//	$commonChooseLink .= "&from_thickbox=1";
						//	echo $linterface->GET_BTN($Lang['General']['ChooseUser'], "button", "show_dyn_thickbox_ip('','".$commonChooseLink."', '', 0, 640, 640, 1, 'fakeDiv', null);");
						//}else{
							echo $linterface->GET_BTN($Lang['General']['ChooseUser'], "button", "newWindow('".$commonChooseLink."',3)");
						//}
					} else { 
						// select parent directly	
						//if($sys_custom['DHL']){
						//	echo $linterface->GET_BTN($Lang['General']['ChooseUser'], "button", "show_dyn_thickbox_ip('','../../../../common_choose/index.php?fieldname=Recipient[]&page_title=SelectAudience&permitted_type=3&DisplayGroupCategory=0&filterAppParent=1&custFollowUpJs=1&logToDB=1&from_thickbox=1', '', 0, 640, 640, 1, 'fakeDiv', null);");
						//}else{
							echo $linterface->GET_BTN($Lang['General']['ChooseUser'], "button", "newWindow('../../../../common_choose/index.php?fieldname=Recipient[]&page_title=SelectAudience&permitted_type=3&DisplayGroupCategory=0&filterAppParent=1&custFollowUpJs=1&logToDB=1',3)");
						//}
					}
					?>
					<br /> <br />
					<?= $linterface->GET_BTN($Lang['Btn']['RemoveSelected'], "button", "checkOptionRemove(document.form1.elements['Recipient[]']);commonChooseFollowUpAction()") ?>
					<br /> <br />
					<span style="display:none" id="outerspan"><span id="numOfUser" name="numOfUser" >x</span> <?=$Lang['MessageCenter']['UserSelected']?></span>
					</td>
					
			
				
				</table>
			</td>
		</tr>
		<tr id="CSVUploadRow" style="display:none">
			<td valign="top" class="field_title"><span class="tabletextrequire">*</span><?=$Lang['AppNotifyMessage']['CSVFile']?></td>
			<td valign="top">
				<input type="file" name="FileCSV" size="60" />
				<br>
				<p><a class="functionlink_new tablelink" href="<?=GET_CSV($sampleCsv)?>"><?=$i_general_clickheredownloadsample?></a></p>
			</td>
		</tr>
		
		<? if ($plugin['eClassApp'] || $plugin['eClassTeacherApp']) { ?>
		
		<?php if (!$sys_custom['MessageCenter']['HideSendingImage']) { ?>
        <tr id='attachedImageRow'>
            <td valign="top" class="field_title">
                <?=$Lang['AppNotifyMessage']['AttachImage']?>
				<br>
				<span class="tabletextremark">(<?=$Lang['MessageCenter']['ImageAttachementReminder']?>)</span>
            </td>
            <td valign="top">
                <input type="file" id="attachedImageFile" name="attachedImage" accept="image/*" />
            </td>
        </tr>
        <?php } ?>

		<tr>
			<td valign="top" class="field_title">
				<span class="tabletextrequire">*</span> <?=$Lang['AppNotifyMessage']['SendTime']?>
				<br>
				<span class="tabletextremark">(<?php echo $Lang['AppNotifyMessage']['SendInBatchesRemarks'] ?>)</span>
			</td>
			<td valign="top">
				<?=$htmlAry['sendTimeNowRadio']?>
				<br />
				<?=$htmlAry['sendTimeScheduledRadio']?>
				<br />
				<div id="specificSendTimeDiv" style="display:none;">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$htmlAry['sendTimeDateTimeDisplay']?>
				</div>
			</td>
		</tr>
		<? } ?>
		
	</table>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tr><td align="left" class="tabletextremark"><?=$i_general_required_field?></td></tr>
					<tr><td align="left" class="tabletextremark"><?=$display_Remarks?></td></tr>
					<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
					<tr>
						<td align="center">
							<div id="buttonDiv">
								<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
								<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "goBack();")?>
							</div>
							<div id="sendingDiv" style="width:100%; text-align:center; display:none;"><?=$linterface->Get_Ajax_Loading_Image($noLang=1)?> <span style="color:red; font-weight:bold;"><?=$Lang['General']['Sending...']?></span></div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<input type="hidden" id="appType" name="appType" value="<?=$appType?>" />
	<input type="hidden" id="sendTimeString" name="sendTimeString" value="" />
	<input type="hidden" id="selectedClassAry" name="selectedClassAry" value=""/>
	<input type="hidden" id="selectedGroupAry" name="selectedGroupAry" value=""/>
	<input type="hidden" id="selectedTargetAry" name="selectedTargetAry" value=""/>
<?php
if (isset($MassMailingID) && $MassMailingID>0)
{
	echo "<input type='hidden' name='MassMailingID' value='{$MassMailingID}' />";
}
?>

</form>
<?=	$linterface->FOCUS_ON_LOAD("form1.MsgTitle") ?>
<?

	$linterface->LAYOUT_STOP();
	
	intranet_closedb();
?>