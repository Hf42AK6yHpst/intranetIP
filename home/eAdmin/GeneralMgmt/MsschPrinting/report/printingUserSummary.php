<?php


///////////////// Access Right START /////////////////
if(!libMsschPrintingAccessRight::checkPrintingSummaryAccessRight()){
	header('Location: /');
	exit;
}
///////////////// Access Right END /////////////////


///////////////// Init START /////////////////
$resultPath = 'report.printingUserSummary.result_v2';
$ajaxSearchUserFilterPath = '?t=generalAjax.getUserSelection';
$ajaxSearchFormFilterPath = '?t=generalAjax.getFormSelection';
$ajaxSearchClassFilterPath = '?t=generalAjax.getClassSelection';
///////////////// Init END /////////////////


///////////////// Validate START /////////////////
///////////////// Validate END /////////////////


///////////////// UI Field START /////////////////
$academicYearFromHTML = getSelectAcademicYear('AcademicYearFrom', $tag='', $noFirst=1, $noPastYear=0, $targetYearID='', $displayAll=0, $pastAndCurrentYearOnly=1, $OrderBySequence=1, $excludeCurrentYear=0, $excludeYearIDArr=array());
$academicYearToHTML = getSelectAcademicYear('AcademicYearTo', $tag='', $noFirst=1, $noPastYear=0, $targetYearID='', $displayAll=0, $pastAndCurrentYearOnly=1, $OrderBySequence=1, $excludeCurrentYear=0, $excludeYearIDArr=array());

$_First = Get_Selection_First_Title($Lang['SysMgr']['Timetable']['Select']['Identity']);
$identityHTML = <<<HTML
	<select id="identity">
		<option value="">{$_First}</option>
		<option value="TeachingStaff">{$Lang['Identity']['TeachingStaff']}</option>
		<option value="NonTeachingStaff">{$Lang['Identity']['NonTeachingStaff']}</option>
		<option value="Student">{$Lang['Identity']['Student']}</option>
	</select>
HTML;
///////////////// UI Field END /////////////////


//////////////////// UI START ////////////////////
$CurrentPage = "ReportPrintUserSummary";
$TAGS_OBJ[] = array($Lang['MsschPrint']['menu']['printingUserSummary'], "", 0);
$MODULE_OBJ = $objPrinting->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($Msg);
?>
<script language="JavaScript" src="/templates/jquery/jquery-1.11.1.min.js"></script>


<form id="form1" action="index.php" method="POST" action="index.php">
	<input type="hidden" name="t" value="<?=$resultPath?>" />
	
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
		
			<table  border="0" cellspacing="0" cellpadding="5" width="1000px" id="searchTable">
			
<!-- -------------------- Form Element START -------------------- -->
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['MsschPrint']['report']['printingSummary']['searchMenu']['academicYear']?></td>
					<td class="tabletext" width="70%"><?=$academicYearFromHTML?>&nbsp;<?=$Lang['General']['To']?>&nbsp;<?=$academicYearToHTML?></td>
				</tr>
				
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['SysMgr']['Timetable']['Identity']?></td>
					<td class="tabletext" width="70%"><?=$identityHTML?></td>
				</tr>
				
				<tr id="searchFormFilter" style="display:none">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['MsschPrint']['report']['printingSummary']['searchMenu']['form']?></td>
					<td id="formFilter"><?=$linterface->Get_Ajax_Loading_Image()?></td>
				</tr>
				
				<tr id="searchClassFilter" style="display:none">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['MsschPrint']['report']['printingSummary']['searchMenu']['class']?></td>
					<td id="classFilter"><?=$linterface->Get_Ajax_Loading_Image()?></td>
				</tr>
				
				<tr id="searchUserFilter" style="display:none">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['MsschPrint']['report']['printingSummary']['searchMenu']['user']?></td>
					<td id="userFilter"><?=$linterface->Get_Ajax_Loading_Image()?></td>
				</tr>
<!-- -------------------- Form Element END -------------------- -->

				<tr>
					<td height="1" class="dotline" colspan="2"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td height="1" align="center" colspan="2"><?= $linterface->GET_ACTION_BTN($Lang['Btn']['View'], 'button',$ParOnClick="", $ParName="search")?>&nbsp;</td>
				</tr>
			</table>

		</td>
	</tr>
</table>
</form>

<script>
	$('#search').click(function(){
		
		var isValid = true;

		//alert('<?=$Lang['MsschPrint']['report']['printingSummary']['searchErr']['wrongTimePeriod']?>');		
		
		
		
		if( ($('#selectedUserID').length == 0) || ($('#selectedUserID').val() == '') ){
			alert('<?=$Lang['MsschPrint']['report']['printingSummary']['searchErr']['noUser']?>');
			isValid = false;
		}
		
		if(isValid){
			$('#form1').submit();
		}
	});
	
	$('#identity').change(function(){
		if($(this).val() == ''){
			return false;
		}else if($(this).val() == 'Student'){
			$('#searchUserFilter').hide();
			$('#searchFormFilter').show();
			$.get('<?=$ajaxSearchFormFilterPath?>', {
				'SelectionID': 'FormID'
			}, function(res){
				$('#formFilter').html(res);
			});
		}else{
			$('#searchFormFilter').hide();
			$('#searchClassFilter').hide();
			$('#searchUserFilter').show();
			$.get('<?=$ajaxSearchUserFilterPath?>', {
				'SelectedUserType': $(this).val(),
				'SelectionID': 'selectedUserID'
			}, function(res){
				$('#userFilter').html(res);
			});
		}
	});
	
	$('#searchTable').on('change', '#FormID', function(){
		$('#searchUserFilter').hide();
		$('#searchClassFilter').show();
		$.get('<?=$ajaxSearchClassFilterPath?>', {
			'FormID': $(this).val(),
			'SelectionID': 'ClassID'
		}, function(res){
			$('#classFilter').html(res);
		});
	});
	
	$('#searchTable').on('change', '#ClassID', function(){
		$('#searchUserFilter').show();
		$.get('<?=$ajaxSearchUserFilterPath?>', {
			'SelectedUserType': 'Student',
			'SelectionID': 'selectedUserID',
			'YearClassIDArr': $(this).val()
		}, function(res){
			$('#userFilter').html(res);
		});
	});
</script>

<?php
$linterface->LAYOUT_STOP();
?>