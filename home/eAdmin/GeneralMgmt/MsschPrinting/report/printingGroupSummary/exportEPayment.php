<?php

///////////////// Access Right START /////////////////
if(!libMsschPrintingAccessRight::checkPrintingSummaryAccessRight()){
	header('Location: /');
	exit;
}
///////////////// Access Right END /////////////////


///////////////// Init START /////////////////
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
$lexport = new libexporttext();

$Rows = array();
$exportColumn = array();

$startDate = $_POST['fromMonth'];
$endDate = $_POST['toMonth'];
$formID = $_POST['formID'];
///////////////// Init END /////////////////

///////////////// Get Data START /////////////////
$result = $objPrinting->getFormUsage($startDate, $endDate, $formID);
///////////////// Get Data END /////////////////

///////////////// Export START /////////////////
$exportColumn[] = $Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['headerEng'];
$Rows[] = $Lang['MsschPrint']['report']['printingSummary']['exportToEPayment']['headerChi'];

if(!$sys_custom['ePayment']['PaymentMethod']){
	unset($exportColumn[0][ count($exportColumn[0])-1 ]);
	unset($Rows[0][ count($Rows[0])-1 ]);
}

//// Export Data START ////
foreach($result as $r){
	$dataRow = array(
		$r["UserLogin"],
		$r["StudentName"],
		$r["TOTAL_AMOUNT"],
		'',
	);
	if($sys_custom['ePayment']['PaymentMethod']){
		$dataRow[] = '';
	}
	$Rows[] = $dataRow;
}




///////////////// Output START /////////////////
if(!empty($Rows)){
	$exportContent = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($Rows, $exportColumn, "\t", "\r\n", "\t", 0, "11");
}else{
	$exportContent = $Lang['SysMgr']['Homework']['NoRecord'];
}
$lexport->EXPORT_FILE('export.csv', $exportContent);
exit;
?>