<?php


///////////////// Access Right START /////////////////
if (!libMsschPrintingAccessRight::checkPrintingSummaryAccessRight()) {
    header('Location: /');
    exit;
}
///////////////// Access Right END /////////////////


///////////////// Report All Class START /////////////////

$selfPagePath = '?t=report.printingClassSummary.result_v2';

//// Get row data START ////
$allClass = $form_class_manage->Get_Class_List_By_Academic_Year($currentAcademicYear, $YearID='', $TeachingOnly=0, $YearClassIDArr=$classID);
$classArr = array();
foreach($allClass as $class){
    $classArr[$class['YearClassID']] = Get_Lang_Selection($class['ClassTitleB5'], $class['ClassTitleEN']);
}
//// Get row data END ////

//// Get Report data START ////
$rs = $objPrinting->getAllClassReportData_v2($YearClassIdArr = $classID);

$rowTotalUsage = array();
$rowMonthUsage = array();
$monthTotalUsage = array();
foreach ($rs as $r) {
    $yearMonth = substr($r['RECORD_DATE'], 0, 7);
    $rowMonthUsage[$r['YearClassID']][$yearMonth] += $r['AMOUNT'];
    $rowTotalUsage[$r['YearClassID']] += $r['AMOUNT'];
    $monthTotalUsage[$yearMonth] += $r['AMOUNT'];
}
//// Get Report data END ////

//// Pack data START ////
$rowData = array();
foreach ($classArr as $yearClassID => $className) {
    $_data = array();

    $title = "<a href=\"{$selfPagePath}&classID={$yearClassID}\">{$className}</a>";
    $_data[] = $title;
    foreach ($yearMonthArr as $yearMonth) {
        if ($rowMonthUsage[$yearClassID][$yearMonth] == 0) {
            $_data[] = '&nbsp;';
        } else {
            $_data[] = number_format($rowMonthUsage[$yearClassID][$yearMonth], $cfg_msschPrint['moneyFormat']['decimals']);
        }
    }
    $_data[] = number_format($rowTotalUsage[$yearClassID], $cfg_msschPrint['moneyFormat']['decimals']);

    $rowData[] = $_data;
}
//// Pack data END ////
///////////////// Report All Class END /////////////////


$className = $Lang['MsschPrint']['report']['printingSummary']['result']['allClass'];

$backPath = '?t=report.printingClassSummary';
$onBackPress = "window.location.href = '{$backPath}'";