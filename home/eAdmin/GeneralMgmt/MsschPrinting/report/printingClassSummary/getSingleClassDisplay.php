<?php


///////////////// Access Right START /////////////////
if(!libMsschPrintingAccessRight::checkPrintingSummaryAccessRight()){
	header('Location: /');
	exit;
}
///////////////// Access Right END /////////////////



///////////////// SQL Safe START /////////////////
$classID = IntegerSafe($classID);
///////////////// SQL Safe END /////////////////

///////////////// Report All Class START /////////////////

$selfPagePath = '?t=report.printingClassSummary.result';

//// Get row data START ////
$allStudent = $form_class_manage->Get_Student_By_Class($classID);
//$rowTitle = BuildMultiKeyAssoc($allStudent, array('UserID'), array('StudentName'), 1);
$rowTitle = array();
foreach($allStudent as $student){
	$className = Get_Lang_Selection($student['ClassTitleB5'], $student['ClassTitleEN']);
	$rowTitle[$student['UserID']] = "{$className}({$student['ClassNumber']}) {$student['StudentName']}";
}
//// Get row data END ////


//// Get Report data START ////
$reportData = $objPrinting->getSingleClassReportData($UserIdArr = array_keys($rowTitle), $firstRecordDate = $period['StartDate']);
$oldBalance = $reportData['OldBalance'];
$data = BuildMultiKeyAssoc($reportData['CurrentYearUsage'], array('UserID', 'RECORD_DATE'), $IncludedDBField=array('TOTAL_AMOUNT'), $SingleValue=1);
//// Get Report data END ////

///////////////// Report All Class END /////////////////

$year_class = new year_class($classID);
$className = $year_class->Get_Class_Name();


$onBackPress = 'history.back()';
?>