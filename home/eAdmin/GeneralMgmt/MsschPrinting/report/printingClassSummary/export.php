<?php
/*
 * 2018-11-13 (Ivan) [K152295] [ip.2.5.9.11.1]
 *  - Hidden balance related columns
 */

///////////////// Init START /////////////////
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
$lexport = new libexporttext();

$Rows = array();
$exportColumn = array();
///////////////// Init END /////////////////


///////////////// Access Right START /////////////////
if(!libMsschPrintingAccessRight::checkPrintingSummaryAccessRight()){
    header('Location: /');
    exit;
}
///////////////// Access Right END /////////////////


///////////////// Export START /////////////////
if(is_array($classID)){
    $exportColumn[] = $Lang['MsschPrint']['report']['printingSummary']['result']['class'];
}else{
    $exportColumn[] = $Lang['MsschPrint']['report']['printingSummary']['result']['userName'];
}
// $exportColumn[] = $Lang['MsschPrint']['report']['printingSummary']['result']['accumulateBalance'];
foreach($monthArr as $month){
    $m = (int)substr($month, 5,2);
    $exportColumn[] = $Lang['General']['month'][$m];
}
$exportColumn[] = $Lang['MsschPrint']['report']['printingSummary']['result']['totalAmount'];
// $exportColumn[] = $Lang['MsschPrint']['report']['printingSummary']['result']['currentBalance'];

$monthAmount = array();
foreach((array)$rowTitle as $id => $title){
    $_cell = array();
    
    //// Filter <a> for title
    if(strpos($title, "<a") > -1){
        $title = substr($title, strpos($title, '>') + 1);
        $title = substr($title, 0, -4);
    }
    $title = str_replace('<font style="color:red;">*</font>', '*', $title);
    $_cell[] = $title;
    
    //     $_cell[] = $oldBalance[$id];
    
    $rowTotalAmount = 0;
    foreach($monthArr as $month){
        $day = str_replace('_','-',$month) . '-01';
        $_amount = $data[$id][$day];
        $rowTotalAmount += $_amount;
        $monthAmount[$month] += $_amount;
        if($_amount){
            $_cell[] = number_format($_amount, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']);
        }else{
            $_cell[] = '';
        }
    }
    if($rowTotalAmount){
        $_cell[] = number_format($rowTotalAmount, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']);
    }else{
        $_cell[] = '';
    }
    //     if($oldBalance[$id] - $rowTotalAmount){
    //         $_cell[] = number_format($oldBalance[$id] - $rowTotalAmount, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']);
    //     }else{
    //         $_cell[] = '';
    //     }
    
    $Rows[] = $_cell;
}

$_cell = array();
$totalOldBalance = array_sum($oldBalance);
$_cell[] = $Lang['MsschPrint']['report']['printingSummary']['result']['totalAmount'];
// $_cell[] = number_format($totalOldBalance, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']);
foreach($monthArr as $month){
    $_amount = $monthAmount[$month];
    if($_amount){
        $_cell[] = number_format($_amount, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']);
    }else{
        $_cell[] = '';
    }
}
if($totalAmount){
    $_cell[] = number_format($totalAmount, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']);
}else{
    $_cell[] = '';
}

// $currentBalance = $totalOldBalance - $totalAmount;
// $_cell[] = number_format($currentBalance, $cfg_msschPrint['moneyFormat']['decimals'], $cfg_msschPrint['moneyFormat']['dec_point'], $cfg_msschPrint['moneyFormat']['thousands_sep']);

$Rows[] = $_cell;


if(!is_array($classID)){
    $Rows[] = '';
    $Rows[] = array(
        str_replace('<font style="color:red;">*</font>', '*', $Lang['SysMgr']['FormClassMapping']['DeletedUserLegend'])
    );
}

///////////////// Export END /////////////////


///////////////// Output START /////////////////
if(!empty($Rows)){
    $exportContent = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($Rows, $exportColumn, "\t", "\r\n", "\t", 0, "11");
}else{
    $exportContent = $Lang['SysMgr']['Homework']['NoRecord'];
}
$lexport->EXPORT_FILE('export.csv', $exportContent);
exit;
?>