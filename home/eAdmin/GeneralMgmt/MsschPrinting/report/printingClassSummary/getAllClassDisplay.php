<?php


///////////////// Access Right START /////////////////
if(!libMsschPrintingAccessRight::checkPrintingSummaryAccessRight()){
	header('Location: /');
	exit;
}
///////////////// Access Right END /////////////////



///////////////// Report All Class START /////////////////

$selfPagePath = '?t=report.printingClassSummary.result';

//// Get row data START ////
$allClass = $form_class_manage->Get_Class_List_By_Academic_Year($currentAcademicYear, $YearID='', $TeachingOnly=0, $YearClassIDArr=$classID);
$classArr = array();
foreach($allClass as $class){
	$classArr[$class['YearClassID']] = Get_Lang_Selection($class['ClassTitleB5'], $class['ClassTitleEN']);
}
//$classArr = BuildMultiKeyAssoc($allClass, array('YearClassID'), array(Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN')), 1);
$rowTitle = array();
foreach($classArr as $id => $title){
	$title = "<a href=\"{$selfPagePath}&classID={$id}\">{$title}</a>";
	$rowTitle[$id] = $title;
}
//// Get row data END ////


//// Get Report data START ////
$reportData = $objPrinting->getAllClassReportData($YearClassIdArr = $classID, $firstRecordDate = $period['StartDate']);
$oldBalance = $reportData['OldBalance'];
$data = BuildMultiKeyAssoc($reportData['CurrentYearUsage'], array('YearClassID', 'RECORD_DATE'), $IncludedDBField=array('TOTAL_AMOUNT'), $SingleValue=1);
//// Get Report data END ////

///////////////// Report All Class END /////////////////


$className = $Lang['MsschPrint']['report']['printingSummary']['result']['allClass'];

$backPath = '?t=report.printingClassSummary';
$onBackPress = "window.location.href = '{$backPath}'";
?>