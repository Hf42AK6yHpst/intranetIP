<?php


///////////////// Access Right START /////////////////
if (!libMsschPrintingAccessRight::checkPrintingSummaryAccessRight()) {
    header('Location: /');
    exit;
}
///////////////// Access Right END /////////////////


///////////////// SQL Safe START /////////////////
$classID = IntegerSafe($classID);
///////////////// SQL Safe END /////////////////

///////////////// Report All Class START /////////////////

$selfPagePath = '?t=report.printingClassSummary.result_v2';


//// Get Report data START ////
$allStudent = $form_class_manage->Get_Student_By_Class($classID);
$allStudentId = Get_Array_By_Key($allStudent, 'UserID');

$rs = $objPrinting->getSingleClassReportData_v2($allStudentId);

$rowTotalUsage = array();
$rowMonthUsage = array();
$monthTotalUsage = array();
foreach ($rs as $r) {
    $yearMonth = substr($r['RECORD_DATE'], 0, 7);
    $rowMonthUsage[$r['UserID']][$yearMonth] += $r['AMOUNT'];
    $rowTotalUsage[$r['UserID']] += $r['AMOUNT'];
    $monthTotalUsage[$yearMonth] += $r['AMOUNT'];
}
//// Get Report data END ////

//// Pack data START ////
$rowData = array();
foreach ($allStudent as $studentInfo) {
    $_data = array();

    $className = Get_Lang_Selection($studentInfo['ClassTitleB5'], $studentInfo['ClassTitleEn']);
    $_data[] = "{$className}({$studentInfo['ClassNumber']}) {$studentInfo['StudentName']}";
    foreach ($yearMonthArr as $yearMonth) {
        if ($rowMonthUsage[$studentInfo['UserID']][$yearMonth] == 0) {
            $_data[] = '&nbsp;';
        } else {
            $_data[] = number_format($rowMonthUsage[$studentInfo['UserID']][$yearMonth], $cfg_msschPrint['moneyFormat']['decimals']);
        }
    }
    $_data[] = number_format($rowTotalUsage[$studentInfo['UserID']], $cfg_msschPrint['moneyFormat']['decimals']);

    $rowData[] = $_data;
}
//// Pack data END ////
///////////////// Report All Class END /////////////////

$year_class = new year_class($classID);
$className = $year_class->Get_Class_Name();


$onBackPress = 'history.back()';