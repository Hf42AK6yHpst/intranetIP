<?php
/*
 * 2018-11-13 (Ivan) [K152295] [ip.2.5.9.11.1]
 *  - Hidden balance related columns
 */

///////////////// Init START /////////////////
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
$lexport = new libexporttext();

$Rows = array();
$exportColumn = array();
///////////////// Init END /////////////////


///////////////// Access Right START /////////////////
if(!libMsschPrintingAccessRight::checkPrintingSummaryAccessRight()){
    header('Location: /');
    exit;
}
///////////////// Access Right END /////////////////


///////////////// Export START /////////////////
if(is_array($classID)){
    $exportColumn[] = $Lang['MsschPrint']['report']['printingSummary']['result']['class'];
}else{
    $exportColumn[] = $Lang['MsschPrint']['report']['printingSummary']['result']['userName'];
}
// $exportColumn[] = $Lang['MsschPrint']['report']['printingSummary']['result']['accumulateBalance'];
foreach ($yearMonthArr as $yearMonth) {
    $m = (int)substr($yearMonth, 5, 2);
    $exportColumn[] = $Lang['General']['month'][$m];
}
$exportColumn[] = $Lang['MsschPrint']['report']['printingSummary']['result']['totalAmount'];
// $exportColumn[] = $Lang['MsschPrint']['report']['printingSummary']['result']['currentBalance'];

foreach ((array)$rowData as $d1) {
    $_cell = array();

    foreach ($d1 as $i => $d){
        if(strpos($d, "<a") > -1){
            $d = substr($d, strpos($d, '>') + 1);
            $d = substr($d, 0, -4);
        }
        $d = str_replace('<font style="color:red;">*</font>', '*', $d);
        $_cell[] = $d;
    }

    $Rows[] = $_cell;
}

$_cell = array();
$_cell[] = $Lang['MsschPrint']['report']['printingSummary']['result']['totalAmount'];

foreach ($yearMonthArr as $i => $yearMonth){
$_cell[] = $monthTotalUsage[$yearMonth];
}
$_cell[] = number_format($totalAmount, $cfg_msschPrint['moneyFormat']['decimals']);


$Rows[] = $_cell;


if(!is_array($classID)){
    $Rows[] = '';
    $Rows[] = array(
        str_replace('<font style="color:red;">*</font>', '*', $Lang['SysMgr']['FormClassMapping']['DeletedUserLegend'])
    );
}

///////////////// Export END /////////////////


///////////////// Output START /////////////////
if(!empty($Rows)){
    $exportContent = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($Rows, $exportColumn, "\t", "\r\n", "\t", 0, "11");
}else{
    $exportContent = $Lang['SysMgr']['Homework']['NoRecord'];
}
$lexport->EXPORT_FILE('export.csv', $exportContent);
exit;
?>