<?php
// using: 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

////////// Cust Include START //////////
include_once($PATH_WRT_ROOT."includes/cust/common_ui.php");
include_once($PATH_WRT_ROOT."includes/cust/common_function.php");
////////// Cust Include END //////////

////////// Printing Include START //////////
include_once($PATH_WRT_ROOT."lang/cust/MSSCH_printing_lang.".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."includes/cust/MSSCH_printing/printingConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/cust/MSSCH_printing/libMsschPrinting.php");
include_once($PATH_WRT_ROOT."includes/cust/MSSCH_printing/libMsschPrintingAccessRight.php");

//// Autoload START ////
function _mssch_printing_autoloader($class) {
	global $PATH_WRT_ROOT;
    include "{$PATH_WRT_ROOT}/includes/cust/MSSCH_printing/DB_related/{$class}.php";
}
spl_autoload_register('_mssch_printing_autoloader');
//// Autoload END ////

////////// Printing Include END //////////

////////// Access Right Check START //////////
if(!$plugin['mssch_printing_module']){
	header("Location: /");
	exit();
}
////////// Access Right Check END //////////

////////// Init START //////////
intranet_auth();
intranet_opendb();
$objDB = new libdb();
$objPrinting = new libMsschPrinting();
$linterface = new interface_html();

$CurrentPageArr['mssch_printing'] = 1;
////////// Init END //////////

$t = trim($t);

// This cust module don't allow IE7 to open
$home_header_no_EmulateIE7 = true;

// Prevent searching parent directories, like 'task=../../index'.
$t= str_replace("../", "", $t);
$t = str_replace('.','/',$t);
$mod_script = $t.'.php';

if($t == ''){
	$CurrentPage = "";
	$MODULE_OBJ = $objPrinting->GET_MODULE_OBJ_ARR();		
	$linterface->LAYOUT_START($Msg);
	
	echo $linterface->Get_Warning_Message_Box($Lang['General']['Instruction'], $i_Sports_Select_Menu);
	$linterface->LAYOUT_STOP();
	exit();
}



$Msg = urldecode($Msg);
if(file_exists($mod_script)){
	include($mod_script);	
}else{
	echo 'file not found<br/>'.$mod_script;
}

intranet_closedb();
exit();
?>