<?php

/**
 * Input{
 * 		SelectionID: The HTML name of selection box
 * 		AcademicYearID(optional): The AcademicYearID of the class, default current year.
 * }
 */


///////////////// Access Right START /////////////////
///////////////// Access Right END /////////////////


///////////// SQL Safe START /////////////
$AcademicYearID = IntegerSafe($AcademicYearID);
if($AcademicYearID === 0){
	$AcademicYearID = Get_Current_Academic_Year_ID();
}
///////////// SQL Safe END /////////////


///////////// Validate START /////////////
///////////// Validate END /////////////


///////////// Init START /////////////
$selectMonthHTML = $objPrinting->getYearMonthSelection($SelectionID, $AcademicYearID);
///////////// Init END /////////////


///////////// UI START /////////////
echo $selectMonthHTML;
