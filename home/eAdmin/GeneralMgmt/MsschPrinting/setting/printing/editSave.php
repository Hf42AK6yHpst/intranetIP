<?php

/////////////////// SQL Safe START ///////////////////
$AcademicYearID = IntegerSafe($AcademicYearID);
/////////////////// SQL Safe END ///////////////////


//////////////////// Access Right START ////////////////////
if (!libMsschPrintingAccessRight::checkGroupSettingAccessRight()) {
    header('Location: /');
    exit;
}
//////////////////// Access Right END ////////////////////


/////////////////// INIT START ///////////////////
$yearMonthArr = $objPrinting->getYearMonthArr($AcademicYearID);
$result = array();
$isValid = true;
/////////////////// INIT END ///////////////////

/////////////////// Validate START ///////////////////
$fields = array('colorBW', 'colorFc', 'a4Price', 'a3Price');
foreach ($fields as $field) {
    foreach($yearMonthArr as $yearMonth) {
        $valid = (preg_match('/^\d+\.?\d{0,3}$/', $_POST[$field][$yearMonth]) || $_POST[$field][$yearMonth] == '');
        $result[] = $valid;
        $isValid = $isValid && $valid;
    }
}
/////////////////// Validate END ///////////////////

/////////////////// Save START ///////////////////
if ($isValid) {
    foreach ($fields as $field) {
        foreach($yearMonthArr as $yearMonth) {
            $obj = new PRINTING_COST(0);
            $obj->setAcademicYearID($AcademicYearID);
            $obj->setYearMonth($yearMonth);
            $obj->setType($field);
            $obj->setCost($_POST[$field][$yearMonth]);

            $result[] = $obj->save();
        }
    }
}
/////////////////// Save END ///////////////////


/////////////////// Redirect START ///////////////////
if (in_array(false, $result)) {
    if ($actionType == 'edit') {
        $Msg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
    } else {
        $Msg = $Lang['General']['ReturnMessage']['AddUnsuccess'];
    }
} else {
    if ($actionType == 'edit') {
        $Msg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
    } else {
        $Msg = $Lang['General']['ReturnMessage']['AddSuccess'];
    }
}
header('Location: ?t=setting.printing&Msg=' . $Msg);
exit;