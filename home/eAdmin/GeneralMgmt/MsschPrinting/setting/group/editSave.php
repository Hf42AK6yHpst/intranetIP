<?php

/////////////////// SQL Safe START ///////////////////
$groupID = IntegerSafe($groupID);
$mfpCode = trim( htmlentities( $mfpCode , ENT_QUOTES, 'UTF-8'));
$groupName = trim( htmlentities( $groupName , ENT_QUOTES, 'UTF-8'));	
$PIC = IntegerSafe($PIC);
$Member = array();
foreach((array)$r_group_user as $m){
	$Member[] = IntegerSafe(trim($m,'U'));
}
/////////////////// SQL Safe END ///////////////////


//////////////////// Access Right START ////////////////////
if($groupID){
	if(!libMsschPrintingAccessRight::checkGroupSettingAccessRight($groupID)){
		header('Location: /');
		exit;
	}
}else{
//	if(!libMsschPrintingAccessRight::isSuperAdmin()){
	if(!libMsschPrintingAccessRight::checkGroupSettingAccessRight()){
		header('Location: /');
		exit;
	}
}
//////////////////// Access Right END ////////////////////


/////////////////// INIT START ///////////////////
$actionType = ($groupID)?'edit':'new';
$result = array();
$isValid = true;
/////////////////// INIT END ///////////////////

/////////////////// Validate START ///////////////////
if(strlen($mfpCode) == 0 ){
	$isValid = false;
	$result[] = false;
}
if(strlen($groupName) == 0){
	$isValid = false;
	$result[] = false;
}
/////////////////// Validate END ///////////////////

/////////////////// Save START ///////////////////
if($isValid){
	$objPRINTING_GROUP = new PRINTING_GROUP($groupID);
	$objPRINTING_GROUP->setAcademicYearID( Get_Current_Academic_Year_ID() );
	$objPRINTING_GROUP->setMFP_CODE($mfpCode);
	$objPRINTING_GROUP->setNAME($groupName);
	$result[] = $objPRINTING_GROUP->save();
	
	$groupID = $objPRINTING_GROUP->getGROUP_ID();
	$result[] = PRINTING_GROUP_MEMBER::deleteAllMember($groupID, $cfg_msschPrint['group']['memberType']['PIC']);
	$result[] = PRINTING_GROUP_MEMBER::insertMember($groupID, $PIC, $cfg_msschPrint['group']['memberType']['PIC']);
	$result[] = PRINTING_GROUP_MEMBER::deleteAllMember($groupID, $cfg_msschPrint['group']['memberType']['Member']);
	$result[] = PRINTING_GROUP_MEMBER::insertMember($groupID, $Member, $cfg_msschPrint['group']['memberType']['Member']);
}
/////////////////// Save END ///////////////////


/////////////////// Redirect START ///////////////////
if(in_array(false,$result)) {
	if($actionType == 'edit'){
		$Msg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
	}else{
		$Msg = $Lang['General']['ReturnMessage']['AddUnsuccess'];
	}
}else{
	if($actionType == 'edit'){
		$Msg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
	}else{
		$Msg = $Lang['General']['ReturnMessage']['AddSuccess'];
	}
}
header('Location: ?t=setting.group&Msg='.$Msg);
exit;
?>