<?php
/**
 * Change Log:
 * 2016-11-07 Pun [107991]
 *  - Fix user can submit the form multiple times.
 */

//////////////////// SQL Safe START ////////////////////
$groupID = IntegerSafe($GroupCheckBox[0]);
//////////////////// SQL Safe END ////////////////////


//////////////////// Access Right START ////////////////////
if($groupID){ // Edit Group
	if(!libMsschPrintingAccessRight::checkGroupSettingAccessRight($groupID)){
		header('Location: /');
		exit;
	}
}else{ // New Group
//	if(!libMsschPrintingAccessRight::isSuperAdmin()){
	if(!libMsschPrintingAccessRight::checkGroupSettingAccessRight()){
		header('Location: /');
		exit;
	}
}
//////////////////// Access Right END ////////////////////


//////////////////// INIT START ////////////////////
$parentPagePath = 'setting.group';
$savePagePath = 'setting.group.editSave';
$checkMfpDuplicatePagePath = 'setting.group.ajax.checkDuplicateMfpCode';

$objPrintingGroup = new PRINTING_GROUP($groupID);
$mfpCode = $objPrintingGroup->getMFP_CODE();
$groupName = $objPrintingGroup->getNAME();

$fcm = new form_class_manage();
$TeacherList = $fcm->Get_Teaching_Staff_List();

$memberList = array();
if($groupID) {
    $picList = PRINTING_GROUP_MEMBER::getUserIDsByGroupIdMemberType($groupID, $cfg_msschPrint['group']['memberType']['PIC']);
    $memberIdList = PRINTING_GROUP_MEMBER::getUserIDsByGroupIdMemberType($groupID, $cfg_msschPrint['group']['memberType']['Member']);

    for ($i=0, $iMax=count($memberIdList); $i < $iMax; $i++) {
        $_name = getUserInfoByLang($memberIdList[$i]);
        $_name = $_name['UserName'];
        $memberList[] = array($memberIdList[$i], $_name);
    }
}else{
	$picList = array($_SESSION['UserID']);
}
//////////////////// INIT END ////////////////////

//////////////////// UI Field START ////////////////////
$picHTML = '<select id="TeacherList" onchange="Add_Teaching_Teacher();">
	<option value="" selected="selected">'.$Lang['SysMgr']['FormClassMapping']['AddTeacher'].'</option>';
for ($i=0, $iMax=count($TeacherList); $i < $iMax; $i++) {
	if(!in_array($TeacherList[$i]['UserID'], $picList)){
		$picHTML .= '<option value="'.$TeacherList[$i]['UserID'].'">'.$TeacherList[$i]['Name'].'</option>';
	}
}
$picHTML .= '</select><p class="spacer"></p>';
// tempory selection box to store class teacher
$picHTML .= '     <select name="PIC[]" size="3" id="SelectedClassTeacher[]" style="width:50%" multiple="true">';
for ($i=0, $iMax=count($picList); $i < $iMax; $i++) {
	$_name = getUserInfoByLang($picList[$i]);
	$_name = $_name['UserName'];
	$picHTML .= '<option value="'.$picList[$i].'">'.$_name.'</option>';
}
$picHTML .= '</select><p class="spacer"></p>';
$picHTML .= '<input onclick="Remove_Teaching_Teacher();" name="submit2" type="button" class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="'.$Lang['Btn']['RemoveSelected'].'"/>';


$thickboxPar = 'fieldname=r_group_user[]&page_title=SelectMembers&permitted_type=1,2&excluded_type=4&from_thickbox=true&KeepThis=true&TB_iframe=true&height=500&width=600';
$memberHTML = '<span style="float:left;">';
$memberHTML .= $linterface->GET_SELECTION_BOX($memberList, "name='r_group_user[]' id='r_group_user[]' class='select_studentlist' size='7' style='width:400px' multiple='multiple'", "");
$memberHTML .= '</span>';
$memberHTML .= $linterface->GET_BTN($button_select, "button", "javascript: $('#thickboxTest').click();");
$memberHTML .= '<br/>';
$memberHTML .= $linterface->GET_BTN($button_remove, "button", "javascript:checkOptionRemove(document.getElementById('r_group_user[]'))");
$memberHTML .= '<br/>';
$memberHTML .= '<a style="display:none;" id="thickboxTest" class="thickbox" href="/home/common_choose/index.php?'.$thickboxPar.'">test</a>';
$memberHTML .= '<span class="form_sep_title"><br style="clear:both;" />'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>';			
//////////////////// UI Field END ////////////////////

//////////////////// UI START ////////////////////
$CurrentPage = "SettingGroup";
$TAGS_OBJ[] = array($Lang['MsschPrint']['menu']['group'], '', 0);
$MODULE_OBJ = $objPrinting->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($Lang['MsschPrint']['menu']['group'], "?t={$parentPagePath}");
$PAGE_NAVIGATION[] = array( ($groupID)?$Lang['MsschPrint']['setting']['group']['editGroup']:$Lang['MsschPrint']['setting']['group']['newGroup'] , '');
$linterface->LAYOUT_START($Msg);
$c_ui = new common_ui();
$js_css = $c_ui->Include_JS_CSS();
echo $js_css;
?>
<style type="text/css">
	.errorMSG{ color: red; }
</style>



<div class="table_board">
	<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION)?>
	
<form id="form1" name="form1" method="post" action="index.php">
	<table class="form_table_v30">
	
		<tr>
			<td class="field_title"><?=$linterface->RequiredSymbol()?><?=$Lang['MsschPrint']['setting']['group']['edit']['mfpCode']?></td>
			<td>
				<input name="mfpCode" id="mfpCode" value="<?=$mfpCode?>" class="textboxtext"/><br>
				<span id='errMfpCodeEmpty' class="errorMSG" style="display:none"><?=$Lang['MsschPrint']['setting']['group']['editErr']['mfpCodeEmpty']?></span>
				<span id='errMfpCodeDuplicate' class="errorMSG" style="display:none"><?=$Lang['MsschPrint']['setting']['group']['editErr']['mfpCodeDuplicate']?></span>
			</td>
		</tr>
	
		<tr>
			<td class="field_title"><?=$linterface->RequiredSymbol()?><?=$Lang['MsschPrint']['setting']['group']['edit']['name']?></td>
			<td>
				<input name="groupName" id="groupName" value="<?=$groupName?>" class="textboxtext"/><br>
				<span id='errGroupName' class="errorMSG" style="display:none"><?=$Lang['MsschPrint']['setting']['group']['editErr']['groupNameEmpty']?></span>
			</td>
		</tr>
	
		<tr>
			<td class="field_title"><?=$Lang['MsschPrint']['setting']['group']['edit']['pic']?></td>
			<td>
				<?=$picHTML?>
				<span id='errRemoveSelf' class="errorMSG" style="display:none"><br /><?=$Lang['MsschPrint']['setting']['group']['editErr']['removeSelf']?></span>
			</td>
		</tr>
	
		<tr>
			<td class="field_title"><?=$Lang['MsschPrint']['setting']['group']['edit']['member']?></td>
			<td>
				<?=$memberHTML?>
			</td>
		</tr>
	
	</table>
	
	<?=$linterface->MandatoryField();?>

	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($button_submit, "button", "","submitForm") ?> 
		<?= $linterface->GET_ACTION_BTN($button_reset, "button", "\$('#form1')[0].reset();","reset2") ?> 
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php?t={$parentPagePath}'","cancelbtn") ?>
		<p class="spacer"></p>
	</div>
	
	<input type="hidden" name="groupID" value="<?=$groupID?>" />
</form>

</div>


<!-------------- Script START -------------->
<script>
	var isSubmitting = false;
	$('#submitForm').click(function(){
		if(isSubmitting){
			return false;
		}
		
		isSubmitting = true;
		$('.errorMSG').hide();
		var isValid = true;
		
		if( $.trim( $('#mfpCode').val() ) == ''){
			$('#errMfpCodeEmpty').show();
			isValid = false;
		}
		if( $.trim( $('#groupName').val() ) == ''){
			$('#errGroupName').show();
			isValid = false;
		}
		<?php
		/* Every teacher can remove self * /
		if(!libMsschPrintingAccessRight::isSuperAdmin()){ ?>
			var hasSelf = false;
			$('#SelectedClassTeacher\\[\\] > option').each(function(index, element){
				if(element.value == '<?=$_SESSION['UserID']?>'){
					hasSelf = true;
					return false;
				}
			});
			if(!hasSelf){
				$('#errRemoveSelf').show();
				isValid = false;
			}
		<?php 
		}
		/**/ 
		?>
		
		$('#SelectedClassTeacher\\[\\] > option').attr("selected", "selected");
		$('#r_group_user\\[\\] > option').attr("selected", "selected");
		
		$.get('?t=<?=$checkMfpDuplicatePagePath?>', {
			'groupID': '<?=$groupID?>',
			'mfpCode': $('#mfpCode').val()
		}, function(res){
			if(res == '1'){
				if(isValid){
					$('#form1').attr('action','?t=<?=$savePagePath?>').submit();
				}else{
            		isSubmitting = false;
				}
			}else{
				$('#errMfpCodeDuplicate').show();
        		isSubmitting = false;
			}
		});
	});
</script>



<script>
function Add_Teaching_Teacher(TeacherID) {
	var TeacherList = document.getElementById('TeacherList');
	var Teacher = TeacherList.options[TeacherList.selectedIndex];
	var TeachingStaffList = document.getElementById('SelectedClassTeacher[]');
	
	if (Teacher.value != "") {
		TeachingStaffList.options[TeachingStaffList.length] = new Option(Teacher.text,Teacher.value);
		
		TeacherList.options[TeacherList.selectedIndex] = null;
		
		Reorder_Selection_List('SelectedClassTeacher[]');
	}
}

function Reorder_Selection_List(selectId) {
	var selectList = document.getElementById(selectId);
	for (var i = 0; i < selectList.length; i++) {
		for (var j=i; j < selectList.length; j++) {
			if (selectList.options[i].text.toLowerCase() > selectList.options[j].text.toLowerCase()) {
				var tempOption1 = new Option(selectList.options[i].text,selectList.options[i].value);
				var tempOption2 = new Option(selectList.options[j].text,selectList.options[j].value);
				selectList.options[i] = tempOption2;
				selectList.options[j] = tempOption1;
			}
		}
		//alert(selectList.options[i].text);
		/*if (opt.selected) {
			selectList.removeChild(opt);
			selectList.insertBefore(opt, selectOptions[i - 1]);
		}*/
  }
}

function Remove_Teaching_Teacher() {
	var TeachingStaffList = document.getElementById('SelectedClassTeacher[]');
	var TeacherList = document.getElementById('TeacherList');
	
	for (var i = (TeachingStaffList.length -1); i >= 0 ; i--) {
		if (TeachingStaffList.options[i].selected) {
			Teacher = TeachingStaffList.options[i];
			TeacherList.options[TeacherList.length] = new Option(Teacher.text,Teacher.value);
			TeachingStaffList.options[i] = null;	
		}
	}

	TeacherList.options[0].selected = true;
	
	Reorder_Selection_List('TeacherList');
}
</script>

<script type="text/javascript">

function addFilter(){
	var filter = [];
	$('body').find('#TB_window iframe').load(function() {
		var form = $(this).contents().find('form');
		$.each(filter,function(key,value){
			form.append('<input type="hidden" name="filterUser[]" value="'+value+'" />');
		});
	});
}
</script>
<!-------------- Script END -------------->


<?php $linterface->LAYOUT_STOP(); ?>