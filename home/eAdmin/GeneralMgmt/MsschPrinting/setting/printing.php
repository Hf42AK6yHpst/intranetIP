<?php
######################## Initialization START ########################
$AcademicYearID = ($AcademicYearID) ? $AcademicYearID : Get_Current_Academic_Year_ID();
$AcademicYearID = IntegerSafe($AcademicYearID);
######################## Initialization END ########################

///////////////// Access Right START /////////////////
if (!libMsschPrintingAccessRight::isSuperAdmin()) {
    header('Location: /');
    exit;
}
///////////////// Access Right END /////////////////

######################## PHP Header START ########################
$CurrentPage = "SettingPrinting";
$TAGS_OBJ[] = array($Lang['MsschPrint']['menu']['printSetting'], '', 0);
$MODULE_OBJ = $objPrinting->GET_MODULE_OBJ_ARR();
######################## PHP Header END ########################

######################## Get Data START ########################
$yearMonthArr = $objPrinting->getYearMonthArr($AcademicYearID);

$rs = $objPrinting->getCostByAcademicYearId($AcademicYearID);
foreach ($rs as $index => $r) {
    $rs[$index]['Cost'] = number_format($r['Cost'], $cfg_msschPrint['moneyFormat']['decimals']);
}

$costArr = BuildMultiKeyAssoc($rs, array('YearMonth', 'Type'), 'Cost', true);
foreach ($yearMonthArr as $yearMonth) {
    $costArr[$yearMonth]['a3Price'] = (isset($costArr[$yearMonth]['a3Price'])) ? $costArr[$yearMonth]['a3Price'] : '';
    $costArr[$yearMonth]['a4Price'] = (isset($costArr[$yearMonth]['a4Price'])) ? $costArr[$yearMonth]['a4Price'] : '';
    $costArr[$yearMonth]['colorBW'] = (isset($costArr[$yearMonth]['colorBW'])) ? $costArr[$yearMonth]['colorBW'] : '';
    $costArr[$yearMonth]['colorFc'] = (isset($costArr[$yearMonth]['colorFc'])) ? $costArr[$yearMonth]['colorFc'] : '';
}

######################## Get Data END ########################

$linterface->LAYOUT_START($Msg);
?>
<style>
    .table_board table {
        margin: 0 auto;
    }

    .field_title {
        border-bottom: 1px solid #FFFFFF;
        width: 200px;
        background: #f3f3f3;
        padding-left: 2px;
        padding-right: 2px;
        vertical-align: top;
        text-align: left;
        line-height: 18px;
        padding-top: 5px;
        padding-bottom: 5px;
    }

    .header {
        width: 100px;
        background-color: #A6A6A6;
        white-space: nowrap;
    }

    .table_board table th, .table_board table td {
        text-align: center;
        border-bottom: 1px solid #CCCCCC;
    }

    .table_board td input {
        text-align: center;
    }
</style>
<form name="form1" id="form1" method="post" action="?t=setting.printing.editSave">
    <div nowrap="nowrap" class="table_filter" style="padding: 7px;">
        <?= $Lang['General']['AcademicYear'] ?>:
        <?= getSelectAcademicYear('AcademicYearID', '', 1, 1, $AcademicYearID, 0) ?>
    </div>


    <div class="table_board">
        <table border="0" cellpadding="5" cellspacing="0">
            <thead>
            <tr>
                <th>&nbsp;</th>
                <?php foreach ($yearMonthArr as $yearMonth) : ?>
                    <th class="header">
                        <?= $yearMonth ?>
                    </th>
                <?php endforeach; ?>
            </tr>
            </thead>
            <tbody>
            <!-- Start Row //-->
            <tr>
                <td class="field_title"><?= $Lang['MsschPrint']['setting']['print']['ColorBwPrice'] ?></td>
                <?php foreach ($yearMonthArr as $i => $yearMonth) : ?>
                    <td>
                        <input
                                name="colorBW[<?= $yearMonth ?>]"
                                value="<?= $costArr[$yearMonth]['colorBW'] ?>"
                                size="6"
                                maxlength="6"
                                tabindex="<?= $i * 4 + 1 ?>"
                        />
                    </td>
                <?php endforeach; ?>
            </tr>
            <!-- End Row //-->
            <!-- Start Row //-->
            <tr>
                <td class="field_title"><?= $Lang['MsschPrint']['setting']['print']['ColorFcPrice'] ?></td>
                <?php foreach ($yearMonthArr as $i => $yearMonth) : ?>
                    <td>
                        <input
                                name="colorFc[<?= $yearMonth ?>]"
                                value="<?= $costArr[$yearMonth]['colorFc'] ?>"
                                size="6"
                                maxlength="6"
                                tabindex="<?= $i * 4 + 2 ?>"
                        />
                    </td>
                <?php endforeach; ?>
            </tr>
            <!-- End Row //-->
            <!-- Start Row //-->
            <tr>
                <td class="field_title"><?= $Lang['MsschPrint']['setting']['print']['A4Price'] ?></td>
                <?php foreach ($yearMonthArr as $i => $yearMonth) : ?>
                    <td>
                        <input
                                name="a4Price[<?= $yearMonth ?>]"
                                value="<?= $costArr[$yearMonth]['a4Price'] ?>"
                                size="6"
                                maxlength="6"
                                tabindex="<?= $i * 4 + 3 ?>"
                        />
                    </td>
                <?php endforeach; ?>
            </tr>
            <!-- End Row //-->
            <!-- Start Row //-->
            <tr>
                <td class="field_title"><?= $Lang['MsschPrint']['setting']['print']['A3Price'] ?></td>
                <?php foreach ($yearMonthArr as $i => $yearMonth) : ?>
                    <td>
                        <input
                                name="a3Price[<?= $yearMonth ?>]"
                                value="<?= $costArr[$yearMonth]['a3Price'] ?>"
                                size="6"
                                maxlength="6"
                                tabindex="<?= $i * 4 + 4 ?>"
                        />
                    </td>
                <?php endforeach; ?>
            </tr>
            <!-- End Row //-->
            <!-- Start Row //-->
            <tr>
                <td class="field_title"><?= $Lang['MsschPrint']['setting']['print']['OtherPrice'] ?></td>
                <td colspan="12">
                    <?= $Lang['MsschPrint']['setting']['print']['OtherPriceRemarks'] ?>
                </td>
            </tr>
            <!-- End Row //-->

            </tbody>
        </table>
        <span class="tabletextremark" style="margin: 5px;display: block;">
            <span class="tabletextrequire">*</span> <?= $Lang['MsschPrint']['setting']['print']['ReImportRemarks'] ?>
        </span>
    </div>

    <div class="edit_bottom_v30">
        <p class="spacer"></p>
        <?= $linterface->GET_ACTION_BTN($button_submit, "button", "", "submitForm") ?>
        <p class="spacer"></p>
    </div>

</form>

<script>
    $('#AcademicYearID').change(function () {
        $('#form1').attr('action', '?t=setting.printing').submit();
    });
    $('#submitForm').click(function () {
        $('#form1').submit();
    });
</script>

<?php $linterface->LAYOUT_STOP(); ?>
