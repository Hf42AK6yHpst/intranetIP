<?php

/////////////////// SQL Safe START ///////////////////
$_ = array();
foreach($RecordIdCheckBox as $id){
    $_[] = intranet_htmlspecialchars(trim($id));
}
$RecordIdCheckBox = $_;
/////////////////// SQL Safe END ///////////////////


///////////////// Access Right START /////////////////
if(!libMsschPrintingAccessRight::checkPrintImportAccessRight()){
	header('Location: /');
	exit;
}
///////////////// Access Right END /////////////////


/////////////////// INIT START ///////////////////
$result = array();
$isValid = true;
/////////////////// INIT END ///////////////////

/////////////////// Validate START ///////////////////
if(empty($RecordIdCheckBox)){
	$isValid = false;
	$result[] = false;
}
foreach($RecordIdCheckBox as $id){
    $_ = explode('_', $id);
    $yearMonth = $_[0];
    $groupId = $_[1];
    if(!preg_match('/^\d{4}-\d{2}$/', $yearMonth)){
        $isValid = false;
        $result[] = false;
    }
    if(IntegerSafe($groupId) != $groupId){
        $isValid = false;
        $result[] = false;
    }
}
/////////////////// Validate END ///////////////////

/////////////////// Save START ///////////////////
if($isValid){
    $whereArr = array();
    foreach($RecordIdCheckBox as $id) {
        $_ = explode('_', $id);
        $yearMonth = $_[0];
        $groupId = $_[1];

        $whereArr[] = "('{$yearMonth}','{$groupId}')";
    }
    $whereSql = implode(',',$whereArr);

    $sql = "DELETE FROM 
        PRINTING_MEMBER_USAGE_V2
    WHERE
        ( DATE_FORMAT(RECORD_DATE, '%Y-%m'), GROUP_ID )
    IN 
        ({$whereSql})";
    $result[] = $objPrinting->db_db_query($sql);
}
/////////////////// Save END ///////////////////

/////////////////// Redirect START ///////////////////
if(in_array(false,$result)) {
	$Msg = $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
}else{
	$Msg = $Lang['General']['ReturnMessage']['DeleteSuccess'];}
header('Location: ?t=management.print&Msg='.$Msg);
exit;
?>