<?php
// modify :
/**
 * Change Log:
 * 2020-08-26 (Pun) [193753]
 *  - Added support for import last year (hidden function)
 */
include_once($PATH_WRT_ROOT . "includes/cust/MSSCH_printing/libMsschPrintingDataImport_v2.php");

// error_reporting(E_ALL & ~E_NOTICE);ini_set('display_errors', 1);
/////////////////// Access Right START ///////////////////
if (!libMsschPrintingAccessRight::checkPrintImportAccessRight()) {
    header('Location: /');
    exit;
}
/////////////////// Access Right END ///////////////////


/////////////////// Init START ///////////////////
$libimport = new libimporttext();
$libfs = new libfilesystem();
$objImport = new libMsschPrintingDataImport();

$name = $_FILES['csvfile']['name'];
$ext = strtoupper($libfs->file_ext($name));

$selectAcademicYear = IntegerSafe($selectAcademicYear);

$nextStepPath = 'management.printImport_v2_3';
$lastStepPath = 'management.printImport_v2_1';
/////////////////// Init END ///////////////////

/////////////////// Get cost START ///////////////////
$rs = $objPrinting->getCostByAcademicYearId($selectAcademicYear);
$costArr = BuildMultiKeyAssoc($rs, array('YearMonth', 'Type'), 'Cost', true);
$costArr = $costArr[$_POST['selectMonth']];

$hasCost = (
    $costArr['a3Price'] != '' &&
    $costArr['a4Price'] != '' &&
    $costArr['colorBW'] != '' &&
    $costArr['colorFc'] != ''
);
/////////////////// Get cost END ///////////////////

/////////////////// Validate START ///////////////////
if (!($ext == ".CSV") && !($ext == ".TXT")) {
    header('Location: /');
    exit();
}
/////////////////// Validate END ///////////////////


/////////////////// Import START ///////////////////
#######################################################
### move to temp folder first for others validation ###
#######################################################
$TargetFilePath = $libfs->Copy_Import_File_To_Temp_Folder($cfg_msschPrint['importTempFolder'], $csvfile, $name);


##################################
### Validate file and Read data ##
##################################
$data = $objImport->ReadImportData($TargetFilePath, $selectAcademicYear);

$passCount = 0;
if ($data === NULL) { // Files contains blank lines.

} else {
    foreach ($data['Data'] as $d) {
        if ($d['pass']) {
            $passCount++;
        }
    }
}
$failCount = $data['NumOfData'] - $passCount;
/////////////////// Import END ///////////////////


/////////////////// Get group name START ///////////////////
$mfpCodeArr = array();
for ($i = 0, $iMax = $data['NumOfData']; $i < $iMax; $i++) {
    $mfpCode = trim(htmlentities($data['Data'][$i]['rawData']['MfpCode'], ENT_QUOTES, 'UTF-8'));
    $mfpCodeArr[] = $mfpCode;
}
$mfpCodeList = implode("','", $mfpCodeArr);
$sql = "SELECT
	MFP_CODE,
	NAME
FROM 
	PRINTING_GROUP
WHERE
	MFP_CODE IN ('{$mfpCodeList}')
AND
	AcademicYearID = '{$selectAcademicYear}'";
$rs = $objDB->returnResultSet($sql);
$nameArr = BuildMultiKeyAssoc($rs, array('MFP_CODE'), $IncludedDBField = array('NAME'), $SingleValue = 1);
/////////////////// Get group name END ///////////////////


//////////////////// UI START ////////////////////
$h_stepUI = $linterface->GET_IMPORT_STEPS($CurrStep = 2, $CustStepArr = '');
$Title = $Lang['MsschPrint']['general']['ImportData'];
$PAGE_NAVIGATION[] = array($Title, "");
$CurrentPage = "ManagementPrint";
$TAGS_OBJ[] = array($Lang['MsschPrint']['menu']['print'], "", 0);
$MODULE_OBJ = $objPrinting->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Msg);
?>
<style>
    .orange{
        color: darkorange;
    }
</style>
    <form method="POST" name="frm1" id="frm1" action="index.php" enctype="multipart/form-data">
        <table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
            <tr>
                <td align='right'><?= $linterface->GET_SYS_MSG("", $xmsg); ?></td>
            </tr>
            <tr>
                <td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
            </tr>
            <tr>
                <td><?= $h_stepUI; ?></td>
            </tr>
            <?php if ($data !== NULL) { ?>
                <tr>
                    <td>
                        <table width="20%" border="0" cellspacing="0" cellpadding="0" align="center">
                            <tr>
                                <td class='formfieldtitle'><?= $Lang['MsschPrint']['management']['print']['importMonth'] ?>
                                    :
                                </td>
                                <td class='tabletext'><?= $_POST['selectMonth'] ?></td>
                            </tr>
                            <tr>
                                <td class='formfieldtitle'><?= $Lang['Btn']['Import'] ?>:</td>
                                <td class='tabletext'><?= $Lang['MsschPrint']['management']['print']['importRecord'] ?></td>
                            </tr>
                            <tr>
                                <td class='formfieldtitle'><?= $Lang['General']['SuccessfulRecord'] ?>:</td>
                                <td class='tabletext'><?= $passCount ?></td>
                            </tr>
                            <tr>
                                <td class='formfieldtitle'><?= $Lang['General']['FailureRecord'] ?>:</td>
                                <td class='tabletext <?php if ($failCount > 0) print('red'); ?>'><?= $failCount ?></td>
                            </tr>
                            <tr>
                                <td class='formfieldtitle'><?= $Lang['MsschPrint']['management']['print']['importSkippedRecord'] ?>:</td>
                                <td class='tabletext <?php if ($data['NumOfEmptyMfpData'] > 0) print('orange'); ?>'><?= $data['NumOfEmptyMfpData'] ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="90%" border="0" cellspacing="0" cellpadding="2" align="center">
                            <tr>
                                <th class='tablebluetop tabletopnolink' width="1%">#</th>

                                <!-- --------------------- Table Header START --------------------- -->
                                <th class="tablebluetop tabletopnolink"><?= $Lang['General']['Date'] ?></th>
                                <th class="tablebluetop tabletopnolink"><?= $Lang['MsschPrint']['management']['print']['importRemarks']['mfpCode'] ?></th>
                                <th class="tablebluetop tabletopnolink"><?= $Lang['MsschPrint']['management']['print']['tableHeader']['groupName'] ?></th>
                                <th class="tablebluetop tabletopnolink"><?= $Lang['MsschPrint']['management']['print']['tableHeader']['colorMode'] ?></th>
                                <th class="tablebluetop tabletopnolink"><?= $Lang['MsschPrint']['management']['print']['tableHeader']['pages'] ?></th>
                                <th class="tablebluetop tabletopnolink"><?= $Lang['MsschPrint']['management']['print']['tableHeader']['sheet'] ?></th>
                                <th class="tablebluetop tabletopnolink"><?= $Lang['MsschPrint']['management']['print']['tableHeader']['paperSize'] ?></th>

                                <!-- --------------------- Table Header END --------------------- -->

                                <th class="tablebluetop tabletopnolink"><?= $Lang['MsschPrint']['general']['import']['dataChecking'] ?></th>
                                <?php if ($failCount > 0) { ?>
                                    <th class="tablebluetop tabletopnolink"><?= $Lang['MsschPrint']['general']['import']['reason'] ?></th>
                                <?php } ?>
                            </tr>

                            <?php
                            $index = 1;
                            foreach($data['Data'] as $i=>$d) {
                                if (empty($d)) {
                                    continue;
                                }
                                ?>
                                <tr class='tablebluerow<?= (($index++) % 2 + 1) ?>'>
                                    <td class='tabletext' valign='top'><?= ($i + 1) ?></td>

                                    <!-- --------------------- Table Content START --------------------- -->
                                    <td class='tabletext'
                                        valign='top'><?= $d['rawData']['Date'] ?></td>
                                    <td class='tabletext'
                                        valign='top'><?= $d['rawData']['MfpCode'] ?></td>
                                    <td class='tabletext'
                                        valign='top'><?= $nameArr[$d['rawData']['MfpCode']] ?></td>
                                    <td class='tabletext'
                                        valign='top'><?= $d['rawData']['ColorMode'] ?></td>
                                    <td class='tabletext' valign='top'><?= $d['rawData']['Pages'] ?></td>
                                    <td class='tabletext' valign='top'><?= $d['rawData']['Sheet'] ?></td>
                                    <td class='tabletext'
                                        valign='top'><?= $d['rawData']['PaperSize'] ?></td>
                                    <!-- --------------------- Table Content END --------------------- -->

                                    <td class='tabletext'
                                        valign='top'><?= ($d['pass']) ? $linterface->Get_Tick_Image() : '<span class="table_row_tool"><a class="delete"/></span>' ?></td>
                                    <?php if ($failCount > 0) { ?>
                                        <td class='tabletext'
                                            valign='top'><?= $d["rawData"]["Error"] ?></td>
                                    <?php } ?>
                                </tr>
                                <?php
                            } //end foreach
                            ?>
                        </table>
                    </td>
                </tr>
                <?php
            } else {
                echo '<tr><td><p align="center">' . $Lang['General']['NoRecordFound'] . '</p></td></tr>';
            }
            ?>
            <tr>
                <td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"/>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <input type="hidden" name="t" value="<?= $nextStepPath ?>"/>
                    <input type="hidden" name="TargetFilePath" value="<?= $TargetFilePath ?>"/>

                    <?php if ($hasCost): ?>
                        <?= ($failCount == 0) ? $linterface->GET_ACTION_BTN($Lang['Btn']['Import'], "submit", "", "submit2", " class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") : "" ?>
                    <?php else: ?>
                        <span style="color: red;display: block;"><?=$Lang['MsschPrint']['management']['print']['importError']['missingCostSetting']?></span>
                    <?php endif; ?>
                    &nbsp;
                    <?= $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='?t={$lastStepPath}'", "back_btn", " class='formbutton' ") ?>
                </td>
            </tr>
        </table>

        <input type="hidden" name="selectAcademicYear" value="<?= $selectAcademicYear ?>"/>
        <input type="hidden" name="selectMonth" value="<?= $_POST['selectMonth'] ?>"/>
    </form>

<?php
$linterface->LAYOUT_STOP();
?>
