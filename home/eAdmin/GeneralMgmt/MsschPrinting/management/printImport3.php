<?php
// modify : 
include_once($PATH_WRT_ROOT."includes/cust/MSSCH_printing/libMsschPrintingDataImport.php");


/////////////////// Access Right START ///////////////////
if(!libMsschPrintingAccessRight::checkPrintImportAccessRight()){
	header('Location: /');
	exit;
}
/////////////////// Access Right END ///////////////////


/////////////////// Init START ///////////////////
$error = array();
$result = array();
$objImport = new libMsschPrintingDataImport();
$data = $objImport->ReadImportData($TargetFilePath);

$nrSuccess = 0;	// number of successfully import record

$parentPath = 'management.print';
/////////////////// Init END ///////////////////


/////////////////// Import START ///////////////////
if ($data != false)
{
	if ($data["HeaderError"] == true)
	{
		$error[] = "Wrong Header";
	}	
	else if ($data["NumOfData"] == 0)
	{
		$error[] = "No record to import";
	}
	else	// Import Data exists
	{
		$rs = $data["Data"];

		for ($i = 0, $iMax = count($rs); $i < $iMax; $i++)
		{			
			if ($rs[$i]["pass"])
			{
				$crs = $rs[$i]["rawData"];

				$date 		= $crs['Date'];
				$mfpCode	= $crs['MfpCode'];
				$amount		= $crs['Amount'];

				$recordID = PRINTING_GROUP_USAGE::insertUsage($date, $mfpCode, $amount);
				if($recordID === false){
					$result[] = false;
				}else{
					$groupID = $objPrinting->getGroupIdByMfpCode($mfpCode);
					$totalMember = PRINTING_GROUP_MEMBER::countGroupMember($groupID, $cfg_msschPrint['group']['memberType']['Member']);
					if($totalMember == 0){
						$result[] = false;
					}else{
						$amount = (float)$amount / $totalMember;
//						$academicYearID = getAcademicYearIdByDate($date); # cust/common_function.php
						$academicYearID = Get_Current_Academic_Year_ID(); # Only import current year data
						$result[] = PRINTING_MEMBER_USAGE::insertUsage($recordID, $academicYearID, $amount);
						$nrSuccess++;
					}
				}
			}
		} // End For
	}
}else{
	$error[] = "False csv data";
}


if(in_array(false, $result)){
	$Msg = $Lang['General']['ReturnMessage']['ImportUnsuccess'];		
}else{
	$Msg = $Lang['General']['ReturnMessage']['ImportSuccess'];
}


//////////////////// UI START ////////////////////
$h_stepUI = $linterface->GET_IMPORT_STEPS($CurrStep=3, $CustStepArr='');
$Title = $Lang['MsschPrint']['general']['ImportData'];
$PAGE_NAVIGATION[] = array($Title,"");
$CurrentPage = "ManagementPrint";
$TAGS_OBJ[] = array($Lang['MsschPrint']['menu']['print'], "", 0);
$MODULE_OBJ = $objPrinting->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Msg);
?>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align='right'><?=$linterface->GET_SYS_MSG("",$xmsg);?></td>
	</tr>
	<tr>
		<td ><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
		<td><?=$h_stepUI;?></td>
	</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr height="20"><td>&nbsp;</td></tr>				
	<tr>
		<td align='center'>
	<?= $nrSuccess ."&nbsp;". $Lang['General']['ImportArr']['RecordsImportedSuccessfully']?>
		</td>
	</tr>
	<tr height="20"><td>&nbsp;</td></tr>
	<tr>
	    <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	</tr>

	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	            <tr>
					<td align="center">
					<br/>				
						<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='?t={$parentPath}'","back_btn"," class='formbutton' "); ?>				
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>


<?php
$linterface->LAYOUT_STOP();
?>