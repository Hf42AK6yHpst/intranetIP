<?php
// kenneth chung
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_opendb();

$libpos_ui = new libpos_ui();

$StudentID = trim($_REQUEST['StudentID']);
$FromDate = trim($_REQUEST['FromDate']);
$ToDate = trim($_REQUEST['ToDate']);
	 
echo $libpos_ui->Get_POS_Student_Report_Detail($FromDate, $ToDate, $StudentID); 	 

intranet_closedb();
?>
