<?php
// Editing by 
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit();
}

$libpos_ui = new libpos_ui();
$CurrentPageArr['ePOS'] = 1;	// Top Menu
$CurrentPage['ReportStudent'] = 1;	// Left Menu

$linterface = new interface_html();

# tag information
$TAGS_OBJ[] = array($Lang['ePOS']['POSStudentReport'], '', 0);

$MODULE_OBJ = $libpos_ui->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$Keyword = trim(stripslashes(urldecode($_REQUEST['keyword'])));
$ClassName = trim(stripslashes(urldecode($_REQUEST['ClassName'])));

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

echo $libpos_ui->Get_POS_Student_Report_Layout($field, $order, $pageNo, $page_size,$FromDate,$ToDate,$Keyword,$ClassName);

?>
<script language="javascript">
function checkForm(formObj){
	if(formObj==null)return false;
	
	fromV = formObj.FromDate;
	toV= formObj.ToDate;
	if(!checkDate(fromV)){
	//formObj.FromDate.focus();
		return false;
	}
	else if(!checkDate(toV)){
		//formObj.ToDate.focus();
		return false;
	}
	return true;
}
function checkDate(obj){
	if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
	return true;
}
function submitForm(obj){
	if(checkForm(obj))
	obj.submit();
}
function exportPage(obj,url){
	old_url = obj.action;
	old_method= obj.method;
	obj.action=url;
	obj.method = "get";
	obj.submit();
	obj.action = old_url;
	obj.method = old_method;
}

function openPrintPage(){
	var FormObj = document.getElementById('form1');
	
	old_url = FormObj.action;
	old_target = FormObj.target;
  FormObj.action="print.php";
  FormObj.target="_blank";
  FormObj.submit();
  FormObj.action = old_url;
  FormObj.target= old_target;
}

function exportPage(obj,url){
  old_url = obj.action;
  obj.action=url;
  obj.submit();
  obj.action = old_url;
}

function Get_Student_Detail(StudentID) {
	var FormObj = document.getElementById('form1');
	
	old_url = FormObj.action;
	old_target = FormObj.target;
  FormObj.action='detail.php?StudentID='+encodeURIComponent(StudentID);
  FormObj.target="_blank";
  FormObj.method="POST";
  FormObj.submit();
  FormObj.method="GET";
  FormObj.action = old_url;
  FormObj.target= old_target;	
}

$(document).ready( function() {
	$('input#keyword').focus().keyup(function(e){
		var keynum = Get_KeyNum(e);
		
		if (keynum==13) {// keynum==13 => Enter
			document.form1.pageNo.value=1;
			document.form1.submit();
		}
	});
});
</script>

<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
