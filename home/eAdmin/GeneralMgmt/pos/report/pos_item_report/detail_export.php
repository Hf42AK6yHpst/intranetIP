<?php
// Editing by 
/*
 * 2020-01-03 (Henry) : bug fixed for the blank page when export (Case#D177723)
 * 2014-10-17 (Carlos) [ip2.5.5.10.1] : created
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libpos_category.php");
include_once($PATH_WRT_ROOT."includes/libpos_item.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit();
}

$libpos = new libpos();
$libpos_ui = new libpos_ui();

$ItemID = trim(urldecode($_REQUEST['ItemID']));
//$ItemName = trim(urldecode($_REQUEST['ItemName']));
$FromDate = trim(urldecode($_REQUEST['FromDate']));
$ToDate = trim(urldecode($_REQUEST['ToDate']));

$itemObj = new POS_Item($ItemID, true);
$lexport = new libexporttext();

$header = array($Lang['ePOS']['TransactionTime'],
				$Lang['ePayment']['InvoiceNumber'],
				$Lang['General']['Class'],
				$Lang['General']['ClassNumber'],
				$Lang['ePOS']['UserName'],
				$Lang['ePayment']['Quantity'],
				$Lang['ePayment']['UnitPrice'],
				$Lang['ePayment']['GrandTotal']);
$rows = array();

$result = $libpos->Get_POS_Item_Report_Detail_Data($FromDate, $ToDate, $itemObj->ItemName, $itemObj->ItemID);
$result_count = count($result);

for ($i=0; $i< $result_count;$i++) {
	//$total += $result[$i]['GrandTotal'];
	$row = array(
			$result[$i]['ProcessDate'],
			$result[$i]['InvoiceNumber'],
			$result[$i]['ClassName'],
			$result[$i]['ClassNumber'],
			$result[$i]['UserName'],
			$result[$i]['ItemQty'],
			$libpos_ui->CurrencySign.$result[$i]['ItemSubTotal'],
			$libpos_ui->CurrencySign.$result[$i]['GrandTotal']
			);
	$rows[] = $row;
}

$filename = $itemObj->CategoryName." - ".$itemObj->ItemName." (".$FromDate." ".$Lang['ePOS']['To']." ".$ToDate.").csv";
$export_content = $lexport->GET_EXPORT_TXT($rows, $header,"","\r\n","",0,"11");
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>