<?
// Using : 
/*
 * 2019-01-03 (Henry): file created
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libpos_item.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit();
}

$libPOS_ui = new libpos_ui();
$CurrentPageArr['ePOS'] = 1;	// Top Menu
$CurrentPage['ReportTransaction'] = 1;	// Left Menu

# tag information
$TAGS_OBJ[] = array($Lang['ePOS']['POSTransactionReport'], '', 1);
$TAGS_OBJ[] = array($Lang['ePOS']['POSCancelTransactionReport'], 'voidindex.php', 0);

$MODULE_OBJ = $libPOS_ui->GET_MODULE_OBJ_ARR();
$linterface = new interface_html();
$ReturnMsg = $Lang['General']['ReturnMessage'][$xmsg];
$linterface->LAYOUT_START($ReturnMsg);

$LogID = trim($_REQUEST['LogID']);
$FromDate = trim($_REQUEST['FromDate']);
$ToDate = trim($_REQUEST['ToDate']);

$pageNo = $_REQUEST['pageNo'];
$order = $_REQUEST['order'];
$field = $_REQUEST['field'];
$PageSize = $_REQUEST['num_per_page'];

echo $libPOS_ui->Get_Report_Pickup_Detail($LogID, $FromDate, $ToDate,$void, $pageNo, $PageSize, $order, $field);

?>


<script>

$(document).ready( function() {
	$('input#Keyword').focus().keyup(function(e){
		var keynum = Get_KeyNum(e);
		
		if (keynum==13) // keynum==13 => Enter
			js_Reload_Report_Table();
	});
});

function js_Pickup_All(){
	if(confirm("你是否確定此訂單的貨品全部領取？")){
		var objForm = document.getElementById('form1');
		objForm.action = 'pickup_item_update.php';
		objForm.target = '_self';
		objForm.submit();
	}
}

function returnCheckedElements(obj, element_name){
        len=obj.elements.length;
        var returnString='';
        var i=0;
        for( i=0 ; i<len ; i++) {
                if (obj.elements[i].name==element_name && obj.elements[i].checked)
				{
					//alert(obj.elements[i].value);
                returnString+= obj.elements[i].value+',';
				}
        }
        return returnString.slice(0, -1);
}

var elementString;
var url;
var logType;
function checkItem(obj,element,page,type){
	var obj=document.form1;
	if(countChecked(obj,element)==0){
    	alert(globalAlertMsg2);
	}
    else{
    	url = page;
    	elementString = returnCheckedElements(obj,element);
    	var title = '';
    	if(type == 'Change'){
    		title = '<?=$Lang['ePOS']['ChangeItem']?>';
    	}
    	else if(type == 'Void'){
    		title = '<?=$Lang['ePOS']['WriteoffItem']?>';
    	}
    	else if(type == 'Pickup'){
    		title = '<?=$Lang['ePOS']['Take']?>';
    	}
    	if(element == 'CompleteItemID[]'){
    		logType = 'Complete';
    	}
    	else if(element == 'NotCompleteItemID[]'){
    		logType = 'NotComplete';
    	}
    	
    	load_dyn_size_thickbox_ip(title, 'onloadThickBox();', inlineID='', defaultHeight=450, defaultWidth=780);
    }
}

function onloadThickBox() {
	$('div#TB_ajaxContent').load(
		url, 
		{ 
			ItemIDs : elementString,
			LogID : <?=$LogID?>,
			LogType : logType
		},
		function(ReturnData) {
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	);
}

function js_Back_To_Report_Index()
{
	var objForm = document.getElementById('form1');
	objForm.action = 'index.php?FromLog=1';
	objForm.target = '_self';
	objForm.submit();
}

function js_Go_Print()
{
	var objForm = document.getElementById('form1');
	objForm.action = '../../report/pos_transaction_report/detail.php?LogID=<?=$LogID?>';
	objForm.target = '_blank';
	objForm.submit();
	
	// restore submit settings
	objForm.action = 'log_view.php';
	objForm.target = '_self';
}

function Get_Void_Transaction_Form(TransactionLogID) {
	var PostVar = {
		"TransactionLogID":TransactionLogID
	};
	
	$.post('../transaction/ajax_get_void_transaction_form.php',PostVar,
		function(data){
			if (data == "die") 
				window.top.location = '/';
			else {
				$('div#TB_ajaxContent').html(data);
			}
		});
}
</script>

<br /> 
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>