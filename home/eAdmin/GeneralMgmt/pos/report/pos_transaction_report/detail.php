<?php
// kenneth chung
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_opendb();

$libpos_ui = new libpos_ui();

$LogID = trim($_REQUEST['LogID']);
$FromDate = trim($_REQUEST['FromDate']);
$ToDate = trim($_REQUEST['ToDate']);


echo $libpos_ui->Get_POS_Transaction_Log_Detail($LogID, $FromDate, $ToDate,$void);

intranet_closedb();
?>
