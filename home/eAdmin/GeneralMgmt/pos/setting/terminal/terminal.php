<?
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$libPOS_ui = new libpos_ui();
$linterface = new interface_html();
$CurrentPageArr['ePOS'] = 1;	// Top Menu
$CurrentPage['Terminal'] = 1;	// Left Menu

# tag information
$TAGS_OBJ[] = array($Lang['ePOS']['GeneralTerminalSetting'], 'terminal.php', 1);
$TAGS_OBJ[] = array($Lang['ePOS']['TerminalManagementSetting'], 'terminal_manage.php', 0);

# Get Return Message
$msg = $_REQUEST['msg'];
	
$MODULE_OBJ = $libPOS_ui->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($msg));

echo $libPOS_ui->Get_Teminal_Setting_Form();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>