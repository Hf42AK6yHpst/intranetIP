<?php
// Editing by 
/*
 * 2018-02-06 (Carlos): Added setting [ConfirmBeforePay]
 * 2014-01-23 (Carlos): Added setting [PaymentMethod]
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$GeneralSetting = new libgeneralsettings();

### Build Settings Array
$SettingArr = array();

# Allow Client Program Connect
$SettingArr['AllowClientProgramConnect'] = ($_REQUEST['AllowClientProgramConnect'])? 1 : 0;
$SettingArr['TerminalIndependentCat'] = ($_REQUEST['TerminalIndependentCat'])? 1 : 0;
$SettingArr['PaymentMethod'] = $_REQUEST['PaymentMethod'];
$SettingArr['ConfirmBeforePay'] = $_REQUEST['ConfirmBeforePay'];
$SettingArr['IPAllow'] = $_REQUEST['IPAllow'];

$Success = $GeneralSetting->Save_General_Setting('ePOS',$SettingArr);

$GeneralSetting->Start_Trans();
if($GeneralSetting->Save_General_Setting('ePOS',$SettingArr)) {
	$msg = urlencode($Lang['ePOS']['ReturnMessage']['TerminalSettingsUpdateSuccess']);
	$GeneralSetting->Commit_Trans();
}
else {
	$msg = urlencode($Lang['ePOS']['ReturnMessage']['TerminalSettingsUpdateFailed']);
	$GeneralSetting->RollBack_Trans();
}

intranet_closedb();
header("Location: terminal.php?msg=$msg");
?>