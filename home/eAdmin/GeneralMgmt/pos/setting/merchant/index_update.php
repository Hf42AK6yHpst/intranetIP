<?php
/*
 *  2020-08-20 Cameron
 *  - create this file
 */

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ePayment']['MultiPaymentGateway']) {
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    intranet_closedb();
    exit();
}

$GeneralSetting = new libgeneralsettings();

### Build Settings Array
$SettingArr = array();

# Allow Client Program Connect
$SettingArr['AlipayMerchantAccountID'] = IntegerSafe($_POST['MerchantAccountID']);

$Success = $GeneralSetting->Save_General_Setting('ePOS',$SettingArr);

$GeneralSetting->Start_Trans();
if($GeneralSetting->Save_General_Setting('ePOS',$SettingArr)) {
    $msg = urlencode($Lang['General']['ReturnMessage']['RecordSaveSuccess']);
    $GeneralSetting->Commit_Trans();
}
else {
    $msg = urlencode($Lang['General']['ReturnMessage']['RecordSaveUnSuccess'] );
    $GeneralSetting->RollBack_Trans();
}

intranet_closedb();
header("Location: index.php?ReturnMsg=$msg");
