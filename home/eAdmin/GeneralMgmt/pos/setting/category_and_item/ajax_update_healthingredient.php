<?php
// using ivan
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");

intranet_opendb();

$libPOS = new libpos();

# Get data
$Action = stripslashes($_REQUEST['Action']);

switch($Action)
{
	case "Add":
		$Value['ItemID'] = stripslashes($_REQUEST['ItemID']);
		$Value['HealthIngredientID'] = stripslashes($_REQUEST['HealthIngredientID']);
		$Value['ItemIntake'] = stripslashes($_REQUEST['ItemIntake']);
		
		$success = $libPOS->InsertItemHealthIngredient($Value);
		if($success)
			$msg = $Lang['ePOS']['ReturnMessage']['Add']['Success']['HealthIngredient'];
		else
			$msg = $Lang['ePOS']['ReturnMessage']['Add']['Fail']['HealthIngredient'];
			
		echo $msg;
	break;
	
	case "Edit":
		$ItemID = stripslashes($_REQUEST['ItemID']);
		$HealthIngredientID = stripslashes($_REQUEST['HealthIngredientID']);
		$Value['IngredientIntake'] = stripslashes($_REQUEST['ItemIntake']);
		
		$success = $libPOS->UpdateItemHealthIngredient($Value,$ItemID,$HealthIngredientID);
		if($success)
			$msg = $Lang['ePOS']['ReturnMessage']['Edit']['Success']['HealthIngredient'];
		else
			$msg = $Lang['ePOS']['ReturnMessage']['Edit']['Fail']['HealthIngredient'];
			
		echo $msg;
	break;
	
	case "Delete":
		$ItemID = stripslashes($_REQUEST['ItemID']);
		$HealthIngredientID = stripslashes($_REQUEST['HealthIngredientID']);
		
		$success = $libPOS->DeleteItemHealthIngredient($ItemID,$HealthIngredientID);
		if($success)
			$msg = $Lang['ePOS']['ReturnMessage']['Edit']['Success']['HealthIngredient'];
		else
			$msg = $Lang['ePOS']['ReturnMessage']['Edit']['Fail']['HealthIngredient'];
			
		echo $msg;
	break;
	
	default: return 0;
	
}

intranet_closedb();
?>