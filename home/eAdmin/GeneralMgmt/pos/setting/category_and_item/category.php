<?
// Editing by  
/*
 * 2020-04-14 (Cameron): set hidKeyword in js_Go_Item_Page() when "Include Item" is not checked for keyword search
 * 2020-03-30 (Cameron): fix jsIncludeSearchItem in js_Reload_Category_Table(), should pass it as 1 or 0 instead of true or false because the parameter is boolean in 
 *      function definition 
 * 2014-02-13 (Carlos): Added js_Delete_Category()
 * 2014-02-11 (Carlos): Disable the maximum number of active category restriction
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libpos_category.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit();
}

$libPOS_ui = new libpos_ui();
$CurrentPageArr['ePOS'] = 1;	// Top Menu
$CurrentPage['Category&ItemSetting'] = 1;	// Left Menu

# tag information
$TAGS_OBJ[] = array($Lang['ePOS']['CategorySetting'], '', 0);
$MODULE_OBJ = $libPOS_ui->GET_MODULE_OBJ_ARR();
$linterface = new interface_html();
$linterface->LAYOUT_START($ReturnMsg);


$hidKeyword = $_POST['hidKeyword'];     // return from item, has higher priority
$Keyword = $hidKeyword ? $hidKeyword : $_REQUEST['Keyword'];
$IncludeSearchItem = $_REQUEST['IncludeSearchItem'];
echo $libPOS_ui->Get_Settings_Category_UI($Keyword, $IncludeSearchItem);

$libCategory = new POS_Category();
$CategoryStatusArr = $libCategory->Get_RecordStatus_Count();
$EnabledCount = ($CategoryStatusArr['Enabled'] == "")? 0:$CategoryStatusArr['Enabled'];
$DisabledCount = ($CategoryStatusArr['Disabled'] == "")? 0:$CategoryStatusArr['Disabled'];
?>


<script>
var isChecking = 0;
var jsEnabledCount = <?=$EnabledCount?>;
var jsDisabledCount = <?=$DisabledCount?>;

$(document).ready( function() {
	initThickBox();
	js_Init_DND_Table();
	js_Update_Client_Connection_Text_Color();
	
	$('input#Keyword').focus().keyup(function(e){
		var keynum = Get_KeyNum(e);
		
		if (keynum==13) // keynum==13 => Enter
			js_Reload_Category_Table();
	});
});

function js_Update_Category_Info(jsActionType, jsCategoryID)
{
	jsCategoryID = jsCategoryID || '';
	
	js_Check_Client_Connection();
	
	var hasChecked = false;
	var performAction = false;
	// jsClientConnectionAjaxFile is defined in /templates/2009a/js/pos.js to ensure the ajax response is from the checking function
	$().ajaxComplete(function(e, xhr, settings) {
		if (settings.url == jsClientConnectionAjaxFile && hasChecked == false) 
		{
			var jsAllowClientConnection = xhr.responseText;
			
			if (jsAllowClientConnection == 1)
			{
				if (confirm('<?=$Lang['ePOS']['jsWarningArr']['ChangeClientConnectionSettings']?>'))
				{
					// update settings
					js_Change_Client_Connection();
					performAction = true;
				}
			}
			else
			{
				performAction = true;
			}
			hasChecked = true;
			
			// perform action
			if (performAction == true)
			{
				if (jsActionType == 'Insert_Edit_Category')
				{
					var CategoryName = Trim($('input#CategoryName').val());
					var CategoryCode = Trim($('input#CategoryCode').val());
					var CategoryPhoto = Trim($('input#CategoryPhoto').val());
					
					// Name checking
					if (js_Is_Input_Blank('CategoryName', 'NameWarningDiv', '<?=$Lang['ePOS']['WarningArr']['Blank']['Name']?>'))
					{
						$("input#CategoryName").focus();
						return false;
					}
					// Code checking
					if (js_Is_Input_Blank('CategoryCode', 'CodeWarningDiv', '<?=$Lang['ePOS']['WarningArr']['Blank']['Code']?>'))
					{
						$("input#CategoryCode").focus();
						return false;
					}
					// Photo checking
					if (CategoryPhoto != '')
					{
						// Photo Dimension not valid
						if ($('input#IsPhotoDimensionValid').val() == 0)
						{
							$("input#CategoryPhoto").focus();
							return false;
						}
						
						var FileExt = Get_File_Ext(document.getElementById('CategoryPhoto').value);
						var FileName = Get_File_Name(document.getElementById('CategoryPhoto').value);
						
						if ((FileExt == "jpg" || FileExt == "jpeg" || FileExt == "gif" || FileExt == "png") && FileExt != false) {
							// do nth
						}
						else {
							$('div#FileUploadWarningDiv').html('<?=$Lang['ePOS']['WarningArr']['InvalidFile']?>');
							$('div#FileUploadWarningDiv').show('fast');
							return false;
						}
					}
					
					$.post(
						"ajax_validate.php", 
						{
							Action: "CategoryName",
							Value: CategoryName,
							CategoryID: jsCategoryID
						},
						function(ReturnData)
						{
							if (ReturnData == 1)
							{
								// valid code
								$('div#CodeWarningDiv').hide();
								
								$.post(
									"ajax_validate.php", 
									{
										Action: "CategoryCode",
										Value: CategoryCode,
										CategoryID: jsCategoryID
									},
									function(ReturnData)
									{
										if (ReturnData == 1)
										{
											// valid name
											$('#NameWarningDiv').hide();
											
											Block_Thickbox();
											
											// Use iFrame to Insert or Edit basic info is too slow => Reload of UI is faster than update info
											// So, the updated info cannot be display
											// => use ajax to update basic data, then use iFrame to upload photo
											$.post(
												"ajax_update_category.php", 
												{
													Action: "Insert_Edit",
													CategoryCode: CategoryCode,
													CategoryName: CategoryName,
													CategoryID: jsCategoryID
												},
												function (ReturnData)
												{
													var TextArr = ReturnData.split(',');
													
													var PhotoCategoryID = TextArr[0];
													var SuccessStatus = TextArr[1];
													
													// Update Category Information
													//Create_IFrame('FileUploadIframe');
													
													var Obj = document.getElementById("CategoryForm");
													
													//Obj.target = 'FileUploadIframe';
													Obj.target = 'CheckDimensionFileUploadIframe'; 
													Obj.encoding = "multipart/form-data";
													Obj.method = 'post'; 
													Obj.action = "ajax_update_category.php?Action=Upload_Photo&CategoryID=" + PhotoCategoryID;
													Obj.submit();


													// Update Enabled Disabled Category Count
													//if (jsEnabledCount < 10)
													//	jsEnabledCount++;
													//else
													//	jsDisabledCount++;
													
													UnBlock_Thickbox();
													js_Hide_ThickBox();
 													js_Reload_Category_Table();
													
													// Show system messages
													if (jsCategoryID == '')
														js_Show_System_Message("add", SuccessStatus);
													else
														js_Show_System_Message("edit", SuccessStatus);
												}
											);
										}
										else
										{
											// invalid code => show warning
											$('div#CodeWarningDiv').html('<?=$Lang['ePOS']['WarningArr']['Duplicated']['Code']?>').show();
											$('input#CategoryCode').focus();
											
											//$('#debugArea').html('aaa ' + ReturnData);
										}
									}
								);
							}
							else
							{
								// invalid name => show warning
								$('div#NameWarningDiv').html('<?=$Lang['ePOS']['WarningArr']['Duplicated']['Name']?>').show();
								$('input#CategoryName').focus();
								
								//$('#debugArea').html('aaa ' + ReturnData);
								
							}
						}
					);
				}
				else if (jsActionType == 'Change_Active_Status')
				{
					var jsLinkID = 'ActiveStatus_' + jsCategoryID;
					var TargetRecordStatus;
					var jsCanUpdateStatus = 1;
					if ($('a#' + jsLinkID).attr('class') == 'active_item')
					{
						// active -> inactive
						$('a#' + jsLinkID).attr('class', 'inactive_item').attr('title', '<?=$Lang['General']['Disabled']?>');
						TargetRecordStatus = 0;
						
						//jsEnabledCount--;
						//jsDisabledCount++;
					}
					else
					{
						// inactive -> active
						//if (jsEnabledCount < 10)
						//{
							$('a#' + jsLinkID).attr('class', 'active_item').attr('title', '<?=$Lang['General']['Enabled']?>');
							TargetRecordStatus = 1;
							
						//	jsEnabledCount++;
						//	jsDisabledCount--;
						//}
						//else
						//{
						//	jsCanUpdateStatus = 0;
						//	alert('<?=$Lang['ePOS']['jsWarningArr']['Max10EnableCategory']?>');
						//}
					}
					
					if (jsCanUpdateStatus == 1)
					{
						$.post(
							"ajax_update_category.php", 
							{ 
								Action: 'Edit',
								CategoryID: jsCategoryID,
								DBFieldName: 'RecordStatus',
								UpdateValue: TargetRecordStatus
							},
							function(ReturnData)
							{
								js_Reload_Status_Count_Display();
								//$('#debugArea').html(ReturnData);
							}
						);
					}
				}
				else if (jsActionType == 'Delete_Photo')
				{
					if (confirm('<?=$Lang['ePOS']['jsWarningArr']['DeletePhoto']?>'))
					{
						$.post(
							"ajax_update_category.php", 
							{
								Action: 'Delete_Photo',
								CategoryID: jsCategoryID
							},
							function(ReturnData)
							{
								js_Reload_Category_Table();
								//$('#debugArea').html(ReturnData);
							}
						);
					}
				}
			}	// End if (performAction == true)
		}	// End if (settings.url == jsClientConnectionAjaxFile && hasChecked == false)
	});	// End ajaxComplete
}


function js_Show_Category_Add_Edit_Layer(jsCategoryID)
{
	jsCategoryID = jsCategoryID || '';
	
	js_Check_Client_Connection();
	
	var hasChecked = false;
	var performAction = false;
	// jsClientConnectionAjaxFile is defined in /templates/2009a/js/pos.js to ensure the ajax response is from the checking function
	$().ajaxComplete(function(e, xhr, settings) {
		if (settings.url == jsClientConnectionAjaxFile && hasChecked == false) 
		{
			var jsAllowClientConnection = xhr.responseText;
			
			if (jsAllowClientConnection == 1)
			{
				if (confirm('<?=$Lang['ePOS']['jsWarningArr']['ChangeClientConnectionSettings']?>'))
				{
					// update settings
					js_Change_Client_Connection();
					performAction = true;
				}
			}
			else
			{
				performAction = true;
			}
			hasChecked = true;
			
			
			if (performAction == true)
			{
				$.post(
					"ajax_reload.php", 
					{ 
						RecordType: "Category_Add_Edit_Layer",
						CategoryID: jsCategoryID
					},
					function(ReturnData)
					{
						$('#TB_ajaxContent').html(ReturnData);
						$('input#CategoryName').focus();
						
						js_Add_KeyUp_Unique_Checking(jsCategoryID, 'CategoryCode', 'CodeWarningDiv', 'CategoryCode', '<?=$Lang['ePOS']['WarningArr']['Blank']['Code']?>','<?=$Lang['ePOS']['WarningArr']['Duplicated']['Code']?>');
						js_Add_KeyUp_Unique_Checking(jsCategoryID, 'CategoryName', 'NameWarningDiv', 'CategoryName', '<?=$Lang['ePOS']['WarningArr']['Blank']['Name']?>','<?=$Lang['ePOS']['WarningArr']['Duplicated']['Name']?>');
						
						$('input#CategoryPhoto').change( function() {
							js_Check_Photo_Dimension();
						});
						
						//$('#debugArea').html(ReturnData);
					}
				);
			}
			else
			{
				js_Hide_ThickBox();
			}
		}
	});
}

function js_Check_Photo_Dimension()
{
	// Disable Submit Btn
	$('input#Btn_Submit').attr('disabled', true);
	
	var Obj = document.getElementById("CategoryForm");
	Obj.target = 'CheckDimensionFileUploadIframe'; 
	Obj.encoding = "multipart/form-data";
	Obj.method = 'post';
	Obj.action = "ajax_validate.php?Action=Check_Photo_Dimension&ForCategory=1";
	Obj.submit();
}

function js_Reload_Category_Table()
{
	jsKeyword = Trim($('input#Keyword').val());
	jsIncludeSearchItem = $('input#IncludeSearchItem').attr('checked') ? '1' : '0';
	
	//alert('jsIncludeSearchItem = ' + jsIncludeSearchItem);
	
	$('div#CategoryDiv').html('');
	Block_Element('CategoryDiv');
	
	$('div#CategoryDiv').load(
		"ajax_reload.php", 
		{ 
			RecordType: 'Category_Table',
			Keyword: jsKeyword,
			IncludeSearchItem: jsIncludeSearchItem
		},
		function(ReturnData)
		{
			initThickBox();
			js_Init_DND_Table();
			UnBlock_Element('CategoryDiv');
		}
	);
}

function js_Reload_Status_Count_Display()
{
	// update enabled & disabled count
	jsEnabledCount = $('#CategoryDiv a.active_item').length;
	jsDisabledCount = $('#CategoryDiv a.inactive_item').length;
	$('span#enabledCountSpan').html(jsEnabledCount);
	$('span#disabledCountSpan').html(jsDisabledCount);
}


function js_Init_DND_Table() {
	var target_tBodiesIndex = 0;
	var JQueryObj = $(".common_table_list");
	JQueryObj.tableDnD({
		onDrop: 
		function(table, DroppedRow) {
			
			js_Check_Client_Connection();
			
			var hasChecked = false;
			var performAction = false;
			
			// jsClientConnectionAjaxFile is defined in /templates/2009a/js/pos.js to ensure the ajax response is from the checking function
			$().ajaxComplete(function(e, xhr, settings) {
				if (settings.url == jsClientConnectionAjaxFile && hasChecked == false) 
				{
					var jsAllowClientConnection = xhr.responseText;
					
					if (jsAllowClientConnection == 1)
					{
						if (confirm('<?=$Lang['ePOS']['jsWarningArr']['ChangeClientConnectionSettings']?>'))
						{
							// update settings
							js_Change_Client_Connection();
							performAction = true;
						}
						else
						{
							// recover the table
							js_Reload_Category_Table();
						}
					}
					else
					{
						performAction = true;
					}
					hasChecked = true;
					
					
					if (performAction == true)
					{
						// Reordering
						if(table.id == "CategoryContentTable")
						{
							var rows = table.tBodies[0].rows;
							var RecordOrder = "";
							for (var i=0; i<rows.length; i++) {
								if (rows[i].id != "")
								{
									var thisID = rows[i].id;
									var thisIDArr = thisID.split('_');
									var thisObjectID = thisIDArr[1];
									RecordOrder += thisObjectID + ",";
								}
							}
							//alert("RecordOrder = " + RecordOrder);
							
							$.post(
								"ajax_update_category.php", 
								{ 
									Action: "DisplayOrder",
									DisplayOrderString: RecordOrder
								},
								function(ReturnData)
								{
									//$('#debugArea').html(ReturnData);
								}
							);
						}
						else
						{
							return false;
						}
					}
				}
			});
		},
		onDragStart: function(table, DraggedRow) 
		{
			//$('#debugArea').html("Started dragging row "+row.id);
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
}



// Add KeyUp Listener for Checking
function js_Add_KeyUp_Unique_Checking(jsCategoryID, InputID, WarningDivID, DBFieldName, WarnBlankMsg, WarnUniqueMsg)
{
	jsCategoryID = jsCategoryID || '';
	
	$("input#"+InputID).keyup(function(){
		js_Check_Unique(jsCategoryID, InputID, WarningDivID, DBFieldName, WarnBlankMsg, WarnUniqueMsg);
	});
}

// validation 
function js_Check_Unique(jsCategoryID, InputID, WarningDivID, DBFieldName, WarnBlankMsg, WarnUniqueMsg)
{
	var jsShowHideSpeed = "100";
	var InputValue = $("input#" + InputID).val();
	if(Trim(InputValue) == '')
	{
		$('div#' + WarningDivID).html(WarnBlankMsg);
		$('div#' + WarningDivID).show(jsShowHideSpeed);
	}
	else
	{
		$('div#' + WarningDivID).html("");
		$('div#' + WarningDivID).hide(jsShowHideSpeed);
		
		$.post(
			"ajax_validate.php", 
			{ 
				Action: DBFieldName,
				Value: InputValue,
				CategoryID: jsCategoryID
			},
			function(isVaild)
			{
				if(isVaild != 1)
				{
					$('div#' + WarningDivID).html(WarnUniqueMsg);
					$('div#' + WarningDivID).show(jsShowHideSpeed);
				}
				else
				{
					$('div#' + WarningDivID).html("");
					$('div#' + WarningDivID).hide(jsShowHideSpeed);
				}
			}
		);
	}
}


function js_Go_Item_Page(jsCategoryID)
{
	jsCategoryID = jsCategoryID || '';
	if (jsCategoryID == '')
		return ;

	if (!$('#IncludeSearchItem').is(':checked') && $('#Keyword').val() != '') {
		$('#hidKeyword').val($('#Keyword').val());
		$('#Keyword').val('');
	} 
	$('input#CategoryID').val(jsCategoryID);
	var objForm = document.getElementById('CategoryMainForm');
	objForm.action = 'item.php';
	objForm.submit();
}


function js_Show_System_Message(Action, Status)
{
	var returnMessage = '';
	
	switch(Action)
	{
		case "add":
			returnMessage = (Status)? '<?=$Lang['ePOS']['ReturnMessage']['Add']['Success']['Category']?>' : '<?=$Lang['ePOS']['ReturnMessage']['Add']['Failed']['Category']?>';
			break;
		case "edit":
			returnMessage = (Status)? '<?=$Lang['ePOS']['ReturnMessage']['Edit']['Success']['Category']?>' : '<?=$Lang['ePOS']['ReturnMessage']['Edit']['Failed']['Category']?>';
			break;
	}
	
	Get_Return_Message(returnMessage);
}

function js_Delete_Category(CategoryID)
{
	js_Check_Client_Connection();
	
	var hasChecked = false;
	var performAction = false;
	// jsClientConnectionAjaxFile is defined in /templates/2009a/js/pos.js to ensure the ajax response is from the checking function
	$().ajaxComplete(function(e, xhr, settings) {
		if (settings.url == jsClientConnectionAjaxFile && hasChecked == false) 
		{
			var jsAllowClientConnection = xhr.responseText;
			
			if (jsAllowClientConnection == 1)
			{
				if (confirm('<?=$Lang['ePOS']['jsWarningArr']['ChangeClientConnectionSettings']?>'))
				{
					// update settings
					js_Change_Client_Connection();
					performAction = true;
				}
			}
			else
			{
				performAction = true;
			}
			hasChecked = true;
			
			if (performAction == true)
			{

				if(confirm('<?=$Lang['General']['JS_warning']['ConfirmDelete']?>')){
					Block_Element('CategoryDiv');
					$.post(
						'ajax_update_category.php',
						{
							'Action': 'Delete_Category',
							'CategoryID': CategoryID 
						},
						function(result){
							if(result == '1'){
								Get_Return_Message('<?=$Lang['General']['ReturnMessage']['DeleteSuccess']?>');
							}else{
								Get_Return_Message('<?=$Lang['General']['ReturnMessage']['DeleteUnsuccess']?>');
							}
							js_Reload_Category_Table();
						}
					);
				}
			}
		}
	});
}
</script>

<br /> 
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>