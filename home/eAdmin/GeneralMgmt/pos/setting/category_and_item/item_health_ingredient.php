<?
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libpos_item.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit();
}

$libPOS_ui = new libpos_ui();
$CurrentPageArr['ePOS'] = 1;	// Top Menu
$CurrentPage['Category&ItemSetting'] = 1;	// Left Menu

# tag information
$TAGS_OBJ[] = array($Lang['ePOS']['ItemSetting'], '', 0);
$MODULE_OBJ = $libPOS_ui->GET_MODULE_OBJ_ARR();
$linterface = new interface_html();
$linterface->LAYOUT_START($ReturnMsg);

$ItemID = $_REQUEST['ItemID'];
echo $libPOS_ui->Get_Settings_Item_Health_Ingredient_Mapping_UI($ItemID);

?>


<script>
var jsItemID = <?=$ItemID?>;

$(document).ready( function() {
	initThickBox();
	
	$('input#SearchTb').focus().keyup(function(e){
		var keynum = Get_KeyNum(e);
		
		if (keynum==13) // keynum==13 => Enter
			js_Reload_Item_Table();
	});
});

function Update_HealthIngredient_List(Action, ItemID ,HealthIngredientID)
	{
		//Check if all warning div are hidden
		if($("div#ItemIntakeWarningDiv:visible").html()||Trim($("input#ItemIntake").val())=='')
			return false;
		
		if(Action=='Add')
			HealthIngredientID = $("select#HealthIngredientID").val();
		else
			HealthIngredientID = HealthIngredientID
				
		$.post(
			"ajax_update_healthingredient.php", 
			{ 
				ItemID				: ItemID,
				HealthIngredientID	: HealthIngredientID,
				Action				: Action,
				ItemIntake			: $("input#ItemIntake").val()
			},
			function(ReturnData)
			{
				Get_Return_Message(ReturnData);
				js_Hide_ThickBox();
				js_Reload_Main_Table();
			}
		);
	}

function js_Show_Health_Ingredient_Add_Edit_Layer(jsItemID, jsHealthIngredientID)
{
	$.post(
			"ajax_reload.php", 
			{ 
				RecordType: "Health_Ingredient_Add_Edit_Layer",
				HealthIngredientID: jsHealthIngredientID,
				ItemID: jsItemID
			},
			function(ReturnData)
			{
				$('#TB_ajaxContent').html(ReturnData);
				// Code validation
				js_Add_KeyUp_Float_Checking(jsItemID,'ItemIntake', 'ItemIntakeWarningDiv', '<?=$Lang['ePOS']['WarningArr']['Blank']['StandardIntakePerDay']?>','<?=$Lang['ePOS']['WarningArr']['NotFloat']['StandardIntakePerDay']?>');
			}
		);
}

function js_Reload_Main_Table()
{
	$('div#MainTableDiv').load(
		"ajax_reload.php", 
		{ 
			RecordType	: 'Item_Health_Ingredient_Table',
			ItemID		:jsItemID
		},
		function(ReturnData)
		{
			initThickBox();
		}
	);
}

// Add KeyUp Listener for Checking
function js_Add_KeyUp_Float_Checking(jsItemID, InputID, WarningDivID, WarnBlankMsg, WarnFloatMsg)
{
	jsItemID = jsItemID || '';
	//WarnGreaterThenZeroMsg = WarnGreaterThenZeroMsg || '';
	
	$("input#"+InputID).keyup(function(){
		js_Check_Float_Num(jsItemID, InputID, WarningDivID, WarnBlankMsg, WarnFloatMsg);
	});
}

// validation 
function js_Check_Float_Num(jsItemID, InputID, WarningDivID, WarnBlankMsg, WarnFloatMsg)
{
	jsItemID = jsItemID || '';
	
	var jsShowHideSpeed = "100";
	var InputValue = Trim($("input#"+InputID).val());
	
	if (InputValue == '')
	{
		$('div#' + WarningDivID).html(WarnBlankMsg).show(jsShowHideSpeed);
		return true;
	}
	else
	{
		$('div#' + WarningDivID).html("").hide(jsShowHideSpeed);
		var patt=/^([+]?((([0-9]+(\.)?)|([0-9]*\.[0-9]+))([eE][+\-]?[0-9]+)?))$/;
		if(patt.test(InputValue) == true)
		{
			$('div#' + WarningDivID).html("").hide(jsShowHideSpeed);
			return false;
		}
		else
		{
			$('div#' + WarningDivID).html(WarnFloatMsg).show(jsShowHideSpeed);
			return true;
		}
	}
}

function js_Show_System_Message(Action, Status)
{
	var returnMessage = '';
	
	switch(Action)
	{
		case "add":
			returnMessage = (Status)? '<?=$Lang['ePOS']['ReturnMessage']['Add']['Success']['Item']?>' : '<?=$Lang['ePOS']['ReturnMessage']['Add']['Failed']['Item']?>';
			break;
		case "edit":
			returnMessage = (Status)? '<?=$Lang['ePOS']['ReturnMessage']['Edit']['Success']['Item']?>' : '<?=$Lang['ePOS']['ReturnMessage']['Edit']['Failed']['Item']?>';
			break;
	}
	
	Get_Return_Message(returnMessage);
}
</script>

<br /> 
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>