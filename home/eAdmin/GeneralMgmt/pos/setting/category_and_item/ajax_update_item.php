<?php
// using 
/*
 * 2019-07-16 (Cameron): add Description field
 * 2014-02-11 (Carlos): Added $Action=="Delete_Item"
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_item.php");

intranet_opendb();

# Get data
$Action = stripslashes($_REQUEST['Action']);

$returnString = '';
if ($Action == "Insert_Edit")
{
	$DataArr = array();
	
	$DataArr['CategoryID'] = stripslashes($_REQUEST['CategoryID']);
	$DataArr['ItemName'] = stripslashes($_REQUEST['ItemName']);
	$DataArr['Barcode'] = stripslashes($_REQUEST['Barcode']);
	$DataArr['UnitPrice'] = stripslashes($_REQUEST['UnitPrice']);
    $DataArr['Description'] = stripslashes($_REQUEST['Description']);
	$ItemID = stripslashes($_REQUEST['ItemID']);
	$SuccessArr = array();
	
	### Insert / Update Basic Info
	$objItem = new POS_Item($ItemID);
	if ($ItemID == '')
	{
		$DataArr['Sequence'] = $objItem->Get_Max_Display_Order() + 1;
		
		$PhotoItemID = $objItem->Insert_Object($DataArr);
		$SuccessArr['UpdateInfo'] = ($PhotoItemID)? true : false;
	}
	else
	{
		$PhotoItemID = $ItemID;
		$SuccessArr['UpdateInfo'] = $objItem->Update_Object($DataArr);
	}
		
	if (in_array(false, $SuccessArr))
		$returnString = $PhotoItemID.',0';
	else
		$returnString = $PhotoItemID.',1';
}
else if ($Action == "Upload_Photo")
{
	### Upload Photos
	if ($ItemPhoto != '')
	{
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		
		$ItemID = stripslashes($_REQUEST['ItemID']);
		$objItem = new POS_Item($ItemID);
		$returnString = $objItem->Upload_Photo($ItemPhoto, $ItemPhoto_name);
	}
}
else if ($Action == "Delete_Photo")
{
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	
	$ItemID = stripslashes($_REQUEST['ItemID']);
	$objItem = new POS_Item($ItemID);
	$returnString = $objItem->Delete_Photo();
}
else if ($Action == "Edit")
{
	$ItemID = stripslashes(urldecode($_REQUEST['ItemID']));
	$DBFieldName = stripslashes(urldecode($_REQUEST['DBFieldName']));
	$UpdateValue = stripslashes(urldecode($_REQUEST['UpdateValue']));
	
	$DataArr = array();
	$DataArr[$DBFieldName] = $UpdateValue;
	
	$objItem = new POS_Item($ItemID);
	$returnString = $objItem->Update_Object($DataArr);
}
else if ($Action == "DisplayOrder")
{
	$DisplayOrderString = $_REQUEST['DisplayOrderString'];
	$objItem = new POS_Item();
	$displayOrderArr = $objItem->Get_Update_Display_Order_Arr($DisplayOrderString);
	$returnString = $objItem->Update_DisplayOrder($displayOrderArr);
}else if ($Action == "Delete_Item"){
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	
	$ItemID = stripslashes($_REQUEST['ItemID']);
	$objItem = new POS_Item($ItemID);
	$result = $objItem->Permanent_Delete_Item();
	$returnString = $result ? "1":"0";
}

intranet_closedb();

echo $returnString;
?>