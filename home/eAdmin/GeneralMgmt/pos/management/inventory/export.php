<?php
// Editing by 
/*
 * 2015-03-02 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libpos_item.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit();
}

$libPOS = new libpos();
$libPOS_ui = new libpos_ui();
$lexport = new libexporttext();

$Keyword = trim(stripslashes(urldecode($_REQUEST['keyword'])));
$field = in_array($field,array(0,1,2,3,4,5,6))? $field : 0;
$asc_desc = $order == 1? " ASC" : " DESC";

$field_array = array("item.ItemName",
					 "cat.CategoryName",
					 "item.Barcode",
					 "item.ItemCount",
					 "item.UnitPrice",
					 "item.InventoryDateModify",
					 "item.InventoryModifyBy"
					 );

$sql = $libPOS->Get_Management_Inventory_Index_Sql($CategoryID, $Keyword, true);

$sql .= " ORDER BY ".$field_array[$field].$asc_desc;

$records = $libPOS->returnResultSet($sql);
$record_count = count($records);

$header = array($Lang['ePOS']['Item'],$Lang['ePOS']['Category'],$Lang['ePOS']['Barcode'],$Lang['ePOS']['Inventory'],$Lang['ePOS']['UnitPrice'],$Lang['ePOS']['Photo'],$Lang['General']['LastUpdatedTime'],$Lang['General']['LastUpdatedBy']);
$rows = array();

for($i=0;$i<$record_count;$i++){
	
	$row = array();
	$row[] = $records[$i]['ItemName'];
	$row[] = $records[$i]['CategoryName'];
	$row[] = $records[$i]['Barcode'];
	$row[] = $records[$i]['ItemCount'];
	$row[] = $records[$i]['UnitPrice'];
	$row[] = $records[$i]['PhotoLink']=='--'?'':$Lang['General']['Yes'];
	$row[] = $records[$i]['InventoryDateModify'];
	$row[] = $records[$i]['InventoryModifyBy'];
	
	$rows[] = $row;	
}

$title = $Lang['ePOS']['Inventory'];

$filename = $title.".csv";
$export_content = $lexport->GET_EXPORT_TXT($rows, $header,"","\r\n","",0,"11");
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>