<?php
// Using :
/*

 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libpos_item.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit();
}

$libpos = new libpos();

$lo = new libfilesystem();
$limport = new libimporttext();
$filepath = $userfile;
$filename = $userfile_name;

$libPOS_ui = new libpos_ui();
$CurrentPageArr['ePOS'] = 1;	// Top Menu
$CurrentPage['ManageInventory'] = 1;	// Left Menu


$file_format = array("Barcode","Cost Price","Increase By","Remarks");
$flagAry = array(1, 1, 1, 1);

if($filepath=="none" || $filepath == ""){          # import failed
	header("Location: import.php?failed=2");
	exit();
}  else {
	$ext = strtoupper($lo->file_ext($filename));
	if ($limport->CHECK_FILE_EXT($filename)) {
		# read file into array
		# return 0 if fail, return csv array if success
		//$data = $lo->file_read_csv($filepath);
		//$col_name = array_shift($data);                   # drop the title bar
		//$data = $limport->GET_IMPORT_TXT($filepath);
		$data = $limport->GET_IMPORT_TXT($filepath, "", "", $file_format, $flagAry);
		//debug_r($data); die;
		$col_name = array_shift($data);

		#Check file format
		$format_wrong = false;


		for ($i = 0; $i < sizeof($file_format); $i++) {
			if ($col_name[$i] != $file_format[$i]) {
				$format_wrong = true;
				break;
			}
		}
		if ($format_wrong) {
			header("Location: import.php?failed=2");
			exit();
		}

		$barcode_array = array();
		$t_sql ="SELECT ItemID,Barcode FROM POS_ITEM";
		$t_result = $libpos->returnArray($t_sql,2);
		for($i=0;$i<sizeof($t_result);$i++) {
			list($item_id, $barcode) = $t_result[$i];
			$barcode_array[$barcode] = $item_id;
		}


		$sql_data = array();
		$error1 = false; // No matched barcode
		$error2 = false; // Invalid cost price
		$error3 = false; // Invalid Amount

		for ($i=0; $i<sizeof($data); $i++) {
			$warning_msg = array();
			# check for empty line
			$test = trim(implode("", $data[$i]));
			if ($test == "") continue;

			list($barcode, $cost_price, $increase_value, $remark) = $data[$i];
			$barcode = trim($barcode);
			$cost_price = trim($cost_price);

			if($barcode == "" || isset($barcode_array[$barcode]) == false) {
				$error1 = true;
				$warning_msg[] = $i_ePOS_InventoryImport_NoMatch_Entry;
			}

			if($cost_price !='') {
				if ((!is_numeric($cost_price) || $cost_price <= 0)) {
					$error2 = true;
					$warning_msg[] = $i_ePOS_InventoryImport_InvalidCostPrice;
				}
			}

			if(trim($increase_value)=='' || (!is_numeric($increase_value) || $increase_value<=0) )
			{
				$error3 = true;
				$warning_msg[]=$i_ePOS_InventoryImport_InvalidIncreaseBy;
			}

			$str_cost_price = $error2?"$".$cost_price:"$".number_format($cost_price,2);
			$warning_message = "<table border=\"0\">";
			for($x=0;$x<sizeof($warning_msg);$x++){
				$warning_message.="<tr class=\"tabletext\"><td><font color=\"red\">-</font></td><td><font color=\"red\">".$warning_msg[$x]."</font></td></tr>";
			}
			if(sizeof($warning_msg)<=0)
				$warning_message.="<tr><td colspan=\"2\">&nbsp;</td></tr>";
			$warning_message.="</table>";

			$error_entries[] = array($barcode, $str_cost_price, $increase_value, intranet_htmlspecialchars($remark), $warning_message);

			if(!$error1 && !$error2 && !$error3){
				$item_id = $barcode_array[$barcode];
				$sql_data[] = "('$item_id','".$libpos->Get_Safe_Sql_Query($barcode)."','$cost_price','$increase_value','".$libpos->Get_Safe_Sql_Query($remark)."')";
			}

		}
		if(!$error1 && !$error2 && !$error3){
			$values = implode(",",$sql_data);
			$sql = "INSERT INTO TEMP_EPOS_INVENTORY_IMPORT (ItemID, Barcode,CostPrice,AdjustedBy,Remark) VALUES $values";
			$libpos->db_db_query($sql);
		}
	} else
	{
		header ("Location: import.php?failed=1");
		exit();
	}
}

$error_exists = $error1 || $error2 || $error3 ? true:false;
if(!$error_exists){
	$namefield = getNameFieldByLang("b.");

	$sql = "SELECT Barcode, CostPrice, AdjustedBy, Remark
        FROM TEMP_EPOS_INVENTORY_IMPORT
     ";
	$result = $libpos->returnArray($sql,3);
}
else{
	$result = $error_entries;
}

$display = "<tr class=\"tabletop\">
                <td class=\"tabletoplink\" width=\"20%\">".$Lang['ePOS']['Barcode']."</td>
                <td class=\"tabletoplink\" width=\"10%\">".$Lang['ePOS']['CostPrice']."</td>
				<td class=\"tabletoplink\" width=\"10%\">".$Lang['ePOS']['IncreaseBy']."</td>
				<td class=\"tabletoplink\" width=\"30%\">".$Lang['General']['Remark']."</td>";

if(!$error_exists)
	$display.="<td class=\"tabletoplink\" width=\"30%\">$i_Payment_Menu_Settings_PaymentItem_Import_Action</td>";
else
	$display.="<td class=\"tabletoplink\" width=\"30%\">$i_Payment_Import_PPSLink_Result_ErrorMsg_Reason</td>";
$display.="</tr>\n";

for ($i=0; $i<sizeof($result); $i++)
{
	list($barcode,$cost_price,$increase_value,$remark, $reason) = $result[$i];
	$str_cost_price = $error_exists? $cost_price:"$".number_format($cost_price, 2);
	$css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
	if(!$error_exists)
		$reason = "<font color=green>".$Lang['ePOS']['InventoryAdd']."</font>";

	$display .= "<tr class=\"$css\">";
	$display .= "<td class=\"tabletext\">$barcode</td>";
	$display .= "<td class=\"tabletext\">$str_cost_price</td>";
	$display .= "<td class=\"tabletext\">$increase_value</td><td class=\"tabletext\">".intranet_htmlspecialchars($remark)."</td>";
	$display .= "<td class=\"tabletext\">$reason</td>";
	$display .= "</tr>\n";
}

# tag information
$TAGS_OBJ[] = array($Lang['ePOS']['Inventory'], '', 0);
$MODULE_OBJ = $libPOS_ui->GET_MODULE_OBJ_ARR();

$PAGE_NAVIGATION[] = array($button_import);


include_once($PATH_WRT_ROOT."includes/libinterface.php");
$linterface = new interface_html();
$linterface->LAYOUT_START();

?>

<form name="form1" method="get" action="import_update.php">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
		</tr>
		<tr>
			<td>
				<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
					<?php if($error_exists){?>
						<tr>
							<td class="tabletext"><?= $i_Payment_Import_Error ?></td>
						</tr>
					<?php } else { ?>
						<tr>
							<td class="tabletext"><?= $i_Payment_Import_Confirm ?></td>
						</tr>
					<?php } ?>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
					<?=$display?>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
					<tr>
						<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" colspan="2">
				<?php if(!$error_exists){ ?>
					<?= $linterface->GET_ACTION_BTN($button_confirm, "submit", "") ?>
				<?php } ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='import.php?clear=1'") ?>
			</td>
		</tr>
	</table>
	<input type="hidden" name="confirm" value="1">
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>


