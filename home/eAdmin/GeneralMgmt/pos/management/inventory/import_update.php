<?php
// Using :
/*

 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libpos_item.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");


intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit();
}

$libpos = new libpos();



if($confirm==1){
	$li = new libdb();

	$li->Start_Trans();
	$sql = "SELECT ItemID,Barcode,CostPrice,AdjustedBy,Remark FROM TEMP_EPOS_INVENTORY_IMPORT";
	$data = $li->returnArray($sql,5);

	for ($i=0; $i<sizeof($data); $i++) {
		list($item_id, $barcode, $cost_price,$AdjustedBy, $remark) = $data[$i];
		$objItem = new POS_Item($item_id);
		$returnString = $objItem->Update_Inventory($AdjustedBy, true, $remark, $cost_price);
		if($returnString == '1') {
			$Result['Insert'.$i] = true;
		} else {
			$Result['Insert' . $i] = false;
		}
	}

	$sql = "DELETE FROM TEMP_EPOS_INVENTORY_IMPORT";
	$Result['ClearTempTable'] = $li->db_db_query($sql);

	if (in_array(false,$Result)) {
		$Msg = $Lang['ePayment']['DataImportUnsuccess'];
		$li->RollBack_Trans();
	}
	else {
		$Msg = $Lang['ePayment']['DataImportSuccess'];
		$li->Commit_Trans();
	}
	header ("Location: inventory.php?ReturnMsg=".urlencode($Msg)."&clear=1");
}
else {
	header("Location: import.php?clear=1");
}

intranet_closedb();
?>
