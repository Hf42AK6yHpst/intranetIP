<?php
// Using :
/*

 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libpos_item.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();



if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit();
}

$libpos = new libpos();

$libPOS_ui = new libpos_ui();
$CurrentPageArr['ePOS'] = 1;	// Top Menu
$CurrentPage['ManageInventory'] = 1;	// Left Menu

# tag information
$TAGS_OBJ[] = array($Lang['ePOS']['Inventory'], '', 0);
$MODULE_OBJ = $libPOS_ui->GET_MODULE_OBJ_ARR();
$linterface = new interface_html();
$linterface->LAYOUT_START($ReturnMsg);


if($clear==1){
	$sql="DROP TABLE TEMP_EPOS_INVENTORY_IMPORT";
	$libpos->db_db_query($sql);
}

$sql = "CREATE TABLE IF NOT EXISTS TEMP_EPOS_INVENTORY_IMPORT (
                                 ItemID int(11),
                                 Barcode varchar(100),
                                 AdjustedBy int(11),
								 Remark varchar(255),
								 CostPrice double(20,2)
                                ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
$libpos->db_db_query($sql);

$sql = "SELECT COUNT(*) FROM TEMP_EPOS_INVENTORY_IMPORT";
$temp = $libpos->returnVector($sql);
$lcount = $temp[0];


$PAGE_NAVIGATION[] = array($button_import);


if ($failed == "1") $SysMsg = $linterface->GET_SYS_MSG("import_failed");
if ($failed == "2") $SysMsg = $linterface->GET_SYS_MSG("import_failed");

if ($lcount != 0)
{
	?>
	<br />
	<form name="form1" method="get" action="">
		<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
			</tr>
			<tr>
				<td colspan="2">
					<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
						<tr>
							<td align="center">
								<?=$i_Payment_Import_ConfirmRemoval."<br />".$i_Payment_Import_ConfirmRemoval2?>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align="center" colspan="2">
					<?= $linterface->GET_ACTION_BTN($button_remove, "submit", "") ?>
				</td>
			</tr>
		</table>
		<input type="hidden" name="clear" value="1">
	</form>
	<br />
	<?
}
else
{
	?>
	<br />
	<form name="form1" method="post" action="import_confirm.php" enctype="multipart/form-data">
		<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
			</tr>
			<tr>
				<td colspan="2">
					<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
						<tr>
							<td colspan="2" align="right" class="tabletext">
								<?=$SysMsg?>
							</td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
								<?= $i_select_file ?>
							</td>
							<td class="tabletext">
								<input class="file" type="file" name="userfile"><br>
								<?= $linterface->GET_IMPORT_CODING_CHKBOX() ?>
							</td>
						</tr>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
								<?= $i_general_Format ?>
							</td>
							<td class="tabletext">
								<table border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td valign="top" class="tabletext"><input type="radio" name="format" value="1" id="format1" checked></td>
										<td class="tabletext">
											<label for="format1"><?=$i_ePOS_InventoryImportFileDescription1?></label><br />
											<a class="tablelink" href="<?= GET_CSV("sample.csv")?>" target="_blank">[<?=$i_general_clickheredownloadsample?>]</a>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
						<tr>
							<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align="center" colspan="2">
					<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
					<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='inventory.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
				</td>
			</tr>
		</table>
	</form>
	<br />
	<?php
}
$linterface->LAYOUT_STOP();
intranet_closedb();
?>