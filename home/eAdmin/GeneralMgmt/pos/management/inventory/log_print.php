<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libpos_item.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit();
}

$libPOS_ui = new libpos_ui();
$linterface = new interface_html();


$ItemID = stripslashes($_REQUEST['ItemID']);
$StartDate = stripslashes($_REQUEST['StartDate']);
$EndDate = stripslashes($_REQUEST['EndDate']);
$Keyword = stripslashes($_REQUEST['Keyword']);
$LogType = stripslashes($_REQUEST['LogType']);
$SortField = stripslashes($_REQUEST['field']);
$Order = stripslashes($_REQUEST['order']);

echo $libPOS_ui->Get_Item_Inventory_Change_Log_Printing_UI($ItemID, $StartDate, $EndDate, $Keyword, $Order,$LogType);
?>


<?
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>
