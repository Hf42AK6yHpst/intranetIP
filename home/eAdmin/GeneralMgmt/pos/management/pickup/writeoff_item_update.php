<?
/*
 *  2020-04-07 Cameron
 *      - pass argument ActionFrom to Void_POS_Transaction_By_Item() 
 *      
 *  2020-04-04 Cameron
 *      - apply standardizeFormPostValue() to remark field to eliminate backslash
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	echo 'die';
	intranet_closedb();
	exit();
}

$LogID = stripslashes($_REQUEST['LogID']);
$ItemRemark = (is_array($_REQUEST['ItemRemark']))? $_REQUEST['ItemRemark']:array();
$ItemQty = (is_array($_REQUEST['ItemQty']))? $_REQUEST['ItemQty']:array();
$ActionFrom = standardizeFormPostValue($_POST['ActionFrom']);

$POS = new libpos();

$success = array();

foreach($ItemQty as $itemID => $quantity){
    $ItemRemark[$itemID] = standardizeFormPostValue($ItemRemark[$itemID]);
	$success[] = $POS->Void_POS_Transaction_By_Item($LogID,$ItemRemark[$itemID],$itemID, $quantity, $ActionFrom);
}
if (!in_array(false, $success)) {
	$xmsg = 'UpdateSuccess';
}
else {
	$xmsg = 'UpdateUnsuccess';
}
intranet_closedb();
header("location: detail.php?LogID=".$LogID."&xmsg=".$xmsg);
?>