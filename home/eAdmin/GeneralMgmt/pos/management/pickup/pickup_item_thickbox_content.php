<?
// Using : 
/*
 * 2019-01-03 (Henry): file created
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libpos_item.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit();
}

$libPOS_ui = new libpos_ui();

$LogID = trim($_REQUEST['LogID']);
$ItemIDs = $_REQUEST['ItemIDs'];

$pageNo = $_REQUEST['pageNo'];
$order = $_REQUEST['order'];
$field = $_REQUEST['field'];
$PageSize = $_REQUEST['num_per_page'];

echo $libPOS_ui->Get_Pickup_Thickbox_Content($LogID, $ItemIDs, $pageNo, $PageSize, $order, $field);

?>


<script>
function js_Pickup(){
	if(confirm("<?=$Lang['ePOS']['jsWarningArr']['CollectTheItems']?>")){
		var objForm = document.getElementById('thickboxForm');
		objForm.action = 'pickup_item_update.php';
		objForm.target = '_self';
		objForm.submit();
	}
}
</script>
<?
intranet_closedb();
?>