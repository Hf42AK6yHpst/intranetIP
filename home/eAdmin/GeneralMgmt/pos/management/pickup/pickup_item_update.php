<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_item.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit();
}

$libPOS = new libpos();

$LogID = stripslashes($_REQUEST['LogID']);
$ItemQtyArray = $_REQUEST['ItemQty'];

$result = $libPOS->Pickup_Item($LogID, $ItemQtyArray);
$xmsg = $result?'UpdateSuccess':'UpdateUnsuccess';
intranet_closedb();
header("location: detail.php?LogID=".$LogID."&xmsg=".$xmsg);
?>
