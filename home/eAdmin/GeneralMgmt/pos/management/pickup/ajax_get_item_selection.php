<?
/*
 *  2020-04-04 Cameron
 *      - pass argument $LogID to Get_Item_Selection_By_Price() to avoid change item back
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	echo 'die';
	intranet_closedb();
	exit();
}

$ItemID = $_REQUEST['ItemID'];
$CategoryID = $_REQUEST['CategoryID'];
$Price = $_REQUEST['Price'];
$LogID = $_POST['LogID'];

$POSUI = new libpos_ui();

echo $POSUI->Get_Item_Selection_By_Price($ItemID, '', $CategoryID, $Price, '', 0, 1, 0, 0, $LogID);

intranet_closedb();
?>