<?php
// Editing by 
/*
 * 2018-07-13 (Anna) : Added void for void transaction
 * 2017-03-17 (Carlos): Pass in $field and $order for sorting data to follow UI.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_opendb();
$libpos_ui = new libpos_ui();

$ClassName = trim(stripslashes(urldecode($_REQUEST['ClassName'])));
$Keyword = trim(stripslashes(urldecode($_REQUEST['keyword'])));


echo $libpos_ui->Get_POS_Transaction_Report_PrintLayout($FromDate, $ToDate,$ClassName, $RecordStatus, $Keyword, $field, $order,$void); 

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>
