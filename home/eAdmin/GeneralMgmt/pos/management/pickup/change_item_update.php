<?
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	echo 'die';
	intranet_closedb();
	exit();
}

$LogID = stripslashes($_REQUEST['LogID']);
$ItemChange = (is_array($_REQUEST['ItemIDFilter']))? $_REQUEST['ItemIDFilter']:array();
$ItemQty = (is_array($_REQUEST['ItemQty']))? $_REQUEST['ItemQty']:array();

$POS = new libpos();

$success = array();

foreach($ItemQty as $itemID => $quantity){
	$success[] = $POS->Change_POS_Transaction_By_Item($LogID,$ItemChange[$itemID],$itemID, $quantity);
//	$success[] = $POS->Void_POS_Transaction_By_Item($LogID,$ItemRemark[$itemID],$itemID, $quantity);
}
if (!in_array(false, $success)) {
	$xmsg = 'UpdateSuccess';
}
else {
	$xmsg = 'UpdateUnsuccess';
}
intranet_closedb();
header("location: detail.php?LogID=".$LogID."&xmsg=".$xmsg);
?>