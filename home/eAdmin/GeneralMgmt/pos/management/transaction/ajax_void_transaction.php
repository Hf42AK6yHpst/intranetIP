<?
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	echo 'die';
	intranet_closedb();
	exit();
}

$TransactionLogID = $_REQUEST['TransactionLogID'];
$Remark = trim(stripslashes(urldecode($_REQUEST['Remark'])));
$InventoryReturn = (is_array($_REQUEST['InventoryReturn']))? $_REQUEST['InventoryReturn']:array();

$POS = new libpos();
if ($POS->Void_POS_Transaction($TransactionLogID,$Remark,$InventoryReturn)) {
	echo '1';
}
else {
	echo '0';
}

intranet_closedb();
?>