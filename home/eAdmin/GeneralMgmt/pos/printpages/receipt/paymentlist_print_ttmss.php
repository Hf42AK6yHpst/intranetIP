<?php
ini_set('memory_limit','256M');
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ttmss_payment_receipt']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$intranet_session_language = 'b5';

$linterface = new interface_html();
$lpayment = new libpayment();


require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");


$margin= 5; // mm
$margin_top = $margin;
$margin_bottom = $margin;
$margin_left = $margin;
$margin_right = $margin;
$margin_top = 65;

$pdf = new mPDF('','A5-L',0,'',$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer);
$pdf->shrink_tables_to_fit = 1;

$pdf->backupSubsFont = array('mingliu');
$pdf->useSubstitutions = true;

$ttmss_payment_receipt_data = $sys_custom['ttmss_payment_receipt_data'][$sys_custom['ttmss_payment_receipt_code']];

$logo = '/images/kis/'.$ttmss_payment_receipt_data['logo'];
$address_1 = $ttmss_payment_receipt_data['address_1'];
$address_2 = $ttmss_payment_receipt_data['address_2'];
$receipt_tel = $ttmss_payment_receipt_data['tel'];
$receipt_fax = $ttmss_payment_receipt_data['fax'];
$receipt_email = $ttmss_payment_receipt_data['email'];
$receipt_website = $ttmss_payment_receipt_data['website'];
$receipt_today = date("Y-m-d");
$receipt_remark = $ReceiptRemark;
$receipt_staff_name = $_SESSION['EnglishName'];


$lclass = new libclass();

if ($StudentID == "")
{
	if ($ClassName == "")
	{
		echo "請選擇學生或班別";
		intranet_closedb();
		exit();
		//$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 ORDER BY ClassName, ClassNumber, EnglishName";
		//$target_students = $lclass->returnVector($sql);
	}
	else
	{
		# All students in $ClassName
		$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND ClassName = '$ClassName' ORDER BY ClassName, ClassNumber, EnglishName";
		$target_students = $lclass->returnVector($sql);
	}
}
else
{
	if (is_array($StudentID)) {
		$target_students = $StudentID;
	}
	else {
		$target_students = array($StudentID);
	}
}

$conds = "";
if ($date_from != "" && $date_to != "") {
	$conds .= " AND ppdr . DateInput between '$date_from' and '$date_to 23:59:59'";
}

# Get Latest Balance
$lpayment = new libpayment();
$list = "'".implode("','", $target_students)."'";
$sql = "SELECT StudentID, Balance FROM PAYMENT_ACCOUNT WHERE StudentID IN ($list)";
$temp = $lpayment->returnArray($sql,2);
$balance_array = build_assoc_array($temp);


$POS = new libpos();
$record_count = 0;

for ($j=0; $j<sizeof($target_students); $j++) {
	$record_data = array();

	$target_id = $target_students[$j];
	$lu = new libuser($target_id);
	$studentname = $lu->UserNameLang()." (".$lu->ClassName.")";
	$i_title = $studentname;

	$receipt_student_name = $studentname;
	$receipt_student_number = $ttmss_payment_receipt_data['code'] . "-" . $lu->UserLogin;


	$curr_balance = $balance_array[$target_id];


	$amountConds = " IF(pil.ItemLogID IS NULL,ppdr.VoidAmount, pil.Amount) < ppdr.ItemQty";
	$voidConds = "AND pt.VoidBy IS NULL AND pt.VoidLogID IS NULL";

	$sql  = "SELECT
					pt.LogID 
				from 
					POS_ITEM as pi
					inner join
					PAYMENT_PURCHASE_DETAIL_RECORD as ppdr
					on pi.ItemID = ppdr.ItemID
					inner join
					POS_TRANSACTION as pt 
					on pt.LogID = ppdr.TransactionLogID and ppdr.InvoiceNumber = pt.InvoiceNumber 
						$conds
					left join PAYMENT_OVERALL_TRANSACTION_LOG as potl
					on pt.LogID = potl.LogID AND pt.VoidBy IS NULL AND pt.VoidDate IS NULL AND pt.VoidLogID IS NULL 
					left join PAYMENT_OVERALL_TRANSACTION_LOG as potl2 
					on potl2.LogID=pt.VoidLogID AND pt.VoidBy IS NOT NULL AND pt.VoidDate IS NOT NULL AND pt.VoidLogID IS NOT NULL 
					LEFT JOIN POS_ITEM_LOG as pil ON pil.ItemID=ppdr.ItemID AND pil.LogType=2 AND pil.RefLogID=potl2.LogID 
					LEFT JOIN 
					INTRANET_USER as u
					on potl.StudentID = u.UserID or potl2.StudentID = u.UserID
					LEFT JOIN
					INTRANET_ARCHIVE_USER as au
					on potl.StudentID = au.UserID or potl2.StudentID = au.UserID 
				WHERE
					$amountConds
                    $voidConds
                    AND potl.StudentID = '$target_id'
				Group By 
					pt.LogID
					";

	$logid_array = $POS->returnVector($sql);

	$total_amount = 0;
	$temp_result = array();
	foreach($logid_array as $log_id) {
		$result = $POS->Get_POS_Transaction_Log_Data($log_id, '0');
		foreach($result as $data) {
			$temp_result[$data['Barcode']][] = $data;
		}
	}

	if(count($temp_result) == 0) {
		continue;
	}

	$sql = "INSERT INTO PAYMENT_PAYMENT_RECEIPT_NUMBER
               (ReceiptType, FromDate, ToDate, StudendIDs, DateInput)
        VALUES ('ePOS','".$lpayment->Get_Safe_Sql_Query($date_from)."','".$lpayment->Get_Safe_Sql_Query($date_to)." 23:59:59','".implode(',', $target_students)."', now())";

	$lpayment->db_db_query($sql);

	if($lpayment->db_affected_rows() <= 0) {
		continue;
	}

	$receipt_id = $lpayment->db_insert_id();

	$sql_val = array();
	foreach($temp_result as $barcode=>$result) {
		$subtotal_amount = 0;
		$item_qty = 0;
		foreach($result as $data) {
			$subtotal_amount += $data['GrandTotal'];
			$item_qty += $data['ItemQty'];
			$total_amount += $data['GrandTotal'];
			$sql_val[] = "('".$lu->Get_Safe_Sql_Query($receipt_id)."','".$lu->Get_Safe_Sql_Query($data['LogID'])."')";
		}
		$temp = array();
		$temp[] = $result[0]['Barcode'];
		$temp[] = $result[0]['ItemName'];
		$temp[] = $item_qty;
		$temp[] = number_format($result[0]['ItemSubTotal'], 2);
		$temp[] = number_format($subtotal_amount, 2);
		$record_data[] = $temp;


	}

	$sql = "INSERT INTO PAYMENT_PAYMENT_RECEIPT_NUMBER_ITEM
               (ReceiptID, PaymentLogID)
        VALUES ".implode(',', $sql_val);

	$lpayment->db_db_query($sql);


	$receipt_code = $ttmss_payment_receipt_data['code'] . "-" . str_pad($receipt_id, 6, '0', STR_PAD_LEFT);

	$record_count++;

	$pdf->DefHTMLHeaderByName(
		'header',
		'<div style="text-align: center; margin-top: 10;">
		<table style="width: 100%">
			<tr>
					<td width="150"></td>
					<td style="text-align: center;"><img src="' . $logo . '"  style="width: 400; height: 80; margin: 0;"  /></td>
					<td width="150" valign="top">' . $receipt_code . '</td>
			</tr>
		</table>
		<div style="text-align: center; font-size: 11pt">' . $address_1 . '</div>
		<div style="text-align: center; font-size: 11pt">' . $address_2 . '</div>
		<div style="text-align: center; font-size: 8pt">Tel: ' . $receipt_tel . ' Fax: ' . $receipt_fax . ' Email: ' . $receipt_email . ' Website: ' . $receipt_website . '</div>
		<table style="width: 100%">
			<tr>
					<td width="150"></td>
					<td style="text-align: center; font-size: 12pt; font-weight: bold; text-decoration: underline;">收 據</td>
					<td width="150" valign="top" style="font-weight: bold;">' . $receipt_today . '</td>
			</tr>
		</table>
		<table style="width: 100%">
			<tr>
					<td width="355" style="text-align: left; font-size: 12pt;">學生姓名 : ' . $receipt_student_name . '</td>
					<td style="text-align: left; font-size: 12pt;">學生編號 : ' . $receipt_student_number . '</td>
			</tr>
		</table>
		<table style="width: 100%; font-weight: bold;">
			<tr>
					<td width="130" style="text-align: left; font-size: 9pt;">編號</td>
					<td style="text-align: left; font-size: 10pt;">名稱</td>
					<td width="100" style="text-align: right; font-size: 9pt;">數量</td>
					<td width="100" style="text-align: right; font-size: 9pt;">單價</td>
					<td width="100" style="text-align: right; font-size: 9pt;">金額</td>
			</tr>
		</table>
		<table style="width: 100%"><tr><td style ="border-bottom:2px solid black; width:100%;"> </td></tr></table>
	</div>
');

	$pdf->SetHTMLHeaderByName('header');
	$pdf->AddPage();

	$remain_item = count($record_data);
	foreach ($record_data as $temp) {
		$x = '<table style="width: 100%; font-weight: bold; overflow: wrap;" autosize="1">
			<tr>
					<td width="130" style="text-align: left; font-size: 8pt;" valign="top">' . $temp[0] . '</td>
					<td style="text-align: left; font-size: 8pt;" valign="top">' . $temp[1] . '</td>
					<td width="100" style="text-align: right; font-size: 8pt;" valign="top">' . $temp[2] . '</td>
					<td width="100" style="text-align: right; font-size: 8pt;" valign="top">' . $temp[3] . '</td>
					<td width="100" style="text-align: right; font-size: 8pt;" valign="top">' . $temp[4] . '</td>
			</tr>';
		$x .= '</table>';
		$pdf->WriteHTML($x);

		$remain_item--;

		if($pdf->y > 115) {
			if($remain_item > 0) {
				$pdf->AddPage();
			}
		}
	}

	$total_amount = number_format($total_amount, 2);
	$footer = '<div style="position: absolute; left: 0; bottom: 0; margin-left: 20; margin-right: 20;">
				<table style="width: 100%"><tr><td style ="border-bottom:2px solid black; width:100%;"> </td></tr></table>';
	$footer .= '<table style="width: 100%; font-weight: bold; margin-bottom: 50">
			<tr>
					<td style="text-align: left; font-size: 10pt;">' . $receipt_remark . '</td>
					<td width="100" style="text-align: right; font-size: 10pt;">總金額:</td>
					<td width="100" style="text-align: right; font-size: 10pt;">' . $total_amount . '</td>
			</tr>
		</table>';


	$footer .= '<table style="width: 100%; margin-bottom: 20;">
			<tr>
					<td style="text-align: right; font-size: 9pt;">職員: ' . $receipt_staff_name . '</td>
					<td width="250" style="text-align: right; font-size: 9pt;">職員簽名: ___________________</td>
			</tr>
		</table>
		</div>
		';

	$pdf->WriteHTML($footer);

}

if($record_count > 0) {
	$pdf->Output('ePOS_'.$receipt_today.'.pdf', 'I');
} else {
	echo "沒有資料";
}
intranet_closedb();