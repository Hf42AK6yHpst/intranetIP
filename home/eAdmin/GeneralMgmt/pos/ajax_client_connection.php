<?php
// using ivan
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");

intranet_opendb();

# Get data
$Action = stripslashes($_REQUEST['Action']);

$returnString = '';
if ($Action == "CheckSettings")
{
	$libpos = new libpos();
	$SettingArr = $libpos->Get_POS_Settings();
	$AllowClientConnect = $SettingArr['AllowClientProgramConnect'];
	
	$returnString = ($AllowClientConnect == 1)? 1 : 0;
	
	// for dev
	//$returnString = 0;
}
else if ($Action == "UpdateSettings")
{
	$SettingArr['AllowClientProgramConnect'] = $_REQUEST['Value'];
	
	include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
	$GeneralSetting = new libgeneralsettings();
	$returnString = $GeneralSetting->Save_General_Setting('ePOS', $SettingArr);
}

intranet_closedb();

echo $returnString;
?>