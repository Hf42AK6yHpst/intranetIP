<?php

$moduleEnableAssoAry = $_POST['moduleEnableAssoAry'];


### get all modules that can be applied to teacher app
$appType = $eclassAppConfig['appType']['Teacher'];
$appApplicableModuleAry = $indexVar['libeClassApp']->getAppApplicableModule($appType);
$numOfApplicableModule = count($appApplicableModuleAry);


$consolidatedModuleAccessRightAssoAry = array();
for ($i=0; $i<$numOfApplicableModule; $i++) {
	$_moduleCode = $appApplicableModuleAry[$i];
	
	$__yearId = 0;
	$__enableModuleInForm = ($moduleEnableAssoAry[$_moduleCode][$__yearId])? true : false;
	$consolidatedModuleAccessRightAssoAry[$_moduleCode][$__yearId] = $__enableModuleInForm;
}

$success = $indexVar['libeClassApp']->saveModuleAccessRightSettings($appType, $consolidatedModuleAccessRightAssoAry);
$returnMsgKey = ($success)? 'UpdateSuccess' : 'UpdateUnsuccess';

header('location: ?task=teacherApp/access_right/list&appType='.$appType.'&returnMsgKey='.$returnMsgKey);
?>