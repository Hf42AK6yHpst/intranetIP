<?php
// using : 
/**  Change Log
 *  Date: 2017-04-25	Carlos
 * 		- Added Department field for DHL.
 * 	Date: 2016-02-22	Kenneth
 * 		- Change order by last login time descendingly
 */
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");


$curTab = ($_POST['curTab'])? $_POST['curTab'] : $_GET['curTab'];
$curTab = ($curTab=='')? 'loggedIn' : $curTab;
$keyword = ($_POST['keyword'])? standardizeFormPostValue($_POST['keyword']) : standardizeFormGetValue($_GET['keyword']);


$lclass = new libclass();


$CurrentPage = 'TeacherApp_LoginStatus';
$MODULE_OBJ = $indexVar['libeClassApp']->getModuleObjArr();
$TAGS_OBJ[] = array($Lang['eClassApp']['LoggedInUser'], 'javascript: goToPage(\'loggedIn\');', $curTab=='loggedIn');
$TAGS_OBJ[] = array($Lang['eClassApp']['NotLoggedInUser'], 'javascript: goToPage(\'notLoggedIn\');', $curTab=='notLoggedIn');
$indexVar['linterface']->LAYOUT_START($returnMsg);


### content tool
// $btnAry[] = array($btnClass, $onclickJs, $displayLang, $subBtnAry);
$btnAry = array();
$btnAry[] = array('export', 'javascript: goExport();');
$btnAry[] = array('print', 'javascript: goPrint();');
$htmlAry['contentTool'] = $indexVar['linterface']->Get_Content_Tool_By_Array_v30($btnAry);


### search box
$htmlAry['searchBox'] = $indexVar['linterface']->Get_Search_Box_Div('keyword', $keyword);


### push message status filtering
if ($curTab == 'loggedIn') {
	$htmlAry['pushMessageStatusSel'] = $indexVar['libeClassApp']->getPushMessageAcceptStatusSelection('pushMessageStatusSel', 'pushMessageStatus', $pushMessageStatus, 'changedPushMessageStatusSelection();');
}
else {
	$pushMessageStatus = '';
}

$field_shift = 0;
if($sys_custom['DHL']){
	$field_shift = 3;
}

### db table
$order = ($order == '') ? 0 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 2+$field_shift : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 50 : $numPerPage;

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("UserLogin", "TeacherName");
if($sys_custom['DHL']){
	$li->field_array[] = 'Company';
	$li->field_array[] = 'Division';
	$li->field_array[] = 'Department';
}
if ($curTab == 'loggedIn') {
	$li->field_array[] = 'LastLoginTime';
}
$li->sql = $indexVar['libeClassApp']->getTeacherAppLoginUserSql($curTab, $keyword, $outputMode='', $pushMessageStatus);
$li->no_col = count($li->field_array) + 1;
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='25' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='30%' >".$li->column($pos++, $Lang['General']['UserLogin'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $i_identity_teachstaff)."</th>\n";
if($sys_custom['DHL']){
	$li->column_list .= "<th>".$li->column($pos++, $Lang['DHL']['Company'])."</th>\n";
	$li->column_list .= "<th>".$li->column($pos++, $Lang['DHL']['Division'])."</th>\n";
	$li->column_list .= "<th>".$li->column($pos++, $Lang['DHL']['Department'])."</th>\n";
}
if ($curTab == 'loggedIn') {
	$li->column_list .= "<th width='33%' >".$li->column($pos++, $Lang['eClassApp']['LastLoginTime'])."</th>\n";
}
$htmlAry['dataTable'] = $li->display();


?>
<script type="text/javascript">
$(document).ready( function() {
	
});

function goToPage(targetTab) {
	$('input#curTab').val(targetTab);
	$('input#task').val('teacherApp/login_status/list');
	$('form#form1').attr('target', '_self').submit();
}

function changedFormClassSelection() {
	reloadPage();
}

function changedPushMessageStatusSelection() {
	reloadPage();
}

function reloadPage() {
	$('input#task').val('teacherApp/login_status/list');
	$('form#form1').attr('target', '_self').submit();
}

function goPrint() {
	$('input#outputMode').val('html');
	$('input#task').val('teacherApp/login_status/report');
	$('form#form1').attr('target', '_blank').submit();
	
	// reset target and task for DB table action
	$('form#form1').attr('target', '_self');
	$('input#task').val('parentApp/login_status/list');
}

function goExport() {
	$('input#outputMode').val('csv');
	$('input#task').val('teacherApp/login_status/report');
	$('form#form1').attr('target', '_blank').submit();
	
	// reset target and task for DB table action
	$('form#form1').attr('target', '_self');
	$('input#task').val('parentApp/login_status/list');
}
</script>
<form name="form1" id="form1" method="POST">
	<div class="content_top_tool">
		<?=$htmlAry['contentTool']?>
		<?=$htmlAry['searchBox']?>
		<br style="clear:both;">
	</div>
	
	<div class="table_board">
		<div class="table_filter">
			<?=$htmlAry['pushMessageStatusSel']?>
		</div>
		<p class="spacer"></p>
		<?=$htmlAry['dataTable']?>
	</div>
	
	<input type="hidden" name="curTab" id="curTab" value="<?=$curTab?>" />
	<input type="hidden" name="task" id="task" value="" />
	<input type="hidden" name="outputMode" id="outputMode" value="" />
	<input type="hidden" id="pageNo" name="pageNo" value="<?=$li->pageNo?>" />
	<input type="hidden" id="order" name="order" value="<?=$li->order?>" />
	<input type="hidden" id="field" name="field" value="<?=$li->field?>" />
	<input type="hidden" id="page_size_change" name="page_size_change" value="" />
	<input type="hidden" id="numPerPage" name="numPerPage" value="<?=$li->page_size?>" />
</form>
<?
$indexVar['linterface']->LAYOUT_STOP();
?>