<?php
// using : 
/*
 * 2017-04-25 (Carlos): Added Department field for DHL.
 */
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");


$curTab = ($_POST['curTab'])? $_POST['curTab'] : $_GET['curTab'];
$outputMode = ($_POST['outputMode'])? $_POST['outputMode'] : $_GET['outputMode'];
$keyword = ($_POST['keyword'])? standardizeFormPostValue($_POST['keyword']) : standardizeFormGetValue($_GET['keyword']);
$pushMessageStatus = ($_POST['pushMessageStatus'])? $_POST['pushMessageStatus'] : $_GET['pushMessageStatus'];


### db table
$order = ($order == '') ? 1 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 0 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 50 : $numPerPage;

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("UserLogin", "TeacherName");
if($sys_custom['DHL']){
	$li->field_array[] = 'Company';
	$li->field_array[] = 'Division';
	$li->field_array[] = 'Department';
}
if ($curTab == 'loggedIn') {
	$li->field_array[] = 'LastLoginTime';
}
$li->sql = $indexVar['libeClassApp']->getTeacherAppLoginUserSql($curTab, $keyword, $outputMode, $pushMessageStatus);
$li->no_col = count($li->field_array) + 1;
$li->IsColOff = "IP25_table";


### build report sql
$sql = Get_Export_SQL_From_DB_Table_SQL($li->built_sql());
$resultAry = $li->returnArray($sql,null,2);


### build report header
$headerAry = array();
$headerAry[] = $Lang['General']['UserLogin'];
$headerAry[] = $i_identity_teachstaff;
if($sys_custom['DHL']){
	$headerAry[] = $Lang['DHL']['Company'];
	$headerAry[] = $Lang['DHL']['Division'];
	$headerAry[] = $Lang['DHL']['Department'];
}
if ($curTab == 'loggedIn') {
	$headerAry[] = $Lang['eClassApp']['LastLoginTime'];
}


### output report
if ($outputMode == 'html') {
	// print
	include_once($PATH_WRT_ROOT."templates/".$LAYOUT_SKIN."/layout/print_header.php");
	include_once($PATH_WRT_ROOT.'/includes/table/table_commonTable.php');
			
	$numOfHeader = count($headerAry);
	$numOfRow = count($resultAry);
	
	$tableObj = new table_commonTable();
	$tableObj->setTableType('view');
	$tableObj->setApplyConvertSpecialChars(false);
	
	$tableObj->addHeaderTr(new tableTr());
	$tableObj->addHeaderCell(new tableTh('#'));
	for ($i=0; $i<$numOfHeader; $i++) {
		$tableObj->addHeaderCell(new tableTh($headerAry[$i]));
	}
	
	for ($i=0; $i<$numOfRow; $i++) {
		$_rowContentAry = $resultAry[$i];
		$_numOfCol = count($_rowContentAry);
		
		$tableObj->addBodyTr(new tableTr());
		$tableObj->addBodyCell(new tableTd($i+1));
		
		for ($j=0; $j<$_numOfCol; $j++) {
			$tableObj->addBodyCell(new tableTd($_rowContentAry[$j]));
		}
	}
	
	echo '<table width="100%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right">'.$indexVar['linterface']->GET_SMALL_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit2").'</td>
	</tr>
	</table>';
	echo $tableObj->returnHtml();
}
else if ($outputMode == 'csv'){
	// export csv
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	$lexport = new libexporttext();
	
	if ($curTab == 'loggedIn') {
		$fileName = 'logged_in_user.csv';
	}
	else if ($curTab == 'notLoggedIn') {
		$fileName = 'not_logged_in_user.csv';
	}
	
	$exportContent = $lexport->GET_EXPORT_TXT($resultAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=1);
	$lexport->EXPORT_FILE($fileName, $exportContent);
}
?>