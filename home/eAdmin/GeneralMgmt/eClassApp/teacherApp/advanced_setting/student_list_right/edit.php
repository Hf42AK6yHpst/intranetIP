<?php
// editing:

/*
 * change log:
 * 2015-05-07  Ivan: add option "Allow to send push message to own class students" and "Allow to send push message to all students"
 * 2015-01-12  Qiao: create file
 */
 
$returnMsgKey = $_GET['returnMsgKey'];
$CurrentPage = 'TeacherApp_AdvancedSetting';

$MODULE_OBJ = $indexVar['libeClassApp']->getModuleObjArr();
$TAGS_OBJ = $indexVar['libeClassApp']->getTeacherAppAdvancedSettingsTagObj($eclassAppConfig['moduleCode']['studentStatus']);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$indexVar['linterface']->LAYOUT_START($returnMsg);

$eClassAppSettingsObj = $indexVar['libeClassApp']->getAppSettingsObj();

### get teacher view studentList and studentContact right
$classTeacherClassStudentListRight = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowViewAllStudentList']);
$classTeacherClassStudentContactRight = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowViewStudentContact']);
$nonClassTeacherClassStudentListRight = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPNonClassTeacherAllowViewAllStudentList']);
$nonClassTeacherClassStudentContactRight = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPNonClassTeacherAllowViewStudentContact']);
$classTeacherCanViewOwnClassRight = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowSendPushMessageToClassStudent']);
$classTeacherCanViewAllClassRight = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowSendPushMessageToAllStudent']);
$nonClassTeacherCanViewAllClassRight = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPNonClassTeacherAllowSendPushMessageToAllStudent']);

$classTeacherClassStudentContactClassRight = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowViewStudentContactClass']);

if($classTeacherClassStudentContactRight== null){
$classTeacherClassStudentContactRight = 1;
}

if($classTeacherClassStudentContactClassRight== null){
	$classTeacherClassStudentContactClassRight = 1;
}


$classTeacherCanViewOwnClassRightStudentApp = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowSendPushMessageToClassStudentApp']);
$classTeacherCanViewAllClassRightStudentApp = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowSendPushMessageToAllStudentApp']);
$nonClassTeacherCanViewAllClassRightStudentApp = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPNonClassTeacherAllowSendPushMessageToAllStudentApp']);


$classTeacherClassStudentListStatus= ($classTeacherClassStudentListRight == 1) ? true:false;
$classTeacherClassStudentContactStatus = ($classTeacherClassStudentContactRight == 1) ? true:false;
$nonClassTeacherClassStudentListStatus= ($nonClassTeacherClassStudentListRight == 1) ? true:false;
$nonClassTeacherClassStudentContactStatus = ($nonClassTeacherClassStudentContactRight == 1) ? true:false;

$classTeacherClassStudentContactClassStatus = ($classTeacherClassStudentContactClassRight == 1) ? true:false;

$formAry[] = array('TeacherStyle'=>'ClassTeacher');
$formAry[] = array('TeacherStyle'=>'NonClassTeacher');
$checkAry[] = array('checkName'=>'AllowViewAllStudentList','ClassTeacher'=>$classTeacherClassStudentListStatus,'NonClassTeacher'=>$nonClassTeacherClassStudentListStatus);
$checkAry[] = array('checkName'=>'AllowViewStudentContactClass','ClassTeacher'=>$classTeacherClassStudentContactClassStatus,'NonClassTeacher'=>false);

$checkAry[] = array('checkName'=>'AllowViewStudentContact','ClassTeacher'=>$classTeacherClassStudentContactStatus,'NonClassTeacher'=>$nonClassTeacherClassStudentContactStatus);

$checkAry[] = array('checkName'=>'AllowSendPushMessageToClassStudentApp','ClassTeacher'=>$classTeacherCanViewOwnClassRightStudentApp,'NonClassTeacher'=>false);

$checkAry[] = array('checkName'=>'AllowSendPushMessageToClassStudent','ClassTeacher'=>$classTeacherCanViewOwnClassRight,'NonClassTeacher'=>false);

$checkAry[] = array('checkName'=>'AllowSendPushMessageToAllStudentApp','ClassTeacher'=>$classTeacherCanViewAllClassRightStudentApp,'NonClassTeacher'=>$nonClassTeacherCanViewAllClassRightStudentApp);

$checkAry[] = array('checkName'=>'AllowSendPushMessageToAllStudent','ClassTeacher'=>$classTeacherCanViewAllClassRight,'NonClassTeacher'=>$nonClassTeacherCanViewAllClassRight);

$_isDisableNonTeacherViewContact = $nonClassTeacherClassStudentListStatus ? false:true;
$_isDisableTeacherViewContact = $classTeacherClassStudentListStatus ? false:true;

### calculate column width
$moduleColWidth = 25;
$formColWidth = floor((100 - $moduleColWidth) / 2);

### construct table content
$x = '';
$linterface = new interface_html();
$htmlAry['oleCategoryMappingRemarksTable'] = $linterface->Get_Warning_Message_Box($Lang['General']['Remark'], $Lang['eClassApp']['StudentStatus']['REMARK']);
$x .= $htmlAry['oleCategoryMappingRemarksTable'];
$x .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
	$x .= '<thead>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<th style="width:'.$moduleColWidth.'%;">&nbsp;</th>'."\r\n";
			for ($i=0; $i<count($formAry); $i++) {
				$_formName = $formAry[$i]['TeacherStyle'];
				$x .= '<th style="width:'.$formColWidth.'%; text-align:center;">'.$Lang['eClassApp'][$_formName].'</th>';
			}
		$x .= '</tr>'."\r\n";
	$x .= '</thead>'."\r\n";
	$x .= '<tbody>'."\r\n";
		for ($i=0; $i<count($checkAry); $i++) {
			$_moduleCodeAry = $checkAry[$i];
			$_checkName = $Lang['eClassApp'][$_moduleCodeAry['checkName']];
			
			$x .= '<tr>'."\r\n";
				$x .= '<td>'.$_checkName.'</td>'."\r\n";
				for ($j=0; $j<count($formAry); $j++) {
					$__teacherStyle = $formAry[$j]['TeacherStyle'];
					
					if (($__teacherStyle == 'NonClassTeacher' && $_moduleCodeAry['checkName'] == 'AllowSendPushMessageToClassStudent') ||
						($__teacherStyle == 'NonClassTeacher' && $_moduleCodeAry['checkName'] == 'AllowSendPushMessageToClassStudentApp') ||
						($formAry[$j]['TeacherStyle'] == 'NonClassTeacher' && $_moduleCodeAry['checkName'] == 'AllowViewStudentContactClass')
                    ) {
                    	$__checkbox = $Lang['General']['EmptySymbol'];
                    }
                    else {
                    	$__isChecked = $_moduleCodeAry[$__teacherStyle];
	                    $__isDisable = false;
	                    if(($_moduleCodeAry['checkName'] == 'AllowViewStudentContact' || $_moduleCodeAry['checkName'] == 'AllowSendPushMessageToAllStudent' || $_moduleCodeAry['checkName'] == 'AllowSendPushMessageToAllStudentApp')&&($__teacherStyle == 'NonClassTeacher')){
	                    	$__isDisable = $_isDisableNonTeacherViewContact;	
	                    }
	                    else if (($_moduleCodeAry['checkName'] == 'AllowSendPushMessageToAllStudent' || $_moduleCodeAry['checkName'] == 'AllowSendPushMessageToAllStudentApp')&&($__teacherStyle == 'ClassTeacher')) {
	                    	$__isDisable = $_isDisableTeacherViewContact;
	                    }
						$__checkboxId = $_moduleCodeAry['checkName'].'_'.$__teacherStyle;
						$__checkboxName = 'moduleEnableAssoAry['.$__teacherStyle.']['.$_moduleCodeAry['checkName'].']';
						$__checkbox = $indexVar['linterface']->Get_Checkbox($__checkboxId, $__checkboxName, $Value=1, $__isChecked, '', '', 'checkTeacherRight(this.id, this.checked);',$__isDisable);
						
                    }
					
                    $x .= '<td style="text-align:center;">'."\r\n";
						//$x .= $_checkbox."\r\n";
						$x .= $__checkbox."\r\n";
					$x .= '</td>'."\r\n";
				}
			$x .= '</tr>'."\r\n";
		}
	$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['formTable'] = $x;

### buttons
$htmlAry['submitBtn'] = $indexVar['linterface']->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn');
$htmlAry['backBtn'] = $indexVar['linterface']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');

?>
<script type="text/javascript">
$(document).ready( function() {
	
});

function goBack() {
	window.location = '?task=teacherApp/advanced_setting/student_list_right/list&appType=<?=$appType?>';
}

function goSubmit() {
	$('input#task').val('teacherApp/advanced_setting/student_list_right/edit_update');
	$('form#form1').submit();
}
function checkTeacherRight(id, parChecked){
	if(id =='AllowViewAllStudentList_NonClassTeacher'){
		if(parChecked){
			//document.getElementById("AllowViewStudentContact_NonClassTeacher").removeAttribute('disabled');
			$('input#AllowViewStudentContact_NonClassTeacher').attr('disabled', '');
			$('input#AllowSendPushMessageToAllStudent_NonClassTeacher').attr('disabled', '');
            $('input#AllowSendPushMessageToAllStudentApp_NonClassTeacher').attr('disabled', '');
		}else{
			//$('#AllowViewStudentContact_NonClassTeacher').removeAttr('checked');
			//document.getElementById("AllowViewStudentContact_NonClassTeacher").setAttribute('disabled', 'disabled');
			$('input#AllowViewStudentContact_NonClassTeacher').attr('checked', '').attr('disabled', 'disabled');
			$('input#AllowSendPushMessageToAllStudent_NonClassTeacher').attr('checked', '').attr('disabled', 'disabled');
            $('input#AllowSendPushMessageToAllStudentApp_NonClassTeacher').attr('checked', '').attr('disabled', 'disabled');
		}
	}
	else if (id =='AllowViewAllStudentList_ClassTeacher'){
		if(parChecked){
			$('input#AllowSendPushMessageToAllStudent_ClassTeacher').attr('disabled', '');
            $('input#AllowSendPushMessageToAllStudentApp_ClassTeacher').attr('disabled', '');
		}else{
			$('input#AllowSendPushMessageToAllStudent_ClassTeacher').attr('checked', '').attr('disabled', 'disabled');
            $('input#AllowSendPushMessageToAllStudentApp_ClassTeacher').attr('checked', '').attr('disabled', 'disabled');
		}
	}
	else if (id =='AllowSendPushMessageToAllStudent_ClassTeacher'){
	    if(parChecked) {
            $('input#AllowSendPushMessageToClassStudent_ClassTeacher').attr('checked', 'checked');
        }
	} else if (id =='AllowSendPushMessageToAllStudentApp_ClassTeacher') {
        if(parChecked) {
            $('input#AllowSendPushMessageToClassStudentApp_ClassTeacher').attr('checked', 'checked');
        }
    } else if (id =='AllowSendPushMessageToClassStudent_ClassTeacher') {
        if($('input#AllowSendPushMessageToAllStudent_ClassTeacher').attr('checked')) {
            $('input#AllowSendPushMessageToClassStudent_ClassTeacher').attr('checked','checked');
        }
    }  else if (id =='AllowSendPushMessageToClassStudentApp_ClassTeacher') {
        if($('input#AllowSendPushMessageToAllStudentApp_ClassTeacher').attr('checked')) {
            $('input#AllowSendPushMessageToClassStudentApp_ClassTeacher').attr('checked','checked');
        }
    }
}

</script>
<form name="form1" id="form1" method="POST" enctype="multipart/form-data">
	<div class="table_board">
		<?=$htmlAry['formTable']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['submitBtn']?>
		<?=$htmlAry['backBtn']?>
		<p class="spacer"></p>
	</div>
	
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="appType" name="appType" value="<?=$appType?>" />
	<input type="hidden" id="imageType" name="imageType" value="" />
</form>
<?
$indexVar['linterface']->LAYOUT_STOP();
?>