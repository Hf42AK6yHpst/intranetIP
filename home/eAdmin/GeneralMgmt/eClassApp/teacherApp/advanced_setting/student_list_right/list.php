<?php
// editing:

/*
 * change log:
 * 2015-05-07  Ivan: add option "Allow to send push message to own class students" and "Allow to send push message to all students"
 * 2015-01-12  Qiao: create file
 */
 
$returnMsgKey = $_GET['returnMsgKey'];
$CurrentPage = 'TeacherApp_AdvancedSetting';

$MODULE_OBJ = $indexVar['libeClassApp']->getModuleObjArr();
$TAGS_OBJ = $indexVar['libeClassApp']->getTeacherAppAdvancedSettingsTagObj($eclassAppConfig['moduleCode']['studentStatus']);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$indexVar['linterface']->LAYOUT_START($returnMsg);

$eClassAppSettingsObj = $indexVar['libeClassApp']->getAppSettingsObj();

### get teacher view studentList and studentContact right
$classTeacherClassStudentListRight = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowViewAllStudentList']);
$classTeacherClassStudentContactRight = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowViewStudentContact']);
$nonClassTeacherClassStudentListRight = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPNonClassTeacherAllowViewAllStudentList']);
$nonClassTeacherClassStudentContactRight = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPNonClassTeacherAllowViewStudentContact']);
$classTeacherCanViewOwnClassRight = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowSendPushMessageToClassStudent']);
$classTeacherCanViewAllClassRight = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowSendPushMessageToAllStudent']);
$nonClassTeacherCanViewAllClassRight = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPNonClassTeacherAllowSendPushMessageToAllStudent']);

$classTeacherClassStudentContactClassRight = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowViewStudentContactClass']);

if($classTeacherClassStudentContactRight== null){
$classTeacherClassStudentContactRight = 1;
}

if($classTeacherClassStudentContactClassRight== null){
	$classTeacherClassStudentContactClassRight = 1;
}


$classTeacherCanViewOwnClassRightStudentApp = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowSendPushMessageToClassStudentApp']);
$classTeacherCanViewAllClassRightStudentApp = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowSendPushMessageToAllStudentApp']);
$nonClassTeacherCanViewAllClassRightStudentApp = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPNonClassTeacherAllowSendPushMessageToAllStudentApp']);


$classTeacherClassStudentListStatus= ($classTeacherClassStudentListRight == 1) ? true:false;
$classTeacherClassStudentContactStatus = ($classTeacherClassStudentContactRight == 1) ? true:false;
$nonClassTeacherClassStudentListStatus= ($nonClassTeacherClassStudentListRight == 1) ? true:false;
$nonClassTeacherClassStudentContactStatus = ($nonClassTeacherClassStudentContactRight == 1) ? true:false;

$classTeacherClassStudentContactClassStatus = ($classTeacherClassStudentContactClassRight == 1) ? true:false;

$formAry[] = array('TeacherStyle'=>'ClassTeacher');
$formAry[] = array('TeacherStyle'=>'NonClassTeacher');
$checkAry[] = array('checkName'=>'AllowViewAllStudentList','ClassTeacher'=>$classTeacherClassStudentListStatus,'NonClassTeacher'=>$nonClassTeacherClassStudentListStatus);
$checkAry[] = array('checkName'=>'AllowViewStudentContactClass','ClassTeacher'=>$classTeacherClassStudentContactClassStatus,'NonClassTeacher'=>false);

$checkAry[] = array('checkName'=>'AllowViewStudentContact','ClassTeacher'=>$classTeacherClassStudentContactStatus,'NonClassTeacher'=>$nonClassTeacherClassStudentContactStatus);

$checkAry[] = array('checkName'=>'AllowSendPushMessageToClassStudentApp','ClassTeacher'=>$classTeacherCanViewOwnClassRightStudentApp,'NonClassTeacher'=>false);

$checkAry[] = array('checkName'=>'AllowSendPushMessageToClassStudent','ClassTeacher'=>$classTeacherCanViewOwnClassRight,'NonClassTeacher'=>false);

$checkAry[] = array('checkName'=>'AllowSendPushMessageToAllStudentApp','ClassTeacher'=>$classTeacherCanViewAllClassRightStudentApp,'NonClassTeacher'=>$nonClassTeacherCanViewAllClassRightStudentApp);

$checkAry[] = array('checkName'=>'AllowSendPushMessageToAllStudent','ClassTeacher'=>$classTeacherCanViewAllClassRight,'NonClassTeacher'=>$nonClassTeacherCanViewAllClassRight);

### calculate column width
$moduleColWidth = 25;
$formColWidth = floor((100 - $moduleColWidth) / 2);

### construct table content
$x = '';
$linterface = new interface_html();
$htmlAry['oleCategoryMappingRemarksTable'] = $linterface->Get_Warning_Message_Box($Lang['General']['Remark'], $Lang['eClassApp']['StudentStatus']['REMARK']);
$x .= $htmlAry['oleCategoryMappingRemarksTable'];
$x .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
	$x .= '<thead>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<th style="width:'.$moduleColWidth.'%;">&nbsp;</th>'."\r\n";
			for ($i=0; $i<count($formAry); $i++) {
				$_formName = $formAry[$i]['TeacherStyle'];
				$x .= '<th style="width:'.$formColWidth.'%; text-align:center;">'.$Lang['eClassApp'][$_formName].'</th>';
			}
		$x .= '</tr>'."\r\n";
	$x .= '</thead>'."\r\n";
	$x .= '<tbody>'."\r\n";
		for ($i=0; $i<count($checkAry); $i++) {
			//$_moduleCode = $checkAry[$i];
			$_moduleCodeAry = $checkAry[$i];
			$_moduleName = $Lang['eClassApp'][$_moduleCodeAry['checkName']];
			
			$x .= '<tr>'."\r\n";
				$x .= '<td>'.$_moduleName.'</td>'."\r\n";
				for ($j=0; $j<count($formAry); $j++) {
                    $__isModuleEnabled = $_moduleCodeAry[$formAry[$j]['TeacherStyle']];
                    
                    if (($formAry[$j]['TeacherStyle'] == 'NonClassTeacher' && $_moduleCodeAry['checkName'] == 'AllowSendPushMessageToClassStudent') ||
						($formAry[$j]['TeacherStyle'] == 'NonClassTeacher' && $_moduleCodeAry['checkName'] == 'AllowSendPushMessageToClassStudentApp') ||
						($formAry[$j]['TeacherStyle'] == 'NonClassTeacher' && $_moduleCodeAry['checkName'] == 'AllowViewStudentContactClass')
                    ) {
                    	$__content = $Lang['General']['EmptySymbol'];
                    }
					else {
						if ($__isModuleEnabled) {
						$__imageName = 'icon_tick_green.gif';
						}
						else {
							$__imageName = 'icon_delete_b.gif';
						}
						$__content = '<img src="'.$PATH_WRT_ROOT.'/images/'.$LAYOUT_SKIN.'/'.$__imageName.'" width="18" height="18" border="0" align="absmiddle">';
					}
					
					$x .= '<td style="text-align:center;">'."\r\n";
						$x .= $__content."\r\n";
					$x .= '</td>'."\r\n";
				}
			$x .= '</tr>'."\r\n";
		}
	$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['formTable'] = $x;

### buttons
$htmlAry['editBtn'] = $indexVar['linterface']->Get_Action_Btn($Lang['Btn']['Edit'], "button", "goEdit()", 'editBtn');

?>
<script type="text/javascript">
$(document).ready( function() {
	
});

function goEdit() {
	window.location = '?task=teacherApp/advanced_setting/student_list_right/edit&appType=<?=$appType?>';
}
</script>
<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<?=$htmlAry['formTable']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['editBtn']?>
		<p class="spacer"></p>
	</div>
	
	<input type="hidden" name="appType" id="appType" value="<?=$appType?>" />
</form>

<?
$indexVar['linterface']->LAYOUT_STOP();
?>