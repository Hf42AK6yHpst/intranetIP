<?php
// editing:	

/*
 * change log:IP25
 * 
 * 2018-03-26 Isaac: pluged in Target user selection plugin to Custom Group User Selection session.
 *            
 * 2018-02-05 Isaac : modified checkform(obj)
 * 
 * 2017-06-20 Icarus: edit.php - Added clickedChooseUser()
 * 
 * 2014-11-04 qiao: create file
 */
$returnMsgKey = $_GET['returnMsgKey'];
$CurrentPage = 'TeacherApp_AdvancedSetting';
$MODULE_OBJ = $indexVar['libeClassApp']->getModuleObjArr();
$TAGS_OBJ = $indexVar['libeClassApp']->getTeacherAppAdvancedSettingsTagObj($eclassAppConfig['moduleCode']['teacherStatus']);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$indexVar['linterface']->LAYOUT_START($returnMsg);

$eClassAppSettingsObj = $indexVar['libeClassApp']->getAppSettingsObj();
$teacherStatusSetting = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPTeacherStatus']);
$selectAllCheck = '';
$selectTeachingStaffCheck = '';
$RecipientRowShow = "";
if($teacherStatusSetting == 0){
	$selectTeachingStaffCheck = 'checked="checked"';
	//Get All choosed Teaching staff
	$authorizedStaffIDArray = $indexVar['libeClassApp']->getAllIntranetAppTeacherStatusUser();
    $_displayNameLang = Get_Lang_Selection('ChineseName', 'EnglishName');
    $authorizedStaffArray = $indexVar['libeClassApp']->getAllIntranetAppTeacherStatusUserName(implode(",", $authorizedStaffIDArray),$_displayNameLang);	
    $authorizedStaffAry = array();
	for($ascount=0;$ascount<count($authorizedStaffArray);$ascount++){
	    $StaffID = 'U'.$authorizedStaffArray[$ascount]['UserID'];
	    $UserName = $authorizedStaffArray[$ascount][$_displayNameLang];
	    $authorizedStaffAry[$ascount][0] = $StaffID;
	    $authorizedStaffAry[$ascount]['UserID'] = $StaffID;
	    $authorizedStaffAry[$ascount][1] = $UserName;
	    $authorizedStaffAry[$ascount]['UserName'] = $UserName;
	} 
}else{
	$selectAllCheck = 'checked="checked"';
	$RecipientRowShow = "style='display:none'";
}

$linterface = new interface_html();
$authorizeStaffSelectionTable = '<input type="radio" name="IsPublic" id="IsPublicY" onclick="$(\'#\'+\'RecipientRow\').hide();" value="Y" '.$selectAllCheck.'>';
$authorizeStaffSelectionTable .= '<label for="IsPublicY">'.$Lang['eClassApp']['TeacherStatusAllTeacher'].'</label>'
                                  .'<input type="radio" name="IsPublic" id="IsPublicN" onclick="$(\'#\'+\'RecipientRow\').show();" value="N" '.$selectTeachingStaffCheck.'>'
                                  .'<label for="IsPublicN">'.$Lang['eClassApp']['TeacherStatusAccessTeacher'].'</label>';
	
### construct table content
$x = '';
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['eClassApp']['TeacherStatusAccessRole'].'<br></td>'."\r\n";
			$x .= '<td align="left" valign="top">'.$authorizeStaffSelectionTable.'</td>'."\r\n";
		$x .= '</tr>'."\r\n";

        $x .= "<tr id='RecipientRow' ".$RecipientRowShow.">
			<td>&nbsp;</td>
			<td><div id='divSelectStaff'>"."</div>";	
$htmlAry['staffTable'] = $x;

###Staff List valid Groups
$commonChooseLink = $PATH_WRT_ROOT.'home/common_choose/index.php?fieldname=Recipient[]&page_title=SelectRecipients&permitted_type=1&excluded_type=4&filterAppTeacher=1&DisplayGroupCategory=1&Disable_AddGroup_Button=1&includeNoPushMessageUser=1';
$intranetAPPTeacherListGroupSettingValue = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPTeacherList_Group']);
$selectAllGroupCheck = '';
$selectDisableAllGroupCheck = '';
$selectSpecificGroupCheck = '';
$GroupRowShow = "";

if($intranetAPPTeacherListGroupSettingValue == 0){
	$selectSpecificGroupCheck = 'checked="checked"';
	//Get All choosed Group
    $authorizedGroupAry = $indexVar['libeClassApp']->getIntranetAppTeacherListEnableGroup();
}else if($intranetAPPTeacherListGroupSettingValue == 2){
	$selectAllGroupCheck = 'checked="checked"';
	$GroupRowShow = "style='display:none'";
}else{
	$selectDisableAllGroupCheck = 'checked="checked"';
	$GroupRowShow = "style='display:none'";
}

$authorizeGroupSelectionTable = '<input type="radio" name="GroupSelection" id="GroupSelection2" onclick="$(\'#\'+\'GroupRowShow\').hide();" value="2" '.$selectAllCheck.'>';
$authorizeGroupSelectionTable .= '<label for="GroupSelection2">'.$Lang['eClassApp']['TeacherStatusEnableAllGroup'].'</label>'
                                  .'<input type="radio" name="GroupSelection" id="GroupSelection0" onclick="$(\'#\'+\'GroupRowShow\').show();" value="0" '.$selectSpecificGroupCheck.'>'
                                  .'<label for="GroupSelection0">'.$Lang['eClassApp']['TeacherStatusEnableSpecificGroup'].'</label>';
$authorizeGroupSelectionTable .= '<input type="radio" name="GroupSelection" id="GroupSelection1" onclick="$(\'#\'+\'GroupRowShow\').hide();" value="1" '.$selectDisableAllGroupCheck.'>'
                                 .'<label for="GroupSelection1">'.$Lang['eClassApp']['TeacherStatusDisableGroup'].'</label>'; 
$x = '';
$x .= "</tr>"."\r\n";
	$x .= '<td class="field_title">'.$Lang['eClassApp']['TeacherStatusAccessGroup'].'<br></td>'."\r\n";
	$x .= '<td align="left" valign="top">'.$authorizeGroupSelectionTable.'</td>'."\r\n";
$x .= '</tr>'."\r\n";

###Group selection Box
$allIntranetAppTeacherListGroup = $indexVar['libeClassApp']->getAllIntranetAppTeacherListGroup();
$TitleNameLang = Get_Lang_Selection('TitleChinese', 'Title');
$groupSelectionBox = '';
$groupSelectionBox = $indexVar['libeClassApp'] ->getSelectionBox($authorizedGroupAry,$allIntranetAppTeacherListGroup,$TitleNameLang,'Title','ID');

$x .= "<tr id='GroupRowShow' ".$GroupRowShow."><td>&nbsp;</td>";
$x .= '<td>'.$groupSelectionBox.'</td></tr>';
$htmlAry['groupTable'] = $x;

### construct table content
$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tbody>'."\r\n";
	$x .= $htmlAry['staffTable'];
	$x .= $htmlAry['groupTable'];		
	$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['formTable'] = $x;

### buttons
$htmlAry['submitBtn'] = $linterface->GET_ACTION_BTN($button_submit, "submit");
$htmlAry['backBtn'] = $indexVar['linterface']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');
?>
<?=$indexVar['linterface']->Include_JS_CSS(); ?>
<?php include_once ($PATH_WRT_ROOT . "home/common_choose/user_selection_plugin_js.php");?>
<script type="text/javascript">


$(document).ready( function() {
	var divId ='divSelectStaff'
	var targetUserTypeAry = [1]; 
 	var userSelectedFieldName = 'Recipient[]'; 
 	var userSelectedFieldId = 'Recipient'; 
 	var selectedUsersData = <?php echo $jsonObj->encode($authorizedStaffAry)?>;
 	Init_User_Selection_Menu(divId,targetUserTypeAry,userSelectedFieldName, userSelectedFieldId,selectedUsersData);
});

function clickedChooseUser() {
	// get selected user list
	var exclude_user_id_array = [];
	var exclude_user_id_list = "";
	$('#recipient option').each(function() {
		if ($(this).val() != '') {
			exclude_user_id_array.push($(this).val());
		}
	});
 	exclude_user_id_list = exclude_user_id_array.join(',');
 	exclude_user_id_list = exclude_user_id_list.replace(/U/g,'');
	show_dyn_thickbox_ip('','<?=$commonChooseLink?>&exclude_user_id_list='+exclude_user_id_list+'&from_thickbox=1', '', 0, 480, 640, 1, 'fakeDiv', null);
}

function goBack() {
	window.location = '?task=teacherApp/advanced_setting/teacher_status_right/list&appType=<?=$appType?>';
}

function checkform(obj){
	
	    var recipients = obj.elements["Recipient[]"];
	    var groupIDs = obj.elements["GroupID[]"];
	    var recipient_cnt = 0;
	    var groupid_cnt = 0;
	// Pending to discover the reason for having the $_SESSION Check (Isaac)
	    //if (<?//=(($_SESSION["SSV_USER_ACCESS"]["eAdmin-ParentAppNotify"]) ? 'true' : 'false')?>) {
		    if (obj.IsPublic[1].checked)
		    {
			    for(i=0;i<recipients.length;++i)
			    {
			    	if(recipients.options[i].value == '' || recipients.options[i].value == 0) continue;
			    	recipient_cnt++;
			    }
			    if(recipient_cnt==0)
			    {
					alert("<?=$Lang['eClassApp']['TeacherStatusSelectAtLeastOne']?>");
					return false; 
				}
			}
			
			if(obj.GroupSelection[1].checked){
			    for(i=0;i<groupIDs.length;++i)
			    {
			    	if(groupIDs.options[i].value == '' || groupIDs.options[i].value == 0) continue;
			    	groupid_cnt++;
			    }
			    if(groupid_cnt==0)
			    {
					alert("<?=$Lang['eClassApp']['TeacherStatusSelectAtLeastOneGroup']?>");
					return false; 
				}
			}
	   /// }	
	    checkOptionAll(obj.elements["Recipient[]"]);
	    checkOptionAll(obj.elements["GroupID[]"]);
	    $('div#buttonDiv').hide();
	    $('div#sendingDiv').show();
	    $('input#task').val('teacherApp/advanced_setting/teacher_status_right/edit_submit');
}

function allSelectionGroupOperation(operationType){
    var X = document.getElementById("AvailableGroupID");
    var Y = document.getElementById("GroupID");
    
	if(operationType == 'REMOVEALL'){
	var X = document.getElementById("GroupID");
    var Y = document.getElementById("AvailableGroupID");
	}
    for (i = 0; i < X.options.length; i++) {
    var tempOption = document.createElement("option");
    tempOption.text = X.options[i].text;
    tempOption.value = X.options[i].value;
    Y.add(tempOption);
    }
    X.options.length = 0;
}

</script>


<form name="form1" id="form1" method="POST" enctype="multipart/form-data" onSubmit="return checkform(this);">
	<div class="table_board">
		<?=$htmlAry['formTable']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div id="buttonDiv" class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['submitBtn']?>
		<?=$htmlAry['backBtn']?>
		<p class="spacer"></p>
	</div>
	<div id="sendingDiv" style="width:100%; text-align:center; display:none;"><?=$linterface->Get_Ajax_Loading_Image($noLang=1)?> <span style="color:red; font-weight:bold;"><?=$Lang['General']['Submitting...']?></span></div>	
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="appType" name="appType" value="<?=$appType?>" />
	<input type="hidden" id="imageType" name="imageType" value="" />
</form>
<?
$indexVar['linterface']->LAYOUT_STOP();
?>