<?php
// using : 

include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");


$returnMsgKey = $_GET['returnMsgKey'];


$lclass = new libclass();
$luser = new libuser();
$fcm = new form_class_manage();


$CurrentPage = 'StudentApp_Blacklist';
$MODULE_OBJ = $indexVar['libeClassApp']->getModuleObjArr();
$TAGS_OBJ[] = array($Lang['eClassApp']['Blacklist']);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$indexVar['linterface']->LAYOUT_START($returnMsg);


### content tool
// $btnAry[] = array($btnClass, $onclickJs, $displayLang, $subBtnAry);
$btnAry = array();
$btnAry[] = array('new', 'javascript: goNew();');
$htmlAry['contentTool'] = $indexVar['linterface']->Get_Content_Tool_By_Array_v30($btnAry);




### class filtering
$htmlAry['formClassSel'] = $lclass->getSelectClassWithWholeForm('id="formClassSel" name="formClassId" onchange="changedFormClassSelection();"', $formClassId, $Lang['SysMgr']['FormClassMapping']['AllClass']);
$targetYearClassIdAry = '';
$filteredStudentIdAry = '';
if ($formClassId != '' && $formClassId != "0") {
	$targetYearClassIdAry = array();
    if (is_numeric($formClassId)) {
    	// selected whole Form => get all Classes of the Form
    	$targetYearClassIdAry = Get_Array_By_Key($lclass->returnClassListByLevel($formClassId), 'YearClassID');
	}
    else {
    	// selected one class only
    	$targetYearClassIdAry[] = substr($formClassId, 2);
    }
    
    ### get filtered students
	$filteredStudentIdAry = Get_Array_By_Key($fcm->Get_Student_By_Class($targetYearClassIdAry), 'UserID');
}


### get blacklisted users
$blacklistUserIdAry = Get_Array_By_Key($indexVar['libeClassApp']->getBlacklistUser($eclassAppConfig['INTRANET_APP_BLACKLIST_USER']['RecordStatus']['active'], $filteredStudentIdAry, USERTYPE_STUDENT), 'UserID');
$numOfUser = count($blacklistUserIdAry);
$userObj = new libuser('', '', $blacklistUserIdAry);


### DB table action buttons
$btnAry = array();
$btnAry[] = array('delete', 'javascript: goDelete();');
$htmlAry['dbTableActionBtn'] = $indexVar['linterface']->Get_DBTable_Action_Button_IP25($btnAry);



### build table
$x = '';
$x .= '<table class="common_table_list_v30">';
	$x .= '<thead>';
		$x .= '<tr>';
			$x .= '<th style="width:20px;">#</th>';
			$x .= '<th style="width:20%;">'.$Lang['General']['Class'].'</th>';
			$x .= '<th style="width:15%;">'.$Lang['General']['ClassNumber'].'</th>';
			$x .= '<th>'.$Lang['Identity']['Student'].'</th>';
			$x .= '<th width="20px">'.$indexVar['linterface']->Get_Checkbox('userChk_global', 'userChk_global', $Value, $isChecked=0, $Class='', $Display='', $Onclick='Check_All_Options_By_Class(\'userIdChk\', this.checked)', $Disabled='').'</th>'."\r\n";
		$x .= '</tr>';
	$x .= '</thead>';
	
	$x .= '<tbody>';
		for ($i=0; $i<$numOfUser; $i++) {
			$_studentId = $blacklistUserIdAry[$i];
			
			$userObj->loadUserData($_studentId);
			$_studentName = Get_Lang_Selection($userObj->ChineseName, $userObj->EnglishName);
			
			$x .= '<tr>';
				$x .= '<td>'.($i+1).'</td>';
				$x .= '<td>'.$userObj->ClassName.'</td>';
				$x .= '<td>'.$userObj->ClassNumber.'</td>';
				$x .= '<td>'.$_studentName.'</td>';
				$x .= '<td>'.$indexVar['linterface']->Get_Checkbox('userChk_'.$_studentId, 'userIdAry[]', $_studentId, $isChecked=0, $Class='userIdChk', '', 'Uncheck_SelectAll(\'userChk_global\', this.checked);').'</td>'."\r\n";
			$x .= '</tr>';
		}
		
		if ($numOfUser == 0) {
			$x .= '<tr><td colspan="6" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>';
		}
	$x .= '</tbody>';
$x .= '</table>';
$htmlAry['dataTable'] = $x;

?>
<script type="text/javascript">
$(document).ready( function() {
	
});

function changedFormClassSelection() {
	reloadPage();
}

function reloadPage() {
	$('input#task').val('studentApp/blacklist/list');
	$('form#form1').attr('action', 'index.php').submit();
}

function goNew() {
	window.location = "?appType=<?=$appType?>&task=parentApp/blacklist/add_user";
}

function goDelete(parCodeStatus) {
	if ($('input.userIdChk:checked').length == 0) {
		alert(globalAlertMsg2);
	}
	else {
		$('input#task').val('parentApp/blacklist/delete_user_update');
		$('form#form1').submit();
	}
}
</script>
<form name="form1" id="form1" method="POST">
	<div class="content_top_tool">
		<?=$htmlAry['contentTool']?>
		<?=$htmlAry['searchBox']?>
		<br style="clear:both;">
	</div>
	
	<div class="table_board">
		<div class="table_filter">
			<?=$htmlAry['formClassSel']?>
			<?=$htmlAry['registerStatusSel']?>
		</div>
		<p class="spacer"></p>
		
		<?=$htmlAry['dbTableActionBtn']?>
		<?=$htmlAry['dataTable']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
	
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="appType" name="appType" value="<?=$appType?>" />
</form>
<?
$indexVar['linterface']->LAYOUT_STOP();
?>