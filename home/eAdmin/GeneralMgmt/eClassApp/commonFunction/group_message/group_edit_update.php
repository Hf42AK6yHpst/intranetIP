<?php
// using : Roy

/*
 * 2017-07-28 Roy: [ip.2.5.8.10.1]
 * move logic to $leClassApp_groupMessage->createOrUpdateGroupAndSyncToCloud()
 * 
 * 2017-05-11 Roy: [ip.2.5.8.7.1]
 * add communication mode
 *
 */

include_once($PATH_WRT_ROOT.'includes/eClassApp/groupMessage/libeClassApp_groupMessage.php');
include_once($PATH_WRT_ROOT."includes/eClassApp/groupMessage/libeClassApp_groupMessage_group.php");
$leClassApp_groupMessage = new libeClassApp_groupMessage();


$groupId = IntegerSafe($_POST['GroupID']);
$groupCode = standardizeFormPostValue($_POST['groupCode']);
$groupNameEn = standardizeFormPostValue($_POST['groupNameEn']);
$groupNameCh = standardizeFormPostValue($_POST['groupNameCh']);
$communicationMode = standardizeFormPostValue($_POST['communicationMode']);
$appType = standardizeFormPostValue($_POST['appType']);

$fromNew = ($groupId == '')? 1 : 0;
if ($fromNew) {
	$maxNumOfGroup = $leClassApp_groupMessage->getMaxNumOfGroup();
	$numOfCurGroup = count($leClassApp_groupMessage->getGroupData($groupIdAry='', $parCode='', $excludeGroupIdAry='', $eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['GroupType']['UserChatroom']));
	if ($numOfCurGroup >= $maxNumOfGroup) {
		No_Access_Right_Pop_Up();
	}
}

$resultAry = $leClassApp_groupMessage->createOrUpdateGroupAndSyncToCloud($groupId, $groupCode, $groupNameEn, $groupNameCh, $communicationMode, $eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['GroupType']['Normal']);
$returnMsgKey = $resultAry['returnMsgKey'];
$fromNew = $resultAry['fromNew'];
$groupId= $resultAry['groupId'];

if ($fromNew) {
	$task = 'commonFunction/group_message/group_member_edit';
	$otherParam = '&GroupID='.$groupId.'&FromNew='.$fromNew;
}
else {
	$task = 'commonFunction/group_message/group_list';
	$otherParam = '';
}
header('location: ?appType='.$appType.'&task='.$task.'&returnMsgKey='.$returnMsgKey.$otherParam);
?>