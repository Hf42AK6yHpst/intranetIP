<?php

include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/groupMessage/libeClassApp_groupMessage.php');
include_once($PATH_WRT_ROOT."includes/eClassApp/groupMessage/libeClassApp_groupMessage_group.php");
include_once ($PATH_WRT_ROOT ."includes/json.php");

$objJson = new JSON_obj();

$AcademicYearID = $_REQUEST['AcademicYearID'];

$fcm = new form_class_manage();
$FormList = $fcm->Get_Form_List();
for ($i=0; $i< sizeof($FormList); $i++) {
	$SelectClass[$FormList[$i]['YearID']] = $_REQUEST[$FormList[$i]['YearID']];
}

$leClassApp_groupMessage = new libeClassApp_groupMessage();

$maxNumOfGroup = $leClassApp_groupMessage->getMaxNumOfGroup();
$numOfCurGroup = count($leClassApp_groupMessage->getGroupData($groupIdAry='', $parCode='', $excludeGroupIdAry='', $eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['GroupType']['UserChatroom']));
if ($numOfCurGroup >= $maxNumOfGroup) {
	$ary = array();
	$ary['error'] = '1';
	$ary['msg'] = $i_general_no_access_right;
	$output = $objJson->encode($ary);
	echo $output;
	return;
}

$maxNumOfMember = $leClassApp_groupMessage->getMaxNumOfGroupMember();

$create_success = false;
$create_count = 0;
$create_class_name = array();
$fail_class_name = array();
foreach($SelectClass as $temp) {
	if(empty($temp)) {
		continue;
	}
	foreach($temp as $thisYearClassID) {
		$yearClassObj = new year_class();
		$yearClassObj->Get_Year_Class_Info($thisYearClassID);
		$name = Get_Lang_Selection($yearClassObj->ClassTitleB5, $yearClassObj->ClassTitleEN);

		$teacher_list = array();
		$parent_list = array();
		$member_count = 0;
		$yearClassObj->Get_Class_Teacher_List(0);
		$member_count += count($yearClassObj->ClassTeacherList);
		foreach($yearClassObj->ClassTeacherList as $temp) {
			$teacher_list[] = $temp['UserID'];
		}

		$yearClassObj->Get_Class_StudentList('', 1);
		$studentids = array();
		foreach($yearClassObj->ClassStudentList as $student) {
			$studentids[] = $student['UserID'];
		}

		if(count($studentids) > 0) {
			$str = implode(',', $studentids);
			$sql = "SELECT DISTINCT ParentID FROM INTRANET_PARENTRELATION WHERE StudentID IN($str)";
			$parent_list = $yearClassObj->returnVector($sql);
			$member_count += count($parent_list);
		}
		if(/*$member_count == 0 || */$member_count > $maxNumOfMember) {
			$fail_class_name[] = $name;
			continue;
		}

		$create_count++;
		if(($create_count+$numOfCurGroup) > $maxNumOfGroup) {
			$fail_class_name[] = $name;
			continue;
		}

		if($CreateGroupMessage == '1') {
			$groupId = '';
			$groupCode = '';
			$groupNameEn = $yearClassObj->ClassTitleEN;
			$groupNameCh = $yearClassObj->ClassTitleB5;
			if($groupNameCh == '') {
				$groupNameCh = $groupNameEn;
			}

			$parent_list_to_delete = array();
			$parent_list_to_add = $parent_list;

			$teacher_list_to_delete = array();
			$teacher_list_to_add = $teacher_list;

			$sql = "Select GroupID From INTRANET_APP_MESSAGE_GROUP Where RecordStatus = 1 AND YearClassID='$thisYearClassID'";
			$current_groupid = $leClassApp_groupMessage->returnResultSet($sql);
			if(count($current_groupid) > 0) {
				$groupId = $current_groupid[0]['GroupID'];
				$memberAry = $leClassApp_groupMessage->getGroupMemberList($groupId,'', false);
				if($memberAry) {
					$memberAssoAry = BuildMultiKeyAssoc($memberAry, 'MemberType', $IncludedDBField = array('UserID'), $SingleValue = 1, $BuildNumericArray = 1);

					if (!empty($memberAssoAry['M']) && is_array($memberAssoAry['M'])) {
						$parent_list_to_delete = array_values(array_diff($memberAssoAry['M'], $parent_list));
						$parent_list_to_add = array_values(array_diff($parent_list, $memberAssoAry['M']));
					}

					if (!empty($memberAssoAry['A']) && is_array($memberAssoAry['A'])) {
						$teacher_list_to_delete = array_values(array_diff($memberAssoAry['A'], $teacher_list));
						$teacher_list_to_add = array_values(array_diff($teacher_list, $memberAssoAry['A']));
					}
				}
			}

			$communicationMode = '0';
			$resultAry = $leClassApp_groupMessage->createOrUpdateGroupAndSyncToCloud($groupId, $groupCode, $groupNameEn, $groupNameCh, $communicationMode, $eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['GroupType']['Normal'], $thisYearClassID);
			if($resultAry['success'] == true) {
				$returnMsgKey = $resultAry['returnMsgKey'];
				$fromNew = $resultAry['fromNew'];
				$groupId = $resultAry['groupId'];

				if($groupId != '') {
					$indexVar['libeClassApp']->Start_Trans();

					$successAry = array();
					$successAry['saveLocalTeacher'] = true;
					$successAry['saveLocalParent'] = true;

					if(count($teacher_list_to_delete) > 0) {
						$leClassApp_groupMessage->deleteGroupMember($groupId, $teacher_list_to_delete);
					}
					if(count($parent_list_to_delete) > 0) {
						$ret = $leClassApp_groupMessage->deleteGroupMember($groupId, $parent_list_to_delete);
					}

					if(count($teacher_list_to_add) > 0) {
						$successAry['saveLocalTeacher'] = $leClassApp_groupMessage->addGroupMember($groupId, $teacher_list_to_add, $eclassAppConfig['INTRANET_APP_MESSAGE_GROUP_MEMBER']['MemberType']['admin'], $thisYearClassID);
					}

					if(count($parent_list_to_add) > 0 && $successAry['saveLocalTeacher']) {
						$successAry['saveLocalParent'] = $leClassApp_groupMessage->addGroupMember($groupId, $parent_list_to_add, $eclassAppConfig['INTRANET_APP_MESSAGE_GROUP_MEMBER']['MemberType']['member'], $thisYearClassID);
					}

					if ($successAry['saveLocalTeacher'] && $successAry['saveLocalParent']) {
						$successAry['saveCloud'] = $leClassApp_groupMessage->saveGroupMemberInCloud($groupId);
					}


					if (in_array(false, $successAry)) {
						$indexVar['libeClassApp']->RollBack_Trans();
					} else {
						$indexVar['libeClassApp']->Commit_Trans();
						$create_success = true;
					}
				}
			}
		}

		$create_class_name[] = $name.' ('.$member_count.')';
	}
}

if($CreateGroupMessage == '1') {
	echo ($create_success) ? 'UpdateSuccess' : 'UpdateUnsuccess';
	return;
}

if(count($create_class_name) == 0 && count($fail_class_name) == 0) {
	$ary = array();
	$ary['error'] = '1';
	$ary['msg'] = $Lang['General']['JS_warning']['SelectClass'];
	$output = $objJson->encode($ary);
	echo $output;
	return;
}

$ary = array();
$msg = '';
if(count($create_class_name) > 0) {
	$msg = str_replace('{number}', count($create_class_name), $Lang['SysMgr']['GroupMessageBatchCreate']['BatchCheckClassWarning1']) . chr(10);
	foreach ($create_class_name as $temp) {
		$msg .= $temp . chr(10);
	}
} else {
	$ary['error'] = '1';
}

if(count($fail_class_name) > 0) {
	$msg .= chr(10) .  str_replace('{number}', count($fail_class_name), $Lang['SysMgr']['GroupMessageBatchCreate']['BatchCheckClassWarning2']). chr(10);
	$msg .= $Lang['SysMgr']['GroupMessageBatchCreate']['BatchCheckClassWarning3'] . chr(10);;
	foreach ($fail_class_name as $temp) {
		$msg .= $temp . chr(10);
	}
}


$ary['msg'] = $msg;
$output = $objJson->encode($ary);
echo $output;

?>