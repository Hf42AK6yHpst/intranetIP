<?php
// using : 
include_once($PATH_WRT_ROOT.'includes/libuser.php');
include_once($PATH_WRT_ROOT.'includes/eClassApp/groupMessage/libeClassApp_groupMessage.php');
include_once($PATH_WRT_ROOT.'includes/eClassApp/groupMessage/libeClassApp_groupMessage_group.php');
$leClassApp_groupMessage = new libeClassApp_groupMessage();


$groupId = (isset($_POST['GroupID']))? $_POST['GroupID'] : $_GET['GroupID'];
$groupId = IntegerSafe($groupId);
$returnMsgKey = $_GET['returnMsgKey'];


$CurrentPage = 'CommonFunction_GroupMessage';
$MODULE_OBJ = $indexVar['libeClassApp']->getModuleObjArr();
$TAGS_OBJ[] = array($Lang['eClassApp']['ModuleNameAry']['GroupMessage']);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$indexVar['linterface']->LAYOUT_START($returnMsg);


$PAGE_NAVIGATION[] = array($Lang['Header']['Menu']['Group'], "javascript:goGroupList()"); 
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['RoleManagement']['UserList'], "");


### Navigation
$htmlAry['navigation'] = $indexVar['linterface']->GET_NAVIGATION_IP25($PAGE_NAVIGATION);


### construct table content
$groupObj = new libeClassApp_groupMessage_group($groupId);
$maxNumOfMember = $leClassApp_groupMessage->getMaxNumOfGroupMember();
$memberUserInfoAry = $leClassApp_groupMessage->getGroupMemberList($groupId);
$numOfMember = count($memberUserInfoAry);
$canAddMember = ($numOfMember >= $maxNumOfMember)? 0 : 1;
$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tbody>'."\r\n"; 
	    if($sys_custom['eClassApp']['GroupMessage']['setCodeForGroup']){
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['General']['Code'].'</td>'."\r\n";
			$x .= '<td>'."\r\n";
				$x .= $groupObj->getCode();
			$x .= '</td>'."\r\n";
	  	$x .= '</tr>'."\r\n";
	    }
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['General']['Name'].'</td>'."\r\n";
			$x .= '<td>'."\r\n";
				$x .= Get_Lang_Selection($groupObj->getNameCh(), $groupObj->getNameEn());
			$x .= '</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['eClassApp']['MemberQuota'].'</td>'."\r\n";
			$x .= '<td>'."\r\n";
				$x .= $numOfMember.' / '.$maxNumOfMember;
			$x .= '</td>'."\r\n";
		$x .= '</tr>'."\r\n";
	$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['formTable'] = $x;


### content tool
// $btnAry[] = array($btnClass, $onclickJs, $displayLang, $subBtnAry);
$btnAry = array();
$btnAry[] = array('new', 'javascript: goAdd();', $Lang['Btn']['Add']);
$htmlAry['contentTool'] = $indexVar['linterface']->Get_Content_Tool_By_Array_v30($btnAry);


### DB table action buttons
// $btnAry[] = array($btnClass, $btnHref, $displayLang);
$btnAry = array();
if ($sys_custom['DHL']) {
	// hide set as admin / member icons
}
else {
	$btnAry[] = array('other', 'javascript: goUpdateAdmin();', $Lang['eClassApp']['SetAsAdmin']);
	$btnAry[] = array('other', 'javascript: goUpdateMember();', $Lang['eClassApp']['SetAsMember']);
}
$btnAry[] = array('delete', 'javascript: goDeleteMember();');
$htmlAry['dbTableActionBtn'] = $indexVar['linterface']->Get_DBTable_Action_Button_IP25($btnAry);


### build member table
$memberUserIdAry = Get_Array_By_Key($memberUserInfoAry, 'UserID');
$userObj = new libuser('', '', $memberUserIdAry);
$x = '';
$x .= '<table class="common_table_list_v30">'."\r\n";
	$x .= '<thead>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<th width="20">#</th>'."\r\n";
			$x .= '<th>'.$Lang['General']['Name'].'</th>'."\r\n";
			$x .= '<th width="32%">'.$Lang['General']['UserType'].'</th>'."\r\n";
            $x .= '<th width="16%">'.$Lang['Identity']['Student'].'</th>'."\r\n";
			if ($sys_custom['DHL']) {
				// hide "member type" column
			}
			else {
				$x .= '<th width="32%">'.$Lang['General']['MemberType'].'</th>'."\r\n";
			}
			$x .= '<th width="20">'.$indexVar['linterface']->Get_Checkbox('userChk_global', 'userChk_global', $Value, $isChecked=0, $Class='', $Display='', $Onclick='Check_All_Options_By_Class(\'userIdChk\', this.checked)', $Disabled='').'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
	$x .= '</thead>'."\r\n";
	$x .= '<tbody>'."\r\n";
	if ($numOfMember == 0) {
		$x .= '<tr><td colspan="6" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>';
	}
	else {
		$studentNameField = getNameFieldWithClassNumberByLang('student.');
		$studentSeparator = ',<br>';

		for ($i=0; $i<$numOfMember; $i++) {
			$_userId = $memberUserInfoAry[$i]['UserID'];
			$_memberType = $memberUserInfoAry[$i]['MemberType'];
			
			$_memberTypeDisplay = $leClassApp_groupMessage->getMemberTypeDisplay($_memberType);
			
			$userObj->loadUserData($_userId);
			$_userName = Get_Lang_Selection($userObj->ChineseName, $userObj->EnglishName);
			$_userType = $userObj->RecordType;
			
			$_userTypeDisplay = '';
			$_studentDisplay = '--';
			switch ($_userType) {
				case USERTYPE_STAFF:
					$_userTypeDisplay = $Lang['Identity']['Staff'];
					break;
				case USERTYPE_PARENT:
					$_userTypeDisplay = $Lang['Identity']['Parent'];
					$sql = "Select
							GROUP_CONCAT(DISTINCT $studentNameField ORDER BY student.ClassName, student.ClassNumber SEPARATOR '".$studentSeparator."') as StudentName
					From
							INTRANET_USER as parent
							Inner Join INTRANET_PARENTRELATION as ipr ON (parent.UserID = ipr.ParentID)
							Inner Join INTRANET_USER as student ON (ipr.StudentID = student.UserID)
					Where
							parent.UserID = '".$_userId."'
					";
	
					$_studentDisplayArray = $leClassApp_groupMessage->returnVector($sql);
					if(!empty($_studentDisplayArray)) {
						$_studentDisplay = $_studentDisplayArray[0];
					}
					break;
			}
			
			$_checkbox = '<td>'.$indexVar['linterface']->Get_Checkbox('userChk_'.$_userId, 'userIdAry[]', $_userId, $isChecked=0, $Class='userIdChk', '', 'Uncheck_SelectAll(\'userChk_global\', this.checked);').'</td>'."\r\n";
			
			$x .= '<tr>'."\r\n";
				$x .= '<td>'.($i+1).'</td>'."\r\n";
				$x .= '<td>'.$_userName.'</td>'."\r\n";
				$x .= '<td>'.$_userTypeDisplay.'</td>'."\r\n";
		    	$x .= '<td>'.$_studentDisplay.'</td>'."\r\n";
				if ($sys_custom['DHL']) {
					// hide "member type" column
				}
				else {
					$x .= '<td>'.$_memberTypeDisplay.'</td>'."\r\n";
				}
				$x .= $_checkbox;
			$x .= '</tr>'."\r\n";
		}
	}
	$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['dataTable'] = $x;


### buttons
$htmlAry['backBtn'] = $indexVar['linterface']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goGroupList()", 'backBtn');
?>
<script type="text/javascript">
$(document).ready( function() {
	
});

function goGroupList() {
	window.location = "?appType=<?=$eclassAppConfig['appType']['Common'] ?>&task=commonFunction/group_message/group_list";
}

function goAdd() {
	if (<?=$canAddMember?> == 1) {
		$('input#task').val('commonFunction/group_message/group_member_edit');
		$('form#form1').submit();
	}
	else {
		alert('<?=$Lang['eClassApp']['jsWarning']['reachedNumOfMemberLimit']?>');
	}
}

function goUpdateAdmin() {
	if ($('input.userIdChk:checked').length == 0) {
		alert(globalAlertMsg2);
	}
	else {
		$('input#targetMemberType').val('<?=$eclassAppConfig['INTRANET_APP_MESSAGE_GROUP_MEMBER']['MemberType']['admin']?>');
		$('input#task').val('commonFunction/group_message/group_member_admin_update');
		$('form#form1').submit();
	}
}

function goUpdateMember() {
	if ($('input.userIdChk:checked').length == 0) {
		alert(globalAlertMsg2);
	}
	else {
		$('input#targetMemberType').val('<?=$eclassAppConfig['INTRANET_APP_MESSAGE_GROUP_MEMBER']['MemberType']['member']?>');
		$('input#task').val('commonFunction/group_message/group_member_admin_update');
		$('form#form1').submit();
	}
}

function goDeleteMember() {
	if ($('input.userIdChk:checked').length == 0) {
		alert(globalAlertMsg2);
	}
	else {
		if (confirm(globalAlertMsg3)) {
			$('input#task').val('commonFunction/group_message/group_member_delete');
			$('form#form1').submit();
		}
	}
}
</script>
<form name="form1" id="form1" method="POST" action="index.php">
	<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	<br style="clear:both;" />
	
	<div class="table_board">
		<?=$htmlAry['formTable']?>
		<br />
		<?=$htmlAry['contentTool']?>
		<br />
		<?=$htmlAry['dbTableActionBtn']?>
		<?=$htmlAry['dataTable']?>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['backBtn']?>
		<p class="spacer"></p>
	</div>
	
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="targetMemberType" name="targetMemberType" value="" />
	<input type="hidden" id="appType" name="appType" value="<?=$appType?>" />
	<input type="hidden" id="GroupID" name="GroupID" value="<?=$groupId?>" />
</form>
<?
$indexVar['linterface']->LAYOUT_STOP();
?>