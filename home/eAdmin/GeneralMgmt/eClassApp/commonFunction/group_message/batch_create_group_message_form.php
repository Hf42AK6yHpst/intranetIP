<?php

include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");


$AcademicYearID = $_REQUEST['AcademicYearID'];

include_once($PATH_WRT_ROOT.'includes/eClassApp/groupMessage/libeClassApp_groupMessage.php');
$leClassApp_groupMessage = new libeClassApp_groupMessage();

$maxNumOfGroup = $leClassApp_groupMessage->getMaxNumOfGroup();
$numOfCurGroup = count($leClassApp_groupMessage->getGroupData($groupIdAry='', $parCode='', $excludeGroupIdAry='', $eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['GroupType']['UserChatroom']));

if ($numOfCurGroup >= $maxNumOfGroup) {
	$x .= '<div class="edit_pop_board" style="height:400px;">
					<div class="edit_pop_board_write" style="height:374px;">
					'.$i_general_no_access_right.'
					<div class="edit_bottom" style="height:10px;">
						<p class="spacer"></p>
						<input name="submit2" type="button" class="formbutton" onclick="window.top.tb_remove();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Cancel'].'" />
						<!--<p class="spacer"></p>-->
					</div>
				</div>
			</div>';
	echo $x;
	return;
}

include_once($PATH_WRT_ROOT.'includes/libeclass40.php');

$fcm = new form_class_manage();

$x .= '<div class="edit_pop_board" style="height:400px;">
					<div class="edit_pop_board_write" style="height:374px;">
					<form name="BatchCreateGroupMessageForm" id="BatchCreateGroupMessageForm" onsubmit="return false;">
						<input type="hidden" name="AcademicYearID" id="AcademicYearID" value="'.$AcademicYearID.'">
						<input type="hidden" name="CreateGroupMessage" id="CreateGroupMessage" value="0">
						<table width="97%" border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td width="100%" align="left" valign="top" class="steptitletext">'.$Lang['SysMgr']['SubjectClassMapping']['SelectClasses'].'</td>
							</tr>
							<tr>
								<td align="left" valign="top">
								<div style=" overflow:auto;">
								<input type="checkbox" value="1" onclick="js_Select_All_Class_Checkbox(this.checked)" />'.$Lang['SysMgr']['FormClassMapping']['AllClass'].'
								<table class="common_table_list" style="">
									<tbody>';
$FormList = $fcm->Get_Form_List(false);
for($i=0; $i< sizeof($FormList); $i++) {
	$thisYearID = $FormList[$i]['YearID'];
	$thisYearName = $FormList[$i]['YearName'];

	$x .= '						<tr>
												<td>'.$thisYearName.'</td>
												<td><input type="checkbox" name="'.$thisYearID.'-SelectAll" id="'.$thisYearID.'-SelectAll" value="1" onclick="js_Select_All_Class('.$thisYearID.', this.checked)" />
													'.$Lang['SysMgr']['FormClassMapping']['AllClass'].'<br /> ';
	$ClassList = $fcm->Get_Class_List($AcademicYearID, $thisYearID);
	for ($j=0; $j< sizeof($ClassList); $j++) {
		$thisYearClassID = $ClassList[$j]['YearClassID'];
		$x .= ' <input type="checkbox" name="'.$thisYearID.'[]" id="'.$thisYearID.'[]" value="'.$thisYearClassID.'" class="ClassChk ClassChk_'.$thisYearID.'" onclick="js_UnCheck_SelectAll('.$thisYearID.', this.checked)" />'.Get_Lang_Selection($ClassList[$j]['ClassTitleB5'],$ClassList[$j]['ClassTitleEN']);
	}
	$x .= '					</td>
										</tr>';
}
$x .= '
									</tbody>
								</table>
								</div>
								</td>
							</tr>
						</table>
						';


$x .= '		</form>
					</div>
					<div class="edit_bottom" style="height:10px;">
						<p class="spacer"></p>
						<input name="submit2" type="button" class="formbutton" onclick="Check_Batch_Create_Group_Message();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Submit'].'" />
						<input name="submit2" type="button" class="formbutton" onclick="window.top.tb_remove();" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Cancel'].'" />
						<!--<p class="spacer"></p>-->
					</div>
				</div>';


echo $x;


?>