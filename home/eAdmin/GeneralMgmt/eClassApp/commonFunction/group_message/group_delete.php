<?php
// using : 

include_once($PATH_WRT_ROOT."includes/eClassApp/groupMessage/libeClassApp_groupMessage_group.php");

$groupIdAry = $_POST['groupIdAry'];
$numOfGroup = count($groupIdAry);

$successAry = array();
$indexVar['libeClassApp']->Start_Trans();
for ($i=0; $i<$numOfGroup; $i++) {
	$_groupId = $groupIdAry[$i];
	
	$_groupObj = new libeClassApp_groupMessage_group($_groupId);
	$_groupObj->setRecordStatus($eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['RecordStatus']['deleted']);
	
	$_groupId = $_groupObj->save();
	$successAry[$i]['saveLocal'] = ($_groupId > 0)? true : false;
	
	if ($successAry[$i]['saveLocal']) {
		$successAry[$i]['saveCloud'] = $_groupObj->saveCloud();
	}
}


if (in_array(false, $successAry)) {
	$returnMsgKey = 'UpdateUnsuccess';
	$indexVar['libeClassApp']->RollBack_Trans();
}
else {
	$returnMsgKey = 'UpdateSuccess';
	$indexVar['libeClassApp']->Commit_Trans();
}

if ($fromNew) {
	$task = 'commonFunction/group_message/group_member_edit';
	$otherParam = '&GroupID='.$groupId.'&FromNew='.$fromNew;
}
else {
	$task = 'commonFunction/group_message/group_list';
	$otherParam = '';
}
header('location: ?appType='.$appType.'&task='.$task.'&returnMsgKey='.$returnMsgKey.$otherParam);
?>