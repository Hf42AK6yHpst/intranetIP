<?php
// using : Tiffany
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_schoolInfo.php");
$libeca_si = new libeClassApp_schoolInfo();
$appType = standardizeFormPostValue($_POST['appType']);
$successAry = array();
$indexVar['libeClassApp']->Start_Trans();
$MenuID = standardizeFormPostValue($_POST['MenuID']);
$MenuID = explode(',',$MenuID);

$successAry[] = $libeca_si->deleteSchoolInfo($MenuID);


if (in_array(false, $successAry)) {
	$returnMsgKey = 'DeleteUnsuccess';
	$indexVar['libeClassApp']->RollBack_Trans();
	
}
else {
	$returnMsgKey = 'DeleteSuccess';
	$indexVar['libeClassApp']->Commit_Trans();
}

header('location: ?appType='.$appType.'&task=commonFunction/school_info/school_info_list&returnMsgKey='.$returnMsgKey.'&ParentMenuID='.$ParentMenuID.'&Level='.$Level.'&goback=1');
?>