<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_schoolInfo.php");
$libeca_si = new libeClassApp_schoolInfo();
intranet_auth();
intranet_opendb();

$Action = $_REQUEST['Action'];
if ($Action == 'Reorder') {
	$DisplayOrderString = $_POST['DisplayOrderString'];
 	$displayOrderArr = $libeca_si->Get_Update_Display_Order_Arr($DisplayOrderString);
 	$success = $libeca_si->Update_Category_Record_DisplayOrder($displayOrderArr);
	echo $success? '1' : '0';
}

intranet_closedb($Action);

?>