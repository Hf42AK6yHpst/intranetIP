<?php
// using :
###################################################### Change Log ######################################################
# Date      : 2019-01-11 (Tiffany)
# Detail    : Support Description for Eng by $sys_custom['eClassApp']['SchoolInfo']['SupportEnglishContent'] = true
# Deploy    :
#
# Date      : 2018-12-31 (Anna)
# Detail    : Added DHL cust
#
# Date      : 2018-04-24 (Anna)
# Detail    : Added sfoc url field
# Deploy    :
#
# Date      : 2017-07-19 (Tiffany)
# Detail    : Support PDF type
# Deploy    :
########################################################################################################################

include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_schoolInfo.php");
$libeca_si = new libeClassApp_schoolInfo();
$appType = standardizeFormPostValue($_POST['appType']);

$successAry = array();
$indexVar['libeClassApp']->Start_Trans();
$MenuID = standardizeFormPostValue($_POST['MenuID']);
$groupTitleEn = standardizeFormPostValue($_POST['groupTitleEn']);
$groupTitleCh = standardizeFormPostValue($_POST['groupTitleCh']);
$appType = standardizeFormPostValue($_POST['appType']);
$status = standardizeFormPostValue($_POST['status']);
$iconType = standardizeFormPostValue($_POST['icon']);
$type = standardizeFormPostValue($_POST['type']);
$defaultIcon = standardizeFormPostValue($_POST['defaultIcon']);
$ParentMenuID = standardizeFormPostValue($_POST['ParentMenuID']);
$Level = standardizeFormPostValue($_POST['Level']);
$iconOld = standardizeFormPostValue($_POST['iconOld']);
$iconTypeOld = standardizeFormPostValue($_POST['iconTypeOld']);
$url = standardizeFormPostValue($_POST['url']);

if($sys_custom['DHL']){
    include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
    $libdhl = new libdhl();
    $GroupIDAry = $_POST['GroupID'];
   
    $GroupIDAry = $libdhl->getMappingIntranetGroupID($GroupIDAry);
    $GroupIDList = implode(',',$GroupIDAry);
   
    $Target_user = $_POST['Target_user'];  
}

if($sys_custom['eClassApp']['SFOC']){
    if($type==1){
        $url=standardizeFormPostValue($_POST['SFOCurl']);
    }else{
        $url = standardizeFormPostValue($_POST['url']);
    }
//    $url = standardizeFormPostValue($_POST['SFOCurl'])==''?standardizeFormPostValue($_POST['url']):standardizeFormPostValue($_POST['SFOCurl']);
}
//debug_pr($MenuID);
//debug_pr($groupTitleEn); 
//debug_pr($groupTitleCh);
//debug_pr($appType);
//debug_pr($status);
//debug_pr($iconType);
//debug_pr($type);
//debug_pr($defaultIcon);
//debug_pr($ParentMenuID);
//debug_pr($Level);
//debug_pr($iconOld);
//debug_pr($iconTypeOld); 
//debug_pr($url);
//debug_pr($_FILES['upload_pdf']);
//die();
if($iconType==1){
	$icon_sql =$defaultIcon;
}else if($iconType==2&&$iconTypeOld==2){
	$icon_sql = $iconOld;
}
 else{
	$icon_sql ="";
}

//add http if url doesn`t has
if($url!=""){
 if ( $ret = parse_url($url) ) {
      if ( !isset($ret["scheme"]) )
       {
            $url = "http://{$url}";
	       }
	 }
}

//get next display order 
$displayOrderArray = $libeca_si->getMAXDisplayOrder($ParentMenuID);
$displayOrder = $displayOrderArray[0]+1;

//if($type==1){
	$Description = intranet_htmlspecialchars(trim($Description));
	
	//for image size reduce 
	$PATH_WRT_ROOT_NEW =  substr($PATH_WRT_ROOT,0, -1);
	$aryData = explode('\\', $Description);
	for($i=0;$i<count($aryData);$i++){
		if(substr_count($aryData[$i],"/file/UserUploadFiles_SchoolInfo/images/tmp/")!=0){
			$filepath = substr($aryData[$i],6);	
			$filepath = $PATH_WRT_ROOT_NEW.$filepath;
			$filepath = urldecode($filepath);
	        $filetype = strtolower(pathinfo($filepath, PATHINFO_EXTENSION));
	        if($filetype=="jpg"||$filetype=="jpeg"||$filetype=="png"){
	        	
	        	list($width, $height) = getimagesize($filepath);
	      	 
	        	$needResize=false;
	        	 
	        	if($width>1500&&$height<=1500){
	        		$needResize=true;
	        		$new_width = 1500;
	        		$new_height = 1500*$height/$width;
	        	}
	        	else if($width<=1500&&$height>1500){
	        		$needResize=true;
	        		$new_height = 1500;
	        		$new_width = 1500*$width/$height;
	        	}
	        	else if($width>1500&&$height>1500){
	        		$needResize=true;
	        		if($width>=$height){
	        			$new_width = 1500;
	        			$new_height = 1500*$height/$width;
	        		}else{
	        			$new_height = 1500;
	        			$new_width = 1500*$width/$height;
	        		}
	        	}
	
	        	if($needResize){
	        		if($filetype=="jpg"||$filetype=="jpeg"){
	        			 
	        			// Resample
	        			$image_p = imagecreatetruecolor($new_width, $new_height);
	        			$image = imagecreatefromjpeg($filepath);
	        			imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
	        			// Output
	        			imagejpeg($image_p, $filepath);
	        			imagedestroy($image_p);
	        		}
	        		if($filetype=="png"){
	        			 
	        			// Resample
	        			$image_p = imagecreatetruecolor($new_width, $new_height);
	        			$image = imagecreatefrompng($filepath);
	        			imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
	        			// Output
	        			imagepng($image_p, $filepath);
	        			imagedestroy($image_p);
	        			 
	        		}
	        		//list($width, $height) = getimagesize($filepath);
	
	        	}
	        	
	        }
	
		}	
	}

    $DescriptionEng = intranet_htmlspecialchars(trim($DescriptionEng));

    //for image size reduce
    $PATH_WRT_ROOT_NEW =  substr($PATH_WRT_ROOT,0, -1);
    $aryData = explode('\\', $DescriptionEng);
    for($i=0;$i<count($aryData);$i++){
        if(substr_count($aryData[$i],"/file/UserUploadFiles_SchoolInfo/images/tmp/")!=0){
            $filepath = substr($aryData[$i],6);
            $filepath = $PATH_WRT_ROOT_NEW.$filepath;
            $filepath = urldecode($filepath);
            $filetype = strtolower(pathinfo($filepath, PATHINFO_EXTENSION));
            if($filetype=="jpg"||$filetype=="jpeg"||$filetype=="png"){

                list($width, $height) = getimagesize($filepath);

                $needResize=false;

                if($width>1500&&$height<=1500){
                    $needResize=true;
                    $new_width = 1500;
                    $new_height = 1500*$height/$width;
                }
                else if($width<=1500&&$height>1500){
                    $needResize=true;
                    $new_height = 1500;
                    $new_width = 1500*$width/$height;
                }
                else if($width>1500&&$height>1500){
                    $needResize=true;
                    if($width>=$height){
                        $new_width = 1500;
                        $new_height = 1500*$height/$width;
                    }else{
                        $new_height = 1500;
                        $new_width = 1500*$width/$height;
                    }
                }

                if($needResize){
                    if($filetype=="jpg"||$filetype=="jpeg"){

                        // Resample
                        $image_p = imagecreatetruecolor($new_width, $new_height);
                        $image = imagecreatefromjpeg($filepath);
                        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
                        // Output
                        imagejpeg($image_p, $filepath);
                        imagedestroy($image_p);
                    }
                    if($filetype=="png"){

                        // Resample
                        $image_p = imagecreatetruecolor($new_width, $new_height);
                        $image = imagecreatefrompng($filepath);
                        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
                        // Output
                        imagepng($image_p, $filepath);
                        imagedestroy($image_p);

                    }
                    //list($width, $height) = getimagesize($filepath);

                }

            }

        }
    }
//}

if($MenuID==""){
	$libeca_si->NewSchoolInfo($groupTitleCh,$groupTitleEn,$iconType,$icon_sql,$type,$Description,$DescriptionEng,$url,$ParentMenuID,$Level,$status,$displayOrder);
}else{
	
	//$sql = "Update SCHOOL_INFO SET TitleChi = '".$groupTitleCh."', TitleEng = '".$groupTitleEn."', IconType = '".$iconType."', Icon = '".$icon_sql."', Description = '".$Description ."', DateModified = now(), ModifyBy = '".$_SESSION['UserID']."'
			//where MenuID = '".$MenuID."'";
	$libeca_si->UpdateSchoolInfo($groupTitleCh,$groupTitleEn,$iconType,$icon_sql,$type,$Description,$DescriptionEng,$url,$MenuID,$status);
}
//$indexVar['libeClassApp']->db_db_query($sql);
if($MenuID==""){
// 	$sql1 = "select MAX(MenuID) FROM SCHOOL_INFO";
// 	$result1 = $indexVar['libeClassApp']->returnVector($sql1);
	$result1 = $libeca_si->getMaxSchoolInfo();
	$MenuID = $result1[0];
}


if($sys_custom['DHL']){
    $libdhl->SetSchoolNewsGroupInfo($Target_user,$MenuID,$GroupIDList);
}


if($iconType==2&&$_FILES['custom_icon']!=""){	
	$successAry['custom_icon'] = $libeca_si->saveSchInfoCustomIcon($MenuID,$_FILES['custom_icon']);
}
if($type==3&&$_FILES['upload_pdf']!=""){	
	$successAry['upload_pdf'] = $libeca_si->saveSchInfoUploadPdf($MenuID,$_FILES['upload_pdf']);
}
if($type==1){
	## Update description if user upload image with Flash upload (fck) [Start]
	$libeca_si->updateSchoolInfoFlashUploadPath($MenuID,$Description);
    $libeca_si->updateSchoolInfoFlashUploadPathEng($MenuID,$DescriptionEng);
    ## Update description if user upload image with Flash upload (fck) [End]
}
if (in_array(false, $successAry)) {
	$returnMsgKey = 'UpdateUnsuccess';
	$indexVar['libeClassApp']->RollBack_Trans();
}
else {
	$returnMsgKey = 'UpdateSuccess';
	$indexVar['libeClassApp']->Commit_Trans();
}

header('location: ?appType='.$appType.'&task=commonFunction/school_info/school_info_list&returnMsgKey='.$returnMsgKey.'&ParentMenuID='.$ParentMenuID.'&Level='.$Level.'&goback=1');
?>