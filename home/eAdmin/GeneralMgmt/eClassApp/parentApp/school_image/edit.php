<?php
// using : 
/********************************
 *
 *  2017-03-12 Ronald
 *      - Add Parent App save banner
 *      - add selecting default image
 *
 * 	2015-07-29 Evan
 * 		-Add timestamp after the path of the pitures to prevent from fetching old pictures
 * 		 Delete default width style to show the real size of the pictures
 * 
 ********************************/
include_once($PATH_WRT_ROOT."includes/libuser.php");


$returnMsgKey = $_GET['returnMsgKey'];


$userObj = new libuser($_SESSION['UserID']);
if ($appType == $eclassAppConfig['appType']['Parent']) {
	$CurrentPage = 'ParentApp_SchoolImage';
    $defaultImageFolder = 'parentApp';
	// if app in trial, only broadlearning account can edit school banner
	$canEditBanner = true;
	if ($sys_custom['eClassApp']['trialMode'] && $userObj->UserLogin != 'broadlearning') {
		$canEditBanner = false;
	}
}
else if ($appType == $eclassAppConfig['appType']['Teacher']) {
	$CurrentPage = 'TeacherApp_SchoolImage';
    $defaultImageFolder = 'teacherApp';
	// if app in trial, only broadlearning account can edit school banner
	$canEditBanner = true;
	if ($sys_custom['eClassTeacherApp']['trialMode'] && $userObj->UserLogin != 'broadlearning') {
		$canEditBanner = false;
	}
}
else if ($appType == $eclassAppConfig['appType']['Student']) {
	$CurrentPage = 'StudentApp_SchoolImage';
    $defaultImageFolder = 'studentApp';
	// if app in trial, only broadlearning account can edit school banner
	$canEditBanner = true;
	if ($sys_custom['eClassStudentApp']['trialMode'] && $userObj->UserLogin != 'broadlearning') {
		$canEditBanner = false;
	}
}


$MODULE_OBJ = $indexVar['libeClassApp']->getModuleObjArr();
$TAGS_OBJ[] = array($Lang['eClassApp']['SchoolImage']);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$indexVar['linterface']->LAYOUT_START($returnMsg);


### get school image info
$schoolBadgePath = $indexVar['libeClassApp']->getSchoolImageUrlPath($appType, $eclassAppConfig['imageType']['schoolBadge']);

$backgroundImageInfo = $indexVar['libeClassApp']->getSchoolImage($appType, $eclassAppConfig['imageType']['backgroundImage']);

if($backgroundImageInfo[0]['RecordStatus'] == $eclassAppConfig['appImage']['status']['upload']){
    $uploadBackgorundType = $eclassAppConfig['appImage']['status']['upload'];
    $backgroundImagePath = $indexVar['libeClassApp']->getSchoolImageUrlPath($appType, $eclassAppConfig['imageType']['backgroundImage']);
}else{
    $uploadBackgorundType = $eclassAppConfig['appImage']['status']['defaultImage'];
    $defaultBackgroundType = $eclassAppConfig['appIamge']['defaultImage']['defaultSelection'];
    if($backgroundImageInfo[0]['RecordStatus'] == $eclassAppConfig['appImage']['status']['defaultImage']){
        $defaultBackgroundType = $backgroundImageInfo[0]['ImagePath'];
    }
}

if($appType == $eclassAppConfig['appType']['Parent']) {

    $bannerImageInfo = $indexVar['libeClassApp']->getSchoolImage($appType , $eclassAppConfig['imageType']['accountPageBanner']);

    if ($bannerImageInfo[0]['RecordStatus'] == $eclassAppConfig['appImage']['status']['upload']) {
        $uploadBannerType = $eclassAppConfig['appImage']['status']['upload'];
        $accountPageBannerPath = $indexVar['libeClassApp']->getSchoolImageUrlPath($appType , $eclassAppConfig['imageType']['accountPageBanner']);
    }else{
        $uploadBannerType = $eclassAppConfig['appIamge']['defaultImage']['defaultSelection'];
        $defaultBannerType = $eclassAppConfig['appIamge']['defaultImage']['defaultSelection'];
        if ($bannerImageInfo[0]['RecordStatus'] == $eclassAppConfig['appIamge']['defaultImage']['defaultSelection']) {
            $defaultBannerType = $bannerImageInfo[0]['ImagePath'];
        }
    }
}

$eClassAppSettingsObj = $indexVar['libeClassApp']->getAppSettingsObj();
$schoolWebsiteURL = $eClassAppSettingsObj->getSettingsValue('parentAppSchoolWebsiteURL');


### suggested resolution remarks display
$x = $Lang['eClassApp']['ImageResolutionRemarks'];
$x = str_replace('<!--width-->', $eclassAppConfig['suggestedResolution']['schoolBadge']['width'], $x);
$x = str_replace('<!--height-->', $eclassAppConfig['suggestedResolution']['schoolBadge']['height'], $x);
$htmlAry['schoolBadgeResolution'] = '<span class="tabletextremark">'.$x.'</span>';

$x = $Lang['eClassApp']['ImageResolutionRemarks'];
$x = str_replace('<!--width-->', $eclassAppConfig['suggestedResolution']['backgroundImage']['width'], $x);
$x = str_replace('<!--height-->', $eclassAppConfig['suggestedResolution']['backgroundImage']['height'], $x);
$htmlAry['backgroundImageResolution'] = '<span class="tabletextremark">'.$x.'</span>';

if ($appType == $eclassAppConfig['appType']['Parent']) {
    $x = $Lang['eClassApp']['ImageResolutionRemarks'];
    $x = str_replace('<!--width-->', $eclassAppConfig['suggestedResolution']['accountPageBanner']['width'], $x);
    $x = str_replace('<!--height-->', $eclassAppConfig['suggestedResolution']['accountPageBanner']['height'], $x);
    $htmlAry['accountPageBannerResolution'] = '<span class="tabletextremark">'.$x.'</span>';
}

### construct table content
$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
$x .= '<tbody>'."\r\n";
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['eClassApp']['SchoolBadge'].'<br>'.$htmlAry['schoolBadgeResolution'].'</td>'."\r\n";
$x .= '<td>'."\r\n";
$x .= '<input type="file" id="schoolBadgeFile" name="schoolBadge" />'."\r\n";
if ($schoolBadgePath) {
    $x .= '<br />'."\r\n";
    $x .= '<br />'."\r\n";
    $x .= $indexVar['linterface']->GET_LNK_DELETE('javascript:void(0);', $Lang['Btn']['Delete'], "goDelete('".$eclassAppConfig['imageType']['schoolBadge']."');", 'delete');
    $x .= '<br style="clear:both;" />'."\r\n";
    $x .= '<img src="'.$schoolBadgePath.'?ts='.time().'" />'."\r\n";
}
$x .= '</td>'."\r\n";
$x .= '</tr>'."\r\n";

if($appType == $eclassAppConfig['appType']['Teacher'] || $appType == $eclassAppConfig['appType']['Parent']){
    if ($canEditBanner) {
        $x .= '<tr>'."\r\n";
        $x .= '<td class="field_title" rowspan="2">'.$Lang['eClassApp']['BackgroundImage'].'<br>'.$htmlAry['backgroundImageResolution'].'</td>'."\r\n";
        $x .= '<td>'."\r\n";
        $x .= $indexVar['linterface']->Get_Radio_Button('background2', 'backgroundType', '2', $uploadBackgorundType == 2 ? true:false, $Class="", $Display=$Lang['eClassApp']['SchoolInfo']['DefaultIcon'], $Onclick="selectBackgroundType(2);",$isDisabled=0);
        $x .= '<br><div id="default_background_div" style="display:none"><table><tr>'."\r\n";
        $x .= '<td id="background_default1" style="border:2px solid white"><a href="javascript:selectDefaultBackground(1);"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/eClassApp/'.$defaultImageFolder.'/background/default1.jpg" height="100px" /></a></td>';
        $x .= '<td id="background_default2" style="border:2px solid white"><a href="javascript:selectDefaultBackground(2);"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/eClassApp/'.$defaultImageFolder.'/background/default2.jpg" height="100px" /></a></td>';
        $x .= '<td id="background_default3" style="border:2px solid white"><a href="javascript:selectDefaultBackground(3);"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/eClassApp/'.$defaultImageFolder.'/background/default3.jpg" height="100px" /></a></td>';
        $x .= '<td id="background_default4" style="border:2px solid white"><a href="javascript:selectDefaultBackground(4);"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/eClassApp/'.$defaultImageFolder.'/background/default4.jpg" height="100px" /></a></td>';
        $x .= '<td id="background_default5" style="border:2px solid white"><a href="javascript:selectDefaultBackground(5);"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/eClassApp/'.$defaultImageFolder.'/background/default5.jpg" height="100px" /></a></td>';
        $x .= '<td id="background_default6" style="border:2px solid white"><a href="javascript:selectDefaultBackground(6);"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/eClassApp/'.$defaultImageFolder.'/background/default6.jpg" height="100px" /></a></td>';
        $x .= '</tr></table></div></td>'."\r\n";
        $x .= '</tr>'."\r\n";
        $x .= '<tr>'."\r\n";
        $x .= '<td>'."\r\n";
        $x .= $indexVar['linterface']->Get_Radio_Button('background1', 'backgroundType', '1', $uploadBackgorundType == 1 ? true:false, $Class="", $Display=$Lang['eClassApp']['SchoolInfo']['CustomIcon'], $Onclick="selectBackgroundType(1);",$isDisabled=0);
        $x .= '<br><div id="custom_background_div" style="display:none"><input type="file" id="backgroundImageFile" name="backgroundImage" accept="image/*" />'."\r\n";
        if (isset($backgroundImagePath)) {
            $x .= '<br />'."\r\n";
            $x .= '<br />'."\r\n";
            $x .= $indexVar['linterface']->GET_LNK_DELETE('javascript:void(0);', $Lang['Btn']['Delete'], "goDelete('".$eclassAppConfig['imageType']['backgroundImage']."');", 'delete');
            $x .= '<br style="clear:both;" />'."\r\n";
            $x .= '<img src="'.$backgroundImagePath.'?ts='.time().'" />'."\r\n";
        }
        $x .= '</div></td>'."\r\n";
        $x .= '</tr>'."\r\n";
    }
    else {
        $x .= '<tr>'."\r\n";
        $x .= '<td>'."\r\n";
        $x .= '<span class="tabletextrequire">'.$Lang['eClassApp']['CannotUpdateSchoolBannerduringTrialPeroid'].'</span>';
        $x .= '</td>'."\r\n";
        $x .= '</tr>'."\r\n";
    }
}else{
    $x .= '<tr>'."\r\n";
    $x .= '<td class="field_title">'.$Lang['eClassApp']['BackgroundImage'].'<br>'.$htmlAry['backgroundImageResolution'].'</td>'."\r\n";
    $x .= '<td>'."\r\n";
    if ($canEditBanner) {
        $x .= '<input type="file" id="backgroundImageFile" name="backgroundImage" accept="image/*" />'."\r\n";
        if ($backgroundImagePath) {
            $x .= '<br />'."\r\n";
            $x .= '<br />'."\r\n";
            $x .= $indexVar['linterface']->GET_LNK_DELETE('javascript:void(0);', $Lang['Btn']['Delete'], "goDelete('".$eclassAppConfig['imageType']['backgroundImage']."');", 'delete');
            $x .= '<br style="clear:both;" />'."\r\n";
            $x .= '<img src="'.$backgroundImagePath.'?ts='.time().'" />'."\r\n";
        }
    }
    else {
        $x .= '<span class="tabletextrequire">'.$Lang['eClassApp']['CannotUpdateSchoolBannerduringTrialPeroid'].'</span>';
    }
    $x .= '</td>'."\r\n";
    $x .= '</tr>'."\r\n";
}

if($appType == $eclassAppConfig['appType']['Parent']){
    if ($canEditBanner) {
        $x .= '<tr>'."\r\n";
        $x .= '<td class="field_title" rowspan="2">'.$Lang['eClassApp']['AccountPageBanner'].'<br>'.$htmlAry['accountPageBannerResolution'].'</td>'."\r\n";
        $x .= '<td>'."\r\n";
        $x .= $indexVar['linterface']->Get_Radio_Button('banner2', 'bannerType', '2', $uploadBannerType == 2 ? true:false, $Class="", $Display=$Lang['eClassApp']['SchoolInfo']['DefaultIcon'], $Onclick="selectBannerType(2);",$isDisabled=0);
        $x .= '<br><div id="default_banner_div" style="display:none"><table>'."\r\n";
        $x .= '<tr><td id="banner_default1" style="border:2px solid white"><a href="javascript:selectDefaultBanner(1);"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/eClassApp/'.$defaultImageFolder.'/banner/default1.jpg" height="100px" /></a></td></tr>';
        $x .= '<tr><td id="banner_default2" style="border:2px solid white"><a href="javascript:selectDefaultBanner(2);"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/eClassApp/'.$defaultImageFolder.'/banner/default2.jpg" height="100px" /></a></td></tr>';
        $x .= '<tr><td id="banner_default3" style="border:2px solid white"><a href="javascript:selectDefaultBanner(3);"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/eClassApp/'.$defaultImageFolder.'/banner/default3.jpg" height="100px" /></a></td></tr>';
        $x .= '<tr><td id="banner_default4" style="border:2px solid white"><a href="javascript:selectDefaultBanner(4);"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/eClassApp/'.$defaultImageFolder.'/banner/default4.jpg" height="100px" /></a></td></tr>';
        $x .= '<tr><td id="banner_default5" style="border:2px solid white"><a href="javascript:selectDefaultBanner(5);"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/eClassApp/'.$defaultImageFolder.'/banner/default5.jpg" height="100px" /></a></td></tr>';
        $x .= '<tr><td id="banner_default6" style="border:2px solid white"><a href="javascript:selectDefaultBanner(6);"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/eClassApp/'.$defaultImageFolder.'/banner/default6.jpg" height="100px" /></a></td></tr>';
        $x .= '</table></div></td>'."\r\n";
        $x .= '</tr>'."\r\n";
        $x .= '<tr>'."\r\n";
        $x .= '<td>'."\r\n";
        $x .= $indexVar['linterface']->Get_Radio_Button('banner1', 'bannerType', '1', $uploadBannerType == 1 ? true:false, $Class="", $Display=$Lang['eClassApp']['SchoolInfo']['CustomIcon'], $Onclick="selectBannerType(1);",$isDisabled=0);
        $x .= '<br><div id="custom_banner_div" style="display:none"><input type="file" id="accountPageBannerFile" name="accountPageBanner" accept="image/*" />'."\r\n";
        if ($accountPageBannerPath) {
            $x .= '<br />'."\r\n";
            $x .= '<br />'."\r\n";
            $x .= $indexVar['linterface']->GET_LNK_DELETE('javascript:void(0);', $Lang['Btn']['Delete'], "goDelete('".$eclassAppConfig['imageType']['accountPageBanner']."');", 'delete');
            $x .= '<br style="clear:both;" />'."\r\n";
            $x .= '<img src="'.$accountPageBannerPath.'?ts='.time().'" />'."\r\n";
        }
        $x .= '</div></td>'."\r\n";
        $x .= '</tr>'."\r\n";
    }
    else {
        $x .= '<tr>'."\r\n";
        $x .= '<td class="field_title">'.$Lang['eClassApp']['AccountPageBanner'].'<br>'.$htmlAry['backgroundImageResolution'].'</td>'."\r\n";
        $x .= '<span class="tabletextrequire">'.$Lang['eClassApp']['CannotUpdateSchoolBannerduringTrialPeroid'].'</span>';
        $x .= '</td>'."\r\n";
        $x .= '</tr>'."\r\n";
    }
}
if($sys_custom['eClassApp']['SchoolWebsiteURL']) {
    $x .= '<tr>' . "\r\n";
    $x .= '<td class="field_title">' . $Lang['eClassApp']['SchoolWebsiteURL'] . '</td>' . "\r\n";
    $x .= '<td>' . "\r\n";
    $x .= '<input type="text" id="schoolWebsiteURL" name="schoolWebsiteURL" style="width:500px;" value="' . $schoolWebsiteURL . '" />' . "\r\n";
    $x .= '</td>' . "\r\n";
    $x .= '</tr>' . "\r\n";
}
$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['formTable'] = $x;


### buttons
$htmlAry['submitBtn'] = $indexVar['linterface']->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn');
$htmlAry['backBtn'] = $indexVar['linterface']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');


?>
<script type="text/javascript">
$(document).ready( function() {
    selectBackgroundType(<?=$uploadBackgorundType?>);
    <? if($appType == $eclassAppConfig['appType']['Parent']) { ?>
        selectBannerType(<?=$uploadBannerType?>);
    <? } ?>
});

function goBack() {
	window.location = '?task=parentApp/school_image/list&appType=<?=$appType?>';
}

function goSubmit() {
    var canSubmit = true;

    <?php if($sys_custom['eClassApp']['SchoolWebsiteURL']) { ?>
    var schoolWebsiteURL = $('#schoolWebsiteURL').val();
    if(schoolWebsiteURL != "" && schoolWebsiteURL.substring(0,7)!='http://' && schoolWebsiteURL.substring(0,8)!='https://'){
        $('#schoolWebsiteURL').val('http://'+$('#schoolWebsiteURL').val());
        schoolWebsiteURL = $('#schoolWebsiteURL').val();
    }
    var regexp = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&/\/=]*)/;
    if(schoolWebsiteURL != "" && !schoolWebsiteURL.match(regexp)){
        canSubmit = false;
        alert('<?php echo $Lang['eClassApp']['jsWarning']['schoolWebsiteURLFormatIncorrect']; ?>');
    }
    <?php } ?>

    if(canSubmit) {
        $('input#task').val('parentApp/school_image/edit_update');
        $('form#form1').submit();
    }
}

function goDelete(imageType) {
	var warningMsg = '';
	if (imageType == '<?=$eclassAppConfig['imageType']['schoolBadge']?>') {
		warningMsg = '<?=$Lang['eClassApp']['jsWarning']['deleteSchoolBadge']?>';
	}
	else if (imageType == '<?=$eclassAppConfig['imageType']['backgroundImage']?>') {
		warningMsg = '<?=$Lang['eClassApp']['jsWarning']['deleteBackgroundImage']?>';
	}
	else if (imageType == '<?=$eclassAppConfig['imageType']['accountPageBanner']?>') {
		warningMsg = '<?=$Lang['eClassApp']['jsWarning']['deleteAccountPageBanner']?>';
	}

	if (confirm(warningMsg)) {
		$('input#task').val('parentApp/school_image/delete_update');
		$('input#imageType').val(imageType);
		$('form#form1').submit();
	}
}

function selectBackgroundType(type){
    if(type == 1){
        $('#default_background_div').hide();
        $('#custom_background_div').show();
    }else if(type == 2){
        $('#default_background_div').show();
        <? if($uploadBackgorundType == 2){ ?>
        selectDefaultBackground(<?=$defaultBackgroundType?>);
        <? }else{ ?>
        selectDefaultBackground(2);
        <? } ?>
        $('#custom_background_div').hide();
    }

}

function selectDefaultBackground(id){

    if($('#defaultBackground').val() != ""){

        $('#background_default'+$('#defaultBackground').val()).css("border-color","white");
    }

    $('#background_default'+id).css("border-color","grey");
    $('#defaultBackground').val(id);
}

<? if($appType == $eclassAppConfig['appType']['Parent']) { ?>
    function selectBannerType(type){
        if(type == 1){
            $('#default_banner_div').hide();
            $('#custom_banner_div').show();
        }else if(type == 2){
            $('#default_banner_div').show();
            <? if($uploadBannerType == 2){ ?>
            selectDefaultBanner(<?=$defaultBannerType?>);
            <? }else{ ?>
            selectDefaultBanner(2);
            <? } ?>
            $('#custom_banner_div').hide();
        }

    }

    function selectDefaultBanner(id){

        if($('#defaultBanner').val() != ""){

            $('#banner_default'+$('#defaultBanner').val()).css("border-color","white");
        }

        $('#banner_default'+id).css("border-color","grey");
        $('#defaultBanner').val(id);
    }
<? } ?>

</script>
<form name="form1" id="form1" method="POST" enctype="multipart/form-data">
	<div class="table_board">
		<?=$htmlAry['formTable']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['submitBtn']?>
		<?=$htmlAry['backBtn']?>
		<p class="spacer"></p>
	</div>
	
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="appType" name="appType" value="<?=$appType?>" />
	<input type="hidden" id="imageType" name="imageType" value="" />
    <input type="hidden" id="defaultBackground" name="defaultBackground" value="" />
    <input type="hidden" id="defaultBanner" name="defaultBanner" value="" />
</form>
<?
$indexVar['linterface']->LAYOUT_STOP();
?>