<?php
// editing: 
/*
 * change log:
 * 2017-03-07 Tiffany [ip.2.5.8.4.1] [G89311]: added settings to show / hide attendance status statistics in eAttendance module;
 * 2015-12-15 Roy [ip.2.5.7.1.1] [U88786]: added settings to show / hide attendance time in eAttendance module; improve layout and wordings
 * 2015-12-11 Ivan [ip.2.5.7.1.1] [K90132]: added settings to send push message for non-school day
 * 2015-11-17 Ivan [ip.2.5.7.1.1] [P88366]: added settings to send arrival and/or leave push message
 * 2015-07-10 Roy [ip.2.5.6.7.1.0]:
 * 	- add show attendance time, show attendance leave section and show eAttendance option
 * 2014-11-06 Ivan: create file
 */
include_once($PATH_WRT_ROOT.'includes/libclass.php');
include_once($PATH_WRT_ROOT.'includes/libinterface.php');

$lclass = new libclass();
$linterface= new interface_html();
 
$returnMsgKey = $_GET['returnMsgKey'];
$CurrentPage = 'ParentApp_AdvancedSetting';

$MODULE_OBJ = $indexVar['libeClassApp']->getModuleObjArr();
$TAGS_OBJ = $indexVar['libeClassApp']->getParentAppAdvancedSettingsTagObj($eclassAppConfig['moduleCode']['eAttendance']);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$indexVar['linterface']->LAYOUT_START($returnMsg);

$eClassAppSettingsObj = $indexVar['libeClassApp']->getAppSettingsObj();
$enablePushMessage = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendancePushMessageEnable']);


### construct form table
$formAry = $lclass->getLevelArray();
$numOfForm = count($formAry);

$x = '';
$x .= '<table class="common_table_list_v30 edit_table_list_v30">'."\r\n";
	$x .= '<thead>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<th style="width:50%;">'.$Lang['General']['Form'].'</th>'."\r\n";
			$x .= '<th style="width:50%; text-align:center;">'.$Lang['General']['Status2'].'</th>'."\r\n";
		$x .= '</tr>'."\r\n";
		$x .= '<tr class="edit_table_head_bulk">'."\r\n";
			$x .= '<th>&nbsp;</th>'."\r\n";
			$x .= '<th style="text-align:center;">'."\r\n";
				$_settingKey = $eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendancePushMessageEnable'].'_global';
				$x .= $indexVar['linterface']->Get_Radio_Button($_settingKey."_yes", $_settingKey, 1, false, $Class="", $Lang['General']['Yes'], $Onclick="changedGlobalRadio('".$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendancePushMessageEnable']."', 1);",$isDisabled=0);
					$x .= ' ';
				$x .= $indexVar['linterface']->Get_Radio_Button($_settingKey."_no", $_settingKey, 0, false, $Class="", $Lang['General']['No'], $Onclick="changedGlobalRadio('".$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendancePushMessageEnable']."', 0);",$isDisabled=0);
			$x .= '</th>'."\r\n";
		$x .= '</tr>'."\r\n";
	$x .= '</thead>'."\r\n";
	$x .= '<tbody>'."\r\n";
		for ($i=0; $i<$numOfForm; $i++) {
			$_classLevelId = $formAry[$i]['ClassLevelID'];
			$_levelName = $formAry[$i]['LevelName'];
			
			$_settingKey = $eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendancePushMessageEnable'].'_'.$_classLevelId;
			$_enablePushMessage = $eClassAppSettingsObj->getSettingsValue($_settingKey);
 			
			$x .= '<tr>'."\r\n";
				$x .= '<td>'.$_levelName.'</td>'."\r\n";
				$x .= '<td style="text-align:center;">'."\r\n";
					$x .= $indexVar['linterface']->Get_Radio_Button($_settingKey."_yes", $_settingKey, 1, $_enablePushMessage, $Class=$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendancePushMessageEnable'].'_yes', $Lang['General']['Yes'], $Onclick="",$isDisabled=0);
					$x .= ' ';
					$x .= $indexVar['linterface']->Get_Radio_Button($_settingKey."_no", $_settingKey, 0, !$_enablePushMessage, $Class=$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendancePushMessageEnable'].'_no', $Lang['General']['No'], $Onclick="",$isDisabled=0);
				$x .= '</td>'."\r\n";
			$x .= '</tr>'."\r\n";
		}
	$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['formTable'] = $x;

### attendance status is showed in app home page
$attendanceStatusEnable  = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceStatusEnable']);
if($attendanceStatusEnable == null){
	$attendanceStatusEnable = 1;
}
$checkYes = ($attendanceStatusEnable == 1) ? 1 : 0;
$checkNo = ($attendanceStatusEnable == 1) ? 0 : 1;
$x = '';
$x .= $indexVar['linterface']->Get_Radio_Button($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceStatusEnable']."_yes", $eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceStatusEnable'], 1, $checkYes, $Class="", $Lang['General']['Yes'], $Onclick="",$isDisabled=0);
$x .= ' ';
$x .= $indexVar['linterface']->Get_Radio_Button($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceStatusEnable']."_no", $eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceStatusEnable'], 0, $checkNo, $Class="", $Lang['General']['No'], $Onclick="",$isDisabled=0);
$htmlAry['AttendanceStatusSettingRadioBtn'] = $x;

#20150710 roy
### attendance time is showed in app home page
$attendanceTimeEnable  = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceTimeEnable']);
if($attendanceTimeEnable == null){
	$attendanceTimeEnable = 1;
}
$checkYes = ($attendanceTimeEnable == 1) ? 1 : 0;
$checkNo = ($attendanceTimeEnable == 1) ? 0 : 1;
$x = '';
$x .= $indexVar['linterface']->Get_Radio_Button($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceTimeEnable']."_yes", $eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceTimeEnable'], 1, $checkYes, $Class="", $Lang['General']['Yes'], $Onclick="",$isDisabled=0);
$x .= ' ';
$x .= $indexVar['linterface']->Get_Radio_Button($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceTimeEnable']."_no", $eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceTimeEnable'], 0, $checkNo, $Class="", $Lang['General']['No'], $Onclick="",$isDisabled=0);
$htmlAry['AttendanceTimeSettingRadioBtn'] = $x;

### Leave Section is showed in app home page
$attendanceLeaveSectionEnable  = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceLeaveSectionEnable']);
if($attendanceLeaveSectionEnable == null){
	$attendanceLeaveSectionEnable = 1;
}
$checkYes = ($attendanceLeaveSectionEnable == 1) ? 1 : 0;
$checkNo = ($attendanceLeaveSectionEnable == 1) ? 0 : 1;
$x = '';
$x .= $indexVar['linterface']->Get_Radio_Button($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceLeaveSectionEnable']."_yes", $eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceLeaveSectionEnable'], 1, $checkYes, $Class="", $Lang['General']['Yes'], $Onclick="",$isDisabled=0);
$x .= ' ';
$x .= $indexVar['linterface']->Get_Radio_Button($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceLeaveSectionEnable']."_no", $eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceLeaveSectionEnable'], 0, $checkNo, $Class="", $Lang['General']['No'], $Onclick="",$isDisabled=0);
$htmlAry['AttendanceLeaveSectionSettingRadioBtn'] = $x;

### eAttendance module is showed in app menu
$eAttendanceEnable  = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceEnable']);
if($eAttendanceEnable == null){
	$eAttendanceEnable = 1;
}
$checkYes = ($eAttendanceEnable == 1) ? 1 : 0;
$checkNo = ($eAttendanceEnable == 1) ? 0 : 1;
$x = '';
$x .= $indexVar['linterface']->Get_Radio_Button($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceEnable']."_yes", $eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceEnable'], 1, $checkYes, $Class="", $Lang['General']['Yes'], $Onclick="",$isDisabled=0);
$x .= ' ';
$x .= $indexVar['linterface']->Get_Radio_Button($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceEnable']."_no", $eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceEnable'], 0, $checkNo, $Class="", $Lang['General']['No'], $Onclick="",$isDisabled=0);
$htmlAry['EAttendanceSettingRadioBtn'] = $x;

// 2015-12-15 U88786 Roy
### attendance time is showed in eAttendance module
$eAttendanceTimeEnable  = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceTimeEnable']);
if($eAttendanceTimeEnable == null){
	$eAttendanceTimeEnable = 1;
}
$checkYes = ($eAttendanceTimeEnable == 1) ? 1 : 0;
$checkNo = ($eAttendanceTimeEnable == 1) ? 0 : 1;
$x = '';
$x .= $indexVar['linterface']->Get_Radio_Button($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceTimeEnable']."_yes", $eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceTimeEnable'], 1, $checkYes, $Class="", $Lang['General']['Yes'], $Onclick="",$isDisabled=0);
$x .= ' ';
$x .= $indexVar['linterface']->Get_Radio_Button($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceTimeEnable']."_no", $eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceTimeEnable'], 0, $checkNo, $Class="", $Lang['General']['No'], $Onclick="",$isDisabled=0);
$htmlAry['EAttendanceTimeEnableRadioBtn'] = $x;

### attendance status statistics in eAttendance module
$eAttendanceStatusStatisticsEnable  = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceStatusStatisticsEnable']);
if($eAttendanceStatusStatisticsEnable == null){
	$eAttendanceStatusStatisticsEnable = 1;
}
$checkYes = ($eAttendanceStatusStatisticsEnable == 1) ? 1 : 0;
$checkNo = ($eAttendanceStatusStatisticsEnable == 1) ? 0 : 1;
$x = '';
$x .= $indexVar['linterface']->Get_Radio_Button($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceStatusStatisticsEnable']."_yes", $eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceStatusStatisticsEnable'], 1, $checkYes, $Class="", $Lang['General']['Yes'], $Onclick="",$isDisabled=0);
$x .= ' ';
$x .= $indexVar['linterface']->Get_Radio_Button($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceStatusStatisticsEnable']."_no", $eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceStatusStatisticsEnable'], 0, $checkNo, $Class="", $Lang['General']['No'], $Onclick="",$isDisabled=0);
$htmlAry['EAttendanceStatusStatisticsEnableRadioBtn'] = $x;

# tap card push message type
$sendArrivalMsg = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableTapCardPushMessageType_Arrival']);
$sendLeaveMsg  = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableTapCardPushMessageType_Leave']);
$checkArrival = ($sendArrivalMsg == 1) ? 1 : 0;
$checkLeave = ($sendLeaveMsg == 1) ? 1 : 0;
$x = '';
$x .= $indexVar['linterface']->Get_Checkbox($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableTapCardPushMessageType_Arrival'].'Chk', $eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableTapCardPushMessageType_Arrival'], 1, $checkArrival, $Class='', $Lang['eClassApp']['TapCardStatusAry']['Arrival']);
$x .= ' ';
$x .= $indexVar['linterface']->Get_Checkbox($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableTapCardPushMessageType_Leave'].'Chk', $eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableTapCardPushMessageType_Leave'], 1, $checkLeave, $Class='', $Lang['eClassApp']['TapCardStatusAry']['Leave']);
$htmlAry['pushMessageStatusChk'] = $x;


### send tap card push message on non-school day
$sendTapCardPushMessageOnNonSchoolDay  = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableTapCardPushMessageOnNonSchoolDay']);
if ($sendTapCardPushMessageOnNonSchoolDay) {
	$checkYes = 1;
	$checkNo = 0;
	$messageTypeDivAttr = '';
}
else {
	$checkYes = 0;
	$checkNo = 1;
	$messageTypeDivAttr = ' display:none; ';
}
$sendArrivalMsg = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableNonSchoolDayTapCardPushMessageType_Arrival']);
$sendLeaveMsg  = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableNonSchoolDayTapCardPushMessageType_Leave']);
$checkArrival = ($sendArrivalMsg == 1) ? 1 : 0;
$checkLeave = ($sendLeaveMsg == 1) ? 1 : 0;
$x = '';
$x .= $indexVar['linterface']->Get_Radio_Button($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableTapCardPushMessageOnNonSchoolDay']."_yes", $eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableTapCardPushMessageOnNonSchoolDay'], 1, $checkYes, $Class="", $Lang['General']['Yes'], "changedNonSchoolDayTapCardSettings();",$isDisabled=0);
$x .= ' ';
$x .= $indexVar['linterface']->Get_Radio_Button($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableTapCardPushMessageOnNonSchoolDay']."_no", $eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableTapCardPushMessageOnNonSchoolDay'], 0, $checkNo, $Class="", $Lang['General']['No'], "changedNonSchoolDayTapCardSettings();",$isDisabled=0);
$x .= '<div id="nonSchoolDayTapCardPushMessageTypeDiv" style="border:1px solid #CCCCCC; padding:10px;'.$messageTypeDivAttr.'">'."\r\n";
	$x .= $Lang['eClassApp']['SendTapCardPushMessageType'].': '."\r\n";
	$x .= $indexVar['linterface']->Get_Checkbox($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableNonSchoolDayTapCardPushMessageType_Arrival'].'Chk', $eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableNonSchoolDayTapCardPushMessageType_Arrival'], 1, $checkArrival, $Class='', $Lang['eClassApp']['TapCardStatusAry']['Arrival']);
	$x .= ' ';
	$x .= $indexVar['linterface']->Get_Checkbox($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableNonSchoolDayTapCardPushMessageType_Leave'].'Chk', $eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableNonSchoolDayTapCardPushMessageType_Leave'], 1, $checkLeave, $Class='', $Lang['eClassApp']['TapCardStatusAry']['Leave']);
	$x .= $indexVar['linterface']->Get_Form_Warning_Msg('nonSchoolDayTapCardMessageTypeWarnDiv', $Lang['eClassApp']['Warning']['PleaseSelectAtLeastOneOption'], $Class='warnMsgDiv')."\n";
$x .= '</div>'."\r\n";
$htmlAry['sendTapCardPushMessageOnNonSchoolDayChk'] = $x;


### construct table content
$x = '';
$x .= $linterface->Get_Form_Separate_Title($Lang['eClassApp']['PushMessage']);
$x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tbody>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['eClassApp']['SendTapCardPushMessage'].'</td>'."\r\n";
			$x .= '<td>'.$htmlAry['formTable'].'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['eClassApp']['SendTapCardPushMessageType'].'</td>'."\r\n";
			$x .= '<td>'.$htmlAry['pushMessageStatusChk'].'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['eClassApp']['SendTapCardPushMessageOnNonSchoolDay'].'</td>'."\r\n";
			$x .= '<td>'.$htmlAry['sendTapCardPushMessageOnNonSchoolDayChk'].'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
	$x .= '</tbody>'."\r\n";
  $x .= '</table>'."\r\n";
  $x .= '<br/>';
  $x .= $linterface->Get_Form_Separate_Title($Lang['eClassApp']['AppHomePageDisplay']);
  $x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tbody>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['eClassApp']['ShowStudentAttendanceStatus'].'</td>'."\r\n";
			$x .= '<td>'.$htmlAry['AttendanceStatusSettingRadioBtn'].'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['eClassApp']['ShowStudentAttendanceTime'].'</td>'."\r\n";
			$x .= '<td>'.$htmlAry['AttendanceTimeSettingRadioBtn'].'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['eClassApp']['ShowStudentAttendanceLeaveSection'].'</td>'."\r\n";
			$x .= '<td>'.$htmlAry['AttendanceLeaveSectionSettingRadioBtn'].'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
	$x .= '</tbody>'."\r\n";
  $x .= '</table>'."\r\n";
  $x .= '<br/>';
  $x .= $linterface->Get_Form_Separate_Title($Lang['eClassApp']['AppEAttendanceDisplay']);
  $x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tbody>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['eClassApp']['ShowStudentAttendanceDetails'].'</td>'."\r\n";
			$x .= '<td>'.$htmlAry['EAttendanceSettingRadioBtn'].'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['eClassApp']['ShowAttendanceTimeInEAttendance'].'</td>'."\r\n";
			$x .= '<td>'.$htmlAry['EAttendanceTimeEnableRadioBtn'].'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['eClassApp']['ShowStatusStatisticsInEAttendance'].'</td>'."\r\n";
			$x .= '<td>'.$htmlAry['EAttendanceStatusStatisticsEnableRadioBtn'].'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
	$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['formTable'] = $x;

### buttons
$htmlAry['submitBtn'] = $indexVar['linterface']->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn');
$htmlAry['backBtn'] = $indexVar['linterface']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');

?>
<script type="text/javascript">
$(document).ready( function() {
	
});

function goBack() {
	window.location = '?task=parentApp/advanced_setting/attendance/list';
}

function goSubmit() {
	var canSubmit = true;
	$('div.warnMsgDiv').hide();
	
	if ($('input#enableTapCardPushMessageOnNonSchoolDay_yes').attr('checked')) {
		if (!$('input#enableNonSchoolDayTapCardPushMessageType_ArrivalChk').attr('checked') && !$('input#enableNonSchoolDayTapCardPushMessageType_LeaveChk').attr('checked')) {
			canSubmit = false;
			$('div#nonSchoolDayTapCardMessageTypeWarnDiv').show();
		}
	}
	
	if (canSubmit) {
		$('input#task').val('parentApp/advanced_setting/attendance/edit_update');
		$('form#form1').submit();
	}
}

function changedGlobalRadio(targetClass, targetStatus) {
	var radioYesCheckStatus;
	var radioNoCheckStatus;
	if (targetStatus == 1) {
		radioYesCheckStatus = 'checked';
		radioNoCheckStatus = '';
	}
	else {
		radioYesCheckStatus = '';
		radioNoCheckStatus = 'checked';
	}
	
	$('input.' + targetClass + '_yes').attr('checked', radioYesCheckStatus);
	$('input.' + targetClass + '_no').attr('checked', radioNoCheckStatus);
}

function changedNonSchoolDayTapCardSettings() {
	if ($('input#enableTapCardPushMessageOnNonSchoolDay_yes').attr('checked')) {
		$('div#nonSchoolDayTapCardPushMessageTypeDiv').show();
	}
	else {
		$('div#nonSchoolDayTapCardPushMessageTypeDiv').hide();
	}
}

</script>
<form name="form1" id="form1" method="POST" enctype="multipart/form-data">
	<div class="table_board">
		<?=$htmlAry['formTable']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['submitBtn']?>
		<?=$htmlAry['backBtn']?>
		<p class="spacer"></p>
	</div>
	
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="imageType" name="imageType" value="" />
</form>
<?
$indexVar['linterface']->LAYOUT_STOP();
?>