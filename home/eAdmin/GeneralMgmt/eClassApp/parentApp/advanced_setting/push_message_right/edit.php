<?php
// editing: 

/*
 * change log:
 * 2020-01-16 Tommy [ip.2.5.11.1.1.1]: hide eEnrolment app function in KIS
 * 2016-01-20 Roy: [ip.2.5.7.3.1]: only show "AllowClubAndActivityPICSendMessage" option if eEnrollment enabled
 * 2014-11-06 Ivan [ip.2.5.5.12.1]: use function to generate $TAGS_OBJ now
 * 2014-10-22 Roy: create file
 */
 
$returnMsgKey = $_GET['returnMsgKey'];
$CurrentPage = 'ParentApp_AdvancedSetting';

$MODULE_OBJ = $indexVar['libeClassApp']->getModuleObjArr();
//$TAGS_OBJ[] = array($Lang['eClassApp']['AdvancedSettingList']['PushMessageRight']);
$TAGS_OBJ = $indexVar['libeClassApp']->getParentAppAdvancedSettingsTagObj($eclassAppConfig['moduleCode']['pushMessage']);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$indexVar['linterface']->LAYOUT_START($returnMsg);

$eClassAppSettingsObj = $indexVar['libeClassApp']->getAppSettingsObj();
$classTeacherSendRight = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['pushMessageSendRight']['classTeacher']);
$PICSendRight = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['pushMessageSendRight']['clubAndActivityPIC']);
$nonAdminUserCanDeleteOwnPushMessageRecord = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['nonAdminUserCanDeleteOwnPushMessageRecord']);


### class teacher push message status
$checkYes = ($classTeacherSendRight == 1) ? 1 : 0;
$checkNo = ($classTeacherSendRight == 1) ? 0 : 1;
$x = '';
$x .= $indexVar['linterface']->Get_Radio_Button($eclassAppConfig['pushMessageSendRight']['classTeacher']."_yes", $eclassAppConfig['pushMessageSendRight']['classTeacher'], 1, $checkYes, $Class="", $Lang['General']['Yes'], $Onclick="",$isDisabled=0);
$x .= ' ';
$x .= $indexVar['linterface']->Get_Radio_Button($eclassAppConfig['pushMessageSendRight']['classTeacher']."_no", $eclassAppConfig['pushMessageSendRight']['classTeacher'], 0, $checkNo, $Class="", $Lang['General']['No'], $Onclick="",$isDisabled=0);
$htmlAry['classTeacherRadioBtn'] = $x;

### PIC push message status
$checkYes = ($PICSendRight == 1) ? 1 : 0;
$checkNo = ($PICSendRight == 1) ? 0 : 1;
$x = '';
$x .= $indexVar['linterface']->Get_Radio_Button($eclassAppConfig['pushMessageSendRight']['clubAndActivityPIC']."_yes", $eclassAppConfig['pushMessageSendRight']['clubAndActivityPIC'], 1, $checkYes, $Class="", $Lang['General']['Yes'], $Onclick="",$isDisabled=0);
$x .= ' ';
$x .= $indexVar['linterface']->Get_Radio_Button($eclassAppConfig['pushMessageSendRight']['clubAndActivityPIC']."_no", $eclassAppConfig['pushMessageSendRight']['clubAndActivityPIC'], 0, $checkNo, $Class="", $Lang['General']['No'], $Onclick="",$isDisabled=0);
$htmlAry['PICStatusRadioBtn'] = $x;

### non-admin user can delete own push message status
$checkYes = ($nonAdminUserCanDeleteOwnPushMessageRecord == 1) ? 1 : 0;
$checkNo = ($nonAdminUserCanDeleteOwnPushMessageRecord == 1) ? 0 : 1;
$x = '';
$x .= $indexVar['linterface']->Get_Radio_Button($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['nonAdminUserCanDeleteOwnPushMessageRecord']."_yes", $eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['nonAdminUserCanDeleteOwnPushMessageRecord'], 1, $checkYes, $Class="", $Lang['General']['Yes'], $Onclick="",$isDisabled=0);
$x .= ' ';
$x .= $indexVar['linterface']->Get_Radio_Button($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['nonAdminUserCanDeleteOwnPushMessageRecord']."_no", $eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['nonAdminUserCanDeleteOwnPushMessageRecord'], 0, $checkNo, $Class="", $Lang['General']['No'], $Onclick="",$isDisabled=0);
$htmlAry['nonAdminUserCanDeleteOwnMessageRadioBtn'] = $x;


### construct table content
$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tbody>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['eClassApp']['AllowClassTeacherSendMessage'].'</td>'."\r\n";
			$x .= '<td>'.$htmlAry['classTeacherRadioBtn'].'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		if ($plugin['eEnrollment']) {
		    if($_SESSION['platform'] == "KIS" && $sys_custom['KIS_eEnrolment']['DisableApp']){
		    }else{
    			$x .= '<tr>'."\r\n";
    				$x .= '<td class="field_title">'.$Lang['eClassApp']['AllowClubAndActivityPICSendMessage'].'</td>'."\r\n";
    				$x .= '<td>'.$htmlAry['PICStatusRadioBtn'].'</td>'."\r\n";
    			$x .= '</tr>'."\r\n";
		    }
		}
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['eClassApp']['AllowDeleteOwnPushMessage'].'</td>'."\r\n";
			$x .= '<td>'.$htmlAry['nonAdminUserCanDeleteOwnMessageRadioBtn'].'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
	$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['formTable'] = $x;

### buttons
$htmlAry['submitBtn'] = $indexVar['linterface']->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn');
$htmlAry['backBtn'] = $indexVar['linterface']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');

?>
<script type="text/javascript">
$(document).ready( function() {
	
});

function goBack() {
	window.location = '?task=parentApp/advanced_setting/push_message_right/list&appType=<?=$appType?>';
}

function goSubmit() {
	$('input#task').val('parentApp/advanced_setting/push_message_right/edit_update');
	$('form#form1').submit();
}

</script>
<form name="form1" id="form1" method="POST" enctype="multipart/form-data">
	<div class="table_board">
		<?=$htmlAry['formTable']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['submitBtn']?>
		<?=$htmlAry['backBtn']?>
		<p class="spacer"></p>
	</div>
	
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="appType" name="appType" value="<?=$appType?>" />
	<input type="hidden" id="imageType" name="imageType" value="" />
</form>
<?
$indexVar['linterface']->LAYOUT_STOP();
?>