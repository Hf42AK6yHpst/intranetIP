<?php
// editing: 

/*
 * change log:
 * 2020-01-16 Tommy [ip.2.5.11.1.1.1]: hide eEnrolment app function in KIS
 * 2016-01-20 Roy: [ip.2.5.7.3.1]: only show "AllowClubAndActivityPICSendMessage" option if eEnrollment enabled
 * 2014-11-06 Ivan [ip.2.5.5.12.1]: use function to generate $TAGS_OBJ now
 * 2014-10-22 Roy: create file
 */
 
$returnMsgKey = $_GET['returnMsgKey'];
$CurrentPage = 'ParentApp_AdvancedSetting';

$MODULE_OBJ = $indexVar['libeClassApp']->getModuleObjArr();
//$TAGS_OBJ[] = array($Lang['eClassApp']['AdvancedSettingList']['PushMessageRight']);
$TAGS_OBJ = $indexVar['libeClassApp']->getParentAppAdvancedSettingsTagObj($eclassAppConfig['moduleCode']['pushMessage']);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$indexVar['linterface']->LAYOUT_START($returnMsg);

$eClassAppSettingsObj = $indexVar['libeClassApp']->getAppSettingsObj();

### get send push message right
$classTeacherSendRight = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['pushMessageSendRight']['classTeacher']);
$PICSendRight = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['pushMessageSendRight']['clubAndActivityPIC']);
$nonAdminUserCanDeleteOwnPushMessageRecord = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['nonAdminUserCanDeleteOwnPushMessageRecord']);

$classTeacherPushMessageStatus = ($classTeacherSendRight == 1) ? $Lang['General']['Yes'] : $Lang['General']['No'];
$PICPushMessageStatus = ($PICSendRight == 1) ? $Lang['General']['Yes'] : $Lang['General']['No'];
$nonAdminUserCanDeleteOwnPushMessageRecordStatus = ($nonAdminUserCanDeleteOwnPushMessageRecord == 1) ? $Lang['General']['Yes'] : $Lang['General']['No'];

### construct table content
$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tbody>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['eClassApp']['AllowClassTeacherSendMessage'].'</td>'."\r\n";
			$x .= '<td>'.$classTeacherPushMessageStatus.'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		if ($plugin['eEnrollment']) {
		    if($_SESSION['platform'] == "KIS" && $sys_custom['KIS_eEnrolment']['DisableApp']){
		    }else{
    			$x .= '<tr>'."\r\n";
    				$x .= '<td class="field_title">'.$Lang['eClassApp']['AllowClubAndActivityPICSendMessage'].'</td>'."\r\n";
    				$x .= '<td>'.$PICPushMessageStatus.'</td>'."\r\n";
    			$x .= '</tr>'."\r\n";
		    }
		}
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['eClassApp']['AllowDeleteOwnPushMessage'].'</td>'."\r\n";
			$x .= '<td>'.$nonAdminUserCanDeleteOwnPushMessageRecordStatus.'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
	$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['formTable'] = $x;

### buttons
$htmlAry['editBtn'] = $indexVar['linterface']->Get_Action_Btn($Lang['Btn']['Edit'], "button", "goEdit()", 'editBtn');

?>
<script type="text/javascript">
$(document).ready( function() {
	
});

function goEdit() {
	window.location = '?task=parentApp/advanced_setting/push_message_right/edit&appType=<?=$appType?>';
}
</script>
<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<?=$htmlAry['formTable']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['editBtn']?>
		<p class="spacer"></p>
	</div>
	
	<input type="hidden" name="appType" id="appType" value="<?=$appType?>" />
</form>

<?
$indexVar['linterface']->LAYOUT_STOP();
?>