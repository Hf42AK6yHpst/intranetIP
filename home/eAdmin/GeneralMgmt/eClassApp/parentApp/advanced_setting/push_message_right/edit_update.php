<?php
// editing:

/*
 * change log:
 * 2016-01-20 Roy: [ip.2.5.7.3.1]: only show "AllowClubAndActivityPICSendMessage" option if eEnrollment enabled
 * 2014-10-22 Roy: create file
 */
 
$eClassAppSettingsObj = $indexVar['libeClassApp']->getAppSettingsObj();
$successAry = array();

$classTeacherSendRight = $_POST[$eclassAppConfig['pushMessageSendRight']['classTeacher']];
$PICSendRight = $_POST[$eclassAppConfig['pushMessageSendRight']['clubAndActivityPIC']];
$nonAdminUserCanDeleteOwnPushMessageRecord = $_POST[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['nonAdminUserCanDeleteOwnPushMessageRecord']];

### save send right to DB
$tmpSettingsAssoAry = array();
$tmpSettingsAssoAry[$eclassAppConfig['pushMessageSendRight']['classTeacher']] = $classTeacherSendRight;
if ($plugin['eEnrollment']) {
	$tmpSettingsAssoAry[$eclassAppConfig['pushMessageSendRight']['clubAndActivityPIC']] = $PICSendRight;
}
$tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['nonAdminUserCanDeleteOwnPushMessageRecord']] = $nonAdminUserCanDeleteOwnPushMessageRecord;
$successAry[] = $eClassAppSettingsObj->saveSettings($tmpSettingsAssoAry);


$returnMsgKey = (!in_array(false, $successAry))? 'UpdateSuccess' : 'UpdateUnsuccess';
header('location: ?task=parentApp/advanced_setting/push_message_right/list&appType='.$appType.'&returnMsgKey='.$returnMsgKey);
?>