<?php
// using : 
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");


$curTab = ($_POST['curTab'])? $_POST['curTab'] : $_GET['curTab'];
$outputMode = ($_POST['outputMode'])? $_POST['outputMode'] : $_GET['outputMode'];
$keyword = ($_POST['keyword'])? standardizeFormPostValue($_POST['keyword']) : standardizeFormGetValue($_GET['keyword']);
$formClassId = ($_POST['formClassId'])? $_POST['formClassId'] : $_GET['formClassId'];
$pushMessageStatus = ($_POST['pushMessageStatus'])? $_POST['pushMessageStatus'] : $_GET['pushMessageStatus'];
$isClassTeacher = (!$_SESSION['SSV_USER_ACCESS']['eAdmin-eClassApp'] && $_SESSION["SSV_PRIVILEGE"]["eClassApp"]['classTeacher']);


### db table
$order = ($order == '') ? 0 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 2 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 50 : $numPerPage;

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("UserLogin", "ParentName");
if ($curTab == 'loggedIn') {
	$li->field_array[] = 'LastLoginTime';
}
$li->sql = $indexVar['libeClassApp']->getParentAppLoginUserSql($curTab, $formClassId, $keyword, $outputMode, $pushMessageStatus, $isClassTeacher, $hideLeftStudent = true);
$li->no_col = count($li->field_array) + 1;
$li->IsColOff = "IP25_table";


### build report sql
$sql = Get_Export_SQL_From_DB_Table_SQL($li->built_sql());
$resultAry = $li->returnArray($sql,null,2);


### build report header
$headerAry = array();
$headerAry[] = $Lang['General']['UserLogin'];
$headerAry[] = $Lang['Identity']['Parent'];
$headerAry[] = $Lang['Identity']['Student'];
if ($outputMode == 'csv') {
	$headerAry[] = $Lang['General']['Class'];
	$headerAry[] = $Lang['General']['ClassNumber'];
}
if ($curTab == 'loggedIn') {
	$headerAry[] = $Lang['eClassApp']['LastLoginTime'];
}


### output report
if ($outputMode == 'html') {
	// print
	include_once($PATH_WRT_ROOT."templates/".$LAYOUT_SKIN."/layout/print_header.php");
	include_once($PATH_WRT_ROOT.'/includes/table/table_commonTable.php');
			
	$numOfHeader = count($headerAry);
	$numOfRow = count($resultAry);
	
	$tableObj = new table_commonTable();
	$tableObj->setTableType('view');
	$tableObj->setApplyConvertSpecialChars(false);
	
	$tableObj->addHeaderTr(new tableTr());
	$tableObj->addHeaderCell(new tableTh('#'));
	for ($i=0; $i<$numOfHeader; $i++) {
		$tableObj->addHeaderCell(new tableTh($headerAry[$i]));
	}
	
	for ($i=0; $i<$numOfRow; $i++) {
		$_rowContentAry = $resultAry[$i];
		$_numOfCol = count($_rowContentAry);
		
		$tableObj->addBodyTr(new tableTr());
		$tableObj->addBodyCell(new tableTd($i+1));
		
		for ($j=0; $j<$numOfHeader; $j++) {
			$tableObj->addBodyCell(new tableTd($_rowContentAry[$j]));
		}
	}
	
	echo '<table width="100%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right">'.$indexVar['linterface']->GET_SMALL_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit2").'</td>
	</tr>
	</table>';
	echo $tableObj->returnHtml();
}
else if ($outputMode == 'csv'){
	// export csv
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	$lexport = new libexporttext();
	
	if ($curTab == 'loggedIn') {
		$fileName = 'logged_in_user.csv';
		$studentIdListIndex = 4;
	}
	else if ($curTab == 'notLoggedIn') {
		$fileName = 'not_logged_in_user.csv';
		$studentIdListIndex = 3;
	}
	
	
	### get all student info
	$numOfResult = count($resultAry);
	$consolidatedStudentIdAry = array();
	for ($i=0; $i<$numOfResult; $i++) {
		$_studentIdList = $resultAry[$i][$studentIdListIndex];
		
		$_studentIdAry = explode(', ', $_studentIdList);
		$consolidatedStudentIdAry = array_merge($consolidatedStudentIdAry, $_studentIdAry);
	}
	$userObj = new libuser('', '', $consolidatedStudentIdAry);
	
	
	### consolidate data to one student one row
	$exportDataAry = array();
	$rowCount = 0; 
	for ($i=0; $i<$numOfResult; $i++) {
		$_parentUserLogin = $resultAry[$i][0];
		$_parentName = $resultAry[$i][1];
		$_studentName = $resultAry[$i][2];
		if ($curTab == 'loggedIn') {
			$_loginTime = $resultAry[$i][3];
		}
		$_studentIdList = $resultAry[$i][$studentIdListIndex];
		
		
		// W78469
//		$_studentNameAry = explode(', ', $_studentName);
//		$_numOfStudent = count($_studentNameAry);
		
		$_studentIdAry = explode(', ', $_studentIdList);
		$_numOfStudent = count($_studentIdAry);
		
		if ($_numOfStudent == 0) {
			$exportDataAry[$rowCount][] = $_parentUserLogin;
			$exportDataAry[$rowCount][] = $_parentName;
			$exportDataAry[$rowCount][] = '';
			$exportDataAry[$rowCount][] = $_loginTime;
			$rowCount++;
		}
		else {
			for ($j=0; $j<$_numOfStudent; $j++) {
				//$__studentNameFull = $_studentNameAry[$j];
				$__studentId = $_studentIdAry[$j];
				
				// separate student name, class name and class number from "xxx xxx xxx (1A-1)"
//				$__studentNamePiece = explode(' (', $__studentNameFull);
//				$__studentName = $__studentNamePiece[0];
//				$__classNameNumber = str_replace(')', '', $__studentNamePiece[1]);
//				$__classNameNumberPiece = explode('-', $__classNameNumber);
//				$__className = $__classNameNumberPiece[0];
//				$__classNumber = $__classNameNumberPiece[1];

				$userObj->LoadUserData($__studentId);
				$__studentName = Get_Lang_Selection($userObj->ChineseName, $userObj->EnglishName);
				$__className = $userObj->ClassName;
				$__classNumber = $userObj->ClassNumber;
				
				$exportDataAry[$rowCount][] = $_parentUserLogin;
				$exportDataAry[$rowCount][] = $_parentName;
				$exportDataAry[$rowCount][] = $__studentName;
				$exportDataAry[$rowCount][] = $__className;
				$exportDataAry[$rowCount][] = $__classNumber;
				$exportDataAry[$rowCount][] = $_loginTime;
				$rowCount++;
			}
		}
	}
	
	$exportContent = $lexport->GET_EXPORT_TXT($exportDataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=1);
	$lexport->EXPORT_FILE($fileName, $exportContent);
}
?>