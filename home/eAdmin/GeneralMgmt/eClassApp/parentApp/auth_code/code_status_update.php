<?php
// using : 

include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_authCode.php");

$appType = $_POST['appType'];
$targetUserId = $_POST['targetUserId'];
$recordStatus = $_POST['targetCodeStatus'];
$codeIdAry = $_POST['codeIdAry'];

$libAuthCode = new libeClassApp_authCode();
$success = $libAuthCode->updateAuthCodeStatus($codeIdAry, $recordStatus);

$task = 'parentApp/auth_code/details&targetUserId='.$targetUserId;
$returnMsgKey = ($success)? 'UpdateSuccess' : 'UpdateUnsuccess';
header('location: ?appType='.$appType.'&task='.$task.'&returnMsgKey='.$returnMsgKey);
?>