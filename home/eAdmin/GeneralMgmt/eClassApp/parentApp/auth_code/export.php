<?php
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_authCode.php");

$luser = new libuser();
$lexport = new libexporttext();
$libAuthCode = new libeClassApp_authCode();



### get student
$studentInfoAry = $libAuthCode->getStudentInfo($parYearClassIdAry='');
$studentIdAry = Get_Array_By_Key($studentInfoAry, 'UserID');
$studentUserObj = new libuser('', '', $studentIdAry);


### get parent
$parentAry = $luser->getParentStudentMappingInfo($studentIdAry);
$parentIdAry = Get_Array_By_Key($parentAry, 'ParentID');
$parentUserObj = new libuser('', '', $parentIdAry);
$parentStudentAssoAry = BuildMultiKeyAssoc($parentAry, 'ParentID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);


### get auth code
//$authCodeAry = $libAuthCode->getAuthCodeData($parentIdAry);
$tmpFilterCodeStatusAry = array($eclassAppConfig['APP_AUTH_CODE']['RecordStatus']['active'], $eclassAppConfig['APP_AUTH_CODE']['RecordStatus']['inactive']);
$authCodeAry = $libAuthCode->getAuthCodeData($parentIdAry, $filterCodeStatus='', $authCode='', $deviceId='', $tmpFilterCodeStatusAry);
$numOfCode = count($authCodeAry);

$headerAry = array();
$headerAry[] = $Lang['General']['UserLogin'];
$headerAry[] = $Lang['Identity']['Parent'];
$headerAry[] = $Lang['Identity']['Student'];
$headerAry[] = $Lang['eClassApp']['AuthorizationCode'];
$headerAry[] = $Lang['eClassApp']['EnableStatus'];
$headerAry[] = $Lang['eClassApp']['RegistrationStatus'];
$headerAry[] = $Lang['eClassApp']['RegistrationDate'];

$dataAry = array();
for ($i=0; $i<$numOfCode; $i++) {
	$_parentId = $authCodeAry[$i]['UserID'];
	$_authCode = $authCodeAry[$i]['AuthCode'];
	$_deviceId = $authCodeAry[$i]['DeviceID'];
	$_registrationDate = $authCodeAry[$i]['RegistrationDate'];
	$_recordStatus = $authCodeAry[$i]['RecordStatus'];
	
	if (is_date_empty($_registrationDate)) {
		$_useStatus = $Lang['eClassApp']['NotYetRegistered'];
	}
	else {
		$_useStatus = $Lang['eClassApp']['Registered'];
	}
	
	if ($_recordStatus == $eclassAppConfig['APP_AUTH_CODE']['RecordStatus']['active']) {
		$_recordStatus = $Lang['General']['Enabled'];
	}
	else {
		$_recordStatus = $Lang['General']['Disabled'];
	}
	
	$_registrationDate = ($_registrationDate=='')? $Lang['General']['EmptySymbol'] : $_registrationDate;
	
	$parentUserObj->LoadUserData($_parentId);
	$_userLogin = $parentUserObj->UserLogin;
	$_parentName = Get_Lang_Selection($parentUserObj->ChineseName, $parentUserObj->EnglishName);
	
	$_childrenName = implode(', ', Get_Array_By_Key((array)$parentStudentAssoAry[$_parentId], 'StudentName'));
	$_childrenName = ($_childrenName=='')? $Lang['General']['EmptySymbol'] : $_childrenName;
	
	$dataAry[$i][] = $_userLogin;
	$dataAry[$i][] = $_parentName;
	$dataAry[$i][] = $_childrenName;
	$dataAry[$i][] = $_authCode;
	$dataAry[$i][] = $_useStatus;
	$dataAry[$i][] = $_recordStatus;
	$dataAry[$i][] = $_registrationDate;
}

$fileName = 'auth_code.csv';
$exportContent = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=1);
$lexport->EXPORT_FILE($fileName, $exportContent);

?>