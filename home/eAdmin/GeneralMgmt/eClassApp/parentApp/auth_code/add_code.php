<?php
// using : 
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");


$returnMsgKey = $_GET['returnMsgKey'];
$targetUserId = $_POST['targetUserId'];


$CurrentPage = 'ParentApp_AuthCode';
$MODULE_OBJ = $indexVar['libeClassApp']->getModuleObjArr();
$TAGS_OBJ[] = array($Lang['eClassApp']['AuthorizationCode']);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$indexVar['linterface']->LAYOUT_START($returnMsg);


### construct table content
$commonChooseLink = '/home/common_choose/index.php?fieldname=TargetUserIDAry[]&page_title=SelectRecipients&permitted_type=3&excluded_type=4&DisplayGroupCategory=1&Disable_AddGroup_Button=1';
$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tbody>'."\r\n"; 
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$indexVar['linterface']->RequiredSymbol().$Lang['Identity']['Parent'].'</td>'."\r\n";
			$x .= '<td>'."\r\n";
				if ($targetUserId == '') {
					// select users
					$x .= '<table border="0">'."\r\n";
						$x .= '<tr>'."\r\n";
							$x .= '<td>'."\r\n";
								$x .= '<select id="TargetUserSel" name="TargetUserIDAry[]" size="10" multiple><option>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option></select>'."\r\n";
							$x .= '</td>'."\r\n";
							$x .= '<td style="vertical-align:bottom;">'."\r\n";
								$x .= $indexVar['linterface']->GET_SMALL_BTN($Lang['General']['ChooseUser'], "button", "newWindow('".$commonChooseLink."',3)")."\r\n";
								$x .= '<br />'."\r\n";
								$x .= $indexVar['linterface']->GET_SMALL_BTN($Lang['Btn']['RemoveSelected'], "button", "checkOptionRemove(document.form1.elements['Recipient[]'])");
							$x .= '</td>'."\r\n";
						$x .= '</tr>'."\r\n";
					$x .= '</table>'."\r\n";	
				}
				else {
					// show chosen user's info
					$userObj = new libuser($targetUserId);
					
					$x .= Get_Lang_Selection($userObj->ChineseName, $userObj->EnglishName);
					$x .= '<input type="hidden" name="TargetUserIDAry[]" value="'.$targetUserId.'" />'."\r\n";
				}
			$x .= '</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$indexVar['linterface']->RequiredSymbol().$Lang['eClassApp']['AddNoOfCode'].'</td>'."\r\n";
			$x .= '<td>'."\r\n";
				$x .= $indexVar['linterface']->Get_Number_Selection('NumOfCodeSel', 1, 10, $SelectedValue=5, $Onchange='', $noFirst=1, $isAll=0, $FirstTitle='', $Disabled=0);
			$x .= '</td>'."\r\n";
		$x .= '</tr>'."\r\n";
	$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['formTable'] = $x;


### buttons
$htmlAry['submitBtn'] = $indexVar['linterface']->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn');
$htmlAry['backBtn'] = $indexVar['linterface']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');
?>
<script type="text/javascript">
$(document).ready( function() {
	
});

function goBack() {
	window.location = "?appType=P&task=parentApp/auth_code/list";
}

function goSubmit() {
	var canSubmit = true;
	
	<? if ($targetUserId == '') { ?>
	var userSelObj = document.getElementById('form1').elements["TargetUserIDAry[]"];
    var userCount = 0;
	for(i=0;i<userSelObj.length;++i) {
    	if(userSelObj.options[i].value == '' || userSelObj.options[i].value == 0) continue;
    	userCount++;
    }
    if(userCount==0) {
		alert("<?=$Lang['eClassApp']['jsWarning']['selectAtLeastOneParent']?>");
		canSubmit = false;
	}
	<? } ?>
	
	if (canSubmit) {
		$('select#TargetUserSel option').attr('selected', 'selected');
		$('input#task').val('parentApp/auth_code/add_code_update');
		$('form#form1').attr('action', 'index.php').submit();
	}
}
</script>
<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<?=$htmlAry['formTable']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['submitBtn']?>
		<?=$htmlAry['backBtn']?>
		<p class="spacer"></p>
	</div>
	
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="appType" name="appType" value="<?=$appType?>" />
	<input type="hidden" id="targetUserId" name="targetUserId" value="<?=$targetUserId?>" />
</form>
<?
$indexVar['linterface']->LAYOUT_STOP();
?>