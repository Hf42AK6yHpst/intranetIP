<?php
// using : 
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_authCode.php");

$returnMsgKey = $_GET['returnMsgKey'];


$lclass = new libclass();
$luser = new libuser();
$libAuthCode = new libeClassApp_authCode();


$CurrentPage = 'ParentApp_AuthCode';
$MODULE_OBJ = $indexVar['libeClassApp']->getModuleObjArr();
$TAGS_OBJ[] = array($Lang['eClassApp']['AuthorizationCode']);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$indexVar['linterface']->LAYOUT_START($returnMsg);

### content tool
// $btnAry[] = array($btnClass, $onclickJs, $displayLang, $subBtnAry);
$btnAry = array();
$btnAry[] = array('new', 'javascript: goNew();');
$btnAry[] = array('export', 'javascript: goExport();');
$htmlAry['contentTool'] = $indexVar['linterface']->Get_Content_Tool_By_Array_v30($btnAry);


### search box
//$htmlAry['searchBox'] = $indexVar['linterface']->Get_Search_Box_Div('keyword', $keyword);


### class filtering
$htmlAry['formClassSel'] = $lclass->getSelectClassWithWholeForm('id="formClassSel" name="formClassId" onchange="changedFormClassSelection();"', $formClassId, $Lang['SysMgr']['FormClassMapping']['AllClass']);
$targetYearClassIdAry = '';
if ($formClassId != '' && $formClassId != "0") {
	$targetYearClassIdAry = array();
    if (is_numeric($formClassId)) {
    	// selected whole Form => get all Classes of the Form
    	$targetYearClassIdAry = Get_Array_By_Key($lclass->returnClassListByLevel($formClassId), 'YearClassID');
	}
    else {
    	// selected one class only
    	$targetYearClassIdAry[] = substr($formClassId, 2);
    }
}


### register status filtering
$htmlAry['registerStatusSel'] = $libAuthCode->getAuthCodeParentStatusSelection('registerStatusSel', 'registerStatus', $registerStatus, 'changedRegisterStatusSelection();');


### get student
$studentInfoAry = $libAuthCode->getStudentInfo($targetYearClassIdAry);
$studentIdAry = Get_Array_By_Key($studentInfoAry, 'UserID');
$numOfStudent = count($studentInfoAry);


### get parent
$parentAry = $luser->getParentStudentMappingInfo($studentIdAry);
$parentIdAry = Get_Array_By_Key($parentAry, 'ParentID');
$parentUserObj = new libuser('', '', $parentIdAry);

$studentParentAssoAry = BuildMultiKeyAssoc($parentAry, 'StudentID', $IncludedDBField=array('ParentID'), $SingleValue=1, $BuildNumericArray=1);
$parentStudentAssoAry = BuildMultiKeyAssoc($parentAry, 'ParentID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);


### get auth code data
$tmpFilterCodeStatusAry = array($eclassAppConfig['APP_AUTH_CODE']['RecordStatus']['active'], $eclassAppConfig['APP_AUTH_CODE']['RecordStatus']['inactive']);
$authCodeAssoAry = BuildMultiKeyAssoc($libAuthCode->getAuthCodeData($parentIdAry, '', '', '', $tmpFilterCodeStatusAry), 'UserID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);


### build table
$x = '';
$x .= '<table class="common_table_list_v30">';
	$x .= '<thead>';
		$x .= '<tr>';
			$x .= '<th style="width:20px;">#</th>';
			$x .= '<th style="width:18%;">'.$Lang['General']['UserLogin'].'</th>';
			$x .= '<th style="width:20%;">'.$Lang['Identity']['Parent'].'</th>';
			$x .= '<th style="width:20%;">'.$Lang['Identity']['Student'].'</th>';
			$x .= '<th style="width:20%;">'.$Lang['eClassApp']['NoOfCode'].'</th>';
			$x .= '<th style="width:20%;">'.$Lang['eClassApp']['NoOfUsedCode'].'</th>';
		$x .= '</tr>';
	$x .= '</thead>';
	
	$x .= '<tbody>';
		$parentCount = 0;
		$displayedParentUserIdAry = array();
		for ($i=0; $i<$numOfStudent; $i++) {
			$_studentId = $studentInfoAry[$i]['UserID'];
			$_parentIdAry = $studentParentAssoAry[$_studentId];
			$_numOfParent = count($_parentIdAry);
			
			for ($j=0; $j<$_numOfParent; $j++) {
				$__parentId = $_parentIdAry[$j];
				
				if (in_array($__parentId, (array)$displayedParentUserIdAry)) {
					continue;
				}
				
				// get parent info
				$parentUserObj->LoadUserData($__parentId);
				$__parentName = Get_Lang_Selection($parentUserObj->ChineseName, $parentUserObj->EnglishName);
				
				// get parent's children info
				$__childrenName = implode(',<br>', Get_Array_By_Key((array)$parentStudentAssoAry[$__parentId], 'StudentName'));
				$__childrenName = ($__childrenName=='')? '&nbsp;' : $__childrenName;
				
				// get parent's auth code data
				$__numOfTotalCode = 0;
				$__numOfUsedCode = 0;
				if (isset($authCodeAssoAry[$__parentId])) {
					$__authCodeAry = $authCodeAssoAry[$__parentId];
					$__numOfAuthCode = count((array)$__authCodeAry);
					for ($k=0; $k<$__numOfAuthCode; $k++) {
						$___registrationDate = $__authCodeAry[$k]['RegistrationDate'];
						
						$__numOfTotalCode++;
						if ($___registrationDate != '') {
							$__numOfUsedCode++;
						}
					}
				}
				$__numOfTotalCodeDisplay = '<a class="tabletextremark" href="javascript:void(0);" onclick="goDetails(\''.$__parentId.'\', \'\');">'.$__numOfTotalCode.'</a>';
				$__numOfUsedCodeDisplay = '<a class="tabletextremark" href="javascript:void(0);" onclick="goDetails(\''.$__parentId.'\', \'1\');">'.$__numOfUsedCode.'</a>';
				
				if ($registerStatus == 1 && $__numOfUsedCode == 0) {
					// show registered users only
					continue;
				}
				else if ($registerStatus == 2 && $__numOfUsedCode > 0) {
					// show NOT registered users only
					continue;
				}
				
				$x .= '<tr>';
					$x .= '<td>'.($parentCount+1).'</td>';
					$x .= '<td>'.$parentUserObj->UserLogin.'</td>';
					$x .= '<td>'.$__parentName.'</td>';
					$x .= '<td>'.$__childrenName.'</td>';
					$x .= '<td>'.$__numOfTotalCodeDisplay.'</td>';
					$x .= '<td>'.$__numOfUsedCodeDisplay.'</td>';
				$x .= '</tr>';
				
				$displayedParentUserIdAry[] = $__parentId;
				$parentCount++; 
			}
		}
		
		if ($parentCount == 0) {
			$x .= '<tr><td colspan="6" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>';
		}
	$x .= '</tbody>';
$x .= '</table>';
$htmlAry['dataTable'] = $x;

?>
<script type="text/javascript">
$(document).ready( function() {
	
});

function changedFormClassSelection() {
	reloadPage();
}

function changedRegisterStatusSelection() {
	reloadPage();
}

function reloadPage() {
	$('input#task').val('parentApp/auth_code/list');
	$('form#form1').attr('action', 'index.php').submit();
}

function goNew() {
	window.location = "?appType=P&task=parentApp/auth_code/add_code";
}

function goDetails(parUserId, parCodeStatus) {
	$('input#targetUserId').val(parUserId);
	$('input#filterCodeStatus').val(parCodeStatus);
	$('input#task').val('parentApp/auth_code/details');
	
	$('form#form1').attr('action', 'index.php').submit();
}

function goExport() {
	$('input#task').val('parentApp/auth_code/export');
	
	$('form#form1').attr('action', 'index.php').submit();
}
</script>
<form name="form1" id="form1" method="POST">
	<div class="content_top_tool">
		<?=$htmlAry['contentTool']?>
		<?=$htmlAry['searchBox']?>
		<br style="clear:both;">
	</div>
	
	<div class="table_board">
		<div class="table_filter">
			<?=$htmlAry['formClassSel']?>
			<?=$htmlAry['registerStatusSel']?>
		</div>
		<p class="spacer"></p>
		
		<?=$htmlAry['dataTable']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
	
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="appType" name="appType" value="<?=$appType?>" />
	<input type="hidden" id="targetUserId" name="targetUserId" value="<?=$appType?>" />
	<input type="hidden" id="filterCodeStatus" name="filterCodeStatus" value="" />
</form>
<?
$indexVar['linterface']->LAYOUT_STOP();
?>