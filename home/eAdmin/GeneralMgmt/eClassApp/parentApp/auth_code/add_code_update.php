<?php
// using : 

include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_authCode.php");

$appType = $_POST['appType'];
$targetUserId = $_POST['targetUserId'];

$libAuthCode = new libeClassApp_authCode();

$targetUserIdAry = array_values(array_remove_empty($_POST['TargetUserIDAry']));
$generateCodeNum = IntegerSafe($_POST['NumOfCodeSel']);


$consolidatedUserIdAry = Get_User_Array_From_Common_Choose($targetUserIdAry, array(3));	// return parent only
$success = $libAuthCode->addAuthCodeForUser($consolidatedUserIdAry, $generateCodeNum);


if ($targetUserId == '') {
	$task = 'parentApp/auth_code/list';
}
else {
	$task = 'parentApp/auth_code/details&targetUserId='.$targetUserId;
}

$returnMsgKey = ($success)? 'UpdateSuccess' : 'UpdateUnsuccess';
header('location: ?appType='.$appType.'&task='.$task.'&returnMsgKey='.$returnMsgKey);
?>