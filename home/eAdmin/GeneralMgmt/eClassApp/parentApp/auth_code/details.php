<?php
// using : 
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_authCode.php");


$returnMsgKey = $_GET['returnMsgKey'];
$targetUserId = ($_POST['targetUserId'])? $_POST['targetUserId'] : $_GET['targetUserId'];
$filterCodeStatus = $_POST['filterCodeStatus'];
$filterRecordStatus = $_POST['filterRecordStatus'];

$luser = new libuser();
$userObj = new libuser($targetUserId);
$libAuthCode = new libeClassApp_authCode();


$CurrentPage = 'ParentApp_AuthCode';
$MODULE_OBJ = $indexVar['libeClassApp']->getModuleObjArr();
$TAGS_OBJ[] = array($Lang['eClassApp']['AuthorizationCode']);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$indexVar['linterface']->LAYOUT_START($returnMsg);


### content tool
// $btnAry[] = array($btnClass, $onclickJs, $displayLang, $subBtnAry);
$btnAry = array();
$btnAry[] = array('new', 'javascript: goNew();');
$htmlAry['contentTool'] = $indexVar['linterface']->Get_Content_Tool_By_Array_v30($btnAry);


### auth code status filter
$htmlAry['recordStatusSel'] = $libAuthCode->getAuthCodeRecordStatusSelection('recordStatusSel', 'filterRecordStatus', $filterRecordStatus, 'reloadPage();');
$htmlAry['inUseStatusSel'] = $libAuthCode->getAuthCodeStatusSelection('codeStatusSel', 'filterCodeStatus', $filterCodeStatus, 'reloadPage();');


### DB table action buttons
$btnAry = array();
$btnAry[] = array('approve', 'javascript: changeCodeStatus('.$eclassAppConfig['APP_AUTH_CODE']['RecordStatus']['active'].');', $Lang['Btn']['Enable']);
$btnAry[] = array('reject', 'javascript: changeCodeStatus('.$eclassAppConfig['APP_AUTH_CODE']['RecordStatus']['inactive'].');', $Lang['Btn']['Disable']);
$btnAry[] = array('delete', 'javascript: checkRemove2(document.form1, \'codeIdAry[]\', \'goDeleteAuthCode()\');', $Lang['Btn']['Delete']);
$htmlAry['dbTableActionBtn'] = $indexVar['linterface']->Get_DBTable_Action_Button_IP25($btnAry);


### get parent auth code data
if ($filterRecordStatus == '') {
	// for "all status" filtering => exclude deleted records 
	$tmpFilterCodeStatusAry = array($eclassAppConfig['APP_AUTH_CODE']['RecordStatus']['active'], $eclassAppConfig['APP_AUTH_CODE']['RecordStatus']['inactive']);
}
else {
	$tmpFilterCodeStatusAry = $filterRecordStatus;
}
$authCodeAry = $libAuthCode->getAuthCodeData($targetUserId, $filterCodeStatus, $authCode='', $deviceId='', $tmpFilterCodeStatusAry);
$numOfAuthCode = count($authCodeAry);


### get student info
$parentAry = $luser->getParentStudentMappingInfo();
$parentStudentAssoAry = BuildMultiKeyAssoc($parentAry, 'ParentID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
$childrenName = implode(',<br>', Get_Array_By_Key((array)$parentStudentAssoAry[$targetUserId], 'StudentName'));
$childrenName = ($childrenName=='')? '&nbsp;' : $childrenName;



### construct table content
$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tbody>'."\r\n"; 
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['General']['UserLogin'].'</td>'."\r\n";
			$x .= '<td>'."\r\n";
				$x .= $userObj->UserLogin;
			$x .= '</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['Identity']['Parent'].'</td>'."\r\n";
			$x .= '<td>'."\r\n";
				$x .= Get_Lang_Selection($userObj->ChineseName, $userObj->EnglishName);
			$x .= '</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['Identity']['Student'].'</td>'."\r\n";
			$x .= '<td>'."\r\n";
				$x .= $childrenName;
			$x .= '</td>'."\r\n";
		$x .= '</tr>'."\r\n";
	$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['formTable'] = $x;



$x = '';
$x .= '<table class="common_table_list_v30">'."\r\n";
	$x .= '<thead>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<th width="20">#</th>'."\r\n";
			$x .= '<th width="23%">'.$Lang['eClassApp']['AuthorizationCode'].'</th>'."\r\n";
			$x .= '<th width="23%">'.$Lang['eClassApp']['EnableStatus'].'</th>'."\r\n";
			$x .= '<th width="23%">'.$Lang['eClassApp']['RegistrationStatus'].'</th>'."\r\n";
			$x .= '<th width="23%">'.$Lang['eClassApp']['RegistrationDate'].'</th>'."\r\n";
			$x .= '<th width="20">'.$indexVar['linterface']->Get_Checkbox('codeChk_global', 'codeChk_global', $Value, $isChecked=0, $Class='', $Display='', $Onclick='Check_All_Options_By_Class(\'codeIdChk\', this.checked)', $Disabled='').'</th>'."\r\n";
		$x .= '</tr>'."\r\n";
	$x .= '</thead>'."\r\n";
	$x .= '<tbody>'."\r\n";
	if ($numOfAuthCode == 0) {
		$x .= '<tr><td colspan="6" style="text-align:center;">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>';
	}
	else {
		for ($i=0; $i<$numOfAuthCode; $i++) {
			$_recordId = $authCodeAry[$i]['RecordID'];
			$_deviceId = $authCodeAry[$i]['DeviceID'];
			$_authCode = $authCodeAry[$i]['AuthCode'];
			$_registrationDate = $authCodeAry[$i]['RegistrationDate'];
			$_recordStatus = $authCodeAry[$i]['RecordStatus'];
			
			if (is_date_empty($_registrationDate)) {
				$_useStatus = $Lang['eClassApp']['NotYetRegistered'];
			}
			else {
				$_useStatus = $Lang['eClassApp']['Registered'];
			}
			
			if ($_recordStatus == $eclassAppConfig['APP_AUTH_CODE']['RecordStatus']['active']) {
				$_recordStatus = $Lang['General']['Enabled'];
			}
			else {
				$_recordStatus = $Lang['General']['Disabled'];
			}
			
			$_registrationDate = ($_registrationDate=='')? $Lang['General']['EmptySymbol'] : $_registrationDate;
			
			$x .= '<tr>'."\r\n";
				$x .= '<td>'.($i+1).'</td>'."\r\n";
				$x .= '<td>'.$_authCode.'</td>'."\r\n";
				$x .= '<td>'.$_recordStatus.'</td>'."\r\n";
				$x .= '<td>'.$_useStatus.'</td>'."\r\n";
				$x .= '<td>'.$_registrationDate.'</td>'."\r\n";
				$x .= '<td>'.$indexVar['linterface']->Get_Checkbox('codeChk_'.$_recordId, 'codeIdAry[]', $_recordId, $isChecked=0, $Class='codeIdChk', '', 'Uncheck_SelectAll(\'codeChk_global\', this.checked);').'</td>'."\r\n";
			$x .= '</tr>'."\r\n";
		}
	}
	$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['dataTable'] = $x;


### buttons
$htmlAry['backBtn'] = $indexVar['linterface']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');
?>
<script type="text/javascript">
$(document).ready( function() {
	
});

function goNew() {
	$('input#task').val('parentApp/auth_code/add_code');
	$('form#form1').attr('action', 'index.php').submit();
}

function goBack() {
	window.location = "?appType=P&task=parentApp/auth_code/list";
}

function goDeleteAuthCode() {
	changeCodeStatus('<?=$eclassAppConfig['APP_AUTH_CODE']['RecordStatus']['deleted']?>');
}

function reloadPage() {
	$('input#task').val('parentApp/auth_code/details');
	$('form#form1').attr('action', 'index.php').submit();
}

function changeCodeStatus(parCodeStatus) {
	if ($('input.codeIdChk:checked').length == 0) {
		alert(globalAlertMsg2);
	}
	else {
		$('input#targetCodeStatus').val(parCodeStatus);
		$('input#task').val('parentApp/auth_code/code_status_update');
		$('form#form1').submit();
	}
}
</script>
<form name="form1" id="form1" method="POST">
	<div class="content_top_tool">
		<?=$htmlAry['contentTool']?>
		<?=$htmlAry['searchBox']?>
		<br style="clear:both;">
	</div>
	<div class="table_board">
		<?=$htmlAry['formTable']?>
		<br />
		<?=$htmlAry['recordStatusSel']?>
		<?=$htmlAry['inUseStatusSel']?>
		<br />
		<?=$htmlAry['dbTableActionBtn']?>
		<?=$htmlAry['dataTable']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['backBtn']?>
		<p class="spacer"></p>
	</div>
	
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="appType" name="appType" value="<?=$appType?>" />
	<input type="hidden" id="targetUserId" name="targetUserId" value="<?=$targetUserId?>" />
	<input type="hidden" id="targetCodeStatus" name="targetCodeStatus" value="" />
</form>
<?
$indexVar['linterface']->LAYOUT_STOP();
?>