<?php
#editing:

/******** Change Log ***********
 * 
 * 	20170615 Icarus
 * 				- Changed javascript:newWindow() to show_dyn_thickbox_ip()
 * 
 ********************************/


$appType = isset($_POST['appType'])? $_POST['appType'] : $_GET['appType'];

$permittedTypeAry = array();
if ($appType == $eclassAppConfig['appType']['Parent']) {
	$CurrentPage = 'ParentApp_Blacklist';
	$appTypePath = 'parentApp';
	$permittedTypeAry[] = USERTYPE_PARENT;
}
else if ($appType == $eclassAppConfig['appType']['Student']) {
	$CurrentPage = 'StudentApp_Blacklist';
	$appTypePath = 'studentApp';
	$permittedTypeAry[] = USERTYPE_STUDENT;
}

$MODULE_OBJ = $indexVar['libeClassApp']->getModuleObjArr();
$TAGS_OBJ[] = array($Lang['eClassApp']['Blacklist']);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$indexVar['linterface']->LAYOUT_START($returnMsg);


### Navigation
$PAGE_NAVIGATION[] = array($Lang['eClassApp']['Blacklist'], "javascript:goBack()"); 
$PAGE_NAVIGATION[] = array($Lang['Btn']['New'], "");
$htmlAry['navigation'] = $indexVar['linterface']->GET_NAVIGATION_IP25($PAGE_NAVIGATION);


### Choose Member Btn
$userSel = '<select id="SelectedUserIDSel" name="SelectedUserIdAry[]" multiple="multiple" size="10"></select>';


// $blacklistedUserIdAry = Get_Array_By_Key($indexVar['libeClassApp']->getBlacklistUser($eclassAppConfig['INTRANET_APP_BLACKLIST_USER']['RecordStatus']['active']), 'UserID');
// $excludeUserIdParam = '&exclude_user_id_ary[]='.implode('&exclude_user_id_ary[]=', $blacklistedUserIdAry);


//U77554
//$param = "fieldname=SelectedUserIdAry[]&page_title=SelectMembers&permitted_type=".implode(',',$permittedTypeAry)."&excluded_type=4&filterAppParent=1&filterAppTeacher=1&DisplayGroupCategory=1&Disable_AddGroup_Button=1".$excludeUserIdParam;
$param = "fieldname=SelectedUserIdAry[]&page_title=SelectMembers&permitted_type=".implode(',',$permittedTypeAry)."&excluded_type=4&DisplayGroupCategory=1&Disable_AddGroup_Button=1".$excludeUserIdParam;


#$chooseMemberBtn = $indexVar['linterface']->GET_SMALL_BTN($Lang['Btn']['Select'], "button", "javascript:newWindow('".$PATH_WRT_ROOT."home/common_choose/index.php?$param', 9)");
$chooseMemberBtn = $indexVar['linterface']->GET_SMALL_BTN($Lang['Btn']['Select'], "button", "show_dyn_thickbox_ip('','".$PATH_WRT_ROOT."home/common_choose/index.php?$param&from_thickbox=1"."', '', 0, 565, 640, 1, 'fakeDiv', null)");
$removeMemberBtn = $indexVar['linterface']->GET_SMALL_BTN($Lang['Btn']['RemoveSelected'], "button", "checkOptionRemove(document.form1.elements['SelectedUserIdAry[]'])");
$x = '';
$x .= '<div>'."\r\n";
	$x .= '<div style="float:left;">'.$userSel.'</div>'."\r\n";
	$x .= '<div>'.$chooseMemberBtn.'<br>'.$removeMemberBtn.'</div>'."\r\n";
	$x .= '<br style="clear:both;" />'."\n";
	$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
$x .= '</div>'."\r\n";
$htmlAry['addMemberSelection'] = $x;


### Submit Button
$htmlAry['submitBtn'] = $indexVar['linterface']->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", $onclick="goAddUser();", $id="Btn_Submit");
$htmlAry['cancelBtn'] = $indexVar['linterface']->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", $onclick="goBack();", $id="Btn_Cancel");
	

$x = '';
$x .= '<table width="100%" class="form_table_v30">'."\n";
	$x .= '<col class="field_title">';
	$x .= '<col class="field_c">';
	
	# Add member
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['Name'].'</td>'."\n";
		$x .= '<td>'.$htmlAry['addMemberSelection'].'</td>'."\n";
	$x .= '</tr>'."\n";
$x .= '</table>'."\n";
$htmlAry['formTable'] = $x;
?>
<?=$indexVar['linterface']->Include_JS_CSS(); ?>
<script type="text/javascript">
$(document).ready( function() {
	
});

function goBack() {
	window.location = "?appType=<?=$appType?>&task=<?=$appTypePath?>/blacklist/list";
}

function goAddUser() {
	var canSubmit = true;
	var objForm = document.getElementById('form1');
	
	var userSelObj = document.getElementById('form1').elements["SelectedUserIdAry[]"];
    var userCount = 0;
	for(i=0;i<userSelObj.length;++i) {
    	if(userSelObj.options[i].value == '' || userSelObj.options[i].value == 0) continue;
    	userCount++;
    }
    if(userCount==0) {
		alert("<?=$Lang['eClassApp']['jsWarning']['selectAtLeastOneParent']?>");
		canSubmit = false;
	}
	
	if (canSubmit) {
		checkOptionAll(objForm.elements["SelectedUserIdAry[]"]);
		$('input#task').val('parentApp/blacklist/add_user_update');
		$('form#form1').attr('action', 'index.php').submit();
	}
}
</script>
<form id="form1" name="form1" method="post">
	<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	<br style="clear:both;" />
	
	<?=$htmlAry['stepTable']?>
	
	<div class="table_board">
		<?=$htmlAry['formTable']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['submitBtn']?>
		<?=$htmlAry['cancelBtn']?>
		<p class="spacer"></p>
	</div>
	
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="appType" name="appType" value="<?=$appType?>" />
</form>
<?
$indexVar['linterface']->LAYOUT_STOP();
?>