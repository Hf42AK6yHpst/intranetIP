<?php
// using : 

$appType = $_POST['appType'];
$userIdAry = $_POST['userIdAry'];

if ($appType == $eclassAppConfig['appType']['Parent']) {
	$appTypePath = 'parentApp';
}
else if ($appType == $eclassAppConfig['appType']['Student']) {
	$appTypePath = 'studentApp';
}

$success = $indexVar['libeClassApp']->deleteBlacklistUser($userIdAry);

$task = $appTypePath.'/blacklist/list';
$returnMsgKey = ($success)? 'UpdateSuccess' : 'UpdateUnsuccess';
header('location: ?appType='.$appType.'&task='.$task.'&returnMsgKey='.$returnMsgKey);
?>