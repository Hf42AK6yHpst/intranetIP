<?php
##### Change Log [Start] #####
#   2019-01-03 (Anna)
#   added $sys_custom['DHL'] cust
#
# - 2011-03-10 (Henry Chow)
#	allow System Admin to view "School News"
#
###### Change Log [End] ######

	$PATH_WRT_ROOT = "../../../../";
	$CurrentPageArr['schoolNews'] = 1;
	
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	include_once($PATH_WRT_ROOT."includes/libfiletable.php");
	include_once($PATH_WRT_ROOT."includes/libannounce.php");

	intranet_auth();
	intranet_opendb();
	
	if($sys_custom['DHL']){
	    include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
	    $libdhl = new libdhl();
	    $isPIC = $libdhl->isPIC();
	}

	if(!($_SESSION['SSV_USER_ACCESS']["other-schoolNews"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || ($isPIC && $sys_custom['DHL']) )){
			include_once($PATH_WRT_ROOT."includes/libaccessright.php");
			$laccessright = new libaccessright();
			$laccessright->NO_ACCESS_RIGHT_REDIRECT();
			exit;
	}

	# Temp Assign memory of this page
	ini_set("memory_limit", "800M"); 

	$lo = new libannounce($AnnouncementID);

	$MODULE_OBJ['title'] = $ip20_what_is_new;
	$linterface = new interface_html("popup_index.html");
	$linterface->LAYOUT_START();
	
	# Power Voice
	if ($plugin['power_voice'])
	{ 
	?>
		<script language="javascript" >
		function listenPVoice(fileName)
		{		
			newWindow("http://<?=$eclass40_httppath?>/src/tool/powervoice/pvoice_listen_now_intranet.php?from_intranet=1&location="+encodeURIComponent(fileName), 18);
		}	
		</script>
	<?php 
	}

	echo $lo->display2();
?>

<?
/*
################################################################ temp: "copy" function not apply in view page, but in index page
## Admin can copy this announcment as a new announcement
if($_SESSION['SSV_USER_ACCESS']["other-schoolNews"])
{ ?>
<script language="javascript">
<!--
function copy_this()
{
	opener.copy_news(<?=$AnnouncementID?>);
	self.close();
}
//-->
</script>
	<div class="edit_bottom_v30">
	<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($button_copy, "button", "javascript:copy_this()")?> 
	<p class="spacer"></p>
</div>

<? } 
*/?>


<script language="javascript" >
function Approval(announcementID, type){
	var path = "approval.php?announcementID="+announcementID+"&type="+type;
    var connectionObject = YAHOO.util.Connect.asyncRequest("GET", path, callback_approval);
}

var callback_approval = {
	success: function ( o )
    {
	    var result = o.responseText;
		if(result){
			self.close();
			window.opener.document.location.reload();
		}
	}
}
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>