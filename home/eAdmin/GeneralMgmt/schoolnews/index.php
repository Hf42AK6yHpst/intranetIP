<?php
# using: 

#######################################
#
#   Date:   2020-10-28 (Tommy)
#           fix: missing approvel and reject selection option
#
#   Date:   2020-03-05 (Ray)
#           add website code
#
#   Date:   2018-12-27 (Anna)
#           added access right for $sys_custom['DHL'] PIC , and only see announcement created by user self
#
#   Date:   2018-09-12 (Anna) [Case#A146339]
#           ADDED fieldorder2
#
#   Date:	2018-03-14 (Isaac) [DM#3390]
#           fixed $keyword for search sql, added intranet_htmlspecialchars() to $keyword.
#
# - 2017-01-17 (Isaac) 
#	improved: added EnglishName & ChineseName to keyword search's sql
#             added Get_Safe_Sql_Like_Query to keyword search's sql
#
# - 2016-07-14 (Ivan) [ip.2.5.7.7.1]
#	improved: added $sys_custom['eClassApp']['SFOC'] control to hide some columns and filtering options for this project
#
# - 2013-09-04 (YatWoon)
#	improved: update the icon title [Case#2013-0902-1051-49066]
#
# - 2011-03-10 (Henry Chow)
#	allow System Admin to view "School News"
#
#	Date:	2010-11-03	YatWoon
#			add copy function
#			update to IP25 UI standard
#
#######################################

	$PATH_WRT_ROOT = "../../../../";
	$CurrentPageArr['schoolNews'] = 1;
	
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."includes/libschoolnews.php");
	include_once($PATH_WRT_ROOT."includes/libdbtable.php");
	include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

	intranet_auth();
	intranet_opendb();
	
	if($sys_custom['DHL']){
	    include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
    	$libdhl = new libdhl();
    	$isPIC = $libdhl->isPIC();
	}
	
	if(!($_SESSION['SSV_USER_ACCESS']["other-schoolNews"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || ($isPIC && $sys_custom['DHL']))){
			include_once($PATH_WRT_ROOT."includes/libaccessright.php");
			$laccessright = new libaccessright();
			$laccessright->NO_ACCESS_RIGHT_REDIRECT();
			exit;
	}
	
	# Temp Assign memory of this page
	ini_set("memory_limit", "150M");

	# Create a new interface instance
	$linterface = new interface_html();

	# Create a new homework instance
	$lschoolnews = new libschoolnews();

	# Select current page
	$CurrentPage = "SchoolNews";

	# Page title
	$PAGE_TITLE = $Lang['SysMgr']['SchoolNews']['SchoolNews'];
	$TAGS_OBJ[] = array($Lang['SysMgr']['SchoolNews']['SchoolNews'], "");
	
	$MODULE_OBJ = $lschoolnews->GET_MODULE_OBJ_ARR();
	
	# Toolbar: new
	$toolbar = $linterface->GET_LNK_NEW("new.php", "", "", "", "", 0);

	# change page size
	if ($page_size_change == 1)
	{
		setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
		$ck_page_size = $numPerPage;
	}

	if (isset($ck_page_size) && $ck_page_size != "") 
		$page_size = $ck_page_size;

	$pageSizeChangeEnabled = true;
	
	# Select sort field
	$sortField = 0;
	
	# Table initialization
	$order = ($order == "") ? 0 : $order;
	$field = ($field == "") ? $sortField : $field;
	$pageNo = ($pageNo == "") ? 1 : $pageNo;

	# Create a new dbtable instance
	$li = new libdbtable2007($field, $order, $pageNo);
	
	# TABLE SQL
	$keyword = intranet_htmlspecialchars(trim($keyword));
	$keyword = addcslashes($keyword, '_');
	
	$statusArr[] = array("0", $i_status_pending);
	$statusArr[] = array("1", $i_status_publish);
	if (!$sys_custom['eClassApp']['SFOC'] && !$sys_custom['DHL'] && !($region_of_client == "zh_TW" || $sys_custom['PowerClass'])) {
		$statusArr[] = array("2", $Lang['SysMgr']['SchoolNews']['NoApproval']);
		$statusArr[] = array("3", $Lang['SysMgr']['SchoolNews']['Reject']);
	}
	
	# Filter
	$selectedStatus = $lschoolnews->getStatus("name=\"status\" onChange=\"reloadForm()\"", $statusArr, $status, $Lang['SysMgr']['SchoolNews']['AllStatus']);
		
	$user_field = getNameFieldWithClassNumberByLang("b.");
	
	
	# SQL statement
	if(sizeof($statusArr)!=0){
		$allStatus = " a.RecordStatus IN (";
		for ($i=0; $i < sizeof($statusArr); $i++)
		{	
			list($statusID)=$statusArr[$i];
			$allStatus .= $statusID.",";
		}
		$allStatus = substr($allStatus,0,strlen($allStatus)-1).")";
	}
	else{
		$allStatus ="";
	}
	
	$status = ($status == "") ? $allStatus : "a.RecordStatus = $status";
	$title = intranet_wordwrap("', a.Title,'", 20,"\n",1);
	
	if ($sys_custom['eClassApp']['SFOC']) {
		// hide View Results, Posted By, From Group, Target columns
	}
	else {
		$viewResultField = " CONCAT('<a href=\"javascript:showRead(',a.AnnouncementID,')\"><img src=\"$image_path/{$LAYOUT_SKIN}/eOffice/icon_resultview.gif\" border=\"0\" title=\"$i_Notice_ViewResult\"></a>'), ";
		$postedByField = " IF (a.UserID IS NOT NULL OR a.UserID != 0, $user_field,'$i_AnnouncementSystemAdmin'), ";
		//$fromGroupField = " IF (a.OwnerGroupID IS NOT NULL OR a.OwnerGroupID != 0, c.Title, '--'), ";
		
		if($sys_custom['DHL']){
			$fromGroupField = "";
			$targetField = " IF(d.GroupAnnouncementID IS NULL, '".$Lang['eSurvey']['public']."','". $Lang['SchoolNews']['SpecificGroup'] ."') as target_group, ";			
		}else{
			$fromGroupField = " IF (a.OwnerGroupID IS NOT NULL OR a.OwnerGroupID != 0, c.Title, '--'), ";
			$targetField = " IF(d.GroupAnnouncementID IS NULL, '$i_AnnouncementWholeSchool','". $Lang['SchoolNews']['SpecificGroup'] ."') as target_group, ";
		}
	}

	$websiteCodeField = '';
	if($sys_custom['SchoolNews']['ShowWebsiteCode']) {
		$websiteCodeField = "a.WebsiteCode,";
	}

	$fields = "DATE_FORMAT(a.AnnouncementDate, '%Y-%m-%d'), 
			   DATE_FORMAT(a.EndDate, '%Y-%m-%d'),
			   $websiteCodeField
			   CONCAT('<a class=\"tablelink\" href=\"javascript:viewNews(',a.AnnouncementID,')\">$title</a>'),
			   $viewResultField
			   $postedByField
			   $fromGroupField
			   $targetField
			   a.DateModified, 
			   CASE a.RecordStatus 
			   WHEN 0 THEN '$i_status_pending' 
			   WHEN 1 THEN'$i_status_publish' 
			   WHEN 2 THEN '".$Lang['SysMgr']['SchoolNews']['NoApproval']."'
			   WHEN 3 THEN '".$Lang['SysMgr']['SchoolNews']['Reject']."' END,
			   CONCAT('<input type=\"checkbox\" name=\"AnnouncementID[]\" value=\"', a.AnnouncementID ,'\" onClick=\"unset_checkall(this, this.form);\">')";

	$dbtables = "INTRANET_ANNOUNCEMENT AS a 
				LEFT JOIN INTRANET_USER AS b ON (b.UserID = a.UserID)
				LEFT JOIN INTRANET_GROUP AS c ON (c.GroupID = a.OwnerGroupID)
				LEFT JOIN INTRANET_GROUPANNOUNCEMENT AS d on (d.AnnouncementID=a.AnnouncementID)
				 ";
	$conds .= " ((a.Title like '%".$lschoolnews->Get_Safe_Sql_Query($keyword)."%') OR (b.EnglishName like '%".$lschoolnews->Get_Safe_Sql_Query($keyword)."%') OR
				(b.ChineseName like '%".$lschoolnews->Get_Safe_Sql_Query($keyword)."%')) AND $status";
	if ($_SESSION['SSV_USER_ACCESS']["other-schoolNews"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]){	    
	    
	}else {
	    $conds .= " AND a.UserID = '".$_SESSION['UserID']."'";
	}
	
	$groupby = " group by a.AnnouncementID ";
	$sql = "SELECT $fields FROM $dbtables WHERE $conds $groupby";
		
	# TABLE INFO
	$li->field_array = array("a.AnnouncementDate","a.EndDate", "a.Title", "$user_field", "c.Title","a.DateModified", "a.RecordStatus", "target_group","WebsiteCode");
	$li->fieldorder2 = ",a.AnnouncementDate DESC, a.AnnouncementID DESC";
	
	$li->sql = $sql;
	$li->no_col = sizeof($li->field_array)+3;
	if ($sys_custom['eClassApp']['SFOC']) {
		$li->no_col = $li->no_col - 4;
	}
	if($sys_custom['DHL']){
		// hide "from group" column
		$li->no_col = $li->no_col - 1;
	}

    if(!$sys_custom['SchoolNews']['ShowWebsiteCode']) {
        $li->no_col = $li->no_col - 1;
    }

	//$li->column_array = array(0,0,5,0,0,0);
	//$li->wrap_array = array(0,0,20,0,0,0);
//	$li->IsColOff = 2;
	$li->IsColOff = "IP25_table";	

	// TABLE COLUMN
	$li->column_list .= "<th class='num_check'>#</td>\n";
	$li->column_list .= "<th>".$li->column(0, $i_AnnouncementDate)."</th>\n";
	$li->column_list .= "<th>".$li->column(1, $i_AnnouncementEndDate)."</th>\n";
    if($sys_custom['SchoolNews']['ShowWebsiteCode']) {
		$li->column_list .= "<th>".$li->column(8, $Lang['SysMgr']['SchoolNews']['WebsiteCode'])."</th>\n";
    }
	$li->column_list .= "<th>".$li->column(2, $i_AnnouncementTitle)."</th>\n";
	if (!$sys_custom['eClassApp']['SFOC']) {
		$li->column_list .= "<th>". $Lang['eSurvey']['ViewResults'] ."</th>\n";
		$li->column_list .= "<th>".$li->column(3, $i_AnnouncementOwner)."</th>\n";
		if($sys_custom['DHL']){
			// hide "from group" column
		}
		else {
			$li->column_list .= "<th>".$li->column(4, $i_AnnouncementByGroup)."</th>\n";
		}
		$li->column_list .= "<th>".$li->column(7, $Lang['SchoolNews']['target'])."</th>\n";
	}
	$li->column_list .= "<th>".$li->column(5, $i_AnnouncementDateModified)."</th>\n";
	$li->column_list .= "<th>".$li->column(6, $i_AnnouncementRecordStatus)."</th>\n";
	$li->column_list .= "<th>".$li->check("AnnouncementID[]")."</th>\n";

	# Start layout
	$linterface->LAYOUT_START($xmsg);
	
$searchTag = "<input name=\"keyword\" id=\"keyword\" type=\"text\" value=\"". stripslashes($keyword) ."\" onkeyup=\"Check_Go_Search(event);\"/>";

?>
<SCRIPT LANGUAGE=Javascript>

function removeCat(obj,element,page){
		var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(alertConfirmRemove)){
                obj.action=page;
                obj.method="post";
                obj.submit();
                }
        }
	}
	
	function reloadForm() {
		document.form1.keyword.value = "";
		document.form1.pageNo.value = 1;
		document.form1.submit();
	}

	function viewNews(id)
	{
			 newWindow('view.php?AnnouncementID='+id,1);
	}

	function showRead(id)
	{
			 newWindow('read.php?AnnouncementID='+id,6);
	}
	
	function copy_news(id)
	{
		document.form1.action="copy.php";
		document.form1.copy_from.value = id;
		document.form1.submit();
	}
	
	function Check_Go_Search(evt)
	{
		var key = evt.which || evt.charCode || evt.keyCode;
		
		if (key == 13) // enter
		{
			document.form1.submit();
		}
		else
			return false;
	}
</SCRIPT>

<form name="form1" method="post" action="">

<div class="content_top_tool">
	<?=$toolbar?>
	<div class="Conntent_search"><?=$searchTag?></div>
	<br style="clear:both" />
</div>

<div class="table_board">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr class="table-action-bar">
		<td valign="bottom">
			<!-- Status //-->
			<div class="table_filter">
				<?=$selectedStatus?>
			</div>
		</td>
		
		<td valign="bottom">
			<!-- Tool //-->
			<div class="common_table_tool">
				<a href="javascript:checkEdit(document.form1,'AnnouncementID[]','copy.php')" class="tool_copy"><?=$button_copy?></a>
				<a href="javascript:checkEdit(document.form1,'AnnouncementID[]','edit.php')" class="tool_edit"><?=$button_edit?></a>
				<a href="javascript:removeCat(document.form1,'AnnouncementID[]','remove_update.php')" class="tool_delete"><?=$button_delete?></a>
			</div>
		</td>
	</tr>
	</table>

<?=$li->display()?>

</div>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>"/>
<input type="hidden" name="order" value="<?php echo $li->order; ?>"/>
<input type="hidden" name="field" value="<?php echo $li->field; ?>"/>
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>"/>
<input type="hidden" name="page_size_change" value=""/>
<input type="hidden" name="cid" value="<?=$childrenID?>"/>
<input type="hidden" name="sid" value="<?=$subjectID?>"/>
</form>
			
<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>