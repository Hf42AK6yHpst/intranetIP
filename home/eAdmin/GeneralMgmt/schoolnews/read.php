<?php
// Editing by 
############ Change Log [Start] ############
#   2018-12-27 Anna
#   added access right for $sys_custom['DHL'] PIC
# - 2017-04-25 (Carlos) $sys_custom['DHL'] - Display Department, hide Role for DHL.
# - 2017-04-03 (Carlos) fix display of identity.
# - 2011-03-10 (Henry Chow)
#	allow System Admin to view "School News"
#
############# Change Log [End] #############
$PATH_WRT_ROOT = "../../../../";
$CurrentPageArr['schoolNews'] = 1;

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libannounce.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfamily.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if($sys_custom['DHL']){
    include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
    $libdhl = new libdhl();
    $isPIC = $libdhl->isPIC();
}

if(!($_SESSION['SSV_USER_ACCESS']["other-schoolNews"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] ||  ($isPIC && $sys_custom['DHL']))){
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
}


if($sys_custom['DHL']){
	include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
	$libdhl = new libdhl();	
}

$lo = new libannounce($AnnouncementID);

$read = $lo->returnReadCount();
$total = $lo->returnTargetCount();
$unread = $total-$read;

if ($read != 0){
	$read = "<a class='tablelink' href='read.php?AnnouncementID=$AnnouncementID&type=1'>$read";
	$read .= " <img src='{$image_path}/{$LAYOUT_SKIN}/icon_view.gif' title='$i_AnnouncementViewReadList' border='0'></a>";
}

if ($unread != 0){
	$unread = "<a class='tablelink' href='read.php?AnnouncementID=$AnnouncementID&type=2'>$unread";
	$unread .= " <img src='{$image_path}/{$LAYOUT_SKIN}/icon_view.gif' title='$i_AnnouncementViewUnreadList' border='0'></a>";
}

$MODULE_OBJ['title'] = $Lang['SysMgr']['SchoolNews']['ViewReadStatus'];
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();
?>

<script language="JavaScript">
	function showChildren(id){
		var div = document.getElementById(id);
		if(div.value==0){
			div.style.display = "block";
			div.value=1;
		}
		else{
			div.style.display = "none";
			div.value=0;
		}
	}
</script>

<br />
<form name="form1" method="post" enctype="multipart/form-data">
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td colspan="2">
				<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr> 
						<td>
							<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
								<!-- Title-->
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['SchoolNews']['Title']?></td>
									<td width="567" align="left" valign="top">
										<?=$lo->Title?>
									</td>
								</tr>
								
								<!-- Start Date-->
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['SchoolNews']['StartDate']?></td>
									<td width="567" align="left" valign="top">
										<?=$lo->AnnouncementDate?>
									</td>
								</tr>
								
								<!-- End Date-->
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['SchoolNews']['EndDate']?></td>
									<td width="567" align="left" valign="top">
										<?=$lo->EndDate?>
									</td>
								</tr>
								
								<!-- Read-->
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['SchoolNews']['Read']?></td>
									<td width="567" align="left" valign="top">
										<?=$read?>
									</td>
								</tr>
								
								<!-- Unread -->
								<tr>
									<td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['SysMgr']['SchoolNews']['Unread']?></td>
									<td width="567" align="left" valign="top">
									  <?=$unread?>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<? 
		if($type){ 
			$otype = $type;
			if ($type == 1)
			{
				$list = $lo->returnReadList();
			}
			else
			{
				$list = $lo->returnUnreadList();
			}
			
			if($sys_custom['DHL']){
				$userIdAry = Get_Array_By_Key($list,0);
				$userRecords = $libdhl->getUserRecords(array('user_id'=>$userIdAry,'GetOrganizationInfo'=>1));
				$userIdToRecordMap = array();
				for($i=0;$i<count($userRecords);$i++){
					$userIdToRecordMap[$userRecords[$i]['UserID']] = $userRecords[$i];
				}
			}

			$x = "<table width='100%' border='0' cellpadding='2' cellspacing='0'>\n";
			$x .= "<tr>";
			$x .= "<td width='20%' class='tablebluetop tabletopnolink'>#</td>";
			$x .= "<td width='50%' class='tablebluetop tabletopnolink'>".$Lang['SysMgr']['SchoolNews']['Name']."</td>";
			if($sys_custom['DHL']){
				$x .= "<td width='6.33%' class='tablebluetop tabletopnolink'>".($Lang['DHL']['Company'])."</td>";
				$x .= "<td width='6.33%' class='tablebluetop tabletopnolink'>".($Lang['DHL']['Division'])."</td>";
				$x .= "<td width='6.33%' class='tablebluetop tabletopnolink'>".($Lang['DHL']['Department'])."</td>";
			}else{
				$x .= "<td width='20%' class='tablebluetop tabletopnolink'>".($Lang['SysMgr']['SchoolNews']['Role'])."</td>";
			}
			$x .= "</tr>";
				
			for ($i=0; $i<sizeof($list); $i++){
				list($id, $name, $type) = $list[$i];

				if($type==3){
					$lfamily = new libfamily();
					//echo ;
					//$tooltipQ = makeTooltip($lfamily->displayChildrenList2007a($id));
					$name = "<a href='javascript:showChildren($id)' class='tablebluelink'>$name <img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_ppl_ex_group.gif' title='$i_general_show_child' border='0'></a>";
					$x .= "<tr class='tablebluerow". ( $i%2 + 1 )."'>\n";
					$x .= "<td class='tabletext'>".($i+1)."</td>";
					$x .= "<td class='tabletext'>$name<br></td>";
					if($sys_custom['DHL']){
						$x .= "<td class='tabletext'>".($userIdToRecordMap[$id]['CompanyCode'])."</td>";
						$x .= "<td class='tabletext'>".($userIdToRecordMap[$id]['DivisionCode'])."</td>";
						$x .= "<td class='tabletext'>".($userIdToRecordMap[$id]['DepartmentCode'])."</td>";
					}else{
						$x .= "<td class='tabletext'>". ($i_identity_array[$type])."</td>";
					}
					$x .= "</tr>";
					$x .= "<tr><td></td>
								<td colspan=\"2\">
								<div id=\"$id\" value=\"0\" style=\"display:none;\">".$lfamily->displayChildrenList2007a($id)."	
								<div>
							</td></tr>";
				}
				else{
					$x .= "<tr class='tablebluerow". ( $i%2 + 1 )."'>\n";
					$x .= "<td class='tabletext'>".($i+1)."</td>";
					$x .= "<td class='tabletext'>$name </td>";
					if($sys_custom['DHL']){
						$x .= "<td class='tabletext'>".($userIdToRecordMap[$id]['CompanyCode'])."</td>";
						$x .= "<td class='tabletext'>".($userIdToRecordMap[$id]['DivisionCode'])."</td>";
						$x .= "<td class='tabletext'>".($userIdToRecordMap[$id]['DepartmentCode'])."</td>";
					}else{
						$x .= "<td class='tabletext'>".($i_identity_array[$type])."</td>";
					}
					$x .= "</tr>";
				}
				
				
			}
			$x .= "</table>\n";

		?>
		<tr>
			<td colspan="2" align="center">
				<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr> 
						<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
					</tr>
				</table>
			</td>
		</tr>

		<tr>
			<td colspan="2">
				<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr> 
						<td><br />
							<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
								<tr valign="top">
									<td><?= $linterface->GET_NAVIGATION2(($otype==1)? $Lang['SysMgr']['SchoolNews']['ReadList'] : $Lang['SysMgr']['SchoolNews']['UnreadList'] ) ?></td>
								</tr>
								<tr>
									<td><?=$x?></td>
								</tr>		
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>        
		<? } ?>
	
		<tr>
			<td colspan="2">        
				<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tr>
						<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
					</tr>
					<tr>
						<td align="center">
							<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();", "close_btn") ?>
						</td>
					</tr>
				</table>                                
			</td>
		</tr>
	</table>
</form>
<?php
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>