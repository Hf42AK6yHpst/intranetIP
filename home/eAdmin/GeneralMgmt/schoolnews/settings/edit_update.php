<?php
# using: 

################# Change Log [Start] ############
#-	Date	2016-09-23(Villa)
#			add to module setting
#	
# - 2011-03-10 (Henry Chow)
#	allow System Admin to view "School News"
#
################# Change Log [End] ############

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if(!($_SESSION['SSV_USER_ACCESS']["other-schoolNews"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])){
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libschoolnews.php");

intranet_auth();
intranet_opendb();

$lschoolnews = new libschoolnews();
$lgeneralsettings = new libgeneralsettings();

$allowUserToViewPastNews = IntegerSafe($_POST['allowUserToViewPastNews']);

$data = array();
$data['defaultNumDays'] = $defaultNumDays;
$data['allowUserToViewPastNews'] = $allowUserToViewPastNews? 1 : 0;
$data['sendPushMsg'] = $sendPushMsg? 1 : 0;

# store in DB
$lgeneralsettings->Save_General_Setting($lschoolnews->ModuleName, $data);

# set $this->
$lschoolnews->defaultNumDays = $defaultNumDays;		
$lschoolnews->allowUserToViewPastNews = $allowUserToViewPastNews;
$lschoolnews->sendPushMsg = $sendPushMsg;

# update session
foreach($data as $name=>$val)
	$_SESSION["SSV_PRIVILEGE"]["SchoolNews"][$name] = $val;
	
intranet_closedb();
header("Location: index.php?xmsg=".$Lang['eNotice']['SettingsUpdateSuccess']);
?>