<?php
// editing by 

$PATH_WRT_ROOT="../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;


intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lp = new libpayment();
$linterface = new interface_html();
$lclass = new libclass();
$ldb 	= new libdb();
$lsms 	= new libsmsv2();
$lf 	= new libfilesystem();

$EmailSubject = stripslashes($EmailSubject);
$Message = stripslashes($Message);

## Handle attachments
$epayment_path = "$file_path/file/ePayment";
if (!is_dir($epayment_path))
{
    $lf->folder_new($epayment_path);
}
// Clear old tmp folders
$composeTime = time();
$composeBufferTime = 24*60*60;
$tmpComposeFolders = $lf->return_folderlist($epayment_path);
$tmpFoldersToRemove = array();
clearstatcache();
for($i=0;$i<sizeof($tmpComposeFolders);$i++){
	$file_modified_time = filectime($tmpComposeFolders[$i]);
	// ready to be removed tmp dir should fulfil these conditions
	// 1). it is a directory; 2). end with tmp; 3). dir has been there longer than buffer time 
	if(is_dir($tmpComposeFolders[$i]) && file_exists($tmpComposeFolders[$i]) && substr($tmpComposeFolders[$i],-3)=="tmp" && ($composeTime-$file_modified_time)>$composeBufferTime){
		if($lf->deleteDirectory($tmpComposeFolders[$i]))
			$tmpFoldersToRemove[] = $tmpComposeFolders[$i];
	}
}
// end of clear old tmp folders

$composeFolder = md5(session_id().".".time()).".tmp";
$tmp_path = $epayment_path."/$composeFolder";
$lf->folder_new($tmp_path);

$attachment_list = array();
for($i=0;$i<sizeof($_FILES['Attachment']['name']);++$i)
{
	$file_name = $_FILES['Attachment']['name'][$i];
	$file_type = $_FILES['Attachment']['type'][$i];
	$file_tmp_name = $_FILES['Attachment']['tmp_name'][$i];
	$file_error = $_FILES['Attachment']['error'][$i];
	$file_size = $_FILES['Attachment']['size'][$i];
	
	if($file_error > 0) continue; // error occurs 
	
	$dest_path = $tmp_path."/".$file_name;
	$copy_success = $lf->file_copy($file_tmp_name, $dest_path);
	$attachment_list[] = array(
							'name' => $file_name,
							'type' => $file_type,
							'path' => $dest_path,
							'size' => $file_size
						);
}
$AttachmentPath = sizeof($attachment_list)>0?$tmp_path:'';
## End handle attachments

## Find Cc to module admin info and class teacher info
$CcToClassTeacher = false;
$admin_info_ary = array();
$class_teacher_ary = array();
$class_teacher_info_ary = array();
$temp_class_teacher_ary = array();
if(sizeof($CcTo)>0)
{
	if(in_array("1",$CcTo)){ // admin users
		//$admin_ary = (array)$lpayment->GET_ADMIN_USER();
		$NameField = getNameFieldByLang2("u.");
		$sql = "SELECT 
					DISTINCT u.UserID, u.UserLogin, $NameField as UserName 
				FROM 
					ROLE_MEMBER as m 
					INNER JOIN ROLE_RIGHT as r ON r.RoleID = m.RoleID AND r.FunctionName = 'eAdmin-ePayment'
					INNER JOIN INTRANET_USER as u ON m.UserID = u.UserID AND u.RecordStatus = 1 
				ORDER BY 
					UserName ";
		$admin_info_ary = $lclass->returnArray($sql);
	}
	if(in_array("2",$CcTo)){ // class teachers
		$CcToClassTeacher = true;
	}
}

## Student info
$stu_ary = array();
for($i=0;$i<sizeof($ClassID);$i++)
{
	$this_class = $ClassID[$i];
	$stu_ary = array_merge($stu_ary, $lclass->getClassStudentList($this_class));
	if($CcToClassTeacher)
		$temp_class_teacher_ary = array_merge($temp_class_teacher_ary, $lclass->returnClassTeacherID($this_class));
}
$stu_ary = array_unique ($stu_ary);

## Check balance less than
if($check_bal){
	$final_stu_ary = array();
	for($i=0;$i<sizeof($stu_ary);$i++){
		$this_bal = $lp->checkBalance($stu_ary[$i]);
		if($this_bal[0] >= $bal_less_than) continue;
		$final_stu_ary[] = $stu_ary[$i];
	}
}else{
	$final_stu_ary = $stu_ary;
}

$stu_str = implode(",",$final_stu_ary);

if($CcToClassTeacher){
	for($i=0;$i<sizeof($temp_class_teacher_ary);$i++)
	{
		$class_teacher_ary[] = $temp_class_teacher_ary[$i]['UserID'];
	}
	if(sizeof($class_teacher_ary)>0){
		$NameField = getNameFieldByLang2("u.");
		$sql = "SELECT 
					u.UserID, u.UserLogin, $NameField as UserName 
				FROM INTRANET_USER as u 
				WHERE u.UserID IN (".implode(",",$class_teacher_ary).") 
				ORDER BY UserName ";
		$class_teacher_info_ary = $lclass->returnArray($sql);
	}
}

## Parent info
$name_field = getNameFieldByLang("b.");
$name_field2 = getNameFieldByLang("c.");
$sql = "SELECT 
        	 distinct(a.ParentID),
        	 $name_field AS ParentName, 
			 a.StudentID, 
			 $name_field2 AS StudentName,
			 c.ClassName, 
			 c.ClassNumber,
			 CONCAT(c.ClassName, LPAD(c.ClassNumber,5,'0')) as ClassField  
         FROM 
         	INTRANET_PARENTRELATION as a 
         	inner join INTRANET_USER as b on (b.UserID=a.ParentID)
			inner join INTRANET_USER as c on (c.UserID=a.StudentID)
         WHERE 
         	a.StudentID in ($stu_str) 
		 ORDER BY 
			ClassField, StudentName, ParentName ";
$parent_ary = $lclass->returnArray($sql);



$to_table = "<table width=\"90%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
$to_table.= "<tr><td>".$linterface->GET_NAVIGATION2($Lang['ePayment']['EmailSentToRecipient'])."</td></tr></table>\n";
$to_table.= "<table width=\"90%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
$to_table.= "<tr class='tabletop tabletoplink'>\n
				<td width='1'>#</td>\n
				<td width='30%'>".$Lang['Identity']['Parent']."</td>\n
				<td width='30%'>".$Lang['Identity']['Student']."</td>\n
				<td width='20%'>$i_ClassName</td>\n
				<td width='20%'>$i_ClassNumber</td>\n
			  </tr>\n";

$parent_ary_size = sizeof($parent_ary);
$parent_ary_no = 0;
for($i=0;$i<$parent_ary_size;$i++)
{
	if(trim($parent_ary[$i]['StudentID'])=="") continue;
	$parent_ary_no++;
	$css = ($i%2) ? "" : "2";
	$to_table .= "<tr>";
	$to_table .= "<td class=tableContent$css>".$parent_ary_no."</td>";
	$to_table .= "<td class=tableContent$css>".$parent_ary[$i]['ParentName']."</td>";
	$to_table .= "<td class=tableContent$css>".$parent_ary[$i]['StudentName']."</td>";
	$to_table .= "<td class=tableContent$css>".$parent_ary[$i]['ClassName']."</td>";
	$to_table .= "<td class=tableContent$css>".$parent_ary[$i]['ClassNumber']."</td>";
	$to_table .= "</tr>\n";
}
$to_table .= "</table>\n";

$cc_admin_table = "<table width=\"90%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
$cc_admin_table.= "<tr><td>".$linterface->GET_NAVIGATION2($Lang['ePayment']['EmailCcToAdmin'])."</td></tr></table>\n";
$cc_admin_table.= "<table width=\"90%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
$cc_admin_table.= "<tr class='tabletop tabletoplink'>\n
						<td width='1'>#</td>\n
						<td width='50%'>".$i_UserLogin."</td>\n
						<td width='50%'>".$i_UserName."</td>\n
					</tr>\n";

$admin_info_ary_size = sizeof($admin_info_ary);
$admin_info_ary_no = 0;
for($i=0;$i<$admin_info_ary_size;$i++)
{
	$admin_info_ary_no++;
	$css = ($i%2) ? "" : "2";
	$cc_admin_table .= "<tr>";
	$cc_admin_table .= "<td class=tableContent$css>".$admin_info_ary_no."</td>";
	$cc_admin_table .= "<td class=tableContent$css>".$admin_info_ary[$i]['UserLogin']."</td>";
	$cc_admin_table .= "<td class=tableContent$css>".$admin_info_ary[$i]['UserName']."</td>";
	$cc_admin_table .= "</tr>\n";
}
$cc_admin_table .= "</table>\n";

$cc_teacher_table = "<table width=\"90%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
$cc_teacher_table.= "<tr><td>".$linterface->GET_NAVIGATION2($Lang['ePayment']['EmailCcToClassTeacher'])."</td></tr></table>\n";
$cc_teacher_table.= "<table width=\"90%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">\n";
$cc_teacher_table.= "<tr class='tabletop tabletoplink'>\n
						<td width='1'>#</td>\n
						<td width='50%'>".$i_UserLogin."</td>\n
						<td width='50%'>".$i_UserName."</td>\n
					</tr>\n";

$class_teacher_info_ary_size = sizeof($class_teacher_info_ary);
$class_teacher_info_ary_no = 0;
for($i=0;$i<$class_teacher_info_ary_size;$i++)
{
	$class_teacher_info_ary_no++;
	$css = ($i%2) ? "" : "2";
	$cc_teacher_table .= "<tr>";
	$cc_teacher_table .= "<td class=tableContent$css>".$class_teacher_info_ary_no."</td>";
	$cc_teacher_table .= "<td class=tableContent$css>".$class_teacher_info_ary[$i]['UserLogin']."</td>";
	$cc_teacher_table .= "<td class=tableContent$css>".$class_teacher_info_ary[$i]['UserName']."</td>";
	$cc_teacher_table .= "</tr>\n";
}
$cc_teacher_table .= "</table>\n";


$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "SmsNotification";
$TAGS_OBJ[] = array($i_SMS_Notification, "", 0);
$MODULE_OBJ = $lp->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

?>
<BR>
<form name="form1" method="post" action="send_email.php" >

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td colspan='2'>
    		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
    			<tr>
    				<td valign="top" nowrap="nowrap" class="tabletext">
	    				<?=$Lang['ePayment']['EmailSubject']?>
	    			</td>
	    			<td class="tabletext" width="70%"><?=$EmailSubject?><BR>
	    			</td>
    			</tr>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="tabletext">
	    				<?=$Lang['ePayment']['EmailContent']?>
	    			</td>
	    			<td class="tabletext" width="70%"><?=$Message?><BR>
	    			</td>
    			</tr>
    			<?php if($check_bal){?>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_SMS_Balance_Lass_Than?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    			$ <?=$bal_less_than?>
	    			</td>
    			</tr>
    			<?php } ?>
			</table>
		</td>
	</tr>
    <tr>
    	<td colspan="2">
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
					<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;</td>
				</tr>
		    	<tr>
                	<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
	<tr>
		<td colspan='2'>
		<?=$to_table?>
		</td>
	</tr>
	<?php if($admin_info_ary_no>0){ ?>
	<tr>
		<td colspan='2'>
		<?=$cc_admin_table?>
		</td>
	</tr>
	<?php } ?>
	<?php if($class_teacher_info_ary_no>0){ ?>
	<tr>
		<td colspan='2'>
		<?=$cc_teacher_table?>
		</td>
	</tr>
	<?php } ?>
	<?php if($parent_ary_no==0){ ?>
	<tr>
		<td align="center" colspan="2">
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
	<?php }else{ ?>
    <tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_send, "submit", "javascript:document.form1.submit();") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
	<?php } ?>
</table>
<br />

<?php 
for($i=0;$i<sizeof($ClassID);$i++){
	echo "<input type=\"hidden\" name=\"ClassID[]\" value=\"".$ClassID[$i]."\" />\n";
}
for($i=0;$i<sizeof($CcTo);$i++){
	echo "<input type=\"hidden\" name=\"CcTo[]\" value=\"".$CcTo[$i]."\" />\n";
}
?>
<input type="hidden" name="Message" value="<?=htmlspecialchars($Message,ENT_QUOTES)?>" />
<input type="hidden" name="EmailSubject" value="<?=htmlspecialchars($EmailSubject,ENT_QUOTES)?>" />
<input type="hidden" name="AttachmentPath" value="<?=$AttachmentPath?>" />
<input type="hidden" name="check_bal" value="<?=$check_bal?>" />
<input type="hidden" name="bal_less_than"  value="<?=$bal_less_than?>" />
</form>
<br />

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>