<?php
// editing by 
/*************************************************** Change log **********************************************
 * 2011-11-07 (Carlos): Modified CC to student's own class teacher
 *************************************************************************************************************/
$PATH_WRT_ROOT="../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lp = new libpayment();
$lclass     = new libclass();
$ldbsms 	= new libsmsv2();
$lf 		= new libfilesystem();
$lwebmail 	= new libwebmail();

/*
## Handle attachments
$epayment_path = "$file_path/file/ePayment";
if (!is_dir($epayment_path))
{
    $lf->folder_new($epayment_path);
}
$composeFolder = md5(session_id().".".time());
$tmp_path = $epayment_path."/$composeFolder";
$lf->folder_new($tmp_path);

$attachment_list = array();
for($i=0;$i<sizeof($_FILES['Attachment']['name']);++$i)
{
	$file_name = $_FILES['Attachment']['name'][$i];
	$file_type = $_FILES['Attachment']['type'][$i];
	$file_tmp_name = $_FILES['Attachment']['tmp_name'][$i];
	$file_error = $_FILES['Attachment']['error'][$i];
	$file_size = $_FILES['Attachment']['size'][$i];
	
	if($file_error > 0) continue; // error occurs 
	
	$dest_path = $tmp_path."/".$file_name;
	$copy_success = $lf->file_copy($file_tmp_name, $dest_path);
	$attachment_list[] = array(
							'name' => $file_name,
							'type' => $file_type,
							'path' => $dest_path,
							'size' => $file_size
						);
}
$AttachmentPath = sizeof($attachment_list)>0?$tmp_path:'';
## End handle attachments
*/
## Find Cc to module admin info and class teacher info
$CcToClassTeacher = false;
$admin_ary = array();
$class_teacher_ary = array();
$temp_class_teacher_ary = array();
if(sizeof($CcTo)>0)
{
	if(in_array("1",$CcTo)){ // admin users
		//$admin_ary = (array)$lpayment->GET_ADMIN_USER();
		$sql = "SELECT 
					DISTINCT m.UserID
				FROM 
					ROLE_MEMBER as m 
					INNER JOIN ROLE_RIGHT as r ON r.RoleID = m.RoleID AND r.FunctionName = 'eAdmin-ePayment'
					INNER JOIN INTRANET_USER as u ON m.UserID = u.UserID AND u.RecordStatus = 1 ";
		$admin_ary = $lclass->returnVector($sql);
		
	}
	if(in_array("2",$CcTo)){ // class teachers
		$CcToClassTeacher = true;
	}
}

## Student info
$stu_ary = array();
$ClassNameToTeacherIDArr = array();
for($i=0;$i<sizeof($ClassID);$i++)
{
	$this_class = $ClassID[$i];
	$tmp_student_list = $lclass->getClassStudentList($this_class);
	$stu_ary = array_merge($stu_ary, $tmp_student_list);
	if($CcToClassTeacher){
		$tmp_class_teacher_list = array();
		$tmp_class_teacher_id = $lclass->returnClassTeacherID($this_class);
		for($j=0;$j<count($tmp_class_teacher_id);$j++){
			$tmp_class_teacher_list[] = $tmp_class_teacher_id[$j]['UserID'];
		}
		$ClassNameToTeacherIDArr[$this_class] = $tmp_class_teacher_list;
		$temp_class_teacher_ary = array_merge($temp_class_teacher_ary, $tmp_class_teacher_id);
	}
}
$stu_ary = array_unique ($stu_ary);

## Check balance less than
if($check_bal){
	$final_stu_ary = array();
	for($i=0;$i<sizeof($stu_ary);$i++){
		$this_bal = $lp->checkBalance($stu_ary[$i]);
		if($this_bal[0] >= $bal_less_than) continue;
		$final_stu_ary[] = $stu_ary[$i];
	}
}else{
	$final_stu_ary = $stu_ary;
}

$stu_str = implode(",",$final_stu_ary);

if($CcToClassTeacher){
	for($i=0;$i<sizeof($temp_class_teacher_ary);$i++)
	{
		$class_teacher_ary[] = $temp_class_teacher_ary[$i]['UserID'];
	}
}

## Parent info
$name_field = getNameFieldWithClassNumberByLang("b.");
$name_field2 = getNameFieldByLang("c.");
$sql = "SELECT 
        	 distinct(a.ParentID),
        	 $name_field AS OutputName, 
			 a.StudentID, 
			 $name_field2 AS StudentName,
			 c.ClassName, 
			 c.ClassNumber 
         FROM 
         	INTRANET_PARENTRELATION as a 
         	inner join INTRANET_USER as b on (b.UserID=a.ParentID)
			inner join INTRANET_USER as c on (c.UserID=a.StudentID)
         WHERE 
         	a.StudentID in ($stu_str)
		ORDER BY 
			StudentName, OutputName ";
$parent_ary = $lclass->returnArray($sql);


$MsgTagArr = $ldbsms->MsgTagAry();
$MailInOneBatch = true;
for($i=0;$i<sizeof($MsgTagArr);$i++){
	$this_word = '($'.$MsgTagArr[$i][0].')';
	if(strstr($Message, $this_word)!=false){
		$MailInOneBatch = false;
		break;
	}
}

// To Recipient UserID array 
$ToArray = array();
// CC Recipient UserID array 
//$CcArray = array();
//if(sizeof($admin_ary)>0) $CcArray = array_merge($CcArray,$admin_ary);
//if(sizeof($class_teacher_ary)>0) $CcArray = array_merge($CcArray,$class_teacher_ary);
//$CcArray = array_unique($CcArray);

$send_success = array();
if($MailInOneBatch)
{
	// changed to send to parent one by one
	for($i=0;$i<sizeof($parent_ary);$i++)
	{
		list($parent_id,$parent_name,$student_id,$student_name,$class_name,$class_number) = $parent_ary[$i];
		if($student_id=="")	continue;
		$ToArray = array();
		$ToArray[] = $parent_id;
		$ToArray = array_unique($ToArray);
		
		$CcArray = array();
		if(sizeof($admin_ary)>0) $CcArray = array_merge($CcArray,$admin_ary);
		if(sizeof($ClassNameToTeacherIDArr[$class_name])>0) $CcArray = array_merge($CcArray,$ClassNameToTeacherIDArr[$class_name]);
		
		$send_success[] = $lwebmail->sendModuleMail2($ToArray,$CcArray,array(),$EmailSubject,$Message,0,$AttachmentPath);
	}
}else
{
	## Find payment balance and construct message for each student's parent 
	for($i=0;$i<sizeof($parent_ary);$i++)
	{
		list($parent_id,$parent_name,$student_id,$student_name,$class_name,$class_number) = $parent_ary[$i];
		if($student_id=="")	continue;
		
		# get msg content
		$this_msg = $ldbsms->replace_content($student_id, $Message, '');
		
		## SendMail
		$ToArray = array();
		$ToArray[] = $parent_id;
		$ToArray = array_unique($ToArray);
		
		$CcArray = array();
		if(sizeof($admin_ary)>0) $CcArray = array_merge($CcArray,$admin_ary);
		if(sizeof($ClassNameToTeacherIDArr[$class_name])>0) $CcArray = array_merge($CcArray,$ClassNameToTeacherIDArr[$class_name]);
		
		$send_success[] = $lwebmail->sendModuleMail2($ToArray,$CcArray,array(),$EmailSubject,$this_msg,0,$AttachmentPath);
	}
}

## Remove temp attachments
if(file_exists($AttachmentPath)) $lf->lfs_remove($AttachmentPath);

intranet_closedb();
if(!in_array(false,$send_success))
	$Msg = $Lang['ePayment']['ReturnMsg']['EmailSentSuccess'];
else
	$Msg = $Lang['ePayment']['ReturnMsg']['EmailSentFail'];
header("Location: index.php?Msg=".urlencode($Msg));
?>
