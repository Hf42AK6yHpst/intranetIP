<?php
// editing by 
/*********************************** Change log ************************************
 * 2019-09-23 (Carlos): Exclude Payment_StartDate and Payment_EndDate from auto fill items. Added remark about it. 
 * 2017-07-17 (Carlos): Modified use eClass App template standard to send push message for parent app.
 * 2017-07-12 (Anna): Add SMS_notificaction Remark
 * 2015-07-21 (Shan) : Add function goExportMobile(), and export button
 * 2015-1-7 (Roy): if sms disabled, hide sms option
 * 2014-10-10 (Roy): add option to send push message to parent as default if eClass app enabled
 * 2011-05-24 (Carlos): changed word "SMS Notification" to "Notification"
 * 2011-05-20 (Carlos): added email notification
 ***********************************************************************************/
$PATH_WRT_ROOT="../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_template.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();

if(!($plugin['sms'] and $bl_sms_version >= 2) && !$plugin['eClassApp'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$libAppTemplate = new libeClassApp_template();
$Module = "ePayment";
$libAppTemplate->canAccess($Module,$Section);
# Init libeClassApp_template
$libAppTemplate->setModule($Module);
$libAppTemplate->setSection($Section);
$libAppTemplate->setExcludedTags(array('Payment_Amount','Payment_Item','Inadequate_Amount','Payment_StartDate','Payment_EndDate'));


$linterface = new interface_html();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "SmsNotification";
$TAGS_OBJ[] = array($Lang['ePayment']['Notification'], "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($Msg));

$lclass = new libclass();
$sql = 'select 
					yc.ClassTitleEN, 
					y.YearName, 
					y.YearID 
				From 
					YEAR_CLASS as yc 
					inner join 
					YEAR as y 
					on yc.YearID = y.YearID and yc.AcademicYearID = \''.Get_Current_Academic_Year_ID().'\'';
/*$sql="SELECT 
				a.ClassName,
				b.LevelName,
				b.ClassLevelID 
			FROM 
				INTRANET_CLASS AS a 
				LEFT OUTER JOIN 
				INTRANET_CLASSLEVEL AS b 
				ON (a.ClassLevelID = b.ClassLevelID) 
			WHERE 
				a.RecordStatus=1 
			ORDER BY 
				b.ClassLevelID,a.ClassName ";*/
$temp = $lclass->returnArray($sql,3);

for($i=0;$i<sizeof($temp);$i++){
	list($name,$level,$levelid)=$temp[$i];
	if($levelid=="")
		$no_level[] = $name;
		
	else{
		$result[$levelid]['name']=$level;
		$result[$levelid]['class'][]=$name;
	}
}

$table = "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
$table.="<tr><td class='tabletop tabletoplink'><b>$i_ClassLevel</b></td><td class='tabletop tabletoplink'><B>$i_ClassName</b></td></tr>";
if(sizeof($result)>0){
	foreach($result as $key =>$values){
		$level_name = $values['name'];
		$classes = $values['class'];
		$table.="<tr><Td class='tabletext'><input type=checkbox name='LevelID[]' value='$key' onClick='selectByClassLevel(this)'> $level_name</td><td class='tabletext'>";
		for($i=0;$i<sizeof($classes);$i++){
			$class_name = $classes[$i];
			$table.="<input type=checkbox name='ClassID[]' value='$class_name' id='ClassID_".$key."_".$i."'> $class_name&nbsp;&nbsp;";
		}
		$table.="<input type=hidden name='$key' value='$i'>";
		$table.="</td></tr>";
	}
}
if(sizeof($no_level)>0){
	$table.="<tr><td class='tabletext'>&nbsp;</td><td>";
	for($i=0;$i<sizeof($no_level);$i++){
		$name = $no_level[$i];
		$table.="<input type=checkbox name='ClassID[]' value='$name'> $name";
	}
	$table.="</td></tr>";
}
$table.="</table>";

$lsms = new libsmsv2();
$TemplateCode = array("STUDENT_MAKE_PAYMENT","BALANCE_REMINDER");
$select_template = "<SELECT name=\"TemplateCode\" onChange=\"toggle();this.form.Message.value=this.value; this.form.Message.focus()\">\n";
$select_template .= "<OPTION value=''> -- $button_select $i_SMS_MessageTemplate -- </OPTION>\n";
for($i=0; $i<sizeof($TemplateCode); $i++)
{
	$content = $lsms->returnSystemMsg($TemplateCode[$i]);
	$status = $lsms->returnTemplateStatus("",$TemplateCode[$i]);
	if($content!="" and $status)
		$select_template .= "<OPTION value='". $lsms->returnSystemMsg($TemplateCode[$i]) ."'>". $lsms->returnTemplateName($TemplateCode[$i]) ."</OPTION>\n";
}
$select_template .= "</SELECT>\n";


if ($sent == 1)
{
   	$SysMsg = $linterface->GET_SYS_MSG("",$i_SMS_MessageSent);
}

 

?>
<? include_once($PATH_WRT_ROOT."home/eClassApp/common/pushMessageTemplate/js_AppMessageReminder.php") ?>
<script language='javascript'>
function toggle()
{
	with(document.form1)
	{
		if(TemplateCode.options[TemplateCode.selectedIndex].text == '<?=$lsms->returnTemplateName("BALANCE_REMINDER")?>')
			document.getElementById('bal_tr').style.display = "inline";
		else
			document.getElementById('bal_tr').style.display = "none";
	}
	
}
function selectByClassLevel(obj){
	classid = obj.value;
	check = obj.checked;
	objLevels = document.getElementsByName(classid);
	objLevel = objLevels[0];
	no_of_class = objLevel.value;
		
	for(i=0;i<no_of_class;i++){
		obj = document.getElementById('ClassID_'+classid+'_'+i);
		if(typeof(obj)!='undefined')
			obj.checked = check;
	}
	
}
function checkform(obj)
{
	class_flag = false;
	classes = document.getElementsByName('ClassID[]');
	for(i=0;i<classes.length;i++){
		if(classes[i].checked) class_flag = true;
	}
	if(!class_flag)	{ alert("<?=$i_StudentGuardian_warning_PleaseSelectClass?>"); return false;}
	
	if (!check_text(obj.Message,'<?=$i_SMS_AlertMessageContent?>')) return false;
	
	if(document.getElementById('bal_tr').style.display=="inline")
	{
		if (!check_text(obj.bal_less_than,'<?=$i_Homework_Error_Empty?>')) return false;
		if (!check_positive_int(obj.bal_less_than,'<?=$i_Payment_Warning_InvalidAmount?>')) return false;
		
		obj.check_bal.value=1;
	}
	
	if(document.getElementById('ByEmail').checked){
		if (!check_text(obj.EmailSubject,'<?=$Lang['ePayment']['WarningMsg']['RequestInputEmailSubject']?>')) return false;
		obj.action = 'mail_list.php';	
	}
	
	if(document.getElementById('ByPushMessage').checked){
		if (!check_text(obj.PushMessageTitle,'<?=$Lang['AppNotifyMessage']['WarningMsg']['RequestInputTitle']?>')) return false;
		obj.action = 'push_message_list.php';	
	}
}
function selectAll(obj){
	classes = document.getElementsByName('ClassID[]');
	levels = document.getElementsByName('LevelID[]');
	check = obj.checked;
	for(i=0;i<classes.length;i++){
		classes[i].checked = check;
	}
	for(i=0;i<levels.length;i++){
		levels[i].checked = check;
	}

	
}

function toggleMethod(value)
{
	if(value==0){ // by sms
		$('.sms_layer').show();
		$('.mail_layer').hide();
		$('.push_message_layer').hide();
		//$('input#btn_send').hide();
		//$('input#btn_search').show();
	} else if (value==1) { // by email
		$('.sms_layer').hide();
		$('.mail_layer').show();
		$('.push_message_layer').hide();
		//$('input#btn_search').hide();
		//$('input#btn_send').show();
	} else if (value==2) { // by push message
		$('.sms_layer').hide();
		$('.mail_layer').hide();
		$('.push_message_layer').show();
	}
	if(value == 0 || value == 1){
		$('.sms_mail_layer').show();
	}else{
		$('.sms_mail_layer').hide();
	}
}

function addAttachment()
{
	var x = '<span class="attach_row"><input type="file" name="Attachment[]" />[<a href="javascript:void(0);" onclick="removeAttachment(this);" ><?=$Lang['Btn']['Delete']?></a>]<br /></span>\n';
	$('div.attach_layer').append(x);
}

function removeAttachment(obj)
{
	$(obj).parent().remove();
}
function goExportMobile(){
	
	class_flag = false;
	classes = document.getElementsByName('ClassID[]');
	for(i=0;i<classes.length;i++){
		if(classes[i].checked) class_flag = true;
	}
	if(!class_flag)	{ alert("<?=$i_StudentGuardian_warning_PleaseSelectClass?>"); return false;}
	
	
	else{
	
	var url = '<?=$PATH_WRT_ROOT?>' + '/home/eAdmin/GeneralMgmt/MessageCenter/export_mobile.php?task=byClassName';
	$('form#form1').attr('target', '' );
	$('form#form1').attr('action', url ).submit();
	
	$('form#form1').attr('target', '_blank' );
	$('form#form1').attr('action', 'list.php' );
	}
}

$(document).ready(function(){
	if(AppMessageReminder){
		AppMessageReminder.targetMessageContentID = 'Message';
		AppMessageReminder.initInsertAtTextarea();
		AppMessageReminder.TargetAjaxScript = 'ajax_send_reminder_update.php';
	}
});
</script>

<BR>
<form name=form1  id="form1" method='post' action='list.php' onsubmit='return checkform(this)' enctype="multipart/form-data">
<!-- new layout -->

<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td class="tabletext" align="right" colspan="2"><?=$SysMsg?></td>
	</tr>
</table>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">

<tr><td colspan="2">
	<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td valign="top" nowrap="nowrap" class="tabletext"><input type=checkbox name='all' value=1 onClick='selectAll(this)'> <?=$i_StudentGuardian_all_students?></td></tr>
		<tr><td valign="top" nowrap="nowrap" class="tabletext"><?=$table?></td></tr>
	</table>	
</td></tr>
	<tr>
    	<td colspan="2">
    		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
    		<? if ($plugin['sms'] and $bl_sms_version >= 2) { ?>
    			<tr class="sms_mail_layer" <?=$plugin['eClassApp']?' style="display:none"':''?>>
    				<td valign="top" nowrap="nowrap" class="tabletext">
	    			</td>
	    			<td class="tabletext" width="70%"><?=$select_template?><BR>
	    			</td>
    			</tr>
    		<? } ?>
    		<?php if($plugin['eClassApp']){ ?>
    			<tr class="push_message_layer">
    				<td valign="top" nowrap="nowrap" class="tabletext">
    					<?=$Lang['SMS']['MessageTemplates']?>
	    			</td>
    				<td class="tabletext" width="70%">
						<?=$libAppTemplate->getMessageTemplateSelectionBox(true);?>
						<span id="AjaxStatus"></span>
					</td>
				</tr>
			<?php } ?>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_SMS_MessageContent?> *
	    			</td>
	    			<td class="tabletext" width="70%">
	    			<?=$linterface->GET_TEXTAREA("Message", "", 40)?>
	    			</td>
    			</tr>
    		<?php if($plugin['eClassApp']){ ?>
	    		<tr class="push_message_layer">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?=$Lang['eClassApp']['AutoFillItem']?>
					</td>
					<td class="tabletext" width="70%">
						<?=$libAppTemplate->getMessageTagSelectionBox();?>
					</td>
				</tr>
	    	<?php } ?>
    			<tr id="bal_tr" style="display: none">
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_SMS_Balance_Lass_Than?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    			$ <input type="text"  class='textboxnum' name="bal_less_than" value="">
	    			
	    			</td>
    			</tr>
    			
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
    					<?=$Lang['ePayment']['By']?>
    				</td>
    				<td class="tabletext" width="70%">
    					<?
    					if ($plugin['eClassApp']) {
    						$selectPushMessageAsDefault = true;
    						$selectSMSAsDefault = false;
    						$hideSMSLayer = 'style="display:none"';
    						$hidePushMessageLayer = '';
    						$hideMailLayer = 'style="display:none"';
    						$includeAppParentAsDefault = false;
    						$resetValue = 2;
    						echo $linterface->Get_Radio_Button("ByPushMessage", "ByMethod", "2", $selectPushMessageAsDefault, "", $Lang['AppNotifyMessage']['PushMessage'], "toggleMethod(this.value)"."&nbsp;",0);
    					} else {
    						$selectPushMessageAsDefault = false;
    						$selectSMSAsDefault = true;
    						$hideSMSLayer = '';
    						$hidePushMessageLayer = 'style="display:none"';
    						$hideMailLayer = 'style="display:none"';
    						$includeAppParentAsDefault = true;
    						$resetValue = 0;
    					}
    					if ($plugin['sms'] and $bl_sms_version >= 2) {
    						echo $linterface->Get_Radio_Button("BySms", "ByMethod", "0", $selectSMSAsDefault, "", $Lang['ePayment']['SMS'], "toggleMethod(this.value);",0)."&nbsp;";
    					}
    					?>
    					<?=$linterface->Get_Radio_Button("ByEmail", "ByMethod", "1", 0, "", $Lang['ePayment']['Email'], "toggleMethod(this.value)",0);?>
    				</td>
    			</tr>
				<tr class="sms_layer" <?=$hideSMSLayer?>>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
    					<?=$Lang['AppNotifyMessage']['ePayment']['IncludeAppParent']?>
    				</td>
    				<td class="tabletext" width="70%">
    					<?=$linterface->Get_Radio_Button("includeAppParent_yes", "includeAppParent", "1", $includeAppParentAsDefault, "", $Lang['General']['Yes'], "",0)."&nbsp;"
    					.$linterface->Get_Radio_Button("includeAppParent_no", "includeAppParent", "0", !$includeAppParentAsDefault, "", $Lang['General']['No'], "",0);?>
    				</td>
    			</tr>
    			<tr class="mail_layer" <?=$hideMailLayer?>>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
    					<?=$Lang['ePayment']['EmailSubject']?>
    				</td>
    				<td class="tabletext" width="70%">
    					<input type="text" id="EmailSubject" name="EmailSubject" value="" size="50" />
    				</td>
    			</tr>
    			<tr class="mail_layer" <?=$hideMailLayer?>>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
    					<?=$Lang['ePayment']['CcTo']?>
    				</td>
    				<td class="tabletext" width="70%">
    					<?=$linterface->Get_Checkbox("CcToAdmin", "CcTo[]", "1", 0, '', $Lang['SysMgr']['RoleManagement']['ePayment'], '', '')."&nbsp;".$linterface->Get_Checkbox("CcToClassTeacher", "CcTo[]", "2", 0, '', $Lang['ePayment']['ClassTeacher'], '', '');?>
    				</td>
    			</tr>
    			<tr class="mail_layer" <?=$hideMailLayer?>>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
    					<?=$Lang['ePayment']['EmailAttachment']?>
    				</td>
    				<td class="tabletext" width="70%">
    					<div class="attach_layer">
    						<span class="attach_row"><input type="file" name="Attachment[]" />[<a href="javascript:void(0);" onclick="removeAttachment(this);" ><?=$Lang['Btn']['Delete']?></a>]<br /></span>
    					</div>
    					[<a href="javascript:void(0);" onclick="addAttachment();" ><?=$Lang['Btn']['AddMore']?></a>]
    				</td>
    			</tr>
    			
				<tr class="push_message_layer" <?=$hidePushMessageLayer?>>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
    					<?=$Lang['AppNotifyMessage']['Title']?>
    				</td>
    				<td class="tabletext" width="70%">
    					<input type="text" id="PushMessageTitle" name="PushMessageTitle" value="" size="50" />
    				</td>
    			</tr>
    
    		</table>
    	</td>
    </tr>
      		
   <tr>
    	<td colspan="2">
		    <table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
		
				<tr>
					<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;</td>
				</tr>    
		    
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                
		    </table>
	    </td>
	</tr>
    <tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_search, "submit", "", "btn_search", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" onClick=\"toggleMethod($resetValue)\" ") ?>
			<?= ($plugin['eClassApp']? $linterface->GET_ACTION_BTN($Lang['MessageCenter']['ExportMobile'], "button", "goExportMobile()"): '')?>
		</td>
	</tr>
	<tr>
    	<td  width="90%" align="left" class="tabletextremark"><?= ($plugin['eClassApp']?$Lang['MessageCenter']['ExportMobileRemarks']:'')?></td>
    </tr>
       	<br>
    <tr>
    	<td class="tabletextremark"><?= ($plugin['eClassApp']?$Lang['AppNotifyMessage']['SMS_notificaction']['Remark'].'<br />* '.$Lang['ePayment']['AutoFillItemsPaymentItemStartDateEndDateAreNotApplicableToNotificationMessage']:'')?></td>
    </tr>
</table>
<input type="hidden" name="check_bal" value="">
<input type="hidden" name="Module" id="Module" value="<?=$Module?>">
<input type="hidden" name="Section" id="Section" value="">
</form>
<BR><BR>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>