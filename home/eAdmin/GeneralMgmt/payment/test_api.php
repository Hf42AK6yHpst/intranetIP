<?php
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

//debug_r($lpayment->Create_Notice_Payment_Item('testing API 3', '2010-02-02', '2010-02-12', '50', '1'));
//$lpayment->Edit_Notice_Payment_Item('13', 'testing API 1', '2010-02-02', '2010-02-12', '2');
//debug_r($lpayment->Remove_Notice_Payment_Item('14'));

//debug_r($lpayment->Process_Notice_Payment_Item('14', '1666', '180', '2'));

$linterface->LAYOUT_STOP();
intranet_closedb();
?>