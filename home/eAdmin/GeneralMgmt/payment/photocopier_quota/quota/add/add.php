<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PhotocopierQuota_QuotaManagement";

$linterface = new interface_html();

$lclass = new libclass();
$lidb = new libdb();

$type_selection = array(
	array(1,$i_ClassLevel),
	array(2,$i_ClassName),
	array(3,$i_SmartCard_Payment_photoCopier_UserType)
);

$add_type = getSelectByArray($type_selection,"name=targetType onChange=\"this.form.submit()\"",$targetType,1);


if($targetType == "")
{
	$table_content .= "<table width=\"96%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
	$table_content .= "<tr>";
	$table_content .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_SmartCard_Payment_PhotoCopier_TotalQuota</td>";
	$table_content .= "<td class=\"tabletext\" width=\"70%\"><input class=\"textboxnum\" type=\"text\" name=\"total_quota\"></td>";
	$table_content .= "</tr>";
	$table_content .= "</table>";
	$table_content .= "<input type=\"hidden\" name=\"flag\" value=\"0\">";
	$table_content .= "<input type=\"hidden\" name=\"add_type\" value=\"0\">";
}

if($targetType == 1)
{
	$sql = 'Select 
						YearID, 
						YearName 
					From 
						YEAR 
					Order by 
						Sequence';
	//$sql = "SELECT DISTINCT a.ClassLevelID, a.LevelName FROM INTRANET_CLASSLEVEL AS a INNER JOIN INTRANET_CLASS AS b ON (a.ClassLevelID = b.ClassLevelID) WHERE b.RecordStatus = 1";
	$classLevel = $lidb->returnArray($sql,2);
	
	$table_content .= "<table width=\"96%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
	$table_content .= "<tr class=\"tabletop\">";
	$table_content .= "<td class=\"tabletoplink\">$i_ClassLevel</td>";
	$table_content .= "<td class=\"tabletoplink\">$i_SmartCard_Payment_PhotoCopier_TotalQuota</td>";
	$table_content .= "<td class=\"tabletoplink\" width=\"1\"><input type=\"checkbox\" onClick=\"(this.checked)?setChecked(1,this.form,'LevelID[]'):setChecked(0,this.form,'LevelID[]')\"></td>";
	$table_content .= "</tr>";
	
	for($i=0; $i<sizeof($classLevel); $i++)
	{
		list($levelID, $levelName) = $classLevel[$i];
		$css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
		$table_content .= "<tr class=\"$css\">";
		$table_content .= "<td class=\"tabletext\">$levelName</td>";
		$table_content .= "<td class=\"tabletext\"><input class=\"textboxnum\" type=\"text\" name=\"total_quota_$levelID\" disabled=\"true\" style=\"background-color:#CCCCCC\"></td>";
		$table_content .= "<td class=\"tabletext\"><input type=\"checkbox\" name=\"LevelID[]\" value=\"$levelID\" onClick=\"this.checked? checkClicked(1, $levelID) : checkClicked(0, $levelID)\"></td>";
		$table_content .= "</tr>";
	}
	
	$table_content .= "</table>";
	$table_content .= "<input type=\"hidden\" name=\"flag\" value=\"1\">";
	$table_content .= "<input type=\"hidden\" name=\"add_type\" value=\"1\">";
}

if($targetType == 2)
{
	$lclass = new libclass();
	$classes = $lclass->getClassList();
		$currLvl = $classes[0][2];
	
	$table_content .= "<table width=\"96%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
	$table_content .= "<tr class=\"tabletop\">";
	$table_content .= "<td class=\"tabletoplink\">$i_ClassLevel</td>";
	$table_content .= "<td class=\"tabletoplink\">$i_SmartCard_Payment_PhotoCopier_TotalQuota</td>";
	$table_content .= "<td class=\"tabletoplink\" width=\"1\"><input type=\"checkbox\" onClick=\"(this.checked)?setChecked(1,this.form,'ClassID[]'):setChecked(0,this.form,'ClassID[]')\"></td>";
	$table_content .= "</tr>";
	
	for ($i=0; $i<sizeof($classes); $i++)
	{
		list($cid, $cname, $clvl) = $classes[$i];
		$css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
		if ($currLvl != $clvl)
		{
		   $currLvl = $clvl;
		}
		$table_content .= "<tr class=\"$css\">";
		$table_content .= "<td class=\"tabletext\">".$cname."</td>";
		$table_content .= "<td class=\"tabletext\"><input class=\"textboxnum\" type=\"text\" name=\"total_quota_".$cid."\" disabled=\"true\" style=\"background-color:#CCCCCC\"></td>";
		$table_content .= "<td class=\"tabletext\"><input type=\"checkbox\" name=\"ClassID[]\" value=\"".$cid."\" onClick=\"this.checked? checkClicked(1, ".$cid."):checkClicked(0, ".$cid.")\"></td>";
		$table_content .= "</tr>";
	}
	
	$table_content .= "</table>";
	$table_content .= "<input type=\"hidden\" name=\"flag\" value=\"2\">";
	$table_content .= "<input type=\"hidden\" name=\"add_type\" value=\"2\">";
}

if($targetType == 3)
{
	$usertype = array(
	array(1,$i_identity_teachstaff),
	array(2,$i_identity_student),
	);
	
	$table_content .= "<table width=\"96%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
	$table_content .= "<tr class=\"tabletop\">";
	$table_content .= "<td class=\"tabletoplink\">$i_SmartCard_Payment_photoCopier_UserType</td>";
	$table_content .= "<td class=\"tabletoplink\">$i_SmartCard_Payment_PhotoCopier_TotalQuota</td>";
	$table_content .= "<td class=\"tabletoplink\" width=\"1\"><input type=\"checkbox\" onClick=\"(this.checked)?setChecked(1,this.form,'TypeID[]'):setChecked(0,this.form,'TypeID[]')\")></td>";
	$table_content .= "</tr>";
	
	for($i=0; $i<sizeof($usertype); $i++)
	{
		list($type_index, $type_name) = $usertype[$i];
		$css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
		$table_content .= "<tr class=\"$css\">";
		$table_content .= "<td class=\"tabletext\">$type_name</td>";
		$table_content .= "<td class=\"tabletext\"><input class=\"textboxnum\" type=\"text\" name=\"total_quota_$type_index\" disabled=\"true\" style=\"background-color:#CCCCCC\"></td>";
		$table_content .= "<td class=\"tabletext\"><input type=\"checkbox\" name=\"TypeID[]\" value=\"$type_index\" onClick=\"this.checked? checkClicked(1, $type_index) : checkClicked(0, $type_index)\"></td>";
		$table_content .= "</tr>";
	}
	
	$table_content .= "</table>";
	$table_content .= "<input type=\"hidden\" name=\"flag\" value=\"3\">";
	$table_content .= "<input type=\"hidden\" name=\"add_type\" value=\"3\">";
}

$TAGS_OBJ[] = array($button_view, $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/photocopier_quota/quota/view/index.php", 0);
$TAGS_OBJ[] = array($button_add, $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/photocopier_quota/quota/add/add.php", 1);
$TAGS_OBJ[] = array($button_reset, $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/photocopier_quota/quota/reset/reset.php", 0);

$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$Msg = urldecode($Msg);
$linterface->LAYOUT_START($Msg);

$PAGE_NAVIGATION[] = array($button_add);

//if ($msg == "2") $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<script language="JavaScript">
function setChecked(val, obj, element_name){
        len=obj.elements.length;
        var i=0;
        var temp;
        
        if (val == 1)
        {
	        for( i=0 ; i<len ; i++) 
	        {
                if (obj.elements[i].name==element_name)
                {
                	obj.elements[i].checked=val;
            	}
            }
            <?
            if(sizeof($classes)>0)
            {
		            
	            for ($i=0; $i<sizeof($classes); $i++)
	            {
		            list($cid, $cname, $clvl) = $classes[$i];
			?>
	            	temp = eval("document.form1.total_quota_"+<?=$cid;?>);
	            	temp.disabled=false;
		            temp.value="0";
	            	temp.style.backgroundColor="";
            <?
        		}
        	}
        	if(sizeof($classLevel)>0)
        	{
	        	for($i=0; $i<sizeof($classLevel); $i++)
	        	{
		        	list($levelID, $levelName) = $classLevel[$i];
            ?>
            		temp = eval("document.form1.total_quota_"+<?=$levelID;?>);
	            	temp.disabled=false;
		            temp.value="0";
	            	temp.style.backgroundColor="";
	        <?
        		}
    		}
    		if(sizeof($usertype)>0)
        	{
	        	for($i=0; $i<sizeof($usertype); $i++)
	        	{
		        	list($type_index, $type_name) = $usertype[$i];
            ?>
            		temp = eval("document.form1.total_quota_"+<?=$type_index;?>);
	            	temp.disabled=false;
		            temp.value="0";
	            	temp.style.backgroundColor="";
	        <?
        		}
    		}
        	?>            
        }
        else
        {
	        for( i=0 ; i<len ; i++) 
	        {
                if (obj.elements[i].name==element_name)
                {
                	obj.elements[i].checked=val;
            	}
            }
            <?
            if(sizeof($classes)>0)
            {
		            
	            for ($i=0; $i<sizeof($classes); $i++)
	            {
		            list($cid, $cname, $clvl) = $classes[$i];
			?>
	            	temp = eval("document.form1.total_quota_"+<?=$cid;?>);
	            	temp.disabled=true; 
					temp.value="";
					temp.style.backgroundColor="#CCCCCC";
            <?
        		}
        	}
        	if(sizeof($classLevel)>0)
        	{
	        	for($i=0; $i<sizeof($classLevel); $i++)
	        	{
		        	list($levelID, $levelName) = $classLevel[$i];
            ?>
            		temp = eval("document.form1.total_quota_"+<?=$levelID;?>);
	            	temp.disabled=true; 
					temp.value="";
					temp.style.backgroundColor="#CCCCCC";
	        <?
        		}
    		}
    		if(sizeof($usertype)>0)
        	{
	        	for($i=0; $i<sizeof($usertype); $i++)
	        	{
		        	list($type_index, $type_name) = $usertype[$i];
            ?>
            		temp = eval("document.form1.total_quota_"+<?=$type_index;?>);
	            	temp.disabled=true; 
					temp.value="";
					temp.style.backgroundColor="#CCCCCC";
	        <?
        		}
    		}
        	?>
		}
}


function checkClicked(cnt, id)
{
	var temp;
	temp = eval("document.form1.total_quota_"+id);
	if (cnt == 1)
	{
		temp.disabled=false; 
		temp.value="0";
		temp.style.backgroundColor="";
	}
	else
	{
		temp.disabled=true; 
		temp.value="";
		temp.style.backgroundColor="#CCCCCC";
	}
}

function checkForm(obj,element)
{
	if(document.form1.flag.value == 0)
	{
		if(document.form1.total_quota.value == "")
		{
			alert('<?=$i_SmartCard_Payment_PhotoCopier_TotalQuota_Instruction?>');
			return false;
		}
		else
		{
			if ((isNaN(document.form1.total_quota.value)) && (document.form1.total_quota.value != ""))
			{
				alert('<?=$i_SmartCard_Payment_PhotoCopier_TotalQuota_Invalid?>');
				return false;
			}
				else
			{
				cnt = 1;
			}
		}
	}
	
	if((document.form1.flag.value==1)||(document.form1.flag.value==2)||(document.form1.flag.value==3))
	{
		var obj = document.form1;
		if(obj.flag.value==1)
		{
			var element = 'LevelID[]';
		}
		if(obj.flag.value==2)
		{
			var element = 'ClassID[]';
		}
		if(obj.flag.value==3)
		{
			var element = 'TypeID[]';
		}
		
	    if(countChecked(obj,element)>=1)
	    {
		    <?
        	if(sizeof($classLevel)>0)
        	{
	        	for($i=0; $i<sizeof($classLevel); $i++)
	        	{
		        	list($levelID, $levelName) = $classLevel[$i];
            ?>
            		temp = eval("document.form1.total_quota_"+<?=$levelID;?>);
	            	if((temp.disabled==false)&&(temp.value==""))
					{
						alert ('<?=$i_SmartCard_Payment_PhotoCopier_TotalQuota_Instruction?>');
						return false;
					}
					else 
					{
						if ((temp.disabled==false)&&(temp.value!="")&&(isNaN(temp.value)))
						{
							alert('<?=$i_SmartCard_Payment_PhotoCopier_TotalQuota_Invalid?>');
							return false;
						}
						else
						{
							cnt = 1;	
						}
					}
	        <?
        		}
    		}
            if(sizeof($classes)>0)
            {
	            for ($i=0; $i<sizeof($classes); $i++)
	            {
		            list($cid, $cname, $clvl) = $classes[$i];
			?>
	            	temp = eval("document.form1.total_quota_"+<?=$cid;?>);
	            	if((temp.disabled==false)&&(temp.value==""))
					{
						alert ('<?=$i_SmartCard_Payment_PhotoCopier_TotalQuota_Instruction?>');
						return false;
					}
					else 
					{
						if ((temp.disabled==false)&&(temp.value!="")&&(isNaN(temp.value)))
						{
							alert('<?=$i_SmartCard_Payment_PhotoCopier_TotalQuota_Invalid?>');
							return false;
						}
						else
						{
							cnt = 1;	
						}
					}
            <?
        		}
        	}
    		if(sizeof($usertype)>0)
        	{
	        	for($i=0; $i<sizeof($usertype); $i++)
	        	{
		        	list($type_index, $type_name) = $usertype[$i];
            ?>
            		temp = eval("document.form1.total_quota_"+<?=$type_index;?>);
	            	if((temp.disabled==false)&&(temp.value==""))
					{
						alert ('<?=$i_SmartCard_Payment_PhotoCopier_TotalQuota_Instruction?>');
						return false;
					}
					else 
					{
						if ((temp.disabled==false)&&(temp.value!="")&&(isNaN(temp.value)))
						{
							alert('<?=$i_SmartCard_Payment_PhotoCopier_TotalQuota_Invalid?>');
							return false;
						}
						else
						{
							cnt = 1;
						}
					}
	        <?
        		}
    		}
        	?>
	    } 
	    else 
		{
			alert(globalAlertMsg1);
	    }
    }
    if (cnt == 1)
	{
		document.form1.action = "add_update.php";
		return true;
	}
    
}

</script>
<br />
<form name="form1" action="" method="post" onSubmit="return checkForm()">
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr><td colspan="2" align="right"><?=$SysMsg?></td></tr>
</table>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
    	<td>
    		<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_alert_pleaseselect?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<?=$add_type?>
	    			</td>
    			</tr>
    		</table>
    	</td>
	</tr>
	<tr>
		<td><?=$table_content?></td>
	</tr>
	<tr>
    	<td>
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
	<tr>
	    <td align="center">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
		</td>
	</tr>
</table>
</form>

<?php
$linterface->LAYOUT_STOP();
print $linterface->FOCUS_ON_LOAD("form1.targetType");
intranet_closedb();
?>
