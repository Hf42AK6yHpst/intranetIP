<?php
// Editing by 
/*
 * 2014-04-10 (Carlos): Changed $i_SmartCard_Payment_PhotoCopier_FinalQuota to $Lang['ePayment']['QuotaLeft']
 */
$includeOncePath = "../../../../../../../";
include_once($includeOncePath."/includes/global.php");
include_once($includeOncePath."/includes/libdb.php");
include_once($includeOncePath."/includes/libuser.php");
include_once($includeOncePath."/includes/libpayment.php");
include_once($includeOncePath."/templates/fileheader.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}


$lu = new libuser($StudentID);

$li = new libdb();



	$sql = "SELECT 
				a.userid, a.Totalquota, a.usedquota,a.DateModified
			FROM 
				PAYMENT_PRINTING_QUOTA a
			WHERE
					a.UserID ='$StudentID' 
			";
	$quota_result = $li->returnArray($sql, 3);

	list($quota_userID, $quota_totalQuota, $quota_UsedQuota,$quota_DateModified)=$quota_result[0];

    $table_header = "<table width=95% border=0 cellpadding=0 cellspacing=0>";
	$table_header .= "<tr><td class='$css_text'><b>$i_UserName : </b>".$lu->UserNameClassNumber()."</td></tr>";
	$table_header .= "<tr><td class='$css_text'><B>$i_SmartCard_Payment_PhotoCopier_TotalQuota: </b>$quota_totalQuota</td></tr>";
	$table_header .= "<tr><td class='$css_text'><b>$i_SmartCard_Payment_PhotoCopier_UsedQuota : </b>$quota_UsedQuota</td></tr>";
	$table_header .= "<tr><td class='$css_text'><B>$i_Payment_Report_Print_Date : </b>".date('Y-m-d H:i:s')."</td></tr>";	
	$table_header .="</table>";




	$sql  =  "select a.DateModified, if (a.QuotaChange=0,'--',a.QuotaChange),  ";
	$sql .=  "a.QuotaAfter, ";
	$sql .=  "CASE a.RecordType ";
	$sql .=  "WHEN 1 THEN '".$i_SmartCard_Payment_PhotoCopier_ChangeRecordType_Add_By_Admin."' ";
	$sql .=  "WHEN 2 THEN '".$i_SmartCard_Payment_PhotoCopier_ChangeRecordType_Reset_By_Admin."' ";
	$sql .=  "WHEN 3 THEN '".$i_SmartCard_Payment_PhotoCopier_ChangeRecordType_Copied_Deduction."' ";
	$sql .=  "ELSE '".$i_SmartCard_Payment_PhotoCopier_ChangeRecordType_Purchasing."' END ";
	$sql .= "from ";
	$sql .= "PAYMENT_PRINTING_QUOTA_CHANGE_LOG a ";
	$sql .= "where ";
	$sql .= "a.UserID = '".$StudentID."' ";
	$sql .= "order by a.DateModified desc ";

	$result = $li->returnArray($sql,4);


$display = "<table width=95% border=0  cellpadding=2 cellspacing=0 class='$css_table'>";
$display .= "<tr class='$css_table_title'>
<td width=1% class='$css_table_title'>&nbsp;</td>
<td width=30% class='$css_table_title'>$i_SmartCard_Payment_PhotoCopier_LastestTransactionTime</td>
<td width=20% class='$css_table_title'>$i_SmartCard_Payment_PhotoCopier_ChangeQuota</td>
<td width=20% class='$css_table_title'>".$Lang['ePayment']['QuotaLeft']."</td>
<td width=29% class='$css_table_title'>$i_SmartCard_Payment_PhotoCopier_ChangeRecordType</td></tr>\n";
for ($i=0; $i<sizeof($result); $i++)
{
     //$css = ($i%2? "":"2");
	 $recordNo = $i +1;
     $css =$i%2==0?$css_table_content:$css_table_content."2";

     list($modifyDate, $quotaChange, $quotaAfter, $recordType) = $result[$i];
     
     $display .= "<tr class='$css'><td class='$css'>$recordNo</td><td class='$css'>$modifyDate</td><td class='$css'>$quotaChange</td><td class='$css'>$quotaAfter</td><td class='$css'>$recordType</td></tr>\n";
}
if(sizeof($result)<=0){
	$display.="<tr class='$css_table_content'><td colspan=5 align=center  class='$css_table_content' height=40 style='vertical-align:middle'>$i_no_record_exists_msg</td></tr>";
}
$display .= "</table>";


echo $table_header;
echo "<BR>";
echo $display;
include_once($includeOncePath."/templates/filefooter.php");
intranet_closedb();

exit();

?>
