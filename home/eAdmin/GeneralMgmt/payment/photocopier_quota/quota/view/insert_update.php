<?
// Editing by 
/*
 * 2014-03-28 (Carlos): Added PAYMENT_PRINTING_QUOTA.ModifiedBy and PAYMENT_PRINTING_QUOTA_CHANGE_LOG.ModifiedBy for trace purpose
 */
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lclass = new libclass();
$lidb = new libdb();

$list = "'".implode("','", $student)."'";

$sql = "SELECT UserID FROM PAYMENT_PRINTING_QUOTA WHERE UserID IN ($list)";
$result = $lclass->returnArray($sql, 1);

if(sizeof($result) == 0)
{
	for($i=0; $i<sizeof($student); $i++)
	{
		$sql = "INSERT INTO PAYMENT_PRINTING_QUOTA VALUES ('".$student[$i]."', $total_quota, 0, '', '', NOW(), NOW(),'".$_SESSION['UserID']."')";
		$lidb->db_db_query($sql);
	}
}
$x=1;


header("Location: index.php?msg=".$x);
intranet_closedb();
?>