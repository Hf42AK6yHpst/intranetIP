<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();

$li = new libdb();

$Result = array();
$li->Start_Trans();
if(sizeof($edit_list) > 0)
{
	for($i=0; $i<sizeof($edit_list); $i++)
	{
		list ($r_id) = $edit_list[$i];
		$sql = "UPDATE PAYMENT_PRINTING_PACKAGE SET NumOfQuota = ".${"num_of_quota_$r_id"}.", Price = ".${"price_$r_id"}." WHERE RecordID = '$r_id'";
		$Result[] = $li->db_db_query($sql);
	}
}

if (in_array(false,$Result)) {
	$li->RollBack_Trans();
	$Msg = $Lang['ePayment']['PhotocopyPackageEditFail'];
}
else {
	$li->Commit_Trans();
	$Msg = $Lang['ePayment']['PhotocopyPackageEditSuccess'];
}

Header("Location: index.php?Msg=".urlencode($Msg));
intranet_closedb();
?>