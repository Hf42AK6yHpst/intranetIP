<?php
// Editing by 
/*
 * 2019-09-24 (Ray):    Added PPSData value into PAYMENT_CREDIT_TRANSACTION
 * 2015-12-18 (Carlos): Allow negative balance for cust $sys_custom['ePayment']['AllowNegativeBalance'].
 * 2015-07-06 (Carlos): Added checking if balance enough to pay PPS charge. 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$GeneralSetting = new libgeneralsettings();

$SettingList[] = "'PPSChargeEnabled'";
$SettingList[] = "'PPSIEPSCharge'";
$SettingList[] = "'PPSCounterBillCharge'";
$Settings = $GeneralSetting->Get_General_Setting('ePayment',$SettingList);

$li = new libdb();

if($TargetStudentID != "")
{
        $sql = "SELECT UserID FROM INTRANET_USER WHERE UserID = '$TargetStudentID'";
        $result = $li->returnArray($sql,1);
        if(sizeof($result)==0)
        {
                header("location: index.php?mapped=1");
                exit();
        }
}

# Check previously stored value of ChargeDeduction
$pps_auto_charge = ($Settings['PPSChargeEnabled']==1);


$sql = "SELECT PPSAccount, Amount, InputTime, MappedStudentID, PPSType, PPSData FROM PAYMENT_MISSED_PPS_BILL WHERE RecordID = '$RecordID'";
$result = $li->returnArray($sql,5);
list($ppsaccount, $amount, $inputtime,$studentid, $PPSType, $PPSData) = $result[0];
$PPSType += 0;
if ($PPSType==1)
{
    $handling_charge = $Settings['PPSCounterBillCharge'];
}
else
{
    $handling_charge = $Settings['PPSIEPSCharge'];
}

if ($studentid != "")
{
    header("Location: index.php?mapped=1");
    exit();
}

$sql = "SELECT PPSAccountNo,Balance FROM PAYMENT_ACCOUNT WHERE StudentID = '$TargetStudentID'";
$temp = $li->returnResultSet($sql);
$actual_pps = $temp[0]['PPSAccountNo'];
$current_balance = $temp[0]['Balance'];

if($pps_auto_charge && !$sys_custom['ePayment']['AllowNegativeBalance'])
{
	$balance_after = $current_balance + $amount - $handling_charge;
	if($balance_after < 0){ // negative
		intranet_closedb();
		header("Location: index.php?Msg=".urlencode($Lang['ePayment']['PPSRemapFail']));
    	exit();
	}
}

$li->Start_Trans();
$sql = "UPDATE PAYMENT_MISSED_PPS_BILL set MappedStudentID = '$TargetStudentID' WHERE RecordID = '$RecordID'";
$Result['update PAYMENT_MISSED_PPS_BILL'] = $li->db_db_query($sql);



# RecordType: 1 - PPS
# RecordStatus: 0 - Completed , 1 - Not completed
$sql = "INSERT INTO PAYMENT_CREDIT_TRANSACTION (
					StudentID, 
					Amount, 
					RecordType, 
					RecordStatus, 
					PPSAccountNo, 
					TransactionTime, 
					DateInput, 
					PPSType,
					AdminInCharge,
					PPSData
					)
				VALUES (
					'$TargetStudentID',
					'$amount',
					1,
					0,
					'$actual_pps',
					'$inputtime',
					now(),
					'$PPSType',
					'".$_SESSION['UserID']."',
					'$PPSData'
					)";
$Result['INSERT PAYMENT_CREDIT_TRANSACTION'] = $li->db_db_query($sql);
$trans_id = $li->db_insert_id();

# Update Ref Code and Admin Name
$sql = "UPDATE PAYMENT_CREDIT_TRANSACTION SET
        RefCode = CONCAT('PPS',TransactionID) 
        WHERE TransactionID = '$trans_id'";
$Result['UPDATE PAYMENT_CREDIT_TRANSACTION'] = $li->db_db_query($sql);

$sql = "SELECT Balance FROM PAYMENT_ACCOUNT WHERE StudentID = '$TargetStudentID'";
$temp = $li->returnVector($sql);
$balanceAfter = $temp[0]+$amount;
if ($pps_auto_charge)
{
    $balanceAfter2 = $balanceAfter - $handling_charge;
    $sql = "UPDATE PAYMENT_ACCOUNT SET Balance = Balance + $amount - $handling_charge,
               LastUpdateByAdmin = '".$_SESSION['UserID']."',
               LastUpdateByTerminal= '', LastUpdated=now() WHERE StudentID = '$TargetStudentID'";
    $Result['UPDATE PAYMENT_ACCOUNT'] = $li->db_db_query($sql);

    $sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
               (StudentID,TransactionType,Amount,RelatedTransactionID,BalanceAfter,
                   TransactionTime,Details,RefCode) VALUES
               ('$TargetStudentID', 1, '$amount','$trans_id','$balanceAfter',now(),'$i_Payment_TransactionDetailPPS','PPS$trans_id')";
    $Result['INSERT PAYMENT_OVERALL_TRANSACTION_LOG'] = $li->db_db_query($sql);

    # PPS Charge
    $sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
               (StudentID,TransactionType,Amount,RelatedTransactionID,BalanceAfter,
                   TransactionTime,Details,RefCode) VALUES
               ('$TargetStudentID', 8, '$handling_charge','$trans_id','$balanceAfter2',now(),'$i_Payment_PPS_Charge','')";
    $Result['INSERT PAYMENT_OVERALL_TRANSACTION_LOG - set auto charge'] = $li->db_db_query($sql);

}
else
{
    $sql = "UPDATE PAYMENT_ACCOUNT SET Balance = Balance + $amount,
               LastUpdateByAdmin = '".$_SESSION['UserID']."',
               LastUpdateByTerminal= '', LastUpdated=now() WHERE StudentID = '$TargetStudentID'";
    $Result['UPDATE PAYMENT_ACCOUNT'] = $li->db_db_query($sql);
    $sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
               (StudentID,TransactionType,Amount,RelatedTransactionID,BalanceAfter,
                   TransactionTime,Details,RefCode) VALUES
               ('$TargetStudentID', 1, '$amount','$trans_id','$balanceAfter',now(),'$i_Payment_TransactionDetailPPS','PPS$trans_id')";
    $Result['INSERT PAYMENT_OVERALL_TRANSACTION_LOG'] = $li->db_db_query($sql);
}


/*
$sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
               (StudentID,TransactionType,Amount,RelatedTransactionID,BalanceAfter,
                   TransactionTime,Details,RefCode) VALUES
               ($TargetStudentID, 1, '$amount','$trans_id','$balanceAfter','$inputtime','$i_Payment_TransactionDetailPPS','PPS$trans_id')";
*/

if (in_array(false,$Result)) {
	$li->RollBack_Trans();
	$Msg = $Lang['ePayment']['PPSRemapFail'];
}
else {
	$li->Commit_Trans();
	$Msg = $Lang['ePayment']['PPSRemapSuccess'];
}

header ("Location: index.php?Msg=".urlencode($Msg));
intranet_closedb();
?>
