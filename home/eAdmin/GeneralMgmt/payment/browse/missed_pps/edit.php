<?php

# using: 

###############################################
#	Date:	2015-10-29  Carlos - include staff user.
#
#	Date:	2013-08-26	Carlos
#			Changed initial search use whole PPS account number length to search for exact match.
#			Added the default search length as default search link. 
#
#	Date:	2011-11-09	YatWoon
#			Improved: Searching result include "LoginID" as well
#
###############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "DataLogBrowsing_UnknownRecord";

$linterface = new interface_html();

$RecordID = (is_array($RecordID)? $RecordID[0]:$RecordID);

$li = new libdb();

/*
$sql = "SELECT PPSAccount, Amount, InputTime, MappedStudentID FROM PAYMENT_MISSED_PPS_BILL WHERE RecordID = '$RecordID'";
$result = $li->returnArray($sql,4);

list($ppsaccount, $amount, $inputtime,$studentid) = $result[0];

if ($studentid != "")
{
    header("Location: index.php?mapped=1");
    exit();
}
*/

$sql = "SELECT PPSAccount, Amount, InputTime, MappedStudentID,PPSType FROM PAYMENT_MISSED_PPS_BILL WHERE RecordID = '$RecordID'";
$result = $li->returnArray($sql,5);
list($ppsaccount, $amount, $inputtime,$studentid, $PPSType) = $result[0];

if ($studentid != "")
{
    header("Location: index.php?mapped=1");
    exit();
}

$stringlength = strlen($ppsaccount);
if (!isset($searchlength) || intval($searchlength)==0 )
{
     $searchlength = $stringlength;
}

# Get substrings
$default_searchlength = 4;
if ($default_searchlength > $stringlength) $default_searchlength = $stringlength;
if ($searchlength > $stringlength) $searchlength = $stringlength;
for ($i=0; $i<$stringlength-$searchlength+1; $i++)
{
     $temp  = substr($ppsaccount, $i, $searchlength);
     $substrings[] = $temp;
}
if ($searchlength == $stringlength)
{
    $substrings[] = $ppsaccount;
}

# Search
$namefield = getNameFieldByLang("b.");
$conds = "";
$delim = "";
for ($i=0; $i<sizeof($substrings); $i++)
{
     $conds .= "$delim a.PPSAccountNo LIKE '%".$substrings[$i]."%' ";
     $conds .= "or b.UserLogin LIKE '%".$substrings[$i]."%' ";
     $delim = " OR ";
}

$sql = "SELECT a.StudentID, a.PPSAccountNo, a.Balance, $namefield, b.ClassName, b.ClassNumber, b.UserLogin
               FROM PAYMENT_ACCOUNT as a INNER JOIN INTRANET_USER as b ON a.StudentID = b.UserID AND b.RecordType IN (1,2) AND b.RecordStatus IN (0,1,2)
               WHERE ($conds)
               ";
$result = $li->returnArray($sql,7);

# Check previously stored value of ChargeDeduction
$GeneralSetting = new libgeneralsettings();

$SettingList[] = "'PPSChargeEnabled'";
$SettingList[] = "'PPSIEPSCharge'";
$SettingList[] = "'PPSCounterBillCharge'";
$Settings = $GeneralSetting->Get_General_Setting('ePayment',$SettingList);
if ($Settings['PPSChargeEnabled']==1)
{
	if ($PPSType==1)
	{
	  $handling_charge = $Settings['PPSCounterBillCharge'];
	}
	else
	{
		$handling_charge = $Settings['PPSIEPSCharge'];
	}
}


$TAGS_OBJ[] = array($i_Payment_Menu_Browse_MissedPPSBill, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($i_Payment_action_handle);


?>
<br />
<form name="form1" action="edit_update.php" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
    	<td>
    		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_Payment_Field_RecordedAccountNo?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<?=$ppsaccount?>
	    			</td>
    			</tr>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_Payment_Field_CreditAmount?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<?=number_format($amount,1)?>
	    			</td>
    			</tr>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_Payment_Field_TransactionTime?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<?=$inputtime?>
	    			</td>
    			</tr>
    			<?php if ($Settings['PPSChargeEnabled']==1){?>

    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<font color=red><?=$i_Payment_PPS_Charge?></font>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<font color=red>$<?=$handling_charge?></font>
	    			</td>
    			</tr>
    			<?php } ?>
    			<tr><td>&nbsp;</td></tr>
    			<tr>
	    			<td colspan="2" class="tabletext" align="left">
	    				<?=$i_Payment_SearchResult?>
	    			</td>
    			</tr>
    		</table>
    	</td>
	</tr>
	<tr>
    	<td>
    		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
<?
if (sizeof($result)!=0)
{
?>
				<tr class="tabletop">
					<td class="tabletoplink" width="20%"><?=$i_UserLogin?></td>
					<td class="tabletoplink" width="20%"><?=$i_Payment_Field_PPSAccountNo?></td>
					<td class="tabletoplink" width="20%"><?=$i_UserName?></td>
					<td class="tabletoplink" width="10%"><?=$i_UserClassName?></td>
					<td class="tabletoplink" width="10%"><?=$i_UserClassNumber?></td>
					<td class="tabletoplink" width="20%"><?=$i_Payment_Field_Balance?></td>
					<td class="tabletoplink" width="1">&nbsp;</td>
				</tr>
<?
	for ($i=0; $i<sizeof($result); $i++)
	{
		list($res_sid, $res_pps, $res_balance, $res_name, $res_class, $res_classnum, $res_login) = $result[$i];
		$css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
?>
				<tr class="<?=$css?>">
					<td class="tabletext"><?=$res_login?></td>
					<td class="tabletext"><?=$res_pps?></td>
					<td class="tabletext"><?=$res_name?></td>
					<td class="tabletext"><?=$res_class?></td>
					<td class="tabletext"><?=$res_classnum?></td>
					<td class="tabletext"><?=number_format($res_balance,1)?></td>
					<td class="tabletext"><input type="radio" name="TargetStudentID" value="<?=$res_sid?>" <?=($i==0?"CHECKED":"")?>></td>
				</tr>
<?
	}
?>
			</table>
<?
} else {
?>
				<tr class="tablerow1">
					<td class="tabletext"><?=$i_no_search_result_msg?></td>
				</tr>
    		</table>
    	</td>
	</tr>
<?
}
if (sizeof($result)!=0)
{
?>
	<tr>
		<td>
			<table width="90%" border="0" cellpadding="3" cellspacing="0" align="center">
				<tr>
    				<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    			</tr>
    		</table>
    	</td>
    </tr>
	<tr>
	    <td align="center">
			<?= $linterface->GET_ACTION_BTN($button_select, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
<?
}
?>
	<tr>
    	<td>
		    <table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
                	<td>
                		<a class="tablelink" href="?RecordID=<?=$RecordID?>&searchlength=<?=($default_searchlength-1 == 0)? 1:($default_searchlength-1)?>"><?=$i_Payment_DecreaseSensitivity?></a>
                	</td>
                </tr>
                <tr>
                	<td>
                		<a class="tablelink" href="?RecordID=<?=$RecordID?>&searchlength=<?=$default_searchlength?>"><?=$Lang['ePayment']['DefaultSearch']?></a>
                	</td>
                </tr>
                <tr>
                	<td>
                		<a class="tablelink" href="?RecordID=<?=$RecordID?>&searchlength=<?=($default_searchlength+1)?>"><?=$i_Payment_IncreaseSensitivity?></a>
                	</td>
                </tr>
                <tr>
                	<td>
                		<a class="tablelink" href="manualselect.php?RecordID=<?=$RecordID?>"><?=$i_Payment_ManualSelectStudent?></a>
                	</td>
                </tr>
		    </table>
	    </td>
	</tr>
</table>
<input type="hidden" name="RecordID" value="<?=$RecordID?>">
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>