<?php
# Using:

#####################################################
#
#   Date:   2020-04-6  Ray
#           direct pay & top up mode different text
#
#   Date:   2020-03-05  Bill    [2020-0210-1031-11073]
#           pass actionTarget to ajax_getUnsettledPaymentItems.php
#
#	Date:	2017-02-14	Carlos
#			$sys_custom['ePayment']['HartsPreschool'] - Added STRN.
#
#	Date:	2015-11-04  Carlos
#			$sys_custom['ePayment']['AllowNegativeBalance'] - Add balance filter.
#			2015-10-12	Carlos
#			$sys_custom['ePayment']['AllowNegativeBalance'] - allow display negative balance.			
#
#	Date:	2015-10-07	Carlos
#			$sys_custom['ePayment']['CreditTransactionWithLoginID'] - added the display of [Login ID].
#
#	Date:	2014-12-11 	Carlos
#			Modified to enable sorting by LastUpdatedAdmin
#
#	Date:	2014-02-05	Carlos
#			Modified [Refund] and [Donate] actions - check unsettled payments before doing refund or donation.
#
#	Date:	2013-10-28	Carlos
#			KIS - Hide [Total Balance], [Balance], [Refund] button, [Donate to school] button
#
#	Date:	2011-05-31	YatWoon
#			Improved: add export function (#2010-1201-1621-05073)
#
#####################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
# preserve table view
if ($ck_data_log_browsing_view_user_record_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_data_log_browsing_view_user_record_page_number", $pageNo, 0, "", "", 0);
	$ck_data_log_browsing_view_user_record_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_data_log_browsing_view_user_record_page_number!="")
{
	$pageNo = $ck_data_log_browsing_view_user_record_page_number;
}

if ($ck_data_log_browsing_view_user_record_page_order!=$order && $order!="")
{
	setcookie("ck_data_log_browsing_view_user_record_page_order", $order, 0, "", "", 0);
	$ck_data_log_browsing_view_user_record_page_order = $order;
} else if (!isset($order) && $ck_data_log_browsing_view_user_record_page_order!="")
{
	$order = $ck_data_log_browsing_view_user_record_page_order;
}

if ($ck_data_log_browsing_view_user_record_page_field!=$field && $field!="")
{
	setcookie("ck_data_log_browsing_view_user_record_page_field", $field, 0, "", "", 0);
	$ck_data_log_browsing_view_user_record_page_field = $field;
} else if (!isset($field) && $ck_data_log_browsing_view_user_record_page_field!="")
{
	$field = $ck_data_log_browsing_view_user_record_page_field;
}

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance'];

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "DataLogBrowsing_ViewUserRecord";

if($lpayment->isEWalletDirectPayEnabled()) {
	$text_i_Payment_Menu_Browse_StudentBalance = $i_Payment_Menu_Browse_StudentBalance_DirectPay;
} else {
	$text_i_Payment_Menu_Browse_StudentBalance = $i_Payment_Menu_Browse_StudentBalance;
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$linterface = new interface_html();

$order = ($order == 1) ? 1 : 0;
if ($field == ""){
	$field = 0;
	if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
		$field += 1;
	}
	if($sys_custom['ePayment']['HartsPreschool']){
		$field += 1;
	}
}

if ($RecordStatus == "") $RecordStatus = 1;
if ($RecordStatus != 0 && $RecordStatus != 2 && $RecordStatus != 3 && $RecordStatus != 4) $RecordStatus = 1;

$namefield = getNameFieldByLang("b.");
$ArchiveNamefield = ($intranet_session_language == 'en')? 'b.EnglishName':'b.ChineseName';

$conds = "";
if ($ClassName != "")
{
    $conds .= "AND b.ClassName = '$ClassName' ";
}


switch($user_type){
	case 1: $user_cond = " AND b.RecordType=1"; break;
	case 2: $user_cond = " AND b.RecordType=2"; break;
	default : $user_cond = " AND b.RecordType IN (1,2) "; break;
}

####### get total outstanding balance of the target users ##
$year_left_cond = "";
if($RecordStatus == 3 || $RecordStatus == 4) {
	if($YearOfLeft != "") {
		$year_left_cond = " AND b.YearOfLeft = '$YearOfLeft' ";
	}		
}

if($sys_custom['ePayment']['AllowNegativeBalance']){
	if($BalanceStatus == '-1')
		$conds .= " AND a.Balance < 0 ";
}

if($RecordStatus != 4){
	$sql = "SELECT COUNT(*),SUM(".$lpayment->getExportAmountFormatDB("a.Balance").") 
					FROM PAYMENT_ACCOUNT AS a LEFT OUTER JOIN INTRANET_USER AS b ON a.StudentID = b.UserID
	        WHERE
	              b.RecordStatus = '$RecordStatus'  
	              $user_cond 
	              $conds 
	       ";
	if ($keyword != "") {
		$sql .= "
		            AND
	              (
	               b.EnglishName LIKE '%".$keyword."%' OR
	               b.ChineseName LIKE '%".$keyword."%' OR
	               b.ClassName LIKE '%".$keyword."%' OR
	               b.ClassNumber LIKE '%".$keyword."%'
	              ) ";
	}
	$sql .= $year_left_cond;
} else {
	$sql = "SELECT COUNT(*),SUM(".$lpayment->getExportAmountFormatDB("a.Balance").") FROM INTRANET_ARCHIVE_USER AS b LEFT OUTER JOIN PAYMENT_ACCOUNT AS a ON a.StudentID = b.UserID
	        WHERE 
	        	1 = 1 
	         	$year_left_cond
	          $user_cond $conds ";
	if ($keyword != "") {
		$sql .= "
								AND 
	              (
	               b.EnglishName LIKE '%".$keyword."%' OR
	               b.ChineseName LIKE '%".$keyword."%' OR
	               b.ClassName LIKE '%".$keyword."%' OR
	               b.ClassNumber LIKE '%".$keyword."%'
	              )
	          ";
	}
}
$ldb = new libdb();
$temp = $ldb->returnArray($sql,2);
if(sizeof($temp)>0){
	$no_of_users = $temp[0][0];
	$total_balance = $temp[0][1];
}else{
	$no_of_users = 0;
	$total_balance = 0;
}

if($total_balance < 0) {
	$str_total_balance = '-$ ' . number_format(abs($total_balance), 2);
} else {
	$str_total_balance = '$ ' . number_format($total_balance,2);
}
$infobar = "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION2($i_Payment_ItemSummary)."</td></tr>\n";
$infobar .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Payment_Total_Users</td>\n";
$infobar .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"70%\">$no_of_users</td></tr>\n";
if(!$isKIS) {
	$infobar .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Payment_Total_Balance</td>\n";
	$infobar .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">".$str_total_balance."</td></tr>\n";
}

/*### list users
$sql  = "SELECT
               CONCAT('<a class=tablelink href=view.php?StudentID=',a.StudentID,'>',$namefield,'</a>')
               , b.ClassName, b.ClassNumber,
               CONCAT('$',IF(a.Balance>=0,FORMAT(a.Balance,1),'0.0') ),
               IF(a.LastUpdated IS NULL,'&nbsp',DATE_FORMAT(a.LastUpdated,'%Y-%m-%d %H:%i')),
               IF(
                  a.LastUpdateByAdmin IS NOT NULL AND a.LastUpdateByAdmin != '',
                     CONCAT(a.LastUpdateByAdmin,' [$i_Payment_Field_AdminHelper]'),
                     IF(
                        a.LastUpdateByTerminal IS NOT NULL AND a.LastUpdateByTerminal != '',
                        CONCAT(a.LastUpdateByTerminal,' [$i_Payment_Field_TerminalUser]'),
                        '$i_Payment_Credit_TypeUnknown'
                     )
               ),
               		CONCAT('<input type=checkbox name=StudentID[] value=', a.StudentID ,'>')
         FROM
             PAYMENT_ACCOUNT as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
         WHERE
              b.RecordStatus = '$RecordStatus'  $user_cond $conds AND
              (
               b.EnglishName LIKE '%$keyword%' OR
               b.ChineseName LIKE '%$keyword%' OR
               b.ClassName LIKE '%$keyword%' OR
               b.ClassNumber LIKE '%$keyword%'
              )
                ";*/
// admin in charge name field
$Inchargenamefield = getNameFieldByLang("c.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$Inchargearchive_namefield = " IF(d.ChineseName IS NULL,d.EnglishName,d.ChineseName)";
	//$archive_namefield="c.ChineseName";
}else {
	//$archive_namefield ="c.EnglishName";
	$Inchargearchive_namefield = "IF(d.EnglishName IS NULL,d.ChineseName,d.EnglishName)";
}

### list users
if($RecordStatus != 4){
	$sql  = "SELECT ";
	if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
		$sql .= " b.UserLogin, ";
	}
	if($sys_custom['ePayment']['HartsPreschool']){
		$sql .= " b.STRN, ";
	}
     $sql .= "CONCAT('<a href=view.php?StudentID=',a.StudentID,'&RecordStatus=',$RecordStatus,'>',$namefield,'</a>')
             , b.ClassName, b.ClassNumber,";
    if(!$isKIS) {
    	if($sys_custom['ePayment']['AllowNegativeBalance']){
    		$sql .= $lpayment->getWebDisplayAmountFormatDB("a.Balance").",";
    	}else{
			$sql .= "IF(a.Balance>=0,".$lpayment->getWebDisplayAmountFormatDB("a.Balance").",".$lpayment->getWebDisplayAmountFormatDB("0")."),";
    	}
    }
	$sql .= "IF(a.LastUpdated IS NULL,'&nbsp',DATE_FORMAT(a.LastUpdated,'%Y-%m-%d %H:%i')),
             IF(
                a.LastUpdateByAdmin IS NOT NULL AND a.LastUpdateByAdmin != '',
                   IF(c.UserID IS NULL AND d.UserID IS NULL,
                   	CONCAT(a.LastUpdateByAdmin,' [$i_Payment_Field_AdminHelper]'),
                   	IF(d.UserID IS NOT NULL,
                   		CONCAT('<font color=red>*</font><i>',$Inchargearchive_namefield,'</i>'),
                   		$Inchargenamefield
                   		)
                   ),
                   IF(
                      a.LastUpdateByTerminal IS NOT NULL AND a.LastUpdateByTerminal != '',
                      CONCAT(a.LastUpdateByTerminal,' [$i_Payment_Field_TerminalUser]'),
                      '$i_Payment_Credit_TypeUnknown'
                   )
             ) as LastUpdatedAdmin,
             CONCAT('<input type=checkbox name=StudentID[] value=', a.StudentID ,'>')
	         FROM
	             PAYMENT_ACCOUNT as a 
	             LEFT OUTER JOIN 
	             INTRANET_USER as b 
	             ON a.StudentID = b.UserID
	             LEFT JOIN 
	             INTRANET_USER as c 
	             ON (a.LastUpdateByAdmin = c.UserID) 
	             LEFT JOIN 
	             INTRANET_ARCHIVE_USER as d 
	             ON (a.LastUpdateByAdmin = d.UserID) 
	         WHERE
	              b.RecordStatus = '$RecordStatus'  
	              $user_cond 
	              $conds ";
	if ($keyword != "") {
		$sql .= "
	              AND
	              (
	               b.EnglishName LIKE '%".$keyword."%' OR
	               b.ChineseName LIKE '%".$keyword."%' OR
	               b.ClassName LIKE '%".$keyword."%' OR
	               b.ClassNumber LIKE '%".$keyword."%'
	              )
	          ";
	}
	$sql .= $year_left_cond;
} else {
	$sql  = "SELECT ";
	if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
		$sql .= " b.UserLogin, ";
	}
	if($sys_custom['ePayment']['HartsPreschool']){
		$sql .= " b.STRN, ";
	}
	 $sql .= "CONCAT('<a href=view.php?StudentID=',b.UserID,'&RecordStatus=',$RecordStatus,'>',$ArchiveNamefield,'</a>')
	               , b.ClassName, b.ClassNumber,";
		if(!$isKIS){
			if($sys_custom['ePayment']['AllowNegativeBalance']){
				$sql .= $lpayment->getWebDisplayAmountFormatDB("a.Balance").",";
			}else{
				$sql .= "IF(a.Balance>=0,".$lpayment->getWebDisplayAmountFormatDB("a.Balance").",".$lpayment->getWebDisplayAmountFormatDB("0")."),";
			}
		}
			$sql .= "IF(a.LastUpdated IS NULL,'&nbsp',DATE_FORMAT(a.LastUpdated,'%Y-%m-%d %H:%i')),
	               IF(
		                a.LastUpdateByAdmin IS NOT NULL AND a.LastUpdateByAdmin != '',
		                   IF(c.UserID IS NULL AND d.UserID IS NULL,
		                   	CONCAT(a.LastUpdateByAdmin,' [$i_Payment_Field_AdminHelper]'),
		                   	IF(d.UserID IS NOT NULL,
		                   		CONCAT('<font color=red>*</font><i>',$Inchargearchive_namefield,'</i>'),
		                   		$Inchargenamefield
		                   		)
		                   ),
		                   IF(
		                      a.LastUpdateByTerminal IS NOT NULL AND a.LastUpdateByTerminal != '',
		                      CONCAT(a.LastUpdateByTerminal,' [$i_Payment_Field_TerminalUser]'),
		                      '$i_Payment_Credit_TypeUnknown'
		                   )
		             ) as LastUpdatedAdmin,
	               CONCAT('')
	         FROM
	             INTRANET_ARCHIVE_USER as b 
	             LEFT OUTER JOIN 
	             PAYMENT_ACCOUNT as a 
	             ON a.StudentID = b.UserID
	             LEFT JOIN 
	             INTRANET_USER as c 
	             ON (a.LastUpdateByAdmin = c.UserID) 
	             LEFT JOIN 
	             INTRANET_ARCHIVE_USER as d 
	             ON (a.LastUpdateByAdmin = d.UserID) 
	         WHERE 
	         	1=1 
	         	$year_left_cond
	          $user_cond $conds ";
	if ($keyword != "") {
		$sql .= "
								AND 
	              (
	               b.EnglishName LIKE '%".$keyword."%' OR
	               b.ChineseName LIKE '%".$keyword."%' OR
	               b.ClassName LIKE '%".$keyword."%' OR
	               b.ClassNumber LIKE '%".$keyword."%'
	              )
	          ";
	}
}
//debug_r($sql);
# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array();
if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
	$li->field_array[] = "b.UserLogin";
}
if($sys_custom['ePayment']['HartsPreschool']){
	$li->field_array[] = "b.STRN";
}
$li->field_array = array_merge($li->field_array, array("b.EnglishName","b.ClassName,b.ClassNumber","b.ClassNumber"));
if(!$isKIS){
	$li->field_array[] = "a.Balance";
}
$li->field_array[] = "a.LastUpdated";
$li->field_array[] = "LastUpdatedAdmin";
/*
if($isKIS) {
	$li->field_array = array("b.EnglishName","b.ClassName,b.ClassNumber","b.ClassNumber","a.LastUpdated","LastUpdatedAdmin");
}else{
	$li->field_array = array("b.EnglishName","b.ClassName,b.ClassNumber","b.ClassNumber","a.Balance","a.LastUpdated","LastUpdatedAdmin");
}
if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
	array_unshift($li->field_array,"b.UserLogin");
}
*/
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0,0,0);
$li->IsColOff = 2;


// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
	$li->column_list .= "<td >".$li->column($pos++, $Lang['AccountMgmt']['LoginID'])."</td>\n";
}
if($sys_custom['ePayment']['HartsPreschool']){
	$li->column_list .= "<td >".$li->column($pos++, $i_STRN)."</td>\n";
}
$li->column_list .= "<td >".$li->column($pos++, $i_Payment_Field_Username)."</td>\n";
$li->column_list .= "<td >".$li->column($pos++, $i_UserClassName)."</td>\n";
$li->column_list .= "<td >".$li->column($pos++, $i_UserClassNumber)."</td>\n";
if(!$isKIS){
	$li->column_list .= "<td >".$li->column($pos++, $i_Payment_Field_Balance)."</td>\n";
}
$li->column_list .= "<td >".$li->column($pos++, $i_Payment_Field_LastUpdated)."</td>\n";
$li->column_list .= "<td >".$li->column($pos++, $i_Payment_Field_LastUpdatedBy)."</td>\n";
$li->column_list .= "<td width='1'>".$li->check("StudentID[]")."</td>\n";

## select user
$array_user_name = array($i_Payment_All_Users, $i_identity_student, $i_identity_teachstaff);
$array_user_data = array("", "2", "1");
$select_user = getSelectByValueDiffName($array_user_data,$array_user_name,"name='user_type' onChange='filterByUser(this.form);'",$user_type,0,1);

## select status
$array_status_name = array($i_status_suspended, $i_status_approved, $i_status_pendinguser, $i_Payment_Menu_Browse_StudentBalance_Status_Left_Temp, $i_Payment_Menu_Browse_StudentBalance_Status_Left_Archived);
$array_status_data = array("0", "1", "2", "3", "4");
$select_status = getSelectByValueDiffName($array_status_data,$array_status_name,"name='RecordStatus' onChange='this.form.submit();'",$RecordStatus,0,1);

if($sys_custom['ePayment']['AllowNegativeBalance']){
	$balance_filter_data = array(array('-1',$Lang['ePayment']['NegativeBalancesOnly']));
	$balance_filter = getSelectByArray($balance_filter_data, ' id="BalanceStatus" name="BalanceStatus" onchange="document.form1.submit();" ', $BalanceStatus, 1, 0, $Lang['ePayment']['AllBalances']);
}

if($lpayment->isEWalletDirectPayEnabled() == false) {
	$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:performRefundDonate(document.form1,'StudentID[]','refund.php','$i_Payment_alert_refund')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_refund.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						" . $Lang['ePayment']['LeaveSchoolRefund'] . "
					</a>
				</td>";
	$table_tool .= "<td nowrap=\"nowrap\">&nbsp;|&nbsp;
					<a href=\"javascript:performRefundDonate(document.form1,'StudentID[]','donate.php','" . $Lang['Payment']['DonateBalanceWarning'] . "')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_donate.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						" . $Lang['Payment']['DonateBalance'] . "
					</a>
				</td>";
}

#################################################
#			added on 20080821 by Ronald			#
#-----------------------------------------------#
# 	Drop down box to select school year left	#
#	Only show when select "LEFT"				#
#################################################
if($RecordStatus != 4){
	$sql = "SELECT DISTINCT YearOfLeft FROM INTRANET_USER WHERE YearOfLeft IS NOT NULL AND YearOfLeft != '' ORDER BY YearOfLeft";
}else{
	$sql = "SELECT DISTINCT YearOfLeft FROM INTRANET_ARCHIVE_USER WHERE YearOfLeft IS NOT NULL AND YearOfLeft != '' ORDER BY YearOfLeft";
}
$arr_year_left = $lpayment->returnVector($sql);

$select_year_left .= "<SELECT name=\"YearOfLeft\" onChange=\"this.form.submit()\">\n";
$empty_selected = ($selected == '')? "SELECTED":"";
$select_year_left .= "<OPTION value='' $empty_selected>-- $i_Profile_DataLeftYear --</OPTION>\n";
if(sizeof($arr_year_left)>0) {
	for($i=0; $i<sizeof($arr_year_left); $i++) {
		$name = $arr_year_left[$i];
		$sel_str = ($YearOfLeft == $name? "SELECTED":"");
		$select_year_left .= "<OPTION value='$name' $sel_str> $name </OPTION>\n";
	}
}
$select_year_left .= "</SELECT>\n";

$lclass = new libclass();
$temp = $button_select;
$button_select = $i_general_all_classes;
$select_class = $lclass->getSelectClass("name='ClassName' onChange='filterByClass(this.form)'",$ClassName);
$button_select = $temp;

$toolbar = $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
$toolbar .= $linterface->GET_LNK_EXPORT("javascript:click_export()","","","","",0);

$filterbar = $select_user.$select_class.$select_status;
if($RecordStatus == 3 || $RecordStatus == 4) {
	$filterbar .= $select_year_left;
}
if($sys_custom['ePayment']['AllowNegativeBalance']){
	$filterbar .= $balance_filter;
}
$searchbar = "<input class=\"textboxnum\" type=\"text\" name=\"keyword\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= $linterface->GET_SMALL_BTN($button_find, "button", "document.form1.submit();","submit2");

if ($refund == 1)
{
    if ($count > 0)
    {
        $xmsg = "<span style='color:green'>$count $i_Payment_Students$i_Payment_Refunded</span>";
    }
    else
    {
        $xmsg = "$i_Payment_con_RefundFailed";
    }
}

//$SysMsg = $linterface->GET_SYS_MSG("", $xmsg);

$TAGS_OBJ[] = array($text_i_Payment_Menu_Browse_StudentBalance, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();

// return message
$Msg=urldecode($Msg);
$linterface->LAYOUT_START($Msg);
?>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />
<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>

<script language="Javascript">
function openPrintPage()
{
	newWindow('list_print.php?SortField=<?=$field?>&SortOrder=<?=$order?>&user_type=<?=$user_type?>&ClassName=<?=$ClassName?>&RecordStatus=<?=$RecordStatus?>&keyword=<?=$keyword?>&YearOfLeft=<?=$YearOfLeft?><?=$sys_custom['ePayment']['AllowNegativeBalance']?'&BalanceStatus='.$BalanceStatus:''?>',10);
}
function click_export()
{
	document.form1.action = 'export.php';
	document.form1.submit();
	document.form1.action = '';
}
function filterByUser(formObj){
	if(formObj==null) return;
	classNameObj = formObj.ClassName;
	userTypeObj = formObj.user_type;
	if(classNameObj==null || userTypeObj ==null) return;
	v = userTypeObj.options[userTypeObj.selectedIndex].value;
	if(v=='' || v==1)
		classNameObj.options[0].selected = true;
	formObj.submit();
}
function filterByClass(formObj){
	if(formObj==null) return;
	classNameObj = formObj.ClassName;
	userTypeObj = formObj.user_type;
	if(classNameObj==null || userTypeObj ==null) return;
	v = classNameObj.options[userTypeObj.selectedIndex].value;
	if(v!='')
		userTypeObj.options[0].selected = true;
	formObj.submit();
}

function performRefundDonate(obj,element,page,msg)
{
	if(countChecked(obj,element) == 0){
       alert(globalAlertMsg2);
	}
	else{
		var studentIdElems = document.getElementsByName(element);
		var studentIdAry = [];
		for(var i=0; i<studentIdElems.length; i++){
			if(studentIdElems[i].checked){
				studentIdAry.push(studentIdElems[i].value);
			}
		}

		$.post(
			'ajax_getUnsettledPaymentItems.php',
			{
				'StudentID[]': studentIdAry,
                'actionTarget': page
			},
			function(data){
				if(data != ''){
					js_Show_ThickBox('<?=$Lang['General']['Warning']?>');
					$('#TB_ajaxContent').html(data);
					$('#TB_ajaxContent').css('overflow','hidden');
				}
				else{
					if(confirm(msg)){
		                obj.action = page;
		                obj.method = "POST";
		                obj.submit();
	                }
				}
			}
		);
    }
}
</script>

<br />
<form name="form1" method="get">
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<?=$infobar?>
	<tr>
		<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	</tr>
</table>
<br />
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?></td>
		<td align="right"><?=$SysMsg?></td>
	</tr>
	<tr>
		<td align="left"><?= $filterbar ?><?=$searchbar?></td>
		<td valign="bottom" align="right">
		<?php if(!$isKIS){ ?>
            <?php if(!empty($table_tool)) { ?>
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<?=$table_tool?>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
            <?php } ?>
		<?php }else{ ?>
			&nbsp;
		<?php } ?>
		</td>
	</tr>
</table>
<?php echo $li->display("96%"); ?>
	<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td class="tabletextremark">
			<?=$i_Payment_Refund_Warning?>
		</td>
	</tr>
</table>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
</form>
<br />

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>