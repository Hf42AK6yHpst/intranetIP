<?
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
if (!$lpayment->IS_ADMIN_USER($_SESSION['UserID'])) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$li = new libdb();

$order = ($order == 1) ? 1 : 0;
if ($field == "" || $field>7) $field = 7;
$field += 0;

$namefield = getNameFieldByLang("b.");

if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$archive_namefield = "c.ChineseName";
} else {
	$archive_namefield = "c.EnglishName";
}

# date range
$FromDate = $FromDate == ""?((date('Y')-1)."-09-01"):$FromDate;
$ToDate = $ToDate == ""?(date('Y')."-08-31"):$ToDate;

$date_cond = " AND a.TransactionTime Between '$FromDate' AND '$ToDate' ";

switch($user_type){
	case 2:
		$user_cond = " AND (b.RecordType=2 OR c.RecordType=2)";
		break;
	case 1:
		$user_cond = " AND (b.RecordType=1 OR (b.RecordType IS NULL AND c.RecordType IS NULL))";
		break;
	default:
		$user_cond = "";
		break;
}


$sql  = "SELECT
               DATE_FORMAT(a.DateInput,'%Y-%m-%d %H:%i'),
               DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i'),
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),c.ClassName, IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassName)),
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),c.ClassNumber,IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassNumber)),
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),$archive_namefield,IF(b.UserID IS NULL AND c.UserID IS NULL,'',$namefield)),
               a.Amount,
               CASE a.RecordType
                    WHEN 1 THEN '$i_Payment_Credit_TypePPS'
                    WHEN 2 THEN '$i_Payment_Credit_TypeCashDeposit'
                    WHEN 3 THEN '$i_Payment_Credit_TypeAddvalueMachine'
                    ELSE '$i_Payment_Credit_TypeUnknown' END,
               a.RefCode, a.AdminInCharge
         FROM
             PAYMENT_CREDIT_TRANSACTION as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
             LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON a.StudentID=c.UserID
         WHERE
              (
               b.EnglishName LIKE '%$keyword%' OR
               b.ChineseName LIKE '%$keyword%' OR
               b.ClassName LIKE '%$keyword%' OR
               b.ClassNumber LIKE '%$keyword%' OR
               a.RefCode LIKE '%$keyword%' OR
               a.AdminInCharge LIKE '%$keyword%' OR
               c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%' 

              )
              $date_cond
              $user_cond
                ";
$field_array = array("b.EnglishName,c.EnglishName","b.ClassName,c.ClassName","b.ClassNumber,c.ClassNumber","a.Amount","a.RecordType","a.RefCode","a.AdminInCharge","a.TransactionTime");
$sql .= " ORDER BY ".$field_array[$field].( ($order==0) ? " DESC" : " ASC");

$result = $li->returnArray($sql,8);

$lexport = new libexporttext();

// Export Column Name
$exportColumn = array("PostTime", "TransactionTime", "Class", "Class Number", "Username", "Amount", "Method", "RefCode", "AdminInCharge");

// Prepare Export File
$export_content = $lexport->GET_EXPORT_TXT($result, $exportColumn);

// Export File
$filename = "credit_transactions.csv";
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>
