<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "DataLogBrowsing_TerminalLog";

### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
# preserve table view
if ($ck_data_log_browsing_terminal_log_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_data_log_browsing_terminal_log_page_number", $pageNo, 0, "", "", 0);
	$ck_data_log_browsing_terminal_log_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_data_log_browsing_terminal_log_page_number!="")
{
	$pageNo = $ck_data_log_browsing_terminal_log_page_number;
}

if ($ck_data_log_browsing_terminal_log_page_order!=$order && $order!="")
{
	setcookie("ck_data_log_browsing_terminal_log_page_order", $order, 0, "", "", 0);
	$ck_data_log_browsing_terminal_log_page_order = $order;
} else if (!isset($order) && $ck_data_log_browsing_terminal_log_page_order!="")
{
	$order = $ck_data_log_browsing_terminal_log_page_order;
}

if ($ck_data_log_browsing_terminal_log_page_field!=$field && $field!="")
{
	setcookie("ck_data_log_browsing_terminal_log_page_field", $field, 0, "", "", 0);
	$ck_data_log_browsing_terminal_log_page_field = $field;
} else if (!isset($field) && $ck_data_log_browsing_terminal_log_page_field!="")
{
	$field = $ck_data_log_browsing_terminal_log_page_field;
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$linterface = new interface_html();

/*
$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 0;
*/

if ($RecordStatus == "") $RecordStatus = 1;
if ($RecordStatus != 0 && $RecordStatus != 2 && $RecordStatus != 3) $RecordStatus = 1;


$namefield = getNameFieldByLang("b.");
$sql  = "SELECT
               LoginTime, LogoutTime, Username, IP
         FROM
             PAYMENT_TERMINAL_LOG
         WHERE
               Username LIKE '%$keyword%' OR
               IP LIKE '%$keyword%'
                ";

# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("LoginTime","LogoutTime","Username","IP");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
$li->column_array = array(0,0,0,0);
$li->wrap_array = array(0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width='1' class='tablebluetop tabletoplink'>#</td>\n";
$li->column_list .= "<td width='30%' class='tablebluetop tabletoplink'>".$li->column($pos++, $i_Payment_Field_LoginTime)."</td>\n";
$li->column_list .= "<td width='30%' class='tablebluetop tabletoplink'>".$li->column($pos++, $i_Payment_Field_LogoutTime)."</td>\n";
$li->column_list .= "<td width='20%' class='tablebluetop tabletoplink'>".$li->column($pos++, $i_Payment_Field_Username)."</td>\n";
$li->column_list .= "<td width='20%' class='tablebluetop tabletoplink'>".$li->column($pos++, $i_Payment_Field_IP)."</td>\n";

$searchbar = "<input class=\"textboxnum\" type=\"text\" name=\"keyword\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= $linterface->GET_SMALL_BTN($button_find, "button", "document.form1.submit();","submit2");

$TAGS_OBJ[] = array($i_Payment_Menu_Browse_TerminalLog, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

?>
<br />
<form name="form1" method="get">
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$searchbar?></td>
	</tr>
</table>
<?php echo $li->displayFormat2("96%", "blue"); ?>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
