<?php
// Editing by 
/*
 * 2017-03-22 (Carlos): Deduct voided transactions. 
 * 2015-02-26 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$lexport = new libexporttext();
$linterface = new interface_html();

if ($start == "" || $end == "") # Default current month
{
    $start = date("Y-m-d",mktime(0,0,0,date('m'),1,date('Y')));
    $end = date("Y-m-d",mktime(0,0,0,date('m')+1,-1,date('Y')));
}

$item = urldecode($item);

$start_ts = strtotime($start);
$end_ts = strtotime($end) + 60*60*24 - 1;

if ($ClassName != "")
{
    $conds .= "AND (b.ClassName = '$ClassName' OR c.ClassName ='$ClassName')";
    $class_line = "<tr><td align='left' class='eSportprinttitle'>$i_ClassName:</td>";
    $class_line = "<td align='left' class='eSportprinttitle'>$ClassName</td></tr>\n";
}
$namefield = getNameFieldByLang("b.");

if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$archive_namefield = "c.ChineseName";
}else
	$archive_namefield = "c.EnglishName";
   
switch($user_type){
	case 1: $user_cond = " AND (b.RecordType=1 OR (b.RecordType IS NULL AND c.RecordType IS NULL))"; break;
	case 2: $user_cond = " AND (b.RecordType=2 OR c.RecordType=2)"; break;
	default : $user_cond = ""; break;
}
      
$sql  = "SELECT
				IF((b.UserID IS NULL AND c.UserID IS NOT NULL),$archive_namefield,$namefield),
				IF((b.UserID IS NULL AND c.UserID IS NOT NULL),c.ClassName,b.ClassName),
				IF((b.UserID IS NULL AND c.UserID IS NOT NULL),c.ClassNumber,b.ClassNumber),
				IF((b.UserID IS NULL AND c.UserID IS NOT NULL),c.CardID,b.CardID) as CardID,
               a.Amount-IF(t.Amount IS NOT NULL,t.Amount,0) as Amount,
               DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i')
         FROM 
             PAYMENT_OVERALL_TRANSACTION_LOG as a 
			 LEFT JOIN PAYMENT_OVERALL_TRANSACTION_LOG as t ON t.TransactionType=11 AND t.StudentID=a.StudentID AND t.RefCode=a.RefCode  
			 LEFT JOIN INTRANET_USER as b ON a.StudentID = b.UserID
             LEFT JOIN INTRANET_ARCHIVE_USER AS c ON a.StudentID = c.UserID
         WHERE
              a.TransactionType = 3 AND TRIM(REPLACE(a.Details,'<br>','')) = '".trim(str_replace(array("<br>","\n"),array("",""),$item))."'
              AND UNIX_TIMESTAMP(a.TransactionTime) BETWEEN $start_ts AND $end_ts
              $conds
              $user_cond 
                AND ( 
              if(b.UserID IS NOT NULL,$namefield LIKE '%$keyword%',IF(c.UserID IS NOT NULL,$archive_namefield LIKE '%$keyword%','$keyword'=''))
              )              

                ";
              //AND ($archive_namefield LIKE '%$keyword%' OR $namefield LIKE '%$keyword%')   
$field_array = array("b.EnglishName","b.ClassName","b.ClassNumber","CardID","a.Amount","a.TransactionTime");
$sql .= " ORDER BY ";
$sql .= (count($field_array)<=$field) ? $field_array[0] : $field_array[$field];
$sql .= ($order==0) ? " DESC" : " ASC";

$info = $lpayment->returnArray($sql,5);
$record_count = count($info);

$header = array($i_Payment_Field_Username,$i_UserClassName, $i_UserClassNumber, $i_SmartCard_CardID , $i_Payment_Field_Amount, $i_Payment_Field_TransactionTime);
$rows = array();

for ($i=0; $i<$record_count; $i++)
{
     list($studentname, $class, $classnum, $cardid, $amount, $trans_time) = $info[$i];
     $amount = "$ ".number_format($amount,2);
     
     $row = array($studentname,$class,$classnum,$cardid,$amount,$trans_time);
     $rows[] = $row;
}

$title = "$i_Payment_Title_Overall_SinglePurchasing - $item (".$start."_".$i_Profile_To."_".$end.")";

$filename = $title.".csv";
$export_content = $lexport->GET_EXPORT_TXT($rows, $header,"","\r\n","",0,"11");
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>
