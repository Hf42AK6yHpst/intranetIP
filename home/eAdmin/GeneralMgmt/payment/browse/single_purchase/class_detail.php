<?php
//editing by 
/*
 * 2017-03-22 (Carlos): Deduct voided transactions. 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

if($Item=="" || $FromDate=="" || $ToDate=="") header("Location: index.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$li = new libpayment();
$fcm = new form_class_manage();

$MODULE_OBJ['title'] = $i_Payment_Menu_Browse_PurchasingRecords." - ".$Item." (".$FromDate." ".$Lang['General']['To']." ".$ToDate.")";
$linterface = new interface_html("popup.html");

$CurrentAcademicYearID = Get_Current_Academic_Year_ID();

$start_ts = strtotime($FromDate." 00:00:00");
$end_ts = strtotime($ToDate." 23:59:59");

if($ClassID != ''){
	$students = $fcm->Get_Student_By_Class($ClassID);
	$libyear = new Year($YearID);
	$libclass = new year_class($ClassID);
	$year_name = htmlspecialchars($libyear->YearName,ENT_QUOTES);
	$class_name = Get_Lang_Selection($libclass->ClassTitleB5,$libclass->ClassTitleEN);
	
	$x = '<div class="navigation" style="width:96%;">
			<img height="15" width="15" align="absmiddle" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/nav_arrow.gif"/>';
		$x.="<span><a href=\"javascript:SubmitForm('','');\">".$Lang['ePayment']['YearForm']."</a></span>";
		$x.='<img height="15" width="15" align="absmiddle" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/nav_arrow.gif"/>';
		$x.= '<span><a href="javascript:SubmitForm(\''.$YearID.'\',\'\');">'.$year_name.'</a></span>';
		$x.='<img height="15" width="15" align="absmiddle" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/nav_arrow.gif"/>';
		$x.= '<span>'.$class_name.'</a>';
	$x.= '</div><br />';
	
	$x.="<table width=\"96%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\" align=\"center\">";
	$x.="<tr>";
	// Student name
	$x.="<td class=\"tablebluetop tabletopnolink\" width=\"50%\">".$Lang['General']['Name']."</td>";
	// Amount
	$x.="<td class=\"tablebluetop tabletopnolink\" width=\"50%\">".$i_Payment_Field_SinglePurchase_TotalAmount."</td>";
	$x.="</tr>";
	
	$student_userid = Get_Array_By_Key($students,'UserID');
	$user_cond = " AND a.StudentID IN (".(count($student_userid)?implode(",",$student_userid):"-1").") ";
	
	$name_field = getNameFieldWithClassNumberByLang("b.");
	$name_field2 = Get_Lang_Selection("c.ChineseName","c.EnglishName");
	$name_field3 = "CONCAT(".$name_field2.",IF(c.ClassName IS NOT NULL,CONCAT(' (',c.ClassName,'-',c.ClassNumber,')'),''))";
	
	$sql  = "SELECT 
				 a.StudentID,
				 IF(b.UserID IS NOT NULL,$name_field,IF(c.UserID IS NOT NULL,$name_field3,'')) as StudentName,
	             SUM(a.Amount-IF(t.Amount IS NOT NULL,t.Amount,0)) as TotalAmount   
	         FROM 
	             PAYMENT_OVERALL_TRANSACTION_LOG as a 
				 LEFT JOIN PAYMENT_OVERALL_TRANSACTION_LOG as t ON t.TransactionType=11 AND t.StudentID=a.StudentID AND t.RefCode=a.RefCode 
				 LEFT JOIN INTRANET_USER as b ON a.StudentID = b.UserID
	             LEFT JOIN INTRANET_ARCHIVE_USER AS c ON a.StudentID = c.UserID
	         WHERE 
	              a.TransactionType = 3 AND a.Details = '$Item'
	              AND UNIX_TIMESTAMP(a.TransactionTime) BETWEEN '".$start_ts."' AND '".$end_ts."' 
	              $user_cond 
			 GROUP BY a.StudentID 
			 ORDER BY IF(b.UserID IS NOT NULL,b.ClassNumber+0,c.ClassNumber+0) ";
	//debug_r($sql);          
	$temp = $li->returnArray($sql);
	
	$sum_amount = 0;
	for($j=0;$j<count($temp);$j++){
		list($student_id,$student_name,$amount) = $temp[$j];
		
		$sum_amount += $amount + 0;
		
		$css = $j%2==0?"tablebluerow1":"tablebluerow2";
		// Student Name
		$x.="<tr class=\"".$css."\">";
		$x.="<td class=\"".$css."\">".$student_name."</td>";
		// Amount 
		$x.="<td class=\"".$css."\">$".number_format(sprintf("%.2f",$amount),2)."</td>";
		$x.="</tr>";
	}
	
	$x .= "<tr class=\"tablebluebottom\">";
	$x .= "<td class=\"tabletext\" align=\"right\">".$i_Payment_SchoolAccount_Total."</td>";
	$x .= "<td class=\"tabletext\">$".number_format(sprintf("%.2f",$sum_amount),2)."</td>";
	$x .= "</tr>";
	
	$x .= '</table>'."\n";
	
}else if($YearID != ''){
	$libyear = new Year($YearID);
		
	$year_name = htmlspecialchars($libyear->YearName,ENT_QUOTES);
	
	$x = '<div class="navigation" style="width:96%;">
			<img height="15" width="15" align="absmiddle" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/nav_arrow.gif"/>';
		$x.="<span><a href=\"javascript:SubmitForm('','');\">".$Lang['ePayment']['YearForm']."</a></span>";
		$x.='<img height="15" width="15" align="absmiddle" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/nav_arrow.gif"/>';
		$x.= '<span>'.$year_name.'</span>';
	$x.= '</div><br />';
	
	$x.="<table width=\"96%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\" align=\"center\">";
	// Class Name
	$x.="<tr>";
	$x.="<td class=\"tablebluetop tabletopnolink\" width=\"50%\">".$Lang['StudentAttendance']['ClassName']."</td>";
	// Amount 
	$x.="<td class=\"tablebluetop tabletopnolink\" width=\"50%\">".$i_Payment_Field_SinglePurchase_TotalAmount."</td>";
	$x.="</tr>";
	
	$sum_amount = 0;
	
	// Get classes by YearID		
	$class_list = $fcm->Get_Class_List_By_YearID($YearID);
	for($j=0;$j<count($class_list);$j++){
		list($year_class_id, $class_title_en, $class_title_ch) = $class_list[$j];
		$class_name = trim(Get_Lang_Selection($class_title_ch,$class_title_en));
		$students = $fcm->Get_Student_By_Class($year_class_id);
		$student_userid = Get_Array_By_Key($students,'UserID');
		$user_cond = " AND a.StudentID IN (".(count($student_userid)>0?implode(",",$student_userid):"-1").")";
		
		$sql  = "SELECT
		             SUM(a.Amount-IF(t.Amount IS NOT NULL,t.Amount,0)) 
		         FROM 
		             PAYMENT_OVERALL_TRANSACTION_LOG as a 
					 LEFT JOIN PAYMENT_OVERALL_TRANSACTION_LOG as t ON t.TransactionType=11 AND t.StudentID=a.StudentID AND t.RefCode=a.RefCode 
					 LEFT JOIN INTRANET_USER as b ON a.StudentID = b.UserID
		             LEFT JOIN INTRANET_ARCHIVE_USER AS c ON a.StudentID = c.UserID
		         WHERE 
		              a.TransactionType = 3 AND a.Details = '$Item'
		              AND UNIX_TIMESTAMP(a.TransactionTime) BETWEEN '".$start_ts."' AND '".$end_ts."' 
		              $user_cond ";
		          
		$temp = $li->returnVector($sql);
		
		$amount = $temp[0];
		
		$sum_amount += $amount + 0;
		
		$css = $j%2==0?"tablebluerow1":"tablebluerow2";
		// Class Name
		$x.="<tr class=\"".$css."\">";
		$x.="<td class=\"".$css."\"><a class=\"tablelink\" href=\"javascript:SubmitForm('".$YearID."','".$year_class_id."')\">".$class_name."</a></td>";
		// Amount 
		$x.="<td class=\"".$css."\">$".number_format(sprintf("%.2f",$amount),2)."</td>";
		$x.="</tr>";
		
	}
	
	$x .= "<tr class=\"tablebluebottom\">";
	$x .= "<td class=\"tabletext\" align=\"right\">".$i_Payment_SchoolAccount_Total."</td>";
	$x .= "<td class=\"tabletext\">$".number_format(sprintf("%.2f",$sum_amount),2)."</td>";
	$x .= "</tr>";
	
	$x .= '</table>'."\n";
	
}else{
	$year_form_list = $fcm->Get_Form_List(false,1,$CurrentAcademicYearID);
	$numOfYearForm = count($year_form_list);
	
	$x="<table width=\"96%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\" align=\"center\">";
	// Year Form
	$x.="<tr>";
	$x.="<td class=\"tablebluetop tabletopnolink\" width=\"50%\">".$Lang['ePayment']['YearForm']."</td>";
	// Amount
	$x.="<td class=\"tablebluetop tabletopnolink\" width=\"50%\">".$i_Payment_Field_SinglePurchase_TotalAmount."</td>";
	$x.="</tr>";
	
	$sum_amount = 0;
	for($j=0;$j<$numOfYearForm;$j++)
	{
		$year_form_id = $year_form_list[$j]['YearID'];
		
		$libyear = new Year($year_form_id);
		$students = $fcm->Get_Student_By_Form($CurrentAcademicYearID,array($year_form_id));

		$id_list = '';
		$delimiter = '';
		for($i=0;$i<count($students);$i++){
			$id_list .= $delimiter.$students[$i]['UserID'];
			$delimiter = ',';
		}
		
		$display_name = htmlspecialchars($libyear->YearName,ENT_QUOTES);
		
		$user_cond = " AND a.StudentID IN (".$id_list.") ";
		
		$sql  = "SELECT
		             SUM(a.Amount-IF(t.Amount IS NOT NULL,t.Amount,0)) 
		         FROM 
		             PAYMENT_OVERALL_TRANSACTION_LOG as a 
					 LEFT JOIN PAYMENT_OVERALL_TRANSACTION_LOG as t ON t.TransactionType=11 AND t.StudentID=a.StudentID AND t.RefCode=a.RefCode 
					 LEFT JOIN INTRANET_USER as b ON a.StudentID = b.UserID
		             LEFT JOIN INTRANET_ARCHIVE_USER AS c ON a.StudentID = c.UserID
		         WHERE 
		              a.TransactionType = 3 AND a.Details = '$Item'
		              AND UNIX_TIMESTAMP(a.TransactionTime) BETWEEN '".$start_ts."' AND '".$end_ts."' 
		              $user_cond ";
		          
		$temp = $li->returnVector($sql);
		
		$amount = $temp[0];
		
		$sum_amount += $amount + 0;
		
		$css = $j%2==0?"tablebluerow1":"tablebluerow2";
		// Year Form Name
		$x.="<tr class=\"".$css."\">";
		$x.="<td class=\"".$css."\"><a class=\"tablelink\" href=\"javascript:SubmitForm('".$year_form_id."','')\">".$display_name."</a></td>";
		// Amount 
		$x.="<td class=\"".$css."\">$".number_format(sprintf("%.2f",$amount),2)."</td>";
		$x.="</tr>";
		
	}
	
	$x .= "<tr class=\"tablebluebottom\">";
	$x .= "<td class=\"tabletext\" align=\"right\">".$i_Payment_SchoolAccount_Total."</td>";
	$x .= "<td class=\"tabletext\">$".number_format(sprintf("%.2f",$sum_amount),2)."</td>";
	$x .= "</tr>";
	
	$x .= '</table>'."\n";
	
}

$linterface->LAYOUT_START();
?>
<style type='text/css' media='print'>
	 .print_hide {display:none;}
	  P.breakhere {page-break-before: always}
</style>

<script type="text/JavaScript" language="JavaScript">
function SubmitForm(YearID,ClassID)
{
	var formObj = document.getElementById('form1');
	var yearIdObj = document.getElementById('YearID');
	var classIdObj = document.getElementById('ClassID');
	
	yearIdObj.value = YearID;
	classIdObj.value = ClassID;
	formObj.submit();
}

function ExportPage(formObj)
{
	var oldTarget = formObj.target;
	var oldAction = formObj.action;
	formObj.action = 'class_detail_export.php';
	formObj.target = '_blank';
	formObj.submit();
	formObj.action = oldAction;
	formObj.target = oldTarget;
}
</script>
<?php
echo '<form id="form1" name="form1" method="post" action="class_detail.php">';
echo '<table width="96%" align="center" class="print_hide" border="0">
			<tr>';
		echo '<td>';
			echo '<div class="content_top_tool">
					<div class="Conntent_tool">';
			echo $linterface->GET_LNK_EXPORT("javascript:ExportPage(document.form1);","","","","",1);
			echo 	'</div>';
			echo '</div>';
		echo '</td>';
		echo '<td align="right">';
			echo $linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit2");
		echo '</td>
			</tr>
	  </table>';
echo "<br />";
echo $x;
echo "<table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\">";
echo "<tr>";
echo "<td class=\"eSportprinttext tabletext\" >&nbsp;</td><td class=\"eSportprinttext tabletext\" align=\"right\">$i_general_report_creation_time : ".date('Y-m-d H:i:s')."</td></tr></table>";
echo "<br />";	

echo '<input type="hidden" name="Item" value="'.$Item.'" />';
echo '<input type="hidden" name="FromDate" value="'.$FromDate.'" />';
echo '<input type="hidden" name="ToDate" value="'.$ToDate.'" />';
echo '<input type="hidden" id="YearID" name="YearID" value="'.$YearID.'" />';
echo '<input type="hidden" id="ClassID" name="ClassID" value="'.$ClassID.'" />';

echo '</form>';

$linterface->LAYOUT_STOP();
intranet_closedb();
?>