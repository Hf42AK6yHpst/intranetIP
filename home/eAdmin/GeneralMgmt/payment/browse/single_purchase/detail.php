<?php
// Editing by 
/*
 * 2017-03-22 (Carlos): Deduct voided transactions. 
 * 2015-02-26 (Carlos): Added export button.
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

if ($item == "")
{
    header("Location: index.php");
    exit();
}

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
# preserve table view
if ($ck_data_log_browsing_single_record_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_data_log_browsing_single_record_page_number", $pageNo, 0, "", "", 0);
	$ck_data_log_browsing_single_record_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_data_log_browsing_single_record_page_number!="")
{
	$pageNo = $ck_data_log_browsing_single_record_page_number;
}

if ($ck_data_log_browsing_single_record_page_order!=$order && $order!="")
{
	setcookie("ck_data_log_browsing_single_record_page_order", $order, 0, "", "", 0);
	$ck_data_log_browsing_single_record_page_order = $order;
} else if (!isset($order) && $ck_data_log_browsing_single_record_page_order!="")
{
	$order = $ck_data_log_browsing_single_record_page_order;
}

if ($ck_data_log_browsing_single_record_page_field!=$field && $field!="")
{
	setcookie("ck_data_log_browsing_single_record_page_field", $field, 0, "", "", 0);
	$ck_data_log_browsing_single_record_page_field = $field;
} else if (!isset($field) && $ck_data_log_browsing_single_record_page_field!="")
{
	$field = $ck_data_log_browsing_single_record_page_field;
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "DataLogBrowsing_SingleRecord";

$linterface = new interface_html();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

/*
$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 4;
*/

$namefield = getNameFieldByLang("b.");
if ($start == "" || $end == "") # Default current month
{
    $start = date("Y-m-d",mktime(0,0,0,date('m'),1,date('Y')));
    $end = date("Y-m-d",mktime(0,0,0,date('m')+1,-1,date('Y')));
}

$item = urldecode($item);

$start_ts = strtotime($start);
$end_ts = strtotime($end) + 60*60*24 - 1;
if ($ClassName != "")
{
    $conds .= "AND (b.ClassName = '$ClassName' OR c.ClassName='$ClassName')";
}

if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$archive_namefield = "c.ChineseName";
}else $archive_namefield = "c.EnglishName";

switch($user_type){
	case 1: $user_cond = " AND (b.RecordType=1 OR (b.RecordType IS NULL AND c.RecordType IS NULL))"; break;
	case 2: $user_cond = " AND (b.RecordType=2 OR c.RecordType=2)"; break;
	default : $user_cond = ""; break;
}

$sql  = "SELECT
				IF((b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), IF(b.UserID IS NULL AND c.UserID IS NULL,'<font color=red>*</font>',$namefield)),
				IF((b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<i>',c.ClassName,'</i>'),IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassName)),
				IF((b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<i>',c.ClassNumber,'</i>'),IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassNumber)),
                IF((b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<i>',c.CardID,'</i>'), IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.CardID)),
               CONCAT('$', FORMAT(a.Amount-IF(t.Amount IS NOT NULL,t.Amount,0), 2)) AS Amount,
               DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i')
         FROM
             PAYMENT_OVERALL_TRANSACTION_LOG as a 
			 LEFT JOIN PAYMENT_OVERALL_TRANSACTION_LOG as t ON t.TransactionType=11 AND t.StudentID=a.StudentID AND t.RefCode=a.RefCode 
			 LEFT JOIN INTRANET_USER as b ON a.StudentID = b.UserID 
             LEFT JOIN INTRANET_ARCHIVE_USER AS c ON a.StudentID = c.UserID 
         WHERE
              a.TransactionType = 3 AND TRIM(REPLACE(a.Details,'<br>','')) = '".trim(str_replace(array("<br>","\n"),array("",""),$item))."'
              AND UNIX_TIMESTAMP(a.TransactionTime) BETWEEN $start_ts AND $end_ts
              $conds
              $user_cond
               AND ( 
              if(b.UserID IS NOT NULL,$namefield LIKE '%$keyword%',IF(c.UserID IS NOT NULL,$archive_namefield LIKE '%$keyword%','$keyword'=''))
              )                              ";
# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("b.EnglishName,c.EnglishName","b.ClassName,c.ClassName","b.ClassNumber,c.ClassNumber","b.CardID,c.CardID","a.Amount","a.TransactionTime");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0);
$li->IsColOff = 2;
if ($field=="" && $order=="") {
	$li->field = 4;
	$li->order = 0;
}

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width='1' class='tablebluetop tabletoplink'>#</td>\n";
$li->column_list .= "<td width='20%' class='tablebluetop tabletoplink'>".$li->column($pos++, $i_Payment_Field_Username)."</td>\n";
$li->column_list .= "<td width='15%' class='tablebluetop tabletoplink'>".$li->column($pos++, $i_UserClassName)."</td>\n";
$li->column_list .= "<td width='15%' class='tablebluetop tabletoplink'>".$li->column($pos++, $i_UserClassNumber)."</td>\n";
$li->column_list .= "<td width='15%' class='tablebluetop tabletoplink'>".$li->column($pos++, $i_SmartCard_CardID)."</td>\n";
$li->column_list .= "<td width='15%' class='tablebluetop tabletoplink'>".$li->column($pos++, $i_Payment_Field_Amount)."</td>\n";
$li->column_list .= "<td width='20%' class='tablebluetop tabletoplink'>".$li->column($pos++, $i_Payment_Field_TransactionTime)."</td>\n";

## select user
$array_user_name = array($i_Payment_Students, $i_general_Teacher, $i_Payment_All);
$array_user_data = array("2", "1", "");
$select_user = getSelectByValueDiffName($array_user_data,$array_user_name,"name='user_type' onChange='filterByUser(this.form);'",$user_type,0,1);


$lclass = new libclass();
$temp = $button_select;
$button_select = $i_status_all;
$select_class = $lclass->getSelectClass("name=ClassName onChange=''".($user_type==2?"":"style='visibility=hidden' disabled=true"),$ClassName);
$button_select = $temp;

$toolbar2 = $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
$toolbar2.= $linterface->GET_LNK_EXPORT("javascript:ExportPage(document.form1);","","","","",0);

$searchbar = "<input class=\"textboxnum\" type=\"text\" name=\"keyword\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= $linterface->GET_SMALL_BTN($button_find, "button", "document.form1.submit();","submit2");

$toolbar = "<table width=\"96%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\" align=\"center\">
				<tr>
					<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Profile_From</td>
					<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"70%\"><input class=\"textboxnum\" type=\"text\" name=\"start\" value=\"$start\">
					 (YYYY-MM-DD)</td>
				</tr>
				<tr>
					<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Profile_To</td>
					<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"70%\"><input class=\"textboxnum\" type=\"text\" name=\"end\" value=\"$end\">
					 (YYYY-MM-DD)</td>
				</tr>
				<tr>
					<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Payment_UserType</td>
					<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"70%\">$select_user $select_class</td>
				</tr>
				<tr>
					<td colspan=\"2\" class=\"dotline\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"1\" /></td>
				</tr>
				<tr>
					<td colspan=\"2\" align=\"center\">
						".$linterface->GET_ACTION_BTN($button_view, "button", "document.form1.submit()")." ".
						$linterface->GET_ACTION_BTN($button_back, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")."
					</td>
				</tr>
			</table>";

$TAGS_OBJ[] = array($i_Payment_Menu_Browse_PurchasingRecords, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($item);

?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.css" type="text/css" />
<script language="Javascript">
function openPrintPage()
{
	newWindow('detail_print.php?user_type=<?=$user_type?>&item=<?=$item?>&ClassName=<?=$ClassName?>&start=<?=$start?>&end=<?=$end?>&keyword=<?=$keyword?>&field=<?=$li->field?>&order=<?=$order?>',10);
}

function filterByUser(formObj){
	
	if(formObj==null) return;
	
	classNameObj = formObj.ClassName;
	userTypeObj = formObj.user_type;
	if(classNameObj==null || userTypeObj ==null) return;
	v = userTypeObj.options[userTypeObj.selectedIndex].value;
	if(v!=2){
		classNameObj.options[0].selected = true;
		classNameObj.disabled = true;
		classNameObj.style.visibility = 'hidden';
	}else{
		classNameObj.disabled = false;
		classNameObj.style.visibility = 'visible';
	}
}

function ExportPage(formObj)
{
	var oldTarget = formObj.target;
	var oldAction = formObj.action;
	formObj.action = 'detail_export.php';
	formObj.target = '_blank';
	formObj.submit();
	formObj.action = oldAction;
	formObj.target = oldTarget;
}

$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
$(document).ready(function(){ 
	$('input#start').datepick({
		dateFormat: 'yy-mm-dd',
		dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		changeFirstDay: false,
		firstDay: 0
		});
	$('input#end').datepick({
		dateFormat: 'yy-mm-dd',
		dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		changeFirstDay: false,
		firstDay: 0
		});
});
</script>
<br />
<form name="form1" method="get">
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
</table>
<?=$toolbar?>
<br />
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar2?></td>
	</tr>
	<tr>
		<td align="left"><?=$searchbar?></td>
	</tr>
</table>
<?php echo $li->displayFormat2("96%", "blue"); ?>

<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td class="tabletextremark" colspan="2" align="left"><?=$i_Payment_Note_StudentRemoved?><br /></td>
	</tr>
</table>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
<input type="hidden" name="item" value="<?=$item?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
