<?php
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<br />
<table align="center" width="90%" border="0" cellpadding="5" cellspacing="0" >
	<tr valign="top">
		<td class="tabletext" align="center"><?=$i_Sports_Select_Menu?> </td>
	</tr>
</table>
<br /> 
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>