<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();

$ldb = new libdb();
$lclass = new libclass();

$target = $_REQUEST['target']; // form | class | student | student2ndLayer
$fieldId = $_REQUEST['fieldId']; // <select> id
$fieldName = $_REQUEST['fieldName']; // <select> name
$academicYearId = $_REQUEST['academicYearId'];
$studentFieldId = $_REQUEST['studentFieldId']; // student <select> id
$studentFieldName = $_REQUEST['studentFieldName']; // student <select> name
//$yearClassIdAry = (array)$_REQUEST['YearClassID']; // use to select students if target=class  

if($target=='form') {
	
	//$temp = $lclass->getSelectLevel("name=\"$fieldName\" id=\"$fieldId\" class=\"formtextbox\" multiple size=\"5\" ", $TargetClassLevel, false);
	$temp = '<select name="'.$fieldName.'" id="'.$fieldId.'" class="formtextbox" multiple size="5" >';
	$ClassLvlArr = $lclass->getLevelArray();
	
	for ($i = 0; $i < sizeof($ClassLvlArr); $i++) {
		$temp .= '<option value="'.$ClassLvlArr[$i][0].'"';
		$temp .= ' selected';
		$temp .= '>'.$ClassLvlArr[$i][1].'</option>'."\n";
	}
	$temp .= '</select>'."\n";
	
}

if($target=="class") {
	
	$temp = '<select name="'.$fieldName.'" id="'.$fieldId.'" class="formtextbox" multiple size="5" >';
	
	$classResult = $lclass->getClassList($academicYearId);
	
	for($k=0;$k<sizeof($classResult);$k++) {
		$temp .= '<option value="'.$classResult[$k][0].'" selected';
		$temp .= '>'.$classResult[$k][1].'</option>';
	}
	$temp .= '</select>'."\n";
}

if($target=="student") {
	
	$temp = '<select name="'.$fieldName.'" id="'.$fieldId.'" class="formtextbox" multiple size="5" onchange="'.$onchange.'">';
	
	$classResult = $lclass->getClassList($academicYearId);
			
	for($k=0;$k<sizeof($classResult);$k++) {
		$temp .= '<option value="'.$classResult[$k][0].'" selected';
		$temp .= '>'.$classResult[$k][1].'</option>';
	}
	$temp .= '</select>'."\n";
	$temp .= '<div id="'.$divStudentSelection.'"></div>'."\n";
}


if($target=="student2ndLayer") {
	$name_field = getNameFieldByLang("u.");
	$yearClassIdAry = (array)$_REQUEST['YearClassID'];
	$temp .= '<br />';
	$temp .= '<select name="'.$studentFieldName.'" id="'.$studentFieldId.'" class="formtextbox" multiple size="5">';
	
	$clsName = ($intranet_session_language=="en") ? "yc.ClassTitleEN" : "yc.ClassTitleB5";
	$sql = "SELECT $clsName, ycu.ClassNumber, $name_field, u.UserID 
			FROM INTRANET_USER as u
			INNER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=u.UserID) 
			INNER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) 
			INNER JOIN YEAR as y ON y.YearID=yc.YearID 
			WHERE yc.YearClassID IN ('".implode("','",$yearClassIdAry)."') AND u.RecordType=2 
			ORDER BY y.Sequence,yc.Sequence,ycu.ClassNumber 
			";
	
	$studentResult = $ldb->returnArray($sql, 3);

	for($k=0;$k<sizeof($studentResult);$k++) {
		$temp .= '<option value="'.$studentResult[$k][3].'" selected';
		$temp .= '>'.$studentResult[$k][0].'-'.$studentResult[$k][1].' '.$studentResult[$k][2].'</option>';
	}
	
	$temp .= '</select>'."\n";
	//$temp .= '<br />';
	
	$temp .= $linterface->GET_BTN($Lang['Btn']['SelectAll'], "button", "Select_All_Options('$studentFieldId', true);return false;");
}	

echo $temp;

intranet_closedb();
?>