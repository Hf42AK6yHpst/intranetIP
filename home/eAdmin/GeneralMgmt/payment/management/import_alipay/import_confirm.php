<?php
// Editing by 
/*
 * 2019-10-29 (Carlos): [!!!Require schema update!!!] Added settlement time to imported records.
 * 2019-03-06 (Carlos): Moved prepare temp table records code to libpayment.php importPrepareAlipayTransactionRecordsToTempTable().
 * 2018-11-21 (Henry): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !($lpayment->isAlipayDirectPayEnabled() || $lpayment->isAlipayTopUpEnabled())) {
	intranet_closedb();
	header ("Location: /");
	exit();
}


$limport = new libimporttext();
$lf = new libfilesystem();


$format_array = array("Partner_transaction_id","Transaction_id","Amount","Fee","Settlement","Currency","Payment_time","Settlement_time","Issue","Product","Type","Status","Remarks","Original_partner_transaction_ID");
$column_size = count($format_array);
$exclude_fields = array(5,8,9,12,13); // "Order Amount" and "Merchant Dollar payment"
$filepath = $_FILES['uploadfile']['tmp_name'];
$filename = $_FILES['uploadfile']['name'];

if($filepath=="none" || $filepath == "")
{
	# import failed
    intranet_closedb();
    $_SESSION['PAYMENT_IMPORT_ALIPAY_RETURN_MSG'] = $Lang['General']['ReturnMessage']['ImportUnsuccess'];
    header("Location:index.php");
    exit();
}

$ext = strtoupper($lf->file_ext($filename));
if($limport->CHECK_FILE_EXT($filename))
{
  # read file into array
  # return 0 if fail, return csv array if success
  //$data = $lf->file_read_csv($filepath);
  //$data = $limport->GET_IMPORT_TXT($filepath);
  
  $data = array();
  $fp = fopen($filepath,"r");
  if($fp)
  {
  	while (!feof($fp))
    {
       //$line = trim(fgets($fp));
       $row = fgetcsv($fp,1000,',');
       if(count($row) != $column_size) continue;
       //if($line == '') continue;
       //$row = explode(",",$line);
       $data[] = $row;
    }
    fclose($fp);
  }
  
  if(sizeof($data)>0)
  {
  		$toprow = array_shift($data);                   # drop the title bar
  }else
  {
  		$_SESSION['PAYMENT_IMPORT_ALIPAY_RETURN_MSG'] = $Lang['General']['ReturnMessage']['ImportUnsuccess'];
	    intranet_closedb();
	    header("Location: index.php");
	    exit();
  }
}
for ($i=0; $i<sizeof($format_array); $i++)
{
	 if ($toprow[$i] != $format_array[$i])
	 {
	 	 $_SESSION['PAYMENT_IMPORT_ALIPAY_RETURN_MSG'] = $Lang['General']['ReturnMessage']['ImportUnsuccess'];
	     intranet_closedb();
	     header("Location: index.php");
	     exit();
	 }
}

$data_size = count($data);
$column_size = count($data[0]);
$cur_sessionid = md5(session_id());
$cur_time = date("Y-m-d H:i:s");
$threshold_date = date("Y-m-d H:i:s", strtotime("-1 day"));

$imported_temp_records_success = $lpayment->importPrepareAlipayTransactionRecordsToTempTable($data, $cur_sessionid, $threshold_date, $cur_time, $exclude_fields);

/*
$sql = "CREATE TABLE IF NOT EXISTS PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS (
			RecordID int(11) NOT NULL auto_increment,
			SessionID varchar(300) NOT NULL,
			ImportTime datetime NOT NULL,
			RecordNumber int(11),
			PaymentChannel varchar(255),
			PaymentDateTime datetime,
			OrderNo varchar(255),
			TNGRefNo varchar(255),
			PaymentType varchar(255),
			PaymentAmount decimal(13,2),
			RebateAmount decimal(13,2),
			NetPaymentAmount decimal(13,2),
			Remarks TEXT,
			TransactionRecordID int(11) DEFAULT NULL COMMENT 'PAYMENT_TNG_TRANSACTION.RecordID',
			PaymentID int(11) DEFAULT NULL, 
			PRIMARY KEY(RecordID),
			INDEX IdxSessionID(SessionID),
			INDEX IdxImportTime(ImportTime),
			INDEX IdxTNGRefNo(TNGRefNo) 
		)ENGINE=InnoDB DEFAULT CHARSET=utf8";

$lpayment->db_db_query($sql);

$sql = "ALTER TABLE PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS ADD COLUMN QueueNo varchar(255) DEFAULT NULL AFTER OrderNo";
$lpayment->db_db_query($sql);

$sql = "ALTER TABLE PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS ADD COLUMN TNGNo varchar(255) DEFAULT NULL AFTER TNGRefNo";
$lpayment->db_db_query($sql);

$threshold_date = date("Y-m-d H:i:s", strtotime("-1 day"));
$sql = "DELETE FROM PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS WHERE SessionID='$cur_sessionid' OR ImportTime>'$threshold_date'";
$lpayment->db_db_query($sql);

$insert_values = array();
for($i=0;$i<$data_size;$i++){
	
	$insert_row = "('$cur_sessionid','$cur_time'";
	for($j=0;$j<$column_size;$j++){
		if(in_array($j,$exclude_fields)){
			continue;
		}
		$val = $data[$i][$j];
		if($j == 2){
			$val = abs($val);
		}
		if($j == 3){
			$val = 0;
			if($data[$i][2] < 0){
				$val = $data[$i][2];
			}
		}
		if($j == 6){ // payment date time
			if(preg_match('/^(\d{1,2})\/(\d{1,2})\/(\d\d\d\d)\s(\d{1,2}):(\d{1,2})(:\d{1,2})*$/',$val,$matches)){
				$val = sprintf('%04d-%02d-%02d %02d:%02d:%02d', $matches[3], $matches[2], $matches[1], $matches[4], $matches[5], str_replace(':','',$matches[6]));
			}
			$val = date("Y-m-d H:i:s", strtotime($val));
		}else if($j == 2 || $j == 3 || $j == 4){ // dollar amount
			$val = str_replace(array('$','(',')',',','"','-'),'',$val);
		}
		$insert_row .= ",'".$val."'";
	}
	$insert_row .= ")";
	$insert_values[] = $insert_row;
}

$chunks = array_chunk($insert_values,500);
//$sql = "INSERT INTO PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS (SessionID,ImportTime,RecordNumber,PaymentChannel,PaymentDateTime,OrderNo,TNGRefNo,PaymentType,PaymentAmount,RebateAmount,NetPaymentAmount,Remarks) VALUES ";
$sql = "INSERT INTO PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS (SessionID,ImportTime,OrderNo,TNGRefNo,PaymentAmount,RebateAmount,NetPaymentAmount,PaymentDateTime,PaymentType) VALUES ";
for($i=0;$i<count($chunks);$i++){
	$insert_sql = $sql.implode(",", $chunks[$i]);
	$lpayment->db_db_query($insert_sql);
//	debug_pr($insert_sql);
}

*/

$student_name_field = getNameFieldWithClassNumberByLang("u1.");
//$archived_student_name_field = getNameFieldWithClassNumberByLang("au1.");
$archived_student_name_field = getNameFieldByLang2("au1.");
$payer_name_field = getNameFieldByLang2("u2.");
$archived_payer_name_field = getNameFieldByLang2("au2.");
$top_up_student_name_field = getNameFieldWithClassNumberByLang("u3.");
$top_up_archived_student_name_field = getNameFieldByLang2("au3.");

$sql = "SELECT 
			r.RecordID,
			r.OrderNo,
			r.TNGRefNo,
			r.PaymentAmount,
			r.RebateAmount,
			r.NetPaymentAmount,
			r.PaymentDateTime,
			r.SettlementTime,
			r.PaymentType,
			r.SettleStatus,
			t.RecordID as TNGRecordID,
			t.PaymentID as TNGPaymentID,
			s.RecordStatus as PaidStatus,
			IF(t.PaymentID = -1,'".$i_Payment_TransactionType_Credit."',i.Name) as ItemName,
			IF(u1.UserID IS NOT NULL,$student_name_field,IF(au1.UserID IS NOT NULL,CONCAT('<span class=\"red\">*</span>',$archived_student_name_field), IF(u3.UserID IS NOT NULL,$top_up_student_name_field,CONCAT('<span class=\"red\">*</span>',$top_up_archived_student_name_field)))) as StudentName,
			t.OrderNo as TNGOrderNo,
			t.SpTxNo,
			".$lpayment->getWebDisplayAmountFormatDB("t.NetPaymentAmount")." as TNGPaymentAmount,
			t.PaymentStatus,
			t.ChargeStatus,
			DATE_FORMAT(t.InputDate,'%Y-%m-%d %H:%i:%s') as InputDate,
			IF(u2.UserID IS NULL AND au2.UserID IS NULL,'".$Lang['General']['EmptySymbol']."',IF(u2.UserID IS NOT NULL,$payer_name_field,CONCAT('<span class=\"red\">*</span>',$archived_payer_name_field))) as PayerName 
		FROM PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS as r 
		LEFT JOIN PAYMENT_TNG_TRANSACTION as t ON t.SpTxNo=r.TNGRefNo 
		LEFT JOIN PAYMENT_CREDIT_TRANSACTION as c ON c.TransactionID=t.CreditTransactionID 
		LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT as s ON s.PaymentID=t.PaymentID 
		LEFT JOIN PAYMENT_PAYMENT_ITEM as i ON i.ItemID=s.ItemID 
		LEFT JOIN INTRANET_USER as u1 ON u1.UserID=s.StudentID 
		LEFT JOIN INTRANET_ARCHIVE_USER as au1 ON au1.UserID=s.StudentID 
		LEFT JOIN INTRANET_USER as u2 ON u2.UserID=t.PayerUserID 
		LEFT JOIN INTRANET_ARCHIVE_USER as au2 ON au2.UserID=t.PayerUserID 
		LEFT JOIN INTRANET_USER as u3 ON u3.UserID=c.StudentID 
		LEFT JOIN INTRANET_ARCHIVE_USER as au3 ON au3.UserID=c.StudentID 
		WHERE r.SessionID='$cur_sessionid' 
		ORDER BY r.RecordNumber ";
$records = $lpayment->returnArray($sql);
$record_size = count($records);
//debug_pr($records);
$errorsAry = array();
$ignoredAry = array();
$import_count = 0;

$col_count = count($format_array);
$col_width = round(100 / ($col_count - 1 + 7));
$x = '<table class="common_table_list_v30" width="100%">';
$x .= '<thead>
			<tr>
				<th class="num_check sub_row_top" style="font-weight:bold;">'.$format_array[0].'</th>';
		for($i=1;$i<$col_count;$i++)
		{
			if(in_array($i,$exclude_fields)){
				continue;
			}
			if($i == 3){
				$format_array[$i] ='Rebate From Merchant';
			}
			$x .= '<th class="sub_row_top" width="'.$col_width.'%" style="font-weight:bold;">'.$format_array[$i].'</th>';
		}
		$x .= '<th width="'.$col_width.'%">'.$Lang['ePayment']['PaymentItem'].'</th>';
		$x .= '<th width="'.$col_width.'%">'.$Lang['Identity']['Student'].'</th>';
		$x .= '<th width="'.$col_width.'%">'.$Lang['ePayment']['PaidBy'].'</th>';
		$x .= '<th width="'.$col_width.'%">'.$Lang['ePayment']['PaymentStatus'].'</th>';
		$x .= '<th width="'.$col_width.'%">'.$Lang['ePayment']['ChargeStatus'].'</th>';
		$x .= '<th width="'.$col_width.'%">'.$Lang['General']['Error'].'</th>';
		$x .= '<th width="'.$col_width.'%">&nbsp;</th>';
	$x .= '</tr>
		</thead>';
$x .= '<tbody>';
for($i=0;$i<$record_size;$i++)
{
	$x .= '<tr>';
	for($j=1;$j<=9;$j++)
	{
		if($j == 3 || $j == 4 || $j == 5)
		{
			$x .= '<td>'.$lpayment->getWebDisplayAmountFormat($records[$i][$j]).'</td>';
		}else{
			$x .= '<td>'.$records[$i][$j].'</td>';
		}
	}
	//$charge_status = ($records[$i]['ChargeStatus']=='1' || in_array($records[$i]['PaymentType'],array('1','Success','success'))? '<span style="color:green">'.$Lang['ePayment']['Success'] : ($records[$i]['ChargeStatus']=='2'? '<span style="color:blue">'.$Lang['ePayment']['Pending'] : ($records[$i]['ChargeStatus']=='3' || in_array($records[$i]['PaymentType'],array('3','Void','void'))? '<span style="color:orange">'.$Lang['ePayment']['Voided'] : '<span style="color:red">'.$Lang['ePayment']['Fail']))).'</span>';
	if($records[$i]['ChargeStatus'] == '4'){
		$charge_status = '<span style="color:purple">'.$Lang['ePayment']['Refunded'].'</span>';
	}else if($records[$i]['ChargeStatus']=='1' || in_array($records[$i]['SettleStatus'],array('L'))){
		$charge_status = '<span style="color:green">'.$Lang['ePayment']['Success'].'</span>';
	}else if($records[$i]['ChargeStatus']=='2' || in_array($records[$i]['SettleStatus'],array('P'))){
		$charge_status = '<span style="color:blue">'.$Lang['ePayment']['Pending'].'</span>';
	}else if($records[$i]['ChargeStatus']=='3' || in_array($records[$i]['PaymentType'],array('3','Void','void'))){
		$charge_status = '<span style="color:orange">'.$Lang['ePayment']['Voided'].'</span>';
	}else{
		$charge_status = '<span style="color:red">'.$Lang['ePayment']['Fail'].'</span>';
	}
	if($records[$i]['TNGRecordID'] == ''){
		// no callback TNG transaction record
		$item_name = $Lang['General']['EmptySymbol'];
		$student_name = $Lang['General']['EmptySymbol'];
		$payer_name = $Lang['General']['EmptySymbol'];
		$payment_status = '<span style="color:red">'.$Lang['General']['NotPaid'].'</span>';
		
		$error_msg = $Lang['General']['EmptySymbol'];
		$action_msg = $Lang['General']['EmptySymbol'];
		$remark_parts = explode("#=#", $records[$i]['Remarks']);
		if($records[$i]['Remarks'] != '' && count($remark_parts)==3){
			// new csv do not have remark, this part of code should not be run !!!
			$payment_id_from_remark = $remark_parts[1];
			$sql = "SELECT s.PaymentID,s.ItemID,s.StudentID,$student_name_field as StudentName,s.Amount,IF(s.PaymentID = -1,'".$i_Payment_TransactionType_Credit."',i.Name) as ItemName,s.RecordStatus,s.PaidTime 
					FROM PAYMENT_PAYMENT_ITEMSTUDENT as s 
					INNER JOIN PAYMENT_PAYMENT_ITEM as i ON i.ItemID=s.ItemID 
					INNER JOIN INTRANET_USER as u1 ON u1.UserID=s.StudentID 
					WHERE s.PaymentID='$payment_id_from_remark' ";
			$payment_student_record = $lpayment->returnResultSet($sql);
			if(count($payment_student_record)==1){
				$item_name = $payment_student_record[0]['ItemName'];
				$student_name = $payment_student_record[0]['StudentName'];
				$payer_name = $Lang['General']['EmptySymbol'];
				if($payment_student_record[0]['RecordStatus']==1){
					// the payment item was already paid
					$payment_status = '<span style="color:green">'.$Lang['General']['Paid'].'</span>';
					$error_msg = $Lang['General']['EmptySymbol'];
				}else{
					// the payment item was not paid yet
					$payment_status = '<span style="color:red">'.$Lang['General']['NotPaid'].'</span>';
					$sql = "UPDATE PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS SET PaymentID='".$payment_student_record[0]['PaymentID']."' WHERE RecordID='".$records[$i]['RecordID']."'";
					$lpayment->db_db_query($sql);
					$action_msg = $Lang['ePayment']['ProceedToNextStepToAddTNGRecord'];
				}
				$import_count += 1;
			}else{
				$errorsAry[] = $records[$i];
				$error_msg = '<span style="color:red">'.$Lang['ePayment']['InvalidTNGTransactionRecordAndItemNotFound'].'</span>';
			}
		}else{
			if(in_array($records[$i]['PaymentType'],array('3','Void','void'))){
				// if no PaymentID and voided, remove it from import record and skip it, but do not count as error
				$sql = "DELETE FROM PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS WHERE RecordID='".$records[$i]['RecordID']."'";
				$lpayment->db_db_query($sql);
				$ignoredAry[]  =$records[$i];
			}else{
				$errorsAry[] = $records[$i];
			}
			$error_msg = '<span style="color:red">'.$Lang['ePayment']['InvalidTNGTransactionRecordAndItemNotFound'].'</span>';
		}
		
	}else if(!in_array($records[$i]['PaymentStatus'],array('1',1,'Success','success'))){
		// TNG transaction fail and not added to ePayment yet
		$item_name = $records[$i]['ItemName'];
		$student_name = $records[$i]['StudentName'];
		$payer_name = $records[$i]['PayerName'];
		$payment_status = '<span style="color:red">'.$Lang['General']['NotPaid'].'</span>';
		$error_msg = $Lang['General']['EmptySymbol'];
		$action_msg = $Lang['ePayment']['ProceedToNextStepToAddTNGRecord'];
		$sql = "UPDATE PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS SET TransactionRecordID='".$records[$i]['TNGRecordID']."',PaymentID='".$records[$i]['TNGPaymentID']."' WHERE RecordID='".$records[$i]['RecordID']."'";
		$lpayment->db_db_query($sql);
		$import_count += 1;
	}else{
		// the TNG transaction was added to ePayment successfully
		$item_name = $records[$i]['ItemName'];
		$student_name = $records[$i]['StudentName'];
		$payer_name = $records[$i]['PayerName'];
		$payment_status = '<span style="color:green">'.$Lang['General']['Paid'].'</span>';
		$error_msg = $Lang['General']['EmptySymbol'];
		if($records[$i]['PaidStatus']!=1 || $records[$i]['ChargeStatus']=='2'){ // TNG has paid but pending to sync with ePayment or payment item has been paid but not mark TNG record as paid
			$action_msg = $Lang['ePayment']['ProceedToNextStepToAddTNGRecord'];
			$import_count += 1;
		}else{
			$action_msg = $Lang['ePayment']['TNGRecordWasProcessed'];
		}
		$sql = "UPDATE PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS SET TransactionRecordID='".$records[$i]['TNGRecordID']."',PaymentID='".$records[$i]['TNGPaymentID']."' WHERE RecordID='".$records[$i]['RecordID']."'";
		$lpayment->db_db_query($sql);
		//debug_pr($sql);
	}
	$x .= '<td>'.$item_name.'</td>';
	$x .= '<td>'.$student_name.'</td>';
	$x .= '<td>'.$payer_name.'</td>';
	$x .= '<td>'.$payment_status.'</td>';
	$x .= '<td>'.$charge_status.'</td>';
	$x .= '<td>'.$error_msg.'</td>';
	$x .= '<td>'.$action_msg.'</td>';
	$x .= '</tr>';
}
$x .= '</tbody>';
$x .= '</table>';

$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "ImportAlipayTransactionRecords";

$linterface = new interface_html();

$TAGS_OBJ[] = array($Lang['ePayment']['ImportAlipayTransactionRecords'],"",0);

# step information
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['SelectCSVFile'], 0);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['CSVConfirmation'], 1);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['ImportResult'], 0);

if(isset($_SESSION['PAYMENT_IMPORT_ALIPAY_RETURN_MSG']))
{
	$Msg = $_SESSION['PAYMENT_IMPORT_ALIPAY_RETURN_MSG'];
	unset($_SESSION['PAYMENT_IMPORT_ALIPAY_RETURN_MSG']);
}
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Msg);
//debug_pr($records);
?>
<br/>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
<div align="center">
<table width="90%" border="0" cellpadding="3" cellspacing="0">
	<tr>
		<td class="formfieldtitle" width="30%" align="left"><?=$Lang['General']['TotalRecord']?></td>
		<td class="tabletext"><?=$data_size?></td>
	</tr>
	<tr>
		<td class="formfieldtitle" width="30%" align="left"><?=$Lang['General']['SuccessfulRecord']?></td>
		<td class="tabletext"><?=($data_size - sizeof($errorsAry) - sizeof($ignoredAry))?></td>
	</tr>
	<tr>
		<td class="formfieldtitle" width="30%" align="left"><?=$Lang['General']['FailureRecord']?></td>
		<td class="tabletext"><?=count($errorsAry)?></td>
	</tr>
</table>
<?=$x?>
<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td class="dotline"><img src="/images/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" /></td>
	</tr>
	<tr>
		<td align="center">
<?php
	if (sizeof($errorsAry) == 0 && $import_count >0)
		echo $linterface->GET_ACTION_BTN($Lang['Btn']['Import'], "button", "window.location='import_update.php';").'&nbsp;';
	echo $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='index.php';");
?>
		</td>
	</tr>
</table>
</div>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>