<?php
// Editing by 
/*
 * 2019-06-24 (Carlos): Cater top-up.
 * 2017-11-28 (Carlos): Do not change balance when pay or unpay payments.
 * 2017-09-27 (Carlos): Modified to adapt new csv format. Added [TNGNo], [RefundStatus]. Handle charge status [Refund].
 * 2017-01-13 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !($lpayment->isTNGDirectPayEnabled() || $lpayment->isTNGTopUpEnabled())) {
	intranet_closedb();
	header ("Location: /");
	exit();
}



$cur_sessionid = md5(session_id());

$rows_affected = 0;

$sql = "SELECT 
			r.*,
			t.RecordID as TNGRecordID,
			t.PaymentID as TNGPaymentID,
			s.ItemID,
			IF(t.PaymentID=-1,c.StudentID,s.StudentID) as StudentID,
			t.PaymentStatus,
			t.ChargeStatus,
			IF(t.PaymentID=-1,t.PaymentStatus,s.RecordStatus) as RecordStatus,
			c.TransactionID    
		FROM PAYMENT_TNG_TRANSACTION_IMPORT_RECORDS as r 
		LEFT JOIN PAYMENT_TNG_TRANSACTION as t ON t.SpTxNo=r.TNGRefNo 
		LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT as s ON s.PaymentID=r.PaymentID 
		LEFT JOIN PAYMENT_CREDIT_TRANSACTION as c ON c.TransactionID=t.CreditTransactionID AND t.PaymentID=-1 
		WHERE r.SessionID='$cur_sessionid' 
		ORDER BY r.RecordNumber ";
$records = $lpayment->returnResultSet($sql);
$record_size = count($records);
//debug_pr($sql);
$updated_by = isset($_SESSION['UserID'])? $_SESSION['UserID'] : 'NULL';
$resultAry = array();
for($i=0;$i<$record_size;$i++)
{
	//$parChargeStatus = in_array($records[$i]['PaymentType'],array('1',1,'Success','success'))?'1': (in_array($records[$i]['PaymentType'],array('3',3,'Void','void'))?'3':'0');
	if(in_array($records[$i]['PaymentType'],array('4',4))){
		$parChargeStatus = '4'; // refunded
	}else if(in_array($records[$i]['PaymentType'],array('1',1,'Success','success'))){
		$parChargeStatus = '1'; // success
	}else if(in_array($records[$i]['PaymentType'],array('3',3,'Void','void'))){
		$parChargeStatus = '3'; // voided
	}else{
		$parChargeStatus = '0'; // fail
	}
	if($records[$i]['TNGRecordID'] == ''){
		// insert a PAYMENT_TNG_TRANSACTION record 
		$parFromServer = $records[$i]['PaymentChannel'];
		$payment_id = $records[$i]['PaymentID'];
		$parSp = 'tng';
		$parPayerUserId = $records[$i]['StudentID'];
		$parSpTxNo = $records[$i]['TNGRefNo'];
		$parTNGNo = $records[$i]['TNGNo'];
		$parPaymentStatus = 0;
		$parRefundStatus = $records[$i]['RefundStatus'];
		$parUniqueCode = $records[$i]['OrderNo'];
		$payment_amount = $records[$i]['PaymentAmount'];
		$rebate_amount = $records[$i]['RebateAmount'];
		$net_payment_amount = $records[$i]['NetPaymentAmount'];
		
		$sql = "INSERT INTO PAYMENT_TNG_TRANSACTION (FromServer,PaymentID,Sp,PayerUserID,PaymentType,PaymentStatus,ChargeStatus,RefundStatus,OrderNo,SpTxNo,TNGNo,PaymentAmount,RebateAmount,NetPaymentAmount,ErrorCode,InputDate,ModifiedDate,UpdatedBy) 
				VALUES ('$parFromServer','$payment_id','$parSp','$parPayerUserId','','$parPaymentStatus','$parChargeStatus','$parRefundStatus','$parUniqueCode','$parTNGNo','$parSpTxNo','$payment_amount','$rebate_amount','$net_payment_amount',NULL,'".$records[$i]['PaymentDateTime']."',NOW(),$updated_by)";
		$success = $lpayment->db_db_query($sql);
		if($success){
			$tng_record_id = $lpayment->db_insert_id();
			$records[$i]['TNGRecordID'] = $tng_record_id;
		}
	}
	
	if($records[$i]['TNGRecordID'] != '')
	{
		// paid the payment item 
		if($records[$i]['TNGPaymentID']!='' && $records[$i]['RecordStatus']!=1 && $parChargeStatus != '3' && $parChargeStatus != '4'){
			$log_id = 0;
			if($records[$i]['TNGPaymentID'] > 0){ // pay payment item
				$log_id = $lpayment->Paid_Payment_Item($records[$i]['ItemID'],$records[$i]['StudentID'],$records[$i]['PaymentID'],$___AllowNegativeBalance=true,$___ReturnTransactionLogID=true,'',true);
			}else if($records[$i]['TNGPaymentID'] == -1){ // top-up
				if($records[$i]['TransactionID'] == ''){ // no credit transaction id
					$log_id = $this->addValue($net_payment_amount, $records[$i]['StudentID'], date("Y-m-d H:i:s"), '', 9, $records[$i]['TNGRecordID']);
				}
			}
			$sql = "UPDATE PAYMENT_TNG_TRANSACTION SET PaymentStatus='".($log_id?'1':'0')."',ChargeStatus='$parChargeStatus',TNGNo='".$records[$i]['TNGNo']."',ModifiedDate=NOW() WHERE RecordID='".$records[$i]['TNGRecordID']."'";
			$resultAry[] = $lpayment->db_db_query($sql);
		}else if($records[$i]['TNGPaymentID'] != '' && $parChargeStatus == '3') // voided
		{
			if($records[$i]['RecordStatus']==1) // paid, then void
			{
				if($records[$i]['TNGPaymentID'] > 0){ // undo payment item
					$resultAry[] = $lpayment->UnPaid_Payment_Item($records[$i]['ItemID'],$records[$i]['StudentID'],$DoNotChangeBalance=true);
				}else if($records[$i]['TNGPaymentID'] == -1 && $records[$i]['TransactionID'] != ''){ // undo Top-up
					$this->Cancel_Cash_Deposit($records[$i]['TransactionID']);
				}
			}
			$sql = "UPDATE PAYMENT_TNG_TRANSACTION SET ChargeStatus='$parChargeStatus',TNGNo='".$records[$i]['TNGNo']."',ModifiedDate=NOW() WHERE RecordID='".$records[$i]['TNGRecordID']."'";
			$resultAry[] = $lpayment->db_db_query($sql);
		}else if($parChargeStatus == '4'){ // refunded
			$sql = "UPDATE PAYMENT_TNG_TRANSACTION SET RefundStatus='$parRefundStatus',TNGNo='".$records[$i]['TNGNo']."',ModifiedDate=NOW() WHERE RecordID='".$records[$i]['TNGRecordID']."'";
			$resultAry[] = $lpayment->db_db_query($sql);
		}
		else if($records[$i]['TNGPaymentID']!='' && $records[$i]['RecordStatus']==1 /*&& $records[$i]['PaymentStatus']!=1*/){
			$sql = "UPDATE PAYMENT_TNG_TRANSACTION SET PaymentStatus='1',ChargeStatus='$parChargeStatus',TNGNo='".$records[$i]['TNGNo']."',ModifiedDate=NOW() WHERE RecordID='".$records[$i]['TNGRecordID']."'";
			$resultAry[] = $lpayment->db_db_query($sql);
		}
	}
}

$rows_affected = count($resultAry);

$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "ImportTNGTransactionRecords";

$linterface = new interface_html();

$TAGS_OBJ[] = array($Lang['ePayment']['ImportTNGTransactionRecords'],"",0);

# step information
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['SelectCSVFile'], 0);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['CSVConfirmation'], 0);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['ImportResult'], 1);

if(isset($_SESSION['PAYMENT_IMPORT_TNG_RETURN_MSG']))
{
	$Msg = $_SESSION['PAYMENT_IMPORT_TNG_RETURN_MSG'];
	unset($_SESSION['PAYMENT_IMPORT_TNG_RETURN_MSG']);
}
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Msg);
?>
<br/>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td align="center">
			<?=$rows_affected.' ' . $Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully'] ?>
		</td>
	</tr>
	<tr>
		<td class="dotline">
			<img src="/images/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td align="center">
			<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='index.php';")?>
		</td>
	</tr>
</table>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>