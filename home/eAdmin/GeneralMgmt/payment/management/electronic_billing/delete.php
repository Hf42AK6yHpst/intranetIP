<?php
// Editing by 
/*
 * 2015-11-06 (Carlos): $sys_custom['ePayment']['ElectronicBilling'] - Created for Leung Shek Chee College
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ePayment']['ElectronicBilling']) {
	//header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();

$result = $lpayment->deleteElectronicBillingRecord($RecordID);

echo $result? "1":"0";

intranet_closedb();
?>