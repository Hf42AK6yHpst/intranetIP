<?php
// Editing by
/*
 * 2019-06-20 (Ray): created
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfamily.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ePayment']['AdminTransferMoneyForStudents']) {
    header ("Location: /");
    intranet_closedb();
    exit();
}

$lclass = new libclass();

$select_class = $lclass->getSelectClass("name=\"targetClass\" onChange=\"onParentStudentChange(true, true);\"",$targetClass);

if ($targetClass != "")
{
    $name_field = getNameFieldWithClassNumberByLang("a.");
    $sql = "SELECT a.UserID, CONCAT($name_field,IF(a.RecordStatus='3','[".$Lang['Status']['Left']."]','')) FROM INTRANET_USER as a
                                 WHERE a.UserID IN
                                (SELECT b.ParentID
                                FROM INTRANET_USER as a
                                LEFT JOIN INTRANET_PARENTRELATION b on b.StudentID=a.UserID
                                WHERE a.RecordType = 2 AND a.RecordStatus = 1 AND a.ClassName = '$targetClass' AND a.UserID
                                AND b.ParentID IS NOT NULL)
                                AND a.RecordType=3 AND a.RecordStatus=1 ";

    $list = $lclass->returnArray($sql,2);
    $select_parent = getSelectByArray($list, "id=\"parentID\" name=\"parentID\"  onChange=\"onParentStudentChange(false, true);\"", $parentID);

    if($parentID != "")
    {
        $hasRight = true;
        $lu = new libuser($parentID);
        if ($lu->RecordType == 3)
        {
            $lfamily = new libfamily();
            if ($studentid == "")
            {
                $studentid = $lfamily->returnFirstChild($parentID);
            }
            else
            {
                $linkedChildren = $lfamily->returnChildrens($parentID);
                if (!in_array($studentid,$linkedChildren))
                {
                    header("Location: /");
                    intranet_closedb();
                    exit();
                }
            }
            if ($studentid == "")
            {
                $hasRight = false;
            }
        } else {
            $hasRight = false;
        }

        if (!$hasRight)
        {
            header("Location: /");
            intranet_closedb();
            exit();
        }

        $lpayment = new libpayment();

        $temp = $lpayment->checkBalance($studentid);
        list($balance, $lastupdated) = $temp;
        if ($balance != "" && $lastupdated != "")
        {
            $balance_str = "$ ".number_format($balance,2)." ($i_Payment_Field_LastUpdated: $lastupdated)";
        }
        else
        {
            $balance_str = "$ $balance";
        }

        $children = $lfamily->returnChildrensList($parentID);
        $targets = array();
        for ($i=0; $i<sizeof($children); $i++)
        {
            list($sid , $s_name) = $children[$i];
            if ($sid == $studentid) continue;

            $temp = $lpayment->checkBalance($sid);
            list($s_bal, $s_lastupdated) = $temp;
            $s_bal = number_format($s_bal,2);
            $display_str = "$s_name ($"."$s_bal)";
            $array = array($sid,$display_str);
            $targets[] = $array;
        }

        $selection_from = $lfamily->getSelectChildren($parentID,"id='studentid' name='studentid' onChange=\"onParentStudentChange(false, false);\"",$studentid);
        $select_children = getSelectByArray($targets,"name=target","",1,1);

    }
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "TransferMoneyForStudents";

$linterface = new interface_html();


$TAGS_OBJ[] = array($Lang['ePayment']['AdminTransferMoneyForStudents'], "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();

if(isset($_SESSION['TRANSFER_MONEY_FOR_STUDENTS_RESULT']))
{
    $Msg = $_SESSION['TRANSFER_MONEY_FOR_STUDENTS_RESULT'];
    unset($_SESSION['TRANSFER_MONEY_FOR_STUDENTS_RESULT']);
}

$linterface->LAYOUT_START($Msg);
?>

    <script type="text/javascript" language="JavaScript">
        function onParentStudentChange(clear_parentid, clear_studentid)
        {
            document.form1.action = "";
            if(clear_parentid) {
                $("#parentID").val("");
            }
            if(clear_studentid) {
                $("#studentid").val("");
            }
            document.form1.submit();
        }

        function checkform(obj)
        {
            if(obj.amount === undefined) return false;
            if(!check_text(obj.amount, "<?php echo $i_alert_pleasefillin.$i_Payment_TransferAmount; ?>.")) return false;
            if(isNaN(obj.amount.value))
            {
                alert("<?php echo $i_alert_pleasefillin.$i_Payment_TransferAmount; ?>");
                return false;
            }
            if(obj.amount.value <= 0)
            {
                alert("<?php echo $i_Payment_Warning_InvalidAmount; ?>");
                return false;
            }

            document.form1.submit();
            return true;
        }
    </script>
    <br />
    <form id="form1" name="form1" action="update.php" method="get">
        <table class="form_table_v30">
            <tr valign="top">
                <td valign="top" nowrap="nowrap" class='field_title'>
                    <?=$iDiscipline['class']?>: <span class='tabletextrequire'>*</span>
                </td>
                <td><?=$select_class?></td>
            </tr>
            <?php if($targetClass != "") { ?>
            <tr valign="top">
                <td valign="top" nowrap="nowrap" class='field_title'>
                    <?=$iDiscipline['parent']?>: <span class='tabletextrequire'>*</span>
                </td>
                <td><?=$select_parent?></td>
            </tr>
            <?php } ?>
            <?php if($parentID != "") { ?>
            <tr valign="top">
                <td valign="top" nowrap="nowrap" class='field_title'>
                    <?=$i_Payment_TransactionType_TransferFrom?>: <span class='tabletextrequire'>*</span>
                </td>
                <td><?=$selection_from?></td>
            </tr>
            <tr valign="top">
                <td valign="top" nowrap="nowrap" class='field_title'>
                    <?=$i_Payment_Field_Balance?>:
                </td>
                <td><?=$balance_str?></td>
            </tr>
                <?php if(sizeof($children) > 1) { ?>
            <tr valign="top">
                <td valign="top" nowrap="nowrap" class='field_title'>
                    <?=$i_Payment_TransactionType_TransferTo?>: <span class='tabletextrequire'>*</span>
                </td>
                <td><?=$select_children?></td>
            </tr>
            <tr valign="top">
                <td valign="top" nowrap="nowrap" class='field_title'>
                    <?=$i_Payment_TransferAmount?>: <span class='tabletextrequire'>*</span>
                </td>
                <td><input name="amount" type="text" class="textboxnum" maxlength="255" value="<?=$amount?>"/></td>
            </tr>
                <?php } ?>
            <?php } ?>
        </table>
        <table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
            <tr><td align="left" class="tabletextremark"><?=$i_general_required_field?></td></tr>
            <tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
            <tr>
                <td align="center">
                    <?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "checkform(document.form1)", "submit_btn").'&nbsp;'?>
                </td>
            </tr>
        </table>
    </form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>