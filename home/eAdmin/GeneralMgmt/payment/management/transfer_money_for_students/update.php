<?php
// Editing by
/*
 * 2019-06-20 (Ray): created
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfamily.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ePayment']['AdminTransferMoneyForStudents']) {
    header ("Location: /");
    intranet_closedb();
    exit();
}

$lu = new libuser($parentID);

$hasRight = true;
if ($lu->RecordType == 3)  # Parent
{
    $lfamily = new libfamily();
    $linkedChildren = $lfamily->returnChildrens($parentID);
    if (!in_array($studentid,$linkedChildren))
    {
        intranet_closedb();
        exit();
    }
    if (!in_array($target,$linkedChildren))
    {
        header("Location: /");
        intranet_closedb();
        exit();
    }
    if ($studentid == "")
    {
        $hasRight = false;
    }
}
else
{
    $hasRight = false;
}

if (!$hasRight)
{
    header("Location: /");
    intranet_closedb();
    exit();
}

if (!is_numeric($amount))
{
    $_SESSION['TRANSFER_MONEY_FOR_STUDENTS_RESULT'] = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
    header("Location: index.php");
    intranet_closedb();
    exit();
}

$lpayment = new libpayment();
$studentid = IntegerSafe($studentid);
$temp = $lpayment->checkBalance($studentid);
list($balance, $lastupdated) = $temp;

if ($balance < $amount)
{
    $_SESSION['TRANSFER_MONEY_FOR_STUDENTS_RESULT'] = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
    header("Location: index.php");
    intranet_closedb();
    exit();
}

$success = $lpayment->account_transfer($studentid,$target,$amount);
if ($success)
{
    $_SESSION['TRANSFER_MONEY_FOR_STUDENTS_RESULT'] = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}
else
{
    $_SESSION['TRANSFER_MONEY_FOR_STUDENTS_RESULT'] = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}

intranet_closedb();
header("Location: index.php");
?>
