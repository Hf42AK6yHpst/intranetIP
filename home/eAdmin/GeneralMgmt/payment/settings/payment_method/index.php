<?php
// Editing by 
/*
 * 2017-02-07 Carlos: Created
 */


$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ePayment']['HartsPreschool'] || !$sys_custom['ePayment']['PaymentMethod']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
# preserve table view
if ($ck_payment_method_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_payment_method_page_number", $pageNo, 0, "", "", 0);
	$ck_payment_method_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_payment_method_page_number!="")
{
	$pageNo = $ck_payment_method_page_number;
}

if ($ck_payment_method_page_order!=$order && $order!="")
{
	setcookie("ck_payment_method_page_order", $order, 0, "", "", 0);
	$ck_payment_method_page_order = $order;
} else if (!isset($order) && $ck_payment_method_page_order!="")
{
	$order = $ck_payment_method_page_order;
}

if ($ck_payment_method_page_field!=$field && $field!="")
{
	setcookie("ck_payment_method_page_field", $field, 0, "", "", 0);
	$ck_payment_method_page_field = $field;
} else if (!isset($field) && $ck_payment_method_page_field!="")
{
	$field = $ck_payment_method_page_field;
}

$lpayment = new libpayment();
$lclass = new libclass();

$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "StudentPaymentMethodSettings";

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$linterface = new interface_html();

$keyword = trim($keyword);

$student_name_field = getNameFieldByLang2("u.");
$modifier_name_field = getNameFieldByLang2("u2.");

$li = new libdbtable2007($field, $order, $pageNo);

$sql = "SELECT 
			u.ClassName,
			u.ClassNumber,
			$student_name_field as StudentName,
			CASE s.PaymentMethod ";
  foreach($sys_custom['ePayment']['PaymentMethodItems'] as $key => $val){
  	$sql .= " WHEN '$key' THEN '$val' ";
  }
  $sql .= "  ELSE '--' 
			 END as PaymentMethod,
			IFNULL(DATE_FORMAT(s.DateInput,'%Y-%m-%d %H:%i:%s'),'--') as DateInput,
			IF(u2.UserID IS NOT NULL,$modifier_name_field,'--') as ModifiedBy,
			CONCAT('<input type=\"checkbox\" name=\"StudentID[]\" value=\"',u.UserID,'\" />') as Checkbox 
		FROM INTRANET_USER as u 
		LEFT JOIN PAYMENT_STUDENT_PAYMENT_METHOD as s ON s.StudentID=u.UserID 
		LEFT JOIN INTRANET_USER as u2 ON u2.UserID=s.ModifiedBy 
		WHERE u.RecordType=2 AND u.RecordStatus=1 ";
if($ClassName != '')
{
	$sql .= " AND u.ClassName='$ClassName' ";
}
if($PaymentMethod == '-1'){
	$sql .= " AND s.PaymentMethod IS NULL ";
}else if($PaymentMethod != ''){
	$sql .= " AND s.PaymentMethod='$PaymentMethod' ";
}
if($keyword != ''){
	$safe_keyword = $li->Get_Safe_Sql_Like_Query($keyword);
	$name_like_field = Get_Lang_Selection("ChineseName","EnglishName");
	$sql .= " AND (u.".$name_like_field." LIKE '%$safe_keyword%') ";
}

$li->field_array = array("u.ClassName", "u.ClassNumber+0", "StudentName", "s.PaymentMethod","s.DateInput", "s.ModifiedBy");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0);
$li->IsColOff = "IP25_table";

if ($field=="" && $order=="") {
	$li->field = 0;
	$li->order = 1;
}

if($li->field == 0){
	$li->fieldorder2 = ",u.ClassNumber+0";
}


$class_selection_filter = $i_general_class.': '.$lclass->getSelectClass('name="ClassName" onchange="document.form1.submit();"',$ClassName,0,$i_general_all_classes);

$payment_method_selection_ary = array();
$payment_method_selection_options_ary = array();
$payment_method_selection_ary[] = array('-1',$Lang['ePayment']['NoPaymentMethod']);
$payment_method_selection_options_ary[] = array('-1',$Lang['ePayment']['NoPaymentMethod']);
foreach($sys_custom['ePayment']['PaymentMethodItems'] as $key => $val){
	$payment_method_selection_ary[] = array($key,$val);
	$payment_method_selection_options_ary[] = array($key,$val);
}
$payment_method_filter = $Lang['ePayment']['PaymentMethod'].': '.$linterface->GET_SELECTION_BOX($payment_method_selection_ary, ' name="PaymentMethod" id="PaymentMethod" onchange="document.form1.submit();" ', $Lang['General']['All'], $PaymentMethod, false);

$payment_method_selection_options = $linterface->GET_SELECTION_BOX($payment_method_selection_options_ary, ' name="AppliedPaymentMethod" id="AppliedPaymentMethod" ', '', '', true);

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<th width='1' class='num_check'>#</th>\n";
$li->column_list .= "<th width='20%'>".$li->column_IP25($pos++, $i_UserClassName)."</th>\n";
$li->column_list .= "<th width='15%'>".$li->column_IP25($pos++, $i_UserClassNumber)."</th>\n";
$li->column_list .= "<th width='20%'>".$li->column_IP25($pos++, $Lang['ePayment']['Student'])."</th>\n";
$li->column_list .= "<th width='10%'>".$li->column_IP25($pos++, $Lang['ePayment']['PaymentMethod'])."</th>\n";
$li->column_list .= "<th width='15%'>".$li->column_IP25($pos++, $Lang['General']['LastUpdatedTime'])."</th>\n";
$li->column_list .= "<th width='15%'>".$li->column_IP25($pos++, $Lang['General']['LastUpdatedBy'])."</th>\n";
$li->column_list .= "<th width='1'>".$li->check("StudentID[]")."</th>\n";


$TAGS_OBJ[] = array($Lang['ePayment']['StudentPaymentMethodSettings'],"",1);

$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();

if(isset($_SESSION['PAYMENT_METHOD_SETTINGS_RETURN_MSG']))
{
	$ReturnMsg = $_SESSION['PAYMENT_METHOD_SETTINGS_RETURN_MSG'];
	unset($_SESSION['PAYMENT_METHOD_SETTINGS_RETURN_MSG']);
}
$linterface->LAYOUT_START($ReturnMsg);

?>
<br />
<form id="form1" name="form1" method="POST">
<div class="table_board">
	<div class="content_top_tool">
		<div class="Conntent_tool">
			
		</div>
		<div class="Conntent_search"><input type="text" id="keyword" name="keyword" value="<?=intranet_htmlspecialchars(stripslashes($keyword))?>" onkeyup="GoSearch(event);"></div>
		<br style="clear:both;">
	</div>
	<br style="clear:both;">
	<div class="table_filter">
		<?=$class_selection_filter?>&nbsp;|&nbsp;<?=$payment_method_filter?>
	</div>
	<br style="clear:both">	
	<?php //$linterface->Get_DBTable_Action_Button_IP25($tool_buttons) ?>
	<div class="common_table_tool">
		<a class="set" href="javascript:checkEditMultiple2(document.form1,'StudentID[]','SetPaymentMethodToStudents(0);');"><?=$Lang['ePayment']['SetSelectedStudentsPaymentMethodAs']?></a><span style="float:left;color:#2F7510;line-height:19px;"> / </span><a class="set" href="javascript:SetPaymentMethodToStudents(1);"><?=$Lang['ePayment']['SetAllStudentsPaymentMethodAs']?></a>&nbsp;<?=$payment_method_selection_options?>
	</div>
	<br style="clear:both">			
	<?=$li->display();?>
</div>
<input type="hidden" name="setAll" id="setAll" value="0" />
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>
<br />
<br />
<script type="text/javascript">
function GoSearch(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		document.form1.submit();
	else
		return false;
}

function SetPaymentMethodToStudents(setAll)
{
	Block_Element('form1');
	$('#setAll').val(setAll?'1':'0');
	$.post(
		'set_payment_method.php',
		$('#form1').serialize(),
		function(returnData){
			document.form1.submit();
		}
	);
}
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>