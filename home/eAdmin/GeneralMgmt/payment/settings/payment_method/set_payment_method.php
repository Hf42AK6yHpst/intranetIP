<?php

/*
 * 2017-02-09 Carlos: Created
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ePayment']['HartsPreschool'] || !$sys_custom['ePayment']['PaymentMethod']) {
	intranet_closedb();
	$_SESSION['PAYMENT_METHOD_SETTINGS_RETURN_MSG'] = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
	exit();
}

$lpayment = new libpayment();

$setAll = $_POST['setAll'] == 1;
if($setAll){
	$sql = "SELECT u.UserID FROM INTRANET_USER as u 
			LEFT JOIN PAYMENT_STUDENT_PAYMENT_METHOD as s ON s.StudentID=u.UserID 
			LEFT JOIN INTRANET_USER as u2 ON u2.UserID=s.ModifiedBy 
			WHERE u.RecordType=2 AND u.RecordStatus=1 ";
	if($ClassName != '')
	{
		$sql .= " AND u.ClassName='$ClassName' ";
	}
	if($PaymentMethod == '-1'){
		$sql .= " AND s.PaymentMethod IS NULL ";
	}else if($PaymentMethod != ''){
		$sql .= " AND s.PaymentMethod='$PaymentMethod' ";
	}
	if($keyword != ''){
		$safe_keyword = $lpayment->Get_Safe_Sql_Like_Query($keyword);
		$name_like_field = Get_Lang_Selection("ChineseName","EnglishName");
		$sql .= " AND (u.".$name_like_field." LIKE '%$safe_keyword%') ";
	}
	$StudentIdAry = $lpayment->returnVector($sql);
}else{
	$StudentIdAry = IntegerSafe($_POST['StudentID']);
}
$AppliedPaymentMethod = IntegerSafe($_POST['AppliedPaymentMethod']);

$resultAry = array();

if($AppliedPaymentMethod == '-1'){
	$sql = "DELETE FROM PAYMENT_STUDENT_PAYMENT_METHOD WHERE StudentID IN (".implode(",",$StudentIdAry).")";
	$resultAry[] = $lpayment->db_db_query($sql);
}else{

	$sql = "SELECT StudentID FROM PAYMENT_STUDENT_PAYMENT_METHOD WHERE StudentID IN (".implode(",",$StudentIdAry).")";
	$existing_students = $lpayment->returnVector($sql);
	
	$missing_students = array_values(array_diff($StudentIdAry, $existing_students));
	
	
	for($i=0;$i<count($existing_students);$i++){
		$sql = "UPDATE PAYMENT_STUDENT_PAYMENT_METHOD SET PaymentMethod='$AppliedPaymentMethod',DateModified=NOW(),ModifiedBy='".$_SESSION['UserID']."' WHERE StudentID='".$existing_students[$i]."'";
		$resultAry[] = $lpayment->db_db_query($sql);
	}
	
	if(count($missing_students)>0)
	{
		$insert_sql = "INSERT INTO PAYMENT_STUDENT_PAYMENT_METHOD (StudentID,PaymentMethod,DateInput,DateModified,InputBy,ModifiedBy) VALUES ";
		$delim = "";
		for($i=0;$i<count($missing_students);$i++){
			$insert_sql .= $delim."('".$missing_students[$i]."','".$AppliedPaymentMethod."',NOW(),NOW(),'".$_SESSION['UserID']."','".$_SESSION['UserID']."')";
			$delim = ",";
		}
		$resultAry[] = $lpayment->db_db_query($insert_sql);
	}
}

$_SESSION['PAYMENT_METHOD_SETTINGS_RETURN_MSG'] = in_array(false, $resultAry)? $Lang['General']['ReturnMessage']['UpdateUnsuccess'] : $Lang['General']['ReturnMessage']['UpdateSuccess'] ;

intranet_closedb();
?>