<?php
/*
 * 2018-04-17 (Carlos): Added custom settings $sys_custom['ePayment']['MinIEPSCharge'] and $sys_custom['ePayment']['MinCounterBillCharge'] to control minimum charge fee.
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$GeneralSetting = new libgeneralsettings();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "BasicSettings_PPS";

$linterface = new interface_html();

$SettingList[] = "'PPSChargeEnabled'";
$SettingList[] = "'PPSIEPSCharge'";
$SettingList[] = "'PPSCounterBillCharge'";
$Settings = $GeneralSetting->Get_General_Setting('ePayment',$SettingList);

$enable = $Settings['PPSChargeEnabled'];
$IEPS = $Settings['PPSIEPSCharge'];
$COUNTER = $Settings['PPSCounterBillCharge'];

$interval  = 0.1 ;

### IEPS ###
$select_ieps ="<SELECT name='PPSIEPSCharge' ".($enable==1?"":" DISABLED ").">";
$i= isset($sys_custom['ePayment']['MinIEPSCharge'])? $sys_custom['ePayment']['MinIEPSCharge'] : 2;

while($i<=4 || abs($i-4)< 0.001){
	$diff = abs($IEPS - $i);
	$selected = $IEPS!="" && $diff < 0.001 ? " SELECTED ":"";
	$select_ieps.="<OPTION VALUE='$i' $selected>".$lpayment->getWebDisplayAmountFormat($i)."</OPTION>";
	$i+=$interval;

}
$select_ieps.="</SELECT>";

### COUNTER ####
$select_counter ="<SELECT name='PPSCounterBillCharge' ".($enable==1?"":" DISABLED ").">";
$j= isset($sys_custom['ePayment']['MinCounterBillCharge'])? $sys_custom['ePayment']['MinCounterBillCharge'] : 3;
while($j<=6  || abs($i-6)< 0.001){
	$diff2 = abs($COUNTER - $j);
	$selected2 = $COUNTER!="" && $diff2< 0.001 ? " SELECTED ":"";
	$select_counter.="<OPTION VALUE='$j' $selected2>".$lpayment->getWebDisplayAmountFormat($j)."</OPTION>";
	$j+=$interval;
}
$select_counter.="</SELECT>";

$TAGS_OBJ[] = array($i_Payment_Menu_Settings_PPS_Setting, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();

$Msg = urldecode($Msg);
$linterface->LAYOUT_START($Msg);

if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<br />
<script language='javascript'>
function setCharge(flag){
	ieps = document.getElementsByName('PPSIEPSCharge');
	counter = document.getElementsByName('PPSCounterBillCharge');
	
	if(ieps==null || counter == null) {
		return;
	}
	ieps[0].disabled = !flag;
	counter[0].disabled = !flag;
}
function resetForm(){
	f = document.form1;
	e = document.form1.PPSChargeEnabled;
	if(f ==null || e==null) return;
	f.reset();
	setCharge(e.checked);
}
</script>
<form name="form1" method="post" action="update.php">
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="tabletext" align="right" colspan="2"><?=$SysMsg?></td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td valign="top" class="formfieldtitle tabletext">
			<?=$i_Payment_PPS_Charge?>
		</td>
		<td class="tabletext" width="50%">
			<input valign="absmiddle" type="checkbox" name="PPSChargeEnabled" value="1" <?=($enable==1?"CHECKED":"")?> onClick='setCharge(this.checked)'>
		</td>
	</tr>
	<tr>
		<td valign="top" class="formfieldtitle tabletext">
			<?=$i_Payment_Import_PPS_FileType_Normal?>
		</td>
		<td class="tabletext" width="50%">
			<?=$select_ieps?>
		</td>
	</tr>
	<tr>
		<td valign="top" class="formfieldtitle tabletext">
			<?=$i_Payment_Import_PPS_FileType_CounterBill?>
		</td>
		<td class="tabletext" width="50%">
			<?=$select_counter?>
		</td>
	</tr>
	
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center">
			<?= $linterface->GET_ACTION_BTN($button_save, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "button", "","reset2"," class='formbutton' onClick='resetForm()' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
</form>
<?
$linterface->LAYOUT_STOP();
?>