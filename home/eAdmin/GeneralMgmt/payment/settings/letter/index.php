<?php

# using: yat

#################################################################################################
#
#	Date:	2011-08-02	YatWoon
#			Improved: using HTML-editor instead of textarea
#
#################################################################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');

intranet_auth();
intranet_opendb();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$GeneralSetting = new libgeneralsettings();
$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "BasicSettings_PaymentLetter";

$linterface = new interface_html();

// get setting value
$SettingList[] = "'PaymentLetterHeader'";
$SettingList[] = "'PaymentLetterHeader1'";
$SettingList[] = "'PaymentLetterFooter'";
$LetterSetting = $GeneralSetting->Get_General_Setting('ePayment',$SettingList);

$TAGS_OBJ[] = array($i_Payment_Menu_Settings_Letter, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if ($msg == "2") $SysMsg = $linterface->GET_SYS_MSG("update");

$objHtmlEditor = new FCKeditor ( 'PaymentLetterHeader' , "100%", "320", "", "", htmlspecialchars_decode($LetterSetting['PaymentLetterHeader']));
$PaymentLetterHeaderTextArea = $objHtmlEditor->CreateHtml();
$objHtmlEditor1 = new FCKeditor ( 'PaymentLetterHeader1' , "100%", "320", "", "", htmlspecialchars_decode($LetterSetting['PaymentLetterHeader1']));
$PaymentLetterHeader1TextArea = $objHtmlEditor1->CreateHtml();
$objHtmlEditor2 = new FCKeditor ( 'PaymentLetterFooter' , "100%", "320", "", "", htmlspecialchars_decode($LetterSetting['PaymentLetterFooter']));
$PaymentLetterFooterTextArea = $objHtmlEditor2->CreateHtml();

?>
<br />
<form name="form1" action="update.php" method="post">
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="tabletext" align="right" colspan="2"><?=$SysMsg?></td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$i_Payment_Menu_Settings_Letter_Header?>
		</td>
		<td>
			<?//=$linterface->GET_TEXTAREA("PaymentLetterHeader", $LetterSetting['PaymentLetterHeader'])?>
			<?=$PaymentLetterHeaderTextArea?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$i_Payment_Menu_Settings_Letter_Header2?>
		</td>
		<td>
			<?//=$linterface->GET_TEXTAREA("PaymentLetterHeader1", $LetterSetting['PaymentLetterHeader1'])?>
			<?=$PaymentLetterHeader1TextArea?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$i_Payment_Menu_Settings_Letter_Footer?>
		</td>
		<td>
			<?//=$linterface->GET_TEXTAREA("PaymentLetterFooter", $LetterSetting['PaymentLetterFooter'])?>
			<?=$PaymentLetterFooterTextArea?>
		</td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center">
			<?= $linterface->GET_ACTION_BTN($button_save, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
</form>
<?
$linterface->LAYOUT_STOP();
print $linterface->FOCUS_ON_LOAD("form1.header");
?>