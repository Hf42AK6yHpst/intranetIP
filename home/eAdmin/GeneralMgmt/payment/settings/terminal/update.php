<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$GeneralSetting = new libgeneralsettings();

$SettingList['TerminalIPList'] = trim(stripslashes(urldecode(HTMLtoDB($_REQUEST['TerminalIPList']))));
$SettingList['ConnectionTimeout'] = trim(stripslashes(urldecode(HTMLtoDB($_REQUEST['ConnectionTimeout']))));
$SettingList['ConnectionTimeout'] = ($SettingList['ConnectionTimeout']=="" || !is_numeric($SettingList['ConnectionTimeout']))? 60:$SettingList['ConnectionTimeout'];
$SettingList['PaymentNoAuth'] = ($_REQUEST['PaymentNoAuth'])? $_REQUEST['PaymentNoAuth']:0;
$SettingList['PurchaseNoAuth'] = ($_REQUEST['PurchaseNoAuth'])? $_REQUEST['PurchaseNoAuth']:0;
$GeneralSetting->Save_General_Setting('ePayment',$SettingList);

intranet_closedb();
header("Location: index.php?msg=2");
?>