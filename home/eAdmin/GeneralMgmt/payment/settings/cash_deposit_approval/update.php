<?php
// Editing by 
/*
 * 2016-08-25 (Carlos): $sys_custom['ePayment']['CashDepositApproval'] - created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ePayment']['CashDepositApproval'] || !isset($GroupID)) {
	intranet_closedb();
	$_SESSION['PAYMENT_CREDIT_APPROVAL_RESULT'] = $Lang['General']['ReturnMessage']['SettingSaveFail'];
	header ("Location:index.php");
	exit();
}

$laccessright = new libaccessright("ePayment");
$GeneralSetting = new libgeneralsettings();

$SettingList['CashDepositApproval'] = $_POST['CashDepositApproval'];
$MemberID = $_POST['MemberID'];

$success = $GeneralSetting->Save_General_Setting('ePayment',$SettingList);
//$laccessright->DELETE_ACCESS_RIGHT_MEMBER($GroupID, 'A', $MemberID);
//$laccessright->NEW_ACCESS_GROUP_MEMBER($GroupID, $MemberID);
$laccessright->db_db_query("DELETE FROM ACCESS_RIGHT_GROUP_MEMBER WHERE GroupID='$GroupID'");

$sql = "INSERT INTO ACCESS_RIGHT_GROUP_MEMBER (GroupID,UserID,DateInput) VALUES ";
$delim = "";
for($i=0;$i<count($MemberID);$i++){
	$MemberID[$i] = str_replace('U','',$MemberID[$i]);
	if($MemberID[$i] == '' || $MemberID[$i]==0) continue;
	$sql .= $delim."('$GroupID','".$MemberID[$i]."',NOW())";
	$delim = ",";
}
$laccessright->db_db_query($sql);

$_SESSION['PAYMENT_CASH_DEPOSIT_APPROVAL_RESULT'] = $success? $Lang['General']['ReturnMessage']['SettingSaveSuccess'] : $Lang['General']['ReturnMessage']['SettingSaveFail'];
intranet_closedb();
header("Location:index.php");
exit;
?>