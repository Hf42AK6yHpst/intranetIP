<?php
// Editing by 
/*
 * 2016-08-25 (Carlos): $sys_custom['ePayment']['CashDepositApproval'] - created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ePayment']['CashDepositApproval']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lpayment = new libpayment();
$linterface = new interface_html();
$laccessright = new libaccessright("ePayment");
$GeneralSetting = new libgeneralsettings();

$SettingList[] = "'CashDepositApproval'";
$Settings = $GeneralSetting->Get_General_Setting('ePayment',$SettingList);

$group_title = 'Cash Deposit Approval';
$group_id = $laccessright->GET_ACCESS_RIGHT_GROUP_ID('A', $group_title);
if($group_id == ''){
	$group_id = $laccessright->NEW_ACCESS_RIGHT_GROUP('A', $group_title, '');
}

$user_name_field = getNameFieldByLang2("u.");
$archived_user_name_field = getNameFieldByLang2("au.");

$sql = "SELECT 
			m.UserID,
			IF(u.UserID IS NULL,$archived_user_name_field,$user_name_field) as UserName 
		FROM ACCESS_RIGHT_GROUP_MEMBER as m 
		LEFT JOIN INTRANET_USER as u ON u.UserID=m.UserID 
		LEFT JOIN INTRANET_ARCHIVE_USER as au ON au.UserID=m.UserID 
		WHERE m.GroupID='$group_id'";
$approval_users = $laccessright->returnArray($sql);
//debug_pr($sql);debug_pr($approval_users);
$approval_user_selection = $linterface->GET_SELECTION_BOX($approval_users, ' name="MemberID[]" id="MemberID[]" multiple="multiple" size="10" style="min-width:10em;" ', '', "");


$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "BasicSettings_CashDepositApproval";

if(isset($_SESSION['PAYMENT_CASH_DEPOSIT_APPROVAL_RESULT'])){
	$msg = $_SESSION['PAYMENT_CASH_DEPOSIT_APPROVAL_RESULT'];
	unset($_SESSION['PAYMENT_CASH_DEPOSIT_APPROVAL_RESULT']);
}

$TAGS_OBJ[] = array($Lang['ePayment']['CashDepositApproval'], "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($msg);
?>
<br />
<form name="form1" method="post" action="update.php" onsubmit="Select_All_Options('MemberID[]',true);return true;">
<input type="hidden" name="GroupID" id="GroupID" value="<?=$group_id?>" />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
			<table class="form_table_v30" width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class="field_title">
						<?=$Lang['ePayment']['CashDepositApproval']?>
					</td>
					<td width="70%">
						<?=$linterface->Get_Radio_Button('CashDepositApproval_Y', 'CashDepositApproval', '1', $Settings['CashDepositApproval']==1, $__Class="", $__Display=$Lang['General']['Enabled'], $__Onclick="",$__isDisabled=0)?>
						<?=$linterface->Get_Radio_Button('CashDepositApproval_N', 'CashDepositApproval', '0', $Settings['CashDepositApproval']!=1, $__Class="", $__Display=$Lang['General']['Disabled'], $__Onclick="",$__isDisabled=0)?>
					</td>
				</tr>
				<tr>
					<td valign="top" class="field_title">
						<?=$Lang['ePayment']['ApprovalUsers']?>
					</td>
					<td width="70%">
						<table border="0" cellpadding="5" cellspacing="0">
							<tr>
								<td><?=$approval_user_selection?></td>
								<td valign="bottom" style="vertical-align:bottom;">
									<?=$linterface->GET_SMALL_BTN($Lang['Btn']['Select'], 'button', 'newWindow(\'/home/common_choose/index.php?fieldname=MemberID[]&permitted_type=1&excluded_type=2,3,4\',2)', 'selectBtn', $__ParOtherAttribute="", $__OtherClass="", $__ParTitle='')?><br />
									<?=$linterface->GET_SMALL_BTN($Lang['Btn']['Remove'], 'button', 'remove_selection_option(\'MemberID[]\',false)', 'removeBtn', $__ParOtherAttribute="", $__OtherClass="", $__ParTitle='')?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
    	<td>
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
	<tr>
	    <td align="center">
			<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "") ?>
		</td>
	</tr>
</table>
</form>
<br />
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>