<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$li = new libdb();
$CatName = intranet_htmlspecialchars($CatName);
$Description = intranet_htmlspecialchars($Description);
$DisplayOrder = intranet_htmlspecialchars($DisplayOrder);

$fields = "Name = '$CatName'";
$fields .= ",Description = '$Description'";
$fields .= ",DisplayOrder = '$DisplayOrder'";
$fields .= ",DateModified = now()";


if($sys_custom['ePayment_PaymentCategory_Code']) {
	$catcode_valid = true;
	$CatCode = intranet_htmlspecialchars($CatCode);
	if($CatCode == "") {
		$catcode_valid = false;
	} else {
		$catcode_valid = $lpayment->checkPaymentCatCodeValid($CatCode, $CatID);
	}
	if($catcode_valid) {
		$fields .= ",CatCode = '$CatCode'";
	}
}


$sql = "UPDATE PAYMENT_PAYMENT_CATEGORY SET $fields WHERE CatID = '$CatID'";
$li->db_db_query($sql);

header ("Location: index.php?msg=2");
intranet_closedb();
?>
