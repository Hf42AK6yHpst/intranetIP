<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$li = new libdb();
$CatName = intranet_htmlspecialchars($CatName);
$Description = intranet_htmlspecialchars($Description);
$DisplayOrder = intranet_htmlspecialchars($DisplayOrder);
$more_fields = "";
$more_values = "";
$catcode_valid = true;
if($sys_custom['ePayment_PaymentCategory_Code']) {
	$CatCode = intranet_htmlspecialchars($CatCode);
	if($CatCode == "") {
		$catcode_valid = false;
	} else {
		$catcode_valid = $lpayment->checkPaymentCatCodeValid($CatCode);
		$more_fields .= ",CatCode";
		$more_values .= ",'$CatCode'";
	}
}

if($catcode_valid) {
	$sql = "INSERT INTO PAYMENT_PAYMENT_CATEGORY (Name, DisplayOrder, Description, DateInput, DateModified $more_fields)
        VALUES ('$CatName','$DisplayOrder','$Description',now(),now() $more_values)";
	$li->db_db_query($sql);
}
header ("Location: index.php?msg=1");
intranet_closedb();
?>
