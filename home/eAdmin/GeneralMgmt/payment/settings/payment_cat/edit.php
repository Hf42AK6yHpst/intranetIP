<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "i_Payment_Menu_Settings_PaymentCategory";

$linterface = new interface_html();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$CatID = (is_array($CatID)? $CatID[0]: $CatID);

$info = $lpayment->returnPaymentCatInfo($CatID);
list($name,$displayOrder,$desp, $catcode) = $info;

$TAGS_OBJ[] = array($i_Payment_Menu_Settings_PaymentCategory, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_edit)

?>
<script>
$(document).ready(function(){
    <?php if($sys_custom['ePayment_PaymentCategory_Code']) { ?>
    js_Add_KeyUp_Unique_Checking('<?=$CatID?>', 'CatCode', 'CatCodeWarningDiv', 'CatCode', '<?=$i_Payment_Warning_Enter_PaymentCategoryCode?>','<?=$i_Payment_Warning_Duplicate_PaymentCategoryCode?>');
    <?php } ?>
});

function js_Add_KeyUp_Unique_Checking(jsItemID, InputID, WarningDivID, DBFieldName, WarnBlankMsg, WarnUniqueMsg)
{
    jsItemID = jsItemID || '';

    $("input#"+InputID).keyup(function(){
        js_Check_Unique(jsItemID, InputID, WarningDivID, DBFieldName, WarnBlankMsg, WarnUniqueMsg, true);
    });
    $("input#"+InputID).change(function(){
        js_Check_Unique(jsItemID, InputID, WarningDivID, DBFieldName, WarnBlankMsg, WarnUniqueMsg, true);
    });
}

function js_Check_Unique(jsItemID, InputID, WarningDivID, DBFieldName, WarnBlankMsg, WarnUniqueMsg, async) {
    jsItemID = jsItemID || '';

    var InputValue = Trim($("input#" + InputID).val());
    var result = false;
    if (InputValue == '') {
        $('div#' + WarningDivID).html(WarnBlankMsg);
        $('div#' + WarningDivID).show();
    } else {
        $('div#' + WarningDivID).html("");
        $('div#' + WarningDivID).hide();
        $.ajax({
            url: "ajax_validate.php",
            data: {
                Action: DBFieldName,
                Value: InputValue,
                ItemID: jsItemID
            },
            async: async,
            type: 'POST',
            success: function (data) {
                if (data != 1) {
                    $('div#' + WarningDivID).html(WarnUniqueMsg).show();
                } else {
                    result = true;
                    $('div#' + WarningDivID).html("").hide();
                }
            }
        });
    }

    return result;
}

function checkForm(obj)
{
    <?php if($sys_custom['ePayment_PaymentCategory_Code']) { ?>
    var objCode = obj.CatCode;
    if(!check_text(objCode,'<?=$i_Payment_Warning_Enter_PaymentCategoryCode?>')) return false;
    var cat_code_check = js_Check_Unique('<?=$CatID?>', 'CatCode', 'CatCodeWarningDiv', 'CatCode', '<?=$i_Payment_Warning_Enter_PaymentItemCode?>','<?=$i_Payment_Warning_Duplicate_PaymentCategoryCode?>', false);
    if(cat_code_check == false) {
        return false;
    }
    <?php } ?>
    return true;
}
</script>
<br />
<form name="form1" action="edit_update.php" method="post" onsubmit="return checkForm(this);">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
    	<td>
    		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_Payment_Field_PaymentCategory?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<input class="textboxtext" type="text" name="CatName" maxlength="255" value="<?=$name?>">
	    			</td>
    			</tr>
				<?php if($sys_custom['ePayment_PaymentCategory_Code']) { ?>
                    <tr>
                        <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
							<?=$i_Payment_Field_PaymentCategory_Code?>
                        </td>
                        <td class="tabletext" width="70%">
                            <input class="textboxtext" type="text" id="CatCode" name="CatCode" maxlength="255" value="<?=$catcode?>">
                            <div id="CatCodeWarningDiv" style="color: #ff0000;"></div>
                        </td>
                    </tr>
				<?php } ?>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_Payment_Field_DisplayOrder?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<input class="textboxnum" type="text" name="DisplayOrder" maxlength="10" value="<?=$displayOrder?>">
	    			</td>
    			</tr>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_general_description?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<?=$linterface->GET_TEXTAREA("Description", $desp)?>
	    			</td>
    			</tr>
    		</table>
    	</td>
	</tr>
	<tr>
    	<td>
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
	<tr>
	    <td align="center">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="CatID" value="<?=$CatID?>">
</form>
<?
$linterface->LAYOUT_STOP();
print $linterface->FOCUS_ON_LOAD("form1.CatName");
intranet_closedb();
?>
