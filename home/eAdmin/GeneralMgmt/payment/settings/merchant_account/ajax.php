<?php
/*

 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !($lpayment->useEWalletMerchantAccount() || $lpayment->isEWalletTopUpEnabled())) {
	intranet_closedb();
	header ("Location: /");
	exit();
}


$linterface = new interface_html();

$task = $_POST['task'];

switch($task)
{
	case 'getModalForm':
		$AccountID = IntegerSafe($_POST['AccountID']);

		$lc = new libcardstudentattend2();
		$years = $lc->getRecordYear();
		$select_year = getSelectByValue($years, "name=\"Year\" onchange=\"QuotaYearChange(this, '".$AccountID."')\"",date('Y'),0,1);


		$x .= '<form name="modalForm" id="modalForm" action="" method="post" onsubmit="return false;">'."\n";
		$x .= '<div id="modalContent" style="overflow-y:auto;">'."\n";
		$x .= '<br />'."\n";
		$x .= $linterface->Get_Thickbox_Warning_Msg_Div("Overall_Error","", "WarnMsgDiv")."\n";
		$x .= '<table width="100%" cellpadding="2" class="form_table_v30">'."\n";
		$x .= '<col class="field_title">'."\n";
		$x .= '<col class="field_c">'."\n";

		$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$i_Attendance_Year.'</td>'."\n";
		$x .= '<td>'."\n";
		$x .= $select_year;
		$x .= '</td>'."\n";
		$x .= '</tr>'."\n";


		$x .= '</table>'."\n";
		$x .= '</div>'."\n";

		$x .= '<div class="edit_bottom_v30">'."\n";
		$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","tb_remove();")."\n";
		$x .= '</div>'."\n";

		$x .= '</form>'."\n";

		$x .= '<div id="TB_ajaxContent2"></div>';
		echo $x;
		break;
	case 'getQuotaDate':
		$AccountID = IntegerSafe($_POST['AccountID']);
		$Year = IntegerSafe($_POST['Year']);

		$sql = "SELECT * FROM PAYMENT_MERCHANT_ACCOUNT_QUOTA WHERE Year='$Year' AND AccountID='$AccountID' ORDER BY Month ASC";
		$rs = $lpayment->returnArray($sql);

		$x = '';
		$x .= '<table id="ClassGroupInfoTable" class="common_table_list" style="width:95%">';
		$x .= '<thead>';
		$x .= '<tr>';
		$x .= '<th style="width: 50%">' . $i_general_Month . '</th>';
		$x .= '<th style="">' . $Lang['ePayment']['FPSWaiveCountName']. '</th>';
		$x .= '</tr>';
		$x .= '</thead>';
		$x .= '<tbody>';

		foreach($rs as $temp) {
			$x .= '<tr>';
			$x .= '<td>'.$temp['Month'].'</td>';
			$x .= '<td>'.$temp['Quota'].'</td>';
			$x .= '</tr>';
		}

		$x .= '</tbody>';
		$x .= '</table>';
		echo $x;
		break;
}

intranet_closedb();
?>