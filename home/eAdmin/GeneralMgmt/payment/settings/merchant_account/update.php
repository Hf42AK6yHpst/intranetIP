<?php
// Editing by 
/*
 * 2019-08-12 Carlos: Added HandlingFee.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !($lpayment->useEWalletMerchantAccount() || $lpayment->isEWalletTopUpEnabled())) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$userObj = new libuser($_SESSION['UserID']);
$is_broadlearning_account = ($userObj->UserLogin == 'broadlearning') ? true : false;

$errors = array();
if(trim($_POST['AccountName']) == ''){
	$errors['Error_AccountName'] = $Lang['General']['JS_warning']['CannotBeBlank'];
}

if($is_broadlearning_account) {
	if (trim($_POST['MerchantAccount']) == '') {
		$errors['Error_MerchantAccount'] = $Lang['General']['JS_warning']['CannotBeBlank'];
	}
	if (trim($_POST['MerchantUID']) == '') {
		$errors['Error_MerchantUID'] = $Lang['General']['JS_warning']['CannotBeBlank'];
	}
}


if(count($errors)>0){
	$_SESSION['EPAYMENT_MERCHANT_ACCOUNT_FLASH_DATA'] = $errors;
	intranet_closedb();
	header("Location:edit.php");
	exit;
}

$is_edit = isset($AccountID) && $AccountID > 0;
$params = array('AccountName'=>$AccountName);
if($is_edit){
	$params['ExcludeAccountID'] = $AccountID;
}
$existing_records = $lpayment->getMerchantAccounts($params);

$flashData = array();
if(count($existing_records)>0){
	$flashData = $_POST;
	$flashData['Error_AccountName'] = $Lang['ePayment']['DuplicatedRecord'];
	$_SESSION['EPAYMENT_MERCHANT_ACCOUNT_FLASH_DATA'] = $flashData;
	intranet_closedb();
	header("Location:edit.php");
	exit;
}

if(isset($_POST['HandlingFee']) && $_POST['HandlingFee'] != ''){
	if(!is_numeric($_POST['HandlingFee']) || $_POST['HandlingFee'] < 0.00){
		$_POST['HandlingFee'] = '';
	}
}

if($_POST['AdministrativeFee'] == '0') {
	$_POST['AdministrativeFeePercentageAmount'] = '';
} else if($_POST['AdministrativeFee'] == '1') {
	$_POST['AdministrativeFeeFixAmount'] = '';
}

if(isset($_POST['AdministrativeFeeFixAmount']) && $_POST['AdministrativeFeeFixAmount'] != ''){
	if(!is_numeric($_POST['AdministrativeFeeFixAmount']) || $_POST['AdministrativeFeeFixAmount'] < 0.00){
		$_POST['AdministrativeFeeFixAmount'] = '';
	}
}

if(isset($_POST['AdministrativeFeePercentageAmount']) && $_POST['AdministrativeFeePercentageAmount'] != ''){
	if(!is_numeric($_POST['AdministrativeFeePercentageAmount']) || $_POST['AdministrativeFeePercentageAmount'] < 0.00){
		$_POST['AdministrativeFeePercentageAmount'] = '';
	}
}

if(isset($_POST['FPSMonthlyWaiveCount']) && $_POST['FPSMonthlyWaiveCount'] != ''){
	if(!is_numeric($_POST['FPSMonthlyWaiveCount']) || $_POST['FPSMonthlyWaiveCount'] < 0 || !(ctype_digit(strval($_POST['FPSMonthlyWaiveCount'])))){
		$_POST['FPSMonthlyWaiveCount'] = '';
	}
}

if($_POST['ServiceProvider'] == 'FPS') {
	$_POST['AdministrativeFeePercentageAmount'] = '';
} else {
	$_POST['FPSMonthlyWaiveCount'] = '';
}

$upsert_success = $lpayment->upsertMerchantAccount($_POST);

if($upsert_success){
	if($_POST['ServiceProvider'] == 'FPS') {
		if($_POST['FPSMonthlyWaiveCount'] != '') {
			$new_quota = $_POST['FPSMonthlyWaiveCount'];
			if($is_edit) {

			} else {
				$AccountID = $lpayment->db_insert_id();
			}

			if($AccountID) {
				$year = date('Y');
				$month = date('m');
				$sql = "SELECT * FROM PAYMENT_MERCHANT_ACCOUNT_QUOTA WHERE Year='$year' AND Month='$month' AND AccountID='$AccountID'";
				$rs_quota = $lpayment->returnArray($sql);
				if(count($rs_quota)>0) {
					$sql = "UPDATE PAYMENT_MERCHANT_ACCOUNT_QUOTA SET Quota='$new_quota', ModifiedDate=now(), ModifiedBy='".$_SESSION['UserID']."' WHERE Year='$year' AND Month='$month' AND AccountID='$AccountID'";
					$lpayment->db_db_query($sql);
				} else {
					$sql = "INSERT INTO PAYMENT_MERCHANT_ACCOUNT_QUOTA SET Quota='$new_quota', DateInput=now(), ModifiedDate=now(), ModifiedBy='".$_SESSION['UserID']."', Year='$year', Month='$month', AccountID='$AccountID'";
					$lpayment->db_db_query($sql);
				}
			}
		}
	}
	$flashData['return_msg'] = $is_edit? $Lang['General']['ReturnMessage']['UpdateSuccess'] : $Lang['General']['ReturnMessage']['AddSuccess'];
}else{
	$flashData['return_msg'] = $is_edit? $Lang['General']['ReturnMessage']['UpdateUnsuccess'] : $Lang['General']['ReturnMessage']['AddUnsuccess'];
}
$_SESSION['EPAYMENT_MERCHANT_ACCOUNT_FLASH_DATA'] = $flashData;
intranet_closedb();
header("Location:index.php");
?>