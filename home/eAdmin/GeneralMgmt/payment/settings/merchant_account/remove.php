<?php
// Editing by 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !($lpayment->useEWalletMerchantAccount() || $lpayment->isEWalletTopUpEnabled())) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

if(!isset($AccountID) || empty($AccountID)){
	$flashData['return_msg'] = $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
	$_SESSION['EPAYMENT_MERCHANT_ACCOUNT_FLASH_DATA'] = $flashData;
	intranet_closedb();
	header("Location:index.php");
	exit;
}

//$lpayment = new libpayment();

$remove_success = $lpayment->deleteMerchantAccount($AccountID);

$flashData['return_msg'] = $remove_success? $Lang['General']['ReturnMessage']['DeleteSuccess'] : $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
$_SESSION['EPAYMENT_MERCHANT_ACCOUNT_FLASH_DATA'] = $flashData;
intranet_closedb();
header("Location:index.php");
?>