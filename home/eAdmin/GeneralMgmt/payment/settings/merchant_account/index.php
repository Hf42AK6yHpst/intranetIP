<?php
// Editing by 
/*
 * 2020-06-29 Ray: Added multi payment gateway
 * 2019-08-12 Carlos: Added column HandlingFee.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !($lpayment->useEWalletMerchantAccount() || $lpayment->isEWalletTopUpEnabled())) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$linterface = new interface_html();
//$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "BasicSettings_MerchantAccount";

$field = trim(urldecode(stripslashes($_REQUEST['field'])));
$order = trim(urldecode(stripslashes($_REQUEST['order'])));
$pageNo = trim(urldecode(stripslashes($_REQUEST['pageNo'])));
$numPerPage = trim(urldecode(stripslashes($_REQUEST['numPerPage'])));

$arrCookies[] = array("ck_epayment_merchant_accounts_page_size", "numPerPage");
$arrCookies[] = array("ck_epayment_merchant_accounts_page_number", "pageNo");
$arrCookies[] = array("ck_epayment_merchant_accounts_page_order", "order");
$arrCookies[] = array("ck_epayment_merchant_accounts_page_field", "field");	
updateGetCookies($arrCookies);

if(!isset($field) || $field == '' || !in_array($field,array(1,2,3,4,5,6,7))) $field = 0; 
if(!isset($order) || $order == '' || !in_array($order,array(0,1))) $order = 0;
if(!isset($pageNo) || $pageNo == '') $pageNo = 1;

$pageSizeChangeEnabled = true;
if (isset($ck_epayment_merchant_accounts_page_size) && $ck_epayment_merchant_accounts_page_size != "") $page_size = $ck_epayment_merchant_accounts_page_size;
$li = new libdbtable2007($field,$order,$pageNo);

$status_data = array();
$status_data[] = array('1',$i_general_active);
$status_data[] = array('0',$i_general_inactive);
$status_selection = $linterface->GET_SELECTION_BOX($status_data, ' id="RecordStatus" name="RecordStatus" onchange="document.form1.submit();" ', $Lang['General']['All'], $RecordStatus, true);

$payment_gateway = array();
$service_provider_list = $lpayment->getPaymentServiceProviderMapping(false,true);
foreach($service_provider_list as $temp) {
	$payment_gateway[] = $temp;
}
$payment_gateway = implode(', ', $payment_gateway);
$payment_method = array();
if($lpayment->isEWalletDirectPayEnabled()) {
	$payment_method[] = $Lang['ePayment']['DirectPay'];
	if($lpayment->isEWalletTopUpEnabled()) {
		$payment_method[] = $Lang['ePayment']['TopUp'];
	}
} else {
	$payment_method[] = $Lang['ePayment']['TopUp'];
}
$payment_method = implode(', ', $payment_method);


$service_provider_map = $lpayment->getPaymentServiceProviderMapping();
$service_provider_field = "CASE ";
foreach($service_provider_map as $key => $val){
	$service_provider_field .= "WHEN a.ServiceProvider='$key' THEN '$val' ";
}
$service_provider_field.= "END ";

$modifier_name_field = getNameFieldByLang2("u.");

$more_field = '';
if ($sys_custom['ePayment']['MultiPaymentGateway']) {
	$more_field = "	IF(a.AdministrativeFeeFixAmount > 0, ".$lpayment->getWebDisplayAmountFormatDB("a.AdministrativeFeeFixAmount").", IF(a.AdministrativeFeePercentageAmount > 0, CONCAT(".$lpayment->getExportAmountFormatDB("a.AdministrativeFeePercentageAmount").", '%'),'".$Lang['General']['EmptySymbol']."')) as AdministrativeFee, ";
}

$sql = "SELECT 
			$service_provider_field as ServiceProvider,
			CONCAT('<span style=\"display:none\">',a.AccountName,'</span><a href=\"edit.php?AccountID=',a.AccountID,'\">',a.AccountName,'</a>') as AccountName,
			a.MerchantAccount,
			a.MerchantUID,
			IF(a.HandlingFee IS NULL,'".$Lang['General']['EmptySymbol']."',".$lpayment->getWebDisplayAmountFormatDB("a.HandlingFee").") as HandlingFee,
			$more_field	
			IF(a.RecordStatus='1','<span style=\"color:green\">$i_general_active</span>','<span style=\"color:red\">$i_general_inactive</span>') as RecordStatus,
			a.ModifiedDate,
			$modifier_name_field as ModifiedName,
			CONCAT('<input type=\"checkbox\" name=\"AccountID[]\" value=\"',a.AccountID,'\">') as CheckBox 
		FROM PAYMENT_MERCHANT_ACCOUNT as a 
		LEFT JOIN INTRANET_USER as u ON a.ModifiedBy=u.UserID 
		WHERE 1 ";
if($RecordStatus != '' && in_array($RecordStatus,array('1','0',1,0))){
	$sql .= " AND a.RecordStatus='$RecordStatus' ";
}

$li->sql = $sql;
$li->IsColOff = "IP25_table";

$field_array = array("a.ServiceProvider","a.AccountName","a.MerchantAccount","a.MerchantUID","a.HandlingFee");
if ($sys_custom['ePayment']['MultiPaymentGateway']) {
	$field_array[] = "AdministrativeFee";
}
$field_array[] = "RecordStatus";
$field_array[] = "a.ModifiedDate";
$field_array[] = "ModifiedName";

$li->field_array = $field_array;
$li->column_array = array(22,18,22,22,22,22,18,22,22);
$li->wrap_array = array(0,0,0,0,0,0,0,0);

$pos = 0;
$li->column_list .= "<th width=\"1\" class=\"num_check\">#</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['ePayment']['ServiceProvider'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['ePayment']['MerchantAccountName'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['ePayment']['MerchantAccount'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['ePayment']['MerchantUID'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['ePayment']['HandlingFee'])."</th>\n";
if ($sys_custom['ePayment']['MultiPaymentGateway']) {
	$li->column_list .= "<th>" . $li->column($pos++, $Lang['ePayment']['AdministrativeFee']) . "</th>\n";
}
$li->column_list .= "<th>".$li->column($pos++, $Lang['General']['Status'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['General']['LastUpdatedTime'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['General']['LastUpdatedBy'])."</th>\n";
$li->column_list .= "<th width=\"1\">".$li->check("AccountID[]")."</th>\n";
$li->no_col = $pos+2;


$userObj = new libuser($_SESSION['UserID']);
$is_broadlearning_account = ($userObj->UserLogin == 'broadlearning') ? true : false;


$tool_buttons = array();
$tool_buttons[] = array('edit',"javascript:checkEdit(document.form1,'AccountID[]','edit.php')");
if($is_broadlearning_account) {
	$tool_buttons[] = array('delete', "javascript:checkRemove(document.form1,'AccountID[]','remove.php')");
}

### Title ###
$TAGS_OBJ[] = array($Lang['ePayment']['MerchantAccount'],"",0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
//$Msg = urldecode($Msg);
$flashData = array();
if(isset($_SESSION['EPAYMENT_MERCHANT_ACCOUNT_FLASH_DATA'])){
	foreach($_SESSION['EPAYMENT_MERCHANT_ACCOUNT_FLASH_DATA'] as $key => $val){
		$flashData[$key] = $val;
	}
	unset($_SESSION['EPAYMENT_MERCHANT_ACCOUNT_FLASH_DATA']);
}
if(isset($flashData['return_msg']))
{
	$Msg = $flashData['return_msg'];
}
$linterface->LAYOUT_START($Msg); 
?>
<br />
<?php if($sys_custom['ePayment']['MultiPaymentGateway']) { ?>
    <table  class="form_table_v30">
        <tr valign="top">
            <td valign="top" nowrap="nowrap" class="field_title">
				<?=$Lang['ePayment']['PaymentGateway']?>
            </td>
            <td>
               <?=$payment_gateway?>
            </td>
        </tr>
        <tr valign="top">
            <td valign="top" nowrap="nowrap" class="field_title">
				<?=$Lang['ePayment']['PaymentMethod']?>
            </td>
            <td>
                <?=$payment_method?>
            </td>
        </tr>
    </table>
<?php } ?>
<form id="form1" name="form1" method="post" action="">
	<div class="content_top_tool">
		<div class="Conntent_tool">
            <?php if($is_broadlearning_account) { ?>
			<?=$linterface->GET_LNK_NEW('edit.php', $Lang['Btn']['New']);?>
            <?php } ?>
		</div>
		<br style="clear:both;">
	</div>
	<br style="clear:both;">
	<div class="table_filter">
		<?=$Lang['General']['Status'].': '.$status_selection?>
	</div>
	<br style="clear:both">	
	<?=$linterface->Get_DBTable_Action_Button_IP25($tool_buttons)?>
	<br style="clear:both">			
	<?=$li->display();?>
	<input type="hidden" id="pageNo" name="pageNo" value="<?=$li->pageNo?>">
	<input type="hidden" id="order" name="order" value="<?=$li->order?>">
	<input type="hidden" id="field" name="field" value="<?=$li->field?>">
	<input type="hidden" id="page_size_change" name="page_size_change" value="">
	<input type="hidden" id="numPerPage" name="numPerPage" value="<?=$li->page_size?>">
</form>	
<br /><br />
<script type="text/javascript">

</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>