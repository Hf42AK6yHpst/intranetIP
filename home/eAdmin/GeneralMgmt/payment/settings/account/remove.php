<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$li = new libdb();
$list = "'".implode("','",$TerminalUserID)."'";

$sql = "DELETE FROM PAYMENT_TERMINAL_USER WHERE TerminalUserID IN ($list)";
$li->db_db_query($sql);
header ("Location: index.php?msg=3");

intranet_closedb();
?>
