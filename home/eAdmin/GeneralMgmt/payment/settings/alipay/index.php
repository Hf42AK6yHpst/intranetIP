<?php
/*
 * 2019-08-07 (Ray):  created
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lpayment = new libpayment();
if(!$lpayment->isAlipayDirectPayEnabled() && !$lpayment->isAlipayTopUpEnabled()) {
	intranet_closedb();
	header("Location: /");
	exit();
}

$GeneralSetting = new libgeneralsettings();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "BasicSettings_AlipayHK_Handling_Fee";

$linterface = new interface_html();

$SettingList[] = "'AlipayHKHandlingFee'";
$Settings = $GeneralSetting->Get_General_Setting('ePayment',$SettingList);

$alipayhk_handling_fee = $Settings['AlipayHKHandlingFee'];
if(empty($alipayhk_handling_fee)) {
	$alipayhk_handling_fee = 0;
}

$TAGS_OBJ[] = array($i_Payment_Menu_Settings_AlipayHK_Handling_Fee, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();

$Msg = urldecode($Msg);
$linterface->LAYOUT_START($Msg);

if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
?>



<script language="javascript">
    <!--
    function EditInfo()
    {
        $('#EditBtn').attr('style', 'visibility: hidden');
        $('.Edit_Hide').attr('style', 'display: none');
        $('.Edit_Show').attr('style', '');

        $('#UpdateBtn').attr('style', '');
        $('#cancelbtn').attr('style', '');
    }

    function ResetInfo()
    {
        document.form1.reset();
        $('#EditBtn').attr('style', '');
        $('.Edit_Hide').attr('style', '');
        $('.Edit_Show').attr('style', 'display: none');

        $('#UpdateBtn').attr('style', 'display: none');
        $('#cancelbtn').attr('style', 'display: none');
    }
    function restrictNumber(obj)
    {
        var val = $.trim(obj.value);
        var num = parseFloat(val);

        if(isNaN(num) || val == '' || num < 0){
            num = 0;
        }
        obj.value = num;
    }
    //-->
</script>

<form name="form1" method="post" action="update.php">

    <div class="table_board">
        <table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
            <tr>
                <td class="tabletext" align="right" colspan="2"><?=$SysMsg?></td>
            </tr>
        </table>
        <div class="table_row_tool row_content_tool">
            <?= $linterface->GET_SMALL_BTN($button_edit, "button","javascript:EditInfo();","EditBtn")?>
        </div>
        <p class="spacer"></p>
        <div class="form_sub_title_v30"><em>- <span class="field_title"> <?=$Lang['ePayment']['Alipay']?> </span>-</em>
            <p class="spacer"></p>
        </div>
        <table class="form_table_v30">

            <tr valign='top'>
                <td class="field_title" nowrap><?=$i_Payment_Menu_Settings_AlipayHK_Handling_Fee?></td>
                <td>
                    <span class="Edit_Hide">$<?=$alipayhk_handling_fee?></span>
                    <span class="Edit_Show" style="display:none">
                        $<input type="text" name="AlipayHKHandlingFee" value="<?=$alipayhk_handling_fee?>" id="AlipayHKHandlingFee" onchange="restrictNumber(this);">
                    </span>
                </td>
            </tr>

        </table>

        <div class="edit_bottom_v30">
            <p class="spacer"></p>
            <?= $linterface->GET_ACTION_BTN($button_update, "submit","","UpdateBtn", "style='display: none'") ?>
            <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "ResetInfo();","cancelbtn", "style='display: none'") ?>
            <p class="spacer"></p>
        </div>
    </div>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>

