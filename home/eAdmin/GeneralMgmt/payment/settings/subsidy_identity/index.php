<?php
// Editing by
/*
 * 2019-07-02 (Ray): Added import, export
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$linterface = new interface_html();

$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "SubsidyIdentitySettings";

$field = trim(urldecode(stripslashes($_REQUEST['field'])));
$order = trim(urldecode(stripslashes($_REQUEST['order'])));
$pageNo = trim(urldecode(stripslashes($_REQUEST['pageNo'])));
$numPerPage = trim(urldecode(stripslashes($_REQUEST['numPerPage'])));

$arrCookies[] = array("ck_epayment_subsidy_identity_page_size", "numPerPage");
$arrCookies[] = array("ck_epayment_subsidy_identity_page_number", "pageNo");
$arrCookies[] = array("ck_epayment_subsidy_identity_page_order", "order");
$arrCookies[] = array("ck_epayment_subsidy_identity_page_field", "field");	
updateGetCookies($arrCookies);

if(!isset($field) || $field == '' || !in_array($field,array(0,1,2,3,))) $field = 0; 
if(!isset($order) || $order == '' || !in_array($order,array(0,1))) $order = 0;
if(!isset($pageNo) || $pageNo == '') $pageNo = 1;

$pageSizeChangeEnabled = true;
if (isset($ck_epayment_subsidy_identity_page_size) && $ck_epayment_subsidy_identity_page_size != "") $page_size = $ck_epayment_subsidy_identity_page_size;
$li = new libdbtable2007($field,$order,$pageNo);

//$status_data = array();
//$status_data[] = array('1',$i_general_active);
//$status_data[] = array('0',$i_general_inactive);
//$status_selection = $linterface->GET_SELECTION_BOX($status_data, ' id="RecordStatus" name="RecordStatus" onchange="document.form1.submit();" ', $Lang['General']['All'], $RecordStatus, true);


$modifier_name_field = getNameFieldByLang2("u.");

$sql = "SELECT 
			CONCAT('<span style=\"display:none\">',i.IdentityName,'</span><a href=\"edit.php?IdentityID=',i.IdentityID,'\">',i.IdentityName,'</a>') as IdentityName,
			i.Code,
			CONCAT('<span style=\"display:none\">',COUNT(s.StudentID),'</span><a href=\"student_list.php?IdentityID=',i.IdentityID,'\">',COUNT(s.StudentID),'</a>') as StudentCount,
			i.ModifiedDate,
			$modifier_name_field as ModifiedName,
			CONCAT('<input type=\"checkbox\" name=\"IdentityID[]\" value=\"',i.IdentityID,'\">') as CheckBox 
		FROM PAYMENT_SUBSIDY_IDENTITY as i 
		LEFT JOIN PAYMENT_SUBSIDY_IDENTITY_STUDENT as s ON s.IdentityID=i.IdentityID 
		LEFT JOIN INTRANET_USER as u ON i.ModifiedBy=u.UserID 
		WHERE i.RecordStatus='1' 
		GROUP BY i.IdentityID ";
//if($RecordStatus != '' && in_array($RecordStatus,array('1','0',1,0))){
//	$sql .= " AND a.RecordStatus='$RecordStatus' ";
//}

$li->sql = $sql;
$li->IsColOff = "IP25_table";

$li->field_array = array("i.IdentityName","i.Code","StudentCount","i.ModifiedDate","ModifiedName");
$li->column_array = array(0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0);

$pos = 0;
$li->column_list .= "<th width=\"1\" class=\"num_check\">#</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['ePayment']['SubsidyIdentityName'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['ePayment']['SubsidyIdentityCode'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $i_ClubsEnrollment_NumOfStudent)."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['General']['LastUpdatedTime'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['General']['LastUpdatedBy'])."</th>\n";
$li->column_list .= "<th width=\"1%\">".$li->check("IdentityID[]")."</th>\n";
$li->no_col = $pos+2;



$tool_buttons = array();
$tool_buttons[] = array('edit',"javascript:checkEdit(document.form1,'IdentityID[]','edit.php')");
$tool_buttons[] = array('delete',"javascript:checkRemove(document.form1,'IdentityID[]','remove.php')");

### Title ###
$TAGS_OBJ[] = array($Lang['ePayment']['SubsidyIdentitySettings'],"",0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
//$Msg = urldecode($Msg);
$flashData = array();
if(isset($_SESSION['EPAYMENT_SUBSIDY_IDENTITY_FLASH_DATA'])){
	foreach($_SESSION['EPAYMENT_SUBSIDY_IDENTITY_FLASH_DATA'] as $key => $val){
		$flashData[$key] = $val;
	}
	unset($_SESSION['EPAYMENT_SUBSIDY_IDENTITY_FLASH_DATA']);
}
if(isset($flashData['return_msg']))
{
	$Msg = $flashData['return_msg'];
}
$linterface->LAYOUT_START($Msg); 
?>
<br />
<form id="form1" name="form1" method="post" action="">
	<div class="content_top_tool">
        <div style="float: left; margin-right: 15px;">
            <?=$linterface->GET_LNK_NEW('edit.php', $Lang['Btn']['New'],"","","",0);?>
        </div>
        <div style="float: left; margin-right: 15px;">
            <?=$linterface->GET_LNK_IMPORT("javascript:checkNew('import.php')",$Lang['Btn']['ImportStudent'],"","","",0);?>
        </div>

        <div class="Conntent_tool">
            <a href="javascript:;"  onClick="checkArrowLayer('exportImgID','exportArrow','export_option')" class="contenttool export" onMouseOver="swapImage('exportImgID','exportArrow')" onMouseOut="swapImage('exportImgID','exportArrow')" onClick="checkExportLayer('exportArrow')"><?=$Lang['SysMgr']['FormClassMapping']['Export']?><img src="<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/selectbox_arrow_open.gif" align="absmiddle" border="0" id="exportArrow"></a>
            <div class="print_option_layer2" id="export_option" style="visibility:hidden">
                <table>
                    <tr>
                        <td>
                            <a href="javascript:exportClassNameClassNumber()" class="blank_content"><img src="<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/btn_sub.gif" align="absmiddle" border="0"><?=$Lang['ePayment']['SubsidyIdentityExportClassNameClassNumber']?></a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="javascript:exportLoginId()" class="blank_content"><img src="<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/btn_sub.gif" align="absmiddle" border="0"><?=$Lang['ePayment']['SubsidyIdentityExportLoginId']?></a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <input type="hidden" name="exportImgID" id="exportImgID" value="0">

    </div>
	<br style="clear:both;">
	<!--
	<div class="table_filter">
		<?=$Lang['General']['Status'].': '.$status_selection?>
	</div>
	<br style="clear:both">
	-->	
	<?=$linterface->Get_DBTable_Action_Button_IP25($tool_buttons)?>
	<br style="clear:both">			
	<?=$li->display();?>
	<input type="hidden" id="pageNo" name="pageNo" value="<?=$li->pageNo?>">
	<input type="hidden" id="order" name="order" value="<?=$li->order?>">
	<input type="hidden" id="field" name="field" value="<?=$li->field?>">
	<input type="hidden" id="page_size_change" name="page_size_change" value="">
	<input type="hidden" id="numPerPage" name="numPerPage" value="<?=$li->page_size?>">
</form>	
<br /><br />
<script type="text/javascript">
function exportClassNameClassNumber()
{
    window.location="export.php?format=1";
}
function exportLoginId()
{
    window.location="export.php?format=2";
}
var imgArrowAry = new Array();
imgArrowAry[0] = "/images/<?=$LAYOUT_SKIN?>/selectbox_arrow_open.gif";
imgArrowAry[1] = "/images/<?=$LAYOUT_SKIN?>/selectbox_arrow_open_on.gif";
imgArrowAry[2] = "/images/<?=$LAYOUT_SKIN?>/selectbox_arrow_close.gif";
imgArrowAry[3] = "/images/<?=$LAYOUT_SKIN?>/selectbox_arrow_close_on.gif";

function swapImage(imgFlag, imgFile) {
    if(document.getElementById(imgFlag).value%2==0) {
        //alert(imgArrowAry[parseInt(document.getElementById(imgFlag).value)+1]);
        document.getElementById(imgFile).src = imgArrowAry[parseInt(document.getElementById(imgFlag).value)+1];
        document.getElementById(imgFlag).value = parseInt(document.getElementById(imgFlag).value)+1;
    } else {
        document.getElementById(imgFile).src = imgArrowAry[parseInt(document.getElementById(imgFlag).value)-1];
        document.getElementById(imgFlag).value = parseInt(document.getElementById(imgFlag).value)-1;
    }
}
function closeAllSpan() {
    MM_showHideLayers('export_option','','hide');
    document.getElementById('exportArrow').src = imgArrowAry[0];
    document.getElementById('exportImgID').value = 0;

}
function checkArrowLayer(imgFlag,imgName,spanName) {
    if(document.getElementById(spanName).style.visibility=="hidden") {
        closeAllSpan();
        document.getElementById(spanName).style.visibility = "visible";
        document.getElementById(imgFlag).value = 2;
        swapImage(imgFlag, imgName);
    } else {
        closeAllSpan();
        document.getElementById(spanName).style.visibility = "hidden";
        document.getElementById(imgFlag).value = 0;
        swapImage(imgFlag, imgName);
    }
}
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>