<?php
// Editing by 
/*
 * 2019-08-23 (Carlos): Added Effective Period.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !isset($IdentityID) || $IdentityID=='') {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$records = $lpayment->getSubsidyIdentity(array('IdentityID'=>$IdentityID));
if(count($records)==0){
	$flashData['return_msg'] = '0|=|'.$Lang['General']['InvalidData'];
	$_SESSION['EPAYMENT_SUBSIDY_IDENTITY_FLASH_DATA'] = $flashData;
	intranet_closedb();
	header ("Location: index.php");
	exit();
}

$IdentityID = IntegerSafe($IdentityID);

$identity_name = $records[0]['IdentityName'];

$linterface = new interface_html();

$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "SubsidyIdentitySettings";

$field = trim(urldecode(stripslashes($_REQUEST['field'])));
$order = trim(urldecode(stripslashes($_REQUEST['order'])));
$pageNo = trim(urldecode(stripslashes($_REQUEST['pageNo'])));
$numPerPage = trim(urldecode(stripslashes($_REQUEST['numPerPage'])));

$arrCookies[] = array("ck_epayment_subsidy_identity_page_size", "numPerPage");
$arrCookies[] = array("ck_epayment_subsidy_identity_page_number", "pageNo");
$arrCookies[] = array("ck_epayment_subsidy_identity_page_order", "order");
$arrCookies[] = array("ck_epayment_subsidy_identity_page_field", "field");	
updateGetCookies($arrCookies);

if(!isset($field) || $field == '' || !in_array($field,array(0,1,2,3,4))) $field = 0; 
if(!isset($order) || $order == '' || !in_array($order,array(0,1))) $order = 0;
if(!isset($pageNo) || $pageNo == '') $pageNo = 1;

$pageSizeChangeEnabled = true;
if (isset($ck_epayment_subsidy_identity_page_size) && $ck_epayment_subsidy_identity_page_size != "") $page_size = $ck_epayment_subsidy_identity_page_size;
$li = new libdbtable2007($field,$order,$pageNo);


$modifier_name_field = getNameFieldByLang2("u.");
$intranet_user_name = Get_Lang_Selection("u1.ChineseName","u1.EnglishName");
$intranet_archive_user_name = Get_Lang_Selection("u2.ChineseName","u2.EnglishName");

$sql = "SELECT 
			IF(u1.UserID IS NOT NULL,u1.ClassName,u2.ClassName) as ClassName,
			IF(u1.UserID IS NOT NULL,u1.ClassNumber,u2.ClassNumber) as ClassNumber,
			IF(u1.UserID IS NOT NULL,$intranet_user_name,$intranet_archive_user_name) as StudentName,
		    IF(s.EffectiveStartDate IS NOT NULL AND s.EffectiveEndDate IS NOT NULL,CONCAT(s.EffectiveStartDate,' ".$Lang['General']['To']." ',s.EffectiveEndDate),IF(s.EffectiveStartDate IS NOT NULL AND s.EffectiveEndDate IS NULL, s.EffectiveStartDate,'".$Lang['General']['EmptySymbol']."')) as EffectivePeriod,
			s.InputDate,
			$modifier_name_field as InputUserName,
			CONCAT('<input type=\"checkbox\" name=\"StudentID[]\" value=\"',s.StudentID,'\">') as CheckBox 
		FROM PAYMENT_SUBSIDY_IDENTITY_STUDENT as s
		LEFT JOIN INTRANET_USER as u1 ON s.StudentID=u1.UserID 
		LEFT JOIN INTRANET_ARCHIVE_USER as u2 ON s.StudentID=u2.UserID 
		LEFT JOIN INTRANET_USER as u ON u.UserID=s.InputBy 
		WHERE s.IdentityID='$IdentityID' ";


$li->sql = $sql;
$li->IsColOff = "IP25_table";

$li->field_array = array("ClassName","ClassNumber","StudentName","EffectivePeriod","s.InputDate","InputUserName");
$li->column_array = array(0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0);

$pos = 0;
$li->column_list .= "<th width=\"1\" class=\"num_check\">#</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['SubsidyIdentity']['ClassName'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['SubsidyIdentity']['ClassNumber'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['SubsidyIdentity']['StudentName'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['SubsidyIdentity']['EffectivePeriod'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['General']['LastUpdatedTime'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['General']['LastUpdatedBy'])."</th>\n";
$li->column_list .= "<th width=\"1%\">".$li->check("StudentID[]")."</th>\n";
$li->no_col = $pos+2;



$tool_buttons = array();
$tool_buttons[] = array('edit',"javascript:checkEdit(document.form1,'StudentID[]','edit_student.php')");
$tool_buttons[] = array('delete',"javascript:checkRemove(document.form1,'StudentID[]','remove_students.php')");

$pages_arr = array(
	array($Lang['ePayment']['SubsidyIdentity'],'index.php'),
	array($identity_name, '')
);

### Title ###
$TAGS_OBJ[] = array($Lang['ePayment']['SubsidyIdentitySettings'],"",0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
//$Msg = urldecode($Msg);
$flashData = array();
if(isset($_SESSION['EPAYMENT_SUBSIDY_IDENTITY_FLASH_DATA'])){
	foreach($_SESSION['EPAYMENT_SUBSIDY_IDENTITY_FLASH_DATA'] as $key => $val){
		$flashData[$key] = $val;
	}
	unset($_SESSION['EPAYMENT_SUBSIDY_IDENTITY_FLASH_DATA']);
}
if(isset($flashData['return_msg']))
{
	$Msg = $flashData['return_msg'];
}
$linterface->LAYOUT_START($Msg); 
?>
<br />
<?=$linterface->GET_NAVIGATION_IP25($pages_arr)?>
<br />
<form id="form1" name="form1" method="post" action="">
	<div class="content_top_tool">
		<span style="float:left"><?=$linterface->GET_LNK_NEW('add_students.php?IdentityID='.$IdentityID, $eDiscipline["AddStudents"],"","","",0);?></span>
		<br style="clear:both;">
	</div>
	<br style="clear:both;">
	<div class="table_filter">
		
	</div>
	<br style="clear:both">
	<?=$linterface->Get_DBTable_Action_Button_IP25($tool_buttons)?>
	<br style="clear:both">			
	<?=$li->display();?>
	<input type="hidden" id="IdentityID" name="IdentityID" value="<?=$IdentityID?>">
	<input type="hidden" id="pageNo" name="pageNo" value="<?=$li->pageNo?>">
	<input type="hidden" id="order" name="order" value="<?=$li->order?>">
	<input type="hidden" id="field" name="field" value="<?=$li->field?>">
	<input type="hidden" id="page_size_change" name="page_size_change" value="">
	<input type="hidden" id="numPerPage" name="numPerPage" value="<?=$li->page_size?>">
</form>	
<br /><br />
<script type="text/javascript">

</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>