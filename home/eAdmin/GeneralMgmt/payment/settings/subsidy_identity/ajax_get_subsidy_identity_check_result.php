<?php
// Editing by
/*
 * 2019-08-27 (Ray): Created
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();


if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();

$StudentID = IntegerSafe($_REQUEST['StudentID']); // array
$IdentityID = IntegerSafe($_REQUEST['IdentityID']);

$records = array();
$NameField = getNameFieldWithClassNumberByLang("u.");
$sql = "SELECT s.StudentID,$NameField as 'StudentName', s.IdentityID, s.EffectiveStartDate, s.EffectiveEndDate, si.IdentityName, si.Code
FROM PAYMENT_SUBSIDY_IDENTITY_STUDENT as s
INNER JOIN INTRANET_USER as u ON s.StudentID=u.UserID 
INNER JOIN PAYMENT_SUBSIDY_IDENTITY as si ON si.IdentityID=s.IdentityID
WHERE s.StudentID IN ('".implode("','",$StudentID)."')
";

$students_records = $lpayment->returnResultSet($sql);
$y = '';
foreach($students_records as $record) {
	if($record['IdentityID'] == $IdentityID) {
		continue;
	}

	$y .= '<tr>';
	$y .= '<td>'.$record['StudentName'].'</td>';
	$y .= '<td>'.$record['IdentityName'].'('.$record['Code'].')</td>';
	$y .= '</tr>';
}

if($y != '') {
	$x = '<table class="common_table_list_v30">';
	$x .= '<thead>';
	$x .= '<th style="width:50%">' . $i_UserStudentName . '</th>';
	$x .= '<th>' . $Lang['ePayment']['SubsidyIdentityName'] . '</th>';
	$x .= '</tr>';
	$x .= '</thead>';
	$x .= '<tbody>';

	$linterface = new interface_html();
	$warning_box = $linterface->Get_Warning_Message_Box('',$x.$y,'');

	echo $warning_box;

}




intranet_closedb();

?>