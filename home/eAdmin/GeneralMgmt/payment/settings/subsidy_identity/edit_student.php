<?php
// Editing by 
/*
 * 2020-04-03 Ray: allow effective end date empty
 * 2019-08-23 Carlos: Created for editing one student's effective period.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$linterface = new interface_html();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "SubsidyIdentitySettings";

$flashData = array();
if(isset($_SESSION['EPAYMENT_SUBSIDY_IDENTITY_FLASH_DATA'])){
	foreach($_SESSION['EPAYMENT_SUBSIDY_IDENTITY_FLASH_DATA'] as $key => $val){
		$flashData[$key] = $val;
	}
	unset($_SESSION['EPAYMENT_SUBSIDY_IDENTITY_FLASH_DATA']);
}

if(!isset($IdentityID) || empty($IdentityID) || !isset($StudentID) || count($StudentID)!=1){
	$flashData['return_msg'] = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
	$_SESSION['EPAYMENT_SUBSIDY_IDENTITY_FLASH_DATA'] = $flashData;
	intranet_closedb();
	header("Location:student_list.php".($IdentityID!=''?"?IdentityID=".$IdentityID:""));
	exit;
}

if(is_array($StudentID)){
	$StudentID = $StudentID[0];
}
$IdentityID = IntegerSafe($IdentityID);
$StudentID = IntegerSafe($StudentID);

$records = $lpayment->getSubsidyIdentityStudents(array('IdentityID'=>$IdentityID,'StudentID'=>$StudentID,'GetStudentUser'=>true));
if(count($records)==0){
	$flashData['return_msg'] = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
	$_SESSION['EPAYMENT_SUBSIDY_IDENTITY_FLASH_DATA'] = $flashData;
	intranet_closedb();
	header("Location:student_list.php?IdentityID=".$IdentityID);
	exit;
}

$record = $records[0];

$identity_records = $lpayment->getSubsidyIdentity(array('IdentityID'=>$IdentityID));
$identity_name = $identity_records[0]['IdentityName'];

$pages_arr = array(
	array($Lang['ePayment']['SubsidyIdentity'],'index.php'),
	array($identity_name, 'student_list.php?IdentityID='.$IdentityID),
	array($Lang['Btn']['Edit'], '')
);

### Title ###
$TAGS_OBJ[] = array($Lang['ePayment']['SubsidyIdentitySettings'],"",0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
//$Msg = urldecode($Msg);
if(isset($flashData['return_msg']))
{
	$Msg = $flashData['return_msg'];
}
$linterface->LAYOUT_START($Msg); 
?>
<br />
<?=$linterface->GET_NAVIGATION_IP25($pages_arr)?>
<br />
<form id="form1" name="form1" method="post" action="edit_student_update.php" onsubmit="return false;">
<?=$hidden_fields?>
<table width="100%" cellpadding="2" class="form_table_v30">
	<col class="field_title">
	<col class="field_c">
	<tr>
		<td class="field_title"><?=$Lang['SysMgr']['FormClassMapping']['ClassTitle']?></td>
		<td>
			<?=Get_String_Display($record['ClassName'])?>
		</td>
	</tr>
	<tr>
		<td class="field_title"><?=$Lang['SysMgr']['FormClassMapping']['ClassNo']?></td>
		<td>
			<?=Get_String_Display($record['ClassNumber'])?>
		</td>
	</tr>
	<tr>
		<td class="field_title"><?=$Lang['SysMgr']['FormClassMapping']['StudentName']?></td>
		<td>
			<?=Get_String_Display(Get_Lang_Selection($record['ChineseName'],$record['EnglishName']))?>
		</td>
	</tr>
	<tr>
		<td class="field_title"><?=$Lang['SubsidyIdentity']['EffectivePeriod']?></td>
		<td>
			<?=$Lang['General']['From'].'&nbsp;'.$linterface->GET_DATE_PICKER("EffectiveStartDate",$record['EffectiveStartDate'],$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1).$Lang['General']['To'].'&nbsp;'.$linterface->GET_DATE_PICKER("EffectiveEndDate",$record['EffectiveEndDate'],$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1)?>
			<div class="tabletextremark">(<?=$Lang['ePayment']['SubsidyIdentityEffectivePeriodRemark']?>)</div>
		</td>
	</tr>
</table>
<div style="float:left;margin-left:8px;">
<?php // echo $linterface->MandatoryField();?>
</div>
<div class="edit_bottom_v30">
<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'],"button","checkSubmitForm(document.form1);",'submit_btn').'&nbsp;'?>
<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.location.href='student_list.php?IdentityID=$IdentityID'",'cancel_btn')?>
</div>
<input type="hidden" id="IdentityID" name="IdentityID" value="<?=$IdentityID?>" />
<input type="hidden" id="StudentID" name="StudentID" value="<?=$StudentID?>" />
</form>	
<br /><br />
<script type="text/javascript">
function validateDate(value)
{
	var dateformat = /^\d{4}-\d{2}-\d{2}$/;
	if(!value.match(dateformat))
	{
		return false;
	}
  
	var parts = value.split('-');
	var year_int = parseInt(parts[0]);
	var month_int = parseInt(parts[1]);
	var day_int = parseInt(parts[2]);
	
	var date_obj = new Date(value);
	var date_year_int = date_obj.getFullYear();
	var date_month_int = date_obj.getMonth() + 1;
	var date_day_int = date_obj.getDate();
	
	if(year_int != date_year_int || month_int != date_month_int || day_int != date_day_int){
		return false;
	}
	
	return true;
}

function checkSubmitForm(formObj)
{
	$('#submit_btn').attr('disabled',true);
	$('#DPWL-EffectiveStartDate').html('').hide();
	$('#DPWL-EffectiveEndDate').html('').hide();
	var valid = true;
	
	var start_date_value = $.trim($('#EffectiveStartDate').val());
	var end_date_value = $.trim($('#EffectiveEndDate').val());
	
	if(start_date_value != '' || end_date_value != ''){
		if(start_date_value == ''){
			valid = false;
			$('#DPWL-EffectiveStartDate').html('<?=$Lang['General']['WarningArr']['InputDate']?>').show();
		}else if(!validateDate(start_date_value)){
			valid = false;
			$('#DPWL-EffectiveStartDate').html('<?=$Lang['General']['InvalidDateFormat']?>').show();
		}
		if(end_date_value == ''){
			//valid = false;
			//$('#DPWL-EffectiveEndDate').html('<?=$Lang['General']['WarningArr']['InputDate']?>').show();
		}else if(!validateDate(end_date_value)){
			valid = false;
			$('#DPWL-EffectiveEndDate').html('<?=$Lang['General']['InvalidDateFormat']?>').show();
		}
		if(start_date_value != '' && end_date_value != '' && end_date_value < start_date_value){
			valid = false;
			$('#DPWL-EffectiveStartDate').html('<?=$Lang['General']['JS_warning']['InvalidDateRange']?>').show();
		}
	}
	
	if(valid){
		formObj.submit();
	}else{
		$('#submit_btn').attr('disabled',false);
	}
}
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>