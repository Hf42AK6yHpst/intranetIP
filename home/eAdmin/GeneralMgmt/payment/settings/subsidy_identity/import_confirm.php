<?php
// Editing by
/*
 * 2019-07-02 (Ray): Created
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libpayment_ui.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");


intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
if (!$lpayment->IS_ADMIN_USER() || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
    intranet_closedb();
    header ("Location: /");
    exit();
}

$lpayment_ui = new libpayment_ui();
$limport = new libimporttext();

$lf = new libfilesystem();
$filepath = $userfile;
$filename = $userfile_name;
if($filepath=="none" || $filepath == ""){          # import failed
    header("Location: import.php?Msg=".urlencode($Lang['ePayment']['DataImportFail']));
    intranet_closedb();
    exit();
} else {
	$toprow = array();
    $ext = strtoupper($lf->file_ext($filename));
    if($limport->CHECK_FILE_EXT($filename))
    {
        # read file into array
        # return 0 if fail, return csv array if success
        //$data = $lf->file_read_csv($filepath);
        $data = $limport->GET_IMPORT_TXT($filepath);
        if(sizeof($data)>0)
        {
            $toprow[] = array_shift($data);                   # drop the title bar
			$toprow[] = array_shift($data);
        }else
        {
            header("Location: import.php?Msg=".urlencode($Lang['ePayment']['DataImportFail']));
            intranet_closedb();
            exit();
        }
    }
    $format_array = $lpayment->Get_Subsidy_Identity_Format_Array($format);
    for ($i=0; $i<sizeof($format_array); $i++)
    {
		for ($j=0; $j<sizeof($format_array[$i]); $j++) {
			if ($toprow[$i][$j] != $format_array[$i][$j]) {
				header("Location: import.php?Msg=" . urlencode($Lang['ePayment']['DataImportFail']));
				intranet_closedb();
				exit();
			}
		}
    }

    $CurrentPageArr['ePayment'] = 1;
    $CurrentPage = "SubsidyIdentitySettings";
    $linterface = new interface_html();

    $TAGS_OBJ[] = array($Lang['ePayment']['SubsidyIdentitySettings'], "", 0);

    $MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
    $linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));
    echo $lpayment_ui->Get_Subsidy_Identity_Import_confirm($data, $format);
}

$linterface->LAYOUT_STOP();
intranet_closedb();

