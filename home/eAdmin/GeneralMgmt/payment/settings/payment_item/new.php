<?php
// Editing by 
/*
 * 2020-10-30 (Ray):    Add quota
 * 2020-06-29 (Ray):    Add multi payment gateway
 * 2019-09-27 (Ray):    Add Item Code
 * 2019-07-24 (Ray):    Add SendReminderPushMessage
 * 2018-07-17 (Carlos): $sys_custom['ePayment']['Alipay'] - Added Merchant Account selection for Alipay.
 * 2016-11-18 (Carlos): Improved to have option to choose individual students.
 * 2016-06-22 (Carlos): $sys_custom['ePayment']['PaymentItemAllowNegativeAmount'] - cust to allow input negative amount for payment item.
 * 2014-08-20 (Carlos): $sys_custom['ePayment']['PaymentItemChangeLog'] - added navigation tag [Payment Item Change Log]
 * 2014-01-16 (Carlos): Fixed the check whole level checkbox of the classes check list
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PaymentItemSettings";

$linterface = new interface_html();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$use_merchant_account = $lpayment->useEWalletMerchantAccount();

$TAGS_OBJ[] = array($i_Payment_Menu_Settings_PaymentItem, $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/",1);
$TAGS_OBJ[] = array($Lang['ePayment']['StudentPaymentItemSettings'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/student_payment_item.php",0);
if($sys_custom['ePayment']['PaymentItemChangeLog']){
	$TAGS_OBJ[] = array($Lang['ePayment']['PaymentItemChangeLog'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/change_log/",0);
}

$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_new);

if ($CatID == "")
{
	$cats = $lpayment->returnPaymentCats();
	$select_cat = getSelectByArray($cats,"name=\"CatID\" onChange=\"this.form.submit()\"",$CatID);

?>
<br />
<form name="form1" method="get">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
		<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
	</tr>
	<tr>
    	<td colspan="2">
    		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_Payment_Field_PaymentCategory?>
	    			</td>
	    			<td class="tabletext" colspan="2" width="70%"><?=$select_cat?></td>
    			</tr>
    		</table>
    	</td>
    </tr>
    <tr>
    	<td colspan="2">
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
    <tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
</form>
<br />
<?
print $linterface->FOCUS_ON_LOAD("form1.CatID");
}
else
{

$nextOrder = $lpayment->getPaymentItemNextDisplayOrder($CatID);

$lclass = new libclass();
$classes = $lclass->getClassList();

$currLvl = $classes[0][2];
$classList = "<input type=\"checkbox\" name=\"checkall\" onclick=\"checkUncheckAll(this)\"> $i_general_WholeSchool<br />";

$lvlArray = array();
for ($i=0; $i<sizeof($classes); $i++)
{
     list($cid, $cname, $clvl) = $classes[$i];
     $lvlArray[] = "YearClassID$i";
     $classList .= "<input type=\"checkbox\" name=\"YearClassID[]\" id=\"YearClassID$i\" value=\"".$cid."\"> ";
     $classList .= "<label for=\"YearClassID$i\">".$cname."</label> &nbsp;&nbsp;";
     //if ($currLvl != $clvl) {
     if ($clvl != $classes[$i+1][2]) {
     	$currLvl = $clvl;
	    if (sizeof($lvlArray) > 1) {
	     	$classList .= "<input type=\"checkbox\" name=\"checkall$i\" id=\"checkall$i\" onclick=\"checkUncheckPart('checkall$i','";
	     	$classList .= implode(",", $lvlArray);
	     	$classList .= "')\"> <label for=\"checkall$i\">$i_Payment_Item_Select_Class_Level</label>";
	     	$classList .= " ";
    	}
        $lvlArray = array();
        $classList .= "<br />\n";
     }
     
}

?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.css" type="text/css" />
<script>
function restrictNumber(obj)
{
	var val = $.trim(obj.value);
	var num = parseFloat(val);
	
	if(isNaN(num) || val == ''){
		num = 0;
	}
	obj.value = num;
}

function checkUncheckAll(theElement) {
	var theForm = theElement.form, z = 0;
	for(z=0; z<theForm.length;z++){
		if(theForm[z].type == 'checkbox' && theForm[z].name != 'checkall'){
			theForm[z].checked = theElement.checked;
		}
	}
}

function checkUncheckPart(controller,theElements) {
	var formElements = theElements.split(',');
	var theController = document.getElementById(controller);
	for(var z=0; z<formElements.length;z++){
		theItem = document.getElementById(formElements[z]);
		if(theItem){
			if(theItem.type){
				if(theItem.type == 'checkbox' && theItem.id != theController.id){
					theItem.checked = theController.checked;
				}
			} else {
			
				var nextArray = '';
				for(var x=0;x <theItem.childNodes.length;x++){
					if(theItem.childNodes[x]){
						if (theItem.childNodes[x].id){
							nextArray += theItem.childNodes[x].id+',';
						}
					}
				}
				checkUncheckSome(controller,nextArray);
			}
		}
	}
}


function checkform(obj){
	if(obj==null) return false;
	objName = obj.ItemName;
	objStartDate = obj.StartDate;
	objEndDate = obj.EndDate;
	objDispOrder = obj.DisplayOrder;
	objPriority = obj.PayPriority;
	objAmount = obj.Amount;
	restrictNumber(objDispOrder);
	restrictNumber(objPriority);
	restrictNumber(objAmount);
<?php if($sys_custom['ePayment_PaymentItem_ItemCode']) { ?>
    objCode = obj.ItemCode;
    if(!check_text(objCode,'<?=$i_Payment_Warning_Enter_PaymentItemCode?>')) return false;
    var item_code_check = js_Check_Unique('', 'ItemCode', 'ItemCodeWarningDiv', 'ItemCode', '<?=$i_Payment_Warning_Enter_PaymentItemCode?>','<?=$i_Payment_Warning_Duplicate_PaymentItemCode?>', false);
    if(item_code_check == false) {
        return false;
    }
<?php } ?>
	if(!check_text(objName,'<?=$i_Payment_Warning_Enter_PaymentItemName?>')) return false;
	if(!check_date(objStartDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) return false;
	if(!check_date(objEndDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) return false;
	if(!check_text(objDispOrder,'<?=$i_Payment_Warning_Enter_DisplayOrder?>')) return false;
	if(!check_text(objPriority,'<?=$i_Payment_Warning_Enter_PayPriority?>')) return false;
	if(!check_text(objAmount,'<?=$i_Payment_Warning_Enter_DefaultAmount?>')) return false;
<?php if(!$sys_custom['ePayment']['PaymentItemAllowNegativeAmount']){ ?>	
	if(!check_numeric(objAmount,'','<?=$i_Payment_Warning_InvalidAmount?>')) return false;
<?php } ?>

	<?php if($sys_custom['ePayment']['MultiPaymentGateway']) { ?>
    var have_merchantid = false;
    $("select[id^=MerchantAccountID_]").each(function(e) {
        if($(this).val() != '') {
            have_merchantid = true;
        }
    });
    if(have_merchantid == false) {
        if(confirm('<?=$Lang['ePayment']['SetupMerchant']?>') == false) {
            return false;
        }
    }
    <?php } ?>
	checkOptionAll(obj.elements["student[]"]);

	return true;
}

function onChooseFrom(obj)
{
	var id = obj.id;
	$('.ChooseFrom').hide();
	$('.'+id).show();
}

function Get_Student_Selection() 
{	
	newWindow('choose/index.php?fieldname=student[]',9);	
}

function removeStudent()
{
	checkOptionRemove(document.form1.elements["student[]"]);
}

$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
$(document).ready(function(){ 
	$('input#StartDate').datepick({
		dateFormat: 'yy-mm-dd',
		dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		changeFirstDay: false,
		firstDay: 0
		});
	$('input#EndDate').datepick({
		dateFormat: 'yy-mm-dd',
		dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		changeFirstDay: false,
		firstDay: 0
		});
	<?php if($sys_custom['ePayment_PaymentItem_ItemCode']) { ?>
    js_Add_KeyUp_Unique_Checking('', 'ItemCode', 'ItemCodeWarningDiv', 'ItemCode', '<?=$i_Payment_Warning_Enter_PaymentItemCode?>','<?=$i_Payment_Warning_Duplicate_PaymentItemCode?>');
    <?php } ?>
});
function js_Add_KeyUp_Unique_Checking(jsItemID, InputID, WarningDivID, DBFieldName, WarnBlankMsg, WarnUniqueMsg)
{
    jsItemID = jsItemID || '';

    $("input#"+InputID).keyup(function(){
        js_Check_Unique(jsItemID, InputID, WarningDivID, DBFieldName, WarnBlankMsg, WarnUniqueMsg, true);
    });
    $("input#"+InputID).change(function(){
        js_Check_Unique(jsItemID, InputID, WarningDivID, DBFieldName, WarnBlankMsg, WarnUniqueMsg, true);
    });
}

function js_Check_Unique(jsItemID, InputID, WarningDivID, DBFieldName, WarnBlankMsg, WarnUniqueMsg, async) {
    jsItemID = jsItemID || '';

    var InputValue = Trim($("input#" + InputID).val());
    var result = false;
    if (InputValue == '') {
        $('div#' + WarningDivID).html(WarnBlankMsg);
        $('div#' + WarningDivID).show();
    } else {
        $('div#' + WarningDivID).html("");
        $('div#' + WarningDivID).hide();
        $.ajax({
            url: "ajax_validate.php",
            data: {
                Action: DBFieldName,
                Value: InputValue,
                ItemID: jsItemID
            },
            async: async,
            type: 'POST',
            success: function (data) {
                if (data != 1) {
                    $('div#' + WarningDivID).html(WarnUniqueMsg).show();
                } else {
                    result = true;
                    $('div#' + WarningDivID).html("").hide();
                }
            }
        });
    }

    return result;
}

function onMerchantChange(obj, sp) {
    var id = $(obj).val();
    $(".quota_"+sp).hide();
    $(".quota_id_"+id).show();
}

</script>
<br />
<form name="form1" action="new_update.php" method="post" onSubmit="return checkform(this)">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
		<td align='right'><?=$lpayment->getResponseMsg($msg)?></td>
	</tr>
	<tr>
    	<td colspan="2">
    		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_Payment_Field_PaymentCategory?> <span class='tabletextrequire'>*</span>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<?=$lpayment->returnPaymentCatName($CatID)?>
	    			</td>
    			</tr>

                <?php if($sys_custom['ePayment_PaymentItem_ItemCode']) { ?>
                <tr>
                    <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?=$i_Payment_Field_PaymentItemCode?> <span class='tabletextrequire'>*</span>
                    </td>
                    <td class="tabletext" width="70%">
                        <input type="text" id="ItemCode" name="ItemCode" class="textboxtext" maxlength="255">
                        <div id="ItemCodeWarningDiv" style="color: #ff0000;"></div>
                    </td>
                </tr>
                <?php } ?>

    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_Payment_Field_PaymentItem?> <span class='tabletextrequire'>*</span>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<input type="text" name="ItemName" class="textboxtext" maxlength="255">
	    			</td>
    			</tr>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_general_startdate?>  <span class='tabletextrequire'>*</span>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<input type="text" name="StartDate" id="StartDate" class="textboxnum" maxlength="10" value="<?=date('Y-m-d')?>">
	    			</td>
    			</tr>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_general_enddate?> <span class='tabletextrequire'>*</span>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<input type="text" name="EndDate" id="EndDate" class="textboxnum" maxlength="10" value="<?=date('Y-m-d', time()+7*24*60*60)?>">
	    			</td>
    			</tr>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_Payment_Field_DisplayOrder?> <span class='tabletextrequire'>*</span>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<input type="text" name="DisplayOrder" class="textboxnum" maxlength="10" value="<?=$nextOrder?>" onchange="restrictNumber(this);">
	    			</td>
    			</tr>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_Payment_Field_PayPriority?> <span class='tabletextrequire'>*</span>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<input type="text" name="PayPriority" class="textboxnum" maxlength="10" value="<?=0?>" onchange="restrictNumber(this);"> 
	    				<span class="tabletextremark">(<?=$i_Payment_Note_Priority?>)</span>
	    			</td>
    			</tr>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_general_description?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<?=$linterface->GET_TEXTAREA("Description", "")?>
	    			</td>
    			</tr>

    			<?php if($use_merchant_account){ ?>
					<?php if($sys_custom['ePayment']['MultiPaymentGateway']) { ?>
                        <tr>
                            <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
								<?=$Lang['ePayment']['MerchantAccount']?>
                            </td>
                            <td class="tabletext" width="70%">
                                <table>
									<?php
									$x = '';
									$service_provider_list = $lpayment->getPaymentServiceProviderMapping(false,true);
									foreach($service_provider_list as $k=>$temp) {
										$merchants = $lpayment->getMerchantAccounts(array('ServiceProvider'=>$k,'RecordStatus'=>1,'order_by_accountname'=>true));
										$merchant_selection_data = array();
										for($i=0;$i<count($merchants);$i++){
											$merchant_selection_data[] = array($merchants[$i]['AccountID'],$merchants[$i]['AccountName']);
										}
										$x .= '<tr>';
										$x .= '<td>'.$temp.'</td>';
										$x .= '<td>';
										$x .= $linterface->GET_SELECTION_BOX($merchant_selection_data, ' id="MerchantAccountID_'.$k.'" name="MerchantAccountID_'.$k.'" onchange="onMerchantChange(this,\''.$k.'\')"', $Lang['General']['EmptySymbol'], '');
                                        foreach($merchant_selection_data as $merchant_data) {
											$year = date('Y');
											$month = date('m');
											$sql = "SELECT * FROM PAYMENT_MERCHANT_ACCOUNT_QUOTA WHERE Year='$year' AND Month='$month' AND AccountID='".$merchant_data[0]."'";
											$rs_quota = $lpayment->returnArray($sql);
											if(count($rs_quota)>0) {
											    $remain_quota = $rs_quota[0]['Quota'] - $rs_quota[0]['UsedQuota'];
											    if($remain_quota < 0) {
													$remain_quota = 0;
												}
											    $quota_str = str_replace('{COUNT}', $remain_quota, $Lang['ePayment']['CurrentRemainQuota']);
												$x .= ' <span class="quota_' . $k . ' quota_id_' . $merchant_data[0] . '" style="display: none;">(' . $quota_str . ')</span>';
											}
										}
										$x .= '</td>';
										$x .= '</tr>';
									}
									echo $x;
									?>
                                </table>
                            </td>
                        </tr>
					<?php } else { ?>
                    <tr>
                        <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
                            <?=$Lang['ePayment']['MerchantAccount']?>
                        </td>
                        <td class="tabletext" width="70%">
                        <?php
                        $merchants = $lpayment->getMerchantAccounts(array('RecordStatus'=>1,'order_by_accountname'=>true));
                        $merchant_selection_data = array();
                        for($i=0;$i<count($merchants);$i++){
                            $merchant_selection_data[] = array($merchants[$i]['AccountID'],$merchants[$i]['AccountName']);
                        }
                        echo $linterface->GET_SELECTION_BOX($merchant_selection_data, ' id="MerchantAccountID" name="MerchantAccountID" ', $Lang['General']['EmptySymbol'], '');
                        ?>
                        </td>
                    </tr>
    			    <?php } ?>
                <?php } ?>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_Payment_Field_PaymentDefaultAmount?> <span class='tabletextrequire'>*</span>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<input type="text" name="Amount" class="textboxnum" maxlength="10" onchange="restrictNumber(this);">
	    			</td>
    			</tr>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_general_SelectTarget?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<?=$linterface->Get_Radio_Button("ChooseFromClass", "ChooseFrom", "1", 1, "", $i_Payment_ClassInvolved, "onChooseFrom(this);")?>&nbsp;
	    				<?=$linterface->Get_Radio_Button("ChooseFromStudent", "ChooseFrom", "2", 0, "", $i_general_choose_student, "onChooseFrom(this);")?>
	    			</td>
    			</tr>
    			<tr class="ChooseFrom ChooseFromClass">
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_Payment_ClassInvolved?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<?=$classList?> <br />
	    				<span class="tabletextremark">(<?=$i_Payment_NoClass?>)</span>
	    			</td>
    			</tr>
    			<tr class="ChooseFrom ChooseFromStudent" style="display:none;">
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">&nbsp;</td>
	    			<td class="tabletext" width="70%">
	    				<table width="100%" border="0" cellspacing="0" cellpadding="0">
	    					<tr>
	    						<td valign="top" width="20%"><?=$linterface->GET_SELECTION_BOX(array(), ' name="student[]" id="student[]" multiple="multiple" size="12" style="width:200px;" ', '', "", false)?></td>
	    						<td valign="bottom" align="left" width="80%">
	    							<?= $linterface->GET_BTN($button_select, "button", "Get_Student_Selection();") ?><br />
	    							<?= $linterface->GET_BTN($button_remove, "button", "removeStudent();") ?>
	    						</td>
	    					</tr>
	    				</table>
	    			</td>
    			</tr>
                <?php if($plugin['eClassApp']) { ?>
                <tr>
                    <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
                    </td>
                    <td class="tabletext" width="70%">
						<?=$linterface->Get_Checkbox("SendReminderPushMessage", "SendReminderPushMessage", "1", 0, "", $Lang['ePayment']['PaymentSendReminderPushMessage'])?>&nbsp;
                    </td>
                </tr>
                <?php } ?>
    		</table>
    	</td>
    </tr>
    <tr>
    	<td colspan="2">
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
					<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
				</tr>
		    	<tr>
                	<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
    <tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
<input type=hidden name=CatID value="<?=$CatID?>">
</form>
<br />
<?
print $linterface->FOCUS_ON_LOAD("form1.ItemName");
}
$linterface->LAYOUT_STOP();
intranet_closedb();
?>