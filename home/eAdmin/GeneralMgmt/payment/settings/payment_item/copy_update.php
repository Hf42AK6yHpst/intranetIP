<?php
// Editing by 
/*
 * 2015-08-04 (Carlos): [ip2.5.6.9.1] Copy PAYMENT_PAYMENT_ITEMSTUDENT.Remark
 * 2015-03-31 (Carlos): [ip2.5.6.5.1] $sys_custom['ePayment']['PaymentMethod'] - Modified to also copy PaymentMethod for KIS. 
 * 2015-03-20 (Carlos): [ip2.5.6.5.1] Modified also copy item subsidy amounts. 
 * 						Also cater flag $sys_custom['ePayment']['SubsidySourceNoLimitChecking'] for KIS, if on, no checking on subsidy and can be negative.
 * 2015-03-05 (Carlos): [ip2.5.6.3.1] Created to copy payment items. Subsidy amount will not be copied as it may exceed the subsidy funding.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
if (!$lpayment->IS_ADMIN_USER() || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

if(!is_array($_POST['ItemID']) || count($_POST['ItemID']) == 0){
	$Msg = $Lang['ePayment']['CopyPaymentItemsUnsuccess'];
	header("Location: index.php?Msg=".urlencode($Msg));
	exit;
}

$use_magic_quotes = get_magic_quotes_gpc();

$subsidy_no_limit = $sys_custom['ePayment']['SubsidySourceNoLimitChecking'];

$result = array();
//$query = array();

$sql = "SELECT ItemID,CatID,Name,DisplayOrder,PayPriority,Description,StartDate,EndDate,DefaultAmount FROM PAYMENT_PAYMENT_ITEM WHERE ItemID IN ('".implode("','",$ItemID)."')";
$items = $lpayment->returnResultSet($sql);
$items_count = count($items);

if(!$subsidy_no_limit){
	$sql="SELECT 
			a.UnitID,
			a.UnitName,
			ROUND(IFNULL(SUM(b.SubsidyAmount),0),2) AS UsedAmount,
			IF(a.TotalAmount IS NULL,ROUND(0,2),ROUND(a.TotalAmount,2)) as TotalAmount 
		  FROM PAYMENT_SUBSIDY_UNIT AS a LEFT JOIN PAYMENT_PAYMENT_ITEM_SUBSIDY AS b ON (a.UnitID = b.SubsidyUnitID)
	      GROUP BY a.UnitID";
	$subsidyUnitAry = $lpayment->returnResultSet($sql);
	$subsidyUnitIdToAry = array();
	for($i=0;$i<count($subsidyUnitAry);$i++){
		$subsidyUnitIdToAry[$subsidyUnitAry[$i]['UnitID']] = $subsidyUnitAry[$i];
	}
	unset($subsidyUnitAry);
}

for($i=0;$i<$items_count;$i++)
{
	$item_id = $items[$i]['ItemID'];
	$cat_id  = $items[$i]['CatID'];
	//$item_name = $lpayment->Get_Safe_Sql_Query($items[$i]['Name']);
	$item_name = $use_magic_quotes? trim($_REQUEST['ItemName'.$item_id]) : addslashes(trim($_REQUEST['ItemName'.$item_id]));
	$display_order = $_REQUEST['DisplayOrder'.$item_id];
	$pay_priority = $_REQUEST['PayPriority'.$item_id];
	//$desc = $lpayment->Get_Safe_Sql_Query($items[$i]['Description']);
	$desc = $use_magic_quotes? trim($_REQUEST['Description'.$item_id]) : addslashes(trim($_REQUEST['Description'.$item_id]));
	$start_date = $_REQUEST['StartDate'.$item_id];
	$end_date = $_REQUEST['EndDate'.$item_id];
	$amount  = $_REQUEST['Amount'.$item_id];
	$sql_insert = "INSERT INTO PAYMENT_PAYMENT_ITEM (CatID,Name,DisplayOrder,PayPriority,Description,StartDate,EndDate,DefaultAmount, 
					RecordStatus,ProcessingTerminalUser,ProcessingTerminalIP,ProcessingAdminUser, DateInput, DateModified) VALUES 
					('$cat_id','$item_name','$display_order','$pay_priority','$desc','$start_date','$end_date','$amount',0, NULL,NULL,'".$_SESSION['UserID']."',now(),now())";
	$new_item_success = $lpayment->db_db_query($sql_insert);
	//$query[] = $sql_insert;
	if($new_item_success){
		
		$new_item_id = $lpayment->db_insert_id();	
		
		//if(abs($amount - $items[$i]['DefaultAmount']) > 0.01){
		//	$amount_field = "'$amount'";
		//}else{
		//	$amount_field = "Amount";
		//}
		
		$sql_get_itemstudents = "SELECT PaymentID,ItemID,StudentID,Amount,SubsidyAmount,Remark";
		if($sys_custom['ePayment']['PaymentMethod']){
			$sql_get_itemstudents.=",PaymentMethod ";
		}
		$sql_get_itemstudents.= " FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID='$item_id'";
		$old_itemstudents = $lpayment->returnResultSet($sql_get_itemstudents);
		$old_itemstudents_count = count($old_itemstudents);
		for($j=0;$j<$old_itemstudents_count;$j++){
			
			$o_item_id = $old_itemstudents[$j]['ItemID'];
			$o_payment_id = $old_itemstudents[$j]['PaymentID'];
			$o_student_id = $old_itemstudents[$j]['StudentID'];
			$o_amount = $old_itemstudents[$j]['Amount'];
			$o_subsidy_amount = $old_itemstudents[$j]['SubsidyAmount'];
			$o_remark = $old_itemstudents[$j]['Remark'];
			if($sys_custom['ePayment']['PaymentMethod']){
				$o_payment_method = trim($old_itemstudents[$j]['PaymentMethod']);
				if($o_payment_method == ''){
					$o_payment_method = "NULL";
				}
			}
			
			$sql = "INSERT INTO PAYMENT_PAYMENT_ITEMSTUDENT (ItemID,StudentID,Amount,Remark";
			if($sys_custom['ePayment']['PaymentMethod']){
				$sql.=",PaymentMethod";	
			}
			$sql.= ",RecordType,RecordStatus,DateInput,DateModified) ";
			$sql.= "VALUES ('$new_item_id','$o_student_id','$o_amount','".$lpayment->Get_Safe_Sql_Query($o_remark)."'";
			if($sys_custom['ePayment']['PaymentMethod']){
				$sql .= ",".$o_payment_method;	
			}
			$sql.= ",0,0,NOW(),NOW())";
			$add_itemstudent_success = $lpayment->db_db_query($sql);
			$result['NewItem_'.$item_id.'_'.$o_student_id] = $add_itemstudent_success;
			if($add_itemstudent_success)
			{
				$new_itemstudent_id = $lpayment->db_insert_id();
				if($o_subsidy_amount!='' && $o_subsidy_amount > 0)
				{
					// if have subsidy, copy it
					$sql_get_subsidy = "SELECT SubsidyUnitID,SubsidyAmount FROM PAYMENT_PAYMENT_ITEM_SUBSIDY WHERE PaymentID='$o_payment_id' AND UserID='$o_student_id'";
					$old_subsidy_items = $lpayment->returnResultSet($sql_get_subsidy);
					$old_subsidy_item_count = count($old_subsidy_items);
					$new_subsidy_amount = 0;
					
					for($k=0;$k<$old_subsidy_item_count;$k++)
					{
						$o_subsidy_unit_id = $old_subsidy_items[$k]['SubsidyUnitID'];
						$o_subsidy_unit_amount = $old_subsidy_items[$k]['SubsidyAmount'];
						if($subsidy_no_limit) // if subsidy no limit, direct copy the student subsidy
						{
							$sql = "INSERT INTO PAYMENT_PAYMENT_ITEM_SUBSIDY (PaymentID,UserID,SubsidyUnitID,SubsidyAmount,SubsidyPICAdmin,SubsidyPICUserID,DateInput,DateModified,InputBy,ModifyBy) 
									VALUES ('$new_itemstudent_id','$o_student_id','$o_subsidy_unit_id','$o_subsidy_unit_amount','','".$_SESSION['UserID']."',NOW(),NOW(),'".$_SESSION['UserID']."','".$_SESSION['UserID']."')";
							$result['NewSubsidy_'.$o_payment_id.'_'.$o_student_id] = $lpayment->db_db_query($sql);
							$new_subsidy_amount += $o_subsidy_unit_amount;
						}else{ // check subsidy amount enough to be copied
							if(isset($subsidyUnitIdToAry[$o_subsidy_unit_id])){
								$unit_used = $subsidyUnitIdToAry[$o_subsidy_unit_id]['UsedAmount'];
								$unit_total = $subsidyUnitIdToAry[$o_subsidy_unit_id]['TotalAmount'];
								$unit_remain = $unit_total - $unit_used;
								if($unit_remain >= $o_subsidy_unit_amount){
									$sql = "INSERT INTO PAYMENT_PAYMENT_ITEM_SUBSIDY (PaymentID,UserID,SubsidyUnitID,SubsidyAmount,SubsidyPICAdmin,SubsidyPICUserID,DateInput,DateModified,InputBy,ModifyBy) 
											VALUES ('$new_itemstudent_id','$o_student_id','$o_subsidy_unit_id','$o_subsidy_unit_amount','','".$_SESSION['UserID']."',NOW(),NOW(),'".$_SESSION['UserID']."','".$_SESSION['UserID']."')";
									$result['NewSubsidy_'.$o_payment_id.'_'.$o_student_id] = $lpayment->db_db_query($sql);
									$new_subsidy_amount += $o_subsidy_unit_amount;
									$subsidyUnitIdToAry[$o_subsidy_unit_id]['UsedAmount'] += $o_subsidy_unit_amount;
								}
							}	
						}
					}
					$sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET SubsidyAmount='$new_subsidy_amount' WHERE PaymentID='$new_itemstudent_id'";
					$result['UpdateStudentSubsidy_'.$o_payment_id.'_'.$o_student_id] = $lpayment->db_db_query($sql);
				}
			}
		}
		
		/*
		$sql_add = "INSERT INTO PAYMENT_PAYMENT_ITEMSTUDENT (ItemID,StudentID,Amount,RecordType, RecordStatus, DateInput, DateModified) 
					SELECT '$new_item_id',StudentID,$amount_field,0,0,now(),now() FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID='$item_id'";
		$add_success = $lpayment->db_db_query($sql_add);
		
		$result['NewItem'.$item_id] = $add_success;
		*/
	}else{
		$result['NewItem'.$item_id] = false;
	}
	
}

intranet_closedb();
if(in_array(false,$result)){
	$Msg = $Lang['ePayment']['CopyPaymentItemsUnsuccess'];
}else{
	$Msg = $Lang['ePayment']['CopyPaymentItemsSuccess'];
}
//header ("Location: index.php?Msg=".urlencode($Msg));

$x = '<html>';
$x.= '<head>';
$x .= '<meta http-equiv="Content-Type" content="text/html" charset="'.returnCharset().'">';
$x.= '</head>';
$x.= '<body>';
$x.= '<form id="form1" name="form1" action="index.php" method="post">';
$x .= '<input type="hidden" id="FromDate" name="FromDate" value="'.$FromDate.'" />';
$x .= '<input type="hidden" id="ToDate" name="ToDate" value="'.$ToDate.'" />';
$x .= '<input type="hidden" id="itemStatus" name="itemStatus" value="'.$itemStatus.'" />';
$x .= '<input type="hidden" id="itemPaidStatus" name="itemPaidStatus" value="'.$itemPaidStatus.'" />';
$x .= '<input type="hidden" id="CatID" name="CatID" value="'.$CatID.'" />';
$x .= '<input type="hidden" id="keyword" name="keyword" value="'.str_replace(array('"'),array('\"'),$use_magic_quotes?stripslashes($keyword):$keyword).'" />';
$x .= '<input type="hidden" id="field" name="field" value="'.$field.'" />';
$x .= '<input type="hidden" id="order" name="order" value="'.$order.'" />';
$x .= '<input type="hidden" id="numPerPage" name="numPerPage" value="'.$numPerPage.'" />';
$x .= '<input type="hidden" id="pageNo" name="pageNo" value="'.$pageNo.'" />';
$x .= '<input type="hidden" id="Msg" name="Msg" value="'.urlencode($Msg).'" />';
$x.= '</form>';
$x .= '<script type="text/javascript" language="JavaScript">';
//$x .= 'function postback(){document.getElementById("form1").submit();}';
$x .= 'document.getElementById("form1").submit();';
$x .= '</script>';
$x.= '</body>';
$x.= '</html>';

echo $x;
?>