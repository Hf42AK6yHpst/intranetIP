<?php
// Editing by 

/*
 * 2018-02-21 Carlos: Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_template.php");


intranet_auth();
intranet_opendb();
$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$plugin['eClassApp']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}


$lpayment = new libpayment();
if($SendReminderPushMessage == 1) {
	$ItemID = IntegerSafe($_GET['ItemID']);
	$PushMessageType = 2;
	$sql = "SELECT PaymentID FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID='$ItemID'";
	$PaymentID = $lpayment->returnVector($sql);


	$sql = "SELECT TemplateID,Title,Content FROM INTRANET_APP_PUSH_MESSAGE_TEMPLATE WHERE FromModule='ePayment' AND RecordStatus=1 AND CanDelete=0";
	$rs = $lpayment->returnArray($sql);
	if(!empty($rs)) {
		$messageTitle = $rs[0]['Title'];
		$messageContent = $rs[0]['Content'];
	}
} else {
	$PushMessageType = $_POST['PushMessageType'];
	$ItemID = IntegerSafe($_POST['ItemID']);
	$PaymentID = IntegerSafe($_POST['PaymentID']);
}


$libAppTemplate = new libeClassApp_template();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PaymentItemSettings";

$linterface = new interface_html();

$itemName = $lpayment->returnPaymentItemName($ItemID);

$TAGS_OBJ[] = array($i_Payment_Menu_Settings_PaymentItem, $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/", 1);
$TAGS_OBJ[] = array($Lang['ePayment']['StudentPaymentItemSettings'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/student_payment_item.php",0);
if($sys_custom['ePayment']['PaymentItemChangeLog']){
	$TAGS_OBJ[] = array($Lang['ePayment']['PaymentItemChangeLog'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/change_log/",0);
}
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();

$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($itemName,'list.php?ItemID='.$ItemID);
$PAGE_NAVIGATION[] = array($PushMessageType == '1'? $Lang['ePayment']['SendPaymentNotification'] : $Lang['ePayment']['SendReminder'],'');

if($plugin['eClassApp'] == true){
	$Module = "ePayment";
	# check Access Right
	$libAppTemplate->canAccess($Module,$Section);
	
	# Init libeClassApp_template
	$libAppTemplate->setModule($Module);
	$libAppTemplate->setSection($Section);
	
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	$luser = new libuser();
	$studentWithParentUsingAppAry = $luser->getStudentWithParentUsingParentApp();
	
}

# Get List
$list = implode(",",$PaymentID);
$namefield = getNameFieldByLang("b.");
$archive_namefield = Get_Lang_Selection("d.ChineseName","d.EnglishName");
$order_field = Get_Lang_Selection("b.ChineseName","b.EnglishName");
if($PushMessageType == 2) // not pay yet
{
	$sql = "SELECT 
				a.PaymentID,
				IF(b.UserID IS NOT NULL, $namefield, $archive_namefield) as StudentName,
				IF(b.UserID IS NOT NULL, b.ClassName, d.ClassName) as ClassName,
				IF(b.UserID IS NOT NULL, b.ClassNumber, d.ClassNumber) as ClassNumber,
				a.Amount,
				c.Balance,
				a.StudentID,
				a.RecordStatus    
	       FROM PAYMENT_PAYMENT_ITEMSTUDENT as a 
				LEFT JOIN INTRANET_USER as b ON a.StudentID = b.UserID AND b.RecordType = 2 AND b.RecordStatus in (0,1,2,3) 
				LEFT JOIN INTRANET_ARCHIVE_USER AS d ON a.StudentID = d.UserID 
	            LEFT JOIN PAYMENT_ACCOUNT as c ON a.StudentID = c.StudentID
	       WHERE a.PaymentID IN ($list) AND a.RecordStatus='0' 
	       ORDER BY b.ClassName, b.ClassNumber+0, $order_field";
}else if($PushMessageType == 1){ // paid
	$sql = "SELECT 
				a.PaymentID,
				IF(b.UserID IS NOT NULL, $namefield, $archive_namefield) as StudentName,
				IF(b.UserID IS NOT NULL, b.ClassName, d.ClassName) as ClassName,
				IF(b.UserID IS NOT NULL, b.ClassNumber, d.ClassNumber) as ClassNumber,
				t.Amount,
				c.Balance,
				a.StudentID,
				a.RecordStatus,
				t.BalanceAfter     
	       FROM PAYMENT_PAYMENT_ITEMSTUDENT as a 
				INNER JOIN PAYMENT_OVERALL_TRANSACTION_LOG as t ON
				t.LogID = (SELECT LogID FROM PAYMENT_OVERALL_TRANSACTION_LOG WHERE TransactionType='2' AND RelatedTransactionID = a.PaymentID AND TIME_TO_SEC(TIMEDIFF(TransactionTime,a.PaidTime))<10 ORDER BY LogID DESC LIMIT 1)
				LEFT JOIN INTRANET_USER as b ON a.StudentID = b.UserID AND b.RecordType = 2 AND b.RecordStatus in (0,1,2,3) 
				LEFT JOIN INTRANET_ARCHIVE_USER AS d ON a.StudentID = d.UserID 
	            LEFT JOIN PAYMENT_ACCOUNT as c ON a.StudentID = c.StudentID
	       WHERE a.PaymentID IN ($list) AND a.RecordStatus='1' 
	       ORDER BY b.ClassName, b.ClassNumber+0, $order_field";
}
$result = $lpayment->returnResultSet($sql);

$allEnough = true;
$x = "";
for ($i=0; $i<sizeof($result); $i++)
{
     //list ($PaymentID, $name, $class, $classnum, $amount, $balance) = $result[$i];
     $PaymentID = $result[$i]['PaymentID'];
     $name = $result[$i]['StudentName'];
     $class = $result[$i]['ClassName'];
     $classnum = $result[$i]['ClassNumber'];
     $amount = $result[$i]['Amount'];
     $balance = $result[$i]['Balance'];
     if($PushMessageType == 2){
     	$balanceAfter = $balance - $amount;
     }else{
     	$balanceAfter = $result[$i]['BalanceAfter'];
     }
	 
     //$hidden = '';
     if ($balanceAfter < 0)
     {
     	 $allEnough = false;
         $balanceAfter = $lpayment->getWebDisplayAmountFormat($balanceAfter);
         $balanceAfter = "<font color=red>$balanceAfter</font>";
     }
     else
     {
     	$balanceAfter = $lpayment->getWebDisplayAmountFormat($balanceAfter);
     }
	 //$hidden = "<input type=hidden name=PaymentID[] value='$PaymentID'>\n";
	
     $x .= "<tr>\n";
     $x .= "<td>".($i+1)."</td>\n";
     $x .= "<td>$name</td>\n";
     $x .= "<td>$class</td>\n";
     $x .= "<td>$classnum</td>\n";
	 if($lpayment->isEWalletDirectPayEnabled() == false) {
		 $x .= "<td>" . $lpayment->getWebDisplayAmountFormat($balance) . "</td>\n";
	 }
     $x .= "<td>".$lpayment->getWebDisplayAmountFormat($amount)."</td>\n";
	 if($lpayment->isEWalletDirectPayEnabled() == false) {
		$x .= "<td>$balanceAfter</td>\n";
	 }
     //$x .= $hidden."\n";
     //$x .= "</td>\n";
 	 if(in_array($result[$i]['StudentID'], $studentWithParentUsingAppAry)){
 		$x .= '<td class="num_check push_message" align="right" valign="top"><input type="checkbox" name="StudentID[]" value="'.$result[$i]['StudentID'].'" checked="checked"></td>';
 	 }else{
 		$x .= '<td class="num_check push_message" align="right" valign="top"></td>';
 	 }
     
     $x .= "</tr>\n";
}


$linterface->LAYOUT_START();
?>
<script language="JavaScript" type="text/javascript">
$(document).ready(function(){
	AppMessageReminder.initInsertAtTextarea();
	AppMessageReminder.TargetAjaxScript = 'ajax_send_reminder_update.php';
});

function goBack()
{
	window.location.href = 'list.php?ItemID=<?php echo $ItemID;?>';
}

function sendPushMessage()
{
	$('#sendBtn').attr('disabled',true);
	Block_Element('form1','<?php echo $Lang['General']['Sending...']?>');
	$.post(
		'send_push_message.php',
		$('#form1').serialize(),
		function(returnMsg){
			UnBlock_Element('form1');
			Get_Return_Message(returnMsg);
			setTimeout(function(){goBack();},3000);
		}
	);
}

function checkForm()
{
	var title = $.trim($('#PushMessageTitle').val());
	var content = $.trim($('#MessageContent').val());
	if($('input[name="StudentID[]"]:checked').length == 0){
		alert('<?php echo $i_SMS_no_student_select?>');
		return false;
	}
	if (title == "") {
		alert("<?php echo $Lang['AppNotifyMessage']['WarningMsg']['RequestInputTitle']?>");
		return false;
	}
	if (content == "") {
		alert("<?php echo $Lang['AppNotifyMessage']['WarningMsg']['RequestInputDescription']?>");
		return false;
	}
	
	sendPushMessage();
}
</script>
<? include_once($PATH_WRT_ROOT."home/eClassApp/common/pushMessageTemplate/js_AppMessageReminder.php") ?>
<br />
<form id="form1" name="form1" action="" method="post" onsubmit="return false;">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td><?php echo $linterface->GET_NAVIGATION($PAGE_NAVIGATION);?></td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
					<tr>
						<td>
							<div id="send_push_message">
								<table border="0" cellpadding="5" cellspacing="0" align="center" width="100%">
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
											<?php echo $Lang['Identity']['Student']?> <span class="tabletextrequire">*</span>
										</td>
										<td class="tabletext" width="70%">
											<table class="common_table_list_v30" width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
											    <tr class="tabletop">
											    	<th class="num_check">#</th>
													<th><?php echo $i_UserStudentName ?></th>
													<th><?php echo $i_UserClassName ?></th>
													<th><?php echo $i_UserClassNumber ?></th>
                                                    <?php if($lpayment->isEWalletDirectPayEnabled() == false) { ?>
													<th><?php echo $i_Payment_Field_CurrentBalance ?></th>
													<?php } ?>
													<th><?php echo $i_Payment_Field_Amount ?></th>
													<?php if($lpayment->isEWalletDirectPayEnabled() == false) { ?>
													<th><?php echo $i_Payment_Field_BalanceAfterPay ?></th>
													<?php } ?>
													<th class="push_message" width="1" align="right"><input type="checkbox" onclick="(this.checked)?setChecked(1,this.form,'StudentID[]'):setChecked(0,this.form,'StudentID[]')"></input></th>
												</tr>
												<?=$x?>
											</table>
										</td>
									</tr>
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
											<?php echo $Lang['SMS']['MessageTemplates']?>
										</td>
										<td class="tabletext" width="70%">
											<?php echo $libAppTemplate->getMessageTemplateSelectionBox(true);?>
											<span id="AjaxStatus"></span>
										</td>
									</tr>
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
											<?php echo $Lang['AppNotifyMessage']['Title']?> <span class="tabletextrequire">*</span>
										</td>
										<td class="tabletext" width="70%">
											<?php echo $linterface->GET_TEXTAREA("PushMessageTitle", $messageTitle, $taCols=70, $taRows=2)?>
										</td>
									</tr>
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
											<?php echo $Lang['AppNotifyMessage']['Description']?> <span class="tabletextrequire">*</span>
										</td>
										<td class="tabletext" width="70%">
											<?php echo $linterface->GET_TEXTAREA("MessageContent", $messageContent, $taCols=70, $taRows=10)?>
										</td>
									</tr>
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
											<?php echo $Lang['eClassApp']['AutoFillItem']?>
										</td>
										<td class="tabletext" width="70%">
											<?php echo $libAppTemplate->getMessageTagSelectionBox();?>
										</td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
    		<td>
		    	<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    		<tr>
                		<td colspan="3" class="dotline"><img src="<?php echo "{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
            	    </tr>
			    </table>
		    </td>
		</tr>
		<tr>
		    <td align="center" colspan="2">
				<?php echo $linterface->GET_ACTION_BTN($Lang['Btn']['Send'], "button", "checkForm();","sendBtn") ?>
				<?php echo $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "goBack();","cancelBtn") ?>
			</td>
		</tr>
	</table>
	<input type=hidden name=ItemID value="<?php echo $ItemID?>">
	<input type="hidden" name="Module" id="Module" value="<?php echo $Module?>">
	<input type="hidden" name="Module" id="Section" value="">
	<input type="hidden" name="PushMessageType" id="PushMessageType" value="<?php echo $PushMessageType?>" />
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>