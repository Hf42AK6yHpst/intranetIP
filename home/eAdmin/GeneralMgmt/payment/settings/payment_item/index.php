<?php
// using by 
/*
 * 2019-07-25 (Ray)   : Add payment notice filter
 * 2017-11-16 (Bill)  : [ip2.5.9.1.1] fix: Use Parent Notice page to preview / edit Payment Notice > Question syntax broken after update	[2017-1019-1100-13207]
 * 2017-05-18 (Carlos): [ip2.5.8.7.1] Search keyword applies to payment notice title.
 * 2016-11-21 (Carlos): [ip2.5.8.1.1] Added pay for selected items function. 
 * 2016-11-09 (Carlos): [ip2.5.8.1.1] Changed to use real TEMPORARY TABLE to avoid different users to manipulate the same temp table at the same time. 
 * 									  Grouping and counting records approach has performance problem when payment student records size is huge.
 * 2016-10-11 (Carlos): [ip2.5.7.10.1] Modified to count paid/total info by grouping records. 
 * 						Do not use temp table which would cause duplication issue when more than one user load the page at the same time. 
 * 2015-03-05 (Carlos): [ip2.5.6.3.1] Added copy payment items function.
 * 2015-02-09 (Carlos): $sys_custom['ePayment']['PaymentItemDateRange'] - date range only apply to EndDate
 * 2014-08-25 (Carlos): Work around default date range to today if no current academic year info
 * 2014-08-20 (Carlos): $sys_custom['ePayment']['PaymentItemChangeLog'] - added navigation tag [Payment Item Change Log]
 * 2014-07-16 (Carlos): added table field [Amount] which is the default amount of an payment item, not the true amount students would pay
 * 2012-10-04 (Carlos): added [Pay All] table button to pay all unpaid items together within the date range
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
# preserve table view
if ($ck_payment_item_setting_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_payment_item_setting_page_number", $pageNo, 0, "", "", 0);
	$ck_payment_item_setting_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_payment_item_setting_page_number!="")
{
	$pageNo = $ck_payment_item_setting_page_number;
}

if ($ck_payment_item_setting_page_order!=$order && $order!="")
{
	setcookie("ck_payment_item_setting_page_order", $order, 0, "", "", 0);
	$ck_payment_item_setting_page_order = $order;
} else if (!isset($order) && $ck_payment_item_setting_page_order!="")
{
	$order = $ck_payment_item_setting_page_order;
}

if ($ck_payment_item_setting_page_field!=$field && $field!="")
{
	setcookie("ck_payment_item_setting_page_field", $field, 0, "", "", 0);
	$ck_payment_item_setting_page_field = $field;
} else if (!isset($field) && $ck_payment_item_setting_page_field!="")
{
	$field = $ck_payment_item_setting_page_field;
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PaymentItemSettings";

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$linterface = new interface_html();

if ($CatID != "") {
    $conds = " AND a.CatID = '$CatID'";
}

if ($itemPaidStatus <> ""){
	if ($itemPaidStatus == 0){
		$conds .= " AND (c.countPaid <> c.TotalCount OR c.TotalCount = 0) ";
		//$having_conds = " HAVING (COUNT(DISTINCT c2.PaymentID)=0 OR COUNT(DISTINCT c1.PaymentID)<COUNT(DISTINCT c2.PaymentID)) ";
	}else if ($itemPaidStatus == 1){
		$conds .= " AND c.countPaid = c.TotalCount AND c.TotalCount <> 0 ";
		//$having_conds = " HAVING (COUNT(DISTINCT c1.PaymentID)=COUNT(DISTINCT c2.PaymentID) AND COUNT(DISTINCT c2.PaymentID)<>0) ";
	}else if($itemPaidStatus == 2 ){ # Not Yet Started
		$today = date('Y-m-d');
		$conds .=" AND DATE_FORMAT(a.StartDate,'%Y-%m-%d') > '$today' ";
	}
}


$itemStatus=$itemStatus==""?0:$itemStatus;

	if($itemStatus==1){
		$conds2 =" AND a.RecordStatus=1 ";
	}else{
		$conds2 =" AND a.RecordStatus=0 ";
	}

$cond3 = "";
if($PaymentNoticeID != "") {
    $cond3 = " AND a.NoticeID='".$PaymentNoticeID."' ";
}

// admin in charge name field
$Inchargenamefield = getNameFieldByLang("d.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$Inchargearchive_namefield = " IF(e.ChineseName IS NULL,e.EnglishName,e.ChineseName)";
	//$archive_namefield="c.ChineseName";
}else {
	//$archive_namefield ="c.EnglishName";
	$Inchargearchive_namefield = "IF(e.EnglishName IS NULL,e.ChineseName,e.EnglishName)";
}

$currentAcademicYearId = Get_Current_Academic_Year_ID();

# date range
if($FromDate==""){
	$FromDate = empty($currentAcademicYearId)? date('Y-m-d') : date('Y-m-d',getStartOfAcademicYear($today_ts));
}
if($ToDate==""){
	$ToDate = empty($currentAcademicYearId)? date('Y-m-d') : date('Y-m-d',getEndOfAcademicYear($today_ts));
}

if($sys_custom['ePayment']['PaymentItemDateRange']){
	$date_conds = " AND (a.EndDate between '$FromDate' AND '$ToDate') ";
}else{
	$date_conds =" AND (
									(a.StartDate between '$FromDate' AND '$ToDate' 
									 or 
									 a.EndDate between '$FromDate' AND '$ToDate' )) ";
}

$ldb = new libdb();

//$temp_table_sql = "DROP TABLE TEMP_PAYMENT_ITEM_SUMMARY";
//$ldb->db_db_query($temp_table_sql);

$temp_table_sql = "CREATE TEMPORARY TABLE TEMP_PAYMENT_ITEM_SUMMARY (
					pid int,
					sumUnpaid varchar(255),
					sumPaid varchar(255),
					TotalPayment varchar(255),
					countUnpaid int,
					countPaid int,
					TotalCount int
				)";
$ldb->db_db_query($temp_table_sql);

$pValue="";

$all_pid_sql  = "SELECT a.ItemID 
				 FROM
					 PAYMENT_PAYMENT_ITEM as a 
					 LEFT JOIN PAYMENT_PAYMENT_CATEGORY as b ON a.CatID = b.CatID
					 LEFT JOIN INTRANET_NOTICE as f on f.NoticeID=a.NoticeID 
				 WHERE
					  (a.Name LIKE '%$keyword%' OR
					   a.Description LIKE '%$keyword%' OR 
						f.Title LIKE '%$keyword%' 
					  )
                ";

$allPaymentID = $ldb->returnVector($all_pid_sql);

// - get the summay of payment :
// - Sum of unpaid amount, Sum of paid amount, Count of unpaid student, Count of paid student
if (sizeOf($allPaymentID)>0) {
	for ($i=0; $i<sizeOf($allPaymentID); $i++){
		$allPaymentIDSummary[$i] = $lpayment->returnPaymentPaidSummary($allPaymentID[$i]);

		if ($i>0) $pValue .= ","; 
		$pValue .=	"(".$allPaymentID[$i].",'"
					.number_format($allPaymentIDSummary[$i][0],1,".",",")."','"
					.number_format($allPaymentIDSummary[$i][1],1,".",",")."','"
					.number_format(($allPaymentIDSummary[$i][0]+$allPaymentIDSummary[$i][1]),1,".",",")."',"
					.$allPaymentIDSummary[$i][2].",".$allPaymentIDSummary[$i][3]."," 
					.($allPaymentIDSummary[$i][2]+$allPaymentIDSummary[$i][3]).")";
	}
}

$insert_temp_sql = "INSERT INTO TEMP_PAYMENT_ITEM_SUMMARY ( pid, sumUnpaid, sumPaid, TotalPayment, countUnpaid, countPaid,TotalCount ) VALUES $pValue";
$ldb->db_db_query($insert_temp_sql);
//+++ end of getting payment

/*
	$last_updated_field="IF(a.ProcessingAdminUser IS NOT NULL AND a.ProcessingAdminUser!='',a.ProcessingAdminUser,IF(a.ProcessingTerminalUser IS NOT NULL AND a.ProcessingTerminalUser!='',a.ProcessingTerminalUser,''))";
	IF(a.ProcessingAdminUser IS NOT NULL AND a.ProcessingAdminUser!='',a.ProcessingAdminUser,
	IF(a.ProcessingTerminalUser IS NOT NULL AND a.ProcessingTerminalUser!='',a.ProcessingTerminalUser,'&nbsp;')),
*/

$sql  = "SELECT
         	CONCAT('<a class=tablelink href=list.php?ItemID=',a.ItemID,'>', if(a.NoticeID is null, a.Name, concat(f.Title,' - ', a.Name)) ,'</a>'),
         	CASE 
         		WHEN a.NoticeID IS NOT NULL OR a.NoticeID <> '' then CONCAT('<a class=\"notice-link\" title=\"\" href=\"#\" onclick=\"viewNotice(',a.NoticeID,'); return false;\"><img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" border=0></a>') 
         		ELSE '&nbsp;'
         	END as NoticeIcon,
         	b.Name, 
			IF(a.DefaultAmount IS NULL,' - ',".$lpayment->getWebDisplayAmountFormatDB("a.DefaultAmount").") as Amount,
         	a.DisplayOrder, 
         	a.PayPriority, 
			   	CONCAT(c.countPaid,' (',c.TotalCount,')') as countInfo,
			   	IF(a.Description IS NULL OR a.Description='','&nbsp;',a.Description),
			   	IF((d.UserID IS NULL AND e.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$Inchargearchive_namefield,'</i>'), IF(d.UserID IS NULL AND e.UserID IS NULL,IF(a.ProcessingAdminUser IS NULL OR a.ProcessingAdminUser = '','&nbsp;',a.ProcessingAdminUser),$Inchargenamefield)) as DisplayAdminInCharge, 
          DATE_FORMAT(a.StartDate,'%Y-%m-%d'),
          DATE_FORMAT(a.EndDate,'%Y-%m-%d'),
          CONCAT('<input type=checkbox name=ItemID[] value=', a.ItemID ,'>')
         FROM
             PAYMENT_PAYMENT_ITEM as a 
             LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY as b ON (a.CatID = b.CatID) 
             LEFT OUTER JOIN TEMP_PAYMENT_ITEM_SUMMARY as c ON (c.pid = a.ItemID) 
             LEFT JOIN INTRANET_USER as d on a.ProcessingAdminUser = d.UserID 
             LEFT JOIN INTRANET_ARCHIVE_USER as e on a.ProcessingAdminUser = e.UserID 
             left join INTRANET_NOTICE as f on f.NoticeID=a.NoticeID
         WHERE
              (a.Name LIKE '%$keyword%' OR
               a.Description LIKE '%$keyword%' OR 
				f.Title LIKE '%$keyword%' 
              )
              $conds $date_conds $conds2 $cond3
       ";
/*
$sql  = "SELECT
         	CONCAT('<a class=tablelink href=list.php?ItemID=',a.ItemID,'>', if(a.NoticeID is null, a.Name, concat(f.Title,' - ', a.Name)) ,'</a>'),
         	CASE 
         		WHEN a.NoticeID IS NOT NULL OR a.NoticeID <> '' then CONCAT('<a title=\"".$Lang['ePayment']['ViewNotice']."\" href=\"#\" onclick=\"viewNotice(',a.NoticeID,'); return false;\"><img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" border=0></a>') 
         		ELSE '&nbsp;'
         	END as NoticeIcon,
         	b.Name, 
			IF(a.DefaultAmount IS NULL,' - ',".$lpayment->getWebDisplayAmountFormatDB("a.DefaultAmount").") as Amount,
         	a.DisplayOrder, 
         	a.PayPriority, 
			   	CONCAT(COUNT(DISTINCT c1.PaymentID),' (',COUNT(DISTINCT c2.PaymentID),')') as countInfo,
			   	IF(a.Description IS NULL OR a.Description='','&nbsp;',a.Description),
			   	IF((d.UserID IS NULL AND e.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$Inchargearchive_namefield,'</i>'), IF(d.UserID IS NULL AND e.UserID IS NULL,IF(a.ProcessingAdminUser IS NULL OR a.ProcessingAdminUser = '','&nbsp;',a.ProcessingAdminUser),$Inchargenamefield)) as DisplayAdminInCharge, 
          DATE_FORMAT(a.StartDate,'%Y-%m-%d'),
          DATE_FORMAT(a.EndDate,'%Y-%m-%d'),
          CONCAT('<input type=checkbox name=ItemID[] value=', a.ItemID ,'>')
         FROM
             PAYMENT_PAYMENT_ITEM as a 
             LEFT JOIN PAYMENT_PAYMENT_CATEGORY as b ON (a.CatID = b.CatID) 
             LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT as c1 ON (c1.ItemID = a.ItemID) AND c1.RecordStatus=1 
			 LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT as c2 ON (c2.ItemID = a.ItemID) 
             LEFT JOIN INTRANET_USER as d on a.ProcessingAdminUser = d.UserID 
             LEFT JOIN INTRANET_ARCHIVE_USER as e on a.ProcessingAdminUser = e.UserID 
             left join INTRANET_NOTICE as f on f.NoticeID=a.NoticeID
         WHERE 
			1 $date_conds $conds $conds2 ";
if(trim($keyword)!=''){
  $sql.= " AND (a.Name LIKE '%$keyword%' OR 
               a.Description LIKE '%$keyword%') ";
}
  $sql.= " GROUP BY a.ItemID $having_conds ";
*/
# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("a.Name",
						 "b.Name",
						 "a.DefaultAmount",
						 "a.DisplayOrder",
						 "a.PayPriority",
						 "countInfo",
						 "a.Description",
						 "DisplayAdminInCharge",
						 "a.StartDate",
						 "a.EndDate");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+3;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0,0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0,0,0,0,0,0,0);
$li->IsColOff = 2;

if ($field=="" && $order=="") {
	$li->field = 3; // default sort by PAYMENT_PAYMENT_ITEM.DisplayOrder
	$li->order = 1;
}

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='20%'>".$li->column($pos++, $i_Payment_Field_PaymentItem)."</td>\n";
$li->column_list .= "<td width='1' class='tabletoplink'>&nbsp;</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_Payment_Field_PaymentCategory)."</td>\n";
$li->column_list .= "<td width='5%'>".$li->column($pos++, $i_Payment_Field_Amount)."</td>\n";
$li->column_list .= "<td width='5%'>".$li->column($pos++, $i_Payment_Field_DisplayOrder)."</td>\n";
$li->column_list .= "<td width='5%'>".$li->column($pos++, $i_Payment_Field_PayPriority)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_Payment_Field_PaidCount." (".$i_Payment_Field_TotalPaidCount.")")."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column($pos++, $i_general_description)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_general_last_modified_by)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_general_startdate)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_general_enddate)."</td>\n";
$li->column_list .= "<td width='1'>".$li->check("ItemID[]")."</td>\n";

$cats = $lpayment->returnPaymentCats();
$select_cat = getSelectByArray($cats,"name=CatID onChange=this.form.submit()",$CatID,1);

$select_item_paid_status = "<select name=\"itemPaidStatus\" onChange=\"this.form.submit()\">\n";
$select_item_paid_status .= "<option value=\"\" ".(!isset($itemPaidStatus) || $itemPaidStatus==""? "SELECTED":"").">$i_status_all</option>\n";
$select_item_paid_status .= "<option value=\"2\" ".(isset($itemPaidStatus) && $itemPaidStatus=="2"? "SELECTED":"").">$i_Payment_PaymentStatus_NotStarted</option>\n";
$select_item_paid_status .= "<option value=\"1\" ".(isset($itemPaidStatus) && $itemPaidStatus=="1"? "SELECTED":"").">$i_Payment_Field_AllPaid</option>\n";
$select_item_paid_status .= "<option value=\"0\" ".(isset($itemPaidStatus) && $itemPaidStatus=="0"? "SELECTED":"").">$i_Payment_Field_NotAllPaid</option>\n";
$select_item_paid_status .= "</select>\n";

$select_item_status="<SELECT name=itemStatus onChange=this.form.submit()>\n";
//$select_item_status.= "<OPTION value='' ".($itemStatus==""? "SELECTED":"").">$i_status_all</OPTION>\n";
$select_item_status.="<OPTION value=0 ".($itemStatus=="0"?" SELECTED":"").">$i_Payment_Menu_Settings_PaymentItem_ActiveRecord</OPTION>";
$select_item_status.="<OPTION value=1 ".($itemStatus=="1"?" SELECTED":"").">$i_Payment_Menu_Settings_PaymentItem_ArchivedRecord</OPTION>";
$select_item_status.="</SELECT>";

if($itemStatus!=1){
	$toolbar = $linterface->GET_LNK_NEW("javascript:checkNew('new.php')","","","","",0);
}
$toolbar .= $linterface->GET_LNK_EXPORT("javascript:exportPage(document.form1,'item_export.php');","","","","",0);
$toolbar .= $linterface->GET_LNK_PRINT("javascript:openPrintPage();","","","","",0);

$filterbar .="$select_item_status&nbsp;";
$filterbar .= "$select_item_paid_status";
$filterbar .= "$select_cat";

$sql = "SELECT b.NoticeID, b.Title FROM PAYMENT_PAYMENT_ITEM as a
LEFT JOIN INTRANET_NOTICE as b ON a.NoticeID=b.NoticeID
WHERE a.NoticeID IS NOT NULL
$date_conds
AND b.RecordStatus=1
GROUP BY b.NoticeID ORDER BY b.Title ASC";
$PaymentNoticeArray = $lpayment->returnArray($sql);


if($itemStatus!=1){
	$table_tool .= "<td nowrap=\"nowrap\">
						<a href=\"javascript:checkAlert(document.form1,'ItemID[]','pay_all_items.php?notall=1','$i_Payment_alert_forcepay')\" class=\"tabletool\">
							<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_pay.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
							$button_pay
						</a>
					</td>";
	$table_tool .= "<td nowrap=\"nowrap\">
						<a href=\"javascript:AlertPost(document.form1,'pay_all_items.php','$i_Payment_alert_forcepayall');\" class=\"tabletool\">
							<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_pay.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
							".$Lang['ePayment']['PayAll']."
						</a>
					</td>";
	$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
	$table_tool .= "<td nowrap=\"nowrap\">
						<a href=\"javascript:checkArchive(document.form1,'ItemID[]','archive.php');\" class=\"tabletool\">
							<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_archive.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
							$button_archive
						</a>
					</td>";
	$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
	$table_tool .= "<td nowrap=\"nowrap\">
						<a href=\"javascript:checkRemove2(document.form1,'ItemID[]','remove.php')\" class=\"tabletool\">
							<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
							$button_remove
						</a>
					</td>";
	$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
	$table_tool .= "<td nowrap=\"nowrap\">
						<a href=\"javascript:checkEdit(document.form1,'ItemID[]','edit.php')\" class=\"tabletool\">
							<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
							$button_edit
						</a>
					</td>";
}
else {
	$table_tool .= "<td nowrap=\"nowrap\">
						<a href=\"javascript:checkUndoArchive(document.form1,'ItemID[]','archive_undo.php');\" class=\"tabletool\">
							<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_undo.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
							$button_restore
						</a>
					</td>";
}
$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:copyPaymentItems(document.form1,'ItemID[]','copy.php');\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_copy.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						".$Lang['Btn']['Copy']."
					</a>
				</td>";

$searchbar = "<input class=\"textboxnum\" type=\"text\" name=\"keyword\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= $linterface->GET_BTN($button_find, "button", "document.form1.submit();","submit2");


//$TAGS_OBJ[] = array($i_Payment_Menu_Settings_PaymentItem, "", 0);
$TAGS_OBJ[] = array($i_Payment_Menu_Settings_PaymentItem, "",1);
$TAGS_OBJ[] = array($Lang['ePayment']['StudentPaymentItemSettings'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/student_payment_item.php",0);
if($sys_custom['ePayment']['PaymentItemChangeLog']){
	$TAGS_OBJ[] = array($Lang['ePayment']['PaymentItemChangeLog'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/change_log/",0);
}

$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();

$Msg = urldecode($Msg);
$linterface->LAYOUT_START($Msg);

//if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.css" type="text/css" />
<script language="javascript">
function viewNotice(id)
{
<?
	if ($intranet_version == "2.5" || $intranet_version == "3.0") {
		//$PathToNotice = '/home/eService/notice/sign.php';
		$PathToNotice = '/home/eService/notice/eNoticePayment_sign.php';
	}
	else {
		$PathToNotice = '/home/notice/sign.php';
	}
?>
	newWindow('<?=$PathToNotice?>?NoticeID='+id,10);
}

function checkForm(formObj) {
	if(formObj==null) return false;
	fromV = formObj.FromDate;
	toV= formObj.ToDate;
	if(!checkDate(fromV)){
		return false;
	} else if(!checkDate(toV)) {
		return false;
	}
	return true;
}
function checkDate(obj){
	if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
	return true;
}
function submitForm(obj){
	if(checkForm(obj))
		obj.submit();	
}
function checkRemove2(obj,element,page){
	if(countChecked(obj,element)==0)
		alert(globalAlertMsg2);
	else {
            obj.action=page;                
            obj.method="POST";
            obj.submit();				             
    }
}

$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
$(document).ready(function(){ 
	$('input#FromDate').datepick({
		dateFormat: 'yy-mm-dd',
		dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		changeFirstDay: false,
		firstDay: 0,
        onSelect: function() {
            updatePaymentNoticeList();
        }
    });
	$('input#ToDate').datepick({
		dateFormat: 'yy-mm-dd',
		dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		changeFirstDay: false,
		firstDay: 0,
        onSelect: function() {
            updatePaymentNoticeList();
        }
    });
});

function updatePaymentNoticeList() {
    $.post(
        '../../ajax_get_payment_notice.php',
        $("form[name='form1']").serialize(),
        function(returnMsg){
           $("#PaymentNoticeID").html(returnMsg);
        }
    );
}

function openPrintPage()
{
	 q_str = "PaymentNoticeID=<?=$PaymentNoticeID?>&FromDate=<?=$FromDate?>&ToDate=<?=$ToDate?>&CatID=<?=$CatID?>&itemStatus=<?=$itemStatus?>&itemPaidStatus=<?=$itemPaidStatus?>&keyword=<?=$keyword?>&pageNo=<?=$li->pageNo?>&order=<?=$li->order?>&field=<?=$li->field?>&numPerPage=<?=$li->page_size?>";
	 url="item_print.php?"+q_str; 
     newWindow(url,8);
}
function exportPage(obj,url){
		old_url = obj.action;
        obj.action=url;
        obj.submit();
        obj.action = old_url;
        
}
function checkArchive(obj,element,page){
  if(countChecked(obj,element)==0)
          alert(globalAlertMsg2);
  else{
    if(confirm('<?=$i_Payment_Menu_Settings_PaymentItem_Archive_Warning?>')){
          obj.action=page;                
          obj.method="POST";
          obj.submit();	
      }			             
  }
}
function checkUndoArchive(obj,element,page){
  if(countChecked(obj,element)==0)
          alert(globalAlertMsg2);
  else{
    if(confirm('<?=$i_Payment_Menu_Settings_PaymentItem_UndoArchive_Warning?>')){
          obj.action=page;                
          obj.method="POST";
          obj.submit();	
      }			             
  }	
}

function copyPaymentItems(obj,element,page)
{
	if(countChecked(obj,element)==0){
		alert(globalAlertMsg2);
	}else{
	    obj.action=page;                
	    obj.method="POST";
	    obj.submit();	
	}
}

$(document).ready(function(){
	$('a.notice-link').attr('title','<?=$Lang['ePayment']['ViewNotice']?>');
});
</script>
<br />
<form name="form1" method="get">
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td colspan="2" class="tabletext"><?=$i_Payment_Menu_Settings_PaymentItem_SelectDateRange?>:</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_Profile_From ?></td>
		<td valign="top" class="tabletext" width="70%">
			<input type="text" name="FromDate" id="FromDate" value="<?=$FromDate?>" maxlength="10" class='textboxnum'>
			<span class="tabletextremark">(yyyy-mm-dd)</span>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_Profile_To ?></td>
		<td valign="top" class="tabletext" width="70%">
			<input type="text" name="ToDate" id="ToDate" value="<?=$ToDate?>" maxlength="10" class='textboxnum'>
			<span class="tabletextremark">(yyyy-mm-dd)</span>
		</td>
	</tr>
    <tr>
        <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?=$Lang['eNotice']['PaymentNotice']?></td>
        <td valign="top" class="tabletext" width="70%">
            <?=$linterface->GET_HIDDEN_INPUT("SelectedPaymentNoticeID","SelectedPaymentNoticeID",$PaymentNoticeID);?>
            <?=$linterface->GET_SELECTION_BOX($PaymentNoticeArray,'id="PaymentNoticeID" name="PaymentNoticeID"', $Lang['General']['All'], $PaymentNoticeID);?>
        </td>
    </tr>
	<tr>
		<td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	</tr>
	<tr>
		<td colspan="2" align="center" class="tabletext">
			<?= $linterface->GET_ACTION_BTN($button_view, "button", "submitForm(document.form1);","submit2") ?>
		</td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td class="tabletext" align="right" colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td align="left"><?=$toolbar?></td>
		<td align="right">&nbsp;</td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr class="table-action-bar">
		<td align="left"><?= $filterbar; ?><?=$searchbar?></td>
		<td valign="bottom" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<?=$table_tool?>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<?php echo $li->display("96%"); ?>
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td align="left">
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_view.gif" border=0> 
			<span class="tabletextremark"> - <?=$Lang['ePayment']['PaymentItemGenerateFromNotice']?></span> 
		</td>
	</tr>
</table>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>
<br />
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>