<?php
// Editing by 
/*
 * 2015-11-23 (Carlos): $sys_custom['ePayment']['PaymentItemWithPOS'] get an ePOS item table row.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_item.php");
include_once($PATH_WRT_ROOT."includes/libpos_category.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$RowNum = $_POST['RowNum'];
$RowCategoryID = $_POST['RowCategoryID'];
$RowItemID = $_POST['RowItemID'];
$RowQuantity = $_POST['RowQuantity'];

$libpos = new libpos();
$libpos_ui = new libpos_ui();
/*
$objItem = new POS_Item($ItemID);
$CategoryID = $objItem->CategoryID;
$libpos_cat = new POS_Category($CategoryID);
$CategoryName = $libpos_cat->CategoryName;
$ItemName = intranet_htmlspecialchars($objItem->ItemName);
$Barcode = intranet_htmlspecialchars($objItem->Barcode);
$UnitPrice = intranet_htmlspecialchars($objItem->UnitPrice);
$Amount = $UnitPrice * $Quantity;
*/

$SelectedCategoryID = $RowCategoryID;
$SelectedItemID = $RowItemID;
$SelectedQuantity = $RowQuantity==''?0:$RowQuantity;

$libpos_cat = new POS_Category($SelectedCategoryID);
$libpos_item = new POS_Item($SelectedItemID);

$unit_price = $libpos_item->UnitPrice;
$barcode = intranet_htmlspecialchars($libpos_item->Barcode);
$item_count = min($libpos_item->ItemCount,100);
$total_amount = $unit_price * $SelectedQuantity;

$categories = $libpos_cat->Get_All_Category('', 1);
$cat_list = array();
for($i=0;$i<count($categories);$i++){
	$cat_list[] = array($categories[$i]['CategoryID'],$categories[$i]['CategoryName']);
}

$items = $libpos_cat->Get_All_Items(1);
$num_item = count($items);

$item_list = array();
for($i=0;$i<$num_item;$i++){
	
	$display = $items[$i]['ItemName'].' ('.$libpos->CurrencySign.$items[$i]['UnitPrice'].')';
	$item_list[] = array($items[$i]['ItemID'], $display);
}

$quantity_list = array();
for($i=0;$i<=$item_count;$i++){
	$quantity_list[] = array($i,$i);
}

$category_selection = getSelectByArray($cat_list, $tags=' name="RowCategoryID[]" onchange="rowCategorySelectionChanged(this);" ', $selected=$SelectedCategoryID, $all=0, $noFirst=0, $FirstTitle="", $ParQuoteValue=1);

$item_selection = getSelectByArray($item_list, $tags=' name="RowItemID[]" onchange="rowItemSelectionChanged(this);" ', $selected=$SelectedItemID, $all=0, $noFirst=0, $FirstTitle="", $ParQuoteValue=1);

$quantity_selection = getSelectByArray($quantity_list, $tags=' name="RowQuantity[]" onchange="rowQuantitySelectionChanged(this);" ', $selected=$SelectedQuantity, $all=0, $noFirst=0, $FirstTitle="", $ParQuoteValue=1);

$is_selected_row = $SelectedQuantity > 0 && $SelectedCategoryID != '' && $SelectedItemID != '';

$x .= '<tr class="ItemRow'.($is_selected_row?' selected_row':'').'">
		<td class="RowNumber">'.$RowNum.'</td>
		<td class="RowCategory">'.$category_selection.'</td>
		<td class="RowItem">'.$item_selection.'</td>
		<td class="RowAmount">'.$libpos->CurrencySign.$total_amount.'<input type="hidden" name="RowAmount[]" value="'.$total_amount.'" />&nbsp;</td>
		<td class="RowUnitPrice">'.($unit_price==''? '': $libpos->CurrencySign.$unit_price).'&nbsp;</td>
		<td class="RowBarcode">'.$barcode.'&nbsp;</td>
		<td class="RowQuantity" colspan="'.($sys_custom['ePayment']['PaymentMethod']? '4':'3').'">'.$quantity_selection.'</td>';
$x .= '<td>
		<div class="table_row_tool">
			<a title="Delete" class="delete_dim" href="javascript:void(0);" onclick="deletePurchasedItem('.$RowNum.');"></a>
		</div>
		</td>
		</tr>';

/*
$x .= '<tr class="ItemRow">
		<td class="RowNumber">'.$RowNum.'</td>
		<td>'.$ItemName.'<input type="hidden" name="PurchasedItemID[]" value="'.$ItemID.'" /></td>
		<td>'.$CategoryName.'</td>
		<td>'.$libpos->CurrencySign.$Amount.'</td>
		<td>'.$Quantity.'<input type="hidden" name="PurchasedItemQuantity[]" value="'.$Quantity.'" /></td>
		<td>'.$libpos->CurrencySign.$UnitPrice.'<input type="hidden" name="PurchasedItemUnitPrice[]" value="'.$UnitPrice.'" /></td>
		<td>'.$Barcode.'</td>
		<td>&nbsp;</td>';
if($sys_custom['ePayment']['PaymentMethod']){
	$x.= '<td>&nbsp;</td>';	
}
	$x .= '<td colspan="2"><div class="table_row_tool">
			<a title="Edit" href="javascript:void(0);" onclick="Get_POS_Form(\''.$ItemID.'\');" class="edit_dim"></a>
			<a title="Delete" class="delete_dim" href="javascript:void(0);" onclick="deletePurchasedItem('.$RowNum.');"></a>
			</div></td>';
$x .= '</tr>';
*/

echo $x;

intranet_closedb();
?>