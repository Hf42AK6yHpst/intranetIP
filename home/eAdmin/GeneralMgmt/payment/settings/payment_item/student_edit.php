<?php
// Editing by 
##################################
#	Date:	2016-06-22 Carlos
#			$sys_custom['ePayment']['PaymentItemAllowNegativeAmount'] - cust to allow input negative amount for payment item.
#
#	Date:	2014-10-16 Carlos [ip2.5.5.10.1] - use flag $sys_custom['ePayment']['SubsidySourceNoLimitChecking'] to eliminate the checking on subsidyy source amount limit
#
#	Date:	2014-08-20 Carlos
#			- $sys_custom['ePayment']['PaymentItemChangeLog'] - added navigation tag [Payment Item Change Log]
#
#	Date:	2014-05-23  Carlos
#			- Added [Apply To All Records] function
#
#	Date:	2014-03-24	Carlos
#			- Removed checking on whether subsidy source enough or not to subsidy students. Allow negative subsidy sum.
#
#	Date:	2014-03-20	Carlos
#			- Format floating point values to 2 decimal places
#
#	Date:	2013-11-25 	Carlos
#			- add remark for payment item 
#
#	Date:	2013-10-18 	Carlos
#			- modified to allow input multiple source of subsidy
#			
#	Date:	2012-03-06	YatWoon
#			- add student ordering (classname > classnumber)
#
##################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PaymentItemSettings";

if($ItemID=="" || sizeof($PaymentID)<=0 || $PaymentID==""){
	header("Location: index.php");
}



$linterface = new interface_html();

$TAGS_OBJ[] = array($i_Payment_Menu_Settings_PaymentItem, $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/",1);
$TAGS_OBJ[] = array($Lang['ePayment']['StudentPaymentItemSettings'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/student_payment_item.php",0);
if($sys_custom['ePayment']['PaymentItemChangeLog']){
	$TAGS_OBJ[] = array($Lang['ePayment']['PaymentItemChangeLog'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/change_log/",0);
}
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();

$PAGE_NAVIGATION[] = array("$button_edit $i_Payment_Field_Amount");


//$itemName = $lpayment->returnPaymentItemName($ItemID);

$namefield = getNameFieldWithClassNumberByLang("b.");

$list = is_array($PaymentID)? "'".implode("','",$PaymentID)."'": "'".$PaymentID."'";

$sql = "SELECT Name,DefaultAmount FROM PAYMENT_PAYMENT_ITEM WHERE ItemID='$ItemID'";
$item_info = $lpayment->returnResultSet($sql);
$itemName = $item_info[0]['Name'];
$itemDefaultAmount = $item_info[0]['DefaultAmount'];

$sql = "SELECT a.PaymentID,a.RecordStatus, a.Amount,a.SubsidyAmount,a.StudentID,$namefield,a.SubsidyUnitID,a.Remark FROM PAYMENT_PAYMENT_ITEMSTUDENT AS a LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) WHERE PaymentID IN ($list)";
// $sql .= " order by b.ClassName, b.ClassNumber";
$payment_infos = $lpayment->returnArray($sql,8);



### Get Subsidy Unit info
$subsidy = array();
$available_sub = array();
$sql="SELECT PaymentID FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE PaymentID IN ($list) AND RecordStatus=0";
$temp = $lpayment->returnVector($sql);

$target_list = implode(",",$temp);
/*
$sql="SELECT a.UnitID, a.UnitName,a.TotalAmount, SUM(c.SubsidyAmount),a.RecordStatus FROM PAYMENT_SUBSIDY_UNIT AS a 
		LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT AS b ON ( a.UnitID = b.SubsidyUnitID AND b.PaymentID NOT IN ($target_list)) 
		LEFT JOIN PAYMENT_PAYMENT_ITEM_SUBSIDY as c ON c.PaymentID=b.PaymentID AND c.UserID=b.StudentID AND c.SubsidyUnitID=a.UnitID 
		GROUP BY a.UnitID ORDER BY a.UnitID";
*/
$sql="SELECT a.UnitID, a.UnitName,a.TotalAmount, SUM(c.SubsidyAmount),a.RecordStatus 
		FROM PAYMENT_SUBSIDY_UNIT AS a 
		LEFT JOIN PAYMENT_PAYMENT_ITEM_SUBSIDY as c ON c.SubsidyUnitID=a.UnitID AND c.PaymentID NOT IN ($target_list) 
		GROUP BY a.UnitID ORDER BY a.UnitID";
$temp = $lpayment->returnArray($sql,5);
$js_subsidy_units = 'var subsidy_unit_ary = [{UnitID:\'\',UnitName:\''.$i_Payment_Subsidy_Option_No.'\',Left:0}];'."\n";
for($i=0;$i<sizeof($temp);$i++){
	list($sub_unit_id,$sub_name,$sub_total,$sub_used,$r_status)=$temp[$i];
	$subsidy[$sub_unit_id]['name'] = $sub_name;
	$subsidy[$sub_unit_id]['total'] = $sub_total;
	$subsidy[$sub_unit_id]['left'] = $sub_total - $sub_used > 0 ? $sub_total - $sub_used : 0;
	$subsidy[$sub_unit_id]['status']=$r_status;
	//if($subsidy[$sub_unit_id]['left']>0)
		$available_sub[]=array($sub_unit_id,$sub_name,$r_status);
		
	if($r_status == 1){
		$js_subsidy_units .= 'subsidy_unit_ary.push({UnitID:\''.$sub_unit_id.'\',UnitName:\''.str_replace('\'','\\\'',$sub_name).'\',Left:'.$subsidy[$sub_unit_id]['left'].'});'."\n";
	}
}

/*
$PaymentID = (is_array($PaymentID)? $PaymentID[0] : $PaymentID);

$itemName = $lpayment->returnPaymentItemName($ItemID);

$paymentInfo = $lpayment->returnPaymentInfo($PaymentID);

list($status, $amount, $studentID) = $paymentInfo;
if ($status == 1)
{
    header("Location: list.php?ItemID=$ItemID&paid=1");
    exit();
}

$lu = new libuser($studentID);
*/
$linterface->LAYOUT_START();
?>
<script language='javascript'>
<?=$js_subsidy_units?>

function getSubsidyUnitSelectionHtml(id,name,selectedValue,onChangeHandler)
{
	var select_html = '<select ';
	if(id && id != ''){
		select_html += ' id="'+id+'" ';
	}
	if(name && name != ''){
		select_html += ' name="'+name+'" ';
	}
	if(onChangeHandler && onChangeHandler != '') {
		select_html += ' onchange="'+onChangeHandler+'" ';
	}
	select_html += '>';
	
	for(var i=0;i<subsidy_unit_ary.length;i++) {
		select_html += '<option value="'+subsidy_unit_ary[i]['UnitID']+'" ';
		if(subsidy_unit_ary[i]['UnitID'] == selectedValue) {
			select_html += ' selected="selected" ';
		}
		select_html += '>'+subsidy_unit_ary[i]['UnitName']+'</option>';
	}
	
	return select_html;
}

function addSubsidyItemTableRow(userId)
{
	var btnRow = $('input#add_btn_'+userId).closest('tr');
	var rowClass = btnRow.attr('class');
	
	var row_html = '<tr class="'+rowClass+' subsidy"><td class="'+rowClass+'"><?=$i_Payment_Subsidy_Unit?></td><td>'+getSubsidyUnitSelectionHtml('','unitid_'+userId+'[]','','subsidyItemChanged(this,\''+userId+'\');')+'</td>';
	row_html += '<td><?=$i_Payment_Subsidy_Amount?> <input type="text" class="textboxnum" name="new_subsidy_amount_' + userId +'[]" value="0" onblur="recalculateSubsidyAmount(\''+userId+'\');"></td><td><span class="table_row_tool row_content_tool"><a href="javascript:void(0);" class="delete_dim" onclick="removeSubsidyItemRow(this,\''+userId+'\');"></a></span><span style="color:red;" class="WarnSpan"></span></td></tr>';
	
	btnRow.before(row_html);
}

function removeSubsidyItemRow(obj,userId)
{
	var row = $(obj).closest('tr');
	if(row){
		row.remove();
		recalculateSubsidyAmount(userId);
	}
}

function subsidyItemChanged(obj,userId)
{
	var unit_item_select = $(obj);
	var unit_id_ary = document.getElementsByName('unitid_'+userId+'[]');
	var new_subsidy_amount_ary = document.getElementsByName('new_subsidy_amount_'+userId+'[]');
	
	for(var i=0;i<unit_id_ary.length;i++){
		for(var j=0;j<unit_id_ary.length;j++){
			var item_i = $(unit_id_ary[i]);
			var item_j = $(unit_id_ary[j]);
			if(item_i.val() == ''){
				new_subsidy_amount_ary[i].value = 0;
			}
			if(item_j.val() == ''){
				new_subsidy_amount_ary[j].value = 0;
			}
			if(i!=j) {
				if(item_i.val()!='' && item_j.val()!='' && item_i.val() == item_j.val()) {
					// duplicated unit source selection
					alert('<?=$Lang['ePayment']['WarningMsg']['SourceOfSubsidyDuplicated']?>');
					if(obj == unit_id_ary[i])
					{
						item_i.val('');
					}else{
						item_j.val('');
					}
				}
			}
		}
		if(unit_id_ary.value != '' && new_subsidy_amount_ary[i].value != 0) {
			$(unit_id_ary[i]).closest('tr').find('span.WarnSpan').html('');
		}
	}
}

function newAmountEdited(userId)
{
	var old_amount = $('input#old_amount_'+userId).val();
	var new_amount_input = $('input#new_amount_'+userId);
	var new_amount = parseFloat(new_amount_input.val(),10);
	if(!(!isNaN(new_amount) 
	<?php if(!$sys_custom['ePayment']['PaymentItemAllowNegativeAmount']){ ?> 
		&& new_amount >=0 
	<?php } ?>
		))
	{	
		new_amount_input.val(old_amount);
	}
	
	recalculateSubsidyAmount(userId);
}

function recalculateSubsidyAmount(userId)
{
	var new_amount = parseFloat($('#new_amount_'+userId).val(),10);
	var cur_subsidy_amount_span = $('#cur_subsidy_amount_'+userId);
	var pay_amount_input = $('#pay_amount_'+userId);
	var new_subsidy_amount_ary = document.getElementsByName('new_subsidy_amount_'+userId+'[]');
	var unit_id_ary = document.getElementsByName('unitid_'+userId+'[]');
	
	var total_subsidy_amount = 0;
	
	var student_id_ary = document.getElementsByName('StudentIDs[]');
	if(userId == 'all'){
		student_id_ary = [{'value':'all'}];
	}
	
	// for each subsidy source
	for(var i=0;i<subsidy_unit_ary.length;i++){
		var unit_id = subsidy_unit_ary[i]['UnitID'];
		var amount_left = subsidy_unit_ary[i]['Left'];
		var unit_subsidy_sum = 0;
		// for each student 
		for(var j=0;j<student_id_ary.length;j++){
			var uid = student_id_ary[j].value;
			var user_new_subsidy_amount_ary = document.getElementsByName('new_subsidy_amount_'+uid+'[]');
			var user_unit_id_ary = document.getElementsByName('unitid_'+uid+'[]');
			var user_new_amount = parseFloat($('#new_amount_'+uid).val(),10);
			var user_subsidy_sum = 0;
			// for each student subsidy item
			for(var k=0;k<user_unit_id_ary.length;k++){
				var tmp_unit = $(user_unit_id_ary[k]);
				if(tmp_unit.val() == ''){
					user_new_subsidy_amount_ary[k].value = 0;
				}
				var tmp_amount = parseFloat(user_new_subsidy_amount_ary[k].value,10);
				if(!(!isNaN(tmp_amount) && tmp_amount >= 0)){
					user_new_subsidy_amount_ary[k].value = 0;
					tmp_amount = 0;
				}
				// check this student subsidy sum exceed amount to pay
				if(user_new_amount < (user_subsidy_sum + tmp_amount)){
					tmp_amount = user_new_amount - user_subsidy_sum;
					user_new_subsidy_amount_ary[k].value = tmp_amount.toFixed(2);
				}
				user_subsidy_sum += tmp_amount;
				
				// check unit source have enough money to subsidy
				if(unit_id == tmp_unit.val()) {
					if(amount_left < (unit_subsidy_sum + tmp_amount)) {
	<?php if(!$sys_custom['ePayment']['SubsidySourceNoLimitChecking']){ ?>
						var max_subsidy = tmp_amount - (unit_subsidy_sum + tmp_amount - amount_left);
						user_new_subsidy_amount_ary[k].value = max_subsidy.toFixed(2);
						tmp_amount = max_subsidy;
						
						var warn_span = tmp_unit.closest('tr').find('span.WarnSpan');
						warn_span.html('<?=$Lang['ePayment']['WarningMsg']['SourceOfSubsidyNotEnoughMoney']?>');
/*						setTimeout(function(){
							var span_obj = warn_span;
							span_obj.html('');
						},3000);  */
	<?php } ?>
					}else if(tmp_unit.val() != '' && tmp_amount > 0){
						tmp_unit.closest('tr').find('span.WarnSpan').html('');
					}
					unit_subsidy_sum += tmp_amount;
				}
			}
		}
	}
	
	for(var i=0;i<new_subsidy_amount_ary.length;i++){
		var tmp_amount = parseFloat(new_subsidy_amount_ary[i].value,10);
		if(tmp_amount > new_amount) {
			new_subsidy_amount_ary[i].value = new_amount;
			tmp_amount = new_amount;
		}
		total_subsidy_amount += tmp_amount;
	}
	
	if(new_amount < 0){
		
	}else if(total_subsidy_amount > new_amount){
		total_subsidy_amount = new_amount;
	}
	cur_subsidy_amount_span.html(total_subsidy_amount.toFixed(2));
	pay_amount_input.val((new_amount - total_subsidy_amount).toFixed(2));
}

function applyToAll()
{
	var new_amount = $('#new_amount_all').val();
	var cur_subsidy_amount = $('#cur_subsidy_amount_all').html();
	var remark = $('#Remark_all').val();
	var unit_objs = document.getElementsByName('unitid_all[]');
	var subsidy_amount_objs = document.getElementsByName('new_subsidy_amount_all[]');
	var student_id_ary = document.getElementsByName('StudentIDs[]');
	
	for(var i=0;i<student_id_ary.length;i++)
	{
		var sid = student_id_ary[i].value;
		var ref_row = $('#cur_subsidy_amount_'+sid).closest('tr');
		ref_row.siblings('.subsidy').remove();
		
		$('#new_amount_'+sid).val(new_amount);
		newAmountEdited(sid);
		
		for(var j=0;j<unit_objs.length;j++)
		{
			addSubsidyItemTableRow(sid);
		}
		var new_unit_objs = document.getElementsByName('unitid_'+sid+'[]');
		var new_amount_objs = document.getElementsByName('new_subsidy_amount_'+sid+'[]');
		for(var j=0;j<unit_objs.length;j++)
		{
			$(new_unit_objs[j]).val($(unit_objs[j]).val());
			$(new_amount_objs[j]).val($(subsidy_amount_objs[j]).val());
		}
		
		recalculateSubsidyAmount(sid);
		
		$('input#Remark_'+sid).val(remark);
	}
}

/*
function getLeftAmount(){
	amount = new Array();
	<?php
	foreach($subsidy as $sub_id => $values){
		echo "\tamount.push(new Array($sub_id,".$values['left']."));\n";
	}
	?>
	return amount;
}
function checkAmount(obj){

	if(!check_numeric(obj,obj.value,'<?=$i_Payment_Warning_InvalidAmount?>')) return false;
	return true;

}


function recalculateAmount(sid){
	obj = document.form1;
	objCurrentAmount = getObj('old_amount_'+sid);
	objNewAmount = getObj('new_amount_'+sid);
	objNewSubsidy = getObj('new_subsidy_amount_'+sid);
	
	if(!checkAmount(objNewAmount)) return;
	
	current_amount = parseFloat(objCurrentAmount.value,10);
	
	new_amount = parseFloat(objNewAmount.value,10);
	
	left = current_amount - new_amount;
	
	new_subsidy_amount = left>0?left:0;
	
	objNewSubsidy.value = new_subsidy_amount;
	
}
function getObj(objName){
	obj = document.getElementsByName(objName);
	return obj[0];
}

function checkform(obj){
	students = document.getElementsByName('StudentIDs[]');
	left = getLeftAmount();
	for(x=0;x<students.length;x++){
		sid = students[x].value;
		
		objNewAmount = getObj('new_amount_'+sid);
		objNewSubsidy = getObj('new_subsidy_amount_'+sid);
		objUnitID = getObj('unitid_'+sid);
		
		if(!checkAmount(objNewAmount)) return false;
		if(!checkAmount(objNewSubsidy)) return false;
		
		selectedUnit = objUnitID.options[objUnitID.selectedIndex].value;
		
		if(selectedUnit=='' && objNewSubsidy.value>0){
			alert('<?=$i_Payment_Subsidy_Warning_Select_SubsidyUnit?>');
			objUnitID.focus();
			return false;
		}
		if(selectedUnit!='' && objNewSubsidy.value<=0){
			alert('<?=$i_Payment_Subsidy_Warning_Enter_New_SubsidyAmount?>');
			objNewSubsidy.focus();
			return false;
		}
		newSubsidyAmount = objNewSubsidy.value;
		if(newSubsidyAmount>0){
			for(j=0;j<left.length;j++){
				if(left[j][0] == selectedUnit){
					if(left[j][1] >=newSubsidyAmount){
						left[j][1] -= newSubsidyAmount;
					}else{
						alert('<?=$i_Payment_Subsidy_Warning_NotEnough_Amount?>');
						objNewSubsidy.focus();
						return false;
					}
				}
			}
		}
		
	}
	if(confirm('<?=$i_Payment_Subsidy_Edit_Confirm?>')){
		return true;
	}
	return false;
}
*/

function checkform(obj)
{
	var student_id_ary = document.getElementsByName('StudentIDs[]');
	var is_all_valid = true;
	// for each student 
	for(var j=0;j<student_id_ary.length;j++){
		var uid = student_id_ary[j].value;
		var user_unit_id_ary = document.getElementsByName('unitid_'+uid+'[]');
		var user_new_subsidy_amount_ary = document.getElementsByName('new_subsidy_amount_'+uid+'[]');
		
		// for each student subsidy item
		for(var k=0;k<user_unit_id_ary.length;k++){
			var tmp_unit = $(user_unit_id_ary[k]);
			var tmp_amount = $(user_new_subsidy_amount_ary[k]);
			var warn_span = tmp_unit.closest('tr').find('span.WarnSpan');
			if(tmp_unit.val() == '' || tmp_amount.val() == '0'){
				warn_span.html('<?=$Lang['ePayment']['WarningMsg']['PleaseRemoveThisSourceOfSubsidy']?>');
				is_all_valid = false;
			}else{
				warn_span.html('');
			}
		}
	}
	
	if(is_all_valid) {
		if(confirm('<?=$i_Payment_Subsidy_Edit_Confirm?>')){
			return true;
		}
	}
	return false;
}
</script>

<br />
<form name=form1 action="student_edit_update.php" method=POST onSubmit='return checkform(this)'>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
    	<td>
    		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
			    <tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_Payment_Field_PaymentItem?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<?=$itemName?>
	    			</td>
    			</tr>
				
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">&nbsp;</td>
					<td class="tabletext" width="70%">
						<table border="0" cellspacing="0" cellpadding="3" width="100%">
							<tr class="tabletext tablerow2">
								<td class="tabletext tablerow2" width="20%"><?=$i_Payment_Subsidy_New_Amount?> <span class="tabletextrequire">*</span></td>
								<td><input type="text" class="textboxnum" id="new_amount_all" name="new_amount_all" value="<?=$itemDefaultAmount?>" onblur="newAmountEdited('all')"><input type="hidden" id="old_amount_all" name="old_amount_all" value="<?=$itemDefaultAmount?>"></td>
								<td class="tabletext tablerow2">&nbsp;</td>
								<td class="tabletext tablerow2" align="right" vlaign="top"><?=$linterface->GET_SMALL_BTN($Lang['ePayment']['ApplyToAllRecords'],"button","applyToAll();","apply_all_btn")?></td>
							</tr>
							<tr class="tabletext tablerow2">
								<td class="tabletext tablerow2" colspan="4">&nbsp;</td>
							</tr>
							<tr class="tabletext tablerow2">
								<td class="tabletext tablerow2" width="20%"><?=$i_Payment_Subsidy_Current_Subsidy_Amount?></td>
								<td class="tabletext tablerow2">$ <span id="cur_subsidy_amount_all">0</span><input type="hidden" class="textboxnum" id="pay_amount_all" name="pay_amount_all" value="<?=$itemDefaultAmount?>"></td>
								<td class="tabletext tablerow2" colspan="2">&nbsp;</td>
							</tr>
							<?php
								$add_subsidy_item_btn = $linterface->GET_SMALL_BTN("+","button","addSubsidyItemTableRow('all');","add_btn_all");
							?>
							<tr class="tabletext tablerow2">
								<td class="tabletext tablerow2" width="20%"><?=$add_subsidy_item_btn?></td>
								<td class="tabletext tablerow2" colspan="3">&nbsp;</td>
							</tr>
							<tr class="tabletext tablerow2">
								<td class="tabletext tablerow2" colspan="4">&nbsp;</td>
							</tr>
							<tr class="tabletext tablerow2">
								<td class="tabletext tablerow2" width="20%"><?=$Lang['General']['Remark']?></td>
								<td><?=$linterface->GET_TEXTBOX("Remark_all", "Remark_all", $t_remark, '', array())?></td>
							</tr>
						</table>
					</td>
				</tr>
				
			</table>
		</td>
	</tr>
	<tr><td>
	<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr class='tabletop'><td width='30%' class='tabletoplink' ><?=$i_UserName?></td><td class='tabletoplink'>&nbsp;</td></tr>

<?php
// print_r($payment_infos);
$valid_count=0;
$students=array();
for($i=0;$i<sizeof($payment_infos);$i++){
	list($payment_id,$paid_status, $t_amount,$t_subsidy,$student_id,$student_name,$t_unit_id, $t_remark) = $payment_infos[$i];
	
	// get subsidy amount records
	$sql = "SELECT a.SubsidyUnitID,a.SubsidyAmount FROM PAYMENT_PAYMENT_ITEM_SUBSIDY as a INNER JOIN PAYMENT_SUBSIDY_UNIT as b ON b.UnitID=a.SubsidyUnitID WHERE a.PaymentID='$payment_id' AND a.UserID='$student_id' ";
	$subsidyRecords = $lpayment->returnResultSet($sql);
	
	$css = $i%2==0?"tabletext tablerow":"tabletext tablerow2";
	$t_subsidy+=0;
	$t_amount+=0;
	
	$total_subsidy_amount = 0;
	$subsidy_item_rows = '';
	for($k=0;$k<sizeof($subsidyRecords);$k++) {
		
		$subsidy_unit_id = $subsidyRecords[$k]['SubsidyUnitID'];
		$subsidy_amount = $subsidyRecords[$k]['SubsidyAmount'];
		$total_subsidy_amount += $subsidy_amount;
		
		$select_unit ='<select name="unitid_'.$student_id.'[]" onchange="subsidyItemChanged(this,'.$student_id.');">'."\n";
		$select_unit .= '<option value="">'.$i_Payment_Subsidy_Option_No.'</option>'."\n";
		
		for($j=0;$j<sizeof($available_sub);$j++){
			list($unit_id,$unit_name,$unit_status)=$available_sub[$j];
			if($unit_id==$t_unit_id || $unit_status==1)
				$select_unit .= '<option value="'.$unit_id.'"'.($unit_id==$subsidy_unit_id?' selected="selected" ':'').'>'.$unit_name.($unit_status==1?'':' ('.$i_general_inactive.')').'</option>'."\n";
		}
		$select_unit.='</select>'."\n";
		
		$subsidy_item_rows .= '<tr class="'.$css.' subsidy"><td class="'.$css.'">'.$i_Payment_Subsidy_Unit.'</td><td>'.$select_unit.'</td>'."\n";
		$subsidy_item_rows .= '<td>'.$i_Payment_Subsidy_Amount.' <input type="text" class="textboxnum" name="new_subsidy_amount_'.$student_id.'[]" value="'.$subsidy_amount.'" onblur="recalculateSubsidyAmount('.$student_id.');"></td><td><span class="table_row_tool row_content_tool"><a href="javascript:void(0);" class="delete_dim" onclick="removeSubsidyItemRow(this,'.$student_id.');"></a></span><span style="color:red;" class="WarnSpan"></span></td></tr>'."\n";
		
	}
	
	
	?>	
	<tr class='<?=$css?>'>
		<td class='<?=$css?>' style='vertical-align:top'><?=($i+1).". ".$student_name?></td>
		<?php 
				if($paid_status==1){
					$remark_table  = '<td class="'.$css.'">';
					$remark_table .= '<table border=0 cellspacing=0 cellpadding=3>';
						$remark_table.= '<tr class="'.$css.'">';
							$remark_table.='<td class="'.$css.'" colspan="2"><font color="red">'.$i_Payment_Import_PayAlready.'</font></td>';
						$remark_table.= '</tr>';
						$remark_table.= '<tr class="'.$css.'">';
							$remark_table.= '<td class="'.$css.'">'.$Lang['General']['Remark'].'</td><td>'.$linterface->GET_TEXTBOX("Remark_$student_id", "Remark_$student_id", $t_remark, '', array()).'</td>';
						$remark_table.= '</tr>';
					$remark_table.= '</table>';
					echo '<input type="hidden" name="PaidPaymentID[]" value="'.$payment_id.'">'."\n";
					echo '<input type="hidden" name="PaidStudentIDs[]" value="'.$student_id.'">'."\n";
					$remark_table .= '</td>';
					//echo "<td class='$css'><font color=\"red\">$i_Payment_Import_PayAlready</font></td>";
					echo $remark_table;
				}
				else{
					$valid_count++;
					$students[] = $student_id;
					/*
					$select_unit ="<select name='unitid_$student_id'>";
					$select_unit .= "<OPTION value=''>$i_Payment_Subsidy_Option_No</OPTION>";
					
					for($j=0;$j<sizeof($available_sub);$j++){
						list($unit_id,$unit_name,$unit_status)=$available_sub[$j];
						if($unit_id==$t_unit_id || $unit_status==1)
							$select_unit .="<OPTION value='$unit_id' ".($unit_id==$t_unit_id?" SELECTED ":"").">$unit_name".($unit_status==1?"":" ($i_general_inactive)")."</OPTION>\n";
					}
					$select_unit.="</select>";
					*/
					echo '<td class="'.$css.'"><table border=0 cellspacing=0 cellpadding=3>';
					echo '<tr class="'.$css.'"><td class="'.$css.'">'.$i_Payment_Subsidy_Current_Amount.'</td><td>$'.($t_amount+$t_subsidy).'<input type="hidden" id="old_amount_'.$student_id.'" name="old_amount_'.$student_id.'" value="'.($t_amount+$t_subsidy).'"></td><td class="'.$css.'" colspan="2">&nbsp;</td></tr>';
					echo '<tr class="'.$css.'"><td class="'.$css.'">'.$i_Payment_Subsidy_New_Amount.' <span class="tabletextrequire">*</span></td><td><input type="text" class="textboxnum" id="new_amount_'.$student_id.'" name="new_amount_'.$student_id.'" value="'.($t_amount+$t_subsidy).'" onblur="newAmountEdited('.$student_id.')"></td><td class="'.$css.'" colspan="2">&nbsp;</td></tr>';
					echo '<tr class="'.$css.'"><td class="'.$css.'" colspan="4">&nbsp;</td></tr>';
					echo '<tr class="'.$css.'"><td class="'.$css.'">'.$i_Payment_Subsidy_Current_Subsidy_Amount.'<td>$ <span id="cur_subsidy_amount_'.$student_id.'">'. $t_subsidy.'</span></td><td class="'.$css.'" colspan="2">&nbsp;</td></tr>';
					
					//echo "<tr class='$css'><td class='$css'>$i_Payment_Subsidy_New_Subsidy</td><td><input type='text' class='textboxnum' name='new_subsidy_amount_$student_id' value='$t_subsidy'></td></tr>";
					//echo "<tr class='$css'><td class='$css'>$i_Payment_Subsidy_Unit</td><td>$select_unit</td></tr>";
					echo $subsidy_item_rows;
					$add_subsidy_item_btn = $linterface->GET_SMALL_BTN("+","button","addSubsidyItemTableRow($student_id);","add_btn_$student_id");
					echo '<tr class="'.$css.'"><td class="'.$css.'">'.$add_subsidy_item_btn.'</td><td class="'.$css.'" colspan="3">&nbsp;</td></tr>';
					echo '<tr class="'.$css.'"><td class="'.$css.'" colspan="4">&nbsp;</td></tr>';
					echo '<tr class="'.$css.'"><td class="'.$css.'">'.$i_Payment_Field_Chargeable_Amount.'</td><td><input type="text" class="textboxnum" id="pay_amount_'.$student_id.'" name="pay_amount_'.$student_id.'" value="'.($t_amount).'" readonly="readonly"></td><td class="'.$css.'" colspan="2">&nbsp;</td></tr>';
					
					echo '<tr class="'.$css.'"><td class="'.$css.'">'.$Lang['General']['Remark'].'</td><td>'.$linterface->GET_TEXTBOX("Remark_$student_id", "Remark_$student_id", $t_remark, '', array()).'</td></tr>';
					
					echo '</table>';
					echo '<input type="hidden" name="PaymentID[]" value="'.$payment_id.'">'."\n";
					echo '<input type="hidden" name="StudentIDs[]" value="'.$student_id.'">'."\n";
					echo '</td>';
				 }
		?>
	</tr>
<?}?>
</table>
		</td>
	</tr>
	<tr>
    	<td>
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
					<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
				</tr>
		    	<tr>
                	<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
    <tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_save, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='list.php?ItemID=$ItemID'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="ItemID" value="<?=$ItemID?>">
<input type="hidden" name="pageNo" value="<?php echo $pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $order; ?>">
<input type="hidden" name="field" value="<?php echo $field; ?>">
<input type="hidden" name="ClassName" value="<?=$ClassName?>">
</form>
<br />
<?
$linterface->LAYOUT_STOP();
print $linterface->FOCUS_ON_LOAD("form1.amount");
intranet_closedb();
?>
