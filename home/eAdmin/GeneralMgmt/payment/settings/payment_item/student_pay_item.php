<?php
#using: 
/*
 * 2017-02-09 (Carlos): $sys_custom['ePayment']['HartsPreschool'] - added payment time and default payment method.
 * 2015-11-23 (Carlos): $sys_custom['ePayment']['PaymentItemWithPOS'] - added confirmation of purchasing ePOS items at this page. 
 * 2015-10-27 (Carlos): Change alert and confirmation dialog with jquery alert, preopen new window before ajax to avoid browser blockage.
 * 2015-10-13 (Carlos): $sys_custom['ePayment']['AllowNegativeBalance'] - let not enough balance accounts to pay. 
 * 2015-04-30 (Carlos): Improved to allow add multiple add value items. 
 * 2015-02-18 (Carlos): Added [ReceiptRemark]
 * 2014-09-17 (Carlos): $sys_custom['ePayment']['ThermalPaperReceipt'] - Added thermal paper receipt
 * 2014-08-20 (Carlos): $sys_custom['ePayment']['PaymentItemChangeLog'] - added navigation tag [Payment Item Change Log]
 * 2014-03-24 (Carlos): $sys_custom['ePayment']['PaymentMethod'] - Added [Payment Method]
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lclass = new libclass();
$lpayment = new libpayment();
$lu = new libuser("$targetID");
$student_name=$lu->UserNameLang();

if (!isset($order)) $order = 1;
if (!isset($field)) $field = 3;
$order = ($order == 1) ? 1 : 0;

$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance'];

$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PaymentItemSettings";

$linterface = new interface_html();

$TAGS_OBJ[] = array($i_Payment_Menu_Settings_PaymentItem, $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/",0);
$TAGS_OBJ[] = array($Lang['ePayment']['StudentPaymentItemSettings'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/student_payment_item.php",1);
if($sys_custom['ePayment']['PaymentItemChangeLog']){
	$TAGS_OBJ[] = array($Lang['ePayment']['PaymentItemChangeLog'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/change_log/",0);
}
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();

$PAGE_NAVIGATION[] = array($i_Payment_action_batchpay);

$now = date("Y-m-d H:i:s");

if(count($PaymentID)>0)
{
	// admin in charge name field
	$Inchargenamefield = getNameFieldByLang("e.");
	if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
		$Inchargearchive_namefield = " IF(f.ChineseName IS NULL,f.EnglishName,f.ChineseName)";
		//$archive_namefield="c.ChineseName";
	}else {
		//$archive_namefield ="c.EnglishName";
		$Inchargearchive_namefield = "IF(f.EnglishName IS NULL,f.ChineseName,f.EnglishName)";
	}
	# Get List
	$list = "'".implode("','",$PaymentID)."'";
	//echo $list;
	
	$sql = " select 
		      b.Name,		  	
	          c.name,
			  ".$lpayment->getExportAmountFormatDB("a.Amount")." as Amount,
			  DATE_FORMAT(b.startdate,'%Y-%m-%d'),
	          DATE_FORMAT(b.enddate,'%Y-%m-%d'),
	          ".$lpayment->getExportAmountFormatDB("d.Balance")." as Balance,";
	if($sys_custom['ePayment']['PaymentMethod']){
		$sql .= " a.PaymentMethod, a.ReceiptRemark, ";
	}
	$sql.=" IF((e.UserID IS NULL AND f.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$Inchargearchive_namefield,'</i>'), IF(e.UserID IS NULL AND f.UserID IS NULL,IF(b.ProcessingAdminUser IS NULL OR b.ProcessingAdminUser = '','&nbsp;',b.ProcessingAdminUser),$Inchargenamefield)) as DisplayAdminInCharge,
				a.PaymentID,
				a.StudentID  
			 from   
			  PAYMENT_PAYMENT_ITEMSTUDENT as a 
			  INNER JOIN PAYMENT_PAYMENT_ITEM as b ON b.ItemID=a.ItemID 
			  LEFT JOIN PAYMENT_PAYMENT_CATEGORY as c ON c.CatID=b.CatID 
			  INNER JOIN PAYMENT_ACCOUNT as d ON d.StudentID=a.StudentID 
	          left join INTRANET_USER as e on b.ProcessingAdminUser = e.UserID
	          LEFT JOIN INTRANET_ARCHIVE_USER as f on b.ProcessingAdminUser = f.UserID
			 where 
	          a.paidtime is null and (a.RecordStatus=0 OR a.RecordStatus IS NULL) 
	          and a.paymentid IN ($list)	
			";
	$field_array = array("b.name","c.name","a.Amount","b.DisplayOrder,b.PayPriority","b.PayPriority","DisplayAdminInCharge","b.startdate","b.enddate");
	if($sys_custom['ePayment']['PaymentMethod']){
		$field_array[] = "a.PaymentMethod";
		$field_array[] = "a.ReceiptRemark";
	}
	$sql .= " order by ";
	$sql .= (!isset($field_array[$field])) ? $field_array[3] : $field_array[$field];
	$sql .= ($order==0) ? " DESC" : " ASC";		
	//	$sql .= " order by PAYMENT_PAYMENT_ITEM.name ASC";
			
	$result = $lpayment->returnArray($sql);
}

if($sys_custom['ePayment']['PaymentItemWithPOS']){
	
	$pos_item_id_ary = $_POST['RowItemID'];
	$pos_quantity_ary = $_POST['RowQuantity'];
	$posItemIdToQuantity = array();
	$selected_pos_item_id_ary = array();
	for($i=0;$i<count($pos_item_id_ary);$i++)
	{
		if($pos_quantity_ary[$i] != '' && $pos_quantity_ary[$i]>0){
			$posItemIdToQuantity[$pos_item_id_ary[$i]] = $pos_quantity_ary[$i];
			$selected_pos_item_id_ary[] = $pos_item_id_ary[$i];
		}
	}
	
	if(count($pos_item_id_ary)>0)
	{
		$sql = "SELECT 
					p.ItemID,
					p.CategoryID,
					c.CategoryName,
					p.ItemName,
					p.Barcode,
					p.UnitPrice,
					p.ItemCount 
				FROM POS_ITEM as p 
				LEFT JOIN POS_ITEM_CATEGORY as c ON c.CategoryID=p.CategoryID 
				WHERE p.ItemID IN ('".(implode("','",$selected_pos_item_id_ary))."') 
				ORDER BY c.Sequence,p.Sequence";
		$pos_records = $lpayment->returnResultSet($sql);
	}else{
		$pos_records = array();
	}
	//debug_r($pos_records);
}


$x = "";
$Enough=true;

$AddValueRowCount = intval($_REQUEST['AddValueRowCount']);
$AddValueAmount = 0;
$addValueAry = array();
for($i=0;$i<=$AddValueRowCount;$i++){
	if(isset($_POST['AddValueAmount_'.$i]) && $_POST['AddValueAmount_'.$i]!=''){
		$tmp_addvalue_amount = trim($_POST['AddValueAmount_'.$i]);
		$tmp_addvalue_ref = trim($_POST['AddValueRef_'.$i]);
		$tmp_addvalue_datetime = trim($_POST['AddValueDate_'.$i]).' '.$_POST['AddValueTime_'.$i.'_hour'].':'.$_POST['AddValueTime_'.$i.'_min'].':'.$_POST['AddValueTime_'.$i.'_sec'];
		$addValueAry[] = array('AddValueAmount'=>$tmp_addvalue_amount,'AddValueRef'=>$tmp_addvalue_ref,'AddValueDatetime'=>$tmp_addvalue_datetime);
		$AddValueAmount += $lpayment->getExportAmountFormat(floatval($tmp_addvalue_amount));
	}
}

if($sys_custom['ePayment']['HartsPreschool']){
	$studentIdToPaymentMethod = $lpayment->getStudentPaymentMethodRecords(array($targetID),1);
}

for ($i=0; $i<sizeof($result); $i++)
{    
	$payment_id = $result[$i]['PaymentID'];
	 $hidden = "<input type=hidden name=PaymentID[] value='".$payment_id."'>\n";
	
     list ($item_name, $categroy_name, $amount, $startdate, $enddate, $balance) = $result[$i];
     if(isset($AddValueAmount) && $AddValueAmount != '' && $AddValueAmount > 0){
     	$balance += $AddValueAmount + 0;
     }
     if($sys_custom['ePayment']['PaymentMethod']){
     	$payment_method = $result[$i]['PaymentMethod'];
     }
     
     if($i==0)$balanceAfter[$i] = $lpayment->getExportAmountFormat( $balance - $amount );
     else $balanceAfter[$i] = $lpayment->getExportAmountFormat( $balanceAfter[$i-1] - $amount );
     
     //if (abs($balanceAfter[$i]) < 0.001)    # Smaller than 0.1 cent
     //{
     //    $balanceAfter[$i] = 0;
     //}
     if ($balanceAfter[$i] < 0.00 && !$sys_custom['ePayment']['AllowNegativeBalance'])
     {
         $balanceAfterformed[$i] = $lpayment->getWebDisplayAmountFormat($balanceAfter[$i]);
         $balanceAfterformed[$i] = "<font color=red>$balanceAfterformed[$i]</font>";
         $Enough = false;

     }
     else
     {
     		$balanceAfterformed[$i] = $lpayment->getWebDisplayAmountFormat($balanceAfter[$i]);
     }

     $css = $i%2==0?"tablebluerow1 tabletext":"tablebluerow2 tabletext";
     $x .= "<tr class=\"$css\">\n";
     $x .= "<td class=\"tabletext\">$item_name</td>\n";
     $x .= "<td class=\"tabletext\">$categroy_name</td>\n";  
     $x .= "<td class=\"tabletext\">".$lpayment->getWebDisplayAmountFormat($amount)."$hidden</td>\n";
     $x .= "<td class=\"tabletext\">$startdate</td>\n";
     $x .= "<td class=\"tabletext\">$enddate</td>\n";     
	 if(!$isKIS) {
		 if ($lpayment->isEWalletDirectPayEnabled() == false) {
			 $x .= "<td class=\"tabletext\">" . $balanceAfterformed[$i];
			 $x .= "</td>\n";
		 }
	 }
	 if($sys_custom['ePayment']['HartsPreschool']){
	 	$x .= "<td class\"tabletext\">".$linterface->GET_TEXTBOX("PaymentTime[".$payment_id."]", "PaymentTime[".$payment_id."]", $now, $___OtherClass='', $___OtherPar=array('onchange'=>'this.value=formatDatetimeString(this.value);'))."</td>\n";
	 }
	 if($sys_custom['ePayment']['PaymentMethod']){
	 	$payment_method = $result[$i]['PaymentMethod'];
     	if($sys_custom['ePayment']['HartsPreschool'] && $payment_method == '' && isset($studentIdToPaymentMethod[$result[$i]['StudentID']])){
     		$payment_method = $studentIdToPaymentMethod[$result[$i]['StudentID']]['PaymentMethod'];
     	}
	 	$x .= "<td class=\"tabletext\">".$lpayment->getPaymentMethodSelection(array(), " name=\"PaymentMethod[".$payment_id."]\" ", $payment_method, 0, 0, $Lang['ePayment']['NA'])."</td>\n";  
	 	//$x .= "<td class=\"tabletext\">".$linterface->GET_TEXTBOX_NAME("ReceiptRemark[".$payment_id."]", "ReceiptRemark[".$payment_id."]", $result[$i]['ReceiptRemark'], '', array())."</td>";
	 	$x .= "<td class=\"tabletext\">".$linterface->GET_TEXTAREA("ReceiptRemark[".$payment_id."]", $result[$i]['ReceiptRemark'] , 40, 3, "", "", '', '', '', '')."</td>";
	 }
     //$x .= $hidden."\n";     
     $x .= "</tr>\n";
   
}

if($sys_custom['ePayment']['PaymentItemWithPOS'] && count($pos_records)>0){
	$pos_x .= '<tr>
				<td>
    			<table width="98%" border="0" cellpadding="5" cellspacing="0" align="center">
			    <tr class="tablebluetop">
					<td class="tabletoplink" width="20%">'.$Lang['Header']['Menu']['ePOS'].' '.$Lang['ePOS']['Item'].'</td>
					<td class="tabletoplink" width="20%">'.$Lang['Header']['Menu']['ePOS'].' '.$Lang['ePOS']['Category'].'</td>
					<td class="tabletoplink" width="10%">'.$i_Payment_Field_Amount.'</td>
					<td class="tabletoplink" width="10%">'.$Lang['ePOS']['UnitPrice'].'</td>
					<td class="tabletoplink" width="10%">'.$Lang['ePayment']['Quantity'].'</td>';
 		if(!$isKIS){
			if ($lpayment->isEWalletDirectPayEnabled() == false) {
				$pos_x .= '<td class="tabletoplink" width="10%">' . $i_Payment_Field_BalanceAfterPay . '</td>';
			}
		}
		if($sys_custom['ePayment']['HartsPreschool']){
			$pos_x .= '<td class="tabletoplink" width="20%">
						'.$Lang['ePayment']['PaymentTime'].'<br />
						'.$linterface->GET_TEXTBOX("MasterPosPaymentTime", "MasterPosPaymentTime", $now, $___OtherClass='', $___OtherPar=array('onchange'=>'this.value=formatDatetimeString(this.value);')).'
						<a href="javascript:void(0);" onclick="$(\'input[name*=PosPaymentTime]\').val($(\'#MasterPosPaymentTime\').val());"><img src="/images/'.$LAYOUT_SKIN.'/icon_assign.gif" height="12" border="0" width="12"></a>
					  </td>';
		}
		$pos_x .= '</tr>';
	if(isset($balanceAfter)){
		$balance_after = $balanceAfter[count($balanceAfter)-1];
	}else{
		$sql = "SELECT Balance FROM PAYMENT_ACCOUNT WHERE StudentID='$targetID'";
		$balance_record = $lpayment->returnVector($sql);
		$balance_after = $balance_record[0]+$AddValueAmount;
	}
	
	if(!isset($balance)){
		$balance = $balance_after;
	}
	
	for($i=0;$i<count($pos_records);$i++){
		
		$pos_item_id = $pos_records[$i]['ItemID'];
		$pos_item_name = $pos_records[$i]['ItemName'];
		$pos_category_name = $pos_records[$i]['CategoryName'];
		$pos_unit_price = $pos_records[$i]['UnitPrice'];
		$purchase_quantity = $posItemIdToQuantity[$pos_item_id];
		$pos_amount = $purchase_quantity * $pos_unit_price;
		$balance_after -= $pos_amount; 
		if ($balance_after < 0 && !$sys_custom['ePayment']['AllowNegativeBalance'])
	    {
	         $display_balance_after = $lpayment->getWebDisplayAmountFormat($balance_after);
	         $display_balance_after = "<font color=red>$balance_after</font>";
	         $Enough = false;
	    }
	    else
	    {
	     	$display_balance_after = $lpayment->getWebDisplayAmountFormat($balance_after);
	    }
		
		$hidden = '<input type="hidden" name="ItemID[]" value="'.$pos_item_id.'" /><input type="hidden" name="PurchaseQuantity['.$pos_item_id.']" value="'.$purchase_quantity.'" />';
		
		 $css = $i%2==0?"tablebluerow1 tabletext":"tablebluerow2 tabletext";
	     $pos_x .= "<tr class=\"$css\">\n";
	     $pos_x .= "<td class=\"tabletext\">$pos_item_name</td>\n";
	     $pos_x .= "<td class=\"tabletext\">$pos_category_name</td>\n";  
	     $pos_x .= "<td class=\"tabletext\">".$lpayment->getWebDisplayAmountFormat($pos_amount)."$hidden</td>\n";
	     $pos_x .= "<td class=\"tabletext\">".$lpayment->getWebDisplayAmountFormat($pos_unit_price)."</td>\n";
	     $pos_x .= "<td class=\"tabletext\">".$purchase_quantity."</td>\n";     
		 if(!$isKIS) {
			 if ($lpayment->isEWalletDirectPayEnabled() == false) {
				 $pos_x .= "<td class=\"tabletext\">" . $display_balance_after . "</td>\n";
			 }
		 }
		 if($sys_custom['ePayment']['HartsPreschool']){
	 		$pos_x .= "<td class\"tabletext\">".$linterface->GET_TEXTBOX("PosPaymentTime[".$pos_item_id."]", "PosPaymentTime[".$pos_item_id."]", $now, $___OtherClass='', $___OtherPar=array('onchange'=>'this.value=formatDatetimeString(this.value);'))."</td>\n";
	 	 }
	     $pos_x .= "</tr>\n";
	}
	
	$pos_x .= '</table>
		</td>
		</tr>';
}

if(!$Enough) {
	$warning_row = '<tr><td colspan="2">'.$linterface->Get_Warning_Message_Box('',$Lang['ePayment']['NotEnoughBalanceToPay']).'</td></tr>';
}

//debug_r($balanceAfter);
//debug_r($balanceAfterformed);

$linterface->LAYOUT_START();
?>
<link href="/templates/jquery/jquery.alerts.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="/templates/jquery/jquery.alerts.js"></script>

<script language="javascript">
function choose_receipt(obj){
	var paymentIdObj = document.getElementsByName('PaymentID[]');
	var paymentIdAry = [];
	var targetID = $('input#targetID').val();
	for(var i=0;i<paymentIdObj.length;i++){
		paymentIdAry.push(paymentIdObj[i].value);
	}
	//var printReceipt = false;
	$('#submit_btn').attr('disabled',true);
	jConfirm('<?=$Lang['ePayment']['NeedToPrintReceipt']?>','<?=$Lang['Btn']['Confirm']?>',function(printReceipt){
		var popup24, popup0;
		if(printReceipt){
			<?php if($sys_custom['ePayment']['ThermalPaperReceipt']){ ?>
			popup0 = window.open ('', 'intranet_popup0', 'resizable,scrollbars,status,top=10,left=10,width=400,height=400');
			<?php } ?>
			popup24 = window.open ('', 'intranet_popup24', 'menubar,resizable,scrollbars,status,top=40,left=40,width=800,height=700');
		}
		
		$.post(
	       	 'student_pay_item_update.php',
	       	 $('#form1').serialize(),
	       	 function(ReturnData){
	<?php if($sys_custom['ePayment']['PaymentItemWithPOS']){ ?>
				if(ReturnData != ''){
					if(printReceipt){
						popup24 = window.open ('view_receipt.php?RecordID[]=' + ReturnData, 'intranet_popup24', 'menubar,resizable,scrollbars,status,top=40,left=40,width=800,height=700');
					<?php if($sys_custom['ePayment']['ThermalPaperReceipt']){ ?>
						popup0 = window.open ('payment_item_thermal_paper_receipt.php?StudentID=' + targetID + '&PaymentID=' + paymentIdAry.join(','), 'intranet_popup0', 'resizable,scrollbars,status,top=10,left=10,width=400,height=400');
	       	 		<?php } ?>
	       	 		}
					//window.location.href='student_payment_item.php?targetClass=<?=$targetClass?>&targetID=<?=$targetID?>&Msg=<?=rawurlencode($Lang['ePayment']['PayProcessSuccess'])?>';
					submitBack('<?=rawurlencode($Lang['ePayment']['PayProcessSuccess'])?>');
				}else{
					//window.location.href='student_payment_item.php?targetClass=<?=$targetClass?>&targetID=<?=$targetID?>&Msg=<?=rawurlencode($Lang['ePayment']['PayProcessUnsuccess'])?>';
					submitBack('<?=rawurlencode($Lang['ePayment']['PayProcessUnsuccess'])?>');
				}
	<?php }else{ ?>       	 
	       	 	// pop receipt window
	       	 	if(ReturnData == '1'){
	       	 		if(printReceipt){
	       	 			//newWindow('student_print_receipt.php?StudentID=' + targetID + '&PaymentID=' + paymentIdAry.join(','), 24);
	       	 	<?php if($sys_custom['ePayment']['ThermalPaperReceipt']){ ?>
	       	 			//newWindow('payment_item_thermal_paper_receipt.php?StudentID=' + targetID + '&PaymentID=' + paymentIdAry.join(','), 0);
						popup0 = window.open ('payment_item_thermal_paper_receipt.php?StudentID=' + targetID + '&PaymentID=' + paymentIdAry.join(','), 'intranet_popup0', 'resizable,scrollbars,status,top=10,left=10,width=400,height=400');
	       	 	<?php } ?>
	       	 			popup24 = window.open ('student_print_receipt.php?StudentID=' + targetID + '&PaymentID=' + paymentIdAry.join(','), 'intranet_popup24', 'menubar,resizable,scrollbars,status,top=40,left=40,width=800,height=700');
	       	 		}
	       	 		//window.location.href='student_payment_item.php?targetClass=<?=$targetClass?>&targetID=<?=$targetID?>&Msg=<?=rawurlencode($Lang['ePayment']['PayProcessSuccess'])?>';
	       	 		submitBack('<?=rawurlencode($Lang['ePayment']['PayProcessSuccess'])?>');
	       	 	}else{
	       	 		//window.location.href='student_payment_item.php?targetClass=<?=$targetClass?>&targetID=<?=$targetID?>&Msg=<?=rawurlencode($Lang['ePayment']['PayProcessUnsuccess'])?>';
	       	 		submitBack('<?=rawurlencode($Lang['ePayment']['PayProcessUnsuccess'])?>');
	       	 	}
	<?php } ?>
	       	 }
	     );
	});
}

function submitBack(msg)
{
	var formObj = $('form#form1');
	formObj.attr('action','student_payment_item.php');
	formObj.append('<input type="hidden" name="Msg" value="'+msg+'" />');
	formObj.submit();
}

<?php if($sys_custom['ePayment']['HartsPreschool']){ ?>
function formatDatetimeString(str)
{
	var dt = new Date(str);
	var year = dt.getFullYear();
	var month = dt.getMonth()+1;
	var day = dt.getDate();
	var hour = dt.getHours();
	var min = dt.getMinutes();
	var sec = dt.getSeconds();
	var formatted = year + '-' + (month<10?'0'+month:month) + '-' + (day<10?'0'+day:day) + ' ' + (hour<10?'0'+hour:hour) + ':' + (min<10?'0'+min:min) + ':' + (sec<10?'0'+sec:sec);
	if(isNaN(year) || isNaN(month) || isNaN(day) || isNaN(hour) || isNaN(min) || isNaN(sec)){
		formatted = formatDatetimeString(new Date());
	}
	return formatted;
}
<?php } ?>
</script>

<br />
<form id="form1" name="form1"  method="post">
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
    <tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
		<td>
			<table width="98%" border="0" cellpadding="5" cellspacing="0" align="center">
			    <tr>
			        <td nowrap="nowrap" class="formfieldtitle tabletext" valign="top" style="width:20%;"><?php echo $Lang['ePayment']['Class']?>: </td><td class="tabletext" valign="top" nowrap style="width:80%;text-align:left;"><?php echo $lclass->getClassName($targetClass)?></td>
			    </tr>
			    <tr>
			        <td nowrap="nowrap" class="formfieldtitle tabletext" valign="top" style="width:20%;"><?php echo $Lang['ePayment']['Student']?>: </td><td class="tabletext" valign="top" nowrap style="width:80%;text-align:left;"><?php echo $student_name?></td>
			    </tr>
			   <?php if(!$isKIS) { ?> 
			    <tr>
			        <td nowrap="nowrap" class="formfieldtitle tabletext" valign="top" style="width:20%;"><?php echo $i_Payment_Field_CurrentBalance?>: </td><td class="tabletext" valign="top" nowrap style="width:80%;text-align:left;"><?php echo $lpayment->getWebDisplayAmountFormat($balance)?></td>
			    </tr>
			    <?php } ?>
			    <?php 
			    if(!$Enough){ 
			    	echo $warning_row;
			    } 
			    ?>
			</table>
		</td>
	</tr>
	<?php if(count($PaymentID)>0){ ?>
	<tr>
		<td>
    		<table width="98%" border="0" cellpadding="5" cellspacing="0" align="center">
			    <tr class="tablebluetop">
					<td class="tabletoplink" width="20%"><?= $i_Payment_Field_PaymentItem ?></td>
					<td class="tabletoplink" width="10%"><?= $i_Payment_Field_PaymentCategory ?></td>
					<td class="tabletoplink" width="10%"><?= $i_Payment_Field_Amount  ?></td>
					<td class="tabletoplink" width="10%"><?= $i_general_startdate ?></td>
					<td class="tabletoplink" width="10%"><?= $i_general_enddate ?></td>
				<?php if(!$isKIS){ ?>
                    <?php  if($lpayment->isEWalletDirectPayEnabled() == false) { ?>
					<td class="tabletoplink" width="10%"><?= $i_Payment_Field_BalanceAfterPay ?></td>
                    <?php } ?>
				<?php } ?>
				<?php if($sys_custom['ePayment']['HartsPreschool']){ ?>
					<td class="tabletoplink" width="10%">
						<?=$Lang['ePayment']['PaymentTime']?><br />
						<?=$linterface->GET_TEXTBOX("MasterPaymentTime", "MasterPaymentTime", $now, $___OtherClass='', $___OtherPar=array('onchange'=>'this.value=formatDatetimeString(this.value);'))?>
						<a href="javascript:void(0);" onclick="$('input[name*=PaymentTime]').val($('#MasterPaymentTime').val());"><img src="/images/<?=$LAYOUT_SKIN?>/icon_assign.gif" height="12" border="0" width="12"></a>
					</td>
				<?php } ?>
				<?php if($sys_custom['ePayment']['PaymentMethod']){ ?>
					<td class="tabletoplink" width="10%">
						<?= $Lang['ePayment']['PaymentMethod'] ?><br />
						<?=$lpayment->getPaymentMethodSelection(array(), " id=\"MasterPaymentMethod\" name=\"MasterPaymentMethod\" ", "", 0, 0, $Lang['ePayment']['NA'])?>
						<a href="javascript:void(0);" onclick="$('select[name*=PaymentMethod]').val($('#MasterPaymentMethod').val());"><img src="/images/<?=$LAYOUT_SKIN?>/icon_assign.gif" height="12" border="0" width="12"></a>
					</td>
					<td class="tabletoplink" width="10%">
						<?=$Lang['ePayment']['PaymentRemark']?>
					</td>
				<?php } ?>
				</tr>
				<?=$x?>
			</table>
		</td>
	</tr>
	<?php } ?>
	<?php if($sys_custom['ePayment']['PaymentItemWithPOS']){
		echo $pos_x;
	}
	?>
	<tr>
    	<td>
		    <table width="98%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	   </td>
	</tr>
	<tr>
	    <td align="center">
		<? if ($Enough) { 
			?>
			<?= $linterface->GET_ACTION_BTN($i_Payment_action_proceed_payment, "button", "choose_receipt(document.form1)","submit_btn") ?>
		<? }  ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "document.form1.action='student_payment_item.php';document.form1.submit();","cancel_btn") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="pageNo" value="<?php echo $pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $order; ?>" />
<input type="hidden" name="field" value="<?php echo $field; ?>" />
<input type="hidden" name="targetClass" value="<?=$targetClass?>" />
<input type="hidden" name="targetID" id="targetID" value="<?=$targetID?>" />
<?php
for($i=0;$i<count($addValueAry);$i++)
{
	echo '<input type="hidden" name="AddValueAmount[]" value="'.$addValueAry[$i]['AddValueAmount'].'" />'."\n";
	echo '<input type="hidden" name="AddValueRef[]" value="'.$addValueAry[$i]['AddValueRef'].'" />'."\n";
	echo '<input type="hidden" name="AddValueDatetime[]" value="'.$addValueAry[$i]['AddValueDatetime'].'" />'."\n";
}
?>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>