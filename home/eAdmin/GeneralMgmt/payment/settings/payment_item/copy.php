<?php
// Editing by 
/*
 * 2020-01-13 (Tommy): Fixed click submit button again and again will create multiple record 
 * 2018-01-22 (Carlos): [ip2.5.9.3.1] Fixed query counting on total students and total paid info without temp table.
 * 2015-03-05 (Carlos): [ip2.5.6.3.1] Created to prepare for copying payment items.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PaymentItemSettings";

$linterface = new interface_html();

if(!is_array($ItemID) && count($ItemID) == 0){
	intranet_closedb();
	header("Location: index.php?Msg=".urlencode($Msg));
	exit;
}

//$use_magic_quotes = get_magic_quotes_gpc();
$use_magic_quotes = $lpayment->isMagicQuotesOn();

// admin in charge name field
$Inchargenamefield = getNameFieldByLang("d.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$Inchargearchive_namefield = " IF(e.ChineseName IS NULL,e.EnglishName,e.ChineseName)";
	//$archive_namefield="c.ChineseName";
}else {
	//$archive_namefield ="c.EnglishName";
	$Inchargearchive_namefield = "IF(e.EnglishName IS NULL,e.ChineseName,e.EnglishName)";
}

$sql = "SELECT 
         	IF(a.NoticeID is null, a.Name, concat(f.Title,' - ', a.Name)) as ItemName,
         	b.Name, 
			IF(a.DefaultAmount IS NULL,'0.00',ROUND(a.DefaultAmount,2)) as Amount,
         	a.DisplayOrder, 
         	a.PayPriority, 
			CONCAT(SUM(IF(c.RecordStatus='1',1,0)),' (',COUNT(c.PaymentID),')') as CountInfo,
			IF(a.Description IS NULL OR a.Description='','&nbsp;',a.Description) as Description,
			IF((d.UserID IS NULL AND e.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$Inchargearchive_namefield,'</i>'), IF(d.UserID IS NULL AND e.UserID IS NULL,IF(a.ProcessingAdminUser IS NULL OR a.ProcessingAdminUser = '','&nbsp;',a.ProcessingAdminUser),$Inchargenamefield)) as DisplayAdminInCharge, 
            DATE_FORMAT(a.StartDate,'%Y-%m-%d') as StartDate,
            DATE_FORMAT(a.EndDate,'%Y-%m-%d') as EndDate,
          	a.ItemID 
         FROM
             PAYMENT_PAYMENT_ITEM as a 
             LEFT JOIN PAYMENT_PAYMENT_CATEGORY as b ON (a.CatID = b.CatID) 
             LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT as c ON c.ItemID = a.ItemID  
             LEFT JOIN INTRANET_USER as d on a.ProcessingAdminUser = d.UserID 
             LEFT JOIN INTRANET_ARCHIVE_USER as e on a.ProcessingAdminUser = e.UserID 
             left join INTRANET_NOTICE as f on f.NoticeID=a.NoticeID 
		WHERE a.ItemID IN ('".implode("','",$ItemID)."') 
		GROUP BY a.ItemID 
		ORDER BY a.DisplayOrder";
$items = $lpayment->returnResultSet($sql);
$items_count = count($items);

//debug_pr($sql);

$table .= '<table class="common_table_list_v30" align="center" width="96%" style="width:96%;">';
	$table.= '<thead>';
	$table.= '<tr>';
		$table .= '<th class="num_check">#</th>';
		$table .= '<th style="width:15%">'.$i_Payment_Field_PaymentItem.'</th>';
		$table .= '<th style="width:10%">'.$i_Payment_Field_PaymentCategory.'</th>';
		$table .= '<th style="width:7%">'.$i_Payment_Field_Amount.'</th>';
		$table .= '<th style="width:7%">'.$i_Payment_Field_DisplayOrder.'</th>';
		$table .= '<th style="width:7%">'.$i_Payment_Field_PayPriority.'</th>';
		$table .= '<th style="width:7%">'.$i_Payment_Field_PaidCount." (".$i_Payment_Field_TotalPaidCount.")".'</th>';
		$table .= '<th style="width:15%">'.$i_general_description.'</th>';
		$table .= '<th style="width:10%">'.$i_general_last_modified_by.'</th>';
		$table .= '<th style="width:10%">'.$i_general_startdate.'</th>';
		$table .= '<th style="width:10%">'.$i_general_enddate.'</th>';
		//$table .= '<th class="num_check"><input type="checkbox" name="checkmaster" onclick="(this.checked)?setChecked(1,this.form,\'PaymentID[]\'):setChecked(0,this.form,\'PaymentID[]\');updatePayAmount();"></th>';
	$table.= '</tr>';
	$table.= '</thead>';
$table.= '<tbody>';

for($i=0;$i<$items_count;$i++){
	$item_id = $items[$i]['ItemID'];
	
	$table .= "<tr>";
		$table .= "<td>".($i+1)."</td>";
		$table .= "<td>".$linterface->GET_TEXTBOX("ItemName".$item_id, "ItemName".$item_id, $items[$i]['ItemName'], '', array())."<input type=\"hidden\" name=\"ItemID[]\" value=\"".$items[$i]['ItemID']."\" /><div id=\"WarnItemName".$item_id."\" style=\"color:red;display:none;\"></div></td>";
		$table .= "<td>".$items[$i]['Name']."</td>";
		$table .= "<td nowrap>".'$'.$linterface->GET_TEXTBOX_NUMBER("Amount".$item_id, "Amount".$item_id, $items[$i]['Amount'], '', array())."<div id=\"WarnAmount".$item_id."\" style=\"color:red;display:none;\"></div></td>";
		$table .= "<td>".$linterface->GET_TEXTBOX_NUMBER("DisplayOrder".$item_id, "DisplayOrder".$item_id, $items[$i]['DisplayOrder'], '', array())."<div id=\"WarnDisplayOrder".$item_id."\" style=\"color:red;display:none;\"></div></td>";
		$table .= "<td>".$linterface->GET_TEXTBOX_NUMBER("PayPriority".$item_id, "PayPriority".$item_id, $items[$i]['PayPriority'], '', array())."<div id=\"WarnPayPriority".$item_id."\" style=\"color:red;display:none;\"></div></td>";
		$table .= "<td>".$items[$i]['CountInfo']."</td>";
		$table .= "<td>".$linterface->getTextArea("Description".$item_id, $items[$i]['Description'])."<div id=\"WarnDescription".$item_id."\" style=\"color:red;display:none;\"></div></td>";
		$table .= "<td>".$items[$i]['DisplayAdminInCharge']."</td>";
		$table .= "<td nowrap>".$linterface->GET_DATE_PICKER("StartDate".$item_id,$items[$i]['StartDate'])."<div id=\"WarnStartDate".$item_id."\" style=\"color:red;display:none;\"></div></td>";
		$table .= "<td nowrap>".$linterface->GET_DATE_PICKER("EndDate".$item_id,$items[$i]['EndDate'])."<div id=\"WarnEndDate".$item_id."\" style=\"color:red;display:none;\"></div></td>";
	$table .= "</tr>";
	
}


$table.= '</tbody>';
$table.= '</table>';


$TAGS_OBJ[] = array($i_Payment_Menu_Settings_PaymentItem, $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/",1);
$TAGS_OBJ[] = array($Lang['ePayment']['StudentPaymentItemSettings'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/student_payment_item.php",0);
if($sys_custom['ePayment']['PaymentItemChangeLog']){
	$TAGS_OBJ[] = array($Lang['ePayment']['PaymentItemChangeLog'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/change_log/",0);
}
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($Lang['Btn']['Copy'].'&nbsp;'.$i_Payment_Field_PaymentItem);

?>
<script type="text/javascript" language="JavaScript">
$(document).ready(function () {
    $("#submitBtn").click(function (event) {  
          event.preventDefault();
          var success = checkform(document.form1);
          if(success){
        	  document.form1.submit();
              $(this).attr('disabled', 'disabled');
          }
    });
});

function checkform(obj)
{
	var itemIdObjs = document.getElementsByName('ItemID[]');
	var isValid = true;
	
	for(var i=0;i<itemIdObjs.length;i++){
		var item_id = itemIdObjs[i].value;
		
		var item_name = $.trim(document.getElementById('ItemName'+item_id).value);
		var amount = $.trim(document.getElementById('Amount'+item_id).value);
		var display_order = $.trim(document.getElementById('DisplayOrder'+item_id).value);
		var pay_priority = $.trim(document.getElementById('PayPriority'+item_id).value);
		var start_date = $.trim(document.getElementById('StartDate'+item_id).value);
		var end_date = $.trim(document.getElementById('EndDate'+item_id).value);
		var description  = $.trim(document.getElementsByName('Description'+item_id)[0].value);
		
		if(item_name == ''){
			$('#WarnItemName'+item_id).html('<?=$Lang['ePayment']['Invalid']?>').show();
			idValid = false;
		}else{
			$('#WarnItemName'+item_id).hide();
		}
		
		var float_amount = parseFloat(amount,10);
		if(amount == '' || isNaN(float_amount) || float_amount < 0.0)
		{
			$('#WarnAmount'+item_id).html('<?=$Lang['ePayment']['Invalid']?>').show();
			isValid = false;
		}else{
			$('#WarnAmount'+item_id).hide();
		}
		
		var int_order = parseInt(display_order,10);
		if(display_order == '' || isNaN(int_order)){
			$('#WarnDisplayOrder'+item_id).html('<?=$Lang['ePayment']['Invalid']?>').show();
			isValid = false;
		}else{
			$('#WarnDisplayOrder'+item_id).hide();
		}
		
		var int_priority = parseInt(pay_priority,10);
		if(pay_priority == '' || isNaN(int_priority)){
			$('#WarnPayPriority'+item_id).html('<?=$Lang['ePayment']['Invalid']?>').show();
			isValid = false;
		}else{
			$('#WarnPayPriority'+item_id).hide();
		}
		
		if(start_date == '' || !start_date.match(/^\d\d\d\d-\d\d-\d\d$/gi)){
			$('#WarnStartDate'+item_id).html('<?=$Lang['ePayment']['Invalid']?>').show();
			isValid = false;
		}else{
			$('#WarnStartDate'+item_id).hide();
		}
		
		if(end_date == '' || !end_date.match(/^\d\d\d\d-\d\d-\d\d$/gi)){
			$('#WarnEndDate'+item_id).html('<?=$Lang['ePayment']['Invalid']?>').show();
			isValid = false;
		}else{
			$('#WarnEndDate'+item_id).hide();
		}
	}
	if(!isValid) return false;

	return isValid;
}

function goBack()
{
	var formObj = document.getElementById('form1');
	formObj.action = 'index.php';
	formObj.submit();
}
</script>
<form id="form1" name="form1" action="copy_update.php" method="post" onsubmit="return false;">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
    <tr>
       <td>
           <?php
           echo $linterface->Get_Warning_Message_Box($Lang['General']['Remark'], $Lang['ePayment']['PaymentCopyWarning']);
           ?>
       </td>
    </tr>
	<tr>
		<td><?=$table?></td>
	</tr>
	<tr>
    	<td>
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<!--
		    	<tr>
					<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
				</tr>
				-->
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
    <tr>
	    <td align="center">
			<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "", "submitBtn") ?>
			<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "goBack();","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
<input type="hidden" id="FromDate" name="FromDate" value="<?=$FromDate?>" />
<input type="hidden" id="ToDate" name="ToDate" value="<?=$ToDate?>" />
<input type="hidden" id="itemStatus" name="itemStatus" value="<?=$itemStatus?>" />
<input type="hidden" id="itemPaidStatus" name="itemPaidStatus" value="<?=$itemPaidStatus?>" />
<input type="hidden" id="CatID" name="CatID" value="<?=$CatID?>" />
<input type="hidden" id="keyword" name="keyword" value="<?=str_replace(array('"'),array('\"'),$use_magic_quotes?stripslashes($keyword):$keyword)?>" />
<input type="hidden" id="field" name="field" value="<?=$field?>" />
<input type="hidden" id="order" name="order" value="<?=$order?>" />
<input type="hidden" id="numPerPage" name="numPerPage" value="<?=$numPerPage?>" />
<input type="hidden" id="pageNo" name="pageNo" value="<?=$pageNo?>" />
</form>
<?php
//debug_r($sql);
$linterface->LAYOUT_STOP();
intranet_closedb();
?>