<?php
// Editing by 
/*
 * 2020-03-03 (Ray):   Added Subsidy
 * 2015-10-28 (Carlos): [ip2.5.6.12.1] Allow to add staff users to payment item.
 * 2015-08-20 (Carlos): [ip2.5.6.10.1] Changed to use UserID as record key.
 * 2014-08-19 (Carlos): $sys_custom['ePayment']['PaymentItemChangeLog'] - Added change log
 * 2014-03-24 (Carlos): $sys_custom['ePayment']['PaymentMethod'] - added [Payment Method]
 * 2013-11-26 (Carlos): Added [Remark] import field
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();

if($sys_custom['ePayment']['PaymentItemChangeLog']){
	$logDeleteUserID = array();
	$logEditUserID = array();
	$logAddUserID = array();	
}

if($confirm==1){
	$li = new libdb();
	
	$li->Start_Trans();
	$sql = "SELECT UserID,Amount,Remark ".($sys_custom['ePayment']['PaymentMethod']?",PaymentMethod":"").",SubsidyUnitID,SubsidyAmount FROM TEMP_PAYMENT_ITEM_IMPORT";
	$data = $li->returnArray($sql,7);
  /*$sql = "LOCK TABLES
               INTRANET_USER READ,
               TEMP_PAYMENT_ITEM_IMPORT WRITE
               ,PAYMENT_PAYMENT_ITEMSTUDENT WRITE
               ";
  $li->db_db_query($sql);*/
  /*	
  # Get Existing Students
  $sql = "SELECT ClassName, ClassNumber, UserID
          FROM INTRANET_USER
          WHERE RecordType = 2 AND RecordStatus = 1
                AND ClassName != '' AND ClassNumber != ''
          ORDER BY ClassName, ClassNumber";
  $result = $li->returnArray($sql,3);
  $students = array();
  $userIdToStudent = array();
  for ($i=0; $i<sizeof($result); $i++)
  {
       list($class,$classnum,$uid) = $result[$i];
       $students[$class][$classnum] = $uid;
       $userIdToStudent[$uid] = $result[$i];
  }
  */

  # Get Existing non-paid StudentID
  $sql = "SELECT StudentID FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID = '$ItemID' AND RecordStatus = 0";
  $unpaid = $li->returnVector($sql);

  # Get Existing paid StudentID
  $sql = "SELECT StudentID FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID = '$ItemID' AND RecordStatus = 1";
  $paid = $li->returnVector($sql);
  # Update the amount
  for ($i=0; $i<sizeof($data); $i++)
  {
  	  if($sys_custom['ePayment']['PaymentMethod']){
  	  	list($user_id, $amount, $remark, $payment_method,$subsidy_unitid, $subsidy_amount) = $data[$i];
  	  }else{
       list($user_id, $amount, $remark,$subsidy_unitid, $subsidy_amount) = $data[$i];
  	  }
       //$targetUID = $students[$class][$classnum];
       $targetUID = $user_id;
       
       if ($targetUID == "") continue;
       if (in_array($targetUID,$paid)) continue;    # Paid records remain unchanged

	  $subsidy_update = '';
	  $subsidy_field = '';
	  $subsidy_value = '';
	  if($subsidy_unitid != '') {
		  $subsidy_update = ",SubsidyAmount='".$li->Get_Safe_Sql_Query($subsidy_amount)."'";
		  $subsidy_field = ',SubsidyAmount';
		  $subsidy_value = ",'".$li->Get_Safe_Sql_Query($subsidy_amount)."'";
	  }

       if (in_array($targetUID, $unpaid))       # Existing record
       {
           if ($amount == "")
           {
               $sql = "DELETE FROM PAYMENT_PAYMENT_ITEMSTUDENT
                       WHERE ItemID = '$ItemID' AND RecordStatus = 0 AND StudentID = '$targetUID'";
                       
               if($sys_custom['ePayment']['PaymentItemChangeLog']){
               		$logDeleteUserID[] = $targetUID;
               }
           }
           else
           {
               $sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET Amount = '$amount', Remark='".$li->Get_Safe_Sql_Query($remark)."', ";
               if($sys_custom['ePayment']['PaymentMethod']){
               	$sql .= " PaymentMethod='$payment_method', ";
               }
			   $sql.=" RecordType = 1, ProcessingTerminalUser=NULL,ProcessingTerminalIP=NULL, ProcessingAdminUser='$PHP_AUTH_USER', DateModified=NOW()
                      $subsidy_update WHERE ItemID = '$ItemID' AND RecordStatus = 0 AND StudentID = '$targetUID'";
               
               if($sys_custom['ePayment']['PaymentItemChangeLog']){
               		$logEditUserID[] = $targetUID;
               }
           }
       }
       else            # New record
       {
           $sql = "INSERT INTO PAYMENT_PAYMENT_ITEMSTUDENT
                          (ItemID, StudentID, Amount, Remark, ".($sys_custom['ePayment']['PaymentMethod']?" PaymentMethod, ":"")." RecordType, RecordStatus, ProcessingTerminalUser, ProcessingAdminUser, ProcessingTerminalIP, DateInput, DateModified $subsidy_field)
                   VALUES
                          ('$ItemID','$targetUID','$amount','".$li->Get_Safe_Sql_Query($remark)."',".($sys_custom['ePayment']['PaymentMethod']?"'$payment_method',":"")." 1,0,NULL,'$PHP_AUTH_USER',NULL,now(),now() $subsidy_value)";
                          
           if($sys_custom['ePayment']['PaymentItemChangeLog']){
           		$logAddUserID[] = $targetUID;
           }
       }
       $Result['DataImport'.$i] = $li->db_db_query($sql);

	  $sql = "SELECT PaymentID FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID='$ItemID' AND StudentID='$targetUID'";
	  $temp = $li->returnVector($sql);
	  if($temp) {
		  $payment_id = $temp[0];
		  $sql = "DELETE FROM PAYMENT_PAYMENT_ITEM_SUBSIDY WHERE PaymentID='$payment_id' AND UserID='$targetUID'";
		  $Result['DeleteSubsidy_PaymentID_' . $payment_id . '_StudentID_' . $targetUID] = $li->db_db_query($sql);

		  if($subsidy_unitid != '') {
			  $sql_insert_subsidy = "INSERT INTO PAYMENT_PAYMENT_ITEM_SUBSIDY (PaymentID,UserID,SubsidyUnitID,SubsidyAmount,SubsidyPICUserID,DateInput,DateModified,InputBy,ModifyBy) VALUES ";
			  $sql_insert_subsidy .= "('$payment_id','$targetUID','" . $subsidy_unitid . "','" . $subsidy_amount . "','" . $_SESSION['UserID'] . "',NOW(),NOW(),'" . $_SESSION['UserID'] . "','" . $_SESSION['UserID'] . "')";
			  $Result['InsertSubsidy_PaymentID_' . $payment_id . '_StudentID_' . $targetUID] = $li->db_db_query($sql_insert_subsidy);
		  }
	  }

  }
  $sql = "DELETE FROM TEMP_PAYMENT_ITEM_IMPORT";
  $Result['ClearTempTable'] = $li->db_db_query($sql);
  /*$sql = "UNLOCK TABLES";
  $li->db_db_query($sql);*/
	if (in_array(false,$Result)) {
		$Msg = $Lang['ePayment']['DataImportUnsuccess'];
		$li->RollBack_Trans();
	}
	else {
		$Msg = $Lang['ePayment']['DataImportSuccess'];
		$li->Commit_Trans();
		
		if($sys_custom['ePayment']['PaymentItemChangeLog']){
			$logAddUserID_count = count($logAddUserID);
			if($logAddUserID_count > 0){
				$logDisplayDetail = 'Import<br />';
				$logHiddenDetail = 'Import<br />';
				
				$name_field = getNameFieldWithClassNumberByLang("u.");
				$sql = "SELECT ppi.Name as ItemName, a.PaymentID, $name_field as StudentName, a.StudentID, a.Amount, c.Balance 
			            FROM PAYMENT_PAYMENT_ITEMSTUDENT as a 
							INNER JOIN PAYMENT_PAYMENT_ITEM as ppi ON ppi.ItemID=a.ItemID 
							LEFT JOIN INTRANET_USER as u ON u.UserID=a.StudentID 
			                LEFT JOIN PAYMENT_ACCOUNT as c ON a.StudentID = c.StudentID 
			            WHERE ppi.ItemID='$ItemID' AND a.ItemID='$ItemID' AND a.StudentID IN ('".implode("','",$logAddUserID)."') ";
			    $logRecords = $lpayment->returnResultSet($sql);
				
				for($i=0;$i<count($logRecords);$i++){
					$logDisplayDetail .= 'Item: '.$logRecords[$i]['ItemName'].' User: '.$logRecords[$i]['StudentName'].' Amount: $'.$logRecords[$i]['Amount'] .'<br />';
					$delim = '';
					foreach($logRecords[$i] as $key => $val){
						$logHiddenDetail .= $delim.$key.': '.$val;
						$delim = ', ';
					}
					$logHiddenDetail .= '<br />';
				}
				
				$logType = $lpayment->getPaymentItemChangeLogType("AddStudent");
				$lpayment->logPaymentItemChange($logType, $logDisplayDetail, $logHiddenDetail);	
			}
			
			$logEditUserID_count = count($logEditUserID);
			if($logEditUserID_count > 0){
				$logDisplayDetail = 'Import<br />';
				$logHiddenDetail = 'Import<br />';
				$name_field = getNameFieldWithClassNumberByLang("u.");
				$sql = "SELECT ppi.Name as ItemName, a.PaymentID, $name_field as StudentName, a.StudentID, a.Amount, a.SubsidyAmount, c.Balance, a.Remark, a.RecordType, a.ProcessingAdminUser, a.DateModified 
				        FROM PAYMENT_PAYMENT_ITEMSTUDENT as a 
							INNER JOIN PAYMENT_PAYMENT_ITEM as ppi ON ppi.ItemID=a.ItemID 
							LEFT JOIN INTRANET_USER as u ON u.UserID=a.StudentID 
				            LEFT JOIN PAYMENT_ACCOUNT as c ON a.StudentID = c.StudentID 
				        WHERE ppi.ItemID='$ItemID' AND a.ItemID='$ItemID' AND a.StudentID IN ('".implode("','",$logEditUserID)."') ";
				$logRecords = $lpayment->returnResultSet($sql);	
				
				for($i=0;$i<count($logRecords);$i++){
					$logDisplayDetail .= 'Item: '.$logRecords[$i]['ItemName'].' User: '.$logRecords[$i]['StudentName'].' Amount: $'.$logRecords[$i]['Amount'] .'<br />';
					$delim = '';
					foreach($logRecords[$i] as $key => $val){
						$logHiddenDetail .= $delim.$key.': '.$val;
						$delim = ', ';
					}
					$logHiddenDetail .= '<br />';
				}
				
				$logType = $lpayment->getPaymentItemChangeLogType("EditStudent");
				$lpayment->logPaymentItemChange($logType, $logDisplayDetail, $logHiddenDetail);	
			}
			
			$logDeleteUserID_count = count($logDeleteUserID);
			if($logDeleteUserID_count > 0){
				$logDisplayDetail = 'Import<br />';
				$logHiddenDetail = 'Import<br />';
				
				$sql = "SELECT * FROM PAYMENT_PAYMENT_ITEM WHERE ItemID='$ItemID'";
				$logItemRecord = $lpayment->returnResultSet($sql);
				$name_field = getNameFieldWithClassNumberByLang("u.");
				$sql = "SELECT u.UserID,$name_field as StudentName FROM INTRANET_USER as u WHERE u.UserID IN (".implode(",",$logDeleteUserID).")";
				$logRecords = $lpayment->returnResultSet($sql);
				
				$logDisplayDetail .= 'Item: '.$logItemRecord[0]['Name'].'<br />';
				$logHiddenDetail .= $logItemRecord[0]['ItemID'].' | '.$logItemRecord[0]['Name'].'<br />';
				
				for($i=0;$i<count($logRecords);$i++){
					$logDisplayDetail .= 'Student: '.$logRecords[$i]['StudentName'].'<br />';
					$logHiddenDetail .= $logRecords[$i]['UserID'].' | '.$logRecords[$i]['StudentName'].'<br />';
				}
				
				$logType = $lpayment->getPaymentItemChangeLogType("DeleteStudent");
				$lpayment->logPaymentItemChange($logType, $logDisplayDetail, $logHiddenDetail);	
			}
		}
	}
	header ("Location: list.php?ItemID=$ItemID&Msg=".urlencode($Msg)."&clear=1");
}
else 
	header("Location: import.php?ItemID=$ItemID&clear=1");

intranet_closedb();
?>
