<?php
#using: 
/*
 * 2017-02-09 (Carlos): $sys_custom['ePayment']['HartsPreschool'] - handle payment time.
 * 2015-11-23 (Carlos): $sys_custom['ePayment']['PaymentItemWithPOS'] - added processing logic of ePOS items and generate receipt logic.
 * 2015-10-13 (Carlos): $sys_custom['ePayment']['AllowNegativeBalance'] - let not enough balance accounts to pay. 
 * 2015-04-30 (Carlos): Improved to allow add multiple add value items. 
 * 2015-02-18 (Carlos): Added [ReceiptRemark]
 * 2014-03-24 (Carlos): $sys_custom['ePayment']['PaymentMethod'] - Added [Payment Method]
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");


intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$use_magic_quotes = get_magic_quotes_gpc();

$lpayment = new libpayment();
//$lclass = new libclass();
//$lu = new libuser("$targetID");
//$student_name=$lu->UserNameLang();
//$class_name=$lclass->getClassName($targetClass);

$addValueResult = array();
$resultAry = array();
$resultCode = 0;
// Add money to balance
if(is_array($AddValueAmount) && count($AddValueAmount) > 0 && $targetID != '' && $targetID > 0){
	
	//$TransactionTime = $AddValueDate." ".$AddValueTime_hour.":".$AddValueTime_min.":".$AddValueTime_sec;
	//$RefCode = trim(rawurldecode($AddValueRef));
	
	$sql = "LOCK TABLES
                 PAYMENT_CREDIT_TRANSACTION WRITE
                 ,INTRANET_USER as b READ
                 ,PAYMENT_ACCOUNT as WRITE
                 ,PAYMENT_OVERALL_TRANSACTION_LOG as WRITE";
    $lpayment->db_db_query($sql);
	
	for($j=0;$j<count($AddValueAmount);$j++)
	{
		$CreditAmount = $AddValueAmount[$j];
		$TransactionTime = $AddValueDatetime[$j];
		$RefCode = trim(rawurldecode($AddValueRef[$j]));
		
		$sql = "INSERT INTO PAYMENT_CREDIT_TRANSACTION (
					StudentID, 
					Amount, 
					RecordType, 
					RecordStatus, 
					TransactionTime, 
					RefCode, 
					AdminInCharge,
					DateInput) 
				VALUES (
					'$targetID', 
					'$CreditAmount', 
					2, 
					1, 
					'$TransactionTime', 
					'".$lpayment->Get_Safe_Sql_Query($RefCode)."', 
					'".$_SESSION['UserID']."',
					NOW())";
		$addValueResult['AddCreditTransaction_'.$j] = $lpayment->db_db_query($sql);
		
		$sql = "UPDATE PAYMENT_CREDIT_TRANSACTION SET
		               RefCode = CONCAT('CSH',TransactionID) 
		        WHERE RecordStatus = 1 AND RecordType = 2 AND (RefCode IS NULL OR RefCode = '')";
		$addValueResult['UpdateRefCodeIfNotInput_'.$j] = $lpayment->db_db_query($sql);
			
		$sql = "SELECT TransactionID, StudentID, Amount, DATE_FORMAT(TransactionTime,'%Y-%m-%d %H:%i'),RefCode
		        FROM PAYMENT_CREDIT_TRANSACTION WHERE RecordStatus = 1 AND RecordType = 2 ORDER BY TransactionTime";
		$transactions = $lpayment->returnArray($sql);
		
		$values = "";
		$delim = "";
		for ($i=0; $i<sizeof($transactions); $i++)
		{
		     list($tid, $sid, $amount, $tran_time, $refCode) = $transactions[$i];
		     $sql = "SELECT Balance FROM PAYMENT_ACCOUNT WHERE StudentID = '$sid'";
		     $temp = $lpayment->returnVector($sql);
		     if(count($temp)==0){
		     	$sql_create_account = "INSERT INTO PAYMENT_ACCOUNT (StudentID,Balance) VALUES ('$sid',0)";
		     	$lpayment->db_db_query($sql_create_account);
		     }
		     
		     $sql = "UPDATE PAYMENT_ACCOUNT SET Balance = Balance + $amount, LastUpdateByAdmin = '".$_SESSION['UserID']."', LastUpdateByTerminal= '', LastUpdated=now() WHERE StudentID = '$sid'";
		     $addValueResult['UpdateAccountBalance_'.$j.'_'.$i] = $lpayment->db_db_query($sql);
		     $balanceAfter = $temp[0]+$amount;
		     # Change transaction time on overall trans to now()
		     #$values .= "$delim ('$sid',1,'$amount','$tid','$balanceAfter','$tran_time','$i_Payment_TransactionDetailCashDeposit','$refCode')";
		     $values .= "$delim ('$sid',1,'$amount','$tid','$balanceAfter',now(),'$i_Payment_TransactionDetailCashDeposit','". $lpayment->Get_Safe_Sql_Query($refCode) ."')";
		     $delim = ",";
		}
		$sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
		               (StudentID,TransactionType,Amount,RelatedTransactionID,BalanceAfter,
		               TransactionTime,Details,RefCode) VALUES $values";
		$addValueResult['InsertOverallTransactionLogs_'.$j] = $lpayment->db_db_query($sql);
		//if($addValueResult['InsertOverallTransactionLogs_'.$j]){
		//	$resultAry[] = $lpayment->db_insert_id();
		//}
		
		# Update in progress to finished status
		$sql = "UPDATE PAYMENT_CREDIT_TRANSACTION SET
		           RecordStatus = 0,
		           DateInput = now() WHERE RecordStatus = 1 AND RecordType = 2 ";
		$addValueResult['SetTransactionToFinishStatus_'.$j] = $lpayment->db_db_query($sql);
	
	}
	
	$sql = "UNLOCK TABLES";
    $lpayment->db_db_query($sql);
}

if(is_array($PaymentID))
	$list = "'".implode("','",$PaymentID)."'";
//echo $list;	
if($list!=""){
	
	$sql = "SELECT a.StudentID, a.Amount, a.PaymentID, a.ItemID 
	        FROM PAYMENT_PAYMENT_ITEMSTUDENT as a 
			INNER JOIN PAYMENT_PAYMENT_ITEM as b ON b.ItemID=a.ItemID 
	        WHERE a.PaymentID IN ($list) and a.paidtime is null 
          		and (a.RecordStatus=0 OR a.RecordStatus IS NULL) 
			ORDER BY b.DisplayOrder,b.PayPriority";
	$result = $lpayment->returnArray($sql);
	$balanceAfter=null;
	//$resultAry = array();
	
	$lpayment->Start_Trans();
	
	for ($i=0; $i<sizeof($result); $i++)
	{
	     list ($StudentID, $amount, $paymentID, $itemID) = $result[$i];
	     
	     if($sys_custom['ePayment']['HartsPreschool'] && isset($PaymentTime[$paymentID]) && $PaymentTime[$paymentID]!=''){
	     	$resultAry[] = $lpayment->Paid_Payment_Item($itemID,$StudentID,$paymentID,$sys_custom['ePayment']['AllowNegativeBalance'],true,$PaymentTime[$paymentID]);
	     }else{
	     	$resultAry[] = $lpayment->Paid_Payment_Item($itemID,$StudentID,$paymentID,$sys_custom['ePayment']['AllowNegativeBalance'],true);
	     }  
	     if($sys_custom['ePayment']['PaymentMethod'] && $resultAry[$i]){
	     	if($use_magic_quotes){
	     		$receipt_remark = trim(htmlspecialchars_decode(stripslashes($ReceiptRemark[$paymentID]),ENT_QUOTES));
	     	}else{
	     		$receipt_remark = trim(htmlspecialchars_decode($ReceiptRemark[$paymentID],ENT_QUOTES));
	     	}
	     	$sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET PaymentMethod='".$PaymentMethod[$paymentID]."',ReceiptRemark='".$lpayment->Get_Safe_Sql_Query($receipt_remark)."' WHERE PaymentID='$paymentID'";
	     	$lpayment->db_db_query($sql);
	     }
	}
	
	if (in_array(false,$resultAry)) {
		$lpayment->RollBack_Trans();
		$Msg = $Lang['ePayment']['PayProcessUnsuccess'];
		$resultCode = 0;
	}
	else {
		$lpayment->Commit_Trans();
		$Msg = $Lang['ePayment']['PayProcessSuccess'];
		$resultCode = 1;
	}
}

if($sys_custom['ePayment']['PaymentItemWithPOS']){
	include_once($PATH_WRT_ROOT."includes/libpayment_ui.php");
	$lpayment_ui = new libpayment_ui();
	$pos_transaction_log_id = array();
	if(count($ItemID)>0)
	{
		include_once($PATH_WRT_ROOT."includes/libpos.php");
		
		$lpos = new libpos();
		//$pos_total = 0;
		$TransactionDetail = array();
		$quantityAry = $_POST['PurchaseQuantity']; // ItemID as key
		
		$sql = "SELECT 
					p.ItemID,
					p.UnitPrice,
					p.ItemCount 
				FROM POS_ITEM as p 
				LEFT JOIN POS_ITEM_CATEGORY as c ON c.CategoryID=p.CategoryID 
				WHERE p.ItemID IN ('".(implode("','",$ItemID))."') 
				ORDER BY c.Sequence,p.Sequence";
		$pos_records = $lpos->returnResultSet($sql);
		for($i=0;$i<count($pos_records);$i++){
			$item_id = $pos_records[$i]['ItemID'];
			$unit_price = $pos_records[$i]['UnitPrice'];
			$item_count = $pos_records[$i]['ItemCount'];
			
			if(isset($quantityAry[$item_id]) && $quantityAry[$item_id] <= $item_count){
				$TransactionDetail = array();
				$TransactionDetail[] = array("ItemID"=>$item_id,"Quantity"=>$quantityAry[$item_id],"UnitPrice"=>$unit_price);
				if($sys_custom['ePayment']['HartsPreschool'] && isset($PosPaymentTime[$item_id]) && $PosPaymentTime[$item_id]!=''){
					$pos_transaction_log_id = $lpos->Process_POS_TRANSACTION('',$quantityAry[$item_id]*$unit_price,'ePayment',$TransactionDetail,0,$targetID,true,$PosPaymentTime[$item_id]);
				}else{
					$pos_transaction_log_id = $lpos->Process_POS_TRANSACTION('',$quantityAry[$item_id]*$unit_price,'ePayment',$TransactionDetail,0,$targetID,true);
				}
				if($pos_transaction_log_id){
					$resultAry[] = $pos_transaction_log_id;
				}
				//$pos_total += $unit_price * $quantityAry[$item_id];
			}
		}
		
		//$pos_transaction_log_id = $lpos->Process_POS_TRANSACTION('',$pos_total,'',$TransactionDetail,0,$targetID,true);
		//if($pos_transaction_log_id){
		//	$resultAry[] = $pos_transaction_log_id;
		//}
		//$resultCode = $resultCode && $pos_transaction_log_id;
	}
	
	// generate receipt and save it, return the receipt record id
	$receipt_record_id = $lpayment_ui->generateReceiptFromTransactions($targetID, $resultAry);
	echo $receipt_record_id;
	
	intranet_closedb();
	exit;
}

echo $resultCode;

intranet_closedb();
?>
