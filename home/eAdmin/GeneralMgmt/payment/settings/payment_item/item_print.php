<?php
// editing by 
########################################### Change Log ###################################################
# 2020-10-19 (Ray)   : Add print button
# 2019-07-25 (Ray)   : Add payment notice filter
# 2017-05-18 (Carlos): [ip2.5.8.7.1] Search keyword applies to payment notice title.
# 2016-11-09 (Carlos): [ip2.5.8.1.1] Changed to use real TEMPORARY TABLE to avoid different users to manipulate the same temp table at the same time. 
# 									  Grouping and counting records approach has performance problem when payment student records size is huge.
# 2016-10-11 (Carlos): [ip2.5.7.10.1] Modified to count paid/total info by grouping records. 
# 						Do not use temp table which would cause duplication issue when more than one user load the page at the same time. 
# 2015-02-09 (Carlos): $sys_custom['ePayment']['PaymentItemDateRange'] - date range only apply to EndDate
# 2014-07-16 (Carlos): added table field [Amount] which is the default amount of an payment item, not the true amount students would pay
# 2011-11-18 by YatWoon: Improved: display notice title in the payment item [Case#2011-1115-1426-21072]
# 2010-02-10 by Carlos: Add border line to table
##########################################################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
//include_once($PATH_WRT_ROOT."templates/fileheader.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$linterface = new interface_html();

if (!isset($order)) $order = 1;
if (!isset($field)) $field = 3;
$order = ($order == 1) ? 1 : 0;

if ($CatID != "")
{
    $conds = " AND a.CatID = '$CatID'";
}

if ($itemPaidStatus <> ""){
	if ($itemPaidStatus == 0){
		$conds .= " AND (c.countPaid <> c.TotalCount OR c.TotalCount = 0) ";
		//$having_conds = " HAVING (COUNT(DISTINCT c2.PaymentID)=0 OR COUNT(DISTINCT c1.PaymentID)<COUNT(DISTINCT c2.PaymentID)) ";
	}else if ($itemPaidStatus == 1){
		$conds .= " AND c.countPaid = c.TotalCount AND c.TotalCount <> 0 ";
		//$having_conds = " HAVING (COUNT(DISTINCT c1.PaymentID)=COUNT(DISTINCT c2.PaymentID) AND COUNT(DISTINCT c2.PaymentID)<>0) ";
	}else if($itemPaidStatus == 2 ){ # Not Yet Started
		$today = date('Y-m-d');
		$conds .=" AND DATE_FORMAT(a.StartDate,'%Y-%m-%d') > '$today' ";
	}
}

$itemStatus=$itemStatus==""?0:$itemStatus;

	if($itemStatus==1){
		$conds2 =" AND a.RecordStatus=1 ";
	}else{
		$conds2 =" AND a.RecordStatus=0 ";
	}

$cond3 = "";
if($PaymentNoticeID != "") {
	$cond3 = " AND a.NoticeID='".$PaymentNoticeID."' ";
}

	# date range
	$today_ts = strtotime(date('Y-m-d'));
	if($FromDate=="")
		$FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
	if($ToDate=="")
		$ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

#$datetype=$datetype!=2?1:2;
#if($datetype==2)
#	$date_conds =" AND DATE_FORMAT(a.EndDate,'%Y-%m-%d') BETWEEN '$FromDate' AND '$ToDate' ";
#else 
if($sys_custom['ePayment']['PaymentItemDateRange']){
	$date_conds = " AND (a.EndDate between '$FromDate' AND '$ToDate') ";
}else{
	$date_conds =" AND (
									(a.StartDate between '$FromDate' AND '$ToDate' 
									 or 
									 a.EndDate between '$FromDate' AND '$ToDate' )) ";
}

//+++ get payment info of each payment item
// - get the payment id of all requested payment item
$ldb = new libdb();

//$temp_table_sql = "DROP TABLE TEMP_PAYMENT_ITEM_SUMMARY";
//$ldb->db_db_query($temp_table_sql);

$temp_table_sql = "CREATE TEMPORARY TABLE TEMP_PAYMENT_ITEM_SUMMARY (
				pid int,
				sumUnpaid varchar(255),
				sumPaid varchar(255),
				TotalPayment varchar(255),
				countUnpaid int,
				countPaid int,
				TotalCount int
			)";
$ldb->db_db_query($temp_table_sql);

$pValue="";

$all_pid_sql  = "SELECT a.ItemID 
				 FROM
					 PAYMENT_PAYMENT_ITEM as a 
					LEFT JOIN PAYMENT_PAYMENT_CATEGORY as b ON a.CatID = b.CatID 
					LEFT JOIN INTRANET_NOTICE as f on f.NoticeID=a.NoticeID 
				 WHERE
					  (a.Name LIKE '%$keyword%' OR
					   a.Description LIKE '%$keyword%' OR 
						f.Title LIKE '%$keyword%' 
					  )
                ";

$allPaymentID = $ldb->returnVector($all_pid_sql);
// - get the summay of payment :
// - Sum of unpaid amount, Sum of paid amount, Count of unpaid student, Count of paid student
if (sizeOf($allPaymentID)>0)
	for ($i=0; $i<sizeOf($allPaymentID); $i++){
		$allPaymentIDSummary[$i] = $lpayment->returnPaymentPaidSummary($allPaymentID[$i]);

		if ($i>0) $pValue .= ","; 
		$pValue .=
							"(".$allPaymentID[$i].",'"
							.number_format($allPaymentIDSummary[$i][0],1,".",",")."','"
							.number_format($allPaymentIDSummary[$i][1],1,".",",")."','"
							.number_format(($allPaymentIDSummary[$i][0]+$allPaymentIDSummary[$i][1]),1,".",",")."',"
							.$allPaymentIDSummary[$i][2].",".$allPaymentIDSummary[$i][3]."," 
							.($allPaymentIDSummary[$i][2]+$allPaymentIDSummary[$i][3]).")";
	}

$insert_temp_sql = "INSERT INTO TEMP_PAYMENT_ITEM_SUMMARY ( pid, sumUnpaid, sumPaid, TotalPayment, countUnpaid, countPaid,TotalCount ) VALUES $pValue";
$ldb->db_db_query($insert_temp_sql);
//+++ end of getting payment

// admin in charge name field
$Inchargenamefield = getNameFieldByLang("d.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$Inchargearchive_namefield = " IF(e.ChineseName IS NULL,e.EnglishName,e.ChineseName)";
	//$archive_namefield="c.ChineseName";
}else {
	//$archive_namefield ="c.EnglishName";
	$Inchargearchive_namefield = "IF(e.EnglishName IS NULL,e.ChineseName,e.EnglishName)";
}

//$last_updated_field="IF(a.ProcessingAdminUser IS NOT NULL AND a.ProcessingAdminUser!='',a.ProcessingAdminUser,IF(a.ProcessingTerminalUser IS NOT NULL AND a.ProcessingTerminalUser!='',a.ProcessingTerminalUser,''))";

$sql  = "SELECT
         	if(a.NoticeID is null, a.Name, concat(f.Title,' - ', a.Name)),
         	b.Name, 
			IF(a.DefaultAmount IS NULL,' - ',".$lpayment->getWebDisplayAmountFormatDB("a.DefaultAmount").") as Amount,
         	a.DisplayOrder, 
         	a.PayPriority, 
			   	CONCAT(c.countPaid,' (',c.TotalCount,')') as countInfo,
			   	IF(a.Description IS NULL OR a.Description='','&nbsp;',a.Description),
			   	IF((d.UserID IS NULL AND e.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$Inchargearchive_namefield,'</i>'), IF(d.UserID IS NULL AND e.UserID IS NULL,IF(a.ProcessingAdminUser IS NULL OR a.ProcessingAdminUser = '','&nbsp;',a.ProcessingAdminUser),$Inchargenamefield)) as DisplayAdminInCharge, 
          DATE_FORMAT(a.StartDate,'%Y-%m-%d'),
          DATE_FORMAT(a.EndDate,'%Y-%m-%d') 
         FROM
             PAYMENT_PAYMENT_ITEM as a 
             LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY as b ON (a.CatID = b.CatID) 
             LEFT OUTER JOIN TEMP_PAYMENT_ITEM_SUMMARY as c ON (c.pid = a.ItemID) 
             LEFT JOIN INTRANET_USER as d on a.ProcessingAdminUser = d.UserID 
             LEFT JOIN INTRANET_ARCHIVE_USER as e on a.ProcessingAdminUser = e.UserID 
             left join INTRANET_NOTICE as f on f.NoticeID=a.NoticeID
         WHERE
              (a.Name LIKE '%$keyword%' OR
               a.Description LIKE '%$keyword%' OR 
				f.Title LIKE '%$keyword%' 
              )
              $conds $date_conds $conds2 $cond3
       ";

/*
$sql  = "SELECT
          if(a.NoticeID is null, a.Name, concat(f.Title,' - ', a.Name)),
          b.Name, 
		  IF(a.DefaultAmount IS NULL,' - ',".$lpayment->getWebDisplayAmountFormatDB("a.DefaultAmount").") as Amount,
          a.DisplayOrder, 
          a.PayPriority, 
			   	CONCAT(COUNT(DISTINCT c1.PaymentID),' (',COUNT(DISTINCT c2.PaymentID),')') as countInfo,
			   	IF(a.Description IS NULL OR a.Description='','&nbsp;',a.Description),
			   	IF((d.UserID IS NULL AND e.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$Inchargearchive_namefield,'</i>'), IF(d.UserID IS NULL AND e.UserID IS NULL,IF(a.ProcessingAdminUser IS NULL OR a.ProcessingAdminUser = '','&nbsp;',a.ProcessingAdminUser),$Inchargenamefield)) as DisplayAdminInCharge, 
					DATE_FORMAT(a.StartDate,'%Y-%m-%d'),
					DATE_FORMAT(a.EndDate,'%Y-%m-%d')
        FROM
          PAYMENT_PAYMENT_ITEM as a 
          LEFT JOIN 
          PAYMENT_PAYMENT_CATEGORY as b 
          ON (a.CatID = b.CatID) 
          LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT as c1 ON (c1.ItemID = a.ItemID) AND c1.RecordStatus=1 
		  LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT as c2 ON (c2.ItemID = a.ItemID) 
          LEFT JOIN 
					INTRANET_USER as d 
					on a.ProcessingAdminUser = d.UserID 
					LEFT JOIN 
					INTRANET_ARCHIVE_USER as e 
					on a.ProcessingAdminUser = e.UserID 
		left join INTRANET_NOTICE as f on f.NoticeID=a.NoticeID
        WHERE 
			1 $conds $date_conds $conds2 ";
if(trim($keyword)!=''){
 $sql.= " AND (a.Name LIKE '%$keyword%' OR
               a.Description LIKE '%$keyword%') ";
} 
$sql.= " GROUP BY a.ItemID $having_conds ";
*/                
                
# TABLE INFO

$field_array = array("a.Name","b.Name","a.DefaultAmount","a.DisplayOrder","a.PayPriority","countInfo","a.Description","DisplayAdminInCharge","a.StartDate","a.EndDate");

$sql .= " ORDER BY ";
$sql .= (count($field_array)<=$field) ? $field_array[0] : $field_array[$field];
$sql .= ($order==0) ? " DESC" : " ASC";
$li = new libdb();


// TABLE COLUMN


$table="<table width=95% border=0 cellpadding=2 cellspacing=0 align='center' class='eSporttableborder'>";
$table.="<thead>";
$table.="<tr class='eSporttdborder eSportprinttabletitle'>";
$table.="<Td class='eSporttdborder eSportprinttabletitle'>#</td>";
$table.="<Td class='eSporttdborder eSportprinttabletitle'>$i_Payment_Field_PaymentItem</td>";
$table.="<Td class='eSporttdborder eSportprinttabletitle'>$i_Payment_Field_PaymentCategory</td>";
$table.="<Td class='eSporttdborder eSportprinttabletitle'>$i_Payment_Field_Amount</td>";
$table.="<Td class='eSporttdborder eSportprinttabletitle'>$i_Payment_Field_DisplayOrder</td>";
$table.="<Td class='eSporttdborder eSportprinttabletitle'>$i_Payment_Field_PayPriority</td>";
$table.="<Td class='eSporttdborder eSportprinttabletitle'>$i_Payment_Field_PaidCount<br>($i_Payment_Field_TotalPaidCount)</td>";
$table.="<Td class='eSporttdborder eSportprinttabletitle'>$i_general_description</td>";
$table.="<Td class='eSporttdborder eSportprinttabletitle'>$i_general_last_modified_by</td>";
$table.="<Td class='eSporttdborder eSportprinttabletitle'>$i_general_startdate</td>";
$table.="<Td class='eSporttdborder eSportprinttabletitle'>$i_general_enddate</td>";
$table.="</tr>";
$table.="</thead>";
$table.="<tbody>";

$temp = $li->returnArray($sql, sizeof($field_array));

for($i=0;$i<sizeof($temp);$i++){
	//$css =$i%2==0?"tableContent":"tableContent2";
	//$css =$i%2==0?$css_table_content:$css_table_content."2";
	$css = $i%2==0?"attendancepresent":"attendanceouting";
	list($item_name,$cat_name,$amount,$display_order,$pay_priority,$count_info,$desc,$last_update,$start_date,$end_date)=$temp[$i];
	$table.="<tr class='$css'><Td class='eSporttdborder eSportprinttext'>".($i+1)."</td><td class='eSporttdborder eSportprinttext'>$item_name</td><td class='eSporttdborder eSportprinttext'>$cat_name</td><td class='eSporttdborder eSportprinttext'>$amount</td><td class='eSporttdborder eSportprinttext'>$display_order&nbsp;</td><td class='eSporttdborder eSportprinttext'>$pay_priority&nbsp;</td><td class='eSporttdborder eSportprinttext'>$count_info</td><Td class='eSporttdborder eSportprinttext'>$desc</td><Td class='eSporttdborder eSportprinttext'>$last_update</td><td class='eSporttdborder eSportprinttext'>$start_date</td><Td class='eSporttdborder eSportprinttext'>$end_date</td></tr>";
}
if(sizeof($temp)<=0){
	$table.="<tr><td colspan='11' align=center height=40 class='eSporttdborder eSportprinttext'>$i_no_record_exists_msg</td></tr>";
}
$table.="</tbody>";
$table.="</table>";


if($CatID!=''){
	$sql="SELECT Name FROM PAYMENT_PAYMENT_CATEGORY WHERE CatID = '$CatID'";
	$temp = $li->returnVector($sql);
	$cat_name = $temp[0];
}else{
	$cat_name = $i_status_all;
}


$str_item_paid_status="";
switch($itemPaidStatus){
	case "": $str_item_paid_status = $i_status_all; break;
	case 0 : $str_item_paid_status = $i_Payment_Field_NotAllPaid; break;
	case 1 : $str_item_paid_status = $i_Payment_Field_AllPaid; break;
	case 2 : $str_item_paid_status = $i_Payment_PaymentStatus_NotStarted; break;
	//default :  $str_item_paid_status = $i_status_all;
}

$str_item_status="";
switch($itemStatus){
	case 1 : $str_item_status = $i_Payment_Menu_Settings_PaymentItem_ArchivedRecord; break;
	default :  $str_item_status = $i_Payment_Menu_Settings_PaymentItem_ActiveRecord;
}

$keyword=$keyword==""?"--":$keyword;
//include_once($PATH_WRT_ROOT."templates/fileheader.php");
include_once($PATH_WRT_ROOT."templates/".$LAYOUT_SKIN."/layout/print_header.php");
?>
<style type="text/css" media="print">
thead {display: table-header-group;}
</style>
    <table width="90%" align="center" class="print_hide" border="0">
        <tr>
            <td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit_print")?></td>
        </tr>
    </table>
<!-- date range -->
<table border=0 width=95% align=center>
<tr><td class='<?=$css_title?>'><b><?=$i_Payment_SchoolAccount_PresetItem?> (<?="$FromDate $i_Profile_To $ToDate"?>)</b></td></tr>
</table>
<table border=0 width=95% align=center>
<tr><td class='<?=$css_text?>'><B><?=$i_Payment_Field_PaymentCategory?></B>: <?=$cat_name?></td></tr>
<tr><td class='<?=$css_text?>'><B><?=$i_general_status?></B>: <?=$str_item_paid_status?></td></tr>
<tr><td class='<?=$css_text?>'><B><?=$i_Payment_Class_Item_Type?></B>: <?=$str_item_status?></td></tr>
<tr><td class='<?=$css_text?>'><B><?=$i_Payment_Field_PaymentItem?></B>: <?=$keyword?></td></tr>
</table>
<BR>
<?php if($itemStatus==1){?>
<table width=95% border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<?=$i_Payment_Menu_Settings_PaymentItem_Archive_Warning2?>
</td></tr>
</table>
<?php } ?>
<BR>
<?php echo $table; ?>




<?php
include_once($PATH_WRT_ROOT."templates/filefooter.php");
# remove the temp table
$temp_table_sql = "DROP TABLE TEMP_PAYMENT_ITEM_SUMMARY";
$ldb->db_db_query($temp_table_sql);

intranet_closedb();
?>