<?php
# using: 
/*
 * 2020-10-30 (Ray):    Add quota
 * 2020-06-29 (Ray):    Add multi payment gateway
 * 2019-09-27 (Ray):    Add Item Code
 * 2018-07-17 (Carlos): $sys_custom['ePayment']['Alipay'] - Added Merchant Account selection for Alipay.
 * 2016-06-22 (Carlos): $sys_custom['ePayment']['PaymentItemAllowNegativeAmount'] - cust to allow input negative amount for payment item.
 * 2014-08-20 (Carlos): $sys_custom['ePayment']['PaymentItemChangeLog'] - added navigation tag [Payment Item Change Log]
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PaymentItemSettings";

$linterface = new interface_html();

$use_merchant_account = $lpayment->useEWalletMerchantAccount();

$TAGS_OBJ[] = array($i_Payment_Menu_Settings_PaymentItem, $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/",1);
$TAGS_OBJ[] = array($Lang['ePayment']['StudentPaymentItemSettings'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/student_payment_item.php",0);
if($sys_custom['ePayment']['PaymentItemChangeLog']){
	$TAGS_OBJ[] = array($Lang['ePayment']['PaymentItemChangeLog'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/change_log/",0);
}
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_edit);

$ItemID = (is_array($ItemID)? $ItemID[0] : $ItemID);

$info = $lpayment->returnPaymentItemInfo($ItemID);
list ($name, $CatID, $displayOrder, $desp, $start, $end, $PayPriority, $RecordStatus, $NoticeID, $itemcode) = $info;

$amount = $lpayment->returnDefaultPaymentAmount($ItemID);

$cats = $lpayment->returnPaymentCats();
$select_cat = getSelectByArray($cats,"name='CatID'",$CatID,0,1);

$allow_edit_merchant = true;
$PaymentIDSummary = $lpayment->returnPaymentPaidSummary($ItemID);
if($PaymentIDSummary[3] > 0) {
	$allow_edit_merchant = false;
}

?>
<script language='javascript'>
function restrictNumber(obj)
{
	var val = $.trim(obj.value);
	var num = parseFloat(val);
	
	if(isNaN(num) || val == ''){
		num = 0;
	}
	obj.value = num;
}

function checkform(obj){
	if(obj==null) return false;
	objName = obj.ItemName;
	objStartDate = obj.StartDate;
	objEndDate = obj.EndDate;
	objDispOrder = obj.DisplayOrder;
	objPriority = obj.PayPriority;
	objChangeAmount = obj.ChangeAmount;
	restrictNumber(objDispOrder);
	restrictNumber(objPriority);
<?php if($sys_custom['ePayment_PaymentItem_ItemCode']) { ?>
    objCode = obj.ItemCode;
    if(!check_text(objCode,'<?=$i_Payment_Warning_Enter_PaymentItemCode?>')) return false;
    var item_code_check = js_Check_Unique('<?=$ItemID?>', 'ItemCode', 'ItemCodeWarningDiv', 'ItemCode', '<?=$i_Payment_Warning_Enter_PaymentItemCode?>','<?=$i_Payment_Warning_Duplicate_PaymentItemCode?>', false);
    if(item_code_check == false) {
        return false;
    }
<?php } ?>
	if(!check_text(objName,'<?=$i_Payment_Warning_Enter_PaymentItemName?>')) return false;
	if(!check_date(objStartDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) return false;
	if(!check_date(objEndDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) return false;
	if(!check_text(objDispOrder,'<?=$Lang['ePayment']['DisplayOrderMissingWarning']?>')) return false;
	if(!check_text(objPriority,'<?=$i_Payment_Warning_Enter_PayPriority?>')) return false;
	if(objChangeAmount.checked){
		objAmount = obj.Amount;
		restrictNumber(objAmount);
		if(!check_text(objAmount,'<?=$i_Payment_Warning_Enter_NewDefaultAmount?>')) return false;
<?php if(!$sys_custom['ePayment']['PaymentItemAllowNegativeAmount']){ ?>	
		if(!check_numeric(objAmount,'<?=$amount?>','<?=$i_Payment_Warning_InvalidAmount?>')) return false;
<?php } ?>
	}

	<?php if($sys_custom['ePayment']['MultiPaymentGateway']) { ?>
    var have_merchantid = false;
    $("select[id^=MerchantAccountID_]").each(function(e) {
        if($(this).val() != '') {
            have_merchantid = true;
        }
    });
    if(have_merchantid == false) {
        if(confirm('<?=$Lang['ePayment']['SetupMerchant']?>') == false) {
            return false;
        }
    }
	<?php } ?>

	return true;

}
$(document).ready(function() {
	<?php if($sys_custom['ePayment_PaymentItem_ItemCode']) { ?>
    js_Add_KeyUp_Unique_Checking('<?=$ItemID?>', 'ItemCode', 'ItemCodeWarningDiv', 'ItemCode', '<?=$i_Payment_Warning_Enter_PaymentItemCode?>','<?=$i_Payment_Warning_Duplicate_PaymentItemCode?>');
	<?php } ?>

    $("[name^='MerchantAccountID']").each(function() {
        $(this).trigger("change");
    })
});

function js_Add_KeyUp_Unique_Checking(jsItemID, InputID, WarningDivID, DBFieldName, WarnBlankMsg, WarnUniqueMsg)
{
    jsItemID = jsItemID || '';

    $("input#"+InputID).keyup(function(){
        js_Check_Unique(jsItemID, InputID, WarningDivID, DBFieldName, WarnBlankMsg, WarnUniqueMsg, true);
    });
    $("input#"+InputID).change(function(){
        js_Check_Unique(jsItemID, InputID, WarningDivID, DBFieldName, WarnBlankMsg, WarnUniqueMsg, true);
    });
}

function js_Check_Unique(jsItemID, InputID, WarningDivID, DBFieldName, WarnBlankMsg, WarnUniqueMsg, async) {
    jsItemID = jsItemID || '';

    var InputValue = Trim($("input#" + InputID).val());
    var result = false;
    if (InputValue == '') {
        $('div#' + WarningDivID).html(WarnBlankMsg);
        $('div#' + WarningDivID).show();
    } else {
        $('div#' + WarningDivID).html("");
        $('div#' + WarningDivID).hide();
        $.ajax({
            url: "ajax_validate.php",
            data: {
                Action: DBFieldName,
                Value: InputValue,
                ItemID: jsItemID
            },
            async: async,
            type: 'POST',
            success: function (data) {
                if (data != 1) {
                    $('div#' + WarningDivID).html(WarnUniqueMsg).show();
                } else {
                    result = true;
                    $('div#' + WarningDivID).html("").hide();
                }
            }
        });
    }

    return result;
}

function onMerchantChange(obj, sp) {
    var id = $(obj).val();
    $(".quota_"+sp).hide();
    $(".quota_id_"+id).show();
}

</script>
<form name="form1" action="edit_update.php" method="post" onSubmit="return checkform(this)">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
    	<td>
    		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_Payment_Field_PaymentCategory?> <span class='tabletextrequire'>*</span>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<?=$select_cat?>
	    			</td>
    			</tr>

				<?php if($sys_custom['ePayment_PaymentItem_ItemCode']) { ?>
                    <tr>
                        <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
							<?=$i_Payment_Field_PaymentItemCode?> <span class='tabletextrequire'>*</span>
                        </td>
                        <td class="tabletext" width="70%">
                            <input type="text" id="ItemCode" name="ItemCode" class="textboxtext" maxlength="255" value="<?=$itemcode?>">
                            <div id="ItemCodeWarningDiv" style="color: #ff0000;"></div>
                        </td>
                    </tr>
				<?php } ?>

    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_Payment_Field_PaymentItem?> <span class='tabletextrequire'>*</span>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<input type="text" name="ItemName" class="textboxtext" maxlength="255" value="<?=$name?>">
	    			</td>
    			</tr>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_general_startdate?>  <span class='tabletextrequire'>*</span>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<input type="text" name="StartDate" class="textboxnum" maxlength="10" value="<?=$start?>">
	    				<?=$linterface->GET_CALENDAR("form1", "StartDate")?>
	    			</td>
    			</tr>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_general_enddate?> <span class='tabletextrequire'>*</span>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<input type="text" name="EndDate" class="textboxnum" maxlength="10" value="<?=$end?>">
	    				<?=$linterface->GET_CALENDAR("form1", "EndDate")?>
	    			</td>
    			</tr>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_Payment_Field_DisplayOrder?> <span class='tabletextrequire'>*</span>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<input type="text" name="DisplayOrder" class="textboxnum" maxlength="10" value="<?=$displayOrder?>" onchange="restrictNumber(this);">
	    			</td>
    			</tr>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_Payment_Field_PayPriority?> <span class='tabletextrequire'>*</span>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<input type="text" name="PayPriority" class="textboxnum" maxlength="10" value="<?=$PayPriority?>" onchange="restrictNumber(this);"> 
	    				<span class="tabletextremark">(<?=$i_Payment_Note_Priority?>)</span>
	    			</td>
    			</tr>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_general_description?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<?=$linterface->GET_TEXTAREA("Description", $desp)?>
	    			</td>
    			</tr>

    			<?php if($use_merchant_account){ ?>
					<?php if($sys_custom['ePayment']['MultiPaymentGateway']) { ?>
                        <tr>
                            <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
								<?=$Lang['ePayment']['MerchantAccount']?>
                            </td>
                            <td class="tabletext" width="70%">
                                <table>
									<?php
									$x = '';
									$payment_merchant_account_ids = $lpayment->returnPaymentItemMultiMerchantAccountID($ItemID);

									$service_provider_list = $lpayment->getPaymentServiceProviderMapping(false,true);
									foreach($service_provider_list as $k=>$temp) {
										$merchants = $lpayment->getMerchantAccounts(array('ServiceProvider'=>$k,'RecordStatus'=>1,'order_by_accountname'=>true));
										$merchant_selection_data = array();
										$selected_merchant_id = '';
										$selected_merchant_account_name = '';
										for($i=0;$i<count($merchants);$i++){
											$merchant_selection_data[] = array($merchants[$i]['AccountID'],$merchants[$i]['AccountName']);
											if(in_array($merchants[$i]['AccountID'], $payment_merchant_account_ids)) {
												$selected_merchant_id = $merchants[$i]['AccountID'];
												$selected_merchant_account_name = $merchants[$i]['AccountName'];
											}
										}
										if ($selected_merchant_account_name == '') {
											$selected_merchant_account_name = $Lang['General']['EmptySymbol'];
										}
										$x .= '<tr>';
										$x .= '<td>'.$temp.'</td>';
										$x .= '<td>';
										if($allow_edit_merchant) {
											$x .= $linterface->GET_SELECTION_BOX($merchant_selection_data, ' id="MerchantAccountID_' . $k . '" name="MerchantAccountID_' . $k . '" onchange="onMerchantChange(this,\''.$k.'\')"', $Lang['General']['EmptySymbol'], $selected_merchant_id);
										} else {
											$x .= $selected_merchant_account_name . '<input type="hidden" id="MerchantAccountID_' . $k . '" name="MerchantAccountID_' . $k . '" value="' . $selected_merchant_id . '" onchange="onMerchantChange(this,\''.$k.'\')" />';
										}

										foreach($merchant_selection_data as $merchant_data) {
											$year = date('Y');
											$month = date('m');
											$sql = "SELECT * FROM PAYMENT_MERCHANT_ACCOUNT_QUOTA WHERE Year='$year' AND Month='$month' AND AccountID='".$merchant_data[0]."'";
											$rs_quota = $lpayment->returnArray($sql);
											if(count($rs_quota)>0) {
												$remain_quota = $rs_quota[0]['Quota'] - $rs_quota[0]['UsedQuota'];
												if($remain_quota < 0) {
													$remain_quota = 0;
												}
												$quota_str = str_replace('{COUNT}', $remain_quota, $Lang['ePayment']['CurrentRemainQuota']);
												$x .= ' <span class="quota_' . $k . ' quota_id_' . $merchant_data[0] . '" style="display: none;">(' . $quota_str . ')</span>';
											}
										}

										$x .= '</td>';
										$x .= '</tr>';
									}
									echo $x;
									?>
                                </table>
                            </td>
                        </tr>
					<?php } else { ?>
                    <tr>
                        <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
                            <?=$Lang['ePayment']['MerchantAccount']?>
                        </td>
                        <td class="tabletext" width="70%">
                        <?php
                        $merchants = $lpayment->getMerchantAccounts(array('RecordStatus'=>1,'order_by_accountname'=>true));
                        $merchant_selection_data = array();
						$selected_merchant_account_name = '';
                        for($i=0;$i<count($merchants);$i++){
                            $merchant_selection_data[] = array($merchants[$i]['AccountID'],$merchants[$i]['AccountName']);
                            if($merchants[$i]['AccountID'] == $info['MerchantAccountID']) {
								$selected_merchant_account_name = $merchants[$i]['AccountName'];
							}
                        }
						if ($selected_merchant_account_name == '') {
							$selected_merchant_account_name = $Lang['General']['EmptySymbol'];
						}
						if($allow_edit_merchant) {
							echo $linterface->GET_SELECTION_BOX($merchant_selection_data, ' id="MerchantAccountID" name="MerchantAccountID" ', $Lang['General']['EmptySymbol'], $info['MerchantAccountID']);
						} else {
							echo $selected_merchant_account_name. '<input type="hidden" id="MerchantAccountID" name="MerchantAccountID" value="' . $info['MerchantAccountID'] . '" />';
						}
                        ?>
                        </td>
                    </tr>
                    <?php } ?>
                <?php } ?>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_Payment_ChangeDefaultAmount?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<input type="checkbox" name="ChangeAmount" value="1" onclick="this.form.Amount.disabled=!this.checked">
	    			</td>
    			</tr>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_Payment_Field_NewDefaultAmount?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<input type="text" name="Amount" class="textboxnum" maxlength="10" disabled value="<?=$amount?>" onchange="restrictNumber(this);">
	    			</td>
    			</tr>
    		</table>
    	</td>
	</tr>
	<tr>
    	<td>
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
					<td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
				</tr>
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
    <tr>
	    <td align="center">
			<?= $linterface->GET_ACTION_BTN($button_save, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
<input type=hidden name=ItemID value="<?=$ItemID?>">
</form>

<?
$linterface->LAYOUT_STOP();
print $linterface->FOCUS_ON_LOAD("form1.CatID");
intranet_closedb();
?>
