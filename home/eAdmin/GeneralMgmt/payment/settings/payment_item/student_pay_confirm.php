<?php
// Editing by 
/*
 * 2018-02-21 (Carlos): Removed send push message function here.
 * 2017-09-01 (Carlos): Fixed the missing itemName.
 * 2017-06-21 (Anna):  $plugin['eClassApp'] -added push message 
 * 2017-02-09 (Carlos): $sys_custom['ePayment']['HartsPreschool'] - handle payment time.
 * 2015-10-28 (Carlos): Allow staff users to pay.
 * 2015-10-13 (Carlos): $sys_custom['ePayment']['AllowNegativeBalance'] - let not enough balance accounts to pay.
 * 2015-03-06 (Carlos): $sys_custom['ePayment']['PaymentMethod'] -  Added [ReceiptRemark]
 * 2014-08-19 (Carlos): $sys_custom['ePayment']['PaymentItemChangeLog'] - Added change log 
 * 2014-03-24 Carlos : $sys_custom['ePayment']['PaymentMethod'] - added [Payment Method]
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$use_magic_quotes = (function_exists("get_magic_quotes_gpc") && get_magic_quotes_gpc()) || function_exists("magicQuotes_addslashes");

$lpayment = new libpayment();

$itemName = $lpayment->returnPaymentItemName($ItemID);

if(is_array($PaymentID))
	$list = "'".implode("','",$PaymentID)."'";

if($list!=""){

/*$sql = "LOCK TABLES
             PAYMENT_PAYMENT_ITEMSTUDENT AS a READ,
             PAYMENT_ACCOUNT AS c READ,
             PAYMENT_PAYMENT_ITEMSTUDENT WRITE,
             PAYMENT_ACCOUNT WRITE,
             PAYMENT_OVERALL_TRANSACTION_LOG WRITE
             ";
$lpayment->db_db_query($sql);*/
$sql_paid = '';
if($sys_custom['ttmss_ePayment']) {

} else {
	$sql_paid = 'a.RecordStatus = 0 AND';
}

$sql = "SELECT a.StudentID, a.Amount, c.Balance,a.PaymentID,a.RecordStatus,a.PaymentMethod, a.NoticePaymentMethod 
               FROM PAYMENT_PAYMENT_ITEMSTUDENT as a
                    LEFT OUTER JOIN PAYMENT_ACCOUNT as c ON a.StudentID = c.StudentID
               WHERE $sql_paid a.PaymentID IN ($list)";
$result = $lpayment->returnArray($sql,4);

if($sys_custom['ePayment']['PaymentItemChangeLog']){
	$name_field = getNameFieldWithClassNumberByLang("u.");
	$sql = "SELECT a.StudentID, a.Amount, c.Balance,a.PaymentID, $name_field as StudentName, a.RecordStatus 
            FROM PAYMENT_PAYMENT_ITEMSTUDENT as a 
				LEFT JOIN INTRANET_USER as u ON u.UserID=a.StudentID 
                LEFT JOIN PAYMENT_ACCOUNT as c ON a.StudentID = c.StudentID 
            WHERE $sql_paid a.PaymentID IN ($list)";
    $oldRecords = $lpayment->returnResultSet($sql);
}

$result2 = array();

$allEnough = true;
for ($i=0; $i<sizeof($result); $i++)
{
     list ($StudentID, $amount, $balance, $paymentID, $RecordStatus,$currentPaymentMethod,$currentNoticePaymentMethod) = $result[$i];

	if($sys_custom['ttmss_ePayment']) {
		if($RecordStatus != '0') {
			$result2[] = $result[$i];
			continue;
		}
	}
     $balanceAfter = $balance - $amount;
     
     //if (abs($balanceAfter) < 0.001)    # Smaller than 0.1 cent
     //{
     //    $balanceAfter = 0;
     //}               
     if ($balanceAfter < 0 && !$sys_custom['ePayment']['AllowNegativeBalance'])
     {
         $allEnough = false;
         //break;
         continue;
     }else{
	     $result2[] = $result[$i];
	 }
}
//if ($allEnough)
//{
$Result = array();
$lpayment->Start_Trans();

    # Proceed payment here
    //$itemName = addslashes($itemName);
    for ($i=0; $i<sizeof($result2); $i++)
    {
         list ($StudentID, $amount, $balance, $paymentID, $RecordStatus, $currentPaymentMethod) = $result2[$i];
         //already paid
		if($sys_custom['ttmss_ePayment']) {
			if ($RecordStatus != '0') {

				if($sys_custom['ePayment']['PaymentMethod']){
					if($use_magic_quotes){
						$receipt_remark = trim(stripslashes($ReceiptRemark[$paymentID]));
					}else{
						$receipt_remark = trim($ReceiptRemark[$paymentID]);
					}

					$sql_update_payment_method = '';
					if($currentNoticePaymentMethod == '') {
						$sql_update_payment_method = "PaymentMethod='" . $PaymentMethod[$paymentID] . "',";
						if (isset($sys_custom['ePayment_NotAllowEditPaymentMethodItems']) && is_array($sys_custom['ePayment_NotAllowEditPaymentMethodItems'])) {
							if (in_array($currentPaymentMethod, $sys_custom['ePayment_NotAllowEditPaymentMethodItems'])) {
								$sql_update_payment_method = '';
							}
						}
					}

					$sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET  ";
					$sql .= $sql_update_payment_method." ReceiptRemark='".$lpayment->Get_Safe_Sql_Query($receipt_remark)."' ";
					$sql.= " ,ProcessingTerminalUser=NULL,ProcessingTerminalIP=NULL, ProcessingAdminUser='".$_SESSION['UserID']."',DateModified=NOW() WHERE PaymentID = '$paymentID'";
					$Result['UpdatePaymentStudentStatus'.$i] = $lpayment->db_db_query($sql);
				}

				continue;
			}
		}

         $balanceAfter = $balance - $amount;
         $StudentIDAry[] = $result2[$i]['StudentID'];
		 $payment_time_val = 'NOW()';
		 if($sys_custom['ePayment']['HartsPreschool'] && isset($PaymentTime[$paymentID]) && $PaymentTime[$paymentID]!=''){
		 	$payment_time_val = "'".$PaymentTime[$paymentID]."'";
		 }
		 
         # 1. Set Payment Record Status to PAID
         $sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET RecordStatus = 1,PaidTime = $payment_time_val ";
         if($sys_custom['ePayment']['PaymentMethod']){
         	if($use_magic_quotes){
	     		$receipt_remark = trim(stripslashes($ReceiptRemark[$paymentID]));
	     	}else{
	     		$receipt_remark = trim($ReceiptRemark[$paymentID]);
	     	}
         	
         	$sql .= ",PaymentMethod='".$PaymentMethod[$paymentID]."',ReceiptRemark='".$lpayment->Get_Safe_Sql_Query($receipt_remark)."' ";
         }
		 $sql.= " ,ProcessingTerminalUser=NULL,ProcessingTerminalIP=NULL, ProcessingAdminUser='".$_SESSION['UserID']."',DateModified=NOW() WHERE PaymentID = '$paymentID'";
         $Result['UpdatePaymentStudentStatus'.$i] = $lpayment->db_db_query($sql);
         
         # 2. Deduct Balance in PAYMENT_ACCOUNT
         /*
         $sql = "UPDATE PAYMENT_ACCOUNT SET Balance = Balance - $amount,
                                        LastUpdateByAdmin = '$PHP_AUTH_USER',
                                        LastUpdateByTerminal = NULL, LastUpdated = NOW() WHERE StudentID = $StudentID";
         */
         $balance_to_db = round($balanceAfter,2);
         
          $sql = "UPDATE PAYMENT_ACCOUNT SET Balance = '$balance_to_db',
                                LastUpdateByAdmin = '".$_SESSION['UserID']."',
                                LastUpdateByTerminal = NULL, LastUpdated = NOW() WHERE StudentID = '$StudentID'";
                       
         $Result['UpdateStudentPaymentAccount'.$i] = $lpayment->db_db_query($sql);
         
         # 3. Insert Transaction Record
         $sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
                 (StudentID, TransactionType, Amount, RelatedTransactionID, BalanceAfter, TransactionTime, Details)
                 VALUES
                 ('$StudentID', 2,'$amount','$paymentID','$balance_to_db',$payment_time_val,'".$lpayment->Get_Safe_Sql_Query($itemName)."')";
         $Result['UpdateStudentOverallTransactionLog'.$i] = $lpayment->db_db_query($sql);
         $logID = $lpayment->db_insert_id();
         $sql = "UPDATE PAYMENT_OVERALL_TRANSACTION_LOG SET RefCode = CONCAT('PAY',LogID) WHERE LogID = '$logID'";
         $Result['UpdateStudentOverallTransactionLogSetRefCode'.$i] = $lpayment->db_db_query($sql);
  //       $StudentIDAry[] = $result2['StudentID'];
    
    }
    $pay_msg = 1;
//}
//else
//{
//    $pay_msg = 2;
//}

/*$sql = "UNLOCK TABLES";
$lpayment->db_db_query($sql);*/
	if (in_array(false,$Result)) {
		$lpayment->RollBack_Trans();
		$Msg = $Lang['ePayment']['PayProcessUnsuccess'];
	}
	else {
		$lpayment->Commit_Trans();
		$Msg = $Lang['ePayment']['PayProcessSuccess'];
		
		if($sys_custom['ePayment']['PaymentItemChangeLog']){
			$logDisplayDetail = $itemName.'<br />';
			$logHiddenDetail = $ItemID.' | '.$itemName.'<br />';
			for($i=0;$i<count($oldRecords);$i++){
				$logDisplayDetail .= 'User: '.$oldRecords[$i]['StudentName'].' Balance: $'.$oldRecords[$i]['Balance'].' Amount: $'.$oldRecords[$i]['Amount'] .'<br />';
				$delim = '';
				foreach($oldRecords[$i] as $key => $val){
					$logHiddenDetail .= $delim.$key.': '.$val;
					$delim = ', ';
				}
				$logHiddenDetail .= '<br />';
			}
			$logType = $lpayment->getPaymentItemChangeLogType("Pay");
			$lpayment->logPaymentItemChange($logType, $logDisplayDetail, $logHiddenDetail);	
		}
		/*
		if($plugin['eClassApp'] == true && $send_push_message['0'] != '' && $PushMessageType=='1' && count($_POST['StudentID'])>0){
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
			include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
			
			$ldbsms 	= new libsmsv2();
			$luser = new libuser();
			$libeClassApp = new libeClassApp();
			
			$messageTitle = standardizeFormPostValue($_POST['PushMessageTitle']);
			$messageContent = standardizeFormPostValue($_POST['MessageContent']);
			$ItemID =  $_POST['ItemID'];
			//$itemName = $lpayment->returnPaymentItemName($ItemID);
			//$StudentIDAry[] = $result[$i]['StudentID'];
			$ClickedStudentIDAry = $_POST['StudentID'];

			$isPublic = "N";
			//debug_pr($luser->getParentStudentMappingInfo($studentIds));
			$parentStudentAssoAry = BuildMultiKeyAssoc($luser->getParentStudentMappingInfo($ClickedStudentIDAry), 'ParentID', $IncludedDBField=array('StudentID'), $SingleValue=1, $BuildNumericArray=1);
			$studentWithParentUsingAppAry = $luser->getStudentWithParentUsingParentApp();
			
			$individualMessageInfoAry = array();
			for ($i=0; $i<sizeof($result2); $i++)
    		{
         		list ($studentId, $amount, $balance, $paymentID) = $result2[$i];
				if(in_array($studentId,$ClickedStudentIDAry) && in_array($studentId,$studentWithParentUsingAppAry)){
					$thisStudent = new libuser($studentId);
					$appParentIdAry = $luser->getParentUsingParentApp($studentId);
				
					//	debug_pr($appParentIdAry);
					$_individualMessageInfoAry = array();
					foreach ($appParentIdAry as $parentId) {
						$_individualMessageInfoAry['relatedUserIdAssoAry'][$parentId] = (array)$studentId;
					}
					$_individualMessageInfoAry['messageTitle'] = $messageTitle;
					$_individualMessageInfoAry['messageContent'] = str_replace("[StudentName]", $thisStudent->UserName(), $messageContent);
					//$_individualMessageInfoAry['messageContent'] = $tempContent;
					
					$_individualMessageInfoAry['messageContent'] = str_replace('($Payment_Amount)',$amount,$_individualMessageInfoAry['messageContent']);
					$_individualMessageInfoAry['messageContent'] = str_replace('($payment_balance)',$balance-$amount,$_individualMessageInfoAry['messageContent']);
					$_individualMessageInfoAry['messageContent'] = str_replace('($Payment_Item)',$itemName,$_individualMessageInfoAry['messageContent']);
					$_individualMessageInfoAry['messageContent'] = str_replace('($payment_outstanding_amount)',max($amount, $amount-$balance),$_individualMessageInfoAry['messageContent']);
					$_individualMessageInfoAry['messageContent'] = str_replace('($Inadequate_Amount)',max(0.00,$amount-$balance),$_individualMessageInfoAry['messageContent']);
					$_individualMessageInfoAry['messageContent'] = $ldbsms->replace_content($studentId, $_individualMessageInfoAry['messageContent'], '');
					
					$individualMessageInfoAry[] = $_individualMessageInfoAry;
				}
    		}
	
			$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $messageTitle, 'MULTIPLE MESSAGES', $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString);
			
			$msg = ($notifyMessageId > 0)? $Lang['AppNotifyMessage']['ReturnMsg']['SendSuccess']:$Lang['AppNotifyMessage']['ReturnMsg']['SendFail'];
		}
		*/
	}
}

intranet_closedb();
header ("Location: list.php?ItemID=$ItemID&ClassName=$ClassName&pageNo=$pageNo&order=$order&field=$field&pay=$pay_msg&msg=$msg&Msg=".urlencode($Msg));
?>