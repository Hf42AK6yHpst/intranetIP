<?php
// Editing by 
/*
 * 2014-03-24 (Carlos): $sys_custom['ePayment']['PaymentMethod'] - Added [Payment Method]
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."templates/fileheader.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();


if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$targetID = (int)$targetID;

$lpayment = new libpayment();

$lclass = new libclass();
$lu = new libuser("$targetID");
$student_name=$lu->UserNameLang();

$is_KIS_hide_balance = $_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance'];

if(!$is_KIS_hide_balance){
	$sql_getbalance = "SELECT Balance FROM PAYMENT_ACCOUNT WHERE StudentID='$targetID'";	
	$balanceRecord = $lpayment->returnVector($sql_getbalance);	
	$student_balance = $balanceRecord[0];
}

if (!isset($order)) $order = 0;
if (!isset($field)) $field = 3;
//$order = ($order == 1) ? 1 : 0;

$Inchargenamefield = getNameFieldByLang("e.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$Inchargearchive_namefield = " IF(f.ChineseName IS NULL,f.EnglishName,f.ChineseName)";
}else {
	$Inchargearchive_namefield = "IF(f.EnglishName IS NULL,f.ChineseName,f.EnglishName)";
}

$sql  = "select 
	      if(b.noticeid is null, b.Name, concat(d.title,' - ',b.name)),		  	
          c.name,
		  a.Amount,
		  b.DisplayOrder,
          b.PayPriority,  
          b.Description,
          ";
		 // IF((e.UserID IS NULL AND f.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$Inchargearchive_namefield,'</i>'), IF(e.UserID IS NULL AND f.UserID IS NULL,IF(b.ProcessingAdminUser IS NULL OR b.ProcessingAdminUser = '','&nbsp;',b.ProcessingAdminUser),$Inchargenamefield)) as DisplayAdminInCharge,
$sql .= " DATE_FORMAT(b.startdate,'%Y-%m-%d'),
          DATE_FORMAT(b.enddate,'%Y-%m-%d') ";
if($sys_custom['ePayment']['PaymentMethod']){
	$sql.=" ,CASE a.PaymentMethod 
				WHEN '1' THEN '".$sys_custom['ePayment']['PaymentMethodItems'][1]."' 
				WHEN '2' THEN '".$sys_custom['ePayment']['PaymentMethodItems'][2]."' 
				WHEN '3' THEN '".$sys_custom['ePayment']['PaymentMethodItems'][3]."' 
				ELSE '".$Lang['ePayment']['NA']."' 
			END as PaymentMethod,
			a.Remark ";
}
$sql.=" from   
          PAYMENT_PAYMENT_ITEMSTUDENT as a 
          INNER JOIN PAYMENT_PAYMENT_ITEM as b on a.itemid=b.itemid
          LEFT JOIN PAYMENT_PAYMENT_CATEGORY as c on b.catid=c.catid 
          LEFT JOIN INTRANET_NOTICE as d on d.noticeid=b.noticeid 
		  LEFT join INTRANET_USER as e on b.ProcessingAdminUser = e.UserID
          LEFT JOIN INTRANET_ARCHIVE_USER as f on b.ProcessingAdminUser = f.UserID
        where 
          a.studentid='".$targetID."' 
          and a.paidtime is null  
		  and (a.RecordStatus=0 OR a.RecordStatus IS NULL) ";
                               
# TABLE INFO

$field_array = array("b.name",
				     "c.name",
					 "a.Amount",
					 "b.DisplayOrder,b.PayPriority",
					 "b.PayPriority",
					 "DisplayAdminInCharge",
					 "b.startdate",
					 "b.enddate");
if($sys_custom['ePayment']['PaymentMethod']){
	$field_array[] = "PaymentMethod";
	$field_array[] = "Remark";
}

$sql .= " ORDER BY ";
$sql .= !isset($field_array[$field]) ? $field_array[3] : $field_array[$field];
//$sql .= ($order==0) ? " DESC" : " ASC";
$li = new libdb();


// TABLE COLUMN
$ePayment_Amount=$Lang['ePayment']['Amount'];

$table="<table width=95% border=0 cellpadding=2 cellspacing=0 align='center' class='eSporttableborder'>";
$table.="<tr class='eSporttdborder eSportprinttabletitle'>";
$table.="<td class='eSporttdborder eSportprinttabletitle'>#</td>";
$table.="<td class='eSporttdborder eSportprinttabletitle'>$i_Payment_Field_PaymentItem</td>";
$table.="<td class='eSporttdborder eSportprinttabletitle'>$i_Payment_Field_PaymentCategory</td>";
$table.="<td class='eSporttdborder eSportprinttabletitle'>$ePayment_Amount</td>";
$table.="<td class='eSporttdborder eSportprinttabletitle'>$i_Payment_Field_DisplayOrder</td>";
$table.="<td class='eSporttdborder eSportprinttabletitle'>$i_Payment_Field_PayPriority</td>";
$table.="<td class='eSporttdborder eSportprinttabletitle'>$i_general_description</td>";
$table.="<td class='eSporttdborder eSportprinttabletitle'>$i_general_startdate</td>";
$table.="<td class='eSporttdborder eSportprinttabletitle'>$i_general_enddate</td>";
if($sys_custom['ePayment']['PaymentMethod']){
	$table.="<td class='eSporttdborder eSportprinttabletitle'>".$Lang['ePayment']['PaymentMethod'] ."</td>";
	$table.="<td class='eSporttdborder eSportprinttabletitle'>".$Lang['General']['Remark'] ."</td>";
}
$table.="</tr>";



$temp = $li->returnArray($sql, sizeof($field_array));

for($i=0;$i<sizeof($temp);$i++){
	$css = $i%2==0?"attendancepresent":"attendanceouting";
	list($item_name,$cat_name,$item_amount,$display_order,$pay_priority,$admin_in_charge,$start_date,$end_date)=$temp[$i];
	$table.="<tr class='$css'><td class='eSporttdborder eSportprinttext'>".($i+1)."</td><td class='eSporttdborder eSportprinttext'>$item_name</td><td class='eSporttdborder eSportprinttext'>$cat_name</td><td class='eSporttdborder eSportprinttext'>$item_amount</td><td class='eSporttdborder eSportprinttext'>$display_order</td><td class='eSporttdborder eSportprinttext'>$pay_priority</td><td class='eSporttdborder eSportprinttext'>$admin_in_charge</td><td class='eSporttdborder eSportprinttext'>$start_date</td><Td class='eSporttdborder eSportprinttext'>$end_date</td>";
	if($sys_custom['ePayment']['PaymentMethod']){
		$table.="<td class='eSporttdborder eSportprinttext'>".$temp[$i]['PaymentMethod']."</td>";
		$table.="<td class='eSporttdborder eSportprinttext'>".Get_String_Display($temp[$i]['PaymentRemark'],0,'&nbsp;')."</td>";
	}
	$table.="</tr>";
}
if(sizeof($temp)<=0){
	$table.="<tr><td colspan='10' align=center height=40 class='eSporttdborder eSportprinttext'>$i_no_record_exists_msg</td></tr>";
}
$table.="</table>";

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
?>
<!-- date range -->
<table border=0 width=95% align=center>
<tr><td class='<?=$css_title?>'><b><?=$Lang['ePayment']['Class']?></b>: <?=$lclass->getClassName($targetClass)?></td></tr>
<tr><td class='<?=$css_title?>'><b><?=$Lang['ePayment']['Student']?></b>: <?=$student_name?></td></tr>
<?php if(!$is_KIS_hide_balance){ ?>
<tr><td class='<?=$css_title?>'><b><?=$i_Payment_Field_CurrentBalance?></b>: <?=$lpayment->getWebDisplayAmountFormat($student_balance)?></td></tr>
<?php } ?>
</table>

<BR>
<?php if($itemStatus==1){?>
<table width=95% border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<?=$i_Payment_Menu_Settings_PaymentItem_Archive_Warning2?>
</td></tr>
</table>
<?php } ?>
<BR>
<?php echo $table; ?>




<?php
include_once($PATH_WRT_ROOT."templates/filefooter.php");

intranet_closedb();
?>