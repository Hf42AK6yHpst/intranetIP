<?php
// Editing by 
/*
 * 2020-06-29 (Ray): Added multi payment gateway
 * 2014-08-19 (Carlos): $sys_custom['ePayment']['PaymentItemChangeLog'] - Added change log
 * 2014-01-16 (Carlos): Remove payment item subsidy records related to payment item students records
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();

$li = new libdb();
$list = "'".implode("','",$ItemID)."'";

if($sys_custom['ePayment']['PaymentItemChangeLog']){
	$sql = "SELECT * FROM PAYMENT_PAYMENT_ITEM WHERE ItemID IN ($list)";
	$oldRecords = $li->returnResultSet($sql);
	if($sys_custom['ePayment']['MultiPaymentGateway']) {
		foreach ($oldRecords as $k => $temp) {
			$payment_merchant_account_ids = $lpayment->returnPaymentItemMultiMerchantAccountID($temp['ItemID']);
			$oldRecords[$k]['multi_payment_gateway'] = implode(',', $payment_merchant_account_ids);
		}
	}
}

$li->Start_Trans();
$sql = "DELETE FROM PAYMENT_PAYMENT_ITEM WHERE ItemID IN ($list)";
$Result['DeleteItem'] = $li->db_db_query($sql);

if($sys_custom['ePayment']['MultiPaymentGateway']) {
	$sql = "DELETE FROM PAYMENT_PAYMENT_ITEM_PAYMENT_GATEWAY WHERE ItemID IN ($list)";
	$Result['DeleteItem'] = $li->db_db_query($sql);
}

$sql = "SELECT PaymentID FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID IN ($list)";
$paymentItemIdAry = $li->returnVector($sql);

if(count($paymentItemIdAry)>0){
	$sql = "DELETE FROM PAYMENT_PAYMENT_ITEM_SUBSIDY WHERE PaymentID IN (".implode(",",$paymentItemIdAry).")";
	$Result['DeleteSubsidyRecords'] = $li->db_db_query($sql);
}

$sql = "DELETE FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID IN ($list)";
$Result['DeleteItemStudent'] = $li->db_db_query($sql);

if (in_array(false,$Result)) {
	$li->RollBack_Trans();
	$Msg = $Lang['ePayment']['PaymentItemDeletedUnsuccess'];
}
else {
	$li->Commit_Trans();
	$Msg = $Lang['ePayment']['PaymentItemDeletedSuccess'];
	
	if($sys_custom['ePayment']['PaymentItemChangeLog']){
		$logDisplayDetail = '';
		$logHiddenDetail = '';
		for($i=0;$i<count($oldRecords);$i++){
			$oldRecord = $oldRecords[$i];
			$logDisplayDetail .= $oldRecord['Name'].'<br />';
			foreach($oldRecord as $key => $val){
				$logHiddenDetail .= $key.': '.$val.'<br />';
			}
		}
		
		$logType = $lpayment->getPaymentItemChangeLogType("Delete");
		$lpayment->logPaymentItemChange($logType, $logDisplayDetail, $logHiddenDetail);	
	}
}

header ("Location: index.php?Msg=".urlencode($Msg));
intranet_closedb();
?>
