<?php
// Editing by 
/*
 * 2017-10-10 (Carlos): Changed to new receipt format.
 * 2015-02-18 (Carlos): Added [Payment Method] and [Receipt Remark].
 * 2015-02-05 (Carlos): Change some css rules.
 * 2015-01-21 (Carlos): Display student info, balance after paid and disclaimer remark
 * 2014-09-11 (Carlos): $sys_custom['ePayment']['ThermalPaperReceipt'] - for KIS tbcpk
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ePayment']['ThermalPaperReceipt']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();

$linterface = new interface_html();

//$page_breaker = "<p class=\"breakhere\">";

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
?>
<style type="text/css">
<!--
@page {
	size: 8cm auto;
	margin: 0;
	padding: 0;
}
* {
	margin: 0;
	padding: 0;
}
body {
	font-size: 62.5%;
	margin: 0;
	padding: 0;
}
body th, body td, .tabletext {
	font-size: 1.2em;
}
table, tr, th, td , span, div, h2, h3, .tabletext, .student-info {
	font-family: "中黑體", "微軟正黑體", "黑體", "SimSun", "Verdana","Arial","Helvetica","sans-serif";
}
.wrapper {
	display: block;
	width: 8cm;
	max-width: 8cm;
	margin-bottom: 4px;
}
td {
	padding: 0.2em;
}
td.table_header {
	padding-bottom: 1.5em;
<?php if($intranet_session_language != 'en'){ ?>	
	white-space: nowrap;
<?php } ?>
}
td.row_total {
	padding-top: 1.5em;
}
h2, h3 {
	font-weight: bold;
	text-align: center;
	font-size: 1.25em;
}
h3 {
	font-size: 1.2em;
}
.divider {
	width: 8cm;
	margin: 0 0.5em;
	padding: 0;
	border-left: 0px;
	border-right: 0px;
	border-top: 1px dashed black;
	border-bottom: 0px;
}
.student-info {
	font-size: 1.25em;
}
.double-underlined {
	border-bottom: 3px double black;
}

div.separator {
	line-height: 5em;
}
}
-->
</style>
<table width="100%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit_print")?></td>
	</tr>
</table>
<?php
if(!is_array($_REQUEST['PaymentID']) && strstr($_REQUEST['PaymentID'],',')!==FALSE){
	$PaymentIdAry = explode(',',$_REQUEST['PaymentID']);
}else{
	$PaymentIdAry = (array)$_REQUEST['PaymentID'];
}
$sql = "SELECT DISTINCT StudentID FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE PaymentID IN ('".implode("','",$PaymentIdAry)."')";
$StudentID = $lpayment->returnVector($sql);

$StartingReceiptNumber = $_REQUEST['StartingReceiptNumber'];
$school_data = explode("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_name = $school_data[0];
$school_org = $school_data[1];

$ReceiptTitle= $_REQUEST['ReceiptTitle'] != '' ? $_REQUEST['ReceiptTitle'] : $Lang['ePayment']['OfficialReceipt'];

if ($sys_custom['PaymentReceiptNumber']) {
	// consolidate school year to recipt number
	$SchoolYear = explode(' - ',GET_ACADEMIC_YEAR());
	$ReceiptHead = substr($SchoolYear[0],-2).substr($SchoolYear[1],-2);
	
	if($StartingReceiptNumber == ""){
		$StartingReceiptNumber = $lpayment->Get_Next_Receipt_Number();
	}
}
/*
$table_header  = "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">";
//$table_header .= "<tr><td class=\"eSporttdborder eSportprinttabletitle\">$i_Payment_Receipt_Payment_Category</td>";
$table_header .= "<tr><td class=\"table_header\">$i_Payment_Receipt_Payment_Item</td>";
//$table_header .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_Payment_Receipt_Payment_Description</td>";
$table_header .= "<td class=\"table_header\">$i_Payment_Field_RefCode</td>";
if($sys_custom['ePayment']['PaymentMethod']){
	$table_header .= "<td class=\"table_header\">".$Lang['ePayment']['PaymentMethod']."</td>";
	//$table_header .= "<td class=\"table_header\">".$Lang['General']['Remark']."</td>";
}
//$table_header .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_Payment_Receipt_Date</td>";
$table_header .= "<td class=\"table_header\">$i_Payment_Receipt_Payment_Amount</td></tr>";
*/
$display = "";

$lclass = new libclass();
if ($StudentID == "")
{
    if ($ClassName == "")
    {
        # All students
        $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 ORDER BY ClassName, ClassNumber, EnglishName";
        $target_students = $lclass->returnVector($sql);
    }
    else
    {
        # All students in $ClassName
        $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND ClassName = '$ClassName' ORDER BY ClassName, ClassNumber, EnglishName";
        $target_students = $lclass->returnVector($sql);
    }
}
else
{
	if (is_array($StudentID)) {
		$target_students = $StudentID;
	}
	else {
    $target_students = array($StudentID);
  }
}

$date_field = "a.PaidTime";
$conds = "";
if ($date_from != "")
{
    $conds .= " AND $date_field >= '$date_from'";
}
if ($date_to != "")
{
    $conds .= " AND $date_field <= '$date_to 23:59:59'";
}

//if($IndividualItems == 1){
	$conds .= " AND a.PaymentID IN ('".implode("','",(array)$PaymentIdAry)."') ";
//}

# Get Latest Balance
$lpayment = new libpayment();
$list = "'".implode("','", $target_students)."'";
$sql = "SELECT StudentID, Balance FROM PAYMENT_ACCOUNT WHERE StudentID IN ($list)";
$temp = $lpayment->returnArray($sql,2);
$balance_array = build_assoc_array($temp);

$total_record = 0;

$colspan = 2;
$remark_colspan = 3;
if($sys_custom['ePayment']['PaymentMethod']){
	$colspan += 1;
	$remark_colspan += 1;
}

for ($j=0; $j<sizeof($target_students); $j++)
{
	$target_id = $target_students[$j];
	$lu = new libuser($target_id);
	$studentname = $lu->UserNameClassNumber();
	$i_title = $studentname;
	$curr_balance = $balance_array[$target_id];  
    $balance_after = 0;
    
    $sql = "SELECT 
						c.Name, 
						b.Name,
						b.Description, 
						a.PaidTime,
						a.Amount, 
						d.RefCode,
						d.RelatedTransactionID, 
						c.Description,
						d.BalanceAfter ";
	if($sys_custom['ePayment']['PaymentMethod']){
		$sql .= ",a.PaymentMethod,a.ReceiptRemark ";
	}				 
	$sql .= "FROM 
						PAYMENT_PAYMENT_ITEMSTUDENT as a
			  inner join PAYMENT_OVERALL_TRANSACTION_LOG as d on (d.TransactionType=2 and d.RelatedTransactionID=a.PaymentID and d.StudentID=$target_id)
	          LEFT OUTER JOIN PAYMENT_PAYMENT_ITEM as b ON a.ItemID = b.ItemID
	          LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY as c ON b.CatID = c.CatID 
	        WHERE 
	        	a.StudentID = '$target_id'
	        	AND a.RecordStatus = 1 AND d.TransactionType=2 
	        	$conds 
	        ORDER BY d.LogID ".($sys_custom['ePayment']['ReceiptByCategoryDesc']?",c.Description ":"")."
                ";
	$student_itemlist = $lu->returnArray($sql);
	
	if($sys_custom['ePayment']['ReceiptByCategoryDesc']){
		// items are group by student and payment item category description
		$sql = "SELECT 
					DISTINCT c.Description 
				FROM PAYMENT_PAYMENT_ITEMSTUDENT as a
			  	INNER JOIN PAYMENT_OVERALL_TRANSACTION_LOG as d on (d.TransactionType=2 and d.RelatedTransactionID=a.PaymentID and d.StudentID=$target_id)
	          	LEFT JOIN PAYMENT_PAYMENT_ITEM as b ON a.ItemID = b.ItemID
	          	LEFT JOIN PAYMENT_PAYMENT_CATEGORY as c ON b.CatID = c.CatID 
	        	WHERE 
	        		a.StudentID = '$target_id' 
	        		AND a.RecordStatus = 1 AND d.TransactionType=2 
	        		$conds ";
		$tmp_category_desc_list = $lu->returnVector($sql);
		$category_desc_list = array();
		for($k=0;$k<sizeof($tmp_category_desc_list);$k++)
		{
			$category_records = array();
			for($i=0;$i<sizeof($student_itemlist);$i++) {
				if($tmp_category_desc_list[$k] == $student_itemlist[$i]['Description']){
					$category_records[] = $student_itemlist[$i];
				}
			}
			if(sizeof($category_records)>0){
				$category_desc_list[] = $category_records;
			}
		}
	}else
	{
		// normal flow, items are group by student
		$category_desc_list = array();
		$category_desc_list[] = $student_itemlist;
	}
	
	for($n=0;$n<sizeof($category_desc_list);$n++)
	{
		if($sys_custom['ePayment']['PaymentMethod']){
	    	$receipt_remark_ary = array();
	    	$remark_content = '';
	    }
		$student_itemlist = $category_desc_list[$n];
		if(!empty($student_itemlist))
		{
			$RecordByRelatedTransactionID = array();
			for ($i=0; $i<sizeof($student_itemlist); $i++)
			{
				$RecordByRelatedTransactionID[$student_itemlist[$i]['RelatedTransactionID']]  = $student_itemlist[$i];
			}
			
			$total_record++;
			# start student
			$total_amount = 0;
			$list_table = "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"2\">";
			
			foreach($RecordByRelatedTransactionID as $key => $recordAry)
			{
			     list($cat_name, $item_name, $item_desp,$item_paidtime, $item_amount, $ref_code) = $recordAry;
			     $total_amount += $item_amount;
			     $item_amount = '$'.number_format($item_amount,2);
			     $balance_after = $recordAry['BalanceAfter'];
			     //$list_table .= "<tr><td class=\"eSporttdborder eSportprinttext\">$cat_name&nbsp;</td>";
			     $list_table .= "<tr><td class=\"tabletext\" width=\"32%\">".$Lang['ePayment']['PaymentDate']."：</td><td class=\"tabletext\" colspan=\"2\">".Get_String_Display(date("Y-m-d",strtotime($item_paidtime)),0,'&nbsp;')."</td></tr>";
			     $list_table .= "<tr><td class=\"tabletext\" width=\"32%\">".$i_Payment_Receipt_Payment_Item."：</td><td class=\"tabletext\" colspan=\"2\">".Get_String_Display($item_name,0,'&nbsp;')."</td></tr>";
			     //$list_table .= "<td class=\"eSporttdborder eSportprinttext\">$item_desp&nbsp;</td>";
			     $list_table .= "<tr><td class=\"tabletext\" width=\"32%\">".$i_Payment_Field_RefCode."：</td><td class=\"tabletext\" colspan=\"2\">".Get_String_Display($ref_code,0,'&nbsp;')."</td></tr>";
			     if($sys_custom['ePayment']['PaymentMethod']){
			     	$payment_method = $recordAry[9] == ''? '&nbsp;' : $sys_custom['ePayment']['PaymentMethodItems'][$recordAry[9]];
			     	$receipt_remark = trim($recordAry[10]);
			     	$remark_symbol = '';
			     	if($receipt_remark != ''){
			     		$next_remark_num = sizeof($receipt_remark_ary)+1;
			     		$receipt_remark_ary[$next_remark_num] = $receipt_remark;
			     		$remark_symbol = '<sup>*'.$next_remark_num.'</sup>';
			     		$remark_content .= ($next_remark_num>1?'<br />':'').$remark_symbol.'&nbsp;'.$receipt_remark;
			     	}
			     	$list_table .= "<tr><td class=\"tabletext\" width=\"32%\">".$Lang['ePayment']['PaymentMethod']."：</td><td class=\"tabletext\" colspan=\"2\">".$payment_method.$remark_symbol."</td></tr>";
			     	//$list_table .= "<td>".nl2br($receipt_remark)."&nbsp;</td>";
			     }
			     //$list_table .= "<td class=\"eSporttdborder eSportprinttext\">$item_paidtime&nbsp;</td>";
			     $list_table .= "<tr><td class=\"tabletext\" width=\"32%\">".$i_Payment_Receipt_Payment_Amount."：</td><td width=\"38%\">&nbsp;</td><td class=\"tabletext\" align=\"right\" width=\"30%\">$item_amount</td></tr>\n";
			     $list_table .= "<tr><td colspan=\"3\">&nbsp;</td></tr>";
			}
			
			$total_amount = '$'.number_format($total_amount,2);
			$list_table .= "<tr>";
			$list_table .= "<td class=\"tabletext\" align=\"left\" width=\"32%\">".$i_Payment_Receipt_Payment_Total."：</td><td width=\"38%\">&nbsp;</td><td class=\"tabletext\" align=\"right\" width=\"30%\">$total_amount</td></tr>";
			//$list_table .= "<td align=\"right\" class=\"row_total\"><span class=\"double-underlined\">$total_amount</span></td></tr>\n";
			//$list_table .= "<tr><td colspan=\"$remark_colspan\">&nbsp;</td></tr>";
			//$list_table .= "<tr><td colspan=\"$remark_colspan\">&nbsp;</td></tr>";
			$list_table .= "<tr>
								<td class=\"tabletext\" align=\"left\" width=\"32%\">".$Lang['ePayment']['BalanceAfter']."： 
								<td width=\"38%\">&nbsp;</td>
								<td class=\"tabletext\" align=\"right\" width=\"30%\">".$lpayment->getWebDisplayAmountFormat($balance_after)."</td>
							</tr>";
			$list_table .= "<tr><td colspan=\"3\">&nbsp;</td></tr>";
			$list_table .= "<tr><td colspan=\"3\" class=\"tabletext\">".$Lang['ePayment']['RemarkReceiptDisclaimer']."</td></tr>";
			if(sizeof($receipt_remark_ary)>0){
				$list_table .= "<tr><td colspan=\"3\" class=\"tabletext\">".$remark_content."</td></tr>";
			}
			$list_table .= "</table>";
			//$name_string = "$i_Payment_Receipt_ReceivedFrom $studentname $i_Payment_Receipt_PaidAmount$total_amount $i_Payment_Receipt_ForItems";
			
			// consolidate receipt number
			$ReceiptNumber = $ReceiptHead;
			$ReceiptNumber .= (trim($lu->ClassName) != "")? '-'.$lu->ClassName:'';
			$ReceiptNumber .= (trim($lu->ClassNumber) != "")? '-'.$lu->ClassNumber:'';
			$ReceiptNumber .= '-'.$StartingReceiptNumber;
			
			//if($j > 0 && $j < sizeof($target_students)){
			//	echo "<div class=\"divider\"></div>";
			//}
			
			if ($j < (sizeof($target_students)-1) /* || ($sys_custom['ePayment']['ReceiptByCategoryDesc'] && $n < (sizeof($category_desc_list)-1))*/){
				//echo '<div style="page-break-after:always">';
				echo '<div>';
			}
			echo "<table class=\"wrapper\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td>\n";
				echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
			
			if($sys_custom['ePayment']['ReceiptByCategoryDesc'] && $student_itemlist[0]['Description'] != ''){
				echo "<tr><td class='tabletext'>\n";
				echo "<h2>".$student_itemlist[0]['Description']."</h2>";
				echo "</td></tr>\n";
				
				echo "<tr><td class='tabletext'>\n";
				echo "<h3>".$ReceiptTitle."</h3>";
				echo "</td></tr>\n";
			}else{
				echo "<tr><td class='tabletext'>\n";
				echo "<h2>".$school_name."</h2>";
				echo "</td></tr>\n";
				
				echo "<tr><td class='tabletext'>\n";
				echo "<h3>".$ReceiptTitle."</h3>";
				echo "</td></tr>\n";
			}
			
			echo "</table>\n";
			if ($sys_custom['PaymentReceiptNumber']) {
				echo "<table width=100% border=0 cellspacing=0 cellpadding=0 >
							<tr>
								<td class=\"tabletext\" align=\"left\" width=\"32%\" nowrap>
									".$Lang['Payment']['ReceiptNumber']."： 
								</td>
								<td class=\"tabletext\" align=\"left\" width=\"68%\">".$ReceiptNumber."</td>
							</tr>
						</table>";
			}
			//echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td class='tabletext'>$name_string</td></tr></table>\n";
			//echo "<br>";
			echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
					<tr>
						<td class=\"tabletext\" align=\"left\" width=\"32%\" nowrap>".$Lang['ePayment']['PrintDate']."： <td>
						<td class=\"tabletext\" align=\"left\" width=\"68%\">".date('Y-m-d')."</td>
					</tr>
				  </table>";
			echo "<br>";
			
			echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
					<tr>
						<td class=\"tabletext student-info\" align=\"left\">$studentname</td>
					</tr>
				  </table><br>";
			
			echo $list_table;
			echo "</td></tr></table>\n";
			/*
			echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td>\n";
			if (is_file("$intranet_root/templates/reportheader/paymentlist_footer.php"))
			{
			    include("$intranet_root/templates/reportheader/paymentlist_footer.php");
			}
			echo "</td></tr></table>\n";
			*/
			//echo "</table>\n";
			
			if ($j < sizeof($target_students))
				echo "</div><div class=\"separator\">&nbsp;</div>";
			# End of 1 student
			
			
			
			if ($sys_custom['PaymentReceiptNumber']) {
				$StartingReceiptNumber++;
				switch (strlen($StartingReceiptNumber)) {
					case 0:
						$StartingReceiptNumber = '0001';
						break;
					case 1:
						$StartingReceiptNumber = '000'.$StartingReceiptNumber;
						break;
					case 2:
						$StartingReceiptNumber = '00'.$StartingReceiptNumber;
						break;
					case 3:
						$StartingReceiptNumber = '0'.$StartingReceiptNumber;
						break;
					default:
						break;
				}
			}
		}
	}
}

$StartingReceiptNumber += 0;
if ($sys_custom['PaymentReceiptNumber']) {
	$lpayment->Save_Receipt_Number($StartingReceiptNumber);
}

if($total_record==0)
{
	echo "<div align='center'>$i_no_record_exists_msg</div>";
}

intranet_closedb();
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
?>