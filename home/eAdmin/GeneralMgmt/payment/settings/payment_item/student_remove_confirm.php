<?
// editing by 
/*
 * 2019-11-26 (Carlos): Delete payment item student records that are not paid to cater refunded records.
 * 2015-10-28 (Carlos): Include staff users.
 * 2014-08-27 (Carlos): Added redirect to back_to_url for student_payment_item.php
 * 2014-08-19 (Carlos): $sys_custom['ePayment']['PaymentItemChangeLog'] - Added change log
 * 2013-10-18 (Carlos): Delete payment item subsidy records
 * 2012-06-08 (Carlos): Renamed this file name from student_remove.php to student_remove_confirm.php 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$list = "'".implode("','",$PaymentID)."'";
$li = new libpayment();

if($sys_custom['ePayment']['PaymentItemChangeLog']){
	$name_field = getNameFieldWithClassNumberByLang("u.");
	$sql = "SELECT ppi.Name as ItemName, a.StudentID, a.Amount, c.Balance, a.PaymentID, $name_field as StudentName 
	        FROM PAYMENT_PAYMENT_ITEMSTUDENT as a 
				INNER JOIN PAYMENT_PAYMENT_ITEM as ppi ON ppi.ItemID=a.ItemID 
				LEFT JOIN INTRANET_USER as u ON u.UserID=a.StudentID 
	            LEFT JOIN PAYMENT_ACCOUNT as c ON a.StudentID = c.StudentID 
	        WHERE a.PaymentID IN ($list) ";
	$oldRecords = $li->returnResultSet($sql);	
}

$sql = "DELETE FROM PAYMENT_PAYMENT_ITEM_SUBSIDY WHERE PaymentID IN ($list)";
$li->db_db_query($sql);

$sql = "DELETE FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE PaymentID IN ($list) AND RecordStatus <> 1";
if ($li->db_db_query($sql)) {
	$Msg = $Lang['ePayment']['PaymentItemStudentDeleteSuccess'];
	$Msg2 = 'PaymentItemDeletedSuccess';
	
	if($sys_custom['ePayment']['PaymentItemChangeLog']){
		$logDisplayDetail = '';
		$logHiddenDetail = '';
		
		for($i=0;$i<count($oldRecords);$i++){
			$logDisplayDetail .= 'Item: '.$oldRecords[$i]['ItemName'].' User: '.$oldRecords[$i]['StudentName'].' Amount: $'.$oldRecords[$i]['Amount'] .'<br />';
			$delim = '';
			foreach($oldRecords[$i] as $key => $val){
				$logHiddenDetail .= $delim.$key.': '.$val;
				$delim = ', ';
			}
			$logHiddenDetail .= '<br />';
		}
		
		$logType = $li->getPaymentItemChangeLogType("DeleteStudent");
		$li->logPaymentItemChange($logType, $logDisplayDetail, $logHiddenDetail);		
	}
	
}else {
	$Msg = $Lang['ePayment']['PaymentItemStudentDeleteFail'];
	$Msg2 = 'PaymentItemDeletedUnsuccess';
}

intranet_closedb();
if($_SERVER['REQUEST_METHOD']=='POST' && $_POST['back_to_url'] != '' && $_POST['back_to_url'] == 'student_payment_item.php'){
	header("Location: ".$_POST['back_to_url']."?targetClass=".$_POST['targetClass']."&targetID=".$_POST['targetID']."&Msg=".urlencode($Msg2));
	exit;
}
header ("Location: list.php?ItemID=$ItemID&ClassName=$ClassName&pageNo=$pageNo&order=$order&field=$field&Msg=".urlencode($Msg));
?>
