<?php
// Editing by 
/*
 * 2014-08-19 (Carlos): $sys_custom['ePayment']['PaymentItemChangeLog'] - Added change log
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$li = new libpayment();
$list = "'".implode("','",$ItemID)."'";

if($sys_custom['ePayment']['PaymentItemChangeLog']){
	$sql = "SELECT * FROM PAYMENT_PAYMENT_ITEM WHERE ItemID IN ($list) ORDER BY ItemID";
	$oldRecords = $li->returnResultSet($sql);	
}

$sql="UPDATE PAYMENT_PAYMENT_ITEM SET RecordStatus=1,ProcessingAdminUser='".$_SESSION['UserID']."' WHERE ItemID IN ($list)";
$Result = $li->db_db_query($sql);

if ($Result) {
	$Msg = $Lang['ePayment']['ArchiveSuccess'];
}
else {
	$Msg = $Lang['ePayment']['ArchiveUnsuccess'];
}

if($Result && $sys_custom['ePayment']['PaymentItemChangeLog']){
	$sql = "SELECT * FROM PAYMENT_PAYMENT_ITEM WHERE ItemID IN ($list) ORDER BY ItemID";
	$newRecords = $li->returnResultSet($sql);
	$logDisplayDetail = '';
	$logHiddenDetail = '';
	for($i=0;$i<count($oldRecords);$i++){
		$oldRecord = $oldRecords[$i];
		$logDisplayDetail .= $oldRecord['Name'].'<br />';
		foreach($oldRecord as $key => $val){
			//if($oldRecord[$key] != $newRecords[$i][$key]){
				$logHiddenDetail .= $key.': '.$oldRecord[$key].' | '.$newRecords[$i][$key].'<br />';
			//}
		}
	}
	$logType = $li->getPaymentItemChangeLogType("Archive");
	$li->logPaymentItemChange($logType, $logDisplayDetail, $logHiddenDetail);
}

header ("Location: index.php?Msg=".urlencode($Msg));
intranet_closedb();
?>
