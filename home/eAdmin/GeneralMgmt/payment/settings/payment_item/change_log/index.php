<?php
// Editing by 
/*
 * 2014-08-20 (Carlos): Created for $sys_custom['ePayment']['PaymentItemChangeLog']
 */

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ePayment']['PaymentItemChangeLog']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
# preserve table view
if ($ck_payment_item_change_log_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_payment_item_change_log_page_number", $pageNo, 0, "", "", 0);
	$ck_payment_item_change_log_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_payment_item_change_log_page_number!="")
{
	$pageNo = $ck_payment_item_change_log_page_number;
}

if ($ck_payment_item_change_log_page_order!=$order && $order!="")
{
	setcookie("ck_payment_item_change_log_page_order", $order, 0, "", "", 0);
	$ck_payment_item_change_log_page_order = $order;
} else if (!isset($order) && $ck_payment_item_change_log_page_order!="")
{
	$order = $ck_payment_item_change_log_page_order;
}

if ($ck_payment_item_change_log_page_field!=$field && $field!="")
{
	setcookie("ck_payment_item_change_log_page_field", $field, 0, "", "", 0);
	$ck_payment_item_change_log_page_field = $field;
} else if (!isset($field) && $ck_payment_item_change_log_page_field!="")
{
	$field = $ck_payment_item_change_log_page_field;
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PaymentItemSettings";

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$linterface = new interface_html();

$date_from = !isset($date_from) || $date_from == ''? date("Y-m-d") : $date_from;
$date_to = !isset($date_to) || $date_to == ''? date("Y-m-d") : $date_to;

$log_types = $lpayment->getPaymentItemChangeLogTypeArray();
$log_type_ary = array();

$name_field = getNameFieldByLang("u.");

$log_type_field = "CASE a.LogType ";
for($i=0;$i<count($log_types);$i++){
	$log_type_field .= " WHEN '".$log_types[$i]."' THEN '".$Lang['ePayment']['PaymentItemChangeLogType'][$log_types[$i]]."' ";
	$log_type_ary[] = array($log_types[$i],$Lang['ePayment']['PaymentItemChangeLogType'][$log_types[$i]]);
}
$log_type_field .= " END ";

$log_type_selection = getSelectByArray($log_type_ary, ' id="LogType" name="LogType" onchange="document.form1.submit();" ', $LogType, 1, 0);

$log_type_cond = "";
if(isset($LogType) && $LogType != ''){
	$log_type_cond = " a.LogType='$LogType' AND ";
}

$sql = "SELECT 
			DATE_FORMAT(a.DateInput,'%Y-%m-%d %H:%i:%s') as DateInput,
			$log_type_field as LogType,
			a.LogDisplayDetail,
			$name_field as InputBy ";
//$sql.= ",CONCAT('<input type=\"checkbox\" name=\"LogID[]\" value=\"', a.LogID ,'\" />') ";
$sql.=" FROM PAYMENT_PAYMENT_ITEM_CHANGE_LOG as a 
		LEFT JOIN INTRANET_USER as u ON u.UserID=a.InputBy 
		WHERE $log_type_cond a.DateInput >= '$date_from 00:00:00' AND a.DateInput <= '$date_to 23:59:59' ";

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("DateInput",
						 "LogType",
						 "a.LogDisplayDetail",
						 "InputBy");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0);
$li->IsColOff = "IP25_table";

if ($field=="" && $order=="") {
	$li->field = 0;
	$li->order = 1;
}

$date_range_filter = '';
$date_range_filter .= $Lang['General']['From']."&nbsp;".$linterface->GET_DATE_PICKER("date_from",$date_from);
$date_range_filter .= $Lang['General']['To']."&nbsp;".$linterface->GET_DATE_PICKER("date_to",$date_to);
$date_range_filter .= "&nbsp;".$linterface->GET_SMALL_BTN($Lang['Btn']['Submit'], "submit", "document.form1.submit();", $ParName="btnSubmit", "", "", $Lang['Btn']['Submit']);

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<th width='1' class='num_check'>#</th>\n";
$li->column_list .= "<th width='15%'>".$li->column_IP25($pos++, $Lang['General']['DateInput'])."</th>\n";
$li->column_list .= "<th width='10%'>".$li->column_IP25($pos++, $Lang['ePayment']['RecordType'])."</th>\n";
$li->column_list .= "<th width='60%'>".$li->column_IP25($pos++, $Lang['ePayment']['RecordInfo'])."</th>\n";
//$li->column_list .= "<th width='60%'>".$Lang['ePayment']['RecordInfo']."</th>\n";
$li->column_list .= "<th width='15%'>".$li->column_IP25($pos++, $Lang['General']['InputBy'])."</th>\n";
//$li->column_list .= "<th width='1'>".$li->check("LogID[]")."</th>\n";

$toolbar = $linterface->GET_LNK_EXPORT("javascript:exportPage(document.form1,'item_export.php');","","","","",0);
$toolbar .= $linterface->GET_LNK_PRINT("javascript:openPrintPage();","","","","",0);


$TAGS_OBJ[] = array($i_Payment_Menu_Settings_PaymentItem, $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/",0);
$TAGS_OBJ[] = array($Lang['ePayment']['StudentPaymentItemSettings'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/student_payment_item.php",0);
$TAGS_OBJ[] = array($Lang['ePayment']['PaymentItemChangeLog'],"",1);

$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();

$ReturnMsg = isset($Lang['General']['ReturnMessage'][$Msg])? $Lang['General']['ReturnMessage'][$Msg] : '';
$linterface->LAYOUT_START($ReturnMsg);

?>
<script language="javascript">
function openPrintPage()
{
	 q_str = "date_from=<?=$date_from?>&date_to=<?=$date_to?>&LogType=<?=$LogType?>&pageNo=<?=$li->pageNo?>&order=<?=$li->order?>&field=<?=$li->field?>&numPerPage=<?=$li->page_size?>";
	 url="item_print.php?"+q_str; 
     newWindow(url,8);
}
function exportPage(obj,url){
	old_url = obj.action;
    obj.action=url;
    obj.submit();
    obj.action = old_url;
}
</script>
<br />
<form id="form1" name="form1" method="POST">
<div class="table_board">
<table width="100%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td class="tabletext" align="right" colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td align="left"><?=$toolbar?></td>
		<td align="right">&nbsp;</td>
	</tr>
</table>
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
		<tbody>
			<tr>
				<td valign="bottom">
					<div class="table_filter">
						<?=$log_type_selection."&nbsp;"?>
						<?=$date_range_filter?>
				    </div>
				</td>
				<td valign="bottom">
					&nbsp;
					<!--
					<div class="common_table_tool">
						<a href="javascript:checkRemove(document.form1,'LogID[]','delete_log.php');" class="tool_delete" title="<?=$Lang['Btn']['Delete']?>" onclick=""><?=$Lang['Btn']['Delete']?></a>
					</div>
					-->
				</td>
			</tr>
			<tr>
				<td colspan="2"><?php echo $li->display("96%"); ?></td>
			</tr>
		</tbody>
	</table>
</div>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>
<br />
<br />
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>