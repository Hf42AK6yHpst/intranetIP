<?php
// Editing by 
################################################################################################################################################
#
################################################################################################################################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$lexport = new libexporttext();

if (!isset($order)) $order = 1;
if (!isset($field)) $field = 3;
$order = ($order == 1) ? 1 : 0;

$date_from = !isset($date_from) || $date_from == ''? date("Y-m-d") : $date_from;
$date_to = !isset($date_to) || $date_to == ''? date("Y-m-d") : $date_to;

$log_type_value = $i_status_all;
if(isset($Lang['ePayment']['PaymentItemChangeLogType'][$LogType])) {
	$log_type_value = $Lang['ePayment']['PaymentItemChangeLogType'][$LogType];
}

$log_types = $lpayment->getPaymentItemChangeLogTypeArray();
$log_type_ary = array();

$name_field = getNameFieldByLang("u.");

$log_type_field = "CASE a.LogType ";
for($i=0;$i<count($log_types);$i++){
	$log_type_field .= " WHEN '".$log_types[$i]."' THEN '".$Lang['ePayment']['PaymentItemChangeLogType'][$log_types[$i]]."' ";
	$log_type_ary[] = array($log_types[$i],$Lang['ePayment']['PaymentItemChangeLogType'][$log_types[$i]]);
}
$log_type_field .= " END ";


$log_type_cond = "";
if(isset($LogType) && $LogType != ''){
	$log_type_cond = " a.LogType='$LogType' AND ";
}

$sql = "SELECT 
			DATE_FORMAT(a.DateInput,'%Y-%m-%d %H:%i:%s') as DateInput,
			$log_type_field as LogType,
			a.LogDisplayDetail,
			$name_field as InputBy ";
//$sql.= ",CONCAT('<input type=\"checkbox\" name=\"LogID[]\" value=\"', a.LogID ,'\" />') ";
$sql.=" FROM PAYMENT_PAYMENT_ITEM_CHANGE_LOG as a 
		LEFT JOIN INTRANET_USER as u ON u.UserID=a.InputBy 
		WHERE $log_type_cond a.DateInput >= '$date_from 00:00:00' AND a.DateInput <= '$date_to 23:59:59' ";


$field_array = array("DateInput","LogType","a.LogDisplayDetail","InputBy");
$sql .= " ORDER BY ";
$sql .= (count($field_array)<=$field) ? $field_array[0] : $field_array[$field];
$sql .= ($order==0) ? " DESC" : " ASC";

$li = new libdb();

$temp = $li->returnArray($sql, sizeof($field_array));

$csv_utf="";
$csv_utf .= $Lang['General']['DateInput']."\t";
$csv_utf .= $Lang['ePayment']['RecordType']."\t";
$csv_utf .= $Lang['ePayment']['RecordInfo']."\t";
$csv_utf .= $Lang['General']['InputBy']."\r\n";


for($i=0;$i<sizeof($temp);$i++){
	list($date_input,$log_type,$log_display_detail,$input_by)=$temp[$i];
	$log_display_detail = str_replace("\t"," ",$log_display_detail);
	
	$csv_utf.="$date_input\t$log_type\t$log_display_detail\t$input_by\t\r\n";
}


$csv_utf_title = $Lang['ePayment']['PaymentItemChangeLog']."\r\n";
$csv_utf_title .= $Lang['ePayment']['RecordType']."\t$log_type_value\r\n";
$csv_utf_title .= $Lang['General']['From']."\t$date_from\r\n";
$csv_utf_title .= $Lang['General']['To']."\t$date_to\r\n";

$csv_utf = $csv_utf_title.$csv_utf;


intranet_closedb();


$filename = "payment_item_change_log.csv";
$lexport->EXPORT_FILE($filename, $csv_utf);

