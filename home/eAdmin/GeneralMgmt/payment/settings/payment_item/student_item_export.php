<?php
#using: 
/*
 * 2014-03-24 (Carlos): $sys_custom['ePayment']['PaymentMethod'] - Added [Payment Method]
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");


intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lclass = new libclass();
$lu = new libuser("$targetID");
$student_name=$lu->UserNameLang();

$lpayment = new libpayment();
$lexport = new libexporttext();

$is_KIS_hide_balance = $_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance'];

if(!$is_KIS_hide_balance){
	$sql_getbalance = "SELECT Balance FROM PAYMENT_ACCOUNT WHERE StudentID='$targetID'";	
	$balanceRecord = $lpayment->returnVector($sql_getbalance);	
	$student_balance = $balanceRecord[0];
}

if (!isset($order)) $order = 0;
if (!isset($field)) $field = 3;
//$order = ($order == 1) ? 1 : 0;

$Inchargenamefield = getNameFieldByLang("e.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$Inchargearchive_namefield = " IF(f.ChineseName IS NULL,f.EnglishName,f.ChineseName)";
}else {
	$Inchargearchive_namefield = "IF(f.EnglishName IS NULL,f.ChineseName,f.EnglishName)";
}

$sql  = "select 
	      if(b.noticeid is null, b.Name, concat(d.title,' - ', b.name)),		  	
          c.name,
		  a.Amount,
		  b.DisplayOrder,
          b.PayPriority,  
          b.Description,
          ";
		 // IF((e.UserID IS NULL AND f.UserID IS NOT NULL),CONCAT('*',$Inchargearchive_namefield), IF(e.UserID IS NULL AND f.UserID IS NULL,IF(b.ProcessingAdminUser IS NULL OR b.ProcessingAdminUser = '',' ',b.ProcessingAdminUser),$Inchargenamefield)) as DisplayAdminInCharge,
$sql .= " DATE_FORMAT(b.startdate,'%Y-%m-%d'),
          DATE_FORMAT(b.enddate,'%Y-%m-%d') ";
if($sys_custom['ePayment']['PaymentMethod']){
	$sql.=" ,CASE a.PaymentMethod 
				WHEN '1' THEN '".$sys_custom['ePayment']['PaymentMethodItems'][1]."' 
				WHEN '2' THEN '".$sys_custom['ePayment']['PaymentMethodItems'][2]."' 
				WHEN '3' THEN '".$sys_custom['ePayment']['PaymentMethodItems'][3]."' 
				ELSE '".$Lang['ePayment']['NA']."' 
			END as PaymentMethod,
			a.Remark ";
}
$sql.=" from   
		  PAYMENT_PAYMENT_ITEMSTUDENT as a 
          INNER JOIN PAYMENT_PAYMENT_ITEM as b on a.itemid=b.itemid
          LEFT JOIN PAYMENT_PAYMENT_CATEGORY as c on b.catid=c.catid 
          LEFT JOIN INTRANET_NOTICE as d on d.noticeid=b.noticeid 
		  LEFT join INTRANET_USER as e on b.ProcessingAdminUser = e.UserID
          LEFT JOIN INTRANET_ARCHIVE_USER as f on b.ProcessingAdminUser = f.UserID 
		 where 
          a.studentid='".$targetID."' 
          and a.paidtime is null
          and (a.RecordStatus=0 OR a.RecordStatus IS NULL) ";
                
                
# TABLE INFO
$field_array = array("b.name",
				     "c.name",
					 "a.Amount",
					 "b.DisplayOrder,b.PayPriority",
					 "b.PayPriority",
					 "DisplayAdminInCharge",
					 "b.startdate",
					 "b.enddate");
if($sys_custom['ePayment']['PaymentMethod']){
	$field_array[] = "PaymentMethod";
	$field_array[] = "Remark";
}
$sql .= " ORDER BY ";
$sql .= (!isset($field_array[$field])) ? $field_array[3] : $field_array[$field];
//$sql .= ($order==0) ? " DESC" : " ASC";
$li = new libdb();
$temp = $li->returnArray($sql, sizeof($field_array));

$ePayment_Amount=$Lang['ePayment']['Amount'];
$csv="";
$csv.="\"$i_Payment_Field_PaymentItem\",";
$csv.="\"$i_Payment_Field_PaymentCategory\",";
$csv.="\"$ePayment_Amount\",";
$csv.="\"$i_Payment_Field_DisplayOrder\"";
$csv.="\"$i_Payment_Field_PayPriority\"";
$csv.="\"$i_general_description\"";
$csv.="\"$i_general_startdate\",";
$csv.="\"$i_general_enddate\"";
if($sys_custom['ePayment']['PaymentMethod']){
	$csv.=",\"".$Lang['ePayment']['PaymentMethod']."\"";
	$csv.=",\"".$Lang['General']['Remark']."\"";
}
$csv.="\n";

$csv_utf="";
$csv_utf="$i_Payment_Field_PaymentItem\t";
$csv_utf.="$i_Payment_Field_PaymentCategory\t";
$csv_utf.="$ePayment_Amount\t";
$csv_utf.="$i_Payment_Field_DisplayOrder\t";
$csv_utf.="$i_Payment_Field_PayPriority\t";
$csv_utf.="$i_general_description\t";
$csv_utf.="$i_general_startdate\t";
$csv_utf.="$i_general_enddate";
if($sys_custom['ePayment']['PaymentMethod']){
	$csv_utf.="\t".$Lang['ePayment']['PaymentMethod'];
	$csv_utf.="\t".$Lang['General']['Remark'];
}
$csv_utf.="\r\n";



for($i=0;$i<sizeof($temp);$i++){
	list($item_name,$cat_name,$item_amount,$display_order,$pay_priority,$admin_in_charge,$start_date,$end_date)=$temp[$i];
	$item_name2 = str_replace("\t"," ",$item_name);
	
	$csv.="\"$item_name2\",\"$cat_name\",\"$item_amount\",\"$display_order\",\"$pay_priority\",\"$admin_in_charge\",\"$start_date\",\"$end_date\"";
	if($sys_custom['ePayment']['PaymentMethod']){
		$csv.=",\"".$temp[$i]['PaymentMethod']."\"";
		$csv.=",\"".trim($temp[$i]['PaymentRemark'])."\"";
	}
	$csv.="\n";
	$csv_utf.="$item_name2\t$cat_name\t$item_amount\t$display_order\t$pay_priority\t$admin_in_charge\t$start_date\t$end_date";
	if($sys_custom['ePayment']['PaymentMethod']){
		$csv_utf.="\t".$temp[$i]['PaymentMethod'];
		$csv_utf.="\t".trim($temp[$i]['PaymentRemark']);	
	}
	$csv_utf.="\r\n";

}


$ePayment_Class=$Lang['ePayment']['Class'];
$ePayment_Student=$Lang['ePayment']['Student'];
$class_name=$lclass->getClassName($targetClass);
$csv_title="\"$ePayment_Class\",\"$class_name\"\n";
$csv_title.="\"$ePayment_Student\",\"$student_name\"\n";
if(!$is_KIS_hide_balance){
	$csv_title.="\"$i_Payment_Field_CurrentBalance\",\"".$lpayment->getWebDisplayAmountFormat($student_balance)."\"\n";
}

$csv = $csv_title.$csv;

$csv_utf_title="$ePayment_Class\t$class_name\r\n";
$csv_utf_title.="$ePayment_Student\t$student_name\r\n";
if(!$is_KIS_hide_balance){
	$csv_utf_title.="\"$i_Payment_Field_CurrentBalance\"\t\"".$lpayment->getWebDisplayAmountFormat($student_balance)."\"\r\n";
}

$csv_utf = $csv_utf_title.$csv_utf;



intranet_closedb();

$filename = "payment_item.csv";
$lexport->EXPORT_FILE($filename, $csv_utf);
?>