<?php
/*
 * 2014-08-20 (Carlos): $sys_custom['ePayment']['PaymentItemChangeLog'] - added navigation tag [Payment Item Change Log]
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

if($ItemID=="" || sizeof($ItemID)<=0)
	header("Location: index.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PaymentItemSettings";

$linterface = new interface_html();

$TAGS_OBJ[] = array($i_Payment_Menu_Settings_PaymentItem, $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/",1);
$TAGS_OBJ[] = array($Lang['ePayment']['StudentPaymentItemSettings'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/student_payment_item.php",0);
if($sys_custom['ePayment']['PaymentItemChangeLog']){
	$TAGS_OBJ[] = array($Lang['ePayment']['PaymentItemChangeLog'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/change_log/",0);
}
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();


$li = new libdb();

$list = "'".implode("','",$ItemID)."'";

$allow_delete = true;

$sql ="SELECT ItemID,RecordStatus FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID IN ($list) ORDER BY ItemID";
$temp = $li->returnArray($sql,2);
for($i=0;$i<sizeof($temp);$i++){
	list($itemid,$isPaid) = $temp[$i];
	
	if($data[$itemid]['paid']=="")
		$data[$itemid]['paid']=0;
	if($data[$itemid]['unpaid']=="")
		$data[$itemid]['unpaid']=0;
	
	if($isPaid==1){
		$data[$itemid]['paid'] +=1;
		$allow_delete = false;
	}
	else $data[$itemid]['unpaid'] +=1;
}

$sql=" 
		SELECT 
			a.ItemID,a.Name, b.Name,a.StartDate,a.EndDate 
		FROM 
			PAYMENT_PAYMENT_ITEM AS a  LEFT OUTER JOIN
			PAYMENT_PAYMENT_CATEGORY AS b ON (a.CatID = b.CatID)
	 	WHERE a.ItemID IN ($list)
	 ";
$temp = $li->returnArray($sql,5);

$display_table="<table width=\"96%\" cellspacing=\"0\" cellpadding=\"4\" border=\"0\" align=\"center\">";
$display_table.="<tr class=\"tabletop\">
			<td class=\"tabletoplink\">$i_Payment_Field_PaymentItem</td>
			<td class=\"tabletoplink\">$i_Payment_Field_PaymentCategory</td>
			<td class=\"tabletoplink\">$i_Payment_PresetPaymentItem_PaidStudentCount</td>
			<td class=\"tabletoplink\">$i_Payment_PresetPaymentItem_UnpaidStudentCount</td></tr>";

for($i=0;$i<sizeof($temp);$i++){
	list($item_id,$item_name,$cat_name,$start_date,$end_date) = $temp[$i];
	$paid = $data[$item_id]['paid'];
	$unpaid=$data[$item_id]['unpaid'];
	
	$paid = $paid==""?0:$paid;
	$paid = $paid > 0? "<font color=red><b>$paid</b></font>":$paid;
	$unpaid = $unpaid==""?0:$unpaid;
	
	$css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
	$display_table.="<tr class=\"$css\">";
	$display_table.="<td class\"tabletext\">$item_name</td><td class\"tabletext\">$cat_name</td><td class\"tabletext\">$paid</td><td class\"tabletext\">$unpaid</td>";
	$display_table.="</tr>";
}
$display_table.="</table>";

if($allow_delete){
	$del_btn = $linterface->GET_ACTION_BTN($button_remove, "button", "submitform()");
}

$cancel_btn = $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'");

$btn_table="$del_btn$cancel_btn";


?>
<script language='javascript'>
function submitform(){
	obj = document.form1;
	if(obj!=null)
	  if(confirm(globalAlertMsg3))	      
		obj.submit();
}
</script>
<br />
<form name="form1" action="remove_update.php" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="4">
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center"><tr>
				<td class="tabletext"><?=($allow_delete?$i_Payment_Warning_Item_No_Has_Paid_Student:$i_Payment_Warning_Item_Has_Paid_Student)?></td>
			</tr></table>
		</td>
	</tr>
	<tr>
    	<td>
    		<?=$display_table?>
    	</td>
	</tr>
	<tr>
    	<td>
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
	<tr>
    	<td align="center">
    		<?=$btn_table?>
    	</td>
	</tr>
</table>
<?php
for($i=0;$i<sizeof($ItemID);$i++){
	echo "<input type=hidden name='ItemID[]' value='".$ItemID[$i]."'>\n";
}
?>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>

