<?php
// Editing by 
/*
 * 2015-08-20 (Carlos): [ip2.5.6.10.1] Changed TEMP_PAYMENT_ITEM_IMPORT to use UserID as primary key, as ClassName and ClassNumber are not unique values.
 * 2015-05-06 (Carlos): Change TEMP_PAYMENT_ITEM_IMPORT float type to decimal(13,2)
 * 2014-08-20 (Carlos): $sys_custom['ePayment']['PaymentItemChangeLog'] - added navigation tag [Payment Item Change Log]
 * 2014-03-24 (Carlos): $sys_custom['ePayment']['PaymentMethod'] - added [Payment Method]
 * 						Changed to use get_import_csv_sample.php to generate csv sample file on the fly
 * 2013-11-26 (Carlos): Added [Remark] import field
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PaymentItemSettings";

$linterface = new interface_html();

$TAGS_OBJ[] = array($i_Payment_Menu_Settings_PaymentItem, $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/",1);
$TAGS_OBJ[] = array($Lang['ePayment']['StudentPaymentItemSettings'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/student_payment_item.php",0);
if($sys_custom['ePayment']['PaymentItemChangeLog']){
	$TAGS_OBJ[] = array($Lang['ePayment']['PaymentItemChangeLog'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/change_log/",0);
}
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

// If still old table schema, need to recreate the table
$sql = "DESC TEMP_PAYMENT_ITEM_IMPORT";
$desc_result = $lpayment->returnResultSet($sql);
if($desc_result[0]['Field'] == 'ClassName'){
	$clear = 1;
}

if($clear==1){
	$sql="DROP TABLE TEMP_PAYMENT_ITEM_IMPORT";
	$lpayment->db_db_query($sql);
}

$sql = "CREATE TABLE IF NOT EXISTS TEMP_PAYMENT_ITEM_IMPORT (
                                 UserID int(11),
                                 Amount decimal(13,2),
								 Remark text ";
if($sys_custom['ePayment']['PaymentMethod']){
	$sql .= ",PaymentMethod varchar(1) ";
}
$sql .= ",SubsidyUnitID int(11) default NULL,
		SubsidyAmount decimal(13,2)";
$sql.= ") ENGINE=InnoDB charset=utf8";
$lpayment->db_db_query($sql);

$itemName = $lpayment->returnPaymentItemName($ItemID);
$count = $lpayment->returnPaymentItemStudentCount($ItemID);

$sql = "SELECT COUNT(*) FROM TEMP_PAYMENT_ITEM_IMPORT";
$temp = $lpayment->returnVector($sql);
$lcount = $temp[0];

$PAGE_NAVIGATION[] = array($itemName,'list.php?ItemID='.$ItemID);
$PAGE_NAVIGATION[] = array($button_import);

if ($failed == "1") $SysMsg = $linterface->GET_SYS_MSG("import_failed");
if ($failed == "2") $SysMsg = $linterface->GET_SYS_MSG("import_failed");

$sql = "SELECT 
            i.UnitCode,
			i.UnitName
		FROM PAYMENT_SUBSIDY_UNIT as i ORDER BY UnitCode ASC";

$rs = $lpayment->returnArray($sql);

$subsidy_content = '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="text-align: center;">
	<tbody>
		<tr class="tabletop">
			<th>'.$i_Payment_Subsidy_UnitCode.'</th>
			<th>'.$i_Payment_Subsidy_Unit.'</th>
		</tr>';
foreach($rs as $temp) {
	$subsidy_content .= '<tr>
                <td class="tabletext tablerow">'.cleanHtmlJavascript($temp['UnitCode']).'</td>
                <td class="tabletext tablerow">'.cleanHtmlJavascript($temp['UnitName']).'</td>
            </tr>';
}
$subsidy_content .= '
	</tbody>
</table>';

$tempLayer = returnRemarkLayer($subsidy_content,"hideMenu('ToolMenu2');", 500, 200);

if ($lcount != 0)
{
?>
<br />
<form name="form1" method="get" action="">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
    	<td colspan="2">
    		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
			    <tr>
					<td align="center">
						<?=$i_Payment_Import_ConfirmRemoval."<br />".$i_Payment_Import_ConfirmRemoval2?>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
    		</table>
    	</td>
    </tr>
    <tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_remove, "submit", "") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="ItemID" value="<?=$ItemID?>">
<input type="hidden" name="clear" value="1">
</form>
<br />
<?
}
else
{
?>
    <script>
        function hideMenu(menuName)
        {
            objMenu = document.getElementById(menuName);
            if(objMenu!=null)
                objMenu.style.visibility='hidden';
        }

        function showMenu2(objName,menuName)
        {
            objIMG = document.getElementById(objName);
            offsetX = (objIMG==null)?0:objIMG.width;
            offsetY =0;
            var pos_left = getPostion(objIMG,"offsetLeft");
            var pos_top  = getPostion(objIMG,"offsetTop");

            objDiv = document.getElementById(menuName);

            if(objDiv!=null){
                objDiv.style.visibility='visible';
                objDiv.style.top = pos_top+offsetY+"px";
                objDiv.style.left = pos_left+offsetX+"px";
            }
        }

    </script>
<br />
    <div id="ToolMenu2" style="position:absolute; width=0px; height=0px; visibility:hidden; z-index:999999"><?=$tempLayer?></div>
<form name="form1" method="post" action="import_confirm.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
    	<td colspan="2">
    		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
    			<tr>
    				<td colspan="2" align="right" class="tabletext">
						<?=$SysMsg?>
					</td>
    			</tr>
			    <tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $i_Payment_Field_PaymentItem ?>
					</td>
					<td class="tabletext"><?=$itemName?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $i_Payment_Payment_PaidCount ?>
					</td>
					<td class="tabletext"><?=$count[1]?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $i_Payment_Payment_UnpaidCount ?>
					</td>
					<td class="tabletext"><?=$count[0]?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $i_select_file ?>
					</td>
					<td class="tabletext">					
					<input class="file" type="file" name="userfile"><br>
					<?= $linterface->GET_IMPORT_CODING_CHKBOX() ?>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $i_general_Format ?>
					</td>
					<td class="tabletext">
						<table border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td valign="top" class="tabletext"><input type="radio" name="format" value="1" id="format1" checked></td>
								<td class="tabletext">
									<label for="format1"><?=$i_Payment_Payment_StudentImportFileDescription1?></label><br />
									<!--<a class="tablelink" href="<?= GET_CSV("sample.csv")?>" target="_blank">[<?=$i_general_clickheredownloadsample?>]</a>-->
									<a class="tablelink" href="get_import_csv_sample.php?file_type=1" target="_blank">[<?=$i_general_clickheredownloadsample?>]</a>
								</td>
							</tr>
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td valign="top" class="tabletext"><input type="radio" name="format" id="format2" value="2"></td>
								<td class="tabletext">
									<label for="format2"><?=$i_Payment_Payment_StudentImportFileDescription2?></label><br />
									<!--<a class="tablelink" href="<?= GET_CSV("sample2.csv")?>" target="_blank">[<?=$i_general_clickheredownloadsample?>]</a>-->
									<a class="tablelink" href="get_import_csv_sample.php?file_type=2" target="_blank">[<?=$i_general_clickheredownloadsample?>]</a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="tabletextremark" colspan="2"><?=$Lang['formClassMapping']['Reference']?></td>
				</tr>
				<tr>
					<td colspan="2" class="tabletextremark"><?=$i_Payment_Payment_StudentImportNotice?></td>
				</tr>
    		</table>
    	</td>
    </tr>
    <tr>
    	<td>
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
    <tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='list.php?ItemID=$ItemID'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="ItemID" value="<?=$ItemID?>">
</form>
<br />
<?
}
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
