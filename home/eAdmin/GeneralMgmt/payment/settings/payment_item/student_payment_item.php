<?php
#using: 
/********************************************* Change Log *******************************************
 * 2015-11-23 (Carlos): $sys_custom['ePayment']['PaymentItemWithPOS'] - can purchase ePOS items at this page.
 * 2015-10-27 (Carlos): Change alert and confirmation dialog with jquery alert to avoid browser blockage.
 * 2015-10-13 (Carlos): $sys_custom['ePayment']['AllowNegativeBalance'] - let not enough balance accounts to pay. 
 * 2015-04-30 (Carlos): Improved to allow add multiple add value items. 
 * 2014-08-27 (Carlos): Added [Remove] button 
 * 2014-08-20 (Carlos): $sys_custom['ePayment']['PaymentItemChangeLog'] - added navigation tag [Payment Item Change Log]
 * 2014-03-24 (Carlos): $sys_custom['ePayment']['PaymentMethod'] - Added [Payment Method].
 * 						Added quick add value function.
 * 2014-02-10 (Carlos): Updated to check balancec against amount to pay
 * 2014-02-04 (Tiffany): Created
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_student_page_size", $numPerPage, 0, "", "", 0);
    $ck_student_page_size = $numPerPage;
}
# preserve table view
if ($ck_student_payment_item_setting_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_student_payment_item_setting_page_number", $pageNo, 0, "", "", 0);
	$ck_student_payment_item_setting_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_student_payment_item_setting_page_number!="")
{
	$pageNo = $ck_student_payment_item_setting_page_number;
}

if ($ck_student_payment_item_setting_page_order!=$order && $order!="")
{
	setcookie("ck_student_payment_item_setting_page_order", $order, 0, "", "", 0);
	$ck_student_payment_item_setting_page_order = $order;
} else if (!isset($order) && $ck_student_payment_item_setting_page_order!="")
{
	$order = $ck_student_payment_item_setting_page_order;
}

if ($ck_student_payment_item_setting_page_field!=$field && $field!="")
{
	setcookie("ck_student_payment_item_setting_page_field", $field, 0, "", "", 0);
	$ck_student_payment_item_setting_page_field = $field;
} else if (!isset($field) && $ck_student_payment_item_setting_page_field!="")
{
	$field = $ck_student_payment_item_setting_page_field;
}

intranet_auth();
intranet_opendb();


if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();

$is_KIS_hide_balance = $_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance'];

#----------choose student
$lclass = new libclass();
$select_class = $lclass->getSelectClassID(" name=\"targetClass\" id=\"targetClass\" onchange=\"classSelectionChanged()\" ",$targetClass,1);
//if($targetClass_old!=$targetClass)$targetID="";
if (isset($targetClass) && $targetClass != "")
{
	$student_list=$lclass->returnStudentListByClass($lclass->getClassName($targetClass)) ;
	$select_students = getSelectByArray($student_list, 'name="targetID" id="targetID" onChange="this.form.action=\'\';this.form.submit()" ', $targetID, 0, 0, "",1);
    $show_students="<tr><td class='tabletext formfieldtitle' valign='top' width='30%'>".$Lang['ePayment']['Student']."</td><td>".$select_students."</td></tr>";
}


//$targetClass_old=$targetClass;
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PaymentItemSettings";

// admin in charge name field
$Inchargenamefield = getNameFieldByLang("e.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$Inchargearchive_namefield = " IF(f.ChineseName IS NULL,f.EnglishName,f.ChineseName)";
	//$archive_namefield="c.ChineseName";
}else {
	//$archive_namefield ="c.EnglishName";
	$Inchargearchive_namefield = "IF(f.EnglishName IS NULL,f.ChineseName,f.EnglishName)";
}

#----------


if (isset($ck_student_page_size) && $ck_student_page_size != "") $page_size = $ck_student_page_size;
$pageSizeChangeEnabled = true;

$linterface = new interface_html();


$sql  = "select 
	      CONCAT(b.name,
	      CASE 
         		WHEN b.NoticeID IS NOT NULL OR b.NoticeID <> '' then CONCAT('<a title=\"".$Lang['ePayment']['ViewNotice']."\" href=\"#\" onclick=\"viewNotice(',b.NoticeID,'); return false;\"><img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" border=0></a>') 
         		ELSE '&nbsp;'
          END) as ItemName,		  	
          c.name as CategoryName,
          a.Amount,
          b.DisplayOrder,
          b.PayPriority,  
          b.Description,
          ";
		 // IF((e.UserID IS NULL AND f.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$Inchargearchive_namefield,'</i>'), IF(e.UserID IS NULL AND f.UserID IS NULL,IF(b.ProcessingAdminUser IS NULL OR b.ProcessingAdminUser = '','&nbsp;',b.ProcessingAdminUser),$Inchargenamefield)) as DisplayAdminInCharge,
$sql .=	" DATE_FORMAT(b.startdate,'%Y-%m-%d') as StartDate,
          DATE_FORMAT(b.enddate,'%Y-%m-%d') as EndDate, ";
if($sys_custom['ePayment']['PaymentMethod']){
	$sql.=" CASE a.PaymentMethod ";
	foreach($sys_custom['ePayment']['PaymentMethodItems'] as $key => $val)
	{
		$sql .= " WHEN '$key' THEN '$val' ";	
			/*	WHEN '1' THEN '".$sys_custom['ePayment']['PaymentMethodItems'][1]."' 
				WHEN '2' THEN '".$sys_custom['ePayment']['PaymentMethodItems'][2]."' 
				WHEN '3' THEN '".$sys_custom['ePayment']['PaymentMethodItems'][3]."' */
	}
	$sql .= " ELSE '".$Lang['ePayment']['NA']."' 
			END as PaymentMethod,";
}
$sql.= " a.PaymentID 
		 from   
		  PAYMENT_PAYMENT_ITEMSTUDENT as a 
          INNER JOIN PAYMENT_PAYMENT_ITEM as b on a.itemid=b.itemid
          LEFT JOIN PAYMENT_PAYMENT_CATEGORY as c on b.catid=c.catid 
          LEFT JOIN INTRANET_NOTICE as d on d.noticeid=b.noticeid
          left join INTRANET_USER as e on b.ProcessingAdminUser = e.UserID
          LEFT JOIN INTRANET_ARCHIVE_USER as f on b.ProcessingAdminUser = f.UserID
		 where 
          a.studentid='".$targetID."' 
          and a.paidtime is null
          and (a.RecordStatus=0 OR a.RecordStatus IS NULL) 
        order by b.DisplayOrder,b.PayPriority";
# TABLE INFO
/*
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("b.name",
												 "c.name",
												 "a.Amount",
												 "b.DisplayOrder",
												 "b.PayPriority",
												 "DisplayAdminInCharge",
												 "b.startdate",
												 "b.enddate"																							 												
												 );
												 
											 
												 
												 
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+3;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0,0);
$li->IsColOff = 2;

if ($field=="" && $order=="") {
	$li->field = 3;
	$li->order = 1;
}




// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='15%'>".$li->column($pos++, $i_Payment_Field_PaymentItem)."</td>\n";
$li->column_list .= "<td width='1' class='tabletoplink'>&nbsp;</td>\n";
$li->column_list .= "<td width='15%'>".$li->column($pos++, $i_Payment_Field_PaymentCategory)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $Lang['ePayment']['Amount'])."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_Payment_Field_DisplayOrder)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_Payment_Field_PayPriority)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column($pos++, $i_general_description)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_general_startdate)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_general_enddate)."</td>\n";
$li->column_list .= "<td width='1'>".$li->check("PaymentID[]")."</td>\n";
*/

$toolbar .= $linterface->GET_LNK_EXPORT("javascript:exportPage(document.form1,'student_item_export.php');","","","","",0);
$toolbar .= $linterface->GET_LNK_PRINT("javascript:openPrintPage();","","","","",0);

//$filterbar .="$select_item_status&nbsp;";
//$filterbar .= "$select_item_paid_status";
//$filterbar .= "$select_cat";
	
	$table_tool .= "<td nowrap=\"nowrap\">
						<a href=\"javascript:void(0);\" onclick=\"checkPay(document.form1,'PaymentID[]','student_pay_item.php','".$Lang['ePayment']['ConfirmForceStudentToPay']."','ItemAmount[]','".$Lang['ePayment']['NotEnoughBalanceToPay']."');\" class=\"tabletool\">
							<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_pay.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
							$button_pay
						</a>
					</td>";
	$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";	
	
	
//$TAGS_OBJ[] = array($i_Payment_Menu_Settings_PaymentItem, "", 0);
$TAGS_OBJ[] = array($i_Payment_Menu_Settings_PaymentItem, $PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/",0);
$TAGS_OBJ[] = array($Lang['ePayment']['StudentPaymentItemSettings'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/student_payment_item.php",1);
if($sys_custom['ePayment']['PaymentItemChangeLog']){
	$TAGS_OBJ[] = array($Lang['ePayment']['PaymentItemChangeLog'],$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/payment/settings/payment_item/change_log/",0);
}

$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();

$Msg = urldecode($Msg);
if($targetClass!="" && $targetID !=""){
	
	if(!$is_KIS_hide_balance) {
		$sql_getbalance = "SELECT Balance FROM PAYMENT_ACCOUNT WHERE StudentID='$targetID'";	
		$balanceRecord = $lpayment->returnVector($sql_getbalance);	
		$currentBalance = $balanceRecord[0];

		if($lpayment->isEWalletDirectPayEnabled() == false) {
			$text_addvalue_btn = $Lang['ePayment']['AddValue'];
		} else {
			$text_addvalue_btn = $Lang['ePayment']['AccountRevenue'];
		}
		$display_addvalue_btn = $linterface->GET_SMALL_BTN($text_addvalue_btn, "button", 'toggleDisplayAddvalue(true);', "DisplayAddValueBtn", "", "", $Lang['ePayment']['AddValue']);
		$current_balance_row = '<tr><td class="tabletext formfieldtitle" valign="top" width="30%">'.$i_Payment_Field_CurrentBalance.'</td><td><span id="CurrentBalanceSpan">'.$lpayment->getWebDisplayAmountFormat($currentBalance).'</span>&nbsp;'.$display_addvalue_btn.'</td></tr>';
	
		$extra_js = 'var student_balance = '.sprintf("%.2f",$currentBalance).';'."\n";
		$extra_js .=  'var raw_student_balance = '.sprintf("%.2f",$currentBalance).';'."\n";
		
		$addvalue_row = '<tr id="AddValueRow" style="display:none;">
							<td class="tabletext formfieldtitle" valign="top" width="30%">'.$text_addvalue_btn.'</td>';
			$addvalue_row.= '<td>';
				
				$addvalue_row.= '<table class="common_table_list" id="AddValueTable">
									<thead>
										<tr>
											<th>'.$i_Payment_Field_Amount.'</th>
											<th>'.$i_Payment_Field_RefCode.'</th>
											<th>'.$i_Payment_CashDepositDate.'</th>
											<th>'.$i_Payment_CashDepositTime.'</th>
											<th><span class="table_row_tool"><a class="add" href="javascript:void(0);" onclick="addAddValueRow();" title="'.$Lang['Btn']['Add'].'"></a></span></th>
										</tr>
									</thead>
									<tbody>';
					/*
					$addvalue_row .= '<tr>
											<td>'.$linterface->GET_TEXTBOX_NUMBER('AddValueAmount', 'AddValueAmount', '', '', array('onchange'=>'onChangeAddValueAmount(this);')).'</td>
											<td>'.$linterface->GET_TEXTBOX('AddValueRef', 'AddValueRef', '').'</td>
											<td>'.$linterface->GET_DATE_PICKER("AddValueDate",date("Y-m-d")).'</td>
											<td>'.$linterface->Get_Time_Selection("AddValueTime", date("H:i:s"), '').'</td>
											<td>&nbsp;</td>
										</tr>';
					*/
					$addvalue_row .= '</tbody>
									</table>
									<input type="hidden" id="AddValueRowCount" name="AddValueRowCount" value="-1" />';
				/*
				$addvalue_row.= '<table width="100%">
										<tr>
											<td class="dotline"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1" /></td>
										</tr>
										<tr>
											<td align="center">
												'.$linterface->GET_ACTION_BTN($button_submit, "button",'addvalueSubmit();',"addvalue_submit").'
												'.$linterface->GET_ACTION_BTN($button_cancel, "button", 'toggleDisplayAddvalue(false);').'
											</td>
										</tr>
									</table>';
				*/					
			$addvalue_row.= '</td>
							</tr>';
	}
	
/*	
$show_table="<table width='96%' border='0' cellpadding='0' cellspacing='0' align='center'>
	<tr>
		<td class='tabletext' lign='right' colspan='2'>&nbsp;</td>
	</tr>
	<tr>
		<td align='left'>".$toolbar."</td>
		<td valign='bottom' align='right'>
			<table border='0' cellspacing='0' cellpadding='0'>
				<tr>
					<td width='21'><img src='"."{$image_path}/{$LAYOUT_SKIN}"."/table_tool_01.gif' width='21' height='23'></td>
					<td background='". "{$image_path}/{$LAYOUT_SKIN}" ."/table_tool_02.gif'>
						<table border='0' cellspacing='0' cellpadding='2'>
							<tr>
								".$table_tool."
							</tr>
						</table>
					</td>
					<td width='6'><img src='". "{$image_path}/{$LAYOUT_SKIN}" ."/table_tool_03.gif' width='6' height='23'></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
". $li->display("96%")."
<table width='96%' border='0' cellpadding='0' cellspacing='0' align='center'>
<tr>
	<td align='left'>
		<img src='".$image_path."/".$LAYOUT_SKIN."/icon_view.gif' border=0> 
		<span class='tabletextremark'> - ".$Lang['ePayment']['PaymentItemGenerateFromNotice']."</span> 
	</td>
</tr>
</table>";
*/

$records = $lpayment->returnArray($sql);
$record_count = count($records);
$selected_amount = 0;

$table .= '<table class="common_table_list_v30" align="center" width="96%" style="width:96%;">';
	$table.= '<thead>';
	$table.= '<tr>';
		$table .= '<th class="num_check">#</th>';
		$table .= '<th style="width:15%">'.$i_Payment_Field_PaymentItem.'</th>';
		$table .= '<th style="width:10%">'.$i_Payment_Field_PaymentCategory.'</th>';
		$table .= '<th style="width:10%">'.$Lang['ePayment']['Amount'].'</th>';
		$table .= '<th style="width:5%">'.$i_Payment_Field_DisplayOrder.'</th>';
		$table .= '<th style="width:5%">'.$i_Payment_Field_PayPriority.'</th>';
		$table .= '<th style="width:15%">'.$i_general_description.'</th>';
		$table .= '<th style="width:15%">'.$i_general_startdate.'</th>';
		$table .= '<th style="width:15%">'.$i_general_enddate.'</th>';
		if($sys_custom['ePayment']['PaymentMethod']){
			$table .= '<th style="width:5%">'.$Lang['ePayment']['PaymentMethod'].'</th>';
			//$table .= '<th style="width:10%">'.$Lang['General']['Remark'].'</th>';
		}
		$table .= '<th class="num_check"><input type="checkbox" name="checkmaster" onclick="(this.checked)?setChecked(1,this.form,\'PaymentID[]\'):setChecked(0,this.form,\'PaymentID[]\');updatePayAmount();"></th>';
	$table.= '</tr>';
	$table.= '</thead>';
$table.= '<tbody>';
$column_count = $records[0];
$total_amount = 0;
$hidden_fields = '';
for($i=0;$i<$record_count;$i++) {
	$table .= '<tr>';
		$table .= '<td>'.($i+1).'</td>';
		$table .= '<td>'.$records[$i][0].'&nbsp;</td>';
		$table .= '<td>'.$records[$i][1].'&nbsp;</td>';
		$table .= '<td>'.$lpayment->getWebDisplayAmountFormat($records[$i][2]).'</td>';
		$table .= '<td>'.$records[$i][3].'&nbsp;</td>';
		$table .= '<td>'.$records[$i][4].'&nbsp;</td>';
		$table .= '<td>'.$records[$i][5].'&nbsp;</td>';
		$table .= '<td>'.$records[$i][6].'&nbsp;</td>';
		$table .= '<td>'.$records[$i][7].'&nbsp;</td>';
		if($sys_custom['ePayment']['PaymentMethod']){
			$table .= '<td>'.$records[$i][8].'&nbsp;</td>';
			//$table .= '<td>'.Get_String_Display($records[$i][9],0,'&nbsp;').'</td>';
		}
		$table .= '<td><input type="checkbox" name="PaymentID[]" value="'.$records[$i]['PaymentID'].'" onclick="updatePayAmount();" '.(is_array($PaymentID) && in_array($records[$i]['PaymentID'],$PaymentID)? 'checked':'').'></td>';
	$table .= '</tr>';
	$hidden_fields .= '<input type="hidden" name="ItemAmount[]" value="'.$records[$i][2].'" />';
	if(is_array($PaymentID) && in_array($records[$i]['PaymentID'],$PaymentID)){
		$selected_amount += $records[$i]['Amount'];
	}
	$total_amount += $records[$i]['Amount'];
}
if($record_count == 0){
	$table .= '<tr><td colspan="11" style="text-align:center;" class="tabletext tablerow">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>';
}else{
	//$table .= '<tr class="total_row"><th colspan="3">'.$Lang['ePayment']['Chargeable_Amount'].' / '.$Lang['ePayment']['Total_Chargeable_Amount'].'</th><td colspan="9" id="TdPayAmount">'.$lpayment->getWebDisplayAmountFormat($selected_amount).' / '.$lpayment->getWebDisplayAmountFormat($total_amount).'</td></tr>';
}
$table.= '</tbody>';

if($sys_custom['ePayment']['PaymentItemWithPOS'])
{
	// ePOS items
	$table.= '<tbody><tr><td colspan="12" style="background:#CCCCCC;">&nbsp;</td></tr></tbody>';
	$table.= '<thead>';
		$table.= '<tr>';
			$table .= '<th class="num_check">#</th>';
			$table .= '<th style="width:15%">'.$Lang['Header']['Menu']['ePOS'].' '.$Lang['ePOS']['Category'].'</th>';
			$table .= '<th style="width:10%">'.$Lang['Header']['Menu']['ePOS'].' '.$Lang['ePOS']['Item'].'</th>';
			$table .= '<th style="width:10%">'.$Lang['ePayment']['Amount'].'</th>';
			$table .= '<th style="width:5%">'.$Lang['ePOS']['UnitPrice'].'</th>';
			$table .= '<th style="width:15%">'.$Lang['ePOS']['Barcode'].'</th>';
			$table .= '<th style="width:45%" colspan="'.($sys_custom['ePayment']['PaymentMethod']?4:3).'">'.$Lang['ePayment']['Quantity'].'</th>';
			//$table .= '<th style="width:15%">'.'&nbsp;'.'</th>';
			//$table .= '<th style="width:15%">'.'&nbsp;'.'</th>';
			//if($sys_custom['ePayment']['PaymentMethod']){
			//	$table .= '<th style="width:5%">'.'&nbsp;'.'</th>';
			//}
			$table .= '<th class="num_check">';
				$table .= '';
			$table .= '</th>';
		$table.= '</tr>';
	$table.= '</thead>';
	$table .= '<tbody>';
		$table .= '<tr class="RowTool"><td colspan="11"><div class="table_row_tool row_content_tool"><a href="javascript:void(0);" onclick="addPOSItemRow();" title="Add ePOS Item" class="add_dim"></a></div></td></tr>';
	$table .= '</tbody>';
	$table.= '<tbody><tr><td colspan="12" style="background:#CCCCCC;">&nbsp;</td></tr></tbody>';
}

if($record_count > 0){
	$table .= '<tbody>';
		$table .= '<tr class="total_row"><th colspan="3">'.$Lang['ePayment']['Chargeable_Amount'].' / '.$Lang['ePayment']['Total_Chargeable_Amount'].'</th><td colspan="9" id="TdPayAmount">'.$lpayment->getWebDisplayAmountFormat($selected_amount).' / '.$lpayment->getWebDisplayAmountFormat($total_amount).'</td></tr>';
	$table .= '</tbody>';
}

$table.= '</table>';

$show_table="<table width='96%' border='0' cellpadding='0' cellspacing='0' align='center'>
	<tr>
		<td class='tabletext' lign='right' colspan='2'>&nbsp;</td>
	</tr>
	<tr>
		<td align='left' colspan='2'>".$toolbar."</td>";
/*
$show_table.="<td valign='bottom' align='right'>
				<table border='0' cellspacing='0' cellpadding='0'>
					<tr>
						<td width='21'><img src='"."{$image_path}/{$LAYOUT_SKIN}"."/table_tool_01.gif' width='21' height='23'></td>
						<td background='". "{$image_path}/{$LAYOUT_SKIN}" ."/table_tool_02.gif'>
							<table border='0' cellspacing='0' cellpadding='2'>
								<tr>
									".$table_tool."
								</tr>
							</table>
						</td>
						<td width='6'><img src='". "{$image_path}/{$LAYOUT_SKIN}" ."/table_tool_03.gif' width='6' height='23'></td>
					</tr>
				</table>
			</td>";
*/
$show_table.="</tr>
	</table>
".$table."
<table width='96%' border='0' cellpadding='0' cellspacing='0' align='center'>
<tr>
	<td align='left'>
		<img src='".$image_path."/".$LAYOUT_SKIN."/icon_view.gif' border=0> 
		<span class='tabletextremark'> - ".$Lang['ePayment']['PaymentItemGenerateFromNotice']."</span> 
	</td>
</tr>
</table>";
        
        
}
if($Lang['ePayment'][$Msg] != ''){
	$Msg = $Lang['ePayment'][$Msg];
}
$linterface->LAYOUT_START($Msg);

//if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
?>
<style type="text/css">
tr.selected_row {background-color: #5cb85c;}
</style>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.css" type="text/css" />
<link href="/templates/jquery/jquery.alerts.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="/templates/jquery/jquery.alerts.js"></script>

<script language="javascript">
<?=$extra_js?>

function viewNotice(id)
{
<?
	if ($intranet_version == "2.5" || $intranet_version == "3.0") {
		$PathToNotice = '/home/eService/notice/sign.php';
	}
	else {
		$PathToNotice = '/home/notice/sign.php';
	}
?>
	newWindow('<?=$PathToNotice?>?NoticeID='+id,10);
}

function checkForm(formObj) {
	if(formObj==null) return false;
	fromV = formObj.FromDate;
	toV= formObj.ToDate;
	if(!checkDate(fromV)){
		return false;
	} else if(!checkDate(toV)) {
		return false;
	}
	return true;
}
function checkDate(obj){
	if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
	return true;
}
function submitForm(obj){
	if(checkForm(obj))
		obj.submit();	
}
function checkRemove2(obj,element,page){
	if(countChecked(obj,element)==0)
		jAlert(globalAlertMsg2);
	else {
            obj.action=page;                
            obj.method="POST";
            obj.submit();				             
    }
}

$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
$(document).ready(function(){ 
	$('input#FromDate').datepick({
		dateFormat: 'yy-mm-dd',
		dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		changeFirstDay: false,
		firstDay: 0
		});
	$('input#ToDate').datepick({
		dateFormat: 'yy-mm-dd',
		dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		changeFirstDay: false,
		firstDay: 0
		});
});

function openPrintPage()
{
	 q_str ="targetClass=<?=$targetClass?>&targetID=<?=$targetID?>&pageNo=<?=$li->pageNo?>&order=<?=$li->order?>&field=<?=$li->field?>&numPerPage=<?=$li->page_size?>";
	 url="student_item_print.php?"+q_str; 
     newWindow(url,8);
}
function exportPage(obj,url){
		old_url = obj.action;
        obj.action=url;
        obj.submit();
        obj.action = old_url;
        
}
function checkArchive(obj,element,page){
  if(countChecked(obj,element)==0)
          alert(globalAlertMsg2);
  else{
    if(confirm('<?=$i_Payment_Menu_Settings_PaymentItem_Archive_Warning?>')){
          obj.action=page;                
          obj.method="POST";
          obj.submit();	
    }			             
  }
}
function checkUndoArchive(obj,element,page){
  if(countChecked(obj,element)==0)
          alert(globalAlertMsg2);
  else{
    if(confirm('<?=$i_Payment_Menu_Settings_PaymentItem_UndoArchive_Warning?>')){
          obj.action=page;                
          obj.method="POST";
          obj.submit();	
      }			             
  }	
}

function classSelectionChanged()
{
	var student_selection = $('#targetID');
	if(student_selection.length > 0){
		student_selection.val('');
	}
	document.form1.submit();
}

function checkPay(obj,element,page,msg,amount_element,unablepay_msg)
{
	var pay_total = 0;
<?php if($sys_custom['ePayment']['PaymentItemWithPOS']){ ?>
	var pos_items = document.getElementsByName('RowItemID[]');
	var pos_quantities = document.getElementsByName('RowQuantity[]');
	var pos_amount = document.getElementsByName('RowAmount[]');
	var payment_items = document.getElementsByName('PaymentID[]');
	var item_count = 0;
	for(var i=0;i<payment_items.length;i++){
		if(payment_items[i].checked){
			item_count+=1;
		}
	}
	for(var i=0;i<pos_items.length;i++){
		var purchased_quantity = pos_quantities[i].options[pos_quantities[i].selectedIndex].value;
		if(pos_items[i].options[pos_items[i].selectedIndex].value != '' && purchased_quantity != '' && parseFloat(purchased_quantity)>0)
		{
			item_count += 1;
			pay_total += parseFloat(pos_amount[i].value);
		}
	}
	pay_total = pay_total.toFixed(2);
	if(item_count == 0){
		jAlert(globalAlertMsg2);
	}
<?php }else{ // not $sys_custom['ePayment']['PaymentItemWithPOS']  ?>
	if(countChecked(obj,element)==0){
    	jAlert(globalAlertMsg2);
	}
<?php } ?>
	else{
    <?php if(!$is_KIS_hide_balance){ ?>
    	if(!checkAddValue()){
    		return false;
    	}
    	var checkboxElements = document.getElementsByName(element);
    	var amountElements = document.getElementsByName(amount_element);
    	//var pay_total = 0;
        pay_total = parseFloat(pay_total);
    	for(var i=0;i<checkboxElements.length;i++){
    		if(checkboxElements[i].checked){
    			pay_total += parseFloat(amountElements[i].value);
    		}
    	}

    	pay_total = parseFloat(pay_total).toFixed(2);
    	<?php if(!$sys_custom['ePayment']['AllowNegativeBalance']){ ?>
    	if(student_balance < pay_total){
    		jAlert(unablepay_msg);
    		return false;
    	}
    	<?php } ?>

		<?php if($lpayment->isEWalletDirectPayEnabled()) { ?>
        var rows = $('.AddValueTableRow');
        if(rows.length > 0) {
            var total_amount_input = 0;
            for (var i = 0; i < rows.length; i++) {
                var row_index = $(rows[i]).find('input')[0].name.replace('AddValueAmount_', '');

                var str_amount = $.trim($('#AddValueAmount_' + row_index).val());
                var amount = parseFloat(str_amount);
                total_amount_input += amount;
            }
            total_amount_input = parseFloat(total_amount_input).toFixed(2);
            if (pay_total != total_amount_input) {
                jAlert('<?=$Lang['ePayment']['AddValueAndPayTotalError']?>');
                return false;
            }
        }

		<?php } ?>

    <?php } ?>	
    	/*
        if(confirm(msg)){
	        obj.action=page;
	        obj.method="POST";
	        obj.submit();
        }
        */
        jConfirm(msg, '<?=$Lang['Btn']['Confirm']?>', function(choice){
        	if(choice){
        		obj.action=page;
	        	obj.method="POST";
	        	obj.submit();
        	}
        });
    }
}

function checkRemoveItems(obj,element,page)
{
	if(countChecked(obj,element)==0)
    	jAlert(globalAlertMsg2);
    else{
    	/*
    	if(confirm(globalAlertMsg3)){
    		$(obj).append('<input type="hidden" name="back_to_url" value="student_payment_item.php" />');
	        obj.action=page;
	        obj.method="POST";
	        obj.submit();
        }
        */
        jConfirm(globalAlertMsg3,'<?=$Lang['Btn']['Confirm']?>',function(choice){
        	if(choice){
        		$(obj).append('<input type="hidden" name="back_to_url" value="student_payment_item.php" />');
		        obj.action=page;
		        obj.method="POST";
		        obj.submit();
        	}
        });
    }
}

Number.prototype.formatMoney = function(decPlaces, thouSeparator, decSeparator, currencySymbol){
    // check the args and supply defaults:
    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces;
    decSeparator = decSeparator == undefined ? "." : decSeparator;
    thouSeparator = thouSeparator == undefined ? "," : thouSeparator;
    currencySymbol = currencySymbol == undefined ? "$" : currencySymbol;

    var n = this,
        sign = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;

    return sign + currencySymbol + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
}

function updatePayAmount()
{
	var paymentElements = document.getElementsByName('PaymentID[]');
	var amountElements = document.getElementsByName('ItemAmount[]');
	var td = $('#TdPayAmount');
	var total_amount = 0;
	var total_pay = 0;
	
	for(var i=0;i<paymentElements.length;i++){
		var amount = parseFloat(amountElements[i].value);
		total_amount += amount;
		if(paymentElements[i].checked){
			total_pay += amount;
			$(paymentElements[i]).closest('tr').addClass('selected_row');
		}else{
			$(paymentElements[i]).closest('tr').removeClass('selected_row');
		}
	}
	
<?php if($sys_custom['ePayment']['PaymentItemWithPOS']){ ?>
	var pos_item_amounts = document.getElementsByName('RowAmount[]');
	for(var i=0;i<pos_item_amounts.length;i++){
		var t_amount = parseFloat(pos_item_amounts[i].value);
		total_amount += t_amount;
		total_pay += t_amount;
	}
<?php } ?>
	total_amount = total_amount.toFixed(2);
	total_pay = total_pay.toFixed(2);
	var display_pay_amount = (new Number(total_pay)).formatMoney(2,',','.','$');
	<?php if(!$is_KIS_hide_balance && !$sys_custom['ePayment']['AllowNegativeBalance']){ ?>
	if(total_pay > student_balance) {
		display_pay_amount = '<span style="color:red">' + display_pay_amount + '</span>';
	}
	<?php } ?>
	if(td.length > 0){
		td.html(display_pay_amount + ' / ' + (new Number(total_amount)).formatMoney(2,',','.','$'));
	}
}

function toggleDisplayAddvalue(isOn)
{
	/*
	$('#AddValueAmount').val('');
	$('#AddValueRef').val('');
	
	var d = new Date();
	var year = d.getFullYear().toString();
	var month = (d.getMonth()+1).toString();
	var day = d.getDate().toString();
	var hour = d.getHours();
	var min = d.getMinutes();
	var sec = d.getSeconds();
	
	var date = year + '-' + (month.length<=1?'0'+month:month) + '-' + (day.length<=1?'0'+day:day);
	$('#AddValueDate').val(date);
	$('#AddValueTime_hour').val(hour);
	$('#AddValueTime_min').val(min);
	$('#AddValueTime_sec').val(sec);
	*/
	if(isOn){
		addAddValueRow();
		$('#AddValueRow').show();
		$('#DisplayAddValueBtn').hide();
	}else{
		$('#AddValueRow').hide();
		$('#DisplayAddValueBtn').show();
	}
}
/*
function addvalueSubmit()
{
	var amount = parseFloat($.trim($('#AddValueAmount').val()));
	var ref_code = $.trim($('#AddValueRef').val());
	var today = getToday();
	var addvalue_date = $.trim($('#AddValueDate').val());
	var is_date_valid = $('#DPWL-AddValueDate').html() == '';
	var addvalue_hour = $('#AddValueTime_hour').val();
	var addvalue_min = $('#AddValueTime_min').val();
	var addvalue_sec = $('#AddValueTime_sec').val();
	var addvalue_time = (addvalue_hour.length<=1? '0'+addvalue_hour : addvalue_hour);
	addvalue_time += ':' + (addvalue_min.length<=1? '0'+addvalue_min : addvalue_min);
	addvalue_time += ':' + (addvalue_sec.length<=1? '0'+addvalue_sec : addvalue_sec); 
	
	if(amount == '' || amount <= 0 || isNaN(amount)){
		alert('<?=$i_Payment_DepositAmountWarning?>');
		return;
	}
	
	if(!is_date_valid){
		alert('<?=$i_invalid_date?>');
		return;
	}
	
	if(addvalue_date > today){
		alert('<?=$i_invalid_date?>');
		return;
	}
	
	Block_Document();
	$.post(
		'ajax_addvalue.php',
		{
			'StudentID': $('#targetID').val(),
			'Amount':amount,
			'RefCode':encodeURIComponent(ref_code),
			'TransactionTime': addvalue_date + ' ' + addvalue_time  
		},
		function(ReturnData){
			if(ReturnData == "1"){
				Get_Return_Message('<?=$Lang['ePayment']['ManualCashInputSuccess']?>');
				student_balance += amount;
				$('#CurrentBalanceSpan').html((new Number(student_balance)).formatMoney(2,',','.','$'));
			}else{
				Get_Return_Message('<?=$Lang['ePayment']['ManualCashInputUnsuccess']?>');
			}
			toggleDisplayAddvalue(false);
			UnBlock_Document();
		}
	);
}
*/

function onChangeAddValueAmount(inputObj)
{
	var str_amount = $.trim($(inputObj).val());
	var amount = parseFloat(str_amount);
	
	if(str_amount == ''){
		student_balance = raw_student_balance;
		return;
	}
	if(amount <= 0 || (str_amount != '' && isNaN(amount))){
		$(inputObj).val('');
		student_balance = raw_student_balance;
		return;
	}
	student_balance = raw_student_balance + amount;
	updatePayAmount();
}

function checkAddValue()
{
	var rows = $('.AddValueTableRow');
	var today = getToday();
	var is_all_valid = true;
	for(var i=0;i<rows.length;i++)
	{
		var row_index = $(rows[i]).find('input')[0].name.replace('AddValueAmount_','');
		
		$('#WarnAddValueAmount_'+row_index).hide();
		$('#WarnAddValueDate_'+row_index).hide();
		
		var str_amount = $.trim($('#AddValueAmount_'+row_index).val());
		var amount = parseFloat(str_amount);
		var ref_code = $.trim($('#AddValueRef_'+row_index).val());
		var addvalue_date = $.trim($('#AddValueDate_'+row_index).val());
		var is_date_valid = $('#DPWL-AddValueDate_'+row_index).html() == '';
		var addvalue_hour = $('#AddValueTime_'+row_index+'_hour').val();
		var addvalue_min = $('#AddValueTime_'+row_index+'_min').val();
		var addvalue_sec = $('#AddValueTime_'+row_index+'_sec').val();
		var addvalue_time = (addvalue_hour.length<=1? '0'+addvalue_hour : addvalue_hour);
		addvalue_time += ':' + (addvalue_min.length<=1? '0'+addvalue_min : addvalue_min);
		addvalue_time += ':' + (addvalue_sec.length<=1? '0'+addvalue_sec : addvalue_sec); 
		
		if(str_amount == ''){
			continue; // ignore this row
		}
		
		if(amount <= 0 || (str_amount!='' && isNaN(amount))){
			$('#WarnAddValueAmount_'+row_index).show();
			is_all_valid = false;
			$('#AddValueAmount_'+row_index).focus();
		}
		
		if(!is_date_valid){
			$('#WarnAddValueDate_'+row_index).show();
			is_all_valid = false;
		}
		
		if(addvalue_date > today){
			$('#WarnAddValueDate_'+row_index).show();
			is_all_valid = false;
		}
	}
	if(!is_all_valid){
		Scroll_To_Top();
	}
	return is_all_valid;
}

function recalculateAddValues()
{
	student_balance = raw_student_balance;
	var rows = $('.AddValueTableRow');
	for(var i=0;i<rows.length;i++){
		var inputs = $(rows[i]).find('input');
		var str_amount = $.trim(inputs[0].value);
		var amount = parseFloat(str_amount);
		
		if(str_amount == ''){
			continue;
		}
		if(amount <= 0 || (str_amount != '' && isNaN(amount))){
			inputs[0].value = '';
			continue;
		}
		student_balance += amount;
	}
	updatePayAmount();
}

function addAddValueRow()
{
	var row_count = parseInt($('#AddValueRowCount').val()) + 1;
	$('#AddValueRowCount').val(row_count);
	$.get(
		'ajax_get_addvalue_row.php',
		{
			'row_index': row_count
		},function(data){
			$('table#AddValueTable tbody').append(data);
		}
	);
}

function removeAddValueRow(obj)
{
	$(obj).closest('.AddValueTableRow').remove();
	recalculateAddValues();
	var num_row = $('.AddValueTableRow').length;
	if(num_row == 0)
	{
		toggleDisplayAddvalue(false);
	}
}

<?php if($sys_custom['ePayment']['PaymentItemWithPOS']){ ?>
function deletePurchasedItem(rowNum)
{
	var rows = $('tr.ItemRow');
	if(rows.length > 0 && rowNum-1 < rows.length){
		$(rows[rowNum-1]).remove();
	}
	var row_nums = $('td.RowNumber');
	for(var i=0;i<row_nums.length;i++){
		$(row_nums[i]).html(i+1);
	};
	
	updatePayAmount();
}

function addPOSItemRow()
{
	var next_row_num = $('tr.ItemRow').length+1;
	$.post(
		'ajax_get_pos_item_table_row.php',
		{
			'RowNum': next_row_num
			 
		},
		function(returnHtml){
			$('tr.RowTool').before(returnHtml);
			var row_nums = $('td.RowNumber');
			for(var i=0;i<row_nums.length;i++){
				$(row_nums[i]).html(i+1);
			};
			updatePayAmount();
		}
	);
}

function updatePOSItemRow(row_num, cat_id, item_id, quantity)
{
	var rows = $('tr.ItemRow');
	$.post(
		'ajax_get_pos_item_table_row.php',
		{
			'RowNum': row_num,
			'RowCategoryID': cat_id,
			'RowItemID': item_id,
			'RowQuantity': quantity
		},
		function(returnHtml){
			$(rows[row_num-1]).replaceWith(returnHtml);
			//var row_nums = $('td.RowNumber');
			//for(var i=0;i<row_nums.length;i++){
			//	$(row_nums[i]).html(i+1);
			//};
			updatePayAmount();
		}
	);
}

function rowCategorySelectionChanged(obj)
{
	var row_num = $(obj).closest('tr').find('td.RowNumber').html();
	var cat_id = $(obj).val();
	updatePOSItemRow(row_num, cat_id, '', '');
}

function rowItemSelectionChanged(obj)
{
	var row_num = $(obj).closest('tr').find('td.RowNumber').html();
	var cat_id = $(obj).closest('tr').find('select[name="RowCategoryID[]"]').val();
	var item_id = $(obj).val();
	updatePOSItemRow(row_num, cat_id, item_id, '');
}

function rowQuantitySelectionChanged(obj)
{
	var row_num = $(obj).closest('tr').find('td.RowNumber').html();
	var cat_id = $(obj).closest('tr').find('select[name="RowCategoryID[]"]').val();
	var item_id = $(obj).closest('tr').find('select[name="RowItemID[]"]').val();
	var quantity = $(obj).val();
	updatePOSItemRow(row_num, cat_id, item_id, quantity);
}

<?php } // $sys_custom['ePayment']['PaymentItemWithPOS'] ?>

<?php if($targetClass!="" && $targetID !=""){ ?>
$(document).ready(function(){
	updatePayAmount();
});
<?php } ?>
</script>
<br />

<form id="form1" name="form1" method="post">
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
    <tr>
        <td class="tabletext formfieldtitle" valign="top" width="30%"><?=$Lang['ePayment']['Class']?></td>
        <td><?=$select_class?></td>
    </tr>
  <tr>
   <?=$show_students?>
   <?=$current_balance_row?>
   <?=$addvalue_row?>
  </tr>
</table>
<?=$show_table?>
<?php if($targetClass!="" && $targetID !=""){ ?>
<table width="96%" align="center">
	<tr>
		<td class="dotline"><img src="<?=$image_path.'/'.$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" /></td>
	</tr>
	<tr>
		<td align="center">
			<?=$linterface->GET_ACTION_BTN($button_pay, "button",'checkPay(document.form1,\'PaymentID[]\',\'student_pay_item.php\',\''.$Lang['ePayment']['ConfirmForceStudentToPay'].'\',\'ItemAmount[]\',\''.$Lang['ePayment']['NotEnoughBalanceToPay'].'\');',"pay_submit")?>
			<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Remove'], "button",'checkRemoveItems(document.form1,\'PaymentID[]\',\'student_remove_confirm.php\');',"remove_submit")?>
		</td>
	</tr>
</table>
<? } ?>
</form>
<?=$hidden_fields?>
<br />
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>