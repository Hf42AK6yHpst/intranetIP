<?php
// Editing by 
/*
 * 2014-06-17 (Carlos): Created for printing payment item student receipt at list.php
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpayment_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$lpayment_ui = new libpayment_ui();

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

?>
<style type="text/css">
	body th, body td {font-size: 12px;} 
</style>
<table width="100%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$lpayment_ui->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<?php

$PaymentIdAry = $_REQUEST['PaymentID'];
$sql = "SELECT DISTINCT StudentID FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE PaymentID IN ('".implode("','",$PaymentIdAry)."')";
$StudentIdAry = $lpayment->returnVector($sql);

$params = array('PaymentID'=>$PaymentIdAry,'StudentID'=>$StudentIdAry);
$x = $lpayment_ui->getPaymentItemReceipt($params);

echo $x;

intranet_closedb();
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
?>
