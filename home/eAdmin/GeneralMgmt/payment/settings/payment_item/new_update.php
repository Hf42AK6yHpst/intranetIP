<?php
// Editing by  
/*
 * 2020-06-29 (Ray):    Add multi payment gateway
 * 2019-09-27 (Ray):    Add Item Code
 * 2018-07-17 (Carlos): $sys_custom['ePayment']['Alipay'] - apply MerchantAccountID for Alipay.
 * 2016-11-18 (Carlos): Improved to have option to choose individual students.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_category.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
if (!$lpayment->IS_ADMIN_USER() || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$use_merchant_account = $lpayment->useEWalletMerchantAccount();

$li = new libdb();
$li->Start_Trans();
$ItemName = intranet_htmlspecialchars($ItemName);
$Description = intranet_htmlspecialchars($Description);
$DisplayOrder = intranet_htmlspecialchars($DisplayOrder);
$PayPriority = intranet_htmlspecialchars($PayPriority);
$ItemCode = intranet_htmlspecialchars($ItemCode);

$item_code_valid = true;
if($sys_custom['ePayment_PaymentItem_ItemCode']) {
	if($ItemCode == "") {
		$item_code_valid = false;
	} else {
		$item_code_valid = $lpayment->checkPaymentItemItemCodeValid($ItemCode);
	}
}


if($item_code_valid == true) {
	$more_insert_fields = "";
	$more_insert_values = "";

	if($sys_custom['ePayment_PaymentItem_ItemCode']) {
		$more_insert_fields .= ",ItemCode";
		$more_insert_values .= ",'$ItemCode'";
	}


	if ($use_merchant_account) {
		if($sys_custom['ePayment']['MultiPaymentGateway']) {
			$last_MerchantAccountID_value = '';
			$service_provider_list = $lpayment->getPaymentServiceProviderMapping(false, true);
			foreach ($service_provider_list as $k => $temp) {
				$MerchantAccountID_value = ${'MerchantAccountID_' . $k};
				if ($MerchantAccountID_value == '') {
					continue;
				}
				$last_MerchantAccountID_value = $MerchantAccountID_value;
			}
			$more_insert_fields .= ",MerchantAccountID";
			$more_insert_values .= $last_MerchantAccountID_value == '' ? ",NULL" : ",'$last_MerchantAccountID_value'";
		} else {
			$more_insert_fields .= ",MerchantAccountID";
			$more_insert_values .= $MerchantAccountID == '' ? ",NULL" : ",'$MerchantAccountID'";
		}
	}

	$sql = "INSERT INTO PAYMENT_PAYMENT_ITEM
               (Name, CatID,DisplayOrder, PayPriority,Description, StartDate, EndDate,
                DefaultAmount, RecordStatus,ProcessingTerminalUser,ProcessingTerminalIP,ProcessingAdminUser,
                DateInput, DateModified $more_insert_fields)
        VALUES ('$ItemName','$CatID','$DisplayOrder','$PayPriority','$Description','$StartDate','$EndDate',
                 '$Amount',0, NULL,NULL,'" . $_SESSION['UserID'] . "',
                 now(),now() $more_insert_values)";

	$Result['CreatePaymentItem'] = $li->db_db_query($sql);
	if ($li->db_affected_rows() > 0) {
		$ItemID = $li->db_insert_id();

		if ($use_merchant_account) {
			if ($sys_custom['ePayment']['MultiPaymentGateway']) {
				$service_provider_list = $lpayment->getPaymentServiceProviderMapping(false, true);
				foreach ($service_provider_list as $k => $temp) {
					$MerchantAccountID_value = ${'MerchantAccountID_' . $k};
					if ($MerchantAccountID_value == '') {
						continue;
					}
					$sql = "INSERT INTO PAYMENT_PAYMENT_ITEM_PAYMENT_GATEWAY
						(ItemID, MerchantAccountID, DateInput, InputBy)
						VALUES
						($ItemID, '$MerchantAccountID_value', now(), '" . $_SESSION['UserID'] . "')
						";
					$Result['CreatePaymentGateway' . $k] = $li->db_db_query($sql);
				}
			}
		}

		if ($ChooseFrom == "1") {
			# Insert each student's payment record
			if (sizeof($YearClassID) != 0) {
				$classList = "'" . implode("','", $YearClassID) . "'";
				if ($intranet_version == "2.5" || $intranet_version == "3.0") {
					$sql = "
			    INSERT IGNORE INTO PAYMENT_PAYMENT_ITEMSTUDENT
			    	(ItemID, StudentID, Amount,RecordType, RecordStatus, DateInput, DateModified)
			    SELECT $ItemID, a.UserID, '$Amount',0,0,now(),now()
			    FROM 
			    	INTRANET_USER as a 
			    	INNER JOIN  
			    	YEAR_CLASS as b 
			    	ON a.ClassName = b.ClassTitleEN and b.YearClassID IN ($classList) AND a.RecordType = 2 AND a.RecordStatus IN (0,1,2)";
				} else {
					$sql = "
			    INSERT IGNORE INTO PAYMENT_PAYMENT_ITEMSTUDENT
			    	(ItemID, StudentID, Amount,RecordType, RecordStatus, DateInput, DateModified)
			    SELECT $ItemID, a.UserID, '$Amount',0,0,now(),now()
			    FROM 
			    	INTRANET_USER as a 
			    	INNER JOIN  
			    	INTRANET_CLASS as b 
			    	ON a.ClassName = b.ClassName and b.ClassID IN ($classList) AND a.RecordType = 2 AND a.RecordStatus IN (0,1,2)";
				}
				$Result['CreateStudentAssociation'] = $li->db_db_query($sql);
			}
		} else if ($ChooseFrom == "2" && count($student) > 0) {
			$student = array_values(array_unique($student));
			$sql = "INSERT IGNORE INTO PAYMENT_PAYMENT_ITEMSTUDENT
			    	(ItemID, StudentID, Amount,RecordType, RecordStatus, DateInput, DateModified)
			    SELECT '$ItemID', UserID, '$Amount',0,0,now(),now() 
			    FROM INTRANET_USER WHERE UserID IN ('" . implode("','", $student) . "')";
			$Result['InsertStudents'] = $li->db_db_query($sql);
		}
	}
} else {
	$Result['ItemCodeDuplicate'] = false;
}

$redirect_send_reminder = false;
if (in_array(false,$Result)) {
	$Msg = $Lang['ePayment']['PaymentItemCreateUnsuccess'];
	$li->RollBack_Trans();
}
else {
	$Msg = $Lang['ePayment']['PaymentItemCreateSuccess'];
	$li->Commit_Trans();

	if($SendReminderPushMessage == 1) {
		$redirect_send_reminder = true;
	}
}

intranet_closedb();

if($redirect_send_reminder == true) {
	header("Location: prepare_send_push_message.php?Msg=" . urlencode($Msg)."&SendReminderPushMessage=".$SendReminderPushMessage."&ItemID=".$ItemID);
} else {
	header("Location: index.php?Msg=" . urlencode($Msg));
}

?>