<?php
// Editing by 
/*
 * 2020-03-03 (Ray):    Added UnitCode
 * 2013-10-18 (Carlos): Modified to sum amount of source subsidy from PAYMENT_PAYMENT_ITEM_SUBSIDY
 */
$PATH_WRT_ROOT="../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$linterface = new interface_html();


if (!isset($order)) $order = 1;
if (!isset($field)) $field = 0;
$order = ($order == 1) ? 1 : 0;



if($status==""){
	$conds="";
}
else if($status==0){
	$conds = " AND a.RecordStatus!=1 ";
}
else if($status==1){
	$conds = " AND a.RecordStatus=1";
}



$sql="SELECT a.UnitName,
    a.UnitCode,
	CONCAT('$',FORMAT(IFNULL(SUM(b.SubsidyAmount),0),1)) AS abc,
	CONCAT('$',IF(a.TotalAmount IS NULL,FORMAT(0,1),FORMAT(a.TotalAmount,1)))
	FROM PAYMENT_SUBSIDY_UNIT AS a 
	LEFT JOIN PAYMENT_PAYMENT_ITEM_SUBSIDY as b ON b.SubsidyUnitID=a.UnitID 
	WHERE (a.UnitName LIKE '%$keyword%' OR
               a.TotalAmount LIKE '%$keyword%' OR
               a.UnitCode LIKE '%$keyword%'
    )
    $conds
    GROUP BY a.UnitID
";

    
$field_array = array("a.UnitName","a.UnitCode","abc","a.TotalAmount");
$sql .= " ORDER BY ";
$sql .= (count($field_array)<=$field) ? $field_array[0] : $field_array[$field];
$sql .= ($order==0) ? " DESC" : " ASC";
$li = new libdb();


$temp = $li->returnArray($sql,3);

$x="<table width='90%' border='0' cellspacing='0' cellpadding='5' align='center' class='eSporttableborder'>";
$x.="<Tr class='eSporttdborder eSportprinttabletitle'>";
$x.="<td class=\"eSporttdborder eSportprinttext\">#</td>";
$x.="<td width=40% class=\"eSporttdborder eSportprinttext\">$i_Payment_Subsidy_Unit</td><td width=20% class=\"eSporttdborder eSportprinttext\">$i_Payment_Subsidy_UnitCode</td><td width=20% class=\"eSporttdborder eSportprinttext\">$i_Payment_Subsidy_Used_Amount</td><td width=20% class=\"eSporttdborder eSportprinttext\">$i_Payment_Subsidy_Total_Amount</td>";
$x.="</tr>";
for($i=0;$i<sizeof($temp);$i++){
	list($unit_name,$unit_code,$used_amount,$total_amount)=$temp[$i];
	//$css =$i%2==0?"tableContent":"tableContent2";
	$x.="<tr class=\"eSporttdborder eSportprinttext\">";
	$x.="<td class=\"eSporttdborder eSportprinttext\">".($i+1)."</td>";
	$x.="<td class=\"eSporttdborder eSportprinttext\">$unit_name</td><Td class=\"eSporttdborder eSportprinttext\">$unit_code</td><Td class=\"eSporttdborder eSportprinttext\">$used_amount</td><td class=\"eSporttdborder eSportprinttext\">$total_amount</td>";
	$x.="</tr>";
}
if(sizeof($temp)<=0){
        $x.="<tr class=\"eSporttdborder eSportprinttext\"><td colspan=5 align=center class=\"eSporttdborder eSportprinttext\" height=40 style='vertical-align:middle'>$i_no_record_exists_msg</td></tr>";
}


$x.="</table>";

$display=$x;

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
//echo displayNavTitle($i_Payment_Menu_Report_SchoolAccount,'');
?>
<table width="90%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<BR>
<table width='90%' border='0' cellspacing='0' cellpadding='5' align='center'>
<tr><td><b><? /* echo $i_Payment_Subsidy_Setting */ ?></b></td></tr>
</table>
<?=$display?>
<BR>

<?php
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>
