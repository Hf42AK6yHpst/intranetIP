<?php
// Editing by 
/*
 * 2020-03-03 (Ray):    Added UnitCode
 * 2013-10-18 (Carlos): Modified to sum amount of source subsidy from PAYMENT_PAYMENT_ITEM_SUBSIDY
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$lexport = new libexporttext();

if (!isset($order)) $order = 1;
if (!isset($field)) $field = 0;
$order = ($order == 1) ? 1 : 0;



if($status==""){
	$conds="";
}
else if($status==0){
	$conds = " AND a.RecordStatus!=1 ";
}
else if($status==1){
	$conds = " AND a.RecordStatus=1";
}



$sql="SELECT a.UnitName,
	a.UnitCode,
	ROUND(IFNULL(SUM(b.SubsidyAmount),0),1) AS abc,
	ROUND(a.TotalAmount,1)
	FROM PAYMENT_SUBSIDY_UNIT AS a LEFT OUTER JOIN PAYMENT_PAYMENT_ITEM_SUBSIDY AS b ON (a.UnitID = b.SubsidyUnitID)
	WHERE (a.UnitName LIKE '%$keyword%' OR
               a.TotalAmount LIKE '%$keyword%' OR
               a.UnitCode LIKE '%$keyword%'
    )
    $conds
    GROUP BY a.UnitID
";
 

$field_array = array("a.UnitName","a.UnitCode","abc","a.TotalAmount");
$sql .= " ORDER BY ";
$sql .= (count($field_array)<=$field) ? $field_array[0] : $field_array[$field];
$sql .= ($order==0) ? " DESC" : " ASC";
$li = new libdb();


$temp = $li->returnArray($sql,3);



$x="\"$i_Payment_Subsidy_Unit\",";
$x.="\"$i_Payment_Subsidy_UnitCode\",";
$x.="\"$i_Payment_Subsidy_Used_Amount\",";
$x.="\"$i_Payment_Subsidy_Total_Amount\"\n";

$utf_x=$i_Payment_Subsidy_Unit."\t";
$utf_x.=$i_Payment_Subsidy_UnitCode."\t";
$utf_x.=$i_Payment_Subsidy_Used_Amount."\t";
$utf_x.=$i_Payment_Subsidy_Total_Amount."\t\r\n";


if(sizeof($temp)<=0){
        $x.="\"$i_no_record_exists_msg\"\n";
        $utf_x.=$i_no_record_exists_msg."\r\n";
}else{
	for($i=0;$i<sizeof($temp);$i++){
		list($unit_name,$unit_code,$used_amount,$total_amount)=$temp[$i];
		$x.="\"$unit_name\",\"$unit_code\",\"$used_amount\",\"$total_amount\"\n";
		$utf_x.=$unit_name."\t".$unit_code."\t".$used_amount."\t".$total_amount."\r\n";
	}
}

$content = $x;

$utf_content=$utf_x;


intranet_closedb();
$filename = "subsidy_unit.csv";

$lexport->EXPORT_FILE($filename, $utf_content);
?>
