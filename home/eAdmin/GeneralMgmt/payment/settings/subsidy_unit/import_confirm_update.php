<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ePayment']['ImportSubsidySource']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lpayment = new libpayment();

$cur_sessionid = md5(session_id());
$sql = "SELECT * FROM PAYMENT_SUBSIDY_UNIT_IMPORT_RECORD WHERE SessionID='$cur_sessionid' ORDER BY RecordID";
$records = $lpayment->returnResultSet($sql);
$records_size = count($records);

$lpayment->Start_Trans();
$resultAry = array();
for($i=0;$i<$records_size;$i++)
{
	$UnitID = $records[$i]['UnitID'];
	$UnitName = $lpayment->Get_Safe_Sql_Query($records[$i]['UnitName']);
	$TotalAmount = $records[$i]['TotalAmount'];
	$Status = $records[$i]['RecordStatus'];
	
	if($UnitID!='')
	{
		$sql="select UnitName, TotalAmount, RecordStatus from PAYMENT_SUBSIDY_UNIT where UnitID = '".$UnitID."'";       
		$Result1 = current($lpayment->returnArray($sql));
		
		$sql="UPDATE PAYMENT_SUBSIDY_UNIT SET UnitName='$UnitName',TotalAmount='$TotalAmount',RecordStatus='$Status',DateModified=NOW() WHERE UnitID='$UnitID' ";         
		$resultAry[$i] = $lpayment->db_db_query($sql);
		
		if($resultAry[$i])
		{
			$RecordDetail = '';
			if($Result1['UnitName'] != $UnitName){
				$RecordDetail .= 'Source of Subsidy: from '.$Result1['UnitName'].' to '.$UnitName;
			}
			if($Result1['TotalAmount'] != $TotalAmount){
				if($Result1['UnitName'] != $UnitName){
					$RecordDetail .= '<br/>';
				}
				$RecordDetail .= 'Total: from '.$Result1['TotalAmount'].' to '.$TotalAmount;
			}
			if($Result1['RecordStatus'] != $Status){
				if($Result1['UnitName'] != $UnitName || $Result1['TotalAmount'] != $TotalAmount){
					$RecordDetail .= '<br/>';
				}
				$RecordDetail .= 'Status: from '.($Result1['RecordStatus']?$i_general_active:$i_general_inactive).' to '.($Status?$i_general_active:$i_general_inactive);
			}
			
			$sql = "INSERT INTO
							MODULE_RECORD_UPDATE_LOG
							(Module, Section, RecordDetail, UpdateTableName, UpdateRecordID, LogDate, LogBy)
					VALUES
							('ePayment', 'SourceOfSubsidySettingsEdit','$RecordDetail', 'PAYMENT_SUBSIDY_UNIT', '$UnitID', NOW(), $UserID)
					";
			$lpayment->db_db_query($sql);
		}
	}else{
		$sql="INSERT INTO PAYMENT_SUBSIDY_UNIT (UnitName,TotalAmount,RecordStatus,DateInput,DateModified) VALUES('$UnitName','$TotalAmount','$Status',NOW(),NOW())";
		$resultAry[$i] = $lpayment->db_db_query($sql);
		
		if($resultAry[$i])
		{
			$insert_id = $lpayment->db_insert_id();
			$RecordDetail = 'Source of Subsidy: '.$UnitName.'<br/>Total: '.$TotalAmount.'<br/>Status: '.($Status?$i_general_active:$i_general_inactive);
			$sql = "INSERT INTO
							MODULE_RECORD_UPDATE_LOG
							(Module, Section, RecordDetail, UpdateTableName, UpdateRecordID, LogDate, LogBy)
					VALUES
							('ePayment', 'SourceOfSubsidySettingsAdd','$RecordDetail', 'PAYMENT_SUBSIDY_UNIT', '".$insert_id."', NOW(), $UserID)
					";
			$lpayment->db_db_query($sql);
		}
	}
}

if(!in_array(false,$resultAry)){
	$lpayment->Commit_Trans();
	$_SESSION['PAYMENT_SUBSIDY_SOURCE_IMPORT_RESULT'] = $Lang['General']['ReturnMessage']['ImportSuccess'];
}else{
	$lpayment->RollBack_Trans();
	$_SESSION['PAYMENT_SUBSIDY_SOURCE_IMPORT_RESULT'] = $Lang['General']['ReturnMessage']['ImportUnsuccess'];
}

intranet_closedb();
header("Location:import.php");
exit;
?>