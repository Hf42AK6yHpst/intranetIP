<?php

# using: 
/*
 * 2014-03-24 Carlos - Allow negative subsidy balance
 * 2013-10-18 Carlos - Modified to sum subsidy amount from PAYMENT_PAYMENT_ITEM_SUBSIDY
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();

$linterface = new interface_html();


if(is_array($UnitID))
	$UnitID = $UnitID[0];

// get summary	
$sql="SELECT SUM(a.SubsidyAmount) FROM PAYMENT_PAYMENT_ITEM_SUBSIDY AS a WHERE a.SubsidyUnitID ='$UnitID' ";
$temp = $lpayment->returnVector($sql);
$unit_used_amount = $temp[0];
	
	
$sql="SELECT UnitName,TotalAmount FROM PAYMENT_SUBSIDY_UNIT WHERE UnitID = '$UnitID'";
$temp = $lpayment->returnArray($sql,2);
list($unit_name,$unit_total_amount) = $temp[0];

$unit_left_amount = $unit_total_amount - $unit_used_amount;
//$unit_left_amount = $unit_left_amount > 0 ?$unit_left_amount:0;
$unit_total_amount = "$ ".number_format($unit_total_amount,2);
$unit_used_amount  = "$ ".number_format($unit_used_amount,2);
$unit_left_amount  = "$ ".number_format($unit_left_amount,2);

$sql="
	SELECT 
		COUNT(*), CONCAT('$ ',FORMAT(SUM(s.SubsidyAmount),2))
	FROM 
		PAYMENT_PAYMENT_ITEM_SUBSIDY as s 
		LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT AS a ON a.PaymentID=s.PaymentID AND a.StudentID=s.UserID 
		LEFT JOIN PAYMENT_PAYMENT_ITEM AS b ON (a.ItemID = b.ItemID) 
		LEFT JOIN INTRANET_USER AS c ON (s.UserID = c.UserID) 
		LEFT JOIN INTRANET_ARCHIVE_USER AS d ON (s.UserID = d.UserID)
	WHERE s.SubsidyUnitID ='$UnitID'  AND 
              (
              s.SubsidyAmount LIKE '%$keyword%' OR
              s.SubsidyPICAdmin LIKE '%$keyword%' OR
              s.DateModified LIKE '%$keyword%' OR
              c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%' OR
               d.EnglishName LIKE '%$keyword%' OR
               d.ChineseName LIKE '%$keyword%' OR
               d.ClassName LIKE '%$keyword%' OR
               d.ClassNumber LIKE '%$keyword%' OR
               b.Name LIKE '%$keyword%'

              )	
	";
	
$temp = $lpayment->returnArray($sql,2);
$no_of_students = $temp[0][0];	              
$search_amount = $temp[0][1];

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;


if (!isset($order)) $order = 1;
if (!isset($field)) $field = 0;
$order = ($order == 1) ? 1 : 0;


$paid_status = $paid_status ? $paid_status : -1;
switch($paid_status)
{
	case 1:
		$status_con = " and a.RecordStatus=1 ";
		break;
	case 2:
		$status_con = " and a.RecordStatus!=1 ";
		break;	
}

if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$archive_namefield = "d.ChineseName";
}else $archive_namefield = "d.EnglishName";
$namefield = getNameFieldWithClassNumberByLang("c.");

if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$archive_namefield2 = "f.ChineseName";
}else $archive_namefield2 = "f.EnglishName";
$namefield2 = getNameFieldByLang("e.");

$sql="
	SELECT 
		IF((c.UserID IS NULL AND d.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), $namefield),
		b.Name,CONCAT('$',FORMAT(s.SubsidyAmount,2)),
		if(s.SubsidyPICAdmin!='', s.SubsidyPICAdmin, IF((e.UserID IS NULL AND f.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$archive_namefield2,'</i>'), $namefield2)) as picadmin,
		s.DateModified,
		IF(a.RecordStatus = 1,'$i_Payment_PaymentStatus_Paid','$i_Payment_PaymentStatus_Unpaid') as PayStatus
	FROM 
		PAYMENT_PAYMENT_ITEM_SUBSIDY as s 
		LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT AS a ON a.PaymentID=s.PaymentID AND a.StudentID=s.UserID 
		LEFT JOIN PAYMENT_PAYMENT_ITEM AS b ON (a.ItemID = b.ItemID) 
		LEFT JOIN INTRANET_USER AS c ON (s.UserID = c.UserID) 
		LEFT JOIN INTRANET_ARCHIVE_USER AS d ON (s.UserID = d.UserID)
		LEFT JOIN INTRANET_USER AS e ON (e.UserID = s.SubsidyPICUserID) 
		LEFT JOIN INTRANET_ARCHIVE_USER AS f ON (f.UserID = s.SubsidyPICUserID) 
	WHERE s.SubsidyUnitID ='$UnitID'  AND 
              (
              s.SubsidyAmount LIKE '%$keyword%' OR
              s.SubsidyPICAdmin LIKE '%$keyword%' OR
              s.DateModified LIKE '%$keyword%' OR
              c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%' OR
               d.EnglishName LIKE '%$keyword%' OR
               d.ChineseName LIKE '%$keyword%' OR
               d.ClassName LIKE '%$keyword%' OR
               d.ClassNumber LIKE '%$keyword%' OR
               b.Name LIKE '%$keyword%'
              )	
		$status_con
	";            
# TABLE INFO

$field_array = array("$namefield","b.Name","s.SubsidyAmount","picadmin","s.DateModified","PayStatus");
$sql .= " ORDER BY ";
$sql .= (count($field_array)<=$field) ? $field_array[0] : $field_array[$field];
$sql .= ($order==0) ? " DESC" : " ASC";
$li = new libdb();


$temp = $li->returnArray($sql,5);

$x="<table width='90%' border='0' cellspacing='0' cellpadding='5' align='center' class='eSporttableborder'>";
$x.="<Tr class='eSporttdborder eSportprinttabletitle'>";
$x.= "<td width=1 class='eSporttdborder eSportprinttabletitle'>#</td>\n";
$x.= "<td width=25% class='eSporttdborder eSportprinttabletitle'>$i_UserStudentName</td>\n";
$x.= "<td width=25% class='eSporttdborder eSportprinttabletitle'>$i_Payment_Field_PaymentItem</td>\n";
$x.= "<td width=15% class='eSporttdborder eSportprinttabletitle'>$i_Payment_Subsidy_Amount</td>\n";
$x.= "<td width=15% class='eSporttdborder eSportprinttabletitle'>$i_Payment_Subsidy_Unit_Admin</td>\n";
$x.= "<td width=20% class='eSporttdborder eSportprinttabletitle'>$i_Payment_Subsidy_Unit_UpdateTime</td>\n";
$x.= "<td width=10% class='eSporttdborder eSportprinttabletitle'>$i_general_status</td>\n";

$x.="</tr>";
for($i=0;$i<sizeof($temp);$i++){
	list($student_name,$item_name,$sub_amount,$last_modified,$last_modified_date, $this_status)=$temp[$i];
	//$css =$i%2==0?"tableContent":"tableContent2";
	$x.="<tr class=\"eSporttdborder eSportprinttext\">";
	$x.="<td class=\"eSporttdborder eSportprinttext\">".($i+1)."</td>";
	$x.="<td class=\"eSporttdborder eSportprinttext\">$student_name</td>
	<td class=\"eSporttdborder eSportprinttext\">$item_name</td>
	<td class=\"eSporttdborder eSportprinttext\">$sub_amount</td>
	<td class=\"eSporttdborder eSportprinttext\">$last_modified&nbsp;</td>
	<td class=\"eSporttdborder eSportprinttext\">$last_modified_date</td>
	<td class=\"eSporttdborder eSportprinttext\">$this_status</td>
	";
	$x.="</tr>";
}
if(sizeof($temp)<=0){
        $x.="<tr><td colspan=7 align=center class=\"eSporttdborder eSportprinttext\" height=40 style='vertical-align:middle'>$i_no_record_exists_msg</td></tr>";
}


$x.="</table>";

$display=$x;

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
?>
<table width="90%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<BR>
<!-- Summary table -->
<table width="90%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Payment_Subsidy_Unit?></td><td valign="top" nowrap="nowrap" class="tabletext"><?=$unit_name?></td></tr>
	<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Payment_Subsidy_Total_Amount?></td><td valign="top" nowrap="nowrap" class="tabletext" width=68%><?=$unit_total_amount?></td></tr>
	<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Payment_Subsidy_Used_Amount?></td><td valign="top" nowrap="nowrap" class="tabletext" ><?=$unit_used_amount?></td></tr>
	<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Payment_Subsidy_Left_Amount?></td><td valign="top" nowrap="nowrap" class="tabletext" ><?=$unit_left_amount?></td></tr>
	<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Payment_Subsidy_Search_Amount?></td><td valign="top" nowrap="nowrap" class="tabletext" ><?=("$search_amount ($no_of_students $i_Payment_Students)")?></td></tr>
</table>
<!-- end summary table -->
<?=$display?>
<table width="90%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td class="tabletextremark">
			<?=$i_Payment_Note_StudentRemoved?>
		</td>
	</tr>
</table>
<BR>

<?php
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>
