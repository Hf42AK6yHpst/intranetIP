<?php
// Editing by 
/*
 *  2020-03-03 (Ray):    Added UnitCode
 * 2014-03-27 (Carlos): Display [Consumed] in red color if it exceeds [Total]
 * 2014-03-20 (Henry): Added tab Source of Subsidy Change Log
 * 2013-10-18 (Carlos): Modified to sum amount of source subsidy from PAYMENT_PAYMENT_ITEM_SUBSIDY
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();

$linterface = new interface_html();

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if (!isset($order)) $order = 1;
if (!isset($field)) $field = 0;
$order = ($order == 1) ? 1 : 0;

if($status=="")
	$status=1;

if($status=="-1"){
	$conds="";
}
else if($status==0){
	$conds = " AND a.RecordStatus=0 ";
}
else if($status==1){
	$conds = " AND a.RecordStatus=1";
}



$sql="SELECT CONCAT('<a class=tablelink href=list.php?UnitID[]=',a.UnitID,'>',a.UnitName,'</a>'),
	a.UnitCode,    
	IF(SUM(b.SubsidyAmount)>a.TotalAmount, CONCAT('<span style=\"color:red\">$',FORMAT(IFNULL(SUM(b.SubsidyAmount),0),2),'</span>'), CONCAT('$',FORMAT(IFNULL(SUM(b.SubsidyAmount),0),2))) AS abc,
	CONCAT('$',IF(a.TotalAmount IS NULL,FORMAT(0,2),FORMAT(a.TotalAmount,2))),
	CONCAT('<input type=checkbox name=UnitID[] value=', a.UnitID ,'>')
	FROM PAYMENT_SUBSIDY_UNIT AS a LEFT OUTER JOIN PAYMENT_PAYMENT_ITEM_SUBSIDY AS b ON (a.UnitID = b.SubsidyUnitID)
	WHERE (a.UnitName LIKE '%$keyword%' OR
               a.TotalAmount LIKE '%$keyword%' OR
               a.UnitCode LIKE '%$keyword%'
    )
    $conds
    GROUP BY a.UnitID
";

              
                
# TABLE INFO

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("a.UnitName","a.UnitCode","abc","a.TotalAmount");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width=40% >".$li->column($pos++, $i_Payment_Subsidy_Unit)."</td>\n";
$li->column_list .= "<td width=20% >".$li->column($pos++, $i_Payment_Subsidy_UnitCode)."</td>\n";
$li->column_list .= "<td width=20% >".$li->column($pos++, $i_Payment_Subsidy_Used_Amount)."</td>\n";
$li->column_list .= "<td width=20% >".$li->column($pos++, $i_Payment_Subsidy_Total_Amount)."</td>\n";
$li->column_list .= "<td width=1 >".$li->check("UnitID[]")."</td>\n";

$select_status = "<SELECT name=status onChange=this.form.submit()>\n";
$select_status .= "<OPTION value='-1' ".($status=="-1"? "SELECTED":"").">$i_status_all</OPTION>\n";
$select_status .= "<OPTION value=1 ".($status==1? "SELECTED":"").">$i_general_active</OPTION>\n";
$select_status .= "<OPTION value=0 ".($status==0? "SELECTED":"").">$i_general_inactive</OPTION>\n";
$select_status .= "</SELECT>\n";


$toolbar = $linterface->GET_LNK_NEW("javascript:checkNew('new.php')","","","","",0);
$toolbar.= $linterface->GET_LNK_PRINT("javascript:openPrintPage(document.form1,'print.php')","","","","",0);
if($sys_custom['ePayment']['ImportSubsidySource']){
	$toolbar .= $linterface->GET_LNK_IMPORT("import.php","","","","",0);
}
$toolbar.= $linterface->GET_LNK_EXPORT("javascript:exportPage(document.form1,'export.php')","","","","",0);


$table_tool = "<a href=\"javascript:checkEdit(document.form1,'UnitID[]','edit.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_edit
					</a>";

$searchbar = "<input class=\"textboxnum\" type=\"text\" name=\"keyword\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= $linterface->GET_SMALL_BTN($button_find, "button", "document.form1.submit();","submit2");


$filterbar = $select_status;

$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "SourceOfSubsidySettings";
$TAGS_OBJ[] = array($i_Payment_Subsidy_Setting, "", 1);
$TAGS_OBJ[] = array($Lang['ePayment']['SourceOfSubsidyChangeLog'], "change_log.php", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();

// return message
$Msg = urldecode($Msg);
$linterface->LAYOUT_START($Msg);

$SysMsg = "<BR>";
if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
?>
<script language='javascript'>
function openPrintPage(obj,url){
	/*
	old_url = obj.action;
	old_target=obj.target;
	obj.action=url;
	obj.target='_blank';
	obj.submit();
	
	obj.action = old_url;
	obj.target = old_target;
	*/
	url+='?pageNo='+obj.pageNo.value;
	url+='&order='+obj.order.value;
	url+='&field='+obj.field.value;
	url+='&page_size_change='+obj.page_size_change.value;
	url+='&numPerPage='+obj.numPerPage.value;
	url+='&status='+obj.status.options[obj.status.selectedIndex].value;
	url+='&keyword='+obj.keyword.value;
	newWindow(url,10);
	
}
function exportPage(obj,url){
        old_url = obj.action;
        obj.action=url;
        obj.submit();
        obj.action = old_url;

}
</script>
<form name="form1" method="get">
<BR>
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td class="tabletext" align="right" colspan="2"><?=$SysMsg?></td>
	</tr>
	<tr>
		<td align="left"><?=$toolbar?></td>
		<td align="right">&nbsp;</td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr class="table-action-bar">
		<td align="left"><?= $filterbar; ?><?=$searchbar?></td>
		<td valign="bottom" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<td>
									<?=$table_tool?>
								</td>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<?php echo $li->display("96%"); ?>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>

<?
/*
# remove the temp table
$temp_table_sql = "DROP TABLE TEMP_PAYMENT_ITEM_SUMMARY";
$lpayment->db_db_query($temp_table_sql);
*/
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
