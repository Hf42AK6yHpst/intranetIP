<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ePayment']['ImportSubsidySource']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$lpayment = new libpayment();
$linterface = new interface_html();

$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "SourceOfSubsidySettings";
$TAGS_OBJ[] = array($i_Payment_Subsidy_Setting, "", 1);
$TAGS_OBJ[] = array($Lang['ePayment']['SourceOfSubsidyChangeLog'], "change_log.php", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();

if(isset($_SESSION['PAYMENT_SUBSIDY_SOURCE_IMPORT_RESULT'])){
	$Msg = $_SESSION['PAYMENT_SUBSIDY_SOURCE_IMPORT_RESULT'];
	unset($_SESSION['PAYMENT_SUBSIDY_SOURCE_IMPORT_RESULT']);
}

$linterface->LAYOUT_START($Msg);

$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($i_Payment_Subsidy_Setting,'index.php');
$PAGE_NAVIGATION[] = array($Lang['Btn']['Import'],'');

# step information
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['SelectCSVFile'], 1);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['CSVConfirmation'], 0);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['ImportResult'], 0);
?>
<br />
<form name="form1" method="POST" action="import_confirm.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION)?></td>
	</tr>
	<tr>
		<td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['Btn']['Select']?>
		</td>
		<td width="70%" class="tabletext">
			<input type="file" class="file" name="upload_file"><br />
			<?=$linterface->GET_IMPORT_CODING_CHKBOX()?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle" align="left"><?=$Lang['General']['CSVSample']?> </td>
		<td width="70%" class="tabletext"><a class="tablelink" href="get_import_csv_sample.php" target="_blank"><?=$Lang['General']['ClickHereToDownloadSample']?></a></td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?></td>
		<td width="70%" class="tabletext">
			<div><?=$Lang['General']['ImportArr']['Column'].' 1 : '.$i_Payment_Subsidy_UnitName?></div>
			<div><?=$Lang['General']['ImportArr']['Column'].' 2 : '.$i_Payment_Subsidy_Total_Amount?></div>
			<div><?=$Lang['General']['ImportArr']['Column'].' 3 : '.$i_general_status.' <span class="tabletextremark">(1: '.$i_general_active.', 0: '.$i_general_inactive.')</span>'?></div>
		</td>
	</tr>
	<tr><td colspan="2"><?=$linterface->MandatoryField()?></td></tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="/images/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "", "", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
			<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "javascript:window.location.href='index.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
		</td>
	</tr>
</table>
</form>
<br />
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>