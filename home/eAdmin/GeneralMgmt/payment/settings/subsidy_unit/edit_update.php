<?

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$li = new libdb();
$lpayment = new libpayment();

$UnitName = intranet_htmlspecialchars($UnitName);
$TotalAmount = $TotalAmount==""?0:$TotalAmount;

$UnitCode = trim($UnitCode);
$unitcode_check = true;
$Result = false;
$values = '';
if($UnitCode != '') {
	$sql = "SELECT UnitID FROM PAYMENT_SUBSIDY_UNIT WHERE UnitCode='.$li->Get_Safe_Sql_Query($UnitCode).' AND UnitID!='".$li->Get_Safe_Sql_Query($UnitID)."'";
	$temp = $li->returnArray($sql);
	if (count($temp) != 0) {
		$unitcode_check = false;
	} else {
		$values .= ",UnitCode='".$li->Get_Safe_Sql_Query($UnitCode)."'";
	}
} else {
	$values .= ", UnitCode=NULL";
}

if($unitcode_check == true) {
	$sql = "select UnitName, TotalAmount, RecordStatus from PAYMENT_SUBSIDY_UNIT where UnitID = '" . $UnitID . "'";
	$Result1 = current($li->returnArray($sql));

	$sql = "UPDATE PAYMENT_SUBSIDY_UNIT SET UnitName='$UnitName',TotalAmount='$TotalAmount',RecordStatus='$Status',DateModified=NOW() $values WHERE UnitID='$UnitID' ";
	$Result = $li->db_db_query($sql);

	$RecordDetail = '';
	if ($Result1['UnitName'] != $UnitName) {
		$RecordDetail .= 'Source of Subsidy: from ' . $Result1['UnitName'] . ' to ' . $UnitName;
	}
	if ($Result1['TotalAmount'] != $TotalAmount) {
		if ($Result1['UnitName'] != $UnitName) {
			$RecordDetail .= '<br/>';
		}
		$RecordDetail .= 'Total: from ' . $Result1['TotalAmount'] . ' to ' . $TotalAmount;
	}
	if ($Result1['RecordStatus'] != $Status) {
		if ($Result1['UnitName'] != $UnitName || $Result1['TotalAmount'] != $TotalAmount) {
			$RecordDetail .= '<br/>';
		}
		$RecordDetail .= 'Status: from ' . ($Result1['RecordStatus'] ? $i_general_active : $i_general_inactive) . ' to ' . ($Status ? $i_general_active : $i_general_inactive);
	}

	$sql = "INSERT INTO
				MODULE_RECORD_UPDATE_LOG
				(Module, Section, RecordDetail, UpdateTableName, UpdateRecordID, LogDate, LogBy)
		VALUES
				('ePayment', 'SourceOfSubsidySettingsEdit','$RecordDetail', 'PAYMENT_SUBSIDY_UNIT', '$UnitID', NOW(), $UserID)
		";
	$Result2 = $li->db_db_query($sql);

}

if ($Result) {
	$Msg = $Lang['ePayment']['UpdateSubsidySourceSuccess'];
}
else {
	$Msg = $Lang['ePayment']['UpdateSubsidySourceUnsuccess'];
}

header ("Location: index.php?Msg=".urlencode($Msg));
intranet_closedb();
?>
