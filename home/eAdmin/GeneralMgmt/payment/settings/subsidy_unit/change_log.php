<?php
// Editing by 
/*
 * 2014-03-21 (Henry): Created file
 */


$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$lpayment = new libpayment();

$linterface = new interface_html();
                    
if($status=="")
	$status=-1;

if($status=="-1"){
	$statusConds="";
}
else if($status==0 || $status==1){
	$statusConds = "and p.RecordStatus like '%".$status."%'";
}

if($subsidy=="")
	$subsidy=-1;

if($subsidy=="-1"){
	$subsidyConds="";
}
else{
	$subsidyConds = " and p.UnitID ='$subsidy'";
}

$StartDate = $StartDate ? $StartDate : date("Y-m-d");
$EndDate = $EndDate ? $EndDate : date("Y-m-d");


# TABLE INFO
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
if($field=="") $field = 0;
$li = new libdbtable2007($field, $order, $pageNo);
//modifying
$li->field_array = array("a.LogDate");
$name_field = getNameFieldWithClassNumberByLang("b.");
$sql = "select 
		a.LogDate,
		p.UnitName,
		IF(a.Section = 'SourceOfSubsidySettingsAdd','".$Lang['Btn']['New']."','".$Lang['Btn']['Edit']."') as type,
		$name_field as logby,
		a.RecordDetail
		from 
			MODULE_RECORD_UPDATE_LOG as a 
			INNER join INTRANET_USER as b on (b.UserID = a.LogBy)
			INNER join PAYMENT_SUBSIDY_UNIT as p on (a.UpdateRecordID = p.UnitID)
		where
			left(a.LogDate,10) >= '$StartDate' and left(a.LogDate,10) <= '$EndDate'
			and a.Module = 'ePayment'
			and a.Section like 'SourceOfSubsidySettings%'
			$statusConds
			$subsidyConds
		";
//$sql = "select 
//		left(a.LogDate,10),
//		$name_field as logby,
//		a.RecordDetail
//		from 
//			MODULE_RECORD_DELETE_LOG as a 
//			INNER join INTRANET_USER as b on (b.UserID = a.LogBy)
//		";
//modifying
$li->sql = $sql;
$li->no_col = 5;
$li->IsColOff = "GeneralDisplayWithNavigation";

// TABLE COLUMN
$li->column_list .= "<th>".$Lang['ePOS']['UpdateTime']."</th>\n";
$li->column_list .= "<th>".$i_Payment_Subsidy_Unit."</th>\n";
$li->column_list .= "<th>".$Lang['General']['Type']."</th>\n";
$li->column_list .= "<th>".$Lang['ePOS']['UpdatedBy']."</th>\n";
$li->column_list .= "<th>".$Lang['ePayment']['RecordInfo']."</th>\n";

### Button
$delBtn = "<a href=\"javascript:clickDelete()\" class=\"tool_delete\">" . $Lang['ePayment']['DeleteRecordsInDateRange'] . "</a>";

### Filter - Date Range
$date_select = $eNotice['Period_Start'] .": ";
$date_select .= $linterface->GET_DATE_PICKER("StartDate",$StartDate);
$date_select .= $eNotice['Period_End']."&nbsp;";
$date_select .= $linterface->GET_DATE_PICKER("EndDate",$EndDate);
$date_select .= $linterface->GET_BTN($button_submit, "submit");

$select_status = "<SELECT name=status onChange=this.form.submit()>\n";
$select_status .= "<OPTION value='-1' ".($status=="-1"? "SELECTED":"").">$i_status_all</OPTION>\n";
$select_status .= "<OPTION value=1 ".($status==1? "SELECTED":"").">$i_general_active</OPTION>\n";
$select_status .= "<OPTION value=0 ".($status==0? "SELECTED":"").">$i_general_inactive</OPTION>\n";
$select_status .= "</SELECT>\n";

$sql = "select UnitID, UnitName from PAYMENT_SUBSIDY_UNIT";
$subsidyArray = $li->returnArray($sql);
$subsidy_filter = "<SELECT name=subsidy onChange=this.form.submit()>\n";
$subsidy_filter .= "<OPTION value='-1' ".($subsidy=="-1"? "SELECTED":"").">-- $i_Payment_Subsidy_Unit --</OPTION>\n";
foreach($subsidyArray as $aSubsidy){
	$subsidy_filter .= "<OPTION value=".$aSubsidy['UnitID']." ".($subsidy==$aSubsidy['UnitID']? "SELECTED":"").">".$aSubsidy['UnitName']."</OPTION>\n";
}
$subsidy_filter .= "</SELECT>\n";

$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "SourceOfSubsidySettings";
$TAGS_OBJ[] = array($i_Payment_Subsidy_Setting, "index.php", 0);
$TAGS_OBJ[] = array($Lang['ePayment']['SourceOfSubsidyChangeLog'], "", 1);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();

// return message
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$msg]);
?>
<SCRIPT LANGUAGE=Javascript>
<!--//
function clickDelete()
{
	document.form1.action = "change_log_remove_update.php";
	AlertPost(document.form1, "change_log_remove_update.php", "<?=$Lang['ePayment']['ConfirmDeleteRecordsInDateRange']?>");
}

function check_form()
{
	obj = document.form1;
	
	if(!check_date(obj.StartDate,"<?=$i_invalid_date?>"))
	{
		obj.StartDate.focus();
		return false;
	}
	
	if(!check_date(obj.EndDate,"<?=$i_invalid_date?>"))
	{
		obj.EndDate.focus();
		return false;
	}
	
	if(compareDate(obj.EndDate.value, obj.StartDate.value)<0) {
		obj.StartDate.focus();
		alert("<?=$i_con_msg_date_startend_wrong_alert?>");
		return false;
	}	
	
	return true;
}
//-->
</SCRIPT>


<form name="form1" method="get" action="change_log.php" onSubmit="return check_form();">

<div class="table_board">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td valign="bottom">
	<?=$select_status.'&nbsp;'.$subsidy_filter.'&nbsp;'.$date_select?>
	</td>
	<td valign="bottom">
	<div class="common_table_tool">
		
	</div>
	</td>
</tr>
</table>
</div>

<?=$li->display();?>


<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>

<?
/*
# remove the temp table
$temp_table_sql = "DROP TABLE TEMP_PAYMENT_ITEM_SUMMARY";
$lpayment->db_db_query($temp_table_sql);
*/
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
