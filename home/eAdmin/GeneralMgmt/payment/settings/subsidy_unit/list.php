<?php
# using: 

##########################################
#	Date:	2014-03-24	Carlos - Allow negative subsidy balance
#
#	Date:	2013-10-18 	Carlos - Modified to sum subsidy amount from PAYMENT_PAYMENT_ITEM_SUBSIDY
#
#	Date:	2011-10-25	YatWoon
#			Fixed: failed to dispaly "last update", missing to change the query after the function migrate from admin console to front-end (database also missing the data) [Case#2011-1010-1145-45071]
#			Improved: Add "Status" and "Filter"
#
##########################################

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");


intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$lpayment = new libpayment();

$linterface = new interface_html();
if($UnitID=="" || sizeof($UnitID)<=0){
	header("Location: index.php");
	exit();
}

if(is_array($UnitID))
	$UnitID = $UnitID[0];

// get summary	
$sql="SELECT SUM(a.SubsidyAmount) FROM PAYMENT_PAYMENT_ITEM_SUBSIDY AS a WHERE a.SubsidyUnitID ='$UnitID' ";
$temp = $lpayment->returnVector($sql);
$unit_used_amount = $temp[0];
	
	
$sql="SELECT UnitName,TotalAmount FROM PAYMENT_SUBSIDY_UNIT WHERE UnitID = '$UnitID'";
$temp = $lpayment->returnArray($sql,2);
list($unit_name,$unit_total_amount) = $temp[0];

$unit_left_amount = $unit_total_amount - $unit_used_amount;
//$unit_left_amount = $unit_left_amount > 0 ?$unit_left_amount:0;
$unit_total_amount = "$ ".number_format($unit_total_amount,2);
$unit_used_amount  = "$ ".number_format($unit_used_amount,2);
$unit_left_amount  = "$ ".number_format($unit_left_amount,2);

$sql="
	SELECT 
		COUNT(*), CONCAT('$ ',FORMAT(SUM(s.SubsidyAmount),2))
	FROM 
		PAYMENT_PAYMENT_ITEM_SUBSIDY as s 
		LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT AS a ON s.PaymentID=a.PaymentID AND a.StudentID=s.UserID 
		LEFT JOIN PAYMENT_PAYMENT_ITEM AS b ON (a.ItemID = b.ItemID) 
		LEFT JOIN INTRANET_USER AS c ON (s.UserID = c.UserID) 
		LEFT JOIN INTRANET_ARCHIVE_USER AS d ON (s.UserID = d.UserID)
	WHERE s.SubsidyUnitID ='$UnitID'  AND 
              (
              s.SubsidyAmount LIKE '%$keyword%' OR
              s.SubsidyPICAdmin LIKE '%$keyword%' OR
              s.DateModified LIKE '%$keyword%' OR
              c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%' OR
               d.EnglishName LIKE '%$keyword%' OR
               d.ChineseName LIKE '%$keyword%' OR
               d.ClassName LIKE '%$keyword%' OR
               d.ClassNumber LIKE '%$keyword%' OR
               b.Name LIKE '%$keyword%'

              )	
	";
	
$temp = $lpayment->returnArray($sql,2);
$no_of_students = $temp[0][0];	              
$search_amount = $temp[0][1];


if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;


if (!isset($order)) $order = 1;
if (!isset($field)) $field = 0;
$order = ($order == 1) ? 1 : 0;

$paid_status = $paid_status ? $paid_status : -1;
$select_status="<SELECT name=paid_status onChange=document.form1.submit()>\n";
$select_status.="<OPTION value=-1 ".($paid_status=="-1"?" SELECTED":"").">$i_Payment_PaymentStatus_Paid / $i_Payment_PaymentStatus_Unpaid </OPTION>";
$select_status.="<OPTION value=1 ".($paid_status=="1"?" SELECTED":"").">$i_Payment_PaymentStatus_Paid</OPTION>";
$select_status.="<OPTION value=2 ".($paid_status=="2"?" SELECTED":"").">$i_Payment_PaymentStatus_Unpaid</OPTION>";
$select_status.="</SELECT>";

switch($paid_status)
{
	case 1:
		$status_con = " and a.RecordStatus=1 ";
		break;
	case 2:
		$status_con = " and a.RecordStatus!=1 ";
		break;	
}

if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$archive_namefield = "d.ChineseName";
}else $archive_namefield = "d.EnglishName";
$namefield = getNameFieldWithClassNumberByLang("c.");

if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$archive_namefield2 = "f.ChineseName";
}else $archive_namefield2 = "f.EnglishName";
$namefield2 = getNameFieldByLang("e.");

$sql="
	SELECT 
		IF((c.UserID IS NULL AND d.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), $namefield),
		b.Name,
		CONCAT('$',FORMAT(s.SubsidyAmount,2)),
		if(s.SubsidyPICAdmin!='', s.SubsidyPICAdmin, IF((e.UserID IS NULL AND f.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$archive_namefield2,'</i>'), $namefield2)) as picadmin,
		s.DateModified,
		IF(a.RecordStatus = 1,'$i_Payment_PaymentStatus_Paid','$i_Payment_PaymentStatus_Unpaid') as PayStatus
	FROM  
		PAYMENT_PAYMENT_ITEM_SUBSIDY as s 
		LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT AS a ON a.PaymentID=s.PaymentID AND a.StudentID=s.UserID 
		LEFT JOIN PAYMENT_PAYMENT_ITEM AS b ON (a.ItemID = b.ItemID) 
		LEFT JOIN INTRANET_USER AS c ON (s.UserID = c.UserID) LEFT OUTER JOIN
		INTRANET_ARCHIVE_USER AS d ON (s.UserID = d.UserID)
		LEFT OUTER JOIN INTRANET_USER AS e ON (e.UserID = s.SubsidyPICUserID) 
		LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS f ON (f.UserID = s.SubsidyPICUserID) 
	WHERE s.SubsidyUnitID ='$UnitID'  AND 
              (
              s.SubsidyAmount LIKE '%$keyword%' OR
              s.SubsidyPICAdmin LIKE '%$keyword%' OR
              s.DateModified LIKE '%$keyword%' OR
              c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%' OR
               d.EnglishName LIKE '%$keyword%' OR
               d.ChineseName LIKE '%$keyword%' OR
               d.ClassName LIKE '%$keyword%' OR
               d.ClassNumber LIKE '%$keyword%' OR
               b.Name LIKE '%$keyword%'
              )	
              $status_con
	";
                
# TABLE INFO

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("$namefield","b.Name","s.SubsidyAmount","picadmin","s.DateModified","PayStatus");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
$li->column_array = array(0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,1);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width=25% >".$li->column($pos++, $i_UserStudentName)."</td>\n";
$li->column_list .= "<td width=25% >".$li->column($pos++, $i_Payment_Field_PaymentItem)."</td>\n";
$li->column_list .= "<td width=15% >".$li->column($pos++, $i_Payment_Subsidy_Amount)."</td>\n";
$li->column_list .= "<td width=15% >".$li->column($pos++, $i_Payment_Subsidy_Unit_Admin)."</td>\n";
$li->column_list .= "<td width=20% >".$li->column($pos++, $i_Payment_Subsidy_Unit_UpdateTime)."</td>\n";
$li->column_list .= "<td width=20% >".$li->column($pos++, $i_general_status)."</td>\n";

$toolbar= $linterface->GET_LNK_PRINT("javascript:openPrintPage(document.form1,'list_print.php')","","","","",0);
$toolbar.= $linterface->GET_LNK_EXPORT("javascript:exportPage(document.form1,'list_export.php')","","","","",0);


$searchbar = "<input class=\"textboxnum\" type=\"text\" name=\"keyword\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= $linterface->GET_BTN($button_find, "button", "document.form1.submit();","submit2");


$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "SourceOfSubsidySettings";
$TAGS_OBJ[] = array($i_Payment_Subsidy_Setting, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($unit_name);


$linterface->LAYOUT_START();


?>
<script language='javascript'>
function openPrintPage(obj,url){
/*	
	old_url = obj.action;
	old_target=obj.target;
	obj.action=url;
	obj.target='_blank';
	obj.submit();
	
	obj.action = old_url;
	obj.target = old_target;
*/
	url+='?pageNo='+obj.pageNo.value;
	url+='&order='+obj.order.value;
	url+='&field='+obj.field.value;
	url+='&page_size_change='+obj.page_size_change.value;
	url+='&numPerPage='+obj.numPerPage.value;
	url+='&UnitID='+obj.UnitID.value;
	url+='&keyword='+obj.keyword.value;
	url+='&paid_status='+obj.paid_status.value;
	newWindow(url,10);
}
function exportPage(obj,url){
        old_url = obj.action;
        obj.action=url;
        obj.submit();
        obj.action = old_url;

}
</script>
<form name="form1" method="get">
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
</table>
<!-- Summary table -->
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr><td colspan='2'><?=$linterface->GET_NAVIGATION2($i_Payment_ItemSummary)?></td></tr>
<!--	<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Payment_Subsidy_Unit?></td><td valign="top" nowrap="nowrap" class="tabletext"><?=$unit_name?></td></tr> -->
	<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Payment_Subsidy_Total_Amount?></td><td valign="top" nowrap="nowrap" class="tabletext" width=68%><?=$unit_total_amount?></td></tr>
	<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Payment_Subsidy_Used_Amount?></td><td valign="top" nowrap="nowrap" class="tabletext" ><?=$unit_used_amount?></td></tr>
	<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Payment_Subsidy_Left_Amount?></td><td valign="top" nowrap="nowrap" class="tabletext" ><?=$unit_left_amount?></td></tr>
	<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Payment_Subsidy_Search_Amount?></td><td valign="top" nowrap="nowrap" class="tabletext" ><?=("$search_amount ($no_of_students $i_Payment_Students)")?></td></tr>
	<tr>
		<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	</tr>
</table>
<!-- end summary table -->

<BR>
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?></td>
		<td align="right"><?=$SysMsg?></td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td align="left"><?= $select_status; ?><?=$searchbar?></td>
		<td valign="bottom" align="right">
		</td>
	</tr>
</table><?php echo $li->display("96%"); ?>
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td class="tabletextremark">
			<?=$i_Payment_Note_StudentRemoved?>
		</td>
	</tr>
</table>
<BR>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<input type=hidden name=UnitID value='<?=$UnitID?>'>
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
