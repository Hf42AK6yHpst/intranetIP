<?php 
# using: 

################# Change Log [Start] ############
#	Date:	2019-08-19 (Carlos)
#			added setting ["Pay at school" display word at eClass Parent App].
#
#	Date:	2019-07-31 (Carlos)
#			added setting [Enable Top-up in eCalss Parent App].
#
#	Date:	2019-06-11 (Carlos)
#			added setting [Payment Notice can pay at school].
#
#	Date:	2015-08-26 (Carlos)
#			KIS - added setting [Show alert message of pending payments for parent users].
#
#	Date:	2013-10-28 (Carlos)
#			KIS - hard code PaymentNoticeFeature to no
#	
#	Date:	2011-09-15 (YatWoon)
#			add option "Allow student choose option for need receipt or not" ($special_feature['AllowStudentChooseReceipt']) 
#			change the layout to IP25 standard
#
################# Change Log [End] ############

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) 
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance'];
$isKISPlatform = $_SESSION["platform"]=="KIS";

$linterface = new interface_html();
$CurrentPageArr['ePayment'] = true;
$CurrentPage['SystemProperty'] = true;	// Left Menu
$lpayment = new libpayment();

# Left menu 
$TAGS_OBJ[] = array($Lang['ePayment']['SystemProperties']);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START($Lang['ePayment'][$msg]);

$GeneralSetting = new libgeneralsettings();
$Settings = $GeneralSetting->Get_General_Setting('ePayment');
		
?>

<script language="javascript">
<!--
function EditInfo()
{
	$('#EditBtn').attr('style', 'visibility: hidden'); 
	$('.Edit_Hide').attr('style', 'display: none');
	$('.Edit_Show').attr('style', '');
	
	$('#UpdateBtn').attr('style', '');
	$('#cancelbtn').attr('style', '');
}		

function ResetInfo()
{
	document.form1.reset();
	$('#EditBtn').attr('style', '');
	$('.Edit_Hide').attr('style', '');
	$('.Edit_Show').attr('style', 'display: none');
	
	$('#UpdateBtn').attr('style', 'display: none');
	$('#cancelbtn').attr('style', 'display: none');
}
//-->
</script>
		
<form name="form1" method="post" action="edit_update.php">

<div class="table_board">
	<div class="table_row_tool row_content_tool">
		<?= $linterface->GET_SMALL_BTN($button_edit, "button","javascript:EditInfo();","EditBtn")?>
	</div>
	<p class="spacer"></p>
	<div class="form_sub_title_v30"><em>- <span class="field_title"> <?=$eDiscipline['SettingName']?> </span>-</em>
		<p class="spacer"></p>
	</div>
	<?php if($isKIS){ ?>
		<input type="hidden" name="PaymentNoticeEnable" value="0" />
	<?php } ?>
	<table class="form_table_v30">
	<?php if(!$isKIS){ ?>
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['ePayment']['PaymentNoticeFeature']?></td>
				<td> 
				<span class="Edit_Hide"><?=$Settings['PaymentNoticeEnable'] ? $i_general_yes:$i_general_no?></span>
				<span class="Edit_Show" style="display:none">
				<input type="radio" name="PaymentNoticeEnable" value="1" onclick="$('#PayAtSchoolTr').show();$('input[name=PaymentNoticeCanPayAtSchool]:checked').val()==1?$('#PayAtSchoolDisplayWordTr').show():$('#PayAtSchoolDisplayWordTr').hide();" <?=$Settings['PaymentNoticeEnable'] ? "checked":"" ?> id="disabled1"> <label for="disabled1"><?=$i_general_yes?></label> 
				<input type="radio" name="PaymentNoticeEnable" value="0" onclick="$('#PayAtSchoolTr').hide();$('#PayAtSchoolDisplayWordTr').hide();" <?=$Settings['PaymentNoticeEnable'] ? "":"checked" ?> id="disabled0"> <label for="disabled0"><?=$i_general_no?></label>
				</span></td>
			</tr>
			<tr id="PayAtSchoolTr" valign="top"<?=($Settings['PaymentNoticeEnable']==1?'':' style="display:none;"')?>>
				<td class="field_title" nowrap><?=$Lang['ePayment']['PaymentNoticeCanPayAtSchool']?></td>
				<td> 
				<span class="Edit_Hide"><?=$Settings['PaymentNoticeCanPayAtSchool'] ? $i_general_yes:$i_general_no?></span>
				<span class="Edit_Show" style="display:none">
				<input type="radio" name="PaymentNoticeCanPayAtSchool" value="1" onclick="$('#PayAtSchoolDisplayWordTr').show();" <?=$Settings['PaymentNoticeCanPayAtSchool']? "checked":"" ?> id="PaymentNoticeCanPayAtSchoolYes"> <label for="PaymentNoticeCanPayAtSchoolYes"><?=$i_general_yes?></label> 
				<input type="radio" name="PaymentNoticeCanPayAtSchool" value="0" onclick="$('#PayAtSchoolDisplayWordTr').hide();" <?=$Settings['PaymentNoticeCanPayAtSchool']? "":"checked" ?> id="PaymentNoticeCanPayAtSchoolNo"> <label for="PaymentNoticeCanPayAtSchoolNo"><?=$i_general_no?></label>
				</span></td>
			</tr>
			<tr id="PayAtSchoolDisplayWordTr" valign="top"<?=($Settings['PaymentNoticeEnable']==1 && $Settings['PaymentNoticeCanPayAtSchool']==1?'':' style="display:none;"')?>>
				<td class="field_title" nowrap><?=$Lang['ePayment']['PayAtSchoolDisplayWordAtParentApp']?></td>
				<td>
					<span class="Edit_Hide">
					<?php
					echo $Lang['General']['English'].': '.Get_String_Display($Settings['PaymentNoticePayAtSchoolDisplayWordEng']).' / '.$Lang['General']['Chinese'].': '.Get_String_Display($Settings['PaymentNoticePayAtSchoolDisplayWordChi']);
					?>
					</span>
					<span class="Edit_Show" style="display:none">
					<?php
					echo $Lang['General']['English'].': '.$linterface->GET_TEXTBOX_NAME('PaymentNoticePayAtSchoolDisplayWordEng', 'PaymentNoticePayAtSchoolDisplayWordEng', $Settings['PaymentNoticePayAtSchoolDisplayWordEng']);
					echo ' / ';
					echo $Lang['General']['Chinese'].': '.$linterface->GET_TEXTBOX_NAME('PaymentNoticePayAtSchoolDisplayWordChi', 'PaymentNoticePayAtSchoolDisplayWordChi', $Settings['PaymentNoticePayAtSchoolDisplayWordChi']);
					?>
					</span>
				</td>
			</tr>
			<?php if($lpayment->isEWalletTopUpEnabled()){ ?>
			<tr valign="top">
				<td class="field_title" nowrap><?=$Lang['ePayment']['EnableTopUpInApp']?></td>
				<td> 
				<span class="Edit_Hide"><?=$Settings['EnableTopUpInApp'] ? $i_general_yes:$i_general_no?></span>
				<span class="Edit_Show" style="display:none">
				<input type="radio" name="EnableTopUpInApp" value="1" <?=$Settings['EnableTopUpInApp'] ? "checked":"" ?> id="EnableTopUpInAppYes"> <label for="EnableTopUpInAppYes"><?=$i_general_yes?></label> 
				<input type="radio" name="EnableTopUpInApp" value="0" <?=$Settings['EnableTopUpInApp'] ? "":"checked" ?> id="EnableTopUpInAppNo"> <label for="EnableTopUpInAppNo"><?=$i_general_no?></label>
				</span></td>
			</tr>
			<?php } ?>
	<?php } ?>		
			<? if($special_feature['AllowStudentChooseReceipt']) {?>
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['ePayment']['AllowStudentChooseReceipt']?></td>
				<td> 
				<span class="Edit_Hide"><?=$Settings['AllowStudentChooseReceipt'] ? $i_general_yes:$i_general_no?></span>
				<span class="Edit_Show" style="display:none">
				<input type="radio" name="AllowStudentChooseReceipt" value="1" <?=$Settings['AllowStudentChooseReceipt'] ? "checked":"" ?> id="AllowStudentChooseReceipt1"> <label for="AllowStudentChooseReceipt1"><?=$i_general_yes?></label> 
				<input type="radio" name="AllowStudentChooseReceipt" value="0" <?=$Settings['AllowStudentChooseReceipt'] ? "":"checked" ?> id="AllowStudentChooseReceipt0"> <label for="AllowStudentChooseReceipt0"><?=$i_general_no?></label>
				</span></td>
			</tr>
			<? } ?>
	<?php if($isKISPlatform){ ?>
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['ePayment']['ShowOutstandingPaymentAlertMessageForParent']?></td>
				<td> 
					<span class="Edit_Hide"><?=$Settings['OutstandingPaymentAlert'] ? $i_general_yes:$i_general_no?></span>
					<span class="Edit_Show" style="display:none">
						<input type="radio" name="OutstandingPaymentAlert" value="1" <?=$Settings['OutstandingPaymentAlert'] ? "checked":"" ?> id="OutstandingPaymentAlert1"> <label for="OutstandingPaymentAlert1"><?=$i_general_yes?></label> 
						<input type="radio" name="OutstandingPaymentAlert" value="0" <?=$Settings['OutstandingPaymentAlert'] ? "":"checked" ?> id="OutstandingPaymentAlert0"> <label for="OutstandingPaymentAlert0"><?=$i_general_no?></label>
					</span>
				</td>
			</tr>
	<?php } ?>		
	</table>                        
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($button_update, "submit","","UpdateBtn", "style='display: none'") ?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "ResetInfo();","cancelbtn", "style='display: none'") ?>
		<p class="spacer"></p>
	</div>
</div>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>