<?php
// Editing by 
/*
 * 2019-08-19 (Carlos): Added setting ["Pay at school" display word at eClass Parent App].
 * 2019-07-31 (Carlos): Added setting [Enable Top-up in eCalss Parent App].
 * 2019-06-11 (Carlos): Added post data [PaymentNoticeCanPayAtSchool].
 * 2015-08-26 (Carlos): KIS - added setting [Show alert message of pending payments for parent users].
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$isKISPlatform = $_SESSION["platform"]=="KIS";

$lpayment = new libpayment();
$GeneralSetting = new libgeneralsettings();

$SettingList['PaymentNoticeEnable'] = $_POST['PaymentNoticeEnable'];
$SettingList['PaymentNoticeCanPayAtSchool'] = $_POST['PaymentNoticeCanPayAtSchool']? '1' : '0';
$SettingList['PaymentNoticePayAtSchoolDisplayWordEng'] = trim($_POST['PaymentNoticePayAtSchoolDisplayWordEng']);
$SettingList['PaymentNoticePayAtSchoolDisplayWordChi'] = trim($_POST['PaymentNoticePayAtSchoolDisplayWordChi']);
if($lpayment->isEWalletTopUpEnabled()){
	$SettingList['EnableTopUpInApp'] = $_POST['EnableTopUpInApp']? '1' : '0';
}
if($special_feature['AllowStudentChooseReceipt'])
	$SettingList['AllowStudentChooseReceipt'] = $_POST['AllowStudentChooseReceipt'];
if($isKISPlatform){
	$SettingList['OutstandingPaymentAlert'] = $_POST['OutstandingPaymentAlert'];
}

$GeneralSetting->Save_General_Setting('ePayment',$SettingList);

intranet_closedb();

header('location: index.php?msg=SettingApplySuccess');
?>