<?php
// Editing by 
ini_set('memory_limit','128M');
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ttmss_custom_report']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$intranet_session_language = 'b5';


$ttmss_payment_receipt_data = $sys_custom['ttmss_payment_receipt_data'][$sys_custom['ttmss_payment_receipt_code']];

$linterface = new interface_html();
$lpayment = new libpayment();
$POS = new libpos();

require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");

$margin= 10; // mm
$margin_top = $margin;
$margin_bottom = $margin;
$margin_left = $margin;
$margin_right = $margin;
$margin_top = 40;




//$school_name = GET_SCHOOL_NAME();
$school_name = $ttmss_payment_receipt_data['school_name'];

$record_data = array();


$amountConds = " IF(pil.ItemLogID IS NULL,ppdr.VoidAmount, pil.Amount) < ppdr.ItemQty";
$voidConds = "AND pt.VoidBy IS NULL AND pt.VoidLogID IS NULL";

$conds = "";
if ($FromDate != "" && $ToDate != "") {
	$conds .= " AND ppdr.DateInput between '$FromDate' and '$ToDate 23:59:59'";
}

$sql  = "SELECT
					pt.LogID 
				from 
					POS_ITEM as pi
					inner join
					PAYMENT_PURCHASE_DETAIL_RECORD as ppdr
					on pi.ItemID = ppdr.ItemID
					inner join
					POS_TRANSACTION as pt 
					on pt.LogID = ppdr.TransactionLogID and ppdr.InvoiceNumber = pt.InvoiceNumber 
						$conds
					left join PAYMENT_OVERALL_TRANSACTION_LOG as potl
					on pt.LogID = potl.LogID AND pt.VoidBy IS NULL AND pt.VoidDate IS NULL AND pt.VoidLogID IS NULL 
					left join PAYMENT_OVERALL_TRANSACTION_LOG as potl2 
					on potl2.LogID=pt.VoidLogID AND pt.VoidBy IS NOT NULL AND pt.VoidDate IS NOT NULL AND pt.VoidLogID IS NOT NULL 
					LEFT JOIN POS_ITEM_LOG as pil ON pil.ItemID=ppdr.ItemID AND pil.LogType=2 AND pil.RefLogID=potl2.LogID 
					LEFT JOIN 
					INTRANET_USER as u
					on potl.StudentID = u.UserID or potl2.StudentID = u.UserID
					LEFT JOIN
					INTRANET_ARCHIVE_USER as au
					on potl.StudentID = au.UserID or potl2.StudentID = au.UserID 
				WHERE
					$amountConds
                    $voidConds
				Group By 
					pt.LogID
					";

$logid_array = $POS->returnVector($sql);

$namefield = getNameFieldByLang("u.");
$archive_namefield = "au.ChineseName";

$total_amount = 0;
$temp_result = array();
foreach($logid_array as $log_id) {

	$amountConds = "AND IF(pil.ItemLogID IS NULL,ppdr.VoidAmount, pil.Amount) < ppdr.ItemQty";
	$voidConds = "AND pt.VoidBy IS NULL AND pt.VoidLogID IS NULL";

	$sql = "SELECT
					ppdr.InvoiceNumber,
					IF(u.UserID IS NULL,CONCAT('<i>',au.ClassName,'</i>'),u.ClassName) as ClassName,
					IF(u.UserID IS NULL,CONCAT('<i>',au.ClassNumber,'</i>'),u.ClassNumber) as ClassNumber,
					IF(u.UserID IS NULL,CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), IF(au.UserID IS NULL AND u.UserID IS NULL,'<font color=red>*</font>',$namefield)) as Name,
					pi.ItemName,
					IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount) as ItemQty,
					ROUND(ppdr.ItemSubTotal," . $POS->DecimalPlace . ") as ItemSubTotal,
					ROUND(ppdr.ItemSubTotal*IF(pil.ItemLogID IS NULL,ppdr.ItemQty-ppdr.VoidAmount, ppdr.ItemQty-pil.Amount)," . $POS->DecimalPlace . ") as GrandTotal,
					IF(potl.LogID IS NOT NULL, potl.REFCode, potl2.REFCode) as RefCode,
					DATE_FORMAT(ppdr.DateInput,'%Y-%m-%d') as DateInput,
					ppdr.DateInput as SortTime,
					c.CategoryName,
					ppdr.ReceiveAmount as ReceiveAmount,
					pt.LogID as LogID,
					pi.Barcode,
					c.CategoryCode,
					rn.ReceiptID,
					ppdr.PaymentMethod,
					ppdr.Remark
				from 
					POS_ITEM as pi 
					inner join POS_ITEM_CATEGORY as c on c.CategoryID=pi.CategoryID 
					inner join 
					PAYMENT_PURCHASE_DETAIL_RECORD as ppdr
					on pi.ItemID = ppdr.ItemID
					inner join
					POS_TRANSACTION as pt
					on pt.LogID = ppdr.TransactionLogID and ppdr.InvoiceNumber = pt.InvoiceNumber 
					left join PAYMENT_OVERALL_TRANSACTION_LOG as potl
					on pt.LogID = potl.LogID AND pt.VoidBy IS NULL AND pt.VoidDate IS NULL AND pt.VoidLogID IS NULL 
					left join PAYMENT_OVERALL_TRANSACTION_LOG as potl2 
					on potl2.LogID=pt.VoidLogID AND pt.VoidBy IS NOT NULL AND pt.VoidDate IS NOT NULL AND pt.VoidLogID IS NOT NULL 
					LEFT JOIN POS_ITEM_LOG as pil ON pil.ItemID=ppdr.ItemID AND pil.LogType=2 AND pil.RefLogID=potl2.LogID 
					LEFT OUTER JOIN PAYMENT_PAYMENT_RECEIPT_NUMBER_ITEM as rn_item ON (rn_item.PaymentLogID=pt.LogID AND rn_item.ReceiptID = (SELECT MAX(ReceiptID) FROM PAYMENT_PAYMENT_RECEIPT_NUMBER_ITEM WHERE PaymentLogID=pt.LogID))
	          		LEFT OUTER JOIN PAYMENT_PAYMENT_RECEIPT_NUMBER as rn ON (rn.ReceiptID = rn_item.ReceiptID AND rn.ReceiptType='ePOS')
					LEFT JOIN
					INTRANET_USER as u
					on potl.StudentID = u.UserID OR potl2.StudentID = u.UserID
					LEFT JOIN
					INTRANET_ARCHIVE_USER as au
					on potl.StudentID = au.UserID OR potl2.StudentID = au.UserID
				WHERE 
					pt.LogID='$log_id' 
					$conds
                    $amountConds
                    $voidConds 
				GROUP BY ppdr.RecordID";


	$result = $POS->returnArray($sql);
	foreach($result as $data) {
		$temp = array();
		$temp['SortTime'] = $data['SortTime'];
		$temp['TransactionTime'] = $data['DateInput'];
		$temp['ClassName'] = $data['ClassName'];
		$temp['ClassNumber'] = $data['ClassNumber'];
		$temp['Name'] = $data['Name'];
		$temp['CatCode'] = $data['CategoryCode'];
		$temp['ItemName'] = $data['ItemName'];
		$temp['Qty'] = $data['ItemQty'];
		$temp['SubTotal'] = $data['GrandTotal'];
		$temp['Remark'] = $data['Remark'];
		$temp['PaymentMethod_code'] = $data['PaymentMethod'];
		$temp['PaymentMethod'] = $sys_custom['ePayment']['PaymentMethodItems'][$data['PaymentMethod']];
		/*if($data['ReceiptID'] == '') {
			$receipt_code = '';
		} else {
			$receipt_code = $ttmss_payment_receipt_data['code'] . "-" . str_pad($data['ReceiptID'], 6, '0', STR_PAD_LEFT);
		}*/
		$temp['ReceiptNumber'] = $data['InvoiceNumber'];
		$record_data[] = $temp;
	}
}

$date_field = "a.PaidTime";
$conds = "";
if ($FromDate != "")
{
	$conds .= " AND $date_field >= '$FromDate'";
}
if ($ToDate != "")
{
	$conds .= " AND $date_field <= '$ToDate 23:59:59'";
}

$sql = "SELECT 
						b.ItemCode,
						c.Name as cName, 
						c.Description as cDescription,
						b.Name as ItemName,
						b.Description, 
						DATE_FORMAT(a.PaidTime,'%Y-%m-%d') as PaidTime,
						a.PaidTime as SortTime,
						a.Amount, 
						d.RefCode,
						d.RelatedTransactionID, 
						c.Description,
						a.Amount as sub_total,
						IF(u.UserID IS NULL,au.ClassName,u.ClassName) as ClassName,
						IF(u.UserID IS NULL,au.ClassNumber,u.ClassNumber) as ClassNumber,
						IF(u.UserID IS NULL,CONCAT('*',$archive_namefield), IF(au.UserID IS NULL AND u.UserID IS NULL,'*',$namefield)) as Name,
						a.ReceiptRemark,
						rn.ReceiptID as ReceiptID,
						c.CatCode,
						a.PaymentMethod
			FROM 
				PAYMENT_PAYMENT_ITEMSTUDENT as a
			  	inner join PAYMENT_OVERALL_TRANSACTION_LOG as d on (d.TransactionType=2 and d.RelatedTransactionID=a.PaymentID)
	         	LEFT OUTER JOIN PAYMENT_PAYMENT_ITEM as b ON a.ItemID = b.ItemID
	          	LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY as c ON b.CatID = c.CatID 
				LEFT OUTER JOIN PAYMENT_PAYMENT_RECEIPT_NUMBER_ITEM as rn_item ON (rn_item.PaymentLogID=a.PaymentID AND rn_item.ReceiptID = (SELECT MAX(ReceiptID) FROM PAYMENT_PAYMENT_RECEIPT_NUMBER_ITEM WHERE PaymentLogID=a.PaymentID))
	          	LEFT OUTER JOIN PAYMENT_PAYMENT_RECEIPT_NUMBER as rn ON (rn.ReceiptID = rn_item.ReceiptID AND rn.ReceiptType='ePayment')
	          	LEFT JOIN
					INTRANET_USER as u
					on a.StudentID = u.UserID OR d.StudentID = u.UserID
					LEFT JOIN
					INTRANET_ARCHIVE_USER as au
					on a.StudentID = au.UserID OR d.StudentID = au.UserID
	        WHERE 
	        	 a.RecordStatus = 1 AND d.TransactionType=2 
	        	AND	a.PaymentID Not IN (
					SELECT 
							a.PaymentID
					FROM 
					PAYMENT_OVERALL_TRANSACTION_LOG d
					INNER JOIN PAYMENT_PAYMENT_ITEMSTUDENT as a ON (d.TransactionType=6 and d.RelatedTransactionID=a.PaymentID)
					WHERE 
						d.TransactionType=6 
				)
				$conds
				GROUP BY d.RelatedTransactionID";


$payment_itemlist = $lpayment->returnArray($sql);

foreach($payment_itemlist as $data) {

	$temp = array();
	$temp['SortTime'] = $data['SortTime'];
	$temp['TransactionTime'] = $data['PaidTime'];
	$temp['ClassName'] = $data['ClassName'];
	$temp['ClassNumber'] = $data['ClassNumber'];
	$temp['Name'] = $data['Name'];
	$temp['CatCode'] = $data['CatCode'];
	$temp['ItemName'] = $data['ItemName'];
	$temp['Qty'] = '1';
	$temp['SubTotal'] = $data['sub_total'];
	$temp['Remark'] = $data['ReceiptRemark'];
	$temp['PaymentMethod_code'] = $data['PaymentMethod'];
	$temp['PaymentMethod'] = $sys_custom['ePayment']['PaymentMethodItems'][$data['PaymentMethod']];
	/*if($data['ReceiptID'] == '') {
		$receipt_code = '';
	} else {
		$receipt_code = $ttmss_payment_receipt_data['code'] . "-" . str_pad($data['ReceiptID'], 6, '0', STR_PAD_LEFT);
	}*/
	$temp['ReceiptNumber'] = '';
	$record_data[] = $temp;
}


uasort($record_data, 'sort_by_transaction_time');
if($format == 0) { //pdf
	$pdf = new mPDF('', 'A4', 0, '', $margin_left, $margin_right, $margin_top, $margin_bottom, $margin_header, $margin_footer);
	$pdf->shrink_tables_to_fit = 1;

	$pdf->backupSubsFont = array('mingliu');
	$pdf->useSubstitutions = true;

	$pdf->DefHTMLHeaderByName(
		'header',
		'<div style="margin-top: 10;">
		<div style="text-align: center; font-size: 18;">' . $school_name . '</div>
		<div style="text-align: center; font-size: 18;">收費明細表</div>
		<table width="100%" style="font-weight: bold; font-size: 12; margin-bottom: 10;">
			<tr>
				<td style="text-align: right;" width="80">從交易日期:</td>
				<td style="text-align: left;">'.$FromDate.'</td>
				<td style="text-align: right;" width="150">日期:</td>
				<td style="text-align: left;" width="100">'.date("Y-m-d").'</td>
			</tr>
			<tr>
				<td style="text-align: right;" width="80">至交易日期:</td>
				<td style="text-align: left;">'.$ToDate.'</td>
				<td style="text-align: right;" width="150">時間:</td>
				<td style="text-align: left;" width="100">'.date("H:i:s").'</td>
			</tr>
			<tr>
				<td style="text-align: right;" width="80"></td>
				<td style="text-align: left;"></td>
				<td style="text-align: right;" width="150">頁次:</td>
				<td style="text-align: left;" width="100">{PAGENO}</td>
			</tr>
		</table>
		
		<table style="width: 100%; font-weight: bold; font-size: 11;">
			<tr>
				<td style="text-align: left; width: 10%;">交易日期</td>
				<td style="text-align: left; width: 8%;">學生班級</td>
				<td style="text-align: left; width: 12%;">收據編號</td>
				<td style="text-align: left; width: 8%;">姓名</td>
				<td style="text-align: left; width: 8%;">類別</td>
				<td style="text-align: left;">產品名稱</td>
				<td style="text-align: left; width: 12%;">備註</td>
				<td style="text-align: left; width: 7%;">付款形式</td>
				<td style="text-align: right; width: 6%;">數量</td>
				<td style="text-align: right; width: 6%;">收費</td>
			</tr>
		</table>
		<table style="width: 100%"><tr><td style ="border-bottom:2px solid black; width:100%;"> </td></tr></table>
	</div>
');

	$pdf->SetHTMLHeaderByName('header');

	$all_sub_total += $type_sub_total;
	$all_qty_total += $type_qty_total;
	$all_catcode_total = array();
	$all_paymethmod_total = array();
	foreach ($record_data as $item) {
		$all_catcode_total[$item['CatCode']] += $item['SubTotal'];
		$all_paymethmod_total[$item['PaymentMethod_code']] += $item['SubTotal'];
		$student_classname = ($item['ClassName']) ? ($item['ClassName'].'-'.$item['ClassNumber']) : '';
		$all_sub_total += $item['SubTotal'];
		$all_qty_total += $item['Qty'];
		$x = '<table width="100%" style="font-size: 11;">
			<tr>
				<td style="text-align: left; width: 10%;" valign="top">'.$item['TransactionTime'].'</td>
				<td style="text-align: left; width: 8%;" valign="top">'.$student_classname.'</td>
				<td style="text-align: left; width: 12%;" valign="top">'.$item['ReceiptNumber'].'</td>
				<td style="text-align: left; width: 8%;" valign="top">'.$item['Name'].'</td>
				<td style="text-align: left; width: 8%;" valign="top">'.$item['CatCode'].'</td>
				<td style="text-align: left;" valign="top">'.$item['ItemName'].'</td>
				<td style="text-align: left; width: 12%;" valign="top">'.$item['Remark'].'</td>
				<td style="text-align: left; width: 7%;" valign="top">'.$item['PaymentMethod'].'</td>
				<td style="text-align: right; width: 6%;" valign="top">'.$item['Qty'].'</td>
				<td style="text-align: right; width: 6%;" valign="top">'.number_format($item['SubTotal'], 2).'</td>
			</tr>
		</table>';
		$pdf->WriteHTML($x);

	}

	$x = '<table style="width: 100%"><tr><td style ="border-bottom:2px solid black; width:100%;"> </td></tr></table>';
	$pdf->WriteHTML($x);

	$x = '<table width="100%" style="font-weight: bold; font-size: 12;">
				<tr>
					<td style="text-align: right;">總計:</td>
					<td width="100" style="text-align: right;">' . $all_qty_total . '</td>
					<td width="100" style="text-align: right;">' . number_format($all_sub_total, 2) . '</td>
				</tr>
			</table>
			<table width="100%" style="font-size: 12;">
				<tr>
					<td> </td>
					<td width="200" style="border-bottom:2px solid black; border-top:2px solid black;"> </td>
				</tr>
			</table>';
	$pdf->WriteHTML($x);

	$x = '<table width="100%" style="font-size: 12; font-weight: bold; page-break-inside: avoid; margin-bottom: 20;">
					<tr>
						<td style="text-align: left;" valign="bottom" height="80">Bank-in-date: _______________________________________</td>
						<td style="text-align: right;" valign="bottom">Amount: _______________________________________</td>
					</tr>
					<tr>
						<td style="text-align: left;" valign="bottom" height="150">負責人簽署/日期: _______________________________________</td>
						<td style="text-align: right;" valign="bottom">校長簽署/日期: _______________________________________</td>
					</tr>
					<tr>
						<td colspan="2" style="text-align: center;">
							<table width="100%" style="font-size: 12;">
								<tr>
									<td width="300">
										<table width="100%" style="font-size: 12;">
											<tr>
												<td width="100%" style="border-bottom:2px solid black;"> </td>
											</tr>
										</table>																
									</td>
									<td style="font-size: 18; font-weight: bold;">報表完</td>
									<td width="300">
										<table width="100%" style="font-size: 12;">
											<tr>
												<td width="100%" style="border-bottom:2px solid black;"> </td>
											</tr>
										</table>																
									</td>
								</tr>
							</table>
						</td>					
					</tr>
				</table>';

	$pdf->WriteHTML($x);

	$p = '<table width="100%" style="border: 1px solid #000000;">';
	$index = 0;
	foreach($sys_custom['ePayment']['PaymentMethodItems'] as $key=>$value) {
		$temp = ($index == 0) ? '付款形式:' : '';
		$paymethmod_value = $all_paymethmod_total[$key];
		if($paymethmod_value == '') {
			$paymethmod_value = 0;
		}
		$p .= '<tr>
					<td width="80" style="text-align: right;">'.$temp.'</td>
					<td>'.$value.'</td>
					<td style="border-bottom:2px solid black;">'.number_format($paymethmod_value, 2).'</td>
				</tr>';
		$index++;
	}
	$p .= '</table>';

	$x = '<table width="100%" style="font-size: 12; font-weight: bold; page-break-inside: avoid; margin-bottom: 20;">
			<tr>
				<td width="50%" valign="top">'.$p.'</td>
				<td valign="top">
					<table width="100%" style="border: 1px solid #000000;">';

	$index = 0;
	foreach($all_catcode_total as $key=>$value) {
		$temp = ($index == 0) ? '繳付項目:' : '';
		$x .= '<tr>
					<td width="80" style="text-align: right;">'.$temp.'</td>
					<td>'.$key.'</td>
					<td style="border-bottom:2px solid black;">'.number_format($value, 2).'</td>
				</tr>';
		$index++;
	}

	$x .= '
				</table>
			</td>
		</tr>
	</table>';
	$pdf->WriteHTML($x);

	
	$pdf->Output('sales_detail_report_' . $FromDate . '_' . $ToDate . '.pdf', 'D');

} else { //csv
	$csv_data = array();
	$csv_data[] = array($school_name);
	$csv_data[] = array('收費明細表');
	$csv_data[] = array();
	$csv_data[] = array('從交易日期:', $FromDate, '日期:', date("Y-m-d"));
	$csv_data[] = array('至交易日期:', $ToDate, '時間:', date("H:i:s"));
	$csv_data[] = array();
	$csv_data[] = array('交易日期','班級及學號','收據編號','姓名','類別','產品名稱','備註','付款形式','數量','收費');

	$all_sub_total = 0;
	$all_qty_total = 0;
	$all_catcode_total = array();
	$all_paymethmod_total = array();
	foreach ($record_data as $item) {
		$all_catcode_total[$item['CatCode']] += $item['SubTotal'];
		$all_paymethmod_total[$item['PaymentMethod_code']] += $item['SubTotal'];
		$student_classname = ($item['ClassName']) ? ($item['ClassName'].' - '.$item['ClassNumber']) : '';
		$all_sub_total += $item['SubTotal'];
		$all_qty_total += $item['Qty'];

		$temp = array();
		$temp[] = $item['TransactionTime'];
		$temp[] = $student_classname;
		$temp[] = $item['ReceiptNumber'];
		$temp[] = $item['Name'];
		$temp[] = $item['CatCode'];
		$temp[] = $item['ItemName'];
		$temp[] = $item['Remark'];
		$temp[] = $item['PaymentMethod'];
		$temp[] = $item['Qty'];
		$temp[] = number_format($item['SubTotal'], 2);
		$csv_data[] = $temp;
	}

	$temp = array();
	$temp[] = '';
	$temp[] = '';
	$temp[] = '';
	$temp[] = '';
	$temp[] = '';
	$temp[] = '';
	$temp[] = '';
	$temp[] = '總數:';
	$temp[] = $all_qty_total;
	$temp[] = number_format($all_sub_total, 2);
	$csv_data[] = $temp;

	$csv_data[] = array();
	$csv_data[] = array();

	$csv_data[] = array('負責人簽署/日期:','','校長簽署/日期:');
	$csv_data[] = array('---------------------- 報表完 ----------------------');

	$csv_data[] = array();
	$csv_data[] = array();

	$index = 0;
	foreach($all_catcode_total as $key=>$value) {
		$temp = ($index == 0) ? '繳付項目:' : '';
		$csv_data[] = array('','','', $temp, $key, $value);
		$index++;
	}

	$csv_data[] = array();
	$index = 0;
	foreach($sys_custom['ePayment']['PaymentMethodItems'] as $key=>$value) {
		$temp = ($index == 0) ? '付款形式:' : '';
		$paymethmod_value = $all_paymethmod_total[$key];
		if($paymethmod_value == '') {
			$paymethmod_value = 0;
		}
		$csv_data[] = array('','','', $temp, $value, $paymethmod_value);
		$index++;
	}


	$lexport = new libexporttext();
	$filename = 'sales_detail_report_' . $FromDate . '_' . $ToDate . '.csv';
	$export_content = '';
	foreach($csv_data as $temp) {
		$export_content .= implode("\t", $temp)."\r\n";
	}
	$lexport->EXPORT_FILE($filename, $export_content);


}

intranet_closedb();

function sort_by_transaction_time($a, $b) {
	if ($a['SortTime'] == $b['SortTime']) {
		return 0;
	}
	return ($a['SortTime'] < $b['SortTime']) ? -1 : 1;
}
