<?php
// Editing by 
/*
 * 2020-01-06 Bill: get students including left students    [2019-1220-1119-59235]
 * 2018-01-02 Carlos: Created.
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	intranet_closedb();
	exit();
}

$lclass = new libclass();

switch($task)
{
	case 'getStudentSelectionByClassId':
	/*
	 * @params:
	 * $TargetClass : class id 
	 * $TargetStudent : student id, array or single value, for selected values
	 * $IsMultiple : multiple/single selection
	 */
		$classIDAry = (array)$TargetClass;
        // $students = $lclass->getStudentListByClassID($classIDAry, $selectFlag=1);
		$students = $lclass->getStudentListByClassID($classIDAry, $selectFlag=1, 1);
		$selection_html = getSelectByArray($students, ' id="TargetStudent" name="TargetStudent[]" '.($IsMultiple?' multiple size="10" ':''), $selected=$TargetStudent, $all=0, $noFirst=1, $FirstTitle="", $ParQuoteValue=1);
		echo $selection_html;
	break;
}

intranet_closedb();
?>