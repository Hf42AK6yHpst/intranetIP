<?php
// Editing by 
/*
 * 2015-11-23 (Carlos): $sys_custom['ePayment']['PaymentItemWithPOS'] - Created for Sin Meng.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ePayment']['PaymentItemWithPOS']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$use_magic_quotes = get_magic_quotes_gpc();

$lpayment = new libpayment();
$lclass = new libclass();
$fcm = new form_class_manage();
$fcm_ui = new form_class_manage_ui();
$linterface = new interface_html();


$field = ($field==='')? 0 : $field;
$order = ($order==='')? 1 : $order;
$pageNo = ($pageNo==='')? 1 : $pageNo;

if (isset($ck_epayment_payment_receipt_page_size) && $ck_epayment_payment_receipt_page_size != "")
{
	$page_size = $ck_epayment_payment_receipt_page_size;
}

$keyword = trim($keyword);
if($use_magic_quotes){
	$keyword = stripslashes($keyword);
}

$current_academic_year_id = Get_Current_Academic_Year_ID();
$AcademicYearId = isset($_REQUEST['AcademicYearId']) && $_REQUEST['AcademicYearId']!=''? intval($_REQUEST['AcademicYearId']) : $current_academic_year_id;
$academicYearObj = new academic_year($AcademicYearId);
$academic_year_start = date("Y-m-d", strtotime($academicYearObj->AcademicYearStart));
$academic_year_end = date("Y-m-d", strtotime($academicYearObj->AcademicYearEnd));

$StartDate = isset($_REQUEST['StartDate'])? date("Y-m-d",strtotime($_REQUEST['StartDate'])): $academic_year_start;
$EndDate = isset($_REQUEST['EndDate'])? date("Y-m-d",strtotime($_REQUEST['EndDate'])):$academic_year_end;

$YearClassId = $_REQUEST['YearClassId']!=''? intval($_REQUEST['YearClassId']): '';

$academic_year_list = $fcm->Get_Academic_Year_List();
//$academic_year_list = $fcm->Get_All_Academic_Year();
//$academic_year_selection = getSelectByArray($academic_year_list, ' id="AcademicYearId" name="AcademicYearId" onchange="academicYearChanged(this.options[this.selectedIndex].value);" ', $AcademicYearId, 0, 1);
$academic_year_selection = getSelectByArray($academic_year_list, ' id="AcademicYearId" name="AcademicYearId" onchange="document.form1.submit();" ', $AcademicYearId, 0, 1);
$year_class_selection = $fcm_ui->Get_YearClass_Selection('YearClassId', $YearClassId, 'document.form1.submit();', $AcademicYearId, 0);
//$year_class_selection = $lclass->getSelectClass(' id="YearClassId" name="YearClassId" onchange="document.form1.submit();" ',$YearClassId,2, $Lang['General']['All'], $AcademicYearId);

$name_field = Get_Lang_Selection('r.ChineseName','r.EnglishName');

$creator_name_field = getNameFieldByLang2("u.");
$creator_archived_name_field = getNameFieldByLang2("u1.");

$li = new libdbtable2007($field,$order,$pageNo);

$sql = "SELECT 
			CONCAT('<a href=\"javascript:void(0);\" onclick=\"viewReceipt(',r.RecordID,');\" title=\"".$Lang['Btn']['View']."\">',r.ReceiptNumber,'</a>') as ReceiptNumber,
			r.ClassName,
			r.ClassNumber,
			$name_field as StudentName,
			r.GeneratedDate,
			IF(u.UserID IS NOT NULL,$creator_name_field,$creator_archived_name_field) as CreatorName,
			CONCAT('<input type=\"checkbox\" name=\"RecordID[]\" value=\"',r.RecordID,'\" />') as Checkbox
		FROM PAYMENT_RECEIPT as r ";
if(isset($YearClassId) && $YearClassId != ''){
	$sql .= "INNER JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=r.UserID AND ycu.YearClassID='$YearClassId' 
			INNER JOIN YEAR_CLASS as yc On yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='$AcademicYearId' ";
}
$sql .= "LEFT JOIN INTRANET_USER as u ON u.UserID=r.GeneratedBy 
		LEFT JOIN INTRANET_ARCHIVE_USER as u1 ON u1.UserID=r.GeneratedBy 
		WHERE r.AcademicYearID='$AcademicYearId' AND (r.GeneratedDate BETWEEN '$StartDate 00:00:00' AND '$EndDate 23:59:59') ";
if($keyword != ''){
	$escaped_keyword = $li->Get_Safe_Sql_Query($keyword);
	$sql .= " AND (r.ClassName LIKE '%$escaped_keyword%' 
					OR r.ChineseName LIKE '%$escaped_keyword%' 
					OR r.EnglishName LIKE '%$escaped_keyword%'
					OR r.ReceiptNumber LIKE '%$escaped_keyword%')";
}

$li->sql = $sql;
$li->IsColOff = "IP25_table";
$li->field_array = array("ReceiptNumber", "r.ClassName,r.ClassNumber+0", "r.ClassNumber+0", "StudentName", "r.GeneratedDate", "CreatorName");
$li->column_array = array(18,18,18,18,18,18);
$li->wrap_array = array(0,0,0,0,0,0);

$pos = 0;
$li->column_list .= "<th width='1' class='num_check'>#</th>\n";
$li->column_list .= "<th width='20%'>".$li->column_IP25($pos++, $Lang['Payment']['ReceiptNumber'])."</th>\n";
$li->column_list .= "<th width='10%'>".$li->column_IP25($pos++, $i_ClassName)."</th>\n";
$li->column_list .= "<th width='10%'>".$li->column_IP25($pos++, $i_ClassNumber)."</th>\n";
$li->column_list .= "<th width='20%'>".$li->column_IP25($pos++, $i_UserStudentName)."</th>\n";
$li->column_list .= "<th width='20%'>".$li->column_IP25($pos++, $Lang['General']['DateInput'])."</th>\n";
$li->column_list .= "<th width='20%'>".$li->column_IP25($pos++, $Lang['General']['InputBy'])."</th>\n";
$li->column_list .= "<th width='1' class='num_check'>".$li->check("RecordID[]")."</th>\n";

$li->no_col = sizeof($li->field_array)+2;

$date_range_filter = '';
$date_range_filter .= $Lang['General']['From']."&nbsp;".$linterface->GET_DATE_PICKER("StartDate",$StartDate);
$date_range_filter .= $Lang['General']['To']."&nbsp;".$linterface->GET_DATE_PICKER("EndDate",$EndDate);
$date_range_filter .= "&nbsp;".$linterface->GET_SMALL_BTN($Lang['Btn']['Submit'], "submit", "document.form1.submit();", $ParName="btnSubmit", "", "", $Lang['Btn']['Submit']);

$btn_ary = array();
$btn_ary[] = array('other','javascript:viewReceipt();',$Lang['Btn']['Print'].' PDF','PrintPDFTool','');
$btn_ary[] = array('delete','javascript:deleteReceipt();',$Lang['Btn']['Delete'],'DeleteTool','');
$common_table_tool = $linterface->Get_DBTable_Action_Button_IP25($btn_ary);

$x  = $li->display("98%");
$x .= '<input type="hidden" name="pageNo" value="'.$li->pageNo.'" />';
$x .= '<input type="hidden" name="order" value="'.$li->order.'" />';
$x .= '<input type="hidden" name="field" value="'.$li->field.'" />';
$x .= '<input type="hidden" name="page_size_change" value="1" />';
$x .= '<input type="hidden" name="numPerPage" value="'.$li->page_size.'" />';

$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PrintPage_PaymentReceiptList";

$TAGS_OBJ[] = array($Lang['ePayment']['PaymentReceiptList'], "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();

$Msg = urldecode($Msg);
$linterface->LAYOUT_START($Msg);

?>
<link href="/templates/jquery/jquery.alerts.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="/templates/jquery/jquery.alerts.js"></script>
<style type="text/css">
tr.selected_row {background-color: #5cb85c;}
</style>
<script type="text/javascript" language="javascript">
//var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image();?>';
function doSearch(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		document.form1.submit();
	}
	else
		return false;
}

function viewReceipt(recordId)
{
	//newWindow('../../settings/payment_item/view_receipt.php', 24);
	var f = $(document.form_print);
	var recordIdAry = [];
	if(recordId && (typeof(recordId) == 'string' || typeof(recordId)=='number')){
		recordIdAry.push(recordId);
	}else{
		var cb = document.getElementsByName('RecordID[]');
		for(var i=0;i<cb.length;i++)
		{
			if(cb[i].checked){
				recordIdAry.push(cb[i].value);
			}
		}
	}
	if(recordIdAry.length == 0){
		jAlert(globalAlertMsg2);
		return;
	}
	var popup = window.open ('', 'intranet_popup', 'menubar,resizable,scrollbars,status,top=40,left=40,width=800,height=700');
	f.find('input').remove();
	for(var i=0;i<recordIdAry.length;i++)
	{
		f.append('<input name="RecordID[]" type="hidden" value="'+recordIdAry[i]+'" />');
	}
	var oldTarget = f.attr('target');
	f.attr('target', 'intranet_popup');
	f.submit();
	f.attr('target',oldTarget);
	f.find('input').remove();
}

function checkRows()
{
	var cbs = document.getElementsByName('RecordID[]');
	for(var i=0;i<cbs.length;i++)
	{
		if(cbs[i].checked){
			$(cbs[i]).closest('tr').removeClass('selected_row').addClass('selected_row');
		}else{
			$(cbs[i]).closest('tr').removeClass('selected_row');
		}
	}
}

function deleteReceipt()
{
	var recordIdAry = [];
	var cb = document.getElementsByName('RecordID[]');
	for(var i=0;i<cb.length;i++)
	{
		if(cb[i].checked){
			recordIdAry.push(cb[i].value);
		}
	}
	if(recordIdAry.length == 0){
		jAlert(globalAlertMsg2);
		return;
	}
	jConfirm('<?=$Lang['General']['JS_warning']['ConfirmDelete']?>','<?=$Lang['Btn']['Confirm']?>',function(choice){
		if(!choice) return;
		document.form1.action = 'delete.php';
		document.form1.submit();
	});
}

$(document).ready(function(){
	$('input[type=checkbox]').click(function(){
		checkRows();
	})
});
</script>
<form id="form1" name="form1" method="post" action="index.php" onsubmit="return false;">
<br />
<div class="content_top_tool">
	<div class="Conntent_search">
		<input id="keyword" name="keyword" type="text" value="<?=str_replace('"','&quot;',$keyword)?>" onkeyup="doSearch(event);" title="<?=$Lang['ePayment']['PaymentReceiptSearchHint']?>">
	</div>
	<br style="clear: both;">
</div>
<div class="table_board">
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
		<tbody>
			<tr class="table-action-bar">
				<td valign="bottom">
					<?=$academic_year_selection."&nbsp;".$year_class_selection."&nbsp;|&nbsp;".$date_range_filter?>
				</td>
				<td valign="bottom">
					<?=$common_table_tool?>
				</td>
			</tr>
			<tr>
				<td colspan="2"><?=$x?></td>
			</tr>
		</tbody>
	</table>
</div>
</form>
<form name="form_print" id="form_print" method="post" action="../../settings/payment_item/view_receipt.php"></form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>