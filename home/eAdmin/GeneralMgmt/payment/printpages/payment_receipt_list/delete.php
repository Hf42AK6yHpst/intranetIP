<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ePayment']['PaymentItemWithPOS']) {
	//header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();

$result = $lpayment->deleteReceiptRecords($RecordID);

header("Location: index.php?Msg=".($result?rawurlencode($Lang['General']['ReturnMessage']['DeleteSuccess']):rawurlencode($Lang['General']['ReturnMessage']['DeleteUnsuccess'])));

intranet_closedb();
?>