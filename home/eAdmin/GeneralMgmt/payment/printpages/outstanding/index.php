<?php
## Using By : 
##################################################################################################
## Modification Log
## 2016-11-21 Carlos
## - Added [Exclude Date] option for print letter.
##
## 2014-10-24 Carlos [ip2.5.5.10.1]: 
## - Add [All Payment Items] type
##
## 2014-07-16 Carlos: 
## - Add ItemType for filtering StartDate and EndDate conditions
##
## 2014-04-24 Carlos: 
## - Add eLibrary Plus Overdue Fine option [IncludeLibraryOverdueFine]
##
## 2013-12-23: Carlos
## - Add class option [Student without classes] 
##
## 2010-02-02: Carlos
## - Add option to allow user to choose whether to show the parent signature field or not 
##
## 2009-12-14: Max (200912101448)
## - Add option to include payment item which is not yet started
##################################################################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PrintPage_OustandingList";

$linterface = new interface_html();

$lclass = new libclass();
$button_select = $i_status_all;
$select_class = $lclass->getSelectClass("name=ClassName onChange=changeSelection(this.form)",$ClassName,"","","",true);

$format_array = array(
                      array(0,"Web"),
                      array(1,"CSV"),
                      array(2,"$i_Payment_PrintPage_PaymentLetter"));
$select_format = getSelectByArray($format_array, "name=format onChange='changeSelection(this.form)'",0,0,1);

$item_types = array();
$item_types[] = array('',$Lang['ePayment']['AllPaymentItems']);
$item_types[] = array('1',$Lang['ePayment']['OverduePaymentItems']);
$item_types[] = array('2',$Lang['ePayment']['NonOverduePaymentItems']);
$item_types[] = array('3',$Lang['ePayment']['StartedPaymentItems']);
$item_types[] = array('4',$Lang['ePayment']['NonStartedPaymentItems']);

$select_item_types = getSelectByArray($item_types, "name=\"ItemType\" ",'1',0,1);

$TAGS_OBJ[] = array($i_Payment_Menu_PrintPage_Outstanding, "", 0);

$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

?>
<script language="javascript">
function changeSelection(formObj){
		if(formObj==null) return;
		var objClassName = formObj.ClassName;
		var objFormat = formObj.format;
        var checkObj = formObj.ExcludeEmpty;
        var checkSignObj = formObj.ExcludeParentSignature;
        var checkExcludeDateObj = formObj.ExcludeDate;
        if(objClassName ==null || objFormat==null || checkObj==null)return;
        var vClass = objClassName.options[objClassName.selectedIndex].value;
   		var vFormat = objFormat.options[objFormat.selectedIndex].value;
        if(vClass=='' && vFormat != 2){ 
        	checkObj.disabled=false;
        }else {
                checkObj.checked = false;
                checkObj.disabled=true;
        }
        if(vFormat == 2){
        	checkSignObj.disabled=false;
        	checkExcludeDateObj.disabled=false;
        }else
        {
        	checkSignObj.checked=false;
        	checkSignObj.disabled=true;
        	checkExcludeDateObj.checked=false;
        	checkExcludeDateObj.disabled=true;
        }
}
/*
function checkform(obj)
{
         if (obj.format.value == 0 || obj.format.value==2)
         {
             obj.target = "_blank";
             if(obj.format.value==2){
	             obj.action='print_letter.php';
	         }else{
		         obj.action='print.php';
		     }
		     
         }
         else
         {
             obj.target = "";
         }
         return true;
}
*/
function checkform(obj)
{
         if (obj.format.value == 0 || obj.format.value==2)
         {
             obj.target = "_blank";
             if(obj.format.value==2){
	             obj.action='print_letter.php';
	         }else{
		         obj.action='print.php';
		     }
		     
         }
         else
         {
             obj.target = "";
             obj.action='print.php';
         }
         exclude = obj.ExcludeEmpty!=null && obj.ExcludeEmpty.checked ? 1 : 0;
         non_overdue = obj.IncludeNonOverDue !=null && obj.IncludeNonOverDue.checked ? 1 :0;
         obj.action+='?ExcludeEmpty='+exclude+'&IncludeNonOverDue='+non_overdue;
         if(obj.ExcludeParentSignature != null && obj.ExcludeParentSignature.checked)
         	obj.action+='&ExcludeSignature=1';
		
         if(obj.format.value==1)
         	obj.submit();
         else {
         	var OldTarget = obj.target;
         	obj.target = '_blank';
         	obj.submit();
         	
         	obj.target = OldTarget;
        }
         
}
function hide(objID) {
	document.getElementById(objID).style.display = 'none';
}
function show(objID) {
	document.getElementById(objID).style.display = '';
}
function isChecked(obj, callbackChecked, paraArr1, callbackUnchecked, paraArr2) {
	if(typeof(callbackChecked)=="function")
		for(i=0;i<paraArr1.length;i++){paraArr1[i]="'"+paraArr1[i]+"'";}
	if(typeof(callbackUnchecked)=="function")
		for(i=0;i<paraArr2.length;i++){paraArr2[i]="'"+paraArr2[i]+"'";}
	
	var callbackCheckedStr = "callbackChecked("+paraArr1.join(',')+");";
	var callbackUncheckedStr = "callbackUnchecked("+paraArr2.join(',')+");";
	if (obj.checked) {
		eval(callbackCheckedStr);
		return true;
	} else {
		eval(callbackUncheckedStr);
		return false;
	}
}

</script>
<form name="form1" id="form1" method="post" action="print.php" >
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
				<tr>
					<td class="tabletext" align="right"><?=$lpayment->getResponseMsg($msg)?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
    	<td>
    		<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_UserClassName?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<?=$select_class?>
	    			</td>
    			</tr>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_general_Format?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<?=$select_format?>
	    			</td>
    			</tr>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$Lang['ePayment']['PaymentItemType']?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<?=$select_item_types?>
	    			</td>
    			</tr>
    			<tr>
    				<td>&nbsp;</td>
	    			<td class="tabletext" width="70%">
	    				<input type="checkbox" name="ExcludeEmpty" id="ExcludeEmpty" value="1"><label for="ExcludeEmpty"><?=$i_Payment_Exclude?></label>
	    			</td>
    			</tr>
    			<tr>
    				<td>&nbsp;</td>
	    			<td class="tabletext" width="70%">
	    				<input type="checkbox" name="ExcludeDate" id="ExcludeDate" value="1" disabled><label for="ExcludeDate"><?=$Lang['ePayment']['ExcludeDate']?></label>
	    				<input type="checkbox" name="ExcludeParentSignature" id="ExcludeParentSignature" value="1" disabled><label for="ExcludeParentSignature"><?=$Lang['ePayment']['ExcludeParentSignature']?></label>
	    			</td>
    			</tr>
    			<!--
    			<tr>
    				<td>&nbsp;</td>
	    			<td class="tabletext" width="70%">
	    				<input type="checkbox" name="IncludeNonOverDue" id="IncludeNonOverDue" value="1" onClick="isChecked(this, show, ['IncludeNotStarted'], hide, ['IncludeNotStarted']);"><label for="IncludeNonOverDue"><?=$i_Payment_IncludeNonOverDue?></label>
	    			</td>
    			</tr>
    			<tr id="IncludeNotStarted" style="display: none;">
    				<td>&nbsp;</td>
    				<td>
    					<input type="checkbox" name="IncludeNotStarted" id="IncludeNotStarted" value="1"><label for="IncludeNotStarted"><?=$Lang['ePayment']['IncludeNotStarted']?></label>
					</td>
				</tr>
				-->
				<?php if($plugin['library_management_system'] && $sys_custom['ePayment']['OutstandingLibraryOverdueFine']){ ?>
				<tr>
    				<td>&nbsp;</td>
	    			<td class="tabletext" width="70%">
	    				<input type="checkbox" name="IncludeLibraryOverdueFine" id="IncludeLibraryOverdueFine" value="1"><label for="IncludeLibraryOverdueFine"><?=$Lang['ePayment']['IncludeLibraryOverdueFine']?></label>
	    			</td>
    			</tr>
    			<?php } ?>
    		</table>
    	</td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
			    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
				 <tr>
				    <td align="center">
						<?= $linterface->GET_ACTION_BTN($button_submit, "button", "javascript:checkform(document.getElementById('form1'))") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>

<?
$linterface->LAYOUT_STOP();
print $linterface->FOCUS_ON_LOAD("form1.ClassName");
intranet_closedb();
?>
