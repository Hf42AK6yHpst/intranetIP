<?php 
// Editing by 
/*
 * 2020-01-10 Ray:    Added sendPushMessageByBatch
 * 2019-09-23 Carlos: Removed auto fill items ($Payment_StartDate) and ($Payment_EndDate) from the message as it is not applicable.
 * 2017-10-27 Carlos: Modified to include ($Payment_Item) and ($Payment_Amount) in push message.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_template.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
$ldbsms 	= new libsmsv2();
$luser = new libuser();
$libeClassApp = new libeClassApp();

$itemName = $lpayment->returnPaymentItemName($ItemID);
$StudentIDAry =$_POST['StudentID'];
$InadequateBalanceAry =$_POST['InadequateAmount'];
$AccountBalanceAry = $_POST['AccountBalance'];
$OutstandingAmountAry = $_POST['OutstandingAmount'];
$StudentItemsAry = $_POST['StudentItems'];
$messageTitle = standardizeFormPostValue($_POST['PushMessageTitle']);
$messageContent = standardizeFormPostValue($_POST['MessageContent']);

$isPublic = "N";
//debug_pr($luser->getParentStudentMappingInfo($studentIds));
$parentStudentAssoAry = BuildMultiKeyAssoc($luser->getParentStudentMappingInfo($StudentIDAry), 'ParentID', $IncludedDBField=array('StudentID'), $SingleValue=1, $BuildNumericArray=1);

$i = 0;
foreach ($StudentIDAry as $studentId) {
	$thisStudent = new libuser($studentId);
	$appParentIdAry = $luser->getParentUsingParentApp($studentId);
	
	//	debug_pr($appParentIdAry);
	$_individualMessageInfoAry = array();
	foreach ($appParentIdAry as $parentId) {
		$_individualMessageInfoAry['relatedUserIdAssoAry'][$parentId] = (array)$studentId;
	}
	$_individualMessageInfoAry['messageTitle'] = $messageTitle;
	$_individualMessageInfoAry['messageContent'] = str_replace("[StudentName]", $thisStudent->UserName(), $messageContent);
	//$_individualMessageInfoAry['messageContent'] = $tempContent;
	
	$_individualMessageInfoAry['messageContent'] = str_replace('($Inadequate_Amount)',$InadequateBalanceAry[$i],$_individualMessageInfoAry['messageContent']);
	$_individualMessageInfoAry['messageContent'] = str_replace('($payment_outstanding_amount)',max($OutstandingAmountAry[$i],$OutstandingAmountAry[$i]-$AccountBalanceAry[$i]),$_individualMessageInfoAry['messageContent']);
	$_individualMessageInfoAry['messageContent'] = str_replace('($Payment_Amount)',$OutstandingAmountAry[$i],$_individualMessageInfoAry['messageContent']);
	$_individualMessageInfoAry['messageContent'] = str_replace('($payment_balance)',$AccountBalanceAry[$i],$_individualMessageInfoAry['messageContent']);
	$_individualMessageInfoAry['messageContent'] = str_replace('($Payment_Item)',base64_decode($StudentItems[$i]),$_individualMessageInfoAry['messageContent']);
	$_individualMessageInfoAry['messageContent'] = str_replace(array('($Payment_StartDate)','($Payment_EndDate)'),array('',''),$_individualMessageInfoAry['messageContent']);
	$_individualMessageInfoAry['messageContent'] = $ldbsms->replace_content($studentId, $_individualMessageInfoAry['messageContent'], '');
	
	$individualMessageInfoAry[$i] = $_individualMessageInfoAry;
	//debug_pr($_individualMessageInfoAry);
	$i++;
}

if ($libeClassApp->isEnabledSendBulkPushMessageInBatches()) {
	$notifyMessageId = $libeClassApp->sendPushMessageByBatch($individualMessageInfoAry, $messageTitle, 'MULTIPLE MESSAGES', $isPublic, $recordStatus = 1, $appType, $sendTimeMode, $sendTimeString);
} else {
	$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $messageTitle, 'MULTIPLE MESSAGES', $isPublic, $recordStatus = 1, $appType, $sendTimeMode, $sendTimeString);
}
$msg = ($notifyMessageId > 0)? $Lang['AppNotifyMessage']['ReturnMsg']['SendSuccess']:$Lang['AppNotifyMessage']['ReturnMsg']['SendFail'];

intranet_closedb();
echo $msg;
?>