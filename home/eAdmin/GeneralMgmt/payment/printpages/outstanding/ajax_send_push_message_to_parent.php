<?php 
// Editing by 
/*********************
 * 2019-09-23 Carlos: Exclude Payment_StartDate and Payment_EndDate from auto fill items. Added remark about it. 
 * 2017-10-27 Carlos: Modified to support fill-in items ($Payment_Item) and ($Payment_Amount) . 
 * 2017-06-26 Anna 
 * - Created this file
 * *******************/
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_template.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libAppTemplate = new libeClassApp_template();
$lpayment = new libpayment();

// if($plugin['eClassApp'] == true){
	$Module = "ePayment";
	# check Access Right
	$libAppTemplate->canAccess($Module,$Section);
	
	# Init libeClassApp_template
	$libAppTemplate->setModule($Module);
	$libAppTemplate->setSection($Section);
	
	$libAppTemplate->setExcludedTags(array('Payment_StartDate','Payment_EndDate'));
// }

$back_button = $linterface->GET_ACTION_BTN($button_cancel, "button", "js_Hide_ThickBox();");
$send_button = $linterface->GET_ACTION_BTN($button_send, "button","checkForm();");

// $student_balance = $lpayment->getBalanceInAssoc();
// $CurrentBalanceAry = array();
// foreach ($StudentIDAry as $StudentID){
// 	$CurrentBalanceAry[] = $student_balance[$StudentID];
// }
// $CurrentBalanceList =  implode(",",$CurrentBalanceAry);
// $InadequateBalanceAry = array_filter(explode(",",$_POST['InadequateBalance']));
// $InadequateBalanceList = implode(",",$InadequateBalanceAry);
?>
<form name="form1"  method="post" onsubmit="return checkForm();">
	<?=$linterface->Get_Thickbox_Return_Message_Layer()?>
	<table width="100%" align="left">
		<tr>
			<td>
				<table border="0" cellpadding="5" cellspacing="0" width="100%" align="center">
					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
							<?=$Lang['SMS']['MessageTemplates']?>
						</td>
						<td class="tabletext" width="70%">
							<?=$libAppTemplate->getMessageTemplateSelectionBox(true);?>
							<span id="AjaxStatus"></span>
						</td>
					</tr>
					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
							<?=$Lang['AppNotifyMessage']['Title']?> <span class="tabletextrequire">*</span>
						</td>
						<td class="tabletext" width="70%">
							<?=$linterface->GET_TEXTAREA("PushMessageTitle", $messageTitle, $taCols=60, $taRows=2)?>
							
						</td>
					</tr>
					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
							<?=$Lang['AppNotifyMessage']['Description']?> <span class="tabletextrequire">*</span>
						</td>
						<td class="tabletext" width="70%">
							<?=$linterface->GET_TEXTAREA("MessageContent", $messageContent,$taCols=60)?>
						</td>
					</tr>
					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
							<?=$Lang['eClassApp']['AutoFillItem']?>
						</td>
						<td class="tabletext" width="70%">
							<?=$libAppTemplate->getMessageTagSelectionBox();?>
						</td>
					</tr>
				</table>
			</td>
		</tr>	
		<tr>
			<td>
				<div>
					<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
						
						<tr>
							<span class="tabletextremark">*** <?=$Lang['ePayment']['AutoFillItemsPaymentItemStartDateEndDateAreNotApplicableToNotificationMessage']?></span>
						</tr>
						
						<tr>
					  		<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
					  	</tr>
						<tr>
						  <td align="center" colspan="2">
								<?= $send_button."&nbsp;".$back_button ?>
							</td>
						</tr>
						
					</table>
				</div>
			</td>
		</tr>
		<input type="hidden" name="Module" id="Module" value="<?=$Module?>">
		</table>
</form>		
<? include_once($PATH_WRT_ROOT."home/eClassApp/common/pushMessageTemplate/js_AppMessageReminder.php") ?>
<br />
<link rel=stylesheet  href="/templates/jquery/thickbox.css" />
<script type="text/javascript" language="JavaScript" src="/templates/jquery/thickbox.js"></script>
<script language="JavaScript" src="/templates/jquery/jquery.blockUI.js"></script>
<script language="JavaScript" type="text/javascript">
$(document).ready(function(){
	$('div#send_push_message').show();
	AppMessageReminder.initInsertAtTextarea();
	AppMessageReminder.TargetAjaxScript = 'ajax_send_reminder_update.php';
});
function checkForm() {
		var title = document.getElementById('PushMessageTitle');
		var titlename = title.value;
		var content = document.getElementById('MessageContent');
		if (title.value == "") {
			alert("<?=$Lang['AppNotifyMessage']['WarningMsg']['RequestInputTitle']?>");
			return false;
		} else if (content.value == "") {
			alert("<?=$Lang['AppNotifyMessage']['WarningMsg']['RequestInputDescription']?>");
			return false;
		} else {
			send_push_message(titlename,content.value);
		}
}
function send_push_message(titlename,content){
	Block_Thickbox();
	var studentIdObjs = document.getElementsByName("StudentID[]");
	var InadequateBalanceObjs= document.getElementsByName("InadequateBalance[]");
	var AccountBalanceObjs = document.getElementsByName("AccountBalance[]");
	var OutstandingAmountObjs = document.getElementsByName("OutstandingAmount[]");
	var StudentItemsObjs = document.getElementsByName("StudentItems[]");
	
	var studentIdAry = [];
	var InadequateBalanceAry = [];
	var AccountBalanceAry = [];
	var OutstandingAmountAry = [];
	var StudentItemsAry = [];
	for(var i=0;i<studentIdObjs.length;i++){
		if(studentIdObjs[i].checked){
			studentIdAry.push(studentIdObjs[i].value);
			InadequateBalanceAry.push(InadequateBalanceObjs[i].value);
			AccountBalanceAry.push(AccountBalanceObjs[i].value);
			OutstandingAmountAry.push(OutstandingAmountObjs[i].value);
			StudentItemsAry.push(StudentItemsObjs[i].value);
		}
	}
	$.post(
			'send_push_message.php',
 			{ 
				"InadequateAmount[]": InadequateBalanceAry,
				"AccountBalance[]": AccountBalanceAry,
				"OutstandingAmount[]": OutstandingAmountAry,
				"StudentItems[]": StudentItemsAry,
				"StudentID[]": studentIdAry,
				"PushMessageTitle":titlename,
				"MessageContent":content
			},
			function(returnmsg) {
				UnBlock_Thickbox();
				Get_Return_Message(returnmsg);
			}
		);	
}
</script>