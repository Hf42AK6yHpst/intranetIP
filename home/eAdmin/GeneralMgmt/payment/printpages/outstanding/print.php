<?php
## Using By : 
##########################################################
## Modification Log
## 2019-02-26 Carlos
## - Added [Select all students] and [Unselect all students] to quickly check/uncheck students for sending push message. 
## 2017-11-10 Carlos
## - All students that parent is using parent app can send push msg to. 
## 2017-10-27 Carlos
## - Modified to include ($Payment_Item) and ($Payment_Amount) in push message.
## 2017-06-26 Anna
## - Added send push message selection when have Inadequate Account Balance
## 2014-11-10 Carlos [ip2.5.5.12.1]:
## - Add sorting field DisplayOrder
## 2014-10-24 Carlos [ip2.5.5.10.1]: 
## - Add [All Payment Items] type
## 2014-09-02 Carlos: 
## - Sort by ClassName, ClassNumber, StudentName, RecordType 
## 2014-07-16 Carlos: 
## - Sort by casting ClassNumber to number ClassNumber+0 
## - Add ItemType for filtering StartDate and EndDate conditions
## 2014-04-24 Carlos: 
## - Added eLibrary Plus Overdue Fine records
## 2013-12-23: Carlos
## - Display students without classes
## 2013-10-29: Carlos
## - KIS - hide [Account Balance], [Inadequate Account Balance], [PPS Account No.]
## 2012-11-27: Carlos
## - Undo html special chars for item name for csv format
## 2009-12-14: Max (200912101448)
## - Add option to include payment item which is not yet started
##########################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();
$lpayment = new libpayment();
if (!$lpayment->IS_ADMIN_USER() || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$include_library_overdue_fine = $plugin['library_management_system'] && $sys_custom['ePayment']['OutstandingLibraryOverdueFine'] && $IncludeLibraryOverdueFine == '1';
if($include_library_overdue_fine){
	include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");
	include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
	
	$liblms = new liblms();
}

if($format!=1)
	include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
	#include_once($PATH_WRT_ROOT."templates/fileheader.php");

$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance'];

$lexport = new libexporttext();
$lpayment = new libpayment();

$linterface = new interface_html();



$ExcludeEmpty = (isset($_REQUEST['ExcludeEmpty']) && $_REQUEST['ExcludeEmpty'] == 1) ? 1 : 0;
$page_breaker = "<P CLASS='breakhere'>";
$namefield = getNameFieldByLang2("c.");

# Get Account Balance
$student_balance = $lpayment->getBalanceInAssoc();
/*
$IncludeNotStarted = $_POST['IncludeNotStarted'];
if ($IncludeNonOverDue == 1) {
	if ($IncludeNotStarted == 1) {
		
	}
	else {
		$cond = " AND b.StartDate <=CURDATE()";
	}
} else {
	$cond = " AND b.EndDate < CURDATE() ";
}
*/
if($ItemType == '1'){ // Overdue Items Only
	$cond = " AND b.EndDate < CURDATE() ";
}else if($ItemType == '2'){ // Non-Overdue Items Only
	$cond = " AND b.EndDate >= CURDATE() ";
}else if($ItemType == '4'){ // Non-Started Items Only
	$cond = " AND b.StartDate > CURDATE() ";
}else if($ItemType == '3'){ // Started, Overdue and Non-Overdue Items
	$cond = " AND b.StartDate <= CURDATE() ";
}// else all payment items

$sql = "SELECT
                $namefield as StudentName,
                IF(c.ClassName IS NULL OR c.ClassName='','-', c.ClassName) as ClassName,
                IF(c.ClassNumber IS NULL OR c.ClassNumber='','-',c.ClassNumber) as ClassNumber,
                b.Name,
                a.Amount,
                IF(c.ClassName IS NULL OR c.ClassName='','".$Lang['AccountMgmt']['StudentNotInClass']."', c.ClassName) as ClassName2,
                a.StudentID,
                d.PPSAccountNo,
                DATE_FORMAT(b.EndDate,'%Y-%m-%d') as DueDate,
				'ePayment Payment Item' as RecordType,
				b.DisplayOrder as DisplayOrder 
        FROM
                PAYMENT_PAYMENT_ITEMSTUDENT AS a
                INNER JOIN PAYMENT_PAYMENT_ITEM AS b ON a.ItemID = b.ItemID
                LEFT JOIN INTRANET_USER AS c ON a.StudentID = c.UserID
                LEFT JOIN PAYMENT_ACCOUNT as d ON c.UserID = d.StudentID
        WHERE
                a.RecordStatus <> 1 AND a.ItemID > 0 
                $cond ";

if($_REQUEST['ClassName'] == '-2'){
	$sql .= " AND (c.ClassName IS NULL OR c.ClassName='') ";
}else{
	$sql .= (!empty($_REQUEST['ClassName'])) ? " AND c.ClassName = '".$_REQUEST['ClassName']."'" : "";
}

if($include_library_overdue_fine){
	$student_name_field = Get_Lang_Selection("lu.ChineseName","lu.EnglishName");
	$sql .= "UNION 
			 SELECT 
				$student_name_field as StudentName,
				IF(lu.ClassName IS NULL OR lu.ClassName='','-',lu.ClassName) as ClassName, 
				IF(lu.ClassNumber IS NULL OR lu.ClassNumber='','-',lu.ClassNumber) as ClassNumber, 
				CONCAT(b.BookTitle,if(TRIM(bu.ACNO)<>'',CONCAT('[',bu.ACNO,']'),IF(TRIM(bu.ACNO_BAK)<>'',CONCAT('[',bu.ACNO_BAK,']'),'')),
					IF(ol.DaysCount is NULL,' (".$Lang["libms"]["book_status"]["LOST"].")',
						CONCAT(' (".str_replace('!','',$Lang["libms"]["CirculationManagement"]["msg"]["overdue1"])." ',ol.DaysCount,' ".$Lang["libms"]["CirculationManagement"]["msg"]["overdue2"].")'))
				) as Name,
				if(ol.PaymentReceived is null, ol.Payment, ol.Payment-ol.PaymentReceived) as Amount,
				IF(lu.ClassName IS NULL OR lu.ClassName='','".$Lang['AccountMgmt']['StudentNotInClass']."', lu.ClassName) as ClassName2,
				lu.UserID as StudentID,
				pa.PPSAccountNo,
				DATE_FORMAT(ol.DateCreated,'%Y-%m-%d') as DueDate,
				'eLibrary Plus Overdue Fine' as RecordType,
				ol.OverDueLogID as DisplayOrder  
			FROM ".$liblms->db.".LIBMS_OVERDUE_LOG as ol 
			INNER JOIN ".$liblms->db.".LIBMS_BORROW_LOG as bl ON bl.BorrowLogID=ol.BorrowLogID 
			INNER JOIN ".$liblms->db.".LIBMS_BOOK as b ON b.BookID=bl.BookID 
			INNER JOIN ".$liblms->db.".LIBMS_USER as lu ON lu.UserID=bl.UserID 
			LEFT JOIN ".$liblms->db.".LIBMS_BOOK_UNIQUE as bu ON bu.UniqueID=bl.UniqueID 
			LEFT JOIN PAYMENT_ACCOUNT as pa ON pa.StudentID=lu.UserID 
			WHERE ol.RecordStatus='OUTSTANDING' AND ol.IsWaived=0 ";
	if($_REQUEST['ClassName'] == '-2'){
		$sql .= " AND (lu.ClassName IS NULL OR lu.ClassName='') ";
	}else{
		$sql .= (!empty($_REQUEST['ClassName'])) ? " AND lu.ClassName = '".$_REQUEST['ClassName']."'" : "";
	}
}

$sql .= " ORDER BY ClassName, ClassNumber+0, StudentName, RecordType DESC, DisplayOrder ";
//debug_r($_REQUEST);
$data = $lpayment->returnArray($sql, 7);

$studentIdToItems = array();
for($i=0; $i<sizeof($data); $i++)
{
        list($sname, $sclassname, $sclassnumber, $itemname, $amount, $classname, $sid, $s_pps_no,$due_date) = $data[$i];
        if($format == 1){ // CSV undo html special chars
        	$itemname = handleCSVExportContent(intranet_undo_htmlspecialchars($itemname));
        }
        $s_pps_no = trim($s_pps_no);
        $s_pps_no = (($s_pps_no=="")?"--":$s_pps_no);
        $TotalRecord[$classname] += 1;
        $TotalAmount[$sid] += $amount;
        if(!isset($studentIdToItems[$sid])){
        	$studentIdToItems[$sid] = array();
        }
        $studentIdToItems[$sid][] = '['.$itemname.'('.$lpayment->getWebDisplayAmountFormat($amount).')]';
        $Payment[$classname][] = array($sname, $sclassname, $sclassnumber, $itemname, $amount, $sid, $s_pps_no,$due_date);
}

$classes = array();
if (empty($_REQUEST['ClassName']))
{
 	if ($intranet_version == "2.5" || $intranet_version == "3.0") {
	  $sql = "SELECT 
	  					yc.ClassTitleEN
	  				FROM
	            YEAR y
	            inner join
	  					YEAR_CLASS yc
	            on y.YearID = yc.YearID
	  				WHERE
	  					AcademicYearID = '".Get_Current_Academic_Year_ID()."'
	  				ORDER BY y.sequence, yc.sequence";
	}
	else {
		$sql = "SELECT 
	  					ClassName
	  				FROM
	  					INTRANET_CLASS  
	  				Where 
	  					RecordStatus = '1' 
	  				Order by 
	  					ClassName";
	}
	$classes = $lpayment->returnVector($sql);
}
else
{
	if($_REQUEST['ClassName'] != '-2'){
    	$classes[] = $_REQUEST['ClassName'];
	}
}

if($_REQUEST['ClassName'] == '' || $_REQUEST['ClassName'] == '-2'){
	$classes[] = $Lang['AccountMgmt']['StudentNotInClass'];
}

$display = "";

$title="<table border=0 width=95% align=center><tr><td class='tabletext'><b><font size=+1>$i_Payment_Menu_PrintPage_Outstanding</font></b></td></tr></table>";
for ($i=0; $i<sizeof($classes); $i++)
{
        $classname = $classes[$i];
        $x = "";
                $csv = "";
        if ($TotalRecord[$classname] > 0 || $ExcludeEmpty == 0)
        {		$x .="<form name='form1'>";
                $x .= "<table border=0 width=95% align=center><Tr><td class='tabletext'><p><strong><font size=+1>".$classname."</font></strong></p></td></tr></table>\n";
                $x .= "<table width=\"95%\" border=\"0\" cellpadding=\"4\" cellspacing=\"0\" class=\"eSporttableborder\" align=center>\n";
                $x .= "<tr>\n";
                        $x .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_UserStudentName</td>";
                        $x .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_UserClassName</td>";
                        $x .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_UserClassNumber</td>";
                        $x .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_Payment_Field_PaymentItem</td>";
                        $x .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_Payment_PrintPage_Outstanding_DueDate</td>";
                        $x .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_Payment_PrintPage_Outstanding_Total</td>";
                	if(!$isKIS){
                        $x .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_Payment_PrintPage_Outstanding_AccountBalance</td>";
                        $x .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_Payment_PrintPage_Outstanding_LeftAmount</td>";
                        $x .= "<td class=\"eSporttdborder eSportprinttabletitle\">$i_Payment_Field_PPSAccountNo</td>";
                	}
                	if($plugin['eClassApp']){
                		$x .= "<td class=\"eSporttdborder eSportprinttabletitle print_hide\" align=\"right\">".$Lang['AppNotifyMessage']['PushMessage']."<input type='checkbox' onClick=\"(this.checked)?setChecked(1,this.form,'StudentID[]'):setChecked(0,this.form,'StudentID[]')\"></input></td>";
                		
                	}
                $x .= "</tr>\n";

                $csv.="\"$classname\"\r\n";
                $csv.="\"$i_UserStudentName\"\t\"$i_UserClassName\"\t\"$i_UserClassNumber\"\t\"$i_Payment_Field_PaymentItem\"\t\"$i_Payment_PrintPage_Outstanding_DueDate\"\t\"$i_Payment_PrintPage_Outstanding_Total\"";
                if(!$isKIS){
                	$csv.="\t\"$i_Payment_PrintPage_Outstanding_AccountBalance\"\t\"$i_Payment_PrintPage_Outstanding_LeftAmount\"\t\"$i_Payment_Field_PPSAccountNo\"";
                }
                $csv.="\r\n";

        }
        $colspanNum = $plugin['eClassApp'] == true? '10':'9';
        if ($TotalRecord[$classname] == 0 && $ExcludeEmpty == 0)
        {
        
                $x .= "<tr><td colspan= $colspanNum><hr></td></tr>\n";
                $x .= "<tr><td colspan=$colspanNum style=\"text-align:center\">$i_no_record_exists_msg</td></tr>\n";

                $csv.="\"$i_no_record_exists_msg\"\r\n";
        }
        else
        {
                $payment_record = $Payment[$classname];
                $curr_student = "";
                for($j=0; $j<sizeof($payment_record); $j++)
                {
                        list($StudentName, $ClassName, $ClassNumber, $ItemName, $Amount, $StudentID, $s_pps_no,$due_date) = $payment_record[$j];
						
                        if ($j != 0)
                        	$x .= ($curr_student == $StudentID) ? "" : "<tr><td colspan=$colspanNum><hr></td></tr>\n";
                        #else
                        	#$x .="<tr><td colspan=\"7\">&nbsp;</td></tr>\n";
                        $x .= "<tr>\n";
                        $x .= "<td>";
                        
                        if($curr_student == $StudentID) {
                                $x.="&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
                                $csv.="\"\"\t\"\"\t\"\"\t";
                        }
                        else{
                                 $x.=$StudentName."</td><td>".$ClassName."</td><td>".$ClassNumber."</td>";
                                 $csv.="\"$StudentName\"\t\"$ClassName\"\t\"$ClassNumber\"\t";
                        }
                               
                        $x .= "<td>".$ItemName."(".$lpayment->getWebDisplayAmountFormat($Amount).")</td>";
                        $x .= "<td nowrap>".$due_date."</td>";
                        
                        $csv.="\"".$ItemName."(".$lpayment->getExportAmountFormat($Amount).")\"\t";
                        $csv.="\"".$due_date."\"\t";
                        
                        if ($curr_student == $StudentID)
                        {
                            $x .= "<td>&nbsp;</td>";
                            if(!$isKIS){
                            	$x .= "<td colspan=\"3\">&nbsp;</td>";
                            }
                            if($plugin['eClassApp']){
                            	$x .= "<td>&nbsp;</td>";
                            }
                            $csv.="\"\"\t";
                        }
                        else
                        {
                            $t_balance = $student_balance[$StudentID] + 0;
                            $t_diff = $TotalAmount[$StudentID] - $t_balance;
                            if (abs($t_diff) < 0.001)    # Smaller than 0.1 cent
                            {
                                $t_diff = 0;
                            }
                            $str_diff = ($t_diff > 0)? "<font color=red>-".$lpayment->getWebDisplayAmountFormat($t_diff,2)."</font>":"--";
                                $csv_str_diff = ($t_diff > 0)? "-".$lpayment->getExportAmountFormat($t_diff):"--";

                            $x .= "<td>".$lpayment->getWebDisplayAmountFormat($TotalAmount[$StudentID])."</td>";
                          if(!$isKIS){  
                            $x .= "<td>".$lpayment->getWebDisplayAmountFormat($t_balance)."</td>";
                            $x .= "<td>$str_diff</td>";
                            $x .= "<td>$s_pps_no</td>";
                          }
                        
                          if($plugin['eClassApp'] == true){
                          	
                          	include_once($PATH_WRT_ROOT."includes/libuser.php");
                          	$luser = new libuser();
                          	$studentWithParentUsingAppAry = $luser->getStudentWithParentUsingParentApp();
                          	if(in_array($StudentID, $studentWithParentUsingAppAry)){
                          	//	$InadequateAccountBalanceAry = array();
	                          	//if($t_diff > 0){
	                          	//	$InadequateAccountBalanceAry[$StudentID] = $t_diff;
	                          		$x .= '<td class="num_check print_hide" align="right" valign="top"><input type="checkbox" name="StudentID[]" value="'.$StudentID.'" class="students-to-send" />
											<input type="hidden" name="OutstandingAmount[]" value="'.$TotalAmount[$StudentID].'" />
											<input type="hidden" name="InadequateBalance[]" value="'.max($t_diff,0).'" />
											<input type="hidden" name="AccountBalance[]" value="'.$t_balance.'" />';
									$x .= '<input type="hidden" name="StudentItems[]" value="'.base64_encode(implode(', ',$studentIdToItems[$StudentID])).'" />';
									$x .= '</td>';
	                          	//}else{
	                          	//	$x .= '<td> </td>';
	                          	//}
                          	}
                          }else{
                            	$x .= '<td>&nbsp;</td>';
                          }
                            
                            $csv .= "\"".$lpayment->getExportAmountFormat($TotalAmount[$StudentID])."\"";
                           if(!$isKIS){
                             $csv .= "\t\"".$lpayment->getExportAmountFormat($t_balance)."\"";
                             $csv .= "\t\"$csv_str_diff\"";
                             $csv .= "\t\"$s_pps_no\"";
                           }
                        }

                        $x .= "</tr>\n";
                        $csv.="\r\n";
                        $curr_student = $StudentID;
                }
        }

        if ($TotalRecord[$classname] > 0 || $ExcludeEmpty == 0)
        {
                $x .= "</table>\n";
                $x .= "</form>\n";
                $x .= $page_breaker;
                $csv.="\r\n\r\n";
        }
                if($format!=1)
                $display .= $x;
        else $display .=$csv;
}
?>
<?php if($format!=1){?>
<table width="95%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right">
	<?php if ($plugin['eClassApp']){?>
		<?=$linterface->GET_BTN($Lang['ePayment']['SelectAllStudents'], "button", "Check_All_Options_By_Class('students-to-send',true);","SelectAllSendPushMessageBtn"," title=\"".$Lang['AppNotifyMessage']['PushMessage']."\" ","0","")?>
		<?=$linterface->GET_BTN($Lang['ePayment']['UnselectAllStudents'], "button", "Check_All_Options_By_Class('students-to-send',false);","DeselectAllSendPushMessageBtn"," title=\"".$Lang['AppNotifyMessage']['PushMessage']."\" ","0","")?>
		<?=$linterface->GET_BTN($Lang['AppNotifyMessage']['button'], "button", "Send_Push_Message_To_Parent();","Send Push Message","","0","thickbox")?>
	<?php }?>
		<?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?>
		</td>
	</tr>
</table>
<style type="text/css">
	body td, body th {font-size: 12px;} 
</style>
<?=$linterface->Include_Thickbox_JS_CSS()?>
<script>
function Send_Push_Message_To_Parent(){
	
	var StudentID = "";
	var InadequateBalance="";
	var checkbox = document.getElementsByName("StudentID[]");
	var InadequateBalanceAmount =  document.getElementsByName("InadequateBalance[]");
	for(var i =0; i<checkbox.length;i++){
		if( checkbox[i].checked){
			StudentID += "," + checkbox[i].value;
			InadequateBalance += ","+ InadequateBalanceAmount[i].value; 
		}
	}

	if(StudentID == ''){
		 alert(globalAlertMsg2);
		 return;
	}else{
		tb_show('<?=$Lang['AppNotifyMessage']['button']?>',"#TB_inline?height=480&width=640&inlineId=FakeLayer");
		$.post(
			'ajax_send_push_message_to_parent.php',
			{ 
				//StudentID: StudentID,
				//InadequateBalance: InadequateBalance
			},
			function(returnHtml) {
				$('div#TB_ajaxContent').html(returnHtml);
			}
		);
		
	}	
}
</script>
<?php } ?>
<?
if($format!=1)
        echo $title.$display;
else {
	$filename = "outstanding".date('Y-m-d').".csv";
	//$display = str_replace("\"","",$display);
	//$display = str_replace(",","\t",$display);
	//$display = str_replace("\n","\r\n",$display);
	$lexport->EXPORT_FILE($filename, $display);
}

?>
<? if($format!=1){?>
<style type="text/css">
	p.breakhere {page-break-before: always;}
</style>
<?}?>
<?php
intranet_closedb();
if($format!=1)
	include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
	#include_once($PATH_WRT_ROOT."templates/filefooter.php");
?>