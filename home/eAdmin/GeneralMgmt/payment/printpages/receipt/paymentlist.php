<?php
# using: 

######################################
#	Date:	2019-12-27  Philips  [2019-1204-1612-18235]
#			Add Issus Date Option
#
#   Date:   2019-09-25  Ray
#           $sys_custom['ttmss_payment_receipt']  for TTMSS
#
#	Date:	2017-08-25	Carlos
#			Added Group by Student or Payment Item option for selecting individual payment items.
#
#	Date:	2014-09-26	YatWoon
#			Improved: update opener.window receiption starting number [Case#X68731]
#			Deploy: IPv10.1
#
#	2014-09-11	Carlos
#		$sys_custom['ePayment']['ThermalPaperReceipt'] - for KIS tbcpk
#
#	2014-09-03	YatWoon
#		Reorder the form field, display date selection first
#
#	2014-08-19	Carlos
#		Added payment item selection
#
#	2013-12-12	YatWoon
#			Improved: add "Receipt Title" field allow client define the receipt title [Case#R56698]
#
#	2011-09-15	YatWoon
#		- add "select all" button
#		- apply "need receipt" checking 
#
######################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PrintPage_Receipt";

$linterface = new interface_html();

$lclass = new libclass();
$button_select = $i_status_all;
if($sys_custom['ttmss_payment_receipt']) {
	$select_class = $lclass->getSelectClass("name=ClassName onChange=this.form.action='';this.form.target='';this.form.submit()", $ClassName, "", '-- ' . $i_general_please_select . ' --');
} else {
    $select_class = $lclass->getSelectClass("name=ClassName onChange=this.form.action='';this.form.target='';this.form.submit()", $ClassName);
}
// get default start receipt number
$DefaultNumber = $lpayment->Get_Next_Receipt_Number(); 

# Default Issue Date as today
$date_issue = date('Y-m-d');

$TAGS_OBJ[] = array($i_Payment_Menu_PrintPage_Receipt_PaymentItemList, $PATH_WRT_ROOT."home/admin/payment/printpages/receipt/paymentlist.php", 1);

$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if ($msg == "2") $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<br />
<script language='javascript'>
function checkform(obj){

	<?php if($sys_custom['ttmss_payment_receipt']){ ?>
    var studentSelectionObj = document.getElementById('StudentID');
    var student_count = 0;
    if(!studentSelectionObj) {
        alert('<?=$Lang['General']['JS_warning']['SelectClass']?>');
        return;
    }

    for (var i = 0; i < studentSelectionObj.options.length; i++) {
        if (studentSelectionObj.options[i].selected) {
            student_count++;
        }
    }

    if(student_count == 0) {
        alert('<?=$Lang['General']['JS_warning']['SelectStudent']?>');
        return;
    }
    <?php } ?>

	if (document.getElementById('DefaultStartingReceiptNumber')) {
		var DefaultStartingNumber = document.getElementById('DefaultStartingReceiptNumber').value;
		var StartingReceiptNumber = document.getElementById('StartingReceiptNumber').value;
	}
	else {
		var DefaultStartingNumber = 0;
		var StartingReceiptNumber = 0;
	}
	
	if (!isInteger(StartingReceiptNumber)) { // check user entered receipt number
		alert("<?=$Lang['Payment']['StartingNextReceiptNumberNumberWarning']?>");
	}
	else if (DefaultStartingNumber > StartingReceiptNumber) { // user inputed value must be greater or equal to default receipt number
		alert("<?=$Lang['Payment']['StartingNextReceiptNumberWarning'].$DefaultNumber?>");
	}
	else {
		if (document.getElementById('select_format')) {
			var format = document.getElementById('select_format').options[document.getElementById('select_format').selectedIndex].value;
			if(format == 2)
			{
				obj.action = "paymentlist_print_lskps.php";
			}else
			{
				obj.action = "paymentlist_print.php";
			}
		}
		else {
			obj.action = "paymentlist_print.php";
		}

		<?php if($sys_custom['ttmss_payment_receipt']){ ?>
        obj.action = "paymentlist_print_ttmss.php";
		<?php } ?>

<?php if($sys_custom['ePayment']['ThermalPaperReceipt']){ ?>
		var is_thermal_paper_receipt = $('#ThermalPaperReceipt').is(':checked');
		if(is_thermal_paper_receipt){
			obj.action = "paymentlist_print_thermal.php";
		}
<?php } ?>

		obj.target = '_blank';
		obj.submit();
	}
}

function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}

function reloadPaymentItems()
{
	if($('#IndividualItems').is(':checked')){
		var dateFrom = $('#date_from').val();
		var dateTo = $('#date_to').val();
		var studentSelectionObj = document.getElementById('StudentID');
		var studentIdAry = [];
		for(var i=0;i<studentSelectionObj.options.length;i++){
			if(studentSelectionObj.options[i].selected){
				studentIdAry.push(studentSelectionObj.options[i].value);
			}
		}
		
		$.post(
			'ajax_reload.php',
			{
				'task': 'getStudentPaymentItems',
				'ID':'PaymentID[]',
				'StudentID[]':studentIdAry,
				'date_from':dateFrom,
				'date_to':dateTo,
				'GroupBy':$('input[name=GroupBy]:checked').val()
			},
			function(data){
				$('#PaymentItemsSpan').html(data);
				SelectAll(document.getElementById('PaymentID[]'));
			}
		);
	}else{
		$('#PaymentItemsSpan').html('');
	}
}

function togglePaymentItemSelection(isChecked)
{
	if(isChecked){
		$('#PaymentItemsRow').show();
		$('#ContainerGroupBy').show();
	}else{
		$('#PaymentItemsRow').hide();
		$('#ContainerGroupBy').hide();
	}
	reloadPaymentItems();
}

<?php if($IndividualItems == '1'){ ?> 
$(document).ready(function(){
	reloadPaymentItems();
});
<?php } ?>
//this.form.target='_blank';this.form.action='paymentlist_print.php'
</script>
<form name="form1" method="post" action="" >
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
    	<td>
    		<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr><td colspan="2" align="right"><?=$SysMsg?></td></tr>
    			<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_UserClassName?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<?=$select_class?>
	    			</td>
    			</tr>
    			
    			<tr>
  				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
    				<?=$i_Profile_From?>
    			</td>
    			<td class="tabletext" width="70%">
    				<?=$linterface->GET_DATE_PICKER('date_from',$date_from,' onchange="reloadPaymentItems();" ', "yy-mm-dd","","","","reloadPaymentItems();")?>
    				<span class="tabletextremark">(yyyy-mm-dd)</span>
    			</td>
  			</tr>
  			<tr>
  				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
    				<?=$i_Profile_To?>
    			</td>
    			<td class="tabletext" width="70%">
    				<?=$linterface->GET_DATE_PICKER('date_to',$date_to,' onchange="reloadPaymentItems();" ', "yy-mm-dd","","","","reloadPaymentItems();")?>
    				<span class="tabletextremark">(yyyy-mm-dd)</span>
    			</td>
  			</tr>
  			
  			
<?
	if ($ClassName != "") {
		//$select_student = $lclass->getStudentSelectByClass($ClassName,'name="StudentID[]" id="StudentID" multiple size="10" style="min-width:150px; width:200px;"');
		$select_student = $lpayment->getReceiptStudentSelectByClass($ClassName,'name="StudentID[]" id="StudentID" multiple size="10" style="min-width:150px; width:200px;" onchange="reloadPaymentItems();" ');
?>
				
				<tr>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	    				<?=$i_UserStudentName?>
	    			</td>
	    			<td class="tabletext" width="70%">
	    				<?=$select_student?>
	    				<?=$linterface->GET_BTN($button_select_all, "button", "javascript:SelectAll(document.getElementById('StudentID'));reloadPaymentItems();"); ?>
	    				<?='<div>'.$linterface->Get_Checkbox("IndividualItems", "IndividualItems", "1", $IndividualItems=='1', '', $Lang['ePayment']['SelectPaymentItems'], 'togglePaymentItemSelection(this.checked);', '').'</div>';?>
	    				<?='<div id="ContainerGroupBy" '.($IndividualItems=='1'?'':'style="display:none;"').'>'.$Lang['ePayment']['GroupBy'].': '.$linterface->Get_Radio_Button("GroupByStudent", "GroupBy", "1", $GroupBy=='1' || $GroupBy=='', "", $Lang['Identity']['Student'], "reloadPaymentItems();", 0).'&nbsp;'.$linterface->Get_Radio_Button("GroupByItem", "GroupBy", "2", $GroupBy=='2', "", $i_Payment_Field_PaymentItem, "reloadPaymentItems();", 0).'</div>'?>
	    			</td>
    			</tr>
    			<tr id="PaymentItemsRow" <?=$IndividualItems==1?'':'style="display:none"'?>>
    				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
    					<?=$Lang['ePayment']['SelectPaymentItems']?>
    				</td>
    				<td class="tabletext" width="70%">
    					<span id="PaymentItemsSpan"></span>
    					<?= $linterface->GET_BTN($button_select_all, "button", "javascript:SelectAll(document.getElementById('PaymentID[]'))"); ?>
    				</td>
    			</tr>
<?
	}
?>


		    <?php if(!$sys_custom['ttmss_payment_receipt']){ ?>
  			<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['ePayment']['ReceiptTitle']?></td>
					<td class="tabletext" width="70%">
						<input type="text" name="ReceiptTitle" id="ReceiptTitle" value="<?=$i_Payment_Receipt_Official_Receipt?>">
					</td>
				</tr>
				
  			<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?=$i_StaffAttendance_Field_Signature?>:
					</td>
					<td class="tabletext" width="70%">
						<input <?=((!$special_option['print_recipient_footer_no_signature'])? 'checked':'')?> type=checkbox name="Signature" id="Signature" value="1" onclick="if (this.checked) document.getElementById('SignatureTextRow').style.display = ''; else document.getElementById('SignatureTextRow').style.display = 'none';">
					</td>
				</tr>
				<tr id="SignatureTextRow" style="<?=((!$special_option['print_recipient_footer_no_signature'])? '':'display:none;')?>">
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?=$Lang['Payment']['SignatureText']?>:
					</td>
					<td class="tabletext" width="70%">
						<select name="SignatureText" id="SignatureText">
							<option <?=((!$special_option['print_recipient_footer_show_PIC'])? 'selected':'')?> value="<?=$i_Payment_Receipt_Person_In_Charge?>"><?=$i_Payment_Receipt_Person_In_Charge?></option>
							<option <?=((!$sys_custom['LSKPS_Payment_Receipt_No_Principle'])? 'selected':'')?> value="<?=$i_Payment_Receipt_Principal?>"><?=$i_Payment_Receipt_Principal?></option>
						</select>
					</td>
				</tr>
				<? if ($sys_custom['lskps_payment_receipt']){?>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?=$Lang['ePayment']['SelectFormat']?>:
					</td>
					<td class="tabletext" width="70%">
						<select id='select_format'>
			  			<option selected="selected" value=1><?=$Lang['ePayment']['Format_1']?></option>
			  			<option value=2><?=$Lang['ePayment']['Format_2']?></option>
					  </select>
					</td>
				</tr>
				<? } ?>

				<? if ($sys_custom['PaymentReceiptNumber']){?>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?=$Lang['Payment']['StartingNextReceiptNumber']?>:
					</td>
					<td class="tabletext" width="70%">
						<input type="hidden" name="DefaultStartingReceiptNumber" id="DefaultStartingReceiptNumber" value="<?=$DefaultNumber?>">
						<span id="DefaultStartingReceiptNumber_Display"><?=$DefaultNumber?></span><br>
						<input type="text" name="StartingReceiptNumber" id="StartingReceiptNumber" value="<?=$DefaultNumber?>">
					</td>
				</tr>
				<? } ?>
				<?php if($sys_custom['ePayment']['ThermalPaperReceipt']){ ?>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?=$Lang['ePayment']['ThermalPaperReceipt']?>:
					</td>
					<td class="tabletext" width="70%">
						<?=$linterface->Get_Checkbox("ThermalPaperReceipt", "ThermalPaperReceipt", "1", $ThermalPaperReceipt=='1', '', '', '', '')?>
					</td>
				</tr>
				<?php } ?>
			<?php } ?>

				<?php if($sys_custom['ttmss_payment_receipt']){ ?>
                    <tr>
                        <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
							<?=$Lang['General']['Remark']?>:
                        </td>
                        <td class="tabletext" width="70%">
                            <input type="hidden" name="ReceiptType" id="ReceiptType" value="ePayment">
                            <input type="text" name="ReceiptRemark" id="ReceiptRemark" value="">
                        </td>
                    </tr>
				<?php } ?>
				
				<?php if(!$sys_custom['ttmss_payment_receipt']){ // 2019-12-27 (Philips) - Add Issus Date Option?>
					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
							<?=$Lang['ePayment']['IssueDate']?>:
                        </td>
                        <td class="tabletext" width="70%">
                        	<?=$linterface->GET_DATE_PICKER('date_issue',$date_issue,'', "yy-mm-dd","","","","")?>
    						<span class="tabletextremark">(yyyy-mm-dd)</span>
                        </td>
                    </tr>
				<?php }?>

    		</table>
    	</td>
	</tr>
	<tr>
    	<td>
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
	<tr>
	    <td align="center">
			<?= $linterface->GET_ACTION_BTN($button_submit, "button", "javascript:checkform(this.form)") ?>
		</td>
	</tr>
</table>
</form>

<?
$linterface->LAYOUT_STOP();

if ($ClassName != "")
	print $linterface->FOCUS_ON_LOAD("form1.StudentID");
else
	print $linterface->FOCUS_ON_LOAD("form1.ClassName");

intranet_closedb();
?>
