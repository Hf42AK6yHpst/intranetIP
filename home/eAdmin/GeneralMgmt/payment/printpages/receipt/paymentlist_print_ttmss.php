<?php
ini_set('memory_limit','128M');
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ttmss_payment_receipt']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$intranet_session_language = 'b5';

$linterface = new interface_html();
$lpayment = new libpayment();


require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");

$lcardattend = new libcardstudentattend2();
$use_magic_quotes = $lcardattend->is_magic_quotes_active;

$margin= 5; // mm
$margin_top = $margin;
$margin_bottom = $margin;
$margin_left = $margin;
$margin_right = $margin;
$margin_top = 65;

$pdf = new mPDF('','A5-L',0,'',$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer);
$pdf->shrink_tables_to_fit = 1;

$pdf->backupSubsFont = array('mingliu');
$pdf->useSubstitutions = true;

$ttmss_payment_receipt_data = $sys_custom['ttmss_payment_receipt_data'][$sys_custom['ttmss_payment_receipt_code']];


$logo = '/images/kis/'.$ttmss_payment_receipt_data['logo'];
$address_1 = $ttmss_payment_receipt_data['address_1'];
$address_2 = $ttmss_payment_receipt_data['address_2'];
$receipt_tel = $ttmss_payment_receipt_data['tel'];
$receipt_fax = $ttmss_payment_receipt_data['fax'];
$receipt_email = $ttmss_payment_receipt_data['email'];
$receipt_website = $ttmss_payment_receipt_data['website'];
$receipt_today = date("Y-m-d");
$receipt_staff_name = $_SESSION['EnglishName'];
if($use_magic_quotes) {
	$receipt_remark = stripslashes($ReceiptRemark);
} else {
	$receipt_remark = $ReceiptRemark;
}


$lclass = new libclass();

if ($StudentID == "")
{
	if ($ClassName == "")
	{
		echo "請選擇學生或班別";
		intranet_closedb();
		exit();
		//$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 ORDER BY ClassName, ClassNumber, EnglishName";
		//$target_students = $lclass->returnVector($sql);
	}
	else
	{
		# All students in $ClassName
		$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND ClassName = '$ClassName' ORDER BY ClassName, ClassNumber, EnglishName";
		$target_students = $lclass->returnVector($sql);
	}
}
else
{
	if (is_array($StudentID)) {
		$target_students = $StudentID;
	}
	else {
		$target_students = array($StudentID);
	}
}

$date_field = "a.PaidTime";
$conds = "";
if ($date_from != "")
{
	$conds .= " AND $date_field >= '$date_from'";
}
if ($date_to != "")
{
	$conds .= " AND $date_field <= '$date_to 23:59:59'";
}

if($IndividualItems == 1){
	$conds .= " AND a.PaymentID IN ('".implode("','",(array)$PaymentID)."') ";
}

# Get Latest Balance
$lpayment = new libpayment();
$list = "'".implode("','", $target_students)."'";
$sql = "SELECT StudentID, Balance FROM PAYMENT_ACCOUNT WHERE StudentID IN ($list)";
$temp = $lpayment->returnArray($sql,2);
$balance_array = build_assoc_array($temp);


$record_count = 0;

for ($j=0; $j<sizeof($target_students); $j++) {
	$record_data = array();

	$target_id = $target_students[$j];
	$lu = new libuser($target_id);
	$studentname = $lu->UserNameLang()." (".$lu->ClassName.")";
	$i_title = $studentname;

	$receipt_student_name = $studentname;
	$receipt_student_number = $ttmss_payment_receipt_data['code'] . "-" . $lu->UserLogin;


	$curr_balance = $balance_array[$target_id];

	$sql = "SELECT 
 					a.PaymentID
 			FROM 
 			PAYMENT_OVERALL_TRANSACTION_LOG d
 			INNER JOIN PAYMENT_PAYMENT_ITEMSTUDENT as a ON (d.TransactionType=6 and d.RelatedTransactionID=a.PaymentID and d.StudentID=$target_id)
 			WHERE 
	        	d.StudentID = '$target_id'
	        	AND d.TransactionType=6 
 		";

	$cancel_payment_ids = $lu->returnVector($sql);

	if(count($cancel_payment_ids) > 0) {
		$cancel_payment_ids = array_unique($cancel_payment_ids);
		$conds .= " AND a.PaymentID NOT IN(".implode(',', $cancel_payment_ids).")";
	}

	$sql = "SELECT 
						a.PaymentID,
						b.ItemCode,
						c.Name as cName, 
						b.Name,
						b.Description, 
						a.PaidTime,
						a.Amount, 
						d.RefCode,
						d.RelatedTransactionID, 
						c.Description ";
	$sql .= " FROM 
				PAYMENT_PAYMENT_ITEMSTUDENT as a
			  inner join PAYMENT_OVERALL_TRANSACTION_LOG as d on (d.TransactionType=2 and d.RelatedTransactionID=a.PaymentID and d.StudentID=$target_id)
	          LEFT OUTER JOIN PAYMENT_PAYMENT_ITEM as b ON a.ItemID = b.ItemID
	          LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY as c ON b.CatID = c.CatID 
	        WHERE 
	        	a.StudentID = '$target_id'
	        	AND a.RecordStatus = 1 AND d.TransactionType=2 
	        	$conds 
	        ORDER BY b.ItemCode ASC
                ";

	$student_itemlist = $lu->returnArray($sql);

	if(count($student_itemlist) == 0) {
		continue;
	}


	$sql = "INSERT INTO PAYMENT_PAYMENT_RECEIPT_NUMBER
               (ReceiptType, FromDate, ToDate, StudendIDs, DateInput)
        VALUES ('ePayment','".$lu->Get_Safe_Sql_Query($date_from)."','".$lu->Get_Safe_Sql_Query($date_to)." 23:59:59','".implode(',', $target_students)."', now())";

	$lu->db_db_query($sql);

	if($lu->db_affected_rows() <= 0) {
		continue;
	}

	$receipt_id = $lu->db_insert_id();

	# start student
	$total_amount = 0;

	$sql_vals = array();

	foreach ($student_itemlist as $recordAry) {
		list($payment_id, $item_code, $cat_name, $item_name, $item_desp, $item_paidtime, $item_amount, $ref_code) = $recordAry;
		$total_amount += $item_amount;
		$item_amount = number_format($item_amount, 2);

		$temp = array();
		$temp[] = $item_code;
		$temp[] = $item_name;
		$temp[] = '1';
		$temp[] = $item_amount;
		$temp[] = $item_amount;
		$record_data[] = $temp;

		$sql_val[] = "('".$lu->Get_Safe_Sql_Query($receipt_id)."','".$lu->Get_Safe_Sql_Query($payment_id)."')";
	}

	$sql = "INSERT INTO PAYMENT_PAYMENT_RECEIPT_NUMBER_ITEM
               (ReceiptID, PaymentLogID)
        VALUES ".implode(',', $sql_val);

	$lu->db_db_query($sql);

	$receipt_code = $ttmss_payment_receipt_data['code'] . "-" . str_pad($receipt_id, 6, '0', STR_PAD_LEFT);

	$record_count++;

	$pdf->DefHTMLHeaderByName(
		'header',
		'<div style="text-align: center; margin-top: 10;">
		<table style="width: 100%">
			<tr>
					<td width="150"></td>
					<td style="text-align: center;"><img src="' . $logo . '"  style="width: 400; height: 80; margin: 0;"  /></td>
					<td width="150" valign="top"></td>
			</tr>
		</table>
		<div style="text-align: center; font-size: 11pt">' . $address_1 . '</div>
		<div style="text-align: center; font-size: 11pt">' . $address_2 . '</div>
		<div style="text-align: center; font-size: 8pt">Tel: ' . $receipt_tel . ' Fax: ' . $receipt_fax . ' Email: ' . $receipt_email . ' Website: ' . $receipt_website . '</div>
		<table style="width: 100%">
			<tr>
					<td width="150"></td>
					<td style="text-align: center; font-size: 12pt; font-weight: bold; text-decoration: underline;">收 據</td>
					<td width="150" valign="top">' . $receipt_code . '</td>
			</tr>
		</table>
		<table style="width: 100%">
			<tr>
					<td width="355" style="text-align: left; font-size: 12pt;">學生姓名 : ' . $receipt_student_name . '</td>
					<td style="text-align: left; font-size: 12pt;">學生編號 : ' . $receipt_student_number . '</td>
					<td width="150" valign="top" style="font-weight: bold;">' . $receipt_today . '</td>
			</tr>
		</table>
		<table style="width: 100%; font-weight: bold;">
			<tr>
					<td width="100" style="text-align: left; font-size: 9pt;">編號</td>
					<td style="text-align: left; font-size: 10pt;">名稱</td>
					<td width="100" style="text-align: right; font-size: 9pt;">數量</td>
					<td width="100" style="text-align: right; font-size: 9pt;">單價</td>
					<td width="100" style="text-align: right; font-size: 9pt;">金額</td>
			</tr>
		</table>
		<table style="width: 100%"><tr><td style ="border-bottom:2px solid black; width:100%;"> </td></tr></table>
	</div>
');

	$pdf->SetHTMLHeaderByName('header');
	$pdf->AddPage();

	$remain_item = count($record_data);
	foreach ($record_data as $temp) {
		$x = '<table style="width: 100%; font-weight: bold; overflow: wrap;" autosize="1">
			<tr>
					<td width="100" style="text-align: left; font-size: 8pt;" valign="top">' . $temp[0] . '</td>
					<td style="text-align: left; font-size: 10pt;" valign="top">' . $temp[1] . '</td>
					<td width="100" style="text-align: right; font-size: 8pt;" valign="top">' . $temp[2] . '</td>
					<td width="100" style="text-align: right; font-size: 8pt;" valign="top">' . $temp[3] . '</td>
					<td width="100" style="text-align: right; font-size: 8pt;" valign="top">' . $temp[4] . '</td>
			</tr>';
		$x .= '</table>';
		$pdf->WriteHTML($x);

		$remain_item--;

		if($pdf->y > 115) {
			if($remain_item > 0) {
				$pdf->AddPage();
			}
		}
	}


	$total_amount = number_format($total_amount, 2);

	$footer = '<div style="position: absolute; left: 0; bottom: 0; margin-left: 20; margin-right: 20;">
				<table style="width: 100%"><tr><td style ="border-bottom:2px solid black; width:100%;"> </td></tr></table>';
	$footer .= '<table style="width: 100%; font-weight: bold; margin-bottom: 50">
			<tr>
					<td style="text-align: left; font-size: 10pt;">備註: ' . $receipt_remark . '</td>
					<td width="100" style="text-align: right; font-size: 10pt;">總金額:</td>
					<td width="100" style="text-align: right; font-size: 10pt;">' . $total_amount . '</td>
			</tr>
		</table>';


	$footer .= '<table style="width: 100%; margin-bottom: 20;">
			<tr>
					<td style="text-align: right; font-size: 9pt;">職員: ' . $receipt_staff_name . '</td>
					<td width="250" style="text-align: right; font-size: 9pt;">職員簽名: ___________________</td>
			</tr>
		</table>
		</div>
		';

	$pdf->WriteHTML($footer);
}

if($record_count > 0) {
	$pdf->Output('ePayment_'.$receipt_today.'.pdf', 'I');
} else {
	echo "沒有資料";
}
intranet_closedb();