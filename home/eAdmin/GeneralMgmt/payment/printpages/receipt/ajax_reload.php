<?php
// Editing by 
/*
 * 2017-08-25 (Carlos): Added parameter $GroupBy for task "getStudentPaymentItems".
 * 2014-08-19 (Carlos): Created for paymentlist.php
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libclass.php");
//include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$linterface = new interface_html();

switch($task)
{
	case "getStudentPaymentItems":
		$name_field = getNameFieldWithClassNumberByLang("c.");
		$sql = "SELECT 
					a.PaymentID, ";
		if($GroupBy == '2'){
			$sql .=	" $name_field as StudentName,
					  b.Name ";
		}else{
			$sql .=	" b.Name, 
		          	  $name_field as StudentName ";
		}
		$sql .= "FROM 
		            PAYMENT_PAYMENT_ITEMSTUDENT AS a
		            INNER JOIN PAYMENT_PAYMENT_ITEM AS b ON a.ItemID = b.ItemID
		            INNER JOIN INTRANET_USER AS c ON a.StudentID = c.UserID 
		        WHERE a.RecordStatus='1' AND a.StudentID IN ('".implode("','",(array)$StudentID)."') AND a.PaidTime IS NOT NULL 
					AND (a.PaidTime >= '$date_from 00:00:00' AND a.PaidTime <= '$date_to 23:59:59') 
				ORDER BY ";
		if($GroupBy == '2'){
			$sql .= " b.DisplayOrder,c.ClassName,c.ClassNumber+0 ";
		}else{
			$sql .= " c.ClassName,c.ClassNumber+0,b.DisplayOrder ";
		}
		$records = $lpayment->returnArray($sql);
		
		//$selection = getSelectByArray($records, ' id="'.$ID.'" name="'.$ID.'" multiple="multiple" size="10" style="min-width:15em;" ', "", 0, 1, "");
		$selection = $linterface->GET_SELECTION_BOX_WITH_OPTGROUP($records, ' id="'.$ID.'" name="'.$ID.'" multiple="multiple" size="10" style="min-width:15em;" ', "", "");
		echo $selection;
	break;	
}

intranet_closedb();
?>