<?php
// Editing by 
/*
 * 2016-06-27 (Carlos): Added staff user selection.
 * 2015-11-26 (Carlos): Modifified to allow select left students. 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$MODULE_OBJ['title'] = $iDiscipline['select_students'];
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$lclass = new libclass();
$lui =  new interface_html();

$user_type = isset($user_type) && in_array($user_type, array(USERTYPE_STUDENT,USERTYPE_STAFF))? $user_type : USERTYPE_STUDENT;

$usertypeAry = array(
					array(USERTYPE_STUDENT, $Lang['Identity']['Student']),
					array(USERTYPE_STAFF, $Lang['Identity']['Staff'])
				);
$usertype_selection = getSelectByArray($usertypeAry, ' id="user_type" name="user_type" onchange="document.form1.submit();" ',$user_type, $__all=0, $__noFirst=1, $__FirstTitle="", $__ParQuoteValue=1);

if($user_type == USERTYPE_STUDENT)
{
	$select_class = $lclass->getSelectClass("name=targetClass onChange=\"this.form.action='';this.form.submit()\"",$targetClass);

	if ($targetClass != "")
    	$select_students = $lclass->getStudentSelectByClass($targetClass,"size=25 multiple name=targetID[]",$__selected="", $__all=0, $__type="", $__typeID=-1, $__recordStatus="1,3");
}

if($user_type == USERTYPE_STAFF)
{
	$staff_name_field = getNameFieldByLang2("u.");
	$sql = "SELECT u.UserID,CONCAT(".$staff_name_field.",IF(u.RecordStatus<>1,' [".$Lang['Status']['Suspended']."]','')) as UserName FROM INTRANET_USER as u WHERE u.RecordType='".USERTYPE_STAFF."' ORDER BY UserName";
	$staffAry = $lclass->returnArray($sql);
	$staff_selection = getSelectByArray($staffAry, ' id="targetID[]" name="targetID[]" multiple="multiple" size="25" ', "", $__all=0, $__noFirst=1, $__FirstTitle="", $__ParQuoteValue=1);
}

?>

<script language="javascript">
function AddOptions(obj){
     par = opener.window;
     parObj = opener.window.document.form1.elements["<?=$fieldname?>"];
		
     checkOption(obj);
     par.checkOption(parObj);
     i = obj.selectedIndex;

     while(i!=-1)
     {
          addtext = obj.options[i].text;
          par.checkOptionAdd(parObj, addtext, obj.options[i].value);
          obj.options[i] = null;
          i = obj.selectedIndex;
     }

     par.checkOptionAdd(parObj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
     par.submitForm();
}

function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}
</script>

<form name=form1 action="" method=post>
<br>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td></td>
	</tr>
	<tr>
		<td>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td valign="top" nowrap="nowrap" class='tabletext'>
			<?=$Lang['General']['UserType']?>:
		</td>
		<td width="80%"><?=$usertype_selection?></td>
	</tr>
	<tr>
		<td class="dotline" colspan="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
	</tr>
<?php if($user_type == USERTYPE_STUDENT){ ?>	
	<tr>
		<td valign="top" nowrap="nowrap" class='tabletext'>
			<?=$iDiscipline['class']?>:
		</td>
		<td width="80%"><?=$select_class?></td>
	</tr>
<?php if($targetClass != "") { ?>
				<tr>
					<td class="dotline" colspan="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class='tabletext'>
						<span class="tabletext"><?= $iDiscipline['students']?>:</span>
					</td>
					<td width="80%">
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td><?= $select_students ?></td>
								<td valign="bottom">
									<table width="100%" border="0" cellspacing="0" cellpadding="6">
										<tr> 
											<td align="left"> 
												<?= $linterface->GET_BTN($button_add, "button", "checkOption(this.form.elements['targetID[]']);AddOptions(this.form.elements['targetID[]'])")?>
											</td>
										</tr>
										<tr> 
											<td align="left"> 
												<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['targetID[]']); return false;")?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<?php } ?>
<?php } ?>

<?php if($user_type == USERTYPE_STAFF){ ?>
	<tr>
		<td valign="top" nowrap="nowrap" class='tabletext'>
			<?=$Lang['Identity']['Staff']?>:
		</td>
		<td width="80%">
			<table width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td><?= $staff_selection ?></td>
					<td valign="bottom">
						<table width="100%" border="0" cellspacing="0" cellpadding="6">
							<tr> 
								<td align="left"> 
									<?= $linterface->GET_BTN($button_add, "button", "checkOption(this.form.elements['targetID[]']);AddOptions(this.form.elements['targetID[]'])")?>
								</td>
							</tr>
							<tr> 
								<td align="left"> 
									<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['targetID[]']); return false;")?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
<? } ?>	
				<tr>
                	<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
				<tr>
					<td align="center" colspan="2">
						<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();") ?>
					</td>
				</tr>
</table>
</td></tr></table>
<input type=hidden name=fieldname value="<?=$fieldname?>">
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>