<?php
// Editing by 
/*
 * 2016-08-26 (Carlos): $sys_custom['ePayment']['CashDepositApproval'] added [Cash Deposit Approval] page tab.
 * 2015-11-26 (Carlos): Modified to allow to select left students. 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$lc = new libclass();
$li = new libdb();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PageAccountDataImport_CashLog";
$linterface = new interface_html();
### Title ###
$MODULE_OBJ['title'] = $Lang['SysMgr']['SubjectClassMapping']['ModuleTitle'];
$TAGS_OBJ[] = array($Lang['Payment']['CSVImportDeposit'],"cash_log.php",0);
$TAGS_OBJ[] = array($Lang['Payment']['ManualDeposit'],"manual_cash_log.php",1);
$TAGS_OBJ[] = array($Lang['Payment']['CancelDeposit'],"cancel_cash_log.php",0);
if($sys_custom['ePayment']['CashDepositApproval']){
	$need_approval = $lpayment->isCashDepositNeedApproval();
	$is_approval_admin = $lpayment->isCashDepositApprovalAdmin();
	if($is_approval_admin){
		$TAGS_OBJ[] = array($Lang['ePayment']['CashDepositApproval'],"cash_deposit_approval.php",0);
	}
}
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();

$Msg = urldecode($Msg);
$linterface->LAYOUT_START($Msg);

if ($flag != 2)
{
    $targetClass = "";
}
$lc = new libclass();
$select_class = $lc->getSelectClass("name=targetClass onChange=changeClass(this.form)", $targetClass);

if ($flag==2 && $targetClass != "")
{
    # Get Class Num
    $target_class_name = $targetClass;
    $sql = "SELECT DISTINCT ClassNumber FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (1,3) AND ClassName = '".$lpayment->Get_Safe_Sql_Query($target_class_name)."' ORDER BY ClassNumber";
    //echo $sql;
    $classnum = $lc->returnVector($sql);
    $select_classnum = getSelectByValue($classnum, "name=targetNum",$targetNum);
    $select_classnum .= $linterface->GET_SMALL_BTN($button_add, "button", "addByClassNum();");
}

if ($flag == 1 && $student_login != '')           # Login
{
    $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType in (1,2) AND RecordStatus IN (1,3) AND UserLogin = '".$lpayment->Get_Safe_Sql_Query($student_login)."'";
    //debug_r($sql);
    $temp = $lc->returnVector($sql);
    $target_new_id = $temp[0];
    if ($target_new_id != "")
    {
        $student[] = $target_new_id;
    }
}
else if ($flag == 2 && $target_class_name != '' && $targetNum != '')
{
     $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (1,3) AND ClassName = '".$lpayment->Get_Safe_Sql_Query($target_class_name)."' AND ClassNumber = '".$lpayment->Get_Safe_Sql_Query($targetNum)."'";
     $temp = $lc->returnVector($sql);
     $target_new_id = $temp[0];
    if ($target_new_id != "")
    {
        $student[] = $target_new_id;
    }
}

$allow_item_number=false;  // allow user to input item numbers

# Last selection
$array_students = array();
if (is_array($student) && sizeof($student)>0)
{
    $list = implode(",", IntegerSafe($student));
    $namefield = getNameFieldWithClassNumberByLang();
    $sql = "SELECT UserID, CONCAT($namefield,IF(RecordStatus='3','[".$Lang['Status']['Left']."]','')) FROM INTRANET_USER WHERE RecordType in (1,2) AND UserID IN ($list) ORDER BY ClassName, ClassNumber, EnglishName";
    $array_students = $lc->returnArray($sql,2);
    if(sizeof($student)==1)
   		$allow_item_number=true;
}

if($allow_item_number){
	$select_item_num = "<SELECT name=itemcount>";
	for($i=1;$i<=20;$i++){
		$select_item_num .="<OPTION value=$i>$i</OPTION>";
	}
	$select_item_num .= "</SELECT>";
}

?>

<SCRIPT LANGUAGE=Javascript>
function addByLogin()
{
         obj = document.form1;
         obj.flag.value = 1;
         generalFormSubmitCheck(obj);
}
function removeStudent(){
		checkOptionRemove(document.form1.elements["student[]"]);
 		submitForm();
}
function submitForm(){
         obj = document.form1;
         obj.flag.value =0;
         generalFormSubmitCheck(obj);
}
function changeClass(obj)
{
         obj.flag.value = 2;
         if (obj.targetNum != undefined)
             obj.targetNum.selectedIndex = 0;
         generalFormSubmitCheck(obj);
}
function addByClassNum()
{
         obj = document.form1;
         obj.flag.value = 2;
         generalFormSubmitCheck(obj);
}
function finishSelection()
{
         obj = document.form1;
         obj.action = 'manual_cash_log2.php';
         checkOptionAll(obj.elements["student[]"]);
         obj.submit();
         return true;
}
function generalFormSubmitCheck(obj)
{
         checkOptionAll(obj.elements["student[]"]);
         obj.submit();
}
function formSubmit(obj)
{
         if (obj.flag.value == 0)
         {
             obj.flag.value = 1;
             generalFormSubmitCheck(obj);
             return true;
         }
         else
         {
             return finishSelection();
         }
}
function checkForm()
{
        obj = document.form1;

        if(obj.elements["student[]"].length != 0)
                return formSubmit(obj);
        else
        {
                alert('<?=$i_Discipline_System_alert_PleaseSelectStudent?>');
                return false;
        }
}
</SCRIPT>
<br />   
<form name="form1" id="form1" method="POST" action="" onSubmit="return checkForm();">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>&nbsp;</td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>
<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$i_general_students_selected?></td>
			<td><SELECT name=student[] size=5 style="width:200px;" multiple>
				<?
				for ($i=0; $i<sizeof($array_students); $i++)
				{
				     list ($id, $name) = $array_students[$i];
				?>
				<OPTION value='<?=$id?>'><?=$name?></OPTION>
				<?
				}
				?>
				</SELECT>
			</td>
			<td width=20%>
				<?=$linterface->GET_SMALL_BTN($button_add, "button", "newWindow('student_selection.php?fieldname=student[]',9);")?>
				&nbsp;
				<?=$linterface->GET_SMALL_BTN($button_delete, "button", "removeStudent();")?>
			</td>
		</tr>
		<tr>
			<td nowrap="nowrap" class="formfieldtitle tabletext">
				<?=$i_UserLogin?>
			</td>
			<td class="tabletext">
				<input type=text name=student_login size=20 maxlength=100>
				&nbsp;
				<?=$linterface->GET_SMALL_BTN($button_add, "button", "addByLogin();")?>
			</td>
			<td class="tabletext">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td nowrap="nowrap" class="formfieldtitle tabletext">
				<?=$i_ClassNameNumber?>
			</td>
			<td class="tabletext">
				<?=$select_class?><?=$select_classnum?>
			</td>
			<td class="tabletext">
				&nbsp;
			</td>
		</tr>
<?php if($sys_custom['ePayment']['CashDepositApproval'] && $need_approval){ ?>
		<tr>
			<td class="tabletext" colspan="3">
				<span class="red">* <?=$Lang['ePayment']['CashDepositApprovalRemark']?></span>
			</td>
		</tr>
<?php } ?>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2">        
        <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
        <tr>
        	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        </tr>
        <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_submit, "button", "document.getElementById('flag').value=3; formSubmit(this.form);") ?>
			</td>
		</tr>
        </table>                                
	</td>
</tr>
</table>                        
<br />
<input type="hidden" name="flag" id="flag" value="0" />
<input type="hidden" name="type" value="<?=escape_double_quotes($type)?>" />
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
