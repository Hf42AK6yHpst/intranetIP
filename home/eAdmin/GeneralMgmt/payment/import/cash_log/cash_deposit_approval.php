<?php
// Editing by 
/*
 * 2019-07-24 (Carlos): Changed allow to approve/reject multiple records.
 * 2014-04-02 (Carlos): Added Remark field for $sys_custom['ePayment']['CashDepositRemark']
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
$is_approval_admin = $lpayment->isCashDepositApprovalAdmin();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$is_approval_admin) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$linterface = new interface_html();

$field = trim(urldecode(stripslashes($_REQUEST['field'])));
$order = trim(urldecode(stripslashes($_REQUEST['order'])));
$pageNo = trim(urldecode(stripslashes($_REQUEST['pageNo'])));
$numPerPage = trim(urldecode(stripslashes($_REQUEST['numPerPage'])));

$arrCookies[] = array("ck_payment_cash_deposit_approval_page_size", "numPerPage");
$arrCookies[] = array("ck_payment_cash_deposit_approval_page_number", "pageNo");
$arrCookies[] = array("ck_payment_cash_deposit_approval_page_order", "order");
$arrCookies[] = array("ck_payment_cash_deposit_approval_page_field", "field");	
updateGetCookies($arrCookies);

if(!isset($field) || $field == '' || !in_array($field,array(1,2,3,4,5,6,7))) $field = 0; 
if(!isset($order) || $order == '' || !in_array($order,array(0,1))) $order = 0;
if(!isset($pageNo) || $pageNo == '') $pageNo = 1;

$pageSizeChangeEnabled = true;
if (isset($ck_payment_cash_deposit_approval_page_size) && $ck_payment_cash_deposit_approval_page_size != "") $page_size = $ck_payment_cash_deposit_approval_page_size;
$li = new libdbtable2007($field,$order,$pageNo);

if(!isset($RecordStatus))
{
	if(isset($_SESSION['PAYMENT_CASH_DEPOSIT_APPROVAL_RECORDSTATUS'])){
		$RecordStatus = $_SESSION['PAYMENT_CASH_DEPOSIT_APPROVAL_RECORDSTATUS'];
		unset($_SESSION['PAYMENT_CASH_DEPOSIT_APPROVAL_RECORDSTATUS']);
	}else{
		$RecordStatus = 1;
	}
}
$RecordStatus = IntegerSafe($RecordStatus);

$student_name_field = getNameFieldWithClassNumberByLang("u.");
$arcived_student_name_field = getNameFieldByLang2("au.");

$admin_name_field = getNameFieldByLang2("u2.");
$archived_admin_name_field = getNameFieldByLang2("au2.");

$credit_types = $lpayment->getCreditMethodArray();

$record_type_field = "CASE t.RecordType \n";
for($i=0;$i<count($credit_types);$i++){
	$record_type_field .= "WHEN '".$credit_types[$i][0]."' THEN '".$credit_types[$i][1]."' \n";
}
$record_type_field .= "END \n";

$sql = "SELECT 
			IF(u.UserID IS NOT NULL,$student_name_field,$arcived_student_name_field) as StudentName,
			".$lpayment->getWebDisplayAmountFormatDB("t.Amount")." as Amount,
			$record_type_field as RecordType,
			t.RefCode,
			IF(u2.UserID IS NOT NULL,$admin_name_field,$archived_admin_name_field) as AdminName,
			t.TransactionTime,
			t.DateInput,";
	if($sys_custom['ePayment']['CashDepositAccount']){
	$sql .="t.Account,
			t.ReceiptNo,";
	}
	$sql .=  ($sys_custom['ePayment']['CashDepositRemark']?"t.Remark,":"")." 
			CASE t.RecordStatus 
			WHEN 2 THEN '<span style=\"color:green\">".$Lang['General']['Approved']."</span>'
			WHEN 1 THEN '<span style=\"color:blue\">".$Lang['General']['Pending']."</span>' 
			WHEN -1 THEN '<span style=\"color:red\">".$Lang['General']['Rejected']."</span>' 
			END as RecordStatus,
			CONCAT('<input type=\"checkbox\" name=\"TransactionID[]\" value=\"',t.TransactionID,'\">') as CheckBox 
		FROM PAYMENT_CREDIT_TRANSACTION_PENDING_RECORD as t
		LEFT JOIN INTRANET_USER as u ON u.UserID=t.StudentID 
		LEFT JOIN INTRANET_ARCHIVE_USER as au ON au.UserID=t.StudentID 
		LEFT JOIN INTRANET_USER as u2 ON u2.UserID=t.AdminInCharge    
		LEFT JOIN INTRANET_ARCHIVE_USER as au2 ON au2.UserID=t.AdminInCharge 
		WHERE t.RecordStatus='$RecordStatus' ";
$li->sql = $sql;
$li->IsColOff = "IP25_table";

$li->field_array = array("StudentName","Amount","RecordType","t.RefCode","AdminName","TransactionTime","DateInput");
if($sys_custom['ePayment']['CashDepositAccount']){
	$li->field_array  = array_merge($li->field_array, array("t.Account","t.ReceiptNo"));
}
if($sys_custom['ePayment']['CashDepositRemark']){
	$li->field_array = array_merge($li->field_array, array("t.Remark"));
}
$li->field_array = array_merge($li->field_array, array("RecordStatus"));

$li->column_array = array(22,18,22,22,22,22,22,22,22);
$li->wrap_array = array(0,0,0,0,0,0,0,0,0);

$pos = 0;
$li->column_list .= "<th width=\"1\" class=\"num_check\">#</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $i_UserStudentName)."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $i_Payment_Field_CreditAmount)."</th>\n";
$li->column_list .= "<th>".$li->column($pos++,$i_Payment_Credit_Method)."</th>\n";
$li->column_list .= "<th>".$li->column($pos++,$i_Payment_Field_RefCode)."</th>\n";
$li->column_list .= "<th>".$li->column($pos++,$i_Payment_Field_AdminInCharge)."</th>\n";
$li->column_list .= "<th>".$li->column($pos++,$i_Payment_Field_TransactionTime)."</th>\n";
$li->column_list .= "<th>".$li->column($pos++,$i_Payment_Field_PostTime)."</th>\n";
if($sys_custom['ePayment']['CashDepositAccount']){
	$li->column_list .= "<th>".$li->column($pos++, $Lang['ePayment']['Account'])."</th>\n";
	$li->column_list .= "<th>".$li->column($pos++, $Lang['ePayment']['ReceiptNo'])."</th>\n";
}
if($sys_custom['ePayment']['CashDepositRemark']){
	$li->column_list .= "<th>".$li->column($pos++, $Lang['General']['Remark'])."</th>\n";
}
$li->column_list .= "<th>".$li->column($pos++, $Lang['General']['Status'])."</th>\n";
$li->column_list .= "<th width=\"1\">".$li->check("TransactionID[]")."</th>\n";
$li->no_col = $pos+2;

$status_ary = array(
	array(1,$Lang['General']['Pending']),
	array(-1,$Lang['General']['Rejected']),
	array(2,$Lang['General']['Approved'])
);
$status_selection = $linterface->GET_SELECTION_BOX($status_ary, ' id="RecordStatus" name="RecordStatus" onchange="document.form1.submit();" ', '', $RecordStatus, false);

$tool_buttons = array();
if(in_array($RecordStatus,array(1,-1))){
	$tool_buttons[] = array('approve',"javascript:checkEditMultiple(document.form1,'TransactionID[]','cash_deposit_approve.php')");
}
if($RecordStatus == 1){
	$tool_buttons[] = array('reject',"javascript:checkEditMultiple(document.form1,'TransactionID[]','cash_deposit_reject.php')");
}
$tool_buttons[] = array('delete',"javascript:checkRemove(document.form1,'TransactionID[]','cash_deposit_remove.php')");


$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PageAccountDataImport_CashLog";

### Title ###
$MODULE_OBJ['title'] = $Lang['SysMgr']['SubjectClassMapping']['ModuleTitle'];
$TAGS_OBJ[] = array($Lang['Payment']['CSVImportDeposit'],"cash_log.php",0);
$TAGS_OBJ[] = array($Lang['Payment']['ManualDeposit'],"manual_cash_log.php",0);
$TAGS_OBJ[] = array($Lang['Payment']['CancelDeposit'],"cancel_cash_log.php",0);
$TAGS_OBJ[] = array($Lang['ePayment']['CashDepositApproval'],"cash_deposit_approval.php",1);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();

if(isset($_SESSION['PAYMENT_CASH_DEPOSIT_APPROVAL_RESULT'])){
	$msg = $_SESSION['PAYMENT_CASH_DEPOSIT_APPROVAL_RESULT'];
	unset($_SESSION['PAYMENT_CASH_DEPOSIT_APPROVAL_RESULT']);
}
$linterface->LAYOUT_START($msg);
//debug_pr($sql);
?>
<br />
<form id="form1" name="form1" method="post" action="">
	<div class="content_top_tool">
		<div class="Conntent_tool">
		</div>
		<br style="clear:both;">
	</div>
	<br style="clear:both;">
	<div class="table_filter">
		<?=$Lang['General']['Status'].': '.$status_selection?>
	</div>
	<br style="clear:both">	
	<?=$linterface->Get_DBTable_Action_Button_IP25($tool_buttons)?>
	<br style="clear:both">			
	<?=$li->display();?>
	<input type="hidden" id="pageNo" name="pageNo" value="<?=$li->pageNo?>">
	<input type="hidden" id="order" name="order" value="<?=$li->order?>">
	<input type="hidden" id="field" name="field" value="<?=$li->field?>">
	<input type="hidden" id="page_size_change" name="page_size_change" value="">
	<input type="hidden" id="numPerPage" name="numPerPage" value="<?=$li->page_size?>">
</form>	
<br /><br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>