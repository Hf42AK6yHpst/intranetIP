<?php
// Editing by 
/*
 * 2019-09-24 Carlos: Fixed DateInput null issue, use $tran_time as the input record time.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
$is_approval_admin = $lpayment->isCashDepositApprovalAdmin();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$is_approval_admin || !count($_POST['TranssactionID'])==0) {
	intranet_closedb();
	$_SESSION['PAYMENT_CASH_DEPOSIT_APPROVAL_RESULT'] = $Lang['General']['ReturnMessage']['RecordApproveUnSuccess'];
	$_SESSION['PAYMENT_CASH_DEPOSIT_APPROVAL_RECORDSTATUS'] = $_POST['RecordStatus'];
	header ("Location:cash_deposit_approval.php");
	exit();
}

$TransactionID = IntegerSafe($_POST['TransactionID']);

$transaction_detail_map = array(2=>$i_Payment_TransactionDetailCashDeposit,4=>$Lang['ePayment']['TransactionDetailAutoPay'],6=>$Lang['ePayment']['TransactionDetailBankTransfer'],7=>$Lang['ePayment']['TransactionDetailChequeDeposit']);

$lpayment->Start_Trans();

$result_ary = array();

$sql = "INSERT INTO PAYMENT_CREDIT_TRANSACTION (StudentID,Amount,RecordType,RecordStatus,RefCode,AdminInCharge,TransactionTime,DateInput,Remark,Account,ReceiptNo)  
		SELECT StudentID,Amount,RecordType,1,RefCode,AdminInCharge,TransactionTime,DateInput,Remark,Account,ReceiptNo FROM PAYMENT_CREDIT_TRANSACTION_PENDING_RECORD WHERE TransactionID IN (".implode(",", $TransactionID).") AND RecordStatus IN (1,-1)";
$result_ary['CopyToCredit'] = $lpayment->db_db_query($sql);

$sql = "SELECT TransactionID, StudentID, Amount, DATE_FORMAT(TransactionTime,'%Y-%m-%d %H:%i:%s'),RefCode ".($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']?",RecordType ":"").",AdminInCharge,DateInput "; 
$sql.= " FROM PAYMENT_CREDIT_TRANSACTION WHERE RecordStatus = 1 AND ".($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']?"RecordType IN (2,4,6,7)":"RecordType = 2")." ORDER BY TransactionTime";
$transactions = $lpayment->returnArray($sql);



$values = "";
$delim = "";
for ($i=0; $i<sizeof($transactions); $i++)
{
     list($tid, $sid, $amount, $tran_time, $refCode) = $transactions[$i];
     $admin_in_charge = $transactions[$i]['AdminInCharge'];
     $date_input = $transactions[$i]['DateInput']; // DateInput is null, do not use it as input time
     
     $sql = "SELECT Balance FROM PAYMENT_ACCOUNT WHERE StudentID = '$sid'";
     $temp = $lpayment->returnVector($sql);
     if(count($temp)==0){
     	$sql_create_account = "INSERT INTO PAYMENT_ACCOUNT (StudentID,Balance) VALUES ('$sid',0)";
     	$result_ary['InsertAccount'.$sid] = $lpayment->db_db_query($sql_create_account);
     }
     
     $sql = "UPDATE PAYMENT_ACCOUNT SET Balance = Balance + $amount, LastUpdateByAdmin = '".$admin_in_charge."', LastUpdateByTerminal= '', LastUpdated=now() WHERE StudentID = '$sid'";
     $result_ary['UpdateAccountBalance'.$sid] = $lpayment->db_db_query($sql);
     $balanceAfter = $temp[0]+$amount;
     if($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']){
     	$transaction_detail = $transaction_detail_map[$transactions[$i]['RecordType']];
     }else{
     	$transaction_detail = $i_Payment_TransactionDetailCashDeposit;
     }
     # Change transaction time on overall trans to now()
     #$values .= "$delim ('$sid',1,'$amount','$tid','$balanceAfter','$tran_time','$i_Payment_TransactionDetailCashDeposit','$refCode')";
     //$values .= "$delim ('$sid',1,'$amount','$tid','$balanceAfter','$date_input','$transaction_detail','".$lpayment->Get_Safe_Sql_Query($refCode)."')";
     $values .= "$delim ('$sid',1,'$amount','$tid','$balanceAfter','$tran_time','$transaction_detail','".$lpayment->Get_Safe_Sql_Query($refCode)."')";
     $delim = ",";
}
$sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
               (StudentID,TransactionType,Amount,RelatedTransactionID,BalanceAfter,
               TransactionTime,Details,RefCode) VALUES $values";
$result_ary['InsertOverall'] = $lpayment->db_db_query($sql);

# Update in progress to finished status
$sql = "UPDATE PAYMENT_CREDIT_TRANSACTION SET RecordStatus = 0 
         WHERE RecordStatus = 1 AND ".($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']?"RecordType IN (2,4,6,7)":"RecordType = 2");
$result_ary['UpdateCreditRecordStatus'] = $lpayment->db_db_query($sql);

$sql = "UPDATE PAYMENT_CREDIT_TRANSACTION_PENDING_RECORD SET RecordStatus=2 WHERE TransactionID IN (".implode(",", $TransactionID).") AND RecordStatus IN (1,-1)";
$result_ary['UpdatePendingStatus'] = $lpayment->db_db_query($sql);

$_SESSION['PAYMENT_CASH_DEPOSIT_APPROVAL_RECORDSTATUS'] = $_POST['RecordStatus'];
if(in_array(false, $result_ary)){
	$lpayment->RollBack_Trans();
	$_SESSION['PAYMENT_CASH_DEPOSIT_APPROVAL_RESULT'] = $Lang['General']['ReturnMessage']['RecordApproveUnSuccess'];
}else{
	$lpayment->Commit_Trans();
	$_SESSION['PAYMENT_CASH_DEPOSIT_APPROVAL_RESULT'] = $Lang['General']['ReturnMessage']['RecordApproveSuccess'];
}

intranet_closedb();
header("Location:cash_deposit_approval.php");
exit;
?>