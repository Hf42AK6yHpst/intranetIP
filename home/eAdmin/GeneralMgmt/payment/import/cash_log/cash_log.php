<?php
// Editing by 
/*
 * 2016-08-26 (Carlos): $sys_custom['ePayment']['CashDepositApproval'] added [Cash Deposit Approval] page tab.
 * 2016-08-23 (Carlos): $sys_custom['ePayment']['CashDepositMethod'] - also use field RecordType 
 * 2015-07-22 (Carlos): $sys_custom['ePayment']['CreditMethodAutoPay'] - add field RecordType to temp table TEMP_CASH_DEPOSIT
 * 2015-05-06 (Carlos): Change TEMP_CASH_DEPOSIT float type to decimal(13,2)
 * 2014-04-02 (Carlos): Added Remark field for $sys_custom['ePayment']['CashDepositRemark']
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}


$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PageAccountDataImport_CashLog";
$linterface = new interface_html();
$limport = new libimporttext();
### Title ###
$MODULE_OBJ['title'] = $Lang['SysMgr']['SubjectClassMapping']['ModuleTitle'];
$TAGS_OBJ[] = array($Lang['Payment']['CSVImportDeposit'],"cash_log.php",1);
$TAGS_OBJ[] = array($Lang['Payment']['ManualDeposit'],"manual_cash_log.php",0);
$TAGS_OBJ[] = array($Lang['Payment']['CancelDeposit'],"cancel_cash_log.php",0);
if($sys_custom['ePayment']['CashDepositApproval']){
	$need_approval = $lpayment->isCashDepositNeedApproval();
	$is_approval_admin = $lpayment->isCashDepositApprovalAdmin();
	if($is_approval_admin){
		$TAGS_OBJ[] = array($Lang['ePayment']['CashDepositApproval'],"cash_deposit_approval.php",0);
	}
}

$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($Msg));

$li = new libdb();

if ($clear == 1)
{
  //$sql = "DELETE FROM TEMP_CASH_DEPOSIT";
  $sql ="DROP TABLE TEMP_CASH_DEPOSIT";
  $li->db_db_query($sql);
}

$sql = "CREATE TABLE IF NOT EXISTS TEMP_CASH_DEPOSIT (
				 UserLogin varchar(20),
				 ClassName varchar(255),
				 ClassNumber varchar(255),
				 Amount decimal(13,2),
				 InputTime datetime,
				 RefCode varchar(255) ";
if($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']){
	$sql .= ",RecordType varchar(1) default '2' ";
}
if($sys_custom['ePayment']['CashDepositRemark']){
	$sql.= ",Remark varchar(255) ";
}
$sql.= ") ENGINE=InnoDB charset=utf8";
$li->db_db_query($sql);


$sql = "SELECT COUNT(*) FROM TEMP_CASH_DEPOSIT";
$temp = $li->returnVector($sql);
$count = $temp[0];

if($error==1)
	$sysMsg = $i_StaffAttendance_import_invalid_entries;
//else $sysMsg = $lpayment->getResponseMsg($msg);

$PAGE_NAVIGATION[] = array($button_import);

if ($count != 0)
{
?>
	<form name="form1" method="GET" action="">
	<blockquote>
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
	    	<td>
			    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				    <tr>
						<td align="center">
							<?=$i_Payment_Import_ConfirmRemoval."<br />".$i_Payment_Import_ConfirmRemoval2?>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
	        	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	        </tr>
			    </table>
		    </td>
		</tr>
		<tr>
	    <td align="center">
				<?= $linterface->GET_ACTION_BTN($button_remove, "submit", "") ?>
			</td>
		</tr>
	</table>
	<input type=hidden name=clear value=1>
	</blockquote>
	</form>
<?
}
else
{
?>
<br/ >
<form name="form1" method="POST" action="cash_log_confirm.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
		<td align="right"><?= $sysMsg ?></td>
	</tr>
	<tr>
    	<td colspan="2">
    		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
			    <tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $i_select_file ?>
					</td>
					<td class="tabletext"><input class="file" type="file" name="userfile"><br>
					<?= $linterface->GET_IMPORT_CODING_CHKBOX() ?>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $i_general_Format ?>
					</td>
					<td class="tabletext">
						<table border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td valign="top" class="tabletext"><input type="radio" name="format" value="1" id="format1" checked></td>
								<td class="tabletext">
									<label for="format1"><?=$i_Payment_Import_CashDeposit_Instruction?></label><br />
									<!--<a class="tablelink" href="<?= GET_CSV("sample_cashdep.csv")?>" target=_blank>[<?=$i_general_clickheredownloadsample?>]</a>-->
									<a class="tablelink" href="get_import_csv_sample.php?format=1" target="_blank">[<?=$i_general_clickheredownloadsample?>]</a>
								</td>
							</tr>
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td valign="top" class="tabletext"><input type="radio" name="format" id="format2" value="2"></td>
								<td class="tabletext">
									<label for="format2"><?=$i_Payment_Import_CashDeposit_Instruction2?></label><br />
									<!--<a class="tablelink" href="<?= GET_CSV("sample_cashdep2.csv")?>" target=_blank>[<?=$i_general_clickheredownloadsample?>]</a>-->
									<a class="tablelink" href="get_import_csv_sample.php?format=2" target="_blank">[<?=$i_general_clickheredownloadsample?>]</a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="tabletext" colspan="2"><?=$Lang['formClassMapping']['Reference']?></td>
				</tr>
		<?php if($sys_custom['ePayment']['CashDepositApproval'] && $need_approval){ ?>
				<tr>
					<td class="tabletext" colspan="2"><span class="red">* <?=$Lang['ePayment']['CashDepositApprovalRemark']?></span></td>
				</tr>
		<?php } ?>
			</table>
		</td>
	</tr>
	<tr>
    	<td colspan="2">
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
    <tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
			<!--<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='pps_account_list.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>-->
		</td>
	</tr>
</table>
</form>
<br /> 
<?
}

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
