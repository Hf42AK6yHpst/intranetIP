<?php
// Editing by 
/*
 * 2019-12-23 (Bill):   Fixed current time checking failed due to too many import records     [2019-1216-1518-04206]
 * 2019-10-25 (Carlos): Added CSRF token to form submission to avoid duplicated update and cause duplicated deposit records.
 * 2019-02-21 (Carlos): $sys_custom['ePayment']['AllowFutureTimeCashDeposit'] disable future transaction checking. 
 * 2017-07-14 (Carlos): Added checking on transaction time format.
 * 2016-08-26 (Carlos): $sys_custom['ePayment']['CashDepositApproval'] added [Cash Deposit Approval] page tab.
 * 2016-08-23 (Carlos): $sys_custom['ePayment']['CashDepositMethod'] - added deposit method 1=Cash,2=Bank transfer,3=Cheque deposit.
 * 						$sys_custom['ePayment']['CashDepositAccount'] - added [Account] and [ReceiptNo].
 * 2015-11-27 (Carlos): Modified allow import for left student and staff.
 * 2015-08-05 (Carlos): Add row number and total deposit amount.
 * 2015-07-22 (Carlos): $sys_custom['ePayment']['CreditMethodAutoPay'] - added deposit method, 1=Cash, 2=Auto-pay
 * 2015-01-27 (Carlos): Initialize data of name, class, classnumber, user_login for each row. 
 * 2014-10-03 (Bill):	Allow DD/MM/YYYY hh:mm:ss
 * 2014-06-10 (Carlos): Add error checking on amount field
 * 2014-04-02 (Carlos): Added Remark field for $sys_custom['ePayment']['CashDepositRemark']
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'])
{
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PageAccountDataImport_CashLog";

$linterface = new interface_html();
$limport = new libimporttext();

$li = new libdb();
$lo = new libfilesystem();
$filepath = $_FILES["userfile"]["tmp_name"];
$filename = $_FILES["userfile"]["name"];

$sql = "DROP TABLE TEMP_CASH_DEPOSIT";
$li->db_db_query($sql);

$sql = "CREATE TABLE IF NOT EXISTS TEMP_CASH_DEPOSIT (
             UserLogin varchar(20),
             ClassName varchar(255),
             ClassNumber varchar(255),
             Amount decimal(13,2),
             InputTime datetime,
             RefCode varchar(255) ";
if($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']) {
	$sql .= ",RecordType varchar(1) default '2' ";
}
if($sys_custom['ePayment']['CashDepositRemark']) {
	$sql.= ",Remark varchar(255) ";
}
if($sys_custom['ePayment']['CashDepositAccount']) {
	$sql .= ",Account varchar(255),ReceiptNo varchar(255) ";
}
$sql.= ") ENGINE=InnoDB charset=utf8";
$li->db_db_query($sql);

// [2019-1216-1518-04206] move inside for loop
//$current_time = date('Y-m-d H:i:s');

$format = $format == 1 ? 1 : 2;
if($format == 1)
{
	//$file_format = array ("ClassName","ClassNumber","Amount","Time","RefCode");
	//$flagAry = array(1, 1, 0, 1, 1, 1);
	$file_format = array("ClassName", "ClassNumber");
	$flagAry = array(1, 1, 0);
	if($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']) {
		$file_format[] = "Deposit Method";
		$flagAry[] = 1;
	}
	$file_format[] = "Amount";
	$file_format[] = "Time";
	$file_format[] = "RefCode";
	$flagAry[] = 1;
	$flagAry[] = 1;
	$flagAry[] = 1;
	if($sys_custom['ePayment']['CashDepositAccount']) {
		$file_format[] = "Account";
		$file_format[] = "ReceiptNo";
		$flagAry[] = 1;
		$flagAry[] = 1;
	}
	if($sys_custom['ePayment']['CashDepositRemark']) {
		$file_format[] = "Remark";
		$flagAry[] = 1;
	}
}
else if($format == 2)
{
	//$file_format = array ("UserLogin","Amount","Time","RefCode");
	//$flagAry = array(1, 0, 1, 1, 1);
	$file_format = array ("UserLogin");
	$flagAry = array(1, 0);
	if($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']) {
		$file_format[] = "Deposit Method";
		$flagAry[] = 1;
	}
	$file_format[] = "Amount";
	$file_format[] = "Time";
	$file_format[] = "RefCode";
	$flagAry[] = 1;
	$flagAry[] = 1;
	$flagAry[] = 1;
	if($sys_custom['ePayment']['CashDepositAccount']) {
		$file_format[] = "Account";
		$file_format[] = "ReceiptNo";
		$flagAry[] = 1;
		$flagAry[] = 1;
	}
	if($sys_custom['ePayment']['CashDepositRemark']) {
		$file_format[] = "Remark";
		$flagAry[] = 1;
	}
}

if($filepath == "none" || $filepath == "" || !$limport->CHECK_FILE_EXT($filename))  # import failed
{
    header("Location: cash_log.php?error=1");
    exit();
}
else
{
    # Read file into array
    # return 0 if fail, return csv array if success
    // $data = $limport->GET_IMPORT_TXT($filepath);
    $data = $limport->GET_IMPORT_TXT_WITH_REFERENCE($filepath, "", "", $file_format, $flagAry);
    $col_name = array_shift($data);

    # Check file format
    $format_wrong = false;
    for ($i=0; $i<sizeof($file_format); $i++)
    {
        if ($col_name[$i] != $file_format[$i])
        {
            $format_wrong = true;
            break;
        }
    }
    if ($format_wrong)
    {
       header ("Location: cash_log.php?error=1");
       exit();
    }

    # Build student array
    $NameField = getNameFieldByLang();
    $t_sql ="SELECT UserID,".$NameField.",UserLogin,ClassName,ClassNumber,RecordType FROM INTRANET_USER WHERE (RecordType=2 OR RecordType=1) AND RecordStatus IN(0,1,2,3)";
    $t_result = $li->returnArray($t_sql,5);
    for($i=0; $i<sizeof($t_result); $i++)
    {
        list($u_id,$u_name,$u_login,$u_class,$u_classnum,$type) = $t_result[$i];
        if($format == 1)
        {
            $students[$u_class][$u_classnum]['uid'] = $u_id;
            $students[$u_class][$u_classnum]['name'] = $u_name;
            $students[$u_class][$u_classnum]['login'] = $u_login;
            $students[$u_class][$u_classnum]['class'] = $u_class;
            $students[$u_class][$u_classnum]['classnumber'] = $u_classnum;
        }
        else if($format == 2)
        {
            $students[$u_login]['uid'] = $u_id;
            $students[$u_login]['name'] = $u_name;
            $students[$u_login]['login'] = $u_login;
            $students[$u_login]['class'] = $u_class;
            $students[$u_login]['classnumber'] = $u_classnum;
            $students[$u_login]['type'] = $type;
        }
    }
          
    $error = false;
    $credit_type_text_map = array(2=>$i_Payment_Credit_TypeCashDeposit, 4=>$Lang['ePayment']['AutoPay'], 6=>$Lang['ePayment']['BankTransfer'], 7=>$Lang['ePayment']['ChequeDeposit']);
    if($sys_custom['ePayment']['CreditMethodAutoPay']) {
       $valid_credit_types = array(1,2); // import credit type are cash=1, auto-pay=2, real type values are cash=2, auto-pay=4
    }
    else if($sys_custom['ePayment']['CashDepositMethod']) {
        $valid_credit_types = array(1,2,3); // import credit type are cash=1, bank transfer=2, cheque deposit=3, real type values are cash=2, bank transfer=6, cheque deposit=7
        $credit_type_map = array(1=>2, 2=>6, 3=>7);
    }

    # Update TEMP_CASH_DEPOSIT
    $values = "";
    $delim = "";
    for ($i=0; $i<sizeof($data); $i++)
    {
        # check for empty line
        $test = trim(implode("", $data[$i]));
        if($test == "") continue;

        if($format == 1)    # Format 1
        {
            //list($class, $classnum, $amount, $tran_time, $refCode) = $data[$i];
            $col_index = -1;

            $class = $data[$i][++$col_index];
            $classnum = $data[$i][++$col_index];
            if($sys_custom['ePayment']['CreditMethodAutoPay'])
            {
                $credit_type = $data[$i][++$col_index];
                if(!in_array($credit_type,$valid_credit_types)){
                    $credit_type = 1;
                }
                $credit_type *= 2;  // times 2 to convert from import type to real type value
            }
            else if($sys_custom['ePayment']['CashDepositMethod'])
            {
                $credit_type = $data[$i][++$col_index];
                if(!in_array($credit_type,$valid_credit_types)){
                    $credit_type = 1;
                }
                $credit_type = $credit_type_map[$credit_type];  // import type to real type
            }
            $amount = $data[$i][++$col_index];
            $tran_time = $data[$i][++$col_index];
            $ref_code = $data[$i][++$col_index];
            if($sys_custom['ePayment']['CashDepositAccount']){
                $account = $data[$i][++$col_index];
                $receipt_no = $data[$i][++$col_index];
            }
            if($sys_custom['ePayment']['CashDepositRemark']){
                $remark = $data[$i][++$col_index];
            }
            $class = trim($class);
            $classnum = trim($classnum);

            $user_login = "";
            $name = "";
            $user_type = "";
            if ($class != "" && $classnum != "")
            {
                $user_login = $students[$class][$classnum]['login'];
                $name = $students[$class][$classnum]['name'];
                $user_type = 2;
            }
            if ($class != "" && $classnum != "" && $amount > 0)
            {
                if($students[$class][$classnum]['uid'] == "")   # no match student
                {
                    $error = true;

                    //$error_row = array("&nbsp;&nbsp;<font color=red><i>*</i></font>","<i>$class</i>","<i>$classnum</i>","","$amount","<i>$tran_time</i>","<i>$refCode</i>");
                    $error_row = array("&nbsp;&nbsp;<font color=red><i>*</i></font>","<i>$class</i>","<i>$classnum</i>");
                    $error_row[] = "";
                    if($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']){
                        $error_row[] = "<i>".($credit_type_text_map[$credit_type])."</i>";
                    }
                    $error_row[] = $amount;
                    $error_row[] = "<i>$tran_time</i>";
                    $error_row[] = "<i>$refCode</i>";
                    if($sys_custom['ePayment']['CashDepositAccount']){
                        $error_row[] = "<i>$account</i>";
                        $error_row[] = "<i>$receipt_no</i>";
                    }
                    if($sys_custom['ePayment']['CashDepositRemark']){
                        $error_row[] = "<i>$remark</i>";
                    }
                    $error_row[] = $i_Payment_Import_NoMatch_Entry;
                    $error_entries[] = $error_row;
                    //$error_entries[] = array("&nbsp;&nbsp;<font color=red><i>*</i></font>","<i>$class</i>","<i>$classnum</i>","","$amount","<i>$tran_time</i>","<i>$refCode</i>",$i_Payment_Import_NoMatch_Entry);

                    continue;
                }
                else
                {
                    $user_login = $students[$class][$classnum]['login'];
                    $name = $students[$class][$classnum]['name'];
                    $user_type = 2;
                }
            }
        }
        else if($format == 2)   # Format 2
        {
            //list($user_login, $amount, $tran_time, $refCode) = $data[$i];
            $col_index = -1;

            $user_login = $data[$i][++$col_index];
            if($sys_custom['ePayment']['CreditMethodAutoPay'])
            {
                $credit_type = $data[$i][++$col_index];
                if(!in_array($credit_type,$valid_credit_types)){
                    $credit_type = 1;
                }
                $credit_type *= 2;  // times 2 to convert from import type to real type value
            }
            else if($sys_custom['ePayment']['CashDepositMethod'])
            {
                $credit_type = $data[$i][++$col_index];
                if(!in_array($credit_type,$valid_credit_types)){
                    $credit_type = 1;
                }
                $credit_type = $credit_type_map[$credit_type];  // import type to real type
            }
            $amount = $data[$i][++$col_index];
            $tran_time = $data[$i][++$col_index];
            $ref_code = $data[$i][++$col_index];
            if($sys_custom['ePayment']['CashDepositAccount']){
                $account = $data[$i][++$col_index];
                $receipt_no = $data[$i][++$col_index];
            }
            if($sys_custom['ePayment']['CashDepositRemark']){
                $remark = $data[$i][++$col_index];
            }

            $name = "";
            $class = "";
            $classnum = "";
            if($students[$user_login]['uid'] == "")   # no matched student
            {
                $error = true;

                //$error_row = array("<font color=red><i>*</i></font>","","","<i>$user_login</i>","$amount","<i>$tran_time</i>","<i>$refCode</i>");
                $error_row = array("<font color=red><i>*</i></font>","","","<i>$user_login</i>");
                if($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']){
                    $error_row[] = "<i>".($credit_type_text_map[$credit_type])."</i>";
                }
                $error_row[] = "$amount";
                $error_row[] = "<i>$tran_time</i>";
                $error_row[] = "<i>$refCode</i>";
                if($sys_custom['ePayment']['CashDepositAccount']){
                    $error_row[] = "<i>$account</i>";
                    $error_row[] = "<i>$receipt_no</i>";
                }
                if($sys_custom['ePayment']['CashDepositRemark']){
                    $error_row[] = "<i>$remark</i>";
                }
                $error_row[] = $i_Payment_Import_NoMatch_Entry2;
                $error_entries[] = $error_row;
                //$error_entries[] = array("<font color=red><i>*</i></font>","","","<i>$user_login</i>","$amount","<i>$tran_time</i>","<i>$refCode</i>",$i_Payment_Import_NoMatch_Entry2);

                continue;
            }
            else if($user_login != "")
            {
                $user_type = $students[$user_login]['type'];
                $name = $students[$user_login]['name'];
                $class = $user_type==1?"-" :$students[$user_login]['class'];
                $classnum = $user_type==1?"-" :$students[$user_login]['classnumber'];
            }
        }

        # Date Time of Deposit
		//$tran_time = (trim($tran_time) == "")? date('Y-m-d H:i:s'):$tran_time;
        $is_tran_time_valid = trim($tran_time)=="" || preg_match('/^\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d$/',$tran_time) || preg_match('/^\d\d\/\d\d\/\d\d\d\d \d\d:\d\d:\d\d$/',$tran_time);
        if($is_tran_time_valid) $tran_time = (trim($tran_time) == "")? date('Y-m-d H:i:s'):(date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $tran_time))));
        $TranTimeArray = explode(" ",$tran_time);

        // [2019-1216-1518-04206] to ensure $current_time is correct when import a large amount of records (e.g. more than 300)
        $current_time = date('Y-m-d H:i:s');
        $allow_future_transaction = $sys_custom['ePayment']['AllowFutureTimeCashDeposit']? true : $tran_time <= $current_time;

        if (!$is_tran_time_valid || !checkDateIsValid($TranTimeArray[0]) || !checkTimeIsValid($TranTimeArray[1]) || !$allow_future_transaction)
        {
            $error = true;

            //$error_row = array($name,$class,$classnum,"<i>$user_login</i>","$amount","<font color=red><i>*</i></font><i>$tran_time</i>","<i>$refCode</i>");
            $error_row = array($name,$class,$classnum,"<i>$user_login</i>");
            if($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']){
                $error_row[] = "<i>".($credit_type_text_map[$credit_type])."</i>";
            }
            $error_row[] = $amount;
            $error_row[] = "<font color=red><i>*</i></font><i>$tran_time</i>";
            $error_row[] = "<i>$refCode</i>";
            if($sys_custom['ePayment']['CashDepositAccount']){
                $error_row[] = "<i>$account</i>";
                $error_row[] = "<i>$receipt_no</i>";
            }
            if($sys_custom['ePayment']['CashDepositRemark']){
                $error_row[] = "<i>$remark</i>";
            }
            $error_row[] = $Lang['General']['InvalidDateFormat'];
            $error_entries[] = $error_row;
            //$error_entries[] = array($name,$class,$classnum,"<i>$user_login</i>","$amount","<font color=red><i>*</i></font><i>$tran_time</i>","<i>$refCode</i>",$Lang['General']['InvalidDateFormat']);

            continue;
        }

        if(!is_numeric($amount) || $amount <= 0.0 || $amount == '')
        {
            $error = true;

            //$error_row = array($name,$class,$classnum,"<i>$user_login</i>","$amount","<i>$tran_time</i>","<i>$refCode</i>");
            $error_row = array($name,$class,$classnum,"<i>$user_login</i>");
            if($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']){
                $error_row[] = "<i>".($credit_type_text_map[$credit_type])."</i>";
            }
            $error_row[] = $amount;
            $error_row[] = "<i>$tran_time</i>";
            $error_row[] = "<i>$refCode</i>";
            if($sys_custom['ePayment']['CashDepositAccount']){
                $error_row[] = "<i>$account</i>";
                $error_row[] = "<i>$receipt_no</i>";
            }
            if($sys_custom['ePayment']['CashDepositRemark']){
                $error_row[] = "<i>$remark</i>";
            }
            $error_row[] = $i_Payment_Import_InvalidAmount;
            $error_entries[] = $error_row;
            //$error_entries[] = array("<font color=red><i>*</i></font>","","","<i>$user_login</i>","$amount","<i>$tran_time</i>","<i>$refCode</i>",$i_Payment_Import_InvalidAmount);

            continue;
        }

        if (!$error && $amount > 0)
        {
            if($user_type == 1 || ($user_type == 2 && $class != "" && $classnum != ""))
            {
                if ($tran_time != "") {
                    $time_str = "'$tran_time'";
                } else {
                    $time_str = "now()";
                }
                $values .= "$delim ('$user_login','$class','$classnum','$amount',$time_str,'".$li->Get_Safe_Sql_Query($ref_code)."'";
                $values .= $sys_custom['ePayment']['CashDepositRemark']? ",'".$li->Get_Safe_Sql_Query($remark)."'" : "";
                $values .= $sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']? ",'".$credit_type."'" : "";
                if($sys_custom['ePayment']['CashDepositAccount']){
                    $values .= ",'".$li->Get_Safe_Sql_Query($account)."','".$li->Get_Safe_Sql_Query($receipt_no)."'";
                }
                $values .= ")";
                $delim = ",";
            }
        }
    }

    if(!$error)
    {
        $sql = "INSERT INTO TEMP_CASH_DEPOSIT (UserLogin,ClassName,ClassNumber,Amount,InputTime,RefCode".($sys_custom['ePayment']['CashDepositRemark']?",Remark":"").($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']?",RecordType":"").($sys_custom['ePayment']['CashDepositAccount']?",Account,ReceiptNo":"").") VALUES $values";
        $li->db_db_query($sql);
    }
}

if(!$error)
{
	$namefield = getNameFieldByLang("b.");
	if($format == 1)
	{
		$sql = "SELECT $namefield, a.ClassName, a.ClassNumber, b.UserLogin ".($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']?",a.RecordType":"").", a.Amount, a.InputTime, a.RefCode ".($sys_custom['ePayment']['CashDepositRemark']?",a.Remark ":"").($sys_custom['ePayment']['CashDepositAccount']?",a.Account,a.ReceiptNo ":"")." 
                FROM TEMP_CASH_DEPOSIT as a 
                LEFT OUTER JOIN INTRANET_USER as b ON a.ClassName = b.ClassName AND a.ClassNumber = b.ClassNumber AND b.RecordType = 2 AND b.RecordStatus IN (0,1,2,3)";
    }
    else if($format == 2)
    {
   		$sql = "SELECT $namefield, a.ClassName, a.ClassNumber, b.UserLogin ".($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']?",a.RecordType":"").", a.Amount, a.InputTime, a.RefCode ".($sys_custom['ePayment']['CashDepositRemark']?",a.Remark ":"").($sys_custom['ePayment']['CashDepositAccount']?",a.Account,a.ReceiptNo ":"")." 
        	    FROM TEMP_CASH_DEPOSIT as a 
        	    LEFT OUTER JOIN INTRANET_USER as b ON a.UserLogin = b.UserLogin AND b.RecordType IN(1,2) AND b.RecordStatus IN (0,1,2,3)";
	}
	$result = $li->returnArray($sql);
}
else
{
	$result = $error_entries;
}

$display = "
<tr class=\"tabletop\">
    <td class=tabletoplink width=1>#</td>
    <td class=tabletoplink width=130>$i_Payment_Field_Username</td>
    <td class=tabletoplink width=60>$i_UserClassName</td>
    <td class=tabletoplink width=60>$i_UserClassNumber</td>
    <td class=tabletoplink width=60>$i_UserLogin</td>";
    if($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']){
        $display .= "<td class=tabletoplink width=80>".$i_Payment_Credit_Method."</td>";
    }
    $display .="<td class=tabletoplink width=80>$i_Payment_Field_CreditAmount</td>
    <td class=tabletoplink width=90>$i_Payment_Field_TransactionTime</td>
    <td class=tabletoplink width=80>$i_Payment_Field_RefCode</td>";
    if($sys_custom['ePayment']['CashDepositAccount']){
        $display.="<td class=tabletoplink width=80>".$Lang['ePayment']['Account']."</td>";
        $display.="<td class=tabletoplink width=80>".$Lang['ePayment']['ReceiptNo']."</td>";
    }
    if($sys_custom['ePayment']['CashDepositRemark']){
        $display.="<td class=tabletoplink width=80>".$Lang['General']['Remark']."</td>";
    }
    $display.="<td class=tabletoplink width=80>".$Lang['General']['Error']."</td>
</tr>\n";

$total_amount = 0.0;
for ($i=0; $i<sizeof($result); $i++)
{
	//if($sys_custom['ePayment']['CashDepositRemark']){
	//	list($name,$class,$classnum,$login,$amount,$tran_time,$refcode,$remark,$ErrorMsg) = $result[$i];
	//}else{
    // 	list($name,$class,$classnum,$login,$amount,$tran_time,$refcode,$ErrorMsg) = $result[$i];
	//}
	$col_index = -1;

	$name = $result[$i][++$col_index];
	$class = $result[$i][++$col_index];
	$classnum = $result[$i][++$col_index];
	$login = $result[$i][++$col_index];
	if($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']){
		$credit_type = $result[$i][++$col_index];
		//$display_credit_type = $credit_type == 4? $Lang['ePayment']['AutoPay'] : $i_Payment_Credit_TypeCashDeposit;
		$display_credit_type = $credit_type_text_map[$credit_type];
	}
	$amount = $result[$i][++$col_index];
	$tran_time = $result[$i][++$col_index];
	$refcode = $result[$i][++$col_index];
	if($sys_custom['ePayment']['CashDepositRemark']){
		$remark = $result[$i][++$col_index];
	}
	if($sys_custom['ePayment']['CashDepositAccount']){
		$account = $result[$i][++$col_index];
		$receipt_no = $result[$i][++$col_index];
	}
	$ErrorMsg = $result[$i][++$col_index];
	
	$total_amount += $amount;
	
    $css = ($i%2? "tablerow1 tabletext":"tablerow2 tabletext");
    $display .= "<tr class=$css>
                    <td class=\"tabletext\">".($i+1)."</td>
                    <td class=\"tabletext\">$name</td>
                    <td class=\"tabletext\">$class</td>
                    <td class=\"tabletext\">$classnum</td>
                    <td class=\"tabletext\">$login</td>";
    if($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']){
        $display .= "<td class=\"tabletext\">".$display_credit_type."</td>";
    }
    $display .= "   <td class=\"tabletext\">".$lpayment->getWebDisplayAmountFormat($amount)."</td>
                    <td class=\"tabletext\">$tran_time</td>
                    <td class=\"tabletext\">$refcode&nbsp;</td>";
    if($sys_custom['ePayment']['CashDepositAccount']){
        $display .= "<td class=\"tabletext\">$account&nbsp;</td>";
        $display .= "<td class=\"tabletext\">$receipt_no&nbsp;</td>";
    }
    if($sys_custom['ePayment']['CashDepositRemark']){
        $display .= "<td class=\"tabletext\">$remark&nbsp;</td>";
    }
    $display.= "     <td class=\"tabletext\">";
        $display .= ($ErrorMsg == "")? "&nbsp;":"<font color=red><i>*".$ErrorMsg."</i></font>";
    $display .= "    </td>
                </tr>\n";
}

$colspan = 9;
if($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']){
	$colspan += 1;
}
if($sys_custom['ePayment']['CashDepositAccount']){
	$colspan+=2;
}

$display .= "<tr class=\"tablebottom\"><td colspan=\"$colspan\" align=\"right\">".$Lang['General']['Total'].":</td><td colspan=\"5\">".$lpayment->getWebDisplayAmountFormat($total_amount)."</td></tr>";

### Title ###
$MODULE_OBJ['title'] = $Lang['SysMgr']['SubjectClassMapping']['ModuleTitle'];
$TAGS_OBJ[] = array($Lang['Payment']['CSVImportDeposit'],"cash_log.php",1);
$TAGS_OBJ[] = array($Lang['Payment']['ManualDeposit'],"manual_cash_log.php",0);
$TAGS_OBJ[] = array($Lang['Payment']['CancelDeposit'],"cancel_cash_log.php",0);
if($sys_custom['ePayment']['CashDepositApproval']){
	$need_approval = $lpayment->isCashDepositNeedApproval();
	$is_approval_admin = $lpayment->isCashDepositApprovalAdmin();
	if($is_approval_admin){
		$TAGS_OBJ[] = array($Lang['ePayment']['CashDepositApproval'],"cash_deposit_approval.php",0);
	}
}
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>

<br />
<form name="form1" method="GET" action="cash_log_update.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
    	<td>
		    <table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td colspan="2" class="tabletextremark">
						<?=$i_Payment_Import_Confirm?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
    	<td>
		    <table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<?=$display?>
			</table>
		</td>
	</tr>
<?php if($sys_custom['ePayment']['CashDepositApproval'] && $need_approval){ ?>
	<tr>
		<td>
			<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td><span class="red">* <?=$Lang['ePayment']['CashDepositApprovalRemark']?></span></td>
				</tr>
			</table>
		</td>
	</tr>
<?php } ?>
	<tr>
    	<td>
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
    <tr>
	    <td align="center">
            <? if(!$error){?>
			    <?= $linterface->GET_ACTION_BTN($button_confirm, "submit", "") ?>
			<? } ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='cash_log.php?clear=1'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>

<input type=hidden name=confirm value=1>
<input type=hidden name=format value="<?=$format?>">
<?php
echo csrfTokenHtml(generateCsrfToken());
?>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>