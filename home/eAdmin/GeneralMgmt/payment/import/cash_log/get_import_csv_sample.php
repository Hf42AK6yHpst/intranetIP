<?php
// Editing by 
/*
 * 請使用萬國碼 UTF-8 編輯此文件
 * 2016-08-23 (Carlos): $sys_custom['ePayment']['CashDepositMethod'] - added deposit method, 1=cash, 2=bank transfer, 3=cheque deposit.
 * 						$sys_custom['ePayment']['CashDepositAccount'] added [Account] and [ReceiptNo].
 * 2015-07-22 (Carlos): $sys_custom['ePayment']['CreditMethodAutoPay'] - added deposit method, 1=Cash, 2=Auto-pay
 * 2014-04-02 (Carlos): Created
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !isset($format) || $format == "") {
	header ("Location: /");
	intranet_closedb();
	exit();
}

//$lpayment = new libpayment();
$lexport = new libexporttext();

if($format == 1){
	$filename = "sample_cashdep_unicode.csv";
	//$exportColumn = array("ClassName","ClassNumber","Student Name [Ref]","Amount","Time","RefCode");
	$exportColumn = array("ClassName","ClassNumber","Student Name [Ref]");
	if($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']){
		$exportColumn[] = "Deposit Method";
	}
	$exportColumn[] = "Amount";
	$exportColumn[] = "Time";
	$exportColumn[] = "RefCode";
	
    $rows = array();
    //$rows[0] = array("(班別(英文))","(班號)","(學生姓名)[參考用途]","(存入金額)","(存入之日期時間)","(參考編號)");
    $rows[0] = array("(班別(英文))","(班號)","(學生姓名)[參考用途]");
    if($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']){
    	$rows[0][] = "(增值方法)";
    }
    $rows[0][] = "(存入金額)";
    $rows[0][] = "(存入之日期時間)";
    $rows[0][] = "(參考編號)";
    
	//$rows[1] = array("1A","11","Chan Tai Man","3500","2006-05-01 10:32:00","ABCD1234");
	$rows[1] = array("1A","11","Chan Tai Man");
	if($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']){
		$rows[1][] = "1"; // Deposit method : Cash
	}
	$rows[1][] = "3500";
	$rows[1][] = "2006-05-01 10:32:00";
	$rows[1][] = "ABCD1234";
	
	//$rows[2] = array("1B","12","","100","","");
	$rows[2] = array("1B","12","");
	if($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']){
		$rows[2][] = "2"; // Deposit method : Auto-pay
	}
	$rows[2][] = "100";
	$rows[2][] = "";
	$rows[2][] = "";
	
	if($sys_custom['ePayment']['CashDepositAccount']){
		$exportColumn[] = "Account";
		$rows[0][] = "(戶口)";
		$rows[1][] = "";
		$rows[2][] = "";
		$exportColumn[] = "ReceiptNo";
		$rows[0][] = "(收據編號)";
		$rows[1][] = "";
		$rows[2][] = "";
	}
	
	if($sys_custom['ePayment']['CashDepositRemark']){
		$exportColumn[] = "Remark";
		$rows[0][] = "(備註)";
		$rows[1][] = "現金";
		$rows[2][] = "轉帳";
	}
} else if($format == 2){
	$filename = "sample_cashdep2_unicode.csv";
	//$exportColumn = array("UserLogin","Student Name [Ref]","Amount","Time","RefCode");
	$exportColumn = array("UserLogin","Student Name [Ref]");
	if($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']){
		$exportColumn[] = "Deposit Method";
	}
	$exportColumn[] = "Amount";
	$exportColumn[] = "Time";
	$exportColumn[] = "RefCode";
	
	$rows = array();
	//$rows[0] = array("(內聯網帳號)","(學生姓名)[參考用途]","(存入金額)","(存入之日期時間)","(參考編號)");
	$rows[0] = array("(內聯網帳號)","(學生姓名)[參考用途]");
	if($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']){
		$rows[0][] = "(增值方法)";
	}
	$rows[0][] = "(存入金額)";
	$rows[0][] = "(存入之日期時間)";
	$rows[0][] = "(參考編號)";
	
	//$rows[1] = array("s00001","Chan Tai Man","3500","2006-05-01 10:32:00","ABCD1234");
	$rows[1] = array("s00001","Chan Tai Man");
	if($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']){
		$rows[1][] = "1"; // Deposit method : Cash
	}
	$rows[1][] = "3500";
	$rows[1][] = "2006-05-01 10:32:00";
	$rows[1][] = "ABCD1234";
	
	//$rows[2] = array("s00002","","100","","");
	$rows[2] = array("s00002","");
	if($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']){
		$rows[2][] = "2"; // Deposit method : Auto-pay
	}
	$rows[2][] = "100";
	$rows[2][] = "";
	$rows[2][] = "";
	
	if($sys_custom['ePayment']['CashDepositAccount']){
		$exportColumn[] = "Account";
		$rows[0][] = "(戶口)";
		$rows[1][] = "";
		$rows[2][] = "";
		$exportColumn[] = "ReceiptNo";
		$rows[0][] = "(收據編號)";
		$rows[1][] = "";
		$rows[2][] = "";
	}
	
	if($sys_custom['ePayment']['CashDepositRemark']){
		$exportColumn[] = "Remark";
		$rows[0][] = "(備註)";
		$rows[1][] = "現金";
		$rows[2][] = "轉帳";
	}
}

$export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn);	
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>