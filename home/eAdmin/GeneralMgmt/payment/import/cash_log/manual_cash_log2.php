<?php
## Using By : 
##########################################################
## Modification Log
## 2020-11-02: Ray    - Added Amount_SetAll
## 2018-11-13: Carlos - Added flag $sys_custom['ePayment']['AllowFutureTimeCashDeposit'] to disable future time checking. 
## 2017-03-17: Carlos - added delete student table row button for fast removing unwanted students. 
## 2016-08-26: Carlos - $sys_custom['ePayment']['CashDepositApproval'] added [Cash Deposit Approval] page tab.
## 2016-08-23: Carlos - $sys_custom['ePayment']['CashDepositMethod'] added credit type selection [Cash, Bank transfer, Cheque deposit].
##					  - $sys_custom['ePayment']['CashDepositAccount'] added [Account] and [ReceiptNo].
## 2015-07-22: Carlos - $sys_custom['ePayment']['CreditMethodAutoPay'] added credit type selection [Cash, Auto-pay]
## 2014-04-02: Carlos - Added Remark field for $sys_custom['ePayment']['CashDepositRemark']
##
## 2013-06-04: Carlos - modified checkForm(), do not allow input future date (and time)
##
## 2012-10-17: Carlos - modified js checkForm(), disable submit button to prevent duplication submit
##
## 2011-09-16: YatWoon
##	- remove js checking for Ref Code
##
## 2009-12-04: Max (200912041804)
## - js date validation
##########################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

if(sizeof($student)==0){
	intranet_closedb();
	header ("Location: manual_cash_log.php");
	exit();
}

$linterface = new interface_html();
$lpayment = new libpayment();
$lclass = new libclass();
$li = new libdb();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "PageAccountDataImport_CashLog";
$linterface = new interface_html();
### Title ###
$MODULE_OBJ['title'] = $Lang['SysMgr']['SubjectClassMapping']['ModuleTitle'];
$TAGS_OBJ[] = array($Lang['Payment']['CSVImportDeposit'],"cash_log.php",0);
$TAGS_OBJ[] = array($Lang['Payment']['ManualDeposit'],"manual_cash_log.php",1);
$TAGS_OBJ[] = array($Lang['Payment']['CancelDeposit'],"cancel_cash_log.php",0);
if($sys_custom['ePayment']['CashDepositApproval']){
	$need_approval = $lpayment->isCashDepositNeedApproval();
	$is_approval_admin = $lpayment->isCashDepositApprovalAdmin();
	if($is_approval_admin){
		$TAGS_OBJ[] = array($Lang['ePayment']['CashDepositApproval'],"cash_deposit_approval.php",0);
	}
}
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();


$hours = array();
$minutes = array();

for($i=0; $i<24; $i++)
{
	if($i<10)
		$i='0'.$i;
	array_push($hours, $i);
}

for($j=0; $j<60; $j++)
{
	if($j<10)
		$j='0'.$j;
	array_push($minutes, $j);
}

if($TargetDate=="")
	$TargetDate = date('Y-m-d');
	
if(sizeof($student)>0)
{
	$curr_hour = date("H");
	$curr_min = date("i");
	
	$student = IntegerSafe($student);
	$student_list = implode(",",$student);
	$namefield = getNameFieldWithClassNumberByLang();
	$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE UserID IN ($student_list) ORDER BY ClassName, ClassNumber, EnglishName";
	$result = $lclass->returnArray($sql,2);
	
	if(sizeof($result)>0)
	{
		/*
		if($sys_custom['ePayment']['CreditMethodAutoPay']){
			$credit_types = array(array('2',$i_Payment_Credit_TypeCashDeposit),array('4',$Lang['ePayment']['AutoPay']));
		}else if($sys_custom['ePayment']['CashDepositMethod']){
			$credit_types = array(
								array('2',$i_Payment_Credit_TypeCashDeposit),
								array('6',$Lang['ePayment']['BankTransfer']),
								array('7',$Lang['ePayment']['ChequeDeposit'])
							);
		}
		*/
		$credit_types = $lpayment->getCreditMethodArray();
		for($i=0; $i<sizeof($result); $i++)
		{
			list($uid, $name) = $result[$i];
			$table_content .= "<tr>";
			$table_content .= "<input type=\"hidden\" name=\"student[]\" value=\"$uid\">";
			$table_content .= "<td>$name</td>";
			if($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']){
				$credit_type_selection = $linterface->GET_SELECTION_BOX($credit_types, ' id="RecordType_'.$uid.'" name="RecordType_'.$uid.'" ', '', "2");
				$table_content .= "<td>".$credit_type_selection."</td>";
			}
			$table_content .= "<td><input type=\"text\" class=\"textbox\" name=\"amount_$uid\" size=\"8\"></td>";
			$table_content .= "<td><input type=\"text\" class=\"textbox\" name=\"ref_$uid\" size=\"8\"></td>";
			$table_content .= "<td><input type=\"text\" class=\"textbox\" name=\"TargetDate_$uid\" size=\"10\" value=\"".(sizeof($student)>1?$TargetDate:"")."\">&nbsp;<span class=extraInfo>(yyyy-mm-dd)</span></td>";
			//$curr_hour = date("H");
			//$curr_min = date("i");
			$table_content .= "<td>".getSelectByValue($hours,"name=hour_$uid",sizeof($student)>1?$curr_hour:0,0,1).":".getSelectByValue($minutes,"name=min_$uid",sizeof($student)>1?$curr_min:0,0,1)."</td>";
			if($sys_custom['ePayment']['CashDepositAccount']){
				$table_content .= '<td>'.$linterface->GET_TEXTBOX("Account_$uid", "Account_$uid", "", "", array()).'</td>';
				$table_content .= '<td>'.$linterface->GET_TEXTBOX("ReceiptNo_$uid", "ReceiptNo_$uid", "", "", array()).'</td>';
			}
			if($sys_custom['ePayment']['CashDepositRemark']){
				$table_content .= '<td>'.$linterface->GET_TEXTBOX("Remark_$uid", "Remark_$uid", "", "", array()).'</td>';
			}
			$table_content .= '<td><span class="table_row_tool"><a class="delete" href="javascript:void(0);" onclick="$(this).closest(\'tr\').remove();" alt="'.$Lang['Btn']['Delete'].'" title="'.$Lang['Btn']['Delete'].'" ></a></span></td>';
			$table_content .= "</tr>";
		}
	}
}
?>

<SCRIPT Language="JavaScript">
function setAllDate(checking)
{
	if(checking == 1)
	{
		temp_value = document.form1.TargetDate_SetAll.value;
		<?
		for($i=0; $i<sizeof($result); $i++)
		{
			list($u_id, $u_name) = $result[$i];
		?>
			temp = eval("document.form1.TargetDate_"+<?=$u_id?>);
			temp.value = temp_value;
		<?
		}
		?>
	}
	if(checking == 0)
	{
		<?
		for($i=0; $i<sizeof($result); $i++)
		{
			list($u_id, $u_name) = $result[$i];
		?>
			temp = eval("document.form1.TargetDate_"+<?=$u_id?>);
			temp.value = '';
		<?
		}
		?>
	}
}

function setAllAmount(checking) {
    if(checking == 1)
    {
        temp_value = document.form1.Amount_SetAll.value;
		<?
		for($i=0; $i<sizeof($result); $i++)
		{
		list($u_id, $u_name) = $result[$i];
		?>
        temp = eval("document.form1.amount_"+<?=$u_id?>);
        temp.value = temp_value;
		<?
		}
		?>
    }
    if(checking == 0)
    {
		<?
		for($i=0; $i<sizeof($result); $i++)
		{
		list($u_id, $u_name) = $result[$i];
		?>
        temp = eval("document.form1.amount_"+<?=$u_id?>);
        temp.value = '';
		<?
		}
		?>
    }
}

function setAllTime(checking)
{
	if(checking == 1)
	{
		selected_hour = document.form1.all_hour.value;
		selected_min = document.form1.all_min.value;
		<?
		for($i=0; $i<sizeof($result); $i++)
		{
			list($u_id, $u_name) = $result[$i];
		?>
			temp_hour = eval("document.form1.hour_"+<?=$u_id?>);
			temp_min = eval("document.form1.min_"+<?=$u_id?>);
			temp_hour.value = selected_hour;
			temp_min.value = selected_min;
		<?
		}
		?>
	}
	if(checking == 0)
	{
		<?
		for($i=0; $i<sizeof($result); $i++)
		{
			list($u_id, $u_name) = $result[$i];
		?>
			temp_hour = eval("document.form1.hour_"+<?=$u_id?>);
			temp_min = eval("document.form1.min_"+<?=$u_id?>);
			temp_hour.value = '00';
			temp_min.value = '00';
		<?
		}
		?>
	}
}
function checkForm()
{
	var checking=0;
	var btn = document.getElementById('btn_submit');
	var dateObj = new Date();
	var today = getToday();
	var todayHour = dateObj.getHours();
	var todayMin = dateObj.getMinutes();
	todayHour = todayHour < 10 ? '0' + todayHour : '' + todayHour;
	todayMin = todayMin < 10 ? '0' + todayMin : '' + todayMin;
	today = today + ' ' + todayHour + ':' + todayMin + ':00';
	btn.disabled = true;
	<?
	for($i=0; $i<sizeof($result); $i++)
	{
		list($u_id, $u_name) = $result[$i];
	?>
		temp_date 	=	eval("document.form1.TargetDate_"+<?=$u_id?>);
		temp_hour 	= 	eval("document.form1.hour_"+<?=$u_id?>);
		temp_min 	= 	eval("document.form1.min_"+<?=$u_id?>);
		temp_amount = 	eval("document.form1.amount_"+<?=$u_id?>);
		temp_ref 	=	eval("document.form1.ref_"+<?=$u_id?>);
		year		=	temp_date.value.substr(0,4);
		month		=	temp_date.value.substr(5,2);
		day			=	temp_date.value.substr(8,2);
		var temp_datetime = temp_date.value + ' ' + temp_hour.value + ':' + temp_min.value + ':00';
		
		if(temp_amount.value!='')
		{
			if(!isNaN(temp_amount.value))
			{
				if(temp_amount.value > 0)
				{
// 					if(temp_ref.value!='')
// 					{
						if(temp_date.value!='')
						{
							if (!check_date(temp_date, "<?=$i_invalid_date?>"))
							{
								btn.disabled = false;
								return false;
							}
	<?php if(!$sys_custom['ePayment']['AllowFutureTimeCashDeposit']){ ?>
							else if(temp_datetime > today)
							{
								alert("<?=$i_invalid_date?>");
								btn.disabled = false;
								return false;
							}
	<?php } ?>
						}
						else
						{
							alert("<?=$i_Payment_DepositDateWarning?>");
							btn.disabled = false;
							return false;
						}
// 					}
// 					else
// 					{
// 						alert("<?=$i_Payment_RefCodeWarning?>");
// 						return false;
// 					}
				}
				else
				{
					alert("<?=$i_Payment_DepositAmountWarning?>");
					btn.disabled = false;
					return false;
				}
			}
			else
			{
				alert("<?=$i_Payment_DepositAmountWarning?>");
				btn.disabled = false;
				return false;
			}
		}
		else
		{
			alert("<?=$i_Payment_AmountWarning?>");
			btn.disabled = false;
			return false;
		}
	<?
	}
	?>
if(checking == 1)
{
	return true;
}	
}
</SCRIPT>
<br><br><br>
<form name="form1" method="POST" action="manual_cash_log_update.php" onSubmit="return checkForm()">
<table class="common_table_list" id="CategoryContentTable" style="width:95%;">
<thead>
<tr>
	<th style="width:30%"><?=$i_UserName?></th>
	<?php
	if($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']){
		echo '<th>'.$i_Payment_Credit_Method.'</th>';
	}
	?>
	<th><?=$i_Payment_Field_Amount?>
        <input type="text" class="textbox" name="Amount_SetAll" size="8">
        <input type="checkbox" name="SetAllAmount" onClick="this.checked?setAllAmount(1):setAllAmount(0)">
    </th>
	<th><?=$i_Payment_Field_RefCode?></th>
	<th><?=$i_Payment_CashDepositDate?><br>
			<?=$linterface->Get_Date_Picker("TargetDate_SetAll",$TargetDate)?>
			&nbsp;<input type=checkbox name=SetAllDate onClick="this.checked?setAllDate(1):setAllDate(0)">
	</th>
	<th><?=$i_Payment_CashDepositTime?><br>
			<?=getSelectByValue($hours,"name=all_hour",$curr_hour,0,1)?>:<?=getSelectByValue($minutes,"name=all_min",$curr_min,0,1)?><input type=checkbox name=SetAllTime onClick="this.checked?setAllTime(1):setAllTime(0)"></th>
<?php 
	if($sys_custom['ePayment']['CashDepositAccount']){
		echo '<th>'.$Lang['ePayment']['Account'].'</th><th>'.$Lang['ePayment']['ReceiptNo'].'</th>';
	}
	if($sys_custom['ePayment']['CashDepositRemark']){
		echo '<th>'.$Lang['General']['Remark'].'</th>';
	}
?>
	<th>&nbsp;</th>
</tr>
</thead>
<?=$table_content?>
</table>
<?php if($sys_custom['ePayment']['CashDepositApproval'] && $need_approval){ ?>
<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td class="tabletext" colspan="2"><span class="red">* <?=$Lang['ePayment']['CashDepositApprovalRemark']?></span></td>
	</tr>
</table>
<?php } ?>
<br>
<table width="95%" align="center">
<tr>
    <td class="tabletext" colspan="2"><span class="red">* <?=$i_Payment_Field_RefCode?> <?=$i_Payment_Empty_Will_Be_Genreated_By_System?></span></td>
</tr>
<tr>
	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
</tr>
<tr>
	<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_submit, "submit","","btn_submit") ?>
		<?= $linterface->GET_ACTION_BTN($button_reset, "button", "this.form.reset();") ?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='manual_cash_log.php';") ?>
	</td>
</tr>
</table>
<br>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
