<?
// modifying : 
### Change Log [Start] ###
/*
*	
*/
### Change Log [End] ###

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_opendb();
$Payment = new libpayment();

$TransactionID = IntegerSafe($_REQUEST['TransactionID']);

$Payment->Start_Trans();
$Result = $Payment->Cancel_Cash_Deposit($TransactionID);

if ($Result) {
	$Payment->Commit_Trans();
	$Msg = $Lang['ePayment']['CancelDepositSuccess'];
	
}
else {
	$Payment->RollBack_Trans();
	$Msg = $Lang['ePayment']['CancelDepositUnsuccess'];
}
intranet_closedb();
header("location: cancel_cash_log.php?Msg=".urlencode($Msg));
?>