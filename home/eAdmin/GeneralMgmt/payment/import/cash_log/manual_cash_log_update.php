<?php
# using: 

############################################################
#	Date:	2016-08-26	Carlos
#			$sys_custom['ePayment']['CashDepositApproval'] added approval logic.
#
#	Date:	2016-08-23	Carlos
#			$sys_custom['ePayment']['CashDepositMethod'] added credit type [Cash, Bank transfer, Cheque deposit].
#			$sys_custom['ePayment']['CashDepositAccount'] added [Account] and [ReceiptNo].
#
#	Date:	2015-07-22  Carlos
#			$sys_custom['ePayment']['CreditMethodAutoPay'] added credit type Auto-pay which is recorded in PAYMENT_CREDIT_TRANSACTION.RecordType
#
#	Date:	2014-04-02  Carlos
#			Added Remark field for $sys_custom['ePayment']['CashDepositRemark']
#
#	Date:	2013-11-05	Carlos
#			Checked if no payment account, create one for the use, prevent fail to update balance issue
#
#	Date:	2012-03-27	YatWoon
#			Fixed: missing add addslashes for RefCode 
############################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$li = new libdb();
$lpayment = new libpayment();
$li->Start_Trans();

$Result = array();

if(sizeof($student)>0)
{
	$need_approval = false;
	$credit_transaction_table = 'PAYMENT_CREDIT_TRANSACTION';
	if($sys_custom['ePayment']['CashDepositApproval'] && $lpayment->isCashDepositNeedApproval()){
		$need_approval = true;
		$credit_transaction_table = 'PAYMENT_CREDIT_TRANSACTION_PENDING_RECORD';
	}
	
	/*$sql = "LOCK TABLES
                 PAYMENT_CREDIT_TRANSACTION WRITE
                 ,TEMP_CASH_DEPOSIT WRITE
                 ,TEMP_CASH_DEPOSIT as a WRITE
                 ,INTRANET_USER as b READ
                 ,PAYMENT_ACCOUNT as WRITE
                 ,PAYMENT_OVERALL_TRANSACTION_LOG as WRITE";
  $Result['LockTable'] = $li->db_db_query($sql);*/
    
    $transaction_detail_map = array(2=>$i_Payment_TransactionDetailCashDeposit,4=>$Lang['ePayment']['TransactionDetailAutoPay'],6=>$Lang['ePayment']['TransactionDetailBankTransfer'],7=>$Lang['ePayment']['TransactionDetailChequeDeposit']);
    
	for($i=0; $i<sizeof($student); $i++)
	{
// 		$thisRefCode = ${"ref_$student[$i]"}; 
// 		debug_pr($thisRefCode);
// $refCode = addslashes($thisRefCode);
// debug_pr($thisRefCode);
// exit;

		$input_time = ${"TargetDate_$student[$i]"}." ".${"hour_$student[$i]"}.":".${"min_$student[$i]"}.":00";
		$record_type = ($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']? $_REQUEST['RecordType_'.$student[$i]] : '2');
		
		$sql = "INSERT INTO $credit_transaction_table (
							StudentID, 
							Amount, 
							RecordType, 
							RecordStatus, 
							TransactionTime, 
							RefCode, 
							AdminInCharge,";
		if($sys_custom['ePayment']['CashDepositAccount']){
			$sql .= "Account,ReceiptNo,";
		}
		if($sys_custom['ePayment']['CashDepositRemark']){
			$sql .= "Remark,";
		}
		$sql.= " DateInput) 
						VALUES (
							'".$lpayment->Get_Safe_Sql_Query($student[$i])."', 
							'".$lpayment->Get_Safe_Sql_Query(${"amount_$student[$i]"})."', 
							'".$lpayment->Get_Safe_Sql_Query($record_type)."', 
							1, 
							'".$lpayment->Get_Safe_Sql_Query($input_time)."', 
							'".$lpayment->Get_Safe_Sql_Query(${"ref_$student[$i]"})."', 
							'".$_SESSION['UserID']."',";
		if($sys_custom['ePayment']['CashDepositAccount']){
			$__account = trim(stripslashes($_REQUEST['Account_'.$student[$i]]));
			$sql .= "'".$li->Get_Safe_Sql_Query($__account)."',";
			$__receipt = trim(stripslashes($_REQUEST['ReceiptNo_'.$student[$i]]));
			$sql .= "'".$li->Get_Safe_Sql_Query($__receipt)."',";
		}
		if($sys_custom['ePayment']['CashDepositRemark']){
			$remark = trim(stripslashes($_REQUEST['Remark_'.$student[$i]]));
			$sql .= "'".$li->Get_Safe_Sql_Query($remark)."',";
		}
		$sql .= " NOW())";
		$Result['AddCreditTransaction-UserID:'.$student[$i]] = $li->db_db_query($sql) or die(mysql_error());
	}
	
	# Update Ref Code
    $sql = "UPDATE $credit_transaction_table SET
               RefCode = CONCAT('CSH',TransactionID) 
               WHERE RecordStatus = 1 AND ".($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']?"RecordType IN (2,4,6,7)" : "RecordType=2")." AND (RefCode IS NULL OR RefCode = '')";
    $Result['UpdateRefCodeIfNotInput'] = $li->db_db_query($sql);
	
	if(!$need_approval)
	{
	
		$sql = "SELECT TransactionID, StudentID, Amount, DATE_FORMAT(TransactionTime,'%Y-%m-%d %H:%i'),RefCode,RecordType 
	            FROM $credit_transaction_table WHERE RecordStatus = 1 AND ".($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']?"RecordType IN (2,4,6,7)":"RecordType = 2")." ORDER BY TransactionTime";
	    $transactions = $li->returnArray($sql,6);
	
	    $values = "";
	    $delim = "";
	    for ($i=0; $i<sizeof($transactions); $i++)
	    {
	         list($tid, $sid, $amount, $tran_time, $refCode) = $transactions[$i];
	         $sql = "SELECT Balance FROM PAYMENT_ACCOUNT WHERE StudentID = '$sid'";
	         $temp = $li->returnVector($sql);
	         if(count($temp)==0){
	         	$sql_create_account = "INSERT INTO PAYMENT_ACCOUNT (StudentID,Balance) VALUES ('$sid',0)";
	         	$li->db_db_query($sql_create_account);
	         }
	         
	         $sql = "UPDATE PAYMENT_ACCOUNT SET Balance = Balance + $amount, LastUpdateByAdmin = '".$_SESSION['UserID']."', LastUpdateByTerminal= '', LastUpdated=now() WHERE StudentID = '$sid' ";
	         $Result['UpdateAccountBalance'.$i] = $li->db_db_query($sql);
	         $balanceAfter = $temp[0]+$amount;
	         # Change transaction time on overall trans to now()
	         #$values .= "$delim ('$sid',1,'$amount','$tid','$balanceAfter','$tran_time','$i_Payment_TransactionDetailCashDeposit','$refCode')";
	         if($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']){
	         	//$transaction_detail = $transactions[$i]['RecordType'] == 4? $Lang['ePayment']['TransactionDetailAutoPay'] : $i_Payment_TransactionDetailCashDeposit;
	         	$transaction_detail = $transaction_detail_map[$transactions[$i]['RecordType']];
	         }else{
	         	$transaction_detail = $i_Payment_TransactionDetailCashDeposit;
	         }
	         $values .= "$delim ('".$lpayment->Get_Safe_Sql_Query($sid)."',1,'".$lpayment->Get_Safe_Sql_Query($amount)."','".$lpayment->Get_Safe_Sql_Query($tid)."','".$lpayment->Get_Safe_Sql_Query($balanceAfter)."',now(),'".$lpayment->Get_Safe_Sql_Query($transaction_detail)."','".$lpayment->Get_Safe_Sql_Query($refCode)."')";
	         $delim = ",";
	    }
	    $sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
	                   (StudentID,TransactionType,Amount,RelatedTransactionID,BalanceAfter,
	                   TransactionTime,Details,RefCode) VALUES $values";
	    $Result['InsertOverallTransactionLogs'] = $li->db_db_query($sql);
	
	    # Update in progress to finished status
	    $sql = "UPDATE $credit_transaction_table SET
	               RecordStatus = 0,
	               DateInput = now() WHERE RecordStatus = 1 AND ".($sys_custom['ePayment']['CreditMethodAutoPay'] || $sys_custom['ePayment']['CashDepositMethod']?" RecordType IN (2,4,6,7) ":" RecordType=2 ");
	    $Result['SetTransactionToFinishStatus'] = $li->db_db_query($sql);
	}
	
    $sql = "DELETE FROM TEMP_CASH_DEPOSIT";
    $Result['DeleteTempRecord'] = $li->db_db_query($sql);
	
	/*$sql = "UNLOCK TABLES";
  $Result['UnLockTable'] = $li->db_db_query($sql);*/
}else{
	$Result[] = false;
}

if (in_array(false,$Result)) {
	$li->RollBack_Trans();
	$Msg = $Lang['ePayment']['ManualCashInputUnsuccess'];
}
else {
	$li->Commit_Trans();
	$Msg = $Lang['ePayment']['ManualCashInputSuccess'];
}
intranet_closedb();
header("location: manual_cash_log.php?Msg=".urlencode($Msg));
?>