<?php


$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();


$lpayment = new libpayment();
$linterface = new interface_html();

if (!$lpayment->IS_ADMIN_USER($_SESSION['UserID'])) {
	header ("Location: /");
	intranet_closedb();
	exit();
}


$student = $_POST['student'];
$flag = $_POST['flag'];
$targetClass = $_POST['targetClass'];
$targetNum = $_POST['targetNum'];
$student_login = $_POST['student_login'];

# Class selection
if ($flag != 2)
{
    $targetClass = "";
}
$lc = new libclass();
$select_class = $lc->getSelectClass("name='targetClass' onChange='changeClass(this.form)'", $targetClass);

if ($flag==2 && $targetClass != "")
{
    # Get Class Number
    $target_class_name = $targetClass;
    $sql = "SELECT DISTINCT ClassNumber, CONCAT(ClassNumber, ' (', ".getNamefieldByLang().", ')') FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (1) AND ClassName = '$target_class_name' ORDER BY ClassNumber";
    $classnum = $lc->returnArray($sql, 2);
    $select_classnum = $linterface->GET_SELECTION_BOX($classnum, "name='targetNum' ","",$targetNum);
    $select_classnum .= $linterface->GET_BTN($button_add, "button", "javascript:addByClassNum()");
}

if ($flag == 1 && $student_login != '')           # Login
{
    $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (1) AND (UserLogin = '$student_login' OR CardID = '$student_login')";
    $temp = $lc->returnVector($sql);
    $target_new_id = $temp[0];
    if ($target_new_id != "")
    {
        $student[] = $target_new_id;
    }
}
else if ($flag == 2 && $target_class_name != '' && $targetNum != '')
{
     $sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND ClassName = '$target_class_name' AND ClassNumber = '$targetNum'";
     $temp = $lc->returnVector($sql);
     $target_new_id = $temp[0];
    if ($target_new_id != "")
    {
        $student[] = $target_new_id;
    }
}

# Last selection
if (is_array($student) && sizeof($student)>0)
{
    $list = implode(",", $student);
    $namefield = getNameFieldWithClassNumberByLang();
    # sort by class and class number
    $sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE RecordType = 2 AND UserID IN ($list) ORDER BY ClassName ASC, ClassNumber ASC";
    $array_students = $lc->returnArray($sql,2);
}

$student_selected = $linterface->GET_SELECTION_BOX($array_students, "name='student[]' size='6' multiple='multiple'", "");
if (sizeof($array_students)>0 && is_array($array_students))
{
	$button_remove_html = $linterface->GET_BTN($button_remove, "button", "javascript:checkOptionRemove(document.form1.elements['student[]'])");
}

$sql="SELECT CategoryID, Name FROM DISCIPLINE_ACCU_CATEGORY WHERE CategoryID<>1 ORDER BY Name";
$temp = $lpayment->returnArray($sql);
$select_cat = getSelectByArray($temp,"name='CategoryID'",$CategoryID,0,0);


$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "AccountDataImport_DepositManual";


$TAGS_OBJ[] = array($i_Payment_Menu_ManualCashDeposit, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();

$STEPS_OBJ[] = array($i_Discipline_System_Select_Student, 1);
$STEPS_OBJ[] = array($i_Discipline_System_Add_Record, 0);

$linterface->LAYOUT_START();
?>



<script language="javascript">
function addByLogin()
{
         obj = document.form1;
         obj.flag.value = 1;
         generalFormSubmitCheck(obj);
}
function changeClass(obj)
{
         obj.flag.value = 2;
         if (obj.targetNum != undefined)
             obj.targetNum.selectedIndex = 0;
         generalFormSubmitCheck(obj);
}
function addByClassNum()
{
         obj = document.form1;
         obj.flag.value = 2;
         generalFormSubmitCheck(obj);
}
function finishSelection()
{
         obj = document.form1;
         obj.action = 'manual_cash_log2.php';
         checkOptionAll(obj.elements["student[]"]);
         obj.submit();
         return true;
}
function generalFormSubmitCheck(obj)
{
         checkOptionAll(obj.elements["student[]"]);
         obj.submit();
}
function formSubmit(obj)
{
         if (obj.flag.value == 0)
         {
             obj.flag.value = 1;
             generalFormSubmitCheck(obj);
             return true;
         }
         else
         {
             return finishSelection();
         }
}
function checkForm()
{
        obj = document.form1;
        if (obj.flag.value==1)
        {
            return addByLogin();
        }

        if(obj.elements["student[]"].length != 0)
                return formSubmit(obj);
        else
        {
                alert('<?=$i_Discipline_System_alert_PleaseSelectStudent?>');
                return false;
        }
}

</SCRIPT>

<form name="form1" method="POST" onsubmit="return checkForm();">

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?>
	</td>
</tr>
<tr>
	<td><table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_general_students_selected; ?> <span class="tabletextrequire">*</span></td><td width="70%"><table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td><?= $student_selected ?></td>
				<td valign="bottom">
				&nbsp;<?= $linterface->GET_BTN($button_select, "button", "javascript:newWindow('choose/index.php?fieldname=student[]', 9)")?><br />
				&nbsp;<?=$button_remove_html?>
				</td>
			</tr>
			</table>
			</td></tr>
		<tr><td>&nbsp;</td><td class="tablerow2"><span class="tabletextremark">(<?=$i_general_alternative?>)</span>
		<table cellpadding="0" cellspacing="0" border="0">
		<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_UserLogin; ?><br /><input onFocus="this.form.flag.value=1" type="text" name="student_login" maxlength="100" class="textboxnum" /><?= $linterface->GET_BTN($button_add, "button", "javascript:addByLogin()")?></td></tr>
		<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_ClassNameNumber; ?><br /><?=$select_class?><?=$select_classnum?></td></tr>
		</table>
		</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center"><?= $linterface->GET_ACTION_BTN($button_continue, "submit", "this.form.flag.value=3") ?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>

<br />
<input type="hidden" name="flag" value="0" />
<input type="hidden" name="type" value="<?=$type?>" />
</form>

<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
