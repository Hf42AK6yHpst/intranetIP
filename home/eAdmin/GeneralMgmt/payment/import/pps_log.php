<?php

# Editing by 

####################################### Change Log (Start) #######################################
##  2019-09-24 (Ray):    Added PPSData in TEMP_PPS_LOG
##	2018-09-28 (Carlos): Improved import records to follow current session to avoid other people clear the temp records.
##	2015-07-06 (Carlos): Added return flag $missed=1 for notifying there are records fail to import due to insufficient balance.
##  2015-06-30 (Carlos): Added menu tab [PPS File Import Log]
##	2015-05-06 (Carlos): Change TEMP_PPS_LOG float type to decimal(13,2) type.
##	Date	: 20110613 (YatWoon)
##				Improved: auto detect the file type is PPS or Counter Bill 
##
####################################### Change Log (End) #######################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "AccountDataImport_PPSLog";

$linterface = new interface_html();

$li = new libdb();

$cur_sessionid = md5(session_id());
$cur_time = date("Y-m-d H:i:s");
$threshold_date = date("Y-m-d H:i:s", strtotime("-1 day"));

$sql = "CREATE TABLE IF NOT EXISTS TEMP_PPS_LOG (
 PPSAccount varchar(255),
 Amount decimal(13,2),
 InputTime datetime
)ENGINE=InnoDB DEFAULT CHARSET=utf8";

$li->db_db_query($sql);

$sql = "ALTER TABLE TEMP_PPS_LOG CONVERT TO CHARACTER SET utf8";
$li->db_db_query($sql);

$sql = "ALTER TABLE TEMP_PPS_LOG ADD COLUMN SessionID varchar(300) NOT NULL,ADD COLUMN ImportTime datetime NOT NULL";
$li->db_db_query($sql);

$sql = "ALTER TABLE TEMP_PPS_LOG ADD COLUMN PPSData varchar(300) NOT NULL";
$li->db_db_query($sql);

if ($clear == 1)
{
    $sql = "DELETE FROM TEMP_PPS_LOG WHERE SessionID='$cur_sessionid' OR ImportTime<'$threshold_date'";
    $li->db_db_query($sql);
}

//$sql = "SELECT COUNT(*) FROM TEMP_PPS_LOG";
//$temp = $li->returnVector($sql);
//$count = $temp[0];
$count = 0;
if ($dup ==1)
{
    $xmsg = $linterface->GET_SYS_MSG("",$i_Payment_con_FileImportedAlready);
}else if($missed == 1)
{
	$xmsg = $linterface->GET_SYS_MSG("",$Lang['ePayment']['ReturnMsgPPSImportNotAllSuccess']);
}else if($no_trans==1){
	$xmsg = $linterface->GET_SYS_MSG("",$i_Payment_NoTransactions_Processed);
}


$TAGS_OBJ[] = array($i_Payment_Menu_Import_PPSLog, "", 1);
$TAGS_OBJ[] = array($Lang['ePayment']['PPSFileImportLog'],"pps_import_history.php?clearCoo=1",0);

$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if ($count != 0)
{
?>
<br />
<form name="form1" method="get" action="">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
  	<td>
	    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    <tr>
				<td align="center">
					<?=$i_Payment_Import_ConfirmRemoval."<br />".$i_Payment_Import_ConfirmRemoval2?>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
      	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
      </tr>
	    </table>
    </td>
	</tr>
  <tr>
    <td align="center">
			<?= $linterface->GET_ACTION_BTN($button_remove, "submit", "") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="clear" value="1">
</form>
<?
}
else
{
$GeneralSetting = new libgeneralsettings();

$SettingList[] = "'PPSChargeEnabled'";
$SettingList[] = "'PPSIEPSCharge'";
$SettingList[] = "'PPSCounterBillCharge'";
$Settings = $GeneralSetting->Get_General_Setting('ePayment',$SettingList);

$pps_charge_from_file = ($Settings['PPSChargeEnabled']==1);
?>
<br />
<form name="form1" method="post" action="pps_log_confirm.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
    	<td>
		    <table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td colspan="2" class="tabletext" align="right"><?=($dup=="" && $no_trans=="" && $missed=="")?$lpayment->getResponseMsg($msg):$xmsg?></td>
				</tr>
				<tr>
					<td colspan=2" class="tabletextremark"><?=$i_Payment_Import_Format_PPSLog?><br>
					<?=$Lang['ePayment']['AutoDetectFileType']?>
					</td>
				</tr>
		    	<tr>
			    	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	                	<?=$i_select_file?>
	                </td>
	                <td class="tabletext" width="70%">
	    				<input class="file" type="file" name="userfile">
	    			</td>
                </tr>
              	<? /* ?>
                <tr>
			    	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Payment_Import_PPS_FileType?></td>
	                <td class="tabletext" width="70%">
	                <input type=radio name=PPSFileType value=0 CHECKED><?=$i_Payment_Import_PPS_FileType_Normal?> &nbsp;<input type=radio name=PPSFileType value=1><?=$i_Payment_Import_PPS_FileType_CounterBill?>
	    			</td>
                </tr>
                <? */ ?>
                <tr>
			    	<td>&nbsp;</td>
	                <td class="tabletext" width="70%">
	                <? if ($pps_charge_from_file==1) { ?>
	    						<font color=red>
	    							<?=$Lang['ePayment']['PPSChargeWarning'].$Settings['PPSIEPSCharge']?>
	    							<?=$Lang['ePayment']['PPSChargeWarning1'].$Settings['PPSCounterBillCharge']?>
	    							<?=$Lang['ePayment']['PPSChargeWarning2']?>
	    						</font>
	    						<? } ?>
	    			</td>
                </tr>
		    </table>
	    </td>
	</tr>
	<tr>
    	<td>
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
    <tr>
	    <td align="center">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
		</td>
	</tr>
</table>
</form>

<?
}
$linterface->LAYOUT_STOP();
print $linterface->FOCUS_ON_LOAD("form1.userfile");
intranet_closedb();
?>
