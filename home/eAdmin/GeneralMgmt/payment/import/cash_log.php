<?php
/*
 * 2015-05-06 (Carlos): Change TEMP_CASH_DEPOSIT float type to decimal(13,2) type.
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

$limport = new libimporttext();

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "AccountDataImport_DepositCSV";

if (!$lpayment->IS_ADMIN_USER($_SESSION['UserID'])) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$li = new libdb();

if ($clear == 1)
{
    //$sql = "DELETE FROM TEMP_CASH_DEPOSIT";
    $sql ="DROP TABLE TEMP_CASH_DEPOSIT";
    $li->db_db_query($sql);
}

$sql = "CREATE TABLE IF NOT EXISTS TEMP_CASH_DEPOSIT (
 UserLogin varchar(20),
 ClassName varchar(255),
 ClassNumber varchar(255),
 Amount decimal(13,2),
 InputTime datetime,
 RefCode varchar(255)
)";
$li->db_db_query($sql);


$sql = "SELECT COUNT(*) FROM TEMP_CASH_DEPOSIT";
$temp = $li->returnVector($sql);
$count = $temp[0];

$linterface = new interface_html();
$TAGS_OBJ[] = array($i_Payment_Menu_Import_CashDeposit, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if($error==1)
	$sysMsg = $linterface->GET_SYS_MSG("",$i_StaffAttendance_import_invalid_entries);
else $sysMsg = $lpayment->getResponseMsg($msg);

if ($count != 0)
{
?>
<br />
<form name="form1" method="get" action="">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
    	<td>
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
			    <tr>
					<td align="center">
						<?=$i_Payment_Import_ConfirmRemoval."<br />".$i_Payment_Import_ConfirmRemoval2?>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
    <tr>
	    <td align="center">
			<?= $linterface->GET_ACTION_BTN($button_remove, "submit", "") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="clear" value="1">
</form>
<?
}
else
{
?>
<br />
<form name="form1" method="post" action="cash_log_confirm.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
    	<td>
		    <table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td colspan="2" class="tabletext" align="right"><?=$sysMsg?></td>
				</tr>
				<tr>
			    	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	                	<?=$i_select_file?>
	                </td>
	                <td class="tabletext" width="70%">
	    				<input class="file" type="file" name="userfile"><br>
	    				<?= $linterface->GET_IMPORT_CODING_CHKBOX() ?>
	    			</td>
                </tr>
                <tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $i_general_Format ?>
					</td>
					<td class="tabletext">
						<table border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td valign="top" class="tabletext"><input type="radio" name="format" value="1" id="format1" checked></td>
								<td valign="top" class="tabletext" style="padding-top:5px;">
									<label for="format1"><?=$i_Payment_Import_CashDeposit_Instruction?></label><br />
									<a class="tablelink" href="<?= GET_CSV("sample_cashdep.csv")?>" target="_blank">[<?=$i_general_clickheredownloadsample?>]</a>
								</td>
							</tr>
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td valign="top" class="tabletext"><input type="radio" name="format" id="format2" value="2"></td>
								<td class="tabletext" style="padding-top:5px;">
									<label for="format2"><?=$i_Payment_Import_CashDeposit_Instruction2?></label><br />
									<a class="tablelink" href="<?= GET_CSV("sample_cashdep2.csv")?>" target="_blank">[<?=$i_general_clickheredownloadsample?>]</a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
    	<td>
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
    <tr>
	    <td align="center">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
</form>

<?
}
$linterface->LAYOUT_STOP();
print $linterface->FOCUS_ON_LOAD("form1.userfile");
intranet_closedb();
?>
