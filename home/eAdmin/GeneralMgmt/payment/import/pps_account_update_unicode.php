<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "AccountDataImport_PPSLinkage";

if (!$lpayment->IS_ADMIN_USER($_SESSION['UserID'])) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

define("ERROR_CODE_NO_MATCH_STUDENT",1);
define("ERROR_CODE_CRASHED_PPS_NO",2);
define("ERROR_CODE_MISSING_PPS_NO",3);
define("ERROR_CODE_ALREADY_HAS_PPS_NO",4);

$li = new libdb();
$limport = new libimporttext();
$filepath = $userfile;
$filename = $userfile_name;

if($filepath=="none" || $filepath == ""){          # import failed
        header("Location: pps_account_unicode.php?failed=2");
} else {
        if($limport->CHECK_FILE_EXT($filename)) {
            # read file into array
            # return 0 if fail, return csv array if success
            $data = $limport->GET_IMPORT_TXT($filepath);
            array_shift($data);                   # drop the title bar
        }
        else
        {
            header("Location: pps_account_unicode.php?failed=2");
            exit();
        }
        # Get Existing PPS Account
        $sql = "SELECT DISTINCT PPSAccountNo FROM PAYMENT_ACCOUNT WHERE PPSAccountNo IS NOT NULL
                AND PPSAccountNo != ''";
        $current_pps = $li->returnVector($sql);

        # Get Existing Students
        
        $sql = "SELECT ClassName, ClassNumber, UserID,UserLogin
                FROM INTRANET_USER
                WHERE RecordType = 2 AND RecordStatus = 1
                      AND ClassName != '' AND ClassNumber != ''
                ORDER BY ClassName, ClassNumber";

        $result = $li->returnArray($sql,4);
        for ($i=0; $i<sizeof($result); $i++)
        {
             list($class,$classnum,$uid,$userlogin) = $result[$i];
             if($format==1){
	             $students[$class][$classnum]=$uid;
	         }
             else if($format==2){
	             	$students[$userlogin] = $uid;
	         }
        }

        # Counts
        $count_successful = 0;
        $count_no_match_student = 0;
        $count_crashed_pps_no = 0;
        $count_student_has_pps_already = 0;
        $count_pps_no_missing = 0;
        $array_error = array();

        # Update PAYMENT_ACCOUNT
        for ($i=0; $i<sizeof($data); $i++)
        {
	        if($format==1){ # Format 1
		         list($class,$classnum,$pps) = $data[$i];

	             if ($class == "" && $classnum == "")
	             {
	                 $count_no_match_student++;
	                 $array_error[] = array($i,ERROR_CODE_NO_MATCH_STUDENT);
	                 continue;
	             }

	             $targetUID = $students[$class][$classnum];
		    }
	        else if($format==2){ # Format 2
	        
                 list($u_login,$pps) = $data[$i];
	             if ($u_login == "")
	             {
	                 $count_no_match_student++;
	                 $array_error[] = array($i,ERROR_CODE_NO_MATCH_STUDENT);
	                 continue;
	             }
	             
             	 $targetUID = $students[$u_login];

		    }


             if ($targetUID == "")
             {
                 $count_no_match_student++;
                 $array_error[] = array($i,ERROR_CODE_NO_MATCH_STUDENT);
                 continue;
             }

             if ($pps != "")
             {
                 if (in_array($pps,$current_pps))
                 {
                     $count_crashed_pps_no++;
                     $array_error[] = array($i,ERROR_CODE_CRASHED_PPS_NO);
                     continue;
                 }
                 else $current_pps[] = $pps;
             }
             else
             {
                 $count_pps_no_missing++;
                 $array_error[] = array($i,ERROR_CODE_MISSING_PPS_NO);
                 continue;
             }

             $sql = "INSERT INTO PAYMENT_ACCOUNT (StudentID,PPSAccountNo,Balance) VALUES
                     ($targetUID,'$pps',0)";
             $li->db_db_query($sql);
             if ($li->db_affected_rows() != 1)
             {
                 $sql = "UPDATE PAYMENT_ACCOUNT SET PPSAccountNo = '$pps' WHERE StudentID = $targetUID AND (PPSAccountNo IS NULL OR PPSAccountNo = '')";
                 $li->db_db_query($sql);
                 /* For not update */
                 if ($li->db_affected_rows() != 1)
                 {
                     $array_error[] = array($i,ERROR_CODE_ALREADY_HAS_PPS_NO);
                     $count_student_has_pps_already++;
                 }
                 else
                 {
                     $count_successful++;
                 }

             }
             else
             {
                 $count_successful++;
             }
        }

}

$display = "<tr>
				<td class=\"tabletoplink tabletop\">$i_Payment_Import_PPSLink_Result_Summary_TotalRows</td>
				<td class=\"tabletext\">".sizeof($data)."</td>
			</tr>
			<tr>
				<td class=\"tabletoplink tabletop\">$i_Payment_Import_PPSLink_Result_Summary_Successful</td>
				<td class=\"tabletext\">$count_successful</td>
			</tr>
			<tr>
				<td class=\"tabletoplink tabletop\">$i_Payment_Import_PPSLink_Result_Summary_NoMatch</td>
				<td class=\"tabletext\">$count_no_match_student</td>
			</tr>
			<tr>
				<td class=\"tabletoplink tabletop\">$i_Payment_Import_PPSLink_Result_Summary_Occupied</td>
				<td class=\"tabletext\">$count_crashed_pps_no</td>
			</tr>
			<tr>
				<td class=\"tabletoplink tabletop\">$i_Payment_Import_PPSLink_Result_Summary_Missing</td>
				<td class=\"tabletext\">$count_pps_no_missing</td>
			</tr>
			<tr>
				<td class=\"tabletoplink tabletop\">$i_Payment_Import_PPSLink_Result_Summary_ExistAlready</td>
				<td class=\"tabletext\">$count_student_has_pps_already</td>
			</tr>\n";

$linterface = new interface_html();
$TAGS_OBJ[] = array($i_Payment_Menu_Import_PPSLink, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_import);
$PAGE_NAVIGATION2 = $i_Payment_Import_PPSLink_Result;
$PAGE_NAVIGATION3 = $i_Payment_Import_PPSLink_Result_ErrorMsg_NotSuccessful;
?>
<? echo print_r($data); ?>
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
    	<td>
    		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
			    <tr>
					<td colspan="2"><?= $linterface->GET_NAVIGATION2($PAGE_NAVIGATION2) ?></td>
				</tr>
				<?=$display?>
			</table>
		</td>
	</tr>
	<? if (sizeof($array_error) != 0) { ?>
	<tr>
		<td>
    		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
			    <tr>
					<td colspan="2"><?= $linterface->GET_NAVIGATION2($PAGE_NAVIGATION3) ?></td>
				</tr>
				<tr class="tabletop">
					<td class="tabletoplink">#</td>
					<td class="tabletoplink"><?=$i_Payment_Import_PPSLink_Result_ErrorMsg_Reason?></td>
				</tr>
				
	<?
	for ($i=0; $i < sizeof($array_error); $i++) {
		list ($t_row, $t_type) = $array_error[$i];
		$t_row++;
		$css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
		switch ($t_type)
		{
			case ERROR_CODE_NO_MATCH_STUDENT:
				$t_string_error = $i_Payment_Import_PPSLink_Result_ErrorMsg_NoMatch; break;
			case ERROR_CODE_CRASHED_PPS_NO:
				$t_string_error = $i_Payment_Import_PPSLink_Result_ErrorMsg_Occupied; break;
			case ERROR_CODE_MISSING_PPS_NO:
				$t_string_error = $i_Payment_Import_PPSLink_Result_ErrorMsg_Missing; break;
			case ERROR_CODE_ALREADY_HAS_PPS_NO:
				$t_string_error = $i_Payment_Import_PPSLink_Result_ErrorMsg_ExistAlready; break;
			default:
				$t_string_error = "Unknown Error";
		}
	?>
			<tr class="<?=$css?>">
				<td class="tabletext"><?=$t_row?></td>
				<td class="tabletext"><?=$t_string_error?></td>
			</tr>
	<? } ?>
			</table>
		</td>
	</tr>
	<? } ?>
	<tr>
    	<td>
    		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
			    <tr>
					<td align="left">
						<a class="tablelink" href="pps_account_list.php"><?=$i_Payment_Import_PPSLink_Result_GoToIndex?></a>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>


<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
