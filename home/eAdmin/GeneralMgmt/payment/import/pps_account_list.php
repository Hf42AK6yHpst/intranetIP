<?php
// Editing by 
#######################################
#	
#	2015-10-26 Carlos: added user type selection filter.
#	Date:	2012-11-29	YatWoon
#			Improved: display system message for "Record Deleted." [Case#2012-1129-1101-35066]
#
#######################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
# preserve table view
if ($ck_account_data_import_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_account_data_import_page_number", $pageNo, 0, "", "", 0);
	$ck_account_data_import_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_account_data_import_page_number!="")
{
	$pageNo = $ck_account_data_import_page_number;
}

if ($ck_account_data_import_page_order!=$order && $order!="")
{
	setcookie("ck_account_data_import_page_order", $order, 0, "", "", 0);
	$ck_account_data_import_page_order = $order;
} else if (!isset($order) && $ck_account_data_import_page_order!="")
{
	$order = $ck_account_data_import_page_order;
}

if ($ck_account_data_import_page_field!=$field && $field!="")
{
	setcookie("ck_account_data_import_page_field", $field, 0, "", "", 0);
	$ck_account_data_import_page_field = $field;
} else if (!isset($field) && $ck_account_data_import_page_field!="")
{
	$field = $ck_account_data_import_page_field;
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "AccountDataImport_PPSLinkage";

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$linterface = new interface_html();


# TABLE SQL
$keyword = trim($keyword);
/*
$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 0;
*/

$user_type_ary = array(array(USERTYPE_STUDENT,$Lang['Identity']['Student']),array(USERTYPE_STAFF,$Lang['Identity']['Staff']));
$user_type_selection = $linterface->GET_SELECTION_BOX($user_type_ary, ' name="user_type" id="user_type" onchange="document.form1.submit();" ', $i_Payment_All_Users, $user_type, false);

$user_field = getNameFieldByLang("b.");

$user_type_cond = '';
if($user_type != ''){
	$user_type_cond = " AND b.RecordType='".$user_type."' ";
}

$sql = "SELECT
               b.ClassName, b.ClassNumber, b.UserLogin, $user_field, a.PPSAccountNo,
               CONCAT('<input type=checkbox name=StudentID[] value=', a.StudentID ,'>')
         FROM
             PAYMENT_ACCOUNT as a INNER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
         WHERE
              PPSAccountNo IS NOT NULL AND PPSAccountNo != '' $user_type_cond
              AND (b.ChineseName like '%$keyword%' OR
                    b.EnglishName like '%$keyword%' OR
                    b.ClassName like '%$keyword%' OR
                    b.ClassNumber like '%$keyword%' OR
                    b.UserLogin like '%$keyword%' OR
                    a.PPSAccountNo like '%$keyword%'
                    )";

# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("concat(b.ClassName,b.ClassNumber)","b.ClassNumber","b.UserLogin",$user_field,"a.PPSAccountNo");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='20%'>".$li->column(0, $i_UserClassName)."</td>\n";
$li->column_list .= "<td width='20%'>".$li->column(1, $i_UserClassNumber)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column(2, $i_UserLogin)."</td>\n";
$li->column_list .= "<td width='25%'>".$li->column(3, $i_UserName)."</td>\n";
$li->column_list .= "<td width='20%'>".$li->column(4, $i_Payment_Field_PPSAccountNo)."</td>\n";
$li->column_list .= "<td width='1'>".$li->check("StudentID[]")."</td>\n";

$toolbar .= $linterface->GET_LNK_IMPORT("javascript:checkNew('pps_account.php')","","","","",0);
$toolbar .= $linterface->GET_LNK_EXPORT("javascript:checkNew('pps_account_export.php')","","","","",0);

$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkRemove(document.form1,'StudentID[]','pps_account_remove.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_remove
					</a>
				</td>";

$searchbar = "<input class=\"textboxnum\" type=\"text\" name=\"keyword\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= $linterface->GET_SMALL_BTN($button_find, "button", "document.form1.submit();","submit2");

$TAGS_OBJ[] = array($i_Payment_Menu_Import_PPSLink, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("delete");
?>
<br />
<form name="form1" method="get" action="">
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?></td>
		<td align="right"><?=$SysMsg?></td>
	</tr>
</table>
<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr class="table-action-bar">
		<td align="left"><?=$user_type_selection.'&nbsp;'.$searchbar?></td>
		<td valign="bottom" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr><?=$table_tool?></tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<?php echo $li->display("96%"); ?>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
</form>
<br />
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>