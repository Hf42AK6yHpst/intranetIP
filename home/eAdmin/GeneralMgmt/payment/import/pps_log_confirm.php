<?php
# Editing by 

####################################### Change Log (Start) #######################################
##  2019-09-24 (Ray):    Added PPSData
##	2019-01-09 (Carlos): Disable the confirm button after submit to prevent duplicated submission.
## 	2018-09-28 (Carlos): Improved import records to follow current session to avoid other people clear the temp records.
##  2018-03-19 (Carlos): $sys_custom['ePayment']['PPSFileLogCheckFileHash'] Check file hash value to allow more than one pps files in the same day can be imported.
##	2015-10-27 (Carlos): Allow staff account to import PPS credit records.
##	2015-06-30 (Carlos): Added menu tab [PPS File Import Log]
##	Date	: 20110613 (YatWoon)
##				Improved: auto detect the file type is PPS or Counter Bill 
##				Improved: display file type
##
####################################### Change Log (End) #######################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "AccountDataImport_PPSLog";

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$li = new libdb();
$filepath = $_FILES["userfile"]["tmp_name"];
$filename = $_FILES["userfile"]["name"];

# Check previously stored value of ChargeDeduction
$GeneralSetting = new libgeneralsettings();

$SettingList[] = "'PPSChargeEnabled'";
$SettingList[] = "'PPSIEPSCharge'";
$SettingList[] = "'PPSCounterBillCharge'";
$Settings = $GeneralSetting->Get_General_Setting('ePayment',$SettingList);

$pps_auto_charge = ($Settings['PPSChargeEnabled']==1);

$cur_sessionid = md5(session_id());
$cur_time = date("Y-m-d H:i:s");
$threshold_date = date("Y-m-d H:i:s", strtotime("-1 day"));

if($filepath=="none" || $filepath == ""){          # import failed
	header("Location: pps_log.php");
	exit();
} else {
	$file_content = get_file_content($filepath);
	$file_hash_value = $sys_custom['ePayment']['PPSFileLogCheckFileHash']? md5($file_content) : '';
	$data = $lpayment->parsePPSLogFile($file_content);
	$PPSFileType = $lpayment->pps_file_type;	# 0 - PPS, 1 - Counter Bill
	
	# Check PPS Record Date
	if ($lpayment->isPPSLogFileUpdatedBefore($PPSFileType,$file_hash_value))
	{
		intranet_closedb();
		header("Location: pps_log.php?dup=1");
		exit();
	}
	
	# Update TEMP_PPS_LOG
	$values = "";
	$delim = "";
	for ($i=0; $i<sizeof($data); $i++)
	{
		$record = $data[$i];
		$bill_account = $record['account'];
		$credit_amount = $record['amount'];
		/*
		if ($pps_auto_charge)
		{
		   $credit_amount -= LIBPAYMENT_PPS_CHARGE;
		}
		*/
		$tran_date = $record['date'];
		$tran_hr = $record['hour'];
		$tran_min = $record['min'];
		$ppsdata = $record['ppsdata'];

		if ($PPSFileType==1) # Counter bill
		{
		   # New for counter bill
		   $tran_date_counter = $record['date_counter'];
		   $tran_hr_counter = $record['hr_counter'];
		   $tran_min_counter = $record['min_counter'];
		   if ($tran_date_counter != "" && $tran_hr_counter!="" && $tran_min_counter!="")
		   {
		       $tran_date = $tran_date_counter;
		       $tran_hr = $tran_hr_counter;
		       $tran_min = $tran_min_counter;
		   }
		}
		else
		{
		}
		
		
		$values .= "$delim ('$bill_account','$credit_amount','$tran_date$tran_hr$tran_min"."00"."','$cur_sessionid','$cur_time','$ppsdata')";
		$delim = ",";
	}
	
	// clear the temp record first
	$sql = "delete from TEMP_PPS_LOG where SessionID='$cur_sessionid' OR ImportTime<'$threshold_date'"; // clear temp import records that are in current session or import time older than one day before
	$li->db_db_query($sql);
	$sql = "INSERT INTO TEMP_PPS_LOG (PPSAccount,Amount,InputTime,SessionID,ImportTime, PPSData) VALUES $values";
	$li->db_db_query($sql);
}

$namefield = getNameFieldWithClassNumberByLang("b.");
$sql = "SELECT 
					a.PPSAccount, 
					IF(b.UserID IS NULL,'$i_Payment_AccountNoMatch',$namefield), 
					b.UserLogin, 
					a.Amount, 
					DATE_FORMAT(a.InputTime,'%Y-%m-%d %H:%i:%s')
        FROM 
        	TEMP_PPS_LOG as a 
        	LEFT OUTER JOIN 
        	PAYMENT_ACCOUNT as c 
        	ON a.PPSAccount = c.PPSAccountNo
          LEFT OUTER JOIN 
          INTRANET_USER as b 
          ON c.StudentID = b.UserID 
          	AND b.RecordType IN (1,2)  
          	AND b.RecordStatus IN (0,1,2) 
		WHERE a.SessionID='$cur_sessionid' 
        ORDER BY a.InputTime";
$result = $li->returnArray($sql,5);


$hasTransaction = false;
if(sizeof($result)>0)
	$hasTransaction = true;

if ($pps_auto_charge)
{
$display = "<tr class=\"tabletop\">
				<td class=\"tabletoplink\" width=\"15%\">$i_Payment_Field_PPSAccountNo</td>
				<td class=\"tabletoplink\" width=\"20%\">$i_UserName</td>
				<td class=\"tabletoplink\" width=\"15%\">$i_UserLogin</td>
				<td class=\"tabletoplink\" width=\"15%\">$i_Payment_Field_CreditAmount</td>
				<td class=\"tabletoplink\" width=\"15%\">$i_Payment_PPS_Charge</td>
				<td class=\"tabletoplink\" width=\"20%\">$i_Payment_Field_TransactionTime</td>
			</tr>\n";
}
else
{
$display = "<tr class=\"tabletop\">
				<td class=\"tabletoplink\" width=\"20%\">$i_Payment_Field_PPSAccountNo</td>
				<td class=\"tabletoplink\" width=\"20%\">$i_UserName</td>
				<td class=\"tabletoplink\" width=\"20%\">$i_UserLogin</td>
				<td class=\"tabletoplink\" width=\"15%\">$i_Payment_Field_CreditAmount</td>
				<td class=\"tabletoplink\" width=\"25%\">$i_Payment_Field_TransactionTime</td>
			</tr>\n";
}


$total_amount = 0;

if ($PPSFileType==1) # Counter bill
{
    $handling_charge = $Settings['PPSCounterBillCharge']==''? 0 : number_format($Settings['PPSCounterBillCharge'],2);
}
else
{
    $handling_charge = $Settings['PPSIEPSCharge']==''? 0 : number_format($Settings['PPSIEPSCharge'],2);
}

if (sizeof($result) > 0) {
	$PossibleDuplicateTran = false;
	for ($i=0; $i<sizeof($result); $i++)
	{
		list($pps,$name,$login,$amount,$tran_time) = $result[$i];
		$total_amount += $amount;
		// $original_amount = $amount+LIBPAYMENT_PPS_CHARGE;
		$amount = number_format($amount,2);
		// $original_amount = number_format($original_amount,2);
		
		$sql = 'select 
							count(1) 
						from 
							PAYMENT_MISSED_PPS_BILL 
						where 
							PPSAccount = \''.$pps.'\' 
							and 
							Amount = \''.$amount.'\' 
							and 
							InputTime like \''.substr($tran_time,0,10).'%\'';
		$FoundInMissedPPS = $li->returnVector($sql);
		
		$sql = 'select 
							count(1) 
						from 
							PAYMENT_CREDIT_TRANSACTION ct 
						where 
							PPSAccountNo = \''.$pps.'\' 
							and 
							Amount = \''.$amount.'\' 
							and 
							TransactionTime like \''.substr($tran_time,0,10).'%\'';
		$FoundInCreditTran = $li->returnVector($sql);
		
		if ($FoundInCreditTran[0] > 0 || $FoundInMissedPPS[0] > 0) {
			$PossibleDuplicateTranWarning = '<font style="color:red">**</font>';
			$PossibleDuplicateTran = true;
		}
		else 
			$PossibleDuplicateTranWarning = '';
		$css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
		if ($pps_auto_charge)
		{
			$display .= "<tr class=\"$css\">";
			$display .= "<td class=\"tabletext\">$pps".$PossibleDuplicateTranWarning."</td><td class=\"tabletext\">$name</td><td class=\"tabletext\">$login</td>";
			$display .= "<td class=\"tabletext\">$".$amount."</td><td class=\"tabletext\">$".$handling_charge."</td><td class=\"tabletext\">$tran_time</td>";
			$display .= "</tr>\n";
		}
		else
		{
			$display .= "<tr class=\"$css\">";
			$display .= "<td class=\"tabletext\">$pps".$PossibleDuplicateTranWarning."</td><td class=\"tabletext\">$name</td><td class=\"tabletext\">$login</td>";
			$display .= "<td class=\"tabletext\">$".$amount."</td><td class=\"tabletext\">$tran_time</td>";
			$display .= "</tr>\n";
		}
	}
} else {
	$display .= "<tr class=\"$css\">";
	$display .= "<td align=\"center\" colspan=\"".($pps_auto_charge?"6":"5")."\" class=\"tabletext\">$i_no_record_searched_msg</td>";
	$display .= "</tr>\n";
}

$linterface = new interface_html();
$TAGS_OBJ[] = array($i_Payment_Menu_Import_PPSLog, "", 1);
$TAGS_OBJ[] = array($Lang['ePayment']['PPSFileImportLog'],"pps_import_history.php?clearCoo=1",0);

$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

?>
<br />
<script>
function Confirm_Import() {
	var FormObj = document.getElementById('form1');
	var confirmBtn = document.getElementById('confirm_btn');
	confirmBtn.disabled = true;
<?
	if ($PossibleDuplicateTran) {
?>
	if (confirm('<?=$Lang['ePayment']['PossibleDuplicatePPSJSWarning']?>')) {
		FormObj.submit();
	}
	else {
		confirmBtn.disabled = false;
		return false;
	}
<?
	}
	else {
?>
		FormObj.submit();
<?
	}
?>
}
</script>
<form name="form1" id="form1" method="get" action="pps_log_update.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
    	<td>
		    <table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td colspan="2" class="tabletextremark"><?=$i_Payment_Import_Confirm?></td>
				</tr>
				<tr>
					<td colspan="2" class="tabletext">
						<span class="tabletextrequire"><?=$i_Payment_Import_PPS_FileType?> : <?=$PPSFileType ? $i_Payment_Import_PPS_FileType_CounterBill :$i_Payment_Import_PPS_FileType_Normal; ?></span>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="tabletext">
						<span class="tabletextrequire"><?=$i_Payment_TotalAmount?> : $<?=number_format($total_amount,2)?></span>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="tabletext">
						<span class="tabletextrequire">
							<? if ($pps_auto_charge) { ?>
  						<font color=red>
  							<?=$Lang['ePayment']['PPSChargeWarning'].$Settings['PPSIEPSCharge']?>
  							<?=$Lang['ePayment']['PPSChargeWarning1'].$Settings['PPSCounterBillCharge']?>
  							<?=$Lang['ePayment']['PPSChargeWarning2']?>
  						</font>
  						<? } ?>
						</span>
					</td>
				</tr>
				
			</table>
		</td>
	</tr>
	<tr>
    	<td>
		    <table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
		    	<?=$display?>
			</table>
		</td>
	</tr>
	<tr><td class="tabletext">
		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class='tabletext'><font color=red>*</font>
		<?=$i_Payment_AccountNoMatch_Remark?></td></tr>
		</table>
	</td></tr>
	<tr><td class="tabletext">
		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class='tabletext'><font color=red>**</font>
		<?=$Lang['ePayment']['PossibleDuplicatePPSWarning']?></td></tr>
		</table>
	</td></tr>
	
	<tr>
    	<td>
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
    <tr>
	    <td align="center">
			<?= $linterface->GET_ACTION_BTN($button_confirm, "button", "Confirm_Import();","confirm_btn") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='pps_log.php?clear=1'","cancel_btn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
<input type="hidden" name="confirm" value="1" />
<input type="hidden" name="FileDate" value="<?=$lpayment->pps_file_date?>" />
<input type=hidden name=PPSFileType value="<?=$PPSFileType?>" />
<input type="hidden" name="FileHashValue" value="<?=$file_hash_value?>" />
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>