<?php
// Editing by 
/*
 * 2016-08-26 (Carlos): import with userlogin do not need class name and class number. 
 * 2015-10-27 (Carlos): allow link staff account to PPS number for userlogin approach.
 * 2013-09-17 (Carlos): add fields DateModified and ModifyBy to PAYMENT_ACCOUNT for tracing purpose
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$limport = new libimporttext();

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "AccountDataImport_PPSLinkage";

define("ERROR_CODE_NO_MATCH_STUDENT",1);
define("ERROR_CODE_CRASHED_PPS_NO",2);
define("ERROR_CODE_MISSING_PPS_NO",3);
define("ERROR_CODE_ALREADY_HAS_PPS_NO",4);

$li = new libdb();
$lf = new libfilesystem();
$filepath = $_FILES["userfile"]["tmp_name"];
$filename = $_FILES["userfile"]["name"];

if($filepath=="none" || $filepath == "" || !$limport->CHECK_FILE_EXT($filename)){          # import failed
	header("Location: pps_account.php?failed=2");
} else {
	if ($format == 1){
		$format_array = array("ClassName","ClassNumber","PPS Account");
		$flagAry = array(1, 1, 0, 1);
	}else{
		$format_array = array("User Login","PPS Account");	
		$flagAry = array(1, 1, 0);
	}
  $ext = strtoupper($lf->file_ext($filename));
  if($limport->CHECK_FILE_EXT($filename)) {
    # read file into array
    # return 0 if fail, return csv array if success
    //$data = $lf->file_read_csv($filepath);
	#20130902 Siuwan Get data from csv with reference
   /*  $data = $limport->GET_IMPORT_TXT($filepath);
		array_shift($data);                   # drop the title bar    
   */
    $data = $limport->GET_IMPORT_TXT_WITH_REFERENCE($filepath, "", "", $format_array, $flagAry);
	$header_row = array_shift($data); 
         
  }
  else
  {
    header("Location: pps_account.php?failed=2");
    exit();
  }

  # Get Existing PPS Account
  $sql = "SELECT DISTINCT PPSAccountNo FROM PAYMENT_ACCOUNT WHERE PPSAccountNo IS NOT NULL
          AND PPSAccountNo != ''";
  $current_pps = $li->returnVector($sql);

  # Get Existing Students and Staff
  if($format==1){
  	  $sql = "SELECT ClassName, ClassNumber, UserID,UserLogin
	          FROM INTRANET_USER
	          WHERE RecordStatus = 1 AND RecordType=2 AND ClassName != '' AND ClassNumber != '' 
	          ORDER BY ClassName, ClassNumber";
  }else{
	  $sql = "SELECT ClassName, ClassNumber, UserID,UserLogin
	          FROM INTRANET_USER
	          WHERE RecordStatus = 1
	               AND (RecordType=2 OR RecordType=1) 
	          ORDER BY ClassName, ClassNumber";
  }

  $result = $li->returnArray($sql,4);
  
  for ($i=0; $i<sizeof($result); $i++)
  {
		list($class,$classnum,$uid,$userlogin) = $result[$i];
		if($format==1){
			$students[$class][$classnum]=$uid;
		}
		else if($format==2){
			$students[$userlogin] = $uid;
		}
	}

  # Counts
  $total_row=0;
  $count_successful = 0;
  $count_no_match_student = 0;
  $count_crashed_pps_no = 0;
  $count_student_has_pps_already = 0;
  $count_pps_no_missing = 0;
  $array_error = array();

  # Update PAYMENT_ACCOUNT
  for ($i=0; $i<sizeof($data); $i++)
  {
    if(trim(implode("",$data[$i]))=="") continue;
    
    $total_row++;
    
    if($format==1){ # Format 1
			list($class,$classnum,$pps) = $data[$i];
			
			if ($class == "" && $classnum == "")
			{
			   $count_no_match_student++;
			   $array_error[] = array($i,ERROR_CODE_NO_MATCH_STUDENT);
			   continue;
			}
			
			$targetUID = $students[$class][$classnum];
 		}
    else if($format==2){ # Format 2
			list($u_login,$pps) = $data[$i];
			if ($u_login == "")
			{
			 $count_no_match_student++;
			 $array_error[] = array($i,ERROR_CODE_NO_MATCH_STUDENT);
			 continue;
			}
			
			$targetUID = $students[$u_login];
  	}


		if ($targetUID == "")
		{
		   $count_no_match_student++;
		   $array_error[] = array($i,ERROR_CODE_NO_MATCH_STUDENT);
		   continue;
		}
		
		if ($pps != "")
		{
		   if (in_array($pps,$current_pps,true))
		   {
		       $count_crashed_pps_no++;
		       $array_error[] = array($i,ERROR_CODE_CRASHED_PPS_NO);
		       continue;
		   }
		   else $current_pps[] = $pps;
		}
		else
		{
		   $count_pps_no_missing++;
		   $array_error[] = array($i,ERROR_CODE_MISSING_PPS_NO);
		   continue;
		}
		
		$sql = "INSERT INTO PAYMENT_ACCOUNT (StudentID,PPSAccountNo,Balance,DateModified,ModifyBy) VALUES
		       ($targetUID,'$pps',0,NOW(),'".$_SESSION['UserID']."')";
		$li->db_db_query($sql);
		if ($li->db_affected_rows() != 1)
		{
		   $sql = "UPDATE PAYMENT_ACCOUNT SET PPSAccountNo = '$pps',DateModified=NOW(),ModifyBy='".$_SESSION['UserID']."' WHERE StudentID = $targetUID AND (PPSAccountNo IS NULL OR PPSAccountNo = '')";
		   $li->db_db_query($sql);
		   /* For not update */
		   if ($li->db_affected_rows() != 1)
		   {
		       $array_error[] = array($i,ERROR_CODE_ALREADY_HAS_PPS_NO);
		       $count_student_has_pps_already++;
		   }
		   else
		   {
		       $count_successful++;
		   }
		
		}
		else
		{
		   $count_successful++;
		}
	}
}

$display = "<tr>
				<td class=\"formfieldtitle tabletext\">$i_Payment_Import_PPSLink_Result_Summary_TotalRows</td>
				<td class=\"tabletext\" width=\"70%\">".$total_row."</td>
			</tr>
			<tr>
				<td class=\"formfieldtitle tabletext\">$i_Payment_Import_PPSLink_Result_Summary_Successful</td>
				<td class=\"tabletext\" width=\"70%\">$count_successful</td>
			</tr>
			<tr>
				<td class=\"formfieldtitle tabletext\">$i_Payment_Import_PPSLink_Result_Summary_NoMatch</td>
				<td class=\"tabletext\" width=\"70%\">$count_no_match_student</td>
			</tr>
			<tr>
				<td class=\"formfieldtitle tabletext\">$i_Payment_Import_PPSLink_Result_Summary_Occupied</td>
				<td class=\"tabletext\" width=\"70%\">$count_crashed_pps_no</td>
			</tr>
			<tr>
				<td class=\"formfieldtitle tabletext\">$i_Payment_Import_PPSLink_Result_Summary_Missing</td>
				<td class=\"tabletext\" width=\"70%\">$count_pps_no_missing</td>
			</tr>
			<tr>
				<td class=\"formfieldtitle tabletext\">$i_Payment_Import_PPSLink_Result_Summary_ExistAlready</td>
				<td class=\"tabletext\" width=\"70%\">$count_student_has_pps_already</td>
			</tr>\n";

$linterface = new interface_html();
$TAGS_OBJ[] = array($i_Payment_Menu_Import_PPSLink, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_import);
$PAGE_NAVIGATION2 = $i_Payment_Import_PPSLink_Result;
$PAGE_NAVIGATION3 = $i_Payment_Import_PPSLink_Result_ErrorMsg_NotSuccessful;
?>
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
    	<td>
    		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
			    <tr>
					<td colspan="2"><?= $linterface->GET_NAVIGATION2($PAGE_NAVIGATION2) ?></td>
				</tr>
				<?=$display?>
			</table>
		</td>
	</tr>
	<? if (sizeof($array_error) != 0) { ?>
	<tr>
		<td>
    		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
			    <tr>
					<td colspan="2"><?= $linterface->GET_NAVIGATION2($PAGE_NAVIGATION3) ?></td>
				</tr>
				<tr class="tabletop">
					<td class="tabletoplink">#</td>
					<td class="tabletoplink"><?=$i_Payment_Import_PPSLink_Result_ErrorMsg_Reason?></td>
				</tr>
				
	<?
	for ($i=0; $i < sizeof($array_error); $i++) {
		list ($t_row, $t_type) = $array_error[$i];
		$t_row++;
		$css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
		switch ($t_type)
		{
			case ERROR_CODE_NO_MATCH_STUDENT:
				$t_string_error = $i_Payment_Import_PPSLink_Result_ErrorMsg_NoMatch; break;
			case ERROR_CODE_CRASHED_PPS_NO:
				$t_string_error = $i_Payment_Import_PPSLink_Result_ErrorMsg_Occupied; break;
			case ERROR_CODE_MISSING_PPS_NO:
				$t_string_error = $i_Payment_Import_PPSLink_Result_ErrorMsg_Missing; break;
			case ERROR_CODE_ALREADY_HAS_PPS_NO:
				$t_string_error = $i_Payment_Import_PPSLink_Result_ErrorMsg_ExistAlready; break;
			default:
				$t_string_error = "Unknown Error";
		}
	?>
			<tr class="<?=$css?>">
				<td class="tabletext"><?=$t_row?></td>
				<td class="tabletext"><?=$t_string_error?></td>
			</tr>
	<? } ?>
			</table>
		</td>
	</tr>
	<? } ?>
	<tr>
    	<td colspan="2">
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
	<tr>
	    <td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($i_Payment_Import_PPSLink_Result_GoToIndex, "button", "window.location = 'pps_account_list.php'") ?>
		</td>
	</tr>
</table>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>