<?php
// Editing by 
/*
 * 2019-09-24 (Ray):    Added PPSData value into PAYMENT_CREDIT_TRANSACTION & PAYMENT_MISSED_PPS_BILL
 * 2018-09-28 (Carlos): Improved import records to follow current session to avoid other people clear the temp records. Use db transaction.
 * 2018-03-19 (Carlos): $sys_custom['ePayment']['PPSFileLogCheckFileHash'] Marked file hash value to allow more than one pps files in the same day can be imported.
 * 2015-12-29 (Carlos): $sys_custom['ePayment']['AllowNegativeBalance'] - do not check enough to pay PPS charge fee. 
 * 2015-07-06 (Carlos): Added checking on the negative balance problem after deduct PPS charge fee. 
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

// trace log
if($sys_custom['ePayment_DebugPPSFileLog']){
	if (!is_dir("$intranet_root/file/paymentDebugLog")) {
		mkdir("$intranet_root/file/paymentDebugLog",0777);
	}
	
	$logFile = "$intranet_root/file/paymentDebugLog/pps_log_update.txt";
	$fh = fopen($logFile, "a");
	if($fh){
		$logContent = "Date: ". date("Y-m-d H:i:s")."\n";
		$logContent .= "By UserID: ".$_SESSION['UserID']."\n";
		$logContent .= "From url: ".$_SERVER['HTTP_REFERER']."\n";
		if(count($_POST)>0){
			foreach($_POST as $k => $v){
				$logContent .= "POST[$k] = $v\n";
			}
		}
		if(count($_GET)>0){
			foreach($_GET as $k => $v){
				$logContent .= "GET[$k] = $v\n";
			}
		}
		if(count($_POST)==0 && count($_GET)==0 && count($_REQUEST)>0){
			foreach($_REQUEST as $k => $v){
				$logContent .= "REQUEST[$k] = $v\n";
			}
		}
		$logContent .= "\n";
		
		fwrite($fh,$logContent);
		fclose($fh);
	}
}

if(!strpos($_SERVER['HTTP_REFERER'],'/home/eAdmin/GeneralMgmt/payment/import/pps_log_confirm.php')){
	header("Location: pps_log.php?no_trans=1");
	exit;
}

intranet_auth();
intranet_opendb();


if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$li = new libdb();
$lpayment = new libpayment();

$Settings = $lpayment->Retrieve_Setting();
$pps_auto_charge = $Settings['PPSChargeEnabled'];

if ($PPSFileType==1) # Counter bill
{
    $handling_charge = $Settings['PPSCounterBillCharge'];
}
else
{
    $handling_charge = $Settings['PPSIEPSCharge'];
}

$cur_sessionid = md5(session_id());
$cur_time = date("Y-m-d H:i:s");
$threshold_date = date("Y-m-d H:i:s", strtotime("-1 day"));

$trans_count =0; // counting no. of transactions being processed

if ($confirm == 1)
{
	$result_ary = array();
	$li->Start_Trans();
	
    if ($FileDate != "")
    {
        $PPSFileType += 0;
        if($sys_custom['ePayment']['PPSFileLogCheckFileHash']){
        	$more_fields = ",FileHashValue ";
        	$more_values = ",'$FileHashValue' ";
        }
        $sql = "INSERT INTO PAYMENT_CREDIT_FILE_LOG (FileDate, FileType $more_fields) VALUES ('$FileDate','$PPSFileType' $more_values)";
        $success = $li->db_db_query($sql);
        $result_ary[] = $success;
        if (!$success)
        {
        	 $li->RollBack_Trans();
        	 intranet_closedb();
             header("Location: pps_log.php?dup=1");
             exit();
        }
    }
    else
    {
    	intranet_closedb();
        header("Location: pps_log.php?dup=1");
        exit();
    }
    


    $sql = "LOCK TABLES
                 PAYMENT_CREDIT_TRANSACTION WRITE
                 ,TEMP_PPS_LOG WRITE
                 ,TEMP_PPS_LOG as a WRITE
                 ,PAYMENT_ACCOUNT as b WRITE
                 ,PAYMENT_ACCOUNT WRITE
                 ,PAYMENT_OVERALL_TRANSACTION_LOG WRITE
                 ,PAYMENT_MISSED_PPS_BILL WRITE";
    $li->db_db_query($sql);

	# check whether enough balance to deduct PPS charge after deposit. 
	# extreme case would be original balance is $0, deposit money is ot enough for paying the PPS charge, the final balance would be negative and no outstanding payment records for such case 
	if($pps_auto_charge && !$sys_custom['ePayment']['AllowNegativeBalance'])
	{
		$userIdToBalance = array();
		$sql = "SELECT a.PPSAccount,a.Amount,a.InputTime,b.Balance,b.StudentID,a.PPSData FROM TEMP_PPS_LOG as a INNER JOIN PAYMENT_ACCOUNT as b ON a.PPSAccount = b.PPSAccountNo WHERE a.SessionID='$cur_sessionid' ORDER BY a.InputTime";
		$records = $li->returnResultSet($sql);
		$record_count = count($records);
		$neg_values = "";
		$t_delim = "";
		for($i=0;$i<$record_count;$i++)
		{
			$t_ppsaccount = $records[$i]['PPSAccount']; 
			$t_userid = $records[$i]['StudentID'];
			$t_amount = sprintf("%.2f",$records[$i]['Amount']);
			$t_inputtime = $records[$i]['InputTime'];
			$t_balance = $records[$i]['Balance'];
			$t_ppsdata = $records[$i]['PPSData'];

			if(!isset($userIdToBalance[$t_userid])){
				$userIdToBalance[$t_userid] = $t_balance;
			}
			
			$userIdToBalance[$t_userid] += $t_amount - $handling_charge;
			if($userIdToBalance[$t_userid] < 0){ // negative!!
				$neg_values .= $t_delim."('$t_ppsaccount','$t_amount','$PPSFileType','$t_inputtime','$t_ppsdata')";
				$t_delim = ",";
				# remove the import record that would make negative balance
				$sql_delete = "DELETE FROM TEMP_PPS_LOG WHERE SessionID='$cur_sessionid' AND PPSAccount='$t_ppsaccount' AND ROUND(Amount,2)='$t_amount' AND InputTime='$t_inputtime'";
				$li->db_db_query($sql_delete);
			}
		}
		# add to missed PPS records 
		if($neg_values != "")
		{
			$sql = "INSERT INTO PAYMENT_MISSED_PPS_BILL (PPSAccount, Amount, PPSType, InputTime, PPSData) VALUES ".$neg_values;
			$result_ary[] = $li->db_db_query($sql);
			$missed = "&missed=1";
		}
	}

    # Insert to Credit transaction table
    # RecordStatus = 1 means in progress
    $sql = "INSERT IGNORE INTO PAYMENT_CREDIT_TRANSACTION (
    					StudentID, 
    					Amount,
    					RecordType ,
    					RecordStatus,
    					PPSAccountNo, 
    					TransactionTime, 
    					PPSType,
    					AdminInCharge,
    					PPSData
    					)
            SELECT 
            	b.StudentID, 
            	a.Amount,
            	1, 
            	1, 
            	a.PPSAccount,
            	a.InputTime, 
            	'$PPSFileType',
            	'".$_SESSION['UserID']."',
            	a.PPSData 
            FROM 
            	TEMP_PPS_LOG as a 
            	LEFT OUTER JOIN 
            	PAYMENT_ACCOUNT as b 
            	ON a.PPSAccount = b.PPSAccountNo 
            WHERE a.SessionID='$cur_sessionid' AND b.StudentID IS NOT NULL";
    //debug_r($sql);
    $result_ary[] = $li->db_db_query($sql);

    # Insert missed link
    $sql = "INSERT INTO PAYMENT_MISSED_PPS_BILL (PPSAccount, Amount, InputTime, PPSType, PPSData)
               SELECT  a.PPSAccount,a.Amount,a.InputTime, '$PPSFileType', a.PPSData
                      FROM TEMP_PPS_LOG as a LEFT OUTER JOIN PAYMENT_ACCOUNT as b ON a.PPSAccount = b.PPSAccountNo
               WHERE b.StudentID IS NULL";
    $result_ary[] = $li->db_db_query($sql);

    # Update Ref Code and Admin Name
    $sql = "UPDATE PAYMENT_CREDIT_TRANSACTION SET
               RefCode = CONCAT('PPS',TransactionID) 
               WHERE RecordStatus = 1 AND RecordType = 1";
    //debug_r($sql);
    $result_ary[] = $li->db_db_query($sql);


    $sql = "SELECT 
    					TransactionID, 
    					StudentID, 
    					Amount, 
    					DATE_FORMAT(TransactionTime,'%Y-%m-%d %H:%i'),
    					RefCode
            FROM 
            	PAYMENT_CREDIT_TRANSACTION 
            WHERE 
            	RecordStatus = 1 
            	AND RecordType = 1 
            ORDER BY TransactionTime";
    $transactions = $li->returnArray($sql,5);
    
    $trans_count = sizeof($transactions);

    $values = "";
    $delim = "";
    # For pps charge input
    $values2 = "";
    $delim2 = "";
    for ($i=0; $i<sizeof($transactions); $i++)
    {
         list($tid, $sid, $amount, $tran_time, $refCode) = $transactions[$i];
         $sql = "SELECT Balance FROM PAYMENT_ACCOUNT WHERE StudentID = $sid";
         //debug_r($sql);
         $temp = $li->returnVector($sql);
         //debug_r($temp);
         if ($pps_auto_charge)
         {
             $balanceAfter = $temp[0]+$amount;
             $values .= "$delim ('$sid',1,'$amount','$tid','$balanceAfter',now(),'$i_Payment_TransactionDetailPPS','$refCode')";
             $delim = ",";

             $sql = "UPDATE PAYMENT_ACCOUNT SET Balance = Balance + $amount - $handling_charge, LastUpdateByAdmin = '".$_SESSION['UserID']."', LastUpdateByTerminal= '', LastUpdated=now() WHERE StudentID = '$sid'";
             //debug_r($sql);
             $result_ary[] = $li->db_db_query($sql);

             $balanceAfter2 = $balanceAfter - $handling_charge;
             $values2 .= "$delim2 ('$sid',8,'$handling_charge','$tid','$balanceAfter2',now(),'$i_Payment_PPS_Charge','')";
             $delim2 = ",";

         }
         else
         {
             $sql = "UPDATE PAYMENT_ACCOUNT SET Balance = Balance + $amount, LastUpdateByAdmin = '".$_SESSION['UserID']."', LastUpdateByTerminal= '', LastUpdated=now() WHERE StudentID = '$sid'";
             //debug_r($sql);
             $result_ary[] = $li->db_db_query($sql);
             $balanceAfter = $temp[0]+$amount;
             # Change transaction time on Overall Transaction to now()
             #$values .= "$delim ('$sid',1,'$amount','$tid','$balanceAfter','$tran_time','$i_Payment_TransactionDetailPPS','$refCode')";
             $values .= "$delim ('$sid',1,'$amount','$tid','$balanceAfter',now(),'$i_Payment_TransactionDetailPPS','$refCode')";
             $delim = ",";
         }
    }
    $sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
                   (StudentID,TransactionType,Amount,RelatedTransactionID,BalanceAfter,
                   TransactionTime,Details,RefCode) VALUES $values";
		//debug_r($sql);
    $result_ary[] = $li->db_db_query($sql);

    if ($pps_auto_charge)
    {
        $sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
                   (StudentID,TransactionType,Amount,RelatedTransactionID,BalanceAfter,
                   TransactionTime,Details,RefCode) VALUES $values2";
        //debug_r($sql);
        $result_ary[] = $li->db_db_query($sql);
    }


    # Update in progress to finished status
    $sql = "UPDATE PAYMENT_CREDIT_TRANSACTION SET
               RecordStatus = 0,
               DateInput = now() WHERE RecordStatus = 1 AND RecordType = 1 ";
    $result_ary[] = $li->db_db_query($sql);

    $sql = "DELETE FROM TEMP_PPS_LOG WHERE SessionID='$cur_sessionid' OR ImportTime<'$threshold_date'";
    $li->db_db_query($sql);
	
    $sql = "UNLOCK TABLES";
    $li->db_db_query($sql);
    
    if(!in_array(false,$result_ary) && $trans_count>0){
    	$msg = 2;
    	$li->Commit_Trans();
    }else{
    	$li->RollBack_Trans();
    }
}
else
{
    $msg = "";
}
if($trans_count<=0){
	$no_trans="&no_trans=1";
}

//die;
intranet_closedb();
header("Location: pps_log.php?msg=$msg".$no_trans.$missed);
?>