<?php
// Editing by
/*
 * 2019-07-25 (Ray): created
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();

$FromDate = (empty($_POST['FromDate']) ? $_POST['date_from'] : $_POST['FromDate']);
$ToDate = (empty($_POST['ToDate']) ? $_POST['date_to'] : $_POST['ToDate']);

$PaymentNoticeID = $_POST['PaymentNoticeID'];

if($sys_custom['ePayment']['PaymentItemDateRange']){
	$date_conds = " AND (a.EndDate between '$FromDate' AND '$ToDate') ";
}else{
	$date_conds =" AND (
									(a.StartDate between '$FromDate' AND '$ToDate' 
									 or 
									 a.EndDate between '$FromDate' AND '$ToDate' )) ";
}

$sql = "SELECT b.NoticeID, b.Title FROM PAYMENT_PAYMENT_ITEM as a
LEFT JOIN INTRANET_NOTICE as b ON a.NoticeID=b.NoticeID
WHERE a.NoticeID IS NOT NULL
$date_conds
AND b.RecordStatus=1
GROUP BY b.NoticeID ORDER BY b.Title ASC";


$rs = $lpayment->returnArray($sql);
$x = '<option value="">'.$Lang['General']['All'].'</option>';
if(!empty($rs)) {
	foreach($rs as $temp) {
		$x .= '<option value="' . $temp['NoticeID'] . '" '.(($PaymentNoticeID == $temp['NoticeID']) ? 'SELECTED' : '').'>' . intranet_htmlspecialchars($temp['Title']) . '</option>';
	}
}

echo $x;

intranet_closedb();
?>