<?php
// editing by 
########################################### Change Log ###################################################
#
# 2010-02-10 by Carlos: Add border line to table
#
##########################################################################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."templates/fileheader.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_opendb();

$lpayment = new libpayment();

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$Keyword = trim(stripslashes(urldecode($_REQUEST['keyword'])));

# date range
$today_ts = strtotime(date('Y-m-d'));
if($FromDate=="")
        $FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
if($ToDate=="")
        $ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

$date_cond = " AND a.DateInput between '$FromDate' and '$ToDate 23:59:59' ";

$sql  = "SELECT
				b.ItemName,
				SUM(ItemQty) as TotalSold,
				ROUND(SUM(ItemSubTotal*ItemQty),2) as TotalPrice 
			from
			  PAYMENT_PURCHASE_DETAIL_RECORD a
			  INNER JOIN POS_ITEM b ON a.ItemID = b.ItemID
         WHERE
						 b.ItemName LIKE '%$keyword%'   
						$date_cond
				Group By
					b.ItemName
					";
//echo $sql; die;
$Result = $lpayment->returnArray($sql);

$x = "<table width=95% border=0  cellpadding=2 cellspacing=0 align='center' class='$css_table'>";
$x .= '<tr>';
$x .= '<td><b>'.$Lang['Payment']['Keyword'].':</b></td>';
$x .= '<td><b>'.$Keyword.'</b></td>';
$x .= '</tr>';
$x .= '</table>';
$x .= '<hr>';
$x .= "<table width=90% border=0  cellpadding=1 cellspacing=0 align='center' class='eSporttableborder'>";
$x .= "<tr class='eSporttdborder eSportprinttabletitle'>";
$x .= "<td width=50% class='eSporttdborder eSportprinttabletitle'><b>".$Lang['ePayment']['ItemName']."</b></td>\n";
$x .= "<td width=25% class='eSporttdborder eSportprinttabletitle'><b>".$Lang['ePayment']['Quantity']."</b></td>\n";
$x .= "<td width=25% class='eSporttdborder eSportprinttabletitle'><b>".$Lang['ePayment']['GrandTotal']."</b></td>\n";
$x .= "</tr>";

for ($i=0; $i< sizeof($Result); $i++) {
	$x .= '<tr>';
	$x .= '<td class=\'eSporttdborder eSportprinttext\'>';
	$x .= $Result[$i]['ItemName'];
	$x .= '</td>';
	$x .= '<td class=\'eSporttdborder eSportprinttext\'>';
	$x .= $Result[$i]['TotalSold'];
	$x .= '</td>';
	$x .= '<td class=\'eSporttdborder eSportprinttext\'>';
	$x .= $Result[$i]['TotalPrice'];
	$x .= '</td>';
	$x .= '</tr>';
}
$x.="</table>";

//include_once($PATH_WRT_ROOT."templates/fileheader.php");
//echo displayNavTitle($i_Payment_Menu_Report_SchoolAccount,'');
?>
<table border=0 width=95% cellpadding=2 align=center>
<tr><td class='<?=$css_title?>'><b><?=$Lang['ePayment']['POSItemReport']?> (<?="$FromDate $i_Profile_To $ToDate"?>)</b></td></tr>
</table>
<?=$x?>
<br>
<table width=90% border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td align=right class='<?=$css_text?>'>
<?="$i_general_report_creation_time : ".date('Y-m-d H:i:s')?>
</td></tr>
</table>
<BR><BR>
<?php
//include_once($PATH_WRT_ROOT."templates/filefooter.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>
