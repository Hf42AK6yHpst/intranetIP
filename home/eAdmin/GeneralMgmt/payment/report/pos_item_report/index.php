<?php
// 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$lpayment = new libpayment();

$linterface = new interface_html();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "StatisticsReport_POSItem";

$TAGS_OBJ[] = array($Lang['ePayment']['POSItemReport'], "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$ClassName = trim(stripslashes(urldecode($_REQUEST['ClassName'])));
$Keyword = trim(stripslashes(urldecode($_REQUEST['keyword'])));

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
if ($field == ""){
	 $field = 8;
}

# date range
$today_ts = strtotime(date('Y-m-d'));
if($FromDate=="")
  $FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
if($ToDate=="")
  $ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

$date_cond = " AND a.DateInput between '$FromDate' and '$ToDate 23:59:59' ";

$sql  = "SELECT
				CONCAT('<a href=\"#\" onclick=\"Get_Item_Detail(this);\" return false;\" class=\"tablelink\">',b.ItemName,'</a>') as ItemName,
				SUM(ItemQty) as TotalSold,
				ROUND(SUM(ItemSubTotal*ItemQty),2) as TotalPrice 
			from
			  PAYMENT_PURCHASE_DETAIL_RECORD a
			  INNER JOIN POS_ITEM b ON a.ItemID = b.ItemID
         WHERE
						 b.ItemName LIKE '%$keyword%'   
						$date_cond
				Group By
					b.ItemName
					";
					
                //echo $sql; //die;
# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("ItemName","TotalSold","TotalPrice");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0);
$li->IsColOff = 2;


// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=55% class=tableTitle>".$li->column($pos++, $Lang['ePayment']['ItemName'])."</td>\n";
$li->column_list .= "<td width=35% class=tableTitle>".$li->column($pos++, $Lang['ePayment']['Quantity'])."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $Lang['ePayment']['GrandTotal'])."</td>\n";

/*$toolbar = "<a class=iconLink href=javascript:openPrintPage()>".printIcon()."$i_PrinterFriendlyPage</a>";
$toolbar2 = "<a class=iconLink href=javascript:exportPage(document.form1,'export.php?FromDate=$FromDate&ToDate=$ToDate')>".exportIcon()."$button_export</a>";
//$toolbar = "<a class=iconLink href=javascript:exportPage(document.form1,'export.php?FromDate=$FromDate&ToDate=$ToDate')>".exportIcon()."$button_export</a>";
//$toolbar .= "<a class=iconLink href=javascript:checkGet(document.form1,'pps_index.php')>".exportIcon()."$button_export_pps</a>";
#$functionbar .= "<a href=\"javascript:checkEdit(document.form1,'TerminalUserID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
#$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'TerminalUserID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";

$searchbar .= $select_user;
$searchbar .= '<span id="ClassLayer" '.$ClassDisplay.'>'.$select_class.'</span>';
$searchbar .= '<span id="StudentLayer" '.$StudentDisplay.'>'.$select_student.'</span>';
$searchbar .= "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";*/

$toolbar = $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
$toolbar2 = $linterface->GET_LNK_EXPORT("javascript:exportPage(document.form1,'export.php?FromDate=$FromDate&ToDate=$ToDate')","","","","",0);

$searchbar .= "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= $linterface->GET_SMALL_BTN($button_search, "button", "document.form1.submit();","submit2");
?>
<script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script language="javascript">
// Calendar callback. When a date is clicked on the calendar
// this function is called so you can do as you want with it
function calendarCallback(date, month, year)
{
	if (String(month).length == 1) {
		month = '0' + month;
	}
	
	if (String(date).length == 1) {
		date = '0' + date;
	}
	dateValue =year + '-' + month + '-' + date;
	document.forms['form1'].FromDate.value = dateValue;

}
function calendarCallback2(date, month, year)
{
	if (String(month).length == 1) {
		month = '0' + month;
	}
	
	if (String(date).length == 1) {
		date = '0' + date;
	}
	dateValue =year + '-' + month + '-' + date;
	document.forms['form1'].ToDate.value = dateValue;

}
function checkForm(formObj){
	if(formObj==null)return false;
	
	fromV = formObj.FromDate;
	toV= formObj.ToDate;
	if(!checkDate(fromV)){
	//formObj.FromDate.focus();
		return false;
	}
	else if(!checkDate(toV)){
		//formObj.ToDate.focus();
		return false;
	}
	return true;
}
function checkDate(obj){
	if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
	return true;
}
function submitForm(obj){
	if(checkForm(obj))
	obj.submit();
}
function exportPage(obj,url){
	old_url = obj.action;
	old_method= obj.method;
	obj.action=url;
	obj.method = "get";
	obj.submit();
	obj.action = old_url;
	obj.method = old_method;
}

function GetXmlHttpObject()
{
  var xmlHttp=null;
  try
  {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
  }
  catch (e)
  {
    // Internet Explorer
    try
    {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch (e)
    {
      xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
  }
  
  return xmlHttp;
}

function Show_Class_Layer(UserType) {
	if (UserType != 1) 
		document.getElementById("ClassLayer").style.display = '';
	else 
		document.getElementById("ClassLayer").style.display = 'none';
}

function Show_Student_Layer(ClassName) {
	if (Trim(ClassName) != "") {
		wordXmlHttp = GetXmlHttpObject();
	   
	  if (wordXmlHttp == null)
	  {
	    alert (errAjax);
	    return;
	  } 
	    
	  var url = 'ajax_get_student_selection.php';
	  var postContent = 'ClassName='+encodeURIComponent(ClassName);
		wordXmlHttp.onreadystatechange = function() {
			if (wordXmlHttp.readyState == 4) {
				ResponseText = Trim(wordXmlHttp.responseText);
			  document.getElementById("StudentLayer").innerHTML = ResponseText;
			  document.getElementById("StudentLayer").style.display = "";
			}
		};
	  wordXmlHttp.open("POST", url, true);
		wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		wordXmlHttp.send(postContent);
	}
	else {
		document.getElementById("StudentLayer").innerHTML = "";
		document.getElementById("StudentLayer").style.display = "none";
	}
}

function openPrintPage(){
	var FormObj = document.getElementById('form1');
	
	old_url = FormObj.action;
	old_target = FormObj.target;
  FormObj.action="print.php";
  FormObj.target="_blank";
  FormObj.submit();
  FormObj.action = old_url;
  FormObj.target= old_target;
}

function exportPage(obj,url){
  old_url = obj.action;
  obj.action=url;
  obj.submit();
  obj.action = old_url;
}

function Get_Item_Detail(Obj) {
	ItemName = Obj.innerHTML;
	document.getElementById("ItemName").value = ItemName;
	
	var FormObj = document.getElementById('form1');
	
	old_url = FormObj.action;
	old_target = FormObj.target;
  FormObj.action='detail.php';
  FormObj.target="_blank";
  FormObj.method="POST";
  FormObj.submit();
  
  FormObj.method="GET";
  FormObj.action = old_url;
  FormObj.target= old_target;
}
</script>
<form name="form1" id="form1" method="get" action='index.php'>
<input type="hidden" name="ItemName" id="ItemName" value="">
<!-- date range -->
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_Payment_Menu_Browse_CreditTransaction_DateRange ?></td>
		<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
			<?=$linterface->GET_DATE_PICKER("FromDate",$FromDate)?>
			<span class="tabletextremark">(yyyy-mm-dd)</span>
			<?=$i_Profile_To?>  
			<?=$linterface->GET_DATE_PICKER("ToDate",$ToDate)?>
			<span class="tabletextremark">(yyyy-mm-dd)</span>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	</tr>
	<tr>
		<td colspan="2" class="tabletext" align="center">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "","submit2") ?>
		</td>
	</tr>
</table>

<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?> <?=$toolbar2?></td>
	</tr>
	<tr>
		<td align="left"><?=$searchbar ?></td>
	</tr>
</table>
<?php echo $li->display(); ?>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>
<?
//include_once($PATH_WRT_ROOT."templates/adminfooter.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
