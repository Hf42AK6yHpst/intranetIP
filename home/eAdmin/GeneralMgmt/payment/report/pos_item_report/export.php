<?php
// kenneth chung
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

$lpayment = new libpayment();
$lexport = new libexporttext();

$Keyword = trim(stripslashes(urldecode($_REQUEST['keyword'])));

# date range
$today_ts = strtotime(date('Y-m-d'));
if($FromDate=="")
        $FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
if($ToDate=="")
        $ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

$date_cond = " AND DateInput between '$FromDate' and '$ToDate 23:59:59' ";

$sql  = "SELECT
						ItemName,
						SUM(ItemQty) as TotalSold,
						ROUND(SUM(ItemSubTotal*ItemQty),2) as TotalPrice 
					from
					  PAYMENT_PURCHASE_DETAIL_RECORD 
         WHERE
						 ItemName LIKE '%$keyword%'   
						$date_cond
				Group By
					ItemName
					";
$Result = $lpayment->returnArray($sql);

$UtfContent = $Lang['ePayment']['POSItemReport']." (".$FromDate." ".$i_Profile_To." ".$ToDate.")\r\n\r\n";
$UtfContent .= $i_ClassName."\t".(($ClassName != "")? $ClassName:$i_general_all)."\r\n";
$UtfContent .= $Lang['Payment']['Keyword']."\t".$Keyword."\r\n";
$UtfContent .= "\r\n";

$ExportColumn = array($Lang['ePayment']['ItemName'],$Lang['ePayment']['Quantity'],$Lang['ePayment']['GrandTotal']);

for ($i=0; $i< sizeof($Result); $i++) {
	unset($Detail);
	$Detail[] = $Result[$i]['ItemName'];
	$Detail[] = $Result[$i]['TotalSold'];
	$Detail[] = $lpayment->getExportAmountFormat($Result[$i]['TotalPrice']);
	
	$Rows[] = $Detail;
}

intranet_closedb();

$filename = "cancel_deposit_report.csv";
$ExportContent = $UtfContent.$lexport->GET_EXPORT_TXT($Rows,$ExportColumn);
$lexport->EXPORT_FILE($filename, $ExportContent);
?>
