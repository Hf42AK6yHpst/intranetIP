<?php
// Editing by 
/*
 * 2020-05-19 (Ray): add last modify name
 * 2015-08-06 (Carlos): Get/Set date range cookies.
 * 2015-03-02 (Carlos): Added Summary information.
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$lpayment = new libpayment();
$linterface = new interface_html();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "StatisticsReport_CancelDeposit";

$ClassName = trim(stripslashes(urldecode($_REQUEST['ClassName'])));
$Keyword = trim(stripslashes(urldecode($_REQUEST['keyword'])));

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
if ($field == ""){
	 $field = 8;
}
	 

$namefield = getNameFieldByLang("b.");
$input_namefield = getNameFieldByLang("input_b.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
        #$archive_namefield = " IF(c.ChineseName IS NULL,c.EnglishName,c.ChineseName)";
        $archive_namefield="c.ChineseName";
	$archive_input_namefield = "input_c.ChineseName";
}else {
	$archive_namefield = "c.EnglishName";
	$archive_input_namefield = "input_c.EnglishName";
}
# $archive_namefield = "IF(c.EnglishName IS NULL,c.ChineseName,c.EnglishName)";

# date range
$today_ts = strtotime(date('Y-m-d'));
$date_range_cookies = $lpayment->Get_Report_Date_Range_Cookies();
if($FromDate=="")
        $FromDate = $date_range_cookies['StartDate']!=''? $date_range_cookies['StartDate'] : date('Y-m-d',getStartOfAcademicYear($today_ts));
if($ToDate=="")
        $ToDate = $date_range_cookies['EndDate']!=''? $date_range_cookies['EndDate'] : date('Y-m-d',getEndOfAcademicYear($today_ts));

if($_REQUEST['FromDate'] != '' && $_REQUEST['ToDate']!=''){
	$lpayment->Set_Report_Date_Range_Cookies($_REQUEST['FromDate'], $_REQUEST['ToDate']);
}

$date_cond = " AND DATE_FORMAT(potl.TransactionTime,'%Y-%m-%d') >= '".$lpayment->Get_Safe_Sql_Query($FromDate)."' AND DATE_FORMAT(potl.TransactionTime,'%Y-%m-%d')<='".$lpayment->Get_Safe_Sql_Query($ToDate)."' ";

if (trim($ClassName) != "") 
	$user_cond .= " AND (b.ClassName like '%".$lpayment->Get_Safe_Sql_Like_Query($ClassName)."%') ";
if (trim($_REQUEST['UserID']) != "")
 	$user_cond .= " AND (b.UserID = '".$lpayment->Get_Safe_Sql_Query($_REQUEST['UserID'])."') ";

$escaped_keyword = $lpayment->Get_Safe_Sql_Like_Query($keyword);

$sql  = "SELECT
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), IF(b.UserID IS NULL AND c.UserID IS NULL,'<font color=red>*</font>',$namefield)) as Name,
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<i>',c.ClassName,'</i>'),IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassName)) as ClassName,
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<i>',c.ClassNumber,'</i>'),IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassNumber)) as ClassNumber,
               ".$lpayment->getWebDisplayAmountFormatDB("potl.Amount")." AS Amount,
               potl.RefCode, 
               DATE_FORMAT(potl.TransactionTime,'%Y-%m-%d %H:%i') as TransactionTime,
               IF( (input_b.UserID IS NULL AND input_c.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$archive_input_namefield,'</i>'), IF(input_b.UserID IS NULL AND input_c.UserID IS NULL,'<font color=red>*</font>',$input_namefield)) as InputName
              
         FROM
         		PAYMENT_OVERALL_TRANSACTION_LOG as potl
         		LEFT JOIN 
         		INTRANET_USER as b 
         		ON potl.StudentID = b.UserID 
            LEFT JOIN 
            INTRANET_ARCHIVE_USER AS c 
            ON potl.StudentID=c.UserID
            LEFT JOIN
            INTRANET_USER as input_b 
         		ON potl.InputBy = input_b.UserID 
         	LEFT JOIN 
            INTRANET_ARCHIVE_USER AS input_c 
            ON potl.InputBy=input_c.UserID
         WHERE
         		 potl.TransactionType=9 
         		 and 
         			(
               b.EnglishName LIKE '%$escaped_keyword%' OR
               b.ChineseName LIKE '%$escaped_keyword%' OR
               b.ClassName LIKE '%$escaped_keyword%' OR
               b.ClassNumber LIKE '%$escaped_keyword%' OR
               potl.RefCode LIKE '%$escaped_keyword%' OR
               c.EnglishName LIKE '%$escaped_keyword%' OR
               c.ChineseName LIKE '%$escaped_keyword%' OR
               c.ClassName LIKE '%$escaped_keyword%' OR
               c.ClassNumber LIKE '%$escaped_keyword%'
              )
             $date_cond
             $user_cond
                ";
# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("Name","ClassName","ClassNumber","Amount","RefCode","TransactionTime","InputName");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0,0);
$li->IsColOff = 2;


// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_Payment_Field_Username)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_UserClassName)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_UserClassNumber)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_Payment_Field_CreditAmount)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_Payment_Field_RefCode)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_Payment_Field_CancelDepositTime)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_general_last_modified_by)."</td>\n";

$sql_summary = "SELECT 
		            COUNT(*) as NumberOfTransaction,
					CONCAT('$',SUM(ROUND(potl.Amount,2))) as TotalAmount 
		         FROM
		         		PAYMENT_OVERALL_TRANSACTION_LOG as potl
		         		LEFT JOIN 
		         		INTRANET_USER as b 
		         		ON potl.StudentID = b.UserID 
		            LEFT JOIN 
		            INTRANET_ARCHIVE_USER AS c 
		            ON potl.StudentID=c.UserID
		         WHERE
		         		 potl.TransactionType=9 
		         		 and 
		         			(
		               b.EnglishName LIKE '%$escaped_keyword%' OR
		               b.ChineseName LIKE '%$escaped_keyword%' OR
		               b.ClassName LIKE '%$escaped_keyword%' OR
		               b.ClassNumber LIKE '%$escaped_keyword%' OR
		               potl.RefCode LIKE '%$escaped_keyword%' OR
		               c.EnglishName LIKE '%$escaped_keyword%' OR
		               c.ChineseName LIKE '%$escaped_keyword%' OR
		               c.ClassName LIKE '%$escaped_keyword%' OR
		               c.ClassNumber LIKE '%$escaped_keyword%'
		              )
		             $date_cond
		             $user_cond ";
$summary_record = $lpayment->returnResultSet($sql_summary);


$summary_info = "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION2($i_Payment_ItemSummary)."</td></tr>\n";
$summary_info .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Payment_TotalCountTransaction</td>\n";
$summary_info .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"70%\">".$summary_record[0]['NumberOfTransaction']."</td></tr>\n";
$summary_info .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Payment_ItemTotal</td>\n";
$summary_info .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">".$summary_record[0]['TotalAmount']."</td></tr>\n";


$lclass = new libclass();
$select_class = $lclass->getSelectClass("name=ClassName onChange=\"document.form1.submit();\"",''.$_REQUEST['ClassName'].'');

$toolbar = $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
$toolbar2 = $linterface->GET_LNK_EXPORT("javascript:exportPage(document.form1,'export.php?FromDate=".urlencode($FromDate)."&ToDate=".urlencode($ToDate)."')","","","","",0);

$searchbar .= $select_user;
$searchbar .= '<span id="ClassLayer" '.$ClassDisplay.'>'.$select_class.'</span>';
$searchbar .= '<span id="StudentLayer" '.$StudentDisplay.'>'.$select_student.'</span>';
$searchbar .= "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".escape_double_quotes(stripslashes($keyword))."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= $linterface->GET_SMALL_BTN($button_search, "button", "document.form1.submit();","submit2");

$TAGS_OBJ[] = array($Lang['Payment']['CancelDepositReport'], "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.css" type="text/css" />
<script language="javascript">
// Calendar callback. When a date is clicked on the calendar
// this function is called so you can do as you want with it

function checkForm(formObj){
	if(formObj==null)return false;
	
	fromV = formObj.FromDate;
	toV= formObj.ToDate;
	if(!checkDate(fromV)){
	//formObj.FromDate.focus();
		return false;
	}
	else if(!checkDate(toV)){
		//formObj.ToDate.focus();
		return false;
	}
	return true;
}
function checkDate(obj){
	if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
	return true;
}
function submitForm(obj){
	if(checkForm(obj))
	obj.submit();
}
function exportPage(obj,url){
	old_url = obj.action;
	old_method= obj.method;
	obj.action=url;
	obj.method = "get";
	obj.submit();
	obj.action = old_url;
	obj.method = old_method;
}

function GetXmlHttpObject()
{
  var xmlHttp=null;
  try
  {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
  }
  catch (e)
  {
    // Internet Explorer
    try
    {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch (e)
    {
      xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
  }
  
  return xmlHttp;
}

function Show_Class_Layer(UserType) {
	if (UserType != 1) 
		document.getElementById("ClassLayer").style.display = '';
	else 
		document.getElementById("ClassLayer").style.display = 'none';
}

function Show_Student_Layer(ClassName) {
	if (Trim(ClassName) != "") {
		wordXmlHttp = GetXmlHttpObject();
	   
	  if (wordXmlHttp == null)
	  {
	    alert (errAjax);
	    return;
	  } 
	    
	  var url = 'ajax_get_student_selection.php';
	  var postContent = 'ClassName='+encodeURIComponent(ClassName);
		wordXmlHttp.onreadystatechange = function() {
			if (wordXmlHttp.readyState == 4) {
				ResponseText = Trim(wordXmlHttp.responseText);
			  document.getElementById("StudentLayer").innerHTML = ResponseText;
			  document.getElementById("StudentLayer").style.display = "";
			}
		};
	  wordXmlHttp.open("POST", url, true);
		wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		wordXmlHttp.send(postContent);
	}
	else {
		document.getElementById("StudentLayer").innerHTML = "";
		document.getElementById("StudentLayer").style.display = "none";
	}
}

function openPrintPage(){
	var FormObj = document.getElementById('form1');
	
	old_url = FormObj.action;
	old_target = FormObj.target;
  FormObj.action="print.php";
  FormObj.target="_blank";
  FormObj.submit();
  FormObj.action = old_url;
  FormObj.target= old_target;
}

function exportPage(obj,url){
  old_url = obj.action;
  obj.action=url;
  obj.submit();
  obj.action = old_url;
}

$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
$(document).ready(function(){ 
	$('input#FromDate').datepick({
		dateFormat: 'yy-mm-dd',
		dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		changeFirstDay: false,
		firstDay: 0
		});
	$('input#ToDate').datepick({
		dateFormat: 'yy-mm-dd',
		dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		changeFirstDay: false,
		firstDay: 0
		});
});
</script>
<form name="form1" id="form1" method="get" action='index.php'>
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_Payment_Menu_Browse_CreditTransaction_DateRange ?></td>
		<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
			<input type=text name=FromDate id=FromDate value="<?=escape_double_quotes($FromDate)?>" size=12>
			<span class="tabletextremark">(yyyy-mm-dd)</span>
			<?=$i_Profile_To?>  
			<input type=text name=ToDate id=ToDate value="<?=escape_double_quotes($ToDate)?>" size=12>
			<span class="tabletextremark">(yyyy-mm-dd)</span>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	</tr>
	<tr>
		<td colspan="2" class="tabletext" align="center">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "document.form1.flag.value=1;","submit2") ?>
		</td>
	</tr>
</table>

<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<?=$summary_info?>
	<tr>
		<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	</tr>
</table>
<br />
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?> <?=$toolbar2?></td>
	</tr>
	<tr>
		<td align="left"><?=$searchbar ?></td>
	</tr>
</table>
<?= $li->display("96%") ?>
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
<tr><td><?= $i_Payment_Note_StudentRemoved ?></td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
