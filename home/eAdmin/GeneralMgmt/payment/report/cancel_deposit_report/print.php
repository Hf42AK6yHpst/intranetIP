<?php
// editing by 
########################################### Change Log ###################################################
# 2020-05-19 by Ray: add last modify name
# 2015-03-02 by Carlos: Added Summary information.
# 2015-02-26 by Carlos: $sys_custom['ePayment']['ReportWithSchoolName'] - display school name.
# 2010-02-10 by Carlos: Add border line to table
##########################################################################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$lpayment = new libpayment();
$linterface = new interface_html();
$ClassName = trim(stripslashes(urldecode($_REQUEST['ClassName'])));
$Keyword = trim(stripslashes(urldecode($_REQUEST['keyword'])));

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
if ($field == ""){
	 $field = 8;
}
	 

$namefield = getNameFieldByLang("b.");
$input_namefield = getNameFieldByLang("input_b.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
        #$archive_namefield = " IF(c.ChineseName IS NULL,c.EnglishName,c.ChineseName)";
        $archive_namefield="c.ChineseName";
	$archive_input_namefield = "input_c.ChineseName";
}else {
	$archive_namefield = "c.EnglishName";
	$archive_input_namefield = "input_c.EnglishName";
}

# $archive_namefield = "IF(c.EnglishName IS NULL,c.ChineseName,c.EnglishName)";

# date range
$today_ts = strtotime(date('Y-m-d'));
if($FromDate=="")
        $FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
if($ToDate=="")
        $ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

$date_cond = " AND DATE_FORMAT(potl.TransactionTime,'%Y-%m-%d') >= '$FromDate' AND DATE_FORMAT(potl.TransactionTime,'%Y-%m-%d')<='$ToDate' ";

if (trim($ClassName) != "") 
	$user_cond .= " AND (b.ClassName like '%".$ClassName."%') ";
if (trim($_REQUEST['UserID']) != "")
 	$user_cond .= " AND (b.UserID = '".$_REQUEST['UserID']."') ";

$sql  = "SELECT
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), IF(b.UserID IS NULL AND c.UserID IS NULL,'<font color=red>*</font>',$namefield)) as Name,
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<i>',c.ClassName,'</i>'),IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassName)) as ClassName,
               IF( (b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<i>',c.ClassNumber,'</i>'),IF(b.UserID IS NULL AND c.UserID IS NULL,'',b.ClassNumber)) as ClassNumber,
               ".$lpayment->getWebDisplayAmountFormatDB("potl.Amount")." AS Amount,
               potl.RefCode, 
               DATE_FORMAT(potl.TransactionTime,'%Y-%m-%d %H:%i') as TransactionTime,
               IF( (input_b.UserID IS NULL AND input_c.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$archive_input_namefield,'</i>'), IF(input_b.UserID IS NULL AND input_c.UserID IS NULL,'<font color=red>*</font>',$input_namefield)) as InputName
              
         FROM
         		PAYMENT_OVERALL_TRANSACTION_LOG as potl
         		LEFT JOIN 
         		INTRANET_USER as b 
         		ON potl.StudentID = b.UserID 
            LEFT JOIN 
            INTRANET_ARCHIVE_USER AS c 
            ON potl.StudentID=c.UserID
            LEFT JOIN
            INTRANET_USER as input_b 
         		ON potl.InputBy = input_b.UserID 
         	LEFT JOIN 
            INTRANET_ARCHIVE_USER AS input_c 
            ON potl.InputBy=input_c.UserID
         WHERE
         		 potl.TransactionType=9 
         		 and 
         			(
               b.EnglishName LIKE '%$keyword%' OR
               b.ChineseName LIKE '%$keyword%' OR
               b.ClassName LIKE '%$keyword%' OR
               b.ClassNumber LIKE '%$keyword%' OR
               potl.RefCode LIKE '%$keyword%' OR
               c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%'
              )
             $date_cond
             $user_cond
                ";
$Result = $lpayment->returnArray($sql);


$sql_summary  = "SELECT
		            COUNT(*) as NumberOfTransaction,
					CONCAT('$',SUM(ROUND(potl.Amount,2))) as TotalAmount 
		         FROM
		         		PAYMENT_OVERALL_TRANSACTION_LOG as potl
		         		LEFT JOIN 
		         		INTRANET_USER as b 
		         		ON potl.StudentID = b.UserID 
		            LEFT JOIN 
		            INTRANET_ARCHIVE_USER AS c 
		            ON potl.StudentID=c.UserID
		         WHERE
		         		 potl.TransactionType=9 
		         		 and 
		         			(
		               b.EnglishName LIKE '%$keyword%' OR
		               b.ChineseName LIKE '%$keyword%' OR
		               b.ClassName LIKE '%$keyword%' OR
		               b.ClassNumber LIKE '%$keyword%' OR
		               potl.RefCode LIKE '%$keyword%' OR
		               c.EnglishName LIKE '%$keyword%' OR
		               c.ChineseName LIKE '%$keyword%' OR
		               c.ClassName LIKE '%$keyword%' OR
		               c.ClassNumber LIKE '%$keyword%'
		              )
		             $date_cond
		             $user_cond
		                ";
$summary_record = $lpayment->returnResultSet($sql_summary);

$x = "<table width=90% border=0  cellpadding=2 cellspacing=0 align='center'>";
$x .= '<tr>';
$x .= '<td width="10%"><b>'.$i_ClassName.'</b></td>';
$x .= '<td><b>:&nbsp;'.(($ClassName != "")? $ClassName:$i_general_all).'</b></td>';
$x .= '</tr>';
$x .= '<tr>';
$x .= '<td><b>'.$Lang['Payment']['Keyword'].'</b></td>';
$x .= '<td><b>:&nbsp;'.$Keyword.'</b></td>';
$x .= '</tr>';
$x .= '</table>';

$x .= "<table width=90% border=0  cellpadding=2 cellspacing=0 align='center'>";
$x .= '<tr>';
$x .= '<td width="10%"><b>'.$i_Payment_TotalCountTransaction.'</b></td>';
$x .= '<td><b>:&nbsp;'.($summary_record[0]['NumberOfTransaction']).'</b></td>';
$x .= '</tr>';
$x .= '<tr>';
$x .= '<td><b>'.$i_Payment_ItemTotal.'</b></td>';
$x .= '<td><b>:&nbsp;'.$summary_record[0]['TotalAmount'].'</b></td>';
$x .= '</tr>';
$x .= '</table>';

//$x .= '<hr>';
$x .= "<table width=90% border=0  cellpadding=1 cellspacing=0 align='center' class='eSporttableborder'>";
$x .= "<thead>";
$x .= "<tr class='eSporttdborder eSportprinttabletitle'>";
$x .= "<td width=20% class='eSporttdborder eSportprinttabletitle'>".$i_Payment_Field_Username."</td>\n";
$x .= "<td width=10% class='eSporttdborder eSportprinttabletitle'>".$i_UserClassName."</td>\n";
$x .= "<td width=10% class='eSporttdborder eSportprinttabletitle'>".$i_UserClassNumber."</td>\n";
$x .= "<td width=10% class='eSporttdborder eSportprinttabletitle'>".$i_Payment_Field_CreditAmount."</td>\n";
$x .= "<td width=10% class='eSporttdborder eSportprinttabletitle'>".$i_Payment_Field_RefCode."</td>\n";
$x .= "<td width=20% class='eSporttdborder eSportprinttabletitle'>".$i_Payment_Field_CancelDepositTime."</td>\n";
$x .= "<td width=20% class='eSporttdborder eSportprinttabletitle'>".$i_general_last_modified_by."</td>\n";
$x .= "</tr>";
$x .= "</thead>";
$x .= "<tbody>";

for ($i=0; $i< sizeof($Result); $i++) {
	$x .= '<tr>';
	$x .= '<td class=\'eSporttdborder eSportprinttext\'>';
	$x .= $Result[$i]['Name'];
	$x .= '</td>';
	$x .= '<td class=\'eSporttdborder eSportprinttext\'>';
	$x .= (trim($Result[$i]['ClassName'])=='')?'--':$Result[$i]['ClassName'];
	$x .= '</td>';
	$x .= '<td class=\'eSporttdborder eSportprinttext\'>';
	$x .= (trim($Result[$i]['ClassNumber'])=='')?'--':$Result[$i]['ClassNumber'];
	$x .= '</td>';
	$x .= '<td class=\'eSporttdborder eSportprinttext\'>';
	$x .= $Result[$i]['Amount'];
	$x .= '</td>';
	$x .= '<td class=\'eSporttdborder eSportprinttext\'>';
	$x .= (trim($Result[$i]['RefCode'])=='')?'--':$Result[$i]['RefCode'];
	$x .= '</td>';
	$x .= '<td class=\'eSporttdborder eSportprinttext\'>';
	$x .= $Result[$i]['TransactionTime'];
	$x .= '</td>';
	$x .= '<td class=\'eSporttdborder eSportprinttext\'>';
	$x .= $Result[$i]['InputName'];
	$x .= '</td>';
	$x .= '</tr>';
}
$x.="</tbody>";
$x.="</table>";

include_once($PATH_WRT_ROOT."templates/fileheader.php");
//echo displayNavTitle($i_Payment_Menu_Report_SchoolAccount,'');
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
?>
<style type="text/css" media="print">
thead {display: table-header-group;}
</style>
<table width="90%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submitPrint")?></td>
	</tr>
</table>
<?php
if($sys_custom['ePayment']['ReportWithSchoolName']){
	$school_name = GET_SCHOOL_NAME();
	echo '<table width="90%" align="center" border="0">
			<tr>
				<td align="center"><h2><b>'.$school_name.'</b></h2></td>
			</tr>
		</table>';
}
?>
<table border=0 width=90% cellpadding=2 align=center>
<tr><td class='<?=$css_title?>'><b><?=$Lang['Payment']['CancelDepositReport']?> (<?="$FromDate $i_Profile_To $ToDate"?>)</b></td></tr>
</table>
<?=$x?>
<br>
<table width=90% border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td align=right class='<?=$css_text?>'>
<?="$i_general_report_creation_time : ".date('Y-m-d H:i:s')?>
</td></tr>
</table>
<BR><BR>
<?php
include_once($PATH_WRT_ROOT."templates/filefooter.php");
intranet_closedb();
?>
