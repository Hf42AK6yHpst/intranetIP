<?php
# using: 

######### Change Log [Start] #########
#
#	2017-11-16 (Carlos): $sys_custom['ePayment']['TNG'] - added TNG paid info.
#	2010-03-24 YatWoon : update the query don't order by "b.ClassName,c.ClassName"
#
######### Change Log [End] #########

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

if($ItemID=="") header("Location: index.php");

include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "StatisticsReport_PaymentItem";

$linterface = new interface_html();

# Get Item Name , Category Name, Start Date , End Date
$sql=" SELECT a.Name,b.Name,a.StartDate,a.EndDate FROM PAYMENT_PAYMENT_ITEM AS a LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY AS b ON (a.CatID = b.CatID) WHERE a.ItemID='$ItemID'";
$temp = $lpayment->returnArray($sql,4);
list($item_name,$cat_name,$start_date,$end_date) = $temp[0];

$x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Payment_Field_PaymentItem</td>\n";
$x .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"70%\">$item_name</td></tr>\n";
$x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Payment_Field_PaymentCategory</td>\n";
$x .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$cat_name</td></tr>\n";
$x .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Payment_PresetPaymentItem_PaymentPeriod</td>\n";
$x .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$start_date $i_To $end_date</td></tr>\n";


$sql=" SELECT a.Amount, 
				a.RecordStatus, 
				IF(b.UserID IS NULL AND c.UserID IS NOT NULL, c.ClassName,b.ClassName) as ClassNameTmp ";
if($sys_custom['ePayment']['TNG']){
	$sql.=" ,t.RecordID as TNGRecordID,t.PaymentStatus as TNGPaymentStatus,t.ChargeStatus as TNGChargeStatus ";	
}
$sql.=" FROM PAYMENT_PAYMENT_ITEMSTUDENT AS a ";
if($sys_custom['ePayment']['TNG']){
	$sql.=" LEFT JOIN PAYMENT_TNG_TRANSACTION as t ON t.PaymentID=a.PaymentID ";
}
$sql.=" LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) 
		LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON (a.StudentID = c.UserID)
		WHERE a.ItemID='$ItemID'  ORDER BY ClassNameTmp";
	

$temp = $lpayment->returnArray($sql);

$colspan = 0;
if($sys_custom['ePayment']['TNG']){
	$colspan += 4;
}

$y.="<tr class=\"tablebluetop\">";
$y.="<td class=\"tabletoplink\">$i_ClassName</td>"; 
$y.="<td class=\"tabletoplink\">$i_Payment_Field_TotalPaidCount</td>"; 
$y.="<td class=\"tabletoplink\">$i_Payment_PresetPaymentItem_PaidStudentCount</td>";
if($sys_custom['ePayment']['TNG']){
	$y.="<td class=\"tabletoplink\">".$Lang['ePayment']['TNGPaidCount']."</td>";
	$y.="<td class=\"tabletoplink\">".$Lang['ePayment']['ManualPaidCount']."</td>";
}
$y.="<td class=\"tabletoplink\">$i_Payment_PresetPaymentItem_UnpaidStudentCount</td>"; 
$y.="<td class=\"tabletoplink\">$i_Payment_PresetPaymentItem_PaidAmount</td>";
if($sys_custom['ePayment']['TNG']){
	$y.="<td class=\"tabletoplink\">".$Lang['ePayment']['TNGAmountPaid']."</td>";
	$y.="<td class=\"tabletoplink\">".$Lang['ePayment']['ManualAmountPaid']."</td>";
}
$y.="<td class=\"tabletoplink\">$i_Payment_PresetPaymentItem_UnpaidAmount</td>"; 
$y.="</tr>";


if(sizeof($temp)>0){
	for($i=0;$i<sizeof($temp);$i++){
		list($amount,$isPaid,$class_name) = $temp[$i];
		$result["$class_name"]['total']++;
		if($isPaid){
			$result["$class_name"]['paidcount']++;
			$result["$class_name"]['paid']+=$amount+0;
			if($sys_custom['ePayment']['TNG'] && $temp[$i]['TNGRecordID']!='' && $temp[$i]['TNGPaymentStatus']==1 && $temp[$i]['TNGChargeStatus']==1){
				$result["$class_name"]['tng_paidcount']++;
				$result["$class_name"]['tng_paid']+=$amount+0;
			}
		}else{
			$result["$class_name"]['unpaidcount']++;
			$result["$class_name"]['unpaid']+=$amount+0;
		}
	}
	
	$overall_total =0;
	$overall_paid  =0;
	$overall_paidcount=0;
	$overall_unpaid =0;
	$overall_unpaidcount=0;
	if($sys_custom['ePayment']['TNG'])
	{
		$overall_tng_paid = 0;
		$overall_tng_paidcount = 0;
	}
	
	$j=0;
	foreach($result as $class => $values){
		
		$total = $values['total'];
		$paid_count = $values['paidcount'] + 0;
		$unpaid_count = $values['unpaidcount'] + 0;
		$paid = $values['paid'] + 0;
		$unpaid=$values['unpaid'] + 0;
		
		$overall_total +=$total+0;
		$overall_paidcount+=$paid_count;
		$overall_unpaidcount+=$unpaid_count;
		$overall_paid +=$paid;
		$overall_unpaid+=$unpaid;
		
		$css = $j%2==0?"tablebluerow1 tabletext":"tablebluerow2 tabletext";
		$y.="<tr class=\"$css\">";
		$y.="<td class=\"tabletext\">$class</td>";
		$y.="<td class=\"tabletext\">$total</td>";
		$y.="<td class=\"tabletext\">".($paid_count==""?0:$paid_count)."</td>";
		if($sys_custom['ePayment']['TNG']){
			$overall_tng_paid += $values['tng_paid'] + 0;
			$overall_tng_paidcount += $values['tng_paidcount'] + 0;
			$y.="<td class=\"tabletext\">".($values['tng_paidcount']+0)."</td>";
			$y.="<td class=\"tabletext\">".($paid_count-$values['tng_paidcount'])."</td>";
		}
		$y.="<td class=\"tabletext\">".($unpaid_count==""?0:$unpaid_count)."</td>";
		$y.="<td class=\"tabletext\">$".number_format($paid,2)."</td>";
		if($sys_custom['ePayment']['TNG']){
			$y.="<td class=\"tabletext\">".$lpayment->getWebDisplayAmountFormat($values['tng_paid'])."</td>";
			$y.="<td class=\"tabletext\">".$lpayment->getWebDisplayAmountFormat($paid-$values['tng_paid'])."</td>";
		}
		$y.="<td class=\"tabletext\">$".number_format($unpaid,2)."</td>";
		$y.="</tr>";
		$j++;
	}
	$y.="<tr class=\"tablebluebottom\"><td class=\"tabletext\">$i_Payment_SchoolAccount_Total</td><td class=\"tabletext\">$overall_total</td><td class=\"tabletext\">$overall_paidcount</td>";
	if($sys_custom['ePayment']['TNG']){
		$y.="<td class=\"tabletext\">$overall_tng_paidcount</td>";
		$y.="<td class=\"tabletext\">".($overall_paidcount-$overall_tng_paidcount)."</td>";
	}
	$y.="<td class=\"tabletext\">$overall_unpaidcount</td><td class=\"tabletext\">$".number_format($overall_paid,2)."</td>";
	if($sys_custom['ePayment']['TNG']){
		$y.="<td class=\"tabletext\">".$lpayment->getWebDisplayAmountFormat($overall_tng_paid)."</td>";
		$y.="<td class=\"tabletext\">".$lpayment->getWebDisplayAmountFormat($overall_paid-$overall_tng_paid)."</td>";
	}
	$y.="<td class=\"tabletext\">$".number_format($overall_unpaid,2)."</td></tr>";
	
}else{ # no record
	$y.="<tr><td class=\"tablebluerow2 tabletext\" colspan=\"".(6+$colspan)."\" align=\"center\" height=\"40\" style=\"vertical-align:middle\">$i_no_record_exists_msg<td></tr>";
}


$display=$y;

$toolbar = $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
$toolbar2 = $linterface->GET_LNK_EXPORT("class_stat_export.php?ItemID=$ItemID","","","","",0);

$TAGS_OBJ[] = array($i_Payment_Menu_Report_PresetItem, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($i_Payment_PresetPaymentItem_ClassStat);
?>
<script language="javascript">
    function openPrintPage()
	{
         newWindow("class_stat_print.php?ItemID=<?=$ItemID?>",10);
	}
</script>
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
				<?= $x ?>
				<tr>
					<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td colspan="5">
						<table width="100%" border="0" cellpadding="3" cellspacing="0" align="center">
							<tr>
								<td colspan="6" align="left"><?=$toolbar?> <?=$toolbar2?></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
				<?=$display?>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td class="tabletext" align="right"><?="$i_general_report_creation_time : ".date('Y-m-d H:i:s')?></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<?php
$linterface->LAYOUT_STOP(); 
intranet_closedb();
?>
