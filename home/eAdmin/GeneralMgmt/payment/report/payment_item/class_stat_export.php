<?php
// Editing by 
######### Change Log [Start] #########
#
#	2017-11-16 (Carlos): $sys_custom['ePayment']['TNG'] - added TNG paid info.
#	2010-03-24 YatWoon : update the query don't order by "b.ClassName,c.ClassName"
#
######### Change Log [End] #########

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$lexport = new libexporttext();

# Get Item Name , Category Name, Start Date , End Date
$sql=" SELECT a.Name,b.Name,a.StartDate,a.EndDate 
		FROM PAYMENT_PAYMENT_ITEM AS a 
		LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY AS b ON (a.CatID = b.CatID) WHERE a.ItemID='$ItemID'";
$temp = $lpayment->returnArray($sql,4);
list($item_name,$cat_name,$start_date,$end_date) = $temp[0];
/*
$x="\"$i_Payment_PresetPaymentItem_ClassStat\"\n";
$x.="\"$i_Payment_Field_PaymentItem\",\"$item_name\"\n";
$x.="\"$i_Payment_Field_PaymentCategory\",\"$cat_name\"\n";
$x.="\"$i_Payment_PresetPaymentItem_PaymentPeriod\",\"$start_date $i_To $end_date\"\n";
*/
$preheader = $i_Payment_PresetPaymentItem_ClassStat."\r\n";
$preheader .= $i_Payment_Field_PaymentItem."\t".$item_name."\r\n";
$preheader .= $i_Payment_Field_PaymentCategory."\t".$cat_name."\r\n";
$preheader .= $i_Payment_PresetPaymentItem_PaymentPeriod."\t".$start_date." ".$i_To." ".$end_date."\r\n";


/*	
$sql=" SELECT a.Amount, a.RecordStatus, b.ClassName FROM PAYMENT_PAYMENT_ITEMSTUDENT AS a 
		LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) WHERE a.ItemID='$ItemID' AND b.ClassName IS NOT NULL  ORDER BY b.ClassName";
*/
$sql=" SELECT a.Amount, 
				a.RecordStatus, 
				IF(b.UserID IS NULL AND c.UserID IS NOT NULL, c.ClassName,b.ClassName) as ClassNameTmp ";
if($sys_custom['ePayment']['TNG']){
	$sql.=" ,t.RecordID as TNGRecordID,t.PaymentStatus as TNGPaymentStatus,t.ChargeStatus as TNGChargeStatus ";	
}
$sql.=" FROM PAYMENT_PAYMENT_ITEMSTUDENT AS a ";
if($sys_custom['ePayment']['TNG']){
	$sql.=" LEFT JOIN PAYMENT_TNG_TRANSACTION as t ON t.PaymentID=a.PaymentID ";
}
$sql.=" LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) 
				LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON (a.StudentID = c.UserID)
		WHERE a.ItemID='$ItemID'  ORDER BY ClassNameTmp";
			
$temp = $lpayment->returnArray($sql);
/*
$y="\"$i_ClassName\","; 
$y.="\"$i_Payment_Field_TotalPaidCount\","; 
$y.="\"$i_Payment_PresetPaymentItem_PaidStudentCount\","; 
$y.="\"$i_Payment_PresetPaymentItem_UnpaidStudentCount\","; 
$y.="\"$i_Payment_PresetPaymentItem_PaidAmount\","; 
$y.="\"$i_Payment_PresetPaymentItem_UnpaidAmount\""; 
$y.="\n";
*/
$exportColumn = array($i_ClassName, $i_Payment_Field_TotalPaidCount, $i_Payment_PresetPaymentItem_PaidStudentCount);
if($sys_custom['ePayment']['TNG']){
	$exportColumn[] = $Lang['ePayment']['TNGPaidCount'];
	$exportColumn[] = $Lang['ePayment']['ManualPaidCount'];
}
$exportColumn[] = $i_Payment_PresetPaymentItem_UnpaidStudentCount;
$exportColumn[] = $i_Payment_PresetPaymentItem_PaidAmount;
if($sys_custom['ePayment']['TNG']){
	$exportColumn[] = $Lang['ePayment']['TNGAmountPaid'];
	$exportColumn[] = $Lang['ePayment']['ManualAmountPaid'];
}
$exportColumn[] = $i_Payment_PresetPaymentItem_UnpaidAmount;

if(sizeof($temp)>0){
	for($i=0;$i<sizeof($temp);$i++){
		list($amount,$isPaid,$class_name) = $temp[$i];
		$result["$class_name"]['total']++;
		if($isPaid){
			$result["$class_name"]['paidcount']++;
			$result["$class_name"]['paid']+=$amount+0;
			if($sys_custom['ePayment']['TNG'] && $temp[$i]['TNGRecordID']!='' && $temp[$i]['TNGPaymentStatus']==1 && $temp[$i]['TNGChargeStatus']==1){
				$result["$class_name"]['tng_paidcount']++;
				$result["$class_name"]['tng_paid']+=$amount+0;
			}
		}else{
			$result["$class_name"]['unpaidcount']++;
			$result["$class_name"]['unpaid']+=$amount+0;
		}
	}
	
	$overall_total =0;
	$overall_paid  =0;
	$overall_paidcount=0;
	$overall_unpaid =0;
	$overall_unpaidcount=0;
	if($sys_custom['ePayment']['TNG'])
	{
		$overall_tng_paid = 0;
		$overall_tng_paidcount = 0;
	}
	
	foreach($result as $class => $values){
		
		$total = $values['total'];
		$paid_count = $values['paidcount'];
		$unpaid_count = $values['unpaidcount'];
		$paid = $values['paid'];
		$unpaid=$values['unpaid'];
		
		$overall_total +=$total+0;
		$overall_paidcount+=$paid_count;
		$overall_unpaidcount+=$unpaid_count;
		$overall_paid +=$paid;
		$overall_unpaid+=$unpaid;
		/*				
		$y.="\"$class\",";
		$y.="\"$total\",";
		$y.="\"".($paid_count==""?0:$paid_count)."\",";
		$y.="\"".($unpaid_count==""?0:$unpaid_count)."\",";
		$y.="\"".round($paid,2)."\",";
		$y.="\"".round($unpaid,2)."\"";
		$y.="\n";
		*/
		$row = array($class, $total, ($paid_count==""?0:$paid_count));
		if($sys_custom['ePayment']['TNG']){
			$row[] = $values['tng_paidcount']+0;
			$row[] = $paid_count-$values['tng_paidcount'];
		}
		$row[] = ($unpaid_count==""?0:$unpaid_count);
		$row[] = round($paid,2);
		if($sys_custom['ePayment']['TNG']){
			$row[] = round($values['tng_paid'],2);
			$row[] = round($paid-$values['tng_paid'],2);
		}
		$row[] = round($unpaid,2);
		$rows[] = $row;
		//$rows[] = array($class, $total, ($paid_count==""?0:$paid_count), ($unpaid_count==""?0:$unpaid_count), round($paid,2), round($unpaid,2));
	}
	//$y.="\"$i_Payment_SchoolAccount_Total\",\"$overall_total\",\"$overall_paidcount\",\"$overall_unpaidcount\",\"".round($overall_paid,2)."\",\"".round($overall_unpaid,2)."\"\n";
	$row = array($i_Payment_SchoolAccount_Total, $overall_total, $overall_paidcount);
	if($sys_custom['ePayment']['TNG']){
		$row[] = $overall_tng_paidcount;
		$row[] = $overall_paidcount-$overall_tng_paidcount;
	}
	$row[] = $overall_unpaidcount;
	$row[] = round($overall_paid,2);
	if($sys_custom['ePayment']['TNG']){
		$row[] = round($overall_tng_paid,2);
		$row[] = round($overall_paid-$overall_tng_paid,2);
	}
	$row[] = round($overall_unpaid,2);
 	$rows[] = $row;
	
}else{ # no record
	//$y.="\"$i_no_record_exists_msg\"\n";
	$rows[] = $i_no_record_exists_msg;
}


//$display=$x.$y;
//$display.="\"$i_general_report_creation_time\",\"".date('Y-m-d H:i:s')."\"\n";
$rows[] = array($i_general_report_creation_time, date('Y-m-d H:i:s'));

intranet_closedb();

$filename = "class_stats.csv";
//output2browser($display,$filename);

$export_content = $preheader.$lexport->GET_EXPORT_TXT($rows, $exportColumn);
$lexport->EXPORT_FILE($filename, $export_content);
?>