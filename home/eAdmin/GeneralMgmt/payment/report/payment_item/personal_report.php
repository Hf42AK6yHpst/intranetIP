<?php
// Editing by 
/*
 * 2017-11-16 (Carlos): $sys_custom['ePayment']['TNG'] - added TNG paid info.
 * 2017-07-04 (Carlos): Added display column [Transaction Time] as paid time.
 * 2017-02-10 (Carlos): $sys_custom['ePayment']['HartsPreschool'] - display STRN, Payment Method, Receipt Remark.
 * 2015-12-29 (Carlos): $sys_custom['ePayment']['CreditTransactionWithLoginID'] - added the display column [Login ID].
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

if($ItemID=="") header("Location: index.php");

include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "StatisticsReport_PaymentItem";

$linterface = new interface_html();

$paid_cond ="";

$namefield = getNameFieldByLang("b.");

if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$archive_namefield="c.ChineseName";
}else  $archive_namefield ="c.EnglishName";



$sql=" SELECT a.StudentID,
			a.Amount,
			a.RecordStatus,
			a.PaidTime,
			IF(b.UserID IS NULL AND c.UserID IS NOT NULL, CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), $namefield),
			IF(b.UserID IS NULL AND c.UserID IS NOT NULL, CONCAT('<i>',c.ClassName,'</i>'), b.ClassName),
			IF(b.UserID IS NULL AND c.UserID IS NOT NULL,CONCAT('<i>',c.ClassNumber,'</i>'),b.ClassNumber) ";
if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
	$sql.= " ,IF(b.UserID IS NOT NULL,b.UserLogin,c.UserLogin) as UserLogin ";
}
if($sys_custom['ePayment']['HartsPreschool']){
	$sql.= " ,IF(b.UserID IS NOT NULL,b.STRN,c.STRN) as STRN ";
	$sql.=" ,CASE a.PaymentMethod ";
	foreach($sys_custom['ePayment']['PaymentMethodItems'] as $key => $val)
	{
		$sql .= " WHEN '$key' THEN '$val' ";	
	}
	$sql .= " ELSE '".$Lang['ePayment']['NA']."' 
			END as PaymentMethod ";
	$sql .= " ,a.ReceiptRemark ";
}
if($sys_custom['ePayment']['TNG']){
	$sql.=" ,t.RecordID as TNGRecordID,t.PaymentStatus as TNGPaymentStatus,t.ChargeStatus as TNGChargeStatus ";	
}
$sql.= "FROM PAYMENT_PAYMENT_ITEMSTUDENT AS a ";
if($sys_custom['ePayment']['TNG']){
	$sql.=" LEFT JOIN PAYMENT_TNG_TRANSACTION as t ON t.PaymentID=a.PaymentID ";
}
$sql.=" LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID)
		LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON (a.StudentID = c.UserID)
		WHERE a.ItemID='$ItemID' $paid_cond ORDER BY b.ClassName,b.ClassNumber+0,c.ClassName,c.ClassNumber";

$temp = $lpayment->returnArray($sql);

$colspan = 6;

$y.="<tr class=\"tablebluetop\">";
if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
	$y.="<td class=\"tabletoplink\">".$Lang['AccountMgmt']['LoginID']."</td>";
	$colspan++;
}
$y.="<td class=\"tabletoplink\">$i_ClassName</td>";
$y.="<td class=\"tabletoplink\">$i_ClassNumber</td>";
if($sys_custom['ePayment']['HartsPreschool']){
	$y.="<td class=\"tabletoplink\">$i_STRN</td>";
	$colspan++;
}
$y.="<td class=\"tabletoplink\">$i_UserName</td>";
$y.="<td class=\"tabletoplink\">$i_Payment_Field_Amount</td>";
$y.="<td class=\"tabletoplink\">$i_general_status</td>";
$y.="<td class=\"tabletoplink\">$i_Payment_Field_TransactionTime</td>";
if($sys_custom['ePayment']['HartsPreschool']){
	$y.="<td class=\"tabletoplink\">".$Lang['ePayment']['PaymentMethod']."</td>";
	$y.="<td class=\"tabletoplink\">".$Lang['ePayment']['PaymentRemark']."</td>";
	$colspan += 2;
}
$y.="</tr>";

$paidcount =0;
$unpaidcount =0;
$paid=0;
$unpaid=0;
$noRecord=false;
if($sys_custom['ePayment']['TNG']){
	$tng_paidcount = 0;
	$tng_paid = 0;
}

if(sizeof($temp)>0){

	for($i=0;$i<sizeof($temp);$i++){
		list($sid,$amount,$isPaid,$paid_time,$sname,$class_name,$class_num) = $temp[$i];
		if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
			$user_login = $temp[$i]['UserLogin'];	
		}
		if($isPaid){
			$paidcount++;
			$paid+=$amount+0;
			if($filter_paid=="" || $filter_paid==1){
				$result[$sid]['class']=$class_name;
				$result[$sid]['classnumber']=$class_num;
				$result[$sid]['name']=$sname;
				$result[$sid]['amount']=$amount+0;
				$result[$sid]['status'] = $i_Payment_PresetPaymentItem_PaidCount;
				$result[$sid]['paidtime'] = $paid_time;
				if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
					$result[$sid]['userlogin'] = $user_login;
				}
				if($sys_custom['ePayment']['HartsPreschool']){
					$result[$sid]['strn'] = $temp[$i]['STRN'];
					$result[$sid]['paymentmethod'] = $temp[$i]['PaymentMethod'];
					$result[$sid]['receiptremark'] = $temp[$i]['ReceiptRemark'];
				}
				if($sys_custom['ePayment']['TNG'] && $temp[$i]['TNGRecordID']!='' && $temp[$i]['TNGPaymentStatus']==1 && $temp[$i]['TNGChargeStatus']==1){
					$result[$sid]['paid_by_tng'] = true;
					$tng_paidcount++;
					$tng_paid += $amount+0;
				}
			}
		}else{
			$unpaidcount++;
			$unpaid+=$amount+0;
			if($filter_paid!=1){
				$result[$sid]['class']=$class_name;
				$result[$sid]['classnumber']=$class_num;
				$result[$sid]['name']=$sname;
				$result[$sid]['amount']=$amount+0;
				$result[$sid]['status'] = $i_Payment_PresetPaymentItem_UnpaidCount;
				$result[$sid]['paidtime'] = $paid_time;
				if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
					$result[$sid]['userlogin'] = $user_login;
				}
				if($sys_custom['ePayment']['HartsPreschool']){
					$result[$sid]['strn'] = $temp[$i]['STRN'];
					$result[$sid]['paymentmethod'] = $temp[$i]['PaymentMethod'];
					$result[$sid]['receiptremark'] = $temp[$i]['ReceiptRemark'];
				}
				
			}
		}
	}
	$j=0;
	if(sizeof($result)>0){
		foreach($result as $sid => $values){
			$r_name 	= $values['name'];
			$r_class 	= $values['class'];
			$r_classnum	= $values['classnumber'];
			$r_amount 	= $values['amount'];
			$r_status 	= $values['status'];
			$r_paid_time = $values['paidtime'];
			if($sys_custom['ePayment']['TNG'] && $values['paid_by_tng']){
				$r_status .= ' (TNG)';
			}
			
			$css = $j%2==0?"tablebluerow1 tabletext":"tablebluerow2 tabletext";
			$y.="<tr class=\"$css\">";
			if($sys_custom['ePayment']['CreditTransactionWithLoginID']){
				$y.="<td class=\"tabletext\">".$values['userlogin']."</td>";
			}
			$y.="<td class=\"tabletext\">$r_class</td>";
			$y.="<td class=\"tabletext\">$r_classnum</td>";
			if($sys_custom['ePayment']['HartsPreschool']){
				$y.="<td class=\"tabletext\">".$values['strn']."</td>";
			}
			$y.="<td class=\"tabletext\">$r_name</td>";
			$y.="<td class=\"tabletext\">$".number_format($r_amount,2)."</td>";
			$y.="<td class=\"tabletext\">$r_status</td>";
			$y.="<td class=\"tabletext\">".Get_String_Display($r_paid_time)."</td>";
			if($sys_custom['ePayment']['HartsPreschool']){
				$y.="<td class=\"tabletext\">".$values['paymentmethod']."</td>";
				$y.="<td class=\"tabletext\">".$values['receiptremark']."</td>";
			}
			$y.="</tr>";
			$j++;
		}
	}else{ # no record
		$noRecord=true;
	}
}else{
	$noRecord = true;
}
if($noRecord){
		$y.="<tr><td class=\"tabletext\" colspan=\"".$colspan."\" align=\"center\" height=\"40\" style=\"vertical-align:middle\">$i_no_record_exists_msg</td></tr>";
}



# Get Item Name , Category Name, Start Date , End Date
$sql=" SELECT a.Name,b.Name,a.StartDate,a.EndDate FROM PAYMENT_PAYMENT_ITEM AS a LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY AS b ON (a.CatID = b.CatID) WHERE a.ItemID='$ItemID'";
$temp = $lpayment->returnArray($sql,4);
list($item_name,$cat_name,$start_date,$end_date) = $temp[0];

$x.="<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Payment_Field_PaymentItem:</td>";
$x.="<td colspan=\"3\" valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"70%\">$item_name</td></tr>";
$x.="<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Payment_Field_PaymentCategory:</td>";
$x.="<td colspan=\"3\" valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"70%\">$cat_name</td></tr>";
$x.="<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Payment_PresetPaymentItem_PaymentPeriod:</td>";
$x.="<td colspan=\"3\" valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"70%\">$start_date $i_To $end_date</td></tr>";

$x.="<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\" width=\"30%\">$i_Payment_PresetPaymentItem_PaidAmount:</td>";
$x.="<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"30%\">$".number_format($paid,2)."</td>";
$x.="<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\" width=\"30%\">$i_Payment_PresetPaymentItem_PaidStudentCount:</td>";
$x.="<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"10%\">$paidcount</td></tr>";

if($sys_custom['ePayment']['TNG']){
	$x.="<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\" width=\"30%\">".$Lang['ePayment']['TNGAmountPaid'].":</td>";
	$x.="<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"30%\">$".number_format($tng_paid,2)."</td>";
	$x.="<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\" width=\"30%\">".$Lang['ePayment']['TNGPaidCount'].":</td>";
	$x.="<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"10%\">$tng_paidcount</td></tr>";
	
	$x.="<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\" width=\"30%\">".$Lang['ePayment']['ManualAmountPaid'].":</td>";
	$x.="<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"30%\">$".number_format($paid - $tng_paid,2)."</td>";
	$x.="<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\" width=\"30%\">".$Lang['ePayment']['ManualPaidCount'].":</td>";
	$x.="<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"10%\">".($paidcount - $tng_paidcount)."</td></tr>";
}

$x.="<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\" width=\"30%\">$i_Payment_PresetPaymentItem_UnpaidAmount:</td>";
$x.="<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"30%\">$".number_format($unpaid,2)."</td>";
$x.="<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\" width=\"30%\">$i_Payment_PresetPaymentItem_UnpaidStudentCount:</td>";
$x.="<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"10%\">$unpaidcount</td></tr>";


$display=$y;


$firstname_paid = "$i_status_all";
$array_paid_name = array($i_Payment_PresetPaymentItem_PaidCount, $i_Payment_PresetPaymentItem_UnpaidCount);
$array_paid_data = array("1", "0");
$select_paid = getSelectByValueDiffName($array_paid_data,$array_paid_name,"name='filter_paid' onChange='document.form1.submit();'",$filter_paid,1,0, $firstname_paid);

$toolbar = $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
$toolbar2 = $linterface->GET_LNK_EXPORT("javascript:exportPage(document.form1,'personal_report_export.php?ItemID=$ItemID&filter_paid=$filter_paid')","","","","",0);

$TAGS_OBJ[] = array($i_Payment_Menu_Report_PresetItem,"", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($i_Payment_PresetPaymentItem_Personal);
?>
<script language="javascript">
    function openPrintPage()
	{
         newWindow("personal_report_print.php?ItemID=<?=$ItemID?>&filter_paid=<?=$filter_paid?>",10);
	}

	function exportPage(obj,url){
		old_url = obj.action;
		obj.action = url;
		obj.submit();
		obj.action = old_url;
	}
</script>
<br />
<form name="form1" action="" method="get">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
				<?= $x ?>
				<tr>
					<td class="dotline" colspan="4"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
							<tr>
								<td align="left"><?=$toolbar?> <?=$toolbar2?></td>
							</tr>
							<tr>
								<td align="left"><?= $select_paid ?></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
				<?= $display ?>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td class="tabletextremark" ><?=$i_Payment_Note_StudentRemoved?></td>
					<td class="tabletext" align="right"><?="$i_general_report_creation_time : ".date('Y-m-d H:i:s')?></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br />
<input type="hidden" name="ItemID" value='<?=$ItemID?>'>
</form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
