<?php
// Editing by 
/*
 * 2017-02-22 (Carlos): [ip.2.5.8.3.1] $sys_custom['ePayment']['HartsPreschool'] display Payment Method and Payment Remark.
 * 2017-02-14 (Carlos): $sys_custom['ePayment']['HartsPreschool'] - added STRN.
 * 2015-08-06 (Carlos): Set date range cookies.
 * 2015-03-24 (Bill): add fields - Log ID, Remarks, Last Modified By, support All Class Level [2014-1127-1747-49207]
 * 2013-10-30 (Carlos): KIS - hide [Add value Record Time], [Credit], [Balance After]
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance'];

$lpayment = new libpayment();
$lclass = new libclass();
$lexport = new libexporttext();

if($_REQUEST['TargetStartDate']!='' && $_REQUEST['TargetEndDate']!=''){
	$lpayment->Set_Report_Date_Range_Cookies($_REQUEST['TargetStartDate'], $_REQUEST['TargetEndDate']);
}

if(isset($TargetStudentID)) {
	
	$TargetStudentID = IntegerSafe($TargetStudentID);
	$user_list = implode(",",$TargetStudentID);
	
} else if(isset($TargetClassID)) {
	
	$TargetClassID = IntegerSafe($TargetClassID);
	$class_list = "'".implode("','",$TargetClassID)."'";
	
	$sql = "SELECT 
						a.UserID 
					FROM 
						YEAR_CLASS_USER as ycu 
						inner join 
						INTRANET_USER AS a 
						on uc.YearClassID in ($class_list) 
							and 
							ycu.UserID = a.UserID 
					ORDER BY a.ClassName,a.ClassNumber+0";
	$arr_user_list = $lpayment->returnVector($sql);
	
	if(sizeof($arr_user_list)>0) {
		$user_list = implode(",",$arr_user_list);
	}
	
} else {
	
	$class_level_id = $TargetClassLevel;
	
	// [2014-1127-1747-49207] report cust - Select All Class Level
	if($sys_custom['ePayment']['ReportWithAllTransactionRecords'] && $class_level_id == ""){
		
		// cond - filter year class user
		$cond = "";
		if(!empty($TargetStartDate) && !empty($TargetEndDate)){
			$StartDateYearID = getAcademicYearAndYearTermByDate($TargetStartDate);
			$StartDateYearID = $StartDateYearID['AcademicYearID'];
	        $EndDateYearID = getAcademicYearAndYearTermByDate($TargetEndDate);
			$EndDateYearID = $EndDateYearID['AcademicYearID'];
			$cond = " AND (yc.AcademicYearID = '".$lpayment->Get_Safe_Sql_Query($StartDateYearID)."' OR yc.AcademicYearID = '".$lpayment->Get_Safe_Sql_Query($EndDateYearID)."')";
		}
		
		// get all ClassLevelID
		$sql = "SELECT YearID FROM YEAR order by Sequence";
        $all_year_list = $lpayment->returnVector($sql);
        $all_year_list_sql = implode("','", (array)$all_year_list);
        
//		else if(!empty($TargetStartDate)){
//			$cond = " AND (yc.AcademicYearID = '$StartDateYearID')";
//		} else if(!empty($TargetEndDate)){
//			$cond = " AND (yc.AcademicYearID = '$EndDateYearID')";
//		}
		
		// get user list
		$sql = "SELECT 
					a.UserID 
				FROM 
					YEAR_CLASS as yc 
				INNER JOIN 
					YEAR_CLASS_USER as ycu ON yc.YearID IN ('$all_year_list_sql')
					AND ycu.YearClassID = ycu.YearClassID
				INNER JOIN 
					INTRANET_USER AS a ON ycu.UserID = a.UserID
				WHERE
					1 $cond
				GROUP BY
					a.UserID";
		$arr_user_list = $lpayment->returnVector($sql);
		
	} else {
		
		$sql = "SELECT 
							a.UserID 
						FROM 
							YEAR_CLASS as yc 
							inner join 
							YEAR_CLASS_USER as ycu 
							on yc.YearID = '".$lpayment->Get_Safe_Sql_Query($class_level_id)."' 
								and 
								ycu.YearClassID = ycu.YearClassID 
							inner join 
							INTRANET_USER AS a 
							on ycu.UserID = a.UserID 
						ORDER BY yc.sequence, ycu.ClassNumber";
		$arr_user_list = $lpayment->returnVector($sql);
	}
	
	if(sizeof($arr_user_list)>0) {
		$user_list = implode(",",$arr_user_list);
	}
}

if($user_list != "") {
	
	$UserMapping = array();
		
	$arr_target_user = explode(",",$user_list);
	
	if(!empty($TargetStartDate) && !empty($TargetEndDate))
	{
		$start_day = substr($TargetStartDate,8,2);
		$start_month = substr($TargetStartDate,5,2);
		$start_year = substr($TargetStartDate,0,4);
		$StartDate = date("Y-m-d H:i:s",mktime(0,0,0,$start_month,$start_day,$start_year));
		
		$end_day = substr($TargetEndDate,8,2);
		$end_month = substr($TargetEndDate,5,2);
		$end_year = substr($TargetEndDate,0,4);
		$EndDate = date("Y-m-d H:i:s",mktime(23,59,59,$end_month,$end_day,$end_year));
		
		$date_range_cond = " AND (b.TransactionTime BETWEEN '".$lpayment->Get_Safe_Sql_Query($StartDate)."' AND '".$lpayment->Get_Safe_Sql_Query($EndDate)."') ";
	}
	
	// [2014-1127-1747-49207] report cust - get transaction logid, payment item remarks and last modified by
	if($sys_custom['ePayment']['ReportWithAllTransactionRecords']){
		$cust_table = "LEFT OUTER JOIN PAYMENT_PAYMENT_ITEMSTUDENT AS payitem ON (b.RelatedTransactionID = payitem.PaymentID AND (b.TransactionType = 2 OR b.TransactionType = 6))";		
		$cust_field = ", b.LogID, payitem.Remark";
		
		// Last Modified By - Ordering
		// 1. Input By
		// 2. Transaction Type - Payment : Processing Admin User / Processing User
		// 3. Transaction Type - Credit : Admin in Charge
		$cust_field .= ", IF((b.TransactionType = 2 OR b.TransactionType = 6) AND (payitem.ProcessingAdminUser IS NOT NULL OR payitem.ProcessingUserID IS NOT NULL), 
							IF(payitem.ProcessingAdminUser IS NOT NULL, payitem.ProcessingAdminUser, payitem.ProcessingUserID),
							IF(b.InputBy IS NOT NULL, b.InputBy,
								IF(b.TransactionType = 1 AND c.AdminInCharge IS NOT NULL, c.AdminInCharge, b.InputBy)
							)
						 ) AS LastModify";
		if($sys_custom['ePayment']['HartsPreschool']){
			$cust_field.=" ,CASE payitem.PaymentMethod ";
			foreach($sys_custom['ePayment']['PaymentMethodItems'] as $key => $val)
			{
				$cust_field .= " WHEN '$key' THEN '$val' ";	
			}
			$cust_field .= " ELSE '".$Lang['ePayment']['NA']."' 
					END as PaymentMethod ";
		
		 	$cust_field .= " ,payitem.ReceiptRemark ";
		 }
	}
	
	if(sizeof($arr_target_user)>0) {
		for($i=0; $i<sizeof($arr_target_user); $i++) {
			$user_id = $arr_target_user[$i];
			
			$sql  = "SELECT
							a.UserLogin,
							a.ClassName,
							a.ClassNumber,
							a.ChineseName,
							a.EnglishName,
							DATE_FORMAT(b.TransactionTime,'%Y-%m-%d %H:%i'),
							IF(b.TransactionType=1,DATE_FORMAT(c.TransactionTime,'%Y-%m-%d %H:%i'),'--'),
							CASE b.TransactionType
								WHEN 1 THEN '$i_Payment_TransactionType_Credit'
				                WHEN 2 THEN '$i_Payment_TransactionType_Payment'
				                WHEN 3 THEN '$i_Payment_TransactionType_Purchase'
				                WHEN 4 THEN '$i_Payment_TransactionType_TransferTo'
				                WHEN 5 THEN '$i_Payment_TransactionType_TransferFrom'
				                WHEN 6 THEN '$i_Payment_TransactionType_CancelPayment'
				                WHEN 7 THEN '$i_Payment_TransactionType_Refund'
				                WHEN 8 THEN '$i_Payment_PPS_Charge'
				                ELSE '$i_Payment_TransactionType_Other' END,
							IF(b.TransactionType IN (".implode(',',$lpayment->CreditTransactionType)."),".$lpayment->getExportAmountFormatDB("b.Amount").",' '),
							IF(b.TransactionType IN (".implode(',',$lpayment->DebitTransactionType)."),".$lpayment->getExportAmountFormatDB("b.Amount").",' '),
							b.Details, 
							".$lpayment->getExportAmountFormatDB("b.BalanceAfter").",
							b.RefCode
							$cust_field 
							,a.STRN 
					FROM
							INTRANET_USER AS a INNER JOIN 
							PAYMENT_OVERALL_TRANSACTION_LOG as b ON (a.UserID = b.StudentID) LEFT OUTER JOIN 
							PAYMENT_CREDIT_TRANSACTION as c ON (b.RelatedTransactionID = c.TransactionID)
							$cust_table
					WHERE
							b.StudentID IN ($user_id) 
							$date_range_cond
			        ORDER BY a.ClassName,a.ClassNumber+0";
			
			$arr_result = $lpayment->returnArray($sql);
			
			if($sys_custom['ePayment']['ReportWithAllTransactionRecords']){
				$content.="Log ID,";
			}
			$content.="\"$i_UserLogin\",";
			$content.="\"$i_UserClassName\",";
			$content.="\"$i_UserClassNumber\",";
			$content.="\"$i_UserChineseName\",";
			$content.="\"$i_UserEnglishName\",";
			$content.="\"$i_Payment_Field_TransactionTime\",";
			$content.="\"$i_Payment_Field_TransactionFileTime\",";
			$content.="\"$i_Payment_Field_TransactionType\",";
			$content.="\"$i_Payment_TransactionType_Credit\",";
			$content.="\"$i_Payment_TransactionType_Debit\",";
			$content.="\"$i_Payment_Field_TransactionDetail\",";
			$content.="\"$i_Payment_Field_BalanceAfterTransaction\",";
			if($sys_custom['ePayment']['ReportWithAllTransactionRecords']){
				$content.="\"".$Lang['General']['Remark']."\",";
			}
			$content.="\"$i_Payment_Field_RefCode\",";
			if($sys_custom['ePayment']['ReportWithAllTransactionRecords']){
				$content.="\"$i_general_last_modified_by\",";
			}
			$content.="\n";
			/*
			$rows[] = array($i_UserLogin, 
						$i_UserClassName, 
						$i_UserClassNumber, 
						$i_UserChineseName, 
						$i_UserEnglishName, 
						$i_Payment_Field_TransactionTime, 
						$i_Payment_Field_TransactionFileTime, 
						$i_Payment_Field_TransactionType, 
						$i_Payment_TransactionType_Credit, 
						$i_Payment_TransactionType_Debit,
						$i_Payment_Field_TransactionDetail,
						$i_Payment_Field_BalanceAfterTransaction,
						$i_Payment_Field_RefCode);
			*/
			$row = array();
			// [2014-1127-1747-49207]
			if($sys_custom['ePayment']['ReportWithAllTransactionRecords']){
				$row[] = "Log ID";
			}
			$row[] = $i_UserLogin;
			if($sys_custom['ePayment']['HartsPreschool']){
				$row[] = $i_STRN;
			}
			$row[] = $i_UserClassName;
			$row[] = $i_UserClassNumber;
			$row[] = $i_UserChineseName;
			$row[] = $i_UserEnglishName;
			$row[] = $i_Payment_Field_TransactionTime;
			if(!$isKIS){
				$row[] = $i_Payment_Field_TransactionFileTime;
			}
			$row[] = $i_Payment_Field_TransactionType;
			if(!$isKIS){
				$row[] = $i_Payment_TransactionType_Credit;
			} 
			$row[] = $i_Payment_TransactionType_Debit;
			$row[] = $i_Payment_Field_TransactionDetail;
			if(!$isKIS){
				$row[] = $i_Payment_Field_BalanceAfterTransaction;
			}
			// [2014-1127-1747-49207]
			if($sys_custom['ePayment']['ReportWithAllTransactionRecords']){
				$row[] = $Lang['General']['Remark'];
			}
			$row[] = $i_Payment_Field_RefCode;
			// [2014-1127-1747-49207]
			if($sys_custom['ePayment']['ReportWithAllTransactionRecords']){
				if($sys_custom['ePayment']['HartsPreschool']){
					$row[] = $Lang['ePayment']['PaymentMethod'];
					$row[] = $Lang['ePayment']['PaymentRemark'];
				}
				$row[] = $i_general_last_modified_by;
			}
			$rows[] = $row;
						
			if(sizeof($arr_result)>0) {
				for($j=0; $j<sizeof($arr_result); $j++) {
					list($user_login, $class_name, $class_number, $chi_name, $eng_name, $tran_time, $tran_file_time, $tran_type, $credit_amount, $debit_amount, $tran_details, $bal_after, $ref_code) = $arr_result[$j];
					
					// [2014-1127-1747-49207] report cust - extra data handling
					if($sys_custom['ePayment']['ReportWithAllTransactionRecords']){
						list($user_login, $class_name, $class_number, $chi_name, $eng_name, $tran_time, $tran_file_time, $tran_type, $credit_amount, $debit_amount, $tran_details, $bal_after, $ref_code, $trans_logid, $payment_item_remarks, $last_modified_userid) = $arr_result[$j];
						
						// Log ID - AXXXXXXXXX
						$trans_logid = "A".str_pad($trans_logid, 9, "0", STR_PAD_LEFT);
						
						// Remarks
						$payment_item_remarks = $payment_item_remarks? $payment_item_remarks : "--";
						
						// Last Modified By
						if($last_modified_userid != "" && !isset($UserMapping[$last_modified_userid])){
							$name_field = getNameFieldByLang();
							$sql = "SELECT $name_field AS Name FROM INTRANET_USER WHERE UserID = '$last_modified_userid'";
							$last_modified_username = $lpayment->returnVector($sql);
							$last_modified_username = $last_modified_username[0];
							
							$UserMapping[$last_modified_userid] = $last_modified_username? $last_modified_username : "--";
						} else {
							$last_modified_username = $UserMapping[$last_modified_userid];
							$last_modified_username = $last_modified_username? $last_modified_username : "--";
						}
					}
					
					if($sys_custom['ePayment']['ReportWithAllTransactionRecords']){
						$content.="\"$trans_logid\",";
					}
					$content.="\"$user_login\",";
					$content.="\"$class_name\",";
					$content.="\"$class_number\",";
					$content.="\"$chi_name\",";
					$content.="\"$eng_name\",";
					$content.="\"$tran_time\",";
					$content.="\"$tran_file_time\",";
					$content.="\"$tran_type\",";
					$content.="\"$credit_amount\",";
					$content.="\"$debit_amount\",";
					$content.="\"$tran_details\",";
					$content.="\"$bal_after\",";
					if($sys_custom['ePayment']['ReportWithAllTransactionRecords']){
						$content.="\"$payment_item_remarks\",";
					}
					$content.="\"$ref_code\",";
					if($sys_custom['ePayment']['ReportWithAllTransactionRecords']){
						$content.="\"$last_modified_username\",";
					}
					$content.="\n";
					/*
					$rows[] = array($user_login,$class_name,$class_number,$chi_name,$eng_name,$tran_time,
									$tran_file_time,$tran_type,$credit_amount,$debit_amount,$tran_details,
									$bal_after,$ref_code);
					*/
					$row = array();
					// [2014-1127-1747-49207]
					if($sys_custom['ePayment']['ReportWithAllTransactionRecords']){
						$row[] = $trans_logid;
					}
					$row[] = $user_login;
					if($sys_custom['ePayment']['HartsPreschool']){
						$row[] = $arr_result[$j]['STRN'];
					}
					$row[] = $class_name;
					$row[] = $class_number;
					$row[] = $chi_name;
					$row[] = $eng_name;
					$row[] = $tran_time;
					if(!$isKIS){
						$row[] = $tran_file_time;
					}
					$row[] = $tran_type;
					if(!$isKIS){
						$row[] = $credit_amount;
					}
					$row[] = $debit_amount;
					$row[] = $tran_details;
					if(!$isKIS){
						$row[] = $bal_after;
					}
					// [2014-1127-1747-49207]
					if($sys_custom['ePayment']['ReportWithAllTransactionRecords']){
						$row[] = $payment_item_remarks;
					}
					$row[] = $ref_code;
					// [2014-1127-1747-49207]
					if($sys_custom['ePayment']['ReportWithAllTransactionRecords']){
						if($sys_custom['ePayment']['HartsPreschool']){
							$row[] = $arr_result[$j]['PaymentMethod'];
							$row[] = $arr_result[$j]['ReceiptRemark'];
						}
						$row[] = $last_modified_username;
					}
					$rows[] = $row;
				}
			} else {
				
				$sql = "SELECT 
								UserLogin,
								ClassName,
								ClassNumber,
								ChineseName,
								EnglishName,
								STRN 
						FROM 
								INTRANET_USER
						WHERE
								UserID = '$user_id'";
				
				$arr_no_record_user = $lpayment->returnArray($sql,5);
				
				if(sizeof($arr_no_record_user)>0){
					list($user_login, $class_name, $class_number, $chi_name, $eng_name) = $arr_no_record_user[0];
					
					if($sys_custom['ePayment']['ReportWithAllTransactionRecords']){
						$content .= "--,";
					}
					$content .= "\"$user_login\",";
					$content .= "\"$class_name\",";
					$content .= "\"$class_number\",";
					$content .= "\"$chi_name\",";
					$content .= "\"$eng_name\",";
					$content .= "\"$i_no_record_exists_msg\",";
					$content .= "\n";
					//$rows[] = array($user_login,$class_name,$class_number,$chi_name,$eng_name,$i_no_record_exists_msg);
					
					$row = array();
					// [2014-1127-1747-49207]
					if($sys_custom['ePayment']['ReportWithAllTransactionRecords']){
						$row[] = "--";
					}
					$row[] = $user_login;
					if($sys_custom['ePayment']['HartsPreschool']){
						$row[] = $arr_no_record_user[0]['STRN'];
					} 
					$row[] = $class_name;
					$row[] = $class_number;
					$row[] = $chi_name;
					$row[] = $eng_name;
					$row[] = $i_no_record_exists_msg;
					$rows[] = $row;
				}
				
			}
			
			$content .= "\n\n";
			$rows[] = "";
			$rows[] = "";
		}
	}
}
intranet_closedb();
		

$display="\"$i_Payment_Menu_Report_StudentBalance\"\n";
$display.="\"$i_general_startdate\",\"$TargetStartDate\"\n";
$display.="\"$i_general_enddate\",\"$TargetEndDate\"\n";
$display.="\n\n";
$utf_display="$i_Payment_Menu_Report_StudentBalance\r\n";
$utf_display.="$i_general_startdate\t$TargetStartDate\r\n";
$utf_display.="$i_general_enddate\t$TargetEndDate\r\n";
$utf_display.="\r\n\r\n";


$timestamp = time();
$filename = "student_balance_report_".$timestamp.".csv";
$file_content .= $content;

$export_content = $display.$lexport->GET_EXPORT_TXT($rows, $exportColumn, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=0);
$lexport->EXPORT_FILE($filename, $export_content);
?>