<?php
// editing by 
########################################### Change Log ###################################################
#
# 2010-02-10 by Carlos: Add border line to table
#
##########################################################################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/fileheader.php");

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

intranet_opendb();
$lpayment = new libpayment();

$ClassName = trim(stripslashes(urldecode($_REQUEST['ClassName'])));
$Keyword = trim(stripslashes(urldecode($_REQUEST['keyword'])));

$namefield = getNameFieldWithClassNumberByLang("u.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
        #$archive_namefield = " IF(c.ChineseName IS NULL,c.EnglishName,c.ChineseName)";
        $archive_namefield="au.ChineseName";
}else  $archive_namefield ="au.EnglishName";
# $archive_namefield = "IF(c.EnglishName IS NULL,c.ChineseName,c.EnglishName)";

# date range
$today_ts = strtotime(date('Y-m-d'));
if($FromDate=="")
        $FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
if($ToDate=="")
        $ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

$date_cond = " AND ppdr.DateInput between '$FromDate' and '$ToDate 23:59:59' ";

if (trim($ClassName) != "") 
	$user_cond .= " AND (u.ClassName like '%".$ClassName."%') ";

$sql  = "SELECT
						CASE 
							WHEN ppdr.InvoiceNumber Is NOT NULL AND ppdr.InvoiceNumber <> '' THEN ppdr.InvoiceNumber 
							ELSE '".$Lang['ePayment']['NoInvoiceNumber']."' 
						END as InvoiceNumber,
						IF(u.UserID IS NULL,CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), IF(au.UserID IS NULL AND u.UserID IS NULL,'<font color=red>*</font>',$namefield)) as Name,
						IF(u.UserID IS NULL,CONCAT('<i>',au.ClassName,'</i>'),u.ClassName) as ClassName,
						IF(u.UserID IS NULL,CONCAT('<i>',au.ClassNumber,'</i>'),u.ClassNumber) as ClassNumber,
					  ROUND(SUM(ppdr.ItemQty*ppdr.ItemSubTotal),2) AS GrandTotal,
					  potl.RefCode,
					  ppdr.DateInput
					from
					  PAYMENT_PURCHASE_DETAIL_RECORD as ppdr
					  inner join
					  PAYMENT_OVERALL_TRANSACTION_LOG as potl
					  on ppdr.TransactionLogID = potl.LogID
					  LEFT JOIN
					  INTRANET_USER as u
					  on potl.StudentID = u.UserID
					  LEFT JOIN
					  INTRANET_ARCHIVE_USER as au
					  on potl.StudentID = au.UserID
         WHERE
						(
						 u.EnglishName LIKE '%$keyword%' OR
						 u.ChineseName LIKE '%$keyword%' OR
						 u.ClassName LIKE '%$keyword%' OR
						 u.ClassNumber LIKE '%$keyword%' OR
						 au.EnglishName LIKE '%$keyword%' OR
						 au.ChineseName LIKE '%$keyword%' OR
						 au.ClassName LIKE '%$keyword%' OR
						 au.ClassNumber LIKE '%$keyword%' OR 
						 potl.RefCode LIKE '%$keyword%' OR 
						 ppdr.InvoiceNumber LIKE '%$keyword%'  
						)
						$date_cond
						$user_cond 
				Group By
					ppdr.InvoiceNumber, potl.RefCode
					";
//echo $sql; die;
$Result = $lpayment->returnArray($sql);

$x = "<table width=95% border=0  cellpadding=2 cellspacing=0 align='center' class='$css_table'>";
$x .= '<tr>';
$x .= '<td width="10%"><b>'.$i_ClassName.'</b></td>';
$x .= '<td><b>:'.(($ClassName != "")? $ClassName:$i_general_all).'</b></td>';
$x .= '</tr>';
$x .= '<tr>';
$x .= '<td><b>'.$Lang['Payment']['Keyword'].'</b></td>';
$x .= '<td><b>:'.$Keyword.'</b></td>';
$x .= '</tr>';
$x .= '</table>';
$x .= '<hr>';
$x .= "<table width=90% border=0  cellpadding=1 cellspacing=0 align='center' class='eSporttableborder'>";
$x .= "<tr class='eSporttdborder eSportprinttabletitle'>";
$x .= "<td width=15% class='eSporttdborder eSportprinttabletitle'>".$Lang['ePayment']['InvoiceNumber']."</td>\n";
$x .= "<td width=15% class='eSporttdborder eSportprinttabletitle'>".$i_Payment_Field_Username."</td>\n";
$x .= "<td width=5% class='eSporttdborder eSportprinttabletitle'>".$i_UserClassName."</td>\n";
$x .= "<td width=5% class='eSporttdborder eSportprinttabletitle'>".$i_UserClassNumber."</td>\n";
$x .= "<td width=10% class='eSporttdborder eSportprinttabletitle'>".$Lang['ePayment']['GrandTotal']."</td>\n";
$x .= "<td width=20% class='eSporttdborder eSportprinttabletitle'>".$i_Payment_Field_RefCode."</td>\n";
$x .= "<td width=30% class='eSporttdborder eSportprinttabletitle'>".$i_Payment_Field_TransactionTime."</td>\n";
$x .= "</tr>";

for ($i=0; $i< sizeof($Result); $i++) {
	$x .= '<tr>';
	$x .= '<td class=\'eSporttdborder eSportprinttext\'>';
	$x .= $Result[$i]['InvoiceNumber'];
	$x .= '</td>';
	$x .= '<td class=\'eSporttdborder eSportprinttext\'>';
	$x .= $Result[$i]['Name'];
	$x .= '</td>';
	$x .= '<td class=\'eSporttdborder eSportprinttext\'>';
	$x .= (trim($Result[$i]['ClassName'])=='')?'--':$Result[$i]['ClassName'];
	$x .= '</td>';
	$x .= '<td class=\'eSporttdborder eSportprinttext\'>';
	$x .= (trim($Result[$i]['ClassNumber'])=='')?'--':$Result[$i]['ClassNumber'];
	$x .= '</td>';
	$x .= '<td class=\'eSporttdborder eSportprinttext\'>';
	$x .= $Result[$i]['GrandTotal'];
	$x .= '</td>';
	$x .= '<td class=\'eSporttdborder eSportprinttext\'>';
	$x .= (trim($Result[$i]['RefCode'])=='')?'--':$Result[$i]['RefCode'];
	$x .= '</td>';
	$x .= '<td class=\'eSporttdborder eSportprinttext\'>';
	$x .= $Result[$i]['DateInput'];
	$x .= '</td>';
	$x .= '</tr>';
}
$x.="</table>";

include_once($PATH_WRT_ROOT."templates/fileheader.php");
//echo displayNavTitle($i_Payment_Menu_Report_SchoolAccount,'');
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
?>
<table border=0 width=95% cellpadding=2 align=center>
<tr><td class='<?=$css_title?>'><b><?=$Lang['Payment']['POSTransactionReport']?> (<?="$FromDate $i_Profile_To $ToDate"?>)</b></td></tr>
</table>
<?=$x?>
<br>
<table width=90% border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td align=right class='<?=$css_text?>'>
<?="$i_general_report_creation_time : ".date('Y-m-d H:i:s')?>
</td></tr>
</table>
<BR><BR>
<?php
include_once($PATH_WRT_ROOT."templates/filefooter.php");
intranet_closedb();
?>
