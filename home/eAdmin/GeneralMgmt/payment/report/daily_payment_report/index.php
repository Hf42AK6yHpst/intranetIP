<?php
# Editing by 
#####################################
#   2020-11-03 (Ray)    : Add merchant account, payment method
#   2020-07-21 (Ray)    : Added multi gateway
#	2017-12-05 (Carlos) : Display payment item category.
#	2017-10-30 (Carlos) : $sys_custom['ePayment']['DailyPaymentReportLoginID'] - display UserLogin.
#	2017-03-07 (Carlos) [ip.2.5.8.4.1]: If [Display cancelled transactions] is not checked, completely hide the cancelled transaction and its cancel record.
#	2017-02-10 (Carlos) : $sys_custom['ePayment']['HartsPreschool'] - display STRN, Payment Method, Receipt Remark.
#	2015-08-13 (Carlos)	: Default check on [Display cancelled transactions].
#	2015-08-06 (Carlos) : Get/Set date range cookies.
# 	2015-08-05 (Carlos) : Added search keyword.
#	2015-07-07 (Carlos) [ip2.5.6.7.1]: Added [Display cancelled transaction] option and display logic for cancelled transactions.
#	2014-10-13 (Carlos) [ip2.5.5.10.1]: Modified to use date range. Added [Date] column. Added [Sort by] options
#
#	Date:	2013-09-17	Henry
#			Page Created
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "StatisticsReport_DailyPaymentReport";

$use_payment_system = $lpayment->isEWalletDirectPayEnabled();

$linterface = new interface_html();
////////////////////////////////
$lb = new libdbtable("","","");

$itemname_field_width = 40;
$extra_field_count = 0;
$extra_field_count2 = 0;

if($sys_custom['ePayment']['MultiPaymentGateway']) {
	$service_provider_list = $lpayment->getPaymentServiceProviderMapping(true, true);
}

$x="<table width=\"96%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
$x.="<tr class=\"tablebluetop\">";
$x.="<td class=\"tabletopnolink\">".$Lang['General']['Date']."</td>";
if($sys_custom['ePayment']['DailyPaymentReportLoginID']){
	$x.="<td class=\"tabletopnolink\">".$i_UserLogin."</td>";
	$extra_field_count += 1;
}
$x.="<td class=\"tabletopnolink\">".$i_UserClassName."</td>";
$x.="<td class=\"tabletopnolink\">".$Lang['General']['ClassNumber']."</td>";
if($sys_custom['ePayment']['HartsPreschool']){
	$x.="<td class=\"tabletopnolink\">".$i_STRN."</td>";
	$extra_field_count += 1;
}
$x.="<td class=\"tabletopnolink\">".$i_UserStudentName."</td>";
$x.="<td class=\"tabletopnolink\">".$Lang['ePayment']['PaymentItem'].' ('.$i_Payment_Field_PaymentCategory.')'."</td>";
if(!$use_payment_system) {
	if ($sys_custom['ePayment']['PaymentMethod']) {
		$extra_field_count2 += 1;
		$x .= "<td class=\"tabletopnolink\">" . $Lang['ePayment']['PaymentMethod'] . "</td>";
	}
}

if($sys_custom['ePayment']['MultiPaymentGateway']) {
	$extra_field_count += 2;
	$x.="<td class=\"tabletopnolink\">".$Lang['ePayment']['PaymentGateway']."</td>";
	$x.="<td class=\"tabletopnolink\">".$Lang['ePayment']['MerchantAccountName']."</td>";
}
$x.="<td class=\"tabletopnolink\">".$Lang['ePayment']['Amount']."</td>";
if($sys_custom['ePayment']['HartsPreschool']){
	$extra_field_count2 += 2;
	$x.="<td class=\"tabletopnolink\">".$Lang['ePayment']['PaymentMethod']."</td>";
	$x.="<td class=\"tabletopnolink\">".$Lang['ePayment']['PaymentRemark']."</td>";
}
$x.="</tr>";

$use_magic_quote = get_magic_quotes_gpc();

# date range
//$today_ts = strtotime(date('Y-m-d'));
$date_range_cookies = $lpayment->Get_Report_Date_Range_Cookies();
if($FromDate=="" || !intranet_validateDate($FromDate)){
	$FromDate = $date_range_cookies['StartDate']!=''? $date_range_cookies['StartDate'] : date('Y-m-d');
}
if($ToDate=="" || !intranet_validateDate($ToDate)){
	$ToDate = $date_range_cookies['EndDate']!=''? $date_range_cookies['EndDate'] : date('Y-m-d');
}
if($_REQUEST['FromDate'] != '' && $_REQUEST['ToDate']!=''){
	$lpayment->Set_Report_Date_Range_Cookies($_REQUEST['FromDate'], $_REQUEST['ToDate']);
}
//if($ToDate=="")
//	$ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

//$date_cond =" DATE_FORMAT(a.StartDate,'%Y-%m-%d')<='$ToDate' AND DATE_FORMAT(a.EndDate,'%Y-%m-%d')>='$FromDate'";	
if(!isset($SortField)){
	$SortField = 0;
}
if(!isset($SortOrder)){
	$SortOrder = 0;
}

$field_index = 0;	
$SortFieldAry = array();
$SortFieldAry[] = array($field_index++,$Lang['General']['Date']);
if($sys_custom['ePayment']['DailyPaymentReportLoginID']){
	$SortFieldAry[] = array($field_index++,$i_UserLogin);
}
$SortFieldAry[] = array($field_index++,$Lang['StudentAttendance']['ClassName']);
$SortFieldAry[] = array($field_index++,$Lang['General']['ClassNumber']);
if($sys_custom['ePayment']['HartsPreschool']){
	$SortFieldAry[] = array($field_index++,$i_STRN);
}
$SortFieldAry[] = array($field_index++,$Lang['StudentAttendance']['StudentName']);
$SortFieldAry[] = array($field_index++,$Lang['ePayment']['PaymentItem']);
if($sys_custom['ePayment']['MultiPaymentGateway']) {
	$SortFieldAry[] = array($field_index++,$Lang['ePayment']['PaymentGateway']);
}
$SortFieldAry[] = array($field_index++,$Lang['ePayment']['Amount']);
$SortFieldSelect = getSelectByArray($SortFieldAry, ' id="SortField" name="SortField" ', $SortField,0,1);

$SortOrderAry = array();
$SortOrderAry[] = array(0,$Lang['ePayment']['Ascendingly']);
$SortOrderAry[] = array(1,$Lang['ePayment']['Descendingly']);
$SortOrderSelect = getSelectByArray($SortOrderAry, ' id="SortOrder" name="SortOrder" ', $SortOrder,0,1);

if($StudentID && $data_format == 2){
$studentList = "'".implode("','",IntegerSafe($StudentID))."'";
$student_cond =" AND t.StudentID IN ($studentList) ";
}

$order= ($SortOrder=='1'?' DESC':' ASC');

$field_index = 0;
$order_fields = array();
$order_fields[$field_index++] = ' t.TransactionTime '.$order.',c.ClassName,c.ClassNumber+0 ';
if($sys_custom['ePayment']['DailyPaymentReportLoginID']){
	$order_fields[$field_index++] = ' c.UserLogin '.$order.',t.TransactionTime ';
}
$order_fields[$field_index++] = ' c.ClassName '.$order.',c.ClassNumber+0 ';
$order_fields[$field_index++] = ' c.ClassNumber+0 '.$order.',c.ClassName ';
if($sys_custom['ePayment']['HartsPreschool']){
	$order_fields[$field_index++] = ' c.STRN '.$order.',t.TransactionTime ';
}
$order_fields[$field_index++] = ' StudentName '.$order.',t.TransactionTime ';
$order_fields[$field_index++] = ' PaymentItem '.$order.',t.TransactionTime ';
if($sys_custom['ePayment']['MultiPaymentGateway']) {
	$order_fields[$field_index++] = ' pt.Sp ' . $order . ',t.TransactionTime ';
}
$order_fields[$field_index++] = ' t.Amount '.$order.',t.TransactionTime ';

$order_cond = isset($order_fields[$SortField])? $order_fields[$SortField]: $order_fields[0];

//debug_pr($FromDate);
//$date_cond =" (DATE_FORMAT(b.PaidTime,'%Y-%m-%d') BETWEEN '$FromDate' AND '$ToDate') ";	
$date_cond =" AND (DATE_FORMAT(t.TransactionTime,'%Y-%m-%d') BETWEEN '".$lpayment->Get_Safe_Sql_Query($FromDate)."' AND '".$lpayment->Get_Safe_Sql_Query($ToDate)."') ";

	$namefield = getNameFieldByLang("c.");
	/*
	$sql="SELECT DATE_FORMAT(b.PaidTime,'%Y-%m-%d') as PaidDate, 
			IF(c.ClassName IS NULL OR c.ClassName = '','".$Lang['General']['EmptySymbol']."',c.ClassName) AS ClassName, 
			IF(c.ClassNumber IS NULL OR c.ClassNumber = '','".$Lang['General']['EmptySymbol']."', c.ClassNumber) AS ClassNumber, 
			".$namefield." AS StudentName, 
			a.Name AS PaymentItem, 
			b.Amount AS Amount 
			FROM PAYMENT_PAYMENT_ITEM AS a 
			JOIN PAYMENT_PAYMENT_ITEMSTUDENT AS b 
			ON a.ItemID= b.ItemID 
			JOIN INTRANET_USER AS c 
			ON b.StudentID = c.UserID 
			WHERE b.PaidTime IS NOT NULL
			AND ".$date_cond."
			".$student_cond."
			ORDER BY a.DisplayOrder, c.ClassName, c.ClassNumber";
	*/

// Default check on [Display cancelled transactions]
if(!isset($_REQUEST['FromDate'])){
	$IncludeCancelled = 1;
}

//if($IncludeCancelled == 1){
//	$trans_type_cond = " AND t.TransactionType IN (2,6) ";
//}else{
//	$trans_type_cond = " AND t.TransactionType='2' AND b.PaidTime IS NOT NULL  ";
//}
$trans_type_cond = " AND t.TransactionType IN (2,6) ";

$keyword = trim(rawurldecode($keyword));
if($use_magic_quote){
	$keyword = stripslashes($keyword);
}


if($keyword != ''){
	$query_keyword = $lpayment->Get_Safe_Sql_Like_Query($keyword);
	//$query_keyword = str_replace(array('[',']','%','_','\'','"'),array('\[','\]','\%','\_','\\\'','\"'), $keyword);
	$keyword_cond = " AND (c.ChineseName LIKE '%$query_keyword%' OR c.EnglishName LIKE '%$query_keyword%' OR t.Details LIKE '%$query_keyword%')";
}

$payment_gateway_cond = '';
if($payment_gateway != '') {
	$payment_gateway_cond = " AND pt.Sp='$payment_gateway'";
}

	$sql="SELECT DATE_FORMAT(t.TransactionTime,'%Y-%m-%d %H:%i') as PaidDate, ";
	if($sys_custom['ePayment']['DailyPaymentReportLoginID']){
		$sql.=" c.UserLogin,";
	}
	  $sql.="IF(c.ClassName IS NULL OR c.ClassName = '','".$Lang['General']['EmptySymbol']."',c.ClassName) AS ClassName, 
			IF(c.ClassNumber IS NULL OR c.ClassNumber = '','".$Lang['General']['EmptySymbol']."', c.ClassNumber) AS ClassNumber, ";
	if($sys_custom['ePayment']['HartsPreschool']){
		$sql .= "c.STRN,";
	}
	$sql.=" ".$namefield." AS StudentName, 
			CONCAT(IF(t.TransactionType='6',t.Details,IF(a.Name IS NOT NULL,a.Name,t.Details)),IF(cat.CatID IS NOT NULL,CONCAT(' (',cat.Name,')'),'')) AS PaymentItem, 
			t.Amount AS Amount,
			IF(b.PaymentID IS NOT NULL,b.PaymentID,t.RelatedTransactionID) as PaymentID,
			t.TransactionType,
			t.TransactionTime ";
	if($sys_custom['ePayment']['HartsPreschool']){
		/*$sql.=" ,CASE b.PaymentMethod ";
		foreach($sys_custom['ePayment']['PaymentMethodItems'] as $key => $val)
		{
			$sql .= " WHEN '$key' THEN '$val' ";	
		}
		$sql .= " ELSE '".$Lang['ePayment']['NA']."' 
				END as PaymentMethod ";*/
	 	$sql .= " ,b.ReceiptRemark ";
	 }


if($sys_custom['ePayment']['PaymentMethod'] || $sys_custom['ePayment']['HartsPreschool'])
{
	$sql .= ",IF(b.RecordStatus = 0, '', ";
	$sql.=" CASE b.PaymentMethod ";
	foreach($sys_custom['ePayment']['PaymentMethodItems'] as $key => $val)
	{
		$sql .= " WHEN '$key' THEN '$val' ";
	}
	if($sys_custom['ttmss_ePayment']) {
		$sql .= "
		ELSE
            CASE
                 WHEN b.NoticePaymentMethod='PayAtSchool' THEN '".$Lang['eNotice']['PaymentNoticePayAtSchool']."' 
                 WHEN b.NoticePaymentMethod <> '' THEN a.NoticePaymentMethod 
                 ELSE '".$Lang['ePayment']['NA']."' 
            END 
		END) as PaymentMethod, ";
	} else {
		$sql .= "   ELSE '" . $Lang['ePayment']['NA'] . "' 
			        END) as PaymentMethod";
	}
}


$more_join = '';
if($sys_custom['ePayment']['MultiPaymentGateway']) {
	$date_cond2 =" AND (DATE_FORMAT(pt.InputDate,'%Y-%m-%d') BETWEEN '".$lpayment->Get_Safe_Sql_Query($FromDate)."' AND '".$lpayment->Get_Safe_Sql_Query($ToDate)."') ";

	$more_join .= 'LEFT JOIN PAYMENT_TNG_TRANSACTION as pt ON pt.PaymentID=IF(b.PaymentID IS NOT NULL,b.PaymentID,t.RelatedTransactionID) '.$date_cond2;
	$sql .= " ,pt.Sp";
}
    $sql .= ",a.ItemID";
	$sql.=" FROM PAYMENT_OVERALL_TRANSACTION_LOG as t 
			LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT AS b ON b.StudentID=t.StudentID AND ((t.TransactionType=6 AND t.RelatedTransactionID=b.ItemID) OR (t.TransactionType!=6 AND t.RelatedTransactionID=b.PaymentID))
			LEFT JOIN PAYMENT_PAYMENT_ITEM AS a ON a.ItemID= b.ItemID 
			LEFT JOIN PAYMENT_PAYMENT_CATEGORY as cat ON cat.CatID=a.CatID 
			LEFT JOIN INTRANET_USER AS c ON t.StudentID = c.UserID
			$more_join
			WHERE 1 $trans_type_cond $date_cond $student_cond $keyword_cond $payment_gateway_cond
			ORDER BY $order_cond";
			
	$temp = $lpayment->returnArray($sql);
	
	// use PaymentID as key, classify paid transactions and cancelled transactions into two data set, for checking which paid transaction should be skipped later time
	$paymentIdToRecord = array();
	$cancelledPaymentIdToRecord = array();
	for($i=0;$i<count($temp);$i++){
		if($temp[$i]['TransactionType'] == 2){ // paid
			$paymentIdToRecord[$temp[$i]['PaymentID']] = $temp[$i];
		}else if($temp[$i]['TransactionType'] == 6){ // cancelled
			$cancelledPaymentIdToRecord[$temp[$i]['PaymentID']] = $temp[$i];
		}
	}
	
	if(sizeof($temp)>0){
		$j=0;
		$total= 0;
		foreach($temp as $item_id =>$values){
			$tmp_payment_id = $values['PaymentID'];
			$tmp_trans_type = $values['TransactionType'];
			$tmp_trans_time = $values['TransactionTime'];
			// if a payment item transaction has later time cancelled transaction, it can be skipped
			if($IncludeCancelled!=1 && $tmp_trans_type == 2 && isset($cancelledPaymentIdToRecord[$tmp_payment_id]) && $tmp_trans_time < $cancelledPaymentIdToRecord[$tmp_payment_id]['TransactionTime']){
				continue;
			}
			
			if($IncludeCancelled != 1 && $tmp_trans_type == 6){
				continue;
			}
			
			$sign = $IncludeCancelled == 1 && $tmp_trans_type == 6 ? -1 : 1;
			
			$record_count += 1;
			$css =$j%2==0?"tablebluerow1 tabletext":"tablebluerow2 tabletext";
			$j++;
			$tempDate = $values['PaidDate'];
			$tempClassName = $values['ClassName'];
			$tempClassNumber  = $values['ClassNumber'];
			$tempStudentName= $values['StudentName'];
			$tempPaymentItem  = $values['PaymentItem'];
			if($sign == -1) {
				$tempAmount = "-$" . (1*$values['Amount']);
			} else {
				$tempAmount = "$" . (1*$values['Amount']);
			}
			$total  += ($sign * $values['Amount']);
			
			$x.="<tr class=\"$css\">";
			$x.="<td class=\"tabletext\">$tempDate</td>";
			if($sys_custom['ePayment']['DailyPaymentReportLoginID']){
				$x.="<td class=\"tabletext\">".$values['UserLogin']."</td>";
			}
			$x.="<td class=\"tabletext\">$tempClassName</td>";
			$x.="<td class=\"tabletext\">$tempClassNumber</td>";
			if($sys_custom['ePayment']['HartsPreschool']){
				$x.="<td class=\"tabletext\">".$values['STRN']."</td>";
			}
			$x.="<td class=\"tabletext\">$tempStudentName</td>";
			$x.="<td class=\"tabletext\">$tempPaymentItem</td>";
			if(!$use_payment_system) {
				if ($sys_custom['ePayment']['PaymentMethod']) {
					$x .= "<td class=\"tabletext\">" . $values['PaymentMethod'] . "</td>";
				}
			}
			if($sys_custom['ePayment']['MultiPaymentGateway']) {
				$x.="<td class=\"tabletext\">".strtoupper($values['Sp'])."</td>";

				if($values['ItemID'] != "") {
					$item_merchant_accounts = $lpayment->returnPaymentItemMultiMerchantAccountID($values['ItemID']);
					$merchant_account_in_use = $lpayment->getMerchantAccounts(array("AccountID" => $item_merchant_accounts, "ServiceProvider" => $values['Sp']));
					$x .= "<td class=\"tabletext\">" . $merchant_account_in_use[0]['AccountName'] . "</td>";
				} else {
					$x .= "<td class=\"tabletext\"></td>";
				}
			}

			$x.="<td class=\"tabletext\">$tempAmount</td>";
			if($sys_custom['ePayment']['HartsPreschool']){
				$x.="<td class=\"tabletext\">".$values['PaymentMethod']."</td>";
				$x.="<td class=\"tabletext\">".$values['ReceiptRemark']."</td>";
			}
			$x.="</tr>";
		}
	}
	if($record_count == 0){ # no record
		$x.="<tr class=\"tablebluerow2 tabletext\"><td class=\"tabletext\" colspan=\"".(6+$extra_field_count+$extra_field_count2)."\" align=\"center\" height=\"40\" style=\"vertical-align:middle\">$i_no_record_exists_msg</td></tr>";
	}

$css ="tablebluebottom";
	$x.="<tr class=\"$css\"><td class=\"tabletext\" colspan=\"".(5+$extra_field_count)."\" align=right>$i_Payment_SchoolAccount_Total:</td><td class=\"tabletext\" ".($extra_field_count2>0?" colspan=\"".($extra_field_count2+1)."\" ":"").">$".number_format((float)$total, 2, '.', '')."</td></tr>";
	
$x.="</table>";

$display=$x;

$toolbar = $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
$toolbar2 = $linterface->GET_LNK_EXPORT("javascript:exportPage(document.form1,'export.php?FromDate=".urlencode($FromDate)."&ToDate=".urlencode($ToDate)."&data_format=".urlencode($data_format)."&SortField=".urlencode($SortField)."&SortOrder=".urlencode($SortOrder)."&ClassName=".urlencode($ClassName)."&StudentID=".($StudentID?implode(',',IntegerSafe($StudentID)):"")."')","","","","",0);
////////////////////////////////

$lclass = new libclass();
$button_select = $i_status_all;

# Class Menu #
$select_class = $lclass->getSelectClass("name=ClassName onChange=removeStudent();this.form.action='';this.form.target='';this.form.submit()",($data_format == 2?$ClassName:""));

//$select_class = $lclass->getSelectClass("name=ClassName",$ClassName,1);

$TAGS_OBJ[] = array($Lang['ePayment']['DailyPaymentReport'], "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.css" type="text/css" />
<script>
function showNotice(obj){

	obj.form.action='';
	obj.form.target='';
	obj.form.submit();
}
	
	function checkDate(obj){
 		if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
		return true;
	}
	
    function checkForm(formObj){
		if(formObj==null)return false;
			fromV = formObj.FromDate;
			objClass = formObj.ClassName;
			if(!checkDate(fromV)){
					return false;
			}
			if(!checkDate(formObj.ToDate)){
					return false;
			}
			if(formObj.FromDate.value > formObj.ToDate.value){
				alert("<?=$Lang['General']['JS_warning']['InvalidDateRange']?>");
				return false;
			}
			else if(document.getElementById('StudentID') != null && document.getElementById('StudentID').selectedIndex == -1 && document.getElementById('data_format2').checked){
				alert("<?=$Lang['StudentAttendance']['PleaseSelectAtLeastOneStudent']?>");
				return false;
			}
			return true;
	}
	
	function submitForm(obj){
        if(checkForm(obj))
        	obj.submit();
    }
     
    function openPrintPage()
	{
		newWindow("print.php?FromDate=<?=urlencode($FromDate)?>&ToDate=<?=urlencode($ToDate)?>&data_format=<?=urlencode($data_format)?>&SortField=<?=urlencode($SortField)?>&SortOrder=<?=urlencode($SortOrder)?>&ClassName=<?=urlencode($ClassName)?>&StudentID=<?=($StudentID?implode(',',IntegerSafe($StudentID)):"")?>&IncludeCancelled=<?=urlencode($IncludeCancelled)?>&keyword=<?=rawurlencode(trim($keyword))?>&payment_gateway=<?=urlencode($payment_gateway)?>",10);
	}
	
	function exportPage(obj,url){
		old_url = obj.action;
        obj.action=url;
        obj.submit();
        obj.action = old_url;
	        
	}
	function removeStudent(){
	if(document.getElementById('StudentID') != null)
		document.getElementById('StudentID').selectedIndex ='-1';
	}
/*
$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
$(document).ready(function(){ 
	$('input#FromDate').datepick({
		dateFormat: 'yy-mm-dd',
		dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		changeFirstDay: false,
		firstDay: 0
		});
	$('input#ToDate').datepick({
		dateFormat: 'yy-mm-dd',
		dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		changeFirstDay: false,
		firstDay: 0
		});
});
*/
function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}
</script>
<br />
<form name="form1" action="" method="get">
<!-- date range -->
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?=$i_Payment_Menu_PrintPage_AddValueReport_Date?></td>
		<td valign="top" class="tabletext" width="70%">
			<?=$Lang['General']['From'].'&nbsp;'.$linterface->GET_DATE_PICKER("FromDate",$FromDate)?>
			<span class="tabletextremark">(yyyy-mm-dd)</span>
			<?=$Lang['General']['To'].'&nbsp;'.$linterface->GET_DATE_PICKER("ToDate",$ToDate)?>
			<span class="tabletextremark">(yyyy-mm-dd)</span>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['ePayment']['SelectMode']?></td>
		<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
			<input type="radio" name="data_format" value="1" id="data_format1" onChange="$('.tbody_class').hide();" <?=($data_format =="" || $data_format == 1?"checked":"")?>><label for="data_format1"><?=$Lang['ePayment']['School']?></label> 
			<input type="radio" name="data_format" value="2" id="data_format2" onChange="$('.tbody_class').show();" <?=($data_format == 2?"checked":"")?>><label for="data_format2"><?=$Lang['Identity']['Student']?></label>
		</td>
	</tr>
	<?//if ($data_format == 2) {?>
	<tr class="tbody_class" style="<?=($data_format == 2?"":"display:none")?>">
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$i_UserClassName?>
		</td>
		<td class="tabletext" width="70%">
			<?=$select_class?>
		</td>
	</tr>
<?//}
	if ($ClassName != "" && $data_format == 2) {
		//$select_student = $lclass->getStudentSelectByClass($ClassName,'name="StudentID[]" id="StudentID" multiple size="10" style="min-width:150px; width:200px;"');
		$select_student = $lpayment->getReceiptStudentSelectByClass($ClassName,'name="StudentID[]" id="StudentID" multiple size="10" style="min-width:150px; width:200px;"',$StudentID);
?>
	<tr class="tbody_class">
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$i_UserStudentName?>
		</td>
		<td class="tabletext" width="70%">
			<?=$select_student?>
			<?= $linterface->GET_BTN($button_select_all, "button", "javascript:SelectAll(document.getElementById('StudentID'))"); ?>
		</td>
	</tr>
<?
	}
?>

    <?php if($sys_custom['ePayment']['MultiPaymentGateway']) { ?>
    <tr>
        <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['ePayment']['PaymentGateway']?></td>
        <td valign="top" nowrap="nowrap" class="tabletext" width="70%">
            <?=$linterface->GET_SELECTION_BOX($service_provider_list, ' id="payment_gateway" name="payment_gateway" ',  "-- ".$button_select." --", $payment_gateway, false);?>
        </td>
    </tr>
    <?php } ?>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['Payment']['Keyword']?></td>
		<td valign="top" nowrap="nowrap" class="tabletext" width="70%"><input type="text" id="keyword" name="keyword" value="<?=escape_double_quotes($keyword)?>" /></td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['ePayment']['SortBy']?></td>
		<td valign="top" nowrap="nowrap" class="tabletext" width="70%"><?=$SortFieldSelect.'&nbsp;'.$SortOrderSelect?></td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['General']['Others']?>
		</td>
		<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
			<input type="checkbox" id="IncludeCancelled" name="IncludeCancelled" value="1" <?=$IncludeCancelled? 'checked="checked"':''?>>
			<label for="IncludeCancelled"><?=$Lang['ePayment']['DisplayCancelledTransactions']?></label>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	</tr>
	<tr>
		<td colspan="2" class="tabletext" align="center">
			<?= $linterface->GET_ACTION_BTN($button_submit, "button", "submitForm(document.form1);","submit2") ?>
		</td>
	</tr>
</table>
</form>

<br />
<?if(!$data_format || $data_format == 1 || $StudentID || $ClassName==""){?>
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?> <?=$toolbar2?></td>
	</tr>
</table>
<?=$display?>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="tabletext" align="right"><?="$i_general_report_creation_time : ".date('Y-m-d H:i:s')?></td>
	</tr>
</table>
<?}?>
<br />
<?
$linterface->LAYOUT_STOP(); 
intranet_closedb();
?>