<?php
# Editing by 

#####################################
#   2020-11-03 (Ray)    : Add merchant account, payment method
#   2020-07-21 (Ray)    : Added multi gateway
#	2017-12-05 (Carlos) : Display payment item category.
#	2017-10-30 (Carlos) : $sys_custom['ePayment']['DailyPaymentReportLoginID'] - display UserLogin.
#	2017-03-07 (Carlos) [ip.2.5.8.4.1]: If [Display cancelled transactions] is not checked, completely hide the cancelled transaction and its cancel record.
#	2017-02-10 (Carlos) : $sys_custom['ePayment']['HartsPreschool'] - display STRN, Payment Method, Receipt Remark.
#	2015-08-05 (Carlos) : Added search keyword.
# 	2015-07-07 (Carlos) [ip2.5.6.7.1]: Added [Display cancelled transaction] option and display logic for cancelled transactions.
#	2015-02-26 (Carlos): $sys_custom['ePayment']['ReportWithSchoolName'] - display school name.
#	2014-10-13 (Carlos) [ip2.5.5.10.1]: Modified to use date range. Added [Date] column. Added [Sort by] options
#	Date:	2013-09-18	Henry
#			Page Created
#
#####################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/lib.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();

$use_payment_system = $lpayment->isEWalletDirectPayEnabled();

$linterface = new interface_html();

$extra_field_count = 0;
$extra_field_count2 = 0;

$x="<table width='90%' border='0' cellspacing='0' cellpadding='5' align='center' class='eSporttableborder'>";
$x.="<thead>";
$x.="<tr>";
$x.="<td class='eSporttdborder eSportprinttabletitle'>".$Lang['General']['Date']."</td>";
if($sys_custom['ePayment']['DailyPaymentReportLoginID']){
	$x.="<td class='eSporttdborder eSportprinttabletitle'>".$i_UserLogin."</td>";
	$extra_field_count += 1;
}
$x.="<td class='eSporttdborder eSportprinttabletitle'>".$i_UserClassName."</td>";
$x.="<td class='eSporttdborder eSportprinttabletitle'>".$Lang['General']['ClassNumber']."</td>";
if($sys_custom['ePayment']['HartsPreschool']){
	$x.="<td class='eSporttdborder eSportprinttabletitle'>".$i_STRN."</td>";
	$extra_field_count += 1;
}
$x.="<td class='eSporttdborder eSportprinttabletitle'>".$i_UserStudentName."</td>";
$x.="<td class='eSporttdborder eSportprinttabletitle'>".$Lang['ePayment']['PaymentItem'].' ('.$i_Payment_Field_PaymentCategory.')'."</td>";
if(!$use_payment_system) {
	if ($sys_custom['ePayment']['PaymentMethod']) {
		$extra_field_count2 += 1;
		$x .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $Lang['ePayment']['PaymentMethod'] . "</td>";
	}
}
if($sys_custom['ePayment']['MultiPaymentGateway']) {
	$extra_field_count += 2;
	$x.="<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['ePayment']['PaymentGateway']."</td>";
	$x.="<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['ePayment']['MerchantAccountName']."</td>";
}
$x.="<td class='eSporttdborder eSportprinttabletitle'>".$Lang['ePayment']['Amount']."</td>";
if($sys_custom['ePayment']['HartsPreschool']){
	$extra_field_count2 += 2;
	$x.="<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['ePayment']['PaymentMethod']."</td>";
	$x.="<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['ePayment']['PaymentRemark']."</td>";
}
$x.="</tr>";
$x.="</thead>";
$x.="<tbody>";

# date range
//$today_ts = strtotime(date('Y-m-d'));
//if($FromDate=="")
//	$FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
//if($ToDate=="")
//	$ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));
//
//$today = date('Y-m-d');
//	
//$date_cond =" DATE_FORMAT(a.StartDate,'%Y-%m-%d')<='$ToDate' AND DATE_FORMAT(a.EndDate,'%Y-%m-%d')>='$FromDate'";		

if($StudentID && $data_format == 2){
$studentList = "'".implode("','", explode(",",$StudentID))."'";
$student_cond =" AND t.StudentID IN ($studentList) ";
}
$cat_cond = $CatID==""?"":" AND a.CatID='$CatID' ";

$order= ($SortOrder=='1'?' DESC':' ASC');

$field_index = 0;	
$order_fields = array();
$order_fields[$field_index++] = ' t.TransactionTime '.$order.',c.ClassName,c.ClassNumber+0 ';
if($sys_custom['ePayment']['DailyPaymentReportLoginID']){
	$order_fields[$field_index++] = ' c.UserLogin '.$order.',t.TransactionTime ';
}
$order_fields[$field_index++] = ' c.ClassName '.$order.',c.ClassNumber+0 ';
$order_fields[$field_index++] = ' c.ClassNumber+0 '.$order.',c.ClassName ';
if($sys_custom['ePayment']['HartsPreschool']){
	$order_fields[$field_index++] = ' c.STRN '.$order.',t.TransactionTime ';
}
$order_fields[$field_index++] = ' StudentName '.$order.',t.TransactionTime ';
$order_fields[$field_index++] = ' a.Name '.$order.',t.TransactionTime ';
if($sys_custom['ePayment']['MultiPaymentGateway']) {
	$order_fields[$field_index++] = ' pt.Sp ' . $order . ',t.TransactionTime ';
}
$order_fields[$field_index++] = ' t.Amount '.$order.',t.TransactionTime ';

$order_cond = isset($order_fields[$SortField])? $order_fields[$SortField]: $order_fields[0];

//debug_pr($FromDate);
//$date_cond =" (DATE_FORMAT(b.PaidTime,'%Y-%m-%d') BETWEEN '$FromDate' AND '$ToDate') ";
$date_cond =" AND (DATE_FORMAT(t.TransactionTime,'%Y-%m-%d') BETWEEN '$FromDate' AND '$ToDate') ";

//if($IncludeCancelled == 1){
//	$trans_type_cond = " AND t.TransactionType IN (2,6) ";
//}else{
//	$trans_type_cond = " AND t.TransactionType='2' AND b.PaidTime IS NOT NULL  ";
//}
$trans_type_cond = " AND t.TransactionType IN (2,6) ";

$use_magic_quote = get_magic_quotes_gpc();
$keyword = trim(rawurldecode($keyword));
if($use_magic_quote){
	$keyword = stripslashes($keyword);
}

if($keyword != ''){
	$query_keyword = $lpayment->Get_Safe_Sql_Like_Query($keyword);
	$keyword_cond = " AND (c.ChineseName LIKE '%$query_keyword%' OR c.EnglishName LIKE '%$query_keyword%' OR t.Details LIKE '%$query_keyword%')";
}

$payment_gateway_cond = '';
if($payment_gateway != '') {
	$payment_gateway_cond = " AND pt.Sp='$payment_gateway'";
}

	$namefield = getNameFieldByLang("c.");
	$sql="SELECT DATE_FORMAT(t.TransactionTime,'%Y-%m-%d %H:%i') as PaidDate, ";
	if($sys_custom['ePayment']['DailyPaymentReportLoginID']){
		$sql.=" c.UserLogin,";
	}
	$sql.=" IF(c.ClassName IS NULL OR c.ClassName = '','".$Lang['General']['EmptySymbol']."',c.ClassName) AS ClassName, 
			IF(c.ClassNumber IS NULL OR c.ClassNumber = '','".$Lang['General']['EmptySymbol']."', c.ClassNumber) AS ClassNumber, ";
	if($sys_custom['ePayment']['HartsPreschool']){
		$sql .= "c.STRN,";
	}
	$sql.=" ".$namefield." AS StudentName, 
			CONCAT(IF(t.TransactionType='6',t.Details,IF(a.Name IS NOT NULL,a.Name,t.Details)),IF(cat.CatID IS NOT NULL,CONCAT(' (',cat.Name,')'),'')) AS PaymentItem, 
			t.Amount AS Amount,
			IF(b.PaymentID IS NOT NULL,b.PaymentID,t.RelatedTransactionID) as PaymentID,
			t.TransactionType,
			t.TransactionTime ";
	if($sys_custom['ePayment']['HartsPreschool']){
		/*$sql.=" ,CASE b.PaymentMethod ";
		foreach($sys_custom['ePayment']['PaymentMethodItems'] as $key => $val)
		{
			$sql .= " WHEN '$key' THEN '$val' ";	
		}
		$sql .= " ELSE '".$Lang['ePayment']['NA']."' 
				END as PaymentMethod ";*/
	 	$sql .= " ,b.ReceiptRemark ";
	}

if($sys_custom['ePayment']['PaymentMethod'] || $sys_custom['ePayment']['HartsPreschool'])
{
	$sql .= ",IF(b.RecordStatus = 0, '', ";
	$sql.=" CASE b.PaymentMethod ";
	foreach($sys_custom['ePayment']['PaymentMethodItems'] as $key => $val)
	{
		$sql .= " WHEN '$key' THEN '$val' ";
	}
	if($sys_custom['ttmss_ePayment']) {
		$sql .= "
		ELSE
            CASE
                 WHEN b.NoticePaymentMethod='PayAtSchool' THEN '".$Lang['eNotice']['PaymentNoticePayAtSchool']."' 
                 WHEN b.NoticePaymentMethod <> '' THEN a.NoticePaymentMethod 
                 ELSE '".$Lang['ePayment']['NA']."' 
            END 
		END) as PaymentMethod, ";
	} else {
		$sql .= "   ELSE '" . $Lang['ePayment']['NA'] . "' 
			        END) as PaymentMethod";
	}
}


$more_join = '';
if($sys_custom['ePayment']['MultiPaymentGateway']) {
	$date_cond2 =" AND (DATE_FORMAT(pt.InputDate,'%Y-%m-%d') BETWEEN '".$lpayment->Get_Safe_Sql_Query($FromDate)."' AND '".$lpayment->Get_Safe_Sql_Query($ToDate)."') ";

	$more_join .= 'LEFT JOIN PAYMENT_TNG_TRANSACTION as pt ON pt.PaymentID=IF(b.PaymentID IS NOT NULL,b.PaymentID,t.RelatedTransactionID) '.$date_cond2;
	$sql .= " ,pt.Sp";
}
    $sql .= ",a.ItemID";
	$sql.= " FROM PAYMENT_OVERALL_TRANSACTION_LOG as t 
			LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT AS b ON b.StudentID=t.StudentID AND ((t.TransactionType=6 AND t.RelatedTransactionID=b.ItemID) OR (t.TransactionType!=6 AND t.RelatedTransactionID=b.PaymentID))
			LEFT JOIN PAYMENT_PAYMENT_ITEM AS a ON a.ItemID= b.ItemID 
			LEFT JOIN PAYMENT_PAYMENT_CATEGORY as cat ON cat.CatID=a.CatID 
			LEFT JOIN INTRANET_USER AS c ON t.StudentID = c.UserID 
			$more_join
			WHERE 1 $trans_type_cond $date_cond $student_cond $keyword_cond $payment_gateway_cond
			ORDER BY $order_cond";
			
	$temp = $lpayment->returnArray($sql);
	
	// use PaymentID as key, classify paid transactions and cancelled transactions into two data set, for checking which paid transaction should be skipped later time
	$paymentIdToRecord = array();
	$cancelledPaymentIdToRecord = array();
	for($i=0;$i<count($temp);$i++){
		if($temp[$i]['TransactionType'] == 2){ // paid
			$paymentIdToRecord[$temp[$i]['PaymentID']] = $temp[$i];
		}else if($temp[$i]['TransactionType'] == 6){ // cancelled
			$cancelledPaymentIdToRecord[$temp[$i]['PaymentID']] = $temp[$i];
		}
	}
	
	if(sizeof($temp)>0){
		$j=0;
		$total= 0;
		foreach($temp as $item_id =>$values){
			$tmp_payment_id = $values['PaymentID'];
			$tmp_trans_type = $values['TransactionType'];
			$tmp_trans_time = $values['TransactionTime'];
			// if a payment item transaction has later time cancelled transaction, it can be skipped
			if($IncludeCancelled!=1 && $tmp_trans_type == 2 && isset($cancelledPaymentIdToRecord[$tmp_payment_id]) && $tmp_trans_time < $cancelledPaymentIdToRecord[$tmp_payment_id]['TransactionTime']){
				continue;
			}
			
			if($IncludeCancelled != 1 && $tmp_trans_type == 6){
				continue;
			}
			
			$sign = $IncludeCancelled == 1 && $tmp_trans_type == 6 ? -1 : 1;
			
			$record_count += 1;
			$css =$j%2==0?"attendancepresent":"attendanceouting";
			$j++;
			$tempDate = $values['PaidDate'];
			$tempClassName = $values['ClassName'];
			$tempClassNumber  = $values['ClassNumber'];
			$tempStudentName= $values['StudentName'];
			$tempPaymentItem  = $values['PaymentItem'];
			if($sign == -1) {
				$tempAmount = "-$" . (1*$values['Amount']);
			} else {
				$tempAmount = "$" . (1*$values['Amount']);
			}
			$total  += $sign*$values['Amount'];
			
			$x.="<tr class=\"$css\">";
			$x.="<td class=\"eSporttdborder eSportprinttext\">$tempDate</td>";
			if($sys_custom['ePayment']['DailyPaymentReportLoginID']){
				$x.="<td class=\"eSporttdborder eSportprinttext\">".$values['UserLogin']."</td>";
			}
			$x.="<td class=\"eSporttdborder eSportprinttext\">$tempClassName</td>";
			$x.="<td class=\"eSporttdborder eSportprinttext\">$tempClassNumber</td>";
			if($sys_custom['ePayment']['HartsPreschool']){
				$x.="<td class=\"eSporttdborder eSportprinttext\">".$values['STRN']."</td>";
			}
			$x.="<td class=\"eSporttdborder eSportprinttext\">$tempStudentName</td>";
			$x.="<td class=\"eSporttdborder eSportprinttext\">$tempPaymentItem</td>";
			if(!$use_payment_system) {
				if ($sys_custom['ePayment']['PaymentMethod']) {
					$x .= "<td class=\"eSporttdborder eSportprinttext\">" . $values['PaymentMethod'] . "</td>";
				}
			}
			if($sys_custom['ePayment']['MultiPaymentGateway']) {
				$x.="<td class=\"eSporttdborder eSportprinttext\">".strtoupper($values['Sp'])."</td>";

				if($values['ItemID'] != "") {
					$item_merchant_accounts = $lpayment->returnPaymentItemMultiMerchantAccountID($values['ItemID']);
					$merchant_account_in_use = $lpayment->getMerchantAccounts(array("AccountID" => $item_merchant_accounts, "ServiceProvider" => $values['Sp']));
					$x .= "<td class=\"eSporttdborder eSportprinttext\">" . $merchant_account_in_use[0]['AccountName'] . "</td>";
				} else {
					$x .= "<td class=\"eSporttdborder eSportprinttext\"></td>";
				}
			}
			$x.="<td class=\"eSporttdborder eSportprinttext\">$tempAmount</td>";
			if($sys_custom['ePayment']['HartsPreschool']){
				$x.="<td class=\"eSporttdborder eSportprinttext\">".$values['PaymentMethod']."</td>";
				$x.="<td class=\"eSporttdborder eSportprinttext\">".$values['ReceiptRemark']."&nbsp;</td>";
			}
			$x.="</tr>";
		}
	}
	if($record_count == 0){ # no record
		$x.="<tr><td class=\"eSporttdborder eSportprinttext\" colspan=\"".(6+$extra_field_count+$extra_field_count2)."\" align=\"center\" height=\"40\" style=\"vertical-align:middle\">$i_no_record_exists_msg</td></tr>";
	}

$css ="tablebluebottom";
	$x.="<tr><td class=\"eSporttdborder eSportprinttext\" colspan=\"".(5+$extra_field_count)."\" align=right>$i_Payment_SchoolAccount_Total:</td><td class=\"eSporttdborder eSportprinttext\" ".($extra_field_count2>0?" colspan=\"".($extra_field_count2+1)."\" ":"").">$".number_format((float)$total, 2, '.', '')."</td></tr>";
$x.="</tbody>";
$x.="</table>";

$display=$x;

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

$studentArr = explode(',',$StudentID);
$strStudent = "";
foreach($studentArr as $tempStudent){
$lu = new libuser($tempStudent);
$strStudent .= Get_Lang_Selection($lu->ChineseName,$lu->EnglishName).", ";
}
$strStudent = substr_replace($strStudent ,"",-1);
$strStudent = substr_replace($strStudent ,"",-1);
?>
<style type="text/css" media="print">
thead {display: table-header-group;}
</style>
<!-- date range -->
<table width="90%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<?php
if($sys_custom['ePayment']['ReportWithSchoolName']){
	$school_name = GET_SCHOOL_NAME();
	$x = '<table width="90%" align="center" border="0">
			<tr>
				<td align="center"><h2><b>'.$school_name.'</b></h2></td>
			</tr>
		</table>';
	echo $x;
}
?>
<table width='90%' border='0' cellspacing='0' cellpadding='5' align='center'>
<tr>
	<td><b><?=$Lang['ePayment']['DailyPaymentReport']?> (<?=$Lang['General']['From'].'&nbsp;'.$FromDate.'&nbsp;'.$Lang['General']['To'].'&nbsp;'.$ToDate?>)</b></td>
</tr>
<?if($StudentID && $data_format == 2){?>
<tr><td><B><?=$Lang['StudentAttendance']['ClassName']?></B>: <?=$ClassName?></td></tr>
<tr><td><B><?=$Lang['StudentAttendance']['StudentName']?></B>: <?=$strStudent?></td></tr>
<?}?>
</table>

<?=$display?>
<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td class="eSportprinttext" align="right"><?="$i_general_report_creation_time : ".date('Y-m-d H:i:s')?></td>
	</tr>
</table>

<?php
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>
