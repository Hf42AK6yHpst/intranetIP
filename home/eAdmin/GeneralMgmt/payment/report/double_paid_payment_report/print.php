<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ePayment']['DoublePaidPaymentReport']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();


$display_1 = '';
$display_2 = '';
$display_3 = '';

$format = 'print';
include('double_paid.php');


include_once($PATH_WRT_ROOT."templates/".$LAYOUT_SKIN."/layout/print_header.php");
?>
	<style type="text/css" media="print">
		thead {display: table-header-group;}
	</style>

    <br/>
	<table border=0 width=95% align=center>
		<tr><td class=''><b><?=$Lang['ePayment']['DoublePaidPaymentReport']?></b></td></tr>
	</table>

    <br/>


    <?php if($display_1 != '') { ?>
    <table border=0 width=95% align=center>
        <tr><td class=''><b><?=$i_Payment_Field_PaymentItem?></b></td></tr>
    </table>

    <table width="96%" border="0" cellpadding="2" cellspacing="0" align="center" class="eSporttableborder">
        <thead>
            <td class="eSporttdborder eSportprinttabletitle">#</td>
            <td class="eSporttdborder eSportprinttabletitle"><?=$Lang['ePayment']['TransactionTime']?></td>
            <td class="eSporttdborder eSportprinttabletitle"><?=$i_general_startdate?></td>
            <td class="eSporttdborder eSportprinttabletitle"><?=$i_general_enddate?></td>
            <td class="eSporttdborder eSportprinttabletitle"><?=$i_UserClassName?></td>
            <td class="eSporttdborder eSportprinttabletitle"><?=$i_ClassNumber?></td>
            <td class="eSporttdborder eSportprinttabletitle"><?=$i_UserStudentName?></td>
            <td class="eSporttdborder eSportprinttabletitle"><?=$i_UserLogin?></td>
            <td class="eSporttdborder eSportprinttabletitle"><?=$i_Payment_Field_PaymentCategory?></td>
            <td class="eSporttdborder eSportprinttabletitle"><?=$i_Payment_Field_PaymentItem?></td>
        </thead>

		<?=$display_1?>
    </table>
    <br/>
    <?php } ?>


    <?php if($display_2 != '') { ?>
    <table border=0 width=95% align=center>
        <tr><td class=''><b><?=$Lang['eNotice']['PaymentNotice']?></b></td></tr>
    </table>

    <table width="96%" border="0" cellpadding="2" cellspacing="0" align="center" class="eSporttableborder">
        <thead>
            <td class="eSporttdborder eSportprinttabletitle">#</td>
            <td class="eSporttdborder eSportprinttabletitle"><?=$Lang['ePayment']['TransactionTime']?></td>
            <td class="eSporttdborder eSportprinttabletitle"><?=$i_Notice_DateStart?></td>
            <td class="eSporttdborder eSportprinttabletitle"><?=$i_Notice_DateEnd?></td>
            <td class="eSporttdborder eSportprinttabletitle"><?=$i_UserClassName?></td>
            <td class="eSporttdborder eSportprinttabletitle"><?=$i_ClassNumber?></td>
            <td class="eSporttdborder eSportprinttabletitle"><?=$i_UserStudentName?></td>
            <td class="eSporttdborder eSportprinttabletitle"><?=$i_UserLogin?></td>
            <td class="eSporttdborder eSportprinttabletitle"><?=$i_Notice_NoticeNumber?></td>
            <td class="eSporttdborder eSportprinttabletitle"><?=$i_Notice_Title?></td>
        </thead>

		<?=$display_2?>
    </table>
    <br/>
    <?php } ?>

    <?php if($display_3 != '') { ?>
    <table border=0 width=95% align=center>
        <tr><td class=''><b><?=$i_Payment_Menu_Browse_MissedPPSBill?></b></td></tr>
    </table>

    <table width="96%" border="0" cellpadding="2" cellspacing="0" align="center" class="eSporttableborder">
        <thead>
            <td class="eSporttdborder eSportprinttabletitle">#</td>
            <td class="eSporttdborder eSportprinttabletitle"><?=$Lang['ePayment']['TransactionTime']?></td>
            <td class="eSporttdborder eSportprinttabletitle"><?=$i_UserClassName?></td>
            <td class="eSporttdborder eSportprinttabletitle"><?=$i_ClassNumber?></td>
            <td class="eSporttdborder eSportprinttabletitle"><?=$i_UserStudentName?></td>
            <td class="eSporttdborder eSportprinttabletitle"><?=$i_UserLogin?></td>
            <td class="eSporttdborder eSportprinttabletitle"><?=$i_Payment_Field_RefCode?></td>
        </thead>

		<?=$display_3?>
    </table>
    <br/>
    <?php } ?>

<?php
include_once($PATH_WRT_ROOT."templates/filefooter.php");

intranet_closedb();

?>