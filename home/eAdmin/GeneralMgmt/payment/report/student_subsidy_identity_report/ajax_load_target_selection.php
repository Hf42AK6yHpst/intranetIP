<?php
// Editing by
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	intranet_closedb();
	exit();
}

$linterface = new interface_html();

$ldb = new libdb();
$lclass = new libclass();
$lpayment = new libpayment();

$target = $_REQUEST['target'];
$fieldId = $_REQUEST['fieldId']; // <select> id
$fieldName = $_REQUEST['fieldName']; // <select> name
$academicYearId = $_REQUEST['academicYearId'];
$singleSelection = $_REQUEST['singleSelection']==1;
$displaySize = $_REQUEST['displaySize']? max(5, intval($_REQUEST['displaySize'])) : 8;


if($target=="class") {

	$temp = '<select name="'.$fieldName.'" id="'.$fieldId.'" class="formtextbox" '.($singleSelection?'':'multiple="multiple" size="'.$displaySize.'"').'>';

	$classResult = $lclass->getClassList($academicYearId);

	for($k=0;$k<sizeof($classResult);$k++) {
		$temp .= '<option value="'.$classResult[$k][0].'" ';
		if(!$singleSelection) $temp .= ' selected';
		$temp .= '>'.$classResult[$k][1].'</option>';
	}
	$temp .= '</select>'."\n";
}

if($target == "subsidy") {
	$temp = '<select name="'.$fieldName.'" id="'.$fieldId.'" class="formtextbox" '.($singleSelection?'':'multiple="multiple" size="'.$displaySize.'"').'>';

	$subsidyResult = $lpayment->getSubsidyIdentity(array());

	for($k=0;$k<sizeof($subsidyResult);$k++) {
		$temp .= '<option value="'.$subsidyResult[$k]['IdentityID'].'" ';
		if(!$singleSelection) $temp .= ' selected';
		$temp .= '>'.$subsidyResult[$k]['IdentityName'].'('.$subsidyResult[$k]['Code'].')</option>';
	}
	$temp .= '</select>'."\n";

}

echo $temp;

intranet_closedb();
?>