<?php
// Editing by
/*
 * 2019-08-28 (Ray): Created
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");


intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	intranet_closedb();
	header ("Location: /");
	exit();
}


$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "StudentSubsidyIdentity_Report";

$linterface = new interface_html();

# date range ( 1st of current month till the last day of current month)
$date_range_cookies = $lpayment->Get_Report_Date_Range_Cookies();
if($FromDate == ""){
	$FromDate = $date_range_cookies['StartDate'] != ''? $date_range_cookies['StartDate'] : ((date('Y-m'))."-01");
}
//$FromDate=$FromDate==""?((date('Y-m'))."-01"):$FromDate;
if($ToDate == ""){
	$ToDate = $date_range_cookies['EndDate'] != ''? $date_range_cookies['EndDate'] : (date('Y-m-d',mktime(0, 0, 0, date("m")+1, 0,  date("Y"))));
}
//$ToDate=$ToDate==""?(date('Y-m-d',mktime(0, 0, 0, date("m")+1, 0,  date("Y")))):$ToDate;


$format_array = array(
	array(0,"Web"),
	array(1,"CSV"));
$select_format = getSelectByArray($format_array, "name=format",0,0,1);

$TAGS_OBJ[] = array($Lang['ePayment']['StudentSubsidyIdentityReport'], "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.css" type="text/css" />
<script language="javascript">
    function checkForm(formObj){
        if(formObj==null) return false;
        $('#TargetID_Error').hide();
        fromV = formObj.FromDate;
        toV= formObj.ToDate;
        if(!checkDate(fromV)){
            //formObj.FromDate.focus();
            return false;
        }
        else if(!checkDate(toV)){
            //formObj.ToDate.focus();
            return false;
        }
        var target_obj = document.getElementById('TargetID');
        var target_values = [];
        if(target_obj)
        {
            for(var i=0;i<target_obj.options.length;i++)
            {
                if(target_obj.options[i].selected) target_values.push(target_obj.options[i].value);
            }
        }

        if(target_values.length <= 0)
        {
            $('#TargetID_Error').show();
            return false;
        }

        return true;
    }

    function checkDate(obj){
        if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
        return true;
    }

    function submitForm(obj){
        if(checkForm(obj)) {
            if(obj.format.value!=1){
                obj.target='intranet_popup10';
                newWindow('about:blank', 10);
            }else{
                obj.target='';
            }
            obj.submit();
        }
    }

    $.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
    $(document).ready(function(){
        $('input#FromDate').datepick({
            dateFormat: 'yy-mm-dd',
            dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
            changeFirstDay: false,
            firstDay: 0
        });
        $('input#ToDate').datepick({
            dateFormat: 'yy-mm-dd',
            dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
            changeFirstDay: false,
            firstDay: 0
        });
    });

    var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image();?>';
    var academicYearId = '<?=Get_Current_Academic_Year_ID()?>';
    function targetTypeChanged(obj)
    {
        var selectedTargetType = obj.value;
        if(selectedTargetType == 'class'){
            $('#DivRankTargetDetail').html(loadingImg).load(
                '../common/ajax_load_target_selection.php',
                {
                    'target':selectedTargetType,
                    'academicYearId':academicYearId,
                    'fieldId':'TargetID',
                    'fieldName':'TargetID[]'
                },
                function(data){
                    $('#selectAllTargetBtn').show();
                    $('#DivSelectAllRemark').show();
                }
            );
        }else if(selectedTargetType == 'subsidy'){
            $('#DivRankTargetDetail').html(loadingImg).load(
                '../common/ajax_load_target_selection.php',
                {
                    'target':selectedTargetType,
                    'fieldId':'TargetID',
                    'fieldName':'TargetID[]'
                },
                function(data){
                    $('#selectAllTargetBtn').show();
                    $('#DivSelectAllRemark').show();
                }
            );
        }else{
            $('#DivRankTargetDetail').html('');
            $('#selectAllTargetBtn').hide();
            $('#DivSelectAllRemark').hide();
        }
    }
</script>
<br />
<form name="form1" method="get" action="report.php">
    <table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
        <tr>
            <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
				<?=$Lang['ePayment']['StudentSubsidyReportValidDate']?>
            </td>
            <td valign="top" nowrap="nowrap" class="tabletext" width="70%">
                <input class="textboxnum" type="text" name="FromDate" id="FromDate" value="<?=$FromDate?>">
				<?=$i_Profile_To?>
                <input class="textboxnum" type="text" name="ToDate" id="ToDate" value="<?=$ToDate?>">
                <span class="tabletextremark">(yyyy-mm-dd)</span>
            </td>
        </tr>
        <tr>
            <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
				<?=$Lang['ePayment']['SubsidyIdentity']?> / <?=$Lang['SysMgr']['FormClassMapping']['Class']?>
            </td>
            <td valign="top" nowrap="nowrap" class="tabletext" width="70%" style="padding: 0;">
                <table class="inside_form_table">
                    <tr>
                        <td valign="top">
                            <select name="TargetType" id="TargetType" onchange="targetTypeChanged(this);">
                                <option value="" selected="selected">-- <?=$Lang['General']['PleaseSelect']?> --</option>
                                <option value="subsidy"><?=$Lang['ePayment']['SubsidyIdentity']?></option>
                                <option value="class"><?=$Lang['SysMgr']['FormClassMapping']['Class']?></option>
                            </select>
                        </td>
                        <td>
                            <span id='DivRankTargetDetail'></span>
							<?=$linterface->GET_BTN($Lang['Btn']['SelectAll'], "button", "if(document.getElementById('TargetID')){Select_All_Options('TargetID', true);}return false;","selectAllTargetBtn",' style="display:none;"')?>
                        </td>
                    </tr>
                </table>
				<?=$linterface->Get_Form_Warning_Msg('TargetID_Error', $Lang['eDiscipline']['WarningMsgArr']['SelectTarget'], 'TargetID_Error', false);?>
                <div class="tabletextremark" id="DivSelectAllRemark" style="display:none;"><?=$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']?></div>
            </td>
        </tr>
        <tr>
            <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
				<?=$i_general_Format?>
            </td>
            <td valign="top" nowrap="nowrap" class="tabletext" width="70%">
				<?=$select_format?>
            </td>
        </tr>

        <tr>
            <td colspan="2">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                    <tr>
                        <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
				<?= $linterface->GET_ACTION_BTN($button_submit, "button", "javascript:submitForm(document.form1)") ?>
            </td>
        </tr>
    </table>
</form>

<?php
$linterface->LAYOUT_STOP();
print $linterface->FOCUS_ON_LOAD("form1.FromDate");
intranet_closedb();
?>
