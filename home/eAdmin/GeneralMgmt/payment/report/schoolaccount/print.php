<?php
# using: 

########################################################
#	Date:	2020-04-24 Ray
#			Add last_modify order
#
#	Date:	2019-08-07  Carlos
#			Addd more credit types - top-up of TNG, AlipayHK, FPS and tap & Go.
#
#	Date:	2016-12-12 	Carlos
#			Added transaction date type option.
#
#	Date:	2016-11-03	Carlos
#			Improved payment item type transaction to first priority use PAYMENT_PAYMENT_ITEM.Name as transaction name, 
#			in order to avoid same payment item displayed as several items if payment item has ever changed its name, 
#			and some students paid before and some students paid after name changing. 
#
#	Date:	2016-08-24 (Carlos)
#			$sys_custom['ePayment']['CashDepositMethod'] - added more credit methods [Bank transfer, Cheque deposit].
#
#	Date:	2015-02-26 (Carlos)
#			$sys_custom['ePayment']['ReportWithSchoolName'] - display school name.
#
#	Date:	2013-10-29	Carlos
#			KIS - hide [Income] and [Total Income]
#
#	Date:	2011-11-18	YatWoon
#			Improved: display notice title in the payment item [Case#2011-1115-1426-21072]
#
########################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance'];

$lpayment = new libpayment();
$linterface = new interface_html();


# date range
$today_ts = strtotime(date('Y-m-d'));
if($FromDate=="")
  $FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
if($ToDate=="")
  $ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

if($SearchBy == 2){ // by transaction time
	$date_cond = " IF(a.TransactionType=1 AND c.TransactionID IS NOT NULL,DATE_FORMAT(c.TransactionTime,'%Y-%m-%d')>='$FromDate' AND DATE_FORMAT(c.TransactionTime,'%Y-%m-%d')<='$ToDate',DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')>='$FromDate' AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')<='$ToDate') ";
}else{ // by input time
	$date_cond =" DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')>='$FromDate' AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')<='$ToDate' ";
}

$records_field = $field;
$records_order = $order;

$sql="SELECT 
				a.Amount,
	      if(f.NoticeID is null, IF(a.TransactionType=2 AND e.ItemID IS NOT NULL,e.Name,a.Details), concat(f.Title,' - ',a.Details)),
	      a.TransactionType,
	      c.RecordType,
	      a.TransactionTime
      FROM 
      	PAYMENT_OVERALL_TRANSACTION_LOG AS a
        LEFT JOIN PAYMENT_CREDIT_TRANSACTION as c ON a.RelatedTransactionID = c.TransactionID
        left join PAYMENT_PAYMENT_ITEMSTUDENT as d on d.PaymentID=a.RelatedTransactionID
		left join PAYMENT_PAYMENT_ITEM as e on e.ItemID=d.ItemID
		left join INTRANET_NOTICE as f on f.NoticeID=e.NoticeID
		WHERE $date_cond ORDER BY a.TransactionTime ASC";

$li = new libpayment();
$temp = $li->returnArray($sql,4);


$ary_types = array(1,2,3,6,7,8,9,10,11);

$x="<table width='90%' border='0' cellspacing='0' cellpadding='5' align='center' class='eSporttableborder'>";
$x.="<thead>";
$x.="<tr>";
$x.="<td rowspan='2' valign='middle' class='eSporttdborder eSportprinttabletitle print_header' width='30%' align='center'>$i_Payment_SchoolAccount_Detailed_Transaction</td>";
if(!$isKIS){
	$x.="<td rowspan='2' valign='middle' class='eSporttdborder eSportprinttabletitle print_header' width='15%' align='center'>$i_Payment_SchoolAccount_Deposit</td>";
}
$x.="<td colspan='3' valign='middle' class='eSporttdborder eSportprinttabletitle print_header' width='40%' align='center'>$i_Payment_SchoolAccount_Expenses</td>";
$x.="<td rowspan='2' valign='middle' class='eSporttdborder eSportprinttabletitle print_header' width='15%' align='center'>$i_Form_LastModified</td>";
$x.="</tr>";
$x.="<tr>";
$x.="<td valign='middle' class='eSporttdborder eSportprinttabletitle' align='center'>$i_Payment_SchoolAccount_PresetItem</td>";
$x.="<td valign='middle' class='eSporttdborder eSportprinttabletitle' align='center'>$i_Payment_SchoolAccount_SinglePurchase</td>";
$x.="<td valign='middle' class='eSporttdborder eSportprinttabletitle' align='center' width='12%'>$i_Payment_SchoolAccount_Other</td>";
$x.="</tr>";
$x.="</thead>";
$x.="<tbody>";

$income=0;
$expense_item=0;
$expense_single=0;
$expense_other=0;

$result = array();
for($i=0;$i<sizeof($temp);$i++){
        list($amount,$detail,$type,$credit_transaction_type, $transaction_time) = $temp[$i];
        if(in_array($type,$ary_types)){
                #$result[$type]["$detail"] += $amount+0;
                switch($type){
                        case 1        :
                                       $result[$type][$credit_transaction_type]['amount'] += $amount;
							           $result[$type][$credit_transaction_type]['last_modify'] = $transaction_time;
                                       $income+=$amount+0;
                                       break;

                        case 2        :
                                       $result[$type]["$detail"]['amount'] += $amount+0;
                                       $result[$type]["$detail"]['last_modify'] = $transaction_time;
                                       $expense_item +=$amount+0;
                                       break;

                        case 3        :
                                       $result[$type]["$detail"]['amount'] += $amount+0;
                                       $result[$type]["$detail"]['last_modify'] = $transaction_time;
                                       $expense_single+=$amount+0;
                                       break;

                        case 6        :
                                       $result[$type]["$detail"]['amount'] += $amount+0;
							           $result[$type]["$detail"]['last_modify'] = $transaction_time;
                                       $income+=$amount+0;
                                       break;

                        case 7        :
                                       $result[$type]["all"]['amount'] += $amount;
                                       $result[$type]["all"]['last_modify'] = $transaction_time;
                                       $expense_other +=$amount+0;
                                       break;

                        case 8        :
                                       $result[$type]["all"]['amount'] += $amount;
                                       $result[$type]["all"]['last_modify'] = $transaction_time;
                                       $expense_other +=$amount+0;
                                       break;
                                       
                        case 9        :
                                       $result[$type]["all"]['amount'] += $amount;
                                       $result[$type]["all"]['last_modify'] = $transaction_time;
                                       $expense_other +=$amount+0;
                                       break;
                         
                        case 10        :
                                       $result[$type]["all"]['amount'] += $amount;
                                       $result[$type]["all"]['last_modify'] = $transaction_time;
                                       $expense_other +=$amount+0;
                                       break;
                        
                        case 11        :
                                       $result[$type]["all"]['amount'] += $amount+0;
                                       $result[$type]["all"]['last_modify'] = $transaction_time;
                                       $income+=$amount+0;
                                       break;

                        default        : break;
                }

        }

}

if(sizeof($result)<=0){
        $x.="<tr><td colspan=5 align=center class=\"eSporttdborder eSportprinttext\" height=40 style='vertical-align:middle'>$i_no_record_exists_msg<td></tr>";
}else{
        $count=0;
	$list_data = array();
	if(!$isKIS){
        # deposit
        $list_item = $result[1];
        if(sizeof($list_item)>0){
                foreach($list_item as $credit_type => $temp){
					    $t_total = $temp['amount'];
                        //$css =$count%2==0?"tableContent":"tableContent2";
                        //$css ="eSporttdborder eSportprinttext";
                        //$count++;
                        switch ($credit_type)
                        {
                                case 1: $name = $i_Payment_Credit_TypePPS; break;
                                case 2: $name = $i_Payment_Credit_TypeCashDeposit; break;
                                case 3: $name = $i_Payment_Credit_TypeAddvalueMachine; break;
                                case 4: $name = $Lang['ePayment']['AutoPay'];break;
	                        	case 6: $name = $Lang['ePayment']['BankTransfer'];break;
	                        	case 7: $name = $Lang['ePayment']['ChequeDeposit'];break;
	                        	case 8: $name = $Lang['ePayment']['TNGTopUp'];break;
		                        case 9: $name = $Lang['ePayment']['AlipayTopUp'];break;
		                        case 10:$name = $Lang['ePayment']['FPSTopUp'];break;
		                        case 11:$name = $Lang['ePayment']['TapAndGoTopUp'];break;
							    case 12:$name = $Lang['ePayment']['VisaMasterTopUp'];break;
							    case 13:$name = $Lang['ePayment']['WeChatTopUp'];break;
							    case 14:$name = $Lang['ePayment']['AlipayCNTopUp'];break;
                                default : $name = $i_Payment_Credit_TypeUnknown . "(Type=$credit_type)";

                        }
					    if(!$isKIS) {
						    $list_data[] = array($name, "$" . number_format($t_total, 2), "", "", "", $temp['last_modify']);
				    	} else {
							$list_data[] = array($name, "", "", "", $temp['last_modify']);
						}
                        //$x.="<tr><td class='$css'>$name&nbsp;</td>";
                     //if(!$isKIS){
					//	$x.="<td class='$css'>$".number_format($t_total,2)."</td>";
                     //}
                    //$x.="<td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td></tr>";
                }
                //$x.="<tr><td colspan=4 class='tableContent'>&nbsp;</td></tr>";
        }
	}
        # payment cancellation
        $list_item = $result[6];
        if(sizeof($list_item)>0){
                foreach($list_item as $name => $temp){
					    $t_total = $temp['amount'];
					    if(!$isKIS) {
							$list_data[] = array($name, "$" . number_format($t_total, 2), "", "", "", $temp['last_modify']);
						} else {
							$list_data[] = array($name, "", "", "", $temp['last_modify']);
						}
                        //$css =$count%2==0?"tableContent":"tableContent2";
                        //$css ="eSporttdborder eSportprinttext";
                        //$count++;
                        //$x.="<tr><td class='$css'>$name&nbsp;</td>";
                        //if(!$isKIS){
						//	$x.="<td class='$css'>$".number_format($t_total,2)."</td>";
                        //}
						//$x.="<td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td></tr>";
                }
        }
        
        # POS Void Transaction Refund
				$list_item = $result[11];
				if(sizeof($list_item)>0){
					$item_name = $Lang['ePOS']['ReportVoidTitle'];
					$s_total = $list_item['all']['amount'];
					if(!$isKIS) {
						$list_data[] = array($item_name, "$".number_format($s_total, 2), "", "", "", $list_item['all']['last_modify']);
					} else {
						$list_data[] = array($item_name, "", "", "", $list_item['all']['last_modify']);
					}
					//$css ="eSporttdborder eSportprinttext";
					//$x.="<tr>
					//				<td class='$css'>$item_name&nbsp;</td>";
					//	if(!$isKIS){
					//		  $x .="<td class='$css'>$".number_format($s_total,2)."</td>";
					//	}
					//		  $x .="<td class='$css'>&nbsp;</td>
					//				<td class='$css'>&nbsp;</td>
					//				<td class='$css'>&nbsp;</td></tr>";
				}
        //$x.="<tr><td colspan=4 class='tableContent'>&nbsp;</td></tr>";

        # payment item
        $list_item = $result[2];
        if(sizeof($list_item)>0){
                foreach($list_item as $name => $temp){
                        $t_total = $temp['amount'];
					    if(!$isKIS) {
						    $list_data[] = array($name, "", "$" . number_format($t_total, 2), "", "", $temp['last_modify']);
					    } else {
							$list_data[] = array($name, "$" . number_format($t_total, 2), "", "", $temp['last_modify']);
						}
                        //$css =$count%2==0?"tableContent":"tableContent2";
                        //$css ="eSporttdborder eSportprinttext";
                        //$count++;
                        //$x.="<tr><td class='$css'>$name&nbsp;</td>";
                        //if(!$isKIS){
						//	$x.="<td class='$css'>&nbsp;</td>";
                        //}
						//$x.="<td class='$css'>$".number_format($t_total,2)."</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td></tr>";
                }
        }

        # single purchase
        $list_item = $result[3];
        if(sizeof($list_item)>0){
                foreach($list_item as $name => $temp){
                        $t_total = $temp['amount'];
					    if(!$isKIS) {
						    $list_data[] = array($name, "", "", "$" . number_format($t_total, 2), "", $temp['last_modify']);
					    } else {
							$list_data[] = array($name, "", "$" . number_format($t_total, 2), "", $temp['last_modify']);
						}
                        //$css =$count%2==0?"tableContent":"tableContent2";
                        //$css ="eSporttdborder eSportprinttext";
                        //$count++;
                        //$x.="<tr><td class='$css'>$name&nbsp;</td>";
                       	//if(!$isKIS){
						//	$x.="<td class='$css'>&nbsp;</td>";
                       	//}
						//$x.="<td class='$css'>&nbsp;</td><td class='$css'>$".number_format($t_total,2)."</td><td class='$css'>&nbsp;</td></tr>";
                }
        }
        //$x.="<tr><td colspan=4 class='tableContent'>&nbsp;</td></tr>";

        # refund
        $list_item = $result[7];
        if(sizeof($list_item)>0){
                  $item_name = $i_Payment_action_refund;
                  //$css =$count%2==0?"tableContent":"tableContent2";
                  //$css ="eSporttdborder eSportprinttext";
                foreach($list_item as $name => $temp){
		    			$t_total = $temp['amount'];
					    if(!$isKIS) {
						    $list_data[] = array($item_name, "", "", "", "$" . number_format($t_total, 2), $temp['last_modify']);
					    } else {
							$list_data[] = array($item_name, "", "", "$" . number_format($t_total, 2), $temp['last_modify']);
						}
                        //$count++;
                        //$x.="<tr><td class='$css'>$item_name&nbsp;</td>";
                       	//if(!$isKIS){
						//	$x.="<td class='$css'>&nbsp;</td>";
                       	//}
						//$x.="<td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>$".number_format($t_total,2)."</td></tr>";
                }
        }

        # pps charges
        $list_item = $result[8];
        if(sizeof($list_item)>0){
          foreach($list_item as $name => $temp){
            $t_total = $temp['amount'];
            if(!$isKIS) {
			    $list_data[] = array($i_Payment_PPS_Charge, "", "", "", "$" . number_format($t_total, 2), $temp['last_modify']);
            } else {
				$list_data[] = array($i_Payment_PPS_Charge, "", "", "$" . number_format($t_total, 2), $temp['last_modify']);
			}
            //$css =$count%2==0?"tableContent":"tableContent2";
            //$css ="eSporttdborder eSportprinttext";
            //$count++;
            //$x.="<tr><td class='$css'>$i_Payment_PPS_Charge&nbsp;</td>";
            //if(!$isKIS){
			//	$x.="<td class='$css'>&nbsp;</td>";
            //}
			//$x.="<td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>$".number_format($t_total,2)."</td></tr>";
          }
        }
        
        # Cancel Deposit Charge
        $list_item = $result[9];
        if(sizeof($list_item)>0){
        	$item_name = $Lang['Payment']['CancelDepositDescription'];
          $s_total =0;
          $last_modify = "";
          //$css ="eSporttdborder eSportprinttext";
          foreach($list_item as $name => $temp){
          	$s_total+=$temp['amount'];
          	$last_modify = $temp['last_modify'];
          }
          if(!$isKIS) {
              $list_data[] = array($item_name, "", "", "", $li->getWebDisplayAmountFormat($s_total), $last_modify);
          } else {
			  $list_data[] = array($item_name, "", "", $li->getWebDisplayAmountFormat($s_total), $last_modify);
		  }
          //$x.="<tr><td class='$css'>$item_name&nbsp;</td>";
         	//if(!$isKIS){
			//	$x.="<td class='$css'>&nbsp;</td>";
         	//}
			//$x.="<td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>".$li->getWebDisplayAmountFormat($s_total)."</td></tr>";
        }
        
        # Donation to school
        $list_item = $result[10];
        if(sizeof($list_item)>0){
        	$item_name = $Lang['Payment']['DonateBalanceDescription'];
          $s_total =0;
          $last_modify = "";
          //$css ="eSporttdborder eSportprinttext";
          foreach($list_item as $name => $temp){
            $s_total+=$temp['amount'];
			  $last_modify = $temp['last_modify'];
          }
          if(!$isKIS) {
			  $list_data[] = array($item_name, "", "", "", $li->getWebDisplayAmountFormat($s_total), $last_modify);
		  } else {
			  $list_data[] = array($item_name, "", "", $li->getWebDisplayAmountFormat($s_total), $last_modify);
		  }
          //$x.="<tr><td class='$css'>$item_name&nbsp;</td>";
          //if(!$isKIS){
		//	$x.="<td class='$css'>&nbsp;</td>";
          //}
			//$x.="<td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>".$li->getWebDisplayAmountFormat($s_total)."</td></tr>";
        }

	usort($list_data, "sort_list_data");
	$count = 0;
	foreach($list_data as $temp) {
		$css ="eSporttdborder eSportprinttext";
		$count++;
		$x.="<tr>
    			<td class=\"$css\">".$temp[0]."</td>
    			<td class=\"$css\">".$temp[1]."</td>
    			<td class=\"$css\">".$temp[2]."</td>
    			<td class=\"$css\">".$temp[3]."</td>
    			<td class=\"$css\">".$temp[4]."</td>";
		if(!$isKIS) {
			$x .= "<td class=\"$css\">" . $temp[5] . "</td>";
		}
		$x .= "</tr>";
	}

        # Total
        $total_income = $income+0;

        //$css =$count%2==0?"tableContent":"tableContent2";
        //$count++;
       
        $x.="<tr><td align=right class=\"eSporttdborder eSportprinttext\">$i_Payment_SchoolAccount_Total</td>";
        if(!$isKIS){
			$x.="<td class=\"eSporttdborder eSportprinttext\">$".number_format($total_income,2)."</td>";
        }
		$x.="<Td class=\"eSporttdborder eSportprinttext\">$".number_format($expense_item,2)."</td><td class=\"eSporttdborder eSportprinttext\">$".number_format($expense_single,2)."</td><Td class=\"eSporttdborder eSportprinttext\">$".number_format($expense_other,2)."</td><td class=\"eSporttdborder eSportprinttext\"></td></tr>";

        # Summary
        $total_expense = $expense_item + $expense_single + $expense_other+0;
        $net_income = $total_income - $total_expense;
        $net_income_str = ($net_income>0)? "$".number_format($net_income,2):"<font color=red>(-$".number_format(abs($net_income),2).")</font>";

      $y ="<table width='90%' border='0' cellspacing='0' cellpadding='5' align='center'>";
    if(!$isKIS){
		$y.="<tr><td class=\"eSportprinttext\">$i_Payment_SchoolAccount_TotalIncome:</td><td width=\"70%\" class=\"eSportprinttext\">$".number_format($total_income,2)."</td></tr>";
    }
	$y.="<tr><td class=\"eSportprinttext\">$i_Payment_SchoolAccount_TotalExpense:</td><td class=\"eSportprinttext\">$".number_format($total_expense,2)."</td></tr>";
	$y.="<tr><td class=\"eSportprinttext\">$i_Payment_SchoolAccount_NetIncomeExpense:</td><td class=\"eSportprinttext\">$net_income_str</td></tr>";
	$y.="</table>";

}

$x.="</tbody>";
$x.="</table>";

$display=$x;

function sort_list_data($a, $b)
{
	global $records_order, $records_field, $isKIS;
	if($records_field == 0) {
		$ret = strnatcasecmp($a[0], $b[0]);
		if($records_order == '1') {
			return $ret;
		} else {
			return $ret * -1;
		}
	}

	if($records_field == 1) {
		$a_t = str_replace(array('$',','),'', $a[1]);
		$b_t = str_replace(array('$',','),'', $b[1]);
		if($a_t == $b_t) {
			return 0;
		}
		if($records_order == '1') {
			return ($a_t < $b_t) ? -1 : 1;
		} else {
			return ($a_t < $b_t) ? 1 : -1;
		}
	}

	if($records_field == 2) {
		$i = ($isKIS) ? 4 : 5;
		$a_t = strtotime($a[$i]);
		$b_t = strtotime($b[$i]);
		if ($a_t == $b_t) {
			return 0;
		}
		if ($records_order == '1') {
			return ($a_t < $b_t) ? -1 : 1;
		} else {
			return ($a_t < $b_t) ? 1 : -1;
		}
	}
}


include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
?>
<style type="text/css" media="print">
thead {display: table-header-group;}
thead td.print_header {border-top: 1px solid #333333;}
table.eSporttableborder {border-top: 0px}
</style>
<table width="90%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<?php
if($sys_custom['ePayment']['ReportWithSchoolName']){
	$school_name = GET_SCHOOL_NAME();
	$x = '<table width="90%" align="center" border="0">
			<tr>
				<td align="center"><h2><b>'.$school_name.'</b></h2></td>
			</tr>
		</table>';
	echo $x;
}
?>
<table width='90%' border='0' cellspacing='0' cellpadding='5' align='center'>
	<tr>
		<td><b><?=$i_Payment_Menu_Report_SchoolAccount?> (<?="$FromDate $i_Profile_To $ToDate"?>)</b></td>
	</tr>
</table>
<br />
<?=$y?>
<br />
<?=$display?>
<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td class="eSportprinttext" align="right"><?="$i_general_report_creation_time : ".date('Y-m-d H:i:s')?></td>
	</tr>
</table>
<?php
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>