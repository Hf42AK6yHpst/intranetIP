<?php

# using: 

########################################################
#	Date:	2020-04-24 Ray
#			Add last_modify order
#
#	Date:	2019-08-07  Carlos
#			Addd more credit types - top-up of TNG, AlipayHK, FPS and tap & Go.
#
#	Date:	2017-10-13 	Paul
#			Improved for PowerClass to initialize the variable $result
#
#	Date:	2016-12-12 	Carlos
#			Added transaction date type option.
#
#	Date:	2016-11-03	Carlos
#			Improved payment item type transaction to first priority use PAYMENT_PAYMENT_ITEM.Name as transaction name, 
#			in order to avoid same payment item displayed as several items if payment item has ever changed its name, 
#			and some students paid before and some students paid after name changing. 
#
#	Date: 	2016-08-24  Carlos
#			$sys_custom['ePayment']['CashDepositMethod'] - added more credit methods [Bank transfer, Cheque deposit].
#
#	Date:	2015-08-06	Carlos
#			Get/Set date range cookies.
#
#	Date:	2015-07-22  Carlos
#			$sys_custom['ePayment']['CreditMethodAutoPay'] - added credit type Auto-pay
#
#	Date:	2013-10-29	Carlos
#			KIS - hide [Income] and [Total Income]
#
#	Date:	2011-11-18	YatWoon
#			Improved: display notice title in the payment item [Case#2011-1115-1426-21072]
#
########################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance'];

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "StatisticsReport_SchoolAccount";

$linterface = new interface_html();

# date range
$today_ts = strtotime(date('Y-m-d'));
$date_range_cookies = $lpayment->Get_Report_Date_Range_Cookies();
if($FromDate=="" || !intranet_validateDate($FromDate)){
	$FromDate = $date_range_cookies['StartDate']!=''? $date_range_cookies['StartDate'] : date('Y-m-d',getStartOfAcademicYear($today_ts));
}
if($ToDate=="" || !intranet_validateDate($ToDate)){
	$ToDate =  $date_range_cookies['EndDate']!=''? $date_range_cookies['EndDate'] : date('Y-m-d',getEndOfAcademicYear($today_ts));
}
if($_REQUEST['FromDate'] != '' && $_REQUEST['ToDate']!=''){
	$lpayment->Set_Report_Date_Range_Cookies($_REQUEST['FromDate'], $_REQUEST['ToDate']);
}

if($SearchBy == 2){ // by transaction time
	$date_cond = " IF(a.TransactionType=1 AND c.TransactionID IS NOT NULL,DATE_FORMAT(c.TransactionTime,'%Y-%m-%d')>='".$lpayment->Get_Safe_Sql_Query($FromDate)."' AND DATE_FORMAT(c.TransactionTime,'%Y-%m-%d')<='".$lpayment->Get_Safe_Sql_Query($ToDate)."',DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')>='".$lpayment->Get_Safe_Sql_Query($FromDate)."' AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')<='".$lpayment->Get_Safe_Sql_Query($ToDate)."') ";
}else{ // by input time
	$date_cond =" DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')>='".$lpayment->Get_Safe_Sql_Query($FromDate)."' AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')<='".$lpayment->Get_Safe_Sql_Query($ToDate)."' ";		
}

if(!isset($field)) $field = 2;
if(!isset($order)) $order = 0;

$litable = new libdbtable2007($field, $order, $pageNo);


$records_field = $field;
$records_order = $order;

$sql="SELECT 
		a.Amount,
		if(f.NoticeID is null, IF(a.TransactionType=2 AND e.ItemID IS NOT NULL,e.Name,a.Details), concat(f.Title,' - ',a.Details)),
	    a.TransactionType,
	    c.RecordType,
	    a.TransactionTime 
	  FROM PAYMENT_OVERALL_TRANSACTION_LOG AS a
	  LEFT JOIN PAYMENT_CREDIT_TRANSACTION as c ON a.RelatedTransactionID = c.TransactionID
	  left join PAYMENT_PAYMENT_ITEMSTUDENT as d on d.PaymentID=a.RelatedTransactionID
	  left join PAYMENT_PAYMENT_ITEM as e on e.ItemID=d.ItemID
	  left join INTRANET_NOTICE as f on f.NoticeID=e.NoticeID
	  WHERE $date_cond ORDER BY a.TransactionTime ASC";
$lb = new libdbtable("","","");	
$li = new libpayment();
$temp = $li->returnArray($sql,4);

$ary_types = array(1,2,3,6,7,8,9,10,11);

$x="<table width=\"96%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";
$x.="<tr class=\"tablebluetop\">";
$x.="<td rowspan=\"2\" class=\"tabletoplink\" width=\"30%\" align=\"left\" style=\"vertical-align:middle\">".$litable->column(0, $i_Payment_SchoolAccount_Detailed_Transaction)."</td>";
if(!$isKIS){
	$x.="<td rowspan=\"2\" class=\"tabletoplink\" widht=\"15%\" align=\"left\" style=\"vertical-align:middle\">".$litable->column(1, $i_Payment_SchoolAccount_Deposit)."</td>";
}
$x.="<td colspan=\"3\" class=\"tabletoplink\" width=\"40%\" align=\"center\" style=\"vertical-align:middle\">$i_Payment_SchoolAccount_Expenses</td>";
$x.="<td rowspan=\"2\" class=\"tabletoplink\" widht=\"15%\" align=\"left\" style=\"vertical-align:middle\">".$litable->column(2, $i_Form_LastModified)."</td>";
$x.="</tr>";
$x.="<tr class=\"tablebluetop\">";
$x.="<td class=\"tabletoplink\" align=\"left\" style=\"vertical-align:middle\">$i_Payment_SchoolAccount_PresetItem</td>";
$x.="<td class=\"tabletoplink\" align=\"left\" style=\"vertical-align:middle\">$i_Payment_SchoolAccount_SinglePurchase</td>";
$x.="<td class=\"tabletoplink\" align=\"left\" style=\"vertical-align:middle\" width=\"12%\">$i_Payment_SchoolAccount_Other</td>";
$x.="</tr>";


$income=0;
$expense_item=0;
$expense_single=0;
$expense_other=0;
$result = array();

for($i=0;$i<sizeof($temp);$i++){
        list($amount,$detail,$type, $credit_transaction_type, $transaction_time) = $temp[$i];
        if(in_array($type,$ary_types)){
                #$result[$type]["$detail"] += $amount+0;
                switch($type){
                        case 1        :
                                       $result[$type][$credit_transaction_type]['amount'] += $amount;
							           $result[$type][$credit_transaction_type]['last_modify'] = $transaction_time;
                                       $income+=$amount+0;
                                       break;

                        case 2        :
                                       $result[$type]["$detail"]['amount'] += $amount+0;
							           $result[$type]["$detail"]['last_modify'] = $transaction_time;
                                       $expense_item +=$amount+0;
                                       break;

                        case 3        :
                                       $result[$type]["$detail"]['amount'] += $amount+0;
							           $result[$type]["$detail"]['last_modify'] = $transaction_time;
                                       $expense_single+=$amount+0;
                                       break;

                        case 6        :
                                       $result[$type]["$detail"]['amount'] += $amount+0;
                                       $result[$type]["$detail"]['last_modify'] = $transaction_time;
                                       $income+=$amount+0;
                                       break;

                        case 7        :
                                       $result[$type]["all"]['amount'] += $amount;
                                       $result[$type]["all"]['last_modify'] = $transaction_time;
                                       $expense_other +=$amount+0;
                                       break;

                        case 8        :
                                       $result[$type]["all"]['amount'] += $amount;
                                       $result[$type]["all"]['last_modify'] = $transaction_time;
                                       $expense_other +=$amount+0;
                                       break;
												
						case 9        :
                                       $result[$type]["all"]['amount'] += $amount;
                                       $result[$type]["all"]['last_modify'] = $transaction_time;
                                       $expense_other +=$amount+0;
                                       break;
                         
                        case 10        :
                                       $result[$type]["all"]['amount'] += $amount;
                                       $result[$type]["all"]['last_modify'] = $transaction_time;
                                       $expense_other +=$amount+0;
                                       break;
                        
                        case 11        :
                                       $result[$type]["all"]['amount'] += $amount+0;
							           $result[$type]["all"]['last_modify'] = $transaction_time;
                                       $income+=$amount+0;
                                       break;
                        default        : break;
                }

        }

}

if(sizeof($result)<=0){
	$x.="<tr><td colspan=\"5\" align=\"center\" class=\"tablebluerow2 tabletext\" height=\"40\" style=\"vertical-align:middle\">$i_no_record_exists_msg<td></tr>";
}else{
	//$count=0;
	$list_data = array();
	if(!$isKIS) {
		# deposit
		$list_item = $result[1];
		if(sizeof($list_item)>0){
	        foreach($list_item as $credit_type => $temp){
	                $t_total = $temp['amount'];
	                //$css =$count%2==0?"tablebluerow1 tabletext":"tablebluerow2 tabletext";
	                //$count++;
	                switch ($credit_type)
	                {
	                        case 1: $name = $i_Payment_Credit_TypePPS; break;
	                        case 2: $name = $i_Payment_Credit_TypeCashDeposit; break;
	                        case 3: $name = $i_Payment_Credit_TypeAddvalueMachine; break;
	                        case 4: $name = $Lang['ePayment']['AutoPay'];break;
	                        case 6: $name = $Lang['ePayment']['BankTransfer'];break;
	                        case 7: $name = $Lang['ePayment']['ChequeDeposit'];break;
	                        case 8: $name = $Lang['ePayment']['TNGTopUp'];break;
	                        case 9: $name = $Lang['ePayment']['AlipayTopUp'];break;
	                        case 10:$name = $Lang['ePayment']['FPSTopUp'];break;
	                        case 11:$name = $Lang['ePayment']['TapAndGoTopUp'];break;
						    case 12:$name = $Lang['ePayment']['VisaMasterTopUp'];break;
					    	case 13:$name = $Lang['ePayment']['WeChatTopUp'];break;
						    case 14:$name = $Lang['ePayment']['AlipayCNTopUp'];break;
						    default : $name = $i_Payment_Credit_TypeUnknown . "(Type=$credit_type)";
	
	                }
				    if(!$isKIS) {
						$list_data[] = array($name, "$" . number_format($t_total, 2), "", "", "", $temp['last_modify']);
					} else {
						$list_data[] = array($name, "", "", "", $temp['last_modify']);
					}
	                //$x.="<tr class=\"$css\"><td class=\"tabletext\">$name&nbsp;</td>";
	            //if(!$isKIS){
				//	$x.="<td class=\"tabletext\">$".number_format($t_total,2)."</td>";
	            //}
				//	$x.="<td class=\"tabletext\">&nbsp;</td><td class=\"tabletext\">&nbsp;</td><td class=\"tabletext\"'>&nbsp;</td></tr>";
	        }
		}
	}
	
	# payment cancellation
	$list_item = $result[6];
	if(sizeof($list_item)>0){
		foreach($list_item as $name => $temp){
			$t_total = $temp['amount'];
			if(!$isKIS) {
				$list_data[] = array($name, "$" . number_format($t_total, 2), "", "", "", $temp['last_modify']);
			} else {
				$list_data[] = array($name, "", "", "", $temp['last_modify']);
			}
			//$css =$count%2==0?"tablebluerow1 tabletext":"tablebluerow2 tabletext";
			//$count++;
			//$x.="<tr class=\"$css\"><td class=\"tabletext\">$name&nbsp;</td>";
			//if(!$isKIS){
			//	$x.="<td class=\"tabletext\">$".number_format($t_total,2)."</td>";
			//}
			//$x.="<td class=\"tabletext\">&nbsp;</td><td class=\"tabletext\">&nbsp;</td><td class=\"tabletext\">&nbsp;</td></tr>";
		}
	}
	
	# POS Void Transaction Refund
	$list_item = $result[11];
	if(sizeof($list_item)>0){
		$item_name = $Lang['ePOS']['ReportVoidTitle'];
		$s_total = $list_item['all']['amount'];
		if(!$isKIS) {
			$list_data[] = array($item_name, "$".number_format($s_total, 2), "", "", "", $list_item['all']['last_modify']);
		} else {
			$list_data[] = array($item_name, "", "", "", $list_item['all']['last_modify']);
		}
		//$css =$count%2==0?"tablebluerow1 tabletext":"tablebluerow2 tabletext";
		//$x.="<tr class=\"$css\">
		//				<td class=\"tabletext\">$item_name&nbsp;</td>";
		//		if(!$isKIS) {
		//			$x  .= "<td class=\"tabletext\">$".number_format($s_total,2)."</td>";
		//		}
		//		$x  .= "<td class=\"tabletext\">&nbsp;</td>
		//				<td class=\"tabletext\">&nbsp;</td>
		//				<td class=\"tabletext\">&nbsp;</td></tr>";
	}
	
	
	# payment item
	$list_item = $result[2];
	if(sizeof($list_item)>0){
		foreach($list_item as $name => $temp){
			$t_total = $temp['amount'];
			if(!$isKIS) {
				$list_data[] = array($name, "", "$" . number_format($t_total, 2), "", "", $temp['last_modify']);
			} else {
				$list_data[] = array($name, "$" . number_format($t_total, 2), "", "", $temp['last_modify']);
			}
			//$css =$count%2==0?"tablebluerow1 tabletext":"tablebluerow2 tabletext";
			//$count++;
			//$x.="<tr class=\"$css\"><td class=\"tabletext\">$name&nbsp;</td>";
		  //if(!$isKIS) {
			//$x.="<td class=\"tabletext\">&nbsp;</td>";
		  //}
			//$x.="<td class=\"tabletext\">$".number_format($t_total,2)."</td><td class=\"tabletext\">&nbsp;</td><td class=\"tabletext\">&nbsp;</td></tr>";
		}
	}
	
	# single purchase
	$list_item = $result[3];
	if(sizeof($list_item)>0){
		foreach($list_item as $name => $temp){
			$t_total = $temp['amount'];
			if(!$isKIS) {
				$list_data[] = array($name, "", "", "$" . number_format($t_total, 2), "", $temp['last_modify']);
			} else {
				$list_data[] = array($name, "", "$" . number_format($t_total, 2), "", $temp['last_modify']);
			}
			//$css =$count%2==0?"tablebluerow1 tabletext":"tablebluerow2 tabletext";
			//$count++;
			//$x.="<tr class=\"$css\"><td class=\"tabletext\">$name&nbsp;</td>";
			//if(!$isKIS){
			//	$x.="<td class=\"tabletext\">&nbsp;</td>";
			//}
			//$x.="<td class='$css'>&nbsp;</td><td class=\"tabletext\">$".number_format($t_total,2)."</td><td class=\"tabletext\">&nbsp;</td></tr>";
		}
	}

	# refund
	$list_item = $result[7];
	if(sizeof($list_item)>0){
		$item_name = $i_Payment_action_refund;
		$s_total =0;
		$last_modify = "";
		//$css =$count%2==0?"tablebluerow1 tabletext":"tablebluerow2 tabletext";
		foreach($list_item as $name => $temp){
			$s_total+=$temp['amount'];
			$last_modify = $temp['last_modify'];
			//$count++;
		}
		if(!$isKIS) {
			$list_data[] = array($item_name, "", "", "", "$" . number_format($s_total, 2), $last_modify);
		} else {
			$list_data[] = array($item_name, "", "", "$" . number_format($s_total, 2), $last_modify);
		}
		//$x.="<tr class=\"$css\"><td class=\"tabletext\">$item_name&nbsp;</td>";
		//if(!$isKIS){
		//	$x.="<td class=\"tabletext\">&nbsp;</td>";
		//}
		//$x.="<td class=\"tabletext\">&nbsp;</td><td class=\"tabletext\">&nbsp;</td><td class=\"tabletext\">$".number_format($s_total,2)."</td></tr>";
		/*
		foreach($list_item as $name => $t_total){
			$css =$count%2==0?"tablebluerow1 tabletext":"tablebluerow2 tabletext";
			$count++;
			$x.="<tr class=\"$css\"><td class=\"tabletext\">$name&nbsp;</td><td class=\"tabletext\">&nbsp;</td><td class=\"tabletext\">&nbsp;</td><td class=\"tabletext\">&nbsp;</td><td class=\"tabletext\">$".number_format($t_total,1)."</td></tr>";
		}
		*/
	}
	
	# pps charges
	$list_item = $result[8];
	if(sizeof($list_item)>0){
		foreach($list_item as $name => $temp){
			$t_total = $temp['amount'];
			if(!$isKIS) {
				$list_data[] = array($i_Payment_PPS_Charge, "", "", "", "$" . number_format($t_total, 2), $temp['last_modify']);
			} else {
				$list_data[] = array($i_Payment_PPS_Charge, "", "", "$" . number_format($t_total, 2), $temp['last_modify']);
			}
			//$css =$count%2==0?"tablebluerow1 tabletext":"tablebluerow2 tabletext";
			//$count++;
			//$x.="<tr class=\"$css\">
			//			<td class=\"tabletext\">$i_Payment_PPS_Charge&nbsp;</td>";
			//	if(!$isKIS){
			//	 	$x .= "<td class=\"tabletext\">&nbsp;</td>";
			//	}
			//	 $x .= "<td class=\"tabletext\">&nbsp;</td>
			//			<td class=\"tabletext\">&nbsp;</td>
			//			<td class=\"tabletext\">$".number_format($t_total,2)."</td>
			//		</tr>";
		}
	}
	
	# Cancel Deposit Charge
  $list_item = $result[9];
  if(sizeof($list_item)>0){
  	$item_name = $Lang['Payment']['CancelDepositDescription'];
    $s_total =0;
    $last_modify = "";
    //$css =$count%2==0?"tablebluerow1 tabletext":"tablebluerow2 tabletext";
    foreach($list_item as $name => $temp){
	    //$count++;
	    $s_total+=$temp['amount'];
		$last_modify = $temp['last_modify'];
    }
    if(!$isKIS) {
		$list_data[] = array($item_name, "", "", "", $li->getWebDisplayAmountFormat($s_total), $last_modify);
	} else {
		$list_data[] = array($item_name, "", "", $li->getWebDisplayAmountFormat($s_total), $last_modify);
	}
    //$x.="<tr class=\"$css\">
    //			<td class=\"tabletext\">$item_name&nbsp;</td>";
	//	if(!$isKIS){
	//		$x  .= "<td class=\"tabletext\">&nbsp;</td>";
	//	}
	//	$x  .= "<td class=\"tabletext\">&nbsp;</td>
    //			<td class=\"tabletext\">&nbsp;</td>
    //			<td class=\"tabletext\">".$li->getWebDisplayAmountFormat($s_total)."</td>
    //		</tr>";
  }
  
  # Donation to school
  $list_item = $result[10];
  if(sizeof($list_item)>0){
  	$item_name = $Lang['Payment']['DonateBalanceDescription'];
    $s_total =0;
    $last_modify = "";
    //$css =$count%2==0?"tablebluerow1 tabletext":"tablebluerow2 tabletext";
    foreach($list_item as $name => $temp){
	    //$count++;
	    $s_total+=$temp['amount'];
		$last_modify = $temp['last_modify'];
    }
    if(!$isKIS) {
		$list_data[] = array($item_name, "", "", "", $li->getWebDisplayAmountFormat($s_total), $last_modify);
	} else {
		$list_data[] = array($item_name, "", "", $li->getWebDisplayAmountFormat($s_total), $last_modify);
	}
    //$x.="<tr class=\"$css\">
    //			<td class=\"tabletext\">$item_name&nbsp;</td>";
	//	if(!$isKIS){
	//		$x  .= "<td class=\"tabletext\">&nbsp;</td>";
	//	}
	//	$x  .= "<td class=\"tabletext\">&nbsp;</td>
    //			<td class=\"tabletext\">&nbsp;</td>
    //			<td class=\"tabletext\">".$li->getWebDisplayAmountFormat($s_total)."</td>
    //		</tr>";
  }

	usort($list_data, "sort_list_data");
	$count = 0;
	foreach($list_data as $temp) {
		$css =$count%2==0?"tablebluerow1 tabletext":"tablebluerow2 tabletext";
		$count++;
		$x.="<tr class=\"$css\">
    			<td class=\"tabletext\">".$temp[0]."</td>	
			    <td class=\"tabletext\">".$temp[1]."</td>
			    <td class=\"tabletext\">".$temp[2]."</td>
    			<td class=\"tabletext\">".$temp[3]."</td>
    			<td class=\"tabletext\">".$temp[4]."</td>";
		if(!$isKIS) {
			$x .= "<td class=\"tabletext\">" . $temp[5] . "</td>";
		}
		$x .= "</tr>";
	}

	# Total
	$total_income = $income+0;
	
	$css ="tablebluebottom";
	$x.="<tr class=\"$css\"><td class=\"tabletext\" align=right>$i_Payment_SchoolAccount_Total:</td>";
	if(!$isKIS){
		$x.="<td class=\"tabletext\">$".number_format($total_income,2)."</td>";
	}
	$x.="<td class=\"tabletext\">$".number_format($expense_item,2)."</td><td class=\"tabletext\">$".number_format($expense_single,2)."</td><Td class=\"tabletext\">$".number_format($expense_other,2)."</td><td class=\"tabletext\"></td></tr>";
	
	# Summary
	$total_expense = $expense_item + $expense_single + $expense_other+0;
	$net_income = $total_income - $total_expense;
	$net_income_str = ($net_income>0)? "$".number_format($net_income,2):"<font color=red>(-$".number_format(abs($net_income),2).")</font>";
	$y="<table width=\"96%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\" align=\"center\">";
  if(!$isKIS) {
	$y.="<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Payment_SchoolAccount_TotalIncome</td>";
	$y.="<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"70%\">$".number_format($total_income,2)."</td></tr>";
  }
	$y.="<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Payment_SchoolAccount_TotalExpense</td>";
	$y.="<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"70%\">$".number_format($total_expense,2)."</td></tr>";
	$y.="<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_Payment_SchoolAccount_NetIncomeExpense</td>";
	$y.="<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\" width=\"70%\">$net_income_str</td></tr>";
	$y.="</table>";

}

$x.="</table>";

$display=$x;

$toolbar = $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
$toolbar2 = $linterface->GET_LNK_EXPORT("javascript:exportPage(document.form1,'export.php?FromDate=".urlencode($FromDate)."&ToDate=".urlencode($ToDate)."')","","","","",0);

$TAGS_OBJ[] = array($i_Payment_Menu_Report_SchoolAccount, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

function sort_list_data($a, $b)
{
	global $records_order, $records_field, $isKIS;
	if($records_field == 0) {
		$ret = strnatcasecmp($a[0], $b[0]);
		if($records_order == '1') {
			return $ret;
		} else {
			return $ret * -1;
		}
	}

    if($records_field == 1) {
        $a_t = str_replace(array('$',','),'', $a[1]);
		$b_t = str_replace(array('$',','),'', $b[1]);
		if($a_t == $b_t) {
			return 0;
		}
		if($records_order == '1') {
			return ($a_t < $b_t) ? -1 : 1;
		} else {
			return ($a_t < $b_t) ? 1 : -1;
		}
    }

    if($records_field == 2) {
		$i = ($isKIS) ? 4 : 5;
		$a_t = strtotime($a[$i]);
		$b_t = strtotime($b[$i]);
		if ($a_t == $b_t) {
			return 0;
		}
		if ($records_order == '1') {
			return ($a_t < $b_t) ? -1 : 1;
		} else {
			return ($a_t < $b_t) ? 1 : -1;
		}
	}
}
?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.css" type="text/css" />
<script language="javascript">
    function checkForm(formObj){
            if(formObj==null) return false;
            fromV = formObj.FromDate;
            toV= formObj.ToDate;
            if(!checkDate(fromV)){
                //formObj.FromDate.focus();
                return false;
            }
            else if(!checkDate(toV)){
                //formObj.ToDate.focus();
                return false;
            }
            return true;
    }
    
    function checkDate(obj){
            if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
            return true;
    }
    
    function submitForm(obj){
            if(checkForm(obj))
                    obj.submit();
    }
    
    function openPrintPage()
	{
         newWindow("print.php?SearchBy=<?=urlencode($SearchBy)?>&FromDate=<?=urlencode($FromDate)?>&ToDate=<?=urlencode($ToDate)?>&order=<?=$litable->order?>&pageNo=<?=$litable->pageNo?>&field=<?=$litable->field?>",10);
	}
	
	function exportPage(obj,url){
			old_url = obj.action;
	        obj.action=url;
	        obj.submit();
	        obj.action = old_url;
	        
	}
	
$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
$(document).ready(function(){ 
	$('input#FromDate').datepick({
		dateFormat: 'yy-mm-dd',
		dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		changeFirstDay: false,
		firstDay: 0
		});
	$('input#ToDate').datepick({
		dateFormat: 'yy-mm-dd',
		dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		changeFirstDay: false,
		firstDay: 0
		});
});
</script>
<br />
<form name="form1" method="get" action="">
<!-- date range -->
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?=$Lang['ePayment']['DateType']?></td>
	  	<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
	    	<input type="radio" id="SearchByPostTime" name="SearchBy" value="1"  <?=($SearchBy!=2?"checked":"")?> />
			<label for="SearchByPostTime"><?=$i_Payment_Search_By_PostTime?></label>&nbsp;&nbsp;
			<input type="radio" id="SearchByTransactionTime" name="SearchBy" value="2" <?=($SearchBy==2?"checked":"")?> />
			<label for="SearchByTransactionTime"><?=$i_Payment_Search_By_TransactionTime?></label>
	    </td>
  	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_Profile_From ?></td>
		<td valign="top" class="tabletext" width="70%">
			<input type="text" name="FromDate" id="FromDate" value="<?=escape_double_quotes($FromDate)?>" maxlength="10" class='textboxnum'>
			<span class="tabletextremark">(yyyy-mm-dd)</span>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_Profile_To ?></td>
		<td valign="top" class="tabletext" width="70%">
			<input type="text" name="ToDate" id="ToDate" value="<?=escape_double_quotes($ToDate)?>" maxlength="10" class='textboxnum'>
			<span class="tabletextremark">(yyyy-mm-dd)</span>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	</tr>
	<tr>
		<td colspan="2" align="center" class="tabletext">
			<?= $linterface->GET_ACTION_BTN($button_view, "button", "submitForm(document.form1);","submit2") ?>
		</td>
	</tr>
</table>
    <input type="hidden" name="order" value="<?php echo $litable->order; ?>" />
    <input type="hidden" name="pageNo" value="<?php echo $litable->pageNo; ?>" />
    <input type="hidden" name="field" value="<?php echo $litable->field; ?>" />
</form>
<br />
<?=$y?>
<br />
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?> <?=$toolbar2?></td>
	</tr>
</table>
<?=$display?>
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td class="tabletext" align="right"><?="$i_general_report_creation_time : ".date('Y-m-d H:i:s')?></td>
	</tr>
</table>
<br />

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
