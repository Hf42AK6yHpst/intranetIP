<?php
// editing by 
/*
 * Created on 2012-07-20
 * 2013-10-10 (Carlos): modified form / class follow selected academic year
 * 2013-10-03 (Carlos): cater archived user records
 * 2013-01-16 (Carlos): fix calculation of photo copier refund records
 * 2015-02-26 (Carlos): $sys_custom['ePayment']['ReportWithSchoolName'] - display school name.
 * 2016-06-24 (Carlos): Added user login field for student list view.  
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

if(count($YearFormID)==0 || $FromDate=="" || $ToDate=="")
	header("Location: index.php");
	
intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$li = new libpayment();
//$lexport = new libexporttext();
$fcm = new form_class_manage();
//$fcm_ui = new form_class_manage_ui();

$MODULE_OBJ['title'] = $Lang['ePayment']['BalanceReport']." (".intranet_htmlspecialchars($FromDate)." to ".intranet_htmlspecialchars($ToDate).")";
$linterface = new interface_html("popup.html");

//$CurrentAcademicYearID = Get_Current_Academic_Year_ID();
$numOfYearFormID = count($YearFormID);
// required transaciton types
$ary_types = array(1,2,3,4,5,6,7,8,9,10,11);

if($ClassID != ''){
	$ClassID = IntegerSafe($ClassID);
	$YearID = IntegerSafe($YearID);
	$students = $fcm->Get_Student_By_Class($ClassID);
	$libyear = new Year($YearID);
	$libclass = new year_class($ClassID);
	$year_name = htmlspecialchars($libyear->YearName,ENT_QUOTES);
	$class_name = Get_Lang_Selection($libclass->ClassTitleB5,$libclass->ClassTitleEN);
	
	$y = '<div class="navigation" style="width:96%;">
				<img height="15" width="15" align="absmiddle" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/nav_arrow.gif"/>';
			$y.="<span><a href=\"javascript:SubmitForm('','');\">".$Lang['ePayment']['IdentityOrYearForm']."</a></span>";
			$y.='<img height="15" width="15" align="absmiddle" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/nav_arrow.gif"/>';
			$y.= '<span><a href="javascript:SubmitForm(\''.$YearID.'\',\'\');">'.$year_name.'</a></span>';
			$y.='<img height="15" width="15" align="absmiddle" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/nav_arrow.gif"/>';
			$y.= '<span>'.$class_name.'</a>';
		$y.= '</div><br />';
	//$y.="<a href=\"javascript:SubmitForm('','');\">".$year_name."</a> &gt; <a href=\"javascript:SubmitForm('".$YearID."','');\">".$class_name."</a><br />";
	
	$y.="<table width=\"96%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\" align=\"center\">";
	// Identity or Year Class
	$y.="<thead>";
	$y.="<tr>";
	$y.="<td class=\"tablebluetop tabletopnolink\">".$Lang['General']['Name']."</td>";
	$y.="<td class=\"tablebluetop tabletopnolink\">".$Lang['General']['UserLogin']."</td>";
	// Opening Balance
	$y.="<td class=\"tablebluetop tabletopnolink\">".$Lang['ePayment']['OpenningBalance']."</td>";
	// Total Income
	$y.="<td class=\"tablebluetop tabletopnolink\">$i_Payment_SchoolAccount_TotalIncome</td>";
	// Total Expense
	$y.="<td class=\"tablebluetop tabletopnolink\">$i_Payment_SchoolAccount_TotalExpense</td>";
	// Closing Balance
	$y.="<td class=\"tablebluetop tabletopnolink\">".$Lang['ePayment']['ClosingBalance']."</td>";
	$y.="</tr>";
	$y.="</thead>";
	$y.="<tbody>";
	
	$student_userid = Get_Array_By_Key($students,'UserID');
	if(count($student_userid)>0){
		$userid_cond = " AND a.StudentID IN (".implode(",",$student_userid).")";
	}else{
		$userid_cond = " AND a.StudentID = -1 ";
	}
	
	//$name_field = getNameFieldWithClassNumberByLang("u.");
	
	$date_cond =" AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')>='".$li->Get_Safe_Sql_Query($FromDate)."' AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')<='".$li->Get_Safe_Sql_Query($ToDate)."'";
	$sql="SELECT 
				a.Amount,
				if(f.NoticeID is null, a.Details, concat(f.Title,' - ',a.Details)),
		      	a.TransactionType,
		      	c.RecordType,
				a.StudentID,
				a.BalanceAfter,
				a.RelatedTransactionID  
		    FROM 
				PAYMENT_OVERALL_TRANSACTION_LOG AS a 
				LEFT JOIN INTRANET_USER as u ON u.UserID=a.StudentID 
				LEFT JOIN INTRANET_ARCHIVE_USER as u2 ON u2.UserID=a.StudentID 
		        LEFT OUTER JOIN PAYMENT_CREDIT_TRANSACTION as c ON a.RelatedTransactionID = c.TransactionID
		        left join PAYMENT_PAYMENT_ITEMSTUDENT as d on d.PaymentID=a.RelatedTransactionID
				left join PAYMENT_PAYMENT_ITEM as e on e.ItemID=d.ItemID
				left join INTRANET_NOTICE as f on f.NoticeID=e.NoticeID
		    WHERE 
				(u.RecordType='2' OR u2.RecordType='2') $userid_cond 
				$date_cond 
			ORDER BY a.StudentID, a.TransactionTime, a.Details, c.RecordType ";
	
	$temp = $li->returnArray($sql,7);
	
	$sql = "SELECT 
				a.StudentID as UserID,
				a.BalanceAfter 
			FROM 
				PAYMENT_OVERALL_TRANSACTION_LOG AS a 
				LEFT JOIN INTRANET_USER as u ON u.UserID=a.StudentID 
				LEFT JOIN INTRANET_ARCHIVE_USER as u2 ON u2.UserID=a.StudentID 
		    WHERE 
				(u.RecordType='2' OR u2.RecordType='2') $userid_cond 
				AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')<'".$li->Get_Safe_Sql_Query($FromDate)."' 
			ORDER BY a.StudentID, a.TransactionTime, a.LogID";
	
	$temp2 = $li->returnResultSet($sql);
	
	$sql = "SELECT 
				a.StudentID as UserID,
				a.BalanceAfter 
			FROM 
				PAYMENT_OVERALL_TRANSACTION_LOG AS a  
				LEFT JOIN INTRANET_USER as u ON u.UserID=a.StudentID 
				LEFT JOIN INTRANET_ARCHIVE_USER as u2 ON u2.UserID=a.StudentID 
		    WHERE 
				(u.RecordType='2' OR u2.RecordType='2') $userid_cond 
				AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')<='".$li->Get_Safe_Sql_Query($ToDate)."' 
			ORDER BY a.StudentID, a.TransactionTime, a.LogID";
	
	$tempCloseBalance = $li->returnResultSet($sql);
	
	$sum_open_balance = 0;
	$sum_close_balance = 0;
	$sum_total_income = 0;
	$sum_total_expense = 0;
	$StudentIDToBalance = array();
	
	$StudentIDToOpenBalance = array();
	for($i=0;$i<count($temp2);$i++) {
		$student_id = $temp2[$i]['UserID'];
		$StudentIDToOpenBalance[$student_id] = $temp2[$i]['BalanceAfter'];
	}
	
	$StudentIDToCloseBalance = array();
	for($i=0;$i<count($tempCloseBalance);$i++) {
		$student_id = $tempCloseBalance[$i]['UserID'];
		$StudentIDToCloseBalance[$student_id] = $tempCloseBalance[$i]['BalanceAfter'];
	}
	
	for($i=0;$i<sizeof($temp);$i++){
        list($amount,$detail,$type, $credit_transaction_type, $student_id, $balance_after, $related_transaction_id) = $temp[$i];
        if(in_array($type,$ary_types)){
        	if(!isset($StudentIDToBalance[$student_id])){
        		$StudentIDToBalance[$student_id] = array();
        	}
        //	$StudentIDToBalance[$student_id]['CloseBalance'] = $balance_after;
            //$close_balance += $balance_after;
            switch($type){
                    case 1        :  # 1 - Add Value ( Cash Deposit, PPS, Add Value Machine )
                                   //$result[$type][$credit_transaction_type] += $amount;
                                   //$sum_income+=$amount+0;
                                   //$total_income += $amount + 0;
                                   $StudentIDToBalance[$student_id]['Income'] += $amount + 0;
                                   break;

                    case 2        : # 2 - Payment Items
                                   //$result[$type]["$detail"] += $amount+0;
                                   //$sum_expense +=$amount+0;
                                   //$total_expense += $amount + 0;
                                   $StudentIDToBalance[$student_id]['Expense'] += $amount + 0;
                                   break;

                    case 3        : # 3 - Single Purchase
                                   //$result[$type]["$detail"] += $amount+0;
                                  // $sum_expense+=$amount+0;
                                   //$total_expense += $amount + 0;
                                   if($related_transaction_id == '0') { // photo copier refund
	                                   	$StudentIDToBalance[$student_id]['Expense'] -= $amount + 0;
                                   }else{
                                   		$StudentIDToBalance[$student_id]['Expense'] += $amount + 0;
                                   }
                                   break;
					
					case 4 		  : # 4 - Transfer TO
									$StudentIDToBalance[$student_id]['Expense'] += $amount + 0;
									break;
					case 5		  : 
									# 5 - Transfer FROM
									$StudentIDToBalance[$student_id]['Income'] += $amount + 0;
									break;
				 	
                    case 6        : # 6 - Cancel Payment
                                   //$result[$type]["$detail"] += $amount+0;
                                   //$sum_income+=$amount+0;
                                   //$total_income += $amount + 0;
                                   $StudentIDToBalance[$student_id]['Income'] += $amount + 0;
                                   break;

                    case 7        : # 7 - Refund
                                   //$result[$type]["all"] += $amount;
                                  // $sum_expense +=$amount+0;
                                   //$total_expense += $amount + 0;
                                   $StudentIDToBalance[$student_id]['Expense'] += $amount + 0;
                                   break;

                    case 8        : # 8 - PPS Charges
                                   //$result[$type]["all"] += $amount;
                                   //$sum_expense +=$amount+0;
                                   //$total_expense += $amount + 0;
                                   $StudentIDToBalance[$student_id]['Expense'] += $amount + 0;
                                   break;
											
					case 9        : # 9 - Cancel Cash Deposit
                                   //$result[$type]["all"] += $amount;
                                  // $sum_expense +=$amount+0;
                                   //$total_expense += $amount + 0;
                                   $StudentIDToBalance[$student_id]['Expense'] += $amount + 0;
                                   break;
                     
                    case 10        : # 10 - Donation to school
                                   //$result[$type]["all"] += $amount;
                                   //$sum_expense +=$amount+0;
                                   //$total_expense += $amount + 0;
                                   $StudentIDToBalance[$student_id]['Expense'] += $amount + 0;
                                   break;
                    
                    case 11        : # 11 - POS void transaction (refund)
                                   //$result[$type]["all"] += $amount+0;
                                   //$sum_income+=$amount+0;
                                   //$total_income+= $amount + 0;
                                   $StudentIDToBalance[$student_id]['Income'] += $amount + 0;
                                   break;
                    default        : break;
            }
        }
	}
	
	for($j=0;$j<count($students);$j++){
		$student_id = $students[$j]['UserID'];
		//$students[$j]['StudentName'];
		$class_number = $students[$j]['ClassNumber'];
		//$students[$j]['ClassTitleEN'];
		//$students[$j]['ClassTitleB5'];
		$user_login = $students[$j]['UserLogin'];
		$class_name = trim(Get_Lang_Selection($students[$j]['ClassTitleB5'],$students[$j]['ClassTitleEN']));
		
		$display_name = $students[$j]['StudentName'];
		if($class_name != ""){
			$display_name .= "(".$class_name."-".$class_number.")";
		}
		
		if(!isset($StudentIDToBalance[$student_id])){
			$StudentIDToBalance[$student_id] = array();
			$StudentIDToBalance[$student_id]['Income'] = 0;
			$StudentIDToBalance[$student_id]['Expense'] = 0;
			$StudentIDToBalance[$student_id]['CloseBalance'] = 0;
		}
		
		//$close_balance = $StudentIDToBalance[$student_id]['CloseBalance'];
		$total_income = $StudentIDToBalance[$student_id]['Income'];
		$total_expense = $StudentIDToBalance[$student_id]['Expense'];
		//$open_balance = $close_balance - $total_income + $total_expense + 0;
		$close_balance = $StudentIDToCloseBalance[$student_id];
		//if(round($oepn_balance,2) == 0.00 && isset($StudentIDToOpenBalance[$student_id])) {
			$open_balance = $StudentIDToOpenBalance[$student_id];
		//}
		
		$css = $j%2==0?"tablebluerow1":"tablebluerow2";
		// Student Name
		$y.="<tr class=\"".$css."\">";
		$y.="<td class=\"".$css."\">".$display_name."</td>";
		$y.="<td class=\"".$css."\">".$user_login."</td>";
		// Opening Balance
		$y.="<td class=\"".$css."\">$".number_format(sprintf("%.2f",$open_balance),2)."</td>";
		// Total Income
		$y.="<td class=\"".$css."\">$".number_format(sprintf("%.2f",$total_income),2)."</td>";
		// Total Expense
		$y.="<td class=\"".$css."\">$".number_format(sprintf("%.2f",$total_expense),2)."</td>";
		// Closing Balance
		$y.="<td class=\"".$css."\">$".number_format(sprintf("%.2f",$close_balance),2)."</td>";
		$y.="</tr>";
		
		$sum_open_balance += $open_balance;
		$sum_close_balance += $close_balance;
		$sum_income += $total_income;
		$sum_expense += $total_expense;
	}
	
//	$sum_open_balance = $sum_close_balance - $sum_income + $sum_expense + 0;
	$y .= "<tr class=\"tablebluebottom\">";
	$y .= "<td class=\"tabletext\" align=\"right\" colspan=\"2\">".$i_Payment_SchoolAccount_Total."</td>";
	$y .= "<td class=\"tabletext\">$".number_format(sprintf("%.2f",$sum_open_balance),2)."</td>";
	$y .= "<td class=\"tabletext\">$".number_format(sprintf("%.2f",$sum_income),2)."</td>";
	$y .= "<td class=\"tabletext\">$".number_format(sprintf("%.2f",$sum_expense),2)."</td>";
	$y .= "<td class=\"tabletext\">$".number_format(sprintf("%.2f",$sum_close_balance),2)."</td>";
	$y .= "</tr>";
	
	$y .= "</tbody>";
	$y .= "</table>";
	
}else if($YearID != ''){
	if($YearID == -1){
		$userid_cond = "";
		$usertype = "1";
	
		$date_cond =" AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')>='".$li->Get_Safe_Sql_Query($FromDate)."' AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')<='".$li->Get_Safe_Sql_Query($ToDate)."'";
		$name_field = getNameFieldByLang("u.");
		$name_field2 = getNameFieldByLang("u2.");
		
		$sql = "SELECT u.UserID,$name_field as UserName FROM INTRANET_USER as u WHERE u.RecordType='1' 
				UNION (SELECT u2.UserID,$name_field2 as UserName FROM INTRANET_ARCHIVE_USER as u2 WHERE u2.RecordType='1')
				ORDER BY UserName";
		$staffs = $li->returnArray($sql);
		/*
		$staff_userid = Get_Array_By_Key($staffs,'UserID');
		if(count($staff_userid)>0){
			$userid_cond = " AND u.UserID IN (".implode(",",$staff_userid).")";
		}else{
			$userid_cond = "";
		}
		*/
		
		
		$sql="SELECT 
				a.StudentID as UserID,
				$name_field as UserName,
				a.Amount,
				if(f.NoticeID is null, a.Details, concat(f.Title,' - ',a.Details)),
		      	a.TransactionType,
		      	c.RecordType,
				a.StudentID,
				a.BalanceAfter,
				a.RelatedTransactionID  
		    FROM 
				PAYMENT_OVERALL_TRANSACTION_LOG AS a 
				LEFT JOIN INTRANET_USER as u ON u.UserID=a.StudentID 
				LEFT JOIN INTRANET_ARCHIVE_USER as u2 ON u2.UserID=a.StudentID 
		        LEFT OUTER JOIN PAYMENT_CREDIT_TRANSACTION as c ON a.RelatedTransactionID = c.TransactionID
		        left join PAYMENT_PAYMENT_ITEMSTUDENT as d on d.PaymentID=a.RelatedTransactionID
				left join PAYMENT_PAYMENT_ITEM as e on e.ItemID=d.ItemID
				left join INTRANET_NOTICE as f on f.NoticeID=e.NoticeID
		    WHERE 
				(u.RecordType='".$li->Get_Safe_Sql_Query($usertype)."' OR u2.RecordType='".$li->Get_Safe_Sql_Query($usertype)."') $userid_cond 
				$date_cond 
			ORDER BY a.StudentID, a.TransactionTime, a.Details, c.RecordType ";
		
		$temp = $li->returnArray($sql,7);
		
		$sql = "SELECT 
					a.StudentID as UserID,
					a.BalanceAfter 
				FROM 
					PAYMENT_OVERALL_TRANSACTION_LOG AS a 
					LEFT JOIN INTRANET_USER as u ON u.UserID=a.StudentID 
					LEFT JOIN INTRANET_ARCHIVE_USER as u2 ON u2.UserID=a.StudentID 
			    WHERE 
					(u.RecordType='".$li->Get_Safe_Sql_Query($usertype)."' OR u2.RecordType='".$li->Get_Safe_Sql_Query($usertype)."') $userid_cond 
					AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')<'".$li->Get_Safe_Sql_Query($FromDate)."' 
				ORDER BY a.StudentID, a.TransactionTime, a.LogID";
		
		$temp2 = $li->returnResultSet($sql);
		
		$sql = "SELECT 
					a.StudentID as UserID,
					a.BalanceAfter 
				FROM 
					PAYMENT_OVERALL_TRANSACTION_LOG AS a 
					LEFT JOIN INTRANET_USER as u ON u.UserID=a.StudentID 
					LEFT JOIN INTRANET_ARCHIVE_USER as u2 ON u2.UserID=a.StudentID 
			    WHERE 
					(u.RecordType='".$li->Get_Safe_Sql_Query($usertype)."' OR u2.RecordType='".$li->Get_Safe_Sql_Query($usertype)."') $userid_cond 
					AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')<='".$li->Get_Safe_Sql_Query($ToDate)."' 
				ORDER BY a.StudentID, a.TransactionTime, a.LogID";
		
		$tempCloseBalance = $li->returnResultSet($sql);
		
		$StaffIDToBalance = array();
		$sum_open_balance = 0;
		$sum_close_balance = 0;
		$sum_expense = 0;
		$sum_income = 0;
		
		$StaffIDToOpenBalance = array();
		for($i=0;$i<sizeof($temp2);$i++){
			$user_id = $temp2[$i]['UserID'];
			$balance_after = $temp2[$i]['BalanceAfter'];
			$StaffIDToOpenBalance[$user_id] = $balance_after;
		}
		
		$StaffIDToCloseBalance = array();
		for($i=0;$i<sizeof($tempCloseBalance);$i++){
			$user_id = $tempCloseBalance[$i]['UserID'];
			$balance_after = $tempCloseBalance[$i]['BalanceAfter'];
			$StaffIDToCloseBalance[$user_id] = $balance_after;
		}
		
		for($i=0;$i<sizeof($temp);$i++){
	        list($user_id, $user_name, $amount,$detail,$type, $credit_transaction_type, $student_id, $balance_after, $related_transaction_id) = $temp[$i];
	        if(in_array($type,$ary_types)){
	        	if(!isset($StaffIDToBalance[$user_id])){
	        		$StaffIDToBalance[$user_id] = array();
	        	}
	        //    $StaffIDToBalance[$user_id]['CloseBalance'] = $balance_after;
	            //$sum_close_balance += $balance_after;
	            switch($type){
	                    case 1        :  # 1 - Add Value ( Cash Deposit, PPS, Add Value Machine )
	                                   //$result[$type][$credit_transaction_type] += $amount;
	                                   //$sum_income+=$amount+0;
	                                   $StaffIDToBalance[$user_id]['Income'] += $amount + 0;
	                                   break;
	
	                    case 2        : # 2 - Payment Items
	                                   //$result[$type]["$detail"] += $amount+0;
	                                   //$sum_expense +=$amount+0;
	                                   $StaffIDToBalance[$user_id]['Expense'] += $amount + 0;
	                                   break;
	
	                    case 3        : # 3 - Single Purchase
	                                   //$result[$type]["$detail"] += $amount+0;
	                                   //$sum_expense+=$amount+0;
	                                   if($related_transaction_id == '0') { // photo copier refund
	                                   		$StaffIDToBalance[$user_id]['Expense'] -= $amount + 0;
	                                   }else{
	                                    	$StaffIDToBalance[$user_id]['Expense'] += $amount + 0;
	                                   }
	                                   break;
						
						case 4 		  : # 4 - Transfer TO
										$StaffIDToBalance[$user_id]['Expense'] += $amount + 0;
										break;
						case 5		  : 
										# 5 - Transfer FROM
										$StaffIDToBalance[$user_id]['Income'] += $amount + 0;
										break;
					 	
	                    case 6        : # 6 - Cancel Payment
	                                   //$result[$type]["$detail"] += $amount+0;
	                                   //$sum_income+=$amount+0;
	                                   $StaffIDToBalance[$user_id]['Income'] += $amount + 0;
	                                   break;
	
	                    case 7        : # 7 - Refund
	                                   //$result[$type]["all"] += $amount;
	                                   //$sum_expense +=$amount+0;
	                                    $StaffIDToBalance[$user_id]['Expense'] += $amount + 0;
	                                   break;
	
	                    case 8        : # 8 - PPS Charges
	                                   //$result[$type]["all"] += $amount;
	                                   //$sum_expense +=$amount+0;
	                                    $StaffIDToBalance[$user_id]['Expense'] += $amount + 0;
	                                   break;
												
						case 9        : # 9 - Cancel Cash Deposit
	                                   //$result[$type]["all"] += $amount;
	                                   //$sum_expense +=$amount+0;
	                                    $StaffIDToBalance[$user_id]['Expense'] += $amount + 0;
	                                   break;
	                     
	                    case 10        : # 10 - Donation to school
	                                   //$result[$type]["all"] += $amount;
	                                   //$sum_expense +=$amount+0;
	                                   $StaffIDToBalance[$user_id]['Expense'] += $amount + 0;
	                                   break;
	                    
	                    case 11        : # 11 - POS void transaction (refund)
	                                   //$result[$type]["all"] += $amount+0;
	                                   //$sum_income+=$amount+0;
	                                   $StaffIDToBalance[$user_id]['Income'] += $amount + 0;
	                                   break;
	                    default        : break;
	            }
	        }
		}
		
		$y = '<div class="navigation" style="width:96%;">
				<img height="15" width="15" align="absmiddle" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/nav_arrow.gif"/>';
			$y.="<span><a href=\"javascript:SubmitForm('','');\">".$Lang['ePayment']['IdentityOrYearForm']."</a></span>";
		$y.= '</div><br />';
		$y.="<table width=\"96%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\" align=\"center\">";
		// Staff Name
		$y.="<thead>";
		$y.="<tr>";
		$y.="<td class=\"tablebluetop tabletopnolink\">".$Lang['General']['Name']."</td>";
		// Opening Balance
		$y.="<td class=\"tablebluetop tabletopnolink\">".$Lang['ePayment']['OpenningBalance']."</td>";
		// Total Income
		$y.="<td class=\"tablebluetop tabletopnolink\">$i_Payment_SchoolAccount_TotalIncome</td>";
		// Total Expense
		$y.="<td class=\"tablebluetop tabletopnolink\">$i_Payment_SchoolAccount_TotalExpense</td>";
		// Closing Balance
		$y.="<td class=\"tablebluetop tabletopnolink\">".$Lang['ePayment']['ClosingBalance']."</td>";
		$y.="</tr>";
		$y.="</thead>";
		$y.="<tbody>";
		
		for($j=0;$j<count($staffs);$j++){
			if(!isset($StaffIDToBalance[$staffs[$j]['UserID']])){
				$StaffIDToBalance[$staffs[$j]['UserID']]['CloseBalance'] = 0;
				$StaffIDToBalance[$staffs[$j]['UserID']]['Income'] = 0;
				$StaffIDToBalance[$staffs[$j]['UserID']]['Expense'] = 0;
			}
			
			//$close_balance = $StaffIDToBalance[$staffs[$j]['UserID']]['CloseBalance'];
			$total_income = $StaffIDToBalance[$staffs[$j]['UserID']]['Income'];
			$total_expense = $StaffIDToBalance[$staffs[$j]['UserID']]['Expense'];
			//$open_balance = $close_balance - $total_income + $total_expense + 0;
			$close_balance = $StaffIDToCloseBalance[$staffs[$j]['UserID']];
			//if(round($open_balance,2) == 0.00 && isset($StaffIDToOpenBalance[$staffs[$j]['UserID']])) {
				$open_balance = $StaffIDToOpenBalance[$staffs[$j]['UserID']];
			//}
			
			$sum_open_balance += $open_balance;
			$sum_close_balance += $close_balance;
			$sum_income += $total_income;
			$sum_expense += $total_expense;
			
			$css = $j%2==0?"tablebluerow1":"tablebluerow2";
			// Staff Name
			$y.="<tr class=\"".$css."\">";
			$y.="<td class=\"".$css."\">".$staffs[$j]['UserName']."</td>";
			// Opening Balance
			$y.="<td class=\"".$css."\">$".number_format(sprintf("%.2f",$open_balance),2)."</td>";
			// Total Income
			$y.="<td class=\"".$css."\">$".number_format(sprintf("%.2f",$total_income),2)."</td>";
			// Total Expense
			$y.="<td class=\"".$css."\">$".number_format(sprintf("%.2f",$total_expense),2)."</td>";
			// Closing Balance
			$y.="<td class=\"".$css."\">$".number_format(sprintf("%.2f",$close_balance),2)."</td>";
			$y.="</tr>";
		}
		
	//	$sum_open_balance = $sum_close_balance - $sum_income + $sum_expense + 0;
		$y .= "<tr class=\"tablebluebottom\">";
		$y .= "<td class=\"tabletext\" align=\"right\">".$i_Payment_SchoolAccount_Total."</td>";
		$y .= "<td class=\"tabletext\">$".number_format(sprintf("%.2f",$sum_open_balance),2)."</td>";
		$y .= "<td class=\"tabletext\">$".number_format(sprintf("%.2f",$sum_income),2)."</td>";
		$y .= "<td class=\"tabletext\">$".number_format(sprintf("%.2f",$sum_expense),2)."</td>";
		$y .= "<td class=\"tabletext\">$".number_format(sprintf("%.2f",$sum_close_balance),2)."</td>";
		$y .= "</tr>";
		
		$y.="</tbody>";
		$y.="</table>";
	}else{
		$libyear = new Year($YearID);
		
		$year_name = htmlspecialchars($libyear->YearName,ENT_QUOTES);
		
		$y = '<div class="navigation" style="width:96%;">
				<img height="15" width="15" align="absmiddle" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/nav_arrow.gif"/>';
			$y.="<span><a href=\"javascript:SubmitForm('','');\">".$Lang['ePayment']['IdentityOrYearForm']."</a></span>";
			$y.='<img height="15" width="15" align="absmiddle" src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/nav_arrow.gif"/>';
			$y.= '<span>'.$year_name.'</span>';
		$y.= '</div><br />';
		
		$y.="<table width=\"96%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\" align=\"center\">";
		// Class Name
		$y.="<thead>";
		$y.="<tr>";
		$y.="<td class=\"tablebluetop tabletopnolink\">".$Lang['StudentAttendance']['ClassName']."</td>";
		// Opening Balance
		$y.="<td class=\"tablebluetop tabletopnolink\">".$Lang['ePayment']['OpenningBalance']."</td>";
		// Total Income
		$y.="<td class=\"tablebluetop tabletopnolink\">$i_Payment_SchoolAccount_TotalIncome</td>";
		// Total Expense
		$y.="<td class=\"tablebluetop tabletopnolink\">$i_Payment_SchoolAccount_TotalExpense</td>";
		// Closing Balance
		$y.="<td class=\"tablebluetop tabletopnolink\">".$Lang['ePayment']['ClosingBalance']."</td>";
		$y.="</tr>";
		$y.="</thead>";
		$y.="<tbody>";
		
		$sum_open_balance = 0;
		$sum_close_balance = 0;
		$sum_expense = 0;
		$sum_income = 0;
		
		// Get classes by YearID		
//		$class_list = $fcm->Get_Class_List_By_YearID($YearID);
		$class_list = $libyear->Get_All_Classes(0,$AcademicYearID);
		
		for($j=0;$j<count($class_list);$j++){
			list($year_class_id, $class_title_en, $class_title_ch) = $class_list[$j];
			$class_name = trim(Get_Lang_Selection($class_title_ch,$class_title_en));
			$students = $fcm->Get_Student_By_Class($year_class_id);
			$student_userid = Get_Array_By_Key($students,'UserID');
			if(count($student_userid)>0){
				$userid_cond = " AND a.StudentID IN (".implode(",",$student_userid).")";
			}else{
				$userid_cond = " AND a.StudentID = -1 ";
			}
			
			$date_cond =" AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')>='".$li->Get_Safe_Sql_Query($FromDate)."' AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')<='".$li->Get_Safe_Sql_Query($ToDate)."'";
			$sql="SELECT 
					a.Amount,
					if(f.NoticeID is null, a.Details, concat(f.Title,' - ',a.Details)),
			      	a.TransactionType,
			      	c.RecordType,
					a.StudentID,
					a.BalanceAfter,
					a.RelatedTransactionID  
			    FROM 
					PAYMENT_OVERALL_TRANSACTION_LOG AS a 
					LEFT JOIN INTRANET_USER as u ON u.UserID=a.StudentID
					LEFT JOIN INTRANET_ARCHIVE_USER as u2 ON u2.UserID=a.StudentID 
			        LEFT OUTER JOIN PAYMENT_CREDIT_TRANSACTION as c ON a.RelatedTransactionID = c.TransactionID
			        left join PAYMENT_PAYMENT_ITEMSTUDENT as d on d.PaymentID=a.RelatedTransactionID
					left join PAYMENT_PAYMENT_ITEM as e on e.ItemID=d.ItemID
					left join INTRANET_NOTICE as f on f.NoticeID=e.NoticeID
			    WHERE 
					(u.RecordType='2' OR u2.RecordType='2') $userid_cond 
					$date_cond 
				ORDER BY a.StudentID, a.TransactionTime, a.Details, c.RecordType ";
			
			$temp = $li->returnArray($sql,7);
			
			$sql = "SELECT 
						a.StudentID as UserID,
						a.BalanceAfter 
					FROM 
						PAYMENT_OVERALL_TRANSACTION_LOG AS a 
						LEFT JOIN INTRANET_USER as u ON u.UserID=a.StudentID
						LEFT JOIN INTRANET_ARCHIVE_USER as u2 ON u2.UserID=a.StudentID 
				    WHERE 
						(u.RecordType='2' OR u2.RecordType='2') $userid_cond 
						AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')<'".$li->Get_Safe_Sql_Query($FromDate)."' 
					ORDER BY a.StudentID, a.TransactionTime, a.LogID";
			
			$temp2 = $li->returnResultSet($sql);
			
			$sql = "SELECT 
						a.StudentID as UserID,
						a.BalanceAfter 
					FROM 
						PAYMENT_OVERALL_TRANSACTION_LOG AS a 
						LEFT JOIN INTRANET_USER as u ON u.UserID=a.StudentID
						LEFT JOIN INTRANET_ARCHIVE_USER as u2 ON u2.UserID=a.StudentID 
				    WHERE 
						(u.RecordType='2' OR u2.RecordType='2') $userid_cond 
						AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')<='".$li->Get_Safe_Sql_Query($ToDate)."' 
					ORDER BY a.StudentID, a.TransactionTime, a.LogID";
			
			$tempCloseBalance = $li->returnResultSet($sql);
			
			$open_balance = 0;
			$close_balance = 0;
			$total_income = 0;
			$total_expense = 0;
			
			$UserIDToBalance = array();
			
			$UserIDToOpenBalance = array();
			for($i=0;$i<count($temp2);$i++) {
				$UserIDToOpenBalance[$temp2[$i]['UserID']] = $temp2[$i]['BalanceAfter'];
				if(!isset($UserIDToBalance[$temp2[$i]['UserID']])){
	        		$UserIDToBalance[$temp2[$i]['UserID']] = array();
	        		$UserIDToBalance[$temp2[$i]['UserID']]['OpenBalance'] = 0;
	        		$UserIDToBalance[$temp2[$i]['UserID']]['CloseBalance'] = 0;
	        		$UserIDToBalance[$temp2[$i]['UserID']]['Income'] = 0;
	        		$UserIDToBalance[$temp2[$i]['UserID']]['Expense'] = 0;
	        	}
			}
			
			$UserIDToCloseBalance = array();
			for($i=0;$i<count($tempCloseBalance);$i++) {
				$UserIDToCloseBalance[$tempCloseBalance[$i]['UserID']] = $tempCloseBalance[$i]['BalanceAfter'];
				if(!isset($UserIDToBalance[$tempCloseBalance[$i]['UserID']])){
	        		$UserIDToBalance[$tempCloseBalance[$i]['UserID']] = array();
	        		$UserIDToBalance[$tempCloseBalance[$i]['UserID']]['OpenBalance'] = 0;
	        		$UserIDToBalance[$tempCloseBalance[$i]['UserID']]['CloseBalance'] = 0;
	        		$UserIDToBalance[$tempCloseBalance[$i]['UserID']]['Income'] = 0;
	        		$UserIDToBalance[$tempCloseBalance[$i]['UserID']]['Expense'] = 0;
	        	}
			}
			
			for($i=0;$i<sizeof($temp);$i++){
		        list($amount,$detail,$type, $credit_transaction_type, $student_id, $balance_after, $related_transaction_id) = $temp[$i];
		        if(in_array($type,$ary_types)){
		        	if(!isset($UserIDToBalance[$student_id])){
		        		$UserIDToBalance[$student_id] = array();
		        		$UserIDToBalance[$student_id]['OpenBalance'] = 0;
		        		$UserIDToBalance[$student_id]['CloseBalance'] = 0;
		        		$UserIDToBalance[$student_id]['Income'] = 0;
		        		$UserIDToBalance[$student_id]['Expense'] = 0;
		        	}
		        	$UserIDToBalance[$student_id]['CloseBalance'] = $UserIDToCloseBalance[$student_id];
		            //$close_balance += $balance_after;
		            //$sum_close_balance += $balance_after;
		            switch($type){
		                    case 1        :  # 1 - Add Value ( Cash Deposit, PPS, Add Value Machine )
		                                   //$result[$type][$credit_transaction_type] += $amount;
		                                   //$sum_income+=$amount+0;
		                                   $total_income += $amount + 0;
		                                   $UserIDToBalance[$student_id]['Income'] += $amount + 0;
		                                   break;
		
		                    case 2        : # 2 - Payment Items
		                                   //$result[$type]["$detail"] += $amount+0;
		                                   //$sum_expense +=$amount+0;
		                                   $total_expense += $amount + 0;
		                                   $UserIDToBalance[$student_id]['Expense'] += $amount + 0;
		                                   break;
		
		                    case 3        : # 3 - Single Purchase
		                                   //$result[$type]["$detail"] += $amount+0;
		                                   //$sum_expense+=$amount+0;
		                                   if($related_transaction_id == '0') { // photo copier refund
		                                   		$total_expense -= $amount + 0;
		                                   		$UserIDToBalance[$student_id]['Expense'] -= $amount + 0;
		                                   }else{
		                                   		$total_expense += $amount + 0;
		                                   		$UserIDToBalance[$student_id]['Expense'] += $amount + 0;
		                                   }
		                                   break;
							
							case 4 		  : # 4 - Transfer TO
											$total_expense += $amount + 0;
											$UserIDToBalance[$student_id]['Expense'] += $amount + 0;
											break;
							case 5		  : 
											# 5 - Transfer FROM
											$total_income += $amount + 0;
											$UserIDToBalance[$student_id]['Income'] += $amount + 0;
											break;
						 	
		                    case 6        : # 6 - Cancel Payment
		                                   //$result[$type]["$detail"] += $amount+0;
		                                   //$sum_income+=$amount+0;
		                                   $total_income += $amount + 0;
		                                   $UserIDToBalance[$student_id]['Income'] += $amount + 0;
		                                   break;
		
		                    case 7        : # 7 - Refund
		                                   //$result[$type]["all"] += $amount;
		                                   //$sum_expense +=$amount+0;
		                                   $total_expense += $amount + 0;
		                                   $UserIDToBalance[$student_id]['Expense'] += $amount + 0;
		                                   break;
		
		                    case 8        : # 8 - PPS Charges
		                                   //$result[$type]["all"] += $amount;
		                                   //$sum_expense +=$amount+0;
		                                   $total_expense += $amount + 0;
		                                   $UserIDToBalance[$student_id]['Expense'] += $amount + 0;
		                                   break;
													
							case 9        : # 9 - Cancel Cash Deposit
		                                   //$result[$type]["all"] += $amount;
		                                   //$sum_expense +=$amount+0;
		                                   $total_expense += $amount + 0;
		                                   $UserIDToBalance[$student_id]['Expense'] += $amount + 0;
		                                   break;
		                     
		                    case 10        : # 10 - Donation to school
		                                   //$result[$type]["all"] += $amount;
		                                   //$sum_expense +=$amount+0;
		                                   $total_expense += $amount + 0;
		                                   $UserIDToBalance[$student_id]['Expense'] += $amount + 0;
		                                   break;
		                    
		                    case 11        : # 11 - POS void transaction (refund)
		                                   //$result[$type]["all"] += $amount+0;
		                                   //$sum_income+=$amount+0;
		                                   $total_income+= $amount + 0;
		                                   $UserIDToBalance[$student_id]['Income'] += $amount + 0;
		                                   break;
		                    default        : break;
		            }
		        }
			}
			if(count($UserIDToBalance)>0){
				foreach($UserIDToBalance as $user_id => $balance_arr){
					//$close_balance += $balance_arr['CloseBalance'] + 0;
					$close_balance += $UserIDToCloseBalance[$user_id] + 0;
					//$UserIDToBalance[$user_id]['OpenBalance'] = $balance_arr['CloseBalance'] - $UserIDToBalance[$user_id]['Income'] + $UserIDToBalance[$user_id]['Expense'] + 0;
					//if(round($UserIDToBalance[$user_id]['OpenBalance'],2)==0.00 && isset($UserIDToOpenBalance[$user_id])) {
						$UserIDToBalance[$user_id]['OpenBalance'] = $UserIDToOpenBalance[$user_id] + 0;
					//}
					$open_balance += $UserIDToBalance[$user_id]['OpenBalance'] + 0;
				}
			}
			
			//$open_balance = $close_balance - $total_income + $total_expense + 0;
			
			$css = $j%2==0?"tablebluerow1":"tablebluerow2";
			// Class Name
			$y.="<tr class=\"".$css."\">";
			$y.="<td class=\"".$css."\"><a class=\"tablelink\" href=\"javascript:SubmitForm('".IntegerSafe($YearID)."','".IntegerSafe($year_class_id)."')\">".$class_name."</a></td>";
			// Opening Balance
			$y.="<td class=\"".$css."\">$".number_format(sprintf("%.2f",$open_balance),2)."</td>";
			// Total Income
			$y.="<td class=\"".$css."\">$".number_format(sprintf("%.2f",$total_income),2)."</td>";
			// Total Expense
			$y.="<td class=\"".$css."\">$".number_format(sprintf("%.2f",$total_expense),2)."</td>";
			// Closing Balance
			$y.="<td class=\"".$css."\">$".number_format(sprintf("%.2f",$close_balance),2)."</td>";
			$y.="</tr>";
			
			$sum_open_balance += $open_balance;
			$sum_close_balance += $close_balance;
			$sum_income += $total_income;
			$sum_expense += $total_expense;
		}
		
		//$sum_open_balance = $sum_close_balance - $sum_income + $sum_expense + 0;
		$y .= "<tr class=\"tablebluebottom\">";
		$y .= "<td class=\"tabletext\" align=\"right\">".$i_Payment_SchoolAccount_Total."</td>";
		$y .= "<td class=\"tabletext\">$".number_format(sprintf("%.2f",$sum_open_balance),2)."</td>";
		$y .= "<td class=\"tabletext\">$".number_format(sprintf("%.2f",$sum_income),2)."</td>";
		$y .= "<td class=\"tabletext\">$".number_format(sprintf("%.2f",$sum_expense),2)."</td>";
		$y .= "<td class=\"tabletext\">$".number_format(sprintf("%.2f",$sum_close_balance),2)."</td>";
		$y .= "</tr>";
		
		$y.="</tbody>";
		$y.="</table>";
		
	}
	
}else{
	$y="<table width=\"96%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\" align=\"center\">";
	// Identity or Year Class
	$y.="<thead>";
	$y.="<tr>";
	$y.="<td class=\"tablebluetop tabletopnolink\">".$Lang['ePayment']['IdentityOrYearForm']."</td>";
	// Opening Balance
	$y.="<td class=\"tablebluetop tabletopnolink\">".$Lang['ePayment']['OpenningBalance']."</td>";
	// Total Income
	$y.="<td class=\"tablebluetop tabletopnolink\">$i_Payment_SchoolAccount_TotalIncome</td>";
	// Total Expense
	$y.="<td class=\"tablebluetop tabletopnolink\">$i_Payment_SchoolAccount_TotalExpense</td>";
	// Closing Balance
	$y.="<td class=\"tablebluetop tabletopnolink\">".$Lang['ePayment']['ClosingBalance']."</td>";
	$y.="</tr>";
	$y.="</thead>";
	$y.="<tbody>";
/*
else{
	$ExportColumn = array($Lang['ePayment']['BalanceReport']." ($FromDate to $ToDate)",'','','','');
	$Detail = array();
	$Detail[] = $Lang['ePayment']['IdentityOrYearForm'];
	$Detail[] = $Lang['ePayment']['OpenningBalance'];
	$Detail[] = $i_Payment_SchoolAccount_TotalIncome;
	$Detail[] = $i_Payment_SchoolAccount_TotalExpense;
	$Detail[] = $Lang['ePayment']['ClosingBalance'];
	$Rows=array();
	$Rows[] = $Detail;
}
*/
	
	$sum_open_balance = 0;
	$sum_income = 0;
	$sum_expense = 0;
	$sum_close_balance = 0;
	for($j=0;$j<$numOfYearFormID;$j++)
	{
		$year_form_id = $YearFormID[$j];
		
		if($year_form_id == -1){
			// Identity Staff
			$display_name = $Lang['ePayment']['Staff'];
			$usertype = "1";
			$userid_cond = "";
		}else if($year_form_id == -2){
			// Identity Student
			$display_name =$Lang['ePayment']['Student'];
			$usertype = "2";
			$userid_cond = "";
		}
		else{
			// Class Level
			
			$libyear = new Year($year_form_id);
			$students = $fcm->Get_Student_By_Form($AcademicYearID,array($year_form_id));
	
			$id_list = '';
			$delimiter = '';
			for($i=0;$i<count($students);$i++){
				$id_list .= $delimiter.$students[$i]['UserID'];
				$delimiter = ',';
			}
			
			$display_name = htmlspecialchars($libyear->YearName,ENT_QUOTES);
			
			$usertype = "2";
			$userid_cond = " AND a.StudentID IN (".($id_list != ""? $id_list : "-1").") ";
		}
		
		$date_cond =" AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')>='".$li->Get_Safe_Sql_Query($FromDate)."' AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')<='".$li->Get_Safe_Sql_Query($ToDate)."'";
		$name_field = getNameFieldByLang("u.");
		
		$sql="SELECT 
				a.Amount,
				if(f.NoticeID is null, a.Details, concat(f.Title,' - ',a.Details)),
		      	a.TransactionType,
		      	c.RecordType,
				a.StudentID,
				a.BalanceAfter,
				a.RelatedTransactionID 
		    FROM 
				PAYMENT_OVERALL_TRANSACTION_LOG AS a 
				LEFT JOIN INTRANET_USER as u ON u.UserID=a.StudentID 
				LEFT JOIN INTRANET_ARCHIVE_USER as u2 ON u2.UserID=a.StudentID 
		        LEFT OUTER JOIN PAYMENT_CREDIT_TRANSACTION as c ON a.RelatedTransactionID = c.TransactionID
		        left join PAYMENT_PAYMENT_ITEMSTUDENT as d on d.PaymentID=a.RelatedTransactionID
				left join PAYMENT_PAYMENT_ITEM as e on e.ItemID=d.ItemID
				left join INTRANET_NOTICE as f on f.NoticeID=e.NoticeID
		    WHERE 
				(u.RecordType='".$li->Get_Safe_Sql_Query($usertype)."' OR u2.RecordType='".$li->Get_Safe_Sql_Query($usertype)."') $userid_cond 
				$date_cond 
			ORDER BY a.StudentID, a.TransactionTime, a.Details, c.RecordType ";
		
		$temp = $li->returnArray($sql,7);
		
		$sql = "SELECT 
					a.StudentID as UserID,
					a.BalanceAfter 
				FROM 
					PAYMENT_OVERALL_TRANSACTION_LOG AS a 
					LEFT JOIN INTRANET_USER as u ON u.UserID=a.StudentID 
					LEFT JOIN INTRANET_ARCHIVE_USER as u2 ON u2.UserID=a.StudentID 
			    WHERE 
					(u.RecordType='".$li->Get_Safe_Sql_Query($usertype)."' OR u2.RecordType='".$li->Get_Safe_Sql_Query($usertype)."') $userid_cond 
					AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')<'".$li->Get_Safe_Sql_Query($FromDate)."' 
				ORDER BY a.StudentID, a.TransactionTime, a.LogID";
		
		$temp2 = $li->returnResultSet($sql);
		
		$sql = "SELECT 
					a.StudentID as UserID,
					a.BalanceAfter 
				FROM 
					PAYMENT_OVERALL_TRANSACTION_LOG AS a 
					LEFT JOIN INTRANET_USER as u ON u.UserID=a.StudentID 
					LEFT JOIN INTRANET_ARCHIVE_USER as u2 ON u2.UserID=a.StudentID 
			    WHERE 
					(u.RecordType='".$li->Get_Safe_Sql_Query($usertype)."' OR u2.RecordType='".$li->Get_Safe_Sql_Query($usertype)."') $userid_cond 
					AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')<='".$li->Get_Safe_Sql_Query($ToDate)."' 
				ORDER BY a.StudentID, a.TransactionTime, a.LogID";
		
		$tempCloseBalance = $li->returnResultSet($sql);
		
		$income=0;
		$expense_item=0;
		$expense_single=0;
		$expense_other=0;
		
		$UserIDToBalance = array();
		//$student_balance = array();
		$open_balance = 0;
		$close_balance = 0;
		$total_income = 0;
		$total_expense = 0;
		
		$UserIDToOpenBalance = array();
		for($i=0;$i<count($temp2);$i++) {
			$UserIDToOpenBalance[$temp2[$i]['UserID']] = $temp2[$i]['BalanceAfter'];
			if(!isset($UserIDToBalance[$temp2[$i]['UserID']])){
        		$UserIDToBalance[$temp2[$i]['UserID']] = array();
        		$UserIDToBalance[$temp2[$i]['UserID']]['OpenBalance'] = 0;
        		$UserIDToBalance[$temp2[$i]['UserID']]['CloseBalance'] = 0;
        		$UserIDToBalance[$temp2[$i]['UserID']]['Income'] = 0;
        		$UserIDToBalance[$temp2[$i]['UserID']]['Expense'] = 0;
        	}
		}
		
		$UserIDToCloseBalance = array();
		for($i=0;$i<count($tempCloseBalance);$i++) {
			$UserIDToCloseBalance[$tempCloseBalance[$i]['UserID']] = $tempCloseBalance[$i]['BalanceAfter'];
			if(!isset($UserIDToBalance[$tempCloseBalance[$i]['UserID']])){
        		$UserIDToBalance[$tempCloseBalance[$i]['UserID']] = array();
        		$UserIDToBalance[$tempCloseBalance[$i]['UserID']]['OpenBalance'] = 0;
        		$UserIDToBalance[$tempCloseBalance[$i]['UserID']]['CloseBalance'] = 0;
        		$UserIDToBalance[$tempCloseBalance[$i]['UserID']]['Income'] = 0;
        		$UserIDToBalance[$tempCloseBalance[$i]['UserID']]['Expense'] = 0;
        	}
		}
		
		for($i=0;$i<sizeof($temp);$i++){
	        list($amount,$detail,$type, $credit_transaction_type, $student_id, $balance_after, $related_transaction_id) = $temp[$i];
	        if(in_array($type,$ary_types)){
	        	if(!isset($UserIDToBalance[$student_id])){
	        		$UserIDToBalance[$student_id] = array();
	        		$UserIDToBalance[$student_id]['OpenBalance'] = 0;
	        		$UserIDToBalance[$student_id]['CloseBalance'] = 0;
	        		$UserIDToBalance[$student_id]['Income'] = 0;
	        		$UserIDToBalance[$student_id]['Expense'] = 0;
	        	}
	        	$UserIDToBalance[$student_id]['CloseBalance'] = $UserIDToCloseBalance[$student_id];
	        	
	            //$student_balance[$student_id] = $balance_after;
	            //$close_balance += $balance_after;
	            switch($type){
	                    case 1        :  # 1 - Add Value ( Cash Deposit, PPS, Add Value Machine )
	                                   //$result[$type][$credit_transaction_type] += $amount;
	                                   $income+=$amount+0;
	                                   $UserIDToBalance[$student_id]['Income'] += $amount + 0;
	                                   break;
	
	                    case 2        : # 2 - Payment Items
	                                   //$result[$type]["$detail"] += $amount+0;
	                                   $expense_item +=$amount+0;
	                                   $UserIDToBalance[$student_id]['Expense'] += $amount + 0;
	                                   break;
	
	                    case 3        : # 3 - Single Purchase
	                                   //$result[$type]["$detail"] += $amount+0;
	                                   if($related_transaction_id == '0') { // photo copier refund
	                                   		$expense_single -= $amount+0;
	                                   		$UserIDToBalance[$student_id]['Expense'] -= $amount + 0;
	                                   }else{
	                                   		$expense_single += $amount+0;
	                                   		$UserIDToBalance[$student_id]['Expense'] += $amount + 0;
	                                   }
	                                   break;
						
						case 4 		  : # 4 - Transfer TO
										$expense_other+=$amount+0;
										$UserIDToBalance[$student_id]['Expense'] += $amount + 0;
										break;
						case 5		  : 
										# 5 - Transfer FROM
										$income +=$amount+0;
										$UserIDToBalance[$student_id]['Income'] += $amount + 0;
										break;
					 	
	                    case 6        : # 6 - Cancel Payment
	                                   //$result[$type]["$detail"] += $amount+0;
	                                   $income+=$amount+0;
	                                   $UserIDToBalance[$student_id]['Income'] += $amount + 0;
	                                   break;
	
	                    case 7        : # 7 - Refund
	                                   //$result[$type]["all"] += $amount;
	                                   $expense_other +=$amount+0;
	                                   $UserIDToBalance[$student_id]['Expense'] += $amount + 0;
	                                   break;
	
	                    case 8        : # 8 - PPS Charges
	                                   //$result[$type]["all"] += $amount;
	                                   $expense_other +=$amount+0;
	                                   $UserIDToBalance[$student_id]['Expense'] += $amount + 0;
	                                   break;
												
						case 9        : # 9 - Cancel Cash Deposit
	                                   //$result[$type]["all"] += $amount;
	                                   $expense_other +=$amount+0;
	                                   $UserIDToBalance[$student_id]['Expense'] += $amount + 0;
	                                   break;
	                     
	                    case 10        : # 10 - Donation to school
	                                   //$result[$type]["all"] += $amount;
	                                   $expense_other +=$amount+0;
	                                   $UserIDToBalance[$student_id]['Expense'] += $amount + 0;
	                                   break;
	                    
	                    case 11        : # 11 - POS void transaction (refund)
	                                   //$result[$type]["all"] += $amount+0;
	                                   $income+=$amount+0;
	                                   $UserIDToBalance[$student_id]['Income'] += $amount + 0;
	                                   break;
	                    default        : break;
	            }
	        }
		}
		
		$count=0;
		
		//$close_balance = 0.0;
	/*	if(count($student_balance)>0){
			foreach($student_balance as $val){
				$close_balance += $val;
			}
		}
	*/
		if(count($UserIDToBalance)>0){
			foreach($UserIDToBalance as $user_id => $balance_arr){
				//$close_balance += $balance_arr['CloseBalance'] + 0;
				$close_balance += $UserIDToCloseBalance[$user_id] + 0;
				
				//$UserIDToBalance[$user_id]['OpenBalance'] = $balance_arr['CloseBalance'] - $UserIDToBalance[$user_id]['Income'] + $UserIDToBalance[$user_id]['Expense'] + 0;
				//if(round($UserIDToBalance[$user_id]['OpenBalance'],2)==0.00 && isset($UserIDToOpenBalance[$user_id])) {
					$UserIDToBalance[$user_id]['OpenBalance'] = $UserIDToOpenBalance[$user_id] + 0;
				//}
				
				$open_balance += $UserIDToBalance[$user_id]['OpenBalance'] + 0;
				$total_income += $UserIDToBalance[$user_id]['Income'] + 0;
				$total_expense += $UserIDToBalance[$user_id]['Expense'] + 0;
			}
		}
		
		# Total
	//	$total_income = $income+0;
		
		# Summary
	//	$total_expense = $expense_item + $expense_single + $expense_other+0;
		//$net_income = $total_income - $total_expense;
	//	$open_balance = $close_balance - $total_income + $total_expense + 0;
		
		$css = $j%2==0?"tablebluerow1":"tablebluerow2";
		// Year Form Name
		$y.="<tr class=\"".$css."\">";
		$y.="<td class=\"".$css."\"><a class=\"tablelink\" href=\"javascript:SubmitForm('".IntegerSafe($year_form_id)."','')\">".$display_name."</a></td>";
		// Opening Balance
		$y.="<td class=\"".$css."\">$".number_format(sprintf("%.2f",$open_balance),2)."</td>";
		// Total Income
		$y.="<td class=\"".$css."\">$".number_format(sprintf("%.2f",$total_income),2)."</td>";
		// Total Expense
		$y.="<td class=\"".$css."\">$".number_format(sprintf("%.2f",$total_expense),2)."</td>";
		// Closing Balance
		$y.="<td class=\"".$css."\">$".number_format(sprintf("%.2f",$close_balance),2)."</td>";
		$y.="</tr>";
		/*
		else{
			$Detail = array();
			$Detail[] = $display_name;
			$Detail[] = '$'.number_format($open_balance,2);
			$Detail[] = '$'.number_format($total_income,2);
			$Detail[] = '$'.number_format($total_expense,2);
			$Detail[] = '$'.number_format($close_balance,2);
			$Rows[] = $Detail;
		}
		*/
		
		$sum_open_balance += $open_balance;
		$sum_income += $total_income;
		$sum_expense += $total_expense;
		$sum_close_balance += $close_balance;
	}
	
	$y .= "<tr class=\"tablebluebottom\">";
	$y .= "<td class=\"tabletext\" align=\"right\">".$i_Payment_SchoolAccount_Total."</td>";
	$y .= "<td class=\"tabletext\">$".number_format(sprintf("%.2f",$sum_open_balance),2)."</td>";
	$y .= "<td class=\"tabletext\">$".number_format(sprintf("%.2f",$sum_income),2)."</td>";
	$y .= "<td class=\"tabletext\">$".number_format(sprintf("%.2f",$sum_expense),2)."</td>";
	$y .= "<td class=\"tabletext\">$".number_format(sprintf("%.2f",$sum_close_balance),2)."</td>";
	$y .= "</tr>";
	
	$y .= '</tbody>'."\n";
	$y .= '</table>'."\n";
}


$linterface->LAYOUT_START();
?>	
<style type='text/css' media='print'>
	 .print_hide {display:none;}
	  P.breakhere {page-break-before: always}
	  thead {display: table-header-group;}
</style>

<script type="text/JavaScript" language="JavaScript">
function SubmitForm(YearID,ClassID)
{
	var formObj = document.getElementById('form1');
	var yearIdObj = document.getElementById('YearID');
	var classIdObj = document.getElementById('ClassID');
	
	yearIdObj.value = YearID;
	classIdObj.value = ClassID;
	formObj.submit();
}

function ExportPage(formObj)
{
	var oldTarget = formObj.target;
	var oldAction = formObj.action;
	formObj.action = 'balance_report_export.php';
	formObj.target = '_blank';
	formObj.submit();
	formObj.action = oldAction;
	formObj.target = oldTarget;
}
</script>
<?php
echo '<form id="form1" name="form1" method="post" action="balance_report.php">';

echo '<table width="96%" align="center" class="print_hide" border="0">
			<tr>
				<td>';
			echo '<div class="content_top_tool">
					<div class="Conntent_tool">';
			echo $linterface->GET_LNK_EXPORT("javascript:ExportPage(document.form1);","","","","",1);
			echo 	'</div>';
			echo '</div>';
		echo '</td>';
		echo '<td align="right">';
			echo $linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit2");
		echo '</td>
			</tr>
	  </table>';
echo "<br />";
if($sys_custom['ePayment']['ReportWithSchoolName']){
	$school_name = GET_SCHOOL_NAME();
	echo '<table width="90%" align="center" border="0">
			<tr>
				<td align="center"><h2><b>'.$school_name.'</b></h2></td>
			</tr>
		</table>';
}
echo $y;
echo "<table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\">";
echo "<tr>";
echo "<td class=\"eSportprinttext tabletext\" >&nbsp;</td><td class=\"eSportprinttext tabletext\" align=\"right\">$i_general_report_creation_time : ".date('Y-m-d H:i:s')."</td></tr></table>";
echo "<br />";	

echo '<input type="hidden" name="FromDate" value="'.escape_double_quotes($FromDate).'" />';
echo '<input type="hidden" name="ToDate" value="'.escape_double_quotes($ToDate).'" />';
for($i=0;$i<count($YearFormID);$i++){
	echo '<input type="hidden" name="YearFormID[]" value="'.escape_double_quotes($YearFormID[$i]).'" />';
}
echo '<input type="hidden" id="YearID" name="YearID" value="'.escape_double_quotes($YearID).'" />';
echo '<input type="hidden" id="ClassID" name="ClassID" value="'.escape_double_quotes($ClassID).'" />';
echo '<input type="hidden" id="AcademicYearID" name="AcademicYearID" value="'.escape_double_quotes($AcademicYearID).'" />';

echo '</form>';

$linterface->LAYOUT_STOP();
/*
else{
	$Detail = array();
	$Detail[] = ' ';
	$Detail[] = ' ';
	$Detail[] = ' ';
	$Detail[] = ' ';
	$Detail[] = $i_general_report_creation_time." : ".date('Y-m-d H:i:s');
	$Rows[] = $Detail;
	
	$filename = "Balance_Report_(".$FromDate."_to_".$ToDate.").csv";
	$export_content = $lexport->GET_EXPORT_TXT($Rows, $ExportColumn,"","\r\n","",0,"11");
	$lexport->EXPORT_FILE($filename, $export_content);	
}
*/
intranet_closedb();
?>