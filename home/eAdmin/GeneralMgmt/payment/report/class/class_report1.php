<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

if($ClassName=="" || $FromDate=="" || $ToDate=="" || $format=="")
	header("Location: index.php");

define('MAX_COLUMN',256);  // max. column 256 in excel, minus 2 columns ( username , balance)

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();

$MODULE_OBJ['title'] = $i_Payment_Class_Payment_Report;
$linterface = new interface_html("popup.html");


## get target students data
$order_student = array();
$namefield = getNameFieldWithClassNumberByLang("a.");

if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$archive_namefield = "IF(a.ClassName IS NOT NULL AND a.ClassName<>'' AND a.ClassNumber IS NOT NULL AND a.ClassNumber<>'',CONCAT(a.ChineseName,' (',a.ClassName,'-',a.ClassNumber,')'),a.ChineseName)";
}else $archive_namefield = "IF(a.ClassName IS NOT NULL AND a.ClassName<>'' AND a.ClassNumber IS NOT NULL AND a.ClassNumber<>'',CONCAT(a.EnglishName,' (',a.ClassName,'-',a.ClassNumber,')'),a.EnglishName)";


// get student list from INTRANET_USER
$sql="SELECT a.UserID, $namefield, b.Balance FROM INTRANET_USER AS a LEFT OUTER JOIN PAYMENT_ACCOUNT AS b ON (a.UserID = b.StudentID) WHERE a.ClassName='$ClassName' AND a.RecordType=2 ORDER BY a.ClassNumber";
$temp = $lpayment->returnArray($sql,3);


// get student list from INTRANET_ARCHIVE_USER
$sql="SELECT a.UserID, $archive_namefield, b.Balance FROM INTRANET_ARCHIVE_USER AS a LEFT OUTER JOIN PAYMENT_ACCOUNT AS b ON (a.UserID = b.StudentID) WHERE a.ClassName='$ClassName' AND a.RecordType=2 ORDER BY a.ClassNumber";
$temp2 = $lpayment->returnArray($sql,3);


for($i=0;$i<sizeof($temp);$i++){
	list($student_id,$student_name,$balance) = $temp[$i];
	$student_data[$student_id]['Name'] = $student_name;
	$student_data[$student_id]['Balance'] = $balance;
	$student_data[$student_id]['archived']=0;
	if(!in_array($student_id,$order_student))
		$order_student[] = $student_id;
}
for($i=0;$i<sizeof($temp2);$i++){
	list($student_id,$student_name,$balance) = $temp2[$i];
	$student_data[$student_id]['Name'] = $student_name;
	$student_data[$student_id]['archived']=1;
	$student_data[$student_id]['Balance'] = $balance;
	
	if(!in_array($student_id,$order_student))
		$order_student[] = $student_id;
}


$list = implode(",",$order_student);

$sql="
	SELECT
		c.StudentID,
		d.Name,
		d.DefaultAmount,
		c.ItemID,
		IF(c.RecordStatus=1,'$i_Payment_PresetPaymentItem_PaidCount','$i_Payment_PresetPaymentItem_UnpaidCount')
	FROM 
		PAYMENT_PAYMENT_ITEMSTUDENT AS c LEFT OUTER JOIN
		PAYMENT_PAYMENT_ITEM AS d ON (c.ItemID = d.ItemID )
	WHERE 
		c.StudentID IN ($list) AND 
		d.StartDate<='$ToDate' AND 
		d.EndDate>='$FromDate'
	ORDER BY
		d.StartDate
";

$temp = $lpayment->returnArray($sql,5);

$order_item = array();
for($i=0;$i<sizeof($temp);$i++){
	list($student_id,$item_name,$default_amount,$item_id,$paid_status) = $temp[$i];
	$payment_item[$item_id]['Name'] = $item_name;
	$payment_item[$item_id]['DefaultAmount'] = $default_amount;
	$student_data[$student_id][$item_id]['PaidStatus'] = $paid_status;
	
	if(!in_array($item_id,$order_item))
		$order_item[] = $item_id;
}

if($format==1 && sizeof($order_item)>MAX_COLUMN){ // too many columns
	$linterface->LAYOUT_START();
	echo "<table width=\"90%\" align=\"center\" class=\"print_hide\" border=\"0\">";
	echo "<tr><td align=\"center\" class=\"eSporttdborder eSportprinttext\">$i_Payment_Class_Payment_Report_Warning<br /><br />";
	echo $linterface->GET_ACTION_BTN($button_close, "button", "window.close();","close2");
	echo "</td></tr>";
	echo "</table>";
	$linterface->LAYOUT_STOP();
	exit();
}

############## Built Display Table / CSV content #############
$table="<table width=\"90%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\">";

# table header
$table.="<tr><td class=\"tablebluetop tabletopnolink\" width=\"20%\">&nbsp</td>";
$table.="<td class=\"tablebluetop tabletopnolink\" width=\"10%\" style=\"vertical-align:bottom;horizontal-align:center\">$i_Payment_PrintPage_Outstanding_AccountBalance</td>";
$csv ="\"\",\"$i_Payment_PrintPage_Outstanding_AccountBalance\",";
for($i=0;$i<sizeof($order_item);$i++){
	$item_id = $order_item[$i];
	$item_name = $payment_item[$item_id]['Name'];
	$default_amount = $payment_item[$item_id]['DefaultAmount'];
	$str_default_amount = "$".number_format($default_amount,2);
	$table.="<td class=\"tablebluetop tabletopnolink\" style=\"vertical-align:bottom;horizontal-align:center\">$item_name<br />$str_default_amount</td>";
	$csv.="\"$item_name ".round($default_amount,2)."\"";
	if($i<sizeof($order_item)-1)
		$csv .=",";
}
$table.="</tr>";
$csv.="\n";

# table content
for($i=0;$i<sizeof($order_student);$i++){
	$student_id   = $order_student[$i];
	$student_name = $student_data[$student_id]['Name'];
	
	if($format!=1 && $student_data[$student_id]['archived']==1){
		$student_name = "<font color=red>*</font> <i>$student_name</i>";
	}	
	
	$balance      = $student_data[$student_id]['Balance'];
	$str_balance  = "$".number_format($balance,2);
	
	$css = $i%2==0?"tablebluerow1":"tablebluerow2";
	
	$table.="<tr class=\"$css\"><td class=\"tabletext\">$student_name</td><td class=\"tabletext\">$str_balance</td>";
		$csv .="\"$student_name\",\"".round($balance,2)."\",";
	for($j=0;$j<sizeof($order_item);$j++){
		$item_id = $order_item[$j];
		$paid_status  = $student_data[$student_id][$item_id]['PaidStatus'];
		
		if($paid_status == ""){
			 $paid_status = $i_Payment_PresetPaymentItem_NoNeedToPay; 
			 $str_paid_status = $paid_status;
		}

		if($paid_status == $i_Payment_PresetPaymentItem_PaidCount)
			$str_paid_status = "<font color=blue>$paid_status</font>";
		else if($paid_status == $i_Payment_PresetPaymentItem_UnpaidCount)
			$str_paid_status = "<font color=red>$paid_status</font>";
			
		$table.="<td class=\"tabletext\">$str_paid_status</td>";	
		$csv .="\"$paid_status\"";
		if($j<sizeof($order_item)-1)
			$csv.=",";
	
	}
	$table.="</tr>";
	$csv.="\n";
}

$table.="</table>";

intranet_closedb();

if($format!=1){
	$linterface->LAYOUT_START();
	echo "<br />";
	echo $table;
	echo "<table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\">";
	echo "<tr>";
	echo "<td class=\"eSportprinttext tabletext\" >$i_Payment_Note_StudentRemoved</td><td class=\"eSportprinttext tabletext\" align=\"right\">$i_general_report_creation_time : ".date('Y-m-d H:i:s')."</td></tr></table>";
	echo "<br />";	

	$linterface->LAYOUT_STOP();
}else{
	$filename = "class_payment_report_".$ClassName.".csv";
	if ($g_encoding_unicode) {
		$csv = str_replace("\"","",$csv);
		$csv = str_replace(",","\t",$csv);
		$csv = str_replace("\n","\r\n",$csv);
		$lexport->EXPORT_FILE($filename, $csv);		
	}
	else output2browser($csv,$filename);
}
?>