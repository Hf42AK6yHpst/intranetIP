<?php
# using: 
#####################################
#	Date: 	2017-04-05
#			Added [Payment Status] for display mode [List students by row, payment items by column].
#
#	Date:	2016-11-23	Carlos
#			Include left students.
#
#	Date:	2015-08-06 	Carlos
#			Get and use date range cookies.
#
#	Date:	2013-08-27	Henry
#			improved the target selection to "Form" level
#
#	Date:	2013-01-07	YatWoon
#			add "Data format"
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$lpayment = new libpayment();
$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "StatisticsReport_ClassPayment";

$linterface = new interface_html();

$lclass = new libclass();

//$select_class = $lclass->getSelectClass("name=ClassName",$ClassName,1);

# Class Menu #
//if($targetClass=="") $targetClass = 2;
//	$select_class = $ldiscipline->getSelectClassWithWholeForm("name=\"targetClass\" onChange=\"reloadForm($RecordType)\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes, $i_general_please_select);
//	$stdAry = $ldiscipline->storeStudent('0', $targetClass, $SchoolYear);
$select_class = $lclass->getSelectClassWithWholeForm("name=\"ClassName\"", $ClassName);


# date range
$today_ts = strtotime(date('Y-m-d'));
$date_range_cookies = $lpayment->Get_Report_Date_Range_Cookies();
if($FromDate=="")
	$FromDate = $date_range_cookies['StartDate']!=''? $date_range_cookies['StartDate'] : date('Y-m-d',getStartOfAcademicYear($today_ts));
if($ToDate=="")
	$ToDate = $date_range_cookies['EndDate']!=''? $date_range_cookies['EndDate'] : date('Y-m-d',getEndOfAcademicYear($today_ts));
	
	
$format_array = array(array(0,"Web"),array(1,"CSV"));
$select_format = getSelectByArray($format_array, "name=format",0,0,1);

$display_mode_array= array(array(0,	$i_Payment_Class_Payment_Report_Display_Mode_1),array(1,$i_Payment_Class_Payment_Report_Display_Mode_2));
$select_display_mode=getSelectByArray($display_mode_array,"name='display_mode' onChange='showNotice(this)'",$display_mode,0,1);
if($display_mode==1 && $format!=1) {
	$select_display_mode.= "<span class='extraInfo'><BR> ( $i_Payment_Class_Payment_Report_Notice )</span>";
	
	$select_items_per_page="<SELECT name='ItemPerPage'>";
	$select_items_per_page.="<OPTION value=''>$i_status_all</OPTION>\n";
	for($i=1;$i<21;$i++){
		$select_items_per_page.="<OPTION value='$i'>$i</OPTION>\n";
	}
	$select_items_per_page.="</SELECT>";

}

$TAGS_OBJ[] = array($i_Payment_Class_Payment_Report, "", 0);
$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.datepick.css" type="text/css" />
<script>
function showNotice(obj){

	obj.form.action='';
	obj.form.target='';
	obj.form.submit();
}
	
	function checkDate(obj){
 		if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
		return true;
	}
	
    function checkForm(formObj){
		if(formObj==null)return false;
			fromV = formObj.FromDate;
			toV= formObj.ToDate;
			objClass = formObj.ClassName;
			if(!checkDate(fromV)){
					return false;
			}
			else if(!checkDate(toV)){
						return false;
			}else if(!check_select(objClass,'<?=$i_Discipline_System_alert_PleaseSelectClass?>','')){
				return false;
			}
			return true;
	}

	function submitForm(obj){
		if(checkForm(obj)) {
			if(obj.format.value!=1){
				obj.target='intranet_popup10';
	        	newWindow('about:blank', 10);
        	}else{
	        	obj.target='';
	        }
			obj.submit();
		}
	}

$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
$(document).ready(function(){ 
	$('input#FromDate').datepick({
		dateFormat: 'yy-mm-dd',
		dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		changeFirstDay: false,
		firstDay: 0
		});
	$('input#ToDate').datepick({
		dateFormat: 'yy-mm-dd',
		dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		changeFirstDay: false,
		firstDay: 0
		});
});
</script>
<br />
<form name="form1" action="class_report.php" method="get">
<!-- date range -->
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_UserClassName ?></td>
		<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
			<?=$select_class?><br>
			<input type="checkbox" name="StudentTypesCURRENT" id="StudentTypesCURRENT" value="1" checked="checked" /><label for="StudentTypesCURRENT"><?=$i_Payment_StudentStatus_NonRemoved?></label>
			<input type="checkbox" name="StudentTypesLEFT" id="StudentTypesLEFT" value="1" checked="checked" /><label for="StudentTypesLEFT"><?=$Lang['General']['LeftStudents']?></label>
			<input type="checkbox" name="StudentTypesREMOVED" id="StudentTypesREMOVED" value="1" checked="checked" /><label for="StudentTypesREMOVED"><?=$i_Payment_StudentStatus_Removed?></label>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_Profile_From ?></td>
		<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
			<input class="textboxnum" type="text" name="FromDate" id="FromDate" value="<?=$FromDate?>">
			<span class="tabletextremark">(yyyy-mm-dd)</span>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_Profile_To ?></td>
		<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
			<input class="textboxnum" type="text" name="ToDate" id="ToDate" value="<?=$ToDate?>">
			<span class="tabletextremark">(yyyy-mm-dd)</span>
		</td>
	</tr>
	<tr>
		<Td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Payment_Class_Payment_Report_Display_Mode?>:</td>
		<td valign="top" nowrap="nowrap" class="tabletext" width="70%"><?=$select_display_mode?></td>
	</tr>
<?if($display_mode==1 && $format!=1){?>
	<tr>
		<Td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Payment_Class_Payment_Report_No_of_Items?>:</td>
		<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
			<?=$select_items_per_page?>
		</td>
	</tr>
<?} ?>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_general_Format ?></td>
		<td valign="top" nowrap="nowrap" class="tabletext" width="70%"><?=$select_format?></td>
	</tr>
	
<?if($display_mode==0){?>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['ePayment']['PaymentStatus']?></td>
		<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
			<select name="payment_status" id="payment_status">
				<option value="" <?=$payment_status==""?"selected":""?>><?=$Lang['General']['All']?></option>
				<option value="1" <?=$payment_status=="1"?"selected":""?>><?=$i_Payment_PaymentStatus_Paid?></option>
				<option value="0" <?=$payment_status=="0"?"selected":""?>><?=$i_Payment_PaymentStatus_Unpaid?></option>
			</select>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['General']['DataFormat']?></td>
		<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
			<input type="radio" name="data_format" value="1" id="data_format1" checked><label for="data_format1"><?=$Lang['General']['Symbol']?></label> 
			<input type="radio" name="data_format" value="2" id="data_format2"><label for="data_format2"><?=$i_Payment_Field_Amount?></label>
		</td>
	</tr>
<? } ?>
	
	<tr>
		<td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	</tr>
	<tr>
		<td colspan="2" class="tabletext" align="center">
			<?= $linterface->GET_ACTION_BTN($button_submit, "button", "submitForm(document.form1);","submit2") ?>
		</td>
	</tr>
</table>
</form>
<?
$linterface->LAYOUT_STOP(); 
intranet_closedb();
?>