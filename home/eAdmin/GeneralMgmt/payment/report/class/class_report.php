<?php
//editing by 

########################################################
#	Date: 	2017-06-08  Carlos
#			Fixed credit amount by deducting cancelled credit deposits.
#	Date:	2017-04-05	Carlos
#			Added [Payment Status] for display mode [List students by row, payment items by column].
#	Date:	2017-02-10  Carlos
#			$sys_custom['ePayment']['HartsPreschool'] - display STRN.
#	Date:	2016-11-23	Carlos
#			added [Left student(s)] target option.
#	Date:	2016-08-24 	Carlos
#			$sys_custom['ePayment']['CashDepositMethod'] - added more credit methods [Bank transfer, Cheque deposit].
#	Date:	2015-08-24 	Carlos
#			If checked Deleted students, left students should also be fetched. 
#	Date:	2015-08-06	Carlos
#			Set date range cookies.
#	Date:	2015-07-22  Carlos 
#			$sys_custom['ePayment']['CreditMethodAutoPay'] - added credit type Auto-pay.
#	Date:	2015-07-07  Carlos
#			Added total ePOS amount for [Display Mode: List students by row, payment items by column] and [Data format: Amount]
#	Date:	2015-02-26	Carlos
#			$sys_custom['ePayment']['ReportWithSchoolName'] - display school name.
#	Date:	2013-10-30	Carlos
#			KIS - hide [Current Balance] and Credit related data
#
#	Date:	2013-10-21	YatWoon
#			fixed: removed left student records [Case#2013-1010-1302-23132]
#
#	Date:	2013-08-27	Henry
#			improved the target selection to "Form" level
#
#	Date:	2013-01-07	YatWoon
#			add "Data format"
#
#	Date:	2011-11-18	YatWoon
#			Improved: display notice title in the payment item [Case#2011-1115-1426-21072]
#
########################################################


$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

if($ClassName=="" || $FromDate=="" || !intranet_validateDate($FromDate) || $ToDate=="" || !intranet_validateDate($ToDate) || $format=="")
	header("Location: index.php");

define('MAX_COLUMN',256);  // max. column 256 in excel, minus 2 columns ( username , balance)
if($intranet_session_language=="en")
	define('MAX_STUDENT_PER_PAGE',20);	
else 
	define('MAX_STUDENT_PER_PAGE',45);	
	
intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$isKIS = $_SESSION["platform"]=="KIS" && $sys_custom['ePayment']['KIS_NoBalance'];

$lpayment = new libpayment();
$lexport = new libexporttext();

$MODULE_OBJ['title'] = $i_Payment_Class_Payment_Report;
$linterface = new interface_html("popup.html");
$lclass = new libclass();

if($_REQUEST['FromDate'] != '' && $_REQUEST['ToDate']!=''){
	$lpayment->Set_Report_Date_Range_Cookies($_REQUEST['FromDate'], $_REQUEST['ToDate']);
}

## get target students data
$order_student = array();
$namefield = getNameFieldWithClassNumberByLang("a.");
$classNamefield = "IF(a.ClassName IS NOT NULL AND a.ClassName<>'' AND a.ClassNumber IS NOT NULL AND a.ClassNumber>=0,a.ClassName, '".$Lang['General']['EmptySymbol']."')";
$classNumberfield = "IF(a.ClassName IS NOT NULL AND a.ClassName<>'' AND a.ClassNumber IS NOT NULL AND a.ClassNumber>=0,a.ClassNumber, '".$Lang['General']['EmptySymbol']."')";

if($display_mode!=1){
	//$namefield = getNameFieldWithClassNumberByLang("a.");
	$namefield = getNameFieldByLang("a.");
//	$classNamefield = "IF(a.ClassName IS NOT NULL AND a.ClassName<>'' AND a.ClassNumber IS NOT NULL AND a.ClassNumber>=0,a.ClassName, '".$Lang['General']['EmptySymbol']."')";
//	$classNumberfield = "IF(a.ClassName IS NOT NULL AND a.ClassName<>'' AND a.ClassNumber IS NOT NULL AND a.ClassNumber>=0,a.ClassNumber, '".$Lang['General']['EmptySymbol']."')";
//	if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
//		$archive_namefield = "IF(a.ClassName IS NOT NULL AND a.ClassName<>'' AND a.ClassNumber IS NOT NULL AND a.ClassNumber<>'',CONCAT(a.ChineseName,' (',a.ClassName,'-',a.ClassNumber,')'),a.ChineseName)";
//	}else $archive_namefield = "IF(a.ClassName IS NOT NULL AND a.ClassName<>'' AND a.ClassNumber IS NOT NULL AND a.ClassNumber<>'',CONCAT(a.EnglishName,' (',a.ClassName,'-',a.ClassNumber,')'),a.EnglishName)";
	if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
		$archive_namefield = "a.ChineseName";
	}else $archive_namefield = "a.EnglishName";
}else{
	$break = $format==1?" / ":"<BR>";
	$namefield = "CONCAT(
					IF(a.ClassName IS NOT NULL AND a.ClassName<>'',a.ClassName,''), 
					IF(a.ClassNumber IS NOT NULL AND a.ClassNumber>=0,CONCAT('$break',a.ClassNumber, '$break'),''), 
					".($sys_custom['ePayment']['HartsPreschool']? " IF(a.STRN IS NOT NULL,CONCAT(a.STRN,'$break'),''), ":"")." 
					".getNameFieldByLang("a.")."
				)";
	
	if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
		$archive_namefield = "IF(a.ClassNumber IS NOT NULL AND a.ClassNumber>=0,CONCAT(a.ClassName,'$break',a.ClassNumber,'$break',a.ChineseName),a.ChineseName)";
	}else $archive_namefield = "IF(a.ClassNumber IS NOT NULL AND a.ClassNumber>=0,CONCAT(a.ClassName,'$break',a.ClassNumber,'$break',a.EnglishName),a.EnglishName)";
	
	/*
	$namefield = getNameFieldByLang("a.");
	
	if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
		$archive_namefield = "a.ChineseName";
	}else $archive_namefield = "a.EnglishName";
	*/
}
//debug_pr(str_replace("::","",$ClassName));
if (strpos($ClassName,'::') !== false) {
    $classID = str_replace("::","",$ClassName);
	$ClassName = $lclass->getClassName($classID);
	$tempClassName = '"'.$lpayment->Get_Safe_Sql_Query($ClassName).'"';
}
else{
	$AcademicYearID = Get_Current_Academic_Year_ID();
	$sql="SELECT YearClassID FROM YEAR_CLASS WHERE AcademicYearID = '".$lpayment->Get_Safe_Sql_Query($AcademicYearID)."' AND YearID='".$lpayment->Get_Safe_Sql_Query($ClassName)."'";
	//debug_pr($sql);
	$temp3 = $lpayment->returnArray($sql,3);
	//debug_pr($temp);
	$ClassName = '';
	$tempClassName = "'";
	for($i=0;  $i<count($temp3);$i++){
		$classID = $temp3[$i]["YearClassID"];
		//debug_pr($lclass->getClassName($classID));
		if($lclass->getClassName($classID) != NULL){
			if($i>0){
				$ClassName .=', ';
				$tempClassName .="','";
			}
			//debug_pr($lclass->getClassName($classID));
			$ClassName .= $lclass->getClassName($classID);
			$tempClassName .= $lpayment->Get_Safe_Sql_Query( $lclass->getClassName($classID) );
		}
	}
	$tempClassName .= "'";
	//debug_pr($tempClassName);
	//debug_pr($ClassName);
}
if($ClassName == "")
	$tempClassName = -1;

//debug_pr($ClassName);
if ($StudentTypesCURRENT || $StudentTypesLEFT)
{
	// get student list from INTRANET_USER
	//$sql="SELECT a.UserID, $namefield, b.Balance FROM INTRANET_USER AS a LEFT OUTER JOIN PAYMENT_ACCOUNT AS b ON (a.UserID = b.StudentID) LEFT OUTER JOIN YEAR_CLASS_USER as c ON (a.UserID = c.UserID) WHERE c.YearClassID='$classID' AND a.RecordType=2 ORDER BY a.ClassNumber";
	//$sql="SELECT a.UserID, $namefield, b.Balance FROM INTRANET_USER AS a LEFT OUTER JOIN PAYMENT_ACCOUNT AS b ON (a.UserID = b.StudentID) WHERE a.ClassName IN ($tempClassName) AND  a.RecordType=2 ORDER BY a.ClassNumber";
	$status_ary = array();
	if($StudentTypesCURRENT) $status_ary = array_merge($status_ary, array(0,1,2));
	if($StudentTypesLEFT) $status_ary = array_merge($status_ary, array(3));
	$sql="SELECT a.UserID, $namefield as DisplayName, $classNamefield, $classNumberfield, b.Balance, a.STRN FROM INTRANET_USER AS a LEFT OUTER JOIN PAYMENT_ACCOUNT AS b ON (a.UserID = b.StudentID) LEFT OUTER JOIN YEAR_CLASS_USER as c ON (a.UserID = c.UserID) LEFT OUTER JOIN YEAR_CLASS as d ON (c.YearClassID = d.YearClassID) WHERE a.ClassName IN ($tempClassName) AND  a.RecordType=2 ";
	$sql .= " and a.RecordStatus in (".implode(",",$status_ary).") ";
	$sql .= " ORDER BY a.ClassName, a.ClassNumber+0, DisplayName";
	
	$temp = $lpayment->returnArray($sql,3);
}

if ($StudentTypesREMOVED)
{
	// get student list from INTRANET_ARCHIVE_USER
	//$sql="SELECT a.UserID, $archive_namefield, b.Balance FROM INTRANET_ARCHIVE_USER AS a LEFT OUTER JOIN PAYMENT_ACCOUNT AS b ON (a.UserID = b.StudentID) WHERE a.ClassName IN ($tempClassName) AND a.RecordType=2 ORDER BY a.ClassNumber";
	$sql="SELECT a.UserID, $archive_namefield as DisplayName, $classNamefield, $classNumberfield, b.Balance, a.STRN FROM INTRANET_ARCHIVE_USER AS a LEFT OUTER JOIN PAYMENT_ACCOUNT AS b ON (a.UserID = b.StudentID) LEFT OUTER JOIN YEAR_CLASS_USER as c ON (a.UserID = c.UserID) LEFT OUTER JOIN YEAR_CLASS as d ON (c.YearClassID = d.YearClassID) WHERE a.ClassName IN ($tempClassName) AND  a.RecordType=2 ORDER BY a.ClassName, a.ClassNumber+0, DisplayName";
	$temp2 = $lpayment->returnArray($sql,3);
}

for($i=0;$i<sizeof($temp);$i++){
	list($student_id,$student_name,$class_name,$class_number,$balance) = $temp[$i];
	$student_data[$student_id]['Name'] = $student_name;
	$student_data[$student_id]['ClassName'] = $class_name;
	$student_data[$student_id]['ClassNumber'] = $class_number;
	$student_data[$student_id]['Balance'] = $balance;
	$student_data[$student_id]['STRN'] = $temp[$i]['STRN'];
	$student_data[$student_id]['archived']=0;
	if(!in_array($student_id,$order_student))
		$order_student[] = $student_id;
}
for($i=0;$i<sizeof($temp2);$i++){
	list($student_id,$student_name,$class_name,$class_number,$balance) = $temp2[$i];
	$student_data[$student_id]['Name'] = $student_name;
	$student_data[$student_id]['ClassName'] = $class_name;
	$student_data[$student_id]['ClassNumber'] = $class_number;
	$student_data[$student_id]['archived']=1;
	$student_data[$student_id]['Balance'] = $balance;
	$student_data[$student_id]['STRN'] = $temp2[$i]['STRN'];
	if(!in_array($student_id,$order_student))
		$order_student[] = $student_id;
}

$list = implode(",",$order_student);

$sql="
	SELECT
		c.StudentID,
		if(d.NoticeID is null, d.Name, concat(e.Title, ' - ' ,d.Name)),
		d.DefaultAmount,
		c.ItemID,
		IF(c.RecordStatus=1,'$i_Payment_PresetPaymentItem_PaidCount','$i_Payment_PresetPaymentItem_UnpaidCount'),
		IF(c.RecordStatus=1,c.Amount,".($display_mode == "0" && $payment_status != ''?"c.Amount":"0").")
	FROM 
		PAYMENT_PAYMENT_ITEMSTUDENT AS c 
		LEFT JOIN PAYMENT_PAYMENT_ITEM AS d ON (c.ItemID = d.ItemID )
		LEFT JOIN INTRANET_NOTICE as e on e.NoticeID=d.NoticeID
	WHERE 
		c.StudentID IN ($list) AND 
		d.StartDate<='".$lpayment->Get_Safe_Sql_Query($ToDate)."' AND 
		d.EndDate>='".$lpayment->Get_Safe_Sql_Query($FromDate)."' ";
if($display_mode == "0" && $payment_status != ''){
	$sql .= " AND c.RecordStatus='".$lpayment->Get_Safe_Sql_Query($payment_status)."' ";
}
$sql.=" ORDER BY
		d.StartDate
";
//debug_pr($sql);
$temp = $lpayment->returnArray($sql);

$order_item = array();
for($i=0;$i<sizeof($temp);$i++){
	list($student_id,$item_name,$default_amount,$item_id,$paid_status,$paid_amount) = $temp[$i];
	$payment_item[$item_id]['Name'] = $item_name;
	$payment_item[$item_id]['DefaultAmount'] = $default_amount;
	$student_data[$student_id][$item_id]['PaidStatus'] = $paid_status;
	$student_data[$student_id][$item_id]['PaidAmount'] = $paid_amount;
	$student_total_amount[$student_id] += $paid_amount;
	
	if(!in_array($item_id,$order_item))
		$order_item[] = $item_id;
}

############### Add value data [start]
$add_value_data = array();
$total_add_value = array();
$payment_method_type = array();
$sql = "select 
			a.StudentID,
			a.Amount,
			a.RecordType,
			a.PPSType,
			b.Amount
		from 
			PAYMENT_CREDIT_TRANSACTION as a
			left join PAYMENT_OVERALL_TRANSACTION_LOG as b on (b.RelatedTransactionID=a.TransactionID and b.TransactionType=8)
		where 
			a.StudentID IN ($list) AND 
			a.TransactionTime >='".$lpayment->Get_Safe_Sql_Query($FromDate)." 00:00:00' and a.TransactionTime <='".$lpayment->Get_Safe_Sql_Query($ToDate)." 23:59:59' 
		";
$temp = $lpayment->returnArray($sql);

// cancelled credit deposit
$sql = "select 
			a.StudentID,
			b.Amount,
			a.RecordType,
			a.PPSType 
		from 
			PAYMENT_CREDIT_TRANSACTION as a
			inner join PAYMENT_OVERALL_TRANSACTION_LOG as b on (b.RelatedTransactionID=a.TransactionID and b.TransactionType=9)
		where 
			a.StudentID IN ($list) AND 
			a.TransactionTime >='".$lpayment->Get_Safe_Sql_Query($FromDate)." 00:00:00' and a.TransactionTime <='".$lpayment->Get_Safe_Sql_Query($ToDate)." 23:59:59' ";
$cancelled_deposits = $lpayment->returnResultSet($sql);

for($i=0;$i<sizeof($temp);$i++){
	list($student_id,$add_amount,$add_recordtype,$pps_type,$charge) = $temp[$i];
	if($add_recordtype==1 && $pps_type==0)
	{
		$add_value_data[$student_id][1] += $add_amount;
		$total_add_value[$student_id] += $add_amount;
		$add_value_data_charge[$student_id][1] += $charge;
		$payment_method_type[1]=1;
		$student_total_amount[$student_id]+=$charge;
		
	}
	else if($add_recordtype==1 && $pps_type==1)
	{
		$add_value_data[$student_id][2] += $add_amount;
		$total_add_value[$student_id] += $add_amount;
		$add_value_data_charge[$student_id][2] += $charge;
		$payment_method_type[2]=1;
		$student_total_amount[$student_id]+=$charge;
	}
	else if($add_recordtype==2)
	{
		$add_value_data[$student_id][3] += $add_amount;
		$total_add_value[$student_id] += $add_amount;
		$payment_method_type[3]=1;
	}else if($add_recordtype==4){
		$add_value_data[$student_id][5] += $add_amount;
		$total_add_value[$student_id] += $add_amount;
		$payment_method_type[5]=1;
	}else if($add_recordtype == 6){
		$add_value_data[$student_id][6] += $add_amount;
		$total_add_value[$student_id] += $add_amount;
		$payment_method_type[6]=1;
	}else if($add_recordtype == 7){
		$add_value_data[$student_id][7] += $add_amount;
		$total_add_value[$student_id] += $add_amount;
		$payment_method_type[7]=1;
	}
	else
	{
		$add_value_data[$student_id][4] += $add_amount;
		$total_add_value[$student_id] += $add_amount;
		$payment_method_type[4]=1;
	}
}
// deduct cancelled credit deposits
for($i=0;$i<sizeof($cancelled_deposits);$i++){
	$student_id = $cancelled_deposits[$i]['StudentID'];
	$add_amount = $cancelled_deposits[$i]['Amount'];
	$add_recordtype = $cancelled_deposits[$i]['RecordType'];
	$pps_type = $cancelled_deposits[$i]['PPSType'];
	
	if($add_recordtype==1 && $pps_type==0)
	{
		$add_value_data[$student_id][1] -= $add_amount;
		$total_add_value[$student_id] -= $add_amount;
		$payment_method_type[1]=1;
	}
	else if($add_recordtype==1 && $pps_type==1)
	{
		$add_value_data[$student_id][2] -= $add_amount;
		$total_add_value[$student_id] -= $add_amount;
		$payment_method_type[2]=1;
	}
	else if($add_recordtype==2)
	{
		$add_value_data[$student_id][3] -= $add_amount;
		$total_add_value[$student_id] -= $add_amount;
		$payment_method_type[3]=1;
	}else if($add_recordtype==4){
		$add_value_data[$student_id][5] -= $add_amount;
		$total_add_value[$student_id] -= $add_amount;
		$payment_method_type[5]=1;
	}else if($add_recordtype == 6){
		$add_value_data[$student_id][6] -= $add_amount;
		$total_add_value[$student_id] -= $add_amount;
		$payment_method_type[6]=1;
	}else if($add_recordtype == 7){
		$add_value_data[$student_id][7] -= $add_amount;
		$total_add_value[$student_id] -= $add_amount;
		$payment_method_type[7]=1;
	}
	else
	{
		$add_value_data[$student_id][4] -= $add_amount;
		$total_add_value[$student_id] -= $add_amount;
		$payment_method_type[4]=1;
	}
}
############### Add value data [end]


############### [Begin] ePOS transaction data ###############
if($plugin['ePOS'])
{
	$pos_data = array(); // user id to ePOS total amount
	
	$sql = "SELECT 
				t.StudentID,
				t.Amount,
				t.TransactionTime 
			FROM PAYMENT_OVERALL_TRANSACTION_LOG as t 
			INNER JOIN POS_TRANSACTION t2 ON t2.LogID=t.LogID 
			WHERE t.StudentID IN ($list) AND t.TransactionType='3' AND (t.TransactionTime BETWEEN '".$lpayment->Get_Safe_Sql_Query($FromDate)." 00:00:00' AND '".$lpayment->Get_Safe_Sql_Query($ToDate)." 23:59:59') AND t2.VoidLogID IS NULL";
	$pos_records = $lpayment->returnArray($sql);
	$pos_record_count = count($pos_records);
	
	for($i=0;$i<$pos_record_count;$i++){
		$pos_data[$pos_records[$i]['StudentID']] += $pos_records[$i]['Amount'];
	}
}
############### [End]   ePOS transaction data ############### 


if($format==1 && sizeof($order_item)>MAX_COLUMN){ // too many columns
	$linterface->LAYOUT_START();
	echo "<br />";
	echo $table;
	echo "<table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\">";
	echo "<tr>";
	echo "<td class=\"eSportprinttext tabletext\" >$i_Payment_Note_StudentRemoved</td><td class=\"eSportprinttext tabletext\" align=\"right\">$i_general_report_creation_time : ".date('Y-m-d H:i:s')."</td></tr></table>";
	echo "<br />";	

	$linterface->LAYOUT_STOP();
	exit();
}

############## Built Display Table / CSV content #############
//$table="<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>";
$header="<table width=\"90%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\">
	<Tr><Td class=\"$css_text\"><b>$i_ClassName</b>: ".intranet_htmlspecialchars($ClassName)."</td></tr>
	<tr><Td class=\"$css_text\"><b>$i_Payment_Receipt_Date</b>: ".intranet_htmlspecialchars($FromDate)." <span class=\"$css_text\"><b>$i_Profile_To</b></span> ".intranet_htmlspecialchars($ToDate)."</td></tr>";
if($display_mode == "0" && $payment_status != ''){
	$header.="<tr><td><b>".$Lang['ePayment']['PaymentStatus']."</b>: ".($payment_status == "1"?$i_Payment_PaymentStatus_Paid:$i_Payment_PaymentStatus_Unpaid)."</td></tr>";
}
$header.="<tr><Td>&nbsp;</td></tr>
";

if($data_format==1)
{
	$header .="
		<tr><td class=\"$css_text\"><font style=\"font-family:Symbol;\">&Ouml;</font> : ".$i_Payment_PresetPaymentItem_PaidCount." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;X : $i_Payment_PresetPaymentItem_UnpaidCount &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-- : $i_Payment_Class_Payment_Report_NoNeedToPay</td></tr>
		<tr><Td>&nbsp;</td></tr>
	";
}
$header .="</table>";		

$csv="\"$i_ClassName\"\t\"$ClassName\"\r\n";
$csv.="\"$i_Payment_Receipt_Date\"\t\"$FromDate $i_Profile_To $ToDate\"\r\n";

if($display_mode!=1)		# Y: Student Name / X: Item Name
{ 
	//$table="<table width=90% border=0  cellpadding=2 cellspacing=0 align='center' class='$css_table'>";
	$table="<table class=\"common_table_list\">";
	
	//$csv .="\"\"\t\"$i_Payment_PrintPage_Outstanding_AccountBalance\"\t";
	if($isKIS){
		$csv .="\"".$Lang['StudentAttendance']['Class']."\"\t\"".$Lang['StudentAttendance']['ClassNumber'].($sys_custom['ePayment']['HartsPreschool']?"\"\t\"".$i_STRN:"")."\"\t\"".$Lang['StudentAttendance']['StudentName']."\"\t";
	}else{
		$csv .="\"".$Lang['StudentAttendance']['Class']."\"\t\"".$Lang['StudentAttendance']['ClassNumber'].($sys_custom['ePayment']['HartsPreschool']?"\"\t\"".$i_STRN:"")."\"\t\"".$Lang['StudentAttendance']['StudentName']."\"\t\"$i_Payment_PrintPage_Outstanding_AccountBalance\"\t";
	}
	if($data_format==2)
	{
		if(!$isKIS)
		{
			if($payment_method_type[1]+$payment_method_type[2]+$payment_method_type[3]+$payment_method_type[4]+$payment_method_type[5]+$payment_method_type[6]+$payment_method_type[7] > 1)
			{
				$csv .= "\"". $Lang['ePayment']['TotalAddValue'] ."\"\t";
			}
			
			if($payment_method_type[1])
			{
				$csv .= "\"". $Lang['ePayment']['AddValue']."-".$Lang['ePayment']['PPSIEPS'] ."\"\t";
			}
			if($payment_method_type[2])
			{
				$csv .= "\"". $Lang['ePayment']['AddValue']."-".$Lang['ePayment']['PPSCounterBill'] ."\"\t";
			}
			if($payment_method_type[3])
			{
				$csv .= "\"". $Lang['ePayment']['AddValue']."-".$Lang['Payment']['CashDeposit'] ."\"\t";
			}
			if($payment_method_type[4])
			{
				$csv .= "\"". $Lang['ePayment']['AddValue']."-".$i_Payment_Credit_TypeAddvalueMachine ."\"\t";
			}
			if($sys_custom['ePayment']['CreditMethodAutoPay'] && $payment_method_type[5])
			{
				$csv .= "\"". $Lang['ePayment']['AddValue']."-".$Lang['ePayment']['AutoPay'] ."\"\t";
			}
			if($sys_custom['ePayment']['CashDepositMethod'] && $payment_method_type[6])
			{
				$csv .= "\"". $Lang['ePayment']['AddValue']."-".$Lang['ePayment']['BankTransfer']."\"\t";
			}
			if($sys_custom['ePayment']['CashDepositMethod'] && $payment_method_type[7])
			{
				$csv .= "\"". $Lang['ePayment']['AddValue']."-".$Lang['ePayment']['ChequeDeposit']."\"\t";
			}
		}
		
		if($plugin['ePOS']){
			$csv .= "\"".$Lang['ePayment']['TotalPOSAmount']."\"\t";
		}

		if(sizeof($order_item)+$payment_method_type[1]+$payment_method_type[2] > 0)
		{
			$csv .= "\"". $Lang['ePayment']['TotalPaidAmount'] ."\"\t";
		}
		if(!$isKIS)
		{	
			if($payment_method_type[1])
			{
				$csv .= "\"". $Lang['ePayment']['PPSCharge'] ."(".$Lang['ePayment']['PPSIEPS'].")\"\t"; 
			}
			if($payment_method_type[2])
			{
				$csv .= "\"". $Lang['ePayment']['PPSCharge'] ."(".$Lang['ePayment']['PPSCounterBill'].")\"\t"; 
			}
		}
	}
	
	for($i=0;$i<sizeof($order_item);$i++){
		$item_id = $order_item[$i];
		$item_name = $payment_item[$item_id]['Name'];
		$default_amount = $payment_item[$item_id]['DefaultAmount'];
		$str_default_amount = $lpayment->getWebDisplayAmountFormat($default_amount);
		$payment_item_table.="<th class=\"tablebluetop tabletopnolink\" style=\"vertical-align:bottom;horizontal-align:center;text-align: center;\">$item_name<Br>$str_default_amount</th>";
		$csv.="\"$item_name ".$lpayment->getExportAmountFormat($default_amount)."\"";
		if($i<sizeof($order_item)-1)
			$csv .="\t";
	}
	
	
	# table header
	if($data_format==2)
	{
		$table .="<thead>";
		$table .="<tr>";
		$table .="<th width=\"5%\" rowspan=\"2\">".$Lang['StudentAttendance']['Class']."</th>
				  <th width=\"5%\" rowspan=\"2\">".$Lang['StudentAttendance']['ClassNumber']."</th>";
		if($sys_custom['ePayment']['HartsPreschool']){
			$table .= "<th width=\"5%\" rowspan=\"2\">".$i_STRN."</th>";
		}
		$table .="<th width=\"5%\" rowspan=\"2\">".$Lang['StudentAttendance']['StudentName']."</th>";
		if(!$isKIS)
		{
			$table .="<th width=\"10%\" rowspan=\"2\" class=\"sub_row_top\" style=\"vertical-align:bottom;horizontal-align:center\">". $Lang['ePayment']['CurrentAccountBalance'] ."</th>";
			
			if($payment_method_type[1]+$payment_method_type[2]+$payment_method_type[3]+$payment_method_type[4]+$payment_method_type[5]+$payment_method_type[6]+$payment_method_type[7] > 1)
				$table.="<th width=\"10%\" rowspan=\"2\" class=\"sub_row_top\" style=\"vertical-align:bottom;horizontal-align:center\">". $Lang['ePayment']['TotalAddValue'] ."</th>";
			if($payment_method_type[1]+$payment_method_type[2]+$payment_method_type[3]+$payment_method_type[4]+$payment_method_type[5]+$payment_method_type[6]+$payment_method_type[7] > 0)
				$table .="<th width=\"10%\" colspan=\"". ($payment_method_type[1]+$payment_method_type[2]+$payment_method_type[3]+$payment_method_type[4]+$payment_method_type[5]+$payment_method_type[6]+$payment_method_type[7])."\" style=\"text-align: center\">". $Lang['ePayment']['AddValue'] ."</th>";
			
		}
		if($plugin['ePOS']){
			$table .= "<th width=\"10%\" class=\"sub_row_top\" rowspan=\"2\" style=\"vertical-align:bottom;horizontal-align:center;text-align: center;\">".$Lang['ePayment']['TotalPOSAmount']."</th>";
		}
		if(sizeof($order_item)+$payment_method_type[1]+$payment_method_type[2] > 0)
		{
			$table.="<th width=\"10%\" rowspan=\"2\" class=\"sub_row_top\" style=\"vertical-align:bottom;horizontal-align:center\">". $Lang['ePayment']['TotalPaidAmount'] ."</th>";
			$table .="<th width=\"10%\" colspan=\"". (sizeof($order_item)+$payment_method_type[1]+$payment_method_type[2]) ."\" style=\"text-align: center\">". $i_Payment_Field_PaymentItem ."</th>";
		}

		
		$table .="</tr>";
		
		$table .="<tr>";
		if(!$isKIS)
		{
			if($payment_method_type[1])
			{
				$table.="<th width=\"10%\" class=\"tablebluetop tabletopnolink\" style=\"vertical-align:bottom;horizontal-align:center;text-align: center;\">". $Lang['ePayment']['PPSIEPS']."</th>";
			}
			if($payment_method_type[2])
			{
				$table.="<th width=\"10%\" class=\"tablebluetop tabletopnolink\" style=\"vertical-align:bottom;horizontal-align:center;text-align: center;\">".$Lang['ePayment']['PPSCounterBill']."</th>";
			}
			if($payment_method_type[3])
			{
				$table.="<th width=\"10%\" class=\"tablebluetop tabletopnolink\" style=\"vertical-align:bottom;horizontal-align:center;text-align: center;\">". $Lang['Payment']['CashDeposit'] ."</th>";
			}
			if($payment_method_type[4])
			{
				$table.="<th width=\"10%\" class=\"tablebluetop tabletopnolink\" style=\"vertical-align:bottom;horizontal-align:center;text-align: center;\">". $i_Payment_Credit_TypeAddvalueMachine ."</th>";
			}
			if($sys_custom['ePayment']['CreditMethodAutoPay'] && $payment_method_type[5])
			{
				$table.="<th width=\"10%\" class=\"tablebluetop tabletopnolink\" style=\"vertical-align:bottom;horizontal-align:center;text-align: center;\">". $Lang['ePayment']['AutoPay'] ."</th>";
			}
			if($sys_custom['ePayment']['CashDepositMethod'] && $payment_method_type[6])
			{
				$table.="<th width=\"10%\" class=\"tablebluetop tabletopnolink\" style=\"vertical-align:bottom;horizontal-align:center;text-align: center;\">". $Lang['ePayment']['BankTransfer'] ."</th>";
			}
			if($sys_custom['ePayment']['CashDepositMethod'] && $payment_method_type[7])
			{
				$table.="<th width=\"10%\" class=\"tablebluetop tabletopnolink\" style=\"vertical-align:bottom;horizontal-align:center;text-align: center;\">". $Lang['ePayment']['ChequeDeposit'] ."</th>";
			}
			
			if($payment_method_type[1])
			{
				$table.="<th width=\"10%\" class=\"tablebluetop tabletopnolink\" style=\"vertical-align:bottom;horizontal-align:center;text-align: center;\">". $Lang['ePayment']['PPSCharge'] ."(".$Lang['ePayment']['PPSIEPS'].")</th>";
			}
			if($payment_method_type[2])
			{
				$table.="<th width=\"10%\" class=\"tablebluetop tabletopnolink\" style=\"vertical-align:bottom;horizontal-align:center;text-align: center;\">". $Lang['ePayment']['PPSCharge'] ."(".$Lang['ePayment']['PPSCounterBill'].")</th>";
			}
		}
		
		$table .= $payment_item_table;
		$table.="</tr>";
		$table.="</thead>";
	}
	else
	{
		$table.="<thead>";
		$table.="<tr >
							<th width=\"5%\" class=\"tablebluetop tabletopnolink\">".$Lang['StudentAttendance']['Class']."</th>
							<th width=\"5%\" class=\"tablebluetop tabletopnolink\">".$Lang['StudentAttendance']['ClassNumber']."</th>";
		if($sys_custom['ePayment']['HartsPreschool']){
			$table .= "<th width=\"5%\" class=\"tablebluetop tabletopnolink\">".$i_STRN."</th>";
		}
		$table.="<th width=\"20%\" class=\"tablebluetop tabletopnolink\">".$Lang['StudentAttendance']['StudentName']."</th>";
		if(!$isKIS){
					$table.="<th width=\"10%\" class=\"sub_row_top\" style=\"vertical-align:bottom;horizontal-align:center;text-align: center;\">". $Lang['ePayment']['CurrentAccountBalance'] ."</th>";
		}
		$table .= $payment_item_table;
		$table.="</tr>";
		$table.="</thead>";
	}
	
	/*
	$table.="<tr >
						<td width=20% class=\"tablebluetop tabletopnolink\">&nbsp</td>
						<td width=10% class=\"tablebluetop tabletopnolink\" style='vertical-align:bottom;horizontal-align:center'>$i_Payment_PrintPage_Outstanding_AccountBalance</td>";
	$csv .="\"\"\t\"$i_Payment_PrintPage_Outstanding_AccountBalance\"\t";
	for($i=0;$i<sizeof($order_item);$i++){
		$item_id = $order_item[$i];
		$item_name = $payment_item[$item_id]['Name'];
		$default_amount = $payment_item[$item_id]['DefaultAmount'];
		$str_default_amount = $lpayment->getWebDisplayAmountFormat($default_amount);
		$table.="<td class=\"tablebluetop tabletopnolink\" style='vertical-align:bottom;horizontal-align:center'>$item_name<Br>$str_default_amount</td>";
		$csv.="\"$item_name ".$lpayment->getExportAmountFormat($default_amount)."\"";
		if($i<sizeof($order_item)-1)
			$csv .="\t";
	}
	$table.="</tr>";
	$csv.="\r\n";
	*/
	$csv.="\r\n";
	
	# table content
	for($i=0;$i<sizeof($order_student);$i++){
		$student_id   = $order_student[$i];
		$student_name = $student_data[$student_id]['Name'];
		if($format!=1 && $student_data[$student_id]['archived']==1){
			$student_name = "<font color=\"red\">*</font> <i>$student_name</i>";
		}
		$balance      = $student_data[$student_id]['Balance'];
		$str_balance  = $lpayment->getWebDisplayAmountFormat($balance);
		
		$css = $i%2==0?"tablebluerow1":"tablebluerow2";

		
		$table.="<tr class=\"$css\"><td class=\"$css\">".$student_data[$student_id]['ClassName']."</td><td class=\"$css\">".$student_data[$student_id]['ClassNumber']."</td>";
		if($sys_custom['ePayment']['HartsPreschool']){
			$table.="<td class=\"$css\">".$student_data[$student_id]['STRN']."</td>";
		}
		$table.="<td class=\"$css\">".$student_name."</td>";
		if(!$isKIS){
			$table.="<td class=\"roster_off\">$str_balance</td>";
		}
		if($isKIS){
			$csv .="\"".$student_data[$student_id]['ClassName']."\"\t\"".$student_data[$student_id]['ClassNumber'].($sys_custom['ePayment']['HartsPreschool']?"\"\t\"".$student_data[$student_id]['STRN']:"")."\"\t\"".$student_name."\"\t";
		}else{
			$csv .="\"".$student_data[$student_id]['ClassName']."\"\t\"".$student_data[$student_id]['ClassNumber'].($sys_custom['ePayment']['HartsPreschool']?"\"\t\"".$student_data[$student_id]['STRN']:"")."\"\t\"".$student_name."\"\t\"".$lpayment->getExportAmountFormat($balance)."\"\t";
		}
		if($data_format==2)
		{
			if(!$isKIS)
			{
				if($payment_method_type[1]+$payment_method_type[2]+$payment_method_type[3]+$payment_method_type[4]+$payment_method_type[5]+$payment_method_type[6]+$payment_method_type[7] > 1)
				{
					if(!isset($total_add_value[$student_id])){
						$total_add_value[$student_id] = 0;
					}
					$table.= "<td class=\"roster_off\" align=\"center\">". $lpayment->getWebDisplayAmountFormat($total_add_value[$student_id]) ."</td>";
					$csv .="\"".$lpayment->getExportAmountFormat($total_add_value[$student_id])."\"\t";
				}
				
				if($payment_method_type[1])
				{
					if(!isset($add_value_data[$student_id][1])){
						$add_value_data[$student_id][1] = 0;
					}
					$table.= "<td class=\"$css\" align=\"center\">". $lpayment->getWebDisplayAmountFormat($add_value_data[$student_id][1]) ."</td>";
					$csv .="\"".$lpayment->getExportAmountFormat($add_value_data[$student_id][1])."\"\t";
				}
				if($payment_method_type[2])
				{
					if(!isset($add_value_data[$student_id][2])){
						$add_value_data[$student_id][2] = 0;
					}
					$table.= "<td class=\"$css\" align=\"center\">". $lpayment->getWebDisplayAmountFormat($add_value_data[$student_id][2]) ."</td>";
					$csv .="\"".$lpayment->getExportAmountFormat($add_value_data[$student_id][2])."\"\t";
				}
				if($payment_method_type[3])
				{
					if(!isset($add_value_data[$student_id][3])){
						$add_value_data[$student_id][3] = 0;
					}
					$table.= "<td class=\"$css\" align=\"center\">". $lpayment->getWebDisplayAmountFormat($add_value_data[$student_id][3]) ."</td>";
					$csv .="\"".$lpayment->getExportAmountFormat($add_value_data[$student_id][3])."\"\t";
				}
				if($payment_method_type[4])
				{
					if(!isset($add_value_data[$student_id][4])){
						$add_value_data[$student_id][4] = 0;
					}
					$table.= "<td class=\"$css\" align=\"center\">". $lpayment->getWebDisplayAmountFormat($add_value_data[$student_id][4]) ."</td>";
					$csv .="\"".$lpayment->getExportAmountFormat($add_value_data[$student_id][4])."\"\t";
				}
				if($sys_custom['ePayment']['CreditMethodAutoPay'] && $payment_method_type[5])
				{
					if(!isset($add_value_data[$student_id][5])){
						$add_value_data[$student_id][5] = 0;
					}
					$table.= "<td class=\"$css\" align=\"center\">". $lpayment->getWebDisplayAmountFormat($add_value_data[$student_id][5]) ."</td>";
					$csv .="\"".$lpayment->getExportAmountFormat($add_value_data[$student_id][5])."\"\t";
				}
				if($sys_custom['ePayment']['CashDepositMethod'] && $payment_method_type[6]){
					if(!isset($add_value_data[$student_id][6])){
						$add_value_data[$student_id][6] = 0;
					}
					$table.= "<td class=\"$css\" align=\"center\">". $lpayment->getWebDisplayAmountFormat($add_value_data[$student_id][6]) ."</td>";
					$csv .="\"".$lpayment->getExportAmountFormat($add_value_data[$student_id][6])."\"\t";
				}
				if($sys_custom['ePayment']['CashDepositMethod'] && $payment_method_type[7]){
					if(!isset($add_value_data[$student_id][7])){
						$add_value_data[$student_id][7] = 0;
					}
					$table.= "<td class=\"$css\" align=\"center\">". $lpayment->getWebDisplayAmountFormat($add_value_data[$student_id][7]) ."</td>";
					$csv .="\"".$lpayment->getExportAmountFormat($add_value_data[$student_id][7])."\"\t";
				}
			}
			if($plugin['ePOS']){
				if(!isset($pos_data[$student_id])){
					$pos_data[$student_id] = 0;
				}
				$table .= "<td class=\"roster_off\" align=\"center\">".$lpayment->getWebDisplayAmountFormat($pos_data[$student_id])."</td>";
				$csv .= "\"".$lpayment->getExportAmountFormat($pos_data[$student_id])."\"\t";
			}
			
			if(sizeof($order_item)+$payment_method_type[1]+$payment_method_type[2] > 0)
			{
				if(!isset($student_total_amount[$student_id])){
					$student_total_amount[$student_id] = 0;
				}
				$table.= "<td class=\"roster_off\" align=\"center\">". $lpayment->getWebDisplayAmountFormat($student_total_amount[$student_id]) ."</td>";
				$csv .="\"".$lpayment->getExportAmountFormat($student_total_amount[$student_id])."\"\t";
			}
			if(!$isKIS)
			{
				if($payment_method_type[1])
				{
					if(!isset($add_value_data_charge[$student_id][1])){
						$add_value_data_charge[$student_id][1] = 0;
					}
					$table.= "<td class=\"$css\" align=\"center\">". $lpayment->getWebDisplayAmountFormat($add_value_data_charge[$student_id][1]) ."</td>";
					$csv .="\"".$lpayment->getExportAmountFormat($add_value_data_charge[$student_id][1])."\"\t";
				}
				if($payment_method_type[2])
				{
					if(!isset($add_value_data_charge[$student_id][2])){
						$add_value_data_charge[$student_id][2] = 0;
					}
					$table.= "<td class=\"$css\" align=\"center\">". $lpayment->getWebDisplayAmountFormat($add_value_data_charge[$student_id][2]) ."</td>";
					$csv .="\"".$lpayment->getExportAmountFormat($add_value_data_charge[$student_id][2])."\"\t";
				}
			}
		}
		
		for($j=0;$j<sizeof($order_item);$j++){
			$item_id = $order_item[$j];
			$paid_status  = $student_data[$student_id][$item_id]['PaidStatus'];
			
			if($paid_status == "")
			{
				 $paid_status = $i_Payment_PresetPaymentItem_NoNeedToPay; 
				 $str_paid_status = $paid_status;
			}
			else
			{
				if($data_format==1)
				{
					if($paid_status == $i_Payment_PresetPaymentItem_PaidCount)
						$str_paid_status ="<font style=\"font-family:Symbol;\">&Ouml;</font>";
					else if($paid_status == $i_Payment_PresetPaymentItem_UnpaidCount)
						$str_paid_status ="X";		
				}
				else
				{
					$str_paid_status = $lpayment->getWebDisplayAmountFormat($student_data[$student_id][$item_id]['PaidAmount']);
				}
			}
			
			$table.="<td align=\"center\" class=\"$css\">$str_paid_status</td>";	
			if($data_format==1)
			{
				$csv .="\"$paid_status\"";
			}
			else
			{
				$csv .="\"". $student_data[$student_id][$item_id]['PaidAmount'] ."\"";
			}
			if($j<sizeof($order_item)-1)
				$csv.="\t";
		
		}
		$table.="</tr>";
		$csv.="\r\n";
	}
	$table.="</table>";
	$display = $header.$table;
}else{
	
	   # Y: Item Name / X: Student Name
	  $header.="<table width=\"90%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" align=\"center\" class=\"$css_table\">";
		$page_breaker="<p class=\"breakhere\"></p>";
		
		$page = $page==""?0:$page;
		
		$pos_start = 0;
		$pos_end = sizeof($order_student);
	
		if($format!=1){ ## cut students
			if(sizeof($order_student)>MAX_STUDENT_PER_PAGE){
				$temp_size = sizeof($order_student);
				$pages = ceil($temp_size / MAX_STUDENT_PER_PAGE);
				$next_page = $page+1 < $pages ?$page+1:-1;
				$prev_page = $page>0?$page-1:-1;
				$pos_start = MAX_STUDENT_PER_PAGE * $page;
				$pos_end  = MAX_STUDENT_PER_PAGE * ($page+1);
				$pos_end = $pos_end > $temp_size ? $temp_size : $pos_end;
			}else{
				$next_page = -1;
				$prev_page = -1;
			}
		}
//	echo "page=$page,prevpage=$prev_page,next_page=$next_page,pos start=$pos_start,end=$pos_end<Br>";	
		$header.="<thead><tr><td class=\"tablebluetop tabletopnolink\">".$Lang['StudentAttendance']['Class']." /<br/>".$Lang['StudentAttendance']['ClassNumber'];
		if($sys_custom['ePayment']['HartsPreschool']){
			$header.=" /<br/>".$i_STRN;
		}
		$header.=" /<br/>".$Lang['StudentAttendance']['StudentName']."</td>";
		$csv="\"".$Lang['StudentAttendance']['Class']." / ".$Lang['StudentAttendance']['ClassNumber'].($sys_custom['ePayment']['HartsPreschool']?" / ".$i_STRN:"")." / ".$Lang['StudentAttendance']['StudentName']."\"\t";
		$csv_bal = "";
		if(!$isKIS){
			$csv_bal .="\"$i_Payment_PrintPage_Outstanding_AccountBalance\"\t";
		}
		$table_bal="<tr>";
		if(!$isKIS){
			$table_bal.="<td class=\"$css_table_content\">$i_Payment_PrintPage_Outstanding_AccountBalance</td>";
		}
		for($i=$pos_start;$i<$pos_end;$i++){
			$student_id   = $order_student[$i];
			$student_name = $student_data[$student_id]['Name'];
			if($format!=1 && $student_data[$student_id]['archived']==1){
				$student_name = "<font color=\"red\">*</font><br><i>$student_name</i>";
			}			
			$balance      = $student_data[$student_id]['Balance'];
			
			$header .= "<td class=\"tablebluetop tabletopnolink\" style=\"vertical-align:bottom;horizontal-align:center\">$student_name</td>";
			if(!$isKIS){
				$table_bal.="<td class=\"$css_table_content\">".$lpayment->getWebDisplayAmountFormat($balance)."</td>";
			}
			$csv.="\"$student_name\"";
			if(!$isKIS){
				$csv_bal.="\"".$lpayment->getExportAmountFormat($balance)."\"";
			}
			if($i<$pos_end-1){
				$csv .="\t";
				$csv_bal.="\t";
			}
		}
		
		$header.="</tr></thead>";
		$table_bal.="</tr>";
		//$header.=$table_bal;
		
		$csv .="\r\n";
		$csv_bal.="\r\n";
		//$csv.=$csv_bal;
		$display="";
		
		///$table.="<tr>";
		for($i=0;$i<sizeof($order_item);$i++){
			//$css = $i%2==0?"tableContent2":"tableContent";
			//$css = $i%2==0?$css_table_content:$css_table_content2;
			$css = $i%2==0?"tablebluerow1":"tablebluerow2";
			
			if($ItemPerPage>0 && $i>0 && $i%$ItemPerPage==0){ ## page break;
				$table.="</table>";
				$display.=$header.$table.$page_breaker;
				$table="";
			}
			
			
			$item_id = $order_item[$i];
			$item_name = $payment_item[$item_id]['Name'];
			$default_amount = $payment_item[$item_id]['DefaultAmount'];			

			$table.="<tr class='$css'><td class='$css'>$item_name (".$lpayment->getWebDisplayAmountFormat($default_amount).")</td>";
			$csv.="\"$item_name ".$lpayment->getExportAmountFormat($default_amount)."\"\t";
			for($j=$pos_start;$j<$pos_end;$j++){
				$student_id =  $order_student[$j];
				$paid_status  = $student_data[$student_id][$item_id]['PaidStatus'];
				if($paid_status == ""){
					 $paid_status = $i_Payment_PresetPaymentItem_NoNeedToPay; 
					 $str_paid_status = $paid_status;
				}
				
				if($paid_status == $i_Payment_PresetPaymentItem_PaidCount)
					//$str_paid_status = "<font color=blue>$paid_status</font>";
					$str_paid_status ="<font style=\"font-family:Symbol;\">&Ouml;</font>";
				else if($paid_status == $i_Payment_PresetPaymentItem_UnpaidCount)
					//$str_paid_status = "<font color=red>$paid_status</font>";
					$str_paid_status ="X";

				
				$table.="<td class=\"$css\" style=\"vertical-align:middle;horizontal-align:center\">$str_paid_status</td>";					
				$csv .="\"$paid_status\"";
				if($j<$pos_end -1)
					$csv.="\t";
			}
			$table.="</tr>";
			$csv.="\r\n";	
			
		}
		$table.="</table>";
		$display.=$header.$table;
		
		$nav_button = "<table width=\"100%\" border=\"0\">";
		$nav_button.="<tr><td><table border=\"0\" class=\"print_hide\" align=\"right\"><tr><td style=\"vertical-align:top;horizontal-align:right\">";
		if($prev_page!=-1)
			$nav_button.="<a href=\"class_report.php?FromDate=".urlencode($FromDate)."&ToDate=".urlencode($ToDate)."&ClassName=".urlencode($ClassName)."&StudentTypesCURRENT=".urlencode($StudentTypesCURRENT)."&StudentTypesREMOVED=".urlencode($StudentTypesREMOVED)."&page=".urlencode($prev_page)."&format=".urlencode($format)."&display_mode=".urlencode($display_mode)."&ItemPerPage=".urlencode($ItemPerPage)."\"><img src=\"$image_path/admin/button/prev.gif\" border=\"0\"></a> ";
		if($next_page!=-1)
			$nav_button.=" <a href=\"class_report.php?FromDate=".urlencode($FromDate)."&ToDate=".urlencode($ToDate)."&ClassName=".urlencode($ClassName)."&StudentTypesCURRENT=".urlencode($StudentTypesCURRENT)."&StudentTypesREMOVED=".urlencode($StudentTypesREMOVED)."&page=".urlencode($next_page)."&format=".urlencode($format)."&display_mode=".urlencode($display_mode)."&ItemPerPage=".urlencode($ItemPerPage)."\"><img src=\"$image_path/admin/button/next.gif\" border=\"0\"></a>";
		$nav_button.="</td></tr></table></td></tr>";
		$nav_button.="<tr><Td style=\"vertical-align:top;horizontal-align:right\">$display</td></tr>";
		$nav_button.="</table>";
		
		$display=$nav_button;
}


intranet_closedb();

if($format!=1){
	$linterface->LAYOUT_START();
	
	echo '<table width="100%" align="center" class="print_hide" border="0">
			<tr>
				<td align="right">'.$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2").'</td>
			</tr>
		  </table>';
	
	if($sys_custom['ePayment']['ReportWithSchoolName']){
		$school_name = GET_SCHOOL_NAME();
		echo '<table width="90%" align="center" border="0">
				<tr>
					<td align="center"><h2><b>'.$school_name.'</b></h2></td>
				</tr>
			</table>';
	}
	echo "<br />";
	echo $display;
	echo "<table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\">";
	echo "<tr>";
	echo "<td class=\"eSportprinttext tabletext\" >$i_Payment_Note_StudentRemoved</td><td class=\"eSportprinttext tabletext\" align=\"right\">$i_general_report_creation_time : ".date('Y-m-d H:i:s')."</td></tr></table>";
	echo "<br />";	

	$linterface->LAYOUT_STOP();
	?>
	<style type='text/css' media='print'>
	 .print_hide {display:none;}
	  P.breakhere {page-break-before: always}
	  thead {display: table-header-group;}
	</style>
<?php
}else{
	$filename = "class_payment_report_".$ClassName.".csv";
	$lexport->EXPORT_FILE($filename, $csv);		
}
?>