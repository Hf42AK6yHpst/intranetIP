<?php
// Editing by 
/*
 * 2020-11-05 (Ray)   : add enotice filter
 * 2020-10-23 (Ray)   : by transaction time count refund record
 * 2020-10-12 (Ray)   : add epayment item cout as cash deposit
 * 2020-04-27 (Ray)   : Added MerchantAccount filter
 * 2019-08-16 (Carlos): Modified only count settled payment items, refunded payment items(not count cancel payment items).
 * 2019-08-13 (Carlos): Added eWallet handling fee.
 * 2019-07-30 (Carlos): Created.
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$lpayment->isEWalletDirectPayEnabled()) {
	intranet_closedb();
	header ("Location: /");
	exit();
}

$CurrentPageArr['ePayment'] = 1;
$CurrentPage = "StatisticsReport_IncomeExpensesReport";

$linterface = new interface_html();

if(!isset($Format) || (isset($Format) && !in_array($Format,array('','web','print','csv')))){
	$Format = '';
}

$is_web_format = in_array($Format,array('','web'));
$is_print_format = $Format == 'print';
$is_csv_format = $Format == 'csv';

# date range
$today_ts = strtotime(date('Y-m-d'));
$date_range_cookies = $lpayment->Get_Report_Date_Range_Cookies();
if($FromDate=="" || !intranet_validateDate($FromDate)){
	$FromDate = $date_range_cookies['StartDate']!=''? $date_range_cookies['StartDate'] : date('Y-m-d',getStartOfAcademicYear($today_ts));
}
if($ToDate=="" || !intranet_validateDate($ToDate)){
	$ToDate =  $date_range_cookies['EndDate']!=''? $date_range_cookies['EndDate'] : date('Y-m-d',getEndOfAcademicYear($today_ts));
}
if($_REQUEST['FromDate'] != '' && $_REQUEST['ToDate']!=''){
	$lpayment->Set_Report_Date_Range_Cookies($_REQUEST['FromDate'], $_REQUEST['ToDate']);
}

# 1 - Add Value ( Cash Deposit, PPS, Add Value Machine )
# 2 - Payment Items
# 3 - Single Purchase (Handling fees for AlipayHK/TNG/FPS)
# 4 - Transfer TO
# 5 - Transfer FROM
# 6 - Cancel Payment
# 7 - Refund
# 8 - PPS Charges
# 9 - Cancel Cash Deposit
# 10 - Donation to school
# 11 - POS void transaction (refund)
# 12 - eWallet handling fee
$find_transaction_types = array(2,6,7,12);

$sp_map = $lpayment->getPaymentServiceProviderMapping();

$selected_merchant_accounts = array();
$merchant_accounts_all = $lpayment->getMerchantAccounts(array());
if(!isset($MerchantAccount) || !is_array($MerchantAccount)) {
	$MerchantAccount = array();
	if($from_submit != '1') {
		$MerchantAccount_Cash = '1';
		foreach ($merchant_accounts_all as $temp) {
			$MerchantAccount[] = $temp['AccountID'];
		}
	}
}

$show_cash_deposit = ($MerchantAccount_Cash == '1') ? true : false;

$merchant_accounts = $lpayment->getMerchantAccounts(array("AccountID"=>$MerchantAccount));
$accountIdToRecords = BuildMultiKeyAssoc($merchant_accounts,array('AccountID'));

$is_tng_enabled = $lpayment->isTNGDirectPayEnabled();
$PaymentMethod_PayAtSchool = 'PayAtSchool';
$PaymentMethod_TNG = 'TNG';

if($SearchBy == 2){ // by transaction time
	$date_cond = " IF(a.TransactionType=1 AND c.TransactionID IS NOT NULL,DATE_FORMAT(c.TransactionTime,'%Y-%m-%d')>='".$lpayment->Get_Safe_Sql_Query($FromDate)."' AND DATE_FORMAT(c.TransactionTime,'%Y-%m-%d')<='".$lpayment->Get_Safe_Sql_Query($ToDate)."',DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')>='".$lpayment->Get_Safe_Sql_Query($FromDate)."' AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')<='".$lpayment->Get_Safe_Sql_Query($ToDate)."') ";
}else{ // by input time
	$date_cond =" DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')>='".$lpayment->Get_Safe_Sql_Query($FromDate)."' AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')<='".$lpayment->Get_Safe_Sql_Query($ToDate)."' ";		
}


if($sys_custom['ePayment']['PaymentItemDateRange']){
	$notice_date_conds = " AND (a.EndDate between '$FromDate' AND '$ToDate') ";
}else{
	$notice_date_conds =" AND (
									(a.StartDate between '$FromDate' AND '$ToDate' 
									 or 
									 a.EndDate between '$FromDate' AND '$ToDate' )) ";
}

$sql = "SELECT b.NoticeID, b.Title FROM PAYMENT_PAYMENT_ITEM as a
LEFT JOIN INTRANET_NOTICE as b ON a.NoticeID=b.NoticeID
WHERE a.NoticeID IS NOT NULL
$notice_date_conds
AND b.RecordStatus=1
GROUP BY b.NoticeID ORDER BY b.Title ASC";
$PaymentNoticeArray = $lpayment->returnArray($sql);

$cond3 = "";
if($PaymentNoticeID != "") {
	$cond3 = " AND e.NoticeID='".$PaymentNoticeID."' ";
}

$sql="SELECT 
		a.Amount,
		IF(f.NoticeID is null, IF(a.TransactionType=2 AND e.ItemID IS NOT NULL,e.Name,a.Details), concat(f.Title,' - ',a.Details)) as ItemName,
	    a.TransactionType,
	    c.RecordType,
		a.RelatedTransactionID,
		IF(a.TransactionType = 6, a.RelatedTransactionID, e.ItemID) as ItemID,
		e.MerchantAccountID,
		d.NoticePaymentMethod,
		g.ServiceProvider,
		g.MerchantAccount,
		t.PaymentStatus,
		t.ChargeStatus,
		t.RefundStatus,
		t.PaymentID,
		t.Sp     
	  FROM PAYMENT_OVERALL_TRANSACTION_LOG AS a
	  LEFT JOIN PAYMENT_CREDIT_TRANSACTION as c ON a.RelatedTransactionID = c.TransactionID
	  LEFT JOIN PAYMENT_PAYMENT_ITEMSTUDENT as d on d.PaymentID=a.RelatedTransactionID
	  LEFT JOIN PAYMENT_PAYMENT_ITEM as e on e.ItemID=d.ItemID 
	  LEFT JOIN PAYMENT_TNG_TRANSACTION as t ON t.PaymentID=d.PaymentID 
	  LEFT JOIN INTRANET_NOTICE as f on f.NoticeID=e.NoticeID 
	  LEFT JOIN PAYMENT_MERCHANT_ACCOUNT as g ON ((a.TransactionType=12 AND g.AccountID=a.RelatedTransactionID)) 
	  WHERE $date_cond AND a.TransactionType IN (".implode(",",$find_transaction_types).") 
	  $cond3
	ORDER BY a.Details, c.RecordType";
$records = $lpayment->returnResultSet($sql);

$record_size = count($records);
//debug_pr($records);
$total_income = 0;
$total_expense = 0;
$net_amount = 0;
$itemIdToData = array();
$sumColumnData = array('MerchantAccounts'=>array(),'Refund'=>0,'Total'=>0);
if($show_cash_deposit) {
	$sumColumnData['CashDeposit'] = 0;
}
if($is_tng_enabled){
	$sumColumnData['TNG'] = 0;
}
if(count($merchant_accounts)>0){
	foreach($merchant_accounts as $merchant_account){
		$sumColumnData['MerchantAccounts'][$merchant_account['AccountID']] = 0;
	}
}
$spToHandlingFee = array();
for($i=0;$i<$record_size;$i++)
{
	$amount = $records[$i]['Amount'];
	$transaction_type = $records[$i]['TransactionType'];
	$item_id = trim($records[$i]['ItemID']);
    $transaction_sp = trim(strtoupper($records[$i]['Sp']));

	if($sys_custom['ePayment']['MultiPaymentGateway']) {
		$item_merchant_accounts = $lpayment->returnPaymentItemMultiMerchantAccountID($item_id);
		$merchant_account_in_use = $lpayment->getMerchantAccounts(array("AccountID"=>$item_merchant_accounts, "ServiceProvider"=>$transaction_sp));
		$records[$i]['MerchantAccountID'] = $merchant_account_in_use[0]['AccountID'];
	}

	$merchant_account_id = trim($records[$i]['MerchantAccountID']);
	$notice_payment_method = trim($records[$i]['NoticePaymentMethod']);
	$charge_status = $records[$i]['ChargeStatus'];

	if($transaction_type == 2) // pay payment item
	{
		if ($merchant_account_id == '' || ($merchant_account_id != '' && $notice_payment_method == $PaymentMethod_PayAtSchool)
			|| ($merchant_account_id != '' && $records[$i]['PaymentID'] == '' && $notice_payment_method == '')
        ) { // pay at school means first cash deposit then pay the item with cash
			if($show_cash_deposit == false) {
				continue;
			}
		} else if ($merchant_account_id != '') {
			if (in_array($merchant_account_id, $MerchantAccount) == false) {
				continue;
			}
		}
	}

	if($item_id != ''){
		if(!isset($itemIdToData[$item_id])){
			$itemIdToData[$item_id] = array('ItemName'=>$records[$i]['ItemName'],'MerchantAccounts'=>array(),'Refund'=>0,'Total'=>0);
			if($show_cash_deposit) {
				$itemIdToData[$item_id]['CashDeposit'] = 0;
			}
			if($is_tng_enabled){
				$itemIdToData[$item_id]['TNG'] = 0;
			}
			if(count($merchant_accounts)>0){
				foreach($merchant_accounts as $merchant_account){
					$itemIdToData[$item_id]['MerchantAccounts'][$merchant_account['AccountID']] = 0;
				}
			}
		}
		

		if($transaction_type == 2) // pay payment item
		{
			if($merchant_account_id == '' || ($merchant_account_id != '' && $notice_payment_method == $PaymentMethod_PayAtSchool)
            || ($merchant_account_id != '' && $records[$i]['PaymentID'] == '' && $notice_payment_method == '')
            ){ // pay at school means first cash deposit then pay the item with cash
				$itemIdToData[$item_id]['CashDeposit'] += $amount;
				$itemIdToData[$item_id]['Total'] += $amount;
				$sumColumnData['CashDeposit'] += $amount;
				$sumColumnData['Total'] += $amount;
				$total_income += $amount;
			}else if($merchant_account_id != ''){
			    $count_as_amount = false;
			    if($SearchBy == 2) {
					$count_as_amount = true;
				} else if($SearchBy == 1) {
					if ($charge_status == '1') { // success and settled
						$count_as_amount = true;
					}

					if(strtolower($transaction_sp) == 'fps') {
						if($charge_status == '2') {
							$count_as_amount = true;
						}
					}
				}

			    if($count_as_amount) {
					$itemIdToData[$item_id]['MerchantAccounts'][$merchant_account_id] += $amount;
					$itemIdToData[$item_id]['Total'] += $amount;
					$sumColumnData['MerchantAccounts'][$merchant_account_id] += $amount;
					$sumColumnData['Total'] += $amount;
					$total_income += $amount;
				}

			}else if($notice_payment_method == $PaymentMethod_TNG){
				if($charge_status == '1'){ // success and settled
					$itemIdToData[$item_id]['TNG'] += $amount;
					$itemIdToData[$item_id]['Total'] += $amount;
					$sumColumnData['TNG'] += $amount;
					$sumColumnData['Total'] += $amount;
					$total_income += $amount;
				}
			}
		}else if($transaction_type == 6 || $transaction_type == 7){ // refunded payment item
			$itemIdToData[$item_id]['Refund'] += $amount;
			$itemIdToData[$item_id]['Total'] -= $amount;
			$sumColumnData['Refund'] += $amount;
			$sumColumnData['Total'] -= $amount;
			$total_expense += $amount;
		}
	}else if($transaction_type == 12){
		$service_provider = $records[$i]['ServiceProvider'];
		if(!isset($spToHandlingFee[$service_provider])){
			$spToHandlingFee[$service_provider] = 0;
		}
		$spToHandlingFee[$service_provider] += $amount;
		$sumColumnData['Refund'] += $amount;
		//$sumColumnData['Total'] -= $amount;
		$total_expense += $amount;
	}
	
}

$net_amount = $total_income - $total_expense;

$income_colspan = ((isset($sumColumnData['CashDeposit']))?1:0) + count($sumColumnData['MerchantAccounts']) + (isset($sumColumnData['TNG'])?1:0);



if($is_web_format || $is_print_format)
{
	$web_row_class = "";
	$header_border_color = $is_web_format ? '#006666' : '#000000';
	
	$y ="<table width=\"96%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\" align=\"center\">";
	if($is_web_format) {
		$y.="<tr>";
		$y.="<td valign=\"top\" nowrap=\"nowrap\" class=\"".($is_print_format?"eSportprinttext":"formfieldtitle tabletext")."\">".$Lang['eNotice']['PaymentNotice']."</td>";
		$y.="<td valign=\"top\" nowrap=\"nowrap\" class=\"".($is_print_format?"eSportprinttext":"tabletext")."\" width=\"70%\">".$linterface->GET_SELECTION_BOX($PaymentNoticeArray,'id="PaymentNoticeID" name="PaymentNoticeID"', $Lang['General']['All'], $PaymentNoticeID)."</td></tr>";
	}

	$y.="<tr>";
		$y.="<td valign=\"top\" nowrap=\"nowrap\" class=\"".($is_print_format?"eSportprinttext":"formfieldtitle tabletext")."\">$i_Payment_SchoolAccount_TotalIncome</td>";
		$y.="<td valign=\"top\" nowrap=\"nowrap\" class=\"".($is_print_format?"eSportprinttext":"tabletext")."\" width=\"70%\">".$lpayment->getWebDisplayAmountFormat($total_income)."</td></tr>";
		$y.="<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"".($is_print_format?"eSportprinttext":"formfieldtitle tabletext")."\">$i_Payment_SchoolAccount_TotalExpense</td>";
		$y.="<td valign=\"top\" nowrap=\"nowrap\" class=\"".($is_print_format?"eSportprinttext":"tabletext")."\" width=\"70%\">".$lpayment->getWebDisplayAmountFormat($total_expense)."</td></tr>";
		$y.="<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"".($is_print_format?"eSportprinttext":"formfieldtitle tabletext")."\">$i_Payment_SchoolAccount_NetIncomeExpense</td>";
		$y.="<td valign=\"top\" nowrap=\"nowrap\" class=\"".($is_print_format?"eSportprinttext":"tabletext")."\" width=\"70%\">".$lpayment->getWebDisplayAmountFormat($net_amount)."</td>";
	$y.="</tr>";
	$y.="</table>";
	
	
	$x = "<table width=\"96%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\" ".($is_print_format?"class=\"eSporttableborder\"":"")." style=\"border-collapse:collapse;\">";
	$x.="<tr".($is_web_format?" class=\"tablebluetop\"":"").">";
		$x.="<td rowspan=\"2\" class=\"".($is_print_format?"eSporttdborder eSportprinttabletitle print_header":"tabletoplink")."\" width=\"20%\" align=\"center\" style=\"vertical-align:middle;border:1px solid $header_border_color;\">$i_Payment_SchoolAccount_Detailed_Transaction</td>";
		$x.="<td colspan=\"$income_colspan\" class=\"".($is_print_format?"eSporttdborder eSportprinttabletitle print_header":"tabletoplink")."\" width=\"60%\" align=\"center\" style=\"vertical-align:middle;border:1px solid $header_border_color;\">$i_Payment_SchoolAccount_Deposit</td>";
		$x.="<td class=\"".($is_print_format?"eSporttdborder eSportprinttabletitle print_header":"tabletoplink")."\" width=\"10%\" align=\"center\" style=\"vertical-align:middle;border:1px solid $header_border_color;\">$i_Payment_SchoolAccount_Expenses</td>";
		$x.="<td rowspan=\"2\" class=\"".($is_print_format?"eSporttdborder eSportprinttabletitle print_header":"tabletoplink")."\" width=\"10%\" align=\"center\" style=\"vertical-align:middle;border:1px solid $header_border_color;\">".$Lang['ePayment']['PaymentItemTotalAmount']."</td>";
	$x.="</tr>";
	
	$x.="<tr".($is_web_format?" class=\"tablebluetop\"":"").">";
	    if(isset($sumColumnData['CashDeposit'])) {
			$x .= "<td class=\"" . ($is_print_format ? "eSporttdborder eSportprinttabletitle print_header" : "tabletoplink") . "\" align=\"center\" style=\"vertical-align:middle;border:1px solid $header_border_color;\">$i_Payment_Credit_TypeCashDeposit</td>";
		}
		if(isset($sumColumnData['TNG'])){
			$x.="<td class=\"".($is_print_format?"eSporttdborder eSportprinttabletitle print_header":"tabletoplink")."\" align=\"center\" style=\"vertical-align:middle;border:1px solid $header_border_color;\">".$Lang['ePayment']['TNG']."</td>";
		}
		if(count($sumColumnData['MerchantAccounts'])>0){
			foreach($sumColumnData['MerchantAccounts'] as $merchant_account_id => $tmp_amount){
				$x.="<td class=\"".($is_print_format?"eSporttdborder eSportprinttabletitle print_header":"tabletoplink")."\" align=\"center\" style=\"vertical-align:middle;border:1px solid $header_border_color;\">".Get_String_Display($accountIdToRecords[$merchant_account_id]['AccountName'])."</td>";
			}
		}

		if($income_colspan == 0) {
			$x.="<td class=\"".($is_print_format?"eSporttdborder eSportprinttabletitle print_header":"tabletoplink")."\" align=\"center\" style=\"vertical-align:middle;border:1px solid $header_border_color;\"></td>";
		}
		$x .= "<td class=\"" . ($is_print_format ? "eSporttdborder eSportprinttabletitle print_header" : "tabletoplink") . "\" align=\"center\" style=\"vertical-align:middle;border:1px solid $header_border_color;\">" . $i_Payment_TransactionType_Refund . "</td>";

	$x.="</tr>";
	
	if(count($itemIdToData)==0){
		$extra_income_colspan = ($income_colspan == 0) ? 1 : 0;
		$x.="<tr><td colspan=\"".($income_colspan+3+$extra_income_colspan)."\" align=\"center\" class=\"".($is_print_format?"eSporttdborder eSportprinttext":"tablebluerow2 tabletext")."\" height=\"40\" style=\"vertical-align:middle;border:1px solid black;\">$i_no_record_exists_msg</td></tr>";
	}else{
		$row_count = 0;
		$row_css = "eSporttdborder eSportprinttext";
		foreach($itemIdToData as $item_id => $ary){
			if($is_web_format){
				$row_css =$row_count%2==0?"tablebluerow1 tabletext":"tablebluerow2 tabletext";
			}
			$row_count++;
			$x.="<tr class=\"$row_css\">";
				$x.="<td class=\"".($is_web_format?"tabletext":"eSporttdborder eSportprinttext")."\">".Get_String_Display($ary['ItemName'])."</td>";
			    if(isset($ary['CashDeposit'])) {
					$x .= "<td class=\"" . ($is_web_format ? "tabletext" : "eSporttdborder eSportprinttext") . "\" align=\"center\">" . $lpayment->getWebDisplayAmountFormat($ary['CashDeposit']) . "</td>";
				}
				if(isset($ary['TNG'])){
					$x.="<td class=\"".($is_web_format?"tabletext":"eSporttdborder eSportprinttext")."\" align=\"center\">".$lpayment->getWebDisplayAmountFormat($ary['TNG'])."</td>";
				}
				if(count($sumColumnData['MerchantAccounts'])>0){
					foreach($sumColumnData['MerchantAccounts'] as $merchant_account_id => $tmp_amount){
						$x.="<td class=\"".($is_web_format?"tabletext":"eSporttdborder eSportprinttext")."\" align=\"center\">".$lpayment->getWebDisplayAmountFormat($ary['MerchantAccounts'][$merchant_account_id])."</td>";
					}
				}
				$x.="<td class=\"".($is_web_format?"tabletext":"eSporttdborder eSportprinttext")."\" align=\"center\">".$lpayment->getWebDisplayAmountFormat($ary['Refund'])."</td>";
				$x.="<td class=\"".($is_web_format?"tabletext":"eSporttdborder eSportprinttext")."\" align=\"center\">".$lpayment->getWebDisplayAmountFormat($ary['Total'])."</td>";
			$x.="</tr>";
		}
		if(count($spToHandlingFee)>0){
			foreach($spToHandlingFee as $sp_key => $sp_handling_fee){
				if($is_web_format){
					$row_css =$row_count%2==0?"tablebluerow1 tabletext":"tablebluerow2 tabletext";
				}
				$row_count++;
				$x.="<tr class=\"$row_css\">";
					$x.="<td class=\"".($is_web_format?"tabletext":"eSporttdborder eSportprinttext")."\">".Get_String_Display($sp_map[$sp_key]).' '.$Lang['ePayment']['HandlingFee']."</td>";
				    if(isset($ary['CashDeposit'])) {
					    $x .= "<td class=\"" . ($is_web_format ? "tabletext" : "eSporttdborder eSportprinttext") . "\" align=\"center\">&nbsp;</td>";
				    }
					if(isset($ary['TNG'])){
						$x.="<td class=\"".($is_web_format?"tabletext":"eSporttdborder eSportprinttext")."\" align=\"center\">&nbsp;</td>";
					}
					if(count($sumColumnData['MerchantAccounts'])>0){
						foreach($sumColumnData['MerchantAccounts'] as $merchant_account_id => $tmp_amount){
							$x.="<td class=\"".($is_web_format?"tabletext":"eSporttdborder eSportprinttext")."\" align=\"center\">&nbsp;</td>";
						}
					}
					$x.="<td class=\"".($is_web_format?"tabletext":"eSporttdborder eSportprinttext")."\" align=\"center\">".$lpayment->getWebDisplayAmountFormat($sp_handling_fee)."</td>";
					$x.="<td class=\"".($is_web_format?"tabletext":"eSporttdborder eSportprinttext")."\" align=\"center\">&nbsp;</td>";
				$x.="</tr>";
			}
		}
		
		$x.="<tr".($is_web_format?" class=\"tablebluebottom\"":"").">";
		$x.="<td class=\"".($is_web_format?"tabletext":"eSporttdborder eSportprinttext")."\" align=\"right\">$i_Payment_SchoolAccount_Total:</td>";
		if(isset($sumColumnData['CashDeposit'])) {
			$x .= "<td class=\"" . ($is_web_format ? "tabletext" : "eSporttdborder eSportprinttext") . "\" align=\"center\">" . $lpayment->getWebDisplayAmountFormat($sumColumnData['CashDeposit']) . "</td>";
		}
		if(isset($sumColumnData['TNG'])){
			$x.="<td class=\"".($is_web_format?"tabletext":"eSporttdborder eSportprinttext")."\" align=\"center\">".$lpayment->getWebDisplayAmountFormat($sumColumnData['TNG'])."</td>";
		}
		if(count($sumColumnData['MerchantAccounts'])>0){
			foreach($sumColumnData['MerchantAccounts'] as $merchant_account_id => $tmp_amount){
				$x.="<td class=\"".($is_web_format?"tabletext":"eSporttdborder eSportprinttext")."\" align=\"center\">".$lpayment->getWebDisplayAmountFormat($tmp_amount)."</td>";
			}
		}
			$x.="<td class=\"".($is_web_format?"tabletext":"eSporttdborder eSportprinttext")."\" align=\"center\">".$lpayment->getWebDisplayAmountFormat($sumColumnData['Refund'])."</td>";
			$x.="<td class=\"".($is_web_format?"tabletext":"eSporttdborder eSportprinttext")."\" align=\"center\">".$lpayment->getWebDisplayAmountFormat($sumColumnData['Total'])."</td>";
		$x.="</tr>";
	}
	
	$x.= "</table>";
		
}

if($is_csv_format)
{
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	
	$lexport = new libexporttext();
	
	$header_row = array();
	$rows = array();
	$row = array();
	$empty_row = array();
	
	$header_row = array($Lang['ePayment']['IncomeExpensesReport']."(".$FromDate." ".$i_Profile_To." ".$ToDate.")");
	$no_of_col = $income_colspan + 3;
	for($i=1;$i<=($no_of_col - 1);$i++){
		$header_row[] = ' ';
	}
	$empty_row = array();
	for($i=0;$i<$no_of_col;$i++){
		$empty_row[] = ' ';
	}
	$rows[] = $empty_row;
	
	$row = array();
	$row[] = $i_Payment_SchoolAccount_TotalIncome;
	$row[] = $lpayment->getExportAmountFormat($total_income);
	for($i=0;$i<($no_of_col-2);$i++){
		$row[] = ' ';
	}
	$rows[] = $row;
	
	$row = array();
	$row[] = $i_Payment_SchoolAccount_TotalExpense;
	$row[] = $lpayment->getExportAmountFormat($total_expense);
	for($i=0;$i<($no_of_col-2);$i++){
		$row[] = ' ';
	}
	$rows[] = $row;
	
	$row = array();
	$row[] = $i_Payment_SchoolAccount_NetIncomeExpense;
	$row[] = $lpayment->getExportAmountFormat($net_amount);
	for($i=0;$i<($no_of_col-2);$i++){
		$row[] = ' ';
	}
	$rows[] = $row;
	
	$rows[] = $empty_row;
	
	$row = array();
	$row[] = $i_Payment_SchoolAccount_Detailed_Transaction;
	$row[] = $i_Payment_SchoolAccount_Deposit;
	for($i=0;$i<($income_colspan-1);$i++){
		$row[] = ' ';
	}
	$row[] = $i_Payment_SchoolAccount_Expenses;
	$row[] = $Lang['ePayment']['PaymentItemTotalAmount'];
	$rows[] = $row;
	
	$row = array();
	$row[] = ' ';
	if(isset($sumColumnData['CashDeposit'])) {
		$row[] = $i_Payment_Credit_TypeCashDeposit;
	}
	if(isset($sumColumnData['TNG'])){
		$row[] = $Lang['ePayment']['TNG'];
	}
	if(count($sumColumnData['MerchantAccounts'])>0){
		foreach($sumColumnData['MerchantAccounts'] as $merchant_account_id => $tmp_amount){
			$row[] = $accountIdToRecords[$merchant_account_id]['AccountName'];
		}
	}
	if($income_colspan == 0) {
		$row[] = ' ';
	}
	$row[] = $i_Payment_TransactionType_Refund;
	$row[] = ' ';
	$rows[] = $row;
	
	if(count($itemIdToData)==0){
		$row = array($i_no_record_exists_msg);
		for($i=1;$i<=($no_of_col - 1);$i++){
			$row[] = ' ';
		}
		$rows[] = $row;
	}else{
		foreach($itemIdToData as $item_id => $ary){
			$row = array($ary['ItemName']);
			if(isset($ary['CashDeposit'])) {
				$row[] = $lpayment->getExportAmountFormat($ary['CashDeposit']);
			}
			if(isset($ary['TNG'])){
				$row[] = $lpayment->getExportAmountFormat($ary['TNG']);
			}
			if(count($sumColumnData['MerchantAccounts'])>0){
				foreach($sumColumnData['MerchantAccounts'] as $merchant_account_id => $tmp_amount){
					$row[] = $lpayment->getExportAmountFormat($ary['MerchantAccounts'][$merchant_account_id]);
				}
			}
			$row[] = $lpayment->getExportAmountFormat($ary['Refund']);
			$row[] = $lpayment->getExportAmountFormat($ary['Total']);
			$rows[] = $row;
		}
		if(count($spToHandlingFee)>0){
			foreach($spToHandlingFee as $sp_key => $sp_handling_fee){
				$row = array($sp_map[$sp_key].' '.$Lang['ePayment']['HandlingFee']);
				if(isset($ary['CashDeposit'])) {
					$row[] = '';
				}
				if(isset($ary['TNG'])){
					$row[] = '';
				}
				if(count($sumColumnData['MerchantAccounts'])>0){
					foreach($sumColumnData['MerchantAccounts'] as $merchant_account_id => $tmp_amount){
						$row[] = '';
					}
				}
				$row[] = $lpayment->getExportAmountFormat($sp_handling_fee);
				$row[] = '';
				$rows[] = $row;
			}
		}
		
		$row = array($i_Payment_SchoolAccount_Total.":");
		if(isset($sumColumnData['CashDeposit'])) {
			$row[] = $lpayment->getExportAmountFormat($sumColumnData['CashDeposit']);
		}
		if(isset($sumColumnData['TNG'])){
			$row[] = $lpayment->getExportAmountFormat($sumColumnData['TNG']);
		}
		if(count($sumColumnData['MerchantAccounts'])>0){
			foreach($sumColumnData['MerchantAccounts'] as $merchant_account_id => $tmp_amount){
				$row[] = $lpayment->getExportAmountFormat($tmp_amount);
			}
		}
		$row[] = $lpayment->getExportAmountFormat($sumColumnData['Refund']);
		$row[] = $lpayment->getExportAmountFormat($sumColumnData['Total']);
		$rows[] = $row;
		
		$row = array("$i_general_report_creation_time : ".date('Y-m-d H:i:s'));
		for($i=1;$i<=($no_of_col - 1);$i++){
			$row[] = ' ';
		}
		$rows[] = $row;
	}
	
	intranet_closedb();
	
	$filename = $Lang['ePayment']['IncomeExpensesReport']."(".$FromDate."_".$i_Profile_To."_".$ToDate.").csv";
	$export_content = $lexport->GET_EXPORT_TXT($rows, $header_row,"","\r\n","",0,"11");
	$lexport->EXPORT_FILE($filename, $export_content);
	exit;
}


if($is_print_format)
{
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
?>
<style type="text/css" media="print">
thead {display: table-header-group;}
thead td.print_header {border-top: 1px solid #333333;}
table.eSporttableborder {border-top: 0px}
</style>
<table width="96%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","print_btn")?></td>
	</tr>
</table>
<?php
if($sys_custom['ePayment']['ReportWithSchoolName']){
	$school_name = GET_SCHOOL_NAME();
	$xx = '<table width="90%" align="center" border="0">
			<tr>
				<td align="center"><h2><b>'.$school_name.'</b></h2></td>
			</tr>
		</table>';
	echo $xx;
}
?>
<table width='96%' border='0' cellspacing='0' cellpadding='5' align='center'>
	<tr>
		<td><b><?=$Lang['ePayment']['IncomeExpensesReport']?> (<?="$FromDate $i_Profile_To $ToDate"?>)</b></td>
	</tr>
</table>
<br />
<?=$y?>
<br />
<?=$x?>
<table width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td class="eSportprinttext" align="right"><?="$i_general_report_creation_time : ".date('Y-m-d H:i:s')?></td>
	</tr>
</table>
<?php
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
}


if($is_web_format)
{
	$toolbar = $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
	$toolbar2 = $linterface->GET_LNK_EXPORT("javascript:exportPage(document.form1,'index.php?Format=csv&SearchBy=".urlencode($SearchBy)."&FromDate=".urlencode($FromDate)."&ToDate=".urlencode($ToDate)."')","","","","",0);
	
	$TAGS_OBJ[] = array($Lang['ePayment']['IncomeExpensesReport'], "", 0);
	$MODULE_OBJ = $lpayment->GET_MODULE_OBJ_ARR();
	$linterface->LAYOUT_START();
	
	//debug_pr($records);
	//debug_pr($accountIdToRecords);
	//debug_pr($itemIdToData);
?>
<script type="text/javascript" language="javascript">
function updatePaymentNoticeList() {
    $.post(
        '../../ajax_get_payment_notice.php',
        $("form[name='form1']").serialize(),
        function(returnMsg){
            $("#PaymentNoticeID").html(returnMsg);
        }
    );
}

function checkForm(formObj){
    if(formObj==null) return false;
    fromV = formObj.FromDate;
    toV= formObj.ToDate;
    if(!checkDate(fromV)){
        //formObj.FromDate.focus();
        return false;
    }
    else if(!checkDate(toV)){
        //formObj.ToDate.focus();
        return false;
    }
    return true;
}

function checkDate(obj){
    if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
    return true;
}

function submitForm(obj){
	if(checkForm(obj)){
    	obj.submit();
	}
}

function openPrintPage()
{
    var query_MerchantAccount = "";
	<?php foreach($MerchantAccount as $temp) { ?>
    query_MerchantAccount += "&MerchantAccount[]=<?=$temp?>";
	<?php } ?>
     newWindow("index.php?Format=print&SearchBy=<?=urlencode($SearchBy)?>&FromDate=<?=urlencode($FromDate)?>&ToDate=<?=urlencode($ToDate)?>&from_submit=<?=urlencode($from_submit)?>&MerchantAccount_Cash=<?=$MerchantAccount_Cash?>"+query_MerchantAccount,10);
}

function exportPage(obj,url){
	var old_url = obj.action;
	var old_target = obj.target;
	var old_format = obj.Format.value;
    obj.action=url;
    obj.target = '_blank';
    obj.Format.value = 'csv';
    obj.submit();
    obj.action = old_url;
    obj.target = old_target;
    obj.Format.value = old_format;
}
</script>
<br />
<form id="form1" name="form1" method="GET" action="">
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?=$Lang['ePayment']['DateType']?></td>
	  	<td valign="top" nowrap="nowrap" class="tabletext" width="70%">
	    	<input type="radio" id="SearchByPostTime" name="SearchBy" value="1"  <?=($SearchBy!=2?"checked":"")?> />
			<label for="SearchByPostTime"><?=$i_Payment_Search_By_PostTime?></label>&nbsp;&nbsp;
			<input type="radio" id="SearchByTransactionTime" name="SearchBy" value="2" <?=($SearchBy==2?"checked":"")?> />
			<label for="SearchByTransactionTime"><?=$i_Payment_Search_By_TransactionTime?></label>
	    </td>
  	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_Profile_From ?></td>
		<td valign="top" class="tabletext" width="70%">
			<?php echo $linterface->GET_DATE_PICKER("FromDate",$FromDate, "","yy-mm-dd","","","","updatePaymentNoticeList();"); ?>
			<span class="tabletextremark">(yyyy-mm-dd)</span>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $i_Profile_To ?></td>
		<td valign="top" class="tabletext" width="70%">
			<?php echo $linterface->GET_DATE_PICKER("ToDate",$ToDate, "","yy-mm-dd","","","","updatePaymentNoticeList();"); ?>
			<span class="tabletextremark">(yyyy-mm-dd)</span>
		</td>
	</tr>
    <tr>
        <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?= $Lang['ePayment']['MerchantAccount'] ?></td>
        <td valign="top" class="tabletext" width="70%">
			<?php
			echo $linterface->Get_Checkbox("MerchantAccount_Cash", "MerchantAccount_Cash", 1, ($MerchantAccount_Cash == '1'), "", $i_Payment_Credit_TypeCashDeposit);
			echo '<br/>';
			foreach($merchant_accounts_all as $temp) {
				echo $linterface->Get_Checkbox("MerchantAccount_" . $temp['AccountID'], "MerchantAccount[]", $temp['AccountID'], (in_array($temp['AccountID'], $MerchantAccount)), "", $temp['AccountName']);
				echo '<br/>';
			}
			?>
        </td>
    </tr>
	<tr>
		<td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	</tr>
	<tr>
		<td colspan="2" align="center" class="tabletext">
			<?= $linterface->GET_ACTION_BTN($button_view, "button", "submitForm(document.form1);","submit_btn") ?>
		</td>
	</tr>
</table>
<input type="hidden" id="Format" name="Format" value="<?=escape_double_quotes($Format)?>" />
<input type="hidden" name="from_submit" value="1" />
<br />
<?=$y?>
<br />
</form>
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?> <?=$toolbar2?></td>
	</tr>
</table>
<?=$x?>
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td class="tabletext" align="right"><?="$i_general_report_creation_time : ".date('Y-m-d H:i:s')?></td>
	</tr>
</table>
<br />
<?php
	$linterface->LAYOUT_STOP();
}
intranet_closedb();
?>