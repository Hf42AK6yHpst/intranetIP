<?php

#################################
#	Date: 2017-11-03 (Pun)
#			- File Created
#################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libemail.php");
include_once($PATH_WRT_ROOT."includes/libsendmail.php");
include_once($PATH_WRT_ROOT."includes/libsurvey.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lu = new libuser($UserID);
$li = new libsurvey();
$ligroup = new libgrouping();


//$Title = intranet_htmlspecialchars(trim($Title));
$Description = intranet_htmlspecialchars(trim($Description));
$StartDate = intranet_htmlspecialchars(trim($StartDate));
$EndDate = intranet_htmlspecialchars(trim($EndDate));
$qStr2 = $qStr;
$qStr = intranet_htmlspecialchars(trim($qStr));
$AllFieldsReq = $AllFieldsReq==1 ? 1 : 0;
$DisplayQuestionNumber = $DisplayQuestionNumber==1 ? 1 : 0;

if (sizeof($GroupID)!=0)
{
	$GroupID = array_unique($GroupID);
	$GroupID = array_values($GroupID);
}

$startStamp = strtotime($StartDate);
$endStamp = strtotime($EndDate);

if (compareDate($startStamp,$endStamp)>0)   // start > end
{
	$valid = false;
}
else if (compareDate($startStamp,time())<0)      // start < now
{
	$valid = false;
}
else
{
	$valid = true;
}
// Create surveys according to subject group setting
$SubjectClassMapping = array();
foreach($selectedSubjectGroup as $sgid){
	$sql = "SELECT s.CH_ABBR FROM SUBJECT_TERM st LEFT JOIN ASSESSMENT_SUBJECT s ON st.SubjectID=s.RecordID WHERE st.SubjectGroupID='".$sgid."'";
	$result = current($li->returnVector($sql));
	$SubjectClassMapping[$sgid] = array(
		"Class" => "",
		"Subject" => $result,
		"Teacher" => array()
	);
	$sql = "SELECT IF(u.ChineseName='', u.EnglishName, u.ChineseName) as Name, u.UserID FROM SUBJECT_TERM_CLASS_TEACHER st  INNER JOIN INTRANET_USER u ON st.UserID=u.UserID WHERE st.SubjectGroupID='".$sgid."'";
	$SubjectClassMapping[$sgid]["Teacher"] = $li->returnArray($sql);
	$sql = "SELECT DISTINCT ClassTitleB5 FROM SUBJECT_TERM_CLASS_YEAR_RELATION r 
			INNER JOIN YEAR y ON r.YearID=y.YearID
			INNER JOIN YEAR_CLASS yc ON yc.YearID = y.YearID
			INNER JOIN YEAR_CLASS_USER ycu ON yc.YearClassID=ycu.YearClassID
			INNER JOIN INTRANET_USER u ON ycu.UserID=u.UserID
			INNER JOIN SUBJECT_TERM_CLASS_USER su ON su.UserID=u.UserID AND su.SubjectGroupID = r.SubjectGroupID
			WHERE r.SubjectGroupID='".$sgid."' AND yc.AcademicYearID='".$AcademicYear."'";
	$SubjectClassMapping[$sgid]["Class"] = implode("",$li->returnVector($sql));
		
}

if($valid)
{
	$username = $lu->getNameForRecord();
	foreach($selectedSubjectGroup as $sgid){
		foreach($SubjectClassMapping[$sgid]["Teacher"] as $teacher){
			$Title = $SubjectClassMapping[$sgid]["Class"]."-".$SubjectClassMapping[$sgid]["Subject"]."-".$teacher['Name'];
			if ($sys_custom['SurveySaveWithDraft']) {
				$sql = "INSERT INTO INTRANET_SURVEY
						(Title, Description, DateStart, RecordStatus, 
						DateInput, DateModified, DateEnd, PosterID,
						PosterName, Question, RecordType, AllFieldsReq, DisplayQuestionNumber, allowSaveDraft, QuestionType) 
						VALUES
						('$Title', '$Description', '$StartDate', '$RecordStatus', 
						now(), now(),'$EndDate',  $UserID,
						'$username', '$qStr', '$isAnonymous', '$AllFieldsReq', '$DisplayQuestionNumber', '$allowSaveDraft', 'LT')
						";
			} else {
				$sql = "INSERT INTO INTRANET_SURVEY
				(Title, Description, DateStart, RecordStatus,
				DateInput, DateModified, DateEnd, PosterID,
				PosterName, Question, RecordType, AllFieldsReq, DisplayQuestionNumber, QuestionType)
				VALUES
				('$Title', '$Description', '$StartDate', '$RecordStatus',
				now(), now(),'$EndDate',  $UserID,
				'$username', '$qStr', '$isAnonymous', '$AllFieldsReq', '$DisplayQuestionNumber', 'LT')
				";
			}
			$li->db_db_query($sql);
			$SurveyID = $li->db_insert_id();	

			$group_survey_field = "";
			$delimiter = "";
			$group_survey_field .= "$delimiter(".$sgid.",$SurveyID, ".$teacher['UserID'].")";
			$delimiter = ",";
			$sql = "INSERT INTO INTRANET_SUBJECTGROUPSURVEY (SUbjectGroupID , SurveyID, TeacherID) VALUES $group_survey_field";
			$li->db_db_query($sql);
			
			# Call sendmail function
			if($email_alert==1)
			{
				$type = $Lang['EmailNotification']['SchoolNews']['Group'];
				
				if (sizeof($GroupID)>0)
				{
					$Groups = $ligroup->returnGroupNames($GroupID);
				}
				
				list($mailSubject, $mailBody) = $li->returnEmailNotificationData($StartDate,$Title,$type, array($SubjectClassMapping[$sgid]["Class"]."-".$SubjectClassMapping[$sgid]["Subject"]));
				
				$lwebmail = new libwebmail();
				$sql = "SELECT UserID FROM SUBJECT_TERM_CLASS_USER WHERE SubjectGroupoID='".$sgid."' ";
				$ToArray = $li->returnVector($sql);
				$lwebmail->sendModuleMail($ToArray,$mailSubject.' - '.$Title,$mailBody);
			}
		}
	}
	header ("Location: index.php?filter=$RecordStatus&xmsg=AddSuccess&SurveyID=$SurveyID&QuestionType=LT");	
}
else
{
 	$qStr2 = str_replace("&","_AND_",trim($qStr2));
	$qStr2 = str_replace("#","___",trim($qStr2));
	
	header ("Location: lnt_new.php?t=$Title&d=$Description&ad=$AnnouncementDate&ed=$EndDate&r=$RecordStatus&ia=$isAnonymous&ar=$AllFieldsReq&qStr=$qStr2&xmsg=AddUnsuccess");
}

intranet_closedb();

?>