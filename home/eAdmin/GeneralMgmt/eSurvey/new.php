<?php
# using: Bill

#################################
#
#	Date: 2016-05-04 (Bill)		[2016-0428-1355-16207]
#			- Support loading content from eSurvey template
#			- Update wordings and remarks
#
#	Date: 2015-12-21 (Kenneth)	[2015-1113-1510-52071]
#			- Move "Pending" then "Template" below Notification option
#			- Move "Send email to users" below "Publish"
#			- Add remark - System will send email immediately after SUBMIT
#			- Move "Anonymous" and "All questions are required to be answered" in "Survey Form"
#			- change wordings: "Published" to "Publish"
#			- js to toggle disable/enable of checkbox 
#
#	Date: 2013-04-19 (Yuen)
#			- support showing alumni groups (which are created by system when adding alumni)
#			- support notification by email
#
#	Date:	2012-09-12 YatWoon
#			Improved: default "All questions are required to be answered" is checked. [Case#2012-0907-1757-42071]
#
#	Date:	2012-06-29	YatWoon
#			can display ECA group for group selection
#
#	Date:	2011-02-23	YatWoon
#			Add "Display question number"
#
#################################

	$PATH_WRT_ROOT = "../../../../";
	
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	
	if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"])
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}

	include_once($PATH_WRT_ROOT."includes/libinterface.php");
 	include_once($PATH_WRT_ROOT."includes/libgrouping.php");
// 	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
// 	include_once($PATH_WRT_ROOT."includes/libaccount.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."includes/libsurvey.php");
	
	intranet_auth();
	intranet_opendb();
	
	# Create a new interface instance
	$linterface = new interface_html();
	$lsurvey = new libsurvey();
	
	# Page title
	$CurrentPageArr['eAdmineSurvey'] = 1;
	$CurrentPage = "PageSurveyList";
// 	$PAGE_TITLE = $Lang['eSurvey']['SurveyList'];
	$TAGS_OBJ[] = array($Lang['eSurvey']['SurveyList']);
	$MODULE_OBJ = $lsurvey->GET_MODULE_OBJ_ARR();
	
	$PAGE_NAVIGATION[] = array($Lang['eSurvey']['SurveyList'], "javascript:history.back();");
	$PAGE_NAVIGATION[] = array($Lang['eSurvey']['NewSurvey']);
	
	# Date Setting
	$preStartDate = date('Y-m-d');
    $preEndDate = date('Y-m-d');
		
	// special_announce_public_allowed
	$lo = new libgrouping();
	
	# [2016-0428-1355-16207] eSurvey Templates
	$survey_templates = $lsurvey->returnSurveyTemplates();
	$NAoption = array(-1, $i_Notice_NotUseTemplate);
	if (sizeof($survey_templates)!=0)
	{
	    array_unshift($survey_templates, $NAoption);
	}
	else
	{
	    $survey_templates = array($NAoption);
	}
	$template_selection = getSelectByArray($survey_templates, "name='templateID' onChange=\"selectTemplate()\"", '', '', 1);
	
	if ($sys_custom['SurveySaveWithDraft']) {
		$AllowSaveDraft = $lsurvey->DefaultAllowSaveDraft;
	}
	
	# Start layout
	$linterface->LAYOUT_START();
?>

<script language="javascript">

$(document).ready(function() {
 	$('input:radio[name="RecordStatus"]').click(
 		function(){
 			if(this.value==2 || this.value==3){
 				$('input:checkbox[name="email_alert"]').removeAttr('checked');
 				$('input:checkbox[name="email_alert"]').attr('disabled','true');
 			}
 			else if(this.value==1){	
 				$('input:checkbox[name="email_alert"]').removeAttr('disabled');
 			}
 		}
 	);
});

function checkOption1(obj){
	for(i=0; i<obj.length; i++){
		if(!parseInt(obj.options[i].value)){
				obj.options[i] = null;
		}
	}
}

function removeAllOption(from, to){		
	checkOption1(from);
	checkOption1(to);
	for(i=0; i<from.length; i++){
		to.options[to.length] = new Option(from.options[i].text, from.options[i].value, false, false);
	}
	checkOptionClear(from);
	text1 = "";
	for(i=0; i<20; i++) 
		text1 += " ";
	checkOptionAdd(from, text1, "")
}

function checkform(obj)
{
        if(!check_date(obj.StartDate, "<?php echo $i_invalid_date; ?>")) return false;
        if(!check_date(obj.EndDate, "<?php echo $i_invalid_date; ?>")) return false;
        if(compareDate(obj.EndDate.value,obj.StartDate.value)<0) 
		{
			alert ("<?=$i_con_msg_date_startend_wrong_alert?>"); 
			return false;
		}
		if (compareDate('<?=date('Y-m-d')?>', obj.StartDate.value) > 0) 
        {
	        alert ("<?php echo $Lang['eSurvey']['StartDateWarning']; ?>"); 
	        return false;
		}
		
        if(!check_text(obj.Title, "<?php echo $i_alert_pleasefillin.$Lang['eSurvey']['Title']; ?>")) return false;
        
        if(Trim(obj.qStr.value)=="")
        {
                alert("<?=$Lang['eSurvey']['PleaseConstructSurvey']?>");
                return false;
		}
		
        var PublicDisplay = document.getElementById("publicdisplay");
		var Group = document.getElementById("GroupID");
		if(PublicDisplay.checked == false){
			if(Group.length==0){
				alert ("<?=$Lang['SysMgr']['SchoolNews']['WarnSelectGroup']?>");
				return false;
			}
		}
		
		checkOptionAll(obj.elements["GroupID[]"]);
		checkOption1(document.getElementById("GroupID"));
}

function back(){
  window.location="index.php";        
}

function groupSelection(){
	var obj = document.getElementById("publicdisplay");
	var group = document.getElementById("groupSelectionBox");
	if(obj.checked == false){
		checkOption1(document.getElementById("GroupID"));
		group.style.display = "block";
	}
	else{
		group.style.display = "none";
		removeAllOption(document.getElementById("GroupID"), document.getElementById("AvailableGroupID"));
	}
}

function selectTemplate()
{
	var t_index = document.form1.templateID.selectedIndex;
	
	if(confirm("<?=$i_Form_chg_template?>"))
	{
		document.form1.CurrentTemplateIndex.value = t_index;
		var tid = document.form1.templateID.value;
		
		// Update eSurvey Content
		$('#div_content').load(
			'ajax_loadContent.php',
			{ templateID: tid },
			function (data){ }
		); 
	}
	else
	{
		document.form1.templateID.selectedIndex = document.form1.CurrentTemplateIndex.value;
	}
}

</script>

<form name="form1" action="new_update.php" method="post" onSubmit="return checkform(this);">
	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($msg, $xmsg);?></td>
		</tr>
		<tr>
			<td class="navigation">
				<table border="0" cellpadding="5" cellspacing="0">
				<tr><td><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?></td></tr>
				</table>
			</td>
		</tr>
	</table>
	
	<!-- eSurvey Template -->
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center" class='form_table_v30' style="padding-left: 15px;">
		<tr>
			<td class="tabletext field_title" width="30%" valign="top"><?=$i_Notice_FromTemplate?></td>
			<td align="left" valign="top"><?=$template_selection?></td>
		</tr>
	</table>
	
	<!-- Content -->
	<div id="div_content" style="padding-left: 15px;">
		<table width="90%" class='form_table_v30'>
		<!-- Start Date -->
		<tr>
			<td class="tabletext field_title" nowrap="nowrap" valign="top"><?=$Lang['eSurvey']['StartDate']?> <span class="tabletextrequire">*</span></td>
			<td align="left" valign="top">
				<?=$linterface->GET_DATE_PICKER("StartDate", ($ad==""?$preStartDate:$ad))?>
			</td>
		</tr>
	
		<!-- End Date -->
		<tr>
			<td class="tabletext field_title" width="30%" valign="top"><?=$Lang['eSurvey']['EndDate']?> <span class="tabletextrequire">*</span></td>
			<td align="left" valign="top">
				<?=$linterface->GET_DATE_PICKER("EndDate", ($ed==""?$preEndDate:$ed))?>
			</td>
		</tr>
		
		<!-- Title -->
		<tr>
		   <td class="tabletext field_title" width="30%" valign="top"><?=$Lang['eSurvey']['Title']?> <span class="tabletextrequire">*</span></td>
			<td align="left" valign="top">
			  <input type="text" class="textboxtext" name="Title" value="<?=$t?>">
			</td>
		</tr>
		
		<!-- Description -->
		<tr>
			<td class="tabletext field_title" valign="top"><?=$Lang['SysMgr']['SchoolNews']['Description']?></td>
			<td align="left" valign="top"><?=$linterface->GET_TEXTAREA("Description", $d)?></td>
		</tr>
<?php if ($sys_custom['SurveySaveWithDraft']) { ?>
		<tr>
			<td class="tabletext field_title" valign="top"><?php echo $Lang['eSurvey']['AllowSaveDraft']; ?></td>
			<td align="left" valign="top">
				<input type="radio" name="AllowSaveDraft" value="1" <?php echo ($AllowSaveDraft? "checked":"") ?> id="AllowSaveDraft1"> <label for="AllowSaveDraft1"><?=$i_general_yes?></label>
				<input type="radio" name="AllowSaveDraft" value="0" <?php echo ($AllowSaveDraft? "":"checked") ?> id="AllowSaveDraft0"> <label for="AllowSaveDraft0"><?=$i_general_no?></label>
			</td>
		</tr>
<?php } ?>
	    </table>

	<input type="hidden" name="qStr" value="<?=str_replace("___","#",str_replace("_AND_","&",$qStr))?>">
	<input type="hidden" name="aStr" value="">
	</div>

	<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center" class='form_table_v30' style="padding-left: 15px;">
		<!-- Record Status -->
		<tr>
			<td class="tabletext field_title" valign="top" rowspan="3"><?=$Lang['SysMgr']['SchoolNews']['Status']?></td>
			<td align="left" valign="top" style="line-height:18px; border-bottom:1px solid #EFEFEF;">
				<input type="radio" name="RecordStatus" id="RecordStatus1" value="1" CHECKED> <label for="RecordStatus1"><?=$Lang['SysMgr']['SchoolNews']['StatusPublish']?></label>
				</br>
			  	<span style='margin-left:20px;'>
					<input type="checkbox" name="email_alert" id="email_alert" value="1"> <label for="email_alert"><?=$Lang['SysMgr']['SchoolNews']['EmailAlert']?></label>
				</span>
				</br>
				<span style='margin-left:20px;'>(<?=$Lang['eNotice']['NotifyRemark']?>)</span>
			</td>
		</tr>
		<tr>
			<td style="line-height:18px; border-bottom:1px solid #EFEFEF;">
				<input type="radio" name="RecordStatus" id="RecordStatus2" value="2" <?=($r==2?"CHECKED":"");?>> <label for="RecordStatus2"><?=$i_status_pending?></label>
			</td>
		</tr>
		<tr>
			<td style="line-height:18px;">
				<input type="radio" name="RecordStatus" id="RecordStatus3" value="3" <?=($r==3?"CHECKED":"");?>> <label for="RecordStatus3"><?=$i_status_template?><br/><?=$Lang['eSurvey']['TemplateRemarks']?></label>
			</td>
		</tr>
		
		<!-- Survey Form -->
		<tr>
			<td class="tabletext field_title" width="30%" valign="top"><?=$Lang['eSurvey']['SurveyForm']?></td>
			<td style="line-height:18px;">
				<?=$linterface->GET_BTN($Lang['Btn']['Edit'], "button","newWindow('editform.php',1)");?>
				<?=$linterface->GET_BTN($Lang['eSurvey']['Preview'], "button","newWindow('preview.php',1)");?>
				</br>
				<span>
					<input name='DisplayQuestionNumber' type="checkbox" value="1" id="DisplayQuestionNumber" > <label for="DisplayQuestionNumber"><?=$Lang['eSurvey']['DisplayQuestionNumber']?></label>
				</span>
				</br>
				<span>
					<input name="AllFieldsReq" type="checkbox" value="1" id="AllFieldsReq" CHECKED> <label for="AllFieldsReq"><?=$i_Survey_AllRequire2Fill?></label>
				</span>
				</br>
				<span>
					<input type="checkbox" name="isAnonymous" value="1" id="isAnonymous" <?=($ia==1?"CHECKED":"");?>> <label for="isAnonymous"><?="$i_Survey_Anonymous ($i_Survey_Anonymous_Description)"?></label>
				</span>
			</td>
		</tr>
		
		<!-- Survey Target -->
		<tr>
			<td class='field_title'><?php echo $Lang["eSurvey"]["Target"]; ?></td>
			<td align="left" valign="top">
				<!-- Whole School -->
				<span>
					<input type="checkbox" name="publicdisplay" id="publicdisplay" onClick="groupSelection();" value="1" CHECKED> <label for="publicdisplay"><?=$Lang['eSurvey']['WholeSchool']?></label>
				</span>
				<!-- Groups -->
				<div id="groupSelectionBox" style="display:none;"><br>
					<table width="100%" border="0" cellpadding="5" cellspacing="0">
						<tr>
							<td valign="top" width="30%" class='field_title'>
								<?=$i_admintitle_group?>
							</td>
							<td colspan="2">
								<?=$lo->displayAnnouncementGroups("",1, 1);?>
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
	</table>

	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center" style="padding-left: 15px;">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tr><td align="left" class="tabletextremark"><?=$i_general_required_field?></td></tr>
					<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
					<tr>
						<td align="center">
							<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
							<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:back()")?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<input type="hidden" name="CurrentTemplateIndex" value="0">
</form>

<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>