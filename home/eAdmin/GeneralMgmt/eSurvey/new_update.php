<?php

#################################
#	Date: 2017-06-13 (Anna)
#			- added title after $mailSubject
#
#	Date:	2013-04-19	Yuen
#			support notification by email
#
#	Date:	2011-02-23	YatWoon
#			Add "Display question number"
#################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libemail.php");
include_once($PATH_WRT_ROOT."includes/libsendmail.php");
include_once($PATH_WRT_ROOT."includes/libsurvey.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lu = new libuser($UserID);
$li = new libsurvey();
$ligroup = new libgrouping();

$Title = intranet_htmlspecialchars(trim($Title));
$Description = intranet_htmlspecialchars(trim($Description));
$StartDate = intranet_htmlspecialchars(trim($StartDate));
$EndDate = intranet_htmlspecialchars(trim($EndDate));
$qStr2 = $qStr;
$qStr = intranet_htmlspecialchars(trim($qStr));
$AllFieldsReq = $AllFieldsReq==1 ? 1 : 0;
$DisplayQuestionNumber = $DisplayQuestionNumber==1 ? 1 : 0;

if (sizeof($GroupID)!=0)
{
	$GroupID = array_unique($GroupID);
	$GroupID = array_values($GroupID);
}

$startStamp = strtotime($StartDate);
$endStamp = strtotime($EndDate);

if (compareDate($startStamp,$endStamp)>0)   // start > end
{
	$valid = false;
}
else if (compareDate($startStamp,time())<0)      // start < now
{
	$valid = false;
}
else
{
	$valid = true;
}

if($valid)
{
	$username = $lu->getNameForRecord();
	if ($sys_custom['SurveySaveWithDraft']) {
		$sql = "INSERT INTO INTRANET_SURVEY
				(Title, Description, DateStart, RecordStatus, 
				DateInput, DateModified, DateEnd, PosterID,
				PosterName, Question, RecordType, AllFieldsReq, DisplayQuestionNumber, allowSaveDraft) 
				VALUES
				('$Title', '$Description', '$StartDate', '$RecordStatus', 
				now(), now(),'$EndDate',  $UserID,
				'$username', '$qStr', '$isAnonymous', '$AllFieldsReq', '$DisplayQuestionNumber', '$allowSaveDraft')
				";
	} else {
		$sql = "INSERT INTO INTRANET_SURVEY
		(Title, Description, DateStart, RecordStatus,
		DateInput, DateModified, DateEnd, PosterID,
		PosterName, Question, RecordType, AllFieldsReq, DisplayQuestionNumber)
		VALUES
		('$Title', '$Description', '$StartDate', '$RecordStatus',
		now(), now(),'$EndDate',  $UserID,
		'$username', '$qStr', '$isAnonymous', '$AllFieldsReq', '$DisplayQuestionNumber')
		";
	}
	$li->db_db_query($sql);
	$SurveyID = $li->db_insert_id();
	
	if (sizeof($GroupID) && $SurveyID)
	{
		$group_survey_field = "";
		$delimiter = "";
		for ($i=0; $i<sizeof($GroupID); $i++)
		{
			$group_survey_field .= "$delimiter(".$GroupID[$i].",$SurveyID)";
			$delimiter = ",";
		}
		$sql = "INSERT INTO INTRANET_GROUPSURVEY (GroupID, SurveyID) VALUES $group_survey_field";
		$li->db_db_query($sql);
	}
	
	# Call sendmail function
	if($email_alert==1)
	{
		$type = (sizeof($GroupID)==0? $Lang['EmailNotification']['SchoolNews']['School']: $Lang['EmailNotification']['SchoolNews']['Group']);
		
		if (sizeof($GroupID)>0)
		{
			$Groups = $ligroup->returnGroupNames($GroupID);
		}
		
		list($mailSubject, $mailBody) = $li->returnEmailNotificationData($StartDate,$Title,$type, $Groups);
		
		$lwebmail = new libwebmail();
		if(sizeof($GroupID) == 0)
		{
			$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordStatus = '1'";
		}
		else
		{
			$sql = "SELECT UserID FROM INTRANET_USERGROUP WHERE GroupID IN (".implode(",",$GroupID).")";
		}
		$ToArray = $li->returnVector($sql);
		$lwebmail->sendModuleMail($ToArray,$mailSubject.' - '.$Title,$mailBody);
	}
	
	header ("Location: index.php?filter=$RecordStatus&xmsg=AddSuccess&SurveyID=$SurveyID");	
}
else
{
 	$qStr2 = str_replace("&","_AND_",trim($qStr2));
	$qStr2 = str_replace("#","___",trim($qStr2));
	
	header ("Location: new.php?t=$Title&d=$Description&ad=$AnnouncementDate&ed=$EndDate&r=$RecordStatus&ia=$isAnonymous&ar=$AllFieldsReq&qStr=$qStr2&xmsg=AddUnsuccess");
}

intranet_closedb();

?>