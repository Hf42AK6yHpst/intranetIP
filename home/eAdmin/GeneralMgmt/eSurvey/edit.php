<?php
# using: 

#################################
#
#   Date: 2019-05-17 (Bill)
#           - Prevent SQL Injection + Cross-site Scripting
#           - Change hidden input field name from "OriginalGroupID" to "OriginalGroupCode"
#
#	Date: 2017-11-07 (Paul)
#			- Update edit for L&T specific features
#
#	Date: 2016-05-04 (Bill)	[2016-0428-1355-16207]
#			- Update wordings
#
#	Date: 2015-12-21 (Kenneth)	[2015-1113-1510-52071]
#			- Move "Pending" then "Template" below Notification option
#			- Move "Send email to users" below "Publish"
#			- Add remark - System will send email immediately after SUBMIT
#			- Move "Anonymous" and "All questions are required to be answered" in "Survey Form"
#			- change wordings: "Published" to "Publish"
#			- js to toggle disable/enable of checkbox 
#
#	Date: 2015-08-18 (Omas) #Q80431 
#		  - support edit target group after there are submission
#
#	Date: 2013-04-19 (Yuen)
#			- support showing alumni groups (which are created by system when adding alumni)
#			- support notification by email
#
#	Date:	2011-02-23	YatWoon
#			Add "Display question number"
#
#################################

	$PATH_WRT_ROOT = "../../../../";
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	
	if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"])
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
    
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
 	include_once($PATH_WRT_ROOT."includes/libgrouping.php");
// 	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
// 	include_once($PATH_WRT_ROOT."includes/libaccount.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."includes/libsurvey.php");
	
	intranet_auth();
	intranet_opendb();
	
	### Handle SQL Injection + XSS [START]
	$SurveyID = IntegerSafe($SurveyID);
	
	$ad = intranet_htmlspecialchars(trim($ad));
	$ed = intranet_htmlspecialchars(trim($ed));
	$t = intranet_htmlspecialchars(trim($t));
	$d = intranet_htmlspecialchars(trim($d));
	$qStr = intranet_htmlspecialchars(trim($qStr));
	### Handle SQL Injection + XSS [END]
	
	$SurveyID = is_array($SurveyID)? $SurveyID[0] : $SurveyID;
	
	# Create a new interface instance
	$linterface = new interface_html();
	
	$lsurvey = new libsurvey($SurveyID);
	$lo = new libgrouping();
	
	# Page title
	$CurrentPageArr['eAdmineSurvey'] = 1;
	$CurrentPage = "PageSurveyList";
// 	$PAGE_TITLE = $Lang['eSurvey']['SurveyList'];
    
	$TAGS_OBJ[] = array($Lang['eSurvey']['SurveyList']);
	$MODULE_OBJ = $lsurvey->GET_MODULE_OBJ_ARR();
	
	$PAGE_NAVIGATION[] = array($Lang['eSurvey']['SurveyList'], "javascript:history.back();");
	$PAGE_NAVIGATION[] = array($Lang['eSurvey']['EditSurvey']);
	
	# Start layout
	$linterface->LAYOUT_START();
	
	$editable = $lsurvey->isSurveyEditable();
	
	$preStartDate = $ad=="" ? $lsurvey->DateStart : $ad;
    $preEndDate = $ed=="" ? $lsurvey->DateEnd: $ed;
    $preTitle = $t=="" ? $lsurvey->Title : $t;
    $preAllFieldsReq = $ar=="" ? $lsurvey->AllFieldsReq : $ar;
    $preisAnonymous = $ia=="" ? $lsurvey->RecordType : $ia;
    $preRecordStatus = $r=="" ? $lsurvey->RecordStatus : $r;
	$preDescription = $d=="" ? $lsurvey->Description : $d;
	$qStr = $qStr=="" ? $lsurvey->Question : $qStr;
	$TargetGroup = $lsurvey->returnTargetGroupsArray();
	$TargetGroupIDArr = Get_Array_By_Key($TargetGroup,'GroupID');
	$QuestionType = $lsurvey->QuestionType;
	
	$allowDraftSetting = false;
	if ($sys_custom['SurveySaveWithDraft']) {
		$allowDraftSetting = true;
	}
	if ($allowDraftSetting) {
		$hasDraft = $lsurvey->isSurveyFilledDraft();
		$preAllowSaveDraft = $sd=="" ? $lsurvey->AllowSaveDraft: $lsurvey->DefaultAllowSaveDraft;
	}
?>

<script language="javascript">

$(document).ready(function() {
 	$('input:radio[name="RecordStatus"]').click(
 		function(){
 			if(this.value==2 || this.value==3){
 				$('input:checkbox[name="email_alert"]').removeAttr('checked');
 				$('input:checkbox[name="email_alert"]').attr('disabled','true');
 			}
 			else if(this.value==1){
 				$('input:checkbox[name="email_alert"]').removeAttr('disabled');
 			}
 		}
 	);
 	if(<?=($preRecordStatus==2 || $preRecordStatus==3)?"true":"false"?>){
 		$('input:checkbox[name="email_alert"]').attr('disabled','true');
 	}
});

function checkOption1(obj){
	for(i=0; i<obj.length; i++){
		if(!parseInt(obj.options[i].value)){
			obj.options[i] = null;
		}
	}
}

function removeAllOption(from, to){		
	checkOption1(from);
	checkOption1(to);
	for(i=0; i<from.length; i++){
		to.options[to.length] = new Option(from.options[i].text, from.options[i].value, false, false);
	}
	checkOptionClear(from);
	text1 = "";
	for(i=0; i<20; i++) 
		text1 += " ";
	checkOptionAdd(from, text1, "")
}

function checkform(obj)
{
        if(!check_date(obj.StartDate, "<?php echo $i_invalid_date; ?>")) return false;
        if(!check_date(obj.EndDate, "<?php echo $i_invalid_date; ?>")) return false;
        if(compareDate(obj.EndDate.value,obj.StartDate.value)<0) {
			alert ("<?=$i_con_msg_date_startend_wrong_alert?>"); 
			return false;
		}
		
		<? /* ?>
		if (compareDate('<?=date('Y-m-d')?>', obj.StartDate.value) > 0) 
        {
	        alert ("<?php echo $Lang['eSurvey']['StartDateWarning']; ?>"); 
	        return false;
		}
		<? */ ?>
		
        if(!check_text(obj.Title, "<?php echo $i_alert_pleasefillin.$Lang['eSurvey']['Title']; ?>")) return false;
        if(Trim(obj.qStr.value)=="")
        {
                alert("<?=$Lang['eSurvey']['PleaseConstructSurvey']?>");
                return false;
		}
		
		<!--? if($editable) {?-->
        var PublicDisplay = document.getElementById("publicdisplay");
		var Group = document.getElementById("GroupID");
		if(PublicDisplay.checked == false) {
			if(Group.length==0) {
				alert ("<?=$Lang['SysMgr']['SchoolNews']['WarnSelectGroup']?>");
				return false;
			}
		}
		
		checkOptionAll(obj.elements["GroupID[]"]);
		//checkOption1(document.getElementById("GroupID[]"));
		checkOption1(document.getElementById("GroupID"));
		<!--? } ?-->
}

function back(){
  window.location="index.php?QuestionType=<?=$QuestionType ?>";        
}

function groupSelection(){
	var obj = document.getElementById("publicdisplay");
	var group = document.getElementById("groupSelectionBox");

	if(obj){
    	if(obj.checked == false)
    	{
    		group.style.display = "block";
    		checkOption1(document.getElementById("GroupID"));
    	}
    	else
    	{
    		removeAllOption(document.getElementById("GroupID"), document.getElementById("AvailableGroupID"));
    		group.style.display = "none";
    	}
	}
}
</script>

<style>
#groupSelectionBox table { width:auto!important; }
</style>

<form name="form1" action="edit_update.php" method="post" onSubmit="return checkform(this);">
	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($msg, $xmsg);?></td>
		</tr>
		<tr>
			<td class="navigation">
				<table width="95%" border="0" cellpadding="5" cellspacing="0">
				<tr><td><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?></td></tr>
				</table>
			</td>
		</tr>
	</table>
	
	<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center" class='form_table_v30' style="padding-left: 15px;">
		<!-- Start Date -->
		<tr>
			<td class="tabletext field_title" width="30%" valign="top"><?=$Lang['eSurvey']['StartDate']?> <span class="tabletextrequire">*</span></td>
			<td align="left" valign="top">
				<?=$linterface->GET_DATE_PICKER("StartDate", $preStartDate)?>
			</td>
		</tr>
		<!-- End Date -->
		<tr>
			<td class="tabletext field_title" width="30%" valign="top"><?=$Lang['eSurvey']['EndDate']?> <span class="tabletextrequire">*</span></td>
			<td align="left" valign="top">
				<?=$linterface->GET_DATE_PICKER("EndDate", $preEndDate)?>
			</td>
		</tr>
		
		<!-- Title -->
		<?php if($sys_custom['iPf']['learnAndTeachReport'] && $QuestionType == 'LT'){ ?>
    		<tr>
    		   <td class="tabletext field_title" width="30%" valign="top"><?=$Lang['eSurvey']['Title']?></td>
    			<td align="left" valign="top">
    				<span><?=($t==""?$preTitle:$t)?></span>
    				<input type="hidden" class="textboxtext" name="Title" value="<?=($t==""?$preTitle:$t)?>">
    			</td>
    		</tr>
		<?php }else{ ?>
    		<tr>
    		   <td class="tabletext field_title" width="30%" valign="top"><?=$Lang['eSurvey']['Title']?> <span class="tabletextrequire">*</span></td>
    			<td align="left" valign="top">
    			  <input type="text" class="textboxtext" name="Title" value="<?=($t==""?$preTitle:$t)?>">
    			</td>
    		</tr>
		<?php } ?>
		
		<!-- Description -->
		<tr>
			<td class="tabletext field_title" valign="top"><?=$Lang['SysMgr']['SchoolNews']['Description']?></td>
			<td align="left" valign="top"><?=$linterface->GET_TEXTAREA("Description", $preDescription)?></td>
		</tr>
<?php if ($allowDraftSetting && !$hasDraft) { ?>
		<tr>
			<td class="tabletext field_title" valign="top"><?php echo $Lang['eSurvey']['AllowSaveDraft']; ?></td>
			<td align="left" valign="top">
				<input type="radio" name="AllowSaveDraft" value="1" <?php echo ($preAllowSaveDraft? "checked":"") ?> id="AllowSaveDraft1"> <label for="AllowSaveDraft1"><?=$i_general_yes?></label> 
				<input type="radio" name="AllowSaveDraft" value="0" <?php echo ($preAllowSaveDraft? "":"checked") ?> id="AllowSaveDraft0"> <label for="AllowSaveDraft0"><?=$i_general_no?></label>
			</td>
		</tr>
<?php } else { ?>
		<tr>
			<td class="tabletext field_title" valign="top"><?php echo $Lang['eSurvey']['AllowSaveDraft']; ?></td>
			<td align="left" valign="top">
				<?php echo $preAllowSaveDraft? $i_general_yes : $i_general_no; ?>
			</td>
		</tr>
<?php } ?>
		
		<!-- Record Status -->
		<tr>
			<td class="tabletext field_title" valign="top" rowspan="3"><?=$Lang['SysMgr']['SchoolNews']['Status']?></td>
			<td align="left" valign="top" style="line-height:18px; border-bottom:1px solid #EFEFEF;">
				<input type="radio" name="RecordStatus" id="RecordStatus1" value="1" CHECKED> <label for="RecordStatus1"><?=$Lang['SysMgr']['SchoolNews']['StatusPublish']?></label> 
			</br>
			<?php if(!$sys_custom['iPf']['learnAndTeachReport'] || $QuestionType != 'LT'){ ?>
    			<span style='margin-left:20px;'>
    				<input type="checkbox" name="email_alert" id="email_alert" value="1"> <label for="email_alert"><?=$Lang['SysMgr']['SchoolNews']['EmailAlert']?></label>
    			</span>
    			</br>
    			<span style='margin-left:20px;'>(<?=$Lang['eNotice']['NotifyRemark']?>)</span>
			<?php } ?>
			</td>
		</tr>
		<tr>
			<td style="line-height:18px; border-bottom:1px solid #EFEFEF;">
			<input type="radio" name="RecordStatus" id="RecordStatus2" value="2" <?=($preRecordStatus==2?"CHECKED":"");?>> <label for="RecordStatus2"><?=$i_status_pending?></label>
			</td>
		</tr>
		<tr>
			<td style="line-height:18px;">
				<input type="radio" name="RecordStatus" id="RecordStatus3" value="3" <?=($preRecordStatus==3?"CHECKED":"");?>> <label for="RecordStatus3"><?=$i_status_template?><br/><?=$Lang['eSurvey']['TemplateRemarks']?></label>
			</td>
		</tr>
		
		<!--? if($editable) {?-->
		<? if($editable) {?>
    		<tr>
    			<td class="tabletext field_title" valign="top"><?=$Lang['eSurvey']['SurveyForm']?></td>
    			<td style="line-height:18px;">
    				<?=$linterface->GET_BTN($Lang['Btn']['Edit'], "button","newWindow('editform.php',1)");?>
    				<?=$linterface->GET_BTN($Lang['eSurvey']['Preview'], "button","newWindow('preview.php',1)");?>
    				</br>
    				<span>
    					<input name='DisplayQuestionNumber' type="checkbox" value="1" id="DisplayQuestionNumber" <?=($lsurvey->DisplayQuestionNumber==1?"CHECKED":"");?>> <label for="DisplayQuestionNumber"><?=$Lang['eSurvey']['DisplayQuestionNumber']?></label>
    				</span>
    				</br>
    				<span>
    					<input name="AllFieldsReq" type="checkbox" value="1" id="AllFieldsReq" <?=($preAllFieldsReq==1?"CHECKED":"");?>> <label for="AllFieldsReq"><?=$i_Survey_AllRequire2Fill?></label>
    				</span>
    				</br>
    				<span>
    					<input type="checkbox" name="isAnonymous" value="1" id="isAnonymous" <?=($preisAnonymous==1?"CHECKED":"");?>> <label for="isAnonymous"><?="$i_Survey_Anonymous ($i_Survey_Anonymous_Description)"?></label>
    				</span>
    			</td>
    		</tr>
		<?}?>
		<tr>
			<td class="field_title"><?php echo $Lang["eSurvey"]["Target"]; ?></td>
			<td align="left" valign="top">
			
				<?php if($sys_custom['iPf']['learnAndTeachReport'] && $QuestionType == 'LT'){ 
					$targetSubjectGroups = $lsurvey->returnTargetSubjectGroups();
					echo implode(",",$targetSubjectGroups);
				?>
				<?php }else{ ?>
    				<input type="checkbox" name="publicdisplay" id="publicdisplay" onClick="groupSelection();" value="1" <?=(sizeof($TargetGroup)>0?"":"CHECKED")?> <?=($editable?'':'disabled')?>> <label for="publicdisplay"><?=$Lang['eSurvey']['WholeSchool']?></label>
    				
    				<!--? if($editable) {?-->
    				<!-- Groups -->
    				<div id="groupSelectionBox" style="display:none;"><br>
    					<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center" class="form_table_v30">
    						<tr>
    							<td valign="top" class="field_title">
    								<?=$i_admintitle_group?>
    							</td>
    							<td>
    								<?=$lo->displaySurveyGroupsFrontend($SurveyID, "GroupID[]", 0, 1)?>
    							</td>
    						</tr>
    						<tr>
    							<td colspan="2"><font color="red"># <?=$Lang['eSurvey']['EditRemarks']?></font></td>
    						</tr>
    					</table>
    				</div>
				<?php } ?>
			</td>
		</tr>
		<!--? } ?-->
	</table>
	
	<!--? } ?-->
	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center" style="padding-left: 15px;">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tr><td align="left" class="tabletextremark"><?=$i_general_required_field?></td></tr>
					<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
					<tr>
						<td align="center">
							<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
							<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:back()")?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<input type="hidden" name="qStr" value="<?=str_replace("___","#",str_replace("_AND_","&",$qStr))?>">
	<input type="hidden" name="aStr" value="">
	<input type="hidden" name="SurveyID" value="<?=$SurveyID?>">
	<input type="hidden" name="OriginalGroupCode" value="<?=urlencode(serialize($TargetGroupIDArr))?>">
	<input type="hidden" name="QuestionType" value="<?=$QuestionType?>">
</form>

<script language="javascript">
<!--? if($editable) {?-->
groupSelection();
<!--? } ?-->
</script>

<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>