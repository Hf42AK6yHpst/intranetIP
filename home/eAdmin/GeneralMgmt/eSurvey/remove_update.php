<?php
# using: Bill

#####################################################
#
#	Date:	2016-02-18	Bill	[2016-0216-1223-01207]
#			Fixed: delete mapping in INTRANET_GROUPSURVEY
#
#	Date:	2011-11-28	YatWoon
#			Improved: add delete log
#
#####################################################
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/liblog.php");

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
	
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$li = new libdb();

# Select which can be deleted
$olist = implode(",",$SurveyID);
$olist = IntegerSafe($olist);
$sql = "SELECT SurveyID, Title, DateStart, DateEnd, PosterID, PosterName FROM INTRANET_SURVEY WHERE SurveyID IN ($olist)";
$result = $li->returnArray($sql);

for($i=0;$i<sizeof($result);$i++)
{
	list($thisSurveyID, $thisTitle, $thisDateStart, $thisDateEnd, $thisPosterID, $thisPosterName ) = $result[$i];
	
	
	# Add deletion log
	$lg = new liblog();
	$this_id = $thisCircularID;
	
	$RecordDetailAry = array();
	$RecordDetailAry['Title'] = $thisTitle;
	$RecordDetailAry['DateStart'] = substr($thisDateStart,0,10);
	$RecordDetailAry['DateEnd'] = substr($thisDateEnd,0,10);
	$RecordDetailAry['IssueUserID'] = $thisPosterID;
	$RecordDetailAry['IssueUserName'] = $thisPosterName;
	
	$lg->INSERT_LOG('eSurvey', 'eSurvey', $RecordDetailAry, 'INTRANET_SURVEY', $thisSurveyID);
	
	$sql = "DELETE FROM INTRANET_SURVEY WHERE SurveyID = '$thisSurveyID'";
    $li->db_db_query($sql);
    $sql = "DELETE FROM INTRANET_SURVEYANSWER WHERE SurveyID = '$thisSurveyID'";
    $li->db_db_query($sql);
    // [2016-0216-1223-01207] delete survey and group mapping
	$sql = "DELETE FROM INTRANET_GROUPSURVEY WHERE SurveyID = '$thisSurveyID'";
    $li->db_db_query($sql);
    if($sys_custom['iPf']['learnAndTeachReport']){
    	// 20171106 delete survey and subject-group mapping
		$sql = "DELETE FROM INTRANET_SUBJECTGROUPSURVEY WHERE SurveyID = '$thisSurveyID'";
   	 	$li->db_db_query($sql);	
    }
}
/*
if (sizeof($result)!=0)
{
    $list = implode(",",$result);
    $sql = "DELETE FROM INTRANET_SURVEY WHERE SurveyID IN ($list)";
    $li->db_db_query($sql);
    $sql = "DELETE FROM INTRANET_SURVEYANSWER WHERE SurveyID IN ($list)";
    $li->db_db_query($sql);
}
*/

header("Location: index.php?&xmsg=DeleteSuccess&QuestionType={$QuestionType}");

intranet_closedb();
?>