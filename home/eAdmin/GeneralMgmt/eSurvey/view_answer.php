<?php

#################################
#	Date:	2015-03-24 (Jason)
#			- add js order_lang to support Ordering Type
#	Date:	2011-02-24	YatWoon
#			Add "Display question number"
#################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
include_once($PATH_WRT_ROOT."includes/libsurvey.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
intranet_auth();
intranet_opendb();

$lform = new libform();
$SurveyID = IntegerSafe($SurveyID);
$lsurvey = new libsurvey($SurveyID);

if ($lsurvey->RecordType == 1)
{
    header("Location: result.php?SurveyID=$SurveyID");
    exit();
}

$MODULE_OBJ['title'] = $i_Survey_perUser;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

# retrieve survey
$viewUser = IntegerSafe($viewUser);
$sql = "SELECT Answer,UserName FROM INTRANET_SURVEYANSWER WHERE SurveyID = '$SurveyID' AND UserID = '$viewUser'";
$answer = $lsurvey->returnArray($sql,2);
list($aStr,$username) = $answer[0];
if ($aStr=="")
{
    header("Location: result_type.php?SurveyID=$SurveyID");
    exit();
}

$queString = $lsurvey->Question;
$queString = str_replace('"','&quot;',$queString);
$queString = str_replace("\n",'<br>',$queString);
$queString = str_replace("\r",'',$queString);
$aStr = str_replace('"','&quot;',$aStr);
$aStr = str_replace("\n",'<br>',$aStr);
$aStr = str_replace("\r",'',$aStr);

#$queString = str_replace('"','&quot;',$queString);
#$aStr = str_replace('"','&quot;',$aStr);


$poster = $lsurvey->returnPosterName();
$ownerGroup = $lsurvey->returnOwnerGroup();
$targetGroups = $lsurvey->returnTargetGroups();

if ($ownerGroup != "")
{
    $poster = "$poster<br>\n$ownerGroup";
}
if (sizeof($targetGroups)==0)
{
    $target = "$i_general_WholeSchool";
}
else
{
    $target = implode(", ",$targetGroups);
}
$luser = new libuser($viewUser);
if ($luser->UserID == $viewUser)
{
    $username = $luser->UserNameClassNumber();
}

$survey_description = nl2br(intranet_convertAllLinks($lsurvey->Description));
$survey_description = $survey_description ? $survey_description : "---";

?>

<br />
<table width=95% cellspacing=0 cellpadding=5 border=0>
<tr>
	<td class="tabletext formfieldtitle" width="30%"><?=$i_general_startdate?></td>
	<td><?=$lsurvey->DateStart?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle" width="30%"><?=$i_general_enddate?></td>
	<td><?=$lsurvey->DateEnd?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle" width="30%"><?=$i_general_title?></td>
	<td><?=$lsurvey->Title?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle" width="30%"><?=$i_general_description?></td>
	<td><?=$survey_description?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle" width="30%"><?=$i_Survey_Poster?></td>
	<td><?=$poster?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle" width="30%"><?=$i_general_TargetGroup?></td>
	<td><?=$target?></td>
</tr>
<tr>
	<td colspan=2>
		<hr>
		
		<B><?=$i_Survey_perUser." ".displayArrow()." ".$username?></B><br>
		
		<script language="javascript" src="/templates/forms/form_view.js"></script>
		<form name="ansForm" method="post" action="update.php">
		        <input type=hidden name="qStr" value="">
		        <input type=hidden name="aStr" value="">
		</form>
		<script language="Javascript">
		var order_lang = '<?=$Lang['eSurvey']['Order']?>';
		var choice_lang = '<?=$Lang['Polling']['Choice']?>';
		var hint_lang = '<?=$Lang['Polling']['Hint']?>';
		var no_answer_lang = '<?=$Lang['eSurvey']['NotAnswered']?>';
		var DisplayQuestionNumber = '<?=$lsurvey->DisplayQuestionNumber?>';
		myQue = "<?=$queString?>";
		myAns = "<?=$aStr?>";
		document.write(viewForm(myQue, myAns));
		</SCRIPT>
		<hr>
	</td>
</tr>
<tr>
	<td colspan="2" align="center">
		<?
		//$linterface->GET_BTN($button_print, "button", "window.print();") 
		echo $linterface->GET_ACTION_BTN($button_print, "button", "newWindow('print.php?SurveyID=".$SurveyID."&viewUser=".$viewUser."', 37);");
		?>
		<?=$linterface->GET_ACTION_BTN($button_close, "button", "window.close();") ?>
		<?=$linterface->GET_ACTION_BTN($button_back, "button", "history.back()") ?>
	</td>
</tr>
</table>
         
<?php

intranet_closedb();
$linterface->LAYOUT_STOP();
?>