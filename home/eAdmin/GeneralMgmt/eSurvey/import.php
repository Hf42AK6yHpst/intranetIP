<?php
// Modifying by: 

##################################################
#	
#	Date:	2017-02-23 Anna	[2016-0928-1637-22240]
#			Create File		($sys_custom['SurveyImportFunction'])
#
##################################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"] || !$sys_custom['SurveyImportFunction'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libsurvey.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# Create interface instance
$linterface = new interface_html();
$lsurvey = new libsurvey();

# Page Title
$CurrentPageArr['eAdmineSurvey'] = 1;
$CurrentPage = "PageSurveyList";

# Tag Information
$TAGS_OBJ[] = array($Lang['eSurvey']['SurveyList']);

# Left Menu
$MODULE_OBJ = $lsurvey->GET_MODULE_OBJ_ARR();

# Navigation
$PAGE_NAVIGATION[] = array($Lang['eSurvey']['SurveyList'], "javascript:history.back();");
$PAGE_NAVIGATION[] = array($Lang['eSurvey']['NewSurvey']);

# Step Information
$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($i_general_confirm_import_data, 0);
$STEPS_OBJ[] = array($i_general_complete, 0);

/*
// special_announce_public_allowed
$lo = new libgrouping();

# eSurvey Templates
$survey_templates = $lsurvey->returnSurveyTemplates();
$NAoption = array(-1, $i_Notice_NotUseTemplate);
if (sizeof($survey_templates)!=0)
{
	array_unshift($survey_templates, $NAoption);
}
else
{
	$survey_templates = array($NAoption);
}
$template_selection = getSelectByArray($survey_templates, "name='templateID' onChange=\"selectTemplate()\"", '', '', 1);
*/

# Sample CSV file
$sample_file = "import_survey_sample_unicode.csv";
$sample_file_update = "update_survey_sample_unicode.csv";
$csvFile = "<a class=\"tablelink\" href=\"/templates/get_sample_csv.php?file=$sample_file\" target=\"_self\">".$Lang['General']['ClickHereToDownloadSample']."</a>";
$csvFileUpdate = "<a class=\"tablelink\" href=\"/templates/get_sample_csv.php?file=$sample_file_update\" target=\"_self\">".$Lang['General']['ClickHereToDownloadSample']."</a>";

# Data Column
// Column Fields
$DataColumnTitleArr = array($Lang['eSurvey']['TemplateTitle'], $Lang['eSurvey']['StartDate'], $Lang['eSurvey']['EndDate'], $Lang['eSurvey']['Title'], $Lang['SysMgr']['SchoolNews']['Description'], $Lang['SysMgr']['SchoolNews']['Status'], $Lang['eSurvey']['DisplayQuestionNumber'], $i_Survey_AllRequire2Fill, $i_Survey_Anonymous, $Lang['eSurvey']['Target']);
$DataColumnTitleArr2 = array($Lang['eSurvey']['StartDate'], $Lang['eSurvey']['EndDate'], $Lang['eSurvey']['Title'], $Lang['SysMgr']['SchoolNews']['Description'], $Lang['SysMgr']['SchoolNews']['Status'], $Lang['eSurvey']['DisplayQuestionNumber'], $i_Survey_AllRequire2Fill, $i_Survey_Anonymous);

// Field Type
$DataColumnPropertyArr = array('2', '1', '1', '2', '', '1', '1', '1', '1', '1');
$DataColumnPropertyArr2 = array('2', '2', '1', '', '1', '1', '1', '1');

// Remarks
$DataColumnRemarksArr = array($Lang['eSurvey']['Import']['Remarks1'], '(YYYY-MM-DD)', '(YYYY-MM-DD)', '', '', '(Publish/Pending)', '(Y/N)', '(Y/N)', '(Y/N)', $Lang['eSurvey']['Import']['Remarks4']);
$DataColumnRemarksArr2 = array('(YYYY-MM-DD) '.$Lang['eSurvey']['Import']['Remarks2'], '(YYYY-MM-DD) '.$Lang['eSurvey']['Import']['Remarks3'], '', '', '(Publish/Pending)', '(Y/N)', '(Y/N)', '(Y/N)');

// Get Column Display
$csv_format = $linterface->Get_Import_Page_Column_Display($DataColumnTitleArr, $DataColumnPropertyArr, $DataColumnRemarksArr);
$csv_format_update = $linterface->Get_Import_Page_Column_Display($DataColumnTitleArr2, $DataColumnPropertyArr2, $DataColumnRemarksArr2);

# Import Mode Radio buttons
$radioButtons = $linterface->Get_Radio_Button('insertNew', 'importMode', 'new', $isChecked=1, $Class="", $Lang['eDiscipline']['Import']['ImportNew'], $Onclick="onClickImportMode(this);",$isDisabled=0);
$radioButtons .= $linterface->Get_Radio_Button('updateExist', 'importMode', 'update', $isChecked=0, $Class="", $Lang['eDiscipline']['Import']['UpdateExisting'], $Onclick="onClickImportMode(this);",$isDisabled=0);

# Start layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
//var ClickID = '';
//
//$(document).ready(function() {
// 	$('input:radio[name="RecordStatus"]').click(
// 		function(){
// 			if(this.value==2 || this.value==3){
// 				$('input:checkbox[name="email_alert"]').removeAttr('checked');
// 				$('input:checkbox[name="email_alert"]').attr('disabled','true');
// 			}
// 			else if(this.value==1){	
// 				$('input:checkbox[name="email_alert"]').removeAttr('disabled');
// 			}
// 		}
// 	);
//});
//
//function checkOption1(obj){
//	for(i=0; i<obj.length; i++){
//		if(!parseInt(obj.options[i].value)){
//				obj.options[i] = null;
//		}
//	}
//}
//
//function show_ref_list(type,click)
//{
//	ClickID = click;
//	document.getElementById('task').value = type;
//	
//	obj = document.form1;
//	YAHOO.util.Connect.setForm(obj);
//	
//	var path = "ajax_ap.php";
//    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_show_ref_list);
//}
//
//
//function removeAllOption(from, to){
//	checkOption1(from);
//	checkOption1(to);
//	for(i=0; i<from.length; i++){
//		to.options[to.length] = new Option(from.options[i].text, from.options[i].value, false, false);
//	}
//	checkOptionClear(from);
//	
//	text1 = "";
//	for(i=0; i<20; i++) 
//		text1 += " ";
//	checkOptionAdd(from, text1, "")
//}
//
//function checkform(obj)
//{
//    if(!check_date(obj.StartDate, "<?php echo $i_invalid_date; ?>")) return false;
//    if(!check_date(obj.EndDate, "<?php echo $i_invalid_date; ?>")) return false;
//    if(compareDate(obj.EndDate.value,obj.StartDate.value)<0) 
//	{
//		alert ("<?=$i_con_msg_date_startend_wrong_alert?>"); 
//		return false;
//	}
//	if (compareDate('<?=date('Y-m-d')?>', obj.StartDate.value) > 0) 
//    {
//        alert ("<?php echo $Lang['eSurvey']['StartDateWarning']; ?>"); 
//        return false;
//	}
//	
//    if(!check_text(obj.Title, "<?php echo $i_alert_pleasefillin.$Lang['eSurvey']['Title']; ?>")) return false;
//    
//    if(Trim(obj.qStr.value)=="")
//    {
//            alert("<?=$Lang['eSurvey']['PleaseConstructSurvey']?>");
//            return false;
//	}
//	
//    var PublicDisplay = document.getElementById("publicdisplay");
//	var Group = document.getElementById("GroupID");
//	if(PublicDisplay.checked == false){
//		if(Group.length==0){
//			alert ("<?=$Lang['SysMgr']['SchoolNews']['WarnSelectGroup']?>");
//			return false;
//		}
//	}
//	
//	checkOptionAll(obj.elements["GroupID[]"]);
//	checkOption1(document.getElementById("GroupID"));
//}

function back(){
  window.location="index.php";        
}

//function groupSelection(){
//	var obj = document.getElementById("publicdisplay");
//	var group = document.getElementById("groupSelectionBox");
//	if(obj.checked == false){
//		checkOption1(document.getElementById("GroupID"));
//		group.style.display = "block";
//	}
//	else{
//		group.style.display = "none";
//		removeAllOption(document.getElementById("GroupID"), document.getElementById("AvailableGroupID"));
//	}
//}
//
//function selectTemplate()
//{
//	var t_index = document.form1.templateID.selectedIndex;
//	
//	if(confirm("<?=$i_Form_chg_template?>"))
//	{
//		document.form1.CurrentTemplateIndex.value = t_index;
//		var tid = document.form1.templateID.value;
//		
//		// Update eSurvey Content
//		$('#div_content').load(
//			'ajax_loadContent.php',
//			{ templateID: tid },
//			function (data){ }
//		); 
//	}
//	else
//	{
//		document.form1.templateID.selectedIndex = document.form1.CurrentTemplateIndex.value;
//	}
//}

function onClickImportMode(obj){
	var importMode = obj.value;
	switch (importMode){
		case "new":
			//todo when new is click
			$('.update').hide();
			$('.new').show();
		break;
		case "update":
			//todo when update is click
			$('.update').show();
			$('.new').hide();
		break;
	}
}
</script>

<br />
<form name="form1"  action="import_result.php"  method="post" onSubmit="return checkform(this);"  enctype="multipart/form-data">
	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($msg, $xmsg);?></td>
		</tr>
		<tr>
			<td class="navigation">
				<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr><td><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?></td></tr>
				</table>
			</td>
		</tr>
	</table>
	
	<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr>
			<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
		</tr>
		<tr>
			<td>
				<table class="form_table_v30">	
					<tr>
						 <td width="100%" valign="top" nowrap="nowrap" class="field_title"><?=$linterface->RequiredSymbol().$Lang['General']['SourceFile']." <span class='tabletextremark'>".$Lang['General']['CSVFileFormat']."</span>"?>: </td>
					  	 <td class="tabletext"><input class="file" type="file" name="csvfile"></td>
				  	</tr>		
					
					<tr>
						<td class="field_title" align="left"><?=$Lang['eSurvey']['Import']['Mode']?></td>
						<td class="tabletext"><?= $radioButtons ?></td>
					</tr>
				
					<tr class="new">
						<td class="field_title" align="left" ><?= $Lang['General']['CSVSample'].' ('.$Lang['eSurvey']['Import']['ImportNew'].')' ?></td>
						<td class="tabletext"><?=$csvFile?></td> 
					</tr>
					<tr style="display:none" class="update">
						<td class="field_title" align="left" ><?=$Lang['General']['CSVSample'].' ('.$Lang['eSurvey']['Import']['UpdateExisting'].')' ?></td>
						<td class="tabletext"><?=$csvFileUpdate?></td>
					</tr>
					
					<tr class="new">
						<td class="field_title" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?></td>
						<td class="tabletext"><?=$csv_format?></td>
					</tr>
					<tr class="update" style="display:none">
						<td class="field_title" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?></td>
						<td class="tabletext"><?=$csv_format_update?></td>
					</tr>
		      	</table>
			</td> 
		</tr>
	</table>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td>
				<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>
					<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
					<tr>
						<td align="center">
							<?= $linterface->GET_ACTION_BTN($button_continue, "submit")?>&nbsp;
							<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:back()")?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

<input type="hidden" name="CurrentTemplateIndex" value="0">
</form>

<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>