<?php
# using: Paul

################## Change Log ##
#	Date:	2017-11-07 (Paul)
#			- get group list for subject group if L&T
#
#	Date:	2015-03-25	Charles Ma
#			Add noapprostat_lang
#
#	Date:	2011-02-24	YatWoon
#			Add "Display question number"
#
#	Date:	2011-02-08	YatWoon
#			Wish List Item: Display "Unfill User List" even the survery is "Anonymous"
#
################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
include_once($PATH_WRT_ROOT."includes/libsurvey.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$MODULE_OBJ['title'] = $Lang['eSurvey']['ViewSurveyResult'];
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$SurveyID = (is_array($SurveyID)? $SurveyID[0]:$SurveyID);
$SurveyID = IntegerSafe($SurveyID);
$lsurvey = new libsurvey($SurveyID);

$queString = $lsurvey->Question;
$queString = str_replace('"','&quot;',$queString);
$queString = str_replace("\n",'<br>',$queString);
$queString = str_replace("\r",'',$queString);

$poster = $lsurvey->returnPosterName();
$targetGroups = $lsurvey->returnTargetGroups();
$targetSubjectGroups = $lsurvey->returnTargetSubjectGroups();

if (sizeof($targetGroups)==0 && sizeof($targetSubjectGroups)==0)
{
    $target = $i_general_WholeSchool;
}
else
{
    $target = implode(", ",array_merge($targetGroups,$targetSubjectGroups));
}

$answer = $lsurvey->returnAnswer();

$survey_description = nl2br(intranet_convertAllLinks($lsurvey->Description));
$survey_description = $survey_description ? $survey_description : "---";

?>

<br />

<table width=95% cellspacing=0 cellpadding=5 border=0>
<tr>
	<td class="tabletext formfieldtitle" width="30%">
	<?=$i_general_startdate?></td><td><?=$lsurvey->DateStart?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle" width="30%"><?=$i_general_enddate?></td>
	<td><?=$lsurvey->DateEnd?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle" width="30%"><?=$i_general_title?></td>
	<td><?=$lsurvey->Title?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle" width="30%"><?=$i_general_description?></td>
	<td><?=$survey_description?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle" width="30%"><?=$i_Survey_Poster?></td>
	<td><?=$poster?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle" width="30%"><?=$i_general_TargetGroup?></td>
	<td><?=$target?></td>
</tr>

<? if ($lsurvey->RecordType == 1) { ?>
<tr>
	<td >&nbsp;</td>
	<td>[<?=$i_Survey_Anonymous?>]</td>
</tr>
<? } ?>

<tr>
<td colspan=2>
	<? if ($answer!="") { ?>
		<hr>
		<B><?=$i_Survey_Overall?></B><br>
		
		<a class=functionlink_new href=result_type.php?SurveyID=<?=$SurveyID?>>(<?=$i_Survey_Detail?>)</a>
		
		<?// if ($lsurvey->RecordType != 1) { ?>
		<a class=functionlink_new href=unfill.php?SurveyID=<?=$SurveyID?>>(<?=$i_Survey_UnfillList?>)</a>
		<?// } ?>
		
		<?=$linterface->GET_LNK_EXPORT("exportall.php?SurveyID=$SurveyID","","","","",0);?>
		
		<script language="javascript" src="/templates/forms/form_view.js"></script>
		
		<form name="ansForm" method="post" action="update.php">
		        <input type=hidden name="qStr" value="">
		        <input type=hidden name="aStr" value="">
		</form>
		
		<script language="Javascript">
		<?=returnFormStatWords()?>
				
		var myQue = "<?=$queString?>";
		var myAns = new Array();
		<?=$answer?>
		var DisplayQuestionNumber = '<?=$lsurvey->DisplayQuestionNumber?>';
		var noapprostat_lang = '<?=$Lang['eSurvey']['NoApproStat']?>';
		var no_answer_lang = '<?=$Lang['eSurvey']['NotAnswered']?>';
		
		var stats= new Statistics();
		stats.qString = myQue;
		stats.answer = myAns;
		stats.analysisData();
		document.write(stats.getStats());
		</SCRIPT>
	<? }
	else
	{
		echo $i_Survey_NoSurveyForCriteria;
	}
	?>
	<hr>
	</td>
</tr>
<tr>
	<td colspan="2" align="center">
		<?=$linterface->GET_BTN($button_print, "button", "window.print();") ?>
		<?=$linterface->GET_BTN($button_close, "button", "window.close();") ?>
	</td>
</tr>
</table>


<?php

intranet_closedb();
$linterface->LAYOUT_STOP();
?>