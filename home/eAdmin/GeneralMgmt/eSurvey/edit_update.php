<?php

#################################
#
#   Date:   2019-05-17 (Bill)
#           Change hidden input field name from "OriginalGroupID" to "OriginalGroupCode"
#
#   Date: 2019-05-06 (Bill)
#           - prevent SQL Injection + Cross-site Scripting
#           - fixed PHP error message display   [2019-0502-1054-53207]
#
#	Date: 2017-11-07 (Paul)
#			- added redirection for L&T
#
#	Date: 2017-06-13 (Anna)
#			- added title after $mailSubject
#
#	Date: 2015-08-18 (Omas) #Q80431 
#		  - support edit target group after there are submission
#		  - clear removed group answered survey
#
#	Date:	2014-04-02	YatWoon
#			Fixed: failed to retrieve GroupID if edit survey without editable rights [Case#C60646]
#
#	Date:	2013-04-19	Yuen
#			support notification by email
#
#	Date:	2011-02-23	YatWoon
#			Add "Display question number"
#
#################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libemail.php");
include_once($PATH_WRT_ROOT."includes/libsendmail.php");
include_once($PATH_WRT_ROOT."includes/libsurvey.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$SurveyID = IntegerSafe($SurveyID);
if(isset($GroupID)) {
    $GroupID = IntegerSafe($GroupID);
}

$RecordStatus = IntegerSafe($RecordStatus);
$isAnonymous = IntegerSafe($isAnonymous);
### Handle SQL Injection + XSS [END]

$li = new libsurvey($SurveyID);
$editable = $li->isSurveyEditable();

$lu = new libuser($UserID);
$ligroup = new libgrouping();

$Title = intranet_htmlspecialchars(trim($Title));
$Description = intranet_htmlspecialchars(trim($Description));
$StartDate = intranet_htmlspecialchars(trim($StartDate));
$EndDate = intranet_htmlspecialchars(trim($EndDate));

$allowDraftSetting = false;
if ($sys_custom['SurveySaveWithDraft']) {
	$allowDraftSetting = true;
}
if ($allowDraftSetting) {
	$hasDraft = $li->isSurveyFilledDraft();
	$AllowSaveDraft = ($AllowSaveDraft == 1) ? $AllowSaveDraft : 0;
}

$qStr2 = $qStr;
$qStr = intranet_htmlspecialchars(trim($qStr));
$AllFieldsReq = $AllFieldsReq==1 ? 1 : 0;
$DisplayQuestionNumber = $DisplayQuestionNumber==1 ? 1 : 0;

if (sizeof($GroupID) != 0)
{
	$GroupID = array_unique($GroupID);
	$GroupID = array_values($GroupID);
}

$startStamp = strtotime($StartDate);
$endStamp = strtotime($EndDate);
if (compareDate($startStamp, $endStamp) > 0 || !intranet_validateDate($StartDate) || !intranet_validateDate($EndDate))   // start > end
{
	$valid = false;
}
else
{
	$valid = true;
}

if ($valid)
{
	$username = $lu->getNameForRecord();
    
	$fieldname = "";
	$fieldname .= "Title = '$Title', ";
	$fieldname .= "Description = '$Description', ";
	$fieldname .= "DateStart = '$StartDate', ";
	$fieldname .= "DateEnd = '$EndDate',";
	$fieldname .= "RecordStatus = '$RecordStatus', ";
	$fieldname .= "PosterID = '$UserID',";
	$fieldname .= "PosterName = '$username',";
    
	if ($allowDraftSetting && !$hasDraft) {
		$fieldname .= " AllowSaveDraft = '$AllowSaveDraft',";
	}
	
	if ($editable)
	{
		$fieldname .= "Question = '$qStr',";
		$fieldname .= "RecordType = '$isAnonymous',";
		$fieldname .= "AllFieldsReq = '$AllFieldsReq',";
		$fieldname .= "DisplayQuestionNumber = '$DisplayQuestionNumber',";
	}
	$fieldname .= "DateModified = now()";
    
	$sql = "UPDATE INTRANET_SURVEY SET $fieldname WHERE SurveyID = '$SurveyID'";
	$li->db_db_query($sql);
    
	//if ($editable)       # Change group target
	if (true)
	{
		# Remove all group first
		$sql = "DELETE FROM INTRANET_GROUPSURVEY WHERE SurveyID = '$SurveyID'";
		$li->db_db_query($sql);

		if (sizeof($GroupID) != 0)
		{
			$sql = "INSERT INTO INTRANET_GROUPSURVEY (GroupID, SurveyID) VALUES ";
			$delimiter = "";
			for ($i=0; $i<sizeof($GroupID); $i++)
			{
				$sql .= "$delimiter ('".$GroupID[$i]."', '$SurveyID')";
				$delimiter = ",";
			}
			$li->db_db_query($sql);
		}
	}
	else       # Retrieve original target group
	{
		$GroupID = $li->returnTargetGroupIDs();
	}
	
	### Clear removed group answer
	if(!$_POST['publicdisplay'])
	{
	    $OriginalGroupIDArr = unserialize(urldecode($_POST['OriginalGroupCode']));
	    // $needClearGroupIDArr  = array_diff($OriginalGroupIDArr,array_intersect((array)$_POST['GroupID'],$OriginalGroupIDArr));
	    
	    $OriginalGroupIDRemainArr = array_intersect((array)$GroupID, (array)$OriginalGroupIDArr);
		$needClearGroupIDArr = array_diff((array)$OriginalGroupIDArr, (array)$OriginalGroupIDRemainArr);
		if(!empty($needClearGroupIDArr))
		{
			$sql = "SELECT UserID FROM INTRANET_USERGROUP WHERE GroupID IN ('".implode("','",(array)$needClearGroupIDArr)."')";
			$UserIDArr = $li->returnVector($sql);
			if(count($UserIDArr) > 0 && $SurveyID > 0) {
			    $sql = "DELETE FROM INTRANET_SURVEYANSWER WHERE UserID IN ('".implode("','",(array)$UserIDArr)."') AND SurveyID = '".IntegerSafe($SurveyID)."' ";
				$li->db_db_query($sql);
			}
		}
	}
	
	# Call send mail function
	if($email_alert == 1)
	{
		$type = (sizeof($GroupID)==0? $Lang['EmailNotification']['SchoolNews']['School']: $Lang['EmailNotification']['SchoolNews']['Group']);
		
		if (sizeof($GroupID) > 0)
		{
			$Groups = $ligroup->returnGroupNames($GroupID);
		}
		list($mailSubject, $mailBody) = $li->returnEmailNotificationData($StartDate, $Title, $type, $Groups);
		
		$lwebmail = new libwebmail();
		if(sizeof($GroupID) == 0)
		{
			$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordStatus = '1'";
		}
		else
		{
			$sql = "SELECT UserID FROM INTRANET_USERGROUP WHERE GroupID IN (".implode(",",$GroupID).")";
		}
		$ToArray = $li->returnVector($sql);
		
		$lwebmail->sendModuleMail($ToArray,$mailSubject.' - '.$Title,$mailBody);
	}
	
	if($sys_custom['iPf']['learnAndTeachReport'] && $QuestionType == 'LT') {
		header ("Location: index.php?filter=$RecordStatus&xmsg=UpdateSuccess&QuestionType=LT");
	}
	else {
		header ("Location: index.php?filter=$RecordStatus&xmsg=UpdateSuccess");
	}
}
else
{
 	$qStr2 = str_replace("&","_AND_",trim($qStr2));
	$qStr2 = str_replace("#","___",trim($qStr2));
	if($sys_custom['iPf']['learnAndTeachReport'] && $QuestionType == 'LT') {
		header ("Location: edit.php?SurveyID=$SurveyID&xmsg=UpdateUnsuccess&QuestionType=LT");
	}
	else {
		header ("Location: edit.php?SurveyID=$SurveyID&xmsg=UpdateUnsuccess");
	}	
}

intranet_closedb();
?>