<?php

# using: yat

################# Change Log [Start] ############
# 
#	Date:	2011-06-02	YatWoon
#			add option allow re-sign the survey or not ($lsurvey->NotAllowReSign)
#
################# Change Log [End] ############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libsurvey.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

$lsurvey = new libsurvey();
$lgeneralsettings = new libgeneralsettings();

$data = array();
$data['MaxReplySlipOption'] = $MaxReplySlipOption;
$data['NotAllowReSign'] = $NotAllowReSign;

if ($sys_custom['SurveySaveWithDraft']) {
	$data['DefaultAllowSaveDraft'] = $DefaultAllowSaveDraft;
}

# store in DB
$lgeneralsettings->Save_General_Setting($lsurvey->Module, $data);

# set $this->
$lsurvey->MaxReplySlipOption = $MaxReplySlipOption;		
$lsurvey->NotAllowReSign = $NotAllowReSign;
if ($sys_custom['SurveySaveWithDraft']) {
	$lsurvey->DefaultAllowSaveDraft= $DefaultAllowSaveDraft;
}

# update session
foreach($data as $name=>$val)
	$_SESSION["SSV_PRIVILEGE"]["eSurvey"][$name] = $val;

intranet_closedb();
header("Location: index.php?xmsg=UpdateSuccess");
?>