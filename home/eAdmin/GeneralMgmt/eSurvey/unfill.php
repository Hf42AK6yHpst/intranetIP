<?php
# using: Paul

################## Change Log ##
#	Date:	2017-12-13	Paul
#			- Improved: Handle Subject Group based survey
#
#	Date:	2011-12-01	YatWoon
#			- Improved: In unanswered list, another table to display student info for no class name and class numebr. [Case#2011-1017-1050-38073]
#
#	Date:	2011-02-08	YatWoon
#			- Wish List Item: Display "Unfill User List" even the survery is "Anonymous"
#			- UI revised
#
################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
include_once($PATH_WRT_ROOT."includes/libsurvey.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$MODULE_OBJ['title'] = $Lang['eSurvey']['ViewSurveyResult'];
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$SurveyID = (is_array($SurveyID)? $SurveyID[0]:$SurveyID);
$SurveyID = IntegerSafe($SurveyID);
$lsurvey = new libsurvey($SurveyID);

# Wish List Item: Display "Unfill User List" even the survery is "Anonymous"
/*
if ($lsurvey->RecordType == 1)
{
    header("Location: result.php?SurveyID=$SurveyID");
    exit();
}
*/

     $poster = $lsurvey->returnPosterName();
     $targetGroups = $lsurvey->returnTargetGroups();
	 $targetSubjectGroups = $lsurvey->returnTargetSubjectGroups();
     
     if (sizeof($targetGroups)==0 && sizeof($targetSubjectGroups)==0)
     {
         $target = "$i_general_WholeSchool";
     }
     else
     {
         $target = implode(", ",array_merge($targetGroups,$targetSubjectGroups));
     }

     $targetGroupArray = $lsurvey->returnTargetGroupsArray();
     $targetSubjectGroupArray = $lsurvey->returnTargetSubjectGroupsArray();
     
     if ($sys_custom['SurveySaveWithDraft']) {
     	$user_cond = " (c.UserID IS NULL OR c.isDraft='1') ";
     } else {
     	$user_cond = " c.UserID IS NULL ";
     }
     $user_cond = " c.UserID IS NULL ";
     # Group-based
     if (sizeof($targetGroupArray)!=0)
     {
         $delim = "";
         $group_list = "";
         for ($i=0; $i<sizeof($targetGroupArray); $i++)
         {
              list($id,$title) = $targetGroupArray[$i];
              $group_list .= "$delim $id";
              $delim = ",";
         }
         $name_field = getNameFieldByLang("a.");
         
         
         $sql = "SELECT a.UserID, a.ClassName, a.ClassNumber, $name_field as this_name, 
         if(a.RecordType != 2, a.RecordType , if((a.ClassName ='' or a.ClassName is null), '2b' , '2a')) as RecordType
         FROM INTRANET_USERGROUP as b
                 LEFT OUTER JOIN INTRANET_USER as a ON a.UserID = b.UserID
                 LEFT OUTER JOIN INTRANET_SURVEYANSWER as c ON a.UserID = c.UserID AND c.SurveyID = $SurveyID
                 WHERE 
                 $user_cond AND b.GroupID IN ($group_list)
                 /*
                 and 
                 if(a.RecordType=2, a.ClassName is not null and a.ClassNumber is not null, 1)
                 */
                 ORDER BY a.ClassName, a.ClassNumber, a.EnglishName";
     }
     else if(sizeof($targetSubjectGroupArray)!=0) # Subject Group Based
     {
     	$delim = "";
        $group_list = "";
        for ($i=0; $i<sizeof($targetSubjectGroupArray); $i++)
        {
             list($id,$title) = $targetSubjectGroupArray[$i];
             $group_list .= "$delim $id";
             $delim = ",";
        }
        $name_field = getNameFieldByLang("u.");
         
         
        $sql = "SELECT u.UserID, u.ClassName, u.ClassNumber, $name_field as this_name, 
		        if(u.RecordType != 2, u.RecordType , if((u.ClassName ='' or u.ClassName is null), '2b' , '2a')) as RecordType
		        FROM INTRANET_SUBJECTGROUPSURVEY sgs
				INNER JOIN SUBJECT_TERM_CLASS_USER stcu ON sgs.SubjectGroupID=stcu.SubjectGroupID
				INNER JOIN INTRANET_USER u ON stcu.UserID=u.UserID
				LEFT OUTER JOIN INTRANET_SURVEYANSWER c ON c.UserID=u.UserID
		        WHERE 
		        $user_cond AND sgs.SubjectGroupID IN ($group_list) AND sgs.SurveyID=$SurveyID
		        ORDER BY u.ClassName, u.ClassNumber, u.EnglishName";
     }
     else  # School-based
     {
	     
         $name_field = getNameFieldByLang("a.");
         
         $sql = "SELECT a.UserID, a.ClassName, a.ClassNumber, $name_field as this_name, 
         		if(a.RecordType != 2, a.RecordType , if((a.ClassName ='' or a.ClassName is null), '2b' , '2a')) as RecordType
         		
         		FROM INTRANET_USER as a
                 LEFT OUTER JOIN INTRANET_SURVEYANSWER as c ON a.UserID = c.UserID AND c.SurveyID = $SurveyID
                 WHERE 
                 $user_cond
                 and a.RecordStatus in (1,2)
                 /* 
                 and 
                 if(a.RecordType=2, a.ClassName is not null and a.ClassNumber is not null, 1) 
                 */
                 ORDER BY a.ClassName, a.ClassNumber, a.EnglishName";
     }
     $result = $lsurvey->returnArray($sql);
     
     ## build result array
     $result_array = array();
     if(sizeof($result)>0)
     {
	     foreach($result as $k=>$d)
	     {
		     $result_array[$d['RecordType']][] = $d;
	     }
     }

     $display = ""; 
     
     ##### Staff unfill list
     if(sizeof($result_array[1]))
     {
	     $display .= $linterface->GET_NAVIGATION2($Lang['Identity']['TeachingStaff'] . " / " . $Lang['Identity']['NonTeachingStaff']) . "<br>";
	     
	     $display .= "<table class='common_table_list_v30 view_table_list_v30'>";
	     $display .= "<tr><th>". $i_general_name ."</th></tr>";
	     
	     foreach($result_array[1] as $k=>$d)
	     {
		     $display .= "<tr><td>". $d['this_name'] ."</td></tr>";
	     }
	     
	     $display .= "</table>";
	     $display .= "<br>";
     }
     
     ##### Student unfill list (with classes)
     if(sizeof($result_array['2a']))
     {
	     $display .= $linterface->GET_NAVIGATION2($Lang['Identity']['Student'] ) . "<br>";
	     
	     $display .= "<table class='common_table_list_v30 view_table_list_v30'>";
	     $display .= "<tr>
	     				<th nowrap>". $i_ClassName ."</th>
	     				<th nowrap>". $i_ClassNumber ."</th>
	     				<th width='100%'>". $i_UserStudentName ."</th>
	     			</tr>";
	     
	     foreach($result_array['2a'] as $k=>$d)
	     {
		     $display .= "<tr>
		     				<td>". $d['ClassName'] ."</td>
		     				<td>". $d['ClassNumber'] ."</td>
		     				<td>". $d['this_name'] ."</td>
		     			</tr>";
	     }
	     
	     $display .= "</table>";
	     $display .= "<br>";
     }
     
     ##### Student unfill list (without classes)
     if(sizeof($result_array['2b']))
     {
	     $display .= $linterface->GET_NAVIGATION2($Lang['AccountMgmt']['StudentNotInClass'] ) . "<br>";
	     
	     $display .= "<table class='common_table_list_v30 view_table_list_v30'>";
	     $display .= "<tr>
	     				<th width='100%'>". $i_UserStudentName ."</th>
	     			</tr>";
	     
	     foreach($result_array['2b'] as $k=>$d)
	     {
		     $display .= "<tr>
		     				<td>". $d['this_name'] ."</td>
		     			</tr>";
	     }
	     
	     $display .= "</table>";
	     $display .= "<br>";
     }
     
     ##### Parent unfill list
     if(sizeof($result_array[3]))
     {
	     $display .= $linterface->GET_NAVIGATION2($Lang['Identity']['Parent'] ) . "<br>";
	     
	     $display .= "<table class='common_table_list_v30 view_table_list_v30'>";
	     $display .= "<tr><th>". $i_general_name ."</th></tr>";
	     
	     foreach($result_array[3] as $k=>$d)
	     {
		     $display .= "<tr><td>". $d['this_name'] ."</td></tr>";
	     }
	     
	     $display .= "</table>";
	     $display .= "<br>";
     }
     
$survey_description = nl2br(intranet_convertAllLinks($lsurvey->Description));
$survey_description = $survey_description ? $survey_description : "---";
?>

<table class="form_table_v30">
<tr><td class="field_title"><?=$i_general_startdate?></td><td><?=$lsurvey->DateStart?></td></tr>
<tr><td class="field_title"><?=$i_general_enddate?></td><td><?=$lsurvey->DateEnd?></td></tr>
<tr><td class="field_title"><?=$i_general_title?></td><td><?=$lsurvey->Title?></td></tr>
<tr><td class="field_title"><?=$i_general_description?></td><td><?=$survey_description?></td></tr>
<tr><td class="field_title"><?=$i_Survey_Poster?></td><td><?=$poster?></td></tr>
<tr><td class="field_title"><?=$i_general_TargetGroup?></td><td><?=$target?></td></tr>
<tr><td colspan=2>
	<? if (sizeof($result)!=0) { ?>
		<hr>
		<B><?=$i_Survey_UnfillList?></B><br>
		<a class=functionlink_new href=result.php?SurveyID=<?=$SurveyID?>>(<?=$i_Survey_Overall?>)</a>
		<br><br>
		<?=$display?>
	<? }
	else
	{
		echo $i_Survey_AllFilled;
	}
	?>
</td>
</tr>
</table>

<div class="edit_bottom_v30">
<p class="spacer"></p>
	<?=$linterface->GET_BTN($button_print, "button", "window.print();") ?>
	<?=$linterface->GET_BTN($button_close, "button", "window.close();") ?><p class="spacer"></p>
</div>


<?

intranet_closedb();
$linterface->LAYOUT_STOP();
?>