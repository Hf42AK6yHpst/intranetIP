<?php
# using:

#################################
#
#   Date:   2020-08-24  Bill    [2020-0820-1452-25235]
#           set convertQuoteInOpts = true, to convert quotes in options only
#           (previous logic : convert quotes in whole qString)
#
#	Date:	2015-12-16	Pun
#			add js lang num_choice for Ordering
#
#	Date:	2015-03-23	Jason
#			add new global js variable enableOrderingQuestion to support new type - Ordering
#
#	Date:	2011-02-24	YatWoon
#			add option "Max. Reply Slip Option"
#
#	Date:	2011-02-23	YatWoon
#			Add "Display question number"
#
#################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libsurvey.php");

intranet_auth();
intranet_opendb();

$lf = new libform();
$ls = new libsurvey();
    
$MODULE_OBJ['title'] = $Lang['eSurvey']['SyrveyConstruction'];
$home_header_no_EmulateIE7 = true;

$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();
?>

<script language="javascript" src="/templates/forms/form_edit.js"></script>
<script language="javascript" src="/templates/forms/layer.js"></script>

<br />
<form name="ansForm" action="">
<input type="hidden" name="qStr" value="">
<input type="hidden" name="aStr" value="">

<script language="Javascript">
var enableOrderingQuestion = true;

// Grab values from
s = new String(window.opener.document.form1.qStr.value);
//s = s.replace(/"/g, '&quot;');
document.ansForm.qStr.value = s;
document.ansForm.aStr.value = window.opener.document.form1.aStr.value;

function copyback()
{
     finish();
     window.opener.document.form1.qStr.value = document.ansForm.qStr.value;
     window.opener.document.form1.aStr.value = document.ansForm.aStr.value;
     self.close();
}

<?=$lf->getWordsInJS()?>
var form_templates = new Array();     // template not in this moment
<?=$ls->getTemplatesInJS()?>

var choice_no = 100;
var answer_sheet = '<?=$i_Notice_ReplyContent?>';
var space10 = '<img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" />';
var add_btn = '<?=str_replace("'", "\'",$linterface->GET_BTN($button_add, 'button', 'retainValues(); if (appendTxt(this.form.name)) {writetolayer(\'blockInput\',sheet.writeSheet()); this.form.reset(); this.form.secDesc.focus();}','submit2'))?>';
var order_name = '<?=$i_Sports_Order?>';
<? $PAGE_NAVIGATION1[] = array($Lang['eSurvey']['Question']); ?>
var Part1 = '<?=str_replace("'","\'",$linterface->GET_NAVIGATION($PAGE_NAVIGATION1))?>'
<? $PAGE_NAVIGATION2[] = array($i_general_DisplayOrder); ?>
var Part2 = '<?=str_replace("'","\'",$linterface->GET_NAVIGATION($PAGE_NAVIGATION2))?>'
<? $PAGE_NAVIGATION3[] = array($Lang['eSurvey']['ReplySlipQuestions']); ?>
var Part3 = '<?=str_replace("'","\'",$linterface->GET_NAVIGATION($PAGE_NAVIGATION3))?>'
var replyslip = '<?=$Lang['eSurvey']['ReplySlip']?>';
var MoveUpBtn = '<?=$Lang['Button']['MoveUp']?>';
var MoveDownBtn = '<?=$Lang['Button']['MoveDown']?>';
var DeleteBtn = '<?=$Lang['Button']['Delete']?>';
var EditBtn = '<?=$Lang['Button']['Edit']?>';
var order_lang = '<?=$Lang['eSurvey']['Order']?>';
var choice_lang = '<?=$Lang['Polling']['Choice']?>';
var hint_lang = '<?=$Lang['Polling']['Hint']?>';
var no_answer_lang = '<?=$Lang['eSurvey']['NotAnswered']?>';
var num_choice = '<?=$Lang['eSurvey']['OrderingTypeNumOfAns'] ?>';
var DisplayQuestionNumber = 0;
if(window.opener.document.form1.DisplayQuestionNumber.checked) {
    DisplayQuestionNumber = 1;
}
background_image = "";

var MaxOptionNo = "<?=$ls->MaxReplySlipOption?>";
var sheet= new Answersheet();
// attention: MUST replace '"' to '&quot;'
sheet.qString = document.ansForm.qStr.value;
sheet.mode = 0;        // 0:edit 1:fill in application
sheet.convertQuoteInOpts = true;    // convert quotes in options only

sheet.answer = sheet.sheetArr();
sheet.templates = form_templates;
document.write(editPanel());
</script>

<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
</tr>
<tr>
	<td align="center">
        <?= $linterface->GET_ACTION_BTN($button_save, "submit", "copyback(); return false;","submit2") ?>
        <?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();","cancelbtn") ?>
	</td>
</tr>
</table>                   

</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>