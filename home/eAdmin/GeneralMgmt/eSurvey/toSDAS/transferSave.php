<?php
//Using: Pun
/**
 * Change Log:
 * 2017-05-24 Pun
 *  - File create
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if (!$sys_custom['iPf']['learnAndTeachReport'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/libsurvey.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/cust/student_data_analysis_system/libSDAS.php");
include_once($PATH_WRT_ROOT."includes/SDAS/customReport/learnAndTeach/learnAndTeach.php");

intranet_auth();
intranet_opendb();

$objDB = new libdb();
$linterface = new interface_html();
$lsurvey = new libsurvey();
$sdas = new libSDAS();
$year = new Year();
$lc = new Learning_Category();

$passCount = 0;
$result = array();
$sqlArr = array();

######## Helper function START ########
function getNumberInString($str){
    return preg_replace('/[^\d]/', '', $str);
}
######## Helper function END ########


######## Get Data START ########

#### Get Year START ####
$rs = $year->Get_All_Year_List();
$allYear = array();
foreach($rs as $r){
    $form = getNumberInString($r['YearName']);
    if($form){
        $allYear[ $form ] = $r['YearID'];
    }
}
#### Get Year END ####

#### Get Class START ####
$allClass = $year->Get_All_Classes($CheckClassTeacher=0, $AcademicYearID);
$allClassUpperDetails = array();
foreach($allClass as $r){
    $allClassUpperDetails[ strtoupper($r['ClassTitleEN']) ] = $r;
    $allClassUpperDetails[ strtoupper($r['ClassTitleB5']) ] = $r;
}
#### Get Class END ####

#### Get Learning Category START ####
$sql = "SELECT
    RecordID,
    LearningCategoryID
FROM
    ASSESSMENT_SUBJECT";
$rs = $objDB->returnResultSet($sql);
$allLearningCategory = BuildMultiKeyAssoc($rs, array('RecordID') , array('LearningCategoryID'), $SingleValue=1);
#### Get Learning Category END ####

#### Get Subject START ####
$sql = "SELECT
    RecordID,
    EN_ABBR,
    CH_ABBR
FROM
    ASSESSMENT_SUBJECT
WHERE
    RecordStatus = 1
AND
    (
        CMP_CODEID = ''
    OR
        CMP_CODEID IS NULL
    )
";
$allSubject = $objDB->returnResultSet($sql);
$allSubjectAbbrId = array();
foreach($allSubject as $subj){
    $allSubjectAbbrId[ strtoupper($subj['EN_ABBR']) ] = $subj['RecordID'];
    $allSubjectAbbrId[ strtoupper($subj['CH_ABBR']) ] = $subj['RecordID'];
}
#### Get Subject END ####

#### Get Subject Teacher START ####
$sql = "SELECT
    STCT.UserID,
    ST.SubjectID
FROM
    SUBJECT_TERM_CLASS_TEACHER STCT
INNER JOIN
    SUBJECT_TERM_CLASS STC
ON
    STCT.SubjectGroupID = STC.SubjectGroupID
INNER JOIN
    SUBJECT_TERM ST
ON
    STC.SubjectGroupID = ST.SubjectGroupID
INNER JOIN
    ACADEMIC_YEAR_TERM AYT
ON
    ST.YearTermID = AYT.YearTermID
AND
    AYT.AcademicYearID = '{$AcademicYearID}'
";
$rs = $objDB->returnResultSet($sql);
$allTeacherId = Get_Array_By_Key($rs, 'UserID');
$allTeacherIdSql = implode("','", $allTeacherId);

$sql = "SELECT UserID,ChineseName,EnglishName FROM INTRANET_USER WHERE UserID IN ('{$allTeacherIdSql}')";
$rs1 = $objDB->returnArray($sql);
$sql = "SELECT UserID,ChineseName,EnglishName FROM INTRANET_ARCHIVE_USER WHERE UserID IN ('{$allTeacherIdSql}')";
$rs2 = $objDB->returnArray($sql);
$teachers = array_merge($rs1, $rs2);

$allTeacherNameId = array();
foreach($teachers as $teacher){
    $allTeacherNameId[ strtoupper($teacher['EnglishName']) ] = $teacher['UserID'];
    $allTeacherNameId[ strtoupper($teacher['ChineseName']) ] = $teacher['UserID'];
}
#### Get Subject Teacher END ####



#### Get all survey START ####
$surveyIdSql = implode("','", $surveyID);
$sql = "SELECT
    *
FROM
    INTRANET_SURVEY
WHERE
    SurveyID IN ('{$surveyIdSql}')
ORDER BY
    DateStart, Title
";
$allSurvey = $objDB->returnResultSet($sql);

$surveyYearId = array();
$surveyYearClassId = array();
$surveySpecialClassName = array();
$surveySubjectId = array();
$surveyTeacherId = array();
foreach($allSurvey as $survey){
    $surveyId = $survey['SurveyID'];
    $title = $survey['Title'];
    list($className, $subjectAbbr, $teacherName) = explode('-', $title);
    
    $classDetail = $allClassUpperDetails[ strtoupper($className) ];
    if($classDetail){
        
        $surveyYearId[$surveyId] = $classDetail['YearID'];
        $surveyYearClassId[$surveyId] = $classDetail['YearClassID'];
        $surveySpecialClassName[$surveyId] = '';
    }else{
        $surveyYearId[$surveyId] = 0;
        $surveyYearClassId[$surveyId] = 0;
        $surveySpecialClassName[$surveyId] = $className;
    }
    
    $surveySubjectId[$surveyId] = $allSubjectAbbrId[ strtoupper($subjectAbbr) ];
    
    foreach($allTeacherNameId as $tName => $tid){
        if(strpos($tName, strtoupper($teacherName)) !== false){
            $surveyTeacherId[$surveyId] = $tid;
            break;
        }
    }
}
#### Get all survey END ####

#### Get Survey Question START ####
$allQuestion = array();
if(count($allSurvey)){
    $lsurvey->splitQuestion($allSurvey[0]['Question']);
    $allQuestion = $lsurvey->question_array;
}
#### Get Survey Question END ####

#### Get all answer START ####
function answerToScore($answer){
    switch ($answer){
        case '0':
            return 5;
        case '1':
            return 4;
        case '2':
            return 3;
        case '3':
            return 2;
        case '4':
            return 1;
        case '5':
            return 0;
    }
    return 0;
}

$sql = "SELECT
    SurveyID,
    Answer
FROM
    INTRANET_SURVEYANSWER
WHERE
    SurveyID IN ('{$surveyIdSql}')";
$rs = $objDB->returnResultSet($sql);

$allSurveyWeight = array();
$allAnswerDetail = array();
$allStdsAnsMarks = array();
foreach($rs as $r){
    $surveyId = $r['SurveyID'];
    $teacherID = $lsurvey->returnLTSurveyTeacher($surveyId);
    $answers = explode("#ANS#",$r['Answer']);
    array_shift($answers);
    if(!$allAnswerDetail[$surveyId]){
        $allAnswerDetail[$surveyId] = array();
        for($i=0,$iMax=count($allQuestion);$i<$iMax;$i++){
            $allAnswerDetail[$surveyId][$i] = array(
                'SUM' => 0,
                'COUNT' => 0
            );
        }
    }
    if(!$allStdsAnsMarks[$teacherID]){
    	$allStdsAnsMarks[$teacherID] = array(
    		'SUM' => 0,
    		'COUNT' => 0
    	);
    }
    for($i=0,$iMax=count($allQuestion);$i<$iMax;$i++){
        $allAnswerDetail[$surveyId][$i]['COUNT']++;
        $allAnswerDetail[$surveyId][$i]['SUM'] += answerToScore($answers[$i]);
        if($answers[$i]!="null"){
        	$allStdsAnsMarks[$teacherID]['COUNT']++;
        	$allStdsAnsMarks[$teacherID]['SUM']  += answerToScore($answers[$i]);
        }
    }
    
    if(!$allSurveyWeight[$surveyId]){
        $allSurveyWeight[$surveyId] = 0;
    }
    $allSurveyWeight[$surveyId]++;
}

foreach($allAnswerDetail as $surveyId => $d1){
    foreach($d1 as $questionIndex => $details){
    	if(!$details['COUNT'])continue;
        $allAnswerDetail[$surveyId][$questionIndex]['MEAN'] = ($details['SUM'] / $details['COUNT'])*($details['COUNT'] /  $allSurveyWeight[$surveyId]);
    }
}
#### Get all answer END ####
######## Get Data END ########


######## Calculate Data START ########

#### Calculate Teacher Average START ####
## Get Class-Subject-Teacher Average START ##
$allSurveyAverageStat = array();
foreach($allAnswerDetail as $surveyId => $questionMeans){
    $sum = 0;
    $count = count($d1);
    foreach($questionMeans as $questionMean){
        $sum += $questionMean['MEAN'];
    }

    $mean = $sum / $count;
    $allSurveyAverageStat[$surveyId] = $mean;
}
## Get Class-Subject-Teacher Average END ##

## Get teacher overall averate START ##
$allTeacherSurveyAverageStat = array();
foreach($allSurveyAverageStat as $surveyId => $mean){
    $teacherID = $surveyTeacherId[$surveyId];
    $allTeacherSurveyAverageStat[$teacherID][] = array(
        $mean,
        $allSurveyWeight[$surveyId]
    );
}

$allTeacherOverallAverage = array();
foreach($allTeacherSurveyAverageStat as $teacherID => $d1){
    $allTeacherOverallAverage[$teacherID] = $sdas->CalculateWeightedArithmeticMean($d1);
}
## Get teacher overall averate END ##

## Calculate overall teacher stat START ##
if(count($allTeacherOverallAverage)){
    $overallTeacherStat = array(
        'Min' => min($allTeacherOverallAverage),
        'Average' => array_sum($allTeacherOverallAverage) / count($allTeacherOverallAverage),
        'Max' => max($allTeacherOverallAverage),
        'SD' => $sdas->sd($allTeacherOverallAverage),
        'TotalCount' => count($allTeacherOverallAverage),
    );
}else{
    $overallTeacherStat = array(
        'Min' => null,
        'Average' => null,
        'Max' => null,
        'SD' => null,
        'TotalCount' => 0,
    );
}
## Calculate overall teacher stat END ##

## Calculate Overall Question-based Average START ##
$allTeacherSurveyQBasedAverageStat = array();
foreach($allStdsAnsMarks as $teacherID => $marks){
	$mean = $marks['SUM'] / $marks['COUNT'];
    $allTeacherSurveyQBasedAverageStat[$teacherID][] = array(
        $mean,
       	1
    );
}

$allQuestionTotalMeanArr = array();
foreach($allTeacherSurveyQBasedAverageStat as $teacherID => $d1){
    $allQuestionTotalMeanArr[$teacherID] = $sdas->CalculateWeightedArithmeticMean($d1);
}
## Get teacher overall averate END ##

## Calculate overall teacher stat START ##
if(count($allQuestionTotalMeanArr)){
    $overallQBasedTeacherStat = array(
        'Min' => min($allQuestionTotalMeanArr),
        'Average' => array_sum($allQuestionTotalMeanArr) / count($allQuestionTotalMeanArr),
        'Max' => max($allQuestionTotalMeanArr),
        'SD' => $sdas->sd($allQuestionTotalMeanArr),
        'TotalCount' => count($allQuestionTotalMeanArr),
    );
}else{
    $overallQBasedTeacherStat = array(
        'Min' => null,
        'Average' => null,
        'Max' => null,
        'SD' => null,
        'TotalCount' => 0,
    );
}
## Calculate Overall Question-based Average END ##
#### Calculate Teacher Average END ####


#### Calculate Subject Average START ####
## Get subject overall average START ##
$allSubjectSurveyAverageStat = array();
foreach($allSurveyAverageStat as $surveyId => $mean){
    $subjectID = $surveySubjectId[$surveyId];
    $allSubjectSurveyAverageStat[$subjectID][] = array(
        $mean,
        $allSurveyWeight[$surveyId]
    );
}

$allSubjectOverallAverage = array();
foreach($allSubjectSurveyAverageStat as $subjectID => $d1){
    $allSubjectOverallAverage[$subjectID] = $sdas->CalculateWeightedArithmeticMean($d1);
}
## Get subject overall averate END ##

## Calculate overall subject stat START ##
$allSubjectMeanArr = array();
foreach($allSurveyAverageStat as $surveyId => $mean){
    $subjectID = $surveySubjectId[$surveyId];
    $allSubjectMeanArr[$subjectID][] = $mean;
}

$overallSubjectStat = array();
if(count($allSubjectMeanArr)){
    foreach($allSubjectMeanArr as $subjectID=>$meanArr){
        $overallSubjectStat[$subjectID] = array(
            'Min' => min($meanArr),
            'Average' => $allSubjectOverallAverage[$subjectID],
            'Max' => max($meanArr),
            'SD' => (count($meanArr) == 1)?0:$sdas->sd($meanArr),
            'TotalCount' => count($meanArr),
        );
    }
}
## Calculate overall subject stat END ##

## Get learning category overall average START ##
$allLearningCategorySurveyAverageStat = array();
foreach($allSurveyAverageStat as $surveyId => $mean){
    $subjectID = $surveySubjectId[$surveyId];
    $learningCategoryID = $allLearningCategory[$subjectID];
    $allLearningCategorySurveyAverageStat[$learningCategoryID][] = array(
        $mean,
        $allSurveyWeight[$surveyId]
    );
}

$allLearningCategoryOverallAverage = array();
foreach($allLearningCategorySurveyAverageStat as $learningCategoryID => $d1){
    $allLearningCategoryOverallAverage[$learningCategoryID] = $sdas->CalculateWeightedArithmeticMean($d1);
}
## Get learning category overall averate END ##

## Calculate overall learning category stat START ##
$allLearningCategoryMeanArr = array();
foreach($allSurveyAverageStat as $surveyId => $mean){
    $subjectID = $surveySubjectId[$surveyId];
    $learningCategoryID = $allLearningCategory[$subjectID];
    $allLearningCategoryMeanArr[$learningCategoryID][] = $mean;
}

$overallLearningCategoryStat = array();
if(count($allLearningCategoryMeanArr)){
    foreach($allLearningCategoryMeanArr as $learningCategoryID=>$meanArr){
        $overallLearningCategoryStat[$learningCategoryID] = array(
            'Min' => min($meanArr),
            'Average' => $allLearningCategoryOverallAverage[$learningCategoryID],
            'Max' => max($meanArr),
            'SD' => (count($meanArr) == 1)?0:$sdas->sd($meanArr),
            'TotalCount' => count($meanArr),
        );
    }
}
## Calculate overall learning category stat END ##
#### Calculate Subject Average END ####

######## Calculate Data END ########

######## Save Data START ########
$objDB->Start_Trans();

#### Delete Original Data START ####
$sql = "SELECT RecordID FROM LNT_QUESTION WHERE AcademicYearID='{$AcademicYearID}'";
$oldQuestionId = $objDB->returnVector($sql);
$oldQuestionIdSql = implode("','", $oldQuestionId);

$sql = "DELETE FROM LNT_QUESTION WHERE AcademicYearID='{$AcademicYearID}'";
$result[] = $objDB->db_db_query($sql);
$sqlArr[] = $sql;
$deleteQuestionRow = $objDB->db_affected_rows();

$sql = "DELETE FROM LNT_RECORD WHERE QuestionID IN ('{$oldQuestionIdSql}')";
$result[] = $objDB->db_db_query($sql);
$sqlArr[] = $sql;
$deleteAnswerRow = $objDB->db_affected_rows();

$sql = "DELETE FROM LNT_AVERAGE_STAT WHERE AcademicYearID='{$AcademicYearID}'";
$result[] = $objDB->db_db_query($sql);
$sqlArr[] = $sql;
$deleteAverageStatRow = $objDB->db_affected_rows();

$sql = "DELETE FROM LNT_QBASED_AVERAGE_STAT WHERE AcademicYearID='{$AcademicYearID}'";
$result[] = $objDB->db_db_query($sql);
$sqlArr[] = $sql;
$deleteAverageStatRow = $objDB->db_affected_rows();

$sql = "DELETE FROM LNT_OVERALL_STAT WHERE AcademicYearID='{$AcademicYearID}'";
$result[] = $objDB->db_db_query($sql);
$sqlArr[] = $sql;
$deleteOverallStatRow = $objDB->db_affected_rows();
#### Delete Original Data END ####

#### Save Survey Question START ####
$allQuestionId = array();
$index = 1;
foreach($allQuestion as $question){
    $sql = "INSERT INTO LNT_QUESTION
    (
        `AcademicYearID`,
        `Title`,
        `Sequence`,
        `InputDate`,
        `Createdby`
    ) VALUES (
        '{$AcademicYearID}',
        '{$question[1]}',
        '{$index}',
        NOW(),
        '{$_SESSION['UserID']}'
    )";
    $result[] = $_result = $objDB->db_db_query($sql);
    $sqlArr[] = $sql;
    $index++;

    $allQuestionId[] = $objDB->db_insert_id();
}
#### Save Survey Question END ####

#### Save Survey Data START ####
$surveyAnswerData = array();
foreach($allSurvey as $survey){
    $surveyId = $survey['SurveyID'];
    $title = $survey['Title'];
    list($className, $subjectAbbr, $teacherName) = explode('-', $title);
    
    $classDetail = $allClassUpperDetails[ strtoupper($className) ];
    $yearClassID = $classDetail['YearClassID'];
    if($yearClassID){
        $yearId = $classDetail['YearID'];
        $specialClassName = '';
    }else{
        $yearId = $allYear[getNumberInString($className)];
        $specialClassName = $surveySpecialClassName[$surveyId];
    }
    
    $subjectID = $allSubjectAbbrId[ strtoupper($subjectAbbr) ];
    $learningCategoryID = $allLearningCategory[$subjectID];
    
    foreach($allTeacherNameId as $tName => $tid){
        if(strpos($tName, strtoupper($teacherName)) !== false){
            $teacherID = $tid;
            break;
        }
    }
    
    for($i=0,$iMax=count($allQuestionId);$i<$iMax;$i++){
        $scoreDetail = $allAnswerDetail[$surveyId][$i];
        $sql = "INSERT INTO 
            `LNT_RECORD` 
        (
            `SurveyID`, 
            `LearningCategoryID`, 
            `SubjectID`, 
            `YearID`, 
            `YearClassID`, 
            `SpecialClass`, 
            `TeacherID`, 
            `QuestionID`, 
            `TotalCount`, 
            `ScoreSum`, 
            `Mean`, 
            `InputDate`, 
            `Createdby`
        ) VALUES (
            '{$surveyId}',
            '{$learningCategoryID}',
            '{$subjectID}',
            '{$yearId}',
            '{$yearClassID}',
            '{$specialClassName}',
            '{$teacherID}',
            '{$allQuestionId[$i]}',
            '{$scoreDetail['COUNT']}',
            '{$scoreDetail['SUM']}',
            '{$scoreDetail['MEAN']}',
            NOW(),
            '{$_SESSION['UserID']}'
        );";
        $result[] = $objDB->db_db_query($sql);
        $sqlArr[] = $sql;
    }
}
#### Save Survey Data END ####

#### Save Transfer Log START ####
$resultLog = <<<LOG
Remove question row: {$deleteQuestionRow}
Remove answer row: {$deleteAnswerRow}
Import Survey fail ID: {$incorrectSurveyId}
LOG;
$sql = "INSERT INTO 
    `LNT_TRANSFER_LOG` 
(
	`UserID`, 
	`AcademicYearID`, 
	`SurveyStartDate`, 
	`SurveyEndDate`, 
	`TransferResult`, 
	`InputDate`
) VALUES (
    '{$_SESSION['UserID']}',
    '{$AcademicYearID}',
    '{$StartDate}',
    '{$EndDate}',
    '{$resultLog}',
    NOW()
);";
$result[] = $objDB->db_db_query($sql);
$sqlArr[] = $sql;
#### Save Transfer Log END ####

#### Save Record Average START ####
## Save Teacher Average Stat START ##
foreach($allAnswerDetail as $surveyId => $questionMeans){
    $subjectID = $surveySubjectId[$surveyId];
    $learningCategoryID = $allLearningCategory[$subjectID];
    $yearClassID = $surveyYearClassId[$surveyId];
    $specialClassName = $surveySpecialClassName[$surveyId];
    $teacherID = $surveyTeacherId[$surveyId];
    $weight = $allSurveyWeight[$surveyId];
    
    $sql = "INSERT INTO `LNT_AVERAGE_STAT` 
    (
        `AcademicYearID`, 
        `LearningCategoryID`, 
        `SubjectID`, 
        `YearClassID`, 
        `SpecialClass`, 
        `TeacherID`, 
        `Mean`, 
        `TotalCount`, 
        `InputDate`, 
        `CreatedBy`
    ) VALUES (
        '{$AcademicYearID}',
        '{$learningCategoryID}',
        '{$subjectID}',
        '{$yearClassID}',
        '{$specialClassName}',
        '{$teacherID}',
        '{$allSurveyAverageStat[$surveyId]}',
        '{$weight}',
        NOW(),
        '{$_SESSION['UserID']}'
    );";
    $result[] = $objDB->db_db_query($sql);
    $sqlArr[] = $sql;
}
## Save Teacher Average Stat END ##

## Save Teacher Overall Average Stat START ##
foreach($allStdsAnsMarks as $TeacherID=>$marks){
	$mean = $marks['SUM']/$marks['COUNT'];
	$sql = "INSERT INTO `LNT_QBASED_AVERAGE_STAT` 
    (
        `AcademicYearID`, 
        `TeacherID`, 
        `Mean`, 
        `TotalCount`, 
        `InputDate`, 
        `CreatedBy`
    ) VALUES (
        '{$AcademicYearID}',
        '{$TeacherID}',
        '{$mean}',
        '".$marks['COUNT']."',
        NOW(),
        '{$_SESSION['UserID']}'
    );";
    $result[] = $objDB->db_db_query($sql);
    $sqlArr[] = $sql;
}

$sql = "INSERT INTO `LNT_OVERALL_STAT` 
(
    `AcademicYearID`, 
    `Min`, 
    `Mean`, 
    `Max`, 
    `SD`, 
    `TotalCount`, 
    `Type`, 
    `LearningCategoryID`, 
    `SubjectID`, 
    `InputDate`, 
    `CreatedBy`
) VALUES (
    '{$AcademicYearID}',
    '{$overallQBasedTeacherStat['Min']}',
    '{$overallQBasedTeacherStat['Average']}',
    '{$overallQBasedTeacherStat['Max']}',
    '{$overallQBasedTeacherStat['SD']}',
    '{$overallQBasedTeacherStat['TotalCount']}',
    '" . learnAndTeach::OVERALL_STAT_TYPE_TQBASED . "',
    NULL,
    NULL,
    NOW(),
    '{$_SESSION['UserID']}'
);";
$result[] = $objDB->db_db_query($sql);
$sqlArr[] = $sql;
## Save Teacher Overall Average Stat END ##

## Save All Teacher Overall Stat START ##
$sql = "INSERT INTO `LNT_OVERALL_STAT` 
(
    `AcademicYearID`, 
    `Min`, 
    `Mean`, 
    `Max`, 
    `SD`, 
    `TotalCount`, 
    `Type`, 
    `LearningCategoryID`, 
    `SubjectID`, 
    `InputDate`, 
    `CreatedBy`
) VALUES (
    '{$AcademicYearID}',
    '{$overallTeacherStat['Min']}',
    '{$overallTeacherStat['Average']}',
    '{$overallTeacherStat['Max']}',
    '{$overallTeacherStat['SD']}',
    '{$overallTeacherStat['TotalCount']}',
    '" . learnAndTeach::OVERALL_STAT_TYPE_TEACHER . "',
    NULL,
    NULL,
    NOW(),
    '{$_SESSION['UserID']}'
);";
$result[] = $objDB->db_db_query($sql);
$sqlArr[] = $sql;
## Save All Teacher Overall Stat END ##

## Save All Subject Overall Stat START ##
foreach($overallSubjectStat as $subjectID => $stat){
    $learningCategoryID = $allLearningCategory[$subjectID];
    $sql = "INSERT INTO `LNT_OVERALL_STAT`
    (
        `AcademicYearID`,
        `Min`,
        `Mean`,
        `Max`,
        `SD`,
        `TotalCount`,
        `Type`,
        `LearningCategoryID`, 
        `SubjectID`,
        `InputDate`,
        `CreatedBy`
    ) VALUES (
        '{$AcademicYearID}',
        '{$stat['Min']}',
        '{$stat['Average']}',
        '{$stat['Max']}',
        '{$stat['SD']}',
        '{$stat['TotalCount']}',
        '" . learnAndTeach::OVERALL_STAT_TYPE_SUBJECT . "',
        '{$learningCategoryID}',
        '{$subjectID}',
        NOW(),
        '{$_SESSION['UserID']}'
    );";
    $result[] = $objDB->db_db_query($sql);
    $sqlArr[] = $sql;
}
## Save All Subject Overall Stat END ##

## Save All Learning Category Overall Stat START ##
foreach($overallLearningCategoryStat as $learningCategoryID => $stat){
    $sql = "INSERT INTO `LNT_OVERALL_STAT`
    (
        `AcademicYearID`,
        `Min`,
        `Mean`,
        `Max`,
        `SD`,
        `TotalCount`,
        `Type`,
        `LearningCategoryID`, 
        `SubjectID`,
        `InputDate`,
        `CreatedBy`
    ) VALUES (
        '{$AcademicYearID}',
        '{$stat['Min']}',
        '{$stat['Average']}',
        '{$stat['Max']}',
        '{$stat['SD']}',
        '{$stat['TotalCount']}',
        '" . learnAndTeach::OVERALL_STAT_TYPE_SUBJECT . "',
        '{$learningCategoryID}',
        NULL,
        NOW(),
        '{$_SESSION['UserID']}'
    );";
    $result[] = $objDB->db_db_query($sql);
    $sqlArr[] = $sql;
}
## Save All Learning Category Overall Stat END ##
#### Save Record Average END ####
/* * /
debug_r($result);
debug_r($sqlArr);
exit;
/* */
if(in_array(false, $result, true)){
    $objDB->RollBack_Trans();
    $passCount = 0;
    $xmsg = 'ImportUnsuccess';
}else{
    $objDB->Commit_Trans();
    $passCount = count($allSurvey);
    $xmsg = 'ImportSuccess';
}
######## Save Data END ########


######## UI START ########
# Left menu 
$TAGS_OBJ[] = array($Lang['eSurvey']['LearnAndTeach']['ToSDAS']);
$CurrentPageArr['eAdmineSurvey'] = 1;
$CurrentPage = "PageLearnAndTeachToSDAS";
$MODULE_OBJ = $lsurvey->GET_MODULE_OBJ_ARR();

# Start layout
$h_stepUI = $linterface->GET_IMPORT_STEPS(
    $CurrStep = 3,
    $CustStepArr = array(
        $Lang['eSurvey']['LearnAndTeach']['SelectData'],
        $Lang['General']['ImportArr']['ImportStepArr']['CSVConfirmation'],
        $Lang['General']['ImportArr']['ImportStepArr']['ImportResult']
    )
);
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>

<form method="POST" name="form1" id=""form1"" action="index.php">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td ><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
		</tr>
		<tr>
			<td><?=$h_stepUI;?></td>
		</tr>
	</table>
			
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr height="20"><td>&nbsp;</td></tr>				
		<tr>
			<td align='center'>
				<?= $passCount ."&nbsp;". $Lang['General']['ImportArr']['RecordsImportedSuccessfully']?>
			</td>
		</tr>
		<tr height="20"><td>&nbsp;</td></tr>

		<tr>
			<td>
				<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
					<tr>
						<td align="center">
							<?= $linterface->GET_ACTION_BTN($button_back, "submit")?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form>


<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
