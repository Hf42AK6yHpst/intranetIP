<?php
//Using: Pun
/**
 * Change Log:
 * 2017-05-24 Pun
 *  - File create
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if (!$sys_custom['iPf']['learnAndTeachReport'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libsurvey.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/SDAS/customReport/learnAndTeach/learnAndTeach.php");

intranet_auth();
intranet_opendb();

$objDB = new libdb();
$linterface = new interface_html();
$lsurvey = new libsurvey();
$learnAndTeach = new learnAndTeach($objDB);
$fcm = new form_class_manage();
$ay = new academic_year($AcademicYearID);


######## Define Const START ########
define('ERROR_FORMAT', 'ERROR_FORMAT');
define('ERROR_CLASS_NAME', 'ERROR_CLASS_NAME');
define('ERROR_SUBJ_ABBR', 'ERROR_SUBJ_ABBR');
define('ERROR_TEACHER_NAME', 'ERROR_TEACHER_NAME');
define('ERROR_TEACHER_SUBJECT', 'ERROR_TEACHER_SUBJECT');
######## Define Const END ########



######## Get Data START ########

#### Get Academic Year START ####
$academicYearName = $ay->Get_Academic_Year_Name();
#### Get Academic Year END ####

#### Get Class START ####
$allClass = $fcm->Get_All_Year_Class($AcademicYearID);
$allClassName = Get_Array_By_Key($allClass, Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN'));
$allClassNameUpper = array_map('strtoupper', $allClassName);
#### Get Class END ####

#### Get Subject START ####
$sql = "SELECT
    RecordID,
    EN_ABBR,
    CH_ABBR
FROM 
    ASSESSMENT_SUBJECT
WHERE
    RecordStatus = 1
AND
    (
        CMP_CODEID = ''
    OR 
        CMP_CODEID IS NULL
    )
";
$allSubject = $objDB->returnResultSet($sql);
$allSubjectAbbrId = array();
$allSubjectAbbr = array();
foreach($allSubject as $subj){
    $allSubjectAbbr[] = $subj['EN_ABBR'];
    $allSubjectAbbr[] = $subj['CH_ABBR'];
    $allSubjectAbbrId[ strtoupper($subj['EN_ABBR']) ] = $subj['RecordID'];
    $allSubjectAbbrId[ strtoupper($subj['CH_ABBR']) ] = $subj['RecordID'];
}
$allSubjectAbbr = array_unique($allSubjectAbbr);
$allSubjectAbbrUpper = array_keys($allSubjectAbbrId);
#### Get Subject END ####

#### Get Subject Teacher START ####
$sql = "SELECT
    STCT.UserID,
    ST.SubjectID
FROM
    SUBJECT_TERM_CLASS_TEACHER STCT
INNER JOIN
    SUBJECT_TERM_CLASS STC
ON
    STCT.SubjectGroupID = STC.SubjectGroupID
INNER JOIN 
    SUBJECT_TERM ST
ON
    STC.SubjectGroupID = ST.SubjectGroupID
INNER JOIN
    ACADEMIC_YEAR_TERM AYT
ON
    ST.YearTermID = AYT.YearTermID
AND
    AYT.AcademicYearID = '{$AcademicYearID}'
";
$rs = $objDB->returnResultSet($sql);
$allTeacherId = Get_Array_By_Key($rs, 'UserID');
$allTeacherIdSql = implode("','", $allTeacherId);

$allTeacherSubjectId = array();
foreach($rs as $r){
    $allTeacherSubjectId[ $r['UserID'] ][] = $r['SubjectID'];
}

$sql = "SELECT UserID,ChineseName,EnglishName FROM INTRANET_USER WHERE UserID IN ('{$allTeacherIdSql}')";
$rs1 = $objDB->returnArray($sql);
$sql = "SELECT UserID,ChineseName,EnglishName FROM INTRANET_ARCHIVE_USER WHERE UserID IN ('{$allTeacherIdSql}')";
$rs2 = $objDB->returnArray($sql);
$teachers = array_merge($rs1, $rs2);

$allTeacherName = array();
$allTeacherNameId = array();
foreach($teachers as $teacher){
    $allTeacherName[] = $teacher['EnglishName'];
    $allTeacherName[] = $teacher['ChineseName'];
    $allTeacherNameId[ strtoupper($teacher['EnglishName']) ] = $teacher['UserID'];
    $allTeacherNameId[ strtoupper($teacher['ChineseName']) ] = $teacher['UserID'];
}
$allTeacherName = array_unique($allTeacherName);
$allTeacherNameUpper = array_keys($allTeacherNameId);
#### Get Subject Teacher END ####

#### Get Survey START ########
$sql = "SELECT
    *
FROM
    INTRANET_SURVEY
WHERE
    DateStart >= '{$StartDate} 00:00:00'
AND
    DateStart <= '{$EndDate} 23:59:59'
AND
    RecordStatus = 1
ORDER BY
    DateStart, Title
";
$rs = $objDB->returnResultSet($sql);

function filterSurvey($r){
    return preg_match('/[^-]+-[^-]+-[^-]+/', $r['Title']);
}
$rs = array_filter($rs, 'filterSurvey');
$allSurvey = BuildMultiKeyAssoc($rs, array('SurveyID'));
#### Get Survey END ########

#### Check Data Validation START ####
$correctSurveyId = array();
$incorrectSurveyId = array();
$surveyResult = array();
foreach($allSurvey as $surveyID => $surveyDetail){
    $title = $surveyDetail['Title'];
    list($className, $subjectAbbr, $teacherName) = explode('-', $title);
    
    $surveyResult[$surveyID] = array(
        'title' => $title,
        'className' => $className,
        'subjectAbbr' => $subjectAbbr,
        'teacherName' => $teacherName,
        'ERROR' => array()
    );
    
    ## Check empty START ##
    if(
        $className == '' ||
        $subjectAbbr == '' ||
        $teacherName == ''
    ){
        $surveyResult[$surveyID]['ERROR'][] = ERROR_FORMAT;
        $incorrectSurveyId[] = $surveyID;
        continue;
    }
    ## Check empty END ##

    ## Check class exists START ##
    /* Removed, class name checking
    if(!in_array(strtoupper($className), $allClassNameUpper)){
        $surveyResult[$surveyID]['ERROR'][] = ERROR_CLASS_NAME;
    }*/
    ## Check class exists END ##

    ## Check subject exists START ##
    if(!in_array(strtoupper($subjectAbbr), $allSubjectAbbrUpper)){
        $surveyResult[$surveyID]['ERROR'][] = ERROR_SUBJ_ABBR;
    }
    ## Check subject exists END ##

    ## Check teacher exists START ##
    $isValid = false;
    $teacherId = 0;
    foreach($allTeacherNameUpper as $tName){
        if(strpos($tName, strtoupper($teacherName)) !== false){
            $teacherId = $allTeacherNameId[$tName];
            $isValid = true;
            break;
        }
    }
    if(!$isValid){
        $surveyResult[$surveyID]['ERROR'][] = ERROR_TEACHER_NAME;
    }
    ## Check teacher exists END ##
    
    ## Check teacher teach subject START ##
    $subjectID = $allSubjectAbbrId[strtoupper($subjectAbbr)];
    if(
        $teacherId && 
        $subjectID &&
        !in_array( $subjectID, (array)$allTeacherSubjectId[$teacherId] )
    ){
        $surveyResult[$surveyID]['ERROR'][] = ERROR_TEACHER_SUBJECT;
    }
    ## Check teacher teach subject END ##
    
    if(empty($surveyResult[$surveyID]['ERROR'])){
        $correctSurveyId[] = $surveyID;
    }else{
        $incorrectSurveyId[] = $surveyID;
    }
}
$passCount = count($correctSurveyId);
$failCount = count($incorrectSurveyId);
#### Check Data Validation END ####


#### Check data exists in academic year START ####
$transferDetails = $learnAndTeach->getAllTransferDetails();

$academicYearDataCount = 0;
foreach($transferDetails as $details){
    if($details['AcademicYearID'] == $AcademicYearID){
        $academicYearDataCount++;
    }
}

if($academicYearDataCount){
    $academicYearDataExists = "<span style=\"color:red;\">{$Lang['eSurvey']['LearnAndTeach']['Exists']}</span>";
}else{
    $academicYearDataExists = "<span>{$Lang['eSurvey']['LearnAndTeach']['NotExists']}</font>";
}
#### Check data exists in academic year END ####

######## Get Data END ########



######## UI START ########
# Left menu 
$TAGS_OBJ[] = array($Lang['eSurvey']['LearnAndTeach']['ToSDAS']);
$CurrentPageArr['eAdmineSurvey'] = 1;
$CurrentPage = "PageLearnAndTeachToSDAS";
$MODULE_OBJ = $lsurvey->GET_MODULE_OBJ_ARR();

# Start layout
$h_stepUI = $linterface->GET_IMPORT_STEPS(
    $CurrStep = 2,
    $CustStepArr = array(
        $Lang['eSurvey']['LearnAndTeach']['SelectData'],
        $Lang['General']['ImportArr']['ImportStepArr']['CSVConfirmation'],
        $Lang['General']['ImportArr']['ImportStepArr']['ImportResult']
    )
);
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
    	<td align='right'><?=$linterface->GET_SYS_MSG("",$xmsg);?></td>
    </tr>
    <tr>
    	<td><?=$h_stepUI;?></td>
    </tr>
    
<!-- ######## Pass/Fail Summary START ######## -->
    <tr>
    	<td>	
    		<table width="20%" border="0" cellspacing="0" cellpadding="0" align="center">
    		<tr>
    			<td class='formfieldtitle'><?=str_replace('<!--Year-->', $academicYearName, $Lang['eSurvey']['LearnAndTeach']['CurrentData'])?>:</td>
    			<td class='tabletext'><?=$academicYearDataExists?></td>
    		</tr>
    		<tr>
    			<td class='formfieldtitle'><?=$Lang['General']['SuccessfulRecord']?>:</td>
    			<td class='tabletext'><?=$passCount?></td>
    		</tr>
    		<tr>
    			<td class='formfieldtitle'><?=$Lang['General']['FailureRecord']?>:</td>
    			<td class='tabletext <?php if($failCount>0) print('red');?>'><?=$failCount?></td>
    		</tr>
    		</table>
    	</td>
    </tr>
<!-- ######## Pass/Fail Summary END ######## -->
    
    
<!-- ######## Fail Details START ######## -->
<?php if($failCount){ ?>
    <tr>
    	<td>
    		<table width="90%" border="0" cellspacing="0" cellpadding="2" align="center">
    			<tr>
    				<td class='tablebluetop tabletopnolink' width="1%">#</th>
    				<td class="tablebluetop tabletopnolink"><?=$Lang['eSurvey']['Title']?></th>
    				<?php if($failCount>0) {?>
    					<td class="tablebluetop tabletopnolink"><?=$Lang['eSurvey']['LearnAndTeach']['WrongRemarks']?></th>
    				<?php }?>
    			</tr>
    			
				<?php 
				$i = 0;
				foreach($surveyResult as $surveyID => $surveyDetail){ 
				    if(empty($surveyDetail['ERROR'])){
				        continue;
				    }
				?>
    			<tr class='tablebluerow<?=(($i+1)%2+1)?>'>
    				<td class='tabletext' valign='top'><?=(++$i)?></td>
    				<td class='tabletext' valign='top'><?=$surveyDetail['title']?></td>
    				<?php if($failCount>0) {?>				
    					<td class='tabletext' valign='top'>
    					<?php foreach($surveyDetail['ERROR'] as $error){ ?>
    						<?= $Lang['eSurvey']['LearnAndTeach']['WrongRemarksDetail'][$error]?><br />
    					<?php } ?>
    					</td>
    				<?php }?>	
    			</tr>
    			<?php 
				} //end foreach
				?>
				
				<tr><td colspan="3">
					<span class="tabletextremark">
						<br />
						<span class="tabletextrequire">*</span>
						<?=$Lang['eSurvey']['LearnAndTeach']['TransferRemarks']?>
					</span>
				</td></tr>
    		</table>
			
    	</td>
    </tr>
<?php } ?>
<!-- ######## Fail Details END ######## -->
    
<!-- ######## Submit Button START ######## -->
    <tr>
    	<td>
    		
            <form id="form1" name="form1" action="transferSave.php" method="post">
            	<?php foreach($correctSurveyId as $surveyID){ ?>
            		<input type="hidden" name="surveyID[]" value="<?=$surveyID ?>" />
            	<?php } ?>
        		<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID ?>" />
        		<input type="hidden" name="StartDate" value="<?=$StartDate ?>" />
        		<input type="hidden" name="EndDate" value="<?=$EndDate ?>" />
        		<input type="hidden" name="incorrectSurveyId" value="<?=implode(',', $incorrectSurveyId) ?>" />

            	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
            		<tr>
            			<td>
            				<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
            					<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
            					<tr>
            						<td align="center">
            							<?= $linterface->GET_ACTION_BTN($Lang['eSurvey']['LearnAndTeach']['Transfer'], "submit")?>&nbsp;
            							<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "document.form1.action = 'index.php';document.form1.submit();")?>
            						</td>
            					</tr>
            				</table>
            			</td>
            		</tr>
            	</table>
            </form>
    	</td>
    </tr>
<!-- ######## Submit Button END ######## -->

</table>



<script>
(function($){

	$('#form1').submit(function(e){

		<?php if($academicYearDataCount){ ?>
			return confirm('<?=$Lang['eSurvey']['LearnAndTeach']['ConfirmOverwrite'] ?>');
		<?php } ?>

		return true;
	});
	
})(jQuery);
</script>


<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
