<?php
//Using: Pun
/**
 * Change Log:
 * 2017-12-13 Pun
 *  - File create
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if (!$sys_custom['iPf']['learnAndTeachReport'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/libsurvey.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/cust/student_data_analysis_system/libSDAS.php");
include_once($PATH_WRT_ROOT."includes/SDAS/customReport/learnAndTeach/learnAndTeach.php");

intranet_auth();
intranet_opendb();

$objDB = new libdb();
$lsurvey = new libsurvey();
$sdas = new libSDAS();
$year = new Year();
$lc = new Learning_Category();
$ay = new academic_year($AcademicYearID);
$fcm = new form_class_manage();
$stc = new subject_term_class();
$lexport = new libexporttext();

$passCount = 0;
$result = array();
$sqlArr = array();



######## Get Survey START ########
$sql = "SELECT
    s.*,
    sgs.SubjectGroupID
FROM
    INTRANET_SURVEY s
INNER JOIN
    INTRANET_SUBJECTGROUPSURVEY sgs
ON
    s.SurveyID = sgs.SurveyID
WHERE
    DateStart >= '{$StartDate} 00:00:00'
AND
    DateStart <= '{$EndDate} 23:59:59'
AND
    s.RecordStatus = 1
AND 
    s.QuestionType = 'LT'
ORDER BY
    s.DateStart, s.Title
";
$rs = $objDB->returnResultSet($sql);

$surveyIdArr = Get_Array_By_Key($rs, 'SurveyID');
$allSurvey = BuildMultiKeyAssoc($rs, array('SurveyID'));
$subjectGroupIdArr = Get_Array_By_Key($rs, 'SubjectGroupID');
######## Get Survey END ########


######## Get Survey Answer START ########
$surveyIdSql = implode("','", $surveyIdArr);
$sql = "SELECT
    SurveyID,
    UserID
FROM
    INTRANET_SURVEYANSWER
WHERE
    SurveyID IN ('{$surveyIdSql}')";
$rs = $objDB->returnResultSet($sql);

$surveyAnswerArr = array();
foreach($rs as $r){
    $surveyAnswerArr[$r['SurveyID']][] = $r['UserID'];
}
######## Get Survey Answer END ########


######## Get Student START ########
$subjectGroupIdSql = implode("','", $subjectGroupIdArr);
$sql = "SELECT
    stcu.SubjectGroupID,
    stc.ClassTitleB5 as SubjectGroupNameB5,
    stc.ClassTitleEN as SubjectGroupNameEN,
    yc.ClassTitleB5 as ClassNameB5,
    yc.ClassTitleEN as ClassNameEN,
    ytcu.ClassNumber,
    stcu.UserID as StudentID,
    iu.UserLogin as UserLogin,
    iu.EnglishName,
    iu.ChineseName
FROM
    SUBJECT_TERM_CLASS_USER as stcu
INNER JOIN
    SUBJECT_TERM_CLASS stc
ON
    stcu.SubjectGroupID = stc.SubjectGroupID
INNER JOIN
    INTRANET_USER iu
ON
    stcu.UserID = iu.UserID
INNER JOIN
    YEAR_CLASS_USER as ytcu 
ON 
    stcu.UserID = ytcu.UserID
INNER JOIN
    YEAR_CLASS as yc 
ON 
    ytcu.YearClassID = yc.YearClassID
AND
    AcademicYearID = '{$AcademicYearID}'
INNER JOIN
    YEAR as y 
ON 
    yc.YearID = y.YearID
WHERE
    stcu.SubjectGroupID IN ('{$subjectGroupIdSql}')
ORDER BY
    y.Sequence, yc.Sequence, ytcu.ClassNumber";
$rs = $objDB->returnResultSet($sql);

$subjectGroupStudent = array();
foreach($rs as $r){
    $subjectGroupStudent[$r['SubjectGroupID']][] = $r;
}
######## Get Student END ########


######## Pack Data START ########
$result = array();
foreach($allSurvey as $survey){
    $surveyId = $survey['SurveyID'];
    $startDate = substr($survey['DateStart'],0,10);
    $endDate = substr($survey['DateEnd'],0,10);
    $title = $survey['Title'];
    $subjectGroupID = $survey['SubjectGroupID'];
    
    $studentArr = $subjectGroupStudent[$subjectGroupID];
    foreach((array)$studentArr as $student){
        $target = Get_Lang_Selection($student['SubjectGroupNameB5'],$student['SubjectGroupNameEN']);
        $className = Get_Lang_Selection($student['ClassNameB5'],$student['ClassNameEN']);
        $classNumber = $student['ClassNumber'];
        $userLogin = $student['UserLogin'];
        $englishName = $student['EnglishName'];
        $chineseName = $student['ChineseName'];
        $studentID = $student['StudentID'];
        $isAnswered = (in_array($studentID, (array)$surveyAnswerArr[$surveyId]));
        
        $result[] = array(
            $startDate,
            $endDate,
            $title,
            $target,
            $className,
            $classNumber,
            $userLogin,
            $englishName,
            $chineseName,
            ($isAnswered)? $Lang['eSurvey']['LearnAndTeach']['SubmissionStatusY'] : $Lang['eSurvey']['LearnAndTeach']['SubmissionStatusN']
        );
    }
}
######## Pack Data END ########


######## Export START ########
$delimiter = "\t";
$valueQuote = "";

$Content = '';
$Content .= $valueQuote.$Lang['eSurvey']['LearnAndTeach']['SurveyStartDate'].$valueQuote.$delimiter;
$Content .= $valueQuote.$Lang['eSurvey']['LearnAndTeach']['SurveyEndDate'].$valueQuote.$delimiter;
$Content .= $valueQuote.$Lang['eSurvey']['Title'].$valueQuote.$delimiter;
$Content .= $valueQuote.$Lang['eSurvey']['SurveyType'].$valueQuote.$delimiter;
$Content .= $valueQuote.$Lang['SysMgr']['FormClassMapping']['ClassTitle'].$valueQuote.$delimiter;
$Content .= $valueQuote.$Lang['SysMgr']['FormClassMapping']['ClassNo'].$valueQuote.$delimiter;
$Content .= $valueQuote.$Lang['General']['UserLogin'].$valueQuote.$delimiter;
$Content .= $valueQuote.$Lang['General']['EnglishName'].$valueQuote.$delimiter;
$Content .= $valueQuote.$Lang['General']['ChineseName'].$valueQuote.$delimiter;
$Content .= $valueQuote.$Lang['eSurvey']['LearnAndTeach']['SubmissionStatus'].$valueQuote.$delimiter;


$Content = trim($Content, $delimiter);
$Content .= "\n";


foreach($result as $d1){
    foreach($d1 as $d2){
        $Content .= $valueQuote.$d2.$valueQuote.$delimiter;
    }
    
    $Content = trim($Content, $delimiter);
    $Content .= "\n";
}


// Output the file to user browser
$lexport->EXPORT_FILE("Survey_submit_status_{$StartDate}_{$EndDate}.csv", $Content);




