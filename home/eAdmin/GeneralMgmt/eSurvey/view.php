<?php

#################################
#	Date:	2017-11-07 (Paul)
#			- get group list for subject group if L&T
#
#	Date:	2015-03-24 (Jason)
#			- add js order_lang to support Ordering Type
#
#	Date:	2011-06-28	YatWoon
#			- add checking NotAllowReSign to determine show the remark or not
#
#	Date:	2011-02-23	YatWoon
#			Add "Display question number"
#################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
include_once($PATH_WRT_ROOT."includes/libsurvey.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
intranet_auth();
intranet_opendb();

$lform = new libform();
$SurveyID = IntegerSafe($SurveyID);
$lsurvey = new libsurvey($SurveyID);

$MODULE_OBJ['title'] = $Lang['eSurvey']['SurveyPreview'];
$home_header_no_EmulateIE7=true;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$queString = $lsurvey->Question;
$queString = str_replace('"','&quot;',$queString);
$queString = str_replace("\n",'<br>',$queString);
$queString = str_replace("\r",'',$queString);

$poster = $lsurvey->returnPosterName();
$ownerGroup = $lsurvey->returnOwnerGroup();
$targetGroups = $lsurvey->returnTargetGroups();
$targetSubjectGroups = $lsurvey->returnTargetSubjectGroups();
$reqFillAllFields = $lsurvey->reqFillAllFields();

if (sizeof($targetGroups)==0 && sizeof($targetSubjectGroups)==0)
{
    $target = $Lang['eSurvey']['WholeSchool'];
}
else
{
    $target = implode(", ",array_merge($targetGroups,$targetSubjectGroups));
}


$link = "edit.php?SurveyID=".$SurveyID;
$edit_btn = $linterface->GET_ACTION_BTN($Lang['Btn']['Edit'], "button", "EditSurvey('$link');" );
?>

<script language="javascript">
function EditSurvey(url)
{
	window.opener.location=url;
	window.close();
}

</script>

<div class="table_content">
<table class="form_table_v30">
<tr>
	<td class="field_title"><?=$i_general_startdate?></td>
	<td><?=$lsurvey->DateStart?></td>
</tr>

<tr>
	<td class="field_title"><?=$i_general_enddate?></td>
	<td><?=$lsurvey->DateEnd?></td>
</tr>
<tr>
	<td class="field_title"><?=$i_general_title?></td>
	<td><?=$lsurvey->Title?></td>
</tr>
<tr>
	<td class="field_title"><?=$i_general_description?></td>
	<td><?=nl2br(intranet_convertAllLinks($lsurvey->Description))?></td>
</tr>
<?php if ($sys_custom['SurveySaveWithDraft']) { ?>
<tr>
	<td class="field_title"><?php echo $Lang['eSurvey']['AllowSaveDraft']; ?></td>
	<td><?php echo ($lsurvey->AllowSaveDraft) ? $i_general_yes : $i_general_no; ?></td>
</tr>
<?php } ?>
<tr>
	<td class="field_title"><?=$i_Survey_Poster?></td>
	<td><?=$poster?></td>
</tr>
<tr>
	<td class="field_title"><?=$i_general_TargetGroup?></td>
	<td><?=$target?></td>
</tr>

<? if ($lsurvey->RecordType == 1) { ?>
<tr>
	<td>&nbsp;</td>
	<td>[<?=$i_Survey_Anonymous?>]</td>
</tr>
<? } ?>

<? if ($reqFillAllFields) { ?>
<tr>
	<td>&nbsp;</td>
	<td>[<?=$i_Survey_AllRequire2Fill?>]</td>
</tr>
<? } ?>


</table>
</div>


<table class="inside_form_table" width="100%">
<tr>
	<td colspan=2>
		<form name="ansForm" method="post" action="update.php">
		<input type=hidden name="qStr" value="">
		<input type=hidden name="aStr" value="">
		</form>
		
		<? if($lsurvey->NotAllowReSign) {?>
		<?=$i_Survey_PleaseFill?>
		<? } ?> <br/><?=$Lang['eSurvey']['Hint'] ?>
		<hr>
		
		<script LANGUAGE=Javascript>
		// Set true if need to fill in all fields before submit
		var need2checkform = <? if($reqFillAllFields){ echo "true"; } else { echo "false"; }  ?>;
		</script>
		<script language="javascript" src="/templates/forms/layer.js"></script>
		<script language="javascript" src="/templates/forms/form_edit.js"></script>
		
		<script language="Javascript">
		<?=$lform->getWordsInJS()?>
	
		var replyslip = '<?=$Lang['eSurvey']['ReplySlip']?>';
		var order_lang = '<?=$Lang['eSurvey']['Order']?>';
		var choice_lang = '<?=$Lang['Polling']['Choice']?>';
		var hint_lang = '<?=$Lang['Polling']['Hint']?>';

		//background_image = "/images/layer_bg.gif";
		var DisplayQuestionNumber = '<?=$lsurvey->DisplayQuestionNumber?>';
		
		var sheet= new Answersheet();
		// attention: MUST replace '"' to '&quot;'
		sheet.qString="<?=$queString?>";
		//edit submitted application
		sheet.mode=1;
		sheet.answer=sheet.sheetArr();
		//sheet.templates=form_templates;
		document.write(editPanel());
		</script>
		<SCRIPT LANGUAGE=javascript>
		function copyback()
		{
		 finish();
		 document.form1.qStr.value = document.ansForm.qStr.value;
		 document.form1.aStr.value = document.ansForm.aStr.value;
		}
		function validSubmit()
		{
		 if (!need2checkform || formAllFilled)
		 {
		      return true;
		 }
		 else //return true;
		 {
		      alert("<?=$i_Survey_alert_PleaseFillAllAnswer?>!");
		      return false;
		 }
		}
		</SCRIPT>
	</td>
</tr>
</table>

<form name="form1" action="print_preview.php" method="post" target="_blank">

<div class="edit_bottom_v30">
<p class="spacer"></p>
	<?= $linterface->GET_ACTION_BTN($Lang['eSurvey']['PrintPreiew'], "button", "document.form1.submit();") ?>
	<?=$edit_btn?>
	<?= $linterface->GET_ACTION_BTN($button_close, "button", "window.close(); return false;","cancelbtn") ?>
<p class="spacer"></p>
</div>
<input type=hidden name=qStr value="">
<input type=hidden name=aStr value="">
<input type=hidden name=SurveyID value="<?=$SurveyID?>">
</form>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>