<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $data["lang"]["TeacherPortfolio"]["title"]; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<script src="<?php echo $data["AssetsPath"]; ?>/jquery/jquery-3.1.1.min.js"></script>
<link href="<?php echo $data["AssetsPath"]; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<script src="<?php echo $data["AssetsPath"]; ?>/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo $data["AssetsPath"]; ?>/bootstrap/js/bootstrap-notify.min.js"></script>
<script src="<?php echo $data["AssetsPath"]; ?>/bootstrap/js/bootbox.min.js"></script>
<script src="<?php echo $data["AssetsPath"]; ?>/js/plupload/js/plupload.full.min.js"></script>
<link href="<?php echo $data["AssetsPath"]; ?>/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="<?php echo $data["AssetsPath"]; ?>/webfont/Arvo.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo $data["AssetsPath"]; ?>/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="<?php echo $data["AssetsPath"]; ?>/bootstrap-datepicker/bootstrap-datepicker3.css"/>
<link rel="stylesheet" href="<?php echo $data["AssetsPath"]; ?>/css/datatables.min.css"/>
<link rel="stylesheet" href="<?php echo $data["AssetsPath"]; ?>/css/jquery.qtip.min.css"/>
<link href="<?php echo $data["AssetsPath"]; ?>/css/animate.css" rel="stylesheet" />
<script src="<?php echo $data["AssetsPath"]; ?>/js/datatables.min.js"></script>
<script src="<?php echo $data["AssetsPath"]; ?>/js/jquery.qtip.min.js"></script>
<script src="<?php echo $data["AssetsPath"]; ?>/js/jquery-sortable.js"></script>
<script src="<?php echo $data["AssetsPath"]; ?>/js/jquery.fileDownload.js"></script>
<link href="<?php echo $data["AssetsPath"]; ?>/css/style.css" rel="stylesheet" />
<link href="<?php echo $data["AssetsPath"]; ?>/css/tp.css" rel="stylesheet" />
<script src="<?php echo $data["PATH_WRT_ROOT"]; ?>templates/script.js"></script>
<script src="<?php echo $data["AssetsPath"]; ?>/js/tbapp.js?t=<?php echo time(); ?>"></script>
</head>
<body class="childwindow">