<?
/*
 * 	2017-11-09 Cameron
 * 		- add manage_allowOwnerAddOrEditCpd
 */
?>
<div class="toolbar">
	<ul class="actions left">
		<li><a href="#" rel="<?php echo $data["section"]; ?>" class="btnAjax"><i class="fa fa-caret-left"></i> <?php echo $data["lang"]["TeacherPortfolio"]["back"]; ?></a></li>
	</ul>
</div>
<div class="content manage">
	<div class="title">
		<span class="text1"><?php echo $data["lang"]["TeacherPortfolio"]["manage_configs"]; ?></span> <span class="text2"><?php echo $data["lang"]["TeacherPortfolio"]["sec_" . $data["section"]]; ?></span>
	</div><br>
	<div class="container-fluid <?php echo $data["currMethod"]; ?> manageUserList">
		<form id="editform" name="editform" method="post" rel="<?php echo $data["section"]; ?>/configsprocess">
		<? if ($data["_CUSTOMIZATION_"]["allowImportCPDByIndividualTeacher"]):?>
			<div class="form-check">
				<label>
					<input type="checkbox" name="allowOwnerAddOrEditCpd" value="Y"<?php if ($data["tpConfigs"]["allowOwnerAddOrEditCpd"] == "Y") echo " checked"; ?>><span class="custom-check"></span>
					<?php echo $data["lang"]["TeacherPortfolio"]["manage_allowOwnerAddOrEditCpd"]; ?>
				</label>
			</div>
		<? endif;?>
		<? if (!isset($data["_CUSTOMIZATION_"]["displayCPDOnly"]) || ($data["_CUSTOMIZATION_"]["displayCPDOnly"]==false)):?>
			<div class="form-check">
				<label>
					<input type="checkbox" name="allowOwnerAddOrEditPerformance" value="Y"<?php if ($data["tpConfigs"]["allowOwnerAddOrEditPerformance"] == "Y") echo " checked"; ?>><span class="custom-check"></span>
					<?php echo $data["lang"]["TeacherPortfolio"]["manage_allowOwnerAddOrEditPerformance"]; ?>
				</label>
			</div>
			<div class="form-check">
				<label>
					<input type="checkbox" name="allowOwnerAddOrEditCertificate" value="Y"<?php if ($data["tpConfigs"]["allowOwnerAddOrEditCertificate"] == "Y") echo " checked"; ?>><span class="custom-check"></span>
					<?php echo $data["lang"]["TeacherPortfolio"]["manage_allowOwnerAddOrEditCertificate"]; ?>
				</label>
			</div>
			<div class="form-check">
				<label>
					<input type="checkbox" name="allowOwnerAddOrEditOthers" value="Y"<?php if ($data["tpConfigs"]["allowOwnerAddOrEditOthers"] == "Y") echo " checked"; ?>><span class="custom-check"></span>
					<?php echo $data["lang"]["TeacherPortfolio"]["manage_allowOwnerAddOrEditOthers"]; ?>
				</label>
			</div>
		<? endif;?>	
			<div class="row">
				<div class="col-xs-12 text-center">
					<a href="#" class="btnSubmit btn btn-sm btn-primary"><?php echo $data["lang"]["TeacherPortfolio"]["Submit"]; ?></a>
					<a href="#" rel="<?php echo $data["section"]; ?>" class="btnAjax btn btn-sm btn-default btnBack"><?php echo $data["lang"]["TeacherPortfolio"]["Cancel"]; ?></a>
				</div>
			</div><br>
		</form>
	</div>
	
	<iframe id="dfileframe" style="display:none;"></iframe>
	<?php echo $data["defaultFooter"]; ?>
</div>