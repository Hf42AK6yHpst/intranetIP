<div class="toolbar">
	<ul class="actions left">
		<li><a href="#" rel="<?php echo $data["section"]; ?>" class="btnAjax"><i class="fa fa-caret-left"></i> <?php echo $data["lang"]["TeacherPortfolio"]["back"]; ?></a></li>
	</ul>
</div>
<div class="content manage">
	<div class="title">
		<span class="text1"><?php echo $data["lang"]["TeacherPortfolio"]["manage_addUser"]; ?></span> <span class="text2"><?php echo $data["lang"]["TeacherPortfolio"]["sec_" . $data["section"]]; ?></span>
	</div><br>
	<div class="container-fluid <?php echo $data["currMethod"]; ?> manageUserList">
		<div class="row">
			<div class="col-xs-6">
				<div class="panel panel-warning">
					<div class="panel-heading">
						<?php echo $data["lang"]["TeacherPortfolio"]["manage_chooseUser"]; ?>
					</div>
					<div class="panel-body">
						<div style="min-height: 300px;">
							<div class="row">
								<div class="col-sm-12">
									<?php echo $data["lang"]["TeacherPortfolio"]["manage_fromGroup"]; ?><br>
									<?php echo $data["lang"]["TeacherPortfolio"]["manage_chooseMember"]; ?><br><br>
									<a href="#" class="btn btn-xs btn-warning btnPopup" rel="<?php echo $data["PATH_WRT_ROOT"]; ?>home/common_choose/index.php?fieldname=SelectedUserIDArr[]&page_title=SelectMembers&permitted_type=1&DisplayGroupCategory=1"><?php echo $data["lang"]["TeacherPortfolio"]["manage_select"]; ?></a><br><br>
									<?php echo $data["lang"]["TeacherPortfolio"]["manage_or"]; ?><br><br>
									<?php echo $data["lang"]["TeacherPortfolio"]["manage_searchByID"]; ?><br><br>
									<input type='text' name='ulogin' class='ajaxAutoComplete' rel="ajaxdata" value=""><br>
									<div id="suggesstion-box"></div><br>
								</div>
								<div class="col-sm-12">
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-6">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<?php echo $data["lang"]["TeacherPortfolio"]["manage_selectedUsers"]; ?>
					</div>
					<div class="panel-body">
						<div style="min-height:300px;">
							<form id="form1" name="form1" method="post">
							<select name="SelectedUserIDArr[]" id="SelectedUserIDArr[]" class="select_teacherlist" size="10" style='width:100%;' multiple="multiple">
							</select><br><br>
							<a href="#" class='btnRemoveSelected btn btn-xs btn-danger pull-right'><?php echo $data["lang"]["TeacherPortfolio"]["manage_removeSelected"]; ?></a>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div><br>
		<div class="row">
			<div class="col-xs-12 text-center">
				<a href="#" class="btnManageSubmit btn btn-sm btn-primary"><?php echo $data["lang"]["TeacherPortfolio"]["Submit"]; ?></a>
				<a href="#" rel="<?php echo $data["section"]; ?>" class="btnAjax btn btn-sm btn-default btnBack"><?php echo $data["lang"]["TeacherPortfolio"]["Cancel"]; ?></a>
			</div>
		</div><br>
	</div>
	
	<iframe id="dfileframe" style="display:none;"></iframe>
	<?php echo $data["defaultFooter"]; ?>
</div>