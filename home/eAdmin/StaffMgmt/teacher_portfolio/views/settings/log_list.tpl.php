<div class="toolbar">
	<ul class="actions left">
		<li><a href="#" rel="<?php echo $data["section"]; ?>" class="btnAjax"><i class="fa fa-caret-left"></i> <?php echo $data["lang"]["TeacherPortfolio"]["back"]; ?></a></li>
	</ul>
	<ul class="actions">
		<li><a href="#" rel="<?php echo $data["section"]; ?>.log_exportrecord" class="btnFDAjax"><i class="glyphicon glyphicon-save-file"></i> <?php echo $data["lang"]["TeacherPortfolio"]["btnExport"]; ?></a></li>
<?php /* <li><a href="#" class="btnPrint printAction"><i class="glyphicon glyphicon-print"></i> <?php echo $data["lang"]["TeacherPortfolio"]["btnPrint"]; ?></a></li> */?>
		<li><a href="?task=<?php echo $data["section"]; ?>.log_printall" class="printAllAction"><i class="glyphicon glyphicon-print"></i> <?php echo $data["lang"]["TeacherPortfolio"]["btnPrint"]; ?></a></li>
	</ul>
</div>
<div class="content manage log_record">
	<div class="title">
		<span class="text1"><?php echo $data["lang"]["TeacherPortfolio"]["manage_logRecord"]; ?></span> <span class="text2"><?php echo $data["lang"]["TeacherPortfolio"]["sec_" . $data["section"]]; ?></span>
	</div><br>
	<form id='ftpl-form'>
	<table id="managelog-bdTable" class="tp-table bdTable" cellspacing="0" width="100%" rel='<?php echo $data["section"]; ?>.log_listjson' data-searching='true' data-noholder='true'>
		<thead>
			<tr>
				<th class='tbheader_col' width="15%" rel='LogDate'><?php echo $data["lang"]["TeacherPortfolio"]["manage_logDate"]; ?> <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th>
				<th class='tbheader_col' width="15%" rel='LogType'><?php echo $data["lang"]["TeacherPortfolio"]["manage_logType"]; ?> <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th>
				<th class='tbheader_col' width="55%" rel='LogDetail'><?php echo $data["lang"]["TeacherPortfolio"]["manage_logDetail"]; ?> <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th>
				<th class='tbheader_col' width="15%" rel='UserName'><?php echo $data["lang"]["TeacherPortfolio"]["manage_userName"]; ?> <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table><br>
	</form>
	<?php echo $data["defaultFooter"]; ?>
</div>