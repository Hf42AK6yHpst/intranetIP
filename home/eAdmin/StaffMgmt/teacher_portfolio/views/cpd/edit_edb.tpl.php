<?
/*
 * 2019-02-21 Paul
 * 		- Add checking for No. of Section field
 * 
 * 2018-01-15 Paul
 * 		- Add fields to fit EDB requirement	
 * 
 * 2017-11-16 Cameron
 * 		- NoOfSection should allow float number
 */
	$fieldTitle = $data["formTplInfo"]["childs"];
?>

<!--CONTENT: DEVELOP - EDIT-->
<div id="others-edit" class="tab-pane fade in active">
	<div class="toolbar">                
		<ul class="actions left">
			<li><a href="#" rel="<?php echo $data["section"]; ?>" class="btnAjax"><i class="fa fa-caret-left"></i> <?php echo $data["lang"]["TeacherPortfolio"]["back"]; ?></a></li>
		</ul>
	</div>
<?php
	if ($data["currAction"] == "edit" && $data["infoRecord"] == "no record") {
?>
		<div class="content">
			<?php echo $data["lang"]["TeacherPortfolio"]["noThisRecord"]; ?>
		</div>
<?php
	} else {
?>
	<div class="content cert-form">
		<script>
			function syncEventName(){
				$('#EventNameChi').val($('#EventNameEn').val());
			} 
			
			function syncEventType(){
				$('#EventTypeChi').val($('#EventTypeEn').val());
			}
			
			function updateCPDdomainSelector(){
				var selectedArr = $('#CPDDomainSelector').val();
				var domainArr = [];
				for(var k=0; k < 6; k++){
					if(selectedArr.indexOf(k.toString()) == -1){
						domainArr.push("0");
					}else{
						domainArr.push("1");
					}
				}
				$('#CPDDomain').val(domainArr.join());
			}
			
			function updateSubjectRelatedSelector(){
				var selectedArr = $('#SubjectRelatedSelector').val();
				var subjectRelatedArr = [];
				for(var k=0; k < <?=count($data["lang"]['TeacherPortfolio']['edit_cpd_subject'])?>; k++){
					if(selectedArr.indexOf(k.toString()) == -1){
						subjectRelatedArr.push("0");
					}else{
						subjectRelatedArr.push("1");
					}
				}
				$('#SubjectRelated').val(subjectRelatedArr.join());
			}
			
			function controlNoOfSection(){
				var section = parseFloat($('#NoOfSection').val());
				if(section < 0){
					notifyMsgJS.func.showMsg('<?php echo $data["lang"]['TeacherPortfolio']["err_cpdhour_negative"]; ?>', 'danger');
					$('#NoOfSection').val('');
					$('#NoOfSection').focus();
				}else{
					$('#NoOfSection').val(section.toFixed(2));
					var startdate = $('#StartDate').val();
					var enddate = $('#EndDate').val();
					var date1 = new Date(startdate+" 00:00:00");
					var date2 = new Date(enddate+" 23:59:59");
					var timeDiff = Math.abs(date2.getTime() - date1.getTime());
					var diffHours = Math.ceil(timeDiff / (1000 * 3600)); 
					if(diffHours < section){
						notifyMsgJS.func.showMsg('<?php echo $data["lang"]['TeacherPortfolio']["err_cpdhour_exceed"]; ?>', 'danger');
						$('#NoOfSection').val('');
						$('#NoOfSection').attr('max',diffHours);
						$('#NoOfSection').focus();
					}
				}
			}
			
			function controlStartDate(){
				var yearStartDate = new Date($('select[name="AcademicYear"] option:selected').attr('start'));
				var yearEndDate = new Date($('select[name="AcademicYear"] option:selected').attr('end'));
				var startDate = new Date($('#StartDate').val());
				if(yearStartDate.getTime() > startDate.getTime() || startDate.getTime() > yearEndDate.getTime()){
					notifyMsgJS.func.showMsg('<?php echo $data["lang"]['TeacherPortfolio']["dateInYear"]; ?>', 'danger');
					$('#StartDate').val('');
					$('#StartDate').focus();
				}
			}
			
			function controlEndDate(){
				var yearStartDate = new Date($('select[name="AcademicYear"] option:selected').attr('start'));
				var yearEndDate = new Date($('select[name="AcademicYear"] option:selected').attr('end'));
				var endDate = new Date($('#EndDate').val());
				if(yearStartDate.getTime() > endDate.getTime() || endDate.getTime() > yearEndDate.getTime()){
					notifyMsgJS.func.showMsg('<?php echo $data["lang"]['TeacherPortfolio']["dateInYear"]; ?>', 'danger');
					$('#EndDate').val('');
					$('#EndDate').focus();
				}
			}
			
		</script>
		<form id="editform" rel="<?php echo $data["section"]; ?>/process">
		<?php if ($data["currAction"] == "edit") { ?>
			<input type='hidden' name='action' value='updaterecord'>
			<input type='hidden' name='recid' value='<?php echo $data["infoRecord"]["FTDataRowID"]; ?>'>
		<?php } else { ?>
			<input type='hidden' name='action' value='insertrecord'>
		<?php } ?>
		<div class="title">
			<span class="text1"><?php echo $data["lang"]["TeacherPortfolio"][$data["currAction"]]; ?></span><span class="text2"><?php echo $data["lang"]['TeacherPortfolio']["sec_cpd"]; ?></span>
		</div>
		<div class="body">
<?php if ($data["_ROLE_"]["isAdminMode"] && $data["_ROLE_"]["hasAdminRight"]) { ?>
			<div class="form-group">
				<label for="selectedUser" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["Teacher"]; ?></label>
				<div class="col-field">
					<select class="form-control input-sm" name='SelectedUserID'>
<?php
			if (count($data["allTeachers"]) > 0) {
?>
				<option value='0'><?php echo $data["lang"]['TeacherPortfolio']["pleaseSelectTeacher"]; ?></option>
<?php
				foreach ($data["allTeachers"] as $tkk => $tvv) {
					echo "<option value='" . $tvv["UserID"] . "_" . $tvv["cs"] . "'";
					if ($data["currAction"] == "edit") {
						if ($data["infoRecord"]["UserID"] == $tvv["UserID"]) {
							echo " selected";
						}
					} else {
						if ($data["userInfo"]["info"]["SelectedUserID"] == $tvv["UserID"]) {
							echo " selected";
						}
					}
					echo ">" . Get_Lang_Selection($tvv["ChineseName"], $tvv["EnglishName"]) . " ( LoginID: " . $tvv["UserLogin"]. " )</option>";
				}
				} ?></select>
				</div>
			</div>		
<?php } else {
		echo '<input type="hidden" name="SelectedUserID" value="'.$_SESSION['UserID'].'_'.time().'">';
	}
?>
			<div class="form-group">
				<label for="year" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["SchoolYear"]; ?></label>
				<div class="col-field">
					<select class="form-control inputstl" name="AcademicYear" id="year" required>
					<option value=""><?php echo $data["lang"]["TeacherPortfolio"]["pleaseSelect"]; ?></option>
<?php
					if (count($data["yearArr"]) > 0) {
						foreach ($data["yearArr"] as $year) {
							$yearID = $year['AcademicYearID'];
							$displayYearLang = ($data['isEJ'])?convert2unicode(Get_Lang_Selection($year["YearNameB5"], $year["YearNameEN"]), true):Get_Lang_Selection($year["YearNameB5"], $year["YearNameEN"]);		
?>
					<option start="<?php echo $year['YearStart']; ?>" end="<?php echo $year['YearEnd']; ?>" value="<?php echo $yearID; ?>"<?php if (isset($data["infoRecord"]["AcademicYear"]) && $data["infoRecord"]["AcademicYear"]==$year['YearNameEN']) { ?> selected<?php } ?>><?php echo $displayYearLang; ?></option>
<?php
						}
					}
?>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label for="date" class="col-form-label"><?=Get_Lang_Selection($fieldTitle["StartDate"]["FieldNameChi"], $fieldTitle["StartDate"]["FieldNameEn"])?></label>
				<div class="col-field">
					<div class="input-group date inputdate" id="datetimepicker2">
						<input class="form-control date" id="StartDate" name="StartDate" placeholder="YYYY-MM-DD" value="<?php if (isset($data["infoRecord"]["StartDate"])) echo $data["infoRecord"]["StartDate"]; ?>" type="text" required onchange="javascript:controlStartDate()" />
						<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
					</div>
				</div>
			</div>    

			<div class="form-group">
				<label for="date" class="col-form-label"><?=Get_Lang_Selection($fieldTitle["EndDate"]["FieldNameChi"], $fieldTitle["EndDate"]["FieldNameEn"])?></label>
				<div class="col-field">
					<div class="input-group date inputdate" id="datetimepicker2">
						<input class="form-control date" id="EndDate" name="EndDate" placeholder="YYYY-MM-DD" value="<?php if (isset($data["infoRecord"]["EndDate"])) echo $data["infoRecord"]["EndDate"]; ?>" type="text" required onchange="javascript:controlEndDate()" />
						<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
					</div>
				</div>
			</div> 
			<?php
				if($data["_CUSTOMIZATION_"]['ltmpsStyle']){
			?>   
			<div class="form-group">
				<label for="NoOfSection" class="col-form-label"><?=Get_Lang_Selection($fieldTitle["NoOfSection"]["FieldNameChi"], $fieldTitle["NoOfSection"]["FieldNameEn"])?></label>
				<div class="col-field">
					<div class="input-group hour" style="width:35%">
						<input class="form-control" type="number" name="NoOfSection" value="<?php if (isset($data["infoRecord"]["NoOfSection"])) echo $data["infoRecord"]["NoOfSection"]; ?>" id="NoOfSection" min=0 step=0.5 required onfocusout="javascript:controlNoOfSection()" style="border-radius: 4px;">
						<span class="input-group-addon" style="background-color: transparent;color: black;border: none;"><?php echo $data["lang"]['TeacherPortfolio']["Hours"];?></span>
					</div>
				</div>
			</div>
			<?php
				}else{
			?>
			<div class="form-group">
				<label for="NoOfSection" class="col-form-label"><?=Get_Lang_Selection($fieldTitle["NoOfSection"]["FieldNameChi"], $fieldTitle["NoOfSection"]["FieldNameEn"])?></label>
				<div class="col-field">
					<input class="form-control" type="number" name="NoOfSection" value="<?php if (isset($data["infoRecord"]["NoOfSection"])) echo $data["infoRecord"]["NoOfSection"]; ?>" id="NoOfSection" min=0 step=0.5 required onfocusout="javascript:controlNoOfSection()">
				</div>
			</div>
			<?php
				}
			?>	
			<div class="form-group">
				<label for="EventNameEn" class="col-form-label"><?=Get_Lang_Selection($fieldTitle["EventName"]["FieldNameChi"], $fieldTitle["EventName"]["FieldNameEn"])?></label>
				<div class="col-field">
					<input class="form-control" type="text" name="EventNameEn" value="<?php if (isset($data["infoRecord"]["EventNameEn"])) echo $data["infoRecord"]["EventNameEn"]; ?>" id="EventNameEn" required onchange="syncEventName();">
					<input class="form-control" type="hidden" name="EventNameChi" value="<?php if (isset($data["infoRecord"]["EventNameEn"])) echo $data["infoRecord"]["EventNameEn"]; ?>" id="EventNameChi" required>
				</div>
			</div>
			<?php
				if($data["_CUSTOMIZATION_"]['ltmpsStyle']){
			?> 
			<div class="form-group">
				<label for="EventTypeEn" class="col-form-label"><?=Get_Lang_Selection($fieldTitle["EventType"]["FieldNameChi"], $fieldTitle["EventType"]["FieldNameEn"])?></label>
				<div class="col-field">
					<input class="form-control" type="text" name="EventTypeEn" value="<?php if (isset($data["infoRecord"]["EventTypeEn"])) echo $data["infoRecord"]["EventTypeEn"]; ?>" id="EventTypeEn" onchange="syncEventType();">
					<input class="form-control" type="hidden" name="EventTypeChi" value="<?php if (isset($data["infoRecord"]["EventTypeEn"])) echo $data["infoRecord"]["EventTypeEn"]; ?>" id="EventTypeChi">
				</div>
			</div>
			<?php
				}else{
			?>
			<div class="form-group">
				<label for="EventTypeEn" class="col-form-label"><?=Get_Lang_Selection($fieldTitle["EventType"]["FieldNameChi"], $fieldTitle["EventType"]["FieldNameEn"])?></label>
				<div class="col-field">
					<input class="form-control" type="text" name="EventTypeEn" value="<?php if (isset($data["infoRecord"]["EventTypeEn"])) echo $data["infoRecord"]["EventTypeEn"]; ?>" id="EventTypeEn" required onchange="syncEventType();">
					<input class="form-control" type="hidden" name="EventTypeChi" value="<?php if (isset($data["infoRecord"]["EventTypeEn"])) echo $data["infoRecord"]["EventTypeEn"]; ?>" id="EventTypeChi" required>
				</div>
			</div>
			<?php
				}
			?>
			<div class="form-group">
				<label for="Content" class="col-form-label"><?=Get_Lang_Selection($fieldTitle["Content"]["FieldNameChi"], $fieldTitle["Content"]["FieldNameEn"])?> </label>
				<div class="col-field">
					<input class="form-control" type="text" name="Content" value="<?php if (isset($data["infoRecord"]["Content"])) echo $data["infoRecord"]["Content"]; ?>" id="Content">
				</div>
			</div>
			
			<div class="form-group">
				<label for="Organization" class="col-form-label"><?=Get_Lang_Selection($fieldTitle["Organization"]["FieldNameChi"], $fieldTitle["Organization"]["FieldNameEn"])?></label>
				<div class="col-field">
					<input class="form-control" type="text" name="Organization" value="<?php if (isset($data["infoRecord"]["Organization"])) echo $data["infoRecord"]["Organization"]; ?>" id="Organization">
				</div>
			</div>
			
			<div class="form-group">
				<label for="CPDMode" class="col-form-label"><?=Get_Lang_Selection($fieldTitle["CPDMode"]["FieldNameChi"], $fieldTitle["CPDMode"]["FieldNameEn"])?></label>
				<div class="col-field">
					<!--<input class="form-control" type="text" name="CPDMode" value="<?php if (isset($data["infoRecord"]["CPDMode"])) echo $data["infoRecord"]["CPDMode"]; ?>" id="CPDMode">-->
					<select class="form-control" name="CPDMode" id="CPDMode">
						<option value="0" <?=($data["infoRecord"]["CPDMode"]==0)?"selected":""?>><?=$data["lang"]['TeacherPortfolio']["edit_cpd_mode_systematic_scheme"]?></option>
						<option value="1" <?=($data["infoRecord"]["CPDMode"]==1)?"selected":""?>><?=$data["lang"]['TeacherPortfolio']["edit_cpd_mode_others"]?></option>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label for="CPDDomain" class="col-form-label"><?=Get_Lang_Selection($fieldTitle["CPDDomain"]["FieldNameChi"], $fieldTitle["CPDDomain"]["FieldNameEn"])?></label>
				<div class="col-field">					
					<?php
						$CPDDomainArr = explode(",",$data["infoRecord"]["CPDDomain"]);
						if($data["_CUSTOMIZATION_"]['ltmpsStyle']){
							$cpdDomainSelStyle = "height:140px";
						}
					?>
					<select class="form-control" id="CPDDomainSelector" multiple onchange="updateCPDdomainSelector()" style="<?php echo $cpdDomainSelStyle; ?>">
						<option value="0" <?=($CPDDomainArr[0]==1)?"selected":""?>><?=$data["lang"]['TeacherPortfolio']['edit_cpd_domain'][0]?></option>
						<option value="1" <?=($CPDDomainArr[1]==1)?"selected":""?>><?=$data["lang"]['TeacherPortfolio']['edit_cpd_domain'][1]?></option>
						<option value="2" <?=($CPDDomainArr[2]==1)?"selected":""?>><?=$data["lang"]['TeacherPortfolio']['edit_cpd_domain'][2]?></option>
						<option value="3" <?=($CPDDomainArr[3]==1)?"selected":""?>><?=$data["lang"]['TeacherPortfolio']['edit_cpd_domain'][3]?></option>
						<option value="4" <?=($CPDDomainArr[4]==1)?"selected":""?>><?=$data["lang"]['TeacherPortfolio']['edit_cpd_domain'][4]?></option>
						<option value="5" <?=($CPDDomainArr[5]==1)?"selected":""?>><?=$data["lang"]['TeacherPortfolio']['edit_cpd_domain'][5]?></option>
					</select>
					
					<input class="form-control" type="hidden" name="CPDDomain" id="CPDDomain" value="<?php if (isset($data["infoRecord"]["CPDDomain"])) echo $data["infoRecord"]["CPDDomain"]; ?>" id="CPDDomain">
				</div>
			</div>
			<!--
			<div class="form-group">
				<label for="BasicLawRelated" class="col-form-label"><?=Get_Lang_Selection($fieldTitle["BasicLawRelated"]["FieldNameChi"], $fieldTitle["BasicLawRelated"]["FieldNameEn"])?></label>
				<div class="col-field">
					<input class="form-control" type="number" name="BasicLawRelated" value="<?php if (isset($data["infoRecord"]["BasicLawRelated"])) echo $data["infoRecord"]["BasicLawRelated"]; ?>" id="BasicLawRelated">
				</div>
			</div>
			-->
			<?php
				if($data["_CUSTOMIZATION_"]['ltmpsStyle']){
			?>
			<div class="form-group">
				<label for="SubjectRelated" class="col-form-label"></label>
				<div class="col-field">
				<b>註���������專業���修範���</b><br>
				<ol>
				 	<li>������學方���������修���學������容������ / 課���������學內容知��� / ���學策略 / 評核���������/li>
				    <li>學������������面������修��� 學������校������不���������  / ���學���建立���信������ / 學������顧  / 學������������學習������/li>
				    <li>學校������������修��� 學校願景，������������風 / ���政 / 家庭���學���������/ ������社���變革</li>
				    <li>專業群������������������方���������修������內������������  / ���師專業������ / ���育����������������/ ������������������社���������������願工���/li>
				    <li>���師���長���發展������修������������學���接������，������助���師作全人發展���活��� （e.g. 減���工������������������能訓練等���</li>
				</ol>         
				<b>註���������������學校���注事���</b>					
				</div>
			</div>			
			<?php		
				}	
			?>
			<?php
				if($data["_CUSTOMIZATION_"]['cpdSubjectTag']){
			?>
			
			<div class="form-group">
				<label for="SubjectRelated" class="col-form-label"><?=Get_Lang_Selection($fieldTitle["SubjectRelated"]["FieldNameChi"], $fieldTitle["SubjectRelated"]["FieldNameEn"])?></label>
				<div class="col-field">					
					<?php
						$SubjectRelatedArr = explode(",",$data["infoRecord"]["SubjectRelated"]);
						if($data["_CUSTOMIZATION_"]['cpdSubjectTag']){
							$subjectRelatedSelStyle = "height:200px";
						}
					?>
					<select class="form-control" id="SubjectRelatedSelector" multiple onchange="updateSubjectRelatedSelector()" style="<?php echo $subjectRelatedSelStyle; ?>">
					<?php
						for($i=0; $i < count($data["lang"]['TeacherPortfolio']['edit_cpd_subject']); $i++){
					?>
						<option value="<?=$i?>" <?=($SubjectRelatedArr[$i]==1)?"selected":""?>><?=$data["lang"]['TeacherPortfolio']['edit_cpd_subject'][$i]?></option>
					<?php		
						}
					?>							
					</select>
					
					<input class="form-control" type="hidden" name="SubjectRelated" id="SubjectRelated" value="<?php if (isset($data["infoRecord"]["SubjectRelated"])) echo $data["infoRecord"]["SubjectRelated"]; ?>">
				</div>
			</div>
			<?php
				}
			?>
			<div class="form-group buttons">
				<div class="col-form-label"></div>
				<div class="col-field">
					<a href="#" class="btn btn-primary btnSubmit" rel="needfile"><?php echo $data["lang"]['TeacherPortfolio']["Submit"]; ?></a>
					<a href="#" rel="<?php echo $data["section"]; ?>" class="btnAjax btn btn-secondary btnBack"><?php echo $data["lang"]['TeacherPortfolio']["Cancel"]; ?></a>
					<?php if ($data["currAction"]=='edit') { ?><a href="#" class="btn btnDel btn-danger" rel="<?php echo $data["section"]; ?>" ><?php echo $data["lang"]['TeacherPortfolio']["Delete"]; ?></a><?php } ?>
				</div>
			</div>
			</form>
		</div>
	</div>
	<?php } ?>
</div>
<?php echo $data["defaultFooter"]; ?>