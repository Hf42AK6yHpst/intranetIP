<!--CONTENT: DEVELOP - EDIT-->
<div id="others-edit" class="tab-pane fade in active">
	<div class="toolbar">                
		<ul class="actions left">
			<li><a href="#" rel="<?php echo $data["section"]; ?>" class="btnAjax"><i class="fa fa-caret-left"></i> <?php echo $data["lang"]["TeacherPortfolio"]["back"]; ?></a></li>
		</ul>
	</div>
<?php
	if ($data["currAction"] == "edit" && $data["infoRecord"] == "no record") {
?>
		<div class="content">
			<?php echo $data["lang"]["TeacherPortfolio"]["noThisRecord"]; ?>
		</div>
<?php
	} else {
?>
	<div class="content cert-form">
		<form id="editform" rel="<?php echo $data["section"]; ?>/process">
		<?php if ($data["currAction"] == "edit") { ?>
			<input type='hidden' name='action' value='updaterecord'>
			<input type='hidden' name='recid' value='<?php echo $data["infoRecord"]["InfoID"]; ?>'>
		<?php } else { ?>
			<input type='hidden' name='action' value='insertrecord'>
		<?php } ?>
			<input type='hidden' name='date' value='<?php if (isset($data["infoRecord"]["InfoDate"])) echo $data["infoRecord"]["InfoDate"]; else echo date("Y-m-d"); ?>'>
		<div class="title">
			<span class="text1"><?php echo $data["lang"]["TeacherPortfolio"][$data["currAction"]]; ?></span><span class="text2"><?php echo $data["lang"]['TeacherPortfolio']["sec_performance"]; ?></span>
		</div>
		<div class="body">
<?php if ($data["_ROLE_"]["isAdminMode"] && $data["_ROLE_"]["hasAdminRight"]) { ?>
			<div class="form-group">
				<label for="selectedUser" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["Teacher"]; ?></label>
				<div class="col-field">
					<select class="form-control input-sm" name='selectedUser'>
<?php
			if (count($data["allTeachers"]) > 0) {
?>
				<option value='0'><?php echo $data["lang"]['TeacherPortfolio']["pleaseSelectTeacher"]; ?></option>
<?php
				foreach ($data["allTeachers"] as $tkk => $tvv) {
					echo "<option value='" . $tvv["UserID"] . "_" . $tvv["cs"] . "'";
					if ($data["currAction"] == "edit") {
						if ($data["infoRecord"]["UserID"] == $tvv["UserID"]) {
							echo " selected";
						}
					} else {
						if ($data["userInfo"]["info"]["SelectedUserID"] == $tvv["UserID"]) {
							echo " selected";
						}
					}
					echo ">" . Get_Lang_Selection($tvv["ChineseName"], $tvv["EnglishName"]) . " ( LoginID: " . $tvv["UserLogin"]. " )</option>";
				}
				} ?></select>
				</div>
			</div>		
<?php } ?>
			<div class="form-group">
				<label for="year" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["SchoolYear"]; ?></label>
				<div class="col-field">
					<select class="form-control inputstl" name="academicYear" id="year" required>
					<option value=""><?php echo $data["lang"]["TeacherPortfolio"]["pleaseSelect"]; ?></option>
<?php
					if (count($data["yearArr"]) > 0) {
						foreach ($data["yearArr"] as $yearID => $year) {
?>
					<option value="<?php echo $yearID; ?>"<?php if (isset($data["infoRecord"]["TypeNameEn"]) && $data["infoRecord"]["TypeNameEn"]==$yearID) { ?> selected<?php } ?>><?php echo Get_Lang_Selection($year["YearNameB5"], $year["YearNameEN"]); ?></option>
<?php
						}
					}
?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="chiInfoSubject" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["AppraisalType"]; ?> (<?php echo $data["lang"]["TeacherPortfolio"]["lang_chi"]; ?>)</label>
				<div class="col-field">
					<input class="form-control" type="text" name="chiInfoSubject" value="<?php if (isset($data["infoRecord"]["InfoSubjectChi"])) echo $data["infoRecord"]["InfoSubjectChi"]; ?>" id="chiInfoSubject" required>
				</div>
			</div>
			<div class="form-group">
				<label for="engInfoSubject" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["AppraisalType"]; ?> (<?php echo $data["lang"]["TeacherPortfolio"]["lang_eng"]; ?>)</label>
				<div class="col-field">
					<input class="form-control" type="text" name="engInfoSubject" value="<?php if (isset($data["infoRecord"]["InfoSubjectEn"])) echo $data["infoRecord"]["InfoSubjectEn"]; ?>" id="engInfoSubject" required>
				</div>
			</div>
			<div class="form-group uploadFileArea required">
				<label for="" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["AppraisalFile"]; ?> <i class="fa fa-asterisk fa-fw"></i></label>
				<div class="col-field">
					<div id="existfilelist" rel='<?php echo $data["section"]; ?>.filelist'><?php
					if (isset($data["infoRecordFiles"]) && count($data["infoRecordFiles"]) > 0) {
						echo '<ul id="extFiles" class="uploaded">';
						foreach ($data["infoRecordFiles"] as $kk => $vv) {
							echo '<li><div class="btnDelfile" rel="' . $data["section"] . '.dfile&flid=' . $vv["FileID"] . '&d=' . $data["currDate"] . '&skey=' . md5($vv["FileID"] . '_' . $vv["FileSize"] . '_' .  $data["currDate"]) .'"> ' . $vv["OrgFileName"] . ' <i class="fa fa-trash" style="cursor: pointer;"></i>  <span></span></div>';
							echo '<input type="hidden" name=files[] value="' . $vv["FileID"] . '"></li>';
						}
						echo "</ul><br>";
					}
					?></div>
					<div id="drop-target"><?php echo $data["lang"]['TeacherPortfolio']["fileupload_dragupload"]; ?></div>
					<div id="filelist">Your browser doesn't have Flash, Silverlight or HTML5 support.</div>
					<div id="file_container" rel='<?php if (isset($data["fileLimit"])) { echo $data["fileLimit"]; } else { echo 0; } ?>'>
						<a id="pickfiles" href="#" class="btn-upload"><i class="fa fa-plus"></i> <?php echo $data["lang"]["TeacherPortfolio"]["fileupload_selectfile"]; ?></a> 
						<!-- <a id="uploadfiles" href="#" class="btn-upload"><i class="fa fa-arrow-circle-o-up"></i> Upload files</a> -->
						<div class="console"></div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label for="chiInfoRemark" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["Remarks"]; ?> (<?php echo $data["lang"]["TeacherPortfolio"]["lang_chi"]; ?>)</label>
				<div class="col-field">
					<input class="form-control" type="text" name="chiInfoRemark" value="<?php if (isset($data["infoRecord"]["InfoRemarkChi"])) echo $data["infoRecord"]["InfoRemarkChi"]; ?>" id="chiInfoRemark">
				</div>
			</div>
			<div class="form-group">
				<label for="engInfoRemark" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["Remarks"]; ?> (<?php echo $data["lang"]["TeacherPortfolio"]["lang_eng"]; ?>)</label>
				<div class="col-field">
					<input class="form-control" type="text" name="engInfoRemark" value="<?php if (isset($data["infoRecord"]["InfoRemarkEn"])) echo $data["infoRecord"]["InfoRemarkEn"]; ?>" id="engInfoRemark">
				</div>
			</div>
<?php if ($data["_ROLE_"]["isAdminMode"] && $data["_ROLE_"]["hasAdminRight"]) { ?>
			<div class="form-group">
				<label for="InfoPermission" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["Permission"]; ?></label>
				<div class="col-field">
					<label><input type="radio" name="InfoPermission" value="Y" <?php if (!isset($data["infoRecord"]["InfoPermission"]) || $data["infoRecord"]["InfoPermission"] != "N") echo "checked" ?> id="InfoPermission"> Y</label>
					<label><input type="radio" name="InfoPermission" value="N" <?php if (isset($data["infoRecord"]["InfoPermission"]) && $data["infoRecord"]["InfoPermission"] == "N") echo "checked" ?> id="InfoPermission"> N</label>
					<br><label>** <?php echo $data["lang"]["TeacherPortfolio"]["Permission_remark"]; ?></label>
				</div>
			</div>
<?php } ?>
			<div class="form-group buttons">
				<div class="col-form-label"></div>
				<div class="col-field">
					<a href="#" class="btn btn-primary btnSubmit" rel="needfile"><?php echo $data["lang"]['TeacherPortfolio']["Submit"]; ?></a>
					<a href="#" rel="<?php echo $data["section"]; ?>" class="btnAjax btn btn-secondary btnBack"><?php echo $data["lang"]['TeacherPortfolio']["Cancel"]; ?></a>
					<?php if ($data["currAction"]=='edit') { ?><a href="#" class="btn btnDel btn-danger" rel="<?php echo $data["section"]; ?>" ><?php echo $data["lang"]['TeacherPortfolio']["Delete"]; ?></a><?php } ?>
				</div>
			</div>
			</form>
		</div>
		<?php echo $data["defaultFooter"]; ?>
	</div>
	<?php } ?>
</div>