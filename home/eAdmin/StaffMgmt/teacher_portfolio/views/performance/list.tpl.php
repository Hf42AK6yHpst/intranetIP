<div class="toolbar">
	<div class="filter btn-group">
		<?php echo $data["html_YearPanel"]; ?>
	</div>
	<ul class="actions">
<?php
if ($data["_ROLE_"]["isAdminMode"] && $data["_ROLE_"]["hasAdminRight"] || $data["_ROLE_"]["isOwnerMode"] && $data["_ROLE_"]["hasOwnerRight"] && $data["tpConfigs"]["allowOwnerAddOrEditPerformance"] == "Y") {
?>
		<li><a href="#" rel="<?php echo $data["section"]; ?>/addrecord" class="btnAjax"><i class="fa fa-plus"></i> <?php echo $data["lang"]["TeacherPortfolio"]["btnAddNew"]; ?></a></li>
<?php } ?>
		<li><a href="#" rel="<?php echo $data["section"]; ?>.exportrecord" class="btnFDAjax btnExport"><i class="glyphicon glyphicon-save-file"></i> <?php echo $data["lang"]["TeacherPortfolio"]["btnExport"]; ?></a></li>
<?php /*<li><a href="#" class="btnPrint printAction"><i class="glyphicon glyphicon-print"></i> <?php echo $data["lang"]["TeacherPortfolio"]["btnPrint"]; ?></a></li> */ ?>
		<li><a href="?task=<?php echo $data["section"]; ?>.printall" class="printAllAction"><i class="glyphicon glyphicon-print"></i> <?php echo $data["lang"]["TeacherPortfolio"]["btnPrint"]; ?></a></li>
	</ul>
</div>
<div class="content performance">
	<table id="performance-bdTable" class="tp-table bdTable table-hover" cellspacing="0" width="100%" <?php if ($data["_ROLE_"]["isAdminMode"] && $data["_ROLE_"]["hasAdminRight"]) { echo "data-searching='Y'"; } ?>>
		<thead>
			<tr>
<?php if ($data["_ROLE_"]["isAdminMode"] && $data["_ROLE_"]["hasAdminRight"]) { ?>
				<th class='tbheader_col' width="10%" rel='TeacherName'><?php echo $data["lang"]["TeacherPortfolio"]["TeacherName"]; ?> <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th><!-- use "fa-sort-asc" for ascending order-->
<?php } ?>
				<th class='tbheader_col' width="10%" rel='academicYear'><?php echo $data["lang"]["TeacherPortfolio"]["SchoolYear"]; ?>  <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th>
				<th class='tbheader_col' width="10%" rel='infoSubject'><?php echo $data["lang"]["TeacherPortfolio"]["AppraisalType"]; ?>  <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th>
				<th class='tbheader_col' width="10%" rel='infoName'><?php echo $data["lang"]["TeacherPortfolio"]["AppraisalFile"]; ?>  <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th>
<?php if ($data["_ROLE_"]["isAdminMode"] && $data["_ROLE_"]["hasAdminRight"]) { ?>
				<th class='tbheader_col' width="10%" rel='InfoPermission'><?php echo $data["lang"]["TeacherPortfolio"]["Permission"]; ?>  <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th>
<?php } ?>
				<th class='tbheader_col' width="10%" rel='infoRemark'><?php echo $data["lang"]["TeacherPortfolio"]["Remarks"]; ?>  <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th>
<?php if ($data["_ROLE_"]["isAdminMode"] && $data["_ROLE_"]["hasAdminRight"]) { ?>
				<th class='tbheader_col' width="10%" rel='DateModified'><nobr><?php echo $data["lang"]["TeacherPortfolio"]["LastUpdate"]; ?> <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th>
<?php } ?>				
<?php if ($data["_ROLE_"]["isAdminMode"] && $data["_ROLE_"]["hasAdminRight"] || $data["_ROLE_"]["isOwnerMode"] && $data["_ROLE_"]["hasOwnerRight"] && $data["tpConfigs"]["allowOwnerAddOrEditPerformance"] == "Y") { ?>
				<th class='tbheader_col' width="10%" style='width:10%' rel='func'>&nbsp;</th>
<?php } ?>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table><br>
	<?php if (isset($data["debugMsg"])) echo $data["debugMsg"]; ?>
</div>
<?php echo $data["defaultFooter"]; ?>