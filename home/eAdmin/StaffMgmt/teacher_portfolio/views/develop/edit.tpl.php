<!--CONTENT: DEVELOP - EDIT-->
<div id="develop-edit" class="tab-pane fade in active">
	<div class="toolbar">                
		<ul class="actions left">
			<li><a href="#" rel="<?php echo $data["section"]; ?>" class="btnAjax"><i class="fa fa-caret-left"></i> <?php echo $data["lang"]["TeacherPortfolio"]["back"]; ?></a></li>
		</ul>
	</div>
<?php
	if ($data["currAction"] == "edit" && $data["cpdRecord"] == "no record") {
?>
		<div class="content">
			<?php echo $data["lang"]["TeacherPortfolio"]["noThisRecord"]; ?>
		</div>
<?php
	} else {
?>
	<div class="content develop-form">
		<form id="editform" rel="<?php echo $data["section"]; ?>/process">
		<?php if ($data["currAction"] == "edit") { ?>
			<input type='hidden' name='action' value='updaterecord'>
			<input type='hidden' name='recid' value='<?php echo $data["cpdRecord"]["CpdID"]; ?>'>
		<?php } else { ?>
			<input type='hidden' name='action' value='insertrecord'>
		<?php } ?>
		<div class="title">
			<span class="text1"><?php echo $data["lang"]["TeacherPortfolio"][$data["currAction"]]; ?></span><span class="text2"><?php echo $data["lang"]['TeacherPortfolio']["sec_develop"]; ?></span>
		</div>
		<div class="body">
			<div class="form-group">
				<label for="year" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["selectSchoolYear"]; ?></label>
				<div class="col-field">
					<select class="form-control inputstl" name="academicYear" id="year" required>
					<option value=""><?php echo $data["lang"]["TeacherPortfolio"]["pleaseSelect"]; ?></option>
				<?php
					if (count($data["yearArr"]) > 0) {
						foreach ($data["yearArr"] as $yearID => $year) {
				?>
					<option value="<?php echo $yearID; ?>"<?php if (isset($data["cpdRecord"]["AcademicYearID"]) && $data["cpdRecord"]["AcademicYearID"]==$yearID) { ?> selected<?php } ?>><?php echo Get_Lang_Selection($year["YearNameB5"], $year["YearNameEN"]); ?></option>
				<?php
						}
					}
				?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="chiName" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["Event"]; ?> (<?php echo $data["lang"]["TeacherPortfolio"]["lang_chi"]; ?>)</label>
				<div class="col-field">
					<input class="form-control" type="text" name="chiName" value="<?php if (isset($data["cpdRecord"]["ActivityNameChi"])) echo $data["cpdRecord"]["ActivityNameChi"]; ?>" id="chiName" required>
				</div>
			</div>
			<div class="form-group">
				<label for="engName" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["Event"]; ?> (<?php echo $data["lang"]["TeacherPortfolio"]["lang_eng"]; ?>)</label>
				<div class="col-field">
					<input class="form-control" type="text" name="engName" value="<?php if (isset($data["cpdRecord"]["ActivityNameEn"])) echo $data["cpdRecord"]["ActivityNameEn"]; ?>" id="engName" required>
				</div>
			</div>
			<div class="form-group">
				<label for="StartDate" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["StartDate"]; ?></label>
				<div class="col-field">
					<div class="input-group date" id="datetimepicker1">
						<input class="form-control date" id="StartDate" name="StartDate" placeholder="YYYY-MM-DD" type="text" required value="<?php if (isset($data["cpdRecord"]["StartDate"])) echo $data["cpdRecord"]["StartDate"]; ?>" />
						<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
					</div>
				</div>
			</div> 
			
			<div class="form-group">
				<label for="EndDate" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["EndDate"]; ?></label>
				<div class="col-field">
					<div class="input-group date" id="datetimepicker2">
						<input class="form-control date" id="EndDate" name="EndDate" placeholder="YYYY-MM-DD" value="<?php if (isset($data["cpdRecord"]["EndDate"])) echo $data["cpdRecord"]["EndDate"]; ?>" type="text"/>
						<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
					</div>
				</div>
			</div>    
															   
			<div class="form-group">
				<label for="" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["TPDD"]; ?></label>
				<div class="col-field">
					<label><input name="isTPDD" value="1" type="checkbox"<?php if (isset($data["cpdRecord"]["IsDevDay"]) && $data["cpdRecord"]["IsDevDay"] == 1) { ?> checked<?php }?>><span class="custom-check"></span></label>
				</div>
			</div>
															   
			<div class="form-group">
				<label for="nature" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["Nature"]; ?></label>
				<div class="col-field">
					<select class="form-control inputstl" id="nature" name="nature" required>
					<option value=""><?php echo $data["lang"]["TeacherPortfolio"]["pleaseSelect"]; ?></option>
				<?php
					if (count($data["Options"]["natureArr"]) > 0) {
						foreach ($data["Options"]["natureArr"] as $natureID => $nature) {
				?>
					<option value="<?php echo $natureID; ?>"<?php if (isset($data["cpdRecord"]["CPDNatureID"]) && $data["cpdRecord"]["CPDNatureID"]==$natureID) { ?> selected<?php } ?>><?php echo Get_Lang_Selection($nature["CPDNatureNameChi"], $nature["CPDNatureNameEn"]); ?></option>
				<?php
						}
					}
				?>
					</select>
				</div>
			</div>
															   
			<div class="form-group">
				<label for="chiContent" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["Content"]; ?> (<?php echo $data["lang"]["TeacherPortfolio"]["lang_chi"]; ?>)</label>
				<div class="col-field">
					<textarea class="form-control inputstl" name="chiContent" rows="4" id="chiContent" required><?php if (isset($data["cpdRecord"]["ContentChi"])) echo $data["cpdRecord"]["ContentChi"]; ?></textarea>
				</div>
			</div>
			
			<div class="form-group">
				<label for="engContent" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["Content"]; ?> (<?php echo $data["lang"]["TeacherPortfolio"]["lang_eng"]; ?>)</label>
				<div class="col-field">
					<textarea class="form-control inputstl" name="engContent" rows="4" id="engContent" required><?php if (isset($data["cpdRecord"]["ContentEn"])) echo $data["cpdRecord"]["ContentEn"]; ?></textarea>
				</div>
			</div>
															   
			<div class="form-group">
				<label for="mode" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["Mode"]; ?></label>
				<div class="col-field">
					<select class="form-control inputstl" id="mode" name="mode" required> 
					<option value=""><?php echo $data["lang"]["TeacherPortfolio"]["pleaseSelect"]; ?></option>
				<?php
					if (count($data["Options"]["modeArr"]) > 0) {
						foreach ($data["Options"]["modeArr"] as $modeID => $mode) {
				?>
					<option value="<?php echo $modeID; ?>"<?php if (isset($data["cpdRecord"]["CPDModeID"]) && $data["cpdRecord"]["CPDModeID"]==$modeID) { ?> selected<?php } ?>><?php echo Get_Lang_Selection($mode["CPDModeNameChi"], $mode["CPDModeNameEn"]); ?></option>
				<?php
						}
					}
				?>
					</select>
				</div>
			</div>
															   
			<div class="form-group">
				<label for="hour" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["Hour"]; ?></label>
				<div class="col-field">
					<input class="form-control short" required name="hour" type="number" value="<?php if (isset($data["cpdRecord"]["TotalHours"])) echo $data["cpdRecord"]["TotalHours"]; ?>" id="hour" min='0'> 
				</div>
			</div>

			<div class="form-group">
				<label for="organizer" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["Organizer"]; ?> (<?php echo $data["lang"]["TeacherPortfolio"]["lang_chi"]; ?>)</label>
				<div class="col-field">
					<input class="form-control" required type="text" name="chiOrganizer" value="<?php if (isset($data["cpdRecord"]["OrganizerNameChi"])) echo $data["cpdRecord"]["OrganizerNameChi"]; ?>" id="organizer">
				</div>
			</div>
			<div class="form-group">
				<label for="organizer" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["Organizer"]; ?> (<?php echo $data["lang"]["TeacherPortfolio"]["lang_eng"]; ?>)</label>
				<div class="col-field">
					<input class="form-control" required type="text" name="engOrganizer" value="<?php if (isset($data["cpdRecord"]["OrganizerNameEn"])) echo $data["cpdRecord"]["OrganizerNameEn"]; ?>" id="organizer">
				</div>
			</div>
															   
			<div class="form-group">
				<label for="chiCert" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["CertificateName"]; ?> (<?php echo $data["lang"]["TeacherPortfolio"]["lang_chi"]; ?>)</label>
				<div class="col-field">
					<input class="form-control" type="text" value="<?php if (isset($data["cpdRecord"]["CertificationNameChi"])) echo $data["cpdRecord"]["CertificationNameChi"]; ?>" name="chiCert" id="chiCert">
				</div>
			</div>
			<div class="form-group">
				<label for="engCert" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["CertificateName"]; ?> (<?php echo $data["lang"]["TeacherPortfolio"]["lang_eng"]; ?>)</label>
				<div class="col-field">
					<input class="form-control" type="text" value="<?php if (isset($data["cpdRecord"]["CertificationNameEn"])) echo $data["cpdRecord"]["CertificationNameEn"]; ?>" name="engCert" id="engCert">
				</div>
			</div>
															   
			<div class="form-group">
				<label for="chiInstitution" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["AwardingInstitution"]; ?> (<?php echo $data["lang"]["TeacherPortfolio"]["lang_chi"]; ?>)</label>
				<div class="col-field">
					<input class="form-control" type="text" value="<?php if (isset($data["cpdRecord"]["AwardingInstitutionChi"])) echo $data["cpdRecord"]["AwardingInstitutionChi"]; ?>" name="chiInstitution" id="chiInstitution">
				</div>
			</div>    
			<div class="form-group">
				<label for="engInstitution" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["AwardingInstitution"]; ?> (<?php echo $data["lang"]["TeacherPortfolio"]["lang_eng"]; ?>)</label>
				<div class="col-field">
					<input class="form-control" type="text" value="<?php if (isset($data["cpdRecord"]["AwardingInstitutionEn"])) echo $data["cpdRecord"]["AwardingInstitutionEn"]; ?>" name="engInstitution" id="engInstitution">
				</div>
			</div>    
															   
			<div class="form-group">
				<label for="area" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["Category"]; ?></label>
				<div class="col-field multiple">
				<?php
					if (count($data["Options"]["categoryArr"]) > 0) {
						foreach ($data["Options"]["categoryArr"] as $cateID => $category) {
				?>
					<label><input type="checkbox" name="Category[]" value='<?php echo $cateID; ?>'<?php if (isset($data["cpdRecord"]["category"]) && in_array($cateID, $data["cpdRecord"]["category"])) echo " checked "; ?>required>
					<span class="custom-check" required></span> <?php echo Get_Lang_Selection($category["CPDCategoryNameChi"], $category["CPDCategoryNameEn"]); ?></label>
				<?php
						}
					}
				?>
				</div>
			</div>
			<div class="form-group buttons">
				<div class="col-form-label"></div>
				<div class="col-field">
					<a href="#" class="btn btn-primary btnSubmit"><?php echo $data["lang"]['TeacherPortfolio']["Submit"]; ?></a>
					<a href="#" rel="<?php echo $data["section"]; ?>" class="btnAjax btn btn-secondary btnBack"><?php echo $data["lang"]['TeacherPortfolio']["Cancel"]; ?></a>
					<?php if ($data["currAction"]=='edit') { ?><a href="#" class="btn btnDel btn-danger" rel="<?php echo $data["section"]; ?>" ><?php echo $data["lang"]['TeacherPortfolio']["Delete"]; ?></a><?php } ?>
				</div>
			</div>
		</div> 
		</form>
		<?php echo $data["defaultFooter"]; ?>
	</div>
	<?php } ?>
</div>
<!--/CONTENT: DEVELOP - NEW-->