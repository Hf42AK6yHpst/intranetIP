<div class="toolbar">
	<ul class="actions">
<?php if ($data["_ROLE_"]["isAdminMode"] && $data["_ROLE_"]["hasAdminRight"] || $data["_ROLE_"]["isOwnerMode"] && $data["_ROLE_"]["hasOwnerRight"] && $data["_ROLE_"]["hasOwnerRight"] && $data["tpConfigs"]["allowOwnerAddOrEditOthers"] == "Y") { ?>
		<li><a href="#" rel="<?php echo $data["section"]; ?>/addrecord" class="btnAjax"><i class="fa fa-plus"></i> <?php echo $data["lang"]["TeacherPortfolio"]["btnAddNew"]; ?></a></li>
<?php } ?>
		<li><a href="#" rel="<?php echo $data["section"]; ?>.exportrecord" class="btnFDAjax"><i class="glyphicon glyphicon-save-file"></i> <?php echo $data["lang"]["TeacherPortfolio"]["btnExport"]; ?></a></li>
<?php /*<li><a href="#" class="btnPrint printAction"><i class="glyphicon glyphicon-print"></i> <?php echo $data["lang"]["TeacherPortfolio"]["btnPrint"]; ?></a></li> */ ?>
		<li><a href="?task=<?php echo $data["section"]; ?>.printall" class="printAllAction"><i class="glyphicon glyphicon-print"></i> <?php echo $data["lang"]["TeacherPortfolio"]["btnPrint"]; ?></a></li>
	</ul>
</div>
<div class="content others">
	<table id="others-bdTable" class="tp-table bdTable table-hover" cellspacing="0" width="100%" <?php if ($data["_ROLE_"]["isAdminMode"] && $data["_ROLE_"]["hasAdminRight"]) { echo "data-searching='Y'"; } ?>>
		<thead>
			<tr>
<?php if ($data["_ROLE_"]["isAdminMode"] && $data["_ROLE_"]["hasAdminRight"]) { ?>
				<th class='tbheader_col' style='width:10%' rel='TeacherName'><?php echo $data["lang"]["TeacherPortfolio"]["TeacherName"]; ?> <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th><!-- use "fa-sort-asc" for ascending order-->
<?php } ?>
				<th class='tbheader_col' style='width:10%' rel='date'><?php echo $data["lang"]["TeacherPortfolio"]["Date"]; ?>  <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th><!-- use "fa-sort-asc" for ascending order-->
				<th class='tbheader_col' style='width:10%' rel='infoName'><?php echo $data["lang"]["TeacherPortfolio"]["Name"]; ?>  <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th>
				<th class='tbheader_col' style='width:10%' rel='infoType'><?php echo $data["lang"]["TeacherPortfolio"]["Type"]; ?>  <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th>
				<th class='tbheader_col' style='width:10%' rel='infoRemark'><?php echo $data["lang"]["TeacherPortfolio"]["Remarks"]; ?>  <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th>
<?php if ($data["_ROLE_"]["isAdminMode"] && $data["_ROLE_"]["hasAdminRight"]) { ?>
				<th class='tbheader_col' width="10%" rel='DateModified'><nobr><?php echo $data["lang"]["TeacherPortfolio"]["LastUpdate"]; ?> <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th>
<?php } ?>
<?php if ($data["_ROLE_"]["isAdminMode"] && $data["_ROLE_"]["hasAdminRight"] || $data["_ROLE_"]["isOwnerMode"] && $data["_ROLE_"]["hasOwnerRight"] && $data["tpConfigs"]["allowOwnerAddOrEditOthers"] == "Y") { ?>
				<th class='tbheader_col' style='width:10%' rel='func'>&nbsp;</th>
<?php } ?>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table><br>
	<iframe id="dfileframe" style="display:none;"></iframe>
	<?php echo $data["defaultFooter"]; ?>
</div>  