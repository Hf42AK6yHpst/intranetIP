<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $data["lang"]["TeacherPortfolio"]["title"]; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<script src="<?php echo $data["AssetsPath"]; ?>/jquery/jquery-3.1.1.min.js"></script>
<link href="<?php echo $data["AssetsPath"]; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<script src="<?php echo $data["AssetsPath"]; ?>/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo $data["AssetsPath"]; ?>/bootstrap/js/bootstrap-notify.min.js"></script>
<script src="<?php echo $data["AssetsPath"]; ?>/bootstrap/js/bootbox.min.js"></script>
<script src="<?php echo $data["AssetsPath"]; ?>/js/plupload/js/plupload.full.min.js"></script>
<link href="<?php echo $data["AssetsPath"]; ?>/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="<?php echo $data["AssetsPath"]; ?>/webfont/Arvo.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo $data["AssetsPath"]; ?>/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="<?php echo $data["AssetsPath"]; ?>/bootstrap-datepicker/bootstrap-datepicker3.css"/>
<link rel="stylesheet" href="<?php echo $data["AssetsPath"]; ?>/css/datatables.min.css"/>
<link rel="stylesheet" href="<?php echo $data["AssetsPath"]; ?>/css/jquery.qtip.min.css"/>
<link href="<?php echo $data["AssetsPath"]; ?>/css/animate.css" rel="stylesheet" />
<script src="<?php echo $data["AssetsPath"]; ?>/js/datatables.min.js"></script>
<script src="<?php echo $data["AssetsPath"]; ?>/js/jquery.qtip.min.js"></script>
<script src="<?php echo $data["AssetsPath"]; ?>/js/jquery-sortable.js"></script>
<script src="<?php echo $data["AssetsPath"]; ?>/js/jquery.fileDownload.js"></script>
<link href="<?php echo $data["AssetsPath"]; ?>/css/style.css" rel="stylesheet" />
<link href="<?php echo $data["AssetsPath"]; ?>/css/tp.css" rel="stylesheet" />
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo $data["AssetsPath"]; ?>/images/favicon/apple-touch-icon-57x57.png" />
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $data["AssetsPath"]; ?>/images/favicon/apple-touch-icon-114x114.png" />
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $data["AssetsPath"]; ?>/images/favicon/apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $data["AssetsPath"]; ?>/images/favicon/apple-touch-icon-144x144.png" />
<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo $data["AssetsPath"]; ?>/images/favicon/apple-touch-icon-60x60.png" />
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo $data["AssetsPath"]; ?>/images/favicon/apple-touch-icon-120x120.png" />
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo $data["AssetsPath"]; ?>/images/favicon/apple-touch-icon-76x76.png" />
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo $data["AssetsPath"]; ?>/images/favicon/apple-touch-icon-152x152.png" />
<link rel="icon" type="image/png" href="<?php echo $data["AssetsPath"]; ?>/images/favicon/favicon-196x196.png" sizes="196x196" />
<link rel="icon" type="image/png" href="<?php echo $data["AssetsPath"]; ?>/images/favicon/favicon-96x96.png" sizes="96x96" />
<link rel="icon" type="image/png" href="<?php echo $data["AssetsPath"]; ?>/images/favicon/favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="<?php echo $data["AssetsPath"]; ?>/images/favicon/favicon-16x16.png" sizes="16x16" />
<link rel="icon" type="image/png" href="<?php echo $data["AssetsPath"]; ?>/images/favicon/favicon-128.png" sizes="128x128" />
<?php
if($data["_CUSTOMIZATION_"]["useEDBCPD"]==true){
?>
<style type="text/css" media="print">
  @page { size: landscape; }
</style>
<?php
}
?>
</head>
<body class="printWrap" onLoad="window.print();">
	<table class="pageheader">
	<thead>
	<tr>
		<th><div class="tp-header">
			<span class="logo"><?php echo $data["lang"]["TeacherPortfolio"]["ModuleTitle"]; ?></span>
			<span class="gradient"></span>
		</div>
		<br>
		<div class="top pringAllTable">
			<ol class='breadcrumb'>
				<li class="breadcrumb-item"><?php echo $data["lang"]["TeacherPortfolio"]["ModuleTitle"]; ?></li>
<?php if ($data["section"] == "manage") { ?>
				<li class="breadcrumb-item"><?php echo $data["lang"]["TeacherPortfolio"]["sec_" . $data["section"]]; ?></li>
				<li class="breadcrumb-item active text-warning"><?php echo $data["lang"]["TeacherPortfolio"]["manage_logRecord"]; ?></li>
<?php } else { ?>
				<li class="breadcrumb-item active text-warning"><?php echo $data["lang"]["TeacherPortfolio"]["sec_" . $data["section"]]; ?></li>
<?php } ?>
			</ol>
		</div>
		</th>
	</tr>
	</thead>
	<tbody>
	<tr>
		<td><div style="clear:both;" class="printWrap">
		<div class='pringAllTable'>
			<table class="table table-condensed" width="100%">
			<thead>
				<tr role="row" valign="top">
<?php
		if (count($data["jsonData"]) > 1) {
			foreach ($data["jsonData"][0] as $kk => $vv) {
					$headerStyle = "width='".(100/count($data["jsonData"][0]))."%'";
?>
					<th class="tbheader_col" <?php echo $headerStyle;?>><?php echo $vv; ?></th>
<?php
			}
		}
?>
				</tr>
			</thead>
			<tbody>
<?php
		if (count($data["jsonData"]) > 1) {
			$header = $data["jsonData"][0];
			unset($data["jsonData"][0]);
			foreach ($data["jsonData"] as $kk => $rolData) {
				if (count($rolData) > 0) {
					echo "<tr>";
					foreach ($rolData as $recIndex => $recData) {
						if (is_null($recData)) $recData = " ";
						if (empty($recData) && !is_null($recData)) $recData = "-";
						echo "<td>" . nl2br($recData) . "</td>";
					}
					echo "</tr>";
				}
			}
		}
?>
			</tbody>
			</table>
			<?php if (($data["section"]=="cpd") && ($data["_CUSTOMIZATION_"]["ltmpsStyle"]==true)){
			?>
			<b>註（Ⅰ）專業進修範疇</b><br>
			<ol>
			 	<li>教與學方面的進修： 學科內容知識 / 課程及教學內容知識  / 教學策略 / 評核及評估</li>
			    <li>學生發展方面的進修： 學生在校內的不同需要  / 與學生建立互信關係  / 學生關顧  / 學生的多元學習經歷</li>
			    <li>學校發展的進修： 學校願景，文化及校風 / 校政 / 家庭與學校協作 / 回應社會變革</li>
			    <li>專業群體關係及服務方面的進修： 校內協作關係  / 教師專業發展 / 教育政策的參與 / 與教育有關的社區服務及志願工作</li>
			    <li>教師成長及發展的進修： 非與教學直接相關，但有助教師作全人發展的活動 （e.g. 減壓工作坊，領導才能訓練等）</li>
			</ol>         
			<b>註（Ⅱ）配合學校關注事項</b>
			<?php	
			}?>
			<div style='text-align:right;'>
				Powered by <img src="../../../../home/eAdmin/StaffMgmt/teacher_portfolio/assets/images/logo_eclass_footer.gif" width="39" height="15" border="0" align="absmiddle">
			</div>
		</div>
	</div></td>
	</tr>
	</tbody>
	</table><br><br>
</body>
</html>