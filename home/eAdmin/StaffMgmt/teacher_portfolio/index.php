<?php
define('TEACHER_PORTFOLIO', TRUE);
$time_start = microtime(true);
/*******************************************/
/* Application Core
/*******************************************/
if (file_exists(dirname(__FILE__) . "/tb_path.php")) include( dirname(__FILE__) . "/tb_path.php" );
if (file_exists(dirname(__FILE__) . "/teacher_portfolio.php")) include( dirname(__FILE__) . "/teacher_portfolio.php" );
$tb = new TBPath(null);
$tb->pathInfos(__FILE__);
$pathInfo = $tb->getUrlInfo();
unset($tb);
$PATH_WRT_ROOT = rtrim($pathInfo["PATH_WRT_ROOT"], "/") . "/";

if (file_exists($PATH_WRT_ROOT . "includes/teacher_portfolio/libtbcore.php")) include($PATH_WRT_ROOT . "includes/teacher_portfolio/libtbcore.php");

$tpMvcCore = new libtbcore();

$config_file = "teacher_portfolio.php";
$tpMvcCore->setPath($pathInfo);

$PATH_WRT_ROOT = $tpMvcCore->pathInfo["PATH_WRT_ROOT"];
$TeacherPortfolioConfigs = array(
	"taskSeparator" => "."
);
/*******************************************/
/* System Libraries
/*******************************************/
if (file_exists($PATH_WRT_ROOT."includes/global.php")) include_once($PATH_WRT_ROOT."includes/global.php");
if (file_exists($PATH_WRT_ROOT."includes/json.php")) include_once($PATH_WRT_ROOT."includes/json.php");
/*******************************************/
$tpMvcCore->load_config($config_file);

if (defined('isDev')) {
	$intranet_db = $tpMvcCore->getDBLink();
}
/*******************************************/
if (file_exists($PATH_WRT_ROOT."includes/libdb.php")) include_once($PATH_WRT_ROOT."includes/libdb.php");
/*******************************************/

if (isset($_SESSION["TPSession"]["language"])) {
	// $intranet_session_language = $_SESSION["TPSession"]["language"];
} else {
	if (!isset($intranet_session_language)) $intranet_session_language = "b5";
}

if (strpos($_GET["task"], "exportrecord") !== false) {
	if (file_exists($PATH_WRT_ROOT."lang/teacher_portfolio_lang.$intranet_session_language.php")) include_once($PATH_WRT_ROOT."lang/teacher_portfolio_lang.en.php");
} else {
	if (file_exists($PATH_WRT_ROOT."lang/teacher_portfolio_lang.$intranet_session_language.php")) include_once($PATH_WRT_ROOT."lang/teacher_portfolio_lang.$intranet_session_language.php");
}
/*******************************************/
/* URL Link Separator */
/*******************************************/
if (!isset($slrsConfig['taskSeparator'])) $slrsConfig['taskSeparator'] = ".";
// if (function_exists("intranet_auth")) intranet_auth();
if (function_exists("intranet_opendb")) intranet_opendb();
// $this->returnResultSet($sql);
/*******************************************/
$tpMvcCore->setTeacherPortfolioConfigs($TeacherPortfolioConfigs);
$tpMvcCore->setCurrentRootFolder($PATH_WRT_ROOT);
if (isset($Lang)) $tpMvcCore->setLangArr($Lang);
$tp_userInfo = array(
				"language" => $intranet_session_language
			);
$tpMvcCore->userValid($tp_userInfo);
$tpMvcCore->run(false);
?>