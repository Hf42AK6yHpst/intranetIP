<?php
if (!defined('TEACHER_PORTFOLIO')) die('Page not found');
global $config_school_code;

$_configs["autoLoad"]["helpers"] = array( "common" );
$_configs["autoLoad"]["models"] = array( "YearObj", "UserObj" );
// $_configs["default_section"] = array( "profile", "teach-record", "activities", "attendance", "develop", "performance", "cert", "others" );
// $_configs["sys_section"] = array( "manage", "activities", "attendance", "develop", "performance" );
$_configs["default_section"] = array( "profile", "teaching_record", "activities", "attendance", "cpd", "performance", "cert", "others" );
$_configs["sys_section"] = array( "teaching_record", "activities", "attendance", "cpd", "performance", "cert", "others", "settings" );
$_configs['passphrase'] = 'iTBisVERYsecure';
$_configs['masterKey'] = 'g0=!sBp{v702a&!' . $config_school_code . 'BHr1<7>S3f';

