<?php
if (!defined('TEACHER_PORTFOLIO')) die('Page not found');
if (!class_exists("FormTemplateController")) include_once(dirname(__FILE__) . "/formtemplate.controller.php");
class Teaching_record extends FormTemplateController {
	public function __construct($parent) {
		parent::__construct($parent);
		$this->parent->data["section"] = "teaching_record";
		$this->parent->data["template"] = "TEACHING_RECORD";
		$this->parent->load_model('FormTemplateObj');
		
		/***********************************************************/
		$this->parent->data["TemplateType"] = "GRID";
		$this->parent->data["TemplateGridTable"] = "INTRANET_TEACHER_PORTFOLIO_FORM_DATA_GRID_" . $this->parent->data["template"];
		$this->parent->data["TemplateGridTableStructure"] = array(
				"FormTemplateID" => array( "field" => "FormTemplateID", "data" => "", "output" => false),
				"UserID" => array( "field" => "UserID", "groupBY" => true, "data" => "", "output" => false),
				"AcademicYear" => array( "field" => "AcademicYear", "groupBY" => true, "data" => "", "output" => true),
				"ClassNameEn" => array( "field" => "ClassNameEn", "action" => "COUNT", "groupBY" => false, "data" => "", "output" => true),
				"ClassNameChi" => array( "field" => "ClassNameChi", "action" => "COUNT", "groupBY" => false, "data" => "", "output" => true),
				"SubjectEn" => array( "field" => "SubjectEn", "groupBY" => false, "action" => "CONCAT", "data" => "", "output" => true),
				"SubjectChi" => array( "field" => "SubjectChi", "groupBY" => false, "action" => "CONCAT", "data" => "", "output" => true),
				"NoOfSection" => array( "field" => "NoOfSection", "groupBY" => false, "action" => "SUM", "data" => "", "output" => true),
				"RemarksEn" => array( "field" => "RemarksEn", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
				"RemarksChi" => array( "field" => "RemarksChi", "groupBY" => false, "action" => "EMPTY", "data" => "", "output" => true),
				"DateInput" => array( "field" => "DateInput", "groupBY" => false, "data" => "__dbnow__", "output" => false),
				"InputBy" => array( "field" => "InputBy", "groupBY" => false, "data" => "", "output" => false),
				"DateModified" => array( "field" => "DateModified", "groupBY" => false, "data" => "__dbnow__", "output" => false),
				"ModifyBy" => array( "field" => "ModifyBy", "groupBY" => false, "data" => "", "output" => false)
		);
		/***********************************************************/
		
		$predefinedArr = array(
			"UserID" => array(
				"FieldLabel" => "UserID",
				"FieldNameChi" => "內聯網帳號",
				"FieldNameEn" => "Teacher Login ID",
				"isMultiLang" => "N",
				"SampleDataChi"=> "tchr01",
				"SampleData"=> "tchr01",
				"FieldType"=> "INT",
				"FixedItem" => "Y",
				"FieldStatus" => "required",
				"showInGrid" => "N",
				"isUsed" => "Y",
				"Priority" => "1"
			),
			"TeacherName" => array(
				"FieldLabel" => "TeacherName",
				"FieldNameChi" => "教師名稱",
				"FieldNameEn" => "Teacher Name",
				"isMultiLang" => "Y",
				"SampleDataChi"=> "陳大文",
				"SampleData"=> "Chan Ta Man",
				"FieldType"=> "TEXT",
				"FixedItem" => "Y",
				"FieldStatus" => "reference",
				"showInGrid" => "N",
				"isUsed" => "Y",
				"Priority" => "2"
			),
			"AcademicYear" => array(
				"FieldLabel" => "AcademicYear",
				"FieldNameChi" => "學年",
				"FieldNameEn" => "School Year",
				"isMultiLang" => "N",
				"SampleDataChi"=> "2016-2017",
				"SampleData"=> "2016-2017",
				"FieldType"=> "TEXT",
				"FixedItem" => "N",
				"FieldStatus" => "required",
				"showInGrid" => "Y",
				"isUsed" => "Y",
				"Priority" => "3"
			),
			"ClassName" => array(
				"FieldLabel" => "ClassName",
				"FieldNameChi" => "班別",
				"FieldNameEn" => "Class Name",
				"isMultiLang" => "Y",
				"SampleDataChi"=> "1A, 1B",
				"SampleData"=> "1A, 1B",
				"FieldType"=> "TEXT",
				"FixedItem" => "N",
				"FieldStatus" => "required",
				"showInGrid" => "Y",
				"isUsed" => "Y",
				"Priority" => "4"
			),
			"Subject" => array(
				"FieldLabel" => "Subject",
				"FieldNameChi" => "科目",
				"FieldNameEn" => "Subject",
				"isMultiLang" => "Y",
				"SampleDataChi"=> "數學",
				"SampleData"=> "Math",
				"FieldType"=> "TEXT",
				"FixedItem" => "N",
				"FieldStatus" => "required",
				"showInGrid" => "Y",
				"isUsed" => "Y",
				"Priority" => "5"
			),
			"NoOfSection" => array(
				"FieldLabel" => "NoOfSection",
				"FieldNameChi" => "節數",
				"FieldNameEn" => "No. of Section",
				"isMultiLang" => "N",
				"SampleDataChi"=> "1",
				"SampleData"=> "1",
				"FieldType"=> "INT",
				"FixedItem" => "N",
				"FieldStatus" => "required",
				"showInGrid" => "Y",
				"isUsed" => "Y",
				"Priority" => "6"
			),
			"Remarks" => array(
				"FieldLabel" => "Remarks",
				"FieldNameChi" => "備註",
				"FieldNameEn" => "Remarks",
				"isMultiLang" => "Y",
				"SampleDataChi"=> "備註",
				"SampleData"=> "Remarks",
				"FieldType"=> "TEXT",
				"FixedItem" => "N",
				"FieldStatus" => "normal",
				"showInGrid" => "Y",
				"isUsed" => "Y",
				"Priority" => "7"
			)
		);

		$this->parent->FormTemplateObj->setCoreData($this->parent->data);
		$this->parent->data["formTplInfo"] = $this->parent->FormTemplateObj->initTemplate($this->parent->data["template"], $predefinedArr);
		if ($this->parent->data["formTplInfo"] == NULL) {
			echo "Template Init Error";
			exit;
		}
		$this->parent->data["yearArr"] = $this->parent->FormTemplateObj->getAcademicYear();
		$this->parent->data["selectedYear"] = isset($this->parent->data["postData"]["selectedYear"]) ? $this->parent->data["postData"]["selectedYear"] : "";
		$this->parent->data["html_YearPanel"] = $this->parent->YearObj->getYearPanel($this->parent->data["yearArr"], $this->parent->data["lang"], $this->parent->data["section"], $this->parent->data["selectedYear"]);
	}
}
?>