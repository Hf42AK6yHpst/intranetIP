<?php
if (!defined('TEACHER_PORTFOLIO')) die('Page not found');
class Profile extends TeacherPortfolio {
	public function __construct($parent) {
		parent::__construct($parent);
		$this->parent = $parent;
		$this->parent->data["section"] = "profile";
	}

	public function index() {
		if ($this->parent->configs["isDev"]) {
			$this->parent->data["debugMsg"] = $this->parent->getMemoryInfo();
		}
		
		$this->parent->data["profile"] = $this->parent->UserObj->getProfile($this->parent->data["userInfo"]);
		$this->parent->tpl->view($this->parent->data["section"] . "/list", $this->parent->data);
	}
	
	public function exportrecord() {
		$this->parent->data["currMethod"] = __FUNCTION__;
		$this->parent->load_model('CSVObj');
		$this->parent->UserObj->setCore($this->parent);
		$this->parent->UserObj->exportAllData($this->parent->CSVObj, $this->parent->data["userInfo"]);
	}
}
?>