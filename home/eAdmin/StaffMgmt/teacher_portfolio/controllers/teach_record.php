<?php
if (!defined('TEACHER_PORTFOLIO')) die('Page not found');
class Teach_record extends TeacherPortfolio {
	public function __construct($parent) {
		parent::__construct($parent);
		$this->parent->data["section"] = "teach_record";
		$this->parent->data["yearArr"] = $this->parent->YearObj->getAcademicYear();
		$this->parent->data["selectedYear"] = isset($this->parent->data["postData"]["selectedYear"]) ? $this->parent->data["postData"]["selectedYear"] : "";
		$this->parent->data["html_YearPanel"] = $this->parent->YearObj->getYearPanel($this->parent->data["yearArr"], $this->parent->data["lang"], $this->parent->data["section"], $this->parent->data["selectedYear"]);
		$this->parent->load_model('TeachRecordObj', "myOBJ");
	}

	public function index() {
		$this->parent->data["currTable"] = "class";
		if ($this->parent->configs["isDev"]) {
			$this->parent->data["debugMsg"] = $this->parent->getMemoryInfo();
		}
		$this->parent->tpl->view($this->parent->data["section"] . "/list", $this->parent->data);
	}
	
	public function subjectlist() {
		$this->parent->data["currTable"] = "subject";
		if ($this->parent->configs["isDev"]) {
			$this->parent->data["debugMsg"] = $this->parent->getMemoryInfo();
		}
		$this->parent->tpl->view($this->parent->data["section"] . "/subjectlist", $this->parent->data);
	}
	
	public function listjson() {
		$this->parent->data["postData"]["tableType"] = "class";
		$jsonData = $this->parent->myOBJ->getDataTableArray($this->parent->data["userInfo"]["info"]["SelectedUserID"], $this->parent->data["postData"]);
		$jsonObj = new JSON_obj();
		echo $jsonObj->encode($jsonData);
	}
	
	public function sjListJson() {
		$this->parent->data["postData"]["tableType"] = "subject";
		$jsonData = $this->parent->myOBJ->getDataTableArray($this->parent->data["userInfo"]["info"]["SelectedUserID"], $this->parent->data["postData"]);
		$jsonObj = new JSON_obj();
		echo $jsonObj->encode($jsonData);
	}
	
	public function exportrecord() {
		$this->parent->data["currMethod"] = __FUNCTION__;
		$this->parent->load_model('CSVObj');
		$this->parent->myOBJ->exportAllData($this->parent->CSVObj, $this->parent->data["userInfo"]["info"]["SelectedUserID"], $this->parent->data["getData"]["sec"]);
	}
}
?>