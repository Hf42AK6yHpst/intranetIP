<?php
if (!defined('TEACHER_PORTFOLIO')) die('Page not found');
class Nav extends TeacherPortfolio {
	public function __construct($parent) {
		parent::__construct($parent);
		$this->parent->data["section"] = "nav";
	}
	
	public function changelang() {
		if (isset($_SESSION)) {
			$_custom_session = $_SESSION["TPSession"];
			if (isset($this->parent->data["getData"]["lg"])) $custom_lang = $this->parent->data["getData"]["lg"];
			else $custom_lang = "en";
			switch ($custom_lang) {
				case "b5":
					$_custom_session["language"] = "b5";
					break;
				default:
					$_custom_session["language"] = "en";
					break;
			}
		}
		$_SESSION["TPSession"] = $_custom_session;
		$url = "index.php";
		if (isset($this->parent->data["getData"]["section"])) {
			$url .= "#" . $this->parent->data["getData"]["section"];
		}
		header("Location: " . $url);
		exit;
	}
	
	public function changemode() {
		if (isset($_SESSION)) {
			if ($this->parent->hasAdminRight() || $this->parent->hasViewerRight()) {
				$_custom_session = $_SESSION["TPSession"];
				if (isset($this->parent->data["getData"]["lg"])) $custom_mode = $this->parent->data["getData"]["lg"];
				else $custom_mode = "owner";
				switch ($custom_mode) {
					case "sys":
					case "reader":
						$_custom_session["currMode"] = $custom_mode;
						break;
					default:
						$_custom_session["currMode"] = "owner";
						break;
				}
			}
			if ($this->parent->checkAccessRight() === NULL) {
				No_Access_Right_Pop_Up();
				exit;
			}
		}
		if (in_array($custom_mode, $_custom_session["allowMode"])) {
			if ($_custom_session["currMode"] == "owner") {
				$_custom_session["info"]["SelectedUserID"] = $_custom_session["info"]["UserID"];
			}
			$_SESSION["TPSession"] = $_custom_session;
		}
		$url = "index.php";
		$selectedSection = "";
		if (isset($this->parent->data["getData"]["section"])) {
			$selectedSection = $this->parent->data["getData"]["section"];
		}
		if (!in_array($selectedSection, $this->parent->userInfo["grantSection"])) {
			$selectedSection = $this->parent->userInfo["grantSection"][0];
		}
		if (!empty($selectedSection)) {
			// $url .= "#" . $selectedSection;
		}
		header("Location: " . $url);
		exit;
	}
	
	public function changethr() {
		if (isset($_SESSION)) {
			if ($this->parent->hasViewerRight()) {
				$_custom_session = $_SESSION["TPSession"];
				if (isset($this->parent->data["getData"]["lg"]) && isset($this->parent->data["postData"]["thr"]) && $this->parent->data["getData"]["lg"] == $this->parent->data["postData"]["thr"]) {
					$custom_mode = explode("_", $this->parent->data["getData"]["lg"]);
					if (count($custom_mode) == 2) {
						$userInfo = $this->parent->UserObj->getInfoByID($custom_mode[0]);
						if ($userInfo != NULL && isset($userInfo[$custom_mode[0]])) {
							if ($userInfo[$custom_mode[0]]["cs"] == $custom_mode[1]) {
								$_custom_session["info"]["SelectedUserID"] = $custom_mode[0];
								$_SESSION["TPSession"] = $_custom_session;
							}
						}
					}
				}
			}
		}
		echo '{"jsonrpc" : "2.0", "result" : "done", "id" : "id"}';
		exit;
	}
}