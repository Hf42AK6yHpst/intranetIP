<?php
if (!defined('TEACHER_PORTFOLIO')) die('Page not found');
class Develop extends TeacherPortfolio {
	public function __construct($parent) {
		parent::__construct($parent);
		$this->parent->data["section"] = "develop";
		$this->parent->data["yearTable"] = $this->parent->YearObj->getMainTable();
		$this->parent->data["yearArr"] = $this->parent->YearObj->getAcademicYear();
		$this->parent->data["selectedYear"] = isset($this->parent->data["postData"]["selectedYear"]) ? $this->parent->data["postData"]["selectedYear"] : "";
		$this->parent->data["html_YearPanel"] = $this->parent->YearObj->getYearPanel($this->parent->data["yearArr"], $this->parent->data["lang"], $this->parent->data["section"], $this->parent->data["selectedYear"]);
		
		$this->parent->load_model('CpdObj', "myOBJ");
		$options = array("category", "mode", "nature" );
		$this->parent->data["Options"] = $this->parent->myOBJ->getOptions($options);
		$this->parent->data["settings_menu"] = array(
			"category" => $this->parent->data["lang"]["TeacherPortfolio"]["Category"],
			"mode" => $this->parent->data["lang"]["TeacherPortfolio"]["Mode"],
			"nature" => $this->parent->data["lang"]["TeacherPortfolio"]["Nature"]
		);
	}

	public function index() {
		if ($this->parent->isAdmin()) {
			$this->settings();
			exit;
		}
		if ($this->parent->configs["isDev"]) {
			$this->parent->data["debugMsg"] = $this->parent->getMemoryInfo();
		}
		$this->parent->tpl->view($this->parent->data["section"] . "/list", $this->parent->data);
	}
	

	public function listjson() {
		if (count($this->parent->data["postData"]) > 0) {
			$jsonData = $this->parent->myOBJ->getDataTableArray($this->parent->data["userInfo"]["info"]["SelectedUserID"], $this->parent->data["postData"]);
			$jsonData["debugInfo"] = $this->parent->getMemoryInfo(false);
			$jsonObj = new JSON_obj();
			echo $jsonObj->encode($jsonData);
		} else {
			echo "Page not found";
		}
	}
	
	public function addrecord() {
		$this->parent->data["currAction"] = "add";
		if ($this->parent->configs["isDev"]) {
			$this->parent->data["debugMsg"] = $this->parent->getMemoryInfo();
		}
		$this->parent->tpl->view($this->parent->data["section"] . "/edit", $this->parent->data);
	}
	
	public function editrecord() {
		$this->parent->data["currAction"] = "edit";
		if ($this->parent->configs["isDev"]) {
			$this->parent->data["debugMsg"] = $this->parent->getMemoryInfo();
		}
		$this->parent->data["cpdRecord"] = $this->parent->myOBJ->getRecordByID($this->parent->data["postData"]["CpdID"]);
		if (!$this->parent->data["cpdRecord"]) {
			$this->parent->data["cpdRecord"] = "no record";
		}
		$this->parent->tpl->view($this->parent->data["section"] . "/edit", $this->parent->data);
	}
	
	public function deleterecord() {
		$this->parent->data["currAction"] = "delete";
		if (isset($this->parent->data["postData"]["recid"])) {
			$recid = $this->parent->data["postData"]["recid"];
		} else if (isset($this->parent->data["postData"]["CpdID"])) {
			$recid = $this->parent->data["postData"]["CpdID"];
		}
		$data = array(
			"result" => "failed"
		);
		if ($recid > 0) {
			if ($this->parent->myOBJ->deleteRecord($recid)) {
				$data["result"] = "success";
			}
		}
		if ($this->parent->configs["isDev"]) {
			$this->parent->data["debugMsg"] = $this->parent->getMemoryInfo();
		}
		$jsonObj = new JSON_obj();
		echo $jsonObj->encode($data);
	}
	
	public function process() {
		$dataMapping = $this->parent->myOBJ->getMappingInfo();
		$processData = $this->mapConvert($this->parent->data["postData"], $dataMapping);
		switch ($this->parent->data["postData"]["action"]) {
			case "insertrecord":
				$myaction = "INSERT";
				$id = null;
				break;
			case "updaterecord":
				$myaction = "UPDATE";
				$id = $this->parent->data["postData"]["recid"];
				break;
		}
		$result = $this->parent->myOBJ->dataHandler($this->parent->data["userInfo"], $processData, $myaction, $id);
		$jsonObj = new JSON_obj();
		echo $jsonObj->encode($result);
	}
	
	public function exportrecord() {
		$this->parent->data["currMethod"] = __FUNCTION__;
		$this->parent->load_model('CSVObj');
		$this->parent->myOBJ->exportAllData($this->parent->CSVObj, $this->parent->data["userInfo"]["info"]["SelectedUserID"]);
	}
	
	public function settings() {
		if (!$this->parent->isAdmin()) {
			No_Access_Right_Pop_Up();
			exit;
		}
		$this->parent->data["currMethod"] = __FUNCTION__;
		if (!isset($this->parent->data["getData"]["otype"])) $this->parent->data["selectedType"] = "category";
		else {
			switch ($this->parent->data["getData"]["otype"]) {
				case "mode":
				case "nature":
					$this->parent->data["selectedType"] = $this->parent->data["getData"]["otype"];
					break;
				default:
					$this->parent->data["selectedType"] = "category";
					break;
			}
		}
		if ($this->parent->configs["isDev"]) {
			$this->parent->data["debugMsg"] = $this->parent->getMemoryInfo();
		}
		$this->parent->tpl->view($this->parent->data["section"] . "/settings", $this->parent->data);
	}
	
	public function updatesort() {
		if (!$this->parent->isAdmin()) {
			No_Access_Right_Pop_Up();
			exit;
		}
		$this->parent->data["currMethod"] = __FUNCTION__;
		$recid = "";
		$option_type = "";
		if (isset($this->parent->data["postData"]["recid"])) {
			$recid = $this->parent->data["postData"]["recid"];
		}
		if (isset($this->parent->data["getData"]["custom"])) {
			$option_type = $this->parent->data["getData"]["custom"];
		}
		$data = array(
			"result" => "failed"
		);
		if ($recid > 0 && !empty($option_type)) {
			if ($this->parent->myOBJ->updateSortRecords($this->parent->data["userInfo"], $recid, $option_type)) {
				$data["result"] = "success";
			}
		}
		if ($this->parent->configs["isDev"]) {
			$this->parent->data["debugMsg"] = $this->parent->getMemoryInfo();
		}
		$jsonObj = new JSON_obj();
		echo $jsonObj->encode($data);
	}
	
	public function optionsmanage() {
		if (!$this->parent->isAdmin()) {
			No_Access_Right_Pop_Up();
			exit;
		}
		$data = array(
			"result" => "failed"
		);
		$manage_result = $this->parent->myOBJ->optionsManage($this->parent->data["userInfo"], $this->parent->data["postData"]);
		if ($manage_result !== false) {
			$data = $manage_result;
		}
		$jsonObj = new JSON_obj();
		echo $jsonObj->encode($data);
	}
}
?> 