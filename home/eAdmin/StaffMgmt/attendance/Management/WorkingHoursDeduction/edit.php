<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-OT'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0) || !$sys_custom['StaffAttendance']['WongFutNamCollege']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}
$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();
//debug_pr($RecordID);
$is_edit = isset($RecordID) && count($RecordID)>0 ? 1: 0;

if($is_edit){
	$RecordID = explode(",",$RecordID[0]);

	$records = $StaffAttend3->Get_Working_Hours_Deduction_Records(array('RecordID'=>$RecordID));
	//debug_pr($records);
	$RecordID = Get_Array_By_Key($records,'RecordID');
	$StaffName = $records[0]['StaffName'];
	$StaffID = $records[0]['StaffID'];
	$DeductMin = $records[0]['DeductMin'];
	$Remark = $records[0]['Remark'];
	$RecordDate = $records[0]['RecordDate'];
	$ts = strtotime($RecordDate);
	$TargetYear = date("Y", $ts);
	$TargetMonth = date("m", $ts);
	$TargetDates = Get_Array_By_Key($records,'RecordDate');
}else{
	$DeductMin = 60;
	$TargetYear = date("Y");
	$TargetMonth = date("m");
	$TargetDates = array();
}

$hidden_id_fields = '';
if($is_edit){
	for($i=0;$i<count($RecordID);$i++)
	{
		$hidden_id_fields .= '<input type="hidden" name="RecordID[]" value="'.$RecordID[$i].'" />';
	}
}

if($is_edit)
{
	$staff_selection = $StaffName.'<input type="hidden" name="StaffID[]" id="StaffID" value="'.$StaffID.'" />';
}else{
	$staff_selection = $StaffAttend3UI->Get_Staff_Selection('StaffID[]','StaffID',$__ParGroup=true,$__ParIndividual=true,$__ParIsMultiple=true,$__ParSize=20,$__ParSelectedValue="",$__ParOthers="",$__ParAllStaffOption=false,$__ShowOptGroupLabel=true);
	$staff_selection.= '&nbsp;'.$StaffAttend3UI->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "Select_All_Options('StaffID',true);");
}

$calendar = $StaffAttend3UI->Get_Shifting_Target_Date_Calendar($TargetMonth,$TargetYear,$TargetDates,true,!$is_edit);

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['WorkingHoursDeduction'] = 1;

$pages_arr = array(
	array($Lang['StaffAttendance']['WorkingHoursDeduction'],'./'),
	array($is_edit? $Lang['Btn']['Edit'] : $Lang['Btn']['New'], '')
);

$TAGS_OBJ[] = array($Lang['StaffAttendance']['WorkingHoursDeduction'], "", 0);
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));
echo $StaffAttend3UI->Include_JS_CSS();
?>
<?=$linterface->GET_NAVIGATION_IP25($pages_arr)?>
<br />
<form name="form1" id="form1" action="edit_update.php" method="post" onsubmit="return false;">
<?=$hidden_id_fields?>
<table width="100%" cellpadding="2" class="form_table_v30">
	<thead>
		<col class="field_title">
		<col class="field_c">
	</thead>
	<tbody>
		<tr>
			<td width="30%" valign="top" nowrap="nowrap" class="field_title">
				<?=$Lang['StaffAttendance']['Staff']?> <span class="tabletextrequire">*</span>
			</td>
			<td>
				<?=$staff_selection?>
				<?=$linterface->Get_Thickbox_Warning_Msg_Div("StaffID_Error",$Lang['StaffAttendance']['EntryLogSelectStaffWarning'], "WarnMsg")?>
			</td>
		</tr>
		<tr>
			<td width="30%" valign="top" nowrap="nowrap" class="field_title">
				<?=$Lang['General']['Date']?> <span class="tabletextrequire">*</span>
			</td>
			<td>
				<div id="DateDiv"><?=$calendar?></div>
				<?=$linterface->Get_Thickbox_Warning_Msg_Div("TargetDate_Error",$Lang['StaffAttendance']['TargetDateSelectWarning'], "WarnMsg")?>
			</td>
		</tr>
		<tr>
			<td width="30%" valign="top" nowrap="nowrap" class="field_title">
				<?=$Lang['StaffAttendance']['DeductHours']?> <span class="tabletextrequire">*</span>
			</td>
			<td>
				<?=$linterface->GET_TEXTBOX_NUMBER("DeductMin", "DeductMin", $DeductMin, '', array('onchange'=>'restrictNonZeroPositiveNumber(this);')).'&nbsp;'.$Lang['StaffAttendance']['Mins']?>
			</td>
		</tr>
		<tr>
			<td width="30%" valign="top" nowrap="nowrap" class="field_title">
				<?=$Lang['General']['Remark']?>
			</td>
			<td><?=$linterface->GET_TEXTAREA('Remark', $Remark)?></td>
		</tr>
	</tbody>
</table>
<?=$linterface->MandatoryField()?>		
<div class="edit_bottom_v30">
<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'],"button", "checkSubmitForm(document.form1);",'submitBtn').'&nbsp;'?>
<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.location.href='./'",'cancelBtn')?>
</div>
</form>
<script type="text/javascript" language="javascript">
function Check_All(CheckStatus,ObjClassName) 
{
	$('.'+ObjClassName).each(
		function () {
			this.checked = CheckStatus;
		}
	);
}

function Get_Month_Calendar(Month,Year)
{
	var PostVar = {
		"task":"getMonthCalendar",
		"TargetMonth":Month,
		"TargetYear":Year
	};
	
	$.post(
		'ajax_task.php',
		PostVar,
		function (data) {
			$('#DateDiv').html(data);
		}
	);
}

function restrictNonZeroPositiveNumber(obj)
{
	var val = $.trim(obj.value);
	var num = parseInt(val);
	
	if(isNaN(num) || val == ''){
		num = 60;
	}
	if(num <= 0){
		num = 60;
	}
	obj.value = num;
}

function checkSubmitForm(formObj)
{
<?php if($is_edit){ ?>
	var is_valid = true;
	var selected_date_count = $('input[name="TargetDate[]"]:checked').length;
	$('.WarnMsg').hide();
	if(selected_date_count == 0){
		is_valid = false;
		$('#TargetDate_Error').show();
	}
	
	if(is_valid){
		formObj.submit();
	}
<?php }else{ ?>
	var is_valid = true;
	var selected_staff_count = $('#StaffID option:selected').length;
	var selected_date_count = $('input[name="TargetDate[]"]:checked').length;
	$('.WarnMsg').hide();
	if(selected_staff_count == 0){
		is_valid = false;
		$('#StaffID_Error').show();
	}
	if(selected_date_count == 0){
		is_valid = false;
		$('#TargetDate_Error').show();
	}
	
	if(is_valid){
		formObj.submit();
	}
<?php } ?>	
}
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>