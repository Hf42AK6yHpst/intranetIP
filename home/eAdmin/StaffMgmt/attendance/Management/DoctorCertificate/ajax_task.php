<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if (!$sys_custom['StaffAttendance']['DoctorCertificate'] || (!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-DOCTORCERTIFICATE'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

switch($task)
{
	case "Delete_Doctor_Certificate":
		$RecordID = $_REQUEST['RecordID'];
		$TargetYear = $_REQUEST['TargetYear'];
		$TargetMonth = $_REQUEST['TargetMonth'];
		$success = $StaffAttend3->Delete_Doctor_Certificates($RecordID,$TargetYear,$TargetMonth);
		if($success){
			echo $Lang['General']['ReturnMessage']['DeleteSuccess'];
		}else{
			echo $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
		}
	break;
	
	case "Delete_Doctor_Certificate_By_FileID":
		$FileID = $_REQUEST['FileID'];
		$success = $StaffAttend3->Delete_Doctor_Certificate_By_FileID($FileID);
		if($success){
			echo $Lang['General']['ReturnMessage']['DeleteSuccess'];
		}else{
			echo $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
		}
	break;
	
	case "Manage_Doctor_Certificate_Files":
		$success = $StaffAttend3->Manage_Doctor_Certificate_Files($_REQUEST,$_FILES);
		
		if($success){
			$msg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
		}else{
			$msg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
		}
		
		echo '<script>';
		echo 'if(window.top.FinishManageDoctorCertificateFiles){
				window.top.FinishManageDoctorCertificateFiles("'.$msg.'");
				}';
		echo '</script>';
	break;
}

intranet_closedb();
?>