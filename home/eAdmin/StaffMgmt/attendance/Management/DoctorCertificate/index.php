<?php
// editing by 
/***************************************** Change log *****************************************************
 *
 **********************************************************************************************************/
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$arrCookies = array();
$arrCookies[] = array("ck_staff_attendance_doctor_cert_page_size", "numPerPage");
$arrCookies[] = array("ck_staff_attendance_doctor_cert_page_number", "pageNo");
$arrCookies[] = array("ck_staff_attendance_doctor_cert_page_order", "order");
$arrCookies[] = array("ck_staff_attendance_doctor_cert_page_field", "field");	
$arrCookies[] = array("ck_staff_attendance_doctor_cert_page_keyword", "Keyword");

if($clearCoo == 1)
	clearCookies($arrCookies);
else
	updateGetCookies($arrCookies);

$StaffAttend3 = new libstaffattend3();
if (!$sys_custom['StaffAttendance']['DoctorCertificate'] || (!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-DOCTORCERTIFICATE'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;

$CurrentPage['DoctorCertificate'] = 1;
$TAGS_OBJ[] = array($Lang['StaffAttendance']['DoctorCertificate'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/DoctorCertificate/",1);
//$TAGS_OBJ_RIGHT[] = array(gen_online_help_btn_and_layer('staff_attendance','doctor_certificate'));

$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

$Keyword = trim(stripslashes($Keyword));
$StatusType = isset($StatusType)?$StatusType:array(CARD_STATUS_OUTGOING,CARD_STATUS_HOLIDAY,CARD_STATUS_ABSENT);

echo $StaffAttend3UI->Get_Doctor_Certificate_Management_Index($TargetYear,$TargetMonth,$RecordType,$StatusType,$Keyword,$field,$order,$pageNo,$numOfPage);

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script type="text/JavaScript" type="JavaScript">
function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		Get_Doctor_Certificate_Management_DBTable();
	}
	else
		return false;
}

function Get_Doctor_Certificate_Management_DBTable()
{
	var formData = $('#form1').serialize();
	formData += "&task=Get_Doctor_Certificate_Management_DBTable";
	
	Block_Element("form1");
	$.ajax({  
		type: "POST",  
		url: "ajax_load.php",
		data: formData,
		cache:false,
		success: function(ReturnData) {
			$("#DBTableLayer").html(ReturnData);
			UnBlock_Element("form1");
			Scroll_To_Top();
		} 
	});
}

function Get_Doctor_Certificate_Edit_Form()
{
	var RecordArray = document.getElementsByName('RecordID[]');
	var RecordIDArray = [];
	
	for(var i=0;i<RecordArray.length;i++){
		if(RecordArray[i].checked){
			RecordIDArray.push(RecordArray[i].value);
		}
	}
	
	if(RecordIDArray.length == 0){
		alert('<?=$Lang['General']['JS_warning']['SelectAtLeastOneRecord']?>');
		return;
	}
	
	tb_show("<?=$Lang['StaffAttendance']['UploadDoctorCertificates']?>","#TB_inline?height=300&width=550&inlineId=FakeLayer");
	
	$.post(
		"ajax_load.php",
		{
			'task':"Get_Doctor_Certificate_Edit_Form",
			'DailyLogID[]':RecordIDArray,
			'TargetYear':$.trim($('#TargetYear').val()),
			'TargetMonth':$.trim($('#TargetMonth').val()) 
		},
		function(ReturnData){
			$("div#TB_ajaxContent").html(ReturnData);
		}
	);
}

function Delete_Doctor_Certificate()
{
	var RecordArray = document.getElementsByName('RecordID[]');
	var RecordIDArray = [];
	
	for(var i=0;i<RecordArray.length;i++){
		if(RecordArray[i].checked){
			RecordIDArray.push(RecordArray[i].value);
		}
	}
	
	if(RecordIDArray.length == 0){
		alert('<?=$Lang['General']['JS_warning']['SelectAtLeastOneRecord']?>');
		return;
	}
	
	if(confirm('<?=$Lang['StaffAttendance']['WarningMsg']['ConfirmDeleteFiles']?>')){
		$.post(
			'ajax_task.php',
			{
				'task':'Delete_Doctor_Certificate',
				'RecordID[]':RecordIDArray,
				'TargetYear':$.trim($('#TargetYear').val()),
				'TargetMonth':$.trim($('#TargetMonth').val())
			},
			function(Msg){
				Get_Return_Message(Msg);
				Get_Doctor_Certificate_Management_DBTable();
				Scroll_To_Top();
			}
		);
	}
}

function DeleteFileByID(FileID)
{
	if(confirm('<?=$Lang['StaffAttendance']['WarningMsg']['ConfirmDeleteFiles']?>'))
	{
		$.post(
			'ajax_task.php',
			{
				'task':'Delete_Doctor_Certificate_By_FileID',
				'FileID':FileID 
			},
			function(Msg){
				Get_Return_Message(Msg);
				Get_Doctor_Certificate_Management_DBTable();
				Scroll_To_Top();
			}
		);
	}
}

function CheckTBEditForm()
{
	var TBEditForm = document.getElementById('TBEditForm');
	var Files = document.getElementsByName('UploadFile[]');
	var WarningDiv = $('div#UploadFileWarningDiv');
	var CountFile = 0;
	for(var i=0;i<Files.length;i++){
		if($.trim(Files[i].value) != ''){
			CountFile++;
		}
	}
	if(CountFile==0){
		WarningDiv.show();
		return;
	}else{
		WarningDiv.hide();
	}
	
	Block_Thickbox();
	
	TBEditForm.action = "ajax_task.php?task=Manage_Doctor_Certificate_Files";
	TBEditForm.target = "FileUploadFrame";
	TBEditForm.encoding = "multipart/form-data";
	TBEditForm.method = 'post';
	TBEditForm.submit();
}

function FinishManageDoctorCertificateFiles(ReturnMsg)
{
	Get_Return_Message(ReturnMsg);
	tb_remove();
	Get_Doctor_Certificate_Management_DBTable();
}

function AddInputFileField()
{
	var divUploadFile = $('div#DivUploadFile');
	divUploadFile.append('<input name="UploadFile[]" type="file" class="Mandatory"><br />');
}

</script>