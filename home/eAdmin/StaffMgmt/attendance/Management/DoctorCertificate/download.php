<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
$isAdminUser = $StaffAttend3->IS_ADMIN_USER() || $StaffAttend3->Check_Access_Right('MGMT-DOCTORCERTIFICATE');
if (!$sys_custom['StaffAttendance']['DoctorCertificate'] 
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

if(!isset($_REQUEST['FileID']) && trim($_REQUEST['FileID'])==''){
	header ("Location: /");
	intranet_closedb();
	exit();
}

// if not is admin user, then only the file's owner can view it
if(!$isAdminUser){
	if(!$StaffAttend3->Is_Doctor_Certificate_File_Owner($FileID)){
		header ("Location: /");
		intranet_closedb();
		exit();
	}
}

$lf = new libfilesystem();

$file_record = $StaffAttend3->Get_Doctor_Certificate_By_FileID($_REQUEST['FileID']);

$file_name = $file_record['FileName'];
$file_path = $intranet_root.$file_record['FilePath'];

$content = $lf->file_read($file_path);

output2browser($content, $file_name);

intranet_closedb();
?>