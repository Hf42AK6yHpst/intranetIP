<?php
// editing by 
/*
 * 2018-01-19 Carlos: Created.
 */
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-Attendance'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	//echo 'die';
	intranet_closedb();
	exit();
}

$CARD_STATUS_LATEEARLYLEAVE = 6;
$CARD_STATUS_ABSENT_SUSPECTED = 7;

$StaffAttend3 = new libstaffattend3();
$StaffAttend3UI = new libstaffattend3_ui();

$params = $_POST;
$params['StaffIDToRecords'] = true;
if(isset($params['RecordID'])){
	unset($params['RecordID']);
}
$staffIdToRecords = $StaffAttend3->Get_Overall_Daily_Attendance_Records($params);

$x .= '<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tbody>
			<tr>
				
				<td valign="bottom">
					';
if ($StaffAttend3->Check_Access_Right('MGMT-Attendance-Manage') || $StaffAttend3->IS_ADMIN_USER())
{
					$x .= '<div class="common_table_tool">
						<a href="javascript:void(0);" class="tool_edit" title="'.$Lang['StaffAttendance']['EditStatus'].'" onclick="Get_Edit_Overall_List();">'.$Lang['StaffAttendance']['EditStatus'].'</a></div>';
}else
{
					$x .= '&nbsp;';
}
				$x .= '
				</td>
	  		</tr>
		</tbody>
	</table>';

$x .= '<table class="common_table_list">
      <tbody>
		<tr>
            <th>'.$Lang['StaffAttendance']['StaffName'].'</th>
            <th class="sub_row_top">'.$Lang['StaffAttendance']['WorkingTimeSlotHeader'].'</th>
            <th class="sub_row_top">'.$Lang['StaffAttendance']['Duty'].'</th>
            <th class="sub_row_top">'.$Lang['StaffAttendance']['In'].'</th>
            <th class="sub_row_top">'.$Lang['StaffAttendance']['Out'].'</th>
			<th class="sub_row_top">'.$Lang['StaffAttendance']['InStation'].'</th>
            <th class="sub_row_top">'.$Lang['StaffAttendance']['OutStation'].'</th>
            <th class="sub_row_top">'.$Lang['StaffAttendance']['Status'].'</th>
            <th class="sub_row_top">'.$Lang['StaffAttendance']['Reason'].'</th>
			<th class="sub_row_top">'.$Lang['StaffAttendance']['Waived'].'</th>
			<th class="sub_row_top">'.$Lang['StaffAttendance']['Remark'].'</th>
            <th class="sub_row_top">'.$Lang['StaffAttendance']['LastUpdated'].'</th>';
if ($StaffAttend3->Check_Access_Right('MGMT-Attendance-Manage') || $StaffAttend3->IS_ADMIN_USER())
{
	 $x .= '<th class="sub_row_top"><span class="num_check">
              <input name="checkall" id="checkall" type="checkbox" onclick="Set_Checkbox_Value(\'RecordID[]\',this.checked);" >
            </span></th>';
}
			$x .= '</tr>';

if(count($staffIdToRecords)==0){
	$x .= '<tr><td colspan="13" nowrap align="center">'.$Lang['General']['NoRecordFound'].'</td></tr>';
}else{
	foreach($staffIdToRecords as $staff_id => $DataList)
	{
		for($j=0;$j<sizeof($DataList);$j++)
		{
			$ELWaived = '';
			switch($DataList[$j]['Status'])
        	{
        		case CARD_STATUS_NORMAL:
        		{
        			$icon_name = 'icon_present_s.png';
        			$status_text = $Lang['StaffAttendance']['Present'];
        			$Reason = '-';
        			$ReasonWaive = '-';
        			break;
        		}
        		case CARD_STATUS_ABSENT:
        		{
        			$icon_name = 'icon_absent_s.png';
        			$status_text = $Lang['StaffAttendance']['Absent'];
        			$Reason = (($DataList[$j]['InReason']=="")?'-':$DataList[$j]['InReason']);
        			$ReasonWaive = (($DataList[$j]['Waived']==1)?$Lang['StaffAttendance']['Waived']:$Lang['StaffAttendance']['NotWaived']);
        			break;
        		}
        		case CARD_STATUS_LATE:
        		{
        			$icon_name = 'icon_late_s.png';
        			$status_text = $Lang['StaffAttendance']['Late'];
        			$Reason = (($DataList[$j]['InReason']=="")?'-':$DataList[$j]['InReason']);
        			$ReasonWaive = (($DataList[$j]['Waived']==1)?$Lang['StaffAttendance']['Waived']:$Lang['StaffAttendance']['NotWaived']);
        			break;
        		}
        		case CARD_STATUS_EARLYLEAVE:
        		{
        			$icon_name = 'icon_early_leave_s.png';
        			$status_text = $Lang['StaffAttendance']['EarlyLeave'];
        			$Reason = (($DataList[$j]['OutReason']=="")?'-':$DataList[$j]['OutReason']);
        			$ReasonWaive = (($DataList[$j]['ELWaived']==1)?$Lang['StaffAttendance']['Waived']:$Lang['StaffAttendance']['NotWaived']);
        			break;
        		}
        		case $CARD_STATUS_LATEEARLYLEAVE:
        		{
        			$icon_name = 'icon_late_early_leave_s.png';
        			$status_text = $Lang['StaffAttendance']['Late'].' & '.$Lang['StaffAttendance']['EarlyLeave'];
        			$Reason = (($DataList[$j]['InReason']=="" && $DataList[$j]['OutReason']=="")?'-':($DataList[$j]['InReason']==""?'-':$DataList[$j]['InReason']).'<br>'.(($DataList[$j]['OutReason']=="")?'-':$DataList[$j]['OutReason']));
        			$ELWaived = ($DataList[$j]['ELWaived']==1)?'<br>'.$Lang['StaffAttendance']['Waived']:'<br>'.$Lang['StaffAttendance']['NotWaived'];
        			$ReasonWaive = (($DataList[$j]['Waived']==1)?$Lang['StaffAttendance']['Waived']:$Lang['StaffAttendance']['NotWaived']).$ELWaived;
        			break;
        		}
        		case CARD_STATUS_OUTGOING:
        		{
        			$icon_name = 'icon_present_out_s.png';
        			$status_text = $Lang['StaffAttendance']['Outing'];
        			$Reason = (($DataList[$j]['InReason']=="")?'-':$DataList[$j]['InReason']);
        			$ReasonWaive = (($DataList[$j]['Waived']==1)?$Lang['StaffAttendance']['Waived']:$Lang['StaffAttendance']['NotWaived']);
        			break;
        		}
        		case CARD_STATUS_HOLIDAY:
        		{
        			$icon_name = 'icon_present_out_s.png';
        			$status_text = $Lang['StaffAttendance']['Holiday'];
        			$Reason = (($DataList[$j]['InReason']=="")?'-':$DataList[$j]['InReason']);
        			$ReasonWaive = (($DataList[$j]['Waived']==1)?$Lang['StaffAttendance']['Waived']:$Lang['StaffAttendance']['NotWaived']);
        			break;
        		}
        		default:
        		{
        			$icon_name = 'icon_absent_suspect_s.png';
        			$status_text = $Lang['StaffAttendance']['AbsentSuspected'];
        			$Reason = '-';
        			$ReasonWaive = '-';
        			break;
        		}
        	}
			
			if($DataList[$j]['Status'] == CARD_STATUS_HOLIDAY)
			{
				$duty_text = $Lang['StaffAttendance']['Holiday']; 
			}else if($DataList[$j]['Status'] == CARD_STATUS_OUTGOING)
			{
				$duty_text = $Lang['StaffAttendance']['Outing'];
			}else if($DataList[$j]['Duty'] == '1')
			{
				$duty_text = $Lang['StaffAttendance']['OnDuty'];
			}else
			{
				$duty_text = $Lang['StaffAttendance']['NoDuty'];
			}
			
			if($DataList[$j]['SlotName']!="")
			{
				$working_time_slot = $DataList[$j]['SlotName'].'('.$DataList[$j]['SlotStart'].'-'.$DataList[$j]['SlotEnd'].')';
			}else
			{
				$working_time_slot = '-';
			}
			
			$display_group_name = '';
			if($DataList[$j]['GroupName'] != ''){
				$display_group_name = ' ('.$DataList[$j]['GroupName'].')';
			}
			
			if($j==0)
			{
				$x .= '<tr>
	                        <td rowspan="'.sizeof($DataList).'">'.$DataList[$j]['StaffName'].$display_group_name.'</td>
	                        <td nowrap="nowrap">'.$working_time_slot.'</td>
	                        <td>'.$duty_text.'</td>
	                        <td nowrap="nowrap">'.(($DataList[$j]['InTime']=="")?'-':$DataList[$j]['InTime']).'</td>
	                        <td nowrap="nowrap">'.(($DataList[$j]['OutTime']=="")?'-':$DataList[$j]['OutTime']).'</td>
							<td nowrap="nowrap">'.(($DataList[$j]['InSchoolStation']=="")?'-':intranet_htmlspecialchars($DataList[$j]['InSchoolStation'] )).'</td>
	                        <td nowrap="nowrap">'.(($DataList[$j]['OutSchoolStation']=="")?'-':intranet_htmlspecialchars($DataList[$j]['OutSchoolStation'] )).'</td>
	                        <td><img src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/attendance/'.$icon_name.'" title="'.$status_text.'" align="absmiddle" border="0" width="24" height="20">'.$status_text.'</td>
	                        <td>'.$Reason.'</td>
							<td nowrap="nowrap">'.$ReasonWaive.'</td>
	                        <td>'.(($DataList[$j]['Remark']=="")?'-':nl2br(intranet_htmlspecialchars($DataList[$j]['Remark']))).'</td>
							<td nowrap="nowrap">'.(($DataList[$j]['LastUpdated']=="")?'-':$DataList[$j]['LastUpdated']).(trim($DataList[$j]['LastUpdater'])!=''?'<br />by '.$DataList[$j]['LastUpdater']:'').'</td>';
				if ($StaffAttend3->Check_Access_Right('MGMT-Attendance-Manage') || $StaffAttend3->IS_ADMIN_USER())
				{
					 $x .= '<td><input name="RecordID[]" id="RecordID[]" type="checkbox" value="'.$DataList[$j]['RecordID'].'">
							</td>';
				}
				$x .= '</tr>';
			}else
			{
				$x .= '<tr>
	                        <td>'.$working_time_slot.'</td>
	                        <td>'.$duty_text.'</td>
	                        <td nowrap="nowrap">'.(($DataList[$j]['InTime']=="")?'-':$DataList[$j]['InTime']).'</td>
	                        <td nowrap="nowrap">'.(($DataList[$j]['OutTime']=="")?'-':$DataList[$j]['OutTime']).'</td>
							<td nowrap="nowrap">'.(($DataList[$j]['InSchoolStation']=="")?'-':intranet_htmlspecialchars($DataList[$j]['InSchoolStation'] )).'</td>
	                        <td nowrap="nowrap">'.(($DataList[$j]['OutSchoolStation']=="")?'-':intranet_htmlspecialchars($DataList[$j]['OutSchoolStation'] )).'</td>
	                        <td><img src="'.$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/attendance/'.$icon_name.'" title="'.$status_text.'" align="absmiddle" border="0" width="24" height="20">'.$status_text.'</td>
	                        <td>'.$Reason.'</td>
							<td nowrap="nowrap">'.$ReasonWaive.'</td>
	                        <td>'.(($DataList[$j]['Remark']=="")?'-':intranet_htmlspecialchars($DataList[$j]['Remark'] )).'</td>
							<td nowrap="nowrap">'.(($DataList[$j]['LastUpdated']=="")?'-':$DataList[$j]['LastUpdated']).(trim($DataList[$j]['LastUpdater'])!=''?'<br />by '.$DataList[$j]['LastUpdater']:'').'</td>';
				 if ($StaffAttend3->Check_Access_Right('MGMT-Attendance-Manage') || $StaffAttend3->IS_ADMIN_USER())
				 {
					 $x .= '<td><input name="RecordID[]" id="RecordID[]" type="checkbox" value="'.$DataList[$j]['RecordID'].'">
							</td>';
				 }
				$x .= '</tr>';
			}
		}
	}
}

$x .= '</tbody>
	</table><br />';
			
echo $x;

intranet_closedb();
?>