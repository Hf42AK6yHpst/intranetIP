<?php
// editing by 
/*
 * 2019-03-22 (Carlos): Modified EditRemark(RemarkNo) and SetRemark(RemarkNo) to use div instead of iframe.
 * 2018-01-19 Carlos: Created.
 */
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-Attendance'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;

$CurrentPage['Attendance'] = 1;
$TAGS_OBJ = $StaffAttend3UI->Get_Management_DailyRecord_Navigation_Tabs('OverallAttendanceRecords');
/*
$TAGS_OBJ[] = array($Lang['StaffAttendance']['AttendanceRecord'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/",0);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['OverallAttendanceRecords'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/overall_list/",1);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['ViewLateAbsentEarlyList'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/daily_profile_list/",0);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['InSchoolInOutStatus'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/real_time_staff_status/",0);
if ($StaffAttend3->Check_Access_Right('MGMT-Attendance-Manage') || $StaffAttend3->IS_ADMIN_USER()) {
	$TAGS_OBJ[] = array($Lang['StaffAttendance']['ImportOfflineRecords'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/offline/",0);
}
*/
$CARD_STATUS_LATEEARLYLEAVE = 6;
$CARD_STATUS_ABSENT_SUSPECTED = 7;

if(!isset($TargetDate)){
	$TargetDate = date('Y-m-d');
}
if(!isset($StatusType) || !is_array($StatusType)){
	$StatusType = array(CARD_STATUS_NORMAL,CARD_STATUS_LATE,CARD_STATUS_EARLYLEAVE,$CARD_STATUS_LATEEARLYLEAVE,CARD_STATUS_ABSENT,CARD_STATUS_HOLIDAY,CARD_STATUS_OUTGOING,$CARD_STATUS_ABSENT_SUSPECTED);
}

$reason_js = "var ReasonData = {};\n";
$reason_js.= "var ReasonTypeToDefaultReasonID = {};\n";
$ReasonTypeList = array(CARD_STATUS_ABSENT,CARD_STATUS_LATE,CARD_STATUS_EARLYLEAVE,CARD_STATUS_HOLIDAY,CARD_STATUS_OUTGOING);
$ReasonList = array();
foreach($ReasonTypeList as $reason_type){
	$ReasonList[$reason_type] = $StaffAttend3->Get_Preset_Leave_Reason_List($reason_type);
	$reason_js .= "ReasonData[".$reason_type."] = [];\n";
	for($i=0;$i<count($ReasonList[$reason_type]);$i++){
		$reason_js .= 'ReasonData['.$reason_type.']['.$ReasonList[$reason_type][$i]['ReasonID'].'] = {"ReasonID":"'.$ReasonList[$reason_type][$i]['ReasonID'].'","ReasonText":"'.str_replace(array('"'),array('\"'),intranet_htmlspecialchars($ReasonList[$reason_type][$i]['ReasonText'])).'","IsDefault":"'.($ReasonList[$reason_type][$i]['DefaultFlag']?"1":"0").'","IsWaived":"'.$ReasonList[$reason_type][$i]['DefaultWavie'].'"};'."\n";
		if($ReasonList[$reason_type][$i]['DefaultFlag']){
			$reason_js.= 'ReasonTypeToDefaultReasonID['.$reason_type.'] = "'.$ReasonList[$reason_type][$i]['ReasonID'].'";'."\n";
		}
	}
}

$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));
echo $StaffAttend3UI->Include_JS_CSS();
?>
<br />
<form id="form1" name="form1" method="post" action="return false;">
<div class="content_top_tool">
	<div class="Conntent_tool">
	</div>
	<div class="Conntent_search">
		<input id="Keyword" name="Keyword" type="text" value="" onkeyup="Check_Go_Search(event);" />
	</div>
	<br style="clear:both;">
</div>
<div class="table_board">
<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tbody>
	<tr>
		<td valign="bottom">
		<div class="table_filter">
		  <table border="0">
		  <tr>
			  <td>
			  <?=$StaffAttend3UI->GET_DATE_PICKER("TargetDate", $TargetDate,'onchange="Get_Overall_List();"',"","","","","Get_Overall_List();")?>
			  <span style="color:red;" id="TargetDateFutureDateWarningLayer"></span>
			  </td>
			  <td> 
<?php

$StatusTypeOptionsLayer = '<div class="selectbox_group selectbox_group_filter"> 
							<a href="javascript:void(0);" onclick="$(\'#status_option\').css(\'visibility\')==\'hidden\'?MM_showHideLayers(\'status_option\',\'\',\'show\'):MM_showHideLayers(\'status_option\',\'\',\'hide\');">'.$Lang['StaffAttendance']['Status'].'</a> 
						</div>
						<p class="spacer"></p>
						<div id="status_option" class="selectbox_layer selectbox_group_layer" style="visibility:hidden;z-index:100;">
							<input type="checkbox" value="'.CARD_STATUS_NORMAL.'" name="StatusType[]" id="Status'.CARD_STATUS_NORMAL.'" '.(in_array(CARD_STATUS_NORMAL,$StatusType)?'checked':'').'>
							<label for="Status'.CARD_STATUS_NORMAL.'">'.$Lang['StaffAttendance']['Present'].'</label><br>
							<input type="checkbox" value="'.CARD_STATUS_LATE.'" name="StatusType[]" id="Status'.CARD_STATUS_LATE.'" '.(in_array(CARD_STATUS_LATE,$StatusType)?'checked':'').'>
							<label for="Status'.CARD_STATUS_LATE.'">'.$Lang['StaffAttendance']['Late'].'</label><br>
							<input type="checkbox" value="'.CARD_STATUS_EARLYLEAVE.'" name="StatusType[]" id="Status'.CARD_STATUS_EARLYLEAVE.'" '.(in_array(CARD_STATUS_EARLYLEAVE,$StatusType)?'checked':'').'>
							<label for="Status'.CARD_STATUS_EARLYLEAVE.'">'.$Lang['StaffAttendance']['EarlyLeave'].'</label><br>
							<input type="checkbox" value="'.$CARD_STATUS_LATEEARLYLEAVE.'" name="StatusType[]" id="Status'.$CARD_STATUS_LATEEARLYLEAVE.'" '.(in_array($CARD_STATUS_LATEEARLYLEAVE,$StatusType)?'checked':'').'>
							<label for="Status'.$CARD_STATUS_LATEEARLYLEAVE.'">'.$Lang['StaffAttendance']['Late'].' &amp; '.$Lang['StaffAttendance']['EarlyLeave'].'</label><br>
							<input type="checkbox" value="'.CARD_STATUS_ABSENT.'" name="StatusType[]" id="Status'.CARD_STATUS_ABSENT.'" '.(in_array(CARD_STATUS_ABSENT,$StatusType)?'checked':'').'>
							<label for="Status'.CARD_STATUS_ABSENT.'">'.$Lang['StaffAttendance']['Absent'].'</label><br>
							<input type="checkbox" value="'.CARD_STATUS_HOLIDAY.'" name="StatusType[]" id="Status'.CARD_STATUS_HOLIDAY.'" '.(in_array(CARD_STATUS_HOLIDAY,$StatusType)?'checked':'').'>
							<label for="Status'.CARD_STATUS_HOLIDAY.'">'.$Lang['StaffAttendance']['Holiday'].'</label><br>
							<input type="checkbox" value="'.CARD_STATUS_OUTGOING.'" name="StatusType[]" id="Status'.CARD_STATUS_OUTGOING.'" '.(in_array(CARD_STATUS_OUTGOING,$StatusType)?'checked':'').'>
							<label for="Status'.CARD_STATUS_OUTGOING.'">'.$Lang['StaffAttendance']['Outing'].'</label><br>
							<input type="checkbox" value="'.$CARD_STATUS_ABSENT_SUSPECTED.'" name="StatusType[]" id="Status'.$CARD_STATUS_ABSENT_SUSPECTED.'" '.(in_array($CARD_STATUS_ABSENT_SUSPECTED,$StatusType)?'checked':'').'>
							<label for="Status'.$CARD_STATUS_ABSENT_SUSPECTED.'">'.$Lang['StaffAttendance']['AbsentSuspected'].'</label><br>
							<p class="spacer"></p>
							<div class="edit_bottom">
								<input type="button" onmouseout="this.className=\'formsmallbutton\'" onmouseover="this.className=\'formsmallbuttonon\'" value="'.$Lang['Btn']['Apply'].'" onclick="Get_Overall_List();MM_showHideLayers(\'status_option\',\'\',\'hide\');" class="formsmallbutton ">&nbsp;
								<input type="button" onmouseout="this.className=\'formsmallbutton\'" onmouseover="this.className=\'formsmallbuttonon\'" value="'.$Lang['Btn']['Cancel'].'" onclick="MM_showHideLayers(\'status_option\',\'\',\'hide\')" class="formsmallbutton ">
							</div>
						</div>';
		echo $StatusTypeOptionsLayer;
?>
			</td>
			<td>
<?php
$group_selection = '<select id="GroupID" name="GroupID[]" multiple="multiple" size="20" style="width:100%;">';
		$GroupList = $StaffAttend3->Get_Group_List('', true);
		$group_selection .= '<optgroup label="-'.$Lang['StaffAttendance']['Group'].'-">';
		for($i=0;$i<sizeof($GroupList);$i++)
		{
			$group_selection .= '<option value="'.$GroupList[$i]['GroupID'].'" selected>'.$GroupList[$i]['GroupName'].'</option>';
		}
		$group_selection .= '</optgroup>';
		$group_selection .= '<optgroup label="-'.$Lang['StaffAttendance']['Individual'].'-">';
		$group_selection .= '<option value="-1" selected>'.$Lang['StaffAttendance']['Individual'].'</option>';
		$group_selection .= '</optgroup>';
		$group_selection .= '</select>';

$GroupTypeOptionsLayer = '<div class="selectbox_group selectbox_group_filter"> 
							<a href="javascript:void(0);" onclick="$(\'#group_option\').css(\'visibility\')==\'hidden\'?MM_showHideLayers(\'group_option\',\'\',\'show\'):MM_showHideLayers(\'group_option\',\'\',\'hide\');">'.$Lang['StaffAttendance']['Group'].' / '.$Lang['StaffAttendance']['Individual'].'</a> 
						</div>
						<p class="spacer"></p>
						<div id="group_option" class="selectbox_layer selectbox_group_layer" style="visibility:hidden;z-index:100;">
							'.$group_selection.'
							<div style="float:right;">'.$StaffAttend3UI->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "Select_All_Options('GroupID',true);return false;").'</div>
							<p class="spacer"></p>
							<div class="edit_bottom">
								<input type="button" onmouseout="this.className=\'formsmallbutton\'" onmouseover="this.className=\'formsmallbuttonon\'" value="'.$Lang['Btn']['Apply'].'" onclick="Get_Overall_List();MM_showHideLayers(\'group_option\',\'\',\'hide\');" class="formsmallbutton ">&nbsp;
								<input type="button" onmouseout="this.className=\'formsmallbutton\'" onmouseover="this.className=\'formsmallbuttonon\'" value="'.$Lang['Btn']['Cancel'].'" onclick="MM_showHideLayers(\'group_option\',\'\',\'hide\')" class="formsmallbutton ">
							</div>
						</div>';
		echo $GroupTypeOptionsLayer;
?>				 
			  </td>
		  </tr>
		  </table>
	    </div>
		</td>
	</tr>
	</tbody>
</table>
</div>
<span id="DateWarningLayer" style="color:red;border-style:solid;border-width:2px;display:none;"></span>
<div id="TableContainer"></div>
</form>
<script type="text/javascript" language="javascript">
<?=$reason_js?>

function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		Get_Overall_List();
	}
	else
		return false;
}

function Check_Valid_Date(DateObjId, WarningLayerId)
{
	var dateObj = document.getElementById(DateObjId);
	var $WarningLayer = $('#'+WarningLayerId);
	if(check_date_without_return_msg(dateObj))
	{
		var targetdate = dateObj.value;
		var currentdate = new Date();
		var curYear = '' + currentdate.getFullYear();
		var curMonth = currentdate.getMonth()+1;
		var curDay = currentdate.getDate();
		curMonth = (curMonth<10)? ('0'+curMonth):(''+curMonth);
		curDay = (curDay<10)? ('0'+curDay):(''+curDay);
		var today = curYear + '-' + curMonth + '-' + curDay;
		
		if(targetdate>today)
		{
			dateObj.focus();
			$WarningLayer.html('<?=$Lang['StaffAttendance']['AlertTakeFutureAttendance']?>').show();
			return false;
		}else
		{
			$WarningLayer.html('').hide();
			return true;
		}
	}else
	{
		dateObj.focus();
		$WarningLayer.html('<?=$Lang['General']['InvalidDateFormat']?>').show();
		return false;
	}
}


function Check_Valid_Time(InTimeHourName, InTimeMinName, InTimeSecName, OutTimeHourName, OutTimeMinName, OutTimeSecName, WarningLayerId)
{
	var InHour = document.getElementsByName(InTimeHourName);
	var InMin = document.getElementsByName(InTimeMinName);
	var InSec = document.getElementsByName(InTimeSecName);
	var OutHour = document.getElementsByName(OutTimeHourName);
	var OutMin = document.getElementsByName(OutTimeMinName);
	var OutSec = document.getElementsByName(OutTimeSecName);
	var $WarningLayer = $('#'+WarningLayerId);
	
	for(var i=0;i<InHour.length;i++)
	{
		if(Trim(InHour[i].value)!='' || Trim(InMin[i].value)!='' || Trim(InSec[i].value)!='')
		{
			d_h = parseInt(InHour[i].value);
			d_m = parseInt(InMin[i].value);
			d_s = parseInt(InSec[i].value);
			
			if(InHour[i].value.length != 2 || d_h<0 || d_h>23 || isNaN(d_h))
			{
				InHour[i].focus();
				$WarningLayer.html('<?=$Lang['General']['InvalidDateFormat']?>').show();
				//$WarningLayer.css('visibility','visible');
				SetTimerToHideWarning(WarningLayerId, 3000);
				return false;
			}
			if(InMin[i].value.length != 2 || d_m<0 || d_m>59 || isNaN(d_m))
			{
				InMin[i].focus();
				$WarningLayer.html('<?=$Lang['StaffAttendance']['SlotTimeFormatWarning']?>').show();
				//$WarningLayer.css('visibility','visible');
				SetTimerToHideWarning(WarningLayerId, 3000);
				return false;
			}
			if(InSec[i].value.length != 2 || d_s<0 || d_s>59 || isNaN(d_s))
			{
				InSec[i].focus();
				$WarningLayer.html('<?=$Lang['StaffAttendance']['SlotTimeFormatWarning']?>').show();
				//$WarningLayer.css('visibility','visible');
				SetTimerToHideWarning(WarningLayerId, 3000);
				return false;
			}
		}
    	
    	if(Trim(OutHour[i].value)!='' || Trim(OutMin[i].value)!='' || Trim(OutSec[i].value)!='')
		{
			d_h = parseInt(OutHour[i].value);
			d_m = parseInt(OutMin[i].value);
			d_s = parseInt(OutSec[i].value);
			
			if(OutHour[i].value.length != 2 || d_h<0 || d_h>23 || isNaN(d_h))
			{
				OutHour[i].focus();
				$WarningLayer.html('<?=$Lang['StaffAttendance']['SlotTimeFormatWarning']?>').show();
				//$WarningLayer.css('visibility','visible');
				SetTimerToHideWarning(WarningLayerId, 3000);
				return false;
			}
			if(OutMin[i].value.length != 2 || d_m<0 || d_m>59 || isNaN(d_m))
			{
				OutMin[i].focus();
				$WarningLayer.html('<?=$Lang['StaffAttendance']['SlotTimeFormatWarning']?>').show();
				//$WarningLayer.css('visibility','visible');
				SetTimerToHideWarning(WarningLayerId, 3000);
				return false;
			}
			if(OutSec[i].value.length != 2 || d_s<0 || d_s>59 || isNaN(d_s))
			{
				OutSec[i].focus();
				$WarningLayer.html('<?=$Lang['StaffAttendance']['SlotTimeFormatWarning']?>').show();
				//$WarningLayer.css('visibility','visible');
				SetTimerToHideWarning(WarningLayerId, 3000);
				return false;
			}
		}
	}
	
	$WarningLayer.html('').hide();
	//$WarningLayer.css('visibility','hidden');
	return true;
}

function Check_Time_Status(StatusName, InTimeHourName, InTimeMinName, InTimeSecName, OutTimeHourName, OutTimeMinName, OutTimeSecName, WarningLayerId)
{
	var InHour = document.getElementsByName(InTimeHourName);
	var InMin = document.getElementsByName(InTimeMinName);
	var InSec = document.getElementsByName(InTimeSecName);
	var Status = document.getElementsByName(StatusName);
	var $WarningLayer = $('#'+WarningLayerId);
	
	for(i=0;i<Status.length;i++){
		if(Trim(InHour[i].value)!='' && Trim(InMin[i].value)!='' && Trim(InSec[i].value)!='' && Status[i].value=='7'){
			$WarningLayer.html('<?=$Lang['StaffAttendance']['StatusMustSetIfInTimeSetWarning']?>').show();
			//$WarningLayer.css('visibility','visible');
			scroll(0,0);
			return false;
		}
	}
	
	$WarningLayer.html('').hide();
	//$WarningLayer.css('visibility','hidden');
	return true;
}

function SetTimerToHideWarning(WarningLayerId, ticks)
{
	setTimeout(function(){
		$('#'+WarningLayerId).hide();
	},ticks);
}

function Get_Overall_List()
{
	if (Check_Valid_Date('TargetDate','DPWL-TargetDate')) {
		if ($('#DPWL-TargetDate').html() == '') {
			var PostVar = $('#form1').serialize();
			Block_Element('TableContainer');
			$.post(
				'get_view_table.php',PostVar,
				function (htmlData) {
					$('#TableContainer').html(htmlData);
					UnBlock_Element('TableContainer');
				}
			);
		}
	}
}

function Get_Edit_Overall_List()
{
	if($('input[name="RecordID[]"]:checked').length == 0){
		alert('<?=$Lang['StaffAttendance']['SelectAtLeastOneWarning']?>');
		return false;
	}
	
	if (Check_Valid_Date('TargetDate','DPWL-TargetDate')) {
		if ($('#DPWL-TargetDate').html() == '') {
			var PostVar = $('#form1').serialize();
			Block_Element('TableContainer');
			$.post(
				'get_edit_table.php',PostVar,
				function (htmlData) {
					$('#TableContainer').html(htmlData);
					UnBlock_Element('TableContainer');
				}
			);
		}
	}
}

function Update_Attendance()
{
	if(!Check_Valid_Time('InHour[]', 'InMin[]', 'InSec[]', 'OutHour[]', 'OutMin[]', 'OutSec[]', 'DateWarningLayer'))
		return;
	
	if(!Check_Time_Status('Status[]','InHour[]', 'InMin[]', 'InSec[]', 'OutHour[]', 'OutMin[]', 'OutSec[]', 'DateWarningLayer'))
		return;
		
	$.post(
		'update_attendance.php',
		$('#form1').serialize(),
		function(data){
			Get_Return_Message(data);
			Get_Overall_List();
			window.top.tb_remove();
			Scroll_To_Top();
		}
	);
}

function ShowHideStatusLayer(recordId,selectedStatus)
{
	var html = '<div style="visibility:hidden;position:absolute;" id="status_options_'+recordId+'" class="selectbox_layer">';
	    html += '<nobr class="select_attendance_status">';
	    html += '<a href="javascript:void(0);" id="status_option_'+recordId+'_<?=CARD_STATUS_NORMAL?>" onclick="SetStatus('+recordId+',<?=CARD_STATUS_NORMAL?>);" class=""><img src="/images/<?=$LAYOUT_SKIN?>/attendance/icon_present_s.png" title="<?=$Lang['StaffAttendance']['Present']?>" align="absmiddle" border="0" width="24" height="20"><?=$Lang['StaffAttendance']['Present']?></a>';
		html +=	'<a href="javascript:void(0);" id="status_option_'+recordId+'_<?=CARD_STATUS_LATE?>" onclick="SetStatus('+recordId+',<?=CARD_STATUS_LATE?>);" class=""><img src="/images/<?=$LAYOUT_SKIN?>/attendance/icon_late_s.png" title="<?=$Lang['StaffAttendance']['Late']?>" align="absmiddle" border="0" width="24" height="20"><?=$Lang['StaffAttendance']['Late']?></a>';
		html +=	'<a href="javascript:void(0);" id="status_option_'+recordId+'_<?=CARD_STATUS_EARLYLEAVE?>" onclick="SetStatus('+recordId+',<?=CARD_STATUS_EARLYLEAVE?>);" class=""><img src="/images/<?=$LAYOUT_SKIN?>/attendance/icon_early_leave_s.png" title="<?=$Lang['StaffAttendance']['EarlyLeave']?>" align="absmiddle" border="0" width="24" height="20"><?=$Lang['StaffAttendance']['EarlyLeave']?></a>';
		html +=	'<a href="javascript:void(0);" id="status_option_'+recordId+'_<?=$CARD_STATUS_LATEEARLYLEAVE?>" onclick="SetStatus('+recordId+',<?=$CARD_STATUS_LATEEARLYLEAVE?>);" class=""><img src="/images/<?=$LAYOUT_SKIN?>/attendance/icon_late_early_leave_s.png" title="<?=$Lang['StaffAttendance']['Late'].' &amp; '.$Lang['StaffAttendance']['EarlyLeave']?>" align="absmiddle" border="0" width="24" height="20"><?=$Lang['StaffAttendance']['Late'].' &amp; '.$Lang['StaffAttendance']['EarlyLeave']?></a>';
		html +=	'<a href="javascript:void(0);" id="status_option_'+recordId+'_<?=CARD_STATUS_OUTGOING?>" onclick="SetStatus('+recordId+',<?=CARD_STATUS_OUTGOING?>);" class=""><img src="/images/<?=$LAYOUT_SKIN?>/attendance/icon_present_out_s.png" title="<?=$Lang['StaffAttendance']['Outing']?>" align="absmiddle" border="0" width="24" height="20"><?=$Lang['StaffAttendance']['Outing']?></a>';
		html += '<a href="javascript:void(0);" id="status_option_'+recordId+'_<?=CARD_STATUS_ABSENT?>" onclick="SetStatus('+recordId+',<?=CARD_STATUS_ABSENT?>);" class=""><img src="/images/<?=$LAYOUT_SKIN?>/attendance/icon_absent_s.png" title="<?=$Lang['StaffAttendance']['Absent']?>" align="absmiddle" border="0" width="24" height="20"><?=$Lang['StaffAttendance']['Absent']?></a>';
		html += '<a href="javascript:void(0);" id="status_option_'+recordId+'_<?=CARD_STATUS_HOLIDAY?>" onclick="SetStatus('+recordId+',<?=CARD_STATUS_HOLIDAY?>);" class=""><img src="/images/<?=$LAYOUT_SKIN?>/attendance/icon_present_out_s.png" title="<?=$Lang['StaffAttendance']['Holiday']?>" align="absmiddle" border="0" width="24" height="20"><?=$Lang['StaffAttendance']['Holiday']?></a>';
		html += '</nobr>';
		html += '</div>';
	
	var status_div = $('#select_status_'+recordId);
	var parent = status_div.closest('td');
	var status_layer = parent.find('#status_options_'+recordId);
	if(!status_layer.length > 0){
		status_div.after(html);
		status_layer = parent.find('#status_options_'+recordId);
	}
	
	var pos = status_div.position();
	var offsetY = status_div.height() + 4;
	status_layer.css({'position':'absolute','left':pos.left+'px','top':(pos.top+offsetY)+'px','visibility':(status_layer.css('visibility') == 'hidden'?'visible':'hidden')});
	status_layer.find('a').removeClass('selected_attendance');
	var status_btn = status_layer.find('a#status_option_'+recordId+'_'+selectedStatus);
	if(status_btn.length > 0){
		status_btn.addClass('selected_attendance');
	}
}

function SetStatus(recordId,selectedStatus)
{
	var status_options_html = {};
	status_options_html[<?=CARD_STATUS_NORMAL?>] = '<a href="javascript:void(0);" onclick="ShowHideStatusLayer('+recordId+',<?=CARD_STATUS_NORMAL?>);" class=""><img src="/images/<?=$LAYOUT_SKIN?>/attendance/icon_present_s.png" title="<?=$Lang['StaffAttendance']['Present']?>" align="absmiddle" border="0" width="24" height="20"><?=$Lang['StaffAttendance']['Present']?></a>';
	status_options_html[<?=CARD_STATUS_LATE?>] = '<a href="javascript:void(0);" onclick="ShowHideStatusLayer('+recordId+',<?=CARD_STATUS_LATE?>);" class=""><img src="/images/<?=$LAYOUT_SKIN?>/attendance/icon_late_s.png" title="<?=$Lang['StaffAttendance']['Late']?>" align="absmiddle" border="0" width="24" height="20"><?=$Lang['StaffAttendance']['Late']?></a>';
	status_options_html[<?=CARD_STATUS_EARLYLEAVE?>] = '<a href="javascript:void(0);" onclick="ShowHideStatusLayer('+recordId+',<?=CARD_STATUS_EARLYLEAVE?>);" class=""><img src="/images/<?=$LAYOUT_SKIN?>/attendance/icon_early_leave_s.png" title="<?=$Lang['StaffAttendance']['EarlyLeave']?>" align="absmiddle" border="0" width="24" height="20"><?=$Lang['StaffAttendance']['EarlyLeave']?></a>';
	status_options_html[<?=$CARD_STATUS_LATEEARLYLEAVE?>] = '<a href="javascript:void(0);" onclick="ShowHideStatusLayer('+recordId+',<?=$CARD_STATUS_LATEEARLYLEAVE?>);" class=""><img src="/images/<?=$LAYOUT_SKIN?>/attendance/icon_late_early_leave_s.png" title="<?=$Lang['StaffAttendance']['Late'].' &amp; '.$Lang['StaffAttendance']['EarlyLeave']?>" align="absmiddle" border="0" width="24" height="20"><?=$Lang['StaffAttendance']['Late'].' &amp; '.$Lang['StaffAttendance']['EarlyLeave']?></a>';
	status_options_html[<?=CARD_STATUS_OUTGOING?>] = '<a href="javascript:void(0);" onclick="ShowHideStatusLayer('+recordId+',<?=CARD_STATUS_OUTGOING?>);" class=""><img src="/images/<?=$LAYOUT_SKIN?>/attendance/icon_present_out_s.png" title="<?=$Lang['StaffAttendance']['Outing']?>" align="absmiddle" border="0" width="24" height="20"><?=$Lang['StaffAttendance']['Outing']?></a>';
	status_options_html[<?=CARD_STATUS_ABSENT?>] = '<a href="javascript:void(0);" onclick="ShowHideStatusLayer('+recordId+',<?=CARD_STATUS_ABSENT?>);" class=""><img src="/images/<?=$LAYOUT_SKIN?>/attendance/icon_absent_s.png" title="<?=$Lang['StaffAttendance']['Absent']?>" align="absmiddle" border="0" width="24" height="20"><?=$Lang['StaffAttendance']['Absent']?></a>';
	status_options_html[<?=CARD_STATUS_HOLIDAY?>] = '<a href="javascript:void(0);" onclick="ShowHideStatusLayer('+recordId+',<?=CARD_STATUS_HOLIDAY?>);" class=""><img src="/images/<?=$LAYOUT_SKIN?>/attendance/icon_present_out_s.png" title="<?=$Lang['StaffAttendance']['Holiday']?>" align="absmiddle" border="0" width="24" height="20"><?=$Lang['StaffAttendance']['Holiday']?></a>';
	
	var status_div = $('#select_status_'+recordId);
	var parent = status_div.closest('td');
	parent.find('#Status_'+recordId).val(selectedStatus);
	status_div.html(status_options_html[selectedStatus]);
	var select_options_layer = parent.find('#status_options_'+recordId);
	if(select_options_layer){
		select_options_layer.remove();
	}
	
	var parent_tr = parent.closest('tr');
	var in_reason_div = parent_tr.find('#in_reason_container_'+recordId);
	var out_reason_div = parent_tr.find('#out_reason_container_'+recordId);
	var in_waived_div = parent_tr.find('#in_waived_container_'+recordId);
	var out_waived_div = parent_tr.find('#out_waived_container_'+recordId);
	var in_reason_default_id = '';
	var out_reason_default_id = '';
	if(selectedStatus == '<?=CARD_STATUS_NORMAL?>'){
		SetReason(recordId,'in',in_reason_default_id);
		SetReason(recordId,'out',out_reason_default_id);
		in_reason_div.hide();
		out_reason_div.hide();
		in_waived_div.hide();
		out_waived_div.hide();
	}else if(selectedStatus == '<?=CARD_STATUS_EARLYLEAVE?>'){
		if(ReasonTypeToDefaultReasonID['<?=CARD_STATUS_EARLYLEAVE?>']){
			out_reason_default_id = ReasonTypeToDefaultReasonID['<?=CARD_STATUS_EARLYLEAVE?>'];
		}
		SetReason(recordId,'in','');
		SetReason(recordId,'out',out_reason_default_id);
		in_reason_div.hide();
		out_reason_div.show();
		in_waived_div.hide();
		out_waived_div.show();
	}else if(selectedStatus == '<?=$CARD_STATUS_LATEEARLYLEAVE?>'){
		if(ReasonTypeToDefaultReasonID['<?=CARD_STATUS_LATE?>']){
			in_reason_default_id = ReasonTypeToDefaultReasonID['<?=CARD_STATUS_LATE?>'];
		}
		if(ReasonTypeToDefaultReasonID['<?=CARD_STATUS_EARLYLEAVE?>']){
			out_reason_default_id = ReasonTypeToDefaultReasonID['<?=CARD_STATUS_EARLYLEAVE?>'];
		}
		SetReason(recordId,'in',in_reason_default_id);
		SetReason(recordId,'out',out_reason_default_id);
		in_reason_div.show();
		out_reason_div.show();
		in_waived_div.show();
		out_waived_div.show();
	}else{
		if(ReasonTypeToDefaultReasonID[selectedStatus]){
			in_reason_default_id = ReasonTypeToDefaultReasonID[selectedStatus];
		}
		SetReason(recordId,'in',in_reason_default_id);
		SetReason(recordId,'out',out_reason_default_id);
		in_reason_div.show();
		out_reason_div.hide();
		in_waived_div.show();
		out_waived_div.hide();
	}
	
	if(!$('#InTimeEdit'+recordId).is(':visible')) $('#InTimeView'+recordId).show();
	if(!$('#OutTimeEdit'+recordId).is(':visible')) $('#OutTimeView'+recordId).show();
	if(!$('#InStationEdit'+recordId).is(':visible')) $('#InStationView'+recordId).show();
	if(!$('#OutStationEdit'+recordId).is(':visible')) $('#OutStationView'+recordId).show();
	if(!$('#EditRemarkLayer'+recordId).is(':visible')) $('#ViewRemarkLayer'+recordId).show();
}

function ShowHideReasonLayer(recordId,inOut)
{
	var reasonType = $('#Status_'+recordId).val();
	if(reasonType == '<?=$CARD_STATUS_LATEEARLYLEAVE?>'){
		reasonType = inOut == 'out'? '<?=CARD_STATUS_EARLYLEAVE?>':'<?=CARD_STATUS_LATE?>';
	}
	if(!ReasonData[reasonType]) return;
	
	var html = '<div style="visibility:hidden;" id="'+inOut+'_reason_options_'+recordId+'" class="selectbox_layer">';
		html += '<nobr class="select_attendance_status">';
		html += '<a href="javascript:void(0);" id="'+inOut+'_reason_option_'+recordId+'_" onclick="SetReason('+recordId+',\''+inOut+'\',\'\');" class="">-</a>';
	if(ReasonData[reasonType])
	{
		for(var key in ReasonData[reasonType]){
			if(ReasonData[reasonType].hasOwnProperty(key)){
				html += '<a href="javascript:void(0);" id="'+inOut+'_reason_option_'+recordId+'_'+ReasonData[reasonType][key]['ReasonID']+'" onclick="SetReason('+recordId+',\''+inOut+'\',\''+ReasonData[reasonType][key]['ReasonID']+'\');" class="">'+ReasonData[reasonType][key]['ReasonText']+'</a>';
			}
		}	
	}
		html += '</nobr>';
		html += '</div>';
	
	var reason_div = $('#select_'+inOut+'_reason_'+recordId);
	var parent = reason_div.closest('td');
	var reason_layer = parent.find('#'+inOut+'_reason_options_'+recordId);
	if(!reason_layer.length > 0){
		reason_div.after(html);
		reason_layer = parent.find('#'+inOut+'_reason_options_'+recordId);
	}
	
	var pos = reason_div.position();
	var offsetY = reason_div.height() + 4;
	reason_layer.css({'position':'absolute','left':pos.left+'px','top':(pos.top+offsetY)+'px','visibility':(reason_layer.css('visibility') == 'hidden'?'visible':'hidden')});
	reason_layer.find('a').removeClass('selected_attendance');
	var reason_btn = reason_layer.find('a#'+inOut+'_reason_option_'+recordId+'_'+parent.find('#'+inOut+'_reason_'+recordId).val());
	if(reason_btn.length > 0){
		reason_btn.addClass('selected_attendance');
	}
	
}

function SetReason(recordId,inOut,reasonId)
{
	var reasonType = $('#Status_'+recordId).val();
	if(reasonType == '<?=$CARD_STATUS_LATEEARLYLEAVE?>'){
		reasonType = inOut == 'out'? '<?=CARD_STATUS_EARLYLEAVE?>':'<?=CARD_STATUS_LATE?>';
	}
	var reason_div = $('#select_'+inOut+'_reason_'+recordId);
	var html = ReasonData[reasonType] && ReasonData[reasonType][reasonId]? ReasonData[reasonType][reasonId]['ReasonText'] : '-';
	var parent = reason_div.closest('td');
	parent.find('#'+inOut+'_reason_'+recordId).val(reasonId);
	reason_div.find('a').html(html);
	var select_options_layer = parent.find('#'+inOut+'_reason_options_'+recordId);
	if(select_options_layer){
		select_options_layer.remove();
	}
	var default_waived = ReasonData[reasonType] && ReasonData[reasonType][reasonId]? ReasonData[reasonType][reasonId]['IsWaived'] : '0';
	SetWaived(recordId,inOut,default_waived);
}

function SetWaived(recordId,inOut,waived)
{
	var waived_div = $('#'+inOut+'_waived_container_'+recordId);
	waived_div.find('input[type=checkbox]').attr('checked',waived=='1'?true:false);
	waived_div.find('input[type=hidden]').val(waived);
}

function CheckWaived(id,checked)
{
	$('#'+id).val(checked?'1':'0');
}

function EditElement(ViewLayerName, EditLayerName, EditNo)
{
	var ViewLayer = $('#'+ViewLayerName+EditNo);
	var EditLayer = $('#'+EditLayerName+EditNo);
	
	ViewLayer.hide();
	EditLayer.show();
}

function EditRemark(RemarkNo)
{
	var RemarkTd = $('td#RemarkTd'+RemarkNo);
	var ViewObj = RemarkTd.find('span#ViewRemarkLayer'+RemarkNo);
	var EditObj = RemarkTd.find('span#EditRemarkLayer'+RemarkNo);
	var HiddenRemarkObj = RemarkTd.find('input#Remark'+RemarkNo);
	var RemarkFrame = RemarkTd.find('#RemarkFrame'+RemarkNo);
	//var TextRemarkObj = RemarkTd.find('textarea#TextRemark'+RemarkNo);
	//TextRemarkObj.val(HiddenRemarkObj.val());
	
	if(ViewObj.css('display')!='none')
	{
		var remark_editor = '<textarea name="TextRemark'+RemarkNo+'" rows="3" wrap="virtual" style="width:140px;height:90px;" id="TextRemark'+RemarkNo+'">'+HiddenRemarkObj.val()+'</textarea>';
		//RemarkFrame.contents().find('body').html(remark_editor);
		RemarkFrame.html(remark_editor);
		ViewObj.hide();
		EditObj.show();
	}else{
		//RemarkFrame.contents().find('body').html('');
		RemarkFrame.html('');
		EditObj.hide();
		ViewObj.show();
	}
}

function SetRemark(RemarkNo)
{
	var RemarkTd = $('td#RemarkTd'+RemarkNo);
	var HiddenRemarkObj = RemarkTd.find('input#Remark'+RemarkNo);
	var DisplayRemarkObj = RemarkTd.find('span#DisplayRemark'+RemarkNo);
	//var TextRemarkObj = RemarkTd.find('textarea#TextRemark'+RemarkNo);
	var RemarkFrame = RemarkTd.find('#RemarkFrame'+RemarkNo);
	//var content = RemarkFrame.contents().find('#TextRemark'+RemarkNo).val();
	var content = RemarkFrame.find('#TextRemark'+RemarkNo).val();
	HiddenRemarkObj.val(content);
	DisplayRemarkObj.html(nl2br(content, false));
	
	EditRemark(RemarkNo);
}

function nl2br(str, is_xhtml)
{
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '' : '<br />';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
}

function ShowHideBatchStatusOption(clickedObj)
{
	var $OptionsObj = $('div#batch_status_options');
	var $ClickedObj = $(clickedObj);
	var $pos = $ClickedObj.position();
	var offsetY = $ClickedObj.height()+4;
	
	$OptionsObj.css('position', 'absolute');
	$OptionsObj.css('left',$pos.left+'px');
	$OptionsObj.css('top',($pos.top+offsetY)+'px');
	if($OptionsObj.css('visibility') == 'hidden')
	{
		$OptionsObj.css('visibility', 'visible');
	}else
	{
		$OptionsObj.css('visibility', 'hidden');
	}
}

function SetAllStatus(target_status)
{
	var objs = document.getElementsByName('SelectedRecordID[]');
	
	for(var i=0;i<objs.length;i++){
		if(objs[i].checked){
			SetStatus(objs[i].value,target_status);
		}
	}
	
	$('div#batch_status_options').css('visibility', 'hidden');
}

function Set_Absent_To_Present()
{
	var objs = document.getElementsByName('SelectedRecordID[]');
	
	for(var i=0;i<objs.length;i++){
		var status_val = $('#Status_'+objs[i].value).val();
		if(status_val == '<?=CARD_STATUS_ABSENT?>' || status_val == '<?=$CARD_STATUS_ABSENT_SUSPECTED?>'){
			SetStatus(objs[i].value,'<?=CARD_STATUS_NORMAL?>');
		}
	}
}

function Get_Available_Entry_Log_Time(jArrData, jHour, jMin, jSec, jMenu, jPos, jViewElem, jEditElem)
{
	var listStr = "";
	var fieldValue, fieldHour, fieldMinute, fieldSecond;
	var count = 0;
	
	fieldHour = document.getElementById(jHour).value;
	fieldMinute = document.getElementById(jMin).value;
	fieldSecond = document.getElementById(jSec).value;
	fieldValue = fieldHour+':'+fieldMinute+':'+fieldSecond;
	var container = $('#'+jMenu);
	var ref = $('#'+jHour).closest('td');
	var pos = ref.position();
	var btn_hide = (typeof(js_btn_hide)!="undefined") ? js_btn_hide : "";
	var box_height = (jArrData.length>10) ? "200" : "105";
	
	listStr  = "<table width='160' height='100' border='0' cellpadding='0' cellspacing='0'>";
	listStr += "<tr>";
	listStr += "<td style='border:0px; padding:0px;' height='19'><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	listStr += "<tr>";
	listStr += "<td style='border:0px; padding:0px;' width='5' height='19'><img src='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_01.gif"?>' width='5' height='19'></td>";
	listStr += "<td style='border:0px; padding:0px;' height='19' valign='middle' background='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_02.gif"?>'>&nbsp;</td>";
	listStr += "<td style='border:0px; padding:0px;' width='19' height='19'><a href='javascript:void(0)' onClick=\"$('#" + jMenu + "').hide();\"><img src='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_close_off.gif"?>' name='pre_close' width='19' height='19' border='0' id='pre_close' onMouseOver=\"MM_swapImage('pre_close','','<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_close_on.gif"?>',1)\" onMouseOut='MM_swapImgRestore()' /></a></td>";
	listStr += "</tr>";
	listStr += "</table></td>";
	listStr += "</tr>";
	listStr += "<tr>";
	listStr += "<td style='border:0px; padding:0px;' ><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	listStr += "<tr style='border:0px; padding:0px;' >";
	listStr += "<td style='border:0px; padding:0px;' width='5' height='50' background='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_04.gif"?>'><img src='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_04.gif"?>' width='5' height='19'></td>";
	
	listStr += "<td style='border:0px; padding:0px;' bgcolor='#FFFFF7'><div style=\"overflow-x: hidden; overflow-y: scroll; height: " + box_height + "px; width: 160px; \">";
	
	listStr += "<table width='180' border='0' cellspacing='0' cellpadding='2' class='tbclip'>";
	if (typeof(jArrData)=="undefined" || jArrData.length==0)
	{
		if (typeof(js_preset_no_record)!="undefined")
		{
			listStr += "<tr><td style='border:0px; padding:0px;' class='guide' align='center'>"+js_preset_no_record+"</td></tr>";
		}
	} else
	{
		for (var i = 0; i < jArrData.length; i++)
		{
			tmpShowValue = jArrData[i];
			tmpArr = tmpShowValue.split(':');
			
			listStr += "<tr><td style=\"border-bottom:1px #EEEEEE solid; background-color: ";
			if (fieldValue == jArrData[i])
			{
				listStr += "#FFF2C3";
			} else
			{
				listStr += "#FFFFFF";
			}
			listStr += "\">";

			if (fieldValue == jArrData[i])
			{
				listStr += "<b>";
			}
			listStr += "<a class='presetlist' style=\"color:#4288E8\" href=\"javascript:void(0);\"  onclick=\"document.getElementById('" + jHour + "').value = '" + tmpArr[0] + "';document.getElementById('" + jMin + "').value = '" + tmpArr[1] + "';document.getElementById('" + jSec + "').value = '" + tmpArr[2] + "';$('#" + jMenu + "').hide();\" title=\"" + jArrData[i] + "\">" + tmpShowValue + "</a>";
			
			if (fieldValue == jArrData[i])
			{
				listStr += "</b>";
			}

			listStr += "</td></tr>";
			count ++;
		}
	}
	listStr += "</table></div></td>";
	listStr += "<td style='border:0px; padding:0px;' width='6' background='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_06.gif"?>'><img src='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_06.gif"?>' width='6' height='6' border='0' /></td>";
	listStr += "</tr>";
	listStr += "<tr>";
	listStr += "<td style='border:0px; padding:0px;' width='5' height='6'><img src='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_07.gif"?>' width='5' height='6'></td>";
	listStr += "<td style='border:0px; padding:0px;' height='6' background='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_08.gif"?>'><img src='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_08.gif"?>' width='100%' height='6' border='0' /></td>";
	listStr += "<td style='border:0px; padding:0px;' width='6' height='6'><img src='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_09.gif"?>' width='6' height='6'></td>";
	listStr += "</tr>";
	listStr += "</table></td>";
	listStr += "</tr>";
	listStr += "</table>";
	
	container.html(listStr);
	container.css({position:'absolute',
					left:pos.left+'px',
					top:pos.top+ref.height()+'px',
					display:''});
}

$(document).ready(function(){
	Get_Overall_List();
	var timer_func = function(){
		var layers = $('.selectbox_layer');
		for(var i=0;i<layers.length;i++){
			if($(layers[i]).css('visibility') == 'hidden') continue;
			var id = layers[i].id;
			id = 'select_' + id.replace('options_','');
			var parent_div = $('#'+id);
			if(parent_div.length > 0){
				var pos = parent_div.position();
				var offsetY = parent_div.height() + 4;
				$(layers[i]).css({'position':'absolute','left':pos.left+'px','top':(pos.top+offsetY)+'px'});
			}
		}
		setTimeout(timer_func,2000);	
	};
	timer_func();
});
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>