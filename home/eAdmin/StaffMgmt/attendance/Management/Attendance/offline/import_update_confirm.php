<?php
// editing by 
/*
 * 2014-03-27 (Carlos): Change [Real Time Staff Status] to [Staff In-School / In-Out Status]
 */
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-Attendance-Manage') && !$StaffAttend3->Check_Access_Right('REPORT-DAILYLOG'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$sql = "SELECT 
			UserID,
			RecordedTime,
			SiteName 
		FROM
			TEMP_CARD_STAFF_LOG
		ORDER BY
			UserID, RecordedTime ASC";
$import_records = $StaffAttend3->returnArray($sql, 3);

$StaffAttend3->Finalize_Attendance_Offline_Import();

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['Attendance'] = 1;
/*
$TAGS_OBJ[] = array($Lang['StaffAttendance']['AttendanceRecord'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/",0);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['ViewLateAbsentEarlyList'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/daily_profile_list/",0);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['InSchoolInOutStatus'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/real_time_staff_status/",0);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['ImportOfflineRecords'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/offline/",1);
*/
$TAGS_OBJ = $StaffAttend3UI->Get_Management_DailyRecord_Navigation_Tabs('ImportOfflineRecords');
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($Msg));

echo $StaffAttend3UI->Get_Import_Offline_Records_Finish_Page(sizeof($import_records),true);

$linterface->LAYOUT_STOP();
intranet_closedb();
?>