<?php
// editing by 
/*
 * 2015-10-08 (Carlos): Added parameter ShowAbsentSuspectedRecord.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('REPORT-DAILYLOG'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$StaffAttend3UI = new libstaffattend3_ui();

$TargetDate = $_REQUEST['TargetDate'];
$GroupID = $_REQUEST['GroupID'];
$SortFields = array("SortField"=>$_REQUEST['SortField'],"SortOrder"=>$_REQUEST['SortOrder']);
$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
$Task = strtoupper($_REQUEST['Task']);
$ShowAbsentSuspectedRecord = $_REQUEST['ShowAbsentSuspectedRecord'];

$Date = explode('-',$TargetDate);
$Year = $Date[0];
$Month = $Date[1];
$Day = $Date[2];

if($Task=="PRINT") include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
echo $StaffAttend3UI->Get_InOut_Record_List($Keyword, $Year, $Month, $Day, $GroupID, $Task, $SortFields, $ShowAbsentSuspectedRecord);
if($Task=="PRINT") include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");

intranet_closedb();
?>