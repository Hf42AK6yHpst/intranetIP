<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-Attendance'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$StaffAttend3 = new libstaffattend3();

$GroupID = $_REQUEST['GroupID'];
$TargetDate = $_REQUEST['TargetDate'];
$Date = explode('-',$TargetDate);
$Year = $Date[0];
$Month = $Date[1];
$Day = $Date[2];
$Mode = $_REQUEST['Mode'];

//$SlotList = $StaffAttend3->Get_Slot_List($GroupID, "Group");
$SlotList = $StaffAttend3->Get_Attendance_Group_Slot_List($GroupID, $Year, $Month, $Day);

$Slot_Collection = '<select id="SelectSlot" name="SelectSlot" ';
if($Mode==1)
	$Slot_Collection .= ' onchange="Get_Edit_Attendance_Record_Group_List(\'\', \'\')" ';
else 
	$Slot_Collection .= ' onchange="Get_View_Attendance_Record_Group_List(\'\', \'\')" ';
$Slot_Collection .= '>';
if(sizeof($SlotList)==0) $Slot_Collection .= '<option value="">- '.$Lang['StaffAttendance']['NoTimeSlot'].' -</option>';
for($i=0;$i<sizeof($SlotList);$i++)
{
	//$Selected = ($SlotID == $SlotList[$i]['SlotID'])? 'selected':'';
	$Slot_Collection .= '<option value="'.$SlotList[$i]['SlotName'].'" '.$Selected.'>'.$SlotList[$i]['SlotName'].'</option>';
}
$Slot_Collection .= '</select>';

echo $Slot_Collection;

intranet_closedb();
?>