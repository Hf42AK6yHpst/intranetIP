<?php
// editing by 
/*********************************************************** Change Log ******************************************************
 * 2015-03-17 (Carlos): Added missing js CheckWaivedEx()
 * 2014-03-27 (Carlos): Change [Real Time Staff Status] to [Staff In-School / In-Out Status]
 * 2013-04-03 (Carlos): added tag [Import Past Attendance Records] for $sys_custom['StaffAttendance']['ImportPastAttendance']
 ******************************************************************************************************************************/
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-Attendance'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;

$CurrentPage['Attendance'] = 1;
/*
$TAGS_OBJ[] = array($Lang['StaffAttendance']['AttendanceRecord'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/",0);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['ViewLateAbsentEarlyList'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/daily_profile_list/",1);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['InSchoolInOutStatus'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/real_time_staff_status/",0);
if ($StaffAttend3->Check_Access_Right('MGMT-Attendance-Manage') || $StaffAttend3->IS_ADMIN_USER()) {
	$TAGS_OBJ[] = array($Lang['StaffAttendance']['ImportOfflineRecords'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/offline/",0);
	if($sys_custom['StaffAttendance']['ImportPastAttendance']) {
		$TAGS_OBJ[] = array($Lang['StaffAttendance']['ImportPastAttendanceRecords'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/import/",0);
	}
}
*/
$TAGS_OBJ = $StaffAttend3UI->Get_Management_DailyRecord_Navigation_Tabs('ViewLateAbsentEarlyList');
//$TAGS_OBJ_RIGHT[] = array(gen_online_help_btn_and_layer('staff_attendance','attendance_record_tab'));


$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

$TargetDate = $_REQUEST['TargetDate'];
$RecordType = $_REQUEST['RecordType'];

echo $StaffAttend3UI->Get_Profile_List_Index($TargetDate,$RecordType);

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script type="text/JavaScript" language="JavaScript">
// dom function 
{
/*
function Check_Go_Search_Group(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		Get_Attendance_Record_Group_List();
	}
	else
		return false;
}
*/

function EditElement(ViewLayerName, EditLayerName, EditNo)
{
	var ViewLayer = document.getElementById(ViewLayerName+EditNo);
	var EditLayer = document.getElementById(EditLayerName+EditNo);
	ViewLayer.style.display = "none";
	EditLayer.style.display = "inline";
}

function EditRemark(RemarkNo)
{
	var ViewObj = document.getElementById('ViewRemarkLayer'+RemarkNo);
	var EditObj = document.getElementById('EditRemarkLayer'+RemarkNo);
	var HiddenRemarkObj = document.getElementById('Remark'+RemarkNo);
	var TextRemarkObj = document.getElementById('TextRemark'+RemarkNo);
	
	TextRemarkObj.value = HiddenRemarkObj.value;
	
	if(ViewObj.style.display != "none")
	{
		ViewObj.style.display = "none";
		EditObj.style.display = "inline";
	}else
	{
		ViewObj.style.display = "inline";
		EditObj.style.display = "none";
	}
}

function SetRemark(RemarkNo)
{
	var HiddenRemarkObj = document.getElementById('Remark'+RemarkNo);
	var DisplayRemarkObj = document.getElementById('DisplayRemark'+RemarkNo);
	var TextRemarkObj = document.getElementById('TextRemark'+RemarkNo);
	HiddenRemarkObj.value = TextRemarkObj.value;
	DisplayRemarkObj.innerHTML = nl2br(TextRemarkObj.value, false);
	
	EditRemark(RemarkNo);
}

function nl2br (str, is_xhtml)
{
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '' : '<br />';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
}

function ShowHideStatusLayer(selectName, objID, hiddenName, idIndex, indexObj)
{
	// selectName: the status selection dropdown list button (div layer) id which the user clicked to activate the status option list;
	// objID: the status options list layer id;
	// hiddenName: hidden object storing the submitted value;
	// idIndex: index appended to selectID and hiddenID to classify which object is being selected;
	
	// Get the selected div layer position and offsetY
	var $statusObj = $('div#'+selectName+idIndex);
	var $pos = $statusObj.position();
	var offsetY = $statusObj.height()+4;
	var $optionsObj = $('div#'+objID);
	// Get the hidden object that storing the selected index value
	var $targetIndexObj = $('input#'+indexObj);
	
	// Set the options layer object to the selected div layer position
	$optionsObj.css('position', 'absolute');
	$optionsObj.css('left',$pos.left+'px');
	$optionsObj.css('top',($pos.top+offsetY)+'px');
	
	// Highlight the previously selected status item
	$('div#'+objID+' a').removeClass('selected_attendance');
	//$('div#'+objID+' a').eq($('input#'+hiddenName+idIndex).val()).addClass('selected_attendance');
	var $targetHTML = $('div#'+selectName+idIndex+' a').html();
	$('div#'+objID+' a').filter(
		function(index)
		{
			return ($(this).html() == $targetHTML);
		}
	).addClass('selected_attendance');
	
	// toggle visibility of the status options div layer
	if($optionsObj.css('visibility') == 'hidden')
	{
		$optionsObj.css('visibility', 'visible');
	}else
	{
		$optionsObj.css('visibility', 'hidden');
	}
	
	if(idIndex != $targetIndexObj.val())
	{
		$optionsObj.css('visibility', 'visible');
	}
	// Store the target index in the hidden object for later use
	$targetIndexObj.val(idIndex);
}

function SetStatus(thisObj, selectName, objID, hiddenName, statusValue, indexObj)
{
	// thisObj: refers to anchor object;
	// statusValue: 0/1/2/3/4/5/6/7... corresponds to each status;
	
	var $targetIndexObj = $('input#'+indexObj);
	// Remove all highlights first, then highlight the selected item
	var $thisOption = $(thisObj);
	$('div#'+objID+' a').removeClass('selected_attendance');
	$thisOption.addClass('selected_attendance');
	// Set the inner html of anchor tag to be the same content of the selected item; content = [icon + text]
	$('div#'+selectName+$targetIndexObj.val()+' a').html($thisOption.html());
	// Set the hidden object value for submitting for processing
	$('Input#'+hiddenName+$targetIndexObj.val()).val(statusValue);
	// toggle visibility of status options div layer
	ShowHideStatusLayer(selectName, objID, hiddenName, $targetIndexObj.val(), indexObj);
}

// Switch visibility of reason selection boxes according to which status is selected
function ToggleReasonLayers(indexObj, presentFlag, lateFlag, earlyleaveFlag, lateEarlyleaveFlag, outingFlag, absentFlag, holidayFlag)
{
	var $targetIndexObj = $('input#'+indexObj);
	var $absentLayer = $('div#select_absent_status_'+$targetIndexObj.val());
	var $lateLayer = $('div#select_late_status_'+$targetIndexObj.val());
	var $earlyleaveLayer = $('div#select_earlyleave_status_'+$targetIndexObj.val());
	var $outingLayer = $('div#select_outing_status_'+$targetIndexObj.val());
	var $holidayLayer = $('div#select_holiday_status_'+$targetIndexObj.val());
	
	var $ELWaivedCB = $('input#ELWaived'+$targetIndexObj.val());
	var $DoubleBreak = $('#DoubleBreak'+$targetIndexObj.val());
	
	$('div#absent_options').css('visibility','hidden');
	$('div#late_options').css('visibility','hidden');
	$('div#earlyleave_options').css('visibility','hidden');
	$('div#outing_options').css('visibility','hidden');
	$('div#holiday_options').css('visibility','hidden');	
	$ELWaivedCB.css('visibility','hidden');
	$DoubleBreak.css('display','none');
	
	if(presentFlag == 1)
	{
		var $cb_waive = $('input#Waived'+$targetIndexObj.val());
		$cb_waive.attr('checked',false);
		$cb_waive.css('display','none');
		$cb_waive.val(0);
		$absentLayer.css('display','none');
		$lateLayer.css('display','none');
		$earlyleaveLayer.css('display','none');
		$outingLayer.css('display','none');
		$holidayLayer.css('display','none');
	}
	
	if(lateFlag == 1)
	{
		SetDefaultReasonAndWaived($targetIndexObj.val(),'late',false);
		$absentLayer.css('display','none');
		$lateLayer.css('display','block');
		$earlyleaveLayer.css('display','none');
		$outingLayer.css('display','none');
		$holidayLayer.css('display','none');
	}
	
	if(earlyleaveFlag == 1)
	{
		SetDefaultReasonAndWaived($targetIndexObj.val(),'earlyleave',false);
		$absentLayer.css('display','none');
		$lateLayer.css('display','none');
		$earlyleaveLayer.css('display','block');
		$outingLayer.css('display','none');
		$holidayLayer.css('display','none');
	}
	
	if(lateEarlyleaveFlag == 1)
	{
		SetDefaultReasonAndWaived($targetIndexObj.val(),'late',false);
		SetDefaultReasonAndWaived($targetIndexObj.val(),'earlyleave',true);
		$absentLayer.css('display','none');
		$lateLayer.css('display','inline');
		$earlyleaveLayer.css('display','block');
		$outingLayer.css('display','none');
		$holidayLayer.css('display','none');
		$ELWaivedCB.css('visibility','visible');
		$DoubleBreak.css('display','');
	}
	
	if(outingFlag == 1)
	{
		SetDefaultReasonAndWaived($targetIndexObj.val(),'outing',false);
		$absentLayer.css('display','none');
		$lateLayer.css('display','none');
		$earlyleaveLayer.css('display','none');
		$outingLayer.css('display','block');
		$holidayLayer.css('display','none');
	}
	
	if(absentFlag == 1)
	{
		SetDefaultReasonAndWaived($targetIndexObj.val(),'absent',false);
		$absentLayer.css('display','block');
		$lateLayer.css('display','none');
		$earlyleaveLayer.css('display','none');
		$outingLayer.css('display','none');
		$holidayLayer.css('display','none');
	}
	
	if(holidayFlag == 1)
	{
		SetDefaultReasonAndWaived($targetIndexObj.val(),'holiday',false);
		$absentLayer.css('display','none');
		$lateLayer.css('display','none');
		$earlyleaveLayer.css('display','none');
		$outingLayer.css('display','none');
		$holidayLayer.css('display','block');
	}
}

// Set a checkbox's value - if checked set to on, not-checked set to off
function CheckSingle(cbObj, on , off)
{
	if(cbObj.checked)
	{
		cbObj.value = on;
	}else
	{
		cbObj.value = off;
	}
}

// Set the Waived checkbox's value corresponds to the defaultWaive value of the reason 
function CheckWaived(indexObj, waivedValueID, waivedName)
{
	var $index = $('input#'+indexObj).val();
	var $valueObj = $('input#'+waivedValueID);
	var $waivedCheckbox = $('input#'+waivedName+$index);
	if($valueObj.val() == 1)
	{
		$waivedCheckbox.attr('checked',true);
	}
	else
	{
		$waivedCheckbox.attr('checked',false);
	}
}

// Extention version of CheckWaived() for Late & Early Leave
function CheckWaivedEx(indexObj, waivedValueID, waivedName, ELWaivedName, selectLateID)
{
	var $index = $('input#'+indexObj).val();
	//var $index = js_select_index;
	var $valueObj = $('input#'+waivedValueID);
	var $waivedCheckbox = $('input#'+waivedName+$index);
	var $ELWaivedCheckbox = $('input#'+ELWaivedName+$index);
	var $selectLateObj = $('#'+selectLateID+$index);
	
	if($selectLateObj.css('display') != 'none')
	{
		// Check ELwaived checkbox
		if($valueObj.val() == 1)
		{
			$ELWaivedCheckbox.attr('checked',true);
			$ELWaivedCheckbox.val($valueObj.val());
		}
		else
		{
			$ELWaivedCheckbox.attr('checked',false);
			$ELWaivedCheckbox.val($valueObj.val());
		}
	}else
	{
		// Check waived checkbox
		if($valueObj.val() == 1)
		{
			$waivedCheckbox.attr('checked',true);
			$waivedCheckbox.val($valueObj.val());
		}
		else
		{
			$waivedCheckbox.attr('checked',false);
			$waivedCheckbox.val($valueObj.val());
		}
	}
	
}

// retrun Array of Hidden Element values
function Get_Element_Values(ElementName, encodeFlag)
{
	var obj = document.getElementsByName(ElementName);
	var returnArray = new Array();
	
	for(var i=0;i<obj.length;i++)
	{
		if(encodeFlag)
		{
			returnArray[i] = encodeURIComponent(obj[i].value);
		}else
		{
			returnArray[i] = obj[i].value;
		}
	}
	
	return returnArray;
}

function Get_Checked_Hidden_Values(CheckName, HiddenName)
{
	var checkObjs = document.getElementsByName(CheckName);
	var hiddenObjs = document.getElementsByName(HiddenName);
	var returnArray = new Array();
	var valueStr = '';
	for(var i=0;i<checkObjs.length;i++)
	{
		if(checkObjs[i].checked)
		{
			if(valueStr=='')
				valueStr = encodeURIComponent(hiddenObjs[i].value);
			else
				valueStr += ','+encodeURIComponent(hiddenObjs[i].value);
		}
	}
	returnArray = valueStr.split(',');
	return returnArray;
}

function Check_Valid_Date(DateObjId, WarningLayerId)
{
	var dateObj = document.getElementById(DateObjId);
	var $WarningLayer = $('#'+WarningLayerId);
	if(check_date_without_return_msg(dateObj))
	{
		var targetdate = dateObj.value;
		var currentdate = new Date();
		var curYear = '' + currentdate.getFullYear();
		var curMonth = currentdate.getMonth()+1;
		var curDay = currentdate.getDate();
		curMonth = (curMonth<10)? ('0'+curMonth):(''+curMonth);
		curDay = (curDay<10)? ('0'+curDay):(''+curDay);
		var today = curYear + '-' + curMonth + '-' + curDay;
		
		if(targetdate>today)
		{
			dateObj.focus();
			$WarningLayer.html('<?=$Lang['StaffAttendance']['AlertTakeFutureAttendance']?>');
			return false;
		}else
		{
			$WarningLayer.html('');
			return true;
		}
	}else
	{
		dateObj.focus();
		$WarningLayer.html('<?=$Lang['LessonAttendance']['InvalidDateFormat']?>');
		return false;
	}
}

function SetTimerToHideWarning(WarningLayerId, ticks)
{
	var action = "$('#"+WarningLayerId+"').css('visibility','hidden');";
	setTimeout(action,ticks);
}

function ShowPanel(thisObj, PanelID)
{
	var $PanelObj = $('#'+PanelID);
	var $ClickOnObj = $(thisObj);
	var $pos = $ClickOnObj.position();
	var offsetY = $ClickOnObj.height();
	$PanelObj.css('position', 'absolute');
	$PanelObj.css('left',$pos.left+'px');
	$PanelObj.css('top',($pos.top+offsetY)+'px');
	$PanelObj.css('visibility','visible');
	$PanelObj.slideDown();
}

function imposeMaxLength(Object, MaxLen)
{
  if(Object.value.length >= MaxLen)
  {
  	Object.value = Object.value.substr(0,MaxLen-1);
  }
  return (Object.value.length <= MaxLen);
}

function ToggleReasonLayersByIndex(index, presentFlag, lateFlag, earlyleaveFlag, lateEarlyleaveFlag, outingFlag, absentFlag, holidayFlag)
{
	var $absentLayer = $('div#select_absent_status_'+index);
	var $lateLayer = $('div#select_late_status_'+index);
	var $earlyleaveLayer = $('div#select_earlyleave_status_'+index);
	var $outingLayer = $('div#select_outing_status_'+index);
	var $holidayLayer = $('div#select_holiday_status_'+index);
	
	var $ELWaivedCB = $('input#ELWaived'+index);
	var $DoubleBreak = $('#DoubleBreak'+index);
	
	$('div#absent_options').css('visibility','hidden');
	$('div#late_options').css('visibility','hidden');
	$('div#earlyleave_options').css('visibility','hidden');
	$('div#outing_options').css('visibility','hidden');
	$('div#holiday_options').css('visibility','hidden');	
	$ELWaivedCB.css('visibility','hidden');
	$DoubleBreak.css('display','none');
	
	if(presentFlag == 1)
	{
		var $cb_waive = $('input#Waived'+index);
		$cb_waive.attr('checked',false);
		$cb_waive.css('display','none');
		$cb_waive.val(0);
		$absentLayer.css('display','none');
		$lateLayer.css('display','none');
		$earlyleaveLayer.css('display','none');
		$outingLayer.css('display','none');
		$holidayLayer.css('display','none');
	}
	
	if(lateFlag == 1)
	{
		SetDefaultReasonAndWaived(index,'late',false);
		$absentLayer.css('display','none');
		$lateLayer.css('display','block');
		$earlyleaveLayer.css('display','none');
		$outingLayer.css('display','none');
		$holidayLayer.css('display','none');
	}
	
	if(earlyleaveFlag == 1)
	{
		SetDefaultReasonAndWaived(index,'earlyleave',false);
		$absentLayer.css('display','none');
		$lateLayer.css('display','none');
		$earlyleaveLayer.css('display','block');
		$outingLayer.css('display','none');
		$holidayLayer.css('display','none');
	}
	
	if(lateEarlyleaveFlag == 1)
	{
		SetDefaultReasonAndWaived(index,'late',false);
		SetDefaultReasonAndWaived(index,'earlyleave',true);
		$absentLayer.css('display','none');
		$lateLayer.css('display','inline');
		$earlyleaveLayer.css('display','block');
		$outingLayer.css('display','none');
		$holidayLayer.css('display','none');
		$ELWaivedCB.css('visibility','visible');
		$DoubleBreak.css('display','');
	}
	
	if(outingFlag == 1)
	{
		SetDefaultReasonAndWaived(index,'outing',false);
		$absentLayer.css('display','none');
		$lateLayer.css('display','none');
		$earlyleaveLayer.css('display','none');
		$outingLayer.css('display','block');
		$holidayLayer.css('display','none');
	}
	
	if(absentFlag == 1)
	{
		SetDefaultReasonAndWaived(index,'absent',false);
		$absentLayer.css('display','block');
		$lateLayer.css('display','none');
		$earlyleaveLayer.css('display','none');
		$outingLayer.css('display','none');
		$holidayLayer.css('display','none');
	}
	
	if(holidayFlag == 1)
	{
		SetDefaultReasonAndWaived(index,'holiday',false);
		$absentLayer.css('display','none');
		$lateLayer.css('display','none');
		$earlyleaveLayer.css('display','none');
		$outingLayer.css('display','none');
		$holidayLayer.css('display','block');
	}
}

function SetDefaultReasonAndWaived(index,reason_type,el)
{
	var $def_reason_index = $('input#def_'+reason_type+'_reason_index').val();
	var $def_reason_id = $('input#def_'+reason_type+'_reason_id').val();
	var $def_reason_waived = $('input#def_'+reason_type+'_reason_waived').val();
	var $reason_html = $('div#'+reason_type+'_options a:eq('+$def_reason_index+')').html();
	$('div#select_'+reason_type+'_status_'+index+' a:first').html($reason_html);
	$('input#reason_'+reason_type+index).val($def_reason_id);
	
	if(el){
		var $cb_waive_el = $('input#ELWaived'+index);
		if($def_reason_waived==1){
			$cb_waive_el.attr('checked',true);
			$cb_waive_el.val($def_reason_waived);
		}else{
			$cb_waive_el.attr('checked',false);
			$cb_waive_el.val($def_reason_waived);
		}
	}else{
		var $cb_waive = $('input#Waived'+index);
		if($def_reason_waived==1){
			$cb_waive.attr('checked',true);
			$cb_waive.val($def_reason_waived);
		}else{
			$cb_waive.attr('checked',false);
			$cb_waive.val($def_reason_waived);
		}
		$cb_waive.css('display','');
	}
}

}

// ajax function
{
function Get_Profile_List() {
	var TargetDate = $('#TargetDate').val();
	var RecordType = Get_Selection_Value('RecordType','String');
	
	if (Check_Valid_Date('TargetDate','DPWL-TargetDate')) {
		if ($('#DPWL-TargetDate').html() == '') {
			var PostVar = {
				"TargetDate":TargetDate,
				"RecordType":RecordType 
			};
			
			Block_Element('ViewProfileListLayer');
			$('#ViewProfileListLayer').load('ajax_get_profile_list.php',PostVar,
				function (data) {
					if (data == "die") 
						window.top.location = '/';
					else {
						UnBlock_Element('ViewProfileListLayer');
					}
				});
		}
	}
}
}
</script>