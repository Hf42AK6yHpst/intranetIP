<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-Attendance'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$StaffAttend3 = new libstaffattend3();

$StaffID = $_REQUEST['StaffID'];
$TargetDate = $_REQUEST['TargetDate'];

$IndividualList = $StaffAttend3->Get_User_List("",false,false,false,$TargetDate);
$Individual_Collection = '<select id="SelectStaff" name="SelectStaff" onchange="Get_Individual_Slots();">';
for($i=0;$i<sizeof($IndividualList);$i++)
{
	$Selected = ($StaffID == $IndividualList[$i]['UserID'])? 'selected':'';
	$Individual_Collection .= '<option value="'.$IndividualList[$i]['UserID'].'" '.$Selected.'>'.$IndividualList[$i]['UserName'].'</option>';
}
$Individual_Collection .= '</select>';

echo $Individual_Collection;

intranet_closedb();
?>