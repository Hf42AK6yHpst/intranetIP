<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-Attendance'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$StaffAttend3UI = new libstaffattend3_ui();
$TargetDate = ($_REQUEST['TargetDate']=="")?date('Y-m-d'):$_REQUEST['TargetDate'];
$GroupID = $_REQUEST['GroupID'];
$StaffID = $_REQUEST['StaffID'];
$SlotName = trim(urldecode(stripslashes($_REQUEST['SlotName'])));
$StatusType = $_REQUEST['StatusType'];
$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));

echo $StaffAttend3UI->Get_Attendance_Record_All($TargetDate,$GroupID,$StaffID,$SlotName,$Keyword,$StatusType);

intranet_closedb();
?>