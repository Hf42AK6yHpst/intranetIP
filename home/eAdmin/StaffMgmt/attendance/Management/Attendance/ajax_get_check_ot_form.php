<?php
/*
 * 2016-12-08 Carlos: $sys_custom['StaffAttendance']['WongFutNamCollege'] Created for 東華三院黃笏南中學.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-Attendance'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0) || !$sys_custom['StaffAttendance']['WongFutNamCollege']) {
	intranet_closedb();
	exit();
}

$StaffAttend3UI = new libstaffattend3_ui();

$RecordDate = $_POST['RecordDate'];
$StaffID = $_POST['StaffID'];
$OTmins = isset($_POST['OTmins'])? $_POST['OTmins'] : 1; // hour

$staff_record = $StaffAttend3->Get_Staff_Info($StaffID,"","1",false);

$x = '<div class="edit_pop_board" style="height:400px;">
		<div class="edit_pop_board_write" style="height:374px;">
		<form name="modalForm" id="modalForm" onsubmit="return false;">
			<input type="hidden" name="StaffID" id="StaffID" value="'.$StaffID.'">
			<div class="edit_pop_board_write" style="height:343px;overflow-y:auto;">
				<table class="form_table">
					<col class="field_title" />
					<col class="field_c" />
					<tr>
						<td>'.$Lang['StaffAttendance']['Staff'].'</td>
						<td>:</td>
						<td>'.$staff_record[0]['StaffName'].'</td>
					</tr>
					<tr>
						<td>'.$Lang['StaffAttendance']['OTDate'].'<span class="tabletextrequire">*</span></td>
						<td>:</td>
						<td>
							'.$StaffAttend3UI->GET_DATE_PICKER('RecordDate',$RecordDate).'
						</td> 
					</tr>
					<tr>
						<td>'.$Lang['StaffAttendance']['OTHours'].'<span class="tabletextrequire">*</span></td>
						<td>:</td>
						<td>
							'.$StaffAttend3UI->GET_TEXTBOX_NUMBER("OTmins", "OTmins", $OTmins, '', array('onchange'=>'restrictNonZeroPositiveFloatNumber(this);')).'
						</td> 
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>'.$StaffAttend3UI->GET_SMALL_BTN($Lang['StaffAttendance']['Check'], 'button', $ParOnClick="Check_OT_Time();", 'btnCheck', $ParOtherAttribute="", $OtherClass="", $Lang['StaffAttendance']['Check']).'&nbsp;&nbsp;<span id="CheckResultLayer"></span></td>
					</tr>
				</table>
			</div>
			'.$StaffAttend3UI->MandatoryField().'
		</form>
		</div>
	</div>';

echo $x;

intranet_closedb();
?>