<?php
// editing by 
set_time_limit(0);
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-Attendance-Manage') && !$StaffAttend3->Check_Access_Right('REPORT-DAILYLOG'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)
		|| !$sys_custom['StaffAttendance']['ImportPastAttendance']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$ResultAry = array();


$sql = "SELECT * FROM TEMP_CARD_STAFF_IMPORT_PAST_ATTENDANCE ORDER BY RecordDate,UserLogin";
$records = $StaffAttend3->returnResultSet($sql);
$record_count = count($records);

for($i=0;$i<$record_count;$i++) {
	$record_date = $records[$i]['RecordDate'];
	$user_id = $records[$i]['UserID'];
	$time_slot = $records[$i]['TimeSlot'];
	$duty_start = $records[$i]['DutyStart'];
	$duty_end = $records[$i]['DutyEnd'];
	$duty_count = $records[$i]['DutyCount'];
	$in_waive = $records[$i]['InWaive'];
	$out_waive = $records[$i]['OutWaive'];
	$in_status = $records[$i]['InStatus'];
	$in_time = $records[$i]['InTime'];
	$in_station = $records[$i]['InStation'];
	$in_waived = $records[$i]['InWaived'];
	$out_status = $records[$i]['OutStatus'];
	$out_time = $records[$i]['OutTime'];
	$out_station = $records[$i]['OutStation'];
	$out_waived = $records[$i]['OutWaived'];
	
	$in_time_val = "'".$in_time."'";
	if($in_time == '' || $in_time == '00:00:00') {
		$in_time_val = 'NULL';
	}
	$out_time_val = "'".$out_time."'";
	if($out_time == '' || $out_time == '00:00:00') {
		$out_time_val = 'NULL';
	}
	
	$tempYear = substr($record_date,0,4);
    $tempMonth = substr($record_date,5,2);
    $tempDay = intval(substr($record_date,8,2));
	
	$card_log_table_name = "CARD_STAFF_ATTENDANCE2_DAILY_LOG_".$tempYear."_".$tempMonth;
	
	if($time_slot == '' && ($in_status == CARD_STATUS_HOLIDAY || $out_status == CARD_STATUS_HOLIDAY)) {
		// full day holiday
		$sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_PROFILE
				(
					StaffID, 
					RecordDate,
					RecordType,
					Waived,
					ProfileCountFor,
					DateInput,
					DateModified
				)
				VALUES
				(
					'".$user_id."',
					'$record_date',
					'".CARD_STATUS_HOLIDAY."',
					'".$in_waived."',
					'$duty_count',
					NOW(),
					NOW()
				)";
					
		$ResultAry['Profile_'.$user_id.'_'.$record_date] =  $StaffAttend3->db_db_query($sql);
		$ProfileID = $StaffAttend3->db_insert_id();
				
		$sql = "INSERT INTO ".$card_log_table_name."
						(
							StaffID, 
							DayNumber,
							Duty,
							InSchoolStatus,
							OutSchoolStatus,
							RecordType,
							InAttendanceRecordID,
							InWaived,
							DateInput,
							DateModified,
							InputBy,
							ModifyBy 
						)
						VALUES
						(
							'".$user_id."',
							'".$tempDay."',
							'0',
							'".CARD_STATUS_HOLIDAY."',
							'".CARD_STATUS_HOLIDAY."',
							'".CARD_STATUS_HOLIDAY."',
							'$ProfileID',
							".$in_waived.",
							NOW(),
							NOW(),
							'".$_SESSION['UserID']."',
							'".$_SESSION['UserID']."'
						)";
		$ResultAry['Cardlog_'.$user_id.'_'.$record_date] = $StaffAttend3->db_db_query($sql);
		
	} else if ($time_slot == '' && ($in_status == CARD_STATUS_OUTGOING || $out_status == CARD_STATUS_OUTGOING)) {
		// full day outing
		$sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_PROFILE
				(
					StaffID, 
					RecordDate,
					RecordType,
					Waived,
					ProfileCountFor,
					DateInput,
					DateModified
				)
				VALUES
				(
					'".$user_id."',
					'$record_date',
					'".CARD_STATUS_OUTGOING."',
					'".$in_waived."',
					'$duty_count',
					NOW(),
					NOW()
				)";
					
		$StaffAttend3->db_db_query($sql);
		$ProfileID = $StaffAttend3->db_insert_id();
				
		$sql = "INSERT INTO ".$card_log_table_name."
						(
							StaffID, 
							DayNumber,
							Duty,
							InSchoolStatus,
							OutSchoolStatus,
							RecordType,
							InAttendanceRecordID,
							InWaived,
							DateInput,
							DateModified,
							InputBy,
							ModifyBy 
						)
						VALUES
						(
							'".$user_id."',
							'".$tempDay."',
							'0',
							'".CARD_STATUS_OUTGOING."',
							'".CARD_STATUS_OUTGOING."',
							'".CARD_STATUS_OUTGOING."',
							'$ProfileID',
							".$in_waived.",
							NOW(),
							NOW(),
							'".$_SESSION['UserID']."',
							'".$_SESSION['UserID']."'
						)";
		$ResultAry['Cardlog_'.$user_id.'_'.$record_date] = $StaffAttend3->db_db_query($sql);
	} else {
		
		$extra_fields = "";
		$extra_values = "";
		if($in_status == CARD_STATUS_LATE) {
			
			$min_late = floor((strtotime($record_date." ".$in_time) - strtotime($record_date." ".$duty_stat))/60);//Late minutes
			
			$sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_PROFILE
					(
						StaffID, 
						RecordDate,
						RecordType,
						Waived,
						ProfileCountFor,
						DateInput,
						DateModified
					)
					VALUES
					(
						'$user_id',
						'$record_date',
						'".CARD_STATUS_LATE."',
						'".$in_waived."',
						'$duty_count',
						NOW(),
						NOW()
					)";
			$StaffAttend3->db_db_query($sql);
			$ProfileID = $StaffAttend3->db_insert_id();
			
			$extra_fields .= "InAttendanceRecordID,MinLate,";
			$extra_values .= "'$ProfileID','$min_late',";
			
		} else if($in_status == CARD_STATUS_HOLIDAY) {
			
			$sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_PROFILE
					(
						StaffID, 
						RecordDate,
						RecordType,
						Waived,
						ProfileCountFor,
						DateInput,
						DateModified
					)
					VALUES
					(
						'$user_id',
						'$record_date',
						'".CARD_STATUS_HOLIDAY."',
						'".$in_waived."',
						'$duty_count',
						NOW(),
						NOW()
					)";
			$StaffAttend3->db_db_query($sql);
			$ProfileID = $StaffAttend3->db_insert_id();
			
			$extra_fields .= "InAttendanceRecordID,RecordType,";
			$extra_values .= "'$ProfileID','".CARD_STATUS_HOLIDAY."',";
		} else if ($in_status == CARD_STATUS_OUTGOING) {
			$sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_PROFILE
					(
						StaffID, 
						RecordDate,
						RecordType,
						Waived,
						ProfileCountFor,
						DateInput,
						DateModified
					)
					VALUES
					(
						'$user_id',
						'$record_date',
						'".CARD_STATUS_OUTGOING."',
						'".$in_waived."',
						'$duty_count',
						NOW(),
						NOW()
					)";
			$StaffAttend3->db_db_query($sql);
			$ProfileID = $StaffAttend3->db_insert_id();
			
			$extra_fields .= "InAttendanceRecordID,RecordType,";
			$extra_values .= "'$ProfileID','".CARD_STATUS_OUTGOING."',";
		}
		
		if($out_status == CARD_STATUS_EARLYLEAVE) {
			
			$min_earlyleave = floor((strtotime($$record_date." ".$duty_end) - strtotime($record_date." ".$out_time))/60);//Early Leave minutes
			
			$sql = "INSERT INTO CARD_STAFF_ATTENDANCE2_PROFILE
					(
						StaffID, 
						RecordDate,
						RecordType,
						Waived,
						ProfileCountFor,
						DateInput,
						DateModified
					)
					VALUES
					(
						'$user_id',
						'$record_date',
						'".CARD_STATUS_EARLYLEAVE."',
						'".$out_waived."',
						'$duty_count',
						NOW(),
						NOW()
					)";
			$StaffAttend3->db_db_query($sql);
			$ProfileID = $StaffAttend3->db_insert_id();
			
			$extra_fields .= "OutAttendanceRecordID,MinEarlyLeave,";
			$extra_values .= "'$ProfileID','$min_earlyleave',";
			
		} 
		
		$sql = "INSERT INTO ".$card_log_table_name."
				(
					StaffID, 
					DayNumber,
					SlotName,
					Duty,
					DutyStart,
					DutyEnd,
					DutyCount,
					InWavie,
					OutWavie,
					InTime,
					InSchoolStatus,
					InSchoolStation,
					InWaived,
					OutTime,
					OutSchoolStatus,
					OutSchoolStation,
					OutWaived,
					$extra_fields 
					DateInput,
					DateModified,
					InputBy,
					ModifyBy 
				)
				VALUES
				(
					'$user_id',
					'$tempDay',
					'".$StaffAttend3->Get_Safe_Sql_Query($time_slot)."',
					'1',
					'$duty_start',
					'$duty_end',
					'$duty_count',
					'$in_waive',
					'$out_waive',
					$in_time_val,
					'$in_status',
					'".$StaffAttend3->Get_Safe_Sql_Query($in_station)."',
					'$in_waived',
					$out_time_val,
					'$out_status',
					'".$StaffAttend3->Get_Safe_Sql_Query($out_station)."',
					'$out_waived',
					$extra_values 
					NOW(),
					NOW(),
					'$UserID',
					'$UserID' 
				)";
				//debug_r($sql);
		$ResultAry['Cardlog_'.$user_id.'_'.$record_date] = $StaffAttend3->db_db_query($sql);
		
		
	}
	
	// Delete no duty dummy record on that day
	$sql = "DELETE FROM ".$card_log_table_name." WHERE StaffID='$user_id' AND DayNumber='$tempDay' AND RecordType='".CARD_STATUS_NOSETTING."'";
	$StaffAttend3->db_db_query($sql);
	
}

if(!in_array(false,$ResultAry)) {
	$Msg = $Lang['General']['ReturnMessage']['ImportSuccess'];
}else {
	$Msg = $Lang['General']['ReturnMessage']['ImportUnsuccess'];
}
header("Location: import.php?Msg=".rawurlencode($Msg));

intranet_closedb();
?>