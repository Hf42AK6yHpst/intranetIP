<?php
// editing by 
/************************************************* Changes ********************************************************
 * 
 ******************************************************************************************************************/
set_time_limit(0);
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-Attendance-Manage') && !$StaffAttend3->Check_Access_Right('REPORT-DAILYLOG'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)
		|| !$sys_custom['StaffAttendance']['ImportPastAttendance']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();
$limport = new libimporttext();
$lf = new libfilesystem();

$format_array = array("Date","UserLogin","Time Slot","Duty Start Time","Duty End Time","Duty Count","No Tap Card In","No Tap Card Out","In Status","In Time","In Station","In Waived","Out Status","Out Time","Out Station","Out Waived");
$filepath = $_FILES["userfile"]["tmp_name"];
$filename = $_FILES["userfile"]["name"];

$recordAry = array();
$errorAry = array(); // array("Row Number","Error Description");
$importedAry = array(); // array[UserID][Date][] = array(); store imported csv data for checking duplication later on
$insertValues = array();

$timeSlotRestrictedChars = array('%', '&', '^', '@', '"', '\\', '\'', ':', ';', '<', '>', ',', '.', '/', '?', '[', ']', '~', '!', '#', '$', '*', '(', ')', '{', '}', '|', '=', '+', '-');
$inStatusCodes = array(CARD_STATUS_NORMAL, CARD_STATUS_ABSENT, CARD_STATUS_LATE, CARD_STATUS_HOLIDAY, CARD_STATUS_OUTGOING);
$outStatusCodes = array(CARD_STATUS_NORMAL, CARD_STATUS_ABSENT, CARD_STATUS_EARLYLEAVE, CARD_STATUS_HOLIDAY, CARD_STATUS_OUTGOING);
$fulldayStatusCodes = array(CARD_STATUS_HOLIDAY, CARD_STATUS_OUTGOING);

if($filepath=="none" || $filepath == "")
{
	# import failed
    header("Location: import.php?Msg=".urlencode($Lang['StaffAttendance']['DataImportFail']));
    intranet_closedb();
    exit();
}else
{
	$ext = strtoupper($lf->file_ext($filename));
    if($limport->CHECK_FILE_EXT($filename))
    {
        # read file into array
        # return 0 if fail, return csv array if success
        //$data = $lf->file_read_csv($filepath);
        $data = $limport->GET_IMPORT_TXT($filepath);
        if(sizeof($data)>0)
        {
        	$toprow = array_shift($data);                   # drop the title bar
        }else
        {
        	header("Location: import.php?Msg=".urlencode($Lang['StaffAttendance']['DataImportFail']));
		    intranet_closedb();
		    exit();
        }
    }
    
    $data_count = count($data);
    $recordAry = $data;
    
	 // Create temp table
    $sql = "DROP TABLE TEMP_CARD_STAFF_IMPORT_PAST_ATTENDANCE";
    $StaffAttend3->db_db_query($sql);
    $sql = "CREATE TABLE TEMP_CARD_STAFF_IMPORT_PAST_ATTENDANCE (
             	RecordDate date NOT NULL,
			 	UserLogin varchar(255) NOT NULL,
				UserID int(11),
				TimeSlot varchar(255),
             	DutyStart time,
				DutyEnd time,
				DutyCount double,
				InWaive int(1),
				OutWaive int(1),
				InStatus int(11),
				InTime time,
				InStation varchar(100),
				InWaived int(1),
				OutStatus int(11),
				OutTime time,
				OutStation varchar(100),
				OutWaived int(1)  
            )ENGINE=InnoDB DEFAULT CHARSET=utf8";
    $StaffAttend3->db_db_query($sql);
	
	# UserLogin -> UserID
    $sql = "SELECT UserID, UserLogin FROM INTRANET_USER
                   WHERE RecordStatus='1' AND RecordType='".USERTYPE_STAFF."'";
    $temp = $StaffAttend3->returnArray($sql);
    $temp_count = count($temp);
    
    $userloginToUserIdAry = array();
    for ($i=0; $i<$temp_count; $i++)
    {
         list($user_id, $user_login) = $temp[$i];
         $userloginToUserIdAry[$user_login] = $user_id;
    }
	
	$today_ts = strtotime(date("Y-m-d"));
	
	for($i=0; $i< $data_count;$i++) {
		list($record_date, $user_login, $time_slot, $duty_start, $duty_end, $duty_count, $in_waive, $out_waive, $in_status, $in_time, $in_station, $in_waived, $out_status, $out_time, $out_station, $out_waived) = $data[$i];
		
		$row_num = $i+2; // csv row number 
		$record_date = getDefaultDateFormat($record_date);
		
		// Check date 
		$ts = strtotime($record_date);             
	     if($ts==-1 || preg_match('/^\d\d\d\d-\d\d-\d\d$/', $record_date)==0)
	     { 
	     	## invalid date format
	     	if(!isset($errorAry[$row_num])) {
	     		$errorAry[$row_num] = array();
	     	}
	 		$errorAry[$row_num][] = $Lang['StaffAttendance']['Warnings']['InvalidDate'];
	    	continue;
	     }
	     # Further check date format
	     $tempYear = substr($record_date,0,4);
	     $tempMonth = substr($record_date,5,2);
	     $tempDay = substr($record_date,8,2);
	     
	     if(!checkdate(intval($tempMonth), intval($tempDay), intval($tempYear)))
	     {
	     	if(!isset($errorAry[$row_num])) {
	     		$errorAry[$row_num] = array();
	     	}
	     	$errorAry[$row_num][] = $Lang['StaffAttendance']['Warnings']['InvalidDate'];
	    	continue;
	     }
		
		if($ts >= $today_ts) {
			if(!isset($errorAry[$row_num])) {
	     		$errorAry[$row_num] = array();
	     	}
	     	
	     	$errorAry[$row_num][] = $Lang['StaffAttendance']['Warnings']['MustBePastDateRecord'];
	     	continue;
		}
		
		// Check staff user presence
		if(!isset($userloginToUserIdAry[$user_login])) {
			if(!isset($errorAry[$row_num])) {
	     		$errorAry[$row_num] = array();
	     	}
	     	
	     	$errorAry[$row_num][] = $Lang['StaffAttendance']['Warnings']['StaffUserNotFound'];
			continue;
		}
		
		$user_id = $userloginToUserIdAry[$user_login];
		
		$card_log_table_name = "CARD_STAFF_ATTENDANCE2_DAILY_LOG_".$tempYear."_".$tempMonth;
		
		$sql = "SELECT * FROM ".$card_log_table_name." 
				WHERE StaffID='$user_id' AND DayNumber='".intval($tempDay)."' AND (RecordType <> '".CARD_STATUS_NOSETTING."' OR RecordType IS NULL)";
		$tempCardlog = $StaffAttend3->returnResultSet($sql);
		$tempCardlog_count = count($tempCardlog);
		
		$is_fullday_holiday = false;
		$is_fullday_outing = false;
		
		$is_import_fullday = false;
		
		if($time_slot=="" && ($in_status == CARD_STATUS_HOLIDAY || $in_status == CARD_STATUS_OUTGOING 
			|| $out_status == CARD_STATUS_HOLIDAY || $out_status == CARD_STATUS_OUTGOING))
		{
			$is_import_fullday = true;
		}
		
		// Check duty 
		if(!$is_import_fullday) { // not full day
			 // Check time slot 
			 if($time_slot == "") {
			 	if(!isset($errorAry[$row_num])) {
		     		$errorAry[$row_num] = array();
		     	}
		     	
		     	$errorAry[$row_num][] = $Lang['StaffAttendance']['Warnings']['InvalidTimeSlot'];
			 }
			 
			$replaced_time_slot = str_replace($timeSlotRestrictedChars," ",$time_slot);
			if($replaced_time_slot != $time_slot) {
				if(!isset($errorAry[$row_num])) {
		     		$errorAry[$row_num] = array();
		     	}
		     	
		     	$errorAry[$row_num][] =$Lang['StaffAttendance']['Warnings']['TimeSlotHasRestrictedChars'];
			}
			// check duty start time
			if(!preg_match('/^\d\d:\d\d:\d\d$/',$duty_start)) {
				if(!isset($errorAry[$row_num])) {
		     		$errorAry[$row_num] = array();
		     	}
		     	
		     	$errorAry[$row_num][] =$Lang['StaffAttendance']['Warnings']['InvalidDutyStartTime'];
			}
			// check duty end time
			if(!preg_match('/^\d\d:\d\d:\d\d$/',$duty_end)) {
				if(!isset($errorAry[$row_num])) {
		     		$errorAry[$row_num] = array();
		     	}
		     	
		     	$errorAry[$row_num][] = $Lang['StaffAttendance']['Warnings']['InvalidDutyEndTime'];
			}
			
			// check duty count
			$duty_count = floatval($duty_count);
			if($duty_count <= 0.0 || $duty_count > 1.0) {
				if(!isset($errorAry[$row_num])) {
		     		$errorAry[$row_num] = array();
		     	}
		     	
		     	$errorAry[$row_num][] = $Lang['StaffAttendance']['Warnings']['InvalidDutyCountValue'];
			}
			
			// auto correct value
			if($in_waive != '1') {
				$in_waive = '';
			}
			
			// auto correct value
			if($out_waive != '1') {
				$out_waive = '';
			}
			
			// Check in status
			if(!in_array($in_status, $inStatusCodes) ) {
				if(!isset($errorAry[$row_num])) {
		     		$errorAry[$row_num] = array();
		     	}
		     	
		     	$errorAry[$row_num][] = $Lang['StaffAttendance']['Warnings']['InvalidInStatus'];
			}
			
			// Check out status
			if(!in_array($out_status, $outStatusCodes) ) {
				if(!isset($errorAry[$row_num])) {
		     		$errorAry[$row_num] = array();
		     	}
		     	
		     	$errorAry[$row_num][] = $Lang['StaffAttendance']['Warnings']['InvalidOutStatus'];
			}
			
			
			
		} else { // full day
			
			// Check in status
			if(!in_array($in_status, $fulldayStatusCodes) ) {
				if(!isset($errorAry[$row_num])) {
		     		$errorAry[$row_num] = array();
		     	}
		     	
		     	$errorAry[$row_num][] =$Lang['StaffAttendance']['Warnings']['InvalidInStatus'];
			}
			
			// Check out status
			if(!in_array($out_status, $fulldayStatusCodes) ) {
				if(!isset($errorAry[$row_num])) {
		     		$errorAry[$row_num] = array();
		     	}
		     	
		     	$errorAry[$row_num][] = $Lang['StaffAttendance']['Warnings']['InvalidOutStatus'];
			}
			
		}
		
		if($tempCardlog_count > 0) {
			if(!isset($errorAry[$row_num])) {
	     		$errorAry[$row_num] = array();
	     	}
	     	
	     	$errorAry[$row_num][] = str_replace("<!--DATE-->",$record_date,$Lang['StaffAttendance']['Warnings']['AlreadyHasDutyOnDate']);;
		}
		
		// Check existing duty
		for($j=0; $j < $tempCardlog_count; $j++) {
			$temp_time_slot = trim($tempCardlog[$j]['SlotName']);
			$temp_duty_start = $tempCardlog[$j]['DutyStart'];
			$temp_duty_end = $tempCardlog[$j]['DutyEnd'];
			$temp_in_status = $tempCardlog[$j]['InSchoolStatus'];
			$temp_out_status = $tempCardlog[$j]['OutSchoolStatus'];
			$temp_record_type = $tempCardlog[$j]['RecordType'];
			
			if($temp_time_slot == "" && $temp_record_type == CARD_STATUS_HOLIDAY) {
				$is_fullday_holiday = true;
			}
			
			if($temp_time_slot == "" && $temp_record_type == CARD_STATUS_OUTGOING) {
				$is_fullday_outing = true;
			}
			
			if(!$is_fullday_holiday && !$is_fullday_outing && !$is_import_fullday) {
				
				if(($duty_start >= $temp_duty_start && $duty_start <= $temp_duty_end) 
					|| ($duty_end >= $temp_duty_start && $duty_end <= $temp_duty_end) 
					|| ($duty_start <= $temp_duty_start && $duty_end >= $temp_duty_end)) 
				{
					if(!isset($errorAry[$row_num])) {
			     		$errorAry[$row_num] = array();
			     	}
			     	
			     	$errorAry[$row_num][] = str_replace("<!--TIMESLOT-->","[".$temp_time_slot." (".$temp_duty_start." - ".$temp_duty_end.")]",$Lang['StaffAttendance']['Warnings']['DutyTimeOverlapWithDuty']);
				}
			}
		}
		
		// Check already imported duty
		if(isset($importedAry[$user_id][$record_date]) && isset($importedAry[$user_id][$record_date]) && count($importedAry[$user_id][$record_date])) {
			foreach($importedAry[$user_id][$record_date] as $imported_ary)
			{
				$temp_time_slot = trim($imported_ary[2]);
				$temp_duty_start = $imported_ary[3];
				$temp_duty_end = $imported_ary[4];
				$temp_in_status = $imported_ary[8];
				$temp_out_status = $imported_ary[12];
				
				if($temp_time_slot == "" && ($temp_in_status == CARD_STATUS_HOLIDAY || $temp_out_status == CARD_STATUS_HOLIDAY)) {
					$is_fullday_holiday = true;
				}
				
				if($temp_time_slot == "" && ($temp_in_status == CARD_STATUS_OUTGOING || $temp_out_status == CARD_STATUS_OUTGOING)) {
					$is_fullday_outing = true;
				}
				
				if(!$is_fullday_holiday && !$is_fullday_outing &&  !$is_import_fullday) {
					
					if(($duty_start >= $temp_duty_start && $duty_start <= $temp_duty_end) 
						|| ($duty_end >= $temp_duty_start && $duty_end <= $temp_duty_end) 
						|| ($duty_start <= $temp_duty_start && $duty_end >= $temp_duty_end)) 
					{
						if(!isset($errorAry[$row_num])) {
				     		$errorAry[$row_num] = array();
				     	}
				     	
				     	$errorAry[$row_num][] = str_replace("<!--TIMESLOT-->","[".$temp_time_slot." (".$temp_duty_start." - ".$temp_duty_end.")]",$Lang['StaffAttendance']['Warnings']['DutyTimeOverlapWithDuty']);
					}
				}
				
			}
		}
		
		if($is_fullday_holiday) {
			if(!isset($errorAry[$row_num])) {
	     		$errorAry[$row_num] = array();
	     	}
	     	
	     	$errorAry[$row_num][] = $Lang['StaffAttendance']['Warnings']['StaffIsFulldayDayoff'];
			continue;
		}
		
		if($is_fullday_outing) {
			if(!isset($errorAry[$row_num])) {
	     		$errorAry[$row_num] = array();
	     	}
	     	
	     	$errorAry[$row_num][] = $Lang['StaffAttendance']['Warnings']['StaffIsFulldayOuting'];
			continue;
		}
		
		
		
		if(!isset($importedAry[$user_id])) {
			$importedAry[$user_id] = array();
		}
		if(!isset($importedAry[$user_id][$record_date])) {
			$importedAry[$user_id][$record_date] = array();
		}
		
		$importedAry[$user_id][$record_date][] = $data[$i];
		
		$time_slot = $StaffAttend3->Get_Safe_Sql_Query($time_slot);
		$in_station = $StaffAttend3->Get_Safe_Sql_Query($in_station);
		$out_station = $StaffAttend3->Get_Safe_Sql_Query($out_station);
		
		$insertValues[] = "('$record_date','$user_login','$user_id','$time_slot','$duty_start','$duty_end','$duty_count','$in_waive','$out_waive','$in_status','$in_time','$in_station','$in_waived','$out_status','$out_time','$out_station','$out_waived')";
		
		if( ($i % 50)==0 ) {
			ob_flush();
			flush();
			usleep(100000);
		}
	}
	
	$insertChunkAry = array_chunk($insertValues,500);
	for($i=0;$i<count($insertChunkAry);$i++) {
		$sql = "INSERT INTO TEMP_CARD_STAFF_IMPORT_PAST_ATTENDANCE (RecordDate,UserLogin,UserID,TimeSlot,DutyStart,DutyEnd,DutyCount,";
		$sql .= "InWaive,OutWaive,InStatus,InTime,InStation,InWaived,OutStatus,OutTime,OutStation,OutWaived) VALUES ";
		$sql .= implode(",",$insertChunkAry[$i]);
		
		$StaffAttend3->db_db_query($sql);
	}
	
}

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['Attendance'] = 1;
$TAGS_OBJ[] = array($Lang['StaffAttendance']['AttendanceRecord'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/",0);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['ViewLateAbsentEarlyList'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/daily_profile_list/",0);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['InSchoolInOutStatus'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/real_time_staff_status/",0);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['ImportOfflineRecords'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/offline/",0);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['ImportPastAttendanceRecords'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/import/",1);
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $StaffAttend3UI->Get_Import_Past_Attendance_Records_Confirm_Page($recordAry, $errorAry);

$linterface->LAYOUT_STOP();
intranet_closedb();
?>