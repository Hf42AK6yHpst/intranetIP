<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-Attendance-Manage') && !$StaffAttend3->Check_Access_Right('REPORT-DAILYLOG'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)
	 	|| !$sys_custom['StaffAttendance']['ImportPastAttendance']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['Attendance'] = 1;
$TAGS_OBJ[] = array($Lang['StaffAttendance']['AttendanceRecord'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/",0);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['ViewLateAbsentEarlyList'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/daily_profile_list/",0);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['InSchoolInOutStatus'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/real_time_staff_status/",0);
if ($StaffAttend3->Check_Access_Right('MGMT-Attendance-Manage') || $StaffAttend3->IS_ADMIN_USER()) {
	$TAGS_OBJ[] = array($Lang['StaffAttendance']['ImportOfflineRecords'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/offline/",0);
	if($sys_custom['StaffAttendance']['ImportPastAttendance']) {
		$TAGS_OBJ[] = array($Lang['StaffAttendance']['ImportPastAttendanceRecords'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/import/",1);
	}
}
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $StaffAttend3UI->Get_Import_Past_Attendance_Records_Index();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>