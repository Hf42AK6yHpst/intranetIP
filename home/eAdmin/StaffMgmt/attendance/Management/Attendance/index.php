<?php
// editing by 
/***************************************** Change log *****************************************************
 * 2019-03-22 (Carlos): Modified EditRemark(RemarkNo) and SetRemark(RemarkNo) to use div instead of iframe.
 * 2016-12-08 (Carlos): $sys_custom['StaffAttendance']['WongFutNamCollege'] - added javascript functions for check ot time customization.
 * 2015-10-22 (Carlos): modified js SetStatus(), show in time, out time, in location, out location and remark edit spans.
 * 2015-10-08 (Carlos): modified js Get_InOut_Record_List(), added parameter ShowAbsentSuspectedRecord.
 * 2014-03-27 (Carlos): Change [Real Time Staff Status] to [Staff In-School / In-Out Status]
 * 2013-04-03 (Carlos): added tag [Import Past Attendance Records] for $sys_custom['StaffAttendance']['ImportPastAttendance']
 * 2012-12-03 (Carlos): modified js ToggleReasonLayers() - added tooltip of refering which status to the waive checkboxes
 * 2011-06-21 (Carlos): Modified js EditRemark() and SetRemark() 
 * 2011-06-09 (Carlos): improved some js performance
 * 2011-05-24 (Carlos): added js Get_Available_Entry_Log_Time() for displaying entry log time selection
 **********************************************************************************************************/
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-Attendance') && !$StaffAttend3->Check_Access_Right('REPORT-DAILYLOG'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;

$RecordType = $_REQUEST['type'];

if ($RecordType=="" || $RecordType=="1") {
	$CurrentPage['Attendance'] = 1;
	/*
	$TAGS_OBJ[] = array($Lang['StaffAttendance']['AttendanceRecord'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/",1);
	$TAGS_OBJ[] = array($Lang['StaffAttendance']['ViewLateAbsentEarlyList'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/daily_profile_list/",0);
	$TAGS_OBJ[] = array($Lang['StaffAttendance']['InSchoolInOutStatus'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/real_time_staff_status/",0);
	if ($StaffAttend3->Check_Access_Right('MGMT-Attendance-Manage') || $StaffAttend3->IS_ADMIN_USER()) {
		$TAGS_OBJ[] = array($Lang['StaffAttendance']['ImportOfflineRecords'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/offline/",0);
		if($sys_custom['StaffAttendance']['ImportPastAttendance']) {
			$TAGS_OBJ[] = array($Lang['StaffAttendance']['ImportPastAttendanceRecords'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/import/",0);
		}
	}
	*/
	$TAGS_OBJ = $StaffAttend3UI->Get_Management_DailyRecord_Navigation_Tabs('AttendanceRecord');
//	$TAGS_OBJ_RIGHT[] = array(gen_online_help_btn_and_layer('staff_attendance','attendance_record_tab'));
}
else {
	$CurrentPage['ReportDailyLog'] = 1;
	$TAGS_OBJ[] = array($Lang['StaffAttendance']['DailyLog'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Report/Attendance/daily_log/",0);
	$TAGS_OBJ[] = array($Lang['StaffAttendance']['InOutRecord'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/index.php?type=2",1);
//	$TAGS_OBJ_RIGHT[] = array(gen_online_help_btn_and_layer('staff_attendance','daily_log'));
}


$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $StaffAttend3UI->Get_Attendance_Index();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script type="text/JavaScript" language="JavaScript">
// dom function 
{
function Check_Go_Search_Group(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		Get_Attendance_Record_Group_List();
	}
	else
		return false;
}

function Check_Go_Search_View_Group_Members(evt, GroupID, SlotName)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		//Get_View_Attendance_Record_Group_List(GroupID, decodeURIComponent(SlotName));
		Get_View_Attendance_Record_Group_List(GroupID);
	}
	else
		return false;
}

function Check_Go_Search_Edit_Group_Members(evt, GroupID, SlotName)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		//Get_Edit_Attendance_Record_Group_List(GroupID, decodeURIComponent(SlotName));
		Get_Edit_Attendance_Record_Group_List(GroupID);
	}
	else
		return false;
}

function Check_Go_Search_Individual(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		Get_Attendance_Record_Individual_List();
	}
	else
		return false;
}

function Check_Go_Search_Edit_Individual(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		Get_Attendance_Record_Individual_List();
	}
	else
		return false;
}

function Check_Go_Search_InOut(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		Get_InOut_Record_List();
	}
	else
		return false;
}

function EditElement(ViewLayerName, EditLayerName, EditNo)
{
	var ViewLayer = $('#'+ViewLayerName+EditNo);
	var EditLayer = $('#'+EditLayerName+EditNo);
	
	ViewLayer.hide();
	EditLayer.show();
}

function EditRemark(RemarkNo)
{
	var RemarkTd = $('td#RemarkTd'+RemarkNo);
	var ViewObj = RemarkTd.find('span#ViewRemarkLayer'+RemarkNo);
	var EditObj = RemarkTd.find('span#EditRemarkLayer'+RemarkNo);
	var HiddenRemarkObj = RemarkTd.find('input#Remark'+RemarkNo);
	var RemarkFrame = RemarkTd.find('#RemarkFrame'+RemarkNo);
	//var TextRemarkObj = RemarkTd.find('textarea#TextRemark'+RemarkNo);
	//TextRemarkObj.val(HiddenRemarkObj.val());
	
	if(ViewObj.css('display')!='none')
	{
		var remark_editor = '<textarea name="TextRemark'+RemarkNo+'" rows="3" wrap="virtual" style="width:140px;height:90px;" id="TextRemark'+RemarkNo+'">'+HiddenRemarkObj.val()+'</textarea>';
		//RemarkFrame.contents().find('body').html(remark_editor);
		RemarkFrame.html(remark_editor);
		ViewObj.hide();
		EditObj.show();
	}else{
		//RemarkFrame.contents().find('body').html('');
		RemarkFrame.html('');
		EditObj.hide();
		ViewObj.show();
	}
}

function SetRemark(RemarkNo)
{
	var RemarkTd = $('td#RemarkTd'+RemarkNo);
	var HiddenRemarkObj = RemarkTd.find('input#Remark'+RemarkNo);
	var DisplayRemarkObj = RemarkTd.find('span#DisplayRemark'+RemarkNo);
	//var TextRemarkObj = RemarkTd.find('textarea#TextRemark'+RemarkNo);
	var RemarkFrame = RemarkTd.find('#RemarkFrame'+RemarkNo);
	//var content = RemarkFrame.contents().find('#TextRemark'+RemarkNo).val();
	var content = RemarkFrame.find('#TextRemark'+RemarkNo).val();
	HiddenRemarkObj.val(content);
	DisplayRemarkObj.html(nl2br(content, false));
	
	EditRemark(RemarkNo);
}

function nl2br (str, is_xhtml)
{
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '' : '<br />';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
}

function ShowHideStatusLayer(selectName, objID, hiddenName, idIndex, indexObj)
{
	// selectName: the status selection dropdown list button (div layer) id which the user clicked to activate the status option list;
	// objID: the status options list layer id;
	// hiddenName: hidden object storing the submitted value;
	// idIndex: index appended to selectID and hiddenID to classify which object is being selected;
	
	// Get the selected div layer position and offsetY
	var $statusObj = $('div#'+selectName+idIndex);
	var $pos = $statusObj.position();
	var offsetY = $statusObj.height()+4;
	var $optionsObj = $('div#'+objID);
	// Get the hidden object that storing the selected index value
	//var $targetIndexObj = $('input#'+indexObj);
	
	// Set the options layer object to the selected div layer position
	$optionsObj.css('position', 'absolute');
	$optionsObj.css('left',$pos.left+'px');
	$optionsObj.css('top',($pos.top+offsetY)+'px');
	
	// Highlight the previously selected status item
	$('div#'+objID+' a').removeClass('selected_attendance');
	//$('div#'+objID+' a').eq($('input#'+hiddenName+idIndex).val()).addClass('selected_attendance');
	var $targetHTML = $('div#'+selectName+idIndex+' a').html();
	$('div#'+objID+' a').filter(
		function(index)
		{
			return ($(this).html() == $targetHTML);
		}
	).addClass('selected_attendance');
	
	// toggle visibility of the status options div layer
	if($optionsObj.css('visibility') == 'hidden')
	{
		$optionsObj.css('visibility', 'visible');
	}else
	{
		$optionsObj.css('visibility', 'hidden');
	}
	
	//if(idIndex != $targetIndexObj.val())
	if(idIndex != js_select_index)
	{
		$optionsObj.css('visibility', 'visible');
	}
	// Store the target index in the hidden object for later use
	//$targetIndexObj.val(idIndex);
	js_select_index = idIndex;
}

function SetStatus(thisObj, selectName, objID, hiddenName, statusValue, indexObj)
{
	// thisObj: refers to anchor object;
	// statusValue: 0/1/2/3/4/5/6/7... corresponds to each status;
	
	//var $targetIndexObj = $('input#'+indexObj);
	// Remove all highlights first, then highlight the selected item
	var $thisOption = $(thisObj);
	$('div#'+objID+' a').removeClass('selected_attendance');
	$thisOption.addClass('selected_attendance');
	// Set the inner html of anchor tag to be the same content of the selected item; content = [icon + text]
	$('div#'+selectName+js_select_index+' a').html($thisOption.html());
	// Set the hidden object value for submitting for processing
	$('Input#'+hiddenName+js_select_index).val(statusValue);
	// toggle visibility of status options div layer
	ShowHideStatusLayer(selectName, objID, hiddenName, js_select_index, indexObj);
	
	$('#InTimeView'+js_select_index).show();
	$('#OutTimeView'+js_select_index).show();
	$('#InStationView'+js_select_index).show();
	$('#OutStationView'+js_select_index).show();
	$('#ViewRemarkLayer'+js_select_index).show();
}

// Switch visibility of reason selection boxes according to which status is selected
function ToggleReasonLayers(indexObj, presentFlag, lateFlag, earlyleaveFlag, lateEarlyleaveFlag, outingFlag, absentFlag, holidayFlag)
{
	//var $targetIndexObj = $('input#'+indexObj);
	var $absentLayer = $('div#select_absent_status_'+js_select_index);
	var $lateLayer = $('div#select_late_status_'+js_select_index);
	var $earlyleaveLayer = $('div#select_earlyleave_status_'+js_select_index);
	var $outingLayer = $('div#select_outing_status_'+js_select_index);
	var $holidayLayer = $('div#select_holiday_status_'+js_select_index);
	
	var $cb_waive = $('input#Waived'+js_select_index);
	var $ELWaivedCB = $('input#ELWaived'+js_select_index);
	var $DoubleBreak = $('#DoubleBreak'+js_select_index);
	
	$('div#absent_options').css('visibility','hidden');
	$('div#late_options').css('visibility','hidden');
	$('div#earlyleave_options').css('visibility','hidden');
	$('div#outing_options').css('visibility','hidden');
	$('div#holiday_options').css('visibility','hidden');
	$cb_waive.attr('title','');	
	$ELWaivedCB.attr('title','').css('visibility','hidden');
	$DoubleBreak.css('display','none');
	
	if(presentFlag == 1)
	{
		$cb_waive.attr('title','<?=$Lang['StaffAttendance']['Present']?>');
		$cb_waive.attr('checked',false);
		$cb_waive.css('display','none');
		$cb_waive.val(0);
		$absentLayer.css('display','none');
		$lateLayer.css('display','none');
		$earlyleaveLayer.css('display','none');
		$outingLayer.css('display','none');
		$holidayLayer.css('display','none');
	}
	
	if(lateFlag == 1)
	{
		$cb_waive.attr('title','<?=$Lang['StaffAttendance']['Late']?>');
		SetDefaultReasonAndWaived(js_select_index,'late',false);
		$absentLayer.css('display','none');
		$lateLayer.css('display','block');
		$earlyleaveLayer.css('display','none');
		$outingLayer.css('display','none');
		$holidayLayer.css('display','none');
	}
	
	if(earlyleaveFlag == 1)
	{
		$cb_waive.attr('title','<?=$Lang['StaffAttendance']['EarlyLeave']?>');
		$ELWaivedCB.attr('title','<?=$Lang['StaffAttendance']['EarlyLeave']?>');
		SetDefaultReasonAndWaived(js_select_index,'earlyleave',false);
		$absentLayer.css('display','none');
		$lateLayer.css('display','none');
		$earlyleaveLayer.css('display','block');
		$outingLayer.css('display','none');
		$holidayLayer.css('display','none');
	}
	
	if(lateEarlyleaveFlag == 1)
	{
		$cb_waive.attr('title','<?=$Lang['StaffAttendance']['Late']?>');
		$ELWaivedCB.attr('title','<?=$Lang['StaffAttendance']['EarlyLeave']?>');
		SetDefaultReasonAndWaived(js_select_index,'late',false);
		SetDefaultReasonAndWaived(js_select_index,'earlyleave',true);
		$absentLayer.css('display','none');
		$lateLayer.css('display','inline');
		$earlyleaveLayer.css('display','block');
		$outingLayer.css('display','none');
		$holidayLayer.css('display','none');
		$ELWaivedCB.css('visibility','visible');
		$DoubleBreak.css('display','');
	}
	
	if(outingFlag == 1)
	{
		$cb_waive.attr('title','<?=$Lang['StaffAttendance']['Outing']?>');
		SetDefaultReasonAndWaived(js_select_index,'outing',false);
		$absentLayer.css('display','none');
		$lateLayer.css('display','none');
		$earlyleaveLayer.css('display','none');
		$outingLayer.css('display','block');
		$holidayLayer.css('display','none');
	}
	
	if(absentFlag == 1)
	{
		$cb_waive.attr('title','<?=$Lang['StaffAttendance']['Absent']?>');
		SetDefaultReasonAndWaived(js_select_index,'absent',false);
		$absentLayer.css('display','block');
		$lateLayer.css('display','none');
		$earlyleaveLayer.css('display','none');
		$outingLayer.css('display','none');
		$holidayLayer.css('display','none');
	}
	
	if(holidayFlag == 1)
	{
		$cb_waive.attr('title','<?=$Lang['StaffAttendance']['Holiday']?>');
		SetDefaultReasonAndWaived(js_select_index,'holiday',false);
		$absentLayer.css('display','none');
		$lateLayer.css('display','none');
		$earlyleaveLayer.css('display','none');
		$outingLayer.css('display','none');
		$holidayLayer.css('display','block');
	}
}

// Set a checkbox's value - if checked set to on, not-checked set to off
function CheckSingle(cbObj, on , off)
{
	if(cbObj.checked)
	{
		cbObj.value = on;
	}else
	{
		cbObj.value = off;
	}
}

// Set the Waived checkbox's value corresponds to the defaultWaive value of the reason 
function CheckWaived(indexObj, waivedValueID, waivedName)
{
	//var $index = $('input#'+indexObj).val();
	var $index = js_select_index;
	var $valueObj = $('input#'+waivedValueID);
	var $waivedCheckbox = $('input#'+waivedName+$index);
	if($valueObj.val() == 1)
	{
		$waivedCheckbox.attr('checked',true);
		$waivedCheckbox.val($valueObj.val());
	}
	else
	{
		$waivedCheckbox.attr('checked',false);
		$waivedCheckbox.val($valueObj.val());
	}
}
// Extention version of CheckWaived() for Late & Early Leave
function CheckWaivedEx(indexObj, waivedValueID, waivedName, ELWaivedName, selectLateID)
{
	//var $index = $('input#'+indexObj).val();
	var $index = js_select_index;
	var $valueObj = $('input#'+waivedValueID);
	var $waivedCheckbox = $('input#'+waivedName+$index);
	var $ELWaivedCheckbox = $('input#'+ELWaivedName+$index);
	var $selectLateObj = $('#'+selectLateID+$index);
	
	if($selectLateObj.css('display') != 'none')
	{
		// Check ELwaived checkbox
		if($valueObj.val() == 1)
		{
			$ELWaivedCheckbox.attr('checked',true);
			$ELWaivedCheckbox.val($valueObj.val());
		}
		else
		{
			$ELWaivedCheckbox.attr('checked',false);
			$ELWaivedCheckbox.val($valueObj.val());
		}
	}else
	{
		// Check waived checkbox
		if($valueObj.val() == 1)
		{
			$waivedCheckbox.attr('checked',true);
			$waivedCheckbox.val($valueObj.val());
		}
		else
		{
			$waivedCheckbox.attr('checked',false);
			$waivedCheckbox.val($valueObj.val());
		}
	}
	
}

// retrun Array of Hidden Element values
function Get_Element_Values(ElementName, encodeFlag)
{
	var obj = document.getElementsByName(ElementName);
	var returnArray = new Array();
	
	for(var i=0;i<obj.length;i++)
	{
		if(encodeFlag)
		{
			returnArray[i] = encodeURIComponent(obj[i].value);
		}else
		{
			returnArray[i] = obj[i].value;
		}
	}
	
	return returnArray;
}

function Get_Checked_Hidden_Values(CheckName, HiddenName)
{
	var checkObjs = document.getElementsByName(CheckName);
	var hiddenObjs = document.getElementsByName(HiddenName);
	var returnArray = new Array();
	var valueStr = '';
	for(var i=0;i<checkObjs.length;i++)
	{
		if(checkObjs[i].checked)
		{
			if(valueStr=='')
				valueStr = encodeURIComponent(hiddenObjs[i].value);
			else
				valueStr += ','+encodeURIComponent(hiddenObjs[i].value);
		}
	}
	returnArray = valueStr.split(',');
	return returnArray;
}

function Check_Valid_Date(DateObjId, WarningLayerId)
{
	var dateObj = document.getElementById(DateObjId);
	var $WarningLayer = $('#'+WarningLayerId);
	if(check_date_without_return_msg(dateObj))
	{
		var targetdate = dateObj.value;
		var currentdate = new Date();
		var curYear = '' + currentdate.getFullYear();
		var curMonth = currentdate.getMonth()+1;
		var curDay = currentdate.getDate();
		curMonth = (curMonth<10)? ('0'+curMonth):(''+curMonth);
		curDay = (curDay<10)? ('0'+curDay):(''+curDay);
		var today = curYear + '-' + curMonth + '-' + curDay;
		
		if(targetdate>today)
		{
			dateObj.focus();
			$WarningLayer.html('<?=$Lang['StaffAttendance']['AlertTakeFutureAttendance']?>');
			$WarningLayer.show();
			SetTimerToHideWarning(WarningLayerId, 3000);
			return false;
		}else
		{
			$WarningLayer.html('');
			$WarningLayer.hide();
			return true;
		}
	}else
	{
		dateObj.focus();
		$WarningLayer.html('<?=$Lang['LessonAttendance']['InvalidDateFormat']?>');
		$WarningLayer.css('visibility','visible');
		SetTimerToHideWarning(WarningLayerId, 3000);
		return false;
	}
}

function Check_Valid_Time(InTimeHourName, InTimeMinName, InTimeSecName, OutTimeHourName, OutTimeMinName, OutTimeSecName, WarningLayerId)
{
	var InHour = document.getElementsByName(InTimeHourName);
	var InMin = document.getElementsByName(InTimeMinName);
	var InSec = document.getElementsByName(InTimeSecName);
	var OutHour = document.getElementsByName(OutTimeHourName);
	var OutMin = document.getElementsByName(OutTimeMinName);
	var OutSec = document.getElementsByName(OutTimeSecName);
	var $WarningLayer = $('#'+WarningLayerId);
	
	for(var i=0;i<InHour.length;i++)
	{
		if(Trim(InHour[i].value)!='' || Trim(InMin[i].value)!='' || Trim(InSec[i].value)!='')
		{
			d_h = parseInt(InHour[i].value);
			d_m = parseInt(InMin[i].value);
			d_s = parseInt(InSec[i].value);
			
			if(InHour[i].value.length != 2 || d_h<0 || d_h>23 || isNaN(d_h))
			{
				InHour[i].focus();
				$WarningLayer.html('<?=$Lang['LessonAttendance']['InvalidDateFormat']?>');
				$WarningLayer.css('visibility','visible');
				SetTimerToHideWarning(WarningLayerId, 3000);
				return false;
			}
			if(InMin[i].value.length != 2 || d_m<0 || d_m>59 || isNaN(d_m))
			{
				InMin[i].focus();
				$WarningLayer.html('<?=$Lang['StaffAttendance']['SlotTimeFormatWarning']?>');
				$WarningLayer.css('visibility','visible');
				SetTimerToHideWarning(WarningLayerId, 3000);
				return false;
			}
			if(InSec[i].value.length != 2 || d_s<0 || d_s>59 || isNaN(d_s))
			{
				InSec[i].focus();
				$WarningLayer.html('<?=$Lang['StaffAttendance']['SlotTimeFormatWarning']?>');
				$WarningLayer.css('visibility','visible');
				SetTimerToHideWarning(WarningLayerId, 3000);
				return false;
			}
		}
    	
    	if(Trim(OutHour[i].value)!='' || Trim(OutMin[i].value)!='' || Trim(OutSec[i].value)!='')
		{
			d_h = parseInt(OutHour[i].value);
			d_m = parseInt(OutMin[i].value);
			d_s = parseInt(OutSec[i].value);
			
			if(OutHour[i].value.length != 2 || d_h<0 || d_h>23 || isNaN(d_h))
			{
				OutHour[i].focus();
				$WarningLayer.html('<?=$Lang['StaffAttendance']['SlotTimeFormatWarning']?>');
				$WarningLayer.css('visibility','visible');
				SetTimerToHideWarning(WarningLayerId, 3000);
				return false;
			}
			if(OutMin[i].value.length != 2 || d_m<0 || d_m>59 || isNaN(d_m))
			{
				OutMin[i].focus();
				$WarningLayer.html('<?=$Lang['StaffAttendance']['SlotTimeFormatWarning']?>');
				$WarningLayer.css('visibility','visible');
				SetTimerToHideWarning(WarningLayerId, 3000);
				return false;
			}
			if(OutSec[i].value.length != 2 || d_s<0 || d_s>59 || isNaN(d_s))
			{
				OutSec[i].focus();
				$WarningLayer.html('<?=$Lang['StaffAttendance']['SlotTimeFormatWarning']?>');
				$WarningLayer.css('visibility','visible');
				SetTimerToHideWarning(WarningLayerId, 3000);
				return false;
			}
		}
	}
	
	$WarningLayer.html('');
	$WarningLayer.css('visibility','hidden');
	return true;
}

function Check_Time_Status(StatusName, InTimeHourName, InTimeMinName, InTimeSecName, OutTimeHourName, OutTimeMinName, OutTimeSecName, WarningLayerId)
{
	var InHour = document.getElementsByName(InTimeHourName);
	var InMin = document.getElementsByName(InTimeMinName);
	var InSec = document.getElementsByName(InTimeSecName);
	var Status = document.getElementsByName(StatusName);
	var $WarningLayer = $('#'+WarningLayerId);
	
	for(i=0;i<Status.length;i++){
		if(Trim(InHour[i].value)!='' && Trim(InMin[i].value)!='' && Trim(InSec[i].value)!='' && Status[i].value=='7'){
			$WarningLayer.html('<?=$Lang['StaffAttendance']['StatusMustSetIfInTimeSetWarning']?>');
			$WarningLayer.css('visibility','visible');
			scroll(0,0);
			return false;
		}
	}
	
	$WarningLayer.html('');
	$WarningLayer.css('visibility','hidden');
	return true;
}

function SetTimerToHideWarning(WarningLayerId, ticks)
{
	var action = "$('#"+WarningLayerId+"').css('visibility','hidden');";
	setTimeout(action,ticks);
}

function ShowPanel(thisObj, PanelID)
{
	var $PanelObj = $('#'+PanelID);
	var $ClickOnObj = $(thisObj);
	var $pos = $ClickOnObj.position();
	var offsetY = $ClickOnObj.height();
	$PanelObj.css('position', 'absolute');
	$PanelObj.css('left',$pos.left+'px');
	$PanelObj.css('top',($pos.top+offsetY)+'px');
	$PanelObj.css('visibility','visible');
	$PanelObj.slideDown();
}

function imposeMaxLength(Object, MaxLen)
{
  if(Object.value.length >= MaxLen)
  {
  	Object.value = Object.value.substr(0,MaxLen-1);
  }
  return (Object.value.length <= MaxLen);
}

function Set_Absent_To_Present()
{
	var $StatusArr = $('input[name="Status[]"]');
	var $StatusPresentHtml = $('div#status_options a:first').html();
	$StatusArr.each(
		function(index){
			var $t = $(this);
			if($t.val()==1 || $t.val()==7){
				$('div#select_status_'+index+' a:first').html($StatusPresentHtml);
				ToggleReasonLayersByIndex(index,1,0,0,0,0,0,0);
				$t.val(0);
			}
		}
	);
}

function ToggleReasonLayersByIndex(index, presentFlag, lateFlag, earlyleaveFlag, lateEarlyleaveFlag, outingFlag, absentFlag, holidayFlag)
{
	var $absentLayer = $('div#select_absent_status_'+index);
	var $lateLayer = $('div#select_late_status_'+index);
	var $earlyleaveLayer = $('div#select_earlyleave_status_'+index);
	var $outingLayer = $('div#select_outing_status_'+index);
	var $holidayLayer = $('div#select_holiday_status_'+index);
	
	var $ELWaivedCB = $('input#ELWaived'+index);
	var $DoubleBreak = $('#DoubleBreak'+index);
	
	$('div#absent_options').css('visibility','hidden');
	$('div#late_options').css('visibility','hidden');
	$('div#earlyleave_options').css('visibility','hidden');
	$('div#outing_options').css('visibility','hidden');
	$('div#holiday_options').css('visibility','hidden');	
	$ELWaivedCB.css('visibility','hidden');
	$DoubleBreak.css('display','none');
	
	if(presentFlag == 1)
	{
		var $cb_waive = $('input#Waived'+index);
		$cb_waive.attr('checked',false);
		$cb_waive.css('display','none');
		$cb_waive.val(0);
		$absentLayer.css('display','none');
		$lateLayer.css('display','none');
		$earlyleaveLayer.css('display','none');
		$outingLayer.css('display','none');
		$holidayLayer.css('display','none');
	}
	
	if(lateFlag == 1)
	{
		SetDefaultReasonAndWaived(index,'late',false);
		$absentLayer.css('display','none');
		$lateLayer.css('display','block');
		$earlyleaveLayer.css('display','none');
		$outingLayer.css('display','none');
		$holidayLayer.css('display','none');
	}
	
	if(earlyleaveFlag == 1)
	{
		SetDefaultReasonAndWaived(index,'earlyleave',false);
		$absentLayer.css('display','none');
		$lateLayer.css('display','none');
		$earlyleaveLayer.css('display','block');
		$outingLayer.css('display','none');
		$holidayLayer.css('display','none');
	}
	
	if(lateEarlyleaveFlag == 1)
	{
		SetDefaultReasonAndWaived(index,'late',false);
		SetDefaultReasonAndWaived(index,'earlyleave',true);
		$absentLayer.css('display','none');
		$lateLayer.css('display','inline');
		$earlyleaveLayer.css('display','block');
		$outingLayer.css('display','none');
		$holidayLayer.css('display','none');
		$ELWaivedCB.css('visibility','visible');
		$DoubleBreak.css('display','');
	}
	
	if(outingFlag == 1)
	{
		SetDefaultReasonAndWaived(index,'outing',false);
		$absentLayer.css('display','none');
		$lateLayer.css('display','none');
		$earlyleaveLayer.css('display','none');
		$outingLayer.css('display','block');
		$holidayLayer.css('display','none');
	}
	
	if(absentFlag == 1)
	{
		SetDefaultReasonAndWaived(index,'absent',false);
		$absentLayer.css('display','block');
		$lateLayer.css('display','none');
		$earlyleaveLayer.css('display','none');
		$outingLayer.css('display','none');
		$holidayLayer.css('display','none');
	}
	
	if(holidayFlag == 1)
	{
		SetDefaultReasonAndWaived(index,'holiday',false);
		$absentLayer.css('display','none');
		$lateLayer.css('display','none');
		$earlyleaveLayer.css('display','none');
		$outingLayer.css('display','none');
		$holidayLayer.css('display','block');
	}
	
	$('#InTimeView'+index).show();
	$('#OutTimeView'+index).show();
	$('#InStationView'+index).show();
	$('#OutStationView'+index).show();
	$('#ViewRemarkLayer'+index).show();
}

function ShowHideBatchStatusOption(clickedObj)
{
	var $OptionsObj = $('div#batch_status_options');
	var $ClickedObj = $(clickedObj);
	var $pos = $ClickedObj.position();
	var offsetY = $ClickedObj.height()+4;
	
	$OptionsObj.css('position', 'absolute');
	$OptionsObj.css('left',$pos.left+'px');
	$OptionsObj.css('top',($pos.top+offsetY)+'px');
	if($OptionsObj.css('visibility') == 'hidden')
	{
		$OptionsObj.css('visibility', 'visible');
	}else
	{
		$OptionsObj.css('visibility', 'hidden');
	}
}

function SetAllStatus(target_status)
{
	var $SelectedArr = $('input[name="SelectRecords[]"]');
	$('input#select_index').val(target_status);
	var optionIndex = 0;
	var presentFlag = 0;
	var lateFlag = 0;
	var earlyleaveFlag = 0;
	var lateEarlyleaveFlag = 0;
	var absentFlag = 0;
	var outingFlag = 0;
	var holidayFlag = 0;
	switch(target_status)
	{
		case 0:optionIndex = 0;presentFlag=1;break;//present
		case 1:optionIndex = 5;absentFlag=1;break;//absent
		case 2:optionIndex = 1;lateFlag=1;break;//late
		case 3:optionIndex = 2;earlyleaveFlag=1;break;//earlyleave
		case 4:optionIndex = 6;holidayFlag=1;break;//holiday
		case 5:optionIndex = 4;outingFlag=1;break;//outing
		case 6:optionIndex = 3;lateEarlyleaveFlag=1;break;//late and earlyleave
	}
	var $StatusHtml = $('div#status_options a:eq('+optionIndex+')').html();
	$SelectedArr.each(
		function(index){
			var $t = $(this);
			if($t.attr('checked')){
				$('input[name="Status[]"]:eq('+index+')').val(target_status);
				$('div#select_status_'+index+' a:first').html($StatusHtml);
				ToggleReasonLayersByIndex(index, presentFlag, lateFlag, earlyleaveFlag, lateEarlyleaveFlag, outingFlag, absentFlag, holidayFlag);
			}
		}
	);
	//$('div#batch_select_status a:first').html($('div#batch_status_options a:eq('+optionIndex+')').html());
	$('div#batch_status_options').css('visibility', 'hidden');
}

function SetDefaultReasonAndWaived(index,reason_type,el)
{
	var $def_reason_index = $('input#def_'+reason_type+'_reason_index').val();
	var $def_reason_id = $('input#def_'+reason_type+'_reason_id').val();
	var $def_reason_waived = $('input#def_'+reason_type+'_reason_waived').val();
	var $reason_html = $('div#'+reason_type+'_options a:eq('+$def_reason_index+')').html();
	$('div#select_'+reason_type+'_status_'+index+' a:first').html($reason_html);
	$('input#reason_'+reason_type+index).val($def_reason_id);
	
	if(el){
		var $cb_waive_el = $('input#ELWaived'+index);
		if($def_reason_waived==1){
			$cb_waive_el.attr('checked',true);
			$cb_waive_el.val($def_reason_waived);
		}else{
			$cb_waive_el.attr('checked',false);
			$cb_waive_el.val($def_reason_waived);
		}
	}else{
		var $cb_waive = $('input#Waived'+index);
		if($def_reason_waived==1){
			$cb_waive.attr('checked',true);
			$cb_waive.val($def_reason_waived);
		}else{
			$cb_waive.attr('checked',false);
			$cb_waive.val($def_reason_waived);
		}
		$cb_waive.css('display','');
	}
}

}

// ajax function
{
function Get_Attendance_Record_Group()
{
	var TargetDate="";
	if($('input#TargetDate')) TargetDate = $('input#TargetDate').val();
	
	var PostVar = {
			"TargetDate": TargetDate,
			"Keyword": encodeURIComponent($('Input#Keyword').val())
		}

	Block_Element("AttendanceRecordLayer");
	$.post('ajax_get_attendance_record_group.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#AttendanceRecordLayer').html(data);
							UnBlock_Element("AttendanceRecordLayer");
							Scroll_To_Top();
						}
					});
}

function Get_Attendance_Record_Individual()
{
	var PostVar = {
			"TargetDate":$('input#TargetDate').val(),
			"DutyType":$('#DutyType').val(),
			"StatusType":$('#StatusType').val(),
			"Keyword": encodeURIComponent($('Input#Keyword').val())
		}
	
	if(Check_Valid_Date('TargetDate','DateWarningLayer'))
	{
		Block_Element("AttendanceRecordLayer");
		$.post('ajax_get_attendance_record_individual.php',PostVar,
						function(data){
							if (data == "die") 
								window.top.location = '/';
							else {
								$('div#AttendanceRecordLayer').html(data);
								UnBlock_Element("AttendanceRecordLayer");
								Scroll_To_Top();
							}
						});
	}
}

function Get_Attendance_Record_Group_List()
{
	var PostVar = {
			"TargetDate":$('input#TargetDate').val(),
			"Keyword": encodeURIComponent($('Input#Keyword').val())
		}
	
	if(Check_Valid_Date('TargetDate','DateWarningLayer'))
	{
		Block_Element("AttendanceRecordGroupListLayer");
		$.post('ajax_get_attendance_record_group_list.php',PostVar,
						function(data){
							if (data == "die") 
								window.top.location = '/';
							else {
								$('div#AttendanceRecordGroupListLayer').html(data);
								UnBlock_Element("AttendanceRecordGroupListLayer");
								Scroll_To_Top();
							}
						});
	}
}
// !!!!! IMPORTANT: Original Design - DO NOT DELETE !!!!!
function Get_View_Attendance_Record_Group_List(GroupID, SlotName)
{
	var GroupID = GroupID || $('#SelectGroup').val();
	var SlotName = SlotName || $('#SelectSlot').val();
	var PostVar = {
			"GroupID": GroupID,
			"SlotName": encodeURIComponent(SlotName),
			"TargetDate":$('input#TargetDate').val(),
			"StatusType":$('#SelectStatus').val(),
			"Keyword": encodeURIComponent($('Input#Keyword').val())
		}
	
	if(Check_Valid_Date('TargetDate','DateWarningLayer'))
	{
		$('#AttendanceRecordLayer').html('');
		Block_Element("AttendanceRecordLayer");
		$.post('ajax_get_view_attendance_record_group_list.php',PostVar,
						function(data){
							if (data == "die") 
								window.top.location = '/';
							else {
								$('#AttendanceRecordLayer').html(data);
								UnBlock_Element("AttendanceRecordLayer");
								Scroll_To_Top();
							}
						});
	}
}

// Original Design
function Get_Edit_Attendance_Record_Group_List(GroupID, SlotName)
{
	var GroupID = GroupID || $('#SelectGroup').val();
	var SlotName = SlotName || $('#SelectSlot').val();
	var PostVar = {
			"GroupID": GroupID,
			"SlotName": encodeURIComponent(SlotName),
			"TargetDate":$('input#TargetDate').val(),
			"StatusType":$('#SelectStatus').val(),
			"Keyword": encodeURIComponent($('Input#Keyword').val())
		}
	
	if(Check_Valid_Date('TargetDate','DateWarningLayer'))
	{
		Block_Element("AttendanceRecordLayer");
		$.post('ajax_get_edit_attendance_record_group_list.php',PostVar,
						function(data){
							if (data == "die") 
								window.top.location = '/';
							else {
								$('div#AttendanceRecordLayer').html(data);
								Scroll_To_Top();
							}
						});
	}
}

function Update_Attendance_Record_Group_List(GroupID, SlotName)
{
	if(!Check_Valid_Time('InHour[]', 'InMin[]', 'InSec[]', 'OutHour[]', 'OutMin[]', 'OutSec[]', 'DateWarningLayer'))
		return;
	
	if(!Check_Time_Status('Status[]','InHour[]', 'InMin[]', 'InSec[]', 'OutHour[]', 'OutMin[]', 'OutSec[]', 'DateWarningLayer'))
		return;
	
	var StaffID = Get_Element_Values('StaffID[]', false);
	var Duty = Get_Element_Values('Duty[]', false);
	var InHour = Get_Element_Values('InHour[]', true);
	var InMin = Get_Element_Values('InMin[]', true);
	var InSec = Get_Element_Values('InSec[]', true);
	var OutHour = Get_Element_Values('OutHour[]', true);
	var OutMin = Get_Element_Values('OutMin[]', true);
	var OutSec = Get_Element_Values('OutSec[]', true);
	var InStation = Get_Element_Values('InStation[]', true);
	var OutStation = Get_Element_Values('OutStation[]', true);
	var Status = Get_Element_Values('Status[]', false);
	var ReasonAbsent = Get_Element_Values('ReasonAbsent[]', false);
	var ReasonLate = Get_Element_Values('ReasonLate[]', false);
	var ReasonEarlyLeave = Get_Element_Values('ReasonEarlyLeave[]', false);
	var ReasonOuting = Get_Element_Values('ReasonOuting[]', false);
	var ReasonHoliday = Get_Element_Values('ReasonHoliday[]', false);
	var Waived = Get_Element_Values('Waived[]', false);
	var ELWaived = Get_Element_Values('ELWaived[]', false);
	var Remark = Get_Element_Values('Remark[]', true);
	
	var GroupID = GroupID || $('#SelectGroup').val();
	var SlotName = SlotName || $('#SelectSlot').val();
	var PostVar = {
			"GroupID": GroupID,
			"SlotName": encodeURIComponent(SlotName),
			"TargetDate":$('input#TargetDate').val(),
			"StatusType":$('#SelectStatus').val(),
			"Keyword": encodeURIComponent($('Input#Keyword').val()),
			"StaffID[]":StaffID,
			"Duty[]":Duty,
			"InHour[]":InHour,
			"InMin[]":InMin,
			"InSec[]":InSec,
			"OutHour[]":OutHour,
			"OutMin[]":OutMin,
			"OutSec[]":OutSec,
			"InStation[]":InStation,
			"OutStation[]":OutStation,
			"Status[]":Status,
			"ReasonAbsent[]":ReasonAbsent,
			"ReasonLate[]":ReasonLate,
			"ReasonEarlyLeave[]":ReasonEarlyLeave,
			"ReasonOuting[]":ReasonOuting,
			"ReasonHoliday[]":ReasonHoliday,
			"Waived[]":Waived,
			"ELWaived[]":ELWaived,
			"Remark[]":Remark,
			"PageLoadTime":$('input#PageLoadTime').val()  
		}
	//Block_Element("AttendanceRecordLayer");//
	$.post('ajax_update_attendance_record_group_list.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							Get_Return_Message(data);
							Get_View_Attendance_Record_Group_List(GroupID, SlotName);
							//Get_Attendance_Record_All();
							//window.top.tb_remove();
							//Scroll_To_Top();
							//$('div#AttendanceRecordLayer').html(data);//
							//Thick_Box_Init();//
							//UnBlock_Element("AttendanceRecordLayer");//
						}
					});
}
// !!!!! IMPORTANT: Original Design - DO NOT DELETE !!!!!
function Get_Attendance_Record_Individual_List()
{
	var PostVar = {
			"TargetDate":$('input#TargetDate').val(),
			"DutyType":$('#DutyType').val(),
			"StatusType":$('#StatusType').val(),
			"Keyword": encodeURIComponent($('Input#Keyword').val())
		}
	
	if(Check_Valid_Date('TargetDate','DateWarningLayer'))
	{
		Block_Element("AttendanceRecordIndividualListLayer");
		$.post('ajax_get_attendance_record_individual_list.php',PostVar,
						function(data){
							if (data == "die") 
								window.top.location = '/';
							else {
								$('div#AttendanceRecordIndividualListLayer').html(data);
								UnBlock_Element("AttendanceRecordIndividualListLayer");
								Scroll_To_Top();
							}
						});
	}
}

// !!!!! IMPORTANT: Original Design - DO NOT DELETE !!!!!
function Get_Edit_Attendance_Record_Individual_List()
{
	var RecordID = Get_Check_Box_Value('EditRecords[]','Array'); // StaffIDs
	var SlotNames = Get_Checked_Hidden_Values('EditRecords[]', 'SlotNames[]'); // SlotNames
	
	if (RecordID.length > 0)
	{
		var PostVar = {
			"RecordID[]":RecordID,
			"SlotNames[]":SlotNames,
			"DutyType":$('#DutyType').val(),
			"StatusType":$('#StatusType').val(),
			"TargetDate":$('input#TargetDate').val(),
			"Keyword": encodeURIComponent($('Input#Keyword').val())
		}
		
		if(Check_Valid_Date('TargetDate','DateWarningLayer'))
		{
			Block_Element("AttendanceRecordLayer");
			$.post('ajax_get_edit_attendance_record_individual_list.php',PostVar,
				function(data) {
					if (data == "die") 
						window.top.location = '/';
					else {
						$('div#AttendanceRecordLayer').html(data);
						UnBlock_Element("AttendanceRecordLayer");
						Scroll_To_Top();
					}
				});
		}
	}
	else {
		alert('<?=$Lang['StaffAttendance']['SelectAtLeastOneWarning']?>');
	}
}

function Update_Attendance_Record_Individual_List()
{
	if(!Check_Valid_Time('InHour[]', 'InMin[]', 'InSec[]', 'OutHour[]', 'OutMin[]', 'OutSec[]', 'DateWarningLayer'))
		return;
	
	if(!Check_Time_Status('Status[]','InHour[]', 'InMin[]', 'InSec[]', 'OutHour[]', 'OutMin[]', 'OutSec[]', 'DateWarningLayer'))
		return;
	
	var StaffID = Get_Element_Values('StaffID[]', false);
	var SlotName = Get_Element_Values('SlotName[]', true);
	var Duty = Get_Element_Values('Duty[]', false);
	var InHour = Get_Element_Values('InHour[]', true);
	var InMin = Get_Element_Values('InMin[]', true);
	var InSec = Get_Element_Values('InSec[]', true);
	var OutHour = Get_Element_Values('OutHour[]', true);
	var OutMin = Get_Element_Values('OutMin[]', true);
	var OutSec = Get_Element_Values('OutSec[]', true);
	var InStation = Get_Element_Values('InStation[]', true);
	var OutStation = Get_Element_Values('OutStation[]', true);
	var Status = Get_Element_Values('Status[]', false);
	var ReasonAbsent = Get_Element_Values('ReasonAbsent[]', false);
	var ReasonLate = Get_Element_Values('ReasonLate[]', false);
	var ReasonEarlyLeave = Get_Element_Values('ReasonEarlyLeave[]', false);
	var ReasonOuting = Get_Element_Values('ReasonOuting[]', false);
	var ReasonHoliday = Get_Element_Values('ReasonHoliday[]', false);
	
	var Waived = Get_Element_Values('Waived[]', false);
	var ELWaived = Get_Element_Values('ELWaived[]', false);
	var Remark = Get_Element_Values('Remark[]', true);
	
	var PostVar = {
			"TargetDate":$('input#TargetDate').val(),
			"StaffID[]":StaffID,
			"SlotName[]": SlotName,
			"Duty[]":Duty,
			"InHour[]":InHour,
			"InMin[]":InMin,
			"InSec[]":InSec,
			"OutHour[]":OutHour,
			"OutMin[]":OutMin,
			"OutSec[]":OutSec,
			"InStation[]":InStation,
			"OutStation[]":OutStation,
			"Status[]":Status,
			"ReasonAbsent[]":ReasonAbsent,
			"ReasonLate[]":ReasonLate,
			"ReasonEarlyLeave[]":ReasonEarlyLeave,
			"ReasonOuting[]":ReasonOuting,
			"ReasonHoliday[]":ReasonHoliday,
			"Waived[]":Waived,
			"ELWaived[]":ELWaived,
			"Remark[]":Remark,
			"PageLoadTime":$('input#PageLoadTime').val()  
		}

	$.post('ajax_update_attendance_record_individual_list.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							Get_Return_Message(data);
							Get_Attendance_Record_Individual_List();
							//Get_Attendance_Record_All();
							window.top.tb_remove();
							Scroll_To_Top();
						}
					});
}

function Get_InOut_Record_List()
{
	var PostVar = {
			"TargetDate":$('input#TargetDate').val(),
			"GroupID":$('#SelectGroup').val(),
			"SortField": $('input#SortField').val(),
			"SortOrder": $('input#SortOrder').val(),
			"Keyword": encodeURIComponent($('Input#Keyword').val()),
			"ShowAbsentSuspectedRecord": $('#ShowAbsentSuspectedRecord').length>0?($('#ShowAbsentSuspectedRecord').is(':checked')?1:0):0
		}
	
	if(Check_Valid_Date('TargetDate','DateWarningLayer'))
	{
		Block_Element("InOutRecordListLayer");
		$.post('ajax_get_inout_record_list.php',PostVar,
						function(data){
							if (data == "die") 
								window.top.location = '/';
							else {
								$('div#InOutRecordListLayer').html(data);
								UnBlock_Element("InOutRecordListLayer");
								Scroll_To_Top();
							}
						});
	}
}

function MouseOverEvent(thisElement)
{
	if($('input#SortOrder').val()=='ASC')
		$(thisElement).children('img').attr('src','<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_sort_a_on.gif"?>');
	else
		$(thisElement).children('img').attr('src','<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_sort_d_on.gif"?>');
}

function MouseOutEvent(thisElement)
{
	if($('input#SortOrder').val()=='ASC')
		$(thisElement).children('img').attr('src','<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_sort_a_off.gif"?>');
	else
		$(thisElement).children('img').attr('src','<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_sort_d_off.gif"?>');
}

function Sort(fieldname)
{
	$('input#SortField').val(fieldname);
	$order = $('input#SortOrder').val();
	if($order=='DESC')
		$('input#SortOrder').val('ASC');
	else
		$('input#SortOrder').val('DESC');
	Get_InOut_Record_List();
}

function Get_Group_Slots(mode)
{
	var Obj = document.getElementById('SelectGroup');
	var GroupID = '';
	if(Obj.selectedIndex >= 0)
		GroupID = Obj.options[Obj.selectedIndex].value;
	
	var PostVar = {
			"GroupID":GroupID,
			"TargetDate":$('input#TargetDate').val(),
			"Mode":mode
		}
	var target_date_obj = document.getElementById('TargetDate');
	if(target_date_obj != null && check_date_without_return_msg(target_date_obj))
	{
		$.post('ajax_get_group_slots.php',PostVar,
						function(data){
							if (data == "die") 
								window.top.location = '/';
							else {
								$('span#SlotSelectionLayer').html(data);
								if(mode==1) Get_Edit_Attendance_Record_Group_List('', '');
								else Get_View_Attendance_Record_Group_List('', '');
							}
						});
	}
}

function Get_Available_Entry_Log_Time(jArrData, jHour, jMin, jSec, jMenu, jPos, jViewElem, jEditElem)
{
	var listStr = "";
	var fieldValue, fieldHour, fieldMinute, fieldSecond;
	var count = 0;
	
	fieldHour = document.getElementById(jHour).value;
	fieldMinute = document.getElementById(jMin).value;
	fieldSecond = document.getElementById(jSec).value;
	fieldValue = fieldHour+':'+fieldMinute+':'+fieldSecond;
	var container = $('#'+jMenu);
	var ref = $('#'+jHour).closest('td');
	var pos = ref.position();
	var btn_hide = (typeof(js_btn_hide)!="undefined") ? js_btn_hide : "";
	var box_height = (jArrData.length>10) ? "200" : "105";
	
	listStr  = "<table width='160' height='100' border='0' cellpadding='0' cellspacing='0'>";
	listStr += "<tr>";
	listStr += "<td style='border:0px; padding:0px;' height='19'><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	listStr += "<tr>";
	listStr += "<td style='border:0px; padding:0px;' width='5' height='19'><img src='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_01.gif"?>' width='5' height='19'></td>";
	listStr += "<td style='border:0px; padding:0px;' height='19' valign='middle' background='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_02.gif"?>'>&nbsp;</td>";
	listStr += "<td style='border:0px; padding:0px;' width='19' height='19'><a href='javascript:void(0)' onClick=\"$('#" + jMenu + "').hide();\"><img src='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_close_off.gif"?>' name='pre_close' width='19' height='19' border='0' id='pre_close' onMouseOver=\"MM_swapImage('pre_close','','<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_close_on.gif"?>',1)\" onMouseOut='MM_swapImgRestore()' /></a></td>";
	listStr += "</tr>";
	listStr += "</table></td>";
	listStr += "</tr>";
	listStr += "<tr>";
	listStr += "<td style='border:0px; padding:0px;' ><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	listStr += "<tr style='border:0px; padding:0px;' >";
	listStr += "<td style='border:0px; padding:0px;' width='5' height='50' background='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_04.gif"?>'><img src='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_04.gif"?>' width='5' height='19'></td>";
	
	listStr += "<td style='border:0px; padding:0px;' bgcolor='#FFFFF7'><div style=\"overflow-x: hidden; overflow-y: scroll; height: " + box_height + "px; width: 160px; \">";
	
	listStr += "<table width='180' border='0' cellspacing='0' cellpadding='2' class='tbclip'>";
	if (typeof(jArrData)=="undefined" || jArrData.length==0)
	{
		if (typeof(js_preset_no_record)!="undefined")
		{
			listStr += "<tr><td style='border:0px; padding:0px;' class='guide' align='center'>"+js_preset_no_record+"</td></tr>";
		}
	} else
	{
		for (var i = 0; i < jArrData.length; i++)
		{
			tmpShowValue = jArrData[i];
			tmpArr = tmpShowValue.split(':');
			
			listStr += "<tr><td style=\"border-bottom:1px #EEEEEE solid; background-color: ";
			if (fieldValue == jArrData[i])
			{
				listStr += "#FFF2C3";
			} else
			{
				listStr += "#FFFFFF";
			}
			listStr += "\">";

			if (fieldValue == jArrData[i])
			{
				listStr += "<b>";
			}
			listStr += "<a class='presetlist' style=\"color:#4288E8\" href=\"javascript:void(0);\"  onclick=\"document.getElementById('" + jHour + "').value = '" + tmpArr[0] + "';document.getElementById('" + jMin + "').value = '" + tmpArr[1] + "';document.getElementById('" + jSec + "').value = '" + tmpArr[2] + "';$('#" + jMenu + "').hide();\" title=\"" + jArrData[i] + "\">" + tmpShowValue + "</a>";
			
			if (fieldValue == jArrData[i])
			{
				listStr += "</b>";
			}

			listStr += "</td></tr>";
			count ++;
		}
	}
	listStr += "</table></div></td>";
	listStr += "<td style='border:0px; padding:0px;' width='6' background='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_06.gif"?>'><img src='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_06.gif"?>' width='6' height='6' border='0' /></td>";
	listStr += "</tr>";
	listStr += "<tr>";
	listStr += "<td style='border:0px; padding:0px;' width='5' height='6'><img src='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_07.gif"?>' width='5' height='6'></td>";
	listStr += "<td style='border:0px; padding:0px;' height='6' background='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_08.gif"?>'><img src='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_08.gif"?>' width='100%' height='6' border='0' /></td>";
	listStr += "<td style='border:0px; padding:0px;' width='6' height='6'><img src='<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/can_board_09.gif"?>' width='6' height='6'></td>";
	listStr += "</tr>";
	listStr += "</table></td>";
	listStr += "</tr>";
	listStr += "</table>";
	
	container.html(listStr);
	container.css({position:'absolute',
					left:pos.left+'px',
					top:pos.top+ref.height()+'px',
					display:''});
}

}

/*
{
// 2nd Design Added Functions
function ToggleIndividualSelection()
{
	var $SelLayer = $('#IndividualUserLayer');
	var $SelGroupVal = $('#SelectGroup').val();
	if(typeof($SelGroupVal) == 'undefined' || $SelGroupVal == '')
	{
		Get_Individual_Selection();
		Get_Individual_Slots();
		$SelLayer.css('display', '');
	}else
	{
		Get_Group_Slots();
		$SelLayer.css('display','none');
	}
}

function Get_Individual_Selection()
{
	$.post(
		'ajax_get_individual_selection.php',
		{
			'StaffID':$('#SelectStaff').val(),
			'TargetDate':$('input#TargetDate').val()
		},
		function(data){
			if (data == "die") 
				window.top.location = '/';
			else {
				$('span#IndividualUserLayer').html(data);
			}
		}
	);
}

function Get_Individual_Slots()
{
	var Obj = document.getElementById('SelectStaff');
	var StaffID = '';
	if(Obj.selectedIndex >= 0)
		StaffID = Obj.options[Obj.selectedIndex].value;
	
	var PostVar = {
			"StaffID":StaffID,
			"TargetDate":$('input#TargetDate').val()
		}
	var target_date_obj = document.getElementById('TargetDate');
	if(target_date_obj != null && check_date_without_return_msg(target_date_obj))
	{
		//Block_Element("SlotSelectionLayer");
		$.post('ajax_get_individual_slots.php',PostVar,
						function(data){
							if (data == "die") 
								window.top.location = '/';
							else {
								$('span#SlotSelectionLayer').html(data);
								//Thick_Box_Init();
								//UnBlock_Element("SlotSelectionLayer");
							}
						});
	}
}

function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		Get_View_Attendance_Record();
	}
	else
		return false;
}

function Check_Go_Search_Edit_Group_Members(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		Get_Edit_Attendance_Record_Group_List();
	}
	else
		return false;
}

function Check_Go_Search_Edit_Individual(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		Get_Edit_Attendance_Record_Individual_List();
	}
	else
		return false;
}

function Get_View_Attendance_Record_Group_List()
{
	var GroupID = $('#SelectGroup').val();
	var SlotName = $('#SelectSlot').val();
	var PostVar = {
			"GroupID": GroupID,
			"SlotName": encodeURIComponent(SlotName),
			"TargetDate":$('input#TargetDate').val(),
			"StatusType":$('#SelectStatus').val(),
			"Keyword": encodeURIComponent($('Input#Keyword').val())
		}
	
	if(Check_Valid_Date('TargetDate','DateWarningLayer'))
	{
		Block_Element("AttendanceRecordGroupListLayer");
		$.post('ajax_get_view_attendance_record_group_list.php',PostVar,
						function(data){
							if (data == "die") 
								window.top.location = '/';
							else {
								$('div#AttendanceRecordGroupListLayer').html(data);
								Thick_Box_Init();
								UnBlock_Element("AttendanceRecordGroupListLayer");
							}
						});
	}
}

function Get_Edit_Attendance_Record_Group_List()
{
	var GroupID = $('#SelectGroup').val();
	var SlotName =  $('#SelectSlot').val();
	var PostVar = {
			"GroupID": GroupID,
			"SlotName": encodeURIComponent(SlotName),
			"TargetDate":$('input#TargetDate').val(),
			"StatusType":$('#SelectStatus').val(),
			"Keyword": encodeURIComponent($('Input#Keyword').val())
		}
	
	if(Check_Valid_Date('TargetDate','DateWarningLayer'))
	{
		Block_Element("AttendanceRecordLayer");
		$.post('ajax_get_edit_attendance_record_group_list.php',PostVar,
						function(data){
							if (data == "die") 
								window.top.location = '/';
							else {
								$('div#AttendanceRecordLayer').html(data);
								Thick_Box_Init();
								UnBlock_Element("AttendanceRecordLayer");
							}
						});
	}
}

function Get_Attendance_Record_Individual_List()
{
	var PostVar = {
			"TargetDate":$('input#TargetDate').val(),
			"StaffID": $('#SelectStaff').val(),
			"SlotName": encodeURIComponent($('#SelectSlot').val()),
			"DutyType":'',
			"StatusType":$('#SelectStatus').val(),
			"Keyword": encodeURIComponent($('Input#Keyword').val())
	}
	if(Check_Valid_Date('TargetDate','DateWarningLayer'))
	{
		Block_Element("AttendanceRecordGroupListLayer");
		$.post('ajax_get_attendance_record_individual_list.php',PostVar,
						function(data){
							if (data == "die") 
								window.top.location = '/';
							else {
								$('div#AttendanceRecordGroupListLayer').html(data);
								Thick_Box_Init();
								UnBlock_Element("AttendanceRecordGroupListLayer");
							}
						});
	}
}

function Get_Edit_Attendance_Record_Individual_List()
{
	var RecordID = Get_Element_Values('EditRecords[]', false);//StaffID
	var SlotNames = Get_Element_Values('SlotNames[]', true);
	
	var PostVar = {
		"RecordID[]":RecordID,
		"SlotNames[]":SlotNames,
		"StaffID":$('#SelectStaff').val(),
		"SlotName":encodeURIComponent($('#SelectSlot').val()),
		"DutyType":'',
		"StatusType":$('#SelectStatus').val(),
		"TargetDate":$('input#TargetDate').val(),
		"Keyword": encodeURIComponent($('Input#Keyword').val())
	}
	
	if(Check_Valid_Date('TargetDate','DateWarningLayer'))
	{
		Block_Element("AttendanceRecordLayer");
		$.post('ajax_get_edit_attendance_record_individual_list.php',PostVar,
			function(data) {
				if (data == "die") 
					window.top.location = '/';
				else {
					$('div#AttendanceRecordLayer').html(data);
					Thick_Box_Init();
					UnBlock_Element("AttendanceRecordLayer");
				}
			});
	}
}

function Get_View_Attendance_Record()
{
	var $SelGroupVal = $('#SelectGroup').val();
	if($SelGroupVal == '')
	{
		Get_Attendance_Record_Individual_List();
	}else
	{
		Get_View_Attendance_Record_Group_List();
	}
}

function Get_Attendance_Record_All()
{
	var GroupID = $('#SelectGroup').val() || '';
	var StaffID = $('#SelectStaff').val() || '';
	
	var PostVar = {
			"TargetDate":$('input#TargetDate').val(),
			"GroupID":GroupID,
			"StaffID": StaffID,
			"SlotName": encodeURIComponent($('#SelectSlot').val()),
			"StatusType":$('#SelectStatus').val(),
			"Keyword": encodeURIComponent($('Input#Keyword').val())
		}
	
	Block_Element("AttendanceRecordLayer");
	$.post('ajax_get_attendance_record_all.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#AttendanceRecordLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("AttendanceRecordLayer");
						}
					});
}
}
*/ // 2nd design

<?php if($sys_custom['StaffAttendance']['WongFutNamCollege']){ ?>
function restrictNonZeroPositiveFloatNumber(obj)
{
	var val = $.trim(obj.value);
	var num = parseFloat(val);
	
	if(isNaN(num) || val == ''){
		num = 1;
	}
	if(num <= 0){
		num = 1;
	}
	obj.value = num;
}

function Get_Check_OT_Form(staffId)
{
	tb_show('<?=$Lang['StaffAttendance']['CheckOTTime']?>',"#TB_inline?height=420&width=750&inlineId=FakeLayer");
	$.post(
		'ajax_get_check_ot_form.php',
		{
			'RecordDate': $('#TargetDate').val(),
			 'StaffID': staffId
		},
		function(returnHtml){
			$("div#TB_ajaxContent").html(returnHtml);
		}
	);
}

function Check_OT_Time()
{
	var loading_img = '<?=$StaffAttend3UI->Get_Ajax_Loading_Image(0,' ')?>';
	//Block_Element('modalForm');
	$('#CheckResultLayer').html(loading_img).show();
	$.post(
		'../OTRecords/ajax_task.php',
		{
			'task':'checkValidity',
			'StaffID':$('#StaffID').val(),
			'RecordDate':$.trim($('#RecordDate').val()),
			'OTmins': parseFloat($('#OTmins').val())*60
		},
		function(returnData){
			var return_ary = [];
			if(JSON && JSON.parse)
			{
				return_ary = JSON.parse(returnData);
			}else{
				eval('return_ary='+returnData);
			}
			var result_code = return_ary[0][0];
			var msg = return_ary[0][1];
			var valid = true;
			if(result_code == 0){
				valid = false; 
				msg = '<?=$Lang['StaffAttendance']['CheckingResult']?>: <span style="color:red">'+msg+'</span>';
			}else{
				msg = '<?=$Lang['StaffAttendance']['CheckingResult']?>: <span style="color:green">'+msg+'</span>';
			}
			$('#CheckResultLayer').html(msg);
			$('#CheckResultLayer').show();
			//UnBlock_Element('modalForm');
		}
	);
}
<?php } ?>
</script>