<?php
// using 
/* Change log
 *  2014-12-10 (Carlos): Added $sys_custom['StaffAttendance']['RealtimeStaffStatusRefreshPeriod'] to control refresh period, default 10 seconds
 *  2014-03-27 (Carlos): Added post parameter [InSchoolInOutStatus] to js Get_Real_Time_Staff_Status()
 * 						 Change [Real Time Staff Status] to [Staff In-School / In-Out Status]
 *  2013-04-03 (Carlos): added tag [Import Past Attendance Records] for $sys_custom['StaffAttendance']['ImportPastAttendance']
 *	By: Kenneth chung
 *	On: 2010-10-05
 *	Details: Added JS function Get_Real_Time_Staff_Status for constant ajax calling
 */

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-Attendance'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['Attendance'] = 1;
/*
$TAGS_OBJ[] = array($Lang['StaffAttendance']['AttendanceRecord'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/",0);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['ViewLateAbsentEarlyList'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/daily_profile_list/",0);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['InSchoolInOutStatus'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/real_time_staff_status/",1);
if ($StaffAttend3->Check_Access_Right('MGMT-Attendance-Manage') || $StaffAttend3->IS_ADMIN_USER()) {
	$TAGS_OBJ[] = array($Lang['StaffAttendance']['ImportOfflineRecords'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/offline/",0);
	if($sys_custom['StaffAttendance']['ImportPastAttendance']) {
		$TAGS_OBJ[] = array($Lang['StaffAttendance']['ImportPastAttendanceRecords'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/Attendance/import/",0);
	}
}
*/
$TAGS_OBJ = $StaffAttend3UI->Get_Management_DailyRecord_Navigation_Tabs('InSchoolInOutStatus');
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

$ExtTime = $_REQUEST['ExtTime']; // parameter for testing purpose
$Keyword = $_REQUEST['Keyword']; // parameter for testing purpose
echo $StaffAttend3UI->Get_Real_Time_Staff_Status_Index($Keyword,$ExtTime);

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script>
function Get_Real_Time_Staff_Status() {
	var PostVar = {
		"Keyword":$('#Keyword').val(),
		"SortBy":Get_Selection_Value("SortBy","String"),
		"InSchoolInOutStatus" : $('input[name=InSchoolInOutStatus]:checked').val() 
	};
	
	$('#UserListLayer').load('ajax_get_real_time_staff_status.php',PostVar,
		function(data) {
			if (data == "die") {
				window.top.location="/";
			}
			else {
				setTimeout('Get_Real_Time_Staff_Status()',<?=isset($sys_custom['StaffAttendance']['RealtimeStaffStatusRefreshPeriod']) && $sys_custom['StaffAttendance']['RealtimeStaffStatusRefreshPeriod']>0?$sys_custom['StaffAttendance']['RealtimeStaffStatusRefreshPeriod']:'10000'?>);
			}
		}
	);
}

$(document).ready(function() {
	Get_Real_Time_Staff_Status();
});
</script>