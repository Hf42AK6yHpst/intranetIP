<?php
// using 
/************************************************** Change Log **********************************************************
 * 2014-03-27 (Carlos) : Added post parameter [InSchoolInOutStatus] 
 *************************************************************************************************************************/
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-Attendance'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$StaffAttend3UI = new libstaffattend3_ui();

$ExtTime = $_REQUEST['ExtTime']; // parameter for testing purpose
$Keyword = urldecode($_REQUEST['Keyword']); // parameter for testing purpose
$SortBy = $_REQUEST['SortBy'];
$InSchoolInOutStatus = $_REQUEST['InSchoolInOutStatus'];

echo $StaffAttend3UI->Get_Real_Time_Staff_Status($Keyword,$ExtTime,$SortBy,$InSchoolInOutStatus);
intranet_closedb();
die;
?>