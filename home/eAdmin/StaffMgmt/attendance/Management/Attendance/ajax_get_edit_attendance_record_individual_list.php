<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-Attendance'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$StaffAttend3UI = new libstaffattend3_ui();

$RecordID = $_REQUEST['RecordID'];
$SlotNames = $_REQUEST['SlotNames'];
$DutyType = $_REQUEST['DutyType'];
$StatusType = $_REQUEST['StatusType'];
$Date = explode('-',$_REQUEST['TargetDate']);
$Year = $Date[0];
$Month = $Date[1];
$Day = $Date[2];
$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));

$StaffID = $_REQUEST['StaffID'];
$SlotName = trim(urldecode(stripslashes($_REQUEST['SlotName'])));

$UserIDList = array();
$SlotNameList = array();
for($i=0;$i<sizeof($RecordID);$i++)
{
	$UserIDList[$i] = $RecordID[$i];
	$SlotNameList[$i] = trim(urldecode(stripslashes($SlotNames[$i])));
}

echo $StaffAttend3UI->Get_Edit_Attendance_Record_Individual_List($UserIDList, $SlotNameList, $DutyType, $StatusType, $Keyword, $Year, $Month, $Day, $StaffID, $SlotName);

intranet_closedb();
?>