<?php
//editing by 
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!($StaffAttend3->Check_Access_Right('DUTYSETUP-BASIC') || $StaffAttend3->Check_Access_Right('DUTYSETUP-SPECIAL')))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$StaffAttend3UI = new libstaffattend3_ui();

$GroupID = $_REQUEST['GroupID'];
$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
$UnAvaliableUserList = (is_array($_REQUEST['UnAvaliableUserList']))? $_REQUEST['UnAvaliableUserList']:array();

$Result = $StaffAttend3UI->Get_Group_Member_List($GroupID,$UnAvaliableUserList,$Keyword);

echo $Result[0];

intranet_closedb();
?>