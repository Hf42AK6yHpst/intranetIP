<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-BASIC'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$StaffAttend3 = new libstaffattend3();

$ID = $_REQUEST['ID'];
$GroupSlotPeriodID = $_REQUEST['GroupSlotPeriodID'];
$EffectiveStart = trim(urldecode(stripslashes($_REQUEST['EffectiveStart'])));
$EffectiveEnd = trim(urldecode(stripslashes($_REQUEST['EffectiveEnd'])));
$GroupOrIndividual = $_REQUEST['GroupOrIndividual'];

if ($StaffAttend3->Check_Duty_Period($ID,$GroupSlotPeriodID,$EffectiveStart,$EffectiveEnd,$GroupOrIndividual)) {
	echo "1"; // ok
}
else {
	echo "0"; // not ok
}

intranet_closedb();
?>