<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-BASIC'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$ID = $_REQUEST['ID'];
$GroupSlotPeriodID = $_REQUEST['GroupSlotPeriodID'];
$GroupOrIndividual = $_REQUEST['GroupOrIndividual'];
$EffectiveStart = trim(urldecode(stripslashes($_REQUEST['EffectiveStart'])));
$EffectiveEnd = trim(urldecode(stripslashes($_REQUEST['EffectiveEnd'])));
$SkipSchoolHoliday = $_REQUEST['SkipSchoolHoliday'];
$SkipPublicHoliday = $_REQUEST['SkipPublicHoliday'];
$SkipSchoolEvent = $_REQUEST['SkipSchoolEvent'];
$SkipAcademicEvent = $_REQUEST['SkipAcademicEvent'];
$DayType = $_REQUEST['DayType'];

if ($StaffAttend3->Save_Duty_Period($ID,$GroupSlotPeriodID,$EffectiveStart,$EffectiveEnd,$SkipSchoolHoliday,$SkipPublicHoliday,$SkipSchoolEvent,$SkipAcademicEvent,$GroupOrIndividual,$DayType)) {
	echo $Lang['StaffAttendance']['SaveNormalGroupDutyPeriodSuccess'].'|=|'.$StaffAttend3->db_insert_id();
}
else {
	echo $Lang['StaffAttendance']['SaveNormalGroupDutyPeriodFail'];
}

intranet_closedb();
?>