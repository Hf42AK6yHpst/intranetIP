<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-SPECIAL'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$SlotID = $_REQUEST['SlotID'];
$GroupOrIndividual = $_REQUEST['GroupOrIndividual'];
$ObjSelected = $_REQUEST['ObjSelected'];
$TemplateID = $_REQUEST['TemplateID'];

$StaffAttend3->Start_Trans();
if ($StaffAttend3->Save_Template_Apply_Target($TemplateID,$SlotID,$GroupOrIndividual,$ObjSelected)) {
	echo $Lang['StaffAttendance']['SaveTemplateSuccess'];
	$StaffAttend3->Commit_Trans();
}
else {
	echo $Lang['StaffAttendance']['SaveTemplateFail'];
	$StaffAttend3->RollBack_Trans();
}

intranet_closedb();
?>