<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-SPECIAL'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$SlotID = $_REQUEST['SlotID'];
$GroupOrIndividual = $_REQUEST['GroupOrIndividual'];
$ObjSelected = $_REQUEST['ObjSelected'];
$TemplateID = $_REQUEST['TemplateID'];

$Result = $StaffAttend3->Check_Involve_Target_Slot_Setting($TemplateID,$SlotID,$ObjSelected,$GroupOrIndividual);

if ($Result !== true) {
	// response problem user list
	for ($i=0; $i< sizeof($Result); $i++) {
		echo $Result[$i][0].'|=|'.$Result[$i][1].'|=|'.$Result[$i][2].'|=|'.$Result[$i][3].'|=|'.$Result[$i][4];
		if ($i<(sizeof($Result)-1))
			echo '|==|';
	}
}
else {
	echo "1"; // ok, no conflict
}

intranet_closedb();
?>