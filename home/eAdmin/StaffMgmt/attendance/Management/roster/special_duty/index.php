<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-SPECIAL'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['DutySpecial'] = 1;
$TAGS_OBJ[] = array($Lang['StaffAttendance']['SpecialRoster'],$PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Management/roster/group/normal/index.php", 0);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['WholeSchoolSpecialDaySetting'],'', 1);
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
//$TAGS_OBJ_RIGHT[] = array(gen_online_help_btn_and_layer('staff_attendance','schoolduty_tab'));
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $StaffAttend3UI->Get_Special_Duty_Index();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script>
var SlotJustCheck = 1;
var SaveSlotGoNextStep = false;
// ajax function
{
function Check_Involve_Target_Slot_Setting() {
	var PostVar = {
		"ObjSelected[]": Get_Selection_Value("ObjSelected[]","Array",true),
		"SlotID": $('Input#SlotID').val(),
		"TemplateID": $('Input#TemplateID').val(),
		"GroupOrIndividual": $('Input#GroupOrIndividual').val()
	};
	
	$.post('ajax_check_involve_target_slot_setting.php',PostVar, 
		function(data) {
			if (data == "die") 
				window.top.location = '/';
			else {
				if (data == "1") {
					Save_Involve_Target();
				}
				else {
					var ProblemUsers = data.split('|==|');
					
					var msg = '';
					var ObjSelected = document.getElementById('ObjSelected[]');
					for (var i=0; i< ProblemUsers.length; i++) {
						var UserDetail = ProblemUsers[i].split('|=|');
						
						msg += UserDetail[1]+' ('+UserDetail[2]+':'+UserDetail[3]+'-'+UserDetail[4]+')\n';
						
						for (var j=0; j< ObjSelected.length; j++) {
							if (ObjSelected.options[j].value == UserDetail[0]) {
								ObjSelected.options[j].selected = true;
								break;
							}
						}
					}
					
					alert('<?=$Lang['StaffAttendance']['TemplateTargetSlotConflictWarning']?>\n'+msg);
				}
			}
		}
	);
}
	
function Save_Involve_Target() {
	if (confirm('<?=$Lang['StaffAttendance']['DateAppliedWarning']?>')) {
		Block_Thickbox();
		var PostVar = {
			"ObjSelected[]": Get_Selection_Value("ObjSelected[]","Array",true),
			"SlotID": $('Input#SlotID').val(),
			"TemplateID": $('Input#TemplateID').val(),
			"GroupOrIndividual": $('Input#GroupOrIndividual').val()
		};
		
		$.post('ajax_save_involve_target.php',PostVar, 
			function(data) {
				if (data == "die") 
					window.top.location = '/';
				else {
					Get_Return_Message(data);
					Get_Special_Day_Duty_List();
					window.top.tb_remove();
				}
			}
		);
	}
}

function Get_Template_Involved_Form(SlotID,GroupOrIndividual,TemplateID) {
	var GroupOrIndividual = GroupOrIndividual || "Group";
	var PostVar = {
		"SlotID": SlotID,
		"GroupOrIndividual":GroupOrIndividual,
		"TemplateID": TemplateID
	};
	
	$.post('ajax_get_party_involve_form.php',PostVar,
		function(data) {
			if (data == "die") 
				window.top.location = '/';
			else {
				if (GroupOrIndividual == "Group") 
					$('div#TB_ajaxWindowTitle').html('<?=$Lang['StaffAttendance']['ManageInvolvedGroup']?>');
				else 
					$('div#TB_ajaxWindowTitle').html('<?=$Lang['StaffAttendance']['ManageInvolvedIndividual']?>');
				$('div#TB_ajaxContent').html(data);
				UnBlock_Thickbox();
			}
		});
}

function Remove_Template_Target_Date(TargetDate,TemplateID) {
	if (confirm('<?=$Lang['StaffAttendance']['DateAppliedDeleteWarning']?>')) {
		var PostVar = {
			"TemplateID": TemplateID,
			"TargetDate": TargetDate
		};
		
		Block_Element('TemplateTargetDateLayer');
		$.post('ajax_delete_template_target_date.php',PostVar,
			function (data) {
				if (data == "die") {
					window.top.location = '/';
				}
				else {
					Get_Return_Message(data);
					Get_Special_Day_Duty_List();
					Refresh_Template_Target_Date_List();
				}
			}
		);
	}
}
	
function Refresh_Template_Target_Date_List() {
	var PostVar = {
			"TemplateID": $('input#TemplateID').val()
		};
	
	$('div#TemplateTargetDateLayer').load('ajax_get_date_apply_detail.php',PostVar,
			function (data) {
				if (data == "die") {
					window.top.location = '/';
				}
				else {
					UnBlock_Element('TemplateTargetDateLayer');
				}
			}
		);
}	

function Save_Date_Applied() {
	if (confirm('<?=$Lang['StaffAttendance']['DateAppliedWarning']?>')) {
		var TargetDate = (Get_Radio_Value("TargetDateMethod") == "Single")? $('input#TargetDate').val():Get_Check_Box_Value("SchCalTargetDate[]","Array");
		var PostVar = {
			"TemplateID": $('input#TemplateID').val(),
			"TargetDate[]": TargetDate
		};
		
		Block_Element('TemplateTargetDateLayer');
		$.post('ajax_save_template_target_date.php',PostVar,
			function (data) {
				if (data == "die") {
					window.top.location = '/';
				}
				else {
					Get_Return_Message(data);
					Get_Special_Day_Duty_List();
					Get_Event_List_Table();
					Refresh_Template_Target_Date_List();
				}
			}
		);
	}
}
	
function Check_Date_Apply(SubmitAfter) {
	var SubmitAfter = SubmitAfter || false;
	var TargetDate = $('input#TargetDate').val();
	var TargetYear = TargetDate.substr(0,4);
	var TargetMonth = TargetDate.substr(5,2);
	var TargetDay = TargetDate.substr(8,2);
	var JsTargetDate = new Date(TargetYear,(TargetMonth-1),TargetDay,0,0,1,1);
	var Today = new Date();
	
	if ($('div#DateApplyWarningLayer').html() == "") {
		if (JsTargetDate < Today) {
			$('tr#DateApplyWarningRow').css('display','');
			$('div#DateApplyWarningLayer').html('<?=$Lang['StaffAttendance']['TargetDateMustBeAfterTodayWarning']?>');
		}
		else {
			$('tr#DateApplyWarningRow').css('display','none');
			$('div#DateApplyWarningLayer').html('');
			
			var PostVar = {
				"TemplateID": $('input#TemplateID').val(),
				"TargetDate": TargetDate
			};
			
			$.post('ajax_check_date_applied.php',PostVar,
				function(data) {
					if (data == "die") {
						window.top.location = '/';
					}
					else if (data == "1") {
						$('tr#DateApplyWarningRow').css('display','none');
						$('div#DateApplyWarningLayer').html('');
						
						if (SubmitAfter) {
							Save_Date_Applied();
						}
					}
					else {
						$('tr#DateApplyWarningRow').css('display','');
						$('div#DateApplyWarningLayer').html('<?=$Lang['StaffAttendance']['TargetDateUsedWarning']?>');
					}
				}
			);
		}
	}
}

function Get_Date_Apply_Form(TemplateID,PrevStepReturnMessage) {
	var PrevStepReturnMessage = PrevStepReturnMessage || "";
	var PostVar = {
		"TemplateID":TemplateID
	};
	$.post('ajax_get_date_apply_form.php',PostVar,
		function(data) {
			if (data == "die") {
				window.top.location = '/';
			}
			else {
				$('div#TB_ajaxContent').html(data);
				Get_Return_Message(PrevStepReturnMessage);
				UnBlock_Thickbox();
			}
		}
	);
}

function Delete_Template(TemplateID) {
	if (confirm('<?=$Lang['StaffAttendance']['DeleteTemplateWarning']?>')) {
		var PostVar = {
			"TemplateID": TemplateID
		};
		
		Block_Element('SpecialDutyListLayer');
		$.post('ajax_delete_template.php',PostVar,
			function (data) {
				if (data == "die") {
					window.top.location = '/';
				}
				else {
					Get_Special_Day_Duty_List();
					Get_Return_Message(data);
				}
			}
		);
	}
}	

function Check_Template_Name() {
	var PostVar = {
		"TemplateID": $('input#TemplateID').val(),
		"TemplateName": encodeURIComponent($('input#TemplateName').val())
	};
	
	$.post('ajax_check_template_name.php',PostVar,
		function(data) {
			if (data == "die") {
				window.top.location = '/';
			}
			else if (data == "1") {
				$('tr#TemplateNameWarningRow').css('display','none');
				$('div#TemplateNameWarningLayer').html('');
			}
			else {
				$('tr#TemplateNameWarningRow').css('display','');
				$('div#TemplateNameWarningLayer').html('<?=$Lang['StaffAttendance']['DuplicateTemplateNameWarning']?>');
			}
		}
	);
}

function Save_Template(GoNextStep) {
	var GoNextStep = GoNextStep || "";
	var TemplateID = $('input#TemplateID').val();
	
	if ($('div#TemplateNameWarningLayer').html() == "") {
		Block_Thickbox();
		var PostVar = {
			"TemplateID": TemplateID,
			"TemplateName": encodeURIComponent($('input#TemplateName').val())
		};
		
		$.post('ajax_save_template.php',PostVar,
			function(data) {
				if (data == "die") 
					window.top.location = '/';
				else {
					Get_Special_Day_Duty_List();
					if (GoNextStep) {
						Temp = data.split('|=|');
						
						if (Temp[0] == "1") {
							if (TemplateID == "") 
								TemplateID = Temp[2];
							Get_Date_Apply_Form(TemplateID,data);
						}
						else {
							Get_Return_Message(data);
							window.top.tb_remove();
						}
					}
					else {
						Get_Return_Message(data);
						
						window.top.tb_remove();
					}
				}
			}
		);
	}
}

function Get_Template_Form(TemplateID) {
	TemplateID = TemplateID || "";
	
	var PostVar = {
		"TemplateID": TemplateID
	};
	
	$.post('ajax_get_template_form.php',PostVar,
		function(data) {
			if (data == "die") 
				window.top.location = '/';
			else {
				$('div#TB_ajaxContent').html(data);
				UnBlock_Thickbox();
			}
		});
}

function Get_Special_Day_Duty_List() {
	Block_Element('SpecialDutyListLayer');
	
	var PostVar = {
		"Keyword": encodeURIComponent($('input#Keyword').val())
	};
	
	$('div#SpecialDutyListLayer').load('ajax_get_special_day_duty_index.php',PostVar,
		function (data) {
			Thick_Box_Init();
			UnBlock_Element('SpecialDutyListLayer');
		}
	);
}

function Get_Slot_Form(SlotID,TemplateID) {
	SlotID = SlotID || "";
	TemplateID = TemplateID || "";
	
	var PostVar = {
		"SlotID": SlotID,
		"TemplateID": TemplateID
	};
	
	$.post('<?=$PATH_WRT_ROOT?>home/eAdmin/StaffMgmt/attendance/Settings/time_slot/ajax_get_slot_form.php',PostVar,
		function(data) {
			if (data == "die") 
				window.top.location = '/';
			else {
				$('div#TB_ajaxContent').html(data);
				UnBlock_Thickbox();
			}
		});
}

function Check_Slot_Name() {
	SlotName = $('input#SlotName').val();
	ElementObj = $('div#SlotNameWarningLayer');
		
	if (DOM_Check_Slot_Name(SlotName,'SlotNameWarningLayer','SlotNameWarningRow')) {
		var PostVar = {
			"TemplateID":$('input#TemplateID').val(),
			"SlotID":$('input#SlotID').val(),
			"SlotName":encodeURIComponent(SlotName)
		}
		
		$.post('<?=$PATH_WRT_ROOT?>home/eAdmin/StaffMgmt/attendance/Settings/time_slot/ajax_check_slot_name.php',PostVar,
			function (data) {
				if (data == "die") 
					window.top.location = '/';
				else if (data == "1") {
					ElementObj.html('');
					document.getElementById('SlotNameWarningRow').style.display = 'none';
				}
				else {
					ElementObj.html('<?=$Lang['StaffAttendance']['SlotNameWarning']?>');
					document.getElementById('SlotNameWarningRow').style.display = '';
				}
			});
	}
}

function Save_Slot(GoNextStep,GroupOrIndividual) {
	SlotJustCheck = 0; // global variable setting
	SaveSlotGoNextStep = GoNextStep || false; // global variable setting
	GroupOrIndividual = GroupOrIndividual || "Group";
	Check_Slot_Period();
	Check_Slot_Name();
	Check_Day_Count();
	
	$(document).ajaxComplete(function(e, xhr, settings) {
		if (settings.url.indexOf('ajax_check_slot_name.php') != -1 && SlotJustCheck == 0) {
			var NameWarning = $('div#SlotNameWarningLayer').html();
			var TimeWarning = $('div#SlotTimeWarningLayer').html();
			var DayCountWarning = $('div#DutyCountWarningLayer').html();
		
			if (Trim(NameWarning) == "" && Trim(TimeWarning) == "" && Trim(DayCountWarning) == "") {
				var StartHour = Get_Selection_Value('StartHour','String');
				var StartMin = Get_Selection_Value('StartMin','String');
				var StartSec = Get_Selection_Value('StartSec','String');
				var EndHour = Get_Selection_Value('EndHour','String');
				var EndMin = Get_Selection_Value('EndMin','String');
				var EndSec = Get_Selection_Value('EndSec','String');
				var SlotStart = StartHour+":"+StartMin+":"+StartSec;
				var SlotEnd = EndHour+":"+EndMin+":"+EndSec;
				var GroupID = $('input#GroupID').val();
				var PostVar = {
					"SlotName": encodeURIComponent($('input#SlotName').val()),
					"SlotID": $('input#SlotID').val(),
					"TemplateID": $('input#TemplateID').val(),
					"SlotStart": encodeURIComponent(SlotStart),
					"SlotEnd": encodeURIComponent(SlotEnd),
					"InWavie": document.getElementById('InWavie').checked,
					"OutWavie": document.getElementById('OutWavie').checked,
					"DutyCount": encodeURIComponent($('input#DutyCount').val())
				};
				
				Block_Thickbox();
				$.post('<?=$PATH_WRT_ROOT?>home/eAdmin/StaffMgmt/attendance/Settings/time_slot/ajax_save_slot.php',PostVar,
					function (data) {
						if (data == "die") 
							window.top.location = '/';
						else {
							Get_Return_Message(data);
							Get_Special_Day_Duty_List();
							
							if (SaveSlotGoNextStep) {
								var Temp = data.split('|=|');
								Get_Template_Involved_Form(Temp[2],GroupOrIndividual,$('input#TemplateID').val());
							}
							else
								window.top.tb_remove();
							Scroll_To_Top();
						}
					});
			}
			
			SlotJustCheck = 1;
		}
	});
}

function Delete_Slot(SlotID) {
	if (confirm('<?=$Lang['StaffAttendance']['DeleteSlotConfirmMessage']?>')) {
		var PostVar = {
			"SlotID":SlotID
			};
		
		Block_Element("SlotTableLayer");
		$.post('<?=$PATH_WRT_ROOT?>home/eAdmin/StaffMgmt/attendance/Settings/time_slot/ajax_delete_slot.php',PostVar,
			function(data) {
				if (data == "die")
					window.top.location = '/';
				else {
					Get_Return_Message(data);
					Get_Special_Day_Duty_List();
				}
			});
	}
}

function Get_Event_List_Table() {
	var PostVar = {
		"EventType":Get_Selection_Value("EventType","String"),
		"FromDate":$('#FromDate').val(),
		"ToDate":$('#ToDate').val()
	};
	
	$('#EventList').load("ajax_get_school_calendar_event_table.php",PostVar,
		function(data) {
			if (data == "die")
				window.top.location = '/';
			else {
				$('#EventCountLayer').html('');
			}
		}
	);
}
}

// dom function 
{
function Check_Go_Search(evt) {
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		Get_Special_Day_Duty_List();
	else
		return false;
}

function Check_Slot_Period() {
	var StartHour = Get_Selection_Value('StartHour','String');
	var StartMin = Get_Selection_Value('StartMin','String');
	var StartSec = Get_Selection_Value('StartSec','String');
	var EndHour = Get_Selection_Value('EndHour','String');
	var EndMin = Get_Selection_Value('EndMin','String');
	var EndSec = Get_Selection_Value('EndSec','String');
	var SlotStart = StartHour+":"+StartMin+":"+StartSec;
	var SlotEnd = EndHour+":"+EndMin+":"+EndSec;
	var ElementRow = document.getElementById('SlotTimeWarningRow');
	var ElementLayer = $('div#SlotTimeWarningLayer');
	
	if (SlotStart == SlotEnd) { // time slot must not exceed 24 hours
		ElementLayer.html('<?=$Lang['StaffAttendance']['SlotTimeIntervalLimitWarning']?>');
		ElementRow.style.display = '';
		return false;
	}
	
	ElementLayer.html('');
	ElementRow.style.display = 'none';
}

function Switch_Select_Method(SelectMethod) {
	if (SelectMethod == "Single") {
		$('#SingleDateTable').show();
		$('#FromSchCalTable').hide();
	}
	else {
		Get_Event_List_Table();
		$('#SingleDateTable').hide();
		$('#FromSchCalTable').show();
	}
}

function Check_Date_Count() {
	var DateSelected = Get_Check_Box_Value("SchCalTargetDate[]","Array");
	
	$('#EventCountLayer').html('<?=$Lang['StaffAttendance']['EventSelected']?> ('+DateSelected.length+')');
}
}

// UI function for add/ remove class teacher/ student in add/edit class
{
function Remove_Selected() {
	var AvalObj = document.getElementById('AvalObj');
	var ObjSelected = document.getElementById('ObjSelected[]');
	
	for (var i = (ObjSelected.length -1); i >= 0 ; i--) {
		if (ObjSelected.options[i].selected) {
			Obj = ObjSelected.options[i];
			AvalObj.options[AvalObj.length] = new Option(Obj.text,Obj.value);
			ObjSelected.options[i] = null;
		}
	}
	
	Reorder_Selection_List('AvalObj');
}

function Remove_All() {
	var AvalObj = document.getElementById('AvalObj');
	var ObjSelected = document.getElementById('ObjSelected[]');
	
	for (var i = (ObjSelected.length -1); i >= 0 ; i--) {
		Obj = ObjSelected.options[i];
		AvalObj.options[AvalObj.length] = new Option(Obj.text,Obj.value);
		ObjSelected.options[i] = null;
	}
	
	Reorder_Selection_List('AvalObj');
}

function Add_Selected() {
	var AvalObj = document.getElementById('AvalObj');
	var ObjSelected = document.getElementById('ObjSelected[]');
	
	for (var i = (AvalObj.length -1); i >= 0 ; i--) {
		if (AvalObj.options[i].selected) {
			Obj = AvalObj.options[i];
			ObjSelected.options[ObjSelected.length] = new Option(Obj.text,Obj.value);
			AvalObj.options[i] = null;	
		}
	}
	
	Reorder_Selection_List('ObjSelected[]');
}

function Add_All() {
	var AvalObj = document.getElementById('AvalObj');
	var ObjSelected = document.getElementById('ObjSelected[]');
	
	for (var i = (AvalObj.length -1); i >= 0 ; i--) {
		Obj = AvalObj.options[i];
		ObjSelected.options[ObjSelected.length] = new Option(Obj.text,Obj.value);
		AvalObj.options[i] = null;
	}
	
	Reorder_Selection_List('ObjSelected[]');
}

function Reorder_Selection_List(selectId) {
	var selectList = document.getElementById(selectId);
	for (var i = 0; i < selectList.length; i++) {
		for (var j=i; j < selectList.length; j++) {
			if (selectList.options[i].text.toLowerCase() > selectList.options[j].text.toLowerCase()) {
				var tempOption1 = new Option(selectList.options[i].text,selectList.options[i].value);
				var tempOption2 = new Option(selectList.options[j].text,selectList.options[j].value);
				selectList.options[i] = tempOption2;
				selectList.options[j] = tempOption1;
			}
		}
		//alert(selectList.options[i].text);
		/*if (opt.selected) {
			selectList.removeChild(opt);
			selectList.insertBefore(opt, selectOptions[i - 1]);
		}*/
  }
}
}

// thick box function 
{
//on page load call tb_init
function Thick_Box_Init(){   
	tb_init('a.thickbox');//pass where to apply thickbox
}
}
</script>