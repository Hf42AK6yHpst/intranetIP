<?php
// Editing by 
/*
 * 2018-02-07 Carlos: Created.
 */
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-LEAVERECORD'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	//echo 'die';
	intranet_closedb();
	exit();
}

$StaffAttend3UI = new libstaffattend3_ui();

$RecordDate = $_POST['RecordDate'];

$staffInfoAry = $StaffAttend3->Get_Staff_Info($_POST['StaffID']);

$staffIdToRecords = $StaffAttend3->Get_FullDay_Setting_Records(array('StaffID'=>$_POST['StaffID'],'RecordDate'=>$RecordDate,'StaffIDToRecords'=>true));

$OutgoingReasonList = $StaffAttend3->Get_Preset_Leave_Reason_List(CARD_STATUS_OUTGOING);
$HolidayReasonList = $StaffAttend3->Get_Preset_Leave_Reason_List(CARD_STATUS_HOLIDAY);

$x = '';

for ($i=0; $i< sizeof($OutgoingReasonList); $i++) {
    $x .= '<input type="hidden" name="OutgoingReasonList[]" id="'.$OutgoingReasonList[$i]['ReasonID'].'" value="'.intranet_htmlspecialchars($OutgoingReasonList[$i]['ReasonText'] ).'">';
}

for ($i=0; $i< sizeof($HolidayReasonList); $i++) {
    $x .= '<input type="hidden" name="HolidayReasonList[]" id="'.$HolidayReasonList[$i]['ReasonID'].'" value="'.intranet_htmlspecialchars($HolidayReasonList[$i]['ReasonText'] ).'">';
}

foreach($staffInfoAry as $staffInfo){
	
	$staffId = $staffInfo['UserID'];
	if(isset($staffIdToRecords[$staffId])){
	    $records = $staffIdToRecords[$staffId];
	}else{
	    $records = array();
	}
	$record_size = count($records);
	
	$IsFullDaySetting = $record_size==0 || ($record_size>0 && $records[0]['SlotID'] == '');
	$IsTimeSlotSetting = $record_size > 0 && $records[0]['SlotID'] != '';
	if($IsFullDaySetting && $record_size > 0){
		$RecordType = $records[0]['RecordType'];
		$ReasonID = $records[0]['ReasonID'];
		$Remark = $records[0]['Remark'];
		$RecordDate = $records[0]['RecordDate'];
	}else{
		$RecordType = CARD_STATUS_HOLIDAY;
		$ReasonID = '';
		$Remark = '';
	}
	
	$slotIdToRemark = array();
	if($IsTimeSlotSetting){
		for($i=0;$i<count($records);$i++){
			$slotIdToRemark[$records[$i]['SlotID']] = $records[$i]['Remark'];
		}
	}
	
	$groupName = !empty($staffInfo['GroupName'])? '('.$staffInfo['GroupName'].')' : '';
	$x .= '<div style="border:2px dashed #AAA;padding:1.2em 0.5em;">
            <form name="'.$staffId.'-EditForm" id="'.$staffId.'-EditForm" action="" method="post">
			<input type="hidden" name="StaffID[]" value="'.$staffId.'" />
			<table class="form_table">
				<tr>
					<td>'.$Lang['StaffAttendance']['Staff'].'</td>
					<td>:</td>
					<td>'.$staffInfo['StaffName'].$groupName.'</td>
				</tr>
				<tr>
					<td>'.$Lang['StaffAttendance']['FullTimeSlotOption'].'</td>
					<td>:</td>
					<td>
						<input type="radio" name="'.$staffId.'-LeaveNature" id="'.$staffId.'-LeaveNatureFull" value="Full" onclick="Switch_To_Full_Day('.$staffId.');" '.(($IsFullDaySetting)? 'checked="checked"':'').'>
						<label for="'.$staffId.'-LeaveNatureFull">'.$Lang['StaffAttendance']['FullDay'].'</label>
						<input type="radio" name="'.$staffId.'-LeaveNature" id="'.$staffId.'-LeaveNatureTimeSlot" value="TimeSlot" onclick="Switch_To_Time_Slot('.$staffId.');" '.((!$IsFullDaySetting)? 'checked="checked"':'').'>
						<label for="'.$staffId.'-LeaveNatureTimeSlot">'.$Lang['StaffAttendance']['TimeSlot'].'</label>
					</td> 
				</tr>
				<col class="field_title" />
				<col class="field_c" />
			</table>';

	$x .= '<div id="'.$staffId.'-FullDaySettingLayer" '.(($IsFullDaySetting)? '':'style="display:none;"').'>
			<table class="form_table">
				<tr>
					<td>'.$Lang['StaffAttendance']['SettingType'].'</td>
					<td>:</td>
					<td>
						<select name="'.$staffId.'-RecordType" onchange="Get_Reason_List('.$staffId.',this.value);">';
$x .= '						<option value="'.CARD_STATUS_HOLIDAY.'" '.(($RecordType == CARD_STATUS_HOLIDAY)? 'selected':'').'>'.$Lang['StaffAttendance']['Leave'].'</option>
							<option value="'.CARD_STATUS_OUTGOING.'" '.(($RecordType == CARD_STATUS_OUTGOING)? 'selected':'').'>'.$Lang['StaffAttendance']['Outgoing'].'</option>
						</select>
					</td> 
				</tr>
				<tr>
					<td>'.$Lang['StaffAttendance']['FullDayReason'].'</td>
					<td>:</td>
					<td>
					<div class="ReasonSelection">
						'.$StaffAttend3UI->Get_Reason_Selection($RecordType,$ReasonID,$staffId.'-ReasonID',$staffId.'-ReasonID').'
					</div>
					</td> 
				</tr>
				<tr>
					<td>'.$Lang['StaffAttendance']['Remark'].'</td>
					<td>:</td>
					<td>'.$StaffAttend3UI->GET_TEXTAREA($staffId."-Remark", $Remark, $taCols=70, $taRows=5, $OnFocus = "", $readonly = "", $other='', $class='', $taID=$staffId.'-Remark', $CommentMaxLength='255').'</td>
				</tr>
				<tr>
					<td>'.$Lang['StaffAttendance']['StartDate'].'</td>
					<td>:</td>
					<td>
						'.$StaffAttend3UI->GET_DATE_PICKER($staffId.'-TargetStartDate',$RecordDate).'
					</td> 
				</tr>
				<col class="field_title" />
				<col class="field_c" />
				<tr>
					<td>'.$Lang['StaffAttendance']['EndDate'].'</td>
					<td>:</td>
					<td>
						'.$StaffAttend3UI->GET_DATE_PICKER($staffId.'-TargetEndDate',$RecordDate).'
					</td> 
				</tr>
			</table>
		  </div>';
	
    $Duty = $StaffAttend3->Get_Duty_By_Date($staffId,$RecordDate,false,true);
    
    $Holiday = '';
    $Outgoing = '';
    $OnDuty = '';
    $OutgoingUserReasonHidden = '';
    $HolidayUserReasonHidden = '';
    $SlotNameHidden = '';
    
    for ($i=0; $i< sizeof($Duty['SlotDetail']); $i++) {
        $SlotID = $Duty['SlotDetail'][$i]['SlotID'];
        $SlotName = $Duty['SlotDetail'][$i]['SlotName'] .'('.$Duty['SlotDetail'][$i]['SlotStart'].'-'.$Duty['SlotDetail'][$i]['SlotEnd'].')';
        
        $OutgoingUserReasonHidden .= '<input type="hidden" name="'.$staffId.'-'.$SlotID.'-Outgoing-User-Reason" id="'.$staffId.'-'.$SlotID.'-Outgoing-User-Reason" value="'.$Duty['OutgoingSlotReason'][$SlotID][$staffId].'">';
        $HolidayUserReasonHidden .= '<input type="hidden" name="'.$staffId.'-'.$SlotID.'-Holiday-User-Reason" id="'.$staffId.'-'.$SlotID.'-Holiday-User-Reason" value="'.$Duty['HolidaySlotReason'][$SlotID][$staffId].'">';
        $SlotNameHidden .= '<input type="hidden" name="'.$staffId.'-SlotName['.$SlotID.']" id="'.$staffId.'-SlotName['.$SlotID.']" value="'.$SlotName.'">';
        
        
        $Duty['UserToSlotHoliday'][$staffId] = is_array($Duty['UserToSlotHoliday'][$staffId])? $Duty['UserToSlotHoliday'][$staffId]: array();
        $Duty['UserToSlotOutgoing'][$staffId] = is_array($Duty['UserToSlotOutgoing'][$staffId])? $Duty['UserToSlotOutgoing'][$staffId]: array();
        if (in_array($SlotID,$Duty['UserToSlotHoliday'][$staffId])) { // holiday
            $Holiday .= '
										<table style="background: transparent; padding:0px; border:0px; width:100%; padding:0px; spacing:0px; cursor: pointer;">
										<tr>
											<td style="background: transparent; padding:0px; border:0px; padding:0px; spacing:0px">
											<div onclick="Change_Background_Color(\''.$staffId.'-'.$SlotID.'-Holiday'.'\',this);">
							          <input style="display:none;" name="'.$staffId.'-Holiday[]" id="'.$staffId.'-'.$SlotID.'-Holiday" type="checkbox" value="'.$SlotID.'"/>
							          <font class="staff_name" style="color:#000000;">
							          '.$SlotName.'
							          </font>
							      	</div>
							      	</td>
							      	<td style="background: transparent; padding:0px; border:0px; padding:0px; spacing:0px; width:20px;">
							      		<span class="table_row_tool" style="float:right;">
							          	<a href="#" onClick="Show_Reason_Layer(this); return false;" class="reason'.(($Duty['HolidaySlotReason'][$SlotID][$staffId] == "")? '_alert':'').'" title="'.$Lang['StaffAttendance']['FullDayReason'].'" style="display:inline; float:right;"></a>
							          	<div id="'.$SlotID.'-HolidayReasonLayer" class="selectbox_layer ReasonSelector">
                            <select name="'.$staffId.'-'.$SlotID.'-Holiday-Reason" id="'.$staffId.'-'.$SlotID.'-Holiday-Reason" onchange="Check_Reason_Icon(this);">
                               <option value="">'.$Lang['StaffAttendance']['SelectReason'].'</option>';
            foreach ($HolidayReasonList as $Index => $ReasonList) {
                if ($ReasonList['ReasonID'] == $Duty['HolidaySlotReason'][$SlotID][$staffId])
                    $Selected = "selected";
                    else
                        $Selected = "";
                        
                        $Holiday .= '<option value="'.$ReasonList['ReasonID'].'" '.$Selected.'>'.intranet_htmlspecialchars($ReasonList['ReasonText'] ).'</option>';
            }
            $Holiday .= '			</select>
									<br />'.$Lang['StaffAttendance']['Remark'].'<br /><textarea name="'.$staffId.'-'.$SlotID.'-Holiday-Remark" id="'.$staffId.'-'.$SlotID.'-Holiday-Remark" rows="3" wrap="virtual" style="width:140px;height:90px;">'.intranet_htmlspecialchars(isset($slotIdToRemark[$SlotID])?$slotIdToRemark[$SlotID]:'').'</textarea>
									<br /><input class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="'.$Lang['Btn']['Done'].'" type="button" onclick="$(this).closest(\'div\').css(\'visibility\',\'hidden\');">
                          </div>
							          </span>
							      	</td>
							      </tr>
							     	</table>';
        }
        else if (in_array($SlotID,$Duty['UserToSlotOutgoing'][$staffId])) { // outgoing
            $Outgoing .= '
										<table style="background: transparent; padding:0px; border:0px; width:100%; padding:0px; spacing:0px; cursor: pointer;">
										<tr>
											<td style="background: transparent; padding:0px; border:0px; padding:0px; spacing:0px">
											<div onclick="Change_Background_Color(\''.$staffId.'-'.$SlotID.'-Outgoing'.'\',this);">
							          <input style="display:none;" name="'.$staffId.'-Outgoing[]" id="'.$staffId.'-'.$SlotID.'-Outgoing" type="checkbox" value="'.$SlotID.'"/>
							          <font class="staff_name" style="color:#000000;">
							          '.$SlotName.'
							          </font>
							      	</div>
							      	</td>
							      	<td style="background: transparent; padding:0px; border:0px; padding:0px; spacing:0px; width:20px;">
							      		<span class="table_row_tool" style="float:right;">
							          	<a href="#" onClick="Show_Reason_Layer(this); return false;" class="reason'.(($Duty['OutgoingSlotReason'][$SlotID][$staffId] == "")? '_alert':'').'" title="'.$Lang['StaffAttendance']['FullDayReason'].'" style="display:inline; float:right;"></a>
							          	<div id="'.$staffId.'-'.$SlotID.'-OutgoingReasonLayer" class="selectbox_layer ReasonSelector">
                            <select name="'.$staffId.'-'.$SlotID.'-Outgoing-Reason" id="'.$staffId.'-'.$SlotID.'-Outgoing-Reason" onchange="Check_Reason_Icon(this);">
                               <option value="">'.$Lang['StaffAttendance']['SelectReason'].'</option>';
            foreach ($OutgoingReasonList as $Index => $ReasonList) {
                if ($ReasonList['ReasonID'] == $Duty['OutgoingSlotReason'][$SlotID][$staffId])
                    $Selected = "selected";
                    else
                        $Selected = "";
                        
                        $Outgoing .= '<option value="'.$ReasonList['ReasonID'].'" '.$Selected.'>'.intranet_htmlspecialchars($ReasonList['ReasonText'] ).'</option>';
            }
            $Outgoing .= '			</select>
									<br />'.$Lang['StaffAttendance']['Remark'].'<br /><textarea name="'.$staffId.'-'.$SlotID.'-Outgoing-Remark" id="'.$staffId.'-'.$SlotID.'-Outgoing-Remark" rows="3" wrap="virtual" style="width:140px;height:90px;">'.intranet_htmlspecialchars(isset($slotIdToRemark[$SlotID])?$slotIdToRemark[$SlotID]:'').'</textarea>
									<br /><input class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="'.$Lang['Btn']['Done'].'" type="button" onclick="$(this).closest(\'div\').css(\'visibility\',\'hidden\');">
                          </div>
							          </span>
							      	</td>
							      </tr>
							     	</table>';
        }
        else {
            $OnDuty .= '<div onclick="Change_Background_Color(\''.$staffId.'-'.$SlotID.'-OnDuty'.'\',this);" style="cursor: pointer;">
						          <input style="display:none;" name="'.$staffId.'-OnDuty[]" id="'.$staffId.'-'.$SlotID.'-OnDuty" type="checkbox" value="'.$SlotID.'"/>
						          <font class="staff_name" style="color:#000000;">
						          '.$SlotName.'
						          </font>
						      	</div>';
        }
    }
    

    $x.='<div id="'.$staffId.'-TimeSlotLayer" '.(($IsFullDaySetting)? ' style="display:none;"':'').'>
            <input type="hidden" name="'.$staffId.'-TargetDate" id="'.$staffId.'-TargetDate" value="'.$RecordDate.'">
		  <table class="form_table">
		    <tr>
				<td>'.$Lang['StaffAttendance']['TargetDate'].'</td>
				<td>:</td>
				<td>'.$RecordDate.'</td>
			</tr>
			<col class="field_title" />
			<col class="field_c" />
			<tr>
				<td colspan="3">
				    <div>';
                $x .= $OutgoingUserReasonHidden;
                $x .= $HolidayUserReasonHidden;
                $x .= $SlotNameHidden;
                $x .= '<table style="spacing:0px; padding:0px;" width="100%">
						<tr>
							<td bgcolor="#EEEEEE" align="center" width="250px;">
								'.$Lang['StaffAttendance']['OnDuty'].'
							</td>
							<td bgcolor="#E2EDF9" width="250px;">
								'.$Lang['StaffAttendance']['Outgoing'].'
							</td>
							<td bgcolor="#FFEDED" width="250px;">
								'.$Lang['StaffAttendance']['Leave'].'
							</td>
						</tr>
						<tr>
							<td bgcolor="#EEEEEE" align="center" style="width:250px;">
							<div id="'.$staffId.'-OnDuty-Cell" style="min-height:300px; height: expression(this.scrollHeight < 301 ? \'300px\':\'auto\');">
								'.$OnDuty.'
							</div>
							</td>
							<td bgcolor="#E2EDF9" style="width:250px;">
							<div id="'.$staffId.'-Outgoing-Cell" style="min-height:300px; height: expression(this.scrollHeight < 301 ? \'300px\':\'auto\');">
								'.$Outgoing.'
							</div>
							</td>
							<td bgcolor="#FFEDED" style="width:250px;">
							<div id="'.$staffId.'-Holiday-Cell" style="min-height:300px; height: expression(this.scrollHeight < 301 ? \'300px\':\'auto\');">
								'.$Holiday.'
							</div>
							</td>
						</tr>
						</table>';
            $x .= '</div>';
           $x.= '</td>';
      $x.= '</tr>';
    $x.='</table>';
    $x.='</div>';
	
    $x.='</form>';
	$x.='</div>';
	$x.='<br />';
}

$x .= '<div class="edit_bottom_v30">';
$x .= $StaffAttend3UI->GET_ACTION_BTN($Lang['Btn']['Submit'],"button","CheckSubmitForm(document.form1);",'submitBtn').'&nbsp;';
$x .= $StaffAttend3UI->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","GoBack();",'cancelBtn');
$x .= '</div>';

echo $x;

intranet_closedb();
?>