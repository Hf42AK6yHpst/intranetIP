<?php
// Editing by 
/*
 * 2018-02-14 Carlos: Added Remark.
 */
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-LEAVERECORD'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

// full day setting
$StartDate = $_REQUEST['StartDate'];
$EndDate = $_REQUEST['EndDate'];
$RecordType = $_REQUEST['RecordType'];
$StaffID = $_REQUEST['StaffID'];
$ReasonID = $_REQUEST['ReasonID'];
$Remark = $_REQUEST['Remark'];
if($StaffAttend3->isMagicQuotesOn()){
	$Remark = stripslashes($Remark);
}

// Slot setting
$Outgoing = $_REQUEST['Outgoing'];
$Holiday = $_REQUEST['Holiday'];
$OutgoingReason = $_REQUEST['OutgoingReason'];
$HolidayReason = $_REQUEST['HolidayReason'];
$LeaveNature = $_REQUEST['LeaveNature'];
$TargetDate = $_REQUEST['TargetDate'];

$OutgoingRemark = $_REQUEST['OutgoingRemark'];
$HolidayRemark = $_REQUEST['HolidayRemark'];
if(count($OutgoingRemark)>0){
	foreach($OutgoingRemark as $slotId => $slotRemark){
		if($StaffAttend3->isMagicQuotesOn()){
			$slotRemark = stripslashes($slotRemark);
		}
		$OutgoingRemark[$slotId] = trim($slotRemark);
	}
}
if(count($HolidayRemark)>0){
	foreach($HolidayRemark as $slotId => $slotRemark){
		if($StaffAttend3->isMagicQuotesOn()){
			$slotRemark = stripslashes($slotRemark);
		}
		$HolidayRemark[$slotId] = trim($slotRemark);
	}
}

$StaffAttend3->Start_Trans();
if ($StaffAttend3->Save_Full_Day_Setting($StaffID,$RecordType,$StartDate,$EndDate,$ReasonID,$LeaveNature,$Outgoing,$Holiday,$OutgoingReason,$HolidayReason,$TargetDate,$Remark,$OutgoingRemark,$HolidayRemark)) {
	echo $Lang['StaffAttendance']['FullDaySettingSaveSuccess'];
	$StaffAttend3->Commit_Trans();
}
else {
	echo $Lang['StaffAttendance']['FullDaySettingSaveFail'];
	$StaffAttend3->RollBack_Trans();
}

intranet_closedb();
?>