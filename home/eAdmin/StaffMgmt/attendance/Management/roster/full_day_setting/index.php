<?php
// Editing by 
/*
 * 2018-02-14 Carlos: Modified js Save_Full_Day_Setting(), added post data Remark.
 * 					  Display the navigation tabs with function.
 */
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-LEAVERECORD'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['LeaveRequest'] = 1;
//$TAGS_OBJ[] = array($Lang['StaffAttendance']['FullDaySetting'], "", 1);
$TAGS_OBJ = $StaffAttend3UI->Get_Management_LeaveRecord_Navigation_Tabs('LeaveRecordByStaff');
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
//$TAGS_OBJ_RIGHT[] = array(gen_online_help_btn_and_layer('staff_attendance','leave_record'));
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $StaffAttend3UI->Get_Full_Day_Setting_Index();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script>
// ajax function
{
function Get_Reason_List(ReasonType) {
	if (ReasonType == '') {
		document.getElementById('ReasonRow').style.display = 'none';
	}
	else {
		document.getElementById('ReasonRow').style.display = '';
		var PostVar = {
			"ReasonType":ReasonType
		};
		
		$('div#ReasonLayer').load('ajax_get_reason_selection.php',PostVar,
		function (data) {
			if (data == "die") 
				window.top.location="/";
		});
	}
}
	
function Save_Full_Day_Setting() {
	var StartDate = $('Input#StartDate').val();
	var EndDate = $('Input#EndDate').val();
	var StaffID = $('Input#StaffID').val();
	var RecordType = document.getElementById('RecordType').options[document.getElementById('RecordType').selectedIndex].value;
	var ReasonID = document.getElementById('ReasonID').options[document.getElementById('ReasonID').selectedIndex].value;
	var Remark = $.trim($('#Remark').val());
	var LeaveNature = Get_Radio_Value('LeaveNature');
	var OutgoingSlot = Get_Check_Box_Value('Outgoing[]','Array',true);
	var HolidaySlot = Get_Check_Box_Value('Holiday[]','Array',true);
	var TargetDate = $('input#TargetDate').val();
	var OutgoingReason = new Array();
	var HolidayReason = new Array();
	var OutgoingRemark = new Array();
	var HolidayRemark = new Array();
	
	var EvalStr = '';
	EvalStr = 'var PostVar = {';
	// full day setting values
	EvalStr += '"StartDate":StartDate,';
	EvalStr += '"EndDate":EndDate,';
	EvalStr += '"StaffID":StaffID,';
	EvalStr += '"RecordType":RecordType,';
	EvalStr += '"ReasonID":ReasonID,';
	EvalStr += '"Remark":Remark,';
	// Slot setting values
	// Slot setting values
	if (OutgoingSlot.length > 0)
		EvalStr += '"Outgoing[]":OutgoingSlot,';
	if (HolidaySlot.length > 0)
		EvalStr += '"Holiday[]":HolidaySlot,';
	for (var i=0; i< OutgoingSlot.length; i++) {
		OutgoingReason[i] = $("input#"+OutgoingSlot[i]+"-Outgoing-User-Reason").val();
		OutgoingRemark[i] = $("#"+OutgoingSlot[i]+"-Outgoing-Remark").val();
		EvalStr += '"OutgoingReason['+OutgoingSlot[i]+']":OutgoingReason['+i+'],';
		EvalStr += '"OutgoingRemark['+OutgoingSlot[i]+']":OutgoingRemark['+i+'],';
	}
	for (var i=0; i< HolidaySlot.length; i++) {
		HolidayReason[i] = $("input#"+HolidaySlot[i]+"-Holiday-User-Reason").val();
		HolidayRemark[i] = $("#"+HolidaySlot[i]+"-Holiday-Remark").val();
		EvalStr += '"HolidayReason['+HolidaySlot[i]+']":HolidayReason['+i+'],';
		EvalStr += '"HolidayRemark['+HolidaySlot[i]+']":HolidayRemark['+i+'],';
	}
	EvalStr += '"LeaveNature":LeaveNature,';
	EvalStr += '"TargetDate":TargetDate';
	EvalStr += '};';
	eval(EvalStr);
	
	Block_Thickbox();
	$.post('ajax_save_full_day_setting.php',PostVar,
		function(data) {
			if (data == "die") 
				window.top.location="/";
			else {
				Get_Return_Message(data);
				Get_Full_Day_Setting_Calendar(StaffID);
				window.top.tb_remove();
				UnBlock_Thickbox();
			}
		});
}
	
function Get_Add_Full_Day_Setting_Form(StaffID,TargetDate) {
	var PostVar = {
		StaffID: StaffID,
		TargetDate: TargetDate
	};
	
	$.post('ajax_get_full_day_setting_form.php',PostVar,
		function(data) {
			if (data == "die") 
				window.top.location="/";
			else {
				$('div#TB_ajaxContent').html(data);
				Drag_And_Drop_Init();
			}
		}
		);
}
	
function Get_Full_Day_Setting_List() {
	var PostVar = {
			Keyword: encodeURIComponent($('Input#Keyword').val())
			};
	
	$('div#CalendarLayer').hide();
	$('Input#Keyword').show();
	$('div#UserListLayer').show();
	Block_Element('UserListLayer');
	$('div#UserListLayer').load('ajax_get_full_day_setting.php',PostVar,
		function (data) {
			if (data == "die") 
				window.top.location = "/";
			else {
				Scroll_To_Top();
				Thick_Box_Init();
				UnBlock_Element('UserListLayer');
			}
		});
}

function Get_Full_Day_Setting_Calendar(StaffID) {
	var TargetMonth = '';
	var TargetYear = '';
	if (document.getElementById('TargetMonth'))
		TargetMonth = document.getElementById('TargetMonth').options[document.getElementById('TargetMonth').selectedIndex].value;
	if (document.getElementById('TargetYear'))
		TargetYear = document.getElementById('TargetYear').options[document.getElementById('TargetYear').selectedIndex].value;
		
	var PostVar = {
		"StaffID":StaffID,
		"TargetMonth":TargetMonth,
		"TargetYear":TargetYear
	};
	
	$('div#UserListLayer').hide();
	$('Input#Keyword').hide();
	$('div#CalendarLayer').show();
	Block_Element('CalendarLayer');
	$('div#CalendarLayer').load('ajax_get_full_day_calendar.php',PostVar,
		function (data) {
			if (data == "die") 
				window.top.location = "/";
			else {
				Scroll_To_Top();
				Thick_Box_Init();
				UnBlock_Element('CalendarLayer');
			}
		});
}
}

// dom function 
{
function Add_To_Outgoing() {
	var OnDutyList = document.getElementById('SlotList[]');
	var Outgoing = document.getElementById('OutgoingTimeSlot[]');
	
	for (var i = (OnDutyList.length -1); i >= 0 ; i--) {
		User = OnDutyList.options[i];
		var elOptNew = document.createElement('option');
    elOptNew.text = User.text;
    elOptNew.value = User.value;
		//cloneFrom.options[cloneFrom.length] = new Option(User.text,User.value);
		try {
      Outgoing.add(elOptNew, Outgoing.options[Outgoing.length]); // standards compliant; doesn't work in IE
    }
    catch(ex) {
      Outgoing.add(elOptNew, Outgoing.length); // IE only
    }
		OnDutyList.options[i] = null;
	}
}

function Add_To_Holiday() {
	
}
	
function Switch_To_Full_Day() {
	$('div#TimeSlotLayer').hide();
	$('div#FullDaySettingLayer').show();
}

function Switch_To_Time_Slot() {
	$('div#FullDaySettingLayer').hide();
	$('div#TimeSlotLayer').show();
}
	
function Check_Date() {	
	var StartDate = Trim($('Input#StartDate').val());
	DateArray = StartDate.split('-');
	var DateStart = new Date(DateArray[0],(DateArray[1]-1),DateArray[2],0,0,0,1);
	
	var EndDate = Trim($('Input#EndDate').val());
	DateArray = EndDate.split('-');
	var DateEnd = new Date(DateArray[0],(DateArray[1]-1),DateArray[2],0,0,0,1);

	var WarningRow = document.getElementById('DateWarningRow');
	var WarningLayer = $('div#DateWarningLayer');
	
	var TempDate = new Date();
	var CurrentDate = new Date(TempDate.getFullYear(),TempDate.getMonth(),TempDate.getDate(),0,0,0,1);
	
	if (WarningLayer.html() == "" && $('div#StartDateWarningLayer').html() == "") {
		if (StartDate == "" || EndDate == "") {
			WarningLayer.html('<?=$Lang['StaffAttendance']['FullDaySettingNullWarning'] ?>');
			WarningRow.style.display = "";
		}
		else if (!check_date_without_return_msg(document.getElementById('StartDate')) || !check_date_without_return_msg(document.getElementById('EndDate'))) {
			WarningLayer.html('<?=$Lang['StaffAttendance']['InvalidDateRange'] ?>');
			WarningRow.style.display = "";
		}
		else if (CurrentDate.getTime() >= DateStart.getTime()) {
			WarningLayer.html('<?=$Lang['StaffAttendance']['TargetDateMustBeAfterTodayWarning'] ?>');
			WarningRow.style.display = "";
		}
		else if (DateStart > DateEnd) {
			WarningLayer.html('<?=$Lang['StaffAttendance']['FullDaySettingEndDayBeforeStartWarning']?>');
			WarningRow.style.display = "";
		}
		else {
			Save_Full_Day_Setting(); // ok
		}
	}
}
	
function Check_Go_Search(evt) {
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		Get_Full_Day_Setting_List();
	else
		return false;
}

function Change_Background_Color(CheckBoxID,DivObj) {
	var CheckboxObj = document.getElementById(CheckBoxID);

	if (CheckboxObj.checked) {
		//alert('-');
		CheckboxObj.checked = false;
		DivObj.style.backgroundColor = '';
	}
	else {
		//alert('+');
		CheckboxObj.checked = true;
		DivObj.style.backgroundColor = '#A6A6A6';
	}		
}

function Show_Reason_Layer(Obj) {
	if (document.all) {
		Obj.nextSibling.style.visibility = "visible";
		Obj.nextSibling.firstChild.focus();
	}
	else {
		Obj.nextSibling.nextSibling.style.visibility = "visible";
		Obj.nextSibling.nextSibling.firstChild.nextSibling.focus();
	}
}
	
function Check_Reason_Icon(SelectObj) {
	if (SelectObj.options[SelectObj.selectedIndex].value == "") {
		if (document.all)
			SelectObj.parentNode.previousSibling.className = "reason_alert";
		else 
			SelectObj.parentNode.previousSibling.previousSibling.className = "reason_alert";
	}
	else {
		if (document.all)
			SelectObj.parentNode.previousSibling.className = "reason";
		else 
			SelectObj.parentNode.previousSibling.previousSibling.className = "reason";
	}
	
	var ObjID = SelectObj.id.split('-');
	document.getElementById(ObjID[0]+'-'+ObjID[1]+'-User-Reason').value = SelectObj.options[SelectObj.selectedIndex].value;
	SelectObj.parentNode.style.visibility = "hidden";
}
}

// Drag and Drop function
{
function Drag_And_Drop_Init() {
	var StartLeft = -10;
	var StartTop = -10;
	// draggable
	$("div#OnDuty-Cell, div#Outgoing-Cell, div#Holiday-Cell").draggable({
			helper:'clone',
			cursorAt:{left:StartLeft,top:StartTop},
			distance:'10',
			revert:false,
			start: function (ev,ui) {
				$('.ReasonSelector').css('visibility','hidden');
				
				var DragObj = $(this);
				var DragNature = DragObj.attr('id').split('-');
				DragNature = DragNature[0];
				ui.helper.html('');
				var DragSelectedSlots = Get_Check_Box_Value(DragNature+'[]','Array');
				var Content = '';
				for (var i=0; i< DragSelectedSlots.length; i++) {
					Content += document.getElementById("SlotName["+DragSelectedSlots[i]+"]").value+"<br>";
				}
				ui.helper.html(Content);
			},
			stop: function(event, ui) {
				$("div#OnDuty-Cell").css('background-color','#EEEEEE');
				$("div#Outgoing-Cell").css('background-color','#E2EDF9');
				$("div#Holiday-Cell").css('background-color','#FFEDED');
			}
		});
		
	// droppable
	$("div#Outgoing-Cell, div#Holiday-Cell, div#OnDuty-Cell").droppable({
		accept: "div#Outgoing-Cell, div#Holiday-Cell, div#OnDuty-Cell",
		tolerance: "pointer",
		/*activeClass: 'droppable-active',
		hoverClass: 'droppable-hover',*/
		drop: function(ev, ui) {
			var DragObj = ui.draggable;
			var DropObj = $(this);
			var DragNature = DragObj.attr('id').split('-');
			var DragNature = DragNature[0];
			var DropNature = DropObj.attr('id').split('-');
			var DropNature = DropNature[0];
			
			var DragSelectedSlots = Get_Check_Box_Value(DragNature+'[]','Array');
			var DragSlots = Get_Check_Box_Value(DragNature+'[]','Array',true);
			var DropSlots = Get_Check_Box_Value(DropNature+'[]','Array',true);
			
			for (var i=0; i< DragSelectedSlots.length; i++) {
				if (!jIN_ARRAY(DropSlots,DragSelectedSlots[i])) {
					var TimeSlotID = DragSelectedSlots[i];
					
					var Content = Get_Slot_Content_Html(TimeSlotID,DropNature);
          var Div = $(Content);
          $('#'+DropNature+'-Cell').append(Div);
				}
			}
			
			var Content = '';
			for (var i=0; i< DragSlots.length; i++) {
				if (!jIN_ARRAY(DragSelectedSlots,DragSlots[i])) {
					var TimeSlotID = DragSlots[i];
					
					Content += Get_Slot_Content_Html(TimeSlotID,DragNature);
				}
			}
			
			$('#'+DragNature+'-Cell').html(Content);
			if (is.ie) // useless code to deal with the strange UI crash case in IE
				$('#'+DragNature+'-Cell').parent().hide('fast').show('fast');
			
			$("div#OnDuty-Cell").css('background-color','#EEEEEE');
			$("div#Outgoing-Cell").css('background-color','#E2EDF9');
			$("div#Holiday-Cell").css('background-color','#FFEDED');
		},
		activate: function(ev, ui) {
			$(this).css('background-color','#DEB887');
		},
		over: function(ev, ui) {
			$(this).css('background-color','#FFFFE6');
		},
		out:  function(ev, ui) {
			$(this).css('background-color','#DEB887');
		}
	});
}
}

// misc
{
function Get_Slot_Content_Html(TimeSlotID,ColumnNature) {
	var Content = '';
	var SlotName = document.getElementById('SlotName['+TimeSlotID+']').value;
	
	if (ColumnNature == "Outgoing" || ColumnNature == "Holiday") {
		Content += '<table style="background: transparent; padding:0px; border:0px; width:100%; padding:0px; spacing:0px; cursor: pointer;">';
		Content += '<tr>';
		Content += '<td style="background: transparent; padding:0px; border:0px; padding:0px; spacing:0px">';
	}
	Content += '<div onclick="Change_Background_Color(\''+TimeSlotID+'-'+ColumnNature+'\',this);" style="cursor: pointer;">';
	Content += '<input style="display:none;" name="'+ColumnNature+'[]" id="'+TimeSlotID+'-'+ColumnNature+'" type="checkbox" value="'+TimeSlotID+'"/>';
  Content += '<font class="staff_name" style="color:#000000;">'+SlotName+'</font>';
  Content += '</div>';
  if (ColumnNature == "Outgoing" || ColumnNature == "Holiday") {
	  Content += '</td>';
		Content += '<td style="background: transparent; padding:0px; border:0px; padding:0px; spacing:0px; width:20px;">';
		Content += '<span class="table_row_tool" style="float:right;">';
		Content += '<a href="#" onClick="Show_Reason_Layer(this); return false;" class="reason'+(($('input#'+TimeSlotID+'-'+ColumnNature+'-User-Reason').val() > 0)? '':'_alert')+'" title="<?=$Lang['StaffAttendance']['FullDayReason']?>" style="display:inline; float:right;"></a>';
		Content += '\n'; // this linebreak must be included for firefox process
		Content += '<div id="'+TimeSlotID+'-'+ColumnNature+'ReasonLayer" class="selectbox_layer ReasonSelector">';
		Content += '\n'; // this linebreak must be included for firefox process
	  Content += '<select name="'+TimeSlotID+'-'+ColumnNature+'" id="'+TimeSlotID+'-'+ColumnNature+'" onchange="Check_Reason_Icon(this);">';
	  Content += '<option value=""><?=$Lang['StaffAttendance']['SelectReason']?></option>';
	  var ReasonList = document.getElementsByName(ColumnNature+'ReasonList[]');
		for (var j=0; j< ReasonList.length; j++) {
			Content += '<option value="'+(ReasonList[j].id)+'" '+((ReasonList[j].id == $('input#'+TimeSlotID+'-'+ColumnNature+'-User-Reason').val())? "selected":"")+'>'+htmlspecialchars(ReasonList[j].value,3)+'</option>';
		}
	  Content += '</select>';
	  Content += '<br /><?=$Lang['StaffAttendance']['Remark']?><br /><textarea name="'+TimeSlotID+'-'+ColumnNature+'-Remark" id="'+TimeSlotID+'-'+ColumnNature+'-Remark" rows="3" wrap="virtual" style="width:140px;height:90px;"></textarea>';
	  Content += '<br /><input class="formsmallbutton" onmouseover="this.className=\'formsmallbuttonon\'" onmouseout="this.className=\'formsmallbutton\'" value="<?=$Lang['Btn']['Done']?>" type="button" onclick="$(this).closest(\'div\').css(\'visibility\',\'hidden\');">';
	  Content += '</div>';
		Content += '</span>';
		Content += '</td>';
		Content += '</tr>';
		Content += '</table>';
	}
	
	return Content;
}
}

// thick box function 
{
//on page load call tb_init
function Thick_Box_Init(){   
	tb_init('a.thickbox');//pass where to apply thickbox
}
}
</script>