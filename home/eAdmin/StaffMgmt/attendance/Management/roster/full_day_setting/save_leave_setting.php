<?php
// Editing by 
/*
 * 2018-02-07 Carlos: Created.
 */
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('MGMT-LEAVERECORD'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	intranet_closedb();
	echo '0';
	exit();
}


$StaffID = $_POST['StaffID'];
if(is_array($StaffID)){
    $StaffID = $StaffID[0];
}

// full day setting
$StartDate = $_POST[$StaffID.'-TargetStartDate'];
$EndDate = $_POST[$StaffID.'-TargetEndDate'];
$RecordType = $_POST[$StaffID.'-RecordType'];
$ReasonID = $_POST[$StaffID.'-ReasonID'];
$Remark = $_POST[$StaffID.'-Remark'];
if($StaffAttend3->isMagicQuotesOn()){
    $Remark = stripslashes($Remark);
}

// Slot setting
$Outgoing = $_POST[$StaffID.'-Outgoing']; // outgoing time slot ids
$Holiday = $_POST[$StaffID.'-Holiday']; // holiday time slot ids
//$OutgoingReason = $_POST[$StaffID.'-OutgoingReason'];
//$HolidayReason = $_POST[$StaffID.'-HolidayReason'];
$OutgoingReason = array();
$HolidayReason = array();
for($i=0;$i<count($Outgoing);$i++){
    $timeslotId = $Outgoing[$i];
    $OutgoingReason[$timeslotId] = $_POST[$StaffID.'-'.$timeslotId.'-Outgoing-User-Reason'];
}
for($i=0;$i<count($Holiday);$i++){
    $timeslotId = $Holiday[$i];
    $HolidayReason[$timeslotId] = $_POST[$StaffID.'-'.$timeslotId.'-Holiday-User-Reason'];
}
$OutgoingRemark = array();
$HolidayRemark = array();
for($i=0;$i<count($Outgoing);$i++){
    $timeslotId = $Outgoing[$i];
    $slotRemark =  $_POST[$StaffID.'-'.$timeslotId.'-Outgoing-Remark'];
    if($StaffAttend3->isMagicQuotesOn()){
    	$slotRemark = stripslashes($slotRemark);
    }
    $OutgoingRemark[$timeslotId] = trim($slotRemark);
}
for($i=0;$i<count($Holiday);$i++){
    $timeslotId = $Holiday[$i];
    $slotRemark = $_POST[$StaffID.'-'.$timeslotId.'-Holiday-Remark'];
    if($StaffAttend3->isMagicQuotesOn()){
    	$slotRemark = stripslashes($slotRemark);
    }
    $HolidayRemark[$timeslotId] = trim($slotRemark);
}
$LeaveNature = $_POST[$StaffID.'-LeaveNature'];
$TargetDate = $_POST[$StaffID.'-TargetDate'];

$StaffAttend3->Start_Trans();
if ($StaffAttend3->Save_Full_Day_Setting($StaffID,$RecordType,$StartDate,$EndDate,$ReasonID,$LeaveNature,$Outgoing,$Holiday,$OutgoingReason,$HolidayReason,$TargetDate,$Remark,$OutgoingRemark,$HolidayRemark)) {
    //echo $Lang['StaffAttendance']['FullDaySettingSaveSuccess'];
    echo '1';
    $StaffAttend3->Commit_Trans();
}
else {
    //echo $Lang['StaffAttendance']['FullDaySettingSaveFail'];
    echo '0';
    $StaffAttend3->RollBack_Trans();
}

intranet_closedb();
?>