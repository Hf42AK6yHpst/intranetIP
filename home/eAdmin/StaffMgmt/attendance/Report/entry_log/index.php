<?php
// editing by 
/********************************** Cahnges *******************************************
 * 2011-07-12 (Carlos): added mode EntryLog
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('REPORT-ENTRYLOG'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['ReportEntryLog'] = 1;

$TAGS_OBJ[] = array($Lang['StaffAttendance']['EntryLogReport'], "", 1);
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
//$TAGS_OBJ_RIGHT[] = array(gen_online_help_btn_and_layer('staff_attendance','entry_log'));
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $StaffAttend3UI->Get_Report_Entry_Log_Index();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script type="text/JavaScript" language="JavaScript">
// ajax function
{
function Generate_Report(Mode) {
	var Mode = Mode || "View";
	var TargetUser = Get_Selection_Value("TargetUser[]");

	if (TargetUser.length == 0) {
		$('span#TargetUserWarningLayer').html('<?=$Lang['StaffAttendance']['EntryLogSelectStaffWarning']?>');
		document.getElementById('TargetUserWarningRow').style.display = "";
	}
	else {
		$('span#TargetUserWarningLayer').html('');
		document.getElementById('TargetUserWarningRow').style.display = "none";
		
		if(Mode == "Print")
		{
			$("input#Mode").val(Mode);
			var FormObj = document.getElementById('EntryLogForm');
			FormObj.action = "ajax_get_entry_log.php";
			FormObj.target = "_blank";
			FormObj.submit();
		}else if(Mode == "Export")
		{
			$("input#Mode").val(Mode);
			var FormObj = document.getElementById('EntryLogForm');
			FormObj.action = "ajax_get_entry_log.php";
			
			FormObj.submit();
		}else if(Mode == "EntryLog")
		{
			$("input#Mode").val(Mode);
			var FormObj = document.getElementById('EntryLogForm');
			FormObj.action = "ajax_get_entry_log.php";
			FormObj.submit();
		}
		else
		{
			var PostVar = {
					"StartDate": $('Input#StartDate').val(),
					"EndDate": $('Input#EndDate').val(),
					"TargetUser[]": TargetUser,
					"GroupBy": Get_Selection_Value("GroupBy","String"), 
					"ShowNoDutyDateOnly": (document.getElementById('ShowNoDutyDateOnly').checked)? "1":"0", 
					"HideWithoutData": (document.getElementById('HideWithoutData').checked)? "1":"0", 
					"Mode":Mode
				}
			
			if($('input#StartDate').val() > $('input#EndDate').val())
			{
				var $WarningLayer = $('#WarningLayer');
				$WarningLayer.html('<?=$Lang['StaffAttendance']['InvalidDateRange']?>');
				$WarningLayer.css('visibility','visible');
				setTimeout("$('#WarningLayer').css('visibility','hidden');",3000);
				return;
			}
			
			Block_Element("ReportLayer");
			$("div#ReportLayer").load('ajax_get_entry_log.php',PostVar,
				function(data){
					if (data == "die") 
						window.top.location = '/';
					else {
						UnBlock_Element("ReportLayer");
					}
				});
		}
	}
}
}

// dom function 
{

}
</script>