<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('REPORT-OT'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['ReportOTRecords'] = 1;

$TAGS_OBJ[] = array($Lang['StaffAttendance']['OTRecordReport'], "", 0);
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
//$TAGS_OBJ_RIGHT[] = array(gen_online_help_btn_and_layer('staff_attendance','ot_rpt'));
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $StaffAttend3UI->Get_Report_OT_Records_Index();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script type="text/JavaScript" language="JavaScript">
// dom function 
{
function Check_Go_Search_OTRecord(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		Get_Report_OT_Table();
	}
	else
		return false;
}

function Check_Go_Search_RedeemRecord(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
	{
		Get_Report_Redeem_Table();
	}
	else
		return false;
}

function ShowHidePeriodSelector(SelectorID, LayerID, TargetIndex)
{
	var $SelectorObj = $('#'+SelectorID);
	var $LayerObj = $('#'+LayerID);
	
	if($SelectorObj.val() == TargetIndex)
	{
		$LayerObj.css('display','inline');
	}else
	{
		$LayerObj.css('display','none');
	}
}

function SetTimerToHideWarning(WarningLayerId, ticks)
{
	var action = "$('#"+WarningLayerId+"').css('visibility','hidden');";
	setTimeout(action,ticks);
}
}

// ajax function
{
function Get_Report_OT_Index()
{
	var PostVar = {
		"SelectDate": $('#SelectDate').val(),
		"SelectStaff": $('#SelectStaff').val(),
		"StartDate": $('#TargetStartDate').val(),
		"EndDate": $('#TargetEndDate').val(),
		"Keyword": encodeURIComponent($('Input#Keyword').val())
	}

	Block_Element("ReportOTLayer");
	$.post('ajax_get_ot_index.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#ReportOTLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("ReportOTLayer");
						}
					});
}

function Get_Report_OT_Table()
{
	var PostVar = {
		"SelectDate": $('#SelectDate').val(),
		"SelectStaff": $('#SelectStaff').val(),
		"StartDate": $('#TargetStartDate').val(),
		"EndDate": $('#TargetEndDate').val(),
		"SortField": $('input#SortField').val(),
		"SortOrder": $('input#SortOrder').val(),
		"Keyword": encodeURIComponent($('Input#Keyword').val())
	}
	
	if(document.getElementById('TargetStartDate') && $('#SelectDate').val() == 4)
	{
		var dateStartObj = document.getElementById('TargetStartDate');
		var dateEndObj = document.getElementById('TargetEndDate');
		
		if(!check_date_without_return_msg(dateStartObj) || !check_date_without_return_msg(dateEndObj))
		{
			$('#DateWarningLayer').html('<?=$Lang['LessonAttendance']['InvalidDateFormat']?>');
			$('#DateWarningLayer').css('visibility','visible');
			SetTimerToHideWarning('DateWarningLayer', 3000);
			return;
		}
		
		if($('Input#TargetStartDate').val() > $('Input#TargetEndDate').val())
		{
			$('#DateWarningLayer').html('<?=$Lang['StaffAttendance']['InvalidDateRange']?>');
			$('#DateWarningLayer').css('visibility','visible');
			SetTimerToHideWarning('DateWarningLayer', 3000);
			return;
		}
	}
	
	Block_Element("ReportOTTableLayer");
	$.post('ajax_get_ot_table.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#ReportOTTableLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("ReportOTTableLayer");
						}
					});
}

function Get_Report_Redeem_Index()
{
	var PostVar = {
		"SelectDate": $('#SelectDate').val(),
		"SelectStaff": $('#SelectStaff').val(),
		"StartDate": $('#TargetStartDate').val(),
		"EndDate": $('#TargetEndDate').val(),
		"Keyword": encodeURIComponent($('Input#Keyword').val())
	}

	Block_Element("ReportOTLayer");
	$.post('ajax_get_redeem_index.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#ReportOTLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("ReportOTLayer");
						}
					});
}

function Get_Report_Redeem_Table()
{
	var PostVar = {
		"SelectDate": $('#SelectDate').val(),
		"SelectStaff": $('#SelectStaff').val(),
		"StartDate": $('#TargetStartDate').val(),
		"EndDate": $('#TargetEndDate').val(),
		"SortField": $('input#SortField').val(),
		"SortOrder": $('input#SortOrder').val(),
		"Keyword": encodeURIComponent($('Input#Keyword').val())
	}
	
	if(document.getElementById('TargetStartDate') && $('#SelectDate').val() == 4)
	{
		var dateStartObj = document.getElementById('TargetStartDate');
		var dateEndObj = document.getElementById('TargetEndDate');
		
		if(!check_date_without_return_msg(dateStartObj) || !check_date_without_return_msg(dateEndObj))
		{
			$('#DateWarningLayer').html('<?=$Lang['LessonAttendance']['InvalidDateFormat']?>');
			$('#DateWarningLayer').css('visibility','visible');
			SetTimerToHideWarning('DateWarningLayer', 3000);
			return;
		}
		
		if($('Input#TargetStartDate').val() > $('Input#TargetEndDate').val())
		{
			$('#DateWarningLayer').html('<?=$Lang['StaffAttendance']['InvalidDateRange']?>');
			$('#DateWarningLayer').css('visibility','visible');
			SetTimerToHideWarning('DateWarningLayer', 3000);
			return;
		}
	}
	
	Block_Element("ReportRedeemTableLayer");
	$.post('ajax_get_redeem_table.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#ReportRedeemTableLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("ReportRedeemTableLayer");
						}
					});
}

function MouseOverEvent(thisElement)
{
	if($('input#SortOrder').val()=='ASC')
		$(thisElement).children('img').attr('src','<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_sort_a_on.gif"?>');
	else
		$(thisElement).children('img').attr('src','<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_sort_d_on.gif"?>');
}

function MouseOutEvent(thisElement)
{
	if($('input#SortOrder').val()=='ASC')
		$(thisElement).children('img').attr('src','<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_sort_a_off.gif"?>');
	else
		$(thisElement).children('img').attr('src','<?=$PATH_WRT_ROOT."images/".$LAYOUT_SKIN."/icon_sort_d_off.gif"?>');
}

function Sort(fieldname)
{
	$('input#SortField').val(fieldname);
	$order = $('input#SortOrder').val();
	if($order=='DESC')
		$('input#SortOrder').val('ASC');
	else
		$('input#SortOrder').val('DESC');
	if($('input#ReportType').val()=='ot')
		Get_Report_OT_Table();
	else
		Get_Report_Redeem_Table();
}

}

// thick box function 
{
//on page load call tb_init
function Thick_Box_Init()
{   
	tb_init('a.thickbox');//pass where to apply thickbox
}
}
</script>

