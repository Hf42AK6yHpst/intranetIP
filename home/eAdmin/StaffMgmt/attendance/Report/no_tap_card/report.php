<?php
// editing by 
/*
 * 2015-08-18 Carlos: Created. 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('REPORT-NOTAPCARD'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	//echo 'die';
	intranet_closedb();
	exit();
}

$StaffAttend3UI = new libstaffattend3_ui();

$params = array();
$params['StartDate'] = intranet_htmlspecialchars($_REQUEST['StartDate']);
$params['EndDate'] = intranet_htmlspecialchars($_REQUEST['EndDate']);
$params['TargetUser'] = IntegerSafe($_REQUEST['TargetUser']);
$params['Mode'] = intranet_htmlspecialchars(strtolower($_REQUEST['Mode']));
$params['GroupBy'] = intranet_htmlspecialchars($_REQUEST['GroupBy']);
$params['NoInTime'] = intranet_htmlspecialchars($_REQUEST['NoInTime']);
$params['NoOutTime'] = intranet_htmlspecialchars($_REQUEST['NoOutTime']);
$params['Status'] = IntegerSafe($_REQUEST['Status']);
//For Print and Export Version
if($params['Mode']=="print" || $Mode=="export")
{
	$params['TargetUser'] = IntegerSafe($_REQUEST['xTargetUser']); 
	$params['StartDate'] = intranet_htmlspecialchars($_REQUEST['xStartDate']);
	$params['EndDate'] = intranet_htmlspecialchars($_REQUEST['xEndDate']);
	$params['GroupBy'] = intranet_htmlspecialchars($_REQUEST['xGroupBy']);
	$params['NoInTime'] = intranet_htmlspecialchars($_REQUEST['xNoInTime']);
	$params['NoOutTime'] = intranet_htmlspecialchars($_REQUEST['xNoOutTime']);
	$params['Status'] = IntegerSafe($_REQUEST['xStatus']);
}

if($params['Mode']=="print") include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
$html = $StaffAttend3UI->Get_Report_No_Tap_Card_Report($params);
if($params['Mode']!='export'){
	echo $html;
}
if($params['Mode']=="print") include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");

intranet_closedb();
?>