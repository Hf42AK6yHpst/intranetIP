<?php
// Editing by 
/*
 * 2015-08-18 Carlos: Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('REPORT-NOTAPCARD'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['ReportNoTapCard'] = 1;

$TAGS_OBJ[] = array($Lang['StaffAttendance']['NoTapCardReport'], "", 1);
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
//$TAGS_OBJ_RIGHT[] = array(gen_online_help_btn_and_layer('staff_attendance','entry_log'));
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $StaffAttend3UI->Get_Report_No_Tap_Card_Index();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script type="text/JavaScript" language="JavaScript">
function Generate_Report(Mode) 
{
	var Mode = Mode || "view";
	var TargetUser = Get_Selection_Value("TargetUser[]");
	var isValid = true;

	if (TargetUser.length == 0) {
		$('#TargetUserWarningLayer').html('<?=$Lang['StaffAttendance']['EntryLogSelectStaffWarning']?>');
		$('#TargetUserWarningLayer').show();
		isValid = false;
	}else{
		$('#TargetUserWarningLayer').html('');
		$('#TargetUserWarningLayer').hide();
	}
	
	if(!$('input#NoInTime').is(':checked') && !$('input#NoOutTime').is(':checked')){
		$('#TimeWarningLayer').html('<?=$Lang['StaffAttendance']['RequestCheckOptionWarningMsg']?>').show();
		isValid = false;
	}else{
		$('#TimeWarningLayer').hide();
	}
	
	if($('input[name="Status\\[\\]"]:checked').length == 0){
		isValid = false;
		$('#StatusWarningLayer').html('<?=$Lang['StaffAttendance']['RequestCheckOptionWarningMsg']?>').show();
	}else{
		$('#StatusWarningLayer').hide();
	}
	
	if(!isValid) return false;
	
	if(Mode == "print")
	{
		$("input#Mode").val(Mode);
		var FormObj = document.getElementById('ReportForm');
		FormObj.action = "report.php";
		FormObj.target = "_blank";
		FormObj.submit();
	}else if(Mode == "export")
	{
		$("input#Mode").val(Mode);
		var FormObj = document.getElementById('ReportForm');
		FormObj.action = "report.php";
		FormObj.submit();
	}else
	{
		var elems = document.getElementsByName('Status[]');
		var statusValues = [];
		for(var i=0;i<elems.length;i++){
			if(elems[i].checked) statusValues.push(elems[i].value);
		}
		
		var PostVar = {
				"StartDate": $('input#StartDate').val(),
				"EndDate": $('input#EndDate').val(),
				"TargetUser[]": TargetUser,
				"NoInTime": $('input#NoInTime').is(':checked')?'1':'0',
				"NoOutTime": $('input#NoOutTime').is(':checked')?'1':'0',
				"Status[]": statusValues,
				"GroupBy": Get_Selection_Value("GroupBy","String"), 
				"Mode":Mode
		};
		
		if($('input#StartDate').val() > $('input#EndDate').val())
		{
			var $WarningLayer = $('#WarningLayer');
			$WarningLayer.html('<?=$Lang['StaffAttendance']['InvalidDateRange']?>');
			$WarningLayer.css('visibility','visible');
			setTimeout("$('#WarningLayer').css('visibility','hidden');",3000);
			return;
		}
		
		Block_Element("ReportLayer");
		$("div#ReportLayer").load(
			'report.php',
			PostVar,
			function(data){
				UnBlock_Element("ReportLayer");
			}
		);
	}
}
</script>