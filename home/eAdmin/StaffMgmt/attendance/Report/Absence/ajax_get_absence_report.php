<?php
//editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('REPORT-ABSENCE'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo "die";
	intranet_closedb();
	exit();
}

$StaffAttend3UI = new libstaffattend3_ui();

$Params = array();
$Params["ReportType"] = intranet_htmlspecialchars($_REQUEST["ReportType"]);
$Params["StaffID"] = IntegerSafe($_REQUEST["StaffID"]);
$Params["GroupID"] = IntegerSafe($_REQUEST["GroupID"]);
$Params["TargetStartDate"] = intranet_htmlspecialchars($_REQUEST["TargetStartDate"]);
$Params["TargetEndDate"] = intranet_htmlspecialchars($_REQUEST["TargetEndDate"]);
$Params["Format"] = intranet_htmlspecialchars($_REQUEST["Format"]);
$Params["ColAll"] = intranet_htmlspecialchars($_REQUEST["ColAll"]);
$Params["ColReason"] = intranet_htmlspecialchars($_REQUEST["ColReason"]);
$Params["ColRemark"] = intranet_htmlspecialchars($_REQUEST["ColRemark"]);
$Params["Task"] = intranet_htmlspecialchars(strtoupper($_REQUEST["Task"]));
$Params["SelectWaived"] = intranet_htmlspecialchars($_REQUEST["SelectWaived"]);

if($Params["Task"]=="PRINT") include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
echo $StaffAttend3UI->Get_Report_Absence_Table($Params);
if($Params["Task"]=="PRINT") include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");

intranet_closedb();
?>