<?php
//editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('REPORT-LEAVEABSENCE'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo "die";
	intranet_closedb();
	exit();
}

$StaffAttend3UI = new libstaffattend3_ui();

$Params = array();
$Params["ReportType"] = $_REQUEST["ReportType"];
$Params["StaffID"] = $_REQUEST["StaffID"];
$Params["GroupID"] = $_REQUEST["GroupID"];
$Params["TargetStartDate"] = $_REQUEST["TargetStartDate"];
$Params["TargetEndDate"] = $_REQUEST["TargetEndDate"];
$Params["Format"] = $_REQUEST["Format"];
$Params["Status"] = $_REQUEST["Status"];
$Params["Waived"] = $_REQUEST["Waived"];
$Params["HaveRecords"] = $_REQUEST["HaveRecords"];
$Params["ShowTimeSlot"] = $_REQUEST["ShowTimeSlot"];
$Params["Task"] = strtoupper($_REQUEST["Task"]);

if($Params["Task"]=="PRINT") include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
echo $StaffAttend3UI->Get_Report_LeaveAbsence_Table($Params);
if($Params["Task"]=="PRINT") include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");

intranet_closedb();
?>