<?php
//editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('REPORT-LEAVEABSENCE'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$StaffAttend3UI = new libstaffattend3_ui();

$Params = array();
$Params["ReportType"] = $_REQUEST["ReportType"];
$Params["StaffID"] = $_REQUEST["SelectPersonal"];
$Params["GroupID"] = $_REQUEST["SelectGroup"];
$Params["TargetStartDate"] = $_REQUEST["TargetStartDate"];
$Params["TargetEndDate"] = $_REQUEST["TargetEndDate"];
$Params["Format"] = $_REQUEST["Format"];
$Params["Status"] = $_REQUEST["SelectStatus"];
$Params["Waived"] = $_REQUEST["SelectWaived"];
$Params["HaveRecords"] = $_REQUEST["SelectHaveRecords"];
$Params["ShowTimeSlot"] = $_REQUEST["ShowTimeSlot"];

$StaffAttend3UI->Get_Report_LeaveAbsence_Table($Params);

intranet_closedb();
?>