<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('REPORT-WORKINGHOUR'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$Action = $_REQUEST["Action"];

if($Action=="get_staff_list"){
	$Settings = $StaffAttend3->Get_General_Settings();
	if($Settings['ReportDisplaySuspendedStaff']==1){
		$recordstatus = "0,1,2";
	}else{
		$recordstatus = "1";
	}
	$ReportDisplayLeftStaff = $Settings['ReportDisplayLeftStaff']==1;
	
	$SelectStaffLevel1 = $_REQUEST["SelectStaffLevel1"];
	$GroupList = $StaffAttend3->Get_Group_List('', false);
	$GroupListAssoc['-1'] = $Lang['StaffAttendance']['Individual'];
    for($i=0;$i<sizeof($GroupList);$i++){
    	$GroupListAssoc[$GroupList[$i]['GroupID']] = $GroupList[$i]['GroupName'];
    }
    $SelectLevel2 = '';
    if(sizeof($SelectStaffLevel1)>0)
    {
	    foreach($SelectStaffLevel1 as $k=>$vid){
			if($vid==-1){
				$SelectLevel2 .= '<optgroup label="-'.htmlspecialchars($GroupListAssoc['-1'],ENT_QUOTES).'-">';
				$IndividualUserList = $StaffAttend3->Get_Aval_User_List("",$recordstatus,$ReportDisplayLeftStaff);
				for($i=0;$i<sizeof($IndividualUserList);$i++)
				{
					$SelectLevel2 .= '<option value="'.$IndividualUserList[$i]['UserID'].'" >'.htmlspecialchars($IndividualUserList[$i]['StaffName'],ENT_QUOTES).'</option>';
				}
			}else if($vid>0){
				$GroupMembers = $StaffAttend3->Get_Group_Member_List($vid,"",$recordstatus,$ReportDisplayLeftStaff);
				$SelectLevel2 .= '<optgroup label="-'.htmlspecialchars($GroupListAssoc[$vid],ENT_QUOTES).'-">';
				for($i=0;$i<sizeof($GroupMembers);$i++)
				{
					$SelectLevel2 .= '<option value="'.$GroupMembers[$i]['UserID'].'" >'.htmlspecialchars($GroupMembers[$i]['StaffName'],ENT_QUOTES).'</option>';
				}
			}
		}
    }
	echo $SelectLevel2;
}

intranet_closedb();
?>