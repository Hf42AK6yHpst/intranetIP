<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('REPORT-ATTENDANCE'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$StaffAttend3UI = new libstaffattend3_ui();

$GroupID = IntegerSafe($_REQUEST['GroupID']);
$Year = intranet_htmlspecialchars($_REQUEST['SelectYear']);
$Month = intranet_htmlspecialchars($_REQUEST['SelectMonth']);
$TargetUserID = IntegerSafe($_REQUEST['SelectStaff']);
$Status = IntegerSafe($_REQUEST['SelectStatus']);
$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
$WithWaived = intranet_htmlspecialchars($_REQUEST['SelectWaived']);

$ByDateRange = $_REQUEST['ByDateRange']==1?true:false;
$StartDate = intranet_htmlspecialchars($_REQUEST['StartDate']);
$EndDate = intranet_htmlspecialchars($_REQUEST['EndDate']);

echo $StaffAttend3UI->Get_Report_Attendance_Monthly_Details($GroupID, $TargetUserID, $Year, $Month, $Status, $Keyword, $WithWaived, $ByDateRange, $StartDate, $EndDate);

intranet_closedb();
?>