<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('REPORT-ATTENDANCE'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$StaffAttend3UI = new libstaffattend3_ui();

$Year = intranet_htmlspecialchars($_REQUEST['SelectYear']);
$Month = intranet_htmlspecialchars($_REQUEST['SelectMonth']);
$Type = IntegerSafe($_REQUEST['SelectType']);
$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
$Task = intranet_htmlspecialchars(strtoupper($_REQUEST['Task']));
$WithWaived = intranet_htmlspecialchars($_REQUEST['SelectWaived']);
$HaveRecord = intranet_htmlspecialchars($_REQUEST['HaveRecord']);

$ByDateRange = $_REQUEST['ByDateRange']==1?true:false;
$StartDate = intranet_htmlspecialchars($_REQUEST['StartDate']);
$EndDate = intranet_htmlspecialchars($_REQUEST['EndDate']);

if($Task=="PRINT") include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
echo $StaffAttend3UI->Get_Report_Attendance_Monthly_Summary_List($Year, $Month, $Type, $Keyword, $Task, $WithWaived, $ByDateRange, $StartDate, $EndDate, $HaveRecord);
if($Task=="PRINT") include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");

intranet_closedb();
?>