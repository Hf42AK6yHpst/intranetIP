<?php
// editing by 
/*
 * 2019-07-23 (Ray): Add Duty Time
 * 2018-12-14 (Carlos): Fix save/load template settings for ColOTHours.
 * 2015-10-07 (Carlos): Add option [DisplayStatus]
 * 2014-01-09 (Carlos): Add option [DisplayAllCalendarDay]
 */
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('REPORT-CUSTOMIZE'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$SettingName = "CustomizeReportSetting";
$action = $_REQUEST["action"];

if($action=="save"){
	$Params["SettingTemplate"] = $_REQUEST["SettingTemplate"];// SettingID
	$Params["SettingTemplateName"] = trim(stripslashes(urldecode($_REQUEST["SettingTemplateName"]))); // Setting DisplayName
	$Params["SelectStaffLevel1"] = $_REQUEST["SelectStaffLevel1"];
	$Params["SelectStaff"] = $_REQUEST["SelectStaff"];
	$Params["PeriodTypeMonthly"] = $_REQUEST["PeriodTypeMonthly"];
	$Params["PeriodTypeWeekly"] = $_REQUEST["PeriodTypeWeekly"];
	$Params["PeriodTypeDaily"] = $_REQUEST["PeriodTypeDaily"];
	$Params["TargetStartDate"] = $_REQUEST["TargetStartDate"];
	$Params["TargetEndDate"] = $_REQUEST["TargetEndDate"];
	$Params["StatusAll"] = $_REQUEST["StatusAll"];
	$Params["StatusPresent"] = $_REQUEST["StatusPresent"];
	$Params["StatusLate"] = $_REQUEST["StatusLate"];
	$Params["StatusAbsent"] = $_REQUEST["StatusAbsent"];
	$Params["StatusOuting"] = $_REQUEST["StatusOuting"];
	$Params["StatusEarlyLeave"] = $_REQUEST["StatusEarlyLeave"];
	$Params["StatusHoliday"] = $_REQUEST["StatusHoliday"];
	$Params["StatusAbsentSuspected"] = $_REQUEST["StatusAbsentSuspected"];
	$Params["SelectWaived"] = $_REQUEST["SelectWaived"];
	$Params["FormatDetails"] = $_REQUEST["FormatDetails"];
	$Params["FormatSummary"] = $_REQUEST["FormatSummary"];
	$Params["HaveRecord"] = $_REQUEST["HaveRecord"];
	$Params["ColAll"] = $_REQUEST["ColAll"];
	$Params["ColStaffName"] = $_REQUEST["ColStaffName"];
	$Params["ColGroupName"] = $_REQUEST["ColGroupName"];
	$Params["ColStatus"] = $_REQUEST["ColStatus"];
	$Params["ColTime"] = $_REQUEST["ColTime"];
	$Params["ColStation"] = $_REQUEST["ColStation"];
	$Params["ColSlotName"] = $_REQUEST["ColSlotName"];
	$Params["ColReason"] = $_REQUEST["ColReason"];
	$Params["ColLateMins"] = $_REQUEST["ColLateMins"];
	$Params["ColEarlyLeaveMins"] = $_REQUEST["ColEarlyLeaveMins"];
	$Params["ColWorkingdays"] = $_REQUEST["ColWorkingdays"];
	$Params["ColRemark"] = $_REQUEST["ColRemark"];
	$Params["ColOffDuty"] = $_REQUEST["ColOffDuty"];
	$Params["PeriodTypeMonthlyByStaff"] = $_REQUEST["PeriodTypeMonthlyByStaff"];
	$Params["DisplayAllCalendarDay"] = $_REQUEST["DisplayAllCalendarDay"];
	$Params["DisplayStatus"] = $_REQUEST["DisplayStatus"];
	$Params["ColOTHours"] = $_REQUEST["ColOTHours"];
	$Params["ColDutyTime"] = $_REQUEST["ColDutyTime"];

	$SelectStaffLevel1Size = sizeof($Params["SelectStaffLevel1"]);
	$SelectStaffSize = sizeof($Params["SelectStaff"]);
	
	$SettingValue = "";
	
	if($SelectStaffLevel1Size>0) $SettingValue .= implode(",",$Params["SelectStaffLevel1"]);
	$SettingValue .= "###";
	if($SelectStaffSize>0) $SettingValue .= implode(",",$Params["SelectStaff"]);
	$SettingValue .= "###";
	$SettingValue .= $Params["PeriodTypeMonthly"].","; // index 0
	$SettingValue .= $Params["PeriodTypeWeekly"].",";
	$SettingValue .= $Params["PeriodTypeDaily"].",";
	$SettingValue .= $Params["TargetStartDate"].",";
	$SettingValue .= $Params["TargetEndDate"].",";
	$SettingValue .= $Params["StatusAll"].",";
	$SettingValue .= $Params["StatusPresent"].",";
	$SettingValue .= $Params["StatusLate"].",";
	$SettingValue .= $Params["StatusAbsent"].",";
	$SettingValue .= $Params["StatusOuting"].",";
	$SettingValue .= $Params["StatusEarlyLeave"].",";
	$SettingValue .= $Params["StatusHoliday"].",";
	$SettingValue .= $Params["StatusAbsentSuspected"].",";
	$SettingValue .= $Params["SelectWaived"].",";
	$SettingValue .= $Params["FormatDetails"].",";
	$SettingValue .= $Params["FormatSummary"].",";
	$SettingValue .= $Params["HaveRecord"].",";
	$SettingValue .= $Params["ColAll"].",";
	$SettingValue .= $Params["ColStaffName"].",";
	$SettingValue .= $Params["ColGroupName"].",";
	$SettingValue .= $Params["ColStatus"].",";
	$SettingValue .= $Params["ColTime"].",";
	$SettingValue .= $Params["ColStation"].",";
	$SettingValue .= $Params["ColSlotName"].",";
	$SettingValue .= $Params["ColReason"].",";
	$SettingValue .= $Params["ColLateMins"].",";
	$SettingValue .= $Params["ColEarlyLeaveMins"].",";
	$SettingValue .= $Params["ColWorkingdays"].",";
	$SettingValue .= $Params["ColRemark"].",";
	$SettingValue .= $Params["ColOffDuty"].",";
	$SettingValue .= $Params["PeriodTypeMonthlyByStaff"].",";
	$SettingValue .= $Params["DisplayAllCalendarDay"].",";
	$SettingValue .= $Params["DisplayStatus"].",";
	$SettingValue .= $Params["ColOTHours"].","; // index 33
	$SettingValue .= $Params["ColDutyTime"];

	$result = $StaffAttend3->Set_User_Setting($_SESSION['UserID'],$SettingName,$SettingValue,$Params["SettingTemplateName"],$Params["SettingTemplate"]);
	if($result) 
		echo "1";
	else 
		echo "0";
}else if($action=="load"){
	$Settings = $StaffAttend3->Get_General_Settings();
	if($Settings['ReportDisplaySuspendedStaff']==1){
		$recordstatus = "0,1,2";
	}else{
		$recordstatus = "1";
	}
	$SettingTemplate = $_REQUEST["SettingTemplate"];// SettingID
	$SettingValue = "";
	$resArr = $StaffAttend3->Get_User_Setting($_SESSION['UserID'],$SettingName,'',$SettingTemplate);
	if(sizeof($resArr)>0){
		$SettingValue = $resArr[0]['SettingValue'];
		
		$Parts = explode("###",$SettingValue);
		$SelectStaffLevel1 = explode(",",$Parts[0]);
		$SelectStaff = explode(",",$Parts[1]);
		$RemainSettings = $Parts[2];
		
		$GroupList = $StaffAttend3->Get_Group_List('', false);
		$GroupListAssoc['-1'] = $Lang['StaffAttendance']['Individual'];
        for($i=0;$i<sizeof($GroupList);$i++){
        	$GroupListAssoc[$GroupList[$i]['GroupID']] = $GroupList[$i]['GroupName'];
        }
        $SelectLevel2 = '';
        foreach($SelectStaffLevel1 as $k=>$vid){
			if($vid==-1){
				$SelectLevel2 .= '<optgroup label="-'.htmlspecialchars($GroupListAssoc['-1'],ENT_QUOTES).'-">';
				$IndividualUserList = $StaffAttend3->Get_Aval_User_List("",$recordstatus);
				for($i=0;$i<sizeof($IndividualUserList);$i++)
				{
					$selected = in_array($IndividualUserList[$i]['UserID'],$SelectStaff)?'selected':'';
					$SelectLevel2 .= '<option value="'.$IndividualUserList[$i]['UserID'].'" '.$selected.'>'.htmlspecialchars($IndividualUserList[$i]['StaffName'],ENT_QUOTES).'</option>';
				}
			}else if($vid>0){
				$GroupMembers = $StaffAttend3->Get_Group_Member_List($vid,"",$recordstatus);
				$SelectLevel2 .= '<optgroup label="-'.htmlspecialchars($GroupListAssoc[$vid],ENT_QUOTES).'-">';
				for($i=0;$i<sizeof($GroupMembers);$i++)
				{
					$selected = in_array($GroupMembers[$i]['UserID'],$SelectStaff)?'selected':'';
					$SelectLevel2 .= '<option value="'.$GroupMembers[$i]['UserID'].'" '.$selected.'>'.htmlspecialchars($GroupMembers[$i]['StaffName'],ENT_QUOTES).'</option>';
				}
			}
		}
		$SettingValue = $Parts[0]."###".$SelectLevel2."###".$RemainSettings;
	}
	echo $SettingValue;
}else if($action=="get_staff_list"){
	$Settings = $StaffAttend3->Get_General_Settings();
	if($Settings['ReportDisplaySuspendedStaff']==1){
		$recordstatus = "0,1,2";
	}else{
		$recordstatus = "1";
	}
	$ReportDisplayLeftStaff = $Settings['ReportDisplayLeftStaff']==1;
	
	$SelectStaffLevel1 = $_REQUEST["SelectStaffLevel1"];
	$GroupList = $StaffAttend3->Get_Group_List('', false);
	$GroupListAssoc['-1'] = $Lang['StaffAttendance']['Individual'];
    for($i=0;$i<sizeof($GroupList);$i++){
    	$GroupListAssoc[$GroupList[$i]['GroupID']] = $GroupList[$i]['GroupName'];
    }
    $SelectLevel2 = '';
    if(sizeof($SelectStaffLevel1)>0)
    {
	    foreach($SelectStaffLevel1 as $k=>$vid){
			if($vid==-1){
				$SelectLevel2 .= '<optgroup label="-'.htmlspecialchars($GroupListAssoc['-1'],ENT_QUOTES).'-">';
				$IndividualUserList = $StaffAttend3->Get_Aval_User_List("",$recordstatus,$ReportDisplayLeftStaff);
				for($i=0;$i<sizeof($IndividualUserList);$i++)
				{
					$SelectLevel2 .= '<option value="'.$IndividualUserList[$i]['UserID'].'" >'.htmlspecialchars($IndividualUserList[$i]['StaffName'],ENT_QUOTES).'</option>';
				}
			}else if($vid>0){
				$GroupMembers = $StaffAttend3->Get_Group_Member_List($vid,"",$recordstatus,$ReportDisplayLeftStaff);
				$SelectLevel2 .= '<optgroup label="-'.htmlspecialchars($GroupListAssoc[$vid],ENT_QUOTES).'-">';
				for($i=0;$i<sizeof($GroupMembers);$i++)
				{
					$SelectLevel2 .= '<option value="'.$GroupMembers[$i]['UserID'].'" >'.htmlspecialchars($GroupMembers[$i]['StaffName'],ENT_QUOTES).'</option>';
				}
			}
		}
    }
	echo $SelectLevel2;
}else if($action=="add"){
	$StaffAttend3UI = new libstaffattend3_ui();
	echo $StaffAttend3UI->Get_User_Setting_Template_Form($action);
}else if($action=="check"){
	$SettingTemplateName = trim(stripslashes(urldecode($_REQUEST['SettingTemplateName'])));
	if($SettingTemplateName!='')
	{
		$setting_list = $StaffAttend3->Get_User_Setting($_SESSION['UserID'],$SettingName,$SettingTemplateName);
		if(sizeof($setting_list)>0){
			echo "0"; // duplicated setting template name
		}else{
			echo "1"; // ok no duplication
		}
	}else{
		echo "0"; // blank name, not ok
	}
}else if($action=="delete"){
	$SettingID = $_REQUEST['SettingID'];
	$result = $StaffAttend3->Delete_User_Setting($_SESSION['UserID'],$SettingName,$SettingID);
	if($result)
		echo "1";
	else
		echo "0";
}else if($action=="get_setting_selection"){
	$SettingTemplateName = trim(stripslashes(urldecode($_REQUEST['SettingTemplateName'])));
	$SettingTemplateArr = $StaffAttend3->Get_User_Setting($_SESSION['UserID'],"CustomizeReportSetting");
	$TemplateSelection = '<option value="">-- '.$Lang['StaffAttendance']['PleaseSelect'].' --</option>';
	for($i=0;$i<sizeof($SettingTemplateArr);$i++){
		$selected = ($SettingTemplateArr[$i]['DisplayName']==$SettingTemplateName?'selected':'');
		$TemplateSelection .= '<optgroup label="'.$SettingTemplateArr[$i]['DateModified'].'">';
		$TemplateSelection .= '<option value="'.$SettingTemplateArr[$i]['SettingID'].'" '.$selected.'>'.htmlspecialchars($SettingTemplateArr[$i]['DisplayName'],ENT_QUOTES).'</option>';
	}
	echo $TemplateSelection;
}

intranet_closedb();
?>