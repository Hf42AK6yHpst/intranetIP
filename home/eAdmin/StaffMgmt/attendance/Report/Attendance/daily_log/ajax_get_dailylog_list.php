<?php
// editing by 
/*
 * 2015-10-08 (Carlos): Added parameter [ShowAbsentSuspectedRecord].
 */
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('REPORT-DAILYLOG'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$StaffAttend3UI = new libstaffattend3_ui();

$TargetDate = $_REQUEST['TargetDate'];
$SelectType = $_REQUEST['SelectType'];
$SelectStaff = $_REQUEST['SelectStaff'];
$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
$SortField = $_REQUEST['SortField'];
$SortOrder = $_REQUEST['SortOrder'];
$Task = strtoupper($_REQUEST['Task']);
$ShowAbsentSuspectedRecord = $_REQUEST['ShowAbsentSuspectedRecord'];

if($Task=="PRINT") include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
echo $StaffAttend3UI->Get_Report_DailyLog_List($TargetDate, $SelectType, $SelectStaff, $Keyword, "", $SortField, $SortOrder,$Task,$ShowAbsentSuspectedRecord);
if($Task=="PRINT") include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");

intranet_closedb();
?>