<?php
// editing by 

/*
 * Change log
 * 
 * Date:	2017-07-14 (Icarus)
 * 			change the flash chart to highchart in the js function Get_Flash_Chart()
 * 
 * */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");
include_once($PATH_WRT_ROOT."includes/flashchart_basic/php-ofc-library/open-flash-chart.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('REPORT-WEEKDAYSTAT'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();
$json = new JSON_obj();

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['WeekDayDistribution'] = 1;
$TAGS_OBJ[] = array($Lang['StaffAttendance']['WeekdayDistribution'], $PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Statistics/attendance/", 1);
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
//$TAGS_OBJ_RIGHT[] = array(gen_online_help_btn_and_layer('staff_attendance','attendance_stat'));
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $StaffAttend3UI->Get_Stat_Weekly_Distribution_Form();
?>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<script language="javascript">
var ChartData = '';
function Get_Flash_Chart() {
	var PostVar = {
		"GroupID": document.getElementById('GroupID').options[document.getElementById('GroupID').selectedIndex].value, 
		"StartDate": $('input#StartDate').val(),
		"EndDate": $('input#EndDate').val(),
		"Status[]": Get_Check_Box_Value('Status[]','Array')
	};
	
	if($('Input#StartDate').val() > $('Input#EndDate').val())
	{
		var $WarningLayer = $('#WarningLayer');
		$WarningLayer.html('<?=$Lang['StaffAttendance']['InvalidDateRange']?>');
		$WarningLayer.css('visibility','visible');
		setTimeout("$('#WarningLayer').css('visibility','hidden');",3000);
		return;
	}
	
	$.post('ajax_get_weekday_distribution.php',PostVar,
		function (highchartDataAry) {
			if (highchartDataAry == "die") 
				window.top.location="/";
			else {
				//ChartData = data;
				//embedFlash('FlashLayer',"<?=$PATH_WRT_ROOT?>includes/flashchart_basic/open-flash-chart.swf", "", "", "850", "350", "9.0.0", "YES");
				var obj = JSON.parse(highchartDataAry);
				
				var data = obj['json_ary'];
				var xaxis_data = obj['xaxis_data'];
				var y_max_record = obj['y_max_record'];
				
				$(function () {
					Highcharts.setOptions({
						global: {
					    	useUTC: false
					  	}
					});
					
				    $('#container').highcharts({
				    	
				        chart: {
				            type: 'column',
				        },
				
				    	title: {
				        	text: '<?=$Lang['StaffAttendance']['WeekdayDistribution']?>'
				        },
				        
				        xAxis: {
				            categories: xaxis_data, 
				            labels: {
				                style: {
				                    fontSize: '13px',
				                    fontFamily: 'Verdana, sans-serif'
				                }
				            },
				            title: {
				            	text: '<?=$Lang['StaffAttendance']['WeekDay']?>'
				            }
				        },
				        
				        yAxis: {
				        	gridLineWidth: 1,
					        min:0,
				       		max: y_max_record,
					        labels: {
				                style: {
				                    fontSize: '13px',
				                    fontFamily: 'Arial,sans-serif'
				                }
				            },
					        title: {
					            text: '<?=$Lang['StaffAttendance']['TimesOf']?>'
					        }
					    },
					    
					    tooltip: {
					        headerFormat: '<span style="font-size:14px;padding: 0px"><b><center>{point.key}</center></b></span><table>',
					        pointFormat: '<tr><td style="color:{series.color};padding: 5px 15px 0px 5px">{series.name}: </td>' +
					            '<td style="padding: 5px 5px 0px 0px"><b>{point.y}</b></td></tr>',
					        footerFormat: '</table>',
					        shared: true,
					        useHTML: true
					    },
				            
				        plotOptions: {
				            line: {
				                dataLabels: {
				                    enabled: true,
				
				                }
				            },
				        },
				        
				        credits: {
				            enabled: false
				        },
				        
				        series: data
				        
				    });
				});			
			}
		}
	);
}

function ofc_ready(){
}

function open_flash_chart_data(){
	if(typeof ChartData == "string"){
		return ChartData;
	}else{
		return JSON.stringify(ChartData);
	}
}

function findSWF(movieName) {
  if (navigator.appName.indexOf("Microsoft")!= -1) {
	return window[movieName];
  } else {
	return document[movieName];
  }
}

function setChart(id, display){
	//for testing
	var heading = document.getElementById("testHead");
	while(heading.hasChildNodes()){
		heading.removeChild(heading.firstChild);
	}
	var h = document.createTextNode(String(id));
	heading.appendChild(h);
	var content = document.getElementById("testContent");
	while(content.hasChildNodes()){
		content.removeChild(content.firstChild);
	}			
	var c = document.createTextNode(String(display));
	content.appendChild(c);
}
</script>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>