<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-BASIC'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$ScrollTo = $_REQUEST['ScrollTo'];

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['DutyBasic'] = 1;
$TAGS_OBJ[] = array($Lang['StaffAttendance']['SetupStatus'], $PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Settings/time_slot/status/", 1);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['StaffInfo'], $PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Settings/time_slot/staff_info/", 0);
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $StaffAttend3UI->Get_Group_Setting_Index();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script>
var SlotJustCheck = 1;
var SaveSlotGoNextStep = false;
// dom function 
{
function Check_Go_Search(evt) {
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		Get_Group_List();
	else
		return false;
}

function Show_Edit_Icon(LayerObj) {
	LayerObj.style.backgroundImage = "url(<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/icon_edit_b.gif)";
	LayerObj.style.backgroundPosition = "center right";
	LayerObj.style.backgroundRepeat = "no-repeat";
}

function Hide_Edit_Icon(LayerObj) {
	LayerObj.style.backgroundImage = "";
	LayerObj.style.backgroundPosition = "";
	LayerObj.style.backgroundRepeat = '';
}

function Check_Slot_Period() {
	var StartHour = Get_Selection_Value('StartHour','String');
	var StartMin = Get_Selection_Value('StartMin','String');
	var StartSec = Get_Selection_Value('StartSec','String');
	var EndHour = Get_Selection_Value('EndHour','String');
	var EndMin = Get_Selection_Value('EndMin','String');
	var EndSec = Get_Selection_Value('EndSec','String');
	var SlotStart = StartHour+":"+StartMin+":"+StartSec;
	var SlotEnd = EndHour+":"+EndMin+":"+EndSec;
	var ElementRow = document.getElementById('SlotTimeWarningRow');
	var ElementLayer = $('div#SlotTimeWarningLayer');
	
	if (SlotStart == SlotEnd) { // time slot must not exceed 24 hours
		ElementLayer.html('<?=$Lang['StaffAttendance']['SlotTimeIntervalLimitWarning']?>');
		ElementRow.style.display = '';
		return false;
	}
	
	ElementLayer.html('');
	ElementRow.style.display = 'none';
}
}

// ajax function 
{
function Get_Member_Form(GroupID) {
	var PostVar = {
		"GroupID": GroupID 
	};
	
	$.post('ajax_get_member_form.php',PostVar,
		function(data) {
			if (data == "die")
				window.top.location = '/';
			else {
				$('div#TB_ajaxContent').html(data);
				$('div#TB_ajaxWindowTitle').html('<?=$Lang['StaffAttendance']['ManageMember']?>');
				UnBlock_Thickbox();
			}
		});
}

function Get_Rename_Group_Form(GroupID) {
	var PostVar = {
		"GroupID": GroupID
	};
	
	$.post('ajax_get_group_form.php',PostVar,
		function(data) {
			if (data == "die")
				window.top.location = '/';
			else 
				$('div#TB_ajaxContent').html(data);
		});
}	

function Save_Group(GroupID,GoNextStep) {
	var GoNextStep = GoNextStep || false;
	var GroupID = GroupID || "";
	var GroupName = $('input#GroupName').val();
	Check_Group_Name(GroupName);
	var GroupWarning = $('div#GroupNameWarningLayer').html();
	if (Trim(GroupWarning) == "") {
		Block_Thickbox();
		
		if (GroupID == "") {
			AjaxPage = 'ajax_save_group.php';

			var PostVar = {
				"GroupName": encodeURIComponent(GroupName)
			};
		}
		else {
			AjaxPage = 'ajax_rename_group.php';

			var PostVar = {
				"GroupID":GroupID,
				"GroupName": encodeURIComponent(GroupName)
			};
		}
		
		$.post(AjaxPage,PostVar,
			function(data) {
				if (data == "die")
					window.top.location = '/';
				else {
					Get_Return_Message(data);
					Temp = data.split('|=|');
					
					if (GoNextStep && Temp[0] == 1) {
						if (GroupID == "") {
							GroupID = Temp[2];
						}
						
						Get_Member_Form(GroupID);
					}
					else
						window.top.tb_remove();
					Get_Group_List();
					Scroll_To_Top();
				}
			});
	}
}

function Get_Add_Group_Form() {
	$.post('ajax_get_group_form.php',"",
		function(data) {
			if (data == "die")
				window.top.location = '/';
			else {
				$('div#TB_ajaxContent').html(data);
				UnBlock_Thickbox();
			}
		});
}
	
function Delete_Group(GroupID) {
	if (confirm('<?=$Lang['StaffAttendance']['DeleteGroupWarning']?>')) {
		var PostVar = {
			"GroupID":GroupID
		}
		
		$.post('ajax_delete_group.php',PostVar,
			function(data) {
				if (data == "die") 
					window.top.location = '/';
				else {
					Get_Return_Message(data);
					Get_Group_List();
					Scroll_To_Top();
				}
			});
	}
}
	
function Delete_Member() {
	var StaffID = Get_Check_Box_Value('StaffID[]','Array');
	
	if (StaffID.length > 0) {
		if (confirm('<?=$Lang['StaffAttendance']['DeleteMemberConfirmMessage']?>')) {
			var PostVar = {
				"GroupID":$('input#GroupID').val(),
				"StaffID[]":StaffID
			}
			
			$.post('ajax_delete_member.php',PostVar,
				function(data) {
					if (data == "die") 
						window.top.location = '/';
					else {
						Get_Return_Message(data);
						Get_Group_List();
						Get_Member_Form($('input#GroupID').val());
						Scroll_To_Top();
					}
				});
		}
	}
	else {
		alert('<?=$Lang['StaffAttendance']['SelectAtLeastOneWarning']?>');
	}
}
	
function Get_Member_List() {
	var PostVar = {
		"GroupID":$('input#GroupID').val()
	}
	
	Block_Element('MemberTableLayer');
	$('div#MemberTableLayer').load('ajax_get_member_list.php',PostVar,
		function(){
			Thick_Box_Init();
			UnBlock_Element('MemberTableLayer');
		});
}
	
function Add_Member(GoNextStep) {
	GoNextStep = GoNextStep || false;
	if (confirm('<?=$Lang['StaffAttendance']['AddMemberWarning']?>')) {
		var GroupID = $('input#GroupID').val();
		var PostVar = {
			"GroupID":GroupID,
			"AddUserID[]":Get_Selection_Value('AddUserID[]','Array',true)
		}
		
		Block_Thickbox();
		$.post('ajax_add_member.php',PostVar,
			function (data) {
				if (data == "die") 
					window.top.location = '/';
				else {
					Get_Return_Message(data);
					Get_Group_List();
					
					if (GoNextStep) 
						Get_Slot_Form("",GroupID);
					else
						window.top.tb_remove();
					Scroll_To_Top();
				}
			});
	}
}
	
function Get_Add_Member_Form() {
	$.post('ajax_get_member_form.php',"",
		function(data) {
			if (data == "die") 
				window.top.location = '/';
			else {
				$('div#TB_ajaxContent').html(data);
				UnBlock_Thickbox();
			}
		});
}
	
function Delete_Slot(SlotID) {
	if (confirm('<?=$Lang['StaffAttendance']['DeleteSlotConfirmMessage']?>')) {
		var PostVar = {
			"SlotID":SlotID
			};
		
		Block_Element("SlotTableLayer");
		$.post('../ajax_delete_slot.php',PostVar,
			function(data) {
				if (data == "die")
					window.top.location = '/';
				else {
					Get_Return_Message(data);
					Get_Group_List();
				}
			});
	}
}
	
function Get_Slot_List() {
	var PostVar = {
		"GroupID": $('input#GroupID').val()
		};
	
	Block_Element("SlotTableLayer");
	$('div#SlotTableLayer').load('ajax_get_slot_list.php',PostVar,
		function() {
			Thick_Box_Init();
			UnBlock_Element("SlotTableLayer");
		});
}	
	
function Save_Slot(GoNextStep) {
	SlotJustCheck = 0; // global variable setting
	SaveSlotGoNextStep = GoNextStep || false; // global variable setting
	Check_Slot_Period();
	Check_Slot_Name();
	Check_Day_Count();

	$(document).ajaxComplete(function(e, xhr, settings) {
		if (settings.url.indexOf('ajax_check_slot_name.php') != -1 && SlotJustCheck == 0) {
			var NameWarning = $('div#SlotNameWarningLayer').html();
			var TimeWarning = $('div#SlotTimeWarningLayer').html();
			var DayCountWarning = $('div#DutyCountWarningLayer').html();
			
			if (Trim(NameWarning) == "" && Trim(TimeWarning) == "" && Trim(DayCountWarning) == "") {
				var StartHour = Get_Selection_Value('StartHour','String');
				var StartMin = Get_Selection_Value('StartMin','String');
				var StartSec = Get_Selection_Value('StartSec','String');
				var EndHour = Get_Selection_Value('EndHour','String');
				var EndMin = Get_Selection_Value('EndMin','String');
				var EndSec = Get_Selection_Value('EndSec','String');
				var SlotStart = StartHour+":"+StartMin+":"+StartSec;
				var SlotEnd = EndHour+":"+EndMin+":"+EndSec;
				var GroupID = $('input#GroupID').val();
				var PostVar = {
					"SlotName": encodeURIComponent($('input#SlotName').val()),
					"SlotID": $('input#SlotID').val(),
					"GroupID": GroupID,
					"SlotStart": encodeURIComponent(SlotStart),
					"SlotEnd": encodeURIComponent(SlotEnd),
					"InWavie": document.getElementById('InWavie').checked,
					"OutWavie": document.getElementById('OutWavie').checked,
					"DutyCount": encodeURIComponent($('input#DutyCount').val())
				};
				
				Block_Thickbox();
				$.post('../ajax_save_slot.php',PostVar,
					function (data) {
						if (data == "die") 
							window.top.location = '/';
						else {
							Get_Return_Message(data);
							Get_Group_List();
							
							if (SaveSlotGoNextStep)
								window.location = '<?=$PATH_WRT_ROOT?>home/eAdmin/StaffMgmt/attendance/Settings/time_slot/status/duty_period_index.php?ID='+GroupID+'&GroupOrIndividual=Group&Msg='+encodeURIComponent(data);
							else
								window.top.tb_remove();
							Scroll_To_Top();
						}
					});
			}
			
			SlotJustCheck = 1;
		}
	});
}
	
function Check_Slot_Name() {
	SlotName = $('input#SlotName').val();
	ElementObj = $('div#SlotNameWarningLayer');
	
	if (DOM_Check_Slot_Name(SlotName,'SlotNameWarningLayer','SlotNameWarningRow')) {
		var PostVar = {
			"GroupID":$('input#GroupID').val(),
			"SlotID":$('input#SlotID').val(),
			"SlotName":encodeURIComponent(SlotName)
		}
		
		$.post('../ajax_check_slot_name.php',PostVar,
			function (data) {
				if (data == "die") 
					window.top.location = '/';
				else if (data == "1") {
					ElementObj.html('');
					document.getElementById('SlotNameWarningRow').style.display = 'none';
				}
				else {
					ElementObj.html('<?=$Lang['StaffAttendance']['SlotNameWarning']?>');
					document.getElementById('SlotNameWarningRow').style.display = '';
				}
			});
	}
}
	
function Get_Slot_Form(SlotID,GroupID) {
	var SlotID = SlotID || "";
	var GroupID = GroupID || "";
	var PostVar = {
		"SlotID": SlotID,
		"GroupID": GroupID
	};
	
	$.post('../ajax_get_slot_form.php',PostVar,
		function(data) {
			if (data == "die") 
				window.top.location = '/';
			else {
				$('div#TB_ajaxContent').html(data);
				$('div#TB_ajaxWindowTitle').html('<?=$Lang['StaffAttendance']['ManageTimeSlot']?>');
				UnBlock_Thickbox();
			}
		});
}
	
function Get_Group_List() {
	var PostVar = {
			Keyword: encodeURIComponent($('Input#Keyword').val())
			}

	Block_Element("GroupListLayer");
	$.post('ajax_get_group_list.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#GroupListLayer').html(data);
							$('div#SearchInputLayer').show();
							Thick_Box_Init();
							UnBlock_Element("GroupListLayer");
						}
					});
}

function Get_Group_Detail(TargetGroupID) {
	var PostVar = {
			GroupID: TargetGroupID
			}

	Block_Element("GroupListLayer");
	$.post('ajax_get_group_detail.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#GroupListLayer').html(data);
							$('div#SearchInputLayer').hide();
							Init_JEdit_Input('span.jEditInput');
							Thick_Box_Init();
							UnBlock_Element("GroupListLayer");
						}
					});
}

function delete_Group(TargetGroupID) {
	var PostVar = {
			GroupID: TargetGroupID
			}

	Block_Element("GroupListLayer");
	$.post('ajax_delete_group.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#GroupListLayer').html(data);
							UnBlock_Element("GroupListLayer");
						}
					});
}

function Check_Group_Name(GroupName,GroupID) {
	var GroupID = GroupID || "";
	var PostVar = {
			"GroupID": GroupID,
			"GroupName": encodeURIComponent(GroupName)
			};
	var ElementObj = $('div#GroupNameWarningLayer');
	
	if (Trim(GroupName) != "") {
		$.post('ajax_check_group_name.php',PostVar,
				function(data){
					if (data == "die") 
						window.top.location = '/';
					else if (data == "1") {
						ElementObj.html('');
						ElementObj.hide();
						if (document.getElementById('GroupNameWarningRow')) 
							document.getElementById('GroupNameWarningRow').style.display = 'none';
					}
					else {
						ElementObj.html('<?=$Lang['StaffAttendance']['GroupTitleDuplicateWarning']?>');
						ElementObj.show('fast');
						if (document.getElementById('GroupNameWarningRow')) 
							document.getElementById('GroupNameWarningRow').style.display = '';
					}
				});
	}
	else {
		CheckRoleNameAjax = "";
		
		ElementObj.html('<?=$Lang['SysMgr']['RoleManagement']['RoleNameDuplicateWarning']?>');
		ElementObj.show('fast');
		
		if (document.getElementById('GroupNameWarningRow')) 
			document.getElementById('GroupNameWarningRow').style.display = '';
	}
}
}

// jEditable function 
{
function Init_JEdit_Input(objDom) {
	var WarningLayer = "#GroupNameWarningLayer";
	$(objDom).editable( 
    function(value, settings) {
    	var ElementObj = $(this);
    	if ($(WarningLayer).html() == "" && ElementObj[0].revert != value) {
    		var PostVar = {
    			"GroupID":ElementObj.attr("id"),
    			"GroupName":encodeURIComponent(value)
    		};
	    				     
	    	$.post('ajax_rename_group.php',PostVar,
	    		function (data) {
	    			Get_Return_Message(Trim(data));
	    			ElementObj.html(value);
						$('span#GroupNameNavLayer').html(value);
	    		});
			}
			else {
				ElementObj[0].reset();
			}
		}, {
    tooltip   : "<?=$Lang['SysMgr']['FormClassMapping']['ClickToEdit']?>",
    event : "click",
    onblur : "submit",
    type : "text",     
    style  : "display: inline",
    height: "20px",
    maxlength: 100,
    onreset: function() {
    	$(WarningLayer).html('');
    	$(WarningLayer).hide();
    }
  }); 
  
  $(objDom).keyup(function() {
		var GroupName = $('form input').val();
		var GroupID = $(this).attr('id');
		
		Check_Group_Name(GroupName,GroupID);
	});
}
}

// thick box function 
{
//on page load call tb_init
function Thick_Box_Init(){   
	tb_init('a.thickbox');//pass where to apply thickbox
}
}

// UI function for add/ remove member in Group
{
function Remove_Selected() {
	var AvalObj = document.getElementById('AvalObj');
	var ObjSelected = document.getElementById('AddUserID[]');
	
	for (var i = (ObjSelected.length -1); i >= 0 ; i--) {
		if (ObjSelected.options[i].selected) {
			Obj = ObjSelected.options[i];
			AvalObj.options[AvalObj.length] = new Option(Obj.text,Obj.value);
			ObjSelected.options[i] = null;
		}
	}
	
	Reorder_Selection_List('AvalObj');
}

function Remove_All() {
	var AvalObj = document.getElementById('AvalObj');
	var ObjSelected = document.getElementById('AddUserID[]');
	
	for (var i = (ObjSelected.length -1); i >= 0 ; i--) {
		Obj = ObjSelected.options[i];
		AvalObj.options[AvalObj.length] = new Option(Obj.text,Obj.value);
		ObjSelected.options[i] = null;
	}
	
	Reorder_Selection_List('AvalObj');
}

function Add_Selected() {
	var AvalObj = document.getElementById('AvalObj');
	var ObjSelected = document.getElementById('AddUserID[]');
	
	for (var i = (AvalObj.length -1); i >= 0 ; i--) {
		if (AvalObj.options[i].selected) {
			Obj = AvalObj.options[i];
			ObjSelected.options[ObjSelected.length] = new Option(Obj.text,Obj.value);
			AvalObj.options[i] = null;	
		}
	}
	
	Reorder_Selection_List('AddUserID[]');
}

function Add_All() {
	var AvalObj = document.getElementById('AvalObj');
	var ObjSelected = document.getElementById('AddUserID[]');
	
	for (var i = (AvalObj.length -1); i >= 0 ; i--) {
		Obj = AvalObj.options[i];
		ObjSelected.options[ObjSelected.length] = new Option(Obj.text,Obj.value);
		AvalObj.options[i] = null;
	}
	
	Reorder_Selection_List('AddUserID[]');
}

function Reorder_Selection_List(selectId) {
	var selectList = document.getElementById(selectId);
	for (var i = 0; i < selectList.length; i++) {
		for (var j=i; j < selectList.length; j++) {
			if (selectList.options[i].text.toLowerCase() > selectList.options[j].text.toLowerCase()) {
				var tempOption1 = new Option(selectList.options[i].text,selectList.options[i].value);
				var tempOption2 = new Option(selectList.options[j].text,selectList.options[j].value);
				selectList.options[i] = tempOption2;
				selectList.options[j] = tempOption1;
			}
		}
		//alert(selectList.options[i].text);
		/*if (opt.selected) {
			selectList.removeChild(opt);
			selectList.insertBefore(opt, selectOptions[i - 1]);
		}*/
  }
}
}

<?
if ($ScrollTo != '') {
?>
$(document).ready(function() {
	Scroll_To_Element('<?=$ScrollTo?>');
});
<?
}
?>
</script>