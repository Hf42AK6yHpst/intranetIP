<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$StaffAttend3 = new libstaffattend3();

$TargetUserID = $_REQUEST['TargetUserID'];
$GroupID = $_REQUEST['GroupID'];
$TemplateID = $_REQUEST['TemplateID'];
$SlotID = $_REQUEST['SlotID'];
$SlotName = trim(urldecode(stripslashes($_REQUEST['SlotName'])));
$SlotStart = trim(urldecode(stripslashes($_REQUEST['SlotStart'])));
$SlotEnd = trim(urldecode(stripslashes($_REQUEST['SlotEnd'])));
$InWavie = ($_REQUEST['InWavie'] == "true")? "1":"0";
$OutWavie = ($_REQUEST['OutWavie'] == "true")? "1":"0";
$DutyCount = trim(urldecode(stripslashes($_REQUEST['DutyCount'])));

if ($StaffAttend3->Save_Slot($GroupID,$TargetUserID,$SlotID,$SlotName,$SlotStart,$SlotEnd,$InWavie,$OutWavie,$DutyCount,$TemplateID)) {
	$SlotID = ($SlotID != "")? $SlotID:$StaffAttend3->db_insert_id();
	echo $Lang['StaffAttendance']['SaveSlotSuccess'].'|=|'.$SlotID;
}
else {
	echo $Lang['StaffAttendance']['SaveSlotFail'];
}

intranet_closedb();
?>