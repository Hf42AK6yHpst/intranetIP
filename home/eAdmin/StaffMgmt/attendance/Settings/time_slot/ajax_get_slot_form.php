<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$StaffAttend3UI = new libstaffattend3_ui();

$SlotID = $_REQUEST['SlotID'];
$TargetUserID = $_REQUEST['TargetUserID'];
$GroupID = $_REQUEST['GroupID'];
$TemplateID = $_REQUEST['TemplateID'];
$NoNextStepBtn = ($_REQUEST['NoNextStepBtn'] == "1");

// for edit past duty form uses
$SlotName = trim(urldecode(stripslashes($_REQUEST['SlotName'])));
$SlotStart = $_REQUEST['SlotStart'];
$SlotEnd = $_REQUEST['SlotEnd'];
$DutyCount = $_REQUEST['DutyCount'];
$InWavie = $_REQUEST['InWavie'];
$OutWavie = $_REQUEST['OutWavie'];
$HiddenSlotID = trim(urldecode(stripslashes($_REQUEST['HiddenSlotID'])));

echo $StaffAttend3UI->Get_Slot_Form($SlotID,$TargetUserID,$SlotName,$SlotStart,$SlotEnd,$DutyCount,$InWavie,$OutWavie,$HiddenSlotID,$GroupID,$TemplateID,$NoNextStepBtn);

intranet_closedb();
?>