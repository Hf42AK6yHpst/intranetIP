<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-BASIC'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

$CardID = trim($_REQUEST['CardID']);
$TargetUserID = $_REQUEST['TargetUserID'];
$IsRFID = $_REQUEST['IsRFID'];

if ($StaffAttend3->Check_Card_ID($CardID,$TargetUserID,$IsRFID)) {
	echo '1'; // ok
}
else {
	echo '0'; // not ok
}

intranet_closedb();
?>