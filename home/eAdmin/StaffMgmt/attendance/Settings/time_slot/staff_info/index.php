<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-BASIC'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['DutyBasic'] = 1;
$TAGS_OBJ[] = array($Lang['StaffAttendance']['SetupStatus'], $PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Settings/time_slot/status/", 0);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['StaffInfo'], $PATH_WRT_ROOT."home/eAdmin/StaffMgmt/attendance/Settings/time_slot/staff_info/", 1);
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
//$TAGS_OBJ_RIGHT[] = array(gen_online_help_btn_and_layer('staff_attendance','staffinfo_tab'));
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $StaffAttend3UI->Get_User_Info_Index();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script>
// dom function 
{
function Check_Go_Search(evt) {
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		Get_User_List();
	else
		return false;
}

function Show_Edit_Icon(LayerObj) {
	LayerObj.style.backgroundImage = "url(<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/icon_edit_b.gif)";
	LayerObj.style.backgroundPosition = "center right";
	LayerObj.style.backgroundRepeat = "no-repeat";
}

function Hide_Edit_Icon(LayerObj) {
	LayerObj.style.backgroundImage = "";
	LayerObj.style.backgroundPosition = "";
	LayerObj.style.backgroundRepeat = '';
}
}

// ajax function 
{
function Get_User_List() {
	var PostVar = {
			Keyword: encodeURIComponent($('Input#Keyword').val())
			}

	Block_Element("UserListLayer");
	$.post('ajax_get_user_list.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#UserListLayer').html(data);
							Init_JEdit_Input('div.jEditInput');
							Init_JEdit_Input('div.jEditInputRFID');
							UnBlock_Element("UserListLayer");
						}
					});
}

function Check_Card_ID(CardID,UserID,JQueryObj,IsRFID,Callback) {
	var PostVar = {
			"CardID": CardID,
			"IsRFID": IsRFID,
			"TargetUserID": UserID
			}
	
	$.post('ajax_check_card_id.php',PostVar,
		function(data){
			if (data == 'die') 
				window.top.location = '/';
			else if (data == '1') { // ok
				JQueryObj.next().html('');
				JQueryObj.next().hide();
			}
			else { // not ok
				JQueryObj.next().html('<?=$Lang['StaffAttendance']['SmartcardWarning']?>');
				JQueryObj.next().show();
			}
			if(Callback && typeof(Callback)=='function'){
				Callback(data);
			}
		});
}
}

// jEditable function 
{
function Init_JEdit_Input(objDom) {
	var IsRFID;
	if (objDom == "div.jEditInputRFID") {
		IsRFID = "1";
	}
	else {
		IsRFID = "0";
	}
	$(objDom).editable( 
    function(value, settings) {
    		var ElementObj = $(this);
    		//if (ElementObj.next().html() == "" && ElementObj[0].revert != value) {
    			
    			var CardID = value;
				var UserID = $(this).attr('id');
				
				var PostVar = {
	    			"CardID":value,
	    			"IsRFID":IsRFID,
	    			"TargetUserID":$(this).attr('id')
	    		};
				
				Check_Card_ID(CardID,UserID,ElementObj,IsRFID, function(returnData){
					if(returnData == '1'){
						$.post('ajax_save_card_id.php',PostVar,
							function(data){
								if (data == "die") 
									window.top.location = '/';
								else {
									Get_Return_Message(data);
									//ElementObj.html(value);
								}
							});
					}else{
						ElementObj[0].reset();
					}
				});
			//}
			//else {
			//	ElementObj[0].reset();
			//}
			
			return value;
		}, {
    tooltip   : "<?=$Lang['SysMgr']['FormClassMapping']['ClickToEdit']?>",
    event : "click",
    onblur : "submit",
    type : "text",     
    style  : "display: inline",
    height: "20px",
    maxlength: 100,
    onreset: function() {
    	$(this).parent().next().html('');
    	$(this).parent().next().hide();
    }
  }); 
  /*
  $(objDom).keyup(function() {
		var CardID = $('form input').val();
		var UserID = $(this).attr('id');
		
		Check_Card_ID(CardID,UserID,$(this),IsRFID);
	});
  */
}
}

Init_JEdit_Input('div.jEditInput');
Init_JEdit_Input('div.jEditInputRFID');
</script>