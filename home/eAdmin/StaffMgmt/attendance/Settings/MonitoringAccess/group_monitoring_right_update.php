<?php
// editing by 
/*
 * 	Log
 * 	2016-10-28 [Cameron]
 * 		- create this file
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_staff_monitoring_right.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();

if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('SETTINGS-ACCESSRIGHT-Access'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$staffmonitoringright = new staff_monitoring_right();

$GroupTitle = trim(urldecode(stripslashes($_REQUEST['GroupTitle'])));
$GroupID = $_REQUEST['GroupID'];
$GroupDescription = trim(urldecode(stripslashes($_REQUEST['GroupDescription'])));

if(trim($GroupTitle) == "")
{
	header("Location: index.php");
	intranet_closedb();
	exit();
}

$staffmonitoringright->Start_Trans();
$GroupID = $staffmonitoringright->Insert_Group_Monitoring_Right($GroupID, $GroupTitle, $GroupDescription);
if($GroupID != false) {
	$staffmonitoringright->Commit_Trans();
	$Msg = $Lang['StaffAttendance']['SettingApplySuccess'];
}
else {
	$staffmonitoringright->RollBack_Trans();
	$Msg = $Lang['StaffAttendance']['SettingApplyFail'];
}

header("Location: group_monitoring_right_detail.php?Msg=".urlencode($Msg)."&GroupID=".$GroupID);
intranet_closedb();
?>