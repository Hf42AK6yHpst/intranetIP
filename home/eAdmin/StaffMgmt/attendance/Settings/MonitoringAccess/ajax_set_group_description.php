<?php
//editing by 
/*
 * 	Log
 * 	2016-10-28 [Cameron]
 * 		- create this file
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

$StaffAttend3 = new libstaffattend3();

if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('SETTINGS-ACCESSRIGHT-Access'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

intranet_auth();
intranet_opendb();

$staffmonitoringright = new staff_monitoring_right();

$GroupDescription= trim(urldecode(stripslashes($_REQUEST['GroupDescription'])));
$GroupID = $_REQUEST['GroupID'];

if ($staffmonitoringright->Set_Group_Description($GroupDescription,$GroupID)) {
	echo $Lang['StaffAttendance']['ChangeGroupDescriptionSuccess'];
}
else {
	echo $Lang['StaffAttendance']['ChangeGroupDescriptionFail'];
}

intranet_closedb();
?>