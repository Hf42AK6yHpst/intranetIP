<?php
// Editing by 
/*
 * 2016-12-14 (Carlos): $sys_custom['StaffAttendance']['WongFutNamCollege'] - added [Attend One Time Slot].
 * 2016-03-15 (Carlos): Improved and added full day on leave and full day outing. 
 * 2015-08-04 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-SPECIAL'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$format = trim($_REQUEST['format']);
$GroupOrIndividual = $format == 1? "Group" : "Individual";
$is_group = $format == 1? true : false;

$sql = "SELECT * FROM TEMP_CARD_STAFF_IMPORT_SPECIAL_DUTY ORDER BY RecordID";
$records = $StaffAttend3->returnResultSet($sql);
$record_count = count($records);

$today = date("Y-m-d");


/*
 * Parameters of Save_Past_Duty()
 * $ID : GroupID or StaffID
 * $TargetDate : Duty date YYYY-MM-DD
 * $TimeSlotName : array of time slot names
 * $TimeSlotUser[TimeSlotName] = StaffID array
 * $HolidayUser[TimeSlotName] = StaffID array
 * $OutgoingUser[TimeSlotName] = StaffID array
 * $OutgoingReason[TimeSlotName][StaffID] = ReasonID
 * $HolidayReason[TimeSlotName][StaffID] = ReasonID
 * $FullHolidayList : array of StaffID
 * $FullOutgoingList : array of StaffID
 * $FullHolidayReason[StaffID] = ReasonID
 * $FullOutgoingReason[StaffID] = ReasonID
 * $SlotName[TimeSlotName] = Time Slot Name
 * $SlotStart[TimeSlotName] = Duty Start Time
 * $SlotEnd[TimeSlotName] = Duty End Time
 * $SlotInWavie[TimeSlotName] = Slot Waive In
 * $SlotOutWavie[TimeSlotName] = Slot Waive Out
 * $SlotDutyCount[TimeSlotName] = Slot Duty Count
 * $GroupOrIndividual : "Group" or "Individual"
 */

/*
 * Parameters of Save_Shifting_Duty()
 * $ID : GroupID or StaffID
 * $TargetDate : Duty date YYYY-MM-DD
 * $TimeSlotID : no use
 * $TimeSlotUser[TimeSlotID] = StaffID array
 * $HolidayUser[TimeSlotID] = StaffID array
 * $OutgoingUser[TimeSlotID] = StaffID array
 * $OutgoingReason[TimeSlotID][StaffID] = ReasonID
 * $HolidayReason[TimeSlotID][StaffID] = ReasonID
 * $GroupOrIndividual : "Group" or "Individual"
 * $SetNonSchoolDay
 */

/*
 * Parameters of Save_Full_Day_Setting()
 * $StaffID : single StaffID
 * $RecordType : 4 is On leave, 5 is Outgoing
 * $StartDate : for full day leave setting
 * $EndDate : for full day leave setting
 * $ReasonID : single reason id
 * $LeaveNature : 'Full' for full day, otherwise for time slot
 * $Outgoing : SlotID array of outgoing for time slot
 * $Holiday : SlotID array of on leave for time slot
 * $OutgoingReason[SlotID] = ReasonID for time slot
 * $HolidayReason[SlotID] = ReasonID for time slot
 * $TargetDate : for time slot
 */

// Group data by date and GroupID/StaffID
$dateIdToData = array(); // $dateIdToData[Date][GroupID or StaffID] = data array
$idSlotNameToSlotID = array(); // $idSlotNameToSlotID[GroupID or StaffID][SlotName] = SlotID;
$fulldayData = array(); 

$no_of_records = array();
for($i=0;$i<$record_count;$i++)
{
	$id = $is_group? $records[$i]['GroupID'] : $records[$i]['UserID'];
	$userid_ary = explode(",",$records[$i]['UserID']);
	$date = $records[$i]['RecordDate'];
	$time_slot = $records[$i]['TimeSlot'];
	$duty_start = $records[$i]['DutyStart'];
	$duty_end = $records[$i]['DutyEnd'];
	$duty_count = $records[$i]['DutyCount'];
	$waive_in = $records[$i]['WaiveIn'];
	$waive_out = $records[$i]['WaiveOut'];
	$duty_type = $records[$i]['DutyType'];
	$reason_id = $records[$i]['ReasonID'];
	if($sys_custom['StaffAttendance']['WongFutNamCollege']){
		$attend_one = $records[$i]['AttendOne'];
	}
	$no_of_records[$date][$id]++;
	if(!isset($idSlotNameToSlotID[$id])){
		$idSlotNameToSlotID[$id] = array();
		$t_slots = $StaffAttend3->Get_Slot_List($id,$GroupOrIndividual);
		for($j=0;$j<count($t_slots);$j++){
			$idSlotNameToSlotID[$id][$t_slots[$j]['SlotName']] = $t_slots[$j]['SlotID'];
		}
	}
	
	if(!isset($dateIdToData[$date])){
		$dateIdToData[$date] = array();
	}
	if(!isset($dateIdToData[$date][$id])){
		$dateIdToData[$date][$id] = array();
		if($date <= $today)
		{
			$dateIdToData[$date][$id]['TimeSlotName'] = array();
			$dateIdToData[$date][$id]['TimeSlotUser'] = array();
			$dateIdToData[$date][$id]['HolidayUser'] = array();
			$dateIdToData[$date][$id]['OutgoingUser'] = array();
			$dateIdToData[$date][$id]['OutgoingReason'] = array();
			$dateIdToData[$date][$id]['HolidayReason'] = array();
			$dateIdToData[$date][$id]['FullHolidayList'] = array();
			$dateIdToData[$date][$id]['FullOutgoingList'] = array();
			$dateIdToData[$date][$id]['FullHolidayReason'] = array();
			$dateIdToData[$date][$id]['FullOutgoingReason'] = array();
			$dateIdToData[$date][$id]['SlotName'] = array();
			$dateIdToData[$date][$id]['SlotStart'] = array();
		 	$dateIdToData[$date][$id]['SlotEnd'] = array();
		 	$dateIdToData[$date][$id]['SlotInWavie'] = array();
		 	$dateIdToData[$date][$id]['SlotOutWavie'] = array();
		 	$dateIdToData[$date][$id]['SlotDutyCount'] = array();
		 	$dateIdToData[$date][$id]['NonSchoolDay'] = 0;
		 	if($sys_custom['StaffAttendance']['WongFutNamCollege']){
		 		$dateIdToData[$date][$id]['AttendOne'] = $attend_one;
		 	}
		}else{
			$dateIdToData[$date][$id]['TimeSlotUser'] = array();
			$dateIdToData[$date][$id]['HolidayUser'] = array();
			$dateIdToData[$date][$id]['OutgoingUser'] = array();
			$dateIdToData[$date][$id]['OutgoingReason'] = array();
			$dateIdToData[$date][$id]['HolidayReason'] = array();
			$dateIdToData[$date][$id]['NonSchoolDay'] = 0;
			if($sys_custom['StaffAttendance']['WongFutNamCollege']){
				$dateIdToData[$date][$id]['AttendOne'] = $attend_one;
			}
		}
	}
	
	$is_no_duty = $duty_type == "N";
	$is_fullday_leave = $duty_type == "FL" || $duty_type == "FO";
	
	if($date <= $today) // past records
	{	
		if(!$is_no_duty && !$is_fullday_leave && !in_array($time_slot, $dateIdToData[$date][$id]['TimeSlotName'])){
			$dateIdToData[$date][$id]['TimeSlotName'][] = $time_slot;
		}
		
		if($duty_type == 'N'){
			$dateIdToData[$date][$id]['NonSchoolDay'] = 1;
		}else if($duty_type == 'D'){
			if(isset($dateIdToData[$date][$id]['TimeSlotUser'][$time_slot])){
				$dateIdToData[$date][$id]['TimeSlotUser'][$time_slot] = array_merge($dateIdToData[$date][$id]['TimeSlotUser'][$time_slot],$userid_ary);
			}else{
				$dateIdToData[$date][$id]['TimeSlotUser'][$time_slot] = $userid_ary;
			}
		}else if($duty_type == 'L'){
			if(!isset($dateIdToData[$date][$id]['TimeSlotUser'][$time_slot])){
				$dateIdToData[$date][$id]['TimeSlotUser'][$time_slot] = array();
			}
			
			if(isset($dateIdToData[$date][$id]['HolidayUser'][$time_slot])){
				$dateIdToData[$date][$id]['HolidayUser'][$time_slot] = array_merge($dateIdToData[$date][$id]['HolidayUser'][$time_slot], $userid_ary);
			}else{
				$dateIdToData[$date][$id]['HolidayUser'][$time_slot] = $userid_ary;
			}
			
			if(!isset($dateIdToData[$date][$id]['HolidayReason'][$time_slot])){
				$dateIdToData[$date][$id]['HolidayReason'][$time_slot] = array();
			}
			for($j=0;$j<count($userid_ary);$j++){
				$dateIdToData[$date][$id]['HolidayReason'][$time_slot][$userid_ary[$j]] = $reason_id;
			}
		}else if($duty_type == 'O'){
			if(!isset($dateIdToData[$date][$id]['TimeSlotUser'][$time_slot])){
				$dateIdToData[$date][$id]['TimeSlotUser'][$time_slot] = array();
			}
			
			if(isset($dateIdToData[$date][$id]['OutgoingUser'][$time_slot])){
				$dateIdToData[$date][$id]['OutgoingUser'][$time_slot] = array_merge($dateIdToData[$date][$id]['OutgoingUser'][$time_slot], $userid_ary);
			}else{
				$dateIdToData[$date][$id]['OutgoingUser'][$time_slot] = $userid_ary;
			}
			
			if(!isset($dateIdToData[$date][$id]['OutgoingReason'][$time_slot])){
				$dateIdToData[$date][$id]['OutgoingReason'][$time_slot] = array();
			}
			for($j=0;$j<count($userid_ary);$j++){
				$dateIdToData[$date][$id]['OutgoingReason'][$time_slot][$userid_ary[$j]] = $reason_id;
			}
		}else if($duty_type == 'FL'){
			for($j=0;$j<count($userid_ary);$j++){
				$dateIdToData[$date][$id]['FullHolidayList'][] = $userid_ary[$j];
				$dateIdToData[$date][$id]['FullHolidayReason'][$userid_ary[$j]] = $reason_id;
			}
		}else if($duty_type == 'FO'){
			for($j=0;$j<count($userid_ary);$j++){
				$dateIdToData[$date][$id]['FullOutgoingList'][] = $userid_ary[$j];
				$dateIdToData[$date][$id]['FullOutgoingReason'][$userid_ary[$j]] = $reason_id;
			}
		}
		
		if(!$is_no_duty && !$is_fullday_leave){
			$dateIdToData[$date][$id]['SlotName'][$time_slot] = $time_slot;
			$dateIdToData[$date][$id]['SlotStart'][$time_slot] = $duty_start;
			$dateIdToData[$date][$id]['SlotEnd'][$time_slot] = $duty_end;
			$dateIdToData[$date][$id]['SlotInWavie'][$time_slot] = $waive_in == 'Y'? '1':'0';
			$dateIdToData[$date][$id]['SlotOutWavie'][$time_slot] = $waive_out == 'Y'? '1':'0';
			$dateIdToData[$date][$id]['SlotDutyCount'][$time_slot] = $duty_count;
		}
		
	}else{ // future records
		
		if(!$is_no_duty && !$is_fullday_leave && !isset($idSlotNameToSlotID[$id][$time_slot])){
			$new_slot_success = $StaffAttend3->Save_Slot($is_group? $id : 0, $is_group? 0 : $id, "", $time_slot,$duty_start,$duty_end, $waive_in == 'Y'? '1':'0',$waive_out == 'Y'? '1':'0',$duty_count,0);
			if($new_slot_success){
				// created new time slot, reload the slot list
				$t_slots = $StaffAttend3->Get_Slot_List($id,$GroupOrIndividual);
				for($j=0;$j<count($t_slots);$j++){
					$idSlotNameToSlotID[$id][$t_slots[$j]['SlotName']] = $t_slots[$j]['SlotID'];
				}
			}
		}
		
		if($is_no_duty)
		{
			$dateIdToData[$date][$id]['NonSchoolDay'] = 1;
		}else if($is_fullday_leave)
		{
			$no_of_records[$date][$id]--;
			for($j=0;$j<count($userid_ary);$j++){
				$fulldayData[] = array('StaffID'=>$userid_ary[$j],'RecordType'=>($duty_type == 'FL'?4:5),'Date'=> $date,'ReasonID'=>$reason_id);
			}
		}
		else{
			$time_slot_id = $idSlotNameToSlotID[$id][$time_slot];
			
			//if($duty_type == 'D'){
				if(isset($dateIdToData[$date][$id]['TimeSlotUser'][$time_slot_id])){
					$dateIdToData[$date][$id]['TimeSlotUser'][$time_slot_id] = array_merge($dateIdToData[$date][$id]['TimeSlotUser'][$time_slot_id], $userid_ary);
				}else{
					$dateIdToData[$date][$id]['TimeSlotUser'][$time_slot_id] = $userid_ary;
				}
			//}
			if($duty_type == 'L'){
				if(isset($dateIdToData[$date][$id]['HolidayUser'][$time_slot_id])){
					$dateIdToData[$date][$id]['HolidayUser'][$time_slot_id] = array_merge($dateIdToData[$date][$id]['HolidayUser'][$time_slot_id], $userid_ary);
				}else{
					$dateIdToData[$date][$id]['HolidayUser'][$time_slot_id] = $userid_ary;
				}
				
				if(!isset($dateIdToData[$date][$id]['HolidayReason'][$time_slot_id])){
					$dateIdToData[$date][$id]['HolidayReason'][$time_slot_id] = array();
				}
				
				for($j=0;$j<count($userid_ary);$j++){
					$dateIdToData[$date][$id]['HolidayReason'][$time_slot_id][$userid_ary[$j]] = $reason_id;
				}
			}else if($duty_type == 'O'){
				if(isset($dateIdToData[$date][$id]['OutgoingUser'][$time_slot_id])){
					$dateIdToData[$date][$id]['OutgoingUser'][$time_slot_id] = array_merge($dateIdToData[$date][$id]['OutgoingUser'][$time_slot_id], $userid_ary);
				}else{
					$dateIdToData[$date][$id]['OutgoingUser'][$time_slot_id] = $userid_ary;
				}
				
				if(!isset($dateIdToData[$date][$id]['OutgoingReason'][$time_slot_id])){
					$dateIdToData[$date][$id]['OutgoingReason'][$time_slot_id] = array();
				}
				
				for($j=0;$j<count($userid_ary);$j++){
					$dateIdToData[$date][$id]['OutgoingReason'][$time_slot_id][$userid_ary[$j]] = $reason_id;
				}
			}
		}
	}
}

$successAry = array();
$successCount = 0;
$failCount = 0;

if(count($dateIdToData)>0)
{
	foreach($dateIdToData as $date => $ary){
		if(count($ary)>0){
			foreach($ary as $id => $data){
				if($date <= $today)
				{
					if($sys_custom['StaffAttendance']['WongFutNamCollege']){
						$result = $StaffAttend3->Save_Past_Duty($id,$date,$dateIdToData[$date][$id]['TimeSlotName'],$dateIdToData[$date][$id]['TimeSlotUser'],$dateIdToData[$date][$id]['HolidayUser'],$dateIdToData[$date][$id]['OutgoingUser'],$dateIdToData[$date][$id]['OutgoingReason'],$dateIdToData[$date][$id]['HolidayReason'],$dateIdToData[$date][$id]['FullHolidayList'],$dateIdToData[$date][$id]['FullOutgoingList'],$dateIdToData[$date][$id]['FullHolidayReason'],$dateIdToData[$date][$id]['FullOutgoingReason'],$dateIdToData[$date][$id]['SlotName'],$dateIdToData[$date][$id]['SlotStart'],$dateIdToData[$date][$id]['SlotEnd'],$dateIdToData[$date][$id]['SlotInWavie'],$dateIdToData[$date][$id]['SlotOutWavie'],$dateIdToData[$date][$id]['SlotDutyCount'],$GroupOrIndividual,$dateIdToData[$date][$id]['AttendOne']);
					}else{
						$result = $StaffAttend3->Save_Past_Duty($id,$date,$dateIdToData[$date][$id]['TimeSlotName'],$dateIdToData[$date][$id]['TimeSlotUser'],$dateIdToData[$date][$id]['HolidayUser'],$dateIdToData[$date][$id]['OutgoingUser'],$dateIdToData[$date][$id]['OutgoingReason'],$dateIdToData[$date][$id]['HolidayReason'],$dateIdToData[$date][$id]['FullHolidayList'],$dateIdToData[$date][$id]['FullOutgoingList'],$dateIdToData[$date][$id]['FullHolidayReason'],$dateIdToData[$date][$id]['FullOutgoingReason'],$dateIdToData[$date][$id]['SlotName'],$dateIdToData[$date][$id]['SlotStart'],$dateIdToData[$date][$id]['SlotEnd'],$dateIdToData[$date][$id]['SlotInWavie'],$dateIdToData[$date][$id]['SlotOutWavie'],$dateIdToData[$date][$id]['SlotDutyCount'],$GroupOrIndividual);
					}
				}else{
					if($sys_custom['StaffAttendance']['WongFutNamCollege']){
						$result = $StaffAttend3->Save_Shifting_Duty($id,array($date),$dateIdToData[$date][$id]['TimeSlotID'],$dateIdToData[$date][$id]['TimeSlotUser'],$dateIdToData[$date][$id]['HolidayUser'],$dateIdToData[$date][$id]['OutgoingUser'],$dateIdToData[$date][$id]['OutgoingReason'],$dateIdToData[$date][$id]['HolidayReason'],$GroupOrIndividual,$dateIdToData[$date][$id]['NonSchoolDay'],$dateIdToData[$date][$id]['AttendOne']);
					}else{
						$result = $StaffAttend3->Save_Shifting_Duty($id,array($date),$dateIdToData[$date][$id]['TimeSlotID'],$dateIdToData[$date][$id]['TimeSlotUser'],$dateIdToData[$date][$id]['HolidayUser'],$dateIdToData[$date][$id]['OutgoingUser'],$dateIdToData[$date][$id]['OutgoingReason'],$dateIdToData[$date][$id]['HolidayReason'],$GroupOrIndividual,$dateIdToData[$date][$id]['NonSchoolDay']);
					}
				}
				$successAry[] = $result;
				if($result) {
					$successCount += $no_of_records[$date][$id];
				} else {
					$failCount += $no_of_records[$date][$id];
				}
			}
		}
	}

	for($i=0;$i<count($fulldayData);$i++){
		$result = $StaffAttend3->Save_Full_Day_Setting($fulldayData[$i]['StaffID'],$fulldayData[$i]['RecordType'],$fulldayData[$i]['Date'],$fulldayData[$i]['Date'],$fulldayData[$i]['ReasonID'],'Full',array(),array(),array(),array(),$fulldayData[$i]['Date']);
		$successAry[] = $result;
		if($result)
			$successCount+=1;
		else 
			$failCount+=1;
	}
}


$PAGE_NAVIGATION[] = array($Lang['StaffAttendance']['SpecialRoster'],$PATH_WRT_ROOT.'home/eAdmin/StaffMgmt/attendance/Management/roster/group/normal/');
$PAGE_NAVIGATION[] = array($Lang['StaffAttendance']['ImportSpecialDuty'],'');

# step information
$STEPS_OBJ[] = array($Lang['StaffAttendance']['SelectCSVFile'], 0);
$STEPS_OBJ[] = array($Lang['StaffAttendance']['CSVConfirmation'], 0);
$STEPS_OBJ[] = array($Lang['StaffAttendance']['ImportResult'], 1);

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['DutySpecial'] = 1;
$TAGS_OBJ[] = array($Lang['StaffAttendance']['SpecialRoster'], $PATH_WRT_ROOT.'home/eAdmin/StaffMgmt/attendance/Management/roster/group/normal/', 1);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['WholeSchoolSpecialDaySetting'], $PATH_WRT_ROOT.'home/eAdmin/StaffMgmt/attendance/Management/roster/special_duty/', 0);
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

?>
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?=$StaffAttend3UI->GET_NAVIGATION($PAGE_NAVIGATION)?></td>
	</tr>
	<tr>
		<td><?=$StaffAttend3UI->GET_STEPS($STEPS_OBJ)?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td align="center">
			<?=!in_array(false,$successAry)?($successCount.' '.$Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully']):$failCount.' '.substr($Lang['StaffAttendance']['DataImportFail'],4)?>
		</td>
	</tr>
	<tr>
		<td class="dotline">
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td align="center">
			<?=$StaffAttend3UI->GET_ACTION_BTN($Lang['Btn']['Back'],"button","window.location='index.php';")?>
		</td>
	</tr>
</table>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>