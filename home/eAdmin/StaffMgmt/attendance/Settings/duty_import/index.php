<?php
// Editing by 
/*
 * 2015-08-04 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-SPECIAL'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['DutySpecial'] = 1;
$TAGS_OBJ[] = array($Lang['StaffAttendance']['SpecialRoster'], $PATH_WRT_ROOT.'home/eAdmin/StaffMgmt/attendance/Management/roster/group/normal/', 1);
$TAGS_OBJ[] = array($Lang['StaffAttendance']['WholeSchoolSpecialDaySetting'], $PATH_WRT_ROOT.'home/eAdmin/StaffMgmt/attendance/Management/roster/special_duty/', 0);
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $StaffAttend3UI->Get_Special_Duty_Import_Page();

?>
<script type="text/javascript" language="JavaScript">
var last_task = '';
function displayInfoLayer(btnElement,layerId,task)
{
	var btnObj = $(btnElement);
	var layerObj = $('#'+layerId);
	var posX = btnObj.offset().left;
	var posY = btnObj.offset().top + btnObj.height();
	
	layerObj.hide('fast');
	if(layerObj.is(':visible') && task == last_task){
		last_task = task;
		return;
	}
	last_task = task;
	$.get(
		'get_ajax_data.php?task='+task,
		{
			
		},
		function(data){
			$('#'+layerId+' .content').html(data);
			layerObj.css({'position':'absolute','top':posY+'px','left':posX+'px'});
			layerObj.show('fast');
		}
	);
}
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>