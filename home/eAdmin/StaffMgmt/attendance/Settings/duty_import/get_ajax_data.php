<?php
// Editing by 
/*
 * 2015-08-04 (Carlos): Created. 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
//include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-SPECIAL'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	//header ("Location: /");
	intranet_closedb();
	exit();
}

$task = strtolower($_REQUEST['task']);

$x = '';
if($task == 'get_group_list')
{
	$groups = $StaffAttend3->Get_Group_List();
	$group_count = count($groups);
	$x .= '<table class="common_table_list_v30">';
		$x .= '<tr class="tabletop"><th>'.$Lang['StaffAttendance']['Group'].' ID</th><th>'.$Lang['StaffAttendance']['GroupName'].'</th></tr>';
		if($group_count == 0){
			$x .= '<tr><td colspan="2" align="center">'.$i_no_record_exists_msg.'</td></tr>';
		}
		for($i=0;$i<$group_count;$i++){
			$group_id = $groups[$i]['GroupID'];
			$group_name = $groups[$i]['GroupName'];
			$x .= '<tr><td>'.$group_id.'</td><td>'.htmlspecialchars($group_name).'</td></tr>';
		}
	$x .= '</table>';
	
}else if($task == 'get_group_staff_list')
{
	$groups = $StaffAttend3->Get_Group_List();
	$group_count = count($groups);
	
	$x .= '<table class="common_table_list_v30">';
		$x .= '<tr class="tabletop"><th>'.$Lang['StaffAttendance']['GroupName'].'</th><th>'.$Lang['StaffAttendance']['StaffName'].'</th><th>'.$Lang['StaffAttendance']['UserLogin'].'</th></tr>';
		if($group_count == 0){
			$x .= '<tr><td colspan="3" align="center">'.$i_no_record_exists_msg.'</td></tr>';
		}
		for($i=0;$i<$group_count;$i++){
			$group_id = $groups[$i]['GroupID'];
			$group_name = $groups[$i]['GroupName'];
			$members = $StaffAttend3->Get_Group_Member_List($group_id);
			$member_count = count($members);
			for($j=0;$j<$member_count;$j++){
				$staff_name = $members[$j]['StaffName'];
				$user_login = $members[$j]['UserLogin'];
				if($j==0){
					$x .= '<tr><td rowspan="'.$member_count.'">'.$group_name.'</td><td>'.htmlspecialchars($staff_name).'</td><td>'.$user_login.'</td></tr>';
				}else{
					$x .= '<tr><td>'.htmlspecialchars($staff_name).'</td><td>'.$user_login.'</td></tr>';
				}
			}
		}
	$x .= '</table>';
	
}else if($task == 'get_nongroup_staff_list'){
	$staffs = $StaffAttend3->Get_Aval_User_List();
	$staff_count = count($staffs);
	$x .= '<table class="common_table_list_v30">';
		$x .= '<tr class="tabletop"><th class="num_check">#</th><th>'.$Lang['StaffAttendance']['StaffName'].'</th><th>'.$Lang['StaffAttendance']['UserLogin'].'</th></tr>';
		if($staff_count == 0){
			$x .= '<tr><td colspan="3" align="center">'.$i_no_record_exists_msg.'</td></tr>';
		}
		for($i=0;$i<$staff_count;$i++){
			$staff_name = $staffs[$i]['StaffName'];
			$user_login = $staffs[$i]['UserLogin'];
			$x .= '<tr><td>'.($i+1).'</td><td>'.htmlspecialchars($staff_name).'</td><td>'.$user_login.'</td></tr>';
		}
	$x .= '</table>';
}else if(strpos($task,'get_reason_list')>=0)
{
	$reasons = $StaffAttend3->Get_Preset_Leave_Reason_List();
	$reason_count = count($reasons);
	$reasonsMap = array();
	$reasonTypes = array(/*(CARD_STATUS_ABSENT=>'Absent',CARD_STATUS_LATE=>'Late',CARD_STATUS_EARLYLEAVE=>'Early Leave',*/ CARD_STATUS_HOLIDAY=>'Holiday',CARD_STATUS_OUTGOING=>'Outing');
	$reasonCats = array_keys($reasonTypes);
	
	for($i=0;$i<$reason_count;$i++){
		$reason_id = $reasons[$i]['ReasonID'];
		$reason_type = $reasons[$i]['ReasonType'];
		$reason_text = $reasons[$i]['ReasonText'];
		$reason_desc = $reasons[$i]['ReasonDescription'];
		
		if(!in_array($reason_type,$reasonCats)){
			continue;
		}
		
		if(!isset($reasonMap[$reason_type])){
			$reasonMap[$reason_type] = array();
		}
		$reasonMap[$reason_type][] = $reasons[$i];
	}
	
	$x .= '<table class="common_table_list_v30">';
		$x .= '<tr class="tabletop"><th>'.$Lang['StaffAttendance']['ReasonType'].'</th><th>'.$Lang['StaffAttendance']['Reason'].' ID</th><th>'.$Lang['StaffAttendance']['Reason'].'</th><th>'.$Lang['StaffAttendance']['Description'].'</th></tr>';
		if(count($reasonMap)==0){
			$x .= '<tr><td colspan="4" align="center">'.$i_no_record_exists_msg.'</td></tr>';
		}else{
			foreach($reasonMap as $type => $records)
			{
				$record_count = count($records);
				for($i=0;$i<$record_count;$i++){
					if($i == 0){
						$x .= '<tr><td rowspan="'.$record_count.'">'.$reasonTypes[$records[$i]['ReasonType']].'</td><td>'.$records[$i]['ReasonID'].'</td><td>'.htmlspecialchars($records[$i]['ReasonText']).'</td><td>'.(nl2br(htmlspecialchars($records[$i]['ReasonDescription']))).'</td></tr>';
					}else{
						$x .= '<tr><td>'.$records[$i]['ReasonID'].'</td><td>'.htmlspecialchars($records[$i]['ReasonText']).'</td><td>'.(nl2br(htmlspecialchars($records[$i]['ReasonDescription']))).'</td></tr>';
					}
				}
			}
		}
		
	$x .= '</table>';
}

echo $x;

intranet_closedb();
?>