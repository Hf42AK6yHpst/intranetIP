<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('DUTYSETUP-EMPLOYMENTPERIOD'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['StaffWorkingPeriod'] = 1;
$TAGS_OBJ[] = array($Lang['StaffAttendance']['StaffWorkingPeriod'], "", 0);
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
//$TAGS_OBJ_RIGHT[] = array(gen_online_help_btn_and_layer('staff_attendance','employment'));
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));

echo $StaffAttend3UI->Get_Staff_Working_Period_Index();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script>
// dom function 
{
function Check_Go_Search(evt) {
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		Get_Working_Period_List();
	else
		return false;
}
}

// ajax function
{	
function Delete_Working_Period(WorkingPeriodID) {
	var PostVar = {
		"WorkingPeriodID": WorkingPeriodID
		};
	
	Block_Element("PeriodListLayer");
	$.post('ajax_delete_working_period.php',PostVar,
		function (data) {
			if (data == "die") 
				window.top.location = "/";
			else {
				Get_Return_Message(data);
				Get_Working_Period_List();
			}
		});
}

function Check_Working_Period() {
	var PeriodArray = new Array();
	
	var PeriodStart = Trim($('Input#PeriodStart').val());
	PeriodArray = PeriodStart.split('-');
	var DateStart = new Date();
	DateStart.setFullYear(PeriodArray[0],(PeriodArray[1]-1),PeriodArray[2]);
	
	var PeriodEnd = Trim($('Input#PeriodEnd').val());
	PeriodArray = PeriodEnd.split('-');
	var DateEnd = new Date();
	DateEnd.setFullYear(PeriodArray[0],(PeriodArray[1]-1),PeriodArray[2]);
	
	var WarningRow = document.getElementById('PeriodWarningRow');
	var WarningLayer = $('div#PeriodWarningLayer');
	
	if (PeriodStart == "" && PeriodEnd == "") {
		WarningLayer.html('<?=$Lang['StaffAttendance']['WorkingPeriodNullWarning']?>');
		WarningRow.style.display = "";
	}
	else if (DateStart >= DateEnd) {
		WarningLayer.html('<?=$Lang['StaffAttendance']['WorkingPeriodEndDateLesserThenStartDateWarning']?>');
		WarningRow.style.display = "";
	}
	else {
		var PostVar = {
				StaffID: $('Input#StaffID').val(),
				WorkingPeriodID: $('Input#WorkingPeriodID').val(),
				"PeriodStart": encodeURIComponent(PeriodStart),
				"PeriodEnd": encodeURIComponent(PeriodEnd)
				};
				
		$.post('ajax_check_working_period.php',PostVar,
			function(data){
				if (data == "die") 
					window.top.location = '/';
				else {
					if (data == "1") {
						WarningLayer.html('');
						WarningRow.style.display = "none";
						
						Save_Working_Period();
					}
					else {
						WarningLayer.html('<?=$Lang['StaffAttendance']['WorkingPeriodOverlapWarning']?>');
						WarningRow.style.display = "";
					}
				}
			});
	}
}

function Save_Working_Period() {
	var PostVar = {
			StaffID: $('Input#StaffID').val(),
			WorkingPeriodID: $('Input#WorkingPeriodID').val(),
			PeriodStart: encodeURIComponent($('Input#PeriodStart').val()),
			PeriodEnd: encodeURIComponent($('Input#PeriodEnd').val())
			};
	
	Block_Thickbox();
	$.post('ajax_save_working_period.php',PostVar,
		function(data){
			if (data == "die") 
				window.top.location = '/';
			else {
				Get_Return_Message(data);
				Get_Working_Period_List();
				window.top.tb_remove();
				UnBlock_Thickbox();
			}
		});
}

function Get_Working_Period_List() {
	var PostVar = {
			Keyword: encodeURIComponent($('Input#Keyword').val()),
			StaffStatus: document.getElementById('StaffStatus').options[document.getElementById('StaffStatus').selectedIndex].value
			};

	Block_Element("PeriodListLayer");
	$.post('ajax_get_working_period_list.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#PeriodListLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("PeriodListLayer");
						}
					});
}
	
function Get_Working_Period_Form(StaffID,WorkingPeriodID) {
	var WorkingPeriodID = WorkingPeriodID || "";
	var PostVar = {
		"StaffID": StaffID,
		"WorkingPeriodID":WorkingPeriodID
	};
	
	$.post('ajax_get_working_period_form.php',PostVar,
		function(data) {
			if (data == "die") 
				window.top.location = '/';
			else 
				$('div#TB_ajaxContent').html(data);
		});
}
}

// thick box function 
{
//on page load call tb_init
function Thick_Box_Init(){   
	tb_init('a.thickbox');//pass where to apply thickbox
}
}
</script>