<?php
//editing by Carlos
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('SETTINGS-REASONTYPE-Access'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();

$SlotID = $_REQUEST['ReasonID'];

$StaffAttend3->Start_Trans();
if ($StaffAttend3->Delete_Preset_Leave_Reason($ReasonID)) {
	$StaffAttend3->Commit_Trans();
	echo $Lang['StaffAttendance']['DeleteReasonSuccess'];
}
else {
	$StaffAttend3->RollBack_Trans();
	echo $Lang['StaffAttendance']['DeleteReasonFail'];
}

intranet_closedb();
?>