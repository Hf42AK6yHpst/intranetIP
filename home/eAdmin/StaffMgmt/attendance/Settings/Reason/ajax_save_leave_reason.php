<?php
//editing by 
/*
 * 2015-10-05 (Carlos): Added ReasonColor.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('SETTINGS-REASONTYPE-Access'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();

$ReasonText = trim(urldecode(stripslashes($_REQUEST['ReasonText'])));
$ReasonDescription = trim(urldecode(stripslashes($_REQUEST['ReasonDescription'])));
$TypeID = $_REQUEST['ReasonType'];
$ReasonID = $_REQUEST['ReasonID'];
$Waived = $_REQUEST['Waived'];
$ReportSymbol = trim(urldecode(stripslashes($_REQUEST['ReportSymbol'])));
$ReasonActive = $_REQUEST['ReasonActive'];
$SetAsDefault = $_REQUEST['SetAsDefault'];
$ReasonColor = trim($_REQUEST['ReasonColor']);

if ($StaffAttend3->Save_Preset_Leave_Reason($TypeID,$ReasonID,$ReasonText,$ReasonDescription,$Waived, $ReportSymbol,$ReasonActive,$SetAsDefault,$ReasonColor)) {
	echo $Lang['StaffAttendance']['SaveReasonSuccess'];
}
else {
	echo $Lang['StaffAttendance']['SaveReasonFail'];
}

intranet_closedb();
?>