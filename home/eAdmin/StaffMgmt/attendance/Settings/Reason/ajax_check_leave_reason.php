<?php
// editing by Carlos
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('SETTINGS-REASONTYPE-Access'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	echo 'die';
	intranet_closedb();
	exit();
}

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();

$TypeID = $_REQUEST['TypeID'];
$ReasonID = $_REQUEST['ReasonID'];
$ReasonText = trim(urldecode(stripslashes($_REQUEST['ReasonText'])));
$ReportSymbol = trim(urldecode(stripslashes($_REQUEST['ReportSymbol'])));

if ($StaffAttend3->Check_Preset_Leave_Reason($ReasonText,$TypeID, $ReasonID, $ReportSymbol)) {
	echo "1"; // ok
}
else {
	echo "0"; // not ok
}

intranet_closedb();
?>