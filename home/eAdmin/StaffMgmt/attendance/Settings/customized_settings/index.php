<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3_ui.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$sys_custom['StaffAttendance']['DailyAttendanceRecord'])
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$StaffAttend3UI = new libstaffattend3_ui();

$CurrentPageArr['eAdminStaffAttendance'] = 1;
$CurrentPage['CustomizedSettings'] = 1;
$TAGS_OBJ[] = array($Lang['StaffAttendance']['CustomizedSettings'], "", 0);
$MODULE_OBJ = $StaffAttend3UI->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($_REQUEST['Msg']));
?>
<script type="text/javascript" language="JavaScript">
function checkform(obj){
	if(obj==null) return false;
	
	var email_title = document.getElementById('CustomizedEmailTitle').value;
	if(email_title.indexOf('##DATE##') == -1){
		document.getElementById('CustomizedEmailTitle').focus();
		alert('<?=$Lang['StaffAttendance']['DatePlaceholderWarning']?>');
		return false;
	}
	
	var email_recipients = document.getElementById('CustomizedEmailRecipient');
	var email_ary = email_recipients.value.split("\n");
    var re = /^([0-9A-Za-z_\.\-])+\@(([0-9A-Za-z\-])+\.)+([0-9A-Za-z]{2,3})+$/;
    var valid_email_count = 0;
    
	for(var i=0;i<email_ary.length;i++){
		if($.trim(email_ary[i])=="") continue;
        if (!re.test($.trim(email_ary[i]))) {
        	email_recipients.focus();
	        alert('<?=$i_invalid_email?>: '+email_ary[i]);
            return false;
        }
        valid_email_count++;
	}
	if(valid_email_count == 0){
		email_recipients.focus();
		alert('<?=$Lang['StaffAttendance']['RequestInputRecipientEmail']?>');
		return false;
	}
	
	return true;
}
function submitForm(obj){
	if(checkform(obj)){
		return true;
	}else{
		return false;
	}
}
/*
function FCKeditor_OnComplete(EDITOR){
	if(EDITOR.Name == 'CustomizedEmailHeader'){
		EDITOR.SetHTML($('textarea#CustomizedEmailHeaderContent').val());
		$('textarea#CustomizedEmailHeaderContent').remove();
	}else if(EDITOR.Name == 'CustomizedEmailFooter'){
		EDITOR.SetHTML($('textarea#CustomizedEmailFooterContent').val());
		$('textarea#CustomizedEmailFooterContent').remove();
	}
}
*/
</script>
<?php
echo $StaffAttend3UI->Get_Customized_Settings_Index();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>