<?
// editing by 
/*
 * 2016-04-06 (Carlos): Added setting [Do not display time slot] for client program does not display time slot at server return message.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

$StaffAttend3 = new libstaffattend3();
if ((!$StaffAttend3->IS_ADMIN_USER() && 
		!$StaffAttend3->Check_Access_Right('SETTINGS-TERMINAL-Access'))
		|| !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] || !($module_version['StaffAttendance'] == 3.0)) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$GeneralSetting = new libgeneralsettings();
$SettingList['TerminalIP'] = $_REQUEST['IPList'];
$SettingList['IgnorePeriod'] = $_REQUEST['ignore_period'];
$SettingList['AbsMinsForTimeSlot'] = $_REQUEST['AbsMinsForTimeSlot'];
$SettingList['HideAttendStatus'] = $_REQUEST['HideAttendStatus'];
$SettingList['HideTimeSlot'] = $_REQUEST['HideTimeSlot'];

$GeneralSetting->Start_Trans();
if ($GeneralSetting->Save_General_Setting('StaffAttendance',$SettingList)) {
	$GeneralSetting->Commit_Trans();
	$Msg = $Lang['StaffAttendance']['SettingApplySuccess'];
}
else {
	$GeneralSetting->RollBack_Trans();
	$Msg = $Lang['StaffAttendance']['SettingApplyFail'];
}

header("Location: index.php?Msg=".urlencode($Msg));
?>