<?php
# using : 

################## Change Log [Start] ##############
#
#   Date:   2020-10-14  Bill    [2020-1012-1551-04207]
#           Individual Recipients / Groups - Handle recipient name before insert
#
#   Date:   2019-04-30  Bill
#           prevent SQL Injection + Cross-site Scripting
#
#   Date:   2018-12-27 (Anna)
#           added access right for $sys_custom['DHL'] PIC
#
#	Date:	2017-04-20	Carlos
#			$sys_custom['DHL'] - convert individual target type selections to final staff users.
#
#	Date:	2015-12-11	Bill
#			- replace session_unregister() by session_unregister_intranet() for PHP 5.4
#
#	Date:	2015-10-16	Roy
#			set $sendTimeMode and $sendTimeString for scheduled push message and improve send push message logic;
#
#	Date:	2015-01-02 Bill [Case #P70566]
#			- get circularAttFolder from $_POST
#
#	Date:	2014-10-16	Roy
#			Improved: add send push message to teacher logic
#
#	Date:	2013-09-10	YatWoon
#			Improved: update the upload attachment method
#
#	Date:	2013-03-11 YatWoon
#			Improved: Allow display user email as sender ($special_feature['DisplayUserEmailSender']['eCircular']) [Case#2013-0304-1518-38093]
#
#	Date:	2012-06-20 YatWoon
#			Fixed: If edit the reply slip again, the question with "enter" will become "<br>"
#
#	Date:	2011-08-25	Yuen
#			handled description for iPad/Andriod
#
#	Date:	2011-08-03	YatWoon
#			HTML-editor issue (need update display description)
#			combine to 1 page
#
#	Date:	2011-03-28	YatWoon
#			change email notification subject & content data 
#
#	Date:	2011-02-23	YatWoon
#			Add "Display question number"
#
#   Date:	2010-03-24	YatWoon
#		    Add "All questions are required to be answered" option
#
################## Change Log [End] ##############

set_time_limit(3600);
ini_set("memory_limit", "500M");

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");

intranet_auth();
intranet_opendb();

if($sys_custom['DHL'])
{
    include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
    $libdhl = new libdhl();
    $isPIC = $libdhl->isPIC();
}

if (!$special_feature['circular'] || ($_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"] || !($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || $_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"] || ($isPIC && $sys_custom['DHL']))))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])
	{
		header("location: ./settings/basic_settings/");	
		exit;
	}
	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libcircular.php");

// intranet_auth();
// intranet_opendb();

### Handle SQL Injection + XSS [START]
// $NoticeNumber = cleanCrossSiteScriptingCode($NoticeNumber);
// $Title = cleanCrossSiteScriptingCode($Title);
// $TitleEN = cleanCrossSiteScriptingCode($TitleEN);
// $Description = cleanCrossSiteScriptingCode($Description);
// $DescriptionEN = cleanCrossSiteScriptingCode($DescriptionEN);

$AllFieldsReq = IntegerSafe($AllFieldsReq);
$DisplayQuestionNumber = IntegerSafe($DisplayQuestionNumber);

$attachment_size = IntegerSafe($attachment_size);

$type = IntegerSafe($type);
$status = IntegerSafe($status);

// $qStr = cleanCrossSiteScriptingCode($qStr);
### Handle SQL Injection + XSS [END]

$lcircular = new libcircular();
$lf = new libfilesystem();
$libeClassApp = new libeClassApp();

$CircularNumber = intranet_htmlspecialchars(trim($CircularNumber));
$Title = intranet_htmlspecialchars(trim($Title));
$pushMsgCircularNumber = standardizeFormPostValue($_POST['CircularNumber']);
$pushMsgTitle = standardizeFormPostValue($_POST['Title']);

if ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod")
{
	if ($Description==strip_tags($Description))
	{
		$Description = nl2br($Description);
	}
}
$Description = intranet_htmlspecialchars(trim($Description));
// $qStr = intranet_htmlspecialchars(trim($qStr));
$AllFieldsReq = $AllFieldsReq==1 ? 1 : 0;
$DisplayQuestionNumber = $DisplayQuestionNumber==1 ? 1 : 0;

$luser = new libuser($UserID);

$circularAttFolder_name = $_POST['circularAttFolder'];
//$path = "$file_path/file/circular/".$circularAttFolder;
$path = "$file_path/file/circular/".$circularAttFolder_name;

# Delete Files
$file2delete = array_filter(explode(":",$deleted_files));
if (sizeof($file2delete) != 0){
	for ($i=0; $i<sizeof($file2delete); $i++)
	{
		$f = urldecode($file2delete[$i]);
		if(trim($f)=='') continue;
		$del_file = $path."/".$f;
		$lf->lfs_remove($del_file);
	}
}

# Upload file
$attachment_size = $attachment_size==""? 0 : $attachment_size;	
for ($i=0; $i<$attachment_size; $i++)
{
	$key = "filea$i";//debug_pr($key);
	$loc = ${"filea$i"};//debug_pr($loc);
	$file = stripslashes(${"hidden_userfile_name$i"});//debug_pr($file);
	$des = "$path/$file";
	if ($loc == "none" || $loc=="")
	{
	    // do nothing
	} 
	else
	{
		if (strpos($file,"."==0)){
		    // do nothing
		} 
		else{
           $lf->lfs_copy($loc, $des);
		}
	}
}
//$Attachment = $circularAttFolder;
$Attachment = $circularAttFolder_name;

/*
$AttachmentStr = (sizeof($Attachment)==0) ? "" : implode(",", $Attachment);
$AttachmentStr = stripslashes($AttachmentStr);

$path = "$file_path/file/circular/".$circularAttFolder;
$lu = new libfilesystem();

$command ="mv $path"."tmp $path";

exec($command);
$lo = new libfiletable("", $path, 0, 0, "");
$files = $lo->files;
while (list($key, $value) = each($files)) {
     if(!strstr($AttachmentStr,$files[$key][0])){
          $lu->file_remove($path."/".$files[$key][0]);
     }
}
$IsAttachment = (isset($Attachment)) ? 1 : 0;
$Attachment = $circularAttFolder;

if ($IsAttachment)
{
}
else
{
    if ($circularAttFolder != ""){
	    if($bug_tracing['notice_attachment_log']){
             $temp_log_filepath = "$file_path/file/log_notice_attachment.txt";
             $temp_time = date("Y-m-d H:i:s");
             $temp_user = $UserID;
             $temp_page = 'circular_new_update.php';
             $temp_action="remove:".$path;
             $temp_content = get_file_content($temp_log_filepath);
             $temp_logentry = "\"$temp_time\",\"$temp_user\",\"$temp_page\",\"$temp_action\"\n";
             $temp_content .= $temp_logentry;
             write_file_content($temp_content, $temp_log_filepath);
            $temp_cmd = "mv $path ".$path."_bak";
        	exec($temp_cmd);

		 }else{
        	 $lu->lfs_remove($path);
        }
    }
}
*/

if (isset($target) && sizeof($target)!=0) {
    $targetID = implode(",",$target);
}

# Store to DB
$IssueUserName = $luser->getNameForRecord();

$fieldname = "CircularNumber,Title,Description,DateStart,DateEnd,IssueUserID,IssueUserName";
$fieldname .= ",RecipientID,Question,Attachment,RecordType,RecordStatus,DateInput,DateModified, AllFieldsReq, DisplayQuestionNumber";
$values = "'$CircularNumber','$Title','$Description','$DateStart','$DateEnd',$UserID,'$IssueUserName'";
$values .= ",'$targetID','$qStr','$Attachment','$type','$status',now(),now(),'$AllFieldsReq','$DisplayQuestionNumber'";
$sql = "INSERT INTO INTRANET_CIRCULAR ($fieldname) VALUES ($values)";
$lcircular->db_db_query($sql) or die(mysql_error());
$CircularID = $lcircular->db_insert_id();

## Update description if user upload image with Flash upload (fck) [Start]
$Description2 = stripslashes(htmlspecialchars_decode($Description));
$Description_updated = ($lf->copy_fck_flash_image_upload($CircularID, $Description2, $PATH_WRT_ROOT, $cfg['fck_image']['eCircular'])); 
$Description_updated = trim(intranet_htmlspecialchars($Description_updated));
$sql = "UPDATE INTRANET_CIRCULAR SET Description = '".$lcircular->Get_Safe_Sql_Query($Description_updated)."' WHERE CircularID = '$CircularID'";
$lcircular->db_db_query($sql);

## Update description if user upload image with Flash upload (fck) [End]
$username_field = getNameFieldWithClassNumberForRecord("");
if ($status == 1) # Published
{
	if ($type==1)         # All Staff
	{
	    $sql = "INSERT IGNORE INTO INTRANET_CIRCULAR_REPLY (CircularID,UserID,UserName,RecordStatus,DateInput,DateModified)
	            SELECT '$CircularID',UserID,$username_field,0,now(),now() FROM INTRANET_USER WHERE RecordType IN (".$lcircular->recipient_id_type.") AND RecordStatus = 1";
	    $lcircular->db_db_query($sql);
	}
	else if ($type==2)    # Teaching Staff
	{
	    $sql = "INSERT IGNORE INTO INTRANET_CIRCULAR_REPLY (CircularID,UserID,UserName,RecordStatus,DateInput,DateModified)
	            SELECT '$CircularID',UserID,$username_field,0,now(),now() FROM INTRANET_USER WHERE RecordType IN (".$lcircular->recipient_id_type.") AND RecordStatus = 1 AND Teaching = 1";
	    $lcircular->db_db_query($sql);
	}
	else if ($type==3)    # Non-teaching staff
	{
	    $sql = "INSERT IGNORE INTO INTRANET_CIRCULAR_REPLY (CircularID,UserID,UserName,RecordStatus,DateInput,DateModified)
	            SELECT '$CircularID',UserID,$username_field,0,now(),now() FROM INTRANET_USER WHERE RecordType  IN (".$lcircular->recipient_id_type.") AND RecordStatus = 1 AND (Teaching != 1 OR Teaching IS NULL)";
	    $lcircular->db_db_query($sql);
	}
	else if ($type == 4)  # Individual
	{
	    $actual_target_users = $lcircular->returnTargetUserIDArray($targetID);
	    if($sys_custom['DHL']) {
			include_once($intranet_root."/includes/DHL/libdhl.php");
			$libdhl = new libdhl();
			$actual_target_users = $libdhl->convertTargetSelectionToUserAry($target);	
		}
	    if (sizeof($actual_target_users)!=0)
	    {
	    	$delimiter = "";
	        $values = "";
	        for ($i=0; $i<sizeof($actual_target_users); $i++)
	        {
	            list($uid,$name,$usertype) = $actual_target_users[$i];

	            // [2020-1012-1551-04207] Handle recipient name before insert
	            //$values .= "$delimiter ('$CircularID','$uid','$name',0,now(),now())";
                $values .= "$delimiter ('$CircularID','$uid','".$lcircular->Get_Safe_Sql_Query(trim($name))."',0,now(),now())";
	            $delimiter = ",";
	        }
	        $sql = "INSERT IGNORE INTO INTRANET_CIRCULAR_REPLY (CircularID,UserID,UserName,RecordStatus,DateInput,DateModified)
	                 VALUES $values";
	        $lcircular->db_db_query($sql);
	     }
	}
	
	# Count Total
	$total = $lcircular->countTotalNumber($CircularID);
	$sql = "UPDATE INTRANET_CIRCULAR SET TotalCount = '$total' WHERE CircularID = '$CircularID'";
	$lcircular->db_db_query($sql);
	
	### Send notification
	if ($emailnotify==1 || $pushmessagenotify==1)
	{	
		$sql = "SELECT DISTINCT UserID FROM INTRANET_CIRCULAR_REPLY WHERE CircularID = '$CircularID'";
	    $ToArray = $lcircular->returnVector($sql);
	}
    
	// if(!empty($ToArray)) {
	if ($emailnotify==1) {
	// 	include_once($PATH_WRT_ROOT."lang/email.php");
		include_once($PATH_WRT_ROOT."includes/libwebmail.php");
		$lwebmail = new libwebmail();
		
	    $DisplayUserEmailSender = 1;
	    if($special_feature['DisplayUserEmailSender']['eCircular'])
	    {
			$sender_mail = $lwebmail->GetUserEmailAddress($UserID, 1);
	    	$DisplayUserEmailSender = 0;
	    }
	    list($email_subject, $email_body) = $lcircular->returnEmailNotificationData($DateStart, $DateEnd, $pushMsgTitle, $sender_mail);
	    $lwebmail->sendModuleMail($ToArray,$email_subject,$email_body, $DisplayUserEmailSender);
	}
    
	# send message to teacher app
	if ($pushmessagenotify==1) {
		$isPublic = "N";
	//			$sendTimeMode = "";
		$sendTimeMode = standardizeFormPostValue($_POST['sendTimeMode']);
	//			$sendTimeString = "";
		$sendTimeString = standardizeFormPostValue($_POST['sendTimeString']);
		$appType = $eclassAppConfig['appType']['Teacher'];
	    list($pushmessage_subject, $pushmessage_body) = $lcircular->returnPushMessageNotificationData($DateStart, $DateEnd, $pushMsgTitle, $pushMsgCircularNumber);
	    
		$individualMessageInfoAry = array();
		foreach ($ToArray as $teacherId) {
			$_targetTeacherId = $libeClassApp->getDemoSiteUserId($teacherId);
			// link the message to be related to oneself
			$parentStudentAssoAry[$teacherId] = array($_targetTeacherId);
		}
		$individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $parentStudentAssoAry;
		$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', 'eCircular', $CircularID);
	}
	else {
		// cancel scheduled push message
		$libeClassApp->overrideExistingScheduledPushMessageFromModule('eCircular', $CircularID, $newNotifyMessageId='');
	}
}

// for PHP 5.4, replace session_unregister()
//session_unregister("circularAttFolder");
session_unregister_intranet("circularAttFolder");

intranet_closedb();
header("Location: index.php?status=$status&xmsg=AddSuccess&CircularID=$CircularID");
?>