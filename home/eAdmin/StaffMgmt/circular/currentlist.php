<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcircular.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

if($sys_custom['DHL']){
    include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
    $libdhl = new libdhl();
    $isPIC = $libdhl->isPIC();
}


if (!$special_feature['circular'] || ($_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"] || !($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || $_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"] ||  ($isPIC && $sys_custom['DHL']))))
{
    if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])
    {
        header("location: ./settings/basic_settings/");
        exit;
    }
    
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}



$lcircular = new libcircular();
$circulars = $lcircular->returnAllCircularWithStatus();

$MODULE_OBJ['title'] = $i_Circular_CurrentList. " (". $i_Circular_ListIncludingSuspend.")";
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$display .= "<table width='96%' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
$display .= "<tr>";
$display .= "<td class='tabletop tabletopnolink' align='left'>".$i_Circular_CircularNumber."</td>";
$display .= "<td class='tabletop tabletopnolink' align='left'>".$i_Circular_DateStart."</td>";
$display .= "<td class='tabletop tabletopnolink' align='left'>".$i_Circular_DateEnd."</td>";
$display .= "<td class='tabletop tabletopnolink' align='left'>".$i_Circular_Title."</td>";
$display .= "<td class='tabletop tabletopnolink' align='left'>".$i_Circular_Type."</td>";
$display .= "</tr>";

for ($i=0; $i<sizeof($circulars); $i++)
{
     list ($cid, $datestart,$dateend, $number, $title, $recordType, $recordStatus) = $circulars[$i];
     $display .= "<tr class='tablerow". ( $i%2 + 1 )."'>";
     $display .= "<td class='tabletext'>$number</td>";
     $display .= "<td class='tabletext'>$datestart</td>";
     $display .= "<td class='tabletext'>$dateend</td>";
     $display .= "<td class='tabletext'>$title</td>";
     $display .= "<td class='tabletext'>".($recordStatus==1?$i_Notice_StatusPublished:$i_Notice_StatusSuspended)."</td>";
     $display .= "</tr>\n";
}

$display .= "<tr>"; 
$display .= "<td height='1' colspan='5' class='dotline'><img src='$image_path/$LAYOUT_SKIN/10x10.gif' width='10' height='1'></td>";
$display .= "</tr>";
$display .= "<tr>"; 
$display .= "<td colspan='5' align='center'>". $linterface->GET_ACTION_BTN($button_close, "button","self.close();") ."</td>";
$display .= "</tr>";
$display .= "</table>\n";

?>
<br />
<?=$display?>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>