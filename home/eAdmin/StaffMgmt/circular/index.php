<?php
# using: 

############ Change Log Start ###############
#
#   Date:   2020-09-07  Bill    [2020-0604-1821-16170]
#           notice access right checking > replace hasIssueRight by hasSchoolNoticeIssueRight & hasPaymentNoticeIssueRight
#
#   Date:   2020-07-16  Bill     [2020-0714-1331-55313]
#           hide notice tab if only has access right of circular
#
#   Date:   2020-03-13 Tommy
#           need flag - $special_feature['ePaymentNotice'] to show "payment notice" in powerclass
#
#   Date:   2019-01-21 Isaac
#           modified navigation for payment notice to $TAGS_OBJ[] only show PowerClass
#
#   Date:   2018-12-27 (Anna)
#           added access right for $sys_custom['DHL'] PIC
#
#   Date:   2018-12-27 Anna
#           Added navigation for $sys_custom['DHL'] not show payment notice
#
#   Date:   2018-10-08 Isaac
#           Added navigation for payment notice to $TAGS_OBJ[] for PowerClass
#
#	Date:	2017-05-31	Bill	[2017-0529-1652-27206]
#			set cookies after include global.php, PHP 5.4 problem
#
#	DAte:	2011-08-03	YatWoon
#			update "edit logic", according to eNotice, can update past circular
#
#	Date:	2010-11-30	YatWoon
#			IP25 UI standard
#			after create a new circular, back to index page with popup for preview (with flag $sys_custom['PopupCircularAfterCreated'])
#
#	Date:	2010-02-03	YatWoon
#			update "Year" select option, only display the year which includes any circular.
#
############ Change Log End ###############

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

### Set Cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# Preserve Table View
if ($ck_circular_admin_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_circular_admin_page_number", $pageNo, 0, "", "", 0);
	$ck_circular_admin_page_number = $pageNo;
}
else if (!isset($pageNo) && $ck_circular_admin_page_number!="")
{
	$pageNo = $ck_circular_admin_page_number;
}
if ($ck_circular_admin_page_order!=$order && $order!="")
{
	setcookie("ck_circular_admin_page_order", $order, 1, "", "", 1);
	$ck_circular_admin_page_order = $order;
}
else if (!isset($order) && $ck_circular_admin_page_order!="")
{
	$order = $ck_circular_admin_page_order;
}
if ($ck_circular_admin_page_field!=$field && $field!="")
{
	setcookie("ck_circular_admin_page_field", $field, 0, "", "", 0);
	$ck_circular_admin_page_field = $field;
}
else if (!isset($field) && $ck_circular_admin_page_field!="")
{
	$field = $ck_circular_admin_page_field;
}

intranet_auth();
intranet_opendb();

if($sys_custom['DHL']){
    include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
    $libdhl = new libdhl();
    $isPIC = $libdhl->isPIC();
}

if (!$special_feature['circular'] || ($_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"] || !($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || $_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"] || ($isPIC && $sys_custom['DHL']))))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])
	{
		header("location: ./settings/basic_settings/");	
		exit;
	}
	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcircular.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");


$CurrentPageArr['eAdminCircular'] = 1;
$CurrentPage = "PageCircular_Index";
$lcircular = new libcircular();

$adminlevel = $_SESSION["SSV_PRIVILEGE"]["circular"]["AdminLevel"] ? $_SESSION["SSV_PRIVILEGE"]["circular"]["AdminLevel"]: 0;

if($sys_custom['DHL']){
    $adminlevel = ($_SESSION["SSV_PRIVILEGE"]["circular"]["AdminLevel"]||$_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])? 1: 0;
}


if ($status != 2 && $status != 3) $status = 1;

## select Year
$filterbar = "";
$currentyear = date('Y');

$array_year = $lcircular->retrieveCircularYears();
if(empty($array_year))	$array_year[] = $currentyear;
$firstname = $i_status_all . ($intranet_session_language=="en"?" ":"") . $i_general_Years;
$select_year = getSelectByValueDiffName($array_year,$array_year,"name='year' onChange='document.form1.submit();'",$year,1,0, $firstname);

## select Month
for ($i=1; $i<=12; $i++)
{
     $array_month[] = $i;
}
$firstname = $i_status_all . ($intranet_session_language=="en"?" ":"") . $i_general_Months;
$select_month = getSelectByValueDiffName($array_month,$array_month,"name='month' onChange='document.form1.submit();'",$month,1,0,$firstname);

## select status
$array_status_name = array($i_Notice_StatusPublished, $i_Notice_StatusSuspended, $i_Notice_StatusTemplate);
$array_status_data = array("1", "2", "3");
$status_select = getSelectByValueDiffName($array_status_data,$array_status_name,"name='status' onChange='document.form1.submit();'",$status,0,1);

## select current/past
$firstname_past = "-- $i_status_all --";
$array_past_name = array($i_Circular_NotExpired, $i_Circular_Expired);
$array_past_data = array("-1", "1");
$select_past = getSelectByValueDiffName($array_past_data,$array_past_name,"name='past' onChange='document.form1.submit();'",$past,1,0, $firstname_past);

# select sign/unsigned
$firstname_sign = "-- $i_status_all --";
$array_sign_name = array($i_Circular_AllSigned, $i_Circular_NotAllSigned);
$array_sign_data = array("1", "-1");
$select_sign = getSelectByValueDiffName($array_sign_data,$array_sign_name,"name='allsign' onChange='document.form1.submit();'",$allsign,1,0, $firstname_sign);

## select all/my
if($adminlevel || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])
{
	$array_my_name = array($i_Circular_AllCircular, $i_Circular_MyIssueCircular);
	$array_my_data = array("0", "1");
	$select_my = getSelectByValueDiffName($array_my_data,$array_my_name,"name='my' onChange='document.form1.submit();'",$my,0,1);
} 
else
{
	$array_my_name = array($i_Circular_MyIssueCircular);
	$array_my_data = array("1");
	$select_my = getSelectByValueDiffName($array_my_data,$array_my_name,"name='my' onChange='document.form1.submit();'",$my,0,1);
	
	$my = 1;
}

$filterbar .= "$select_my $status_select";

# if status = 2 (suspend) or 3 (template) , no past and count selection
if ($status == 2 || $status == 3)
{
    unset($past);
    unset($allsign);
}
else
{
    $filterbar .= " $select_past $select_sign";
}

$filterbar .= "$select_year $select_month";

$linterface = new interface_html();
############################################################################################################

# TABLE SQL
$keyword = convertKeyword(trim($keyword));


if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if (!isset($field)) $field = 0;
if (!isset($order)) $order = 0;

$li = new libdbtable2007($field, $order, $pageNo);

$conds = "";
if ($year != "")
{
	$conds .= " AND DATE_FORMAT(a.DateStart,'%Y')='$year'";
}
if ($month != "")
{
	$conds .= " AND DATE_FORMAT(a.DateStart,'%c')='$month'";
}
if ($my == 1)
{
	global $UserID;
	$conds .= " AND a.IssueUserID = $UserID";
}
if ($past == 1)
{
	$conds .= " AND a.DateEnd < CURDATE()";
}
else if ($past == -1)
{
	$conds .= " AND a.DateEnd >= CURDATE()";
}

if ($allsign==1)
{
	$conds .= " AND a.SignedCount = a.TotalCount";
}
else if ($allsign == -1)
{
	$conds .= " AND a.SignedCount < a.TotalCount";
}

$iseCircularAdmin = $_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || ($_SESSION["SSV_PRIVILEGE"]["circular"]["AdminLevel"]==1) ? 1 : 0;

$name_field = getNameFieldByLang("b.");
//a.DateStart > CURDATE()
if ($status==1)
{
	$sql =	"SELECT
				DATE_FORMAT(a.DateStart,'%Y-%m-%d'),
       			DATE_FORMAT(a.DateEnd,'%Y-%m-%d'),
       			a.CircularNumber,
				IF (IssueUserID=". $UserID ." or $iseCircularAdmin,
					CONCAT('<a href=javascript:viewCircular(', a.CircularID, ')>', 
	       				'<img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" alt=\"$button_view\" border=0></a>',
						'<a href=\"edit.php?CircularID=', a.CircularID, '\">', 
	       				'<img src=\"$image_path/$LAYOUT_SKIN/icon_edit_b.gif\" alt=\"$button_edit\" border=0></a>'),
	       			CONCAT('<a href=javascript:viewCircular(', a.CircularID, ')>', 
	       				'<img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" alt=\"$button_view\" border=0></a>')
	       		),
       			CONCAT('<a href=javascript:viewCircular(', a.CircularID, ') class=\"tablelink\">', a.Title, '</a>'),
				IF (b.UserID IS NULL,CONCAT('<i>',a.IssueUserName,'</i>'),$name_field),
				CONCAT(a.SignedCount, '/' ,a.TotalCount),

				if(($adminlevel or a.IssueUserID=$UserID),
					CONCAT('<a href=javascript:viewResult(', a.circularID, ')><img src=\"$image_path/$LAYOUT_SKIN/eOffice/icon_resultview.gif\" border=\"0\" width=\"20\" height=\"20\" align=\"absmiddle\"></a>'),
					''),
				CASE (a.RecordType) 
	       			WHEN '' THEN ''
	       			WHEN 1 THEN '$i_Circular_RecipientTypeAllStaff'
	       			WHEN 2 THEN '$i_Circular_RecipientTypeAllTeaching'
	       			WHEN 3 THEN '$i_Circular_RecipientTypeAllNonTeaching'
	       			WHEN 4 THEN '$i_Circular_RecipientTypeIndividual'
	       			ELSE '' 
	       		END,
	       		CONCAT('<input type=checkbox name=CircularID[] value=', a.CircularID ,'>')
				
					
	       	FROM INTRANET_CIRCULAR as a 
	       		LEFT OUTER JOIN INTRANET_USER as b ON a.IssueUserID = b.UserID		
				
			WHERE 
				('%$keyword%' IS NOT NULL) AND (
				((a.CircularNumber like '%$keyword%') OR
				(a.Title like '%$keyword%') OR
				(b.EnglishName like '%$keyword%') OR
				(b.ChineseName like '%$keyword%')) AND 
				(a.RecordStatus = $status $conds))
			";
	$li->field_array = array("a.DateStart", "a.DateEnd", "a.CircularNumber", "a.Title", "a.IssueUserName", "a.SignedCount", "a.CircularID", "a.RecordType");
	$li->sql = $sql;
	$li->no_col = sizeof($li->field_array)+3;
	$li->title = "";
	$li->column_array = array(0,0,0,0,0,1,0,0,0);
	$li->wrap_array = array(0,0,0,0,0,0,0,0,0);
	$li->IsColOff = "IP25_table";
	
	$pos = 0;
	$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";              
	$li->column_list .= "<th width='10%'>".$li->column($pos++, $i_Circular_DateStart)."</th>\n";           
	$li->column_list .= "<th width='10%'>".$li->column($pos++, $i_Circular_DateEnd)."</th>\n";
	$li->column_list .= "<th width='10%'>".$li->column($pos++, $i_Circular_CircularNumber)."</th>\n";
	$li->column_list .= "<th width='8%'>". $Lang['eCircular']['ViewCircular'] ."</th>\n";
	$li->column_list .= "<th width='20%'>".$li->column($pos++, $i_Circular_Title)."</th>\n";
	$li->column_list .= "<th width='15%'>".$li->column($pos++, $i_Circular_Issuer)."</th>\n";
	$li->column_list .= "<th width='13%'>".$li->column($pos++, $i_Circular_Signed."/".$i_Circular_Total)."</th>\n";
	$li->column_list .= "<th width='8%'>".$li->column($pos++, $i_Circular_ViewResult)."</th>\n";
	$li->column_list .= "<th width='10%'>".$li->column($pos++, $i_Circular_RecipientType)."</th>\n";
	$li->column_list .= "<th width='1'>".$li->check("CircularID[]")."</th>\n";
	
	
} else {
	$sql =	"SELECT
				DATE_FORMAT(a.DateStart,'%Y-%m-%d'),
	       		DATE_FORMAT(a.DateEnd,'%Y-%m-%d'),
	       		a.CircularNumber,
				CONCAT('<a href=javascript:viewCircular(', a.CircularID, ')>', 
       				'<img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" alt=\"$button_view\" border=0></a>',
					'<a href=\"edit.php?CircularID=', a.CircularID, '\">', 
       				'<img src=\"$image_path/$LAYOUT_SKIN/icon_edit_b.gif\" alt=\"$button_edit\" border=0></a>'),
       			CONCAT('<a href=javascript:viewCircular(', a.CircularID, ') class=\"tablelink\">', a.Title, '</a>'),
				IF (b.UserID IS NULL,CONCAT('<I>',a.IssueUserName,'</I>'),$name_field),
				CASE (a.RecordType) 
	       			WHEN '' THEN ''
	       			WHEN 1 THEN '$i_Circular_RecipientTypeAllStaff'
	       			WHEN 2 THEN '$i_Circular_RecipientTypeAllTeaching'
	       			WHEN 3 THEN '$i_Circular_RecipientTypeAllNonTeaching'
	       			WHEN 4 THEN '$i_Circular_RecipientTypeIndividual'
	       			ELSE '' 
	       		END,
	       		CONCAT('<input type=checkbox name=CircularID[] value=', a.CircularID ,'>')
	       	FROM INTRANET_CIRCULAR as a 
	       		LEFT OUTER JOIN INTRANET_USER as b ON a.IssueUserID = b.UserID
				
			WHERE 
				('%$keyword%' IS NOT NULL) AND (
				((a.CircularNumber like '%$keyword%') OR
				(a.Title like '%$keyword%') OR
				(b.EnglishName like '%$keyword%') OR
				(b.ChineseName like '%$keyword%')) AND 
				(a.RecordStatus = $status $conds))
			";

	$li->field_array = array("a.DateStart", "a.DateEnd", "a.CircularNumber", "a.Title", "a.IssueUserName", "a.RecordType");
	$li->sql = $sql;
	$li->no_col = sizeof($li->field_array)+3;
	$li->title = "";
	$li->column_array = array(0,0,0,0,1,0,0);
	$li->wrap_array = array(0,0,0,0,0,0,0);
	$li->IsColOff = "IP25_table";
	
	$pos = 0;
	$li->column_list .= "<th width='1' class='num_check'>#</th>\n";              
	$li->column_list .= "<th width='10%'>".$li->column($pos++, $i_Circular_DateStart)."</th>\n";           
	$li->column_list .= "<th width='10%'>".$li->column($pos++, $i_Circular_DateEnd)."</th>\n";
	$li->column_list .= "<th width='15%'>".$li->column($pos++, $i_Circular_CircularNumber)."</th>\n";
	$li->column_list .= "<th width='8%'>".$Lang['eNotice']['ViewNotice']."</th>\n";
	$li->column_list .= "<th width='20%'>".$li->column($pos++, $i_Circular_Title)."</th>\n";
	$li->column_list .= "<th width='20%'>".$li->column($pos++, $i_Circular_Issuer)."</th>\n";
	$li->column_list .= "<th width='15%'>".$li->column($pos++, $i_Circular_RecipientType)."</th>\n";
	$li->column_list .= "<th width='1'>".$li->check("CircularID[]")."</th>\n";
}
$searchTag = "<input name=\"keyword\" id=\"keyword\" type=\"text\" value=\"". stripslashes($keyword) ."\" onkeyup=\"Check_Go_Search(event);\"/>";

# Power Class
if($sys_custom['PowerClass'])
{
    // [2020-0714-1331-55313] display tab if has access right of notice
    if ($plugin['notice'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $_SESSION["SSV_PRIVILEGE"]["notice"]["hasIssueRight"]))
    {
        // [2020-0604-1821-16170]
        if($_SESSION["SSV_PRIVILEGE"]["notice"]["hasSchoolNoticeIssueRight"]) {
            $TAGS_OBJ[] = array($Lang['eNotice']['SchoolNotice'],'../../StudentMgmt/notice/index.php', 0);
            $TAGS_OBJ[] = array($Lang['eNotice']['SchoolStudentNotice'],"../../StudentMgmt/notice/student_notice/index.php", 0);
        }
        if($special_feature['ePaymentNotice'] && $_SESSION["SSV_PRIVILEGE"]["notice"]["hasPaymentNoticeIssueRight"]){
           $TAGS_OBJ[] = array($Lang['eNotice']['PaymentNotice'],'../../StudentMgmt/notice/payment_notice/paymentNotice.php', 0);
        }
    }
}
$TAGS_OBJ[] = array($Lang['Header']['Menu']['eCircular'],"", $sys_custom['PowerClass']);
$MODULE_OBJ = $lcircular->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>

<script language="Javascript">
function viewCircular(id)
{
         newWindow('/home/eService/circular/view.php?CircularID='+id+'&showStar='+0,10);
}
function viewResult(id)
{
         newWindow('/home/eAdmin/StaffMgmt/circular/tableview.php?CircularID='+id,10);
}
function sign(id,targetID)
{
         newWindow('/home/eService/circular/sign.php?CircularID='+id+'&TargetUserID='+targetID,10);
}
function viewReply(id,targetID)
{
         newWindow('/home/eAdmin/StaffMgmt/circular/view.php?CircularID='+id+'&TargetUserID='+targetID,10);
}
function checkRemoveThis(obj,element,page)
{
	if(countChecked(obj,element)==0)
		alert(globalAlertMsg2);
	else{
		if(confirm("<?=$i_Circular_RemovalWarning?>")){	            
			obj.action=page;              
			obj.method="POST";
			obj.submit();				             
		}
	}
}

function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		document.form1.submit();
	else
		return false;
}
</script>

<form name="form1" method="get">


<div class="content_top_tool">
	<div class="Conntent_tool">
		<a href="new.php" class="new"> <?=$Lang['Btn']['New']?></a>
	</div>
	<div class="Conntent_search"><?=$searchTag?></div>
<br style="clear:both" />
</div>

<div class="table_board">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td valign="bottom"><?= $filterbar; ?></td>
	<td valign="bottom"><div class="common_table_tool"><a href="javascript:checkRemoveThis(document.form1,'CircularID[]','remove_update.php')" class="tool_delete"><?=$Lang['Btn']['Delete']?></a></div></td>
</tr>
</table>
</div>


<?php echo $li->display(); ?>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>

<br>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

if($sys_custom['PopupCircularAfterCreated'] && !empty($CircularID))
{
?>
<SCRIPT LANGUAGE=Javascript>
newWindow('/home/eService/circular/view.php?CircularID=<?=$CircularID?>',10);
</script>
<?	
}
?>