<?php
################# Change Log [Start] ############
# 	Date:	2016-09-23	(Villa)
#			-modified return_FormContent()	libcircular()
#			add to module setting - "default using eclass App to notify"
#
################# Change Log [End] ############
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if(!$special_feature['circular'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libcircular.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lcircular = new libcircular();
$lgeneralsettings = new libgeneralsettings();

$data = array();
$data['disabled'] = $disabled;
$data['isHelpSignAllow'] = $isHelpSignAllow;
$data['isLateSignAllow'] = $isLateSignAllow;
$data['isResignAllow'] = $isResignAllow;
$data['defaultNumDays'] = $defaultNumDays;
$data['showAllEnabled'] = $showAllEnabled;
$data['MaxReplySlipOption'] = $MaxReplySlipOption;
$data['sendPushMsg'] = $sendPushMsg;

# store in DB
$lgeneralsettings->Save_General_Setting($lcircular->Module, $data);

# set $this->
$lcircular->disabled = $disabled;			
$lcircular->isHelpSignAllow = $isHelpSignAllow;		
$lcircular->isLateSignAllow = $isLateSignAllow;				
$lcircular->isResignAllow = $isResignAllow;		
$lcircular->defaultNumDays = $defaultNumDays;			
$lcircular->showAllEnabled = $showAllEnabled;	
$lcircular->MaxReplySlipOption = $MaxReplySlipOption;
$lcircular->sendPushMsg = $sendPushMsg;
	


# update session
foreach($data as $name=>$val)
	$_SESSION["SSV_PRIVILEGE"]["circular"][$name] = $val;

intranet_closedb();
header("Location: index.php?xmsg=UpdateSuccess");
?>