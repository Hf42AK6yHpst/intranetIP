<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if(!$special_feature['circular'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libcircular.php");
include_once($PATH_WRT_ROOT."includes/libadminjob.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eAdminCircular'] = 1;
$CurrentPage = "PageCircularSettings_AdminSettings";

$linterface = new interface_html();
$lcircular = new libcircular();
$ladminjob = new libadminjob();

$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit']);

# Left menu 
$TAGS_OBJ[] = array($Lang['Circular']['AdminSettings']);
$MODULE_OBJ = $lcircular->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

list($tempUserID, $AdminLevel, $RecordType) = $ladminjob->RetrieveAdminUser($AdminID[0]);
$lu = new libuser($tempUserID);

if ($AdminLevel == 1)
{
    $strFull = "CHECKED";
    $strNormal = "";
}
else
{
    $strFull = "";
    $strNormal = "CHECKED";
}
?>

<br />   
<form name="form1" method="get" action="edit_update.php" onSubmit="return checkform();">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$Lang['Circular']['StaffName']?></td>
			<td class='tabletext'><?=$lu->UserNameLang()?></td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$Lang['Circular']['AdminLevel']?></td>
			<td class='tabletext'>
			<input type="radio" name="AdminLevel" value="0" id="AdminLevel0" <?=$strNormal?>> <label for="AdminLevel0"><?=$Lang['Circular']['AdminLevel_Normal_Detail']?></label> <br>
			<input type="radio" name="AdminLevel" value="1" id="AdminLevel1" <?=$strFull?>> <label for="AdminLevel1"><?=$Lang['Circular']['AdminLevel_Full_Detail']?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">        
        <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
        <tr>
        	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        </tr>
        <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit") ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back()","cancelbtn") ?>
			</td>
		</tr>
        </table>                                
	</td>
</tr>

</table>                        
<br />

<input type="hidden" name="AdminID" value="<?=$AdminID[0]?>">
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>