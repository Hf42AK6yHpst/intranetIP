<?php
# using: 

#####################################################
#
#	Date:	2017-05-31	Bill	[2017-0529-1652-27206]
#			set cookies after include global.php, PHP 5.4 problem
#
#	Date:	2010-11-29 YatWoon
#			update to IP25 UI standard
#
#####################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

### Set Cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# Preserve Table View
if ($ck_circular_admin_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_circular_admin_page_number", $pageNo, 0, "", "", 0);
	$ck_circular_admin_page_number = $pageNo;
}
else if (!isset($pageNo) && $ck_circular_admin_page_number!="")
{
	$pageNo = $ck_circular_admin_page_number;
}
if ($ck_circular_admin_page_order!=$order && $order!="")
{
	setcookie("ck_circular_admin_page_order", $order, 1, "", "", 1);
	$ck_circular_admin_page_order = $order;
}
else if (!isset($order) && $ck_circular_admin_page_order!="")
{
	$order = $ck_circular_admin_page_order;
}
if ($ck_circular_admin_page_field!=$field && $field!="")
{
	setcookie("ck_circular_admin_page_field", $field, 0, "", "", 0);
	$ck_circular_admin_page_field = $field;
}
else if (!isset($field) && $ck_circular_admin_page_field!="")
{
	$field = $ck_circular_admin_page_field;
}

intranet_auth();
intranet_opendb();

if(!$special_feature['circular'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libcircular.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");


$linterface 	= new interface_html();
$CurrentPage	= "PageAnnualSettings";
$CurrentPageArr['eAdminCircular'] = 1;
$CurrentPage = "PageCircularSettings_AdminSettings";
$lcircular = new libcircular();

if($AdminLevel>-1)
	$cond = " and a.AdminLevel = $AdminLevel";
$name_field = getNameFieldByLang("b.");
$sql = "
	SELECT 
		$name_field as NameField, 
		if(a.AdminLevel=1, '". $Lang['Circular']['AdminLevel_Full'] ."','". $Lang['Circular']['AdminLevel_Normal'] ."') as AdminLevel,
		CONCAT('<input type=\"checkbox\" name=\"AdminID[]\" value=\"', AdminID ,'\">')
	FROM 
		INTRANET_ADMIN_USER as a
		LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
	WHERE 
		a.RecordType = '$lcircular->admin_id_type'
		$cond
";

# TABLE INFO
if($field=="") $field = 0;
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$li = new libdbtable2007($field, 1, $pageNo);
$li->field_array = array("NameField", "AdminLevel");
$li->sql = $sql;
$li->no_col = 3;
$li->IsColOff = "GeneralDisplayWithNavigation";

// TABLE COLUMN
$li->column_list .= "<th width='80%'>". $Lang['Circular']['StaffName'] ."</th>\n";
$li->column_list .= "<th width='20%'>". $Lang['Circular']['AdminLevel'] ."</th>\n";
$li->column_list .= "<th width='5'>".$li->check("AdminID[]")."</th>\n";

### Filter
$AdminList = array();
$AdminList[] = array("-1",$Lang['Btn']['All']);
$AdminList[] = array("0",$Lang['Circular']['AdminLevel_Normal']);
$AdminList[] = array("1",$Lang['Circular']['AdminLevel_Full']);
$AdminLevelSelection = $Lang['Circular']['AdminLevel']." : ".getSelectByArray($AdminList,"name='AdminLevel' onChange='this.form.submit()'",$AdminLevel,0,1);

### Title ###
$TAGS_OBJ[] = array($Lang['Circular']['AdminSettings']);
$MODULE_OBJ = $lcircular->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>

<div class="content_top_tool">
	<div class="Conntent_tool">
		<a href="add.php" class="new"> <?=$Lang['Btn']['Add']?></a>
	</div>
<br style="clear:both" />
</div>

<form name="form1" method="get">

<div class="table_board">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr class="table-action-bar">
<td valign="bottom"><?=$AdminLevelSelection?></td>
<td valign="bottom">
<div class="common_table_tool">
	<a href="javascript:checkEdit(document.form1,'AdminID[]','edit.php')" class="tool_edit"><?=$Lang['Btn']['Edit']?></a>
	<a href="javascript:checkRemove(document.form1,'AdminID[]','remove.php')" class="tool_delete"><?=$Lang['Btn']['Delete']?></a> 
</div>
</td>

</tr>
</table>

<?php
echo $li->display();
?>
		
</div>
				
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

</form>
 
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>