<?php
# using: 

################### Change Log 
#   Date:   2019-05-30 (Anna)
#           added dhl pic access right
#
#	Date:	2017-04-25 (Carlos) 
#			$sys_custom['DHL'] - Display Department, hide Identity type for DHL.
# 
#	Date :	2010-10-29 [YatWoon]
#			add teaching staff / non-teaching staff filter
#
#	Date:	2010-09-10	YatWoon
#			trim line break for question
#
##############################


$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcircular.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();



if($sys_custom['DHL']){
    include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
    $libdhl = new libdhl();
    $isPIC = $libdhl->isPIC();
}

if (!$special_feature['circular'] || ($_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"] || !($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || $_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"] || ($isPIC && $sys_custom['DHL']))))
{
    if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])
    {
        header("location: ./settings/basic_settings/");
        exit;
    }
    
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}


$lexport = new libexporttext();


$lcircular = new libcircular($CircularID);

if ($lcircular->disabled)
{
     header ("Location: $intranet_httppath/close.php");
     intranet_closedb();
     exit();
}



$hasRight = false;
if ($_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"])
{
    $adminlevel = $_SESSION["SSV_PRIVILEGE"]["circular"]["AdminLevel"];  
    if ($adminlevel==1 || $lcircular->IssueUserID == $UserID)     # Allow if Full admin or issuer
    {
        $hasRight = true;
    }
}

$questions = $lcircular->splitQuestion($lcircular->Question);

$data = $lcircular->returnTableViewAll($TeachingType);
$totalCount = sizeof($data);

if($sys_custom['DHL']){
	include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
	$libdhl = new libdhl();
	
	$userIdAry = Get_Array_By_Key($data,0);
	$userRecords = $libdhl->getUserRecords(array('user_id'=>$userIdAry,'GetOrganizationInfo'=>1));
	$userIdToRecordMap = array();
	for($i=0;$i<count($userRecords);$i++){
		$userIdToRecordMap[$userRecords[$i]['UserID']] = $userRecords[$i];
	}
}

if (sizeof($data)!=0)
{
    $content = "\"$i_UserName\", \"$i_identity\"";
    $exportColumn[] = $i_UserName;
    if($sys_custom['DHL']){
    	$exportColumn[] = $Lang['General']['UserLogin'];
    	$exportColumn[] = $Lang['DHL']['Company'];
    	$exportColumn[] = $Lang['DHL']['Division'];
    	$exportColumn[] = $Lang['DHL']['Department'];
    }else{
    	$exportColumn[] = $i_identity;
    }
    for ($i=0; $i<sizeof($questions); $i++)
    {
         if ($questions[$i][0]!=6)
         {
             $content .= ",\"". str_replace("\r\n","",$questions[$i][1]) ."\"";
             $exportColumn[] = str_replace("<br>"," ",$questions[$i][1]) ;
         }
    }
    $content .= ",\"$i_Circular_Signer\",\"$i_Circular_SignedAt\"\n";
    $exportColumn[] = $i_Circular_Signer;
    $exportColumn[] = $i_Circular_SignedAt;
    for ($i=0; $i<sizeof($data); $i++)
    {
         list ($targetID,$name,$type,$status,$answer,$signer,$last, $user_identity) = $data[$i];
         if ($status == 2)
         {
         }
         else
         {
             $last = "$i_Circular_Unsigned";
         }
         $name = str_replace("<I>","",$name);
         $name = str_replace("</I>","",$name);
         $content .= "\"$name\", \"$user_identity\"";
         $row[] = $name;
         if($sys_custom['DHL']){
         	$row[] = $userIdToRecordMap[$targetID]['UserLogin'];
         	$row[] = $userIdToRecordMap[$targetID]['CompanyCode'];
         	$row[] = $userIdToRecordMap[$targetID]['DivisionCode'];
         	$row[] = $userIdToRecordMap[$targetID]['DepartmentCode'];
         }else{
         	$row[] = $user_identity;
         }
         for ($j=0; $j<sizeof($questions); $j++)
         {
              if ($questions[$j][0]!=6) {
              	$content .= ",\"".$answer[$j]."\"";
              	$row[] = $answer[$j];
          	  }
         }
         $content .= ",\"$signer\",\"$last\"\n";
         $row[] = $signer;
         $row[] = $last;
         $rows[] = $row;
         unset($row);
    }
}
else
{
    $content = "$i_Circular_NoTarget";
    $row[] = $i_Circular_NoTarget;
}

// Output the file to user browser
$filename = "circular_result.csv";
/*
header("Content-type: application/octet-stream");
header("Content-Length: ".strlen($content) );
header("Content-Disposition: attachment; filename=\"".$filename."\"");
echo $content;
*/
intranet_closedb();

$export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn);
$lexport->EXPORT_FILE($filename, $export_content);

?>