<?php
# using: 

############### Change Log ######
#   Date:   2018-12-27 (Anna)
#           added access right for $sys_custom['DHL'] PIC
#
#	Date:	2017-12-08 [Carlos] 
#			$sys_custom['DHL'] - Display Department, hide Identity type for DHL.
#
#	Date:	2012-08-01 [YatWoon]
#			display circular content at the top
#
#	Date :	2010-10-29 [YatWoon]
#			add teaching staff / non-teaching staff filter
#
#################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

if($sys_custom['DHL']){
    include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
    $libdhl = new libdhl();
    $isPIC = $libdhl->isPIC();
}


if (!$special_feature['circular'] || ($_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"] || !($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || $_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"] 
    ||  ($isPIC && $sys_custom['DHL']))))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])
	{
		header("location: ./settings/basic_settings/");	
		exit;
	}
	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcircular.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lcircular = new libcircular($CircularID);
if ($lcircular->disabled)
{
     header ("Location: /");
     intranet_closedb();
     exit();
}
$hasRight = false;
if ($_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"])
{
    $adminlevel = $_SESSION["SSV_PRIVILEGE"]["circular"]["AdminLevel"];  # 1 - circular type admin
    if ($adminlevel==1 || $lcircular->IssueUserID == $UserID)     # Allow if Full admin or issuer
    {
        $hasRight = true;
    }
}

$questions = $lcircular->splitQuestion($lcircular->Question);

$signedCount = 0;
$totalCount = 0;
$singleTable = true;

$targetType = array("",$i_Circular_RecipientTypeAllStaff,
                    $i_Circular_RecipientTypeAllTeaching,
                    $i_Circular_RecipientTypeAllNonTeaching,
                    $i_Circular_RecipientTypeIndividual);

$all = 1;
$data = $lcircular->returnTableViewAll($TeachingType);

if($sys_custom['DHL']){
	include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
	$libdhl = new libdhl();
	
	$userIdAry = Get_Array_By_Key($data,0);
	$userRecords = $libdhl->getUserRecords(array('user_id'=>$userIdAry,'GetOrganizationInfo'=>1));
	$userIdToRecordMap = array();
	for($i=0;$i<count($userRecords);$i++){
		$userIdToRecordMap[$userRecords[$i]['UserID']] = $userRecords[$i];
	}
}

$TeachingType_str = $TeachingType==1 ? $Lang['Identity']['TeachingStaff'] : ($TeachingType=="" ? $Lang['StaffAttendance']['AllStaff'] : $Lang['Identity']['NonTeachingStaff']);
$totalCount = sizeof($data);
if (sizeof($data)!=0)
{
       //$toolbar = exportIcon2()."<a href=\"export.php?CircularID=$CircularID\">$button_export</a>";
       $x .= "<table width='90%' border='0' cellspacing='0' cellpadding='5' align='center' class='eSporttableborder'>";
       $x .= "<tr>";
       $x .= "<td valign='middle' class='eSporttdborder eSportprinttabletitle'>$i_UserName</td>";
       if($sys_custom['DHL']){
       		$x .= "<td class='eSporttdborder eSportprinttabletitle'>".$Lang['General']['UserLogin']."</td>";
       		$x .= "<td class='eSporttdborder eSportprinttabletitle'>".$Lang['DHL']['Company']."</td>";
       		$x .= "<td class='eSporttdborder eSportprinttabletitle'>".$Lang['DHL']['Division']."</td>";
       		$x .= "<td class='eSporttdborder eSportprinttabletitle'>".$Lang['DHL']['Department']."</td>";
       }else{
       		$x .= "<td class='eSporttdborder eSportprinttabletitle'>$i_identity</td>";
       }
        for ($i=0; $i<sizeof($questions); $i++)
        {
             if ($questions[$i][0]!=6)
             {
                 $tooltipQ = makeTooltip($questions[$i][1]);
                 $x .= "<td valign='middle' class='eSporttdborder eSportprinttabletitle'>$i_Circular_Option ".($i+1)."</td>";
             }
        }
        $x .= "<td valign='middle' class='eSporttdborder eSportprinttabletitle'>$i_Circular_Signer</td>";
        $x .= "<td valign='middle' class='eSporttdborder eSportprinttabletitle'>$i_Circular_SignedAt</td></tr>\n";
        for ($i=0; $i<sizeof($data); $i++)
        {
             list ($targetID,$name,$type,$status,$answer,$signer,$last, $user_identity) = $data[$i];
             if ($status == 2)
             {
                 $css = ($type==1?"attendancepresent":"attendanceouting");
                 $signedCount++;
             }
             else
             {
                 $css = "attendanceabsent";
                 $last = "$i_Circular_Unsigned";
             }

             $x .= "<tr class=\"$css\"><td class=\"eSporttdborder eSportprinttext\">$name</td>";
             if($sys_custom['DHL']){
			 	$x .= "<td class=\"eSporttdborder eSportprinttext\">".($userIdToRecordMap[$targetID]['UserLogin'])."</td>";
			 	$x .= "<td class=\"eSporttdborder eSportprinttext\">".($userIdToRecordMap[$targetID]['CompanyCode'])."</td>";
			 	$x .= "<td class=\"eSporttdborder eSportprinttext\">".($userIdToRecordMap[$targetID]['DivisionCode'])."</td>";
			 	$x .= "<td class=\"eSporttdborder eSportprinttext\">".($userIdToRecordMap[$targetID]['DepartmentCode'])."</td>";
			 }else{
			 	$x .= "<td class=\"eSporttdborder eSportprinttext\">$user_identity</td>";
             }
             for ($j=0; $j<sizeof($questions); $j++)
             {
                  if ($questions[$j][0]!=6)
                  $x .= "<td  class=\"eSporttdborder eSportprinttext\">".$answer[$j]." &nbsp;</td>";
             }
             $x .= "<td class=\"eSporttdborder eSportprinttext\">$signer&nbsp;</td><td class=\"eSporttdborder eSportprinttext\">$last</td></tr>\n";
        }
        $x .= "</table>\n";
    }
    else
    {
        $x .= "<table width=\"100%\" border=\"0\" cellpadding=\"1\" cellspacing=\"1\">\n";
        $x .= "<tr><td align=\"center\">$i_Circular_NoTarget</td></tr>\n";
        $x .= "</table>\n";
    }

?>
<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='center' class='eSportprinttitle'><b><?=$i_Circular_Circular?><br /><?=$i_Circular_ViewResult?></b></td>
	</tr>
</table>

<table width="90%" border="0" cellpadding="3" cellspacing="0" align="center">
<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Circular_DateStart?></span>
					</td>
					<td class="tabletext"><?=$lcircular->DateStart?></td>
				</tr>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Circular_DateEnd?></span>
					</td>
					<td class="tabletext"><?=$lcircular->DateEnd?></td>
				</tr>
				<tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_Circular_CircularNumber?></span></td>
					<td class='tabletext'><?=$lcircular->CircularNumber?></td>
				</tr>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Circular_Title?></span>
					</td>
					<td class="tabletext"><?=$lcircular->Title?></td>
				</tr>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Circular_Description?></span>
					</td>
					<td class="tabletext"><?=htmlspecialchars_decode($lcircular->Description)?></td>
				</tr>
				<? if ($attachment != "") { ?>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Circular_Attachment?></span>
					</td>
					<td class="tabletext"><?=$attachment?></td>
				</tr>
				<? } ?>
				<?
					$issuer = $lcircular->returnIssuerName();
				?>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Circular_Issuer?></span>
					</td>
					<td class="tabletext"><?=$issuer?></td>
				</tr>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Circular_RecipientType?></span>
					</td>
					<td class="tabletext"><?=$targetType[$lcircular->RecordType]?></td>
				</tr>
				
	<tr valign='top'>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_identity?></td>
		<td class="tabletext"><?=$TeachingType_str?></td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?="$i_Circular_Signed/$i_Circular_Total"?></td>
		<td class="tabletext"><?="$signedCount/$totalCount"?></td>
	</tr>
</table>

<?=$x?>

<?php
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>