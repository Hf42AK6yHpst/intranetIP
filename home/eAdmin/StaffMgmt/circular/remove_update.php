<?php
# using: Bill

################## Change Log [Start] ##############
#
#	Date:   2019-04-30  Bill
#           - prevent SQL Injection + Command Injection
#
####################################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/liblog.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");


include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libcircular.php");

intranet_auth();
intranet_opendb();

if($sys_custom['DHL']){
    include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
    $libdhl = new libdhl();
    $isPIC = $libdhl->isPIC();
}
if (!$special_feature['circular'] || ($_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"] || !($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || $_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"] || ($isPIC && $sys_custom['DHL']))))
{
    if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])
    {
        header("location: ./settings/basic_settings/");
        exit;
    }
    
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$leClassApp = new libeClassApp();

$CircularID = IntegerSafe($CircularID);
for($i=0; $i<sizeof($CircularID); $i++)
{
	$thisCircularID = $CircularID[$i];
	$lcircular = new libcircular($thisCircularID);
	
	$lf = new libfilesystem();
	if ($lcircular->disabled)
	{
	     header ("Location: /");
	     intranet_closedb();
	     exit();
	}
	$CircularAttachment = OsCommandSafe($lcircular->Attachment);
    
	# Check editable
	$hasRight = false;
	if (isset($_SESSION["SSV_PRIVILEGE"]["circular"]["AdminLevel"]) || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])
	{
	    $adminlevel = $_SESSION["SSV_PRIVILEGE"]["circular"]["AdminLevel"];  # 1 - circular type admin
	    if ($adminlevel==1 || $lcircular->IssueUserID == $UserID || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])     # Allow if Full admin or issuer
	    {
	        $hasRight = true;
	    }
	    
	    if($isPIC && $sys_custom['DHL']){
	        $hasRight = true;
	    }
	}
	if (!$hasRight)
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
	
	$sql = "DELETE FROM INTRANET_CIRCULAR_REPLY WHERE CircularID = '$thisCircularID'";
	$lcircular->db_db_query($sql);
	
	# Grab attachment
	// $path = "$file_path/file/circular/".$lcircular->Attachment;
	$path = "$file_path/file/circular/".$CircularAttachment;
	if (trim($lcircular->Attachment) != "" && is_dir($path))
	{
        if($bug_tracing['notice_attachment_log']){
             $temp_log_filepath = "$file_path/file/log_notice_attachment.txt";
             $temp_time = date("Y-m-d H:i:s");
             $temp_user = $UserID;
             $temp_page = 'circular_remove_update.php';
             $temp_action="remove:".$path;
             $temp_content = get_file_content($temp_log_filepath);
             $temp_logentry = "\"$temp_time\",\"$temp_user\",\"$temp_page\",\"$temp_action\"\n";
             $temp_content .= $temp_logentry;
             write_file_content($temp_content, $temp_log_filepath);
       		 $temp_cmd = "mv $path ".$path."_bak";
        	 exec($temp_cmd);
        }
        else{
        	 $lf->lfs_remove($path);
        }
	}
	
	# Add deletion log
	$lg = new liblog();
	$sql = "SELECT * FROM INTRANET_CIRCULAR WHERE CircularID = '$thisCircularID'";
	$result = $lcircular->returnArray($sql);
	foreach($result as $k=>$d)
	{
		$this_id = $thisCircularID;
		
		$RecordDetailAry = array();
		$RecordDetailAry['DateStart'] = substr($d['DateStart'],0,10);
		$RecordDetailAry['DateEnd'] = substr($d['DateEnd'],0,10);
		$RecordDetailAry['IssueUserID'] = $d['IssueUserID'];
		$RecordDetailAry['IssueUserName'] = $d['IssueUserName'];
		$RecordDetailAry['CircularNumber'] = $d['CircularNumber'];
		$RecordDetailAry['Title'] = $d['Title'];
		
		$lg->INSERT_LOG('eCircular', 'eCircular', $RecordDetailAry, 'INTRANET_CIRCULAR', $this_id);
	}
	
	$sql = "DELETE FROM INTRANET_CIRCULAR WHERE CircularID = '$thisCircularID'";
	$lcircular->db_db_query($sql);
	
	# delete unsent scheduled push message
	$leClassApp->overrideExistingScheduledPushMessageFromModule("eCircular", $thisCircularID, $newNotifyMessageId='');
}

intranet_closedb();
header("Location: index.php?status=$status&xmsg=DeleteSuccess");
?>