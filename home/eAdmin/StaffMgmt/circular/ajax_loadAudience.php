<?
# using: 

################################
# 2018-12-18 (Anna) : Added SelectGroupOnly when select group
# 2017-08-22 (Carlos): Added batch search staff users by userlogin function. 
# 2017-08-02 (Carlos): Changed back to use popup window for choosing target for DHL.
# 2017-04-27 (Carlos): $sys_custom['DHL'] Use thickbox for choosing target for DHL.
################################


$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libcircular.php");

$lcircular = new libcircular();

intranet_auth();
intranet_opendb();

$lclass = new libclass();

$nextSelect = "";

if ($type==1)       # All Staff
{
}
else if ($type==2)	# All teaching staff
{
}
else if ($type==3)	# All Non-teaching staff
{
}
else if ($type==4)	## Individual Recipients / Groups 
{
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	
	$linterface 	= new interface_html();
	$lu = new libuser();
			
	$option = "";	

	if($RecipientID)
	{
		$staff_ary = explode(",",$RecipientID);
		$recipient_name_ary = $lcircular->returnIndividualTypeNames2($RecipientID);

		if($sys_custom['DHL']){
		    $staff_ary = explode(",",$RecipientID);
		   
		    foreach($staff_ary as $Recipient_ary){
		        $targetType = substr($Recipient_ary,0,1);	    
		        if($targetType == 'D' ){ // for deparment id format DEPXXXX
		            include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
		            $libdhl = new libdhl();
		            $targetID = substr($Recipient_ary,3);
		           
		            $GroupID = $libdhl->getMappingIntranetGroupID($targetID);
		            $GroupIDAry[] = 'G'.$GroupID[0];       
		        }else if ($targetType == 'G'){ // normal geoup id 
		            $GroupIDAry[] = $Recipient_ary;   
		        }
		        else{
		            $GroupIDAry[] =  'U'.substr($Recipient_ary,1);
		        }
		    }
		    $recipient_name_ary = $lcircular->returnIndividualTypeNames2(implode(",",$GroupIDAry));	   
    	}

// 		for($i=0; $i<sizeof($recipient_name_ary); $i++)
// 		{
// 			$option .= "<option value='". $staff_ary[$i] ."'>". $recipient_name_ary[$i] ."</option>";
// 		}
	}
	else
	{
// 		for($i = 0; $i < 40; $i++) $space .= "&nbsp;";
// 		$option = "<option>$space</option>";
	}
	
	
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || $_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"]){
	    $isAdmin = true;
	}
	
	### Choose Member Btn
	$choose_btn_js = "newWindow('/home/common_choose/index.php?fieldname=target[]&page_title=SelectAudience&permitted_type=1&DisplayGroupCategory=1&SelectGroupOnly=1&DHLSelectGroup=1&isAdmin=$isAdmin',16)";
	//if($sys_custom['DHL']){
	//	$choose_btn_js = "show_dyn_thickbox_ip('','/home/common_choose/index.php?fieldname=target[]&from_thickbox=1&page_title=SelectAudience&permitted_type=1&DisplayGroupCategory=1', '', 0, 640, 640, 1, 'fakeDiv', null);";
	//}
	$btn_ChooseMember = $linterface->GET_SMALL_BTN($Lang['Btn']['Select'], "button", $choose_btn_js);
	
	### Auto-complete ClassName ClassNumber StudentName search
	$UserClassNameClassNumberInput = '';
	$UserClassNameClassNumberInput .= '<div style="float:left;">';
	$UserClassNameClassNumberInput .= '<input type="text" id="UserClassNameClassNumberSearchTb" name="UserClassNameClassNumberSearchTb" value="" />';
	$UserClassNameClassNumberInput .= '</div>';
	
	### Auto-complete login search
	$UserLoginInput = '';
	$UserLoginInput .= '<div style="float:left;">';
	$UserLoginInput .= '<input type="text" id="UserLoginSearchTb" name="UserLoginSearchTb" value="" />';
	$UserLoginInput .= '</div>';
	
	### Student Selection Box & Remove all Btn
	$MemberSelectionBox = $linterface->GET_SELECTION_BOX($recipient_name_ary, "name='target[]' id='target[]' class='select_studentlist' size='15' multiple='multiple'", "");
	$btn_RemoveSelected = $linterface->GET_SMALL_BTN($Lang['Btn']['RemoveSelected'], "button", "js_Remove_Selected_Student();");
	
	$nextSelect .= "<table>";
	$nextSelect .= '<tr>
									<td class="tablerow2" valign="top">
										<table width="100%" border="0" cellpadding="3" cellspacing="0">
										<tr>
											<td class="tabletext">'.$Lang['Circular']['FromGroup'].'</td>
										</tr>
										<tr>
											<td class="tabletext">'.$btn_ChooseMember.'</td>
										</tr>
										<tr>
											<td class="tabletext"><i>'.$Lang['General']['Or'].'</i></td>
										</tr>
										<tr>
											<td class="tabletext">
												'.$Lang['Circular']['SearchStaffName'] .'
												<br />
												'.$UserClassNameClassNumberInput.'
											</td>
										</tr>
										</table>
									</td>
									<td class="tabletext" ><img src="'. $image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>
									<td class="tablerow2" valign="top">
										'.$Lang['AppNotifyMessage']['msgSearchAndInsertPICInfo'].'<br /> 
										<div class="pic_textarea">
										<textarea rows="10" cols="36" id="SearchStaffTextarea" name="SearchStaffTextarea"></textarea><div id="SearchStaffTextarea_Error" class="red"></div><br />
										'.$linterface->GET_SMALL_BTN($Lang['AppNotifyMessage']['searchAndInsertStaff'], "button" , "onSearchAndInsertStaff(this,document.form1['SearchStaffTextarea'],document.form1['target[]']);").'
										</div>
									</td>
									<td class="tabletext" ><img src="'. $image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>
									<td align="left" valign="top">
										<table width="100%" border="0" cellpadding="5" cellspacing="0">
										<tr>
											<td align="left">
												'. $MemberSelectionBox .'
												'.$btn_RemoveSelected.'
											</td>
										</tr>
										<tr>
											<td>
												'.$linterface->Get_Thickbox_Warning_Msg_Div("MemberSelectionWarningDiv", '* '.$Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['SelectionContainsMember']).'
											</td>
										</tr>
										</table>
									</td>
								</tr>';
	$nexSelect .= "</table>";
	
// 	$nextSelect .= "<table border='0' cellspacing='1' cellpadding='1' class='inside_form_table'>";
// 	$nextSelect .= "<tr>";
// 	$nextSelect .= "<td>";
	
// 	$nextSelect .= "<select name='target[]' size='4' multiple>$option</select>";
// 	$nextSelect .= "</td>";
// 	$nextSelect .= "<td>";
// 	$nextSelect .= $linterface->GET_BTN($i_frontpage_campusmail_choose, "button","newWindow('/home/common_choose/index.php?fieldname=target[]&page_title=SelectAudience&permitted_type=1&DisplayGroupCategory=1',16)") . "<br>";
// 	$nextSelect .= $linterface->GET_BTN($i_frontpage_campusmail_remove, "button","checkOptionRemove(document.form1.elements['target[]'])") . "<br>";
// 	$nextSelect .= "</td>";
// 	$nextSelect .= "</tr>";
// 	$nextSelect .= "</table>";
}

intranet_closedb();

echo $nextSelect;
	
?>