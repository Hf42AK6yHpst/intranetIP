<?php
# using: 

################## Change Log [Start] ##############
#
#   Date:   2020-09-07  Bill    [2020-0604-1821-16170]
#           notice access right checking > replace hasIssueRight by hasSchoolNoticeIssueRight
#
#   Date:   2020-07-16  Bill     [2020-0714-1331-55313]
#           hide notice tab if only has access right of circular
#
#   Date:   2019-05-30 (Anna)
#           added UserLogin after username for DHL
#
#   Date:   2018-12-27 (Anna)
#           added access right for $sys_custom['DHL'] PIC
#
#   Date:   2018-12-18 (Anna)
#           hide select AllStaff for cust $sys_custom['DHL'] 
#
#	Date:	2017-08-22	(Carlos) added js onSearchAndInsertStaff() for searching staff users by userlogins.
#	Date:	2017-04-20	(Carlos) $sys_custom['DHL'] - hide teaching and non-teaching staff target selection types for DHL.
#	Date:	2017-04-07	(Carlos) Fixed send push message time checking.
#	Date:	2017-03-30	(Carlos) Fixed js related to attachments checking.
#	Date:	2017-03-13	(Carlos)
#			Fix the js at submit button.
#
#	Date:	2016-10-18	(villa)
#			fix the problem with hiding the pushMsg setting box when clicking Approve and Issue 
# 	Date:	2016-09-23	(Villa)
#			add to module setting - "default using eclass App to notify"
#
#	Date:	2016-09-23	Villa 
#			UI improvement 
#			add block to push msg setting
#	
#	Date:	2015-12-14	Bill
#			- replace session_register() by session_register_intranet() for PHP 5.4
#
#	Date:	2015-10-16	Roy
#			modified reset_innerHtml(), checkform(), added clickedSendTimeRadio(), checkedSendPushMessage() for scheduled push message
#
#	Date:	2015-01-22	Roy
#			fix: check $plugin['eClassTeacherApp'] instead of $plugin['eClassApp'] for push message option
#
#	Date:	2015-01-02 (Bill)
#			Copy from "notice/new.php"
#			- Disable submit button after submit the form [Case #D72427]
#			- Add hidden input field "circularAttFolder" for POST [Case #P70566]
#
#	Date:	2014-10-16	Roy
#			Improved: add notify by push message option
#
#	Date:	2013-09-10	YatWoon
#			Improved: update the upload attachment method
#
#	Date:	2012-09-12 YatWoon
#			Improved: default "All questions are required to be answered" is checked. [Case#2012-0907-1757-42071]
#
#	Date:	2012-05-04 YatWoon
#			Improve: skip the start date checking [Case#2012-0504-1030-05066]
#
#	Date:	2011-08-02	YaTWoon
#			- update to IP25 standard
#			- combine steps to one step
#			- HTML-editor issue (need update display description)
#
#	Date:	2011-02-18	YatWoon
#			Add "Display question number" option
#
# Date:	2010-03-24	YatWoon
#		Add "All questions are required to be answered" option
#
################## Change Log [End] ##############

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

if($sys_custom['DHL']){
    include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
    $libdhl = new libdhl();
    $isPIC = $libdhl->isPIC();
}

if (!$special_feature['circular'] || ($_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"] || !($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || $_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"] || ($isPIC && $sys_custom['DHL']))))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])
	{
		header("location: ./settings/basic_settings/");
		exit;
	}

	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcircular.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

// intranet_auth();
// intranet_opendb();

$linterface = new interface_html();
// $CurrentPage = "PageCircular";
$CurrentPage = "PageCircular_Index";
$CurrentPageArr['eAdminCircular'] = 1;
$lcircular = new libcircular();

$lform = new libform();

# Attachment Folder
# create a new folder to avoid more than 1 circular using the same folder
//session_register("circularAttFolder");
//$circularAttFolder = session_id().".".time();
$circularAttFolder = $lcircular->genAttachmentFolderName();
// for PHP 5.4, replace session_register()
session_register_intranet("circularAttFolder", $circularAttFolder);

$li = new libfilesystem();
$path = "$file_path/file/circular/";
if (!is_dir($path))
{
	if($bug_tracing['notice_attachment_log']){
             $temp_log_filepath = "$file_path/file/log_notice_attachment.txt";
             $temp_time = date("Y-m-d H:i:s");
             $temp_user = $UserID;
             $temp_page = 'circular_new.php';
             $temp_action='create folder:'.$path;
             $temp_content = get_file_content($temp_log_filepath);
             $temp_logentry = "\"$temp_time\",\"$temp_user\",\"$temp_page\",\"$temp_action\"\n";
             $temp_content .= $temp_logentry;
             write_file_content($temp_content, $temp_log_filepath);
	}
    $li->folder_new($path);
}
//$path = "$file_path/file/circular/$circularAttFolder"."tmp";
$path = "$file_path/file/circular/$circularAttFolder";
if (!is_dir($path))
{
	if($bug_tracing['notice_attachment_log']){
             $temp_log_filepath = "$file_path/file/log_notice_attachment.txt";
             $temp_time = date("Y-m-d H:i:s");
             $temp_user = $UserID;
             $temp_page = 'circular_new.php';
             $temp_action='create folder:'.$path;
             $temp_content = get_file_content($temp_log_filepath);
             $temp_logentry = "\"$temp_time\",\"$temp_user\",\"$temp_page\",\"$temp_action\"\n";
             $temp_content .= $temp_logentry;
             write_file_content($temp_content, $temp_log_filepath);
	}

     $li->folder_new($path);
}
/*
$lo = new libfiletable("", $path, 0, 0, "");
$files = $lo->files;
*/
    $circular_templates = $lcircular->returnTemplates();
    $NAoption = array(-1,$i_Circular_NotUseTemplate);
    if (sizeof($circular_templates)!=0)
    {
        array_unshift($circular_templates,$NAoption);
    }
    else
    {
        $circular_templates = array($NAoption);
    }
    $template_selection = getSelectByArray($circular_templates,"name=templateID onChange=\"selectTemplate()\"", "", 0, 1);

### Title ###
$TAGS_OBJ[] = array($Lang['Header']['Menu']['eCircular']);
if($sys_custom['PowerClass'])
{
	$TAGS_OBJ = array();

	// [2020-0604-1821-16170]
    // [2020-0714-1331-55313] display tab if has access right of notice
    //if ($plugin['notice'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $_SESSION["SSV_PRIVILEGE"]["notice"]["hasIssueRight"])) {
    if ($plugin['notice'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $_SESSION["SSV_PRIVILEGE"]["notice"]["hasSchoolNoticeIssueRight"])) {
        $TAGS_OBJ[] = array($Lang['eNotice']['SchoolNotice'], '../../StudentMgmt/notice/new.php', 0);
        $TAGS_OBJ[] = array($Lang['eNotice']['SchoolStudentNotice'], "../../StudentMgmt/notice/student_notice/new.php", 0);
    }
    $TAGS_OBJ[] = array($Lang['Header']['Menu']['eCircular'],"", 1);
}
$MODULE_OBJ = $lcircular->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($i_Circular_New);

$linterface->LAYOUT_START();

$now = time();
$defaultEnd = $now + ($lcircular->defaultNumDays*24*3600);
$start = date('Y-m-d',$now);
$end = date('Y-m-d',$defaultEnd);
        
$type_selection = "<SELECT name='type' onChange='selectAudience()'>";
$type_selection.= "<OPTION value='' > -- $button_select -- </OPTION>";
if(!$sys_custom['DHL'])
{
    $type_selection.= "<OPTION value='1'>$i_Circular_RecipientTypeAllStaff</OPTION>";
}

if(!$sys_custom['DHL'])
{
	$type_selection.= "<OPTION value='2'>$i_Circular_RecipientTypeAllTeaching</OPTION>";
	$type_selection.= "<OPTION value='3'>$i_Circular_RecipientTypeAllNonTeaching</OPTION>";
}
$type_selection.= "<OPTION value='4'>$i_Circular_RecipientTypeIndividual</OPTION>";
$type_selection.= "</SELECT>";

echo $linterface->Include_AutoComplete_JS_CSS();
echo $linterface->Include_JS_CSS();
?>

<script language="javascript">
var no_of_upload_file = <? echo $no_file==""?1:$no_file;?>;

$(document).ready( function() {
	
	if('<?=$plugin['eClassTeacherApp']?>'=='1'){
		if('<?= $lcircular->sendPushMsg?>' =='1'){
			$('div#PushMessageSetting').show();
		}else{
			$('div#PushMessageSetting').hide();
		}
	}


// 		$('div#PushMessageSetting').show();
// 	}else{
// 		$('div#PushMessageSetting').hide();
// 	}
});

function compareDate(s1,s2)
{
        y1 = parseInt(s1.substring(0,4),10);
        y2 = parseInt(s2.substring(0,4),10);
        m1 = parseInt(s1.substring(5,7),10);
        m2 = parseInt(s2.substring(5,7),10);
        d1 = parseInt(s1.substring(8,10),10);
        d2 = parseInt(s2.substring(8,10),10);

        if (y1 > y2)
        {
                return 1;
        }
        else if (y1 < y2)
        {
                return -1;
        }
        else if (m1 > m2)
        {
                return 1;
        }
        else if (m1 < m2)
        {
                return -1;
        }
        else if (d1 > d2)
        {
                return 1;
        }
        else if (d1 < d2)
        {
                return -1;
        }
        return 0;


}
function checkform_old(obj){
     checkOption(obj.elements['Attachment[]']);
     if (compareDate(obj.DateStart.value, obj.DateEnd.value) > 0) {alert ("<?php echo $i_Homework_new_duedate_wrong; ?>"); return false;}
     if (compareDate('<?=date('Y-m-d')?>', obj.DateStart.value) > 0) {alert ("<?php echo $i_Homework_new_startdate_wrong; ?>"); return false;}
     if(!check_text(obj.Title, "<?php echo $i_alert_pleasefillin.$i_Circular_Title; ?>.")) return false;
     if(!check_text(obj.CircularNumber, "<?php echo $i_alert_pleasefillin.$i_Circular_CircularNumber; ?>.")) return false;
     if (!check_date(obj.DateStart,"<?php echo $i_invalid_date; ?>")) return false;
     if (!check_date(obj.DateEnd,"<?php echo $i_invalid_date; ?>")) return false;
     <?
    if ($type==4)
    {
    ?>
     //checkOption(this.form.elements['target[]']);
     if(obj.elements["target[]"].length==0){ alert("<?php echo $i_frontpage_campusmail_choose; ?>"); return false; }
     checkOptionAll(obj.elements["target[]"]);
    <?
    }
    ?>
     checkOptionAll(obj.elements["Attachment[]"]);
}

function js_show_email_notification(val)
{
	if(val == 1) {
		if('<?=$plugin['eClassTeacherApp']?>'=='1'){
			$('#pushmessagenotify').removeAttr('disabled');
		}
		$('#emailnotify').removeAttr('disabled');
	}else{
		if('<?=$plugin['eClassTeacherApp']?>'=='1'){
			$('#pushmessagenotify').removeAttr('checked');
			$('#pushmessagenotify').attr('disabled','true');
			$('div#PushMessageSetting').hide();
		}
		
		$('#emailnotify').removeAttr('checked');
		$('#emailnotify').attr('disabled','true');
		
	}
}

function selectAudience()
{
	var tid = document.form1.type.value;
	document.getElementById('div_audience_err_msg').innerHTML = "";
	
	$('#div_audience').load(
		'ajax_loadAudience.php', 
		{type: tid}, 
		function (data){
			if(tid == 4){
				// initialize jQuery Auto Complete plugin
				Init_JQuery_AutoComplete('UserClassNameClassNumberSearchTb', 'UserLoginSearchTb');
				$('input#UserClassNameClassNumberSearchTb').focus();
			}
		});
}

function selectTemplate()
{
	var t_index = document.form1.templateID.selectedIndex;
	
	if(confirm("<?=$i_Form_chg_template?>"))
	{
		document.form1.CurrentTemplateIndex.value = t_index;
		var tid = document.form1.templateID.value;
		
		$('#div_content').load(
			'ajax_loadContent.php', 
			{templateID: tid, isIssuedCircular: 0, circularAttFolder:'<?=$circularAttFolder?>' }, 
			function (data){
				if('<?=$plugin['eClassTeacherApp']?>'=='1'){
					$('div#PushMessageSetting').hide();
				}
			}); 
	}
	else
	{
		document.form1.templateID.selectedIndex = document.form1.CurrentTemplateIndex.value;
	}
}

function reset_innerHtml()
{
 	document.getElementById('div_audience_err_msg').innerHTML = "";
 	document.getElementById('div_CircularNumber_err_msg').innerHTML = "";
 	document.getElementById('div_Title_err_msg').innerHTML = "";
 	document.getElementById('div_DateStart_err_msg').innerHTML = "";
 	document.getElementById('div_DateEnd_err_msg').innerHTML = "";
 	<?php if($plugin['eClassTeacherApp']) { ?>
 		document.getElementById('div_push_message_date_err_msg').innerHTML = "";
 	<?php } ?>
}

function checkform(obj)
{
	var error_no = 0;
	var focus_field = "";
	
	//// Reset div innerHtml
	reset_innerHtml();
	
	$('#submit2').attr('disabled', true);
	
	if(obj.type.value=="") 
	{
		document.getElementById('div_audience_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect?><?=$i_Circular_RecipientType?></font>';
		error_no++;
		focus_field = "type";
	}
		
    if(obj.type.value==4) 
    {
	    checkOptionAll(obj.elements["target[]"]);
	    
		if(obj.elements["target[]"].length==0)
		{ 
			document.getElementById('div_audience_err_msg').innerHTML = '<font color="red"><?=$i_frontpage_campusmail_choose?></font>';
			error_no++;
			focus_field = "type";
		}
	}
	
	if(!check_text_30(obj.CircularNumber, "<?php echo $i_alert_pleasefillin.$i_Circular_CircularNumber; ?>.", "div_CircularNumber_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "CircularNumber";
	}
	 
	 if(!check_text_30(obj.Title, "<?php echo $i_alert_pleasefillin.$i_Circular_Title; ?>.", "div_Title_err_msg"))
	 {
		error_no++;
		if(focus_field=="")	focus_field = "Title";
	 }
	
	if (compareDate(obj.DateStart.value, obj.DateEnd.value) > 0) 
	{
		document.getElementById('div_DateEnd_err_msg').innerHTML = '<font color="red"><?=$i_Homework_new_duedate_wrong?></font>';
		error_no++;
		if(focus_field=="")	focus_field = "DateEnd";
	}
	
	<? /* ?>
	if (compareDate('<?=date('Y-m-d')?>', obj.DateStart.value) > 0)
	{
		document.getElementById('div_DateStart_err_msg').innerHTML = '<font color="red"><?=$i_Homework_new_startdate_wrong?></font>';
		error_no++;
		if(focus_field=="")	focus_field = "DateStart";
	}
	<? */ ?>
	if($('#status1').attr('checked') && $('#pushmessagenotify').attr('checked'))
	{
		var scheduleString = '';
	    if ($('input#sendTimeRadio_scheduled').attr('checked')) {
	    	scheduleString = $('input#sendTime_date').val() + ' ' + str_pad($('select#sendTime_hour').val(),2) + ':' + str_pad($('select#sendTime_minute').val(),2) + ':00';
	    }
	    $('input#sendTimeString').val(scheduleString);
	    
	    var minuteValid = false;
		if (str_pad($('select#sendTime_minute').val(),2) == '00' || str_pad($('select#sendTime_minute').val(),2) == '15' || str_pad($('select#sendTime_minute').val(),2) == '30' || str_pad($('select#sendTime_minute').val(),2) == '45') {
			minuteValid = true;
		}
		
		// if scheduled message => check time is past or not
		var dateNow = new Date();
		var dateNowString = dateNow.getFullYear() + '-' + str_pad((dateNow.getMonth()+1),2) + '-' + str_pad(dateNow.getDate(),2) + ' ' + str_pad(dateNow.getHours(),2) + ':' + str_pad(dateNow.getMinutes(),2) + ':' + str_pad(dateNow.getSeconds(),2);
		if ($('input#sendTimeRadio_scheduled').attr('checked') && minuteValid && scheduleString <= dateNowString) {
			// alert('<?=$Lang['AppNotifyMessage']['WarningMsg']['SendTimeInvalid']?>');
			// return false;
			document.getElementById('div_push_message_date_err_msg').innerHTML = '<font color="red"><?=$Lang['AppNotifyMessage']['WarningMsg']['SendTimeInvalid']?></font>';
			error_no++;
			if(focus_field=="")	focus_field = "sendTime_date";
		}
		
		// check time is within 30 days
		var maxDate = new Date();
		maxDate.setDate(dateNow.getDate() + 31);
		var maxDateString = maxDate.getFullYear() + '-' + str_pad((maxDate.getMonth()+1),2) + '-' + str_pad(maxDate.getDate(),2) + ' ' + str_pad(maxDate.getHours(),2) + ':' + str_pad(maxDate.getMinutes(),2) + ':' + str_pad(maxDate.getSeconds(),2);
		if ($('input#sendTimeRadio_scheduled').attr('checked') && minuteValid && scheduleString > maxDateString) {
			// alert('<?=$Lang['AppNotifyMessage']['WarningMsg']['PleaseSelectDateWithinRange']?>');
			$('input#sendTime_date').focus();
			// return false;
			document.getElementById('div_push_message_date_err_msg').innerHTML = '<font color="red"><?=$Lang['AppNotifyMessage']['WarningMsg']['PleaseSelectDateWithinRange']?></font>';
			error_no++;
			if(focus_field=="")	focus_field = "sendTime_date";
		}
	}
	
	var canSubmit = false;
	if(error_no>0)
	{
		eval("obj." + focus_field +".focus();");
//		return false;
		$('#submit2').attr('disabled', false);
		canSubmit = false;
	}
	else
	{
		//checkOption(obj.elements['Attachment[]']);
		//checkOptionAll(obj.elements["Attachment[]"]);
		//return true
		
		// create a list of files to be deleted
        var objDelFiles = document.getElementsByName('file2delete[]');
        var files = obj.deleted_files;
        if(objDelFiles!=null)
        {
	        var x="";
	        for(var i=0;i<objDelFiles.length;i++){
		        if(objDelFiles[i].checked==true){
			 		x+=objDelFiles[i].value+":";       
			    }
		    }
		    files.value = x.substr(0,x.length-1);
	    }
		    
//		return Big5FileUploadHandler();
		canSubmit = Big5FileUploadHandler();
	}
	
	if (canSubmit) {
		$('form#form1').submit();
	}
	else {
		$('#submit2').attr('disabled', false);
	}
	
}

function Big5FileUploadHandler() {
	
	 for(var i=0;i<=no_of_upload_file;i++)
	 {
	 	var objFile = eval('document.form1.filea'+i);
	 	var objUserFile = eval('document.form1.hidden_userfile_name'+i);
	 	if(objFile!=null && objFile.value!='' && objUserFile!=null)
	 	{
		 	var Ary = objFile.value.split('\\');
		 	objUserFile.value = Ary[Ary.length-1];
		}
	 }
	 return true;
}

function add_field()
{
	var table = document.getElementById("upload_file_list");
	var row = table.insertRow(no_of_upload_file);
	var cell = row.insertCell(0);
	x= '<input class="file" type="file" name="filea'+no_of_upload_file+'" size="40">';
	x+='<input type="hidden" name="hidden_userfile_name'+no_of_upload_file+'">';

	cell.innerHTML = x;
	no_of_upload_file++;
	
	document.form1.attachment_size.value = no_of_upload_file;
}

function setRemoveFile(index,v){
	obj = document.getElementById('a_'+index);
	obj.style.textDecoration = v?"line-through":"";
}

if('<?=$plugin['eClassTeacherApp']?>'=='1'){
	function clickedSendTimeRadio(targetType) {
		if (targetType == 'now') {
			$('div#specificSendTimeDiv').hide();
		}
		else {
			$('div#specificSendTimeDiv').show();
		}
	}
	
	function checkedSendPushMessage(checkBox) {
		if (checkBox.checked == true) {
			$('div#sendTimeSettingDiv').show();
			$('div#PushMessageSetting').show();
		} else {
			$('div#sendTimeSettingDiv').hide();
			$('div#PushMessageSetting').hide();
		}
	}
}
function Init_JQuery_AutoComplete(InputID_ClassNameClassNumber, InputID_UserLogin) {
	
	AutoCompleteObj_ClassNameClassNumber = $("input#" + InputID_ClassNameClassNumber).autocomplete(
		"ajax_search_user.php",
		{  			
			onItemSelect: function(li) {
				Add_Selected_User(li.extra[1], li.selectValue, InputID_ClassNameClassNumber);
			},
			formatItem: function(row) {
				return row[0];
			},
			maxItemsToShow: 100,
			minChars: 1,
			delay: 0,
			width: 200
		}
	);
	
	Update_Auto_Complete_Extra_Para();
}

function Add_Selected_User(UserID, UserName, InputID) {
	var UserID = UserID || "";
	var UserName = UserName || "";
	var UserSelected = document.getElementById('target[]');

	UserSelected.options[UserSelected.length] = new Option(UserName, UserID);
	
	Update_Auto_Complete_Extra_Para();
	
	// reset and refocus the textbox
	$('input#' + InputID).val('').focus();
}

function Update_Auto_Complete_Extra_Para(){
	checkOptionAll(document.getElementById('form1').elements["target[]"]);
	ExtraPara = new Array();
	ExtraPara['SelectedUserIDList'] = Get_Selection_Value('target[]', 'Array', true);
	AutoCompleteObj_ClassNameClassNumber[0].autocompleter.setExtraParams(ExtraPara);
}

function js_Remove_Selected_Student(){
	
	checkOptionRemove(document.getElementById('target[]'));
	Update_Auto_Complete_Extra_Para();
}

function onSearchAndInsertStaff(btnObj,textObj,targetObj)
{
	var loading_image = '<?=$linterface->Get_Ajax_Loading_Image(1)?>';
	var error_layer = $('#'+textObj.id+'_Error');
	var search_text = $.trim(textObj.value);
	error_layer.html('');
	if(search_text == ''){
		error_layer.html('<?=$Lang['AppNotifyMessage']['WarningRequestInputSearchLoginID']?>');
		return false;
	}
	
	error_layer.html(loading_image);
	btnObj.disabled = true;
	$.post(
		'ajax_batch_search_staff.php',
		{
			"SearchText":encodeURIComponent(search_text)
		},
		function(returnJson){
			var ary = [];
			if( JSON && JSON.parse){
				ary = JSON.parse(returnJson);
			}else{
				eval('ary='+returnJson);
			}
			var UserSelected = targetObj;
			var inputted_target_values = search_text.split("\n");
			var selected_target_values = Get_Selection_Value(targetObj.id,'Array',true);
			var searched_userlogins = [];
			var not_found_values = [];
			for(var i=0;i<ary.length;i++){
				var id_value = 'U'+ary[i]['UserID'];
				searched_userlogins.push(ary[i]['UserLogin']);
				if(selected_target_values.indexOf(id_value)==-1)
				{
    				<?php 	if($sys_custom['DHL']){?>
    				UserSelected.options[UserSelected.length] = new Option(ary[i]['UserName']+' ('+ary[i]['UserLogin']+')', id_value);
    				<?}else{ ?>
    				UserSelected.options[UserSelected.length] = new Option(ary[i]['UserName'], id_value);
    				<?php }?>
    				
					selected_target_values.push(id_value);
				}
			}
			for(var i=0;i<inputted_target_values.length;i++){
				if($.trim(inputted_target_values[i])!='' && searched_userlogins.indexOf(inputted_target_values[i])==-1){
					not_found_values.push(inputted_target_values[i]);
				}
			}
			js_Select_All(targetObj.id);
			if(not_found_values.length>0){
				error_layer.html('<?=$Lang['AppNotifyMessage']['WarningFollowingUsersCannotBeFound']?><br />'+not_found_values.join("<br />"));
			}else{
				error_layer.html('');
			}
			btnObj.disabled = false;
		}
	);
}
</script>

<?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>
<form name="form1" id="form1" method="post" action="new_update.php" enctype="multipart/form-data" >
<table class="form_table_v30">
<tr valign='top'>
	<td nowrap class='field_title'><span class='tabletextrequire'>*</span><?=$i_Circular_RecipientType?></td>
	<td><?=$type_selection?><br><span id="div_audience_err_msg"></span>
	<div id="div_audience"></div>
	</td>
</tr>

<tr>
	<td class='field_title'><?=$i_Notice_FromTemplate?></td>
	<td><?=$template_selection?></td>
</tr>
</table>

<div id="div_content">
	<?= $lcircular->return_FormContent($start, $end,0,'','','','',1,1,$circularAttFolder,'', ($plugin['eClassTeacherApp'] ? 1 : 0));?>
</div>

<?=$linterface->MandatoryField();?>

<div class="edit_bottom_v30">
	<?//= $linterface->GET_ACTION_BTN($button_submit, "submit", "checkOption(this.form.elements['Attachment[]']); ". ($type==4 ? "checkOption(this.form.elements['target[]']);" :"") ."this.form.action='new_update.php'; this.form.method='post'; return checkform(this.form);","submit2") ?>
	<?= $linterface->GET_ACTION_BTN($button_submit, "button", ($type==4 ? "checkOption(document.form1.elements['target[]']);" :"") ." checkform(document.form1);","submit2") ?>
    <?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
	<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
</div>	

<input type="hidden" name="CurrentTemplateIndex" value="0">
<input type="hidden" name="circularAttFolder" value="<?=$circularAttFolder?>" />

<input type="hidden" name="deleted_files" value='' />
<input type="hidden" name='attachment_size' value='<? echo $no_file==""?1:$no_file;?>'>

</form>

<script language="JavaScript1.2">
<? /* ?>
obj = document.form1.elements["Attachment[]"];
checkOptionClear(obj);
<?php while (list($key, $value) = each($files)) echo "checkOptionAdd(obj, \"".$files[$key][0]." (".ceil($files[$key][1]/1000)."Kb".")\", \"".$files[$key][0]."\");\n";  ?>
checkOptionAdd(obj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
<? */ ?>
<? if ($templateID!=-1 && $lnotice->RecordType == 4) { ?>
obj = document.form1.elements["target[]"];
checkOptionClear(obj);
checkOptionAdd(obj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
<? } ?>
</script>



<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
