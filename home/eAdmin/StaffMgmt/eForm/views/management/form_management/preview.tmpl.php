<script type="text/javascript">
	var langtxt = [];
<?php
if (count($Lang["eForm"]["jsMsg"]) > 0) {
	foreach ($Lang["eForm"]["jsMsg"]as $jsLangIndex => $jsLangVal) {
?>
		langtxt["<?php echo $jsLangIndex; ?>"] = "<?php echo $jsLangVal; ?>";
<?php
	}
}
?>
</script>
<link rel="stylesheet" href="/templates/slrs/css/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" href="/templates/eForm/assets/css/jquery.alerts.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" href="/templates/eForm/assets/datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="/templates/eForm/assets/fancybox/jquery.fancybox.min.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/templates/eForm/assets/css/eform_viewer.css?t=<?php echo time(); ?>" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="/templates/eForm/assets/css/custom.css?t=<?php echo time(); ?>" type="text/css" />

<script src="/templates/eForm/assets/libs/jquery.3.1.1.js"></script>
<script src="/templates/eForm/assets/libs/jquery-migrate-1.4.1.js"></script>
<script src="/templates/eForm/assets/libs/jquery.alerts.js"></script>
<script src="/templates/eForm/assets/fancybox/jquery.fancybox.min.js"></script>
<script src="/templates/eForm/assets/datetimepicker/js/moment-with-locales.min.js"></script>
<script src="/templates/eForm/assets/datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
<script src="/templates/eForm/assets/js/eform_viewer.js?t=<?php echo time(); ?>"></script>

<div class='folder_breadcrumb'>
<?php echo $htmlBreadcrumb; ?>
</div>
<?php
echo $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
echo $htmlAry['contentTool'];
?>
<div class='form-preview form-style-2'>
	<div class='form-wrap'>
<?php
echo $htmlAry['formBody'];
?>
	</div>
</div>
<div style='text-align:center; margin-top:20px; margin-bottom:20px;'>
<?php echo $indexVar['libeform_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn'); ?>
</div>
<script type="text/javascript">
	function goBack() {
		window.location.href = "?task=management/form_management/list&folder=<?php echo $folderID; ?>";
	}
</script>