<?php
$task = $indexVar['task'];
$taskArr = explode("/", $task);
if (count($taskArr) > 0) {
    array_pop($taskArr);
    $task = implode("/", $taskArr);
}
$is_admin = false;
if (strpos($_SERVER["REQUEST_URI"], 'eAdmin') !== false) {
    $is_admin = true;
}
?>
<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/Product" lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
	<title><?php echo $Lang["eForm"]["select_panel"]; ?></title>
	
	<link rel="stylesheet" href="/templates/eForm/assets/bootstrap-3.3.7/css/bootstrap.min.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="/templates/slrs/css/font-awesome.min.css" type="text/css" />
	<link rel="stylesheet" href="/templates/eForm/assets/duallistbox/bootstrap-duallistbox.min.css" type="text/css" media="screen" />
	<link href="/templates/eForm/assets/css/panel.css?t=<?php echo time(); ?>" rel="stylesheet" type="text/css">
	
	<script src="/templates/eForm/assets/libs/jquery.3.1.1.js"></script>
	<script src="/templates/eForm/assets/bootstrap-3.3.7/js/bootstrap.min.js"></script>
	<script src="/templates/eForm/assets/duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
</head>
<body style='margin:0px; padding:0px;'>
<div class='dualSelectPanel <?php echo (($args["subtype"] == "multiple" && $args["maxopt"] > 1) ? $args["subtype"] : 'single'); ?> <?php echo $_GET["type"]; ?>'>
	<div class='ds_title'>
		<span><?php echo $Lang["eForm"]["select_panel"]; ?></span>
		<a href='#' class='closebtn rightbtn'><i class='fa fa-close'></i></a>
	</div>
	<div class='ds_content'>
<?php if (isset($year_selection)) { ?>
		<div class="row bottomborder">
			<div class='col-sm-6'>
				<div class='text-bold'><?php echo $year_label ?></div>
				<div class='yearpanel'>
					<?php echo $year_selection; ?>
				</div>
				<small class='text-danger'><i class='fa fa-warning'></i> <?php echo $Lang["eForm"]["YearOptionWarningMsg"]; ?></small>
			</div>
<?php if ($_GET["type"] == "subjectGroupElm") { ?>
			<div class='col-sm-6'>
				<div class='text-bold'><?php echo $yearterm_label ?></div>
				<div class='yeartermpanel'>
					<?php echo $yearterm_selection; ?>
				</div>
			</div>
<?php } ?>
			<div class='col-sm-6'>
				<div class='text-bold'><?php echo $option_label ?></div>
        		<div class='typepanel'>
        			<?php echo $type_selection; ?>
        		</div>
			</div>
		</div><br>
<?php } else { ?>
		<div class='text-bold'><?php echo $option_label ?></div>
		<div class='typepanel'>
		<?php echo $type_selection; ?>
		</div>
<?php } ?>
		<div class='ajax_content'></div>
		<div class="remark"><?php if ($args["subtype"] == "multiple") {
		    if (isset($args["maxopt"]) && $args["maxopt"] > 1) {
		        echo "<i class='fa fa-info-circle'></i> " . str_replace(":max", $args["maxopt"], $Lang["eForm"]["multiple_option"]);
		    } else {
		        echo "<i class='fa fa-info-circle'></i> " . $Lang["eForm"]["single_option"];
		    }
		} else {
		    echo "<i class='fa fa-info-circle'></i> " . $Lang["eForm"]["single_option"];
		}?></div>
	</div>
	<div class='ds_footer'>
<?php if (!$is_admin) { ?>
		<a href='#' class='btn btn-sm btn-info submitbtn'><?php echo $Lang["eForm"]["dualselect"]["confirm"]; ?></a>
<?php } ?>
		<a href='#' class='btn btn-sm btn-default closebtn'><?php echo $Lang["eForm"]["dualselect"]["cancel"]; ?></a>
	</div>
</div>
<script language='javascript' type='text/javascript'>

var panelJS = {
		vrs : {
			rtype: '<?php echo $_GET["type"]; ?>',
			info_url : "?task=<?php echo $task; ?>/panel&elm=<?php echo $_GET["elm"]; ?>&type=<?php echo $_GET["type"]; ?>&infotype=panelonly&subtype=<?php echo $args["subtype"]; ?>",
			dualpanelbox :'',
			elm: "<?php echo $_GET["elm"]; ?>",
			year: '<?php echo $yearID; ?>',
			term: '<?php echo $yearTermID; ?>',
			maxopt : <?php echo ($args["maxopt"] > 0 ? $args["maxopt"] : "1"); ?>,
			optType : ''
		},
    	ltr: {
    		closePanelHandler: function(e) {
    			parent.jQuery.fancybox.close();
    		},
    		changeTeacherTypeHandler: function(e) {
        		var opt = $(this).val();
        		panelJS.vrs.optType = opt;
        		var selectedOpt = panelJS.cfn.getSelectOptions();
            	panelJS.cfn.changeOption(opt, selectedOpt);
    		},
    		changeStudentTypeHandler: function(e) {
    			var opt = $(this).val();
    			panelJS.vrs.optType = opt;
    			var selectedOpt = panelJS.cfn.getSelectOptions();
        		panelJS.cfn.changeOption(opt, selectedOpt);
    		},
    		changeSubjectTypeHandler: function(e) {
    			var opt = $(this).val();
    			panelJS.vrs.optType = opt; 
    			var selectedOpt = panelJS.cfn.getSelectOptions();
        		panelJS.cfn.changeOption(opt, selectedOpt);
    		},
    		changeYearHandler: function(e) {
    			panelJS.vrs.year = $(this).val();
    			panelJS.vrs.term = "";
    			panelJS.cfn.yearAndTermHandler('YEAR');
    		},
    		changeYearTermHandler: function(e) {
    			panelJS.vrs.term = $(this).val();
    			panelJS.cfn.yearAndTermHandler('TERM');
    		},
    		limitCheckHandle: function(e) {
        		var elm = $(this);
    			var size = elm.find(":selected").length;
    			
    		    if(size > panelJS.vrs.maxopt){
    		    	elm.find(":selected").each(function(ind, sel){            
    		            if(ind > panelJS.vrs.maxopt - 1)
    		                $(this).prop("selected", false)
    		        })
    		        elm.bootstrapDualListbox('refresh', true);
    		    }
    		}
    	},
    	cfn: {
        	yearAndTermHandler: function(type) {
        		var url = panelJS.vrs.info_url;
    			var panelType = "class";
    			if (panelJS.vrs.rtype == "subjectGroupElm" && type == "YEAR") {
    				panelType = "yearterm";
    			}
    			var firstOpt = "";
    			if (type == "YEAR") {
                	$.ajax({
                		url: url,
                		type: 'get',
                		data: { 'yid': panelJS.vrs.year, 'tid': panelJS.vrs.term, 'paneltype': panelType },
                		success: function (data, status) {
                			if (panelJS.vrs.rtype == "subjectGroupElm") {
                				$('select[name="yearterm"]').unbind('change', panelJS.ltr.changeYearTermHandler);
                				$('.yeartermpanel').html(data);
                				panelJS.vrs.term = $('select[name="yearterm"] > option').eq(0).val();
        						$('select[name="yearterm"]').bind('change', panelJS.ltr.changeYearTermHandler);
        						firstOpt = panelJS.vrs.optType;
                			} else {
                				$('.typepanel').html(data);
                				$('.typepanel select.studentopt').unbind('change', panelJS.ltr.changeStudentTypeHandler);
                    			$('.typepanel select.studentopt').bind('change', panelJS.ltr.changeStudentTypeHandler);
        						firstOpt = $('select[name="type"].studentopt > option').eq(0).val();
                			}
                			var selectedOpt = panelJS.cfn.getSelectOptions();
        	        		panelJS.cfn.changeOption(firstOpt, selectedOpt);
            		    },
            		    error: function (xhr, desc, err) {
    //             		    console.log(xhr);
    //             		    console.log("Desc: " + desc + "\nErr:" + err);
            		    }
                	});
    			} else {

    				firstOpt = panelJS.vrs.optType;
    				var selectedOpt = panelJS.cfn.getSelectOptions();
	        		panelJS.cfn.changeOption(firstOpt, selectedOpt);
    			}
        	},
        	getSelectOptions: function() {
        		var selectedOpt = [];
    			if ($('.box2 select > option').length > 0) {
        			$('.box2 select > option').each(function() {
        				if ($(this).val() != '') {
                			selectedOpt.push($(this).val());
                    	}
                	});
    			}
    			return selectedOpt;
        	},
        	getSelectOptionsTags: function() {
        		var selectedTags = "";
        		if ($('.box2 select > option').length > 0) {
        			var yeartxt = $('select[name="year"] option[value="' + panelJS.vrs.year + '"]').text();
        			if (yeartxt != "") {
    					selectedTags += "<div class='yeartag'><i class='fa fa-calendar-minus-o'></i> " + yeartxt + "</div>";
					}
        			$('.box2 select > option').each(function() {
        				if ($(this).val() != '') {
        					selectedTags += "<a href='#' class='tags' rel='" + $(this).val() + "'><nobr><i class='fa fa-close'></i> " + $(this).text() + "</nobr></a>";
                    	}
                	});
    			}
    			return selectedTags;
        	},
        	changeOption: function(val, selectedOpt) {
            	var url = panelJS.vrs.info_url;
            	$.ajax({
            		url: url,
            		type: 'POST',
            		data: { "selected" : selectedOpt, "opt" : val, 'yid': panelJS.vrs.year, 'tid': panelJS.vrs.term },
            		success: function (data, status) {
            		    $('.ajax_content').html(data);
            		    panelJS.cfn.selectPanelInit();
        		    },
        		    error: function (xhr, desc, err) {
//             		    console.log(xhr);
//             		    console.log("Desc: " + desc + "\nErr:" + err);
        		    }
            	});
        	},
        	selectPanelInit: function() {
        		panelJS.vrs.dualpanelbox = $('.option_panel').bootstrapDualListbox({
                        		      				  nonSelectedListLabel: '<?php echo $Lang["eForm"]["dualselect"]["non-selected"]; ?>',
                        		      				  selectedListLabel: '<?php echo $Lang["eForm"]["dualselect"]["selected"]; ?>',
                        		      				  infoText: '<?php echo $Lang["eForm"]["dualselect"]["infoText"]; ?>',
                        		      				  infoTextFiltered: '<?php echo $Lang["eForm"]["dualselect"]["infoTextFiltered"]; ?>',
                        		      				  filterTextClear: '<?php echo $Lang["eForm"]["dualselect"]["filterTextClear"]; ?>',
                        		      				  filterPlaceHolder: '<?php echo $Lang["eForm"]["dualselect"]["filterPlaceHolder"]; ?>',
                        		      				  moveSelectedLabel: '<?php echo $Lang["eForm"]["dualselect"]["moveSelectedLabel"]; ?>',
                        		      				  moveAllLabel: '<?php echo $Lang["eForm"]["dualselect"]["moveAllLabel"]; ?>',
                        		      				  removeSelectedLabel: '<?php echo $Lang["eForm"]["dualselect"]["removeSelectedLabel"]; ?>',
                        		      				  removeAllLabel: '<?php echo $Lang["eForm"]["dualselect"]["removeAllLabel"]; ?>',
                        		      				  infoTextEmpty: '<?php echo $Lang["eForm"]["dualselect"]["infoTextEmpty"]; ?>',
                        		      				  preserveSelectionOnMove: false,
                        		      				  moveOnSelect: false,
                        		      				  selectorMinimalHeight: '120',
                        		      				  helperSelectNamePostfix: "_bs"
       										});
    
    			if (typeof panelJS.vrs.maxopt != "undefined") {
    				$('#myds').bind('change', panelJS.ltr.limitCheckHandle);
    				$('.box1 select').bind('change', panelJS.ltr.limitCheckHandle);
    			}
      			$('.nullopt').delay(1000).remove();
				
        	},
    		init: function() {
    			var elmLabel = panelJS.vrs.elm;
				var elmID = elmLabel.replace("dselect_", "");
				var tagElm = "#" + elmID + "_data";
				var hiddleElm = "input[name='" + elmID + "']";
<?php if ($is_admin) { ?>
				var selectedOpt = [];
<?php } else { ?>
				var initData = window.parent.$(hiddleElm).eq(0).val();
				var initJson = (initData != "") ? JSON.parse(initData) : '' ;
				var selectedOpt = initJson.selectOpt || [];
				var selectedYear = initJson.year || '';
				var selectedTerm = initJson.term || '';
				var selectedType = initJson.otype || '';
				if (selectedYear != "") {
					panelJS.vrs.year = selectedYear;
					$("select[name='year']").val(selectedYear);
				}
				if (selectedTerm != "") {
					panelJS.vrs.term = selectedTerm;
					$("select[name='term']").val(selectedTerm);
				}
				if (selectedType != "") {
					panelJS.vrs.optType = selectedType;
					$("select[name='type']").val(selectedType);
				}
<?php } ?>
        		panelJS.cfn.selectPanelInit();
    			$('.closebtn').bind('click', panelJS.ltr.closePanelHandler);
    			var panel_title = window.parent.$(hiddleElm).eq(0).parent().attr('title');
    			if (typeof panel_title != "undefined") {
<?php if ($is_admin) { ?>
					panel_title += " <?php echo $Lang["eForm"]["preview"]; ?>";
<?php } ?>
					$('.ds_title > span').html(panel_title);
    			}
    			var firstOpt = "";
    			switch (panelJS.vrs.rtype) {
	   				case "teacherElm":
    					$('.typepanel select.teacheropt').bind('change', panelJS.ltr.changeTeacherTypeHandler);
    					firstOpt = $('select[name="type"].teacheropt > option').eq(0).val();
    					panelJS.vrs.optType = firstOpt; 
    					break;
	   				case "studentElm":
	   					$('select[name="year"]').bind('change', panelJS.ltr.changeYearHandler);
    					$('.typepanel select.studentopt').bind('change', panelJS.ltr.changeStudentTypeHandler);
    					firstOpt = $('select[name="type"].studentopt > option').eq(0).val();
    					panelJS.vrs.optType = firstOpt;
    					break;
	   				case "subjectGroupElm":
	   					$('select[name="year"]').bind('change', panelJS.ltr.changeYearHandler);
	   					$('select[name="yearterm"]').bind('change', panelJS.ltr.changeYearTermHandler);
	   					$('.typepanel select.subjectgroupopt').bind('change', panelJS.ltr.changeSubjectTypeHandler);
	   					
    					firstOpt = $('select[name="type"].subjectgroupopt > option').eq(0).val();
    					panelJS.vrs.optType = firstOpt;
		   				break;
    			}
    			panelJS.cfn.changeOption(firstOpt, selectedOpt);
<?php if (!$is_admin) { ?>
				ePanelHandleJS.cfn.init();
<?php } ?>
    		}
    	}
    };
<?php if (!$is_admin) { ?>
	var ePanelHandleJS = {
		vrs: {
			container: '.box2 select',
			btn: '.submitbtn'
		},
		ltr: {
			submitHandler: function(e) {
				var elmLabel = panelJS.vrs.elm;
				var elmID = elmLabel.replace("dselect_", "");
				var tagElm = "#" + elmID + "_data";
				var hiddleElm = "input[name='" + elmID + "']";
				var selectOpts = panelJS.cfn.getSelectOptions();
				if (selectOpts.length > 0) {
					var parentData = { year: panelJS.vrs.year, term: panelJS.vrs.term, otype: panelJS.vrs.optType, selectOpt: panelJS.cfn.getSelectOptions() };
					window.parent.$(hiddleElm).val(JSON.stringify(parentData));
					var htmlElm = panelJS.cfn.getSelectOptionsTags();
					window.parent.$(hiddleElm).parent().find('.tagsbox').eq(0).html(htmlElm);
					// window.parent.$(tagElm).html(htmlElm);

					var tagsJS = window.parent.tagsJS;
					tagsJS.cfn.bindings();
					
				} else {
					window.parent.$(hiddleElm).val('');
					window.parent.$(hiddleElm).parent().find('.tagsbox').eq(0).html('');
					// window.parent.$(tagElm).html('');
				}
				
				parent.jQuery.fancybox.close();				
				e.preventDefault();
			}
		},
		cfn: {
			init: function() {
				if ($(ePanelHandleJS.vrs.btn).length > 0) {
					$(ePanelHandleJS.vrs.btn).bind('click', ePanelHandleJS.ltr.submitHandler);
				} 
			}
		}
	}
<?php } ?>
$(document).ready(function() {
	panelJS.cfn.init();
});
</script>
</body>
</html>