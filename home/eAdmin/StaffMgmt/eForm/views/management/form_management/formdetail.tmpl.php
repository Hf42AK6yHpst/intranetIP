<?php 
echo $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
?>
<script src="/templates/eForm/assets/libs/jquery.3.1.1.js"></script>
<script src="/templates/eForm/assets/libs/jquery-migrate-1.4.1.js"></script>
<link href="/templates/eForm/assets/css/jquery.alerts.css" rel="stylesheet" type="text/css">
<script src="/templates/eForm/assets/libs/jquery.alerts.js"></script>
<link rel="stylesheet" href="/templates/slrs/css/font-awesome.min.css" type="text/css" />
<link href="/templates/eForm/assets/css/eform_viewer.css?t=<?php echo time(); ?>" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="/templates/eForm/assets/css/custom.css" type="text/css" />
<script src="/templates/eForm/assets/js/eform_viewer.js?t=<?php echo time(); ?>"></script>
<div class='form-preview form-style-2'>
	<div class='form-wrap'>
<?php
echo $htmlAry['formBody'];
?>
		<div class='SupplementaryRequest' id="SupplementaryRequestBox" style='display:none;'>
			<form id='suppForm'>
			<input type='hidden' name='fid' value='<?php echo $_GET["fid"]; ?>'>
			<input type='hidden' name='gid' value='<?php echo $_GET["gid"]; ?>'>
			<input type='hidden' name='ctk' value='<?php echo $_GET["ctk"]; ?>'>
			<div class='contenttitle'>
				<?php echo $Lang["eForm"]["SUPPLEMENTARY_REQUEST"]; ?>
			</div><br>
			<div class='SuppFormBody'>
				<div class='SupFormLabel'><?php echo $Lang["eForm"]["SUPPLEMENTARY_REQUEST_Remark"]; ?>:</div>
				<div class='SupFormLabel'><textarea name='SuppRemarks' class='sup_field input-field' rows="5"></textarea></div>
			</div>
			<div class="text-right" style='margin-top:5px;'>
				<input type='submit' id='SuppReSubmitBtn' class='formbutton_v30 print_hide' value='<?php echo $Lang['Btn']['Submit']; ?>'>
			</div>
			</form>
<?php echo $indexVar["libeform_ui"]->getLoadingElm(); ?>
		</div>
	</div>
</div>
<div style='text-align:center; margin-top:20px; margin-bottom:20px;'>
<?php echo $indexVar['libeform_ui']->Get_Action_Btn($Lang["eForm"]["SUPPLEMENTARY_REQUEST"], "button", "showSupp()", 'SuppReqOpenBtn'); ?> 
<?php echo $indexVar['libeform_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn'); ?>
</div>
<script type="text/javascript">
	function goBack() {
		window.location.href = "?task=management/form_management/recordlist&fid=<?php echo $_GET["fid"]; ?>&ctk=<?php echo $indexVar["FormInfo"]->getToken($_GET["fid"]); ?>";
	}

	function showSupp() {
		$("#SuppReqOpenBtn").hide();
		$("#SupplementaryRequestBox").slideDown('fast');
	}

var suppReqJS = {
		vrs: {
			hdr: ".SupplementaryRequest",
			url: "?task=management/form_management/suppreq_update",
			container: "#SuppReSubmitBtn",
			remarkObj: ".sup_field",
			loadingElm: ".loadingContainer",
			formObj: "#suppForm",
			suppData: {},
			xhr: ""
		},
		ltr: {
			clickHandler: function (e) {
				if ($(suppReqJS.vrs.remarkObj).length > 0) {
					var isValid = true;
					$(suppReqJS.vrs.remarkObj).each(function() {
						if (typeof $(this).val() == "undefined" || $(this).val() == "" || typeof $(this).attr('name') == "undefined") {
							isValid = false;
							$(this).addClass('error').focus();
						} else {
							$(this).removeClass('error');
						}
					});
					if (isValid) {
						suppReqJS.vrs.suppData = $(suppReqJS.vrs.formObj).serializeArray();
						suppReqJS.cfn.showLoading();

						var xhr = suppReqJS.vrs.xhr;
						if (xhr != "") {
							xhr.abort();
						}
						suppReqJS.vrs.xhr = $.ajax({
							url: suppReqJS.vrs.url,
							data: suppReqJS.vrs.suppData,
							method:"post",
							dataType: "json",
							success: function(data) {
								if (data.result != "SUCCESS") {
									suppReqJS.cfn.submitFail();
								} else {
									suppReqJS.cfn.hideLoading();
									$(suppReqJS.vrs.hdr).eq(0).html(data.resultHTML);
									jAlert('<?php echo $Lang["eForm"]["jsMsg"]["SubmitSuccess"]; ?>', '<?php echo $Lang["eForm"]["jsMsg"]["AlertDialog"]; ?>');
								}
							},
							error: function() {
								suppReqJS.cfn.submitFail();
							}
						});
					}
				}
				e.preventDefault();
			}
		},
		cfn: {
			submitFail: function() {
				jAlert('<?php echo $Lang["eForm"]["jsMsg"]["SubmitFail"]; ?>', '<?php echo $Lang["eForm"]["jsMsg"]["AlertDialog"]; ?>', function() {
					suppReqJS.cfn.hideLoading();
				});
			},
			showLoading: function(scrolto) {
				$("input, textarea, select").prop('disabled', true);
				$("#popup_ok").prop('disabled', false);
				$(suppReqJS.vrs.loadingElm).eq(0).fadeIn(800, function() {
					if (typeof scrolto != "undefined" && scrolto != false) {
						$('html, body').animate({
						    scrollTop: $(suppReqJS.vrs.loadingElm).offset().top - 100
						}, 1000);
					}
				});
			},
			hideLoading: function() {
				$("input, textarea, select").prop('disabled', false); 
				$(suppReqJS.vrs.loadingElm).eq(0).delay(500).fadeOut(400);
			},
			init: function () {
				$(suppReqJS.vrs.container).bind('click', suppReqJS.ltr.clickHandler);
			}
		}	
	};
$(document).ready(function() {
	suppReqJS.cfn.init();
});

</script>