<form name="form1" id="form1" method="POST" action="?task=settings<?=$eFormConfig['taskSeparator']?>table_template<?=$eFormConfig['taskSeparator']?>list">
	<div class="table_board">
	
		<div class="content_top_tool">
			<?=$htmlAry['contentTool']?>
			<?=$htmlAry['searchBox']?>
			<br style="clear:both;">
		</div>
		
		<p class="spacer"></p>
		<?=$htmlAry['dbTableActionBtn']?>
		<?=$htmlAry['dataTable']?>
		<p class="spacer"></p>
		
		<?=$htmlAry['hiddenField']?>
		<?=$htmlAry['maxRow']?>	
		
	</div>
</form>
<script type="text/javascript">
$(document).ready( function() {	
	$('input#keyword').keydown( function(evt) {
		if (Check_Pressed_Enter(evt)) {
			// pressed enter
			goSearch();
		}
	});	
	eClassFormJS.cfn.init();
});

function goSearch() {
	$('form#form1').submit();
}

function goExchangeLesson(){
	window.location = '?task=settings<?=$eFormConfig['taskSeparator']?>table_template<?=$eFormConfig['taskSeparator']?>edit';
}

function goEdit(){
	checkEdit(document.form1,'groupIdAry[]','?task=settings<?=$eFormConfig['taskSeparator']?>table_template<?=$eFormConfig['taskSeparator']?>edit');
}
function goDelete(){
	checkRemove(document.form1,'groupIdAry[]','?task=settings<?=$eFormConfig['taskSeparator']?>table_template<?=$eFormConfig['taskSeparator']?>delete');
}

function goNewForm() {
	window.location.href = "?task=settings<?=$eFormConfig['taskSeparator']?>table_template<?=$eFormConfig['taskSeparator']?>edit";
}

var eClassFormJS = {
	vrs: {},
	ltr: {
		disableHandler: function(e) {
			e.preventDefault();
		}
	},
	cfn: {
		init: function() {
			$(window).bind('hashchange', eClassFormJS.ltr.disableHandler);
			var strHash = window.location.hash;
			if (typeof strHash != "undefined" && strHash != "") {
				strHash = strHash.replace("#", "");
				alert(strHash);
				window.location.hash = "";
			}
			$('a.linkdisable').bind('click', eClassFormJS.ltr.disableHandler);
		}
	}	
};

</script>