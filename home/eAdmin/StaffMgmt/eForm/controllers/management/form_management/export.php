<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST, INTRANET_SLRS_LESSON_ARRANGEMENT
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

if (isset($_POST['keyword'])) {
	$keyword = $_POST['keyword'];
}
$arrCookies = array();
$arrCookies[] = array("keyword");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}
$thisToken = $indexVar['FormGrid']->getToken($_GET["fid"] . "_csv");
if (!empty($_GET["fid"]) && $thisToken != $_GET["ctk"]) {
	No_Access_Right_Pop_Up();
}
$args = array(
		"page_size" => $page_size,
		"keyword" => $keyword,
		"FormID" => $_GET["fid"],
		"completed" => true
);
$FormID= $_GET["fid"];
$formInfo = $indexVar["FormInfo"]->getFormInfoById($FormID);
$gridInfo = $indexVar['FormGrid']->getTableGridInfo($args, false, false);

if (isset($formInfo["TemplateID"]) && $formInfo["TemplateID"] > 0) {
	$formInfo["TemplateInfo"] = $indexVar["FormTemplate"]->getFormTemplateInfoById($formInfo["TemplateID"], $getFullDetail = true);
	$headerScheme = $indexVar["FormTemplate"]->getDataHeaderScheme($formInfo);
	if (count($headerScheme) > 0) {
		$dataArr["header"] = $indexVar["FormTemplate"]->retrieveHeaderArray($headerScheme);
		$args = array(
				"sql" => $gridInfo["sql"],
				"girdInfo" => $gridInfo
		);
		$gridData = $indexVar['FormGrid']->getTableGridData($args, false, false);
		$dataArr["formdata"] = array();
		
		if (count($gridData) > 0) {
			foreach ($gridData as $gIndex => $gVal) {
				 $thisGridData = $indexVar['FormGrid']->getGridDataByID($gVal["GridID"]);
				 $dataArr["formdata"][$gIndex] = $indexVar['FormGrid']->retrieveForExport($gVal, $thisGridData, $headerScheme);
			}
		}
		$forDebug = false;
		if ($forDebug) {
			// header('Content-Type: text/plain; charset=utf-8');
			echo "<style> td, th { border:1px solid #000; border-collapse: collapse; font-size:11px; } </style>";
			echo "<table border='0' cellpadding=5 cellspacing=0>\n";
			echo "<thead>\n";
			echo "<tr>\n";
			echo "<th><nobr>" . implode("</nobr></th>\n<th><nobr>", $dataArr["header"]) . "</nobr></th>\n";
			echo "</tr>\n";
			echo "</thead>\n";
			echo "<tbody>\n";
			if (count($dataArr["formdata"]) > 0) {
				foreach ($dataArr["formdata"] as $kk => $vv) {
					echo "<tr>\n";
					echo "<td><nobr>" . implode("</nobr></td>\n<td><nobr>", $vv) . "</nobr></td>\n";
					echo "</tr>\n";
				}
			}
			echo "</tbody>\n";
			echo "</table>\n";
			exit;
		}
		$lexport = new libexporttext();
		$exportContent = $lexport->GET_EXPORT_TXT($dataArr["formdata"], $dataArr["header"], $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=1);
		$fileName = $formInfo["FormTitle"] . '.csv';
		$lexport->EXPORT_FILE($fileName, $exportContent);
		exit;
	}
}
No_Access_Right_Pop_Up();
?>