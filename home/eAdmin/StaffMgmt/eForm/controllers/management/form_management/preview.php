<?php
// editing by
/********************
 * Date : 2018-03-12 Frankie
 * Description :    Add Folder Structure to eForm
 ********************/

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

if (isset($_POST['keyword'])) {
	$keyword = $_POST['keyword'];
}
/*
$arrCookies = array();
$arrCookies[] = array("eform_form_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}
*/
# Page Title
$TAGS_OBJ[] = array($Lang['eForm']['FormManagement']);
$indexVar['libeform_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libinterface']->Include_AutoComplete_JS_CSS();
echo $indexVar['libeform_ui']->Include_Thickbox_JS_CSS();
echo $indexVar['libinterface']->Include_JS_CSS();

$formData = array();
// if (isset($_POST["groupIdAry"][0]) && $_POST["groupIdAry"][0] > 0) {
$thisCTK =  $indexVar["FormInfo"]->getToken($_GET["fid"]);
if (isset($_GET["fid"]) && !empty($_GET["fid"]) && ($_GET["ctk"] == $thisCTK)) { 
	// $FormID = $_POST["groupIdAry"][0];
	$FormID= $_GET["fid"];
	$formInfo = $indexVar["FormInfo"]->getFormInfoById($FormID);

	$folderID = (isset($formInfo["FolderID"]) && !empty($formInfo["FolderID"])) ? $formInfo["FolderID"]: '0';
	$folderBreadcrumb = $indexVar['libeform']->getBreadcrumb($folderID);
	$htmlBreadcrumb = $indexVar['libeform_ui']->getBreadcrumbHTML($folderBreadcrumb);
	
	if (isset($formInfo["TemplateID"]) && $formInfo["TemplateID"] > 0) {
		$formInfo["TemplateInfo"] = $indexVar["FormTemplate"]->getFormTemplateInfoById($formInfo["TemplateID"], $getFullDetail = true);
		$htmlAry['formBody'] = $indexVar['libeform_ui']->return_FormBuilderByTemplate($formInfo, array(), true);
	} else {
		No_Access_Right_Pop_Up();
	}
	$PAGE_NAVIGATION[] = array($Lang["eForm"]["FormPreview"]);
} else {
	No_Access_Right_Pop_Up();
}
$btnAry[] = array('edit', '?task=management/form_management/edit&fid=' .$_GET["fid"]."&ctk=" . $indexVar["FormInfo"]->getToken($_GET["fid"]), $Lang["eForm"]["FormEdit"], $subBtnAry);
$htmlAry['contentTool'] = $indexVar['libeform_ui']->Get_Content_Tool_By_Array_v30($btnAry);
$htmlAry['backBtn'] = $indexVar['libeform_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');
?>