<?php

$groupID = $_POST["groupIdAry"];
$result = $indexVar["FormInfo"]->removeForm($groupID);
if (count($result) > 0) {
	$saveSuccess = true;
} else {
	$saveSuccess = false;
}
$returnMsgKey = ($saveSuccess)? $Lang["eForm"]["ReturnMessage"]["UpdateSuccess"] : $Lang["eForm"]["ReturnMessage"]["UpdateUnsuccess"];
header('Location: ?task=management'.$eFormConfig['taskSeparator'].'form_management'.$eFormConfig['taskSeparator'].'list&folder=' . $_GET["folder"] . '&slug=' . $_GET["slug"] . '#'.$returnMsgKey);
exit;
?>