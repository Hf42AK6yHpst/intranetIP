<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST, INTRANET_SLRS_LESSON_ARRANGEMENT
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

if (isset($_POST['keyword'])) {
	$keyword = $_POST['keyword'];
}
/*
$arrCookies = array();
$arrCookies[] = array("eform_form_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}
*/
# Page Title
$TAGS_OBJ[] = array($Lang['eForm']['FormManagement']);
$indexVar['libeform_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libinterface']->Include_AutoComplete_JS_CSS();
echo $indexVar['libeform_ui']->Include_Thickbox_JS_CSS();
echo $indexVar['libinterface']->Include_JS_CSS();

$formData = array();
// if (isset($_POST["groupIdAry"][0]) && $_POST["groupIdAry"][0] > 0) {
$thisCTK =  $indexVar["FormInfo"]->getToken($_GET["fid"] . "_" . $_GET["gid"]);
if (!empty($_GET["fid"]) && !empty($_GET["gid"]) && ($_GET["ctk"] == $thisCTK)) { 
	// $FormID = $_POST["groupIdAry"][0];
	$FormID= $_GET["fid"];
	$formInfo = $indexVar["FormInfo"]->getFormInfoById($FormID);
	if (isset($formInfo["TemplateID"]) && $formInfo["TemplateID"] > 0) {
		$formInfo["TemplateInfo"] = $indexVar["FormTemplate"]->getFormTemplateInfoById($formInfo["TemplateID"], $getFullDetail = true);
		
		$formInfo["GridInfo"] = $indexVar["FormGrid"]->getGridInfoByID($_GET["gid"], $formInfo);
		$formInfo["GridInfo"]["GridData"] = array();
		if (count($formInfo["GridInfo"]) > 0) {
			$formInfo["GridInfo"]["GridData"] =  $indexVar["FormGrid"]->getGridDataByID($_GET["gid"]);
			$formInfo["GridInfo"]["SuppReqInfo"] =  $indexVar["FormGrid"]->getGridLatestSuppInfo($_GET["gid"]);
		}
		$htmlAry['formBody'] = $indexVar['libeform_ui']->return_FormBuilderByTemplate($formInfo, $formInfo["GridInfo"]["GridData"], false, true);
	} else {
		No_Access_Right_Pop_Up();
	}
	$PAGE_NAVIGATION[] = array($Lang["eForm"]["FormPreview"]);
} else {
	No_Access_Right_Pop_Up();
}

$htmlAry['backBtn'] = $indexVar['libeform_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');
?>