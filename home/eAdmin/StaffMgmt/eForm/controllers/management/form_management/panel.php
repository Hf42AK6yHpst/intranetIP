<?php
// editing by
/********************
 * Date : 2018-03-12 Frankie
 * Description :    Add Folder Structure to eForm
  ********************/
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/eForm/class.select_panel.php");

$selectPanel = new FormSelectPanel();


$yearID = (isset($_POST["yid"]) ? $_POST["yid"] : (isset($_GET["yid"]) ? $_GET["yid"] : ''));
$yearTermID = (isset($_POST["tid"]) ? $_POST["tid"] : (isset($_GET["tid"]) ? $_GET["tid"] : ''));

$infotype = (isset($_POST["infotype"]) ? $_POST["infotype"] : (isset($_GET["infotype"]) ? $_GET["infotype"] : ''));
$paneltype = (isset($_POST["paneltype"]) ? $_POST["paneltype"] : (isset($_GET["paneltype"]) ? $_GET["paneltype"] : ''));
$selected = (isset($_POST["selected"]) ? $_POST["selected"] : (isset($_GET["selected"]) ? $_GET["selected"] : ''));
$maxopt = (isset($_POST["maxopt"]) ? $_POST["maxopt"] : (isset($_GET["maxopt"]) ? $_GET["maxopt"] : '1'));

switch ($_GET["type"]) {
    case "studentElm":
        $recordType = 2;
        
        // set args
        $yearClassID = (isset($_POST["opt"]) ? $_POST["opt"] : (isset($_GET["opt"]) ? $_GET["opt"] : ''));
        
        $year_selection = $selectPanel->getYearSelectionPanel($yearID);
        
        $args = array( 'yearID' => $yearID, 'yearTermID' => $yearTermID, "selectedType" => $yearClassID );
        $type_selection = $selectPanel->getTypeSelectionPanel($recordType, $args);
        
        $year_label = $Lang["eForm"]["AcademicYear"];
        $option_label = $Lang["eForm"]["Class"];

        $args = array( "html" => true, 'subtype' => 'multiple', 'yearClassID' => $yearClassID, 'yearID' => $yearID, 'yearTermID' => $yearTermID, 'maxopt' => $maxopt);
        $args["subtype"] = (isset($_POST["subtype"]) ? $_POST["subtype"] : (isset($_GET["subtype"]) ? $_GET["subtype"] : ''));
        if ($args["subtype"] != "multiple") {
            $args["subtype"] = "multiple";
            $args["maxopt"] = 1;
            $maxopt = 1;
        }
        
        if ($infotype == "panelonly" ) {
            if ($paneltype == "class") {
                echo $type_selection;
                exit;
            } else {
                if (isset($selected)) {
                    $args["selected"] = $selected;
                }
                $selectionHTML = $selectPanel->getUserOptions($recordType, $args);
            }
            echo $selectionHTML;
            exit;
        }
        break;
    case "teacherElm":
        $recordType = 1;
        $option_label = $Lang["eForm"]["StaffType"];
        // set args
        $args = array( "selectedType" => 't1' );
        $selectedType = (isset($_POST["opt"]) ? $_POST["opt"] : (isset($_GET["opt"]) ? $_GET["opt"] : ''));
        if ($selectedType == "t2") {
            $args["selectedType"] = 't2';
        }
        
        $type_selection = $selectPanel->getTypeSelectionPanel($recordType, $args);
        // set args
        $args = array( "teaching" => '1', "html" => true, 'multiple' => 'single', 'yearID' => $yearID, 'yearTermID' => $yearTermID, 'maxopt' => $maxopt );
        
        if ($selectedType == "t2") {
            $args["teaching"] = '0';
        }
        $args["subtype"] = (isset($_POST["subtype"]) ? $_POST["subtype"] : (isset($_GET["subtype"]) ? $_GET["subtype"] : ''));
        if ($args["subtype"] != "multiple") {
            $args["subtype"] = "multiple";
            $args["maxopt"] = 1;
            $maxopt = 1;
        }
        
        if ($infotype == "panelonly" ) {
            if (isset($selected)) {
                $args["selected"] = $selected;
            }
            $selectionHTML = $selectPanel->getUserOptions($recordType, $args);
            echo $selectionHTML;
            exit;
        }
        break;

    case "subjectGroupElm":
        
        $year_selection = $selectPanel->getYearSelectionPanel($yearID);
        
        $args = array( 'yearID' => $yearID, 'yearTermID' => $yearTermID );
        $yearterm_selection = $selectPanel->getYearTermSelectionPanel($args);
        
        $type_selection = $selectPanel->getTypeSelectionPanel($recordType, $args);
        
        $year_label = $Lang["eForm"]["AcademicYear"];
        $yearterm_label = $Lang["eForm"]["YearTerm"];
        $option_label = $Lang["eForm"]["Subject"];

        $args["subjects"] = (isset($_POST["opt"]) ? $_POST["opt"] : (isset($_GET["opt"]) ? $_GET["opt"] : ''));
        
        $type_selection = $selectPanel->getSubjectSelectionPanel($args);
        
        $args = array( "html" => true, 'subtype' => 'single', 'yearID' => $yearID, 'yearTermID' => $yearTermID, 'maxopt' => $maxopt);
        if (isset($_POST["opt"])) {
            $args["subjectID"] = $_POST["opt"];
        }
        $args["subtype"] = (isset($_POST["subtype"]) ? $_POST["subtype"] : (isset($_GET["subtype"]) ? $_GET["subtype"] : ''));
        $args["subjectID"] = (isset($_POST["opt"]) ? $_POST["opt"] : (isset($_GET["opt"]) ? $_GET["opt"] : ''));
        
        if ($args["subtype"] != "multiple") {
            $args["subtype"] = "multiple";
            $args["maxopt"] = 1;
            $maxopt = 1;
        }
        
        if ($infotype == "panelonly" ) {
            if ($paneltype == "yearterm") {
                echo $yearterm_selection;
                exit;
            } else {
                if (isset($selected)) {
                    $args["selected"] = $selected;
                }
                $selectionHTML = $selectPanel->getSubjectGroupOptions($args);
                echo $selectionHTML;
                exit;
            }
        }
        break;
}

?>