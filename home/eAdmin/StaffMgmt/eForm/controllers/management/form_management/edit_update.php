<?php
// editing by
/********************
 * Date : 2018-03-12 Frankie
 * Description :    Add Folder Structure to eForm
 ********************/

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

$thisToken = $indexVar["FormInfo"]->getToken($_POST["FormID"] . $_POST["TemplateID"]);
if ($thisToken == $_POST["token"]) {
	$usedTotal = $indexVar["FormInfo"]->getTotalSubmitted(array($_POST["FormID"]), TRUE);
	$isUsed = false;
	if (isset($usedTotal[$_POST["FormID"]]["Total"]) && $usedTotal[$_POST["FormID"]]["Total"] > 0) {
		$isUsed = true;
	}
	$_POST["TemplateType"] = "Form";
	if ($isUsed) {
		$formInfo = $indexVar["FormInfo"]->getFormInfoById($_POST["FormID"]);
		if ($formInfo["TemplateID"] == $_POST["TemplateID"]) {
			$templateResult = array( "result" => true, "templateid" => $formInfo["TemplateID"]);
		} else {
			$templateResult = array( "result" => false );
		}
	} else {
		$templateResult = $indexVar['FormTemplate']->editFormTemplate($_POST["FormID"], $_POST, $isUsed);
	}
	if ($templateResult["result"]) {
		$_POST["TemplateID"] = $templateResult["templateid"];
		if (!empty($_POST["target"]) && count($_POST["target"]) > 0) {
			$targetUsers = $indexVar['libeform']->returnTargetUserIDArray(implode(",", $_POST["target"]));
			if (count($targetUsers) > 0) {
				$targetUsers = BuildMultiKeyAssoc($targetUsers, "UserID");
				$targetUsers = array_keys($targetUsers);
			} else {
				$targetUsers = array();
			}
		} else {
			$targetUsers = $indexVar['libeform']->returnTargetUserByType($_POST["type"]);
		}
		$infoResult = $indexVar['FormInfo']->editFormInformation($_POST, $targetUsers);
		if (count($infoResult["remUser"]) > 0 && $infoResult["fid"] > 0) {
			$indexVar["FormGrid"]->removeUserData($infoResult["fid"], $infoResult["remUser"]);
		}
		if (!$isUsed) {
			if ($infoResult["result"]) {
				$indexVar['FormTemplate']->FormRelation($infoResult["fid"], $templateResult["templateid"]);
			}
		}
	}
	if ($infoResult["result"]) {
		// echo "SUCCESS||?task=management/form_management/preview&fid=" . $infoResult["fid"] . "&ctk=" . $indexVar["FormInfo"]->getToken($infoResult["fid"]);
		echo "SUCCESS||?task=management/form_management/recordlist&fid=" . $infoResult["fid"] . "&ctk=" . $indexVar["FormInfo"]->getToken($infoResult["fid"]);
		exit;
	}
}
echo "FAIL";