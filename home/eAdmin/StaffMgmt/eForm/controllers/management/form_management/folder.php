<?php
// editing by
/********************
 * Date : 2018-03-12 Frankie
 * Description :    Add Folder Structure to eForm
 ********************/

/* AJAX check  */
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
    include_once($PATH_WRT_ROOT."includes/libdbtable.php");
    include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
    include_once($PATH_WRT_ROOT."includes/json.php");
    $json = new JSON_obj();
    
    $folderID = (isset($_GET["folder"]) && !empty($_GET["folder"])) ? $_GET["folder"] : '0';
    $folderType = (isset($_GET["type"]) && !empty($_GET["type"])) ? $_GET["type"] : 'init';
    $treeNode = array();
    if ($folderType == "init")
    {
        $treeNode = $indexVar['libeform']->getTreeNode($folderID);
        
    } else {
        $treeNode = $indexVar['libeform']->getChildNodes($folderID);
    }
    // echo "<pre>";
    // print_r($treeNode);
    // echo "</pre>";
    echo $json->encode($treeNode);
    exit;
}
header("HTTP/1.0 404 Not Found");
exit;


