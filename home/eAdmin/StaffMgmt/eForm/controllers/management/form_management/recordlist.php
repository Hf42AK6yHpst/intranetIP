<?php
// editing by
/********************
 * Date : 2018-03-12 Frankie
 * Description :    Add Folder Structure to eForm
 ********************/
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

if (isset($_POST['keyword'])) {
	$keyword = $_POST['keyword'];
}
$arrCookies = array();
$arrCookies[] = array("keyword");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}

# Page Title
$TAGS_OBJ[] = array($Lang['eForm']['FormManagement']);
$indexVar['libeform_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libeform_ui']->Include_Thickbox_JS_CSS();

// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================

$order = ($order == '') ? 0 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 0 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 50 : $numPerPage;


$thisToken = $indexVar['FormGrid']->getToken($_GET["fid"]);
if (!empty($_GET["fid"]) && $thisToken != $_GET["ctk"]) {
	No_Access_Right_Pop_Up();
}

$args = array(
		"page_size" => $page_size,
		"keyword" => $keyword,
		"FormID" => $_GET["fid"],
		"Completed" => true
	);
$li = new libdbtable2007($field, $order, $pageNo);

$FormID= $_GET["fid"];
$formInfo = $indexVar["FormInfo"]->getFormInfoById($_GET["fid"]);

$folderID = (isset($formInfo["FolderID"]) && !empty($formInfo["FolderID"])) ? $formInfo["FolderID"]: '0';
$folderBreadcrumb = $indexVar['libeform']->getBreadcrumb($folderID);
$htmlBreadcrumb = $indexVar['libeform_ui']->getBreadcrumbHTML($folderBreadcrumb);

$gridInfo = $indexVar['FormGrid']->getTableGridInfo($args);

if (isset($gridInfo["fields"]) && is_array($gridInfo["fields"])) $li->field_array = $gridInfo["fields"];
if (isset($gridInfo["sql"]) && !empty($gridInfo["sql"])) $li->sql = $gridInfo["sql"];
if (isset($gridInfo["order2"]) && !empty($gridInfo["order2"])) $li->fieldorder2 = $gridInfo["order2"];

$args = array(
		"sql" => $li->built_sql(),
		"girdInfo" => $gridInfo
	);
$li->custDataset = true;
$li->custDataSetArr = $indexVar['FormGrid']->getTableGridData($args);
$li->no_col = sizeof($li->field_array) + 2;
$li->IsColOff = "IP25_table";
$li->count_mode = 1;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$pos = 0;
$enlargeFieldArr = array( "UserName", "TemplateTitle", "FormTitle" );
if (count($li->field_array) > 0) {
	$colTotal = count($li->field_array) + 1;
	$eqWidth = floor(100 / $colTotal);
} else {
	$eqWidth = "";
}
foreach ($li->field_array as $kk => $vv) {
	$cust_str_eqWidth = "";
	if (!empty($eqWidth)) {
		if (!in_array($vv, $enlargeFieldArr)) {
			$cust_str_eqWidth = "width='" . $eqWidth . "%'";
		}
	}
	$li->column_list .= "<th " . $cust_str_eqWidth . ">".$li->column($pos++, $Lang["eForm"]["th_" . $vv])."</th>\n";
}
$li->column_list .= "<th width='1'>".$li->check("groupIdAry[]")."</th>\n";

$htmlAry['dataTable'] = $li->display();

$htmlAry['maxRow'] = $indexVar['libeform_ui']->GET_HIDDEN_INPUT('rowGroupID', 'rowGroupID', $a[0]["rowGroupID"]);

$x .= $indexVar['libeform_ui']->GET_HIDDEN_INPUT('pageNo', 'pageNo', $li->pageNo);
$x .= $indexVar['libeform_ui']->GET_HIDDEN_INPUT('order', 'order', $li->order);
$x .= $indexVar['libeform_ui']->GET_HIDDEN_INPUT('field', 'field', $li->field);
$x .= $indexVar['libeform_ui']->GET_HIDDEN_INPUT('page_size_change', 'page_size_change', '');
$x .= $indexVar['libeform_ui']->GET_HIDDEN_INPUT('numPerPage', 'numPerPage', $li->page_size);
$htmlAry['hiddenField'] = $x;
// ============================== Transactional data ==============================
// ============================== Define Button ==============================*/
$subBtnAry = array();
$btnAry[] = array('edit', '?task=management/form_management/edit&fid=' .$_GET["fid"]."&ctk=" . $indexVar["FormInfo"]->getToken($_GET["fid"]), $Lang["eForm"]["FormEdit"], $subBtnAry);
$btnAry[] = array('copy', '?task=management/form_management/preview&fid=' .$_GET["fid"]."&ctk=" . $indexVar["FormInfo"]->getToken($_GET["fid"]), $Lang["eForm"]["FormPreview"], $subBtnAry);
$btnAry[] = array('export', '?task=management/form_management/export&fid=' .$_GET["fid"]."&ctk=" . $indexVar["FormInfo"]->getToken($_GET["fid"] . "_csv"), $Lang["eForm"]["ExportData"], $subBtnAry);
$htmlAry['contentTool'] = $indexVar['libeform_ui']->Get_Content_Tool_By_Array_v30($btnAry);
$htmlAry['searchBox'] = $indexVar['libeform_ui']->Get_Search_Box_Div('keyword', $keyword);

$btnAry = array();
// $btnAry[] = array('edit', 'javascript: goEdit();');
$btnAry[] = array('delete', 'javascript: goDelete();', $Lang['eForm']['Delete']);
$htmlAry['dbTableActionBtn'] = $indexVar['libeform_ui']->Get_DBTable_Action_Button_IP25($btnAry);
// ============================== Define Button ==============================
$htmlAry['backBtn'] = $indexVar['libeform_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');
?>