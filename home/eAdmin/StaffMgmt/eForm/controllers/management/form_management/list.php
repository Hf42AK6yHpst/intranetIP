<?php
// editing by
/********************
 * Date : 2018-03-12 Frankie
 * Description :    Add Folder Structure to eForm
 ********************/
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

if (isset($_POST['keyword'])) {
	$keyword = $_POST['keyword'];
}
$arrCookies = array();
$arrCookies[] = array("keyword");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}

$folderID = (isset($_GET["folder"]) && !empty($_GET["folder"])) ? $_GET["folder"] : '0';
$folderInfo = $indexVar['libeform']->getNodeInfo($folderID);
if ($folderInfo == null) {
    $folderID = '0';
}

# Page Title
$TAGS_OBJ[] = array($Lang['eForm']['FormManagement']);
$indexVar['libeform_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libeform_ui']->Include_Thickbox_JS_CSS();

// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================

$folderBreadcrumb = $indexVar['libeform']->getBreadcrumb($folderID);
$htmlBreadcrumb = $indexVar['libeform_ui']->getBreadcrumbHTML($folderBreadcrumb);

$order = ($order == '') ? 0 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 5 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 50 : $numPerPage;

$args = array(
		"page_size" => $page_size,
		"keyword" => $keyword,
        "folderID" => $folderID
	);
$li = new libdbtable2007($field, $order, $pageNo);

$gridInfo = $indexVar['FormInfo']->getTableGridInfo($args);
if (isset($gridInfo["fields"]) && is_array($gridInfo["fields"])) $li->field_array = $gridInfo["fields"];
if (isset($gridInfo["sql"]) && !empty($gridInfo["sql"])) $li->sql = $gridInfo["sql"];
if (isset($gridInfo["order2"]) && !empty($gridInfo["order2"])) $li->fieldorder2 = $gridInfo["order2"];

$args = array(
		"sql" => $li->built_sql(),
		"girdInfo" => $gridInfo,
        "folderID" => $folderID
	);
$li->custDataset = true;
$li->custDataSetArr = $indexVar['FormInfo']->getTableGridData($args);

$li->no_col = sizeof($li->field_array) + 2;
$li->IsColOff = "IP25_table";
$li->count_mode = 1;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$pos = 0;
if (count($li->field_array) > 0) {
	$colTotal = count($li->field_array) + 1;
	$eqWidth = floor(100 / $colTotal);
} else {
	$eqWidth = "";
}
foreach ($li->field_array as $kk => $vv) {
	if (!empty($eqWidth)) {
		if ($vv == "FormTitle") {
			$cust_str_eqWidth = "";
		} else {
			$cust_str_eqWidth = "width='" . $eqWidth . "%'";
		}
	}
	$li->column_list .= "<th " . $cust_str_eqWidth . ">".$li->column($pos++, $Lang["eForm"]["th_" . $vv])."</th>\n";
}
$li->column_list .= "<th width='1'>".$li->check("groupIdAry[]")."</th>\n";
$htmlAry['dataTable'] = $li->display();
$htmlAry['maxRow'] = $indexVar['libeform_ui']->GET_HIDDEN_INPUT('rowGroupID', 'rowGroupID', $a[0]["rowGroupID"]);

$x .= $indexVar['libeform_ui']->GET_HIDDEN_INPUT('pageNo', 'pageNo', $li->pageNo);
$x .= $indexVar['libeform_ui']->GET_HIDDEN_INPUT('order', 'order', $li->order);
$x .= $indexVar['libeform_ui']->GET_HIDDEN_INPUT('field', 'field', $li->field);
$x .= $indexVar['libeform_ui']->GET_HIDDEN_INPUT('page_size_change', 'page_size_change', '');
$x .= $indexVar['libeform_ui']->GET_HIDDEN_INPUT('numPerPage', 'numPerPage', $li->page_size);
$htmlAry['hiddenField'] = $x;
// ============================== Transactional data ==============================
// ============================== Define Button ==============================*/
$subBtnAry = array();
$btnAry[] = array('new', 'javascript: goNewForm();', '', $subBtnAry);

$htmlAry['contentTool'] = $indexVar['libeform_ui']->Get_Content_Tool_By_Array_v30($btnAry);
$htmlAry['searchBox'] = $indexVar['libeform_ui']->Get_Search_Box_Div('keyword', $keyword);

$btnAry = array();
// $btnAry[] = array('edit', 'javascript: goEdit();');
$btnAry[] = array('delete', 'javascript: goDelete();', $Lang['eForm']['Delete']);
$htmlAry['dbTableActionBtn'] = $indexVar['libeform_ui']->Get_DBTable_Action_Button_IP25($btnAry);
// ============================== Define Button ==============================
?>