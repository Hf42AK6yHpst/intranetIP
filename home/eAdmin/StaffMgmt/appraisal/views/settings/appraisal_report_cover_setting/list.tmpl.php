<div class="table_board">
	<div>
		<?=$htmlAry['contentTbl']?>
	</div>
	<br style="clear:both;" />
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['saveBtn']?>
		<?=$htmlAry['previewBtn']?>
	</div>
</div>


<script type="text/javascript">
$(document).ready( function() { 
	$('.checkBox').click(function() {
		if($(this).is(':checked')) {
			$(this).val(1);
		}else{
			$(this).val(0);
		}
	});
	$('#schoolEmblem_photo').change(function(){
		var url = window.URL.createObjectURL(window.cover_form.schoolEmblem_photo.files[0]);
		$('#photo_preview').attr('src',url);
	});
});

function goSave() {
	//CoverPageSettingValues Array
	var errorPass = true;
	var coverPageSettingValues = {};
	coverPageSettingValues["Language"] = $("input[name='language']:checked").val();
	coverPageSettingValues["SchoolName"] = $("#schoolName").val();
	coverPageSettingValues["GroupName"] = $("#groupName").val();
	coverPageSettingValues["SchoolEmblem"] = $("#schoolEmblem").val();
	coverPageSettingValues["SchoolEmblemPhoto"] = $("#SchoolEmblemPhoto").val();
	
	coverPageSettingValues["SchoolYear"] = $("#schoolYear").val();
	coverPageSettingValues["ReportName"] = $("#reportName").val();
	coverPageSettingValues["ReportNameText"] = $("#reportNameText").val();
	coverPageSettingValues["TeacherChineseName"] = $("#teacherChineseName").val();

	coverPageSettingValues["TeacherEnglishName"] = $("#teacherEnglishName").val();
	coverPageSettingValues["SchoolMotto"] = $('#schoolMotto').val();
	coverPageSettingValues["SchoolMottoText"] = $("#schoolMottoText").val();
	coverPageSettingValues["AllAppRole"] = $("#allAppRole").val();
	//END
	coverPageSettingValues = JSON.stringify(coverPageSettingValues);

	if($("input[name='language']:checked").val() == "Zh" && $("#schoolMottoText").val().length > 50){
			alert("<?=$Lang['Appraisal']['Settings']['SchoolMottoLimitation']?>");
			errorPass = false;
	}
	if(errorPass == true){
	   	var coverPageSetting = $("<input/>").attr("type", "hidden").attr("name", "coverPageSettingValues").val(coverPageSettingValues);
		var returnPathVal= 'Location: ?task=settings<?=$appraisalConfig['taskSeparator']?>appraisal_report_cover_setting<?=$appraisalConfig['taskSeparator']?>list';	
	   	var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
	   	$('form#cover_form').append($(returnPath)).append($(coverPageSetting));
	   	$('form#cover_form').attr('action', '?task=settings<?=$appraisalConfig['taskSeparator']?>appraisal_report_cover_setting<?=$appraisalConfig['taskSeparator']?>edit_update').submit();
	}
}

function goPreview() {	
	params  = 'width='+screen.width;
	params += ', height='+screen.height;
	params += ', top=0, left=0';
	params += ', fullscreen=yes';
	var coverPageSetting = $("<input/>").attr("type", "hidden").attr("name", "coverPageSettingValues").val(coverPageSettingValues);

	//CoverPageSettingValues Array
	var errorPass = true;
	var coverPageSettingValues = {};
	coverPageSettingValues["Language"] = $("input[name='language']:checked").val();
	coverPageSettingValues["SchoolName"] = $("#schoolName").val();
	coverPageSettingValues["GroupName"] = $("#groupName").val();
	coverPageSettingValues["SchoolEmblem"] = $("#schoolEmblem").val();
	coverPageSettingValues["SchoolEmblemPhoto"] = $("#SchoolEmblemPhoto").val();
	if($("#SchoolEmblemPhotoOriginal").length > 0){
		coverPageSettingValues["SchoolEmblemPhotoOriginal"] = $("#SchoolEmblemPhotoOriginal").val();
	}
	
	coverPageSettingValues["SchoolYear"] = $("#schoolYear").val();
	coverPageSettingValues["ReportName"] = $("#reportName").val();
	coverPageSettingValues["ReportNameText"] = $("#reportNameText").val();
	coverPageSettingValues["TeacherChineseName"] = $("#teacherChineseName").val();

	coverPageSettingValues["TeacherEnglishName"] = $("#teacherEnglishName").val();
	coverPageSettingValues["SchoolMotto"] = $('#schoolMotto').val();
	coverPageSettingValues["SchoolMottoText"] = $("#schoolMottoText").val();
	coverPageSettingValues["AllAppRole"] = $("#allAppRole").val();
	//END
	coverPageSettingValues = JSON.stringify(coverPageSettingValues);

	if($("input[name='language']:checked").val() == "Zh" && $("#schoolMottoText").val().length > 50){
			alert("<?=$Lang['Appraisal']['Settings']['SchoolMottoLimitation']?>");
			errorPass = false;
	}
	if(errorPass == true){
		if($('#coverPageSettingValues').length == 1){
			$('#coverPageSettingValues').val(coverPageSettingValues);
		}else{
	   		var coverPageSetting = $("<input/>").attr("type", "hidden").attr("id", "coverPageSettingValues").attr("name", "coverPageSettingValues").val(coverPageSettingValues);
	   		$('form#cover_form').append($(coverPageSetting));
		}  
		window.open("/home/eAdmin/StaffMgmt/appraisal/controllers/settings/appraisal_report_cover_setting/preview.php","preview_window", params);
	}
}

</script>