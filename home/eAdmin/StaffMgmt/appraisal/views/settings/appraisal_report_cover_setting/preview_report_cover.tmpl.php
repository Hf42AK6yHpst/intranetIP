﻿<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title></title>
</head>
<style type="text/css">
	/*PRINT*/
	@media print {
		html, body {width:21cm;}
		.page-break	{display:block; page-break-before:always;}
	}
	@page {size:A4 portrait; margin:0 0 0 0;}
	.page-wrapper {width:19.6cm; height:28cm; margin:0 auto; border:1px solid #fff;}
	.page-content {margin:.5cm 2cm 1cm 2cm; height:27.7cm;}
	@-moz-document url-prefix() {.page-wrapper {height:27cm;} .page-content {height:26cm;}}
	@media screen and (-ms-high-contrast:active), (-ms-high-contrast:none) { /*for IE*/
	   .page-wrapper {height:27cm;}
	   .page-content {height:26cm;}
	}
	table {table-layout: fixed;} /*for IE*/
	/*GENERAL*/
	body, html {background-color:#fff; color:#000; font-family:Verdana, Arial, '新細明體', sans-serif; font-size:12px; margin:0; padding:0; -webkit-print-color-adjust:exact; text-align:center;}
	/*CONTENT*/
	table.main {height:100%; width:100%;}
	table.main td {vertical-align:middle; text-align:center;}
	table.main .box-lower td {vertical-align:bottom;}
	.box-upper {height:4.5cm; max-height:4.5cm;}
	.box-logo {height:9cm;}
	.box-middle {max-height:12cm;}
	.org-name {font-size:2rem; margin-bottom:1rem;}
	.sch-name {font-size:4rem;}
	.sch-logo img {max-width:100%; height:7cm; max-height:8cm;}
	.sch-year {font-size:3rem; margin-bottom:1rem;}
	.report-name {font-size:4rem; margin-bottom:3rem;}
	.tea-name {font-size:3rem;}
	.sch-motto {font-size:1.8rem; margin-top:1rem;}
	.roles {font-size:1.3rem; width:100%; margin-top:2rem;}
</style>

<body>
<!--start content-->
<div id="content">
	<div class="page-wrapper">
		<div class="page-content">
			<?=$htmlAry['contentTbl']?>
		</div>
	</div>
	<div class="page-break"></div>
</div>
<!--end content-->
</body>
</html>