<form id="form1" name="form1" method="post" action="index.php">

	<?=$htmlAry['navigation']?>
	<br/>
	<?=$htmlAry['customizedImportStepTbl']?>	
	
	<div class="table_board">
		<div>
			<?=$htmlAry['contentTbl']?>
		</div>
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['previewBtn']?>
			<?=$htmlAry['submitNewBtn']?>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['resetBtn']?>
			<?=$htmlAry['backBtn']?>
			<p class="spacer"></p>
		</div>
	</div>
</form>

<script type="text/javascript">
$(document).ready( function() {
	
});
function goPreview(templateID){
	window.open('?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>gen_form&templateID='+templateID,'_blank');
	//window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>gen_form&templateID='+templateID;
}
function goSubmitNew(){
	var status=formValidation();
	if (status== true){
		var templateID=$("#TemplateID").val();
		var parentSecID=$("#ParentSecID").val();
		var secID=$("#SecID").val();
		var qCatID=$("#QCatID").val();
		var qID=$("#QID").val();
		var qAnsID=($("#QAnsID").val()=="NULL")?"%s":$("#QAnsID").val();
		var returnPathVal='Location: ?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_qAns&templateID='+templateID+"&parSecID="+parentSecID+"&secID="+secID+"&qCatID="+qCatID+"&qID="+qID+"&qAnsID=";
		var typeVal="SDFQAnsSave";		
		var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
		var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
		$('#form1').append($(type)).append($(returnPath));
		$('form#form1').attr('action', 'index.php?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_update').submit();	
	}
}
function goSubmit(){	
	var status=formValidation();
	if (status== true){
		var templateID=$("#TemplateID").val();
		var parentSecID=$("#ParentSecID").val();
		var secID=$("#SecID").val();
		var qCatID=$("#QCatID").val();
		var qID=$("#QID").val();
		var qAnsID=($("#QAnsID").val()=="NULL")?"%s":$("#QAnsID").val();
		var returnPathVal='Location: ?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_qAns&templateID='+templateID+"&parSecID="+parentSecID+"&secID="+secID+"&qCatID="+qCatID+"&qID="+qID+"&qAnsID="+qAnsID;
		var typeVal="SDFQAnsSave";		
		var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
		var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
		$('#form1').append($(type)).append($(returnPath));
		$('form#form1').attr('action', 'index.php?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_update').submit();	
	}
}
function formValidation(){
	return true;
}
function goReset(){
	$("#form1")[0].reset();
}
function goBack(){
	window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>list';
}
function goBackTemplate(templateID){
	window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf&templateID='+templateID;
}
function goBackParSec(templateID,secID){
	window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_sec&templateID='+templateID+"&secID="+secID;
}
function goBackSubSec(templateID,parSecID,secID){
	window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_subSec&templateID='+templateID+"&parSecID="+parSecID+"&secID="+secID;
}
function goBackQCat(templateID,parSecID,secID,qCatID){
	window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_qCat&templateID='+templateID+"&parSecID="+parSecID+"&secID="+secID+"&qCatID="+qCatID;
}
function goBackQues(templateID,parSecID,secID,qCatID,qID){
	window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_ques&templateID='+templateID+"&parSecID="+parSecID+"&secID="+secID+"&qCatID="+qCatID+"&qID="+qID;
}

$("#HasRmk").click(function(){
	if($('#HasRmk').is(':checked')){
		$("#RmkDescrEng").removeAttr("readonly");
		$("#RmkDescrChi").removeAttr("readonly");
	}
	else{
		$("#RmkDescrEng").attr("readonly","readonly");
		$("#RmkDescrChi").attr("readonly","readonly");
		$("#RmkDescrEng").val("");
		$("#RmkDescrChi").val("");
	}			
});
</script>