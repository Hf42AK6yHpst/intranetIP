<form id="form1" name="form1" method="post" action="index.php">

	<?=$htmlAry['navigation']?>
	<br/>
	<?=$htmlAry['customizedImportStepTbl']?>
	
	
	<div class="table_board">
		<div>
			<?=$htmlAry['contentTbl']?>
		</div>
		<br style="clear:both;" />		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>			
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['resetBtn']?>
			<?=$htmlAry['backBtn']?>
			<p class="spacer"></p>
		</div>	
		<div class="content_top_tool">
			<?=$htmlAry['contentTool']?>			
			<br style="clear:both;">
		</div>	
		<div>
			<?=$htmlAry['contentTbl2']?>
		</div>
		<?=$htmlAry['previewBtn']?>
	</div>
</form>

<script type="text/javascript">
$(document).ready( function() {
	initDndTable();
});

function initDndTable() {
	$("table#dndTable").tableDnD({
		onDrop: function(table, row) {
	        var rows = table.tBodies[0].rows;
	        var recordOrder = "";
	        var displayOrder = "";
	        for (var i=0; i<rows.length; i++) {
		        var j=i+1;
	        	if (rows[i].id != ""){
	        		recordOrder += rows[i].id+",";
        			displayOrder += j+",";
	        	}
	        }	        
	        //alert(recordOrder+" "+displayOrder);
	        reorderAjax = GetXmlHttpObject();
			  if (reorderAjax == null)
			  {
			    alert (errAjax);
			    return;
			  } 
			  var templateID = $("#TemplateID").val();
			  var ElementObj = $(this);
			  var url = 'index.php?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>ajax_edit_update';
			  var postContent = "recordOrder="+encodeURIComponent(recordOrder)+"&displayOrder="+encodeURIComponent(displayOrder)+"&TemplateID="+templateID+"&Type=Section";				
				reorderAjax.onreadystatechange = function() {
					if (reorderAjax.readyState == 4) {
						//Get_Form_Class_List();
					}
				};
				reorderAjax.open("POST", url, true);
				reorderAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
				reorderAjax.send(postContent);
		},
		onDragStart: function(table, DraggedRow) {
			//$('#debugArea').html("Started dragging row "+row.id);	
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
}
function goPreview(templateID){
	window.open('?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>gen_form&templateID='+templateID,'_blank');
	//window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>gen_form&templateID='+templateID;
}
function goSubmit(){	
	var status=formValidation();
	if (status== true){
		var templateID=($("#TemplateID").val()=="NULL")?"%s":$("#TemplateID").val();
		var returnPathVal='Location: ?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf&templateID='+templateID;		
		var typeVal="SDFSave";
		var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
		/*var needClassVal = ($("input[name='NeedClass']:checked").val()!=undefined)?$("input[name='NeedClass']:checked").val():0;
		var needClass = $("<input/>").attr("type", "hidden").attr("name", "NeedClass").val(needClassVal);
		var needSubjectVal = ($("input[name='NeedSubject']:checked").val()!=undefined)?$("input[name='NeedSubject']:checked").val():0;
		var needSubject = $("<input/>").attr("type", "hidden").attr("name", "NeedSubject").val(needSubjectVal);		
		*/var statusVal = $("input[name='Status']:checked").val();
		var status = $("<input/>").attr("type", "hidden").attr("name", "Status").val(statusVal);
		var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
		$('#form1').append($(type)).append($(status)).append($(returnPath));//.append($(needClass)).append($(needSubject));
		$('form#form1').attr('action', 'index.php?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_update').submit();	
	}
}
function formValidation(){
	var status = true;
	$("#FrmTitleEngEmptyWarnDiv,#FrmTitleChiEmptyWarnDiv,#StatusEmptyWarnDiv").hide();
	
	if($("#FrmTitleEng").val()==""){
		$("#FrmTitleEngEmptyWarnDiv").show();
		status = false;
	}
	else if($("#FrmTitleChi").val()==""){
		$("#FrmTitleChiEmptyWarnDiv").show();
		status = false;
	}
	else if($("input[name='Status']").is(":checked")==false){
		$("#StatusEmptyWarnDiv").show();
		status = false;
	}	
	/*else if($("input[name='NeedClass']").is(":checked")==false){
		$("#NeedClassEmptyWarnDiv").show();
		status = false;
	}
	else if($("input[name='NeedSubject']").is(":checked")==false){
		$("#NeedSubjectEmptyWarnDiv").show();
		status = false;
	}*/
	if($('#HasSelected').val()==1 && $("input[name='Status']:checked").val()==0){
		alert('<?=$Lang['Appraisal']['Message']['ErrorInactive']?>');
		status = false;
		$('#active').click();
	}
	return status;
}
function goReset(){
	$("#form1")[0].reset();
}
function goEditParSec(templateID,secID){
	window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_sec&templateID='+templateID+'&secID='+secID;
}
function goBack(){
	window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>list';
}
/*function goDelete(secID){
	var typeVal="SDFDelete";
	var type = $("<input/>").attr("type", "hidden").attr("name", "Type").val(typeVal);
	var secID= $("<input/>").attr("type", "hidden").attr("name", "Status").val(secID);
	$('#form1').append($(type)).append($(secID));
	$('form#form1').attr('action', 'index.php?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>delete').submit();	
}*/
function goAddParSec(templateID){
	window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_sec&templateID='+templateID+'&secID=';
}
function goDelete(templateIDVal,secIDVal){
	var templateID=$("#TemplateID").val();
	var secID=$("#SecID").val();
	var returnPathVal='Location: ?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf&templateID='+templateID;
	var typeVal="SDFParentSecDelete";		
	var templateID = $("<input/>").attr("type", "hidden").attr("name", "templateID").val(templateIDVal);
	var secID = $("<input/>").attr("type", "hidden").attr("name", "parentSecID").val(secIDVal);
	var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
	var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
	$('#form1').append($(type)).append($(returnPath)).append($(templateID)).append($(secID));
	$('form#form1').attr('action', 'index.php?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>delete').submit();	
}

function addPageBreak(templateIDVal,secIDVal){
	var templateID=templateIDVal;
	var secID=secIDVal;
	var setPageBreak = $('#PageBreak_'+secID+':checked').length;
	var typeVal="SDFParentSecAddPageBreak";
	var actionPath = "index.php?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>ajax_edit_update";
	$.post(
		actionPath,
		{
			'TemplateID': templateID,
			'ParentSecID': secID,
			'Type': typeVal,
			'SetPageBreak': setPageBreak
		}
	);
}

function execLang(){
	var needSbj = $('#NeedSubject:checked').val();
	if(needSbj == 1){
		$('#NeedSubjectLangRow').css('display','table-row');
	}else{
		$('#NeedSubjectLangRow').css('display','none');
	}
}
</script>