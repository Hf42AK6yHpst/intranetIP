<form id="form1" name="form1" method="post" action="index.php">
	<?=$htmlAry['navigation']?>
	<br/>
	<div class="table_board">
		<div>
			<?=$htmlAry['contentTbl']?>
		</div>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['previewBtn']?>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['resetBtn']?>
			<?=$htmlAry['backBtn']?>
			<p class="spacer"></p>
		</div>	
	</div>
</form>

<script type="text/javascript">
$(document).ready( function() {
	initDndTable();
});

function initDndTable() {
	$("table#dndTable").tableDnD({
		onDrop: function(table, row) {
	        var rows = table.tBodies[0].rows;
	        var recordOrder = "";
	        var displayOrder = "";
	        for (var i=0; i<rows.length; i++) {
		        var j=i+1;
	        	if (rows[i].id != ""){
	        		recordOrder += rows[i].id+",";
        			displayOrder += j+",";
	        	}
	        }	        
	        //alert(recordOrder+" "+displayOrder);
	        reorderAjax = GetXmlHttpObject();
			  if (reorderAjax == null)
			  {
			    alert (errAjax);
			    return;
			  } 
			  var templateID = $("#TemplateID").val();
			  var ElementObj = $(this);
			  var url = 'index.php?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>ajax_edit_update';
			  var postContent = "recordOrder="+encodeURIComponent(recordOrder)+"&displayOrder="+encodeURIComponent(displayOrder)+"&TemplateID="+templateID+"&Type=Template";				
				reorderAjax.onreadystatechange = function() {
					if (reorderAjax.readyState == 4) {
						//Get_Form_Class_List();
					}
				};
				reorderAjax.open("POST", url, true);
				reorderAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
				reorderAjax.send(postContent);
		},
		onDragStart: function(table, DraggedRow) {
			//$('#debugArea').html("Started dragging row "+row.id);	
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});	
}

function loadSubTable(){
	$('.subTables').each(function(index){
		var dataTypes = $(this).attr('dataType');
		$(this).tableDnD({
			onDrop: function(table, row) {
		        var rows = table.tBodies[0].rows;
		        var recordOrder = "";
		        var displayOrder = "";
		        for (var i=0; i<rows.length; i++) {
			        var j=i+1;
		        	if (rows[i].id != ""){
		        		recordOrder += rows[i].id+",";
	        			displayOrder += j+",";
		        	}
		        }	        
		        //alert(recordOrder+" "+displayOrder);
		        reorderAjax = GetXmlHttpObject();
				  if (reorderAjax == null)
				  {
				    alert (errAjax);
				    return;
				  } 
				  var templateID = $("#TemplateID").val();
				  var ElementObj = $(this);
				  var url = 'index.php?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>ajax_edit_update';
				  var postContent = "recordOrder="+encodeURIComponent(recordOrder)+"&displayOrder="+encodeURIComponent(displayOrder)+"&TemplateID="+templateID+"&Type=TemplateSubData";				
					reorderAjax.onreadystatechange = function() {
						if (reorderAjax.readyState == 4) {
							//Get_Form_Class_List();
						}
					};
					reorderAjax.open("POST", url, true);
					reorderAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
					reorderAjax.send(postContent);
			},
			onDragStart: function(table, DraggedRow) {
				//$('#debugArea').html("Started dragging row "+row.id);	
			},
			dragHandle: "SubDraggable_"+dataTypes, 
			onDragClass: "move_selected"
		});
	});
}

function goPreview(templateID){
	window.open('?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>gen_form&templateID='+templateID,'_blank');
	//window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>gen_form&templateID='+templateID;
}
function goSubmit(){	
	var status=formValidation();
	if (status== true){
		var templateID = $("#TemplateID").val();
		var returnPathVal='Location: ?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit&templateID='+templateID;
		var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);	
		var typeVal="PDFSave";
		var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
		var statusVal = $("input[name='Status']:checked").val();
		var status = $("<input/>").attr("type", "hidden").attr("name", "Status").val(statusVal);
		$('#form1').append($(type)).append($(status)).append($(returnPath));
		$('form#form1').attr('action', 'index.php?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_update').submit();	
	}
}
function formValidation(){
	var status = true;
	if($('#HasSelected').val()==1 && $("input[name='Status']:checked").val()==0){
		alert('<?=$Lang['Appraisal']['Message']['ErrorInactive']?>');
		status = false;
		$('#active').click();
	}
	return status;
}
function goReset(){
	$("#form1")[0].reset();
}
function goBack(){
	window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>list';
}
$("#includeAll").click(function(){
	if ($(this).attr("checked")) {
		$("input[id^=Include_]").attr("checked",true);
	}
	else{
		$("input[id^=Include_]").attr("checked",false);
	}

});

function submitSubSection(){
	var templateID = $("#TemplateID").val();
	var postArr = $('#subSectionForm').serializeArray();
	postArr[postArr.length] = {"name":"Type", "value":"PDFSubSectionSave"};
	postArr[postArr.length] = {"name":"TemplateID", "value":templateID};
	$.post(
		'index.php?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>ajax_edit_update',
		postArr,
		function(){				
			js_Hide_ThickBox();
		}
	);
}

function getEditContent(dataType){
	$.post(
		'index.php?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>ajax_getData',
		{
			"type": "getSubContentList",
			"dataType": dataType,
			"templateID": $("#TemplateID").val()
		},
		function(returnData){
			$('#TB_ajaxContent').html(returnData);
			loadSubTable();
		}
	);
}

function subSectionSelectAll(){
	if(	$('#subSectionIncludeAll:checked').length > 0 ){
		$('.subSectionInclude').attr('checked', true);
	}else{
		$('.subSectionInclude').attr('checked', false);
	}
}

function getOrientation(dataType){
	$.post(
		'index.php?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>ajax_getData',
		{
			"type": "getOrientaiton",
			"dataType": dataType,
			"templateID": $("#TemplateID").val()
		},
		function(returnData){
			$('#TB_ajaxContent').html(returnData);
			loadSubTable();
		}
	);
}

function submitAttendanceOrientation(){
	var templateID = $("#TemplateID").val();
	var postArr = $('#subSectionForm').serializeArray();
	postArr[postArr.length] = {"name":"Type", "value":"PDFAttendanceTableOrientation"};
	postArr[postArr.length] = {"name":"TemplateID", "value":templateID};
	$.post(
		'index.php?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>ajax_edit_update',
		postArr,
		function(){				
			js_Hide_ThickBox();
		}
	);
}
</script>