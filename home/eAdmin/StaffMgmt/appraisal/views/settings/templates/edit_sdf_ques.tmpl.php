<form id="form1" name="form1" method="post" action="index.php">

	<?=$htmlAry['navigation']?>
	<br/>
	<?=$htmlAry['customizedImportStepTbl']?>	
	
	<div class="table_board">
		<div>
			<?=$htmlAry['contentTbl']?>
		</div>
		<br style="clear:both;" />		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['previewBtn1']?>
			<?=$htmlAry['submitNewBtn']?>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['resetBtn']?>
			<?=$htmlAry['backBtn']?>
			<p class="spacer"></p>
		</div>
		<div>
			<?=$htmlAry['contentTbl2']?>
		</div>
		<?=$htmlAry['previewBtn2']?>
	</div>
</form>

<script type="text/javascript">
$(document).ready( function() {
	initDndTable();
	var inputType = $("input[name^='InputType']:checked").val();
	if(inputType != 1 && inputType != 6){
		$("#divMCAns").hide();
	}
	else{
		$("#divMCAns").show();
	}
	if(inputType==3){
		$("#trAvgMark,#trAvgDec").show();
	}
	else{
		$("#trAvgMark,#trAvgDec").hide();
	}	
	if($("#HasRmk").attr("checked") == true){
		$("#RmkDescrEngTr, #RmkDescrChiTr").show();
	}
	else{
		$("#RmkDescrEngTr, #RmkDescrChiTr").hide();
	}
	var inputTypeID =  $("Input:radio[name='InputType']:checked").attr("id");
	if( inputTypeID == "InputTypeMultiText"){
		$("#RmkDescCbTr").hide();
		$("#RmkDescrEngTr").hide();
		$("#RmkDescrChiTr").hide();
	}
	if( inputTypeID == "InputTypeText" || inputTypeID == "InputTypeScore"){
		$("#TxtboxNoteTr").show();
	}
	else{
		$("#TxtboxNoteTr").hide();
	}
});
function initDndTable() {
	$("table#dndTable").tableDnD({
		onDrop: function(table, row) {
	        var rows = table.tBodies[0].rows;
	        var recordOrder = "";
	        var displayOrder = "";
	        for (var i=0; i<rows.length; i++) {
		        var j=i+1;
	        	if (rows[i].id != ""){
	        		recordOrder += rows[i].id+",";
        			displayOrder += j+",";
	        	}
	        }	        
	        //alert(recordOrder+" "+displayOrder);
	        reorderAjax = GetXmlHttpObject();
			  if (reorderAjax == null)
			  {
			    alert (errAjax);
			    return;
			  } 
			  var templateID = $("#TemplateID").val();
			  var secID = $("#SecID").val();
			  var qCatID = $("#QCatID").val();
			  var qID = $("#QID").val();
			  var qAnsID = $("#QAnsID").val();
			  var inputType = "QAns";
			  var ElementObj = $(this);
			  var url = 'index.php?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>ajax_edit_update';
			  var postContent = "recordOrder="+encodeURIComponent(recordOrder)+"&displayOrder="+encodeURIComponent(displayOrder)+"&TemplateID="+templateID+"&SecID="+secID+"&QCatID="+qCatID+"&QID="+qID+"&QAnsID="+qAnsID+"&Type="+inputType;
				reorderAjax.onreadystatechange = function() {
					if (reorderAjax.readyState == 4) {
						//Get_Form_Class_List();
					}
				};
				reorderAjax.open("POST", url, true);
				reorderAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
				reorderAjax.send(postContent);
		},
		onDragStart: function(table, DraggedRow) {
			//$('#debugArea').html("Started dragging row "+row.id);	
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
}
function goPreview(templateID){
	window.open('?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>gen_form&templateID='+templateID,'_blank');
	//window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>gen_form&templateID='+templateID;
}
function goSubmitNew(){	
	var status=formValidation();
	if (status== true){
		var templateID=$("#TemplateID").val();
		var parentSecID=$("#ParentSecID").val();
		var secID=$("#SecID").val();
		var qCatID=$("#QCatID").val();
		var qID=($("#QID").val()=="NULL")?"%s":$("#QID").val();		
		
		var returnPathVal='Location: ?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_ques&templateID='+templateID+"&parSecID="+parentSecID+"&secID="+secID+"&qCatID="+qCatID+"&qID=";
		var typeVal="SDFQuesSave";		
		var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
		var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
		$('#form1').append($(type)).append($(returnPath));
		$('form#form1').attr('action', 'index.php?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_update').submit();	
	}
}
function goSubmit(){	
	var status=formValidation();
	if (status== true){
		var templateID=$("#TemplateID").val();
		var parentSecID=$("#ParentSecID").val();
		var secID=$("#SecID").val();
		var qCatID=$("#QCatID").val();
		var qID=($("#QID").val()=="NULL")?"%s":$("#QID").val();		
		var returnPathVal='Location: ?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_ques&templateID='+templateID+"&parSecID="+parentSecID+"&secID="+secID+"&qCatID="+qCatID+"&qID="+qID;
		var typeVal="SDFQuesSave";		
		var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
		var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
		$('#form1').append($(type)).append($(returnPath));
		$('form#form1').attr('action', 'index.php?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_update').submit();	
	}
}
function formValidation(){
	var status = true;
	avgDec = $("#AvgDec").val();
	$("#AvgDecEmptyWarnDiv").hide();
	var quesDispMode = $("#QuesDispMode").val();
	if(quesDispMode==0){
		if(isNumeric(avgDec) == false){
			$("AvgDecEmptyWarnDiv").val('<?=$Lang['Appraisal']['TemplateSample']['Number']?>');
			$("#AvgDecEmptyWarnDiv").show();
			status = false;
		}
		else if(avgDec<0 || avgDec > 3){
			$("AvgDecEmptyWarnDiv").val('<?=$Lang['Appraisal']['TemplateSample']['DecRng03']?>');
			$("#AvgDecEmptyWarnDiv").show();
			status = false;
		}
	}
	return status;
}
function isNumeric(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}
function goReset(){
	$("#form1")[0].reset();
}
function goBack(){
	window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>list';
}
function goBackTemplate(templateID){
	window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf&templateID='+templateID;
}
function goBackParSec(templateID,secID){
	window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_sec&templateID='+templateID+"&secID="+secID;
}
function goBackSubSec(templateID,parSecID,secID){
	window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_subSec&templateID='+templateID+"&parSecID="+parSecID+"&secID="+secID;
}
function goBackQCat(templateID,parSecID,secID,qCatID){
	window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_qCat&templateID='+templateID+"&parSecID="+parSecID+"&secID="+secID+"&qCatID="+qCatID;
}
$("#HasRmk").click(function(){
	if($('#HasRmk').is(':checked')){
		$("#RmkDescrEng").removeAttr("readonly");
		$("#RmkDescrChi").removeAttr("readonly");
	}
	else{
		$("#RmkDescrEng").attr("readonly","readonly");
		$("#RmkDescrChi").attr("readonly","readonly");
		$("#RmkDescrEng").val("");
		$("#RmkDescrChi").val("");
	}			
});
function goAddQAns(templateID,parSecID,secID,qCatID,qID){
	window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_qAns&templateID='+templateID+"&parSecID="+parSecID+"&secID="+secID+"&qCatID="+qCatID+"&qID="+qID+"&qAnsID=";
}
function goEditQAns(templateID,parSecID,secID,qCatID,qID,qAnsID){
	window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_qAns&templateID='+templateID+"&parSecID="+parSecID+"&secID="+secID+"&qCatID="+qCatID+"&qID="+qID+"&qAnsID="+qAnsID;
}
$("input[id^='InputType']").change(function(e){
	var id = e.target.id;
	if(id!="InputTypeMC" && id!="InputTypeMCM"){
		$("#divMCAns").hide();
	}
	else if(id=="InputTypeMC" || id=="InputTypeMCM"){
		$("#divMCAns").show();
	}
	if(id=="InputTypeMultiText"){
		$("#RmkDescCbTr").hide();
		$("#RmkDescrEngTr").hide();
		$("#RmkDescrChiTr").hide();

		$("#RmkLabelEng").text("");
		$("#RmkLabelChi").text("");
		$("#HasRmk").attr("checked","checked");
	}
	else{
		$("#HasRmk").attr("checked",false);
		$("#RmkDescCbTr").show();
		$("#RmkDescrEngTr, #RmkDescrChiTr").hide();		
	}
	if(id=="InputTypeText" || id=="InputTypeScore"){
		$("#TxtboxNoteTr").show();
	}
	else{
		$("#TxtboxNoteTr").hide();
		$("#TxtBoxNote").val("");
	}
	if(id=="InputTypeScore"){
		$("#trAvgMark,#trAvgDec").show();		
	}
	else{
		$('#AvgMark').attr('checked', false);
		$("#AvgDec").val(0);
		$("#trAvgMark,#trAvgDec").hide();
	}
});
$("input[name^='HasRmk']").change(function(e){
	var id = e.target.id;
	if($("#HasRmk").attr("checked") == true){
		$("#RmkDescrEngTr, #RmkDescrChiTr").show();
	}
	else{
		$("#RmkDescrEngTr, #RmkDescrChiTr").hide();
	}
});
function goDelete(templateIDVal,ansIDVal){
	var templateID=$("#TemplateID").val();
	var parentSecID=$("#ParentSecID").val();
	var secID=$("#SecID").val();
	var qCatID=$("#QCatID").val();
	var qID=$("#QID").val();		
	var returnPathVal='Location: ?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_ques&templateID='+templateID+"&parSecID="+parentSecID+"&secID="+secID+"&qCatID="+qCatID+"&qID="+qID;
	var typeVal="SDFAnsDelete";		
	var templateID = $("<input/>").attr("type", "hidden").attr("name", "templateID").val(templateIDVal);
	var ansID = $("<input/>").attr("type", "hidden").attr("name", "ansID").val(ansIDVal);
	var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
	var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
	$('#form1').append($(type)).append($(returnPath)).append($(templateID)).append($(ansID));
	$('form#form1').attr('action', 'index.php?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>delete').submit();	
}

</script>