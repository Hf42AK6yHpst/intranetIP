<script type="text/javascript" src="../../../../templates/jquery/jquery.blockUI.js"></script>
<script type="text/javascript" src="../../../../templates/jquery/jquery.tablednd_0_5.js"></script>
<form id="form1" name="form1" method="post" action="index.php">
	<div class="table_board">
		<div>
			<?=$htmlAry['contentTbl']?>
		</div>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['backBtn']?>
			<p class="spacer"></p>
		</div>	
	</div>
</form>


<script type="text/javascript" src="../../../../templates/jquery/jquery.autocomplete.js"></script>
<link rel="stylesheet" href="../../../../templates/jquery/jquery.autocomplete.css" type="text/css">
<script type="text/javascript">
var monthLangArray = {};
<?php
	foreach($Lang['Appraisal']['Month'] as $key=>$month){
?>
	monthLangArray['<?php echo $key;?>'] = '<?php echo $month;?>';
<?php		
	}
?>
$(document).ready( function() {
	$("table#dndTable").tableDnD({
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
	$("#exemptTeacher").each(function(){
		var elem = $(this);
        var elemId=elem.attr("id");
        var elemIdArr = elemId.split("_");
        var idx = elemIdArr[1];
		$(this).autocomplete(
			'?task=management.appraisal_period.ajax_getData&Type=GetLoginUser&',
			{  			
				onItemSelect: function(li,engine) {
					$("#exemptTeacherList").append($("<option></option>")
                    .attr("value",li.extra[0])
                    .text(li.extra[2]));
                    $("#exemptTeacher").val("");
				},
				formatItem: function(row) {
					return row[0];
				},
				maxItemsToShow: 100,
				minChars: 1,
				delay: 0,
				width: 200
			}
		);		
	});
	$("#reportDoubleCountPerson").each(function(){
		var elem = $(this);
        var elemId=elem.attr("id");
        var elemIdArr = elemId.split("_");
        var idx = elemIdArr[1];
		$(this).autocomplete(
			'?task=management.appraisal_period.ajax_getData&Type=GetLoginUser&',
			{  			
				onItemSelect: function(li,engine) {
					$("#reportDoubleCountPersonList").append($("<option></option>")
                    .attr("value",li.extra[0])
                    .text(li.extra[2]));
                    $("#reportDoubleCountPerson").val("");
				},
				formatItem: function(row) {
					return row[0];
				},
				maxItemsToShow: 100,
				minChars: 1,
				delay: 0,
				width: 200
			}
		);		
	});
	$("#reportDisplayPerson").each(function(){
		var elem = $(this);
        var elemId=elem.attr("id");
        var elemIdArr = elemId.split("_");
        var idx = elemIdArr[1];
		$(this).autocomplete(
			'?task=management.appraisal_period.ajax_getData&Type=GetLoginUser&',
			{  			
				onItemSelect: function(li,engine) {
					$("#reportDisplayPersonList").append($("<option></option>")
                    .attr("value",li.extra[0])
                    .text(li.extra[2]));
                    $("#reportDisplayPerson").val("");
				},
				formatItem: function(row) {
					return row[0];
				},
				maxItemsToShow: 100,
				minChars: 1,
				delay: 0,
				width: 200
			}
		);		
	});
	$('#CPDDurationSetting').change(function(){
		var month = parseInt($(this).val());
		if(month==12){
			$('#SecondPeriodStartMonth').html(monthLangArray[1]);
		}else{
			$('#SecondPeriodStartMonth').html(monthLangArray[month+1]);
		}
	});
});

function goSubmit() {
	clearMemberIDPrefix();
	setMemberIDStr();
	clearReportDisplayPersonIDPrefix();
	setReportDisplayPersonIDStr();
	clearReportDoubleCountPersonIDPrefix();
	setReportDoubleCountPersonIDStr();
	$('form#form1').attr('action', 'index.php?task=settings<?=$appraisalConfig['taskSeparator']?>basic_settings<?=$appraisalConfig['taskSeparator']?>edit_update').submit();
}

function goBack() {
	window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>basic_settings<?=$appraisalConfig['taskSeparator']?>list';
}

function goSelect(){
	newWindow('/home/common_choose/index.php?fieldname=exemptTeacherList&page_title=SelectMembers&permitted_type=1&excludeSupplyTeacher=1&excluded_type=4');
}
function goSelectReportDisplayPerson(){
	newWindow('/home/common_choose/index.php?fieldname=reportDisplayPersonList&page_title=SelectMembers&permitted_type=1&excludeSupplyTeacher=1&excluded_type=4');
}
function goSelectDoubleCountPerson(){
	newWindow('/home/common_choose/index.php?fieldname=reportDoubleCountPersonList&page_title=SelectMembers&permitted_type=1&excludeSupplyTeacher=1&excluded_type=4');
}
function goDeleteMember(){
	// remove selected members
	$("#exemptTeacherList :selected").each(function(i, selected){
		  $("#exemptTeacherList option[value='"+$(selected).val()+"']").remove();
	});
	// reconstruct members list
	var memberIDStr="";
	for(i=0;i<$("#exemptTeacherList option").length;i++){
		if(i==($("#exemptTeacherList option").length)-1){
			memberIDStr+=$("#exemptTeacherList option").eq(i).val();
		}
		else{
			memberIDStr+=$("#exemptTeacherList option").eq(i).val()+",";
		}		
	}
	$("#ExcludeAssessmentTeacherIDStr").val(memberIDStr);
}
function clearMemberIDPrefix(){
	var memberID = "";
	for(i=0;i<$("#exemptTeacherList option").length;i++){
		memberID = $("#exemptTeacherList option").eq(i).val();
		memberID = memberID.replace("U","").replace("G","");
		$("#exemptTeacherList option").eq(i).val(memberID);
	}
}
function setMemberIDStr(){
	// reconstruct members list
	var memberIDStr="";
	for(i=0;i<$("#exemptTeacherList option").length;i++){
		if(i==($("#exemptTeacherList option").length)-1){
			memberIDStr+=$("#exemptTeacherList option").eq(i).val();
		}
		else{
			memberIDStr+=$("#exemptTeacherList option").eq(i).val()+",";
		}		
	}
	$("#ExcludeAssessmentTeacherIDStr").val(memberIDStr);
}
function goDeleteReportDisplayPerson(){
	// remove selected members
	$("#reportDisplayPersonList :selected").each(function(i, selected){
		  $("#reportDisplayPersonList option[value='"+$(selected).val()+"']").remove();
	});
	// reconstruct members list
	var memberIDStr="";
	for(i=0;i<$("#reportDisplayPersonList option").length;i++){
		if(i==($("#reportDisplayPersonList option").length)-1){
			memberIDStr+=$("#reportDisplayPersonList option").eq(i).val();
		}
		else{
			memberIDStr+=$("#reportDisplayPersonList option").eq(i).val()+",";
		}		
	}
	$("#ReportDisplayPersonIDStr").val(memberIDStr);
}
function clearReportDisplayPersonIDPrefix(){
	var memberID = "";
	for(i=0;i<$("#reportDisplayPersonList option").length;i++){
		memberID = $("#reportDisplayPersonList option").eq(i).val();
		memberID = memberID.replace("U","").replace("G","");
		$("#reportDisplayPersonList option").eq(i).val(memberID);
	}
}
function setReportDisplayPersonIDStr(){
	// reconstruct members list
	var memberIDStr="";
	for(i=0;i<$("#reportDisplayPersonList option").length;i++){
		if(i==($("#reportDisplayPersonList option").length)-1){
			memberIDStr+=$("#reportDisplayPersonList option").eq(i).val();
		}
		else{
			memberIDStr+=$("#reportDisplayPersonList option").eq(i).val()+",";
		}		
	}
	$("#ReportDisplayPersonIDStr").val(memberIDStr);
}
function addReason(){
	var iindex = ""; 
	$('.reasonRow').each(function(){
		if($(this).css('display') == 'none' && $(this).attr('status')!="delete" && iindex == ""){
			iindex = $(this).attr('id'); 
		}
	});
	if(iindex != ""){
		$('#'+iindex).css('display', '');
	}
}

function deleteReason(index){
	$('#reasonRow_'+index+' input[name="is_delete[]"]').val(1);
	$('#reasonRow_'+index).css('display','none');
	$('#reasonRow_'+index).attr('status','delete');
}

function addColumn(){
	var iindex = ""; 
	$('.columnRow').each(function(){
		if($(this).css('display') == 'none' && $(this).attr('status')!="delete" && iindex == ""){
			iindex = $(this).attr('id'); 
		}
	});
	if(iindex != ""){
		$('#'+iindex).css('display', '');
	}
}

function deleteColumn(index){
	$('#columnRow_'+index+' input[name="is_col_delete[]"]').val(1);
	$('#columnRow_'+index).css('display','none');
	$('#columnRow_'+index).attr('status','delete');
}

function goDeleteDoubleCountPerson(){
	// remove selected members
	$("#reportDoubleCountPersonList :selected").each(function(i, selected){
		  $("#reportDoubleCountPersonList option[value='"+$(selected).val()+"']").remove();
	});
	// reconstruct members list
	var memberIDStr="";
	for(i=0;i<$("#reportDoubleCountPersonList option").length;i++){
		if(i==($("#reportDoubleCountPersonList option").length)-1){
			memberIDStr+=$("#reportDoubleCountPersonList option").eq(i).val();
		}
		else{
			memberIDStr+=$("#reportDoubleCountPersonList option").eq(i).val()+",";
		}		
	}
	$("#ReportDoubleCountPersonIDStr").val(memberIDStr);
}
function clearReportDoubleCountPersonIDPrefix(){
	var memberID = "";
	for(i=0;i<$("#reportDoubleCountPersonList option").length;i++){
		memberID = $("#reportDoubleCountPersonList option").eq(i).val();
		memberID = memberID.replace("U","").replace("G","");
		$("#reportDoubleCountPersonList option").eq(i).val(memberID);
	}
}
function setReportDoubleCountPersonIDStr(){
	// reconstruct members list
	var memberIDStr="";
	for(i=0;i<$("#reportDoubleCountPersonList option").length;i++){
		if(i==($("#reportDoubleCountPersonList option").length)-1){
			memberIDStr+=$("#reportDoubleCountPersonList option").eq(i).val();
		}
		else{
			memberIDStr+=$("#reportDoubleCountPersonList option").eq(i).val()+",";
		}		
	}
	$("#ReportDoubleCountPersonIDStr").val(memberIDStr);
}

function changeDatePicker(){}
</script>
