<form id="form1" name="form1" method="post" action="index.php">
	<?=$htmlAry['navigation']?>
	<br/>
	<div class="table_board">
		<div>
			<?=$htmlAry['contentTbl']?>
		</div>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['backBtn']?>
			<p class="spacer"></p>
		</div>	
	</div>
	

</form>

<script type="text/javascript">
	$(document).ready(function(){
		
	});
	function goSubmit(){
		var status=formValidation();
		if (status== true){
			var typeVal="formSave";
			var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
			/*var sysUseVal = $("input[name='SysUse']:checked").val();
			var sysUse = $("<input/>").attr("type", "hidden").attr("name", "SysUse").val(sysUseVal);*/
			var appRoleStatusVal = $("input[name='AppRoleStatus']:checked").val();
			var appRoleStatus = $("<input/>").attr("type", "hidden").attr("name", "AppRoleStatus").val(appRoleStatusVal);
			//$('#form1').append($(type)).append($(sysUse)).append($(appRoleStatus));
			$('#form1').append($(type)).append($(appRoleStatus));
			$('form#form1').attr('action', 'index.php?task=settings<?=$appraisalConfig['taskSeparator']?>special_roles<?=$appraisalConfig['taskSeparator']?>edit_update').submit();	
		}
	}
	function goBack(){
		window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>special_roles<?=$appraisalConfig['taskSeparator']?>list';
	}
	function formValidation(){
		var status = true;
		$("#nameEngEmptyWarnDiv,#nameChiEmptyWarnDiv,#appRoleStatusEmptyWarnDiv").hide();
		var nameEng=$("#NameEng").val();
		var nameChi=$("#NameChi").val();
		var appRoleStatus=$("input[name='AppRoleStatus']:checked").val();
		if(nameEng==""||nameEng==null){
			$("#nameEngEmptyWarnDiv").show();
			status=false;
		}
		else if(nameChi==""||nameChi==null){
			$("#nameChiEmptyWarnDiv").show();
			status=false;
		}
		else if(appRoleStatus=="" || appRoleStatus== undefined){
			$("#appRoleStatusEmptyWarnDiv").show();
			status=false;
		}
		return status;
	}


</script>