<form name="form1" id="form1" method="POST" action="?task=settings<?=$appraisalConfig['taskSeparator']?>special_roles<?=$appraisalConfig['taskSeparator']?>list">
	<div class="table_board">
	
	<div class="content_top_tool">
		<?=$htmlAry['contentTool']?>
		<?=$htmlAry['dbTableActionBtn']?>
		<br style="clear:both;">
	</div>
	
	
	<div>
		<?=$htmlAry['contentTbl1']?>
	</div>	
	<div>
		<?=$htmlAry['dataTable1']?>
	</div>
		

	</div>
</form>

<script type="text/javascript">
$(document).ready( function() {	
	$('input#keyword').keydown( function(evt) {
		if (Check_Pressed_Enter(evt)) {
			// pressed enter			
		}
	});	
});

function goArrange(appRoleID){
	window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>special_roles<?=$appraisalConfig['taskSeparator']?>edit&appRoleID='+appRoleID;	
}
function goAddRole(){
	window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>special_roles<?=$appraisalConfig['taskSeparator']?>edit';
}
function goChangeOrder(){
	window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>special_roles<?=$appraisalConfig['taskSeparator']?>edit_order';
}
function goDelete(){
	var confirmMessage = "<?=$Lang['Appraisal']['BtbDelItem']?>";
	var r = confirm(confirmMessage);
	if(r == true){
		checkRemove(document.form1,'AppRoleIDAry[]','?task=settings<?=$appraisalConfig['taskSeparator']?>special_roles<?=$appraisalConfig['taskSeparator']?>delete');
	}	
}

</script>