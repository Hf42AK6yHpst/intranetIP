<script type="text/javascript" src="../../../../templates/jquery/jquery.autocomplete.js"></script>
<link rel="stylesheet" href="../../../../templates/jquery/jquery.autocomplete.css" type="text/css">

<div class="table_board">
	<div>
		<?=$htmlAry['contentTbl']?>
	</div>
	<br style="clear:both;" />
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['viewBtn']?>
		<p class="spacer"></p>
	</div>	
</div>

<div id="report"></div>


<script type="text/javascript">
var reportContent = "";
var ajaxImage = '<?=$indexVar['libappraisal_ui']->Get_Ajax_Loading_Image()?>';
$(document).ready( function() {
	$("input[id^='LoginID']").each(function(){
		var elem = $(this);
        var elemId=elem.attr("id");
		$(this).autocomplete(
			'?task=reports<?=$appraisalConfig['taskSeparator']?>personal_result<?=$appraisalConfig['taskSeparator']?>ajax_getData&Type=GetLoginUser&',
			{  			
				onItemSelect: function(li,engine) {
					var idArr=elemId.split('_');
					var idIdx=idArr[1];					
					$("#UserName").text(li.extra[1]);
					$("#UserID").val(li.extra[0]);
				},
				formatItem: function(row) {
					return row[0];
				},
				maxItemsToShow: 100,
				minChars: 1,
				delay: 0,
				width: 200
			}
		);		
	});
	$('#CycleID').change(function(){
		$('#staff_selection').html(ajaxImage);
		$.post(
			'?task=reports<?=$appraisalConfig['taskSeparator']?>personal_result<?=$appraisalConfig['taskSeparator']?>ajax_getData',
			{
				"Type":"GetLoginUserSelection",
				"CycleID": $('#CycleID').val()
			},
			function(res){
				if(res==""){
					$('#genRptBtn').attr("disabled",'').addClass("formbutton_disable").removeClass("formbutton");
				}else{
					if($('#staff_selection').length > 0){
						$('#staff_selection').html(res);
						$('#genRptBtn').removeAttr("disabled").removeClass("formbutton_disable").addClass("formbutton");
					}
				}
			}
		);
	});
	$('input[name="displayMode"]').change(function(){
		if(this.value == "g"){
			$('#displayName').parent().parent().hide();
			$('#displayDate').parent().parent().hide();
		}else{
			$('#displayName').parent().parent().show();
			$('#displayDate').parent().parent().show();
		}
	});
});

function getStaffSelection(){
	$.post(
		'?task=reports<?=$appraisalConfig['taskSeparator']?>personal_result<?=$appraisalConfig['taskSeparator']?>ajax_getData',
		{
			"Type":"GetLoginUserSelection",
			"CycleID": $('#CycleID').val()
		},
		function(res){
			if($('#staff_selection').length > 0){
				$('#staff_selection').html(res);
			}else{
				$('#genRptBtn').removeAttr("disabled").removeClass("formbutton_disable").addClass("formbutton");
			}
		}
	);
}

function goRptGen(){
	var cycleID=$("#CycleID :selected").val();
	var login = $("#LoginID").val();
	var inclObs = ($("input[name=inclObs]:checked").val()==undefined)?"0":"1";
	var outputDOC = ($("input[name=outputDOC]:checked").val()==undefined)?"0":"1";
	var displayName = ($("input[name=displayName]:checked").val()==undefined)?"0":"1";
	var displayDate = ($("input[name=displayDate]:checked").val()==undefined)?"0":"1";
	var displayMode = $('input[name="displayMode"]:checked').val();
	var displayCover = ($("input[name=displayCover]:checked").val()==undefined)?"0":"1";
	if(displayMode=="g"){
		<?php
		if($sys_custom['eAppraisal']['WusichongStyle']==true){
		?>
			if(cycleID==2){
				var targetURL = 'view_print_special';
			}else{
				var targetURL = 'view_print_word';
			}
		<?php
		}else{
		?>
			var targetURL = 'view_print_word';
		<?php
		}
		?>
	}else if(displayMode=="s"){
		var targetURL = 'view_print';
	}
    $("#cycleIDEmptyWarnDiv").hide();
    $("#userNameEmptyWarnDiv").hide();
    if(cycleID == ""){
    	$("#cycleIDEmptyWarnDiv").show();
    }
    else if(login == ""){
		$("#userNameEmptyWarnDiv").show();
    }
    else{        
      	var data = "user="+login+"&cycleID="+cycleID+"&inclObs="+inclObs;      	
        var url = '?task=reports<?=$appraisalConfig['taskSeparator']?>personal_result<?=$appraisalConfig['taskSeparator']?>'+targetURL+'&cycleID='+$("#CycleID").val()+"&inclObs="+inclObs+"&loginID="+login+"&displayName="+displayName+"&displayDate="+displayDate+"&outputDOC="+outputDOC+"&displayMode="+displayMode+"&displayCover="+displayCover;
        newWindow(url,35);   
    	$.ajax({ type: "POST", url: '?task=reports<?=$appraisalConfig['taskSeparator']?>personal_result<?=$appraisalConfig['taskSeparator']?>ajax_getAttachment', 
            data: data,
            success: function (data, textStatus, jqXHR) {
                $("#report").empty();
                $("#report").append(data); 
                reportContent = data;      	            
            }, error: function (jqXHR, textStatus, errorThrown) {
               
            }
        });
    }
}

function print_timetable()
{
	var url = '?task=reports<?=$appraisalConfig['taskSeparator']?>personal_result<?=$appraisalConfig['taskSeparator']?>view_print&cycleID='+$("#CycleID").val();
	newWindow(url,35);	
}


</script>