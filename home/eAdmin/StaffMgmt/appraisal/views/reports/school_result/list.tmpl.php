<script type="text/javascript" src="../../../../templates/jquery/jquery.autocomplete.js"></script>
<script type="text/javascript" src="../../../../templates/jquery/jquery.tablesorter.js"></script>
<link rel="stylesheet" href="../../../../templates/jquery/jquery.autocomplete.css" type="text/css">

<div class="table_board">
	<div>
		<?=$htmlAry['contentTbl']?>
	</div>
	<br style="clear:both;" />
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['viewBtn']?>
		<p class="spacer"></p>
	</div>	
</div>
<?=$htmlAry['contentTool']?>
<div id="PrintArea">
	<div id="report">
	</div>
</div>

<script type="text/javascript">
var reportContent = "";
var ajaxImage = '<?=$indexVar['libappraisal_ui']->Get_Ajax_Loading_Image()?>';
$(document).ready( function() {
	$('.mode_display').css('display','none');
	$('#CycleID').change(function(){
		var cycleID = $('#CycleID').val();
		$('#sectionSelection').html('');
		if(cycleID==""){
			$('.mode_display').css('display','none');	
			$('#mode_selection').css('display','none');	
			$('#genRptBtn').attr('disabled','disabled');		
		}else{	
			$('#genRptBtn').removeAttr('disabled');
			$('#mode_selection').css('display','table-row');	
			var mode = $('input[name="mode"]:checked').val();
			if(typeof mode == "undefined"){
				mode = $('#mode').val();
			}
			$('.mode_display_'+mode).css('display','table-row');
			if(mode==1){
				if(typeof $('#LoginID').val() != "undefined"){
					getUserTemplate();
				}else{
					$('#staff_selection').html(ajaxImage);
					$.post(
						'?task=reports<?=$appraisalConfig['taskSeparator']?>school_result<?=$appraisalConfig['taskSeparator']?>ajax_getData',
						{
							"Type":"GetLoginUserSelection",
							"CycleID": $('#CycleID').val()
						},
						function(res){
							if(res==""){
								$('#genRptBtn').attr("disabled",'').addClass("formbutton_disable").removeClass("formbutton");
							}else{
								if($('#staff_selection').length > 0){
									$('#staff_selection').html(res);
									$('#genRptBtn').removeAttr("disabled").removeClass("formbutton_disable").addClass("formbutton");
								}
							}
						}
					);
				}
			}else if(mode==0){
				getTemplateSelection();
			}	
		}		
	});
	$('#LoginID').change(function(){
		$('#templateFilter').html(ajaxImage);
		$.post(
			'?task=reports<?=$appraisalConfig['taskSeparator']?>school_result<?=$appraisalConfig['taskSeparator']?>ajax_getData&Type=GetTemplateList',
			{
				'cycleID':$('#CycleID').val(),
				'userLogin':$('#LoginID').val()
			},
			function(response){
				$('#templateFilter').html(response);
			}
		);
	});
});

function getUserTemplate(){
	$('#templateFilter').html(ajaxImage);
	$.post(
		'?task=reports<?=$appraisalConfig['taskSeparator']?>school_result<?=$appraisalConfig['taskSeparator']?>ajax_getData&Type=GetTemplateList',
		{
			'cycleID':$('#CycleID').val(),
			'userLogin':$('#LoginID').val()
		},
		function(response){
			$('#templateFilter').html(response);
		}
	);
}

function goRptGen(){
	var cycleID = $('#CycleID').val();
	var mode = $('input[name="mode"]:checked').val();
	if(typeof mode == "undefined"){
		mode = $('#mode').val();
	}
	if(mode==0){
		if($('#TemplateList').val()==null){
			alert('error!');
			return '';
		}
		$('#report').html(ajaxImage);
		
		var templateList = $('#TemplateList').val();
		var sectionList = "";
		if($('#SectionList').val()!="" && $('#SectionList').val()!=null){
			sectionList = $('#SectionList').val();
		}
		$.post(
			'?task=reports<?=$appraisalConfig['taskSeparator']?>school_result<?=$appraisalConfig['taskSeparator']?>ajax_getData&Type=GetOverallDetailsList',
			{
				'cycleID':$('#CycleID').val(),
				'templateList':templateList.toString(),
				'sectionList':sectionList.toString()
			},
			function(data){
				$('#report').html(data);
				$("#PrintBtn").show();
				$("#AverageTable").tablesorter({
					sortList: [[0,0]]
				});
			}
		);
	}else if(mode == 1){
		var templateID = $('#TemplateID').val();
		$('#report').html(ajaxImage);
		$("#PrintBtn").hide();
		if(cycleID != '' && templateID != ''){
			if(typeof templateID == 'undefined'){
				$.post(
					'?task=reports<?=$appraisalConfig['taskSeparator']?>school_result<?=$appraisalConfig['taskSeparator']?>ajax_getData&Type=GetUserDetailsList',
					{
						'cycleID':$('#CycleID').val(),
						'userID':$('#UserID').val()
					},
					function(data){
						$('#report').html(data);
						$("#PrintBtn").show();
					}
				);
			}else{
				$.post(
					'?task=reports<?=$appraisalConfig['taskSeparator']?>school_result<?=$appraisalConfig['taskSeparator']?>ajax_getData&Type=GetFormDetailsList',
					{
						'cycleID':$('#CycleID').val(),
						'templateID':$('#TemplateID').val(),
						'loginID':$('#LoginID').val()
					},
					function(data){
						$('#report').html(data);
						$("#PrintBtn").show();
					}
				);
			}
		}else{
			alert('error!');
		}
	}
}
function Print(){
	var mode = $('input[name="mode"]:checked').val();
	if(typeof mode == "undefined"){
		mode = $('#mode').val();
	}
	if(mode==1){
		var cycleID = $('#CycleID').val();
		var templateID = $('#TemplateID').val();
		var userID = $('#UserID').val();
		var loginID = $('#LoginID').val();
		newWindow("/home/eAdmin/StaffMgmt/appraisal/controllers/reports/school_result/view_print.php?cycleID="+cycleID+"&templateID="+templateID+"&userID="+userID+"&loginID="+loginID,12);
	}else if(mode==0){
		newWindow("/home/eAdmin/StaffMgmt/appraisal/controllers/reports/school_result/view_print.php?mode="+mode,12);
	}		
};

function getStaffSelection(){
	$.post(
		'?task=reports<?=$appraisalConfig['taskSeparator']?>school_result<?=$appraisalConfig['taskSeparator']?>ajax_getData',
		{
			"Type":"GetLoginUserSelection",
			"CycleID": $('#CycleID').val()
		},
		function(res){
			if($('#staff_selection').length > 0){
				$('#staff_selection').html(res);
			}else{
				$('#genRptBtn').removeAttr("disabled").removeClass("formbutton_disable").addClass("formbutton");
			}
		}
	);
}

function getTemplateSelection(){
	$('#templateSelection').html(ajaxImage);
	$.post(
		'?task=reports<?=$appraisalConfig['taskSeparator']?>school_result<?=$appraisalConfig['taskSeparator']?>ajax_getData&Type=GetTemplateList',
		{
			'cycleID':$('#CycleID').val(),
			'isMultiple': true
		},
		function(response){
			$('#templateSelection').html(response);
			if($('#TemplateList').val().length > 0){
				getSectionSelection();
			}
		}
	);
}

function getSectionSelection(){	
	if($('#SectionList').length > 0 && $('#SectionList').val() != null){
		var sectionList = $('#SectionList').val().toString();
	}else{
		var sectionList = "";
	}
	if($('#sectionSelection').html()==""){
		$('#sectionSelection').html(ajaxImage);
	}
	$.post(
		'?task=reports<?=$appraisalConfig['taskSeparator']?>school_result<?=$appraisalConfig['taskSeparator']?>ajax_getData&Type=GetSectionList',
		{
			'cycleID': $('#CycleID').val(),
			'templateList':	$('#TemplateList').val().toString(),
			'selectedSectionList': sectionList
		},
		function(response){
			$('#sectionSelection').html(response);
		}
	);
}

function activateUserSelection(mode){
	$('.mode_display').css('display','none');
	$('.mode_display_'+mode).css('display','table-row');
	if(mode==0){
		getTemplateSelection();
	}else if(mode==1){
		getStaffSelection();
	}
}
</script>