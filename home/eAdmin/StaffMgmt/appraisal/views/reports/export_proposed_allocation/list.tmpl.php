<div class="table_board">
	<div>
		<?=$htmlAry['contentTbl']?>
	</div>
	<br style="clear:both;" />
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['viewBtn']?>
		<p class="spacer"></p>
	</div>	
</div>

<div id="report"></div>


<script type="text/javascript">
var reportContent = "";
$(document).ready( function() {
});

function goRptGen(){
	var cycleID=$("#CycleID :selected").val();	
	var data = "CycleID="+cycleID;	
	$("#cycleIDEmptyWarnDiv").hide();
	if(cycleID == ""){
		$("#cycleIDEmptyWarnDiv").show();
	}
	else{
		var url = '?task=reports<?=$appraisalConfig['taskSeparator']?>export_proposed_allocation<?=$appraisalConfig['taskSeparator']?>export&cycleID='+$("#CycleID").val();
		window.location.href = url;
	}
}
</script>