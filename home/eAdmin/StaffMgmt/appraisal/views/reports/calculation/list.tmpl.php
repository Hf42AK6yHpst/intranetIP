<script type="text/javascript" src="../../../../templates/jquery/jquery.autocomplete.js"></script>
<link rel="stylesheet" href="../../../../templates/jquery/jquery.autocomplete.css" type="text/css">

<div class="table_board">
	<div>
		<?=$htmlAry['contentTbl']?>
	</div>
	<br style="clear:both;" />
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['viewBtn']?>
		<p class="spacer"></p>
	</div>	
</div>

<div id="report"></div>


<script type="text/javascript">
var reportContent = "";
$(document).ready( function() {
	$("input[id^='LoginID']").each(function(){
		var elem = $(this);
        var elemId=elem.attr("id");
		$(this).autocomplete(
			'?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>ajax_getData&Type=GetLoginUser&',
			{  			
				onItemSelect: function(li,engine) {
					var idArr=elemId.split('_');
					var idIdx=idArr[1];					
					$("#UserName").text(li.extra[1]);
					$("#UserID").val(li.extra[0]);
				},
				formatItem: function(row) {
					return row[0];
				},
				maxItemsToShow: 100,
				minChars: 1,
				delay: 0,
				width: 200
			}
		);		
	})
});

function goRptCal(){
	var batchID = $("#batchID").val();
	var login = $("#LoginID").val();
	var data="batchID="+batchID+"&userID="+login;
	$.ajax({ type: "POST", url: '?task=reports<?=$appraisalConfig['taskSeparator']?>calculation<?=$appraisalConfig['taskSeparator']?>ajax_getData', 
        data: data,
        success: function (data, textStatus, jqXHR) {
            $("#report").empty();
            $("#report").append(data); 
            reportContent = data;          	            
        }, error: function (jqXHR, textStatus, errorThrown) {
           
        }
    });

}

</script>