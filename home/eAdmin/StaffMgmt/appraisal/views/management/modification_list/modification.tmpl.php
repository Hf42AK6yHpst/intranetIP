<form name="form1" id="form1" method="POST" action="?task=management<?=$appraisalConfig['taskSeparator']?>modification_list<?=$appraisalConfig['taskSeparator']?>list">
	<div class="table_board">
	
	<div>
		<?=$htmlAry['contentTbl']?>
	</div>	
	<div>
		<?=$htmlAry['dataTable']?>
	</div>
	<!--
	<br/><br/>
	<div>
		<?=$htmlAry['contentTbl2']?>
	</div>
	<div>
		<?=$htmlAry['dataTable2']?>
	</div>
		-->
	</div>
</form>

<script type="text/javascript">
$(document).ready( function() {	
	$('input#keyword').keydown( function(evt) {
		if (Check_Pressed_Enter(evt)) {
			// pressed enter			
		}
	});
});
function goEdit(rlsNo,recordID,batchID,formType,isObs){
	if(isObs == "1"){
	if(formType=='PDF'){
		window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>observation_period<?=$appraisalConfig['taskSeparator']?>edit&rlsNo='+rlsNo+'&recordID='+recordID+"&batchID="+batchID+"&formType="+formType+"&modification=1";	
	}
	else if(formType=='SDF'){
		window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>observation_period<?=$appraisalConfig['taskSeparator']?>edit_sdf&rlsNo='+rlsNo+'&recordID='+recordID+"&batchID="+batchID+"&formType="+formType+"&modification=1";	
	}	
} if(isObs == "0"){
	if(formType=='PDF'){
		window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_records<?=$appraisalConfig['taskSeparator']?>edit&rlsNo='+rlsNo+'&recordID='+recordID+"&batchID="+batchID+"&formType="+formType+"&modification=1";	
	}
	else if(formType=='SDF'){
		window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_records<?=$appraisalConfig['taskSeparator']?>edit_sdf&rlsNo='+rlsNo+'&recordID='+recordID+"&batchID="+batchID+"&formType="+formType+"&modification=1";	
	}
	}
}
function goView(rlsNo,recordID,batchID,formType){
	if(formType=='PDF'){
		window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>modification_list<?=$appraisalConfig['taskSeparator']?>view&rlsNo='+rlsNo+'&recordID='+recordID+"&batchID="+batchID+"&formType="+formType;	
	}
	else if(formType=='SDF'){
		window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>modification_list<?=$appraisalConfig['taskSeparator']?>view&rlsNo='+rlsNo+'&recordID='+recordID+"&batchID="+batchID+"&formType="+formType;	
	}	
}
function goModificationList(){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>modification_list<?=$appraisalConfig['taskSeparator']?>modification';
}
function goReturnList(){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>modification_list<?=$appraisalConfig['taskSeparator']?>return';
}
</script>