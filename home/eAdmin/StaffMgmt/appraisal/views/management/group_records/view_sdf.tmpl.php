<script type="text/javascript" src="../../../../templates/jquery/jquery.jeditable.js"></script>
<link href='../../../../templates/2009a/css/appraisal/content.css' rel='stylesheet'>
<style>
.content_top_tool .Conntent_tool a{
	background: url('../../../../images/2009a/content_icon.gif') no-repeat 0px -80px;
}
</style>

<form id="form1" name="form1" method="post" action="index.php">
	<?=$htmlAry['navigation']?>
	<div class="content_top_tool" style="float:right">
		<?=$htmlAry['contentTool']?>
		<br style="clear:both;">
	</div>
	<br/>
	<div class="table_board">
		<div>
			<?=$htmlAry['contentTbl']?>
		</div>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['backBtn']?>
			<p class="spacer"></p>
		</div>
		<div>						
			<?=$htmlAry['contentTbl3']?>
		</div>
		<div class="edit_bottom_v30">
			<?=$htmlAry['modBtn']?>
		</div>
		<?=$htmlAry['contentTbl2']?>
		
	</div>
	

</form>


<script type="text/javascript">
	$(document).ready(function(){
		var nonEditDivID=$("#nonEditDivID").val();
		//alert($nonEditDivID);
		$("<div></div>").css({
		    position: "absolute",width: "100%",height: "100%",top: 0,left: 0
		}).appendTo($(nonEditDivID).css("position", "relative").fadeTo("slow",0.7));
	});
	function goSignatureSubmit(){		
		var rlsNo = $("#rlsNo").val();
		var recordID = $("#recordID").val();
		var batchID = $("#batchID").val();
		var returnPathVal='Location: ?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_records<?=$appraisalConfig['taskSeparator']?>edit_sdf&rlsNo='+rlsNo+'&recordID='+recordID+'&batchID='+batchID;	
		var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
		var typeVal="formSignature";
		var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
		$('#form1').append($(type)).append($(returnPath));
		$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_records<?=$appraisalConfig['taskSeparator']?>edit_sdf_update').submit();
	}
	function goSubmit(){
		var status=formValidation();
		if (status== true){
			var rlsNo = $("#rlsNo").val();
			var recordID = $("#recordID").val();
			var batchID = $("#batchID").val();
			var returnPathVal='Location: ?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_records<?=$appraisalConfig['taskSeparator']?>edit_sdf&rlsNo='+rlsNo+'&recordID='+recordID+'&batchID='+batchID;	
			var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
			var typeVal="formSave";
			var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
			$('#form1').append($(type)).append($(returnPath));
			$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_records<?=$appraisalConfig['taskSeparator']?>edit_sdf_update').submit();	
		}	
	}
	function goBackList(){
		window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>group_records<?=$appraisalConfig['taskSeparator']?>list';
	}
	function goBack(grpID){
		window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>group_records<?=$appraisalConfig['taskSeparator']?>member&grpID='+grpID;
	}
	function formValidation(){
		return true;
	}
	function goModify(){
		var cycleStart=$("#cycleStart").val();
		var cycleClose=$("#cycleClose").val();
		var editPrdFr=$("#editPrdFr").val();
		var editPrdTo=$("#editPrdTo").val();
		var modDateFr=$("#modDateFr").val();
		var modDateTo=$("#modDateTo").val();
		var modRemark=$("#modRemark").val();
		var status = true;
		$("#ModDateOutCycleWarnDiv").hide();
		$("#ModDateInvalidRangeWarnDiv").hide();
		if(modDateFr<cycleStart || modDateTo>cycleClose){
			$("#ModDateOutCycleWarnDiv").show();
			status = false;
		}
		else if(modDateTo<modDateFr){
			$("#ModDateInvalidRangeWarnDiv").show();
			status = false;
		}
		else if(modDateFr<editPrdFr || modDateTo>editPrdTo){
			$("#ModDateOutEditWarnDiv").show();
			status = false;
		}
		if (status== true){
			var rlsNo = $("#rlsNo").val();
			var recordID = $("#recordID").val();
			var batchID = $("#batchID").val();
			var grpID = $("#grpID").val();
			var returnPathVal='Location: ?task=management<?=$appraisalConfig['taskSeparator']?>group_records<?=$appraisalConfig['taskSeparator']?>member&grpID='+grpID;	
			var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
			var typeVal="modifySave";
			var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
			$('#form1').append($(type)).append($(returnPath));
			$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_records<?=$appraisalConfig['taskSeparator']?>edit_sdf_update').submit();	
		}	
	}
	
	function goPrint(){
		var url = '?task=reports<?=$appraisalConfig['taskSeparator']?>personal_result<?=$appraisalConfig['taskSeparator']?>print_from_view&rlsNo='+$("#rlsNo").val()+'&recordID='+$("#recordID").val()+'&batchID='+$("#batchID").val();
		newWindow(url,35);	
	}
	function goViewRelated(templateID){
		var url = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_records<?=$appraisalConfig['taskSeparator']?>view_related_forms&rlsNo='+$("#rlsNo").val()+'&recordID='+$("#recordID").val()+'&batchID='+$("#batchID").val()+'&viewTemplateID='+templateID;
		newWindow(url,35);	
	}
</script>