<form name="form1" id="form1" method="POST" action="?task=management<?=$appraisalConfig['taskSeparator']?>group_records<?=$appraisalConfig['taskSeparator']?>member">
	<?=$htmlAry['navigation']?>
	<br/>
	
	<div class="table_board">
	
	<div>
		<?=$htmlAry['contentTbl1']?>
	</div>	
	<div>		
		<?=$htmlAry['dbTableActionBtn']?>
		<?=$htmlAry['dataTable1']?>
	</div>
		
	</div>
</form>

<script type="text/javascript">
$(document).ready( function() {	
	$('input#keyword').keydown( function(evt) {
		if (Check_Pressed_Enter(evt)) {
			// pressed enter			
		}
	});	
});
function goBack(){
	window.location = '?task=management.group_records.list';	
}
function goView(rlsNo,recordID,batchID,grpID,formType){
	var recordIDList = [];
	var batchIDList = [];
 	
	$('#recordIDList').val(recordID);
	$('#batchIDList').val(batchID);
	
	if(formType==0){
		var returnPathVal = 'Location: ?task=management<?=$appraisalConfig['taskSeparator']?>group_records<?=$appraisalConfig['taskSeparator']?>view&rlsNo='+rlsNo+'&recordID='+recordID+"&batchID="+batchID+"&grpID="+grpID;	
	}
	else if(formType==1){
		var returnPathVal = 'Location: ?task=management<?=$appraisalConfig['taskSeparator']?>group_records<?=$appraisalConfig['taskSeparator']?>view_sdf&rlsNo='+rlsNo+'&recordID='+recordID+"&batchID="+batchID+"&grpID="+grpID;	
	}
	var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
	var type = $("<input/>").attr("type", "hidden").attr("name", "type").val('unreadToRead');	
	$('#form1').append($(type)).append($(returnPath));
	$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>group_records<?=$appraisalConfig['taskSeparator']?>status_update').submit();
	
}
function changeStatus(grpID,toRead){
	var hasChecked = $('.statusChangeCheckbox:checked').length;
	if(hasChecked==0){
		alert(globalAlertMsg2);
	}else{
		var recordIDList = [];
		var batchIDList = [];
	 	$('.statusChangeCheckbox:checked').each(function(){
				var idValue = $(this).val().split("_");
				recordIDList.push(idValue[0]);
				batchIDList.push(idValue[1]);
		});
		$('#recordIDList').val(recordIDList.join(','));
		$('#batchIDList').val(batchIDList.join(','));
		var returnPathVal='Location: ?task=management<?=$appraisalConfig['taskSeparator']?>group_records<?=$appraisalConfig['taskSeparator']?>member&grpID='+grpID;
		var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
		if(toRead=='0'){
			var type = $("<input/>").attr("type", "hidden").attr("name", "type").val('readToUnread');
		}else if(toRead=='1'){
			var type = $("<input/>").attr("type", "hidden").attr("name", "type").val('unreadToRead');
		}	
		$('#form1').append($(type)).append($(returnPath));
		$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>group_records<?=$appraisalConfig['taskSeparator']?>status_update').submit();
	}
}
</script>