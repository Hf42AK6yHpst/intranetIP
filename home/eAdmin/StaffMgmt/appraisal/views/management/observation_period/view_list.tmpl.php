<form name="form1" id="form1" method="POST" action="?task=management<?=$appraisalConfig['taskSeparator']?>observation_period<?=$appraisalConfig['taskSeparator']?>view_list">
	<div class="table_board">
	
	<div>
		<?=$htmlAry['contentTbl1']?>
	</div>	
	<div>
		<?=$htmlAry['dataTable1']?>
	</div>
	<br/><br/>
	<div class="content_top_tool">
		<?=$htmlAry['contentTool']?>
		<br style="clear:both;">
	</div>
	
	<div class="content_top_tool">
		<div class="Conntent_tool">
		</div>
		<div class="autocomplete Conntent_search">
    		<input type="text" id="searchInput" name="searchInput" value="<?=$default_search_value ?>">
    		<input type="hidden" id="search_stfid" name="search_stfid" value="">
    		<input type="hidden" id="search_stfname" name="search_stfname" value="">
    		<input type="hidden" id="searchType" name="searchType" value="<?=$searchType?>">
  		</div>
  	</div>
  	
	<br/><br/>
	
	<div>
		<?=$htmlAry['contentTbl2']?>
	</div>
	<div>
		<?=$htmlAry['dataTable2']?>
	</div>
	</div>
</form>

<script type="text/javascript">
$(document).ready( function() {	
	$('input#keyword').keydown( function(evt) {
		if (Check_Pressed_Enter(evt)) {
			// pressed enter			
		}
	});		
	$("#member").hide();
});
function goEdit(rlsNo,recordID,batchID,formType){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>observation_period<?=$appraisalConfig['taskSeparator']?>edit_sdf&rlsNo='+rlsNo+'&recordID='+recordID+"&batchID="+batchID;	
		
}
function goView(rlsNo,recordID,batchID,formType){	
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>observation_period<?=$appraisalConfig['taskSeparator']?>view_sdf&rlsNo='+rlsNo+'&recordID='+recordID+"&batchID="+batchID;	
		
}
function goAdd(){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>observation_period<?=$appraisalConfig['taskSeparator']?>edit';
}
function goEditList(){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>observation_period<?=$appraisalConfig['taskSeparator']?>list';
}
function goViewList(){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>observation_period<?=$appraisalConfig['taskSeparator']?>view_list';
}
/***** Search function START *****/
function autocomplete(inp, arr) {
	  /*the autocomplete function takes two arguments,
	  the text field element and an array of possible autocompleted values:*/
	  var currentFocus;
	  /*execute a function when someone writes in the text field:*/
	  inp.addEventListener("input", function(e) {
	      var a, b, i, val = this.value;
	      /*close any already open lists of autocompleted values*/
	      closeAllLists();
	      if (!val) { return false;}
	      currentFocus = -1;
	      /*create a DIV element that will contain the items (values):*/
	      a = document.createElement("DIV");
	      a.setAttribute("id", this.id + "autocomplete-list");
	      a.setAttribute("class", "autocomplete-items");
	      /*append the DIV element as a child of the autocomplete container:*/
	      this.parentNode.appendChild(a);
	      /*for each item in the array...*/
	      if(isEmpty(arr)){
	    	  return false;
		      }
	      for (i = 0; i < arr.length; i++) {
	        /*check if the item starts with the same letters as the text field value:*/
	        if (arr[i]['UserName'].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
	          /*create a DIV element for each matching element:*/
	          b = document.createElement("DIV");
	          /*make the matching letters bold:*/
	          b.className = "suggestion";
	          b.setAttribute('UserID',arr[i]['UserID']);
	          b.setAttribute('UserName',arr[i]['UserName']);
	          b.innerHTML += "<strong>" + arr[i]['UserName'].substr(0, val.length) + "</strong>";
	          b.innerHTML += arr[i]['UserName'].substr(val.length);
	          /*insert a input field that will hold the current array item's value:*/
	          b.innerHTML += "<input type='hidden' value='" + arr[i]['UserName'] + "' stfid= '" + arr[i]['UserID'] + "'></div>";
	          /*execute a function when someone clicks on the item value (DIV element):*/
	          b.addEventListener("click", function(e) {
	              /*insert the value for the autocomplete text field:*/
	              inp.value = this.getElementsByTagName("input")[0].value;
	              /*close the list of autocompleted values,
	              (or any other open lists of autocompleted values:*/
	              closeAllLists();

	              //submit form1 with Staff ID after clicked the suggested value
	          	$('input#search_stfid').val($(this).attr("UserID"));
	          	$('input#search_stfname').val($(this).attr("UserName"));
	          	$('input#searchType').val("byID");
				$("#form1").submit();
	              
	          });
	          a.appendChild(b);
	        }
	      }
	  });
	  /*execute a function presses a key on the keyboard:*/
	  inp.addEventListener("keydown", function(e) {
	      var x = document.getElementById(this.id + "autocomplete-list");
	      if (x) x = x.getElementsByTagName("div");
	      if (e.keyCode == 40) {
	        /*If the arrow DOWN key is pressed,
	        increase the currentFocus variable:*/
	        currentFocus++;
	        /*and and make the current item more visible:*/
	        addActive(x);
	      } else if (e.keyCode == 38) { //up
	        /*If the arrow UP key is pressed,
	        decrease the currentFocus variable:*/
	        currentFocus--;
	        /*and and make the current item more visible:*/
	        addActive(x);
	      } else if (e.keyCode == 13) {
	        /*If the ENTER key is pressed, Search by the input name,*/
	        if (currentFocus > -1) {
	          /*and simulate a click on the "active" item:*/
	          if (x) x[currentFocus].click();
	        }

	        //submit form1 with input value after clicked "Enter"
	        $('input#searchType').val("byInputName");
	        $("#form1").submit();
	      }
	  });
	  function addActive(x) {
	    /*a function to classify an item as "active":*/
	    if (!x) return false;
	    /*start by removing the "active" class on all items:*/
	    removeActive(x);
	    if (currentFocus >= x.length) currentFocus = 0;
	    if (currentFocus < 0) currentFocus = (x.length - 1);
	    /*add class "autocomplete-active":*/
	    x[currentFocus].classList.add("autocomplete-active");
	  }
	  function removeActive(x) {
	    /*a function to remove the "active" class from all autocomplete items:*/
	    for (var i = 0; i < x.length; i++) {
	      x[i].classList.remove("autocomplete-active");
	    }
	  }
	  function closeAllLists(elmnt) {
	    /*close all autocomplete lists in the document,
	    except the one passed as an argument:*/
	    var x = document.getElementsByClassName("autocomplete-items");
	    for (var i = 0; i < x.length; i++) {
	      if (elmnt != x[i] && elmnt != inp) {
	        x[i].parentNode.removeChild(x[i]);
	      }
	    }
	  }
	  /*execute a function when someone clicks in the document:*/
	  document.addEventListener("click", function (e) {
	      closeAllLists(e.target);
	  });
	}
/***** Search function END *****/

// Run Search function
<?php
$json_obj = new JSON_obj();
$ARFormStfOwnerNameJsonObj = $json_obj->encode($ARFormStfOwnerNameList);
?>
var ARFormStfOwnerNameList = <?= $ARFormStfOwnerNameJsonObj ?>;
autocomplete(document.getElementById("searchInput"), ARFormStfOwnerNameList);
</script>
<style>
/*the container must be positioned relative:*/
.autocomplete {
  position: relative;
  display: inline-block;
}

.autocomplete-items {
  position: absolute;
  border: 1px solid #d4d4d4;
  border-bottom: none;
  border-top: none;
  z-index: 99;
  /*position the autocomplete items to be the same width as the container:*/
  top: 100%;
  left: 0;
  right: 0;
}

.autocomplete-items div {
  padding: 10px;
  cursor: pointer;
  background-color: #fff; 
  border-bottom: 1px solid #d4d4d4; 
}

/*when hovering an item:*/
.autocomplete-items div:hover {
  background-color: #e9e9e9; 
}

/*when navigating through the items using the arrow keys:*/
.autocomplete-active {
  background-color: #e9e9e9 !important; 
/*   color: #ffffff;  */
}
</style>