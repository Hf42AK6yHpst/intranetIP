<script type="text/javascript" src="../../../../templates/jquery/jquery.autocomplete.js"></script>
<link rel="stylesheet" href="../../../../templates/jquery/jquery.autocomplete.css" type="text/css">

<form id="form1" name="form1" method="post" action="index.php">

	<?=$htmlAry['navigation']?>
	<br/>
	<?=$htmlAry['customizedImportStepTbl']?>
		
	<div class="table_board">
		<div>
			<?=$htmlAry['contentTbl']?>
		</div>
		<br style="clear:both;" />		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['saveAsBtn']?>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['resetBtn']?>
			<?=$htmlAry['backBtn']?>
			<p class="spacer"></p>
		</div>			
	</div>
</form>

<script type="text/javascript">
var ajaxImage = '<?=$indexVar['libappraisal_ui']->Get_Ajax_Loading_Image()?>';
$(document).ready( function() {
	$("input[id^='LeaderUserID']").each(function(){
		var elem = $(this);
        var elemId=elem.attr("id");
        var elemIdArr = elemId.split("_");
        var idx = elemIdArr[1];
		$(this).autocomplete(
			'?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>ajax_getData&Type=GetLoginUser&',
			{  			
				onItemSelect: function(li,engine) {
					$("#LeaderUserName_"+idx).text(li.extra[1]);
					$("#LeaderIDStr_"+idx).val(li.extra[0]);
					$("#LeaderUserRole_"+idx+" input[type='radio']:nth-child(2)").attr('checked',true);
					$("#LeaderFillForm_"+idx+" input[type='radio']:nth-child(2)").attr('checked',true);
					$("#deleteLeader_"+idx).show();
					if($('.appraisalMode:checked').val()==2){
						$("#LeaderUserRole_"+idx).show();
					}
					if($('.appraisalMode:checked').val()==1){
						$("#LeaderFillForm_"+idx).show();
					}
					$("#LeaderCanEdit_"+idx).show();
					showNxtLeader();
				},
				formatItem: function(row) {
					return row[0];
				},
				maxItemsToShow: 100,
				minChars: 1,
				delay: 0,
				width: 200
			}
		);		
	});
	function showNxtLeader(){
		var nxtIdx = $("#NumLeader").val();
		nxtIdx = parseInt(nxtIdx) + 1;
		$("#NumLeader").val(nxtIdx);
		$("#row_"+nxtIdx).show();		
	}
	$("#MemberUserID").each(function(){
		var elem = $(this);
        var elemId=elem.attr("id");
		$(this).autocomplete(
			'?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>ajax_getData&Type=GetLoginUser&',
			{  			
				onItemSelect: function(li,engine) {
					//"<option value=\"".$a[$i]["UserID"]."\">".$a[$i]["UserLogin"]."</option>";  
					$("#member").append("<option value="+li.extra[0]+">"+li.extra[1]+"</option>");
					var memberIDStr="";
					for(i=0;i<$("#member option").length;i++){
						if(i==($("#member option").length)-1){
							memberIDStr+=$("#member option").eq(i).val();
						}
						else{
							memberIDStr+=$("#member option").eq(i).val()+",";
						}		
					}
					$("#MemberIDStr").val(memberIDStr);
				},
				formatItem: function(row) {
					return row[0];
				},
				maxItemsToShow: 100,
				minChars: 1,
				delay: 0,
				width: 200
			}
		);		
	});
	$("select[id^='LeaderUserNameSel_']").each(function(){
		var elem = $(this);
        var elemId=elem.attr("id");
		$(this).change(function(){
			var obj = $(this);
			var value = $(this).val();
			var text = $("option:selected",this).text();
			var idArr=elemId.split('_');
			var idIdx=idArr[1];	
			$(this).hide();
			$('#LoadingPlace_'+idIdx).html(ajaxImage);
			setTimeout(function(){
				$("#LeaderUserName_"+idIdx).text(text);
				$("#LeaderIDStr_"+idIdx).val(value);
				$("#LeaderUserRole_"+idIdx+" input[type='radio']:nth-child(2)").attr('checked',true);
				$("#LeaderFillForm_"+idIdx+" input[type='radio']:nth-child(2)").attr('checked',true);
				$("#deleteLeader_"+idIdx).show();
				if($('.appraisalMode:checked').val()==2){
					$("#LeaderUserRole_"+idIdx).show();
				}
				if($('.appraisalMode:checked').val()==1){
					$("#LeaderFillForm_"+idIdx).show();
				}
				$("#LeaderCanEdit_"+idIdx).show();
				if($("#row_"+(parseInt(idIdx)+1)).css('display')=="none"){
					showNxtLeader();
				}
				$('#LoadingPlace_'+idIdx).html('');
				obj.show();
			},750);
		});		
	});
});
function changeDatePicker(id){
	
}
function goSubmit(){	
	var status=formValidation();
	if (status== true){
		setLeaderIDStr();
		setLeaderIDRoleStr();
		setLeaderModifyIDStr();
		setLeaderFillIDStr();
		clearMemberIDPrefix();
		setMemberIDStr();		
		var cycleID=$("#CycleID").val();
		var groupID=($("#GrpID").val()=="")?"%s":$("#GrpID").val();
		var returnPathVal='Location: ?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_grp_arr&cycleID='+cycleID+"&grpID="+groupID;		
		var typeVal="GrpSave";
		var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
		var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
		$('#form1').append($(type)).append($(returnPath));
		$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_update').submit();	
	}
}
function formValidation(){
	var status = true;
	if($('.appraisalMode:checked').val()==2){
		$('#tbLeader .LeaderRole').each(function(idx){
			if($('#LeaderUserName_'+idx).html()!=""){
				if(typeof $('input[name="LeaderRole_'+idx+'"]:checked').val() === "undefined"){
					status = false;
					alert('<?=$Lang['Appraisal']['GroupRoleUnselected']?>');
				}
			}
		});
	}
	return status;
}
function goReset(){
	$("#form1")[0].reset();
}
function goBack(){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>list';
}
function goBackCycle(cycleID){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_info&cycleID='+cycleID;
}
function goDeleteLeader(idx){
	$("#LeaderUserName_"+idx).text("");
	$("#LeaderIDStr_"+idx).val("");
	$("#LeaderUserID_"+idx).val("");
	$("#LeaderUserRole_"+idx+" input[type='radio']").each(function(){
		$(this).attr('checked', false); 
	});
	$("#LeaderFillForm_"+idx+" input[type='radio']").each(function(){
		$(this).attr('checked', false); 
	});
	$("#LeaderCanEdit_"+idx+" input[type='checkbox']").each(function(){
		$(this).attr('checked', false); 
	});
	$("#LeaderUserRole_"+idx).hide();
	$("#LeaderFillForm_"+idx).hide();
	$("#deleteLeader_"+idx).hide();
	$('#LeaderCanEdit_'+idx).hide();
}
function goDeleteMember(){
	// remove selected members
	$("#member :selected").each(function(i, selected){
		  $("#member option[value='"+$(selected).val()+"']").remove();
	});
	// reconstruct members list
	var memberIDStr="";
	for(i=0;i<$("#member option").length;i++){
		if(i==($("#member option").length)-1){
			memberIDStr+=$("#member option").eq(i).val();
		}
		else{
			memberIDStr+=$("#member option").eq(i).val()+",";
		}		
	}
	$("#MemberIDStr").val(memberIDStr);
}
function goSelect(){
	newWindow('/home/common_choose/index.php?fieldname=member&page_title=SelectMembers&permitted_type=1&excludeSupplyTeacher=1&excluded_type=4&exclude_user_id_list='+$('#ExcludeTeacherID').val());
}
function clearMemberIDPrefix(){
	var memberID = "";
	for(i=0;i<$("#member option").length;i++){
		memberID = $("#member option").eq(i).val();
		memberID = memberID.replace("U","").replace("G","");
		$("#member option").eq(i).val(memberID);
	}
}
function setLeaderIDStr(){
	// reconstruct members list
	var leaderIDStr="";
	$("input[id^='LeaderIDStr_']").each(function(e){
		var elem = $(this);
        var elemId=elem.attr("id");
        if($("#"+elemId).val()!= ""){
       		leaderIDStr+=$("#"+elemId).val()+",";
        }
	});
	$("#LeaderIDStr").val(leaderIDStr);
}
function setLeaderIDRoleStr(){
	// reconstruct members list
	var LeaderIDRoleStr="";
	$("input[name^='LeaderRole_']").each(function(e){
		if($(this).attr('checked')==true){
			LeaderIDRoleStr+=$(this).val()+",";
		}
	});
	$("#LeaderIDRoleStr").val(LeaderIDRoleStr);
}
function setLeaderModifyIDStr(){
	// reconstruct list of leader can call form filler to modify form in group records
	var LeaderModifyIDStr="";
	$("input[name^='LeaderEdit_']").each(function(e){
		if($(this).attr('checked')==true){
			LeaderModifyIDStr+="1,";
		}else{
			LeaderModifyIDStr+="0,";
		}
	});
	$("#LeaderModifyIDStr").val(LeaderModifyIDStr);
}
function setLeaderFillIDStr(){
	// reconstruct list of leader can call form filler to modify form in group records
	var LeaderFillIDStr="";
	$("input[name^='LeaderFill_']").each(function(e){
		if($(this).attr('checked')==true){
			LeaderFillIDStr+=$(this).val()+",";
		}
	});
	$("#LeaderFillIDStr").val(LeaderFillIDStr);
}
function setMemberIDStr(){
	// reconstruct members list
	var memberIDStr="";
	for(i=0;i<$("#member option").length;i++){
		if(i==($("#member option").length)-1){
			memberIDStr+=$("#member option").eq(i).val();
		}
		else{
			memberIDStr+=$("#member option").eq(i).val()+",";
		}		
	}
	$("#MemberIDStr").val(memberIDStr);
}
function resetMode(id){
	// reset appraisal mode 
	$( "input[name^='AppMode_']" ).attr("checked", false);
	
	// lock all other appraisal mode
	$('.appraisalMode').attr("checked", false);
	$('.appraisalMode').attr("disabled", "true");
	$( "input[name^='AppMode_"+id+"']" ).removeAttr("disabled");
}
function enableRoleSelection(display){
	if(display==1){
		$("span[id^='LeaderUserRole_']").each(function(idx){
			if($("#LeaderIDStr_"+idx).val()!=""){
				$("#LeaderUserRole_"+idx).css('display','');
			}
		});		
	}else{
		$("span[id^='LeaderUserRole_']").css('display','none');
		$("span[id^='LeaderUserRole_'] > input[type='radio']:nth-child(2)").attr('checked',true);	
	}	
}
function enableLeaderFillSelection(display){
	if(display==1){
		$("span[id^='LeaderFillForm_']").each(function(idx){
			if($("#LeaderIDStr_"+idx).val()!=""){
				$("#LeaderFillForm_"+idx).css('display','');
			}
		});		
	}else{
		$("span[id^='LeaderFillForm_']").css('display','none');
		$("span[id^='LeaderFillForm_'] > input[type='radio']:nth-child(2)").attr('checked',true);	
	}
}
function goSaveAs(){
	var cycleID=$("#CycleID").val();
	var groupID=($("#GrpID").val()=="")?"%s":$("#GrpID").val();
	var returnPathVal='Location: ?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_grp_arr&cycleID='+cycleID+"&grpID=%s";		
	var typeVal="GrpSaveAs";
	var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
	var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
	$('#form1').append($(type)).append($(returnPath));
	$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_update').submit();	
}
</script>
