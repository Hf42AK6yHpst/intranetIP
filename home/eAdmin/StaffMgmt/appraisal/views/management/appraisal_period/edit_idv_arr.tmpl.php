<script type="text/javascript" src="../../../../templates/jquery/jquery.autocomplete.js"></script>
<link rel="stylesheet" href="../../../../templates/jquery/jquery.autocomplete.css" type="text/css">

<form id="form1" name="form1" method="post" action="index.php">

	<?=$htmlAry['navigation']?>
	<br/>
	<?=$htmlAry['customizedImportStepTbl']?>
		
	<div class="table_board">
		<div>
			<?=$htmlAry['contentTbl']?>
		</div>
		<br style="clear:both;" />		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['resetBtn']?>
			<?=$htmlAry['backBtn']?>
			<p class="spacer"></p>
		</div>			
	</div>
</form>

<script type="text/javascript">
var ajaxImage = '<?=$indexVar['libappraisal_ui']->Get_Ajax_Loading_Image()?>';
$(document).ready( function() {
	$("#AppraiseeID").each(function(){
		var elem = $(this);
        var elemId=elem.attr("id");
		$(this).autocomplete(
			'?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>ajax_getData&addType=Appraisee&Type=GetLoginUser&',
			{  			
				onItemSelect: function(li,engine) {
					$("#AppraiseeUserName").text(li.extra[1]);
					$("#AppraiseeIDStr").val(li.extra[0]);
					$("#deleteAppraisee").show();
				},
				formatItem: function(row) {
					return row[0];
				},
				maxItemsToShow: 100,
				minChars: 1,
				delay: 0,
				width: 200
			}
		);		
	});
	$("#AppraiserID").each(function(){
		var elem = $(this);
        var elemId=elem.attr("id");
		$(this).autocomplete(
			'?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>ajax_getData&Type=GetLoginUser&',
			{  			
				onItemSelect: function(li,engine) {
					//"<option value=\"".$a[$i]["UserID"]."\">".$a[$i]["UserLogin"]."</option>";  
					$("#appraiser").append("<option value="+li.extra[0]+">"+li.extra[1]+"</option>");
					var appraiserIDStr="";
					for(i=0;i<$("#appraiser option").length;i++){
						if(i==($("#appraiser option").length)-1){
							appraiserIDStr+=$("#appraiser option").eq(i).val();
						}
						else{
							appraiserIDStr+=$("#appraiser option").eq(i).val()+",";
						}		
					}
					$("#AppraiserIDStr").val(appraiserIDStr);
				},
				formatItem: function(row) {
					return row[0];
				},
				maxItemsToShow: 100,
				minChars: 1,
				delay: 0,
				width: 200
			}
		);		
	});
	$("#AppraiseeSel").each(function(){
		var elem = $(this);
        var elemId=elem.attr("id");
		$(this).change(function(){
			var obj = $(this);
			var value = $(this).val();
			var text = $("option:selected",this).text();
			$(this).hide();
			$('#LoadingPlace').html(ajaxImage);
			setTimeout(function(){				
				$("#AppraiseeUserName").text(text);
				$("#AppraiseeIDStr").val(value);
				$("#"+elemId).val("");
				$("#deleteAppraisee").show();
				$('#LoadingPlace').html('');
				obj.show();
			},750);
		});		
	});
});
function changeDatePicker(id){
	
}
function goSubmit(){	
	var status=formValidation();
	if (status== true){
		clearAppraiserIDPrefix();
		setAppraiserIDStr();
		var cycleID=$("#CycleID").val();
		var idvID=($("#IdvID").val()=="")?"%s":$("#IdvID").val();		
		var returnPathVal='Location: ?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_idv_arr&cycleID='+cycleID+"&idvID="+idvID;	
		var typeVal="IdvSave";
		var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
		var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
		$('#form1').append($(type)).append($(returnPath));
		$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_update').submit();	
	}
}
function formValidation(){
	var status = true;
	
	return status;
}
function goReset(){
	$("#form1")[0].reset();
}
function goBack(){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>list';
}
function goBackCycle(cycleID){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_info&cycleID='+cycleID;
}
function goDeleteAppraisee(){
	$("#AppraiseeUserName").text("");
	$("#AppraiseeIDStr").val("");
	$("#deleteAppraisee").hide();
}
function goDeleteAppraiser(){
	// remove selected members
	$("#appraiser :selected").each(function(i, selected){
		  $("#appraiser option[value='"+$(selected).val()+"']").remove();
	});
	// reconstruct members list
	var appraiserStr="";
	for(i=0;i<$("#appraiser option").length;i++){
		if(i==($("#appraiser option").length)-1){
			appraiserStr+=$("#appraiser option").eq(i).val();
		}
		else{
			appraiserStr+=$("#appraiser option").eq(i).val()+",";
		}		
	}
	$("#AppraiserIDStr").val(appraiserStr);
}
$("#DDLTemplateID").change(function(){
	var templateID=$("#DDLTemplateID :selected").val();
	$("#TemplateID").val(templateID);
});
function goSelect(){
	newWindow('/home/common_choose/index.php?fieldname=appraiser&page_title=SelectMembers&permitted_type=1&excludeSupplyTeacher=1&excluded_type=4');
}
function clearAppraiserIDPrefix(){
	var appraiserID = "";
	for(i=0;i<$("#appraiser option").length;i++){
		appraiserID = $("#appraiser option").eq(i).val();
		appraiserID = appraiserID.replace("U","").replace("G","");
		$("#appraiser option").eq(i).val(appraiserID);
	}
}
function setAppraiserIDStr(){
	// reconstruct members list
	var appraiserIDStr="";
	for(i=0;i<$("#appraiser option").length;i++){
		if(i==($("#appraiser option").length)-1){
			appraiserIDStr+=$("#appraiser option").eq(i).val();
		}
		else{
			appraiserIDStr+=$("#appraiser option").eq(i).val()+",";
		}		
	}
	$("#AppraiserIDStr").val(appraiserIDStr);
}
</script>
