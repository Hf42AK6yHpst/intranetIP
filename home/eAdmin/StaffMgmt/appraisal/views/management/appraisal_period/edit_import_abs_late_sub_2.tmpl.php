<form id="form1" name="form1" method="post" action="index.php">
	<?=$htmlAry['navigation']?>
	<br/>
	<?=$htmlAry['customizedImportStepTbl']?>
		
	<div class="table_board">
		<div>
			<?=$htmlAry['contentTbl']?>
		</div>
		<br style="clear:both;" />		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['backBtn']?>
			<?=$htmlAry['cancelBtn']?>
			<p class="spacer"></p>
		</div>			
	</div>
</form>
<script type="text/javascript">
$(document).ready( function() {
	$("#SuccessfulRecord").text($("#HiddenSuccessfulRecord").val());
	$("#FailureRecord").text($("#HiddenFailureRecord").val());
});
function goImport(){		
	var cycleID = $("#CycleID").val();
	$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_import_abs_late_sub_3&cycleID='+cycleID).submit();		
}
function goBack(){
	var cycleID = $("#CycleID").val();
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_import_abs_late_sub_1&cycleID='+cycleID;
}
function goCancel(cycleID){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_info&cycleID='+cycleID;
}
function goBackCycle(cycleID){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_info&cycleID='+cycleID;
}


</script>