<form name="form1" id="form1" method="POST" action="?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>list">
	<div class="table_board">
		<?=$htmlAry['contentTool']?>
		<?=$htmlAry['dbTableActionBtn']?>
	</div>
	<div>
		<?=$htmlAry['contentTbl1']?>
	</div>	
	<div>
		<?=$htmlAry['dataTable1']?>
	</div>
	<br/><br/>
	<div>
		<?=$htmlAry['contentTbl2']?>
	</div>
	<div>
		<?=$htmlAry['dataTable2']?>
	</div>
		
	</div>
</form>

<script type="text/javascript">
$(document).ready( function() {	
	$('input#keyword').keydown( function(evt) {
		if (Check_Pressed_Enter(evt)) {
			// pressed enter			
		}
	});	
});

function goEdit(cycleID){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_info&cycleID='+cycleID;	
}
function goAddCycle(){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_info&cycleID=';	
}
function goDelete(){
	checkRemove(document.form1,'cycleIDAry[]','?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>delete&type=list');
}
function goCopyCycle(cycleID){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>copy';
}

</script>