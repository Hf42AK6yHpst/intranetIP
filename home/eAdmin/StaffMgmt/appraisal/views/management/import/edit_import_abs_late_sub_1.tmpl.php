<form id="form1" name="form1" method="post" action="index.php" enctype="multipart/form-data">
	<?=$htmlAry['msg']?>
	<br/>
	<?=$htmlAry['navigation']?>
	<br/>
	<?=$htmlAry['customizedImportStepTbl']?>
		
	<div class="table_board">
		<div>
			<?=$htmlAry['contentTbl']?>
		</div>
		<br style="clear:both;" />		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['backBtn']?>
			<p class="spacer"></p>
		</div>			
	</div>
</form>
<script type="text/javascript">
$(document).ready( function() {
});

function goSubmit(){	
	$("#msgTable").hide();
	$("#errMsg").hide();
	var status=formValidation();
	if (status== true){
		var cycleID=$("#CycleID").val();
		$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>import<?=$appraisalConfig['taskSeparator']?>edit_import_abs_late_sub_2&cycleID='+cycleID).submit();
	}
	else{
		$("#msgTable").show();
		$("#errMsg").show();
	}
}

function formValidation(){
	var status = true;
	var fileVal = $("#ImportAbsLateSub").val();
	var cycleID = $("#ddlCycleID :selected").val();
	$("#CycleIDEmptyWarnDiv").hide();
	if(fileVal==""){
		status = false;
	}
	else if(cycleID ==""){
		status = false;
		$("#CycleIDEmptyWarnDiv").show();
	}
	return status; 
}

function goExportAbsLateSub(){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>import<?=$appraisalConfig['taskSeparator']?>export_absLateSub';
}
function goBackCycle(cycleID){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>import<?=$appraisalConfig['taskSeparator']?>edit_import_leave_1';
}
function goBack(cycleID){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>import<?=$appraisalConfig['taskSeparator']?>edit_import_leave_1';
}
function goLeave(){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>import<?=$appraisalConfig['taskSeparator']?>edit_import_leave_1';
}
function goAbsLateSub(){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>import<?=$appraisalConfig['taskSeparator']?>edit_import_abs_late_sub_1';
}
function goSLLateSub(){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>import<?=$appraisalConfig['taskSeparator']?>edit_import_sl_late_1';
}
function goAttendance(){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>import<?=$appraisalConfig['taskSeparator']?>edit_import_attendance_1';
}
<?php if($plugin['eAppraisal_settings']['LnTReport']){ ?>
    function goLntSub(){
    	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>import_lnt<?=$appraisalConfig['taskSeparator']?>edit_import_lnt_data_1';
    }
<?php } ?>

$("#ddlCycleID").change(function(){
	var cycleID = $("#ddlCycleID :selected").val();
	$("#CycleID").val(cycleID);
	var data = "Type=CheckAbsence&cycleID="+cycleID;	
	$.ajax({ type: "POST", url: '?task=management<?=$appraisalConfig['taskSeparator']?>import<?=$appraisalConfig['taskSeparator']?>ajax_checkData', 
        data: data,
        success: function (data, textStatus, jqXHR) {			
        	var result = eval('(' + data + ')');
        	if(result[0]["result"] == 0){
            	var text = "<a class=\"tablelink\" href=\"javascript:goDownloadAbsence()\">"+"<?=$Lang['Appraisal']['CycleTemplate']['DownloadLeave']?>"+"</a>";
            	$("#dlFile").html(text);
        	}
        	else if(result[0]["result"] == 1){
        		$("#dlFile").text(result[0]["msg"]);
        	}
        }
	});	
});
function goDownloadAbsence(){
	var cycleID = $("#ddlCycleID :selected").val();
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>import<?=$appraisalConfig['taskSeparator']?>export_absLateSub_record&cycleID='+cycleID;
}


</script>