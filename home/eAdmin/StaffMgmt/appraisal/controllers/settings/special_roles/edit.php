<?php
// ============================== Related Tables ==============================
// 
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================

# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['SettingsSpecialRole']);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
$appRoleID=$_GET["appRoleID"];

$navigationAry[] = array($Lang['Appraisal']['SettingsSpecialRole'], 'javascript: goBack();');
$navigationAry[] = array($Lang['Appraisal']['SpecialRole']);
$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);

//echo $appRoleID;

$connection = new libgeneralsettings();
//======================================================================== Content ========================================================================//
$sql="SELECT AppRoleID,NameChi,NameEng,DisplayOrder,IsActive,SysUse
		FROM(
			SELECT AppRoleID,NameChi,NameEng,DisplayOrder,IsActive,SysUse FROM INTRANET_PA_S_APPROL WHERE AppRoleID=".IntegerSafe($appRoleID)."			
		) as ipsa";
//echo $sql."<br/><br/>";
$a=$connection->returnResultSet($sql);

$x .= "<table class=\"form_table_v30\">"."\r\n";
	$x .= "<tr>"."\r\n";
		$x .= "<td class=\"field_title\">".$indexVar['libappraisal_ui']->RequiredSymbol().$Lang['Appraisal']['SpecialRolesEngDes']."</td><td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('NameEng', 'NameEng', $a[0]["NameEng"], $OtherClass='', $OtherPar=array('maxlength'=>255));
		$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('nameEngEmptyWarnDiv', $Lang['Appraisal']['SpecialRoles']['Mandatory'], $Class='warnMsgDiv')."</td>\n";
	$x .= "</tr>"."\r\n";
	$x .= "<tr>"."\r\n";
		$x .= "<td class=\"field_title\">".$indexVar['libappraisal_ui']->RequiredSymbol().$Lang['Appraisal']['SpecialRolesChiDes']."</td><td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('NameChi', 'NameChi', $a[0]["NameChi"], $OtherClass='', $OtherPar=array('maxlength'=>255));
		$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('nameChiEmptyWarnDiv', $Lang['Appraisal']['SpecialRoles']['Mandatory'], $Class='warnMsgDiv')."</td>\n";
	$x .= "</tr>"."\r\n";
	$x .= "<tr>"."\r\n";
		$x .= "<td class=\"field_title\">".$indexVar['libappraisal_ui']->RequiredSymbol().$Lang['Appraisal']['SpecialRolesIsActive']."</td>";
		$x .= "<td>"."\r\n";
		$isSysUse = ($a[0]["SysUse"]==1)?1:0;
		$x .= $indexVar['libappraisal_ui']->Get_Radio_Button('active', 'AppRoleStatus', $Value="1", $isChecked=($a[0]["IsActive"]=="1")?1:0, $Class="requiredField", $Display=$Lang['Appraisal']['SpecialRolesIsActiveDes']['1'], $Onclick="",$isDisabled=$isSysUse);
		$x .= "&nbsp;";
		$x .= $indexVar['libappraisal_ui']->Get_Radio_Button('inactive', 'AppRoleStatus', $Value="0", $isChecked=($a[0]["IsActive"]=="0")?1:0, $Class="requiredField", $Display=$Lang['Appraisal']['SpecialRolesIsActiveDes']['0'], $Onclick="",$isDisabled=$isSysUse);
		$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('appRoleStatusEmptyWarnDiv', $Lang['Appraisal']['SpecialRoles']['Mandatory'], $Class='warnMsgDiv')."</td>\n";
		$x .= "</td>"."\r\n";
	$x .= "</tr>"."\r\n";
	
	if($a[0]["SysUse"]==1){
		$sysUseYesRadioButton = 1;
		$sysUseNoRadioButton = 1;
	}
	else if($a[0]["SysUse"]==0){
		$sysUseYesRadioButton = 1;
		$sysUseNoRadioButton = 1;
	}
	else if($a[0]["SysUse"]==null){
		$sysUseYesRadioButton = 1;
		$sysUseNoRadioButton = 0;
	}	
	/*$x .= "<tr>"."\r\n";
		$x .= "<td class=\"field_title\">".$Lang['Appraisal']['SpecialRolesSysUse']."</td>";
		$x .= "<td>"."\r\n";
		$x .= $indexVar['libappraisal_ui']->Get_Radio_Button('yes', 'SysUse', $Value="1", $isChecked=($a[0]["SysUse"]=="1")?1:0, $Class="requiredField", $Display=$Lang['Appraisal']['SpecialRolesSysUseDes']['1'], $Onclick="",$isDisabled=$sysUseYesRadioButton);
		$x .= "&nbsp;";
		$x .= $indexVar['libappraisal_ui']->Get_Radio_Button('no', 'SysUse', $Value="0", $isChecked=($a[0]["SysUse"]=="0" || $a[0]["SysUse"]==null)?1:0, $Class="requiredField", $Display=$Lang['Appraisal']['SpecialRolesSysUseDes']['0'], $Onclick="",$isDisabled=$sysUseNoRadioButton);
		$x .= "</td>"."\r\n";
	$x .= "</tr>"."\r\n";*/
$x .= "</table>";
$x .= "<input type=\"hidden\" id=\"AppRoleID\" name=\"AppRoleID\" value=\"".$appRoleID."\">";

$htmlAry['contentTbl'] = $x;
// ============================== Define Button ==============================

$htmlAry['submitBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Save'], "button", "goSubmit()", 'submitBtn');
$htmlAry['backBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');
// ============================== Define Button ==============================














?>