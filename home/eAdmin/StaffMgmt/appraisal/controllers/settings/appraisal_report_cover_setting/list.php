<?php
// ============================== Related Tables ==============================
// GENERAL_SETTING
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/json.php");

# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['SettingsAppraisalReportCoverSettings']);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libappraisal_ui']->Include_Thickbox_JS_CSS();
$connection = new libgeneralsettings();
$JSON_obj = new JSON_obj();

$sql = "SELECT * FROM GENERAL_SETTING WHERE SettingName = 'ReportCoverPageSetting'";
$a = $connection->returnResultSet($sql);
$a = $JSON_obj->decode($a[0]['SettingValue']);
// print_r($a);
// ============================== Includes files/libraries ==============================

// ============================== Transactional data ============================== 

$x = "";
$x .= '<form id="cover_form" method="post" enctype="multipart/form-data">';
$x .= '<table class="form_table_v30">'."\r\n";	
	$x .= '<tr>'."\r\n";
	$x .= '<td class="field_title">'.$indexVar['libappraisal_ui']->RequiredSymbol().$Lang['Appraisal']['Settings']['Language'].'</td>'."\r\n";
	$x .= '<td>'."\r\n";
	if($a["Language"]=="Zh"){
	   $x .= $indexVar['libappraisal_ui']->Get_Radio_Button("language", "language", $value='Zh', $isChecked=1, $Class="", $Display=$Lang['Appraisal']['Settings']['TraditionalChinese'], $Onclick="",$isDisabled=0,$isIndented=0,$specialLabel="");
	   $x .= '&emsp;&emsp;';
	   $x .= $indexVar['libappraisal_ui']->Get_Radio_Button("language", "language", $value='En', $isChecked=0, $Class="", $Display=$Lang['Appraisal']['Settings']['English'], $Onclick="",$isDisabled=0,$isIndented=0,$specialLabel="");
	}else{
	   $x .= $indexVar['libappraisal_ui']->Get_Radio_Button("language", "language", $value='Zh', $isChecked=0, $Class="", $Display=$Lang['Appraisal']['Settings']['TraditionalChinese'], $Onclick="",$isDisabled=0,$isIndented=0,$specialLabel="");
	   $x .= '&emsp;&emsp;';
	   $x .= $indexVar['libappraisal_ui']->Get_Radio_Button("language", "language", $value='En', $isChecked=1, $Class="", $Display=$Lang['Appraisal']['Settings']['English'], $Onclick="",$isDisabled=0,$isIndented=0,$specialLabel="");
	}
	$x .= '</td>'."\r\n";
	
	$x .= '<tr>'."\r\n";
	$x .= '<td class="field_title">'.$Lang['Appraisal']['ARFormExpSchoolName'].'</td>'."\r\n";
	$x .= '<td>'."\r\n";
	$x .= $indexVar['libappraisal_ui']->Get_Checkbox("schoolName", "schoolName", $Value=$a["SchoolName"], $isChecked=$a["SchoolName"], $Class="checkBox", $Display=$Lang['Appraisal']['Settings']['Display'], $Onclick="",$isDisabled=0);
	$x .= '</td>'."\r\n";
	
	$x .= '<tr>'."\r\n";
	$x .= '<td class="field_title">'.$Lang['Appraisal']['Settings']['GroupName'].'</td>'."\r\n";
	$x .= '<td>'."\r\n";
	$x .= $indexVar['libappraisal_ui']->Get_Checkbox("groupName", "groupName", $Value=$a["GroupName"], $isChecked=$a["GroupName"], $Class="checkBox", $Display=$Lang['Appraisal']['Settings']['Display'], $Onclick="",$isDisabled=0);
	$x .= '</td>'."\r\n";
	
	$x .= '<tr>'."\r\n";
	$x .= '<td class="field_title">'.$Lang['Appraisal']['Settings']['SchoolEmblem'].'</td>'."\r\n";
	$x .= '<td>'."\r\n";
	$x .= $indexVar['libappraisal_ui']->Get_Checkbox("schoolEmblem", "schoolEmblem", $Value=$a["SchoolEmblem"], $isChecked=$a["SchoolEmblem"], $Class="checkBox", $Display=$Lang['Appraisal']['Settings']['Display'], $Onclick="",$isDisabled=0);
	$x .= '&emsp;&emsp;';
	$x .= '<input type="file" id="schoolEmblem_photo" name="schoolEmblem_photo" class="file" value="">';
	if($a["SchoolEmblemPhoto"] != ""){
	    $x .= '<div style="border: 2px solid #e2e2e2; width: 150px; height: 170px; margin-left: 75%;"><img id="photo_preview" src="'.$PATH_WRT_ROOT.'/file/appraisal/cover_image/'.$a["SchoolEmblemPhoto"].'" style="width: 150px; height: 170px;"></div>';	    
	}else{
		$x .= '<div style="border: 2px solid #e2e2e2; width: 150px; height: 170px; margin-left: 75%;"><img id="photo_preview" src="#" style="width: 150px; height: 170px;"></div>';
	}
	$x .= '<input type="hidden" id="SchoolEmblemPhotoOriginal" name="SchoolEmblemPhotoOriginal" value="'.$a["SchoolEmblemPhoto"].'">';
	$x .= '</td>'."\r\n";
	
	$x .= '<tr>'."\r\n";
	$x .= '<td class="field_title">'.$Lang['Appraisal']['Settings']['SchoolYear'].'</td>'."\r\n";
	$x .= '<td>'."\r\n";
	$x .= $indexVar['libappraisal_ui']->Get_Checkbox("schoolYear", "schoolYear", $Value=$a["SchoolYear"], $isChecked=$a["SchoolYear"], $Class="checkBox", $Display=$Lang['Appraisal']['Settings']['Display'], $Onclick="",$isDisabled=0);
	$x .= '</td>'."\r\n";
	
	$x .= '<tr>'."\r\n";
	$x .= '<td class="field_title">'.$Lang['Appraisal']['Settings']['ReportName'].'</td>'."\r\n";
	$x .= '<td>'."\r\n";
	$x .= $indexVar['libappraisal_ui']->Get_Checkbox("reportName", "reportName", $Value=$a["ReportName"], $isChecked=$a["ReportName"], $Class="checkBox", $Display=$Lang['Appraisal']['Settings']['Display'], $Onclick="",$isDisabled=0);
	$x .= '&emsp;&emsp;';
	$x .= $indexVar['libappraisal_ui']->GET_TEXTBOX('reportNameText', 'reportNameText', $a["ReportNameText"], $OtherClass='', $OtherPar=array('style'=>"width:80%;"));
	$x .= '</td>'."\r\n";
	
	$x .= '<tr>'."\r\n";
	$x .= '<td class="field_title">'.$Lang['Appraisal']['Settings']['TeacherName'].'</td>'."\r\n";
	$x .= '<td>'."\r\n";
	$x .= $indexVar['libappraisal_ui']->Get_Checkbox("teacherChineseName", "teacherChineseName", $Value=$a["TeacherChineseName"], $isChecked= $a["TeacherChineseName"], $Class="checkBox", $Display=$Lang['Appraisal']['Settings']['DisplayChineseName'], $Onclick="",$isDisabled=0);
	$x .= '&emsp;&emsp;';
	$x .= $indexVar['libappraisal_ui']->Get_Checkbox("teacherEnglishName", "teacherEnglishName", $Value=$a["TeacherEnglishName"], $isChecked= $a["TeacherEnglishName"], $Class="checkBox", $Display=$Lang['Appraisal']['Settings']['DisplayEnglishName'], $Onclick="",$isDisabled=0);
	$x .= '</td>'."\r\n";
	
	$x .= '<tr>'."\r\n";
	$x .= '<td class="field_title">'.$Lang['Appraisal']['Settings']['SchoolMotto'].'</td>'."\r\n";
	$x .= '<td>'."\r\n";
	$x .= $indexVar['libappraisal_ui']->Get_Checkbox("schoolMotto", "schoolMotto", $Value=$a["SchoolMotto"], $isChecked=$a["SchoolMotto"], $Class="checkBox", $Display=$Lang['Appraisal']['Settings']['Display'], $Onclick="",$isDisabled=0);
	$x .= '&emsp;&emsp;';
	$x .= $indexVar['libappraisal_ui']->GET_TEXTAREA("schoolMottoText", $taContents=$a["SchoolMottoText"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='');
	$x .= '</td>'."\r\n";

	$x .= '<tr>'."\r\n";
	$x .= '<td class="field_title">'.$Lang['Appraisal']['Settings']['AllAppRole'].'</td>'."\r\n";
	$x .= '<td>'."\r\n";
	$x .= $indexVar['libappraisal_ui']->Get_Checkbox("allAppRole", "allAppRole", $Value=$a["AllAppRole"], $isChecked=$a["AllAppRole"], $Class="checkBox", $Display=$Lang['Appraisal']['Settings']['Display'], $Onclick="",$isDisabled=0);
	$x .= '</td>'."\r\n";
$x .= '</table>';
$x .= '</form>';

$x .= '<br>';
$x .= "<span style=\"color:#999999\">".sprintf($Lang['Appraisal']['Cycle']['Remark'],"<span style=\"color:#FF0000\">*</span>")."</span><br/><br/>";
$x .= "<input type='hidden' id='preview_use' value=''>";

$htmlAry['contentTbl'] = $x;
// ============================== Define Button ============================== 
$htmlAry['saveBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnSave'], "button", "goSave()", 'editBtn');
$htmlAry['previewBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnPreviewCover'], "button", "goPreview()", 'editBtn');
?>