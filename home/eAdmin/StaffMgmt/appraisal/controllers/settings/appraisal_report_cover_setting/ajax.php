<?php
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');


$lgeneralsettings = new libgeneralsettings();
$fm = new libfilesystem();
$json = new JSON_obj();

$returnPath = $_POST["returnPath"];
$method = $_POST["method"];

if($method=="load_preview_data"){
	$coverPageSettingValues = $json->decode($_POST['preview_data']);
	$logoLink = "#";
	if(isset($coverPageSettingValues['SchoolEmblemPhotoOriginal']) && $coverPageSettingValues['SchoolEmblemPhotoOriginal']!=""){
		$logoLink = '/file/appraisal/cover_image/'.$coverPageSettingValues['SchoolEmblemPhotoOriginal'];
	}
	$school_data = explode("\n",get_file_content("$intranet_root/file/school_data.txt"));
	$school_name = $school_data[0];
	$school_org = $school_data[1];
	
	$sql = "SELECT YearNameEN, YearNameB5 FROM ACADEMIC_YEAR WHERE AcademicYearID='".$_SESSION['CurrentSchoolYearID']."'";
	$yearNameResult = current($indexVar['libappraisal']->returnArray($sql));
	if($coverPageSettingValues['Language']=='Zh'){
		$schoolYear = $yearNameResult['YearNameB5'];
	}else{
		$schoolYear = $yearNameResult['YearNameEN'];
	}
	
	$sql = "SELECT EnglishName, ChineseName FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID='".$_SESSION['UserID']."'";
	$userNameResult = current($indexVar['libappraisal']->returnArray($sql));
	
	$field = ($coverPageSettingValues['Language']=='Zh')?"NameChi":"NameEng";
	$sql = "Select $field as Name From INTRANET_PA_S_APPROL Order By DisplayOrder ASC";
	$approleList = $indexVar['libappraisal']->returnVector($sql);
?>
	<div class="page-wrapper">
		<div class="page-content">
			<table class="main">
				<tr class="box-upper">
					<td>
					<?php if($coverPageSettingValues["GroupName"] == 1){?>
					<div class="org-name"><?php echo $indexVar['libappraisal']->displayChinese($school_org);?></div>
					<?php }?>
					<?php if($coverPageSettingValues["SchoolName"] == 1){?>
					<div class="sch-name"><?php echo $indexVar['libappraisal']->displayChinese($school_name);?></div>
					<?php }?>
					</td>
				</tr>
				<tr class="box-logo">
					<td>
						<?php if($coverPageSettingValues["SchoolEmblem"] == 1){?>
						<div class="sch-logo"><img id="school_emblem" src="<?php echo $logoLink;?>"></div>
						<?php }?>
					</td>
				</tr>
				<tr class="box-middle">
					<td>
						<?php if($coverPageSettingValues["SchoolYear"] == 1){?>
						<div class="sch-year"><?php echo $indexVar['libappraisal']->displayChinese($schoolYear);?></div>
						<?php }
						if($coverPageSettingValues["ReportName"] == 1){?>
						<div class="report-name"><?= $coverPageSettingValues["ReportNameText"]?></div>
						<?php }
						if($coverPageSettingValues["TeacherChineseName"] == 1){?>
						<div class="tea-name"><?php echo $userNameResult['ChineseName'];?>
						<?php }
						if($coverPageSettingValues["TeacherEnglishName"] == 1){?>
						<br><?php echo $userNameResult['EnglishName'];?></div>
						<?php }?>
					</td>
				</tr>
				<tr class="box-lower">
					<td>
					<?php if($coverPageSettingValues["SchoolMotto"] == 1){?>
						<div class="sch-motto"><?= $coverPageSettingValues["SchoolMottoText"] ?></div>
						<?php }if($coverPageSettingValues["AllAppRole"] == 1){?>
						<table class="roles">
							<?php
								$idx=0;
								foreach($approleList as $approle){
									if($idx==0){
										echo '<tr>';
									}
									echo '<td>'.$approle.': xxxxx</td>';
									if($idx==count($approleList)-1){
										echo '</tr>';
									}elseif($idx % 2 == 1){
										echo '</tr><tr>';
									}
									$idx++;
								}
							?>
						</table>
						<?php }?>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<div class="page-break"></div>
<?php	
}
?>