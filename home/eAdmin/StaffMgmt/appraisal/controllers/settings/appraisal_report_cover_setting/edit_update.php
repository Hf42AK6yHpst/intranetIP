<?php 

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');


$lgeneralsettings = new libgeneralsettings();
$fm = new libfilesystem();
$json = new JSON_obj();

$returnPath = $_POST["returnPath"];
$tmp_name = $_FILES["schoolEmblem_photo"]["tmp_name"];
$filename = $_FILES['schoolEmblem_photo']['name']; 
    
$folderToDownload = $intranet_root."/file/appraisal/cover_image/";

if(empty($tmp_name) && empty($filename)){
    $sql = "SELECT SettingValue FROM GENERAL_SETTING
            WHERE Module = \"TeacherApprisal\"
            AND SettingName = \"ReportCoverPageSetting\"";
    $oldCoverPageSettingValues=$lgeneralsettings->returnVector($sql);
    $oldCoverPageSettingValues=$json->decode($oldCoverPageSettingValues[0]);
    $filename = $oldCoverPageSettingValues["SchoolEmblemPhoto"];
    
    $coverPageSettingValues = array();
    $coverPageSettingValues["ReportCoverPageSetting"] = $_POST["coverPageSettingValues"];
    $coverPageSettingValues["ReportCoverPageSetting"] = $json->decode($coverPageSettingValues["ReportCoverPageSetting"]);
    $coverPageSettingValues["ReportCoverPageSetting"]["SchoolEmblemPhoto"] = $filename;
    $coverPageSettingValues["ReportCoverPageSetting"] = $json->encode($coverPageSettingValues["ReportCoverPageSetting"]);
}else{
    $coverPageSettingValues = array();
    $coverPageSettingValues["ReportCoverPageSetting"] = $_POST["coverPageSettingValues"];
    $coverPageSettingValues["ReportCoverPageSetting"] = $json->decode($coverPageSettingValues["ReportCoverPageSetting"]);
    $coverPageSettingValues["ReportCoverPageSetting"]["SchoolEmblemPhoto"] = $filename;
    $coverPageSettingValues["ReportCoverPageSetting"] = $json->encode($coverPageSettingValues["ReportCoverPageSetting"]);
    
    $fm->folder_remove_recursive($folderToDownload);
    $fm->createFolder($folderToDownload);
    move_uploaded_file($tmp_name, $folderToDownload.$filename);
}
    
    $lgeneralsettings->Save_General_Setting("TeacherApprisal", $coverPageSettingValues);
    
    header($returnPath);
?> 