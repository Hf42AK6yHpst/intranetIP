﻿﻿<?php 
$NoUTF8 = true;
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/appraisal_lang.$intranet_session_language.php");

intranet_opendb();
$g_encoding_unicode = true;

include_once($PATH_WRT_ROOT."includes/appraisal/appraisalConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/appraisal/libappraisal.php");
include_once($PATH_WRT_ROOT."includes/appraisal/libappraisal_ui.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();
$indexVar['libappraisal'] = new libappraisal();
$indexVar['libappraisal_ui'] = new libappraisal_ui();
?>
<meta http-equiv='content-type' content='text/html; charset=utf8'>
<script language="JavaScript" src="/templates/jquery/jquery-1.3.2.min.js"></script>
<style type="text/css">
	/*PRINT*/
	@media print {
		html, body {width:21cm;}
		.page-break	{display:block; page-break-before:always;}
	}
	@page {size:A4 portrait; margin:0 0 0 0;}
	.page-wrapper {width:19.6cm; height:28cm; margin:0 auto; border:1px solid #fff;}
	.page-content {margin:.5cm 2cm 1cm 2cm; height:27.7cm;}
	@-moz-document url-prefix() {.page-wrapper {height:27cm;} .page-content {height:26cm;}}
	@media screen and (-ms-high-contrast:active), (-ms-high-contrast:none) { /*for IE*/
	   .page-wrapper {height:27cm;}
	   .page-content {height:26cm;}
	}
	table {table-layout: fixed;} /*for IE*/
	/*GENERAL*/
	body, html {background-color:#fff; color:#000; font-family:Verdana, Arial, '新細明體', sans-serif; font-size:12px; margin:0; padding:0; -webkit-print-color-adjust:exact; text-align:center;}
	/*CONTENT*/
	table.main {height:100%; width:100%;}
	table.main td {vertical-align:middle; text-align:center;}
	table.main .box-lower td {vertical-align:bottom;}
	.box-upper {height:4.5cm; max-height:4.5cm;}
	.box-logo {height:9cm;}
	.box-middle {max-height:12cm;}
	.org-name {font-size:2rem; margin-bottom:1rem;}
	.sch-name {font-size:3.5rem;}
	.sch-logo img {max-width:100%; height:7cm; max-height:8cm;}
	.sch-year {font-size:3rem; margin-bottom:1rem;}
	.report-name {font-size:4rem; margin-bottom:3rem;}
	.tea-name {font-size:3rem;}
	.sch-motto {font-size:1.8rem; margin-top:1rem;}
	.roles {font-size:1.3rem; width:100%; margin-top:2rem;}
</style>
<div style="color:red;"><?php echo $Lang['Appraisal']['PreviewRemindSave'];?></div>
<div id="content">
	
</div>
<?php
intranet_closedb();
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");

?>
<script>
$(document).ready(function(){
	$.post('/home/eAdmin/StaffMgmt/appraisal/index.php?task=settings<?=$appraisalConfig['taskSeparator']?>appraisal_report_cover_setting<?=$appraisalConfig['taskSeparator']?>ajax',
		{
			'method':'load_preview_data',
			'preview_data': window.opener.cover_form.coverPageSettingValues.value
		},
		function(response){
			$('#content').html(response);
			var url = window.URL.createObjectURL(window.opener.cover_form.schoolEmblem_photo.files[0]);
			$('#school_emblem').attr('src',url);
		}
	);
});
</script>