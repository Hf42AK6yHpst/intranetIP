<?php
// ============================== Related Tables ==============================
// 
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================

# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['TemplateSampleForm']);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libappraisal_ui']->Include_JS_CSS();

$templateID=$_GET["templateID"];
$secID=($_GET["secID"]!=null)?$_GET["secID"]:"NULL";
$connection = new libgeneralsettings();
//======================================================================== Header ========================================================================//
### navigation
$navigationAry[] = array($Lang['Appraisal']['SettingsTemplate'], 'javascript: goBack();');
$navigationAry[] = array($Lang['Appraisal']['TemplateSample']['Title']);
$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);
### step box
/*$stepAry = array();
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps1'];
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps2'];
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps3'];
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps4'];
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps5'];
$htmlAry['customizedImportStepTbl'] = $indexVar['libappraisal_ui']->GET_IMPORT_STEPS($CurrStep=2, $stepAry);
*/
//======================================================================== Content ========================================================================//
$sql="SELECT ipsfsec.TemplateID,SecID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng,FrmTitleChi,FrmTitleEng,FrmCodChi,FrmCodEng,ObjChi,ObjEng,DateTimeModified,IsObs
	FROM(				
		SELECT TemplateID,FrmTitleChi,FrmTitleEng,FrmCodChi,FrmCodEng,ObjChi,ObjEng,IsObs FROM INTRANET_PA_S_FRMTPL WHERE TemplateID=".IntegerSafe($templateID)."
	) as ipsf
	LEFT JOIN(
		SELECT TemplateID,SecID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng,DateTimeModified
		FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND SecID=".IntegerSafe($secID)." AND ParentSecID IS NULL
	) as ipsfsec ON ipsfsec.TemplateID=ipsf.TemplateID;";
// echo $sql."<br/><br/>";
$a=$connection->returnResultSet($sql);
$dateTimeModified = $a[0]["DateTimeModified"];
$tgtName = ($_SESSION['intranet_session_language']=="en")?$Lang['Appraisal']['ARFormTgtName'].$a[0]["AppTgtEng"]:$a[0]["AppTgtChi"].$Lang['Appraisal']['ARFormTgtName'];
$templateTitle = ($_SESSION['intranet_session_language']=="en")?($a[0]["FrmTitleEng"]."-".$a[0]["FrmCodEng"]."-".$a[0]["ObjEng"]):($a[0]["FrmTitleChi"]."-".$a[0]["FrmCodChi"]."-".$a[0]["ObjChi"]);
$isObs=$a[0]["IsObs"];


$x .="<table class=\"form_table_v30\">";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['CurrentTemplate']."</td>";
$x .= "<td><a id='templateTitleID' class=\"tablelink\" href=\"javascript:goBackTemplate('".$templateID."')\">".$templateTitle."</a></td></tr>";
$x .= "</table>";
$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['TemplateSample']['SDFSecBasicInformation'])."\r\n";
$x .="<table class=\"form_table_v30\">";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['SDFSecCodEng']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('SecCodEng', 'SecCodEng', $a[0]["SecCodEng"], $OtherClass='', $OtherPar=array('maxlength'=>255)).$indexVar['libappraisal_ui']->Get_Form_Warning_Msg('frmTitleEngEmptyWarnDiv', $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv')."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['SDFSecCodChi']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('SecCodChi', 'SecCodChi', $a[0]["SecCodChi"], $OtherClass='', $OtherPar=array('maxlength'=>255)).$indexVar['libappraisal_ui']->Get_Form_Warning_Msg('frmTitleEngEmptyWarnDiv', $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv')."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['SDFSecTitleEng']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('SecTitleEng', 'SecTitleEng', $a[0]["SecTitleEng"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['SDFSecTitleChi']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('SecTitleChi', 'SecTitleChi', $a[0]["SecTitleChi"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['SDFSecDescrEng']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTAREA("SecDescrEng", $taContents=$a[0]["SecDescrEng"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['SDFSecDescrChi']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTAREA("SecDescrChi", $taContents=$a[0]["SecDescrChi"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
$x .="</table><br/>";
//======================================================================== Section Access Right ========================================================================//
$sql="SELECT ipsa.AppRoleID,Name,DisplayOrder,CanBrowse,CanFill
		FROM(
			SELECT AppRoleID,".$indexVar['libappraisal']->getLangSQL("NameChi","NameEng")." as Name,DisplayOrder,IsActive FROM INTRANET_PA_S_APPROL WHERE IsActive=1
		) as ipsa
		LEFT JOIN(
			SELECT SecID,AppRoleID,CanBrowse,CanFill FROM INTRANET_PA_S_SECROL WHERE SecID=".IntegerSafe($secID)."
		) as ipss ON ipsa.AppRoleID=ipss.AppRoleID
		ORDER BY DisplayOrder
		";
//echo $sql."<br/><br/>";
$a=$connection->returnResultSet($sql);
$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['TemplateSample']['SDFSecRoleAccess'])."\r\n";
$x .= "<div><span>";
$x .= "<table class=\"common_table_list ClassDragAndDrop\">";
$x .= "<thead>"."\r\n";
$x .= "<tr>";
$x .= "<th class=\"sub_row_top\" style=\"width:30%;\">".$Lang['Appraisal']['TemplateSample']['SDFSecAppRoleName']."</th>";
$x .= "<th class=\"sub_row_top\" style=\"width:35%;\">".$Lang['Appraisal']['TemplateSample']['SDFSecCanBrowse']."</th>";
$x .= "<th class=\"sub_row_top\" style=\"width:35%;\">".$Lang['Appraisal']['TemplateSample']['SDFSecCanFill']."</th>";
$x .= "</tr>";
$x .= "</thead>";
for($i=0;$i<sizeof($a);$i++){
	if($i==sizeof($a)-1){
		$appRoleIDStr.=$a[$i]["AppRoleID"];
	}
	else{
		$appRoleIDStr.=$a[$i]["AppRoleID"].",";
	}
	if($isObs==0){
		$x .= "<tr id=\"".$a[$i]['AppRoleID']."\">"."\r\n";
		$x .="<td>".$a[$i]["Name"]."</td>";
		$x .="<td>".$indexVar['libappraisal_ui']->Get_Checkbox('CanBrowse_'.$a[$i]["AppRoleID"], 'CanBrowse_'.$a[$i]["AppRoleID"], $Value=1, $isChecked=($a[$i]["CanBrowse"]=="1")?1:0, $Class='', $Display='', $Onclick='', $Disabled='')."</td>";
		$x .="<td>".$indexVar['libappraisal_ui']->Get_Checkbox('CanFill_'.$a[$i]["AppRoleID"], 'CanFill_'.$a[$i]["AppRoleID"], $Value=1, $isChecked=($a[$i]["CanFill"]=="1")?1:0, $Class='', $Display='', $Onclick='', $Disabled='')."</td>";
		$x .= "</tr>";
	}
	else{
		if($a[$i]["AppRoleID"]==1){
			$x .= "<tr id=\"".$a[$i]['AppRoleID']."\">"."\r\n";
			$x .="<td>".$a[$i]["Name"]."</td>";
			$x .="<td>".$indexVar['libappraisal_ui']->Get_Checkbox('CanBrowse_'.$a[$i]["AppRoleID"], 'CanBrowse_'.$a[$i]["AppRoleID"], $Value=1, $isChecked=($a[$i]["CanBrowse"]=="1")?1:0, $Class='', $Display='', $Onclick='', $Disabled='')."</td>";
			$x .="<td>".$indexVar['libappraisal_ui']->Get_Checkbox('CanFill_'.$a[$i]["AppRoleID"], 'CanFill_'.$a[$i]["AppRoleID"], $Value=1, $isChecked=($a[$i]["CanFill"]=="1")?1:0, $Class='', $Display='', $Onclick='', $Disabled='')."</td>";
			$x .= "</tr>";
		}
		else if($a[$i]["AppRoleID"]==2){
			$x .= "<tr id=\"".$a[$i]['AppRoleID']."\">"."\r\n";
			$x .="<td>".$a[$i]["Name"]."</td>";
			$x .="<td>".$indexVar['libappraisal_ui']->Get_Checkbox('CanBrowse_'.$a[$i]["AppRoleID"], 'CanBrowse_'.$a[$i]["AppRoleID"], $Value=1, $isChecked=($a[$i]["CanBrowse"]=="1")?1:0, $Class='', $Display='', $Onclick='', $Disabled='')."</td>";
			$x .="<td>".$indexVar['libappraisal_ui']->Get_Checkbox('CanFill_'.$a[$i]["AppRoleID"], 'CanFill_'.$a[$i]["AppRoleID"], $Value=1, $isChecked=($a[$i]["CanFill"]=="1")?1:0, $Class='', $Display='', $Onclick='', $Disabled='')."</td>";
			$x .= "</tr>";
		}
		else{
			
		}
	}
}
$x .= "</table>";
$x .= "</div><br/>";
$x .= "<span class=\"row_content tabletextremark\">".$Lang['Appraisal']['TemplateSample']["ModifiedDate"].": ".(($dateTimeModified!="")?$dateTimeModified:" - ")."</span>";

$x .= "<input type=\"hidden\" id=\"TemplateID\" name=\"TemplateID\" value=".$templateID.">";
$x .= "<input type=\"hidden\" id=\"SecID\" name=\"SecID\" value=".$secID.">";
$x .= "<input type=\"hidden\" id=\"AppRoleIDStr\" name=\"AppRoleIDStr\" value=".$appRoleIDStr.">";

$htmlAry['contentTbl'] = $x;
// ============================== Define Button ==============================
if($secID != "NULL" && $secID!="undefined"){
	$htmlAry['previewBtn'] = "<div class=\"edit_bottom_v30\"><p class=\"spacer\"></p>".$indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnPreview'], "button", "goPreview(".$templateID.")", 'previewBtn')."</div>";;
}
$htmlAry['submitBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnSave'], "button", "goSubmit()", 'submitBtn');
//$htmlAry['resetBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnReset'], "button", "goReset()", 'resetBtn');
$htmlAry['backBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBackTemplate(".$templateID.")", 'backBtn');
// ============================== Define Button ==============================


//======================================================================== Sub Section ========================================================================//
if($secID != "NULL" && $secID!="undefined"){
	$x = $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['TemplateSample']['SDFSubSecInclude'])."\r\n";
	$x .= "<a name=\"anchSubSec\"></a>";
	$subBtnAry = array();
	$btnAry[] = array('new', 'javascript: goAddSubSec('.$templateID.','.$secID.');', $Lang['Appraisal']['BtnAddSubSec'], $subBtnAry);
	$x .= $indexVar['libappraisal_ui']->Get_Content_Tool_By_Array_v30($btnAry);
	
	$sql="SELECT SecID,TemplateID,SecCodChi,SecTitleChi,SecCodEng,SecTitleEng,ParentSecID
		FROM(
			SELECT SecID,TemplateID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,DisplayOrder,ParentSecID
			FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND ParentSecID=".IntegerSafe($secID)."
		) as ipsf
		ORDER BY DisplayOrder";
	//echo $sql."<br/><br/>";
	$a=$connection->returnResultSet($sql);
	
	$x .= "<div id=\"DetailLayer\"><span>";
	$x .= "<table id=\"dndTable\" class=\"common_table_list ClassDragAndDrop\">";
	$x .= "<thead>"."\r\n";
	$x .= "<tr>";
	$x .= "<th class=\"sub_row_top\" style=\"width:2%;\">#</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:30%;\">".$Lang['Appraisal']['TemplateSampleSection']['SubSecCodDescrEng']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:30%;\">".$Lang['Appraisal']['TemplateSampleSection']['SubSecCodDescrChi']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:10%;\">".$Lang['Appraisal']['TemplateSampleSection']['SubSecDisplayOrder']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:10%;\">".$Lang['Appraisal']['TemplateSampleSection']['SubSecDelete']."</th>";
	$x .= "</tr>";
	$x .= "</thead>";
	$x .= "<tbody>"."\r\n";
	for($i=0;$i<sizeof($a);$i++){
		$j=$i+1;
		$x .= "<tr id=\"".$a[$i]['SecID']."\">"."\r\n";
		$x .= "<td>".$j."</td>"."\r\n";
		$subSecTitleEng=($a[$i]["SecTitleEng"]==""&&$a[$i]["SecCodEng"]=="")?$Lang['Appraisal']['TemplateSampleSection']['NoTitleAndCod']:($a[$i]["SecCodEng"]."-".$a[$i]["SecTitleEng"]);
		$x .= "<td><a id='SubSectionEng_'".$a[$i]['SecID']." class=\"tablelink\" href=\"javascript:goEditSubSec(".$templateID.",".$a[$i]['ParentSecID'].",".$a[$i]['SecID'].")\">".$subSecTitleEng."</a></td>"."\r\n";
		$subSecTitleChi=($a[$i]["SecTitleChi"]==""&&$a[$i]["SecCodChi"]=="")?$Lang['Appraisal']['TemplateSampleSection']['NoTitleAndCod']:($a[$i]["SecCodChi"]."-".$a[$i]["SecTitleChi"]);
		$x .= "<td><a id='SubSectionChi_'".$a[$i]['SecID']." class=\"tablelink\" href=\"javascript:goEditSubSec(".$templateID.",".$a[$i]['ParentSecID'].",".$a[$i]['SecID'].")\">".$subSecTitleChi."</a></td>"."\r\n";
		$x .= "<td class=\"Dragable\"><div class=\"table_row_tool\"><a href=\"#\" class=\"move_order_dim\" title=\"".$Lang['SysMgr']['FormClassMapping']['Move']."\"></a></div></td>"."\r\n";
		$x .= "<td><div class=\"table_row_tool\" id='delete_".$a[$i]['SecID']."'><a href=\"javascript:void(0)\" class=\"delete_dim\" onclick=javascript:goDelete(".$templateID.",".$a[$i]['SecID'].") title=\"".$Lang['SysMgr']['FormClassMapping']['Delete']."\"></a></div></td>"."\r\n";
		$x .= "<input type=\"hidden\" id=\"SecID_".$a[$i]["SecID"]."\" name=\"SecID_".$a[$i]["SecID"]."\" value=".$a[$i]["SecID"]."></td>"."\r\n";
		$x .= "</tr>";
	}
	$x .= "</table>";
	$x .= "</div>";
	$htmlAry['contentTbl2'] = $x;
}


?>