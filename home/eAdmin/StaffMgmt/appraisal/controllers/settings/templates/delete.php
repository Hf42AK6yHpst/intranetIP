<?php

// ============================== Related Tables ==============================

// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
$indexVar['libappraisal'] = new libappraisal();

# Page Title
// ============================== Includes files/libraries ==============================

// ============================== Includes files/libraries ==============================

$connection = new libgeneralsettings();
$saveSuccess = true;

if($_GET["type"] != ""){
    //======================================================================== Delete template from list ========================================================================//
    $type = $_GET["type"];
    if($type=="list"){
        $returnPath = 'Location: ?task=settings'.$appraisalConfig['taskSeparator'].'templates'.$appraisalConfig['taskSeparator'].'list&returnMsgKey=%s';
        $templateIDAry= $_POST["templateIDAry"];
        for($i=0; $i<sizeof($templateIDAry); $i++){
            $templateID = $templateIDAry[$i];
            $sql="SELECT TemplateID,TemplateType FROM INTRANET_PA_S_FRMTPL WHERE TemplateID=".IntegerSafe($templateID).";";
            $a=$connection->returnResultSet($sql);
            $templateType=$a[0]["TemplateType"];
            
            if($templateType==0){
                //delete the pre-defined form
                $sql="DELETE FROM INTRANET_PA_S_FRMPRE WHERE TemplateID=".IntegerSafe($templateID).";";
                //echo $sql."<br/><br/>";
                $a=$connection->db_db_query($sql);
                $sql = "DELETE FROM INTRANET_PA_S_FRMPRE_SUB WHERE TemplateID=".IntegerSafe($templateID).";";
                //echo $sql."<br/><br/>";
                $a=$connection->db_db_query($sql);
                $sql="DELETE FROM INTRANET_PA_S_FRMTPL WHERE TemplateID=".IntegerSafe($templateID).";";
                //echo $sql."<br/><br/>";
                $a=$connection->db_db_query($sql);
            }
            else if($templateType==1){
                //delete the self-defined form
                $sql = "SELECT TemplateID FROM INTRANET_PA_T_FRMSUM WHERE TemplateID=".IntegerSafe($templateID).";";
                //echo $sql."<br/><br/>";
                $a=$connection->returnResultSet($sql);
                if(sizeof($a)==0){
                    $sql="SELECT SecID FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND ParentSecID IS NULL;";
                    $parentSecIDArr=$connection->returnResultSet($sql);
                    for($j=0;$j<sizeof($parentSecIDArr);$j++){
                        $sql="SELECT SecID FROM INTRANET_PA_S_FRMSEC WHERE ParentSecID=".IntegerSafe($parentSecIDArr[$j]["SecID"]).";";
                        $secIDArr=$connection->returnResultSet($sql);
                        for($i=0;$i<sizeof($secIDArr);$i++){
                            $sql="SELECT QCatID FROM INTRANET_PA_S_QCAT WHERE SecID=".IntegerSafe($secIDArr[$i]["SecID"]).";";
                            $a=$connection->returnResultSet($sql);
                            $qCatSQLStr=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($a,"QCatID");
                            $sql = "DELETE FROM INTRANET_PA_S_QLKS WHERE QCatID IN (".$qCatSQLStr.");";
                            //echo $sql."<br/><br/>";
                            $a=$connection->db_db_query($sql);
                            $sql = "DELETE FROM INTRANET_PA_S_QMTX WHERE QCatID IN (".$qCatSQLStr.");";
                            //echo $sql."<br/><br/>";
                            $a=$connection->db_db_query($sql);
                            $sql="DELETE FROM INTRANET_PA_S_QANS WHERE QID IN (SELECT QID FROM INTRANET_PA_S_QUES WHERE QCatID IN (".IntegerSafe($qCatSQLStr)."));";
                            //echo $sql."<br/><br/>";
                            $a=$connection->db_db_query($sql);
                            $sql="DELETE FROM INTRANET_PA_S_QUES WHERE QCatID IN (".$qCatSQLStr.");";
                            //echo $sql."<br/><br/>";
                            $a=$connection->db_db_query($sql);
                            $sql="DELETE FROM INTRANET_PA_S_QCAT WHERE QCatID IN (".$qCatSQLStr.");";
                            //echo $sql."<br/><br/>";
                            $a=$connection->db_db_query($sql);
                            $sql="DELETE FROM INTRANET_PA_S_SECROL WHERE SecID=".IntegerSafe($secIDArr[$i]["SecID"]).";";
                            //echo $sql."<br/><br/>";
                            $a=$connection->db_db_query($sql);
                            $sql="DELETE FROM INTRANET_PA_S_FRMSEC WHERE SecID=".IntegerSafe($secIDArr[$i]["SecID"]).";";
                            //echo $sql."<br/><br/>";
                            $a=$connection->db_db_query($sql);
                        }
                        $sql="DELETE FROM INTRANET_PA_S_FRMSEC WHERE SecID=".IntegerSafe($parentSecIDArr[$j]["SecID"]).";";
                        //echo $sql."<br/><br/>";
                        $a=$connection->db_db_query($sql);
                    }
                    $sql="DELETE FROM INTRANET_PA_S_FRMTPL WHERE TemplateID=".$templateID.";";
                    //echo $sql."<br/><br/>";
                    $a=$connection->db_db_query($sql);
                }
            }
            else{
                $saveSuccess = false;
                break;
            }
        }
    }
}
//======================================================================== Delete template items ========================================================================//
else{
    $type = $_POST["type"];
    $templateID = $_POST["templateID"];
    if(empty($templateID)){
        $templateID = $_POST["TemplateID"];
    }
    //$sql = "SELECT TemplateID FROM INTRANET_PA_T_FRMSUM WHERE TemplateID=".$templateID.";";
    $sql = "SELECT TemplateID FROM INTRANET_PA_T_FRMSUM WHERE TemplateID=".IntegerSafe($templateID).";";
    //echo $sql."<br/>";
    $a=$connection->returnResultSet($sql);
    if(sizeof($a)==0){
        if($type=="SDFDelete"){
            $templateID = $_POST["templateID"];
            $sql="SELECT SecID FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND ParentSecID IS NULL;";
            $parentSecIDArr=$connection->returnResultSet($sql);
            for($j=0;$j<sizeof($parentSecIDArr);$j++){
                $sql="SELECT SecID FROM INTRANET_PA_S_FRMSEC WHERE ParentSecID=".IntegerSafe($parentSecIDArr[$j]["SecID"]).";";
                $secIDArr=$connection->returnResultSet($sql);
                for($i=0;$i<sizeof($secIDArr);$i++){
                    $sql="SELECT QCatID FROM INTRANET_PA_S_QCAT WHERE SecID=".IntegerSafe($secIDArr[$i]["SecID"]).";";
                    $a=$connection->returnResultSet($sql);
                    $qCatSQLStr=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($sql,"QCatID");
                    $sql = "DELETE FROM INTRANET_PA_S_QLKS WHERE QCatID IN (".$qCatSQLStr.");";
                    //echo $sql."<br/><br/>";
                    $a=$connection->db_db_query($sql);
                    $sql = "DELETE FROM INTRANET_PA_S_QMTX WHERE QCatID IN (".$qCatSQLStr.");";
                    //echo $sql."<br/><br/>";
                    $a=$connection->db_db_query($sql);
                    $sql="DELETE FROM INTRANET_PA_S_QANS WHERE QID IN (SELECT QID FROM INTRANET_PA_S_QUES WHERE QCatID IN (".IntegerSafe($qCatSQLStr)."));";
                    //echo $sql."<br/><br/>";
                    $a=$connection->db_db_query($sql);
                    $sql="DELETE FROM INTRANET_PA_S_QUES WHERE QCatID IN (".$qCatSQLStr.");";
                    //echo $sql."<br/><br/>";
                    $a=$connection->db_db_query($sql);
                    $sql="DELETE FROM INTRANET_PA_S_QCAT WHERE QCatID IN (".$qCatSQLStr.");";
                    //echo $sql."<br/><br/>";
                    $a=$connection->db_db_query($sql);
                    $sql="DELETE FROM INTRANET_PA_S_SECROL WHERE SecID=".IntegerSafe($secIDArr[$i]["SecID"]).";";
                    //echo $sql."<br/><br/>";
                    $a=$connection->db_db_query($sql);
                    $sql="DELETE FROM INTRANET_PA_S_FRMSEC WHERE SecID=".IntegerSafe($secIDArr[$i]["SecID"]).";";
                    //echo $sql."<br/><br/>";
                    $a=$connection->db_db_query($sql);
                }
                $sql="DELETE FROM INTRANET_PA_S_FRMSEC WHERE SecID=".IntegerSafe($parentSecIDArr[$j]["SecID"]).";";
                //echo $sql."<br/><br/>";
                $a=$connection->db_db_query($sql);
            }
            $sql="DELETE FROM INTRANET_PA_S_FRMTPL WHERE TemplateID=".$templateID.";";
            //echo $sql."<br/><br/>";
            $a=$connection->db_db_query($sql);
        }
        else if($type=="SDFParentSecDelete"){
            $secID = $_POST["parentSecID"];
            $sql="SELECT SecID FROM INTRANET_PA_S_FRMSEC WHERE ParentSecID=".IntegerSafe($secID).";";
            //echo $sql."<br/><br/>";
            $secIDArr=$connection->returnResultSet($sql);
            for($i=0;$i<sizeof($secIDArr);$i++){
                $sql="SELECT QCatID FROM INTRANET_PA_S_QCAT WHERE SecID=".$secIDArr[$i]["SecID"].";";
                //echo $sql."<br/><br/>";
                $a=$connection->returnResultSet($sql);
                $qCatSQLStr=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($a,"QCatID");
                $sql = "DELETE FROM INTRANET_PA_S_QLKS WHERE QCatID IN (".$qCatSQLStr.");";
                //echo $sql."<br/><br/>";
                $a=$connection->db_db_query($sql);
                $sql = "DELETE FROM INTRANET_PA_S_QMTX WHERE QCatID IN (".$qCatSQLStr.");";
                //echo $sql."<br/><br/>";
                $a=$connection->db_db_query($sql);
                $sql="DELETE FROM INTRANET_PA_S_QANS WHERE QID IN (SELECT QID FROM INTRANET_PA_S_QUES WHERE QCatID IN (".IntegerSafe($qCatSQLStr)."));";
                //echo $sql."<br/><br/>";
                $a=$connection->db_db_query($sql);
                $sql="DELETE FROM INTRANET_PA_S_QUES WHERE QCatID IN (".$qCatSQLStr.");";
                //echo $sql."<br/><br/>";
                $a=$connection->db_db_query($sql);
                $sql="DELETE FROM INTRANET_PA_S_QCAT WHERE QCatID IN (".$qCatSQLStr.");";
                //echo $sql."<br/><br/>";
                $a=$connection->db_db_query($sql);
                $sql="DELETE FROM INTRANET_PA_S_SECROL WHERE SecID=".IntegerSafe($secIDArr[$i]["SecID"]).";";
                //echo $sql."<br/><br/>";
                $a=$connection->db_db_query($sql);
                $sql="DELETE FROM INTRANET_PA_S_FRMSEC WHERE SecID=".IntegerSafe($secIDArr[$i]["SecID"]).";";
                //echo $sql."<br/><br/>";
                $a=$connection->db_db_query($sql);
            }
            $sql="DELETE FROM INTRANET_PA_S_FRMSEC WHERE SecID=".IntegerSafe($secID).";";
            //echo $sql."<br/><br/>";
            $a=$connection->db_db_query($sql);
        }
        else if($type=="SDFSecDelete"){
            $secID = $_POST["subSecID"];
            $sql="SELECT QCatID FROM INTRANET_PA_S_QCAT WHERE SecID=".IntegerSafe($secID).";";
            //echo $sql."<br/><br/>";
            $a=$connection->returnResultSet($sql);
            $qCatSQLStr=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($a,"QCatID");
            $sql = "DELETE FROM INTRANET_PA_S_QLKS WHERE QCatID IN (".$qCatSQLStr.");";
            //echo $sql."<br/><br/>";
            $a=$connection->db_db_query($sql);
            $sql = "DELETE FROM INTRANET_PA_S_QMTX WHERE QCatID IN (".$qCatSQLStr.");";
            //echo $sql."<br/><br/>";
            $a=$connection->db_db_query($sql);
            $sql="DELETE FROM INTRANET_PA_S_QANS WHERE QID IN (SELECT QID FROM INTRANET_PA_S_QUES WHERE QCatID IN (".IntegerSafe($qCatSQLStr)."));";
            //echo $sql."<br/><br/>";
            $a=$connection->db_db_query($sql);
            $sql="DELETE FROM INTRANET_PA_S_QUES WHERE QCatID IN (".$qCatSQLStr.");";
            //echo $sql."<br/><br/>";
            $a=$connection->db_db_query($sql);
            $sql="DELETE FROM INTRANET_PA_S_QCAT WHERE QCatID IN (".$qCatSQLStr.");";
            //echo $sql."<br/><br/>";
            $a=$connection->db_db_query($sql);
            $sql="DELETE FROM INTRANET_PA_S_SECROL WHERE SecID=".IntegerSafe($secID).";";
            //echo $sql."<br/><br/>";
            $a=$connection->db_db_query($sql);
            $sql="DELETE FROM INTRANET_PA_S_FRMSEC WHERE SecID=".IntegerSafe($secID).";";
            //echo $sql."<br/><br/>";
            $a=$connection->db_db_query($sql);
        }
        else if($type=="SDFQCatDelete"){
            $qCatID = $_POST["qCatID"];
            $sql = "DELETE FROM INTRANET_PA_S_QLKS WHERE QCatID=".IntegerSafe($qCatID).";";
            //echo $sql."<br/><br/>";
            $a=$connection->db_db_query($sql);
            $sql = "DELETE FROM INTRANET_PA_S_QMTX WHERE QCatID=".IntegerSafe($qCatID).";";
            //echo $sql."<br/><br/>";
            $a=$connection->db_db_query($sql);
            $sql="DELETE FROM INTRANET_PA_S_QANS WHERE QID IN (SELECT QID FROM INTRANET_PA_S_QUES WHERE QCatID=".IntegerSafe($qCatID).");";
            //echo $sql."<br/><br/>";
            $a=$connection->db_db_query($sql);
            $sql="DELETE FROM INTRANET_PA_S_QUES WHERE QCatID=".IntegerSafe($qCatID).";";
            //echo $sql."<br/><br/>";
            $a=$connection->db_db_query($sql);
            $sql="DELETE FROM INTRANET_PA_S_QCAT WHERE QCatID=".IntegerSafe($qCatID).";";
            //echo $sql."<br/><br/>";
            $a=$connection->db_db_query($sql);
        }
        else if($type=="SDFLikertDelete"){
            $markID = $_POST["markID"];
            $sql = "DELETE FROM INTRANET_PA_S_QLKS WHERE MarkID=".IntegerSafe($markID).";";
            //echo $sql."<br/><br/>";
            $a=$connection->db_db_query($sql);
        }
        else if($type=="SDFMatrixDelete"){
            $mtxFldID = $_POST["mtxFldID"];
            $sql = "DELETE FROM INTRANET_PA_S_QMTX WHERE MtxFldID=".IntegerSafe($mtxFldID).";";
            //echo $sql."<br/><br/>";
            $a=$connection->db_db_query($sql);
        }
        else if($type=="SDFQuesDelete"){
            $qID = $_POST["qID"];
            $sql="DELETE FROM INTRANET_PA_S_QANS WHERE QID IN (SELECT QID FROM INTRANET_PA_S_QUES WHERE QID=".IntegerSafe($qID).");";
            //echo $sql."<br/><br/>";
            $a=$connection->db_db_query($sql);
            $sql="DELETE FROM INTRANET_PA_S_QUES WHERE QID=".IntegerSafe($qID).";";
            //echo $sql."<br/><br/>";
            $a=$connection->db_db_query($sql);
        }
        else if($type=="SDFAnsDelete"){
            $ansID = $_POST["ansID"];
            $sql="DELETE FROM INTRANET_PA_S_QANS WHERE QAnsID=".$ansID.";";
            //echo $sql."<br/><br/>";
            $a=$connection->db_db_query($sql);
        }
    }
    else{
        $saveSuccess = false;
        //		break;
    }
}
$returnMsgKey = ($saveSuccess)? 'DeleteSuccess' : 'DeleteUnsuccess';
$returnPath=sprintf($returnPath,$returnMsgKey).'&returnMsgKey='.$returnMsgKey;
// echo $returnPath."<br/><br/>";
//header('Location: ?task=management'.$appraisalConfig['taskSeparator'].'appraisal_period'.$appraisalConfig['taskSeparator'].'list&returnMsgKey='.$returnMsgKey);
header($returnPath);
?>