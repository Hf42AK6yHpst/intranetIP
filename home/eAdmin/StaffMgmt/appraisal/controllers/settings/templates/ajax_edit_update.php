<?php 

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
$connection = new libgeneralsettings();

$recordOrder = $_POST["recordOrder"];
$recordOrderArr = explode(",",$recordOrder);
$displayOrder = $_POST["displayOrder"];
$displayOrderArr = explode(",",$displayOrder);
$TemplateID = $_POST["TemplateID"];
$SecID = $_POST["SecID"];
$Type = $_POST["Type"];
$QCatID=$_POST["QCatID"];

if($Type=="Template"){
	for($i=0; $i<sizeof($recordOrderArr); $i++){
		if($recordOrderArr[$i]!=""){
			$sql = 'UPDATE INTRANET_PA_S_FRMPRE  ';
			$sql .= 'SET DisplayOrder='.IntegerSafe($displayOrderArr[$i]).',DateTimeModified=NOW(),ModifiedBy_UserID='.$_SESSION['UserID'].' ';
			$sql .= 'WHERE TemplateID='.IntegerSafe($TemplateID).' AND DataTypeID='.IntegerSafe($recordOrderArr[$i]).';';
			//echo $sql."<br/><br/>";
			$connection->db_db_query($sql);
		}		
	}
}
else if($Type=="Section"){
	for($i=0; $i<sizeof($recordOrderArr); $i++){
		if($recordOrderArr[$i]!=""){
			$sql = 'UPDATE INTRANET_PA_S_FRMSEC ';
			$sql .= 'SET DisplayOrder='.IntegerSafe($displayOrderArr[$i]).',DateTimeModified=NOW(),ModifiedBy_UserID='.$_SESSION['UserID'].' ';
			$sql .= 'WHERE TemplateID='.IntegerSafe($TemplateID).' AND SecID='.IntegerSafe($recordOrderArr[$i]).' AND ParentSecID IS NULL;';
			//echo $sql."<br/><br/>";
			$connection->db_db_query($sql);
		}
	}
}
else if($Type=="SubSection"){
	for($i=0; $i<sizeof($recordOrderArr); $i++){
		if($recordOrderArr[$i]!=""){
			$sql = 'UPDATE INTRANET_PA_S_FRMSEC ';
			$sql .= 'SET DisplayOrder='.IntegerSafe($displayOrderArr[$i]).',DateTimeModified=NOW(),ModifiedBy_UserID='.$_SESSION['UserID'].' ';
			$sql .= 'WHERE TemplateID='.IntegerSafe($TemplateID).' AND SecID='.IntegerSafe($recordOrderArr[$i]).' AND ParentSecID IS NOT NULL ;';
			//echo $sql."<br/><br/>";
			$connection->db_db_query($sql);
		}
	}
}
else if($Type=="QCat"){
	for($i=0; $i<sizeof($recordOrderArr); $i++){
		if($recordOrderArr[$i]!=""){
			$sql = 'UPDATE INTRANET_PA_S_QCAT  ';
			$sql .= 'SET DisplayOrder='.IntegerSafe($displayOrderArr[$i]).',DateTimeModified=NOW(),ModifiedBy_UserID='.$_SESSION['UserID'].' ';
			$sql .= 'WHERE SecID='.IntegerSafe($SecID).' AND QCatID='.IntegerSafe($recordOrderArr[$i]);
			//echo $sql."<br/><br/>";
			$connection->db_db_query($sql);
		}
	}
}
else if($Type=="Likert"){
	for($i=0; $i<sizeof($recordOrderArr); $i++){
		if($recordOrderArr[$i]!=""){
			$sql = 'UPDATE INTRANET_PA_S_QLKS ';
			$sql .= 'SET DisplayOrder='.IntegerSafe($displayOrderArr[$i]).',DateTimeModified=NOW(),ModifiedBy_UserID='.$_SESSION['UserID'].' ';
			$sql .= 'WHERE QCatID='.IntegerSafe($QCatID).' AND MarkID='.IntegerSafe($recordOrderArr[$i]);
			//echo $sql."<br/><br/>";
			$connection->db_db_query($sql);
		}
	}
}
else if($Type=="Matrix"){
	for($i=0; $i<sizeof($recordOrderArr); $i++){
		if($recordOrderArr[$i]!=""){
			$sql = 'UPDATE INTRANET_PA_S_QMTX ';
			$sql .= 'SET DisplayOrder='.IntegerSafe($displayOrderArr[$i]).',DateTimeModified=NOW(),ModifiedBy_UserID='.$_SESSION['UserID'].' ';
			$sql .= 'WHERE QCatID='.IntegerSafe($QCatID).' AND MtxFldID='.IntegerSafe($recordOrderArr[$i]);
			//echo $sql."<br/><br/>";
			$connection->db_db_query($sql);
		}
	}
}
else if($Type=="Ques"){
	for($i=0; $i<sizeof($recordOrderArr); $i++){
		if($recordOrderArr[$i]!=""){
			$sql = 'UPDATE INTRANET_PA_S_QUES ';
			$sql .= 'SET DisplayOrder='.IntegerSafe($displayOrderArr[$i]).',DateTimeModified=NOW(),ModifiedBy_UserID='.$_SESSION['UserID'].' ';
			$sql .= 'WHERE QCatID='.IntegerSafe($QCatID).' AND QID='.IntegerSafe($recordOrderArr[$i]);
			//echo $sql."<br/><br/>";
			$connection->db_db_query($sql);
		}
	}
}
else if($Type=="TemplateOrder"){
	for($i=0; $i<sizeof($recordOrderArr); $i++){
		if($recordOrderArr[$i]!=""){
			$sql = 'UPDATE INTRANET_PA_S_FRMTPL ';
			$sql .= 'SET DisplayOrder='.IntegerSafe($displayOrderArr[$i]).',DateTimeModified=NOW(),ModifiedBy_UserID='.$_SESSION['UserID'].' ';
			$sql .= 'WHERE TemplateID='.IntegerSafe($recordOrderArr[$i]);
			//echo $sql."<br/><br/>";
			$connection->db_db_query($sql);
		}
	}
}
else if($Type=="QAns"){
	for($i=0; $i<sizeof($recordOrderArr); $i++){
		if($recordOrderArr[$i]!=""){
			$sql = 'UPDATE INTRANET_PA_S_QANS ';
			$sql .= 'SET DisplayOrder='.IntegerSafe($displayOrderArr[$i]).',DateTimeModified=NOW(),ModifiedBy_UserID='.$_SESSION['UserID'].' ';
			$sql .= 'WHERE QID='.IntegerSafe($QID).' AND QAnsID='.IntegerSafe($recordOrderArr[$i]);
			//echo $sql."<br/><br/>";
			$connection->db_db_query($sql);
		}
	}
}
else if($Type=="SDFParentSecAddPageBreak"){
	if($_POST['SetPageBreak']==1){
		$sql = "UPDATE INTRANET_PA_S_FRMSEC ";
		$sql .= "SET PageBreakAfter=1".',DateTimeModified=NOW(),ModifiedBy_UserID='.$_SESSION['UserID'].' ';
		$sql .= "WHERE SecID=".IntegerSafe($_POST['ParentSecID'])." AND TemplateID=".IntegerSafe($_POST['TemplateID']);
	}else if($_POST['SetPageBreak']==0){
		$sql = "UPDATE INTRANET_PA_S_FRMSEC ";
		$sql .= "SET PageBreakAfter=0".',DateTimeModified=NOW(),ModifiedBy_UserID='.$_SESSION['UserID'].' ';
		$sql .= "WHERE SecID=".IntegerSafe($_POST['ParentSecID'])." AND TemplateID=".IntegerSafe($_POST['TemplateID']);
	}
	echo $sql."<br/><br/>";
	$connection->db_db_query($sql);
}
else if ($Type=="TemplateSubData"){
	for($i=0; $i < count($recordOrderArr); $i++){
		if($recordOrderArr[$i]=="")continue;
		$dataType = explode("_",$recordOrderArr[$i]);
		$sql = "UPDATE INTRANET_PA_S_FRMPRE_SUB SET DisplayOrder='".$displayOrderArr[$i]."' WHERE TemplateID='".$_POST['TemplateID']."' AND DataTypeID='".$dataType[0]."' AND DataSubTypeID='".$dataType[1]."'";
		echo $sql."<br/><br/>";
		$connection->db_db_query($sql);
	}
}
else if ($Type=="PDFSubSectionSave"){
	$DataSubTypeIDList = $_POST['DataSubTypeID'];
	for($i=0; $i < count($DataSubTypeIDList); $i++){
		$isInclude = ($_POST['Include_'.$_POST['DataTypeID'][$i].'_'.$_POST['DataSubTypeID'][$i]] == null)?0:1;
		$sql = "UPDATE INTRANET_PA_S_FRMPRE_SUB SET DescrChi='".$_POST['DSDescrChi'][$i]."', DescrEng='".$_POST['DSDescrEng'][$i]."', Incl='".$isInclude."', ModifiedBy_UserID='".$_SESSION['UserID']."', DateTimeModified=NOW() WHERE TemplateID='".$_POST['TemplateID']."' AND DataTypeID='".$_POST['DataTypeID'][$i]."' AND DataSubTypeID='".$_POST['DataSubTypeID'][$i]."'";
		echo $sql."<br/><br/>";
		$connection->db_db_query($sql);
	}
}
else if($Type=="PDFAttendanceTableOrientation"){
	$sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='TeacherApprisal' AND SettingName='PreDefinedFormAttendanceTableOrientation'";
	$settingValue = current($connection->returnVector($sql));
	if(empty($settingValue)){
		$settingValueArr = array($_POST['TemplateID']=>$_POST['orientation']);
	}else{
		$settingValueArr = unserialize($settingValue);
		$settingValueArr[$_POST['TemplateID']] = $_POST['orientation'];
	}
	$connection->Save_General_Setting('TeacherApprisal', array("PreDefinedFormAttendanceTableOrientation"=>serialize($settingValueArr)));
}
?>