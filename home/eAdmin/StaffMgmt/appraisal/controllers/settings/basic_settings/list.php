<?php
// ============================== Related Tables ==============================
// GENERAL_SETTING
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['SettingsBasicSettings']);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libappraisal_ui']->Include_Thickbox_JS_CSS();

// ============================== Includes files/libraries ==============================

// ============================== Transactional data ==============================

$x = "";
$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['SettingsBasicSettings']);
$x .= '<table class="form_table_v30">'."\r\n";
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['Appraisal']['SettingsExcludeTeachers'].'</td>'."\r\n";
$x .= '<td>'."\r\n";
$x .= $indexVar['libappraisal']->getExemptTeacherDisplay("nameList");
$x .= '</td>'."\r\n";
$x .= '</tr>'."\r\n";
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['Appraisal']['SettingsPrintMcDisplayMode'].'</td>'."\r\n";
$x .= '<td>'."\r\n";
$x .= $indexVar['libappraisal']->getPrintMcDisplayModeDisplay();
$x .= '</td>'."\r\n";
$x .= '</tr>'."\r\n";
if($sys_custom['eAppraisal']['AverageReport']){
    $x .= '<tr>'."\r\n";
    $x .= '<td class="field_title">'.$Lang['Appraisal']['SettingsReportDoubleCountPerson'].'</td>'."\r\n";
    $x .= '<td>'."\r\n";
    $x .= $indexVar['libappraisal']->getReportDoubleCountPerson("nameList");
    $x .= '</td>'."\r\n";
    $x .= '</tr>'."\r\n";
}
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['Appraisal']['SettingsReportDisplayPerson'].'<br><br>'.$Lang['Appraisal']['SettingsReportDisplayPersonRemarks'].'</td>'."\r\n";
$x .= '<td>'."\r\n";
$x .= $indexVar['libappraisal']->getReportDisplayPerson("nameList");
$x .= '</td>'."\r\n";
$x .= '</tr>'."\r\n";
if($sys_custom['eAppraisal']['3YearsCPD']==true){
    $x .= '<tr>'."\r\n";
    $x .= '<td class="field_title">'.$Lang['Appraisal']['SettingsCPDRecords'].'</td>'."\r\n";
    $x .= '<td>'."\r\n";
    $x .= $indexVar['libappraisal']->getCPDSetting();
    $x .= '</td>'."\r\n";
    $x .= '</tr>'."\r\n";
}
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['Appraisal']['SettingsCPDDuration'] .'</td>'."\r\n";
$x .= '<td>'."\r\n";
$x .= $indexVar['libappraisal']->getCPDDurationSetting();
$x .= '</td>'."\r\n";
$x .= '</tr>'."\r\n";
$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['Appraisal']['SettingsCPDShowingTotalHours'] .'</td>'."\r\n";
$x .= '<td>'."\r\n";
$x .= $indexVar['libappraisal']->getCPDShowingTotalHoursSetting();
$x .= '</td>'."\r\n";
$x .= '</tr>'."\r\n";

$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['Appraisal']['SettingsAttendanceReasons'].'</td>'."\r\n";
$x .= '<td>'."\r\n";
$x .= $indexVar['libappraisal']->getAttendanceReasons();
$x .= '</td>'."\r\n";
$x .= '</tr>'."\r\n";

$x .= '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['Appraisal']['SettingsColumnReasons'] .'</td>'."\r\n";
$x .= '<td>'."\r\n";
$x .= $indexVar['libappraisal']->getAttendanceTableColumn();
$x .= '</td>'."\r\n";
$x .= '</tr>'."\r\n";

$x .= '</table>';


$htmlAry['contentTbl'] = $x;

$x = "";
if($plugin['attendancestaff'] == true && ($sys_custom['eAppraisal']['ltmpsTransferAttendnanceDataToForm'] == true || $sys_custom['eAppraisal']['generalTransferAttendnanceDataToForm'] == true)){
    $x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['SettingsAttendanceImport']);
    $x .= '<table class="form_table_v30">'."\r\n";
    $x .= '<tr>'."\r\n";
    $x .= '<td class="field_title">'.$Lang['Appraisal']['SettingsAttendanceImport'].'</td>'."\r\n";
    $x .= '<td>'."\r\n";
    $x .= $indexVar['libappraisal']->getAttendanceReasonMapping();
    $x .= '</td>'."\r\n";
    $x .= '</tr>'."\r\n";
    $x .= '</table>';
}

$htmlAry['contentMappingTbl'] = $x;
// ============================== Transactional data ==============================

// ============================== Define Button ==============================
$htmlAry['editBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Edit'], "button", "goEdit()", 'editBtn');
if($plugin['attendancestaff'] == true && ($sys_custom['eAppraisal']['ltmpsTransferAttendnanceDataToForm'] == true || $sys_custom['eAppraisal']['generalTransferAttendnanceDataToForm'] == true)){
    $htmlAry['editMappingBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Edit'], "button", "goMappingEdit()", 'editMappingBtn');
}
// ============================== Define Button ==============================
?>