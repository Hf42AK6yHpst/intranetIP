<?php 

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/liblog.php");
include_once($PATH_WRT_ROOT."includes/json.php");
$lgeneralsettings = new libgeneralsettings();
$liblog = new liblog();
$JSON_obj = new JSON_obj();

$saveSuccess = true;
if($_POST['editPart']=='1'){
	$data = array();
	$data['ExcludeAssessmentTeacher'] = IntegerSafe($_POST["ExcludeAssessmentTeacherIDStr"]);
	$data['ReportDisplayPerson'] = IntegerSafe($_POST["ReportDisplayPersonIDStr"]);
	$data['ReportDoubleCountPerson'] = IntegerSafe($_POST["ReportDoubleCountPersonIDStr"]);
	$data['CPDSetting'] = serialize(array("Period"=>IntegerSafe($_POST["cpdPeriod"]), "SelectedYear"=>IntegerSafe($_POST['AcademicYearID'])));
	$data['CPDDurationSetting'] = IntegerSafe($_POST['CPDDurationSetting']);
	$data['CPDShowingTotalHourSetting'] = IntegerSafe($_POST['CPDShowingTotalHourSetting']);
	$data['PrintMcDisplayMode'] = $_POST['PrintMcDisplayMode'];
	
	$lgeneralsettings->Save_General_Setting("TeacherApprisal", $data);
	
	$liblog->INSERT_LOG($Module='TeacherApprisal', $Section='BASIC_SETTING_UPDATE', $RecordDetail=$JSON_obj->encode($data));
	
	// Attendance Setting
	for($i=0; $i < count($_POST['reason_chi']); $i++){
		if($_POST['reason_chi'][$i]!= "" || $_POST['reason_eng'][$i]!= ""){
			$countTotalBefore = ($_POST['reason_show_total_before']==$i)?"1":"0";
			if($_POST['reason_id'][$i] == "" && $_POST['is_delete'][$i]==""){
				$sql = "INSERT INTO INTRANET_PA_S_ATTEDNACE_REASON (TAReasonNameChi, TAReasonNameEng, ReasonUnit, CountTotalBeforeThisReason) VALUES ('".$_POST['reason_chi'][$i]."', '".$_POST['reason_eng'][$i]."', '".$_POST['reason_unit'][$i]."', '".$countTotalBefore."')";
			}else{
				if($_POST['is_delete'][$i]=="1"){
					$sql = "DELETE FROM INTRANET_PA_S_ATTEDNACE_REASON WHERE TAReasonID='".$_POST['reason_id'][$i]."'";	
				}else{
					$sql = "UPDATE INTRANET_PA_S_ATTEDNACE_REASON SET TAReasonNameChi='".$_POST['reason_chi'][$i]."', TAReasonNameEng='".$_POST['reason_eng'][$i]."', ReasonUnit='".$_POST['reason_unit'][$i]."', CountTotalBeforeThisReason='".$countTotalBefore."' WHERE TAReasonID='".$_POST['reason_id'][$i]."'";
				}
			}
			$indexVar['libappraisal']->db_db_query($sql);
		}
	}
	for($i=0; $i < count($_POST['column_chi']); $i++){
		if($_POST['column_chi'][$i]!= "" || $_POST['column_eng'][$i]!= ""){
			if($_POST['periodStart_'.$_POST['row_id'][$i]]==""){
				$_POST['periodStart_'.$_POST['row_id'][$i]] = "0000:00:00";
			}
			if($_POST['periodEnd_'.$_POST['row_id'][$i]]==""){
				$_POST['periodEnd_'.$_POST['row_id'][$i]] = "0000:00:00";
			}
			if($_POST['column_id'][$i] == "" && $_POST['is_col_delete'][$i]==""){
				$sql = "INSERT INTO INTRANET_PA_S_ATTEDNACE_COLUMN (TAColumnNameChi, TAColumnNameEng, PeriodStart, PeriodEnd, DisplayOrder) VALUES ('".$_POST['column_chi'][$i]."', '".$_POST['column_eng'][$i]."', '".$_POST['periodStart_'.$_POST['row_id'][$i]]."', '".$_POST['periodEnd_'.$_POST['row_id'][$i]]."', '".($i+1)."')";
			}else{
				if($_POST['is_col_delete'][$i]=="1"){
					$sql = "DELETE FROM INTRANET_PA_S_ATTEDNACE_COLUMN WHERE TAColumnID='".$_POST['column_id'][$i]."'";	
				}else{
					$sql = "UPDATE INTRANET_PA_S_ATTEDNACE_COLUMN SET TAColumnNameChi='".$_POST['column_chi'][$i]."', TAColumnNameEng='".$_POST['column_eng'][$i]."', PeriodStart='".$_POST['periodStart_'.$_POST['row_id'][$i]]."', PeriodEnd='".$_POST['periodEnd_'.$_POST['row_id'][$i]]."', DisplayOrder='".($i+1)."' WHERE TAColumnID='".$_POST['column_id'][$i]."'";
				}
			}
			$indexVar['libappraisal']->db_db_query($sql);
		}
	}	
}elseif($_POST['editPart']=='2'){
	for($i=0; $i < count($_POST['taReasonList']); $i++){
		if($_POST['taReasonList'][$i]!= ""){
			if($_POST['mapID'][$i] == ""){
				$sql = "INSERT INTO INTRANET_PA_S_ATTEDNACE_REASON_MAPPING (TypeID, ReasonID, TAReasonID, Days) VALUES ('".$_POST['attendanceTypeID'][$i]."','".$_POST['reasonID'][$i]."','".$_POST['taReasonList'][$i]."', '".$_POST['taReasonDaysList'][$i]."')";
			}else{
				$sql = "UPDATE INTRANET_PA_S_ATTEDNACE_REASON_MAPPING SET TypeID='".$_POST['attendanceTypeID'][$i]."', ReasonID='".$_POST['reasonID'][$i]."', TAReasonID='".$_POST['taReasonList'][$i]."', Days='".$_POST['taReasonDaysList'][$i]."' WHERE MapID='".$_POST['mapID'][$i]."'";
			}
			$indexVar['libappraisal']->db_db_query($sql);
		}
	}
}
$returnMsgKey = ($saveSuccess)? 'UpdateSuccess' : 'UpdateUnsuccess';
header('Location: ?task=settings'.$appraisalConfig['taskSeparator'].'basic_settings'.$appraisalConfig['taskSeparator'].'list&returnMsgKey='.$returnMsgKey);

?>