<?php
@ini_set("memory_limit","1024M");
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
//include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
include_once($PATH_WRT_ROOT."includes/appraisal/gen_form_class.php");
require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");


# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$connection = new libgeneralsettings();
$rlsNo=$_GET["rlsNo"];
$recordID=$_GET["recordID"];
$batchID=$_GET["batchID"];
$inclObs=$_GET["inclObs"];
$displayName = $_GET["displayName"];
$displayDate = $_GET["displayDate"];
$loginID=$_GET["loginID"];

$sysDate=date("Y-m-d");
/*
$x .= "<div align=\"right\">"."\r\n";
$x .= "<input type=\"button\" class=\"formsubbutton\" onclick=\"javascript:window.print();\" name=\"PrintBtn\" id=\"PrintBtn\" value=".$Lang['Appraisal']['ReportPrint']." onmouseover=\"this.className='formsubbuttonon'\" onmouseout=\"this.className='formsubbutton'\">";
$x .= "<br/><div id=\"resizeFontLoadingDiv\" style=\"display:none; float:right;\">
	<img src=\"/images/2009a/indicator.gif\"><span class=\"tabletextremark\"><span style=\"color:red;\">".$Lang['Appraisal']['ReportProcessing'] ."</span></span></div>";
$x .= "</div>"."\r\n";
*/


$indexVar['libappraisal'] = new libappraisal();
$indexVar['libappraisal_ui'] = new libappraisal_ui();
global $intranet_session_language, $Lang;

$connection = new libgeneralsettings();
$genform = new genform();

$sql="SELECT RlsNo,iptf.RecordID,iptf.CycleID,iptf.TemplateID,UserID,BatchID
	FROM(
		SELECT RecordID,CycleID,TemplateID,UserID FROM INTRANET_PA_T_FRMSUM WHERE RecordID=".IntegerSafe($recordID)."
	) as iptf
	INNER JOIN(
		SELECT RlsNo,RecordID,BatchID FROM INTRANET_PA_T_FRMSUB WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)."
	) as iptfrmsub ON iptf.RecordID=iptfrmsub.RecordID
	INNER JOIN(
		SELECT TemplateID,IsObs FROM INTRANET_PA_S_FRMTPL
	) as ipsfrmtpl ON iptf.TemplateID=ipsfrmtpl.TemplateID
";
//echo $sql."<br/><br/>";
$a=$connection->returnResultSet($sql);
$formContent = array();

$x = "";
if($displayName || $displayDate){
	$nameField = ($displayName)?$indexVar['libappraisal']->getLangSQL("u.ChineseName","u.EnglishName")." as UserName":"";
	$dateField = ($displayDate)?"sub.SubDate":"";
	$fieldArr = array_filter(array($nameField,$dateField));
	$aprInfoSql = "SELECT ".implode(",",$fieldArr)." FROM INTRANET_PA_T_FRMSUB sub 
				INNER JOIN INTRANET_PA_T_FRMSUM sum ON sub.RecordID=sum.RecordID 
				INNER JOIN ".$appraisalConfig['INTRANET_USER']." u ON sum.AprID=u.UserID
				WHERE sub.RecordID=".$a[0]["RecordID"]." ORDER BY SubDate DESC LIMIT 1";
	$aprInfo = current($connection->returnArray($aprInfoSql));
	$x = "<div>";
	if($displayName){
		$x.="<span style=\"float:left;\"><b>".$Lang['Appraisal']['CycleTemplate']['AssignedAppraiser']."</b>: ".$aprInfo['UserName']."</span>  ";
	}
	if($displayDate){
		$x.= "<span style=\"float:right;\"><b>".$Lang['Appraisal']['ARFormDate']."</b>: ".$aprInfo['SubDate']."</span>";
	}
	$x.= "</div><br/><br/>";
}
$x .= $genform->exeForm($connection,$a[0]["TemplateID"],$a[0]["UserID"],$a[0]["RecordID"],$a[0]["CycleID"],$a[0]["RlsNo"],$a[0]["BatchID"], $isPrint=true);
//$x .= "<div class=\"page-break\"></div>";
//$x .= "<p style=\"page-break-after:always;\"></p>";
array_push($formContent, $x);

if ($_SESSION['intranet_session_language'] == 'en') {
	$sql = "SELECT EnglishName FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID='".$a[0]["UserID"]."'";
	$userName=current($connection->returnVector($sql));
	$sql = "SELECT CONCAT(FrmTitleEng, '(".$userName.")') FROM INTRANET_PA_S_FRMTPL WHERE TemplateID='".$a[0]["TemplateID"]."'";
	$fileName=current($connection->returnVector($sql));
}else{
	$sql = "SELECT ChineseName FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID='".$a[0]["UserID"]."'";
	$userName=current($connection->returnVector($sql));
	$sql = "SELECT CONCAT(FrmTitleChi, '(".$userName.")') FROM INTRANET_PA_S_FRMTPL WHERE TemplateID='".$a[0]["TemplateID"]."'";
	$fileName=current($connection->returnVector($sql));
}

$allStyle.="<style>";
$allStyle.=".common_table_list{
	width: 100%;border: 1px solid #CCCCCC;margin:0 auto;border-collapse:separate;border-spacing: 0px;*border-collapse: expression('separate', cellSpacing = '0px');
}
.common_table_list tr th{
	margin:0px;padding:3px;padding-top:5px;padding-bottom:5px;background-color: #A6A6A6;font-weight: normal;color: #FFFFFF;text-align:left;border-bottom: 1px solid #CCCCCC;
	border-right: 1px solid #CCCCCC	;
}
.common_table_list tr th a{	color: #FFFFFF;}
.common_table_list tr th a.sort_asc {	font-weight: bold;display:block; float:left;
background-image:url(../../../images/2009a/icon_table_sort_tool.gif); background-repeat:no-repeat; background-position:0px 0px; padding-left:15px; }
.common_table_list tr th a.sort_asc:hover {	 background-position:0px -80px; }
.common_table_list tr th a.sort_dec {	font-weight: bold;display:block; float:left;
background-image:url(../../../images/2009a/icon_table_sort_tool.gif); background-repeat:no-repeat; background-position:0px -160px; padding-left:15px; }
.common_table_list tr th a.sort_dec:hover {	 background-position:0px -240px; }
.common_table_list  tr th a:hover{	color: #FF0000;}
.common_table_list tr td{
	margin:0px;padding:3px;background-color: #FFFFFF;border-bottom: 1px solid #CCCCCC;border-right: 1px solid #f1f1f1;vertical-align:top;padding-top:5px;padding-bottom:5px;
}
.common_table_list a {	color: #2286C5;}
.common_table_list a:hover {	color: #FF0000;}
.common_table_list  .num_check{	width:20px;}
.common_table_list  .num_class{	width:40px;}
.common_table_list  .record_code{	width:150px;}
.common_table_list  .record_code_long{	width:250px;}
.common_table_list  .record_tool{	width:100px;}
.common_table_list  .record_title{	width:200px;}
.common_table_list tr.sub_header th{	background-color:#B3B3B3}
.common_table_list tr.sub_row td{ background-color:#f4f4f4;  border-right: 1px solid #cfcfcf}
.common_table_list tr.sub_row td .form_table td { border-right: none}
.common_table_list tr.selected_row td{ background-color: #EFFDDB}
.common_table_list tr td.sub_function_row{ padding-left : 15px;}
.common_table_list tr td.rights_title_row{ background-color:#CCCCCC; width:120px; text-align:left}
.common_table_list tr td.rights_selected{ background-color: #EFFDDB}
.common_table_list tr td.rights_not_select_sub{ background-color: #eeeeee}
.common_table_list tr td.rights_not_select{ color:#999999}
.common_table_list tr th.dummy_row { background-color:#CCCCCC; width:1px; padding:0px;}
.common_table_list tr td.dummy_row { background-color:#A6A6A6; width:1px; padding:0px; border-bottom-color:#A6A6A6}
.common_table_list tr.move_selected td{ background-color:#fbf786; border-top: 2px dashed #d3981a; border-bottom: 2px dashed #d3981a}
.common_table_list tr.sub_sub_row td{ background-color:#eeeeee}
.common_table_list tr.seperate_row td{ background-color:#B3B3B3; text-align:left; color:#FFFFFF; font-style:italic}
.common_table_list tr.total_row td{ border-top:2px solid #666666}
.common_table_list tr td.total_col ,th.total_col { border-left:2px solid #999999}
.form_table_v30{ width:100%; margin:0 0;	border-collapse:separate;	border-spacing: 5px; 	*border-collapse: expression('separate', cellSpacing = '5px');}
.form_table_thickbox{ width:90%;}
.form_table_v30 tr td{	vertical-align:top;	text-align:left; line-height:18px; padding-top:5px; padding-bottom:5px;}
.form_table_v30 tr td.field_title{	border-bottom:1px solid #FFFFFF ; width:30%; background:#f3f3f3; padding-left:2px; padding-right:2px}
.form_table_v30 tr td.field_title_short{	border-bottom:1px solid #FFFFFF ; width:14%; background:#f3f3f3/*f4f4f4*/; padding-left:2px; padding-right:2px}
.form_table_v30 tr td{	border-bottom:1px solid #EFEFEF; padding-left:2px; padding-right:2px}";
$allStyle .= "</style>";



//echo $allStyle.$html;

$pdf = new mPDF('','A4',0,'',15, 15, 16, 16, 9, 9);
$pdf->backupSubsFont = array('mingliu');
$pdf->useSubstitutions = true;
$pdf->allow_charset_conversion=true;
$pdf->charset_in='UTF-8';
$pdf->WriteHTML($allStyle);
foreach($formContent as $html){
	$htmlContent = explode("<p style=\"page-break-after:always;\"></p>", $html);
	if(count($htmlContent)==1){
		$pdf->WriteHTML($html);
	}else{
		$htmlContent[0] = preg_replace('/(<br\/>)+$/', '', $htmlContent[0]);
		$pdf->WriteHTML($htmlContent[0]);
		for($idx=1; $idx < count($htmlContent); $idx++){
			$pdf->AddPage();
			if($htmlContent[$idx]!=""){
				if($idx < count($htmlContent)){
					$htmlContent[$idx] = preg_replace('/(<br\/>)+$/', '', $htmlContent[$idx]);
				}
				$pdf->WriteHTML($htmlContent[$idx]);
			}
		}
	}
}

$pdf->Output($fileName.'.pdf', 'I');


?>