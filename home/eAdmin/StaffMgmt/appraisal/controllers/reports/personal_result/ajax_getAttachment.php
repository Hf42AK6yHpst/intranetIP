<?php

// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================

$user=$_POST["user"];
$cycleID=$_POST["cycleID"];
$inclObs=$_POST["inclObs"];
//$file_parentPath = $PATH_WRT_ROOT.'../intranetdata/appraisal/';
$file_parentPath = $intranet_root.'/file/appraisal/';

$connection = new libgeneralsettings();
if($indexVar['libappraisal']->isEJ()){
    $sql="SELECT u8.UserID,u8.UserLogin FROM ".$appraisalConfig['INTRANET_USER']." u8
		INNER JOIN INTRANET_USER u ON u8.UserID=u.UserID
		WHERE u8.UserLogin='".$user."' AND u.RecordType=1 AND u.RecordStatus=1";
    $a=$connection->returnResultSet($sql);
}else{
    $sql="SELECT UserID,UserLogin FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserLogin='".$user."' AND RecordType=1 AND RecordStatus=1";
    $a=$connection->returnResultSet($sql);
}
if(empty($a)){
    if($indexVar['libappraisal']->isEJ()){
        $sql="SELECT UserID,UserLogin FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserLogin='".$user."' AND RecordType=1 AND RecordStatus=1";
        $a=$connection->returnResultSet($sql);
    }else{
        $sql="SELECT UserID,UserLogin FROM INTRANET_ARCHIVE_USER WHERE UserLogin='".$user."';";
        $a=$connection->returnResultSet($sql);
        if(!empty($a)){
            $archived = true;
            $userTable = $appraisalConfig['INTRANET_USER'];
            $appraisalConfig['INTRANET_USER'] = "INTRANET_ARCHIVE_USER";
        }
    }
}
$userID=$a[0]["UserID"];

//$sql="SELECT iptf.RecordID,iptfrmsub2.BatchID,iptf.CycleID,iptf.TemplateID,FrmTitle,QIDDescr,TxtAns
//	FROM(
//		SELECT RecordID,CycleID,TemplateID,UserID FROM INTRANET_PA_T_FRMSUM WHERE UserID=".IntegerSafe($userID)." AND CycleID=".$cycleID."
//	) as iptf
//	INNER JOIN(
//		SELECT MAX(RlsNo) as RlsNo,RecordID FROM INTRANET_PA_T_FRMSUB WHERE IsLastSub=1 GROUP BY RecordID
//	) as iptfrmsub ON iptf.RecordID=iptfrmsub.RecordID
//	INNER JOIN (
//		SELECT RlsNo as RlsNo2, RecordID as RecordID2, BatchID FROM INTRANET_PA_T_FRMSUB WHERE IsLastSub=1 GROUP BY RlsNo, RecordID
//	) iptfrmsub2 ON iptfrmsub.RecordID = iptfrmsub2.RecordID2 AND iptfrmsub.RlsNo = iptfrmsub2.RlsNo2
//	INNER JOIN(
//		SELECT TemplateID,".$indexVar['libappraisal']->getLangSQL("FrmTitleChi","FrmTitleEng")." as FrmTitle
//			FROM INTRANET_PA_S_FRMTPL
//	) as ipsfrmtpl ON iptf.TemplateID=ipsfrmtpl.TemplateID
//	INNER JOIN(
//		SELECT SecID,TemplateID FROM INTRANET_PA_S_FRMSEC WHERE ParentSecID IS NOT NULL
//	) as ipsfrmsec ON iptf.TemplateID=ipsfrmsec.TemplateID
//	INNER JOIN(
//		SELECT QcatID,SecID FROM INTRANET_PA_S_QCAT WHERE QuesDispMode=0
//	) as ipsqcat ON ipsfrmsec.SecID=ipsqcat.SecID
//	INNER JOIN(
//		SELECT QID,QCatID,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as QIDDescr,InputType
//			FROM INTRANET_PA_S_QUES WHERE InputType=7
//	) as ipsques ON ipsqcat.QCatID=ipsques.QCatID
//	LEFT JOIN(
//		SELECT RecordID,BatchID,QID,TxtAns FROM INTRANET_PA_T_SLF_DTL
//	) as iptsd ON ipsques.QID=iptsd.QID AND iptf.RecordID=iptsd.RecordID AND iptfrmsub2.BatchID=iptsd.BatchID;";
$sql = "SELECT frmdtl.RecordID,frmsub.BatchID,frmsum.CycleID,frmsum.TemplateID,frmques.QID
,".$indexVar['libappraisal']->getLangSQL("frmtpl.FrmTitleChi","frmtpl.FrmTitleEng")." as FrmTitle
,".$indexVar['libappraisal']->getLangSQL("frmques.DescrChi","frmques.DescrEng")." as QIDDescr
,frmdtl.TxtAns,frmsub.FillerID,frmsub.ObsID
FROM INTRANET_PA_T_FRMSUM frmsum
INNER JOIN INTRANET_PA_T_FRMSUB frmsub ON frmsum.RecordID = frmsub.RecordID
INNER JOIN INTRANET_PA_C_BATCH batch ON frmsub.BatchID = batch.BatchID
INNER JOIN INTRANET_PA_S_FRMTPL frmtpl ON frmsum.TemplateID = frmtpl.TemplateID
INNER JOIN INTRANET_PA_S_FRMSEC frmsec ON frmtpl.TemplateID = frmsec.TemplateID AND frmsec.ParentSecID IS NOT NULL
INNER JOIN INTRANET_PA_S_QCAT frmqcat ON frmsec.SecID = frmqcat.SecID AND frmqcat.QuesDispMode=0
INNER JOIN INTRANET_PA_S_QUES frmques ON frmqcat.QCatID=frmques.QCatID AND frmques.InputType=7
INNER JOIN INTRANET_PA_T_SLF_DTL frmdtl ON frmques.QID = frmdtl.QID AND frmdtl.RecordID= frmsum.RecordID AND frmdtl.RlsNo=frmsub.RlsNo AND frmdtl.BatchID = frmsub.BatchID
WHERE frmsum.UserID='".IntegerSafe($userID)."' AND frmsum.CycleID='".$cycleID."' AND frmsub.SubDate IS NOT NULL ";

if($inclObs=="1"){
    //$sql .= " AND (frmtpl.IsObs=0 OR frmtpl.IsObs=1)  ";
}elseif($inclObs=="0"){
    $sql .= " AND frmtpl.IsObs=0  ";
}
$sql .= " GROUP BY frmsub.BatchID, frmques.QID ";
$sql .= " ORDER BY frmtpl.TemplateID ASC,frmsum.RecordID ASC, batch.BatchOrder ASC ";
//echo $sql."<br/>";
$a=$connection->returnResultSet($sql);

$x .= "<table class=\"form_table_v30\">";
$x .= "<tr>";
$x .= "<th>".$Lang['Appraisal']['ARForm']."</th>";
$x .= "<th>".$Lang['Appraisal']['TemplateQues']['SDFDescr']."</th>";
$x .= "<th>".$Lang['Appraisal']['CycleTemplate']['DownloadFiles']."</th>";
$x .= "</tr>";

for($i=0;$i<sizeof($a);$i++){
    $fileNameCompose = explode("|",$a[$i]['TxtAns']);
    if(!empty($a[$i]['ObsID'])){
        $appraiserID = $a[$i]['FillerID'].",".$a[$i]['ObsID'];
        if(!in_array($fileNameCompose[1], explode(",",$appraiserID))){
            continue;
        }
    }else{
        $appraiserID = $a[$i]['FillerID'];
        if($fileNameCompose[1] != $appraiserID){
            continue;
        }
    }
    $tmpFilePath = $file_parentPath.'sdf_file/'.$a[$i]["BatchID"].'/'.$a[$i]["TxtAns"];
    if(!file_exists($tmpFilePath)){
        continue;
    }
    
    $x .= "<tr>";
    $x .= "<td>";
    $x .= $a[$i]["FrmTitle"];
    $x .= "</td>";
    $x .= "<td>";
    $x .= $a[$i]["QIDDescr"];
    $x .= "</td>";
    $x .= "<td>";
    //$x .= $a[$i]["TxtAns"];
    $tmpFilePathAbs = downloadAttachmentRelativePathToAbsolutePath($tmpFilePath);
    $fileDataArr = explode("|",$a[$i]["TxtAns"]);
    $x.="<a id='download' class=\"tablelink\" href=\"/home/download_attachment.php?target_e=".getEncryptedText($tmpFilePathAbs)."&filename_e=".getEncryptedText($fileDataArr[2])."\">".$fileDataArr[2]."</a>";
    
    $x .= "</td>";
    $x .= "</tr>";
    
    
}


$x .= "</table>";





echo $x;




?>