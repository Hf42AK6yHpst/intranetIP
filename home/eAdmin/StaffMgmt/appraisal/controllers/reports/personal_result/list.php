<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST, INTRANET_SLRS_LESSON_ARRANGEMENT
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");

$libacc = new libaccountmgmt();
$linterface = new interface_html();
$connection = new libgeneralsettings();

# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['ReportPersonalAppraisalResult']);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libappraisal_ui']->Include_Thickbox_JS_CSS();

// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
/*
$sql="SELECT CycleID,CONCAT(DescrChi,' (', YearNameB5,')') as DescrChi,CONCAT(DescrEng,' (', YearNameEN,')') as DescrEng
	FROM(
		SELECT CycleID,DescrChi,DescrEng,AcademicYearID
		FROM INTRANET_PA_C_CYCLE
	) as ipsf
	INNER JOIN(
		SELECT AcademicYearID,YearNameEN,YearNameB5 FROM ACADEMIC_YEAR
	) as ay ON ipsf.AcademicYearID=ay.AcademicYearID";
	$cycleList=$connection->returnResultSet($sql);
*/
$conds = "";
if(!in_array($_SESSION['UserID'], $indexVar['libappraisal']->getReportDisplayPersonList(false))){
    $conds = " WHERE ReportReleaseDate IS NOT NULL ";
}
$sql="SELECT CycleID, DescrChi, YearNameB5, DescrEng, YearNameEN
	FROM(
		SELECT CycleID,DescrChi,DescrEng,AcademicYearID
		FROM INTRANET_PA_C_CYCLE $conds
	) as ipsf
	INNER JOIN(
		SELECT AcademicYearID,YearNameEN,YearNameB5 FROM ACADEMIC_YEAR
	) as ay ON ipsf.AcademicYearID=ay.AcademicYearID";
$tmp_cycleList=$connection->returnResultSet($sql);
$cycleList = array();
if (count($tmp_cycleList) > 0) {
	foreach ($tmp_cycleList as $kk => $vv) {
		$cycleList[$kk] = array();
		$cycleList[$kk]["CycleID"] = $vv["CycleID"];
		if ($indexVar['libappraisal']->isEJ()) {
			$cycleList[$kk]["DescrChi"] = $vv["DescrChi"] . " (" . $indexVar['libappraisal']->displayChinese($vv["YearNameB5"]) . ")";
		} else {
			$cycleList[$kk]["DescrChi"] = $vv["DescrChi"] . " (" . $vv["YearNameB5"] . ")";
		}
		$cycleList[$kk]["DescrEng"] = $vv["DescrEng"] . " (" . $vv["YearNameEN"] . ")";
	}
}

// Get current special identity list
$specialIdentityList = $libacc->getSpecialIdentityMapping("",true);

$x ="<table class=\"form_table slrs_form\">"."\r\n";
$x .="<tr>"."\r\n";
$x .= "<td width=\"30%\">".$indexVar['libappraisal_ui']->RequiredSymbol().$Lang['Appraisal']['AppraisalPeriod']."</td><td>:</td>"."\r\n";
$x .= "<td width=\"70%\">".getSelectByAssoArray($indexVar['libappraisal']->cnvGeneralArrToSelect($cycleList, "CycleID","DescrChi","DescrEng"), $selectionTags='id="CycleID" name="CycleID" onChange="javascript:getStaffSelection()"', $SelectedType=$a[$i]["CycleID"], $all=0, $noFirst=0);
$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('cycleIDEmptyWarnDiv', $Lang['Appraisal']['Report']['MandatorySelection'], $Class='warnMsgDiv')."\r\n";
$x .= "</td>";
$x .= "</tr>"."\r\n";
$x .= "<tr>"."\r\n";
if (!$_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["isService"] && (!$sys_custom['eAppraisal']['TANotDisplayPeronalReport'] || in_array($_SESSION['UserID'], $indexVar['libappraisal']->getReportDisplayPersonList(false)))) {
	$x .= "<td>"."\r\n";
	$x .= $indexVar['libappraisal_ui']->RequiredSymbol()."<span>".$Lang['Appraisal']['ReportPersonResult']['SelectAppraisee']."</span><br/>";
	$x .= "</td>"."\r\n";
	$x .= "<td>:</td>";
	$x .= "<td>";
	$x .= "<span id=\"staff_selection\">";
//	$x .= $indexVar['libappraisal']->Get_Staff_Selection('LoginID','LoginID',$__ParIndividual=true,$__ParIsMultiple=false,$__ParSize=20,$__ParSelectedValue=$StaffID,$__ParOthers="",$__ParAllStaffOption=false,$__ShowOptGroupLabel=true,$LoginIDAsValue=true);
	$x .= "</span>";
//	$x .= "</td>";
//	$x .= "<td>";
//	$x .= $Lang['Appraisal']['SearchByLoginID']." "."<input type=\"text\" id=\"LoginID\" name=\"LoginID\" value=\"\">";
//	$x .= "</td>";
//	$x .= "<tr>";
//	$x .= "<td>";
//	$x .= $indexVar['libappraisal_ui']->RequiredSymbol().$Lang['Appraisal']['ReportPersonResult']['SelectedAppraisee']."<br/>";
//	$x .= "<td>:</td>";
//	$x .= "<td>";
//	$x .= "<span id=\"UserName\" name=\"UserName\"></span>";
//	$x .= "<input type=\"hidden\" name=\"UserID\" id=\"UserID\" value/>";
//	$x .= "</td>";
	$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('userNameEmptyWarnDiv', $Lang['Appraisal']['Report']['MandatorySelection'], $Class='warnMsgDiv')."\r\n";
	$x .= "</td>";
}else{
	$sql = "SELECT UserLogin FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID='".$_SESSION['UserID']."'";
	$LoginID = current($connection->returnVector($sql));
	$x .= "<td><input type=\"hidden\" id=\"LoginID\" name=\"LoginID\" value=\"".$LoginID."\"><input type=\"hidden\" name=\"UserID\" id=\"UserID\" value=\"".$_SESSION['UserID']."\"/></td>";	
}
$x .= "</tr>"."\r\n";
$x .= "<tr>"."\r\n";
$x .= "<td>"."\r\n";
$x .= $Lang['Appraisal']['ReportPersonResult']['DisplayMode'];
$x .= "</td>";
$x .= "<td>:</td>";
$x .= "<td>";
if($sys_custom['eAppraisal']['WusichongStyle']==true){
	if(in_array($_SESSION['UserID'],$specialIdentityList[1]) || !$_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["isService"]){
		$x .= $indexVar['libappraisal_ui']->Get_Radio_Button("displayMode_s", "displayMode", $value='s', $isChecked=0, $Class="", $Display=$Lang['Appraisal']['ReportPersonResult']['DisplayModeSeparate'], $Onclick="",$isDisabled=0,$isIndented=0,$specialLabel="");
	}
	$x .= $indexVar['libappraisal_ui']->Get_Radio_Button("displayMode_g", "displayMode", $value='g', $isChecked=1, $Class="", $Display=$Lang['Appraisal']['ReportPersonResult']['DisplayModeGrouping'], $Onclick="",$isDisabled=0,$isIndented=0,$specialLabel="");
}else{	
	$x .= $indexVar['libappraisal_ui']->Get_Radio_Button("displayMode_s", "displayMode", $value='s', $isChecked=1, $Class="", $Display=$Lang['Appraisal']['ReportPersonResult']['DisplayModeSeparate'], $Onclick="",$isDisabled=0,$isIndented=0,$specialLabel="");
	$x .= $indexVar['libappraisal_ui']->Get_Radio_Button("displayMode_g", "displayMode", $value='g', $isChecked=0, $Class="", $Display=$Lang['Appraisal']['ReportPersonResult']['DisplayModeGrouping'], $Onclick="",$isDisabled=0,$isIndented=0,$specialLabel="");
}
$x .= "</td>";
$x .= "</tr>"."\r\n";
$x .= "<tr>"."\r\n";
$x .= "<td>"."\r\n";
$x .= $Lang['Appraisal']['ReportPersonResult']['InclObs'];
$x .= "</td>";
$x .= "<td>:</td>";
$x .= "<td>";
$x .= $indexVar['libappraisal_ui']->Get_Checkbox("inclObs", "inclObs", $Value=1, $isChecked=0, $Class=$isRequiredField, $Display="", $Onclick="",$isDisabled=0);
$x .= "</td>";
$x .= "</tr>"."\r\n";

$x .= "<tr>"."\r\n";
$x .= "<td>"."\r\n";
$x .= $Lang['Appraisal']['ReportPersonResult']['DisplayName'];
$x .= "</td>";
$x .= "<td>:</td>";
$x .= "<td>";
$x .= $indexVar['libappraisal_ui']->Get_Checkbox("displayName", "displayName", $Value=1, $isChecked=0, $Class=$isRequiredField, $Display="", $Onclick="",$isDisabled=0);
$x .= "</td>";
$x .= "</tr>"."\r\n";

$x .= "<tr>"."\r\n";
$x .= "<td>"."\r\n";
$x .= $Lang['Appraisal']['ReportPersonResult']['DisplayDate'];
$x .= "</td>";
$x .= "<td>:</td>";
$x .= "<td>";
$x .= $indexVar['libappraisal_ui']->Get_Checkbox("displayDate", "displayDate", $Value=1, $isChecked=0, $Class=$isRequiredField, $Display="", $Onclick="",$isDisabled=0);
$x .= "</td>";
$x .= "</tr>"."\r\n";

$x .= "<tr>"."\r\n";
$x .= "<td>"."\r\n";
$x .= $Lang['Appraisal']['ReportPersonResult']['OutputDOC'];
$x .= "</td>";
$x .= "<td>:</td>";
$x .= "<td>";
$x .= $indexVar['libappraisal_ui']->Get_Checkbox("outputDOC", "outputDOC", $Value=1, $isChecked=0, $Class=$isRequiredField, $Display="", $Onclick="",$isDisabled=0);
$x .= "</td>";
$x .= "</tr>"."\r\n";

$x .= "<tr>"."\r\n";
$x .= "<td>"."\r\n";
$x .= $Lang['Appraisal']['ReportPersonResult']['DisplayWithCover'];
$x .= "</td>";
$x .= "<td>:</td>";
$x .= "<td>";
$x .= $indexVar['libappraisal_ui']->Get_Checkbox("displayCover", "displayCover", $Value=1, $isChecked=0, $Class=$isRequiredField, $Display="", $Onclick="",$isDisabled=0);
$x .= "</td>";
$x .= "</tr>"."\r\n";

$x .= "</table>";
		

$htmlAry['contentTbl'] = $x;
// ============================== Transactional data ==============================

// ============================== Define Button ==============================
$htmlAry['viewBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnRptGen'], "submit", "goRptGen()", 'genRptBtn', '', 1);
// ============================== Define Button ==============================








?>