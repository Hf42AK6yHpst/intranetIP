<?php
@ini_set("memory_limit","1024M");
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
//include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");

# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$connection = new libgeneralsettings();
$cycleID=$_GET["cycleID"];
$appRole=$_GET["appRole"];

$sysDate=date("Y-m-d");
$strSQL = "Select DISTINCT RlsNo From INTRANET_PA_T_FRMSUB WHERE IsActive=1 AND RlsNo <> '1'";
$result = $indexVar['libappraisal']->returnVector($strSQL);
if (count($result) > 0) {
	$RlsNo = "'".implode("','",$result)."'";
}
$strSQL = "SELECT ".$indexVar['libappraisal']->getLangSQL('NameChi','NameEng')." as RoleName FROM INTRANET_PA_S_APPROL WHERE AppRoleID='".$appRole."'";
$result = $indexVar['libappraisal']->returnVector($strSQL);
if (count($result) > 0) {
	$RoleName = current($result);
}
$filter_deleted = "";
if(!$sys_custom['eAppraisal']['showArchieveTeacher']){
	$filter_deleted = " AND auser.UserLogin IS NULL AND afuser.UserLogin IS NULL ";
}
$sql = "SELECT CycleID,RecordID,UserID,UserName,FillerID,FillerUserName,TemplateID,
		CASE WHEN FrmTitle IS NULL THEN '".$Lang['Appraisal']['ReportOutstanding']['NoFrmTitle']."' ELSE FrmTitle END as FrmTitle,
		SecID,FrmType,FrmTypeID,
		CASE WHEN SecTitle IS NULL THEN '".$Lang['Appraisal']['ReportOutstanding']['NoSecTitle']."' ELSE SecTitle END as SecTitle,BatchID,EditPrdFr,EditPrdTo,RmnDay
		FROM(
			SELECT ipcc.CycleID,iptf.RecordID,iptf.UserID,".$indexVar['libappraisal']->getLangSQL("iu.ChineseName","iu.EnglishName")." as UserName,
					iptfsub.FillerID,".$indexVar['libappraisal']->getLangSQL("iuFiller.ChineseName","iuFiller.EnglishName")." as FillerUserName,ipsf.TemplateID,"
					.$indexVar['libappraisal']->getLangSQL("CONCAT(FrmTitleChi,'-',FrmCodChi,'-',ObjChi)","CONCAT(FrmTitleEng,'-',FrmCodEng,'-',ObjEng)")." as FrmTitle,
					ipcb.SecID,"
						.$indexVar['libappraisal']->getLangSQL("CONCAT(SecCodChi,'-',SecTitleChi)","CONCAT(SecCodEng,'-',SecTitleEng)")." as SecTitle,iptfsub.BatchID,
					DATE_FORMAT(ipcb.EditPrdFr,'%Y-%m-%d') as EditPrdFr,DATE_FORMAT(ipcb.EditPrdTo,'%Y-%m-%d') as EditPrdTo,DATEDIFF(ipcb.EditPrdTo,'".$sysDate."') as RmnDay,
					IF(iptfsub.ObsID IS NULL, IF(iptfsub.GrpID IS NULL, 'IDV','GRP'), 'OBS') as FrmType, IFNULL(iptfsub.ObsID, IF(iptfsub.GrpID IS NULL, iptfsub.IdvID, ".$indexVar['libappraisal']->getLangSQL("ipcg.DescrChi","ipcg.DescrEng").")) as FrmTypeID
			FROM INTRANET_PA_C_CYCLE ipcc
			INNER JOIN INTRANET_PA_T_FRMSUM iptf ON ipcc.CycleID=iptf.CycleID AND iptf.IsComp=0
			INNER JOIN INTRANET_PA_T_FRMSUB iptfsub ON iptf.RecordID=iptfsub.RecordID AND iptfsub.SubDate IS NULL
			INNER JOIN INTRANET_PA_C_BATCH ipcb ON iptfsub.BatchID=ipcb.BatchID AND ipcc.CycleID=ipcb.CycleID
			INNER JOIN ".$appraisalConfig['INTRANET_USER']." iu ON iptf.UserID=iu.UserID
			INNER JOIN ".$appraisalConfig['INTRANET_USER']." iuFiller ON iptfsub.FillerID=iuFiller.UserID
			INNER JOIN INTRANET_PA_S_FRMTPL ipsf ON iptf.TemplateID=ipsf.TemplateID
			INNER JOIN INTRANET_PA_S_FRMSEC ipsfrmsec ON ipcb.SecID=ipsfrmsec.SecID
			LEFT JOIN INTRANET_PA_C_GRPSUM ipcg ON iptfsub.GrpID = ipcg.GrpID 
			LEFT JOIN INTRANET_ARCHIVE_USER afuser ON iuFiller.UserID = afuser.UserID
			LEFT JOIN INTRANET_ARCHIVE_USER auser ON iu.UserID = auser.UserID
			WHERE ipcc.CycleID=".$cycleID." AND iptfsub.AppRoleID='".$appRole."' AND iptfsub.RlsNo IN('1',".$RlsNo.") $filter_deleted
		) a";		
//echo $sql."<br/><br/>";die();
$a=$connection->returnResultSet($sql);
/*$x .= "<div align=\"right\">"."\r\n";
$x .= "<input type=\"button\" class=\"formsubbutton\" onclick=\"javascript:window.print();\" name=\"PrintBtn\" id=\"PrintBtn\" value=".$Lang['Appraisal']['ReportPrint']." onmouseover=\"this.className='formsubbuttonon'\" onmouseout=\"this.className='formsubbutton'\">";
$x .= "<br/><div id=\"resizeFontLoadingDiv\" style=\"display:none; float:right;\">
	<img src=\"/images/2009a/indicator.gif\"><span class=\"tabletextremark\"><span style=\"color:red;\">".$Lang['Appraisal']['ReportProcessing'] ."</span></span></div>";
$x .= "</div>"."\r\n";
*/
$unsubmittedList = array();
for($i=0;$i<sizeof($a);$i++){
	if(!isset($unsubmittedList[$a[$i]['FillerID']])){
		$unsubmittedList[$a[$i]['FillerID']] = array(
			"Filler"=>$a[$i]['FillerUserName'],
			"Data"=>array()
		);	
	}
	$indicator = $Lang['Appraisal']['ReportOutstanding'][$a[$i]['FrmType']];
	if($a[$i]['FrmType']=='GRP'){
		$indicator .= " ".$a[$i]['FrmTypeID'];
	}
	$unsubmittedList[$a[$i]['FillerID']]["Data"][] = array(
		"Title"=>$a[$i]['FrmTitle']." - ".$a[$i]['SecTitle'],
		"Period"=>$a[$i]['EditPrdFr']." -<br/>".$a[$i]['EditPrdTo'],
		"RemainDay"=>$a[$i]['RmnDay'],
		"Appraisee"=>$a[$i]['UserName']."<br/>(".$indicator.")"
	);	
}

$html = "";
$html.= $x;

$allStyle = "<style>";
$allStyle .= ".common_table_list_v30{	width: 100%;	border: 1px solid #CCCCCC;		margin:0 auto;	border-collapse:separate;	border-spacing: 0px; 	*border-collapse: expression('separate', cellSpacing = '0px');	clear:both;}
.common_table_list_v30 tr th{	margin:0px;	padding:3px;	padding-top:5px;	padding-bottom:5px;	background-color: #A6A6A6;	font-weight: normal;	color: #FFFFFF;	text-align:left;	border-bottom: 1px solid #CCCCCC;		border-right: 1px solid #CCCCCC	;}
.edit_table_list_v30 tr th{	background-color: #669746/* #71AA2F*/;}
.common_table_list_v30 tr th a{	color: #FFFFFF;}
.common_table_list_v30 tr th a.sort_asc {font-weight: bold;display:block; float:left;background:url(../../../images/2009a/icon_table_sort_tool.gif) no-repeat 0px 0px; padding-left:15px; }
.common_table_list_v30 tr th a.sort_asc:hover {	 background-position:0px -80px; }
.common_table_list_v30 tr th a.sort_dec {font-weight: bold;display:block;float:left;background:url(../../../images/2009a/icon_table_sort_tool.gif) no-repeat 0px -160px;padding-left:15px; }
.common_table_list_v30 tr th a.sort_dec:hover {	 background-position:0px -240px; }
.common_table_list_v30  tr th a:hover{	color: #FF0000;}
.edit_table_list_v30 tr.edit_table_head_bulk th {background-color: #97bd7e}  
.common_table_list_v30 tr td{margin:0px;padding:5px 3px 5px 3px;background-color: #FFFFFF;	border-bottom: 1px solid #CCCCCC;	border-right: 1px solid #f1f1f1	;vertical-align:top;}
.common_table_list_v30 a {	color: #2286C5;}
.common_table_list_v30 a:hover {	color: #FF0000;}
.common_table_list_v30  .num_check{	width:20px;}
.common_table_list_v30  .num_class{	width:40px;}
.common_table_list_v30  .record_code{	width:150px;}
.common_table_list_v30  .record_code_long{	width:250px;}
.common_table_list_v30  .record_tool{	width:100px;}
.common_table_list_v30  .record_title{	width:200px;}
.common_table_list_v30 tr.sub_header th{	background-color:#B3B3B3}
.common_table_list_v30 tr.sub_row td{ background-color:#f4f4f4;  border-right: 1px solid #cfcfcf}
.common_table_list_v30 tr.sub_row td .form_table tr td { border-right: none;}
.common_table_list_v30 tr.sub_row td .form_table tr th { border-right: none; background:#FFFFFF;color:#000000}

.common_table_list_v30 tr.selected_row td{ background-color: #EFFDDB}
.common_table_list_v30 tr td.sub_function_row{ padding-left : 15px;}
.common_table_list_v30 tr td.rights_title_row{ background-color:#CCCCCC; width:120px; text-align:left}
.common_table_list_v30 tr td.rights_selected{ background-color: #EFFDDB}
.common_table_list_v30 tr td.rights_not_select_sub{ background-color: #eeeeee}
.common_table_list_v30 tr td.rights_not_select{ color:#999999}
.common_table_list_v30 tr th.dummy_row { background-color:#CCCCCC; width:1px; padding:0px;}
.common_table_list_v30 tr td.dummy_row { background-color:#A6A6A6; width:1px; padding:0px; border-bottom-color:#A6A6A6}

.common_table_list_v30 tr.move_selected td{ background-color:#fbf786; border-top: 2px dashed #d3981a; border-bottom: 2px dashed #d3981a}
.common_table_list_v30 tr.sub_sub_row td{ background-color:#eeeeee}
.common_table_list_v30 tr.seperate_row td{ background-color:#B3B3B3; text-align:left; color:#FFFFFF; font-style:italic}
.common_table_list_v30 tr.total_row td{ border-top:2px solid #999999}
.common_table_list_v30 tr td.total_col ,th.total_col { border-left:2px solid #999999}

.common_table_list_v30 tr th.sub_row_top { background-color:#7E7E7E}

.common_table_list_v30 tr.row_avaliable td{color:#000;background-color:#ECFEDA;border-bottom:1px solid #B8B8B8;padding:3px}
.common_table_list_v30 tr.row_avaliable a{color:#06C;text-decoration:none}
.common_table_list_v30 tr.row_suspend td{color:#000;background-color:#FFE0E1;border-bottom:1px solid #B8B8B8;padding:3px}
.common_table_list_v30 tr.row_waiting td{color:#000;background-color:#EBF6FA;border-bottom:1px solid #B8B8B8;padding:3px}
.common_table_list_v30 tr.record_Waived td{color:#000;background-color:#f6f6f6;border-bottom:1px solid #B8B8B8;padding:3px}
.common_table_list_v30 tr.high_score td{color:#000;background-color:#89a1f3 !important; border-bottom:1px solid #B8B8B8;padding:3px}
.common_table_list_v30 tr.low_score td{color:#000;background-color:#dc9898 !important;border-bottom:1px solid #B8B8B8;padding:3px}



.common_table_list_v30 tr.row_on td {background-color: #FFFFFF;}
.common_table_list_v30 tr.row_off td {background-color: #f0f0f0;}
.common_table_list_v30 table.table_plain tr td { border:none}

.common_table_list_v30 tr.row_drafted td{color:#000000;background-color:#f1f1f1;border-bottom:1px solid #B8B8B8;padding:3px}
.common_table_list_v30 tr.row_drafted a{color:#06C;text-decoration:none}

.view_table_bottom_v30 { 	background-color: #BEC7DC}
.view_table_list_v30 tr th{	background-color: #487db4/* #4A7ECC*/;}
.view_table_list_v30 tr th.sub_row_top { background-color:#32608f}
.view_table_list_v30 tr td.sub_row_group { background-color:#FFFFFF; border-right-color:#f1f1f1}";
$allStyle .= "</style>";

//echo $allStyle.$html;

$pdf = new mPDF('','A4',0,'',15, 15, 16, 16, 9, 9);
$pdf->backupSubsFont = array('mingliu');
$pdf->useSubstitutions = true;

$pdf->WriteHTML($allStyle);

$x .= "<table>"."\r\n";
$x .= "<tr><td colspan=3><b>".$RoleName." ".sprintf($Lang['Appraisal']['ReportOutstanding']['Title'],$sysDate)."</b></td></tr>"."\r\n";
$x .= "</table><br/>"."\r\n";

$x .= "<table class=\"common_table_list_v30\">"."\r\n";
//$x .= "<tr><th width=\"10%\">".$Lang['Appraisal']['ReportOutstanding']['User']."</th><th width=\"20%\">".$Lang['Appraisal']['ReportOutstanding']['Template']."</th><th width=\"10%\">".$Lang['Appraisal']['ReportOutstanding']['Filler']."</th><th width=\"20%\">".$Lang['Appraisal']['ReportOutstanding']['CurSec']."</th><th width=\"15%\">".$Lang['Appraisal']['ReportOutstanding']['SubDatFr']."</th>"."\r\n";;
//$x .= "<th width=\"15%\">".$Lang['Appraisal']['ReportOutstanding']['SubDatTo']."</th><th width=\"10%\">".$Lang['Appraisal']['ReportOutstanding']['RmnSubDat']."</th></tr>"."\r\n";
$x .= "<tr>";
$x .= "<th>".$Lang['Appraisal']['ReportOutstanding']['Filler']."</th>";
$x .= "<th>".$Lang['Appraisal']['ReportOutstanding']['CurSec']."</th>";
$x .= "<th style='width:20%'>".$Lang['Appraisal']['CycleTemplate']['Deadline']."</th>";
$x .= "<th>".$Lang['Appraisal']['ReportOutstanding']['RmnDat']."</th>";
$x .= "<th style='width:20%'>".$Lang['Appraisal']['ReportOutstanding']['User']."</th>";
$x .= "</tr>";
$pdf->WriteHTML($x);
//for($i=0;$i<sizeof($unsubmittedList);$i++){
//	$x = "<tr>";
//	$x .= "<td>".$unsubmittedList[$i]["UserName"]."</td>";
//	$x .= "<td>".$unsubmittedList[$i]["FrmTitle"]."</td>";
//	$x .= "<td>".$unsubmittedList[$i]["FillerUserName"]."</td>";
//	$x .= "<td>".$unsubmittedList[$i]["SecTitle"]."</td>";
//	$x .= "<td>".$unsubmittedList[$i]["EditPrdFr"]."</td>";
//	$x .= "<td>".$unsubmittedList[$i]["EditPrdTo"]."</td>";
//	$x .= "<td>".$unsubmittedList[$i]["RmnDay"]."</td>";
//	$x .= "</tr>";
//	$pdf->WriteHTML($x);
//}

foreach($unsubmittedList as $usdata){
	$x = "<tr>";
	$x .= "<td rowspan='".sizeof($usdata['Data'])."'>".$usdata["Filler"]."</td>";
	$x .= "<td>".$usdata['Data'][0]["Title"]."</td>";
	$x .= "<td>".$usdata['Data'][0]["Period"]."</td>";
	$x .= "<td>".$usdata['Data'][0]["RemainDay"]."</td>";
	$x .= "<td>".$usdata['Data'][0]["Appraisee"]."</td>";
	$x .= "</tr>";
	$pdf->WriteHTML($x);
	for($i=1;$i<sizeof($usdata['Data']);$i++){
		$x = "<tr>";
		$x .= "<td>".$usdata['Data'][$i]["Title"]."</td>";
		$x .= "<td>".$usdata['Data'][$i]["Period"]."</td>";
		$x .= "<td>".$usdata['Data'][$i]["RemainDay"]."</td>";
		$x .= "<td>".$usdata['Data'][$i]["Appraisee"]."</td>";
		$x .= "</tr>";
		$pdf->WriteHTML($x);
	}	
}

$x = "</table>";
$pdf->WriteHTML($x);
//echo $x;

//$htmlAry['contentTbl'] = $x;



$pdf->Output('outstanding_report.pdf', 'I');

?>