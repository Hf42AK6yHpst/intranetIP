<?php
@ini_set("memory_limit","1024M");
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
//include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");

# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$connection = new libgeneralsettings();
$cycleID=$_GET["cycleID"];
$appRole=$_GET["appRole"];

$sysDate=date("Y-m-d");

$currCycleIDSQL = "SELECT CycleID FROM INTRANET_PA_T_FRMSUM  WHERE IsActive=1 GROUP BY CycleID ORDER BY CycleID DESC";
$currCycleID = current($connection->returnVector($currCycleIDSQL));

//$sql="SELECT CycleID,RecordID,UserID,UserName,FillerID,FillerUserName,TemplateID,
//		CASE WHEN FrmTitle IS NULL THEN '".$Lang['Appraisal']['ReportOutstanding']['NoFrmTitle']."' ELSE FrmTitle END as FrmTitle,
//		SecID,
//		CASE WHEN SecTitle IS NULL THEN '".$Lang['Appraisal']['ReportOutstanding']['NoSecTitle']."' ELSE SecTitle END as SecTitle,BatchID,EditPrdFr,EditPrdTo,RmnDay
//		FROM(
//			SELECT ipcc.CycleID,iptf.RecordID,iptf.TemplateID,iptf.UserID,iu.UserName,iptfsub.FillerID,iuFiller.FillerUserName,"
//				.$indexVar['libappraisal']->getLangSQL("FrmTitleChi","FrmTitleEng")." as FrmTitle,
//			ipcb.SecID,"
//						.$indexVar['libappraisal']->getLangSQL("SecTitleChi","SecTitleEng")." as SecTitle,iptfsub.BatchID,
//		EditPrdFr,EditPrdTo,DATEDIFF(EditPrdTo,'".$sysDate."') as RmnDay
//		FROM(
//			SELECT CycleID FROM INTRANET_PA_C_CYCLE WHERE CycleID=".$cycleID."
//		) as ipcc
//		INNER JOIN(
//			SELECT RecordID,CycleID,TemplateID,IsComp,UserID FROM INTRANET_PA_T_FRMSUM WHERE IsComp=0
//		) as iptf ON ipcc.CycleID=iptf.CycleID
//		INNER JOIN(
//			SELECT RecordID,BatchID,AppRoleID,SubDate,FillerID FROM INTRANET_PA_T_FRMSUB WHERE SubDate IS NULL
//		) as iptfsub ON iptf.RecordID=iptfsub.RecordID
//		INNER JOIN(
//			SELECT BatchID,CycleID,TemplateID,DATE_FORMAT(EditPrdFr,'%Y-%m-%d') as EditPrdFr,DATE_FORMAT(EditPrdTo,'%Y-%m-%d') as EditPrdTo,SecID
//			FROM INTRANET_PA_C_BATCH
//			WHERE '".$sysDate."' >=EditPrdFr AND '".$sysDate."' <=EditPrdTo
//		) as ipcb ON iptfsub.BatchID=ipcb.BatchID AND ipcc.CycleID=ipcb.CycleID
//		INNER JOIN(
//			SELECT UserID,".$indexVar['libappraisal']->getLangSQL("ChineseName","EnglishName")." as UserName,UserLogin FROM ".$appraisalConfig['INTRANET_USER']."
//		) as iu ON iptf.UserID=iu.UserID
//		INNER JOIN(
//			SELECT UserID,".$indexVar['libappraisal']->getLangSQL("ChineseName","EnglishName")." as FillerUserName,UserLogin FROM ".$appraisalConfig['INTRANET_USER']."
//		) as iuFiller ON iptfsub.FillerID=iuFiller.UserID
//		INNER JOIN(
//			SELECT TemplateID,CONCAT(FrmTitleChi,'-',FrmCodChi,'-',ObjChi) as FrmTitleChi,CONCAT(FrmTitleEng,'-',FrmCodEng,'-',ObjEng) as FrmTitleEng FROM INTRANET_PA_S_FRMTPL
//		) as ipsf ON iptf.TemplateID=ipsf.TemplateID
//		INNER JOIN(
//			SELECT SecID,CONCAT(SecTitleChi,'-',SecCodChi) as SecTitleChi,CONCAT(SecTitleEng,'-',SecCodEng) as SecTitleEng FROM INTRANET_PA_S_FRMSEC
//		) as ipsfrmsec ON ipcb.SecID=ipsfrmsec.SecID
//	) as a
//		";
$filter_deleted = "";
if(!$sys_custom['eAppraisal']['showArchieveTeacher']){
	$filter_deleted = " AND auser.UserLogin IS NULL AND afuser.UserLogin IS NULL ";
}
$filter_curr_cycle_active = "";
if($currCycleID == $cycleID){
	$filter_curr_cycle_active = " AND iptfsub.IsActive = '1' ";
}
$sql = "SELECT CycleID,RecordID,UserID,UserName,FillerID,FillerUserName,TemplateID,
		CASE WHEN FrmTitle IS NULL THEN '".$Lang['Appraisal']['ReportOutstanding']['NoFrmTitle']."' ELSE FrmTitle END as FrmTitle,
		SecID,
		CASE WHEN SecTitle IS NULL THEN '".$Lang['Appraisal']['ReportOutstanding']['NoSecTitle']."' ELSE SecTitle END as SecTitle,BatchID,EditPrdFr,EditPrdTo,RmnDay,TemplateOrder
		FROM(
			SELECT ipcc.CycleID,iptf.RecordID,iptf.UserID,".$indexVar['libappraisal']->getLangSQL("iu.ChineseName","iu.EnglishName")." as UserName, iu.EnglishName as EngName,
					iptfsub.FillerID,".$indexVar['libappraisal']->getLangSQL("iuFiller.ChineseName","iuFiller.EnglishName")." as FillerUserName,ipsf.TemplateID,"
					.$indexVar['libappraisal']->getLangSQL("CONCAT(FrmTitleChi,'-',FrmCodChi,'-',ObjChi)","CONCAT(FrmTitleEng,'-',FrmCodEng,'-',ObjEng)")." as FrmTitle,
					ipcb.SecID,"
						.$indexVar['libappraisal']->getLangSQL("CONCAT(SecTitleChi,'-',SecCodChi)","CONCAT(SecTitleEng,'-',SecCodEng)")." as SecTitle,iptfsub.BatchID,
					DATE_FORMAT(ipcb.EditPrdFr,'%Y-%m-%d') as EditPrdFr,DATE_FORMAT(ipcb.EditPrdTo,'%Y-%m-%d') as EditPrdTo,DATEDIFF(ipcb.EditPrdTo,'".$sysDate."') as RmnDay, ipsf.DisplayOrder as TemplateOrder
			FROM INTRANET_PA_C_CYCLE ipcc
			INNER JOIN INTRANET_PA_T_FRMSUM iptf ON ipcc.CycleID=iptf.CycleID AND iptf.IsComp=0
			INNER JOIN INTRANET_PA_T_FRMSUB iptfsub ON iptf.RecordID=iptfsub.RecordID AND iptfsub.SubDate IS NULL
			INNER JOIN INTRANET_PA_C_BATCH ipcb ON iptfsub.BatchID=ipcb.BatchID AND ipcc.CycleID=ipcb.CycleID
			INNER JOIN ".$appraisalConfig['INTRANET_USER']." iu ON iptf.UserID=iu.UserID
			INNER JOIN ".$appraisalConfig['INTRANET_USER']." iuFiller ON iptfsub.FillerID=iuFiller.UserID
			INNER JOIN INTRANET_PA_S_FRMTPL ipsf ON iptf.TemplateID=ipsf.TemplateID
			INNER JOIN INTRANET_PA_S_FRMSEC ipsfrmsec ON ipcb.SecID=ipsfrmsec.SecID
			LEFT JOIN INTRANET_ARCHIVE_USER afuser ON iuFiller.UserID = afuser.UserID
			LEFT JOIN INTRANET_ARCHIVE_USER auser ON iu.UserID = auser.UserID
			WHERE ipcc.CycleID=".$cycleID." $filter_deleted $filter_curr_cycle_active
		) a ORDER BY RmnDay ASC, EngName ASC, TemplateOrder ASC ";		
//echo $sql."<br/><br/>";
$a=$connection->returnResultSet($sql);
/*$x .= "<div align=\"right\">"."\r\n";
$x .= "<input type=\"button\" class=\"formsubbutton\" onclick=\"javascript:window.print();\" name=\"PrintBtn\" id=\"PrintBtn\" value=".$Lang['Appraisal']['ReportPrint']." onmouseover=\"this.className='formsubbuttonon'\" onmouseout=\"this.className='formsubbutton'\">";
$x .= "<br/><div id=\"resizeFontLoadingDiv\" style=\"display:none; float:right;\">
	<img src=\"/images/2009a/indicator.gif\"><span class=\"tabletextremark\"><span style=\"color:red;\">".$Lang['Appraisal']['ReportProcessing'] ."</span></span></div>";
$x .= "</div>"."\r\n";
*/
$html = "";
$html.= $x;

$allStyle = "<style></style>";

//echo $allStyle.$html;

$pdf = new mPDF('','A4',22,'',5, 5, 5, 5, 9, 9);
$pdf->backupSubsFont = array('mingliu');
$pdf->useSubstitutions = true;

$pdf->WriteHTML($allStyle);

$x .= "<table>"."\r\n";
$x .= "<tr><td colspan=3><b>".sprintf($Lang['Appraisal']['ReportOutstanding']['Title'],$sysDate)."</b></td></tr>"."\r\n";
$x .= "</table><br/>"."\r\n";

$x .= "<table style=\"max-width:760px;\" border=\"1\" class=\"common_table_list\">"."\r\n";
$x .= "<tr>";
$x .= "<th style=\"width:2%\">#</th>";
$x .= "<th style=\"width:8%\">".$Lang['Appraisal']['ReportOutstanding']['User']."</th>";
$x .= "<th style=\"width:25%\">".$Lang['Appraisal']['ReportOutstanding']['Template']."</th>";
$x .= "<th style=\"width:10%\">".$Lang['Appraisal']['ReportOutstanding']['Filler']."</th>";
$x .= "<th style=\"width:25%\">".$Lang['Appraisal']['ReportOutstanding']['CurSec']."</th>";
$x .= "<th style=\"width:10%\">".$Lang['Appraisal']['ReportOutstanding']['SubDatFr']."</th>";
$x .= "<th style=\"width:10%\">".$Lang['Appraisal']['ReportOutstanding']['SubDatTo']."</th>";
$x .= "<th style=\"width:10%\">".$Lang['Appraisal']['ReportOutstanding']['RmnSubDat']."</th>";
$x .= "</tr>"."\r\n";
$pdf->WriteHTML($x);
for($i=0;$i<sizeof($a);$i++){
	$x = "<tr>";
	$x .= "<td>".($i+1)."</td>";
	$x .= "<td>".$a[$i]["UserName"]."</td>";
	$x .= "<td>".$a[$i]["FrmTitle"]."</td>";
	$x .= "<td>".$a[$i]["FillerUserName"]."</td>";
	$x .= "<td>".$a[$i]["SecTitle"]."</td>";
	$x .= "<td>".$a[$i]["EditPrdFr"]."</td>";
	$x .= "<td>".$a[$i]["EditPrdTo"]."</td>";
	$x .= "<td>".$a[$i]["RmnDay"]."</td>";
	$x .= "</tr>";
	$pdf->WriteHTML($x);
}

$x = "</table>";
$pdf->WriteHTML($x);
//echo $x;

//$htmlAry['contentTbl'] = $x;


$pdf->Output('outstanding_report.pdf', 'I');

?>