<?php

// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================


$lexport = new libexporttext();

// build header array
$headerAry = array();

$dataAry = array();
$connection = new libgeneralsettings();
$cycleID=$_GET["cycleID"];
$sysDate=date("Y-m-d");

$sql="SELECT CycleID,RecordID,UserID,UserName,FillerID,FillerUserName,TemplateID,
	CASE WHEN FrmTitle IS NULL THEN '".$Lang['Appraisal']['ReportOutstanding']['NoFrmTitle']."' ELSE FrmTitle END as FrmTitle,
	SecID,
	CASE WHEN SecTitle IS NULL THEN '".$Lang['Appraisal']['ReportOutstanding']['NoSecTitle']."' ELSE SecTitle END as SecTitle,BatchID,EditPrdFr,EditPrdTo,RmnDay
	FROM(
		SELECT ipcc.CycleID,iptf.RecordID,iptf.TemplateID,iptf.UserID,iu.UserName,iptfsub.FillerID,iuFiller.FillerUserName,"
			.$indexVar['libappraisal']->getLangSQL("FrmTitleChi","FrmTitleEng")." as FrmTitle,
		ipcb.SecID,"
					.$indexVar['libappraisal']->getLangSQL("SecTitleChi","SecTitleEng")." as SecTitle,iptfsub.BatchID,
	EditPrdFr,EditPrdTo,DATEDIFF(EditPrdTo,'".$sysDate."') as RmnDay
	FROM(
		SELECT CycleID FROM INTRANET_PA_C_CYCLE WHERE CycleID=".$cycleID."
	) as ipcc
	INNER JOIN(
		SELECT RecordID,CycleID,TemplateID,IsComp,UserID FROM INTRANET_PA_T_FRMSUM WHERE IsComp=0
	) as iptf ON ipcc.CycleID=iptf.CycleID
	INNER JOIN(
		SELECT RecordID,BatchID,AppRoleID,SubDate,FillerID FROM INTRANET_PA_T_FRMSUB WHERE SubDate IS NULL
	) as iptfsub ON iptf.RecordID=iptfsub.RecordID
	INNER JOIN(
		SELECT BatchID,CycleID,TemplateID,DATE_FORMAT(EditPrdFr,'%Y-%m-%d') as EditPrdFr,DATE_FORMAT(EditPrdTo,'%Y-%m-%d') as EditPrdTo,SecID
		FROM INTRANET_PA_C_BATCH
		WHERE '".$sysDate."' >=EditPrdFr AND '".$sysDate."' <=EditPrdTo
	) as ipcb ON iptfsub.BatchID=ipcb.BatchID AND ipcc.CycleID=ipcb.CycleID
	INNER JOIN(
		SELECT UserID,".$indexVar['libappraisal']->getLangSQL("ChineseName","EnglishName")." as UserName,UserLogin FROM ".$appraisalConfig['INTRANET_USER']."
	) as iu ON iptf.UserID=iu.UserID
	INNER JOIN(
		SELECT UserID,".$indexVar['libappraisal']->getLangSQL("ChineseName","EnglishName")." as FillerUserName,UserLogin FROM ".$appraisalConfig['INTRANET_USER']."
	) as iuFiller ON iptfsub.FillerID=iuFiller.UserID
	INNER JOIN(
		SELECT TemplateID,CONCAT(FrmTitleChi,'-',FrmCodChi,'-',ObjChi) as FrmTitleChi,CONCAT(FrmTitleEng,'-',FrmCodEng,'-',ObjEng) as FrmTitleEng FROM INTRANET_PA_S_FRMTPL
	) as ipsf ON iptf.TemplateID=ipsf.TemplateID
	INNER JOIN(
		SELECT SecID,CONCAT(SecTitleChi,'-',SecCodChi) as SecTitleChi,CONCAT(SecTitleEng,'-',SecCodEng) as SecTitleEng FROM INTRANET_PA_S_FRMSEC
	) as ipsfrmsec ON ipcb.SecID=ipsfrmsec.SecID
) as a
	";
//echo $sql."<br/><br/>";
$a=$connection->returnResultSet($sql);
$dataAry[0][0]=sprintf($Lang['Appraisal']['ReportOutstanding']['Title'],$sysDate);
$dataAry[1][0]="";

$index=2;

$dataAry[($index)][0]=$Lang['Appraisal']['ReportOutstanding']['User'];
$dataAry[($index)][1]=$Lang['Appraisal']['ReportOutstanding']['Template'];
$dataAry[($index)][2]=$Lang['Appraisal']['ReportOutstanding']['Filler'];
$dataAry[($index)][3]=$Lang['Appraisal']['ReportOutstanding']['CurSec'];
$dataAry[($index)][4]=$Lang['Appraisal']['ReportOutstanding']['SubDatFr'];
$dataAry[($index)][5]=$Lang['Appraisal']['ReportOutstanding']['SubDatTo'];
$dataAry[($index)][6]=$Lang['Appraisal']['ReportOutstanding']['RmnSubDat'];

for($i=0;$i<sizeof($a);$i++){
	$dataAry[($index)+1][0]=$a[$i]["UserName"];
	$dataAry[($index)+1][1]=$a[$i]["FrmTitle"];
	$dataAry[($index)+1][2]=$a[$i]["FillerUserName"];
	$dataAry[($index)+1][3]=$a[$i]["SecTitle"];
	$dataAry[($index)+1][4]=$a[$i]["EditPrdFr"];
	$dataAry[($index)+1][5]=$a[$i]["EditPrdTo"];
	$dataAry[($index)+1][6]=$a[$i]["RmnDay"];
	$index=$index+1;
}

$exportContent = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=1);

$fileName = 'outstanding_report.csv';
$lexport->EXPORT_FILE($fileName, $exportContent);







?>