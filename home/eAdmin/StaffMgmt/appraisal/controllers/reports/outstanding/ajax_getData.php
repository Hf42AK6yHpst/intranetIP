<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');

# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$connection = new libgeneralsettings();
$cycleID=$_POST["CycleID"];

$sysDate=date("Y-m-d");

$sql="SELECT CycleID,RecordID,UserID,UserName,FillerID,FillerUserName,TemplateID,
		CASE WHEN FrmTitle IS NULL THEN '".$Lang['Appraisal']['ReportOutstanding']['NoFrmTitle']."' ELSE FrmTitle END as FrmTitle,
		SecID,
		CASE WHEN SecTitle IS NULL THEN '".$Lang['Appraisal']['ReportOutstanding']['NoSecTitle']."' ELSE SecTitle END as SecTitle,BatchID,EditPrdFr,EditPrdTo,RmnDay
		FROM(			
			SELECT ipcc.CycleID,iptf.RecordID,iptf.TemplateID,iptf.UserID,iu.UserName,iptfsub.FillerID,iuFiller.FillerUserName,"
			.$indexVar['libappraisal']->getLangSQL("FrmTitleChi","FrmTitleEng")." as FrmTitle,
			ipcb.SecID,"
			.$indexVar['libappraisal']->getLangSQL("SecTitleChi","SecTitleEng")." as SecTitle,iptfsub.BatchID,
		EditPrdFr,EditPrdTo,DATEDIFF(EditPrdTo,'".$sysDate."') as RmnDay
		FROM(
			SELECT CycleID FROM INTRANET_PA_C_CYCLE WHERE CycleID=".$cycleID."		
		) as ipcc
		INNER JOIN(
			SELECT RecordID,CycleID,TemplateID,IsComp,UserID FROM INTRANET_PA_T_FRMSUM WHERE IsComp=0
		) as iptf ON ipcc.CycleID=iptf.CycleID
		INNER JOIN(
			SELECT RecordID,BatchID,AppRoleID,SubDate,FillerID FROM INTRANET_PA_T_FRMSUB WHERE SubDate IS NULL
		) as iptfsub ON iptf.RecordID=iptfsub.RecordID
		INNER JOIN(
			SELECT BatchID,CycleID,TemplateID,DATE_FORMAT(EditPrdFr,'%Y-%m-%d') as EditPrdFr,DATE_FORMAT(EditPrdTo,'%Y-%m-%d') as EditPrdTo,SecID
			FROM INTRANET_PA_C_BATCH
			WHERE '".$sysDate."' >=EditPrdFr AND '".$sysDate."' <=EditPrdTo
		) as ipcb ON iptfsub.BatchID=ipcb.BatchID AND ipcc.CycleID=ipcb.CycleID	
		INNER JOIN(	
			SELECT UserID,".$indexVar['libappraisal']->getLangSQL("ChineseName","EnglishName")." as UserName,UserLogin FROM ".$appraisalConfig['INTRANET_USER']." 
		) as iu ON iptf.UserID=iu.UserID
		INNER JOIN(	
			SELECT UserID,".$indexVar['libappraisal']->getLangSQL("ChineseName","EnglishName")." as FillerUserName,UserLogin FROM ".$appraisalConfig['INTRANET_USER']." 
		) as iuFiller ON iptfsub.FillerID=iuFiller.UserID
		INNER JOIN(
			SELECT TemplateID,CONCAT(FrmTitleChi,'-',FrmCodChi,'-',ObjChi) as FrmTitleChi,CONCAT(FrmTitleEng,'-',FrmCodEng,'-',ObjEng) as FrmTitleEng FROM INTRANET_PA_S_FRMTPL 
		) as ipsf ON iptf.TemplateID=ipsf.TemplateID
		INNER JOIN(
			SELECT SecID,CONCAT(SecTitleChi,'-',SecCodChi) as SecTitleChi,CONCAT(SecTitleEng,'-',SecCodEng) as SecTitleEng FROM INTRANET_PA_S_FRMSEC 
		) as ipsfrmsec ON ipcb.SecID=ipsfrmsec.SecID
	) as a
		";
//echo $sql."<br/><br/>";
$a=$connection->returnResultSet($sql);

$btnAry = array();
$btnAry[] = array('export', 'javascript: export_csv();');
$btnAry[] = array('print', 'javascript: print_timetable();');

$x = $indexVar['libappraisal_ui']->Get_Content_Tool_By_Array_v30($btnAry);
$x .= "<br/><br/>";
$x .= "<table>"."\r\n";
$x .= "<tr><td colspan=3><b>".sprintf($Lang['Appraisal']['ReportOutstanding']['Title'],$sysDate)."</b></td></tr>"."\r\n";
$x .= "</table><br/>"."\r\n";

$x .= "<table class=\"common_table_list\">"."\r\n";
$x .= "<tr><th width=\"10%\">".$Lang['Appraisal']['ReportOutstanding']['User']."</th><th width=\"20%\">".$Lang['Appraisal']['ReportOutstanding']['Template']."</th><th width=\"10%\">".$Lang['Appraisal']['ReportOutstanding']['Filler']."</th><th width=\"20%\">".$Lang['Appraisal']['ReportOutstanding']['CurSec']."</th><th width=\"15%\">".$Lang['Appraisal']['ReportOutstanding']['SubDatFr']."</th>"."\r\n";;
$x .= "<th width=\"15%\">".$Lang['Appraisal']['ReportOutstanding']['SubDatTo']."</th><th width=\"10%\">".$Lang['Appraisal']['ReportOutstanding']['RmnSubDat']."</th></tr>"."\r\n";

for($i=0;$i<sizeof($a);$i++){
	$x .= "<tr>";
		$x .= "<td>".$a[$i]["UserName"]."</td>";
		$x .= "<td>".$a[$i]["FrmTitle"]."</td>";
		$x .= "<td>".$a[$i]["FillerUserName"]."</td>";
		$x .= "<td>".$a[$i]["SecTitle"]."</td>";
		$x .= "<td>".$a[$i]["EditPrdFr"]."</td>";
		$x .= "<td>".$a[$i]["EditPrdTo"]."</td>";
		$x .= "<td>".$a[$i]["RmnDay"]."</td>";
	$x .= "</tr>";
}

$x .= "</table>";
echo $x;























?>