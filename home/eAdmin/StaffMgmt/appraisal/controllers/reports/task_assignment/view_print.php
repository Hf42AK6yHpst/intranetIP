<?
//editing by 
####################################### Change Log #################################################
# 2018-08-06 Paul: create this file
####################################################################################################
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/appraisal_lang.$intranet_session_language.php");

intranet_opendb();

include_once($PATH_WRT_ROOT."includes/appraisal/appraisalConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/appraisal/libappraisal.php");
include_once($PATH_WRT_ROOT."includes/appraisal/libappraisal_ui.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();
$indexVar['libappraisal'] = new libappraisal();
$indexVar['libappraisal_ui'] = new libappraisal_ui();

?>
<style type="text/css">
#PrintArea table {
	border:1px solid black;
}
#PrintArea table th{
	border:1px solid black;
	color:black;
}
#PrintArea table td{
	border:1px solid black;
}
@page {
	size: landscape; 
}
</style>
<link href="<?=$PATH_WRT_ROOT?>templates/2009a/css/content_30.css" rel="stylesheet" type="text/css">
<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='left'><?=$indexVar['libappraisal_ui']->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
		<td align='left'><input type="hidden" id="CycleID" value="<?=$cycleID?>"></td>
		<td align='left'><input type="hidden" id="LoginID" value="<?=$login?>"></td>
		<td align='left'><input type="hidden" id="mode" value="<?=$mode?>"></td>
	</tr>
</table>
<div id="PrintArea">
</div>
<?php
intranet_closedb();
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");

?>
<script>
var reportContent = "";
var ajaxImage = '<?=$indexVar['libappraisal_ui']->Get_Ajax_Loading_Image()?>';
$(document).ready( function() {
	goRptGen();	
});

function goRptGen(){
	var cycleID=$("#CycleID").val();
	var login = $("#LoginID").val();
	var mode = $("input[name='mode']:checked").val();
	$('#PrintArea').html(ajaxImage);
	if(cycleID != ''){
		$.post(
			'/home/eAdmin/StaffMgmt/appraisal/index.php?task=reports<?=$appraisalConfig['taskSeparator']?>task_assignment<?=$appraisalConfig['taskSeparator']?>ajax_getData&Type=GetReport',
			{
				"cycle": cycleID,
				"login": login,
				"mode": mode
			},
			function(data){
				$('#PrintArea').html(data);
				$('#submit2').click();
			}
		);
	}else{
		alert('error!');
	}
}
</script>