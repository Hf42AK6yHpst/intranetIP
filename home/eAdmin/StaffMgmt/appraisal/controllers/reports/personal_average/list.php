<?php
// ============================== Related Tables ==============================
// INTRANET_PA_T_FRMSUM,INTRANET_PA_S_FRMTPL,INTRANET_PA_C_GRPSUM,INTRANET_PA_C_IDVFRM,INTRANET_PA_C_BATCH,INTRANET_PA_T_FRMSUB,INTRANET_PA_S_APPROL
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
### check access right


$currentDate = date("Y-m-d");

$arrCookies = array();
$arrCookies[] = array("records_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}

/*
 $ltimetable = new Timetable();
 $curTimetableId = $ltimetable->Get_Current_Timetable();
 */
$connection = new libgeneralsettings();
$currentDate = date("Y-m-d");
$a = $indexVar['libappraisal']->getAcademicYearTerm($currentDate);

if(!isset($cycleID)){
	// get Current Cycle Description
	$sql = "SELECT CycleID,AcademicYearID,DescrChi,DescrEng FROM INTRANET_PA_C_CYCLE WHERE AcademicYearID=".IntegerSafe($a[0]["AcademicYearID"]).";";
	$a = $connection->returnResultSet($sql);
	$cycleID = ($a[0]["CycleID"]!="")?$a[0]["CycleID"]:0;
	$cycleDesc = Get_Lang_Selection($a[0]["DescrChi"],$a[0]["DescrEng"]);
}

# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['ReportPersonalAverageList']);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libappraisal_ui']->Include_Thickbox_JS_CSS();

// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$order = ($order == '') ? 1 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 7 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 10 : $numPerPage;

$x = "<br/>";

// ============================== Filter for Cycle (If not set, use current Cycle) ===================================
$conds = "";
if(!in_array($_SESSION['UserID'], $indexVar['libappraisal']->getReportDisplayPersonList(false))){
    $conds = " WHERE ReportReleaseDate IS NOT NULL ";
}
$sql="SELECT CycleID, DescrChi, YearNameB5, DescrEng, YearNameEN
	FROM(
		SELECT CycleID,DescrChi,DescrEng,AcademicYearID
		FROM INTRANET_PA_C_CYCLE $conds
	) as ipsf
	INNER JOIN(
		SELECT AcademicYearID,YearNameEN,YearNameB5 FROM ACADEMIC_YEAR
	) as ay ON ipsf.AcademicYearID=ay.AcademicYearID";
$tmp_cycleList=$connection->returnResultSet($sql);
$cycleList = array();
if (count($tmp_cycleList) > 0) {
	foreach ($tmp_cycleList as $kk => $vv) {
		$cycleList[$kk] = array();
		$cycleList[$kk]["CycleID"] = $vv["CycleID"];
		if ($indexVar['libappraisal']->isEJ()) {
			$cycleList[$kk]["DescrChi"] = $vv["DescrChi"] . " (" . $indexVar['libappraisal']->displayChinese($vv["YearNameB5"]) . ")";
		} else {
			$cycleList[$kk]["DescrChi"] = $vv["DescrChi"] . " (" . $vv["YearNameB5"] . ")";
		}
		$cycleList[$kk]["DescrEng"] = $vv["DescrEng"] . " (" . $vv["YearNameEN"] . ")";
	}
}

$sql="SELECT ipcf.TemplateID,ipsf.FrmInfoChi,ipsf.FrmInfoEng
		FROM(
			SELECT CycleID,TemplateID FROM INTRANET_PA_C_FRMSEL WHERE CycleID=".IntegerSafe($cycleID)."
		) as ipcf
		INNER JOIN(
			SELECT TemplateID,FrmTitleChi,FrmTitleEng,FrmCodChi,FrmCodEng,ObjChi,ObjEng,
			CONCAT(FrmTitleChi,'-',FrmCodChi,'-',ObjChi) as FrmInfoChi,CONCAT(FrmTitleEng,'-',FrmCodEng,'-',ObjEng) as FrmInfoEng,TemplateType
			FROM INTRANET_PA_S_FRMTPL
		) as ipsf ON ipcf.TemplateID=ipsf.TemplateID
		UNION
		SELECT ipof.TemplateID,ipsf2.FrmInfoChi,ipsf2.FrmInfoEng
		FROM(
			SELECT CycleID,TemplateID FROM INTRANET_PA_C_OBSSEL WHERE CycleID=".IntegerSafe($cycleID)."
		) as ipof
		INNER JOIN(
			SELECT TemplateID,FrmTitleChi,FrmTitleEng,FrmCodChi,FrmCodEng,ObjChi,ObjEng,
			CONCAT(FrmTitleChi,'-',FrmCodChi,'-',ObjChi) as FrmInfoChi,CONCAT(FrmTitleEng,'-',FrmCodEng,'-',ObjEng) as FrmInfoEng,TemplateType
			FROM INTRANET_PA_S_FRMTPL
		) as ipsf2 ON ipof.TemplateID=ipsf2.TemplateID";
							   
$FormList = $connection->returnArray($sql);

$x ="<table class=\"form_table slrs_form\">"."\r\n";
$x .="<tr>"."\r\n";
$x .= "<td width=\"30%\">".$indexVar['libappraisal_ui']->RequiredSymbol().$Lang['Appraisal']['AppraisalPeriod']."</td><td>:</td>"."\r\n";
$x .= "<td width=\"70%\">".getSelectByAssoArray($indexVar['libappraisal']->cnvGeneralArrToSelect($cycleList, "CycleID","DescrChi","DescrEng"), $selectionTags='id="CycleID" name="CycleID" onChange="javascript:reloadTemplateSel()"', $SelectedType=$a[$i]["CycleID"], $all=0, $noFirst=0);
$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('cycleIDEmptyWarnDiv', $Lang['Appraisal']['Report']['MandatorySelection'], $Class='warnMsgDiv')."\r\n";
$x .= "</td>";
$x .= "</tr>"."\r\n";
$x .= "<tr>"."\r\n";
if (!$_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["isService"]) {
	$x .= "<td>"."\r\n";
	$x .= $indexVar['libappraisal_ui']->RequiredSymbol()."<span>".$Lang['Appraisal']['ReportPersonResult']['SelectAppraisee']."</span><br/>";
	$x .= "</td>"."\r\n";
	$x .= "<td>:</td>";
	$x .= "<td>";	
	$x .= "<span id=\"staff_selection\">";
	//$x .= $Lang['Appraisal']['SearchByLoginID']." "."<input type=\"text\" id=\"LoginID\" name=\"LoginID\" value=\"\"> ";
	//$x .= $indexVar['libappraisal_ui']->GET_BTN($Lang['Btn']['Select'], "button", "goSelect()", 'selectBtn')."<br>\r\n";
	//$x .= $indexVar['libappraisal']->Get_Staff_Selection('UserID','UserID',$__ParIndividual=true,$__ParIsMultiple=false,$__ParSize=20,$__ParSelectedValue=$StaffID,$__ParOthers="",$__ParAllStaffOption=false,$__ShowOptGroupLabel=true);
	$x .= "</span>";
	$x .= "</td>";
//	$x .= "<tr>";
//	$x .= "<td>";
//	$x .= $indexVar['libappraisal_ui']->RequiredSymbol().$Lang['Appraisal']['ReportPersonResult']['SelectedAppraisee']."<br/>";
//	$x .= "<td>:</td>";
	$x .= "<td>";
//	$x .= "<span id=\"UserName\" name=\"UserName\"></span>";
//	$x .= "<input type=\"hidden\" name=\"UserID\" id=\"UserID\" value/>";
//	$x .= "</td>";
	$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('userNameEmptyWarnDiv', $Lang['Appraisal']['Report']['MandatorySelection'], $Class='warnMsgDiv')."\r\n";
	$x .= "</td>";
}else{
	$sql = "SELECT UserLogin FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID='".$_SESSION['UserID']."'";
	$LoginID = current($connection->returnVector($sql));
	$x .= "<td><input type=\"hidden\" id=\"LoginID\" name=\"LoginID\" value=\"".$LoginID."\"><input type=\"hidden\" name=\"UserID\" id=\"UserID\" value=\"".$_SESSION['UserID']."\"/></td>";	
}
$x .= "</tr>"."\r\n";
$x .= "<tr id='templateShowAllRow'>";
if (!$_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["isService"]) {
$x .= "<td>"."\r\n";
$x .= "<span>".$indexVar['libappraisal_ui']->RequiredSymbol().$Lang['Appraisal']['AllForm']."</span><br/>";
$x .= "</td>"."\r\n";
$x .= "<td>:</td>";
$x .= "<td id='templateShowAllCol'>".$indexVar['libappraisal_ui']->Get_Checkbox("templateShowAll", "templateShowAll", $Value=1, $isChecked=1, $Class=$isRequiredField, $Display="", $Onclick="",$isDisabled=0)."</td>";
$x .= "</tr>";
$x .= "<tr id='templateSelectionRow' style=\"display:none;\">"."\r\n";
$x .= "<td>"."\r\n";
$x .= "<span>".$indexVar['libappraisal_ui']->RequiredSymbol().$Lang['Appraisal']['ARForm']."</span><br/>";
$x .= "</td>"."\r\n";
$x .= "<td>:</td>";
/*
$x .= "<td>";
$x .= $filter2;
$x .= "</td>";
*/
$x .= "<td id='templateSelection'>".getSelectByAssoArray($indexVar['libappraisal']->cnvGeneralArrToSelect($FormList, "TemplateID","FrmInfoChi","FrmInfoEng"), $selectionTags='id="TemplateID" name="templateID"', $SelectedType=$templateID, $all=0, $noFirst=0);
}else{
	$sql = "SELECT UserLogin FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID='".$_SESSION['UserID']."'";
	$LoginID = current($connection->returnVector($sql));
	$x .= "<td><input type=\"hidden\" id=\"LoginID\" name=\"LoginID\" value=\"".$LoginID."\"><input type=\"hidden\" name=\"UserID\" id=\"UserID\" value=\"".$_SESSION['UserID']."\"/></td>";	
}
$x .= "</tr>"."\r\n";


$x .= "</table>";
		

$htmlAry['contentTbl'] = $x;
// ============================== Transactional data ==============================

// ============================== Define Button ==============================
$htmlAry['viewBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnRptGen'], "submit", "goRptGen()", 'genRptBtn');

$btnAry[] = array('print', 'javascript:Print()','',array(),' id="PrintBtn" style="display:none;"');
$htmlAry['contentTool'] = $indexVar['libappraisal_ui']->Get_Content_Tool_By_Array_v30($btnAry);
// ============================== Define Button ==============================

?>