<?php
/**
 * Change Log:
 * 2018-01-05 Pun
 *  - Added access right checking
 *  - Added seperate YearClass result
 */
include_once($PATH_WRT_ROOT."includes/appraisal/lnt/libappraisal_lnt.php");

$libLnt = new libappraisal_lnt();

define('DECIMAL_PLACE_PERCENT', 1);
define('DECIMAL_PLACE_NORMAL', 2);
$AcademicYearID = IntegerSafe($AcademicYearID);
$FromSubjectID = IntegerSafe($FromSubjectID);
$TeacherID = IntegerSafe($TeacherID);
//$school_name = $_SESSION["SSV_PRIVILEGE"]["school"]["name"];


#### Access Right START ####
if($_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["isService"]){
    $TeacherID = $_SESSION['UserID'];
}
#### Access Right END ####


#### Get YearClass Name ####
$sql = "SELECT
    YC.YearID,
    YC.YearClassID,
    YC.ClassTitleEN,
    YC.ClassTitleB5,
    Y.YearName
FROM
    YEAR_CLASS YC
INNER JOIN
    YEAR Y
USING 
    (YearID)
WHERE
    YC.AcademicYearID = '{$AcademicYearID}'
ORDER BY
    Y.Sequence, YC.Sequence";
$rs = $libLnt->returnResultSet($sql);

$yearNameMapping = array();
$allYearClass = array();
foreach($rs as $r){
    $r['ClassTitle'] = Get_Lang_Selection($r['ClassTitleB5'], $r['ClassTitleEN']);
    $allYearClass[$r['YearClassID']] = $r;
    $yearNameMapping["Y{$r['YearID']}"] = $r['YearName'];
}
#### Get YearClass END ####


#### Get questions START ####
$rs = $libLnt->getAllQuestionByAcademicYearID($AcademicYearID);
$questionSubjectId = array_filter(array_unique(Get_Array_By_Key($rs, 'SubjectID')));
$filterSubjectId = (in_array($FromSubjectID, $questionSubjectId))? $FromSubjectID : '';

$questions = array();
foreach($rs as $r){
    if($r['SubjectID'] != $filterSubjectId){
        continue;
    }
    $questions[] = $r;
}

$minScore = NULL;
$maxScore = NULL;
foreach($questions as $question){
    if($question['QuestionType'] == libappraisal_lnt::LNT_QUESTION_TYPE_SCALE){
        if($minScore === NULL || $question['MinScore'] < $minScore){
            $minScore = $question['MinScore'];
        }
        if($maxScore === NULL ||$question['MaxScore'] > $maxScore){
            $maxScore = $question['MaxScore'];
        }
    }
}

$countPartQuestion = array();
foreach($questions as $question){
    $countPartQuestion[$question['Part']]++;
}
#### Get questions END ####


#### Get answer START ####
$rs = $libLnt->getAnswerByAcademicYearIdSubjectIdTeacherId($AcademicYearID, $FromSubjectID, $TeacherID);
$subjectAnswerArr = array();
if($FromSubjectID){
    $subjectAnswerArr = $rs;
}else{
    foreach($rs as $r){
        if(in_array($r['SubjectID'], $questionSubjectId)){
            continue;
        }
        $subjectAnswerArr[] = $r;
    }
}
$allYearClassAnswers = BuildMultiKeyAssoc($subjectAnswerArr, array('YearClassID', 'QuestionnaireID', 'QuestionID') , array('Answer'), $SingleValue=1);
#### Get answer END ####

#### Calculate answer student START ####
$totalAnswerStudent = array();
foreach($subjectAnswerArr as $r){
    $totalAnswerStudent[$r['YearClassID']]++;
}
$totalQuestions = count($questions);
if($totalQuestions){
    foreach($totalAnswerStudent as $YearClassID => $totalAnswer){
        $totalAnswerStudent[$YearClassID] = $totalAnswer / $totalQuestions;
        if(!is_int($totalAnswerStudent[$YearClassID])){
            echo '<!--Total number of answer does not match question-->';
            exit;
        }
    }
}else{
    echo '<!--No question-->';
    exit;
}
#### Calculate answer student END ####

#### Group data (if result has only one student) START ####
$groupYearIdArr = array();
foreach($allYearClassAnswers as $YearClassID => $d){
    if($totalAnswerStudent[$YearClassID] == 1){
        $groupYearIdArr[] = $allYearClass[$YearClassID]['YearID'];
    }
}

$groupAnswers = array();
foreach($allYearClassAnswers as $YearClassID => $d){
    $yearClass = $allYearClass[$YearClassID];

    if(!isset($groupAnswers[$yearClass['YearID']])){
        $groupAnswers[$yearClass['YearID']] = array();
    }
    if(in_array($yearClass['YearID'], $groupYearIdArr)){
        foreach($d as $questionId => $d2){
            foreach($d2 as $recordId => $answer){
                $groupAnswers["Y{$yearClass['YearID']}"][$questionId][$recordId] = $answer;
            }
        }
    }else{
        $groupAnswers[$YearClassID] = $d;
    }
}
$groupAnswers = array_filter($groupAnswers);
#### Group data (if result has only one student) END ####


foreach($groupAnswers as $YearClassID => $allAnswers){
    if(strpos($YearClassID, 'Y') === 0){
        $classTitle = $yearNameMapping[$YearClassID];
    }else{
        $classTitle = $allYearClass[$YearClassID]['ClassTitle'];
    }
    
    #### Calculate data START ####
    $answerArr = array();
    foreach($questions as $question){
        if($question['QuestionType'] != libappraisal_lnt::LNT_QUESTION_TYPE_SCALE){
            continue;
        }
        
        $questionId = $question['QuestionID'];
        foreach((array)$allAnswers as $answerInfo){
            foreach((array)$answerInfo[$questionId] as $answer){
                $answerArr[$questionId][$answer]++;
            }
        }
    }
    // debug_r($answerArr);
    // debug_r($countAnswerArr);
    #### Calculate data END ####
    
    /* * /
    debug_r($countPartQuestion);
    debug_r($answerAverageArr);
    debug_r($statResultQuestionArr);
    debug_r($statResultPartArr);
    /* */
    
    ?>
    
    <style>
    .realNum{
        display:none;
    }
    </style>
    
    <h1 style="margin-top: 50px;"><?=$classTitle ?></h1>
    
    <div class="chart_tables">
    	<table class="common_table_list_v30 view_table_list_v30 resultTable">
    		<thead>
    			<tr>
    				<th rowspan="2" style="width: 50px;">&nbsp;</th>
    				<?php foreach($countPartQuestion as $part => $count){ ?>
    					<th colspan="<?=$count ?>" style="text-align: center;"><?=$Lang['Appraisal']['LNT']['Part'] . ' ' . $part ?></th>
    				<?php } ?>
    			</tr>
    			
    			<tr>
    				<?php 
    				foreach($questions as $question){
    				    if($question['QuestionType'] == libappraisal_lnt::LNT_QUESTION_TYPE_SCALE){
    				?>
    						<th title="<?=$question['QuestionTitle'] ?>"><?=$question['QuestionNumber'] ?></th>
    				<?php
    				    }else{
    				?>
    						<th><?=$question['QuestionTitle'] ?></th>
    				<?php
    				    }
    				} 
    				?>
    			</tr>
    		</thead>
    		<tbody>
    		
    			<?php 
    			$index = 1;
    			foreach($allAnswers as $answers){ 
    			?>
    				<tr>
    					<td>#<?= $index++ ?></td>
    					<?php 
    					foreach($questions as $question){ 
    					    $questionId = $question['QuestionID'];
    					?>
    						<td><?=$answers[$questionId] ?></td>
    					<?php
    					}
    					?>
    				</tr>
    			<?php 
    			} 
    			?>
    		
    			<tr>
    				<td colspan="999">&nbsp;</td>
    			</tr>
    		</tbody>
    		
    		<thead>
    			<tr>
    				<th><?=$Lang['Appraisal']['LNT']['Score'] ?></th>
    				<th colspan="999"><?=$Lang['Appraisal']['LNT']['Report']['Count'] ?></th>
    			</tr>
    		</thead>
    		<tbody>
    			<?php for($i=$maxScore;$i>=$minScore;$i--){ ?>
    				<tr>
    					<td><?=$i ?></td>
    					<?php 
    					foreach($questions as $question){ 
    					    $questionId = $question['QuestionID'];
    					    if($question['QuestionType'] == libappraisal_lnt::LNT_QUESTION_TYPE_SCALE){
    					?>
    							<td><?=number_format($answerArr[$questionId][$i], 0) ?></td>
    					<?php
    					    }else{
    					?>
    							<td>&nbsp;</td>
    					<?php 
    					    }
    					} 
    					?>
    				</tr>
    			<?php } ?>
    		</tbody>
    		
    	</table>
    </div>
    
<?php
} // End foreach($allYearClassAnswers as $allAnswers)

?>