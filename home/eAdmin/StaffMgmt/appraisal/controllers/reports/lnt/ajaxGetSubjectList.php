<?php
/**
 * Change Log:
 * 2019-03-20 Pun
 *  - File Created
 */
include_once($PATH_WRT_ROOT."includes/appraisal/lnt/libappraisal_lnt.php");

$libLnt = new libappraisal_lnt();
$libSCM_ui = new subject_class_mapping_ui();

$AcademicYearID = IntegerSafe($AcademicYearID);

#### Get subjectId START ####
$rs = $libLnt->getAnswerByAcademicYearIdSubjectIdTeacherId($AcademicYearID);
$subjectIdArr = array_unique(Get_Array_By_Key($rs, 'SubjectID'));
$subjectIdSql = implode("','", $subjectIdArr);
#### Get subjectId END ####

#### Get special question subjectId START ####
$rs = $libLnt->getAllQuestionByAcademicYearID($AcademicYearID);
$specialQuestionSubjectIdArr = array_unique(Get_Array_By_Key($rs, 'SubjectID'));
#### Get special question subjectId END ####

#### Get subject info START ####
$sql = "SELECT 
    RecordID AS SubjectID, 
    CH_DES, 
    EN_DES
FROM 
    ASSESSMENT_SUBJECT 
WHERE 
    RecordID IN ('{$subjectIdSql}') 
AND 
    (CMP_CODEID = '' OR CMP_CODEID IS NULL) 
AND 
    RecordStatus='1' 
";
$rs = $libLnt->returnResultSet($sql);
foreach($rs as $index=>$r){
    $rs[$index]['IsSpecialSubject'] = in_array($r['SubjectID'], $specialQuestionSubjectIdArr);
    $rs[$index]['Title'] = Get_Lang_Selection($r['CH_DES'],$r['EN_DES']);
}
#### Get subject info END ####

#### Pack subject info START ####
$subjectInfoArr = array(
    "{$Lang['Appraisal']['LNT']['GeneralSubject']}" => array(),
    "{$Lang['Appraisal']['LNT']['OtherSubject']}" => array(),
);
foreach($rs as $r){
    if($r['IsSpecialSubject']){
        $subjectInfoArr["{$Lang['Appraisal']['LNT']['OtherSubject']}"][$r['SubjectID']] = $r['Title'];
    }else{
        $subjectInfoArr["{$Lang['Appraisal']['LNT']['GeneralSubject']}"][$r['SubjectID']] = $r['Title'];
    }
}

if(empty($subjectInfoArr["{$Lang['Appraisal']['LNT']['GeneralSubject']}"])){
    unset($subjectInfoArr["{$Lang['Appraisal']['LNT']['GeneralSubject']}"]);
}
if(empty($subjectInfoArr["{$Lang['Appraisal']['LNT']['OtherSubject']}"])){
    unset($subjectInfoArr["{$Lang['Appraisal']['LNT']['OtherSubject']}"]);
}
#### Pack subject info END ####

if($subjectInfoArr){
    echo getSelectByAssoArray($subjectInfoArr, "name='FromSubjectID' id='FromSubjectID'", '', 0, 0, $Lang['Appraisal']['LNT']['AllGeneralSubject']);
}else{
    echo $Lang['General']['NoRecordAtThisMoment'];
}