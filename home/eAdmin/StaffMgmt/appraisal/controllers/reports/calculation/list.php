<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST, INTRANET_SLRS_LESSON_ARRANGEMENT
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

$linterface = new interface_html();
$connection = new libgeneralsettings();

# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['ReportCalculationAppraisal']);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libappraisal_ui']->Include_Thickbox_JS_CSS();


$x ="<table class=\"form_table slrs_form\">"."\r\n";
$x .= "<tr>";
$x .= "<td>".$Lang['Appraisal']['Template']['BatchID']."</td><td>:</td>"."\r\n";
$x .= "<td>";
$x .= $indexVar['libappraisal_ui']->GET_TEXTBOX_NUMBER("batchID", "batchID", "", $OtherClass="", $OtherPar=array('maxlength'=>255));
$x .= "</td>";
$x .= "</tr>"."\r\n";
$x .="<tr>"."\r\n";
/*$x .= "<td>".$Lang['Appraisal']['Template']['RecordID']."</td><td>:</td>"."\r\n";
$x .= "<td>";
$x .= $indexVar['libappraisal_ui']->GET_TEXTBOX_NUMBER("recordID", "recordID", "", $OtherClass="", $OtherPar=array('maxlength'=>255));
$x .= "</td><td></td>";
$x .= "</tr>";*/
$x .= "<td>";
$x .= $Lang['Appraisal']['SearchByLoginID']."</td><td>:</td><td>"."<input type=\"text\" id=\"LoginID\" name=\"LoginID\" value=\"\">";
$x .= "</td>";
$x .= "</tr>";

$x .= "</table>";


$htmlAry['contentTbl'] = $x;
// ============================== Transactional data ==============================

// ============================== Define Button ==============================
$htmlAry['viewBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnRptCal'], "submit", "goRptCal()", 'genRptBtn');
// ============================== Define Button ==============================
 










?>