<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');

# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$connection = new libgeneralsettings();
$loginID=$_POST["userID"];
$sql="SELECT UserID,UserLogin FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserLogin='".$loginID."';";
$a=$connection->returnResultSet($sql);
$userID=$a[0]["UserID"];
$batchID=$_POST["batchID"];
$qIDArr = array();
$inTypArr = array();
//echo $batchID;

$sql = "SELECT RlsNo,iptfrmsub.RecordID,BatchID,CycleID,TemplateID
		FROM (
			SELECT RlsNo,RecordID,BatchID FROM INTRANET_PA_T_FRMSUB WHERE BatchID=".IntegerSafe($batchID)." AND SubDate IS NOT NULL
		) as iptfrmsub
		INNER JOIN(
			SELECT RecordID,CycleID,TemplateID FROM INTRANET_PA_T_FRMSUM WHERE IsActive=1 AND UserID=".$userID."
		) as iptfrmsum WHERE iptfrmsub.RecordID=iptfrmsum.RecordID		
		";
//echo $sql."<br/>";
$z0=$connection->returnResultSet($sql);

$sql = "SELECT ipsfrmsec.SecID
		FROM(
			SELECT SecID,TemplateID FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($z0[0]["TemplateID"])." AND ParentSecID IS NULL
		) as ipsfrmsec
		INNER JOIN(
			SELECT SecID,AppRoleID,CanFill FROM INTRANET_PA_S_SECROL WHERE AppRoleID=1 AND CanFill=1
		) as ipssecrol ON ipsfrmsec.SecID=ipssecrol.SecID
		";
//echo $sql."<br/>";
$z1=$connection->returnResultSet($sql);
//$parentSecIDArr=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($z1,"SecID");

for($a1=0;$a1<sizeof($z1);$a1++){
	
	$sql = "SELECT ipsfrmsec.SecID,ipsqcat.QCatID
			FROM(
				SELECT SecID,TemplateID FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($z0[0]["TemplateID"])." AND ParentSecID IN (".$z1[$a1]["SecID"].")
			) as ipsfrmsec
			INNER JOIN(
				SELECT QCatID,SecID,IsAvgMark,AvgDec,IsVerAvgMark,VerAvgDec,QuesDispMode FROM INTRANET_PA_S_QCAT	
			) as ipsqcat ON ipsfrmsec.SecID=ipsqcat.SecID			
			GROUP BY ipsfrmsec.SecID,ipsqcat.QCatID
			ORDER BY SecID,QCatID;
			";
	//echo $sql."<br/>";
	$z2=$connection->returnResultSet($sql);
	
	for($a2=0;$a2<sizeof($z2);$a2++){
		$sql = "SELECT ipsfrmsec.SecID,ipsqcat.QCatID,QID,IsAvgMark,AvgDec,IsVerAvgMark,VerAvgDec,ScoreIsAvgMark,ScoreAvgDec,QuesDispMode,InputType
			FROM(
				SELECT SecID,TemplateID FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($z0[0]["TemplateID"])." AND ParentSecID IN (".$z1[$a1]["SecID"].")
			) as ipsfrmsec
			INNER JOIN(
				SELECT QCatID,SecID,IsAvgMark,AvgDec,IsVerAvgMark,VerAvgDec,QuesDispMode FROM INTRANET_PA_S_QCAT
				WHERE SecID=".$z2[$a2]["SecID"]." AND QCatID=".$z2[$a2]["QCatID"]."
			) as ipsqcat ON ipsfrmsec.SecID=ipsqcat.SecID
			LEFT JOIN(
				SELECT QID,QCatID,InputType,IsAvgMark as ScoreIsAvgMark,AvgDec as ScoreAvgDec FROM INTRANET_PA_S_QUES
			) as ipsques ON ipsqcat.QCatID=ipsques.QCatID
			ORDER BY SecID,QCatID;
			";
		//echo $sql."<br/>";
		$z3=$connection->returnResultSet($sql);
		$qIDArr[$a2]=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($z3,"QID");
		if($z3[0]["QuesDispMode"]==1){
			$inTypArr[$a2]="Likert";
		}
		else if($z3[0]["QuesDispMode"]==0 && $z3[0]["InputType"]==3){
			$inTypArr[$a2]="Score";
		}
	}		
}
for($b0=0;$b0<sizeof($inTypArr);$b0++){
	if($inTypArr[$b0]=="Likert"){
		// get Likert Information
		$sql ="SELECT RlsNo,iptsd.RecordID,BatchID,iptsd.QID,QCatID,IsAvgMark,AvgDec,IsVerAvgMark,VerAvgDec,UserID,AprID,SecID
				FROM(
					SELECT RlsNo,RecordID,BatchID,QID FROM INTRANET_PA_T_SLF_DTL WHERE BatchID=".IntegerSafe($batchID)."
				) as iptsd
				INNER JOIN(
					SELECT QID,ipsqcat.QCatID,IsAvgMark,AvgDec,IsVerAvgMark,VerAvgDec,SecID
					FROM( 		
						SELECT QID,QCatID FROM INTRANET_PA_S_QUES WHERE QID IN (".$qIDArr[$b0].")
					) as ipsq
					INNER JOIN(
						SELECT QCatID,QuesDispMode,IsAvgMark,AvgDec,IsVerAvgMark,VerAvgDec,SecID FROM INTRANET_PA_S_QCAT WHERE QuesDispMode=1 
					) as ipsqcat ON ipsq.QCatID=ipsqcat.QCatID
				) as a ON iptsd.QID=a.QID
				INNER JOIN(
					SELECT RecordID,CycleID,UserID,AprID FROM INTRANET_PA_T_FRMSUM WHERE IsActive AND UserID=".$userID."
				) as iptf ON iptsd.RecordID=iptf.RecordID
				ORDER BY iptsd.QID,AprID
				";
		//echo $sql."<br/>";;
		$a=$connection->returnResultSet($sql);
		
		$sql = "SELECT RlsNo,iptfsum.RecordID,BatchID,CycleID,iu.UserID,UserName,AprID,AprName
				FROM(
					SELECT RlsNo,RecordID,BatchID FROM INTRANET_PA_T_FRMSUB WHERE BatchID=".IntegerSafe($batchID)."
				) as iptfsub
				INNER JOIN(
					SELECT RecordID,CycleID,UserID,AprID FROM INTRANET_PA_T_FRMSUM WHERE IsActive=1 AND UserID=".$userID."
				) as iptfsum ON iptfsub.RecordID=iptfsum.RecordID
				INNER JOIN(	
					SELECT UserID,".$indexVar['libappraisal']->getLangSQL("ChineseName","EnglishName")." as UserName,UserLogin FROM ".$appraisalConfig['INTRANET_USER']." 
				) as iu ON iptfsum.UserID=iu.UserID
				INNER JOIN(	
					SELECT UserID,".$indexVar['libappraisal']->getLangSQL("ChineseName","EnglishName")." as AprName,UserLogin FROM ".$appraisalConfig['INTRANET_USER']." 
				) as iuApr ON iptfsum.AprID=iuApr.UserID
				";
		//echo $sql."<br/>";
		$b=$connection->returnResultSet($sql);
		
		$x .= $Lang['Appraisal']['TemplateQues']['SDFAvgMark'].": ". $Lang['Appraisal']['TemplateSample'][$a[0]["IsAvgMark"]]."<br/>";
		$x .= $Lang['Appraisal']['TemplateSample']['SDFAvgDec'].": ". $a[0]["AvgDec"]."<br/><br/>";
		$x .= $Lang['Appraisal']['TemplateSample']['SDFVerAvgMark'].": ". $Lang['Appraisal']['TemplateSample'][$a[0]["IsVerAvgMark"]]."<br/>";
		$x .= $Lang['Appraisal']['TemplateSample']['SDFVerAvgDec'].": ". $a[0]["VerAvgDec"]."<br/>";
		
		$x .= "<table class=\"common_table_list\">";
		$x.= "<tr><th></th>";
		for($i=0;$i<sizeof($b);$i++){
			$x .= "<th>".$b[$i]["AprName"]."</th>";
		}
		if($a[0]["IsAvgMark"]==1){
			$x .= "<th>".sprintf($Lang['Appraisal']['TemplateSample']['SDFAvg'],$a[0]["AvgDec"])."</th>";
		}
		$x .= "</tr>";
		$value = 0;
		$dec = 0;
		//$verValue = array();
		$verValue = 0;
		$numRow = 0;
		for($i=0;$i<sizeof($a);$i++){
			$sql="SELECT QID, ".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng"). " as Descr FROM INTRANET_PA_S_QUES WHERE QID=".$a[$i]["QID"].";";
			$c=$connection->returnResultSet($sql);
			
			if($i % sizeof($b)==0){
				$x .= "<tr>";
				$x .= "<td>".$c[0]["Descr"]."</td>";
				$value = 0;
				$numRow = $numRow+1;
			}	
			for($j=0;$j<sizeof($b);$j++){
				if($a[$i]["AprID"]==$b[$j]["AprID"]){	
					$sql = "SELECT MarkID FROM INTRANET_PA_T_SLF_DTL WHERE RlsNo=".IntegerSafe($a[$i]["RlsNo"])." AND RecordID=".IntegerSafe($a[$i]["RecordID"])." AND BatchID=".IntegerSafe($a[$i]["BatchID"])." AND QID=".IntegerSafe($a[$i]["QID"]).";";
					$d=$connection->returnResultSet($sql);
					$sql = "SELECT MarkID,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng"). " as Descr,Score FROM INTRANET_PA_S_QLKS WHERE MarkID=".IntegerSafe($d[0]["MarkID"]).";";
					$e=$connection->returnResultSet($sql);
					
					$x .= "<td>";
					$x .= $e[0]["Descr"];
					$x .= "</td>";	
					$value = $value + $e[0]["Score"];
					//$verValue[$j] = $verValue[$j] + $e[0]["Score"];
				}
			}		
			if($i % sizeof($b)== (sizeof($b)-1)){
				if($a[0]["IsAvgMark"]==1){
					$x .= "<td>";		
					$x .= round($value/sizeof($b),$a[0]["AvgDec"]);					
					$x .= "</td>";
					$verValue=$verValue + ($value/sizeof($b));
				}
				$x .="</tr>";		
			}	
		}
		if($a[0]["IsVerAvgMark"]==1){
			$x .= "<td>".sprintf($Lang['Appraisal']['TemplateSample']['SDFVerAvg'],$a[0]["VerAvgDec"])."</td>";
			for($j=0;$j<sizeof($b);$j++){
				$x .= "<td>";
				//$x .= round($verValue[$j]/sizeof($b),$a[0]["VerAvgDec"]);
				$x .= "</td>";
			}
			$x .= "<td>";
			$x .= round($verValue/$numRow,$a[0]["VerAvgDec"]);
			$x .= "</td>";
		}
		$x .= "</table><br/>";
	}
	else if($inTypArr[$b0]=="Score"){
		// get Score Information
		$sql ="SELECT RlsNo,iptsd.RecordID,BatchID,iptsd.QID,QCatID,IsAvgMark,AvgDec,UserID,AprID,SecID
				FROM(
					SELECT RlsNo,RecordID,BatchID,QID FROM INTRANET_PA_T_SLF_DTL WHERE BatchID=".IntegerSafe($batchID)."
				) as iptsd
				INNER JOIN(
					SELECT QID,ipsqcat.QCatID,IsAvgMark,AvgDec,SecID
					FROM(
						SELECT QID,QCatID,IsAvgMark,AvgDec FROM INTRANET_PA_S_QUES WHERE QID IN (".$qIDArr[$b0].")
					) as ipsq
					INNER JOIN(
						SELECT QCatID,QuesDispMode,SecID FROM INTRANET_PA_S_QCAT
					) as ipsqcat ON ipsq.QCatID=ipsqcat.QCatID
				) as a ON iptsd.QID=a.QID
				INNER JOIN(
					SELECT RecordID,CycleID,UserID,AprID FROM INTRANET_PA_T_FRMSUM WHERE IsActive=1 AND UserID=".$userID."
				) as iptf ON iptsd.RecordID=iptf.RecordID
				ORDER BY iptsd.QID,AprID
				";
		//echo $sql."<br/>";
		$a=$connection->returnResultSet($sql);
		
		$sql = "SELECT RlsNo,iptfsum.RecordID,BatchID,CycleID,iu.UserID,UserName,AprID,AprName
				FROM(
					SELECT RlsNo,RecordID,BatchID FROM INTRANET_PA_T_FRMSUB WHERE BatchID=".IntegerSafe($batchID)."
				) as iptfsub
				INNER JOIN(
					SELECT RecordID,CycleID,UserID,AprID FROM INTRANET_PA_T_FRMSUM WHERE IsActive=1 AND UserID=".$userID."
				) as iptfsum ON iptfsub.RecordID=iptfsum.RecordID
				INNER JOIN(
					SELECT UserID,".$indexVar['libappraisal']->getLangSQL("ChineseName","EnglishName")." as UserName,UserLogin FROM ".$appraisalConfig['INTRANET_USER']."
				) as iu ON iptfsum.UserID=iu.UserID
				INNER JOIN(
					SELECT UserID,".$indexVar['libappraisal']->getLangSQL("ChineseName","EnglishName")." as AprName,UserLogin FROM ".$appraisalConfig['INTRANET_USER']."
				) as iuApr ON iptfsum.AprID=iuApr.UserID
				";
		//echo $sql."<br/>";
		$b=$connection->returnResultSet($sql);
		
		$x .= $Lang['Appraisal']['TemplateQues']['SDFAvgMark'].": ". $Lang['Appraisal']['TemplateSample'][$a[0]["IsAvgMark"]]."<br/>";
		$x .= $Lang['Appraisal']['TemplateSample']['SDFAvgDec'].": ". $a[0]["AvgDec"]."<br/>";
		
		$x .= "<table class=\"common_table_list\">";
		$x.= "<tr><th></th>";
		for($i=0;$i<sizeof($b);$i++){
			$x .= "<th>".$b[$i]["AprName"]."</th>";
		}
		if($a[0]["IsAvgMark"]==1){
			$x .= "<th>".sprintf($Lang['Appraisal']['TemplateSample']['SDFAvg'],$a[0]["AvgDec"])."</th>";
		}
		$x .= "</tr>";
		$value = 0;
		$dec = 0;
		$verValue = array();
		
		for($i=0;$i<sizeof($a);$i++){
			$sql="SELECT QID, ".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng"). " as Descr FROM INTRANET_PA_S_QUES WHERE QID=".$a[$i]["QID"].";";
			$c=$connection->returnResultSet($sql);
			$curQID = $a[$i]["QID"];
				
			if($i % sizeof($b)==0){
				$x .= "<tr>";
				$x .= "<td>".$c[0]["Descr"]."</td>";
				$value = 0;
			}
			for($j=0;$j<sizeof($b);$j++){
				if($a[$i]["AprID"]==$b[$j]["AprID"]){
					$sql = "SELECT Score FROM INTRANET_PA_T_SLF_DTL WHERE RlsNo=".IntegerSafe($a[$i]["RlsNo"])." AND RecordID=".IntegerSafe($a[$i]["RecordID"])." AND BatchID=".IntegerSafe($a[$i]["BatchID"])." AND QID=".IntegerSafe($a[$i]["QID"]).";";
					$d=$connection->returnResultSet($sql);
						
					$x .= "<td>";
					$x .= $d[0]["Score"];
					$x .= "</td>";
					$value = $value + $d[0]["Score"];
				}
			}
		
			if($i % sizeof($b)== (sizeof($b)-1)){
				if($a[0]["IsAvgMark"]==1){
					$x .= "<td>";
					$x .= round($value/sizeof($b),$a[0]["AvgDec"]);
					$x .= "</td>";
				}
				$x .="</tr>";
			}
		}
		$x .= "</table><br/>";
	}
}

echo $x;




?>