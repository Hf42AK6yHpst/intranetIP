<?php

// ============================== Related Tables ==============================

// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once ($PATH_WRT_ROOT . "includes/libgeneralsettings.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "includes/appraisal/gen_form_class.php");
include_once ($PATH_WRT_ROOT . 'includes/json.php');

# Page Title
// ============================== Includes files/libraries ==============================

// ============================== Includes files/libraries ==============================
$connection = new libgeneralsettings();
$markerList = array();
$teacherPerformance = array();
$teacherQuestionList = array();
$teacherCommentList = array();
//$genform = new genform();

$type=($_GET["Type"]=="")?$_POST["Type"]:$_GET["Type"];
$q = $_GET["?q"];
$cycleID = $_POST['cycleID'];
$currTemplateID = $_POST['templateID'];
$getUserOnly = $_POST['getUserOnly'];
$currUserID = $_POST['userID'];
$loginID = $_POST["loginID"];
$sql="SELECT UserID,UserLogin FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserLogin='".$loginID."';";
$a=$connection->returnResultSet($sql);
if($currUserID==""){
	$currUserID = $a[0]['UserID'];
}
// ============================== generate teacher scores in each section and the average marks ==============================
if($type == "GetUserDetailsList"){
	/* Put data in table format */
//	$form .= "<table id='AverageTable' class='common_table_list_v30 tablesorter'>";
//	$form .= "<tr><thead>";
//	$form .= "<th width='50%'>".$Lang['Appraisal']['TemplateSampleForm']."</th>";
//	$form .= "<th width='50%'>".$Lang['Appraisal']['Report']['AverageMark']."</th>";
//	$form .= "<thead></tr><tbody>";	
//	
//	$sql = "SELECT DISTINCT psft.TemplateID,".Get_Lang_Selection("psft.FrmTitleChi","psft.FrmTitleEng")." as TemplateName
//		FROM INTRANET_PA_T_FRMSUM ptfs 
//		INNER JOIN INTRANET_PA_S_FRMTPL psft ON ptfs.TemplateID=psft.TemplateID
//		WHERE ptfs.CycleID=$cycleID AND ptfs.UserID='".$currUserID."'";
//	$templateInfo = $connection->returnArray($sql);	
//	foreach($templateInfo as $tinfo){
//		$sql = "SELECT RlsNo,ptfsub.RecordID,ptfsub.BatchID,ptfsum.UserID,TemplateID FROM INTRANET_PA_T_FRMSUB ptfsub
//					INNER JOIN INTRANET_PA_T_FRMSUM ptfsum ON ptfsub.RecordID=ptfsum.RecordID
//					WHERE CycleID='$cycleID' AND TemplateID='".$tinfo['TemplateID']."'  AND ptfsub.IsActive=1 AND ptfsum.UserID='".$currUserID."'";						
//		$basicInfo = $connection->returnArray($sql);
//		foreach ($basicInfo as $bi) {
//			getSdfAvg($connection, $bi['TemplateID'], $bi["UserID"], $bi["RecordID"], $cycleID, $bi["RlsNo"], $bi["BatchID"]);
//		}				
//		
//		foreach($teacherPerformance as $user=>$userContent){
//			$sum = 0;
//			$totalQ = 0;
//			$form .= "<tr>";
//			$form .= "<td>".$tinfo['TemplateName']."</td>";
//			foreach($userContent["Section"] as $secKey=>$secContent){
//				foreach($secContent["SubSection"] as $subKey=>$subContent){
//					$sectionSum = 0;
//					$sectionQno = 0;
//					foreach($subContent["QuestionData"] as $qdata){
//						$sectionSum = $sectionSum + $qdata['SectionSum'];
//						$sectionQno = $sectionQno + $qdata['SectionQNo'];
//					}
//					$sum = $sum + $sectionSum;
//					$totalQ = $totalQ + $sectionQno; 
//				}			
//			}
//			$average = ($totalQ == 0)?0:round($sum/$totalQ, 2);
//			//$form .= "<td>".number_format($sum,2)."</td>";
//			$form .= "<td>".number_format($average,2)."</td>";
//			$form .= "</tr>";
//		}		
//		//debug_r($teacherPerformance);
//	}
//	$form .= "<tbody></table>";	
}else if($type=="GetLoginUserSelection"){
	$cycleID = $_POST['CycleID'];
	if($cycleID==""){
		echo '';
		die();
	}else{
		$name_field = getNameFieldByLang("USR.");
		$sql = "SELECT Distinct USR.userLogin, 
				CASE 
					WHEN au.UserLogin IS NOT NULL then CONCAT({$name_field}, ' (".$Lang['Appraisal']['Deleted'].")') 
					ELSE {$name_field} 
				END  as name FROM INTRANET_PA_T_FRMSUM frmsum 
				INNER JOIN ".$appraisalConfig['INTRANET_USER']." USR ON frmsum.UserID=USR.UserID				
				INNER JOIN INTRANET_PA_T_FRMSUB frmsub ON frmsum.RecordID = frmsub.RecordID
				LEFT JOIN INTRANET_ARCHIVE_USER au ON USR.UserID = au.UserID	
				WHERE frmsum.CycleID='".$cycleID."' AND frmsub.IsLastSub=1";
		$resultList=$connection->returnResultSet($sql);
		$selList = array();
		foreach($resultList as $r){
			array_push($selList, array("id"=>$r['userLogin'], "name"=>$r['name']));
		}		
		echo  getSelectByAssoArray($indexVar['libappraisal']->cnvGeneralArrToSelect($selList, "id","name","name"), $selectionTags='id="LoginID" name="LoginID" onclick="javascript:getUserTemplate()"', $SelectedType="", $all=0, $noFirst=0);	
	}
}elseif ($type == "GetFormDetailsList") {
	$sql = "SELECT RlsNo,ptfsub.RecordID,ptfsub.BatchID,ptfsum.UserID,TemplateID FROM INTRANET_PA_T_FRMSUB ptfsub
				INNER JOIN INTRANET_PA_T_FRMSUM ptfsum ON ptfsub.RecordID=ptfsum.RecordID
				WHERE CycleID='$cycleID' AND TemplateID='$currTemplateID' AND UserID='".$currUserID."' AND ptfsub.IsActive=1";				
	$basicInfo = $connection->returnArray($sql);
	foreach ($basicInfo as $bi) {
		getSdfAvg($connection, $bi['TemplateID'], $bi["UserID"], $bi["RecordID"], $cycleID, $bi["RlsNo"], $bi["BatchID"]);
	}
	//$headerName = array($Lang['Appraisal']['Report']['TeacherName']);
	/* Clear unwanted Sections */
//	$headerAdded = false;
//	$teacherPerformanceCopy = $teacherPerformance;
//	foreach($teacherPerformanceCopy as $user=>$userContent){
//		foreach($userContent["Section"] as $secKey=>$secContent){
//			foreach($secContent["SubSection"] as $subKey=>$subContent){
//				if(empty($subContent["QuestionData"])){			
//					unset($teacherPerformance[$user]["Section"][$secKey]["SubSection"][$subKey]);					
//				}else{
//					if(!$headerAdded){
//						array_push($headerName, $subContent["SubSectionName"]);
//					}
//				}
//			}
//			if(empty($teacherPerformance[$user]["Section"][$secKey]["SubSection"])){
//				unset($teacherPerformance[$user]["Section"][$secKey]);
//			}
//			$headerAdded = true;
//		}
//	}
	//array_push($headerName, $Lang['Appraisal']['Report']['SumOfScore'], $Lang['Appraisal']['Report']['AverageMark']);
//	array_push($headerName, $Lang['Appraisal']['Report']['AverageMark']);
//	unset($teacherPerformanceCopy);
		
	/* Put data in table format */
//	$form .= "<table id='AverageTable' class='common_table_list_v30 tablesorter'>";
//	$form .= "<tr><thead>";
//	foreach($headerName as $header){
//		$form .= "<th>".$header."</th>";
//	}
//	$form .= "<thead></tr><tbody>";
//	foreach($teacherPerformance as $user=>$userContent){
//		$sum = 0;
//		$totalQ = 0;
//		$form .= "<tr>";
//		$form .= "<td>".$userContent['Name']."</td>";
//		foreach($userContent["Section"] as $secKey=>$secContent){
//			foreach($secContent["SubSection"] as $subKey=>$subContent){
//				$sectionSum = 0;
//				$sectionQno = 0;
//				foreach($subContent["QuestionData"] as $qdata){
//					$sectionSum = $sectionSum + $qdata['SectionSum'];
//					$sectionQno = $sectionQno + $qdata['SectionQNo'];
//				}
//				$form .= "<td>".number_format($sectionSum,2)."</td>";
//				$sum = $sum + $sectionSum;
//				$totalQ = $totalQ + $sectionQno; 
//			}			
//		}
//		$average = ($totalQ == 0)?0:round($sum/$totalQ, 2);
//		//$form .= "<td>".number_format($sum,2)."</td>";
//		$form .= "<td>".number_format($average,2)."</td>";
//		$form .= "</tr>";
//	}
//	$form .= "<tbody></table>";
$tmpPrincipal = array();
$tmpVicePrincipal = array();
$tmpOthers = array();
foreach($markerList as $header){
	$sql = "SELECT TitleChinese FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID='".$header."'";	
	$title = current($connection->returnVector($sql));
	if(strstr($title,'校長')){
		if($title=='校長'){
			array_push($tmpPrincipal, $header);
		}else{
			array_push($tmpVicePrincipal, $header);
		}
	}else{
		array_push($tmpOthers, $header);
	}
}
$markerList = array();
foreach($tmpPrincipal as $t){
	array_push($markerList, $t);
}
foreach($tmpVicePrincipal as $t){
	array_push($markerList, $t);
}
sort($tmpOthers, SORT_NUMERIC);
foreach($tmpOthers as $t){
	array_push($markerList, $t);
}
unset($tmpPrincipal);unset($tmpVicePrincipal);unset($tmpOthers);
$hasBorder = (count($teacherQuestionList)>0 && count($markerList)>0)?"":"style='border:none;'";
$form .= "<table id='AverageTable' class='common_table_list_v30' $hasBorder>";
	if(count($teacherQuestionList) > 0 && count($markerList) > 0){		
		$form .= "<tr><thead>";
		$form .= "<th style='width:20%'></th>";
		if (!$_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["isService"]) {
			foreach($markerList as $header){
				$form .= "<th style='width:".intval(70/count($markerList))."%'>".getUserName($connection,$header)."</th>";
			}
		}
		$form .= "<th style='width:10%'>".$Lang['Appraisal']['Report']['AverageMark']."</th>";
		$form .= "</thead></tr><tbody>";
		$sumPerMarker = array();
		$sumPerMarker['average']=0;
		$sumPerMarker['totalQ']=0;
		$totalQPerMarker = array();
		$countSectionQ = 0;
		$sectionAvgSum = 0;
		foreach($teacherQuestionList as $qid=>$content){
			if($qid==0)continue;
			$sum = 0;
			$totalQ = 0;
			if(isset($teacherQuestionList[$qid]['QCat']) && trim($teacherQuestionList[$qid]['QCat']) != ""){
				$form .= "<tr>";
				$form .= "<td>".$teacherQuestionList[$qid]['QCat']."</td>";	
				if (!$_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["isService"]) {
					$form .= "<td colspan='".(count($markerList)+1)."'></td>";	
				}else{
					$form .= "<td></td>";
				}			
				$form .= "</tr>";	
			}
			$form .= "<tr>";
			$form .= "<td>".getQuestionName($connection,$qid)."</td>";
			$hasNA = true; // assume if one given N/A for question, all give N/A; if someone not giving N/A, keep counting the total average
			foreach($markerList as $header){
				if(!isset($sumPerMarker[$header])){
					$sumPerMarker[$header] = array("sum"=>0, "isDouble"=>false);
				}
				if(!isset($totalQPerMarker[$header])){
					$totalQPerMarker[$header] = 0;
				}
				if(!isset($content[$header]['marks']) || $content[$header]['marks'] === 'N/A'){
					$content[$header]['marks'] = 'N/A';
				}else{
					$totalQ++;
					$totalQPerMarker[$header]++;
					$sumPerMarker[$header]["sum"] += $content[$header]['marks'];
					if($content[$header]['isDouble']==true){
						$sumPerMarker[$header]["isDouble"] = true;
						$sum += $content[$header]['marks']*2;
						$totalQ++;
					}else{
						$sum += $content[$header]['marks'];
					}
					$hasNA=false;
				}
				
				if (!$_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["isService"]) {
					$form .= "<td>".$content[$header]['marks']."</td>";
				}
			}
			$average = ($totalQ == 0)?0:round($sum/$totalQ, 2);
			if($hasNA==true){ 
				$form .= "<td>N/A</td>";
			}else{	
				$form .= "<td>".number_format($average,2)."</td>";
			}
			$form .= "</tr>";
			$sumPerMarker['average'] += $average; 
			if($hasNA==false){ 
				$sumPerMarker['totalQ']++;
			}
			$sectionAvgSum += $average;
			if($hasNA==false){ 
				$countSectionQ++;
			}
			if($teacherQuestionList[$qid]['lastQPerSection']===true){
				$form .= "<tr>";
				if (!$_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["isService"]) {
					$form .= "<td colspan='".(count($markerList)+1)."' style='text-align:right;'>".$Lang['Appraisal']['Report']['AverageMark']."</td>";
				}else{
					$form .= "<td style='text-align:right;'>".$Lang['Appraisal']['Report']['AverageMark']."</td>";
				}
				$average = ($countSectionQ == 0)?0:round($sectionAvgSum/$countSectionQ, 2);
				$form .= "<td>".number_format($average,2)."</td>";	
				$form .= "</tr>";				
				$countSectionQ = 0;
				$sectionAvgSum = 0;
			}
		}
		$form .= "<tr>";
		$form .= "<td style='border-top-style: groove;'>".$Lang['Appraisal']['Report']['SumOfScore']."</td>";
		if (!$_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["isService"]) {
		foreach($markerList as $header){
			$form .= "<td style='border-top-style: groove;'>".number_format($sumPerMarker[$header]["sum"],2)."</td>";	
		}
		}
		//$form .= "<td style='border-top-style: groove;'>".number_format($sumPerMarker['average'],2)."</td>";
		$form .= "<td style='border-top-style: groove;'></td>";	
		$form .= "</tr>";
		$form .= "<tr>";
		if (!$_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["isService"]) {
			$form .= "<td colspan='".(count($markerList)+1)."' style='text-align:right;'>".$Lang['Appraisal']['Report']['AverageMark']."</td>";
		}else{
			$form .= "<td style='text-align:right;'>".$Lang['Appraisal']['Report']['AverageMark']."</td>";
		}
		$average = ($sumPerMarker['totalQ'] == 0)?0:round($sumPerMarker['average']/$sumPerMarker['totalQ'], 2);
		$form .= "<td>".number_format($average,2)."</td>";	
		$form .= "</tr>";
		$form .= "</tbody>";
	}else if(count($teacherCommentList)==0 && count($markerList)==0){
		//$form .= $i_no_record_exists_msg;
		$form .= "<tr><td align=center style='border:none;'><br>".$i_no_record_exists_msg."<br><br></td></tr>\n";
	}
	$form .= "</table>";
	//debug_r($teacherPerformance);
	if (!$_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["isService"]) {
		foreach($teacherCommentList as $qid=>$qData){
			if(!empty($qData['data'])){
				if($qData['name']==""){
					$qData['name'] = $Lang['Appraisal']['ARFormIdvArrRmk'];
				}
				$form .= "<h3>".$qData['name']."</h3>";
				$form .= "<table class='common_table_list_v30' style=\"border:none;\">";
				$form .= "<tbody>";
				foreach($markerList as $header){
					$form .= "<tr>";
					$form .= "<td style=\"border:none; width:10%;\">".getUserName($connection,$header).":</td>";
					$form .= "<td style=\"border:none; width:90%;\">".$indexVar['libappraisal_ui']->GET_TEXTAREA($qid."_".$header, $taContents=$qData['data'][$header], $taCols=70, $taRows=5, $OnFocus="", $readonly=true, $other='style="width:100%;"', $class='', $taID='', $CommentMaxLength='')."</td>";
					$form .= "</tr>";
				}
				$form .= "</tbody></table>";
			}
		}	
	}
}elseif($type == 'GetTemplateList'){
	$userLogin = isset($_POST['userLogin'])?$_POST['userLogin']:"";
	$isMultiple = isset($_POST['isMultiple'])?$_POST['isMultiple']:false;
	$sql ="SELECT c.TemplateID, c.TemplateName FROM (SELECT DISTINCT psft.TemplateID,".Get_Lang_Selection("psft.FrmTitleChi","psft.FrmTitleEng")." as TemplateName
		FROM INTRANET_PA_C_FRMSEL pcfs 
		INNER JOIN INTRANET_PA_S_FRMTPL psft ON pcfs.TemplateID=psft.TemplateID
		WHERE pcfs.CycleID=$cycleID
		UNION
		SELECT psft.TemplateID, ".Get_Lang_Selection("psft.FrmTitleChi","psft.FrmTitleEng")." as TemplateName FROM INTRANET_PA_C_OBSSEL pcos
		INNER JOIN INTRANET_PA_S_FRMTPL psft ON pcos.TemplateID = psft.TemplateID
		WHERE pcos.CycleID='$cycleID') c ORDER BY c.TemplateID";
	$FormList = $connection->returnArray($sql);
	$forms = array();
	foreach($FormList as $ufc){	
		if(!in_array($ufc['TemplateID'], $forms)){
			$forms[] = $ufc['TemplateID'];	
		}
	}
	$sql = "SELECT DISTINCT frmsum.TemplateID FROM INTRANET_PA_T_FRMSUM frmsum  
			INNER JOIN INTRANET_PA_T_FRMSUB frmsub ON frmsum.RecordID = frmsub.RecordID
			INNER JOIN INTRANET_USER USR ON frmsum.UserID=USR.UserID	
			WHERE frmsum.CycleID='$cycleID' AND frmsub.IsLastSub=1";	
	if($userLogin != ""){
		$sql .= "  AND USR.UserLogin='".$userLogin."' ";
	}
	$templateIDList = $connection->returnVector($sql);		
	$filter2 = "";
	if($isMultiple){
		$cycleDefaultSetup = $indexVar['libappraisal']->getSchoolResultReportOverallSetup($cycleID);
		$filter2 .= "<select id='TemplateList' name='TemplateList' multiple size='8' onchange='javascript:getSectionSelection()'>";
	}else{
		$filter2 .= "<select id='TemplateID' name='templateID'>";
	}
	foreach($FormList as $ufc){
		if(in_array($ufc['TemplateID'], $templateIDList)){
			$isChecked = "";
			if(isset($cycleDefaultSetup) && !empty($cycleDefaultSetup)){
				if(in_array($ufc['TemplateID'], $cycleDefaultSetup['templateList'])){
					$isChecked = "selected";
				}
			}		
			$filter2 .= "	<option value='".$ufc['TemplateID']."' $isChecked>".$indexVar['libappraisal']->displayChinese($ufc['TemplateName'])."</option>";
		}
	}
	$filter2 .= "</select>";
	echo $filter2;
}elseif($type=='GetSectionList'){
	$templateList = isset($_POST['templateList'])?$_POST['templateList']:"";
	$selectedSectionList = isset($_POST['selectedSectionList'])?$_POST['selectedSectionList']:"";
	if($templateList==""){
		echo '';
		exit();
	}
	if($selectedSectionList==""){
		$cycleDefaultSetup = $indexVar['libappraisal']->getSchoolResultReportOverallSetup($cycleID);
		$selectedSectionList = (isset($cycleDefaultSetup['sectionList']))?$cycleDefaultSetup['sectionList']:"";
	}
	if(is_array($selectedSectionList)){
		$selectedSectionArr = $selectedSectionList;
	}else{
		$selectedSectionArr = explode(",",$selectedSectionList);
	}
	$sql = "SELECT frmsec.SecID, ".Get_Lang_Selection("frmtpl.FrmTitleChi","frmtpl.FrmTitleEng")." as FormName, ".Get_Lang_Selection("frmsec.SecCodChi","frmsec.SecCodEng")." as SecCode, ".Get_Lang_Selection("frmsec.SecTitleChi","frmsec.SecTitleEng")." as SecName 
			FROM INTRANET_PA_S_FRMSEC frmsec
			INNER JOIN INTRANET_PA_S_FRMTPL frmtpl ON frmsec.TemplateID = frmtpl.TemplateID 
			WHERE frmsec.TemplateID IN ('".implode("','",explode(",",$templateList))."') AND frmtpl.IsActive=1 AND ParentSecID IS NOT NULL";
	$sectionList = $connection->returnArray($sql);	
	
	$filter3 = "";
	$filter3 .= "<select id='SectionList' name='SectionList' multiple size='8'>";
	foreach($sectionList as $sec){
		$sectionName = $sec['FormName'];
		if($sec['SecCode']!="" || $sec['SecName']!=""){
			if($sec['SecCode']==""){
				$sectionName .= " - ".$sec['SecName'];
			}elseif($sec['SecName']==""){
				$sectionName .= " - ".$sec['SecCode'];
			}else{
				$sectionName .= " - ".$sec['SecCode'].": ".$sec['SecName'];
			}
		}
		if(in_array($sec['SecID'],$selectedSectionArr)){
			$filter3 .= "	<option value='".$sec['SecID']."' selected>".$sectionName."</option>";
		}else{
			$filter3 .= "	<option value='".$sec['SecID']."'>".$sectionName."</option>";
		}
	}
	$filter3 .= "</select>";
	echo $filter3;
}elseif($type=="GetOverallDetailsList"){
	$templateIDArr = explode(",",$_POST['templateList']);
	$specialSectionArr = explode(",",$_POST['sectionList']);
	
	// Save the setting first
	$setupResult = $indexVar['libappraisal']->setSchoolResultReportOverallSetup($cycleID, $templateIDArr, $specialSectionArr);
	
	$templateDataSet = array();
	$userDataSet = array();
	$specialSectionDisplayDataSet = array();
	foreach($templateIDArr as $templateID){
		$sql = "SELECT RlsNo,ptfsub.RecordID,ptfsub.BatchID,ptfsum.UserID,TemplateID FROM INTRANET_PA_T_FRMSUB ptfsub
					INNER JOIN INTRANET_PA_T_FRMSUM ptfsum ON ptfsub.RecordID=ptfsum.RecordID
					WHERE CycleID='$cycleID' AND TemplateID='$templateID' AND ptfsub.IsActive=1";				
		$basicInfo = $connection->returnArray($sql);
		foreach ($basicInfo as $bi) {
			getSdfAvg($connection, $bi['TemplateID'], $bi["UserID"], $bi["RecordID"], $cycleID, $bi["RlsNo"], $bi["BatchID"]);
		}
		$templateDataSet[$templateID] = $teacherPerformance;
		$teacherPerformance = array();
	}
	foreach($templateDataSet as $templateID=>$templateMarkingData){
		foreach($templateMarkingData as $userID=>$userTemplateData){
			if(!isset($userDataSet[$userID])){
				$userDataSet[$userID] = array();
			}
			if(!isset($specialSectionDisplayDataSet[$userID])){				
				$specialSectionDisplayDataSet[$userID] = array();
			}
			if(!isset($userDataSet[$userID][$templateID])){
				$userDataSet[$userID][$templateID] = array(
					"TotalMarks"=>0,
					"TotalQ"=>0
				);
			}
			
			foreach($userTemplateData['Section'] as $section){
				foreach($section['SubSection'] as $subsection){
					if(in_array($subsection['SubSectionID'],$specialSectionArr)){
						if(empty($subsection['QuestionData'])){
							$specialSectionDisplayDataSet[$userID][$subsection['SubSectionID']] = getSdfQuestionMarks($connection, $templateID, $userID, $subsection['SubSectionID'], $cycleID);
						}else{
							$totalQ = 0;
							$totalMarks = 0;
							$hasNA = false;
							foreach($subsection['QuestionData'] as $qData){
								$totalQ += $qData['SectionQNo'];
								$totalMarks += $qData['SectionSum'];
								if($qData['hasNA']){
									$hasNA = true;
								}
							}
							$specialSectionDisplayDataSet[$userID][$subsection['SubSectionID']] = ($totalQ==0)?(($hasNA==true)?'N/A':'--'):number_format(round($totalMarks/$totalQ,2),2);
						}
					}else{
						foreach($subsection['QuestionData'] as $qData){
							$userDataSet[$userID][$templateID]['TotalQ'] += $qData['SectionQNo'];
							$userDataSet[$userID][$templateID]['TotalMarks'] += $qData['SectionSum'];
							if($qData['hasNA']){
								$userDataSet[$userID][$templateID]['hasNA'] = true;
							}
						}
					}
				}
			}
			$userDataSet[$userID][$templateID]['Average'] = ($userDataSet[$userID][$templateID]['TotalQ']==0)?(($userDataSet[$userID][$templateID]['hasNA']==true)?'N/A':'--'):number_format(round($userDataSet[$userID][$templateID]['TotalMarks']/$userDataSet[$userID][$templateID]['TotalQ'],2),2);
		}
	}
	
	$form = "";
	$hasBorder = (count($userDataSet)>0)?"":"style='border:none;'";
	$form .= "<table id='AverageTable' class='tablesorter common_table_list_v30' $hasBorder>";
	
	//header builder
	$form .= "<thead><tr>";
	$form .= "<th style=\"width:15%\">".$Lang['Appraisal']['Report']['TeacherName']."</th>";
	foreach($templateIDArr as $templateID){
		$form .= "<th class=\"sorter-digit string-none\">".getTemplateName($connection,$templateID)."</th>";	
	}
	$form .= "<th class=\"sorter-digit\">".$Lang['Appraisal']['Report']['AverageMark']."</th>";
	foreach($specialSectionArr as $sectionID){
		$form .= "<th class=\"sorter-digit string-none\">".getSectionName($connection,$sectionID)."</th>";	
	}		
	$form .= "</tr></thead>";
	if(empty($userDataSet)){
		$form .= "<tr><td colspan='".(2+count($templateIDArr)+count($specialSectionArr))."'>".$Lang['Appraisal']['Message']['NoRecord']."</td></tr>";
	}else{
		$form .= "<tbody>";	
		foreach($userDataSet as $userID=>$userData){
			$formSum = 0;			
			$form .= "<tr>";
			$form .= "<td>".getUserName($connection,$userID)."</td>";
			$total = 0;	
			foreach($templateIDArr as $templateID){
				if(isset($userDataSet[$userID][$templateID])){
					$form .= "<td>".$userDataSet[$userID][$templateID]['Average']."</td>";	
					$formSum += $userDataSet[$userID][$templateID]['Average'];
					$total++;
				}else{
					$form .= "<td>--</td>";	
				}
			}
			$form .= "<td>".number_format(round($formSum/$total,2),2)."</td>";
			foreach($specialSectionArr as $sectionID){
				if(isset($specialSectionDisplayDataSet[$userID][$sectionID])){
					$form .= "<td>".$specialSectionDisplayDataSet[$userID][$sectionID]."</td>";
				}else{
					$form .= "<td>--</td>";
				}
			}		
			$form .= "</tr>";
		}		
		$form .= "</tbody>";
	}
	$form .= "</table>";
}

function getSdfAvg($connection, $templateID, $userID, $recordID, $cycleID, $rlsNo, $batchID) {
	$indexVar['libappraisal'] = new libappraisal();
	$indexVar['libappraisal_ui'] = new libappraisal_ui();
	global $intranet_session_language, $Lang, $teacherPerformance,$teacherQuestionList,$appraisalConfig;

	if ($userID == "" && $recordID == "" && $cycleID == "" && $rlsNo == "" && $batchID == "") {
		$sql = "SELECT TemplateID," . $indexVar['libappraisal']->getLangSQL("FrmTitleChi", "FrmTitleEng") . " as FormTitle," .
		$indexVar['libappraisal']->getLangSQL("FrmCodChi", "FrmCodEng") . " as FormCode," .
		$indexVar['libappraisal']->getLangSQL("HdrRefChi", "HdrRefEng") . " as HdrRef," .
		$indexVar['libappraisal']->getLangSQL("ObjChi", "ObjEng") . " as Obj," .
		$indexVar['libappraisal']->getLangSQL("DescrChi", "DescrEng") . " as Descr,
					AppTgtChi,AppTgtEng,NeedSubj,NeedClass
					FROM INTRANET_PA_S_FRMTPL WHERE TemplateID=" . IntegerSafe($templateID) . ";";
	} else {
		$sql = "SELECT iptf.RecordID,UserID,iptf.CycleID,CycleDescr,iptf.TemplateID,FormTitle,FormCode,HdrRef,Obj,Descr,AppRoleID,AppTgtChi,AppTgtEng,AcademicYearID,COALESCE(SubDate,'') as SubDate,
				EditPrdFr,EditPrdTo,CASE WHEN NOW()>EditPrdTo THEN '1' ELSE '0' END as OverdueDate,YearClassID,SubjectID,TeachLang,NeedSubj,NeedClass
				FROM(
					SELECT RecordID,CycleID,TemplateID,UserID FROM INTRANET_PA_T_FRMSUM WHERE RecordID=" . IntegerSafe($recordID) . "
				) as iptf
				INNER JOIN(
					SELECT TemplateID," . $indexVar['libappraisal']->getLangSQL("FrmTitleChi", "FrmTitleEng") . " as FormTitle," .
		$indexVar['libappraisal']->getLangSQL("FrmCodChi", "FrmCodEng") . " as FormCode," .
		$indexVar['libappraisal']->getLangSQL("HdrRefChi", "HdrRefEng") . " as HdrRef," .
		$indexVar['libappraisal']->getLangSQL("ObjChi", "ObjEng") . " as Obj," .
		$indexVar['libappraisal']->getLangSQL("DescrChi", "DescrEng") . " as Descr,
					AppTgtChi,AppTgtEng,NeedSubj,NeedClass
					FROM INTRANET_PA_S_FRMTPL
				) as ipsf ON iptf.TemplateID=ipsf.TemplateID
				LEFT JOIN(
					SELECT CycleID," . $indexVar['libappraisal']->getLangSQL("DescrChi", "DescrEng") . " as CycleDescr,AcademicYearID
					FROM INTRANET_PA_C_CYCLE
				) as ipcc ON iptf.CycleID=ipcc.CycleID
				LEFT JOIN(
					SELECT BatchID,CycleID,EditPrdFr,EditPrdTo,AppRoleID FROM INTRANET_PA_C_BATCH WHERE BatchID=" . IntegerSafe($batchID) . "
				) as ipcb ON ipcc.CycleID=ipcb.CycleID
				LEFT JOIN(
					SELECT RlsNo,RecordID,BatchID,SubDate FROM INTRANET_PA_T_FRMSUB
				) as iptfrmSub ON iptf.RecordID=iptfrmSub.RecordID AND ipcb.BatchID=iptfrmSub.BatchID
				LEFT JOIN(
					SELECT RlsNo,RecordID,BatchID,YearClassID,SubjectID,TeachLang FROM INTRANET_PA_T_SLF_HDR WHERE RecordID=" . IntegerSafe($recordID) . " AND BatchID=" . IntegerSafe($batchID) . "
				) as iptsh ON iptf.RecordID=iptsh.RecordID AND iptfrmSub.RlsNo=iptsh.RlsNo AND iptfrmSub.BatchID=iptsh.BatchID;";
		//echo $sql."<br/>";
	}

	$header = $connection->returnResultSet($sql);
	$templateID = $header[0]["TemplateID"];
	$academicYearID = $header[0]["AcademicYearID"];

	$tchNameSql = "SELECT EnglishName FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID='" . $userID . "'";
	$tchName = current($connection->returnVector($tchNameSql));
	$isSetBefore = false;
	if (isset ($teacherPerformance[$userID])) {
		$isSetBefore = true;
	} else {
		$teacherPerformance[$userID] = array (
			"Name" => $tchName,
			"TemplateID" => $templateID,
			"Section" => array ()
		);
	}
	//======================================================================== Section ========================================================================//
	$sql = "SELECT SecID,TemplateID,SecTitle,SecCod,SecDescr,DisplayOrder,PageBreakAfter
		FROM(
			SELECT SecID,TemplateID," . $indexVar['libappraisal']->getLangSQL("SecTitleChi", "SecTitleEng") . " as SecTitle," . $indexVar['libappraisal']->getLangSQL("SecCodChi", "SecCodEng") . " as SecCod," .
	$indexVar['libappraisal']->getLangSQL("SecDescrChi", "SecDescrEng") . " as SecDescr,DisplayOrder,PageBreakAfter FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=" . IntegerSafe($templateID) . " AND ParentSecID IS NULL
		) as ipsf ORDER BY DisplayOrder;
		";
	//echo $sql."<br/><br/>";
	$sectionHeader = $connection->returnResultSet($sql);

	// prepare the QID for that form
	$parSecID = $indexVar['libappraisal']->convertMultipleRowsIntoOneRow($sectionHeader, "SecID");
	$sql = "SELECT SecID,TemplateID,SecTitle,SecCod,SecDescr,DisplayOrder,ParentSecID
		FROM(
			SELECT SecID,TemplateID," . $indexVar['libappraisal']->getLangSQL("SecTitleChi", "SecTitleEng") . " as SecTitle," . $indexVar['libappraisal']->getLangSQL("SecCodChi", "SecCodEng") . " as SecCod," .
	$indexVar['libappraisal']->getLangSQL("SecDescrChi", "SecDescrEng") . " as SecDescr,DisplayOrder,ParentSecID FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=" . IntegerSafe($templateID) . " AND ParentSecID IS NOT NULL
			AND ParentSecID IN (" . $parSecID . ")
		) as ipsf";
	//echo $sql."<br/><br/>";
	$y = $connection->returnResultSet($sql);
	$subSecID = $indexVar['libappraisal']->convertMultipleRowsIntoOneRow($y, "SecID");
	$sql = "SELECT QCatID,SecID," . $indexVar['libappraisal']->getLangSQL("QCatCodChi", "QCatCodEng") . " as QCatCod," . $indexVar['libappraisal']->getLangSQL("DescrChi", "DescrEng") . " as QCatDescr," .
	"DisplayOrder as QCatDisplayOrder FROM INTRANET_PA_S_QCAT WHERE SecID IN (" . $subSecID . ")";
	//echo $sql."<br/><br/>";
	$y = $connection->returnResultSet($sql);
	$qCatID = $indexVar['libappraisal']->convertMultipleRowsIntoOneRow($y, "QCatID");
	$sql = "SELECT QID,QCatID FROM INTRANET_PA_S_QUES WHERE QCatID IN (" . $qCatID . ")";
	$y = $connection->returnResultSet($sql);
	$qID = $indexVar['libappraisal']->convertMultipleRowsIntoOneRow($y, "QID");
	// prepare the QID for that form

	for ($h = 0; $h < sizeof($sectionHeader); $h++) {
		$sectionHeaderTitle = "";
		if ($sectionHeader[$h]["SecCod"] != "" && $sectionHeader[$h]["SecTitle"] != "") {
			$sectionHeaderTitle = $sectionHeader[$h]["SecCod"] . "-" . $sectionHeader[$h]["SecTitle"];
		} else
			if ($sectionHeader[$h]["SecCod"] != "" && $sectionHeader[$h]["SecTitle"] == "") {
				$sectionHeaderTitle = $sectionHeader[$h]["SecCod"];
			} else
				if ($sectionHeader[$h]["SecCod"] == "" && $sectionHeader[$h]["SecTitle"] != "") {
					$sectionHeaderTitle = $sectionHeader[$h]["SecTitle"];
				}
		if (!isset ($teacherPerformance[$userID]['Section']['Section' . ($h +1)])) {
			$teacherPerformance[$userID]['Section']['Section' . ($h +1)] = array (
				"SectionName" => $sectionHeaderTitle,
				"SubSection" => array ()
			);
		}

		//======================================================================== Sub Section ========================================================================//
		$sql = "SELECT SecID,TemplateID,SecTitle,SecCod,SecDescr,DisplayOrder,ParentSecID
							FROM(
								SELECT SecID,TemplateID," . $indexVar['libappraisal']->getLangSQL("SecTitleChi", "SecTitleEng") . " as SecTitle," . $indexVar['libappraisal']->getLangSQL("SecCodChi", "SecCodEng") . " as SecCod," .
		$indexVar['libappraisal']->getLangSQL("SecDescrChi", "SecDescrEng") . " as SecDescr,DisplayOrder,ParentSecID FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=" . IntegerSafe($templateID) . " AND ParentSecID IS NOT NULL
								AND ParentSecID=" . IntegerSafe($sectionHeader[$h]["SecID"]) . "
							) as ipsf
							ORDER BY DisplayOrder;
							";
		//echo $sql."<br/><br/>";
		$a = $connection->returnResultSet($sql);
		for ($i = 0; $i < sizeof($a); $i++) {
			$subSectionName = "";
			if ($a[$i]["SecCod"] != "" && $a[$i]["SecTitle"] != "") {
				$subSectionName = $a[$i]["SecCod"] . " " . $a[$i]["SecTitle"];
			} else
				if ($a[$i]["SecCod"] != "" && $a[$i]["SecTitle"] == "") {
					$subSectionName = $a[$i]["SecCod"];
				} else
					if ($a[$i]["SecCod"] == "" && $a[$i]["SecTitle"] != "") {
						$subSectionName = $a[$i]["SecTitle"];
					}

			if (!isset ($teacherPerformance[$userID]['Section']['Section' . ($h +1)]['SubSection']['SubSection' . ($i +1)])) {
				$teacherPerformance[$userID]['Section']['Section' . ($h +1)]['SubSection']['SubSection' . ($i +1)] = array (
					"SubSectionName" => $subSectionName,
					"SubSectionID"=> $a[$i]["SecID"],
					"QuestionData" => array ()
				);
			}

			$sql = "SELECT QCatID,SecID," . $indexVar['libappraisal']->getLangSQL("QCatCodChi", "QCatCodEng") . " as QCatCod," . $indexVar['libappraisal']->getLangSQL("DescrChi", "DescrEng") . " as QCatDescr,QuesDispMode," .
			"DisplayOrder as QCatDisplayOrder FROM INTRANET_PA_S_QCAT WHERE SecID=" . $a[$i]["SecID"] . " ORDER BY DisplayOrder;";
			//echo $sql."<br/><br/>";
			$b = $connection->returnResultSet($sql);
			for ($j = 0; $j < sizeof($b); $j++) {
				if ($b[$j]['QuesDispMode'] == 1 && empty($teacherPerformance[$userID]['Section']['Section'.($h+1)]['SubSection']['SubSection'.($i+1)]["QuestionData"][$j])) {
					//======================================================================== Question Category ========================================================================//
					$returnQueData = getSDFSectionContentMarks("", $a[$i]["SecID"], $b[$j]["QCatID"], $recordID, $rlsNo, $batchID, $connection);
					if ($returnQueData) {
						$teacherPerformance[$userID]['Section']['Section'.($h+1)]['SubSection']['SubSection'.($i+1)]["QuestionData"][$j] = $returnQueData;
					}

				}else if($b[$j]['QuesDispMode'] == 0){
					$returnQueData = getSDFCommentList("", $a[$i]["SecID"], $b[$j]["QCatID"], $recordID, $rlsNo, $batchID, $connection);
					if ($returnQueData) {
						$teacherPerformance[$userID]['Section']['Section'.($h+1)]['SubSection']['SubSection'.($i+1)]["QuestionData"][$j] = $returnQueData;
					}
				}
			}
		}
	}
}

function getSDFSectionContentMarks($prefix, $secID, $qCatID, $recordID, $rlsNo, $batchID, $connection) {
	$indexVar['libappraisal'] = new libappraisal();
	$indexVar['libappraisal_ui'] = new libappraisal_ui();
	global $intranet_session_language, $Lang, $sys_custom,$teacherQuestionList,$markerList,$teacherCommentList;
	$questionScore = array();
	$sql = "SELECT SecID,ipsqcat.QCatID,QID,QuesDispMode,ipsqcat.QCatCodDes,QCodDes,ipsqcat.QCatDescr,Descr,QuesDisplayOrder,IsMC,IsScore,HasRmk,Rmk,IsTxtAns,TxtBoxNote,InputType,RmkLabel,IsAvgMark,AvgDec,IsVerAvgMark,VerAvgDec,ScoreIsAvgMark,ScoreAvgDec
					FROM(
						SELECT QCatID,SecID,".$indexVar['libappraisal']->getLangSQL("QCatCodChi", "QCatCodEng")." as QCatCodDes,".$indexVar['libappraisal']->getLangSQL("DescrChi", "DescrEng")." as QCatDescr,DisplayOrder as CatDisplayOrder,QuesDispMode,IsAvgMark,AvgDec,IsVerAvgMark,VerAvgDec FROM INTRANET_PA_S_QCAT WHERE SecID=" . $secID . " AND QCatID=" . $qCatID . "
					) as ipsqcat
					LEFT JOIN(
						SELECT QID,QCatID," . $indexVar['libappraisal']->getLangSQL("QCodChi", "QCodEng") . " as QCodDes," . $indexVar['libappraisal']->getLangSQL("DescrChi", "DescrEng") . " as Descr,DisplayOrder,InputType,
								IsMC,IsScore,IsTxtAns,TxtBoxNote,HasRmk," . $indexVar['libappraisal']->getLangSQL("RmkLabelChi", "RmkLabelEng") . " as Rmk,IsAvgMark as ScoreIsAvgMark,AvgDec as ScoreAvgDec,
						DisplayOrder as QuesDisplayOrder," . $indexVar['libappraisal']->getLangSQL("RmkLabelChi", "RmkLabelEng") . " as RmkLabel
						FROM INTRANET_PA_S_QUES WHERE QCatID=" . $qCatID . "
					) as ipsques ON ipsqcat.QCatID=ipsques.QCatID
					ORDER BY QuesDisplayOrder;";
	$a = $connection->returnResultSet($sql);
	//echo ($sql)."<br/><br/>";
	$quesDispMode = $a[0]["QuesDispMode"];
	
	// get Double Count Filler
	$doubleCountFillerList = $indexVar['libappraisal']->getReportDoubleCountPersonList(false);
	
	// check any content in the first content
	$hasContent = false;
	$sql = "SELECT SUM(LENGTH(" . $indexVar['libappraisal']->getLangSQL("QCodChi", "QCodEng") . ")) as content FROM  INTRANET_PA_S_QUES WHERE QCatID=" . $qCatID . ";";
	$x = $connection->returnResultSet($sql);
	if ($x[0]["content"] > 0) {
		$hasContent = true;
	}
	if ($quesDispMode == 1) {
		$verValue = 0;
		$noOfQ = 0;
		$hasNA = false;
		for ($i = 0; $i < sizeof($a); $i++) {
			$value = 0;

			$intSecID = $a[$i]["SecID"];
			$intQusCodID = $i +1;
			$qID = $a[$i]["QID"];

			$sql = "SELECT UserID,TemplateID  FROM(
								SELECT RlsNo,RecordID,BatchID FROM INTRANET_PA_T_FRMSUB WHERE RlsNo=" . $rlsNo . " AND BatchID=" . $batchID . " AND RecordID=" . $recordID . "
							) as iptfsub
							INNER JOIN(
								SELECT RecordID,UserID,TemplateID FROM INTRANET_PA_T_FRMSUM 
							) as iptf ON iptfsub.RecordID=iptf.RecordID;";
			//echo $sql."<br/><br/>";
			$x = $connection->returnResultSet($sql);
			$userID = $x[0]["UserID"];
			$templateID = $x[0]["TemplateID"];
			$sql = "SELECT RecordID FROM INTRANET_PA_T_FRMSUM WHERE UserID=" . $userID . " AND TemplateID=" . $templateID . ";";
			//echo $sql."<br/><br/>";
			$x = $connection->returnResultSet($sql);
			$recordIDArr = $indexVar['libappraisal']->convertMultipleRowsIntoOneRow($x, "RecordID");
			//echo $recordIDArr."<br/><br/>";				
			$sql = "SELECT ipstdtl.RecordID,MAX(ipstdtl.BatchID),QID,QAnsID,ipstdtl.MarkID,Remark,DateAns,ipsqlks.Score,ipsqlks.MarkCodChi,ipsqlks.MarkCodEng,frmsub.FillerID
								FROM(
									SELECT RecordID,BatchID,QID,QAnsID,MarkID,Remark,Score,DATE_FORMAT(DateAns,'%Y-%m-%d') as DateAns FROM INTRANET_PA_T_SLF_DTL 
									WHERE RlsNo=" . $rlsNo . " AND QID=" . $qID . " AND RecordID IN (" . $recordIDArr . ")
								) as ipstdtl
								INNER JOIN(
									SELECT MarkID,Score,MarkCodChi,MarkCodEng FROM INTRANET_PA_S_QLKS 
								) as ipsqlks ON ipstdtl.MarkID=ipsqlks.MarkID
								INNER JOIN (
									SELECT RlsNo,RecordID,BatchID,FillerID FROM INTRANET_PA_T_FRMSUB WHERE RlsNo=" . $rlsNo . " AND RecordID IN (" . $recordIDArr . ") AND SubDate IS NOT NULL
								) frmsub ON ipstdtl.RecordID=frmsub.RecordID GROUP BY ipstdtl.RecordID";
			//echo $sql."<br/><br/>";;
			$b = $connection->returnResultSet($sql);
			$noOfReply = sizeof($b);
			$lastQID = 0;
			if($i==0 && ($a[$i]['QCatCodDes']!="" || $a[$i]['QCatDescr'] !="")){
				$teacherQuestionList[$b[0]['QID']]['QCat']=$a[$i]['QCatCodDes']." ".$a[$i]['QCatDescr'];
			}
			for ($j = 0; $j < sizeof($b); $j++) {
				if(!isset($teacherQuestionList[$b[$j]['QID']])){
					$teacherQuestionList[$b[$j]['QID']] = array();
				}
				if ($b[$j]['MarkCodChi'] == 'N/A' || $b[$j]['MarkCodEng'] == 'N/A') {
					$teacherQuestionList[$b[$j]['QID']][$b[$j]['FillerID']] = array("marks"=>'N/A',"isDouble"=>false);
					$value = $value;
					$noOfReply--;
					$hasNA = true;
				} else {
					if(in_array($b[$j]["FillerID"],$doubleCountFillerList)){
						$teacherQuestionList[$b[$j]['QID']][$b[$j]['FillerID']] = array("marks"=>intval($b[$j]["Score"]),"isDouble"=>true);
						$value = $value + (intval($b[$j]["Score"])*2);
						$noOfReply++;	
					}else{
						$teacherQuestionList[$b[$j]['QID']][$b[$j]['FillerID']] = array("marks"=>intval($b[$j]["Score"]),"isDouble"=>false);
						$value = $value + intval($b[$j]["Score"]);						
					}				
				}
				if(!in_array($b[$j]['FillerID'],$markerList)){
					array_push($markerList,$b[$j]['FillerID']);
				}
				$lastQID = $b[$j]['QID'];
			}
			if($i==count($a)-1){
				$teacherQuestionList[$lastQID]['lastQPerSection']=true;
			}
			$average = ($noOfReply == 0) ? 0 : round($value / ($noOfReply), $a[0]["AvgDec"]);
			$verValue = (sizeof($b) == 0) ? $verValue : $verValue + $average;
			$noOfQ = ($noOfReply == 0) ? $noOfQ: $noOfQ+1;			
		}
		$questionScore = array("SectionSum"=>$verValue,"SectionQNo"=>$noOfQ,"hasNA"=>$hasNA);
	}
	
	return $questionScore;
}

function getSDFCommentList($prefix, $secID, $qCatID, $recordID, $rlsNo, $batchID, $connection){
	$indexVar['libappraisal'] = new libappraisal();
	$indexVar['libappraisal_ui'] = new libappraisal_ui();
	global $intranet_session_language, $Lang, $sys_custom,$teacherQuestionList,$markerList,$teacherCommentList;
	$questionScore = array();
	$sql = "SELECT SecID,ipsqcat.QCatID,QID,QuesDispMode,QCodDes,Descr,QuesDisplayOrder,IsMC,IsScore,HasRmk,Rmk,IsTxtAns,TxtBoxNote,InputType,RmkLabel,IsAvgMark,AvgDec,IsVerAvgMark,VerAvgDec,ScoreIsAvgMark,ScoreAvgDec
					FROM(
						SELECT QCatID,SecID,QCatCodChi,QCatCodEng,DescrChi,DescrEng,DisplayOrder as CatDisplayOrder,QuesDispMode,IsAvgMark,AvgDec,IsVerAvgMark,VerAvgDec FROM INTRANET_PA_S_QCAT WHERE SecID=" . $secID . " AND QCatID=" . $qCatID . "
					) as ipsqcat
					LEFT JOIN(
						SELECT QID,QCatID," . $indexVar['libappraisal']->getLangSQL("QCodChi", "QCodEng") . " as QCodDes," . $indexVar['libappraisal']->getLangSQL("DescrChi", "DescrEng") . " as Descr,DisplayOrder,InputType,
								IsMC,IsScore,IsTxtAns,TxtBoxNote,HasRmk," . $indexVar['libappraisal']->getLangSQL("RmkLabelChi", "RmkLabelEng") . " as Rmk,IsAvgMark as ScoreIsAvgMark,AvgDec as ScoreAvgDec,
						DisplayOrder as QuesDisplayOrder," . $indexVar['libappraisal']->getLangSQL("RmkLabelChi", "RmkLabelEng") . " as RmkLabel
						FROM INTRANET_PA_S_QUES WHERE QCatID=" . $qCatID . "
					) as ipsques ON ipsqcat.QCatID=ipsques.QCatID
					ORDER BY QuesDisplayOrder;";
	$a = $connection->returnResultSet($sql);
	//echo ($sql)."<br/><br/>";
	$quesDispMode = $a[0]["QuesDispMode"];

	// get Double Count Filler
	$doubleCountFillerList = $indexVar['libappraisal']->getReportDoubleCountPersonList(false);
	
	// check any content in the first content
	$hasContent = false;
	$sql = "SELECT SUM(LENGTH(" . $indexVar['libappraisal']->getLangSQL("QCodChi", "QCodEng") . ")) as content FROM  INTRANET_PA_S_QUES WHERE QCatID=" . $qCatID . ";";
	$x = $connection->returnResultSet($sql);
	if ($x[0]["content"] > 0) {
		$hasContent = true;
	}
	for ($i = 0; $i < sizeof($a); $i++) {
		if($a[$i]["InputType"]==5 || $a[$i]["InputType"]==0){
			$intSecID = $a[$i]["SecID"];
			$intQusCodID = $i +1;
			$qID = $a[$i]["QID"];

			$sql = "SELECT UserID,TemplateID  FROM(
								SELECT RlsNo,RecordID,BatchID FROM INTRANET_PA_T_FRMSUB WHERE RlsNo=" . $rlsNo . " AND BatchID=" . $batchID . " AND RecordID=" . $recordID . "
							) as iptfsub
							INNER JOIN(
								SELECT RecordID,UserID,TemplateID FROM INTRANET_PA_T_FRMSUM 
							) as iptf ON iptfsub.RecordID=iptf.RecordID;";
			//echo $sql."<br/><br/>";
			$x = $connection->returnResultSet($sql);
			$userID = $x[0]["UserID"];
			$templateID = $x[0]["TemplateID"];
			$sql = "SELECT RecordID FROM INTRANET_PA_T_FRMSUM WHERE UserID=" . $userID . " AND TemplateID=" . $templateID . ";";
			//echo $sql."<br/><br/>";
			$x = $connection->returnResultSet($sql);
			$recordIDArr = $indexVar['libappraisal']->convertMultipleRowsIntoOneRow($x, "RecordID");
			//echo $recordIDArr."<br/><br/>";				
			$sql = "SELECT ipstdtl.RecordID,MAX(ipstdtl.BatchID),QID,QAnsID,ipstdtl.MarkID,MAX(Remark) as remark_ans,DateAns,frmsub.FillerID
								FROM(
									SELECT RecordID,BatchID,QID,QAnsID,MarkID,Remark,Score,DATE_FORMAT(DateAns,'%Y-%m-%d') as DateAns FROM INTRANET_PA_T_SLF_DTL 
									WHERE RlsNo=" . $rlsNo . " AND QID=" . $qID . " AND RecordID IN (" . $recordIDArr . ")
								) as ipstdtl
								INNER JOIN (
									SELECT RlsNo,RecordID,BatchID,FillerID FROM INTRANET_PA_T_FRMSUB WHERE RlsNo=" . $rlsNo . " AND RecordID IN (" . $recordIDArr . ")
								) frmsub ON ipstdtl.RecordID=frmsub.RecordID GROUP BY ipstdtl.RecordID";
			//echo $sql."<br/><br/>";;
			$b = $connection->returnResultSet($sql);
			if(!isset($teacherCommentList[$qID])){
				$teacherCommentList[$qID] = array("name"=>$a[$i]['Descr'],"data"=>array());
			}
			for ($j = 0; $j < sizeof($b); $j++) {
				$teacherCommentList[$qID]["data"][$b[$j]['FillerID']] = $b[$j]['remark_ans'];
			}	
		}
	}
}

function getSdfQuestionMarks($connection, $templateID, $userID, $secID, $cycleID){
	$indexVar['libappraisal'] = new libappraisal();
	$indexVar['libappraisal_ui'] = new libappraisal_ui();
	global $intranet_session_language, $Lang, $sys_custom,$teacherQuestionList,$markerList,$teacherCommentList;
	$questionScore = 0;
	$questionScoreBase = 0;
	$doubleCountFillerList = $indexVar['libappraisal']->getReportDoubleCountPersonList(false);
	
	$sql = "SELECT Score,frmsub.FillerID FROM INTRANET_PA_T_SLF_DTL sdf
			INNER JOIN INTRANET_PA_S_QUES sdfq ON sdf.QID = sdfq.QID
			INNER JOIN INTRANET_PA_S_QCAT sdfqcat ON sdfq.QCatID = sdfqcat.QCatID
			INNER JOIN INTRANET_PA_T_FRMSUB frmsub ON sdf.RecordID = frmsub.RecordID AND sdf.RlsNo = frmsub.RlsNo AND sdf.BatchID = frmsub.BatchID
			INNER JOIN INTRANET_PA_T_FRMSUM frmsum ON sdf.RecordID = frmsum.RecordID
			WHERE sdfqcat.SecID = '".$secID."' AND frmsum.CycleID='".$cycleID."' AND frmsum.UserID='".$userID."' AND frmsum.TemplateID='".$templateID."'";
	$scoreList = $connection->returnArray($sql);
	
	foreach($scoreList as $sc){
		if(in_array($sc["FillerID"],$doubleCountFillerList)){
			$questionScore += ($sc['Score']*2);
			$questionScoreBase++;
		}else{				
			$questionScore += $sc['Score'];
		}
		$questionScoreBase++;
	}
	
	$result = ($questionScoreBase==0)?0:number_format(round($questionScore/$questionScoreBase,2),2);
	return $result;
}

function getUserName($connection,$userID){
	global $appraisalConfig,$indexVar;
	$tchNameSql = "SELECT ".$indexVar['libappraisal']->getLangSQL("ChineseName", "EnglishName") . " as user_name FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID='" . $userID . "'";
	$tchName = current($connection->returnVector($tchNameSql));
	return $tchName;
}

function getQuestionName($connection,$QID){
	global $indexVar;
	$qNameSql = "SELECT CONCAT(".$indexVar['libappraisal']->getLangSQL("QCodChi", "QCodEng").", ' ', ".$indexVar['libappraisal']->getLangSQL("DescrChi", "DescrEng").") as QCode  FROM INTRANET_PA_S_QUES WHERE QID='".$QID."'";
	$qName = current($connection->returnVector($qNameSql));
	return $qName;
}

function getTemplateName($connection,$templateID){
	global $indexVar;
	$tplNameSql = "SELECT CONCAT(".$indexVar['libappraisal']->getLangSQL("FrmCodChi", "FrmCodEng").", ' ', ".$indexVar['libappraisal']->getLangSQL("FrmTitleChi", "FrmTitleEng").") as QCode  FROM INTRANET_PA_S_FRMTPL WHERE TemplateID='".$templateID."'";
	$tplName = current($connection->returnVector($tplNameSql));
	return $tplName;
}

function getSectionName($connection,$secID){
	global $indexVar;
	$secNameSql = "SELECT CONCAT(".$indexVar['libappraisal']->getLangSQL("SecCodChi", "SecCodEng").", ' ', ".$indexVar['libappraisal']->getLangSQL("SecTitleChi", "SecTitleEng").") as SecCode  FROM INTRANET_PA_S_FRMSEC WHERE SecID='".$secID."'";
	$secName = current($connection->returnVector($secNameSql));
	return $secName;
}
?>
<style>
.tablesorter-default .header,
.tablesorter-default .tablesorter-header {
	padding: 4px 20px 4px 4px;
	cursor: pointer;
	background-image: url(data:image/gif;base64,R0lGODlhFQAJAIAAAP///////yH5BAEAAAEALAAAAAAVAAkAAAIXjI+AywnaYnhUMoqt3gZXPmVg94yJVQAAOw==);
	background-position: center right;
	background-repeat: no-repeat;
}
.tablesorter-default .headerSortUp,
.tablesorter-default .tablesorter-headerSortUp,
.tablesorter-default .tablesorter-headerAsc {
	background-image: url(data:image/gif;base64,R0lGODlhFQAEAIAAAP///////yH5BAEAAAEALAAAAAAVAAQAAAINjI8Bya2wnINUMopZAQA7);
	color: #fff;
}
.tablesorter-default .headerSortDown,
.tablesorter-default .tablesorter-headerSortDown,
.tablesorter-default .tablesorter-headerDesc {
	color: #fff;
	background-image: url(data:image/gif;base64,R0lGODlhFQAEAIAAAP///////yH5BAEAAAEALAAAAAAVAAQAAAINjB+gC+jP2ptn0WskLQA7);
}
</style>
<script type="text/javascript" src="/templates/jquery/tablesorter/jquery.tablesorter.min.js"></script>
<script>
$(function(){
	//$("#AverageTable").tablesorter();
});
</script>
<?php
echo $form;
?>