<?php

// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================


$lexport = new libexporttext();

// build header array
$headerAry = array();

$dataAry = array();
$connection = new libgeneralsettings();
$cycleID=$_GET["cycleID"];
$targetQues="Your proposed school allocations";

$sql = "
SELECT u.UserLogin, ".$indexVar['libappraisal']->getLangSQL("u.ChineseName","u.EnglishName")." as Name, iptsd.Remark FROM INTRANET_PA_T_SLF_DTL iptsd
INNER JOIN INTRANET_PA_S_QUES ipsq ON iptsd.QID=ipsq.QID
INNER JOIN INTRANET_PA_S_QCAT ipqc ON ipsq.QCatID=ipqc.QCatID
INNER JOIN INTRANET_PA_T_FRMSUM iptfs ON iptsd.RecordID=iptfs.RecordID
INNER JOIN INTRANET_USER u ON iptfs.UserID=u.UserID
WHERE ipqc.DescrChi LIKE '%".$targetQues."%' OR ipqc.DescrEng LIKE '%".$targetQues."%' AND iptfs.CycleID='".IntegerSafe($cycleID)."' AND iptfs.IsActive=1 ORDER BY u.UserID ";

//echo $sql."<br/><br/>";
$a=$connection->returnResultSet($sql);
$index = 0;
$headerAry[0]=$Lang['Appraisal']['CycleExportLeaveChi']['TeacherID'];
$headerAry[1]=$Lang['Appraisal']['Report']['TeacherName'];
$headerAry[2]=$targetQues;

for($i=0;$i<sizeof($a);$i++){
	$dataAry[$i][0]=$a[$i]["UserLogin"];
	$dataAry[$i][1]=$a[$i]["Name"];
	$dataAry[$i][2]=$a[$i]["Remark"];
}
$exportContent = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=1);

$fileName = $targetQues.'.csv';
$lexport->EXPORT_FILE($fileName, $exportContent);

?>