<?php
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."includes/appraisal/gen_form_class.php");
require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");
include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');

intranet_opendb();
$db = new libgeneralsettings();
$fm = new libfilesystem();
$genform = new genform();
$archived = false;
$json = new JSON_obj();

$currDir = getcwd();
chdir($intranet_root."/file/temp");
foreach (glob("export_appraisal_report*.txt") as $filename) {
    unlink($filename);
}
foreach (glob("export_appraisal_report*.zip") as $filename) {
    unlink($filename);
}
chdir($currDir);

function recordProgress($currentProgress, $total){
    global $intranet_root, $json;
    $percent = intval($currentProgress/$total * 100);
    $arr_content['percent'] = $percent;
    $arr_content['message'] = $percent . "%";
    file_put_contents($intranet_root."/file/temp/export_appraisal_report" . session_id() . ".txt", $json->encode($arr_content));
    sleep(1);
}

$indexVar['libappraisal'] = new libappraisal();
$indexVar['libappraisal_ui'] = new libappraisal_ui();
global $intranet_session_language, $Lang;

# Temp Assign memory of this page
@ini_set("memory_limit","1024M");

$cycleID = $_POST['cycleID'];

//Select apprasiseeID and NameList
$selectedAppraiseeID = $_POST['selectedAppraisee'];
$selectedAppraiseeID = explode(",",$selectedAppraiseeID);
$selectedAppraiseeName = array();
$selectedAppraiseeLogin = array();

$totalProgress = (sizeof($selectedAppraiseeID)*3)+1;
$currentProgress = 0;
recordProgress($currentProgress, $totalProgress);


foreach($selectedAppraiseeID as $AppraiseeID){
    $sql = "SELECT ".$indexVar['libappraisal']->getLangSQL("ChineseName","EnglishName")." as AppraiseeName, UserLogin FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID = '".$AppraiseeID."'";
    $resultselectedAppraiseeName = $db->returnArray($sql);
    //array_push($selectedAppraiseeName,$resultselectedAppraiseeName[0]['AppraiseeName']);
    $selectedAppraiseeName[$AppraiseeID] = $resultselectedAppraiseeName[0]['AppraiseeName'];
    $selectedAppraiseeLogin[$AppraiseeID] = $resultselectedAppraiseeName[0]['UserLogin'];
}


//Start Pack ZIP File
$mainSql = "";
$folderToDownload = $intranet_root."/file/temp/export_appraisal_report/";
$fm->folder_remove_recursive($folderToDownload);
$fm->createFolder($folderToDownload);

$x = 0;
//Layer 1 (Selected Appraisee)

foreach($selectedAppraiseeID as $AppraiseeID){
    recordProgress(++$currentProgress, $totalProgress);
    $appraiseeName = iconv("utf8", "big5", $selectedAppraiseeName[$AppraiseeID]);
    $appraiseeLogin = $selectedAppraiseeLogin[$AppraiseeID];
    $dest_layer1 = $folderToDownload.$appraiseeName;
    $mkdir_result = mkdir($dest_layer1,0777);
    
    //Call PDF file generate function on Layer 1
    $_GET['cycleID'] = $cycleID;
    $_GET['inclObs'] = 1;
    $_GET['loginID'] = $appraiseeLogin;
    $_GET['displayName'] = 1;
    $_GET['displayDate'] = 1;
    $_GET['displayCover'] = 1;
    $_GET['displayMode'] = 's';
    $_GET['outputAsZip'] = 1;
    $_GET['outputDOC'] = 0;
    $_GET['slang'] = 'b5';
    $_GET['outputAsZipPath'] = ($mkdir_result)?$dest_layer1:$folderToDownload;
    include(dirname(__FILE__).'/../personal_result/view_print.php');
    
    // Prepare data for attachment download
    $sql = "SELECT ".$indexVar['libappraisal']->getLangSQL("frmtpl.FrmTitleChi","frmtpl.FrmTitleEng")." as FrmTitle
                ,".$indexVar['libappraisal']->getLangSQL("frmtpl.FrmCodChi","frmtpl.FrmCodEng")." as FrmCod
                ,frmsum.UserID,frmsub.FillerID,frmsub.ObsID,frmdtl.RecordID,frmsub.BatchID,frmsum.CycleID,frmsum.TemplateID,frmques.QID,frmdtl.TxtAns as FileName
                FROM INTRANET_PA_T_FRMSUM frmsum
                INNER JOIN INTRANET_PA_T_FRMSUB frmsub ON frmsum.RecordID = frmsub.RecordID
				INNER JOIN INTRANET_PA_C_BATCH batch ON frmsub.BatchID = batch.BatchID
                INNER JOIN INTRANET_PA_S_FRMTPL frmtpl ON frmsum.TemplateID = frmtpl.TemplateID
                INNER JOIN INTRANET_PA_S_FRMSEC frmsec ON frmtpl.TemplateID = frmsec.TemplateID AND frmsec.ParentSecID IS NOT NULL
                INNER JOIN INTRANET_PA_S_QCAT frmqcat ON frmsec.SecID = frmqcat.SecID AND frmqcat.QuesDispMode=0
                INNER JOIN INTRANET_PA_S_QUES frmques ON frmqcat.QCatID=frmques.QCatID AND frmques.InputType=7
                INNER JOIN INTRANET_PA_T_SLF_DTL frmdtl ON frmques.QID = frmdtl.QID AND frmdtl.RecordID= frmsum.RecordID AND frmdtl.RlsNo=frmsub.RlsNo AND frmdtl.BatchID = frmsub.BatchID
                WHERE frmsum.UserID='".$AppraiseeID."' AND frmsum.CycleID='".$cycleID."' AND frmsub.SubDate IS NOT NULL ";
    
    //        $sql .= " AND (frmtpl.IsObs=0 OR frmtpl.IsObs=1)  ";
    $sql .= " GROUP BY frmsub.BatchID, frmques.QID ";
    $sql .= " ORDER BY frmtpl.TemplateID ASC,frmsum.RecordID ASC, batch.BatchOrder ASC ";
    //         echo $sql."<br/>";
    $results = $db->returnResultSet($sql);
    //Build two type of array 1.AppraiserList(ID,Name) 2.AttchmentList(FormTitle,AttachmentPath)
    $appraiserList = array();
    $appraiserList['ID'] = array();
    $appraiserList['Name'] = array();
    
    $attachmentList['FormTitle'] = array();
    $attachmentList['AttachmentPath'] = array();
    $attachmentList['AttachmentFileName'] = array();
    
    $z = 0;
    
    recordProgress(++$currentProgress, $totalProgress);
    
    //Pick Layer folder
    foreach ($results as $result) {
        //Store appraiser ID list
        $fileNameCompose = explode("|",$result['FileName']);
        if(!empty($result['ObsID'])){
            $appraiserID = $result['FillerID'].",".$result['ObsID'];
            if(!in_array($fileNameCompose[1], explode(",",$appraiserID))){
                continue;
            }
        }else{
            $appraiserID = $result['FillerID'];
            if($fileNameCompose[1] != $appraiserID){
                continue;
            }
        }
        array_push($appraiserList['ID'],$appraiserID);
        
        array_push($attachmentList['FormTitle'],$result['FrmTitle']);
        $attachmentPath = $intranet_root."/file/appraisal/sdf_file/".$result['BatchID']."/".$result['FileName'];
        array_push($attachmentList['AttachmentPath'],$attachmentPath);
        array_push($attachmentList['AttachmentFileName'],$result['FileName']);
        
        // Layer 2 (attachment folder with form title)
        if(empty($result['FrmCod'])){
            //                 $attachmentList['FormTitle'][$z] = ($z+1) . "_" .$attachmentList['FormTitle'][$z];
            $attachmentList['FormTitle'][$z] = $attachmentList['FormTitle'][$z];
        }else if(empty($attachmentList['FormTitle'][$z])){
            //                 $attachmentList['FormTitle'][$z] = ($z+1) . "_" .$result['FrmCod'];
            $attachmentList['FormTitle'][$z] = $result['FrmCod'];
        }else{
            //             $attachmentList['FormTitle'][$z] = ($z+1) . "_" . $attachmentList['FormTitle'][$z]."_".$result['FrmCod'];
            $attachmentList['FormTitle'][$z] = $attachmentList['FormTitle'][$z]."_".$result['FrmCod'];
            
        }
        $formTitle = iconv("utf8", "big5", $attachmentList['FormTitle'][$z]);
        $dest_layer2 = $dest_layer1."/".$formTitle;
        if(!file_exists($dest_layer2)){
            mkdir($dest_layer2,0777);
        }
        
        // Layer 3 (Appraiser folder)
        $appraiserList['ID'][$z] = explode(",",$appraiserList['ID'][$z]);
        $appraiserList['ID'][$z] = array_unique($appraiserList['ID'][$z]);
        $appraiserNameStringList = "";
        foreach ($appraiserList['ID'][$z] as $id) {
            $sql = "SELECT ".$indexVar['libappraisal']->getLangSQL("ChineseName","EnglishName")." as AppraiserName FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID = '".$id."'";
            $appraiserName = $db->returnResultSet($sql);
            
            if ($appraiserNameStringList == "") {
                $appraiserNameStringList .= $appraiserName[0]['AppraiserName'];
            }else{
                $appraiserNameStringList .= ",".$appraiserName[0]['AppraiserName'];
            }
            $appraiserName = iconv("utf8", "big5", $appraiserName[0]['AppraiserName']);
            $dest_layer3 = $dest_layer2."/".$appraiserName."/";
            if(!file_exists($dest_layer3)){
                mkdir($dest_layer3,0777);
            }
        }
        $appraiserList['Name'][$z] = $appraiserNameStringList;
        
        //Layer 4 (attachment of each appraiser)
        //Match $attachmentList to $appraiserList
        $appraiserList['Name'][$z] = explode(",",$appraiserList['Name'][$z]);
        $appraiserList['Name'][$z] = array_unique($appraiserList['Name'][$z]);
        foreach ($appraiserList['Name'][$z] as $targetName) {
            $targetName = iconv("utf8", "big5", $targetName);
            $dest_layer4 = $dest_layer2."/".$targetName."/";
            //folder_copy_recursive($attachmentList['AttachmentPath'][$z],$dest_layer4);
            if(file_exists($attachmentList['AttachmentPath'][$z]) && $attachmentList['AttachmentFileName'][$z]!=""){
                $nameComposeArray = explode("|",$attachmentList['AttachmentFileName'][$z]);
                copy($attachmentList['AttachmentPath'][$z],$dest_layer4.$nameComposeArray[2]);
            }
        }
        $z++;
    }
    $x++;
    recordProgress(++$currentProgress, $totalProgress);
}


//Pack the Zip file;
$ZipFileName = "export_appraisal_report".date('Ymdhis').".zip";
$export_directory = $intranet_root."/file/temp/";
$fm->file_zip('export_appraisal_report', $ZipFileName, $export_directory);
recordProgress(++$currentProgress, $totalProgress);

echo '/home/download_attachment.php?target_e='.getEncryptedText($export_directory.$ZipFileName);

function folder_copy_recursive($src, $dest){
    if(file_exists($src)){
        $dir = opendir($src);
        @mkdir($dest);
        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($src . '/' . $file) ) {
                    folder_copy_recursive($src . '/' . $file,$dest . '/' . $file);
                }
                else {
                    $file_exploded = explode("|",$file);
                    copy($src . '/' . $file,$dest . '/' . $file_exploded[2]);
                    //                         if (!copy($src . '/' . $file,$dest . '/' . $file)) {
                    //                             echo "File cannot be copied! \n";
                    //                         }
                    //                         else {
                    //                             echo "File has been copied!: ".$src . $file,$dest . $file." ";
                    //                         }
                }
            }
        }
        closedir($dir);
    }
}
//     intranet_closedb();
?>