<?php

// ============================== Related Tables ==============================

// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once ($PATH_WRT_ROOT . "includes/libgeneralsettings.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "includes/appraisal/gen_form_class.php");
include_once ($PATH_WRT_ROOT . 'includes/json.php');

# Page Title
// ============================== Includes files/libraries ==============================

// ============================== Includes files/libraries ==============================
$connection = new libgeneralsettings();

$type=($_GET["Type"]=="")?$_POST["Type"]:$_GET["Type"];

if($type=="GetLoginUserSelection"){
	$cycleID = $_POST['CycleID'];
	if($cycleID==""){
		echo '';
		die();
	}else{
		$name_field = getNameFieldByLang("USR.");
		$sql = "SELECT Distinct USR.UserID, 
				CASE 
					WHEN au.UserLogin IS NOT NULL then CONCAT({$name_field}, ' (".$Lang['Appraisal']['Deleted'].")') 
					ELSE {$name_field} 
				END  as name FROM INTRANET_PA_T_FRMSUM frmsum 
				INNER JOIN ".$appraisalConfig['INTRANET_USER']." USR ON frmsum.UserID=USR.UserID				
				INNER JOIN INTRANET_PA_T_FRMSUB frmsub ON frmsum.RecordID = frmsub.RecordID
				LEFT JOIN INTRANET_ARCHIVE_USER au ON USR.UserID = au.UserID	
				WHERE frmsum.CycleID='".$cycleID."' AND frmsub.IsLastSub=1";
		$resultList=$connection->returnResultSet($sql);
		$selList = array();
		foreach($resultList as $r){
			array_push($selList, array("id"=>$r['UserID'], "name"=>$r['name']));
		}
		echo  getSelectByAssoArray($indexVar['libappraisal']->cnvGeneralArrToSelect($selList, "id","name","name"), $selectionTags='id="appraiseelist_content" size="10" multiple="multiple"', $SelectedType="multiple", $all=0, $noFirst=1);	
	}
}
?>