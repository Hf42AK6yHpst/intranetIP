<?php 
// ============================== Related Tables ==============================

// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');

# Page Title
// ============================== Includes files/libraries ==============================

// ============================== Includes files/libraries ==============================
$connection = new libgeneralsettings();
$type=$_POST["Type"];


if($type=="CheckLeave"){
	$arr = array();
	$jsonObj = new JSON_obj();
	$cycleID = $_POST["cycleID"];
	$sql = "SELECT UserID FROM INTRANET_PA_C_LEAVE WHERE CycleID=".$cycleID;
	$a=$connection->returnResultSet($sql);
	//echo $sql;
	if(sizeof($a)>0){
		//contain records to download
		$arr[] = array('result' => "0", 'msg' => "");
	}
	else{
		// no records to download
		$arr[] = array('result' => "1", 'msg' => $Lang['Appraisal']['Message']['NoRecord']);
	}
	echo $jsonObj->encode($arr);
}
else if($type=="CheckAbsence"){
	$arr = array();
	$jsonObj = new JSON_obj();
	$cycleID = $_POST["cycleID"];
	$sql = "SELECT UserID FROM INTRANET_PA_C_ABSENCE WHERE CycleID=".$cycleID;
	$a=$connection->returnResultSet($sql);
	//echo $sql;
	if(sizeof($a)>0){
		//contain records to download
		$arr[] = array('result' => "0", 'msg' => "");
	}
	else{
		// no records to download
		$arr[] = array('result' => "1", 'msg' => $Lang['Appraisal']['Message']['NoRecord']);
	}
	echo $jsonObj->encode($arr);
}
else if($type=="CheckSLLate"){
	$arr = array();
	$jsonObj = new JSON_obj();
	$cycleID = $_POST["cycleID"];
	$sql = "SELECT UserID FROM INTRANET_PA_C_ATTENDANCE WHERE CycleID=".$cycleID;
	$a=$connection->returnResultSet($sql);
	//echo $sql;
	if(sizeof($a)>0){
		//contain records to download
		$arr[] = array('result' => "0", 'msg' => "");
	}
	else{
		// no records to download
		$arr[] = array('result' => "1", 'msg' => $Lang['Appraisal']['Message']['NoRecord']);
	}
	echo $jsonObj->encode($arr);
}else if($type=="CheckAttendance"){
	$arr = array();
	$jsonObj = new JSON_obj();
	$cycleID = $_POST["cycleID"];
	$sql = "SELECT data.AttID FROM INTRANET_PA_T_FRMSUM frmsum
			INNER JOIN INTRANET_PA_T_FRMSUB frmsub ON frmsum.RecordID = frmsub.RecordID
			INNER JOIN INTRANET_PA_S_ATTEDNACE_DATA data ON frmsub.RlsNo = data.RlsNo AND frmsub.RecordID=data.RecordID AND frmsub.BatchID=data.BatchID
			WHERE CycleID=".$cycleID;
	$a=$connection->returnResultSet($sql);
	//echo $sql;
	if(sizeof($a)>0){
		//contain records to download
		$arr[] = array('result' => "0", 'msg' => "");
	}
	else{
		// no records to download
		$arr[] = array('result' => "1", 'msg' => $Lang['Appraisal']['Message']['NoRecord']);
	}
	echo $jsonObj->encode($arr);
}


?>