<?php
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

$MODULE_OBJ['title'] = $Lang['Appraisal']['CycleTemplate']['Leave']. " (". $Lang['Appraisal']['CycleExportLeaveChi']['LeaveType'].")";
$linterface = new interface_html("popup.html");
$connection = new libgeneralsettings();
$linterface->LAYOUT_START();

$sql = "SELECT ".$indexVar['libappraisal']->getLangSQL("TAReasonNameChi", "TAReasonNameEng")." as reason FROM INTRANET_PA_S_ATTEDNACE_REASON";
$reasonList = $connection->returnVector($sql);

$display .= "<table width='96%' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
$display .= "<tr>";
$display .= "<td class='tabletop tabletopnolink' align='left'>#</td>";
$display .= "<td class='tabletop tabletopnolink' align='left'>".$Lang['Appraisal']['CycleExportLeaveChi']['LeaveType']."</td>";
$display .= "</tr>";

for ($i=0; $i<sizeof($reasonList); $i++)
{     
     $display .= "<tr class='tablerow". ( $i%2 + 1 )."'>";
     $display .= "<td class='tabletext'>".($i+1)."</td>";
     $display .= "<td class='tabletext'>$reasonList[$i]</td>";
     $display .= "</tr>\n";
}

$display .= "<tr>"; 
$display .= "<td height='1' colspan='2' class='dotline'><img src='$image_path/$LAYOUT_SKIN/10x10.gif' width='10' height='1'></td>";
$display .= "</tr>";
$display .= "<tr>"; 
$display .= "<td colspan='2' align='center'>". $linterface->GET_ACTION_BTN($button_close, "button","self.close();") ."</td>";
$display .= "</tr>";
$display .= "</table>\n";

?>
<br />
<?=$display?>
<br />
<?
$linterface->LAYOUT_STOP();
?>