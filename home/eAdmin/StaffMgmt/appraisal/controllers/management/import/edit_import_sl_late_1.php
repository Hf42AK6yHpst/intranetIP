<?php 
// ============================== Related Tables ==============================
//
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
/*$curTab = 'sample1';
$TAGS_OBJ[] = array('Page Sample 1', 'javascript: void(0);', $curTab=='sample1');
$TAGS_OBJ[] = array('Page Sample 2', '#', $curTab=='sample2');
$TAGS_OBJ[] = array('Page Sample 3', 'javascript: void(0);', $curTab=='sample3');
*/
# Page Title
$curTab="slLateSub";
$TAGS_OBJ[] = array($Lang['Appraisal']['CycleTemplate']['Leave'], 'javascript: goLeave(0);', $curTab=='leave');
$TAGS_OBJ[] = array($Lang['Appraisal']['CycleTemplate']['AbsLateSub'], 'javascript: goAbsLateSub(0);', $curTab=='absLateSub');
$TAGS_OBJ[] = array($Lang['Appraisal']['CycleTemplate']['Attendance'], 'javascript: goAttendance(0);', $curTab=='attendance');
$TAGS_OBJ[] = array($Lang['Appraisal']['SickLeaveAndLateRecords'], 'javascript: goSLLateSub(0);', $curTab=='slLateSub');
if($plugin['eAppraisal_settings']['LnTReport']){
    $TAGS_OBJ[] = array($Lang['Appraisal']['LNT']['LearningAndTeaching'], 'javascript: goLntSub(0)', $curTab=='lnt');
}

$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
$cycleID = $_GET["cycleID"];


$connection = new libgeneralsettings();
//======================================================================== Header ========================================================================//
### navigation
$navigationAry[] = array($Lang['Appraisal']['ImportInformation']);
$navigationAry[] = array($Lang['Appraisal']['BtnImportSLLateSub']);
$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);
### step box
$stepAry = array();
$stepAry[] = $Lang['Appraisal']['CycleLeave']['Step1'];
$stepAry[] = $Lang['Appraisal']['CycleLeave']['Step2'];
$stepAry[] = $Lang['Appraisal']['CycleLeave']['Step3'];
$htmlAry['customizedImportStepTbl'] = $indexVar['libappraisal_ui']->GET_IMPORT_STEPS($CurrStep=1, $stepAry);
//======================================================================== Content ========================================================================//
$x = "<table id=\"msgTable\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" style=\"display:none\">";
$x .= "<tr>";
$x .= "<td align=\"right\" colspan=\"2\">";
$x .= "<table border=\"0\" cellpadding=\"3\" cellspacing=\"0\" class=\"systemmsg\">";
$x .= "<tbody>";
$x .= "<tr>";
$x .= "<td nowrap=\"nowrap\">";
$x .= "<span id=\"errMsg\" style=\"display:none\"><font color=\"red\">".$Lang['Appraisal']['Report']['InvalidFormat']."</font></span>";
$x .= "</td>";
$x .= "</tr>";
$x .= "</tbody>";
$x .= "</table>";
$x .= "</td>";
$x .= "</tr>";
$x .= "</table>";
$htmlAry['msg'] = $x;

$sql="SELECT CycleID,DescrChi,DescrEng
	FROM(
		SELECT c.CycleID, IF(c.DescrChi IS NULL OR c.DescrChi='', ay.YearNameB5, c.DescrChi) as DescrChi, IF(c.DescrEng IS NULL OR c.DescrEng ='', ay.YearNameEN, c.DescrEng) as DescrEng 
		FROM INTRANET_PA_C_CYCLE c
		INNER JOIN ACADEMIC_YEAR ay ON c.AcademicYearID=ay.AcademicYearID
	) as ipsf";
//echo $sql."<br/><br/>";
$cycleList=$connection->returnResultSet($sql);

$sql="SELECT CycleID FROM INTRANET_PA_C_LEAVE WHERE CycleID=".$cycleID.";";
$leaveList=$connection->returnResultSet($sql);
$sql="SELECT CycleID FROM INTRANET_PA_C_ABSENCE WHERE CycleID=".$cycleID.";";
$absenceList=$connection->returnResultSet($sql);

//$cycleDescr=$a[0]["AppDescr"];
$x = "<table class=\"form_table_v30\">";
$x .= "<tr><td class=\"field_title\">".$indexVar['libappraisal_ui']->RequiredSymbol()."<b>".$Lang['Appraisal']['CycleTemplate']['CurrTemplate']."</b></td>";
//$x .= "<td><a id='templateTitleID' class=\"tablelink\" href=\"javascript:goBackCycle(".$cycleID.")\">".$cycleDescr."</a></td>";
$x .= "<td>".getSelectByAssoArray($indexVar['libappraisal']->cnvGeneralArrToSelect($cycleList, "CycleID","DescrChi","DescrEng"), $selectionTags='id="ddlCycleID" name="ddlCycleID"', $SelectedType="", $all=0, $noFirst=0);
$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('CycleIDEmptyWarnDiv', $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv');
$x .= "</td>";
$x .= "</tr>";
$x .= "<tr>";
$x .= "<td class=\"field_title\">".$indexVar['libappraisal_ui']->RequiredSymbol()."<span>".$Lang['Appraisal']['CycleTemplate']['InfoFile']."</span>"."<span style=\"color:#999999\">".$Lang['Appraisal']['CycleTemplate']['CSVFile']."</span>";
$x .= "</td>";
$x .= "<td><input type=\"file\" name=\"ImportLeave\" id=\"ImportLeave\" class=\"file\"></td>";
$x .= "</tr>";
/*
$x .= "<tr>";
$x .= "<td class=\"field_title\"><span>".$Lang['Appraisal']['CycleTemplate']['TemplateFile']."</span></td>";
$x .= "<td><a id='exportTemplate' class=\"tablelink\" href=\"javascript:goExportLeave()\">".$Lang['Appraisal']['CycleExportTemplate']."</a></td>";
$x .= "</tr>";
*/
$x .= "<tr>";
$x .= "<td class=\"field_title\">".$Lang['Appraisal']['CycleTemplate']['DownloadAttendance']."</td>";
$x .= "<td>";
$x .= "<span id=\"dlFile2\"></span>";
$x .= "</td>";
$x .= "</tr>";
$x .= "<tr>";
$x .= "<td class=\"field_title\"><span>".$Lang['Appraisal']['CycleTemplate']['LeaveInfoRow']."</span></td>";
$x .= "<td>";
$x .= "<span>".sprintf($Lang['Appraisal']['CycleTemplate']['LeaveInfoRow1'],"<span style=\"color:#FF0000\">*</span>")."<br/></span>";
//$x .= "<span>".sprintf($Lang['Appraisal']['CycleTemplate']['LeaveInfoRow2'],"<span style=\"color:#999999\">".$Lang['Appraisal']['CycleTemplate']['LeaveInfoRmk1']."</span>")."<br/></span>";
//$x .= "<span>".sprintf($Lang['Appraisal']['CycleTemplate']['LeaveInfoRow3'],"<span style=\"color:#999999\">".$Lang['Appraisal']['CycleTemplate']['LeaveInfoRmk2']."</span>")."<br/></span>";
//$x .= "<span>".sprintf($Lang['Appraisal']['CycleTemplate']['LeaveInfoRow4'],"<span style=\"color:#999999\">".$Lang['Appraisal']['CycleTemplate']['LeaveInfoRmk2']."</span>")."<br/></span>";
//$x .= "<span>".sprintf($Lang['Appraisal']['CycleTemplate']['LeaveInfoRow5'],"<span style=\"color:#999999\">".$Lang['Appraisal']['CycleTemplate']['LeaveInfoRmk2']."</span>")."<br/></span>";
//$x .= "<span>".$Lang['Appraisal']['CycleTemplate']['LeaveInfoRow6']."<br/></span>";		
$x .= "<span>".$Lang['Appraisal']['CycleTemplate']['Column'].' B: '.$Lang['Appraisal']['SickLeaveRecord']." - ".$Lang['Appraisal']['LastAcademicYear']."<br/></span>";		
$x .= "<span>".$Lang['Appraisal']['CycleTemplate']['Column'].' C: '.$Lang['Appraisal']['SickLeaveRecord']." - ".$Lang['Appraisal']['CurrentAcademicYearTilMar']."<br/></span>";		
$x .= "<span>".$Lang['Appraisal']['CycleTemplate']['Column'].' D: '.$Lang['Appraisal']['LatenessRecord']." - ".$Lang['Appraisal']['LastAcademicYear']."<br/></span>";		
$x .= "<span>".$Lang['Appraisal']['CycleTemplate']['Column'].' E: '.$Lang['Appraisal']['LatenessRecord']." - ".$Lang['Appraisal']['CurrentAcademicYearTilMar']."<br/></span>";		
$x .= "<span>".$Lang['Appraisal']['CycleTemplate']['Column'].' F: '.$Lang['Appraisal']['IronMan']."<br/></span>";	
$x .= "<span>".$Lang['Appraisal']['CycleTemplate']['Column'].' G: '.$Lang['Appraisal']['EarlyBird']."<br/></span>";	
$x .= "</td>";
$x .= "</tr>";
$x .= "<tr>";
$x .= "<td class=\"field_title\">".$Lang['Appraisal']['CycleTemplate']['LastUploadLeaveCSV']."</td>";
$x .= "<td>";
$x .= "<span id=\"dlFile\"></span>";
$x .= "</td>";
$x .= "</tr>";
$x .= "</table>";
$x .="<span style=\"color:#999999\">".sprintf($Lang['Appraisal']['CycleLeave']['Remark'],"<span style=\"color:#FF0000\">*</span>")."</span><br/><br/>";
$x .= "<input type=\"hidden\" id=\"CycleID\" name=\"CycleID\" value=".$cycleID.">"."\r\n";

$htmlAry['contentTbl'] = $x;
// ============================== Define Button ==============================
$htmlAry['submitBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnImportLeave'], "button", "goSubmit()", 'submitBtn');
//$htmlAry['resetBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnReset'], "button", "goReset()", 'resetBtn');
//$htmlAry['backBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack('".$cycleID."')", 'backBtn');
// ============================== Define Button ==============================










?>