<?php
@ini_set("memory_limit","1024M");
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================

$cycleID=$_GET["cycleID"];
$lexport = new libexporttext();
// build content data array
$dataAry = array();
$connection = new libgeneralsettings();
$StaffAttend3 = new libstaffattend3();

$sql = "Select MIN(ayt.TermStart) as start, MAX(ayt.TermEnd) as end, ay.Sequence From INTRANET_PA_C_CYCLE c
INNER JOIN ACADEMIC_YEAR ay ON c.AcademicYearID=ay.AcademicYearID
INNER JOIN ACADEMIC_YEAR_TERM ayt ON ay.AcademicYearID = ayt.AcademicYearID
WHERE c.CycleID='".$cycleID."' GROUP BY ayt.AcademicYearID";
$thisYearData = current($connection->returnArray($sql));

$sql = "SELECT MIN(ayt.TermStart) as start, MAX(ayt.TermEnd) as end From ACADEMIC_YEAR ay 
INNER JOIN ACADEMIC_YEAR_TERM ayt ON ay.AcademicYearID = ayt.AcademicYearID
WHERE ay.Sequence='".($thisYearData['Sequence']+1)."' GROUP BY ayt.AcademicYearID";
$lastYearData = current($connection->returnArray($sql));

$StaffInfoList = $StaffAttend3->Get_Staff_Info();
$staffData = array();
for($i=0;$i<sizeof($StaffInfoList);$i++){
	$data = array();
	$isNewTeacher = false;
	$sql = "SELECT MIN(PeriodStart) as Start FROM CARD_STAFF_ATTENDANCE3_WORKING_PERIOD WHERE UserID='".$StaffInfoList[$i]['UserID']."' GROUP BY UserID;";
	$userWorkingPeriodStartDate = current($connection->returnVector($sql));
	if($userWorkingPeriodStartDate > $lastYearData['end']){ // This year entry teacher
		$isNewTeacher = true;
	}
	
	$sql = "SELECT UserLogin FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID='".$StaffInfoList[$i]['UserID']."'";
	
	$data['UserLogin'] = current($connection->returnVector($sql));
	$data['SLLastYear'] = 0;
	$data['SLThisYear'] = 0;
	$data['LateLastYear'] = 0;
	$data['LateThisYear'] = 0;
	$Params['TargetStartDate'] = $lastYearData['start'];
	$Params['TargetEndDate'] = $lastYearData['end'];
	$Params['Waived'] = false;
	$Params["Status"] = "";
	if($isNewTeacher==true){
		$data['SLLastYear'] = "N.A.";
		$data['LateLastYear'] = "N.A.";
	}else{
		$LastYearLeaveDataList = $StaffAttend3->Get_LeaveAbsence_Report_Data($Params, $StaffInfoList[$i]['UserID'], $card_log_table_names);
		
		foreach($LastYearLeaveDataList as $ld){
			if(in_array($ld['ReasonID'], explode(",",$sys_custom['eAppraisal']['SLLateRecords_eAttendance_SL_LIST']))){
				$data['SLLastYear'] += $ld['DutyCount'];
			}
		}
		$sql = "select SUM(p.ProfileCountFor) as total
					from 
						CARD_STAFF_ATTENDANCE2_PROFILE p 
						INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp 
					 	ON wp.UserID=p.StaffID 
					 		AND p.RecordDate BETWEEN wp.PeriodStart AND wp.PeriodEnd where 
							p.RecordDate Between '".$Params['TargetStartDate']."' and '".$Params['TargetEndDate']."' 
							and 
							p.RecordType in (2) AND wp.UserID='".$StaffInfoList[$i]['UserID']."'";
 		$data['LateLastYear'] = floatval(current($StaffAttend3->returnVector($sql)));
	}
  	
	
	$Params['TargetStartDate'] = $thisYearData['start'];
	$Params['TargetEndDate'] = date('Y-03-31 23:59:59', strtotime($thisYearData['end']));
	
	$ThisYearLeaveDataList = $StaffAttend3->Get_LeaveAbsence_Report_Data($Params, $StaffInfoList[$i]['UserID'], $card_log_table_names);
	
	foreach($ThisYearLeaveDataList as $td){
		if(in_array($td['ReasonID'], explode(",",$sys_custom['eAppraisal']['SLLateRecords_eAttendance_SL_LIST']))){
			$data['SLThisYear'] += floatval($td['DutyCount']);
		}
	}
	$sql = "select SUM(p.ProfileCountFor) as total
					from 
						CARD_STAFF_ATTENDANCE2_PROFILE p 
						INNER JOIN CARD_STAFF_ATTENDANCE3_WORKING_PERIOD as wp 
					 	ON wp.UserID=p.StaffID 
					 		AND p.RecordDate BETWEEN wp.PeriodStart AND wp.PeriodEnd where 
							p.RecordDate Between '".$Params['TargetStartDate']."' and '".$Params['TargetEndDate']."' 
							and 
							p.RecordType in (2) AND wp.UserID='".$StaffInfoList[$i]['UserID']."'";
 	$data['LateThisYear'] = floatval(current($StaffAttend3->returnVector($sql)));	
	$staffData[$StaffInfoList[$i]['UserID']] = $data;
}
$currentLang = $_SESSION['intranet_session_language'];
include_once($PATH_WRT_ROOT."/lang/appraisal_lang.en.php");
$dataAry[0][0]=$Lang['Appraisal']['CycleExportLeaveEng']['TeacherID'];
$dataAry[0][1]=$Lang['Appraisal']['SickLeaveRecord']." - ".$Lang['Appraisal']['LastAcademicYear'];
$dataAry[0][2]=$Lang['Appraisal']['SickLeaveRecord']." - ".$Lang['Appraisal']['CurrentAcademicYearTilMar'];
$dataAry[0][3]=$Lang['Appraisal']['LatenessRecord']." - ".$Lang['Appraisal']['LastAcademicYear'];
$dataAry[0][4]=$Lang['Appraisal']['LatenessRecord']." - ".$Lang['Appraisal']['CurrentAcademicYearTilMar'];
$dataAry[0][5]=$Lang['Appraisal']['IronMan'];
$dataAry[0][6]=$Lang['Appraisal']['EarlyBird'];
include_once($PATH_WRT_ROOT."/lang/appraisal_lang.b5.php");
$dataAry[1][0]=$Lang['Appraisal']['CycleExportLeaveChi']['TeacherID'];
$dataAry[1][1]=$Lang['Appraisal']['SickLeaveRecord']." - ".$Lang['Appraisal']['LastAcademicYear'];
$dataAry[1][2]=$Lang['Appraisal']['SickLeaveRecord']." - ".$Lang['Appraisal']['CurrentAcademicYearTilMar'];
$dataAry[1][3]=$Lang['Appraisal']['LatenessRecord']." - ".$Lang['Appraisal']['LastAcademicYear'];
$dataAry[1][4]=$Lang['Appraisal']['LatenessRecord']." - ".$Lang['Appraisal']['CurrentAcademicYearTilMar'];
$dataAry[1][5]=$Lang['Appraisal']['IronMan'];
$dataAry[1][6]=$Lang['Appraisal']['EarlyBird'];
if($currentLang!='b5'){
	include_once($PATH_WRT_ROOT."/lang/appraisal_lang.".$currentLang.".php");
}

$i = 0;
foreach($staffData as $sd){
	$dataAry[$i+2][0]=$sd["UserLogin"];
	$dataAry[$i+2][1]=($sd["SLLastYear"]==="")?0:$sd["SLLastYear"];
	$dataAry[$i+2][2]=($sd["SLThisYear"]==="")?0:$sd["SLThisYear"];
	$dataAry[$i+2][3]=($sd["LateLastYear"]==="")?0:$sd["LateLastYear"];
	$dataAry[$i+2][4]=($sd["LateThisYear"]==="")?0:$sd["LateThisYear"];
	$dataAry[$i+2][5]="";
	$dataAry[$i+2][6]="";
	$i++;
}

$exportContent = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=1);

$fileName = 'export_sl_late_record.csv';
$lexport->EXPORT_FILE($fileName, $exportContent, false, false, "UTF-8");