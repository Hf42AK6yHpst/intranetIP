<?php

// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================

$cycleID=$_GET["cycleID"];
$lexport = new libexporttext();
// build content data array
$dataAry = array();
$connection = new libgeneralsettings();

$sql="SELECT CycleID,ipcl.UserID,AbsLsnCnt,SubLsnCnt,LateCnt,EnglishName,ChineseName,UserLogin
	FROM(
		SELECT CycleID,UserID,AbsLsnCnt,SubLsnCnt,LateCnt 
		FROM INTRANET_PA_C_ABSENCE WHERE CycleID=".$cycleID."
	) as ipcl
	INNER JOIN(
		SELECT EnglishName,ChineseName,UserLogin,UserID
		FROM ".$appraisalConfig['INTRANET_USER']."
	) as iu ON ipcl.UserID=iu.UserID
	;";
//echo $sql."<br/><br/>";
$a=$connection->returnResultSet($sql);

$dataAry[0][0]=$Lang['Appraisal']['CycleExportAbsLateSubEng']['TeacherID'];
$dataAry[0][1]=$Lang['Appraisal']['CycleExportAbsLateSubEng']['NumAbsence'];
$dataAry[0][2]=$Lang['Appraisal']['CycleExportAbsLateSubEng']['NumSubstitution'];
$dataAry[0][3]=$Lang['Appraisal']['CycleExportAbsLateSubEng']['NumLateness'];

$dataAry[1][0]=$Lang['Appraisal']['CycleExportAbsLateSubChi']['TeacherID'];
$dataAry[1][1]=$Lang['Appraisal']['CycleExportAbsLateSubChi']['NumAbsence'];
$dataAry[1][2]=$Lang['Appraisal']['CycleExportAbsLateSubChi']['NumSubstitution'];
$dataAry[1][3]=$Lang['Appraisal']['CycleExportAbsLateSubChi']['NumLateness'];

for($i=0;$i<sizeof($a);$i++){
	$dataAry[$i+2][0]=$a[$i]["UserLogin"];
	$dataAry[$i+2][1]=$a[$i]["AbsLsnCnt"];
	$dataAry[$i+2][2]=$a[$i]["SubLsnCnt"];
	$dataAry[$i+2][3]=$a[$i]["LateCnt"];
}

$exportContent = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=1);

$fileName = 'export_absence_record.csv';
$lexport->EXPORT_FILE($fileName, $exportContent, false, false, "UTF-8");