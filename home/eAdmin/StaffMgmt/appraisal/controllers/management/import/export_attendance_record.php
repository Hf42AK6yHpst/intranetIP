<?php

// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================

$cycleID=$_GET["cycleID"];
$lexport = new libexporttext();
// build content data array
$dataAry = array();
$connection = new libgeneralsettings();

$sql = "SELECT TAColumnNameChi,TAColumnNameEng,TAColumnID FROM INTRANET_PA_S_ATTEDNACE_COLUMN ORDER BY DisplayOrder";
$colNameList = $connection->returnArray($sql);

$sql="SELECT u.UserLogin, reason.TAReasonNameChi, col.TAColumnNameChi, data.Days,data.Remarks,reason.TAReasonID,col.TAColumnID  FROM INTRANET_PA_T_FRMSUM frmsum
	INNER JOIN INTRANET_PA_T_FRMSUB frmsub ON frmsum.RecordID = frmsub.RecordID
	INNER JOIN INTRANET_PA_S_ATTEDNACE_DATA data ON frmsub.RlsNo = data.RlsNo AND frmsub.RecordID=data.RecordID AND frmsub.BatchID=data.BatchID
	INNER JOIN INTRANET_PA_S_ATTEDNACE_REASON reason ON data.TAReasonID = reason.TAReasonID
	INNER JOIN INTRANET_PA_S_ATTEDNACE_COLUMN col ON data.TAColumnID= col.TAColumnID
	INNER JOIN INTRANET_USER u ON frmsum.UserID=u.UserID
	WHERE CycleID='".$cycleID."' ORDER BY col.DisplayOrder";
//echo $sql."<br/><br/>";
$a=$connection->returnResultSet($sql);

$exportDataSet = array();
foreach($a as $data){
	if(!isset($exportDataSet[$data['UserLogin']."_".$data['TAReasonID']])){
		$exportDataSet[$data['UserLogin']."_".$data['TAReasonID']] = array(
			'UserLogin'=>$data['UserLogin'],
			'LeaveType'=>$data['TAReasonNameChi'],
			'Remark'=>$data['Remarks']
		);
	}
	$exportDataSet[$data['UserLogin']."_".$data['TAReasonID']][$data['TAColumnID']] = $data['Days'];
}

$idx = 2;
$dataAry[0][0]=$Lang['Appraisal']['CycleExportLeaveEng']['TeacherID'];
$dataAry[0][1]=$Lang['Appraisal']['CycleExportLeaveEng']['LeaveType'];
foreach($colNameList as $col){
	$dataAry[0][$idx]=$col['TAColumnNameEng'];
	$idx++;
}
$dataAry[0][$idx]=$Lang['Appraisal']['CycleExportLeaveEng']['Rmk'];

$idx = 2;
$dataAry[1][0]=$Lang['Appraisal']['CycleExportLeaveChi']['TeacherID'];
$dataAry[1][1]=$Lang['Appraisal']['CycleExportLeaveChi']['LeaveType'];
foreach($colNameList as $col){
	$dataAry[1][$idx]=$col['TAColumnNameChi'];
	$idx++;
}
$dataAry[1][$idx]=$Lang['Appraisal']['CycleExportLeaveChi']['Rmk'];

$i=0;
foreach($exportDataSet as $dataset){
	$idx = 2;
	$dataAry[$i+2][0]=$dataset["UserLogin"];
	$dataAry[$i+2][1]=$dataset["LeaveType"];
	foreach($colNameList as $col){
		$dataAry[$i+2][$idx]=$dataset[$col['TAColumnID']];
		$idx++;
	}
	$dataAry[$i+2][$idx]=$dataset["Remark"];
	$i++;
}
$exportContent = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=1);

$fileName = 'export_attendance_record.csv';
$lexport->EXPORT_FILE($fileName, $exportContent, false, false, "UTF-8");