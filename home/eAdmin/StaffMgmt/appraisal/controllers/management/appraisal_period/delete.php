<?php 
// ============================== Related Tables ==============================

// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');

# Page Title
// ============================== Includes files/libraries ==============================

// ============================== Includes files/libraries ==============================
$connection = new libgeneralsettings();
$json = new JSON_obj();
$saveSuccess = true;
$returnPath=$_POST["returnPath"];
if($_GET["type"] != ""){
	$type = $_GET["type"];
	if($type=="list"){
		$cycleIDArr= $_POST["cycleIDAry"];
		$returnPath = 'Location: ?task=management'.$appraisalConfig['taskSeparator'].'appraisal_period'.$appraisalConfig['taskSeparator'].'list&returnMsgKey=%s';
		for($i=0; $i<sizeof($cycleIDArr); $i++){
			$cycleID= $cycleIDArr[$i];
			$sql = "SELECT CycleID FROM INTRANET_PA_T_FRMSUM WHERE CycleID=".$cycleID.";";
			//echo $sql."<br/><br/>";
			$a=$connection->returnResultSet($sql);
			if(sizeof($a)==0){
				
				$sql="SELECT AcademicYearID FROM INTRANET_PA_C_CYCLE WHERE CycleID=".$cycleID.";";
				$cycleAcademicYear=current($connection->returnVector($sql));
				
				$sql = "DELETE FROM INTRANET_PA_C_ABSENCE WHERE CycleID=".$cycleID.";";
				//echo $sql."<br/><br/>";
				$connection->db_db_query($sql);
				$sql = "DELETE FROM INTRANET_PA_C_LEAVE WHERE CycleID=".$cycleID.";";
				//echo $sql."<br/><br/>";
				$connection->db_db_query($sql);
				$sql = "DELETE FROM INTRANET_PA_C_BATCH WHERE CycleID=".$cycleID.";";
				//echo $sql."<br/><br/>";
				$connection->db_db_query($sql);
				$sql = "DELETE FROM INTRANET_PA_C_SPECIAL_BATCH_USER WHERE CycleID=".$cycleID.";";
				//echo $sql."<br/><br/>";
				$connection->db_db_query($sql);
				$sql = "DELETE FROM INTRANET_PA_C_IDVAPR WHERE IdvID IN (SELECT IdvID FROM INTRANET_PA_C_IDVFRM WHERE CycleID=".$cycleID.");";
				//echo $sql."<br/><br/>";
				$connection->db_db_query($sql);
				$sql = "DELETE FROM INTRANET_PA_C_IDVFRM WHERE CycleID=".$cycleID.";";
				//echo $sql."<br/><br/>";
				$connection->db_db_query($sql);
				$sql = "DELETE FROM INTRANET_PA_C_GRPFRM WHERE GrpID IN (SELECT GrpID FROM INTRANET_PA_C_GRPSUM WHERE CycleID=".$cycleID.");";
				//echo $sql."<br/><br/>";
				$connection->db_db_query($sql);
				$sql = "DELETE FROM INTRANET_PA_C_GRPMEM WHERE GrpID IN (SELECT GrpID FROM INTRANET_PA_C_GRPSUM WHERE CycleID=".$cycleID.");";
				//echo $sql."<br/><br/>";
				$connection->db_db_query($sql);
				$sql = "DELETE FROM INTRANET_PA_C_GRPSUM WHERE CycleID=".$cycleID.";";
				//echo $sql."<br/><br/>";
				$connection->db_db_query($sql);
				$sql = "DELETE FROM INTRANET_PA_C_FRMROL WHERE CycleID=".$cycleID.";";
				//echo $sql."<br/><br/>";
				$connection->db_db_query($sql);
				$sql = "DELETE FROM INTRANET_PA_C_FRMSEL WHERE CycleID=".$cycleID.";";
				//echo $sql."<br/><br/>";
				$connection->db_db_query($sql);
				$sql = "DELETE FROM INTRANET_PA_C_CYCLE WHERE CycleID=".$cycleID.";";
				//echo $sql."<br/><br/>";
				$connection->db_db_query($sql);
				
				// Log changes
				$recordDetail = array();
				$recordDetail[] = array(
										"Parameter"=>"AcademicYearID",
									  	"DisplayName"=>$Lang['Appraisal']['Cycle']['AcademicYear'],
									  	"Original"=>$cycleAcademicYear,
							  			"MapWithTable"=>"ACADEMIC_YEAR"
									  );			
				$logData = array();
				$logData['CycleID'] = $cycleID;
				$logData['Function'] = 'Cycle Info';
				$logData['RecordType'] = 'DELETE';
				$logData['RecordDetail'] = $json->encode($recordDetail);
				$result = $indexVar['libappraisal']->addCycleSettingLog($logData);		
			}
			else{
				$saveSuccess = false;		
				break;
			}
			//echo $sql;
			//$connection->db_db_query($sql);
		}
	}
}
else{
	$type = $_POST["type"];
	$cycleID = $_POST["CycleID"];
	if($type=="Arr"){
		$templateID = $_POST["templateID"];
		
		$sql = "SELECT grpsum.GrpID FROM INTRANET_PA_C_GRPFRM grpfrm INNER JOIN INTRANET_PA_C_GRPSUM grpsum ON grpfrm.GrpID = grpsum.GrpID WHERE grpfrm.TemplateID='".IntegerSafe($templateID)."' AND grpfrm.Incl=1 AND grpsum.CycleID='".IntegerSafe($cycleID)."'";
		$hasGrpUsage = $connection->returnVector($sql);
		$sql = "SELECT IdvID FROM INTRANET_PA_C_IDVFRM WHERE TemplateID='".IntegerSafe($templateID)."' AND CycleID='".IntegerSafe($cycleID)."'";
		$hasIdvUsage = $connection->returnVector($sql);	
		if(empty($hasGrpUsage) && empty($hasIdvUsage)){
			$sql = "DELETE FROM INTRANET_PA_C_FRMROL WHERE CycleID=".IntegerSafe($cycleID)." AND TemplateID=".IntegerSafe($templateID).";";
			//echo $sql."<br/><br/>";
			$a=$connection->db_db_query($sql);
			$sql = "DELETE FROM INTRANET_PA_C_FRMSEL WHERE CycleID=".IntegerSafe($cycleID)." AND TemplateID=".IntegerSafe($templateID).";";
			//echo $sql."<br/><br/>";
			$a=$connection->db_db_query($sql);
			
			// log form delete from cycle		
			$logData = array();
			$logData['CycleID'] = $cycleID;
			$logData['Function'] = 'Delete Form from Cycle';
			$logData['RecordType'] = 'DELETE';
			$recordDetail[] = array(
									"Parameter"=>"TemplateID",
								  	"DisplayName"=>$Lang['Appraisal']['Cycle']['AccTemplate'],
								  	"Original"=>$templateID,
								  	"MapWithTable"=>"INTRANET_PA_S_FRMTPL",
								  	"MapDisplay"=>"FrmTitleEng"
							);
			$logData['RecordDetail'] = $json->encode($recordDetail);
			$result = $indexVar['libappraisal']->addCycleSettingLog($logData);
		}else{
			$saveSuccess = false;
		}
	}
	else if($type=="Grp"){
		$grpID = $_POST["grpID"];
		
		// Original Data
		$sql = "SELECT DescrChi,DescrEng FROM INTRANET_PA_C_GRPSUM WHERE GrpID='".IntegerSafe($grpID)."'";
		$originalGrpData = current($connection->returnResultSet($sql));
		$sql = "SELECT TemplateID,AppMode FROM INTRANET_PA_C_GRPFRM WHERE GrpID='".IntegerSafe($grpID)."' AND Incl=1";
		$originalGrpFrmData = current($connection->returnArray($sql));
		$sql = "SELECT UserID,LeaderRole,CanCallModify,IsLeaderFill FROM INTRANET_PA_C_GRPMEM WHERE GrpID='".IntegerSafe($grpID)."' AND IsLeader=1";
		$originalGrpLeaderData = $connection->returnArray($sql);
		$sql = "SELECT UserID FROM INTRANET_PA_C_GRPMEM WHERE GrpID='".IntegerSafe($grpID)."' AND IsLeader=0";
		$originalGrpMemberData = $connection->returnVector($sql);
		
		$sql = "DELETE FROM INTRANET_PA_C_GRPFRM WHERE GrpID=".IntegerSafe($grpID).";";
		//echo $sql."<br/><br/>";
		$a=$connection->db_db_query($sql);
		$sql = "DELETE FROM INTRANET_PA_C_GRPMEM WHERE GrpID=".IntegerSafe($grpID).";";
		//echo $sql."<br/><br/>";
		$a=$connection->db_db_query($sql);
		$sql = "DELETE FROM INTRANET_PA_C_GRPSUM WHERE GrpID=".IntegerSafe($grpID)." AND CycleID=".IntegerSafe($cycleID).";";
		//echo $sql."<br/><br/>";
		$a=$connection->db_db_query($sql);
		
		// log group appraisal delete from cycle
		$logData = array();
		$logData['CycleID'] = $cycleID;
		$logData['Function'] = 'Delete Group Appraisal';
		$logData['RecordType'] = 'DELETE';
		$recordDetail[] = array(
								"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['StfGrpDescrChi'],
								"Original"=>$originalGrpData['DescrChi']
							);
		$recordDetail[] = array(
									"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['StfGrpDescrEng'],
									"Original"=>$originalGrpData['DescrEng']
								);
		$recordDetail[] = array(
									"Parameter"=>"TemplateID",
								  	"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['PossibleTemplate'],
								  	"Original"=>$originalGrpFrmData['TemplateID'],
								  	"MapWithTable"=>"INTRANET_PA_S_FRMTPL",
								  	"MapDisplay"=>"FrmTitleEng"
								  );
		$appModeLang = array(
			1=>$Lang['Appraisal']['CycleTemplate']['SelfApprsialMode'],
			2=>$Lang['Appraisal']['CycleTemplate']['UpToDownApprsialMode'],
			3=>$Lang['Appraisal']['CycleTemplate']['DownToUpApprsialMode'],
			4=>$Lang['Appraisal']['CycleTemplate']['GroupApprsialMode']
		);
		$recordDetail[] = array(
								  	"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['ApprsialMode'],
								  	"Original"=>$appModeLang[$originalGrpFrmData['AppMode']]
								  );
		$originalLeaderMsg = "";
		foreach($originalGrpLeaderData as $leader){
			$sql = "SELECT EnglishName FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID='".$leader['UserID']."'";
			$name = current($connection->returnVector($sql));
			$originalLeaderMsg .= $name." (UserID: ".$leader['UserID'].") ";
			if($originalGrpFrmData['AppMode']>1){
				$originalLeaderMsg .= "[".(($leader['LeaderRole']==0)?$Lang['Appraisal']['AppraisalRecordsView']:$Lang['Appraisal']['AppraisalRecordsEdit'])."]";
			}else{
				$originalLeaderMsg .= "[".(($leader['IsLeaderFill']==0)?$Lang['Appraisal']['AppraisalRecordsNeedNotFill']:$Lang['Appraisal']['AppraisalRecordsNeedFill'])."]";
			}
			$originalLeaderMsg .= ($leader['CanCallModify']==1)?"[".$Lang['Appraisal']['AppraisalRecordsModify']."]":"";
			$originalLeaderMsg .= "<br>";
		}
		$recordDetail[] = array(
									"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['AssignLeader'],
								  	"Original"=>$originalLeaderMsg
								  );
		$recordDetail[] = array(
									"Parameter"=>"UserID",
								  	"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['AssignMembers'],
								  	"Original"=>$originalGrpMemberData,
								  	"MapWithTable"=>$appraisalConfig['INTRANET_USER'],
								  	"MapDisplay"=>"EnglishName"
								  );
		$logData['RecordDetail'] = $json->encode($recordDetail);
		$result = $indexVar['libappraisal']->addCycleSettingLog($logData);
	}
	else if($type=="Idv"){
		$idvID = $_POST["idvID"];
		$sql = "SELECT ".$indexVar['libappraisal']->getLangSQL("FrmTitleChi","FrmTitleEng")." as FormName, ".$indexVar['libappraisal']->getLangSQL("FrmCodChi","FrmCodEng")." as FormCode , ".$indexVar['libappraisal']->getLangSQL("u.ChineseName","u.EnglishName")." as UserName, u.UserID, frmtpl.TemplateID FROM INTRANET_PA_C_IDVFRM idvfrm 
				INNER JOIN INTRANET_PA_S_FRMTPL frmtpl ON idvfrm.TemplateID = frmtpl.TemplateID
				INNER JOIN INTRANET_USER u ON idvfrm.UserID= u.UserID
				WHERE idvfrm.IdvID='".IntegerSafe($idvID)."'";
		$idvFrmData = current($connection->returnResultSet($sql));
		$sql = "SELECT ".$indexVar['libappraisal']->getLangSQL("u.ChineseName","u.EnglishName")." as UserName, u.UserID FROM INTRANET_PA_C_IDVAPR idvapr
				INNER JOIN INTRANET_USER u ON idvapr.AprID= u.UserID
				WHERE IdvID='".IntegerSafe($idvID)."'";
		$idvAprData = $connection->returnResultSet($sql);
		
		$sql = "DELETE FROM INTRANET_PA_C_IDVAPR WHERE IdvID=".IntegerSafe($idvID).";";
		//echo $sql."<br/><br/>";
		$a=$connection->db_db_query($sql);
		$sql = "DELETE FROM INTRANET_PA_C_IDVFRM WHERE IdvID=".IntegerSafe($idvID).";";
		//echo $sql."<br/><br/>";
		$a=$connection->db_db_query($sql);
		
		// log one-to-one appraisal delete from cycle	
		$msg = $Lang['Appraisal']['CycleTemplate']['PossibleTemplateForm'].": ".(($idvFrmData['FormCode']=="")?$idvFrmData['FormName']:$idvFrmData['FormName']."-".$idvFrmData['FormCode'])."(ID: ".$idvFrmData['TemplateID'].") <br>";	
		$msg .= $Lang['Appraisal']['CycleTemplate']['AssignedAppraisee'].": ".$idvFrmData['UserName']." (ID: ".$idvFrmData['UserID'].") <br>";
		$msg .= $Lang['Appraisal']['CycleTemplate']['AssignedAppraiser'].": <br>";
		foreach($idvAprData as $apr){
			$msg .= " ".$apr['UserName']." (ID: ".$apr['UserID'].") <br>";
		}
		$logData = array();
		$logData['CycleID'] = $cycleID;
		$logData['Function'] = 'Delete One-to-one Appraisal';
		$logData['RecordType'] = 'DELETE';
		$recordDetail[] = array(
							  	"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['StaffIdvAndArr'],
							  	"Message"=>$msg
							  );
		$logData['RecordDetail'] = $json->encode($recordDetail);
		$result = $indexVar['libappraisal']->addCycleSettingLog($logData);
	}
	else if($type=="ObsArr"){
		$templateID = $_POST["templateID"];
		$sql = "DELETE FROM INTRANET_PA_C_OBSSEL  WHERE CycleID=".IntegerSafe($cycleID)." AND TemplateID=".IntegerSafe($templateID).";";
		//echo $sql."<br/><br/>";
		$a=$connection->db_db_query($sql);
		
		// log obs form delete from cycle		
		$logData = array();
		$logData['CycleID'] = $cycleID;
		$logData['Function'] = 'Delete Form from Academic Affairs';
		$logData['RecordType'] = 'DELETE';
		$recordDetail[] = array(
							  	"DisplayName"=>$Lang['Appraisal']['Cycle']['ObsTemplate'],
							  	"Original"=>$templateID
							  );
		$logData['RecordDetail'] = $json->encode($recordDetail);
		$result = $indexVar['libappraisal']->addCycleSettingLog($logData);
	}
}

$returnMsgKey = ($saveSuccess)? 'DeleteSuccess' : 'DeleteUnsuccess';
$returnPath=sprintf($returnPath,$returnMsgKey).'&returnMsgKey='.$returnMsgKey;
//echo $returnPath;
header($returnPath);



?>