<?php 
// ============================== Related Tables ==============================
//
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================

# Page Title
$cycleID = $_GET["cycleID"];
$titleBar = "<div style=\"display:inline;float:left\"><span class=\"contenttitle\" style=\"vertical-align:bottom\">".$Lang['Appraisal']['AppraisalPeriod']."</span></div>";;
if($cycleID!=0&&$cycleID!=""){
	//$titleBar .= "<div style=\"display:inline;float:right;\" class=\"online_help\"><a href=\"javascript:goDownloadAttachment('".$cycleID."')\" title=\"".$Lang['Appraisal']['TitleBar']['DownloadDocument']."\" onclick=\"goDownload(".$cycleID.")\" class=\"help_icon\" id=\"downloadIcon\">";
	//$titleBar .= "<span style=\"font-size:10px\">".$Lang['Appraisal']['TitleBar']['DownloadDocument']."</span></div>";
}
$TAGS_OBJ[] = array($titleBar);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libappraisal_ui']->Include_JS_CSS();

$connection = new libgeneralsettings();
//======================================================================== Header ========================================================================//
### navigation
$navigationAry[] = array($Lang['Appraisal']['AppraisalPeriod'], 'javascript: goBack();');
$navigationAry[] = array($Lang['Appraisal']['AppraisalPeriodSetting']);
$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);
### step box
/*$stepAry = array();
$stepAry[] = $Lang['Appraisal']['CycleHeader']['Steps1'];
$stepAry[] = $Lang['Appraisal']['CycleHeader']['Steps2'];
$stepAry[] = $Lang['Appraisal']['CycleHeader']['Steps3'];
$stepAry[] = $Lang['Appraisal']['CycleHeader']['Steps4'];
$htmlAry['customizedImportStepTbl'] = $indexVar['libappraisal_ui']->GET_IMPORT_STEPS($CurrStep=1, $stepAry);*/
//======================================================================== Content ========================================================================//
$sql="SELECT CycleID,AcademicYearID,DATE_FORMAT(AppPrdFr,'%Y-%m-%d') as AppPrdFr,DATE_FORMAT(AppPrdTo,'%Y-%m-%d') as AppPrdTo,DescrChi,DescrEng,
	DATE_FORMAT(CycleStart,'%Y-%m-%d') as CycleStart,DATE_FORMAT(CycleClose,'%Y-%m-%d') as CycleClose,GuideChi,GuideEng,GuideFile,RlsBy_UserID,DateTimeRls,
	CASE WHEN DATE_FORMAT(NOW(),'%Y-%m-%d') BETWEEN CycleStart AND CycleClose THEN '".$Lang['Appraisal']['CycleStatusDes']['InProgress']."'
	WHEN DATE_FORMAT(NOW(),'%Y-%m-%d') > CycleClose THEN '".$Lang['Appraisal']['CycleStatusDes']['Finish']."' WHEN DATE_FORMAT(NOW(),'%Y-%m-%d') < CycleStart THEN '".$Lang['Appraisal']['CycleStatusDes']['NotYetStart']."' END as Status,ReportReleaseDate 
	FROM(
		SELECT CycleID,AcademicYearID,AppPrdFr,AppPrdTo,DescrChi,DescrEng,CycleStart,CycleClose,GuideChi,GuideEng,GuideFile,RlsBy_UserID,DateTimeRls,ReportReleaseDate 
		FROM INTRANET_PA_C_CYCLE WHERE CycleID=".IntegerSafe($cycleID)."
	) as ipcc;";
//echo $sql."<br/><br/>";
$a=$connection->returnResultSet($sql);
$dateTimeRls=$a[0]["DateTimeRls"];
$rlsByUserID=$a[0]["RlsBy_UserID"];

$academicYearList=$indexVar['libappraisal']->getAllAcademicYear("");
//======================================================================== 考績週期基本設定 ========================================================================//
$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['Cycle']['BasicSetting'])."\r\n";
$x .= "<table class=\"form_table_v30\">";
$x .= "<tr><td class=\"field_title\">".$indexVar['libappraisal_ui']->RequiredSymbol().$Lang['Appraisal']['Cycle']['AcademicYearID']."</td>";
$x .= "<td>".getSelectByAssoArray($indexVar['libappraisal']->cnvGeneralArrToSelect($academicYearList, "AcademicYearID","YearNameB5","YearNameEN"), $selectionTags='id="AcademicYearID" name="AcademicYearID"', $SelectedType=$a[0]["AcademicYearID"], $all=0, $noFirst=0);
$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('AcademicYearIDEmptyWarnDiv', $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv');
$x .= "</td></tr>";
/*$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['Cycle']['AppPrd']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_DATE_PICKER('AppPrdFr',$a[0]["AppPrdFr"],$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('AppPrdFr')",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum");
$x .= "-".$indexVar['libappraisal_ui']->GET_DATE_PICKER('AppPrdTo',$a[0]["AppPrdTo"],$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('AppPrdTo')",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum");
$x .= "</td></tr>";*/
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['Cycle']['DescrEng']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTAREA("DescrEng", $taContents=$a[0]["DescrEng"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['Cycle']['DescrChi']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTAREA("DescrChi", $taContents=$a[0]["DescrChi"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['Cycle']['CycleStart']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_DATE_PICKER('CycleStart',$a[0]["CycleStart"],$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('AppPrdFr')",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum");
$x .= "<span style=\"color:#999999;margin-left:20px\"><i>".$Lang['Appraisal']['Cycle']['AfterCycleStart']."</i></span>";
$x .= "</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['Cycle']['CycleClose']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_DATE_PICKER('CycleClose',$a[0]["CycleClose"],$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('AppPrdFr')",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum");
$x .= "<span style=\"color:#999999;margin-left:20px\"><i>".$Lang['Appraisal']['Cycle']['AfterCycleClose']."</i></span><br/>";
$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('DateWarnDiv', $Lang['Appraisal']['Cycle']['DateCheck'], $Class='warnMsgDiv')."\r\n";
$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('DateWarnDiv2', $Lang['Appraisal']['Cycle']['DateCheckOverPeriod'], $Class='warnMsgDiv')."\r\n";
$x .= "</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['Cycle']['Status']."</td>";
$x .= "<td>"."<span>".(($a[0]["Status"]!=null)?$a[0]["Status"]:$Lang['Appraisal']['Cycle']['NotRls'])."</span>";
$x .= "</td></tr>";
$x .= "</table>";
$x .="<span style=\"color:#999999\">".sprintf($Lang['Appraisal']['Cycle']['Remark'],"<span style=\"color:#FF0000\">*</span>")."</span><br/><br/>";
$x .= "<br/>";


//======================================================================== 填寫指引文件 ========================================================================//
$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['Cycle']['Guideline'])."\r\n";
$x .= "<table class=\"form_table_v30\">";
/*$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['Cycle']['GuideEng']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('GuideEng', 'GuideEng', $a[0]["GuideEng"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</span>";
$x .= "</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['Cycle']['GuideChi']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('GuideChi', 'GuideChi', $a[0]["GuideChi"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</span>";
$x .= "</td></tr>";*/
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['Cycle']['GuideFile']."</td>";
//<a id='DownloadAbsence' class=\"tablelink\" href=\"javascript:goDownloadAbsence(".$cycleID.")\">".$Lang['Appraisal']['CycleTemplate']['DownloadAbsence']."</a>
$x .= "<td>"."<input type=\"file\" name=\"GuideFile\" id=\"GuideFile\" class=\"file\"><br/>";
$x .= "<a id='DownloadAttachment' class=\"tablelink\" href=\"javascript:goDownloadAttachment(".$cycleID.")\">".$a[0]["GuideFile"]."</a>";
$x .="</span>";
$x .= "</td></tr>";
$x .= "</table>";
$x .= "<br/>";
$x .= "<div class=\"edit_bottom_v30\"><p class=\"spacer\"></p>";
if($cycleID != null){
	$x .= $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnSave'], "button", "goSubmit()", 'submitBtn');
}
else{
	$x .= $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnSaveNext'], "button", "goSubmit()", 'submitBtn');
}
$x .= "&nbsp;";
$x .= $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');
$x .= "&nbsp;";
if($a[0]['ReportReleaseDate']==""){
    $x .= $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnReportRelease'], "button", "goReleaseReport()", 'rlsReportBtn');
}
$x .= "</div>";
//======================================================================== 適用評審表格及特別考績角色 ========================================================================//
if($cycleID != null){
	$sql="SELECT ipcf.CycleID,ipcf.TemplateID,".$indexVar['libappraisal']->getLangSQL("FrmInfoChi","FrmInfoEng")." as FrmInfo,
			COALESCE(SpecRoleCnt,0) as SpecRoleCnt,COALESCE(NoAssignSpcRoleCnt,0) as NoAssignSpcRoleCnt,TemplateType
		FROM(
			SELECT CycleID,TemplateID FROM INTRANET_PA_C_FRMSEL WHERE CycleID=".IntegerSafe($cycleID)."
		) as ipcf
		INNER JOIN(
			SELECT TemplateID,FrmTitleChi,FrmTitleEng,FrmCodChi,FrmCodEng,ObjChi,ObjEng,
			CONCAT(FrmTitleChi,'-',FrmCodChi,'-',ObjChi) as FrmInfoChi,CONCAT(FrmTitleEng,'-',FrmCodEng,'-',ObjEng) as FrmInfoEng,TemplateType
			FROM INTRANET_PA_S_FRMTPL
		) as ipsf ON ipcf.TemplateID=ipsf.TemplateID
		LEFT JOIN(
			SELECT ipcf.TemplateID,COUNT(DISTINCT AssSpecUserID) as NoAssignSpcRoleCnt,COUNT(DISTINCT AllSpecUserID) as SpecRoleCnt
			FROM(
				SELECT AppRoleID FROM INTRANET_PA_S_APPROL WHERE SysUse=0 AND IsActive=1
			) as ipsa
			LEFT JOIN(
				SELECT CycleID,TemplateID,AppRoleID,set_order FROM INTRANET_PA_C_FRMROL
				WHERE CycleID=".IntegerSafe($cycleID)." AND UserID IS NOT NULL
			) as ipcf ON ipsa.AppRoleID=ipcf.AppRoleID
			LEFT JOIN(
				SELECT CycleID,TemplateID,AppRoleID as AllSpecAppRoleID,set_order, UserID as AllSpecUserID FROM INTRANET_PA_C_FRMROL
				WHERE CycleID=".IntegerSafe($cycleID)." AND UserID IS NOT NULL
			) as ipcfRole ON ipcf.CycleID=ipcfRole.CycleID AND ipcf.TemplateID=ipcfRole.TemplateID AND ipsa.AppRoleID=ipcfRole.AllSpecAppRoleID AND ipcf.set_order = ipcfRole.set_order
			LEFT JOIN(
				SELECT CycleID,TemplateID,AppRoleID as AssSpecAppRoleID,set_order, UserID as AssSpecUserID FROM INTRANET_PA_C_FRMROL WHERE UserID IS NOT NULL
				AND CycleID=".IntegerSafe($cycleID)."
			) as ipcfUser ON ipcf.CycleID=ipcfUser.CycleID AND ipcf.TemplateID=ipcfUser.TemplateID AND ipsa.AppRoleID=ipcfUser.AssSpecAppRoleID AND ipcf.set_order = ipcfRole.set_order
			GROUP BY ipcf.TemplateID						
		) as a ON ipcf.TemplateID=a.TemplateID";
	//echo $sql."<br/><br/>";
	$a=$connection->returnResultSet($sql);
	$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['Cycle']['AccTemplate'])."\r\n";
	$subBtnAry = array();
	$btnAry[] = array('new', 'javascript: goAddTemplate('.$cycleID.');', $Lang['Appraisal']['BtnAddAccTemplate'], $subBtnAry);
	$x .= $indexVar['libappraisal_ui']->Get_Content_Tool_By_Array_v30($btnAry);
	
	$x .= "<div id=\"DetailLayer\"><span>";
	$x .= "<table id=\"dndTable\" class=\"common_table_list ClassDragAndDrop\">";
	$x .= "<thead>"."\r\n";
	$x .= "<tr>";
	$x .= "<th class=\"sub_row_top\" style=\"width:2%;\">#</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:30%;\">".$Lang['Appraisal']['Cycle']['AccTemplate']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:30%;\">".$Lang['Appraisal']['CycleTemplate']['InvolvedAppRoleID']."</th>";
	//$x .= "<th class=\"sub_row_top\" style=\"width:20%;\">".$Lang['Appraisal']['CycleTemplate']['DisplayOrder']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:20%;\">".$Lang['Appraisal']['CycleTemplate']['Delete']."</th>";
	$x .= "</tr>";
	$x .= "</thead>";
	$x .= "<tbody>"."\r\n";
	for($i=0;$i<sizeof($a);$i++){
		$j=$i+1;
		$x .= "<tr id=\"".$a[$i]['TemplateID']."\">"."\r\n";
		$x .= "<td>".$j."</td>"."\r\n";
		$x .= "<td><a id='AccTemplateID_'".$a[$i]['TemplateID']." class=\"tablelink\" href=\"javascript:goEditAcceptTemplate(".$cycleID.",".$a[$i]["TemplateID"].")\">".$a[$i]["FrmInfo"]."</a></td>"."\r\n";
		$x .= "<td>";
		$x .= ($a[$i]["TemplateType"]!="0")? $a[$i]["SpecRoleCnt"]:"0";
		//$x .= (($a[$i]["NoAssignSpcRoleCnt"]!=$a[$i]["SpecRoleCnt"]) && $a[$i]["TemplateType"]!="0")?"<span style=\"color:#FF0000\">".sprintf($Lang['Appraisal']['CycleTemplate']['NoAssSpecAppRole'],$a[$i]["SpecRoleCnt"] - $a[$i]["NoAssignSpcRoleCnt"])."</span>":"";
		$x .= "</td>\r\n";
		//$x .= "<td class=\"Dragable\"><div class=\"table_row_tool\"><a href=\"#\" class=\"move_order_dim\" title=\"".$Lang['SysMgr']['FormClassMapping']['Move']."\"></a></div></td>"."\r\n";
		$x .= "<td><div class=\"table_row_tool\" id='delete_".$a[$i]['TemplateID']."'><a href=\"javascript:void(0)\" class=\"delete_dim\" onclick=javascript:goDeleteArr(".$cycleID.",".$a[$i]["TemplateID"].") title=\"".$Lang['SysMgr']['FormClassMapping']['Delete']."\"></a></div></td>"."\r\n";
		$x .= "<input type=\"hidden\" id=\"TemplateID_".$a[$i]["TemplateID"]."\" name=\"TemplateID_".$a[$i]["TemplateID"]."\" value=".$a[$i]["TemplateID"]."></td>"."\r\n";
		$x .= "</tr>";
	}
	$x .= "</table>";
	$x .= "</div><br/>";
	//======================================================================== 職員分組及評核安排 ========================================================================//
	$sql="SELECT ipcg.GrpID,CycleID,Descr,Count
		FROM(
			SELECT GrpID,CycleID,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr
			FROM INTRANET_PA_C_GRPSUM WHERE CycleID=".IntegerSafe($cycleID)."
		) as ipcg
		LEFT JOIN(
			SELECT GrpID,COUNT(UserID) as Count FROM INTRANET_PA_C_GRPMEM GROUP BY GrpID
		) as ipcgrp ON ipcg.GrpID=ipcgrp.GrpID";
	//echo $sql."<br/><br/>";
	$a=$connection->returnResultSet($sql);
	$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['CycleTemplate']['StfGrpAndArr'])."\r\n";
	$btnAry=null;
	$subBtnAry = array();
	$btnAry[] = array('new', 'javascript: goAddStfGrp('.$cycleID.');', $Lang['Appraisal']['BtnAddStaffGrp'], $subBtnAry);
	$x .= $indexVar['libappraisal_ui']->Get_Content_Tool_By_Array_v30($btnAry);
	
	$x .= "<div id=\"DetailLayer\"><span>";
	$x .= "<table id=\"dndTable1\" class=\"common_table_list ClassDragAndDrop\">";
	$x .= "<thead>"."\r\n";
	$x .= "<tr>";
	$x .= "<th class=\"sub_row_top\" style=\"width:2%;\">#</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:31%;\">".$Lang['Appraisal']['CycleTemplate']['StfGrp']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:50%;\">".$Lang['Appraisal']['CycleTemplate']['StfGrpCnt']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:20%;\">".$Lang['Appraisal']['CycleTemplate']['StfGrpDelete']."</th>";
	$x .= "</tr>";
	$x .= "</thead>";
	$x .= "<tbody>"."\r\n";
	for($i=0;$i<sizeof($a);$i++){
		$j=$i+1;
		$x .= "<tr id=\"".$a[$i]['GrpID']."\">"."\r\n";
		$x .= "<td>".$j."</td>"."\r\n";
		$descr = ($a[$i]["Descr"]!="")?$a[$i]["Descr"]:$Lang['Appraisal']['CycleTemplate']['NoGrpDescr'];
		$x .= "<td><a id='StaffGroupID_'".$a[$i]['GrpID']." class=\"tablelink\" href=\"javascript:goEditStfGrp(".$cycleID.",".$a[$i]["GrpID"].")\">".$descr."</a></td>"."\r\n";
		$x .= "<td>".$a[$i]["Count"]."</td>\r\n";
		$x .= "<td><div class=\"table_row_tool\" id='deleteStfGroup_".$a[$i]['GrpID']."'><a href=\"javascript:void(0)\" class=\"delete_dim\" onclick=javascript:goDeleteGrp(".$cycleID.",".$a[$i]['GrpID'].") title=\"".$Lang['SysMgr']['FormClassMapping']['Delete']."\"></a></div></td>"."\r\n";
		$x .= "<input type=\"hidden\" id=\"TemplateID_".$a[$i]["TemplateID"]."\" name=\"TemplateID_".$a[$i]["TemplateID"]."\" value=".$a[$i]["TemplateID"]."></td>"."\r\n";
		$x .= "</tr>";
	}
	$x .= "</table>";
	$x .= "</div><br/>";
	//======================================================================== 個別職員評核安排 ========================================================================//
	$userNameSql = $indexVar['libappraisal']->getLangSQL("ChineseName","EnglishName");
	$sql="SELECT IdvID,CycleID,".$indexVar['libappraisal']->Get_Name_Field("", true)." as Name,".$indexVar['libappraisal']->getLangSQL("FrmChiTitle","FrmEngTitle")." as FrmTitle
		FROM(
			SELECT IdvID,CycleID,UserID,TemplateID
			FROM INTRANET_PA_C_IDVFRM WHERE CycleID=".IntegerSafe($cycleID)."
		) as ipci
		LEFT JOIN(			
			SELECT EnglishName,ChineseName,UserLogin,UserID,RecordStatus FROM ".$appraisalConfig['INTRANET_USER']."				
		) as iu ON ipci.UserID=iu.UserID
		LEFT JOIN(			
			SELECT TemplateID,TemplateType,FrmTitleChi,FrmCodChi,ObjChi,FrmTitleEng,FrmCodEng,ObjEng,IsActive,DisplayOrder as OrderNum,DisplayOrder,DateTimeModified,
			CONCAT(FrmTitleEng,'-',FrmCodEng,'-',ObjEng) as FrmEngTitle,CONCAT(FrmTitleChi,'-',FrmCodChi,'-',ObjChi) as FrmChiTitle
			FROM INTRANET_PA_S_FRMTPL
		) as ipsf ON ipci.TemplateID=ipsf.TemplateID
					";
	//echo $sql."<br/><br/>";
	$a=$connection->returnResultSet($sql);
	$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['CycleTemplate']['StaffIdvAndArr'])."\r\n";
	$btnAry=null;
	$subBtnAry = array();
	$btnAry[] = array('new', 'javascript: goAddStfIdv('.$cycleID.');', $Lang['Appraisal']['BtnAddStaffIdv'], $subBtnAry);
	$x .= $indexVar['libappraisal_ui']->Get_Content_Tool_By_Array_v30($btnAry);
	
	$x .= "<div id=\"DetailLayer\"><span>";
	$x .= "<table id=\"dndTable1\" class=\"common_table_list ClassDragAndDrop\">";
	$x .= "<thead>"."\r\n";
	$x .= "<tr>";
	$x .= "<th class=\"sub_row_top\" style=\"width:2%;\">#</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:31%;\">".$Lang['Appraisal']['CycleTemplate']['StfIdvName']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:50%;\">".$Lang['Appraisal']['CycleTemplate']['StfIdvTemplate']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:20%;\">".$Lang['Appraisal']['CycleTemplate']['StfGrpDelete']."</th>";
	$x .= "</tr>";
	$x .= "</thead>";
	$x .= "<tbody>"."\r\n";
	for($i=0;$i<sizeof($a);$i++){
		$j=$i+1;
		$x .= "<tr id=\"".$a[$i]['GrpID']."\">"."\r\n";
		$x .= "<td>".$j."</td>"."\r\n";
		$x .= "<td><a id='StfIdvNameID_'".$a[$i]['IdvID']." class=\"tablelink\" href=\"javascript:goEditStfIdv(".$cycleID.",".$a[$i]["IdvID"].")\">".$a[$i]["Name"]."</a></td>"."\r\n";
		$x .= "<td><a id='StfIdvTemplateID_'".$a[$i]['IdvID']." class=\"tablelink\" href=\"javascript:goEditStfIdv(".$cycleID.",".$a[$i]["IdvID"].")\">".$a[$i]["FrmTitle"]."</a></td>\r\n";
		$x .= "<td><div class=\"table_row_tool\" id='deleteStfGroup_".$a[$i]['GrpID']."'><a href=\"javascript:void(0)\" class=\"delete_dim\" onclick=javascript:goDeleteIdv(".$cycleID.",".$a[$i]['IdvID'].") title=\"".$Lang['SysMgr']['FormClassMapping']['Delete']."\"></a></div></td>"."\r\n";
		$x .= "<input type=\"hidden\" id=\"TemplateID_".$a[$i]["TemplateID"]."\" name=\"TemplateID_".$a[$i]["TemplateID"]."\" value=".$a[$i]["TemplateID"]."></td>"."\r\n";
		$x .= "</tr>";
	}
	$x .= "</table>";
	$x .= "</div><br/>";
	
	//======================================================================== 考績週期內的請假、缺席、遲到、代課資料 ========================================================================//
	/*$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['CycleTemplate']['ApprasialLeave'])."\r\n";
	
	$sql="SELECT CycleID FROM INTRANET_PA_C_LEAVE WHERE CycleID=".$cycleID.";";
	$leaveList=$connection->returnResultSet($sql);
	$sql="SELECT CycleID FROM INTRANET_PA_C_ABSENCE WHERE CycleID=".$cycleID.";";
	$absenceList=$connection->returnResultSet($sql);
	
	$btnAry=null;
	$subBtnAry = array();
	$btnAry[] = array('import', 'javascript: goImportLeave('.$cycleID.');', $Lang['Appraisal']['BtnImportLeave'], $subBtnAry);
	$btnAry[] = array('import', 'javascript: goImportAbsLateSub('.$cycleID.');', $Lang['Appraisal']['BtnImportAbsLateSub'], $subBtnAry);
	$x .= $indexVar['libappraisal_ui']->Get_Content_Tool_By_Array_v30($btnAry);
	$x .= "<table class=\"form_table_v30\">";
	$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['CycleTemplate']['LastUploadLeaveCSV']."</td>";
	$x .= "<td><span>".((sizeof($leaveList)>0)?"<a id='DownloadLeave' class=\"tablelink\" href=\"javascript:goDownloadLeave(".$cycleID.")\">".$Lang['Appraisal']['CycleTemplate']['DownloadLeave']."</a>":"")."</span>";
	$x .= "</td></tr>";
	$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['CycleTemplate']['LastUploadAbsLateSubCSV']."</td>";
	$x .= "<td><span>".((sizeof($leaveList)>0)?"<a id='DownloadAbsence' class=\"tablelink\" href=\"javascript:goDownloadAbsence(".$cycleID.")\">".$Lang['Appraisal']['CycleTemplate']['DownloadAbsence']."</a>":"")."</span>";
	$x .= "</td></tr>";
	$x .= "</table>";
	$x .= "<br/>";*/
	
	//======================================================================== 考績表格發放資料 ========================================================================//
	$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['Cycle']['RlsInfo'])."\r\n";
	$x .= "<table class=\"form_table_v30\">";
	$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['Cycle']['DateTimeRls']."</td>";
	$x .= "<td>"."<span>".(($dateTimeRls!=null)?$dateTimeRls:" -- ")."</span>";
	$x .= "</td></tr>";
	$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['Cycle']['RlsBy_UserID']."</td>";
	$x .= "<td>"."<span>".(($rlsByUserID!=null)?$indexVar['libappraisal']->getUserNameID($rlsByUserID,"nameID"):" -- ")."</span>";
	$x .= "</td></tr>";
	$x .= "</table><br/>";
	//======================================================================== 設定本週期可用觀課表格 ========================================================================//
	$sql="SELECT ipcf.CycleID,ipcf.TemplateID,".$indexVar['libappraisal']->getLangSQL("FrmInfoChi","FrmInfoEng")." as FrmInfo,
			COALESCE(SpecRoleCnt,0) as SpecRoleCnt,COALESCE(NoAssignSpcRoleCnt,0) as NoAssignSpcRoleCnt,TemplateType
		FROM(
			SELECT CycleID,TemplateID FROM INTRANET_PA_C_OBSSEL WHERE CycleID=".IntegerSafe($cycleID)."
		) as ipcf
		INNER JOIN(
			SELECT TemplateID,FrmTitleChi,FrmTitleEng,FrmCodChi,FrmCodEng,ObjChi,ObjEng,
			CONCAT(FrmTitleChi,'-',FrmCodChi,'-',ObjChi) as FrmInfoChi,CONCAT(FrmTitleEng,'-',FrmCodEng,'-',ObjEng) as FrmInfoEng,TemplateType
			FROM INTRANET_PA_S_FRMTPL
		) as ipsf ON ipcf.TemplateID=ipsf.TemplateID
		LEFT JOIN(
			SELECT ipcf.TemplateID,COUNT(AssSpecAppRoleID) as NoAssignSpcRoleCnt,COUNT(AllSpecAppRoleID) as SpecRoleCnt
			FROM(
				SELECT AppRoleID FROM INTRANET_PA_S_APPROL WHERE SysUse=0 AND IsActive=1
			) as ipsa
			LEFT JOIN(
				SELECT CycleID,TemplateID,AppRoleID FROM INTRANET_PA_C_FRMROL
				WHERE CycleID=".IntegerSafe($cycleID)."
			) as ipcf ON ipsa.AppRoleID=ipcf.AppRoleID
			LEFT JOIN(
				SELECT CycleID,TemplateID,AppRoleID as AllSpecAppRoleID FROM INTRANET_PA_C_FRMROL
				WHERE CycleID=".IntegerSafe($cycleID)."
			) as ipcfRole ON ipcf.CycleID=ipcfRole.CycleID AND ipcf.TemplateID=ipcfRole.TemplateID AND ipsa.AppRoleID=ipcfRole.AllSpecAppRoleID
			LEFT JOIN(
				SELECT CycleID,TemplateID,AppRoleID as AssSpecAppRoleID FROM INTRANET_PA_C_FRMROL WHERE UserID IS NOT NULL
				AND CycleID=".IntegerSafe($cycleID)."
			) as ipcfUser ON ipcf.CycleID=ipcfUser.CycleID AND ipcf.TemplateID=ipcfUser.TemplateID AND ipsa.AppRoleID=ipcfUser.AssSpecAppRoleID
			GROUP BY ipcf.TemplateID						
		) as a ON ipcf.TemplateID=a.TemplateID";
	//echo $sql."<br/><br/>";
	$a=$connection->returnResultSet($sql);;
	$x0 .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['Cycle']['RlsObsInfo'])."\r\n";
	$btnAry=null;
	$subBtnAry = array();
	$btnAry[] = array('new', 'javascript: goAddObs('.$cycleID.');', $Lang['Appraisal']['BtnAddAns'], $subBtnAry);
	$x0 .= $indexVar['libappraisal_ui']->Get_Content_Tool_By_Array_v30($btnAry);
	$x0 .= "<div id=\"DetailLayer\"><span>";
	$x0 .= "<table id=\"dndTable\" class=\"common_table_list ClassDragAndDrop\">";
	$x0 .= "<thead>"."\r\n";
	$x0 .= "<tr>";
	$x0 .= "<th class=\"sub_row_top\" style=\"width:2%;\">#</th>";
	$x0 .= "<th class=\"sub_row_top\" style=\"width:60%;\">".$Lang['Appraisal']['Cycle']['ObsTemplate']."</th>";
	//$x0 .= "<th class=\"sub_row_top\" style=\"width:20%;\">".$Lang['Appraisal']['CycleTemplate']['DisplayOrder']."</th>";
	$x0 .= "<th class=\"sub_row_top\" style=\"width:20%;\">".$Lang['Appraisal']['CycleTemplate']['Delete']."</th>";
	$x0 .= "</tr>";
	$x0 .= "</thead>";
	$x0 .= "<tbody>"."\r\n";
	for($i=0;$i<sizeof($a);$i++){
		$j=$i+1;
		$x0 .= "<tr id=\"".$a[$i]['TemplateID']."\">"."\r\n";
		$x0 .= "<td>".$j."</td>"."\r\n";
		$x0 .= "<td>".$a[$i]["FrmInfo"]."</td>"."\r\n";
		//$x0 .= "<td class=\"Dragable\"><div class=\"table_row_tool\"><a href=\"#\" class=\"move_order_dim\" title=\"".$Lang['SysMgr']['FormClassMapping']['Move']."\"></a></div></td>"."\r\n";
		$x0 .= "<td><div class=\"table_row_tool\" id='delete_".$a[$i]['TemplateID']."'><a href=\"javascript:void(0)\" class=\"delete_dim\" onclick=javascript:goDeleteObsArr(".$cycleID.",".$a[$i]["TemplateID"].") title=\"".$Lang['SysMgr']['FormClassMapping']['Delete']."\"></a></div></td>"."\r\n";
		$x0 .= "<input type=\"hidden\" id=\"TemplateID_".$a[$i]["TemplateID"]."\" name=\"TemplateID_".$a[$i]["TemplateID"]."\" value=".$a[$i]["TemplateID"]."></td>"."\r\n";
		$x0 .= "</tr>";
	}
	$x0 .= "</table>";
	$x0 .= "</div><br/>";
	
	
	//======================================================================== 設定本週期可用觀課表格 ========================================================================//
	
	
}
$x .= "<input type=\"hidden\" id=\"CycleID\" name=\"CycleID\" value=".$cycleID.">"."\r\n";

$htmlAry['contentTbl'] = $x;
$htmlAry['contentTbl2'] = $x0;
// ============================== Define Button ==============================
if($cycleID != null){
	$htmlAry['rlsAppFormBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtbRlsAppForm'], "button", "goRlsAppForm()", 'rlsAppFormBtn');	
	$htmlAry['rlsObsFormBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtbRlsObsAppForm'], "button", "goRlsObsAppForm()", 'rlsObsFormBtn');
}
// ============================== Define Button ==============================











?>