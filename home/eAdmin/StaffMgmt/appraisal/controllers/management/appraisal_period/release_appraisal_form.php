<?php
// ============================== Related Tables ==============================
//
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================

# Page Title
//$TAGS_OBJ[] = array($Lang['Appraisal']['AppraisalPeriod']);
//$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
$cycleID = $_POST["CycleID"];
$returnPath=$_POST["returnPath"];
$total = 46;
$connection = new libgeneralsettings();
$json = new JSON_obj();
$result = array();

$currDir = getcwd();
chdir($intranet_root."/file/temp");
foreach (glob("teacherAppraisal*.txt") as $filename) {
    unlink($filename);
}
chdir($currDir);

function recordProgress($currentProgress, $total){
	global $intranet_root, $json;
	$percent = intval($currentProgress/$total * 100);
	$arr_content['percent'] = $percent;
	$arr_content['message'] = $percent . "%";
	file_put_contents($intranet_root."/file/temp/teacherAppraisal" . session_id() . ".txt", $json->encode($arr_content));
	sleep(1);
}
//$connection->Start_Trans();
//-- *** Generate forms and batches to Staging ***
//-- G0. Update Cycle Release No.
$sql = "UPDATE IGNORE INTRANET_PA_C_CYCLE SET RlsNo = RlsNo + 1, RlsBy_UserID ='".IntegerSafe($_SESSION['UserID'])."', DateTimeRls = NOW() WHERE CycleID = '".IntegerSafe($cycleID)."';";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(1, $total);

//-- G1. Clear Staging tables
$sql = "TRUNCATE TABLE INTRANET_PA_T_FRMSUBSTG;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
$sql = "TRUNCATE TABLE INTRANET_PA_T_FRMSUMSTG;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(2, $total);


//-- G2. Create Form Records for Group 自評 (Same person being Appraisee and Appraiser)
$conds = "";
$sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='TeacherApprisal' AND SettingName='ExcludeAssessmentTeacher'";
$condParm = current($connection->returnVector($sql));
if($condParm!= ""){
	$conds = " WHERE APS.UserID NOT IN (".$condParm.") AND APS.IsLeaderFill=1";
}else{
	$conds = " WHERE APS.IsLeaderFill=1";
}
$sql = "INSERT INTO INTRANET_PA_T_FRMSUMSTG (CycleID, TemplateID, UserID, AprID, GrpID, IsActive, InputBy_UserID, DateTimeInput, ModifiedBy_UserID, DateTimeModified)
	SELECT CycleID, TemplateID, APS.UserID, APS.UserID, GSF.GrpID,1,".IntegerSafe($_SESSION['UserID']).",NOW(),".IntegerSafe($_SESSION['UserID']).",NOW() FROM INTRANET_PA_C_GRPMEM APS
	INNER JOIN
	(SELECT CycleID, GS.GrpID, TemplateID FROM INTRANET_PA_C_GRPSUM GS INNER JOIN INTRANET_PA_C_GRPFRM GF ON GS.GrpID = GF.GrpID WHERE CYCLEID = '".IntegerSafe($cycleID)."' AND Incl = 1 AND AppMode = 1) GSF
	ON APS.GrpID = GSF.GrpID $conds";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(3, $total);
$conds = "";

//-- G3. Create Form Records for Group 組長評組員 (Group Member =  Appraisee, Group Leader = Appraiser)
$sql = "INSERT INTO INTRANET_PA_T_FRMSUMSTG (CycleID, TemplateID, UserID, AprID, GrpID, IsActive, InputBy_UserID, DateTimeInput, ModifiedBy_UserID, DateTimeModified)
	SELECT CycleID, TemplateID, APS.UserID, APR.UserID, GSF.GrpID,1,".IntegerSafe($_SESSION['UserID']).",NOW(),".IntegerSafe($_SESSION['UserID']).",NOW() FROM INTRANET_PA_C_GRPMEM APS
	INNER JOIN
	(SELECT CycleID, GS.GrpID, TemplateID FROM INTRANET_PA_C_GRPSUM GS INNER JOIN INTRANET_PA_C_GRPFRM GF ON GS.GrpID = GF.GrpID WHERE CYCLEID = '".IntegerSafe($cycleID)."' AND Incl = 1 AND AppMode = 2) GSF
	ON APS.GrpID = GSF.GrpID AND APS.IsLeader = 0
	INNER JOIN INTRANET_PA_C_GRPMEM APR ON APS.GrpID = APR.GrpID AND APR.IsLeader = 1 AND APR.LeaderRole = 1;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(4, $total);

//-- G4. Create Form Records for Group 組員評組長 (Group Leader =  Appraisee, Group Member = Appraiser(s))
$sql = "INSERT INTO INTRANET_PA_T_FRMSUMSTG (CycleID, TemplateID, UserID, AprID, GrpID, IsActive, InputBy_UserID, DateTimeInput, ModifiedBy_UserID, DateTimeModified)
SELECT CycleID, TemplateID, APS.UserID, APR.UserID, GSF.GrpID,1,".IntegerSafe($_SESSION['UserID']).",NOW(),".IntegerSafe($_SESSION['UserID']).",NOW() FROM INTRANET_PA_C_GRPMEM APS
INNER JOIN
(SELECT CycleID, GS.GrpID, TemplateID FROM INTRANET_PA_C_GRPSUM GS INNER JOIN INTRANET_PA_C_GRPFRM GF ON GS.GrpID = GF.GrpID WHERE CYCLEID = '".IntegerSafe($cycleID)."' AND Incl = 1 AND AppMode = 3) GSF
ON APS.GrpID = GSF.GrpID AND APS.IsLeader = 1
INNER JOIN INTRANET_PA_C_GRPMEM APR ON APS.GrpID = APR.GrpID AND APR.IsLeader = 0;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(5, $total);

//-- G5. Create Form Records for Group 互評 (Group Members + Leader =  Appraisee, Group Members + Leader (except myself) = Appraiser(s))
$sql = "INSERT INTO INTRANET_PA_T_FRMSUMSTG (CycleID, TemplateID, UserID, AprID, GrpID, IsActive, InputBy_UserID, DateTimeInput, ModifiedBy_UserID, DateTimeModified)
	SELECT CycleID, TemplateID, APS.UserID, APR.UserID, GSF.GrpID,1,".IntegerSafe($_SESSION['UserID']).",NOW(),".IntegerSafe($_SESSION['UserID']).",NOW() FROM INTRANET_PA_C_GRPMEM APS
	INNER JOIN
	(SELECT CycleID, GS.GrpID, TemplateID FROM INTRANET_PA_C_GRPSUM GS INNER JOIN INTRANET_PA_C_GRPFRM GF ON GS.GrpID = GF.GrpID WHERE CYCLEID = '".IntegerSafe($cycleID)."' AND Incl = 1 AND AppMode = 4) GSF
	ON APS.GrpID = GSF.GrpID
	INNER JOIN INTRANET_PA_C_GRPMEM APR ON APS.GrpID = APR.GrpID AND APS.UserID <> APR.UserID;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(6, $total);

//-- G6. Create Form Records for Individual Assignment
$sql = "INSERT INTO INTRANET_PA_T_FRMSUMSTG (CycleID, TemplateID, UserID, AprID, IdvID, IsActive, InputBy_UserID, DateTimeInput, ModifiedBy_UserID, DateTimeModified)
	SELECT CycleID, TemplateID, UserID, AprID, F.IdvID, 1,".IntegerSafe($_SESSION['UserID']).",NOW(),".IntegerSafe($_SESSION['UserID']).",NOW()
	FROM INTRANET_PA_C_IDVFRM F INNER JOIN INTRANET_PA_C_IDVAPR A ON A.IdvID = F.IdvID
	WHERE CYCLEID = '".IntegerSafe($cycleID)."';";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(7, $total);

//-- G7. Create Form Submission Batches - Appraisee
$sql = "INSERT INTO INTRANET_PA_T_FRMSUBSTG (RlsNo, RecordID, BatchID, AppRoleID, EditPrdFr, EditPrdTo, SecID, BatchOrder, IsActive, FillerID, GrpID, IdvID, InputBy_UserID, DateTimeInput, ModifiedBy_UserID, DateTimeModified)
	SELECT C.RlsNo, S.RecordID, B.BatchID, B.AppRoleID, B.EditPrdFr, B.EditPrdTo, B.SecID, BatchOrder, 1, S.UserID, S.GrpID, S.IdvID,'".IntegerSafe($_SESSION['UserID'])."',NOW(),'".IntegerSafe($_SESSION['UserID'])."',NOW()
	FROM INTRANET_PA_C_BATCH B
	INNER JOIN INTRANET_PA_T_FRMSUMSTG S ON S.CycleID = B.CycleID AND S.TemplateID =  B.TemplateID
	INNER JOIN INTRANET_PA_C_CYCLE C ON C.CycleID = S.CycleID
	LEFT JOIN INTRANET_PA_C_SPECIAL_BATCH_USER SBU ON C.CycleID=SBU.CycleID AND B.TemplateID=SBU.TemplateID AND S.UserID=SBU.UserID
	WHERE S.CycleID = '".IntegerSafe($cycleID)."' AND B.AppRoleID = 2 AND ((B.set_order=1 AND SBU.SetOrder IS NULL)  OR B.set_order=SBU.SetOrder);";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(8, $total);

//-- G8. Create Form Submission Batches - Appraiser
$sql = "INSERT INTO INTRANET_PA_T_FRMSUBSTG (RlsNo, RecordID, BatchID, AppRoleID, EditPrdFr, EditPrdTo, SecID, BatchOrder, IsActive, FillerID, GrpID, IdvID, InputBy_UserID, DateTimeInput, ModifiedBy_UserID, DateTimeModified)
	SELECT C.RlsNo, S.RecordID, B.BatchID, B.AppRoleID, B.EditPrdFr, B.EditPrdTo, B.SecID, BatchOrder, 1, S.AprID, S.GrpID, S.IdvID,'".IntegerSafe($_SESSION['UserID'])."',NOW(),'".IntegerSafe($_SESSION['UserID'])."',NOW()
	FROM INTRANET_PA_C_BATCH B
	INNER JOIN INTRANET_PA_T_FRMSUMSTG S ON S.CycleID = B.CycleID AND S.TemplateID =  B.TemplateID
	INNER JOIN INTRANET_PA_C_CYCLE C ON C.CycleID = S.CycleID
	LEFT JOIN INTRANET_PA_C_SPECIAL_BATCH_USER SBU ON C.CycleID=SBU.CycleID AND B.TemplateID=SBU.TemplateID AND S.UserID=SBU.UserID
	WHERE S.CycleID = '".IntegerSafe($cycleID)."' AND B.AppRoleID = 1 AND ((B.set_order=1 AND SBU.SetOrder IS NULL)  OR B.set_order=SBU.SetOrder);";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(9, $total);

//-- G9. Create Form Submission Batches - Special Appraising Roles
$sql = "INSERT INTO INTRANET_PA_T_FRMSUBSTG (RlsNo, RecordID, BatchID, AppRoleID, EditPrdFr, EditPrdTo, SecID, BatchOrder, IsActive, FillerID, GrpID, IdvID, InputBy_UserID, DateTimeInput, ModifiedBy_UserID, DateTimeModified)
	SELECT C.RlsNo, S.RecordID, B.BatchID, B.AppRoleID, B.EditPrdFr, B.EditPrdTo, B.SecID, BatchOrder, 1, R.UserID, S.GrpID, S.IdvID, '".IntegerSafe($_SESSION['UserID'])."',NOW(),'".IntegerSafe($_SESSION['UserID'])."',NOW()
	FROM INTRANET_PA_C_BATCH B
	INNER JOIN (SELECT CycleID, TemplateID, AppRoleID, UserID, set_order  FROM INTRANET_PA_C_FRMROL WHERE UserID IS NOT NULL AND CycleID = '".IntegerSafe($cycleID)."') R
	ON B.AppRoleID = R.AppRoleID AND B.TemplateID = R.TemplateID AND B.CycleID = R.CycleID AND B.set_order = R.set_order 
	INNER JOIN INTRANET_PA_T_FRMSUMSTG S ON S.CycleID = B.CycleID AND S.TemplateID =  B.TemplateID
	INNER JOIN INTRANET_PA_C_CYCLE C ON C.CycleID = S.CycleID
	LEFT JOIN INTRANET_PA_C_SPECIAL_BATCH_USER SBU ON C.CycleID=SBU.CycleID AND B.TemplateID=SBU.TemplateID AND S.UserID=SBU.UserID
	WHERE S.CycleID = '".IntegerSafe($cycleID)."' AND B.AppRoleID NOT IN (1,2) AND ((B.set_order=1 AND SBU.SetOrder IS NULL)  OR B.set_order=SBU.SetOrder);";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(10, $total);

//-- *** Migrate Staging to Transaction ***
//-- (Inputted data would be kept for those C_BATCH records if they have no change in AppRoleID and SecID)

//-- M1. Clear Past Mapping Information
$sql = "TRUNCATE TABLE INTRANET_PA_T_MATCHLOG;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(11, $total);

//-- M2. Set all form batches inactive (i.e., virtual deletion)
$sql = "UPDATE IGNORE INTRANET_PA_T_FRMSUB SET IsActive = 0, ModifiedBy_UserID = '".IntegerSafe($_SESSION['UserID'])."', DateTimeModified = NOW() WHERE IsActive = 1;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
$sql = "UPDATE IGNORE INTRANET_PA_T_FRMSUM SET MapKey = NULL, IsActive = 0, ModifiedBy_UserID = '".IntegerSafe($_SESSION['UserID'])."', DateTimeModified = NOW() WHERE IsActive = 1;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(12, $total);

//-- M3. Keep those with everything unchanged
$sql = "INSERT INTO INTRANET_PA_T_MATCHLOG (SL, SR, SB, TL, TR, TB, MapTyp)
SELECT STG.RlsNo SL, STG.RecordID SR, STG.BatchID SB, TRX.RlsNo TL, TRX.RecordID TR, TRX.BatchID TB, 'UNCHANGED' FROM
(SELECT S.CycleID, S.RecordID, S.TemplateID, S.UserID, S.AprID, B.RlsNo, B.BatchID, B.AppRoleID, B.EditPrdFr, B.EditPrdTo, B.SecID, B.BatchOrder, B.FillerID, B.GrpID, B.IdvID
		FROM INTRANET_PA_T_FRMSUMSTG S
		INNER JOIN INTRANET_PA_T_FRMSUBSTG B ON S.RecordID = B.RecordID AND S.CycleID = '".IntegerSafe($cycleID)."') STG
		INNER JOIN
		(SELECT S.CycleID, S.RecordID, S.TemplateID, S.UserID, S.AprID, B.RlsNo, B.BatchID, B.AppRoleID, B.EditPrdFr, B.EditPrdTo, B.SecID, B.BatchOrder, B.FillerID, B.GrpID, B.IdvID
				FROM INTRANET_PA_T_FRMSUM S
				INNER JOIN INTRANET_PA_T_FRMSUB B ON S.RecordID = B.RecordID AND S.CycleID = '".IntegerSafe($cycleID)."') TRX
				ON STG.CycleID = TRX.CycleID AND STG.TemplateID = TRX.TemplateID AND STG.UserID = TRX.UserID
				AND STG.BatchOrder = TRX.BatchOrder AND STG.SecID = TRX.SecID AND STG.AppRoleID = TRX.AppRoleID
				AND STG.AprID = TRX.AprID AND STG.FillerID = TRX.FillerID
				AND COALESCE(STG.GrpID,0) = COALESCE(TRX.GrpID,0) AND COALESCE(STG.IdvID,0) = COALESCE(TRX.IdvID,0)
				AND STG.EditPrdFr = TRX.EditPrdFr AND STG.EditPrdTo = TRX.EditPrdTo;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(13, $total);

$sql = "UPDATE IGNORE INTRANET_PA_T_FRMSUB T, INTRANET_PA_T_MATCHLOG M
	SET T.IsActive = 1, T.RlsNo = M.SL, T.BatchID = M.SB,
	T.ModifiedBy_UserID = '".IntegerSafe($_SESSION['UserID'])."', T.DateTimeModified = NOW()
	WHERE T.RlsNo = M.TL AND T.RecordID = M.TR AND T.BatchID = M.TB
	AND M.MapTyp = 'UNCHANGED' AND T.IsActive = 0;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(14, $total);

$sql = "UPDATE IGNORE INTRANET_PA_T_FRMSUM T, INTRANET_PA_T_MATCHLOG M
	SET T.IsActive = 1, T.MapKey = M.SR, T.ModifiedBy_UserID = '".IntegerSafe($_SESSION['UserID'])."', T.DateTimeModified = NOW()
	WHERE T.RecordID = M.TR
	AND M.MapTyp = 'UNCHANGED' AND T.IsActive = 0;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(15, $total);

//- M4. For same Appraisee + Appraiser + Special Appraising Roles + GrpID + IdvID + Batch definition (i.e., Appraising Role, Batch Order, Section) but Filling Period changed
//-- Update existing Form and Batch with new Filling Period
$sql = "INSERT INTO INTRANET_PA_T_MATCHLOG (SL, SR, SB, TL, TR, TB, MapTyp)
SELECT STG.RlsNo SL, STG.RecordID SR, STG.BatchID SB, TRX.RlsNo TL, TRX.RecordID TR, TRX.BatchID TB, 'FILLING PERIOD CHANGED' FROM
(SELECT S.CycleID, S.RecordID, S.TemplateID, S.UserID, S.AprID, B.RlsNo, B.BatchID, B.AppRoleID, B.EditPrdFr, B.EditPrdTo, B.SecID, B.BatchOrder, B.FillerID, B.GrpID, B.IdvID
		FROM INTRANET_PA_T_FRMSUMSTG S
		INNER JOIN INTRANET_PA_T_FRMSUBSTG B ON S.RecordID = B.RecordID AND S.CycleID = '".IntegerSafe($cycleID)."'
		LEFT JOIN INTRANET_PA_T_MATCHLOG M ON B.RlsNo = M.SL AND B.RecordID = M.SR AND B.BatchID = M.SB
		WHERE M.SR IS NULL) STG
		INNER JOIN
		(SELECT S.CycleID, S.RecordID, S.TemplateID, S.UserID, S.AprID, B.RlsNo, B.BatchID, B.AppRoleID, B.EditPrdFr, B.EditPrdTo, B.SecID, B.BatchOrder, B.FillerID, B.GrpID, B.IdvID
				FROM INTRANET_PA_T_FRMSUM S
				INNER JOIN INTRANET_PA_T_FRMSUB B ON S.RecordID = B.RecordID AND S.CycleID = '".IntegerSafe($cycleID)."'
				LEFT JOIN INTRANET_PA_T_MATCHLOG M ON B.RlsNo = M.TL AND B.RecordID = M.TR AND B.BatchID = M.TB
				WHERE M.TR IS NULL) TRX
				ON STG.CycleID = TRX.CycleID AND STG.TemplateID = TRX.TemplateID AND STG.UserID = TRX.UserID
				AND STG.BatchOrder = TRX.BatchOrder AND STG.SecID = TRX.SecID AND STG.AppRoleID = TRX.AppRoleID
				AND STG.AprID = TRX.AprID AND STG.FillerID = TRX.FillerID
				AND COALESCE(STG.GrpID,0) = COALESCE(TRX.GrpID,0) AND COALESCE(STG.IdvID,0) = COALESCE(TRX.IdvID,0) ;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(16, $total);

$sql = "UPDATE IGNORE INTRANET_PA_T_FRMSUB T, INTRANET_PA_T_FRMSUBSTG S, INTRANET_PA_T_MATCHLOG M
		SET T.IsActive = 1, T.RlsNo = M.SL, T.BatchID = M.SB, T.EditPrdFr = S.EditPrdFr, T.EditPrdTo = S.EditPrdTo,
		T.ModifiedBy_UserID = '".IntegerSafe($_SESSION['UserID'])."', T.DateTimeModified = NOW()
		WHERE T.RlsNo = M.TL AND T.RecordID = M.TR AND T.BatchID = M.TB AND S.RlsNo = M.SL  AND S.RecordID = M.SR AND S.BatchID = M.SB
		AND M.MapTyp = 'FILLING PERIOD CHANGED';";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(17, $total);

$sql = "UPDATE IGNORE INTRANET_PA_T_FRMSUM T, INTRANET_PA_T_FRMSUMSTG S, INTRANET_PA_T_MATCHLOG M
		SET T.IsActive = 1, T.MapKey = M.SR,
		T.ModifiedBy_UserID = '".IntegerSafe($_SESSION['UserID'])."', T.DateTimeModified = NOW()
		WHERE T.RecordID = M.TR AND S.RecordID = M.SR
		AND M.MapTyp = 'FILLING PERIOD CHANGED';";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(18, $total);

//-- M5. For same Appraisee + GrpID + IdvID + batch definition (i.e., Appraising Role, Batch Order, Section) but changed Appraiser (Single Appraiser Case)
//-- M5.1 Keep the Form, Appraisee and Special Appraising Roles' batch by replacing the linked AprID
$sql = "INSERT INTO INTRANET_PA_T_MATCHLOG (SL, SR, SB, TL, TR, TB, MapTyp)
	SELECT STG.RlsNo SL, STG.RecordID SR, STG.BatchID SB, TRX.RlsNo TL, TRX.RecordID TR, TRX.BatchID TB, '1-ON-1 APPRAISER CHANGE - EXISTING RECORDS' FROM
	(SELECT S.CycleID, S.RecordID, S.TemplateID, S.UserID, S.AprID, B.RlsNo, B.BatchID, B.AppRoleID, B.EditPrdFr, B.EditPrdTo, B.SecID, B.BatchOrder, B.FillerID, B.GrpID, B.IdvID
	   FROM INTRANET_PA_T_FRMSUMSTG S
	  INNER JOIN (SELECT UserID, TemplateID FROM INTRANET_PA_T_FRMSUMSTG WHERE CycleID = '".IntegerSafe($cycleID)."' GROUP BY UserID, TemplateID HAVING COUNT(RECORDID) = 1) A
	     ON S.UserID = A.UserID AND S.TemplateID = A.TemplateID
	  INNER JOIN INTRANET_PA_T_FRMSUBSTG B ON S.RecordID = B.RecordID AND S.CycleID = '".IntegerSafe($cycleID)."'
	   LEFT JOIN INTRANET_PA_T_MATCHLOG M ON B.RlsNo = M.SL AND B.RecordID = M.SR AND B.BatchID = M.SB
	  WHERE M.SR IS NULL) STG
	INNER JOIN
	(SELECT S.CycleID, S.RecordID, S.TemplateID, S.UserID, S.AprID, B.RlsNo, B.BatchID, B.AppRoleID, B.EditPrdFr, B.EditPrdTo, B.SecID, B.BatchOrder, B.FillerID, B.GrpID, B.IdvID
	   FROM INTRANET_PA_T_FRMSUM S
	  INNER JOIN (SELECT UserID, TemplateID FROM INTRANET_PA_T_FRMSUM WHERE CycleID = '".IntegerSafe($cycleID)."' GROUP BY UserID, TemplateID HAVING COUNT(RECORDID) = 1) A
	     ON S.UserID = A.UserID AND S.TemplateID = A.TemplateID
	  INNER JOIN INTRANET_PA_T_FRMSUB B ON S.RecordID = B.RecordID AND S.CycleID = '".IntegerSafe($cycleID)."'
	   LEFT JOIN INTRANET_PA_T_MATCHLOG M ON B.RlsNo = M.TL AND B.RecordID = M.TR AND B.BatchID = M.TB
	  WHERE M.TR IS NULL) TRX
	ON STG.CycleID = TRX.CycleID AND STG.TemplateID = TRX.TemplateID AND STG.UserID = TRX.UserID
	AND STG.BatchOrder = TRX.BatchOrder AND STG.SecID = TRX.SecID AND STG.AppRoleID = TRX.AppRoleID
	AND STG.FillerID = TRX.FillerID
	AND COALESCE(STG.GrpID,0) = COALESCE(TRX.GrpID,0) AND COALESCE(STG.IdvID,0) = COALESCE(TRX.IdvID,0) ;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(19, $total);

$sql = "UPDATE IGNORE INTRANET_PA_T_FRMSUB T, INTRANET_PA_T_FRMSUBSTG S, INTRANET_PA_T_MATCHLOG M
	   SET T.IsActive = 1, T.RlsNo = M.SL, T.BatchID = M.SB, T.EditPrdFr = S.EditPrdFr, T.EditPrdTo = S.EditPrdTo,
	       T.ModifiedBy_UserID = '".IntegerSafe($_SESSION['UserID'])."', T.DateTimeModified = NOW()
	 WHERE T.RlsNo = M.TL AND T.RecordID = M.TR AND T.BatchID = M.TB AND S.RlsNo = M.SL AND S.RecordID = M.SR AND S.BatchID = M.SB
	   AND M.MapTyp = '1-ON-1 APPRAISER CHANGE - EXISTING RECORDS';";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(20, $total);

$sql = "UPDATE IGNORE INTRANET_PA_T_FRMSUM T, INTRANET_PA_T_FRMSUMSTG S, INTRANET_PA_T_MATCHLOG M
	   SET T.IsActive = 1, T.MapKey = M.SR, T.AprID = S.AprID,
	       T.ModifiedBy_UserID = '".IntegerSafe($_SESSION['UserID'])."', T.DateTimeModified = NOW()
	 WHERE T.RecordID = M.TR AND S.RecordID = M.SR
	   AND M.MapTyp = '1-ON-1 APPRAISER CHANGE - EXISTING RECORDS';";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(21, $total);


//-- M5.2 Deactivate those batches with changed form filler (Ex-Appraiser / special appraising roles), and copy the new batch records from Staging
$sql = "INSERT INTO INTRANET_PA_T_FRMSUB (RlsNo, RecordID, BatchID, AppRoleID, EditPrdFr, EditPrdTo, SecID, IsActive, BatchOrder, FillerID, GrpID, IdvID, InputBy_UserID, DateTimeInput, ModifiedBy_UserID, DateTimeModified)
		SELECT B.RlsNo, A.TR, B.BatchID, B.AppRoleID, B.EditPrdFr, B.EditPrdTo, B.SecID, 1, B.BatchOrder, B.FillerID, B.GrpID, B.IdvID,".IntegerSafe($_SESSION['UserID']).",NOW(),".IntegerSafe($_SESSION['UserID']).",NOW()
		FROM INTRANET_PA_T_FRMSUMSTG S
		INNER JOIN (SELECT DISTINCT SR,TR FROM INTRANET_PA_T_MATCHLOG WHERE MapTyp = '1-ON-1 APPRAISER CHANGE - EXISTING RECORDS') A ON S.RecordID = A.SR
		INNER JOIN INTRANET_PA_T_FRMSUBSTG B ON S.RecordID = B.RecordID AND S.CycleID = '".IntegerSafe($cycleID)."'
		LEFT JOIN INTRANET_PA_T_MATCHLOG M ON B.RlsNo = M.SL AND B.RecordID = M.SR AND B.BatchID = M.SB
		WHERE M.SR IS NULL;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(22, $total);

$sql = "INSERT INTO INTRANET_PA_T_MATCHLOG (SL, SR, SB, TL, TR, TB, MapTyp)
		SELECT B.RlsNo, A.SR, B.BatchID, B.RlsNo, A.TR, B.BatchID, '1-ON-1 APPRAISER CHANGE - ADDED RECORDS'
		FROM INTRANET_PA_T_FRMSUMSTG S
		INNER JOIN (SELECT DISTINCT SR,TR FROM INTRANET_PA_T_MATCHLOG WHERE MapTyp = '1-ON-1 APPRAISER CHANGE - EXISTING RECORDS') A ON S.RecordID = A.SR
		INNER JOIN INTRANET_PA_T_FRMSUBSTG B ON S.RecordID = B.RecordID AND S.CycleID = '".IntegerSafe($cycleID)."'
		LEFT JOIN INTRANET_PA_T_MATCHLOG M ON B.RlsNo = M.SL AND B.RecordID = M.SR AND B.BatchID = M.SB
		WHERE M.SR IS NULL;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
$percent = intval(23/$total * 100);
recordProgress(23, $total);

//-- M7. Add new Forms and Batch records that never exist before
$sql = "INSERT INTO INTRANET_PA_T_MATCHLOG (SL, SR, SB, TL, TR, TB, MapTyp)
		SELECT B.RlsNo, B.RecordID, B.BatchID, B.RlsNo, 0, B.BatchID, 'NEW & RENEWED FORM RECORDS'
		FROM INTRANET_PA_T_FRMSUMSTG S
		INNER JOIN INTRANET_PA_T_FRMSUBSTG B ON S.RecordID = B.RecordID AND S.CycleID = '".IntegerSafe($cycleID)."'
		LEFT JOIN INTRANET_PA_T_MATCHLOG M ON B.RlsNo = M.SL AND B.RecordID = M.SR AND B.BatchID = M.SB
		WHERE M.SR IS NULL;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(24, $total);

$sql = "INSERT INTO INTRANET_PA_T_FRMSUM (CycleID, TemplateID, UserID, AprID, GrpID, IdvID, IsActive, MapKey, InputBy_UserID, DateTimeInput, ModifiedBy_UserID, DateTimeModified)
		SELECT S.CycleID, S.TemplateID, S.UserID, S.AprID, S.GrpID, S.IdvID, 1, S.RecordID, ".IntegerSafe($_SESSION['UserID']).",NOW(),".IntegerSafe($_SESSION['UserID']).",NOW()
		FROM INTRANET_PA_T_FRMSUMSTG S
		INNER JOIN (SELECT DISTINCT SR FROM INTRANET_PA_T_MATCHLOG WHERE MapTyp = 'NEW & RENEWED FORM RECORDS') M ON S.RecordID = M.SR
		WHERE S.CycleID = '".IntegerSafe($cycleID)."';";
////echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(25, $total);

$sql = "UPDATE IGNORE INTRANET_PA_T_MATCHLOG M, INTRANET_PA_T_FRMSUM S
		SET M.TR = S.RecordID
		WHERE M.SR = S.MapKey AND M.MapTyp = 'NEW & RENEWED FORM RECORDS';";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(26, $total);

$sql = "INSERT INTO INTRANET_PA_T_FRMSUB (RlsNo, RecordID, BatchID, AppRoleID, EditPrdFr, EditPrdTo, SecID, BatchOrder, IsActive, FillerID, GrpID, IdvID, InputBy_UserID, DateTimeInput, ModifiedBy_UserID, DateTimeModified)
		SELECT M.TL, M.TR, M.TB, B.AppRoleID, B.EditPrdFr, B.EditPrdTo, B.SecID, B.BatchOrder, 1, B.FillerID, B.GrpID, B.IdvID, ".IntegerSafe($_SESSION['UserID']).",NOW(),".IntegerSafe($_SESSION['UserID']).",NOW()
		FROM INTRANET_PA_T_FRMSUBSTG B
		INNER JOIN INTRANET_PA_T_MATCHLOG M ON B.RlsNo = M.SL AND B.RecordID = M.SR AND B.BatchID = M.SB AND MapTyp = 'NEW & RENEWED FORM RECORDS';";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(27, $total);

//-- M8. Map existing user form details to updated forms and batches (2017-08-16)
$sql = "UPDATE IGNORE INTRANET_PA_T_SLF_HDR D, INTRANET_PA_T_MATCHLOG M
	SET D.RlsNo = M.SL, D.BatchID = M.SB WHERE M.TL = D.RlsNo AND M.TR = D.RecordID AND M.TB = D.BatchID;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(28, $total);

$sql = "UPDATE IGNORE INTRANET_PA_T_SLF_DTL D, INTRANET_PA_T_MATCHLOG M
	SET D.RlsNo = M.SL, D.BatchID = M.SB WHERE M.TL = D.RlsNo AND M.TR = D.RecordID AND M.TB = D.BatchID;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(29, $total);

$sql = "UPDATE IGNORE INTRANET_PA_T_SLF_MTX D, INTRANET_PA_T_MATCHLOG M
	SET D.RlsNo = M.SL, D.BatchID = M.SB WHERE M.TL = D.RlsNo AND M.TR = D.RecordID AND M.TB = D.BatchID;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(30, $total);

$sql = "UPDATE IGNORE INTRANET_PA_T_PRE_CRS D, INTRANET_PA_T_MATCHLOG M
	SET D.RlsNo = M.SL, D.BatchID = M.SB WHERE M.TL = D.RlsNo AND M.TR = D.RecordID AND M.TB = D.BatchID;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(31, $total);

$sql = "UPDATE IGNORE INTRANET_PA_T_PRE_GEN D, INTRANET_PA_T_MATCHLOG M
	SET D.RlsNo = M.SL, D.BatchID = M.SB WHERE M.TL = D.RlsNo AND M.TR = D.RecordID AND M.TB = D.BatchID;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(32, $total);

$sql = "UPDATE IGNORE INTRANET_PA_T_PRE_OTH D, INTRANET_PA_T_MATCHLOG M
	SET D.RlsNo = M.SL, D.BatchID = M.SB WHERE M.TL = D.RlsNo AND M.TR = D.RecordID AND M.TB = D.BatchID;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(33, $total);

$sql = "UPDATE IGNORE INTRANET_PA_T_PRE_QUALI D, INTRANET_PA_T_MATCHLOG M
	SET D.RlsNo = M.SL,  D.BatchID = M.SB WHERE M.TL = D.RlsNo AND M.TR = D.RecordID AND M.TB = D.BatchID;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(34, $total);

$sql = "UPDATE IGNORE INTRANET_PA_T_PRE_TEACHCLASS D, INTRANET_PA_T_MATCHLOG M
	SET D.RlsNo = M.SL,  D.BatchID = M.SB WHERE M.TL = D.RlsNo AND M.TR = D.RecordID AND M.TB = D.BatchID;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(35, $total);

$sql = "UPDATE IGNORE INTRANET_PA_T_PRE_TEACHYEAR D, INTRANET_PA_T_MATCHLOG M
	SET D.RlsNo = M.SL, D.BatchID = M.SB WHERE M.TL = D.RlsNo AND M.TR = D.RecordID AND M.TB = D.BatchID;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(36, $total);

$sql = "UPDATE IGNORE INTRANET_PA_T_PRE_TASKASSESSMENT D, INTRANET_PA_T_MATCHLOG M
	SET D.RlsNo = M.SL, D.BatchID = M.SB WHERE M.TL = D.RlsNo AND M.TR = D.RecordID AND M.TB = D.BatchID;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(37, $total);

$sql = "UPDATE IGNORE INTRANET_PA_T_PRE_STUDENTTRAINING D, INTRANET_PA_T_MATCHLOG M
	SET D.RlsNo = M.SL, D.BatchID = M.SB WHERE M.TL = D.RlsNo AND M.TR = D.RecordID AND M.TB = D.BatchID;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(38, $total);

$sql = "UPDATE IGNORE INTRANET_PA_S_T_PRE_CPD D, INTRANET_PA_T_MATCHLOG M
	SET D.RlsNo = M.SL, D.BatchID = M.SB WHERE M.TL = D.RlsNo AND M.TR = D.RecordID AND M.TB = D.BatchID;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(39, $total);

$sql = "UPDATE IGNORE INTRANET_PA_S_ATTEDNACE_DATA D, INTRANET_PA_T_MATCHLOG M
	SET D.RlsNo = M.SL, D.BatchID = M.SB WHERE M.TL = D.RlsNo AND M.TR = D.RecordID AND M.TB = D.BatchID;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(40, $total);

$sql = "UPDATE IGNORE INTRANET_PA_T_PRE_CPD_HOURS D, INTRANET_PA_T_MATCHLOG M
	SET D.RlsNo = M.SL, D.BatchID = M.SB WHERE M.TL = D.RlsNo AND M.TR = D.RecordID AND M.TB = D.BatchID;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(41, $total);

//-- M9. Create PRE_GEN record for any new Pre-Defined Forms and batch
$sql = "INSERT INTO INTRANET_PA_T_PRE_GEN (RlsNo, RecordID, BatchID, NameChi, NameEng, Gender, DateOfBirth, AddressChi, AddressEng, PhoneNo, InputBy_UserID, DateTimeInput, ModifiedBy_UserID, DateTimeModified)
	SELECT B.RlsNo, B.RecordID, B.BatchID, U.ChineseName, U.EnglishName, U.Gender, U.DateOfBirth, U.Address, U.Address, U.MobileTelNo ,".IntegerSafe($_SESSION['UserID']).",NOW(),".IntegerSafe($_SESSION['UserID']).",NOW()
	FROM INTRANET_PA_T_FRMSUB B
	INNER JOIN INTRANET_PA_T_FRMSUM S ON S.RecordID = B.RecordID AND S.CycleID = '".IntegerSafe($cycleID)."' AND B.IsActive = 1
	INNER JOIN INTRANET_PA_S_FRMTPL P ON S.TemplateID = P.TemplateID AND P.TemplateType = 0
	INNER JOIN ".$appraisalConfig['INTRANET_USER']." U ON S.UserID = U.UserID
	LEFT JOIN INTRANET_PA_T_PRE_GEN D ON B.RlsNo = D.RlsNo AND B.RecordID = D.RecordID AND B.BatchID = D.BatchID
	WHERE D.RecordID IS NULL;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(42, $total);

//-- M10. Refresh the INTRANET_PA_T_MATCHLOG with latest Source and Target Release No and Batch ID (2017-08-16)
$sql = "UPDATE IGNORE INTRANET_PA_T_MATCHLOG SET TL = SL, TB = SB;";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(43, $total);

//-- M11. Bring back all observation records still exist but have not activated (2017-11-17)
$sql = "UPDATE IGNORE INTRANET_PA_T_FRMSUB SET IsActive = 1 WHERE ObsID IS NOT NULL AND IsActive = 0";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);
$result[] = array($a,$sql);
recordProgress(44, $total);

$sql = "SELECT RecordID FROM INTRANET_PA_T_FRMSUB WHERE ObsID IS NOT NULL";
$recordIDList = $connection->returnVector($sql);
$recordID = implode("','",$recordIDList);
if(!empty($recordID)){
	$sql = "UPDATE IGNORE INTRANET_PA_T_FRMSUM SET IsActive = 1 WHERE RecordID IN ('$recordID') AND IsActive = 0";
	//echo $sql."<br/><br/>";
	$a=$connection->db_db_query($sql);
	$result[] = array($a,$sql);
}
recordProgress(45, $total);

//-- M12. If form section has been submitted before by other users but new appraiser added in, sync their submission to the filling forms
$sql = "SELECT grpsum.GrpID, grpfrm.TemplateID FROM INTRANET_PA_C_GRPSUM grpsum 
INNER JOIN INTRANET_PA_C_GRPFRM grpfrm ON grpsum.GrpID=grpfrm.GrpID 
INNER JOIN INTRANET_PA_S_FRMTPL frmtpl ON grpfrm.TemplateID = frmtpl.TemplateID
WHERE grpsum.CycleID='".IntegerSafe($cycleID)."' AND frmtpl.TemplateType=1 AND frmtpl.IsActive=1 AND grpfrm.Incl=1";
$grpTemplateCombinations = $indexVar['libappraisal']->returnArray($sql);
foreach($grpTemplateCombinations as $combine){
	$sql = "SELECT DISTINCT UserID FROM INTRANET_PA_T_FRMSUM WHERE GrpID='".$combine['GrpID']."' AND CycleID='".IntegerSafe($cycleID)."' AND IsActive=1 AND TemplateID='".$combine['TemplateID']."'";
	$userSet = $indexVar['libappraisal']->returnVector($sql);
	foreach($userSet as $user){
		$sql = "SELECT frmsub.RlsNo, frmsub.RecordID, frmsub.BatchID, frmsub.SubDate, frmsub.FillerID FROM INTRANET_PA_T_FRMSUB frmsub
				INNER JOIN INTRANET_PA_T_FRMSUM frmsum ON frmsub.RecordID = frmsum.RecordID 
				WHERE frmsub.GrpID='".$combine['GrpID']."' AND frmsub.IsActive=1 AND frmsub.AppRoleID>=2 AND frmsum.IsActive=1 AND frmsum.UserID='".$user."' ORDER BY frmsub.BatchID ASC, frmsub.SubDate DESC  ";
		$batchSet = $indexVar['libappraisal']->returnArray($sql);
		$isAllSynced = true;
		$sample = array();
		foreach($batchSet as $batchData){
			if($batchData['SubDate']=="" || $batchData['SubDate']==null){
				$isAllSynced = false;
			}else{
				$sample[$batchData['BatchID']] = $batchData;
			}
		}
		if($isAllSynced==false){
			foreach($batchSet as $batchData){
				if($batchData['SubDate']=="" && !empty($sample[$batchData['BatchID']]) && $batchData['FillerID']==$sample[$batchData['BatchID']]['FillerID']){
					$sql = "INSERT INTO INTRANET_PA_T_SLF_HDR 
							SELECT '".$batchData['RlsNo']."', '".$batchData['RecordID']."', '".$batchData['BatchID']."', 
							YearClassID, SubjectID, TeachLang, InputBy_UserID, DateTimeInput, ModifiedBy_UserID, DateTimeModified
							FROM INTRANET_PA_T_SLF_HDR WHERE RlsNo='".$sample[$batchData['BatchID']]['RlsNo']."' AND RecordID='".$sample[$batchData['BatchID']]['RecordID']."' AND BatchID='".$sample[$batchData['BatchID']]['BatchID']."';";
					$a=$connection->db_db_query($sql);
					
					$sql = "INSERT INTO INTRANET_PA_T_SLF_DTL  
							SELECT '".$batchData['RlsNo']."', '".$batchData['RecordID']."', '".$batchData['BatchID']."', 
							QID, QAnsID, MarkID, Score, TxtAns, DateAns, Remark, InputBy_UserID, DateTimeInput, ModifiedBy_UserID, DateTimeModified
							FROM INTRANET_PA_T_SLF_DTL WHERE RlsNo='".$sample[$batchData['BatchID']]['RlsNo']."' AND RecordID='".$sample[$batchData['BatchID']]['RecordID']."' AND BatchID='".$sample[$batchData['BatchID']]['BatchID']."';";
					$a=$connection->db_db_query($sql);
					
					$sql = "INSERT INTO INTRANET_PA_T_SLF_MTX   
							SELECT '".$batchData['RlsNo']."', '".$batchData['RecordID']."', '".$batchData['BatchID']."', 
							QID, MtxFldID, Score, TxtAns, DateAns, Remark, InputBy_UserID, DateTimeInput, ModifiedBy_UserID, DateTimeModified
							FROM INTRANET_PA_T_SLF_MTX  WHERE RlsNo='".$sample[$batchData['BatchID']]['RlsNo']."' AND RecordID='".$sample[$batchData['BatchID']]['RecordID']."' AND BatchID='".$sample[$batchData['BatchID']]['BatchID']."';";
					$a=$connection->db_db_query($sql);
					
					$sql = "UPDATE IGNORE INTRANET_PA_T_FRMSUB SET SubDate='".$sample[$batchData['BatchID']]['SubDate']."', IsLastSub=1  WHERE RlsNo='".$batchData['RlsNo']."' AND RecordID='".$batchData['RecordID']."' AND BatchID='".$batchData['BatchID']."';";
					$a=$connection->db_db_query($sql);					
				}
			}
		}	
	}
}
recordProgress(46, $total);

// log obs form release		
$logData = array();
$logData['CycleID'] = $cycleID;
$logData['Function'] = 'Form Release';
$logData['RecordType'] = 'RELEASE';
$recordDetail[] = array(
					  	"DisplayName"=>$Lang['Appraisal']['BtbRlsAppForm'],
					  	"Message"=>''
					  );
$logData['RecordDetail'] = $json->encode($recordDetail);
$result = $indexVar['libappraisal']->addCycleSettingLog($logData);
//$connection->RollBack_Trans();
$returnMsgKey = (in_array(false,$result))? 'UpdateUnsuccess' : 'UpdateSuccess';
//header($returnPath);
?>