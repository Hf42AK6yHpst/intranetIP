<?php
// ============================== Related Tables ==============================
// INTRANET_PA_T_FRMSUM,INTRANET_PA_S_FRMTPL,INTRANET_PA_C_GRPSUM,INTRANET_PA_C_IDVFRM,INTRANET_PA_C_BATCH,INTRANET_PA_T_FRMSUB,INTRANET_PA_S_APPROL
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
### check access right


$currentDate = date("Y-m-d");

$arrCookies = array();
$arrCookies[] = array("records_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}

/*
 $ltimetable = new Timetable();
 $curTimetableId = $ltimetable->Get_Current_Timetable();
 */

# Page Title
$titleBar = "<div style=\"display:inline;float:left\"><span class=\"contenttitle\" style=\"vertical-align:bottom\">".$Lang['Appraisal']['AppraisalPeriod']."</span></div>";;
if($cycleID!=0){
	//$titleBar .= "<div style=\"display:inline;float:right;\" class=\"online_help\"><a href=\"javascript:goDownloadAttachment('".$cycleID."')\" title=\"".$Lang['Appraisal']['TitleBar']['DownloadDocument']."\" onclick=\"goDownload(".$cycleID.")\" class=\"help_icon\" id=\"downloadIcon\">";
	//$titleBar .= "<span style=\"font-size:10px\">".$Lang['Appraisal']['TitleBar']['DownloadDocument']."</span></div>";
}

$TAGS_OBJ[] = array($titleBar);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libappraisal_ui']->Include_Thickbox_JS_CSS();

$connection = new libgeneralsettings();
//======================================================================== Header ========================================================================//
### navigation
$navigationAry[] = array($Lang['Appraisal']['AppraisalPeriod'], 'javascript: goBack();');
$navigationAry[] = array($Lang['Appraisal']['AppraisalPeriodCopy']);
$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);


$sql = "SELECT CycleID,DescrChi,DescrEng,AcademicYearID FROM INTRANET_PA_C_CYCLE ";
$cycleList = $connection->returnResultSet($sql);

$academicYearWithCycle = array();
foreach($cycleList as $cycle){
	$academicYearWithCycle[] = $cycle['AcademicYearID'];
}

$academicYearList=$indexVar['libappraisal']->getAllAcademicYear("");
foreach($academicYearList as $idx=>$academicYear){
	if(in_array($academicYear['AcademicYearID'],$academicYearWithCycle)){
		unset($academicYearList[$idx]);
	}
}
$academicYearList = array_splice($academicYearList,0);
//======================================================================== 考績週期基本設定 ========================================================================//
$x = "";
$x .= "<table class=\"form_table_v30\">";
$x .= "<tr><td class=\"field_title\">".$indexVar['libappraisal_ui']->RequiredSymbol().$Lang['Appraisal']['Cycle']['CopyFromCurrentCycle']."</td>";
$x .= "<td>".getSelectByAssoArray($indexVar['libappraisal']->cnvGeneralArrToSelect($cycleList, "CycleID","DescrChi","DescrEng"), $selectionTags='id="CycleID" name="CycleID"', $SelectedType='', $all=0, $noFirst=0);
$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('AcademicYearIDEmptyWarnDiv', $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv');
$x .= "</td></tr>";
$x .= "<tr><td class=\"field_title\">".$indexVar['libappraisal_ui']->RequiredSymbol().$Lang['Appraisal']['Cycle']['CopyToAcademicYear']."</td>";
$x .= "<td>".getSelectByAssoArray($indexVar['libappraisal']->cnvGeneralArrToSelect($academicYearList, "AcademicYearID","YearNameB5","YearNameEN"), $selectionTags='id="AcademicYearID" name="AcademicYearID"', $SelectedType='', $all=0, $noFirst=0);
$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('AcademicYearIDEmptyWarnDiv', $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv');
$x .= "</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['Cycle']['DescrEng']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTAREA("DescrEng", $taContents=$a[0]["DescrEng"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['Cycle']['DescrChi']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTAREA("DescrChi", $taContents=$a[0]["DescrChi"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['Cycle']['CycleStart']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_DATE_PICKER('CycleStart',$a[0]["CycleStart"],$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('AppPrdFr')",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum");
$x .= "<span style=\"color:#999999;margin-left:20px\"><i>".$Lang['Appraisal']['Cycle']['AfterCycleStart']."</i></span>";
$x .= "</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['Cycle']['CycleClose']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_DATE_PICKER('CycleClose',$a[0]["CycleClose"],$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('AppPrdFr')",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum");
$x .= "<span style=\"color:#999999;margin-left:20px\"><i>".$Lang['Appraisal']['Cycle']['AfterCycleClose']."</i></span><br/>";
$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('DateWarnDiv', $Lang['Appraisal']['Cycle']['DateCheck'], $Class='warnMsgDiv')."\r\n";
$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('DateWarnDiv2', $Lang['Appraisal']['Cycle']['DateCheckOverPeriod'], $Class='warnMsgDiv')."\r\n";
$x .= "</td></tr>";
$x .= "</table>";
$x .="<span style=\"color:#999999\">".sprintf($Lang['Appraisal']['Cycle']['Remark'],"<span style=\"color:#FF0000\">*</span>")."</span><br/><br/>";
$x .= "<br/>";
$htmlAry['contentTbl1'] = $x;

### Get Btn
$BackBtn = $indexVar['libappraisal_ui']->GET_ACTION_BTN($button_back, "button", "javascript:goBack()","back"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
$NextBtn = $indexVar['libappraisal_ui']->GET_ACTION_BTN($button_continue, "submit", "","submit2"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
$Btn = $NextBtn."&nbsp;".$BackBtn."&nbsp;".$CancelBtn;
?>