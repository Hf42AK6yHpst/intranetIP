<?php 
// using: Paul.
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

$connection = new libgeneralsettings();
$json = new JSON_obj();

$formType=$_POST["type"]; 
$returnPath=$_POST["returnPath"];
$saveSuccess = true;
if($formType=="InfoSave"){
	$lo = new libfilesystem();
	
	$cycleID=$_POST["CycleID"];
	$sql="SELECT CycleID FROM INTRANET_PA_C_CYCLE WHERE CycleID=".IntegerSafe($cycleID).";";
	$a=$connection->returnResultSet($sql);
	$academicYearID=$_POST["AcademicYearID"];
	$appPrdFr=($_POST["AppPrdFr"]!=null)?"'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["AppPrdFr"])."'":"NULL";
	$appPrdTo=($_POST["AppPrdTo"]!=null)?"'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["AppPrdTo"])."'":"NULL";
	$descrChi=($_POST["DescrChi"]!=null)?"'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["DescrChi"])."'":"''";
	$descrEng=($_POST["DescrEng"]!=null)?"'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["DescrEng"])."'":"''";
	$cycleStart=($_POST["CycleStart"]!=null)?"'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["CycleStart"])." 00:00:00'":"NULL";
	$cycleClose=($_POST["CycleClose"]!=null)?"'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["CycleClose"])." 23:59:59'":"NULL";
	//$guideChi=($_POST["GuideChi"]!=null)?"'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["GuideChi"])."'":"''";
	//$guideEng=($_POST["GuideEng"]!=null)?"'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["GuideEng"])."'":"''";
	//$guideFileName=($_FILES['GuideFile']['name']!=null)?"'".$_FILES['GuideFile']['name']."'":"''";
	$guideFileName=$_FILES['GuideFile']['name'];
	//echo $guideFileName."<br/><br/>";
	//$uploadPath="/home/eclass/intranetdata/appraisal/upload";;
	$uploadPath = $intranet_root.'/file/appraisal/upload';
	//echo $uploadPath."<br/><br/>";
	if (!file_exists($uploadPath))
		$lo->folder_new($uploadPath);	
	$TargetFilePath = stripslashes($uploadPath."/".$guideFileName);
		
	if(sizeof($a)>0){
		$sql="SELECT * FROM INTRANET_PA_C_CYCLE WHERE CycleID=".IntegerSafe($cycleID).";";
		$originalCycleData=current($connection->returnResultSet($sql));
		$sql="UPDATE INTRANET_PA_C_CYCLE SET AcademicYearID=".$academicYearID.",AppPrdFr=".$appPrdFr.",AppPrdTo=".$appPrdTo.",DescrChi=".$descrChi.",DescrEng=".$descrEng.",";
		$sql.="CycleStart=".$cycleStart.",CycleClose=".$cycleClose.",GuideChi=NULL,GuideEng=NULL,GuideFile='".$guideFileName."',";
		$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
		$sql.="WHERE CycleID=".$cycleID.";";
		//echo $sql."<br/><br/>";
		$a=$connection->db_db_query($sql);		
		$lo->lfs_move($GuideFile, $TargetFilePath);
		
		// Update Edit Period of the Obs Form under the cycle
		$sql = "SELECT DISTINCT frmsub.RecordID FROM INTRANET_PA_T_FRMSUB frmsub
				INNER JOIN INTRANET_PA_T_FRMSUM frmsum ON frmsub.RecordID = frmsum.RecordID
				WHERE frmsum.CycleID='".$cycleID."' AND frmsub.ObsID IS NOT NULL";
		$obsRecordList = $connection->returnVector($sql);
		$sql = "UPDATE INTRANET_PA_T_FRMSUB SET EditPrdFr=".$cycleStart.", EditPrdTo=".$cycleClose." WHERE RecordID IN ('".implode("','",$obsRecordList)."')";
		$a=$connection->db_db_query($sql);
		
		// Log changes
		$recordDetail = array();
		if($originalCycleData['AcademicYearID'] != $academicYearID){
			$recordDetail[] = array(
								"Parameter"=>"AcademicYearID",
							  	"DisplayName"=>$Lang['Appraisal']['Cycle']['AcademicYear'],
							  	"Original"=>$originalCycleData['AcademicYearID'],
							  	"New"=>$academicYearID,
							  	"MapWithTable"=>"ACADEMIC_YEAR",
							  	"MapDisplay"=>"YearNameEN"
							  );
		}
		if("'".$originalCycleData['AppPrdFr']."'" != $appPrdFr && !($appPrdFr==="NULL" && $originalCycleData['AppPrdFr']===null)){			
			$recordDetail[] = array(
								"Parameter"=>"AppPrdFr",
							  	"DisplayName"=>$Lang['Appraisal']['Cycle']['AppPrd'],
							  	"Original"=>$originalCycleData['AppPrdFr'],
							  	"New"=>$appPrdFr
							  );
		}
		if("'".$originalCycleData['AppPrdTo']."'" != $appPrdTo && !($appPrdTo==="NULL" && $originalCycleData['AppPrdTo']===null)){			
			$recordDetail[] = array(
								"Parameter"=>"AppPrdTo",
							  	"DisplayName"=>$Lang['Appraisal']['Cycle']['AppPrd'],
							  	"Original"=>$originalCycleData['AppPrdTo'],
							  	"New"=>$appPrdTo
							  );
		}
		if("'".$originalCycleData['DescrChi']."'" != $descrChi){
			$recordDetail[] = array(
								"Parameter"=>"DescrChi",
							  	"DisplayName"=>$Lang['Appraisal']['Cycle']['DescrChi'],
							  	"Original"=>$originalCycleData['DescrChi'],
							  	"New"=>$descrChi
							  );			
		}
		if("'".$originalCycleData['DescrEng']."'" != $descrEng){
			$recordDetail[] = array(
								"Parameter"=>"DescrEng",
							  	"DisplayName"=>$Lang['Appraisal']['Cycle']['DescrEng'],
							  	"Original"=>$originalCycleData['DescrEng'],
							  	"New"=>$descrEng
							  );
		}
		if("'".$originalCycleData['CycleStart']."'" != $cycleStart){
			$recordDetail[] = array(
								"Parameter"=>"CycleStart",
							  	"DisplayName"=>$Lang['Appraisal']['Cycle']['CycleStart'],
							  	"Original"=>$originalCycleData['CycleStart'],
							  	"New"=>$cycleStart
							  );
		}
		if("'".$originalCycleData['CycleClose']."'" != $cycleClose){
			$recordDetail[] = array(
								"Parameter"=>"CycleClose",
							  	"DisplayName"=>$Lang['Appraisal']['Cycle']['CycleClose'],
							  	"Original"=>$originalCycleData['CycleClose'],
							  	"New"=>$cycleClose
							  );
		}
		if($originalCycleData['GuideFile'] != $guideFileName){
			$recordDetail[] = array(
								"Parameter"=>"GuideFile",
							  	"DisplayName"=>$Lang['Appraisal']['Cycle']['GuideFile'],
							  	"Original"=>$originalCycleData['GuideFile'],
							  	"New"=>$guideFileName
							  );
		}			
		$logData = array();
		$logData['CycleID'] = $cycleID;
		$logData['Function'] = 'Cycle Info';
		$logData['RecordType'] = 'UPDATE';
		if(empty($recordDetail)){
			$logData['RecordDetail'] = 'UNCHANGED';
		}else{	
			$logData['RecordDetail'] = $json->encode($recordDetail);
		}
		$result = $indexVar['libappraisal']->addCycleSettingLog($logData);	
	}
	else{
		$sql="INSERT INTO INTRANET_PA_C_CYCLE(AcademicYearID,AppPrdFr,AppPrdTo,DescrChi,DescrEng,CycleStart,CycleClose,GuideChi,GuideEng,GuideFile,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified )";
		$sql.="SELECT * FROM(SELECT ".$academicYearID." as AcademicYearID,".$appPrdFr." as AppPrdFr,".$appPrdTo." as AppPrdTo,".$descrChi." as DescrChi,".$descrEng." as DescrEng,";
		$sql.=$cycleStart." as CycleStart,".$cycleClose." as CycleClose,NULL as GuideChi,NULL as GuideEng,'".$guideFileName."' as GuideFile,";		
		$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
		$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";
		//echo $sql."<br/><br/>";
		$a=$connection->db_db_query($sql);
		
		$lo->lfs_move($GuideFile, $TargetFilePath);
		$sql="SELECT CycleID FROM INTRANET_PA_C_CYCLE WHERE AcademicYearID=".$academicYearID.";";
		$a=$connection->returnResultSet($sql);
		$getCycleID=$a[0]["CycleID"];
		$returnPath=sprintf($returnPath,$getCycleID);		
		
		// Log changes
		$recordDetail = array();
		$recordDetail[] = array(
								"Parameter"=>"AcademicYearID",
							  	"DisplayName"=>$Lang['Appraisal']['Cycle']['AcademicYear'],
							  	"New"=>$academicYearID,
							  	"MapWithTable"=>"ACADEMIC_YEAR",
							  	"MapDisplay"=>"YearNameEN"
							  );
		$recordDetail[] = array(
								"Parameter"=>"AppPrdFr",
							  	"DisplayName"=>$Lang['Appraisal']['Cycle']['AppPrd'],
							  	"New"=>$appPrdFr
							  );
		$recordDetail[] = array(
								"Parameter"=>"AppPrdTo",
							  	"DisplayName"=>$Lang['Appraisal']['Cycle']['AppPrd'],
							  	"New"=>$appPrdTo
							  );
		$recordDetail[] = array(
								"Parameter"=>"DescrChi",
							  	"DisplayName"=>$Lang['Appraisal']['Cycle']['DescrChi'],
							  	"New"=>$descrChi
							  );
		$recordDetail[] = array(
								"Parameter"=>"DescrEng",
							  	"DisplayName"=>$Lang['Appraisal']['Cycle']['DescrEng'],
							  	"New"=>$descrEng
							  );
		$recordDetail[] = array(
								"Parameter"=>"CycleStart",
							  	"DisplayName"=>$Lang['Appraisal']['Cycle']['CycleStart'],
							  	"New"=>$cycleStart
							  );
		$recordDetail[] = array(
								"Parameter"=>"CycleClose",
							  	"DisplayName"=>$Lang['Appraisal']['Cycle']['CycleClose'],
							  	"New"=>$cycleClose
							  );
		$recordDetail[] = array(
								"Parameter"=>"GuideFile",
							  	"DisplayName"=>$Lang['Appraisal']['Cycle']['GuideFile'],
							  	"New"=>$guideFileName
							  );			
		$logData = array();
		$logData['CycleID'] = $a[0]["CycleID"];
		$logData['Function'] = 'Cycle Info';
		$logData['RecordType'] = 'NEW';
		$logData['RecordDetail'] = $json->encode($recordDetail);
		$result = $indexVar['libappraisal']->addCycleSettingLog($logData);		
	}
}
else if($formType=="ArrSave"){
	$templateID=$_POST["TemplateID"];
	$cycleID=$_POST["CycleID"];
	$appRoleIDArr=explode(",",$_POST["AppRoleIDStr"]);
	$delBatchIDArr=explode(",",$_POST["DelBatchIDStr"]);	
	$batchIDArr=explode(",",$_POST["BatchIDStr"]);
	$specialSetName = explode(",",$_POST["SpecialSetName"]);
	$totalSpecialSet = intval($_POST["TotalSpecialSet"]);
	
	$sql="SELECT TemplateID,RelatedTemplates FROM INTRANET_PA_C_FRMSEL WHERE CycleID=".$cycleID." AND TemplateID=".$templateID.";";
	$originalData=$connection->returnResultSet($sql);
	if(sizeof($originalData)==0){
		$sql="INSERT INTO INTRANET_PA_C_FRMSEL(CycleID,TemplateID,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
		$sql.="SELECT * FROM(SELECT ".$cycleID." as CycleID,".$templateID." as TemplateID,";
		$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
		$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";
		//echo $sql."<br/><br/>";
		$a=$connection->db_db_query($sql);
		
		// log creation
		$sql = "SELECT ".$indexVar['libappraisal']->getLangSQL("FrmTitleChi","FrmTitleEng")." as FormName, ".$indexVar['libappraisal']->getLangSQL("FrmCodChi","FrmCodEng")." as FormCode From INTRANET_PA_S_FRMTPL WHERE TemplateID='".$templateID."'";		
		$templateData=current($connection->returnResultSet($sql));
		$frmName = ($templateData['FormCode']=="")?$templateData['FormName']:$templateData['FormName']."-".$templateData['FormCode'];
		$logData = array();
		$recordDetail = array();
		$logData['CycleID'] = $cycleID;
		$logData['Function'] = 'Cycle Template Setting';
		$logData['RecordType'] = 'NEW';
		$recordDetail[] = array(
								"Parameter"=>"TemplateID",
							  	"DisplayName"=>$Lang['Appraisal']['Cycle']['AccTemplate'],
							  	"New"=>$frmName."(ID: ".$templateID.")"
							  );
		$logData['RecordDetail'] = $json->encode($recordDetail);
		$result = $indexVar['libappraisal']->addCycleSettingLog($logData);	
	}
	//======================================================================== 相關評審表格  ========================================================================//
	if(empty($_POST['relatedTemplates'])){
		$relatedTemplateList = "NULL";
	}else{
		$relatedTemplateList = "'".implode(",",$_POST['relatedTemplates'])."'";
	}
	$sql = "UPDATE INTRANET_PA_C_FRMSEL SET RelatedTemplates=".$relatedTemplateList." WHERE CycleID=".$cycleID." AND TemplateID=".$templateID."";
	$a=$connection->db_db_query($sql);
	
	if($originalData[0]['RelatedTemplates']!=$_POST['relatedTemplates']){
		// log related forms		
		$logData = array();
		$recordDetail = array();
		$logData['CycleID'] = $cycleID;
		$logData['Function'] = 'Cycle Template Related Forms';
		$logData['RecordType'] = 'UPDATE';
		$sql = "SELECT ".$indexVar['libappraisal']->getLangSQL("FrmTitleChi","FrmTitleEng")." as FormName, ".$indexVar['libappraisal']->getLangSQL("FrmCodChi","FrmCodEng")." as FormCode From INTRANET_PA_S_FRMTPL WHERE TemplateID='".$templateID."'";		
		$templateData=current($connection->returnResultSet($sql));
		$frmName = ($templateData['FormCode']=="")?$templateData['FormName']:$templateData['FormName']."-".$templateData['FormCode'];
		$recordDetail[] = array(
									"Parameter"=>"TemplateID",
								  	"DisplayName"=>$Lang['Appraisal']['Cycle']['AccTemplate'],
								  	"Message"=>$frmName."(ID: ".$templateID.")"
								  );
		if(empty($originalData[0]['RelatedTemplates'])){
			$originalFrmName = 'NULL';
		}else{
			$sql = "SELECT TemplateID,".$indexVar['libappraisal']->getLangSQL("FrmTitleChi","FrmTitleEng")." as FormName, ".$indexVar['libappraisal']->getLangSQL("FrmCodChi","FrmCodEng")." as FormCode From INTRANET_PA_S_FRMTPL WHERE TemplateID IN (".$originalData[0]['RelatedTemplates'].")";		
			$templateData=$connection->returnResultSet($sql);
			$originalFrmNameArr = array();
			foreach($templateData as $tpl){
				$originalFrmNameArr[] = (($tpl['FormCode']=="")?$tpl['FormName']:$tpl['FormName']."-".$tpl['FormCode'])."(ID: ".$tpl['TemplateID'].")";
			}
			$originalFrmName = implode("<br>",$originalFrmNameArr);
		}
		if(empty($_POST['relatedTemplates'])){
			$newFrmName = 'NULL';
		}else{	
			$sql = "SELECT TemplateID,".$indexVar['libappraisal']->getLangSQL("FrmTitleChi","FrmTitleEng")." as FormName, ".$indexVar['libappraisal']->getLangSQL("FrmCodChi","FrmCodEng")." as FormCode From INTRANET_PA_S_FRMTPL WHERE TemplateID IN ('".implode("','",$_POST['relatedTemplates'])."')";		
			$templateData=$connection->returnResultSet($sql);
			$newFrmNameArr = array();
			foreach($templateData as $tpl){
				$newFrmNameArr[] = (($tpl['FormCode']=="")?$tpl['FormName']:$tpl['FormName']."-".$tpl['FormCode'])."(ID: ".$tpl['TemplateID'].")";
			}
			$newFrmName = implode("<br>",$newFrmNameArr);
		}
		$recordDetail[] = array(
									"Parameter"=>"RelatedTemplates",
								  	"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['RelatedApprsialForm'],
								  	"Original"=>$originalFrmName,
								  	"New"=>$newFrmName
								  );
		$logData['RecordDetail'] = $json->encode($recordDetail);
		$result = $indexVar['libappraisal']->addCycleSettingLog($logData);
	}							  
	//======================================================================== 特別考績角色職員編配  ========================================================================//
	for($j=1;$j<=$totalSpecialSet+1;$j++){
	    $nameArr=explode(",",$_POST["SpecialSetName"]);
    	if($j == 1){
    		$newLogUserPair = array();
    		$originalUserPair = array();
        	for($i=0;$i<sizeof($appRoleIDArr);$i++){
        	    $userID=($_POST["UserID_".$j."_".$i]!=null)?$_POST["UserID_".$j."_".$i]:"NULL";
        	    if($userID!="NULL"){
        	    	$newLogUserPair[] = array('AppRoleID'=>IntegerSafe($appRoleIDArr[$i]),'UserID'=>$userID);
        	    }
        		$sql="SELECT AppRoleID,UserID FROM INTRANET_PA_C_FRMROL WHERE CycleID=".IntegerSafe($cycleID)." AND TemplateID=".IntegerSafe($templateID)." AND AppRoleID=".IntegerSafe($appRoleIDArr[$i])." AND set_order = 1;";
        		//echo $sql."<br/><br/>";
        		$approleResult=$connection->returnResultSet($sql);
        		if(sizeof($approleResult)>0){
        			$originalUserPair[] = $approleResult[0];
        			$sql="UPDATE INTRANET_PA_C_FRMROL SET UserID=".$userID.",";
        			$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
        			$sql.="WHERE CycleID=".IntegerSafe($cycleID)." AND TemplateID=".IntegerSafe($templateID)." AND AppRoleID=".IntegerSafe($appRoleIDArr[$i])." AND set_order = 1;";
                    //echo $sql."<br/><br/>";
        			$a=$connection->db_db_query($sql);
        		} else{       	    	
        	    	if($userID=="NULL"){
        	    		continue;
        	    	}
        		    $sql="INSERT INTO INTRANET_PA_C_FRMROL(CycleID,TemplateID,AppRoleID,UserID,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
        		    $sql.="SELECT * FROM(SELECT ".$cycleID." as CycleID,".$templateID." as TemplateID,".IntegerSafe($appRoleIDArr[$i])." as AppRoleID,".$userID." as UserID,";
        		    $sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
        		    $sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";
        		    //echo $sql."<br/><br/>";
        		    $a=$connection->db_db_query($sql);
        		}
        	}        	
        	
    	    if($newLogUserPair != $originalUserPair){
		    	// log special role			
				$logData = array();
				$recordDetail = array();
				$logData['CycleID'] = $cycleID;
				$logData['Function'] = 'Cycle Template Special Role ';
				$logData['RecordType'] = (sizeof($approleResult)>0)?'UPDATE':'NEW';
				$recordDetail[] = array(
									  	"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['SpecialFormSettingName'],
									  	"Message"=>"Default"
									  );
				$recordDetail[] = array(
										"Parameter"=>"TemplateID",
									  	"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['ApprsialForm'],
									  	"Original"=>$templateID,
									  	"MapWithTable"=>"INTRANET_PA_S_FRMTPL",
									  	"MapDisplay"=>"FrmTitleEng"
									  );
				$newMsg = "<br>";
				foreach($newLogUserPair as $userRole){
					$sql = "SELECT NameChi,NameEng FROM INTRANET_PA_S_APPROL WHERE AppRoleID='".$userRole['AppRoleID']."' ";
					$roleResult = current($connection->returnResultSet($sql));				 
					$sql = "SELECT EnglishName,ChineseName FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID='".$userRole['UserID']."'";
					$userResult = current($connection->returnResultSet($sql));
					$newMsg .= Get_Lang_Selection($roleResult['NameChi'],$roleResult['NameEng']).": ".Get_Lang_Selection($userResult['ChineseName'],$userResult['EnglishName'])."<br>";
				}
				if(empty($originalUserPair)){							  
					$recordDetail[] = array(
										  	"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['SpecialRole'],
										  	"New"=>$newMsg
										  );
				}else{
					$oriMsg = "<br>";
					foreach($originalUserPair as $userRole){
						$sql = "SELECT NameChi,NameEng FROM INTRANET_PA_S_APPROL WHERE AppRoleID='".$userRole['AppRoleID']."' ";
						$roleResult = current($connection->returnResultSet($sql));				 
						$sql = "SELECT EnglishName,ChineseName FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID='".$userRole['UserID']."'";
						$userResult = current($connection->returnResultSet($sql));
						$oriMsg .= Get_Lang_Selection($roleResult['NameChi'],$roleResult['NameEng']).": ".Get_Lang_Selection($userResult['ChineseName'],$userResult['EnglishName'])."<br>";
					}					  
					$recordDetail[] = array(
										  	"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['SpecialRole'],
										  	"Original"=>$oriMsg,
										  	"New"=>$newMsg
										  );
				}
				$logData['RecordDetail'] = $json->encode($recordDetail);
				$result = $indexVar['libappraisal']->addCycleSettingLog($logData);
    	    }
    	}
    	if($j > 1){    	 		
    		$setName = ($nameArr[$j-1]=="undefined" || $nameArr[$j-1]=="")?"NULL":"'".$nameArr[$j-1]."'";
    		$newLogUserPair = array();
    		$originalUserPair = array();
    	    for($i=0;$i<sizeof($appRoleIDArr);$i++){
        	    $userID=($_POST["UserID_".$j."_".$i]!=null)?$_POST["UserID_".$j."_".$i]:"NULL";
        	    if($userID!="NULL"){
        	    	$newLogUserPair[] = array('AppRoleID'=>IntegerSafe($appRoleIDArr[$i]),'UserID'=>$userID);
        	    }
        	    $sql="SELECT AppRoleID,UserID FROM INTRANET_PA_C_FRMROL WHERE CycleID=".IntegerSafe($cycleID)." AND TemplateID=".IntegerSafe($templateID)." AND AppRoleID=".IntegerSafe($appRoleIDArr[$i])." AND set_order = " .$j. ";";
        	    //echo $sql."<br/><br/>";
        		$approleResult=$connection->returnResultSet($sql);
        		if(sizeof($approleResult)>0){
        			$originalUserPair[] = $approleResult[0];
        	        $sql="UPDATE INTRANET_PA_C_FRMROL SET UserID=".$userID.", set_name=\"".$specialSetName[$j-1]."\",";
        	        $sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
        	        $sql.="WHERE CycleID=".IntegerSafe($cycleID)." AND TemplateID=".IntegerSafe($templateID)." AND AppRoleID=".IntegerSafe($appRoleIDArr[$i])." AND set_order = ".$j.";";
        	        //echo $sql."<br/><br/>";
        	        $a=$connection->db_db_query($sql);
        	    }else{        	    	
        	    	if($userID=="NULL"){
        	    		continue;
        	    	}
        	        $sql="INSERT INTO INTRANET_PA_C_FRMROL(CycleID,TemplateID,AppRoleID,UserID,is_special,set_order,set_name,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
        	        $sql.="SELECT * FROM(SELECT ".$cycleID." as CycleID,".$templateID." as TemplateID,".IntegerSafe($appRoleIDArr[$i])." as AppRoleID,".$userID." as UserID,1 as is_special,".$j." as set_order,".$setName." as set_name,";
        	        $sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
        	        $sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";
        	        //echo $sql."<br/><br/>";
        	        $a=$connection->db_db_query($sql);  
        	    }
    	    }
    	    if($newLogUserPair != $originalUserPair){
	    	    // log special role			
				$logData = array();
				$recordDetail = array();
				$logData['CycleID'] = $cycleID;
				$logData['Function'] = 'Cycle Template Special Role ';
				$logData['RecordType'] = (sizeof($approleResult)>0)?'UPDATE':'NEW';
				$recordDetail[] = array(
									  	"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['SpecialFormSettingName'],
									  	"Message"=>$setName
									  );
				$recordDetail[] = array(
										"Parameter"=>"TemplateID",
									  	"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['ApprsialForm'],
									  	"Original"=>$templateID,
									  	"MapWithTable"=>"INTRANET_PA_S_FRMTPL",
									  	"MapDisplay"=>"FrmTitleEng"
									  );
				$newMsg = "<br>";
				foreach($newLogUserPair as $userRole){
					$sql = "SELECT NameChi,NameEng FROM INTRANET_PA_S_APPROL WHERE AppRoleID='".$userRole['AppRoleID']."' ";
					$roleResult = current($connection->returnResultSet($sql));				 
					$sql = "SELECT EnglishName,ChineseName FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID='".$userRole['UserID']."'";
					$userResult = current($connection->returnResultSet($sql));
					$newMsg .= Get_Lang_Selection($roleResult['NameChi'],$roleResult['NameEng']).": ".Get_Lang_Selection($userResult['ChineseName'],$userResult['EnglishName'])."<br>";
				}
				if(empty($originalUserPair)){							  
					$recordDetail[] = array(
										  	"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['SpecialRole'],
										  	"New"=>$newMsg
										  );
				}else{
					$oriMsg = "<br>";
					foreach($originalUserPair as $userRole){
						$sql = "SELECT NameChi,NameEng FROM INTRANET_PA_S_APPROL WHERE AppRoleID='".$userRole['AppRoleID']."' ";
						$roleResult = current($connection->returnResultSet($sql));				 
						$sql = "SELECT EnglishName,ChineseName FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID='".$userRole['UserID']."'";
						$userResult = current($connection->returnResultSet($sql));
						$oriMsg .= Get_Lang_Selection($roleResult['NameChi'],$roleResult['NameEng']).": ".Get_Lang_Selection($userResult['ChineseName'],$userResult['EnglishName'])."<br>";
					}					  
					$recordDetail[] = array(
										  	"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['SpecialRole'],
										  	"Original"=>$oriMsg,
										  	"New"=>$newMsg
										  );
				}
				$logData['RecordDetail'] = $json->encode($recordDetail);
				$result = $indexVar['libappraisal']->addCycleSettingLog($logData);
    	    }
    	}	
	}
	//======================================================================== 各章節遞交批次  ========================================================================//
	if($_POST["DelBatchIDStr"]!=null){
		$sql="DELETE FROM INTRANET_PA_C_BATCH WHERE BatchID IN (".$_POST["DelBatchIDStr"].");";
		//echo $sql."<br/><br/>";
		$a=$connection->db_db_query($sql);
		$delBatchIDArr = explode(",",$_POST["DelBatchIDStr"]);
		
		// log delete batch		
		$logData = array();
		$recordDetail = array();
		$logData['CycleID'] = $cycleID;
		$logData['Function'] = 'Cycle Template Batch Delete ';
		$logData['RecordType'] = 'DELETE';
		$recordDetail[] = array(
								"Parameter"=>"TemplateID",
							  	"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['ApprsialForm'],
							  	"Original"=>$templateID,
							  	"MapWithTable"=>"INTRANET_PA_S_FRMTPL",
							  	"MapDisplay"=>"FrmTitleEng"
							  );
		$recordDetail[] = array(
							  	"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['SectionBatch'],
							  	"Original"=>$delBatchIDArr
							  );						
		$logData['RecordDetail'] = $json->encode($recordDetail);
		$result = $indexVar['libappraisal']->addCycleSettingLog($logData);
	}
	for($j=1;$j<=$totalSpecialSet+1;$j++){
	    $secIDArr=explode(",",$_POST["SecIDStr_".$j]);
	    $nameArr=explode(",",$_POST["SpecialSetName"]);
	    $hasUpdateBatch = false;
		$batchLogDetails = array();	
    	for($i=0;$i<sizeof($secIDArr);$i++){
    		$appRoleID=($_POST["SecAppRoleID_".$j."_".$i]!=null)?"'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["SecAppRoleID_".$j."_".$i])."'":"NULL";;
    		$editPrdFr=($_POST["EditPrdFr_".$j."_".$i]!=null)?"'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["EditPrdFr_".$j."_".$i])." 00:00:00'":"NULL";
    		$editPrdTo=($_POST["EditPrdTo_".$j."_".$i]!=null)?"'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["EditPrdTo_".$j."_".$i])." 23:59:59'":"NULL";
    		$batchID=$_POST["BatchID_".$j."_".$i];
    		if($batchID!="" && in_array($batchID,$delBatchIDArr)){
    			continue;
    		}
    		$sql="SELECT AppRoleID,EditPrdFr,EditPrdTo FROM INTRANET_PA_C_BATCH WHERE CycleID='".IntegerSafe($cycleID)."' AND TemplateID='".IntegerSafe($templateID)."' AND ";
    		$sql.="BatchID='".$batchID."'";
    		//$sql.="EditPrdFr=".$editPrdFr.";";
    		//$sql="SELECT AppRoleID FROM INTRANET_PA_C_BATCH WHERE CycleID=".IntegerSafe($cycleID)." AND TemplateID=".IntegerSafe($templateID)." AND SecID=".IntegerSafe($secIDArr[$i]).";";
    		//echo $sql."<br/><br/>";
    		$a=$connection->returnResultSet($sql);    		
    		$isSpecial = ($j > 1)?"'1'":"''";     		
    		$setName = ($nameArr[$j-1]=="undefined" || $nameArr[$j-1]=="")?"NULL":"'".$nameArr[$j-1]."'";
    		if($secIDArr[$i]!="NULL" && $secIDArr[$i]!="" && $appRoleID!="NULL"){
    			if(sizeof($a)>0){
    				if($sys_custom['eAppraisal']['allowSaveToDraft'] && ($editPrdFr != $a[0]['EditPrdFr'] || $editPrdTo != $a[0]['EditPrdTo'])){
    					$taSetting = $connection->Get_General_Setting('TeacherApprisal');
    					$editPeriodUpdated = unserialize($taSetting['editPeriodUpdated']);
    					if($editPeriodUpdated==false){
    						$editPeriodUpdated = array();
    					}
    					$editPeriodUpdated[$cycleID."-".$templateID] = true;
    					$newSetting["editPeriodUpdated"] = serialize($editPeriodUpdated);
    					$connection->Save_General_Setting("TeacherApprisal", $newSetting);
    				}
    				$sql="UPDATE INTRANET_PA_C_BATCH SET AppRoleID=".$appRoleID.",EditPrdFr=".$editPrdFr.",EditPrdTo=".$editPrdTo.",SecID=".$secIDArr[$i].",";
    				$sql.="is_special=".$isSpecial.", set_name=".$setName.",";
    				$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
    				$sql.="WHERE CycleID=".$cycleID." AND TemplateID=".$templateID." AND BatchID=".$batchID." AND set_order=".IntegerSafe($j).";";
    				//echo $sql."<br/><br/>";
    				$a=$connection->db_db_query($sql);
    				$hasUpdateBatch = true;    				
    			}
    			else{
    				$sql="INSERT INTO INTRANET_PA_C_BATCH(CycleID,TemplateID,AppRoleID,EditPrdFr,EditPrdTo,SecID,is_special,set_order,set_name,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
    				$sql.="SELECT * FROM(SELECT ".$cycleID." as CycleID,".$templateID." as TemplateID,".$appRoleID." as AppRoleID,".$editPrdFr." as EditPrdFr,";
    				$sql.=$editPrdTo." as EditPrdTo,".IntegerSafe($secIDArr[$i])." as SecID,";
    				$sql.=$isSpecial." as is_special,".IntegerSafe($j)." as set_order,";
    				$sql.=$setName." as set_name,";
    				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
    				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";
    				//echo $sql."<br/><br/>";
    				$a=$connection->db_db_query($sql);
    				$batchID = $connection->db_insert_id();
    			}
    			$batchLogDetails[] = "BatchID: ".$batchID."(".$editPrdFr." - ".$editPrdTo.")[App role:".$appRoleID."]";
    		}
    	}
    	// Update INTRANET_PA_C_BATCH BatchOrder
    	$hasOrderChange = false;
    	$orderChangeLog = array();
    	$sql="SELECT batch.BatchID,batch.CycleID,batch.TemplateID,batch.BatchOrder,batch.EditPrdFr
		FROM INTRANET_PA_C_BATCH batch LEFT JOIN INTRANET_PA_S_FRMSEC frmsec ON batch.SecID = frmsec.SecID 
		WHERE batch.CycleID=".IntegerSafe($cycleID)." AND batch.TemplateID=".IntegerSafe($templateID)." AND batch.SecID IN (".$_POST["SecIDStr_".$j].") AND batch.set_order='".$j."' ORDER BY batch.EditPrdFr ASC ,frmsec.DisplayOrder ASC, batch.SecID ASC;";
    	$a=$connection->returnResultSet($sql);
    	for($i=0;$i<sizeof($a);$i++){
    		$batchID=$a[$i]["BatchID"];
    		$sql="UPDATE INTRANET_PA_C_BATCH SET BatchOrder=".($i+1).",";
    		$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
    		$sql.="WHERE CycleID=".IntegerSafe($cycleID)." AND TemplateID=".IntegerSafe($templateID)." AND BatchID=".IntegerSafe($batchID).";";
    		//echo $sql."<br/><br/>";
    		$b=$connection->db_db_query($sql);
    		if($a[$i]["BatchOrder"]!=($i+1)){
    			$hasOrderChange = true;
    			$orderChangeLog[] = "BatchID ".$batchID." Order : ".$a[$i]["BatchOrder"]." -> ".($i+1);
    		}
    	}
    	
    	// log batch setup		
		$logData = array();
		$recordDetail = array();
		$logData['CycleID'] = $cycleID;
		$logData['Function'] = 'Cycle Template Batch Setup ';
		$logData['RecordType'] = ($hasUpdateBatch || $hasOrderChange)?'UPDATE':'NEW';
		$recordDetail[] = array(
							  	"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['SpecialFormSettingName'],
							  	"Message"=>($j==1)?'Default':$specialSetName[$j-1]
							  );						  
		$recordDetail[] = array(
								"Parameter"=>"TemplateID",
							  	"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['ApprsialForm'],
							  	"Original"=>$templateID,
							  	"MapWithTable"=>"INTRANET_PA_S_FRMTPL",
							  	"MapDisplay"=>"FrmTitleEng"
							  );
		if(!empty($batchLogDetails)){							  
			$recordDetail[] = array(
								  	"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['SectionBatch'],
								  	"Message"=>implode('<br>',$batchLogDetails)
								  );
		}					  
		if($hasOrderChange==true){
			$recordDetail[] = array(
								  	"DisplayName"=>$Lang['Appraisal']['BtnChangeTemplateOrder'],
								  	"Message"=>'has change in order<br>'.implode('<br>',$orderChangeLog)
								  );
		}						  					
		$logData['RecordDetail'] = $json->encode($recordDetail);
		$result = $indexVar['libappraisal']->addCycleSettingLog($logData);
    		
    }
    
    //======================================================================== 管理使用評審表格特別設定用戶  ========================================================================//    
	$sql="DELETE FROM INTRANET_PA_C_SPECIAL_BATCH_USER WHERE CycleID=".IntegerSafe($cycleID)." AND TemplateID=".IntegerSafe($templateID).";";
    $a=$connection->db_db_query($sql);
	for($j=2;$j<=$totalSpecialSet+1;$j++){
		$sectionUserList = $_POST['SpecialSetUsersStr_'.$j];		
		if($sectionUserList!=""){
			$sectionUserArr = explode(",",$sectionUserList);
			foreach($sectionUserArr as $setUser){
				if($setUser != ""){
					$sql="INSERT INTO INTRANET_PA_C_SPECIAL_BATCH_USER(CycleID,TemplateID,SetOrder,UserID,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
	    			$sql.="SELECT * FROM(SELECT ".$cycleID." as CycleID,".$templateID." as TemplateID,".$j." as SetOrder,".$setUser." as UserID,";
	    			$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
	    			$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";
	    			//echo $sql."<br/><br/>";
	    			$a=$connection->db_db_query($sql);
				}
			}
			
			// log special role			
			$logData = array();
			$recordDetail = array();
			$logData['CycleID'] = $cycleID;
			$logData['Function'] = 'Cycle Template Special Setup User ';
			$logData['RecordType'] = 'UPDATE';
			$recordDetail[] = array(
								  	"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['SpecialFormSettingName'],
								  	"Message"=>($j==1)?'Default':$specialSetName[$j-1]
								  );
			$recordDetail[] = array(
									"Parameter"=>"TemplateID",
								  	"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['ApprsialForm'],
								  	"New"=>$templateID,
								  	"MapWithTable"=>"INTRANET_PA_S_FRMTPL",
								  	"MapDisplay"=>"FrmTitleEng"
								  );
			$recordDetail[] = array(
									"Parameter"=>"UserID",
								  	"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['SetupSpecialFormSettingUsers'],
								  	"New"=>$sectionUserArr,
								  	"MapWithTable"=>$appraisalConfig['INTRANET_USER'],
								  	"MapDisplay"=>"EnglishName"
								  );						
			$logData['RecordDetail'] = $json->encode($recordDetail);
			$result = $indexVar['libappraisal']->addCycleSettingLog($logData);
		}
	}	
}
else if($formType=="GrpSave"){
	$groupID=$_POST["GrpID"];
	$cycleID=$_POST["CycleID"];
	$descrEng=($_POST["DescrEng"]!=null)?"'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["DescrEng"])."'":"''";
	$descrChi=($_POST["DescrChi"]!=null)?"'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["DescrChi"])."'":"''";
	$templateIDArr=explode(",",$_POST["TemplateIDStr"]);
	$logType='UPDATE';
	
	// Original Data
	if($groupID!=""){
		$sql = "SELECT DescrChi,DescrEng FROM INTRANET_PA_C_GRPSUM WHERE GrpID='".IntegerSafe($groupID)."'";
		$originalGrpData = current($connection->returnResultSet($sql));
		$sql = "SELECT TemplateID,AppMode FROM INTRANET_PA_C_GRPFRM WHERE GrpID='".IntegerSafe($groupID)."' AND Incl=1";
		$originalGrpFrmData = current($connection->returnArray($sql));
		$sql = "SELECT UserID,LeaderRole,CanCallModify,IsLeaderFill FROM INTRANET_PA_C_GRPMEM WHERE GrpID='".IntegerSafe($groupID)."' AND IsLeader=1";
		$originalGrpLeaderData = $connection->returnArray($sql);
		$sql = "SELECT UserID FROM INTRANET_PA_C_GRPMEM WHERE GrpID='".IntegerSafe($groupID)."' AND IsLeader=0";
		$originalGrpMemberData = $connection->returnVector($sql);
	}
	
	//======================================================================== 職員分組內容  ========================================================================//
	$sql="SELECT GrpID FROM INTRANET_PA_C_GRPSUM WHERE CycleID=".IntegerSafe($cycleID)." AND GrpID=".IntegerSafe($groupID).";";
	//echo $sql."<br/><br/>";
	$a=$connection->returnResultSet($sql);
	if(sizeof($a)>0){
		$sql="UPDATE INTRANET_PA_C_GRPSUM SET DescrChi=".$descrChi.",DescrEng=".$descrEng.",";
		$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
		$sql.="WHERE GrpID=".IntegerSafe($groupID)." AND CycleID=".IntegerSafe($cycleID).";";
		//echo $sql."<br/><br/>";
		$a=$connection->db_db_query($sql);
	}
	else{
		$sql="INSERT INTO INTRANET_PA_C_GRPSUM(CycleID,DescrChi,DescrEng,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
		$sql.="SELECT * FROM(SELECT ".$cycleID." as CycleID,".$descrChi." as DescrChi,".$descrEng." as DescrEng,";
		$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
		$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";
		//echo $sql."<br/><br/>";
		$a=$connection->db_db_query($sql);
		
		$sql="SELECT GrpID FROM INTRANET_PA_C_GRPSUM WHERE CycleID=".IntegerSafe($cycleID)." AND DescrChi=".$descrChi." AND DescrEng=".$descrEng.";";
		$a=$connection->returnResultSet($sql);
		$getGrpID=$a[0]["GrpID"];
		$returnPath=sprintf($returnPath,$getGrpID);
		if($groupID==""){
			$groupID=$getGrpID;
			$logType = 'NEW';
		}
	}	
	//======================================================================== 編配表格及評核模式 ========================================================================//
	for($i=0;$i<sizeof($templateIDArr);$i++){		
		$appMode=$_POST["AppMode_".$i];
		$incl=($_POST["IncludeTemplate"]==$i)?1:0;
		$appMode=($incl!=0)?$appMode:0;
		
		$sql="SELECT GrpID FROM INTRANET_PA_C_GRPFRM WHERE GrpID=".IntegerSafe($groupID)." AND TemplateID=".IntegerSafe($templateIDArr[$i]).";";
		//echo $sql."<br/><br/>";
		$a=$connection->returnResultSet($sql);
		if(sizeof($a)>0){
			$sql="UPDATE INTRANET_PA_C_GRPFRM SET AppMode=".IntegerSafe($appMode).",Incl=".IntegerSafe($incl).",";
			$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
			$sql.="WHERE GrpID=".$groupID." AND TemplateID=".IntegerSafe($templateIDArr[$i]).";";
			//echo $sql."<br/><br/>";
			$a=$connection->db_db_query($sql);
		}
		else{
			$sql="INSERT INTO INTRANET_PA_C_GRPFRM(GrpID,TemplateID,AppMode,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
			$sql.="SELECT * FROM(SELECT ".$groupID." as GrpID,".IntegerSafe($templateIDArr[$i])." as TemplateID,".$appMode." as AppMode,".$incl." as Incl,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
			$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";
			//echo $sql."<br/><br/>";
			$a=$connection->db_db_query($sql);
		}
	}
	//======================================================================== 編配組長 & 編配組員 ========================================================================//
	// prepare to clear all group leader & members
	$sql="DELETE FROM INTRANET_PA_C_GRPMEM WHERE GrpID=".IntegerSafe($groupID).";";
	//echo $sql."<br/><br/>";
	$a=$connection->db_db_query($sql);
	// prepare for leader
	//$leaderID=$_POST["LeaderIDStr"];
	$leaderIDArr=explode(",",$_POST["LeaderIDStr"]);
	$leaderIDRoleArr=explode(",",$_POST["LeaderIDRoleStr"]);
	$leaderModifyIDStrArr=explode(",",$_POST["LeaderModifyIDStr"]);
	$leaderFillIDStrArr=explode(",",$_POST["LeaderFillIDStr"]);
	if($leaderIDArr!=null){
		for($i=0;$i<sizeof($leaderIDArr);$i++){
			if($leaderIDArr[$i]!=""){
				$sql="INSERT INTO INTRANET_PA_C_GRPMEM(GrpID,UserID,IsLeader,LeaderRole,CanCallModify,IsLeaderFill,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT * FROM(SELECT ".$groupID." as GrpID,".$leaderIDArr[$i]." as UserID,1 as IsLeader,";
				$sql.=$leaderIDRoleArr[$i]." as LeaderRole,";
				$sql.=$leaderModifyIDStrArr[$i]." as CanCallModify,";
				$sql.= $leaderFillIDStrArr[$i]." as IsLeaderFill,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";
				//echo $sql."<br/><br/>";
				$a=$connection->db_db_query($sql);
			}
		}
	}
	$memberIDArr=explode(",",$_POST["MemberIDStr"]);
	if($memberIDArr!=null){
		for($i=0;$i<sizeof($memberIDArr);$i++){
			$sql="INSERT INTO INTRANET_PA_C_GRPMEM(GrpID,UserID,IsLeader,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
			$sql.="SELECT * FROM(SELECT ".$groupID." as GrpID,".$memberIDArr[$i]." as UserID,0 as IsLeader,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
			$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";
			//echo $sql."<br/><br/>";
			$a=$connection->db_db_query($sql);
		}
	}
	/*$sql="SELECT GrpID FROM INTRANET_PA_C_GRPSUM WHERE GrpID=".IntegerSafe($groupID)." AND CycleID=".IntegerSafe($cycleID).";";
	$a=$connection->returnResultSet($sql);
	$getGrpID=$a[0]["GrpID"];
	$returnPath=sprintf($returnPath,$getGrpID);*/
	// log update one-to-one appraisal		
	$logData = array();
	$logData['CycleID'] = $cycleID;
	if($logType=='NEW'){
		$logData['Function'] = 'Add Group Appraisal';
	}else{
		$logData['Function'] = 'Update Group Appraisal';
	}
	$logData['RecordType'] = $logType;
	$recordDetail[] = array(
								"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['StfGrpDescrChi'],
								"Original"=>$originalGrpData['DescrChi'],
								"New"=>$descrChi
							);
	$recordDetail[] = array(
								"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['StfGrpDescrEng'],
								"Original"=>$originalGrpData['DescrEng'],
								"New"=>$descrEng
							);
	$recordDetail[] = array(
								"Parameter"=>"TemplateID",
							  	"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['PossibleTemplate'],
							  	"Original"=>$originalGrpFrmData['TemplateID'],
							  	"New"=>$templateIDArr[$_POST["IncludeTemplate"]],
							  	"MapWithTable"=>"INTRANET_PA_S_FRMTPL",
							  	"MapDisplay"=>"FrmTitleEng"
							  );
	$appModeLang = array(
		1=>$Lang['Appraisal']['CycleTemplate']['SelfApprsialMode'],
		2=>$Lang['Appraisal']['CycleTemplate']['UpToDownApprsialMode'],
		3=>$Lang['Appraisal']['CycleTemplate']['DownToUpApprsialMode'],
		4=>$Lang['Appraisal']['CycleTemplate']['GroupApprsialMode']
	);
	$recordDetail[] = array(
							  	"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['ApprsialMode'],
							  	"Original"=>$appModeLang[$originalGrpFrmData['AppMode']],
							  	"New"=>$appModeLang[$_POST["AppMode_".$_POST["IncludeTemplate"]]]
							  );
	$originalLeaderMsg = "";
	if(isset($originalGrpLeaderData) && !empty($originalGrpLeaderData)){
		foreach($originalGrpLeaderData as $leader){
			$sql = "SELECT EnglishName FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID='".$leader['UserID']."'";
			$name = current($connection->returnVector($sql));
			$originalLeaderMsg .= $name." (UserID: ".$leader['UserID'].") ";
			if($originalGrpFrmData['AppMode']>1){
				$originalLeaderMsg .= "[".(($leader['LeaderRole']==0)?$Lang['Appraisal']['AppraisalRecordsView']:$Lang['Appraisal']['AppraisalRecordsEdit'])."]";
			}else{
				$originalLeaderMsg .= "[".(($leader['IsLeaderFill']==0)?$Lang['Appraisal']['AppraisalRecordsNeedNotFill']:$Lang['Appraisal']['AppraisalRecordsNeedFill'])."]";
			}
			$originalLeaderMsg .= ($leader['CanCallModify']==1)?"[".$Lang['Appraisal']['AppraisalRecordsModify']."]":"";
			$originalLeaderMsg .= "<br>";
		}
	}
	$newLeaderMsg = "";
	for($i=0;$i<sizeof($leaderIDArr);$i++){
			if($leaderIDArr[$i]!=""){
				$sql = "SELECT EnglishName FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID='".$leaderIDArr[$i]."'";
				$name = current($connection->returnVector($sql));
				$newLeaderMsg .= $name." (UserID: ".$leaderIDArr[$i].") ";
				if($_POST["AppMode_".$_POST["IncludeTemplate"]] > 1){
					$newLeaderMsg .= "[".(($leaderIDRoleArr[$i]==0)?$Lang['Appraisal']['AppraisalRecordsView']:$Lang['Appraisal']['AppraisalRecordsEdit'])."]";
				}else{
					$newLeaderMsg .= "[".(($leaderFillIDStrArr[$i]==0)?$Lang['Appraisal']['AppraisalRecordsNeedNotFill']:$Lang['Appraisal']['AppraisalRecordsNeedFill'])."]";
				}
				$newLeaderMsg .= ($leaderModifyIDStrArr[$i]==1)?"[".$Lang['Appraisal']['AppraisalRecordsModify']."]":"";
				$newLeaderMsg .= "<br>";
			}
		}
	$recordDetail[] = array(
								"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['AssignLeader'],
							  	"Original"=>$originalLeaderMsg,
							  	"New"=>$newLeaderMsg
							  );
	$recordDetail[] = array(
								"Parameter"=>"UserID",
							  	"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['AssignMembers'],
							  	"Original"=>$originalGrpMemberData,
							  	"New"=>$memberIDArr,
							  	"MapWithTable"=>$appraisalConfig['INTRANET_USER'],
							  	"MapDisplay"=>"EnglishName"
							  );
	$logData['RecordDetail'] = $json->encode($recordDetail);
	$result = $indexVar['libappraisal']->addCycleSettingLog($logData);
}
else if($formType=="GrpSaveAs"){
	$groupID=$_POST["GrpID"];
	$cycleID=$_POST["CycleID"];
	$sql="SELECT DescrChi,DescrEng FROM INTRANET_PA_C_GRPSUM WHERE GrpID=".IntegerSafe($groupID)." AND CycleID=".IntegerSafe($cycleID);
	//echo $sql."<br/><br/>";
	$a=$connection->returnResultSet($sql);
	$descrChi = "'".$indexVar['libappraisal']->Get_Safe_Sql_Query($a[0]["DescrChi"]."-".$Lang['Appraisal']['CopyChi'])."'";
	$descrEng = "'".$indexVar['libappraisal']->Get_Safe_Sql_Query($a[0]["DescrEng"]."-".$Lang['Appraisal']['CopyEng'])."'";
	$sql="INSERT INTO INTRANET_PA_C_GRPSUM(CycleID,DescrChi,DescrEng,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
	$sql.="SELECT * FROM(SELECT ".$cycleID." as CycleID,".$descrChi." as DescrChi,".$descrEng." as DescrEng,";
	$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
	$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";
	//echo $sql."<br/><br/>";
	$a=$connection->db_db_query($sql);
	$sql="SELECT GrpID FROM INTRANET_PA_C_GRPSUM WHERE CycleID=".IntegerSafe($cycleID)." AND DescrChi=".$descrChi." AND DescrEng=".$descrEng.";";
	$a=$connection->returnResultSet($sql);
	$getGrpID=$a[0]["GrpID"];
	$returnPath=sprintf($returnPath,$getGrpID);	
	//echo $getGrpID."<br/><br/>";
	$sql ="INSERT INTO INTRANET_PA_C_GRPMEM(GrpID,UserID,IsLeader,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
	$sql.="SELECT * FROM(SELECT ".$getGrpID." as GrpID,UserID,IsLeader,";
	$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
	$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified FROM INTRANET_PA_C_GRPMEM WHERE GrpID=".IntegerSafe($groupID).") as a ";
	//echo $sql."<br/><br/>";
	$a=$connection->db_db_query($sql);
	$sql ="INSERT INTO INTRANET_PA_C_GRPFRM(GrpID,TemplateID,AppMode,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
	$sql.="SELECT * FROM(SELECT ".$getGrpID." as GrpID,TemplateID,AppMode,Incl,";
	$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
	$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified FROM INTRANET_PA_C_GRPFRM WHERE GrpID=".IntegerSafe($groupID).") as a ";
	//echo $sql."<br/><br/>";
	$a=$connection->db_db_query($sql);
	// log update one-to-one appraisal		
	$logData = array();
	$logData['CycleID'] = $cycleID;
	$logData['Function'] = 'Copy Group Appraisal';
	$logData['RecordType'] = 'NEW';
	$recordDetail[] = array(
								"Parameter"=>"GrpID",
							  	"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['StfGrp'],
							  	"Original"=>$groupID,
							  	"New"=>$getGrpID,
							  	"MapWithTable"=>"INTRANET_PA_C_GRPSUM",
							  	"MapDisplay"=>"DescrEng"
							  );
	$logData['RecordDetail'] = $json->encode($recordDetail);
	$result = $indexVar['libappraisal']->addCycleSettingLog($logData);
}
else if($formType=="IdvSave"){
	$idvID=$_POST["IdvID"];
	$cycleID=$_POST["CycleID"];
	$logType = "UPDATE";
	//======================================================================== 編配被評者 ========================================================================//
	$templateID=$_POST["TemplateID"];
	$appraisee=($_POST["AppraiseeIDStr"]!=null)?$_POST["AppraiseeIDStr"]:"NULL";
	$remark=($_POST["Rmk"]!=null)?"'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["Rmk"])."'":"''";
	
	// Original Data
	if($idvID!=""){
		$sql = "SELECT TemplateID,UserID FROM INTRANET_PA_C_IDVFRM WHERE IdvID='".IntegerSafe($idvID)."'";
		$originalFrmData = current($connection->returnResultSet($sql));
		$sql = "SELECT AprID FROM INTRANET_PA_C_IDVAPR WHERE IdvID='".IntegerSafe($idvID)."'";
		$originalAprList = $connection->returnVector($sql);
	}
	
	// prepare for appraisee
	$sql="SELECT IdvID FROM INTRANET_PA_C_IDVFRM WHERE IdvID=".IntegerSafe($idvID)." AND TemplateID=".IntegerSafe($templateID);
	$a=$connection->returnResultSet($sql);
	if(sizeof($a)>0){
		$sql="UPDATE INTRANET_PA_C_IDVFRM SET UserID=".$appraisee.",Remark=".$remark.",";
		$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
		$sql.="WHERE IdvID=".$idvID." AND TemplateID=".IntegerSafe($templateID)." AND CycleID=".IntegerSafe($cycleID).";";
		//echo $sql."<br/><br/>";
		$a=$connection->db_db_query($sql);
	}
	else{
		$sql="INSERT INTO INTRANET_PA_C_IDVFRM(CycleID,TemplateID,UserID,Remark,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
		$sql.="SELECT * FROM(SELECT ".$cycleID." as CycleID,".$templateID." as TemplateID,".$appraisee." as UserID,".$remark." as Remark,";
		$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
		$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";
		//echo $sql."<br/><br/>";
		$a=$connection->db_db_query($sql);
		$logType = "NEW";
	}
	//======================================================================== 編配評核者 ========================================================================//
	if($idvID==null){
		$sql="SELECT IdvID FROM INTRANET_PA_C_IDVFRM WHERE CycleID=".IntegerSafe($cycleID)." AND TemplateID=".IntegerSafe($templateID)." AND UserID=".$appraisee.";";
		//echo $sql."<br/><br/>";
		$a=$connection->returnResultSet($sql);
		$idvID=$a[0]["IdvID"];		
	}
	// prepare to clear all appraiser	
	$sql="DELETE FROM INTRANET_PA_C_IDVAPR WHERE IdvID=".IntegerSafe($idvID).";";
	$a=$connection->db_db_query($sql);
	$appraiserArr=explode(",",$_POST["AppraiserIDStr"]);
	if($appraiserArr!=null){
		for($i=0;$i<sizeof($appraiserArr);$i++){
			$sql="INSERT INTO INTRANET_PA_C_IDVAPR(IdvID,AprID,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
			$sql.="SELECT * FROM(SELECT ".$idvID." as IdvID,".$appraiserArr[$i]." as AprID,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
			$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";
			//echo $sql."<br/><br/>";
			$a=$connection->db_db_query($sql);
		}
	}
	$sql="SELECT IdvID FROM INTRANET_PA_C_IDVFRM WHERE TemplateID=".IntegerSafe($templateID)." AND CycleID=".IntegerSafe($cycleID).";";
	$a=$connection->returnResultSet($sql);
	$getIdvID=$a[0]["IdvID"];
	$returnPath=sprintf($returnPath,$getIdvID);
	//echo $returnPath;
	
	// log update one-to-one appraisal		
	$logData = array();
	$logData['CycleID'] = $cycleID;
	if($logType=='NEW'){
		$logData['Function'] = 'Add One-to-one Appraisal';
	}else{
		$logData['Function'] = 'Update One-to-one Appraisal';
	}
	$logData['RecordType'] = $logType;
	$recordDetail[] = array(
								"Parameter"=>"TemplateID",
							  	"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['PossibleTemplateForm'],
							  	"Original"=>$originalFrmData['TemplateID'],
							  	"New"=>$templateID,
							  	"MapWithTable"=>"INTRANET_PA_S_FRMTPL",
							  	"MapDisplay"=>"FrmTitleEng"
							  );
	$recordDetail[] = array(
								"Parameter"=>"UserID",
							  	"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['AssignedAppraisee'],
							  	"Original"=>$originalFrmData['UserID'],
							  	"New"=>$appraisee,
							  	"MapWithTable"=>$appraisalConfig['INTRANET_USER'],
							  	"MapDisplay"=>"EnglishName"
							  );
	$recordDetail[] = array(
								"Parameter"=>"UserID",
							  	"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['AssignedAppraiser'],
							  	"Original"=>$originalAprList,
							  	"New"=>explode(",",$_POST["AppraiserIDStr"]),
							  	"MapWithTable"=>$appraisalConfig['INTRANET_USER'],
							  	"MapDisplay"=>"EnglishName"
							  );
	$logData['RecordDetail'] = $json->encode($recordDetail);
	$result = $indexVar['libappraisal']->addCycleSettingLog($logData);
}
else if($formType=="ObsSave"){
	$templateID=$_POST["DDLTemplateID"];
	$cycleID=$_POST["CycleID"];
	$sql="SELECT TemplateID FROM INTRANET_PA_C_OBSSEL WHERE CycleID=".$cycleID." AND TemplateID=".$templateID.";";
	$a=$connection->returnResultSet($sql);
	if(sizeof($a)==0){
		$sql="INSERT INTO INTRANET_PA_C_OBSSEL(CycleID,TemplateID,IsActive,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
		$sql.="SELECT * FROM(SELECT ".$cycleID." as CycleID,".$templateID." as TemplateID,0 as IsActive,";
		$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
		$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";
		//echo $sql."<br/><br/>";
		$a=$connection->db_db_query($sql);
		// log obs form add to cycle		
		$logData = array();
		$logData['CycleID'] = $cycleID;
		$logData['Function'] = 'Add Form To Academic Affairs';
		$logData['RecordType'] = 'NEW';
		$recordDetail[] = array(
							  	"DisplayName"=>$Lang['Appraisal']['Cycle']['ObsTemplate'],
							  	"New"=>$templateID
							  );
		$logData['RecordDetail'] = $json->encode($recordDetail);
		$result = $indexVar['libappraisal']->addCycleSettingLog($logData);
	}
}
else if($formType=="ObsSaveAs"){
	$templateID=$_POST["DDLTemplateID"];
	$cycleID=$_POST["CycleID"];
	$sql="SELECT TemplateID FROM INTRANET_PA_C_FRMSEL WHERE CycleID=".$cycleID." AND TemplateID=".$templateID.";";
	$a=$connection->returnResultSet($sql);
	if(sizeof($a)==0){
		$sql="INSERT INTO INTRANET_PA_C_OBSSEL(CycleID,TemplateID,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
		$sql.="SELECT * FROM(SELECT ".$cycleID." as CycleID,".$templateID." as TemplateID,0 as IsActive,";
		$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
		$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";
		//echo $sql."<br/><br/>";
		$a=$connection->db_db_query($sql);
	}
}
else if($formType=="RlsObsFrm"){
	$cycleID=$_POST["CycleID"];
	$sql .= "UPDATE INTRANET_PA_C_OBSSEL SET IsActive=1,";
	$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
	$sql.="WHERE CycleID=".IntegerSafe($cycleID).";";
	//echo $sql."<br/><br/>";
	$a=$connection->db_db_query($sql);
	// log obs form release		
	$logData = array();
	$logData['CycleID'] = $cycleID;
	$logData['Function'] = 'Academic Affairs Form Release';
	$logData['RecordType'] = 'RELEASE';
	$recordDetail[] = array(
						  	"DisplayName"=>$Lang['Appraisal']['BtbRlsObsAppForm'],
						  	"Message"=>''
						  );
	$logData['RecordDetail'] = $json->encode($recordDetail);
	$result = $indexVar['libappraisal']->addCycleSettingLog($logData);
}
else if($formType=="FormReOpen"){
	if($sys_custom['eAppraisal']['allowSaveToDraft']){
		$templateID=$_POST["TemplateID"];
		$cycleID=$_POST["CycleID"];
		$sql = "SELECT RlsNo,RecordID,BatchID FROM INTRANET_PA_T_FRMSUB WHERE RecordID IN (SELECT RecordID FROM INTRANET_PA_T_FRMSUM WHERE TemplateID='".$templateID."' AND CycleID='".$cycleID."') AND IsActive=1";
		$currActiveForms=$connection->returnArray($sql);
		foreach($currActiveForms as $form){
			$sql = "UPDATE INTRANET_PA_T_FRMSUB SET SubDate = NULL WHERE RlsNo='".$form['RlsNo']."' AND RecordID='".$form['RecordID']."' AND BatchID='".$form['BatchID']."'";
			$a=$connection->db_db_query($sql);
		}
	
		$taSetting = $connection->Get_General_Setting('TeacherApprisal');
		$editPeriodUpdated = unserialize($taSetting['editPeriodUpdated']);
		if($editPeriodUpdated==false){
			$editPeriodUpdated = array();
		}
		unset($editPeriodUpdated[$cycleID."-".$templateID]);
		$newSetting["editPeriodUpdated"] = serialize($editPeriodUpdated);
		$connection->Save_General_Setting("TeacherApprisal", $newSetting);
		
		// log form re-open		
		$logData = array();
		$logData['CycleID'] = $cycleID;
		$logData['Function'] = 'Form re-open';
		$logData['RecordType'] = 'UPDATE';
		$recordDetail[] = array(
							  	"Parameter"=>"TemplateID",
							  	"DisplayName"=>$Lang['Appraisal']['BtnReOpen'],
							  	"Original"=>$templateID,
							  	"MapWithTable"=>"INTRANET_PA_S_FRMTPL",
							  	"MapDisplay"=>"FrmTitleEng"
							  );
		$logData['RecordDetail'] = $json->encode($recordDetail);
		$result = $indexVar['libappraisal']->addCycleSettingLog($logData);

	}
}
else if($formType=="BtnReportRelease"){    
    $cycleID=$_POST["CycleID"];
    $sql = "UPDATE INTRANET_PA_C_CYCLE SET ReportReleaseDate=NOW(), ModifiedBy_UserID='".IntegerSafe($_SESSION['UserID'])."', DateTimeModified=NOW() WHERE CycleID='".$cycleID."' ";
    $a=$connection->db_db_query($sql);
    $saveSuccess = ($a)?true:false;
    
    // log form re-open
    $logData = array();
    $logData['CycleID'] = $cycleID;
    $logData['Function'] = 'Cycle release report';
    $logData['RecordType'] = 'UPDATE';
    $recordDetail[] = array(
        "Parameter"=>"CycleID",
        "DisplayName"=>$Lang['Appraisal']['BtnReportRelease'],
        "Original"=>$cycleID
    );
    $logData['RecordDetail'] = $json->encode($recordDetail);
    $result = $indexVar['libappraisal']->addCycleSettingLog($logData);
}

$returnMsgKey = ($saveSuccess)? 'UpdateSuccess' : 'UpdateUnsuccess';
//echo $returnPath;
header($returnPath."&returnMsgKey=".$returnMsgKey);



?>