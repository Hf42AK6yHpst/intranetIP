<?php
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");

$connection = new libgeneralsettings();

$cycleID = $_GET["cycleID"];

### Check csv file format
$limport = new libimporttext();
$libfs = new libfilesystem();

$sql="SELECT CycleID,GuideFile FROM INTRANET_PA_C_CYCLE WHERE CycleID=".IntegerSafe($cycleID).";";
//echo $sql."<br/><br/>";
$a=$connection->returnResultSet($sql);
$fileName=$a[0]["GuideFile"];
//$fullpath="/home/eclass/intranetdata/appraisal/upload/".$fileName;
$fullpath=$intranet_root.'/file/appraisal/upload/'.$fileName;
//echo $fullpath."<br/><br/>";
if(file_exists($fullpath) && $fileName != ""){
    $content = $libfs->file_read($fullpath);
    output2browser($content, urlencode($fileName));
}
else{
    header("Location: ".$_SERVER["HTTP_REFERER"]);
}

?>


