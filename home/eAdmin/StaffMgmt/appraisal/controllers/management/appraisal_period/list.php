<?php
// ============================== Related Tables ==============================
// INTRANET_PA_T_FRMSUM,INTRANET_PA_S_FRMTPL,INTRANET_PA_C_GRPSUM,INTRANET_PA_C_IDVFRM,INTRANET_PA_C_BATCH,INTRANET_PA_T_FRMSUB,INTRANET_PA_S_APPROL
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
### check access right


$currentDate = date("Y-m-d");

$arrCookies = array();
$arrCookies[] = array("records_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}

/*
 $ltimetable = new Timetable();
 $curTimetableId = $ltimetable->Get_Current_Timetable();
 */

# Page Title
$titleBar = "<div style=\"display:inline;float:left\"><span class=\"contenttitle\" style=\"vertical-align:bottom\">".$Lang['Appraisal']['AppraisalPeriod']."</span></div>";;
if($cycleID!=0){
	//$titleBar .= "<div style=\"display:inline;float:right;\" class=\"online_help\"><a href=\"javascript:goDownloadAttachment('".$cycleID."')\" title=\"".$Lang['Appraisal']['TitleBar']['DownloadDocument']."\" onclick=\"goDownload(".$cycleID.")\" class=\"help_icon\" id=\"downloadIcon\">";
	//$titleBar .= "<span style=\"font-size:10px\">".$Lang['Appraisal']['TitleBar']['DownloadDocument']."</span></div>";
}

$TAGS_OBJ[] = array($titleBar);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libappraisal_ui']->Include_Thickbox_JS_CSS();

// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$order = ($order == '') ? 0 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 7 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 10 : $numPerPage;

$connection = new libgeneralsettings();
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("AppPrd","AppDescr","CycleStart","CycleClose","Action","DateTimeRls","checkbox","CycleID");

if ($keyword !== '' && $keyword !== null) {
	$condsKeyword = " WHERE 1=1";
}
$li -> sql ="SELECT 
		CONCAT('<a id=\"cycleID_',a.CycleID,'\" class=\"tablelink\" href=\"javascript:goEdit(',a.CycleID,')\">',YearName,'</a>') as AppPrd,
		CONVERT(CONCAT('<a id=\"cycleID_',a.CycleID,'\" class=\"tablelink\" href=\"javascript:goEdit(',a.CycleID,')\">',AppDescr,'</a>') USING UTF8) as AppDescr,		
		CycleStart,CycleClose,Action,DateTimeRls,checkbox,DateTimeModified,CycleID
		FROM(
			SELECT CycleID,AcademicYearID,AppPrdFr,AppPrdTo,CONCAT(YEAR(AppPrdFr),'-',YEAR(AppPrdTo)) as AppPrd,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as AppDescr,
			DATE_FORMAT(CycleStart,'%Y-%m-%d') as CycleStart,DATE_FORMAT(CycleClose,'%Y-%m-%d') as CycleClose,DATE_FORMAT(DateTimeRls,'%Y-%m-%d') as DateTimeRls,
			CASE WHEN NOW() BETWEEN CycleStart AND CycleClose THEN '".$Lang['Appraisal']['CycleStatusDes']['InProgress']."'
			WHEN NOW() > CycleClose THEN '".$Lang['Appraisal']['CycleStatusDes']['Finish']."' WHEN (NOW() < CycleStart OR COALESCE(CycleStart,'')='') THEN '".$Lang['Appraisal']['CycleStatusDes']['NotYetStart']."' END as Action,					
			CONCAT('<input type=checkbox name=cycleIDAry[] id=cycleIDAry[] value=\"', CycleID,'\">') as checkbox,DateTimeModified			
			FROM INTRANET_PA_C_CYCLE 
		) as a
		INNER JOIN(			
			SELECT AcademicYearID,".$indexVar['libappraisal']->getLangSQL("YearNameB5","YearNameEN")." as YearName,Sequence FROM ACADEMIC_YEAR		
		) as ay ON a.AcademicYearID=ay.AcademicYearID 
					
					";
// echo $li->sql."<br/><br/>";
$li->no_col = sizeof($li->field_array);
$li->fieldorder2 = ", CycleID";
$li->IsColOff = "IP25_table";
$li->count_mode = 1;
if ($indexVar['libappraisal']->isEJ()) {
	$li->column_array = array(20);
}
$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['Appraisal']['Cycle']['AcademicYear'])."</th>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['Appraisal']['Cycle']['Descr'])."</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang['Appraisal']['Cycle']['CycleStart'])."</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang['Appraisal']['Cycle']['CycleClose'])."</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang['Appraisal']['Cycle']['Status'])."</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang['Appraisal']['Cycle']['DateTimeRls'])."</th>\n";
$li->column_list .= "<th width='1%'>".$li->check("cycleIdAry[]")."</th>\n";
$htmlAry['dataTable1'] = $li->display();


$htmlAry['contentTbl1'] = $x;


// ============================== Transactional data ==============================

// ============================== Define Button ==============================
$subBtnAry = array();
$btnAry[] = array('new', 'javascript: goAddCycle();', $Lang['Appraisal']['BtbAddCycle'], $subBtnAry);
$btnAry[] = array('copy', 'javascript: goCopyCycle();',$Lang['Appraisal']['AppraisalPeriodCopy']);
$htmlAry['contentTool'] = $indexVar['libappraisal_ui']->Get_Content_Tool_By_Array_v30($btnAry)."<br/>";

$btnAry = array();
$btnAry[] = array('delete', 'javascript: goDelete();',$Lang['Appraisal']['TemplateSample']['DeleteTemplate']);
$htmlAry['dbTableActionBtn'] = $indexVar['libappraisal_ui']->Get_DBTable_Action_Button_IP25($btnAry)

// ============================== Define Button ==============================
// ============================== Custom Function ==============================

// ============================== Custom Function ==============================
?>