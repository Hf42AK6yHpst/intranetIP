<?php 
// ============================== Related Tables ==============================
//
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['AppraisalPeriod']);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
$cycleID = $_GET["cycleID"];
$templateID = $_GET["templateID"];

$connection = new libgeneralsettings();
//======================================================================== Header ========================================================================//
### navigation
$navigationAry[] = array($Lang['Appraisal']['AppraisalPeriod'], 'javascript: goBack();');
$navigationAry[] = array($Lang['Appraisal']['AppraisalPeriodSetting'], 'javascript: goBackCycle('.$cycleID.');');
$navigationAry[] = array($Lang['Appraisal']['Cycle']['ObsTemplate']);
$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);
//======================================================================== Content ========================================================================//
$sql="SELECT CycleID,AppDescr
	FROM(
		SELECT CycleID,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as AppDescr
		FROM INTRANET_PA_C_CYCLE WHERE CycleID=".IntegerSafe($cycleID)."
	) as ipsf";
$a=$connection->returnResultSet($sql);
$cycleDescr=$a[0]["AppDescr"];
$x .= "<table class=\"form_table_v30\">";
$x .= "<tr><td class=\"field_title\"><b>".$Lang['Appraisal']['CycleTemplate']['CurrTemplate']."</b></td>";
$x .= "<td><a id='templateTitleID' class=\"tablelink\" href=\"javascript:goBackCycle(".$cycleID.")\">".$cycleDescr."</a></td></tr>";
$x .= "</table>";
//======================================================================== 職員分組內容 ========================================================================//
//======================================================================== 適用評審表格 ========================================================================//$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['CycleTemplate']['AcceptApprsialForm'])."\r\n";
$sql="SELECT TemplateID,FrmInfoChi,FrmInfoEng
	FROM(
		SELECT TemplateID,CONCAT(FrmTitleChi,'-',FrmCodChi,'-',ObjChi) as FrmInfoChi,CONCAT(FrmTitleEng,'-',FrmCodEng,'-',ObjEng) as FrmInfoEng
		FROM INTRANET_PA_S_FRMTPL WHERE IsActive=1 AND IsObs=1
	) as ipsf";
$a=$connection->returnResultSet($sql);
for($i=0;$i<sizeof($a);$i++){
	if($a[$i]["TemplateID"]==$templateID){
		$idx = $i;
		$templateDescr = Get_Lang_Selection($a[$i]["FrmInfoChi"],$a[$i]["FrmInfoEng"]);
	}
}
$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['CycleTemplate']['AcceptApprsialForm'])."\r\n";
$x .= "<table class=\"form_table_v30\">";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['CycleTemplate']['ApprsialForm']."</td>";
$x .= "<td>";
if($templateID=="" ||$templateID=="undefined"){
	$x .= getSelectByAssoArray($indexVar['libappraisal']->cnvGeneralArrToSelect($a, "TemplateID","FrmInfoChi","FrmInfoEng"), $selectionTags='id="DDLTemplateID" name="DDLTemplateID"', $SelectedType=$a[$idx]["TemplateID"], $all=0, $noFirst=0);
}
else{
	$x .= "<span>".$templateDescr."</span>";
}
$x .= "</td>";
$x .= "</tr></table><br/>";


$x .= "<input type=\"hidden\" id=\"CycleID\" name=\"CycleID\" value=".$cycleID.">"."\r\n";

$htmlAry['contentTbl'] = $x;
// ============================== Define Button ==============================
//$htmlAry['saveAsBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtbSaveAsObs'], "button", "goSaveAs()", 'saveAsBtn');
$htmlAry['submitBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnSave'], "button", "goSubmit()", 'submitBtn');
//$htmlAry['resetBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnReset'], "button", "goReset()", 'resetBtn');
$htmlAry['backBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBackCycle(".$cycleID.")", 'backBtn');
// ============================== Define Button ==============================







?>