<?php
// ============================== Related Tables ==============================

// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');

# Page Title
// ============================== Security Handling ==============================
$_POST['AcademicYearID'] = IntegerSafe($_POST['AcademicYearID']);
$_POST['DescrChi'] = cleanHtmlJavascript($_POST['DescrChi']);
$_POST['DescrEng'] = cleanHtmlJavascript($_POST['DescrEng']);
$_POST['CycleStart'] = (intranet_validateDate($_POST['CycleStart']))?$_POST['CycleStart']:false;
$_POST['CycleClose'] = (intranet_validateDate($_POST['CycleClose']))?$_POST['CycleClose']:false;
if($_POST['CycleStart']===false || $_POST['CycleClose']===false){
	$returnPath=sprintf($returnPath,$returnMsgKey).'&returnMsgKey=AddUnsuccess';
	//echo $returnPath;
	header($returnPath);
}
// ============================== Includes files/libraries ==============================
$connection = new libgeneralsettings();
$json = new JSON_obj();
$saveSuccess = true;
$returnPath=$_POST["returnPath"];
$returnPath = 'Location: ?task=management'.$appraisalConfig['taskSeparator'].'appraisal_period'.$appraisalConfig['taskSeparator'].'list&returnMsgKey=%s';
$cycleID = $_POST['CycleID'];

// Check if that academic year has already had a scheme
$sql = "SELECT CycleID FROM INTRANET_PA_C_CYCLE WHERE AcademicYearID='".$_POST['AcademicYearID']."'";
$hasCycleAlready = $connection->returnVector($sql);

if(empty($hasCycleAlready)){
	// Create new cycle
	$newCycleSql = "INSERT INTO INTRANET_PA_C_CYCLE (AcademicYearID, DescrChi, DescrEng, CycleStart, CycleClose, GuideFile, InputBy_UserID, DateTimeInput, ModifiedBy_UserID, DateTimeModified)
			SELECT '".$_POST['AcademicYearID']."', '".$_POST['DescrChi']."', '".$_POST['DescrEng']."', '".$_POST['CycleStart']."', '".$_POST['CycleClose']."', GuideFile, '".$UserID."', NOW(),'".$UserID."', NOW() FROM INTRANET_PA_C_CYCLE WHERE CycleID='".$cycleID."';";
	$connection->db_db_query($newCycleSql);
	$newCycleID = $connection->db_insert_id();
	
	/* Copy cycle interior */
	
	// Copy Form Selection
	$sql = "INSERT INTO INTRANET_PA_C_FRMSEL (CycleID,TemplateID,RelatedTemplates,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
			SELECT '".$newCycleID."',TemplateID,RelatedTemplates,'".$UserID."', NOW(),'".$UserID."', NOW() FROM INTRANET_PA_C_FRMSEL WHERE CycleID='".$cycleID."' ";
	$connection->db_db_query($sql);
	$sql = "INSERT INTO INTRANET_PA_C_BATCH (CycleID,TemplateID,AppRoleID,EditPrdFr,EditPrdTo,SecID,BatchOrder,is_special,set_order,set_name,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
			SELECT '".$newCycleID."',TemplateID,AppRoleID,EditPrdFr,EditPrdTo,SecID,BatchOrder,is_special,set_order,set_name,'".$UserID."', NOW(),'".$UserID."', NOW() FROM INTRANET_PA_C_BATCH WHERE CycleID='".$cycleID."' ";
	$connection->db_db_query($sql);
	$sql = "INSERT INTO INTRANET_PA_C_SPECIAL_BATCH_USER (CycleID,TemplateID,SetOrder,UserID,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
			SELECT '".$newCycleID."',TemplateID,SetOrder,UserID,'".$UserID."', NOW(),'".$UserID."', NOW() FROM INTRANET_PA_C_SPECIAL_BATCH_USER WHERE CycleID='".$cycleID."' ";
	$connection->db_db_query($sql);
	$sql = "INSERT INTO INTRANET_PA_C_FRMROL (CycleID,TemplateID,AppRoleID,UserID,is_special,set_order,set_name,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
			SELECT '".$newCycleID."',TemplateID,AppRoleID,UserID,is_special,set_order,set_name,'".$UserID."', NOW(),'".$UserID."', NOW() FROM INTRANET_PA_C_FRMROL WHERE CycleID='".$cycleID."' ";
	$connection->db_db_query($sql);
	$sql = "INSERT INTO INTRANET_PA_C_OBSSEL (CycleID,TemplateID,IsActive,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
			SELECT '".$newCycleID."',TemplateID,IsActive,'".$UserID."', NOW(),'".$UserID."', NOW() FROM INTRANET_PA_C_OBSSEL WHERE CycleID='".$cycleID."' ";
	$connection->db_db_query($sql);
	
	// Copy Form Group
	$sql = "SELECT GrpID FROM INTRANET_PA_C_GRPSUM WHERE CycleID='".$cycleID."'";
	$originalGrpList = $connection->returnVector($sql);
	foreach($originalGrpList as $oldGrp){
		$sql = "INSERT INTO INTRANET_PA_C_GRPSUM (CycleID,DescrChi,DescrEng,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
			SELECT '".$newCycleID."',DescrChi,DescrEng,'".$UserID."', NOW(),'".$UserID."', NOW() FROM INTRANET_PA_C_GRPSUM WHERE GrpID='".$oldGrp."' ";
		$connection->db_db_query($sql);
		$newGrpID= $connection->db_insert_id();
		
		$sql = "INSERT INTO INTRANET_PA_C_GRPMEM (GrpID,UserID,IsLeader,LeaderRole,CanCallModify,IsLeaderFill,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
			SELECT '".$newGrpID."',UserID,IsLeader,LeaderRole,CanCallModify,IsLeaderFill,'".$UserID."', NOW(),'".$UserID."', NOW() FROM INTRANET_PA_C_GRPMEM WHERE GrpID='".$oldGrp."' ";
		$connection->db_db_query($sql);
		$sql = "INSERT INTO INTRANET_PA_C_GRPFRM (GrpID,TemplateID,AppMode,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
				SELECT '".$newGrpID."',TemplateID,AppMode,Incl,'".$UserID."', NOW(),'".$UserID."', NOW() FROM INTRANET_PA_C_GRPFRM WHERE GrpID='".$oldGrp."' ";
		$connection->db_db_query($sql);
	}
	
	// Copy Form Idv
	$sql = "SELECT IdvID FROM INTRANET_PA_C_IDVFRM WHERE CycleID='".$cycleID."'";
	$originalIdvList = $connection->returnVector($sql);
	foreach($originalIdvList as $oldIdv){
		$sql = "INSERT INTO INTRANET_PA_C_IDVFRM (CycleID,TemplateID,UserID,Remark,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
			SELECT '".$newCycleID."',TemplateID,UserID,Remark,'".$UserID."', NOW(),'".$UserID."', NOW() FROM INTRANET_PA_C_IDVFRM WHERE IdvID='".$oldIdv."' ";
		$connection->db_db_query($sql);
		$newIdvID= $connection->db_insert_id();
		
		$sql = "INSERT INTO INTRANET_PA_C_IDVAPR (IdvID,AprID,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
				SELECT '".$newIdvID."',AprID,'".$UserID."', NOW(),'".$UserID."', NOW() FROM INTRANET_PA_C_IDVAPR WHERE IdvID='".$oldIdv."' ";
		$connection->db_db_query($sql);
	}
	$saveSuccess = true;
}else{
	$saveSuccess = false;
}

$returnMsgKey = ($saveSuccess)? 'AddSuccess' : 'AddUnsuccess';
$returnPath=sprintf($returnPath,$returnMsgKey).'&returnMsgKey='.$returnMsgKey;
//echo $returnPath;
header($returnPath);
?>