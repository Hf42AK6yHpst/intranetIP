<?php 
// ============================== Related Tables ==============================

// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');

# Page Title
// ============================== Includes files/libraries ==============================

// ============================== Includes files/libraries ==============================
$connection = new libgeneralsettings();
$type=$_REQUEST["Type"];
$q=$_REQUEST["?q"];
$addType = $_REQUEST["addType"];
// ============================== get Login User ==============================
if($type=="GetLoginUser"){
	$typeList = "1";		# 1-teacher only
	$name_field = getNameFieldByLang("USR.");
	if($addType=="Appraisee"){
		$sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='TeacherApprisal' AND SettingName='ExcludeAssessmentTeacher'";
		$condParm = current($connection->returnVector($sql));
		if($condParm!= ""){
			$conds = " AND USR.UserID NOT IN (".$condParm.")";
		}
	}
	$sql = "SELECT UserID,Name,ClassNumber,UserLogin
		FROM(			
			SELECT USR.UserID, $name_field as Name, ycu.ClassNumber, UserLogin 
			FROM ".$appraisalConfig['INTRANET_USER']." as USR LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) WHERE USR.RecordType IN ($typeList) AND USR.RecordStatus=1 $conds 
			GROUP BY USR.UserID ORDER BY UserLogin
		) as a
		WHERE UserLogin like '%".$q."%';
	";
	//echo $sql."<br/><br/>";
	$a=$connection->returnResultSet($sql);
	for ($i=0; $i<sizeof($a); $i++){
		//$x .= $a[$i]["UserLogin"]."|".$a[$i]["UserID"]."|".$a[$i]["Name"]."|".$a[$i]["ClassNumber"]."\n";		
		//$x .= $a[$i]["Name"]."|".$a[$i]["UserID"]."|".$a[$i]["UserLogin"]."\n";	
		$x .= $a[$i]["UserLogin"]."|".$a[$i]["UserID"]."|".$a[$i]["UserLogin"]."|".$a[$i]["Name"]."\n";
	}
	echo $x;
}else if($type=="GetSectionAvailableEditRole"){
	$sectionID = $_REQUEST["SectionID"];
	$specialSetNo = $_REQUEST["SpecialSetNo"];
	$index = $_REQUEST["Index"];
	$sql = "SELECT TemplateType FROM INTRANET_PA_S_FRMTPL ft 
			INNER JOIN INTRANET_PA_S_FRMSEC fs ON ft.TemplateID=fs.TemplateID
			WHERE SecID=$sectionID";
	$TemplateType = $connection->returnResultSet($sql);	
	if($TemplateType[0]['TemplateType']==0){
		$sql="SELECT ar.AppRoleID,ar.NameChi,ar.NameEng,ar.DisplayOrder,ar.IsActive,ar.SysUse FROM INTRANET_PA_S_APPROL ar WHERE ar.AppRoleID=2 AND ar.IsActive=1 ORDER BY ar.DisplayOrder";	
	}else{
		$sql="SELECT ar.AppRoleID,ar.NameChi,ar.NameEng,ar.DisplayOrder,ar.IsActive,ar.SysUse FROM INTRANET_PA_S_SECROL sr
			  INNER JOIN INTRANET_PA_S_APPROL ar ON sr.AppRoleID=ar.AppRoleID
			  WHERE SecID=".$sectionID." AND CanFill=1 AND ar.IsActive=1 ORDER BY ar.DisplayOrder		
		";
	}
	$appRoleList=$connection->returnResultSet($sql);
	echo getSelectByAssoArray($indexVar['libappraisal']->cnvGeneralArrToSelect($appRoleList, "AppRoleID","NameChi","NameEng"), $selectionTags='id="SecAppRoleID_'.$specialSetNo.'_'.$index.'" name="SecAppRoleID_'.$specialSetNo.'_'.$index.'"', "", $all=0, $noFirst=0);
	if(empty($appRoleList)){
		echo $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('NoRecordKeepWarnDiv_'.$specialSetNo.'_'.$index, $Lang['Appraisal']['CycleTemplate']['NoRecordKeep'], $Class='warnMsgDiv')."\r\n";
		echo $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('NoRoleWarnDiv_'.$specialSetNo.'_'.$index, $Lang['Appraisal']['Error']['TemplateSettingEditRoleMissing'], $Class='warnMsgDiv', true)."\r\n";	
	}else{
		echo $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('NoRecordKeepWarnDiv_'.$specialSetNo.'_'.$index, $Lang['Appraisal']['CycleTemplate']['NoRecordKeep'], $Class='warnMsgDiv')."\r\n";
		echo $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('NoRoleWarnDiv_'.$specialSetNo.'_'.$index, $Lang['Appraisal']['Error']['TemplateSettingEditRoleMissing'], $Class='warnMsgDiv')."\r\n";
	}	
}

?>