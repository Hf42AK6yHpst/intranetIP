<?php 
// ============================== Related Tables ==============================
//
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['AppraisalPeriod']);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
$cycleID = $_GET["cycleID"];
$templateID = $_GET["templateID"];

$connection = new libgeneralsettings();
//======================================================================== Header ========================================================================//
### navigation
$navigationAry[] = array($Lang['Appraisal']['AppraisalPeriod'], 'javascript: goBack();');
$navigationAry[] = array($Lang['Appraisal']['AppraisalPeriodSetting'], 'javascript: goBackCycle('.$cycleID.');');
$navigationAry[] = array($Lang['Appraisal']['AppraisalSecAppRoleSetting']);
$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);
### step box
/*$stepAry = array();
$stepAry[] = $Lang['Appraisal']['CycleHeader']['Steps1'];
$stepAry[] = $Lang['Appraisal']['CycleHeader']['Steps2'];
$stepAry[] = $Lang['Appraisal']['CycleHeader']['Steps3'];
$stepAry[] = $Lang['Appraisal']['CycleHeader']['Steps4'];
$htmlAry['customizedImportStepTbl'] = $indexVar['libappraisal_ui']->GET_IMPORT_STEPS($CurrStep=2, $stepAry);*/
//======================================================================== Content ========================================================================//
//======================================================================== 適用評審表格 ========================================================================//
$sql="SELECT CycleID,AppDescr,CycleStart,CycleClose
	FROM(
		SELECT CycleID,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as AppDescr,DATE_FORMAT(CycleStart,'%Y-%m-%d') as CycleStart,DATE_FORMAT(CycleClose,'%Y-%m-%d') as CycleClose
		FROM INTRANET_PA_C_CYCLE WHERE CycleID=".IntegerSafe($cycleID)."
	) as ipsf";
//echo $sql."<br/><br/>";
$a=$connection->returnResultSet($sql);
$cycleDescr=$a[0]["AppDescr"];
$cycleStart=$a[0]["CycleStart"];
$cycleClose=$a[0]["CycleClose"];
$x .= "<table class=\"form_table_v30\">";
$x .= "<tr><td class=\"field_title\"><b>".$Lang['Appraisal']['CycleTemplate']['CurrTemplate']."</b></td>";
$x .= "<td><a id='templateTitleID' class=\"tablelink\" href=\"javascript:goBackCycle(".$cycleID.")\">".$cycleDescr."</a></td></tr>";
$x .= "</table>";

//======================================================================== 適用評審表格 ========================================================================//$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['CycleTemplate']['AcceptApprsialForm'])."\r\n";
$sql="SELECT TemplateID,FrmInfoChi,FrmInfoEng,TemplateType
	FROM(
		SELECT TemplateID,CONCAT(FrmTitleChi,'-',FrmCodChi,'-',ObjChi) as FrmInfoChi,CONCAT(FrmTitleEng,'-',FrmCodEng,'-',ObjEng) as FrmInfoEng,TemplateType
		FROM INTRANET_PA_S_FRMTPL WHERE IsActive=1
	) as ipsf";
$a=$connection->returnResultSet($sql);
for($i=0;$i<sizeof($a);$i++){
	if($a[$i]["TemplateID"]==$templateID){
		$idx = $i;
		$templateDescr = Get_Lang_Selection($a[$i]["FrmInfoChi"],$a[$i]["FrmInfoEng"]);
		$templateType = $a[$i]["TemplateType"];
	}
}
$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['CycleTemplate']['AcceptApprsialForm'])."\r\n";
$x .= "<table class=\"form_table_v30\">";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['CycleTemplate']['ApprsialForm']."</td>";
$x .= "<td>";
if($templateID=="" ||$templateID=="undefined"){
	$x .= getSelectByAssoArray($indexVar['libappraisal']->cnvGeneralArrToSelect($a, "TemplateID","FrmInfoChi","FrmInfoEng"), $selectionTags='id="DDLTemplateID" name="DDLTemplateID"', $SelectedType=$a[$idx]["TemplateID"], $all=0, $noFirst=0);
}
else{
	$x .= "<span>".$templateDescr."</span>";
}
$x .= "</td>";
$x .= "</tr></table><br/>";
//======================================================================== 相關評審表格  ========================================================================//
$displayContent=($templateID!=null)?"block":"none";
$sql = "SELECT RelatedTemplates FROM INTRANET_PA_C_FRMSEL WHERE TemplateID='".$templateID."' AND CycleID=".IntegerSafe($cycleID);
$relatedTemplateList = current($connection->returnVector($sql));
$relatedTemplateArr = explode(",",$relatedTemplateList);

$sql="SELECT ipsf.TemplateID,FrmInfoChi,FrmInfoEng,TemplateType
	FROM(
		SELECT TemplateID,CONCAT(FrmTitleChi,'-',FrmCodChi,'-',ObjChi) as FrmInfoChi,CONCAT(FrmTitleEng,'-',FrmCodEng,'-',ObjEng) as FrmInfoEng,TemplateType
		FROM INTRANET_PA_S_FRMTPL WHERE IsActive=1 AND TemplateID NOT IN ('".$templateID."')
	) as ipsf
	INNER JOIN (
		SELECT TemplateID FROM INTRANET_PA_C_FRMSEL WHERE CycleID=".IntegerSafe($cycleID)."
		UNION
		SELECT TemplateID FROM INTRANET_PA_C_OBSSEL WHERE CycleID=".IntegerSafe($cycleID)."
	) as cfs ON ipsf.TemplateID=cfs.TemplateID";
$a=$connection->returnResultSet($sql);

$x .= "<div style=\"display:".$displayContent."\">".$indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['CycleTemplate']['RelatedApprsialForm'])."</div>"."\r\n";
$x .= "<div style=\"display:".$displayContent."\">";
$x .= "<table class=\"common_table_list\">";
$x .= "<thead>"."\r\n";
$x .= "<tr>";
$x .= "<th class=\"sub_row_top\" style=\"width:20%;\">".$Lang['Appraisal']['CycleTemplate']['ForPreView']."</th>";
$x .= "<th class=\"sub_row_top\" style=\"width:50%;\">".$Lang['Appraisal']['CycleTemplate']['AcceptApprsialForm']."</th>";
$x .= "</tr>";
$x .= "</thead>";
$x .= "<tbody>"."\r\n";
for($i=0;$i<sizeof($a);$i++){
	$templateDescr = Get_Lang_Selection($a[$i]["FrmInfoChi"],$a[$i]["FrmInfoEng"]);
	$isRelatedForm = (in_array($a[$i]['TemplateID'], $relatedTemplateArr))?"checked":"";
	$x .= "<tr>";
	$x .= "	<td><input type='checkbox' name='relatedTemplates[]' value='".$a[$i]['TemplateID']."' $isRelatedForm></td>";
	$x .= "	<td>".$templateDescr."</td>";
	$x .= "</tr>";
}
if(sizeof($a)==0){
	$x .= "<i><span class=\"row_content tabletextremark\">".$Lang['Appraisal']['Message']['NoRecordBrkt']."</span></i><br/>";
}
$x .= "</tbody>";
$x .= "</table></div></div><br/>";

$sql = "SELECT BatchID,CycleID,TemplateID,AppRoleID,set_order,set_name FROM INTRANET_PA_C_BATCH WHERE CycleID='".IntegerSafe($cycleID)."' AND TemplateID='".IntegerSafe($templateID)."' GROUP BY set_order ";
//echo $sql."<br/><br/>";
$batchSizeSet=$connection->returnResultSet($sql);
if(empty($batchSizeSet)){
	$batchSizeSet = array();
	$batchSizeSet[] = array(
		'CycleID'=>$cycleID,
		'TemplateID'=>$templateID,
		'set_order'=>1
	);
}
foreach($batchSizeSet as $batchSection){
	//======================================================================== 特別考績角色職員編配  ========================================================================//
	if($batchSection['set_order']==1){
		$x .= "<div class=\"initialSpecialSet\">";
		$x .= "<div id='special_set_select_users_div_".$batchSection['set_order']."' style='display:none;'>";
		$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['CycleTemplate']['SetupSpecialFormSettingUsers'])."\r\n";
		$x .= "<div>";
		$x .= "<table class=\"common_table_list ClassDragAndDrop\">";
		$x .= "<tr>";
		$x .= "<td style=\"width:50%;\">";
		//$x .= $Lang['Appraisal']['SearchByLoginID']." "."<input type=\"text\" id=\"MemberUserID\" name=\"MemberUserID\" value=\"\">";
		$x .= $Lang['Appraisal']['SearchByLoginID']." ".$indexVar['libappraisal_ui']->GET_BTN($Lang['Btn']['Select'], "button", "goAddSpecialSetUsers(".$batchSection['set_order'].")", 'selectBtn_'.$batchSection['set_order']);;
		$x .= "</td>";
		$x .= "<td style=\"width:50%;\">";
		$x .= "<select name=\"special_set_users_".$batchSection['set_order']."\" id=\"special_set_users_".$batchSection['set_order']."\" class=\"select_studentlist\" size=\"5\" multiple=\"multiple\">";
		
		$x .= "</select>";
		$x .= $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['RemoveSelected'], "button", "goDeleteSpecialSetUsers(".$batchSection['set_order'].")", 'submitBtn_'.$batchSection['set_order']);
		$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('EditDuplicatedUserDiv_'.$batchSection['set_order'], $Lang['Appraisal']['CycleTemplate']['DuplicatedSettingUsers'], $Class='warnMsgDiv')."\r\n";
		$x .= "</td>";
		$x .= "</tr>";
		$x .= "</table>";
		$x .= "</div>";
		$x .= "</div><br/>";
	}else{
		$sql = "SELECT UserID FROM INTRANET_PA_C_SPECIAL_BATCH_USER  WHERE CycleID=".IntegerSafe($cycleID)." AND TemplateID=".IntegerSafe($templateID)." AND SetOrder='".$batchSection['set_order']."' ";
		$sectionUserList = $connection->returnVector($sql);
		$x .= "<div class=\"newSpecialSet\" id=\"newSpecialSet".($batchSection['set_order']-1)."\">";
		$x .= "<br style=\"clear:both;\">";
		$x .= "<br>";
		$x .= "<p class=\"title\">#".($batchSection['set_order']-1).": ".$Lang['Appraisal']['CycleTemplate']['SpecialFormSettingName'].": <input type='text' id='newSpecialSetTitle".$batchSection['set_order']."' name='newSpecialSetTitle".$batchSection['set_order']."' value='".$batchSection['set_name']."'/></p>";
		$x .= "<div id='special_set_select_users_div_".$batchSection['set_order']."'>";
		$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['CycleTemplate']['SetupSpecialFormSettingUsers'])."\r\n";
		$x .= "<div>";
		$x .= "<table class=\"common_table_list ClassDragAndDrop\">";
		$x .= "<tr>";
		$x .= "<td style=\"width:50%;\">";
		//$x .= $Lang['Appraisal']['SearchByLoginID']." "."<input type=\"text\" id=\"MemberUserID\" name=\"MemberUserID\" value=\"\">";
		$x .= $Lang['Appraisal']['SearchByLoginID']." ".$indexVar['libappraisal_ui']->GET_BTN($Lang['Btn']['Select'], "button", "goAddSpecialSetUsers(".$batchSection['set_order'].")", 'selectBtn_'.$batchSection['set_order']);;
		$x .= "</td>";
		$x .= "<td style=\"width:50%;\">";
		$x .= "<select name=\"special_set_users_".($batchSection['set_order'])."\" id=\"special_set_users_".($batchSection['set_order'])."\" class=\"select_studentlist\" size=\"5\" multiple=\"multiple\">";
		foreach($sectionUserList as $setUser){
			$x .= "<option value=\"".$setUser."\">".$indexVar['libappraisal']->getUserNameID($setUser, 'name')."</option>";  
		}
		$x .= "</select>";
		$x .= $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['RemoveSelected'], "button", "goDeleteSpecialSetUsers(".$batchSection['set_order'].")", 'submitBtn_'.$batchSection['set_order']);
		$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('EditDuplicatedUserDiv_'.$batchSection['set_order'], $Lang['Appraisal']['CycleTemplate']['DuplicatedSettingUsers'], $Class='warnMsgDiv')."\r\n";
		$x .= "</td>";
		$x .= "</tr>";
		$x .= "</table>";
		$x .= "</div>";
		$x .= "</div><br/>";
	}
	
	$sql="SELECT ipsa.AppRoleID,AppName,UserID
		FROM(
			SELECT AppRoleID,".$indexVar['libappraisal']->getLangSQL("NameChi","NameEng")." as AppName FROM INTRANET_PA_S_APPROL WHERE SysUse=0 AND IsActive=1		
		) as ipsa
		LEFT JOIN(
			SELECT SecID,AppRoleID,CanBrowse,CanFill FROM INTRANET_PA_S_SECROL
			WHERE SecID IN (SELECT SecID FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND ParentSecID IS NULL)
			AND (CanBrowse=1 OR CanFill=1)
					GROUP BY SecID,AppRoleID
		) as ipsf ON ipsa.AppRoleID=ipsf.AppRoleID
		LEFT JOIN(
			SELECT CycleID,TemplateID,AppRoleID,UserID FROM INTRANET_PA_C_FRMROL WHERE CycleID=".IntegerSafe($cycleID)." AND TemplateID=".IntegerSafe($templateID)." AND set_order='".$batchSection['set_order']."'
		) as ipcf ON ipsa.AppRoleID=ipcf.AppRoleID
		
		GROUP BY ipsa.AppRoleID,AppName,UserID
		";
	//echo $sql."<br/><br/>";
	$a=$connection->returnResultSet($sql);
	$x .= "<div style=\"display:".$displayContent."\">".$indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['CycleTemplate']['ArrangeSpecialRole'])."</div>"."\r\n";
	$x .= "<div style=\"display:".$displayContent."\">";
	$x .= "<table class=\"common_table_list ClassDragAndDrop\">";
	$x .= "<thead>"."\r\n";
	$x .= "<tr>";
	$x .= "<th class=\"sub_row_top\" style=\"width:20%;\">".$Lang['Appraisal']['CycleTemplate']['SpecialRole']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:50%;\">".$Lang['Appraisal']['CycleTemplate']['SelectedStaff']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:30%;\">".$Lang['Appraisal']['CycleTemplate']['ArrangedStaff']."</th>";
	$x .= "</tr>";
	$x .= "</thead>";
	$x .= "<tbody>"."\r\n";
	$appRoleIDStr = "";
	for($i=0;$i<sizeof($a);$i++){
		$j=$i+1;	
		if($i==sizeof($a)-1){
			$appRoleIDStr.=$a[$i]["AppRoleID"];
		}
		else{
			$appRoleIDStr.=$a[$i]["AppRoleID"].",";
		}	
		//$Lang['Appraisal']['Message']['NoRecord']
		$x .= "<tr id=\"".$a[$i]['AppRoleID']."\">"."\r\n";
		$x .= "<td>".$a[$i]["AppName"]."</td>";	
		$x .= "<td class=\"tabletext\">";
		$x .= "<div style=\"display:inline\">";
			//$x .= $Lang['Appraisal']['SearchByLoginID']." "."<input type=\"text\" id=\"LoginID_".$i."\" name=\"LoginID_".$i."\" value=\"\">";	
		$x .= $indexVar['libappraisal']->Get_Staff_Selection("UserSel_".$batchSection['set_order']."_".$i, "UserSel_".$batchSection['set_order']."_".$i, $ParIndividual=true,$ParIsMultiple=false,$ParSize=20,$ParSelectedValue="",$ParOthers="",$ParAllStaffOption=false,$ShowOptGroupLabel=true,$LoginIDAsValue=false,$Currentonly=true,$ParEmptyFirst=true);	
		$x .= "<span id=\"LoadingPlace_".$batchSection['set_order']."_".$i."\"></span>";
		$x .= "</div>";
		$x .= "</td>";
		$x .= "<td>";
		$x .= "<span id=\"UserName_".$batchSection['set_order']."_".$i."\" name=\"UserName_".$batchSection['set_order']."_".$i."\">".$indexVar['libappraisal']->getUserNameID($a[$i]["UserID"],"name")."</span>";
		$divDisplay=($indexVar['libappraisal']->getUserNameID($a[$i]["UserID"],"login")!="")?"block":"none";
		$x .= "<div style=\"float:right;display:".$divDisplay."\" class=\"table_row_tool\" id='delete_".$batchSection['set_order']."_".$i."' ><a href=\"javascript:void(0)\" class=\"delete_dim\" onclick=javascript:goDelete('UserName_".$batchSection['set_order']."_".$i."') title=\"".$Lang['SysMgr']['FormClassMapping']['Delete']."\"></a></div>";		
		$x .= "</td>";
		$x .= "<input type=\"hidden\" id=\"UserID_".$batchSection['set_order']."_".$i."\" name=\"UserID_".$batchSection['set_order']."_".$i."\" value=".$a[$i]["UserID"].">"."\r\n";
		$x .= "<input type=\"hidden\" id=\"AppRoleID_".$batchSection['set_order']."_".$i."\" name=\"AppRoleID_".$batchSection['set_order']."_".$i."\" value=".$a[$i]["AppRoleID"].">"."\r\n";	
		$x .= "</tr>";
	}
	$x .= "</tbody>";
	$x .= "</table>";
	if(sizeof($a)==0){
		$x .= "<i><span class=\"row_content tabletextremark\">".$Lang['Appraisal']['Message']['NoRecordBrkt']."</span></i><br/>";
	}
	$x .= "</div><br/>";
	//======================================================================== 各章節遞交批次  ========================================================================//
	$sql="
		SELECT TemplateID,SecID,CycleID,BatchID,AppRoleID,EditPrdFr,EditPrdTo,CASE WHEN COALESCE(SecTitleCod,'')='' THEN '".$Lang['Appraisal']['CycleTemplate']['NoTitleSection']."' ELSE SecTitleCod END SecTitleCod,BatchOrder
		FROM(
			SELECT ipsf.TemplateID,ipsf.SecID,CycleID,BatchID,AppRoleID,EditPrdFr,EditPrdTo,".$indexVar['libappraisal']->getLangSQL("SecTitleCodChi","SecTitleCodEng")." as SecTitleCod,BatchOrder
			FROM(
				SELECT BatchID,CycleID,TemplateID,AppRoleID,DATE_FORMAT(EditPrdFr,'%Y-%m-%d') as EditPrdFr,DATE_FORMAT(EditPrdTo,'%Y-%m-%d') as EditPrdTo,SecID,BatchOrder
				FROM INTRANET_PA_C_BATCH WHERE CycleID=".IntegerSafe($cycleID)." AND TemplateID=".IntegerSafe($templateID)." AND set_order='".IntegerSafe($batchSection['set_order'])."'
			) as ipcb
			INNER JOIN(
				SELECT SecID,TemplateID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,CONCAT(SecCodChi,'-',SecTitleChi) as SecTitleCodChi,CONCAT(SecCodEng,'-',SecTitleEng) as SecTitleCodEng
				FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND ParentSecID IS NULL
			) as ipsf ON ipsf.TemplateID=ipcb.TemplateID AND ipsf.SecID=ipcb.SecID			 
		) as a
		ORDER BY BatchOrder";
	//echo $sql."<br/><br/>";;
	$a=$connection->returnResultSet($sql);
	$existRecords=sizeof($a);
	
	$sql="SELECT SecID,TemplateID,CASE WHEN COALESCE(SecTitleChi,'')='' THEN '".$Lang['Appraisal']['CycleTemplate']['NoTitleSection']."' ELSE SecTitleChi END SecTitleChi,
			CASE WHEN COALESCE(SecTitleEng,'')='' THEN '".$Lang['Appraisal']['CycleTemplate']['NoTitleSection']."' ELSE SecTitleEng END SecTitleEng	
			,SecCodChi,SecCodEng,CONCAT(SecCodChi,'-',SecTitleChi) as SecTitleCodChi,CONCAT(SecCodEng,'-',SecTitleEng) as SecTitleCodEng
		FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND ParentSecID IS NULL
		";
	//echo $sql."<br/><br/>";
	$sectionList=$connection->returnResultSet($sql);
	$sectionSize = sizeof($sectionList);
	$x .= "<div style=\"display:".$displayContent."\">".$indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['CycleTemplate']['SectionBatch'])."</div>"."\r\n";
	
	$subBtnAry = array();
	$subBtnAry[] = array('add', 'javascript: goAddBatch('.$batchSection['set_order'].');', $Lang['Appraisal']['BtbAddBatch'], $subBtnAry, "specialSetNo=1");
	if($templateID != "" && $templateID != "undefined"){
		$x .= $indexVar['libappraisal_ui']->Get_Content_Tool_By_Array_v30($subBtnAry);
	}
	
	$x .= "<div style=\"display:".$displayContent."\">";
	$x .= "<table class=\"common_table_list ClassDragAndDrop\">";
	$x .= "<thead>"."\r\n";
	$x .= "<tr>";
	$x .= "<th class=\"sub_row_top\" style=\"width:2%;\">#</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:18%;\">".$Lang['Appraisal']['CycleTemplate']['SelectedSection']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:50%;\">".$Lang['Appraisal']['CycleTemplate']['AppRole']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:30%;\">".$Lang['Appraisal']['CycleTemplate']['Deadline']."</th>";
	$x .= "</tr>";
	$x .= "</thead>";
	$x .= "<tbody>"."\r\n";
	$maxID="";
	$secIDStr = "";
	$batchIDStr = "";
	$rowIDStr = "";
	for($i=0;$i<sizeof($a);$i++){
		$j=$i+1;
		$maxID=$i;
		if($i==sizeof($a)-1){
			$secIDStr.=$a[$i]["SecID"];
			$batchIDStr.=$a[$i]["BatchID"];
			$rowIDStr.=$i;
		}
		else{
			$secIDStr.=$a[$i]["SecID"].",";
			$batchIDStr.=$a[$i]["BatchID"].",";
			$rowIDStr.=$i.",";
		}
		if($templateType==0){
			$sql="SELECT ar.AppRoleID,ar.NameChi,ar.NameEng,ar.DisplayOrder,ar.IsActive,ar.SysUse FROM INTRANET_PA_S_APPROL ar WHERE ar.AppRoleID=2 AND ar.IsActive=1 ORDER BY ar.DisplayOrder		
			";
		}else{
			$sql="SELECT ar.AppRoleID,ar.NameChi,ar.NameEng,ar.DisplayOrder,ar.IsActive,ar.SysUse FROM INTRANET_PA_S_SECROL sr
				  INNER JOIN INTRANET_PA_S_APPROL ar ON sr.AppRoleID=ar.AppRoleID
				  WHERE SecID=".$a[$i]["SecID"]." AND CanFill=1 AND ar.IsActive=1 ORDER BY ar.DisplayOrder		
			";
		}
		$appRoleList=$connection->returnResultSet($sql);
		$displayNotSetRole = false;
		if(empty($appRoleList)){
			$displayNotSetRole = true;		
		}
		$x .= "<tr id=\"SecRow_".$batchSection['set_order']."_".$i."\">"."\r\n";
		$x .= "<td>".$j."</td>";
		//$x .= "<td>".(($a[$i]["SecTitleCod"]!="")?$a[$i]["SecTitleCod"]:$Lang['Appraisal']['CycleTemplate']['NoTitleSection'])."</td>";
		$x .= "<td>";
			$x .= getSelectByAssoArray($indexVar['libappraisal']->cnvGeneralArrToSelect($sectionList, "SecID","SecTitleChi","SecTitleEng"), $selectionTags='id="SecTitleID_'.$batchSection['set_order'].'_'.$i.'" name="SecTitleID_'.$batchSection['set_order'].'_'.$i.'"', $SelectedType=$a[$i]["SecID"], $all=0, $noFirst=0);
		$x .= "</td>";
		$x .= "<td>";
			$x .= getSelectByAssoArray($indexVar['libappraisal']->cnvGeneralArrToSelect($appRoleList, "AppRoleID","NameChi","NameEng"), $selectionTags='id="SecAppRoleID_'.$batchSection['set_order'].'_'.$i.'" name="SecAppRoleID_'.$batchSection['set_order'].'_'.$i.'"', $SelectedType=$a[$i]["AppRoleID"], $all=0, $noFirst=0);
			$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('NoRecordKeepWarnDiv_'.$batchSection['set_order'].'_'.$i, $Lang['Appraisal']['CycleTemplate']['NoRecordKeep'], $Class='warnMsgDiv')."\r\n";
			$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('NoRoleWarnDiv_'.$batchSection['set_order'].'_'.$i, $Lang['Appraisal']['Error']['TemplateSettingEditRoleMissing'], $Class='warnMsgDiv', $displayNotSetRole)."\r\n";
		$x .= "</td>";
		$x .= "<td>";
			$x .= "<div style=\"display:inline;float:left\">";
			$x .= $indexVar['libappraisal_ui']->GET_DATE_PICKER('EditPrdFr_'.$batchSection['set_order'].'_'.$i,$a[$i]["EditPrdFr"],$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('EditPrdFr')",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum");
			$x .= "-".$indexVar['libappraisal_ui']->GET_DATE_PICKER('EditPrdTo_'.$batchSection['set_order'].'_'.$i,$a[$i]["EditPrdTo"],$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('EditPrdTo')",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum");
			$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('EditPrdEmptyWarnDiv_'.$batchSection['set_order'].'_'.$i, $Lang['Appraisal']['TemplateSample']['DateOverlap'], $Class='warnMsgDiv')."\r\n";
			$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('EditPrdOutCycleWarnDiv_'.$batchSection['set_order'].'_'.$i, $Lang['Appraisal']['TemplateSample']['DateOutCycle'], $Class='warnMsgDiv')."\r\n";
			$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('EditPrdInvalidRangeWarnDiv_'.$batchSection['set_order'].'_'.$i, $Lang['Appraisal']['TemplateSample']['DateInvalidRange'], $Class='warnMsgDiv')."\r\n";		
			$x .= "</div>";
			$x .= "<div style=\"display:inline;float:right\" class=\"table_row_tool\" id='delete_".$a[$i]['SecID']."'><a href=\"javascript:void(0)\" class=\"delete_dim\" id=\"delete_dim_".$batchSection['set_order']."_".$i."\" onclick=javascript:goDeleteBatch('".$i."','".$a[$i]['BatchID']."','".$batchSection['set_order']."') title=\"".$Lang['SysMgr']['FormClassMapping']['Delete']."\"></a></div>";
			$x .= "<input type=\"hidden\" id=\"BatchID_".$batchSection['set_order']."_".$i."\" name=\"BatchID_".$batchSection['set_order']."_".$i."\" value=\"".$a[$i]["BatchID"]."\">"; 
		$x .= "</td>";
		$x .= "</tr>";
		$testing123 = $indexVar['libappraisal_ui']->GET_DATE_PICKER('EditPrdFr_'.$i,$a[$i]["EditPrdFr"],$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('EditPrdFr')",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum");
	}
	for($i=sizeof($a);$i<sizeof($a)+20;$i++){
		$j=$i+1;
		$sql="SELECT AppRoleID,NameChi,NameEng,DisplayOrder,IsActive,SysUse
		FROM INTRANET_PA_S_APPROL WHERE IsActive=1
		ORDER BY DisplayOrder		
		";
		$appRoleList=$connection->returnResultSet($sql);
		$x .= "<tr id=\"SecRow_".$batchSection['set_order']."_".$i."\" style=\"display:none\" >"."\r\n";
		$x .= "<td>".$j."</td>";
		$x .= "<td>";
			$x .= getSelectByAssoArray($indexVar['libappraisal']->cnvGeneralArrToSelect($sectionList, "SecID","SecTitleChi","SecTitleEng"), $selectionTags='id="SecTitleID_'.$batchSection['set_order'].'_'.$i.'" name="SecTitleID_'.$batchSection['set_order'].'_'.$i.'"', $SelectedType=$a[$i]["SecID"], $all=0, $noFirst=0);
		$x .= "</td>";	
		$x .= "<td>";
		$x .= getSelectByAssoArray($indexVar['libappraisal']->cnvGeneralArrToSelect($appRoleList, "AppRoleID","NameChi","NameEng"), $selectionTags='id="SecAppRoleID_'.$batchSection['set_order'].'_'.$i.'" name="SecAppRoleID_'.$batchSection['set_order'].'_'.$i.'"', $SelectedType=$a[$i]["AppRoleID"], $all=0, $noFirst=0);
		$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('NoRoleWarnDiv_'.$batchSection['set_order'].'_'.$i, $Lang['Appraisal']['Error']['TemplateSettingEditRoleMissing'], $Class='warnMsgDiv')."\r\n";
		$x .= "</td>";
		$x .= "<td>";
		$x .= "<div style=\"display:inline;float:left\">";
		$x .= $indexVar['libappraisal_ui']->GET_DATE_PICKER('EditPrdFr_'.$batchSection['set_order'].'_'.$i,$a[$i]["EditPrdFr"],$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('EditPrdFr')",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum");
		$x .= "-".$indexVar['libappraisal_ui']->GET_DATE_PICKER('EditPrdTo_'.$batchSection['set_order'].'_'.$i,$a[$i]["EditPrdTo"],$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('EditPrdTo')",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum");
		$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('EditPrdEmptyWarnDiv_'.$batchSection['set_order'].'_'.$i, $Lang['Appraisal']['TemplateSample']['DateOverlap'], $Class='warnMsgDiv')."\r\n";
		$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('EditPrdOutCycleWarnDiv_'.$batchSection['set_order'].'_'.$i, $Lang['Appraisal']['TemplateSample']['DateOutCycle'], $Class='warnMsgDiv')."\r\n";
		$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('EditPrdInvalidRangeWarnDiv_'.$batchSection['set_order'].'_'.$i, $Lang['Appraisal']['TemplateSample']['DateInvalidRange'], $Class='warnMsgDiv')."\r\n";
		$x .= "</div>";
		$x .= "<div style=\"display:inline;float:right\" class=\"table_row_tool\" id='delete_".$a[$i]['SecID']."'><a href=\"javascript:void(0)\" class=\"delete_dim\"id=\"delete_dim_1_".$i."\" onclick=javascript:goDeleteBatch('".$i."','".$a[$i]['BatchID']."','1') title=\"".$Lang['SysMgr']['FormClassMapping']['Delete']."\"></a></div>";
		$x .= "<input type=\"hidden\" id=\"BatchID_".$batchSection['set_order']."_".$i."\" name=\"BatchID_".$batchSection['set_order']."_".$i."\" value=\"\">";
		$x .= "</td>";
		$x .= "</tr>";
	}
	$x .= "</tbody>";
	$x .= "</table>";
	$x .= "</div>";
	$x .= "<input type=\"hidden\" id=\"SecIDStr_".$batchSection['set_order']."\" name=\"SecIDStr_".$batchSection['set_order']."\" value=".$secIDStr.">"."\r\n";
	$x .= "<input type=\"hidden\" id=\"MaxSecID_".$batchSection['set_order']."\" name=\"MaxSecID_".$batchSection['set_order']."\" value=".$maxID.">"."\r\n";
	$x .= "<input type=\"hidden\" id=\"RowIDStr_".$batchSection['set_order']."\" name=\"RowIDStr_".$batchSection['set_order']."\" value=\"".$rowIDStr."\">"."\r\n";
	$x .= "</div>";
}

$x .= "<div class=\"sampleSpecialSet\" style=\"display: none\">";
$x .= "<br style=\"clear:both;\">";
$x .= "<br><p class=\"title\">#1: ".$Lang['Appraisal']['CycleTemplate']['SpecialFormSettingName'].": <input></p>";
$x .= "</div>";

$x .= "<div class=\"newSpecialSetList\" style=\"display: none\">";
$x .= "</div>";
	
$btnAry = array();
$btnAry[] = array('add', 'javascript: goAddSpecialSet();', $Lang['Appraisal']['CycleTemplate']['NewSpecialFormSetting']);
if($templateID != "" && $templateID != "undefined"){
	$x .= "<br>";
	$x .= $indexVar['libappraisal_ui']->Get_Content_Tool_By_Array_v30($btnAry);
}

$x .= "<input type=\"hidden\" id=\"CycleID\" name=\"CycleID\" value=".$cycleID.">"."\r\n";
$x .= "<input type=\"hidden\" id=\"TemplateID\" name=\"TemplateID\" value=".$templateID.">"."\r\n";
$x .= "<input type=\"hidden\" id=\"BatchIDStr\" name=\"BatchIDStr\" value=\"".$batchIDStr."\">"."\r\n";
$x .= "<input type=\"hidden\" id=\"CycleStart\" name=\"CycleStart\" value=\"".$cycleStart."\">"."\r\n";
$x .= "<input type=\"hidden\" id=\"CycleClose\" name=\"CycleClose\" value=\"".$cycleClose."\">"."\r\n";
$x .= "<input type=\"hidden\" id=\"ExistRecords\" name=\"ExistRecords\" value=\"".$existRecords."\">"."\r\n";
$x .= "<input type=\"hidden\" id=\"SectionSize\" name=\"SectionSize\" value=\"".$sectionSize."\">"."\r\n";
$x .= "<input type=\"hidden\" id=\"AppRoleIDStr\" name=\"AppRoleIDStr\" value=".$appRoleIDStr.">"."\r\n";
$x .= "<input type=\"hidden\" id=\"DelBatchIDStr\" name=\"DelBatchIDStr\" value=\"\">"."\r\n";
$x .= "<input type=\"hidden\" id=\"TotalSpecialSet\" name=\"TotalSpecialSet\" value=\"".(count($batchSizeSet)-1)."\">"."\r\n";
$x .= "<input type=\"hidden\" id=\"SpecialSetName\" name=\"SpecialSetName\" value=\"\">"."\r\n";

$htmlAry['contentTbl'] = $x;
// ============================== Define Button ==============================
$htmlAry['submitBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnSave'], "button", "goSubmit()", 'submitBtn');
//$htmlAry['resetBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnReset'], "button", "goReset()", 'resetBtn');
if($sys_custom['eAppraisal']['allowSaveToDraft'] && $templateType=="0"){
	$taSetting = $connection->Get_General_Setting('TeacherApprisal');
	$editPeriodUpdated = unserialize($taSetting['editPeriodUpdated']);
	if(!empty($editPeriodUpdated)){
		if($editPeriodUpdated[$cycleID."-".$templateID]==1){
			$htmlAry['reOpenBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnReOpen'], "button", "goReOpen()", 'reOpenBtn');
		}
	}
}
$htmlAry['backBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBackCycle(".$cycleID.")", 'backBtn');
// ============================== Define Button ==============================


?>