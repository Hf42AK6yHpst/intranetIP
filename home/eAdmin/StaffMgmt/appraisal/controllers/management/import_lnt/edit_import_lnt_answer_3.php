<?php
/**
 * Change Log:
 * 2020-01-07 Paul
 *  - Add back missing page switching tabs
 * 2018-01-05 Pun
 *  - Changed delete data to delete AcademicYear-Subject-YearClass-Record only
 */
// ============================== Related Tables ==============================
//
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================

@set_time_limit(30 * 60);

# Page Title
$curTab="lnt";
$TAGS_OBJ[] = array($Lang['Appraisal']['CycleTemplate']['Leave'], 'javascript: goLeave(0);', $curTab=='leave');
$TAGS_OBJ[] = array($Lang['Appraisal']['CycleTemplate']['AbsLateSub'], 'javascript: goAbsLateSub(0);', $curTab=='absLateSub');
$TAGS_OBJ[] = array($Lang['Appraisal']['CycleTemplate']['Attendance'], 'javascript: goAttendance(0);', $curTab=='attendance');
if($sys_custom['eAppraisal']['SLLateRecords']){
    $TAGS_OBJ[] = array($Lang['Appraisal']['SickLeaveAndLateRecords'], 'javascript: goSLLateSub(0);', $curTab=='slLateSub');
}
$TAGS_OBJ[] = array($Lang['Appraisal']['LNT']['LearningAndTeaching'], 'javascript: goLntSub(0)', $curTab=='lnt');
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
$AcademicYearID = $_POST["AcademicYearID"];
$filePath = $_POST["FilePath"];


include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");
include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");
include_once($PATH_WRT_ROOT."includes/appraisal/lnt/libappraisal_lnt.php");

$libLnt = new libappraisal_lnt();
//======================================================================== Header ========================================================================//
### navigation
$navigationAry[] = array($Lang['Appraisal']['ImportInformation']);
$navigationAry[] = array($Lang['Appraisal']['LNT']['ImportLnt']);
$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);
### step box
$stepAry = array();
$stepAry[] = $Lang['General']['ImportArr']['ImportStepArr']['SelectCSVFile'];
$stepAry[] = $Lang['General']['ImportArr']['ImportStepArr']['CSVConfirmation'];
$stepAry[] = $Lang['General']['ImportArr']['ImportStepArr']['ImportResult'];
$htmlAry['customizedImportStepTbl'] = $indexVar['libappraisal_ui']->GET_IMPORT_STEPS($CurrStep=3, $stepAry);
//======================================================================== Content ========================================================================//

### Check csv file format
$limport = new libimporttext();
$lo = new libfilesystem();

$AcademicYearID = IntegerSafe($AcademicYearID);

$record = 0;
if(file_exists($filePath)){
    ######################## Before import START ########################
    $deleteSubjectIdYearClassIdArr = array();
    
    // get subject-record mapping
    $sql = "SELECT YearClassID, SubjectID, RecordID FROM INTRANET_PA_LNT_ANSWER WHERE AcademicYearID='{$AcademicYearID}'";
    $rs = $libLnt->returnResultSet($sql);
    $subjectIdYearClassIdRecordIdMapping = array();
    foreach($rs as $r){
        $subjectIdYearClassIdRecordIdMapping[ "{$r['SubjectID']}_{$r['YearClassID']}" ][] = $r['RecordID'];
    }
    
    // get next ImportBatchID
    $sql = "SELECT MAX(ImportBatchID)+1 FROM INTRANET_PA_LNT_LOG";
    $rs = $libLnt->returnVector($sql);
    $newImportBatchID = $rs[0];
    
    // get next QuestionnaireID
    $sql = "SELECT MAX(QuestionnaireID) FROM INTRANET_PA_LNT_ANSWER WHERE AcademicYearID='{$AcademicYearID}'";
    $rs = $libLnt->returnVector($sql);
    $nextQuestionnaireID = (count($rs))? $rs[0] + 1 : 1;
    
    // get questions
    ######## Get question START ########
    $rs = $libLnt->getAllQuestionByAcademicYearID($AcademicYearID);
    
    $questions = array();
    foreach($rs as $r){
        if((int)$r['SubjectID'] != (int)$SubjectID){
            continue;
        }
        $questions[] = $r;
    }
    ######## Get question END ########
    ######################## Before import END ########################
    
    
    
    ######################## Import START ########################
    ### move to temp folder first for others validation
    $folder_prefix = $intranet_root."/file/import_temp/appraisal/lnt";
    
    $TargetFilePath = $filePath;
    
    ### Get Data from the csv file
    $ColumnTitleArr = array();
    $ColumnPropertyArr = array();
    
    $ColumnTitleArr[] = 'Teacher User Login';
    $ColumnTitleArr[] = 'Subject WebSAMSCode';
    $ColumnTitleArr[] = 'Subject Name (Ref.)';
    $ColumnTitleArr[] = 'ClassName';
    $ColumnPropertyArr[] = 1;
    $ColumnPropertyArr[] = 1;
    $ColumnPropertyArr[] = 0;
    $ColumnPropertyArr[] = 1;
    
    
    foreach($questions as $question){
        $ColumnTitleArr[] = "Part{$question['Part']}_Q{$question['QuestionNumber']}";
        $ColumnPropertyArr[] = 1;
    }
    
    
    
    $data = $limport->GET_IMPORT_TXT_WITH_REFERENCE($TargetFilePath, "", "", $ColumnTitleArr, $ColumnPropertyArr);
    $col_name = array_shift($data);
    $numRcd = count($data);
    
    
    #### Get all UserLogin/Subject/YearClass START ####
    $userLoginArr = array();
    $subjectArr = array();
    $classNameArr = array();
    for($i=0;$i<$numRcd;$i++){
        $userLoginArr[] = trim($data[$i][0]);
        $subjectArr[] = trim($data[$i][1]);
        $classNameArr[] = trim($data[$i][2]);
    }
    $userLoginArr = array_unique($userLoginArr);
    $subjectArr = array_unique($subjectArr);
    $classNameArr = array_unique($classNameArr);
    $userLoginSql = implode("','", $userLoginArr);
    $subjectSql = implode("','", $subjectArr);
    $classNameSql = implode("','", $classNameArr);
    
    $sql = "SELECT UserID, UserLogin FROM INTRANET_USER WHERE UserLogin IN ('{$userLoginSql}')";
    $rs = $libLnt->returnResultSet($sql);
    $userLoginArr = BuildMultiKeyAssoc($rs, array('UserLogin') , array('UserID'), $SingleValue=1);
    
    $sql = "SELECT RecordID AS SubjectID, CODEID FROM ASSESSMENT_SUBJECT WHERE CODEID IN ('{$subjectSql}') AND (CMP_CODEID = '' OR CMP_CODEID IS NULL) AND RecordStatus='1' ";
    $rs = $libLnt->returnResultSet($sql);
    $subjectArr = array();
    foreach($rs as $r){
        $subjectArr[strtoupper($r['CODEID'])] = $r['SubjectID'];
    }
    
    $sql = "SELECT
        YearClassID,
        ClassTitleEN,
        ClassTitleB5
    FROM
        YEAR_CLASS
    WHERE
        AcademicYearID = '{$AcademicYearID}'
    AND
        (
            ClassTitleEN IN ('{$classNameSql}')
        OR
            ClassTitleB5 IN ('{$classNameSql}')
        )
    ";
    $rs = $libLnt->returnResultSet($sql);
    $classNameArr = array();
    foreach($rs as $r){
        $classNameArr[strtoupper($r['ClassTitleEN'])] = $r['YearClassID'];
        $classNameArr[strtoupper($r['ClassTitleB5'])] = $r['YearClassID'];
    }
    #### Get all UserLogin/Subject/YearClass END ####
    
    $resultAnswer = array();
    for($i=0;$i<$numRcd;$i++){
        $teacherUserLogin = trim($data[$i][0]);
        $subjectCode = trim(strtoupper($data[$i][1]));
        $className = trim(strtoupper($data[$i][2]));
        
        $teacherID = $userLoginArr[$teacherUserLogin];
        $subjectID = $subjectArr[$subjectCode];
        $yearClassID = $classNameArr[$className];
        
        $questionnaireID = $nextQuestionnaireID++;
        
        foreach($questions as $index=>$question){
            $questionTitle = "Part{$question['Part']}_Q{$question['QuestionNumber']}";
            $questionID = $question['QuestionID'];
            $answer = addslashes(trim($data[$i][3 + $index]));
            
            if($question['QuestionType'] == libappraisal_lnt::LNT_QUESTION_TYPE_SCALE){
                if($answer == 0){
                    continue;
                }
            }
            
            $sql = "INSERT INTO `INTRANET_PA_LNT_ANSWER` (
                `AcademicYearID`,
                `SubjectID`,
                `YearClassID`,
                `QuestionID`,
                `TeacherID`,
                `QuestionnaireID`,
                `Answer`,
                `ImportBatchID`,
                `InputDate`,
                `Createdby`
            ) VALUES (
                '{$AcademicYearID}',
                '{$subjectID}',
                '{$yearClassID}',
                '{$questionID}',
                '{$teacherID}',
                '{$questionnaireID}',
                '{$answer}',
                '{$newImportBatchID}',
                NOW(),
                '{$_SESSION['UserID']}'
            )";
            $resultAnswer["Row#{$i}_{$questionTitle}"] = $libLnt->db_db_query($sql);
            
            $deleteSubjectIdYearClassIdArr[] = "{$subjectID}_{$yearClassID}";
        }
        
        $record++;
    }
    
    
    #### Delete data START ####
    $deleteRecordIdArr = array();
    $deleteSubjectIdYearClassIdArr = array_unique($deleteSubjectIdYearClassIdArr);
    foreach($deleteSubjectIdYearClassIdArr as $deleteSubjectIdYearClassId){
        $deleteRecordIdArr = array_merge($deleteRecordIdArr, (array)$subjectIdYearClassIdRecordIdMapping[$deleteSubjectIdYearClassId]);
    }
    $deleteRecordIdSql = implode("','", $deleteRecordIdArr);
    
    $sql = "DELETE FROM INTRANET_PA_LNT_ANSWER WHERE RecordID IN ('{$deleteRecordIdSql}')";
    $resultDeleteAnswer = $libLnt->db_db_query($sql);
    #### Delete data END ####
    
    
    #### Insert delete log START ####
    $remarks = '';
    $remarks .= "Delete answer SubjectID_YearClassID: [".implode(', ', $deleteSubjectIdYearClassIdArr)."]\n";
    if(!$resultDeleteAnswer){
        $remarks .= "Delete answer fail!!\n";
    }
    
    $insertAnswerCount = 0;
    foreach($resultAnswer as $question => $result){
        if($result){
            $insertAnswerCount++;
        }else{
            $remarks .= "Insert answer ({$question}) fail!!\n";
        }
    }
    
    $remarks .= "Insert answer success: {$insertAnswerCount}\n";
    
    $sql = "INSERT INTO `INTRANET_PA_LNT_LOG` (
        `Action`,
        `AcademicYearID`,
        `Remarks`,
        `ImportBatchID`,
        `InputDate`,
        `Createdby`
    ) VALUES (
        'IMPORT_ANSWER',
        '{$AcademicYearID}',
        '{$remarks}',
        '{$newImportBatchID}',
        NOW(),
        '{$_SESSION['UserID']}'
    );";
    $libLnt->db_db_query($sql);
    #### Insert delete log END ####
    
    unlink($folder_prefix.'/'.basename($TargetFilePath));
    ######################## Import END ########################
}


$x .= "<div align=\"center\">";
$x .= "<span>{$record} {$Lang['General']['ImportArr']['RecordsImportedSuccessfully']}</span>";
$x .= "</div>";

$htmlAry['contentTbl'] = $x;


// ============================== Define Button ==============================
$htmlAry['finishBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Done'], "button", "goBack()", 'finishBtn');
// ============================== Define Button ==============================




?>