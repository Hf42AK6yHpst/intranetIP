<?php
/**
 * Change Log:
 * 2020-01-07 Paul
 *  - Add back missing page switching tabs
 */
// ============================== Related Tables ==============================
//
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
# Page Title
$curTab="lnt";
$TAGS_OBJ[] = array($Lang['Appraisal']['CycleTemplate']['Leave'], 'javascript: goLeave(0);', $curTab=='leave');
$TAGS_OBJ[] = array($Lang['Appraisal']['CycleTemplate']['AbsLateSub'], 'javascript: goAbsLateSub(0);', $curTab=='absLateSub');
$TAGS_OBJ[] = array($Lang['Appraisal']['CycleTemplate']['Attendance'], 'javascript: goAttendance(0);', $curTab=='attendance');
if($sys_custom['eAppraisal']['SLLateRecords']){
    $TAGS_OBJ[] = array($Lang['Appraisal']['SickLeaveAndLateRecords'], 'javascript: goSLLateSub(0);', $curTab=='slLateSub');
}
$TAGS_OBJ[] = array($Lang['Appraisal']['LNT']['LearningAndTeaching'], 'javascript: goLntSub(0)', $curTab=='lnt');
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
$AcademicYearID = $_POST["AcademicYearID"];
$filePath = $_POST["FilePath"];


include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");
include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");
include_once($PATH_WRT_ROOT."includes/appraisal/lnt/libappraisal_lnt.php");

$libLnt = new libappraisal_lnt();
//======================================================================== Header ========================================================================//
### navigation
$navigationAry[] = array($Lang['Appraisal']['ImportInformation']);
$navigationAry[] = array($Lang['Appraisal']['LNT']['ImportLnt']);
$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);
### step box
$stepAry = array();
$stepAry[] = $Lang['General']['ImportArr']['ImportStepArr']['SelectCSVFile'];
$stepAry[] = $Lang['General']['ImportArr']['ImportStepArr']['CSVConfirmation'];
$stepAry[] = $Lang['General']['ImportArr']['ImportStepArr']['ImportResult'];
$htmlAry['customizedImportStepTbl'] = $indexVar['libappraisal_ui']->GET_IMPORT_STEPS($CurrStep=3, $stepAry);
//======================================================================== Content ========================================================================//

### Check csv file format
$limport = new libimporttext();
$lo = new libfilesystem();

$AcademicYearID = IntegerSafe($AcademicYearID);

$record = 0;
if(file_exists($filePath)){
    ######################## Read CSV START ########################
    ### move to temp folder first for others validation
    $folder_prefix = $intranet_root."/file/import_temp/appraisal/lnt";
    
    $TargetFilePath = $filePath;
    
    ### Get Data from the csv file
    $ColumnTitleArr = array();
    $ColumnTitleArr[] = 'Subject WebSAMSCode';
    $ColumnTitleArr[] = 'Subject Name (Ref.)';
    $ColumnTitleArr[] = 'Part';
    $ColumnTitleArr[] = 'Title';
    $ColumnTitleArr[] = 'Question Number';
    $ColumnTitleArr[] = 'Question';
    $ColumnTitleArr[] = 'Type';
    $ColumnTitleArr[] = 'Min';
    $ColumnTitleArr[] = 'Max';
    
    $ColumnPropertyArr = array_fill(0, 9, 1);
    
    $data = $limport->GET_IMPORT_TXT_WITH_REFERENCE($TargetFilePath, "", "", $ColumnTitleArr, $ColumnPropertyArr);
    $col_name = array_shift($data);
    $numRcd = count($data);
    ######################## Read CSV END ########################
    
    ######################## Before import START ########################
    #### Get all Subject START ####
    $sql = "SELECT RecordID AS SubjectID, CODEID FROM ASSESSMENT_SUBJECT WHERE (CMP_CODEID = '' OR CMP_CODEID IS NULL) AND RecordStatus='1' ";
    $rs = $libLnt->returnResultSet($sql);
    
    $subjectIdMapping = array();
    foreach($rs as $r){
        $subjectIdMapping[strtoupper($r['CODEID'])] = $r['SubjectID'];
    }
    #### Get all Subject END ####
    
    #### Get csv Subject START ####
    $containsGeneralSubject = false;
    $subjectIdArr = array();
    for($i=0;$i<$numRcd;$i++){
        $subjectCode = strtoupper(trim($data[$i][0]));
        if($subjectCode == ''){
            $containsGeneralSubject = true;
        }
        $subjectIdArr[] = $subjectIdMapping[strtoupper(trim($data[$i][0]))];
    }
    $subjectIdArr = array_unique($subjectIdArr);
    #### Get csv Subject END ####
    
    #### Get all SubjectID to be delete START ####
    if($containsGeneralSubject){
        $generalSubjectIdArr = $libLnt->getAllGeneralSubjectId($AcademicYearID);
        $filterSubjectIdArr = array_union($generalSubjectIdArr, $subjectIdArr);
        $deleteSubjectIdSql = implode("','", $filterSubjectIdArr);
        $deleteSubjectIdSql = "AND (SubjectID IS NULL OR SubjectID IN ('{$deleteSubjectIdSql}'))";
    }else{
        $deleteSubjectIdSql = implode("','", $subjectIdArr);
        $deleteSubjectIdSql = "AND SubjectID IN ('{$deleteSubjectIdSql}')";
    }
    #### Get all SubjectID to be delete END ####
    
    #### Get next ImportBatchID START ####
    $sql = "SELECT DISTINCT ImportBatchID FROM INTRANET_PA_LNT_QUESTION";
    $rs1 = $libLnt->returnVector($sql);
    $sql = "SELECT DISTINCT ImportBatchID FROM INTRANET_PA_LNT_ANSWER";
    $rs2 = $libLnt->returnVector($sql);
    $sql = "SELECT DISTINCT ImportBatchID FROM INTRANET_PA_LNT_LOG";
    $rs3 = $libLnt->returnVector($sql);
    $currentBatchIDs = array_merge($rs1, $rs2, $rs3);
    $newImportBatchID = (count($currentBatchIDs))? max($currentBatchIDs) + 1 : 1;
    #### Get next ImportBatchID END ####
    
    #### Delete the existing records START ####
    $sql = "DELETE FROM
        INTRANET_PA_LNT_QUESTION_PART
    WHERE
        AcademicYearID='{$AcademicYearID}'
        {$deleteSubjectIdSql}
    ";
        $resultDeletePart = $libLnt->db_db_query($sql);
        
        $sql = "DELETE FROM
        INTRANET_PA_LNT_QUESTION
    WHERE
        AcademicYearID='{$AcademicYearID}'
        {$deleteSubjectIdSql}
    ";
        $resultDeleteQuestion = $libLnt->db_db_query($sql);
        
        $sql = "DELETE FROM
        INTRANET_PA_LNT_ANSWER
    WHERE
        AcademicYearID='{$AcademicYearID}'
        {$deleteSubjectIdSql}
    ";
        $resultDeleteAnswer = $libLnt->db_db_query($sql);
        #### Delete the existing records END ####
        ######################## Before import END ########################
        
        
        
        ######################## Import START ########################
        $insertedPartArr = array();
        $partIdMapping = array();
        $partQuestionCount = array();
        $resultPart = array();
        $resultQuestion = array();
        for($i=0;$i<$numRcd;$i++){
            $subjectCode = strtoupper(trim($data[$i][0]));
            $subjectId = (isset($subjectIdMapping[$subjectCode]))?$subjectIdMapping[$subjectCode]:'NULL';
            $part = trim($data[$i][2]);
            $title = addslashes(trim($data[$i][3]));
            $qNum = trim($data[$i][4]);
            $qTitle = trim($data[$i][5]);
            $qType = trim(strtoupper($data[$i][6]));
            $qMin = trim($data[$i][7]);
            $qMax = trim($data[$i][8]);
            
            if(!in_array("{$part}_{$subjectId}", $insertedPartArr)){
                $sequence = count($insertedPartArr) + 1;
                $sql = "INSERT INTO `INTRANET_PA_LNT_QUESTION_PART` (
                `AcademicYearID`,
                `Part`,
                `Title`,
                `Sequence`,
                `ImportBatchID`,
                `InputDate`,
                `Createdby`,
                `SubjectID`
            ) VALUES (
                '{$AcademicYearID}',
                '{$part}',
                '{$title}',
                '{$sequence}',
                '{$newImportBatchID}',
                NOW(),
                '{$_SESSION['UserID']}',
                {$subjectId}
            );";
                $resultPart[$part] = $libLnt->db_db_query($sql);
                $partId = $libLnt->db_insert_id();
                
                $insertedPartArr[] = "{$part}_{$subjectId}";
                $partIdMapping["{$part}_{$subjectId}"] = $partId;
            }
            $partId = $partIdMapping["{$part}_{$subjectId}"];
            
            $sequence = ++$partQuestionCount[$part];
            $sql = "INSERT INTO `INTRANET_PA_LNT_QUESTION` (
            `AcademicYearID`,
            `PartID`,
            `QuestionNumber`,
            `Title`,
            `QuestionType`,
            `MinScore`,
            `MaxScore`,
            `Sequence`,
            `ImportBatchID`,
            `InputDate`,
            `Createdby`,
            `SubjectID`
        ) VALUES (
            '{$AcademicYearID}',
            '{$partId}',
            '{$qNum}',
            '{$qTitle}',
            '{$qType}',
            '{$qMin}',
            '{$qMax}',
            '{$sequence}',
            '{$newImportBatchID}',
            NOW(),
            '{$_SESSION['UserID']}',
            {$subjectId}
        );";
            $resultQuestion[$record] = $libLnt->db_db_query($sql);
            
            $record++;
        }
        
        
        
        #### Insert delete log START ####
        $remarks = '';
        $remarks .= "Delete question and answer [AcademicYearID={$AcademicYearID} ".addslashes($deleteSubjectIdSql)."]\n";
        if(!$resultDeletePart){
            $remarks .= "Delete part fail!!\n";
        }
        if(!$resultDeleteQuestion){
            $remarks .= "Delete question fail!!\n";
        }
        if(!$resultDeleteAnswer){
            $remarks .= "Delete answer fail!!\n";
        }
        
        $insertPartCount = 0;
        foreach($resultPart as $part => $result){
            if($result){
                $insertPartCount++;
            }else{
                $remarks .= "Insert part ({$part}) fail!!\n";
            }
        }
        
        $insertQuestionCount = 0;
        foreach($resultQuestion as $row => $result){
            if($result){
                $insertQuestionCount++;
            }else{
                $record = $row+1;
                $remarks .= "Insert question ({$record}) fail!!\n";
            }
        }
        
        $remarks .= "Insert part success: {$insertPartCount}\n";
        $remarks .= "Insert question success: {$insertQuestionCount}\n";
        
        $sql = "INSERT INTO `INTRANET_PA_LNT_LOG` (
        `Action`,
        `AcademicYearID`,
        `Remarks`,
        `ImportBatchID`,
        `InputDate`,
        `Createdby`
    ) VALUES (
        'IMPORT_QUESTION',
        '{$AcademicYearID}',
        '{$remarks}',
        '{$newImportBatchID}',
        NOW(),
        '{$_SESSION['UserID']}'
    );";
        $libLnt->db_db_query($sql);
        #### Insert delete log END ####
        
        unlink($folder_prefix.'/'.basename($TargetFilePath));
        ######################## Import END ########################
}


$x .= "<div align=\"center\">";
$x .= "<span>{$record} {$Lang['General']['ImportArr']['RecordsImportedSuccessfully']}</span>";
$x .= "</div>";

$htmlAry['contentTbl'] = $x;


// ============================== Define Button ==============================
$htmlAry['finishBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Done'], "button", "goBack()", 'finishBtn');
// ============================== Define Button ==============================




?>