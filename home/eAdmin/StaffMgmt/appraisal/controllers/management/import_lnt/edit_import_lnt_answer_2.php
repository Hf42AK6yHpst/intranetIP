<?php
/**
 * Change Log:
 * 2020-01-07 Paul
 *  - Add back missing page switching tabs
 */
// ============================== Related Tables ==============================
//
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
include_once($PATH_WRT_ROOT."includes/appraisal/lnt/libappraisal_lnt.php");

# Page Title
$curTab="lnt";
$TAGS_OBJ[] = array($Lang['Appraisal']['CycleTemplate']['Leave'], 'javascript: goLeave(0);', $curTab=='leave');
$TAGS_OBJ[] = array($Lang['Appraisal']['CycleTemplate']['AbsLateSub'], 'javascript: goAbsLateSub(0);', $curTab=='absLateSub');
$TAGS_OBJ[] = array($Lang['Appraisal']['CycleTemplate']['Attendance'], 'javascript: goAttendance(0);', $curTab=='attendance');
if($sys_custom['eAppraisal']['SLLateRecords']){
    $TAGS_OBJ[] = array($Lang['Appraisal']['SickLeaveAndLateRecords'], 'javascript: goSLLateSub(0);', $curTab=='slLateSub');
}
$TAGS_OBJ[] = array($Lang['Appraisal']['LNT']['LearningAndTeaching'], 'javascript: goLntSub(0)', $curTab=='lnt');
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");
include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");

$libLnt = new libappraisal_lnt();
//======================================================================== Header ========================================================================//
### navigation
$navigationAry[] = array($Lang['Appraisal']['ImportInformation']);
$navigationAry[] = array($Lang['Appraisal']['LNT']['ImportLnt']);
$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);
### step box
$stepAry = array();
$stepAry[] = $Lang['General']['ImportArr']['ImportStepArr']['SelectCSVFile'];
$stepAry[] = $Lang['General']['ImportArr']['ImportStepArr']['CSVConfirmation'];
$stepAry[] = $Lang['General']['ImportArr']['ImportStepArr']['ImportResult'];
$htmlAry['customizedImportStepTbl'] = $indexVar['libappraisal_ui']->GET_IMPORT_STEPS($CurrStep=2, $stepAry);
//======================================================================== Content ========================================================================//
### Check csv file format
$limport = new libimporttext();
$lo = new libfilesystem();
$name = $_FILES['ImportData']['name'];
$tmpName = $_FILES['ImportData']['tmp_name'];
$ext = strtoupper($lo->file_ext($name));
$err = false;
//echo $name." ".$ext." ".$tmpName." ".$cycleID."<br/><br/>";

if(!($ext == ".CSV" || $ext == ".TXT"))
{
    /*$returnPath = 'Location: ?task=management'.$appraisalConfig['taskSeparator'].'appraisal_period'.$appraisalConfig['taskSeparator'].'edit_import_leave_1&returnMsgKey=WrongFileFormat';
     echo $returnPath;
     header($returnPath);
     exit();*/
    $err = true;
    $htmlAry['navigation'] = "<div width=\"100%\"><div width=\"80%\" style=\"display:inline-block\">".$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry)."</div><div id=\"msgDiv\" class=\"systemmsg\" style=\"float:right;\" width=\"20%\"><span id=\"msg\">".$Lang['Appraisal']['Report']['InvalidFormat']."</span></div></div><br/>";;
    
}
### move to temp folder first for others validation
$folder_prefix = $intranet_root."/file/import_temp/appraisal/lnt";
if (!file_exists($folder_prefix))
    $lo->folder_new($folder_prefix);
    
    $TargetFileName = date('Ymd_His').'_'.$_SESSION['UserID'].$ext;
    $TargetFilePath = stripslashes($folder_prefix."/".$TargetFileName);
    
    $SuccessArr['MoveCsvFileToTempFolder'] = $lo->lfs_move($ImportData, $TargetFilePath);
    $SuccessArr['MoveCsvFileToTempFolder'] = "1";
    
    
    
    ######## Get question START ########
    $rs = $libLnt->getAllQuestionByAcademicYearID($AcademicYearID);
    
    $questions = array();
    foreach($rs as $r){
        if((int)$r['SubjectID'] != (int)$SubjectID){
            continue;
        }
        $questions[] = $r;
    }
    ######## Get question END ########
    
    ### Get Data from the csv file
    $ColumnTitleArr = array();
    $ColumnPropertyArr = array();
    
    $ColumnTitleArr[] = 'Teacher User Login';
    $ColumnTitleArr[] = 'Subject WebSAMSCode';
    $ColumnTitleArr[] = 'Subject Name (Ref.)';
    $ColumnTitleArr[] = 'ClassName';
    $ColumnPropertyArr[] = 1;
    $ColumnPropertyArr[] = 1;
    $ColumnPropertyArr[] = 0;
    $ColumnPropertyArr[] = 1;
    
    
    foreach($questions as $question){
        $ColumnTitleArr[] = "Part{$question['Part']}_Q{$question['QuestionNumber']}";
        $ColumnPropertyArr[] = 1;
    }
    
    $data = $limport->GET_IMPORT_TXT($TargetFilePath, "", "");
    $col_name = array_shift($data);
    array_shift($data);
    $numRcd = count($data);
    
    if($ColumnTitleArr != $col_name){
        $err = true;
        list($_, $msg) = explode('|=|', $Lang['General']['ReturnMessage']['WrongCSVHeader']);
        $htmlAry['navigation'] = "<div width=\"100%\"><div width=\"80%\" style=\"display:inline-block\">".$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry)."</div><div id=\"msgDiv\" class=\"systemmsg\" style=\"float:right;\" width=\"20%\"><span id=\"msg\">{$msg}</span></div></div><br/>";;
    }else if(count($questions) == 0){
        $err = true;
        $htmlAry['navigation'] = "<div width=\"100%\"><div width=\"80%\" style=\"display:inline-block\">".$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry)."</div><div id=\"msgDiv\" class=\"systemmsg\" style=\"float:right;\" width=\"20%\"><span id=\"msg\">{$Lang['Appraisal']['LNT']['ImportMsg']['EmptyQuestion']}</span></div></div><br/>";;
    }else if($err==false){
        
        $x .= "<table class=\"form_table_v30\">";
        $x .= "<tr>";
        $x .= "<td class=\"field_title\">".$Lang['General']['SuccessfulRecord']."</td>";
        $x .= "<td><div><span id=\"SuccessfulRecord\"></span></div></td>";
        $x .= "</tr>";
        $x .= "<tr>";
        $x .= "<td class=\"field_title\">".$Lang['General']['FailureRecord']."</td>";
        $x .= "<td><div><span id=\"FailureRecord\"></span></div></td>";
        $x .= "</tr>";
        $x .= "</table>";
        
        $x .= "<div id=\"ErrorTableDiv\">";
        $x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
        $x .= "<tbody>";
        $showHeader = true;
        $count = 1;
        $trueRecord=0;
        $falseRecord=0;
        
        
        #### Get all UserLogin/Subject/YearClass START ####
        $userLoginArr = array();
        $subjectArr = array();
        $classNameArr = array();
        for($i=0;$i<$numRcd;$i++){
            $data[$i][1] = strtoupper(trim($data[$i][1]));
            $userLoginArr[] = trim($data[$i][0]);
            $subjectArr[] = trim($data[$i][1]);
            $classNameArr[] = trim($data[$i][3]);
        }
        $userLoginArr = array_unique($userLoginArr);
        $subjectArr = array_unique($subjectArr);
        $classNameArr = array_unique($classNameArr);
        $userLoginSql = implode("','", $userLoginArr);
        $subjectSql = implode("','", $subjectArr);
        $classNameSql = implode("','", $classNameArr);
        
        $sql = "SELECT UserLogin FROM INTRANET_USER WHERE UserLogin IN ('{$userLoginSql}')";
        $userLoginArr = $libLnt->returnVector($sql);
        
        $sql = "SELECT RecordID AS SubjectID, CODEID FROM ASSESSMENT_SUBJECT WHERE CODEID IN ('{$subjectSql}') AND (CMP_CODEID = '' OR CMP_CODEID IS NULL) AND RecordStatus='1' ";
        $rs = $libLnt->returnResultSet($sql);
        $subjectArr = BuildMultiKeyAssoc($rs, array('CODEID') , array('SubjectID'), $SingleValue=1, $BuildNumericArray=0);
        $subjectArr = array_map('strtoupper', $subjectArr);
        
        $sql = "SELECT
    	ClassTitleEN,
    	ClassTitleB5
	FROM
	    YEAR_CLASS
	WHERE
	    AcademicYearID = '{$AcademicYearID}'
    AND
        (
    	    ClassTitleEN IN ('{$classNameSql}')
    	OR
    	    ClassTitleB5 IN ('{$classNameSql}')
    	)
	";
        $rs = $libLnt->returnResultSet($sql);
        $classNameArr = array();
        foreach($rs as $r){
            $classNameArr[] = $r['ClassTitleEN'];
            $classNameArr[] = $r['ClassTitleB5'];
        }
        #### Get all UserLogin/Subject/YearClass END ####
        
        
        for($i=0;$i<$numRcd;$i++){
            $err = false;
            $errArr = array();
            
            if(trim($data[$i][0]) == ''){
                $errArr[] = $Lang['Appraisal']['LNT']['ImportError']['Answer']['EmptyUser'];
                $err = true;
            }else if(!in_array(trim($data[$i][0]), $userLoginArr)){
                $errArr[] = $Lang['General']['ImportWarningArr']['WrongUserLogin'];
                $err = true;
            }
            
            if(trim($data[$i][1]) == ''){
                $errArr[] = $Lang['Appraisal']['LNT']['ImportError']['Answer']['EmptySubject'];
                $err = true;
            }else if(!isset($subjectArr[$data[$i][1]])){
                $errArr[] = $Lang['Appraisal']['LNT']['ImportError']['Answer']['NoSubject'];
                $err = true;
            }elseif($SubjectID && $SubjectID != $subjectArr[$data[$i][1]]){
                $errArr[] = $Lang['Appraisal']['LNT']['ImportError']['Answer']['WrongSubject'];
                $err = true;
            }
            
            if(trim($data[$i][3]) == ''){
                $errArr[] = $Lang['Appraisal']['LNT']['ImportError']['Answer']['EmptyClass'];
                $err = true;
            }else if(!in_array(trim($data[$i][3]), $classNameArr)){
                $errArr[] = $Lang['Appraisal']['LNT']['ImportError']['Answer']['NoClass'];
                $err = true;
            }
            
            foreach($questions as $index=>$question){
                $title = "Part{$question['Part']}_Q{$question['QuestionNumber']}";
                if($question['QuestionType'] == libappraisal_lnt::LNT_QUESTION_TYPE_SCALE){
                    $score = trim($data[$i][4 + $index]);
                    if($score == ''){
                        $errArr[] = "{$Lang['Appraisal']['LNT']['ImportError']['Answer']['EmptyAnswer']} ({$title})";
                        $err = true;
                    }else if(!is_numeric($score)){
                        $errArr[] = "{$Lang['Appraisal']['LNT']['ImportError']['Answer']['NotIntAnswer']} ({$title})";
                        $err = true;
                    }else if(
                        $score != 0 &&
                        ($score < $question['MinScore'] || $score > $question['MaxScore'])
                        ){
                            $errArr[] = "{$Lang['Appraisal']['LNT']['ImportError']['Answer']['AnswerOverMinMax']} ({$title})";
                            $err = true;
                    }
                }
            }
            
            if($err){
                $falseRecord++;
            }else{
                $trueRecord++;
            }
            
            if($showHeader==true && $err==true){
                $x .= "<tr>";
                $x .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\" >#</td>";
                $x .= "<td width=\"15%\" class=\"tablebluetop tabletopnolink\" >{$Lang['General']['UserLogin']}</td>";
                $x .= "<td width=\"15%\" class=\"tablebluetop tabletopnolink\" >{$Lang['SysMgr']['SubjectClassMapping']['Subject']}</td>";
                $x .= "<td width=\"20%\" class=\"tablebluetop tabletopnolink\" >{$Lang['SysMgr']['FormClassMapping']['ClassTitle']}</td>";
                $x .= "<td width=\"30%\" class=\"tablebluetop tabletopnolink\" >{$Lang['General']['Remark']}</td>";
                $x .= "</tr>";
                $showHeader = false;
            }
            
            $errMsg = implode('<br />', $errArr);
            $class = 'tablebluerow' . (($count%2)? '' : '2');
            if($err==true){
                $x .= "<tr style=\"vertical-align:top\">";
                $x .= "<td class=\"{$class}\">".($i+1)."</td>";
                $x .= "<td class=\"{$class}\">{$data[$i][0]}</td>";
                $x .= "<td class=\"{$class}\">{$data[$i][1]}</td>";
                $x .= "<td class=\"{$class}\">{$data[$i][3]}</td>";
                $x .= "<td class=\"{$class}\">".$errMsg."</td>";
                $x .= "</tr>";
                $count ++;
            }
        }
        $x .= "</tbody>";
        $x .= "</table>";
        $x .= "</div>";
    }
    else{
        $finalErr=$err;
    }
    $x .= "<input type=\"hidden\" id=\"AcademicYearID\" name=\"AcademicYearID\" value='{$AcademicYearID}'>\r\n";
    $x .= "<input type=\"hidden\" id=\"SubjectID\" name=\"SubjectID\" value='{$SubjectID}'>\r\n";
    $x .= "<input type=\"hidden\" id=\"FilePath\" name=\"FilePath\" value='".$TargetFilePath."'>\r\n";
    $x .= "<input type=\"hidden\" id=\"HiddenSuccessfulRecord\" name=\"HiddenSuccessfulRecord\" value=".$trueRecord.">\r\n";
    $x .= "<input type=\"hidden\" id=\"HiddenFailureRecord\" name=\"HiddenFailureRecord\" value=".$falseRecord.">\r\n";
    
    $htmlAry['contentTbl'] = $x;
    
    
    // ============================== Define Button ==============================
    
    if(!$err){
        $htmlAry['submitBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnImport'], "button", "goImport()", 'submitBtn', '', $Disabled=false);
    }
    //$htmlAry['backBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack('".$cycleID."')", 'backBtn');
    $htmlAry['cancelBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "goCancel()", 'cancelBtn');
    // ============================== Define Button ==============================
    
    ?>