<?php
/**
 * Change Log:
 * 2019-03-19 Pun
 *  - File created
 */
include_once($PATH_WRT_ROOT."includes/appraisal/lnt/libappraisal_lnt.php");

$libLnt = new libappraisal_lnt();

$AcademicYearID = IntegerSafe($AcademicYearID);
//$school_name = $_SESSION["SSV_PRIVILEGE"]["school"]["name"];


#### Get questions START ####
$rs = $libLnt->getAllQuestionByAcademicYearID($AcademicYearID);
$subjectIds = Get_Array_By_Key($rs, 'SubjectID');
$subjectIdSql = implode("','", $subjectIds);

$sql = "SELECT RecordID AS SubjectID, CH_DES, EN_DES FROM ASSESSMENT_SUBJECT WHERE RecordID IN ('{$subjectIdSql}') ";
$rs = $libLnt->returnResultSet($sql);

$subjects = array();
foreach($rs as $r){
    $subjects[$r['SubjectID']] = Get_Lang_Selection($r['CH_DES'],$r['EN_DES']);
}
#### Get questions END ####
$data = array(0 => $Lang['Appraisal']['LNT']['GeneralSubject']);
$data = ($data + $subjects); // Array merge and keep keys

echo getSelectByAssoArray($data, ' id="SubjectID" name="SubjectID"','',0,1);