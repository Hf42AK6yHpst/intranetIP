<?php
// ============================== Related Tables ==============================
// 
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================

$navigationAry[] = array($Lang['Appraisal']['AppraisalRecords'], 'javascript: goBack();');
$navigationAry[] = array($Lang['Appraisal']['AppraisalRecordsEdit']);
$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);


# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['AppraisalRecords']);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
$rlsNo = $_GET["rlsNo"];
$recordID = $_GET["recordID"];
$batchID = $_GET["batchID"];

$currentDate = date("Y-m-d");

$connection = new libgeneralsettings();

$sql = "SELECT iptf.RecordID,UserID,iptf.CycleID,CycleDescr,iptf.TemplateID,FormTitle,FormCode,HdrRef,Obj,Descr,AppRoleID,AppTgtChi,AppTgtEng,AcademicYearID,COALESCE(SubDate,'') as SubDate,
		EditPrdFr,EditPrdTo,CASE WHEN NOW()>EditPrdTo THEN '1' ELSE '0' END as OverdueDate,YearClassID,SubjectID,TeachLang,NeedSubj,NeedClass,FillerID,CycleStart,CycleClose,ModDateFr,ModDateTo,ModRemark,NeedSubjLang
		FROM(
			SELECT RecordID,CycleID,TemplateID,UserID FROM INTRANET_PA_T_FRMSUM WHERE RecordID=".IntegerSafe($recordID)."
		) as iptf
		INNER JOIN(
			SELECT TemplateID,".$indexVar['libappraisal']->getLangSQL("FrmTitleChi","FrmTitleEng")." as FormTitle,"
			.$indexVar['libappraisal']->getLangSQL("FrmCodChi","FrmCodEng")." as FormCode,"
			.$indexVar['libappraisal']->getLangSQL("HdrRefChi","HdrRefEng")." as HdrRef,"
			.$indexVar['libappraisal']->getLangSQL("ObjChi","ObjEng")." as Obj,"
			.$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr,
			AppTgtChi,AppTgtEng,NeedSubj,NeedClass,NeedSubjLang
			FROM INTRANET_PA_S_FRMTPL
		) as ipsf ON iptf.TemplateID=ipsf.TemplateID
		INNER JOIN(
			SELECT CycleID,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as CycleDescr,AcademicYearID,DATE_FORMAT(CycleStart,'%Y-%m-%d') as CycleStart,DATE_FORMAT(CycleClose,'%Y-%m-%d') as CycleClose
			FROM INTRANET_PA_C_CYCLE
		) as ipcc ON iptf.CycleID=ipcc.CycleID
		INNER JOIN(
			SELECT BatchID,CycleID,EditPrdFr,CONCAT(DATE_FORMAT(EditPrdTo,'%Y-%m-%d'),' 23:59:59') as EditPrdTo,AppRoleID FROM INTRANET_PA_C_BATCH WHERE BatchID=".IntegerSafe($batchID)."
		) as ipcb ON ipcc.CycleID=ipcb.CycleID
		INNER JOIN(
			SELECT RlsNo,RecordID,BatchID,SubDate,FillerID,DATE_FORMAT(ModDateFr,'%Y-%m-%d') as ModDateFr,DATE_FORMAT(ModDateTo,'%Y-%m-%d') as ModDateTo, ModRemark
					FROM INTRANET_PA_T_FRMSUB WHERE RlsNo=".IntegerSafe($rlsNo). " AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)."
		) as iptfrmSub ON iptf.RecordID=iptfrmSub.RecordID AND ipcb.BatchID=iptfrmSub.BatchID
		LEFT JOIN(
			SELECT RlsNo,RecordID,BatchID,YearClassID,SubjectID,TeachLang FROM INTRANET_PA_T_SLF_HDR WHERE RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)."
		) as iptsh ON iptf.RecordID=iptsh.RecordID AND iptfrmSub.RlsNo=iptsh.RlsNo AND iptfrmSub.BatchID=iptsh.BatchID;";
//echo $sql."<br/>";
$header=$connection->returnResultSet($sql);
$templateID=$header[0]["TemplateID"];
$cycleID=$header[0]["CycleID"];
$cycleStart=$header[0]["CycleStart"];
$cycleClose=$header[0]["CycleClose"];
$userID = $header[0]["UserID"];
$fillerID = $header[0]["FillerID"];
if($_GET["modification"] == 1 && strstr($_SERVER['HTTP_REFERER'],'modification_list.modification')){
    $canEdit = "1";
} else {
    $canEdit = ($header[0]["OverdueDate"]=="0"&&$header[0]["SubDate"]=="")?"1":"0";
}
if($canEdit && $fillerID!=$_SESSION['UserID']){
	$canEdit = "0";
}
// $canEdit=($header[0]["OverdueDate"]=="0"&&$header[0]["SubDate"]=="")?"1":"0";
$academicYearID=$header[0]["AcademicYearID"];
$appRole=Get_Lang_Selection($header[0]["AppTgtChi"],$header[0]["AppTgtEng"]);
$appRoleID=$header[0]["AppRoleID"];
$modDateFr=$header[0]["ModDateFr"];
$modDateTo=$header[0]["ModDateTo"];
$modRemark=$header[0]["ModRemark"];
/*
$sql="SELECT RecordID,BatchID,SubDate,CASE WHEN SubDate IS NULL OR (NOW()<=SubDate) THEN 1 ELSE 0 END as CanEdit FROM INTRANET_PA_T_FRMSUB WHERE RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
$a=$connection->returnResultSet($sql);
*/
//======================================================================== View Related Form ========================================================================//
$sql = "SELECT RelatedTemplates FROM INTRANET_PA_C_FRMSEL WHERE TemplateID='".$templateID."' AND CycleID=".IntegerSafe($cycleID);
$relatedTemplateList = current($connection->returnVector($sql));
if(!empty($relatedTemplateList)){
	$sql = "SELECT TemplateID,CONCAT(FrmTitleChi,'-',FrmCodChi,'-',ObjChi) as FrmInfoChi,CONCAT(FrmTitleEng,'-',FrmCodEng,'-',ObjEng) as FrmInfoEng,TemplateType
		FROM INTRANET_PA_S_FRMTPL WHERE IsActive=1 AND TemplateID IN (".$relatedTemplateList.")";
	$relatedTemplateData = $connection->returnArray($sql);
	foreach($relatedTemplateData as $rtd){	
		$btnAry[] = array('view_stat', 'javascript:goViewRelated('.$rtd['TemplateID'].')',  Get_Lang_Selection($rtd["FrmInfoChi"],$rtd["FrmInfoEng"]));
	}
	$htmlAry['contentTool'] = $indexVar['libappraisal_ui']->Get_Content_Tool_By_Array_v30($btnAry);
}
//======================================================================== Section ========================================================================//
$sql="SELECT ipsf.SecID,TemplateID,SecTitle,SecCod,SecDescr,DisplayOrder
		FROM(
			SELECT SecID,TemplateID,".$indexVar['libappraisal']->getLangSQL("SecTitleChi","SecTitleEng")." as SecTitle,".$indexVar['libappraisal']->getLangSQL("SecCodChi","SecCodEng")." as SecCod,".
			$indexVar['libappraisal']->getLangSQL("SecDescrChi","SecDescrEng")." as SecDescr,DisplayOrder FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND ParentSecID IS NULL
		) as ipsf 				
		ORDER BY DisplayOrder;
		";
//echo $sql."<br/><br/>";
$sectionHeader=$connection->returnResultSet($sql);

// prepare the QID for that form which can be filled
$sql="SELECT ipsf.SecID,TemplateID,SecTitle,SecCod,SecDescr,DisplayOrder
		FROM(
			SELECT SecID,TemplateID,".$indexVar['libappraisal']->getLangSQL("SecTitleChi","SecTitleEng")." as SecTitle,".$indexVar['libappraisal']->getLangSQL("SecCodChi","SecCodEng")." as SecCod,".
			$indexVar['libappraisal']->getLangSQL("SecDescrChi","SecDescrEng")." as SecDescr,DisplayOrder FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND ParentSecID IS NULL
		) as ipsf
		INNER JOIN(
			SELECT SecID,AppRoleID,CanFill FROM INTRANET_PA_S_SECROL WHERE AppRoleID=".$appRoleID." AND CanFill=1  	
		) as ipss ON ipsf.SecID=ipss.SecID
		INNER JOIN(
			SELECT SecID,AppRoleID FROM INTRANET_PA_C_BATCH WHERE AppRoleID=".$appRoleID." AND BatchID='".$batchID."' AND TemplateID='".$templateID."'  	
		) as ipbs ON ipsf.SecID=ipbs.SecID
		ORDER BY DisplayOrder;
		";
//echo $sql."<br/><br/>";
$y=$connection->returnResultSet($sql);

$parSecID=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($y,"SecID");
$sql="SELECT SecID,TemplateID,SecTitle,SecCod,SecDescr,DisplayOrder,ParentSecID
		FROM(
			SELECT SecID,TemplateID,".$indexVar['libappraisal']->getLangSQL("SecTitleChi","SecTitleEng")." as SecTitle,".$indexVar['libappraisal']->getLangSQL("SecCodChi","SecCodEng")." as SecCod,".
			$indexVar['libappraisal']->getLangSQL("SecDescrChi","SecDescrEng")." as SecDescr,DisplayOrder,ParentSecID FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND ParentSecID IS NOT NULL
			AND ParentSecID IN (".$parSecID.")
		) as ipsf";
		
//echo $sql."<br/><br/>";
$y=$connection->returnResultSet($sql);
$subSecID=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($y,"SecID");
$sql="SELECT QCatID,SecID,".$indexVar['libappraisal']->getLangSQL("QCatCodChi","QCatCodEng")." as QCatCod,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as QCatDescr,".
			"DisplayOrder as QCatDisplayOrder FROM INTRANET_PA_S_QCAT WHERE SecID IN (".$subSecID.")";
$y=$connection->returnResultSet($sql);
$qCatID=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($y,"QCatID");
$sql="SELECT QID,QCatID FROM INTRANET_PA_S_QUES WHERE QCatID IN (".$qCatID.")";
//echo $sql."<br/><br/>";
$y=$connection->returnResultSet($sql);
$qID=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($y,"QID");
// prepare the QID for that form which can be filled


//======================================================================== Personal Master ========================================================================//
$x .= "<div class=\"formTitle\">";
$x .= $header[0]["FormTitle"];
if($header[0]["CycleDescr"]!=""){
$x .= "(".$header[0]["CycleDescr"].")";
}
$x .= "</div>"."\r\n";
if($modRemark!=""){
	$x .= "<br><div>";
	$x .= "<span class=\"formObjective\" style=\"float: left;color: red;\">".nl2br($modRemark)."</span>";
	$x .= "</div>"."\r\n<br><br>";
}
$x .= "<div>";
	if($header[0]["FormCode"]!=""&&$header[0]["Obj"]!=""){
		$x .= "<span class=\"formObjective\" style=\"float: left;\">".$header[0]["FormCode"]." - ".$header[0]["Obj"]."</span>";
	}
	else if($header[0]["FormCode"]!=""&&$header[0]["Obj"]==""){
		$x .= "<span class=\"formObjective\" style=\"float: left;\">".$header[0]["FormCode"]."</span>";
	}
	else if($header[0]["FormCode"]==""&&$header[0]["Obj"]!=""){
		$x .= "<span class=\"formObjective\" style=\"float: left;\">".$header[0]["Obj"]."</span>";
	}
	else if($header[0]["FormCode"]==""&&$header[0]["Obj"]==""){
		$x .= "<span class=\"formObjective\" style=\"float: left;\"></span>";
	}
	if($header[0]["Descr"]!=""){
		$x .= "<br/><br/><span>".$header[0]["Descr"]."</span>";
	}
	$x .= "<span class=\"formRefHeader\" style=\"float: right;\">".$header[0]["HdrRef"]."</span>";
	$x .="</div>"."\r\n";
$x .= "<div>".$header[0]["Descr"]."</div>".($header[0]["Descr"]!="")?"<br/>":""."\r\n";

$sql = "SELECT UserID,EnglishName,ChineseName,Gender
		FROM(
			SELECT UserID,EnglishName,ChineseName,Gender FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID=".IntegerSafe($userID)."
		) as iu
		";
$iu=$connection->returnResultSet($sql);
$sql = "SELECT UserID,EnglishName,ChineseName,Gender
		FROM(
			SELECT UserID,EnglishName,ChineseName,Gender FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID=".IntegerSafe($fillerID)."
		) as iu
		";
$filler=$connection->returnResultSet($sql);
$tgtName = ($_SESSION['intranet_session_language']=="en")?$Lang['Appraisal']['ARFormTgtName'].$header[0]["AppTgtEng"]:$header[0]["AppTgtChi"].$Lang['Appraisal']['ARFormTgtName'];
$x .= "<table class=\"form_table_v30\">";
if($iu[0]["ChineseName"]==""){
	$x .= "<tr><td class=\"field_title\">".$tgtName."</td><td colspan=\"3\">".$iu[0]["EnglishName"]."</td></tr>"."\r\n";
}else{
	$x .= "<tr><td class=\"field_title\">".$tgtName."</td><td colspan=\"3\">".$iu[0]["EnglishName"]."(".$iu[0]["ChineseName"].")"."</td></tr>"."\r\n";
}
$needSubjCheck = false;
if($header[0]["NeedSubj"]=="1"){
	$needSubjCheck = true;
	$subjectSQL=$indexVar['libappraisal']->getAllSubject();
	$x .="<tr><td class=\"field_title required\"><span class=\"tabletextrequire\">*</span>".$Lang['Appraisal']['ARFormSubject']."</td><td>".getSelectByAssoArray($indexVar['libappraisal']->cnvGeneralArrToSelect($subjectSQL, "SubjectID","SubjectTitleB5","SubjectTitleEN"), $selectionTags='id="subjectID" name="subjectID" class="required"', $SelectedType=$header[0]["SubjectID"], $all=0, $noFirst=0)."</td>";
	$x .= "<td id=\"subjectEmptyWarnDiv\" style=\"display:none; color:red;\">".$Lang['Appraisal']['ObsFormSbjMissing']."</td>";
	if($header[0]["NeedSubjLang"]=="1"){
		$x .="<td class=\"field_title\">".$Lang['Appraisal']['ARFormSubjectMedium']."</td><td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('teachLang', 'teachLang', $header[0]["TeachLang"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td>";
	}
	$x .= "</tr>";
}
$needClassCheck = false;
if($header[0]["NeedClass"]=="1"){
	$needClassCheck = true;
	$yearClassSQL=$indexVar['libappraisal']->getAllYearClass($academicYearID);
	$x .="<tr><td class=\"field_title required\"><span class=\"tabletextrequire\">*</span>".$Lang['Appraisal']['ARFormYearClass']."</td><td>".getSelectByAssoArray($indexVar['libappraisal']->cnvGeneralArrToSelect($yearClassSQL, "YearClassID","ClassTitleB5","ClassTitleEN"), $selectionTags='id="yearClassID" name="yearClassID" class="required"', $SelectedType=$header[0]["YearClassID"], $all=0, $noFirst=0)."</td><td id=\"classEmptyWarnDiv\" style=\"display:none; color:red;\" colspan=\"2\">".$Lang['Appraisal']['ObsFormClassMissing']."</td></tr>";;
}
$x .= "</table>".($header[0]["NeedClass"]!="0" || $header[0]["NeedClass"]!="0")?"<br/>":"\r\n";;
if($header[0]["NeedClass"]!="0" || $header[0]["NeedClass"]!="0"){
	$appTag = "<br/>";
	$x .= "<div style=\"color:#999999\">".sprintf($Lang['Appraisal']['TemplateSample']['Remark'],"<span style=\"color:#FF0000\">*</span>")."</div>";
}
else{
	$appTag = "\r\n";
}
$x .= "</table>".$appTag;
//======================================================================== Personal Master ========================================================================//
for($h=0;$h<sizeof($sectionHeader);$h++){
	// to check the section security
	$sectionSecurity=$indexVar['libappraisal']->getSectionUserRole($userID,$recordID,$batchID,$sectionHeader[$h]["SecID"],$rlsNo);
	if($sectionSecurity[0]["CanFill"]==0 && $h<sizeof($sectionHeader)-1){
		$nonEditDivID .= "#SecID_".$sectionHeader[$h]["SecID"].",";
	}
	else if($sectionSecurity[0]["CanFill"]==0 && $h==sizeof($sectionHeader)-1){
		$nonEditDivID .= "#SecID_".$sectionHeader[$h]["SecID"];
	}
	
	if($sectionSecurity[0]["CanBrowse"]==1){
		if(sizeof($sectionSecurity)>0){	
			$x .= "<div id=\"SecID_".$sectionHeader[$h]["SecID"]."\" name=\"SecID_".$sectionHeader[$h]["SecID"]."\">";		
			if($sectionHeader[$h]["SecCod"]!="" && $sectionHeader[$h]["SecTitle"]!=""){
				$x .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($sectionHeader[$h]["SecCod"]."-".$sectionHeader[$h]["SecTitle"])."<br/>\r\n";
			}
			else if($sectionHeader[$h]["SecCod"]!="" && $sectionHeader[$h]["SecTitle"]==""){
				$x .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($sectionHeader[$h]["SecCod"])."<br/>\r\n";
			}
			else if($sectionHeader[$h]["SecCod"]=="" && $sectionHeader[$h]["SecTitle"]!=""){
				$x .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($sectionHeader[$h]["SecTitle"])."<br/>\r\n";
			}
			$x .= "</div><br/>\r\n";
			$x .= "<table>";
			$x .= "<tr><td>".nl2br($sectionHeader[$h]["SecDescr"])."</td></tr></table>".(($sectionHeader[$h]["SecDescr"]!="")?"<br/>":""."\r\n");
			//======================================================================== Sub Section ========================================================================//
			$sql="SELECT SecID,TemplateID,SecTitle,SecCod,SecDescr,DisplayOrder,ParentSecID
					FROM(
						SELECT SecID,TemplateID,".$indexVar['libappraisal']->getLangSQL("SecTitleChi","SecTitleEng")." as SecTitle,".$indexVar['libappraisal']->getLangSQL("SecCodChi","SecCodEng")." as SecCod,".
						$indexVar['libappraisal']->getLangSQL("SecDescrChi","SecDescrEng")." as SecDescr,DisplayOrder,ParentSecID FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND ParentSecID IS NOT NULL
						AND ParentSecID=".IntegerSafe($sectionHeader[$h]["SecID"])."
					) as ipsf 		
					ORDER BY DisplayOrder;
					";
			//echo $sql."<br/><br/>";
			$a=$connection->returnResultSet($sql);
			
			for($i=0;$i<sizeof($a);$i++){	
				if($a[$i]["SecCod"]!="" && $a[$i]["SecTitle"]!=""){
					$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($a[$i]["SecCod"]."-".$a[$i]["SecTitle"])."\r\n";
				}
				else if($a[$i]["SecCod"]!="" && $a[$i]["SecTitle"]==""){
					$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($a[$i]["SecCod"])."\r\n";
				}
				else if($a[$i]["SecCod"]=="" && $a[$i]["SecTitle"]!=""){
					$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($a[$i]["SecTitle"])."\r\n";
				}
				$x .= "<table>";
				$x .= "<tr><td>".(($a[$i]["SecDescr"]!="")? $a[$i]["SecDescr"]:""."\r\n")."</td></tr></table>".(($a[$i]["SecDescr"]!="")?"<br/>":""."\r\n");
				
				$sql="SELECT QCatID,SecID,".$indexVar['libappraisal']->getLangSQL("QCatCodChi","QCatCodEng")." as QCatCod,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as QCatDescr,QuesDispMode,".
						"DisplayOrder as QCatDisplayOrder FROM INTRANET_PA_S_QCAT WHERE SecID=".$a[$i]["SecID"]." ORDER BY DisplayOrder;";
				//echo $sql."<br/><br/>";
				$b=$connection->returnResultSet($sql);
				for($j=0;$j<sizeof($b);$j++){
					$x .= "<table><tr><td valign=\"top\">".$b[$j]["QCatCod"]."</td><td>".nl2br($b[$j]["QCatDescr"])."</td></tr></table>";
					//======================================================================== Question Category ========================================================================//
					if($b[$j]["QuesDispMode"]=="1"||$b[$j]["QuesDispMode"]=="2"){
						$x .= "<div style=\"padding-left:20px;\"><table class=\"common_table_list\" width=\"100%\">";
					}
					else{
						$x .= "<div style=\"padding-left:20px;\"><table width=\"100%\">";
					}
					$x .= $indexVar['libappraisal']->getSectionContent("",$a[$i]["SecID"],$b[$j]["QCatID"],$recordID,$batchID,$rlsNo,$appRoleID);
					$x .= "</table></div><br/>\r\n";
				
				}
			}
			//$x .="</div>";
		}
	}
}
$htmlAry['contentTbl'] = $x;
// ============================== Define Button ==============================
if($canEdit=="1"){
	$htmlAry['submitBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnTempSave'], "button", "goSubmit()", 'submitBtn');
}
$htmlAry['backBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');
// ============================== Define Button ==============================
//======================================================================== Form Submission ========================================================================//
if($canEdit=="1" && ($modDateFr==""&& $modDateTo=="")){
	$z .= "<div style=\"border-top: 1px dashed #CCCCCC;\">";
	$z .= $indexVar['libappraisal']->getFormSubmission($Lang,$filler[0]["UserID"],Get_Lang_Selection($filler[0]["ChineseName"],$filler[0]["EnglishName"]),$appRole);
	$z .= "</div>";
}
else if($canEdit=="1" && ($modDateFr!=""&&$modDateTo!="")){
	$z .= "<div style=\"border-top: 1px dashed #CCCCCC;\">";
	$z .= $indexVar['libappraisal']->getModFormSubmission($Lang,$filler[0]["UserID"],Get_Lang_Selection($filler[0]["ChineseName"],$filler[0]["EnglishName"]),$appRole);
	$z .= "</div>";
}

$sql="SELECT GrpID, IdvID, AppRoleID FROM INTRANET_PA_T_FRMSUB WHERE RlsNo=$rlsNo AND RecordID=$recordID AND batchID=$batchID";
$grpData=$connection->returnResultSet($sql);

$z .= "<input type=\"hidden\" id=\"rlsNo\" name=\"rlsNo\" value='".IntegerSafe($rlsNo)."'/>";
$z .= "<input type=\"hidden\" id=\"recordID\" name=\"recordID\" value='".IntegerSafe($recordID)."'/>";
$z .= "<input type=\"hidden\" id=\"batchID\" name=\"batchID\" value='".IntegerSafe($batchID)."'/>";
$z .= "<input type=\"hidden\" id=\"cycleID\" name=\"cycleID\" value='".IntegerSafe($cycleID)."'/>";
$z .= "<input type=\"hidden\" id=\"cycleStart\" name=\"cycleStart\" value='".$cycleStart."'/>";
$z .= "<input type=\"hidden\" id=\"cycleClose\" name=\"cycleClose\" value='".$cycleClose."'/>";
$z .= "<input type=\"hidden\" id=\"modDateFr\" name=\"modDateFr\" value='".$modDateFr."'/>";
$z .= "<input type=\"hidden\" id=\"modDateTo\" name=\"modDateTo\" value='".$modDateTo."'/>";
$z .= "<input type=\"hidden\" id=\"userID\" name=\"userID\" value='".IntegerSafe($userID)."'/>";
$z .= "<input type=\"hidden\" id=\"fillerID\" name=\"fillerID\" value='".IntegerSafe($_SESSION['UserID'])."'/>";
$z .= "<input type=\"hidden\" id=\"templateID\" name=\"templateID\" value='".$templateID."'/>";
$z .= "<input type=\"hidden\" id=\"qID\" name=\"qID\" value='".$qID."'/>";
$z .= "<input type=\"hidden\" id=\"grpID\" name=\"grpID\" value='".$grpData[0]['GrpID']."'/>";
$z .= "<input type=\"hidden\" id=\"idvID\" name=\"idvID\" value='".$grpData[0]['IdvID']."'/>";
$z .= "<input type=\"hidden\" id=\"appRoleID\" name=\"appRoleID\" value='".$grpData[0]['AppRoleID']."'/>";
$z .= "<input type=\"hidden\" id=\"nonEditDivID\" name=\"nonEditDivID\" value='".$nonEditDivID."'/>";
$htmlAry['contentTbl2'] = $z;
//======================================================================== Form Submission ========================================================================//







?>