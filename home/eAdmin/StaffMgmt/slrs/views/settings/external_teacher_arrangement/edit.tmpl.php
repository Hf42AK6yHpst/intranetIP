<form id="form1" name="form1" method="post" action="index.php">
	<div class="table_board">
		<div>
			<?=$htmlAry['contentTbl']?>
		</div>	
		<br style="clear:both;" />		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['backBtn']?>
			<p class="spacer"></p>
		</div>
	</div>
	<input type="hidden" id="supplyID" value="<?=$htmlAry['supplyID']?>" />
</form>

<script type="text/javascript">
$(document).ready( function() {
	
});

function goSubmit(){
	var supplyID = $("<input/>").attr("type", "hidden").attr("name", "supplyID").val($("#supplyID").val());
	$('#form1').append($(supplyID));
	var canSubmit = true;

	canSubmit = checkData() & canSubmit;

	$('.requiredField').each( function() {
		var _elementId = $(this).attr('id');
		if(_elementId.indexOf("supplyTeacher") != -1){
			$('#supplyTeacherEmptyWarnDiv').hide();
			if( $("select[name='supplyTeacher'] option:selected").val() == ""){
				$('#supplyTeacherEmptyWarnDiv').show();	
				canSubmit = false & canSubmit;	
			}
		}
		else if(_elementId.indexOf("teacher") != -1){
			$('#teacherEmptyWarnDiv').hide();
			if( $("select[name='teacher'] option:selected").val() == ""){
				$('#teacherEmptyWarnDiv').show();	
				canSubmit = false & canSubmit;	
			}
		}
	});
	
	if(canSubmit == true){
		var data = "type=checkOverlap&userID="+$("#supplyTeacher option:selected").val()+"&startDate="+ $("#startDate").val()+"&endDate="+ $("#endDate").val()+"&supplyID="+$("#supplyID").val()+"&teacherID="+$("#teacher option:selected").val();	
		$.ajax({ type: "POST", url: '?task=settings<?=$slrsConfig['taskSeparator']?>external_teacher_arrangement<?=$slrsConfig['taskSeparator']?>ajax_checkData', 				
	        data: data,
	        success: function (data, textStatus, jqXHR) {				
	        	var result = eval('(' + data + ')');
	        	if(result[0]["result"] == 1){
	        		$("#dateStartWarningDiv").show();
	        		$("#dateStartWarningDiv span").empty();
	            	$("#dateStartWarningDiv span").text("*"+result[0]["msg"]);
	        	}
	        	else{
	        		$('form#form1').attr('action', 'index.php?task=settings<?=$slrsConfig['taskSeparator']?>external_teacher_arrangement<?=$slrsConfig['taskSeparator']?>edit_update').submit();
	        	}
	        }, error: function (jqXHR, textStatus, errorThrown) {
	           
	        }
	    });
	}	
	//$('form#form1').attr('action', 'index.php?task=settings<?=$slrsConfig['taskSeparator']?>external_teacher_arrangement<?=$slrsConfig['taskSeparator']?>edit_update').submit();
}

function goBack(){
	window.location = '?task=settings<?=$slrsConfig['taskSeparator']?>external_teacher_arrangement<?=$slrsConfig['taskSeparator']?>list';
}
function checkData(){
	var startDate = $("#startDate").val();
	var endDate = $("#endDate").val();
	if(startDate > endDate){
		var errorMessage = '<?= $Lang['SLRS']['ExternalTeacherDes']["DateErrorMessage"]?>';
		$("#dateStartWarningDiv").show();
		$("#dateStartWarningDiv span").empty();
    	$("#dateStartWarningDiv span").text("*"+errorMessage);
		return false;
	}
	else{
		return true;
	}
}

</script>