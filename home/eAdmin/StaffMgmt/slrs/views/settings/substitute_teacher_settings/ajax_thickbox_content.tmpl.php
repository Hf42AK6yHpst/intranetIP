
<form id="form1" name="form1" method="post" action="index.php">
	<div class="table_board">
		<div>
			<?=$htmlAry['contentTbl']?>
		</div>	
		<br style="clear:both;" />		
		<div id="editBottomDiv" class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['cancelBtn']?>
			<p class="spacer"></p>
		</div>
	</div>
</form>


<script type="text/javascript">
$(document).ready( function() {
	initDndTable();
});

function initDndTable() {
	$("table#dndTable").tableDnD({
		onDrop: function(table, row) {
	        var rows = table.tBodies[0].rows;
	        var recordOrder = "";
	        var displayOrder = "";
	        for (var i=0; i<rows.length; i++) {
	        	if (rows[i].id != ""){
	        		recordOrder += rows[i].id+",";
        			displayOrder += i+",";
	        	}
	        }
	        reorderAjax = GetXmlHttpObject();
			  if (reorderAjax == null)
			  {
			    alert (errAjax);
			    return;
			  } 
			    
			  var ElementObj = $(this);
			  var url = 'index.php?task=settings<?=$slrsConfig['taskSeparator']?>substitute_teacher_settings<?=$slrsConfig['taskSeparator']?>ajax_thickbox_content_update';
				var postContent = "recordOrder="+encodeURIComponent(recordOrder)+"&displayOrder="+encodeURIComponent(displayOrder);
				
				reorderAjax.onreadystatechange = function() {
					if (reorderAjax.readyState == 4) {
						//Get_Form_Class_List();
					}
				};
				reorderAjax.open("POST", url, true);
				reorderAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
				reorderAjax.send(postContent);
		},
		onDragStart: function(table, DraggedRow) {
			//$('#debugArea').html("Started dragging row "+row.id);	
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
}

function back(){
	window.location = '?task=settings<?=$slrsConfig['taskSeparator']?>substitute_teacher_settings<?=$slrsConfig['taskSeparator']?>list';
}
</script>