<form name="form1" method="POST" onsubmit="return checkForm();" action='?task=settings<?=$slrsConfig['taskSeparator']?>substitute_teacher_settings<?=$slrsConfig['taskSeparator']?>add_update'>
	<div class="navigation_v30 navigation_2">
		<a href="javascript:goBack()"><?=$Lang['SLRS']['SubstituteTeacherSettings']?></a>
		<span><?=$button_new?> </span>
	</div>
	<p class="spacer"></p>
	<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">

			<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr valign="top">
					<td width="40%"><span class="tabletext"><?=$htmlAry['selectSubstituteTeachers']?>
					:</span></td>
					<td width="10" nowrap="nowrap"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="10"></td>
					<td width="60%" nowrap="nowrap"><span class="tabletext"><?=$htmlAry['selectedSubstituteTeachers']?>
					:</span></td>
				</tr>
				<tr>
					<td valign="top" class="tablerow2">
						<table width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr>
								<td class="tabletext"><?=$htmlAry['selectSubstituteTeachers']?></td>
							</tr>
							<tr>
							<td class="tabletext"><?=$htmlAry['selectBtn']?></td>
								</tr>
							<tr>
								<td class="tabletext"><i><?=$htmlAry['Or']?></i></td>
							</tr>
							<tr>
								<td class="tabletext"><?=$htmlAry['SearchByLoginID']?><br/>
									<div id="statesautocomplete">
										<input type="text" class="tabletext" name="search2" id="search2">
										<div id="statescontainerCC" style="left:142px; top:0px;">
											<div style="display: none; width: 199px; height: 0px;" class="yui-ac-content">
												<div style="display: none;" class="yui-ac-hd"></div>
												<div class="yui-ac-bd"></div>
												<div style="display: none;" class="yui-ac-ft"></div>
											</div>
											<div style="width: 0pt; height: 0pt;" class="yui-ac-shadow"></div>
										</div>
									</div>

								</td>
							</tr>
						</table>
					</td>
					<td valign="top" nowrap="nowrap">&nbsp;</td>
					<td valign="top" nowrap="nowrap">
						<table width="100%" border="0" cellpadding="3" cellspacing="0">
						<tr>
							<td><?=$htmlAry['selectedTeachers']?>
							</td>
						</tr>
						<tr>
							<td align="right"><?=$htmlAry['deleteBtn']?></td>
						</tr>
						</table>
					</td>
				</tr>
			</table>
			<div class="edit_bottom_v30">
				<?=$htmlAry['submitBtn']?>&nbsp;
				<?=$htmlAry['backBtn'] ?>

			</div>
		</tr>
	</table>
<input type="hidden" name="flag" value="0" />

</form>

<style type="text/css">
    #statesmod {position:relative;}
    #statesautocomplete {position:relative;width:22em;margin-bottom:1em;}/* set width of widget here*/
    #statesautocomplete {z-index:9000} /* for IE z-index of absolute divs inside relative divs issue */
    #statesinput {_position:absolute;width:100%;height:1.4em;z-index:0;} /* abs for ie quirks */
    #statescontainer, #statescontainerCC, #statescontainerBCC {position:absolute;top:0.3em;width:100%}
    #statescontainer .yui-ac-content, #statescontainerCC .yui-ac-content, #statescontainerBCC .yui-ac-content {position:absolute;width:100%;border:1px solid #404040;background:#eeeeee;overflow:hidden;z-index:9050;}
    #statescontainer .yui-ac-shadow, #statescontainerCC .yui-ac-shadow, #statescontainerBCC .yui-ac-shadow {position:absolute;margin:.3em;width:100%;background:#a0a0a0;z-index:9049;}
    #statescontainer ul, #statescontainerCC ul, #statescontainerBCC ul {padding:5px 0;width:100%;}
    #statescontainer li, #statescontainerCC li, #statescontainerBCC li {padding:0 5px;cursor:default;white-space:nowrap;}
    #statescontainer li.yui-ac-highlight, #statescontainerCC li.yui-ac-highlight, #statescontainerBCC li.yui-ac-highlight {background:#bbbbbb;}
    #statescontainer li.yui-ac-prehighlight, #statescontainerCC li.yui-ac-prehighlight, #statescontainerBCC li.yui-ac-prehighlight {background:#FFFFFF;}
    
    
	#statesmod div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,form,fieldset,input,textarea,p,blockquote{margin:0;padding:0;}
	#statesmod table{border-collapse:collapse;border-spacing:0;}
	#statesmod fieldset,img{border:0;}
	#statesmod address,caption,cite,code,dfn,em,strong,th,var{font-style:normal;font-weight:normal;}
	#statesmod ol,ul {list-style:none;}
	#statesmod caption,th {text-align:left;}
	#statesmod h1,h2,h3,h4,h5,h6{font-size:100%;font-weight:normal;}
	#statesmod q:before,q:after{content:'';}
	#statesmod abbr,acronym {border:0;}
	#statesmod {font:13px arial,helvetica,clean,sans-serif;*font-size:small;*font:x-small;}
</style>
<!-- In-memory JS array begins-->
<!-- Libary begins -->
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/yahoo.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/dom.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/event-debug.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/animation.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/autocomplete-debug-ip20.js"></script>
<!-- Library ends -->

<script type="text/javascript">
	$(document).ready(function(){
	});

	function goBack(){
		window.location = '?task=settings<?=$slrsConfig['taskSeparator']?>substitute_teacher_settings<?=$slrsConfig['taskSeparator']?>list';
	}
	function goSelect(){
		newWindow('/home/common_choose/index.php?fieldname=teacher[]&page_title=SelectMembers&permitted_type=1&excludeSupplyTeacher=1&excluded_type=4');
	}
	function goDelete(){
		checkOptionRemove(document.getElementById('teacher[]'));
	}
	function checkForm(){
		obj = document.form1;				
        if(obj.elements["teacher[]"].length != 0) {
        	checkOptionAll(obj.elements["teacher[]"]);
        	return true;
		}
        else
        {
                alert('<?=$i_Discipline_System_alert_PleaseSelectMember?>');
                return false;
        }
	}
	function goSubmit(){
		
	}
	var loginidArray = [<?= $htmlAry['liArr2']?>];
</script>
<script type="text/javascript">
var loginidArray = [ <?= $liArr2?>];
	YAHOO.example.ACJSArray = function() {
	    var oACDS, oACDS2, oAutoComp, oAutoComp2;
	    return {
	        init: function() {

	            // Instantiate first JS Array DataSource
	            /*
	            oACDS = new YAHOO.widget.DS_JSArray(statesArray);

	            // Instantiate first AutoComplete            
	            oAutoComp = new YAHOO.widget.AutoComplete('search1','student[]', 'statescontainer', oACDS);
	            oAutoComp.queryDelay = 0;
	            oAutoComp.prehighlightClassName = "yui-ac-prehighlight";
	            oAutoComp.useShadow = true;
	            oAutoComp.minQueryLength = 0;
	            */
	            oACDS2 = new YAHOO.widget.DS_JSArray(loginidArray);
	            oAutoComp2 = new YAHOO.widget.AutoComplete('search2','teacher[]', 'statescontainerCC', oACDS2);
	            oAutoComp2.queryDelay = 0;
	            oAutoComp2.prehighlightClassName = "yui-ac-prehighlight";
	            oAutoComp2.useShadow = true;
	            oAutoComp2.minQueryLength = 0;
	        },

	        validateForm: function() {
	            // Validate form inputs here
	            return false;
	        }
	    };
	}();

	YAHOO.util.Event.addListener(this,'load',YAHOO.example.ACJSArray.init);
</script>

<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/dpSyntaxHighlighter.js"></script>
<script type="text/javascript">
dp.SyntaxHighlighter.HighlightAll('code');
</script>

