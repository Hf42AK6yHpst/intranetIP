<link rel="stylesheet" href="/templates/slrs/css/select2.css?25911a">
<link rel="stylesheet" href="/templates/slrs/css/datepicker.min.css?25911a">
<script language="JavaScript" src="/templates/jquery/jquery-1.11.1.min.js?25911a"></script>
<script language="JavaScript" src="/templates/slrs/js/deprecated.js?25911a"></script>
<script language="JavaScript" src="/templates/slrs/js/select2.full.min.js?25911a"></script>
<script language="JavaScript" src="/templates/slrs/js/datepicker.min.js?25911a"></script>
<?php if ($intranet_session_language == "en") {?>
<script src="/templates/slrs/js/i18n/en.js"></script>
<script src="/templates/slrs/js/i18n/datepicker.en.js"></script>
<?php } else { ?>
<script src="/templates/slrs/js/i18n/zh-TW.js"></script>
<script src="/templates/slrs/js/i18n/datepicker.zh.js"></script>
<?php } ?>
<form id="form1" name="form1" method="post" action="index.php">
	<div class="table_board">
		<div>
			<?php echo $htmlAry['contentTbl']; ?>
		</div>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?php echo $htmlAry['submitBtn']; ?>
			<?php echo $htmlAry['backBtn']; ?>
			<p class="spacer"></p>
		</div>
	</div>
</form>

<script type="text/javascript">

	var loadingTxt = '<?php echo $htmlAry["loadingTxt"]; ?>';
	$(document).ready(function(){
		$("#form1 .loadingbox").eq(0).css({"padding":"10px", "color":"#808080"}).hide().html(loadingTxt);
		slrsJS.cfn.init();
	});

	function setStartDateTime(dateVal) {
		if (dateVal == "") return '';
		else {
			var DateArr = dateVal.split(" ");
			var data = {};
			
			data.year = DateArr[0].split("-")[0]; 
			data.month = DateArr[0].split("-")[1] - 1;
			data.day = DateArr[0].split("-")[2];
			data.hour = 0;
			data.min = 0;
			return new Date(data.year, data.month, data.day, data.hour, data.min, '00')
		}
	}

	$("#session").change(function() {
		$("#sessionWarningDiv span").empty();
		$("#sessionWarningDiv").hide();
		var data = "type=checkSession&userID="+$("#leaveTeacher option:selected").val()+"&date="+ $("#datePicker").val()+"&session="+$("#session option:selected").val();		
		$.ajax({ type: "POST", url: '?task=management<?=$slrsConfig['taskSeparator']?>substitute_arrangement<?=$slrsConfig['taskSeparator']?>ajax_checkData', 
	        data: data,
            success: function (data, textStatus, jqXHR) {				
            	var result = eval('(' + data + ')');
            	if(result[0]["result"] == 1){
            		$("#sessionWarningDiv").show();
            		$("#sessionWarningDiv span").empty();
                	//$(".tabletextrequire").text("*"+result[0]["msg"]);
                	$("#sessionWarningDiv span").text("*"+result[0]["msg"]);
            	}
            }, error: function (jqXHR, textStatus, errorThrown) {
               
            }
        });
	});

	function goSubmit() {
		var canSubmit = true;
		var isFocused = false;
		var errorMsg = "";
		$('#substitutionEmptyWarnDiv').hide();
		
		if( $("select[name='leaveTeacher'] option:selected").val() == undefined || $("select[name='leaveTeacher'] option:selected").val() == ''){
			errorMsg += "* <?php echo $Lang['SLRS']['SubstitutionArrangementDes']['TeacherMandatory']; ?><br>";
			canSubmit = false;	
		}

		if( $("select[name='reason'] option:selected").val() == undefined || $("select[name='reason'] option:selected").val() == ''){
			errorMsg += "* <?php echo $Lang['SLRS']['SubstitutionArrangementDes']['ReasonMandatory']; ?><br>";
			canSubmit = false;	
		}

		if( $("select[name='session'] option:selected").val() == undefined || $("select[name='session'] option:selected").val() == ''){
			errorMsg += "* <?php echo $Lang['SLRS']['SubstitutionArrangementDes']['SessionMandatory']; ?><br>";
			canSubmit = false;	
		}
		
		if( $("input[name='substitution']:checked").val() == undefined){
			errorMsg += "* <?php echo $Lang['SLRS']['SubstitutionArrangementDes']['ArrangementMandatory']; ?><br>";
			canSubmit = false;	
		}

		if (!canSubmit) {
			$("#substitutionEmptyWarnDiv span").html(errorMsg);
			$('#substitutionEmptyWarnDiv').show();	
		}
		
// 		$('.requiredField').each( function() {
// 			var _elementId = $(this).attr('id');
// 			console.log(_elementId.indexOf("Substitution"));
// 			if(_elementId.indexOf("Substitution") != -1){
// 				$('#substitutionEmptyWarnDiv').hide();	
// 				if( $("input[name='substitution']:checked").val() == undefined){
// 					$('#substitutionEmptyWarnDiv').show();	
// 					canSubmit = false;	
// 				}
// 			}
// 		});

		if (canSubmit) {
			$("#sessionWarningDiv").hide();
			var data = "type=checkSession&userID="+$("#leaveTeacher option:selected").val()+"&date="+ $("#datePicker").val()+"&session="+$("#session option:selected").val();		
			$.ajax({ type: "POST", url: '?task=management<?=$slrsConfig['taskSeparator']?>substitute_arrangement<?=$slrsConfig['taskSeparator']?>ajax_checkData', 
		        data: data,
	            success: function (data, textStatus, jqXHR) {				
	            	var result = eval('(' + data + ')');
	            	if(result[0]["result"] == 1){
	            		$("#sessionWarningDiv").show();
	            		$("#sessionWarningDiv span").empty();
	                	//$(".tabletextrequire").text("*"+result[0]["msg"]);
	                	$("#sessionWarningDiv span").text("*"+result[0]["msg"]);
	            	}
	            	else{
	            		$('form#form1').attr('action', 'index.php?task=management<?=$slrsConfig['taskSeparator']?>substitute_arrangement<?=$slrsConfig['taskSeparator']?>edit_update').submit();
	            	}
	            }, error: function (jqXHR, textStatus, errorThrown) {
	               
	            }
	        });
		}
		else {
			// enable the action buttons if not allowed to submit
			$('input.actionBtn').attr('disabled', '');
		}
	}

	function changeDatePicker(id){			
		var data = "type=teacher"+"&date="+ $("#"+id).val();
		$("#form1 .loadingbox").eq(0).show();
		$.ajax({ type: "POST", url: '?task=management<?=$slrsConfig['taskSeparator']?>substitute_arrangement<?=$slrsConfig['taskSeparator']?>ajax_getData', 
	        data: data,
            success: function (data, textStatus, jqXHR) {
               	var result = eval('(' + data + ')');
               	// get room information
               	var firstOptionValue = $("#leaveTeacher" + " option:first").val();
               	var firstOptionText = $("#leaveTeacher" + " option:first").text();
				$("#leaveTeacher").empty();
				$("#leaveTeacher").append($('<option></option>').val(firstOptionValue).html(firstOptionText));
                for (i=0; i<result.length; i++){
                	$("#leaveTeacher").append($('<option></option>').val(result[i].UserID).html(result[i].Name));
                }
			 	$("#leaveTeacher").select2({
			 		language: "<?php echo (($intranet_session_language == "en") ? "en" : "zh-TW"); ?>",
				 	placeholder: '<?php echo $Lang['SLRS']['NotAvailableTeachersForm']["please-select"]; ?>'
				});

                setTimeout('$("#form1 .loadingbox").eq(0).hide()', 1000);
            }, error: function (jqXHR, textStatus, errorThrown) {
            	setTimeout('$("#form1 .loadingbox").eq(0).hide()', 1000);
            }
        });
	}
	function goBack(){
		window.location = '?task=management<?=$slrsConfig['taskSeparator']?>substitute_arrangement<?=$slrsConfig['taskSeparator']?>list';
	}


	var slrsJS = {
			vrs: {
				ready: false
			},
			ltr: {
			},
			cfn: {
				init: function() {
					if ($(".js-basic-single").length > 0) {
					 	$(".js-basic-single").select2({
					 		language: "<?php echo (($intranet_session_language == "en") ? "en" : "zh-TW"); ?>",
						 	placeholder: '<?php echo $Lang['SLRS']['NotAvailableTeachersForm']["please-select"]; ?>'
						});
					}
					var options = {
							language: "<?php echo (($intranet_session_language == "en") ? "en" : "zh"); ?>",
						    dateFormat: 'yyyy-mm-dd',
					        autoClose: true,
					        immediateUpdates: true,
					        showOnFocus:true,
						    toggleSelected: false,
						    range: false,
						    todayButton: true,
						    timepicker: false,
						    minDate: new Date(<?php echo $dateLimitation['min']["Y"]; ?>, <?php echo $dateLimitation['min']["M"]; ?>, <?php echo $dateLimitation['min']["D"]; ?>),
						    maxDate: new Date(<?php echo $dateLimitation['max']["Y"]; ?>, <?php echo $dateLimitation['max']["M"]; ?>, <?php echo $dateLimitation['max']["D"]; ?>),
						    onSelect: function(data, date, inst) {
						    	changeDatePicker('datePicker');
						    }
						    
						};
					$('#datePicker').datepicker(options);
					$('#datePicker').bind("change", function (e) {
						changeDatePicker('datePicker');
					});;
					slrsJS.vrs.ready = true;
				}
			}	
		};
		
</script>

