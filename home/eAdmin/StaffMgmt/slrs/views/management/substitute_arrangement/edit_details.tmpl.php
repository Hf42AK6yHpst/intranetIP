<link rel="stylesheet" href="/templates/slrs/css/select2.css?v=20180921">
<link rel="stylesheet" href="/templates/slrs/css/font-awesome.min.css?v=20171211">
<script language="JavaScript" src="/templates/jquery/jquery-1.11.1.min.js"></script>
<script language="JavaScript" src="/templates/slrs/js/deprecated.js"></script>
<script language="JavaScript" src="/templates/slrs/js/select2.full.min.js"></script>
<?php if ($intranet_session_language == "en") {?>
<script src="/templates/slrs/js/i18n/en.js"></script>
<?php } else { ?>
<script src="/templates/slrs/js/i18n/zh-TW.js"></script>
<?php } ?>
<form id="form1" name="form1" method="post" action="index.php">
	<div class="table_board">
		<div>
			<?=$htmlAry['contentTbl']?>
		</div>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['backBtn']?>
			<p class="spacer"></p>
		</div>	
	</div>
	
	<input type="hidden" id="startIndex" value="<?=$htmlAry['startIndex']?>">
	<input type="hidden" id="endIndex" value="<?=$htmlAry['endIndex']?>">
	<input type="hidden" id="leaveID" value="<?=$htmlAry['leaveID']?>">
	<input type="hidden" id="day" value="<?=$htmlAry['day']?>">
</form>

<script type="text/javascript">
	var jsSelectOBJ = [];
	var presetLocationJS = {
		vrs: {
			limitCount: <?php echo (isset($PresetLocationInfo["ThisCount"])) ? $PresetLocationInfo["ThisCount"] : 0; ?>,
			currentUsed: 0,
			LimitLocation: '<?php echo ($PresetLocationInfo["LimitLocation"]) ? 'true' : 'false'; ?>',
			presetID: <?php echo ($PresetLocationInfo["LocationID"] > 0) ? $PresetLocationInfo["LocationID"] : 0; ?>,
			type: '<?php echo $PresetLocationInfo["type"]; ?>',
			defVal: []
		},
		ltr: {
			locationChangeHandle: function(e) {
				var prevVal = presetLocationJS.vrs.defVal[$(this).attr('id')];
				if (typeof prevVal != "undefined") {
					presetLocationJS.vrs.defVal[$(this).attr('id')] = $(this).val();
					if (prevVal == presetLocationJS.vrs.presetID && $(this).val() != presetLocationJS.vrs.presetID) {
						presetLocationJS.vrs.currentUsed--;
					} else if (prevVal != presetLocationJS.vrs.presetID && $(this).val() == presetLocationJS.vrs.presetID) {
						presetLocationJS.vrs.currentUsed++;
						var name = $(this).attr('name');
						var ignoreAction = "#ignoreThisRec_" + name.replace("locationID_", "");
						if ($(ignoreAction).size() > 0) {
							if ($(ignoreAction).is(":checked") == false) {
								$(ignoreAction).trigger('click');
							}
						}
					}
					presetLocationJS.cfn.initSelection();
					try {
						$(".js-select2").select2({
								allowClear: true,
	 							language: "<?php echo (($intranet_session_language == "en") ? "en" : "zh-TW"); ?>",
	 							placeholder: '<?php echo $Lang['SLRS']['NotAvailableTeachersForm']["please-select"]; ?>'
						});
					} catch(err) {
						// console.clear();
					}
				} 
		
			}
		},
		cfn: {
			initSelection: function() {
				$(".js-select2").each(function(index) {
					$(this).find("option[value='" + presetLocationJS.vrs.presetID + "']").eq(0).removeAttr('disabled');
				});
				if (presetLocationJS.vrs.LimitLocation) {
					$(".js-select2").bind('change', presetLocationJS.ltr.locationChangeHandle);
					if (presetLocationJS.vrs.currentUsed >= presetLocationJS.vrs.limitCount) {
						$(".js-select2").each(function(index) {
							if ($(this).val() != presetLocationJS.vrs.presetID) {
								$(this).find("option[value='" + presetLocationJS.vrs.presetID + "']").eq(0).attr('disabled', 'disabled');
							}
						});
					}
				}
			},
			init: function() {
				if ($(".js-select2").size() > 0) {
					$(".js-select2").each(function(index) {
						presetLocationJS.vrs.defVal[$(this).attr('id')] = $(this).val();
						if ($(this).val() == presetLocationJS.vrs.presetID) {
							presetLocationJS.vrs.currentUsed++;
							if (presetLocationJS.vrs.type == "edit") {
								presetLocationJS.vrs.limitCount++;
							}
						}
					});
					presetLocationJS.cfn.initSelection();
				}
			}
		}
	};

	$(document).ready(function(){
		var leaveID = getURLParameter("leaveID");
		if(leaveID != "null"){		
			$("#datePicker").attr("disabled", true);
			$(".datepick-trigger").remove();
			$("#leaveTeacher,#reason,#session").attr("disabled", true);			
			$(':radio').each(function(){				 
				 var name = this.name;
				 var ignoreSub = "ignoreThisRec" + name.replace("teacher", "");
				 /*
				 if($('input[name="'+name+'"]:checked').val() == undefined) {
					 $('input[name='+name+'][disabled=false]:first').attr("checked", true);
					 $('input[name='+name+']:first').trigger("click");
				 }
				 */
				 if($('input[name="'+name+'"]:checked').val() == undefined && $('input[name="'+ignoreSub+'"]:checked').val() == undefined){
					 $('input[name='+name+'][disabled=false]:first').attr("checked", true);
					 $('input[name='+name+']:first').trigger("click");
				 }
			});
		}
		$(".ignoreAction").bind('click', ignoreAction);

<?php if ($PresetLocationInfo["LimitLocation"]) { ?>
		presetLocationJS.cfn.init();
<?php } ?>
		if ($(".js-select2").length > 0) {
			$(".js-select2").css({'width':'100%'});
			$(".js-select2").select2({
				allowClear: true,
				language: "<?php echo (($intranet_session_language == "en") ? "en" : "zh-TW"); ?>",
				placeholder: '<?php echo $Lang['SLRS']['NotAvailableTeachersForm']["please-select"]; ?>'
			});
		 	$(".js-select2").css({'min-width':'320px'});
		}
		viewTeacherInfo.cfn.init();
	});
	$("[id^='locationText']").click(function(e){
		var id = e.target.id;
		var index = id.split("_").pop();
		var text = $("#locationText_"+index).text();
		if(text == "<?= $Lang['SLRS']['SubstitutionArrangementDes']['ToBeChanged'] ?>"){		
			$("#locationText_"+index).text("<?= $Lang['SLRS']['SubstitutionArrangementDes']['Cancel'] ?>");
			var tgtID = "#locationDiv_"+index;
			$("#locationDiv_"+index).show();
		}
		else{
			$("#locationText_"+index).text("<?= $Lang['SLRS']['SubstitutionArrangementDes']['ToBeChanged'] ?>");
			var hideLocationID = $("#oriLocationID_"+index).val();
			var tgtID = "#locationDiv_"+index;
			$("#locationDiv_"+index+" option[value="+hideLocationID+"]").attr("selected","selected");
			$("#locationDiv_"+index).hide();
		}
	});

	$("[id^='viewTimeTable']").click(function(e){
		var id = e.target.id;
		var index = id.split("_").pop();
	});

	function goSubmit() {
		$("#datePicker").attr("disabled", false);
		$("#leaveTeacher,#reason,#session").attr("disabled", false);
		
		var startIndexVal = parseInt($("#startIndex").val());
		var endIndexVal = parseInt($("#endIndex").val());		
		var leaveIDVal = parseInt($("#leaveID").val());
		var startIndex = $("<input/>").attr("type", "hidden").attr("name", "startIndex").val(startIndexVal);
		var endIndex = $("<input/>").attr("type", "hidden").attr("name", "endIndex").val(endIndexVal);
		var leaveID = $("<input/>").attr("type", "hidden").attr("name", "leaveID").val(leaveIDVal);
		$('#form1').append($(startIndex)).append($(endIndex)).append($(leaveID));

		$('form#form1').attr('action', 'index.php?task=management<?=$slrsConfig['taskSeparator']?>substitute_arrangement<?=$slrsConfig['taskSeparator']?>edit_details_update');
		$('form#form1').bind('submit', checkTeacherJS.ltr.checkTeacherHandler);
		
		// $('form#form1').attr('action', 'index.php?task=management<?=$slrsConfig['taskSeparator']?>substitute_arrangement<?=$slrsConfig['taskSeparator']?>edit_details_update').submit();
	}
	function goView(teacherID){
		var date = $("#datePicker").val();
		var day = $("#day").val();
		var leaveID = $("#leaveID").val();
		load_dyn_size_thickbox_ip('<?=$Lang['SLRS']['SubstitutionArrangementDes']['Timetable']?>', 'onloadThickBox("'+teacherID+'","'+date+'","'+day+'","'+leaveID+'");', inlineID='', defaultHeight=550, defaultWidth=550);
		
	}
	function onloadThickBox(id,date,day,leaveID) {
		$('div#TB_ajaxContent').load(
			'?task=management<?=$slrsConfig['taskSeparator']?>substitute_arrangement<?=$slrsConfig['taskSeparator']?>ajax_thickbox_content', 
		{ 
			userID: id,
			date: date,
			day: day,
			leaveID: leaveID,
			task: 'management<?=$slrsConfig['taskSeparator']?>substitute_arrangement<?=$slrsConfig['taskSeparator']?>ajax_thickbox_content'
			},
			function(ReturnData) {
				$("#cancelBtn_tb").bind('click', viewTeacherInfo.ltr.closeTbBoxHandler);
				// adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
			}
		);
	}
	$("[id^='more_']").click(function(e){
		e.preventDefault();
		var id = e.target.id;
		var index = id.split("_").pop();
		// var rowspan = $("#rowspan_"+index).val();
		var rowspan = $(".row_n_"+index).size() + 1;
		$("#tdTime_"+index).attr("rowspan",rowspan);
		$("#tdDate_"+index).attr("rowspan",rowspan);
		$("#tdLocation_"+index).attr("rowspan",rowspan);
		$("[id^='row_more_"+index+"']").remove();
		$("[id^='row_" + index +"']").show();
	});
	$("[id^='details_']").click(function(e){
		var id = e.target.id;
		// console.log(id);
		hlRow(id);

	});

	function hlRow(id){
		var index = "";
		var indexArr = "";
		if(id.indexOf("teacher") != -1) {
			index = id.substring((id.indexOf('teacher')+"teacher".length), id.length);
			indexArr = id.split('_');
		}
		else if(id.indexOf("details") != -1) {
			index = id.substring((id.indexOf('details')+"details".length), id.length);
			indexArr = id.split('_');
		}
		if(index != ""){
			$("[id^='details_"+indexArr[1]+"']").removeClass("row_suggest");
			$("[id='details" + index + "']").addClass("row_suggest");
			$("#teacher"+index).attr('checked', true);
			var ignoreIndex = parseInt(indexArr[1]);
			$("#ignoreThisRec_"+ignoreIndex).attr('checked', false);
		}
	}

	function ignoreAction(e) {
		var name = $(this).attr('name');
		var teacher_name = "teacher_" + name.substring((name.indexOf('ignoreThisRec_')+"ignoreThisRec_".length), name.length);
		if ($(this).is(':checked')) {
			if ($("input[name='" + teacher_name + "']:checked").length > 0) {
				var parentDetail = $("input[name='" + teacher_name + "']:checked").parent().parent();
				if (parentDetail.find('.row_right_none').size() > 0) {
					parentDetail.find('.row_right_none').each(function() {
						$(this).removeClass('row_suggest');
					});
				}
				$("input[name='" + teacher_name + "']:checked").attr("checked", false);
			}
		} else {
			if ($("input[name='" + teacher_name + "']:checked").length > 0) {
			} else {
				$('input[name='+teacher_name+'][disabled=false]:first').attr("checked", true);
				$('input[name='+teacher_name+']:first').trigger("click");
//				e.preventDefault();
			}
		}
	}

	function goBack(){
		window.location = '?task=management<?=$slrsConfig['taskSeparator']?>substitute_arrangement<?=$slrsConfig['taskSeparator']?>list';
	}

	function getURLParameter(name) {
	    return decodeURI((RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, null])[1]);
	}

	var viewTeacherInfo = {
		vrs: {
			elm: 'a.viewinfo',
			activeElm: ''
		},
		ltr: {
			clickHandler: function(e) {
				e.preventDefault();
				var view_id = $(this).attr('rel');
				viewTeacherInfo.vrs.activeElm = $(this).attr('id');
				goView(view_id);
				$("#TB_closeWindowButton").bind('click', viewTeacherInfo.ltr.closeTbBoxHandler);
			},
			closeTbBoxHandler: function(e) {
				e.preventDefault();
				$('html, body').animate({
			        scrollTop: $("#" + viewTeacherInfo.vrs.activeElm).offset().top
			    }, 0);
				viewTeacherInfo.vrs.activeElm = '';
			}
		},
		cfn: {
			init: function() {
				viewTeacherInfo.cfn.binding(); 
			},
			binding: function() {
				$(viewTeacherInfo.vrs.elm).unbind('click', viewTeacherInfo.ltr.clickHandler);
				$(viewTeacherInfo.vrs.elm).bind('click', viewTeacherInfo.ltr.clickHandler);
			}
		}
	};
	

	var checkTeacherJS = {
		vrs: {
			xhr: ''
		},
		ltr: {
			checkTeacherHandler: function(e)
			{
				e.preventDefault();
				$('form#form1').unbind('submit', checkTeacherJS.ltr.checkTeacherHandler);
				var data = $('form#form1').serialize() + "&type=checkSettings";
				var checkURL = 'index.php?task=management<?=$slrsConfig['taskSeparator']?>substitute_arrangement<?=$slrsConfig['taskSeparator']?>ajax_checkData';
				var xhr = checkTeacherJS.vrs.xhr;
				if (xhr != "")
				{
					xhr.abort();
				}
				if ($('#SA_error').size() > 0) 
				{
					$('#SA_error').remove();
				}
				$("#datePicker").attr("disabled", true);
				$("#leaveTeacher,#reason,#session").attr("disabled", true);
				checkTeacherJS.vrs.xhr = $.ajax({
					url: checkURL,
					type:'post',
					data: data,
					dataType: "json",
					success: function(resp) {
						if (typeof resp.result != "undefined" && resp.result == "success")
						{
							$("#datePicker").attr("disabled", false);
							$("#leaveTeacher,#reason,#session").attr("disabled", false);
							$('form#form1').submit();
						}
						else
						{
							var errObj = resp.errmsg;
							if (Object.keys(errObj).length > 0)
							{
								var errorHTML = "<div id='SA_error' style='padding:10px; color:#f00;'>";
								$.each(errObj, function(index, data) {
									errorHTML += "<i class='fa fa-exclamation-triangle' aria-hidden='true'></i> " + data.name;
									var dataObj = data.msg;
									if (Object.keys(dataObj).length > 0)
									{
										$.each(dataObj, function(mInd, mVal) {
											errorHTML += "<li>" + mVal + "</li>";
										});
									}
								});
								errorHTML += "</div>";
								$('div.table_board > div').eq(0).append(errorHTML);
							}
						}
					}
				});
				
			}
		}
	}
</script>
