<link rel="stylesheet" href="/templates/slrs/css/select2.css?v=20171211">
<link rel="stylesheet" href="/templates/slrs/css/font-awesome.min.css?v=20171211">
<form name="form1" id="form1" method="POST" action="?task=management<?php echo $slrsConfig['taskSeparator'];?>cancel_lesson<?php echo $slrsConfig['taskSeparator'];?>list">
	<div class="table_board">
	
		<div class="content_top_tool">
			<?=$htmlAry['contentTool']?>
			<?=$htmlAry['searchBox']?>
			<br style="clear:both;">
		</div>
		
		<p class="spacer"></p>
		<?=$htmlAry['dataPickerTool']?>
		<?=$htmlAry['viewBtn']?>
		<?=$htmlAry['dbTableActionBtn']?>
		<br style="clear:both;">		
		
		<?=$htmlAry['dataTable']?>
		<p class="spacer"></p>
		
		<?=$htmlAry['hiddenField']?>	
	</div>
</form>

<script type="text/javascript">
$(document).ready( function() {	
	$('input#keyword').keydown( function(evt) {
		if (Check_Pressed_Enter(evt)) {
			// pressed enter
			goSearch();
		}
	});	
});
function goSearch() {
	$('form#form1').submit();
}

function goDelete(){
	checkRemove(document.form1,'cancelIdAry[]','?task=management<?=$slrsConfig['taskSeparator']?>cancel_lesson<?=$slrsConfig['taskSeparator']?>delete');
}

function goAddCancel(){
	window.location = '?task=management<?=$slrsConfig['taskSeparator']?>cancel_lesson<?=$slrsConfig['taskSeparator']?>edit';
}

</script>