<link rel="stylesheet" href="/templates/slrs/css/select2.css?201809">
<link rel="stylesheet" href="/templates/slrs/css/font-awesome.min.css?v=20180125">
<link rel="stylesheet" href="/templates/slrs/css/datepicker.min.css?25911a">
<script language="JavaScript" src="/templates/jquery/jquery-1.11.1.min.js?25911a"></script>
<script language="JavaScript" src="/templates/slrs/js/deprecated.js?25911a"></script>
<script language="JavaScript" src="/templates/slrs/js/select2.full.min.js?25911a"></script>
<script language="JavaScript" src="/templates/slrs/js/datepicker.min.js?25911a"></script>
<?php if ($intranet_session_language == "en") {?>
<script src="/templates/slrs/js/i18n/en.js"></script>
<script src="/templates/slrs/js/i18n/datepicker.en.js"></script>
<?php } else { ?>
<script src="/templates/slrs/js/i18n/zh-TW.js"></script>
<script src="/templates/slrs/js/i18n/datepicker.zh.js"></script>
<?php } ?>
<form id="CancelLessonForm" name="CancelLessonForm" method="post" action="?task=management<?php echo $slrsConfig['taskSeparator']?>cancel_lesson<?php echo $slrsConfig['taskSeparator']?>edit_update">
	<div class="table_board">
		<div>
			<?php echo $htmlAry['contentTbl']; ?>
		</div>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?php echo $htmlAry['submitBtn']; ?>
			<?php echo $htmlAry['backBtn']; ?>
			<p class="spacer"></p>
		</div>
	</div>
</form>

<script type="text/javascript">

	var loadingTxt = '<?php echo $htmlAry["loadingTxt"]; ?>';
	$(document).ready(function(){
		$("#CancelLessonForm .loadingbox").eq(0).css({"padding":"10px", "color":"#808080"}).hide().html(loadingTxt);
		slrsJS.cfn.init();
	});

	function setStartDateTime(dateVal) {
		if (dateVal == "") return '';
		else {
			var DateArr = dateVal.split(" ");
			var data = {};
			
			data.year = DateArr[0].split("-")[0]; 
			data.month = DateArr[0].split("-")[1] - 1;
			data.day = DateArr[0].split("-")[2];
			data.hour = 0;
			data.min = 0;
			return new Date(data.year, data.month, data.day, data.hour, data.min, '00')
		}
	}

	function goSubmit(e) {
		$('#CancelLessonForm').submit();
	}

	function changeDatePicker(id){			
		var data = "type=teacher"+"&date="+ $("#"+id).val();
		$("#CancelLessonForm .loadingbox").eq(0).show();
		$('#cancelLessonBody').html('');
		$("#cancelTeacher").empty();
		$.ajax({ type: "POST", url: "?task=management<?php echo $slrsConfig['taskSeparator']?>cancel_lesson<?php echo $slrsConfig['taskSeparator']?>ajax_getTeacher", 
	        data: data,
            success: function (data, textStatus, jqXHR) {
               	var result = eval('(' + data + ')');
               	// get room information
               	var firstOptionValue = $("#cancelTeacher" + " option:first").val();
               	var firstOptionText = $("#cancelTeacher" + " option:first").text();
				$("#cancelTeacher").append($('<option></option>').val(firstOptionValue).html(firstOptionText));
                for (i=0; i<result.length; i++){
                	$("#cancelTeacher").append($('<option></option>').val(result[i].UserID).html(result[i].Name));
                }
			 	$("#cancelTeacher").select2({
			 		language: "<?php echo (($intranet_session_language == "en") ? "en" : "zh-TW"); ?>",
				 	placeholder: '<?php echo $Lang['SLRS']['NotAvailableTeachersForm']["please-select"]; ?>'
				});

                setTimeout('$("#CancelLessonForm .loadingbox").eq(0).hide()', 1000);
            }, error: function (jqXHR, textStatus, errorThrown) {
            	setTimeout('$("#CancelLessonForm .loadingbox").eq(0).hide()', 1000);
            }
        });

	}
	function goBack(){
		window.location = '?task=management<?php echo $slrsConfig['taskSeparator']?>cancel_lesson<?php echo $slrsConfig['taskSeparator']?>list';
	}

	var slrsJS = {
			vrs: {
				ready: false,
				xhr: ''
			},
			ltr: {
				textareaKeyUpHandle: function(e)
				{
					if ($(this).val() != '')
					{
						var chkElm = $(this).closest( "tr" ).find('input.cancelLessonItem').eq(0);
						chkElm.prop("checked", true);
					}
				},
				itemHandle: function(e){
					var chkElm = $(this).closest( "tr" ).find('textarea.cancelLessonRemark').eq(0);
					if(!$(this).prop("checked")) {
						chkElm.prop('disabled', true);
					} else {
						chkElm.prop('disabled', false);
					}
				},
				dataHandler: function(response_data) {
					$('#cancelLessonBody').html(response_data);
					$("#CancelLessonForm .loadingbox").eq(0).hide();
					$('input.select_all').unbind('change', slrsJS.ltr.selectAllHandle);
					$('textarea.cancelLessonRemark').unbind('keyup', slrsJS.ltr.textareaKeyUpHandle);
					$("input.cancelLessonItem").unbind('change', slrsJS.ltr.itemHandle);
					if ($('input.cancelLessonItem').length > 0)
					{
						$('input#submitBtn').show();
						$('input.select_all').bind('change', slrsJS.ltr.selectAllHandle);
						$('textarea.cancelLessonRemark').bind('keyup', slrsJS.ltr.textareaKeyUpHandle);
						$("input.cancelLessonItem").bind('change', slrsJS.ltr.itemHandle);
					}
				},
				errorHandler: function(resp, status, xhr) {
					setTimeout('$("#CancelLessonForm .loadingbox").eq(0).hide()', 1000);
					var strHTML = "<table><tr><td style='border-bottom:0px;'><?php echo $Lang['SLRS']['CancelLessonDes']['msg']["noTimeslotRecord"]; ?></td></tr></table>";
					$('#cancelLessonBody').html(strHTML);
					$("#CancelLessonForm .loadingbox").eq(0).hide();
				},
				teacherChangeHandler: function(e) {
					$("#CancelLessonForm .loadingbox").eq(0).show();
					$('input#submitBtn').hide();
					var selectedDate = $('#datePicker').val();
					var selectedTeacher = $('#cancelTeacher').val();
					
					if (slrsJS.vrs.xhr != "")
					{
						var xhr = slrsJS.vrs.xhr;
						xhr.abort();
					}
					var url = '?task=management<?=$slrsConfig['taskSeparator']?>cancel_lesson<?=$slrsConfig['taskSeparator']?>ajax_getLesson';
					var param = { selecteddate: selectedDate, teacher: selectedTeacher }
					$('#cancelLessonBody').html('');
					slrsJS.vrs.xhr = $.ajax({
						 					url: url,
						 					data: param,
						 					type: "POST",
						 					success: slrsJS.ltr.dataHandler,
						 					error: slrsJS.ltr.errorHandler
					});
				},
				selectAllHandle: function(e) {
					if($(this).prop("checked")) {
						$("input.cancelLessonItem").each(function() {
							$(this).prop("checked", true);
							var chkElm = $(this).closest( "tr" ).find('textarea.cancelLessonRemark').eq(0);
							chkElm.prop('disabled', false);
						});
					} else {
						$("input.cancelLessonItem").each(function() {
							$(this).prop("checked", false);
							var chkElm = $(this).closest( "tr" ).find('textarea.cancelLessonRemark').eq(0);
							chkElm.prop('disabled', true);
						});
					}
				}
			},
			cfn: {
				init: function() {
					if ($(".js-basic-single").length > 0) {
					 	$(".js-basic-single").select2({
					 		language: "<?php echo (($intranet_session_language == "en") ? "en" : "zh-TW"); ?>",
						 	placeholder: '<?php echo $Lang['SLRS']['NotAvailableTeachersForm']["please-select"]; ?>'
						});
					}
					var options = {
							language: "<?php echo (($intranet_session_language == "en") ? "en" : "zh"); ?>",
						    dateFormat: 'yyyy-mm-dd',
					        autoClose: true,
					        immediateUpdates: true,
					        showOnFocus:true,
						    toggleSelected: false,
						    range: false,
						    todayButton: true,
						    timepicker: false,
						    minDate: new Date(<?php echo $dateLimitation['min']["Y"]; ?>, <?php echo $dateLimitation['min']["M"]; ?>, <?php echo $dateLimitation['min']["D"]; ?>),
						    maxDate: new Date(<?php echo $dateLimitation['max']["Y"]; ?>, <?php echo $dateLimitation['max']["M"]; ?>, <?php echo $dateLimitation['max']["D"]; ?>),
						    onSelect: function(data, date, inst) {
						    	changeDatePicker('datePicker');
						    }
						    
						};
					$('#datePicker').datepicker(options);
					$('#datePicker').bind("change", function (e) {
						changeDatePicker('datePicker');
					});;

					$('#cancelTeacher').bind('change', slrsJS.ltr.teacherChangeHandler);
					$('input#submitBtn').hide();
					slrsJS.vrs.ready = true;
				}
			}	
		};
		
</script>

