<link rel="stylesheet" href="/templates/slrs/css/select2.css?v=20171211">
<form name="form1" id="form1" method="POST" action="?task=management<?=$slrsConfig['taskSeparator']?>temporary_substitute_arrangement<?=$slrsConfig['taskSeparator']?>list">
	<div class="table_board">
	
		<div class="content_top_tool">
			<?=$htmlAry['contentTool']?>
			<?=$htmlAry['searchBox']?>
			<br style="clear:both;">
		</div>
		
		<p class="spacer"></p>
		<?=$htmlAry['dataPickerTool']?>
		<?=$htmlAry['viewBtn']?>
		<?=$htmlAry['dbTableActionBtn']?>
		<br style="clear:both;">
		
		<?=$htmlAry['dataTable']?>
		<p class="spacer"></p>
		
		<?=$htmlAry['hiddenField']?>	
	</div>
</form>

<script type="text/javascript">
$(document).ready( function() {	
	$('input#keyword').keydown( function(evt) {
		if (Check_Pressed_Enter(evt)) {
			// pressed enter
			goSearch();
		}
	});	
});

function goSearch() {
	$('form#form1').submit();
}

function goArrange(arrangementID){
	window.location = '?task=management<?=$slrsConfig['taskSeparator']?>temporary_substitute_arrangement<?=$slrsConfig['taskSeparator']?>edit&arrangementID='+arrangementID;
}

function goAddLeave(){
	var selected_date = $('#datePicker').val();
	window.location = '?task=management<?=$slrsConfig['taskSeparator']?>temporary_substitute_arrangement<?=$slrsConfig['taskSeparator']?>edit&selected_date=' + selected_date;
}
function goView(){
	goSearch();
}
function goDelete(){
	//window.location = '?task=management<?=$slrsConfig['taskSeparator']?>substitute_balance<?=$slrsConfig['taskSeparator']?>edit';
	checkRemove(document.form1,'lessonArrangementIdAry[]','?task=management<?=$slrsConfig['taskSeparator']?>temporary_substitute_arrangement<?=$slrsConfig['taskSeparator']?>delete');
}
function changeDatePicker(id){
	
}
</script>