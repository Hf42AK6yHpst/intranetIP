<link rel="stylesheet" href="/templates/slrs/css/select2.css">
<link rel="stylesheet" href="/templates/slrs/css/datepicker.min.css">
<script language="JavaScript" src="/templates/jquery/jquery-1.11.1.min.js"></script>
<script language="JavaScript" src="/templates/slrs/js/select2.full.min.js"></script>
<script language="JavaScript" src="/templates/slrs/js/datepicker.min.js"></script>
<?php if ($intranet_session_language == "en") {?>
<script src="/templates/slrs/js/i18n/en.js"></script>
<script src="/templates/slrs/js/i18n/datepicker.en.js"></script>
<?php } else { ?>
<script src="/templates/slrs/js/i18n/zh-TW.js"></script>
<script src="/templates/slrs/js/i18n/datepicker.zh.js"></script>
<?php } ?>

<form id="form1" name="form1" method="post" action="index.php">
	<div class="table_board">
		<div>
			<?=$htmlAry['contentTbl']?>
		</div>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['backBtn']?>
			<p class="spacer"></p>
		</div>	
	</div>
	<input type="hidden" name="hideUserID" id="hideUserID"/>
	<input type="hidden" name="hidePreviousBalance" id="hidePreviousBalance"/>
	<input type="hidden" name="hideUpdateBalance" id="hideUpdateBalance"/>
	<input type="hidden" name="hideAcademicYearID" id="hideAcademicYearID"/>
</form>




<script type="text/javascript">
var previousBalanceArr = [];
var updateBalanceArr = [];
var userIDArr = [];
var balanceValueText1 = "<?php echo $Lang['SLRS']['SubstitutionBalanceDes']['AdjustedValue']; ?>";
var balanceValueText2 = "<?php echo $Lang['SLRS']['SubstitutionBalanceRecordDes']['AfterBalance']; ?>";

function goSubmit() {
	if (calculateVal())
	{
		$('form#form1').attr('action', 'index.php?task=management<?=$slrsConfig['taskSeparator']?>substitute_balance<?=$slrsConfig['taskSeparator']?>edit_update').submit();
	}
	return false;
}

function back() {
	window.location = '?task=management<?=$slrsConfig['taskSeparator']?>substitute_balance<?=$slrsConfig['taskSeparator']?>list';
}

function calculateVal() {
	if (!$("input[name='balanceSelection']:checked").val()) {
        $("#balanceSelectionEmptyWarnDiv").show();
        return false;
	}
	else if($("#adjustedValue").val() == ""){
		$("#adjustedValueEmptyWarnDiv").show();
		return false;
	}
	var adjustedType = $('input[name=balanceSelection]:checked').val();
	var adjustedValue = $("#adjustedValue").val();
	if (adjustedValue != "") {
		if(adjustedType == "ADD"){
			for(i=0; i < updateBalanceArr.length; i++){
				updateBalanceArr[i] = parseInt(previousBalanceArr[i]) + parseInt(adjustedValue);
			}
		}
		else if(adjustedType == "REDUCE"){
			for(i=0; i < updateBalanceArr.length; i++){
				updateBalanceArr[i] = parseInt(previousBalanceArr[i]) - parseInt(adjustedValue);
			}
		}
		else if(adjustedType == "SET"){
			for(i=0; i < updateBalanceArr.length; i++){
				updateBalanceArr[i] = parseInt(adjustedValue);
			}
		}
		$("#hideUpdateBalance").val(updateBalanceArr);
		for(i=0; i < updateBalanceArr.length; i++){
			$("#balance_"+i).text(previousBalanceArr[i] + "->" + updateBalanceArr[i]);
		}
	}
	return true;
}

$('#balanceRadio_reduce').click(function() {
	$("#balanceSelectionEmptyWarnDiv").hide();
	$('#initialDate').hide();
	$('#balanceValueText').text(balanceValueText1);
	calculateVal();
});

$('#balanceRadio_add').click(function() {
	$("#balanceSelectionEmptyWarnDiv").hide();
	$('#initialDate').hide();
	$('#balanceValueText').text(balanceValueText1);
	calculateVal();
});

$('#balanceRadio_set').click(function() {
	$("#balanceSelectionEmptyWarnDiv").hide();
	$('#initialDate').hide();
	$('#balanceValueText').text(balanceValueText2);
	calculateVal();
});

$('#balanceRadio_set_year').click(function() {
	$("#balanceSelectionEmptyWarnDiv").hide();
	$('#initialDate').show();
	$('#balanceValueText').text(balanceValueText2);
	calculateVal();
});

$("#adjustedValue").keyup(function(){
	calculateVal();
});

var slrsJS = {
		vrs: {
			ready: false
		},
		ltr: {
			adjustedValueHandler: function(e) {
				if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
		             // Allow: Ctrl+A, Command+A
		            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
		             // Allow: home, end, left, right, down, up
		            (e.keyCode >= 35 && e.keyCode <= 40)) {
		                 // let it happen, don't do anything
		                 return;
		        }
		        // Ensure that it is a number and stop the keypress
		        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
		            e.preventDefault();
		        }
			}
		},
		cfn: {
			init: function() {
				if ($(".js-basic-single").length > 0) {
				 	$(".js-basic-single").select2({
				 		language: "<?php echo (($intranet_session_language == "en") ? "en" : "zh-TW"); ?>",
					 	placeholder: '<?php echo $Lang['SLRS']['NotAvailableTeachersForm']["please-select"]; ?>'
					});
				}
				var options = {
						language: "<?php echo (($intranet_session_language == "en") ? "en" : "zh"); ?>",
					    dateFormat: 'yyyy-mm-dd',
				        autoClose: true,
					    toggleSelected: false,
					    range: false,
					    timepicker: false,
					    todayButton: true,
					    minDate: new Date(<?php echo $dateLimitation['min']["Y"]; ?>, <?php echo $dateLimitation['min']["M"]; ?>, <?php echo $dateLimitation['min']["D"]; ?>),
					    maxDate: new Date(<?php echo $dateLimitation['max']["Y"]; ?>, <?php echo $dateLimitation['max']["M"]; ?>, <?php echo $dateLimitation['max']["D"]; ?>),
					    onSelect: function(data, date, inst) {
					    }
					};
				$('#datePicker').datepicker(options);
				slrsJS.vrs.ready = true;
			}
		}	
	};

$('#adjustedValue').bind('keydown', slrsJS.ltr.adjustedValueHandler);

$(document).ready( function() {
	previousBalanceArr = "<?=$htmlAry['previousBalanceArr']?>".split(",");
	updateBalanceArr = "<?=$htmlAry['updatedBalanceArr']?>".split(",");
	userIDArr = "<?=$htmlAry['userIDArr']?>".split(",");
	$("#hideUserID").val(userIDArr);
	$("#hidePreviousBalance").val(previousBalanceArr);
	$("#hideUpdateBalance").val(updateBalanceArr);
	$("#hideAcademicYearID").val("<?=$htmlAry['academicYearID']?>");
	slrsJS.cfn.init();
});
</script>
