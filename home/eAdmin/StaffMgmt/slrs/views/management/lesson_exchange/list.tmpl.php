<link rel="stylesheet" href="/templates/slrs/css/select2.css?v=20180125">
<link rel="stylesheet" href="/templates/slrs/css/font-awesome.min.css?v=20180125">
<link rel="stylesheet" href="/templates/slrs/jconfirm/jquery-confirm.min.css?v=20180125">
<script src="/templates/jquery/jquery-1.8.3.min.js"></script>
<script src="/templates/jquery/jquery-migrate-1.4.1.min.js"></script>
<script src="/templates/slrs/jconfirm/jquery-confirm.min.js"></script>
<form name="form1" id="form1" method="POST" action="?task=management<?=$slrsConfig['taskSeparator']?>lesson_exchange<?=$slrsConfig['taskSeparator']?>list">
	<div class="table_board">
	
		<div class="content_top_tool">
			<?=$htmlAry['contentTool']?>
			<?=$htmlAry['searchBox']?>
			<br style="clear:both;">
		</div>
		
		<p class="spacer"></p>
		<?=$htmlAry['dbTableActionBtn']?>
		<?=$htmlAry['dataTable']?>
		<p class="spacer"></p>
		
		<?=$htmlAry['hiddenField']?>
		<?=$htmlAry['maxRow']?>	
		
	</div>
</form>

<script type="text/javascript">
$(document).ready( function() {	
	$('input#keyword').keydown( function(evt) {
		if (Check_Pressed_Enter(evt)) {
			// pressed enter
			goSearch();
		}
	});	
	getListDetails();

	$('.AnotherRecBtn').bind('click', anotherRecBtnHandler);
});

function anotherRecBtnHandler(e)
{
	var link = $(this).attr('rel');
	if (typeof link  != "undefined") {
		location.href = link;
	}
   	e.preventDefault();
}

function anotherRecConfirmBtnHandler(e)
{
	var link = $(this).attr('rel');
	var content = $(this).html();
	var holderID = $(this).attr('data-id');
	var exchangeInfo = $('#' + holderID).html();
	var new_location = $('#' + holderID + ' .lessonB_location').html();

	var confirmBoxInfo = "";
	confirmBoxInfo += "<div class='slrs-card-time sameInfo'>";
	confirmBoxInfo += exchangeInfo;
	confirmBoxInfo += "<div class='slrs-clear'>&nbsp;</div>";
	confirmBoxInfo += "</div>"
	confirmBoxInfo += "<div class='slrs-card-time confirmbox'>";
	confirmBoxInfo += exchangeInfo;
	confirmBoxInfo += "<div class='slrs-card-body slrs-card-body-green'>";
	confirmBoxInfo += "<div class='slrs-card-body-content'>";
	confirmBoxInfo += content;
	confirmBoxInfo += new_location;
	confirmBoxInfo += "</div>";
	confirmBoxInfo += "</div>";
	confirmBoxInfo += "<div class='slrs-clear'>&nbsp;</div>";
	confirmBoxInfo += "</div>";
	confirmBoxInfo += "<div class='alertmsg'><i class='fa fa-warning'></i> <?php echo $Lang['SLRS']['ForcedExchangePrompt']; ?></div>"; 
	
	if (typeof link  != "undefined") {
		// location.href = link;
		$.confirm({
			escapeKey: 'cancel',
		    title: "<span class='boxtitle'><?php echo $Lang['SLRS']['ForcedExchange']; ?></span>",
		    content: confirmBoxInfo,
		    type: 'green',
		    closeIcon: true,
		    draggable: false,
		    useBootstrap: false,
		    buttons: {
		        confirm: {
		        	btnClass: 'btn-green',
		        	text: '<?php echo $Lang['SLRS']['confirmBtn']; ?>',
			        action: function () {
			        	location.href = link;
			        }
		        },
		        cancel: {
			        text: '<?php echo $Lang['SLRS']['cancelBtn']; ?>',
			        action: function() {}
		        }
		    }
		});
	}
   	e.preventDefault();
}

function goSearch() {
	$('form#form1').submit();
}

function goExchangeLesson(){
	window.location = '?task=management<?=$slrsConfig['taskSeparator']?>lesson_exchange<?=$slrsConfig['taskSeparator']?>edit';
}

function goEdit(){
	var valid = true
	if ($('input:checked').size() > 0)
	{
		$('input:checked').each(function() {
			if ($(this).attr('rel') == "SLAVE")
			{
				valid = false;
			}
		});
	}	 
	if (valid)
	{
		checkEdit(document.form1,'groupIdAry[]','?task=management<?=$slrsConfig['taskSeparator']?>lesson_exchange<?=$slrsConfig['taskSeparator']?>edit');
	}
	else
	{
		alert("<?php echo $Lang['SLRS']['ForcedExchangeSlaveAlert']; ?>");
	}
}
function goDelete(){
	checkRemove(document.form1,'groupIdAry[]','?task=management<?=$slrsConfig['taskSeparator']?>lesson_exchange<?=$slrsConfig['taskSeparator']?>delete');
}
function getListDetails(){
	var rowGroupID = $("#rowGroupID").val();
	var data = "type=list&groupID="+rowGroupID+"&keyword="+$("#keyword").val();
	$.ajax({ type: "POST", url: '?task=management<?=$slrsConfig['taskSeparator']?>lesson_exchange<?=$slrsConfig['taskSeparator']?>ajax_getData', 
        data: data,
        async:false,
        success: function (data, textStatus, jqXHR) {
            var rowGroupIDArr = rowGroupID.split(',');			
           	var result = eval('(' + data + ')');
           	var groupID = "";
           	var teacher="", lesson="", modifiedDate="";
//            	for (i=0; i<rowGroupIDArr.length; i++){
//                	for(j=0; j<result.length; j++){
//                 	if(rowGroupIDArr[i] == result[j]["GroupID"]){  	
// 						teacher += result[j]["Name"]+"<br/>";
// 						if (result[j]["hasSubstitution"] == "1")
//                 		{
// 	                		lesson += "<div class='slrs-card slrs-card-red'> ** ";
//                 		}
// 						else
// 						{
// 							lesson += "<div class='slrs-card'>";
// 						}
// 						lesson += result[j]["Date"] + " " + result[j]["TimeSlotName"] + " (" + result[j]["SlotTime"] + ")"
// 										+ "<div class='slrs-card-body'>"
// 										+ result[j]["orgName"] + " ("
// 										+ result[j]["ClassName"] + ")<br/>"
// 										+ "</div>" + "</div>";
// 						if (result[j]["hasSubstitution"] == "1")
// 						{
// 							$("#name_"+rowGroupIDArr[i]).parent().parent().find('input[type="checkbox"]').eq(0).replaceWith('<center>-</center>');
// 						}
// 						else
// 						{
// 							$("#name_"+rowGroupIDArr[i]).parent().parent().find('input[type="checkbox"]').eq(0).attr("disabled", '');
// 						}
//                     }
//                	}
// 				$("#name_"+rowGroupIDArr[i]).append(teacher);
// 				$("#lesson_"+rowGroupIDArr[i]).append(lesson);
//                	teacher = "";    
//                	lesson = "";
//                	modifiedDate = "";     	 
//            	}
			if (Object.keys(result).length > 0)
			{
				$.each(result, function(groupID, LessonData) {
					$('#name_' + groupID).html(LessonData.teacherInfo);
					if (LessonData.combined_type == "MASTER")
					{
						$('#lesson_' + groupID).html(LessonData.exchangeDetail + '<div class="combinedMsg"><i class="fa fa-window-maximize" aria-hidden="true"></i> <?php echo $Lang['SLRS']['ForcedExchangeMaster']; ?></div>');
					}
					else if (LessonData.combined_type == "SLAVE")
					{
						$('#lesson_' + groupID).html(LessonData.exchangeDetail + '<div class="combinedMsg"><i class="fa fa-window-restore" aria-hidden="true"></i> <?php echo $Lang['SLRS']['ForcedExchangeSlave']; ?></div>');
					}
					else
					{
						$('#lesson_' + groupID).html(LessonData.exchangeDetail);
					}
					
 					if (LessonData.hasSubstitution || LessonData.combined_type == "MASTER")
 					{
 						$("#name_"+groupID).parent().parent().find('input[type="checkbox"]').eq(0).replaceWith('<center>-</center>');
 					} else {
 						$("#name_"+groupID).parent().parent().find('input[type="checkbox"]').eq(0).attr("disabled", '');
 						$("#name_"+groupID).parent().parent().find('input[type="checkbox"]').eq(0).prop("disabled", false);
 						$("#name_"+groupID).parent().parent().find('input[type="checkbox"]').eq(0).attr("rel", LessonData.combined_type);
 					}
				});
			}
			$('.anotherForcedExchangeBtn').unbind('click', anotherRecConfirmBtnHandler);
			$('.anotherForcedExchangeBtn').bind('click', anotherRecConfirmBtnHandler);

        }, error: function (jqXHR, textStatus, errorThrown) {
           
        }
    });
}
</script>