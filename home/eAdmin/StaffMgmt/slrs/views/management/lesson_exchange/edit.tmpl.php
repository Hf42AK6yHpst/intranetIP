<form id="form1" name="form1" method="post" action="?task=management<?=$slrsConfig['taskSeparator']?>lesson_exchange<?=$slrsConfig['taskSeparator']?>ajax_update">
	<div class="table_board">
		<div>
			<?php echo $htmlAry['contentTbl']?>
		</div>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?php echo $htmlAry['submitBtn']?>
			<?php echo $htmlAry['backBtn']?>
			<p class="spacer"></p>
		</div>	
	</div>
	<input type="hidden" id="rowGroupID" value="<?php echo $htmlAry['rowGroupID']; ?>">
	<input type="hidden" id="startIndex" value="<?php echo $htmlAry['startIndex']; ?>">
	<input type="hidden" id="endIndex" value="<?php echo $htmlAry['endIndex']; ?>">
	
</form>

<script type="text/javascript">

	var loadingTxt = '<?php echo $htmlAry["loadingTxt"]; ?>';
	var debugMsg = false;
	$(document).ready(function(){
		$('#div_1').append('<br><div class="loadingbox"></div>');
		$('#div_1 .loadingbox').eq(0).css({"padding":"5px","color":"#808080"}).hide().html(loadingTxt);
		var groupID = $("#rowGroupID").val();
		if(groupID == ""){
<?php if (isset($anotherInfo)) { ?>
			getLessonDetails("timeSlot_1");
<?php } else { ?>
			changeDatePicker("datePicker_1");
<?php } ?>
			changeDatePicker("datePicker_2");
		}
		// $('input[type="submit"]').hide();
	});	
	$("select[id^='datePicker']").change(function(){
		resetOptions(this.id, "datePicker");
		getTeachingTeacherDetails(this.id);
		getRoomDetails(this.id);
		$('.lessonMsg').remove();
	});
	$("select[id^='teachingTeacher']").change(function(){
		resetOptions(this.id, "teachingTeacher");
		getRoomDetails(this.id);
		$('.lessonMsg').remove();
	});
	$("select[id^='timeSlot']").change(function(){
		resetOptions(this.id, "timeSlot");
		getLessonDetails(this.id);
		$('.lessonMsg').remove();
	});

	$("[id^='locationText']").click(function(e){
		var id = e.target.id;
		showLocationList(id);
	});

	
	function showLocationList(id){
		if (debugMsg) console.log("%c showLocationList", 'color: #bada55', id);
		var index = id.split("_").pop();
		var text = $("#locationText_"+index).text();
		if(text == "<?= $Lang['SLRS']['SubstitutionArrangementDes']['ToBeChanged'] ?>"){		
			$("#locationText_"+index).text("<?= $Lang['SLRS']['SubstitutionArrangementDes']['Cancel'] ?>");
			var tgtID = "#locationDiv_"+index;
			$("#locationDiv_"+index).show();
		}
		else{
			var preIndex = (index % 2==0)?(index-1):index;
			var locationID = (index % 2==0)?$("#LocationID_"+preIndex).val():result[0].LocationID;
			$("#locationText_"+index).text("<?= $Lang['SLRS']['SubstitutionArrangementDes']['ToBeChanged'] ?>");
			var hideLocationID = $("#LocationID_"+preIndex).val();
			var tgtID = "#locationDiv_"+index;
			$("#locationDiv_"+index+" option[value="+hideLocationID+"]").attr("selected","selected");
			$("#locationDiv_"+index).hide();
			$("#LocationID_"+index).val(hideLocationID);
		}
	}

	$("select[id^='locationID']").change(function(e){
		var id = e.target.id;
		changeLocationList(id);
	});

	function changeLocationList(id){
		if (debugMsg) console.log("%c changeLocationList", 'color: #bada55', id);
		var value = $("#"+id+" option:selected").val();
		var index = id.split("_").pop();
		$("#LocationID_"+index).val(value);
	}
	
	function changeDatePicker(id){
		if (debugMsg) console.log("%c changeDatePicker", 'color: #bada55', id);
		$('.lessonMsg').remove();
		var groupID = $("#rowGroupID").val();
		resetOptions(id, "datePicker");
		getTeachingTeacherDetails(id);
		getRoomDetails(id);
	}
	function changeTeachingTeacher(id){
		if (debugMsg) console.log("%c changeTeachingTeacher", 'color: #bada55', id);
		resetOptions(id, "teachingTeacher");
		getRoomDetails(id);
	}
	function changeRoom(id){
		if (debugMsg) console.log("%c changeRoom", 'color: #bada55', id);
		resetOptions(id, "timeSlot");
		getLessonDetails(id);
	}	
	function resetOptions(id, type){
		if (debugMsg) console.log("%c resetOptions", 'color: #bada55', id);
		var inputID = id;
		//var index = inputID.substr(inputID.indexOf("_")+1,1);
		var index=inputID.split("_").pop();
		//reset selection
		if(index == 1 && type == "teachingTeacher"){
			$("#detailsLesson_"+index).empty();			
			$("#divCenter_1").hide();
			$("#div_2").hide();			
			$("#teachingTeacher_2 option:selected").removeAttr("selected");
			$("#timeSlot_2 option:selected").removeAttr("selected");
			$("#detailsLesson_2").empty();
			var maxIndex = parseInt($("#endIndex").val());
			for (i=parseInt(index)+2; i<=maxIndex; i++){
				$("#div_"+i).remove();
				$("#divCenter_"+i).remove();
			}		
			$("#endIndex").val("2");
			$('.lessonMsg').remove();	
		}
		else if (index >= 2 && type == "teachingTeacher"){
			var maxIndex = parseInt($("#endIndex").val());
			$("#timeSlot_" +index +" option:selected").removeAttr("selected");
			$("#detailsLesson_"+index).empty();	
			for (i=parseInt(index)+1; i<=maxIndex; i++){
				$("#div_"+i).remove();
				$("#divCenter_"+i).remove();
			}
			$("#endIndex").val(index);
		}
		else if(index == 1 && type == "datePicker"){
			var maxIndex = parseInt($("#endIndex").val());
			$("#detailsLesson_"+index).empty();			
			$("#divCenter_1").hide();
			$("#div_2").hide();		
			$("#teachingTeacher_1").find("option:gt(0)").empty();
			$("#timeSlot_1").find("option:gt(0)").empty();	
			for (i=parseInt(index)+2; i<=maxIndex; i++){
				$("#div_"+i).remove();
				$("#divCenter_"+i).remove();
			}
			$("#endIndex").val("2");
		}
		else if(index >= 2 && type == "datePicker"){
			var maxIndex = parseInt($("#endIndex").val());			
			$("#teachingTeacher_" +index +" option:selected").removeAttr("selected");
			$("#timeSlot_" +index +" option:selected").removeAttr("selected");
			$("#detailsLesson_"+index).empty();		
			for (i=parseInt(index)+1; i<=maxIndex; i++){
				$("#div_"+i).remove();
				$("#divCenter_"+i).remove();
			}
			$("#endIndex").val(index);
		}
	}
	function getTeachingTeacherDetails(id){
		if (debugMsg) console.log("%c getTeachingTeacherDetails", 'color: #bada55', id);
		var inputID = id;
		//var index = inputID.substr(inputID.indexOf("_")+1,1);
		var index=inputID.split("_").pop();
		// var val = $("#"+inputID +" option:selected").val();
		var teacher = $("#teachingTeacher_"+index).val();
// 		if (index % 2 == 0)
// 		{
// 			var preIndex = index - 1;
// 			var teacher = $("#teachingTeacher_"+preIndex).val();
// 		}

		if (debugMsg) console.log("inputID :", inputID, $("#"+inputID).val());

		if (debugMsg) console.log("index", index, inputID);
		if (debugMsg) console.log("teacher", teacher);
		
		var data = "type=teacher&"+"teacher="+teacher+"&date="+$("#datePicker_"+index).val();
		var groupID = $("#rowGroupID").val();
		if (debugMsg) console.log("%c getTeachingTeacherDetails > rowGroupID", 'color: #0f0', groupID);
		if(groupID != ""){
			data += "&groupID=" + groupID; 
		}
		
		if (debugMsg) console.log("%c getTeachingTeacherDetails > aj", 'color: #f00', data);
		$('input[type="submit"]').hide();
		$.ajax({ type: "POST", url: '?task=management<?=$slrsConfig['taskSeparator']?>lesson_exchange<?=$slrsConfig['taskSeparator']?>ajax_getData', 
	        data: data,
	        async: debugMsg,
            success: function (data, textStatus, jqXHR) {
            	if (debugMsg) console.log("getTeachingTeacherDetails", data);
                
				var teachingTeacher = "teachingTeacher_" + index;
               	var result = eval('(' + data + ')');
               	// get room information
               	var firstOptionValue = $("#" + teachingTeacher + " option:first").val();
               	var firstOptionText = $("#" + teachingTeacher + " option:first").text();
				$("#" + teachingTeacher).empty();
				$("#" + teachingTeacher).append($('<option></option>').val(firstOptionValue).html(firstOptionText));
                for (i=0; i<result.length; i++){
                	$("#" + teachingTeacher).append($('<option></option>').val(result[i].UserID).html(result[i].Name));
                }
                
            }, error: function (jqXHR, textStatus, errorThrown) {
               
            }
        });
	}

	function getRoomDetails(id){
		
		var inputID = id;
		//var index = inputID.substr(inputID.indexOf("_")+1,1);
		var index=inputID.split("_").pop();
		var val = $("#teachingTeacher_"+ index ).val();

		if (debugMsg) console.log("%c getRoomDetails", 'color: #bada55');
		if (debugMsg) console.log("inputID :", inputID, $("#"+inputID).val(), index);
		
		if (typeof val != "undefined") {
			var data = "type=room&"+"teacher="+val+"&date="+$("#datePicker_"+index).val();

			var groupID = $("#rowGroupID").val();
			if(groupID != ""){
				data += "&groupID=" + groupID; 
			}
			
			$('#div_1 .loadingbox').eq(0).show();
			$('input[type="submit"]').hide();

			if (debugMsg) console.log("%c getRoomDetails > aj", 'color: #f00', data);
			
			$.ajax({ type: "POST", url: '?task=management<?=$slrsConfig['taskSeparator']?>lesson_exchange<?=$slrsConfig['taskSeparator']?>ajax_getData', 
		        data: data,
		        async: debugMsg,
	            success: function (data, textStatus, jqXHR) {

	            	if (debugMsg) console.log("%c getRoomDetails > Data", 'background: #222; color: #bada55');
	            	if (debugMsg) console.log(data);
	            	
		            
	            	$('#div_1 .loadingbox').eq(0).hide();
					var roomID = "timeSlot_" + index;
	               	var result = eval('(' + data + ')');
	               	// get room information
	               	
	               	var firstOptionValue = $("#" + roomID + " option:first").val();
	               	var firstOptionText = $("#" + roomID + " option:first").text();

					$("#" + roomID).empty();
					$("#" + roomID).append($('<option></option>')
								.val(firstOptionValue)
								.html(firstOptionText));
	                for (i=0; i<result.length; i++){
	                	$("#" + roomID).append($('<option></option>')
	    	                	.val(result[i].TimeSlotID)
	    	                	.attr('rel', result[i].SubjectGroupID)
	    	                	.html(result[i].Name));
	                }
	            }, error: function (jqXHR, textStatus, errorThrown) {
	            	$('#div_1 .loadingbox').eq(0).hide();
	            }
	        });
		}
	}

	function getLessonDetails(id){
		if (debugMsg) console.log("%c getLessonDetails", 'color: #bada55', id);
		var inputID = id;
		//var index = inputID.substr(inputID.indexOf("_")+1,1);
		
		var index=inputID.split("_").pop();
		var val = $("#"+inputID +" option:selected").val();
		var teacherVal = $("#teachingTeacher_"+index).val();
		var subjectGroup = $("#" + id + " option:selected").attr('rel');

		if (debugMsg) console.log("subjectGroup", subjectGroup);
		if (debugMsg) console.log("teacherVal", teacherVal);
		if (debugMsg) console.log("index", index, inputID);
		
		var data = "type=roomDetails&"+"teacher="+teacherVal+"&timeSlotID="+val+"&date="+$("#datePicker_"+index).val();
		if (typeof subjectGroup != "undefined") {
			data += "&sg=" + subjectGroup;
		}
		var groupID = $("#rowGroupID").val();
		if(groupID != ""){
			data += "&groupID=" + groupID; 
		}
		if ($('textarea[name="LessonRemarks_' + index + '"]').size() > 0) {
			var lessonLabel = $("#" + id + " option:selected").text();
			$("#div_" + index).find('span.lesson_remarks_txt').eq(0).text(lessonLabel);					
		}

		if (debugMsg) console.log("%c getLessonDetails > aj **", 'color: #f00', data);
		
		$.ajax({ type: "POST", url: '?task=management<?=$slrsConfig['taskSeparator']?>lesson_exchange<?=$slrsConfig['taskSeparator']?>ajax_getData', 
	        data: data,
	        async: debugMsg,
            success: function (data, textStatus, jqXHR) {
            	if (debugMsg) console.log("getLessonDetails", data);
            	if (debugMsg) console.log("detailsLesson_" + index);
            	if (debugMsg) console.log("%c getLessonDetails > rp", 'color: #f00', data);
            	
				var detailsLessonID = "detailsLesson_" + index;
               	var result = eval('(' + data + ')');

               	if (debugMsg) console.log("result" + result);

               	$("#"+detailsLessonID).empty();
               	//$("#"+detailsLessonID).append("<table class=\"common_table_list slrs_list\"></table");
               	var preIndex = "";
               	var startTime = "";
               	var endTime = "";
               	var day = "";
               	var timeSlotName = "";
               	var locationName = "";
               	var locationID = "";
               	var subjectGroupID = "";
               	var subjectGroupName = "";
               	var date = "";

               	if (index % 2 == 0 && index > 1) {
	               	$(".slrs_exchange").each(function(remInd, remData) {
						var div_id = $(this).attr('id');
						var idInt = parseInt(div_id.replace("div_", ""));
						if (parseInt(index) < idInt) {
							$("#div_" + idInt).remove();
							if (idInt % 2 == 1) {
								$("#divCenter_" + idInt).remove();
							}
						} else {
							$("#endIndex").val(idInt);
						}
					});
	               	$('input[type="submit"]').hide();
               	}
               	if (typeof result[0] == "undefined") {
                   	return false;
               	}
               	
               	try {
               		preIndex = (index % 2==0)?(index-1):index;

               		/*
               		startTime = (index % 2==0)?$("#StartTime_"+preIndex).val():result[0].StartTime;
               		endTime = (index % 2==0)?$("#EndTime_"+preIndex).val():result[0].EndTime;
               		day = (index % 2==0)?$("#Day_"+preIndex).val():result[0].Day;
	               	timeSlotName = (index % 2==0)?$("#TimeSlotName_"+preIndex).val():result[0].TimeSlotName;
	               	*/
	               	startTime = result[0].StartTime;
               		endTime = result[0].EndTime;
               		day = result[0].Day;
	               	timeSlotName = result[0].TimeSlotName;

	               	/*
               		locationName = (index % 2==0)?$("#LocationName_"+preIndex).val():result[0].Name;
               		locationID = (index % 2==0)?$("#LocationID_"+preIndex).val():result[0].LocationID;
               		*/
               		locationName = result[0].Name;
               		locationID = result[0].LocationID;

					// subjectGroupID = (index % 2==0)?$("#TimeSlotName_"+preIndex).attr("rel"):result[0].SubjectGroupID;
               		subjectGroupID = result[0].SubjectGroupID;

               		if (typeof result[0].SubjectGroupName != "undefined") {
               			subjectGroupName = (index % 2==0) ? $("#SubjectGroupName_"+preIndex).val() : result[0].SubjectGroupName;
	               		// subjectGroupName = result[0].SubjectGroupName;
               		}    
               		// date = (index % 2==0)?$("#DatePicker_"+preIndex).val():result[0].Date;
               		date = result[0].Date;
               	} catch (e) {
                   	//Block of code to handle errors
                   	if (debugMsg) console.log(e);
               	}
				var detailsTable = "<table class=\"common_table_list slrs_list\">";
				detailsTable += "<tr><th style='width:30%;'></th><th style='width:30%;' class=\"date\">" + date + "<br/>" + day + "</th>";
				detailsTable += "<th style='width:40%;' class=\"date\">" + "<?=$Lang['SLRS']['LessonExchangeDes']['Location']?>" + "</th></tr>";
				detailsTable += "<tr><td>"+startTime+" - "+endTime+"<br/>"+timeSlotName+"</td>";
				if(index % 2 == 1){
					// for failure result			
					detailsTable += "<td class=\"date class_fail\">"+subjectGroupName+"</td>";
					detailsTable += "<td class=\"date\">"+locationName;										
					detailsTable += "</td>";
				}
				else if(index % 2 == 0){
					// for succes result
					var locationNameArray = (typeof result[0] != "undefined") ? result[0].LocationName.split(",") : "";
					var locationValueArray = (typeof result[0] != "undefined") ? result[0].LocationValue.split(",") : "";
					var list = "locationID_"+index+"<select id=locationID_"+index+" name=locationID_"+index+" onchange=changeLocationList(\"locationID_"+index+"\")>";
					for(i=0; i<locationNameArray.length; i++){
						if(locationID == locationValueArray[i]){
							list += "<option value=\""+locationValueArray[i]+"\" selected>"+locationNameArray[i]+"</option>";
						}
						else{
							list += "<option value=\""+locationValueArray[i]+"\">"+locationNameArray[i]+"</option>";
						}
					}
					list += "</select>";
					detailsTable += "<td class=\"date class_success\">"+subjectGroupName+"</td>";
					detailsTable += "<td class=\"date\">"+locationName					
						detailsTable += "<div><a id=\"locationText_"+index+"\" onclick=showLocationList(\"locationText_"+index+"\")>"+"<?=$Lang['SLRS']['LessonExchangeDes']['ToBeChanged']?>"+"</a><br/>";
						detailsTable += "<div id=\"locationDiv_"+index+"\" style=\"display:none\">";
						detailsTable += list;
						detailsTable += "</div>";	
					detailsTable += "</div>";	
					detailsTable += "</td>";
				}
				detailsTable +="</tr>";
				detailsTable +="</table>";
				detailsTable +="<input type=\"hidden\" id=\"StartTime_"+index+"\" value=\""+result[0].StartTime+"\"/>";
				detailsTable +="<input type=\"hidden\" id=\"EndTime_"+index+"\" value=\""+result[0].EndTime+"\"/>";
				detailsTable +="<input type=\"hidden\" id=\"TimeSlotID_"+index+"\" value=\""+result[0].TimeSlotID+"\"/>";
				detailsTable +="<input type=\"hidden\" id=\"SubjectGroupID_"+index+"\" value=\""+result[0].SubjectGroupID+"\"/>";
				detailsTable +="<input type=\"hidden\" id=\"YearClassID_"+index+"\" value=\""+result[0].YearClassID+"\"/>";
				detailsTable +="<input type=\"hidden\" id=\"SubjectID_"+index+"\" value=\""+result[0].SubjectID+"\"/>";
				detailsTable +="<input type=\"hidden\" id=\"LocationID_"+index+"\" value=\""+locationID+"\"/>";
				detailsTable +="<input type=\"hidden\" id=\"Day_"+index+"\" value=\""+result[0].Day+"\"/>";
				detailsTable +="<input type=\"hidden\" id=\"TimeSlotName_"+index+"\" value=\""+result[0].TimeSlotName+"\"/>";
				detailsTable +="<input type=\"hidden\" id=\"SubjectGroupName_"+index+"\" value=\""+result[0].SubjectGroupName+"\"/>";
				detailsTable +="<input type=\"hidden\" id=\"LocationName_"+index+"\" value=\""+result[0].Name+"\"/>";
				detailsTable +="<input type=\"hidden\" id=\"DatePicker_"+index+"\" value=\""+result[0].Date+"\"/>";
				$("#"+detailsLessonID).append(detailsTable);
				$("#divCenter_1").show();
				$("#div_2").show();

				var previousID = "";
				var currentID = "";
				var previousTimeSlot = "";
				var currentTimeSlot = "";
				var previousSubjectGroup = "";
				var currentSubjectGroup = "";
				var previousDate = "";
				var currentDate = "";

				currentID = $("#teachingTeacher_"+index+" option:selected").val();
				currentTimeSlot = $("#timeSlot_"+index+" option:selected").val();
				currentSubjectGroup = $("#timeSlot_"+index+" option:selected").attr('rel');
				currentDate = $("#datePicker_"+index).val();
				/*
				if(index == 2){					
					previousID = $("#teachingTeacher_"+(index-1)+" option:selected").val();
					if (previousID == "") {
						previousID = $("#teachingTeacher_"+(index-1)).val();
					}
					previousTimeSlot = $("#timeSlot_"+(index-1)+" option:selected").val();
					if (previousTimeSlot == "") {
						previousTimeSlot =$("#timeSlot_"+(index-1)+"").val();
					}
				}else if(index > 2){
					previousID = $("#teachingTeacher_1 option:selected").val();
					if (previousID == "") {
						previousID = $("#teachingTeacher_1").val();
					}
					previousTimeSlot = $("#timeSlot_1 option:selected").val();
					if (previousTimeSlot == "") {
						previousTimeSlot = $("#timeSlot_1").val();
					}
				}
				*/
				if (index >= 2) {
					previousID = $("#teachingTeacher_1 option:selected").val();
					if (previousID == "" || previousID=='undefined') {
						previousID = $("#teachingTeacher_1").val();
					}
					previousTimeSlot = $("#timeSlot_1 option:selected").val();
					if (previousTimeSlot == "" || previousTimeSlot=='undefined') {
						previousTimeSlot = $("#timeSlot_1").val();
					}
					previousDate = $("#datePicker_1").val();
					previousSubjectGroup = $("#timeSlot_1 option:selected").attr('rel');
				}
				//if(index % 2 == 0 && !(previousID == currentID && previousSubjectGroupID == currentSubjectGroupID)){
				//(previousTimeSlot+" "+currentTimeSlot);
				if(index % 2 == 0 && !((previousID == currentID) && (previousTimeSlot==currentTimeSlot) && (previousSubjectGroup == currentSubjectGroup) && (previousDate == currentDate)) ){
					data = "maxRowIndex="+$("#endIndex").val()+"&date="+$("#datePicker_"+index).val()+"&teacherName="+$("#teachingTeacher_"+index+" option:selected").text();
					data += "&room="+$("#timeSlot_"+index+" option:selected").text();
					data += "&startTime="+result[0].StartTime+"&endTime="+result[0].EndTime+"&subjectGroupName="+result[0].SubjectGroupName+"&locationName="+result[0].Name;
					data += "&day="+result[0].Day+"&timeSlotName="+result[0].TimeSlotName+"&oriDate="+$("#datePicker_1").val();
					data += "&timeSlotID="+result[0].TimeSlotID+"&yearClassID="+result[0].YearClassID+"&subjectID="+result[0].SubjectID+"&subjectGroupID="+result[0].SubjectGroupID;
					data += "&teachingTeacher="+$("#teachingTeacher_"+index+" option:selected").val()+"&classRoom="+$("#subjectGroup_"+index+" option:selected").val();
					data += "&locationID="+result[0].LocationID+"&groupID="+$("#rowGroupID").val();
					
					if (debugMsg) console.log("%c getLessonDetails > aj", 'color: #f00', data);
					
					$.ajax({ type: "POST", url: '?task=management<?=$slrsConfig['taskSeparator']?>lesson_exchange<?=$slrsConfig['taskSeparator']?>ajax_getNewExchange',
						data: data,
						async: debugMsg,
			            success: function (data, textStatus, jqXHR) {

			            	if (debugMsg) console.log("%c getLessonDetails > ** rp", 'color: #f00', data);
				            
				            //alert(data);
				            $("#exchangeTable").append(data);
				            var newIndex = parseInt(index) + 2;				            
				            $("#endIndex").val(newIndex);

				         // var teachingTeacherIndex = $("#teachingTeacher_1").get(0).selectedIndex;
				         //	$("#teachingTeacher_"+newIndex).attr("selectedIndex", teachingTeacherIndex);
				            
				            var teachingVal = $("#teachingTeacher_1").val();
				            $("#teachingTeacher_"+newIndex).find("option[value='" + teachingVal + "']").eq(0).attr("selected", 'selected');

				            if (debugMsg) console.log("new Index", newIndex);
				            
				            getRoomDetailsAuto("teachingTeacher_"+newIndex);
				            //getLessonDetails("subjectGroup_"+newIndex);
			            },error: function (jqXHR, textStatus, errorThrown) {
			            }		            
					}); 
				} else {
					$('input[type="submit"]').show();
				}
				
            }, error: function (jqXHR, textStatus, errorThrown) {
               
            }
        });
	}
	function getRoomDetailsAuto(id){
		if (debugMsg) console.log("%c getRoomDetailsAuto", 'color: #bada55', id);
		var inputID = id;
		//var index = inputID.substr(inputID.indexOf("_")+1,1);
		var index=inputID.split("_").pop();
		var val = $("#"+inputID +" option:selected").val();
		var data = "type=room&"+"teacher="+val+"&date="+$("#datePicker_"+index).val();

		var groupID = $("#rowGroupID").val();
		if(groupID != ""){
			data += "&groupID=" + groupID; 
		}
		
		if (debugMsg) console.log("index", index, inputID);
		if (debugMsg) console.log("val", val);

		if (debugMsg) console.log("%c getRoomDetailsAuto > aj", 'color: #f00', data);
		
		$.ajax({ type: "POST", url: '?task=management<?=$slrsConfig['taskSeparator']?>lesson_exchange<?=$slrsConfig['taskSeparator']?>ajax_getData', 
	        data: data,
	        async: debugMsg,
            success: function (data, textStatus, jqXHR) {
				var roomID = "timeSlot_" + index;
				var result = eval('(' + data + ')');
				if (debugMsg) console.log("%c getRoomDetailsAuto > rep", 'color: #f00', data);
				
				
               	// get room information
               	var firstOptionValue = $("#" + roomID + " option:first").val();
               	var firstOptionText = $("#" + roomID + " option:first").text();
               	if (debugMsg) console.log(firstOptionValue, firstOptionText); 
				$("#" + roomID).empty();
				$("#" + roomID).append($('<option></option>').val(firstOptionValue).html(firstOptionText));
                for (i=0; i<result.length; i++){
                	//$("#" + roomID).append($('<option></option>').val(result[0].TimeSlotID).html(result[0].Name));
                	$("#" + roomID).append($('<option></option>').val(result[i].TimeSlotID).attr('rel', result[i].SubjectGroupID).html(result[i].Name));
                }         
                var newIndex = $("#endIndex").val();
                
                // var classRoomIndex = $("#timeSlot_1").get(0).selectedIndex;
	            // $("#timeSlot_"+newIndex).attr("selectedIndex", classRoomIndex);
	            
	            var timeslotVal = $("#timeSlot_1").val();
				$("#timeSlot_"+newIndex).find("option[value='" + timeslotVal + "']").eq(0).attr("selected", 'selected');
	            
	            getLessonDetailsAuto("timeSlot_"+newIndex);
            }, error: function (jqXHR, textStatus, errorThrown) {
               
            }
        });
	}
	function getLessonDetailsAuto(id){
		if (debugMsg) console.log("%c getLessonDetailsAuto", 'color: #bada55', id);
		var inputID = id;
		var endIndex = parseInt($("#endIndex").val());
		//var index = inputID.substr(inputID.indexOf("_")+1,1);
		var index=inputID.split("_").pop();
		var val = $("#"+inputID +" option:selected").val();		
		var teacherVal = $("#teachingTeacher_"+index).val();
		var subjectGroup = $("#" + id + " option:selected").attr('rel');

		if (debugMsg) console.log("subjectGroup", subjectGroup);
		if (debugMsg) console.log("teacherVal", teacherVal);
		if (debugMsg) console.log("index", index, inputID);
		
		var data = "type=roomDetails&"+"teacher="+teacherVal+"&timeSlotID="+val+"&date="+$("#datePicker_"+index).val();;
		if (typeof subjectGroup != "undefined") {
			data += "&sg=" + subjectGroup;
		}

		var groupID = $("#rowGroupID").val();
		if(groupID != ""){
			data += "&groupID=" + groupID; 
		}
		if (debugMsg) console.log("%c getLessonDetailsAuto > aj", 'color: #f00', data);
		
		$.ajax({ type: "POST", url: '?task=management<?=$slrsConfig['taskSeparator']?>lesson_exchange<?=$slrsConfig['taskSeparator']?>ajax_getData', 
	        data: data,
	        async: debugMsg,
            success: function (data, textStatus, jqXHR) {
	           	if (debugMsg) console.log("%c getLessonDetailsAuto > rep", 'color: #f00', data);
	           	try {
					var detailsLessonID = "detailsLesson_" + index;
	               	var result = eval('(' + data + ')');
	               	$("#"+detailsLessonID).empty();
	               	//$("#"+detailsLessonID).append("<table class=\"common_table_list slrs_list\"></table");
	               	var preIndex = (index % 2==0)?(index-1):index;
	               	
	               	/*
	               	var startTime = (index % 2==0)?$("#StartTime_"+preIndex).val():result[0].StartTime;
	               	var endTime = (index % 2==0)?$("#EndTime_"+preIndex).val():result[0].EndTime;
	               	var day = (index % 2==0)?$("#Day_"+preIndex).val():result[0].Day;
	               	var timeSlotName = (index % 2==0)?$("#TimeSlotName_"+preIndex).val():result[0].TimeSlotName;
	               	var locationName = (index % 2==0)?$("#LocationName_"+preIndex).val():result[0].Name;
	               	var locationID = (index % 2==0)?$("#LocationID_"+preIndex).val():result[0].LocationID;
					*/
					var startTime = result[0].StartTime;
	               	var endTime = result[0].EndTime;
	               	var day = result[0].Day;
	               	var timeSlotName = result[0].TimeSlotName;
	               	var locationName = result[0].Name;
	               	var locationID = result[0].LocationID;
	               	
	               	var subjectGroupName = "";
	               	if (debugMsg) console.log("getLessonDetailsAuto", result);
	                   	
	               	if (typeof result[0].SubjectGroupName != "undefined") {
	               		// subjectGroupName = result[0].SubjectGroupName;
	               		subjectGroupName = (index % 2==0) ? $("#SubjectGroupName_"+preIndex).val() : result[0].SubjectGroupName;
	           		}
	               	// var date = (index % 2==0)?$("#DatePicker_"+preIndex).val():result[0].Date;
	               	var date = result[0].Date;
	
					var detailsTable = "<table class=\"common_table_list slrs_list\">";
					detailsTable += "<tr><th style='width:30%;'></th><th style='width:30%;' class=\"date\">" + date + "<br/>" + day + "</th>";
					detailsTable += "<th style='width:40%;' class=\"date\">" + "<?=$Lang['SLRS']['LessonExchangeDes']['Location']?>" + "</th></tr>";
					detailsTable += "<tr><td>"+startTime+" - "+endTime+"<br/>"+timeSlotName+"</td>";
					if(index % 2 == 1){
						// for failure result
						detailsTable += "<td class=\"date class_fail\">"+subjectGroupName+"</td><td>"+locationName+"</td>";
					}
					else if(index % 2 == 0){
						// for succes result
						// for succes result
						var locationNameArray = result[0].LocationName.split(",");
						var locationValueArray = result[0].LocationValue.split(",");
						var list = "<select id=locationID_"+index+" name=locationID_"+index+" onchange=changeLocationList(\"locationID_"+index+"\")>";
						for(i=0; i<locationNameArray.length; i++){
							if(locationID == locationValueArray[i]){
								list += "<option value=\""+locationValueArray[i]+"\" selected>"+locationNameArray[i]+"</option>";
							}
							else{
								list += "<option value=\""+locationValueArray[i]+"\">"+locationNameArray[i]+"</option>";
							}
						}
						list += "</select>";
						detailsTable += "<td class=\"date class_success\">"+subjectGroupName+"</td>";
						detailsTable += "<td class=\"date\">"+locationName
							detailsTable += "<div><a id=\"locationText_"+index+"\" onclick=showLocationList(\"locationText_"+index+"\")>"+"<?=$Lang['SLRS']['LessonExchangeDes']['ToBeChanged']?>"+"</a><br/>";
							detailsTable += "<div id=\"locationDiv_"+index+"\" style=\"display:none\">";
							detailsTable += list;
							detailsTable += "</div>";	
						detailsTable += "</div>";	
						detailsTable += "</td>";
					}
					detailsTable +="</table>";
					detailsTable +="<input type=\"hidden\" id=\"StartTime_"+index+"\" value=\""+result[0].StartTime+"\"/>";
					detailsTable +="<input type=\"hidden\" id=\"EndTime_"+index+"\" value=\""+result[0].EndTime+"\"/>";
					detailsTable +="<input type=\"hidden\" id=\"TimeSlotID_"+index+"\" value=\""+result[0].TimeSlotID+"\"/>";
					detailsTable +="<input type=\"hidden\" id=\"SubjectGroupID_"+index+"\" value=\""+result[0].SubjectGroupID+"\"/>";
					detailsTable +="<input type=\"hidden\" id=\"YearClassID_"+index+"\" value=\""+result[0].YearClassID+"\"/>";
					detailsTable +="<input type=\"hidden\" id=\"SubjectID_"+index+"\" value=\""+result[0].SubjectID+"\"/>";
					detailsTable +="<input type=\"hidden\" id=\"LocationID_"+index+"\" value=\""+locationID+"\"/>";
					detailsTable +="<input type=\"hidden\" id=\"Day_"+index+"\" value=\""+result[0].Day+"\"/>";
					detailsTable +="<input type=\"hidden\" id=\"TimeSlotName_"+index+"\" value=\""+result[0].TimeSlotName+"\"/>";
					detailsTable +="<input type=\"hidden\" id=\"SubjectGroupName_"+index+"\" value=\""+result[0].SubjectGroupName+"\"/>";
					detailsTable +="<input type=\"hidden\" id=\"LocationName_"+index+"\" value=\""+result[0].Name+"\"/>";
					detailsTable +="<input type=\"hidden\" id=\"DatePicker_"+index+"\" value=\""+result[0].Date+"\"/>";
					$("#"+detailsLessonID).append(detailsTable);
					
               	} catch (e) {
                   	//Block of code to handle errors
                   	if (debugMsg) console.log(e);
               	}
				//$("#divCenter_1").show();
				//$("#div_2").show();

				var previousID = "";
				var currentID = "";
				var previousTimeSlot = "";
				var currentTimeSlot = "";
				var currentSubjectGroup = "";
				var previousSubjectGroup = "";
				var previousDate = "";
				var currentDate = "";

				currentID = $("#teachingTeacher_"+index+" option:selected").val();
				currentTimeSlot = $("#timeSlot_"+index+" option:selected").val();
				currentSubjectGroup = $("#timeSlot_"+index+" option:selected").attr('rel');
				currentDate = $("#datePicker_"+index).val();
				/*
				if(index == 2){					
					previousID = $("#teachingTeacher_"+(index-1)+" option:selected").val();
					if (previousID == "") {
						previousID = $("#teachingTeacher_"+(index-1)).val();
					}
					previousTimeSlot = $("#timeSlot_"+(index-1)+" option:selected").val();
					if (previousTimeSlot == "") {
						previousTimeSlot =$("#timeSlot_"+(index-1)+"").val();
					}
				}else if(index > 2){
					previousID = $("#teachingTeacher_1 option:selected").val();
					if (previousID == "") {
						previousID = $("#teachingTeacher_1").val();
					}
					previousTimeSlot = $("#timeSlot_1 option:selected").val();
					if (previousTimeSlot == "") {
						previousTimeSlot = $("#timeSlot_1").val();
					}
				}
				*/
				if (index >= 2) {
					previousID = $("#teachingTeacher_1 option:selected").val();
					if (previousID == "" || previousID=="undefined") {
						previousID = $("#teachingTeacher_1").val();
					}
					previousTimeSlot = $("#timeSlot_1 option:selected").val();
					if (previousTimeSlot == "" || previousTimeSlot=="undefined") {
						previousTimeSlot = $("#timeSlot_1").val();
					}
					previousSubjectGroup = $("#timeSlot_1 option:selected").attr('rel');
					previousDate = $("#datePicker_1").val();
				}
				//if(index % 2 == 0 && !(previousID == currentID && previousSubjectGroupID == currentSubjectGroupID)){
				//alert(previousTimeSlot+" "+currentTimeSlot);
				if (debugMsg) console.log(index, index % 2 == 0, previousID, currentID, previousID==currentID, previousTimeSlot, currentTimeSlot, previousTimeSlot==currentTimeSlot, previousDate, currentDate, previousDate==currentDate, previousSubjectGroup, currentSubjectGroup, previousSubjectGroup == currentSubjectGroup);
				if (debugMsg) console.log((previousID == currentID) &&
						(previousTimeSlot == currentTimeSlot) &&
						(previousSubjectGroup == currentSubjectGroup) &&
						(previousDate == currentDate));
				if(
					index % 2 == 0 
					&& !(
							(previousID == currentID) &&
							(previousTimeSlot == currentTimeSlot) &&
							(previousSubjectGroup == currentSubjectGroup) &&
							(previousDate == currentDate)
						)
					){
					try {
						
						data = "maxRowIndex="+$("#endIndex").val()+"&date="+$("#datePicker_"+index).val()+"&teacherName="+$("#teachingTeacher_"+index+" option:selected").text();
						data += "&room="+$("#timeSlot_"+index+" option:selected").text();
						data += "&startTime="+result[0].StartTime+"&endTime="+result[0].EndTime+"&subjectGroupName="+result[0].SubjectGroupName+"&locationName="+result[0].Name;
						data += "&day="+result[0].Day+"&timeSlotName="+result[0].TimeSlotName+"&oriDate="+$("#datePicker_1").val();
						data += "&timeSlotID="+result[0].TimeSlotID+"&yearClassID="+result[0].YearClassID+"&subjectID="+result[0].SubjectID+"&subjectGroupID="+result[0].SubjectGroupID;
						data += "&teachingTeacher="+$("#teachingTeacher_"+index+" option:selected").val()+"&classRoom="+$("#subjectGroup_"+index+" option:selected").val();
						data += "&groupID="+$("#rowGroupID").val();

						if (debugMsg) console.log("%c getLessonDetailsAuto > aj", 'color: #f00', data);
						
						$.ajax({ type: "POST", url: '?task=management<?=$slrsConfig['taskSeparator']?>lesson_exchange<?=$slrsConfig['taskSeparator']?>ajax_getNewExchange',
							async: debugMsg,
							data: data,
				            success: function (data, textStatus, jqXHR) {
	
				            	if (debugMsg) console.log("%c getLessonDetailsAuto > rep", 'color: #f00', data);
					            
					            //alert(data);
					            $("#exchangeTable").append(data);
					            
				            },error: function (jqXHR, textStatus, errorThrown) {
				            }		            
						});
					} catch (e) {
	                   	//Block of code to handle errors
	                   	if (debugMsg) console.log(e);
	               	}  
				} else {
					$('input[type="submit"]').show();
				}
				
            }, error: function (jqXHR, textStatus, errorThrown) {
               
            }
        });
	}

	function goSubmit() {
		
		var startIndex = parseInt($("#startIndex").val());
		var endIndex = parseInt($("#endIndex").val());
		var rowGroupID = parseInt($("#rowGroupID").val());
		for(i=1; i<=endIndex; i++){			
			var datePickerVal = ($("#DatePicker_"+i).val() != undefined)?$("#DatePicker_"+i).val() : $("#datePicker_"+i).val();
			var datePicker = $("<input/>").attr("type", "hidden").attr("name", "datePicker_"+i).val(datePickerVal);			
			var teachingTeacherVal = ($("#TeachingTeacher_"+i).val() != undefined)?$("#TeachingTeacher_"+i).val() : $("#teachingTeacher_"+i).val();
			var teachingTeacher = $("<input/>").attr("type", "hidden").attr("name", "teachingTeacher_"+i).val(teachingTeacherVal);	
			//var locationVal = ($("#LocationID_"+i).val() != undefined)?$("#LocationID_"+i).val() : $("#locationID_"+i).val();
			//alert(i+ " " +locationVal);
			var location = $("<input/>").attr("type", "hidden").attr("name", "locationID_"+i).val($("#LocationID_"+i).val());
			var startTime = $("<input/>").attr("type", "hidden").attr("name", "startTime_"+i).val($("#StartTime_"+i).val());
			var endTime = $("<input/>").attr("type", "hidden").attr("name", "endTime_"+i).val($("#EndTime_"+i).val());
			var timeSlotID = $("<input>").attr("type", "hidden").attr("name", "timeSlotID_"+i).val($("#TimeSlotID_"+i).val());
			var subjectGroupID = $("<input>").attr("type", "hidden").attr("name", "subjectGroupID_"+i).val($("#SubjectGroupID_"+i).val());
			var yearClassID = $("<input>").attr("type", "hidden").attr("name", "yearClassID_"+i).val($("#YearClassID_"+i).val());
			var subjectID = $("<input>").attr("type", "hidden").attr("name", "subjectID_"+i).val($("#SubjectID_"+i).val());
			$('#form1').append($(datePicker)).append($(teachingTeacher)).append($(location)).append($(startTime)).append($(endTime)).append($(timeSlotID)).append($(subjectGroupID)).append($(yearClassID)).append($(subjectID));
			//var input = $("<input>").attr("type", "hidden").attr("name", "mydata").val("bla");
			//$('#form1').append($(input));
			//alert($("form#form1").serialize());
		}
		var inputStartIndex = $("<input/>").attr("type", "hidden").attr("name", "startIndex").val(startIndex);
		var inputEndIndex = $("<input/>").attr("type", "hidden").attr("name", "endIndex").val(endIndex);
		var rowGroupIndex = $("<input/>").attr("type", "hidden").attr("name", "rowGroupID").val(rowGroupID);	
		$('#form1').append($(inputStartIndex)).append($(inputEndIndex)).append($(rowGroupIndex));
		$('#form1').bind('submit', ajaxCheckingHandler);
	}

	function goBack() {
		window.location = '?task=management<?=$slrsConfig['taskSeparator']?>lesson_exchange<?=$slrsConfig['taskSeparator']?>list';
	}

	function disableHandler(e)
	{
		e.preventDefault();
	}
	
	function ajaxCheckingHandler(e)
	{
		e.preventDefault();
		$('#form1').unbind('submit', disableHandler);
		$('#form1').bind('submit', disableHandler);
		$('#form1').unbind('submit', ajaxCheckingHandler);
		$('form#form1').attr('action', 'index.php?task=management<?=$slrsConfig['taskSeparator']?>lesson_exchange<?=$slrsConfig['taskSeparator']?>ajax_update').submit();
		var data = "type=checklesson&" + $('#form1').serialize();
		$('.lessonMsg').each(function() {
			$(this).remove();
		});
		$('#div_1 .loadingbox').eq(0).show();
		$.ajax({
			type: "POST",
			url: '?task=management<?=$slrsConfig['taskSeparator']?>lesson_exchange<?=$slrsConfig['taskSeparator']?>ajax_getData',
			async: debugMsg,
			data: data,
			dataType: 'json',
            success: function (resp, textStatus, jqXHR) {
            	$('#div_1 .loadingbox').eq(0).hide();
            	console.log(resp);
            	if (debugMsg) console.log("%c ajaxCheckingHandler > rep", 'color: #f00', resp);
            	$.each(resp.data, function(div, rec) {
                	if (rec[2].hasLesson){
                    	$('#div_' + rec[2].i).append("<span class='lessonMsg' style='background-color:#f00; padding:5px; color:#fff; line-height:25px;'>" + rec[2].errorMsg + "</span>");
                	}
            	});
				if (resp.result)
				{
					$('#form1').unbind('submit', disableHandler);
					$('#form1').submit();
				}
				else
				{
					$('#form1').unbind('submit', disableHandler);
 	            	$('#form1').bind('submit', ajaxCheckingHandler);
				}
            },error: function (jqXHR, textStatus, errorThrown) {
            	$('#div_1 .loadingbox').eq(0).hide();
            	$('#form1').unbind('submit', disableHandler);
            	$('#form1').bind('submit', ajaxCheckingHandler);
            }
		});
	}
</script>
