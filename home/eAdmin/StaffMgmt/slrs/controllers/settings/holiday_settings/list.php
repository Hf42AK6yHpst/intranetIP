<?php
// ============================== Related Tables ==============================
// GENERAL_SETTING
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

# Page Title
$TAGS_OBJ[] = array($Lang['SLRS']['HolidaySettings']);
$indexVar['libslrs_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);

// ============================== Includes files/libraries ==============================

// ============================== Transactional data ============================== 
$lgeneralsettings = new libgeneralsettings();
$data = $lgeneralsettings->Get_General_Setting("SLRS");

$x = "";
$x .= $indexVar['libslrs_ui']->Get_Form_Sub_Title_v30($Lang['SLRS']['HolidaySettings']);
$x .= '<table class="form_table_v30">'."\r\n";	
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']["OfficialLeavelAO"].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
		$x .= $Lang['SLRS']['OfficialLeavelOptDes'][$data["OfficialLeavelAO"]] ;
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['OfficialLeavelWS'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
		$x .= $Lang['SLRS']['OfficialLeavelOptDes'][$data["OfficialLeavelWS"]];
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['OfficialLeavelSLP'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
		$x .= $Lang['SLRS']['OfficialLeavelOptDes'][$data["OfficialLeavelSLP"]];
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['OfficialLeavelPL'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
		$x .= $Lang['SLRS']['OfficialLeavelOptDes'][$data["OfficialLeavelPL"]];
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['OfficialLeavelSTL'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
		$x .= $Lang['SLRS']['OfficialLeavelOptDes'][$data["OfficialLeavelSTL"]];
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";	
$x .= '</table>';


$htmlAry['contentTbl'] = $x;
// ============================== Transactional data ==============================

// ============================== Define Button ============================== 
$htmlAry['editBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['Btn']['Edit'], "button", "goEdit()", 'editBtn');
// ============================== Define Button ==============================
?>