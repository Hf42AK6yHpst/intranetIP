<?php
// ============================== Related Tables ==============================
// GENERAL_SETTING
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

# Page Title
$TAGS_OBJ[] = array($Lang['SLRS']['HolidaySettings']);
$indexVar['libslrs_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);

// ============================== Includes files/libraries ==============================

// ============================== Transactional data ============================== 
$lgeneralsettings = new libgeneralsettings();
$data = $lgeneralsettings->Get_General_Setting("SLRS");

$x = "";
$x .= $indexVar['libslrs_ui']->Get_Form_Sub_Title_v30($Lang['SLRS']['HolidaySettings']);
$x .= '<table class="form_table_v30">'."\r\n";	
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']["OfficialLeavelAO"].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
		$x .= getSelectByAssoArray(cnvArrToSelect($Lang['SLRS']['OfficialLeavelOptDes'],null), $selectionTags='id="OfficialLeavelAO" name="OfficialLeavelAO"', $SelectedType=$data["OfficialLeavelAO"], $all=0, $noFirst=1);
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['OfficialLeavelWS'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
		$x .= getSelectByAssoArray(cnvArrToSelect($Lang['SLRS']['OfficialLeavelOptDes'],null), $selectionTags='id="OfficialLeavelWS" name="OfficialLeavelWS"', $SelectedType=$data["OfficialLeavelWS"], $all=0, $noFirst=1);
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['OfficialLeavelSLP'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
		$x .= getSelectByAssoArray(cnvArrToSelect($Lang['SLRS']['OfficialLeavelOptDes'],null), $selectionTags='id="OfficialLeavelSLP" name="OfficialLeavelSLP"', $SelectedType=$data["OfficialLeavelSLP"], $all=0, $noFirst=1);
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['OfficialLeavelPL'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
		$x .= getSelectByAssoArray(cnvArrToSelect($Lang['SLRS']['OfficialLeavelOptDes'],null), $selectionTags='id="OfficialLeavelPL" name="OfficialLeavelPL"', $SelectedType=$data["OfficialLeavelPL"], $all=0, $noFirst=1);
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['OfficialLeavelSTL'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
		$x .= getSelectByAssoArray(cnvArrToSelect($Lang['SLRS']['OfficialLeavelOptDes'],null), $selectionTags='id="OfficialLeavelSTL" name="OfficialLeavelSTL"', $SelectedType=$data["OfficialLeavelSTL"], $all=0, $noFirst=1);
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";	
$x .= '</table>';


$htmlAry['contentTbl'] = $x;
// ============================== Transactional data ==============================

// ============================== Define Button ============================== 
$htmlAry['submitBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn');
$htmlAry['backBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');
// ============================== Define Button ==============================

// ============================== Custom Function ==============================

function cnvArrToSelect($arrDes,$arrVal){
	$oAry = array();	
	if($arrVal == null){
		for($_i=0; $_i<sizeof($arrDes); $_i++){
			$oAry[$_i] = $arrDes[$_i];		
		}
	}
	else{
		for($_i=0; $_i<sizeof($arrDes); $_i++){
			$oAry[$arrVal[$_i]] = $arrDes[$_i];
		}
	}
	return $oAry;
}
// ============================== Custom Function ==============================

?>