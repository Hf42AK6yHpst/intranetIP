<?php 

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

$lgeneralsettings = new libgeneralsettings();

$saveSuccess = true;

$data = array();
$data['OfficialLeavelAO'] = IntegerSafe($_POST["OfficialLeavelAO"]);
$data['OfficialLeavelWS'] = IntegerSafe($_POST["OfficialLeavelWS"]);
$data['OfficialLeavelSLP'] = IntegerSafe($_POST["OfficialLeavelSLP"]);
$data['OfficialLeavelPL'] = IntegerSafe($_POST["OfficialLeavelPL"]);
$data['OfficialLeavelSTL'] = IntegerSafe($_POST["OfficialLeavelSTL"]);
$lgeneralsettings->Save_General_Setting("SLRS", $data);


$returnMsgKey = ($saveSuccess)? 'UpdateSuccess' : 'UpdateUnsuccess';
header('Location: ?task=settings'.$slrsConfig['taskSeparator'].'holiday_settings'.$slrsConfig['taskSeparator'].'list&returnMsgKey='.$returnMsgKey);

?>