<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');

# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$type = $_POST['type'];
$supplyID = $_POST['supplyID'];
$userID = $_POST['userID'];
$teacherID = $_POST['teacherID'];
$startDate = $_POST['startDate'];
$endDate = $_POST['endDate'];
$arrangedToUserID = $_POST["arrangedToUserID"];

$connection = new libgeneralsettings();
$arr = array();
$jsonObj = new JSON_obj();

if($type=="checkOverlap"){	
	if($supplyID==""){
		$sql = "SELECT Supply_UserID,TeacherID,DateStart,DateEnd
				FROM INTRANET_SLRS_SUPPLY_PERIOD WHERE Supply_UserID=".IntegerSafe($userID)." AND DateEnd>='".$startDate."' AND DateStart<='".$endDate."' ";
		//echo $sql;
		$a = $connection->returnResultSet($sql);
		$sql = "SELECT SupplyPeriodID,Supply_UserID,TeacherID,DateStart,DateEnd
				FROM INTRANET_SLRS_SUPPLY_PERIOD WHERE TeacherID=".IntegerSafe($teacherID)." AND DateEnd>='".$startDate."' AND DateStart<='".$endDate."' ";
		$b = $connection->returnResultSet($sql);
		if(sizeof($a)>0){
			$arr[] = array('result' => "1", 'msg' => $Lang['SLRS']['ExternalTeacherDes']["ErrorMessage"]);
		}
		else if(sizeof($b)>0){
			$arr[] = array('result' => "1", 'msg' => $Lang['SLRS']['ExternalTeacherDes']["ErrorTeacherMessage"]);
		}
		else{		
			$arr[] = array('result' => "0", 'msg' => "");		
		}
		echo $jsonObj->encode($arr);	
	}
	else{
		$sql = "SELECT SupplyPeriodID,Supply_UserID,TeacherID,DateStart,DateEnd
				FROM INTRANET_SLRS_SUPPLY_PERIOD WHERE Supply_UserID=".IntegerSafe($userID)." AND DateEnd>='".$startDate."' AND DateStart<='".$endDate."' AND SupplyPeriodID<>".$supplyID.";";
		//echo $sql;
		$a = $connection->returnResultSet($sql);
		$sql = "SELECT SupplyPeriodID,Supply_UserID,TeacherID,DateStart,DateEnd
				FROM INTRANET_SLRS_SUPPLY_PERIOD WHERE TeacherID=".IntegerSafe($teacherID)." AND DateEnd>='".$startDate."' AND DateStart<='".$endDate."' AND SupplyPeriodID<>".$supplyID.";";
		$b = $connection->returnResultSet($sql);
		if(sizeof($a)>0){
			$arr[] = array('result' => "1", 'msg' => $Lang['SLRS']['ExternalTeacherDes']["ErrorMessage"]);
		}
		else if(sizeof($b)>0){
			$arr[] = array('result' => "1", 'msg' => $Lang['SLRS']['ExternalTeacherDes']["ErrorTeacherMessage"]);
		}
		else{
			$arr[] = array('result' => "0", 'msg' => "");
		}
		echo $jsonObj->encode($arr);
	}
}














?>