<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST, INTRANET_SLRS_LESSON_ARRANGEMENT
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

# Page Title
$TAGS_OBJ[] = array($Lang['SLRS']['ExternalTeacher']);
$indexVar['libslrs_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
//echo $indexVar['libslrs_ui']->Include_Thickbox_JS_CSS();

// ============================== Includes files/libraries ==============================
// ============================= Transactional data ==============================
$connection = new libgeneralsettings();

$supplyPeriodID = $_POST["supplyIdAry"];

$sql = "SELECT SupplyPeriodID,Supply_UserID,TeacherID,DateStart,DateEnd,Remarks FROM INTRANET_SLRS_SUPPLY_PERIOD WHERE SupplyPeriodID=".$supplyPeriodID[0].";";
$supplyDetails = $connection->returnResultSet($sql);


$x .= "<table class=\"form_table_v30\">"."\r\n";
	$x .= "<tr>"."\r\n";
		$x .= "<td>".$Lang['SLRS']['ExternalTeacherDes']["Teacher"].$indexVar['libslrs_ui']->RequiredSymbol()."</td><td>:</td>"."\r\n";
		$x .= "<td>".getSelectByAssoArray(cnvUserArrToSelect($indexVar['libslrs']->getSupplyTeacher()), $selectionTags='id="supplyTeacher" name="supplyTeacher" class="requiredField"', $SelectedType=$supplyDetails[0]["Supply_UserID"], $all=0, $noFirst=0);
		$x .= $indexVar['libslrs_ui']->Get_Form_Warning_Msg('supplyTeacherEmptyWarnDiv', $Lang['SLRS']['ExternalTeacherDes']["EmptyErrorMessage"], $Class='warnMsgDiv')."\n";"</td>"."\r\n";
	$x .= "</tr>"."\r\n";
	$x .= "<tr>"."\r\n";
		$x .= "<td>".$Lang['SLRS']['ExternalTeacherDes']["StartDate"]."</td><td>:</td>"."\r\n";
		$x .= "<td>".$indexVar['libslrs_ui']->GET_DATE_PICKER('startDate', $supplyDetails[0]["DateStart"],  $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum");
		$x .= $indexVar['libslrs_ui']->Get_Form_Warning_Msg('dateStartWarningDiv', '', $Class='warnMsgDiv')."\n"."</td>"."\r\n";
	$x .= "</tr>"."\r\n";
	$x .= "<tr>"."\r\n";
		$x .= "<td>".$Lang['SLRS']['ExternalTeacherDes']["EndDate"]."</td><td>:</td>"."\r\n";
		$x .= "<td>".$indexVar['libslrs_ui']->GET_DATE_PICKER('endDate', $supplyDetails[0]["DateEnd"],  $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum")."</td>"."\r\n";
	$x .= "</tr>"."\r\n";
	$x .= "<tr>"."\r\n";
		$x .= "<td>".$Lang['SLRS']['ExternalTeacherDes']["ToTeacher"].$indexVar['libslrs_ui']->RequiredSymbol()."</td><td>:</td>"."\r\n";
		$x .="<td>".getSelectByAssoArray(cnvUserArrToSelect($indexVar['libslrs']->getTeachingTeacher()), $selectionTags='id="teacher" name="teacher" class="requiredField"', $SelectedType=$supplyDetails[0]["TeacherID"], $all=0, $noFirst=0);
		$x .= $indexVar['libslrs_ui']->Get_Form_Warning_Msg('teacherEmptyWarnDiv', $Lang['SLRS']['ExternalTeacherDes']["EmptyErrorMessage"], $Class='warnMsgDiv')."\n";"</td>"."\r\n";
	$x .= "</tr>"."\r\n";
	$x .= "<tr>"."\r\n";
		$x .= "<td>".$Lang['SLRS']['ExternalTeacherDes']["Remark"]."</td><td>:</td>"."\r\n";
		$x .= "<td>".$indexVar['libslrs_ui']->GET_TEXTAREA('remark', $taContents=$supplyDetails[0]["Remarks"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td>"."\r\n";
	$x .= "</tr>"."\r\n";
$x .= "</table>";

$htmlAry['contentTbl'] = $x;
$htmlAry['supplyID'] = $supplyPeriodID[0];

// ============================= Transactional data ==============================

// ============================== Define Button ==============================
$htmlAry['submitBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn');
$htmlAry['backBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');
// ============================== Define Button ==============================
function cnvUserArrToSelect($arr){
	$oAry = array();
	for($_i=0; $_i<sizeof($arr); $_i++){
		$oAry[$arr[$_i]["UserID"]] = Get_Lang_Selection($arr[$_i]["ChineseName"],$arr[$_i]["EnglishName"]);
	}
	return $oAry;;
}

?>