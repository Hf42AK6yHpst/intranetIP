<?php
/*
############################
##	Date:	2017-02-03	Omas
##			- fix dbtable column wrong problem 
############################
 */
// ============================== Related Tables ==============================
// GENERAL_SETTING
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

# Page Title
$TAGS_OBJ[] = array($Lang['SLRS']['ExternalTeacher']);
$indexVar['libslrs_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libslrs_ui']->Include_JS_CSS();
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$order = ($order == '') ? 1 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 0 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 50 : $numPerPage;
 

$name_field = $indexVar['libslrs']->getNameFieldByLang("b.");

$toTeacherName_field = $indexVar['libslrs']->getNameFieldByLang("c.");
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("EnglishName","StartDate","EndDate","ToTeacher","remarks", "ModifiedDate");
$li->sql = "SELECT ".$name_field." as Teacher, /*b.EnglishName,*/ DATE_FORMAT(DateStart,'%Y-%m-%d') as StartDate,
	 DATE_FORMAT(DateEnd,'%Y-%m-%d') as EndDate,".
	 $toTeacherName_field." as ToTeacher, If(remarks is null or remarks='', '-', remarks) as remarks, DateTimeModified as ModifiedDate,
	 CONCAT('<input type=checkbox name=supplyIdAry[] id=supplyIdAry[] value=\"',a.SupplyPeriodID,'\">') as checkbox,
	 b.EnglishName
	 From (
		SELECT SupplyPeriodID,Supply_UserID,TeacherID,DateStart,DateEnd,remarks,DateTimeModified FROM
	 	INTRANET_SLRS_SUPPLY_PERIOD
	 ) as a
	 LEFT JOIN(
		 SELECT UserID,EnglishName,ChineseName,TitleChinese,TitleEnglish FROM
		 ".$slrsConfig['INTRANET_USER']."
	 ) as b ON a.Supply_UserID=b.UserID
	 LEFT JOIN(
		 SELECT UserID,EnglishName,ChineseName,TitleChinese,TitleEnglish FROM
		 ".$slrsConfig['INTRANET_USER']."
	 ) as c ON a.TeacherID=c.UserID
		
	";
// 	echo $li->sql;

$li->no_col = sizeof($li->field_array) + 2;
$li->fieldorder2 = ", ModifiedDate";
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='16%' >".$li->column($pos++, $Lang['SLRS']['ExternalTeacherDes']["Teacher"])."</th>\n";
$li->column_list .= "<th width='16%' >".$li->column($pos++, $Lang['SLRS']['ExternalTeacherDes']["StartDate"])."</th>\n";
$li->column_list .= "<th width='16%' >".$li->column($pos++, $Lang['SLRS']['ExternalTeacherDes']["EndDate"])."</th>\n";
$li->column_list .= "<th width='16%' >".$li->column($pos++, $Lang['SLRS']['ExternalTeacherDes']["ToTeacher"])."</th>\n";
$li->column_list .= "<th width='18%' >".$li->column($pos++, $Lang['SLRS']['ExternalTeacherDes']["Remark"])."</th>\n";
$li->column_list .= "<th width='16%' >".$li->column($pos++, $Lang['SLRS']['ExternalTeacherDes']["ModifiedDate"])."</th>\n";
$li->column_list .= "<th width='1'>".$li->check("supplyIdAry[]")."</th>\n";
$htmlAry['dataTable'] = $li->display();

### hidden fields for db table
$x = '';
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('pageNo', 'pageNo', $li->pageNo);
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('order', 'order', $li->order);
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('field', 'field', $li->field);
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('page_size_change', 'page_size_change', '');
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('numPerPage', 'numPerPage', $li->page_size);
$htmlAry['hiddenField'] = $x;
// ============================== Transactional data ==============================
// ============================== Define Button ==============================
$btnAry = array();
$btnAry[] = array('new', 'javascript: goNew();');
$htmlAry['contentTool'] = $indexVar['libslrs_ui']->Get_Content_Tool_By_Array_v30($btnAry);
$btnAry = array();
$btnAry[] = array('edit', 'javascript: goEditSelectionOrder();');
$btnAry[] = array('delete', 'javascript: goDelete();');
$htmlAry['dbTableActionBtn'] = $indexVar['libslrs_ui']->Get_DBTable_Action_Button_IP25($btnAry);

// ============================== Define Button ==============================



?>