<?php
// ============================== Related Tables ==============================
// GENERAL_SETTING
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

if (count($_POST) > 0) {
	
	if (isset($_POST["LocationRecIdAry"]["0"]) || isset($_POST["rec_id"]) && $_POST["rec_id"] > 0) {
		$rec_id = (isset($_POST["LocationRecIdAry"]["0"])) ? $_POST["LocationRecIdAry"]["0"] : $_POST["rec_id"];
		$is_update = true;
	} else {
		$is_update = false;
	}
	if (isset($_POST["token"])) {
		$err = array();
		if (empty($_POST["Location"])) $err["Location"] = $Lang["SLRS"]["ClosedLocationErr"]["Location"];
		if (empty($_POST["StartDate"])) $err["StartDate"] = $Lang["SLRS"]["ClosedLocationErr"]["StartDate"];
		if (empty($_POST["EndDate"])) $err["EndDate"] = $Lang["SLRS"]["ClosedLocationErr"]["EndDate"];
		if (empty($_POST["Reasons"])) $err["Reasons"] = $Lang["SLRS"]["ClosedLocationErr"]["Reasons"];
		
		if (count($err) == 0) {
			$param = array(
					"LocationID" => $_POST["Location"],
					"StartDate" => $indexVar['libslrs']->reformat_date($_POST["StartDate"]),
					"EndDate" => $indexVar['libslrs']->reformat_date($_POST["EndDate"]),
					"Reasons" => $_POST["Reasons"]
			);
			
			if ($is_update) {
				$strSQL = "UPDATE INTRANET_SLRS_NOT_AVAILABLE_LOCATION set ";
				foreach ($param as $kk => $vv) {
					$strSQL .= " " . $kk . "='" . $vv . "', ";
				}
				$strSQL .= "ModifiedBy_UserID='" . $_SESSION["UserID"] . "', DateTimeModified=NOW() WHERE NA_Location_ID='" . $rec_id . "'";
				$indexVar['libslrs']->db_db_query($strSQL);
			} else {
				$strSQL = "INSERT INTO INTRANET_SLRS_NOT_AVAILABLE_LOCATION (" . implode(", ", array_keys($param)) . ", InputBy_UserID, DateTimeInput, ModifiedBy_UserID, DateTimeModified)";
				$strSQL .= " VALUES ";
				$strSQL .= " ('" . implode("', '", array_values($param)) . "', '" . $_SESSION["UserID"] . "', NOW(), '" . $_SESSION["UserID"] . "', NOW())";
				$indexVar['libslrs']->db_db_query($strSQL);
			}
			$saveSuccess = true;
			$returnMsgKey = ($saveSuccess)? 'UpdateSuccess' : 'UpdateUnsuccess';
			header('Location: ?task=settings'.$slrsConfig['taskSeparator'].'closed_location'.$slrsConfig['taskSeparator'].'list&returnMsgKey=' . $returnMsgKey);
			exit;
		}
		
	} else if ($is_update) {
		$data = $indexVar['libslrs']->getNotAvailableLocationByID($rec_id);
		$_POST["Location"] = $data[0]["LocationID"];
		$_POST["StartDate"] = $data[0]["StartDate"];
		$_POST["EndDate"] = $data[0]["EndDate"];
		$_POST["Reasons"] = $data[0]["Reasons"];
	}
}
# Page Title
$TAGS_OBJ[] = array($Lang['SLRS']['ClosedLocation']);
$indexVar['libslrs_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);

// ============================== Includes files/libraries ==============================

// ============================== Transactional data ============================== 
$lgeneralsettings = new libgeneralsettings();
$data = $lgeneralsettings->Get_General_Setting("SLRS");

$allLocations = $indexVar['libslrs']->getAllLocationSelection();


// ============================== Transactional data ==============================
$x = "";
$x .= "<table class=\"form_table_v30\">"."\r\n";
$x .= "<tr>"."\r\n";
$x .= "<td class='field_title'><span class='tabletextrequire'>*</span> ".$Lang['SLRS']['ClosedLocationTH']["th_Location"].":</td>"."\r\n";
$x .= "<td><select class='js-basic-single' name='Location' id='slrs-location style='width:100%; min-width:300px;'>";
$x .= "<option></option>";
if (count($allLocations) > 0) {
	foreach ($allLocations as $kk => $vv) {
		$x .= "<option value='" . $kk . "'";
		if (isset($_POST["Location"]) && $_POST["Location"] == $kk) $x .= " selected";
		$x .= ">" . $vv . "</option>";
	}
}
$x .= "</select>";
if (isset($err["Location"])) {
	$x .= " <span class='error' style='color:#f00;'>" . $err["Location"] . "</span>";
}
$x .= "</td>"."\r\n";
$x .= "</tr>"."\r\n";
$x .= "<tr>"."\r\n";
$x .= "<td class='field_title'><span class='tabletextrequire'>*</span> ".$Lang['SLRS']['ClosedLocationTH']["th_StartDate"].":</td>"."\r\n";
$x .= "<td><input type='text' class='slrs-datepicker' data-position='right top' name='StartDate' id='StartDate' value='' />";
if (isset($err["StartDate"])) {
	$x .= " <span class='error' style='color:#f00;'>" . $err["StartDate"] . "</span>";
}
$x .= "</td>"."\r\n";
$x .= "</tr>"."\r\n";
$x .= "<tr>"."\r\n";
$x .= "<td class='field_title'><span class='tabletextrequire'>*</span> ".$Lang['SLRS']['ClosedLocationTH']["th_EndDate"].":</td>"."\r\n";
$x .= "<td><input type='text' class='slrs-datepicker' data-position='right top' name='EndDate' id='EndDate' value='' />";
if (isset($err["EndDate"])) {
	$x .= " <span class='error' style='color:#f00;'>" . $err["EndDate"] . "</span>";
}
$x .= "</td>"."\r\n";
$x .= "</tr>"."\r\n";
$x .= "<tr valign='top'>"."\r\n";
$x .= "<td class='field_title' class='slrs-textarea'><span class='tabletextrequire'>*</span> ".$Lang['SLRS']['ClosedLocationTH']["th_Reasons"].":</td>\r\n";
$x .= "<td><textarea id='Reasons' class='slrs-textarea' name='Reasons' style='width:90%; height:70px; resize: vertical;'>" . (isset($_POST["Reasons"]) ? $_POST["Reasons"] : '') . "</textarea>";
if (isset($err["Reasons"])) {
	$x .= "<br><span class='error' style='color:#f00;'>" . $err["Reasons"] . "</span>";
}
$x .= "</td>"."\r\n";
$x .= "</tr>"."\r\n";
$x .= "</table>"."\r\n";

$htmlAry['contentTbl'] = $x;
$htmlAry['loadingTxt'] = $linterface->Get_Ajax_Loading_Image();

// ============================== Define Button ==============================
$htmlAry['submitBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn');
$htmlAry['backBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');
// ============================== Define Button ==============================
