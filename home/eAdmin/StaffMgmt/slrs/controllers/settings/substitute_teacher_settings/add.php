<?php
/*
#################
##	Date 2017-02-03 Omas
##		Modified yui data array excluding supply teacher
##################
 */
// ============================== Related Tables ==============================
// GENERAL_SETTING
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

# Page Title
$TAGS_OBJ[] = array($Lang['SLRS']['SubstituteTeacherSettings']);
$indexVar['libslrs_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);

// ============================== Includes files/libraries ==============================
$htmlAry['selectSubstituteTeachers'] = $Lang['SLRS']['SelectSubstituteTeachers'];
$htmlAry['selectedSubstituteTeachers'] = $Lang['SLRS']['SelectedSubstituteTeachers'];
$htmlAry['Or'] = $Lang['SLRS']['Or'];
$htmlAry['SearchByLoginID']=$Lang['SLRS']['SearchByLoginID'];

// generate the login account for auto-complete
$connection = new libgeneralsettings();
$name_field = $indexVar['libslrs']->getNameFieldByLang("USR.");
$typeList = "1";		# 1-teacher only

if(sizeof($stdInGroup)>0)
	$conds = " AND USR.UserID NOT IN (".implode(',', $stdInGroup).")";

$sql1 = "SELECT USR.UserID, $name_field as name, ycu.ClassNumber, UserLogin FROM ".$slrsConfig['INTRANET_USER']." USR LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) WHERE USR.RecordType IN ($typeList)  AND Teaching != 'S' $conds GROUP BY USR.UserID ORDER BY UserLogin";
$result1 = $connection->returnArray($sql1,5);

for ($j = 0; $j < sizeof($result1); $j++)
{
	list($this_userid, $this_stu_name, $this_class_number, $this_userlogin) = $result1[$j];
	$data_ary[] = array($this_userid, $this_classname, $this_class_number, $this_stu_name, $this_userlogin);
}

if(!empty($data_ary))
{
	#define yui array (Search by input format )
	for($i=0;$i<sizeof($data_ary);$i++)
	{
		list($this_userid, $this_classname, $this_class_number, $this_stu_name, $this_userlogin) = $data_ary[$i];

		$temp_str = $this_classname . $this_class_number. " ". $this_stu_name;
		$temp_str2 = $this_stu_name;

		$liArr .= "[\"". addslashes(intranet_undo_htmlspecialchars($temp_str)) ."\", \"". addslashes(intranet_undo_htmlspecialchars($temp_str2)) ."\", \"". $this_userid ."\"]";
		($i == (sizeof($result)-1)) ? $liArr .= "" : $liArr .= ",\n";
	}

	foreach ($data_ary as $key => $row)
	{
		$field1[$key] = $row[0];	//user id
		$field2[$key] = $row[1];	//class name
		$field3[$key] = $row[2];	//class number
		$field4[$key] = $row[3];	//stu name
		$field5[$key] = $row[4];	//login id
	}
	array_multisort($field5, SORT_ASC, $data_ary);

	#define yui array (Search by login id )
	for($i=0;$i<sizeof($data_ary);$i++)
	{
		list($this_userid, $this_classname, $this_class_number, $this_stu_name, $this_userlogin) = $data_ary[$i];
		$temp_str2 = $this_stu_name;

		$liArr2 .= "[\"". $this_userlogin ."\", \"". addslashes(intranet_undo_htmlspecialchars($temp_str2)) ."\", \"". $this_userid ."\"]";
		($i == (sizeof($result)-1)) ? $liArr2 .= "" : $liArr2 .= ",\n";
	}
	### end for searching
}
$htmlAry['liArr2'] = $liArr2;


// ============================== Define Button/Selection ==============================
$htmlAry['selectedTeachers'] = $indexVar['libslrs_ui']->GET_SELECTION_BOX($array_student, "name='teacher[]' id='teacher[]' class='select_studentlist' size='15' multiple='multiple'", "");
$htmlAry['deleteBtn'] = $indexVar['libslrs_ui']->GET_BTN($Lang['Btn']['Delete'], "button", "goDelete()", 'deleteBtn');
$htmlAry['selectBtn'] = $indexVar['libslrs_ui']->GET_BTN($Lang['Btn']['Select'], "button", "goSelect()", 'selectBtn');
$htmlAry['submitBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['Btn']['Submit'], "submit", "", 'submitBtn');
$htmlAry['backBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');
// ============================== Define Button ==============================
?>