<?php
// ============================== Related Tables ==============================
// GENERAL_SETTING
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

# Page Title
$TAGS_OBJ[] = array($Lang['SLRS']['BasicSettings']);
$indexVar['libslrs_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);

// ============================== Includes files/libraries ==============================

// ============================== Transactional data ============================== 
$lgeneralsettings = new libgeneralsettings();
$data = $lgeneralsettings->Get_General_Setting("SLRS");

$x = "";
$x .= $indexVar['libslrs_ui']->Get_Form_Sub_Title_v30($Lang['SLRS']['BasicSettings']);
$x .= '<table class="form_table_v30">'."\r\n";	
	/*$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['CycleWeekMaximunLessons'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
		$x .= $Lang['SLRS']['CycleWeekMaximunLessonsDes'][$data["CycleWeekMaximunLessons"]] ;
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";*/
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['DailyMaximunLessons'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
		$x .= $Lang['SLRS']['DailyMaximunLessonsDes'][$data["DailyMaximunLessons"]];
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['DailyMaximunSubstitution'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
		$x .= $Lang['SLRS']['DailyMaximunSubstitutionDes'][$data["DailyMaximunSubstitution"]];
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
// 	$x .= '<tr>'."\r\n";
// 		$x .= '<td class="field_title">'.$Lang['SLRS']['SpecialClassroomLocation'].'</td>'."\r\n";
// 		$x .= '<td>'."\r\n";
// 		$x .= $Lang['SLRS']['SpecialClassroomLocationDes'][$data["SpecialClassroomLocation"]];
// 		$x .= '</td>'."\r\n";
// 	$x .= '</tr>'."\r\n";
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['SubstitutionAfterSickLeave'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
		$x .= $Lang['SLRS']['SubstitutionAfterSickLeaveDes'][$data["SubstitutionAfterSickLeave"]];
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	/*$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['FirstLessonSubstituion'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";		
		$x .= $Lang['SLRS']['FirstLessonSubstituionDes'][$data["FirstLessonSubstituion"]];
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";*/
// 	if(!$indexVar['libslrs']->isEJ()){
// 		$x .= '<tr>'."\r\n";
// 			$x .= '<td class="field_title">'.$Lang['SLRS']['FormSixTeacherSubstition'].'</td>'."\r\n";
// 			$x .= '<td>'."\r\n";
// 			$x .= $Lang['SLRS']['FormSixTeacherSubstitionDes'][$data["FormSixTeacherSubstition"]];
// 			$x .= '</td>'."\r\n";
// 		$x .= '</tr>'."\r\n";
// 	}
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['VoluntarySubstitute'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
		$x .= $Lang['SLRS']['VoluntarySubstituteDes'][$data["VoluntarySubstitute"]];
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";

	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['NoSubstitutionCalBalance'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
		$x .= (isset($data["NoSubstitutionCalBalance"])) ? $Lang['SLRS']['NoSubstitutionCalBalanceDes'][$data["NoSubstitutionCalBalance"]] : $Lang['SLRS']['NoSubstitutionCalBalanceDes'][0];
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	
$x .= '</table>';
$x .= $indexVar['libslrs_ui']->Get_Form_Sub_Title_v30($Lang['SLRS']['Display']);
$x .= '<table class="form_table_v30">'."\r\n";
	/*$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['SubjectDisplay'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
		$x .= $Lang['SLRS']['SubjectDisplayDes'][$data["SubjectDisplay"]];
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";*/
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['LocationNameDisplay'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
		$x .= $Lang['SLRS']['LocationNameDisplayDes'][$data["LocationNameDisplay"]];
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	/*$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['SubjectGroupDisplay'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
		$x .= $Lang['SLRS']['SubjectGroupDisplayDes'][$data["SubjectGroupDisplay"]];
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";*/
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['SubstitutionBalanceDisplay'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
		$x .= $Lang['SLRS']['SubstitutionBalanceDisplayDes'][$data["SubstitutionBalanceDisplay"]];
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
$x .= '</table>';
if ($sys_custom['SLRS']["defaultToPresetLocation"]) {
	$x .= $indexVar['libslrs_ui']->Get_Form_Sub_Title_v30($Lang['SLRS']['PresetSubstitutionLocation']);
	$x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tr>'."\r\n";
	$x .= '<td class="field_title">'.$Lang['SLRS']['PresetSubstitutionLocationTH']["SubstitutionLimitPerDay"].'</td>'."\r\n";
	$x .= '<td>'."\r\n";
	$x .= ($data["PSSubstitutionLimitPerDay"] !== '') ? $Lang['SLRS']['DailyMaximunSubstitutionDes'][$data["PSSubstitutionLimitPerDay"]] : $Lang['SLRS']['DailyMaximunSubstitutionDes'][2];
	$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	$x .= '<tr>'."\r\n";
	$x .= '<td class="field_title">'.$Lang['SLRS']['PresetSubstitutionLocationTH']["PresetLocation"].'</td>'."\r\n";
	$x .= '<td>'."\r\n";
	$AllLocationSelection = str_replace(" (" . $Lang['SLRS']['PresetSubstitutionLocationTH']["PresetLocation"] . ")", "", $indexVar["libslrs"]->getAllLocationSelection());
	$x .= (isset($data["PresetLocation"])) ? $AllLocationSelection[$data["PresetLocation"]] : "-";
	$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	$x .= '</table>';
}

$htmlAry['contentTbl'] = $x;
// ============================== Transactional data ==============================

// ============================== Define Button ============================== 
$htmlAry['editBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['Btn']['Edit'], "button", "goEdit()", 'editBtn');
// ============================== Define Button ==============================
?>