<?php 

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."includes/slrs/libslrs.php");

$connection = new libgeneralsettings();
$indexVar['libslrs'] = new libslrs();

$userID = $_POST["leaveTeacher"];
$leaveID = $_POST["leaveID"];
$startIndex = $_POST["startIndex"];
$endIndex = $_POST["endIndex"];

function TextSafe($val, $rev = false) {
	$replaceArr = array(
		'"' => "&#34;",
		"'" => "&#39;",
	);
	return str_replace(array_keys($replaceArr), array_values($replaceArr), $val);
}

$general_settings_SLRS = $connection->Get_General_Setting("SLRS");

$j = 0;
for($i=$startIndex; $i<=$endIndex;$i++){
	if (isset($_POST["timeSlotID_".$i]) && isset($_POST["subjectGroupID_".$i]))
	{
		$postdata[$i]["timeSlotID"] = $_POST["timeSlotID_".$i];
		$postdata[$i]["arrangedTo_userID"] = $_POST["teacher_".$i];
		$postdata[$i]["arrangedTo_locationID"] = $_POST["locationID_".$i];
		$postdata[$i]["timeStart"] = $_POST["startTime_".$i];
		$postdata[$i]["timeEnd"] = $_POST["endTime_".$i];
		$postdata[$i]["subjectGroupID"] = $_POST["subjectGroupID_".$i];
		$postdata[$i]["subjectID"] = $_POST["subjectID_".$i];
		$postdata[$i]["locationID"] = $_POST["originalLocationID_".$i];
		$postdata[$i]["lessonArrangementID"] = $_POST["lessonArrangementID_".$i];
		$postdata[$i]["subTeacher"] = $_POST["subTeacher_".$i];
		$postdata[$i]["isVolunteer"] = $_POST["isVolunteer_".$i];
		$postdata[$i]["isVolunteer"] = ( $postdata[$j]["isVolunteer"] == "1" ) ? "1":"0";
		$postdata[$i]["leID"] = $_POST["leID_".$i];
		$postdata[$i]["LessonRemarks"] = TextSafe($_POST["LessonRemarks_".$i]);
		$postdata[$i]["ignoreThisRec"] = $_POST["ignoreThisRec_".$i];
	}
}

if (count($postdata) > 0) 
{
	foreach ($postdata as $i => $_data)
	{
	// for($i=$startIndex; $i<=$endIndex;$i++){
		$timeSlotID = $_POST["timeSlotID_".$i];
		$arrangedTo_userID = $_POST["teacher_".$i];
		$arrangedTo_locationID = $_POST["locationID_".$i];
		$timeStart = $_POST["startTime_".$i];
		$timeEnd = $_POST["endTime_".$i];
		$subjectGroupID = $_POST["subjectGroupID_".$i];
		$subjectID = $_POST["subjectID_".$i];
		$locationID = $_POST["originalLocationID_".$i];
		$lessonArrangementID = $_POST["lessonArrangementID_".$i];
		$subTeacher = $_POST["subTeacher_".$i];
		$isVolunteer = $_POST["isVolunteer_".$i];
		$isVolunteer=($isVolunteer=="1")?"1":"0";
		
		$leID = $_POST["leID_".$i];
		
		$LessonRemarks = TextSafe($_POST["LessonRemarks_".$i]);
		
		$ignoreThisRec = $_POST["ignoreThisRec_".$i];
		$ignoreThisRec = ($ignoreThisRec=="1")?"1":"0";

		if ($ignoreThisRec === "1" || empty($arrangedTo_userID)) {
			$sql_arrangedTo_userID = -999;
		} else {
			$sql_arrangedTo_userID = IntegerSafe($arrangedTo_userID);
		}
		
		$dt=strtotime($timeStart);
		$date = date("Y-m-d",$dt);
			
		$a = $indexVar['libslrs']->getAcademicYearTerm($date);
		$academicID=$a[0]["AcademicYearID"];	
		
		//get YearClassID
		$sql = "
				SELECT yc.YearClassID
				FROM(
					SELECT SubjectGroupID,UserID FROM SUBJECT_TERM_CLASS_USER WHERE SubjectGroupID=".IntegerSafe($subjectGroupID)." 
				) as stcu
				INNER JOIN(
					SELECT UserID,YearClassID FROM YEAR_CLASS_USER		
				) as ycu ON stcu.UserID=ycu.UserID
				INNER JOIN(
					SELECT YearClassID FROM YEAR_CLASS_TEACHER	
				) as yct ON ycu.YearClassID=yct.YearClassID
				INNER JOIN(
					SELECT YearClassID,AcademicYearID FROM YEAR_CLASS WHERE AcademicYearID IN (".$academicID.") 
				) as yc ON yct.YearClassID=yc.YearClassID
				GROUP BY yc.YearClassID
				";
		$a = $connection->returnResultSet($sql);
		$yearClassID=$a[0]["YearClassID"];
		
		$sql = "SELECT LeaveID, ReasonCode,DateLeave FROM INTRANET_SLRS_LEAVE WHERE LeaveID='".IntegerSafe($leaveID)."';";
		$isl = $connection->returnResultSet($sql);
		$reasonCode = $isl[0]["ReasonCode"];
		$DateLeave = $isl[0]["DateLeave"];
		
		$sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='SLRS' AND SettingName='OfficialLeavel".$reasonCode."';";
		$isl = $connection->returnResultSet($sql);
		
		// $sql = "SELECT UserID FROM INTRANET_SLRS_LESSON_ARRANGEMENT WHERE UserID='".IntegerSafe($userID)."' AND LeaveID='".IntegerSafe($leaveID)."' AND TimeSlotID='".IntegerSafe($timeSlotID) . "'";
		$sql = "SELECT UserID, LessonArrangementID FROM INTRANET_SLRS_LESSON_ARRANGEMENT WHERE UserID='".IntegerSafe($userID)."' AND LeaveID='".IntegerSafe($leaveID)."' AND TimeSlotID='".IntegerSafe($timeSlotID) . "' AND SubjectGroupID='".IntegerSafe($subjectGroupID) . "'";
		$checkExistence = $connection->returnResultSet($sql);
		if(sizeof($checkExistence) > 0)
		{
			
			$_balInfo = array(
					"LessonArrangementID" => $checkExistence[0]["LessonArrangementID"],
					"LeaveID" => IntegerSafe($leaveID),
					"DateLeave" => $DateLeave
			);
			
			$sql = "SELECT * FROM INTRANET_SLRS_LESSON_ARRANGEMENT WHERE LessonArrangementID='".IntegerSafe($lessonArrangementID)."';";
			$a = $connection->returnResultSet($sql);
			
			if($a[0]["ArrangedTo_UserID"] <> $sql_arrangedTo_userID)
			{
				if ($a[0]["ArrangedTo_UserID"] > 0)
				{
					// the original Substition Teacher
					$indexVar['libslrs']->updateSLRSBalance(IntegerSafe($a[0]["ArrangedTo_UserID"]), $academicID, "REDUCE", $DateLeave . " __SubstitutionArrangementUpdate__" . "__SubstitutionArrangementSubTC__", IntegerSafe($_SESSION['UserID']), '', $_balInfo);
					$sql = "UPDATE INTRANET_SLRS_TEACHER SET Balance=Balance-1,DateTimeModified=NOW(),ModifiedBy_UserID='".IntegerSafe($_SESSION['UserID'])."' WHERE UserID=".IntegerSafe($a[0]["ArrangedTo_UserID"]).";";
					$x = $connection->db_db_query($sql);
				}
				## the latest Substition Teacher
				if ($arrangedTo_userID > 0)
				{
					// when 'no substitution' change to another teacher
					if ($a[0]["ArrangedTo_UserID"] == "-999" && $isl[0]["SettingValue"]=="0")
					{
						if (isset($general_settings_SLRS['NoSubstitutionCalBalance']) && $general_settings_SLRS['NoSubstitutionCalBalance'] === '1')
						{
							## No substitution (settings: no need to calculate balance value)
							## update Leave Teacher
							$indexVar['libslrs']->updateSLRSBalance(IntegerSafe($userID), $academicID, "REDUCE", "*" . $DateLeave . " __SubstitutionArrangementUpdate__", IntegerSafe($_SESSION['UserID']), '', $_balInfo);
							$sql = "UPDATE INTRANET_SLRS_TEACHER SET Balance=Balance-1,DateTimeModified=NOW(),ModifiedBy_UserID='".IntegerSafe($_SESSION['UserID'])."' WHERE UserID=".$userID.";";
							$x = $connection->db_db_query($sql);
						}
					}
					
					## update Substition Teacher
					$indexVar['libslrs']->updateSLRSBalance(IntegerSafe($arrangedTo_userID), $academicID, "ADD", $DateLeave . " __SubstitutionArrangementUpdate__" . "__SubstitutionArrangementSubTC__", IntegerSafe($_SESSION['UserID']), '', $_balInfo);
					$sql = "UPDATE INTRANET_SLRS_TEACHER SET Balance=Balance+1,DateTimeModified=NOW(),ModifiedBy_UserID='".IntegerSafe($_SESSION['UserID'])."' WHERE UserID=".IntegerSafe($arrangedTo_userID).";";
					$x = $connection->db_db_query($sql);
					//echo $sql."2<br/>";
				}
				else 
				{
					// when Substition Teacher change to 'no substitution'
					if ($sql_arrangedTo_userID == "-999" && $isl[0]["SettingValue"]=="0")
					{
						## No substitution (settings: no need to calculate balance value)
						if (isset($general_settings_SLRS['NoSubstitutionCalBalance']) && $general_settings_SLRS['NoSubstitutionCalBalance'] === '1')
						{
							## update Leave Teacher
							$indexVar['libslrs']->updateSLRSBalance(IntegerSafe($userID), $academicID, "ADD", "*" . $DateLeave . " __SubstitutionArrangementUpdate__", IntegerSafe($_SESSION['UserID']), '', $_balInfo);
							$sql = "UPDATE INTRANET_SLRS_TEACHER SET Balance=Balance+1,DateTimeModified=NOW(),ModifiedBy_UserID='".IntegerSafe($_SESSION['UserID'])."' WHERE UserID=".$userID.";";
							$x = $connection->db_db_query($sql);
						}
					}
				}
			}
			/******************************** ignoreThisRec will no record in INTRANET_SLRS_LESSON_ARRANGEMENT ***
			if ($ignoreThisRec === "1") {
	
				if($isl[0]["SettingValue"]=="0"){
					
					$indexVar['libslrs']->updateSLRSBalance(IntegerSafe($userID), $academicID, "ADD", $DateLeave . " __SubstitutionArrangement__", IntegerSafe($_SESSION['UserID']));
					
					$sql = "UPDATE INTRANET_SLRS_TEACHER SET Balance=Balance+1,DateTimeModified=NOW(),ModifiedBy_UserID='".IntegerSafe($_SESSION['UserID'])."' WHERE UserID=".IntegerSafe($userID).";";
				// 	echo $sql . "3<br/>";
					$a = $connection->db_db_query($sql);
				}
				
				$sql = "UPDATE INTRANET_SLRS_LEAVE SET SubstitutionArrangement='LATER', DateTimeModified=NOW() WHERE LeaveID='".IntegerSafe($leaveID)."';";
				$a = $connection->db_db_query($sql);
	
				$sql = "DELETE FROM INTRANET_SLRS_LESSON_ARRANGEMENT WHERE LessonArrangementID='".IntegerSafe($lessonArrangementID) . "'";
				// echo $sql."<br/>";
			} else {
				$sql = "UPDATE INTRANET_SLRS_LESSON_ARRANGEMENT SET TimeStart='".$timeStart."',TimeEnd='".$timeEnd."',";
				$sql .= "ArrangedTo_UserID='".IntegerSafe($arrangedTo_userID)."',ArrangedTo_LocationID='".IntegerSafe($arrangedTo_locationID)."',isVolunteer='".IntegerSafe($isVolunteer)."',";
				$sql .= "YearClassID='".IntegerSafe($yearClassID)."',DateTimeModified=NOW(),ModifiedBy_UserID='".IntegerSafe($_SESSION['UserID'])."' ";
				$sql .= "WHERE LessonArrangementID=".IntegerSafe($lessonArrangementID);
			}
			$a = $connection->db_db_query($sql);
			*/
			/******************************** START ignoreThisRec will store -999 into INTRANET_SLRS_LESSON_ARRANGEMENT ***/
			
			$sql = "UPDATE INTRANET_SLRS_LESSON_ARRANGEMENT SET TimeStart='".$timeStart."',TimeEnd='".$timeEnd."',";
			$sql .= "ArrangedTo_UserID='".$sql_arrangedTo_userID."',ArrangedTo_LocationID='".IntegerSafe($arrangedTo_locationID)."',isVolunteer='".IntegerSafe($isVolunteer)."', LessonRemarks='" . $LessonRemarks . "',";
			$sql .= "YearClassID='".IntegerSafe($yearClassID)."',DateTimeModified=NOW(),ModifiedBy_UserID='".IntegerSafe($_SESSION['UserID'])."' ";
			$sql .= "WHERE LessonArrangementID=".IntegerSafe($lessonArrangementID);
			/******************************** END ignoreThisRec will store -999 into INTRANET_SLRS_LESSON_ARRANGEMENT ***/
			// echo $sql."4<br/>";
			$a = $connection->db_db_query($sql);
		}
		else if(sizeof($checkExistence) == '0'){
			/******************************** START ignoreThisRec will no record in INTRANET_SLRS_LESSON_ARRANGEMENT ***
			if ($ignoreThisRec != "1") {
			/******************************** END ignoreThisRec will no record in INTRANET_SLRS_LESSON_ARRANGEMENT ***/
			$allowUpdate = true;
			if ($allowUpdate) {
				// echo $sql."<br/>";
				
				$sql = "UPDATE INTRANET_SLRS_LEAVE SET SubstitutionArrangement='NOW', DateTimeModified=NOW() WHERE LeaveID='".IntegerSafe($leaveID)."';";
				$a = $connection->db_db_query($sql);
				
				$sql = "INSERT INTO INTRANET_SLRS_LESSON_ARRANGEMENT(UserID,LeaveID,TimeSlotID,TimeStart,TimeEnd,SubjectGroupID,YearClassID,SubjectID,LocationID,ArrangedTo_UserID,ArrangedTo_LocationID,isVolunteer,LessonRemarks,InputBy_UserID,DateTimeInput,DateTimeModified,ModifiedBy_UserID) ";
				$sql .="SELECT '".IntegerSafe($userID)."', '".IntegerSafe($leaveID)."', '".IntegerSafe($timeSlotID)."', '".$timeStart."', '".$timeEnd."', '";
				$sql .= IntegerSafe($subjectGroupID)."', '".IntegerSafe($yearClassID)."', '".IntegerSafe($subjectID)."', '".IntegerSafe($locationID)."', '".$sql_arrangedTo_userID."', '".IntegerSafe($arrangedTo_locationID)."', '".IntegerSafe($isVolunteer)."','" . $LessonRemarks . "','".IntegerSafe($_SESSION['UserID'])."',";
				$sql .="NOW(), NOW(), '".IntegerSafe($_SESSION['UserID']) . "'";
				$a = $connection->db_db_query($sql);
				$_LessonArrangementID = $connection->db_insert_id();
				
				if (isset($leID) && $leID > 0)
				{
					$sql = "UPDATE INTRANET_SLRS_LESSON_EXCHANGE set laID='" . $_LessonArrangementID. "' WHERE LessonExchangeID='" . $leID . "' LIMIT 1";
					$a = $connection->db_db_query($sql);
				}
				
				//check whether this leave will affect the requester balance
				$sql = "SELECT LeaveID,ReasonCode FROM INTRANET_SLRS_LEAVE WHERE LeaveID='".IntegerSafe($leaveID)."';";
				$a = $connection->returnResultSet($sql);
				$reasonCode=$a[0]["ReasonCode"];
				//echo $sql."<br/>";
				## Leave Type need to calculate the balance value
				
				$_balInfo = array(
						"LessonArrangementID" => $_LessonArrangementID,
						"LeaveID" => IntegerSafe($leaveID),
						"DateLeave" => $DateLeave
				);
				
				if($isl[0]["SettingValue"]=="0"){
					
					if (isset($general_settings_SLRS['NoSubstitutionCalBalance']) && $general_settings_SLRS['NoSubstitutionCalBalance'] === '1' && $sql_arrangedTo_userID == '-999')
					{
						## No substitution (settings: no need to calculate balance value) -- IGNORED
					}
					else 
					{
						$indexVar['libslrs']->updateSLRSBalance(IntegerSafe($userID), $academicID, "REDUCE", $DateLeave . " __SubstitutionArrangementCreate__", IntegerSafe($_SESSION['UserID']), '', $_balInfo);
						// Request
						$sql = "UPDATE INTRANET_SLRS_TEACHER SET Balance=Balance-1,DateTimeModified=NOW(),ModifiedBy_UserID='".IntegerSafe($_SESSION['UserID'])."' WHERE UserID=".IntegerSafe($userID).";";
						//echo $sql;
						$a = $connection->db_db_query($sql);
					}
				}
	
				if (IntegerSafe($arrangedTo_userID) > 0) {
					$indexVar['libslrs']->updateSLRSBalance(IntegerSafe($arrangedTo_userID), $academicID, "ADD", $DateLeave . " __SubstitutionArrangementCreate__" . "__SubstitutionArrangementSubTC__", IntegerSafe($_SESSION['UserID']), '', $_balInfo);
					// Substition Teacher
					$sql = "UPDATE INTRANET_SLRS_TEACHER SET Balance=Balance+1,DateTimeModified=NOW(),ModifiedBy_UserID='".IntegerSafe($_SESSION['UserID'])."' WHERE UserID=".IntegerSafe($arrangedTo_userID).";";
					//	echo $sql;
					$a = $connection->db_db_query($sql);
				}
			}
		}
	}
}
$saveSuccess = true;
$returnMsgKey = ($saveSuccess)? 'UpdateSuccess' : 'UpdateUnsuccess';
header('Location: ?task=management'.$slrsConfig['taskSeparator'].'substitute_arrangement'.$slrsConfig['taskSeparator'].'list&returnMsgKey='.$returnMsgKey);

?>