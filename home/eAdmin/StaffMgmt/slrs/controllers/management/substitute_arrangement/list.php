<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST, INTRANET_SLRS_LESSON_ARRANGEMENT
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
if($indexVar['libslrs']->isEJ())
{
	include_once($PATH_WRT_ROOT."includes/libschooltimetable.php");
}
else
{
	include_once($PATH_WRT_ROOT."includes/libtimetable.php");
}
 

// 20160831 added by Ivan - cookies must be set before echo anything
$currentDate = date("Y-m-d");
//$date =( isset($_POST['datePicker'])&& $_POST['datePicker']!="")?$_POST['datePicker']:$currentDate;

if (isset($_POST['datePicker'])&& $_POST['datePicker']!="") {
	$keyword = $_POST['keyword'];
	$date = $_POST['datePicker'];
	$dateArr[0]=$date;
}
else if (isset($_POST['datePicker'])&& $_POST['datePicker']=="") {	
	$keyword = $_POST['keyword'];
	$date = "%";
	
	$sql = "SELECT LeaveID,UserID,DateLeave,DateTimeModified,SubstitutionArrangement,Duration,
		Duration as DurationSession
		FROM INTRANET_SLRS_LEAVE
		WHERE DateLeave like '".$date."'";
	$connection = new libgeneralsettings();
	$a = $connection->returnResultSet($sql);
	for($i=0;$i<sizeof($a);$i++){
		$dateArr[$i]=$a[$i]["DateLeave"];
	}
}
else{
	$sql = "SELECT DateLeave FROM INTRANET_SLRS_LEAVE ORDER BY DateTimeModified DESC, DateLeave DESC LIMIT 1";
	$connection = new libgeneralsettings();
	$lastDateRec = $connection->returnResultSet($sql);
	if (count($lastDateRec) > 0 && (!$sys_custom['SLRS']["defaultTodayArrangement"])) {
		$date = $lastDateRec[0]["DateLeave"];
		$dateArr[0] = $lastDateRec[0]["DateLeave"];
		$datePicker = $lastDateRec[0]["DateLeave"];
	} else {
		$date = $currentDate;
		$dateArr[0]=$currentDate;
		$datePicker = $currentDate;
	}
}


$arrCookies = array();
$arrCookies[] = array("substitute_arrangement_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}


$ltimetable = new Timetable();
$curTimetableId = $ltimetable->Get_Current_Timetable();


# Page Title
$TAGS_OBJ[] = array($Lang['SLRS']['SubstitutionArrangement']);
$indexVar['libslrs_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libslrs_ui']->Include_Thickbox_JS_CSS();

// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$order = ($order == '') ? 0 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 3 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 50 : $numPerPage;

//echo $name_field;

// 20160831 commented by Ivan - cookies must be set before echo anything
//$date =( isset($_POST['datePicker'])&& $_POST['datePicker']!="")?$_POST['datePicker']:"%";
//if (isset($_POST['keyword'])) {
//	$keyword = $_POST['keyword'];
//}
//$arrCookies = array();
//$arrCookies[] = array("substitute_arrangement_keyword", "keyword");
//if(isset($clearCoo) && $clearCoo == 1) {
//	clearCookies($arrCookies);
//}
//else {
//	updateGetCookies($arrCookies);
//}

$name_field = $indexVar['libslrs']->getNameFieldByLang("iu.");

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("TeacherName","Lesson","Duration","DateTimeModified","Action");

if ($keyword !== '' && $keyword !== null) {
	// use Get_Safe_Sql_Like_Query for " LIKE "
	// use Get_Safe_Sql_Query for " = "
	$condsKeyword = " WHERE (
							TeacherName LIKE '%".$indexVar['libslrs']->Get_Safe_Sql_Like_Query($keyword)."%'
							OR Lesson LIKE '%".$indexVar['libslrs']->Get_Safe_Sql_Like_Query($keyword)."%'
							OR DateTimeModified LIKE '%".$indexVar['libslrs']->Get_Safe_Sql_Like_Query($keyword)."%'
						  ) 
					";
}


$li -> sql ="
		SELECT TeacherName, Lesson, Duration, DateTimeModified, Action, checkbox, LeaveID, UserID, DateLeave, orgDuration
		FROM(
			SELECT userName as TeacherName,SUM(NumberOfLessons) as Lesson, DateTimeModified,
			CONCAT('<a id=\"leaveID_',a.LeaveID,'\" class=\"tablelink\" href=\"javascript:goArrange(',a.leaveID,')\">',Action,'</a>') as Action,
					CONCAT('<input type=checkbox name=leaveIdAry[] id=leaveIdAry[] value=\"', LeaveID,'\">') as checkbox, LeaveID, Duration, SessionType, UserID, DateLeave, orgDuration
						FROM(
							SELECT isl.LeaveID,".$name_field." as userName, isl.UserID, stct.SubjectGroupID, DateTimeModified,
							CASE WHEN NumberOfLessons IS NOT NULL THEN NumberOfLessons ELSE 0 END as NumberOfLessons, SubstitutionArrangement,
							CASE WHEN SubstitutionArrangement='NOW' THEN '".$Lang['SLRS']['SubstitutionArrangementDes']['View'].
							"' WHEN SubstitutionArrangement='LATER' THEN '".$Lang['SLRS']['SubstitutionArrangementDes']['Arrange']."' END as Action,
							CASE WHEN Duration='am' THEN '".$Lang['SLRS']['SubstitutionSessionDes']['0']."'
								WHEN Duration='pm' THEN '".$Lang['SLRS']['SubstitutionSessionDes']['1']."'
								WHEN Duration='wd' THEN '".$Lang['SLRS']['SubstitutionSessionDes']['2']."'
							END as Duration, Duration as orgDuration, SessionType, DateLeave
							FROM (
								SELECT LeaveID,UserID,DateLeave,DateTimeModified,SubstitutionArrangement,Duration,
								Duration as DurationSession
								FROM INTRANET_SLRS_LEAVE
								WHERE DateLeave like '".$date."'
								) as isl										
							LEFT JOIN(
								SELECT LeaveID,SubjectGroupID, 1 as NumberOfLessons, LessonArrangementID,TimeSlotID FROM INTRANET_SLRS_LESSON_ARRANGEMENT
							) as isla ON isl.LeaveID=isla.LeaveID										
							LEFT JOIN(
								SELECT UserID,EnglishName,ChineseName,TitleChinese,TitleEnglish FROM
							   	".$slrsConfig['INTRANET_USER']." WHERE UserID
							) as iu ON isl.UserID=iu.UserID
						   	LEFT JOIN(
							   	SELECT SubjectClassTeacherID,SubjectGroupID,UserID FROM SUBJECT_TERM_CLASS_TEACHER		
							) as stct ON isl.UserID=stct.UserID AND isla.SubjectGroupID=stct.SubjectGroupID						
							LEFT JOIN (".$indexVar['libslrs']->getDaySQL($dateArr,"").") as icd ON isl.DateLeave=icd.RecordDate
							LEFT JOIN(
							   	SELECT SubjectGroupID, Day, TimeSlotID FROM INTRANET_TIMETABLE_ROOM_ALLOCATION Where TimetableID = '".$curTimetableId."'
							) as itra ON itra.SubjectGroupID=stct.SubjectGroupID AND icd.CycleDay=itra.Day AND isla.TimeSlotID=itra.TimeSlotID													
							LEFT JOIN(
								SELECT TimeSlotID,TimeSlotName,StartTime,EndTime,DisplayOrder,SessionType FROM INTRANET_TIMETABLE_TIMESLOT		
							) as itt ON itra.TimeSlotID=itt.TimeSlotID  
								AND  (CASE WHEN DurationSession='wd' THEN SessionType IN ('am','pm') ELSE DurationSession=SessionType END)							
						) as a
						GROUP BY username,UserID,DateTimeModified	
			) as a0 ".$condsKeyword."
		";
//echo $li->sql;
$li->no_col = sizeof($li->field_array) + 2;
$li->fieldorder2 = ",DateTimeModified";
$li->IsColOff = "IP25_table";
$li->count_mode = 1;

$showSADetails = true;
if ($showSADetails)
{
	$args["table"] = "custom_sql";
	$args["sql"] = $li->sql;
	$args["fieldorder2"] = $li->fieldorder2;
	
	$gridInfo = $indexVar['libslrs']->getTableGridInfo($args);
	// $gridInfo["fields"] = array("TeacherName", "Lesson", "Duration", "DateTimeModified", "Action", "checkbox", "LeaveID");
	$gridInfo["fields"] = $li->field_array;
	$gridInfo["fields"][] = "checkbox";
	$gridInfo["fields"][] = "LeaveID";
	$gridInfo["fields"][] = "UserID";
	$gridInfo["fields"][] = "DateLeave";
	$gridInfo["fields"][] = "orgDuration";
	
	$gridInfo["sql"] = $li->sql;
	
	$args = array(
			"sql" => $li->built_sql(),
			"girdInfo" => $gridInfo
	);
	$li->custDataset = true;
	$listData = $indexVar['libslrs']->getTableGridData($args);
	$li->custDataSetArr = $indexVar['libslrs']->getArrangeDetailsForListing($listData, $gridInfo);
}

$pos = 0;
$li->column_list .= "<th width='1%' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='24%' >".$li->column($pos++, $Lang['SLRS']['SubstitutionArrangementDes']['Teacher'])."</th>\n";
$li->column_list .= "<th width='23%' >".$li->column($pos++, $Lang['SLRS']['SubstitutionArrangementDes']['AffectedLesson'])."</th>\n";
$li->column_list .= "<th width='13%' >".$li->column($pos++, $Lang['SLRS']['SubstitutionArrangementDes']['Duration'])."</th>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['SLRS']['SubstitutionArrangementDes']['DateTimeModified'])."</th>\n";
$li->column_list .= "<th width='18%' >".$li->column($pos++, $Lang['SLRS']['SubstitutionArrangementDes']['Action'])."</th>\n";
$li->column_list .= "<th width='1%'>".$li->check("leaveIdAry[]")."</th>\n";
$htmlAry['dataTable'] = $li->display();


$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('pageNo', 'pageNo', $li->pageNo);
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('order', 'order', $li->order);
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('field', 'field', $li->field);
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('page_size_change', 'page_size_change', '');
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('numPerPage', 'numPerPage', $li->page_size);
$htmlAry['hiddenField'] = $x;

//echo $indexVar['libslrs']->getLocationName("R0101","班房101","Rm.101","");

// ============================== Transactional data ==============================

// ============================== Define Button ==============================
$subBtnAry = array();
$btnAry[] = array('new', 'javascript: goAddLeave();', $Lang['SLRS']['SubstitutionArrangementDes']['AddLeave'], $subBtnAry);
/*$btnAry[] = array('import', 'javascript: void(0);', '', $subBtnAry);
$subBtnAry = array();
$btnAry[] = array('export', 'javascript: void(0);', '', $subBtnAry);*/
$htmlAry['contentTool'] = $indexVar['libslrs_ui']->Get_Content_Tool_By_Array_v30($btnAry);

$htmlAry['searchBox'] = $indexVar['libslrs_ui']->Get_Search_Box_Div('keyword', $keyword);

$btnAry = array();
$btnAry[] = array('delete', 'javascript: goDelete();',$Lang['SLRS']['SubstitutionArrangementDes']['DeleteSubstition']);
$htmlAry['dbTableActionBtn'] = $indexVar['libslrs_ui']->Get_DBTable_Action_Button_IP25($btnAry);

$htmlAry['dataPickerTool']=$indexVar['libslrs_ui']->GET_DATE_PICKER('datePicker', $datePicker, $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum");
$htmlAry['viewBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['Btn']['View'], "button", "goView()", 'viewBtn');
// ============================== Define Button ==============================
// ============================== Custom Function ==============================

// ============================== Custom Function ==============================
?>