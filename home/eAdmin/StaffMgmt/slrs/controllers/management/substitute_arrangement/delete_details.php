<?php 

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT.'includes/json.php');

$connection = new libgeneralsettings();
$general_settings_SLRS = $connection->Get_General_Setting("SLRS");
$leaveID= $_POST["leaveIdAry"];

//echo $leaveID;
for($i=0; $i<sizeof($leaveID); $i++){
	//echo $leaveID[$i]."<br/>";
	$tempLeaveIDAry[] = (is_numeric($leaveID[$i])) ? $leaveID[$i] : substr($leaveID[$i],1);
	
	//check whether this leave will affect the requester balance
	$sql = "SELECT LeaveID,ReasonCode, DateLeave FROM INTRANET_SLRS_LEAVE WHERE LeaveID=".$tempLeaveIDAry[$i].";";
	$a = $connection->returnResultSet($sql);
	$reasonCode = $a[0]["ReasonCode"];
	$DateLeave = $a[0]["DateLeave"];
	
	$a = $indexVar['libslrs']->getAcademicYearTerm($DateLeave);
	$academicID=$a[0]["AcademicYearID"];
	// echo $sql."<br/>";
	$sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='SLRS' AND SettingName='OfficialLeavel".$reasonCode."';";
	$a = $connection->returnResultSet($sql);
	$SettingValue = $a[0]["SettingValue"];
	
	// echo $sql."<br/>";
	$sql = "SELECT LessonArrangementID, UserID, ArrangedTo_UserID FROM INTRANET_SLRS_LESSON_ARRANGEMENT WHERE LeaveID=".$tempLeaveIDAry[$i]."";
	$b = $connection->returnResultSet($sql);
	
	if (count($b) > 0) {
		foreach ($b as $leaveInfo) 
		{
			$userID = $leaveInfo["UserID"];
			$arrangedTo_userID = $leaveInfo["ArrangedTo_UserID"];
			
			if($SettingValue == "0")
			{
				if (isset($general_settings_SLRS['NoSubstitutionCalBalance']) && $general_settings_SLRS['NoSubstitutionCalBalance'] === '1' && $arrangedTo_userID=='-999')
				{
					## ignore 'No substitution'
				}
				else
				{
					$indexVar['libslrs']->updateSLRSBalance(IntegerSafe($userID), $academicID, "ADD", $DateLeave . " __SubstitutionArrangementDelete__", IntegerSafe($_SESSION['UserID']));
					// Request
					$sql = "UPDATE INTRANET_SLRS_TEACHER SET Balance=Balance+1,DateTimeModified=NOW(),ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID'])." WHERE UserID=".IntegerSafe($userID).";";
					// echo $sql . "<br>";
					$a = $connection->db_db_query($sql);
				}
			}
			
			$indexVar['libslrs']->updateSLRSBalance(IntegerSafe($arrangedTo_userID), $academicID, "REDUCE", $DateLeave . " __SubstitutionArrangementDelete__" . "__SubstitutionArrangementSubTC__", IntegerSafe($_SESSION['UserID']));
			// Substition Teacher
			$sql = "UPDATE INTRANET_SLRS_TEACHER SET Balance=Balance-1,DateTimeModified=NOW(),ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID'])." WHERE UserID=".IntegerSafe($arrangedTo_userID).";";
			// echo $sql . "<br>";
			$a = $connection->db_db_query($sql);
			
			$sql = "DELETE FROM INTRANET_SLRS_LESSON_ARRANGEMENT WHERE LessonArrangementID=".$leaveInfo["LessonArrangementID"]."";
			$connection->db_db_query($sql);
			// echo $sql . "<br>";
		}
	}
	
	$sql = "DELETE FROM INTRANET_SLRS_LEAVE WHERE LeaveID=".$tempLeaveIDAry[$i]."";
	$connection->db_db_query($sql);
	// echo $sql . "<br>";
}
// exit;
$saveSuccess = true;
$returnMsgKey = ($saveSuccess)? 'UpdateSuccess' : 'UpdateUnsuccess';
header('Location: ?task=management'.$slrsConfig['taskSeparator'].'substitute_arrangement'.$slrsConfig['taskSeparator'].'list&returnMsgKey='.$returnMsgKey);

?>