<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST, INTRANET_SLRS_LESSON_ARRANGEMENT
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

# Page Title
//$TAGS_OBJ[] = array($Lang['SLRS']['LessonExchangeTitle']);
//$indexVar['libslrs_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
//echo $indexVar['libslrs_ui']->Include_Thickbox_JS_CSS();

// ============================== Includes files/libraries ==============================
// ============================= Transactional data ==============================

$maxRowIndex = $_POST["maxRowIndex"];

$date = $_POST["date"];
$oriDate = $_POST["oriDate"];
$teacherName = $_POST["teacherName"];
$room = $_POST["room"];

$startTime = $_POST["startTime"];
$endTime = $_POST["endTime"];
$subjectGroupName = $_POST["subjectGroupName"];
$locationName = $_POST["locationName"];
$day = $_POST["day"];
$timeSlotName = $_POST["timeSlotName"];

$timeSlotID = $_POST["timeSlotID"];
$yearClassID = $_POST["yearClassID"];
$subjectID = $_POST["subjectID"];
$subjectGroupID = $_POST["subjectGroupID"];
$locationID = $_POST["locationID"];

$teachingTeacher = $_POST["teachingTeacher"];
$classRoom = $_POST["classRoom"];

$startIndex = $maxRowIndex + 1;
$endIndex = $maxRowIndex + 2;

$groupID = $_POST["rowGroupID"];

$connection = new libgeneralsettings();
$sql = "SELECT TeacherA_Date,TeacherB_Date FROM INTRANET_SLRS_LESSON_EXCHANGE WHERE GroupID=".$groupID."";
$a = $connection->returnResultSet($sql);
$teacherA_DateArr=array();
$teacherB_DateArr=array();
for($i=0;$i<sizeof($a);$i++){
	$teacherA_DateArr[$i]=$a[$i]["TeacherA_Date"];
	$teacherB_DateArr[$i]=$a[$i]["TeacherB_Date"];
}


$sql = "SELECT *
			FROM INTRANET_SLRS_LESSON_EXCHANGE as isle
			INNER JOIN INTRANET_USER as iu ON (isle.TeacherA_UserID =iu.UserID)
			INNER JOIN INTRANET_TIMETABLE_TIMESLOT as itt ON (isle.TeacherA_TimeSlotID=itt.TimeSlotID)
			INNER JOIN SUBJECT_TERM_CLASS as stc ON (isle.TeacherA_SubjectGroupID=stc.SubjectGroupID)
			INNER JOIN INVENTORY_LOCATION as il ON (isle.TeacherA_LocationID = il.LocationID)
			INNER JOIN ((".$indexVar['libslrs']->getDaySQL($teacherA_DateArr,"A").")) as icdA ON (isle.TeacherA_Date=icdA.RecordDateA)
			INNER JOIN ((".$indexVar['libslrs']->getDaySQL($teacherB_DateArr,"B").")) as icdB ON (isle.TeacherB_Date=icdB.RecordDateB)
			LEFT JOIN (SELECT RoomAllocationID,TimeSlotID as TimeSlotIDA,SubjectGroupID as SubjectGroupIDA,LocationID as LocationIDA,Day as DayA FROM INTRANET_TIMETABLE_ROOM_ALLOCATION) as itraA
			ON (isle.TeacherA_SubjectGroupID=itraA.SubjectGroupIDA AND icdA.CycleDayA=itraA.DayA AND isle.TeacherA_TimeSlotID=itraA.TimeSlotIDA)
			LEFT JOIN (SELECT RoomAllocationID,TimeSlotID as TimeSlotIDB,SubjectGroupID as SubjectGroupIDB,LocationID as LocationIDB,Day as DayB FROM INTRANET_TIMETABLE_ROOM_ALLOCATION) as itraB
			ON (isle.TeacherB_SubjectGroupID=itraB.SubjectGroupIDB AND icdB.CycleDayB=itraB.DayB AND isle.TeacherB_TimeSlotID=itraB.TimeSlotIDB)
			WHERE GroupID=".$groupID." AND stc.SubjectGroupID='" . $subjectGroupID . "'
			ORDER BY SequenceNumber;"
					;
//echo $sql;
// new row
$x = "<br/>";
	$x .= "<tr>"."\r\n";
		$x .="<td class=\"main_content_slrs\">"."\r\n";
		// Left Teacher
		$x .= "<div id=\"div_".$startIndex."\" class=\"slrs_exchange left\" style='clear:both;'>"."\r\n";
				$x .= "<div class=\"table_board\">";
					$x .= "<span class=\"slrs_heading\">".$Lang['SLRS']['LessonExchangeDes']['OriginalLesson']."</span><br/>"."\r\n";
						$x .= "<table style='width:90%;'>"."\r\n";
							$x .= "<colgroup><col width=\"25%\"><col class=\"field_c\"></colgroup>"."\r\n";
							$x .= "<tbody>"."\r\n";
//							$x .="<tr>"."\r\n";							
								$x .="<tr>"."\r\n";
									$x .="<td style='width:13%;'>".$Lang['SLRS']['LessonExchangeDes']['Date']."</td>";
									$x .= "<td style='width:2%;'>:</td>"."\r\n";
									$x .="<td style='width:85%;'>".$date."</td>"."\r\n";
								$x .="</tr>"."\r\n";
								$x .="<tr>"."\r\n";
									$x .="<td>".$Lang['SLRS']['LessonExchangeDes']['Teacher']."</td><td>:</td>"."\r\n";
									$x .="<td>".$teacherName."</td>"."\r\n";
								$x .="</tr>"."\r\n";
								$x .="<tr>"."\r\n";
									$x .="<td>".$Lang['SLRS']['LessonExchangeDes']['Classroom']."</td><td>:</td>"."\r\n";
									$x .="<td>".$room."</td>"."\r\n";
								$x .="</tr>"."\r\n";
								
								if ($sys_custom['SLRS']["allowSubstitutionRemarks"]) {
									$x .="<tr valign=top>"."\r\n";
									$x .="<td>". sprintf($Lang['SLRS']['LessonRemarksForOriginalLessonMarks'], $room)."</td><td>:</td>"."\r\n";
									$x .="<td><textarea name='LessonRemarks_" . $startIndex. "' style='width:100%; resize:vertical; height:40px;'></textarea></td>"."\r\n";
									$x .="</tr>"."\r\n";
								}
								
							$x .= "</tbody>"."\r\n";
						$x .= "</table>"."\r\n";									
				$x .= "</div>";
				$b = $indexVar['libslrs']->getRoomDetails($a[$j]["TeacherA_UserID"],$a[$j]["TeacherA_Date"],$a[$j]["TeacherA_TimeSlotID"]);
				$x .= "<div id=\"detailsLesson_".$startIndex."\" class=\"table_board\">";
					$x .= "<table class=\"common_table_list slrs_list\">";
						$x .= "<tr>";
						$x .= "<th style='width:30%;'></th>";
						$x .= "<th style='width:30%;' class=\"date\">".$date."<br/>".$day."</th>";
						$x .= "<th style='width:40%;' class=\"date\">".$Lang['SLRS']['LessonExchangeDes']['Location']."</th>";
						$x .= "</tr>";
						$x .= "<tr><td>".$startTime."-".$endTime."<br/>".$timeSlotName."</td>";
						$x .= "<td class=\"date class_fail\">".$subjectGroupName."</td>";
						$x .= "<td class=\"date\">".$locationName;						
						$x .= "</td>";
					$x .= "</table>";
					$x .="<input type=\"hidden\" id=\"TeachingTeacher_".$startIndex."\" value=\"".$teachingTeacher."\"/>";
					$x .="<input type=\"hidden\" id=\"ClassRoom_".$startIndex."\" value=\"".$classRoom."\"/>";
					$x .="<input type=\"hidden\" id=\"DatePicker_".$startIndex."\" value=\"".$date."\"/>";
					$x .="<input type=\"hidden\" id=\"StartTime_".$startIndex."\" value=\"".$startTime."\"/>";
					$x .="<input type=\"hidden\" id=\"EndTime_".$startIndex."\" value=\"".$endTime."\"/>";
					$x .="<input type=\"hidden\" id=\"TimeSlotID_".$startIndex."\" value=\"".$timeSlotID."\"/>";
					$x .="<input type=\"hidden\" id=\"SubjectGroupID_".$startIndex."\" value=\"".$subjectGroupID."\"/>";
					$x .="<input type=\"hidden\" id=\"YearClassID_".$startIndex."\" value=\"".$yearClassID."\"/>";
					$x .="<input type=\"hidden\" id=\"SubjectID_".$startIndex."\" value=\"".$subjectID."\"/>";
					$x .="<input type=\"hidden\" id=\"LocationID_".$startIndex."\" value=\"".$locationID."\"/>";					
					$x .="<input type=\"hidden\" id=\"Day_".$startIndex."\" value=\"".$day."\"/>";
					$x .="<input type=\"hidden\" id=\"TimeSlotName_".$startIndex."\" value=\"".$timeSlotName."\"/>";
					$x .="<input type=\"hidden\" id=\"SubjectGroupName_".$startIndex."\" value=\"".$subjectGroupName."\"/>";
					$x .="<input type=\"hidden\" id=\"LocationName_".$startIndex."\" value=\"".$locationName."\"/>";
					$x .="<input type=\"hidden\" id=\"DatePicker_".$startIndex."\" value=\"".$date."\"/>";
				$x .= "</div>"."\r\n";			
			$x .= "</div>";
			// End of Left Teacher
			// Center Separator
			$x .= "<div id=\"divCenter_".$startIndex."\" class=\"slrs_exchange_separator\"><span></span></div>"."\r\n";
			// End of Center Separator
			// Right Teacher
			$x .= "<div id=\"div_".$endIndex."\" class=\"slrs_exchange\">"."\r\n";
				$x .= "<div class=\"table_board\">";
					$x .= "<span class=\"slrs_heading\">".$Lang['SLRS']['LessonExchangeDes']['ExchangedLesson']."</span><br/>"."\r\n";
					$x .= "<table>"."\r\n";
						$x .= "<colgroup><col width=\"25%\"><col class=\"field_c\"></colgroup>"."\r\n";
							$x .= "<tbody>"."\r\n";
								$x .="<tr>"."\r\n";
									$x .="<td>".$Lang['SLRS']['LessonExchangeDes']['Date']."</td><td>:</td>"."\r\n";
									$x .="<td>".$indexVar['libslrs_ui']->GET_DATE_PICKER('datePicker_'.$endIndex, $oriDate, $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('datePicker_".$endIndex."')",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum")."</td>"."\r\n";
								$x .="</tr>"."\r\n";
								$x .="<tr>"."\r\n";
									$x .="<td>".$Lang['SLRS']['LessonExchangeDes']['Teacher']."</td><td>:</td>"."\r\n";
									
									
									// $indexVar['libslrs']->getTeachingTeacherDate($oriDate)
									$teacherList = $indexVar['libslrs']->getLessonTeacherByDate($oriDate, $checkWithArrangement = true, $ignoreExchangeGroup = array($groupID));
									/*
									$finalTeacherList = array();
									if ($sys_custom['SLRS']["disallowSubstitutionAtOneDate"]) {
										$teacherList = BuildMultiKeyAssoc($teacherList, 'UserID');
										$ignore_teacherList = $indexVar['libslrs']->getTeachingTeacherDate($oriDate, TRUE);
										$ignore_teacherList = BuildMultiKeyAssoc($ignore_teacherList, 'UserID');
										
										
										if (count($teacherList) > 0 && count($ignore_teacherList)) {
											$finalTeacherList = array_diff_key($teacherList, $ignore_teacherList);
											if (count($finalTeacherList) > 0) {
												$finalTeacherList = array_values($finalTeacherList);
											}
										} else {
											$finalTeacherList = array_values($teacherList);
										}
									} else {
										$finalTeacherList = array_values($teacherList);
									}*/
									$finalTeacherList = array_values($teacherList);
									
									$x .="<td>".getSelectByAssoArray(cnvArrToSelect($finalTeacherList), $selectionTags='id="teachingTeacher_'.$endIndex.'" name="teachingTeacher_'.$endIndex.'" onChange=changeTeachingTeacher("teachingTeacher_'.$endIndex.'")', $SelectedType="", $all=0, $noFirst=0)."</td>"."\r\n";
								$x .="</tr>"."\r\n";
								$x .="<tr>"."\r\n";
									$x .="<td>".$Lang['SLRS']['LessonExchangeDes']['Classroom']."</td><td>:</td>"."\r\n";
									$x .="<td>".getSelectByAssoArray("", $selectionTags='id="timeSlot_'.$endIndex.'" name="timeSlot_'.$endIndex.'" onChange=changeRoom("timeSlot_'.$endIndex.'")', $SelectedType="", $all=0, $noFirst=0)."</td>"."\r\n";
								$x .="</tr>"."\r\n";
							$x .= "</tbody>"."\r\n";
					$x .= "</table>"."\r\n";
				$x .= "</div>";
				$x .= "<div id=\"detailsLesson_".$endIndex."\" class=\"table_board\"></div>"."\r\n";
			$x .= "</div>";
			// End of Right Teacher
		$x .="</td>"."\r\n";
	$x .= "</tr>"."\r\n";
	// End of new row
	echo $x;

// ============================== Custom Function ==============================
function cnvArrToSelect($arr){
	$oAry = array();
	for($_i=0; $_i<sizeof($arr); $_i++){
		$oAry[$arr[$_i]["UserID"]] = Get_Lang_Selection($arr[$_i]["ChineseName"],$arr[$_i]["EnglishName"]);
	}
	return $oAry;
}

// ============================== Custom Function ==============================



?>