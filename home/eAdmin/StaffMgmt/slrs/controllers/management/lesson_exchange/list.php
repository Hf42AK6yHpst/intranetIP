<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST, INTRANET_SLRS_LESSON_ARRANGEMENT
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

if($indexVar['libslrs']->isEJ())
{
	include_once($PATH_WRT_ROOT."includes/libschooltimetable.php");
}
else
{
	include_once($PATH_WRT_ROOT."includes/libtimetable.php");
}

if (isset($_POST['keyword'])) {
	$keyword = $_POST['keyword'];
}
$arrCookies = array();
$arrCookies[] = array("lesson_exchange_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}

$ltimetable = new Timetable();
$curTimetableId = $ltimetable->Get_Current_Timetable();


# Page Title
$TAGS_OBJ[] = array($Lang['SLRS']['LessonExchange']);
$indexVar['libslrs_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libslrs_ui']->Include_Thickbox_JS_CSS();

// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================


$order = ($order == '') ? 0 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 2 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 50 : $numPerPage;

//echo $name_field;

$name_field = $indexVar['libslrs']->getNameFieldByLang("iu.");

if ($keyword !== '' && $keyword !== null) {
	// use Get_Safe_Sql_Like_Query for " LIKE "
	// use Get_Safe_Sql_Query for " = "
	$condsKeyword = " WHERE (
							UserName LIKE '%".$indexVar['libslrs']->Get_Safe_Sql_Like_Query($keyword)."%'
							OR ClassTitle LIKE '%".$indexVar['libslrs']->Get_Safe_Sql_Like_Query($keyword)."%'
							OR DateTimeModified LIKE '%".$indexVar['libslrs']->Get_Safe_Sql_Like_Query($keyword)."%'
						  )
					";
}
$withLessonExchangePromptAnotherLession = $sys_custom['SLRS']["lessonExchangePromptAnotherLession"];

$li = new libdbtable2007($field, $order, $pageNo);

// if (!$sys_custom['SLRS']["lessonExchangePromptAnotherLession"]) {
	if ($withLessonExchangePromptAnotherLession)
	{
		$li->field_array = array("Name", "Lesson", "DateTimeModified", "AnotherSubjectGroup", "checkbox");
	}
	else 
	{
		$li->field_array = array("Name", "Lesson", "DateTimeModified", "checkbox");
	}
	
	$TermInfoArr = getCurrentAcademicYearAndYearTerm();
	$academicYearID = $TermInfoArr['AcademicYearID'];
	$Year_StartDate = getStartDateOfAcademicYear($academicYearID);
	$Year_EndDate = getEndDateOfAcademicYear($academicYearID);
	
	$li->sql ="
		SELECT
			Name, Lesson, DateTimeModified, checkbox, UserName, ClassTitle,
			LessonExchangeID, TeacherA_Date, TeacherA_TimeSlotID, TeacherA_YearClassID, TeacherA_SubjectID, TeacherA_SubjectGroupID, GroupID 
		FROM(
			SELECT
				CONCAT('<div id=name_',isle.GroupID,'></div>') as Name,
				CONCAT('<div id=lesson_', isle.GroupID,'></div>') as Lesson,
				MAX(isle.DateTimeModified) as DateTimeModified,
				CONCAT('<input type=checkbox name=groupIdAry[] id=groupIdAry[] disabled value=\"',isle.GroupID,'\">') as checkbox,
				GroupID, SequenceNumber,
				CONCAT(GROUP_CONCAT(EnglishName,''),',',GROUP_CONCAT(ChineseName,''),',',GROUP_CONCAT(TitleChinese,'')) as UserName,
				CONCAT(GROUP_CONCAT(TeacherA_Date,''),',',GROUP_CONCAT(ClassTitleEN,''),',',GROUP_CONCAT(ClassTitleB5,'')) as ClassTitle,
				GROUP_CONCAT(LessonExchangeID) as LessonExchangeID, 
				GROUP_CONCAT(TeacherA_Date) as TeacherA_Date,
				GROUP_CONCAT(TeacherA_TimeSlotID) as TeacherA_TimeSlotID,
				GROUP_CONCAT(TeacherA_YearClassID) as TeacherA_YearClassID,
				GROUP_CONCAT(TeacherA_SubjectGroupID) as TeacherA_SubjectGroupID,
				GROUP_CONCAT(TeacherA_SubjectID) as TeacherA_SubjectID 
			FROM (
				SELECT
					*
				FROM 
					INTRANET_SLRS_LESSON_EXCHANGE
				WHERE 
					TeacherA_Date between '" . $Year_StartDate . "' AND '" . $Year_EndDate . "' OR
					TeacherB_Date between '" . $Year_StartDate . "' AND '" . $Year_EndDate . "'
				ORDER BY TeacherA_Date 
			) as isle
			INNER JOIN ".$slrsConfig['INTRANET_USER']." as iu ON (isle.TeacherA_UserID =iu.UserID)
			INNER JOIN INTRANET_TIMETABLE_TIMESLOT as itt ON (isle.TeacherA_TimeSlotID=itt.TimeSlotID)
			INNER JOIN SUBJECT_TERM_CLASS as stc ON (isle.TeacherA_SubjectGroupID=stc.SubjectGroupID)
			GROUP BY GroupID
			ORDER BY DateTimeModified DESC, TeacherA_Date, SequenceNumber
		) as a ".$condsKeyword."
		";
	$li->no_col = sizeof($li->field_array) + 1;
	$li->fieldorder2 = ", DateTimeModified DESC";
	$li->IsColOff = "IP25_table";
	$li->count_mode = 1;
	
	$args["table"] = "custom_sql";
	$args["sql"] = $li->sql;
	$args["fieldorder2"] = $li->fieldorder2;
	$gridInfo = $indexVar['libslrs']->getTableGridInfo($args);
	
	$gridInfo["sql"] = $li->sql;
	$gridInfo["fields"] = $li->field_array;
	
	$sortRev = array(
		" Name DESC " => " UserName DESC ",
		" Name ASC " => " UserName ASC ",
		" Lesson DESC " => " ClassTitle DESC ",
		" Lesson ASC " => " ClassTitle ASC "
	);
	
	$args = array(
			"sql" => str_replace(array_keys($sortRev), array_values($sortRev), $li->built_sql()),
			"girdInfo" => $gridInfo
	);
	
	$li->custDataset = true;
	$listData = $indexVar['libslrs']->getTableGridData($args);
	$li->custDataSetArr = $indexVar['libslrs']->checkWithSubstitution($listData);
	$pos = 0;
	if ($withLessonExchangePromptAnotherLession)
	{
		$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
		$li->column_list .= "<th width='20%'>".$li->column($pos++, $Lang['SLRS']['LessonExchangeDes']['ExchangeLessonTeacher'])."</th>\n";
		$li->column_list .= "<th width='40%'>".$li->column($pos++, $Lang['SLRS']['LessonExchangeDes']['ExchangeLesson'])."</th>\n";
		$li->column_list .= "<th width='20%'>".$li->column($pos++, $Lang['SLRS']['LessonExchangeDes']['DateTimeModified'])."</th>\n";
		$li->column_list .= "<th width='18%'>". $Lang['SLRS']['LessonExchangeDes']['AnotherClass'] . "</th>\n";
		$li->column_list .= "<th width='1'>".$li->check("groupIdAry[]")."</th>\n";
	}
	else 
	{
		$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
		$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['SLRS']['LessonExchangeDes']['ExchangeLessonTeacher'])."</th>\n";
		$li->column_list .= "<th width='60%' >".$li->column($pos++, $Lang['SLRS']['LessonExchangeDes']['ExchangeLesson'])."</th>\n";
		$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['SLRS']['LessonExchangeDes']['DateTimeModified'])."</th>\n";
		$li->column_list .= "<th width='1'>".$li->check("groupIdAry[]")."</th>\n";
	}
	
	$htmlAry['dataTable'] = $li->display();

/*
} else {
	$li->field_array = array("UserName","ClassTitle","DateTimeModified");
	
	$li -> sql ="
			SELECT Name, Lesson, DateTimeModified, checkbox, UserName, ClassTitle
			FROM(		
				SELECT 
					CONCAT('<div id=name_',isle.GroupID,'></div>') as Name, CONCAT('<div id=lesson_',isle.GroupID,'></div>') as Lesson, 
					MAX(isle.DateTimeModified) as DateTimeModified,
					CONCAT('<input type=checkbox name=groupIdAry[] id=groupIdAry[] value=\"',isle.GroupID,'\">') as checkbox,GroupID,SequenceNumber,
					CONCAT(GROUP_CONCAT(EnglishName,' '),',',GROUP_CONCAT(ChineseName,' '),',',GROUP_CONCAT(TitleChinese,' ')) as UserName,
					CONCAT(GROUP_CONCAT(TeacherA_Date,' '),',',GROUP_CONCAT(ClassTitleEN,' '),',',GROUP_CONCAT(ClassTitleB5,' ')) as ClassTitle
				FROM
					INTRANET_SLRS_LESSON_EXCHANGE as isle	
				INNER JOIN ".$slrsConfig['INTRANET_USER']." as iu ON (isle.TeacherA_UserID =iu.UserID)
				INNER JOIN INTRANET_TIMETABLE_TIMESLOT as itt ON (isle.TeacherA_TimeSlotID=itt.TimeSlotID)
				INNER JOIN SUBJECT_TERM_CLASS as stc ON (isle.TeacherA_SubjectGroupID=stc.SubjectGroupID)
				GROUP BY GroupID
			) as a ".$condsKeyword."		
			";
	// echo $li->built_sql();
	// echo $li->sql;
	
	$li->no_col = sizeof($li->field_array) + 2;
	$li->fieldorder2 = ", DateTimeModified";
	$li->IsColOff = "IP25_table";
	$li->count_mode = 1;
	
	$pos = 0;
	$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
	$li->column_list .= "<th width='30%' >".$li->column($pos++, $Lang['SLRS']['LessonExchangeDes']['ExchangeLessonTeacher'])."</th>\n";
	$li->column_list .= "<th width='40%' >".$li->column($pos++, $Lang['SLRS']['LessonExchangeDes']['ExchangeLesson'])."</th>\n";
	$li->column_list .= "<th width='30%' >".$li->column($pos++, $Lang['SLRS']['LessonExchangeDes']['DateTimeModified'])."</th>\n";
	$li->column_list .= "<th width='1'>".$li->check("groupIdAry[]")."</th>\n";
	$htmlAry['dataTable'] = $li->display();
}
*/
$connection = new libgeneralsettings();

/*
$tmp = explode("ORDER BY", $args['sql']);
$sql_order = $tmp[count($tmp) - 1];

$sql = "SELECT GROUP_CONCAT(GroupID SEPARATOR ',') as rowGroupID 
		FROM(
			SELECT GroupID FROM INTRANET_SLRS_LESSON_EXCHANGE
			WHERE 
					TeacherA_Date between '" . $Year_StartDate . "' AND '" . $Year_EndDate . "' OR
					TeacherB_Date between '" . $Year_StartDate . "' AND '" . $Year_EndDate . "'
			GROUP BY GroupID
			ORDER BY " . $sql_order . "
		)as a
		";
$a = $connection->returnResultSet($sql);
$htmlAry['maxRow'] = $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('rowGroupID', 'rowGroupID', $a[0]["rowGroupID"]);
*/


$rowGroupData = $connection->returnResultSet($args["sql"]);
if (count($rowGroupData) > 0)
{
	$rowGroupID = "";
	foreach ($rowGroupData as $kk => $vv)
	{
		if (!empty($rowGroupID))
		{
			$rowGroupID .= ",";
		}
		$rowGroupID .= $vv["GroupID"];
	}
	$htmlAry['maxRow'] = $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('rowGroupID', 'rowGroupID', $rowGroupID);
}
else 
{
	$htmlAry['maxRow'] = $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('rowGroupID', 'rowGroupID', '');
}


$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('pageNo', 'pageNo', $li->pageNo);
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('order', 'order', $li->order);
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('field', 'field', $li->field);
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('page_size_change', 'page_size_change', '');
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('numPerPage', 'numPerPage', $li->page_size);
$htmlAry['hiddenField'] = $x;

// ============================== Transactional data ==============================

// ============================== Define Button ==============================
$subBtnAry = array();
$btnAry[] = array('new', 'javascript: goExchangeLesson();', '', $subBtnAry);
/*$btnAry[] = array('import', 'javascript: void(0);', '', $subBtnAry);
$subBtnAry = array();
$btnAry[] = array('export', 'javascript: void(0);', '', $subBtnAry);*/
$htmlAry['contentTool'] = $indexVar['libslrs_ui']->Get_Content_Tool_By_Array_v30($btnAry);

$htmlAry['searchBox'] = $indexVar['libslrs_ui']->Get_Search_Box_Div('keyword', $keyword);

$btnAry = array();
$btnAry[] = array('edit', 'javascript: goEdit();');
$btnAry[] = array('delete', 'javascript: goDelete();',$Lang['SLRS']['LessonExchangeDes']['DeleteSubstition']);
$htmlAry['dbTableActionBtn'] = $indexVar['libslrs_ui']->Get_DBTable_Action_Button_IP25($btnAry);
// ============================== Define Button ==============================

?>