<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST, INTRANET_SLRS_LESSON_ARRANGEMENT
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
$connection = new libgeneralsettings();

$GroupID = $_GET["gpid"];
$LessonExchangeID = $_GET["laid"];
$orginal_SubjectGroupID = $_GET["osg"];
$SubjectGroupID = $_GET["nsg"];

function checkEmpty($value){
	return (($value==""||$value==null)?"-":$value);
}

if (!$sys_custom['SLRS']["lessonExchangePromptAnotherLessionForced"])
{
	header("Location: ./");
	exit;
}

function convertMultipleRowsIntoOneRow($arr,$fieldName){
	$x = "";
	for($i=0; $i<sizeof($arr);$i++){
		if($i==sizeof($arr)-1){
			$x .= $arr[$i][$fieldName];
		}
		else{
			$x .= $arr[$i][$fieldName].",";
		}
	}
	if(sizeof($arr)==0){
		$x = "''";
	}
	return $x;
}


$log_param = array(
	"group_id" => $GroupID,
	"lesson_exchange_id" => $LessonExchangeID,
	"orginal_subject_group_id" => $orginal_SubjectGroupID,
	"related_subject_group_id" => $SubjectGroupID,
	"remarks" => "",
	"created_at" => date("Y-m-d H:i:s")
);

$token = md5($GroupID. "_" . $LessonExchangeID . "_" . $orginal_SubjectGroupID. "_" . $SubjectGroupID. "_SLRS_FORCE_EXC");
if ($token == $_GET["exchangetoken"])
{
	$saveSuccess = true;
	
	$strSQL = "SELECT (MAX(GroupID) + 1) as NextGroupID FROM INTRANET_SLRS_LESSON_EXCHANGE";
	$result = $indexVar["libslrs"]->returnResultSet($strSQL);
	$slaveGroupID = $result[0]["NextGroupID"];
	
	
	$strSQL = "SELECT
					LessonExchangeID,
					TeacherA_UserID, TeacherA_Date, TeacherA_TimeSlotID, TeacherA_TimeStart, TeacherA_TimeEnd,
					TeacherA_SubjectGroupID, TeacherA_YearClassID, TeacherA_SubjectID, TeacherA_LocationID,
					TeacherB_UserID, TeacherB_Date, TeacherB_TimeSlotID, TeacherB_TimeStart, TeacherB_TimeEnd,
					TeacherB_SubjectGroupID, TeacherB_YearClassID, TeacherB_SubjectID, TeacherB_LocationID,
					Remarks, GroupID, SequenceNumber, LessonRemarks, laID,
					combined_type, combined_id
			FROM INTRANET_SLRS_LESSON_EXCHANGE WHERE GroupID = '" . $GroupID . "' ORDER BY SequenceNumber ASC";
	$result = $indexVar["libslrs"]->returnResultSet($strSQL);
	
	if (count($result) > 0)
	{
		$anotherInfo = $indexVar["libslrs"]->getAnotherSubjectGroupByExchangeRecord( array('LessonExchangeID' => $LessonExchangeID), true );
		
		if (isset($anotherInfo[$_GET['nsg']]))
		{
			$selected_anotherInfo = $anotherInfo[$_GET['nsg']];
			
			$_db_record = array();
			foreach ($result as $kk => $vv)
			{
				
				if ($selected_anotherInfo['TeacherA_Date'] == $vv["TeacherA_Date"] &&
					$selected_anotherInfo['TeacherA_TimeSlotID'] == $vv["TeacherA_TimeSlotID"] &&
					$selected_anotherInfo['TeacherA_SubjectGroupID'] == $vv["TeacherA_SubjectGroupID"])
				{
					$update_record = $vv;
					$update_record["combined_type"] = "MASTER";
					$_db_record[$vv["LessonExchangeID"]]["master"] = $update_record;

					$yearClassIDSQL = $indexVar['libslrs']->getYearClassID($selected_anotherInfo['TeacherA_Date']);
					//$yearClass = $yearClassIDSQL[0]["YearClassID"];
					$yearClass = convertMultipleRowsIntoOneRow($yearClassIDSQL,"YearClassID");
					$classInfo = $indexVar['libslrs']->getClassInfoBySubjectGroupID($selected_anotherInfo["SubjectGroupID"], $yearClass);
					
					$strSQL = "SELECT SubjectID FROM SUBJECT_TERM WHERE SubjectGroupID='" . $selected_anotherInfo["SubjectGroupID"] . "' LIMIT 1";
					$subject_result = $indexVar["libslrs"]->returnResultSet($strSQL);
					
					$combined_record = $vv;
					$combined_record["combined_type"] = "SLAVE";
					$combined_record["has_another"] = "1";
					$combined_record["combined_id"] = $vv["LessonExchangeID"];
					$combined_record["GroupID"] = $slaveGroupID;
					$combined_record["TeacherA_UserID"] = $selected_anotherInfo["UserID"];
					unset($combined_record["LessonExchangeID"]);
					
					$combined_record["TeacherA_SubjectGroupID"] = $selected_anotherInfo["SubjectGroupID"];
					$combined_record["TeacherB_LocationID"] = $selected_anotherInfo["LocationID"];
					if (isset($classInfo[0]["YearClassID"])) $combined_record["TeacherA_YearClassID"] = $classInfo[0]["YearClassID"];
					if (isset($subject_result[0]["SubjectID"])) $combined_record["TeacherA_SubjectID"] = $subject_result[0]["SubjectID"];
					$_db_record[$vv["LessonExchangeID"]]["slave"] = $combined_record;
				}
				else if (
						$selected_anotherInfo['TeacherA_Date'] == $vv["TeacherB_Date"] &&
						$selected_anotherInfo['TeacherA_TimeSlotID'] == $vv["TeacherB_TimeSlotID"] &&
						$selected_anotherInfo['TeacherA_SubjectGroupID'] == $vv["TeacherB_SubjectGroupID"]
						)
				{
					$update_record = $vv;
					$update_record["combined_type"] = "MASTER";
					$_db_record[$vv["LessonExchangeID"]]["master"] = $update_record;

					
					$yearClassIDSQL = $indexVar['libslrs']->getYearClassID($selected_anotherInfo['TeacherB_Date']);
					//$yearClass = $yearClassIDSQL[0]["YearClassID"];
					$yearClass = convertMultipleRowsIntoOneRow($yearClassIDSQL,"YearClassID");
					$classInfo = $indexVar['libslrs']->getClassInfoBySubjectGroupID($selected_anotherInfo["SubjectGroupID"], $yearClass);
					
					$combined_record = $vv;
					$combined_record["combined_type"] = "SLAVE";
					$combined_record["combined_id"] = $vv["LessonExchangeID"];
					$combined_record["has_another"] = "0";
					$combined_record["GroupID"] = $slaveGroupID;
					$combined_record["TeacherB_UserID"] = $selected_anotherInfo["UserID"];
					unset($combined_record["LessonExchangeID"]);
					
					$combined_record["TeacherB_SubjectGroupID"] = $selected_anotherInfo["SubjectGroupID"];
					if (isset($classInfo[0]["YearClassID"])) $combined_record["TeacherB_YearClassID"] = $classInfo[0]["YearClassID"];
					if (isset($subject_result[0]["SubjectID"])) $combined_record["TeacherB_SubjectID"] = $subject_result[0]["SubjectID"];
					$_db_record[$vv["LessonExchangeID"]]["slave"] = $combined_record;
				}
			}
			
			if (count($_db_record) >= 2)
			{
				foreach ($_db_record as $lxid => $pa_val)
				{
					if (count($pa_val) == 2)
					{
						$strSQL = "UPDATE INTRANET_SLRS_LESSON_EXCHANGE 
										SET DateTimeModified=NOW(), ModifiedBy_UserID='" . $_SESSION["UserID"] . "',
										combined_type='" . $pa_val["master"]["combined_type"] . "' 
										WHERE LessonExchangeID='" . $lxid . "' AND combined_type='NORMAL';";
						$indexVar['libslrs']->db_db_query($strSQL);
						
						$strSQL = "INSERT INTO 
										INTRANET_SLRS_LESSON_EXCHANGE (" . implode(", ", array_keys($pa_val["slave"])) . ", InputBy_UserID, DateTimeInput, DateTimeModified, ModifiedBy_UserID)
									 VALUES
										('" . implode("', '", array_values($pa_val["slave"])) . "', '" . $_SESSION["UserID"] . "', NOW(), NOW(), '" . $_SESSION["UserID"] . "');" ;
						$indexVar['libslrs']->db_db_query($strSQL);
						
						$record_log = $log_param;
						$record_log["lesson_exchange_id"] = $lxid;
						$record_log["remarks"] = "SUCCESS";
						
						$strSQL = "INSERT INTO INTRANET_SLRS_LESSON_EXCHANGE_COMBINED_REQ (" . implode(", ", array_keys($record_log)) . ") VALUES ('" . implode("', '", array_values($record_log)) . "');";
						$indexVar['libslrs']->db_db_query($strSQL);
					}
					else 
					{
						$record_log = $log_param;
						$record_log["lesson_exchange_id"] = $lxid;
						$record_log["remarks"] = "FAILED";
						
						$strSQL = "INSERT INTO INTRANET_SLRS_LESSON_EXCHANGE_COMBINED_REQ (" . implode(", ", array_keys($record_log)) . ") VALUES ('" . implode("', '", array_values($record_log)) . "');";
						$indexVar['libslrs']->db_db_query($strSQL);
						
						$log_param["remarks"] = "Is not master and slave";
						$saveSuccess = false;
					}
				}
			}
			else 
			{
				$log_param["remarks"] = "Is not pair record.";
				$saveSuccess = false;
			}
		}
		else
		{
			$log_param["remarks"] = "Another Subject Group not found";
			$saveSuccess = false;
		}
	}
	else
	{
		$log_param["remarks"] = "Group info not found";
		$saveSuccess = false;
	}

}
else
{
	$log_param["remarks"] = "token failed";
	$saveSuccess = false;
}
if (!$saveSuccess)
{
	$strSQL = "INSERT INTO INTRANET_SLRS_LESSON_EXCHANGE_COMBINED_REQ (" . implode(", ", array_keys($log_param)) . ") VALUES ('" . implode("', '", array_values($log_param)) . "')";
	$indexVar['libslrs']->db_db_query($strSQL);
}
$returnMsgKey = ($saveSuccess)? 'UpdateSuccess' : 'UpdateUnsuccess';
header('Location: ?task=management'.$slrsConfig['taskSeparator'].'lesson_exchange'.$slrsConfig['taskSeparator'].'list&returnMsgKey='.$returnMsgKey);

?>