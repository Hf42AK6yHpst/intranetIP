<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');

# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$type = $_POST['type'];
$date = $_POST['date'];
$timeSlotID = $_POST['timeSlotID'];
$userID = $_POST['userID'];
$teacher = $_POST['teacher'];

$connection = new libgeneralsettings();
$arr = array();
$jsonObj = new JSON_obj();

if($type == "checkExistence"){
	// check duplication
	$a = checkExistence($connection,$date,$userID,$timeSlotID);
	// check supply teacher period
	$b = checkOverlapSupplyTeacher($connection,$date,$userID);
	// check 
	
	if(sizeof($a)>0){
		$arr[] = array('result' => "1", 'msg' => $Lang['SLRS']['TemporarySubstitutionArrangementDes']['DuplicationRequest']);
	}
	else if(sizeof($b)>0){
		$arr[] = array('result' => "1", 'msg' => $Lang['SLRS']['TemporarySubstitutionArrangementDes']['ExistSupplyTeacherTemporarySubstitution']);
	}
	else{
		$arr[] = array('result' => "0", 'msg' => "");
	}
	echo $jsonObj->encode($arr);
}

function checkExistence($connection,$date,$userID,$timeSlotID){
	$sql = "SELECT * FROM INTRANET_SLRS_LESSON_ARRANGEMENT WHERE LeaveID=-1 AND UserID=".IntegerSafe($userID)." AND TimeSlotID=".IntegerSafe($timeSlotID).
	" AND DATE_FORMAT(TimeStart,'%Y-%m-%d')='".$date."'";	
	return $connection->returnResultSet($sql);
}


function checkOverlapSupplyTeacher($connection,$date,$userID){
	$sql = "SELECT *
			FROM INTRANET_SLRS_SUPPLY_PERIOD WHERE '".$date."' BETWEEN DateStart AND DateEnd AND TeacherID=".IntegerSafe($userID)."		
		";
	return $connection->returnResultSet($sql);
}




?>