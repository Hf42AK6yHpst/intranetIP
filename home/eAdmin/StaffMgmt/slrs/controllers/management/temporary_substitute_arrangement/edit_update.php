<?php 

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."includes/slrs/libslrs.php");

$connection = new libgeneralsettings();
$indexVar['libslrs'] = new libslrs();

$userID = $_POST["leaveTeacher"];
$leaveID = $_POST["leaveID"];
$startIndex = $_POST["startIndex"];
$endIndex = $_POST["endIndex"];

$general_settings_SLRS = $connection->Get_General_Setting("SLRS");

function TextSafe($val, $rev = false) {
	$replaceArr = array(
			'"' => "&#34;",
			"'" => "&#39;",
	);
	return str_replace(array_keys($replaceArr), array_values($replaceArr), $val);
}

for($i=$startIndex; $i<=$endIndex;$i++){
	
	$date = $_POST["datePicker"];
	$timeSlotID = $_POST["timeSlotID_".$i];
	$arrangedTo_userID = $_POST["teacher_".$i];
	$arrangedTo_locationID = $_POST["locationID_".$i];
	$timeStart = $date." ".$_POST["startTime_".$i].":00";
	$timeEnd = $date." ".$_POST["endTime_".$i].":00";;
	$subjectGroupID = $_POST["subjectGroupID_".$i];
	$subjectID = $_POST["subjectID_".$i];
	$locationID = $_POST["originalLocationID_".$i];
	$lessonArrangementID = $_POST["lessonArrangementID_".$i];
	$isVolunteer = $_POST["isVolunteer_".$i];
	$isVolunteer=($isVolunteer=="1")?"1":"0";
	
	$DateLeave = date("Y-m-d", strtotime($timeStart));
	
	$LessonRemarks = TextSafe($_POST["LessonRemarks_".$i]);
	
	$ignoreThisRec = $_POST["ignoreThisRec_".$i];
	$ignoreThisRec = ($ignoreThisRec=="1")?"1":"0";
	
	$dt=strtotime($timeStart);
	$date = date("Y-m-d",$dt);
	
	$a = $indexVar['libslrs']->getAcademicYearTerm($date);
	$academicID=$a[0]["AcademicYearID"];
	
	//get YearClassID
	$sql = "
			SELECT yc.YearClassID
			FROM(
				SELECT SubjectGroupID,UserID FROM SUBJECT_TERM_CLASS_USER WHERE SubjectGroupID=".IntegerSafe($subjectGroupID)."
			) as stcu
			INNER JOIN(
				SELECT UserID,YearClassID FROM YEAR_CLASS_USER
			) as ycu ON stcu.UserID=ycu.UserID
			INNER JOIN(
				SELECT YearClassID FROM YEAR_CLASS_TEACHER
			) as yct ON ycu.YearClassID=yct.YearClassID
			INNER JOIN(
				SELECT YearClassID,AcademicYearID FROM YEAR_CLASS WHERE AcademicYearID IN (".$academicID.")
			) as yc ON yct.YearClassID=yc.YearClassID
			GROUP BY yc.YearClassID
			";
	$a = $connection->returnResultSet($sql);
	$yearClassID=$a[0]["YearClassID"];

	$sql = "SELECT UserID, LessonArrangementID FROM INTRANET_SLRS_LESSON_ARRANGEMENT WHERE UserID=".IntegerSafe($userID)." AND LeaveID=".IntegerSafe($leaveID)." AND TimeSlotID=".IntegerSafe($timeSlotID)." AND DATE_FORMAT(TimeStart,'%Y-%m-%d')='".$date."'";
	$checkExistence = $connection->returnResultSet($sql);
	
	if(sizeof($checkExistence) > 0)
	{
		$_balInfo = array(
				"LessonArrangementID" => $checkExistence[0]["LessonArrangementID"],
				"LeaveID" => IntegerSafe($leaveID),
				"DateLeave" => $DateLeave
		);
		if ($ignoreThisRec === "1") {
			$sql_arrangedTo_userID = -999;
			/*
			 if($isl[0]["SettingValue"]=="0"){
			 
			 $indexVar['libslrs']->updateSLRSBalance(IntegerSafe($userID), $academicID, "ADD", $DateLeave . " __SubstitutionArrangement__", IntegerSafe($_SESSION['UserID']));
			 
			 $sql = "UPDATE INTRANET_SLRS_TEACHER SET Balance=Balance+1,DateTimeModified=NOW(),ModifiedBy_UserID='".IntegerSafe($_SESSION['UserID'])."' WHERE UserID=".IntegerSafe($userID).";";
			 // 	echo $sql . "3<br/>";
			 $a = $connection->db_db_query($sql);
			 }
			 */
		} else {
			$sql_arrangedTo_userID = IntegerSafe($arrangedTo_userID);
		}
		
		$sql = "SELECT * FROM INTRANET_SLRS_LESSON_ARRANGEMENT WHERE LessonArrangementID=".IntegerSafe($lessonArrangementID).";";
		$a = $connection->returnResultSet($sql);
		//echo $sql."<br/>";
		if($a[0]["ArrangedTo_UserID"] <> $sql_arrangedTo_userID)
		{
			//echo $sql."<br/>";
			if ($a[0]["ArrangedTo_UserID"] > 0)
			{
				// the latest Substition Teacher
				$indexVar['libslrs']->updateSLRSBalance(IntegerSafe($a[0]["ArrangedTo_UserID"]), $academicID, "REDUCE", $DateLeave . " __TempSubstitutionArrangementUpdate__" . "__SubstitutionArrangementSubTC__", IntegerSafe($_SESSION['UserID']), '', $_balInfo);
				// the original Substition Teacher
				$sql = "UPDATE INTRANET_SLRS_TEACHER SET Balance=Balance-1,DateTimeModified=NOW(),ModifiedBy_UserID='".IntegerSafe($_SESSION['UserID'])."' WHERE UserID=".IntegerSafe($a[0]["ArrangedTo_UserID"]).";";
				$x = $connection->db_db_query($sql);
			}
			//echo $sql."1<br/>";
			// the latest Substition Teacher
			if ($sql_arrangedTo_userID > 0)
			{
				// when 'no substitution' change to another teacher
				if ($a[0]["ArrangedTo_UserID"] == "-999")
				{
					if (isset($general_settings_SLRS['NoSubstitutionCalBalance']) && $general_settings_SLRS['NoSubstitutionCalBalance'] === '1')
					{
						## No substitution (settings: no need to calculate balance value)
						## update Leave Teacher
						$indexVar['libslrs']->updateSLRSBalance(IntegerSafe($userID), $academicID, "REDUCE", "*" . $DateLeave . " __TempSubstitutionArrangementUpdate__", IntegerSafe($_SESSION['UserID']), '', $_balInfo);
						$sql = "UPDATE INTRANET_SLRS_TEACHER SET Balance=Balance-1,DateTimeModified=NOW(),ModifiedBy_UserID='".IntegerSafe($_SESSION['UserID'])."' WHERE UserID=".$userID.";";
						$x = $connection->db_db_query($sql);
					}
				}
				
				$indexVar['libslrs']->updateSLRSBalance(IntegerSafe($arrangedTo_userID), $academicID, "ADD", $DateLeave . " __TempSubstitutionArrangementUpdate__" . "__SubstitutionArrangementSubTC__", IntegerSafe($_SESSION['UserID']), '', $_balInfo);
				$sql = "UPDATE INTRANET_SLRS_TEACHER SET Balance=Balance+1,DateTimeModified=NOW(),ModifiedBy_UserID='".IntegerSafe($_SESSION['UserID'])."' WHERE UserID=".IntegerSafe($sql_arrangedTo_userID).";";
				$x = $connection->db_db_query($sql);
				//echo $sql."2<br/>";
			}
			else 
			{
				// when Substition Teacher change to 'no substitution'
				if ($sql_arrangedTo_userID == "-999")
				{
					if (isset($general_settings_SLRS['NoSubstitutionCalBalance']) && $general_settings_SLRS['NoSubstitutionCalBalance'] === '1')
					{
						## No substitution (settings: no need to calculate balance value)
						## update Leave Teacher
						$indexVar['libslrs']->updateSLRSBalance(IntegerSafe($userID), $academicID, "ADD", "*" . $DateLeave . " __TempSubstitutionArrangementUpdate__", IntegerSafe($_SESSION['UserID']), '', $_balInfo);
						$sql = "UPDATE INTRANET_SLRS_TEACHER SET Balance=Balance+1,DateTimeModified=NOW(),ModifiedBy_UserID='".IntegerSafe($_SESSION['UserID'])."' WHERE UserID=".$userID.";";
						$x = $connection->db_db_query($sql);
					}
				}
			}

			//echo $sql."<br/>";
		}
		
		$sql = "UPDATE INTRANET_SLRS_LESSON_ARRANGEMENT SET TimeStart='".$timeStart."',TimeEnd='".$timeEnd."',";
		$sql .= "ArrangedTo_UserID='".IntegerSafe($sql_arrangedTo_userID)."',ArrangedTo_LocationID='".IntegerSafe($arrangedTo_locationID)."',isVolunteer='".IntegerSafe($isVolunteer)."', LessonRemarks='" . $LessonRemarks . "',";
		$sql .= "YearClassID='".IntegerSafe($yearClassID)."',DateTimeModified=NOW(),ModifiedBy_UserID='".IntegerSafe($_SESSION['UserID'])."' ";
		$sql .= "WHERE LessonArrangementID='".IntegerSafe($lessonArrangementID) . "'";		
		//echo $sql;
		$a = $connection->db_db_query($sql);
		
	}
	else if(sizeof($checkExistence) == 0){
		
		if ($ignoreThisRec === "1") {
			$sql_arrangedTo_userID = -999;
		} else {
			$sql_arrangedTo_userID = IntegerSafe($arrangedTo_userID);
		}
		
		$sql = "INSERT INTO INTRANET_SLRS_LESSON_ARRANGEMENT(UserID,LeaveID,TimeSlotID,TimeStart,TimeEnd,SubjectGroupID,YearClassID,SubjectID,LocationID,ArrangedTo_UserID,ArrangedTo_LocationID,isVolunteer,LessonRemarks,InputBy_UserID,DateTimeInput,DateTimeModified,ModifiedBy_UserID) ";
		$sql .="SELECT '".IntegerSafe($userID)."','".IntegerSafe($leaveID)."','".IntegerSafe($timeSlotID)."','".$timeStart."','".$timeEnd."',";
		$sql .="'" . IntegerSafe($subjectGroupID)."','".IntegerSafe($yearClassID)."','".IntegerSafe($subjectID)."','".IntegerSafe($locationID)."','".IntegerSafe($sql_arrangedTo_userID)."','".IntegerSafe($arrangedTo_locationID)."','".IntegerSafe($isVolunteer)."','" . $LessonRemarks . "','".IntegerSafe($_SESSION['UserID'])."',";
		$sql .="NOW(),NOW(),'".IntegerSafe($_SESSION['UserID']) . "'";		
		$a = $connection->db_db_query($sql);	
		
		
		if (isset($general_settings_SLRS['NoSubstitutionCalBalance']) && $general_settings_SLRS['NoSubstitutionCalBalance'] === '1' && $sql_arrangedTo_userID == '-999')
		{
			## No substitution (settings: no need to calculate balance value) -- IGNORED
		}
		else
		{
			// Request
			$indexVar['libslrs']->updateSLRSBalance(IntegerSafe($userID), $academicID, "REDUCE", $DateLeave . " __TempSubstitutionArrangementCreate__", IntegerSafe($_SESSION['UserID']));
			$sql = "UPDATE INTRANET_SLRS_TEACHER SET Balance=Balance-1,DateTimeModified=NOW(),ModifiedBy_UserID='".IntegerSafe($_SESSION['UserID'])."' WHERE UserID=".IntegerSafe($userID).";";
			//echo $sql;
			$a = $connection->db_db_query($sql);
		}
		
		if (IntegerSafe($arrangedTo_userID) > 0)
		{
			// 	Substitution Teacher
			$indexVar['libslrs']->updateSLRSBalance(IntegerSafe($arrangedTo_userID), $academicID, "ADD", $DateLeave . " __TempSubstitutionArrangementCreate__" . "__SubstitutionArrangementSubTC__", IntegerSafe($_SESSION['UserID']));
			$sql = "UPDATE INTRANET_SLRS_TEACHER SET Balance=Balance+1,DateTimeModified=NOW(),ModifiedBy_UserID='".IntegerSafe($_SESSION['UserID'])."' WHERE UserID=".IntegerSafe($arrangedTo_userID).";";
			//	echo $sql;
			$a = $connection->db_db_query($sql);
		}
	}
}

$returnMsgKey = ($saveSuccess)? 'UpdateSuccess' : 'UpdateUnsuccess';
header('Location: ?task=management'.$slrsConfig['taskSeparator'].'temporary_substitute_arrangement'.$slrsConfig['taskSeparator'].'list&selected_date=' . $_POST["datePicker"]);

?>