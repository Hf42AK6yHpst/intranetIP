<?php 

// ============================== Related Tables ==============================
// INTRANET_SLRS_BALANCE_ADJUST, INTRANET_SLRS_TEACHER
// ============================== Related Tables ==============================

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
$connection = new libgeneralsettings();

$saveSuccess = true;

$data = array();

$balanceDisplay=$indexVar['libslrs']->getBalanceDisplay();

$userID = $_POST["hideUserID"];
$userIDArr = explode(",",$userID);
$hidePreviousBalance = $_POST["hidePreviousBalance"];
$hidePreviousBalanceArr = explode(",",$hidePreviousBalance);
$hideUpdateBalance = $_POST["hideUpdateBalance"];
$hideUpdateBalanceArr = explode(",",$hideUpdateBalance);
$adjustedType = $_POST["balanceSelection"];
$reason = $_POST["reason"];
// $academicYearID = $_POST["hideAcademicYearID"];

if (strtoupper($_POST['balanceSelection']) == 'SET_YEAR')
{
	$resetType = "DATE_RESET";
}
else 
{
	$resetType = '';
}


$TermInfoArr = getCurrentAcademicYearAndYearTerm();
$academicYearID = $TermInfoArr['AcademicYearID'];
$Year_StartDate = date("Y-m-d", strtotime(getStartDateOfAcademicYear($academicYearID)));
$Year_EndDate = date("Y-m-d", strtotime(getEndDateOfAcademicYear($academicYearID)));

$settingsArr = $indexVar['libslrs']->getOfficialLeavelSetting($onlyDisableAdjust= false);
$general_settings_SLRS = $connection->Get_General_Setting("SLRS");

// $Lang['SLRS']['NoSubstitutionCalBalanceDes'][0] = "Enable";
// $Lang['SLRS']['NoSubstitutionCalBalanceDes'][1] = "Disable";

//echo $sql."<br/>";

$forDebug = false;
if ($forDebug)
{
	echo "<pre>";
	print_r($_POST);
	print_r($TermInfoArr);
	echo "</pre>";
	echo "<hr style='border:0px; border-bottom:1px solid #000;'>";
	echo "<div style='font-size:12px; line-height:18px; font-family:Arial;'>";
}
for($i=0; $i<sizeof($userIDArr);$i++)
{
	
	// get the maximun BalanceAdjustID
	$sql = "SELECT MAX(BalanceAdjustID) as MaxBalanceAdjustID FROM INTRANET_SLRS_BALANCE_ADJUST;";
	$a = $connection->returnArray($sql);
	$maxBalanceID = ($a[0]["MaxBalanceAdjustID"]=="")?"1":$a[0]["MaxBalanceAdjustID"]+1;
	// update the table of INTRANET_SLRS_BALANCE_ADJUST
	
	if ($resetType == "DATE_RESET")
	{
		if (strtotime($Year_StartDate) <= strtotime($_POST['datePicker']) && strtotime($Year_EndDate) >= strtotime($_POST['datePicker']))
		{
			if (strtotime($Year_StartDate) > strtotime($_POST['datePicker']))
			{
				$manual_start = $Year_StartDate;
			}
			else
			{
				$manual_start = $_POST['datePicker'];
			}
			$sql = "UPDATE INTRANET_SLRS_BALANCE_ADJUST SET Deleted_at = NOW(), Deleted_by='" . $_SESSION["UserID"] . "' WHERE UserID='".IntegerSafe($userIDArr[$i])."' AND AcademicYearID='" . $academicYearID . "'";
			if ($forDebug)
			{
				echo "<b>Remove all records of this year($academicYearID) for this user (" . IntegerSafe($userIDArr[$i]) . ")</b><br>";
				echo $sql . "<br><br>";
			}
			else $connection->db_db_query($sql);
			
			
			$sql = "SELECT BalanceAdjustTo FROM INTRANET_SLRS_BALANCE_ADJUST WHERE UserID='".IntegerSafe($userIDArr[$i])."' AND Deleted_at IS NULL ORDER BY DateTimeModified DESC, BalanceAdjustID DESC LIMIT 1";
			if ($forDebug)
			{
				echo "<b>Get Latest Balance Adjustment for this user (" . IntegerSafe($userIDArr[$i]) . ")</b><br>";
				echo $sql . "<br><br>";
			}
			$query_result = $connection->returnArray($sql);
			$latest_balanceAmount = (isset($query_result[0]['BalanceAdjustTo'])) ? $query_result[0]['BalanceAdjustTo'] : 0;
			$reset_balanceAmount = $_POST["number"];
			$final_balance = $reset_balanceAmount;
			
			$insert_param = array(
					"UserID" => IntegerSafe($userIDArr[$i]),
					"AcademicYearID" => $academicYearID,
					"BalanceBeforeAdjust" => $latest_balanceAmount,
					"BalanceAdjustTo" => $final_balance,
					"AdjustedType" => $adjustedType,
					"Reason" => $reason,
					"InputBy_UserID" => $_SESSION["UserID"],
					"ModifiedBy_UserID" => $_SESSION["UserID"],
					"DateLeave" => $manual_start,
					"ResetType" => 'ADJUST'
			);
			$sql = "INSERT INTO INTRANET_SLRS_BALANCE_ADJUST ( " . implode(', ', array_keys($insert_param)) . ", DateTimeInput, DateTimeModified) VALUES ('" . implode("', '", array_values($insert_param)) . "', NOW(), NOW())";
			if ($forDebug)
			{
				echo "<b>INSERT New Balance Adjustment for this user (" . IntegerSafe($userIDArr[$i]) . ")</b><br>";
				echo $sql . "<br><br>";
			}
			else $connection->db_db_query($sql);
			
			$latest_balanceAmount = $final_balance;
			
			$sql = "SELECT
				isla.*, isl.DateLeave, isl.ReasonCode, isl.Duration
			FROM
				INTRANET_SLRS_LESSON_ARRANGEMENT AS isla
			LEFT JOIN
				INTRANET_SLRS_LEAVE AS isl ON (isla.LeaveID=isl.LeaveID)
			WHERE
				isla.TimeStart >= '" . $manual_start. "' AND (isla.UserID='".IntegerSafe($userIDArr[$i])."' OR isla.ArrangedTo_UserID='".IntegerSafe($userIDArr[$i])."')
			ORDER BY isla.TimeStart";
			$query_result = $connection->returnResultSet($sql);
			if ($forDebug)
			{
				echo "<b>Append all Substitution Records after " . $manual_start . "</b><br>";
				echo $sql . "<br><br>";
			}
			if (count($query_result) > 0)
			{
				foreach($query_result as $key => $val)
				{
					$user_AdjustedType = "";
					$BalanceAdjustTo = $final_balance;
					$Reason = DATE("Y-m-d", strtotime($val["TimeStart"])) . " ";
					if ($val["LeaveID"] > 0)
					{
						$Reason .= "__SubstitutionArrangementCreate__";
					}
					else
					{
						$Reason .= "__TempSubstitutionArrangementCreate__";
					}
					if ($val["UserID"] == $userIDArr[$i])
					{
						$user_AdjustedType = ($balanceDisplay) ? "REDUCE" : "ADD";
						if (isset($settingsArr[$val['ReasonCode']]) && $settingsArr[$val['ReasonCode']] == '0' || !isset($settingsArr[$val['ReasonCode']]))
						{
							if (isset($general_settings_SLRS['NoSubstitutionCalBalance']) && $general_settings_SLRS['NoSubstitutionCalBalance'] === '1' && $val['ArrangedTo_UserID'] == '-999')
							{
								if ($forDebug)
								{
									if ($val["LeaveID"] > 0)
									{
										echo "<b>Leave ID:" . $val["LeaveID"] . " - No substitution</b><br>";
									}
									else
									{
										echo "<b>Leave ID (Temp): - No substitution</b><br>";
									}
									echo "IGNORE LessonArrangementID: " . $val['LessonArrangementID'] . " <br><br>";
								}
							}
							else
							{
								$BalanceAdjustTo -= 1 * $balanceDisplay;
								$adjustParam = array(
										"UserID" => $userIDArr[$i],
										"AcademicYearID" => $academicYearID,
										"BalanceBeforeAdjust" => $final_balance,
										"BalanceAdjustTo" => $BalanceAdjustTo,
										"AdjustedType"  => $user_AdjustedType,
										"Reason" => $Reason,
										"InputBy_UserID" => $val['InputBy_UserID'],
										"ModifiedBy_UserID" => $val['ModifiedBy_UserID'],
										"LessonArrangementID" => $val['LessonArrangementID'],
										"LeaveID" => $val["LeaveID"],
										"DateLeave" => Date('Y-m-d', strtotime($val['TimeStart'])),
										"DateTimeInput" => $val['DateTimeInput']
								);
								$sql = "INSERT INTO INTRANET_SLRS_BALANCE_ADJUST (" . implode(", ", array_keys($adjustParam)) . ", DateTimeModified)";
								$sql .= " VALUES ";
								$sql .= " ('" . implode("', '", array_values($adjustParam)) . "', NOW())";
								if ($forDebug)
								{
									echo "<b>Insert SLRS record (" . $user_AdjustedType. " [" . $final_balance. "])</b><br>";
									echo $sql . "<br><br>";
								}
								else $connection->db_db_query($sql);
							}
						}
						else
						{
							if ($forDebug)
							{
								echo "<b>" . $val['ReasonCode'] . " (no affection)</b><br><br>";
							}
						}
					}
					else if ($val["ArrangedTo_UserID"] == $userIDArr[$i])
					{
						$user_AdjustedType = ($balanceDisplay) ? "ADD" : "REDUCE";
						$BalanceAdjustTo += 1 * $balanceDisplay;
						$Reason .= "__SubstitutionArrangementSubTC__";
						
						$adjustParam = array(
								"UserID" => $userIDArr[$i],
								"AcademicYearID" => $academicYearID,
								"BalanceBeforeAdjust" => $final_balance,
								"BalanceAdjustTo" => $BalanceAdjustTo,
								"AdjustedType"  => $user_AdjustedType,
								"Reason" => $Reason,
								"InputBy_UserID" => $val['InputBy_UserID'],
								"ModifiedBy_UserID" => $val['ModifiedBy_UserID'],
								"LessonArrangementID" => $val['LessonArrangementID'],
								"LeaveID" => $val["LeaveID"],
								"DateLeave" => Date('Y-m-d', strtotime($val['TimeStart'])),
								"DateTimeInput" => $val['DateTimeInput']
						);
						$sql = "INSERT INTO INTRANET_SLRS_BALANCE_ADJUST (" . implode(", ", array_keys($adjustParam)) . ", DateTimeModified)";
						$sql .= " VALUES ";
						$sql .= " ('" . implode("', '", array_values($adjustParam)) . "', NOW())";
						if ($forDebug)
						{
							echo "<b>Insert SLRS record (" . $user_AdjustedType. " [" . $final_balance. "])</b><br>";
							echo $sql . "<br><br>";
						}
						else $connection->db_db_query($sql);
					}
					$latest_balanceAmount = $BalanceAdjustTo;
					$final_balance = $latest_balanceAmount;
				}
			}
			
			/***********************************************************************************/
			$insert_param = array(
					"BalanceAdjustID" => $maxBalanceID,
					"UserID" => IntegerSafe($userIDArr[$i]),
					"AcademicYearID" => $academicYearID,
					"BalanceBeforeAdjust" => $latest_balanceAmount,
					"BalanceAdjustTo" => $final_balance,
					"AdjustedType" => $adjustedType,
					"Reason" => $reason,
					"InputBy_UserID" => $_SESSION["UserID"],
					"ModifiedBy_UserID" => $_SESSION["UserID"],
					"ResetType" => 'DATE_RESET'
			);
			$sql = "INSERT INTO INTRANET_SLRS_BALANCE_ADJUST ( " . implode(', ', array_keys($insert_param)) . ", DateTimeInput, DateTimeModified) VALUES ('" . implode("', '", array_values($insert_param)) . "', NOW(), NOW())";
			if ($forDebug)
			{
				echo "<b>INSERT New Balance Adjustment for this user (" . IntegerSafe($userIDArr[$i]) . ")</b><br>";
				echo $sql . "<br><br>";
			}
			else $connection->db_db_query($sql);
			
			/***********************************************************************************/
			$sql = "UPDATE INTRANET_SLRS_TEACHER SET Balance=".$final_balance*$balanceDisplay.",DateTimeModified=NOW(),ModifiedBy_UserID=".$_SESSION['UserID']." ";
			$sql .="WHERE UserID=".IntegerSafe($userIDArr[$i])."; ";
			
			if ($forDebug)
			{
				echo "<b>Update Teacher Records (" . $adjustedType . " [" . $latest_balanceAmount . " to " . $final_balance. "])</b><br>";
				echo $sql . "<br><br>";
			}
			else $connection->db_db_query($sql);
			/***********************************************************************************/
		}
	}
	else
	{
		/***********************************************************************************/
		$sql = "SELECT BalanceAdjustTo FROM INTRANET_SLRS_BALANCE_ADJUST WHERE UserID='".IntegerSafe($userIDArr[$i])."' AND Deleted_at IS NULL ORDER BY DateTimeModified DESC, BalanceAdjustID DESC LIMIT 1";
		if ($forDebug)
		{
			echo "<b>Get Latest Balance Adjustment for this user (" . IntegerSafe($userIDArr[$i]) . ")</b><br>";
			echo $sql . "<br><br>";
		}
		$query_result = $connection->returnArray($sql);
		$latest_balanceAmount = (isset($query_result[0]['BalanceAdjustTo'])) ? $query_result[0]['BalanceAdjustTo'] : 0;
		// 
		
		switch (strtoupper($adjustedType))
		{
			case "ADD":
			    $reset_balanceAmount = abs($_POST['number']);
				$final_balance = $latest_balanceAmount + $reset_balanceAmount;
				break;
			case "REDUCE":
			    $reset_balanceAmount = abs($_POST['number']);
				$final_balance = $latest_balanceAmount - $reset_balanceAmount;
				break;
			default:
			    $reset_balanceAmount = $_POST['number'];
				$final_balance = $reset_balanceAmount;
				break;
		}
		
		/***********************************************************************************/
		$insert_param = array(
			"BalanceAdjustID" => $maxBalanceID,
			"UserID" => IntegerSafe($userIDArr[$i]),
			"AcademicYearID" => $academicYearID,
			"BalanceBeforeAdjust" => $latest_balanceAmount,
			"BalanceAdjustTo" => $final_balance,
			"AdjustedType" => $adjustedType,
			"Reason" => $reason,
			"InputBy_UserID" => $_SESSION["UserID"],
			"ModifiedBy_UserID" => $_SESSION["UserID"],
			"ResetType" => 'ADJUST'	
		);
		$sql = "INSERT INTO INTRANET_SLRS_BALANCE_ADJUST ( " . implode(', ', array_keys($insert_param)) . ", DateTimeInput, DateTimeModified) VALUES ('" . implode("', '", array_values($insert_param)) . "', NOW(), NOW())";
		if ($forDebug)
		{
			echo "<b>INSERT New Balance Adjustment for this user (" . IntegerSafe($userIDArr[$i]) . ")</b><br>";
			echo $sql . "<br><br>";
		}
		else $connection->db_db_query($sql);
		/***********************************************************************************/
		$sql = "UPDATE INTRANET_SLRS_TEACHER SET Balance=".($final_balance*$balanceDisplay).",DateTimeModified=NOW(),ModifiedBy_UserID=".$_SESSION['UserID']." ";
		$sql .="WHERE UserID=".IntegerSafe($userIDArr[$i])."; ";
		
		if ($forDebug)
		{
			echo "<b>Update Teacher Records (" . $adjustedType . " [" . $latest_balanceAmount . " to " . $final_balance. "])</b><br>";
			echo $sql . "<br><br>";
		}
		else $connection->db_db_query($sql);
		/***********************************************************************************/
	}
	if ($forDebug)
	{
		echo "<hr style='border:0px; border-bottom:1px solid #000;'>";
	}

}
if ($forDebug)
{
	echo "</div>";
}

$returnMsgKey = ($saveSuccess)? 'UpdateSuccess' : 'UpdateUnsuccess';
if (!$forDebug) header('Location: ?task=management'.$slrsConfig['taskSeparator'].'substitute_balance'.$slrsConfig['taskSeparator'].'list&returnMsgKey='.$returnMsgKey);
else exit;

?>