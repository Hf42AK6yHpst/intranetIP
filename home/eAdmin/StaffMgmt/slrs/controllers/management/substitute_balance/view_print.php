<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================

// build header array

// build content data array
$dataAry= array();

$balanceDisplay=$indexVar['libslrs']->getBalanceDisplay();

$name_field = $indexVar['libslrs']->getNameFieldByLang("b.");

$order = ($_POST['order'] == '') ? 1 : $_POST['order'];	// 1 => asc, 0 => desc
$field = ($_POST['field'] == '') ? 0 : $_POST['field'];	

$colAry = array("EnglishName","Balance","YearlyAdd","YearlyReduce","DateTimeModified");

if($order == 1){
	$sql_cond = "order by $colAry[$field], EnglishName asc";
}else{
	$sql_cond = "order by $colAry[$field], EnglishName desc";
}

/**************
$sql = "
	SELECT UserName, Balance * ".$balanceDisplay." as Balance, YearlyAdd, YearlyReduce, DateTimeModified
		FROM(
			SELECT ".$name_field." as UserName, Balance, 0 as YearlyAdd, 0 as YearlyReduce,
			c.MaxDateTimeModified as DateTimeModified,
			b.EnglishName
		From (SELECT UserID,Balance FROM
			INTRANET_SLRS_TEACHER
		) as a
		INNER JOIN(
			SELECT UserID,EnglishName,ChineseName,TitleChinese,TitleEnglish FROM
			".$slrsConfig['INTRANET_USER']." WHERE Teaching <> 'S' AND RecordStatus = '1'
		) as b ON a.UserID=b.UserID
		LEFT JOIN(
			SELECT UserID,DATE_FORMAT(MAX(DateTimeModified),'%Y-%m-%d %H:%i') as MaxDateTimeModified FROM INTRANET_SLRS_BALANCE_ADJUST WHERE Deleted_at IS NULL GROUP BY UserID
		) as c ON a.UserID=c.UserID
	) as a $sql_cond
	";
**************/
if (isset($_POST['keyword'])) {
	$keyword = $_POST['keyword'];
}
$order = ($order == '') ? 1 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 0 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 50 : $numPerPage;

$args['order'] = $order;
$args['field'] = $field;
$args['pageNo'] = $pageNo;
$args['page_size'] = $page_size;
$args['keyword'] = $keyword;
$args['data_only'] = true;

$sql = $indexVar['libslrs']->buildBalanceSummarySQL($args) . " ORDER BY EnglishName ASC";
$dataAry =  $indexVar['libslrs']->returnResultSet($sql);

// $pos = 0;
$x .= "<div align=\"right\">"."\r\n";
$x .= "<input type=\"button\" class=\"formsubbutton\" onclick=\"javascript:window.print();\" name=\"PrintBtn\" id=\"PrintBtn\" value=".$Lang['SLRS']['ReportPrint']." onmouseover=\"this.className='formsubbuttonon'\" onmouseout=\"this.className='formsubbutton'\">";
$x .= "<br/><div id=\"resizeFontLoadingDiv\" style=\"display:none; float:right;\">
	<img src=\"/images/2009a/indicator.gif\"><span class=\"tabletextremark\"><span style=\"color:red;\">".$Lang['SLRS']['ReportProcessing']."</span></span></div>";

$x .= "</div>"."\r\n";
$x .= "<br/><br/>";

$x .= "<table>"."\r\n";
// $x .= "<tr><td colspan=3>".$Lang['SLRS']['ReportYearlyStatisticsofTeacherSubstitution']."</td></tr>"."\r\n";
$x .= "</table><br/>"."\r\n";

$x .= "<table class=\"common_table_list\">"."\r\n";
$x .= "<thead>"."\r\n";
$x .= "<tr>"."\r\n";
$x .= "<th>".$Lang['SLRS']['SubstitutionBalanceDes']['UserName']."</th>"."\r\n";
$x .= "<th>".$Lang['SLRS']['SubstitutionBalanceDes']['Balance']."</th>"."\r\n";
$x .= "<th>".$Lang['SLRS']['SubstitutionBalanceDes']['YearlyAdd']."</th>"."\r\n";
$x .= "<th>".$Lang['SLRS']['SubstitutionBalanceDes']['YearlyReduce']."</th>"."\r\n";
$x .= "<th>".$Lang['SLRS']['SubstitutionBalanceDes']['LastModified']."</th>"."\r\n";
$x .= "</tr>"."\r\n";
$x .= "</thead>"."\r\n";
$x .= "<tbody>"."\r\n";

if (count($dataAry) > 0)
{
	foreach ($dataAry as $rData)
	{
		$x .= "<tr>"."\r\n";
		$x .= "<td>". (empty($rData["UserName"]) ? '--' : $rData["UserName"])."</td>"."\r\n";
		$x .= "<td>". $rData["Balance"] ."</td>"."\r\n";
		$x .= "<td>". $rData["YearlyAdd"] ."</td>"."\r\n";
		$x .= "<td>". $rData["YearlyReduce"] ."</td>"."\r\n";
		$x .= "<td>". $rData["DateTimeModified"] ."</td>"."\r\n";
		$x .= "</tr>"."\r\n";
	}
}
$x .= "</tbody>"."\r\n";
$x .= "</table>"."\r\n";
echo $x;



?>