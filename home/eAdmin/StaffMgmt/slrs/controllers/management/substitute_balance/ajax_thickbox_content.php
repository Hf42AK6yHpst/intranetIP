<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

# Page Title
echo $indexVar['libslrs_ui']->Include_JS_CSS();
//$TAGS_OBJ[] = array($Lang['SLRS']['SubstituteBalance']);
//$indexVar['libslrs_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
//echo $indexVar['libslrs_ui']->Include_Thickbox_JS_CSS();
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$userID = $_POST['userID'];
$yearID = $_POST['yearID'];
# Academic Year
$academicYear = $indexVar['libslrs']->GetAllAcademicYear();
$yearID = ($yearID=="")? Get_Current_Academic_Year_ID():$yearID;
//$yearTerm

$selectedYear = $indexVar['libslrs']->getCurrentYear("name=\"yearID\" onChange=\"reloadForm()\"", $academicYear, $yearID, "", false);

$balanceDisplay=$indexVar['libslrs']->getBalanceDisplay();
$connection = new libgeneralsettings();
$name_field = $indexVar['libslrs']->getNameFieldByLang("b.");
$inputUser_name_field = $indexVar['libslrs']->getNameFieldByLang("d.");
$sql = "SELECT ".$name_field." as userName, a.UserID, BalanceBeforeAdjust * ".$balanceDisplay." as BalanceBeforeAdjust,
		BalanceAdjustTo * ".$balanceDisplay." as BalanceAdjustTo,(BalanceAdjustTo-BalanceBeforeAdjust) * ".$balanceDisplay." as AdjustedValue,
	DateTimeModified,".$inputUser_name_field." as inputUserName, AdjustedType, Reason, DateLeave, LessonArrangementID
	FROM (
		SELECT UserID,Balance as previousBalance, Balance as updatedBalance FROM INTRANET_SLRS_TEACHER WHERE UserID=".$userID." 
	) as a 
	INNER JOIN(
		SELECT UserID,EnglishName,ChineseName,TitleChinese,TitleEnglish FROM
		".$slrsConfig['INTRANET_USER']."
	) as b ON a.UserID=b.UserID
	INNER JOIN(	
		SELECT BalanceAdjustID, UserID, BalanceBeforeAdjust, BalanceAdjustTo, AdjustedType, Reason,
		DATE_FORMAT(DateTimeModified,'%Y-%m-%d %H:%i:%s') as DateTimeModified, InputBy_UserID, DateLeave, LessonArrangementID
		FROM INTRANET_SLRS_BALANCE_ADJUST
		WHERE AcademicYearID='" . $yearID . "' AND Deleted_at IS NULL
	) as c ON a.UserID=c.UserID
	INNER JOIN(
		SELECT UserID,EnglishName,ChineseName,TitleChinese,TitleEnglish FROM
		".$slrsConfig['INTRANET_USER']."
	) as d ON c.InputBy_UserID=d.UserID
	ORDER BY c.DateTimeModified DESC, c.BalanceAdjustID DESC;"
	;
//echo $sql;
$a = $connection->returnArray($sql);
$x = $selectedYear . "<br><br>";

$x .= "<div id='tableContainer'><table class=\"common_table_list_v30\">";
$x .= "<tr class=\"tableTop\">"."\r\n";
$x .= "<th width=\"15%\">".$Lang['SLRS']['SubstitutionBalanceRecordDes']['Date']."</th>"."\r\n";
$x .= "<th width=\"10%\">".$Lang['SLRS']['SubstitutionBalanceRecordDes']['BeforeBalance']."</th>"."\r\n";
$x .= "<th width=\"10%\">".$Lang['SLRS']['SubstitutionBalanceRecordDes']['Adjustment']."</th>"."\r\n";
$x .= "<th width=\"10%\">".$Lang['SLRS']['SubstitutionBalanceRecordDes']['AfterBalance']."</th>"."\r\n";
$x .= "<th width=\"15%\">".$Lang['SLRS']['SubstitutionBalanceRecordDes']['AdjustedType']."</th>"."\r\n";
$x .= "<th width=\"20%\">".$Lang['SLRS']['SubstitutionBalanceRecordDes']['Reason']."</th>"."\r\n";
$x .= "<th width=\"20%\">".$Lang['SLRS']['SubstitutionBalanceRecordDes']['InputUser']."</th>"."\r\n";
$x .= "</tr>"."\r\n";

$_replaceLang = array(
	'__SubstitutionArrangement__' => $Lang['SLRS']['SubstitutionArrangement'],
	'__SubstitutionArrangementCreate__' => $Lang['SLRS']['SubstitutionArrangementCreate'],
	'__SubstitutionArrangementDelete__' => $Lang['SLRS']['SubstitutionArrangementDelete'],
	'__SubstitutionArrangementUpdate__' => $Lang['SLRS']['SubstitutionArrangementUpdate'],
	'__TempSubstitutionArrangementCreate__' => $Lang['SLRS']['TempSubstitutionArrangementCreate'],
	'__TempSubstitutionArrangementDelete__' => $Lang['SLRS']['TempSubstitutionArrangementDelete'],
	'__TempSubstitutionArrangementUpdate__' => $Lang['SLRS']['TempSubstitutionArrangementUpdate'],
	'__SubstitutionArrangementSubTC__' => $Lang['SLRS']['SubstitutionArrangementSubTC']
);

if (count($a) > 0) {
		for($i=0; $i<sizeof($a);$i++){
			$x .= "<tr>"."\r\n";
				$x .= "<td class=\"tablerow\">".$a[$i]['DateTimeModified']."&nbsp;</td>"."\r\n";
				$x .= "<td class=\"tablerow\">".$a[$i]['BalanceBeforeAdjust']." </td>"."\r\n";
				if ($a[$i]['AdjustedType'] == "SET")
				{
				    $x .= "<td class=\"tablerow\">-&nbsp;</td>"."\r\n";
				}
				else 
				{
				    if ($a[$i]['AdjustedValue'] > 0)
				    {
				        $x .= "<td class=\"tablerow\">+".$a[$i]['AdjustedValue'] ."&nbsp;</td>"."\r\n";
				    }
				    else 
				    {
				        $x .= "<td class=\"tablerow\">".$a[$i]['AdjustedValue']."&nbsp;</td>"."\r\n";
				    }
				}
				$x .= "<td class=\"tablerow\">".$a[$i]['BalanceAdjustTo']."&nbsp;</td>"."\r\n";
				$x .= "<td class=\"tablerow\">".getAdjustedType($Lang, $a[$i], $balanceDisplay)."&nbsp;</td>"."\r\n";
				$_reason = str_replace(array_keys($_replaceLang), array_values($_replaceLang), $a[$i]['Reason']);
				
				if ($a[$i]['AdjustedType'] == "SET_YEAR")
				{
					$x .= "<td class=\"tablerow\">";
					$x .= $_reason;
					if (!empty($_reason))
					{
						$x .= "<br>";
					}
					$x .= str_replace("__RESET_DATE__", $a[$i]["DateLeave"], $Lang['SLRS']['SubstitutionBalanceAdjTyp']['SetYearBalanceInfoReason']);
					$x .= "&nbsp;</td>"."\r\n";
				}
				else
				{
					$x .= "<td class=\"tablerow\">".$_reason."&nbsp;</td>"."\r\n";
				}

				$x .= "<td class=\"tablerow\">".$a[$i]['inputUserName']."&nbsp;</td>"."\r\n";
			$x .= "</tr>"."\r\n";
		}
} else {
	$x .= "<tr>"."\r\n";
	$x .= "<td colspan='7' align='center'>"."\r\n";
	$x .= $Lang['SLRS']['ReportGenerationNoRecord'];
	$x .= "</td>"."\r\n";
	$x .= "</tr>"."\r\n";
}
$x .= "</table></div>";
$html['contentTbl'] = $x;

// ============================= Transactional data ==============================
// ============================== Define Button ==============================
// $htmlAry['cancelBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['closeBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['Btn']['Close'], "button", "js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
// ============================== Define Button ==============================
function getAdjustedType($Lang, $info, $balanceDisplay){
	$a = "";
	if($info["AdjustedType"] == "ADD")
	{
		$a = $balanceDisplay ? $Lang['SLRS']['SubstitutionBalanceAdjTyp']['AddBalance'] : $Lang['SLRS']['SubstitutionBalanceAdjTyp']['ReduceBalance'];
	}
	else if($info["AdjustedType"] == "REDUCE")
	{
		$a = $balanceDisplay ? $Lang['SLRS']['SubstitutionBalanceAdjTyp']['ReduceBalance'] : $Lang['SLRS']['SubstitutionBalanceAdjTyp']['AddBalance'];
	}
	else if($info["AdjustedType"] == "SET")
	{
		$a = $Lang['SLRS']['SubstitutionBalanceAdjTyp']['SetBalance'];
	}
	else if($info["AdjustedType"] == "SET_YEAR")
	{
		$a = $Lang['SLRS']['SubstitutionBalanceAdjTyp']['SetYearBalanceInfo'];
	}
	return $a;
}

?>