<?php

// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================

$lexport = new libexporttext();

// build header array
$headerAry = array();

// build content data array
$dataAry= array();

$balanceDisplay=$indexVar['libslrs']->getBalanceDisplay();


$name_field = $indexVar['libslrs']->getNameFieldByLang("b.");

$order = ($_POST['order'] == '') ? 1 : $_POST['order'];	// 1 => asc, 0 => desc
$field = ($_POST['field'] == '') ? 0 : $_POST['field'];

$colAry = array("EnglishName","Balance","YearlyAdd","YearlyReduce","DateTimeModified");

if($order == 1){
	$sql_cond = "order by $colAry[$field], EnglishName asc";
}else{
	$sql_cond = "order by $colAry[$field], EnglishName desc";
}

/*************************
$sql = "
	SELECT UserName,Balance * ".$balanceDisplay." as Balance,YearlyAdd,YearlyReduce,DateTimeModified
		FROM(
			SELECT ".$name_field." as UserName, Balance,0 as YearlyAdd,0 as YearlyReduce,
		c.MaxDateTimeModified as DateTimeModified,
		b.EnglishName
		From (SELECT UserID,Balance FROM
			INTRANET_SLRS_TEACHER
		) as a
		INNER JOIN(
			SELECT UserID,EnglishName,ChineseName,TitleChinese,TitleEnglish FROM
			".$slrsConfig['INTRANET_USER']." WHERE Teaching <> 'S' AND RecordStatus = 1
		) as b ON a.UserID=b.UserID
		LEFT JOIN(
			SELECT UserID,DATE_FORMAT(MAX(DateTimeModified),'%Y-%m-%d %H:%i') as MaxDateTimeModified FROM INTRANET_SLRS_BALANCE_ADJUST WHERE Deleted_at IS NULL GROUP BY UserID
		) as c ON a.UserID=c.UserID
	) as a $sql_cond
	";
************************/
if (isset($_POST['keyword'])) {
	$keyword = $_POST['keyword'];
}
$order = ($order == '') ? 1 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 0 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 50 : $numPerPage;

$args['order'] = $order;
$args['field'] = $field;
$args['pageNo'] = $pageNo;
$args['page_size'] = $page_size;
$args['keyword'] = $keyword;
$args['data_only'] = true;

$sql = $indexVar['libslrs']->buildBalanceSummarySQL($args) . " ORDER BY EnglishName ASC";
//debug_pr($sql);
$dataAry = $indexVar['libslrs']->returnResultSet($sql);

#Column name
$headerAry[0] = $Lang['SLRS']['SubstitutionBalanceDes']['UserName'];
$headerAry[1] = $Lang['SLRS']['SubstitutionBalanceDes']['Balance'];
$headerAry[2] = $Lang['SLRS']['SubstitutionBalanceDes']['YearlyAdd'];
$headerAry[3] = $Lang['SLRS']['SubstitutionBalanceDes']['YearlyReduce'];
$headerAry[4] = $Lang['SLRS']['SubstitutionBalanceDes']['LastModified'];

$index = 0;

if (count($dataAry) > 0){
	foreach ($dataAry as $rData)
	{
		$rows[$index][0] = $rData["UserName"];
		$rows[$index][1] = $rData["Balance"];
		$rows[$index][2] = $rData["YearlyAdd"];
		$rows[$index][3] = $rData["YearlyReduce"];
		$rows[$index][4] = $rData["DateTimeModified"];
		$index++;
	}
}

$exportContent = $lexport->GET_EXPORT_TXT($rows, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=1);
$fileName = 'substitution_balance_report.csv';
$lexport->EXPORT_FILE($fileName, $exportContent, false, false, 'UTF-8');

?>
