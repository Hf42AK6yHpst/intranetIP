<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST, INTRANET_SLRS_LESSON_ARRANGEMENT
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
$general_settings = new libgeneralsettings();
if (isset($_POST['keyword'])) {
	$keyword = $_POST['keyword'];
}
$arrCookies = array();
$arrCookies[] = array("substitute_balance_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}

# Page Title
$TAGS_OBJ[] = array($Lang['SLRS']['SubstituteBalance']);
$indexVar['libslrs_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libslrs_ui']->Include_Thickbox_JS_CSS();

// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array( "EnglishName", "Balance", "YearlyAdd", "YearlyReduce", "DateTimeModified" );

$order = ($order == '') ? 1 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 0 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 50 : $numPerPage;

$args['order'] = $order;
$args['field'] = $field;
$args['pageNo'] = $pageNo;
$args['page_size'] = $page_size;
$args['keyword'] = $keyword;


$li->sql = $indexVar['libslrs']->buildBalanceSummarySQL($args);
$li->no_col = sizeof($li->field_array) + 2;
$li->fieldorder2 = ", EnglishName";
$li->IsColOff = "IP25_table";
$li->count_mode = 1;

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['SLRS']['SubstitutionBalanceDes']['UserName'])."</th>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['SLRS']['SubstitutionBalanceDes']['Balance'])."</th>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['SLRS']['SubstitutionBalanceDes']['YearlyAdd'])."</th>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['SLRS']['SubstitutionBalanceDes']['YearlyReduce'])."</th>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['SLRS']['SubstitutionBalanceDes']['LastModified'])."</th>\n";
$li->column_list .= "<th width='1'>".$li->check("userIdAry[]")."</th>\n";

$htmlAry['dataTable'] = $li->display();

$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('pageNo', 'pageNo', $li->pageNo);
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('order', 'order', $li->order);
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('field', 'field', $li->field);
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('page_size_change', 'page_size_change', '');
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('numPerPage', 'numPerPage', $li->page_size);
$htmlAry['hiddenField'] = $x;


// ============================== Transactional data ==============================
// ============================== Define Button ==============================
$subBtnAry = array();
/*$subBtnAry[] = array('javascript: void(0);', 'Import 1');
$subBtnAry[] = array('javascript: void(0);', 'Import 2');
$subBtnAry[] = array('javascript: void(0);', 'Import 3');*/
/*$btnAry[] = array('import', 'javascript: void(0);', '', $subBtnAry);
$subBtnAry = array();*/
/*$subBtnAry[] = array('javascript: void(0);', 'Export 1');
$subBtnAry[] = array('javascript: void(0);', 'Export 2');
$subBtnAry[] = array('javascript: void(0);', 'Export 3');*/
//$btnAry[] = array('export', 'javascript: void(0);', '', $subBtnAry);
$htmlAry['contentTool'] = $indexVar['libslrs_ui']->Get_Content_Tool_By_Array_v30($btnAry);
$htmlAry['searchBox'] = $indexVar['libslrs_ui']->Get_Search_Box_Div('keyword', $keyword);
$btnAry = array();
$btnAry[] = array('edit', 'javascript: goEdit();');
$htmlAry['dbTableActionBtn'] = $indexVar['libslrs_ui']->Get_DBTable_Action_Button_IP25($btnAry);

// ============================== Define Button ==============================

?>