<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');

# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$type = $_POST['type'];
$teacher = $_POST['teacher'];
$room = $_POST['room'];
$date = $_POST['date'];
$groupID = $_POST['groupID'];
$timeSlotID = $_POST['timeSlotID'];

$connection = new libgeneralsettings();

/***********************************************************************/
// $teacherList = $indexVar['libslrs']->getTeachingTeacherDate($date);
$teacherList = $indexVar['libslrs']->getLessonTeacherByDate($date, $fromExchange=true);
/***********************************************************************/
if ($sys_custom['SLRS']["disallowSubstitutionAtOneDate"]) {
    $teacherList = BuildMultiKeyAssoc($teacherList, 'UserID');
    $teacherList = $indexVar['libslrs']->getExchangedTeachersByDate($date, $teacherList);
    
    $ignore_teacherList = $indexVar['libslrs']->getTeachingTeacherDate($date, TRUE);
    $ignore_teacherList = BuildMultiKeyAssoc($ignore_teacherList, 'UserID');
    
    if (count($teacherList) > 0 && count($ignore_teacherList)) {
        $finalTeacherList = array_diff_key($teacherList, $ignore_teacherList);
        if (count($finalTeacherList) > 0) {
            $finalTeacherList = array_values($finalTeacherList);
        }
    } else {
        $finalTeacherList = array_values($teacherList);
    }
} else {
    $teacherList = $indexVar['libslrs']->getExchangedTeachersByDate($date, $teacherList);
    $finalTeacherList = BuildMultiKeyAssoc($teacherList, 'UserID');
    $finalTeacherList = array_values($finalTeacherList);
}
$arr = array();
$jsonObj = new JSON_obj();
for ($i=0; $i<sizeof($finalTeacherList); $i++){
    $arr[] = array('UserID' => $finalTeacherList[$i]["UserID"], 'Name' => Get_Lang_Selection($finalTeacherList[$i]["ChineseName"], $finalTeacherList[$i]["EnglishName"]));
}
echo $jsonObj->encode($arr);
exit;