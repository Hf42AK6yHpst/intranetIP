<?php
// ============================== Includes files/libraries ==============================
### check access right

# Page Title


// ============================== Includes files/libraries ==============================
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
if($indexVar['libslrs']->isEJ())
{
    include_once($PATH_WRT_ROOT."includes/libschooltimetable.php");
}
else
{
    include_once($PATH_WRT_ROOT."includes/libtimetable.php");
}
if (file_exists($PATH_WRT_ROOT."home/eAdmin/StaffMgmt/slrs/model/cancellesson.php"))
{
    include_once($PATH_WRT_ROOT."home/eAdmin/StaffMgmt/slrs/model/cancellesson.php");
    $CancelLessonModel = new CancelLessonMD();
}
$ltimetable = new Timetable();
$connection = new libgeneralsettings();
// ============================== Transactional data ==============================

$dateArr = array ( $_POST["selecteddate"] );
$sql = $indexVar['libslrs']->getDaySQL($dateArr,"");
$cycleDayRecord = $connection->returnResultSet($sql);

$_record = array(
    "UserID" => $_POST["teacher"],
    "date" => $_POST["selecteddate"],
    "day" => $cycleDayRecord[0]["CycleDay"]
);


$_record["TimetableID"] = $ltimetable->Get_Current_Timetable($_record["date"]);
$_record["TimeslotIDs"] = array();
$strTimeslots = "";
$timeslotIds = $indexVar['libslrs']->getAcademicTimeSlot($_record["date"], $moreDetails=true);
if (count($timeslotIds) > 0)
{
    foreach ($timeslotIds as $timeslot)
    {
        $_record["TimeslotIDs"][] = $timeslot["TimeSlotID"];
    }
}
$_record["timeslotDetail"] = $indexVar['libslrs']->getTimeSlotInfoByIds($_record);


$output = "<br><br>\n";
$output .= "<table class='common_table_list_v30'>\n";
$output .= "<thead>\n";
$output .= "  <tr class=tabletop>\n";
$output .= "      <th style='width:20%'>\n";
$output .= $Lang['SLRS']['CancelLessonDes']['Timeslot'];
$output .= "      </th>\n";
$output .= "      <th style='width:40%'>\n";
$output .= $Lang['SLRS']['CancelLessonDes']['LessonInfo'];
$output .= "      </th>\n";
$output .= "      <th style='width:40%'>\n";
$output .= $Lang['SLRS']['CancelLessonDes']['Remark'];
$output .= "      </th>\n";
$output .= "      <th style='width:1%'>\n";
$output .= "<input type='checkbox' class='select_all'>";
$output .= "      </th>\n";
$output .= "  </tr>\n";
$output .= "</thead>\n";
$output .= "<tbody>\n";
$allowSubmit = false;
if (count($_record["timeslotDetail"]) > 0)
{
    $i = 1;

    foreach ($_record["timeslotDetail"] as $timeslot_detail)
    {
        if ($timeslot_detail["is_arranged"] || $timeslot_detail["is_exchanged"])
        {
            $output .= "<tr style='color:#8f8f8f;'>\n";
        } else {
            $output .= "<tr>\n";
        }
        $output .= "    <td>\n";
        $output .= "" . $timeslot_detail["TimeSlotName"] . "<br>(" . $timeslot_detail["StartTime"] . " - " . $timeslot_detail["EndTime"] . ")";
        $output .= "    </td>\n";
        $output .= "    <td>\n";
        if ($intranet_session_language == "en")
        {
            $output .= "" . $timeslot_detail["EN_DES"] . " > " . $timeslot_detail["ClassTitleEN"];
        } else {
            if ($indexVar['libslrs']->isEJ())
            {
                $output .= convert2unicode("" . $timeslot_detail["CH_DES"] . " > " . $timeslot_detail["ClassTitleB5"]);
            } else {
                $output .= "" . $timeslot_detail["CH_DES"] . " > " . $timeslot_detail["ClassTitleB5"];
            }
        }
        $output .= "    </td>\n";
        $output .= "    <td>\n";
        if ($timeslot_detail["is_arranged"] || $timeslot_detail["is_exchanged"])
        {
            if ($timeslot_detail["is_arranged"])
            {
                $output .= "<span class='text-blue'><i class='fa fa-warning'></i> " . $Lang['SLRS']['CancelLessonDes']['msg']["alreadyArrange"] . "</span>";
            }
            if ($timeslot_detail["is_exchanged"])
            {
                $output .= "<span class='text-blue'><i class='fa fa-warning'></i> " . $Lang['SLRS']['CancelLessonDes']['msg']["alreadyExchange"] . "</span>";
            }
        } else {
            $output .= "<div style='display:block;'>";
            $output .= "<textarea name='cancelLessonRemarks[" . $i . "]' style='display:block; width:300px; max-width:100%; height:50px;' class='cancelLessonRemark' " . $disabled . ">";

            if ($timeslot_detail["is_cancelled"] && !empty($timeslot_detail["CancelRemark"]))
            {
                $output .= str_replace("<br>", "\n", $timeslot_detail["CancelRemark"]);
            }
            $output .= "</textarea>";
            $output .= "</div>";
        }
        $output .= "    </td>\n";
        $output .= "    <td style='text-align:center;'>\n";
        if ($timeslot_detail["is_arranged"] || $timeslot_detail["is_exchanged"])
        {
            $output .= "-";
        } else {
            if ($timeslot_detail["is_cancelled"])
            {
                $checked = " checked";
            } else {
                $checked = "";
            }
            $output .= "<input type='hidden' name='clYearTermID[" . $i . "]' value='" . $timeslot_detail["YearTermID"] . "'>\n";
            $output .= "<input type='hidden' name='clTimetableID[" . $i . "]' value='" . $timeslot_detail["TimetableID"] . "'>\n";
            $output .= "<input type='hidden' name='clTimeSlotID[" . $i . "]' value='" . $timeslot_detail["TimeSlotID"] . "'>\n";
            $output .= "<input type='hidden' name='clSubjectID[" . $i . "]' value='" . $timeslot_detail["SubjectID"] . "'>\n";
            $output .= "<input type='hidden' name='clLocationID[" . $i . "]' value='" . $timeslot_detail["LocationID"] . "'>\n";
            $output .= "<input type='hidden' name='clSubjectGroupID[" . $i . "]' value='" . $timeslot_detail["SubjectGroupID"] . "'>\n";
            $output .= "<input type='checkbox' name='clCancel[" . $i . "]' value='1' class='cancelLessonItem' " . $checked . ">";
        }
        $output .= "    </td>\n";
        $output .= "</tr>\n";
        $i++;
    }
}
$output .= "</tbody>\n";
$output .= "</table>\n";
echo $output;
exit;