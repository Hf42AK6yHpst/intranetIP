<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST, INTRANET_SLRS_LESSON_ARRANGEMENT
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

if($indexVar['libslrs']->isEJ())
{
	include_once($PATH_WRT_ROOT."includes/libschooltimetable.php");
}
else
{
	include_once($PATH_WRT_ROOT."includes/libtimetable.php");
}

if (isset($_POST['keyword'])) {
	$keyword = $_POST['keyword'];
}
$arrCookies = array();
$arrCookies[] = array("lesson_exchange_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}

$ltimetable = new Timetable();
$curTimetableId = $ltimetable->Get_Current_Timetable();


# Page Title
$TAGS_OBJ[] = array($Lang['SLRS']['CancelLesson']);
$indexVar['libslrs_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libslrs_ui']->Include_Thickbox_JS_CSS();

// ============================== Includes files/libraries ==============================
if (file_exists($PATH_WRT_ROOT."home/eAdmin/StaffMgmt/slrs/model/cancellesson.php"))
{
    include_once($PATH_WRT_ROOT."home/eAdmin/StaffMgmt/slrs/model/cancellesson.php");
    $CancelLessonModel = new CancelLessonMD();
}
// ============================== Transactional data ==============================

$order = ($_POST["order"] == '') ? 0 : $_POST["order"];	// 1 => asc, 0 => desc
$field = ($_POST["field"] == '') ? 0 : $_POST["field"];
$pageNo = ($_POST["pageNo"] == '') ? 1 : $_POST["pageNo"];
$page_size = ($_POST["numPerPage"] == '') ? 50 : $_POST["numPerPage"];

//echo $name_field;
$name_field = $indexVar['libslrs']->getNameFieldByLang("iu.");
$condsKeyword = "";
if ($_POST["keyword"] !== '' && $_POST["keyword"]!== null && isset($_POST["keyword"])) {
    $keyword = $_POST["keyword"];
    // use Get_Safe_Sql_Like_Query for " LIKE "
    // use Get_Safe_Sql_Query for " = "
    $condsKeyword = " AND (
							CancelDate LIKE '%".$indexVar['libslrs']->Get_Safe_Sql_Like_Query($keyword)."%'
							OR iu.EnglishName LIKE '%".$indexVar['libslrs']->Get_Safe_Sql_Like_Query($keyword)."%'
                            OR iu.ChineseName LIKE '%".$indexVar['libslrs']->Get_Safe_Sql_Like_Query($keyword)."%'
                            OR TimeSlotName LIKE '%".$indexVar['libslrs']->Get_Safe_Sql_Like_Query($keyword)."%'
							OR LEFT(StartTime, 5) LIKE '%".$indexVar['libslrs']->Get_Safe_Sql_Like_Query($keyword)."%'
                            OR LEFT(EndTime, 5) LIKE '%".$indexVar['libslrs']->Get_Safe_Sql_Like_Query($keyword)."%'
                            OR ClassTitleEN LIKE '%".$indexVar['libslrs']->Get_Safe_Sql_Like_Query($keyword)."%'
                            OR ClassTitleB5 LIKE '%".$indexVar['libslrs']->Get_Safe_Sql_Like_Query($keyword)."%' 
                            OR CancelRemark LIKE '%".$indexVar['libslrs']->Get_Safe_Sql_Like_Query($keyword)."%'
                            OR DateTimeInput LIKE '%".$indexVar['libslrs']->Get_Safe_Sql_Like_Query($keyword)."%'
						  )";
}
$li = new libdbtable2007($field, $order, $pageNo);

$li->field_array = $CancelLessonModel->getListingHeader();
$li->fieldorder2 = ", DateTimeInput DESC";
$li->no_col = sizeof($li->field_array) + 1;
$li->IsColOff = "IP25_table";
$li->count_mode = 1;

$TermInfoArr = getCurrentAcademicYearAndYearTerm();
$academicYearID = $TermInfoArr['AcademicYearID'];
$Year_StartDate = getStartDateOfAcademicYear($academicYearID);
$Year_EndDate = getEndDateOfAcademicYear($academicYearID);

$param = array(
    "user_table" => $slrsConfig['INTRANET_USER'],
    "name_field" => $indexVar['libslrs']->getNameFieldByLang("iu.")
);
$li->sql = $CancelLessonModel->getListingSQL($param) . $condsKeyword;

$args["table"] = "custom_sql";
$args["sql"] = $li->sql;
$args["fieldorder2"] = $li->fieldorder2;
$gridInfo = $indexVar['libslrs']->getTableGridInfo($args);

$gridInfo["sql"] = $li->sql;
$gridInfo["fields"] = $li->field_array;

$sortRev = array();

$args = array(
    "sql" => str_replace(array_keys($sortRev), array_values($sortRev), $li->built_sql()),
    "girdInfo" => $gridInfo
);

$li->custDataset = true;
$listData = $indexVar['libslrs']->getTableGridData($args);
if ($indexVar['libslrs']->isEJ())
{
    if (count($listData) > 0)
    {
        foreach ($listData as $key => $data)
        {
            $listData[$key][3] = convert2unicode($data[3]);
        }
    }
}

$li->custDataSetArr = $listData;
$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='15%'>".$li->column($pos++, $Lang['SLRS']['CancelLessonDes']['CancelDate'])."</th>\n";
$li->column_list .= "<th width='15%'>".$li->column($pos++, $Lang['SLRS']['CancelLessonDes']['TeacherName'])."</th>\n";
$li->column_list .= "<th width='15%'>".$li->column($pos++, $Lang['SLRS']['CancelLessonDes']['Timeslot'])."</th>\n";
$li->column_list .= "<th width='20%'>".$li->column($pos++, $Lang['SLRS']['CancelLessonDes']['LessonInfo'])."</th>\n";
$li->column_list .= "<th width='20%'>".$li->column($pos++, $Lang['SLRS']['CancelLessonDes']['Remark'])."</th>\n";
$li->column_list .= "<th width='15%'>".$li->column($pos++, $Lang['SLRS']['CancelLessonDes']['DateTimeInput'])."</th>\n";
$li->column_list .= "<th width='1'>".$li->check("cancelIdAry[]")."</th>\n";

$htmlAry['dataTable'] = $li->display();
$htmlAry['maxRow'] = $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('rowGroupID', 'rowGroupID', '');
$x = $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('pageNo', 'pageNo', $li->pageNo);
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('order', 'order', $li->order);
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('field', 'field', $li->field);
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('page_size_change', 'page_size_change', '');
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('numPerPage', 'numPerPage', $li->page_size);
$htmlAry['hiddenField'] = $x;

// ============================== Transactional data ==============================

// ============================== Define Button ==============================
$subBtnAry = array();
$btnAry[] = array('new', 'javascript: goAddCancel();', '', $subBtnAry);
/*$btnAry[] = array('import', 'javascript: void(0);', '', $subBtnAry);
 $subBtnAry = array();
 $btnAry[] = array('export', 'javascript: void(0);', '', $subBtnAry);*/
$htmlAry['contentTool'] = $indexVar['libslrs_ui']->Get_Content_Tool_By_Array_v30($btnAry);

$htmlAry['searchBox'] = $indexVar['libslrs_ui']->Get_Search_Box_Div('keyword', $keyword);

$btnAry = array();
$btnAry[] = array('delete', 'javascript: goDelete();',$Lang['SLRS']['CancelLessonDes']['Delete']);
$htmlAry['dbTableActionBtn'] = $indexVar['libslrs_ui']->Get_DBTable_Action_Button_IP25($btnAry);
// ============================== Define Button ==============================

?>