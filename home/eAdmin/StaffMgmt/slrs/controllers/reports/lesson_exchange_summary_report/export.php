<?php

// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

function checkEmpty($value){
    return (($value==""||$value==null)?"-":$value);
}

function convertMultipleRowsIntoOneRow($arr,$fieldName){
    $x = "";
    for($i=0; $i<sizeof($arr);$i++){
        if($i==sizeof($arr)-1){
            $x .= $arr[$i][$fieldName];
        }
        else{
            $x .= $arr[$i][$fieldName].",";
        }
    }
    if(sizeof($arr)==0){
        $x = "''";
    }
    return $x;
}

# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================


$lexport = new libexporttext();

// build header array
$headerAry = array();
/*$headerAry[] = $Lang['SLRS']['ReportPeriod'];
$headerAry[] = $Lang['SLRS']['ReportAssignedTo'];
$headerAry[] = $Lang['SLRS']['ReportClass'];
$headerAry[] = $Lang['SLRS']['ReportSubject'];
$headerAry[] = $Lang['SLRS']['ReportRoom'];
*/

// build content data array
$dataAry = array();

$connection = new libgeneralsettings();
$name_field = $indexVar['libslrs']->getNameFieldByLang($slrsConfig['INTRANET_USER'].".");
$startDate = $_GET["startDate"];
$endDate = $_GET["endDate"];

$sql = "SELECT TeacherA_Date
			FROM INTRANET_SLRS_LESSON_EXCHANGE WHERE TeacherA_Date BETWEEN '".$startDate ."' AND '".$endDate ."'";
$a = $connection->returnResultSet($sql);
for($i=0;$i<sizeof($a);$i++){
	$dateArr[$i]=$a[$i]["TeacherA_Date"];
}

$sql = "SELECT
            GroupID,SequenceNumber,LessonExchangeID,TeacherA_UserID,UserNameA,TeacherB_UserID,UserNameB,TeacherA_Date,TeacherB_Date,
		      itt.TimeSlotName,ClassTitleAEN,ClassTitleAB5,ClassTitleBEN,ClassTitleBB5,NameChiA,NameEngA,NameChiB,NameEngB,
		      stca.SubjectGroupID as SubjectGroupIDA,stcb.SubjectGroupID as SubjectGroupIDB,CodeA,CodeB,itt.TimeSlotID,CycleDay,
              TeacherB_TimeSlotID, TimeSlotNameB
		FROM (
			SELECT
                LessonExchangeID,TeacherA_UserID,TeacherA_Date,TeacherA_SubjectGroupID,TeacherA_SubjectID,TeacherA_LocationID,TeacherA_TimeSlotID,
			    TeacherB_UserID,TeacherB_Date,TeacherB_SubjectGroupID,TeacherB_SubjectID,TeacherB_LocationID,TeacherB_TimeSlotID,GroupID,SequenceNumber
			FROM
                INTRANET_SLRS_LESSON_EXCHANGE
            WHERE
                TeacherA_Date BETWEEN '".$startDate ."' AND '".$endDate ."'
		) as isle
		LEFT JOIN(
			SELECT UserID,".$name_field." as UserNameA FROM ".$slrsConfig['INTRANET_USER']."
		) as iua ON isle.TeacherA_UserID =iua.UserID
		LEFT JOIN(
			SELECT UserID,".$name_field." as UserNameB FROM ".$slrsConfig['INTRANET_USER']."
		) as iub ON isle.TeacherB_UserID =iub.UserID
		LEFT JOIN(
			SELECT TimeSlotID,TimeSlotName FROM INTRANET_TIMETABLE_TIMESLOT
		) as itt ON isle.TeacherA_TimeSlotID=itt.TimeSlotID
		LEFT JOIN(
			SELECT TimeSlotID as TimeSlotIDB,TimeSlotName as TimeSlotNameB FROM INTRANET_TIMETABLE_TIMESLOT
		) as ittB ON isle.TeacherB_TimeSlotID=TimeSlotIDB
		LEFT JOIN(
			SELECT SubjectGroupID,ClassTitleEN as ClassTitleAEN,ClassTitleB5 as ClassTitleAB5 FROM SUBJECT_TERM_CLASS
		) as stca ON isle.TeacherA_SubjectGroupID=stca.SubjectGroupID
		LEFT JOIN(
			SELECT SubjectGroupID,ClassTitleEN as ClassTitleBEN,ClassTitleB5 as ClassTitleBB5 FROM SUBJECT_TERM_CLASS
		) as stcb ON isle.TeacherB_SubjectGroupID=stcb.SubjectGroupID
		LEFT JOIN(
			SELECT LocationID,NameChi as NameChiA,NameEng as NameEngA,Code as CodeA FROM INVENTORY_LOCATION
		) as ila ON isle.TeacherA_LocationID = ila.LocationID
		LEFT JOIN(
			SELECT LocationID,NameChi as NameChiB,NameEng as NameEngB,Code as CodeB FROM INVENTORY_LOCATION
		) as ilb ON isle.TeacherB_LocationID = ilb.LocationID
		LEFT JOIN (".$indexVar['libslrs']->getDaySQL($dateArr,"").") as icd ON isle.TeacherA_Date=icd.RecordDate
		ORDER BY TeacherA_Date DESC,GroupID,SequenceNumber;";
		//echo $sql;
$info = $connection->returnResultSet($sql);

$dataAry[0][0] = $Lang['SLRS']['ReportLessonExchangeSummary'];
$dataAry[1][0] = $Lang['SLRS']['TemporarySubstitutionArrangementDes']['Date'];
$dataAry[1][1] = $startDate;
$dataAry[1][2] = '-';
$dataAry[1][3] = $endDate;

$dataAry[2][0] = "";

$dataAry[3][0] = $Lang['SLRS']['ReportDate'];
$dataAry[3][1] = $Lang['SLRS']['ReportDay'];
$dataAry[3][2] = $Lang['SLRS']['ReportClass'];
$dataAry[3][3] = $Lang['SLRS']['ReportPeriod'];
$dataAry[3][4] = $Lang['SLRS']['ReportTeacher'];
$dataAry[3][5] = "";
$dataAry[3][6] = $Lang['SLRS']['ReportSubject'];
$dataAry[3][7] = "";
$dataAry[3][8] = $Lang['SLRS']['ReportRoom'];
$dataAry[3][9] = "";

$index = 4;

for($i=0;$i<sizeof($info);$i++){
	// $dataAry[$i+$index][0] = (($info[$i]["TeacherA_Date"]==$info[$i]["TeacherB_Date"])?$info[$i]["TeacherA_Date"]:($info[$i]["TeacherA_Date"]." ".$Lang['SLRS']['ReportTo']." ".$info[$i]["TeacherB_Date"]));
    $dataAry[$i+$index][0] = (($info[$i]["TeacherA_Date"]==$info[$i]["TeacherB_Date"])?$info[$i]["TeacherA_Date"]:($info[$i]["TeacherA_Date"]." > ".$info[$i]["TeacherB_Date"]));
	$dataAry[$i+$index][1] = $info[$i]["CycleDay"];
	// $dataAry[$i+$index][2] = $indexVar['libslrs']->getYearClass($info[$i]["SubjectGroupIDA"]);
	$yearClassIDSQL = $indexVar['libslrs']->getYearClassID($info[$i]["TeacherA_Date"]);
	//$yearClass = $yearClassIDSQL[0]["YearClassID"];
	$yearClass = convertMultipleRowsIntoOneRow($yearClassIDSQL,"YearClassID");
	
	$classNameInfo = $indexVar['libslrs']->getClassInfoBySubjectGroupID($info[$i]["SubjectGroupIDA"], $yearClass);
	
	if (empty($classNameInfo) || count($classNameInfo) == 0)
	{
	    $dataAry[$i+$index][2] = $indexVar['libslrs']->getYearClass($info[$i]["SubjectGroupIDA"])."</td>"."\r\n";
	}
	else
	{
	    $str_class_name = "";
	    foreach ($classNameInfo as $_class_key => $_class_val)
	    {
	        if (!empty($str_class_name))
	        {
	            $str_class_name .= ", ";
	        }
	        $str_class_name .= Get_Lang_Selection(
	            (
	                $indexVar['libslrs']->isEJ() ? convert2unicode($_class_val["ClassTitleB5"],1,1) : $_class_val["ClassTitleB5"]
	                ),
	            $_class_val["ClassTitleEN"]
	            );
	    }
	    $dataAry[$i+$index][2] = checkEmpty($str_class_name);
	}
	
	
	// $dataAry[$i+$index][3] = $info[$i]["TimeSlotName"];
	if ($indexVar['libslrs']->isEJ()) {
	    $dataAry[$i+$index][3] = convert2unicode($info[$i]["TimeSlotName"]);
    	if (!empty($info[$i]["TimeSlotNameB"]) && $info[$i]["TimeSlotNameB"] != $info[$i]["TimeSlotName"])
    	{
    	    $dataAry[$i+$index][3] .=  " > " . convert2unicode($info[$i]["TimeSlotNameB"]);
    	}
	} else {
	    $dataAry[$i+$index][3] = $info[$i]["TimeSlotName"];
	    if (!empty($info[$i]["TimeSlotNameB"]) && $info[$i]["TimeSlotNameB"] != $info[$i]["TimeSlotName"])
	    {
	        $dataAry[$i+$index][3] .=  " > " . $info[$i]["TimeSlotNameB"];
	    }
	}
	
	
// 	$dataAry[$i+$index][4] = $info[$i]["UserNameA"];
// 	$dataAry[$i+$index][5] = $info[$i]["UserNameB"];
	$dataAry[$i+$index][4] = $info[$i]["UserNameB"];
	$dataAry[$i+$index][5] = $info[$i]["UserNameA"];
	
	$othersLocationA = $indexVar['libslrs']->getOthersLocation($info[$i]["TimeSlotID"],$info[$i]["SubjectGroupIDA"],$info[$i]["TeacherA_Date"]);
	$othersLocationB = $indexVar['libslrs']->getOthersLocation($info[$i]["TimeSlotID"],$info[$i]["SubjectGroupIDB"],$info[$i]["TeacherB_Date"]);
		
// 	$dataAry[$i+$index][6] = $indexVar['libslrs']->getSubjectName($info[$i]["SubjectGroupIDA"],"");
// 	$dataAry[$i+$index][7] = $indexVar['libslrs']->getSubjectName($info[$i]["SubjectGroupIDB"],"");
	$dataAry[$i+$index][6] = $indexVar['libslrs']->getSubjectName($info[$i]["SubjectGroupIDB"],"");
	$dataAry[$i+$index][7] = $indexVar['libslrs']->getSubjectName($info[$i]["SubjectGroupIDA"],"");
	
// 	$dataAry[$i+$index][8] = (Get_Lang_Selection($info[$i]["NameChiA"],$info[$i]["NameEngA"])?($indexVar['libslrs']->getLocationName($info[$i]["CodeA"],$info[$i]["NameChiA"],$info[$i]["NameEngA"])) :$othersLocationA);
// 	$dataAry[$i+$index][9] = (Get_Lang_Selection($info[$i]["NameChiB"],$info[$i]["NameEngB"])?($indexVar['libslrs']->getLocationName($info[$i]["CodeB"],$info[$i]["NameChiB"],$info[$i]["NameEngB"])):$othersLocationB);
	$dataAry[$i+$index][8] = (Get_Lang_Selection($info[$i]["NameChiB"],$info[$i]["NameEngB"])?($indexVar['libslrs']->getLocationName($info[$i]["CodeB"],$info[$i]["NameChiB"],$info[$i]["NameEngB"])):$othersLocationB);
	$dataAry[$i+$index][9] = (Get_Lang_Selection($info[$i]["NameChiA"],$info[$i]["NameEngA"])?($indexVar['libslrs']->getLocationName($info[$i]["CodeA"],$info[$i]["NameChiA"],$info[$i]["NameEngA"])) :$othersLocationA);
	
}


$exportContent = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=1);

$fileName = 'lesson_exchange_summary_report.csv';
// $lexport->EXPORT_FILE($fileName, $exportContent);
$lexport->EXPORT_FILE($fileName, $exportContent, false, false, 'UTF-8');

?>