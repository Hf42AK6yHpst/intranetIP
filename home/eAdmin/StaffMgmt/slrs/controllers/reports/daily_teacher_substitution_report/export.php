<?php

// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================


$lexport = new libexporttext();

// build header array
$headerAry = array();
/*$headerAry[] = $Lang['SLRS']['ReportPeriod'];
$headerAry[] = $Lang['SLRS']['ReportAssignedTo'];
$headerAry[] = $Lang['SLRS']['ReportClass'];
$headerAry[] = $Lang['SLRS']['ReportSubject'];
$headerAry[] = $Lang['SLRS']['ReportRoom'];
*/

// build content data array
$dataAry = array();

$connection = new libgeneralsettings();
$name_field = $indexVar['libslrs']->getNameFieldByLang("iu.");
$date = $_GET["date"];
$dateArr[0]=$date;
$absentee = $_GET["lthr"];
if ($absentee == "all") {
	$absentee = null;
}
/*
$sql = "SELECT RecordDate,CycleDay,LeaveID,isl.UserID,DateLeave,ReasonCode,Duration,".$name_field." as userName
		FROM(".$indexVar['libslrs']->getDaySQL($dateArr,"").") as icd
		LEFT JOIN(
			SELECT LeaveID,UserID,DateLeave,ReasonCode,Duration FROM INTRANET_SLRS_LEAVE
		) as isl ON icd.RecordDate=isl.DateLeave
		LEFT JOIN(
			SELECT UserID,EnglishName,ChineseName,TitleChinese,TitleEnglish FROM INTRANET_USER
		) as iu ON isl.UserID=iu.UserID	";
$a = $connection->returnResultSet($sql);
*/

$locaitonList = $indexVar['libslrs']->getLocationList();
$locaitonList = $indexVar["libslrs"]->convertArrKeyToID($locaitonList, "LocationID");

$targetIsAssigned = false;
if (isset($_GET["reportTarget"]) && $_GET["reportTarget"]=='AssignedTeacher' && $sys_custom['SLRS']["dailyReportWithAssignedTeacherOption"])
{
    $a = $indexVar["libslrs"]->getAssignedUserByDate($date, $name_field, $absentee);
    $targetIsAssigned = true;
} else {
    $a = $indexVar["libslrs"]->getSubstituteUserByDate($date, $name_field, $absentee);
}
$index = 0;

$dataAry[0][0]=$Lang['SLRS']['ReportDailyTeacherSubstituion'];
$dataAry[1][0]=$date;
$dataAry[2][0]="";

$index = 3;
$row = 3;

for($i=0;$i<sizeof($a);$i++){
	//for($i=0;$i<1;$i++){
	$day = $a[$i]["CycleDay"];
	$userID = $a[$i]["UserID"];
	
	$academicTimeSlot = convertMultipleRowsIntoOneRow($indexVar['libslrs']->getAcademicTimeSlot($date),"TimeSlotID");
	$yearClassIDSQL = $indexVar['libslrs']->getYearClassID($date);
	//$yearClass = $yearClassIDSQL[0]["YearClassID"];
	$yearClass = convertMultipleRowsIntoOneRow($yearClassIDSQL,"YearClassID");
	
	// get timetable information
// 	$sql = "SELECT itt.TimeSlotID,TimeSlotName,LEFT(StartTime,5) as StartTime,LEFT(EndTime,5) as EndTime,itt.DisplayOrder,RoomAllocationID,Day,a.LocationID,a.SubjectGroupID,OthersLocation,
// 		RecordDate,CycleDay,ClassCode,ClassTitleEN,ClassTitleB5,NameChi,NameEng,Code,BarCode,a.UserID,a.EnglishName,a.ChineseName,SubjectTermID,a.SubjectID,islUserID,LeaveID,LessonArrangementID,
// 		ArrangedTo_UserID,ArrangedTo_LocationID,a.YearTermID,".$name_field." as ArrangedTo_UserName,YearClassTitleEN,YearClassTitleB5,stcuUser
// 		FROM (
// 			SELECT TimeSlotID,TimeSlotName,StartTime,EndTime,DisplayOrder FROM INTRANET_TIMETABLE_TIMESLOT WHERE SessionType like '%'
// 				AND TimeSlotID IN (".$academicTimeSlot.")
// 		) as itt
// 		LEFT JOIN(
// 			SELECT RoomAllocationID,Day,itra.TimeSlotID,itra.LocationID,itra.SubjectGroupID,OthersLocation,RecordDate,CycleDay,ClassCode,ClassTitleEN,ClassTitleB5,
// 			NameChi,NameEng,DisplayOrder,Code,BarCode,SubjectClassTeacherID,stct.UserID,EnglishName,ChineseName,
// 			SubjectTermID,st.SubjectID,st.YearTermID,isl.LeaveID,islUserID,DateLeave,LessonArrangementID,ArrangedTo_UserID,ArrangedTo_LocationID
// 			FROM(			
// 				SELECT RoomAllocationID,Day,TimeSlotID,LocationID,SubjectGroupID,OthersLocation FROM INTRANET_TIMETABLE_ROOM_ALLOCATION WHERE Day=".$day." ) as itra
// 				LEFT JOIN (".$indexVar['libslrs']->getDaySQL($dateArr,"").") as icd ON itra.Day=icd.CycleDay
// 				LEFT JOIN (SELECT SubjectGroupID,ClassCode,ClassTitleEN,ClassTitleB5 FROM SUBJECT_TERM_CLASS) as stc ON itra.SubjectGroupID=stc.SubjectGroupID
// 				LEFT JOIN (SELECT LocationID,NameChi,NameEng,DisplayOrder,Code,BarCode FROM INVENTORY_LOCATION) as il ON itra.LocationID = il.LocationID
// 				LEFT JOIN (SELECT SubjectClassTeacherID,SubjectGroupID,UserID FROM SUBJECT_TERM_CLASS_TEACHER WHERE UserID=".IntegerSafe($userID).") as stct ON itra.SubjectGroupID=stct.SubjectGroupID
// 				LEFT JOIN (SELECT UserID,EnglishName,ChineseName FROM ".$slrsConfig['INTRANET_USER'].") as iu ON stct.UserID=iu.UserID
// 				LEFT JOIN (SELECT SubjectTermID,SubjectGroupID,SubjectID,YearTermID FROM SUBJECT_TERM) as st ON itra.SubjectGroupID = st.SubjectGroupID
// 				LEFT JOIN (SELECT LeaveID,UserID as islUserID,DateLeave FROM INTRANET_SLRS_LEAVE WHERE DateLeave='".$date."') as isl ON stct.UserID=isl.islUserID
// 				LEFT JOIN (SELECT LessonArrangementID,UserID,LeaveID,TimeSlotID,LocationID,ArrangedTo_UserID,ArrangedTo_LocationID
// 						FROM INTRANET_SLRS_LESSON_ARRANGEMENT) as isla ON isl.islUserID=isla.UserID AND isl.LeaveID=isla.LeaveID 
// 						AND itra.TimeSlotID=isla.TimeSlotID				
// 				WHERE stct.UserID IS NOT NULL
// 		) as a ON itt.TimeSlotID=a.TimeSlotID
// 		LEFT JOIN(
// 			SELECT UserID,EnglishName,ChineseName,TitleChinese,TitleEnglish FROM ".$slrsConfig['INTRANET_USER']."
// 		) as iu ON a.ArrangedTo_UserID=iu.UserID
// 		LEFT JOIN(
// 			SELECT SubjectGroupID,MIN(UserID) as stcuUser FROM SUBJECT_TERM_CLASS_USER
// 			GROUP BY SubjectGroupID 
// 		) as stcu ON a.SubjectGroupID=stcu.SubjectGroupID
// 		LEFT JOIN(
// 			SELECT UserID,YearClassID FROM YEAR_CLASS_USER WHERE YearClassID IN (".$yearClass.")
// 		) as ycu ON stcu.stcuUser=ycu.UserID
// 		LEFT JOIN(
// 			SELECT YearClassID,ClassTitleEN as YearClassTitleEN,ClassTitleB5 as YearClassTitleB5 FROM YEAR_CLASS		
// 		) as yc ON ycu.YearClassID=yc.YearClassID
// 		ORDER BY DisplayOrder
// 		";
	
// 	$content=$connection->returnResultSet($sql);
	if ($targetIsAssigned)
	{
	    $content = $indexVar['libslrs']->getAssignedTimetableInformation($yearClass, $date, $day, IntegerSafe($userID), $academicTimeSlot, $name_field);
	} else {
	    $content = $indexVar['libslrs']->getSubstituteTimetableInformation($yearClass, $date, $day, IntegerSafe($userID), $academicTimeSlot, $name_field);
	}

	if (count($content) > 0) {
	    if ($targetIsAssigned)
	    {
	        $dataAry[$row][0] = $Lang['SLRS']['DailyTeacherSubstitutionReport']['assignedTeacher'] . ":";
	    } else {
	        $dataAry[$row][0] = $Lang['SLRS']['ReportSubstitutionFor'] . ":";
	    }
		$dataAry[$row][1] = $a[$i]["userName"];
		$dataAry[$row][2] = $Lang['SLRS']['ReportDay'].":";
		$dataAry[$row][3] = $a[$i]["CycleDay"];
		$dataAry[$row][4] = $Lang['SLRS']['ReportDate'].":";
		$dataAry[$row][5] = $date;
		$row++;
		$dataAry[$row][0] = $Lang['SLRS']['SubstitutionArrangement'];
		$row++;
		
		$dataAry[$row][0] = $Lang['SLRS']['ReportPeriod'];
		if ($targetIsAssigned)
		{
		    $dataAry[$row][1] = $Lang['SLRS']['ReportSubstitutionFor'];
		} else {
		    $dataAry[$row][1] = $Lang['SLRS']['ReportAssignedTo'];
		}
		$dataAry[$row][2] = $Lang['SLRS']['ReportClass'];
		$dataAry[$row][3] = $Lang['SLRS']['ReportSubject'];
		$dataAry[$row][4] = $Lang['SLRS']['ReportRoom'];
		$next_col = 5;
		if($display_signature){
		    $dataAry[$row][$next_col] = $Lang['SLRS']['PrintDetailedOption']['Signature'];
		    $next_col++;
		}
		if($display_remark){
		    $dataAry[$row][$next_col] = $Lang['SLRS']['PrintDetailedOption']['Remark'];
		    $next_col++;
		}
		$row++;
		$index = $index +1;
		
		for($j=0;$j<sizeof($content);$j++)
		{
			$classNameInfo = $indexVar['libslrs']->getClassInfoBySubjectGroupID($content[$j]["SubjectGroupID"], $yearClass);
			
			$chi_loc_name = $content[$j]["NameChi"];
			$eng_loc_name = $content[$j]["NameEng"];
			if (!empty($content[$j]['LocationDesc'])) {
				$chi_loc_name .= ' ('.$content[$j]['LocationDesc'].')';
				$eng_loc_name .= ' ('.$content[$j]['LocationDesc'].')';
			}
			
			if ($content[$j]["LocationID"] != $content[$j]["ArrangedTo_LocationID"] && !empty($content[$j]["ArrangedTo_LocationID"]) && isset($locaitonList[$content[$j]["ArrangedTo_LocationID"]])) {
				$chi_loc_name = $locaitonList[$content[$j]["ArrangedTo_LocationID"]]["NameChi"];
				$eng_loc_name = $locaitonList[$content[$j]["ArrangedTo_LocationID"]]["NameEng"];
				if (!empty($locaitonList[$content[$j]["ArrangedTo_LocationID"]]["LocationDesc"])) {
					$chi_loc_name .= ' ('.$locaitonList[$content[$j]["ArrangedTo_LocationID"]]["LocationDesc"].')';
					$eng_loc_name .= ' ('.$locaitonList[$content[$j]["ArrangedTo_LocationID"]]["LocationDesc"].')';
				}
			}
			
			$location_name = checkEmpty((Get_Lang_Selection($chi_loc_name, $eng_loc_name)?($indexVar['libslrs']->getLocationName($content[$j]["Code"], $chi_loc_name, $eng_loc_name)) : $content[$j]["OthersLocaiton"]));
			
			$dataAry[$row][0] = checkEmpty($content[$j]["TimeSlotName"]);
			$dataAry[$row][1] = checkEmpty($content[$j]["ArrangedTo_UserName"]);
			
			// $dataAry[$row][2] = checkEmpty(Get_Lang_Selection($content[$j]["YearClassTitleB5"],$content[$j]["YearClassTitleEN"]));
			
			if (empty($classNameInfo) || count($classNameInfo) == 0)
			{
				$dataAry[$row][2] = checkEmpty(Get_Lang_Selection($content[$j]["YearClassTitleB5"],$content[$j]["YearClassTitleEN"]));
			}
			else
			{
				$str_class_name = "";
				foreach ($classNameInfo as $_class_key => $_class_val)
				{
					if (!empty($str_class_name))
					{
						$str_class_name .= ", ";
					}
					$str_class_name .= Get_Lang_Selection(
							(
									$indexVar['libslrs']->isEJ() ? convert2unicode($_class_val["ClassTitleB5"],1,1) : $_class_val["ClassTitleB5"]
									),
							$_class_val["ClassTitleEN"]
							);
				}
				$dataAry[$row][2] = checkEmpty($str_class_name);
			}
			
			$dataAry[$row][3] = checkEmpty($indexVar['libslrs']->getSubjectName($content[$j]["SubjectGroupID"],"subjectName"));
			// $dataAry[($j+1)+$index][4] = checkEmpty((Get_Lang_Selection($content[$j]["NameChi"],$content[$j]["NameEng"])?($indexVar['libslrs']->getLocationName($content[$j]["Code"],$content[$j]["NameChi"],$content[$j]["NameEng"])) :$$content[$j]["OthersLocaiton"]));
			$dataAry[$row][4] = $location_name;
			$row++;
		}
		$index = sizeof($content)+2+$index;
		$dataAry[$row][0] = "";
		$dataAry[$row][1] = "";
		$dataAry[$row][2] = "";
		$dataAry[$row][3] = "";
		$dataAry[$row][4] = "";
		$row++;

		$dataAry[$row][0] = "";
		$dataAry[$row][1] = "";
		$dataAry[$row][2] = "";
		$dataAry[$row][3] = "";
		$dataAry[$row][4] = "";
		$row++;
	}
}

$notAvailableInfo = $indexVar['libslrs']->getNotAvailableTeacherRecordByDate($date, $date, $absentee);
		
if (count($notAvailableInfo) > 0)
{
	$header = true;
	$dataAry[$row][0]="";
	$index = $index+1;
	foreach ($notAvailableInfo as $i => $info)
	{
		if($header == true){
			$dataAry[$row][0]= $Lang['SLRS']['NotAvailableTeachers'];
			$row++;
			
			$dataAry[$row][0]=$Lang['SLRS']['ReportTC'];
			$dataAry[$row][1]=$Lang['SLRS']['ReportDate'];
			$dataAry[$row][2]=$Lang['SLRS']['NotAvailableTeachersTH']["th_Reasons"];
			$row++;
		}
		
		$dataAry[$row][0]=$info["UserName"];
		$dataAry[$row][1]=$info["DateLeave"];
		$dataAry[$row][2]=$info["ReasonCode"];
		$header = false;
		$row++;
	}
}
$dataAry[$row][0] = "";

$exportContent = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=1);

$fileName = 'daily_teacher_substitution_report.csv';
// $lexport->EXPORT_FILE($fileName, $exportContent);
$lexport->EXPORT_FILE($fileName, $exportContent, false, false, 'UTF-8');


function checkEmpty($value){
	return (($value==""||$value==null)?"-":$value);
}
function convertMultipleRowsIntoOneRow($arr,$fieldName){
	$x = "";
	for($i=0; $i<sizeof($arr);$i++){
		if($i==sizeof($arr)-1){
			$x .= $arr[$i][$fieldName];
		}
		else{
			$x .= $arr[$i][$fieldName].",";
		}
	}
	if(sizeof($arr)==0){
		$x = "''";
	}
	return $x;
}
?>