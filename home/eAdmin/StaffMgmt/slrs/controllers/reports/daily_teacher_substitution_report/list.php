<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST, INTRANET_SLRS_LESSON_ARRANGEMENT
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

$linterface = new interface_html();

# Page Title
$TAGS_OBJ[] = array($Lang['SLRS']['ReportDailyTeacherSubstituion']);
$indexVar['libslrs_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libslrs_ui']->Include_Thickbox_JS_CSS();

// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$date = date("Y-m-d");
$leaveTeacher = $indexVar['libslrs']->getTeachingTeacherDate($date, TRUE);

$classSel = '<select id="leaveTeacherIdIdSel" name="LeaveTeacherIdAry[]" multiple="multiple" size="7">';
if (count($leaveTeacher) > 0) {
	foreach ($leaveTeacher as $kk => $vv) {
		$classSel .= '<option value="' .  $vv["UserID"] . '">' . Get_Lang_Selection($vv["ChineseName"], $vv["EnglishName"]) . '</option>'; 
	}
}
$classSel .= '</select>';
$selectAllBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('leaveTeacherIdIdSel', 1);");

$x ="<table class=\"form_table slrs_form\">"."\r\n";
$x .="<tr>"."\r\n";
$x .= "<td>".$indexVar['libslrs_ui']->RequiredSymbol().$Lang['SLRS']['ReportDate']."</td><td>:</td>"."\r\n";
	$x .= "<td>".$indexVar['libslrs_ui']->GET_DATE_PICKER('datePicker', $date,  $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="goDateChange()",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum")."</td>"."\r\n";
	$x .=  $indexVar['libslrs_ui']->Get_Form_Warning_Msg('datePickerTbEmptyWarnDiv', $Lang['SLRS']['ReportDateWarning'], $Class='warnMsgDiv')."\r\n";
$x .="</tr>"."\r\n";
if ($sys_custom['SLRS']["dailyReportWithAssignedTeacherOption"])
{
    $x .="<tr>"."\r\n";
    $x .= "<td>".$indexVar['libslrs_ui']->RequiredSymbol().$Lang['SLRS']['DailyTeacherSubstitutionReport']["target"]."</td><td>:</td>"."\r\n";
    $x .= "<td>";
    $x .= "<label><input type='radio' id='report_target' onClick='goDateChange();' name='report_target' value='LeaveTeacher' checked>" . $Lang['SLRS']['TemporarySubstitutionArrangementDes']['LeaveTeacher'] . "</label>";
    $x .= "&nbsp;";
    $x .= "<label><input type='radio' id='report_target' onClick='goDateChange();' name='report_target' value='AssignedTeacher'>" . $Lang['SLRS']['DailyTeacherSubstitutionReport']['assignedTeacher'] . "</label>";
    $x .= "</td>"."\r\n";
    $x .="</tr>"."\r\n";
    $teacher_label = $Lang['SLRS']['DailyTeacherSubstitutionReport']["teacher"];
} else {
    $teacher_label = $Lang['SLRS']['TemporarySubstitutionArrangementDes']['LeaveTeacher'];
}
$x .="<tr id='teacherArea'>"."\r\n";
$x .= "<td>".$indexVar['libslrs_ui']->RequiredSymbol().$teacher_label."</td><td>:</td>"."\r\n";
// Multiple selection
	$x .= '<td>'."\r\n";
		$x .= $linterface->Get_MultipleSelection_And_SelectAll_Div($classSel, $selectAllBtn, $SpanID='');
	$x .= '</td>'."\r\n";
$x .= '</tr>'."\r\n";

$x .="</table>"."\r\n";
$x .="<span>".sprintf($Lang['SLRS']['ReportWarning'],$indexVar['libslrs_ui']->RequiredSymbol())."</span>";

$htmlAry['contentTbl'] = $x;
// ============================== Transactional data ==============================

// ============================== Define Button ==============================
$htmlAry['viewBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['SLRS']['ReportGeneration'], "submit", "goRptGen()", 'genRptBtn');
// ============================== Define Button ==============================
// ============================== Detail Print Option Layer ==============================
### Get detailed print / export option layer
$x = '';
$x .= '<div id="detailedOptionDiv" class="selectbox_layer" style="width:120px;">'."\r\n";
$x .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
$x .= '<tbody>'."\r\n";
$x .= '<tr>'."\r\n";
$x .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
$x .= '<a href="javascript:hideDetailedOptionDiv();"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
$x .= '</td>'."\r\n";
$x .= '</tr>'."\r\n";
$x .= '<tr>'."\r\n";
$x .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
$x .= '<table class="form_table_v30">'."\r\n";
$x .= '<colgroup>'."\r\n";
$x .= '<col class="field_c" style="vertical-align:top">'."\r\n";
$x .= '</colgroup>'."\r\n";
$x .= '<tr>'."\r\n";
$x .= '<td>'."\r\n";
$x .= '<input type="checkbox" name="display_signature" value="1" id="display_signature" checked> <label for="display_role">'.$Lang['SLRS']['PrintDetailedOption']['Signature'].'</label><br>'."\r\n";
$x .= '<input type="checkbox" name="display_remark" value="1" id="display_remark" checked> <label for="display_attendance">'.$Lang['SLRS']['PrintDetailedOption']['Remark'].'</label><br>'."\r\n";
$x .= '</td>'."\r\n";
$x .= '</tr>'."\r\n";
$x .= '</table>'."\r\n";
$x .= '</td>'."\r\n";
$x .= '</tr>'."\r\n";
$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
$x .= '<div class="edit_bottom_v30">'."\r\n";
$x .= '<div id="detailedPrintBtnDiv">'."\r\n";
$x .= $linterface->GET_BTN($Lang['Btn']['Print'], "button", "print_timetable();");
$x .= '</div>'."\r\n";
$x .= '</div>'."\r\n";
$x .= '</div>'."\r\n";

$htmlAry['detailedOptionDiv'] = $x;

// ============================== Detail Print Option Layer ==============================
    function cnvUserArrToSelect($arr){
        $oAry = array();
        for($_i=0; $_i<sizeof($arr); $_i++){
            $oAry[$arr[$_i]["UserID"]] = Get_Lang_Selection($arr[$_i]["ChineseName"],$arr[$_i]["EnglishName"]);
        }
        return $oAry;;
    }


?>