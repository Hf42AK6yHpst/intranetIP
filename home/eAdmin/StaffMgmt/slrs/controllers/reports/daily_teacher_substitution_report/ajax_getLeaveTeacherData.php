<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");

# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================

$lmc = new libmessagecenter();
$connection = new libgeneralsettings();

define("CUS_PHP_COL", "\r\n");
$appType = $eclassAppConfig['appType']['Teacher'];

$name_field = $indexVar['libslrs']->getNameFieldByLang("iu.");
$date = $_POST["date"];
$absentee = $_POST["lthr"];
if ($absentee == "all") {
	$absentee = null;
} 
$dateArr[0]=$date;

/*
$sql = "SELECT RecordDate,CycleDay,LeaveID,isl.UserID,DateLeave,ReasonCode,Duration,".$name_field." as userName
		FROM(".$indexVar['libslrs']->getDaySQL($dateArr,"").") as icd
		LEFT JOIN(
			SELECT LeaveID,UserID,DateLeave,ReasonCode,Duration FROM INTRANET_SLRS_LEAVE
		) as isl ON icd.RecordDate=isl.DateLeave
		LEFT JOIN(
			SELECT UserID,EnglishName,ChineseName,TitleChinese,TitleEnglish FROM INTRANET_USER
		) as iu ON isl.UserID=iu.UserID	";
$a = $connection->returnResultSet($sql);
*/
if (isset($_POST["reportTarget"]) && $_POST["reportTarget"]=='AssignedTeacher' && $sys_custom['SLRS']["dailyReportWithAssignedTeacherOption"])
{
    $a = $indexVar["libslrs"]->getAssignedUserByDate($date, $name_field);
} else {
    $a = $indexVar["libslrs"]->getSubstituteUserByDate($date, $name_field);
}

$a = BuildMultiKeyAssoc($a, "UserID");

$args = array(
			"result" => "success",
			"date" => $date,
			"teacher" => $absentee,
			"teacherObj" => $a 
		);
$jsonObj = new JSON_obj();
echo $jsonObj->encode($args);
exit;

function checkEmpty($value){
	return (($value==""||$value==null)?"-":$value);
}

function convertMultipleRowsIntoOneRow($arr,$fieldName){
	$x = "";
	for($i=0; $i<sizeof($arr);$i++){
		if($i==sizeof($arr)-1){
			$x .= $arr[$i][$fieldName];
		}
		else{
			$x .= $arr[$i][$fieldName].",";
		}
	}
	if(sizeof($arr)==0){
		$x = "''";
	}
	return $x;
}
?>