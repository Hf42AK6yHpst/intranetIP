<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$connection = new libgeneralsettings();
$name_field = $indexVar['libslrs']->getNameFieldByLang("iu.");
$date = $_GET["date"];
$absentee = $_GET["lthr"];
if ($absentee == "all") {
	$absentee = null;
}
$display_signature = IntegerSafe($_GET['display_signature']);
$display_remark = IntegerSafe($_GET['display_remark']);
$dateArr[0]=$date;
/*
$sql = "SELECT RecordDate,CycleDay,LeaveID,isl.UserID,DateLeave,ReasonCode,Duration,".$name_field." as userName
		FROM(".$indexVar['libslrs']->getDaySQL($dateArr,"").") as icd
		LEFT JOIN(
			SELECT LeaveID,UserID,DateLeave,ReasonCode,Duration FROM INTRANET_SLRS_LEAVE
		) as isl ON icd.RecordDate=isl.DateLeave
		LEFT JOIN(
			SELECT UserID,EnglishName,ChineseName,TitleChinese,TitleEnglish FROM INTRANET_USER
		) as iu ON isl.UserID=iu.UserID	";
$a = $connection->returnResultSet($sql);
//echo $sql;
*/

$targetIsAssigned = false;
if (isset($_GET["reportTarget"]) && $_GET["reportTarget"]=='AssignedTeacher' && $sys_custom['SLRS']["dailyReportWithAssignedTeacherOption"])
{
    $a = $indexVar["libslrs"]->getAssignedUserByDate($date, $name_field, $absentee);
    $targetIsAssigned = true;
} else {
    $a = $indexVar["libslrs"]->getSubstituteUserByDate($date, $name_field, $absentee);
}

//$x .= "<link href=\"/templates/2009a/css/content_25.css\" rel=\"stylesheet\" type=\"text/css\">";
$x .= "<div align=\"right\">"."\r\n";	
	$x .= "<input type=\"button\" class=\"formsubbutton\" onclick=\"javascript:window.print();\" name=\"PrintBtn\" id=\"PrintBtn\" value=".$Lang['SLRS']['ReportPrint']." onmouseover=\"this.className='formsubbuttonon'\" onmouseout=\"this.className='formsubbutton'\">";
	$x .= "<br/><div id=\"resizeFontLoadingDiv\" style=\"display:none; float:right;\">
	<img src=\"/images/2009a/indicator.gif\"><span class=\"tabletextremark\"><span style=\"color:red;\">".$Lang['SLRS']['ReportProcessing']."</span></span></div>";
$x .= "</div>"."\r\n";

$x .= "<table>"."\r\n";
$x .= "<tr><td colspan=3>".$Lang['SLRS']['ReportDailyTeacherSubstituion']."</td></tr>"."\r\n";
$x .= "<tr><td colspan=3>".$date."</td></tr></table><br/>"."\r\n";

$locaitonList = $indexVar['libslrs']->getLocationList();
$locaitonList = $indexVar["libslrs"]->convertArrKeyToID($locaitonList, "LocationID");

for($i=0;$i<sizeof($a);$i++){
//for($i=0;$i<1;$i++){
	$day = $a[$i]["CycleDay"];
	$userID = $a[$i]["UserID"];

	$academicTimeSlot = convertMultipleRowsIntoOneRow($indexVar['libslrs']->getAcademicTimeSlot($date),"TimeSlotID");
	$yearClassIDSQL = $indexVar['libslrs']->getYearClassID($date);
	//$yearClass = $yearClassIDSQL[0]["YearClassID"];
	$yearClass = convertMultipleRowsIntoOneRow($yearClassIDSQL,"YearClassID");
	
	/*
	// get timetable information
	$sql = "SELECT itt.TimeSlotID,TimeSlotName,LEFT(StartTime,5) as StartTime,LEFT(EndTime,5) as EndTime,itt.DisplayOrder,RoomAllocationID,Day,a.LocationID,a.SubjectGroupID,OthersLocation,
		RecordDate,CycleDay,ClassCode,ClassTitleEN,ClassTitleB5,NameChi,NameEng,Code,BarCode,a.UserID,a.EnglishName,a.ChineseName,SubjectTermID,a.SubjectID,islUserID,LeaveID,LessonArrangementID,
		ArrangedTo_UserID,ArrangedTo_LocationID,a.YearTermID,".$name_field." as ArrangedTo_UserName,YearClassTitleEN,YearClassTitleB5,stcuUser
		FROM (
			SELECT TimeSlotID,TimeSlotName,StartTime,EndTime,DisplayOrder FROM INTRANET_TIMETABLE_TIMESLOT WHERE SessionType like '%'
				AND TimeSlotID IN (".$academicTimeSlot.")
		) as itt
		LEFT JOIN(
			SELECT RoomAllocationID,Day,itra.TimeSlotID,itra.LocationID,itra.SubjectGroupID,OthersLocation,RecordDate,CycleDay,ClassCode,ClassTitleEN,ClassTitleB5,
			NameChi,NameEng,DisplayOrder,Code,BarCode,SubjectClassTeacherID,stct.UserID,EnglishName,ChineseName,
			SubjectTermID,st.SubjectID,st.YearTermID,isl.LeaveID,islUserID,DateLeave,LessonArrangementID,ArrangedTo_UserID,ArrangedTo_LocationID
			FROM(			
				SELECT RoomAllocationID,Day,TimeSlotID,LocationID,SubjectGroupID,OthersLocation FROM INTRANET_TIMETABLE_ROOM_ALLOCATION WHERE Day=".$day." ) as itra
				LEFT JOIN (".$indexVar['libslrs']->getDaySQL($dateArr,"").") as icd ON itra.Day=icd.CycleDay
				LEFT JOIN (SELECT SubjectGroupID,ClassCode,ClassTitleEN,ClassTitleB5 FROM SUBJECT_TERM_CLASS) as stc ON itra.SubjectGroupID=stc.SubjectGroupID
				LEFT JOIN (SELECT LocationID,NameChi,NameEng,DisplayOrder,Code,BarCode FROM INVENTORY_LOCATION) as il ON itra.LocationID = il.LocationID
				LEFT JOIN (SELECT SubjectClassTeacherID,SubjectGroupID,UserID FROM SUBJECT_TERM_CLASS_TEACHER WHERE UserID=".IntegerSafe($userID).") as stct ON itra.SubjectGroupID=stct.SubjectGroupID
				LEFT JOIN (SELECT UserID,EnglishName,ChineseName FROM INTRANET_USER) as iu ON stct.UserID=iu.UserID
				LEFT JOIN (SELECT SubjectTermID,SubjectGroupID,SubjectID,YearTermID FROM SUBJECT_TERM) as st ON itra.SubjectGroupID = st.SubjectGroupID
				LEFT JOIN (SELECT LeaveID,UserID as islUserID,DateLeave FROM INTRANET_SLRS_LEAVE WHERE DateLeave='".$date."') as isl ON stct.UserID=isl.islUserID
				LEFT JOIN (SELECT LessonArrangementID,UserID,LeaveID,TimeSlotID,LocationID,ArrangedTo_UserID,ArrangedTo_LocationID
						FROM INTRANET_SLRS_LESSON_ARRANGEMENT) as isla ON isl.islUserID=isla.UserID AND isl.LeaveID=isla.LeaveID 
						AND itra.TimeSlotID=isla.TimeSlotID				
				WHERE stct.UserID IS NOT NULL
		) as a ON itt.TimeSlotID=a.TimeSlotID
		LEFT JOIN(
			SELECT UserID,EnglishName,ChineseName,TitleChinese,TitleEnglish FROM INTRANET_USER
		) as iu ON a.ArrangedTo_UserID=iu.UserID
		LEFT JOIN(
			SELECT SubjectGroupID,MIN(UserID) as stcuUser FROM SUBJECT_TERM_CLASS_USER
			GROUP BY SubjectGroupID 
		) as stcu ON a.SubjectGroupID=stcu.SubjectGroupID
		LEFT JOIN(
			SELECT UserID,YearClassID FROM YEAR_CLASS_USER WHERE YearClassID IN (".$yearClass.")
		) as ycu ON stcu.stcuUser=ycu.UserID
		LEFT JOIN(
			SELECT YearClassID,ClassTitleEN as YearClassTitleEN,ClassTitleB5 as YearClassTitleB5 FROM YEAR_CLASS		
		) as yc ON ycu.YearClassID=yc.YearClassID
		ORDER BY DisplayOrder
		";
	//echo $sql;
	$content=$connection->returnResultSet($sql);
	*/
	$messageTitle = $Lang['SLRS']['ReportGenerationSLRSNoticationChi'] . " [" . $date . "] " . $Lang['SLRS']['ReportGenerationSLRSNoticationEN'] . " [" . $date . "]";
	if ($targetIsAssigned)
	{
	    $content = $indexVar['libslrs']->getAssignedTimetableInformation($yearClass, $date, $day, IntegerSafe($userID), $academicTimeSlot, $name_field);
	} else {
	    $content = $indexVar['libslrs']->getSubstituteTimetableInformation($yearClass, $date, $day, IntegerSafe($userID), $academicTimeSlot, $name_field);
	}
	
	if (count($content) > 0) {
	//	$x .= "<table><tr><td>".$Lang['SLRS']['ReportSubstitutionFor'].": ".$a[$i]["userName"]."</td><td>".$Lang['SLRS']['ReportDay'].": ".$a[$i]["CycleDay"]."</td>"."\r\n";
	//	$x .= "<td>".$Lang['SLRS']['ReportDate'].": ".$date."</td></tr></table><br/>"."\r\n";
		if ($sys_custom['SLRS']["custom_library"] == "ppaulvi") {
			$x .= "<div>";
		} else {
			$x .= "<div style='page-break-after: always;'>";
		}
		if ($targetIsAssigned)
		{
		    $x .= $indexVar['libslrs_ui']->GET_NAVIGATION2_IP25($Lang['SLRS']['DailyTeacherSubstitutionReport']['assignedTeacher'].": ".$a[$i]["userName"]);
		} else {
		    $x .= $indexVar['libslrs_ui']->GET_NAVIGATION2_IP25($Lang['SLRS']['ReportSubstitutionFor'].": ".$a[$i]["userName"]);
		}
		
		$x .= '<table class="form_table_v30">'."\r\n";
		$x .= '<tr><td class="field_title">'.$Lang['SLRS']['ReportDay'].'</td><td>'.$a[$i]["CycleDay"].'</td></tr>'."\r\n";
		$x .= '<tr><td class="field_title">'.$Lang['SLRS']['ReportDate'].'</td><td>'.$date.'</td></tr>'."\r\n";
		$x .= '</table>'."\r\n";
		$x .= "<table class=\"common_table_list\">"."\r\n";
		$x .= "<thead>"."\r\n";
		$x .= "<tr>"."\r\n";
		$x .= "<th style='min-width:15%;'>".$Lang['SLRS']['ReportPeriod']."</th>"."\r\n";
		if ($targetIsAssigned)
		{
		    $x .= "<th style='width:15%;'>".$Lang['SLRS']['ReportSubstitutionFor']."</th>"."\r\n";
		} else {
		    $x .= "<th style='width:15%;'>".$Lang['SLRS']['ReportAssignedTo']."</th>"."\r\n";
		}
		$x .= "<th style='min-width:15%;'>".$Lang['SLRS']['ReportClass']."</th>"."\r\n";
		$x .= "<th style='min-width:15%;'>".$Lang['SLRS']['ReportSubject']."</th>"."\r\n";
		$x .= "<th style='min-width:20%;'>".$Lang['SLRS']['ReportRoom']."</th>"."\r\n";
		if($display_signature){
		    $x .= "<th style='width:15%;'>".$Lang['SLRS']['PrintDetailedOption']['Signature']."</th>"."\r\n";
		}
		if($display_remark){
		    $x .= "<th style='width:25%;'>".$Lang['SLRS']['PrintDetailedOption']['Remark']."</th>"."\r\n";
		}
		$x .= "</tr>"."\r\n";
		$x .= "</thead>"."\r\n";
		$x .= "<tbody>"."\r\n";
		
		for($j=0;$j<sizeof($content);$j++){
			
			$classNameInfo = $indexVar['libslrs']->getClassInfoBySubjectGroupID($content[$j]["SubjectGroupID"], $yearClass);
			
			if ($content[$j]["ArrangedTo_UserID"] > 0) {
				$hasRecord = true;
			}
			$x .= "<tr>"."\r\n";
			$x .= "<td>".checkEmpty($content[$j]["TimeSlotName"])."</td>";
			if (empty($content[$j]["ArrangedTo_UserID"])) {
				$x .= "<td>". $Lang['SLRS']['SubstitutionArrangementDes']['notArranged'] ."</td>"."\r\n";
			} else if ($content[$j]["ArrangedTo_UserID"] < 0) {
				$x .= "<td>". $Lang['SLRS']['SubstitutionArrangementDes']['ignoreThisRec'] ."</td>"."\r\n";
			} else {
				$x .= "<td>".checkEmpty($content[$j]["ArrangedTo_UserName"])."</td>";
			}
			// $x .= "<td>".checkEmpty(Get_Lang_Selection(($indexVar['libslrs']->isEJ() ? convert2unicode($content[$j]["YearClassTitleB5"],1,1) : $content[$j]["YearClassTitleB5"]),$content[$j]["YearClassTitleEN"]))."</td>"."\r\n";
			
			if (empty($classNameInfo) || count($classNameInfo) == 0)
			{
				$x .= "<td>".checkEmpty(Get_Lang_Selection(($indexVar['libslrs']->isEJ() ? convert2unicode($content[$j]["YearClassTitleB5"],1,1) : $content[$j]["YearClassTitleB5"]),$content[$j]["YearClassTitleEN"]))."</td>"."\r\n";
			}
			else
			{
				$str_class_name = "";
				foreach ($classNameInfo as $_class_key => $_class_val)
				{
					if (!empty($str_class_name))
					{
						$str_class_name .= ", ";
					}
					$str_class_name .= Get_Lang_Selection(
							(
									$indexVar['libslrs']->isEJ() ? convert2unicode($_class_val["ClassTitleB5"],1,1) : $_class_val["ClassTitleB5"]
									),
							$_class_val["ClassTitleEN"]
							);
				}
				$x .= "<td>".checkEmpty($str_class_name) . "</td>"."\r\n";
			}
			
			//x .= "<td>".checkEmpty(Get_Lang_Selection($content[$j]["ClassTitleB5"],$content[$j]["ClassTitleEN"]))."</td><td>".checkEmpty(Get_Lang_Selection($content[$j]["NameChi"],$content[$j]["NameEng"]))."</td>"."\r\n";
			$x .= "<td>".checkEmpty($indexVar['libslrs']->getSubjectName($content[$j]["SubjectGroupID"],"subjectName"))."</td>";
			// $x .= "<td>".checkEmpty((Get_Lang_Selection($content[$j]["NameChi"],$content[$j]["NameEng"])?($indexVar['libslrs']->getLocationName($content[$j]["Code"],$content[$j]["NameChi"],$content[$j]["NameEng"])) : $content[$j]["OthersLocaiton"]))."</td>"."\r\n";
			
			$chi_loc_name = $content[$j]["NameChi"];
			$eng_loc_name = $content[$j]["NameEng"];
			if (!empty($content[$j]['LocationDesc'])) {
				$chi_loc_name .= ' ('.$content[$j]['LocationDesc'].')';
				$eng_loc_name .= ' ('.$content[$j]['LocationDesc'].')';
			}
			
			if ($content[$j]["LocationID"] != $content[$j]["ArrangedTo_LocationID"] && !empty($content[$j]["ArrangedTo_LocationID"]) && isset($locaitonList[$content[$j]["ArrangedTo_LocationID"]])) {
				$chi_loc_name = $locaitonList[$content[$j]["ArrangedTo_LocationID"]]["NameChi"];
				$eng_loc_name = $locaitonList[$content[$j]["ArrangedTo_LocationID"]]["NameEng"];
				if (!empty($locaitonList[$content[$j]["ArrangedTo_LocationID"]]["LocationDesc"])) {
					$chi_loc_name .= ' ('.$locaitonList[$content[$j]["ArrangedTo_LocationID"]]["LocationDesc"].')';
					$eng_loc_name .= ' ('.$locaitonList[$content[$j]["ArrangedTo_LocationID"]]["LocationDesc"].')';
				}
			}
			
			$location_name = checkEmpty((Get_Lang_Selection($chi_loc_name, $eng_loc_name)?($indexVar['libslrs']->getLocationName($content[$j]["Code"], $chi_loc_name, $eng_loc_name)) : $content[$j]["OthersLocaiton"]));
			
			$x .= "<td>". $location_name ."</td>"."\r\n";
			if($display_signature){
			    $x .= "<td></td>"."\r\n";
			}
			if($display_remark){
			    $x .= "<td></td>"."\r\n";
			}
			$x .= "</tr>"."\r\n";
		}
		$x .= "</tbody>"."\r\n";
		$x .= "</table><br/><br/>"."\r\n";
		$x .= "</div>";
	}
}
$notAvailableInfo = $indexVar['libslrs']->getNotAvailableTeacherRecordByDate($date, $date, $absentee);
if (count($notAvailableInfo) > 0) {
	$x .= $indexVar['libslrs_ui']->GET_NAVIGATION2_IP25($Lang['SLRS']['NotAvailableTeachers']);
	$x .= "<table class=\"common_table_list\">"."\r\n";
	$x .= "<tr><th style='width: 20%'>".$Lang['SLRS']['ReportTC']."</th>";
	$x .= "<th style='width: 40%'>".$Lang['SLRS']['ReportDate']."</th>";
	$x .= "<th>".$Lang['SLRS']['NotAvailableTeachersTH']["th_Reasons"]."</th></tr>";
	foreach ($notAvailableInfo as $kk => $vv) {
		$x .= "<tr>";
		$x .= "<td>" . $vv["UserName"]. "</td>";
		$x .= "<td>" . $vv["DateLeave"]. "</td>";
		$x .= "<td>" . nl2br($vv["ReasonCode"]). "</td>";
		$x .= "</tr>";
	}
	$x .= "</table>";
}
$htmlAry['contentTbl'] = $x;


function checkEmpty($value){
	return (($value==""||$value==null)?"-":$value);
}
function convertMultipleRowsIntoOneRow($arr,$fieldName){
	$x = "";
	for($i=0; $i<sizeof($arr);$i++){
		if($i==sizeof($arr)-1){
			$x .= $arr[$i][$fieldName];
		}
		else{
			$x .= $arr[$i][$fieldName].",";
		}
	}
	if(sizeof($arr)==0){
		$x = "''";
	}
	return $x;
}
?>