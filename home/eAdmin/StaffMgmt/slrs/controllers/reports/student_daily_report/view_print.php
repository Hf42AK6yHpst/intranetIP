<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");


# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================

$connection = new libgeneralsettings();
$name_field = $indexVar['libslrs']->getNameFieldByLang($slrsConfig['INTRANET_USER'].".");

$startDate = $_GET["startDate"];
$endDate = $_GET["endDate"];

$sql = "SELECT TeacherA_Date
			FROM INTRANET_SLRS_LESSON_EXCHANGE WHERE TeacherA_Date like '".$startDate ."'
		UNION
		SELECT DATE_FORMAT(TimeStart,'%Y-%m-%d') as TeacherA_Date
			FROM 
		INTRANET_SLRS_LESSON_ARRANGEMENT WHERE DATE_FORMAT(TimeStart,'%Y-%m-%d') like '".$startDate ."'					
		";
$a = $connection->returnResultSet($sql);
for($i=0;$i<sizeof($a);$i++){
	$dateArr[$i]=$a[$i]["TeacherA_Date"];
}

$sql = "SELECT
			GroupID, SequenceNumber, LessonRemarks, LessonExchangeID, LessonArrangementID, TeacherA_UserID, UserNameA, TeacherB_UserID, UserNameB, TeacherA_Date, TeacherA_LocationID,
			TeacherB_Date, TeacherB_LocationID, CycleDay, TimeSlotName, ClassTitleAEN, ClassTitleAB5, ClassTitleBEN, ClassTitleBB5,
			NameChiA, NameEngA, NameChiB, NameEngB, CycleDay, TeacherA_SubjectGroupID as SubjectGroupIDA,
			TeacherB_SubjectGroupID as SubjectGroupIDB, CodeA, CodeB, laID
		FROM (
			SELECT
				0 as LessonArrangementID, LessonExchangeID, TeacherA_UserID, TeacherA_Date, TeacherA_SubjectGroupID, TeacherA_SubjectID, TeacherA_LocationID, TeacherA_TimeSlotID,
				TeacherB_UserID, TeacherB_Date, TeacherB_SubjectGroupID, TeacherB_SubjectID, TeacherB_LocationID, TeacherB_TimeSlotID, GroupID,
				SequenceNumber, LessonRemarks, laID
			FROM
				INTRANET_SLRS_LESSON_EXCHANGE
			WHERE
				TeacherB_Date = '".$startDate ."' AND (laID IS NULL)
			UNION
				SELECT
					LessonArrangementID as LessonArrangementID, 0 as LessonExchangeID, UserID as TeacherA_UserID, DATE_FORMAT(TimeStart,'%Y-%m-%d') as TeacherA_Date,
					SubjectGroupID as TeacherA_SubjectGroupID, SubjectID as TeacherA_SubjectID, LocationID as TeacherA_LocationID,
					TimeSlotID as TeacherA_TimeSlotID,ArrangedTo_UserID as TeacherB_UserID, DATE_FORMAT(TimeStart,'%Y-%m-%d') as TeacherB_Date,
					SubjectGroupID as TeacherB_SubjectGroupID, SubjectID as TeacherB_SubjectID, ArrangedTo_LocationID as TeacherB_LocationID,
					TimeSlotID as TeacherB_TimeSlotID, NULL as GroupID, 1 as SequenceNUmber, LessonRemarks, Null as laID
				FROM
					INTRANET_SLRS_LESSON_ARRANGEMENT
				WHERE
					DATE_FORMAT(TimeStart,'%Y-%m-%d')='".$startDate ."'
		) as isle
		LEFT JOIN(
			SELECT
				UserID,".$name_field." as UserNameA
			FROM
				".$slrsConfig['INTRANET_USER']."
		) as iua ON isle.TeacherA_UserID =iua.UserID
		LEFT JOIN(
			SELECT
				UserID,".$name_field." as UserNameB
			FROM
				".$slrsConfig['INTRANET_USER']."
		) as iub ON isle.TeacherB_UserID =iub.UserID
		LEFT JOIN(
			SELECT
				TimeSlotID, TimeSlotName
			FROM
				INTRANET_TIMETABLE_TIMESLOT
		) as itt ON isle.TeacherA_TimeSlotID=itt.TimeSlotID
		LEFT JOIN(
			SELECT
				SubjectGroupID, ClassTitleEN as ClassTitleAEN, ClassTitleB5 as ClassTitleAB5
			FROM
				SUBJECT_TERM_CLASS
		) as stca ON isle.TeacherA_SubjectGroupID=stca.SubjectGroupID
		LEFT JOIN(
			SELECT
				SubjectGroupID, ClassTitleEN as ClassTitleBEN, ClassTitleB5 as ClassTitleBB5
			FROM
				SUBJECT_TERM_CLASS
		) as stcb ON isle.TeacherB_SubjectGroupID=stcb.SubjectGroupID
		LEFT JOIN(
			SELECT
				LocationID, NameChi as NameChiA, NameEng as NameEngA, Code as CodeA
			FROM
				INVENTORY_LOCATION
		) as ila ON isle.TeacherA_LocationID = ila.LocationID
		LEFT JOIN(
			SELECT
				LocationID, NameChi as NameChiB, NameEng as NameEngB, Code as CodeB
			FROM
				INVENTORY_LOCATION
		) as ilb ON isle.TeacherB_LocationID = ilb.LocationID
		LEFT JOIN (
			".$indexVar['libslrs']->getDaySQL($dateArr,"")."
		) as icd ON isle.TeacherA_Date=icd.RecordDate
		ORDER BY TeacherA_Date DESC,GroupID,SequenceNumber;";
		//echo $sql;
	$info = $connection->returnResultSet($sql);
	
	if (count($info) > 0) {
		foreach ($info as $kk => $vv) {
			$_YearClass = $indexVar['libslrs']->getYearClass($vv["SubjectGroupIDA"]);
			$class = (($_YearClass) ? $_YearClass : '-');
			$classInfo[$class][] = $vv;  
		}
		ksort($classInfo);
	}
	
	$x .= "<div align=\"right\">"."\r\n";
	$x .= "<input type=\"button\" class=\"formsubbutton\" onclick=\"javascript:window.print();\" name=\"PrintBtn\" id=\"PrintBtn\" value=".$Lang['SLRS']['ReportPrint']." onmouseover=\"this.className='formsubbuttonon'\" onmouseout=\"this.className='formsubbutton'\">";
	$x .= "<br/><div id=\"resizeFontLoadingDiv\" style=\"display:none; float:right;\">
		<img src=\"/images/2009a/indicator.gif\"><span class=\"tabletextremark\"><span style=\"color:red;\">".$Lang['SLRS']['ReportProcessing']."</span></span></div>";
	$x .= "</div>"."\r\n";

	if (count($classInfo) > 0) {
		foreach ($classInfo as $class => $info) {
			$x .= "<div style='page-break-after: always;'>";
			$x .= "<table>"."\r\n";
			$x .= "<tr><td colspan=3>".$Lang['SLRS']['StudentDailyReport']."</td></tr>"."\r\n";
			$x .= "<tr><td colspan=3>".$date."</td></tr>"."\r\n";
			$x .= "<tr><td colspan=3>".$startDate."</td></tr></table><br/>"."\r\n";
			
			$x .= "<table class=\"common_table_list\">"."\r\n";
			$x .= "<thead>";
			$x .= "<tr>";
			// $x .= "<th>".$Lang['SLRS']['ReportDate']."</th>";
			// $x .= "<th>".$Lang['SLRS']['ReportDay']."</th>";
			$x .= "<th style='width:10%'>".$Lang['SLRS']['ReportClass']."</th>";
			$x .= "<th style='width:10%'>".$Lang['SLRS']['ReportPeriod']."</th>";
			$x .= "<th style='width:20%'>".$Lang['SLRS']['ReportSubject']."</th>";
			$x .= "<th style='width:15%'>".$Lang['SLRS']['ReportTeacher']."</th>"."\r\n";;
			$x .= "<th style='width:25%'>".$Lang['SLRS']['ReportRoom']."</th>";
			$x .= "<th style='width:25%'>".$Lang['SLRS']['LessonRemarks']."</th>";
			$x .= "</tr>"."\r\n";
			$x .= "</thead>";
			$locationSelection = $indexVar['libslrs']->getAllLocationSelection($date = '', $timeSlotID = '', $keepLocationID = '', $withNotAvailableLocation = true);
			$x .= "<tbody>";
			for($i=0;$i<sizeof($info);$i++){
				$x .= "<tr>"."\r\n";
				// $x .="<td>".(($info[$i]["TeacherA_Date"]==$info[$i]["TeacherB_Date"])?$info[$i]["TeacherA_Date"]:($info[$i]["TeacherA_Date"]." ".$Lang['SLRS']['ReportTo']." ".$info[$i]["TeacherB_Date"]))."</td>"."\r\n";
				// $x .="<td>".$info[$i]["CycleDay"]."</td>"."\r\n";
				$_YearClass = $indexVar['libslrs']->getYearClass($info[$i]["SubjectGroupIDA"]);
				$x .="<td>". (($_YearClass) ? $_YearClass : '-') . "</td>"."\r\n";
				$x .="<td>".$info[$i]["TimeSlotName"]."</td>"."\r\n";
				/*
				 $x .="<td>".$indexVar['libslrs']->getSubjectName($info[$i]["SubjectGroupIDA"],"subjectName")
				 .">".
				 $indexVar['libslrs']->getSubjectName($info[$i]["SubjectGroupIDB"],"subjectName") ."</td>"."\r\n";;
				 */
				$x .="<td>".$indexVar['libslrs']->getSubjectName($info[$i]["SubjectGroupIDA"],"subjectName") . "</td>";
				// $x .="<td>".$info[$i]["UserNameA"]." > ".$info[$i]["UserNameB"]."</td>"."\r\n";
				$info[$i]["UserNameB"] = (!empty($info[$i]["UserNameB"])) ? $info[$i]["UserNameB"] : "-";
				$x .="<td>".$info[$i]["UserNameB"]."</td>"."\r\n";
				
				$othersLocationA = $indexVar['libslrs']->getOthersLocation($info[$i]["TimeSlotID"],$info[$i]["SubjectGroupIDA"],$info[$i]["TeacherA_Date"]);
				$othersLocationB = $indexVar['libslrs']->getOthersLocation($info[$i]["TimeSlotID"],$info[$i]["SubjectGroupIDB"],$info[$i]["TeacherB_Date"]);
				/*
				 $x .="<td>".(Get_Lang_Selection($info[$i]["NameChiA"],$info[$i]["NameEngA"]) ?($indexVar['libslrs']->getLocationName($info[$i]["CodeA"],$info[$i]["NameChiA"],$info[$i]["NameEngA"])) :$othersLocationA)
				 .">".
				 (Get_Lang_Selection($info[$i]["NameChiB"],$info[$i]["NameEngB"])?($indexVar['libslrs']->getLocationName($info[$i]["CodeB"],$info[$i]["NameChiB"],$info[$i]["NameEngB"])) :$othersLocationB)."</td>"."\r\n";
				 */
				
				
				$x .= "<td>" . (!empty($locationSelection[$info[$i]["TeacherB_LocationID"]]) ? $locationSelection[$info[$i]["TeacherB_LocationID"]] : $othersLocationB) . "</td>";
				$x .= "<td>". ($info[$i]["LessonRemarks"] ? nl2br($info[$i]["LessonRemarks"]) : "-") . "</td>";
				$x .= "</tr>"."\r\n";
			}
			$x .= "</tbody>";
			$x .= "</table><br><br>"."\r\n";
			$x .= "</div>";
		}
		
	}
	
	

$htmlAry['contentTbl'] = $x;


?>