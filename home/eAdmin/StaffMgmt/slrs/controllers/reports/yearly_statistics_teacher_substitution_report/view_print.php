<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$connection = new libgeneralsettings();
$name_field = $indexVar['libslrs']->getNameFieldByLang($slrsConfig['INTRANET_USER'].".");
$academicYearID = $_GET["academicYearID"];
$semester = $_GET["semester"];
$semsterArray = explode(',',$semester);

$sql = "SELECT TermStart,TermEnd
			FROM(
				SELECT AcademicYearID FROM ACADEMIC_YEAR WHERE AcademicYearID=".IntegerSafe($academicYearID)."
			)as ay
			INNER JOIN(
				SELECT YearTermID,AcademicYearID,TermStart,TermEnd FROM ACADEMIC_YEAR_TERM WHERE YearTermID IN (".$semester.")
			) as ayt ON ay.AcademicYearID=ayt.AcademicYearID
			ORDER BY TermStart
	";
//echo $sql;
$info=$connection->returnResultSet($sql);
$startDate=$info[0]["TermStart"];
$endDate=$info[sizeof($info)-1]["TermEnd"];

$name_field = $indexVar['libslrs']->getNameFieldByLang($slrsConfig['INTRANET_USER'].".");

/*
$sql="
		SELECT UserID,UserName,CntLeave,CntSubstitution,CntPreYear,(CntSubstitution-CntLeave+CntPreYear) as Balance
			FROM(
			SELECT ist.UserID,UserName,COALESCE(CntLeave,'--') as CntLeave,COALESCE(CntSubstitution,'--') as CntSubstitution,COALESCE(BalanceAdjustTo,'--') as CntPreYear
			FROM(
				SELECT UserID FROM INTRANET_SLRS_TEACHER
			) as ist
			LEFT JOIN(
				SELECT UserID FROM INTRANET_SLRS_LEAVE WHERE DateLeave BETWEEN '".$startDate."' AND '".$endDate."'
				GROUP BY UserID
			) as isl ON ist.UserID=isl.UserID
			LEFT JOIN(
				SELECT a0.UserID,SubstituteUser as CntSubstitution,CntLeave,BalanceAdjustTo
				FROM( 
					SELECT UserID FROM INTRANET_SLRS_LEAVE WHERE DateLeave BETWEEN '".$startDate."' AND '".$endDate."'
					GROUP BY UserID 
				) as a0 
				INNER JOIN( 
					SELECT ArrangedTo_UserID,COUNT(ArrangedTo_UserID) as SubstituteUser
					FROM INTRANET_SLRS_LESSON_ARRANGEMENT GROUP BY ArrangedTo_UserID 
				) as a1 ON a0.UserID=a1.ArrangedTo_UserID
				INNER JOIN(
					SELECT UserID,COUNT(TimeSlotID) as CntLeave
					FROM INTRANET_SLRS_LESSON_ARRANGEMENT GROUP BY UserID 		
				) as a2 ON a0.UserID=a2.UserID
				LEFT JOIN(
					SELECT MIN(DateTimeInput) as DateTimeInput,UserID,BalanceAdjustID,BalanceAdjustTo
					FROM(
						SELECT YearTermID,AcademicYearID,TermStart,TermEnd FROM ACADEMIC_YEAR_TERM WHERE YearTermID IN (".$semsterArray[0].")
					) as ayt
					INNER JOIN(
						SELECT UserID,BalanceAdjustTo,DateTimeInput,BalanceAdjustID FROM INTRANET_SLRS_BALANCE_ADJUST
					) as isba ON isba.DateTimeInput BETWEEN TermStart AND TermEnd
					GROUP BY UserID
				) as a3 ON a0.UserID=a3.UserID					
			) as a ON ist.UserID=a.UserID
			LEFT JOIN(
				SELECT UserID,".$name_field." as UserName FROM ".$slrsConfig['INTRANET_USER']."
			) as iu ON ist.UserID =iu.UserID						
		) as a0
	";
//echo $sql;
$info=$connection->returnResultSet($sql);
*/
$reportData = $indexVar['libslrs']->getYearStatSubstitutionReportData($academicYearID, $semester, $semsterArray, $filterNoAffectLeaveReason=true);

$x .= "<div align=\"right\">"."\r\n";
$x .= "<input type=\"button\" class=\"formsubbutton\" onclick=\"javascript:window.print();\" name=\"PrintBtn\" id=\"PrintBtn\" value=".$Lang['SLRS']['ReportPrint']." onmouseover=\"this.className='formsubbuttonon'\" onmouseout=\"this.className='formsubbutton'\">";
$x .= "<br/><div id=\"resizeFontLoadingDiv\" style=\"display:none; float:right;\">
	<img src=\"/images/2009a/indicator.gif\"><span class=\"tabletextremark\"><span style=\"color:red;\">".$Lang['SLRS']['ReportProcessing']."</span></span></div>";

$x .= "</div>"."\r\n";
$x .= "<br/><br/>";

$x .= "<table>"."\r\n";
$x .= "<tr><td colspan=3>".$Lang['SLRS']['ReportYearlyStatisticsofTeacherSubstitution']."</td></tr>"."\r\n";
$x .= "</table><br/>"."\r\n";

/*
$x .= "<table class=\"common_table_list\">"."\r\n";
$x .= "<tr><th>".$Lang['SLRS']['ReportTC']."</th><th>".$Lang['SLRS']['ReportPeriodSickLeave']."</th><th>".$Lang['SLRS']['ReportPeriodSubstitition']."</th><th>".$Lang['SLRS']['ReportCarriedPreviousYear']."</th><th>".$Lang['SLRS']['ReportBalance']."</th></tr>"."\r\n";

//echo sizeof($info);

for($i=0;$i<sizeof($info);$i++){
	$x .= "<tr>"."\r\n";
	$x .="<td>".$info[$i]["UserName"]."</td>"."\r\n";
	$x .="<td>".$info[$i]["CntLeave"]."</td>"."\r\n";
	$x .="<td>".$info[$i]["CntSubstitution"]."</td>"."\r\n";
	$x .="<td>".$info[$i]["CntPreYear"]."</td>"."\r\n";
	$x .="<td>".$info[$i]["Balance"]."</td>"."\r\n";
	$x .= "</tr>"."\r\n";
}
$x .= "</table>"."\r\n";
*/

$x .= "<table class=\"common_table_list\">"."\r\n";
$x .= "<thead>"."\r\n";
$x .= "<tr>"."\r\n";
$x .= "<th style='width:40%'>".$Lang['SLRS']['ReportTC']."</th>"."\r\n";
$x .= "<th style='width:15%'>".$Lang['SLRS']['ReportPeriodSickLeave']."</th>"."\r\n";
$x .= "<th style='width:15%'>".$Lang['SLRS']['ReportPeriodSubstitition']."</th>"."\r\n";
$x .= "<th style='width:15%'>".$Lang['SLRS']['ReportCarriedPreviousYear']."</th>"."\r\n";
$x .= "<th style='width:15%'>".$Lang['SLRS']['ReportBalance']."</th>"."\r\n";
$x .= "</tr>"."\r\n";
$x .= "</thead>"."\r\n";
$x .= "<tbody>"."\r\n";

if (count($reportData) > 0)
{
	foreach ($reportData as $_UserID => $rData) {
		$x .= "<tr>"."\r\n";
		$x .="<td>".$rData["UserName"]."</td>"."\r\n";
		$x .="<td>". (empty($rData["CntLeave"]) ? '--' : $rData["CntLeave"]) ."</td>"."\r\n";
		$x .="<td>". (empty($rData["SubstituteUser"]) ? '--' : $rData["SubstituteUser"]) ."</td>"."\r\n";
		$x .="<td>". (empty($rData["BalanceAdjustTo"]) ? '0' : $rData["BalanceAdjustTo"]) ."</td>"."\r\n";
		$x .="<td>". $rData["balance"] ."</td>"."\r\n";
		$x .= "</tr>"."\r\n";
	}
}
$x .= "</tbody>"."\r\n";
$x .= "</table>"."\r\n";
echo $x;




?>