<?php
// using : 
/*
 * Change Log:
 * 2017-02-03 Omas Add $junior_mck for ej 
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/slrs_lang.$intranet_session_language.php");

intranet_opendb();

include_once($PATH_WRT_ROOT."includes/slrs/slrsConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/slrs/libslrs.php");
include_once($PATH_WRT_ROOT."includes/slrs/libslrs_ui.php");

$home_header_no_EmulateIE7 = true;

if(isset($junior_mck)){
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.utf8.php");
	include_once($PATH_WRT_ROOT."includes/libschooltimetable.php");
	$g_encoding_unicode = true; // Change encoding from Big5 to UTF-8
	$module_custom['isUTF8'] = true;
}

## Customization extends 
if (isset($sys_custom["SLRS"]["custom_library"]) && file_exists($PATH_WRT_ROOT."includes/slrs/customization/libslrs_" . $sys_custom["SLRS"]["custom_library"] . ".custom.php")) {
	require_once($PATH_WRT_ROOT."includes/slrs/customization/libslrs_" . $sys_custom["SLRS"]["custom_library"] . ".custom.php");
	$indexVar['libslrs'] = new libslrs_customization();
} else {
	$indexVar['libslrs'] = new libslrs();
}
$indexVar['libslrs_ui'] = new libslrs_ui();
$indexVar['curUserId'] = $_SESSION['UserID'];


function custHandleFormPost($item, $key) {
	
	$replaceArr = array(
		"'" => "&#39;",
		'"' => "&#34;"
	);
	return str_replace(array_keys($replaceArr), array_values($replaceArr), $item);
}

intranet_auth();
intranet_opendb();
$indexVar['libslrs']->checkAccessRight();

### handle all http post/get value by urldeocde, stripslashes, trim
array_walk_recursive($_POST, 'custHandleFormPost');
array_walk_recursive($_GET, 'custHandleFormPost'); 

if ($junior_mck) {
	$is_magic_quotes_active = (function_exists("get_magic_quotes_gpc") && get_magic_quotes_gpc()) || function_exists("magicQuotes_addslashes");
	
	function magicQuotes_stripSlashes(&$item, $key)
	{
		$item = stripslashes($item);
	}
	if (count($_POST) > 0 && $is_magic_quotes_active) {
		$gpc = array(&$_POST, &$_GET);
		array_walk_recursive($gpc, 'magicQuotes_stripSlashes');
	}
}

### get system message
$returnMsgKey = $_GET['returnMsgKey'];
$indexVar['returnMsgLang'] = $Lang['General']['ReturnMessage'][$returnMsgKey];


### include corresponding php files
$indexVar['task'] = $_POST['task']? $_POST['task'] : $_GET['task'];


// load normal pages
if ($indexVar['task']=='') {
	// go to module index page if not defined
	$indexVar['task'] = 'management'.$slrsConfig['taskSeparator'].'substitute_arrangement'.$slrsConfig['taskSeparator'].'list';
}

$indexVar['controllerScript'] = 'controllers/'.str_replace($slrsConfig['taskSeparator'], '/', $indexVar['task']).'.php';
if (file_exists($indexVar['controllerScript'])){
	include_once($indexVar['controllerScript']);
	
	$indexVar['viewScript'] = 'views/'.str_replace($slrsConfig['taskSeparator'], '/', $indexVar['task']).'.tmpl.php';
	if (file_exists($indexVar['viewScript'])){
		
		// normal script => with template script
		include_once($indexVar['viewScript']);
		
		if( strpos($indexVar['task'], 'print') !== false || strpos($indexVar['task'], 'thickbox') !== false) {
			// print page and thickbox no footer
		}
		else {
			$indexVar['libslrs_ui']->echoModuleLayoutStop();
		}
	}
	else {
		// update or ajax script => without template script
	}
}
else {
	No_Access_Right_Pop_Up();
}

// initial SLRS module default setting
$indexVar['libslrs']->setDefaultSettings();

intranet_closedb();
?>