<?php

// Modifing by : Jason

/********************** Change Log ***********************/
#
#	Date	: 	2011-12-08 (Yuen)
#	Details	: 	change default number of users from 50 to 300
#
#	Date	: 	2011-11-16 (Jason)
#	Details	: 	add the PowerLesson Right Settings in eClass Management if enable only
#
# 	Date	:	2010-04-16 [Yuen]
#	Details	:	add default max course storage setting and change default user # from 30 to 50
#				will be adopted when create new courses
#
/******************* End Of Change Log *******************/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");

intranet_auth();
eclass_opendb();

$specialClassroom = true;

# eClass
$lo 		= new libeclass();
$leclass	= new libeclass2007('', $roomType, $specialClassroom);
$eclass_quota 	= $lo->status();
$lu 		= new libuser($UserID);

# block illegal access
$la = new libaccess($UserID);
$la->retrieveEClassSettings();
$la->retrieveAccessEClass();
if(!$_SESSION['SSV_USER_ACCESS']['eLearning-eClass'])
{
  if ($lu->teaching&&!$la->isAccessEClassMgtCourse() || !$lu->teaching&&$lu->RecordType==1&& !$la->isAccessEClassNTMgtCourse() || $lu->RecordType!=1 || !$la->isAccessEClassMgt() )
  {
    header("Location: /");
  }
}

$default_max_quota = $la->eclass_settings_loaded["default_max_quota"];
if ($default_max_quota=="" || $default_max_quota<0)
{
	$default_max_quota = 30;
}

$linterface 	= new interface_html();
$CurrentPage	= "PageManagement";

#### subject and subject group select box #####
$libdb = new libdb();
$fieldName = $intranet_session_language=="en"?"EN_DES":"CH_DES";
$sql = "select RecordID, $fieldName as SubjectName from ASSESSMENT_SUBJECT where RecordStatus = 1 AND
(CMP_CODEID IS NULL OR CMP_CODEID = '')";
$result = $libdb->returnArray($sql);
$subjectBox = '<select id="subjectID" name="subjectID" onchange="getSelectGroup()">';
$subjectBox .= "<option value=''>--{$button_select}--</option>";
foreach ($result as $r){
	$subjectBox .= '<option value="'.$r["RecordID"].'" '.$selected.'>'.$r["SubjectName"].'</option>';
}
$subjectBox .= '</select>';
$subjectBox .= '&nbsp;&nbsp;<div id="subjectGroupSelectBox" style="display:inline"></div>';

$groupList = "<select name='subjectGroupID[]' id='subjectGroupList' multiple>";  
$groupList .= "</select>";

$subjectBox .= '<div id="subjectGroupListBox">'.$groupList.'<br />
'.$linterface->GET_ACTION_BTN($Lang['Btn']['RemoveSelected'], "button", "javascript:subjectGroupHandler.removeFromList();",$Lang['Btn']['RemoveSelected']).'
</div>';
###############################################

#################### Subject Group Mapping ########################
$sql = "select t.SubjectID, t.SubjectGroupID 
		from {$intranet_db}.SUBJECT_TERM as t
		where
			t.YearTermID in (
				select
					ayt.YearTermID
				from
					{$intranet_db}.ACADEMIC_YEAR as ay
					inner join
					{$intranet_db}.ACADEMIC_YEAR_TERM as ayt
					on ay.AcademicYearID = ayt.AcademicYearID
				where NOW() between ayt.TermStart and ayt.TermEnd
			)
			order by t.SubjectID, t.SubjectGroupID 
		";
$result = $libdb->returnArray($sql);
$asso = array();
$jsMapping = '{}';
if (!empty($result)){
	foreach($result as $r){
		$asso[$r[0]][] = $r[1];
	}
	$jsMapping = '{';
	foreach($asso as $sid=>$gid){
		$jsMapping .= $sid.':[';
		if (!empty($gid)){
			$jsMapping .= implode(',',$gid);
		}
		$jsMapping .= '],';
	}
	$jsMapping= rtrim($jsMapping,',');
	$jsMapping .= '}';
}
###################################################################

$tool_select = "";
# Check Equation Editor
if ($lo->license_equation !=0)
{
	$content = trim(get_file_content($lo->filepath."/files/equation.txt"));
	$equation_class = ($content=="")? array(): explode(",",$content);
	$left = $lo->license_equation - sizeof($equation_class);
	if ($left > 0)
	{
		$tool_select .= "<input type=checkbox name=hasEquation value=1> $i_eClass_Tool_EquationEditor ($i_eClass_Tool_LicenseLeft: $left)";
	}
}

$powerlesson_select = "";
## Check PowerLesson Rights
if($plugin['WWS_eLearningProject']){
	# always enable for WWS eLearning Project
	$powerlesson_select .= '<input type="hidden" id="enablePowerLesson" name="enablePowerLesson" value="1" /> ';
} else {
	if ($ck_power_lesson == true){
		$powerlesson_select .= ($tool_select != '') ? '<br>' : '';
		$powerlesson_select .= '<input type="checkbox" id="enablePowerLesson" name="enablePowerLesson" value="1" /> ';
		$powerlesson_select .= '<label for="enablePowerLesson">'.$Lang['PowerLesson']['PowerLesson'].'</label>';
	}
}

### Title ###
$TAGS_OBJ[] = array($i_frontpage_menu_eclass_mgt,"");
$MODULE_OBJ = $leclass->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_new.($intranet_session_language=="en"?" ":""). $i_adminmenu_eclass, "");
?>
<script language="javascript">
function checkform(obj){
$('#subjectGroupList option').attr('selected','selected');
  if(!check_text(obj.course_code, "<?php echo $i_alert_pleasefillin.$i_eClassCourseCode; ?>.")) return false;
  if(!check_text(obj.course_name, "<?php echo $i_alert_pleasefillin.$i_eClassCourseName; ?>.")) return false;
//  if ($('#subjectGroupList option:selected').length == 0){
//	alert('<?=$Lang['eclass']['warning']['noSubjectGroup']?>');
//  }
}

function getSelectGroup(){
	selectedValue = document.getElementById('subjectID').options[document.getElementById('subjectID').selectedIndex].value;
	document.body.style.cursor='wait';
	var param = {};
	param['subjectID'] = selectedValue;
	
	var sbjList= [];
	$('#subjectGroupList option').each(function(){
		sbjList.push(this.value);
	});
	param['exclusList[]'] = sbjList;
	
	$('div#subjectGroupSelectBox').load(
		'getSelectSubjectGroup.php',
		param,
		function(){
			document.body.style.cursor='default';
		}
	);
}

var subjectGroupHandler=(function(){
	var groupMapping = <?=$jsMapping?>;
	return {
		addSubjectGroup:function (obj){
			if (obj.selectedIndex == 0){
				return;
			}
			
			var selectedOpt = $('#subjectGroupID option:selected');
			
			$('#subjectGroupList').append(selectedOpt);
		},
		removeFromList:function(){
			var groupList = groupMapping[$('#subjectID option:selected').val()];
			$('#subjectGroupList option:selected').each(function(){
				if (groupList instanceof Array && $.inArray(parseInt(this.value),groupList)!=-1){
					this.selected = false;
					 $('#subjectGroupID').append(this);
				}
				else{
					$(this).remove();
				}
			});
		}
	}	
})();
</script>


<br />
<form name="form1" action="new_update.php" method="post" onSubmit="return checkform(this);">
  <table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
      <td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
    </tr>

    <tr>
      <td colspan="2">
        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr>
            <td><br />
            <?
            if($leclass->checkeSpecialRoomtAccessPermission()){
            ?>
              <table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
			<? 
			//if($lo->license == "" || $lo->ticket()>0){
			?>
                <tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_eClassCourseCode?> <span class='tabletextrequire'>*</span></span></td>
                  <td><input name="course_code" type="text" class="textboxnum" maxlength="10" value="" /></td>
                </tr>
                <tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_eClassCourseName?> <span class='tabletextrequire'>*</span></span></td>
                  <td><input name="course_name" type="text" class="textboxtext" maxlength="100" value="" /></td>
                </tr>
                <tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_eClassCourseDesc?> </span></td>
                  <td><?=$linterface->GET_TEXTAREA("course_desc", "");?></td>
                </tr>
                <tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_eClassNumUsers?> </span></td>
                  <td><input name="max_user" type="text" class="textboxnum" maxlength="5" value="300" /></td>
                </tr>
                <tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_eClassMaxStorage?> </span></td>
                  <td class="tabletext"><input name="max_storage" type="text" class="textboxnum" maxlength="5" value="<?=$default_max_quota?>" /> MB</td>
                </tr>
<? /*
				<!-- subject group -->
				<tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$Lang['SysMgr']['Homework']['SubjectGroup']?></span></td>
                  <td class="tabletext"><?=$subjectBox?></td>
                </tr>
*/ ?>
<? if($plugin['WWS_eLearningProject']){
		#### Subject Select for Wong Wah San Project ####
		$subjectSelection  = '<select id="wws_subject" name="wws_subject">';
		$subjectSelection .= '<option value="chi">'.$Lang['WWS']['Subject_Chinese'].'</option>';
		$subjectSelection .= '<option value="eng">'.$Lang['WWS']['Subject_English'].'</option>';
		$subjectSelection .= '<option value="ls">'.$Lang['WWS']['Subject_LS'].'</option>';
		$subjectSelection .= '</select>'; 
?>
				<tr valign="top">
				  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_eClass_SubjectLink?></td>
				  <td class="tabletext">
				  <?=$subjectSelection?>
				  </td>
                </tr>
<? } ?>
<? if ($tool_select != "" || $powerlesson_select != "") { ?>
                <tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_eClass_Tools?> </span></td>
                  <td class="tabletext"><?=$tool_select.$powerlesson_select?></td>
                </tr>
<? } ?>
<? /*
                <tr valign="top">
                  <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">&nbsp;</td>
                  <td class="tabletext">
				  <?php echo $lo->eClassCategory(); ?>
				  </td>
                </tr>
*/ ?>
				<? /*} 
				else {?>
                <tr valign="top">
                  <td class="tabletext" colspan="2" align="center"><?=$i_eClassLicenseFull?></td>
                </tr>
				<? } */ ?>
              </table>
              <? } ?>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
		<? 
		//if($lo->license == "" || $lo->ticket()>0){ 
		if($leclass->checkeSpecialRoomtAccessPermission(false)){
		?>
          <tr>
            <td align="left" class="tabletextremark">&nbsp;&nbsp;&nbsp;<?=$i_general_required_field2?></td>
          </tr>
          <tr>
            <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
          </tr>
          <tr>
            <td align="center">
              <?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submit2") ?>
              <?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2") ?>
              <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back();","cancelbtn") ?>
            </td>
          </tr>
		<? } else {?>
          <tr>
            <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
          </tr>
          <tr>
            <td align="center">
              <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back();","cancelbtn") ?>
            </td>
          </tr>
		<? } ?>
        </table>
      </td>
    </tr>
  </table>
  <br />
  <input type="hidden" id="roomType" name="roomType" value="<?=$roomType?>" />
</form>

<?php
print $linterface->FOCUS_ON_LOAD("form1.course_code");
$linterface->LAYOUT_STOP();
eclass_closedb();
?>